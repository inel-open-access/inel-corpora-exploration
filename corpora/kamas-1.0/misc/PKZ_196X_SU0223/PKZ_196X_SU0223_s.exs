<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID06E2E697-E9DA-D9F1-D00E-00757749FCAC">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SU0223.wav" />
         <referenced-file url="PKZ_196X_SU0223.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0223\PKZ_196X_SU0223.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1513</ud-information>
            <ud-information attribute-name="# HIAT:w">834</ud-information>
            <ud-information attribute-name="# e">880</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">48</ud-information>
            <ud-information attribute-name="# HIAT:u">224</ud-information>
            <ud-information attribute-name="# sc">18</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.47" type="appl" />
         <tli id="T1" time="1.083" type="appl" />
         <tli id="T2" time="1.696" type="appl" />
         <tli id="T3" time="2.309" type="appl" />
         <tli id="T4" time="2.922" type="appl" />
         <tli id="T5" time="3.535" type="appl" />
         <tli id="T6" time="4.441" type="appl" />
         <tli id="T7" time="5.348" type="appl" />
         <tli id="T8" time="6.254" type="appl" />
         <tli id="T9" time="7.16" type="appl" />
         <tli id="T10" time="8.066" type="appl" />
         <tli id="T11" time="8.973" type="appl" />
         <tli id="T12" time="10.173314221809086" />
         <tli id="T13" time="11.658" type="appl" />
         <tli id="T14" time="12.825" type="appl" />
         <tli id="T15" time="13.992" type="appl" />
         <tli id="T16" time="15.16" type="appl" />
         <tli id="T17" time="16.327" type="appl" />
         <tli id="T18" time="17.494" type="appl" />
         <tli id="T19" time="18.499" type="appl" />
         <tli id="T20" time="19.504" type="appl" />
         <tli id="T21" time="20.51" type="appl" />
         <tli id="T22" time="21.515" type="appl" />
         <tli id="T23" time="28.71327939274688" />
         <tli id="T24" time="29.6" type="appl" />
         <tli id="T25" time="30.44" type="appl" />
         <tli id="T26" time="31.28" type="appl" />
         <tli id="T27" time="32.12" type="appl" />
         <tli id="T28" time="33.854" type="appl" />
         <tli id="T29" time="35.579" type="appl" />
         <tli id="T30" time="37.305" type="appl" />
         <tli id="T31" time="39.03" type="appl" />
         <tli id="T32" time="42.299920535634776" />
         <tli id="T33" time="43.351" type="appl" />
         <tli id="T34" time="44.144" type="appl" />
         <tli id="T35" time="44.938" type="appl" />
         <tli id="T36" time="46.006580238993784" />
         <tli id="T37" time="46.61" type="appl" />
         <tli id="T38" time="47.178" type="appl" />
         <tli id="T39" time="47.747" type="appl" />
         <tli id="T40" time="48.315" type="appl" />
         <tli id="T41" time="48.884" type="appl" />
         <tli id="T42" time="49.452" type="appl" />
         <tli id="T43" time="50.021" type="appl" />
         <tli id="T44" time="50.589" type="appl" />
         <tli id="T45" time="51.66656960617328" />
         <tli id="T46" time="52.88" type="appl" />
         <tli id="T47" time="53.911" type="appl" />
         <tli id="T48" time="54.931" type="appl" />
         <tli id="T49" time="55.951" type="appl" />
         <tli id="T50" time="57.103" type="appl" />
         <tli id="T51" time="58.04" type="appl" />
         <tli id="T52" time="58.978" type="appl" />
         <tli id="T53" time="60.2665534502976" />
         <tli id="T54" time="61.701" type="appl" />
         <tli id="T55" time="62.975" type="appl" />
         <tli id="T56" time="64.25" type="appl" />
         <tli id="T57" time="65.525" type="appl" />
         <tli id="T58" time="66.8" type="appl" />
         <tli id="T59" time="68.074" type="appl" />
         <tli id="T60" time="69.75986894954802" />
         <tli id="T61" time="70.708" type="appl" />
         <tli id="T62" time="71.651" type="appl" />
         <tli id="T63" time="72.593" type="appl" />
         <tli id="T64" time="73.536" type="appl" />
         <tli id="T65" time="74.478" type="appl" />
         <tli id="T66" time="75.42" type="appl" />
         <tli id="T67" time="76.363" type="appl" />
         <tli id="T68" time="77.71318734182735" />
         <tli id="T69" time="78.64" type="appl" />
         <tli id="T70" time="79.323" type="appl" />
         <tli id="T71" time="80.006" type="appl" />
         <tli id="T72" time="80.689" type="appl" />
         <tli id="T73" time="81.372" type="appl" />
         <tli id="T74" time="82.13317903845868" />
         <tli id="T75" time="82.875" type="appl" />
         <tli id="T76" time="83.507" type="appl" />
         <tli id="T77" time="84.49317460498584" />
         <tli id="T78" time="85.322" type="appl" />
         <tli id="T79" time="86.186" type="appl" />
         <tli id="T80" time="87.051" type="appl" />
         <tli id="T81" time="87.916" type="appl" />
         <tli id="T82" time="88.78" type="appl" />
         <tli id="T83" time="89.96649765616883" />
         <tli id="T84" time="91.343" type="appl" />
         <tli id="T85" time="92.52" type="appl" />
         <tli id="T86" time="94.30648950308739" />
         <tli id="T87" time="95.11" type="appl" />
         <tli id="T88" time="95.764" type="appl" />
         <tli id="T89" time="96.418" type="appl" />
         <tli id="T90" time="97.072" type="appl" />
         <tli id="T91" time="97.726" type="appl" />
         <tli id="T92" time="98.38" type="appl" />
         <tli id="T93" time="99.034" type="appl" />
         <tli id="T94" time="100.496" type="appl" />
         <tli id="T95" time="101.703" type="appl" />
         <tli id="T96" time="102.909" type="appl" />
         <tli id="T97" time="103.588" type="appl" />
         <tli id="T98" time="104.267" type="appl" />
         <tli id="T99" time="104.946" type="appl" />
         <tli id="T100" time="105.625" type="appl" />
         <tli id="T101" time="106.62646635885618" />
         <tli id="T102" time="107.84" type="appl" />
         <tli id="T103" time="108.7" type="appl" />
         <tli id="T104" time="109.56" type="appl" />
         <tli id="T105" time="110.42" type="appl" />
         <tli id="T106" time="111.28" type="appl" />
         <tli id="T107" time="112.499" type="appl" />
         <tli id="T108" time="113.372" type="appl" />
         <tli id="T109" time="114.246" type="appl" />
         <tli id="T110" time="115.755" type="appl" />
         <tli id="T111" time="116.665" type="appl" />
         <tli id="T112" time="117.576" type="appl" />
         <tli id="T113" time="118.486" type="appl" />
         <tli id="T114" time="119.397" type="appl" />
         <tli id="T115" time="120.307" type="appl" />
         <tli id="T116" time="121.15133490586264" />
         <tli id="T117" time="121.744" type="appl" />
         <tli id="T118" time="122.27" type="appl" />
         <tli id="T119" time="122.797" type="appl" />
         <tli id="T120" time="124.01976701724406" />
         <tli id="T121" time="124.846" type="appl" />
         <tli id="T122" time="125.558" type="appl" />
         <tli id="T123" time="126.269" type="appl" />
         <tli id="T124" time="126.98" type="appl" />
         <tli id="T125" time="127.725" type="appl" />
         <tli id="T126" time="128.385" type="appl" />
         <tli id="T127" time="129.045" type="appl" />
         <tli id="T128" time="129.86642270042006" />
         <tli id="T129" time="130.667" type="appl" />
         <tli id="T130" time="131.379" type="appl" />
         <tli id="T131" time="132.09" type="appl" />
         <tli id="T132" time="132.801" type="appl" />
         <tli id="T133" time="133.513" type="appl" />
         <tli id="T134" time="134.224" type="appl" />
         <tli id="T135" time="135.163" type="appl" />
         <tli id="T136" time="135.864" type="appl" />
         <tli id="T137" time="136.564" type="appl" />
         <tli id="T138" time="137.265" type="appl" />
         <tli id="T139" time="137.966" type="appl" />
         <tli id="T140" time="138.666" type="appl" />
         <tli id="T141" time="139.367" type="appl" />
         <tli id="T142" time="140.33306970450934" />
         <tli id="T143" time="141.311" type="appl" />
         <tli id="T144" time="142.198" type="appl" />
         <tli id="T145" time="143.084" type="appl" />
         <tli id="T146" time="144.29972891943493" />
         <tli id="T147" time="145.188" type="appl" />
         <tli id="T148" time="146.027" type="appl" />
         <tli id="T149" time="146.866" type="appl" />
         <tli id="T150" time="147.706" type="appl" />
         <tli id="T151" time="148.546" type="appl" />
         <tli id="T152" time="149.385" type="appl" />
         <tli id="T153" time="150.316" type="appl" />
         <tli id="T154" time="151.247" type="appl" />
         <tli id="T155" time="152.178" type="appl" />
         <tli id="T156" time="153.36637855355048" />
         <tli id="T157" time="154.203" type="appl" />
         <tli id="T158" time="154.954" type="appl" />
         <tli id="T159" time="155.706" type="appl" />
         <tli id="T160" time="156.8397053619139" />
         <tli id="T161" time="157.598" type="appl" />
         <tli id="T162" time="158.214" type="appl" />
         <tli id="T163" time="159.1330343870137" />
         <tli id="T164" time="160.123" type="appl" />
         <tli id="T165" time="160.919" type="appl" />
         <tli id="T166" time="161.714" type="appl" />
         <tli id="T167" time="162.51" type="appl" />
         <tli id="T168" time="163.306" type="appl" />
         <tli id="T169" time="164.22635815206098" />
         <tli id="T170" time="164.927" type="appl" />
         <tli id="T171" time="165.534" type="appl" />
         <tli id="T172" time="166.55302044787447" />
         <tli id="T173" time="167.271" type="appl" />
         <tli id="T174" time="167.806" type="appl" />
         <tli id="T175" time="168.341" type="appl" />
         <tli id="T176" time="168.876" type="appl" />
         <tli id="T177" time="169.41" type="appl" />
         <tli id="T178" time="169.945" type="appl" />
         <tli id="T179" time="170.48" type="appl" />
         <tli id="T180" time="171.015" type="appl" />
         <tli id="T181" time="171.55" type="appl" />
         <tli id="T182" time="172.795" type="appl" />
         <tli id="T183" time="174.011" type="appl" />
         <tli id="T184" time="175.228" type="appl" />
         <tli id="T185" time="176.253" type="appl" />
         <tli id="T186" time="176.923" type="appl" />
         <tli id="T187" time="177.593" type="appl" />
         <tli id="T188" time="178.797" type="appl" />
         <tli id="T189" time="179.697" type="appl" />
         <tli id="T190" time="180.596" type="appl" />
         <tli id="T191" time="181.496" type="appl" />
         <tli id="T192" time="182.395" type="appl" />
         <tli id="T193" time="183.294" type="appl" />
         <tli id="T194" time="184.194" type="appl" />
         <tli id="T195" time="185.093" type="appl" />
         <tli id="T196" time="185.992" type="appl" />
         <tli id="T197" time="186.892" type="appl" />
         <tli id="T198" time="187.791" type="appl" />
         <tli id="T199" time="188.691" type="appl" />
         <tli id="T200" time="189.59" type="appl" />
         <tli id="T201" time="190.482" type="appl" />
         <tli id="T202" time="192.8063044619532" />
         <tli id="T203" time="193.625" type="appl" />
         <tli id="T204" time="194.3" type="appl" />
         <tli id="T205" time="194.976" type="appl" />
         <tli id="T206" time="195.651" type="appl" />
         <tli id="T207" time="196.326" type="appl" />
         <tli id="T208" time="197.598" type="appl" />
         <tli id="T209" time="198.345" type="appl" />
         <tli id="T210" time="199.092" type="appl" />
         <tli id="T211" time="199.839" type="appl" />
         <tli id="T212" time="200.586" type="appl" />
         <tli id="T213" time="201.333" type="appl" />
         <tli id="T214" time="202.08" type="appl" />
         <tli id="T215" time="203.065" type="appl" />
         <tli id="T216" time="203.835" type="appl" />
         <tli id="T217" time="204.75961533987171" />
         <tli id="T218" time="205.45" type="appl" />
         <tli id="T219" time="206.075" type="appl" />
         <tli id="T220" time="206.7" type="appl" />
         <tli id="T221" time="207.324" type="appl" />
         <tli id="T222" time="207.948" type="appl" />
         <tli id="T223" time="208.72627455479733" />
         <tli id="T224" time="209.445" type="appl" />
         <tli id="T225" time="209.958" type="appl" />
         <tli id="T226" time="210.47" type="appl" />
         <tli id="T227" time="211.223" type="appl" />
         <tli id="T384" time="211.5457142857143" type="intp" />
         <tli id="T228" time="211.976" type="appl" />
         <tli id="T229" time="212.73" type="appl" />
         <tli id="T230" time="214.1129311021248" />
         <tli id="T231" time="215.012" type="appl" />
         <tli id="T232" time="215.681" type="appl" />
         <tli id="T233" time="216.35" type="appl" />
         <tli id="T234" time="217.93292392591027" />
         <tli id="T235" time="219.066" type="appl" />
         <tli id="T236" time="219.971" type="appl" />
         <tli id="T237" time="220.875" type="appl" />
         <tli id="T238" time="221.779" type="appl" />
         <tli id="T239" time="222.684" type="appl" />
         <tli id="T240" time="223.588" type="appl" />
         <tli id="T241" time="224.493" type="appl" />
         <tli id="T242" time="225.4662431071975" />
         <tli id="T243" time="226.108" type="appl" />
         <tli id="T244" time="226.818" type="appl" />
         <tli id="T245" time="227.529" type="appl" />
         <tli id="T246" time="228.239" type="appl" />
         <tli id="T247" time="229.04623638184458" />
         <tli id="T248" time="229.578" type="appl" />
         <tli id="T249" time="230.167" type="appl" />
         <tli id="T250" time="230.755" type="appl" />
         <tli id="T251" time="231.343" type="appl" />
         <tli id="T252" time="231.932" type="appl" />
         <tli id="T253" time="232.52" type="appl" />
         <tli id="T254" time="233.185" type="appl" />
         <tli id="T255" time="233.641" type="appl" />
         <tli id="T256" time="234.096" type="appl" />
         <tli id="T257" time="234.552" type="appl" />
         <tli id="T258" time="235.007" type="appl" />
         <tli id="T259" time="235.463" type="appl" />
         <tli id="T260" time="235.918" type="appl" />
         <tli id="T261" time="236.374" type="appl" />
         <tli id="T262" time="239.41288357379284" />
         <tli id="T263" time="241.371" type="appl" />
         <tli id="T264" time="244.07954147370532" />
         <tli id="T265" time="245.5328720768209" />
         <tli id="T266" time="247.5928682069251" />
         <tli id="T267" time="248.77953264433137" />
         <tli id="T884" time="249.30286499453587" type="intp" />
         <tli id="T268" time="249.82619734474034" />
         <tli id="T269" time="250.39286294687255" />
         <tli id="T270" time="251.839" type="appl" />
         <tli id="T271" time="252.944" type="appl" />
         <tli id="T272" time="254.112855958517" />
         <tli id="T273" time="254.571" type="appl" />
         <tli id="T274" time="255.092" type="appl" />
         <tli id="T275" time="255.614" type="appl" />
         <tli id="T276" time="256.136" type="appl" />
         <tli id="T277" time="256.658" type="appl" />
         <tli id="T278" time="257.179" type="appl" />
         <tli id="T279" time="257.701" type="appl" />
         <tli id="T280" time="258.325" type="appl" />
         <tli id="T281" time="258.798" type="appl" />
         <tli id="T282" time="259.272" type="appl" />
         <tli id="T283" time="260.3928441609706" />
         <tli id="T883" time="260.9879220804853" type="intp" />
         <tli id="T284" time="261.583" type="appl" />
         <tli id="T285" time="262.572" type="appl" />
         <tli id="T286" time="263.562" type="appl" />
         <tli id="T287" time="264.552" type="appl" />
         <tli id="T288" time="265.541" type="appl" />
         <tli id="T289" time="266.531" type="appl" />
         <tli id="T290" time="267.521" type="appl" />
         <tli id="T291" time="268.51" type="appl" />
         <tli id="T292" time="269.5" type="appl" />
         <tli id="T293" time="270.138" type="appl" />
         <tli id="T294" time="270.775" type="appl" />
         <tli id="T295" time="271.412" type="appl" />
         <tli id="T296" time="272.05" type="appl" />
         <tli id="T297" time="273.5061528597245" />
         <tli id="T298" time="274.602" type="appl" />
         <tli id="T299" time="275.58" type="appl" />
         <tli id="T300" time="276.558" type="appl" />
         <tli id="T301" time="278.2928105342062" />
         <tli id="T302" time="279.065" type="appl" />
         <tli id="T303" time="279.625" type="appl" />
         <tli id="T304" time="280.185" type="appl" />
         <tli id="T305" time="281.91" type="appl" />
         <tli id="T306" time="285.01279791008" />
         <tli id="T307" time="285.905" type="appl" />
         <tli id="T308" time="286.67" type="appl" />
         <tli id="T309" time="288.67" type="appl" />
         <tli id="T310" time="289.795" type="appl" />
         <tli id="T311" time="290.705" type="appl" />
         <tli id="T312" time="291.87278502295123" />
         <tli id="T313" time="293.095" type="appl" />
         <tli id="T314" time="296.26611010301167" />
         <tli id="T315" time="297.71" type="appl" />
         <tli id="T316" time="303.1394305241684" />
         <tli id="T317" time="304.276" type="appl" />
         <tli id="T318" time="305.362" type="appl" />
         <tli id="T319" time="306.448" type="appl" />
         <tli id="T320" time="307.534" type="appl" />
         <tli id="T321" time="308.62" type="appl" />
         <tli id="T322" time="309.706" type="appl" />
         <tli id="T323" time="310.792" type="appl" />
         <tli id="T324" time="311.878" type="appl" />
         <tli id="T325" time="312.964" type="appl" />
         <tli id="T326" time="316.26607253120784" />
         <tli id="T327" time="317.58" type="appl" />
         <tli id="T328" time="318.505" type="appl" />
         <tli id="T329" time="319.9660655804241" />
         <tli id="T330" time="321.255" type="appl" />
         <tli id="T331" time="322.335" type="appl" />
         <tli id="T332" time="323.415" type="appl" />
         <tli id="T333" time="324.495" type="appl" />
         <tli id="T334" time="330.66604547950897" />
         <tli id="T335" time="331.972" type="appl" />
         <tli id="T336" time="332.913" type="appl" />
         <tli id="T337" time="333.855" type="appl" />
         <tli id="T338" time="334.797" type="appl" />
         <tli id="T339" time="335.738" type="appl" />
         <tli id="T340" time="339.68602853462545" />
         <tli id="T341" time="340.797" type="appl" />
         <tli id="T342" time="341.913" type="appl" />
         <tli id="T343" time="343.03" type="appl" />
         <tli id="T344" time="344.53935275053436" />
         <tli id="T345" time="345.266" type="appl" />
         <tli id="T346" time="345.982" type="appl" />
         <tli id="T347" time="346.698" type="appl" />
         <tli id="T348" time="347.414" type="appl" />
         <tli id="T349" time="348.13" type="appl" />
         <tli id="T350" time="349.008" type="appl" />
         <tli id="T351" time="349.572" type="appl" />
         <tli id="T352" time="353.61266903879266" />
         <tli id="T353" time="354.74" type="appl" />
         <tli id="T354" time="355.59" type="appl" />
         <tli id="T355" time="357.15266238858334" />
         <tli id="T356" time="358.15" type="appl" />
         <tli id="T357" time="358.955" type="appl" />
         <tli id="T358" time="361.4059877316464" />
         <tli id="T359" time="362.592" type="appl" />
         <tli id="T360" time="363.583" type="appl" />
         <tli id="T361" time="364.575" type="appl" />
         <tli id="T362" time="365.567" type="appl" />
         <tli id="T363" time="366.558" type="appl" />
         <tli id="T364" time="370.3659708994782" />
         <tli id="T365" time="371.575" type="appl" />
         <tli id="T366" time="372.563" type="appl" />
         <tli id="T367" time="373.566" type="appl" />
         <tli id="T368" time="374.569" type="appl" />
         <tli id="T369" time="375.571" type="appl" />
         <tli id="T370" time="376.574" type="appl" />
         <tli id="T371" time="377.577" type="appl" />
         <tli id="T372" time="379.7126200075886" />
         <tli id="T373" time="381.05" type="appl" />
         <tli id="T374" time="382.21" type="appl" />
         <tli id="T375" time="384.3192780202164" />
         <tli id="T376" time="385.34" type="appl" />
         <tli id="T377" time="386.12" type="appl" />
         <tli id="T378" time="386.9" type="appl" />
         <tli id="T379" time="387.68" type="appl" />
         <tli id="T380" time="390.1926003199633" />
         <tli id="T381" time="391.3325981783705" />
         <tli id="T382" time="393.0925948720518" />
         <tli id="T383" time="395.96592280756926" />
         <tli id="T885" time="397.07925404940556" />
         <tli id="T385" time="398.40591822380924" />
         <tli id="T386" time="400.11258168434864" />
         <tli id="T387" time="401.113" type="appl" />
         <tli id="T388" time="402.127" type="appl" />
         <tli id="T389" time="403.14" type="appl" />
         <tli id="T390" time="404.153" type="appl" />
         <tli id="T391" time="405.167" type="appl" />
         <tli id="T392" time="406.18" type="appl" />
         <tli id="T393" time="409.86589669516553" />
         <tli id="T394" time="411.075" type="appl" />
         <tli id="T395" time="411.85" type="appl" />
         <tli id="T396" time="412.98589083396416" />
         <tli id="T397" time="413.843" type="appl" />
         <tli id="T398" time="414.506" type="appl" />
         <tli id="T399" time="415.169" type="appl" />
         <tli id="T400" time="415.831" type="appl" />
         <tli id="T401" time="416.494" type="appl" />
         <tli id="T402" time="417.157" type="appl" />
         <tli id="T403" time="418.339214110578" />
         <tli id="T404" time="419.055" type="appl" />
         <tli id="T405" time="419.915" type="appl" />
         <tli id="T406" time="421.14587550466814" />
         <tli id="T407" time="422.35" type="appl" />
         <tli id="T408" time="423.43" type="appl" />
         <tli id="T409" time="425.0458681781665" />
         <tli id="T410" time="426.006" type="appl" />
         <tli id="T411" time="426.912" type="appl" />
         <tli id="T412" time="427.818" type="appl" />
         <tli id="T413" time="428.724" type="appl" />
         <tli id="T414" time="429.631" type="appl" />
         <tli id="T415" time="430.537" type="appl" />
         <tli id="T416" time="431.443" type="appl" />
         <tli id="T417" time="432.349" type="appl" />
         <tli id="T418" time="433.33918593172507" />
         <tli id="T419" time="434.1" type="appl" />
         <tli id="T420" time="434.74" type="appl" />
         <tli id="T421" time="435.59251503196856" />
         <tli id="T422" time="436.367" type="appl" />
         <tli id="T423" time="437.033" type="appl" />
         <tli id="T424" time="437.7791775907846" />
         <tli id="T425" time="438.472" type="appl" />
         <tli id="T426" time="439.07" type="appl" />
         <tli id="T427" time="439.668" type="appl" />
         <tli id="T428" time="440.265" type="appl" />
         <tli id="T429" time="441.035" type="appl" />
         <tli id="T430" time="441.775" type="appl" />
         <tli id="T431" time="442.49" type="appl" />
         <tli id="T432" time="443.617" type="appl" />
         <tli id="T433" time="444.563" type="appl" />
         <tli id="T434" time="445.51" type="appl" />
         <tli id="T435" time="446.457" type="appl" />
         <tli id="T436" time="447.403" type="appl" />
         <tli id="T437" time="448.35" type="appl" />
         <tli id="T438" time="449.62" type="appl" />
         <tli id="T439" time="450.74" type="appl" />
         <tli id="T440" time="451.86" type="appl" />
         <tli id="T441" time="452.98" type="appl" />
         <tli id="T442" time="454.1" type="appl" />
         <tli id="T443" time="455.22" type="appl" />
         <tli id="T444" time="456.34" type="appl" />
         <tli id="T445" time="457.46" type="appl" />
         <tli id="T446" time="459.90580269051225" />
         <tli id="T447" time="460.87" type="appl" />
         <tli id="T448" time="461.555" type="appl" />
         <tli id="T449" time="462.24" type="appl" />
         <tli id="T450" time="463.15912991216544" />
         <tli id="T451" time="464.615" type="appl" />
         <tli id="T452" time="465.82" type="appl" />
         <tli id="T453" time="466.4924569835315" />
         <tli id="T454" time="468.093" type="appl" />
         <tli id="T455" time="469.557" type="appl" />
         <tli id="T456" time="471.02" type="appl" />
         <tli id="T457" time="472.392" type="appl" />
         <tli id="T458" time="473.595" type="appl" />
         <tli id="T459" time="474.798" type="appl" />
         <tli id="T460" time="476.9924372583344" />
         <tli id="T461" time="477.84" type="appl" />
         <tli id="T462" time="478.43" type="appl" />
         <tli id="T463" time="479.02" type="appl" />
         <tli id="T464" time="479.61" type="appl" />
         <tli id="T891" time="479.95000000000005" type="intp" />
         <tli id="T465" time="480.29" type="appl" />
         <tli id="T466" time="481.11909617268554" />
         <tli id="T467" time="481.859" type="appl" />
         <tli id="T468" time="482.638" type="appl" />
         <tli id="T469" time="483.417" type="appl" />
         <tli id="T470" time="484.196" type="appl" />
         <tli id="T471" time="484.975" type="appl" />
         <tli id="T472" time="485.755" type="appl" />
         <tli id="T473" time="486.534" type="appl" />
         <tli id="T474" time="487.313" type="appl" />
         <tli id="T475" time="488.092" type="appl" />
         <tli id="T476" time="488.871" type="appl" />
         <tli id="T477" time="489.79241321238" />
         <tli id="T478" time="490.91" type="appl" />
         <tli id="T479" time="491.75" type="appl" />
         <tli id="T480" time="492.75240765175306" />
         <tli id="T481" time="494.648" type="appl" />
         <tli id="T482" time="496.525" type="appl" />
         <tli id="T483" time="498.402" type="appl" />
         <tli id="T484" time="500.61905954017675" />
         <tli id="T485" time="501.47" type="appl" />
         <tli id="T486" time="502.3" type="appl" />
         <tli id="T487" time="503.13" type="appl" />
         <tli id="T488" time="503.96" type="appl" />
         <tli id="T489" time="504.79" type="appl" />
         <tli id="T490" time="505.62" type="appl" />
         <tli id="T491" time="510.1190416935699" />
         <tli id="T492" time="511.96570489110667" />
         <tli id="T493" time="513.32" type="appl" />
         <tli id="T494" time="514.5190334277731" />
         <tli id="T495" time="515.409" type="appl" />
         <tli id="T496" time="516.323" type="appl" />
         <tli id="T497" time="517.237" type="appl" />
         <tli id="T498" time="518.151" type="appl" />
         <tli id="T499" time="519.3923576061102" />
         <tli id="T500" time="519.987" type="appl" />
         <tli id="T501" time="520.633" type="appl" />
         <tli id="T502" time="521.28" type="appl" />
         <tli id="T503" time="522.1190191504876" />
         <tli id="T504" time="522.824" type="appl" />
         <tli id="T505" time="523.468" type="appl" />
         <tli id="T506" time="524.112" type="appl" />
         <tli id="T507" time="524.756" type="appl" />
         <tli id="T508" time="525.4" type="appl" />
         <tli id="T509" time="526.044" type="appl" />
         <tli id="T510" time="526.688" type="appl" />
         <tli id="T511" time="527.332" type="appl" />
         <tli id="T512" time="527.976" type="appl" />
         <tli id="T513" time="528.62" type="appl" />
         <tli id="T514" time="529.7856714146294" />
         <tli id="T515" time="530.837" type="appl" />
         <tli id="T516" time="531.863" type="appl" />
         <tli id="T517" time="533.1856650274227" />
         <tli id="T518" time="534.217" type="appl" />
         <tli id="T519" time="535.088" type="appl" />
         <tli id="T520" time="535.96" type="appl" />
         <tli id="T521" time="536.84" type="appl" />
         <tli id="T522" time="537.65" type="appl" />
         <tli id="T523" time="538.46" type="appl" />
         <tli id="T524" time="539.27" type="appl" />
         <tli id="T525" time="540.08" type="appl" />
         <tli id="T526" time="540.855" type="appl" />
         <tli id="T527" time="541.59" type="appl" />
         <tli id="T528" time="542.9456466923825" />
         <tli id="T529" time="544.899" type="appl" />
         <tli id="T530" time="546.983" type="appl" />
         <tli id="T531" time="549.066" type="appl" />
         <tli id="T532" time="549.0856351578387" />
         <tli id="T533" time="552.055" type="appl" />
         <tli id="T534" time="553.356" type="appl" />
         <tli id="T535" time="554.412" type="appl" />
         <tli id="T536" time="555.468" type="appl" />
         <tli id="T537" time="557.1656199788299" />
         <tli id="T886" time="557.175" />
         <tli id="T539" time="557.8589999999999" type="intp" />
         <tli id="T540" time="558.543" type="appl" />
         <tli id="T541" time="559.386" type="appl" />
         <tli id="T542" time="560.229" type="appl" />
         <tli id="T543" time="561.071" type="appl" />
         <tli id="T544" time="561.914" type="appl" />
         <tli id="T545" time="562.757" type="appl" />
         <tli id="T546" time="564.1856067911268" />
         <tli id="T547" time="565.165" type="appl" />
         <tli id="T548" time="566.0" type="appl" />
         <tli id="T549" time="566.8322684857914" />
         <tli id="T550" time="567.914" type="appl" />
         <tli id="T551" time="568.798" type="appl" />
         <tli id="T552" time="569.682" type="appl" />
         <tli id="T553" time="570.566" type="appl" />
         <tli id="T554" time="571.7322592806994" />
         <tli id="T555" time="573.18" type="appl" />
         <tli id="T556" time="574.62" type="appl" />
         <tli id="T557" time="576.2922507143281" />
         <tli id="T558" time="577.205" type="appl" />
         <tli id="T559" time="577.955" type="appl" />
         <tli id="T560" time="579.7189109436924" />
         <tli id="T561" time="580.418" type="appl" />
         <tli id="T562" time="581.061" type="appl" />
         <tli id="T563" time="581.704" type="appl" />
         <tli id="T564" time="582.346" type="appl" />
         <tli id="T565" time="582.989" type="appl" />
         <tli id="T566" time="583.632" type="appl" />
         <tli id="T567" time="584.275" type="appl" />
         <tli id="T568" time="584.713" type="appl" />
         <tli id="T569" time="585.047" type="appl" />
         <tli id="T570" time="585.8988993340049" />
         <tli id="T571" time="587.175" type="appl" />
         <tli id="T572" time="587.945" type="appl" />
         <tli id="T573" time="589.181" type="appl" />
         <tli id="T574" time="590.242" type="appl" />
         <tli id="T575" time="591.303" type="appl" />
         <tli id="T576" time="592.364" type="appl" />
         <tli id="T577" time="593.426" type="appl" />
         <tli id="T578" time="594.487" type="appl" />
         <tli id="T579" time="595.548" type="appl" />
         <tli id="T580" time="596.609" type="appl" />
         <tli id="T581" time="597.8988767909227" />
         <tli id="T582" time="600.2455390491642" />
         <tli id="T583" time="600.933" type="appl" />
         <tli id="T584" time="601.597" type="appl" />
         <tli id="T585" time="602.26" type="appl" />
         <tli id="T586" time="604.4721977756564" />
         <tli id="T587" time="605.275" type="appl" />
         <tli id="T588" time="606.111" type="appl" />
         <tli id="T589" time="606.946" type="appl" />
         <tli id="T590" time="607.782" type="appl" />
         <tli id="T591" time="608.617" type="appl" />
         <tli id="T592" time="609.453" type="appl" />
         <tli id="T593" time="610.288" type="appl" />
         <tli id="T594" time="611.123" type="appl" />
         <tli id="T595" time="611.959" type="appl" />
         <tli id="T596" time="612.794" type="appl" />
         <tli id="T597" time="613.63" type="appl" />
         <tli id="T598" time="614.465" type="appl" />
         <tli id="T599" time="615.35" type="appl" />
         <tli id="T600" time="616.01" type="appl" />
         <tli id="T601" time="616.67" type="appl" />
         <tli id="T602" time="617.465" type="appl" />
         <tli id="T603" time="618.2388385803981" />
         <tli id="T604" time="618.6921710621039" />
         <tli id="T605" time="619.9988352740794" />
         <tli id="T606" time="621.9521649378999" />
         <tli id="T607" time="622.3588308406065" />
         <tli id="T608" time="625.0721590766983" />
         <tli id="T887" time="627.255" />
         <tli id="T609" time="627.2921549062282" />
         <tli id="T611" time="629.185484682764" />
         <tli id="T612" time="630.07" type="appl" />
         <tli id="T613" time="630.895" type="appl" />
         <tli id="T614" time="631.72" type="appl" />
         <tli id="T615" time="632.6188115662711" />
         <tli id="T616" time="633.2788103264016" />
         <tli id="T617" time="634.288" type="appl" />
         <tli id="T618" time="635.185" type="appl" />
         <tli id="T619" time="636.082" type="appl" />
         <tli id="T620" time="636.98" type="appl" />
         <tli id="T621" time="637.878" type="appl" />
         <tli id="T622" time="638.775" type="appl" />
         <tli id="T623" time="639.672" type="appl" />
         <tli id="T624" time="641.2854619518228" />
         <tli id="T625" time="643.09" type="appl" />
         <tli id="T626" time="645.3121210540328" />
         <tli id="T627" time="645.938" type="appl" />
         <tli id="T628" time="646.536" type="appl" />
         <tli id="T629" time="647.133" type="appl" />
         <tli id="T630" time="647.731" type="appl" />
         <tli id="T631" time="648.329" type="appl" />
         <tli id="T632" time="648.927" type="appl" />
         <tli id="T633" time="649.524" type="appl" />
         <tli id="T634" time="650.122" type="appl" />
         <tli id="T635" time="650.72" type="appl" />
         <tli id="T636" time="652.252108016617" />
         <tli id="T637" time="653.452" type="appl" />
         <tli id="T638" time="654.438" type="appl" />
         <tli id="T639" time="656.2254338856852" />
         <tli id="T640" time="658.139" type="appl" />
         <tli id="T641" time="659.898" type="appl" />
         <tli id="T642" time="661.657" type="appl" />
         <tli id="T643" time="663.416" type="appl" />
         <tli id="T644" time="665.175" type="appl" />
         <tli id="T645" time="666.31" type="appl" />
         <tli id="T646" time="667.17" type="appl" />
         <tli id="T647" time="668.1387448387475" />
         <tli id="T648" time="669.163" type="appl" />
         <tli id="T649" time="670.156" type="appl" />
         <tli id="T650" time="671.148" type="appl" />
         <tli id="T651" time="672.141" type="appl" />
         <tli id="T652" time="673.134" type="appl" />
         <tli id="T653" time="674.127" type="appl" />
         <tli id="T654" time="675.119" type="appl" />
         <tli id="T655" time="676.112" type="appl" />
         <tli id="T656" time="677.2453943977193" />
         <tli id="T657" time="677.88" type="appl" />
         <tli id="T658" time="678.565" type="appl" />
         <tli id="T659" time="679.5787233476757" />
         <tli id="T660" time="680.805" type="appl" />
         <tli id="T661" time="681.89" type="appl" />
         <tli id="T662" time="682.85" type="appl" />
         <tli id="T663" time="683.46" type="appl" />
         <tli id="T664" time="684.07" type="appl" />
         <tli id="T665" time="684.68" type="appl" />
         <tli id="T666" time="685.635" type="appl" />
         <tli id="T667" time="686.385" type="appl" />
         <tli id="T668" time="688.4187067409382" />
         <tli id="T669" time="689.309" type="appl" />
         <tli id="T670" time="690.174" type="appl" />
         <tli id="T671" time="691.038" type="appl" />
         <tli id="T672" time="691.902" type="appl" />
         <tli id="T673" time="692.767" type="appl" />
         <tli id="T674" time="693.631" type="appl" />
         <tli id="T675" time="694.496" type="appl" />
         <tli id="T676" time="695.36" type="appl" />
         <tli id="T677" time="696.21" type="appl" />
         <tli id="T678" time="696.935" type="appl" />
         <tli id="T679" time="697.9653554733305" />
         <tli id="T680" time="699.131" type="appl" />
         <tli id="T681" time="700.232" type="appl" />
         <tli id="T682" time="701.334" type="appl" />
         <tli id="T683" time="702.435" type="appl" />
         <tli id="T684" time="703.536" type="appl" />
         <tli id="T685" time="704.638" type="appl" />
         <tli id="T686" time="705.739" type="appl" />
         <tli id="T687" time="708.3386693194216" />
         <tli id="T688" time="709.28" type="appl" />
         <tli id="T689" time="710.0" type="appl" />
         <tli id="T690" time="710.72" type="appl" />
         <tli id="T691" time="711.5453299620757" />
         <tli id="T888" time="712.3626649810378" type="intp" />
         <tli id="T692" time="713.18" type="appl" />
         <tli id="T693" time="714.74" type="appl" />
         <tli id="T694" time="716.3" type="appl" />
         <tli id="T695" time="717.86" type="appl" />
         <tli id="T696" time="719.42" type="appl" />
         <tli id="T697" time="720.165" type="appl" />
         <tli id="T698" time="720.9386456491851" />
         <tli id="T699" time="721.8786438833104" />
         <tli id="T700" time="722.719" type="appl" />
         <tli id="T701" time="723.487" type="appl" />
         <tli id="T702" time="724.256" type="appl" />
         <tli id="T703" time="725.024" type="appl" />
         <tli id="T704" time="725.793" type="appl" />
         <tli id="T705" time="726.561" type="appl" />
         <tli id="T706" time="727.33" type="appl" />
         <tli id="T707" time="728.105" type="appl" />
         <tli id="T708" time="728.705" type="appl" />
         <tli id="T709" time="729.625295997165" />
         <tli id="T710" time="730.397" type="appl" />
         <tli id="T711" time="731.043" type="appl" />
         <tli id="T712" time="731.69" type="appl" />
         <tli id="T713" time="732.337" type="appl" />
         <tli id="T714" time="732.983" type="appl" />
         <tli id="T715" time="733.63" type="appl" />
         <tli id="T716" time="734.277" type="appl" />
         <tli id="T717" time="734.923" type="appl" />
         <tli id="T718" time="735.57" type="appl" />
         <tli id="T719" time="736.217" type="appl" />
         <tli id="T720" time="736.863" type="appl" />
         <tli id="T721" time="737.7652807054408" />
         <tli id="T722" time="738.789" type="appl" />
         <tli id="T723" time="739.834" type="appl" />
         <tli id="T724" time="740.878" type="appl" />
         <tli id="T725" time="741.922" type="appl" />
         <tli id="T726" time="742.966" type="appl" />
         <tli id="T727" time="744.011" type="appl" />
         <tli id="T728" time="745.4852662027245" />
         <tli id="T729" time="746.57" type="appl" />
         <tli id="T730" time="749.1319260187988" />
         <tli id="T731" time="750.055" type="appl" />
         <tli id="T732" time="751.21" type="appl" />
         <tli id="T733" time="752.365" type="appl" />
         <tli id="T734" time="753.52" type="appl" />
         <tli id="T735" time="754.675" type="appl" />
         <tli id="T736" time="755.83" type="appl" />
         <tli id="T737" time="756.536" type="appl" />
         <tli id="T738" time="757.243" type="appl" />
         <tli id="T739" time="757.949" type="appl" />
         <tli id="T740" time="758.656" type="appl" />
         <tli id="T741" time="759.362" type="appl" />
         <tli id="T742" time="760.069" type="appl" />
         <tli id="T743" time="761.0719035884318" />
         <tli id="T744" time="761.722" type="appl" />
         <tli id="T745" time="762.45" type="appl" />
         <tli id="T746" time="763.178" type="appl" />
         <tli id="T747" time="763.8783566495802" />
         <tli id="T748" time="764.771" type="appl" />
         <tli id="T749" time="765.636" type="appl" />
         <tli id="T750" time="766.502" type="appl" />
         <tli id="T751" time="767.368" type="appl" />
         <tli id="T752" time="768.233" type="appl" />
         <tli id="T753" time="769.099" type="appl" />
         <tli id="T754" time="769.964" type="appl" />
         <tli id="T755" time="770.83" type="appl" />
         <tli id="T756" time="771.822" type="appl" />
         <tli id="T757" time="772.522" type="appl" />
         <tli id="T758" time="773.222" type="appl" />
         <tli id="T759" time="774.454" type="appl" />
         <tli id="T760" time="775.362" type="appl" />
         <tli id="T761" time="776.27" type="appl" />
         <tli id="T762" time="778.0718716523986" />
         <tli id="T763" time="778.567" type="appl" />
         <tli id="T764" time="779.183" type="appl" />
         <tli id="T765" time="779.8" type="appl" />
         <tli id="T766" time="780.417" type="appl" />
         <tli id="T767" time="781.033" type="appl" />
         <tli id="T768" time="781.8851978220414" />
         <tli id="T769" time="783.104" type="appl" />
         <tli id="T770" time="784.46" type="appl" />
         <tli id="T771" time="785.817" type="appl" />
         <tli id="T772" time="786.957" type="appl" />
         <tli id="T773" time="787.897" type="appl" />
         <tli id="T774" time="788.837" type="appl" />
         <tli id="T775" time="789.584" type="appl" />
         <tli id="T776" time="790.196" type="appl" />
         <tli id="T777" time="790.808" type="appl" />
         <tli id="T778" time="791.42" type="appl" />
         <tli id="T779" time="792.031" type="appl" />
         <tli id="T780" time="792.643" type="appl" />
         <tli id="T781" time="793.255" type="appl" />
         <tli id="T782" time="794.2251746402384" />
         <tli id="T783" time="795.426" type="appl" />
         <tli id="T784" time="796.513" type="appl" />
         <tli id="T785" time="797.6" type="appl" />
         <tli id="T786" time="799.8518307367042" />
         <tli id="T787" time="800.888" type="appl" />
         <tli id="T788" time="801.889" type="appl" />
         <tli id="T789" time="802.89" type="appl" />
         <tli id="T790" time="803.9166668505286" />
         <tli id="T791" time="804.714" type="appl" />
         <tli id="T792" time="805.534" type="appl" />
         <tli id="T793" time="807.0984837898538" />
         <tli id="T794" time="807.926" type="appl" />
         <tli id="T795" time="808.773" type="appl" />
         <tli id="T796" time="809.62" type="appl" />
         <tli id="T797" time="810.966" type="appl" />
         <tli id="T798" time="812.311" type="appl" />
         <tli id="T799" time="813.657" type="appl" />
         <tli id="T800" time="815.002" type="appl" />
         <tli id="T801" time="816.348" type="appl" />
         <tli id="T802" time="817.693" type="appl" />
         <tli id="T803" time="819.039" type="appl" />
         <tli id="T804" time="820.384" type="appl" />
         <tli id="T805" time="821.7633520738578" />
         <tli id="T806" time="822.657" type="appl" />
         <tli id="T807" time="823.583" type="appl" />
         <tli id="T808" time="824.51" type="appl" />
         <tli id="T809" time="825.437" type="appl" />
         <tli id="T810" time="826.363" type="appl" />
         <tli id="T811" time="827.29" type="appl" />
         <tli id="T812" time="828.4451103548819" />
         <tli id="T813" time="829.37" type="appl" />
         <tli id="T814" time="830.128" type="appl" />
         <tli id="T815" time="830.88" type="appl" />
         <tli id="T816" time="831.632" type="appl" />
         <tli id="T817" time="832.4851027653776" />
         <tli id="T818" time="833.74" type="appl" />
         <tli id="T819" time="834.865" type="appl" />
         <tli id="T889" time="835.4725000000001" type="intp" />
         <tli id="T820" time="836.08" type="appl" />
         <tli id="T821" time="839.6984225478136" />
         <tli id="T822" time="840.819" type="appl" />
         <tli id="T823" time="841.833" type="appl" />
         <tli id="T824" time="842.847" type="appl" />
         <tli id="T825" time="843.861" type="appl" />
         <tli id="T826" time="844.875" type="appl" />
         <tli id="T827" time="845.89" type="appl" />
         <tli id="T828" time="846.904" type="appl" />
         <tli id="T829" time="847.918" type="appl" />
         <tli id="T830" time="848.932" type="appl" />
         <tli id="T831" time="849.946" type="appl" />
         <tli id="T832" time="850.96" type="appl" />
         <tli id="T833" time="851.533" type="appl" />
         <tli id="T834" time="852.097" type="appl" />
         <tli id="T835" time="852.66" type="appl" />
         <tli id="T836" time="854.095" type="appl" />
         <tli id="T837" time="854.644" type="appl" />
         <tli id="T838" time="855.193" type="appl" />
         <tli id="T839" time="855.742" type="appl" />
         <tli id="T840" time="856.291" type="appl" />
         <tli id="T841" time="856.839" type="appl" />
         <tli id="T842" time="857.388" type="appl" />
         <tli id="T843" time="857.937" type="appl" />
         <tli id="T844" time="858.486" type="appl" />
         <tli id="T845" time="864.5850424626323" />
         <tli id="T846" time="865.45" type="appl" />
         <tli id="T847" time="866.2" type="appl" />
         <tli id="T848" time="867.1383709992987" />
         <tli id="T849" time="867.692" type="appl" />
         <tli id="T850" time="868.435" type="appl" />
         <tli id="T851" time="869.178" type="appl" />
         <tli id="T852" time="870.1983652508126" />
         <tli id="T853" time="871.5650293500727" />
         <tli id="T854" time="872.804" type="appl" />
         <tli id="T855" time="873.808" type="appl" />
         <tli id="T856" time="874.812" type="appl" />
         <tli id="T857" time="875.816" type="appl" />
         <tli id="T858" time="876.82" type="appl" />
         <tli id="T859" time="877.89" type="appl" />
         <tli id="T860" time="878.774" type="appl" />
         <tli id="T861" time="879.657" type="appl" />
         <tli id="T862" time="880.593" type="appl" />
         <tli id="T863" time="881.426" type="appl" />
         <tli id="T864" time="882.258" type="appl" />
         <tli id="T865" time="883.091" type="appl" />
         <tli id="T866" time="884.5516716201147" />
         <tli id="T867" time="885.238" type="appl" />
         <tli id="T868" time="886.01" type="appl" />
         <tli id="T869" time="886.782" type="appl" />
         <tli id="T870" time="887.555" type="appl" />
         <tli id="T871" time="888.328" type="appl" />
         <tli id="T872" time="889.1" type="appl" />
         <tli id="T873" time="889.872" type="appl" />
         <tli id="T874" time="890.645" type="appl" />
         <tli id="T875" time="892.027" type="appl" />
         <tli id="T876" time="893.243" type="appl" />
         <tli id="T877" time="894.46" type="appl" />
         <tli id="T878" time="895.22" type="appl" />
         <tli id="T879" time="895.95" type="appl" />
         <tli id="T880" time="896.68" type="appl" />
         <tli id="T881" time="897.41" type="appl" />
         <tli id="T882" time="898.14" type="appl" />
         <tli id="T890" time="898.278" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T40" start="T39">
            <tli id="T39.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T751" start="T750">
            <tli id="T750.tx-PKZ.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T364" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Dĭgəttə</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">dĭ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">nʼizeŋ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">aktʼinə</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">kambiʔi</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Šobiʔi</ts>
                  <nts id="Seg_23" n="HIAT:ip">,</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">šobiʔi</ts>
                  <nts id="Seg_27" n="HIAT:ip">,</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">dĭgəttə</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_32" n="HIAT:ip">(</nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">aʔtʼi=</ts>
                  <nts id="Seg_35" n="HIAT:ip">)</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">aʔtʼigən</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">toʔndə</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">amnobiʔi</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_48" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">Kuliaʔi:</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">šonəga</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">kăldun</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">onʼiʔ</ts>
                  <nts id="Seg_60" n="HIAT:ip">,</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">ine</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">kömə</ts>
                  <nts id="Seg_67" n="HIAT:ip">.</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_70" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">Dĭgəttə</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">dĭ</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">bar</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T21">tăŋ</ts>
                  <nts id="Seg_82" n="HIAT:ip">…</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_85" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">Dĭgəttə</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">onʼiʔ</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">свистнул</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">tăŋ</ts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_100" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_102" n="HIAT:w" s="T27">Inet</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">bar</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_107" n="HIAT:ip">(</nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">s-</ts>
                  <nts id="Seg_110" n="HIAT:ip">)</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">kălʼenkanə</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_115" n="HIAT:ip">(</nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">уп-</ts>
                  <nts id="Seg_118" n="HIAT:ip">)</nts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_122" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">Dĭ</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">ine</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">saʔməluʔbi</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">kălʼenkaʔinə</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_137" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_139" n="HIAT:w" s="T36">Dĭgəttə</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">dĭ</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">găldun</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_147" n="HIAT:ip">(</nts>
                  <ts e="T39.tx-PKZ.1" id="Seg_149" n="HIAT:w" s="T39">măndəbi</ts>
                  <nts id="Seg_150" n="HIAT:ip">)</nts>
                  <ts e="T40" id="Seg_152" n="HIAT:w" s="T39.tx-PKZ.1">:</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_155" n="HIAT:w" s="T40">Dĭ</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_158" n="HIAT:w" s="T41">dĭn</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_160" n="HIAT:ip">(</nts>
                  <ts e="T43" id="Seg_162" n="HIAT:w" s="T42">e-</ts>
                  <nts id="Seg_163" n="HIAT:ip">)</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_166" n="HIAT:w" s="T43">enəidənə</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_169" n="HIAT:w" s="T44">amnolaʔbə</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_173" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_175" n="HIAT:w" s="T45">Ugaːndə</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_178" n="HIAT:w" s="T46">pimniem</ts>
                  <nts id="Seg_179" n="HIAT:ip">!</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_182" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_184" n="HIAT:w" s="T47">Dĭgəttə</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_187" n="HIAT:w" s="T48">kambi</ts>
                  <nts id="Seg_188" n="HIAT:ip">.</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_191" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_193" n="HIAT:w" s="T49">Amnolaʔbəʔjə</ts>
                  <nts id="Seg_194" n="HIAT:ip">,</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_197" n="HIAT:w" s="T50">amnolaʔbəʔjə</ts>
                  <nts id="Seg_198" n="HIAT:ip">,</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_201" n="HIAT:w" s="T51">bazoʔ</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_204" n="HIAT:w" s="T52">šonəga</ts>
                  <nts id="Seg_205" n="HIAT:ip">.</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_208" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_210" n="HIAT:w" s="T53">Sagər</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_213" n="HIAT:w" s="T54">inetsiʔ</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_215" n="HIAT:ip">(</nts>
                  <ts e="T56" id="Seg_217" n="HIAT:w" s="T55">oʔ-</ts>
                  <nts id="Seg_218" n="HIAT:ip">)</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_220" n="HIAT:ip">(</nts>
                  <ts e="T57" id="Seg_222" n="HIAT:w" s="T56">baška</ts>
                  <nts id="Seg_223" n="HIAT:ip">)</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_225" n="HIAT:ip">(</nts>
                  <ts e="T58" id="Seg_227" n="HIAT:w" s="T57">šide=</ts>
                  <nts id="Seg_228" n="HIAT:ip">)</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_231" n="HIAT:w" s="T58">šidegit</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_234" n="HIAT:w" s="T59">găldun</ts>
                  <nts id="Seg_235" n="HIAT:ip">.</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_238" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_240" n="HIAT:w" s="T60">Dĭgəttə</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_243" n="HIAT:w" s="T61">dĭzeŋ</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_246" n="HIAT:w" s="T62">bazoʔ</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_249" n="HIAT:w" s="T63">свистнул</ts>
                  <nts id="Seg_250" n="HIAT:ip">,</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_253" n="HIAT:w" s="T64">gəttə</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_256" n="HIAT:w" s="T65">dĭ</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_259" n="HIAT:w" s="T66">saʔməluʔbi</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_262" n="HIAT:w" s="T67">kălʼenkaʔinə</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_266" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_268" n="HIAT:w" s="T68">Tĭ</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_271" n="HIAT:w" s="T69">tože</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_274" n="HIAT:w" s="T70">măndə:</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_277" n="HIAT:w" s="T71">Enəidənə</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_280" n="HIAT:w" s="T72">ugandə</ts>
                  <nts id="Seg_281" n="HIAT:ip">,</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_284" n="HIAT:w" s="T73">kuštü</ts>
                  <nts id="Seg_285" n="HIAT:ip">.</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_288" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_290" n="HIAT:w" s="T74">Ugandə</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_293" n="HIAT:w" s="T75">măn</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_296" n="HIAT:w" s="T76">pimniem</ts>
                  <nts id="Seg_297" n="HIAT:ip">.</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_300" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_302" n="HIAT:w" s="T77">Dĭgəttə</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_304" n="HIAT:ip">(</nts>
                  <ts e="T79" id="Seg_306" n="HIAT:w" s="T78">u-</ts>
                  <nts id="Seg_307" n="HIAT:ip">)</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_309" n="HIAT:ip">(</nts>
                  <ts e="T80" id="Seg_311" n="HIAT:w" s="T79">ine=</ts>
                  <nts id="Seg_312" n="HIAT:ip">)</nts>
                  <nts id="Seg_313" n="HIAT:ip">,</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_316" n="HIAT:w" s="T80">inet</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_319" n="HIAT:w" s="T81">uʔbdəbi</ts>
                  <nts id="Seg_320" n="HIAT:ip">,</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_323" n="HIAT:w" s="T82">kambi</ts>
                  <nts id="Seg_324" n="HIAT:ip">.</nts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_327" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_329" n="HIAT:w" s="T83">Gəttə</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_332" n="HIAT:w" s="T84">amnolaʔbəʔjə</ts>
                  <nts id="Seg_333" n="HIAT:ip">,</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_336" n="HIAT:w" s="T85">amnolaʔbəʔjə</ts>
                  <nts id="Seg_337" n="HIAT:ip">.</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_340" n="HIAT:u" s="T86">
                  <nts id="Seg_341" n="HIAT:ip">(</nts>
                  <ts e="T87" id="Seg_343" n="HIAT:w" s="T86">Na-</ts>
                  <nts id="Seg_344" n="HIAT:ip">)</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_347" n="HIAT:w" s="T87">Nagurgət</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_350" n="HIAT:w" s="T88">ine</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_353" n="HIAT:w" s="T89">šonəga</ts>
                  <nts id="Seg_354" n="HIAT:ip">,</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_357" n="HIAT:w" s="T90">dö</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_360" n="HIAT:w" s="T91">ine</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_363" n="HIAT:w" s="T92">sĭri</ts>
                  <nts id="Seg_364" n="HIAT:ip">.</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_367" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_369" n="HIAT:w" s="T93">Dĭzeŋ</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_372" n="HIAT:w" s="T94">bazoʔ</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_375" n="HIAT:w" s="T95">свистнули</ts>
                  <nts id="Seg_376" n="HIAT:ip">.</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_379" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_381" n="HIAT:w" s="T96">Dĭgəttə</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_384" n="HIAT:w" s="T97">dĭ</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_387" n="HIAT:w" s="T98">ine</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_390" n="HIAT:w" s="T99">nuldəbi</ts>
                  <nts id="Seg_391" n="HIAT:ip">,</nts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_394" n="HIAT:w" s="T100">nulaʔbə</ts>
                  <nts id="Seg_395" n="HIAT:ip">.</nts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_398" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_400" n="HIAT:w" s="T101">Dĭ</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_403" n="HIAT:w" s="T102">kăldun</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_405" n="HIAT:ip">(</nts>
                  <ts e="T104" id="Seg_407" n="HIAT:w" s="T103">s-</ts>
                  <nts id="Seg_408" n="HIAT:ip">)</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_411" n="HIAT:w" s="T104">kambi</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_414" n="HIAT:w" s="T105">dĭzeŋdə</ts>
                  <nts id="Seg_415" n="HIAT:ip">.</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_418" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_420" n="HIAT:w" s="T106">Girgit</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_423" n="HIAT:w" s="T107">esseŋ</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_426" n="HIAT:w" s="T108">amnolaʔbəʔjə</ts>
                  <nts id="Seg_427" n="HIAT:ip">?</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_430" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_432" n="HIAT:w" s="T109">Da</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_435" n="HIAT:w" s="T110">miʔ</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_438" n="HIAT:w" s="T111">ot</ts>
                  <nts id="Seg_439" n="HIAT:ip">,</nts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_442" n="HIAT:w" s="T112">miʔnʼibeʔ</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_445" n="HIAT:w" s="T113">bar</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_448" n="HIAT:w" s="T114">bünə</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_451" n="HIAT:w" s="T115">barəʔluʔpiʔi</ts>
                  <nts id="Seg_452" n="HIAT:ip">.</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_455" n="HIAT:u" s="T116">
                  <nts id="Seg_456" n="HIAT:ip">(</nts>
                  <ts e="T117" id="Seg_458" n="HIAT:w" s="T116">Măn=</ts>
                  <nts id="Seg_459" n="HIAT:ip">)</nts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_462" n="HIAT:w" s="T117">Miʔ</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_465" n="HIAT:w" s="T118">döbər</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_468" n="HIAT:w" s="T119">šobibaʔ</ts>
                  <nts id="Seg_469" n="HIAT:ip">.</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_472" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_474" n="HIAT:w" s="T120">Dĭgəttə:</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_477" n="HIAT:w" s="T121">No</ts>
                  <nts id="Seg_478" n="HIAT:ip">,</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_481" n="HIAT:w" s="T122">amnogaʔ</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_484" n="HIAT:w" s="T123">dön</ts>
                  <nts id="Seg_485" n="HIAT:ip">.</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_488" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_490" n="HIAT:w" s="T124">Dĭ</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_493" n="HIAT:w" s="T125">dĭʔnə</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_495" n="HIAT:ip">(</nts>
                  <ts e="T127" id="Seg_497" n="HIAT:w" s="T126">abiʔi</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_500" n="HIAT:w" s="T127">=</ts>
                  <nts id="Seg_501" n="HIAT:ip">)</nts>
                  <nts id="Seg_502" n="HIAT:ip">…</nts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_505" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_507" n="HIAT:w" s="T128">Abi</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_510" n="HIAT:w" s="T129">maʔ</ts>
                  <nts id="Seg_511" n="HIAT:ip">,</nts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_514" n="HIAT:w" s="T130">ipek</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_517" n="HIAT:w" s="T131">mĭbi</ts>
                  <nts id="Seg_518" n="HIAT:ip">,</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_521" n="HIAT:w" s="T132">uja</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_524" n="HIAT:w" s="T133">mĭbi</ts>
                  <nts id="Seg_525" n="HIAT:ip">.</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_528" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_530" n="HIAT:w" s="T134">Amnogaʔ</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_533" n="HIAT:w" s="T135">dön</ts>
                  <nts id="Seg_534" n="HIAT:ip">,</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_537" n="HIAT:w" s="T136">măn</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_540" n="HIAT:w" s="T137">parlam</ts>
                  <nts id="Seg_541" n="HIAT:ip">,</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_544" n="HIAT:w" s="T138">dʼili</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_547" n="HIAT:w" s="T139">molam</ts>
                  <nts id="Seg_548" n="HIAT:ip">,</nts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_551" n="HIAT:w" s="T140">šiʔnʼileʔ</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_554" n="HIAT:w" s="T141">ilim</ts>
                  <nts id="Seg_555" n="HIAT:ip">.</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_558" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_560" n="HIAT:w" s="T142">Dĭgəttə</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_563" n="HIAT:w" s="T143">dĭzeŋ</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_566" n="HIAT:w" s="T144">amnolaʔpiʔi</ts>
                  <nts id="Seg_567" n="HIAT:ip">,</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_570" n="HIAT:w" s="T145">amnolaʔpi</ts>
                  <nts id="Seg_571" n="HIAT:ip">.</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_574" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_576" n="HIAT:w" s="T146">Nagur</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_579" n="HIAT:w" s="T147">dʼala</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_582" n="HIAT:w" s="T148">onʼiʔ</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_585" n="HIAT:w" s="T149">ine</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_588" n="HIAT:w" s="T150">šonəga</ts>
                  <nts id="Seg_589" n="HIAT:ip">,</nts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_592" n="HIAT:w" s="T151">kömə</ts>
                  <nts id="Seg_593" n="HIAT:ip">.</nts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_596" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_598" n="HIAT:w" s="T152">Dĭgəttə</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_601" n="HIAT:w" s="T153">naga</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_604" n="HIAT:w" s="T154">dĭn</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_607" n="HIAT:w" s="T155">kuza</ts>
                  <nts id="Seg_608" n="HIAT:ip">.</nts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_611" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_613" n="HIAT:w" s="T156">Dĭgəttə</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_616" n="HIAT:w" s="T157">sagər</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_619" n="HIAT:w" s="T158">ine</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_622" n="HIAT:w" s="T159">šonəga</ts>
                  <nts id="Seg_623" n="HIAT:ip">.</nts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_626" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_628" n="HIAT:w" s="T160">Kuza</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_631" n="HIAT:w" s="T161">tože</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_634" n="HIAT:w" s="T162">naga</ts>
                  <nts id="Seg_635" n="HIAT:ip">.</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_638" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_640" n="HIAT:w" s="T163">Amnolaʔbəʔi</ts>
                  <nts id="Seg_641" n="HIAT:ip">,</nts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_644" n="HIAT:w" s="T164">amnolaʔbəʔi</ts>
                  <nts id="Seg_645" n="HIAT:ip">,</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_648" n="HIAT:w" s="T165">dĭgəttə</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_651" n="HIAT:w" s="T166">sĭre</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_654" n="HIAT:w" s="T167">ine</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_657" n="HIAT:w" s="T168">šonəga</ts>
                  <nts id="Seg_658" n="HIAT:ip">.</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_661" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_663" n="HIAT:w" s="T169">I</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_666" n="HIAT:w" s="T170">kuza</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_669" n="HIAT:w" s="T171">amnolaʔbə</ts>
                  <nts id="Seg_670" n="HIAT:ip">.</nts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_673" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_675" n="HIAT:w" s="T172">No</ts>
                  <nts id="Seg_676" n="HIAT:ip">,</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_679" n="HIAT:w" s="T173">kanžəbaʔ</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_682" n="HIAT:w" s="T174">mănziʔ</ts>
                  <nts id="Seg_683" n="HIAT:ip">,</nts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_686" n="HIAT:w" s="T175">maʔnəl</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_689" n="HIAT:w" s="T176">možet</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_692" n="HIAT:w" s="T177">kallal</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_695" n="HIAT:w" s="T178">alʼi</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_698" n="HIAT:w" s="T179">măna</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_701" n="HIAT:w" s="T180">amnolal</ts>
                  <nts id="Seg_702" n="HIAT:ip">.</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_705" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_707" n="HIAT:w" s="T181">Šobiʔi</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_710" n="HIAT:w" s="T182">dĭ</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_713" n="HIAT:w" s="T183">kuzanə</ts>
                  <nts id="Seg_714" n="HIAT:ip">.</nts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_717" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_719" n="HIAT:w" s="T184">Onʼiʔ</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_722" n="HIAT:w" s="T185">kambi</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_725" n="HIAT:w" s="T186">maːʔndə</ts>
                  <nts id="Seg_726" n="HIAT:ip">.</nts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_729" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_731" n="HIAT:w" s="T187">Šobi</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_734" n="HIAT:w" s="T188">maːʔndə</ts>
                  <nts id="Seg_735" n="HIAT:ip">,</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_738" n="HIAT:w" s="T189">naga</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_740" n="HIAT:ip">(</nts>
                  <ts e="T191" id="Seg_742" n="HIAT:w" s="T190">t-</ts>
                  <nts id="Seg_743" n="HIAT:ip">)</nts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_746" n="HIAT:w" s="T191">il</ts>
                  <nts id="Seg_747" n="HIAT:ip">,</nts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_750" n="HIAT:w" s="T192">šindidə</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_753" n="HIAT:w" s="T193">naga</ts>
                  <nts id="Seg_754" n="HIAT:ip">,</nts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_756" n="HIAT:ip">(</nts>
                  <ts e="T195" id="Seg_758" n="HIAT:w" s="T194">onʼiʔ=</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_761" n="HIAT:w" s="T195">onʼiʔ</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_764" n="HIAT:w" s="T196">amnolaʔ-</ts>
                  <nts id="Seg_765" n="HIAT:ip">)</nts>
                  <nts id="Seg_766" n="HIAT:ip">,</nts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_769" n="HIAT:w" s="T197">onʼiʔ</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_772" n="HIAT:w" s="T198">maʔ</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_775" n="HIAT:w" s="T199">nuga</ts>
                  <nts id="Seg_776" n="HIAT:ip">.</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_779" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_781" n="HIAT:w" s="T200">Dĭ</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_784" n="HIAT:w" s="T201">šobi</ts>
                  <nts id="Seg_785" n="HIAT:ip">.</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_788" n="HIAT:u" s="T202">
                  <nts id="Seg_789" n="HIAT:ip">(</nts>
                  <ts e="T203" id="Seg_791" n="HIAT:w" s="T202">Ĭm-</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_794" n="HIAT:w" s="T203">gib-</ts>
                  <nts id="Seg_795" n="HIAT:ip">)</nts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_798" n="HIAT:w" s="T204">Giraːmbi</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_801" n="HIAT:w" s="T205">bar</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_804" n="HIAT:w" s="T206">il</ts>
                  <nts id="Seg_805" n="HIAT:ip">?</nts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_808" n="HIAT:u" s="T207">
                  <nts id="Seg_809" n="HIAT:ip">(</nts>
                  <ts e="T208" id="Seg_811" n="HIAT:w" s="T207">Da</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_814" n="HIAT:w" s="T208">miʔ</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_817" n="HIAT:w" s="T209">koʔbdobaʔ</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_820" n="HIAT:w" s="T210">bar=</ts>
                  <nts id="Seg_821" n="HIAT:ip">)</nts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_824" n="HIAT:w" s="T211">Šide</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_827" n="HIAT:w" s="T212">nʼi</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_830" n="HIAT:w" s="T213">ibiʔi</ts>
                  <nts id="Seg_831" n="HIAT:ip">.</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_834" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_836" n="HIAT:w" s="T214">Dĭzeŋ</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_839" n="HIAT:w" s="T215">bünə</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_842" n="HIAT:w" s="T216">öʔluʔpiʔi</ts>
                  <nts id="Seg_843" n="HIAT:ip">.</nts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_846" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_848" n="HIAT:w" s="T217">A</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_851" n="HIAT:w" s="T218">miʔ</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_854" n="HIAT:w" s="T219">koʔbdobaʔ</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_857" n="HIAT:w" s="T220">bar</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_860" n="HIAT:w" s="T221">iləm</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_863" n="HIAT:w" s="T222">dʼăgarluʔpi</ts>
                  <nts id="Seg_864" n="HIAT:ip">.</nts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_867" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_869" n="HIAT:w" s="T223">A</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_872" n="HIAT:w" s="T224">gijen</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_875" n="HIAT:w" s="T225">dĭ</ts>
                  <nts id="Seg_876" n="HIAT:ip">?</nts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_879" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_881" n="HIAT:w" s="T226">Gibərdə</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_884" n="HIAT:w" s="T227">kalla</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_887" n="HIAT:w" s="T384">dʼürbi</ts>
                  <nts id="Seg_888" n="HIAT:ip">,</nts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_891" n="HIAT:w" s="T228">ej</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_894" n="HIAT:w" s="T229">tĭmnebeʔ</ts>
                  <nts id="Seg_895" n="HIAT:ip">.</nts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_898" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_900" n="HIAT:w" s="T230">Dĭgəttə</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_903" n="HIAT:w" s="T231">dĭ</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_906" n="HIAT:w" s="T232">ine</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_909" n="HIAT:w" s="T233">ibi</ts>
                  <nts id="Seg_910" n="HIAT:ip">.</nts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_913" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_915" n="HIAT:w" s="T234">Kambi</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_918" n="HIAT:w" s="T235">kuzittə</ts>
                  <nts id="Seg_919" n="HIAT:ip">,</nts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_921" n="HIAT:ip">(</nts>
                  <ts e="T237" id="Seg_923" n="HIAT:w" s="T236">ku-</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_926" n="HIAT:w" s="T237">ku-</ts>
                  <nts id="Seg_927" n="HIAT:ip">)</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_930" n="HIAT:w" s="T238">kulaːmbi</ts>
                  <nts id="Seg_931" n="HIAT:ip">,</nts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_934" n="HIAT:w" s="T239">kubi</ts>
                  <nts id="Seg_935" n="HIAT:ip">,</nts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_938" n="HIAT:w" s="T240">kubiʔi</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_941" n="HIAT:w" s="T241">dĭn</ts>
                  <nts id="Seg_942" n="HIAT:ip">.</nts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_945" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_947" n="HIAT:w" s="T242">Dĭgəttə</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_950" n="HIAT:w" s="T243">davaj</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_953" n="HIAT:w" s="T244">dĭziʔ</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_956" n="HIAT:w" s="T245">bar</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_959" n="HIAT:w" s="T246">dʼabrozittə</ts>
                  <nts id="Seg_960" n="HIAT:ip">.</nts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_963" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_965" n="HIAT:w" s="T247">Dĭ</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_968" n="HIAT:w" s="T248">xatʼel</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_971" n="HIAT:w" s="T249">dĭm</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_973" n="HIAT:ip">(</nts>
                  <ts e="T251" id="Seg_975" n="HIAT:w" s="T250">bazə-</ts>
                  <nts id="Seg_976" n="HIAT:ip">)</nts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_979" n="HIAT:w" s="T251">bădəsʼtə</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_982" n="HIAT:w" s="T252">dagajziʔ</ts>
                  <nts id="Seg_983" n="HIAT:ip">.</nts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_986" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_988" n="HIAT:w" s="T253">Dĭ</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_991" n="HIAT:w" s="T254">bar</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_994" n="HIAT:w" s="T255">măndə:</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_997" n="HIAT:w" s="T256">Iʔ</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1000" n="HIAT:w" s="T257">bădaʔ</ts>
                  <nts id="Seg_1001" n="HIAT:ip">,</nts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1004" n="HIAT:w" s="T258">măn</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1007" n="HIAT:w" s="T259">tăn</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1010" n="HIAT:w" s="T260">kagam</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1013" n="HIAT:w" s="T261">igem</ts>
                  <nts id="Seg_1014" n="HIAT:ip">.</nts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_1017" n="HIAT:u" s="T262">
                  <nts id="Seg_1018" n="HIAT:ip">(</nts>
                  <ts e="T263" id="Seg_1020" n="HIAT:w" s="T262">Kurizəʔi</ts>
                  <nts id="Seg_1021" n="HIAT:ip">)</nts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1024" n="HIAT:w" s="T263">dʼăbaktərlaʔbəʔjə</ts>
                  <nts id="Seg_1025" n="HIAT:ip">.</nts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T269" id="Seg_1028" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_1030" n="HIAT:w" s="T264">Kapaluxa</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1033" n="HIAT:w" s="T265">i</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1035" n="HIAT:ip">(</nts>
                  <nts id="Seg_1036" n="HIAT:ip">(</nts>
                  <ats e="T267" id="Seg_1037" n="HIAT:non-pho" s="T266">PAUSE</ats>
                  <nts id="Seg_1038" n="HIAT:ip">)</nts>
                  <nts id="Seg_1039" n="HIAT:ip">)</nts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1042" n="HIAT:w" s="T267">kuropatka</ts>
                  <nts id="Seg_1043" n="HIAT:ip">.</nts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_1046" n="HIAT:u" s="T269">
                  <ts e="T270" id="Seg_1048" n="HIAT:w" s="T269">Kuropatka</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1051" n="HIAT:w" s="T270">mălia</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1054" n="HIAT:w" s="T271">kapaluxanə:</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1057" n="HIAT:w" s="T272">Tăn</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1060" n="HIAT:w" s="T273">ej</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1063" n="HIAT:w" s="T274">kuvas</ts>
                  <nts id="Seg_1064" n="HIAT:ip">,</nts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1067" n="HIAT:w" s="T275">girgit</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1069" n="HIAT:ip">(</nts>
                  <ts e="T277" id="Seg_1071" n="HIAT:w" s="T276">piʔmeʔi</ts>
                  <nts id="Seg_1072" n="HIAT:ip">)</nts>
                  <nts id="Seg_1073" n="HIAT:ip">,</nts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1076" n="HIAT:w" s="T277">ej</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1079" n="HIAT:w" s="T278">jakšə</ts>
                  <nts id="Seg_1080" n="HIAT:ip">.</nts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_1083" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_1085" n="HIAT:w" s="T279">A</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1088" n="HIAT:w" s="T280">măn</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1091" n="HIAT:w" s="T281">kuvas</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1094" n="HIAT:w" s="T282">igem</ts>
                  <nts id="Seg_1095" n="HIAT:ip">.</nts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_1098" n="HIAT:u" s="T283">
                  <ts e="T883" id="Seg_1100" n="HIAT:w" s="T283">Коричневый</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1103" n="HIAT:w" s="T883">inegəʔ</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1106" n="HIAT:w" s="T284">eʔbdə</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1109" n="HIAT:w" s="T285">bar</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1112" n="HIAT:w" s="T286">sajnʼiʔluʔpiеm</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1115" n="HIAT:w" s="T287">i</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1118" n="HIAT:w" s="T288">boskəndə</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1121" n="HIAT:w" s="T289">embiem</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1124" n="HIAT:w" s="T290">ulunə</ts>
                  <nts id="Seg_1125" n="HIAT:ip">.</nts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_1128" n="HIAT:u" s="T292">
                  <ts e="T293" id="Seg_1130" n="HIAT:w" s="T292">Ugaːndə</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1133" n="HIAT:w" s="T293">kuvas</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1136" n="HIAT:w" s="T294">igem</ts>
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1139" n="HIAT:w" s="T295">tüj</ts>
                  <nts id="Seg_1140" n="HIAT:ip">.</nts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1143" n="HIAT:u" s="T296">
                  <nts id="Seg_1144" n="HIAT:ip">(</nts>
                  <nts id="Seg_1145" n="HIAT:ip">(</nts>
                  <ats e="T297" id="Seg_1146" n="HIAT:non-pho" s="T296">BRK</ats>
                  <nts id="Seg_1147" n="HIAT:ip">)</nts>
                  <nts id="Seg_1148" n="HIAT:ip">)</nts>
                  <nts id="Seg_1149" n="HIAT:ip">.</nts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_1152" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1154" n="HIAT:w" s="T297">Măjagən</ts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1157" n="HIAT:w" s="T298">bar</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1160" n="HIAT:w" s="T299">aspak</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1163" n="HIAT:w" s="T300">mĭnzəlleʔbə</ts>
                  <nts id="Seg_1164" n="HIAT:ip">.</nts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_1167" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1169" n="HIAT:w" s="T301">Ĭmbi</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1172" n="HIAT:w" s="T302">dĭ</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1175" n="HIAT:w" s="T303">dĭrgit</ts>
                  <nts id="Seg_1176" n="HIAT:ip">?</nts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_1179" n="HIAT:u" s="T304">
                  <ts e="T305" id="Seg_1181" n="HIAT:w" s="T304">Murašeʔi</ts>
                  <nts id="Seg_1182" n="HIAT:ip">.</nts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T306" id="Seg_1185" n="HIAT:u" s="T305">
                  <nts id="Seg_1186" n="HIAT:ip">(</nts>
                  <nts id="Seg_1187" n="HIAT:ip">(</nts>
                  <ats e="T306" id="Seg_1188" n="HIAT:non-pho" s="T305">BRK</ats>
                  <nts id="Seg_1189" n="HIAT:ip">)</nts>
                  <nts id="Seg_1190" n="HIAT:ip">)</nts>
                  <nts id="Seg_1191" n="HIAT:ip">.</nts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_1194" n="HIAT:u" s="T306">
                  <ts e="T308" id="Seg_1196" n="HIAT:w" s="T306">Măjagən</ts>
                  <nts id="Seg_1197" n="HIAT:ip">…</nts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T309" id="Seg_1200" n="HIAT:u" s="T308">
                  <nts id="Seg_1201" n="HIAT:ip">(</nts>
                  <nts id="Seg_1202" n="HIAT:ip">(</nts>
                  <ats e="T309" id="Seg_1203" n="HIAT:non-pho" s="T308">BRK</ats>
                  <nts id="Seg_1204" n="HIAT:ip">)</nts>
                  <nts id="Seg_1205" n="HIAT:ip">)</nts>
                  <nts id="Seg_1206" n="HIAT:ip">.</nts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1209" n="HIAT:u" s="T309">
                  <ts e="T310" id="Seg_1211" n="HIAT:w" s="T309">Măjagən</ts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1214" n="HIAT:w" s="T310">süjö</ts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1217" n="HIAT:w" s="T311">edölaʔbə</ts>
                  <nts id="Seg_1218" n="HIAT:ip">.</nts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_1221" n="HIAT:u" s="T312">
                  <ts e="T313" id="Seg_1223" n="HIAT:w" s="T312">Ĭmbi</ts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1226" n="HIAT:w" s="T313">dĭrgit</ts>
                  <nts id="Seg_1227" n="HIAT:ip">?</nts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T315" id="Seg_1230" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1232" n="HIAT:w" s="T314">Nüjö</ts>
                  <nts id="Seg_1233" n="HIAT:ip">.</nts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_1236" n="HIAT:u" s="T315">
                  <nts id="Seg_1237" n="HIAT:ip">(</nts>
                  <nts id="Seg_1238" n="HIAT:ip">(</nts>
                  <ats e="T316" id="Seg_1239" n="HIAT:non-pho" s="T315">BRK</ats>
                  <nts id="Seg_1240" n="HIAT:ip">)</nts>
                  <nts id="Seg_1241" n="HIAT:ip">)</nts>
                  <nts id="Seg_1242" n="HIAT:ip">.</nts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_1245" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_1247" n="HIAT:w" s="T316">Bostə</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1249" n="HIAT:ip">(</nts>
                  <ts e="T318" id="Seg_1251" n="HIAT:w" s="T317">dʼünən-</ts>
                  <nts id="Seg_1252" n="HIAT:ip">)</nts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1255" n="HIAT:w" s="T318">dʼügən</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1258" n="HIAT:w" s="T319">amnolaʔbə</ts>
                  <nts id="Seg_1259" n="HIAT:ip">,</nts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1262" n="HIAT:w" s="T320">a</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1265" n="HIAT:w" s="T321">kut</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1268" n="HIAT:w" s="T322">baška</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1271" n="HIAT:w" s="T323">dʼünən</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1274" n="HIAT:w" s="T324">amnolaʔbə</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1277" n="HIAT:w" s="T325">serʼožkaʔizi</ts>
                  <nts id="Seg_1278" n="HIAT:ip">.</nts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1281" n="HIAT:u" s="T326">
                  <ts e="T327" id="Seg_1283" n="HIAT:w" s="T326">Ответ:</ts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1286" n="HIAT:w" s="T327">саранка</ts>
                  <nts id="Seg_1287" n="HIAT:ip">.</nts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_1290" n="HIAT:u" s="T328">
                  <nts id="Seg_1291" n="HIAT:ip">(</nts>
                  <nts id="Seg_1292" n="HIAT:ip">(</nts>
                  <ats e="T329" id="Seg_1293" n="HIAT:non-pho" s="T328">BRK</ats>
                  <nts id="Seg_1294" n="HIAT:ip">)</nts>
                  <nts id="Seg_1295" n="HIAT:ip">)</nts>
                  <nts id="Seg_1296" n="HIAT:ip">.</nts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_1299" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1301" n="HIAT:w" s="T329">Šide</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1303" n="HIAT:ip">(</nts>
                  <ts e="T331" id="Seg_1305" n="HIAT:w" s="T330">neg-</ts>
                  <nts id="Seg_1306" n="HIAT:ip">)</nts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1309" n="HIAT:w" s="T331">nezeŋ</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1312" n="HIAT:w" s="T332">iʔbolaʔbəʔjə</ts>
                  <nts id="Seg_1313" n="HIAT:ip">.</nts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T334" id="Seg_1316" n="HIAT:u" s="T333">
                  <nts id="Seg_1317" n="HIAT:ip">(</nts>
                  <nts id="Seg_1318" n="HIAT:ip">(</nts>
                  <ats e="T334" id="Seg_1319" n="HIAT:non-pho" s="T333">BRK</ats>
                  <nts id="Seg_1320" n="HIAT:ip">)</nts>
                  <nts id="Seg_1321" n="HIAT:ip">)</nts>
                  <nts id="Seg_1322" n="HIAT:ip">.</nts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T340" id="Seg_1325" n="HIAT:u" s="T334">
                  <ts e="T335" id="Seg_1327" n="HIAT:w" s="T334">Iʔbolaʔbəʔjə</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1329" n="HIAT:ip">(</nts>
                  <ts e="T336" id="Seg_1331" n="HIAT:w" s="T335">šünə-</ts>
                  <nts id="Seg_1332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1334" n="HIAT:w" s="T336">š-</ts>
                  <nts id="Seg_1335" n="HIAT:ip">)</nts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1338" n="HIAT:w" s="T337">šünə</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1341" n="HIAT:w" s="T338">tondə</ts>
                  <nts id="Seg_1342" n="HIAT:ip">,</nts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1345" n="HIAT:w" s="T339">nanəʔsəbiʔi</ts>
                  <nts id="Seg_1346" n="HIAT:ip">.</nts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T343" id="Seg_1349" n="HIAT:u" s="T340">
                  <ts e="T341" id="Seg_1351" n="HIAT:w" s="T340">Dĭ</ts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1354" n="HIAT:w" s="T341">üjüzeŋdə</ts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1357" n="HIAT:w" s="T342">lʼažkaʔi</ts>
                  <nts id="Seg_1358" n="HIAT:ip">.</nts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_1361" n="HIAT:u" s="T343">
                  <nts id="Seg_1362" n="HIAT:ip">(</nts>
                  <nts id="Seg_1363" n="HIAT:ip">(</nts>
                  <ats e="T344" id="Seg_1364" n="HIAT:non-pho" s="T343">BRK</ats>
                  <nts id="Seg_1365" n="HIAT:ip">)</nts>
                  <nts id="Seg_1366" n="HIAT:ip">)</nts>
                  <nts id="Seg_1367" n="HIAT:ip">.</nts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T349" id="Seg_1370" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1372" n="HIAT:w" s="T344">Üjüt</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1375" n="HIAT:w" s="T345">naga</ts>
                  <nts id="Seg_1376" n="HIAT:ip">,</nts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1379" n="HIAT:w" s="T346">i</ts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1382" n="HIAT:w" s="T347">udat</ts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1385" n="HIAT:w" s="T348">naga</ts>
                  <nts id="Seg_1386" n="HIAT:ip">.</nts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T352" id="Seg_1389" n="HIAT:u" s="T349">
                  <ts e="T350" id="Seg_1391" n="HIAT:w" s="T349">A</ts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1394" n="HIAT:w" s="T350">mĭnge</ts>
                  <nts id="Seg_1395" n="HIAT:ip">…</nts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T355" id="Seg_1398" n="HIAT:u" s="T352">
                  <nts id="Seg_1399" n="HIAT:ip">(</nts>
                  <ts e="T353" id="Seg_1401" n="HIAT:w" s="T352">M-</ts>
                  <nts id="Seg_1402" n="HIAT:ip">)</nts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1405" n="HIAT:w" s="T353">Măjanə</ts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1408" n="HIAT:w" s="T354">kandəga</ts>
                  <nts id="Seg_1409" n="HIAT:ip">.</nts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1412" n="HIAT:u" s="T355">
                  <ts e="T356" id="Seg_1414" n="HIAT:w" s="T355">Dĭ</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1417" n="HIAT:w" s="T356">kuja</ts>
                  <nts id="Seg_1418" n="HIAT:ip">.</nts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1421" n="HIAT:u" s="T357">
                  <nts id="Seg_1422" n="HIAT:ip">(</nts>
                  <nts id="Seg_1423" n="HIAT:ip">(</nts>
                  <ats e="T358" id="Seg_1424" n="HIAT:non-pho" s="T357">BRK</ats>
                  <nts id="Seg_1425" n="HIAT:ip">)</nts>
                  <nts id="Seg_1426" n="HIAT:ip">)</nts>
                  <nts id="Seg_1427" n="HIAT:ip">.</nts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T364" id="Seg_1430" n="HIAT:u" s="T358">
                  <ts e="T359" id="Seg_1432" n="HIAT:w" s="T358">Iʔgö</ts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1435" n="HIAT:w" s="T359">süjöʔi</ts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1438" n="HIAT:w" s="T360">uluzaŋdə</ts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1441" n="HIAT:w" s="T361">bar</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1444" n="HIAT:w" s="T362">onʼiʔ</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1447" n="HIAT:w" s="T363">onʼiʔtə</ts>
                  <nts id="Seg_1448" n="HIAT:ip">.</nts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T491" id="Seg_1450" n="sc" s="T365">
               <ts e="T372" id="Seg_1452" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_1454" n="HIAT:w" s="T365">A</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1456" n="HIAT:ip">(</nts>
                  <ts e="T367" id="Seg_1458" n="HIAT:w" s="T366">kötezeŋ-</ts>
                  <nts id="Seg_1459" n="HIAT:ip">)</nts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1462" n="HIAT:w" s="T367">kötenzeŋdə</ts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1465" n="HIAT:w" s="T368">bar</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1468" n="HIAT:w" s="T369">kuŋgeŋ</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1471" n="HIAT:w" s="T370">onʼiʔ</ts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1474" n="HIAT:w" s="T371">onʼiʔtə</ts>
                  <nts id="Seg_1475" n="HIAT:ip">.</nts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1478" n="HIAT:u" s="T372">
                  <ts e="T373" id="Seg_1480" n="HIAT:w" s="T372">Dĭ</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1483" n="HIAT:w" s="T373">maʔ</ts>
                  <nts id="Seg_1484" n="HIAT:ip">.</nts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_1487" n="HIAT:u" s="T374">
                  <nts id="Seg_1488" n="HIAT:ip">(</nts>
                  <nts id="Seg_1489" n="HIAT:ip">(</nts>
                  <ats e="T375" id="Seg_1490" n="HIAT:non-pho" s="T374">BRK</ats>
                  <nts id="Seg_1491" n="HIAT:ip">)</nts>
                  <nts id="Seg_1492" n="HIAT:ip">)</nts>
                  <nts id="Seg_1493" n="HIAT:ip">.</nts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T380" id="Seg_1496" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1498" n="HIAT:w" s="T375">Turagən</ts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1501" n="HIAT:w" s="T376">iʔbolaʔbə</ts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1503" n="HIAT:ip">(</nts>
                  <ts e="T378" id="Seg_1505" n="HIAT:w" s="T377">dʼo-</ts>
                  <nts id="Seg_1506" n="HIAT:ip">)</nts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1509" n="HIAT:w" s="T378">dʼüʔpi</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1512" n="HIAT:w" s="T379">buzo</ts>
                  <nts id="Seg_1513" n="HIAT:ip">.</nts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_1516" n="HIAT:u" s="T380">
                  <ts e="T381" id="Seg_1518" n="HIAT:w" s="T380">A</ts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1521" n="HIAT:w" s="T381">nörbəsʼtə</ts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1524" n="HIAT:w" s="T382">dĭ</ts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1526" n="HIAT:ip">(</nts>
                  <nts id="Seg_1527" n="HIAT:ip">(</nts>
                  <ats e="T885" id="Seg_1528" n="HIAT:non-pho" s="T383">PAUSE</ats>
                  <nts id="Seg_1529" n="HIAT:ip">)</nts>
                  <nts id="Seg_1530" n="HIAT:ip">)</nts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1533" n="HIAT:w" s="T885">šĭkət</ts>
                  <nts id="Seg_1534" n="HIAT:ip">.</nts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T386" id="Seg_1537" n="HIAT:u" s="T385">
                  <nts id="Seg_1538" n="HIAT:ip">(</nts>
                  <nts id="Seg_1539" n="HIAT:ip">(</nts>
                  <ats e="T386" id="Seg_1540" n="HIAT:non-pho" s="T385">BRK</ats>
                  <nts id="Seg_1541" n="HIAT:ip">)</nts>
                  <nts id="Seg_1542" n="HIAT:ip">)</nts>
                  <nts id="Seg_1543" n="HIAT:ip">.</nts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1546" n="HIAT:u" s="T386">
                  <nts id="Seg_1547" n="HIAT:ip">(</nts>
                  <ts e="T387" id="Seg_1549" n="HIAT:w" s="T386">Šire-</ts>
                  <nts id="Seg_1550" n="HIAT:ip">)</nts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1553" n="HIAT:w" s="T387">Sĭre</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1555" n="HIAT:ip">(</nts>
                  <ts e="T389" id="Seg_1557" n="HIAT:w" s="T388">pa-</ts>
                  <nts id="Seg_1558" n="HIAT:ip">)</nts>
                  <nts id="Seg_1559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1561" n="HIAT:w" s="T389">paʔinə</ts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1564" n="HIAT:w" s="T390">tarara</ts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1567" n="HIAT:w" s="T391">amnolaʔbə</ts>
                  <nts id="Seg_1568" n="HIAT:ip">.</nts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T393" id="Seg_1571" n="HIAT:u" s="T392">
                  <nts id="Seg_1572" n="HIAT:ip">(</nts>
                  <nts id="Seg_1573" n="HIAT:ip">(</nts>
                  <ats e="T393" id="Seg_1574" n="HIAT:non-pho" s="T392">BRK</ats>
                  <nts id="Seg_1575" n="HIAT:ip">)</nts>
                  <nts id="Seg_1576" n="HIAT:ip">)</nts>
                  <nts id="Seg_1577" n="HIAT:ip">.</nts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1580" n="HIAT:u" s="T393">
                  <ts e="T394" id="Seg_1582" n="HIAT:w" s="T393">Dĭ</ts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1585" n="HIAT:w" s="T394">šĭkə</ts>
                  <nts id="Seg_1586" n="HIAT:ip">.</nts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T396" id="Seg_1589" n="HIAT:u" s="T395">
                  <nts id="Seg_1590" n="HIAT:ip">(</nts>
                  <nts id="Seg_1591" n="HIAT:ip">(</nts>
                  <ats e="T396" id="Seg_1592" n="HIAT:non-pho" s="T395">BRK</ats>
                  <nts id="Seg_1593" n="HIAT:ip">)</nts>
                  <nts id="Seg_1594" n="HIAT:ip">)</nts>
                  <nts id="Seg_1595" n="HIAT:ip">.</nts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_1598" n="HIAT:u" s="T396">
                  <ts e="T397" id="Seg_1600" n="HIAT:w" s="T396">Üjüt</ts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1602" n="HIAT:ip">(</nts>
                  <ts e="T398" id="Seg_1604" n="HIAT:w" s="T397">uje-</ts>
                  <nts id="Seg_1605" n="HIAT:ip">)</nts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1608" n="HIAT:w" s="T398">udat</ts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1611" n="HIAT:w" s="T399">naga</ts>
                  <nts id="Seg_1612" n="HIAT:ip">,</nts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1615" n="HIAT:w" s="T400">a</ts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1618" n="HIAT:w" s="T401">kudajdə</ts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1621" n="HIAT:w" s="T402">üzlie</ts>
                  <nts id="Seg_1622" n="HIAT:ip">.</nts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T405" id="Seg_1625" n="HIAT:u" s="T403">
                  <ts e="T404" id="Seg_1627" n="HIAT:w" s="T403">Dĭ</ts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1630" n="HIAT:w" s="T404">зыбка</ts>
                  <nts id="Seg_1631" n="HIAT:ip">.</nts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1634" n="HIAT:u" s="T405">
                  <nts id="Seg_1635" n="HIAT:ip">(</nts>
                  <nts id="Seg_1636" n="HIAT:ip">(</nts>
                  <ats e="T406" id="Seg_1637" n="HIAT:non-pho" s="T405">BRK</ats>
                  <nts id="Seg_1638" n="HIAT:ip">)</nts>
                  <nts id="Seg_1639" n="HIAT:ip">)</nts>
                  <nts id="Seg_1640" n="HIAT:ip">.</nts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T409" id="Seg_1643" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_1645" n="HIAT:w" s="T406">Üjüt</ts>
                  <nts id="Seg_1646" n="HIAT:ip">,</nts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1649" n="HIAT:w" s="T407">udat</ts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1652" n="HIAT:w" s="T408">naga</ts>
                  <nts id="Seg_1653" n="HIAT:ip">.</nts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T418" id="Seg_1656" n="HIAT:u" s="T409">
                  <ts e="T410" id="Seg_1658" n="HIAT:w" s="T409">A</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1661" n="HIAT:w" s="T410">paʔinə</ts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1664" n="HIAT:w" s="T411">mĭllie</ts>
                  <nts id="Seg_1665" n="HIAT:ip">,</nts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1668" n="HIAT:w" s="T412">кланяется</ts>
                  <nts id="Seg_1669" n="HIAT:ip">,</nts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1672" n="HIAT:w" s="T413">кланяется</ts>
                  <nts id="Seg_1673" n="HIAT:ip">;</nts>
                  <nts id="Seg_1674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1676" n="HIAT:w" s="T414">a</ts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1679" n="HIAT:w" s="T415">maʔndə</ts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1682" n="HIAT:w" s="T416">šoləj</ts>
                  <nts id="Seg_1683" n="HIAT:ip">—</nts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1686" n="HIAT:w" s="T417">iʔbolaʔbə</ts>
                  <nts id="Seg_1687" n="HIAT:ip">.</nts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T420" id="Seg_1690" n="HIAT:u" s="T418">
                  <ts e="T419" id="Seg_1692" n="HIAT:w" s="T418">Dĭ</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1695" n="HIAT:w" s="T419">baltu</ts>
                  <nts id="Seg_1696" n="HIAT:ip">.</nts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T421" id="Seg_1699" n="HIAT:u" s="T420">
                  <nts id="Seg_1700" n="HIAT:ip">(</nts>
                  <nts id="Seg_1701" n="HIAT:ip">(</nts>
                  <ats e="T421" id="Seg_1702" n="HIAT:non-pho" s="T420">BRK</ats>
                  <nts id="Seg_1703" n="HIAT:ip">)</nts>
                  <nts id="Seg_1704" n="HIAT:ip">)</nts>
                  <nts id="Seg_1705" n="HIAT:ip">.</nts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_1708" n="HIAT:u" s="T421">
                  <ts e="T422" id="Seg_1710" n="HIAT:w" s="T421">Udat</ts>
                  <nts id="Seg_1711" n="HIAT:ip">,</nts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1714" n="HIAT:w" s="T422">üjüt</ts>
                  <nts id="Seg_1715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1717" n="HIAT:w" s="T423">naga</ts>
                  <nts id="Seg_1718" n="HIAT:ip">.</nts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T428" id="Seg_1721" n="HIAT:u" s="T424">
                  <ts e="T425" id="Seg_1723" n="HIAT:w" s="T424">A</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1726" n="HIAT:w" s="T425">ajim</ts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1729" n="HIAT:w" s="T426">bar</ts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1732" n="HIAT:w" s="T427">karlaʔbə</ts>
                  <nts id="Seg_1733" n="HIAT:ip">.</nts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T430" id="Seg_1736" n="HIAT:u" s="T428">
                  <ts e="T429" id="Seg_1738" n="HIAT:w" s="T428">Dĭ</ts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1741" n="HIAT:w" s="T429">beržə</ts>
                  <nts id="Seg_1742" n="HIAT:ip">.</nts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T431" id="Seg_1745" n="HIAT:u" s="T430">
                  <nts id="Seg_1746" n="HIAT:ip">(</nts>
                  <nts id="Seg_1747" n="HIAT:ip">(</nts>
                  <ats e="T431" id="Seg_1748" n="HIAT:non-pho" s="T430">BRK</ats>
                  <nts id="Seg_1749" n="HIAT:ip">)</nts>
                  <nts id="Seg_1750" n="HIAT:ip">)</nts>
                  <nts id="Seg_1751" n="HIAT:ip">.</nts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T437" id="Seg_1754" n="HIAT:u" s="T431">
                  <ts e="T432" id="Seg_1756" n="HIAT:w" s="T431">Üdʼüge</ts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1758" n="HIAT:ip">(</nts>
                  <ts e="T433" id="Seg_1760" n="HIAT:w" s="T432">men-</ts>
                  <nts id="Seg_1761" n="HIAT:ip">)</nts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1764" n="HIAT:w" s="T433">men</ts>
                  <nts id="Seg_1765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1767" n="HIAT:w" s="T434">maːndə</ts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1770" n="HIAT:w" s="T435">toʔndə</ts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1773" n="HIAT:w" s="T436">amnolaʔbəʔjə</ts>
                  <nts id="Seg_1774" n="HIAT:ip">.</nts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_1777" n="HIAT:u" s="T437">
                  <ts e="T438" id="Seg_1779" n="HIAT:w" s="T437">Ej</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1782" n="HIAT:w" s="T438">kürümnie</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1785" n="HIAT:w" s="T439">i</ts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1787" n="HIAT:ip">(</nts>
                  <ts e="T441" id="Seg_1789" n="HIAT:w" s="T440">šindi-</ts>
                  <nts id="Seg_1790" n="HIAT:ip">)</nts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1793" n="HIAT:w" s="T441">šindimdə</ts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1796" n="HIAT:w" s="T442">maʔtə</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1799" n="HIAT:w" s="T443">ej</ts>
                  <nts id="Seg_1800" n="HIAT:ip">…</nts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_1803" n="HIAT:u" s="T445">
                  <nts id="Seg_1804" n="HIAT:ip">(</nts>
                  <nts id="Seg_1805" n="HIAT:ip">(</nts>
                  <ats e="T446" id="Seg_1806" n="HIAT:non-pho" s="T445">BRK</ats>
                  <nts id="Seg_1807" n="HIAT:ip">)</nts>
                  <nts id="Seg_1808" n="HIAT:ip">)</nts>
                  <nts id="Seg_1809" n="HIAT:ip">.</nts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T450" id="Seg_1812" n="HIAT:u" s="T446">
                  <ts e="T447" id="Seg_1814" n="HIAT:w" s="T446">Šindinədə</ts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1817" n="HIAT:w" s="T447">maʔndə</ts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1820" n="HIAT:w" s="T448">ej</ts>
                  <nts id="Seg_1821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1823" n="HIAT:w" s="T449">öʔle</ts>
                  <nts id="Seg_1824" n="HIAT:ip">.</nts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T452" id="Seg_1827" n="HIAT:u" s="T450">
                  <ts e="T451" id="Seg_1829" n="HIAT:w" s="T450">Dĭ</ts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1832" n="HIAT:w" s="T451">замок</ts>
                  <nts id="Seg_1833" n="HIAT:ip">.</nts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1836" n="HIAT:u" s="T452">
                  <nts id="Seg_1837" n="HIAT:ip">(</nts>
                  <nts id="Seg_1838" n="HIAT:ip">(</nts>
                  <ats e="T453" id="Seg_1839" n="HIAT:non-pho" s="T452">BRK</ats>
                  <nts id="Seg_1840" n="HIAT:ip">)</nts>
                  <nts id="Seg_1841" n="HIAT:ip">)</nts>
                  <nts id="Seg_1842" n="HIAT:ip">.</nts>
                  <nts id="Seg_1843" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T456" id="Seg_1845" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_1847" n="HIAT:w" s="T453">Krasnojarskəʔi</ts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1850" n="HIAT:w" s="T454">turaʔi</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1853" n="HIAT:w" s="T455">jaʔleʔbəʔjə</ts>
                  <nts id="Seg_1854" n="HIAT:ip">.</nts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T460" id="Seg_1857" n="HIAT:u" s="T456">
                  <ts e="T457" id="Seg_1859" n="HIAT:w" s="T456">A</ts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1862" n="HIAT:w" s="T457">döber</ts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1865" n="HIAT:w" s="T458">paʔi</ts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1868" n="HIAT:w" s="T459">nʼergölaʔbəʔjə</ts>
                  <nts id="Seg_1869" n="HIAT:ip">.</nts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_1872" n="HIAT:u" s="T460">
                  <ts e="T461" id="Seg_1874" n="HIAT:w" s="T460">Dĭ</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1877" n="HIAT:w" s="T461">sazən</ts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1880" n="HIAT:w" s="T462">kandəga</ts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1883" n="HIAT:w" s="T463">dĭʔə</ts>
                  <nts id="Seg_1884" n="HIAT:ip">.</nts>
                  <nts id="Seg_1885" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T466" id="Seg_1887" n="HIAT:u" s="T464">
                  <nts id="Seg_1888" n="HIAT:ip">(</nts>
                  <nts id="Seg_1889" n="HIAT:ip">(</nts>
                  <ats e="T891" id="Seg_1890" n="HIAT:non-pho" s="T464">…</ats>
                  <nts id="Seg_1891" n="HIAT:ip">)</nts>
                  <nts id="Seg_1892" n="HIAT:ip">)</nts>
                  <nts id="Seg_1893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1894" n="HIAT:ip">(</nts>
                  <nts id="Seg_1895" n="HIAT:ip">(</nts>
                  <ats e="T466" id="Seg_1896" n="HIAT:non-pho" s="T891">BRK</ats>
                  <nts id="Seg_1897" n="HIAT:ip">)</nts>
                  <nts id="Seg_1898" n="HIAT:ip">)</nts>
                  <nts id="Seg_1899" n="HIAT:ip">.</nts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T477" id="Seg_1902" n="HIAT:u" s="T466">
                  <nts id="Seg_1903" n="HIAT:ip">(</nts>
                  <ts e="T467" id="Seg_1905" n="HIAT:w" s="T466">Šĭket</ts>
                  <nts id="Seg_1906" n="HIAT:ip">)</nts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1909" n="HIAT:w" s="T467">naga</ts>
                  <nts id="Seg_1910" n="HIAT:ip">,</nts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1913" n="HIAT:w" s="T468">simat</ts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1916" n="HIAT:w" s="T469">naga</ts>
                  <nts id="Seg_1917" n="HIAT:ip">,</nts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1920" n="HIAT:w" s="T470">aŋdə</ts>
                  <nts id="Seg_1921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1923" n="HIAT:w" s="T471">naga</ts>
                  <nts id="Seg_1924" n="HIAT:ip">,</nts>
                  <nts id="Seg_1925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1927" n="HIAT:w" s="T472">a</ts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1930" n="HIAT:w" s="T473">măllia</ts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1933" n="HIAT:w" s="T474">kumen</ts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1936" n="HIAT:w" s="T475">vremja</ts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1939" n="HIAT:w" s="T476">kambi</ts>
                  <nts id="Seg_1940" n="HIAT:ip">.</nts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T479" id="Seg_1943" n="HIAT:u" s="T477">
                  <ts e="T478" id="Seg_1945" n="HIAT:w" s="T477">Dĭ</ts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1948" n="HIAT:w" s="T478">časɨʔi</ts>
                  <nts id="Seg_1949" n="HIAT:ip">.</nts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T480" id="Seg_1952" n="HIAT:u" s="T479">
                  <nts id="Seg_1953" n="HIAT:ip">(</nts>
                  <nts id="Seg_1954" n="HIAT:ip">(</nts>
                  <ats e="T480" id="Seg_1955" n="HIAT:non-pho" s="T479">BRK</ats>
                  <nts id="Seg_1956" n="HIAT:ip">)</nts>
                  <nts id="Seg_1957" n="HIAT:ip">)</nts>
                  <nts id="Seg_1958" n="HIAT:ip">.</nts>
                  <nts id="Seg_1959" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T484" id="Seg_1961" n="HIAT:u" s="T480">
                  <ts e="T481" id="Seg_1963" n="HIAT:w" s="T480">Iʔbolaʔbə</ts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1966" n="HIAT:w" s="T481">dăk</ts>
                  <nts id="Seg_1967" n="HIAT:ip">,</nts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1970" n="HIAT:w" s="T482">üdʼüge</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1973" n="HIAT:w" s="T483">tospaktə</ts>
                  <nts id="Seg_1974" n="HIAT:ip">.</nts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T491" id="Seg_1977" n="HIAT:u" s="T484">
                  <ts e="T485" id="Seg_1979" n="HIAT:w" s="T484">A</ts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1982" n="HIAT:w" s="T485">nulaʔbə</ts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1985" n="HIAT:w" s="T486">dăk</ts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1988" n="HIAT:w" s="T487">ugandə</ts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1991" n="HIAT:w" s="T488">urgo</ts>
                  <nts id="Seg_1992" n="HIAT:ip">,</nts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1995" n="HIAT:w" s="T489">urgo</ts>
                  <nts id="Seg_1996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1998" n="HIAT:w" s="T490">inetsi</ts>
                  <nts id="Seg_1999" n="HIAT:ip">.</nts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T882" id="Seg_2001" n="sc" s="T492">
               <ts e="T493" id="Seg_2003" n="HIAT:u" s="T492">
                  <ts e="T493" id="Seg_2005" n="HIAT:w" s="T492">Duga</ts>
                  <nts id="Seg_2006" n="HIAT:ip">.</nts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T494" id="Seg_2009" n="HIAT:u" s="T493">
                  <nts id="Seg_2010" n="HIAT:ip">(</nts>
                  <nts id="Seg_2011" n="HIAT:ip">(</nts>
                  <ats e="T494" id="Seg_2012" n="HIAT:non-pho" s="T493">BRK</ats>
                  <nts id="Seg_2013" n="HIAT:ip">)</nts>
                  <nts id="Seg_2014" n="HIAT:ip">)</nts>
                  <nts id="Seg_2015" n="HIAT:ip">.</nts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T499" id="Seg_2018" n="HIAT:u" s="T494">
                  <ts e="T495" id="Seg_2020" n="HIAT:w" s="T494">Amnolaʔbə</ts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_2023" n="HIAT:w" s="T495">koʔbdo</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_2026" n="HIAT:w" s="T496">bar</ts>
                  <nts id="Seg_2027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_2029" n="HIAT:w" s="T497">ospaʔizi</ts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2031" n="HIAT:ip">(</nts>
                  <ts e="T499" id="Seg_2033" n="HIAT:w" s="T498">tĭ</ts>
                  <nts id="Seg_2034" n="HIAT:ip">)</nts>
                  <nts id="Seg_2035" n="HIAT:ip">.</nts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T502" id="Seg_2038" n="HIAT:u" s="T499">
                  <ts e="T500" id="Seg_2040" n="HIAT:w" s="T499">A</ts>
                  <nts id="Seg_2041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_2043" n="HIAT:w" s="T500">dĭ</ts>
                  <nts id="Seg_2044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_2046" n="HIAT:w" s="T501">indak</ts>
                  <nts id="Seg_2047" n="HIAT:ip">.</nts>
                  <nts id="Seg_2048" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T503" id="Seg_2050" n="HIAT:u" s="T502">
                  <nts id="Seg_2051" n="HIAT:ip">(</nts>
                  <nts id="Seg_2052" n="HIAT:ip">(</nts>
                  <ats e="T503" id="Seg_2053" n="HIAT:non-pho" s="T502">BRK</ats>
                  <nts id="Seg_2054" n="HIAT:ip">)</nts>
                  <nts id="Seg_2055" n="HIAT:ip">)</nts>
                  <nts id="Seg_2056" n="HIAT:ip">.</nts>
                  <nts id="Seg_2057" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T513" id="Seg_2059" n="HIAT:u" s="T503">
                  <ts e="T504" id="Seg_2061" n="HIAT:w" s="T503">Oʔbdəbiam</ts>
                  <nts id="Seg_2062" n="HIAT:ip">,</nts>
                  <nts id="Seg_2063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_2065" n="HIAT:w" s="T504">oʔbdəbiam</ts>
                  <nts id="Seg_2066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_2068" n="HIAT:w" s="T505">nʼitkaʔi</ts>
                  <nts id="Seg_2069" n="HIAT:ip">,</nts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_2072" n="HIAT:w" s="T506">ej</ts>
                  <nts id="Seg_2073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2074" n="HIAT:ip">(</nts>
                  <ts e="T508" id="Seg_2076" n="HIAT:w" s="T507">mombia-</ts>
                  <nts id="Seg_2077" n="HIAT:ip">)</nts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_2080" n="HIAT:w" s="T508">ej</ts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_2083" n="HIAT:w" s="T509">mobiam</ts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_2086" n="HIAT:w" s="T510">oʔbdəsʼtə</ts>
                  <nts id="Seg_2087" n="HIAT:ip">,</nts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_2090" n="HIAT:w" s="T511">dĭ</ts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_2093" n="HIAT:w" s="T512">aʔtʼi</ts>
                  <nts id="Seg_2094" n="HIAT:ip">.</nts>
                  <nts id="Seg_2095" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T514" id="Seg_2097" n="HIAT:u" s="T513">
                  <nts id="Seg_2098" n="HIAT:ip">(</nts>
                  <nts id="Seg_2099" n="HIAT:ip">(</nts>
                  <ats e="T514" id="Seg_2100" n="HIAT:non-pho" s="T513">BRK</ats>
                  <nts id="Seg_2101" n="HIAT:ip">)</nts>
                  <nts id="Seg_2102" n="HIAT:ip">)</nts>
                  <nts id="Seg_2103" n="HIAT:ip">.</nts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T517" id="Seg_2106" n="HIAT:u" s="T514">
                  <ts e="T515" id="Seg_2108" n="HIAT:w" s="T514">Amnolaʔbə</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2111" n="HIAT:w" s="T515">büzʼe</ts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2114" n="HIAT:w" s="T516">grʼadkagən</ts>
                  <nts id="Seg_2115" n="HIAT:ip">.</nts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T520" id="Seg_2118" n="HIAT:u" s="T517">
                  <ts e="T518" id="Seg_2120" n="HIAT:w" s="T517">Bar</ts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_2123" n="HIAT:w" s="T518">iʔgö</ts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_2126" n="HIAT:w" s="T519">zaplatkaʔi</ts>
                  <nts id="Seg_2127" n="HIAT:ip">.</nts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T525" id="Seg_2130" n="HIAT:u" s="T520">
                  <ts e="T521" id="Seg_2132" n="HIAT:w" s="T520">Šində</ts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2134" n="HIAT:ip">(</nts>
                  <ts e="T522" id="Seg_2136" n="HIAT:w" s="T521">kulil-</ts>
                  <nts id="Seg_2137" n="HIAT:ip">)</nts>
                  <nts id="Seg_2138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2140" n="HIAT:w" s="T522">kulaʔbə</ts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_2143" n="HIAT:w" s="T523">bar</ts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2146" n="HIAT:w" s="T524">dʼorlaʔbə</ts>
                  <nts id="Seg_2147" n="HIAT:ip">.</nts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T527" id="Seg_2150" n="HIAT:u" s="T525">
                  <ts e="T526" id="Seg_2152" n="HIAT:w" s="T525">Dĭ</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2155" n="HIAT:w" s="T526">köbergən</ts>
                  <nts id="Seg_2156" n="HIAT:ip">.</nts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T528" id="Seg_2159" n="HIAT:u" s="T527">
                  <nts id="Seg_2160" n="HIAT:ip">(</nts>
                  <nts id="Seg_2161" n="HIAT:ip">(</nts>
                  <ats e="T528" id="Seg_2162" n="HIAT:non-pho" s="T527">BRK</ats>
                  <nts id="Seg_2163" n="HIAT:ip">)</nts>
                  <nts id="Seg_2164" n="HIAT:ip">)</nts>
                  <nts id="Seg_2165" n="HIAT:ip">.</nts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T532" id="Seg_2168" n="HIAT:u" s="T528">
                  <ts e="T529" id="Seg_2170" n="HIAT:w" s="T528">Amnolaʔbə</ts>
                  <nts id="Seg_2171" n="HIAT:ip">,</nts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2174" n="HIAT:w" s="T529">iʔgö</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2177" n="HIAT:w" s="T530">oldʼa</ts>
                  <nts id="Seg_2178" n="HIAT:ip">…</nts>
                  <nts id="Seg_2179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T533" id="Seg_2181" n="HIAT:u" s="T532">
                  <nts id="Seg_2182" n="HIAT:ip">(</nts>
                  <nts id="Seg_2183" n="HIAT:ip">(</nts>
                  <ats e="T533" id="Seg_2184" n="HIAT:non-pho" s="T532">BRK</ats>
                  <nts id="Seg_2185" n="HIAT:ip">)</nts>
                  <nts id="Seg_2186" n="HIAT:ip">)</nts>
                  <nts id="Seg_2187" n="HIAT:ip">.</nts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T539" id="Seg_2190" n="HIAT:u" s="T533">
                  <nts id="Seg_2191" n="HIAT:ip">(</nts>
                  <ts e="T534" id="Seg_2193" n="HIAT:w" s="T533">Sojp-</ts>
                  <nts id="Seg_2194" n="HIAT:ip">)</nts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2197" n="HIAT:w" s="T534">Sejʔpü</ts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2199" n="HIAT:ip">(</nts>
                  <ts e="T536" id="Seg_2201" n="HIAT:w" s="T535">aktʼa=</ts>
                  <nts id="Seg_2202" n="HIAT:ip">)</nts>
                  <nts id="Seg_2203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2205" n="HIAT:ip">(</nts>
                  <nts id="Seg_2206" n="HIAT:ip">(</nts>
                  <ats e="T537" id="Seg_2207" n="HIAT:non-pho" s="T536">PAUSE</ats>
                  <nts id="Seg_2208" n="HIAT:ip">)</nts>
                  <nts id="Seg_2209" n="HIAT:ip">)</nts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2211" n="HIAT:ip">(</nts>
                  <ts e="T539" id="Seg_2213" n="HIAT:w" s="T537">o-</ts>
                  <nts id="Seg_2214" n="HIAT:ip">)</nts>
                  <nts id="Seg_2215" n="HIAT:ip">.</nts>
                  <nts id="Seg_2216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T546" id="Seg_2218" n="HIAT:u" s="T539">
                  <ts e="T540" id="Seg_2220" n="HIAT:w" s="T539">Bʼeʔ</ts>
                  <nts id="Seg_2221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2222" n="HIAT:ip">(</nts>
                  <ts e="T541" id="Seg_2224" n="HIAT:w" s="T540">sojp-</ts>
                  <nts id="Seg_2225" n="HIAT:ip">)</nts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2228" n="HIAT:w" s="T541">sejʔpü</ts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2231" n="HIAT:w" s="T542">oldʼa</ts>
                  <nts id="Seg_2232" n="HIAT:ip">,</nts>
                  <nts id="Seg_2233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2235" n="HIAT:w" s="T543">a</ts>
                  <nts id="Seg_2236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2238" n="HIAT:w" s="T544">kajzittə</ts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2241" n="HIAT:w" s="T545">naga</ts>
                  <nts id="Seg_2242" n="HIAT:ip">.</nts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T548" id="Seg_2245" n="HIAT:u" s="T546">
                  <ts e="T547" id="Seg_2247" n="HIAT:w" s="T546">Dĭ</ts>
                  <nts id="Seg_2248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2250" n="HIAT:w" s="T547">kapusta</ts>
                  <nts id="Seg_2251" n="HIAT:ip">.</nts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_2254" n="HIAT:u" s="T548">
                  <nts id="Seg_2255" n="HIAT:ip">(</nts>
                  <nts id="Seg_2256" n="HIAT:ip">(</nts>
                  <ats e="T549" id="Seg_2257" n="HIAT:non-pho" s="T548">BRK</ats>
                  <nts id="Seg_2258" n="HIAT:ip">)</nts>
                  <nts id="Seg_2259" n="HIAT:ip">)</nts>
                  <nts id="Seg_2260" n="HIAT:ip">.</nts>
                  <nts id="Seg_2261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T554" id="Seg_2263" n="HIAT:u" s="T549">
                  <ts e="T550" id="Seg_2265" n="HIAT:w" s="T549">Amnolaʔbə</ts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2268" n="HIAT:w" s="T550">koʔbdo</ts>
                  <nts id="Seg_2269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2271" n="HIAT:w" s="T551">dʼügən</ts>
                  <nts id="Seg_2272" n="HIAT:ip">,</nts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2275" n="HIAT:w" s="T552">bostə</ts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2278" n="HIAT:w" s="T553">kömə</ts>
                  <nts id="Seg_2279" n="HIAT:ip">.</nts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T557" id="Seg_2282" n="HIAT:u" s="T554">
                  <ts e="T555" id="Seg_2284" n="HIAT:w" s="T554">A</ts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2287" n="HIAT:w" s="T555">eʔbdət</ts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2290" n="HIAT:w" s="T556">nʼiʔnen</ts>
                  <nts id="Seg_2291" n="HIAT:ip">.</nts>
                  <nts id="Seg_2292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_2294" n="HIAT:u" s="T557">
                  <ts e="T558" id="Seg_2296" n="HIAT:w" s="T557">Dĭ</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2299" n="HIAT:w" s="T558">морковка</ts>
                  <nts id="Seg_2300" n="HIAT:ip">.</nts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T560" id="Seg_2303" n="HIAT:u" s="T559">
                  <nts id="Seg_2304" n="HIAT:ip">(</nts>
                  <nts id="Seg_2305" n="HIAT:ip">(</nts>
                  <ats e="T560" id="Seg_2306" n="HIAT:non-pho" s="T559">BRK</ats>
                  <nts id="Seg_2307" n="HIAT:ip">)</nts>
                  <nts id="Seg_2308" n="HIAT:ip">)</nts>
                  <nts id="Seg_2309" n="HIAT:ip">.</nts>
                  <nts id="Seg_2310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T567" id="Seg_2312" n="HIAT:u" s="T560">
                  <ts e="T561" id="Seg_2314" n="HIAT:w" s="T560">Măn</ts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2317" n="HIAT:w" s="T561">kubiam</ts>
                  <nts id="Seg_2318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2320" n="HIAT:w" s="T562">plʼotka</ts>
                  <nts id="Seg_2321" n="HIAT:ip">,</nts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2324" n="HIAT:w" s="T563">a</ts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2327" n="HIAT:w" s="T564">kanzittə</ts>
                  <nts id="Seg_2328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2330" n="HIAT:w" s="T565">ej</ts>
                  <nts id="Seg_2331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2333" n="HIAT:w" s="T566">mobiam</ts>
                  <nts id="Seg_2334" n="HIAT:ip">.</nts>
                  <nts id="Seg_2335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T570" id="Seg_2337" n="HIAT:u" s="T567">
                  <ts e="T568" id="Seg_2339" n="HIAT:w" s="T567">A</ts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2342" n="HIAT:w" s="T568">dĭ</ts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2345" n="HIAT:w" s="T569">ĭmbi</ts>
                  <nts id="Seg_2346" n="HIAT:ip">?</nts>
                  <nts id="Seg_2347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T571" id="Seg_2349" n="HIAT:u" s="T570">
                  <ts e="T571" id="Seg_2351" n="HIAT:w" s="T570">Penzə</ts>
                  <nts id="Seg_2352" n="HIAT:ip">.</nts>
                  <nts id="Seg_2353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T572" id="Seg_2355" n="HIAT:u" s="T571">
                  <nts id="Seg_2356" n="HIAT:ip">(</nts>
                  <nts id="Seg_2357" n="HIAT:ip">(</nts>
                  <ats e="T572" id="Seg_2358" n="HIAT:non-pho" s="T571">BRK</ats>
                  <nts id="Seg_2359" n="HIAT:ip">)</nts>
                  <nts id="Seg_2360" n="HIAT:ip">)</nts>
                  <nts id="Seg_2361" n="HIAT:ip">.</nts>
                  <nts id="Seg_2362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T581" id="Seg_2364" n="HIAT:u" s="T572">
                  <ts e="T573" id="Seg_2366" n="HIAT:w" s="T572">Šonəga</ts>
                  <nts id="Seg_2367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2369" n="HIAT:w" s="T573">koʔbdo</ts>
                  <nts id="Seg_2370" n="HIAT:ip">,</nts>
                  <nts id="Seg_2371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2373" n="HIAT:w" s="T574">i</ts>
                  <nts id="Seg_2374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2376" n="HIAT:w" s="T575">dĭn</ts>
                  <nts id="Seg_2377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2379" n="HIAT:w" s="T576">eʔbdəʔi</ts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2382" n="HIAT:w" s="T577">bar</ts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2384" n="HIAT:ip">(</nts>
                  <ts e="T579" id="Seg_2386" n="HIAT:w" s="T578">ko-</ts>
                  <nts id="Seg_2387" n="HIAT:ip">)</nts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2390" n="HIAT:w" s="T579">kurgonaʔi</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2393" n="HIAT:w" s="T580">bar</ts>
                  <nts id="Seg_2394" n="HIAT:ip">.</nts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T582" id="Seg_2397" n="HIAT:u" s="T581">
                  <nts id="Seg_2398" n="HIAT:ip">(</nts>
                  <nts id="Seg_2399" n="HIAT:ip">(</nts>
                  <ats e="T582" id="Seg_2400" n="HIAT:non-pho" s="T581">BRK</ats>
                  <nts id="Seg_2401" n="HIAT:ip">)</nts>
                  <nts id="Seg_2402" n="HIAT:ip">)</nts>
                  <nts id="Seg_2403" n="HIAT:ip">.</nts>
                  <nts id="Seg_2404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T585" id="Seg_2406" n="HIAT:u" s="T582">
                  <ts e="T583" id="Seg_2408" n="HIAT:w" s="T582">Dĭ</ts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2411" n="HIAT:w" s="T583">soroka</ts>
                  <nts id="Seg_2412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2414" n="HIAT:w" s="T584">ige</ts>
                  <nts id="Seg_2415" n="HIAT:ip">.</nts>
                  <nts id="Seg_2416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T586" id="Seg_2418" n="HIAT:u" s="T585">
                  <nts id="Seg_2419" n="HIAT:ip">(</nts>
                  <nts id="Seg_2420" n="HIAT:ip">(</nts>
                  <ats e="T586" id="Seg_2421" n="HIAT:non-pho" s="T585">BRK</ats>
                  <nts id="Seg_2422" n="HIAT:ip">)</nts>
                  <nts id="Seg_2423" n="HIAT:ip">)</nts>
                  <nts id="Seg_2424" n="HIAT:ip">.</nts>
                  <nts id="Seg_2425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T598" id="Seg_2427" n="HIAT:u" s="T586">
                  <ts e="T587" id="Seg_2429" n="HIAT:w" s="T586">Šide</ts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2432" n="HIAT:w" s="T587">nugaʔi</ts>
                  <nts id="Seg_2433" n="HIAT:ip">,</nts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2436" n="HIAT:w" s="T588">šide</ts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2439" n="HIAT:w" s="T589">iʔbolaʔbəʔjə</ts>
                  <nts id="Seg_2440" n="HIAT:ip">,</nts>
                  <nts id="Seg_2441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2442" n="HIAT:ip">(</nts>
                  <ts e="T591" id="Seg_2444" n="HIAT:w" s="T590">onʼiʔ</ts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2447" n="HIAT:w" s="T591">mĭlleʔbə=</ts>
                  <nts id="Seg_2448" n="HIAT:ip">)</nts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2451" n="HIAT:w" s="T592">sumna</ts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2454" n="HIAT:w" s="T593">mĭlleʔbə</ts>
                  <nts id="Seg_2455" n="HIAT:ip">,</nts>
                  <nts id="Seg_2456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2458" n="HIAT:w" s="T594">a</ts>
                  <nts id="Seg_2459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2461" n="HIAT:w" s="T595">sejʔpü</ts>
                  <nts id="Seg_2462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2463" n="HIAT:ip">(</nts>
                  <ts e="T597" id="Seg_2465" n="HIAT:w" s="T596">kaj-</ts>
                  <nts id="Seg_2466" n="HIAT:ip">)</nts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2469" n="HIAT:w" s="T597">kajlaʔbə</ts>
                  <nts id="Seg_2470" n="HIAT:ip">.</nts>
                  <nts id="Seg_2471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T601" id="Seg_2473" n="HIAT:u" s="T598">
                  <ts e="T599" id="Seg_2475" n="HIAT:w" s="T598">Dĭ</ts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2478" n="HIAT:w" s="T599">aji</ts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2481" n="HIAT:w" s="T600">ige</ts>
                  <nts id="Seg_2482" n="HIAT:ip">.</nts>
                  <nts id="Seg_2483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T602" id="Seg_2485" n="HIAT:u" s="T601">
                  <nts id="Seg_2486" n="HIAT:ip">(</nts>
                  <nts id="Seg_2487" n="HIAT:ip">(</nts>
                  <ats e="T602" id="Seg_2488" n="HIAT:non-pho" s="T601">BRK</ats>
                  <nts id="Seg_2489" n="HIAT:ip">)</nts>
                  <nts id="Seg_2490" n="HIAT:ip">)</nts>
                  <nts id="Seg_2491" n="HIAT:ip">.</nts>
                  <nts id="Seg_2492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T611" id="Seg_2494" n="HIAT:u" s="T602">
                  <nts id="Seg_2495" n="HIAT:ip">(</nts>
                  <ts e="T603" id="Seg_2497" n="HIAT:w" s="T602">Ula-</ts>
                  <nts id="Seg_2498" n="HIAT:ip">)</nts>
                  <nts id="Seg_2499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2501" n="HIAT:w" s="T603">ular</ts>
                  <nts id="Seg_2502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2504" n="HIAT:w" s="T604">iʔgö</ts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2507" n="HIAT:w" s="T605">mĭlleʔbəʔi</ts>
                  <nts id="Seg_2508" n="HIAT:ip">,</nts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2511" n="HIAT:w" s="T606">a</ts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2514" n="HIAT:w" s="T607">pastux</ts>
                  <nts id="Seg_2515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2516" n="HIAT:ip">(</nts>
                  <nts id="Seg_2517" n="HIAT:ip">(</nts>
                  <ats e="T609" id="Seg_2518" n="HIAT:non-pho" s="T608">PAUSE</ats>
                  <nts id="Seg_2519" n="HIAT:ip">)</nts>
                  <nts id="Seg_2520" n="HIAT:ip">)</nts>
                  <nts id="Seg_2521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2523" n="HIAT:w" s="T609">amnut</ts>
                  <nts id="Seg_2524" n="HIAT:ip">.</nts>
                  <nts id="Seg_2525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T615" id="Seg_2527" n="HIAT:u" s="T611">
                  <ts e="T612" id="Seg_2529" n="HIAT:w" s="T611">Dĭ</ts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2532" n="HIAT:w" s="T612">mʼesʼas</ts>
                  <nts id="Seg_2533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2535" n="HIAT:w" s="T613">i</ts>
                  <nts id="Seg_2536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2538" n="HIAT:w" s="T614">zvʼozdaʔi</ts>
                  <nts id="Seg_2539" n="HIAT:ip">.</nts>
                  <nts id="Seg_2540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T616" id="Seg_2542" n="HIAT:u" s="T615">
                  <nts id="Seg_2543" n="HIAT:ip">(</nts>
                  <nts id="Seg_2544" n="HIAT:ip">(</nts>
                  <ats e="T616" id="Seg_2545" n="HIAT:non-pho" s="T615">BRK</ats>
                  <nts id="Seg_2546" n="HIAT:ip">)</nts>
                  <nts id="Seg_2547" n="HIAT:ip">)</nts>
                  <nts id="Seg_2548" n="HIAT:ip">.</nts>
                  <nts id="Seg_2549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T624" id="Seg_2551" n="HIAT:u" s="T616">
                  <ts e="T617" id="Seg_2553" n="HIAT:w" s="T616">Šide</ts>
                  <nts id="Seg_2554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2556" n="HIAT:w" s="T617">ši</ts>
                  <nts id="Seg_2557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2559" n="HIAT:w" s="T618">i</ts>
                  <nts id="Seg_2560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2562" n="HIAT:w" s="T619">šide</ts>
                  <nts id="Seg_2563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2565" n="HIAT:w" s="T620">puʔməʔi</ts>
                  <nts id="Seg_2566" n="HIAT:ip">,</nts>
                  <nts id="Seg_2567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2569" n="HIAT:w" s="T621">a</ts>
                  <nts id="Seg_2570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2572" n="HIAT:w" s="T622">šüjögən</ts>
                  <nts id="Seg_2573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2575" n="HIAT:w" s="T623">marka</ts>
                  <nts id="Seg_2576" n="HIAT:ip">.</nts>
                  <nts id="Seg_2577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T625" id="Seg_2579" n="HIAT:u" s="T624">
                  <ts e="T625" id="Seg_2581" n="HIAT:w" s="T624">Kaptəʔi</ts>
                  <nts id="Seg_2582" n="HIAT:ip">.</nts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T626" id="Seg_2585" n="HIAT:u" s="T625">
                  <nts id="Seg_2586" n="HIAT:ip">(</nts>
                  <nts id="Seg_2587" n="HIAT:ip">(</nts>
                  <ats e="T626" id="Seg_2588" n="HIAT:non-pho" s="T625">BRK</ats>
                  <nts id="Seg_2589" n="HIAT:ip">)</nts>
                  <nts id="Seg_2590" n="HIAT:ip">)</nts>
                  <nts id="Seg_2591" n="HIAT:ip">.</nts>
                  <nts id="Seg_2592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T635" id="Seg_2594" n="HIAT:u" s="T626">
                  <ts e="T627" id="Seg_2596" n="HIAT:w" s="T626">Măn</ts>
                  <nts id="Seg_2597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2599" n="HIAT:w" s="T627">üge</ts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2602" n="HIAT:w" s="T628">amnaʔbəm</ts>
                  <nts id="Seg_2603" n="HIAT:ip">,</nts>
                  <nts id="Seg_2604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2606" n="HIAT:w" s="T629">amnaʔbəm</ts>
                  <nts id="Seg_2607" n="HIAT:ip">,</nts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2610" n="HIAT:w" s="T630">a</ts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2613" n="HIAT:w" s="T631">tüʔsittə</ts>
                  <nts id="Seg_2614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2616" n="HIAT:w" s="T632">ej</ts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2619" n="HIAT:w" s="T633">moliam</ts>
                  <nts id="Seg_2620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2621" n="HIAT:ip">—</nts>
                  <nts id="Seg_2622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2624" n="HIAT:w" s="T634">Bura</ts>
                  <nts id="Seg_2625" n="HIAT:ip">.</nts>
                  <nts id="Seg_2626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T636" id="Seg_2628" n="HIAT:u" s="T635">
                  <nts id="Seg_2629" n="HIAT:ip">(</nts>
                  <nts id="Seg_2630" n="HIAT:ip">(</nts>
                  <ats e="T636" id="Seg_2631" n="HIAT:non-pho" s="T635">BRK</ats>
                  <nts id="Seg_2632" n="HIAT:ip">)</nts>
                  <nts id="Seg_2633" n="HIAT:ip">)</nts>
                  <nts id="Seg_2634" n="HIAT:ip">.</nts>
                  <nts id="Seg_2635" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T639" id="Seg_2637" n="HIAT:u" s="T636">
                  <ts e="T637" id="Seg_2639" n="HIAT:w" s="T636">Pʼetux</ts>
                  <nts id="Seg_2640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2642" n="HIAT:w" s="T637">amnolaʔbə</ts>
                  <nts id="Seg_2643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2645" n="HIAT:w" s="T638">zaborgən</ts>
                  <nts id="Seg_2646" n="HIAT:ip">.</nts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T644" id="Seg_2649" n="HIAT:u" s="T639">
                  <ts e="T640" id="Seg_2651" n="HIAT:w" s="T639">Xvost</ts>
                  <nts id="Seg_2652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2654" n="HIAT:w" s="T640">dʼügən</ts>
                  <nts id="Seg_2655" n="HIAT:ip">,</nts>
                  <nts id="Seg_2656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2658" n="HIAT:w" s="T641">a</ts>
                  <nts id="Seg_2659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2661" n="HIAT:w" s="T642">kirgarlaʔbə</ts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2664" n="HIAT:w" s="T643">kudajdə</ts>
                  <nts id="Seg_2665" n="HIAT:ip">.</nts>
                  <nts id="Seg_2666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T646" id="Seg_2668" n="HIAT:u" s="T644">
                  <ts e="T645" id="Seg_2670" n="HIAT:w" s="T644">Dĭ</ts>
                  <nts id="Seg_2671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2673" n="HIAT:w" s="T645">koŋgoro</ts>
                  <nts id="Seg_2674" n="HIAT:ip">.</nts>
                  <nts id="Seg_2675" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T647" id="Seg_2677" n="HIAT:u" s="T646">
                  <nts id="Seg_2678" n="HIAT:ip">(</nts>
                  <nts id="Seg_2679" n="HIAT:ip">(</nts>
                  <ats e="T647" id="Seg_2680" n="HIAT:non-pho" s="T646">BRK</ats>
                  <nts id="Seg_2681" n="HIAT:ip">)</nts>
                  <nts id="Seg_2682" n="HIAT:ip">)</nts>
                  <nts id="Seg_2683" n="HIAT:ip">.</nts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T656" id="Seg_2686" n="HIAT:u" s="T647">
                  <ts e="T648" id="Seg_2688" n="HIAT:w" s="T647">Nuga</ts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2691" n="HIAT:w" s="T648">nʼi</ts>
                  <nts id="Seg_2692" n="HIAT:ip">,</nts>
                  <nts id="Seg_2693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2695" n="HIAT:w" s="T649">šüjögən</ts>
                  <nts id="Seg_2696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2697" n="HIAT:ip">(</nts>
                  <ts e="T651" id="Seg_2699" n="HIAT:w" s="T650">neŋe-</ts>
                  <nts id="Seg_2700" n="HIAT:ip">)</nts>
                  <nts id="Seg_2701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2703" n="HIAT:w" s="T651">nenileʔbə</ts>
                  <nts id="Seg_2704" n="HIAT:ip">,</nts>
                  <nts id="Seg_2705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2707" n="HIAT:w" s="T652">a</ts>
                  <nts id="Seg_2708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2710" n="HIAT:w" s="T653">bostə</ts>
                  <nts id="Seg_2711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2713" n="HIAT:w" s="T654">bar</ts>
                  <nts id="Seg_2714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2716" n="HIAT:w" s="T655">kĭnzleʔbə</ts>
                  <nts id="Seg_2717" n="HIAT:ip">.</nts>
                  <nts id="Seg_2718" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T658" id="Seg_2720" n="HIAT:u" s="T656">
                  <ts e="T657" id="Seg_2722" n="HIAT:w" s="T656">Dĭ</ts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2725" n="HIAT:w" s="T657">kujdərgan</ts>
                  <nts id="Seg_2726" n="HIAT:ip">.</nts>
                  <nts id="Seg_2727" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T659" id="Seg_2729" n="HIAT:u" s="T658">
                  <nts id="Seg_2730" n="HIAT:ip">(</nts>
                  <nts id="Seg_2731" n="HIAT:ip">(</nts>
                  <ats e="T659" id="Seg_2732" n="HIAT:non-pho" s="T658">BRK</ats>
                  <nts id="Seg_2733" n="HIAT:ip">)</nts>
                  <nts id="Seg_2734" n="HIAT:ip">)</nts>
                  <nts id="Seg_2735" n="HIAT:ip">.</nts>
                  <nts id="Seg_2736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T661" id="Seg_2738" n="HIAT:u" s="T659">
                  <ts e="T660" id="Seg_2740" n="HIAT:w" s="T659">Šidegöʔ</ts>
                  <nts id="Seg_2741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2743" n="HIAT:w" s="T660">kandəgaʔi</ts>
                  <nts id="Seg_2744" n="HIAT:ip">.</nts>
                  <nts id="Seg_2745" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T665" id="Seg_2747" n="HIAT:u" s="T661">
                  <ts e="T662" id="Seg_2749" n="HIAT:w" s="T661">Onʼiʔ</ts>
                  <nts id="Seg_2750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2752" n="HIAT:w" s="T662">kandəga</ts>
                  <nts id="Seg_2753" n="HIAT:ip">,</nts>
                  <nts id="Seg_2754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2756" n="HIAT:w" s="T663">onʼiʔ</ts>
                  <nts id="Seg_2757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2759" n="HIAT:w" s="T664">bĭtlie</ts>
                  <nts id="Seg_2760" n="HIAT:ip">.</nts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T667" id="Seg_2763" n="HIAT:u" s="T665">
                  <ts e="T666" id="Seg_2765" n="HIAT:w" s="T665">Dĭ</ts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2768" n="HIAT:w" s="T666">üjüʔi</ts>
                  <nts id="Seg_2769" n="HIAT:ip">.</nts>
                  <nts id="Seg_2770" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T668" id="Seg_2772" n="HIAT:u" s="T667">
                  <nts id="Seg_2773" n="HIAT:ip">(</nts>
                  <nts id="Seg_2774" n="HIAT:ip">(</nts>
                  <ats e="T668" id="Seg_2775" n="HIAT:non-pho" s="T667">BRK</ats>
                  <nts id="Seg_2776" n="HIAT:ip">)</nts>
                  <nts id="Seg_2777" n="HIAT:ip">)</nts>
                  <nts id="Seg_2778" n="HIAT:ip">.</nts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T676" id="Seg_2781" n="HIAT:u" s="T668">
                  <ts e="T669" id="Seg_2783" n="HIAT:w" s="T668">Toʔnaraʔ</ts>
                  <nts id="Seg_2784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2786" n="HIAT:w" s="T669">măna</ts>
                  <nts id="Seg_2787" n="HIAT:ip">,</nts>
                  <nts id="Seg_2788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2790" n="HIAT:w" s="T670">münöreʔ</ts>
                  <nts id="Seg_2791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2793" n="HIAT:w" s="T671">măna</ts>
                  <nts id="Seg_2794" n="HIAT:ip">,</nts>
                  <nts id="Seg_2795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2797" n="HIAT:w" s="T672">sʼaʔ</ts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2800" n="HIAT:w" s="T673">măna</ts>
                  <nts id="Seg_2801" n="HIAT:ip">,</nts>
                  <nts id="Seg_2802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2804" n="HIAT:w" s="T674">ige</ts>
                  <nts id="Seg_2805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2807" n="HIAT:w" s="T675">măna</ts>
                  <nts id="Seg_2808" n="HIAT:ip">.</nts>
                  <nts id="Seg_2809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T678" id="Seg_2811" n="HIAT:u" s="T676">
                  <ts e="T677" id="Seg_2813" n="HIAT:w" s="T676">Dĭ</ts>
                  <nts id="Seg_2814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2816" n="HIAT:w" s="T677">sanə</ts>
                  <nts id="Seg_2817" n="HIAT:ip">.</nts>
                  <nts id="Seg_2818" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T679" id="Seg_2820" n="HIAT:u" s="T678">
                  <nts id="Seg_2821" n="HIAT:ip">(</nts>
                  <nts id="Seg_2822" n="HIAT:ip">(</nts>
                  <ats e="T679" id="Seg_2823" n="HIAT:non-pho" s="T678">BRK</ats>
                  <nts id="Seg_2824" n="HIAT:ip">)</nts>
                  <nts id="Seg_2825" n="HIAT:ip">)</nts>
                  <nts id="Seg_2826" n="HIAT:ip">.</nts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T687" id="Seg_2829" n="HIAT:u" s="T679">
                  <ts e="T680" id="Seg_2831" n="HIAT:w" s="T679">Edleʔbə</ts>
                  <nts id="Seg_2832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2834" n="HIAT:w" s="T680">bar</ts>
                  <nts id="Seg_2835" n="HIAT:ip">,</nts>
                  <nts id="Seg_2836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2838" n="HIAT:w" s="T681">a</ts>
                  <nts id="Seg_2839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2841" n="HIAT:w" s="T682">il</ts>
                  <nts id="Seg_2842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2844" n="HIAT:w" s="T683">bar</ts>
                  <nts id="Seg_2845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2847" n="HIAT:w" s="T684">kabarlaʔbəʔjə</ts>
                  <nts id="Seg_2848" n="HIAT:ip">,</nts>
                  <nts id="Seg_2849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2851" n="HIAT:w" s="T685">băzəjdlaʔbəʔjə</ts>
                  <nts id="Seg_2852" n="HIAT:ip">,</nts>
                  <nts id="Seg_2853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2855" n="HIAT:w" s="T686">kĭškəleʔbəʔjə</ts>
                  <nts id="Seg_2856" n="HIAT:ip">.</nts>
                  <nts id="Seg_2857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T690" id="Seg_2859" n="HIAT:u" s="T687">
                  <ts e="T688" id="Seg_2861" n="HIAT:w" s="T687">Dĭ</ts>
                  <nts id="Seg_2862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2864" n="HIAT:w" s="T688">rušnʼik</ts>
                  <nts id="Seg_2865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2867" n="HIAT:w" s="T689">ige</ts>
                  <nts id="Seg_2868" n="HIAT:ip">.</nts>
                  <nts id="Seg_2869" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T691" id="Seg_2871" n="HIAT:u" s="T690">
                  <nts id="Seg_2872" n="HIAT:ip">(</nts>
                  <nts id="Seg_2873" n="HIAT:ip">(</nts>
                  <ats e="T691" id="Seg_2874" n="HIAT:non-pho" s="T690">BRK</ats>
                  <nts id="Seg_2875" n="HIAT:ip">)</nts>
                  <nts id="Seg_2876" n="HIAT:ip">)</nts>
                  <nts id="Seg_2877" n="HIAT:ip">.</nts>
                  <nts id="Seg_2878" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T696" id="Seg_2880" n="HIAT:u" s="T691">
                  <ts e="T888" id="Seg_2882" n="HIAT:w" s="T691">Вокруг</ts>
                  <nts id="Seg_2883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2885" n="HIAT:w" s="T888">paʔi</ts>
                  <nts id="Seg_2886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2888" n="HIAT:w" s="T692">noʔ</ts>
                  <nts id="Seg_2889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2891" n="HIAT:w" s="T693">özerleʔbə</ts>
                  <nts id="Seg_2892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2894" n="HIAT:w" s="T694">золотой</ts>
                  <nts id="Seg_2895" n="HIAT:ip">.</nts>
                  <nts id="Seg_2896" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T698" id="Seg_2898" n="HIAT:u" s="T696">
                  <ts e="T697" id="Seg_2900" n="HIAT:w" s="T696">Dĭ</ts>
                  <nts id="Seg_2901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2903" n="HIAT:w" s="T697">müjö</ts>
                  <nts id="Seg_2904" n="HIAT:ip">.</nts>
                  <nts id="Seg_2905" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T699" id="Seg_2907" n="HIAT:u" s="T698">
                  <nts id="Seg_2908" n="HIAT:ip">(</nts>
                  <nts id="Seg_2909" n="HIAT:ip">(</nts>
                  <ats e="T699" id="Seg_2910" n="HIAT:non-pho" s="T698">BRK</ats>
                  <nts id="Seg_2911" n="HIAT:ip">)</nts>
                  <nts id="Seg_2912" n="HIAT:ip">)</nts>
                  <nts id="Seg_2913" n="HIAT:ip">.</nts>
                  <nts id="Seg_2914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T706" id="Seg_2916" n="HIAT:u" s="T699">
                  <ts e="T700" id="Seg_2918" n="HIAT:w" s="T699">Kömə</ts>
                  <nts id="Seg_2919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2921" n="HIAT:w" s="T700">poʔto</ts>
                  <nts id="Seg_2922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2924" n="HIAT:w" s="T701">iʔbolaʔpi</ts>
                  <nts id="Seg_2925" n="HIAT:ip">,</nts>
                  <nts id="Seg_2926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2928" n="HIAT:w" s="T702">dĭn</ts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2931" n="HIAT:w" s="T703">noʔ</ts>
                  <nts id="Seg_2932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2934" n="HIAT:w" s="T704">ej</ts>
                  <nts id="Seg_2935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2937" n="HIAT:w" s="T705">özerleʔbə</ts>
                  <nts id="Seg_2938" n="HIAT:ip">.</nts>
                  <nts id="Seg_2939" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T708" id="Seg_2941" n="HIAT:u" s="T706">
                  <ts e="T707" id="Seg_2943" n="HIAT:w" s="T706">Šü</ts>
                  <nts id="Seg_2944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2946" n="HIAT:w" s="T707">dĭ</ts>
                  <nts id="Seg_2947" n="HIAT:ip">.</nts>
                  <nts id="Seg_2948" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T709" id="Seg_2950" n="HIAT:u" s="T708">
                  <nts id="Seg_2951" n="HIAT:ip">(</nts>
                  <nts id="Seg_2952" n="HIAT:ip">(</nts>
                  <ats e="T709" id="Seg_2953" n="HIAT:non-pho" s="T708">BRK</ats>
                  <nts id="Seg_2954" n="HIAT:ip">)</nts>
                  <nts id="Seg_2955" n="HIAT:ip">)</nts>
                  <nts id="Seg_2956" n="HIAT:ip">.</nts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T721" id="Seg_2959" n="HIAT:u" s="T709">
                  <ts e="T710" id="Seg_2961" n="HIAT:w" s="T709">Kambi</ts>
                  <nts id="Seg_2962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2964" n="HIAT:w" s="T710">keʔbdejle</ts>
                  <nts id="Seg_2965" n="HIAT:ip">,</nts>
                  <nts id="Seg_2966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2968" n="HIAT:w" s="T711">i</ts>
                  <nts id="Seg_2969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2971" n="HIAT:w" s="T712">măn</ts>
                  <nts id="Seg_2972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2974" n="HIAT:w" s="T713">kambiam</ts>
                  <nts id="Seg_2975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2977" n="HIAT:w" s="T714">keʔbde</ts>
                  <nts id="Seg_2978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2980" n="HIAT:w" s="T715">nĭŋgəsʼtə</ts>
                  <nts id="Seg_2981" n="HIAT:ip">,</nts>
                  <nts id="Seg_2982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2984" n="HIAT:w" s="T716">i</ts>
                  <nts id="Seg_2985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2987" n="HIAT:w" s="T717">išo</ts>
                  <nts id="Seg_2988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2990" n="HIAT:w" s="T718">onʼiʔ</ts>
                  <nts id="Seg_2991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2993" n="HIAT:w" s="T719">koʔbdo</ts>
                  <nts id="Seg_2994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2996" n="HIAT:w" s="T720">kambi</ts>
                  <nts id="Seg_2997" n="HIAT:ip">.</nts>
                  <nts id="Seg_2998" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T728" id="Seg_3000" n="HIAT:u" s="T721">
                  <nts id="Seg_3001" n="HIAT:ip">(</nts>
                  <ts e="T722" id="Seg_3003" n="HIAT:w" s="T721">I-</ts>
                  <nts id="Seg_3004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_3006" n="HIAT:w" s="T722">ine-</ts>
                  <nts id="Seg_3007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_3009" n="HIAT:w" s="T723">inezi=</ts>
                  <nts id="Seg_3010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_3012" n="HIAT:w" s="T724">ine-</ts>
                  <nts id="Seg_3013" n="HIAT:ip">)</nts>
                  <nts id="Seg_3014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3015" n="HIAT:ip">(</nts>
                  <ts e="T726" id="Seg_3017" n="HIAT:w" s="T725">Kambila</ts>
                  <nts id="Seg_3018" n="HIAT:ip">)</nts>
                  <nts id="Seg_3019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_3021" n="HIAT:w" s="T726">Kambibaʔ</ts>
                  <nts id="Seg_3022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3023" n="HIAT:ip">(</nts>
                  <ts e="T728" id="Seg_3025" n="HIAT:w" s="T727">šumiganə</ts>
                  <nts id="Seg_3026" n="HIAT:ip">)</nts>
                  <nts id="Seg_3027" n="HIAT:ip">.</nts>
                  <nts id="Seg_3028" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T730" id="Seg_3030" n="HIAT:u" s="T728">
                  <ts e="T729" id="Seg_3032" n="HIAT:w" s="T728">Dĭbər</ts>
                  <nts id="Seg_3033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_3035" n="HIAT:w" s="T729">šobibaʔ</ts>
                  <nts id="Seg_3036" n="HIAT:ip">.</nts>
                  <nts id="Seg_3037" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T736" id="Seg_3039" n="HIAT:u" s="T730">
                  <nts id="Seg_3040" n="HIAT:ip">(</nts>
                  <ts e="T731" id="Seg_3042" n="HIAT:w" s="T730">Šalaʔjə</ts>
                  <nts id="Seg_3043" n="HIAT:ip">)</nts>
                  <nts id="Seg_3044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_3046" n="HIAT:w" s="T731">keʔbde</ts>
                  <nts id="Seg_3047" n="HIAT:ip">,</nts>
                  <nts id="Seg_3048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_3050" n="HIAT:w" s="T732">măndorbibaʔ</ts>
                  <nts id="Seg_3051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_3053" n="HIAT:w" s="T733">i</ts>
                  <nts id="Seg_3054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_3056" n="HIAT:w" s="T734">idʼiʔeʔe</ts>
                  <nts id="Seg_3057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_3059" n="HIAT:w" s="T735">oʔbdəbibaʔ</ts>
                  <nts id="Seg_3060" n="HIAT:ip">.</nts>
                  <nts id="Seg_3061" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T747" id="Seg_3063" n="HIAT:u" s="T736">
                  <ts e="T737" id="Seg_3065" n="HIAT:w" s="T736">Dĭgəttə</ts>
                  <nts id="Seg_3066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_3068" n="HIAT:w" s="T737">šobibaʔ</ts>
                  <nts id="Seg_3069" n="HIAT:ip">,</nts>
                  <nts id="Seg_3070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_3072" n="HIAT:w" s="T738">gijen</ts>
                  <nts id="Seg_3073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_3075" n="HIAT:w" s="T739">ine</ts>
                  <nts id="Seg_3076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_3078" n="HIAT:w" s="T740">mĭnleʔbə</ts>
                  <nts id="Seg_3079" n="HIAT:ip">,</nts>
                  <nts id="Seg_3080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_3082" n="HIAT:w" s="T741">iam</ts>
                  <nts id="Seg_3083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_3085" n="HIAT:w" s="T742">măndə:</ts>
                  <nts id="Seg_3086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_3088" n="HIAT:w" s="T743">Ugandə</ts>
                  <nts id="Seg_3089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_3091" n="HIAT:w" s="T744">sagər</ts>
                  <nts id="Seg_3092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_3094" n="HIAT:w" s="T745">туча</ts>
                  <nts id="Seg_3095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_3097" n="HIAT:w" s="T746">šonəga</ts>
                  <nts id="Seg_3098" n="HIAT:ip">.</nts>
                  <nts id="Seg_3099" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T755" id="Seg_3101" n="HIAT:u" s="T747">
                  <ts e="T748" id="Seg_3103" n="HIAT:w" s="T747">Nada</ts>
                  <nts id="Seg_3104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_3106" n="HIAT:w" s="T748">azittə</ts>
                  <nts id="Seg_3107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_3109" n="HIAT:w" s="T749">maʔ</ts>
                  <nts id="Seg_3110" n="HIAT:ip">,</nts>
                  <nts id="Seg_3111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750.tx-PKZ.1" id="Seg_3113" n="HIAT:w" s="T750">a</ts>
                  <nts id="Seg_3114" n="HIAT:ip">_</nts>
                  <ts e="T751" id="Seg_3116" n="HIAT:w" s="T750.tx-PKZ.1">to</ts>
                  <nts id="Seg_3117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_3119" n="HIAT:w" s="T751">kunolzittə</ts>
                  <nts id="Seg_3120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_3122" n="HIAT:w" s="T752">ej</ts>
                  <nts id="Seg_3123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_3125" n="HIAT:w" s="T753">jakše</ts>
                  <nts id="Seg_3126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_3128" n="HIAT:w" s="T754">moləj</ts>
                  <nts id="Seg_3129" n="HIAT:ip">.</nts>
                  <nts id="Seg_3130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T758" id="Seg_3132" n="HIAT:u" s="T755">
                  <ts e="T756" id="Seg_3134" n="HIAT:w" s="T755">Gəttə</ts>
                  <nts id="Seg_3135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_3137" n="HIAT:w" s="T756">maʔ</ts>
                  <nts id="Seg_3138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_3140" n="HIAT:w" s="T757">abibaʔ</ts>
                  <nts id="Seg_3141" n="HIAT:ip">.</nts>
                  <nts id="Seg_3142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T762" id="Seg_3144" n="HIAT:u" s="T758">
                  <ts e="T759" id="Seg_3146" n="HIAT:w" s="T758">Mĭnzərbibeʔ</ts>
                  <nts id="Seg_3147" n="HIAT:ip">,</nts>
                  <nts id="Seg_3148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_3150" n="HIAT:w" s="T759">segi</ts>
                  <nts id="Seg_3151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_3153" n="HIAT:w" s="T760">bü</ts>
                  <nts id="Seg_3154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_3156" n="HIAT:w" s="T761">bĭʔpibeʔ</ts>
                  <nts id="Seg_3157" n="HIAT:ip">.</nts>
                  <nts id="Seg_3158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T768" id="Seg_3160" n="HIAT:u" s="T762">
                  <ts e="T763" id="Seg_3162" n="HIAT:w" s="T762">A</ts>
                  <nts id="Seg_3163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_3165" n="HIAT:w" s="T763">dĭn</ts>
                  <nts id="Seg_3166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3167" n="HIAT:ip">(</nts>
                  <ts e="T765" id="Seg_3169" n="HIAT:w" s="T764">был=</ts>
                  <nts id="Seg_3170" n="HIAT:ip">)</nts>
                  <nts id="Seg_3171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_3173" n="HIAT:w" s="T765">ibi</ts>
                  <nts id="Seg_3174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_3176" n="HIAT:w" s="T766">pălăvik</ts>
                  <nts id="Seg_3177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_3179" n="HIAT:w" s="T767">tĭbər</ts>
                  <nts id="Seg_3180" n="HIAT:ip">.</nts>
                  <nts id="Seg_3181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T771" id="Seg_3183" n="HIAT:u" s="T768">
                  <ts e="T769" id="Seg_3185" n="HIAT:w" s="T768">Pălăviktə</ts>
                  <nts id="Seg_3186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3187" n="HIAT:ip">(</nts>
                  <ts e="T770" id="Seg_3189" n="HIAT:w" s="T769">edəbibiʔibeʔ</ts>
                  <nts id="Seg_3190" n="HIAT:ip">)</nts>
                  <nts id="Seg_3191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_3193" n="HIAT:w" s="T770">paʔinə</ts>
                  <nts id="Seg_3194" n="HIAT:ip">.</nts>
                  <nts id="Seg_3195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T774" id="Seg_3197" n="HIAT:u" s="T771">
                  <ts e="T772" id="Seg_3199" n="HIAT:w" s="T771">Dĭgəttə</ts>
                  <nts id="Seg_3200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_3202" n="HIAT:w" s="T772">iʔbəbeʔ</ts>
                  <nts id="Seg_3203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_3205" n="HIAT:w" s="T773">kunolzittə</ts>
                  <nts id="Seg_3206" n="HIAT:ip">.</nts>
                  <nts id="Seg_3207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T782" id="Seg_3209" n="HIAT:u" s="T774">
                  <ts e="T775" id="Seg_3211" n="HIAT:w" s="T774">A</ts>
                  <nts id="Seg_3212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_3214" n="HIAT:w" s="T775">nüdʼin</ts>
                  <nts id="Seg_3215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_3217" n="HIAT:w" s="T776">bar</ts>
                  <nts id="Seg_3218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_3220" n="HIAT:w" s="T777">ugandə</ts>
                  <nts id="Seg_3221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_3223" n="HIAT:w" s="T778">tăŋ</ts>
                  <nts id="Seg_3224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_3226" n="HIAT:w" s="T779">surno</ts>
                  <nts id="Seg_3227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3229" n="HIAT:w" s="T780">šonəbi</ts>
                  <nts id="Seg_3230" n="HIAT:ip">,</nts>
                  <nts id="Seg_3231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_3233" n="HIAT:w" s="T781">šonəbi</ts>
                  <nts id="Seg_3234" n="HIAT:ip">.</nts>
                  <nts id="Seg_3235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T786" id="Seg_3237" n="HIAT:u" s="T782">
                  <ts e="T783" id="Seg_3239" n="HIAT:w" s="T782">Ertən</ts>
                  <nts id="Seg_3240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3242" n="HIAT:w" s="T783">uʔbdəbibaʔ</ts>
                  <nts id="Seg_3243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_3245" n="HIAT:w" s="T784">bar</ts>
                  <nts id="Seg_3246" n="HIAT:ip">,</nts>
                  <nts id="Seg_3247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_3249" n="HIAT:w" s="T785">nünörbibaʔ</ts>
                  <nts id="Seg_3250" n="HIAT:ip">.</nts>
                  <nts id="Seg_3251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T790" id="Seg_3253" n="HIAT:u" s="T786">
                  <ts e="T787" id="Seg_3255" n="HIAT:w" s="T786">Ineʔi</ts>
                  <nts id="Seg_3256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_3258" n="HIAT:w" s="T787">dʼaʔpibaʔ</ts>
                  <nts id="Seg_3259" n="HIAT:ip">,</nts>
                  <nts id="Seg_3260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_3262" n="HIAT:w" s="T788">kambibaʔ</ts>
                  <nts id="Seg_3263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_3265" n="HIAT:w" s="T789">maʔnʼi</ts>
                  <nts id="Seg_3266" n="HIAT:ip">.</nts>
                  <nts id="Seg_3267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T793" id="Seg_3269" n="HIAT:u" s="T790">
                  <nts id="Seg_3270" n="HIAT:ip">(</nts>
                  <ts e="T791" id="Seg_3272" n="HIAT:w" s="T790">Šobi-</ts>
                  <nts id="Seg_3273" n="HIAT:ip">)</nts>
                  <nts id="Seg_3274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3276" n="HIAT:w" s="T791">Šobibaʔ</ts>
                  <nts id="Seg_3277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_3279" n="HIAT:w" s="T792">bünə</ts>
                  <nts id="Seg_3280" n="HIAT:ip">.</nts>
                  <nts id="Seg_3281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T796" id="Seg_3283" n="HIAT:u" s="T793">
                  <ts e="T794" id="Seg_3285" n="HIAT:w" s="T793">Kădedə</ts>
                  <nts id="Seg_3286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3288" n="HIAT:w" s="T794">nʼelʼzʼa</ts>
                  <nts id="Seg_3289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3291" n="HIAT:w" s="T795">kanzittə</ts>
                  <nts id="Seg_3292" n="HIAT:ip">.</nts>
                  <nts id="Seg_3293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T805" id="Seg_3295" n="HIAT:u" s="T796">
                  <ts e="T797" id="Seg_3297" n="HIAT:w" s="T796">Dĭgəttə</ts>
                  <nts id="Seg_3298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3299" n="HIAT:ip">(</nts>
                  <ts e="T798" id="Seg_3301" n="HIAT:w" s="T797">nʼ-</ts>
                  <nts id="Seg_3302" n="HIAT:ip">)</nts>
                  <nts id="Seg_3303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3305" n="HIAT:w" s="T798">вниз</ts>
                  <nts id="Seg_3306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_3308" n="HIAT:w" s="T799">bünə</ts>
                  <nts id="Seg_3309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3311" n="HIAT:w" s="T800">kanbibaʔ</ts>
                  <nts id="Seg_3312" n="HIAT:ip">,</nts>
                  <nts id="Seg_3313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_3315" n="HIAT:w" s="T801">kanbibaʔ</ts>
                  <nts id="Seg_3316" n="HIAT:ip">,</nts>
                  <nts id="Seg_3317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_3319" n="HIAT:w" s="T802">dĭgəttə</ts>
                  <nts id="Seg_3320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3322" n="HIAT:w" s="T803">kubibaʔ</ts>
                  <nts id="Seg_3323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3325" n="HIAT:w" s="T804">mostə</ts>
                  <nts id="Seg_3326" n="HIAT:ip">.</nts>
                  <nts id="Seg_3327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T811" id="Seg_3329" n="HIAT:u" s="T805">
                  <ts e="T806" id="Seg_3331" n="HIAT:w" s="T805">Mostə</ts>
                  <nts id="Seg_3332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3333" n="HIAT:ip">(</nts>
                  <ts e="T807" id="Seg_3335" n="HIAT:w" s="T806">перей-</ts>
                  <nts id="Seg_3336" n="HIAT:ip">)</nts>
                  <nts id="Seg_3337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3339" n="HIAT:w" s="T807">šobibaʔ</ts>
                  <nts id="Seg_3340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3342" n="HIAT:w" s="T808">i</ts>
                  <nts id="Seg_3343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3345" n="HIAT:w" s="T809">maʔnʼi</ts>
                  <nts id="Seg_3346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3348" n="HIAT:w" s="T810">šobibaʔ</ts>
                  <nts id="Seg_3349" n="HIAT:ip">.</nts>
                  <nts id="Seg_3350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T812" id="Seg_3352" n="HIAT:u" s="T811">
                  <ts e="T812" id="Seg_3354" n="HIAT:w" s="T811">Все</ts>
                  <nts id="Seg_3355" n="HIAT:ip">.</nts>
                  <nts id="Seg_3356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T813" id="Seg_3358" n="HIAT:u" s="T812">
                  <nts id="Seg_3359" n="HIAT:ip">(</nts>
                  <nts id="Seg_3360" n="HIAT:ip">(</nts>
                  <ats e="T813" id="Seg_3361" n="HIAT:non-pho" s="T812">BRK</ats>
                  <nts id="Seg_3362" n="HIAT:ip">)</nts>
                  <nts id="Seg_3363" n="HIAT:ip">)</nts>
                  <nts id="Seg_3364" n="HIAT:ip">.</nts>
                  <nts id="Seg_3365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T817" id="Seg_3367" n="HIAT:u" s="T813">
                  <ts e="T814" id="Seg_3369" n="HIAT:w" s="T813">Măn</ts>
                  <nts id="Seg_3370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_3372" n="HIAT:w" s="T814">teinen</ts>
                  <nts id="Seg_3373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3375" n="HIAT:w" s="T815">ertə</ts>
                  <nts id="Seg_3376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_3378" n="HIAT:w" s="T816">uʔbdəbiam</ts>
                  <nts id="Seg_3379" n="HIAT:ip">.</nts>
                  <nts id="Seg_3380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T819" id="Seg_3382" n="HIAT:u" s="T817">
                  <ts e="T818" id="Seg_3384" n="HIAT:w" s="T817">Măgăzində</ts>
                  <nts id="Seg_3385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3387" n="HIAT:w" s="T818">kambiam</ts>
                  <nts id="Seg_3388" n="HIAT:ip">.</nts>
                  <nts id="Seg_3389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T821" id="Seg_3391" n="HIAT:u" s="T819">
                  <ts e="T889" id="Seg_3393" n="HIAT:w" s="T819">Очередь</ts>
                  <nts id="Seg_3394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3396" n="HIAT:w" s="T889">nubiam</ts>
                  <nts id="Seg_3397" n="HIAT:ip">.</nts>
                  <nts id="Seg_3398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T832" id="Seg_3400" n="HIAT:u" s="T821">
                  <ts e="T822" id="Seg_3402" n="HIAT:w" s="T821">Dĭgəttə</ts>
                  <nts id="Seg_3403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3404" n="HIAT:ip">(</nts>
                  <ts e="T823" id="Seg_3406" n="HIAT:w" s="T822">šide=</ts>
                  <nts id="Seg_3407" n="HIAT:ip">)</nts>
                  <nts id="Seg_3408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_3410" n="HIAT:w" s="T823">šide</ts>
                  <nts id="Seg_3411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3413" n="HIAT:w" s="T824">čas</ts>
                  <nts id="Seg_3414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3416" n="HIAT:w" s="T825">nubiam</ts>
                  <nts id="Seg_3417" n="HIAT:ip">,</nts>
                  <nts id="Seg_3418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3420" n="HIAT:w" s="T826">dĭgəttə</ts>
                  <nts id="Seg_3421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3423" n="HIAT:w" s="T827">ibiem</ts>
                  <nts id="Seg_3424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3426" n="HIAT:w" s="T828">kola</ts>
                  <nts id="Seg_3427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3429" n="HIAT:w" s="T829">i</ts>
                  <nts id="Seg_3430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_3432" n="HIAT:w" s="T830">šobiam</ts>
                  <nts id="Seg_3433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3435" n="HIAT:w" s="T831">maʔnə</ts>
                  <nts id="Seg_3436" n="HIAT:ip">.</nts>
                  <nts id="Seg_3437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T835" id="Seg_3439" n="HIAT:u" s="T832">
                  <ts e="T833" id="Seg_3441" n="HIAT:w" s="T832">I</ts>
                  <nts id="Seg_3442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3444" n="HIAT:w" s="T833">šiʔ</ts>
                  <nts id="Seg_3445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3447" n="HIAT:w" s="T834">šobilaʔ</ts>
                  <nts id="Seg_3448" n="HIAT:ip">.</nts>
                  <nts id="Seg_3449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T836" id="Seg_3451" n="HIAT:u" s="T835">
                  <nts id="Seg_3452" n="HIAT:ip">(</nts>
                  <nts id="Seg_3453" n="HIAT:ip">(</nts>
                  <ats e="T836" id="Seg_3454" n="HIAT:non-pho" s="T835">BRK</ats>
                  <nts id="Seg_3455" n="HIAT:ip">)</nts>
                  <nts id="Seg_3456" n="HIAT:ip">)</nts>
                  <nts id="Seg_3457" n="HIAT:ip">.</nts>
                  <nts id="Seg_3458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T845" id="Seg_3460" n="HIAT:u" s="T836">
                  <ts e="T837" id="Seg_3462" n="HIAT:w" s="T836">Gəttə</ts>
                  <nts id="Seg_3463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3465" n="HIAT:w" s="T837">măn</ts>
                  <nts id="Seg_3466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3468" n="HIAT:w" s="T838">ibiem</ts>
                  <nts id="Seg_3469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3471" n="HIAT:w" s="T839">oʔb</ts>
                  <nts id="Seg_3472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_3474" n="HIAT:w" s="T840">kilo</ts>
                  <nts id="Seg_3475" n="HIAT:ip">,</nts>
                  <nts id="Seg_3476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3478" n="HIAT:w" s="T841">aktʼa</ts>
                  <nts id="Seg_3479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3481" n="HIAT:w" s="T842">mĭbiem</ts>
                  <nts id="Seg_3482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3484" n="HIAT:w" s="T843">onʼiʔ</ts>
                  <nts id="Seg_3485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3487" n="HIAT:w" s="T844">šălkovej</ts>
                  <nts id="Seg_3488" n="HIAT:ip">.</nts>
                  <nts id="Seg_3489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T848" id="Seg_3491" n="HIAT:u" s="T845">
                  <ts e="T846" id="Seg_3493" n="HIAT:w" s="T845">Bʼeʔ</ts>
                  <nts id="Seg_3494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3496" n="HIAT:w" s="T846">teʔtə</ts>
                  <nts id="Seg_3497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3499" n="HIAT:w" s="T847">mĭbiem</ts>
                  <nts id="Seg_3500" n="HIAT:ip">.</nts>
                  <nts id="Seg_3501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T852" id="Seg_3503" n="HIAT:u" s="T848">
                  <ts e="T849" id="Seg_3505" n="HIAT:w" s="T848">Onʼiʔ</ts>
                  <nts id="Seg_3506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3508" n="HIAT:w" s="T849">šălkovej</ts>
                  <nts id="Seg_3509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3511" n="HIAT:w" s="T850">bʼeʔ</ts>
                  <nts id="Seg_3512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3514" n="HIAT:w" s="T851">teʔtə</ts>
                  <nts id="Seg_3515" n="HIAT:ip">.</nts>
                  <nts id="Seg_3516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T853" id="Seg_3518" n="HIAT:u" s="T852">
                  <nts id="Seg_3519" n="HIAT:ip">(</nts>
                  <nts id="Seg_3520" n="HIAT:ip">(</nts>
                  <ats e="T853" id="Seg_3521" n="HIAT:non-pho" s="T852">BRK</ats>
                  <nts id="Seg_3522" n="HIAT:ip">)</nts>
                  <nts id="Seg_3523" n="HIAT:ip">)</nts>
                  <nts id="Seg_3524" n="HIAT:ip">.</nts>
                  <nts id="Seg_3525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T858" id="Seg_3527" n="HIAT:u" s="T853">
                  <ts e="T854" id="Seg_3529" n="HIAT:w" s="T853">Dĭn</ts>
                  <nts id="Seg_3530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_3532" n="HIAT:w" s="T854">bar</ts>
                  <nts id="Seg_3533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3535" n="HIAT:w" s="T855">nezeŋ</ts>
                  <nts id="Seg_3536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_3538" n="HIAT:w" s="T856">kudonzlaʔbəʔjə</ts>
                  <nts id="Seg_3539" n="HIAT:ip">,</nts>
                  <nts id="Seg_3540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3542" n="HIAT:w" s="T857">kirgarlaʔbəʔjə</ts>
                  <nts id="Seg_3543" n="HIAT:ip">.</nts>
                  <nts id="Seg_3544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T861" id="Seg_3546" n="HIAT:u" s="T858">
                  <ts e="T859" id="Seg_3548" n="HIAT:w" s="T858">Mănliaʔi:</ts>
                  <nts id="Seg_3549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3551" n="HIAT:w" s="T859">Amga</ts>
                  <nts id="Seg_3552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3554" n="HIAT:w" s="T860">kola</ts>
                  <nts id="Seg_3555" n="HIAT:ip">.</nts>
                  <nts id="Seg_3556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T866" id="Seg_3558" n="HIAT:u" s="T861">
                  <nts id="Seg_3559" n="HIAT:ip">(</nts>
                  <ts e="T862" id="Seg_3561" n="HIAT:w" s="T861">Onʼiʔ</ts>
                  <nts id="Seg_3562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_3564" n="HIAT:w" s="T862">k-</ts>
                  <nts id="Seg_3565" n="HIAT:ip">)</nts>
                  <nts id="Seg_3566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3568" n="HIAT:w" s="T863">Onʼiʔ</ts>
                  <nts id="Seg_3569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3571" n="HIAT:w" s="T864">kilo</ts>
                  <nts id="Seg_3572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3574" n="HIAT:w" s="T865">mĭleʔbə</ts>
                  <nts id="Seg_3575" n="HIAT:ip">.</nts>
                  <nts id="Seg_3576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T874" id="Seg_3578" n="HIAT:u" s="T866">
                  <ts e="T867" id="Seg_3580" n="HIAT:w" s="T866">Dĭ</ts>
                  <nts id="Seg_3581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_3583" n="HIAT:w" s="T867">mĭbi</ts>
                  <nts id="Seg_3584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_3586" n="HIAT:w" s="T868">onʼiʔ</ts>
                  <nts id="Seg_3587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3589" n="HIAT:w" s="T869">bar</ts>
                  <nts id="Seg_3590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_3592" n="HIAT:w" s="T870">nezeŋdə</ts>
                  <nts id="Seg_3593" n="HIAT:ip">,</nts>
                  <nts id="Seg_3594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3596" n="HIAT:w" s="T871">onʼiʔ</ts>
                  <nts id="Seg_3597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3599" n="HIAT:w" s="T872">kilo</ts>
                  <nts id="Seg_3600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3602" n="HIAT:w" s="T873">mĭbi</ts>
                  <nts id="Seg_3603" n="HIAT:ip">.</nts>
                  <nts id="Seg_3604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T877" id="Seg_3606" n="HIAT:u" s="T874">
                  <ts e="T875" id="Seg_3608" n="HIAT:w" s="T874">Mănliaʔi:</ts>
                  <nts id="Seg_3609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3611" n="HIAT:w" s="T875">Esseŋbə</ts>
                  <nts id="Seg_3612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_3614" n="HIAT:w" s="T876">kirgarlaʔbəʔjə</ts>
                  <nts id="Seg_3615" n="HIAT:ip">.</nts>
                  <nts id="Seg_3616" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T882" id="Seg_3618" n="HIAT:u" s="T877">
                  <nts id="Seg_3619" n="HIAT:ip">(</nts>
                  <ts e="T878" id="Seg_3621" n="HIAT:w" s="T877">Kola=</ts>
                  <nts id="Seg_3622" n="HIAT:ip">)</nts>
                  <nts id="Seg_3623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3625" n="HIAT:w" s="T878">Kola</ts>
                  <nts id="Seg_3626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_3628" n="HIAT:w" s="T879">amzittə</ts>
                  <nts id="Seg_3629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_3631" n="HIAT:w" s="T880">kereʔ</ts>
                  <nts id="Seg_3632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3634" n="HIAT:w" s="T881">dĭzeŋdə</ts>
                  <nts id="Seg_3635" n="HIAT:ip">.</nts>
                  <nts id="Seg_3636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T364" id="Seg_3637" n="sc" s="T0">
               <ts e="T1" id="Seg_3639" n="e" s="T0">Dĭgəttə </ts>
               <ts e="T2" id="Seg_3641" n="e" s="T1">dĭ </ts>
               <ts e="T3" id="Seg_3643" n="e" s="T2">nʼizeŋ </ts>
               <ts e="T4" id="Seg_3645" n="e" s="T3">aktʼinə </ts>
               <ts e="T5" id="Seg_3647" n="e" s="T4">kambiʔi. </ts>
               <ts e="T6" id="Seg_3649" n="e" s="T5">Šobiʔi, </ts>
               <ts e="T7" id="Seg_3651" n="e" s="T6">šobiʔi, </ts>
               <ts e="T8" id="Seg_3653" n="e" s="T7">dĭgəttə </ts>
               <ts e="T9" id="Seg_3655" n="e" s="T8">(aʔtʼi=) </ts>
               <ts e="T10" id="Seg_3657" n="e" s="T9">aʔtʼigən </ts>
               <ts e="T11" id="Seg_3659" n="e" s="T10">toʔndə </ts>
               <ts e="T12" id="Seg_3661" n="e" s="T11">amnobiʔi. </ts>
               <ts e="T13" id="Seg_3663" n="e" s="T12">Kuliaʔi: </ts>
               <ts e="T14" id="Seg_3665" n="e" s="T13">šonəga </ts>
               <ts e="T15" id="Seg_3667" n="e" s="T14">kăldun </ts>
               <ts e="T16" id="Seg_3669" n="e" s="T15">onʼiʔ, </ts>
               <ts e="T17" id="Seg_3671" n="e" s="T16">ine </ts>
               <ts e="T18" id="Seg_3673" n="e" s="T17">kömə. </ts>
               <ts e="T19" id="Seg_3675" n="e" s="T18">Dĭgəttə </ts>
               <ts e="T20" id="Seg_3677" n="e" s="T19">dĭ </ts>
               <ts e="T21" id="Seg_3679" n="e" s="T20">bar </ts>
               <ts e="T23" id="Seg_3681" n="e" s="T21">tăŋ… </ts>
               <ts e="T24" id="Seg_3683" n="e" s="T23">Dĭgəttə </ts>
               <ts e="T25" id="Seg_3685" n="e" s="T24">onʼiʔ </ts>
               <ts e="T26" id="Seg_3687" n="e" s="T25">свистнул </ts>
               <ts e="T27" id="Seg_3689" n="e" s="T26">tăŋ. </ts>
               <ts e="T28" id="Seg_3691" n="e" s="T27">Inet </ts>
               <ts e="T29" id="Seg_3693" n="e" s="T28">bar </ts>
               <ts e="T30" id="Seg_3695" n="e" s="T29">(s-) </ts>
               <ts e="T31" id="Seg_3697" n="e" s="T30">kălʼenkanə </ts>
               <ts e="T32" id="Seg_3699" n="e" s="T31">(уп-). </ts>
               <ts e="T33" id="Seg_3701" n="e" s="T32">Dĭ </ts>
               <ts e="T34" id="Seg_3703" n="e" s="T33">ine </ts>
               <ts e="T35" id="Seg_3705" n="e" s="T34">saʔməluʔbi </ts>
               <ts e="T36" id="Seg_3707" n="e" s="T35">kălʼenkaʔinə. </ts>
               <ts e="T37" id="Seg_3709" n="e" s="T36">Dĭgəttə </ts>
               <ts e="T38" id="Seg_3711" n="e" s="T37">dĭ </ts>
               <ts e="T39" id="Seg_3713" n="e" s="T38">găldun </ts>
               <ts e="T40" id="Seg_3715" n="e" s="T39">(măndəbi): </ts>
               <ts e="T41" id="Seg_3717" n="e" s="T40">Dĭ </ts>
               <ts e="T42" id="Seg_3719" n="e" s="T41">dĭn </ts>
               <ts e="T43" id="Seg_3721" n="e" s="T42">(e-) </ts>
               <ts e="T44" id="Seg_3723" n="e" s="T43">enəidənə </ts>
               <ts e="T45" id="Seg_3725" n="e" s="T44">amnolaʔbə. </ts>
               <ts e="T46" id="Seg_3727" n="e" s="T45">Ugaːndə </ts>
               <ts e="T47" id="Seg_3729" n="e" s="T46">pimniem! </ts>
               <ts e="T48" id="Seg_3731" n="e" s="T47">Dĭgəttə </ts>
               <ts e="T49" id="Seg_3733" n="e" s="T48">kambi. </ts>
               <ts e="T50" id="Seg_3735" n="e" s="T49">Amnolaʔbəʔjə, </ts>
               <ts e="T51" id="Seg_3737" n="e" s="T50">amnolaʔbəʔjə, </ts>
               <ts e="T52" id="Seg_3739" n="e" s="T51">bazoʔ </ts>
               <ts e="T53" id="Seg_3741" n="e" s="T52">šonəga. </ts>
               <ts e="T54" id="Seg_3743" n="e" s="T53">Sagər </ts>
               <ts e="T55" id="Seg_3745" n="e" s="T54">inetsiʔ </ts>
               <ts e="T56" id="Seg_3747" n="e" s="T55">(oʔ-) </ts>
               <ts e="T57" id="Seg_3749" n="e" s="T56">(baška) </ts>
               <ts e="T58" id="Seg_3751" n="e" s="T57">(šide=) </ts>
               <ts e="T59" id="Seg_3753" n="e" s="T58">šidegit </ts>
               <ts e="T60" id="Seg_3755" n="e" s="T59">găldun. </ts>
               <ts e="T61" id="Seg_3757" n="e" s="T60">Dĭgəttə </ts>
               <ts e="T62" id="Seg_3759" n="e" s="T61">dĭzeŋ </ts>
               <ts e="T63" id="Seg_3761" n="e" s="T62">bazoʔ </ts>
               <ts e="T64" id="Seg_3763" n="e" s="T63">свистнул, </ts>
               <ts e="T65" id="Seg_3765" n="e" s="T64">gəttə </ts>
               <ts e="T66" id="Seg_3767" n="e" s="T65">dĭ </ts>
               <ts e="T67" id="Seg_3769" n="e" s="T66">saʔməluʔbi </ts>
               <ts e="T68" id="Seg_3771" n="e" s="T67">kălʼenkaʔinə. </ts>
               <ts e="T69" id="Seg_3773" n="e" s="T68">Tĭ </ts>
               <ts e="T70" id="Seg_3775" n="e" s="T69">tože </ts>
               <ts e="T71" id="Seg_3777" n="e" s="T70">măndə: </ts>
               <ts e="T72" id="Seg_3779" n="e" s="T71">Enəidənə </ts>
               <ts e="T73" id="Seg_3781" n="e" s="T72">ugandə, </ts>
               <ts e="T74" id="Seg_3783" n="e" s="T73">kuštü. </ts>
               <ts e="T75" id="Seg_3785" n="e" s="T74">Ugandə </ts>
               <ts e="T76" id="Seg_3787" n="e" s="T75">măn </ts>
               <ts e="T77" id="Seg_3789" n="e" s="T76">pimniem. </ts>
               <ts e="T78" id="Seg_3791" n="e" s="T77">Dĭgəttə </ts>
               <ts e="T79" id="Seg_3793" n="e" s="T78">(u-) </ts>
               <ts e="T80" id="Seg_3795" n="e" s="T79">(ine=), </ts>
               <ts e="T81" id="Seg_3797" n="e" s="T80">inet </ts>
               <ts e="T82" id="Seg_3799" n="e" s="T81">uʔbdəbi, </ts>
               <ts e="T83" id="Seg_3801" n="e" s="T82">kambi. </ts>
               <ts e="T84" id="Seg_3803" n="e" s="T83">Gəttə </ts>
               <ts e="T85" id="Seg_3805" n="e" s="T84">amnolaʔbəʔjə, </ts>
               <ts e="T86" id="Seg_3807" n="e" s="T85">amnolaʔbəʔjə. </ts>
               <ts e="T87" id="Seg_3809" n="e" s="T86">(Na-) </ts>
               <ts e="T88" id="Seg_3811" n="e" s="T87">Nagurgət </ts>
               <ts e="T89" id="Seg_3813" n="e" s="T88">ine </ts>
               <ts e="T90" id="Seg_3815" n="e" s="T89">šonəga, </ts>
               <ts e="T91" id="Seg_3817" n="e" s="T90">dö </ts>
               <ts e="T92" id="Seg_3819" n="e" s="T91">ine </ts>
               <ts e="T93" id="Seg_3821" n="e" s="T92">sĭri. </ts>
               <ts e="T94" id="Seg_3823" n="e" s="T93">Dĭzeŋ </ts>
               <ts e="T95" id="Seg_3825" n="e" s="T94">bazoʔ </ts>
               <ts e="T96" id="Seg_3827" n="e" s="T95">свистнули. </ts>
               <ts e="T97" id="Seg_3829" n="e" s="T96">Dĭgəttə </ts>
               <ts e="T98" id="Seg_3831" n="e" s="T97">dĭ </ts>
               <ts e="T99" id="Seg_3833" n="e" s="T98">ine </ts>
               <ts e="T100" id="Seg_3835" n="e" s="T99">nuldəbi, </ts>
               <ts e="T101" id="Seg_3837" n="e" s="T100">nulaʔbə. </ts>
               <ts e="T102" id="Seg_3839" n="e" s="T101">Dĭ </ts>
               <ts e="T103" id="Seg_3841" n="e" s="T102">kăldun </ts>
               <ts e="T104" id="Seg_3843" n="e" s="T103">(s-) </ts>
               <ts e="T105" id="Seg_3845" n="e" s="T104">kambi </ts>
               <ts e="T106" id="Seg_3847" n="e" s="T105">dĭzeŋdə. </ts>
               <ts e="T107" id="Seg_3849" n="e" s="T106">Girgit </ts>
               <ts e="T108" id="Seg_3851" n="e" s="T107">esseŋ </ts>
               <ts e="T109" id="Seg_3853" n="e" s="T108">amnolaʔbəʔjə? </ts>
               <ts e="T110" id="Seg_3855" n="e" s="T109">Da </ts>
               <ts e="T111" id="Seg_3857" n="e" s="T110">miʔ </ts>
               <ts e="T112" id="Seg_3859" n="e" s="T111">ot, </ts>
               <ts e="T113" id="Seg_3861" n="e" s="T112">miʔnʼibeʔ </ts>
               <ts e="T114" id="Seg_3863" n="e" s="T113">bar </ts>
               <ts e="T115" id="Seg_3865" n="e" s="T114">bünə </ts>
               <ts e="T116" id="Seg_3867" n="e" s="T115">barəʔluʔpiʔi. </ts>
               <ts e="T117" id="Seg_3869" n="e" s="T116">(Măn=) </ts>
               <ts e="T118" id="Seg_3871" n="e" s="T117">Miʔ </ts>
               <ts e="T119" id="Seg_3873" n="e" s="T118">döbər </ts>
               <ts e="T120" id="Seg_3875" n="e" s="T119">šobibaʔ. </ts>
               <ts e="T121" id="Seg_3877" n="e" s="T120">Dĭgəttə: </ts>
               <ts e="T122" id="Seg_3879" n="e" s="T121">No, </ts>
               <ts e="T123" id="Seg_3881" n="e" s="T122">amnogaʔ </ts>
               <ts e="T124" id="Seg_3883" n="e" s="T123">dön. </ts>
               <ts e="T125" id="Seg_3885" n="e" s="T124">Dĭ </ts>
               <ts e="T126" id="Seg_3887" n="e" s="T125">dĭʔnə </ts>
               <ts e="T127" id="Seg_3889" n="e" s="T126">(abiʔi </ts>
               <ts e="T128" id="Seg_3891" n="e" s="T127">=)… </ts>
               <ts e="T129" id="Seg_3893" n="e" s="T128">Abi </ts>
               <ts e="T130" id="Seg_3895" n="e" s="T129">maʔ, </ts>
               <ts e="T131" id="Seg_3897" n="e" s="T130">ipek </ts>
               <ts e="T132" id="Seg_3899" n="e" s="T131">mĭbi, </ts>
               <ts e="T133" id="Seg_3901" n="e" s="T132">uja </ts>
               <ts e="T134" id="Seg_3903" n="e" s="T133">mĭbi. </ts>
               <ts e="T135" id="Seg_3905" n="e" s="T134">Amnogaʔ </ts>
               <ts e="T136" id="Seg_3907" n="e" s="T135">dön, </ts>
               <ts e="T137" id="Seg_3909" n="e" s="T136">măn </ts>
               <ts e="T138" id="Seg_3911" n="e" s="T137">parlam, </ts>
               <ts e="T139" id="Seg_3913" n="e" s="T138">dʼili </ts>
               <ts e="T140" id="Seg_3915" n="e" s="T139">molam, </ts>
               <ts e="T141" id="Seg_3917" n="e" s="T140">šiʔnʼileʔ </ts>
               <ts e="T142" id="Seg_3919" n="e" s="T141">ilim. </ts>
               <ts e="T143" id="Seg_3921" n="e" s="T142">Dĭgəttə </ts>
               <ts e="T144" id="Seg_3923" n="e" s="T143">dĭzeŋ </ts>
               <ts e="T145" id="Seg_3925" n="e" s="T144">amnolaʔpiʔi, </ts>
               <ts e="T146" id="Seg_3927" n="e" s="T145">amnolaʔpi. </ts>
               <ts e="T147" id="Seg_3929" n="e" s="T146">Nagur </ts>
               <ts e="T148" id="Seg_3931" n="e" s="T147">dʼala </ts>
               <ts e="T149" id="Seg_3933" n="e" s="T148">onʼiʔ </ts>
               <ts e="T150" id="Seg_3935" n="e" s="T149">ine </ts>
               <ts e="T151" id="Seg_3937" n="e" s="T150">šonəga, </ts>
               <ts e="T152" id="Seg_3939" n="e" s="T151">kömə. </ts>
               <ts e="T153" id="Seg_3941" n="e" s="T152">Dĭgəttə </ts>
               <ts e="T154" id="Seg_3943" n="e" s="T153">naga </ts>
               <ts e="T155" id="Seg_3945" n="e" s="T154">dĭn </ts>
               <ts e="T156" id="Seg_3947" n="e" s="T155">kuza. </ts>
               <ts e="T157" id="Seg_3949" n="e" s="T156">Dĭgəttə </ts>
               <ts e="T158" id="Seg_3951" n="e" s="T157">sagər </ts>
               <ts e="T159" id="Seg_3953" n="e" s="T158">ine </ts>
               <ts e="T160" id="Seg_3955" n="e" s="T159">šonəga. </ts>
               <ts e="T161" id="Seg_3957" n="e" s="T160">Kuza </ts>
               <ts e="T162" id="Seg_3959" n="e" s="T161">tože </ts>
               <ts e="T163" id="Seg_3961" n="e" s="T162">naga. </ts>
               <ts e="T164" id="Seg_3963" n="e" s="T163">Amnolaʔbəʔi, </ts>
               <ts e="T165" id="Seg_3965" n="e" s="T164">amnolaʔbəʔi, </ts>
               <ts e="T166" id="Seg_3967" n="e" s="T165">dĭgəttə </ts>
               <ts e="T167" id="Seg_3969" n="e" s="T166">sĭre </ts>
               <ts e="T168" id="Seg_3971" n="e" s="T167">ine </ts>
               <ts e="T169" id="Seg_3973" n="e" s="T168">šonəga. </ts>
               <ts e="T170" id="Seg_3975" n="e" s="T169">I </ts>
               <ts e="T171" id="Seg_3977" n="e" s="T170">kuza </ts>
               <ts e="T172" id="Seg_3979" n="e" s="T171">amnolaʔbə. </ts>
               <ts e="T173" id="Seg_3981" n="e" s="T172">No, </ts>
               <ts e="T174" id="Seg_3983" n="e" s="T173">kanžəbaʔ </ts>
               <ts e="T175" id="Seg_3985" n="e" s="T174">mănziʔ, </ts>
               <ts e="T176" id="Seg_3987" n="e" s="T175">maʔnəl </ts>
               <ts e="T177" id="Seg_3989" n="e" s="T176">možet </ts>
               <ts e="T178" id="Seg_3991" n="e" s="T177">kallal </ts>
               <ts e="T179" id="Seg_3993" n="e" s="T178">alʼi </ts>
               <ts e="T180" id="Seg_3995" n="e" s="T179">măna </ts>
               <ts e="T181" id="Seg_3997" n="e" s="T180">amnolal. </ts>
               <ts e="T182" id="Seg_3999" n="e" s="T181">Šobiʔi </ts>
               <ts e="T183" id="Seg_4001" n="e" s="T182">dĭ </ts>
               <ts e="T184" id="Seg_4003" n="e" s="T183">kuzanə. </ts>
               <ts e="T185" id="Seg_4005" n="e" s="T184">Onʼiʔ </ts>
               <ts e="T186" id="Seg_4007" n="e" s="T185">kambi </ts>
               <ts e="T187" id="Seg_4009" n="e" s="T186">maːʔndə. </ts>
               <ts e="T188" id="Seg_4011" n="e" s="T187">Šobi </ts>
               <ts e="T189" id="Seg_4013" n="e" s="T188">maːʔndə, </ts>
               <ts e="T190" id="Seg_4015" n="e" s="T189">naga </ts>
               <ts e="T191" id="Seg_4017" n="e" s="T190">(t-) </ts>
               <ts e="T192" id="Seg_4019" n="e" s="T191">il, </ts>
               <ts e="T193" id="Seg_4021" n="e" s="T192">šindidə </ts>
               <ts e="T194" id="Seg_4023" n="e" s="T193">naga, </ts>
               <ts e="T195" id="Seg_4025" n="e" s="T194">(onʼiʔ= </ts>
               <ts e="T196" id="Seg_4027" n="e" s="T195">onʼiʔ </ts>
               <ts e="T197" id="Seg_4029" n="e" s="T196">amnolaʔ-), </ts>
               <ts e="T198" id="Seg_4031" n="e" s="T197">onʼiʔ </ts>
               <ts e="T199" id="Seg_4033" n="e" s="T198">maʔ </ts>
               <ts e="T200" id="Seg_4035" n="e" s="T199">nuga. </ts>
               <ts e="T201" id="Seg_4037" n="e" s="T200">Dĭ </ts>
               <ts e="T202" id="Seg_4039" n="e" s="T201">šobi. </ts>
               <ts e="T203" id="Seg_4041" n="e" s="T202">(Ĭm- </ts>
               <ts e="T204" id="Seg_4043" n="e" s="T203">gib-) </ts>
               <ts e="T205" id="Seg_4045" n="e" s="T204">Giraːmbi </ts>
               <ts e="T206" id="Seg_4047" n="e" s="T205">bar </ts>
               <ts e="T207" id="Seg_4049" n="e" s="T206">il? </ts>
               <ts e="T208" id="Seg_4051" n="e" s="T207">(Da </ts>
               <ts e="T209" id="Seg_4053" n="e" s="T208">miʔ </ts>
               <ts e="T210" id="Seg_4055" n="e" s="T209">koʔbdobaʔ </ts>
               <ts e="T211" id="Seg_4057" n="e" s="T210">bar=) </ts>
               <ts e="T212" id="Seg_4059" n="e" s="T211">Šide </ts>
               <ts e="T213" id="Seg_4061" n="e" s="T212">nʼi </ts>
               <ts e="T214" id="Seg_4063" n="e" s="T213">ibiʔi. </ts>
               <ts e="T215" id="Seg_4065" n="e" s="T214">Dĭzeŋ </ts>
               <ts e="T216" id="Seg_4067" n="e" s="T215">bünə </ts>
               <ts e="T217" id="Seg_4069" n="e" s="T216">öʔluʔpiʔi. </ts>
               <ts e="T218" id="Seg_4071" n="e" s="T217">A </ts>
               <ts e="T219" id="Seg_4073" n="e" s="T218">miʔ </ts>
               <ts e="T220" id="Seg_4075" n="e" s="T219">koʔbdobaʔ </ts>
               <ts e="T221" id="Seg_4077" n="e" s="T220">bar </ts>
               <ts e="T222" id="Seg_4079" n="e" s="T221">iləm </ts>
               <ts e="T223" id="Seg_4081" n="e" s="T222">dʼăgarluʔpi. </ts>
               <ts e="T224" id="Seg_4083" n="e" s="T223">A </ts>
               <ts e="T225" id="Seg_4085" n="e" s="T224">gijen </ts>
               <ts e="T226" id="Seg_4087" n="e" s="T225">dĭ? </ts>
               <ts e="T227" id="Seg_4089" n="e" s="T226">Gibərdə </ts>
               <ts e="T384" id="Seg_4091" n="e" s="T227">kalla </ts>
               <ts e="T228" id="Seg_4093" n="e" s="T384">dʼürbi, </ts>
               <ts e="T229" id="Seg_4095" n="e" s="T228">ej </ts>
               <ts e="T230" id="Seg_4097" n="e" s="T229">tĭmnebeʔ. </ts>
               <ts e="T231" id="Seg_4099" n="e" s="T230">Dĭgəttə </ts>
               <ts e="T232" id="Seg_4101" n="e" s="T231">dĭ </ts>
               <ts e="T233" id="Seg_4103" n="e" s="T232">ine </ts>
               <ts e="T234" id="Seg_4105" n="e" s="T233">ibi. </ts>
               <ts e="T235" id="Seg_4107" n="e" s="T234">Kambi </ts>
               <ts e="T236" id="Seg_4109" n="e" s="T235">kuzittə, </ts>
               <ts e="T237" id="Seg_4111" n="e" s="T236">(ku- </ts>
               <ts e="T238" id="Seg_4113" n="e" s="T237">ku-) </ts>
               <ts e="T239" id="Seg_4115" n="e" s="T238">kulaːmbi, </ts>
               <ts e="T240" id="Seg_4117" n="e" s="T239">kubi, </ts>
               <ts e="T241" id="Seg_4119" n="e" s="T240">kubiʔi </ts>
               <ts e="T242" id="Seg_4121" n="e" s="T241">dĭn. </ts>
               <ts e="T243" id="Seg_4123" n="e" s="T242">Dĭgəttə </ts>
               <ts e="T244" id="Seg_4125" n="e" s="T243">davaj </ts>
               <ts e="T245" id="Seg_4127" n="e" s="T244">dĭziʔ </ts>
               <ts e="T246" id="Seg_4129" n="e" s="T245">bar </ts>
               <ts e="T247" id="Seg_4131" n="e" s="T246">dʼabrozittə. </ts>
               <ts e="T248" id="Seg_4133" n="e" s="T247">Dĭ </ts>
               <ts e="T249" id="Seg_4135" n="e" s="T248">xatʼel </ts>
               <ts e="T250" id="Seg_4137" n="e" s="T249">dĭm </ts>
               <ts e="T251" id="Seg_4139" n="e" s="T250">(bazə-) </ts>
               <ts e="T252" id="Seg_4141" n="e" s="T251">bădəsʼtə </ts>
               <ts e="T253" id="Seg_4143" n="e" s="T252">dagajziʔ. </ts>
               <ts e="T254" id="Seg_4145" n="e" s="T253">Dĭ </ts>
               <ts e="T255" id="Seg_4147" n="e" s="T254">bar </ts>
               <ts e="T256" id="Seg_4149" n="e" s="T255">măndə: </ts>
               <ts e="T257" id="Seg_4151" n="e" s="T256">Iʔ </ts>
               <ts e="T258" id="Seg_4153" n="e" s="T257">bădaʔ, </ts>
               <ts e="T259" id="Seg_4155" n="e" s="T258">măn </ts>
               <ts e="T260" id="Seg_4157" n="e" s="T259">tăn </ts>
               <ts e="T261" id="Seg_4159" n="e" s="T260">kagam </ts>
               <ts e="T262" id="Seg_4161" n="e" s="T261">igem. </ts>
               <ts e="T263" id="Seg_4163" n="e" s="T262">(Kurizəʔi) </ts>
               <ts e="T264" id="Seg_4165" n="e" s="T263">dʼăbaktərlaʔbəʔjə. </ts>
               <ts e="T265" id="Seg_4167" n="e" s="T264">Kapaluxa </ts>
               <ts e="T266" id="Seg_4169" n="e" s="T265">i </ts>
               <ts e="T267" id="Seg_4171" n="e" s="T266">((PAUSE)) </ts>
               <ts e="T269" id="Seg_4173" n="e" s="T267">kuropatka. </ts>
               <ts e="T270" id="Seg_4175" n="e" s="T269">Kuropatka </ts>
               <ts e="T271" id="Seg_4177" n="e" s="T270">mălia </ts>
               <ts e="T272" id="Seg_4179" n="e" s="T271">kapaluxanə: </ts>
               <ts e="T273" id="Seg_4181" n="e" s="T272">Tăn </ts>
               <ts e="T274" id="Seg_4183" n="e" s="T273">ej </ts>
               <ts e="T275" id="Seg_4185" n="e" s="T274">kuvas, </ts>
               <ts e="T276" id="Seg_4187" n="e" s="T275">girgit </ts>
               <ts e="T277" id="Seg_4189" n="e" s="T276">(piʔmeʔi), </ts>
               <ts e="T278" id="Seg_4191" n="e" s="T277">ej </ts>
               <ts e="T279" id="Seg_4193" n="e" s="T278">jakšə. </ts>
               <ts e="T280" id="Seg_4195" n="e" s="T279">A </ts>
               <ts e="T281" id="Seg_4197" n="e" s="T280">măn </ts>
               <ts e="T282" id="Seg_4199" n="e" s="T281">kuvas </ts>
               <ts e="T283" id="Seg_4201" n="e" s="T282">igem. </ts>
               <ts e="T883" id="Seg_4203" n="e" s="T283">Коричневый </ts>
               <ts e="T284" id="Seg_4205" n="e" s="T883">inegəʔ </ts>
               <ts e="T285" id="Seg_4207" n="e" s="T284">eʔbdə </ts>
               <ts e="T286" id="Seg_4209" n="e" s="T285">bar </ts>
               <ts e="T287" id="Seg_4211" n="e" s="T286">sajnʼiʔluʔpiеm </ts>
               <ts e="T288" id="Seg_4213" n="e" s="T287">i </ts>
               <ts e="T289" id="Seg_4215" n="e" s="T288">boskəndə </ts>
               <ts e="T290" id="Seg_4217" n="e" s="T289">embiem </ts>
               <ts e="T292" id="Seg_4219" n="e" s="T290">ulunə. </ts>
               <ts e="T293" id="Seg_4221" n="e" s="T292">Ugaːndə </ts>
               <ts e="T294" id="Seg_4223" n="e" s="T293">kuvas </ts>
               <ts e="T295" id="Seg_4225" n="e" s="T294">igem </ts>
               <ts e="T296" id="Seg_4227" n="e" s="T295">tüj. </ts>
               <ts e="T297" id="Seg_4229" n="e" s="T296">((BRK)). </ts>
               <ts e="T298" id="Seg_4231" n="e" s="T297">Măjagən </ts>
               <ts e="T299" id="Seg_4233" n="e" s="T298">bar </ts>
               <ts e="T300" id="Seg_4235" n="e" s="T299">aspak </ts>
               <ts e="T301" id="Seg_4237" n="e" s="T300">mĭnzəlleʔbə. </ts>
               <ts e="T302" id="Seg_4239" n="e" s="T301">Ĭmbi </ts>
               <ts e="T303" id="Seg_4241" n="e" s="T302">dĭ </ts>
               <ts e="T304" id="Seg_4243" n="e" s="T303">dĭrgit? </ts>
               <ts e="T305" id="Seg_4245" n="e" s="T304">Murašeʔi. </ts>
               <ts e="T306" id="Seg_4247" n="e" s="T305">((BRK)). </ts>
               <ts e="T308" id="Seg_4249" n="e" s="T306">Măjagən… </ts>
               <ts e="T309" id="Seg_4251" n="e" s="T308">((BRK)). </ts>
               <ts e="T310" id="Seg_4253" n="e" s="T309">Măjagən </ts>
               <ts e="T311" id="Seg_4255" n="e" s="T310">süjö </ts>
               <ts e="T312" id="Seg_4257" n="e" s="T311">edölaʔbə. </ts>
               <ts e="T313" id="Seg_4259" n="e" s="T312">Ĭmbi </ts>
               <ts e="T314" id="Seg_4261" n="e" s="T313">dĭrgit? </ts>
               <ts e="T315" id="Seg_4263" n="e" s="T314">Nüjö. </ts>
               <ts e="T316" id="Seg_4265" n="e" s="T315">((BRK)). </ts>
               <ts e="T317" id="Seg_4267" n="e" s="T316">Bostə </ts>
               <ts e="T318" id="Seg_4269" n="e" s="T317">(dʼünən-) </ts>
               <ts e="T319" id="Seg_4271" n="e" s="T318">dʼügən </ts>
               <ts e="T320" id="Seg_4273" n="e" s="T319">amnolaʔbə, </ts>
               <ts e="T321" id="Seg_4275" n="e" s="T320">a </ts>
               <ts e="T322" id="Seg_4277" n="e" s="T321">kut </ts>
               <ts e="T323" id="Seg_4279" n="e" s="T322">baška </ts>
               <ts e="T324" id="Seg_4281" n="e" s="T323">dʼünən </ts>
               <ts e="T325" id="Seg_4283" n="e" s="T324">amnolaʔbə </ts>
               <ts e="T326" id="Seg_4285" n="e" s="T325">serʼožkaʔizi. </ts>
               <ts e="T327" id="Seg_4287" n="e" s="T326">Ответ: </ts>
               <ts e="T328" id="Seg_4289" n="e" s="T327">саранка. </ts>
               <ts e="T329" id="Seg_4291" n="e" s="T328">((BRK)). </ts>
               <ts e="T330" id="Seg_4293" n="e" s="T329">Šide </ts>
               <ts e="T331" id="Seg_4295" n="e" s="T330">(neg-) </ts>
               <ts e="T332" id="Seg_4297" n="e" s="T331">nezeŋ </ts>
               <ts e="T333" id="Seg_4299" n="e" s="T332">iʔbolaʔbəʔjə. </ts>
               <ts e="T334" id="Seg_4301" n="e" s="T333">((BRK)). </ts>
               <ts e="T335" id="Seg_4303" n="e" s="T334">Iʔbolaʔbəʔjə </ts>
               <ts e="T336" id="Seg_4305" n="e" s="T335">(šünə- </ts>
               <ts e="T337" id="Seg_4307" n="e" s="T336">š-) </ts>
               <ts e="T338" id="Seg_4309" n="e" s="T337">šünə </ts>
               <ts e="T339" id="Seg_4311" n="e" s="T338">tondə, </ts>
               <ts e="T340" id="Seg_4313" n="e" s="T339">nanəʔsəbiʔi. </ts>
               <ts e="T341" id="Seg_4315" n="e" s="T340">Dĭ </ts>
               <ts e="T342" id="Seg_4317" n="e" s="T341">üjüzeŋdə </ts>
               <ts e="T343" id="Seg_4319" n="e" s="T342">lʼažkaʔi. </ts>
               <ts e="T344" id="Seg_4321" n="e" s="T343">((BRK)). </ts>
               <ts e="T345" id="Seg_4323" n="e" s="T344">Üjüt </ts>
               <ts e="T346" id="Seg_4325" n="e" s="T345">naga, </ts>
               <ts e="T347" id="Seg_4327" n="e" s="T346">i </ts>
               <ts e="T348" id="Seg_4329" n="e" s="T347">udat </ts>
               <ts e="T349" id="Seg_4331" n="e" s="T348">naga. </ts>
               <ts e="T350" id="Seg_4333" n="e" s="T349">A </ts>
               <ts e="T352" id="Seg_4335" n="e" s="T350">mĭnge… </ts>
               <ts e="T353" id="Seg_4337" n="e" s="T352">(M-) </ts>
               <ts e="T354" id="Seg_4339" n="e" s="T353">Măjanə </ts>
               <ts e="T355" id="Seg_4341" n="e" s="T354">kandəga. </ts>
               <ts e="T356" id="Seg_4343" n="e" s="T355">Dĭ </ts>
               <ts e="T357" id="Seg_4345" n="e" s="T356">kuja. </ts>
               <ts e="T358" id="Seg_4347" n="e" s="T357">((BRK)). </ts>
               <ts e="T359" id="Seg_4349" n="e" s="T358">Iʔgö </ts>
               <ts e="T360" id="Seg_4351" n="e" s="T359">süjöʔi </ts>
               <ts e="T361" id="Seg_4353" n="e" s="T360">uluzaŋdə </ts>
               <ts e="T362" id="Seg_4355" n="e" s="T361">bar </ts>
               <ts e="T363" id="Seg_4357" n="e" s="T362">onʼiʔ </ts>
               <ts e="T364" id="Seg_4359" n="e" s="T363">onʼiʔtə. </ts>
            </ts>
            <ts e="T491" id="Seg_4360" n="sc" s="T365">
               <ts e="T366" id="Seg_4362" n="e" s="T365">A </ts>
               <ts e="T367" id="Seg_4364" n="e" s="T366">(kötezeŋ-) </ts>
               <ts e="T368" id="Seg_4366" n="e" s="T367">kötenzeŋdə </ts>
               <ts e="T369" id="Seg_4368" n="e" s="T368">bar </ts>
               <ts e="T370" id="Seg_4370" n="e" s="T369">kuŋgeŋ </ts>
               <ts e="T371" id="Seg_4372" n="e" s="T370">onʼiʔ </ts>
               <ts e="T372" id="Seg_4374" n="e" s="T371">onʼiʔtə. </ts>
               <ts e="T373" id="Seg_4376" n="e" s="T372">Dĭ </ts>
               <ts e="T374" id="Seg_4378" n="e" s="T373">maʔ. </ts>
               <ts e="T375" id="Seg_4380" n="e" s="T374">((BRK)). </ts>
               <ts e="T376" id="Seg_4382" n="e" s="T375">Turagən </ts>
               <ts e="T377" id="Seg_4384" n="e" s="T376">iʔbolaʔbə </ts>
               <ts e="T378" id="Seg_4386" n="e" s="T377">(dʼo-) </ts>
               <ts e="T379" id="Seg_4388" n="e" s="T378">dʼüʔpi </ts>
               <ts e="T380" id="Seg_4390" n="e" s="T379">buzo. </ts>
               <ts e="T381" id="Seg_4392" n="e" s="T380">A </ts>
               <ts e="T382" id="Seg_4394" n="e" s="T381">nörbəsʼtə </ts>
               <ts e="T383" id="Seg_4396" n="e" s="T382">dĭ </ts>
               <ts e="T885" id="Seg_4398" n="e" s="T383">((PAUSE)) </ts>
               <ts e="T385" id="Seg_4400" n="e" s="T885">šĭkət. </ts>
               <ts e="T386" id="Seg_4402" n="e" s="T385">((BRK)). </ts>
               <ts e="T387" id="Seg_4404" n="e" s="T386">(Šire-) </ts>
               <ts e="T388" id="Seg_4406" n="e" s="T387">Sĭre </ts>
               <ts e="T389" id="Seg_4408" n="e" s="T388">(pa-) </ts>
               <ts e="T390" id="Seg_4410" n="e" s="T389">paʔinə </ts>
               <ts e="T391" id="Seg_4412" n="e" s="T390">tarara </ts>
               <ts e="T392" id="Seg_4414" n="e" s="T391">amnolaʔbə. </ts>
               <ts e="T393" id="Seg_4416" n="e" s="T392">((BRK)). </ts>
               <ts e="T394" id="Seg_4418" n="e" s="T393">Dĭ </ts>
               <ts e="T395" id="Seg_4420" n="e" s="T394">šĭkə. </ts>
               <ts e="T396" id="Seg_4422" n="e" s="T395">((BRK)). </ts>
               <ts e="T397" id="Seg_4424" n="e" s="T396">Üjüt </ts>
               <ts e="T398" id="Seg_4426" n="e" s="T397">(uje-) </ts>
               <ts e="T399" id="Seg_4428" n="e" s="T398">udat </ts>
               <ts e="T400" id="Seg_4430" n="e" s="T399">naga, </ts>
               <ts e="T401" id="Seg_4432" n="e" s="T400">a </ts>
               <ts e="T402" id="Seg_4434" n="e" s="T401">kudajdə </ts>
               <ts e="T403" id="Seg_4436" n="e" s="T402">üzlie. </ts>
               <ts e="T404" id="Seg_4438" n="e" s="T403">Dĭ </ts>
               <ts e="T405" id="Seg_4440" n="e" s="T404">зыбка. </ts>
               <ts e="T406" id="Seg_4442" n="e" s="T405">((BRK)). </ts>
               <ts e="T407" id="Seg_4444" n="e" s="T406">Üjüt, </ts>
               <ts e="T408" id="Seg_4446" n="e" s="T407">udat </ts>
               <ts e="T409" id="Seg_4448" n="e" s="T408">naga. </ts>
               <ts e="T410" id="Seg_4450" n="e" s="T409">A </ts>
               <ts e="T411" id="Seg_4452" n="e" s="T410">paʔinə </ts>
               <ts e="T412" id="Seg_4454" n="e" s="T411">mĭllie, </ts>
               <ts e="T413" id="Seg_4456" n="e" s="T412">кланяется, </ts>
               <ts e="T414" id="Seg_4458" n="e" s="T413">кланяется; </ts>
               <ts e="T415" id="Seg_4460" n="e" s="T414">a </ts>
               <ts e="T416" id="Seg_4462" n="e" s="T415">maʔndə </ts>
               <ts e="T417" id="Seg_4464" n="e" s="T416">šoləj— </ts>
               <ts e="T418" id="Seg_4466" n="e" s="T417">iʔbolaʔbə. </ts>
               <ts e="T419" id="Seg_4468" n="e" s="T418">Dĭ </ts>
               <ts e="T420" id="Seg_4470" n="e" s="T419">baltu. </ts>
               <ts e="T421" id="Seg_4472" n="e" s="T420">((BRK)). </ts>
               <ts e="T422" id="Seg_4474" n="e" s="T421">Udat, </ts>
               <ts e="T423" id="Seg_4476" n="e" s="T422">üjüt </ts>
               <ts e="T424" id="Seg_4478" n="e" s="T423">naga. </ts>
               <ts e="T425" id="Seg_4480" n="e" s="T424">A </ts>
               <ts e="T426" id="Seg_4482" n="e" s="T425">ajim </ts>
               <ts e="T427" id="Seg_4484" n="e" s="T426">bar </ts>
               <ts e="T428" id="Seg_4486" n="e" s="T427">karlaʔbə. </ts>
               <ts e="T429" id="Seg_4488" n="e" s="T428">Dĭ </ts>
               <ts e="T430" id="Seg_4490" n="e" s="T429">beržə. </ts>
               <ts e="T431" id="Seg_4492" n="e" s="T430">((BRK)). </ts>
               <ts e="T432" id="Seg_4494" n="e" s="T431">Üdʼüge </ts>
               <ts e="T433" id="Seg_4496" n="e" s="T432">(men-) </ts>
               <ts e="T434" id="Seg_4498" n="e" s="T433">men </ts>
               <ts e="T435" id="Seg_4500" n="e" s="T434">maːndə </ts>
               <ts e="T436" id="Seg_4502" n="e" s="T435">toʔndə </ts>
               <ts e="T437" id="Seg_4504" n="e" s="T436">amnolaʔbəʔjə. </ts>
               <ts e="T438" id="Seg_4506" n="e" s="T437">Ej </ts>
               <ts e="T439" id="Seg_4508" n="e" s="T438">kürümnie </ts>
               <ts e="T440" id="Seg_4510" n="e" s="T439">i </ts>
               <ts e="T441" id="Seg_4512" n="e" s="T440">(šindi-) </ts>
               <ts e="T442" id="Seg_4514" n="e" s="T441">šindimdə </ts>
               <ts e="T443" id="Seg_4516" n="e" s="T442">maʔtə </ts>
               <ts e="T445" id="Seg_4518" n="e" s="T443">ej… </ts>
               <ts e="T446" id="Seg_4520" n="e" s="T445">((BRK)). </ts>
               <ts e="T447" id="Seg_4522" n="e" s="T446">Šindinədə </ts>
               <ts e="T448" id="Seg_4524" n="e" s="T447">maʔndə </ts>
               <ts e="T449" id="Seg_4526" n="e" s="T448">ej </ts>
               <ts e="T450" id="Seg_4528" n="e" s="T449">öʔle. </ts>
               <ts e="T451" id="Seg_4530" n="e" s="T450">Dĭ </ts>
               <ts e="T452" id="Seg_4532" n="e" s="T451">замок. </ts>
               <ts e="T453" id="Seg_4534" n="e" s="T452">((BRK)). </ts>
               <ts e="T454" id="Seg_4536" n="e" s="T453">Krasnojarskəʔi </ts>
               <ts e="T455" id="Seg_4538" n="e" s="T454">turaʔi </ts>
               <ts e="T456" id="Seg_4540" n="e" s="T455">jaʔleʔbəʔjə. </ts>
               <ts e="T457" id="Seg_4542" n="e" s="T456">A </ts>
               <ts e="T458" id="Seg_4544" n="e" s="T457">döber </ts>
               <ts e="T459" id="Seg_4546" n="e" s="T458">paʔi </ts>
               <ts e="T460" id="Seg_4548" n="e" s="T459">nʼergölaʔbəʔjə. </ts>
               <ts e="T461" id="Seg_4550" n="e" s="T460">Dĭ </ts>
               <ts e="T462" id="Seg_4552" n="e" s="T461">sazən </ts>
               <ts e="T463" id="Seg_4554" n="e" s="T462">kandəga </ts>
               <ts e="T464" id="Seg_4556" n="e" s="T463">dĭʔə. </ts>
               <ts e="T891" id="Seg_4558" n="e" s="T464">((…)) </ts>
               <ts e="T466" id="Seg_4560" n="e" s="T891">((BRK)). </ts>
               <ts e="T467" id="Seg_4562" n="e" s="T466">(Šĭket) </ts>
               <ts e="T468" id="Seg_4564" n="e" s="T467">naga, </ts>
               <ts e="T469" id="Seg_4566" n="e" s="T468">simat </ts>
               <ts e="T470" id="Seg_4568" n="e" s="T469">naga, </ts>
               <ts e="T471" id="Seg_4570" n="e" s="T470">aŋdə </ts>
               <ts e="T472" id="Seg_4572" n="e" s="T471">naga, </ts>
               <ts e="T473" id="Seg_4574" n="e" s="T472">a </ts>
               <ts e="T474" id="Seg_4576" n="e" s="T473">măllia </ts>
               <ts e="T475" id="Seg_4578" n="e" s="T474">kumen </ts>
               <ts e="T476" id="Seg_4580" n="e" s="T475">vremja </ts>
               <ts e="T477" id="Seg_4582" n="e" s="T476">kambi. </ts>
               <ts e="T478" id="Seg_4584" n="e" s="T477">Dĭ </ts>
               <ts e="T479" id="Seg_4586" n="e" s="T478">časɨʔi. </ts>
               <ts e="T480" id="Seg_4588" n="e" s="T479">((BRK)). </ts>
               <ts e="T481" id="Seg_4590" n="e" s="T480">Iʔbolaʔbə </ts>
               <ts e="T482" id="Seg_4592" n="e" s="T481">dăk, </ts>
               <ts e="T483" id="Seg_4594" n="e" s="T482">üdʼüge </ts>
               <ts e="T484" id="Seg_4596" n="e" s="T483">tospaktə. </ts>
               <ts e="T485" id="Seg_4598" n="e" s="T484">A </ts>
               <ts e="T486" id="Seg_4600" n="e" s="T485">nulaʔbə </ts>
               <ts e="T487" id="Seg_4602" n="e" s="T486">dăk </ts>
               <ts e="T488" id="Seg_4604" n="e" s="T487">ugandə </ts>
               <ts e="T489" id="Seg_4606" n="e" s="T488">urgo, </ts>
               <ts e="T490" id="Seg_4608" n="e" s="T489">urgo </ts>
               <ts e="T491" id="Seg_4610" n="e" s="T490">inetsi. </ts>
            </ts>
            <ts e="T882" id="Seg_4611" n="sc" s="T492">
               <ts e="T493" id="Seg_4613" n="e" s="T492">Duga. </ts>
               <ts e="T494" id="Seg_4615" n="e" s="T493">((BRK)). </ts>
               <ts e="T495" id="Seg_4617" n="e" s="T494">Amnolaʔbə </ts>
               <ts e="T496" id="Seg_4619" n="e" s="T495">koʔbdo </ts>
               <ts e="T497" id="Seg_4621" n="e" s="T496">bar </ts>
               <ts e="T498" id="Seg_4623" n="e" s="T497">ospaʔizi </ts>
               <ts e="T499" id="Seg_4625" n="e" s="T498">(tĭ). </ts>
               <ts e="T500" id="Seg_4627" n="e" s="T499">A </ts>
               <ts e="T501" id="Seg_4629" n="e" s="T500">dĭ </ts>
               <ts e="T502" id="Seg_4631" n="e" s="T501">indak. </ts>
               <ts e="T503" id="Seg_4633" n="e" s="T502">((BRK)). </ts>
               <ts e="T504" id="Seg_4635" n="e" s="T503">Oʔbdəbiam, </ts>
               <ts e="T505" id="Seg_4637" n="e" s="T504">oʔbdəbiam </ts>
               <ts e="T506" id="Seg_4639" n="e" s="T505">nʼitkaʔi, </ts>
               <ts e="T507" id="Seg_4641" n="e" s="T506">ej </ts>
               <ts e="T508" id="Seg_4643" n="e" s="T507">(mombia-) </ts>
               <ts e="T509" id="Seg_4645" n="e" s="T508">ej </ts>
               <ts e="T510" id="Seg_4647" n="e" s="T509">mobiam </ts>
               <ts e="T511" id="Seg_4649" n="e" s="T510">oʔbdəsʼtə, </ts>
               <ts e="T512" id="Seg_4651" n="e" s="T511">dĭ </ts>
               <ts e="T513" id="Seg_4653" n="e" s="T512">aʔtʼi. </ts>
               <ts e="T514" id="Seg_4655" n="e" s="T513">((BRK)). </ts>
               <ts e="T515" id="Seg_4657" n="e" s="T514">Amnolaʔbə </ts>
               <ts e="T516" id="Seg_4659" n="e" s="T515">büzʼe </ts>
               <ts e="T517" id="Seg_4661" n="e" s="T516">grʼadkagən. </ts>
               <ts e="T518" id="Seg_4663" n="e" s="T517">Bar </ts>
               <ts e="T519" id="Seg_4665" n="e" s="T518">iʔgö </ts>
               <ts e="T520" id="Seg_4667" n="e" s="T519">zaplatkaʔi. </ts>
               <ts e="T521" id="Seg_4669" n="e" s="T520">Šində </ts>
               <ts e="T522" id="Seg_4671" n="e" s="T521">(kulil-) </ts>
               <ts e="T523" id="Seg_4673" n="e" s="T522">kulaʔbə </ts>
               <ts e="T524" id="Seg_4675" n="e" s="T523">bar </ts>
               <ts e="T525" id="Seg_4677" n="e" s="T524">dʼorlaʔbə. </ts>
               <ts e="T526" id="Seg_4679" n="e" s="T525">Dĭ </ts>
               <ts e="T527" id="Seg_4681" n="e" s="T526">köbergən. </ts>
               <ts e="T528" id="Seg_4683" n="e" s="T527">((BRK)). </ts>
               <ts e="T529" id="Seg_4685" n="e" s="T528">Amnolaʔbə, </ts>
               <ts e="T530" id="Seg_4687" n="e" s="T529">iʔgö </ts>
               <ts e="T532" id="Seg_4689" n="e" s="T530">oldʼa… </ts>
               <ts e="T533" id="Seg_4691" n="e" s="T532">((BRK)). </ts>
               <ts e="T534" id="Seg_4693" n="e" s="T533">(Sojp-) </ts>
               <ts e="T535" id="Seg_4695" n="e" s="T534">Sejʔpü </ts>
               <ts e="T536" id="Seg_4697" n="e" s="T535">(aktʼa=) </ts>
               <ts e="T537" id="Seg_4699" n="e" s="T536"> ((PAUSE)) </ts>
               <ts e="T539" id="Seg_4701" n="e" s="T537">(o-). </ts>
               <ts e="T540" id="Seg_4703" n="e" s="T539">Bʼeʔ </ts>
               <ts e="T541" id="Seg_4705" n="e" s="T540">(sojp-) </ts>
               <ts e="T542" id="Seg_4707" n="e" s="T541">sejʔpü </ts>
               <ts e="T543" id="Seg_4709" n="e" s="T542">oldʼa, </ts>
               <ts e="T544" id="Seg_4711" n="e" s="T543">a </ts>
               <ts e="T545" id="Seg_4713" n="e" s="T544">kajzittə </ts>
               <ts e="T546" id="Seg_4715" n="e" s="T545">naga. </ts>
               <ts e="T547" id="Seg_4717" n="e" s="T546">Dĭ </ts>
               <ts e="T548" id="Seg_4719" n="e" s="T547">kapusta. </ts>
               <ts e="T549" id="Seg_4721" n="e" s="T548">((BRK)). </ts>
               <ts e="T550" id="Seg_4723" n="e" s="T549">Amnolaʔbə </ts>
               <ts e="T551" id="Seg_4725" n="e" s="T550">koʔbdo </ts>
               <ts e="T552" id="Seg_4727" n="e" s="T551">dʼügən, </ts>
               <ts e="T553" id="Seg_4729" n="e" s="T552">bostə </ts>
               <ts e="T554" id="Seg_4731" n="e" s="T553">kömə. </ts>
               <ts e="T555" id="Seg_4733" n="e" s="T554">A </ts>
               <ts e="T556" id="Seg_4735" n="e" s="T555">eʔbdət </ts>
               <ts e="T557" id="Seg_4737" n="e" s="T556">nʼiʔnen. </ts>
               <ts e="T558" id="Seg_4739" n="e" s="T557">Dĭ </ts>
               <ts e="T559" id="Seg_4741" n="e" s="T558">морковка. </ts>
               <ts e="T560" id="Seg_4743" n="e" s="T559">((BRK)). </ts>
               <ts e="T561" id="Seg_4745" n="e" s="T560">Măn </ts>
               <ts e="T562" id="Seg_4747" n="e" s="T561">kubiam </ts>
               <ts e="T563" id="Seg_4749" n="e" s="T562">plʼotka, </ts>
               <ts e="T564" id="Seg_4751" n="e" s="T563">a </ts>
               <ts e="T565" id="Seg_4753" n="e" s="T564">kanzittə </ts>
               <ts e="T566" id="Seg_4755" n="e" s="T565">ej </ts>
               <ts e="T567" id="Seg_4757" n="e" s="T566">mobiam. </ts>
               <ts e="T568" id="Seg_4759" n="e" s="T567">A </ts>
               <ts e="T569" id="Seg_4761" n="e" s="T568">dĭ </ts>
               <ts e="T570" id="Seg_4763" n="e" s="T569">ĭmbi? </ts>
               <ts e="T571" id="Seg_4765" n="e" s="T570">Penzə. </ts>
               <ts e="T572" id="Seg_4767" n="e" s="T571">((BRK)). </ts>
               <ts e="T573" id="Seg_4769" n="e" s="T572">Šonəga </ts>
               <ts e="T574" id="Seg_4771" n="e" s="T573">koʔbdo, </ts>
               <ts e="T575" id="Seg_4773" n="e" s="T574">i </ts>
               <ts e="T576" id="Seg_4775" n="e" s="T575">dĭn </ts>
               <ts e="T577" id="Seg_4777" n="e" s="T576">eʔbdəʔi </ts>
               <ts e="T578" id="Seg_4779" n="e" s="T577">bar </ts>
               <ts e="T579" id="Seg_4781" n="e" s="T578">(ko-) </ts>
               <ts e="T580" id="Seg_4783" n="e" s="T579">kurgonaʔi </ts>
               <ts e="T581" id="Seg_4785" n="e" s="T580">bar. </ts>
               <ts e="T582" id="Seg_4787" n="e" s="T581">((BRK)). </ts>
               <ts e="T583" id="Seg_4789" n="e" s="T582">Dĭ </ts>
               <ts e="T584" id="Seg_4791" n="e" s="T583">soroka </ts>
               <ts e="T585" id="Seg_4793" n="e" s="T584">ige. </ts>
               <ts e="T586" id="Seg_4795" n="e" s="T585">((BRK)). </ts>
               <ts e="T587" id="Seg_4797" n="e" s="T586">Šide </ts>
               <ts e="T588" id="Seg_4799" n="e" s="T587">nugaʔi, </ts>
               <ts e="T589" id="Seg_4801" n="e" s="T588">šide </ts>
               <ts e="T590" id="Seg_4803" n="e" s="T589">iʔbolaʔbəʔjə, </ts>
               <ts e="T591" id="Seg_4805" n="e" s="T590">(onʼiʔ </ts>
               <ts e="T592" id="Seg_4807" n="e" s="T591">mĭlleʔbə=) </ts>
               <ts e="T593" id="Seg_4809" n="e" s="T592">sumna </ts>
               <ts e="T594" id="Seg_4811" n="e" s="T593">mĭlleʔbə, </ts>
               <ts e="T595" id="Seg_4813" n="e" s="T594">a </ts>
               <ts e="T596" id="Seg_4815" n="e" s="T595">sejʔpü </ts>
               <ts e="T597" id="Seg_4817" n="e" s="T596">(kaj-) </ts>
               <ts e="T598" id="Seg_4819" n="e" s="T597">kajlaʔbə. </ts>
               <ts e="T599" id="Seg_4821" n="e" s="T598">Dĭ </ts>
               <ts e="T600" id="Seg_4823" n="e" s="T599">aji </ts>
               <ts e="T601" id="Seg_4825" n="e" s="T600">ige. </ts>
               <ts e="T602" id="Seg_4827" n="e" s="T601">((BRK)). </ts>
               <ts e="T603" id="Seg_4829" n="e" s="T602">(Ula-) </ts>
               <ts e="T604" id="Seg_4831" n="e" s="T603">ular </ts>
               <ts e="T605" id="Seg_4833" n="e" s="T604">iʔgö </ts>
               <ts e="T606" id="Seg_4835" n="e" s="T605">mĭlleʔbəʔi, </ts>
               <ts e="T607" id="Seg_4837" n="e" s="T606">a </ts>
               <ts e="T608" id="Seg_4839" n="e" s="T607">pastux </ts>
               <ts e="T609" id="Seg_4841" n="e" s="T608">((PAUSE)) </ts>
               <ts e="T611" id="Seg_4843" n="e" s="T609">amnut. </ts>
               <ts e="T612" id="Seg_4845" n="e" s="T611">Dĭ </ts>
               <ts e="T613" id="Seg_4847" n="e" s="T612">mʼesʼas </ts>
               <ts e="T614" id="Seg_4849" n="e" s="T613">i </ts>
               <ts e="T615" id="Seg_4851" n="e" s="T614">zvʼozdaʔi. </ts>
               <ts e="T616" id="Seg_4853" n="e" s="T615">((BRK)). </ts>
               <ts e="T617" id="Seg_4855" n="e" s="T616">Šide </ts>
               <ts e="T618" id="Seg_4857" n="e" s="T617">ši </ts>
               <ts e="T619" id="Seg_4859" n="e" s="T618">i </ts>
               <ts e="T620" id="Seg_4861" n="e" s="T619">šide </ts>
               <ts e="T621" id="Seg_4863" n="e" s="T620">puʔməʔi, </ts>
               <ts e="T622" id="Seg_4865" n="e" s="T621">a </ts>
               <ts e="T623" id="Seg_4867" n="e" s="T622">šüjögən </ts>
               <ts e="T624" id="Seg_4869" n="e" s="T623">marka. </ts>
               <ts e="T625" id="Seg_4871" n="e" s="T624">Kaptəʔi. </ts>
               <ts e="T626" id="Seg_4873" n="e" s="T625">((BRK)). </ts>
               <ts e="T627" id="Seg_4875" n="e" s="T626">Măn </ts>
               <ts e="T628" id="Seg_4877" n="e" s="T627">üge </ts>
               <ts e="T629" id="Seg_4879" n="e" s="T628">amnaʔbəm, </ts>
               <ts e="T630" id="Seg_4881" n="e" s="T629">amnaʔbəm, </ts>
               <ts e="T631" id="Seg_4883" n="e" s="T630">a </ts>
               <ts e="T632" id="Seg_4885" n="e" s="T631">tüʔsittə </ts>
               <ts e="T633" id="Seg_4887" n="e" s="T632">ej </ts>
               <ts e="T634" id="Seg_4889" n="e" s="T633">moliam — </ts>
               <ts e="T635" id="Seg_4891" n="e" s="T634">Bura. </ts>
               <ts e="T636" id="Seg_4893" n="e" s="T635">((BRK)). </ts>
               <ts e="T637" id="Seg_4895" n="e" s="T636">Pʼetux </ts>
               <ts e="T638" id="Seg_4897" n="e" s="T637">amnolaʔbə </ts>
               <ts e="T639" id="Seg_4899" n="e" s="T638">zaborgən. </ts>
               <ts e="T640" id="Seg_4901" n="e" s="T639">Xvost </ts>
               <ts e="T641" id="Seg_4903" n="e" s="T640">dʼügən, </ts>
               <ts e="T642" id="Seg_4905" n="e" s="T641">a </ts>
               <ts e="T643" id="Seg_4907" n="e" s="T642">kirgarlaʔbə </ts>
               <ts e="T644" id="Seg_4909" n="e" s="T643">kudajdə. </ts>
               <ts e="T645" id="Seg_4911" n="e" s="T644">Dĭ </ts>
               <ts e="T646" id="Seg_4913" n="e" s="T645">koŋgoro. </ts>
               <ts e="T647" id="Seg_4915" n="e" s="T646">((BRK)). </ts>
               <ts e="T648" id="Seg_4917" n="e" s="T647">Nuga </ts>
               <ts e="T649" id="Seg_4919" n="e" s="T648">nʼi, </ts>
               <ts e="T650" id="Seg_4921" n="e" s="T649">šüjögən </ts>
               <ts e="T651" id="Seg_4923" n="e" s="T650">(neŋe-) </ts>
               <ts e="T652" id="Seg_4925" n="e" s="T651">nenileʔbə, </ts>
               <ts e="T653" id="Seg_4927" n="e" s="T652">a </ts>
               <ts e="T654" id="Seg_4929" n="e" s="T653">bostə </ts>
               <ts e="T655" id="Seg_4931" n="e" s="T654">bar </ts>
               <ts e="T656" id="Seg_4933" n="e" s="T655">kĭnzleʔbə. </ts>
               <ts e="T657" id="Seg_4935" n="e" s="T656">Dĭ </ts>
               <ts e="T658" id="Seg_4937" n="e" s="T657">kujdərgan. </ts>
               <ts e="T659" id="Seg_4939" n="e" s="T658">((BRK)). </ts>
               <ts e="T660" id="Seg_4941" n="e" s="T659">Šidegöʔ </ts>
               <ts e="T661" id="Seg_4943" n="e" s="T660">kandəgaʔi. </ts>
               <ts e="T662" id="Seg_4945" n="e" s="T661">Onʼiʔ </ts>
               <ts e="T663" id="Seg_4947" n="e" s="T662">kandəga, </ts>
               <ts e="T664" id="Seg_4949" n="e" s="T663">onʼiʔ </ts>
               <ts e="T665" id="Seg_4951" n="e" s="T664">bĭtlie. </ts>
               <ts e="T666" id="Seg_4953" n="e" s="T665">Dĭ </ts>
               <ts e="T667" id="Seg_4955" n="e" s="T666">üjüʔi. </ts>
               <ts e="T668" id="Seg_4957" n="e" s="T667">((BRK)). </ts>
               <ts e="T669" id="Seg_4959" n="e" s="T668">Toʔnaraʔ </ts>
               <ts e="T670" id="Seg_4961" n="e" s="T669">măna, </ts>
               <ts e="T671" id="Seg_4963" n="e" s="T670">münöreʔ </ts>
               <ts e="T672" id="Seg_4965" n="e" s="T671">măna, </ts>
               <ts e="T673" id="Seg_4967" n="e" s="T672">sʼaʔ </ts>
               <ts e="T674" id="Seg_4969" n="e" s="T673">măna, </ts>
               <ts e="T675" id="Seg_4971" n="e" s="T674">ige </ts>
               <ts e="T676" id="Seg_4973" n="e" s="T675">măna. </ts>
               <ts e="T677" id="Seg_4975" n="e" s="T676">Dĭ </ts>
               <ts e="T678" id="Seg_4977" n="e" s="T677">sanə. </ts>
               <ts e="T679" id="Seg_4979" n="e" s="T678">((BRK)). </ts>
               <ts e="T680" id="Seg_4981" n="e" s="T679">Edleʔbə </ts>
               <ts e="T681" id="Seg_4983" n="e" s="T680">bar, </ts>
               <ts e="T682" id="Seg_4985" n="e" s="T681">a </ts>
               <ts e="T683" id="Seg_4987" n="e" s="T682">il </ts>
               <ts e="T684" id="Seg_4989" n="e" s="T683">bar </ts>
               <ts e="T685" id="Seg_4991" n="e" s="T684">kabarlaʔbəʔjə, </ts>
               <ts e="T686" id="Seg_4993" n="e" s="T685">băzəjdlaʔbəʔjə, </ts>
               <ts e="T687" id="Seg_4995" n="e" s="T686">kĭškəleʔbəʔjə. </ts>
               <ts e="T688" id="Seg_4997" n="e" s="T687">Dĭ </ts>
               <ts e="T689" id="Seg_4999" n="e" s="T688">rušnʼik </ts>
               <ts e="T690" id="Seg_5001" n="e" s="T689">ige. </ts>
               <ts e="T691" id="Seg_5003" n="e" s="T690">((BRK)). </ts>
               <ts e="T888" id="Seg_5005" n="e" s="T691">Вокруг </ts>
               <ts e="T692" id="Seg_5007" n="e" s="T888">paʔi </ts>
               <ts e="T693" id="Seg_5009" n="e" s="T692">noʔ </ts>
               <ts e="T694" id="Seg_5011" n="e" s="T693">özerleʔbə </ts>
               <ts e="T696" id="Seg_5013" n="e" s="T694">золотой. </ts>
               <ts e="T697" id="Seg_5015" n="e" s="T696">Dĭ </ts>
               <ts e="T698" id="Seg_5017" n="e" s="T697">müjö. </ts>
               <ts e="T699" id="Seg_5019" n="e" s="T698">((BRK)). </ts>
               <ts e="T700" id="Seg_5021" n="e" s="T699">Kömə </ts>
               <ts e="T701" id="Seg_5023" n="e" s="T700">poʔto </ts>
               <ts e="T702" id="Seg_5025" n="e" s="T701">iʔbolaʔpi, </ts>
               <ts e="T703" id="Seg_5027" n="e" s="T702">dĭn </ts>
               <ts e="T704" id="Seg_5029" n="e" s="T703">noʔ </ts>
               <ts e="T705" id="Seg_5031" n="e" s="T704">ej </ts>
               <ts e="T706" id="Seg_5033" n="e" s="T705">özerleʔbə. </ts>
               <ts e="T707" id="Seg_5035" n="e" s="T706">Šü </ts>
               <ts e="T708" id="Seg_5037" n="e" s="T707">dĭ. </ts>
               <ts e="T709" id="Seg_5039" n="e" s="T708">((BRK)). </ts>
               <ts e="T710" id="Seg_5041" n="e" s="T709">Kambi </ts>
               <ts e="T711" id="Seg_5043" n="e" s="T710">keʔbdejle, </ts>
               <ts e="T712" id="Seg_5045" n="e" s="T711">i </ts>
               <ts e="T713" id="Seg_5047" n="e" s="T712">măn </ts>
               <ts e="T714" id="Seg_5049" n="e" s="T713">kambiam </ts>
               <ts e="T715" id="Seg_5051" n="e" s="T714">keʔbde </ts>
               <ts e="T716" id="Seg_5053" n="e" s="T715">nĭŋgəsʼtə, </ts>
               <ts e="T717" id="Seg_5055" n="e" s="T716">i </ts>
               <ts e="T718" id="Seg_5057" n="e" s="T717">išo </ts>
               <ts e="T719" id="Seg_5059" n="e" s="T718">onʼiʔ </ts>
               <ts e="T720" id="Seg_5061" n="e" s="T719">koʔbdo </ts>
               <ts e="T721" id="Seg_5063" n="e" s="T720">kambi. </ts>
               <ts e="T722" id="Seg_5065" n="e" s="T721">(I- </ts>
               <ts e="T723" id="Seg_5067" n="e" s="T722">ine- </ts>
               <ts e="T724" id="Seg_5069" n="e" s="T723">inezi= </ts>
               <ts e="T725" id="Seg_5071" n="e" s="T724">ine-) </ts>
               <ts e="T726" id="Seg_5073" n="e" s="T725">(Kambila) </ts>
               <ts e="T727" id="Seg_5075" n="e" s="T726">Kambibaʔ </ts>
               <ts e="T728" id="Seg_5077" n="e" s="T727">(šumiganə). </ts>
               <ts e="T729" id="Seg_5079" n="e" s="T728">Dĭbər </ts>
               <ts e="T730" id="Seg_5081" n="e" s="T729">šobibaʔ. </ts>
               <ts e="T731" id="Seg_5083" n="e" s="T730">(Šalaʔjə) </ts>
               <ts e="T732" id="Seg_5085" n="e" s="T731">keʔbde, </ts>
               <ts e="T733" id="Seg_5087" n="e" s="T732">măndorbibaʔ </ts>
               <ts e="T734" id="Seg_5089" n="e" s="T733">i </ts>
               <ts e="T735" id="Seg_5091" n="e" s="T734">idʼiʔeʔe </ts>
               <ts e="T736" id="Seg_5093" n="e" s="T735">oʔbdəbibaʔ. </ts>
               <ts e="T737" id="Seg_5095" n="e" s="T736">Dĭgəttə </ts>
               <ts e="T738" id="Seg_5097" n="e" s="T737">šobibaʔ, </ts>
               <ts e="T739" id="Seg_5099" n="e" s="T738">gijen </ts>
               <ts e="T740" id="Seg_5101" n="e" s="T739">ine </ts>
               <ts e="T741" id="Seg_5103" n="e" s="T740">mĭnleʔbə, </ts>
               <ts e="T742" id="Seg_5105" n="e" s="T741">iam </ts>
               <ts e="T743" id="Seg_5107" n="e" s="T742">măndə: </ts>
               <ts e="T744" id="Seg_5109" n="e" s="T743">Ugandə </ts>
               <ts e="T745" id="Seg_5111" n="e" s="T744">sagər </ts>
               <ts e="T746" id="Seg_5113" n="e" s="T745">туча </ts>
               <ts e="T747" id="Seg_5115" n="e" s="T746">šonəga. </ts>
               <ts e="T748" id="Seg_5117" n="e" s="T747">Nada </ts>
               <ts e="T749" id="Seg_5119" n="e" s="T748">azittə </ts>
               <ts e="T750" id="Seg_5121" n="e" s="T749">maʔ, </ts>
               <ts e="T751" id="Seg_5123" n="e" s="T750">a_to </ts>
               <ts e="T752" id="Seg_5125" n="e" s="T751">kunolzittə </ts>
               <ts e="T753" id="Seg_5127" n="e" s="T752">ej </ts>
               <ts e="T754" id="Seg_5129" n="e" s="T753">jakše </ts>
               <ts e="T755" id="Seg_5131" n="e" s="T754">moləj. </ts>
               <ts e="T756" id="Seg_5133" n="e" s="T755">Gəttə </ts>
               <ts e="T757" id="Seg_5135" n="e" s="T756">maʔ </ts>
               <ts e="T758" id="Seg_5137" n="e" s="T757">abibaʔ. </ts>
               <ts e="T759" id="Seg_5139" n="e" s="T758">Mĭnzərbibeʔ, </ts>
               <ts e="T760" id="Seg_5141" n="e" s="T759">segi </ts>
               <ts e="T761" id="Seg_5143" n="e" s="T760">bü </ts>
               <ts e="T762" id="Seg_5145" n="e" s="T761">bĭʔpibeʔ. </ts>
               <ts e="T763" id="Seg_5147" n="e" s="T762">A </ts>
               <ts e="T764" id="Seg_5149" n="e" s="T763">dĭn </ts>
               <ts e="T765" id="Seg_5151" n="e" s="T764">(был=) </ts>
               <ts e="T766" id="Seg_5153" n="e" s="T765">ibi </ts>
               <ts e="T767" id="Seg_5155" n="e" s="T766">pălăvik </ts>
               <ts e="T768" id="Seg_5157" n="e" s="T767">tĭbər. </ts>
               <ts e="T769" id="Seg_5159" n="e" s="T768">Pălăviktə </ts>
               <ts e="T770" id="Seg_5161" n="e" s="T769">(edəbibiʔibeʔ) </ts>
               <ts e="T771" id="Seg_5163" n="e" s="T770">paʔinə. </ts>
               <ts e="T772" id="Seg_5165" n="e" s="T771">Dĭgəttə </ts>
               <ts e="T773" id="Seg_5167" n="e" s="T772">iʔbəbeʔ </ts>
               <ts e="T774" id="Seg_5169" n="e" s="T773">kunolzittə. </ts>
               <ts e="T775" id="Seg_5171" n="e" s="T774">A </ts>
               <ts e="T776" id="Seg_5173" n="e" s="T775">nüdʼin </ts>
               <ts e="T777" id="Seg_5175" n="e" s="T776">bar </ts>
               <ts e="T778" id="Seg_5177" n="e" s="T777">ugandə </ts>
               <ts e="T779" id="Seg_5179" n="e" s="T778">tăŋ </ts>
               <ts e="T780" id="Seg_5181" n="e" s="T779">surno </ts>
               <ts e="T781" id="Seg_5183" n="e" s="T780">šonəbi, </ts>
               <ts e="T782" id="Seg_5185" n="e" s="T781">šonəbi. </ts>
               <ts e="T783" id="Seg_5187" n="e" s="T782">Ertən </ts>
               <ts e="T784" id="Seg_5189" n="e" s="T783">uʔbdəbibaʔ </ts>
               <ts e="T785" id="Seg_5191" n="e" s="T784">bar, </ts>
               <ts e="T786" id="Seg_5193" n="e" s="T785">nünörbibaʔ. </ts>
               <ts e="T787" id="Seg_5195" n="e" s="T786">Ineʔi </ts>
               <ts e="T788" id="Seg_5197" n="e" s="T787">dʼaʔpibaʔ, </ts>
               <ts e="T789" id="Seg_5199" n="e" s="T788">kambibaʔ </ts>
               <ts e="T790" id="Seg_5201" n="e" s="T789">maʔnʼi. </ts>
               <ts e="T791" id="Seg_5203" n="e" s="T790">(Šobi-) </ts>
               <ts e="T792" id="Seg_5205" n="e" s="T791">Šobibaʔ </ts>
               <ts e="T793" id="Seg_5207" n="e" s="T792">bünə. </ts>
               <ts e="T794" id="Seg_5209" n="e" s="T793">Kădedə </ts>
               <ts e="T795" id="Seg_5211" n="e" s="T794">nʼelʼzʼa </ts>
               <ts e="T796" id="Seg_5213" n="e" s="T795">kanzittə. </ts>
               <ts e="T797" id="Seg_5215" n="e" s="T796">Dĭgəttə </ts>
               <ts e="T798" id="Seg_5217" n="e" s="T797">(nʼ-) </ts>
               <ts e="T799" id="Seg_5219" n="e" s="T798">вниз </ts>
               <ts e="T800" id="Seg_5221" n="e" s="T799">bünə </ts>
               <ts e="T801" id="Seg_5223" n="e" s="T800">kanbibaʔ, </ts>
               <ts e="T802" id="Seg_5225" n="e" s="T801">kanbibaʔ, </ts>
               <ts e="T803" id="Seg_5227" n="e" s="T802">dĭgəttə </ts>
               <ts e="T804" id="Seg_5229" n="e" s="T803">kubibaʔ </ts>
               <ts e="T805" id="Seg_5231" n="e" s="T804">mostə. </ts>
               <ts e="T806" id="Seg_5233" n="e" s="T805">Mostə </ts>
               <ts e="T807" id="Seg_5235" n="e" s="T806">(перей-) </ts>
               <ts e="T808" id="Seg_5237" n="e" s="T807">šobibaʔ </ts>
               <ts e="T809" id="Seg_5239" n="e" s="T808">i </ts>
               <ts e="T810" id="Seg_5241" n="e" s="T809">maʔnʼi </ts>
               <ts e="T811" id="Seg_5243" n="e" s="T810">šobibaʔ. </ts>
               <ts e="T812" id="Seg_5245" n="e" s="T811">Все. </ts>
               <ts e="T813" id="Seg_5247" n="e" s="T812">((BRK)). </ts>
               <ts e="T814" id="Seg_5249" n="e" s="T813">Măn </ts>
               <ts e="T815" id="Seg_5251" n="e" s="T814">teinen </ts>
               <ts e="T816" id="Seg_5253" n="e" s="T815">ertə </ts>
               <ts e="T817" id="Seg_5255" n="e" s="T816">uʔbdəbiam. </ts>
               <ts e="T818" id="Seg_5257" n="e" s="T817">Măgăzində </ts>
               <ts e="T819" id="Seg_5259" n="e" s="T818">kambiam. </ts>
               <ts e="T889" id="Seg_5261" n="e" s="T819">Очередь </ts>
               <ts e="T821" id="Seg_5263" n="e" s="T889">nubiam. </ts>
               <ts e="T822" id="Seg_5265" n="e" s="T821">Dĭgəttə </ts>
               <ts e="T823" id="Seg_5267" n="e" s="T822">(šide=) </ts>
               <ts e="T824" id="Seg_5269" n="e" s="T823">šide </ts>
               <ts e="T825" id="Seg_5271" n="e" s="T824">čas </ts>
               <ts e="T826" id="Seg_5273" n="e" s="T825">nubiam, </ts>
               <ts e="T827" id="Seg_5275" n="e" s="T826">dĭgəttə </ts>
               <ts e="T828" id="Seg_5277" n="e" s="T827">ibiem </ts>
               <ts e="T829" id="Seg_5279" n="e" s="T828">kola </ts>
               <ts e="T830" id="Seg_5281" n="e" s="T829">i </ts>
               <ts e="T831" id="Seg_5283" n="e" s="T830">šobiam </ts>
               <ts e="T832" id="Seg_5285" n="e" s="T831">maʔnə. </ts>
               <ts e="T833" id="Seg_5287" n="e" s="T832">I </ts>
               <ts e="T834" id="Seg_5289" n="e" s="T833">šiʔ </ts>
               <ts e="T835" id="Seg_5291" n="e" s="T834">šobilaʔ. </ts>
               <ts e="T836" id="Seg_5293" n="e" s="T835">((BRK)). </ts>
               <ts e="T837" id="Seg_5295" n="e" s="T836">Gəttə </ts>
               <ts e="T838" id="Seg_5297" n="e" s="T837">măn </ts>
               <ts e="T839" id="Seg_5299" n="e" s="T838">ibiem </ts>
               <ts e="T840" id="Seg_5301" n="e" s="T839">oʔb </ts>
               <ts e="T841" id="Seg_5303" n="e" s="T840">kilo, </ts>
               <ts e="T842" id="Seg_5305" n="e" s="T841">aktʼa </ts>
               <ts e="T843" id="Seg_5307" n="e" s="T842">mĭbiem </ts>
               <ts e="T844" id="Seg_5309" n="e" s="T843">onʼiʔ </ts>
               <ts e="T845" id="Seg_5311" n="e" s="T844">šălkovej. </ts>
               <ts e="T846" id="Seg_5313" n="e" s="T845">Bʼeʔ </ts>
               <ts e="T847" id="Seg_5315" n="e" s="T846">teʔtə </ts>
               <ts e="T848" id="Seg_5317" n="e" s="T847">mĭbiem. </ts>
               <ts e="T849" id="Seg_5319" n="e" s="T848">Onʼiʔ </ts>
               <ts e="T850" id="Seg_5321" n="e" s="T849">šălkovej </ts>
               <ts e="T851" id="Seg_5323" n="e" s="T850">bʼeʔ </ts>
               <ts e="T852" id="Seg_5325" n="e" s="T851">teʔtə. </ts>
               <ts e="T853" id="Seg_5327" n="e" s="T852">((BRK)). </ts>
               <ts e="T854" id="Seg_5329" n="e" s="T853">Dĭn </ts>
               <ts e="T855" id="Seg_5331" n="e" s="T854">bar </ts>
               <ts e="T856" id="Seg_5333" n="e" s="T855">nezeŋ </ts>
               <ts e="T857" id="Seg_5335" n="e" s="T856">kudonzlaʔbəʔjə, </ts>
               <ts e="T858" id="Seg_5337" n="e" s="T857">kirgarlaʔbəʔjə. </ts>
               <ts e="T859" id="Seg_5339" n="e" s="T858">Mănliaʔi: </ts>
               <ts e="T860" id="Seg_5341" n="e" s="T859">Amga </ts>
               <ts e="T861" id="Seg_5343" n="e" s="T860">kola. </ts>
               <ts e="T862" id="Seg_5345" n="e" s="T861">(Onʼiʔ </ts>
               <ts e="T863" id="Seg_5347" n="e" s="T862">k-) </ts>
               <ts e="T864" id="Seg_5349" n="e" s="T863">Onʼiʔ </ts>
               <ts e="T865" id="Seg_5351" n="e" s="T864">kilo </ts>
               <ts e="T866" id="Seg_5353" n="e" s="T865">mĭleʔbə. </ts>
               <ts e="T867" id="Seg_5355" n="e" s="T866">Dĭ </ts>
               <ts e="T868" id="Seg_5357" n="e" s="T867">mĭbi </ts>
               <ts e="T869" id="Seg_5359" n="e" s="T868">onʼiʔ </ts>
               <ts e="T870" id="Seg_5361" n="e" s="T869">bar </ts>
               <ts e="T871" id="Seg_5363" n="e" s="T870">nezeŋdə, </ts>
               <ts e="T872" id="Seg_5365" n="e" s="T871">onʼiʔ </ts>
               <ts e="T873" id="Seg_5367" n="e" s="T872">kilo </ts>
               <ts e="T874" id="Seg_5369" n="e" s="T873">mĭbi. </ts>
               <ts e="T875" id="Seg_5371" n="e" s="T874">Mănliaʔi: </ts>
               <ts e="T876" id="Seg_5373" n="e" s="T875">Esseŋbə </ts>
               <ts e="T877" id="Seg_5375" n="e" s="T876">kirgarlaʔbəʔjə. </ts>
               <ts e="T878" id="Seg_5377" n="e" s="T877">(Kola=) </ts>
               <ts e="T879" id="Seg_5379" n="e" s="T878">Kola </ts>
               <ts e="T880" id="Seg_5381" n="e" s="T879">amzittə </ts>
               <ts e="T881" id="Seg_5383" n="e" s="T880">kereʔ </ts>
               <ts e="T882" id="Seg_5385" n="e" s="T881">dĭzeŋdə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T5" id="Seg_5386" s="T0">PKZ_196X_SU0223.PKZ.001 (001)</ta>
            <ta e="T12" id="Seg_5387" s="T5">PKZ_196X_SU0223.PKZ.002 (002)</ta>
            <ta e="T18" id="Seg_5388" s="T12">PKZ_196X_SU0223.PKZ.003 (003)</ta>
            <ta e="T23" id="Seg_5389" s="T18">PKZ_196X_SU0223.PKZ.004 (004)</ta>
            <ta e="T27" id="Seg_5390" s="T23">PKZ_196X_SU0223.PKZ.005 (005)</ta>
            <ta e="T32" id="Seg_5391" s="T27">PKZ_196X_SU0223.PKZ.006 (006)</ta>
            <ta e="T36" id="Seg_5392" s="T32">PKZ_196X_SU0223.PKZ.007 (007)</ta>
            <ta e="T45" id="Seg_5393" s="T36">PKZ_196X_SU0223.PKZ.008 (008)</ta>
            <ta e="T47" id="Seg_5394" s="T45">PKZ_196X_SU0223.PKZ.009 (009)</ta>
            <ta e="T49" id="Seg_5395" s="T47">PKZ_196X_SU0223.PKZ.010 (010)</ta>
            <ta e="T53" id="Seg_5396" s="T49">PKZ_196X_SU0223.PKZ.011 (011)</ta>
            <ta e="T60" id="Seg_5397" s="T53">PKZ_196X_SU0223.PKZ.012 (012)</ta>
            <ta e="T68" id="Seg_5398" s="T60">PKZ_196X_SU0223.PKZ.013 (013)</ta>
            <ta e="T74" id="Seg_5399" s="T68">PKZ_196X_SU0223.PKZ.014 (014)</ta>
            <ta e="T77" id="Seg_5400" s="T74">PKZ_196X_SU0223.PKZ.015 (015)</ta>
            <ta e="T83" id="Seg_5401" s="T77">PKZ_196X_SU0223.PKZ.016 (016)</ta>
            <ta e="T86" id="Seg_5402" s="T83">PKZ_196X_SU0223.PKZ.017 (017)</ta>
            <ta e="T93" id="Seg_5403" s="T86">PKZ_196X_SU0223.PKZ.018 (018)</ta>
            <ta e="T96" id="Seg_5404" s="T93">PKZ_196X_SU0223.PKZ.019 (019)</ta>
            <ta e="T101" id="Seg_5405" s="T96">PKZ_196X_SU0223.PKZ.020 (020)</ta>
            <ta e="T106" id="Seg_5406" s="T101">PKZ_196X_SU0223.PKZ.021 (021)</ta>
            <ta e="T109" id="Seg_5407" s="T106">PKZ_196X_SU0223.PKZ.022 (022)</ta>
            <ta e="T116" id="Seg_5408" s="T109">PKZ_196X_SU0223.PKZ.023 (023)</ta>
            <ta e="T120" id="Seg_5409" s="T116">PKZ_196X_SU0223.PKZ.024 (024)</ta>
            <ta e="T124" id="Seg_5410" s="T120">PKZ_196X_SU0223.PKZ.025 (025)</ta>
            <ta e="T128" id="Seg_5411" s="T124">PKZ_196X_SU0223.PKZ.026 (026)</ta>
            <ta e="T134" id="Seg_5412" s="T128">PKZ_196X_SU0223.PKZ.027 (027)</ta>
            <ta e="T142" id="Seg_5413" s="T134">PKZ_196X_SU0223.PKZ.028 (028)</ta>
            <ta e="T146" id="Seg_5414" s="T142">PKZ_196X_SU0223.PKZ.029 (029)</ta>
            <ta e="T152" id="Seg_5415" s="T146">PKZ_196X_SU0223.PKZ.030 (030)</ta>
            <ta e="T156" id="Seg_5416" s="T152">PKZ_196X_SU0223.PKZ.031 (031)</ta>
            <ta e="T160" id="Seg_5417" s="T156">PKZ_196X_SU0223.PKZ.032 (032)</ta>
            <ta e="T163" id="Seg_5418" s="T160">PKZ_196X_SU0223.PKZ.033 (033)</ta>
            <ta e="T169" id="Seg_5419" s="T163">PKZ_196X_SU0223.PKZ.034 (034)</ta>
            <ta e="T172" id="Seg_5420" s="T169">PKZ_196X_SU0223.PKZ.035 (035)</ta>
            <ta e="T181" id="Seg_5421" s="T172">PKZ_196X_SU0223.PKZ.036 (036)</ta>
            <ta e="T184" id="Seg_5422" s="T181">PKZ_196X_SU0223.PKZ.037 (037)</ta>
            <ta e="T187" id="Seg_5423" s="T184">PKZ_196X_SU0223.PKZ.038 (038)</ta>
            <ta e="T200" id="Seg_5424" s="T187">PKZ_196X_SU0223.PKZ.039 (039)</ta>
            <ta e="T202" id="Seg_5425" s="T200">PKZ_196X_SU0223.PKZ.040 (040)</ta>
            <ta e="T207" id="Seg_5426" s="T202">PKZ_196X_SU0223.PKZ.041 (041)</ta>
            <ta e="T214" id="Seg_5427" s="T207">PKZ_196X_SU0223.PKZ.042 (042)</ta>
            <ta e="T217" id="Seg_5428" s="T214">PKZ_196X_SU0223.PKZ.043 (043)</ta>
            <ta e="T223" id="Seg_5429" s="T217">PKZ_196X_SU0223.PKZ.044 (044)</ta>
            <ta e="T226" id="Seg_5430" s="T223">PKZ_196X_SU0223.PKZ.045 (045)</ta>
            <ta e="T230" id="Seg_5431" s="T226">PKZ_196X_SU0223.PKZ.046 (046)</ta>
            <ta e="T234" id="Seg_5432" s="T230">PKZ_196X_SU0223.PKZ.047 (047)</ta>
            <ta e="T242" id="Seg_5433" s="T234">PKZ_196X_SU0223.PKZ.048 (048)</ta>
            <ta e="T247" id="Seg_5434" s="T242">PKZ_196X_SU0223.PKZ.049 (049)</ta>
            <ta e="T253" id="Seg_5435" s="T247">PKZ_196X_SU0223.PKZ.050 (050)</ta>
            <ta e="T262" id="Seg_5436" s="T253">PKZ_196X_SU0223.PKZ.051 (051)</ta>
            <ta e="T264" id="Seg_5437" s="T262">PKZ_196X_SU0223.PKZ.052 (052)</ta>
            <ta e="T269" id="Seg_5438" s="T264">PKZ_196X_SU0223.PKZ.053 (053)</ta>
            <ta e="T279" id="Seg_5439" s="T269">PKZ_196X_SU0223.PKZ.054 (055) </ta>
            <ta e="T283" id="Seg_5440" s="T279">PKZ_196X_SU0223.PKZ.055 (057)</ta>
            <ta e="T292" id="Seg_5441" s="T283">PKZ_196X_SU0223.PKZ.056 (058)</ta>
            <ta e="T296" id="Seg_5442" s="T292">PKZ_196X_SU0223.PKZ.057 (059)</ta>
            <ta e="T297" id="Seg_5443" s="T296">PKZ_196X_SU0223.PKZ.058 (060)</ta>
            <ta e="T301" id="Seg_5444" s="T297">PKZ_196X_SU0223.PKZ.059 (061)</ta>
            <ta e="T304" id="Seg_5445" s="T301">PKZ_196X_SU0223.PKZ.060 (062)</ta>
            <ta e="T305" id="Seg_5446" s="T304">PKZ_196X_SU0223.PKZ.061 (063)</ta>
            <ta e="T306" id="Seg_5447" s="T305">PKZ_196X_SU0223.PKZ.062 (064)</ta>
            <ta e="T308" id="Seg_5448" s="T306">PKZ_196X_SU0223.PKZ.063 (065)</ta>
            <ta e="T309" id="Seg_5449" s="T308">PKZ_196X_SU0223.PKZ.064 (066)</ta>
            <ta e="T312" id="Seg_5450" s="T309">PKZ_196X_SU0223.PKZ.065 (067)</ta>
            <ta e="T314" id="Seg_5451" s="T312">PKZ_196X_SU0223.PKZ.066 (068)</ta>
            <ta e="T315" id="Seg_5452" s="T314">PKZ_196X_SU0223.PKZ.067 (069)</ta>
            <ta e="T316" id="Seg_5453" s="T315">PKZ_196X_SU0223.PKZ.068 (070)</ta>
            <ta e="T326" id="Seg_5454" s="T316">PKZ_196X_SU0223.PKZ.069 (071)</ta>
            <ta e="T328" id="Seg_5455" s="T326">PKZ_196X_SU0223.PKZ.070 (072)</ta>
            <ta e="T329" id="Seg_5456" s="T328">PKZ_196X_SU0223.PKZ.071 (073)</ta>
            <ta e="T333" id="Seg_5457" s="T329">PKZ_196X_SU0223.PKZ.072 (074)</ta>
            <ta e="T334" id="Seg_5458" s="T333">PKZ_196X_SU0223.PKZ.073 (075)</ta>
            <ta e="T340" id="Seg_5459" s="T334">PKZ_196X_SU0223.PKZ.074 (076)</ta>
            <ta e="T343" id="Seg_5460" s="T340">PKZ_196X_SU0223.PKZ.075 (077)</ta>
            <ta e="T344" id="Seg_5461" s="T343">PKZ_196X_SU0223.PKZ.076 (078)</ta>
            <ta e="T349" id="Seg_5462" s="T344">PKZ_196X_SU0223.PKZ.077 (079)</ta>
            <ta e="T352" id="Seg_5463" s="T349">PKZ_196X_SU0223.PKZ.078 (080)</ta>
            <ta e="T355" id="Seg_5464" s="T352">PKZ_196X_SU0223.PKZ.079 (081)</ta>
            <ta e="T357" id="Seg_5465" s="T355">PKZ_196X_SU0223.PKZ.080 (082)</ta>
            <ta e="T358" id="Seg_5466" s="T357">PKZ_196X_SU0223.PKZ.081 (083)</ta>
            <ta e="T364" id="Seg_5467" s="T358">PKZ_196X_SU0223.PKZ.082 (084)</ta>
            <ta e="T372" id="Seg_5468" s="T365">PKZ_196X_SU0223.PKZ.083 (086)</ta>
            <ta e="T374" id="Seg_5469" s="T372">PKZ_196X_SU0223.PKZ.084 (087)</ta>
            <ta e="T375" id="Seg_5470" s="T374">PKZ_196X_SU0223.PKZ.085 (088)</ta>
            <ta e="T380" id="Seg_5471" s="T375">PKZ_196X_SU0223.PKZ.086 (089)</ta>
            <ta e="T385" id="Seg_5472" s="T380">PKZ_196X_SU0223.PKZ.087 (090)</ta>
            <ta e="T386" id="Seg_5473" s="T385">PKZ_196X_SU0223.PKZ.088 (092)</ta>
            <ta e="T392" id="Seg_5474" s="T386">PKZ_196X_SU0223.PKZ.089 (093)</ta>
            <ta e="T393" id="Seg_5475" s="T392">PKZ_196X_SU0223.PKZ.090 (094)</ta>
            <ta e="T395" id="Seg_5476" s="T393">PKZ_196X_SU0223.PKZ.091 (095)</ta>
            <ta e="T396" id="Seg_5477" s="T395">PKZ_196X_SU0223.PKZ.092 (096)</ta>
            <ta e="T403" id="Seg_5478" s="T396">PKZ_196X_SU0223.PKZ.093 (097)</ta>
            <ta e="T405" id="Seg_5479" s="T403">PKZ_196X_SU0223.PKZ.094 (098)</ta>
            <ta e="T406" id="Seg_5480" s="T405">PKZ_196X_SU0223.PKZ.095 (099)</ta>
            <ta e="T409" id="Seg_5481" s="T406">PKZ_196X_SU0223.PKZ.096 (100)</ta>
            <ta e="T418" id="Seg_5482" s="T409">PKZ_196X_SU0223.PKZ.097 (101)</ta>
            <ta e="T420" id="Seg_5483" s="T418">PKZ_196X_SU0223.PKZ.098 (102)</ta>
            <ta e="T421" id="Seg_5484" s="T420">PKZ_196X_SU0223.PKZ.099 (103)</ta>
            <ta e="T424" id="Seg_5485" s="T421">PKZ_196X_SU0223.PKZ.100 (104)</ta>
            <ta e="T428" id="Seg_5486" s="T424">PKZ_196X_SU0223.PKZ.101 (105)</ta>
            <ta e="T430" id="Seg_5487" s="T428">PKZ_196X_SU0223.PKZ.102 (106)</ta>
            <ta e="T431" id="Seg_5488" s="T430">PKZ_196X_SU0223.PKZ.103 (107)</ta>
            <ta e="T437" id="Seg_5489" s="T431">PKZ_196X_SU0223.PKZ.104 (108)</ta>
            <ta e="T445" id="Seg_5490" s="T437">PKZ_196X_SU0223.PKZ.105 (109)</ta>
            <ta e="T446" id="Seg_5491" s="T445">PKZ_196X_SU0223.PKZ.106 (110)</ta>
            <ta e="T450" id="Seg_5492" s="T446">PKZ_196X_SU0223.PKZ.107 (111)</ta>
            <ta e="T452" id="Seg_5493" s="T450">PKZ_196X_SU0223.PKZ.108 (112)</ta>
            <ta e="T453" id="Seg_5494" s="T452">PKZ_196X_SU0223.PKZ.109 (113)</ta>
            <ta e="T456" id="Seg_5495" s="T453">PKZ_196X_SU0223.PKZ.110 (114)</ta>
            <ta e="T460" id="Seg_5496" s="T456">PKZ_196X_SU0223.PKZ.111 (115)</ta>
            <ta e="T464" id="Seg_5497" s="T460">PKZ_196X_SU0223.PKZ.112 (116)</ta>
            <ta e="T466" id="Seg_5498" s="T464">PKZ_196X_SU0223.PKZ.113 (117)</ta>
            <ta e="T477" id="Seg_5499" s="T466">PKZ_196X_SU0223.PKZ.114 (118)</ta>
            <ta e="T479" id="Seg_5500" s="T477">PKZ_196X_SU0223.PKZ.115 (119)</ta>
            <ta e="T480" id="Seg_5501" s="T479">PKZ_196X_SU0223.PKZ.116 (120)</ta>
            <ta e="T484" id="Seg_5502" s="T480">PKZ_196X_SU0223.PKZ.117 (121)</ta>
            <ta e="T491" id="Seg_5503" s="T484">PKZ_196X_SU0223.PKZ.118 (122)</ta>
            <ta e="T493" id="Seg_5504" s="T492">PKZ_196X_SU0223.PKZ.119 (124)</ta>
            <ta e="T494" id="Seg_5505" s="T493">PKZ_196X_SU0223.PKZ.120 (125)</ta>
            <ta e="T499" id="Seg_5506" s="T494">PKZ_196X_SU0223.PKZ.121 (126)</ta>
            <ta e="T502" id="Seg_5507" s="T499">PKZ_196X_SU0223.PKZ.122 (127)</ta>
            <ta e="T503" id="Seg_5508" s="T502">PKZ_196X_SU0223.PKZ.123 (128)</ta>
            <ta e="T513" id="Seg_5509" s="T503">PKZ_196X_SU0223.PKZ.124 (129)</ta>
            <ta e="T514" id="Seg_5510" s="T513">PKZ_196X_SU0223.PKZ.125 (130)</ta>
            <ta e="T517" id="Seg_5511" s="T514">PKZ_196X_SU0223.PKZ.126 (131)</ta>
            <ta e="T520" id="Seg_5512" s="T517">PKZ_196X_SU0223.PKZ.127 (132)</ta>
            <ta e="T525" id="Seg_5513" s="T520">PKZ_196X_SU0223.PKZ.128 (133)</ta>
            <ta e="T527" id="Seg_5514" s="T525">PKZ_196X_SU0223.PKZ.129 (134)</ta>
            <ta e="T528" id="Seg_5515" s="T527">PKZ_196X_SU0223.PKZ.130 (135)</ta>
            <ta e="T532" id="Seg_5516" s="T528">PKZ_196X_SU0223.PKZ.131 (136)</ta>
            <ta e="T533" id="Seg_5517" s="T532">PKZ_196X_SU0223.PKZ.132 (137)</ta>
            <ta e="T539" id="Seg_5518" s="T533">PKZ_196X_SU0223.PKZ.133 (138)</ta>
            <ta e="T546" id="Seg_5519" s="T539">PKZ_196X_SU0223.PKZ.134 (140)</ta>
            <ta e="T548" id="Seg_5520" s="T546">PKZ_196X_SU0223.PKZ.135 (141)</ta>
            <ta e="T549" id="Seg_5521" s="T548">PKZ_196X_SU0223.PKZ.136 (142)</ta>
            <ta e="T554" id="Seg_5522" s="T549">PKZ_196X_SU0223.PKZ.137 (143)</ta>
            <ta e="T557" id="Seg_5523" s="T554">PKZ_196X_SU0223.PKZ.138 (144)</ta>
            <ta e="T559" id="Seg_5524" s="T557">PKZ_196X_SU0223.PKZ.139 (145)</ta>
            <ta e="T560" id="Seg_5525" s="T559">PKZ_196X_SU0223.PKZ.140 (146)</ta>
            <ta e="T567" id="Seg_5526" s="T560">PKZ_196X_SU0223.PKZ.141 (147)</ta>
            <ta e="T570" id="Seg_5527" s="T567">PKZ_196X_SU0223.PKZ.142 (148)</ta>
            <ta e="T571" id="Seg_5528" s="T570">PKZ_196X_SU0223.PKZ.143 (149)</ta>
            <ta e="T572" id="Seg_5529" s="T571">PKZ_196X_SU0223.PKZ.144 (150)</ta>
            <ta e="T581" id="Seg_5530" s="T572">PKZ_196X_SU0223.PKZ.145 (151)</ta>
            <ta e="T582" id="Seg_5531" s="T581">PKZ_196X_SU0223.PKZ.146 (152)</ta>
            <ta e="T585" id="Seg_5532" s="T582">PKZ_196X_SU0223.PKZ.147 (153)</ta>
            <ta e="T586" id="Seg_5533" s="T585">PKZ_196X_SU0223.PKZ.148 (154)</ta>
            <ta e="T598" id="Seg_5534" s="T586">PKZ_196X_SU0223.PKZ.149 (155)</ta>
            <ta e="T601" id="Seg_5535" s="T598">PKZ_196X_SU0223.PKZ.150 (156)</ta>
            <ta e="T602" id="Seg_5536" s="T601">PKZ_196X_SU0223.PKZ.151 (157)</ta>
            <ta e="T611" id="Seg_5537" s="T602">PKZ_196X_SU0223.PKZ.152 (158)</ta>
            <ta e="T615" id="Seg_5538" s="T611">PKZ_196X_SU0223.PKZ.153 (160)</ta>
            <ta e="T616" id="Seg_5539" s="T615">PKZ_196X_SU0223.PKZ.154 (161)</ta>
            <ta e="T624" id="Seg_5540" s="T616">PKZ_196X_SU0223.PKZ.155 (162)</ta>
            <ta e="T625" id="Seg_5541" s="T624">PKZ_196X_SU0223.PKZ.156 (163)</ta>
            <ta e="T626" id="Seg_5542" s="T625">PKZ_196X_SU0223.PKZ.157 (164)</ta>
            <ta e="T635" id="Seg_5543" s="T626">PKZ_196X_SU0223.PKZ.158 (165)</ta>
            <ta e="T636" id="Seg_5544" s="T635">PKZ_196X_SU0223.PKZ.159 (166)</ta>
            <ta e="T639" id="Seg_5545" s="T636">PKZ_196X_SU0223.PKZ.160 (167)</ta>
            <ta e="T644" id="Seg_5546" s="T639">PKZ_196X_SU0223.PKZ.161 (168)</ta>
            <ta e="T646" id="Seg_5547" s="T644">PKZ_196X_SU0223.PKZ.162 (169)</ta>
            <ta e="T647" id="Seg_5548" s="T646">PKZ_196X_SU0223.PKZ.163 (170)</ta>
            <ta e="T656" id="Seg_5549" s="T647">PKZ_196X_SU0223.PKZ.164 (171)</ta>
            <ta e="T658" id="Seg_5550" s="T656">PKZ_196X_SU0223.PKZ.165 (172)</ta>
            <ta e="T659" id="Seg_5551" s="T658">PKZ_196X_SU0223.PKZ.166 (173)</ta>
            <ta e="T661" id="Seg_5552" s="T659">PKZ_196X_SU0223.PKZ.167 (174)</ta>
            <ta e="T665" id="Seg_5553" s="T661">PKZ_196X_SU0223.PKZ.168 (175)</ta>
            <ta e="T667" id="Seg_5554" s="T665">PKZ_196X_SU0223.PKZ.169 (176)</ta>
            <ta e="T668" id="Seg_5555" s="T667">PKZ_196X_SU0223.PKZ.170 (177)</ta>
            <ta e="T676" id="Seg_5556" s="T668">PKZ_196X_SU0223.PKZ.171 (178)</ta>
            <ta e="T678" id="Seg_5557" s="T676">PKZ_196X_SU0223.PKZ.172 (179)</ta>
            <ta e="T679" id="Seg_5558" s="T678">PKZ_196X_SU0223.PKZ.173 (180)</ta>
            <ta e="T687" id="Seg_5559" s="T679">PKZ_196X_SU0223.PKZ.174 (181)</ta>
            <ta e="T690" id="Seg_5560" s="T687">PKZ_196X_SU0223.PKZ.175 (182)</ta>
            <ta e="T691" id="Seg_5561" s="T690">PKZ_196X_SU0223.PKZ.176 (183)</ta>
            <ta e="T696" id="Seg_5562" s="T691">PKZ_196X_SU0223.PKZ.177 (184)</ta>
            <ta e="T698" id="Seg_5563" s="T696">PKZ_196X_SU0223.PKZ.178 (185)</ta>
            <ta e="T699" id="Seg_5564" s="T698">PKZ_196X_SU0223.PKZ.179 (186)</ta>
            <ta e="T706" id="Seg_5565" s="T699">PKZ_196X_SU0223.PKZ.180 (187)</ta>
            <ta e="T708" id="Seg_5566" s="T706">PKZ_196X_SU0223.PKZ.181 (188)</ta>
            <ta e="T709" id="Seg_5567" s="T708">PKZ_196X_SU0223.PKZ.182 (189)</ta>
            <ta e="T721" id="Seg_5568" s="T709">PKZ_196X_SU0223.PKZ.183 (190)</ta>
            <ta e="T728" id="Seg_5569" s="T721">PKZ_196X_SU0223.PKZ.184 (191)</ta>
            <ta e="T730" id="Seg_5570" s="T728">PKZ_196X_SU0223.PKZ.185 (192)</ta>
            <ta e="T736" id="Seg_5571" s="T730">PKZ_196X_SU0223.PKZ.186 (193)</ta>
            <ta e="T747" id="Seg_5572" s="T736">PKZ_196X_SU0223.PKZ.187 (194) </ta>
            <ta e="T755" id="Seg_5573" s="T747">PKZ_196X_SU0223.PKZ.188 (196)</ta>
            <ta e="T758" id="Seg_5574" s="T755">PKZ_196X_SU0223.PKZ.189 (197)</ta>
            <ta e="T762" id="Seg_5575" s="T758">PKZ_196X_SU0223.PKZ.190 (198)</ta>
            <ta e="T768" id="Seg_5576" s="T762">PKZ_196X_SU0223.PKZ.191 (199)</ta>
            <ta e="T771" id="Seg_5577" s="T768">PKZ_196X_SU0223.PKZ.192 (200)</ta>
            <ta e="T774" id="Seg_5578" s="T771">PKZ_196X_SU0223.PKZ.193 (201)</ta>
            <ta e="T782" id="Seg_5579" s="T774">PKZ_196X_SU0223.PKZ.194 (202)</ta>
            <ta e="T786" id="Seg_5580" s="T782">PKZ_196X_SU0223.PKZ.195 (203)</ta>
            <ta e="T790" id="Seg_5581" s="T786">PKZ_196X_SU0223.PKZ.196 (204)</ta>
            <ta e="T793" id="Seg_5582" s="T790">PKZ_196X_SU0223.PKZ.197 (205)</ta>
            <ta e="T796" id="Seg_5583" s="T793">PKZ_196X_SU0223.PKZ.198 (206)</ta>
            <ta e="T805" id="Seg_5584" s="T796">PKZ_196X_SU0223.PKZ.199 (207)</ta>
            <ta e="T811" id="Seg_5585" s="T805">PKZ_196X_SU0223.PKZ.200 (208)</ta>
            <ta e="T812" id="Seg_5586" s="T811">PKZ_196X_SU0223.PKZ.201 (209)</ta>
            <ta e="T813" id="Seg_5587" s="T812">PKZ_196X_SU0223.PKZ.202 (210)</ta>
            <ta e="T817" id="Seg_5588" s="T813">PKZ_196X_SU0223.PKZ.203 (211)</ta>
            <ta e="T819" id="Seg_5589" s="T817">PKZ_196X_SU0223.PKZ.204 (212)</ta>
            <ta e="T821" id="Seg_5590" s="T819">PKZ_196X_SU0223.PKZ.205 (213)</ta>
            <ta e="T832" id="Seg_5591" s="T821">PKZ_196X_SU0223.PKZ.206 (214)</ta>
            <ta e="T835" id="Seg_5592" s="T832">PKZ_196X_SU0223.PKZ.207 (215)</ta>
            <ta e="T836" id="Seg_5593" s="T835">PKZ_196X_SU0223.PKZ.208 (216)</ta>
            <ta e="T845" id="Seg_5594" s="T836">PKZ_196X_SU0223.PKZ.209 (217)</ta>
            <ta e="T848" id="Seg_5595" s="T845">PKZ_196X_SU0223.PKZ.210 (218)</ta>
            <ta e="T852" id="Seg_5596" s="T848">PKZ_196X_SU0223.PKZ.211 (219)</ta>
            <ta e="T853" id="Seg_5597" s="T852">PKZ_196X_SU0223.PKZ.212 (220)</ta>
            <ta e="T858" id="Seg_5598" s="T853">PKZ_196X_SU0223.PKZ.213 (221)</ta>
            <ta e="T861" id="Seg_5599" s="T858">PKZ_196X_SU0223.PKZ.214 (222)</ta>
            <ta e="T866" id="Seg_5600" s="T861">PKZ_196X_SU0223.PKZ.215 (223)</ta>
            <ta e="T874" id="Seg_5601" s="T866">PKZ_196X_SU0223.PKZ.216 (224)</ta>
            <ta e="T877" id="Seg_5602" s="T874">PKZ_196X_SU0223.PKZ.217 (225)</ta>
            <ta e="T882" id="Seg_5603" s="T877">PKZ_196X_SU0223.PKZ.218 (226)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T5" id="Seg_5604" s="T0">Dĭgəttə dĭ nʼizeŋ aktʼinə kambiʔi. </ta>
            <ta e="T12" id="Seg_5605" s="T5">Šobiʔi, šobiʔi, dĭgəttə (aʔtʼi=) aʔtʼigən toʔndə amnobiʔi. </ta>
            <ta e="T18" id="Seg_5606" s="T12">Kuliaʔi: šonəga kăldun onʼiʔ, ine kömə. </ta>
            <ta e="T23" id="Seg_5607" s="T18">Dĭgəttə dĭ bar tăŋ … </ta>
            <ta e="T27" id="Seg_5608" s="T23">Dĭgəttə onʼiʔ свистнул tăŋ. </ta>
            <ta e="T32" id="Seg_5609" s="T27">Inet bar (s-) kălʼenkanə (уп-). </ta>
            <ta e="T36" id="Seg_5610" s="T32">Dĭ ine saʔməluʔbi kălʼenkaʔinə. </ta>
            <ta e="T45" id="Seg_5611" s="T36">Dĭgəttə dĭ găldun (măndəbi):" Dĭ dĭn (e-) enəidənə amnolaʔbə. </ta>
            <ta e="T47" id="Seg_5612" s="T45">Ugaːndə pimniem!" </ta>
            <ta e="T49" id="Seg_5613" s="T47">Dĭgəttə kambi. </ta>
            <ta e="T53" id="Seg_5614" s="T49">Amnolaʔbəʔjə, amnolaʔbəʔjə, bazoʔ šonəga. </ta>
            <ta e="T60" id="Seg_5615" s="T53">Sagər inetsiʔ (oʔ-) (baška) (šide=) šidegit găldun. </ta>
            <ta e="T68" id="Seg_5616" s="T60">Dĭgəttə dĭzeŋ bazoʔ свистнул, gəttə dĭ saʔməluʔbi kălʼenkaʔinə. </ta>
            <ta e="T74" id="Seg_5617" s="T68">Tĭ tože măndə:" Enəidənə ugandə, kuštü. </ta>
            <ta e="T77" id="Seg_5618" s="T74">Ugandə măn pimniem". </ta>
            <ta e="T83" id="Seg_5619" s="T77">Dĭgəttə (u-) (ine=), inet uʔbdəbi, kambi. </ta>
            <ta e="T86" id="Seg_5620" s="T83">Gəttə amnolaʔbəʔjə, amnolaʔbəʔjə. </ta>
            <ta e="T93" id="Seg_5621" s="T86">(Na-) Nagurgət ine šonəga, dö ine sĭri. </ta>
            <ta e="T96" id="Seg_5622" s="T93">Dĭzeŋ bazoʔ свистнули. </ta>
            <ta e="T101" id="Seg_5623" s="T96">Dĭgəttə dĭ ine nuldəbi, nulaʔbə. </ta>
            <ta e="T106" id="Seg_5624" s="T101">Dĭ kăldun (s-) kambi dĭzeŋdə. </ta>
            <ta e="T109" id="Seg_5625" s="T106">"Girgit esseŋ amnolaʔbəʔjə?" </ta>
            <ta e="T116" id="Seg_5626" s="T109">"Da miʔ ot, miʔnʼibeʔ bar bünə barəʔluʔpiʔi. </ta>
            <ta e="T120" id="Seg_5627" s="T116">(Măn=) Miʔ döbər šobibaʔ". </ta>
            <ta e="T124" id="Seg_5628" s="T120">Dĭgəttə:" No, amnogaʔ dön". </ta>
            <ta e="T128" id="Seg_5629" s="T124">Dĭ dĭʔnə (abiʔi =)… </ta>
            <ta e="T134" id="Seg_5630" s="T128">Abi maʔ, ipek mĭbi, uja mĭbi. </ta>
            <ta e="T142" id="Seg_5631" s="T134">"Amnogaʔ dön, măn parlam, dʼili molam, šiʔnʼileʔ ilim". </ta>
            <ta e="T146" id="Seg_5632" s="T142">Dĭgəttə dĭzeŋ amnolaʔpiʔi, amnolaʔpi. </ta>
            <ta e="T152" id="Seg_5633" s="T146">Nagur dʼala onʼiʔ ine šonəga, kömə. </ta>
            <ta e="T156" id="Seg_5634" s="T152">Dĭgəttə naga dĭn kuza. </ta>
            <ta e="T160" id="Seg_5635" s="T156">Dĭgəttə sagər ine šonəga. </ta>
            <ta e="T163" id="Seg_5636" s="T160">Kuza tože naga. </ta>
            <ta e="T169" id="Seg_5637" s="T163">Amnolaʔbəʔi, amnolaʔbəʔi, dĭgəttə sĭre ine šonəga. </ta>
            <ta e="T172" id="Seg_5638" s="T169">I kuza amnolaʔbə. </ta>
            <ta e="T181" id="Seg_5639" s="T172">"No, kanžəbaʔ mănziʔ, maʔnəl možet kallal alʼi măna amnolal". </ta>
            <ta e="T184" id="Seg_5640" s="T181">Šobiʔi dĭ kuzanə. </ta>
            <ta e="T187" id="Seg_5641" s="T184">Onʼiʔ kambi maːʔndə. </ta>
            <ta e="T200" id="Seg_5642" s="T187">Šobi maːʔndə, naga (t-) il, šindidə naga, (onʼiʔ= onʼiʔ amnolaʔ-), onʼiʔ maʔ nuga. </ta>
            <ta e="T202" id="Seg_5643" s="T200">Dĭ šobi. </ta>
            <ta e="T207" id="Seg_5644" s="T202">"(Ĭm- gib-) Giraːmbi bar il?" </ta>
            <ta e="T214" id="Seg_5645" s="T207">"(Da miʔ koʔbdobaʔ bar=) Šide nʼi ibiʔi. </ta>
            <ta e="T217" id="Seg_5646" s="T214">Dĭzeŋ bünə öʔluʔpiʔi. </ta>
            <ta e="T223" id="Seg_5647" s="T217">A miʔ koʔbdobaʔ bar iləm dʼăgarluʔpi". </ta>
            <ta e="T226" id="Seg_5648" s="T223">"A gijen dĭ?" </ta>
            <ta e="T230" id="Seg_5649" s="T226">"Gibərdə kalla dʼürbi, ej tĭmnebeʔ". </ta>
            <ta e="T234" id="Seg_5650" s="T230">Dĭgəttə dĭ ine ibi. </ta>
            <ta e="T242" id="Seg_5651" s="T234">Kambi kuzittə, (ku- ku-) kulaːmbi, kubi, kubiʔi dĭn. </ta>
            <ta e="T247" id="Seg_5652" s="T242">Dĭgəttə davaj dĭziʔ bar dʼabrozittə. </ta>
            <ta e="T253" id="Seg_5653" s="T247">Dĭ xatʼel dĭm (bazə-) bădəsʼtə dagajziʔ. </ta>
            <ta e="T262" id="Seg_5654" s="T253">Dĭ bar măndə:" Iʔ bădaʔ, măn tăn kagam igem". </ta>
            <ta e="T264" id="Seg_5655" s="T262">(Kurizəʔi) dʼăbaktərlaʔbəʔjə. </ta>
            <ta e="T269" id="Seg_5656" s="T264">Kapaluxa i ((PAUSE)) kuropatka. </ta>
            <ta e="T279" id="Seg_5657" s="T269">Kuropatka mălia kapaluxanə: "Tăn ej kuvas, girgit (piʔmeʔi), ej jakšə. </ta>
            <ta e="T283" id="Seg_5658" s="T279">A măn kuvas igem. </ta>
            <ta e="T292" id="Seg_5659" s="T283">Коричневый inegəʔ eʔbdə bar sajnʼiʔluʔpiеm i boskəndə embiem ulunə. </ta>
            <ta e="T296" id="Seg_5660" s="T292">Ugaːndə kuvas igem tüj. </ta>
            <ta e="T297" id="Seg_5661" s="T296">((BRK)). </ta>
            <ta e="T301" id="Seg_5662" s="T297">Măjagən bar aspak mĭnzəlleʔbə. </ta>
            <ta e="T304" id="Seg_5663" s="T301">Ĭmbi dĭ dĭrgit? </ta>
            <ta e="T305" id="Seg_5664" s="T304">Murašeʔi. </ta>
            <ta e="T306" id="Seg_5665" s="T305">((BRK)). </ta>
            <ta e="T308" id="Seg_5666" s="T306">Măjagən … </ta>
            <ta e="T309" id="Seg_5667" s="T308">((BRK)). </ta>
            <ta e="T312" id="Seg_5668" s="T309">Măjagən süjö edölaʔbə. </ta>
            <ta e="T314" id="Seg_5669" s="T312">Ĭmbi dĭrgit? </ta>
            <ta e="T315" id="Seg_5670" s="T314">Nüjö. </ta>
            <ta e="T316" id="Seg_5671" s="T315">((BRK)). </ta>
            <ta e="T326" id="Seg_5672" s="T316">Bostə (dʼünən-) dʼügən amnolaʔbə, a kut baška dʼünən amnolaʔbə serʼožkaʔizi. </ta>
            <ta e="T328" id="Seg_5673" s="T326">Ответ: саранка. </ta>
            <ta e="T329" id="Seg_5674" s="T328">((BRK)). </ta>
            <ta e="T333" id="Seg_5675" s="T329">Šide (neg-) nezeŋ iʔbolaʔbəʔjə. </ta>
            <ta e="T334" id="Seg_5676" s="T333">((BRK)). </ta>
            <ta e="T340" id="Seg_5677" s="T334">Iʔbolaʔbəʔjə (šünə- š-) šünə tondə, nanəʔsəbiʔi. </ta>
            <ta e="T343" id="Seg_5678" s="T340">Dĭ üjüzeŋdə lʼažkaʔi. </ta>
            <ta e="T344" id="Seg_5679" s="T343">((BRK)). </ta>
            <ta e="T349" id="Seg_5680" s="T344">Üjüt naga, i udat naga. </ta>
            <ta e="T352" id="Seg_5681" s="T349">A mĭnge … </ta>
            <ta e="T355" id="Seg_5682" s="T352">(M-) Măjanə kandəga. </ta>
            <ta e="T357" id="Seg_5683" s="T355">Dĭ kuja. </ta>
            <ta e="T358" id="Seg_5684" s="T357">((BRK)). </ta>
            <ta e="T364" id="Seg_5685" s="T358">Iʔgö süjöʔi uluzaŋdə bar onʼiʔ onʼiʔtə. </ta>
            <ta e="T372" id="Seg_5686" s="T365">A (kötezeŋ-) kötenzeŋdə bar kuŋgeŋ onʼiʔ onʼiʔtə. </ta>
            <ta e="T374" id="Seg_5687" s="T372">Dĭ maʔ. </ta>
            <ta e="T375" id="Seg_5688" s="T374">((BRK)). </ta>
            <ta e="T380" id="Seg_5689" s="T375">Turagən iʔbolaʔbə (dʼo-) dʼüʔpi buzo. </ta>
            <ta e="T385" id="Seg_5690" s="T380">A nörbəsʼtə dĭ ((PAUSE)) šĭkət. </ta>
            <ta e="T386" id="Seg_5691" s="T385">((BRK)). </ta>
            <ta e="T392" id="Seg_5692" s="T386">(Šire-) Sĭre (pa-) paʔinə tarara amnolaʔbə. </ta>
            <ta e="T393" id="Seg_5693" s="T392">((BRK)). </ta>
            <ta e="T395" id="Seg_5694" s="T393">Dĭ šĭkə. </ta>
            <ta e="T396" id="Seg_5695" s="T395">((BRK)). </ta>
            <ta e="T403" id="Seg_5696" s="T396">Üjüt (uje-) udat naga, a kudajdə üzlie. </ta>
            <ta e="T405" id="Seg_5697" s="T403">Dĭ зыбка. </ta>
            <ta e="T406" id="Seg_5698" s="T405">((BRK)). </ta>
            <ta e="T409" id="Seg_5699" s="T406">Üjüt, udat naga. </ta>
            <ta e="T418" id="Seg_5700" s="T409">A paʔinə mĭllie, кланяется, кланяется; a maʔndə šoləj— iʔbolaʔbə. </ta>
            <ta e="T420" id="Seg_5701" s="T418">Dĭ baltu. </ta>
            <ta e="T421" id="Seg_5702" s="T420">((BRK)). </ta>
            <ta e="T424" id="Seg_5703" s="T421">Udat, üjüt naga. </ta>
            <ta e="T428" id="Seg_5704" s="T424">A ajim bar karlaʔbə. </ta>
            <ta e="T430" id="Seg_5705" s="T428">Dĭ beržə. </ta>
            <ta e="T431" id="Seg_5706" s="T430">((BRK)). </ta>
            <ta e="T437" id="Seg_5707" s="T431">Üdʼüge (men-) men maːndə toʔndə amnolaʔbəʔjə. </ta>
            <ta e="T445" id="Seg_5708" s="T437">Ej kürümnie i (šindi-) šindimdə maʔtə ej … </ta>
            <ta e="T446" id="Seg_5709" s="T445">((BRK)). </ta>
            <ta e="T450" id="Seg_5710" s="T446">Šindinədə maʔndə ej öʔle. </ta>
            <ta e="T452" id="Seg_5711" s="T450">Dĭ замок. </ta>
            <ta e="T453" id="Seg_5712" s="T452">((BRK)). </ta>
            <ta e="T456" id="Seg_5713" s="T453">Krasnojarskəʔi turaʔi jaʔleʔbəʔjə. </ta>
            <ta e="T460" id="Seg_5714" s="T456">A döber paʔi nʼergölaʔbəʔjə. </ta>
            <ta e="T464" id="Seg_5715" s="T460">Dĭ sazən kandəga dĭʔə. </ta>
            <ta e="T466" id="Seg_5716" s="T464">((…)) ((BRK)). </ta>
            <ta e="T477" id="Seg_5717" s="T466">(Šĭket) naga, simat naga, aŋdə naga, a măllia kumen время kambi. </ta>
            <ta e="T479" id="Seg_5718" s="T477">Dĭ časɨʔi. </ta>
            <ta e="T480" id="Seg_5719" s="T479">((BRK)). </ta>
            <ta e="T484" id="Seg_5720" s="T480">Iʔbolaʔbə dăk, üdʼüge tospaktə. </ta>
            <ta e="T491" id="Seg_5721" s="T484">A nulaʔbə dăk ugandə urgo, urgo inetsi. </ta>
            <ta e="T493" id="Seg_5722" s="T492">Duga. </ta>
            <ta e="T494" id="Seg_5723" s="T493">((BRK)). </ta>
            <ta e="T499" id="Seg_5724" s="T494">Amnolaʔbə koʔbdo bar ospaʔizi (tĭ). </ta>
            <ta e="T502" id="Seg_5725" s="T499">A dĭ indak. </ta>
            <ta e="T503" id="Seg_5726" s="T502">((BRK)). </ta>
            <ta e="T513" id="Seg_5727" s="T503">Oʔbdəbiam, oʔbdəbiam nʼitkaʔi, ej (mombia-) ej mobiam oʔbdəsʼtə, dĭ aʔtʼi. </ta>
            <ta e="T514" id="Seg_5728" s="T513">((BRK)). </ta>
            <ta e="T517" id="Seg_5729" s="T514">Amnolaʔbə büzʼe grʼadkagən. </ta>
            <ta e="T520" id="Seg_5730" s="T517">Bar iʔgö zaplatkaʔi. </ta>
            <ta e="T525" id="Seg_5731" s="T520">Šində (kulil-) kulaʔbə bar dʼorlaʔbə. </ta>
            <ta e="T527" id="Seg_5732" s="T525">Dĭ köbergən. </ta>
            <ta e="T528" id="Seg_5733" s="T527">((BRK)). </ta>
            <ta e="T532" id="Seg_5734" s="T528">Amnolaʔbə, iʔgö oldʼa … </ta>
            <ta e="T533" id="Seg_5735" s="T532">((BRK)). </ta>
            <ta e="T539" id="Seg_5736" s="T533">(Sojp-) Sejʔpü (aktʼa=) ((PAUSE)) (o- )… </ta>
            <ta e="T546" id="Seg_5737" s="T539">Bʼeʔ (sojp-) sejʔpü oldʼa, a kajzittə naga. </ta>
            <ta e="T548" id="Seg_5738" s="T546">Dĭ kapusta. </ta>
            <ta e="T549" id="Seg_5739" s="T548">((BRK)). </ta>
            <ta e="T554" id="Seg_5740" s="T549">Amnolaʔbə koʔbdo dʼügən, bostə kömə. </ta>
            <ta e="T557" id="Seg_5741" s="T554">A eʔbdət nʼiʔnen. </ta>
            <ta e="T559" id="Seg_5742" s="T557">Dĭ морковка. </ta>
            <ta e="T560" id="Seg_5743" s="T559">((BRK)). </ta>
            <ta e="T567" id="Seg_5744" s="T560">Măn kubiam plʼotka, a kanzittə ej mobiam. </ta>
            <ta e="T570" id="Seg_5745" s="T567">A dĭ ĭmbi? </ta>
            <ta e="T571" id="Seg_5746" s="T570">Penzə. </ta>
            <ta e="T572" id="Seg_5747" s="T571">((BRK)). </ta>
            <ta e="T581" id="Seg_5748" s="T572">Šonəga koʔbdo, i dĭn eʔbdəʔi bar (ko-) kurgonaʔi bar. </ta>
            <ta e="T582" id="Seg_5749" s="T581">((BRK)). </ta>
            <ta e="T585" id="Seg_5750" s="T582">Dĭ soroka ige. </ta>
            <ta e="T586" id="Seg_5751" s="T585">((BRK)). </ta>
            <ta e="T598" id="Seg_5752" s="T586">Šide nugaʔi, šide iʔbolaʔbəʔjə, (onʼiʔ mĭlleʔbə=) sumna mĭlleʔbə, a sejʔpü (kaj-) kajlaʔbə. </ta>
            <ta e="T601" id="Seg_5753" s="T598">Dĭ aji ige. </ta>
            <ta e="T602" id="Seg_5754" s="T601">((BRK)). </ta>
            <ta e="T611" id="Seg_5755" s="T602">(Ula-) ular iʔgö mĭlleʔbəʔi, a pastux amnut. </ta>
            <ta e="T615" id="Seg_5756" s="T611">Dĭ mʼesʼas i zvʼozdaʔi. </ta>
            <ta e="T616" id="Seg_5757" s="T615">((BRK)). </ta>
            <ta e="T624" id="Seg_5758" s="T616">Šide ši i šide puʔməʔi, a šüjögən marka. </ta>
            <ta e="T625" id="Seg_5759" s="T624">Kaptəʔi. </ta>
            <ta e="T626" id="Seg_5760" s="T625">((BRK)). </ta>
            <ta e="T635" id="Seg_5761" s="T626">Măn üge amnaʔbəm, amnaʔbəm, a tüʔsittə ej moliam — Bura. </ta>
            <ta e="T636" id="Seg_5762" s="T635">((BRK)). </ta>
            <ta e="T639" id="Seg_5763" s="T636">Pʼetux amnolaʔbə zaborgən. </ta>
            <ta e="T644" id="Seg_5764" s="T639">Xvost dʼügən, a kirgarlaʔbə kudajdə. </ta>
            <ta e="T646" id="Seg_5765" s="T644">Dĭ koŋgoro. </ta>
            <ta e="T647" id="Seg_5766" s="T646">((BRK)). </ta>
            <ta e="T656" id="Seg_5767" s="T647">Nuga nʼi, šüjögən (neŋe-) nenileʔbə, a bostə bar kĭnzleʔbə. </ta>
            <ta e="T658" id="Seg_5768" s="T656">Dĭ kujdərgan. </ta>
            <ta e="T659" id="Seg_5769" s="T658">((BRK)). </ta>
            <ta e="T661" id="Seg_5770" s="T659">Šidegöʔ kandəgaʔi. </ta>
            <ta e="T665" id="Seg_5771" s="T661">Onʼiʔ kandəga, onʼiʔ bĭtlie. </ta>
            <ta e="T667" id="Seg_5772" s="T665">Dĭ üjüʔi. </ta>
            <ta e="T668" id="Seg_5773" s="T667">((BRK)). </ta>
            <ta e="T676" id="Seg_5774" s="T668">Toʔnaraʔ măna, münöreʔ măna, sʼaʔ măna, ige măna. </ta>
            <ta e="T678" id="Seg_5775" s="T676">Dĭ sanə. </ta>
            <ta e="T679" id="Seg_5776" s="T678">((BRK)). </ta>
            <ta e="T687" id="Seg_5777" s="T679">Edleʔbə bar, a il bar kabarlaʔbəʔjə, băzəjdlaʔbəʔjə, kĭškəleʔbəʔjə. </ta>
            <ta e="T690" id="Seg_5778" s="T687">Dĭ rušnʼik ige. </ta>
            <ta e="T691" id="Seg_5779" s="T690">((BRK)). </ta>
            <ta e="T696" id="Seg_5780" s="T691">Вокруг paʔi noʔ özerleʔbə золотой. </ta>
            <ta e="T698" id="Seg_5781" s="T696">Dĭ müjö. </ta>
            <ta e="T699" id="Seg_5782" s="T698">((BRK)). </ta>
            <ta e="T706" id="Seg_5783" s="T699">Kömə poʔto iʔbolaʔpi, dĭn noʔ ej özerleʔbə. </ta>
            <ta e="T708" id="Seg_5784" s="T706">Šü dĭ. </ta>
            <ta e="T709" id="Seg_5785" s="T708">((BRK)). </ta>
            <ta e="T721" id="Seg_5786" s="T709">Kambi keʔbdejle, i măn kambiam keʔbde nĭŋgəsʼtə, i išo onʼiʔ koʔbdo kambi. </ta>
            <ta e="T728" id="Seg_5787" s="T721">(I- ine- inezi= ine-) (Kambila) Kambibaʔ (šumiganə). </ta>
            <ta e="T730" id="Seg_5788" s="T728">Dĭbər šobibaʔ. </ta>
            <ta e="T736" id="Seg_5789" s="T730">(Šalaʔjə) keʔbde, măndorbibaʔ i idʼiʔeʔe oʔbdəbibaʔ. </ta>
            <ta e="T747" id="Seg_5790" s="T736">Dĭgəttə šobibaʔ, gijen ine mĭnleʔbə, iam măndə: "Ugandə sagər туча šonəga. </ta>
            <ta e="T755" id="Seg_5791" s="T747">Nada azittə maʔ, a to kunolzittə ej jakše moləj". </ta>
            <ta e="T758" id="Seg_5792" s="T755">Gəttə maʔ abibaʔ. </ta>
            <ta e="T762" id="Seg_5793" s="T758">Mĭnzərbibeʔ, segi bü bĭʔpibeʔ. </ta>
            <ta e="T768" id="Seg_5794" s="T762">A dĭn (был=) ibi pălăvik tĭbər. </ta>
            <ta e="T771" id="Seg_5795" s="T768">Pălăviktə (edəbibiʔibeʔ) paʔinə. </ta>
            <ta e="T774" id="Seg_5796" s="T771">Dĭgəttə iʔbəbeʔ kunolzittə. </ta>
            <ta e="T782" id="Seg_5797" s="T774">A nüdʼin bar ugandə tăŋ surno šonəbi, šonəbi. </ta>
            <ta e="T786" id="Seg_5798" s="T782">Ertən uʔbdəbibaʔ bar, nünörbibaʔ. </ta>
            <ta e="T790" id="Seg_5799" s="T786">Ineʔi dʼaʔpibaʔ, kambibaʔ maʔnʼi. </ta>
            <ta e="T793" id="Seg_5800" s="T790">(Šobi-) Šobibaʔ bünə. </ta>
            <ta e="T796" id="Seg_5801" s="T793">Kădedə nʼelʼzʼa kanzittə. </ta>
            <ta e="T805" id="Seg_5802" s="T796">Dĭgəttə (nʼ-) вниз bünə kanbibaʔ, kanbibaʔ, dĭgəttə kubibaʔ mostə. </ta>
            <ta e="T811" id="Seg_5803" s="T805">Mostə (перей-) šobibaʔ i maʔnʼi šobibaʔ. </ta>
            <ta e="T812" id="Seg_5804" s="T811">Все. </ta>
            <ta e="T813" id="Seg_5805" s="T812">((BRK)). </ta>
            <ta e="T817" id="Seg_5806" s="T813">Măn teinen ertə uʔbdəbiam. </ta>
            <ta e="T819" id="Seg_5807" s="T817">Măgăzində kambiam. </ta>
            <ta e="T821" id="Seg_5808" s="T819">Очередь nubiam. </ta>
            <ta e="T832" id="Seg_5809" s="T821">Dĭgəttə (šide=) šide čas nubiam, dĭgəttə ibiem kola i šobiam maʔnə. </ta>
            <ta e="T835" id="Seg_5810" s="T832">I šiʔ šobilaʔ. </ta>
            <ta e="T836" id="Seg_5811" s="T835">((BRK)). </ta>
            <ta e="T845" id="Seg_5812" s="T836">Gəttə măn ibiem oʔb kilo, aktʼa mĭbiem onʼiʔ šălkovej. </ta>
            <ta e="T848" id="Seg_5813" s="T845">Bʼeʔ teʔtə mĭbiem. </ta>
            <ta e="T852" id="Seg_5814" s="T848">Onʼiʔ šălkovej bʼeʔ teʔtə. </ta>
            <ta e="T853" id="Seg_5815" s="T852">((BRK)). </ta>
            <ta e="T858" id="Seg_5816" s="T853">Dĭn bar nezeŋ kudonzlaʔbəʔjə, kirgarlaʔbəʔjə. </ta>
            <ta e="T861" id="Seg_5817" s="T858">Mănliaʔi:" Amga kola. </ta>
            <ta e="T866" id="Seg_5818" s="T861">(Onʼiʔ k-) Onʼiʔ kilo mĭleʔbə". </ta>
            <ta e="T874" id="Seg_5819" s="T866">Dĭ mĭbi onʼiʔ bar nezeŋdə, onʼiʔ kilo mĭbi. </ta>
            <ta e="T877" id="Seg_5820" s="T874">Mănliaʔi:" Esseŋbə kirgarlaʔbəʔjə. </ta>
            <ta e="T882" id="Seg_5821" s="T877">(Kola=) Kola amzittə kereʔ dĭzeŋdə". </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T1" id="Seg_5822" s="T0">dĭgəttə</ta>
            <ta e="T2" id="Seg_5823" s="T1">dĭ</ta>
            <ta e="T3" id="Seg_5824" s="T2">nʼi-zeŋ</ta>
            <ta e="T4" id="Seg_5825" s="T3">aktʼi-nə</ta>
            <ta e="T5" id="Seg_5826" s="T4">kam-bi-ʔi</ta>
            <ta e="T6" id="Seg_5827" s="T5">šo-bi-ʔi</ta>
            <ta e="T7" id="Seg_5828" s="T6">šo-bi-ʔi</ta>
            <ta e="T8" id="Seg_5829" s="T7">dĭgəttə</ta>
            <ta e="T9" id="Seg_5830" s="T8">aʔtʼi</ta>
            <ta e="T10" id="Seg_5831" s="T9">aʔtʼi-gən</ta>
            <ta e="T11" id="Seg_5832" s="T10">toʔ-ndə</ta>
            <ta e="T12" id="Seg_5833" s="T11">amno-bi-ʔi</ta>
            <ta e="T13" id="Seg_5834" s="T12">ku-lia-ʔi</ta>
            <ta e="T14" id="Seg_5835" s="T13">šonə-ga</ta>
            <ta e="T15" id="Seg_5836" s="T14">kăldun</ta>
            <ta e="T16" id="Seg_5837" s="T15">onʼiʔ</ta>
            <ta e="T17" id="Seg_5838" s="T16">ine</ta>
            <ta e="T18" id="Seg_5839" s="T17">kömə</ta>
            <ta e="T19" id="Seg_5840" s="T18">dĭgəttə</ta>
            <ta e="T20" id="Seg_5841" s="T19">dĭ</ta>
            <ta e="T21" id="Seg_5842" s="T20">bar</ta>
            <ta e="T23" id="Seg_5843" s="T21">tăŋ</ta>
            <ta e="T24" id="Seg_5844" s="T23">dĭgəttə</ta>
            <ta e="T25" id="Seg_5845" s="T24">onʼiʔ</ta>
            <ta e="T27" id="Seg_5846" s="T26">tăŋ</ta>
            <ta e="T28" id="Seg_5847" s="T27">ine-t</ta>
            <ta e="T29" id="Seg_5848" s="T28">bar</ta>
            <ta e="T31" id="Seg_5849" s="T30">kălʼenka-nə</ta>
            <ta e="T33" id="Seg_5850" s="T32">dĭ</ta>
            <ta e="T34" id="Seg_5851" s="T33">ine</ta>
            <ta e="T35" id="Seg_5852" s="T34">saʔmə-luʔ-bi</ta>
            <ta e="T36" id="Seg_5853" s="T35">kălʼenka-ʔi-nə</ta>
            <ta e="T37" id="Seg_5854" s="T36">dĭgəttə</ta>
            <ta e="T38" id="Seg_5855" s="T37">dĭ</ta>
            <ta e="T39" id="Seg_5856" s="T38">găldun</ta>
            <ta e="T40" id="Seg_5857" s="T39">măn-də-bi</ta>
            <ta e="T41" id="Seg_5858" s="T40">dĭ</ta>
            <ta e="T42" id="Seg_5859" s="T41">dĭn</ta>
            <ta e="T44" id="Seg_5860" s="T43">enəidənə</ta>
            <ta e="T45" id="Seg_5861" s="T44">amno-laʔbə</ta>
            <ta e="T46" id="Seg_5862" s="T45">ugaːndə</ta>
            <ta e="T47" id="Seg_5863" s="T46">pim-nie-m</ta>
            <ta e="T48" id="Seg_5864" s="T47">dĭgəttə</ta>
            <ta e="T49" id="Seg_5865" s="T48">kam-bi</ta>
            <ta e="T50" id="Seg_5866" s="T49">amno-laʔbə-ʔjə</ta>
            <ta e="T51" id="Seg_5867" s="T50">amno-laʔbə-ʔjə</ta>
            <ta e="T52" id="Seg_5868" s="T51">bazoʔ</ta>
            <ta e="T53" id="Seg_5869" s="T52">šonə-ga</ta>
            <ta e="T54" id="Seg_5870" s="T53">sagər</ta>
            <ta e="T55" id="Seg_5871" s="T54">ine-t-siʔ</ta>
            <ta e="T57" id="Seg_5872" s="T56">baška</ta>
            <ta e="T58" id="Seg_5873" s="T57">šide</ta>
            <ta e="T59" id="Seg_5874" s="T58">šide-git</ta>
            <ta e="T60" id="Seg_5875" s="T59">găldun</ta>
            <ta e="T61" id="Seg_5876" s="T60">dĭgəttə</ta>
            <ta e="T62" id="Seg_5877" s="T61">dĭ-zeŋ</ta>
            <ta e="T63" id="Seg_5878" s="T62">bazoʔ</ta>
            <ta e="T65" id="Seg_5879" s="T64">gəttə</ta>
            <ta e="T66" id="Seg_5880" s="T65">dĭ</ta>
            <ta e="T67" id="Seg_5881" s="T66">saʔmə-luʔ-bi</ta>
            <ta e="T68" id="Seg_5882" s="T67">kălʼenka-ʔi-nə</ta>
            <ta e="T69" id="Seg_5883" s="T68">tĭ</ta>
            <ta e="T70" id="Seg_5884" s="T69">tože</ta>
            <ta e="T71" id="Seg_5885" s="T70">măn-də</ta>
            <ta e="T72" id="Seg_5886" s="T71">enəidənə</ta>
            <ta e="T73" id="Seg_5887" s="T72">ugandə</ta>
            <ta e="T74" id="Seg_5888" s="T73">kuštü</ta>
            <ta e="T75" id="Seg_5889" s="T74">ugandə</ta>
            <ta e="T76" id="Seg_5890" s="T75">măn</ta>
            <ta e="T77" id="Seg_5891" s="T76">pim-nie-m</ta>
            <ta e="T78" id="Seg_5892" s="T77">dĭgəttə</ta>
            <ta e="T80" id="Seg_5893" s="T79">ine</ta>
            <ta e="T81" id="Seg_5894" s="T80">ine-t</ta>
            <ta e="T82" id="Seg_5895" s="T81">uʔbdə-bi</ta>
            <ta e="T83" id="Seg_5896" s="T82">kam-bi</ta>
            <ta e="T84" id="Seg_5897" s="T83">gəttə</ta>
            <ta e="T85" id="Seg_5898" s="T84">amno-laʔbə-ʔjə</ta>
            <ta e="T86" id="Seg_5899" s="T85">amno-laʔbə-ʔjə</ta>
            <ta e="T88" id="Seg_5900" s="T87">nagur-gət</ta>
            <ta e="T89" id="Seg_5901" s="T88">ine</ta>
            <ta e="T90" id="Seg_5902" s="T89">šonə-ga</ta>
            <ta e="T91" id="Seg_5903" s="T90">dö</ta>
            <ta e="T92" id="Seg_5904" s="T91">ine</ta>
            <ta e="T93" id="Seg_5905" s="T92">sĭri</ta>
            <ta e="T94" id="Seg_5906" s="T93">dĭ-zeŋ</ta>
            <ta e="T95" id="Seg_5907" s="T94">bazoʔ</ta>
            <ta e="T97" id="Seg_5908" s="T96">dĭgəttə</ta>
            <ta e="T98" id="Seg_5909" s="T97">dĭ</ta>
            <ta e="T99" id="Seg_5910" s="T98">ine</ta>
            <ta e="T100" id="Seg_5911" s="T99">nuldə-bi</ta>
            <ta e="T101" id="Seg_5912" s="T100">nu-laʔbə</ta>
            <ta e="T102" id="Seg_5913" s="T101">dĭ</ta>
            <ta e="T103" id="Seg_5914" s="T102">kăldun</ta>
            <ta e="T105" id="Seg_5915" s="T104">kam-bi</ta>
            <ta e="T106" id="Seg_5916" s="T105">dĭ-zeŋ-də</ta>
            <ta e="T107" id="Seg_5917" s="T106">girgit</ta>
            <ta e="T108" id="Seg_5918" s="T107">es-seŋ</ta>
            <ta e="T109" id="Seg_5919" s="T108">amno-laʔbə-ʔjə</ta>
            <ta e="T110" id="Seg_5920" s="T109">da</ta>
            <ta e="T111" id="Seg_5921" s="T110">miʔ</ta>
            <ta e="T112" id="Seg_5922" s="T111">ot</ta>
            <ta e="T113" id="Seg_5923" s="T112">miʔnʼibeʔ</ta>
            <ta e="T114" id="Seg_5924" s="T113">bar</ta>
            <ta e="T115" id="Seg_5925" s="T114">bü-nə</ta>
            <ta e="T116" id="Seg_5926" s="T115">barəʔ-luʔ-pi-ʔi</ta>
            <ta e="T117" id="Seg_5927" s="T116">măn</ta>
            <ta e="T118" id="Seg_5928" s="T117">miʔ</ta>
            <ta e="T119" id="Seg_5929" s="T118">döbər</ta>
            <ta e="T120" id="Seg_5930" s="T119">šo-bi-baʔ</ta>
            <ta e="T121" id="Seg_5931" s="T120">dĭgəttə</ta>
            <ta e="T122" id="Seg_5932" s="T121">no</ta>
            <ta e="T123" id="Seg_5933" s="T122">amno-gaʔ</ta>
            <ta e="T124" id="Seg_5934" s="T123">dön</ta>
            <ta e="T125" id="Seg_5935" s="T124">dĭ</ta>
            <ta e="T126" id="Seg_5936" s="T125">dĭʔ-nə</ta>
            <ta e="T127" id="Seg_5937" s="T126">a-bi-ʔi</ta>
            <ta e="T129" id="Seg_5938" s="T128">a-bi</ta>
            <ta e="T130" id="Seg_5939" s="T129">maʔ</ta>
            <ta e="T131" id="Seg_5940" s="T130">ipek</ta>
            <ta e="T132" id="Seg_5941" s="T131">mĭ-bi</ta>
            <ta e="T133" id="Seg_5942" s="T132">uja</ta>
            <ta e="T134" id="Seg_5943" s="T133">mĭ-bi</ta>
            <ta e="T135" id="Seg_5944" s="T134">amno-gaʔ</ta>
            <ta e="T136" id="Seg_5945" s="T135">dön</ta>
            <ta e="T137" id="Seg_5946" s="T136">măn</ta>
            <ta e="T138" id="Seg_5947" s="T137">par-la-m</ta>
            <ta e="T139" id="Seg_5948" s="T138">dʼili</ta>
            <ta e="T140" id="Seg_5949" s="T139">mo-la-m</ta>
            <ta e="T141" id="Seg_5950" s="T140">šiʔnʼileʔ</ta>
            <ta e="T142" id="Seg_5951" s="T141">i-li-m</ta>
            <ta e="T143" id="Seg_5952" s="T142">dĭgəttə</ta>
            <ta e="T144" id="Seg_5953" s="T143">dĭ-zeŋ</ta>
            <ta e="T145" id="Seg_5954" s="T144">amno-laʔ-pi-ʔi</ta>
            <ta e="T146" id="Seg_5955" s="T145">amno-laʔ-pi</ta>
            <ta e="T147" id="Seg_5956" s="T146">nagur</ta>
            <ta e="T148" id="Seg_5957" s="T147">dʼala</ta>
            <ta e="T149" id="Seg_5958" s="T148">onʼiʔ</ta>
            <ta e="T150" id="Seg_5959" s="T149">ine</ta>
            <ta e="T151" id="Seg_5960" s="T150">šonə-ga</ta>
            <ta e="T152" id="Seg_5961" s="T151">kömə</ta>
            <ta e="T153" id="Seg_5962" s="T152">dĭgəttə</ta>
            <ta e="T154" id="Seg_5963" s="T153">naga</ta>
            <ta e="T155" id="Seg_5964" s="T154">dĭn</ta>
            <ta e="T156" id="Seg_5965" s="T155">kuza</ta>
            <ta e="T157" id="Seg_5966" s="T156">dĭgəttə</ta>
            <ta e="T158" id="Seg_5967" s="T157">sagər</ta>
            <ta e="T159" id="Seg_5968" s="T158">ine</ta>
            <ta e="T160" id="Seg_5969" s="T159">šonə-ga</ta>
            <ta e="T161" id="Seg_5970" s="T160">kuza</ta>
            <ta e="T162" id="Seg_5971" s="T161">tože</ta>
            <ta e="T163" id="Seg_5972" s="T162">naga</ta>
            <ta e="T164" id="Seg_5973" s="T163">amno-laʔbə-ʔi</ta>
            <ta e="T165" id="Seg_5974" s="T164">amno-laʔbə-ʔi</ta>
            <ta e="T166" id="Seg_5975" s="T165">dĭgəttə</ta>
            <ta e="T167" id="Seg_5976" s="T166">sĭre</ta>
            <ta e="T168" id="Seg_5977" s="T167">ine</ta>
            <ta e="T169" id="Seg_5978" s="T168">šonə-ga</ta>
            <ta e="T170" id="Seg_5979" s="T169">i</ta>
            <ta e="T171" id="Seg_5980" s="T170">kuza</ta>
            <ta e="T172" id="Seg_5981" s="T171">amno-laʔbə</ta>
            <ta e="T173" id="Seg_5982" s="T172">no</ta>
            <ta e="T174" id="Seg_5983" s="T173">kan-žə-baʔ</ta>
            <ta e="T175" id="Seg_5984" s="T174">măn-ziʔ</ta>
            <ta e="T176" id="Seg_5985" s="T175">maʔ-nə-l</ta>
            <ta e="T177" id="Seg_5986" s="T176">možet</ta>
            <ta e="T178" id="Seg_5987" s="T177">kal-la-l</ta>
            <ta e="T179" id="Seg_5988" s="T178">alʼi</ta>
            <ta e="T180" id="Seg_5989" s="T179">măna</ta>
            <ta e="T181" id="Seg_5990" s="T180">amno-la-l</ta>
            <ta e="T182" id="Seg_5991" s="T181">šo-bi-ʔi</ta>
            <ta e="T183" id="Seg_5992" s="T182">dĭ</ta>
            <ta e="T184" id="Seg_5993" s="T183">kuza-nə</ta>
            <ta e="T185" id="Seg_5994" s="T184">onʼiʔ</ta>
            <ta e="T186" id="Seg_5995" s="T185">kam-bi</ta>
            <ta e="T187" id="Seg_5996" s="T186">maːʔ-ndə</ta>
            <ta e="T188" id="Seg_5997" s="T187">šo-bi</ta>
            <ta e="T189" id="Seg_5998" s="T188">maːʔ-ndə</ta>
            <ta e="T190" id="Seg_5999" s="T189">naga</ta>
            <ta e="T192" id="Seg_6000" s="T191">il</ta>
            <ta e="T193" id="Seg_6001" s="T192">šindi=də</ta>
            <ta e="T194" id="Seg_6002" s="T193">naga</ta>
            <ta e="T195" id="Seg_6003" s="T194">onʼiʔ</ta>
            <ta e="T196" id="Seg_6004" s="T195">onʼiʔ</ta>
            <ta e="T198" id="Seg_6005" s="T197">onʼiʔ</ta>
            <ta e="T199" id="Seg_6006" s="T198">maʔ</ta>
            <ta e="T200" id="Seg_6007" s="T199">nu-ga</ta>
            <ta e="T201" id="Seg_6008" s="T200">dĭ</ta>
            <ta e="T202" id="Seg_6009" s="T201">šo-bi</ta>
            <ta e="T205" id="Seg_6010" s="T204">giraːm-bi</ta>
            <ta e="T206" id="Seg_6011" s="T205">bar</ta>
            <ta e="T207" id="Seg_6012" s="T206">il</ta>
            <ta e="T208" id="Seg_6013" s="T207">da</ta>
            <ta e="T209" id="Seg_6014" s="T208">miʔ</ta>
            <ta e="T210" id="Seg_6015" s="T209">koʔbdo-bAʔ</ta>
            <ta e="T211" id="Seg_6016" s="T210">bar</ta>
            <ta e="T212" id="Seg_6017" s="T211">šide</ta>
            <ta e="T213" id="Seg_6018" s="T212">nʼi</ta>
            <ta e="T214" id="Seg_6019" s="T213">i-bi-ʔi</ta>
            <ta e="T215" id="Seg_6020" s="T214">dĭ-zeŋ</ta>
            <ta e="T216" id="Seg_6021" s="T215">bü-nə</ta>
            <ta e="T217" id="Seg_6022" s="T216">öʔ-luʔ-pi-ʔi</ta>
            <ta e="T218" id="Seg_6023" s="T217">a</ta>
            <ta e="T219" id="Seg_6024" s="T218">miʔ</ta>
            <ta e="T220" id="Seg_6025" s="T219">koʔbdo-bAʔ</ta>
            <ta e="T221" id="Seg_6026" s="T220">bar</ta>
            <ta e="T222" id="Seg_6027" s="T221">il-əm</ta>
            <ta e="T223" id="Seg_6028" s="T222">dʼăgar-luʔ-pi</ta>
            <ta e="T224" id="Seg_6029" s="T223">a</ta>
            <ta e="T225" id="Seg_6030" s="T224">gijen</ta>
            <ta e="T226" id="Seg_6031" s="T225">dĭ</ta>
            <ta e="T227" id="Seg_6032" s="T226">gibər=də</ta>
            <ta e="T384" id="Seg_6033" s="T227">kal-la</ta>
            <ta e="T228" id="Seg_6034" s="T384">dʼür-bi</ta>
            <ta e="T229" id="Seg_6035" s="T228">ej</ta>
            <ta e="T230" id="Seg_6036" s="T229">tĭmne-beʔ</ta>
            <ta e="T231" id="Seg_6037" s="T230">dĭgəttə</ta>
            <ta e="T232" id="Seg_6038" s="T231">dĭ</ta>
            <ta e="T233" id="Seg_6039" s="T232">ine</ta>
            <ta e="T234" id="Seg_6040" s="T233">i-bi</ta>
            <ta e="T235" id="Seg_6041" s="T234">kam-bi</ta>
            <ta e="T236" id="Seg_6042" s="T235">ku-zittə</ta>
            <ta e="T239" id="Seg_6043" s="T238">ku-laːm-bi</ta>
            <ta e="T240" id="Seg_6044" s="T239">ku-bi</ta>
            <ta e="T241" id="Seg_6045" s="T240">ku-bi-ʔi</ta>
            <ta e="T242" id="Seg_6046" s="T241">dĭn</ta>
            <ta e="T243" id="Seg_6047" s="T242">dĭgəttə</ta>
            <ta e="T244" id="Seg_6048" s="T243">davaj</ta>
            <ta e="T245" id="Seg_6049" s="T244">dĭ-ziʔ</ta>
            <ta e="T246" id="Seg_6050" s="T245">bar</ta>
            <ta e="T247" id="Seg_6051" s="T246">dʼabro-zittə</ta>
            <ta e="T248" id="Seg_6052" s="T247">dĭ</ta>
            <ta e="T249" id="Seg_6053" s="T248">xatʼel</ta>
            <ta e="T250" id="Seg_6054" s="T249">dĭ-m</ta>
            <ta e="T252" id="Seg_6055" s="T251">bădə-sʼtə</ta>
            <ta e="T253" id="Seg_6056" s="T252">dagaj-ziʔ</ta>
            <ta e="T254" id="Seg_6057" s="T253">dĭ</ta>
            <ta e="T255" id="Seg_6058" s="T254">bar</ta>
            <ta e="T256" id="Seg_6059" s="T255">măn-də</ta>
            <ta e="T257" id="Seg_6060" s="T256">i-ʔ</ta>
            <ta e="T258" id="Seg_6061" s="T257">băd-a-ʔ</ta>
            <ta e="T259" id="Seg_6062" s="T258">măn</ta>
            <ta e="T260" id="Seg_6063" s="T259">tăn</ta>
            <ta e="T261" id="Seg_6064" s="T260">kaga-m</ta>
            <ta e="T262" id="Seg_6065" s="T261">i-ge-m</ta>
            <ta e="T263" id="Seg_6066" s="T262">kurizə-ʔi</ta>
            <ta e="T264" id="Seg_6067" s="T263">dʼăbaktər-laʔbə-ʔjə</ta>
            <ta e="T265" id="Seg_6068" s="T264">kapaluxa</ta>
            <ta e="T266" id="Seg_6069" s="T265">i</ta>
            <ta e="T269" id="Seg_6070" s="T267">kuropatka</ta>
            <ta e="T270" id="Seg_6071" s="T269">kuropatka</ta>
            <ta e="T271" id="Seg_6072" s="T270">mă-lia</ta>
            <ta e="T272" id="Seg_6073" s="T271">kapaluxa-nə</ta>
            <ta e="T273" id="Seg_6074" s="T272">tăn</ta>
            <ta e="T274" id="Seg_6075" s="T273">ej</ta>
            <ta e="T275" id="Seg_6076" s="T274">kuvas</ta>
            <ta e="T276" id="Seg_6077" s="T275">girgit</ta>
            <ta e="T277" id="Seg_6078" s="T276">piʔme-ʔi</ta>
            <ta e="T278" id="Seg_6079" s="T277">ej</ta>
            <ta e="T279" id="Seg_6080" s="T278">jakšə</ta>
            <ta e="T280" id="Seg_6081" s="T279">a</ta>
            <ta e="T281" id="Seg_6082" s="T280">măn</ta>
            <ta e="T282" id="Seg_6083" s="T281">kuvas</ta>
            <ta e="T283" id="Seg_6084" s="T282">i-ge-m</ta>
            <ta e="T284" id="Seg_6085" s="T883">ine-gəʔ</ta>
            <ta e="T285" id="Seg_6086" s="T284">eʔbdə</ta>
            <ta e="T286" id="Seg_6087" s="T285">bar</ta>
            <ta e="T287" id="Seg_6088" s="T286">saj-nʼiʔ-luʔ-piе-m</ta>
            <ta e="T288" id="Seg_6089" s="T287">i</ta>
            <ta e="T289" id="Seg_6090" s="T288">bos-kəndə</ta>
            <ta e="T290" id="Seg_6091" s="T289">em-bie-m</ta>
            <ta e="T292" id="Seg_6092" s="T290">ulu-nə</ta>
            <ta e="T293" id="Seg_6093" s="T292">ugaːndə</ta>
            <ta e="T294" id="Seg_6094" s="T293">kuvas</ta>
            <ta e="T295" id="Seg_6095" s="T294">i-ge-m</ta>
            <ta e="T296" id="Seg_6096" s="T295">tüj</ta>
            <ta e="T298" id="Seg_6097" s="T297">măja-gən</ta>
            <ta e="T299" id="Seg_6098" s="T298">bar</ta>
            <ta e="T300" id="Seg_6099" s="T299">aspak</ta>
            <ta e="T301" id="Seg_6100" s="T300">mĭnzəl-leʔbə</ta>
            <ta e="T302" id="Seg_6101" s="T301">ĭmbi</ta>
            <ta e="T303" id="Seg_6102" s="T302">dĭ</ta>
            <ta e="T304" id="Seg_6103" s="T303">dĭrgit</ta>
            <ta e="T305" id="Seg_6104" s="T304">muraše-ʔi</ta>
            <ta e="T308" id="Seg_6105" s="T306">măja-gən</ta>
            <ta e="T310" id="Seg_6106" s="T309">măja-gən</ta>
            <ta e="T311" id="Seg_6107" s="T310">süjö</ta>
            <ta e="T312" id="Seg_6108" s="T311">edö-laʔbə</ta>
            <ta e="T313" id="Seg_6109" s="T312">ĭmbi</ta>
            <ta e="T314" id="Seg_6110" s="T313">dĭrgit</ta>
            <ta e="T315" id="Seg_6111" s="T314">nüjö</ta>
            <ta e="T317" id="Seg_6112" s="T316">bos-tə</ta>
            <ta e="T319" id="Seg_6113" s="T318">dʼü-gən</ta>
            <ta e="T320" id="Seg_6114" s="T319">amno-laʔbə</ta>
            <ta e="T321" id="Seg_6115" s="T320">a</ta>
            <ta e="T322" id="Seg_6116" s="T321">ku-t</ta>
            <ta e="T323" id="Seg_6117" s="T322">baška</ta>
            <ta e="T324" id="Seg_6118" s="T323">dʼü-nən</ta>
            <ta e="T325" id="Seg_6119" s="T324">amno-laʔbə</ta>
            <ta e="T326" id="Seg_6120" s="T325">serʼožka-ʔi-zi</ta>
            <ta e="T330" id="Seg_6121" s="T329">šide</ta>
            <ta e="T332" id="Seg_6122" s="T331">ne-zeŋ</ta>
            <ta e="T333" id="Seg_6123" s="T332">iʔbo-laʔbə-ʔjə</ta>
            <ta e="T335" id="Seg_6124" s="T334">iʔbo-laʔbə-ʔjə</ta>
            <ta e="T338" id="Seg_6125" s="T337">šü-nə</ta>
            <ta e="T339" id="Seg_6126" s="T338">to-ndə</ta>
            <ta e="T340" id="Seg_6127" s="T339">nanəʔsəbi-ʔi</ta>
            <ta e="T341" id="Seg_6128" s="T340">dĭ</ta>
            <ta e="T342" id="Seg_6129" s="T341">üjü-zeŋ-də</ta>
            <ta e="T343" id="Seg_6130" s="T342">lʼažka-ʔi</ta>
            <ta e="T345" id="Seg_6131" s="T344">üjü-t</ta>
            <ta e="T346" id="Seg_6132" s="T345">naga</ta>
            <ta e="T347" id="Seg_6133" s="T346">i</ta>
            <ta e="T348" id="Seg_6134" s="T347">uda-t</ta>
            <ta e="T349" id="Seg_6135" s="T348">naga</ta>
            <ta e="T350" id="Seg_6136" s="T349">a</ta>
            <ta e="T352" id="Seg_6137" s="T350">mĭn-ge</ta>
            <ta e="T354" id="Seg_6138" s="T353">măja-nə</ta>
            <ta e="T355" id="Seg_6139" s="T354">kandə-ga</ta>
            <ta e="T356" id="Seg_6140" s="T355">dĭ</ta>
            <ta e="T357" id="Seg_6141" s="T356">kuja</ta>
            <ta e="T359" id="Seg_6142" s="T358">iʔgö</ta>
            <ta e="T360" id="Seg_6143" s="T359">süjö-ʔi</ta>
            <ta e="T361" id="Seg_6144" s="T360">ulu-zaŋ-də</ta>
            <ta e="T362" id="Seg_6145" s="T361">bar</ta>
            <ta e="T363" id="Seg_6146" s="T362">onʼiʔ</ta>
            <ta e="T364" id="Seg_6147" s="T363">onʼiʔ-tə</ta>
            <ta e="T366" id="Seg_6148" s="T365">a</ta>
            <ta e="T368" id="Seg_6149" s="T367">köten-zeŋ-də</ta>
            <ta e="T369" id="Seg_6150" s="T368">bar</ta>
            <ta e="T370" id="Seg_6151" s="T369">kuŋge-ŋ</ta>
            <ta e="T371" id="Seg_6152" s="T370">onʼiʔ</ta>
            <ta e="T372" id="Seg_6153" s="T371">onʼiʔ-tə</ta>
            <ta e="T373" id="Seg_6154" s="T372">dĭ</ta>
            <ta e="T374" id="Seg_6155" s="T373">maʔ</ta>
            <ta e="T376" id="Seg_6156" s="T375">tura-gən</ta>
            <ta e="T377" id="Seg_6157" s="T376">iʔbo-laʔbə</ta>
            <ta e="T379" id="Seg_6158" s="T378">dʼüʔpi</ta>
            <ta e="T380" id="Seg_6159" s="T379">buzo</ta>
            <ta e="T381" id="Seg_6160" s="T380">a</ta>
            <ta e="T382" id="Seg_6161" s="T381">nörbə-sʼtə</ta>
            <ta e="T383" id="Seg_6162" s="T382">dĭ</ta>
            <ta e="T385" id="Seg_6163" s="T885">šĭkə-t</ta>
            <ta e="T388" id="Seg_6164" s="T387">sĭre</ta>
            <ta e="T390" id="Seg_6165" s="T389">pa-ʔi-nə</ta>
            <ta e="T391" id="Seg_6166" s="T390">tarara</ta>
            <ta e="T392" id="Seg_6167" s="T391">amno-laʔbə</ta>
            <ta e="T394" id="Seg_6168" s="T393">dĭ</ta>
            <ta e="T395" id="Seg_6169" s="T394">šĭkə</ta>
            <ta e="T397" id="Seg_6170" s="T396">üjü-t</ta>
            <ta e="T399" id="Seg_6171" s="T398">uda-t</ta>
            <ta e="T400" id="Seg_6172" s="T399">naga</ta>
            <ta e="T401" id="Seg_6173" s="T400">a</ta>
            <ta e="T402" id="Seg_6174" s="T401">kudaj-də</ta>
            <ta e="T403" id="Seg_6175" s="T402">üz-lie</ta>
            <ta e="T404" id="Seg_6176" s="T403">dĭ</ta>
            <ta e="T407" id="Seg_6177" s="T406">üjü-t</ta>
            <ta e="T408" id="Seg_6178" s="T407">uda-t</ta>
            <ta e="T409" id="Seg_6179" s="T408">naga</ta>
            <ta e="T410" id="Seg_6180" s="T409">a</ta>
            <ta e="T411" id="Seg_6181" s="T410">pa-ʔi-nə</ta>
            <ta e="T412" id="Seg_6182" s="T411">mĭl-lie</ta>
            <ta e="T415" id="Seg_6183" s="T414">a</ta>
            <ta e="T416" id="Seg_6184" s="T415">maʔ-ndə</ta>
            <ta e="T417" id="Seg_6185" s="T416">šo-lə-j</ta>
            <ta e="T418" id="Seg_6186" s="T417">iʔbo-laʔbə</ta>
            <ta e="T419" id="Seg_6187" s="T418">dĭ</ta>
            <ta e="T420" id="Seg_6188" s="T419">baltu</ta>
            <ta e="T422" id="Seg_6189" s="T421">uda-t</ta>
            <ta e="T423" id="Seg_6190" s="T422">üjü-t</ta>
            <ta e="T424" id="Seg_6191" s="T423">naga</ta>
            <ta e="T425" id="Seg_6192" s="T424">a</ta>
            <ta e="T426" id="Seg_6193" s="T425">aji-m</ta>
            <ta e="T427" id="Seg_6194" s="T426">bar</ta>
            <ta e="T428" id="Seg_6195" s="T427">kar-laʔbə</ta>
            <ta e="T429" id="Seg_6196" s="T428">dĭ</ta>
            <ta e="T430" id="Seg_6197" s="T429">beržə</ta>
            <ta e="T432" id="Seg_6198" s="T431">üdʼüge</ta>
            <ta e="T434" id="Seg_6199" s="T433">men</ta>
            <ta e="T435" id="Seg_6200" s="T434">ma-ndə</ta>
            <ta e="T436" id="Seg_6201" s="T435">toʔ-ndə</ta>
            <ta e="T437" id="Seg_6202" s="T436">amno-laʔbə-ʔjə</ta>
            <ta e="T438" id="Seg_6203" s="T437">ej</ta>
            <ta e="T439" id="Seg_6204" s="T438">kürüm-nie</ta>
            <ta e="T440" id="Seg_6205" s="T439">i</ta>
            <ta e="T442" id="Seg_6206" s="T441">šindi-m=də</ta>
            <ta e="T443" id="Seg_6207" s="T442">maʔ-tə</ta>
            <ta e="T445" id="Seg_6208" s="T443">ej</ta>
            <ta e="T447" id="Seg_6209" s="T446">šindi-nə=də</ta>
            <ta e="T448" id="Seg_6210" s="T447">maʔ-ndə</ta>
            <ta e="T449" id="Seg_6211" s="T448">ej</ta>
            <ta e="T450" id="Seg_6212" s="T449">öʔ-le</ta>
            <ta e="T451" id="Seg_6213" s="T450">dĭ</ta>
            <ta e="T454" id="Seg_6214" s="T453">Krasnojarskə-ʔi</ta>
            <ta e="T455" id="Seg_6215" s="T454">tura-ʔi</ta>
            <ta e="T456" id="Seg_6216" s="T455">jaʔ-leʔbə-ʔjə</ta>
            <ta e="T457" id="Seg_6217" s="T456">a</ta>
            <ta e="T458" id="Seg_6218" s="T457">döber</ta>
            <ta e="T459" id="Seg_6219" s="T458">pa-ʔi</ta>
            <ta e="T460" id="Seg_6220" s="T459">nʼergö-laʔbə-ʔjə</ta>
            <ta e="T461" id="Seg_6221" s="T460">dĭ</ta>
            <ta e="T462" id="Seg_6222" s="T461">sazən</ta>
            <ta e="T463" id="Seg_6223" s="T462">kandə-ga</ta>
            <ta e="T464" id="Seg_6224" s="T463">dĭʔə</ta>
            <ta e="T467" id="Seg_6225" s="T466">šĭke-t</ta>
            <ta e="T468" id="Seg_6226" s="T467">naga</ta>
            <ta e="T469" id="Seg_6227" s="T468">sima-t</ta>
            <ta e="T470" id="Seg_6228" s="T469">naga</ta>
            <ta e="T471" id="Seg_6229" s="T470">aŋ-də</ta>
            <ta e="T472" id="Seg_6230" s="T471">naga</ta>
            <ta e="T473" id="Seg_6231" s="T472">a</ta>
            <ta e="T474" id="Seg_6232" s="T473">măl-lia</ta>
            <ta e="T475" id="Seg_6233" s="T474">kumen</ta>
            <ta e="T476" id="Seg_6234" s="T475">vremja</ta>
            <ta e="T477" id="Seg_6235" s="T476">kam-bi</ta>
            <ta e="T478" id="Seg_6236" s="T477">dĭ</ta>
            <ta e="T479" id="Seg_6237" s="T478">časɨ-ʔi</ta>
            <ta e="T481" id="Seg_6238" s="T480">iʔbo-laʔbə</ta>
            <ta e="T482" id="Seg_6239" s="T481">dăk</ta>
            <ta e="T483" id="Seg_6240" s="T482">üdʼüge</ta>
            <ta e="T484" id="Seg_6241" s="T483">tospak-tə</ta>
            <ta e="T485" id="Seg_6242" s="T484">a</ta>
            <ta e="T486" id="Seg_6243" s="T485">nu-laʔbə</ta>
            <ta e="T487" id="Seg_6244" s="T486">dăk</ta>
            <ta e="T488" id="Seg_6245" s="T487">ugandə</ta>
            <ta e="T489" id="Seg_6246" s="T488">urgo</ta>
            <ta e="T490" id="Seg_6247" s="T489">urgo</ta>
            <ta e="T491" id="Seg_6248" s="T490">ine-t-si</ta>
            <ta e="T493" id="Seg_6249" s="T492">duga</ta>
            <ta e="T495" id="Seg_6250" s="T494">amno-laʔbə</ta>
            <ta e="T496" id="Seg_6251" s="T495">koʔbdo</ta>
            <ta e="T497" id="Seg_6252" s="T496">bar</ta>
            <ta e="T498" id="Seg_6253" s="T497">ospa-ʔi-zi</ta>
            <ta e="T499" id="Seg_6254" s="T498">tĭ</ta>
            <ta e="T500" id="Seg_6255" s="T499">a</ta>
            <ta e="T501" id="Seg_6256" s="T500">dĭ</ta>
            <ta e="T502" id="Seg_6257" s="T501">indak</ta>
            <ta e="T504" id="Seg_6258" s="T503">oʔbdə-bia-m</ta>
            <ta e="T505" id="Seg_6259" s="T504">oʔbdə-bia-m</ta>
            <ta e="T506" id="Seg_6260" s="T505">nʼitka-ʔi</ta>
            <ta e="T507" id="Seg_6261" s="T506">ej</ta>
            <ta e="T509" id="Seg_6262" s="T508">ej</ta>
            <ta e="T510" id="Seg_6263" s="T509">mo-bia-m</ta>
            <ta e="T511" id="Seg_6264" s="T510">oʔbdə-sʼtə</ta>
            <ta e="T512" id="Seg_6265" s="T511">dĭ</ta>
            <ta e="T513" id="Seg_6266" s="T512">aʔtʼi</ta>
            <ta e="T515" id="Seg_6267" s="T514">amno-laʔbə</ta>
            <ta e="T516" id="Seg_6268" s="T515">büzʼe</ta>
            <ta e="T517" id="Seg_6269" s="T516">grʼadka-gən</ta>
            <ta e="T518" id="Seg_6270" s="T517">bar</ta>
            <ta e="T519" id="Seg_6271" s="T518">iʔgö</ta>
            <ta e="T520" id="Seg_6272" s="T519">zaplatka-ʔi</ta>
            <ta e="T521" id="Seg_6273" s="T520">šində</ta>
            <ta e="T523" id="Seg_6274" s="T522">ku-laʔbə</ta>
            <ta e="T524" id="Seg_6275" s="T523">bar</ta>
            <ta e="T525" id="Seg_6276" s="T524">dʼor-laʔbə</ta>
            <ta e="T526" id="Seg_6277" s="T525">dĭ</ta>
            <ta e="T527" id="Seg_6278" s="T526">köbergən</ta>
            <ta e="T529" id="Seg_6279" s="T528">amno-laʔbə</ta>
            <ta e="T530" id="Seg_6280" s="T529">iʔgö</ta>
            <ta e="T532" id="Seg_6281" s="T530">oldʼa</ta>
            <ta e="T535" id="Seg_6282" s="T534">sejʔpü</ta>
            <ta e="T536" id="Seg_6283" s="T535">aktʼa</ta>
            <ta e="T540" id="Seg_6284" s="T539">bʼeʔ</ta>
            <ta e="T542" id="Seg_6285" s="T541">sejʔpü</ta>
            <ta e="T543" id="Seg_6286" s="T542">oldʼa</ta>
            <ta e="T544" id="Seg_6287" s="T543">a</ta>
            <ta e="T545" id="Seg_6288" s="T544">kaj-zittə</ta>
            <ta e="T546" id="Seg_6289" s="T545">naga</ta>
            <ta e="T547" id="Seg_6290" s="T546">dĭ</ta>
            <ta e="T548" id="Seg_6291" s="T547">kăpusta</ta>
            <ta e="T550" id="Seg_6292" s="T549">amno-laʔbə</ta>
            <ta e="T551" id="Seg_6293" s="T550">koʔbdo</ta>
            <ta e="T552" id="Seg_6294" s="T551">dʼü-gən</ta>
            <ta e="T553" id="Seg_6295" s="T552">bos-tə</ta>
            <ta e="T554" id="Seg_6296" s="T553">kömə</ta>
            <ta e="T555" id="Seg_6297" s="T554">a</ta>
            <ta e="T556" id="Seg_6298" s="T555">eʔbdə-t</ta>
            <ta e="T557" id="Seg_6299" s="T556">nʼiʔnen</ta>
            <ta e="T558" id="Seg_6300" s="T557">dĭ</ta>
            <ta e="T561" id="Seg_6301" s="T560">măn</ta>
            <ta e="T562" id="Seg_6302" s="T561">ku-bia-m</ta>
            <ta e="T563" id="Seg_6303" s="T562">plʼotka</ta>
            <ta e="T564" id="Seg_6304" s="T563">a</ta>
            <ta e="T565" id="Seg_6305" s="T564">kan-zittə</ta>
            <ta e="T566" id="Seg_6306" s="T565">ej</ta>
            <ta e="T567" id="Seg_6307" s="T566">mo-bia-m</ta>
            <ta e="T568" id="Seg_6308" s="T567">a</ta>
            <ta e="T569" id="Seg_6309" s="T568">dĭ</ta>
            <ta e="T570" id="Seg_6310" s="T569">ĭmbi</ta>
            <ta e="T571" id="Seg_6311" s="T570">penzə</ta>
            <ta e="T573" id="Seg_6312" s="T572">šonə-ga</ta>
            <ta e="T574" id="Seg_6313" s="T573">koʔbdo</ta>
            <ta e="T575" id="Seg_6314" s="T574">i</ta>
            <ta e="T576" id="Seg_6315" s="T575">dĭn</ta>
            <ta e="T577" id="Seg_6316" s="T576">eʔbdə-ʔi</ta>
            <ta e="T578" id="Seg_6317" s="T577">bar</ta>
            <ta e="T580" id="Seg_6318" s="T579">kurgo-na-ʔi</ta>
            <ta e="T581" id="Seg_6319" s="T580">bar</ta>
            <ta e="T583" id="Seg_6320" s="T582">dĭ</ta>
            <ta e="T584" id="Seg_6321" s="T583">soroka</ta>
            <ta e="T585" id="Seg_6322" s="T584">i-ge</ta>
            <ta e="T587" id="Seg_6323" s="T586">šide</ta>
            <ta e="T588" id="Seg_6324" s="T587">nu-ga-ʔi</ta>
            <ta e="T589" id="Seg_6325" s="T588">šide</ta>
            <ta e="T590" id="Seg_6326" s="T589">iʔbo-laʔbə-ʔjə</ta>
            <ta e="T591" id="Seg_6327" s="T590">onʼiʔ</ta>
            <ta e="T592" id="Seg_6328" s="T591">mĭl-leʔbə</ta>
            <ta e="T593" id="Seg_6329" s="T592">sumna</ta>
            <ta e="T594" id="Seg_6330" s="T593">mĭl-leʔbə</ta>
            <ta e="T595" id="Seg_6331" s="T594">a</ta>
            <ta e="T596" id="Seg_6332" s="T595">sejʔpü</ta>
            <ta e="T598" id="Seg_6333" s="T597">kaj-laʔbə</ta>
            <ta e="T599" id="Seg_6334" s="T598">dĭ</ta>
            <ta e="T600" id="Seg_6335" s="T599">aji</ta>
            <ta e="T601" id="Seg_6336" s="T600">i-ge</ta>
            <ta e="T604" id="Seg_6337" s="T603">ular</ta>
            <ta e="T605" id="Seg_6338" s="T604">iʔgö</ta>
            <ta e="T606" id="Seg_6339" s="T605">mĭl-leʔbə-ʔi</ta>
            <ta e="T607" id="Seg_6340" s="T606">a</ta>
            <ta e="T608" id="Seg_6341" s="T607">pastux</ta>
            <ta e="T611" id="Seg_6342" s="T609">amnu-t</ta>
            <ta e="T612" id="Seg_6343" s="T611">dĭ</ta>
            <ta e="T613" id="Seg_6344" s="T612">mʼesʼas</ta>
            <ta e="T614" id="Seg_6345" s="T613">i</ta>
            <ta e="T615" id="Seg_6346" s="T614">zvʼozda-ʔi</ta>
            <ta e="T617" id="Seg_6347" s="T616">šide</ta>
            <ta e="T618" id="Seg_6348" s="T617">ši</ta>
            <ta e="T619" id="Seg_6349" s="T618">i</ta>
            <ta e="T620" id="Seg_6350" s="T619">šide</ta>
            <ta e="T621" id="Seg_6351" s="T620">puʔmə-ʔi</ta>
            <ta e="T622" id="Seg_6352" s="T621">a</ta>
            <ta e="T623" id="Seg_6353" s="T622">šüjö-gən</ta>
            <ta e="T624" id="Seg_6354" s="T623">marka</ta>
            <ta e="T625" id="Seg_6355" s="T624">kaptə-ʔi</ta>
            <ta e="T627" id="Seg_6356" s="T626">măn</ta>
            <ta e="T628" id="Seg_6357" s="T627">üge</ta>
            <ta e="T629" id="Seg_6358" s="T628">am-naʔbə-m</ta>
            <ta e="T630" id="Seg_6359" s="T629">am-naʔbə-m</ta>
            <ta e="T631" id="Seg_6360" s="T630">a</ta>
            <ta e="T632" id="Seg_6361" s="T631">tüʔ-sittə</ta>
            <ta e="T633" id="Seg_6362" s="T632">ej</ta>
            <ta e="T634" id="Seg_6363" s="T633">mo-lia-m</ta>
            <ta e="T635" id="Seg_6364" s="T634">bura</ta>
            <ta e="T637" id="Seg_6365" s="T636">pʼetux</ta>
            <ta e="T638" id="Seg_6366" s="T637">amno-laʔbə</ta>
            <ta e="T639" id="Seg_6367" s="T638">zabor-gən</ta>
            <ta e="T640" id="Seg_6368" s="T639">xvost</ta>
            <ta e="T641" id="Seg_6369" s="T640">dʼü-gən</ta>
            <ta e="T642" id="Seg_6370" s="T641">a</ta>
            <ta e="T643" id="Seg_6371" s="T642">kirgar-laʔbə</ta>
            <ta e="T644" id="Seg_6372" s="T643">kudaj-də</ta>
            <ta e="T645" id="Seg_6373" s="T644">dĭ</ta>
            <ta e="T646" id="Seg_6374" s="T645">koŋgoro</ta>
            <ta e="T648" id="Seg_6375" s="T647">nu-ga</ta>
            <ta e="T649" id="Seg_6376" s="T648">nʼi</ta>
            <ta e="T650" id="Seg_6377" s="T649">šüjö-gən</ta>
            <ta e="T652" id="Seg_6378" s="T651">neni-leʔbə</ta>
            <ta e="T653" id="Seg_6379" s="T652">a</ta>
            <ta e="T654" id="Seg_6380" s="T653">bos-tə</ta>
            <ta e="T655" id="Seg_6381" s="T654">bar</ta>
            <ta e="T656" id="Seg_6382" s="T655">kĭnz-leʔbə</ta>
            <ta e="T657" id="Seg_6383" s="T656">dĭ</ta>
            <ta e="T658" id="Seg_6384" s="T657">kujdərgan</ta>
            <ta e="T660" id="Seg_6385" s="T659">šide-göʔ</ta>
            <ta e="T661" id="Seg_6386" s="T660">kan-də-ga-ʔi</ta>
            <ta e="T662" id="Seg_6387" s="T661">onʼiʔ</ta>
            <ta e="T663" id="Seg_6388" s="T662">kandə-ga</ta>
            <ta e="T664" id="Seg_6389" s="T663">onʼiʔ</ta>
            <ta e="T665" id="Seg_6390" s="T664">bĭt-lie</ta>
            <ta e="T666" id="Seg_6391" s="T665">dĭ</ta>
            <ta e="T667" id="Seg_6392" s="T666">üjü-ʔi</ta>
            <ta e="T669" id="Seg_6393" s="T668">toʔ-nar-a-ʔ</ta>
            <ta e="T670" id="Seg_6394" s="T669">măna</ta>
            <ta e="T671" id="Seg_6395" s="T670">münör-e-ʔ</ta>
            <ta e="T672" id="Seg_6396" s="T671">măna</ta>
            <ta e="T673" id="Seg_6397" s="T672">sʼa-ʔ</ta>
            <ta e="T674" id="Seg_6398" s="T673">măna</ta>
            <ta e="T675" id="Seg_6399" s="T674">i-ge</ta>
            <ta e="T676" id="Seg_6400" s="T675">măna</ta>
            <ta e="T677" id="Seg_6401" s="T676">dĭ</ta>
            <ta e="T678" id="Seg_6402" s="T677">sanə</ta>
            <ta e="T680" id="Seg_6403" s="T679">ed-leʔbə</ta>
            <ta e="T681" id="Seg_6404" s="T680">bar</ta>
            <ta e="T682" id="Seg_6405" s="T681">a</ta>
            <ta e="T683" id="Seg_6406" s="T682">il</ta>
            <ta e="T684" id="Seg_6407" s="T683">bar</ta>
            <ta e="T685" id="Seg_6408" s="T684">kabar-laʔbə-ʔjə</ta>
            <ta e="T686" id="Seg_6409" s="T685">băzə-jd-laʔbə-ʔjə</ta>
            <ta e="T687" id="Seg_6410" s="T686">kĭškə-leʔbə-ʔjə</ta>
            <ta e="T688" id="Seg_6411" s="T687">dĭ</ta>
            <ta e="T689" id="Seg_6412" s="T688">rušnʼik</ta>
            <ta e="T690" id="Seg_6413" s="T689">i-ge</ta>
            <ta e="T692" id="Seg_6414" s="T888">pa-ʔi</ta>
            <ta e="T693" id="Seg_6415" s="T692">noʔ</ta>
            <ta e="T694" id="Seg_6416" s="T693">özer-leʔbə</ta>
            <ta e="T697" id="Seg_6417" s="T696">dĭ</ta>
            <ta e="T698" id="Seg_6418" s="T697">müjö</ta>
            <ta e="T700" id="Seg_6419" s="T699">kömə</ta>
            <ta e="T701" id="Seg_6420" s="T700">poʔto</ta>
            <ta e="T702" id="Seg_6421" s="T701">iʔbo-laʔ-pi</ta>
            <ta e="T703" id="Seg_6422" s="T702">dĭn</ta>
            <ta e="T704" id="Seg_6423" s="T703">noʔ</ta>
            <ta e="T705" id="Seg_6424" s="T704">ej</ta>
            <ta e="T706" id="Seg_6425" s="T705">özer-leʔbə</ta>
            <ta e="T707" id="Seg_6426" s="T706">šü</ta>
            <ta e="T708" id="Seg_6427" s="T707">dĭ</ta>
            <ta e="T710" id="Seg_6428" s="T709">kam-bi</ta>
            <ta e="T711" id="Seg_6429" s="T710">keʔbde-j-le</ta>
            <ta e="T712" id="Seg_6430" s="T711">i</ta>
            <ta e="T713" id="Seg_6431" s="T712">măn</ta>
            <ta e="T714" id="Seg_6432" s="T713">kam-bia-m</ta>
            <ta e="T715" id="Seg_6433" s="T714">keʔbde</ta>
            <ta e="T716" id="Seg_6434" s="T715">nĭŋgə-sʼtə</ta>
            <ta e="T717" id="Seg_6435" s="T716">i</ta>
            <ta e="T718" id="Seg_6436" s="T717">išo</ta>
            <ta e="T719" id="Seg_6437" s="T718">onʼiʔ</ta>
            <ta e="T720" id="Seg_6438" s="T719">koʔbdo</ta>
            <ta e="T721" id="Seg_6439" s="T720">kam-bi</ta>
            <ta e="T724" id="Seg_6440" s="T723">ine-zi</ta>
            <ta e="T727" id="Seg_6441" s="T726">kam-bi-baʔ</ta>
            <ta e="T728" id="Seg_6442" s="T727">šumiga-nə</ta>
            <ta e="T729" id="Seg_6443" s="T728">dĭbər</ta>
            <ta e="T730" id="Seg_6444" s="T729">šo-bi-baʔ</ta>
            <ta e="T731" id="Seg_6445" s="T730">ša-la-ʔjə</ta>
            <ta e="T732" id="Seg_6446" s="T731">keʔbde</ta>
            <ta e="T733" id="Seg_6447" s="T732">măndo-r-bi-baʔ</ta>
            <ta e="T734" id="Seg_6448" s="T733">i</ta>
            <ta e="T735" id="Seg_6449" s="T734">idʼiʔeʔe</ta>
            <ta e="T736" id="Seg_6450" s="T735">oʔbdə-bi-baʔ</ta>
            <ta e="T737" id="Seg_6451" s="T736">dĭgəttə</ta>
            <ta e="T738" id="Seg_6452" s="T737">šo-bi-baʔ</ta>
            <ta e="T739" id="Seg_6453" s="T738">gijen</ta>
            <ta e="T740" id="Seg_6454" s="T739">ine</ta>
            <ta e="T741" id="Seg_6455" s="T740">mĭn-leʔbə</ta>
            <ta e="T742" id="Seg_6456" s="T741">ia-m</ta>
            <ta e="T743" id="Seg_6457" s="T742">măn-də</ta>
            <ta e="T744" id="Seg_6458" s="T743">ugandə</ta>
            <ta e="T745" id="Seg_6459" s="T744">sagər</ta>
            <ta e="T747" id="Seg_6460" s="T746">šonə-ga</ta>
            <ta e="T748" id="Seg_6461" s="T747">nada</ta>
            <ta e="T749" id="Seg_6462" s="T748">a-zittə</ta>
            <ta e="T750" id="Seg_6463" s="T749">maʔ</ta>
            <ta e="T751" id="Seg_6464" s="T750">ato</ta>
            <ta e="T752" id="Seg_6465" s="T751">kunol-zittə</ta>
            <ta e="T753" id="Seg_6466" s="T752">ej</ta>
            <ta e="T754" id="Seg_6467" s="T753">jakše</ta>
            <ta e="T755" id="Seg_6468" s="T754">mo-lə-j</ta>
            <ta e="T756" id="Seg_6469" s="T755">gəttə</ta>
            <ta e="T757" id="Seg_6470" s="T756">maʔ</ta>
            <ta e="T758" id="Seg_6471" s="T757">a-bi-baʔ</ta>
            <ta e="T759" id="Seg_6472" s="T758">mĭnzər-bi-beʔ</ta>
            <ta e="T760" id="Seg_6473" s="T759">segi</ta>
            <ta e="T761" id="Seg_6474" s="T760">bü</ta>
            <ta e="T762" id="Seg_6475" s="T761">bĭʔ-pi-beʔ</ta>
            <ta e="T763" id="Seg_6476" s="T762">a</ta>
            <ta e="T764" id="Seg_6477" s="T763">dĭn</ta>
            <ta e="T766" id="Seg_6478" s="T765">i-bi</ta>
            <ta e="T767" id="Seg_6479" s="T766">pălăvik</ta>
            <ta e="T768" id="Seg_6480" s="T767">tĭbər</ta>
            <ta e="T769" id="Seg_6481" s="T768">pălăvik-tə</ta>
            <ta e="T770" id="Seg_6482" s="T769">edəbibiʔibeʔ</ta>
            <ta e="T771" id="Seg_6483" s="T770">pa-ʔi-nə</ta>
            <ta e="T772" id="Seg_6484" s="T771">dĭgəttə</ta>
            <ta e="T773" id="Seg_6485" s="T772">iʔbə-beʔ</ta>
            <ta e="T774" id="Seg_6486" s="T773">kunol-zittə</ta>
            <ta e="T775" id="Seg_6487" s="T774">a</ta>
            <ta e="T776" id="Seg_6488" s="T775">nüdʼi-n</ta>
            <ta e="T777" id="Seg_6489" s="T776">bar</ta>
            <ta e="T778" id="Seg_6490" s="T777">ugandə</ta>
            <ta e="T779" id="Seg_6491" s="T778">tăŋ</ta>
            <ta e="T780" id="Seg_6492" s="T779">surno</ta>
            <ta e="T781" id="Seg_6493" s="T780">šonə-bi</ta>
            <ta e="T782" id="Seg_6494" s="T781">šonə-bi</ta>
            <ta e="T783" id="Seg_6495" s="T782">ertə-n</ta>
            <ta e="T784" id="Seg_6496" s="T783">uʔbdə-bi-baʔ</ta>
            <ta e="T785" id="Seg_6497" s="T784">bar</ta>
            <ta e="T786" id="Seg_6498" s="T785">nünör-bi-baʔ</ta>
            <ta e="T787" id="Seg_6499" s="T786">ine-ʔi</ta>
            <ta e="T788" id="Seg_6500" s="T787">dʼaʔ-pi-baʔ</ta>
            <ta e="T789" id="Seg_6501" s="T788">kam-bi-baʔ</ta>
            <ta e="T790" id="Seg_6502" s="T789">maʔ-nʼi</ta>
            <ta e="T792" id="Seg_6503" s="T791">šo-bi-baʔ</ta>
            <ta e="T793" id="Seg_6504" s="T792">bü-nə</ta>
            <ta e="T794" id="Seg_6505" s="T793">kăde=də</ta>
            <ta e="T795" id="Seg_6506" s="T794">nʼelʼzʼa</ta>
            <ta e="T796" id="Seg_6507" s="T795">kan-zittə</ta>
            <ta e="T797" id="Seg_6508" s="T796">dĭgəttə</ta>
            <ta e="T800" id="Seg_6509" s="T799">bü-nə</ta>
            <ta e="T801" id="Seg_6510" s="T800">kan-bi-baʔ</ta>
            <ta e="T802" id="Seg_6511" s="T801">kan-bi-baʔ</ta>
            <ta e="T803" id="Seg_6512" s="T802">dĭgəttə</ta>
            <ta e="T804" id="Seg_6513" s="T803">ku-bi-baʔ</ta>
            <ta e="T805" id="Seg_6514" s="T804">mostə</ta>
            <ta e="T806" id="Seg_6515" s="T805">mostə</ta>
            <ta e="T808" id="Seg_6516" s="T807">šo-bi-baʔ</ta>
            <ta e="T809" id="Seg_6517" s="T808">i</ta>
            <ta e="T810" id="Seg_6518" s="T809">maʔ-nʼi</ta>
            <ta e="T811" id="Seg_6519" s="T810">šo-bi-baʔ</ta>
            <ta e="T814" id="Seg_6520" s="T813">măn</ta>
            <ta e="T815" id="Seg_6521" s="T814">teinen</ta>
            <ta e="T816" id="Seg_6522" s="T815">ertə</ta>
            <ta e="T817" id="Seg_6523" s="T816">uʔbdə-bia-m</ta>
            <ta e="T818" id="Seg_6524" s="T817">măgăzin-də</ta>
            <ta e="T819" id="Seg_6525" s="T818">kam-bia-m</ta>
            <ta e="T821" id="Seg_6526" s="T889">nu-bia-m</ta>
            <ta e="T822" id="Seg_6527" s="T821">dĭgəttə</ta>
            <ta e="T823" id="Seg_6528" s="T822">šide</ta>
            <ta e="T824" id="Seg_6529" s="T823">šide</ta>
            <ta e="T825" id="Seg_6530" s="T824">čas</ta>
            <ta e="T826" id="Seg_6531" s="T825">nu-bia-m</ta>
            <ta e="T827" id="Seg_6532" s="T826">dĭgəttə</ta>
            <ta e="T828" id="Seg_6533" s="T827">i-bie-m</ta>
            <ta e="T829" id="Seg_6534" s="T828">kola</ta>
            <ta e="T830" id="Seg_6535" s="T829">i</ta>
            <ta e="T831" id="Seg_6536" s="T830">šo-bia-m</ta>
            <ta e="T832" id="Seg_6537" s="T831">maʔ-nə</ta>
            <ta e="T833" id="Seg_6538" s="T832">i</ta>
            <ta e="T834" id="Seg_6539" s="T833">šiʔ</ta>
            <ta e="T835" id="Seg_6540" s="T834">šo-bi-laʔ</ta>
            <ta e="T837" id="Seg_6541" s="T836">gəttə</ta>
            <ta e="T838" id="Seg_6542" s="T837">măn</ta>
            <ta e="T839" id="Seg_6543" s="T838">i-bie-m</ta>
            <ta e="T840" id="Seg_6544" s="T839">oʔb</ta>
            <ta e="T841" id="Seg_6545" s="T840">kilo</ta>
            <ta e="T842" id="Seg_6546" s="T841">aktʼa</ta>
            <ta e="T843" id="Seg_6547" s="T842">mĭ-bie-m</ta>
            <ta e="T844" id="Seg_6548" s="T843">onʼiʔ</ta>
            <ta e="T845" id="Seg_6549" s="T844">šălkovej</ta>
            <ta e="T846" id="Seg_6550" s="T845">bʼeʔ</ta>
            <ta e="T847" id="Seg_6551" s="T846">teʔtə</ta>
            <ta e="T848" id="Seg_6552" s="T847">mĭ-bie-m</ta>
            <ta e="T849" id="Seg_6553" s="T848">onʼiʔ</ta>
            <ta e="T850" id="Seg_6554" s="T849">šălkovej</ta>
            <ta e="T851" id="Seg_6555" s="T850">bʼeʔ</ta>
            <ta e="T852" id="Seg_6556" s="T851">teʔtə</ta>
            <ta e="T854" id="Seg_6557" s="T853">dĭn</ta>
            <ta e="T855" id="Seg_6558" s="T854">bar</ta>
            <ta e="T856" id="Seg_6559" s="T855">ne-zeŋ</ta>
            <ta e="T857" id="Seg_6560" s="T856">kudo-nz-laʔbə-ʔjə</ta>
            <ta e="T858" id="Seg_6561" s="T857">kirgar-laʔbə-ʔjə</ta>
            <ta e="T859" id="Seg_6562" s="T858">măn-lia-ʔi</ta>
            <ta e="T860" id="Seg_6563" s="T859">amga</ta>
            <ta e="T861" id="Seg_6564" s="T860">kola</ta>
            <ta e="T862" id="Seg_6565" s="T861">onʼiʔ</ta>
            <ta e="T864" id="Seg_6566" s="T863">onʼiʔ</ta>
            <ta e="T865" id="Seg_6567" s="T864">kilo</ta>
            <ta e="T866" id="Seg_6568" s="T865">mĭ-leʔbə</ta>
            <ta e="T867" id="Seg_6569" s="T866">dĭ</ta>
            <ta e="T868" id="Seg_6570" s="T867">mĭ-bi</ta>
            <ta e="T869" id="Seg_6571" s="T868">onʼiʔ</ta>
            <ta e="T870" id="Seg_6572" s="T869">bar</ta>
            <ta e="T871" id="Seg_6573" s="T870">ne-zeŋ-də</ta>
            <ta e="T872" id="Seg_6574" s="T871">onʼiʔ</ta>
            <ta e="T873" id="Seg_6575" s="T872">kilo</ta>
            <ta e="T874" id="Seg_6576" s="T873">mĭ-bi</ta>
            <ta e="T875" id="Seg_6577" s="T874">măn-lia-ʔi</ta>
            <ta e="T876" id="Seg_6578" s="T875">es-seŋ-bə</ta>
            <ta e="T877" id="Seg_6579" s="T876">kirgar-laʔbə-ʔjə</ta>
            <ta e="T878" id="Seg_6580" s="T877">kola</ta>
            <ta e="T879" id="Seg_6581" s="T878">kola</ta>
            <ta e="T880" id="Seg_6582" s="T879">am-zittə</ta>
            <ta e="T881" id="Seg_6583" s="T880">kereʔ</ta>
            <ta e="T882" id="Seg_6584" s="T881">dĭ-zeŋ-də</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T1" id="Seg_6585" s="T0">dĭgəttə</ta>
            <ta e="T2" id="Seg_6586" s="T1">dĭ</ta>
            <ta e="T3" id="Seg_6587" s="T2">nʼi-zAŋ</ta>
            <ta e="T4" id="Seg_6588" s="T3">aʔtʼi-Tə</ta>
            <ta e="T5" id="Seg_6589" s="T4">kan-bi-jəʔ</ta>
            <ta e="T6" id="Seg_6590" s="T5">šo-bi-jəʔ</ta>
            <ta e="T7" id="Seg_6591" s="T6">šo-bi-jəʔ</ta>
            <ta e="T8" id="Seg_6592" s="T7">dĭgəttə</ta>
            <ta e="T9" id="Seg_6593" s="T8">aʔtʼi</ta>
            <ta e="T10" id="Seg_6594" s="T9">aʔtʼi-Kən</ta>
            <ta e="T11" id="Seg_6595" s="T10">toʔ-gəndə</ta>
            <ta e="T12" id="Seg_6596" s="T11">amnə-bi-jəʔ</ta>
            <ta e="T13" id="Seg_6597" s="T12">ku-liA-jəʔ</ta>
            <ta e="T14" id="Seg_6598" s="T13">šonə-gA</ta>
            <ta e="T15" id="Seg_6599" s="T14">kăldun</ta>
            <ta e="T16" id="Seg_6600" s="T15">onʼiʔ</ta>
            <ta e="T17" id="Seg_6601" s="T16">ine</ta>
            <ta e="T18" id="Seg_6602" s="T17">kömə</ta>
            <ta e="T19" id="Seg_6603" s="T18">dĭgəttə</ta>
            <ta e="T20" id="Seg_6604" s="T19">dĭ</ta>
            <ta e="T21" id="Seg_6605" s="T20">bar</ta>
            <ta e="T23" id="Seg_6606" s="T21">tăŋ</ta>
            <ta e="T24" id="Seg_6607" s="T23">dĭgəttə</ta>
            <ta e="T25" id="Seg_6608" s="T24">onʼiʔ</ta>
            <ta e="T27" id="Seg_6609" s="T26">tăŋ</ta>
            <ta e="T28" id="Seg_6610" s="T27">ine-t</ta>
            <ta e="T29" id="Seg_6611" s="T28">bar</ta>
            <ta e="T31" id="Seg_6612" s="T30">kălʼenka-Tə</ta>
            <ta e="T33" id="Seg_6613" s="T32">dĭ</ta>
            <ta e="T34" id="Seg_6614" s="T33">ine</ta>
            <ta e="T35" id="Seg_6615" s="T34">saʔmə-luʔbdə-bi</ta>
            <ta e="T36" id="Seg_6616" s="T35">kălʼenka-jəʔ-Tə</ta>
            <ta e="T37" id="Seg_6617" s="T36">dĭgəttə</ta>
            <ta e="T38" id="Seg_6618" s="T37">dĭ</ta>
            <ta e="T39" id="Seg_6619" s="T38">kăldun</ta>
            <ta e="T40" id="Seg_6620" s="T39">măn-ntə-bi</ta>
            <ta e="T41" id="Seg_6621" s="T40">dĭ</ta>
            <ta e="T42" id="Seg_6622" s="T41">dĭn</ta>
            <ta e="T44" id="Seg_6623" s="T43">eneidəne</ta>
            <ta e="T45" id="Seg_6624" s="T44">amno-laʔbə</ta>
            <ta e="T46" id="Seg_6625" s="T45">ugaːndə</ta>
            <ta e="T47" id="Seg_6626" s="T46">pim-liA-m</ta>
            <ta e="T48" id="Seg_6627" s="T47">dĭgəttə</ta>
            <ta e="T49" id="Seg_6628" s="T48">kan-bi</ta>
            <ta e="T50" id="Seg_6629" s="T49">amno-laʔbə-jəʔ</ta>
            <ta e="T51" id="Seg_6630" s="T50">amno-laʔbə-jəʔ</ta>
            <ta e="T52" id="Seg_6631" s="T51">bazoʔ</ta>
            <ta e="T53" id="Seg_6632" s="T52">šonə-gA</ta>
            <ta e="T54" id="Seg_6633" s="T53">sagər</ta>
            <ta e="T55" id="Seg_6634" s="T54">ine-t-ziʔ</ta>
            <ta e="T57" id="Seg_6635" s="T56">baška</ta>
            <ta e="T58" id="Seg_6636" s="T57">šide</ta>
            <ta e="T59" id="Seg_6637" s="T58">šide-git</ta>
            <ta e="T60" id="Seg_6638" s="T59">kăldun</ta>
            <ta e="T61" id="Seg_6639" s="T60">dĭgəttə</ta>
            <ta e="T62" id="Seg_6640" s="T61">dĭ-zAŋ</ta>
            <ta e="T63" id="Seg_6641" s="T62">bazoʔ</ta>
            <ta e="T65" id="Seg_6642" s="T64">dĭgəttə</ta>
            <ta e="T66" id="Seg_6643" s="T65">dĭ</ta>
            <ta e="T67" id="Seg_6644" s="T66">saʔmə-luʔbdə-bi</ta>
            <ta e="T68" id="Seg_6645" s="T67">kălʼenka-jəʔ-Tə</ta>
            <ta e="T69" id="Seg_6646" s="T68">dĭ</ta>
            <ta e="T70" id="Seg_6647" s="T69">tože</ta>
            <ta e="T71" id="Seg_6648" s="T70">măn-ntə</ta>
            <ta e="T72" id="Seg_6649" s="T71">eneidəne</ta>
            <ta e="T73" id="Seg_6650" s="T72">ugaːndə</ta>
            <ta e="T74" id="Seg_6651" s="T73">kuštü</ta>
            <ta e="T75" id="Seg_6652" s="T74">ugaːndə</ta>
            <ta e="T76" id="Seg_6653" s="T75">măn</ta>
            <ta e="T77" id="Seg_6654" s="T76">pim-liA-m</ta>
            <ta e="T78" id="Seg_6655" s="T77">dĭgəttə</ta>
            <ta e="T80" id="Seg_6656" s="T79">ine</ta>
            <ta e="T81" id="Seg_6657" s="T80">ine-t</ta>
            <ta e="T82" id="Seg_6658" s="T81">uʔbdə-bi</ta>
            <ta e="T83" id="Seg_6659" s="T82">kan-bi</ta>
            <ta e="T84" id="Seg_6660" s="T83">dĭgəttə</ta>
            <ta e="T85" id="Seg_6661" s="T84">amno-laʔbə-jəʔ</ta>
            <ta e="T86" id="Seg_6662" s="T85">amno-laʔbə-jəʔ</ta>
            <ta e="T88" id="Seg_6663" s="T87">nagur-git</ta>
            <ta e="T89" id="Seg_6664" s="T88">ine</ta>
            <ta e="T90" id="Seg_6665" s="T89">šonə-gA</ta>
            <ta e="T91" id="Seg_6666" s="T90">dö</ta>
            <ta e="T92" id="Seg_6667" s="T91">ine</ta>
            <ta e="T93" id="Seg_6668" s="T92">sĭri</ta>
            <ta e="T94" id="Seg_6669" s="T93">dĭ-zAŋ</ta>
            <ta e="T95" id="Seg_6670" s="T94">bazoʔ</ta>
            <ta e="T97" id="Seg_6671" s="T96">dĭgəttə</ta>
            <ta e="T98" id="Seg_6672" s="T97">dĭ</ta>
            <ta e="T99" id="Seg_6673" s="T98">ine</ta>
            <ta e="T100" id="Seg_6674" s="T99">nuldə-bi</ta>
            <ta e="T101" id="Seg_6675" s="T100">nu-laʔbə</ta>
            <ta e="T102" id="Seg_6676" s="T101">dĭ</ta>
            <ta e="T103" id="Seg_6677" s="T102">kăldun</ta>
            <ta e="T105" id="Seg_6678" s="T104">kan-bi</ta>
            <ta e="T106" id="Seg_6679" s="T105">dĭ-zAŋ-Tə</ta>
            <ta e="T107" id="Seg_6680" s="T106">girgit</ta>
            <ta e="T108" id="Seg_6681" s="T107">ešši-zAŋ</ta>
            <ta e="T109" id="Seg_6682" s="T108">amno-laʔbə-jəʔ</ta>
            <ta e="T110" id="Seg_6683" s="T109">da</ta>
            <ta e="T111" id="Seg_6684" s="T110">miʔ</ta>
            <ta e="T112" id="Seg_6685" s="T111">vot</ta>
            <ta e="T113" id="Seg_6686" s="T112">miʔnʼibeʔ</ta>
            <ta e="T114" id="Seg_6687" s="T113">bar</ta>
            <ta e="T115" id="Seg_6688" s="T114">bü-Tə</ta>
            <ta e="T116" id="Seg_6689" s="T115">barəʔ-luʔbdə-bi-jəʔ</ta>
            <ta e="T117" id="Seg_6690" s="T116">măn</ta>
            <ta e="T118" id="Seg_6691" s="T117">miʔ</ta>
            <ta e="T119" id="Seg_6692" s="T118">döbər</ta>
            <ta e="T120" id="Seg_6693" s="T119">šo-bi-bAʔ</ta>
            <ta e="T121" id="Seg_6694" s="T120">dĭgəttə</ta>
            <ta e="T122" id="Seg_6695" s="T121">no</ta>
            <ta e="T123" id="Seg_6696" s="T122">amno-KAʔ</ta>
            <ta e="T124" id="Seg_6697" s="T123">dön</ta>
            <ta e="T125" id="Seg_6698" s="T124">dĭ</ta>
            <ta e="T126" id="Seg_6699" s="T125">dĭ-Tə</ta>
            <ta e="T127" id="Seg_6700" s="T126">a-bi-jəʔ</ta>
            <ta e="T129" id="Seg_6701" s="T128">a-bi</ta>
            <ta e="T130" id="Seg_6702" s="T129">maʔ</ta>
            <ta e="T131" id="Seg_6703" s="T130">ipek</ta>
            <ta e="T132" id="Seg_6704" s="T131">mĭ-bi</ta>
            <ta e="T133" id="Seg_6705" s="T132">uja</ta>
            <ta e="T134" id="Seg_6706" s="T133">mĭ-bi</ta>
            <ta e="T135" id="Seg_6707" s="T134">amno-KAʔ</ta>
            <ta e="T136" id="Seg_6708" s="T135">dön</ta>
            <ta e="T137" id="Seg_6709" s="T136">măn</ta>
            <ta e="T138" id="Seg_6710" s="T137">par-lV-m</ta>
            <ta e="T139" id="Seg_6711" s="T138">tʼili</ta>
            <ta e="T140" id="Seg_6712" s="T139">mo-lV-m</ta>
            <ta e="T141" id="Seg_6713" s="T140">šiʔnʼileʔ</ta>
            <ta e="T142" id="Seg_6714" s="T141">i-lV-m</ta>
            <ta e="T143" id="Seg_6715" s="T142">dĭgəttə</ta>
            <ta e="T144" id="Seg_6716" s="T143">dĭ-zAŋ</ta>
            <ta e="T145" id="Seg_6717" s="T144">amno-laʔbə-bi-jəʔ</ta>
            <ta e="T146" id="Seg_6718" s="T145">amno-laʔbə-bi</ta>
            <ta e="T147" id="Seg_6719" s="T146">nagur</ta>
            <ta e="T148" id="Seg_6720" s="T147">tʼala</ta>
            <ta e="T149" id="Seg_6721" s="T148">onʼiʔ</ta>
            <ta e="T150" id="Seg_6722" s="T149">ine</ta>
            <ta e="T151" id="Seg_6723" s="T150">šonə-gA</ta>
            <ta e="T152" id="Seg_6724" s="T151">kömə</ta>
            <ta e="T153" id="Seg_6725" s="T152">dĭgəttə</ta>
            <ta e="T154" id="Seg_6726" s="T153">naga</ta>
            <ta e="T155" id="Seg_6727" s="T154">dĭn</ta>
            <ta e="T156" id="Seg_6728" s="T155">kuza</ta>
            <ta e="T157" id="Seg_6729" s="T156">dĭgəttə</ta>
            <ta e="T158" id="Seg_6730" s="T157">sagər</ta>
            <ta e="T159" id="Seg_6731" s="T158">ine</ta>
            <ta e="T160" id="Seg_6732" s="T159">šonə-gA</ta>
            <ta e="T161" id="Seg_6733" s="T160">kuza</ta>
            <ta e="T162" id="Seg_6734" s="T161">tože</ta>
            <ta e="T163" id="Seg_6735" s="T162">naga</ta>
            <ta e="T164" id="Seg_6736" s="T163">amno-laʔbə-jəʔ</ta>
            <ta e="T165" id="Seg_6737" s="T164">amno-laʔbə-jəʔ</ta>
            <ta e="T166" id="Seg_6738" s="T165">dĭgəttə</ta>
            <ta e="T167" id="Seg_6739" s="T166">sĭri</ta>
            <ta e="T168" id="Seg_6740" s="T167">ine</ta>
            <ta e="T169" id="Seg_6741" s="T168">šonə-gA</ta>
            <ta e="T170" id="Seg_6742" s="T169">i</ta>
            <ta e="T171" id="Seg_6743" s="T170">kuza</ta>
            <ta e="T172" id="Seg_6744" s="T171">amno-laʔbə</ta>
            <ta e="T173" id="Seg_6745" s="T172">no</ta>
            <ta e="T174" id="Seg_6746" s="T173">kan-žə-bAʔ</ta>
            <ta e="T175" id="Seg_6747" s="T174">măn-ziʔ</ta>
            <ta e="T176" id="Seg_6748" s="T175">maʔ-Tə-l</ta>
            <ta e="T177" id="Seg_6749" s="T176">možet</ta>
            <ta e="T178" id="Seg_6750" s="T177">kan-lV-l</ta>
            <ta e="T179" id="Seg_6751" s="T178">aľi</ta>
            <ta e="T180" id="Seg_6752" s="T179">măna</ta>
            <ta e="T181" id="Seg_6753" s="T180">amno-lV-l</ta>
            <ta e="T182" id="Seg_6754" s="T181">šo-bi-jəʔ</ta>
            <ta e="T183" id="Seg_6755" s="T182">dĭ</ta>
            <ta e="T184" id="Seg_6756" s="T183">kuza-Tə</ta>
            <ta e="T185" id="Seg_6757" s="T184">onʼiʔ</ta>
            <ta e="T186" id="Seg_6758" s="T185">kan-bi</ta>
            <ta e="T187" id="Seg_6759" s="T186">maʔ-gəndə</ta>
            <ta e="T188" id="Seg_6760" s="T187">šo-bi</ta>
            <ta e="T189" id="Seg_6761" s="T188">maʔ-gəndə</ta>
            <ta e="T190" id="Seg_6762" s="T189">naga</ta>
            <ta e="T192" id="Seg_6763" s="T191">il</ta>
            <ta e="T193" id="Seg_6764" s="T192">šində=də</ta>
            <ta e="T194" id="Seg_6765" s="T193">naga</ta>
            <ta e="T195" id="Seg_6766" s="T194">onʼiʔ</ta>
            <ta e="T196" id="Seg_6767" s="T195">onʼiʔ</ta>
            <ta e="T198" id="Seg_6768" s="T197">onʼiʔ</ta>
            <ta e="T199" id="Seg_6769" s="T198">maʔ</ta>
            <ta e="T200" id="Seg_6770" s="T199">nu-gA</ta>
            <ta e="T201" id="Seg_6771" s="T200">dĭ</ta>
            <ta e="T202" id="Seg_6772" s="T201">šo-bi</ta>
            <ta e="T205" id="Seg_6773" s="T204">giraːm-bi</ta>
            <ta e="T206" id="Seg_6774" s="T205">bar</ta>
            <ta e="T207" id="Seg_6775" s="T206">il</ta>
            <ta e="T208" id="Seg_6776" s="T207">da</ta>
            <ta e="T209" id="Seg_6777" s="T208">miʔ</ta>
            <ta e="T210" id="Seg_6778" s="T209">koʔbdo-bAʔ</ta>
            <ta e="T211" id="Seg_6779" s="T210">bar</ta>
            <ta e="T212" id="Seg_6780" s="T211">šide</ta>
            <ta e="T213" id="Seg_6781" s="T212">nʼi</ta>
            <ta e="T214" id="Seg_6782" s="T213">i-bi-jəʔ</ta>
            <ta e="T215" id="Seg_6783" s="T214">dĭ-zAŋ</ta>
            <ta e="T216" id="Seg_6784" s="T215">bü-Tə</ta>
            <ta e="T217" id="Seg_6785" s="T216">öʔ-luʔbdə-bi-jəʔ</ta>
            <ta e="T218" id="Seg_6786" s="T217">a</ta>
            <ta e="T219" id="Seg_6787" s="T218">miʔ</ta>
            <ta e="T220" id="Seg_6788" s="T219">koʔbdo-bAʔ</ta>
            <ta e="T221" id="Seg_6789" s="T220">bar</ta>
            <ta e="T222" id="Seg_6790" s="T221">il-m</ta>
            <ta e="T223" id="Seg_6791" s="T222">dʼagar-luʔbdə-bi</ta>
            <ta e="T224" id="Seg_6792" s="T223">a</ta>
            <ta e="T225" id="Seg_6793" s="T224">gijen</ta>
            <ta e="T226" id="Seg_6794" s="T225">dĭ</ta>
            <ta e="T227" id="Seg_6795" s="T226">gibər=də</ta>
            <ta e="T384" id="Seg_6796" s="T227">kan-lAʔ</ta>
            <ta e="T228" id="Seg_6797" s="T384">tʼür-bi</ta>
            <ta e="T229" id="Seg_6798" s="T228">ej</ta>
            <ta e="T230" id="Seg_6799" s="T229">tĭmne-bAʔ</ta>
            <ta e="T231" id="Seg_6800" s="T230">dĭgəttə</ta>
            <ta e="T232" id="Seg_6801" s="T231">dĭ</ta>
            <ta e="T233" id="Seg_6802" s="T232">ine</ta>
            <ta e="T234" id="Seg_6803" s="T233">i-bi</ta>
            <ta e="T235" id="Seg_6804" s="T234">kan-bi</ta>
            <ta e="T236" id="Seg_6805" s="T235">ku-zittə</ta>
            <ta e="T239" id="Seg_6806" s="T238">ku-laːm-bi</ta>
            <ta e="T240" id="Seg_6807" s="T239">ku-bi</ta>
            <ta e="T241" id="Seg_6808" s="T240">ku-bi-jəʔ</ta>
            <ta e="T242" id="Seg_6809" s="T241">dĭn</ta>
            <ta e="T243" id="Seg_6810" s="T242">dĭgəttə</ta>
            <ta e="T244" id="Seg_6811" s="T243">davaj</ta>
            <ta e="T245" id="Seg_6812" s="T244">dĭ-ziʔ</ta>
            <ta e="T246" id="Seg_6813" s="T245">bar</ta>
            <ta e="T247" id="Seg_6814" s="T246">tʼabəro-zittə</ta>
            <ta e="T248" id="Seg_6815" s="T247">dĭ</ta>
            <ta e="T249" id="Seg_6816" s="T248">xatʼel</ta>
            <ta e="T250" id="Seg_6817" s="T249">dĭ-m</ta>
            <ta e="T252" id="Seg_6818" s="T251">băt-zittə</ta>
            <ta e="T253" id="Seg_6819" s="T252">tagaj-ziʔ</ta>
            <ta e="T254" id="Seg_6820" s="T253">dĭ</ta>
            <ta e="T255" id="Seg_6821" s="T254">bar</ta>
            <ta e="T256" id="Seg_6822" s="T255">măn-ntə</ta>
            <ta e="T257" id="Seg_6823" s="T256">e-ʔ</ta>
            <ta e="T258" id="Seg_6824" s="T257">băt-ə-ʔ</ta>
            <ta e="T259" id="Seg_6825" s="T258">măn</ta>
            <ta e="T260" id="Seg_6826" s="T259">tăn</ta>
            <ta e="T261" id="Seg_6827" s="T260">kaga-m</ta>
            <ta e="T262" id="Seg_6828" s="T261">i-gA-m</ta>
            <ta e="T263" id="Seg_6829" s="T262">kuriza-jəʔ</ta>
            <ta e="T264" id="Seg_6830" s="T263">tʼăbaktər-laʔbə-jəʔ</ta>
            <ta e="T265" id="Seg_6831" s="T264">kapaluxa</ta>
            <ta e="T266" id="Seg_6832" s="T265">i</ta>
            <ta e="T269" id="Seg_6833" s="T267">kuropatka</ta>
            <ta e="T270" id="Seg_6834" s="T269">kuropatka</ta>
            <ta e="T271" id="Seg_6835" s="T270">măn-liA</ta>
            <ta e="T272" id="Seg_6836" s="T271">kapaluxa-Tə</ta>
            <ta e="T273" id="Seg_6837" s="T272">tăn</ta>
            <ta e="T274" id="Seg_6838" s="T273">ej</ta>
            <ta e="T275" id="Seg_6839" s="T274">kuvas</ta>
            <ta e="T276" id="Seg_6840" s="T275">girgit</ta>
            <ta e="T277" id="Seg_6841" s="T276">piʔme-jəʔ</ta>
            <ta e="T278" id="Seg_6842" s="T277">ej</ta>
            <ta e="T279" id="Seg_6843" s="T278">jakšə</ta>
            <ta e="T280" id="Seg_6844" s="T279">a</ta>
            <ta e="T281" id="Seg_6845" s="T280">măn</ta>
            <ta e="T282" id="Seg_6846" s="T281">kuvas</ta>
            <ta e="T283" id="Seg_6847" s="T282">i-gA-m</ta>
            <ta e="T284" id="Seg_6848" s="T883">ine-gəʔ</ta>
            <ta e="T285" id="Seg_6849" s="T284">eʔbdə</ta>
            <ta e="T286" id="Seg_6850" s="T285">bar</ta>
            <ta e="T287" id="Seg_6851" s="T286">săj-nʼeʔbdə-luʔbdə-bi-m</ta>
            <ta e="T288" id="Seg_6852" s="T287">i</ta>
            <ta e="T289" id="Seg_6853" s="T288">bos-gəndə</ta>
            <ta e="T290" id="Seg_6854" s="T289">hen-bi-m</ta>
            <ta e="T292" id="Seg_6855" s="T290">ulu-Tə</ta>
            <ta e="T293" id="Seg_6856" s="T292">ugaːndə</ta>
            <ta e="T294" id="Seg_6857" s="T293">kuvas</ta>
            <ta e="T295" id="Seg_6858" s="T294">i-gA-m</ta>
            <ta e="T296" id="Seg_6859" s="T295">tüj</ta>
            <ta e="T298" id="Seg_6860" s="T297">măja-Kən</ta>
            <ta e="T299" id="Seg_6861" s="T298">bar</ta>
            <ta e="T300" id="Seg_6862" s="T299">aspaʔ</ta>
            <ta e="T301" id="Seg_6863" s="T300">mĭnzəl-laʔbə</ta>
            <ta e="T302" id="Seg_6864" s="T301">ĭmbi</ta>
            <ta e="T303" id="Seg_6865" s="T302">dĭ</ta>
            <ta e="T304" id="Seg_6866" s="T303">dĭrgit</ta>
            <ta e="T305" id="Seg_6867" s="T304">muraše-jəʔ</ta>
            <ta e="T308" id="Seg_6868" s="T306">măja-Kən</ta>
            <ta e="T310" id="Seg_6869" s="T309">măja-Kən</ta>
            <ta e="T311" id="Seg_6870" s="T310">süjö</ta>
            <ta e="T312" id="Seg_6871" s="T311">edö-laʔbə</ta>
            <ta e="T313" id="Seg_6872" s="T312">ĭmbi</ta>
            <ta e="T314" id="Seg_6873" s="T313">dĭrgit</ta>
            <ta e="T315" id="Seg_6874" s="T314">nüjü</ta>
            <ta e="T317" id="Seg_6875" s="T316">bos-də</ta>
            <ta e="T319" id="Seg_6876" s="T318">tʼo-Kən</ta>
            <ta e="T320" id="Seg_6877" s="T319">amno-laʔbə</ta>
            <ta e="T321" id="Seg_6878" s="T320">a</ta>
            <ta e="T322" id="Seg_6879" s="T321">ku-t</ta>
            <ta e="T323" id="Seg_6880" s="T322">baška</ta>
            <ta e="T324" id="Seg_6881" s="T323">tʼo-nən</ta>
            <ta e="T325" id="Seg_6882" s="T324">amno-laʔbə</ta>
            <ta e="T326" id="Seg_6883" s="T325">serʼožka-jəʔ-ziʔ</ta>
            <ta e="T330" id="Seg_6884" s="T329">šide</ta>
            <ta e="T332" id="Seg_6885" s="T331">ne-zAŋ</ta>
            <ta e="T333" id="Seg_6886" s="T332">iʔbö-laʔbə-jəʔ</ta>
            <ta e="T335" id="Seg_6887" s="T334">iʔbö-laʔbə-jəʔ</ta>
            <ta e="T338" id="Seg_6888" s="T337">šü-Tə</ta>
            <ta e="T339" id="Seg_6889" s="T338">toʔ-gəndə</ta>
            <ta e="T340" id="Seg_6890" s="T339">nanəʔzəbi-jəʔ</ta>
            <ta e="T341" id="Seg_6891" s="T340">dĭ</ta>
            <ta e="T342" id="Seg_6892" s="T341">üjü-zAŋ-də</ta>
            <ta e="T343" id="Seg_6893" s="T342">lʼažka-jəʔ</ta>
            <ta e="T345" id="Seg_6894" s="T344">üjü-t</ta>
            <ta e="T346" id="Seg_6895" s="T345">naga</ta>
            <ta e="T347" id="Seg_6896" s="T346">i</ta>
            <ta e="T348" id="Seg_6897" s="T347">uda-t</ta>
            <ta e="T349" id="Seg_6898" s="T348">naga</ta>
            <ta e="T350" id="Seg_6899" s="T349">a</ta>
            <ta e="T352" id="Seg_6900" s="T350">mĭn-gA</ta>
            <ta e="T354" id="Seg_6901" s="T353">măja-Tə</ta>
            <ta e="T355" id="Seg_6902" s="T354">kandə-gA</ta>
            <ta e="T356" id="Seg_6903" s="T355">dĭ</ta>
            <ta e="T357" id="Seg_6904" s="T356">kuja</ta>
            <ta e="T359" id="Seg_6905" s="T358">iʔgö</ta>
            <ta e="T360" id="Seg_6906" s="T359">süjö-jəʔ</ta>
            <ta e="T361" id="Seg_6907" s="T360">ulu-zAŋ-də</ta>
            <ta e="T362" id="Seg_6908" s="T361">bar</ta>
            <ta e="T363" id="Seg_6909" s="T362">onʼiʔ</ta>
            <ta e="T364" id="Seg_6910" s="T363">onʼiʔ-Tə</ta>
            <ta e="T366" id="Seg_6911" s="T365">a</ta>
            <ta e="T368" id="Seg_6912" s="T367">köten-zAŋ-də</ta>
            <ta e="T369" id="Seg_6913" s="T368">bar</ta>
            <ta e="T370" id="Seg_6914" s="T369">kuŋgə-ŋ</ta>
            <ta e="T371" id="Seg_6915" s="T370">onʼiʔ</ta>
            <ta e="T372" id="Seg_6916" s="T371">onʼiʔ-Tə</ta>
            <ta e="T373" id="Seg_6917" s="T372">dĭ</ta>
            <ta e="T374" id="Seg_6918" s="T373">maʔ</ta>
            <ta e="T376" id="Seg_6919" s="T375">tura-Kən</ta>
            <ta e="T377" id="Seg_6920" s="T376">iʔbö-laʔbə</ta>
            <ta e="T379" id="Seg_6921" s="T378">dʼüʔpi</ta>
            <ta e="T380" id="Seg_6922" s="T379">büzəj</ta>
            <ta e="T381" id="Seg_6923" s="T380">a</ta>
            <ta e="T382" id="Seg_6924" s="T381">nörbə-zittə</ta>
            <ta e="T383" id="Seg_6925" s="T382">dĭ</ta>
            <ta e="T385" id="Seg_6926" s="T885">šĭkə-t</ta>
            <ta e="T388" id="Seg_6927" s="T387">sĭri</ta>
            <ta e="T390" id="Seg_6928" s="T389">pa-jəʔ-Tə</ta>
            <ta e="T391" id="Seg_6929" s="T390">tarara</ta>
            <ta e="T392" id="Seg_6930" s="T391">amno-laʔbə</ta>
            <ta e="T394" id="Seg_6931" s="T393">dĭ</ta>
            <ta e="T395" id="Seg_6932" s="T394">šĭkə</ta>
            <ta e="T397" id="Seg_6933" s="T396">üjü-t</ta>
            <ta e="T399" id="Seg_6934" s="T398">uda-t</ta>
            <ta e="T400" id="Seg_6935" s="T399">naga</ta>
            <ta e="T401" id="Seg_6936" s="T400">a</ta>
            <ta e="T402" id="Seg_6937" s="T401">kudaj-Tə</ta>
            <ta e="T403" id="Seg_6938" s="T402">üzə-liA</ta>
            <ta e="T404" id="Seg_6939" s="T403">dĭ</ta>
            <ta e="T407" id="Seg_6940" s="T406">üjü-t</ta>
            <ta e="T408" id="Seg_6941" s="T407">uda-t</ta>
            <ta e="T409" id="Seg_6942" s="T408">naga</ta>
            <ta e="T410" id="Seg_6943" s="T409">a</ta>
            <ta e="T411" id="Seg_6944" s="T410">pa-jəʔ-Tə</ta>
            <ta e="T412" id="Seg_6945" s="T411">mĭn-liA</ta>
            <ta e="T415" id="Seg_6946" s="T414">a</ta>
            <ta e="T416" id="Seg_6947" s="T415">maʔ-gəndə</ta>
            <ta e="T417" id="Seg_6948" s="T416">šo-lV-j</ta>
            <ta e="T418" id="Seg_6949" s="T417">iʔbö-laʔbə</ta>
            <ta e="T419" id="Seg_6950" s="T418">dĭ</ta>
            <ta e="T420" id="Seg_6951" s="T419">baltu</ta>
            <ta e="T422" id="Seg_6952" s="T421">uda-t</ta>
            <ta e="T423" id="Seg_6953" s="T422">üjü-t</ta>
            <ta e="T424" id="Seg_6954" s="T423">naga</ta>
            <ta e="T425" id="Seg_6955" s="T424">a</ta>
            <ta e="T426" id="Seg_6956" s="T425">ajə-m</ta>
            <ta e="T427" id="Seg_6957" s="T426">bar</ta>
            <ta e="T428" id="Seg_6958" s="T427">kar-laʔbə</ta>
            <ta e="T429" id="Seg_6959" s="T428">dĭ</ta>
            <ta e="T430" id="Seg_6960" s="T429">beržə</ta>
            <ta e="T432" id="Seg_6961" s="T431">üdʼüge</ta>
            <ta e="T434" id="Seg_6962" s="T433">men</ta>
            <ta e="T435" id="Seg_6963" s="T434">maʔ-gəndə</ta>
            <ta e="T436" id="Seg_6964" s="T435">toʔ-gəndə</ta>
            <ta e="T437" id="Seg_6965" s="T436">amno-laʔbə-jəʔ</ta>
            <ta e="T438" id="Seg_6966" s="T437">ej</ta>
            <ta e="T439" id="Seg_6967" s="T438">kürüm-liA</ta>
            <ta e="T440" id="Seg_6968" s="T439">i</ta>
            <ta e="T442" id="Seg_6969" s="T441">šində-m=də</ta>
            <ta e="T443" id="Seg_6970" s="T442">maʔ-Tə</ta>
            <ta e="T445" id="Seg_6971" s="T443">ej</ta>
            <ta e="T447" id="Seg_6972" s="T446">šində-Tə=də</ta>
            <ta e="T448" id="Seg_6973" s="T447">maʔ-gəndə</ta>
            <ta e="T449" id="Seg_6974" s="T448">ej</ta>
            <ta e="T450" id="Seg_6975" s="T449">öʔ-lAʔ</ta>
            <ta e="T451" id="Seg_6976" s="T450">dĭ</ta>
            <ta e="T454" id="Seg_6977" s="T453">Krasnojarskə-jəʔ</ta>
            <ta e="T455" id="Seg_6978" s="T454">tura-jəʔ</ta>
            <ta e="T456" id="Seg_6979" s="T455">hʼaʔ-laʔbə-jəʔ</ta>
            <ta e="T457" id="Seg_6980" s="T456">a</ta>
            <ta e="T458" id="Seg_6981" s="T457">döbər</ta>
            <ta e="T459" id="Seg_6982" s="T458">pa-jəʔ</ta>
            <ta e="T460" id="Seg_6983" s="T459">nʼergö-laʔbə-jəʔ</ta>
            <ta e="T461" id="Seg_6984" s="T460">dĭ</ta>
            <ta e="T462" id="Seg_6985" s="T461">sazən</ta>
            <ta e="T463" id="Seg_6986" s="T462">kandə-gA</ta>
            <ta e="T464" id="Seg_6987" s="T463">dĭʔə</ta>
            <ta e="T467" id="Seg_6988" s="T466">šĭkə-t</ta>
            <ta e="T468" id="Seg_6989" s="T467">naga</ta>
            <ta e="T469" id="Seg_6990" s="T468">sima-t</ta>
            <ta e="T470" id="Seg_6991" s="T469">naga</ta>
            <ta e="T471" id="Seg_6992" s="T470">aŋ-də</ta>
            <ta e="T472" id="Seg_6993" s="T471">naga</ta>
            <ta e="T473" id="Seg_6994" s="T472">a</ta>
            <ta e="T474" id="Seg_6995" s="T473">măn-liA</ta>
            <ta e="T475" id="Seg_6996" s="T474">kumən</ta>
            <ta e="T476" id="Seg_6997" s="T475">vremja</ta>
            <ta e="T477" id="Seg_6998" s="T476">kan-bi</ta>
            <ta e="T478" id="Seg_6999" s="T477">dĭ</ta>
            <ta e="T479" id="Seg_7000" s="T478">časɨ-jəʔ</ta>
            <ta e="T481" id="Seg_7001" s="T480">iʔbö-laʔbə</ta>
            <ta e="T482" id="Seg_7002" s="T481">tak</ta>
            <ta e="T483" id="Seg_7003" s="T482">üdʼüge</ta>
            <ta e="T484" id="Seg_7004" s="T483">tospak-Tə</ta>
            <ta e="T485" id="Seg_7005" s="T484">a</ta>
            <ta e="T486" id="Seg_7006" s="T485">nu-laʔbə</ta>
            <ta e="T487" id="Seg_7007" s="T486">tak</ta>
            <ta e="T488" id="Seg_7008" s="T487">ugaːndə</ta>
            <ta e="T489" id="Seg_7009" s="T488">urgo</ta>
            <ta e="T490" id="Seg_7010" s="T489">urgo</ta>
            <ta e="T491" id="Seg_7011" s="T490">ine-t-ziʔ</ta>
            <ta e="T493" id="Seg_7012" s="T492">duga</ta>
            <ta e="T495" id="Seg_7013" s="T494">amno-laʔbə</ta>
            <ta e="T496" id="Seg_7014" s="T495">koʔbdo</ta>
            <ta e="T497" id="Seg_7015" s="T496">bar</ta>
            <ta e="T498" id="Seg_7016" s="T497">ospa-jəʔ-ziʔ</ta>
            <ta e="T499" id="Seg_7017" s="T498">dĭ</ta>
            <ta e="T500" id="Seg_7018" s="T499">a</ta>
            <ta e="T501" id="Seg_7019" s="T500">dĭ</ta>
            <ta e="T502" id="Seg_7020" s="T501">ĭntak</ta>
            <ta e="T504" id="Seg_7021" s="T503">oʔbdə-bi-m</ta>
            <ta e="T505" id="Seg_7022" s="T504">oʔbdə-bi-m</ta>
            <ta e="T506" id="Seg_7023" s="T505">nʼitka-jəʔ</ta>
            <ta e="T507" id="Seg_7024" s="T506">ej</ta>
            <ta e="T509" id="Seg_7025" s="T508">ej</ta>
            <ta e="T510" id="Seg_7026" s="T509">mo-bi-m</ta>
            <ta e="T511" id="Seg_7027" s="T510">oʔbdə-zittə</ta>
            <ta e="T512" id="Seg_7028" s="T511">dĭ</ta>
            <ta e="T513" id="Seg_7029" s="T512">aʔtʼi</ta>
            <ta e="T515" id="Seg_7030" s="T514">amno-laʔbə</ta>
            <ta e="T516" id="Seg_7031" s="T515">büzʼe</ta>
            <ta e="T517" id="Seg_7032" s="T516">grʼadka-Kən</ta>
            <ta e="T518" id="Seg_7033" s="T517">bar</ta>
            <ta e="T519" id="Seg_7034" s="T518">iʔgö</ta>
            <ta e="T520" id="Seg_7035" s="T519">zaplatka-jəʔ</ta>
            <ta e="T521" id="Seg_7036" s="T520">šində</ta>
            <ta e="T523" id="Seg_7037" s="T522">ku-laʔbə</ta>
            <ta e="T524" id="Seg_7038" s="T523">bar</ta>
            <ta e="T525" id="Seg_7039" s="T524">tʼor-laʔbə</ta>
            <ta e="T526" id="Seg_7040" s="T525">dĭ</ta>
            <ta e="T527" id="Seg_7041" s="T526">köbergən</ta>
            <ta e="T529" id="Seg_7042" s="T528">amno-laʔbə</ta>
            <ta e="T530" id="Seg_7043" s="T529">iʔgö</ta>
            <ta e="T532" id="Seg_7044" s="T530">oldʼa</ta>
            <ta e="T535" id="Seg_7045" s="T534">sejʔpü</ta>
            <ta e="T536" id="Seg_7046" s="T535">aktʼa</ta>
            <ta e="T540" id="Seg_7047" s="T539">biəʔ</ta>
            <ta e="T542" id="Seg_7048" s="T541">sejʔpü</ta>
            <ta e="T543" id="Seg_7049" s="T542">oldʼa</ta>
            <ta e="T544" id="Seg_7050" s="T543">a</ta>
            <ta e="T545" id="Seg_7051" s="T544">kaj-zittə</ta>
            <ta e="T546" id="Seg_7052" s="T545">naga</ta>
            <ta e="T547" id="Seg_7053" s="T546">dĭ</ta>
            <ta e="T548" id="Seg_7054" s="T547">kăpusta</ta>
            <ta e="T550" id="Seg_7055" s="T549">amno-laʔbə</ta>
            <ta e="T551" id="Seg_7056" s="T550">koʔbdo</ta>
            <ta e="T552" id="Seg_7057" s="T551">tʼo-Kən</ta>
            <ta e="T553" id="Seg_7058" s="T552">bos-də</ta>
            <ta e="T554" id="Seg_7059" s="T553">kömə</ta>
            <ta e="T555" id="Seg_7060" s="T554">a</ta>
            <ta e="T556" id="Seg_7061" s="T555">eʔbdə-t</ta>
            <ta e="T557" id="Seg_7062" s="T556">nʼiʔnen</ta>
            <ta e="T558" id="Seg_7063" s="T557">dĭ</ta>
            <ta e="T561" id="Seg_7064" s="T560">măn</ta>
            <ta e="T562" id="Seg_7065" s="T561">ku-bi-m</ta>
            <ta e="T563" id="Seg_7066" s="T562">plʼotka</ta>
            <ta e="T564" id="Seg_7067" s="T563">a</ta>
            <ta e="T565" id="Seg_7068" s="T564">kan-zittə</ta>
            <ta e="T566" id="Seg_7069" s="T565">ej</ta>
            <ta e="T567" id="Seg_7070" s="T566">mo-bi-m</ta>
            <ta e="T568" id="Seg_7071" s="T567">a</ta>
            <ta e="T569" id="Seg_7072" s="T568">dĭ</ta>
            <ta e="T570" id="Seg_7073" s="T569">ĭmbi</ta>
            <ta e="T571" id="Seg_7074" s="T570">penzi</ta>
            <ta e="T573" id="Seg_7075" s="T572">šonə-gA</ta>
            <ta e="T574" id="Seg_7076" s="T573">koʔbdo</ta>
            <ta e="T575" id="Seg_7077" s="T574">i</ta>
            <ta e="T576" id="Seg_7078" s="T575">dĭn</ta>
            <ta e="T577" id="Seg_7079" s="T576">eʔbdə-jəʔ</ta>
            <ta e="T578" id="Seg_7080" s="T577">bar</ta>
            <ta e="T580" id="Seg_7081" s="T579">kurgo-NTA-jəʔ</ta>
            <ta e="T581" id="Seg_7082" s="T580">bar</ta>
            <ta e="T583" id="Seg_7083" s="T582">dĭ</ta>
            <ta e="T584" id="Seg_7084" s="T583">soroka</ta>
            <ta e="T585" id="Seg_7085" s="T584">i-gA</ta>
            <ta e="T587" id="Seg_7086" s="T586">šide</ta>
            <ta e="T588" id="Seg_7087" s="T587">nu-gA-jəʔ</ta>
            <ta e="T589" id="Seg_7088" s="T588">šide</ta>
            <ta e="T590" id="Seg_7089" s="T589">iʔbö-laʔbə-jəʔ</ta>
            <ta e="T591" id="Seg_7090" s="T590">onʼiʔ</ta>
            <ta e="T592" id="Seg_7091" s="T591">mĭn-laʔbə</ta>
            <ta e="T593" id="Seg_7092" s="T592">sumna</ta>
            <ta e="T594" id="Seg_7093" s="T593">mĭn-laʔbə</ta>
            <ta e="T595" id="Seg_7094" s="T594">a</ta>
            <ta e="T596" id="Seg_7095" s="T595">sejʔpü</ta>
            <ta e="T598" id="Seg_7096" s="T597">kaj-laʔbə</ta>
            <ta e="T599" id="Seg_7097" s="T598">dĭ</ta>
            <ta e="T600" id="Seg_7098" s="T599">ajə</ta>
            <ta e="T601" id="Seg_7099" s="T600">i-gA</ta>
            <ta e="T604" id="Seg_7100" s="T603">ular</ta>
            <ta e="T605" id="Seg_7101" s="T604">iʔgö</ta>
            <ta e="T606" id="Seg_7102" s="T605">mĭn-laʔbə-jəʔ</ta>
            <ta e="T607" id="Seg_7103" s="T606">a</ta>
            <ta e="T608" id="Seg_7104" s="T607">pastux</ta>
            <ta e="T611" id="Seg_7105" s="T609">amnu-t</ta>
            <ta e="T612" id="Seg_7106" s="T611">dĭ</ta>
            <ta e="T613" id="Seg_7107" s="T612">mesʼats</ta>
            <ta e="T614" id="Seg_7108" s="T613">i</ta>
            <ta e="T615" id="Seg_7109" s="T614">zvʼozda-jəʔ</ta>
            <ta e="T617" id="Seg_7110" s="T616">šide</ta>
            <ta e="T618" id="Seg_7111" s="T617">ši</ta>
            <ta e="T619" id="Seg_7112" s="T618">i</ta>
            <ta e="T620" id="Seg_7113" s="T619">šide</ta>
            <ta e="T621" id="Seg_7114" s="T620">puʔmə-jəʔ</ta>
            <ta e="T622" id="Seg_7115" s="T621">a</ta>
            <ta e="T623" id="Seg_7116" s="T622">šüjə-Kən</ta>
            <ta e="T624" id="Seg_7117" s="T623">marka</ta>
            <ta e="T625" id="Seg_7118" s="T624">kaptə-jəʔ</ta>
            <ta e="T627" id="Seg_7119" s="T626">măn</ta>
            <ta e="T628" id="Seg_7120" s="T627">üge</ta>
            <ta e="T629" id="Seg_7121" s="T628">am-laʔbə-m</ta>
            <ta e="T630" id="Seg_7122" s="T629">am-laʔbə-m</ta>
            <ta e="T631" id="Seg_7123" s="T630">a</ta>
            <ta e="T632" id="Seg_7124" s="T631">tüʔ-zittə</ta>
            <ta e="T633" id="Seg_7125" s="T632">ej</ta>
            <ta e="T634" id="Seg_7126" s="T633">mo-liA-m</ta>
            <ta e="T635" id="Seg_7127" s="T634">băra</ta>
            <ta e="T637" id="Seg_7128" s="T636">pʼetux</ta>
            <ta e="T638" id="Seg_7129" s="T637">amno-laʔbə</ta>
            <ta e="T639" id="Seg_7130" s="T638">zabor-Kən</ta>
            <ta e="T640" id="Seg_7131" s="T639">xvostə</ta>
            <ta e="T641" id="Seg_7132" s="T640">tʼo-Kən</ta>
            <ta e="T642" id="Seg_7133" s="T641">a</ta>
            <ta e="T643" id="Seg_7134" s="T642">kirgaːr-laʔbə</ta>
            <ta e="T644" id="Seg_7135" s="T643">kudaj-Tə</ta>
            <ta e="T645" id="Seg_7136" s="T644">dĭ</ta>
            <ta e="T646" id="Seg_7137" s="T645">koŋgoro</ta>
            <ta e="T648" id="Seg_7138" s="T647">nu-gA</ta>
            <ta e="T649" id="Seg_7139" s="T648">nʼi</ta>
            <ta e="T650" id="Seg_7140" s="T649">šüjə-Kən</ta>
            <ta e="T652" id="Seg_7141" s="T651">neni-laʔbə</ta>
            <ta e="T653" id="Seg_7142" s="T652">a</ta>
            <ta e="T654" id="Seg_7143" s="T653">bos-də</ta>
            <ta e="T655" id="Seg_7144" s="T654">bar</ta>
            <ta e="T656" id="Seg_7145" s="T655">kĭnzə-laʔbə</ta>
            <ta e="T657" id="Seg_7146" s="T656">dĭ</ta>
            <ta e="T658" id="Seg_7147" s="T657">kujdərgan</ta>
            <ta e="T660" id="Seg_7148" s="T659">šide-göʔ</ta>
            <ta e="T661" id="Seg_7149" s="T660">kan-ntə-gA-jəʔ</ta>
            <ta e="T662" id="Seg_7150" s="T661">onʼiʔ</ta>
            <ta e="T663" id="Seg_7151" s="T662">kandə-gA</ta>
            <ta e="T664" id="Seg_7152" s="T663">onʼiʔ</ta>
            <ta e="T665" id="Seg_7153" s="T664">bĭdə-liA</ta>
            <ta e="T666" id="Seg_7154" s="T665">dĭ</ta>
            <ta e="T667" id="Seg_7155" s="T666">üjü-jəʔ</ta>
            <ta e="T669" id="Seg_7156" s="T668">toʔbdə-nar-ə-ʔ</ta>
            <ta e="T670" id="Seg_7157" s="T669">măna</ta>
            <ta e="T671" id="Seg_7158" s="T670">münör-ə-ʔ</ta>
            <ta e="T672" id="Seg_7159" s="T671">măna</ta>
            <ta e="T673" id="Seg_7160" s="T672">sʼa-ʔ</ta>
            <ta e="T674" id="Seg_7161" s="T673">măna</ta>
            <ta e="T675" id="Seg_7162" s="T674">i-gA</ta>
            <ta e="T676" id="Seg_7163" s="T675">măna</ta>
            <ta e="T677" id="Seg_7164" s="T676">dĭ</ta>
            <ta e="T678" id="Seg_7165" s="T677">sanə</ta>
            <ta e="T680" id="Seg_7166" s="T679">edə-laʔbə</ta>
            <ta e="T681" id="Seg_7167" s="T680">bar</ta>
            <ta e="T682" id="Seg_7168" s="T681">a</ta>
            <ta e="T683" id="Seg_7169" s="T682">il</ta>
            <ta e="T684" id="Seg_7170" s="T683">bar</ta>
            <ta e="T685" id="Seg_7171" s="T684">kabar-laʔbə-jəʔ</ta>
            <ta e="T686" id="Seg_7172" s="T685">bazə-jd-laʔbə-jəʔ</ta>
            <ta e="T687" id="Seg_7173" s="T686">kĭškə-laʔbə-jəʔ</ta>
            <ta e="T688" id="Seg_7174" s="T687">dĭ</ta>
            <ta e="T689" id="Seg_7175" s="T688">rušnʼik</ta>
            <ta e="T690" id="Seg_7176" s="T689">i-gA</ta>
            <ta e="T692" id="Seg_7177" s="T888">pa-jəʔ</ta>
            <ta e="T693" id="Seg_7178" s="T692">noʔ</ta>
            <ta e="T694" id="Seg_7179" s="T693">özer-laʔbə</ta>
            <ta e="T697" id="Seg_7180" s="T696">dĭ</ta>
            <ta e="T698" id="Seg_7181" s="T697">müjə</ta>
            <ta e="T700" id="Seg_7182" s="T699">kömə</ta>
            <ta e="T701" id="Seg_7183" s="T700">poʔto</ta>
            <ta e="T702" id="Seg_7184" s="T701">iʔbö-laʔbə-bi</ta>
            <ta e="T703" id="Seg_7185" s="T702">dĭn</ta>
            <ta e="T704" id="Seg_7186" s="T703">noʔ</ta>
            <ta e="T705" id="Seg_7187" s="T704">ej</ta>
            <ta e="T706" id="Seg_7188" s="T705">özer-laʔbə</ta>
            <ta e="T707" id="Seg_7189" s="T706">šü</ta>
            <ta e="T708" id="Seg_7190" s="T707">dĭ</ta>
            <ta e="T710" id="Seg_7191" s="T709">kan-bi</ta>
            <ta e="T711" id="Seg_7192" s="T710">keʔbde-j-lAʔ</ta>
            <ta e="T712" id="Seg_7193" s="T711">i</ta>
            <ta e="T713" id="Seg_7194" s="T712">măn</ta>
            <ta e="T714" id="Seg_7195" s="T713">kan-bi-m</ta>
            <ta e="T715" id="Seg_7196" s="T714">keʔbde</ta>
            <ta e="T716" id="Seg_7197" s="T715">nĭŋgə-zittə</ta>
            <ta e="T717" id="Seg_7198" s="T716">i</ta>
            <ta e="T718" id="Seg_7199" s="T717">ĭššo</ta>
            <ta e="T719" id="Seg_7200" s="T718">onʼiʔ</ta>
            <ta e="T720" id="Seg_7201" s="T719">koʔbdo</ta>
            <ta e="T721" id="Seg_7202" s="T720">kan-bi</ta>
            <ta e="T724" id="Seg_7203" s="T723">ine-ziʔ</ta>
            <ta e="T727" id="Seg_7204" s="T726">kan-bi-bAʔ</ta>
            <ta e="T728" id="Seg_7205" s="T727">šumiga-Tə</ta>
            <ta e="T729" id="Seg_7206" s="T728">dĭbər</ta>
            <ta e="T730" id="Seg_7207" s="T729">šo-bi-bAʔ</ta>
            <ta e="T731" id="Seg_7208" s="T730">šaʔ-lV-jəʔ</ta>
            <ta e="T732" id="Seg_7209" s="T731">keʔbde</ta>
            <ta e="T733" id="Seg_7210" s="T732">măndo-r-bi-bAʔ</ta>
            <ta e="T734" id="Seg_7211" s="T733">i</ta>
            <ta e="T735" id="Seg_7212" s="T734">idʼiʔeʔe</ta>
            <ta e="T736" id="Seg_7213" s="T735">oʔbdə-bi-bAʔ</ta>
            <ta e="T737" id="Seg_7214" s="T736">dĭgəttə</ta>
            <ta e="T738" id="Seg_7215" s="T737">šo-bi-bAʔ</ta>
            <ta e="T739" id="Seg_7216" s="T738">gijen</ta>
            <ta e="T740" id="Seg_7217" s="T739">ine</ta>
            <ta e="T741" id="Seg_7218" s="T740">mĭn-laʔbə</ta>
            <ta e="T742" id="Seg_7219" s="T741">ija-m</ta>
            <ta e="T743" id="Seg_7220" s="T742">măn-ntə</ta>
            <ta e="T744" id="Seg_7221" s="T743">ugaːndə</ta>
            <ta e="T745" id="Seg_7222" s="T744">sagər</ta>
            <ta e="T747" id="Seg_7223" s="T746">šonə-gA</ta>
            <ta e="T748" id="Seg_7224" s="T747">nadə</ta>
            <ta e="T749" id="Seg_7225" s="T748">a-zittə</ta>
            <ta e="T750" id="Seg_7226" s="T749">maʔ</ta>
            <ta e="T751" id="Seg_7227" s="T750">ato</ta>
            <ta e="T752" id="Seg_7228" s="T751">kunol-zittə</ta>
            <ta e="T753" id="Seg_7229" s="T752">ej</ta>
            <ta e="T754" id="Seg_7230" s="T753">jakšə</ta>
            <ta e="T755" id="Seg_7231" s="T754">mo-lV-j</ta>
            <ta e="T756" id="Seg_7232" s="T755">dĭgəttə</ta>
            <ta e="T757" id="Seg_7233" s="T756">maʔ</ta>
            <ta e="T758" id="Seg_7234" s="T757">a-bi-bAʔ</ta>
            <ta e="T759" id="Seg_7235" s="T758">mĭnzər-bi-bAʔ</ta>
            <ta e="T760" id="Seg_7236" s="T759">segi</ta>
            <ta e="T761" id="Seg_7237" s="T760">bü</ta>
            <ta e="T762" id="Seg_7238" s="T761">bĭs-bi-bAʔ</ta>
            <ta e="T763" id="Seg_7239" s="T762">a</ta>
            <ta e="T764" id="Seg_7240" s="T763">dĭn</ta>
            <ta e="T766" id="Seg_7241" s="T765">i-bi</ta>
            <ta e="T767" id="Seg_7242" s="T766">pălăvik</ta>
            <ta e="T768" id="Seg_7243" s="T767">dĭbər</ta>
            <ta e="T769" id="Seg_7244" s="T768">pălăvik-də</ta>
            <ta e="T770" id="Seg_7245" s="T769">edəbibiʔibeʔ</ta>
            <ta e="T771" id="Seg_7246" s="T770">pa-jəʔ-Tə</ta>
            <ta e="T772" id="Seg_7247" s="T771">dĭgəttə</ta>
            <ta e="T773" id="Seg_7248" s="T772">iʔbə-bAʔ</ta>
            <ta e="T774" id="Seg_7249" s="T773">kunol-zittə</ta>
            <ta e="T775" id="Seg_7250" s="T774">a</ta>
            <ta e="T776" id="Seg_7251" s="T775">nüdʼi-n</ta>
            <ta e="T777" id="Seg_7252" s="T776">bar</ta>
            <ta e="T778" id="Seg_7253" s="T777">ugaːndə</ta>
            <ta e="T779" id="Seg_7254" s="T778">tăŋ</ta>
            <ta e="T780" id="Seg_7255" s="T779">surno</ta>
            <ta e="T781" id="Seg_7256" s="T780">šonə-bi</ta>
            <ta e="T782" id="Seg_7257" s="T781">šonə-bi</ta>
            <ta e="T783" id="Seg_7258" s="T782">ertə-n</ta>
            <ta e="T784" id="Seg_7259" s="T783">uʔbdə-bi-bAʔ</ta>
            <ta e="T785" id="Seg_7260" s="T784">bar</ta>
            <ta e="T786" id="Seg_7261" s="T785">nünör-bi-bAʔ</ta>
            <ta e="T787" id="Seg_7262" s="T786">ine-jəʔ</ta>
            <ta e="T788" id="Seg_7263" s="T787">dʼabə-bi-bAʔ</ta>
            <ta e="T789" id="Seg_7264" s="T788">kan-bi-bAʔ</ta>
            <ta e="T790" id="Seg_7265" s="T789">maʔ-gənʼi</ta>
            <ta e="T792" id="Seg_7266" s="T791">šo-bi-bAʔ</ta>
            <ta e="T793" id="Seg_7267" s="T792">bü-Tə</ta>
            <ta e="T794" id="Seg_7268" s="T793">kădaʔ=də</ta>
            <ta e="T795" id="Seg_7269" s="T794">nʼelʼzʼa</ta>
            <ta e="T796" id="Seg_7270" s="T795">kan-zittə</ta>
            <ta e="T797" id="Seg_7271" s="T796">dĭgəttə</ta>
            <ta e="T800" id="Seg_7272" s="T799">bü-Tə</ta>
            <ta e="T801" id="Seg_7273" s="T800">kan-bi-bAʔ</ta>
            <ta e="T802" id="Seg_7274" s="T801">kan-bi-bAʔ</ta>
            <ta e="T803" id="Seg_7275" s="T802">dĭgəttə</ta>
            <ta e="T804" id="Seg_7276" s="T803">ku-bi-bAʔ</ta>
            <ta e="T805" id="Seg_7277" s="T804">most</ta>
            <ta e="T806" id="Seg_7278" s="T805">most</ta>
            <ta e="T808" id="Seg_7279" s="T807">šo-bi-bAʔ</ta>
            <ta e="T809" id="Seg_7280" s="T808">i</ta>
            <ta e="T810" id="Seg_7281" s="T809">maʔ-gənʼi</ta>
            <ta e="T811" id="Seg_7282" s="T810">šo-bi-bAʔ</ta>
            <ta e="T814" id="Seg_7283" s="T813">măn</ta>
            <ta e="T815" id="Seg_7284" s="T814">teinen</ta>
            <ta e="T816" id="Seg_7285" s="T815">ertə</ta>
            <ta e="T817" id="Seg_7286" s="T816">uʔbdə-bi-m</ta>
            <ta e="T818" id="Seg_7287" s="T817">măgăzin-Tə</ta>
            <ta e="T819" id="Seg_7288" s="T818">kan-bi-m</ta>
            <ta e="T821" id="Seg_7289" s="T889">nu-bi-m</ta>
            <ta e="T822" id="Seg_7290" s="T821">dĭgəttə</ta>
            <ta e="T823" id="Seg_7291" s="T822">šide</ta>
            <ta e="T824" id="Seg_7292" s="T823">šide</ta>
            <ta e="T825" id="Seg_7293" s="T824">časɨ</ta>
            <ta e="T826" id="Seg_7294" s="T825">nu-bi-m</ta>
            <ta e="T827" id="Seg_7295" s="T826">dĭgəttə</ta>
            <ta e="T828" id="Seg_7296" s="T827">i-bi-m</ta>
            <ta e="T829" id="Seg_7297" s="T828">kola</ta>
            <ta e="T830" id="Seg_7298" s="T829">i</ta>
            <ta e="T831" id="Seg_7299" s="T830">šo-bi-m</ta>
            <ta e="T832" id="Seg_7300" s="T831">maʔ-Tə</ta>
            <ta e="T833" id="Seg_7301" s="T832">i</ta>
            <ta e="T834" id="Seg_7302" s="T833">šiʔ</ta>
            <ta e="T835" id="Seg_7303" s="T834">šo-bi-lAʔ</ta>
            <ta e="T837" id="Seg_7304" s="T836">dĭgəttə</ta>
            <ta e="T838" id="Seg_7305" s="T837">măn</ta>
            <ta e="T839" id="Seg_7306" s="T838">i-bi-m</ta>
            <ta e="T840" id="Seg_7307" s="T839">oʔb</ta>
            <ta e="T841" id="Seg_7308" s="T840">kilo</ta>
            <ta e="T842" id="Seg_7309" s="T841">aktʼa</ta>
            <ta e="T843" id="Seg_7310" s="T842">mĭ-bi-m</ta>
            <ta e="T844" id="Seg_7311" s="T843">onʼiʔ</ta>
            <ta e="T845" id="Seg_7312" s="T844">šălkovej</ta>
            <ta e="T846" id="Seg_7313" s="T845">biəʔ</ta>
            <ta e="T847" id="Seg_7314" s="T846">teʔdə</ta>
            <ta e="T848" id="Seg_7315" s="T847">mĭ-bi-m</ta>
            <ta e="T849" id="Seg_7316" s="T848">onʼiʔ</ta>
            <ta e="T850" id="Seg_7317" s="T849">šălkovej</ta>
            <ta e="T851" id="Seg_7318" s="T850">biəʔ</ta>
            <ta e="T852" id="Seg_7319" s="T851">teʔdə</ta>
            <ta e="T854" id="Seg_7320" s="T853">dĭn</ta>
            <ta e="T855" id="Seg_7321" s="T854">bar</ta>
            <ta e="T856" id="Seg_7322" s="T855">ne-zAŋ</ta>
            <ta e="T857" id="Seg_7323" s="T856">kudo-nzə-laʔbə-jəʔ</ta>
            <ta e="T858" id="Seg_7324" s="T857">kirgaːr-laʔbə-jəʔ</ta>
            <ta e="T859" id="Seg_7325" s="T858">măn-liA-jəʔ</ta>
            <ta e="T860" id="Seg_7326" s="T859">amka</ta>
            <ta e="T861" id="Seg_7327" s="T860">kola</ta>
            <ta e="T862" id="Seg_7328" s="T861">onʼiʔ</ta>
            <ta e="T864" id="Seg_7329" s="T863">onʼiʔ</ta>
            <ta e="T865" id="Seg_7330" s="T864">kilo</ta>
            <ta e="T866" id="Seg_7331" s="T865">mĭ-laʔbə</ta>
            <ta e="T867" id="Seg_7332" s="T866">dĭ</ta>
            <ta e="T868" id="Seg_7333" s="T867">mĭ-bi</ta>
            <ta e="T869" id="Seg_7334" s="T868">onʼiʔ</ta>
            <ta e="T870" id="Seg_7335" s="T869">bar</ta>
            <ta e="T871" id="Seg_7336" s="T870">ne-zAŋ-də</ta>
            <ta e="T872" id="Seg_7337" s="T871">onʼiʔ</ta>
            <ta e="T873" id="Seg_7338" s="T872">kilo</ta>
            <ta e="T874" id="Seg_7339" s="T873">mĭ-bi</ta>
            <ta e="T875" id="Seg_7340" s="T874">măn-liA-jəʔ</ta>
            <ta e="T876" id="Seg_7341" s="T875">ešši-zAŋ-m</ta>
            <ta e="T877" id="Seg_7342" s="T876">kirgaːr-laʔbə-jəʔ</ta>
            <ta e="T878" id="Seg_7343" s="T877">kola</ta>
            <ta e="T879" id="Seg_7344" s="T878">kola</ta>
            <ta e="T880" id="Seg_7345" s="T879">am-zittə</ta>
            <ta e="T881" id="Seg_7346" s="T880">kereʔ</ta>
            <ta e="T882" id="Seg_7347" s="T881">dĭ-zAŋ-Tə</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T1" id="Seg_7348" s="T0">then</ta>
            <ta e="T2" id="Seg_7349" s="T1">this.[NOM.SG]</ta>
            <ta e="T3" id="Seg_7350" s="T2">boy-PL</ta>
            <ta e="T4" id="Seg_7351" s="T3">road-LAT</ta>
            <ta e="T5" id="Seg_7352" s="T4">go-PST-3PL</ta>
            <ta e="T6" id="Seg_7353" s="T5">come-PST-3PL</ta>
            <ta e="T7" id="Seg_7354" s="T6">come-PST-3PL</ta>
            <ta e="T8" id="Seg_7355" s="T7">then</ta>
            <ta e="T9" id="Seg_7356" s="T8">road.[NOM.SG]</ta>
            <ta e="T10" id="Seg_7357" s="T9">road-LOC</ta>
            <ta e="T11" id="Seg_7358" s="T10">edge-LAT/LOC.3SG</ta>
            <ta e="T12" id="Seg_7359" s="T11">sit.down-PST-3PL</ta>
            <ta e="T13" id="Seg_7360" s="T12">see-PRS-3PL</ta>
            <ta e="T14" id="Seg_7361" s="T13">come-PRS.[3SG]</ta>
            <ta e="T15" id="Seg_7362" s="T14">sorcerer.[NOM.SG]</ta>
            <ta e="T16" id="Seg_7363" s="T15">one.[NOM.SG]</ta>
            <ta e="T17" id="Seg_7364" s="T16">horse.[NOM.SG]</ta>
            <ta e="T18" id="Seg_7365" s="T17">red.[NOM.SG]</ta>
            <ta e="T19" id="Seg_7366" s="T18">then</ta>
            <ta e="T20" id="Seg_7367" s="T19">this.[NOM.SG]</ta>
            <ta e="T21" id="Seg_7368" s="T20">PTCL</ta>
            <ta e="T23" id="Seg_7369" s="T21">strongly</ta>
            <ta e="T24" id="Seg_7370" s="T23">then</ta>
            <ta e="T25" id="Seg_7371" s="T24">one.[NOM.SG]</ta>
            <ta e="T27" id="Seg_7372" s="T26">strongly</ta>
            <ta e="T28" id="Seg_7373" s="T27">horse-NOM/GEN.3SG</ta>
            <ta e="T29" id="Seg_7374" s="T28">PTCL</ta>
            <ta e="T31" id="Seg_7375" s="T30">knee-LAT</ta>
            <ta e="T33" id="Seg_7376" s="T32">this.[NOM.SG]</ta>
            <ta e="T34" id="Seg_7377" s="T33">horse.[NOM.SG]</ta>
            <ta e="T35" id="Seg_7378" s="T34">fall-MOM-PST.[3SG]</ta>
            <ta e="T36" id="Seg_7379" s="T35">knee-PL-LAT</ta>
            <ta e="T37" id="Seg_7380" s="T36">then</ta>
            <ta e="T38" id="Seg_7381" s="T37">this.[NOM.SG]</ta>
            <ta e="T39" id="Seg_7382" s="T38">sorcerer.[NOM.SG]</ta>
            <ta e="T40" id="Seg_7383" s="T39">say-IPFVZ-PST.[3SG]</ta>
            <ta e="T41" id="Seg_7384" s="T40">this</ta>
            <ta e="T42" id="Seg_7385" s="T41">there</ta>
            <ta e="T44" id="Seg_7386" s="T43">devil.[NOM.SG]</ta>
            <ta e="T45" id="Seg_7387" s="T44">sit-DUR.[3SG]</ta>
            <ta e="T46" id="Seg_7388" s="T45">very</ta>
            <ta e="T47" id="Seg_7389" s="T46">fear-PRS-1SG</ta>
            <ta e="T48" id="Seg_7390" s="T47">then</ta>
            <ta e="T49" id="Seg_7391" s="T48">go-PST.[3SG]</ta>
            <ta e="T50" id="Seg_7392" s="T49">sit-DUR-3PL</ta>
            <ta e="T51" id="Seg_7393" s="T50">sit-DUR-3PL</ta>
            <ta e="T52" id="Seg_7394" s="T51">again</ta>
            <ta e="T53" id="Seg_7395" s="T52">come-PRS.[3SG]</ta>
            <ta e="T54" id="Seg_7396" s="T53">black.[NOM.SG]</ta>
            <ta e="T55" id="Seg_7397" s="T54">horse-3SG-INS</ta>
            <ta e="T57" id="Seg_7398" s="T56">another.[NOM.SG]</ta>
            <ta e="T58" id="Seg_7399" s="T57">two.[NOM.SG]</ta>
            <ta e="T59" id="Seg_7400" s="T58">two-ORD.[NOM.SG]</ta>
            <ta e="T60" id="Seg_7401" s="T59">sorcerer.[NOM.SG]</ta>
            <ta e="T61" id="Seg_7402" s="T60">then</ta>
            <ta e="T62" id="Seg_7403" s="T61">this-PL</ta>
            <ta e="T63" id="Seg_7404" s="T62">again</ta>
            <ta e="T65" id="Seg_7405" s="T64">then</ta>
            <ta e="T66" id="Seg_7406" s="T65">this.[NOM.SG]</ta>
            <ta e="T67" id="Seg_7407" s="T66">fall-MOM-PST.[3SG]</ta>
            <ta e="T68" id="Seg_7408" s="T67">knee-PL-LAT</ta>
            <ta e="T69" id="Seg_7409" s="T68">this</ta>
            <ta e="T70" id="Seg_7410" s="T69">also</ta>
            <ta e="T71" id="Seg_7411" s="T70">say-IPFVZ.[3SG]</ta>
            <ta e="T72" id="Seg_7412" s="T71">devil.[NOM.SG]</ta>
            <ta e="T73" id="Seg_7413" s="T72">very</ta>
            <ta e="T74" id="Seg_7414" s="T73">powerful.[NOM.SG]</ta>
            <ta e="T75" id="Seg_7415" s="T74">very</ta>
            <ta e="T76" id="Seg_7416" s="T75">I.NOM</ta>
            <ta e="T77" id="Seg_7417" s="T76">fear-PRS-1SG</ta>
            <ta e="T78" id="Seg_7418" s="T77">then</ta>
            <ta e="T80" id="Seg_7419" s="T79">horse.[NOM.SG]</ta>
            <ta e="T81" id="Seg_7420" s="T80">horse-NOM/GEN.3SG</ta>
            <ta e="T82" id="Seg_7421" s="T81">get.up-PST.[3SG]</ta>
            <ta e="T83" id="Seg_7422" s="T82">go-PST.[3SG]</ta>
            <ta e="T84" id="Seg_7423" s="T83">then</ta>
            <ta e="T85" id="Seg_7424" s="T84">sit-DUR-3PL</ta>
            <ta e="T86" id="Seg_7425" s="T85">sit-DUR-3PL</ta>
            <ta e="T88" id="Seg_7426" s="T87">three-ORD.[NOM.SG]</ta>
            <ta e="T89" id="Seg_7427" s="T88">horse.[NOM.SG]</ta>
            <ta e="T90" id="Seg_7428" s="T89">come-PRS.[3SG]</ta>
            <ta e="T91" id="Seg_7429" s="T90">that.[NOM.SG]</ta>
            <ta e="T92" id="Seg_7430" s="T91">horse.[NOM.SG]</ta>
            <ta e="T93" id="Seg_7431" s="T92">white.[NOM.SG]</ta>
            <ta e="T94" id="Seg_7432" s="T93">this-PL</ta>
            <ta e="T95" id="Seg_7433" s="T94">again</ta>
            <ta e="T97" id="Seg_7434" s="T96">then</ta>
            <ta e="T98" id="Seg_7435" s="T97">this.[NOM.SG]</ta>
            <ta e="T99" id="Seg_7436" s="T98">horse.[NOM.SG]</ta>
            <ta e="T100" id="Seg_7437" s="T99">place-PST.[3SG]</ta>
            <ta e="T101" id="Seg_7438" s="T100">stand-DUR.[3SG]</ta>
            <ta e="T102" id="Seg_7439" s="T101">this</ta>
            <ta e="T103" id="Seg_7440" s="T102">sorcerer.[NOM.SG]</ta>
            <ta e="T105" id="Seg_7441" s="T104">go-PST.[3SG]</ta>
            <ta e="T106" id="Seg_7442" s="T105">this-PL-LAT</ta>
            <ta e="T107" id="Seg_7443" s="T106">what.kind</ta>
            <ta e="T108" id="Seg_7444" s="T107">child-PL</ta>
            <ta e="T109" id="Seg_7445" s="T108">sit-DUR-3PL</ta>
            <ta e="T110" id="Seg_7446" s="T109">well</ta>
            <ta e="T111" id="Seg_7447" s="T110">we.NOM</ta>
            <ta e="T112" id="Seg_7448" s="T111">look</ta>
            <ta e="T113" id="Seg_7449" s="T112">we.ACC</ta>
            <ta e="T114" id="Seg_7450" s="T113">PTCL</ta>
            <ta e="T115" id="Seg_7451" s="T114">water-LAT</ta>
            <ta e="T116" id="Seg_7452" s="T115">throw.away-MOM-PST-3PL</ta>
            <ta e="T117" id="Seg_7453" s="T116">I.NOM</ta>
            <ta e="T118" id="Seg_7454" s="T117">we.NOM</ta>
            <ta e="T119" id="Seg_7455" s="T118">here</ta>
            <ta e="T120" id="Seg_7456" s="T119">come-PST-1PL</ta>
            <ta e="T121" id="Seg_7457" s="T120">then</ta>
            <ta e="T122" id="Seg_7458" s="T121">well</ta>
            <ta e="T123" id="Seg_7459" s="T122">live-IMP.2PL</ta>
            <ta e="T124" id="Seg_7460" s="T123">here</ta>
            <ta e="T125" id="Seg_7461" s="T124">this</ta>
            <ta e="T126" id="Seg_7462" s="T125">this-LAT</ta>
            <ta e="T127" id="Seg_7463" s="T126">make-PST-3PL</ta>
            <ta e="T129" id="Seg_7464" s="T128">make-PST.[3SG]</ta>
            <ta e="T130" id="Seg_7465" s="T129">tent.[NOM.SG]</ta>
            <ta e="T131" id="Seg_7466" s="T130">bread.[NOM.SG]</ta>
            <ta e="T132" id="Seg_7467" s="T131">give-PST.[3SG]</ta>
            <ta e="T133" id="Seg_7468" s="T132">meat.[NOM.SG]</ta>
            <ta e="T134" id="Seg_7469" s="T133">give-PST.[3SG]</ta>
            <ta e="T135" id="Seg_7470" s="T134">live-IMP.2PL</ta>
            <ta e="T136" id="Seg_7471" s="T135">here</ta>
            <ta e="T137" id="Seg_7472" s="T136">I.NOM</ta>
            <ta e="T138" id="Seg_7473" s="T137">return-FUT-1SG</ta>
            <ta e="T139" id="Seg_7474" s="T138">alive.[NOM.SG]</ta>
            <ta e="T140" id="Seg_7475" s="T139">become-FUT-1SG</ta>
            <ta e="T141" id="Seg_7476" s="T140">you.PL.ACC</ta>
            <ta e="T142" id="Seg_7477" s="T141">take-FUT-1SG</ta>
            <ta e="T143" id="Seg_7478" s="T142">then</ta>
            <ta e="T144" id="Seg_7479" s="T143">this-PL</ta>
            <ta e="T145" id="Seg_7480" s="T144">live-DUR-PST-3PL</ta>
            <ta e="T146" id="Seg_7481" s="T145">live-DUR-PST</ta>
            <ta e="T147" id="Seg_7482" s="T146">three.[NOM.SG]</ta>
            <ta e="T148" id="Seg_7483" s="T147">day.[NOM.SG]</ta>
            <ta e="T149" id="Seg_7484" s="T148">one.[NOM.SG]</ta>
            <ta e="T150" id="Seg_7485" s="T149">horse.[NOM.SG]</ta>
            <ta e="T151" id="Seg_7486" s="T150">come-PRS.[3SG]</ta>
            <ta e="T152" id="Seg_7487" s="T151">red.[NOM.SG]</ta>
            <ta e="T153" id="Seg_7488" s="T152">then</ta>
            <ta e="T154" id="Seg_7489" s="T153">NEG.EX.[3SG]</ta>
            <ta e="T155" id="Seg_7490" s="T154">there</ta>
            <ta e="T156" id="Seg_7491" s="T155">man.[NOM.SG]</ta>
            <ta e="T157" id="Seg_7492" s="T156">then</ta>
            <ta e="T158" id="Seg_7493" s="T157">black.[NOM.SG]</ta>
            <ta e="T159" id="Seg_7494" s="T158">horse.[NOM.SG]</ta>
            <ta e="T160" id="Seg_7495" s="T159">come-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_7496" s="T160">man.[NOM.SG]</ta>
            <ta e="T162" id="Seg_7497" s="T161">also</ta>
            <ta e="T163" id="Seg_7498" s="T162">NEG.EX.[3SG]</ta>
            <ta e="T164" id="Seg_7499" s="T163">live-DUR-3PL</ta>
            <ta e="T165" id="Seg_7500" s="T164">live-DUR-3PL</ta>
            <ta e="T166" id="Seg_7501" s="T165">then</ta>
            <ta e="T167" id="Seg_7502" s="T166">snow.[NOM.SG]</ta>
            <ta e="T168" id="Seg_7503" s="T167">horse.[NOM.SG]</ta>
            <ta e="T169" id="Seg_7504" s="T168">come-PRS.[3SG]</ta>
            <ta e="T170" id="Seg_7505" s="T169">and</ta>
            <ta e="T171" id="Seg_7506" s="T170">man.[NOM.SG]</ta>
            <ta e="T172" id="Seg_7507" s="T171">sit-DUR.[3SG]</ta>
            <ta e="T173" id="Seg_7508" s="T172">well</ta>
            <ta e="T174" id="Seg_7509" s="T173">go-OPT.DU/PL-1PL</ta>
            <ta e="T175" id="Seg_7510" s="T174">I-COM</ta>
            <ta e="T176" id="Seg_7511" s="T175">tent-LAT-2SG</ta>
            <ta e="T177" id="Seg_7512" s="T176">maybe</ta>
            <ta e="T178" id="Seg_7513" s="T177">go-FUT-2SG</ta>
            <ta e="T179" id="Seg_7514" s="T178">or</ta>
            <ta e="T180" id="Seg_7515" s="T179">I.LAT</ta>
            <ta e="T181" id="Seg_7516" s="T180">live-FUT-2SG</ta>
            <ta e="T182" id="Seg_7517" s="T181">come-PST-3PL</ta>
            <ta e="T183" id="Seg_7518" s="T182">this.[NOM.SG]</ta>
            <ta e="T184" id="Seg_7519" s="T183">man-LAT</ta>
            <ta e="T185" id="Seg_7520" s="T184">one.[NOM.SG]</ta>
            <ta e="T186" id="Seg_7521" s="T185">go-PST.[3SG]</ta>
            <ta e="T187" id="Seg_7522" s="T186">tent-LAT/LOC.3SG</ta>
            <ta e="T188" id="Seg_7523" s="T187">come-PST.[3SG]</ta>
            <ta e="T189" id="Seg_7524" s="T188">tent-LAT/LOC.3SG</ta>
            <ta e="T190" id="Seg_7525" s="T189">NEG.EX.[3SG]</ta>
            <ta e="T192" id="Seg_7526" s="T191">people.[NOM.SG]</ta>
            <ta e="T193" id="Seg_7527" s="T192">who.[NOM.SG]=INDEF</ta>
            <ta e="T194" id="Seg_7528" s="T193">NEG.EX.[3SG]</ta>
            <ta e="T195" id="Seg_7529" s="T194">one.[NOM.SG]</ta>
            <ta e="T196" id="Seg_7530" s="T195">one.[NOM.SG]</ta>
            <ta e="T198" id="Seg_7531" s="T197">one.[NOM.SG]</ta>
            <ta e="T199" id="Seg_7532" s="T198">tent.[NOM.SG]</ta>
            <ta e="T200" id="Seg_7533" s="T199">stand-PRS.[3SG]</ta>
            <ta e="T201" id="Seg_7534" s="T200">this</ta>
            <ta e="T202" id="Seg_7535" s="T201">come-PST.[3SG]</ta>
            <ta e="T205" id="Seg_7536" s="T204">go.where-PST.[3SG]</ta>
            <ta e="T206" id="Seg_7537" s="T205">PTCL</ta>
            <ta e="T207" id="Seg_7538" s="T206">people.[NOM.SG]</ta>
            <ta e="T208" id="Seg_7539" s="T207">and</ta>
            <ta e="T209" id="Seg_7540" s="T208">we.NOM</ta>
            <ta e="T210" id="Seg_7541" s="T209">daughter-NOM/GEN/ACC.1PL</ta>
            <ta e="T211" id="Seg_7542" s="T210">PTCL</ta>
            <ta e="T212" id="Seg_7543" s="T211">two.[NOM.SG]</ta>
            <ta e="T213" id="Seg_7544" s="T212">son.[NOM.SG]</ta>
            <ta e="T214" id="Seg_7545" s="T213">be-PST-3PL</ta>
            <ta e="T215" id="Seg_7546" s="T214">this-PL</ta>
            <ta e="T216" id="Seg_7547" s="T215">water-LAT</ta>
            <ta e="T217" id="Seg_7548" s="T216">let-MOM-PST-3PL</ta>
            <ta e="T218" id="Seg_7549" s="T217">and</ta>
            <ta e="T219" id="Seg_7550" s="T218">we.NOM</ta>
            <ta e="T220" id="Seg_7551" s="T219">daughter-NOM/GEN/ACC.1PL</ta>
            <ta e="T221" id="Seg_7552" s="T220">PTCL</ta>
            <ta e="T222" id="Seg_7553" s="T221">people-ACC</ta>
            <ta e="T223" id="Seg_7554" s="T222">stab-MOM-PST.[3SG]</ta>
            <ta e="T224" id="Seg_7555" s="T223">and</ta>
            <ta e="T225" id="Seg_7556" s="T224">where</ta>
            <ta e="T226" id="Seg_7557" s="T225">this.[NOM.SG]</ta>
            <ta e="T227" id="Seg_7558" s="T226">where.to=INDEF</ta>
            <ta e="T384" id="Seg_7559" s="T227">go-CVB</ta>
            <ta e="T228" id="Seg_7560" s="T384">disappear-PST.[3SG]</ta>
            <ta e="T229" id="Seg_7561" s="T228">NEG</ta>
            <ta e="T230" id="Seg_7562" s="T229">know-1PL</ta>
            <ta e="T231" id="Seg_7563" s="T230">then</ta>
            <ta e="T232" id="Seg_7564" s="T231">this.[NOM.SG]</ta>
            <ta e="T233" id="Seg_7565" s="T232">horse.[NOM.SG]</ta>
            <ta e="T234" id="Seg_7566" s="T233">take-PST.[3SG]</ta>
            <ta e="T235" id="Seg_7567" s="T234">go-PST.[3SG]</ta>
            <ta e="T236" id="Seg_7568" s="T235">find-INF.LAT</ta>
            <ta e="T239" id="Seg_7569" s="T238">see-RES-PST.[3SG]</ta>
            <ta e="T240" id="Seg_7570" s="T239">see-PST.[3SG]</ta>
            <ta e="T241" id="Seg_7571" s="T240">see-PST-3PL</ta>
            <ta e="T242" id="Seg_7572" s="T241">there</ta>
            <ta e="T243" id="Seg_7573" s="T242">then</ta>
            <ta e="T244" id="Seg_7574" s="T243">INCH</ta>
            <ta e="T245" id="Seg_7575" s="T244">this-COM</ta>
            <ta e="T246" id="Seg_7576" s="T245">PTCL</ta>
            <ta e="T247" id="Seg_7577" s="T246">fight-INF.LAT</ta>
            <ta e="T248" id="Seg_7578" s="T247">this</ta>
            <ta e="T249" id="Seg_7579" s="T248">want.PST.M.SG</ta>
            <ta e="T250" id="Seg_7580" s="T249">this-ACC</ta>
            <ta e="T252" id="Seg_7581" s="T251">cut-INF.LAT</ta>
            <ta e="T253" id="Seg_7582" s="T252">knife-INS</ta>
            <ta e="T254" id="Seg_7583" s="T253">this</ta>
            <ta e="T255" id="Seg_7584" s="T254">PTCL</ta>
            <ta e="T256" id="Seg_7585" s="T255">say-IPFVZ.[3SG]</ta>
            <ta e="T257" id="Seg_7586" s="T256">NEG.AUX-IMP.2SG</ta>
            <ta e="T258" id="Seg_7587" s="T257">cut-EP-CNG</ta>
            <ta e="T259" id="Seg_7588" s="T258">I.NOM</ta>
            <ta e="T260" id="Seg_7589" s="T259">you.NOM</ta>
            <ta e="T261" id="Seg_7590" s="T260">brother-NOM/GEN/ACC.1SG</ta>
            <ta e="T262" id="Seg_7591" s="T261">be-PRS-1SG</ta>
            <ta e="T263" id="Seg_7592" s="T262">hen-PL</ta>
            <ta e="T264" id="Seg_7593" s="T263">speak-DUR-3PL</ta>
            <ta e="T265" id="Seg_7594" s="T264">capercaillie.[NOM.SG]</ta>
            <ta e="T266" id="Seg_7595" s="T265">and</ta>
            <ta e="T269" id="Seg_7596" s="T267">partridge.[NOM.SG]</ta>
            <ta e="T270" id="Seg_7597" s="T269">partridge.[NOM.SG]</ta>
            <ta e="T271" id="Seg_7598" s="T270">say-PRS.[3SG]</ta>
            <ta e="T272" id="Seg_7599" s="T271">capercaillie-LAT</ta>
            <ta e="T273" id="Seg_7600" s="T272">you.NOM</ta>
            <ta e="T274" id="Seg_7601" s="T273">NEG</ta>
            <ta e="T275" id="Seg_7602" s="T274">beautiful.[NOM.SG]</ta>
            <ta e="T276" id="Seg_7603" s="T275">what.kind</ta>
            <ta e="T277" id="Seg_7604" s="T276">pants-PL</ta>
            <ta e="T278" id="Seg_7605" s="T277">NEG</ta>
            <ta e="T279" id="Seg_7606" s="T278">good.[NOM.SG]</ta>
            <ta e="T280" id="Seg_7607" s="T279">and</ta>
            <ta e="T281" id="Seg_7608" s="T280">I.NOM</ta>
            <ta e="T282" id="Seg_7609" s="T281">beautiful.[NOM.SG]</ta>
            <ta e="T283" id="Seg_7610" s="T282">be-PRS-1SG</ta>
            <ta e="T284" id="Seg_7611" s="T883">horse-ABL</ta>
            <ta e="T285" id="Seg_7612" s="T284">hair.[NOM.SG]</ta>
            <ta e="T286" id="Seg_7613" s="T285">PTCL</ta>
            <ta e="T287" id="Seg_7614" s="T286">off-pull-MOM-PST-1SG</ta>
            <ta e="T288" id="Seg_7615" s="T287">and</ta>
            <ta e="T289" id="Seg_7616" s="T288">self-LAT/LOC.3SG</ta>
            <ta e="T290" id="Seg_7617" s="T289">put-PST-1SG</ta>
            <ta e="T292" id="Seg_7618" s="T290">head-LAT</ta>
            <ta e="T293" id="Seg_7619" s="T292">very</ta>
            <ta e="T294" id="Seg_7620" s="T293">beautiful.[NOM.SG]</ta>
            <ta e="T295" id="Seg_7621" s="T294">be-PRS-1SG</ta>
            <ta e="T296" id="Seg_7622" s="T295">now</ta>
            <ta e="T298" id="Seg_7623" s="T297">mountain-LOC</ta>
            <ta e="T299" id="Seg_7624" s="T298">PTCL</ta>
            <ta e="T300" id="Seg_7625" s="T299">cauldron.[NOM.SG]</ta>
            <ta e="T301" id="Seg_7626" s="T300">boil-DUR.[3SG]</ta>
            <ta e="T302" id="Seg_7627" s="T301">what.[NOM.SG]</ta>
            <ta e="T303" id="Seg_7628" s="T302">this.[NOM.SG]</ta>
            <ta e="T304" id="Seg_7629" s="T303">such.[NOM.SG]</ta>
            <ta e="T305" id="Seg_7630" s="T304">ant.PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T308" id="Seg_7631" s="T306">mountain-LOC</ta>
            <ta e="T310" id="Seg_7632" s="T309">mountain-LOC</ta>
            <ta e="T311" id="Seg_7633" s="T310">bird.[NOM.SG]</ta>
            <ta e="T312" id="Seg_7634" s="T311">hang-DUR.[3SG]</ta>
            <ta e="T313" id="Seg_7635" s="T312">what.[NOM.SG]</ta>
            <ta e="T314" id="Seg_7636" s="T313">such.[NOM.SG]</ta>
            <ta e="T315" id="Seg_7637" s="T314">breast.[NOM.SG]</ta>
            <ta e="T317" id="Seg_7638" s="T316">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T319" id="Seg_7639" s="T318">earth-LOC</ta>
            <ta e="T320" id="Seg_7640" s="T319">sit-DUR.[3SG]</ta>
            <ta e="T321" id="Seg_7641" s="T320">and</ta>
            <ta e="T322" id="Seg_7642" s="T321">ear-NOM/GEN.3SG</ta>
            <ta e="T323" id="Seg_7643" s="T322">another.[NOM.SG]</ta>
            <ta e="T324" id="Seg_7644" s="T323">container-%%</ta>
            <ta e="T325" id="Seg_7645" s="T324">sit-DUR.[3SG]</ta>
            <ta e="T326" id="Seg_7646" s="T325">earring-PL-INS</ta>
            <ta e="T330" id="Seg_7647" s="T329">two.[NOM.SG]</ta>
            <ta e="T332" id="Seg_7648" s="T331">woman-PL</ta>
            <ta e="T333" id="Seg_7649" s="T332">lie-DUR-3PL</ta>
            <ta e="T335" id="Seg_7650" s="T334">lie-DUR-3PL</ta>
            <ta e="T338" id="Seg_7651" s="T337">fire-LAT</ta>
            <ta e="T339" id="Seg_7652" s="T338">edge-LAT/LOC.3SG</ta>
            <ta e="T340" id="Seg_7653" s="T339">pregnant-PL</ta>
            <ta e="T341" id="Seg_7654" s="T340">this</ta>
            <ta e="T342" id="Seg_7655" s="T341">foot-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T343" id="Seg_7656" s="T342">thigh-PL</ta>
            <ta e="T345" id="Seg_7657" s="T344">foot-NOM/GEN.3SG</ta>
            <ta e="T346" id="Seg_7658" s="T345">NEG.EX.[3SG]</ta>
            <ta e="T347" id="Seg_7659" s="T346">and</ta>
            <ta e="T348" id="Seg_7660" s="T347">hand-NOM/GEN.3SG</ta>
            <ta e="T349" id="Seg_7661" s="T348">NEG.EX.[3SG]</ta>
            <ta e="T350" id="Seg_7662" s="T349">and</ta>
            <ta e="T352" id="Seg_7663" s="T350">go-PRS.[3SG]</ta>
            <ta e="T354" id="Seg_7664" s="T353">mountain-LAT</ta>
            <ta e="T355" id="Seg_7665" s="T354">walk-PRS.[3SG]</ta>
            <ta e="T356" id="Seg_7666" s="T355">this</ta>
            <ta e="T357" id="Seg_7667" s="T356">sun.[NOM.SG]</ta>
            <ta e="T359" id="Seg_7668" s="T358">many</ta>
            <ta e="T360" id="Seg_7669" s="T359">bird-PL</ta>
            <ta e="T361" id="Seg_7670" s="T360">head-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T362" id="Seg_7671" s="T361">PTCL</ta>
            <ta e="T363" id="Seg_7672" s="T362">one.[NOM.SG]</ta>
            <ta e="T364" id="Seg_7673" s="T363">one-LAT</ta>
            <ta e="T366" id="Seg_7674" s="T365">and</ta>
            <ta e="T368" id="Seg_7675" s="T367">ass-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T369" id="Seg_7676" s="T368">PTCL</ta>
            <ta e="T370" id="Seg_7677" s="T369">far-LAT.ADV</ta>
            <ta e="T371" id="Seg_7678" s="T370">one.[NOM.SG]</ta>
            <ta e="T372" id="Seg_7679" s="T371">one-LAT</ta>
            <ta e="T373" id="Seg_7680" s="T372">this</ta>
            <ta e="T374" id="Seg_7681" s="T373">tent.[NOM.SG]</ta>
            <ta e="T376" id="Seg_7682" s="T375">house-LOC</ta>
            <ta e="T377" id="Seg_7683" s="T376">lie-DUR.[3SG]</ta>
            <ta e="T379" id="Seg_7684" s="T378">wet.[NOM.SG]</ta>
            <ta e="T380" id="Seg_7685" s="T379">calf.[NOM.SG]</ta>
            <ta e="T381" id="Seg_7686" s="T380">and</ta>
            <ta e="T382" id="Seg_7687" s="T381">tell-INF.LAT</ta>
            <ta e="T383" id="Seg_7688" s="T382">this.[NOM.SG]</ta>
            <ta e="T385" id="Seg_7689" s="T885">language-NOM/GEN.3SG</ta>
            <ta e="T388" id="Seg_7690" s="T387">white.[NOM.SG]</ta>
            <ta e="T390" id="Seg_7691" s="T389">tree-3PL-LAT</ta>
            <ta e="T391" id="Seg_7692" s="T390">%%</ta>
            <ta e="T392" id="Seg_7693" s="T391">sit-DUR.[3SG]</ta>
            <ta e="T394" id="Seg_7694" s="T393">this</ta>
            <ta e="T395" id="Seg_7695" s="T394">language.[NOM.SG]</ta>
            <ta e="T397" id="Seg_7696" s="T396">foot-NOM/GEN.3SG</ta>
            <ta e="T399" id="Seg_7697" s="T398">hand-NOM/GEN.3SG</ta>
            <ta e="T400" id="Seg_7698" s="T399">NEG.EX.[3SG]</ta>
            <ta e="T401" id="Seg_7699" s="T400">and</ta>
            <ta e="T402" id="Seg_7700" s="T401">God-LAT</ta>
            <ta e="T403" id="Seg_7701" s="T402">fall-PRS.[3SG]</ta>
            <ta e="T404" id="Seg_7702" s="T403">this</ta>
            <ta e="T407" id="Seg_7703" s="T406">foot-NOM/GEN.3SG</ta>
            <ta e="T408" id="Seg_7704" s="T407">hand-NOM/GEN.3SG</ta>
            <ta e="T409" id="Seg_7705" s="T408">NEG.EX.[3SG]</ta>
            <ta e="T410" id="Seg_7706" s="T409">and</ta>
            <ta e="T411" id="Seg_7707" s="T410">tree-3PL-LAT</ta>
            <ta e="T412" id="Seg_7708" s="T411">go-PRS.[3SG]</ta>
            <ta e="T415" id="Seg_7709" s="T414">and</ta>
            <ta e="T416" id="Seg_7710" s="T415">tent-LAT/LOC.3SG</ta>
            <ta e="T417" id="Seg_7711" s="T416">come-FUT-3SG</ta>
            <ta e="T418" id="Seg_7712" s="T417">lie-DUR.[3SG]</ta>
            <ta e="T419" id="Seg_7713" s="T418">this</ta>
            <ta e="T420" id="Seg_7714" s="T419">axe.[NOM.SG]</ta>
            <ta e="T422" id="Seg_7715" s="T421">hand-NOM/GEN.3SG</ta>
            <ta e="T423" id="Seg_7716" s="T422">foot-NOM/GEN.3SG</ta>
            <ta e="T424" id="Seg_7717" s="T423">NEG.EX.[3SG]</ta>
            <ta e="T425" id="Seg_7718" s="T424">and</ta>
            <ta e="T426" id="Seg_7719" s="T425">door-NOM/GEN/ACC.1SG</ta>
            <ta e="T427" id="Seg_7720" s="T426">PTCL</ta>
            <ta e="T428" id="Seg_7721" s="T427">open-DUR.[3SG]</ta>
            <ta e="T429" id="Seg_7722" s="T428">this</ta>
            <ta e="T430" id="Seg_7723" s="T429">wind.[NOM.SG]</ta>
            <ta e="T432" id="Seg_7724" s="T431">small.[NOM.SG]</ta>
            <ta e="T434" id="Seg_7725" s="T433">dog.[NOM.SG]</ta>
            <ta e="T435" id="Seg_7726" s="T434">tent-LAT/LOC.3SG</ta>
            <ta e="T436" id="Seg_7727" s="T435">edge-LAT/LOC.3SG</ta>
            <ta e="T437" id="Seg_7728" s="T436">sit-DUR-3PL</ta>
            <ta e="T438" id="Seg_7729" s="T437">NEG</ta>
            <ta e="T439" id="Seg_7730" s="T438">shout-PRS.[3SG]</ta>
            <ta e="T440" id="Seg_7731" s="T439">and</ta>
            <ta e="T442" id="Seg_7732" s="T441">who-ACC=INDEF</ta>
            <ta e="T443" id="Seg_7733" s="T442">tent-LAT</ta>
            <ta e="T445" id="Seg_7734" s="T443">NEG</ta>
            <ta e="T447" id="Seg_7735" s="T446">who-LAT=INDEF</ta>
            <ta e="T448" id="Seg_7736" s="T447">tent-LAT/LOC.3SG</ta>
            <ta e="T449" id="Seg_7737" s="T448">NEG</ta>
            <ta e="T450" id="Seg_7738" s="T449">let-CVB</ta>
            <ta e="T451" id="Seg_7739" s="T450">this</ta>
            <ta e="T454" id="Seg_7740" s="T453">Krasnoyarsk-PL</ta>
            <ta e="T455" id="Seg_7741" s="T454">house-PL</ta>
            <ta e="T456" id="Seg_7742" s="T455">cut-DUR-3PL</ta>
            <ta e="T457" id="Seg_7743" s="T456">and</ta>
            <ta e="T458" id="Seg_7744" s="T457">here</ta>
            <ta e="T459" id="Seg_7745" s="T458">tree-PL</ta>
            <ta e="T460" id="Seg_7746" s="T459">fly-DUR-3PL</ta>
            <ta e="T461" id="Seg_7747" s="T460">this</ta>
            <ta e="T462" id="Seg_7748" s="T461">paper.[NOM.SG]</ta>
            <ta e="T463" id="Seg_7749" s="T462">walk-PRS.[3SG]</ta>
            <ta e="T464" id="Seg_7750" s="T463">from.there</ta>
            <ta e="T467" id="Seg_7751" s="T466">tongue-NOM/GEN.3SG</ta>
            <ta e="T468" id="Seg_7752" s="T467">NEG.EX.[3SG]</ta>
            <ta e="T469" id="Seg_7753" s="T468">eye-NOM/GEN.3SG</ta>
            <ta e="T470" id="Seg_7754" s="T469">NEG.EX.[3SG]</ta>
            <ta e="T471" id="Seg_7755" s="T470">mouth-NOM/GEN/ACC.3SG</ta>
            <ta e="T472" id="Seg_7756" s="T471">NEG.EX.[3SG]</ta>
            <ta e="T473" id="Seg_7757" s="T472">and</ta>
            <ta e="T474" id="Seg_7758" s="T473">say-PRS.[3SG]</ta>
            <ta e="T475" id="Seg_7759" s="T474">how.much</ta>
            <ta e="T476" id="Seg_7760" s="T475">time</ta>
            <ta e="T477" id="Seg_7761" s="T476">go-PST.[3SG]</ta>
            <ta e="T478" id="Seg_7762" s="T477">this</ta>
            <ta e="T479" id="Seg_7763" s="T478">hour-NOM/GEN/ACC.3PL</ta>
            <ta e="T481" id="Seg_7764" s="T480">lie-DUR.[3SG]</ta>
            <ta e="T482" id="Seg_7765" s="T481">so</ta>
            <ta e="T483" id="Seg_7766" s="T482">small.[NOM.SG]</ta>
            <ta e="T484" id="Seg_7767" s="T483">cat-LAT</ta>
            <ta e="T485" id="Seg_7768" s="T484">and</ta>
            <ta e="T486" id="Seg_7769" s="T485">stand-DUR.[3SG]</ta>
            <ta e="T487" id="Seg_7770" s="T486">so</ta>
            <ta e="T488" id="Seg_7771" s="T487">very</ta>
            <ta e="T489" id="Seg_7772" s="T488">big.[NOM.SG]</ta>
            <ta e="T490" id="Seg_7773" s="T489">big.[NOM.SG]</ta>
            <ta e="T491" id="Seg_7774" s="T490">horse-3SG-INS</ta>
            <ta e="T493" id="Seg_7775" s="T492">saddle.[NOM.SG]</ta>
            <ta e="T495" id="Seg_7776" s="T494">sit-DUR.[3SG]</ta>
            <ta e="T496" id="Seg_7777" s="T495">girl.[NOM.SG]</ta>
            <ta e="T497" id="Seg_7778" s="T496">all</ta>
            <ta e="T498" id="Seg_7779" s="T497">smallpox-3PL-INS</ta>
            <ta e="T499" id="Seg_7780" s="T498">this</ta>
            <ta e="T500" id="Seg_7781" s="T499">and</ta>
            <ta e="T501" id="Seg_7782" s="T500">this.[NOM.SG]</ta>
            <ta e="T502" id="Seg_7783" s="T501">thimble.[NOM.SG]</ta>
            <ta e="T504" id="Seg_7784" s="T503">collect-PST-1SG</ta>
            <ta e="T505" id="Seg_7785" s="T504">collect-PST-1SG</ta>
            <ta e="T506" id="Seg_7786" s="T505">thread-PL</ta>
            <ta e="T507" id="Seg_7787" s="T506">NEG</ta>
            <ta e="T509" id="Seg_7788" s="T508">NEG</ta>
            <ta e="T510" id="Seg_7789" s="T509">can-PST-1SG</ta>
            <ta e="T511" id="Seg_7790" s="T510">collect-INF.LAT</ta>
            <ta e="T512" id="Seg_7791" s="T511">this.[NOM.SG]</ta>
            <ta e="T513" id="Seg_7792" s="T512">road.[NOM.SG]</ta>
            <ta e="T515" id="Seg_7793" s="T514">sit-DUR.[3SG]</ta>
            <ta e="T516" id="Seg_7794" s="T515">man.[NOM.SG]</ta>
            <ta e="T517" id="Seg_7795" s="T516">garden.bed-LOC</ta>
            <ta e="T518" id="Seg_7796" s="T517">PTCL</ta>
            <ta e="T519" id="Seg_7797" s="T518">many</ta>
            <ta e="T520" id="Seg_7798" s="T519">patch-NOM/GEN/ACC.3PL</ta>
            <ta e="T521" id="Seg_7799" s="T520">who.[NOM.SG]</ta>
            <ta e="T523" id="Seg_7800" s="T522">see-DUR.[3SG]</ta>
            <ta e="T524" id="Seg_7801" s="T523">PTCL</ta>
            <ta e="T525" id="Seg_7802" s="T524">cry-DUR.[3SG]</ta>
            <ta e="T526" id="Seg_7803" s="T525">this</ta>
            <ta e="T527" id="Seg_7804" s="T526">onion.[NOM.SG]</ta>
            <ta e="T529" id="Seg_7805" s="T528">sit-DUR.[3SG]</ta>
            <ta e="T530" id="Seg_7806" s="T529">many</ta>
            <ta e="T532" id="Seg_7807" s="T530">clothing.[NOM.SG]</ta>
            <ta e="T535" id="Seg_7808" s="T534">seven.[NOM.SG]</ta>
            <ta e="T536" id="Seg_7809" s="T535">money.[NOM.SG]</ta>
            <ta e="T540" id="Seg_7810" s="T539">ten.[NOM.SG]</ta>
            <ta e="T542" id="Seg_7811" s="T541">seven.[NOM.SG]</ta>
            <ta e="T543" id="Seg_7812" s="T542">clothing.[NOM.SG]</ta>
            <ta e="T544" id="Seg_7813" s="T543">and</ta>
            <ta e="T545" id="Seg_7814" s="T544">close-INF.LAT</ta>
            <ta e="T546" id="Seg_7815" s="T545">NEG.EX.[3SG]</ta>
            <ta e="T547" id="Seg_7816" s="T546">this</ta>
            <ta e="T548" id="Seg_7817" s="T547">cabbage.[NOM.SG]</ta>
            <ta e="T550" id="Seg_7818" s="T549">sit-DUR.[3SG]</ta>
            <ta e="T551" id="Seg_7819" s="T550">girl.[NOM.SG]</ta>
            <ta e="T552" id="Seg_7820" s="T551">earth-LOC</ta>
            <ta e="T553" id="Seg_7821" s="T552">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T554" id="Seg_7822" s="T553">red.[NOM.SG]</ta>
            <ta e="T555" id="Seg_7823" s="T554">and</ta>
            <ta e="T556" id="Seg_7824" s="T555">hair-NOM/GEN.3SG</ta>
            <ta e="T557" id="Seg_7825" s="T556">outside</ta>
            <ta e="T558" id="Seg_7826" s="T557">this</ta>
            <ta e="T561" id="Seg_7827" s="T560">I.NOM</ta>
            <ta e="T562" id="Seg_7828" s="T561">find-PST-1SG</ta>
            <ta e="T563" id="Seg_7829" s="T562">whip.[NOM.SG]</ta>
            <ta e="T564" id="Seg_7830" s="T563">and</ta>
            <ta e="T565" id="Seg_7831" s="T564">go-INF.LAT</ta>
            <ta e="T566" id="Seg_7832" s="T565">NEG</ta>
            <ta e="T567" id="Seg_7833" s="T566">can-PST-1SG</ta>
            <ta e="T568" id="Seg_7834" s="T567">and</ta>
            <ta e="T569" id="Seg_7835" s="T568">this.[NOM.SG]</ta>
            <ta e="T570" id="Seg_7836" s="T569">what.[NOM.SG]</ta>
            <ta e="T571" id="Seg_7837" s="T570">%snake.[NOM.SG]</ta>
            <ta e="T573" id="Seg_7838" s="T572">come-PRS.[3SG]</ta>
            <ta e="T574" id="Seg_7839" s="T573">girl.[NOM.SG]</ta>
            <ta e="T575" id="Seg_7840" s="T574">and</ta>
            <ta e="T576" id="Seg_7841" s="T575">there</ta>
            <ta e="T577" id="Seg_7842" s="T576">hair-PL</ta>
            <ta e="T578" id="Seg_7843" s="T577">PTCL</ta>
            <ta e="T580" id="Seg_7844" s="T579">%%-PTCP-3PL</ta>
            <ta e="T581" id="Seg_7845" s="T580">PTCL</ta>
            <ta e="T583" id="Seg_7846" s="T582">this</ta>
            <ta e="T584" id="Seg_7847" s="T583">magpie.[NOM.SG]</ta>
            <ta e="T585" id="Seg_7848" s="T584">be-PRS.[3SG]</ta>
            <ta e="T587" id="Seg_7849" s="T586">two.[NOM.SG]</ta>
            <ta e="T588" id="Seg_7850" s="T587">stand-PRS-3PL</ta>
            <ta e="T589" id="Seg_7851" s="T588">two.[NOM.SG]</ta>
            <ta e="T590" id="Seg_7852" s="T589">lie-DUR-3PL</ta>
            <ta e="T591" id="Seg_7853" s="T590">one.[NOM.SG]</ta>
            <ta e="T592" id="Seg_7854" s="T591">go-DUR.[3SG]</ta>
            <ta e="T593" id="Seg_7855" s="T592">five.[NOM.SG]</ta>
            <ta e="T594" id="Seg_7856" s="T593">go-DUR.[3SG]</ta>
            <ta e="T595" id="Seg_7857" s="T594">and</ta>
            <ta e="T596" id="Seg_7858" s="T595">seven.[NOM.SG]</ta>
            <ta e="T598" id="Seg_7859" s="T597">close-DUR.[3SG]</ta>
            <ta e="T599" id="Seg_7860" s="T598">this</ta>
            <ta e="T600" id="Seg_7861" s="T599">door</ta>
            <ta e="T601" id="Seg_7862" s="T600">be-PRS.[3SG]</ta>
            <ta e="T604" id="Seg_7863" s="T603">sheep.[NOM.SG]</ta>
            <ta e="T605" id="Seg_7864" s="T604">many</ta>
            <ta e="T606" id="Seg_7865" s="T605">go-DUR-3PL</ta>
            <ta e="T607" id="Seg_7866" s="T606">and</ta>
            <ta e="T608" id="Seg_7867" s="T607">herdsman.[NOM.SG]</ta>
            <ta e="T611" id="Seg_7868" s="T609">horn-NOM/GEN.3SG</ta>
            <ta e="T612" id="Seg_7869" s="T611">this</ta>
            <ta e="T613" id="Seg_7870" s="T612">moon.[NOM.SG]</ta>
            <ta e="T614" id="Seg_7871" s="T613">and</ta>
            <ta e="T615" id="Seg_7872" s="T614">star.PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T617" id="Seg_7873" s="T616">two.[NOM.SG]</ta>
            <ta e="T618" id="Seg_7874" s="T617">hole.[NOM.SG]</ta>
            <ta e="T619" id="Seg_7875" s="T618">and</ta>
            <ta e="T620" id="Seg_7876" s="T619">two.[NOM.SG]</ta>
            <ta e="T621" id="Seg_7877" s="T620">sharp-PL</ta>
            <ta e="T622" id="Seg_7878" s="T621">and</ta>
            <ta e="T623" id="Seg_7879" s="T622">inside-LOC</ta>
            <ta e="T624" id="Seg_7880" s="T623">button.[NOM.SG]</ta>
            <ta e="T625" id="Seg_7881" s="T624">scissors-PL</ta>
            <ta e="T627" id="Seg_7882" s="T626">I.NOM</ta>
            <ta e="T628" id="Seg_7883" s="T627">always</ta>
            <ta e="T629" id="Seg_7884" s="T628">eat-DUR-1SG</ta>
            <ta e="T630" id="Seg_7885" s="T629">eat-DUR-1SG</ta>
            <ta e="T631" id="Seg_7886" s="T630">and</ta>
            <ta e="T632" id="Seg_7887" s="T631">shit-INF.LAT</ta>
            <ta e="T633" id="Seg_7888" s="T632">NEG</ta>
            <ta e="T634" id="Seg_7889" s="T633">can-PRS-1SG</ta>
            <ta e="T635" id="Seg_7890" s="T634">sack.[NOM.SG]</ta>
            <ta e="T637" id="Seg_7891" s="T636">rooster.[NOM.SG]</ta>
            <ta e="T638" id="Seg_7892" s="T637">sit-DUR.[3SG]</ta>
            <ta e="T639" id="Seg_7893" s="T638">fence-LOC</ta>
            <ta e="T640" id="Seg_7894" s="T639">tail.[NOM.SG]</ta>
            <ta e="T641" id="Seg_7895" s="T640">earth-LOC</ta>
            <ta e="T642" id="Seg_7896" s="T641">and</ta>
            <ta e="T643" id="Seg_7897" s="T642">shout-DUR.[3SG]</ta>
            <ta e="T644" id="Seg_7898" s="T643">God-LAT</ta>
            <ta e="T645" id="Seg_7899" s="T644">this</ta>
            <ta e="T646" id="Seg_7900" s="T645">bell.[NOM.SG]</ta>
            <ta e="T648" id="Seg_7901" s="T647">stand-PRS.[3SG]</ta>
            <ta e="T649" id="Seg_7902" s="T648">boy.[NOM.SG]</ta>
            <ta e="T650" id="Seg_7903" s="T649">inside-LOC</ta>
            <ta e="T652" id="Seg_7904" s="T651">%burn-DUR.[3SG]</ta>
            <ta e="T653" id="Seg_7905" s="T652">and</ta>
            <ta e="T654" id="Seg_7906" s="T653">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T655" id="Seg_7907" s="T654">PTCL</ta>
            <ta e="T656" id="Seg_7908" s="T655">piss-DUR.[3SG]</ta>
            <ta e="T657" id="Seg_7909" s="T656">this</ta>
            <ta e="T658" id="Seg_7910" s="T657">%kettle.[NOM.SG]</ta>
            <ta e="T660" id="Seg_7911" s="T659">two-COLL</ta>
            <ta e="T661" id="Seg_7912" s="T660">go-IPFVZ-PRS-3PL</ta>
            <ta e="T662" id="Seg_7913" s="T661">one.[NOM.SG]</ta>
            <ta e="T663" id="Seg_7914" s="T662">walk-PRS.[3SG]</ta>
            <ta e="T664" id="Seg_7915" s="T663">one.[NOM.SG]</ta>
            <ta e="T665" id="Seg_7916" s="T664">catch.up-PRS.[3SG]</ta>
            <ta e="T666" id="Seg_7917" s="T665">this</ta>
            <ta e="T667" id="Seg_7918" s="T666">foot-NOM/GEN/ACC.3PL</ta>
            <ta e="T669" id="Seg_7919" s="T668">hit-MULT-EP-IMP.2SG</ta>
            <ta e="T670" id="Seg_7920" s="T669">I.ACC</ta>
            <ta e="T671" id="Seg_7921" s="T670">beat-EP-IMP.2SG</ta>
            <ta e="T672" id="Seg_7922" s="T671">I.ACC</ta>
            <ta e="T673" id="Seg_7923" s="T672">climb-IMP.2SG</ta>
            <ta e="T674" id="Seg_7924" s="T673">I.LAT</ta>
            <ta e="T675" id="Seg_7925" s="T674">be-PRS.[3SG]</ta>
            <ta e="T676" id="Seg_7926" s="T675">I.LAT</ta>
            <ta e="T677" id="Seg_7927" s="T676">this</ta>
            <ta e="T678" id="Seg_7928" s="T677">pine.nut.[NOM.SG]</ta>
            <ta e="T680" id="Seg_7929" s="T679">hang.up-DUR.[3SG]</ta>
            <ta e="T681" id="Seg_7930" s="T680">PTCL</ta>
            <ta e="T682" id="Seg_7931" s="T681">and</ta>
            <ta e="T683" id="Seg_7932" s="T682">people.[NOM.SG]</ta>
            <ta e="T684" id="Seg_7933" s="T683">PTCL</ta>
            <ta e="T685" id="Seg_7934" s="T684">grab-DUR-3PL</ta>
            <ta e="T686" id="Seg_7935" s="T685">wash-DRV-DUR-3PL</ta>
            <ta e="T687" id="Seg_7936" s="T686">rub-DUR-3PL</ta>
            <ta e="T688" id="Seg_7937" s="T687">this</ta>
            <ta e="T689" id="Seg_7938" s="T688">towel.[NOM.SG]</ta>
            <ta e="T690" id="Seg_7939" s="T689">be-PRS.[3SG]</ta>
            <ta e="T692" id="Seg_7940" s="T888">tree-PL</ta>
            <ta e="T693" id="Seg_7941" s="T692">grass.[NOM.SG]</ta>
            <ta e="T694" id="Seg_7942" s="T693">grow-DUR.[3SG]</ta>
            <ta e="T697" id="Seg_7943" s="T696">this</ta>
            <ta e="T698" id="Seg_7944" s="T697">finger.[NOM.SG]</ta>
            <ta e="T700" id="Seg_7945" s="T699">red.[NOM.SG]</ta>
            <ta e="T701" id="Seg_7946" s="T700">goat.[NOM.SG]</ta>
            <ta e="T702" id="Seg_7947" s="T701">lie-DUR-PST.[3SG]</ta>
            <ta e="T703" id="Seg_7948" s="T702">there</ta>
            <ta e="T704" id="Seg_7949" s="T703">grass.[NOM.SG]</ta>
            <ta e="T705" id="Seg_7950" s="T704">NEG</ta>
            <ta e="T706" id="Seg_7951" s="T705">grow-DUR.[3SG]</ta>
            <ta e="T707" id="Seg_7952" s="T706">fire.[NOM.SG]</ta>
            <ta e="T708" id="Seg_7953" s="T707">this.[NOM.SG]</ta>
            <ta e="T710" id="Seg_7954" s="T709">go-PST.[3SG]</ta>
            <ta e="T711" id="Seg_7955" s="T710">berry-VBLZ-CVB</ta>
            <ta e="T712" id="Seg_7956" s="T711">and</ta>
            <ta e="T713" id="Seg_7957" s="T712">I.NOM</ta>
            <ta e="T714" id="Seg_7958" s="T713">go-PST-1SG</ta>
            <ta e="T715" id="Seg_7959" s="T714">berry.[NOM.SG]</ta>
            <ta e="T716" id="Seg_7960" s="T715">tear-INF.LAT</ta>
            <ta e="T717" id="Seg_7961" s="T716">and</ta>
            <ta e="T718" id="Seg_7962" s="T717">more</ta>
            <ta e="T719" id="Seg_7963" s="T718">one.[NOM.SG]</ta>
            <ta e="T720" id="Seg_7964" s="T719">girl.[NOM.SG]</ta>
            <ta e="T721" id="Seg_7965" s="T720">go-PST.[3SG]</ta>
            <ta e="T724" id="Seg_7966" s="T723">horse-INS</ta>
            <ta e="T727" id="Seg_7967" s="T726">go-PST-1PL</ta>
            <ta e="T728" id="Seg_7968" s="T727">%%-LAT</ta>
            <ta e="T729" id="Seg_7969" s="T728">there</ta>
            <ta e="T730" id="Seg_7970" s="T729">come-PST-1PL</ta>
            <ta e="T731" id="Seg_7971" s="T730">hide-FUT-3PL</ta>
            <ta e="T732" id="Seg_7972" s="T731">berry.[NOM.SG]</ta>
            <ta e="T733" id="Seg_7973" s="T732">look-FRQ-PST-1PL</ta>
            <ta e="T734" id="Seg_7974" s="T733">and</ta>
            <ta e="T735" id="Seg_7975" s="T734">a.few</ta>
            <ta e="T736" id="Seg_7976" s="T735">collect-PST-1PL</ta>
            <ta e="T737" id="Seg_7977" s="T736">then</ta>
            <ta e="T738" id="Seg_7978" s="T737">come-PST-1PL</ta>
            <ta e="T739" id="Seg_7979" s="T738">where</ta>
            <ta e="T740" id="Seg_7980" s="T739">horse.[NOM.SG]</ta>
            <ta e="T741" id="Seg_7981" s="T740">go-DUR.[3SG]</ta>
            <ta e="T742" id="Seg_7982" s="T741">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T743" id="Seg_7983" s="T742">say-IPFVZ.[3SG]</ta>
            <ta e="T744" id="Seg_7984" s="T743">very</ta>
            <ta e="T745" id="Seg_7985" s="T744">black.[NOM.SG]</ta>
            <ta e="T747" id="Seg_7986" s="T746">come-PRS.[3SG]</ta>
            <ta e="T748" id="Seg_7987" s="T747">one.should</ta>
            <ta e="T749" id="Seg_7988" s="T748">make-INF.LAT</ta>
            <ta e="T750" id="Seg_7989" s="T749">tent.[NOM.SG]</ta>
            <ta e="T751" id="Seg_7990" s="T750">otherwise</ta>
            <ta e="T752" id="Seg_7991" s="T751">sleep-INF.LAT</ta>
            <ta e="T753" id="Seg_7992" s="T752">NEG</ta>
            <ta e="T754" id="Seg_7993" s="T753">good</ta>
            <ta e="T755" id="Seg_7994" s="T754">become-FUT-3SG</ta>
            <ta e="T756" id="Seg_7995" s="T755">then</ta>
            <ta e="T757" id="Seg_7996" s="T756">tent.[NOM.SG]</ta>
            <ta e="T758" id="Seg_7997" s="T757">make-PST-1PL</ta>
            <ta e="T759" id="Seg_7998" s="T758">boil-PST-1PL</ta>
            <ta e="T760" id="Seg_7999" s="T759">yellow.[NOM.SG]</ta>
            <ta e="T761" id="Seg_8000" s="T760">water.[NOM.SG]</ta>
            <ta e="T762" id="Seg_8001" s="T761">drink-PST-1PL</ta>
            <ta e="T763" id="Seg_8002" s="T762">and</ta>
            <ta e="T764" id="Seg_8003" s="T763">there</ta>
            <ta e="T766" id="Seg_8004" s="T765">be-PST.[3SG]</ta>
            <ta e="T767" id="Seg_8005" s="T766">mat.[NOM.SG]</ta>
            <ta e="T768" id="Seg_8006" s="T767">there</ta>
            <ta e="T769" id="Seg_8007" s="T768">mat-NOM/GEN/ACC.3SG</ta>
            <ta e="T770" id="Seg_8008" s="T769">%%</ta>
            <ta e="T771" id="Seg_8009" s="T770">tree-3PL-LAT</ta>
            <ta e="T772" id="Seg_8010" s="T771">then</ta>
            <ta e="T773" id="Seg_8011" s="T772">lie.down-1PL</ta>
            <ta e="T774" id="Seg_8012" s="T773">sleep-INF.LAT</ta>
            <ta e="T775" id="Seg_8013" s="T774">and</ta>
            <ta e="T776" id="Seg_8014" s="T775">evening-LOC.ADV</ta>
            <ta e="T777" id="Seg_8015" s="T776">PTCL</ta>
            <ta e="T778" id="Seg_8016" s="T777">very</ta>
            <ta e="T779" id="Seg_8017" s="T778">strongly</ta>
            <ta e="T780" id="Seg_8018" s="T779">rain.[NOM.SG]</ta>
            <ta e="T781" id="Seg_8019" s="T780">come-PST.[3SG]</ta>
            <ta e="T782" id="Seg_8020" s="T781">come-PST.[3SG]</ta>
            <ta e="T783" id="Seg_8021" s="T782">morning-LOC.ADV</ta>
            <ta e="T784" id="Seg_8022" s="T783">get.up-PST-1PL</ta>
            <ta e="T785" id="Seg_8023" s="T784">PTCL</ta>
            <ta e="T786" id="Seg_8024" s="T785">wet-PST-1PL</ta>
            <ta e="T787" id="Seg_8025" s="T786">horse-PL</ta>
            <ta e="T788" id="Seg_8026" s="T787">capture-PST-1PL</ta>
            <ta e="T789" id="Seg_8027" s="T788">go-PST-1PL</ta>
            <ta e="T790" id="Seg_8028" s="T789">tent-LAT/LOC.1SG</ta>
            <ta e="T792" id="Seg_8029" s="T791">come-PST-1PL</ta>
            <ta e="T793" id="Seg_8030" s="T792">water-LAT</ta>
            <ta e="T794" id="Seg_8031" s="T793">how=INDEF</ta>
            <ta e="T795" id="Seg_8032" s="T794">one.cannot</ta>
            <ta e="T796" id="Seg_8033" s="T795">go-INF.LAT</ta>
            <ta e="T797" id="Seg_8034" s="T796">then</ta>
            <ta e="T800" id="Seg_8035" s="T799">water-LAT</ta>
            <ta e="T801" id="Seg_8036" s="T800">go-PST-1PL</ta>
            <ta e="T802" id="Seg_8037" s="T801">go-PST-1PL</ta>
            <ta e="T803" id="Seg_8038" s="T802">then</ta>
            <ta e="T804" id="Seg_8039" s="T803">find-PST-1PL</ta>
            <ta e="T805" id="Seg_8040" s="T804">bridge.[NOM.SG]</ta>
            <ta e="T806" id="Seg_8041" s="T805">bridge.[NOM.SG]</ta>
            <ta e="T808" id="Seg_8042" s="T807">come-PST-1PL</ta>
            <ta e="T809" id="Seg_8043" s="T808">and</ta>
            <ta e="T810" id="Seg_8044" s="T809">tent-LAT/LOC.1SG</ta>
            <ta e="T811" id="Seg_8045" s="T810">come-PST-1PL</ta>
            <ta e="T814" id="Seg_8046" s="T813">I.NOM</ta>
            <ta e="T815" id="Seg_8047" s="T814">today</ta>
            <ta e="T816" id="Seg_8048" s="T815">morning</ta>
            <ta e="T817" id="Seg_8049" s="T816">get.up-PST-1SG</ta>
            <ta e="T818" id="Seg_8050" s="T817">shop-LAT</ta>
            <ta e="T819" id="Seg_8051" s="T818">go-PST-1SG</ta>
            <ta e="T821" id="Seg_8052" s="T889">stand-PST-1SG</ta>
            <ta e="T822" id="Seg_8053" s="T821">then</ta>
            <ta e="T823" id="Seg_8054" s="T822">two.[NOM.SG]</ta>
            <ta e="T824" id="Seg_8055" s="T823">two.[NOM.SG]</ta>
            <ta e="T825" id="Seg_8056" s="T824">hour.[NOM.SG]</ta>
            <ta e="T826" id="Seg_8057" s="T825">stand-PST-1SG</ta>
            <ta e="T827" id="Seg_8058" s="T826">then</ta>
            <ta e="T828" id="Seg_8059" s="T827">take-PST-1SG</ta>
            <ta e="T829" id="Seg_8060" s="T828">fish.[NOM.SG]</ta>
            <ta e="T830" id="Seg_8061" s="T829">and</ta>
            <ta e="T831" id="Seg_8062" s="T830">come-PST-1SG</ta>
            <ta e="T832" id="Seg_8063" s="T831">tent-LAT</ta>
            <ta e="T833" id="Seg_8064" s="T832">and</ta>
            <ta e="T834" id="Seg_8065" s="T833">you.PL.NOM</ta>
            <ta e="T835" id="Seg_8066" s="T834">come-PST-2PL</ta>
            <ta e="T837" id="Seg_8067" s="T836">then</ta>
            <ta e="T838" id="Seg_8068" s="T837">I.NOM</ta>
            <ta e="T839" id="Seg_8069" s="T838">take-PST-1SG</ta>
            <ta e="T840" id="Seg_8070" s="T839">one.[NOM.SG]</ta>
            <ta e="T841" id="Seg_8071" s="T840">kilogramm.[NOM.SG]</ta>
            <ta e="T842" id="Seg_8072" s="T841">money.[NOM.SG]</ta>
            <ta e="T843" id="Seg_8073" s="T842">give-PST-1SG</ta>
            <ta e="T844" id="Seg_8074" s="T843">one.[NOM.SG]</ta>
            <ta e="T845" id="Seg_8075" s="T844">rouble.[NOM.SG]</ta>
            <ta e="T846" id="Seg_8076" s="T845">ten.[NOM.SG]</ta>
            <ta e="T847" id="Seg_8077" s="T846">four.[NOM.SG]</ta>
            <ta e="T848" id="Seg_8078" s="T847">give-PST-1SG</ta>
            <ta e="T849" id="Seg_8079" s="T848">one.[NOM.SG]</ta>
            <ta e="T850" id="Seg_8080" s="T849">rouble.[NOM.SG]</ta>
            <ta e="T851" id="Seg_8081" s="T850">ten.[NOM.SG]</ta>
            <ta e="T852" id="Seg_8082" s="T851">four.[NOM.SG]</ta>
            <ta e="T854" id="Seg_8083" s="T853">there</ta>
            <ta e="T855" id="Seg_8084" s="T854">PTCL</ta>
            <ta e="T856" id="Seg_8085" s="T855">woman-PL</ta>
            <ta e="T857" id="Seg_8086" s="T856">scold-DES-DUR-3PL</ta>
            <ta e="T858" id="Seg_8087" s="T857">shout-DUR-3PL</ta>
            <ta e="T859" id="Seg_8088" s="T858">say-PRS-3PL</ta>
            <ta e="T860" id="Seg_8089" s="T859">few</ta>
            <ta e="T861" id="Seg_8090" s="T860">fish.[NOM.SG]</ta>
            <ta e="T862" id="Seg_8091" s="T861">one.[NOM.SG]</ta>
            <ta e="T864" id="Seg_8092" s="T863">one.[NOM.SG]</ta>
            <ta e="T865" id="Seg_8093" s="T864">kilogramm.[NOM.SG]</ta>
            <ta e="T866" id="Seg_8094" s="T865">give-DUR.[3SG]</ta>
            <ta e="T867" id="Seg_8095" s="T866">this</ta>
            <ta e="T868" id="Seg_8096" s="T867">give-PST.[3SG]</ta>
            <ta e="T869" id="Seg_8097" s="T868">one.[NOM.SG]</ta>
            <ta e="T870" id="Seg_8098" s="T869">PTCL</ta>
            <ta e="T871" id="Seg_8099" s="T870">woman-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T872" id="Seg_8100" s="T871">one.[NOM.SG]</ta>
            <ta e="T873" id="Seg_8101" s="T872">kilogramm.[NOM.SG]</ta>
            <ta e="T874" id="Seg_8102" s="T873">give-PST.[3SG]</ta>
            <ta e="T875" id="Seg_8103" s="T874">say-PRS-3PL</ta>
            <ta e="T876" id="Seg_8104" s="T875">child-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T877" id="Seg_8105" s="T876">shout-DUR-3PL</ta>
            <ta e="T878" id="Seg_8106" s="T877">fish.[NOM.SG]</ta>
            <ta e="T879" id="Seg_8107" s="T878">fish.[NOM.SG]</ta>
            <ta e="T880" id="Seg_8108" s="T879">eat-INF.LAT</ta>
            <ta e="T881" id="Seg_8109" s="T880">one.needs</ta>
            <ta e="T882" id="Seg_8110" s="T881">this-PL-LAT</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T1" id="Seg_8111" s="T0">тогда</ta>
            <ta e="T2" id="Seg_8112" s="T1">этот.[NOM.SG]</ta>
            <ta e="T3" id="Seg_8113" s="T2">мальчик-PL</ta>
            <ta e="T4" id="Seg_8114" s="T3">дорога-LAT</ta>
            <ta e="T5" id="Seg_8115" s="T4">пойти-PST-3PL</ta>
            <ta e="T6" id="Seg_8116" s="T5">прийти-PST-3PL</ta>
            <ta e="T7" id="Seg_8117" s="T6">прийти-PST-3PL</ta>
            <ta e="T8" id="Seg_8118" s="T7">тогда</ta>
            <ta e="T9" id="Seg_8119" s="T8">дорога.[NOM.SG]</ta>
            <ta e="T10" id="Seg_8120" s="T9">дорога-LOC</ta>
            <ta e="T11" id="Seg_8121" s="T10">край-LAT/LOC.3SG</ta>
            <ta e="T12" id="Seg_8122" s="T11">сесть-PST-3PL</ta>
            <ta e="T13" id="Seg_8123" s="T12">видеть-PRS-3PL</ta>
            <ta e="T14" id="Seg_8124" s="T13">прийти-PRS.[3SG]</ta>
            <ta e="T15" id="Seg_8125" s="T14">колдун.[NOM.SG]</ta>
            <ta e="T16" id="Seg_8126" s="T15">один.[NOM.SG]</ta>
            <ta e="T17" id="Seg_8127" s="T16">лошадь.[NOM.SG]</ta>
            <ta e="T18" id="Seg_8128" s="T17">красный.[NOM.SG]</ta>
            <ta e="T19" id="Seg_8129" s="T18">тогда</ta>
            <ta e="T20" id="Seg_8130" s="T19">этот.[NOM.SG]</ta>
            <ta e="T21" id="Seg_8131" s="T20">PTCL</ta>
            <ta e="T23" id="Seg_8132" s="T21">сильно</ta>
            <ta e="T24" id="Seg_8133" s="T23">тогда</ta>
            <ta e="T25" id="Seg_8134" s="T24">один.[NOM.SG]</ta>
            <ta e="T27" id="Seg_8135" s="T26">сильно</ta>
            <ta e="T28" id="Seg_8136" s="T27">лошадь-NOM/GEN.3SG</ta>
            <ta e="T29" id="Seg_8137" s="T28">PTCL</ta>
            <ta e="T31" id="Seg_8138" s="T30">коленка-LAT</ta>
            <ta e="T33" id="Seg_8139" s="T32">этот.[NOM.SG]</ta>
            <ta e="T34" id="Seg_8140" s="T33">лошадь.[NOM.SG]</ta>
            <ta e="T35" id="Seg_8141" s="T34">упасть-MOM-PST.[3SG]</ta>
            <ta e="T36" id="Seg_8142" s="T35">коленка-PL-LAT</ta>
            <ta e="T37" id="Seg_8143" s="T36">тогда</ta>
            <ta e="T38" id="Seg_8144" s="T37">этот.[NOM.SG]</ta>
            <ta e="T39" id="Seg_8145" s="T38">колдун.[NOM.SG]</ta>
            <ta e="T40" id="Seg_8146" s="T39">сказать-IPFVZ-PST.[3SG]</ta>
            <ta e="T41" id="Seg_8147" s="T40">этот</ta>
            <ta e="T42" id="Seg_8148" s="T41">там</ta>
            <ta e="T44" id="Seg_8149" s="T43">черт.[NOM.SG]</ta>
            <ta e="T45" id="Seg_8150" s="T44">сидеть-DUR.[3SG]</ta>
            <ta e="T46" id="Seg_8151" s="T45">очень</ta>
            <ta e="T47" id="Seg_8152" s="T46">бояться-PRS-1SG</ta>
            <ta e="T48" id="Seg_8153" s="T47">тогда</ta>
            <ta e="T49" id="Seg_8154" s="T48">пойти-PST.[3SG]</ta>
            <ta e="T50" id="Seg_8155" s="T49">сидеть-DUR-3PL</ta>
            <ta e="T51" id="Seg_8156" s="T50">сидеть-DUR-3PL</ta>
            <ta e="T52" id="Seg_8157" s="T51">опять</ta>
            <ta e="T53" id="Seg_8158" s="T52">прийти-PRS.[3SG]</ta>
            <ta e="T54" id="Seg_8159" s="T53">черный.[NOM.SG]</ta>
            <ta e="T55" id="Seg_8160" s="T54">лошадь-3SG-INS</ta>
            <ta e="T57" id="Seg_8161" s="T56">другой.[NOM.SG]</ta>
            <ta e="T58" id="Seg_8162" s="T57">два.[NOM.SG]</ta>
            <ta e="T59" id="Seg_8163" s="T58">два-ORD.[NOM.SG]</ta>
            <ta e="T60" id="Seg_8164" s="T59">колдун.[NOM.SG]</ta>
            <ta e="T61" id="Seg_8165" s="T60">тогда</ta>
            <ta e="T62" id="Seg_8166" s="T61">этот-PL</ta>
            <ta e="T63" id="Seg_8167" s="T62">опять</ta>
            <ta e="T65" id="Seg_8168" s="T64">тогда</ta>
            <ta e="T66" id="Seg_8169" s="T65">этот.[NOM.SG]</ta>
            <ta e="T67" id="Seg_8170" s="T66">упасть-MOM-PST.[3SG]</ta>
            <ta e="T68" id="Seg_8171" s="T67">коленка-PL-LAT</ta>
            <ta e="T69" id="Seg_8172" s="T68">этот</ta>
            <ta e="T70" id="Seg_8173" s="T69">тоже</ta>
            <ta e="T71" id="Seg_8174" s="T70">сказать-IPFVZ.[3SG]</ta>
            <ta e="T72" id="Seg_8175" s="T71">черт.[NOM.SG]</ta>
            <ta e="T73" id="Seg_8176" s="T72">очень</ta>
            <ta e="T74" id="Seg_8177" s="T73">сильный.[NOM.SG]</ta>
            <ta e="T75" id="Seg_8178" s="T74">очень</ta>
            <ta e="T76" id="Seg_8179" s="T75">я.NOM</ta>
            <ta e="T77" id="Seg_8180" s="T76">бояться-PRS-1SG</ta>
            <ta e="T78" id="Seg_8181" s="T77">тогда</ta>
            <ta e="T80" id="Seg_8182" s="T79">лошадь.[NOM.SG]</ta>
            <ta e="T81" id="Seg_8183" s="T80">лошадь-NOM/GEN.3SG</ta>
            <ta e="T82" id="Seg_8184" s="T81">встать-PST.[3SG]</ta>
            <ta e="T83" id="Seg_8185" s="T82">пойти-PST.[3SG]</ta>
            <ta e="T84" id="Seg_8186" s="T83">тогда</ta>
            <ta e="T85" id="Seg_8187" s="T84">сидеть-DUR-3PL</ta>
            <ta e="T86" id="Seg_8188" s="T85">сидеть-DUR-3PL</ta>
            <ta e="T88" id="Seg_8189" s="T87">три-ORD.[NOM.SG]</ta>
            <ta e="T89" id="Seg_8190" s="T88">лошадь.[NOM.SG]</ta>
            <ta e="T90" id="Seg_8191" s="T89">прийти-PRS.[3SG]</ta>
            <ta e="T91" id="Seg_8192" s="T90">тот.[NOM.SG]</ta>
            <ta e="T92" id="Seg_8193" s="T91">лошадь.[NOM.SG]</ta>
            <ta e="T93" id="Seg_8194" s="T92">белый.[NOM.SG]</ta>
            <ta e="T94" id="Seg_8195" s="T93">этот-PL</ta>
            <ta e="T95" id="Seg_8196" s="T94">опять</ta>
            <ta e="T97" id="Seg_8197" s="T96">тогда</ta>
            <ta e="T98" id="Seg_8198" s="T97">этот.[NOM.SG]</ta>
            <ta e="T99" id="Seg_8199" s="T98">лошадь.[NOM.SG]</ta>
            <ta e="T100" id="Seg_8200" s="T99">поставить-PST.[3SG]</ta>
            <ta e="T101" id="Seg_8201" s="T100">стоять-DUR.[3SG]</ta>
            <ta e="T102" id="Seg_8202" s="T101">этот</ta>
            <ta e="T103" id="Seg_8203" s="T102">колдун.[NOM.SG]</ta>
            <ta e="T105" id="Seg_8204" s="T104">пойти-PST.[3SG]</ta>
            <ta e="T106" id="Seg_8205" s="T105">этот-PL-LAT</ta>
            <ta e="T107" id="Seg_8206" s="T106">какой</ta>
            <ta e="T108" id="Seg_8207" s="T107">ребенок-PL</ta>
            <ta e="T109" id="Seg_8208" s="T108">сидеть-DUR-3PL</ta>
            <ta e="T110" id="Seg_8209" s="T109">да</ta>
            <ta e="T111" id="Seg_8210" s="T110">мы.NOM</ta>
            <ta e="T112" id="Seg_8211" s="T111">вот</ta>
            <ta e="T113" id="Seg_8212" s="T112">мы.ACC</ta>
            <ta e="T114" id="Seg_8213" s="T113">PTCL</ta>
            <ta e="T115" id="Seg_8214" s="T114">вода-LAT</ta>
            <ta e="T116" id="Seg_8215" s="T115">выбросить-MOM-PST-3PL</ta>
            <ta e="T117" id="Seg_8216" s="T116">я.NOM</ta>
            <ta e="T118" id="Seg_8217" s="T117">мы.NOM</ta>
            <ta e="T119" id="Seg_8218" s="T118">здесь</ta>
            <ta e="T120" id="Seg_8219" s="T119">прийти-PST-1PL</ta>
            <ta e="T121" id="Seg_8220" s="T120">тогда</ta>
            <ta e="T122" id="Seg_8221" s="T121">ну</ta>
            <ta e="T123" id="Seg_8222" s="T122">жить-IMP.2PL</ta>
            <ta e="T124" id="Seg_8223" s="T123">здесь</ta>
            <ta e="T125" id="Seg_8224" s="T124">этот</ta>
            <ta e="T126" id="Seg_8225" s="T125">этот-LAT</ta>
            <ta e="T127" id="Seg_8226" s="T126">делать-PST-3PL</ta>
            <ta e="T129" id="Seg_8227" s="T128">делать-PST.[3SG]</ta>
            <ta e="T130" id="Seg_8228" s="T129">чум.[NOM.SG]</ta>
            <ta e="T131" id="Seg_8229" s="T130">хлеб.[NOM.SG]</ta>
            <ta e="T132" id="Seg_8230" s="T131">дать-PST.[3SG]</ta>
            <ta e="T133" id="Seg_8231" s="T132">мясо.[NOM.SG]</ta>
            <ta e="T134" id="Seg_8232" s="T133">дать-PST.[3SG]</ta>
            <ta e="T135" id="Seg_8233" s="T134">жить-IMP.2PL</ta>
            <ta e="T136" id="Seg_8234" s="T135">здесь</ta>
            <ta e="T137" id="Seg_8235" s="T136">я.NOM</ta>
            <ta e="T138" id="Seg_8236" s="T137">вернуться-FUT-1SG</ta>
            <ta e="T139" id="Seg_8237" s="T138">живой.[NOM.SG]</ta>
            <ta e="T140" id="Seg_8238" s="T139">стать-FUT-1SG</ta>
            <ta e="T141" id="Seg_8239" s="T140">вы.ACC</ta>
            <ta e="T142" id="Seg_8240" s="T141">взять-FUT-1SG</ta>
            <ta e="T143" id="Seg_8241" s="T142">тогда</ta>
            <ta e="T144" id="Seg_8242" s="T143">этот-PL</ta>
            <ta e="T145" id="Seg_8243" s="T144">жить-DUR-PST-3PL</ta>
            <ta e="T146" id="Seg_8244" s="T145">жить-DUR-PST</ta>
            <ta e="T147" id="Seg_8245" s="T146">три.[NOM.SG]</ta>
            <ta e="T148" id="Seg_8246" s="T147">день.[NOM.SG]</ta>
            <ta e="T149" id="Seg_8247" s="T148">один.[NOM.SG]</ta>
            <ta e="T150" id="Seg_8248" s="T149">лошадь.[NOM.SG]</ta>
            <ta e="T151" id="Seg_8249" s="T150">прийти-PRS.[3SG]</ta>
            <ta e="T152" id="Seg_8250" s="T151">красный.[NOM.SG]</ta>
            <ta e="T153" id="Seg_8251" s="T152">тогда</ta>
            <ta e="T154" id="Seg_8252" s="T153">NEG.EX.[3SG]</ta>
            <ta e="T155" id="Seg_8253" s="T154">там</ta>
            <ta e="T156" id="Seg_8254" s="T155">мужчина.[NOM.SG]</ta>
            <ta e="T157" id="Seg_8255" s="T156">тогда</ta>
            <ta e="T158" id="Seg_8256" s="T157">черный.[NOM.SG]</ta>
            <ta e="T159" id="Seg_8257" s="T158">лошадь.[NOM.SG]</ta>
            <ta e="T160" id="Seg_8258" s="T159">прийти-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_8259" s="T160">мужчина.[NOM.SG]</ta>
            <ta e="T162" id="Seg_8260" s="T161">тоже</ta>
            <ta e="T163" id="Seg_8261" s="T162">NEG.EX.[3SG]</ta>
            <ta e="T164" id="Seg_8262" s="T163">жить-DUR-3PL</ta>
            <ta e="T165" id="Seg_8263" s="T164">жить-DUR-3PL</ta>
            <ta e="T166" id="Seg_8264" s="T165">тогда</ta>
            <ta e="T167" id="Seg_8265" s="T166">снег.[NOM.SG]</ta>
            <ta e="T168" id="Seg_8266" s="T167">лошадь.[NOM.SG]</ta>
            <ta e="T169" id="Seg_8267" s="T168">прийти-PRS.[3SG]</ta>
            <ta e="T170" id="Seg_8268" s="T169">и</ta>
            <ta e="T171" id="Seg_8269" s="T170">мужчина.[NOM.SG]</ta>
            <ta e="T172" id="Seg_8270" s="T171">сидеть-DUR.[3SG]</ta>
            <ta e="T173" id="Seg_8271" s="T172">ну</ta>
            <ta e="T174" id="Seg_8272" s="T173">пойти-OPT.DU/PL-1PL</ta>
            <ta e="T175" id="Seg_8273" s="T174">я-COM</ta>
            <ta e="T176" id="Seg_8274" s="T175">чум-LAT-2SG</ta>
            <ta e="T177" id="Seg_8275" s="T176">может.быть</ta>
            <ta e="T178" id="Seg_8276" s="T177">пойти-FUT-2SG</ta>
            <ta e="T179" id="Seg_8277" s="T178">или</ta>
            <ta e="T180" id="Seg_8278" s="T179">я.LAT</ta>
            <ta e="T181" id="Seg_8279" s="T180">жить-FUT-2SG</ta>
            <ta e="T182" id="Seg_8280" s="T181">прийти-PST-3PL</ta>
            <ta e="T183" id="Seg_8281" s="T182">этот.[NOM.SG]</ta>
            <ta e="T184" id="Seg_8282" s="T183">мужчина-LAT</ta>
            <ta e="T185" id="Seg_8283" s="T184">один.[NOM.SG]</ta>
            <ta e="T186" id="Seg_8284" s="T185">пойти-PST.[3SG]</ta>
            <ta e="T187" id="Seg_8285" s="T186">чум-LAT/LOC.3SG</ta>
            <ta e="T188" id="Seg_8286" s="T187">прийти-PST.[3SG]</ta>
            <ta e="T189" id="Seg_8287" s="T188">чум-LAT/LOC.3SG</ta>
            <ta e="T190" id="Seg_8288" s="T189">NEG.EX.[3SG]</ta>
            <ta e="T192" id="Seg_8289" s="T191">люди.[NOM.SG]</ta>
            <ta e="T193" id="Seg_8290" s="T192">кто.[NOM.SG]=INDEF</ta>
            <ta e="T194" id="Seg_8291" s="T193">NEG.EX.[3SG]</ta>
            <ta e="T195" id="Seg_8292" s="T194">один.[NOM.SG]</ta>
            <ta e="T196" id="Seg_8293" s="T195">один.[NOM.SG]</ta>
            <ta e="T198" id="Seg_8294" s="T197">один.[NOM.SG]</ta>
            <ta e="T199" id="Seg_8295" s="T198">чум.[NOM.SG]</ta>
            <ta e="T200" id="Seg_8296" s="T199">стоять-PRS.[3SG]</ta>
            <ta e="T201" id="Seg_8297" s="T200">этот</ta>
            <ta e="T202" id="Seg_8298" s="T201">прийти-PST.[3SG]</ta>
            <ta e="T205" id="Seg_8299" s="T204">куда.идти-PST.[3SG]</ta>
            <ta e="T206" id="Seg_8300" s="T205">PTCL</ta>
            <ta e="T207" id="Seg_8301" s="T206">люди.[NOM.SG]</ta>
            <ta e="T208" id="Seg_8302" s="T207">и</ta>
            <ta e="T209" id="Seg_8303" s="T208">мы.NOM</ta>
            <ta e="T210" id="Seg_8304" s="T209">дочь-NOM/GEN/ACC.1PL</ta>
            <ta e="T211" id="Seg_8305" s="T210">PTCL</ta>
            <ta e="T212" id="Seg_8306" s="T211">два.[NOM.SG]</ta>
            <ta e="T213" id="Seg_8307" s="T212">сын.[NOM.SG]</ta>
            <ta e="T214" id="Seg_8308" s="T213">быть-PST-3PL</ta>
            <ta e="T215" id="Seg_8309" s="T214">этот-PL</ta>
            <ta e="T216" id="Seg_8310" s="T215">вода-LAT</ta>
            <ta e="T217" id="Seg_8311" s="T216">пускать-MOM-PST-3PL</ta>
            <ta e="T218" id="Seg_8312" s="T217">а</ta>
            <ta e="T219" id="Seg_8313" s="T218">мы.NOM</ta>
            <ta e="T220" id="Seg_8314" s="T219">дочь-NOM/GEN/ACC.1PL</ta>
            <ta e="T221" id="Seg_8315" s="T220">PTCL</ta>
            <ta e="T222" id="Seg_8316" s="T221">люди-ACC</ta>
            <ta e="T223" id="Seg_8317" s="T222">зарезать-MOM-PST.[3SG]</ta>
            <ta e="T224" id="Seg_8318" s="T223">а</ta>
            <ta e="T225" id="Seg_8319" s="T224">где</ta>
            <ta e="T226" id="Seg_8320" s="T225">этот.[NOM.SG]</ta>
            <ta e="T227" id="Seg_8321" s="T226">куда=INDEF</ta>
            <ta e="T384" id="Seg_8322" s="T227">пойти-CVB</ta>
            <ta e="T228" id="Seg_8323" s="T384">исчезнуть-PST.[3SG]</ta>
            <ta e="T229" id="Seg_8324" s="T228">NEG</ta>
            <ta e="T230" id="Seg_8325" s="T229">знать-1PL</ta>
            <ta e="T231" id="Seg_8326" s="T230">тогда</ta>
            <ta e="T232" id="Seg_8327" s="T231">этот.[NOM.SG]</ta>
            <ta e="T233" id="Seg_8328" s="T232">лошадь.[NOM.SG]</ta>
            <ta e="T234" id="Seg_8329" s="T233">взять-PST.[3SG]</ta>
            <ta e="T235" id="Seg_8330" s="T234">пойти-PST.[3SG]</ta>
            <ta e="T236" id="Seg_8331" s="T235">проверить-INF.LAT</ta>
            <ta e="T239" id="Seg_8332" s="T238">видеть-RES-PST.[3SG]</ta>
            <ta e="T240" id="Seg_8333" s="T239">видеть-PST.[3SG]</ta>
            <ta e="T241" id="Seg_8334" s="T240">видеть-PST-3PL</ta>
            <ta e="T242" id="Seg_8335" s="T241">там</ta>
            <ta e="T243" id="Seg_8336" s="T242">тогда</ta>
            <ta e="T244" id="Seg_8337" s="T243">INCH</ta>
            <ta e="T245" id="Seg_8338" s="T244">этот-COM</ta>
            <ta e="T246" id="Seg_8339" s="T245">PTCL</ta>
            <ta e="T247" id="Seg_8340" s="T246">бороться-INF.LAT</ta>
            <ta e="T248" id="Seg_8341" s="T247">этот</ta>
            <ta e="T249" id="Seg_8342" s="T248">хотеть.PST.M.SG</ta>
            <ta e="T250" id="Seg_8343" s="T249">этот-ACC</ta>
            <ta e="T252" id="Seg_8344" s="T251">резать-INF.LAT</ta>
            <ta e="T253" id="Seg_8345" s="T252">нож-INS</ta>
            <ta e="T254" id="Seg_8346" s="T253">этот</ta>
            <ta e="T255" id="Seg_8347" s="T254">PTCL</ta>
            <ta e="T256" id="Seg_8348" s="T255">сказать-IPFVZ.[3SG]</ta>
            <ta e="T257" id="Seg_8349" s="T256">NEG.AUX-IMP.2SG</ta>
            <ta e="T258" id="Seg_8350" s="T257">резать-EP-CNG</ta>
            <ta e="T259" id="Seg_8351" s="T258">я.NOM</ta>
            <ta e="T260" id="Seg_8352" s="T259">ты.NOM</ta>
            <ta e="T261" id="Seg_8353" s="T260">брат-NOM/GEN/ACC.1SG</ta>
            <ta e="T262" id="Seg_8354" s="T261">быть-PRS-1SG</ta>
            <ta e="T263" id="Seg_8355" s="T262">курица-PL</ta>
            <ta e="T264" id="Seg_8356" s="T263">говорить-DUR-3PL</ta>
            <ta e="T265" id="Seg_8357" s="T264">капалуха.[NOM.SG]</ta>
            <ta e="T266" id="Seg_8358" s="T265">и</ta>
            <ta e="T269" id="Seg_8359" s="T267">куропатка.[NOM.SG]</ta>
            <ta e="T270" id="Seg_8360" s="T269">куропатка.[NOM.SG]</ta>
            <ta e="T271" id="Seg_8361" s="T270">сказать-PRS.[3SG]</ta>
            <ta e="T272" id="Seg_8362" s="T271">капалуха-LAT</ta>
            <ta e="T273" id="Seg_8363" s="T272">ты.NOM</ta>
            <ta e="T274" id="Seg_8364" s="T273">NEG</ta>
            <ta e="T275" id="Seg_8365" s="T274">красивый.[NOM.SG]</ta>
            <ta e="T276" id="Seg_8366" s="T275">какой</ta>
            <ta e="T277" id="Seg_8367" s="T276">штаны-PL</ta>
            <ta e="T278" id="Seg_8368" s="T277">NEG</ta>
            <ta e="T279" id="Seg_8369" s="T278">хороший.[NOM.SG]</ta>
            <ta e="T280" id="Seg_8370" s="T279">а</ta>
            <ta e="T281" id="Seg_8371" s="T280">я.NOM</ta>
            <ta e="T282" id="Seg_8372" s="T281">красивый.[NOM.SG]</ta>
            <ta e="T283" id="Seg_8373" s="T282">быть-PRS-1SG</ta>
            <ta e="T284" id="Seg_8374" s="T883">лошадь-ABL</ta>
            <ta e="T285" id="Seg_8375" s="T284">волосы.[NOM.SG]</ta>
            <ta e="T286" id="Seg_8376" s="T285">PTCL</ta>
            <ta e="T287" id="Seg_8377" s="T286">от-тянуть-MOM-PST-1SG</ta>
            <ta e="T288" id="Seg_8378" s="T287">и</ta>
            <ta e="T289" id="Seg_8379" s="T288">сам-LAT/LOC.3SG</ta>
            <ta e="T290" id="Seg_8380" s="T289">класть-PST-1SG</ta>
            <ta e="T292" id="Seg_8381" s="T290">голова-LAT</ta>
            <ta e="T293" id="Seg_8382" s="T292">очень</ta>
            <ta e="T294" id="Seg_8383" s="T293">красивый.[NOM.SG]</ta>
            <ta e="T295" id="Seg_8384" s="T294">быть-PRS-1SG</ta>
            <ta e="T296" id="Seg_8385" s="T295">сейчас</ta>
            <ta e="T298" id="Seg_8386" s="T297">гора-LOC</ta>
            <ta e="T299" id="Seg_8387" s="T298">PTCL</ta>
            <ta e="T300" id="Seg_8388" s="T299">котел.[NOM.SG]</ta>
            <ta e="T301" id="Seg_8389" s="T300">кипеть-DUR.[3SG]</ta>
            <ta e="T302" id="Seg_8390" s="T301">что.[NOM.SG]</ta>
            <ta e="T303" id="Seg_8391" s="T302">этот.[NOM.SG]</ta>
            <ta e="T304" id="Seg_8392" s="T303">такой.[NOM.SG]</ta>
            <ta e="T305" id="Seg_8393" s="T304">муравей.PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T308" id="Seg_8394" s="T306">гора-LOC</ta>
            <ta e="T310" id="Seg_8395" s="T309">гора-LOC</ta>
            <ta e="T311" id="Seg_8396" s="T310">птица.[NOM.SG]</ta>
            <ta e="T312" id="Seg_8397" s="T311">висеть-DUR.[3SG]</ta>
            <ta e="T313" id="Seg_8398" s="T312">что.[NOM.SG]</ta>
            <ta e="T314" id="Seg_8399" s="T313">такой.[NOM.SG]</ta>
            <ta e="T315" id="Seg_8400" s="T314">грудь.[NOM.SG]</ta>
            <ta e="T317" id="Seg_8401" s="T316">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T319" id="Seg_8402" s="T318">земля-LOC</ta>
            <ta e="T320" id="Seg_8403" s="T319">сидеть-DUR.[3SG]</ta>
            <ta e="T321" id="Seg_8404" s="T320">а</ta>
            <ta e="T322" id="Seg_8405" s="T321">ухо-NOM/GEN.3SG</ta>
            <ta e="T323" id="Seg_8406" s="T322">другой.[NOM.SG]</ta>
            <ta e="T324" id="Seg_8407" s="T323">корзина-%%</ta>
            <ta e="T325" id="Seg_8408" s="T324">сидеть-DUR.[3SG]</ta>
            <ta e="T326" id="Seg_8409" s="T325">сережка-PL-INS</ta>
            <ta e="T330" id="Seg_8410" s="T329">два.[NOM.SG]</ta>
            <ta e="T332" id="Seg_8411" s="T331">женщина-PL</ta>
            <ta e="T333" id="Seg_8412" s="T332">лежать-DUR-3PL</ta>
            <ta e="T335" id="Seg_8413" s="T334">лежать-DUR-3PL</ta>
            <ta e="T338" id="Seg_8414" s="T337">огонь-LAT</ta>
            <ta e="T339" id="Seg_8415" s="T338">край-LAT/LOC.3SG</ta>
            <ta e="T340" id="Seg_8416" s="T339">беременная-PL</ta>
            <ta e="T341" id="Seg_8417" s="T340">этот</ta>
            <ta e="T342" id="Seg_8418" s="T341">нога-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T343" id="Seg_8419" s="T342">ляжка-PL</ta>
            <ta e="T345" id="Seg_8420" s="T344">нога-NOM/GEN.3SG</ta>
            <ta e="T346" id="Seg_8421" s="T345">NEG.EX.[3SG]</ta>
            <ta e="T347" id="Seg_8422" s="T346">и</ta>
            <ta e="T348" id="Seg_8423" s="T347">рука-NOM/GEN.3SG</ta>
            <ta e="T349" id="Seg_8424" s="T348">NEG.EX.[3SG]</ta>
            <ta e="T350" id="Seg_8425" s="T349">а</ta>
            <ta e="T352" id="Seg_8426" s="T350">идти-PRS.[3SG]</ta>
            <ta e="T354" id="Seg_8427" s="T353">гора-LAT</ta>
            <ta e="T355" id="Seg_8428" s="T354">идти-PRS.[3SG]</ta>
            <ta e="T356" id="Seg_8429" s="T355">этот</ta>
            <ta e="T357" id="Seg_8430" s="T356">солнце.[NOM.SG]</ta>
            <ta e="T359" id="Seg_8431" s="T358">много</ta>
            <ta e="T360" id="Seg_8432" s="T359">птица-PL</ta>
            <ta e="T361" id="Seg_8433" s="T360">голова-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T362" id="Seg_8434" s="T361">PTCL</ta>
            <ta e="T363" id="Seg_8435" s="T362">один.[NOM.SG]</ta>
            <ta e="T364" id="Seg_8436" s="T363">один-LAT</ta>
            <ta e="T366" id="Seg_8437" s="T365">а</ta>
            <ta e="T368" id="Seg_8438" s="T367">зад-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T369" id="Seg_8439" s="T368">PTCL</ta>
            <ta e="T370" id="Seg_8440" s="T369">далеко-LAT.ADV</ta>
            <ta e="T371" id="Seg_8441" s="T370">один.[NOM.SG]</ta>
            <ta e="T372" id="Seg_8442" s="T371">один-LAT</ta>
            <ta e="T373" id="Seg_8443" s="T372">этот</ta>
            <ta e="T374" id="Seg_8444" s="T373">чум.[NOM.SG]</ta>
            <ta e="T376" id="Seg_8445" s="T375">дом-LOC</ta>
            <ta e="T377" id="Seg_8446" s="T376">лежать-DUR.[3SG]</ta>
            <ta e="T379" id="Seg_8447" s="T378">влажный.[NOM.SG]</ta>
            <ta e="T380" id="Seg_8448" s="T379">теленок.[NOM.SG]</ta>
            <ta e="T381" id="Seg_8449" s="T380">а</ta>
            <ta e="T382" id="Seg_8450" s="T381">сказать-INF.LAT</ta>
            <ta e="T383" id="Seg_8451" s="T382">этот.[NOM.SG]</ta>
            <ta e="T385" id="Seg_8452" s="T885">язык-NOM/GEN.3SG</ta>
            <ta e="T388" id="Seg_8453" s="T387">белый.[NOM.SG]</ta>
            <ta e="T390" id="Seg_8454" s="T389">дерево-3PL-LAT</ta>
            <ta e="T391" id="Seg_8455" s="T390">%%</ta>
            <ta e="T392" id="Seg_8456" s="T391">сидеть-DUR.[3SG]</ta>
            <ta e="T394" id="Seg_8457" s="T393">этот</ta>
            <ta e="T395" id="Seg_8458" s="T394">язык.[NOM.SG]</ta>
            <ta e="T397" id="Seg_8459" s="T396">нога-NOM/GEN.3SG</ta>
            <ta e="T399" id="Seg_8460" s="T398">рука-NOM/GEN.3SG</ta>
            <ta e="T400" id="Seg_8461" s="T399">NEG.EX.[3SG]</ta>
            <ta e="T401" id="Seg_8462" s="T400">а</ta>
            <ta e="T402" id="Seg_8463" s="T401">Бог-LAT</ta>
            <ta e="T403" id="Seg_8464" s="T402">упасть-PRS.[3SG]</ta>
            <ta e="T404" id="Seg_8465" s="T403">этот</ta>
            <ta e="T407" id="Seg_8466" s="T406">нога-NOM/GEN.3SG</ta>
            <ta e="T408" id="Seg_8467" s="T407">рука-NOM/GEN.3SG</ta>
            <ta e="T409" id="Seg_8468" s="T408">NEG.EX.[3SG]</ta>
            <ta e="T410" id="Seg_8469" s="T409">а</ta>
            <ta e="T411" id="Seg_8470" s="T410">дерево-3PL-LAT</ta>
            <ta e="T412" id="Seg_8471" s="T411">идти-PRS.[3SG]</ta>
            <ta e="T415" id="Seg_8472" s="T414">а</ta>
            <ta e="T416" id="Seg_8473" s="T415">чум-LAT/LOC.3SG</ta>
            <ta e="T417" id="Seg_8474" s="T416">прийти-FUT-3SG</ta>
            <ta e="T418" id="Seg_8475" s="T417">лежать-DUR.[3SG]</ta>
            <ta e="T419" id="Seg_8476" s="T418">этот</ta>
            <ta e="T420" id="Seg_8477" s="T419">топор.[NOM.SG]</ta>
            <ta e="T422" id="Seg_8478" s="T421">рука-NOM/GEN.3SG</ta>
            <ta e="T423" id="Seg_8479" s="T422">нога-NOM/GEN.3SG</ta>
            <ta e="T424" id="Seg_8480" s="T423">NEG.EX.[3SG]</ta>
            <ta e="T425" id="Seg_8481" s="T424">а</ta>
            <ta e="T426" id="Seg_8482" s="T425">дверь-NOM/GEN/ACC.1SG</ta>
            <ta e="T427" id="Seg_8483" s="T426">PTCL</ta>
            <ta e="T428" id="Seg_8484" s="T427">открыть-DUR.[3SG]</ta>
            <ta e="T429" id="Seg_8485" s="T428">этот</ta>
            <ta e="T430" id="Seg_8486" s="T429">ветер.[NOM.SG]</ta>
            <ta e="T432" id="Seg_8487" s="T431">маленький.[NOM.SG]</ta>
            <ta e="T434" id="Seg_8488" s="T433">собака.[NOM.SG]</ta>
            <ta e="T435" id="Seg_8489" s="T434">чум-LAT/LOC.3SG</ta>
            <ta e="T436" id="Seg_8490" s="T435">край-LAT/LOC.3SG</ta>
            <ta e="T437" id="Seg_8491" s="T436">сидеть-DUR-3PL</ta>
            <ta e="T438" id="Seg_8492" s="T437">NEG</ta>
            <ta e="T439" id="Seg_8493" s="T438">кричать-PRS.[3SG]</ta>
            <ta e="T440" id="Seg_8494" s="T439">и</ta>
            <ta e="T442" id="Seg_8495" s="T441">кто-ACC=INDEF</ta>
            <ta e="T443" id="Seg_8496" s="T442">чум-LAT</ta>
            <ta e="T445" id="Seg_8497" s="T443">NEG</ta>
            <ta e="T447" id="Seg_8498" s="T446">кто-LAT=INDEF</ta>
            <ta e="T448" id="Seg_8499" s="T447">чум-LAT/LOC.3SG</ta>
            <ta e="T449" id="Seg_8500" s="T448">NEG</ta>
            <ta e="T450" id="Seg_8501" s="T449">пускать-CVB</ta>
            <ta e="T451" id="Seg_8502" s="T450">этот</ta>
            <ta e="T454" id="Seg_8503" s="T453">Красноярск-PL</ta>
            <ta e="T455" id="Seg_8504" s="T454">дом-PL</ta>
            <ta e="T456" id="Seg_8505" s="T455">резать-DUR-3PL</ta>
            <ta e="T457" id="Seg_8506" s="T456">а</ta>
            <ta e="T458" id="Seg_8507" s="T457">здесь</ta>
            <ta e="T459" id="Seg_8508" s="T458">дерево-PL</ta>
            <ta e="T460" id="Seg_8509" s="T459">лететь-DUR-3PL</ta>
            <ta e="T461" id="Seg_8510" s="T460">этот</ta>
            <ta e="T462" id="Seg_8511" s="T461">бумага.[NOM.SG]</ta>
            <ta e="T463" id="Seg_8512" s="T462">идти-PRS.[3SG]</ta>
            <ta e="T464" id="Seg_8513" s="T463">оттуда</ta>
            <ta e="T467" id="Seg_8514" s="T466">язык-NOM/GEN.3SG</ta>
            <ta e="T468" id="Seg_8515" s="T467">NEG.EX.[3SG]</ta>
            <ta e="T469" id="Seg_8516" s="T468">глаз-NOM/GEN.3SG</ta>
            <ta e="T470" id="Seg_8517" s="T469">NEG.EX.[3SG]</ta>
            <ta e="T471" id="Seg_8518" s="T470">рот-NOM/GEN/ACC.3SG</ta>
            <ta e="T472" id="Seg_8519" s="T471">NEG.EX.[3SG]</ta>
            <ta e="T473" id="Seg_8520" s="T472">а</ta>
            <ta e="T474" id="Seg_8521" s="T473">сказать-PRS.[3SG]</ta>
            <ta e="T475" id="Seg_8522" s="T474">сколько</ta>
            <ta e="T476" id="Seg_8523" s="T475">время</ta>
            <ta e="T477" id="Seg_8524" s="T476">пойти-PST.[3SG]</ta>
            <ta e="T478" id="Seg_8525" s="T477">этот</ta>
            <ta e="T479" id="Seg_8526" s="T478">час-NOM/GEN/ACC.3PL</ta>
            <ta e="T481" id="Seg_8527" s="T480">лежать-DUR.[3SG]</ta>
            <ta e="T482" id="Seg_8528" s="T481">так</ta>
            <ta e="T483" id="Seg_8529" s="T482">маленький.[NOM.SG]</ta>
            <ta e="T484" id="Seg_8530" s="T483">кот-LAT</ta>
            <ta e="T485" id="Seg_8531" s="T484">а</ta>
            <ta e="T486" id="Seg_8532" s="T485">стоять-DUR.[3SG]</ta>
            <ta e="T487" id="Seg_8533" s="T486">так</ta>
            <ta e="T488" id="Seg_8534" s="T487">очень</ta>
            <ta e="T489" id="Seg_8535" s="T488">большой.[NOM.SG]</ta>
            <ta e="T490" id="Seg_8536" s="T489">большой.[NOM.SG]</ta>
            <ta e="T491" id="Seg_8537" s="T490">лошадь-3SG-INS</ta>
            <ta e="T493" id="Seg_8538" s="T492">седло.[NOM.SG]</ta>
            <ta e="T495" id="Seg_8539" s="T494">сидеть-DUR.[3SG]</ta>
            <ta e="T496" id="Seg_8540" s="T495">девушка.[NOM.SG]</ta>
            <ta e="T497" id="Seg_8541" s="T496">весь</ta>
            <ta e="T498" id="Seg_8542" s="T497">оспа-3PL-INS</ta>
            <ta e="T499" id="Seg_8543" s="T498">этот</ta>
            <ta e="T500" id="Seg_8544" s="T499">а</ta>
            <ta e="T501" id="Seg_8545" s="T500">этот.[NOM.SG]</ta>
            <ta e="T502" id="Seg_8546" s="T501">наперсток.[NOM.SG]</ta>
            <ta e="T504" id="Seg_8547" s="T503">собирать-PST-1SG</ta>
            <ta e="T505" id="Seg_8548" s="T504">собирать-PST-1SG</ta>
            <ta e="T506" id="Seg_8549" s="T505">нитка-PL</ta>
            <ta e="T507" id="Seg_8550" s="T506">NEG</ta>
            <ta e="T509" id="Seg_8551" s="T508">NEG</ta>
            <ta e="T510" id="Seg_8552" s="T509">мочь-PST-1SG</ta>
            <ta e="T511" id="Seg_8553" s="T510">собирать-INF.LAT</ta>
            <ta e="T512" id="Seg_8554" s="T511">этот.[NOM.SG]</ta>
            <ta e="T513" id="Seg_8555" s="T512">дорога.[NOM.SG]</ta>
            <ta e="T515" id="Seg_8556" s="T514">сидеть-DUR.[3SG]</ta>
            <ta e="T516" id="Seg_8557" s="T515">мужчина.[NOM.SG]</ta>
            <ta e="T517" id="Seg_8558" s="T516">грядка-LOC</ta>
            <ta e="T518" id="Seg_8559" s="T517">PTCL</ta>
            <ta e="T519" id="Seg_8560" s="T518">много</ta>
            <ta e="T520" id="Seg_8561" s="T519">заплатка-NOM/GEN/ACC.3PL</ta>
            <ta e="T521" id="Seg_8562" s="T520">кто.[NOM.SG]</ta>
            <ta e="T523" id="Seg_8563" s="T522">видеть-DUR.[3SG]</ta>
            <ta e="T524" id="Seg_8564" s="T523">PTCL</ta>
            <ta e="T525" id="Seg_8565" s="T524">плакать-DUR.[3SG]</ta>
            <ta e="T526" id="Seg_8566" s="T525">этот</ta>
            <ta e="T527" id="Seg_8567" s="T526">лук.[NOM.SG]</ta>
            <ta e="T529" id="Seg_8568" s="T528">сидеть-DUR.[3SG]</ta>
            <ta e="T530" id="Seg_8569" s="T529">много</ta>
            <ta e="T532" id="Seg_8570" s="T530">одежда.[NOM.SG]</ta>
            <ta e="T535" id="Seg_8571" s="T534">семь.[NOM.SG]</ta>
            <ta e="T536" id="Seg_8572" s="T535">деньги.[NOM.SG]</ta>
            <ta e="T540" id="Seg_8573" s="T539">десять.[NOM.SG]</ta>
            <ta e="T542" id="Seg_8574" s="T541">семь.[NOM.SG]</ta>
            <ta e="T543" id="Seg_8575" s="T542">одежда.[NOM.SG]</ta>
            <ta e="T544" id="Seg_8576" s="T543">а</ta>
            <ta e="T545" id="Seg_8577" s="T544">закрыть-INF.LAT</ta>
            <ta e="T546" id="Seg_8578" s="T545">NEG.EX.[3SG]</ta>
            <ta e="T547" id="Seg_8579" s="T546">этот</ta>
            <ta e="T548" id="Seg_8580" s="T547">капуста.[NOM.SG]</ta>
            <ta e="T550" id="Seg_8581" s="T549">сидеть-DUR.[3SG]</ta>
            <ta e="T551" id="Seg_8582" s="T550">девушка.[NOM.SG]</ta>
            <ta e="T552" id="Seg_8583" s="T551">земля-LOC</ta>
            <ta e="T553" id="Seg_8584" s="T552">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T554" id="Seg_8585" s="T553">красный.[NOM.SG]</ta>
            <ta e="T555" id="Seg_8586" s="T554">а</ta>
            <ta e="T556" id="Seg_8587" s="T555">волосы-NOM/GEN.3SG</ta>
            <ta e="T557" id="Seg_8588" s="T556">снаружи</ta>
            <ta e="T558" id="Seg_8589" s="T557">этот</ta>
            <ta e="T561" id="Seg_8590" s="T560">я.NOM</ta>
            <ta e="T562" id="Seg_8591" s="T561">найти-PST-1SG</ta>
            <ta e="T563" id="Seg_8592" s="T562">плетка.[NOM.SG]</ta>
            <ta e="T564" id="Seg_8593" s="T563">а</ta>
            <ta e="T565" id="Seg_8594" s="T564">пойти-INF.LAT</ta>
            <ta e="T566" id="Seg_8595" s="T565">NEG</ta>
            <ta e="T567" id="Seg_8596" s="T566">мочь-PST-1SG</ta>
            <ta e="T568" id="Seg_8597" s="T567">а</ta>
            <ta e="T569" id="Seg_8598" s="T568">этот.[NOM.SG]</ta>
            <ta e="T570" id="Seg_8599" s="T569">что.[NOM.SG]</ta>
            <ta e="T571" id="Seg_8600" s="T570">%snake.[NOM.SG]</ta>
            <ta e="T573" id="Seg_8601" s="T572">прийти-PRS.[3SG]</ta>
            <ta e="T574" id="Seg_8602" s="T573">девушка.[NOM.SG]</ta>
            <ta e="T575" id="Seg_8603" s="T574">и</ta>
            <ta e="T576" id="Seg_8604" s="T575">там</ta>
            <ta e="T577" id="Seg_8605" s="T576">волосы-PL</ta>
            <ta e="T578" id="Seg_8606" s="T577">PTCL</ta>
            <ta e="T580" id="Seg_8607" s="T579">%%-PTCP-3PL</ta>
            <ta e="T581" id="Seg_8608" s="T580">PTCL</ta>
            <ta e="T583" id="Seg_8609" s="T582">этот</ta>
            <ta e="T584" id="Seg_8610" s="T583">сорока.[NOM.SG]</ta>
            <ta e="T585" id="Seg_8611" s="T584">быть-PRS.[3SG]</ta>
            <ta e="T587" id="Seg_8612" s="T586">два.[NOM.SG]</ta>
            <ta e="T588" id="Seg_8613" s="T587">стоять-PRS-3PL</ta>
            <ta e="T589" id="Seg_8614" s="T588">два.[NOM.SG]</ta>
            <ta e="T590" id="Seg_8615" s="T589">лежать-DUR-3PL</ta>
            <ta e="T591" id="Seg_8616" s="T590">один.[NOM.SG]</ta>
            <ta e="T592" id="Seg_8617" s="T591">идти-DUR.[3SG]</ta>
            <ta e="T593" id="Seg_8618" s="T592">пять.[NOM.SG]</ta>
            <ta e="T594" id="Seg_8619" s="T593">идти-DUR.[3SG]</ta>
            <ta e="T595" id="Seg_8620" s="T594">а</ta>
            <ta e="T596" id="Seg_8621" s="T595">семь.[NOM.SG]</ta>
            <ta e="T598" id="Seg_8622" s="T597">закрыть-DUR.[3SG]</ta>
            <ta e="T599" id="Seg_8623" s="T598">этот</ta>
            <ta e="T600" id="Seg_8624" s="T599">дверь</ta>
            <ta e="T601" id="Seg_8625" s="T600">быть-PRS.[3SG]</ta>
            <ta e="T604" id="Seg_8626" s="T603">овца.[NOM.SG]</ta>
            <ta e="T605" id="Seg_8627" s="T604">много</ta>
            <ta e="T606" id="Seg_8628" s="T605">идти-DUR-3PL</ta>
            <ta e="T607" id="Seg_8629" s="T606">а</ta>
            <ta e="T608" id="Seg_8630" s="T607">пастух.[NOM.SG]</ta>
            <ta e="T611" id="Seg_8631" s="T609">рог-NOM/GEN.3SG</ta>
            <ta e="T612" id="Seg_8632" s="T611">этот</ta>
            <ta e="T613" id="Seg_8633" s="T612">месяц.[NOM.SG]</ta>
            <ta e="T614" id="Seg_8634" s="T613">и</ta>
            <ta e="T615" id="Seg_8635" s="T614">звезда.PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T617" id="Seg_8636" s="T616">два.[NOM.SG]</ta>
            <ta e="T618" id="Seg_8637" s="T617">отверстие.[NOM.SG]</ta>
            <ta e="T619" id="Seg_8638" s="T618">и</ta>
            <ta e="T620" id="Seg_8639" s="T619">два.[NOM.SG]</ta>
            <ta e="T621" id="Seg_8640" s="T620">острый-PL</ta>
            <ta e="T622" id="Seg_8641" s="T621">а</ta>
            <ta e="T623" id="Seg_8642" s="T622">внутри-LOC</ta>
            <ta e="T624" id="Seg_8643" s="T623">пуговица.[NOM.SG]</ta>
            <ta e="T625" id="Seg_8644" s="T624">ножницы-PL</ta>
            <ta e="T627" id="Seg_8645" s="T626">я.NOM</ta>
            <ta e="T628" id="Seg_8646" s="T627">всегда</ta>
            <ta e="T629" id="Seg_8647" s="T628">съесть-DUR-1SG</ta>
            <ta e="T630" id="Seg_8648" s="T629">съесть-DUR-1SG</ta>
            <ta e="T631" id="Seg_8649" s="T630">а</ta>
            <ta e="T632" id="Seg_8650" s="T631">испражняться-INF.LAT</ta>
            <ta e="T633" id="Seg_8651" s="T632">NEG</ta>
            <ta e="T634" id="Seg_8652" s="T633">мочь-PRS-1SG</ta>
            <ta e="T635" id="Seg_8653" s="T634">мешок.[NOM.SG]</ta>
            <ta e="T637" id="Seg_8654" s="T636">петух.[NOM.SG]</ta>
            <ta e="T638" id="Seg_8655" s="T637">сидеть-DUR.[3SG]</ta>
            <ta e="T639" id="Seg_8656" s="T638">забор-LOC</ta>
            <ta e="T640" id="Seg_8657" s="T639">хвост.[NOM.SG]</ta>
            <ta e="T641" id="Seg_8658" s="T640">земля-LOC</ta>
            <ta e="T642" id="Seg_8659" s="T641">а</ta>
            <ta e="T643" id="Seg_8660" s="T642">кричать-DUR.[3SG]</ta>
            <ta e="T644" id="Seg_8661" s="T643">бог-LAT</ta>
            <ta e="T645" id="Seg_8662" s="T644">этот</ta>
            <ta e="T646" id="Seg_8663" s="T645">колокол.[NOM.SG]</ta>
            <ta e="T648" id="Seg_8664" s="T647">стоять-PRS.[3SG]</ta>
            <ta e="T649" id="Seg_8665" s="T648">мальчик.[NOM.SG]</ta>
            <ta e="T650" id="Seg_8666" s="T649">внутри-LOC</ta>
            <ta e="T652" id="Seg_8667" s="T651">%гореть-DUR.[3SG]</ta>
            <ta e="T653" id="Seg_8668" s="T652">а</ta>
            <ta e="T654" id="Seg_8669" s="T653">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T655" id="Seg_8670" s="T654">PTCL</ta>
            <ta e="T656" id="Seg_8671" s="T655">мочиться-DUR.[3SG]</ta>
            <ta e="T657" id="Seg_8672" s="T656">этот</ta>
            <ta e="T658" id="Seg_8673" s="T657">%самовар.[NOM.SG]</ta>
            <ta e="T660" id="Seg_8674" s="T659">два-COLL</ta>
            <ta e="T661" id="Seg_8675" s="T660">пойти-IPFVZ-PRS-3PL</ta>
            <ta e="T662" id="Seg_8676" s="T661">один.[NOM.SG]</ta>
            <ta e="T663" id="Seg_8677" s="T662">идти-PRS.[3SG]</ta>
            <ta e="T664" id="Seg_8678" s="T663">один.[NOM.SG]</ta>
            <ta e="T665" id="Seg_8679" s="T664">догонять-PRS.[3SG]</ta>
            <ta e="T666" id="Seg_8680" s="T665">этот</ta>
            <ta e="T667" id="Seg_8681" s="T666">нога-NOM/GEN/ACC.3PL</ta>
            <ta e="T669" id="Seg_8682" s="T668">ударить-MULT-EP-IMP.2SG</ta>
            <ta e="T670" id="Seg_8683" s="T669">я.ACC</ta>
            <ta e="T671" id="Seg_8684" s="T670">бить-EP-IMP.2SG</ta>
            <ta e="T672" id="Seg_8685" s="T671">я.ACC</ta>
            <ta e="T673" id="Seg_8686" s="T672">влезать-IMP.2SG</ta>
            <ta e="T674" id="Seg_8687" s="T673">я.LAT</ta>
            <ta e="T675" id="Seg_8688" s="T674">быть-PRS.[3SG]</ta>
            <ta e="T676" id="Seg_8689" s="T675">я.LAT</ta>
            <ta e="T677" id="Seg_8690" s="T676">этот</ta>
            <ta e="T678" id="Seg_8691" s="T677">кедровый.орех.[NOM.SG]</ta>
            <ta e="T680" id="Seg_8692" s="T679">вешать-DUR.[3SG]</ta>
            <ta e="T681" id="Seg_8693" s="T680">PTCL</ta>
            <ta e="T682" id="Seg_8694" s="T681">а</ta>
            <ta e="T683" id="Seg_8695" s="T682">люди.[NOM.SG]</ta>
            <ta e="T684" id="Seg_8696" s="T683">PTCL</ta>
            <ta e="T685" id="Seg_8697" s="T684">хватать-DUR-3PL</ta>
            <ta e="T686" id="Seg_8698" s="T685">мыть-DRV-DUR-3PL</ta>
            <ta e="T687" id="Seg_8699" s="T686">тереть-DUR-3PL</ta>
            <ta e="T688" id="Seg_8700" s="T687">этот</ta>
            <ta e="T689" id="Seg_8701" s="T688">рушник.[NOM.SG]</ta>
            <ta e="T690" id="Seg_8702" s="T689">быть-PRS.[3SG]</ta>
            <ta e="T692" id="Seg_8703" s="T888">дерево-PL</ta>
            <ta e="T693" id="Seg_8704" s="T692">трава.[NOM.SG]</ta>
            <ta e="T694" id="Seg_8705" s="T693">расти-DUR.[3SG]</ta>
            <ta e="T697" id="Seg_8706" s="T696">этот</ta>
            <ta e="T698" id="Seg_8707" s="T697">палец.[NOM.SG]</ta>
            <ta e="T700" id="Seg_8708" s="T699">красный.[NOM.SG]</ta>
            <ta e="T701" id="Seg_8709" s="T700">коза.[NOM.SG]</ta>
            <ta e="T702" id="Seg_8710" s="T701">лежать-DUR-PST.[3SG]</ta>
            <ta e="T703" id="Seg_8711" s="T702">там</ta>
            <ta e="T704" id="Seg_8712" s="T703">трава.[NOM.SG]</ta>
            <ta e="T705" id="Seg_8713" s="T704">NEG</ta>
            <ta e="T706" id="Seg_8714" s="T705">расти-DUR.[3SG]</ta>
            <ta e="T707" id="Seg_8715" s="T706">огонь.[NOM.SG]</ta>
            <ta e="T708" id="Seg_8716" s="T707">этот.[NOM.SG]</ta>
            <ta e="T710" id="Seg_8717" s="T709">пойти-PST.[3SG]</ta>
            <ta e="T711" id="Seg_8718" s="T710">ягода-VBLZ-CVB</ta>
            <ta e="T712" id="Seg_8719" s="T711">и</ta>
            <ta e="T713" id="Seg_8720" s="T712">я.NOM</ta>
            <ta e="T714" id="Seg_8721" s="T713">пойти-PST-1SG</ta>
            <ta e="T715" id="Seg_8722" s="T714">ягода.[NOM.SG]</ta>
            <ta e="T716" id="Seg_8723" s="T715">рвать-INF.LAT</ta>
            <ta e="T717" id="Seg_8724" s="T716">и</ta>
            <ta e="T718" id="Seg_8725" s="T717">еще</ta>
            <ta e="T719" id="Seg_8726" s="T718">один.[NOM.SG]</ta>
            <ta e="T720" id="Seg_8727" s="T719">девушка.[NOM.SG]</ta>
            <ta e="T721" id="Seg_8728" s="T720">пойти-PST.[3SG]</ta>
            <ta e="T724" id="Seg_8729" s="T723">лошадь-INS</ta>
            <ta e="T727" id="Seg_8730" s="T726">пойти-PST-1PL</ta>
            <ta e="T728" id="Seg_8731" s="T727">%%-LAT</ta>
            <ta e="T729" id="Seg_8732" s="T728">там</ta>
            <ta e="T730" id="Seg_8733" s="T729">прийти-PST-1PL</ta>
            <ta e="T731" id="Seg_8734" s="T730">спрятаться-FUT-3PL</ta>
            <ta e="T732" id="Seg_8735" s="T731">ягода.[NOM.SG]</ta>
            <ta e="T733" id="Seg_8736" s="T732">смотреть-FRQ-PST-1PL</ta>
            <ta e="T734" id="Seg_8737" s="T733">и</ta>
            <ta e="T735" id="Seg_8738" s="T734">немного</ta>
            <ta e="T736" id="Seg_8739" s="T735">собирать-PST-1PL</ta>
            <ta e="T737" id="Seg_8740" s="T736">тогда</ta>
            <ta e="T738" id="Seg_8741" s="T737">прийти-PST-1PL</ta>
            <ta e="T739" id="Seg_8742" s="T738">где</ta>
            <ta e="T740" id="Seg_8743" s="T739">лошадь.[NOM.SG]</ta>
            <ta e="T741" id="Seg_8744" s="T740">идти-DUR.[3SG]</ta>
            <ta e="T742" id="Seg_8745" s="T741">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T743" id="Seg_8746" s="T742">сказать-IPFVZ.[3SG]</ta>
            <ta e="T744" id="Seg_8747" s="T743">очень</ta>
            <ta e="T745" id="Seg_8748" s="T744">черный.[NOM.SG]</ta>
            <ta e="T747" id="Seg_8749" s="T746">прийти-PRS.[3SG]</ta>
            <ta e="T748" id="Seg_8750" s="T747">надо</ta>
            <ta e="T749" id="Seg_8751" s="T748">делать-INF.LAT</ta>
            <ta e="T750" id="Seg_8752" s="T749">чум.[NOM.SG]</ta>
            <ta e="T751" id="Seg_8753" s="T750">а.то</ta>
            <ta e="T752" id="Seg_8754" s="T751">спать-INF.LAT</ta>
            <ta e="T753" id="Seg_8755" s="T752">NEG</ta>
            <ta e="T754" id="Seg_8756" s="T753">хороший</ta>
            <ta e="T755" id="Seg_8757" s="T754">стать-FUT-3SG</ta>
            <ta e="T756" id="Seg_8758" s="T755">тогда</ta>
            <ta e="T757" id="Seg_8759" s="T756">чум.[NOM.SG]</ta>
            <ta e="T758" id="Seg_8760" s="T757">делать-PST-1PL</ta>
            <ta e="T759" id="Seg_8761" s="T758">кипятить-PST-1PL</ta>
            <ta e="T760" id="Seg_8762" s="T759">желтый.[NOM.SG]</ta>
            <ta e="T761" id="Seg_8763" s="T760">вода.[NOM.SG]</ta>
            <ta e="T762" id="Seg_8764" s="T761">пить-PST-1PL</ta>
            <ta e="T763" id="Seg_8765" s="T762">а</ta>
            <ta e="T764" id="Seg_8766" s="T763">там</ta>
            <ta e="T766" id="Seg_8767" s="T765">быть-PST.[3SG]</ta>
            <ta e="T767" id="Seg_8768" s="T766">половик.[NOM.SG]</ta>
            <ta e="T768" id="Seg_8769" s="T767">там</ta>
            <ta e="T769" id="Seg_8770" s="T768">половик-NOM/GEN/ACC.3SG</ta>
            <ta e="T770" id="Seg_8771" s="T769">%%</ta>
            <ta e="T771" id="Seg_8772" s="T770">дерево-3PL-LAT</ta>
            <ta e="T772" id="Seg_8773" s="T771">тогда</ta>
            <ta e="T773" id="Seg_8774" s="T772">ложиться-1PL</ta>
            <ta e="T774" id="Seg_8775" s="T773">спать-INF.LAT</ta>
            <ta e="T775" id="Seg_8776" s="T774">а</ta>
            <ta e="T776" id="Seg_8777" s="T775">вечер-LOC.ADV</ta>
            <ta e="T777" id="Seg_8778" s="T776">PTCL</ta>
            <ta e="T778" id="Seg_8779" s="T777">очень</ta>
            <ta e="T779" id="Seg_8780" s="T778">сильно</ta>
            <ta e="T780" id="Seg_8781" s="T779">дождь.[NOM.SG]</ta>
            <ta e="T781" id="Seg_8782" s="T780">прийти-PST.[3SG]</ta>
            <ta e="T782" id="Seg_8783" s="T781">прийти-PST.[3SG]</ta>
            <ta e="T783" id="Seg_8784" s="T782">утро-LOC.ADV</ta>
            <ta e="T784" id="Seg_8785" s="T783">встать-PST-1PL</ta>
            <ta e="T785" id="Seg_8786" s="T784">PTCL</ta>
            <ta e="T786" id="Seg_8787" s="T785">мочить-PST-1PL</ta>
            <ta e="T787" id="Seg_8788" s="T786">лошадь-PL</ta>
            <ta e="T788" id="Seg_8789" s="T787">ловить-PST-1PL</ta>
            <ta e="T789" id="Seg_8790" s="T788">пойти-PST-1PL</ta>
            <ta e="T790" id="Seg_8791" s="T789">чум-LAT/LOC.1SG</ta>
            <ta e="T792" id="Seg_8792" s="T791">прийти-PST-1PL</ta>
            <ta e="T793" id="Seg_8793" s="T792">вода-LAT</ta>
            <ta e="T794" id="Seg_8794" s="T793">как=INDEF</ta>
            <ta e="T795" id="Seg_8795" s="T794">нельзя</ta>
            <ta e="T796" id="Seg_8796" s="T795">пойти-INF.LAT</ta>
            <ta e="T797" id="Seg_8797" s="T796">тогда</ta>
            <ta e="T800" id="Seg_8798" s="T799">вода-LAT</ta>
            <ta e="T801" id="Seg_8799" s="T800">пойти-PST-1PL</ta>
            <ta e="T802" id="Seg_8800" s="T801">пойти-PST-1PL</ta>
            <ta e="T803" id="Seg_8801" s="T802">тогда</ta>
            <ta e="T804" id="Seg_8802" s="T803">видеть-PST-1PL</ta>
            <ta e="T805" id="Seg_8803" s="T804">мост.[NOM.SG]</ta>
            <ta e="T806" id="Seg_8804" s="T805">мост.[NOM.SG]</ta>
            <ta e="T808" id="Seg_8805" s="T807">прийти-PST-1PL</ta>
            <ta e="T809" id="Seg_8806" s="T808">и</ta>
            <ta e="T810" id="Seg_8807" s="T809">чум-LAT/LOC.1SG</ta>
            <ta e="T811" id="Seg_8808" s="T810">прийти-PST-1PL</ta>
            <ta e="T814" id="Seg_8809" s="T813">я.NOM</ta>
            <ta e="T815" id="Seg_8810" s="T814">сегодня</ta>
            <ta e="T816" id="Seg_8811" s="T815">утро</ta>
            <ta e="T817" id="Seg_8812" s="T816">встать-PST-1SG</ta>
            <ta e="T818" id="Seg_8813" s="T817">магазин-LAT</ta>
            <ta e="T819" id="Seg_8814" s="T818">пойти-PST-1SG</ta>
            <ta e="T821" id="Seg_8815" s="T889">стоять-PST-1SG</ta>
            <ta e="T822" id="Seg_8816" s="T821">тогда</ta>
            <ta e="T823" id="Seg_8817" s="T822">два.[NOM.SG]</ta>
            <ta e="T824" id="Seg_8818" s="T823">два.[NOM.SG]</ta>
            <ta e="T825" id="Seg_8819" s="T824">час.[NOM.SG]</ta>
            <ta e="T826" id="Seg_8820" s="T825">стоять-PST-1SG</ta>
            <ta e="T827" id="Seg_8821" s="T826">тогда</ta>
            <ta e="T828" id="Seg_8822" s="T827">взять-PST-1SG</ta>
            <ta e="T829" id="Seg_8823" s="T828">рыба.[NOM.SG]</ta>
            <ta e="T830" id="Seg_8824" s="T829">и</ta>
            <ta e="T831" id="Seg_8825" s="T830">прийти-PST-1SG</ta>
            <ta e="T832" id="Seg_8826" s="T831">чум-LAT</ta>
            <ta e="T833" id="Seg_8827" s="T832">и</ta>
            <ta e="T834" id="Seg_8828" s="T833">вы.NOM</ta>
            <ta e="T835" id="Seg_8829" s="T834">прийти-PST-2PL</ta>
            <ta e="T837" id="Seg_8830" s="T836">тогда</ta>
            <ta e="T838" id="Seg_8831" s="T837">я.NOM</ta>
            <ta e="T839" id="Seg_8832" s="T838">взять-PST-1SG</ta>
            <ta e="T840" id="Seg_8833" s="T839">один.[NOM.SG]</ta>
            <ta e="T841" id="Seg_8834" s="T840">килограмм.[NOM.SG]</ta>
            <ta e="T842" id="Seg_8835" s="T841">деньги.[NOM.SG]</ta>
            <ta e="T843" id="Seg_8836" s="T842">дать-PST-1SG</ta>
            <ta e="T844" id="Seg_8837" s="T843">один.[NOM.SG]</ta>
            <ta e="T845" id="Seg_8838" s="T844">рубль.[NOM.SG]</ta>
            <ta e="T846" id="Seg_8839" s="T845">десять.[NOM.SG]</ta>
            <ta e="T847" id="Seg_8840" s="T846">четыре.[NOM.SG]</ta>
            <ta e="T848" id="Seg_8841" s="T847">дать-PST-1SG</ta>
            <ta e="T849" id="Seg_8842" s="T848">один.[NOM.SG]</ta>
            <ta e="T850" id="Seg_8843" s="T849">рубль.[NOM.SG]</ta>
            <ta e="T851" id="Seg_8844" s="T850">десять.[NOM.SG]</ta>
            <ta e="T852" id="Seg_8845" s="T851">четыре.[NOM.SG]</ta>
            <ta e="T854" id="Seg_8846" s="T853">там</ta>
            <ta e="T855" id="Seg_8847" s="T854">PTCL</ta>
            <ta e="T856" id="Seg_8848" s="T855">женщина-PL</ta>
            <ta e="T857" id="Seg_8849" s="T856">ругать-DES-DUR-3PL</ta>
            <ta e="T858" id="Seg_8850" s="T857">кричать-DUR-3PL</ta>
            <ta e="T859" id="Seg_8851" s="T858">сказать-PRS-3PL</ta>
            <ta e="T860" id="Seg_8852" s="T859">мало</ta>
            <ta e="T861" id="Seg_8853" s="T860">рыба.[NOM.SG]</ta>
            <ta e="T862" id="Seg_8854" s="T861">один.[NOM.SG]</ta>
            <ta e="T864" id="Seg_8855" s="T863">один.[NOM.SG]</ta>
            <ta e="T865" id="Seg_8856" s="T864">килограмм.[NOM.SG]</ta>
            <ta e="T866" id="Seg_8857" s="T865">дать-DUR.[3SG]</ta>
            <ta e="T867" id="Seg_8858" s="T866">этот</ta>
            <ta e="T868" id="Seg_8859" s="T867">дать-PST.[3SG]</ta>
            <ta e="T869" id="Seg_8860" s="T868">один.[NOM.SG]</ta>
            <ta e="T870" id="Seg_8861" s="T869">PTCL</ta>
            <ta e="T871" id="Seg_8862" s="T870">женщина-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T872" id="Seg_8863" s="T871">один.[NOM.SG]</ta>
            <ta e="T873" id="Seg_8864" s="T872">килограмм.[NOM.SG]</ta>
            <ta e="T874" id="Seg_8865" s="T873">дать-PST.[3SG]</ta>
            <ta e="T875" id="Seg_8866" s="T874">сказать-PRS-3PL</ta>
            <ta e="T876" id="Seg_8867" s="T875">ребенок-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T877" id="Seg_8868" s="T876">кричать-DUR-3PL</ta>
            <ta e="T878" id="Seg_8869" s="T877">рыба.[NOM.SG]</ta>
            <ta e="T879" id="Seg_8870" s="T878">рыба.[NOM.SG]</ta>
            <ta e="T880" id="Seg_8871" s="T879">съесть-INF.LAT</ta>
            <ta e="T881" id="Seg_8872" s="T880">нужно</ta>
            <ta e="T882" id="Seg_8873" s="T881">этот-PL-LAT</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T1" id="Seg_8874" s="T0">adv</ta>
            <ta e="T2" id="Seg_8875" s="T1">dempro-n:case</ta>
            <ta e="T3" id="Seg_8876" s="T2">n-n:num</ta>
            <ta e="T4" id="Seg_8877" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_8878" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_8879" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_8880" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_8881" s="T7">adv</ta>
            <ta e="T9" id="Seg_8882" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_8883" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_8884" s="T10">n-n:case.poss</ta>
            <ta e="T12" id="Seg_8885" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_8886" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_8887" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_8888" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_8889" s="T15">num-n:case</ta>
            <ta e="T17" id="Seg_8890" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_8891" s="T17">adj-n:case</ta>
            <ta e="T19" id="Seg_8892" s="T18">adv</ta>
            <ta e="T20" id="Seg_8893" s="T19">dempro-n:case</ta>
            <ta e="T21" id="Seg_8894" s="T20">ptcl</ta>
            <ta e="T23" id="Seg_8895" s="T21">adv</ta>
            <ta e="T24" id="Seg_8896" s="T23">adv</ta>
            <ta e="T25" id="Seg_8897" s="T24">num-n:case</ta>
            <ta e="T27" id="Seg_8898" s="T26">adv</ta>
            <ta e="T28" id="Seg_8899" s="T27">n-n:case.poss</ta>
            <ta e="T29" id="Seg_8900" s="T28">ptcl</ta>
            <ta e="T31" id="Seg_8901" s="T30">n-n:case</ta>
            <ta e="T33" id="Seg_8902" s="T32">dempro-n:case</ta>
            <ta e="T34" id="Seg_8903" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_8904" s="T34">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_8905" s="T35">n-n:num-n:case</ta>
            <ta e="T37" id="Seg_8906" s="T36">adv</ta>
            <ta e="T38" id="Seg_8907" s="T37">dempro-n:case</ta>
            <ta e="T39" id="Seg_8908" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_8909" s="T39">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_8910" s="T40">dempro</ta>
            <ta e="T42" id="Seg_8911" s="T41">adv</ta>
            <ta e="T44" id="Seg_8912" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_8913" s="T44">v-v&gt;v-v:pn</ta>
            <ta e="T46" id="Seg_8914" s="T45">adv</ta>
            <ta e="T47" id="Seg_8915" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_8916" s="T47">adv</ta>
            <ta e="T49" id="Seg_8917" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_8918" s="T49">v-v&gt;v-v:pn</ta>
            <ta e="T51" id="Seg_8919" s="T50">v-v&gt;v-v:pn</ta>
            <ta e="T52" id="Seg_8920" s="T51">adv</ta>
            <ta e="T53" id="Seg_8921" s="T52">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_8922" s="T53">adj-n:case</ta>
            <ta e="T55" id="Seg_8923" s="T54">n-n:case.poss-n:case</ta>
            <ta e="T57" id="Seg_8924" s="T56">adj-n:case</ta>
            <ta e="T58" id="Seg_8925" s="T57">num-n:case</ta>
            <ta e="T59" id="Seg_8926" s="T58">num-num&gt;num-n:case</ta>
            <ta e="T60" id="Seg_8927" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_8928" s="T60">adv</ta>
            <ta e="T62" id="Seg_8929" s="T61">dempro-n:num</ta>
            <ta e="T63" id="Seg_8930" s="T62">adv</ta>
            <ta e="T65" id="Seg_8931" s="T64">adv</ta>
            <ta e="T66" id="Seg_8932" s="T65">dempro-n:case</ta>
            <ta e="T67" id="Seg_8933" s="T66">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_8934" s="T67">n-n:num-n:case</ta>
            <ta e="T69" id="Seg_8935" s="T68">dempro</ta>
            <ta e="T70" id="Seg_8936" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_8937" s="T70">v-v&gt;v-v:pn</ta>
            <ta e="T72" id="Seg_8938" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_8939" s="T72">adv</ta>
            <ta e="T74" id="Seg_8940" s="T73">adj-n:case</ta>
            <ta e="T75" id="Seg_8941" s="T74">adv</ta>
            <ta e="T76" id="Seg_8942" s="T75">pers</ta>
            <ta e="T77" id="Seg_8943" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_8944" s="T77">adv</ta>
            <ta e="T80" id="Seg_8945" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_8946" s="T80">n-n:case.poss</ta>
            <ta e="T82" id="Seg_8947" s="T81">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_8948" s="T82">v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_8949" s="T83">adv</ta>
            <ta e="T85" id="Seg_8950" s="T84">v-v&gt;v-v:pn</ta>
            <ta e="T86" id="Seg_8951" s="T85">v-v&gt;v-v:pn</ta>
            <ta e="T88" id="Seg_8952" s="T87">num-num&gt;num-n:case</ta>
            <ta e="T89" id="Seg_8953" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_8954" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_8955" s="T90">dempro-n:case</ta>
            <ta e="T92" id="Seg_8956" s="T91">n-n:case</ta>
            <ta e="T93" id="Seg_8957" s="T92">adj-n:case</ta>
            <ta e="T94" id="Seg_8958" s="T93">dempro-n:num</ta>
            <ta e="T95" id="Seg_8959" s="T94">adv</ta>
            <ta e="T97" id="Seg_8960" s="T96">adv</ta>
            <ta e="T98" id="Seg_8961" s="T97">dempro-n:case</ta>
            <ta e="T99" id="Seg_8962" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_8963" s="T99">v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_8964" s="T100">v-v&gt;v-v:pn</ta>
            <ta e="T102" id="Seg_8965" s="T101">dempro</ta>
            <ta e="T103" id="Seg_8966" s="T102">n-n:case</ta>
            <ta e="T105" id="Seg_8967" s="T104">v-v:tense-v:pn</ta>
            <ta e="T106" id="Seg_8968" s="T105">dempro-n:num-n:case</ta>
            <ta e="T107" id="Seg_8969" s="T106">que</ta>
            <ta e="T108" id="Seg_8970" s="T107">n-n:num</ta>
            <ta e="T109" id="Seg_8971" s="T108">v-v&gt;v-v:pn</ta>
            <ta e="T110" id="Seg_8972" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_8973" s="T110">pers</ta>
            <ta e="T112" id="Seg_8974" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_8975" s="T112">pers</ta>
            <ta e="T114" id="Seg_8976" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_8977" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_8978" s="T115">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T117" id="Seg_8979" s="T116">pers</ta>
            <ta e="T118" id="Seg_8980" s="T117">pers</ta>
            <ta e="T119" id="Seg_8981" s="T118">adv</ta>
            <ta e="T120" id="Seg_8982" s="T119">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_8983" s="T120">adv</ta>
            <ta e="T122" id="Seg_8984" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_8985" s="T122">v-v:mood.pn</ta>
            <ta e="T124" id="Seg_8986" s="T123">adv</ta>
            <ta e="T125" id="Seg_8987" s="T124">dempro</ta>
            <ta e="T126" id="Seg_8988" s="T125">dempro-n:case</ta>
            <ta e="T127" id="Seg_8989" s="T126">v-v:tense-v:pn</ta>
            <ta e="T129" id="Seg_8990" s="T128">v-v:tense-v:pn</ta>
            <ta e="T130" id="Seg_8991" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_8992" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_8993" s="T131">v-v:tense-v:pn</ta>
            <ta e="T133" id="Seg_8994" s="T132">n-n:case</ta>
            <ta e="T134" id="Seg_8995" s="T133">v-v:tense-v:pn</ta>
            <ta e="T135" id="Seg_8996" s="T134">v-v:mood.pn</ta>
            <ta e="T136" id="Seg_8997" s="T135">adv</ta>
            <ta e="T137" id="Seg_8998" s="T136">pers</ta>
            <ta e="T138" id="Seg_8999" s="T137">v-v:tense-v:pn</ta>
            <ta e="T139" id="Seg_9000" s="T138">adj-n:case</ta>
            <ta e="T140" id="Seg_9001" s="T139">v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_9002" s="T140">pers</ta>
            <ta e="T142" id="Seg_9003" s="T141">v-v:tense-v:pn</ta>
            <ta e="T143" id="Seg_9004" s="T142">adv</ta>
            <ta e="T144" id="Seg_9005" s="T143">dempro-n:num</ta>
            <ta e="T145" id="Seg_9006" s="T144">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_9007" s="T145">v-v&gt;v-v:tense</ta>
            <ta e="T147" id="Seg_9008" s="T146">num-n:case</ta>
            <ta e="T148" id="Seg_9009" s="T147">n-n:case</ta>
            <ta e="T149" id="Seg_9010" s="T148">num-n:case</ta>
            <ta e="T150" id="Seg_9011" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_9012" s="T150">v-v:tense-v:pn</ta>
            <ta e="T152" id="Seg_9013" s="T151">adj-n:case</ta>
            <ta e="T153" id="Seg_9014" s="T152">adv</ta>
            <ta e="T154" id="Seg_9015" s="T153">v-v:pn</ta>
            <ta e="T155" id="Seg_9016" s="T154">adv</ta>
            <ta e="T156" id="Seg_9017" s="T155">n-n:case</ta>
            <ta e="T157" id="Seg_9018" s="T156">adv</ta>
            <ta e="T158" id="Seg_9019" s="T157">adj-n:case</ta>
            <ta e="T159" id="Seg_9020" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_9021" s="T159">v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_9022" s="T160">n-n:case</ta>
            <ta e="T162" id="Seg_9023" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_9024" s="T162">v-v:pn</ta>
            <ta e="T164" id="Seg_9025" s="T163">v-v&gt;v-v:pn</ta>
            <ta e="T165" id="Seg_9026" s="T164">v-v&gt;v-v:pn</ta>
            <ta e="T166" id="Seg_9027" s="T165">adv</ta>
            <ta e="T167" id="Seg_9028" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_9029" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_9030" s="T168">v-v:tense-v:pn</ta>
            <ta e="T170" id="Seg_9031" s="T169">conj</ta>
            <ta e="T171" id="Seg_9032" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_9033" s="T171">v-v&gt;v-v:pn</ta>
            <ta e="T173" id="Seg_9034" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_9035" s="T173">v-v:mood-v:pn</ta>
            <ta e="T175" id="Seg_9036" s="T174">pers-n:case</ta>
            <ta e="T176" id="Seg_9037" s="T175">n-n:case-n:case.poss</ta>
            <ta e="T177" id="Seg_9038" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_9039" s="T177">v-v:tense-v:pn</ta>
            <ta e="T179" id="Seg_9040" s="T178">conj</ta>
            <ta e="T180" id="Seg_9041" s="T179">pers</ta>
            <ta e="T181" id="Seg_9042" s="T180">v-v:tense-v:pn</ta>
            <ta e="T182" id="Seg_9043" s="T181">v-v:tense-v:pn</ta>
            <ta e="T183" id="Seg_9044" s="T182">dempro-n:case</ta>
            <ta e="T184" id="Seg_9045" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_9046" s="T184">num-n:case</ta>
            <ta e="T186" id="Seg_9047" s="T185">v-v:tense-v:pn</ta>
            <ta e="T187" id="Seg_9048" s="T186">n-n:case.poss</ta>
            <ta e="T188" id="Seg_9049" s="T187">v-v:tense-v:pn</ta>
            <ta e="T189" id="Seg_9050" s="T188">n-n:case.poss</ta>
            <ta e="T190" id="Seg_9051" s="T189">v-v:pn</ta>
            <ta e="T192" id="Seg_9052" s="T191">n-n:case</ta>
            <ta e="T193" id="Seg_9053" s="T192">que-n:case=ptcl</ta>
            <ta e="T194" id="Seg_9054" s="T193">v-v:pn</ta>
            <ta e="T195" id="Seg_9055" s="T194">num-n:case</ta>
            <ta e="T196" id="Seg_9056" s="T195">num-n:case</ta>
            <ta e="T198" id="Seg_9057" s="T197">num-n:case</ta>
            <ta e="T199" id="Seg_9058" s="T198">n-n:case</ta>
            <ta e="T200" id="Seg_9059" s="T199">v-v:tense-v:pn</ta>
            <ta e="T201" id="Seg_9060" s="T200">dempro</ta>
            <ta e="T202" id="Seg_9061" s="T201">v-v:tense-v:pn</ta>
            <ta e="T205" id="Seg_9062" s="T204">v-v:tense-v:pn</ta>
            <ta e="T206" id="Seg_9063" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_9064" s="T206">n-n:case</ta>
            <ta e="T208" id="Seg_9065" s="T207">conj</ta>
            <ta e="T209" id="Seg_9066" s="T208">pers</ta>
            <ta e="T210" id="Seg_9067" s="T209">n-n:case.poss</ta>
            <ta e="T211" id="Seg_9068" s="T210">ptcl</ta>
            <ta e="T212" id="Seg_9069" s="T211">num-n:case</ta>
            <ta e="T213" id="Seg_9070" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_9071" s="T213">v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_9072" s="T214">dempro-n:num</ta>
            <ta e="T216" id="Seg_9073" s="T215">n-n:case</ta>
            <ta e="T217" id="Seg_9074" s="T216">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T218" id="Seg_9075" s="T217">conj</ta>
            <ta e="T219" id="Seg_9076" s="T218">pers</ta>
            <ta e="T220" id="Seg_9077" s="T219">n-n:case.poss</ta>
            <ta e="T221" id="Seg_9078" s="T220">ptcl</ta>
            <ta e="T222" id="Seg_9079" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_9080" s="T222">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T224" id="Seg_9081" s="T223">conj</ta>
            <ta e="T225" id="Seg_9082" s="T224">que</ta>
            <ta e="T226" id="Seg_9083" s="T225">dempro-n:case</ta>
            <ta e="T227" id="Seg_9084" s="T226">que=ptcl</ta>
            <ta e="T384" id="Seg_9085" s="T227">v-v:n-fin</ta>
            <ta e="T228" id="Seg_9086" s="T384">v-v:tense-v:pn</ta>
            <ta e="T229" id="Seg_9087" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_9088" s="T229">v-v:pn</ta>
            <ta e="T231" id="Seg_9089" s="T230">adv</ta>
            <ta e="T232" id="Seg_9090" s="T231">dempro-n:case</ta>
            <ta e="T233" id="Seg_9091" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_9092" s="T233">v-v:tense-v:pn</ta>
            <ta e="T235" id="Seg_9093" s="T234">v-v:tense-v:pn</ta>
            <ta e="T236" id="Seg_9094" s="T235">v-v:n.fin</ta>
            <ta e="T239" id="Seg_9095" s="T238">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T240" id="Seg_9096" s="T239">v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_9097" s="T240">v-v:tense-v:pn</ta>
            <ta e="T242" id="Seg_9098" s="T241">adv</ta>
            <ta e="T243" id="Seg_9099" s="T242">adv</ta>
            <ta e="T244" id="Seg_9100" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_9101" s="T244">dempro-n:case</ta>
            <ta e="T246" id="Seg_9102" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_9103" s="T246">v-v:n.fin</ta>
            <ta e="T248" id="Seg_9104" s="T247">dempro</ta>
            <ta e="T249" id="Seg_9105" s="T248">v</ta>
            <ta e="T250" id="Seg_9106" s="T249">dempro-n:case</ta>
            <ta e="T252" id="Seg_9107" s="T251">v-v:n.fin</ta>
            <ta e="T253" id="Seg_9108" s="T252">n-n:case</ta>
            <ta e="T254" id="Seg_9109" s="T253">dempro</ta>
            <ta e="T255" id="Seg_9110" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_9111" s="T255">v-v&gt;v-v:pn</ta>
            <ta e="T257" id="Seg_9112" s="T256">aux-v:mood.pn</ta>
            <ta e="T258" id="Seg_9113" s="T257">v-v:ins-v:mood.pn</ta>
            <ta e="T259" id="Seg_9114" s="T258">pers</ta>
            <ta e="T260" id="Seg_9115" s="T259">pers</ta>
            <ta e="T261" id="Seg_9116" s="T260">n-n:case.poss</ta>
            <ta e="T262" id="Seg_9117" s="T261">v-v:tense-v:pn</ta>
            <ta e="T263" id="Seg_9118" s="T262">n-n:num</ta>
            <ta e="T264" id="Seg_9119" s="T263">v-v&gt;v-v:pn</ta>
            <ta e="T265" id="Seg_9120" s="T264">n-n:case</ta>
            <ta e="T266" id="Seg_9121" s="T265">conj</ta>
            <ta e="T269" id="Seg_9122" s="T267">n-n:case</ta>
            <ta e="T270" id="Seg_9123" s="T269">n-n:case</ta>
            <ta e="T271" id="Seg_9124" s="T270">v-v:tense-v:pn</ta>
            <ta e="T272" id="Seg_9125" s="T271">n-n:case</ta>
            <ta e="T273" id="Seg_9126" s="T272">pers</ta>
            <ta e="T274" id="Seg_9127" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_9128" s="T274">adj-n:case</ta>
            <ta e="T276" id="Seg_9129" s="T275">que</ta>
            <ta e="T277" id="Seg_9130" s="T276">n-n:num</ta>
            <ta e="T278" id="Seg_9131" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_9132" s="T278">adj-n:case</ta>
            <ta e="T280" id="Seg_9133" s="T279">conj</ta>
            <ta e="T281" id="Seg_9134" s="T280">pers</ta>
            <ta e="T282" id="Seg_9135" s="T281">adj-n:case</ta>
            <ta e="T283" id="Seg_9136" s="T282">v-v:tense-v:pn</ta>
            <ta e="T284" id="Seg_9137" s="T883">n-n:case</ta>
            <ta e="T285" id="Seg_9138" s="T284">n-n:case</ta>
            <ta e="T286" id="Seg_9139" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_9140" s="T286">v&gt;v-v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T288" id="Seg_9141" s="T287">conj</ta>
            <ta e="T289" id="Seg_9142" s="T288">refl-n:case.poss</ta>
            <ta e="T290" id="Seg_9143" s="T289">v-v:tense-v:pn</ta>
            <ta e="T292" id="Seg_9144" s="T290">n-n:case</ta>
            <ta e="T293" id="Seg_9145" s="T292">adv</ta>
            <ta e="T294" id="Seg_9146" s="T293">adj-n:case</ta>
            <ta e="T295" id="Seg_9147" s="T294">v-v:tense-v:pn</ta>
            <ta e="T296" id="Seg_9148" s="T295">adv</ta>
            <ta e="T298" id="Seg_9149" s="T297">n-n:case</ta>
            <ta e="T299" id="Seg_9150" s="T298">ptcl</ta>
            <ta e="T300" id="Seg_9151" s="T299">n-n:case</ta>
            <ta e="T301" id="Seg_9152" s="T300">v-v&gt;v-v:pn</ta>
            <ta e="T302" id="Seg_9153" s="T301">que-n:case</ta>
            <ta e="T303" id="Seg_9154" s="T302">dempro-n:case</ta>
            <ta e="T304" id="Seg_9155" s="T303">adj-n:case</ta>
            <ta e="T305" id="Seg_9156" s="T304">n-n:case.poss</ta>
            <ta e="T308" id="Seg_9157" s="T306">n-n:case</ta>
            <ta e="T310" id="Seg_9158" s="T309">n-n:case</ta>
            <ta e="T311" id="Seg_9159" s="T310">n-n:case</ta>
            <ta e="T312" id="Seg_9160" s="T311">v-v&gt;v-v:pn</ta>
            <ta e="T313" id="Seg_9161" s="T312">que-n:case</ta>
            <ta e="T314" id="Seg_9162" s="T313">adj-n:case</ta>
            <ta e="T315" id="Seg_9163" s="T314">n-n:case</ta>
            <ta e="T317" id="Seg_9164" s="T316">refl-n:case.poss</ta>
            <ta e="T319" id="Seg_9165" s="T318">n-n:case</ta>
            <ta e="T320" id="Seg_9166" s="T319">v-v&gt;v-v:pn</ta>
            <ta e="T321" id="Seg_9167" s="T320">conj</ta>
            <ta e="T322" id="Seg_9168" s="T321">n-n:case.poss</ta>
            <ta e="T323" id="Seg_9169" s="T322">adj-n:case</ta>
            <ta e="T324" id="Seg_9170" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_9171" s="T324">v-v&gt;v-v:pn</ta>
            <ta e="T326" id="Seg_9172" s="T325">n-n:num-n:case</ta>
            <ta e="T330" id="Seg_9173" s="T329">num-n:case</ta>
            <ta e="T332" id="Seg_9174" s="T331">n-n:num</ta>
            <ta e="T333" id="Seg_9175" s="T332">v-v&gt;v-v:pn</ta>
            <ta e="T335" id="Seg_9176" s="T334">v-v&gt;v-v:pn</ta>
            <ta e="T338" id="Seg_9177" s="T337">n-n:case</ta>
            <ta e="T339" id="Seg_9178" s="T338">n-n:case.poss</ta>
            <ta e="T340" id="Seg_9179" s="T339">adj-n:num</ta>
            <ta e="T341" id="Seg_9180" s="T340">dempro</ta>
            <ta e="T342" id="Seg_9181" s="T341">n-n:num-n:case.poss</ta>
            <ta e="T343" id="Seg_9182" s="T342">n-n:num</ta>
            <ta e="T345" id="Seg_9183" s="T344">n-n:case.poss</ta>
            <ta e="T346" id="Seg_9184" s="T345">v-v:pn</ta>
            <ta e="T347" id="Seg_9185" s="T346">conj</ta>
            <ta e="T348" id="Seg_9186" s="T347">n-n:case.poss</ta>
            <ta e="T349" id="Seg_9187" s="T348">v-v:pn</ta>
            <ta e="T350" id="Seg_9188" s="T349">conj</ta>
            <ta e="T352" id="Seg_9189" s="T350">v-v:tense-v:pn</ta>
            <ta e="T354" id="Seg_9190" s="T353">n-n:case</ta>
            <ta e="T355" id="Seg_9191" s="T354">v-v:tense-v:pn</ta>
            <ta e="T356" id="Seg_9192" s="T355">dempro</ta>
            <ta e="T357" id="Seg_9193" s="T356">n-n:case</ta>
            <ta e="T359" id="Seg_9194" s="T358">quant</ta>
            <ta e="T360" id="Seg_9195" s="T359">n-n:num</ta>
            <ta e="T361" id="Seg_9196" s="T360">n-n:num-n:case.poss</ta>
            <ta e="T362" id="Seg_9197" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_9198" s="T362">num-n:case</ta>
            <ta e="T364" id="Seg_9199" s="T363">num-n:case</ta>
            <ta e="T366" id="Seg_9200" s="T365">conj</ta>
            <ta e="T368" id="Seg_9201" s="T367">n-n:num-n:case.poss</ta>
            <ta e="T369" id="Seg_9202" s="T368">ptcl</ta>
            <ta e="T370" id="Seg_9203" s="T369">adv-adv&gt;adv</ta>
            <ta e="T371" id="Seg_9204" s="T370">num-n:case</ta>
            <ta e="T372" id="Seg_9205" s="T371">num-n:case</ta>
            <ta e="T373" id="Seg_9206" s="T372">dempro</ta>
            <ta e="T374" id="Seg_9207" s="T373">n-n:case</ta>
            <ta e="T376" id="Seg_9208" s="T375">n-n:case</ta>
            <ta e="T377" id="Seg_9209" s="T376">v-v&gt;v-v:pn</ta>
            <ta e="T379" id="Seg_9210" s="T378">adj-n:case</ta>
            <ta e="T380" id="Seg_9211" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_9212" s="T380">conj</ta>
            <ta e="T382" id="Seg_9213" s="T381">v-v:n.fin</ta>
            <ta e="T383" id="Seg_9214" s="T382">dempro-n:case</ta>
            <ta e="T385" id="Seg_9215" s="T885">n-n:case.poss</ta>
            <ta e="T388" id="Seg_9216" s="T387">adj-n:case</ta>
            <ta e="T390" id="Seg_9217" s="T389">n-n:case.poss-n:case</ta>
            <ta e="T391" id="Seg_9218" s="T390">n</ta>
            <ta e="T392" id="Seg_9219" s="T391">v-v&gt;v-v:pn</ta>
            <ta e="T394" id="Seg_9220" s="T393">dempro</ta>
            <ta e="T395" id="Seg_9221" s="T394">n-n:case</ta>
            <ta e="T397" id="Seg_9222" s="T396">n-n:case.poss</ta>
            <ta e="T399" id="Seg_9223" s="T398">n-n:case.poss</ta>
            <ta e="T400" id="Seg_9224" s="T399">v-v:pn</ta>
            <ta e="T401" id="Seg_9225" s="T400">conj</ta>
            <ta e="T402" id="Seg_9226" s="T401">n-n:case</ta>
            <ta e="T403" id="Seg_9227" s="T402">v-v:tense-v:pn</ta>
            <ta e="T404" id="Seg_9228" s="T403">dempro</ta>
            <ta e="T407" id="Seg_9229" s="T406">n-n:case.poss</ta>
            <ta e="T408" id="Seg_9230" s="T407">n-n:case.poss</ta>
            <ta e="T409" id="Seg_9231" s="T408">v-v:pn</ta>
            <ta e="T410" id="Seg_9232" s="T409">conj</ta>
            <ta e="T411" id="Seg_9233" s="T410">n-n:case.poss-n:case</ta>
            <ta e="T412" id="Seg_9234" s="T411">v-v:tense-v:pn</ta>
            <ta e="T415" id="Seg_9235" s="T414">conj</ta>
            <ta e="T416" id="Seg_9236" s="T415">n-n:case.poss</ta>
            <ta e="T417" id="Seg_9237" s="T416">v-v:tense-v:pn</ta>
            <ta e="T418" id="Seg_9238" s="T417">v-v&gt;v-v:pn</ta>
            <ta e="T419" id="Seg_9239" s="T418">dempro</ta>
            <ta e="T420" id="Seg_9240" s="T419">n-n:case</ta>
            <ta e="T422" id="Seg_9241" s="T421">n-n:case.poss</ta>
            <ta e="T423" id="Seg_9242" s="T422">n-n:case.poss</ta>
            <ta e="T424" id="Seg_9243" s="T423">v-v:pn</ta>
            <ta e="T425" id="Seg_9244" s="T424">conj</ta>
            <ta e="T426" id="Seg_9245" s="T425">n-n:case.poss</ta>
            <ta e="T427" id="Seg_9246" s="T426">ptcl</ta>
            <ta e="T428" id="Seg_9247" s="T427">v-v&gt;v-v:pn</ta>
            <ta e="T429" id="Seg_9248" s="T428">dempro</ta>
            <ta e="T430" id="Seg_9249" s="T429">n-n:case</ta>
            <ta e="T432" id="Seg_9250" s="T431">adj-n:case</ta>
            <ta e="T434" id="Seg_9251" s="T433">n-n:case</ta>
            <ta e="T435" id="Seg_9252" s="T434">n-n:case.poss</ta>
            <ta e="T436" id="Seg_9253" s="T435">n-n:case.poss</ta>
            <ta e="T437" id="Seg_9254" s="T436">v-v&gt;v-v:pn</ta>
            <ta e="T438" id="Seg_9255" s="T437">ptcl</ta>
            <ta e="T439" id="Seg_9256" s="T438">v-v:tense-v:pn</ta>
            <ta e="T440" id="Seg_9257" s="T439">conj</ta>
            <ta e="T442" id="Seg_9258" s="T441">que-n:case=ptcl</ta>
            <ta e="T443" id="Seg_9259" s="T442">n-n:case</ta>
            <ta e="T445" id="Seg_9260" s="T443">ptcl</ta>
            <ta e="T447" id="Seg_9261" s="T446">que-n:case=ptcl</ta>
            <ta e="T448" id="Seg_9262" s="T447">n-n:case.poss</ta>
            <ta e="T449" id="Seg_9263" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_9264" s="T449">v-v:n.fin</ta>
            <ta e="T451" id="Seg_9265" s="T450">dempro</ta>
            <ta e="T454" id="Seg_9266" s="T453">propr-n:num</ta>
            <ta e="T455" id="Seg_9267" s="T454">n-n:num</ta>
            <ta e="T456" id="Seg_9268" s="T455">v-v&gt;v-v:pn</ta>
            <ta e="T457" id="Seg_9269" s="T456">conj</ta>
            <ta e="T458" id="Seg_9270" s="T457">adv</ta>
            <ta e="T459" id="Seg_9271" s="T458">n-n:num</ta>
            <ta e="T460" id="Seg_9272" s="T459">v-v&gt;v-v:pn</ta>
            <ta e="T461" id="Seg_9273" s="T460">dempro</ta>
            <ta e="T462" id="Seg_9274" s="T461">n-n:case</ta>
            <ta e="T463" id="Seg_9275" s="T462">v-v:tense-v:pn</ta>
            <ta e="T464" id="Seg_9276" s="T463">adv</ta>
            <ta e="T467" id="Seg_9277" s="T466">n-n:case.poss</ta>
            <ta e="T468" id="Seg_9278" s="T467">v-v:pn</ta>
            <ta e="T469" id="Seg_9279" s="T468">n-n:case.poss</ta>
            <ta e="T470" id="Seg_9280" s="T469">v-v:pn</ta>
            <ta e="T471" id="Seg_9281" s="T470">n-n:case.poss</ta>
            <ta e="T472" id="Seg_9282" s="T471">v-v:pn</ta>
            <ta e="T473" id="Seg_9283" s="T472">conj</ta>
            <ta e="T474" id="Seg_9284" s="T473">v-v:tense-v:pn</ta>
            <ta e="T475" id="Seg_9285" s="T474">adv</ta>
            <ta e="T476" id="Seg_9286" s="T475">n</ta>
            <ta e="T477" id="Seg_9287" s="T476">v-v:tense-v:pn</ta>
            <ta e="T478" id="Seg_9288" s="T477">dempro</ta>
            <ta e="T479" id="Seg_9289" s="T478">n-n:case.poss</ta>
            <ta e="T481" id="Seg_9290" s="T480">v-v&gt;v-v:pn</ta>
            <ta e="T482" id="Seg_9291" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_9292" s="T482">adj-n:case</ta>
            <ta e="T484" id="Seg_9293" s="T483">n-n:case</ta>
            <ta e="T485" id="Seg_9294" s="T484">conj</ta>
            <ta e="T486" id="Seg_9295" s="T485">v-v&gt;v-v:pn</ta>
            <ta e="T487" id="Seg_9296" s="T486">ptcl</ta>
            <ta e="T488" id="Seg_9297" s="T487">adv</ta>
            <ta e="T489" id="Seg_9298" s="T488">adj-n:case</ta>
            <ta e="T490" id="Seg_9299" s="T489">adj-n:case</ta>
            <ta e="T491" id="Seg_9300" s="T490">n-n:case.poss-n:case</ta>
            <ta e="T493" id="Seg_9301" s="T492">n-n:case</ta>
            <ta e="T495" id="Seg_9302" s="T494">v-v&gt;v-v:pn</ta>
            <ta e="T496" id="Seg_9303" s="T495">n-n:case</ta>
            <ta e="T497" id="Seg_9304" s="T496">quant</ta>
            <ta e="T498" id="Seg_9305" s="T497">n-n:case.poss-n:case</ta>
            <ta e="T499" id="Seg_9306" s="T498">dempro</ta>
            <ta e="T500" id="Seg_9307" s="T499">conj</ta>
            <ta e="T501" id="Seg_9308" s="T500">dempro-n:case</ta>
            <ta e="T502" id="Seg_9309" s="T501">n-n:case</ta>
            <ta e="T504" id="Seg_9310" s="T503">v-v:tense-v:pn</ta>
            <ta e="T505" id="Seg_9311" s="T504">v-v:tense-v:pn</ta>
            <ta e="T506" id="Seg_9312" s="T505">n-n:num</ta>
            <ta e="T507" id="Seg_9313" s="T506">ptcl</ta>
            <ta e="T509" id="Seg_9314" s="T508">ptcl</ta>
            <ta e="T510" id="Seg_9315" s="T509">v-v:tense-v:pn</ta>
            <ta e="T511" id="Seg_9316" s="T510">v-v:n.fin</ta>
            <ta e="T512" id="Seg_9317" s="T511">dempro-n:case</ta>
            <ta e="T513" id="Seg_9318" s="T512">n-n:case</ta>
            <ta e="T515" id="Seg_9319" s="T514">v-v&gt;v-v:pn</ta>
            <ta e="T516" id="Seg_9320" s="T515">n-n:case</ta>
            <ta e="T517" id="Seg_9321" s="T516">n-n:case</ta>
            <ta e="T518" id="Seg_9322" s="T517">ptcl</ta>
            <ta e="T519" id="Seg_9323" s="T518">quant</ta>
            <ta e="T520" id="Seg_9324" s="T519">n-n:case.poss</ta>
            <ta e="T521" id="Seg_9325" s="T520">que</ta>
            <ta e="T523" id="Seg_9326" s="T522">v-v&gt;v-v:pn</ta>
            <ta e="T524" id="Seg_9327" s="T523">ptcl</ta>
            <ta e="T525" id="Seg_9328" s="T524">v-v&gt;v-v:pn</ta>
            <ta e="T526" id="Seg_9329" s="T525">dempro</ta>
            <ta e="T527" id="Seg_9330" s="T526">n-n:case</ta>
            <ta e="T529" id="Seg_9331" s="T528">v-v&gt;v-v:pn</ta>
            <ta e="T530" id="Seg_9332" s="T529">quant</ta>
            <ta e="T532" id="Seg_9333" s="T530">n-n:case</ta>
            <ta e="T535" id="Seg_9334" s="T534">num-n:case</ta>
            <ta e="T536" id="Seg_9335" s="T535">n-n:case</ta>
            <ta e="T540" id="Seg_9336" s="T539">num-n:case</ta>
            <ta e="T542" id="Seg_9337" s="T541">num-n:case</ta>
            <ta e="T543" id="Seg_9338" s="T542">n-n:case</ta>
            <ta e="T544" id="Seg_9339" s="T543">conj</ta>
            <ta e="T545" id="Seg_9340" s="T544">v-v:n.fin</ta>
            <ta e="T546" id="Seg_9341" s="T545">v</ta>
            <ta e="T547" id="Seg_9342" s="T546">dempro</ta>
            <ta e="T548" id="Seg_9343" s="T547">n-n:case</ta>
            <ta e="T550" id="Seg_9344" s="T549">v-v&gt;v-v:pn</ta>
            <ta e="T551" id="Seg_9345" s="T550">n-n:case</ta>
            <ta e="T552" id="Seg_9346" s="T551">n-n:case</ta>
            <ta e="T553" id="Seg_9347" s="T552">refl-n:case.poss</ta>
            <ta e="T554" id="Seg_9348" s="T553">adj-n:case</ta>
            <ta e="T555" id="Seg_9349" s="T554">conj</ta>
            <ta e="T556" id="Seg_9350" s="T555">n-n:case.poss</ta>
            <ta e="T557" id="Seg_9351" s="T556">adv</ta>
            <ta e="T558" id="Seg_9352" s="T557">dempro</ta>
            <ta e="T561" id="Seg_9353" s="T560">pers</ta>
            <ta e="T562" id="Seg_9354" s="T561">v-v:tense-v:pn</ta>
            <ta e="T563" id="Seg_9355" s="T562">n-n:case</ta>
            <ta e="T564" id="Seg_9356" s="T563">conj</ta>
            <ta e="T565" id="Seg_9357" s="T564">v-v:n.fin</ta>
            <ta e="T566" id="Seg_9358" s="T565">ptcl</ta>
            <ta e="T567" id="Seg_9359" s="T566">v-v:tense-v:pn</ta>
            <ta e="T568" id="Seg_9360" s="T567">conj</ta>
            <ta e="T569" id="Seg_9361" s="T568">dempro-n:case</ta>
            <ta e="T570" id="Seg_9362" s="T569">que-n:case</ta>
            <ta e="T571" id="Seg_9363" s="T570">n-n:case</ta>
            <ta e="T573" id="Seg_9364" s="T572">v-v:tense-v:pn</ta>
            <ta e="T574" id="Seg_9365" s="T573">n-n:case</ta>
            <ta e="T575" id="Seg_9366" s="T574">conj</ta>
            <ta e="T576" id="Seg_9367" s="T575">adv</ta>
            <ta e="T577" id="Seg_9368" s="T576">n-n:num</ta>
            <ta e="T578" id="Seg_9369" s="T577">ptcl</ta>
            <ta e="T580" id="Seg_9370" s="T579">v-v:n.fin-v:pn</ta>
            <ta e="T581" id="Seg_9371" s="T580">ptcl</ta>
            <ta e="T583" id="Seg_9372" s="T582">dempro</ta>
            <ta e="T584" id="Seg_9373" s="T583">n-n:case</ta>
            <ta e="T585" id="Seg_9374" s="T584">v-v:tense-v:pn</ta>
            <ta e="T587" id="Seg_9375" s="T586">num-n:case</ta>
            <ta e="T588" id="Seg_9376" s="T587">v-v:tense-v:pn</ta>
            <ta e="T589" id="Seg_9377" s="T588">num-n:case</ta>
            <ta e="T590" id="Seg_9378" s="T589">v-v&gt;v-v:pn</ta>
            <ta e="T591" id="Seg_9379" s="T590">num-n:case</ta>
            <ta e="T592" id="Seg_9380" s="T591">v-v&gt;v-v:pn</ta>
            <ta e="T593" id="Seg_9381" s="T592">num-n:case</ta>
            <ta e="T594" id="Seg_9382" s="T593">v-v&gt;v-v:pn</ta>
            <ta e="T595" id="Seg_9383" s="T594">conj</ta>
            <ta e="T596" id="Seg_9384" s="T595">num-n:case</ta>
            <ta e="T598" id="Seg_9385" s="T597">v-v&gt;v-v:pn</ta>
            <ta e="T599" id="Seg_9386" s="T598">dempro</ta>
            <ta e="T600" id="Seg_9387" s="T599">n</ta>
            <ta e="T601" id="Seg_9388" s="T600">v-v:tense-v:pn</ta>
            <ta e="T604" id="Seg_9389" s="T603">n-n:case</ta>
            <ta e="T605" id="Seg_9390" s="T604">quant</ta>
            <ta e="T606" id="Seg_9391" s="T605">v-v&gt;v-v:pn</ta>
            <ta e="T607" id="Seg_9392" s="T606">conj</ta>
            <ta e="T608" id="Seg_9393" s="T607">n-n:case</ta>
            <ta e="T611" id="Seg_9394" s="T609">n-n:case.poss</ta>
            <ta e="T612" id="Seg_9395" s="T611">dempro</ta>
            <ta e="T613" id="Seg_9396" s="T612">n-n:case</ta>
            <ta e="T614" id="Seg_9397" s="T613">conj</ta>
            <ta e="T615" id="Seg_9398" s="T614">n-n:case.poss</ta>
            <ta e="T617" id="Seg_9399" s="T616">num-n:case</ta>
            <ta e="T618" id="Seg_9400" s="T617">n-n:case</ta>
            <ta e="T619" id="Seg_9401" s="T618">conj</ta>
            <ta e="T620" id="Seg_9402" s="T619">num-n:case</ta>
            <ta e="T621" id="Seg_9403" s="T620">adj-n:num</ta>
            <ta e="T622" id="Seg_9404" s="T621">conj</ta>
            <ta e="T623" id="Seg_9405" s="T622">n-n:case</ta>
            <ta e="T624" id="Seg_9406" s="T623">n-n:case</ta>
            <ta e="T625" id="Seg_9407" s="T624">n-n:num</ta>
            <ta e="T627" id="Seg_9408" s="T626">pers</ta>
            <ta e="T628" id="Seg_9409" s="T627">adv</ta>
            <ta e="T629" id="Seg_9410" s="T628">v-v&gt;v-v:pn</ta>
            <ta e="T630" id="Seg_9411" s="T629">v-v&gt;v-v:pn</ta>
            <ta e="T631" id="Seg_9412" s="T630">conj</ta>
            <ta e="T632" id="Seg_9413" s="T631">v-v:n.fin</ta>
            <ta e="T633" id="Seg_9414" s="T632">ptcl</ta>
            <ta e="T634" id="Seg_9415" s="T633">v-v:tense-v:pn</ta>
            <ta e="T635" id="Seg_9416" s="T634">n-n:case</ta>
            <ta e="T637" id="Seg_9417" s="T636">n-n:case</ta>
            <ta e="T638" id="Seg_9418" s="T637">v-v&gt;v-v:pn</ta>
            <ta e="T639" id="Seg_9419" s="T638">n-n:case</ta>
            <ta e="T640" id="Seg_9420" s="T639">n-n:case</ta>
            <ta e="T641" id="Seg_9421" s="T640">n-n:case</ta>
            <ta e="T642" id="Seg_9422" s="T641">conj</ta>
            <ta e="T643" id="Seg_9423" s="T642">v-v&gt;v-v:pn</ta>
            <ta e="T644" id="Seg_9424" s="T643">n-n:case</ta>
            <ta e="T645" id="Seg_9425" s="T644">dempro</ta>
            <ta e="T646" id="Seg_9426" s="T645">n-n:case</ta>
            <ta e="T648" id="Seg_9427" s="T647">v-v:tense-v:pn</ta>
            <ta e="T649" id="Seg_9428" s="T648">n-n:case</ta>
            <ta e="T650" id="Seg_9429" s="T649">n-n:case</ta>
            <ta e="T652" id="Seg_9430" s="T651">v-v&gt;v-v:pn</ta>
            <ta e="T653" id="Seg_9431" s="T652">conj</ta>
            <ta e="T654" id="Seg_9432" s="T653">refl-n:case.poss</ta>
            <ta e="T655" id="Seg_9433" s="T654">ptcl</ta>
            <ta e="T656" id="Seg_9434" s="T655">v-v&gt;v-v:pn</ta>
            <ta e="T657" id="Seg_9435" s="T656">dempro</ta>
            <ta e="T658" id="Seg_9436" s="T657">n-n:case</ta>
            <ta e="T660" id="Seg_9437" s="T659">num-num&gt;num</ta>
            <ta e="T661" id="Seg_9438" s="T660">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T662" id="Seg_9439" s="T661">num-n:case</ta>
            <ta e="T663" id="Seg_9440" s="T662">v-v:tense-v:pn</ta>
            <ta e="T664" id="Seg_9441" s="T663">num-n:case</ta>
            <ta e="T665" id="Seg_9442" s="T664">v-v:tense-v:pn</ta>
            <ta e="T666" id="Seg_9443" s="T665">dempro</ta>
            <ta e="T667" id="Seg_9444" s="T666">n-n:case.poss</ta>
            <ta e="T669" id="Seg_9445" s="T668">v-v&gt;v-v:ins-v:mood.pn</ta>
            <ta e="T670" id="Seg_9446" s="T669">pers</ta>
            <ta e="T671" id="Seg_9447" s="T670">v-v:ins-v:mood.pn</ta>
            <ta e="T672" id="Seg_9448" s="T671">pers</ta>
            <ta e="T673" id="Seg_9449" s="T672">v-v:mood.pn</ta>
            <ta e="T674" id="Seg_9450" s="T673">pers</ta>
            <ta e="T675" id="Seg_9451" s="T674">v-v:tense-v:pn</ta>
            <ta e="T676" id="Seg_9452" s="T675">pers</ta>
            <ta e="T677" id="Seg_9453" s="T676">dempro</ta>
            <ta e="T678" id="Seg_9454" s="T677">n-n:case</ta>
            <ta e="T680" id="Seg_9455" s="T679">v-v&gt;v-v:pn</ta>
            <ta e="T681" id="Seg_9456" s="T680">ptcl</ta>
            <ta e="T682" id="Seg_9457" s="T681">conj</ta>
            <ta e="T683" id="Seg_9458" s="T682">n-n:case</ta>
            <ta e="T684" id="Seg_9459" s="T683">ptcl</ta>
            <ta e="T685" id="Seg_9460" s="T684">v-v&gt;v-v:pn</ta>
            <ta e="T686" id="Seg_9461" s="T685">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T687" id="Seg_9462" s="T686">v-v&gt;v-v:pn</ta>
            <ta e="T688" id="Seg_9463" s="T687">dempro</ta>
            <ta e="T689" id="Seg_9464" s="T688">n-n:case</ta>
            <ta e="T690" id="Seg_9465" s="T689">v-v:tense-v:pn</ta>
            <ta e="T692" id="Seg_9466" s="T888">n-n:num</ta>
            <ta e="T693" id="Seg_9467" s="T692">n-n:case</ta>
            <ta e="T694" id="Seg_9468" s="T693">v-v&gt;v-v:pn</ta>
            <ta e="T697" id="Seg_9469" s="T696">dempro</ta>
            <ta e="T698" id="Seg_9470" s="T697">n-n:case</ta>
            <ta e="T700" id="Seg_9471" s="T699">adj-n:case</ta>
            <ta e="T701" id="Seg_9472" s="T700">n-n:case</ta>
            <ta e="T702" id="Seg_9473" s="T701">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T703" id="Seg_9474" s="T702">adv</ta>
            <ta e="T704" id="Seg_9475" s="T703">n-n:case</ta>
            <ta e="T705" id="Seg_9476" s="T704">ptcl</ta>
            <ta e="T706" id="Seg_9477" s="T705">v-v&gt;v-v:pn</ta>
            <ta e="T707" id="Seg_9478" s="T706">n-n:case</ta>
            <ta e="T708" id="Seg_9479" s="T707">dempro-n:case</ta>
            <ta e="T710" id="Seg_9480" s="T709">v-v:tense-v:pn</ta>
            <ta e="T711" id="Seg_9481" s="T710">n-n&gt;v-v:n.fin</ta>
            <ta e="T712" id="Seg_9482" s="T711">conj</ta>
            <ta e="T713" id="Seg_9483" s="T712">pers</ta>
            <ta e="T714" id="Seg_9484" s="T713">v-v:tense-v:pn</ta>
            <ta e="T715" id="Seg_9485" s="T714">n-n:case</ta>
            <ta e="T716" id="Seg_9486" s="T715">v-v:n.fin</ta>
            <ta e="T717" id="Seg_9487" s="T716">conj</ta>
            <ta e="T718" id="Seg_9488" s="T717">adv</ta>
            <ta e="T719" id="Seg_9489" s="T718">num-n:case</ta>
            <ta e="T720" id="Seg_9490" s="T719">n-n:case</ta>
            <ta e="T721" id="Seg_9491" s="T720">v-v:tense-v:pn</ta>
            <ta e="T724" id="Seg_9492" s="T723">n-n:case</ta>
            <ta e="T727" id="Seg_9493" s="T726">v-v:tense-v:pn</ta>
            <ta e="T728" id="Seg_9494" s="T727">n-n:case</ta>
            <ta e="T729" id="Seg_9495" s="T728">adv</ta>
            <ta e="T730" id="Seg_9496" s="T729">v-v:tense-v:pn</ta>
            <ta e="T731" id="Seg_9497" s="T730">v-v:tense-v:pn</ta>
            <ta e="T732" id="Seg_9498" s="T731">n-n:case</ta>
            <ta e="T733" id="Seg_9499" s="T732">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T734" id="Seg_9500" s="T733">conj</ta>
            <ta e="T735" id="Seg_9501" s="T734">adv</ta>
            <ta e="T736" id="Seg_9502" s="T735">v-v:tense-v:pn</ta>
            <ta e="T737" id="Seg_9503" s="T736">adv</ta>
            <ta e="T738" id="Seg_9504" s="T737">v-v:tense-v:pn</ta>
            <ta e="T739" id="Seg_9505" s="T738">que</ta>
            <ta e="T740" id="Seg_9506" s="T739">n-n:case</ta>
            <ta e="T741" id="Seg_9507" s="T740">v-v&gt;v-v:pn</ta>
            <ta e="T742" id="Seg_9508" s="T741">n-n:case.poss</ta>
            <ta e="T743" id="Seg_9509" s="T742">v-v&gt;v-v:pn</ta>
            <ta e="T744" id="Seg_9510" s="T743">adv</ta>
            <ta e="T745" id="Seg_9511" s="T744">adj-n:case</ta>
            <ta e="T747" id="Seg_9512" s="T746">v-v:tense-v:pn</ta>
            <ta e="T748" id="Seg_9513" s="T747">ptcl</ta>
            <ta e="T749" id="Seg_9514" s="T748">v-v:n.fin</ta>
            <ta e="T750" id="Seg_9515" s="T749">n-n:case</ta>
            <ta e="T751" id="Seg_9516" s="T750">ptcl</ta>
            <ta e="T752" id="Seg_9517" s="T751">v-v:n.fin</ta>
            <ta e="T753" id="Seg_9518" s="T752">ptcl</ta>
            <ta e="T754" id="Seg_9519" s="T753">adj</ta>
            <ta e="T755" id="Seg_9520" s="T754">v-v:tense-v:pn</ta>
            <ta e="T756" id="Seg_9521" s="T755">adv</ta>
            <ta e="T757" id="Seg_9522" s="T756">n-n:case</ta>
            <ta e="T758" id="Seg_9523" s="T757">v-v:tense-v:pn</ta>
            <ta e="T759" id="Seg_9524" s="T758">v-v:tense-v:pn</ta>
            <ta e="T760" id="Seg_9525" s="T759">adj-n:case</ta>
            <ta e="T761" id="Seg_9526" s="T760">n-n:case</ta>
            <ta e="T762" id="Seg_9527" s="T761">v-v:tense-v:pn</ta>
            <ta e="T763" id="Seg_9528" s="T762">conj</ta>
            <ta e="T764" id="Seg_9529" s="T763">adv</ta>
            <ta e="T766" id="Seg_9530" s="T765">v-v:tense-v:pn</ta>
            <ta e="T767" id="Seg_9531" s="T766">n-n:case</ta>
            <ta e="T768" id="Seg_9532" s="T767">adv</ta>
            <ta e="T769" id="Seg_9533" s="T768">n-n:case.poss</ta>
            <ta e="T770" id="Seg_9534" s="T769">%%</ta>
            <ta e="T771" id="Seg_9535" s="T770">n-n:case.poss-n:case</ta>
            <ta e="T772" id="Seg_9536" s="T771">adv</ta>
            <ta e="T773" id="Seg_9537" s="T772">v-v:pn</ta>
            <ta e="T774" id="Seg_9538" s="T773">v-v:n.fin</ta>
            <ta e="T775" id="Seg_9539" s="T774">conj</ta>
            <ta e="T776" id="Seg_9540" s="T775">n-n:case</ta>
            <ta e="T777" id="Seg_9541" s="T776">ptcl</ta>
            <ta e="T778" id="Seg_9542" s="T777">adv</ta>
            <ta e="T779" id="Seg_9543" s="T778">adv</ta>
            <ta e="T780" id="Seg_9544" s="T779">n-n:case</ta>
            <ta e="T781" id="Seg_9545" s="T780">v-v:tense-v:pn</ta>
            <ta e="T782" id="Seg_9546" s="T781">v-v:tense-v:pn</ta>
            <ta e="T783" id="Seg_9547" s="T782">n-adv:case</ta>
            <ta e="T784" id="Seg_9548" s="T783">v-v:tense-v:pn</ta>
            <ta e="T785" id="Seg_9549" s="T784">ptcl</ta>
            <ta e="T786" id="Seg_9550" s="T785">v-v:tense-v:pn</ta>
            <ta e="T787" id="Seg_9551" s="T786">n-n:num</ta>
            <ta e="T788" id="Seg_9552" s="T787">v-v:tense-v:pn</ta>
            <ta e="T789" id="Seg_9553" s="T788">v-v:tense-v:pn</ta>
            <ta e="T790" id="Seg_9554" s="T789">n-n:case.poss</ta>
            <ta e="T792" id="Seg_9555" s="T791">v-v:tense-v:pn</ta>
            <ta e="T793" id="Seg_9556" s="T792">n-n:case</ta>
            <ta e="T794" id="Seg_9557" s="T793">que=ptcl</ta>
            <ta e="T795" id="Seg_9558" s="T794">ptcl</ta>
            <ta e="T796" id="Seg_9559" s="T795">v-v:n.fin</ta>
            <ta e="T797" id="Seg_9560" s="T796">adv</ta>
            <ta e="T800" id="Seg_9561" s="T799">n-n:case</ta>
            <ta e="T801" id="Seg_9562" s="T800">v-v:tense-v:pn</ta>
            <ta e="T802" id="Seg_9563" s="T801">v-v:tense-v:pn</ta>
            <ta e="T803" id="Seg_9564" s="T802">adv</ta>
            <ta e="T804" id="Seg_9565" s="T803">v-v:tense-v:pn</ta>
            <ta e="T805" id="Seg_9566" s="T804">n-n:case</ta>
            <ta e="T806" id="Seg_9567" s="T805">n-n:case</ta>
            <ta e="T808" id="Seg_9568" s="T807">v-v:tense-v:pn</ta>
            <ta e="T809" id="Seg_9569" s="T808">conj</ta>
            <ta e="T810" id="Seg_9570" s="T809">n-n:case.poss</ta>
            <ta e="T811" id="Seg_9571" s="T810">v-v:tense-v:pn</ta>
            <ta e="T814" id="Seg_9572" s="T813">pers</ta>
            <ta e="T815" id="Seg_9573" s="T814">adv</ta>
            <ta e="T816" id="Seg_9574" s="T815">n</ta>
            <ta e="T817" id="Seg_9575" s="T816">v-v:tense-v:pn</ta>
            <ta e="T818" id="Seg_9576" s="T817">n-n:case</ta>
            <ta e="T819" id="Seg_9577" s="T818">v-v:tense-v:pn</ta>
            <ta e="T821" id="Seg_9578" s="T889">v-v:tense-v:pn</ta>
            <ta e="T822" id="Seg_9579" s="T821">adv</ta>
            <ta e="T823" id="Seg_9580" s="T822">num-n:case</ta>
            <ta e="T824" id="Seg_9581" s="T823">num-n:case</ta>
            <ta e="T825" id="Seg_9582" s="T824">n-n:case</ta>
            <ta e="T826" id="Seg_9583" s="T825">v-v:tense-v:pn</ta>
            <ta e="T827" id="Seg_9584" s="T826">adv</ta>
            <ta e="T828" id="Seg_9585" s="T827">v-v:tense-v:pn</ta>
            <ta e="T829" id="Seg_9586" s="T828">n-n:case</ta>
            <ta e="T830" id="Seg_9587" s="T829">conj</ta>
            <ta e="T831" id="Seg_9588" s="T830">v-v:tense-v:pn</ta>
            <ta e="T832" id="Seg_9589" s="T831">n-n:case</ta>
            <ta e="T833" id="Seg_9590" s="T832">conj</ta>
            <ta e="T834" id="Seg_9591" s="T833">pers</ta>
            <ta e="T835" id="Seg_9592" s="T834">v-v:tense-v:pn</ta>
            <ta e="T837" id="Seg_9593" s="T836">adv</ta>
            <ta e="T838" id="Seg_9594" s="T837">pers</ta>
            <ta e="T839" id="Seg_9595" s="T838">v-v:tense-v:pn</ta>
            <ta e="T840" id="Seg_9596" s="T839">num-n:case</ta>
            <ta e="T841" id="Seg_9597" s="T840">n-n:case</ta>
            <ta e="T842" id="Seg_9598" s="T841">n-n:case</ta>
            <ta e="T843" id="Seg_9599" s="T842">v-v:tense-v:pn</ta>
            <ta e="T844" id="Seg_9600" s="T843">num-n:case</ta>
            <ta e="T845" id="Seg_9601" s="T844">n-n:case</ta>
            <ta e="T846" id="Seg_9602" s="T845">num-n:case</ta>
            <ta e="T847" id="Seg_9603" s="T846">num-n:case</ta>
            <ta e="T848" id="Seg_9604" s="T847">v-v:tense-v:pn</ta>
            <ta e="T849" id="Seg_9605" s="T848">num-n:case</ta>
            <ta e="T850" id="Seg_9606" s="T849">n-n:case</ta>
            <ta e="T851" id="Seg_9607" s="T850">num-n:case</ta>
            <ta e="T852" id="Seg_9608" s="T851">num-n:case</ta>
            <ta e="T854" id="Seg_9609" s="T853">adv</ta>
            <ta e="T855" id="Seg_9610" s="T854">ptcl</ta>
            <ta e="T856" id="Seg_9611" s="T855">n-n:num</ta>
            <ta e="T857" id="Seg_9612" s="T856">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T858" id="Seg_9613" s="T857">v-v&gt;v-v:pn</ta>
            <ta e="T859" id="Seg_9614" s="T858">v-v:tense-v:pn</ta>
            <ta e="T860" id="Seg_9615" s="T859">adv</ta>
            <ta e="T861" id="Seg_9616" s="T860">n-n:case</ta>
            <ta e="T862" id="Seg_9617" s="T861">num-n:case</ta>
            <ta e="T864" id="Seg_9618" s="T863">num-n:case</ta>
            <ta e="T865" id="Seg_9619" s="T864">n-n:case</ta>
            <ta e="T866" id="Seg_9620" s="T865">v-v&gt;v-v:pn</ta>
            <ta e="T867" id="Seg_9621" s="T866">dempro</ta>
            <ta e="T868" id="Seg_9622" s="T867">v-v:tense-v:pn</ta>
            <ta e="T869" id="Seg_9623" s="T868">num-n:case</ta>
            <ta e="T870" id="Seg_9624" s="T869">ptcl</ta>
            <ta e="T871" id="Seg_9625" s="T870">n-n:num-n:case.poss</ta>
            <ta e="T872" id="Seg_9626" s="T871">num-n:case</ta>
            <ta e="T873" id="Seg_9627" s="T872">n-n:case</ta>
            <ta e="T874" id="Seg_9628" s="T873">v-v:tense-v:pn</ta>
            <ta e="T875" id="Seg_9629" s="T874">v-v:tense-v:pn</ta>
            <ta e="T876" id="Seg_9630" s="T875">n-n:num-n:case.poss</ta>
            <ta e="T877" id="Seg_9631" s="T876">v-v&gt;v-v:pn</ta>
            <ta e="T878" id="Seg_9632" s="T877">n-n:case</ta>
            <ta e="T879" id="Seg_9633" s="T878">n-n:case</ta>
            <ta e="T880" id="Seg_9634" s="T879">v-v:n.fin</ta>
            <ta e="T881" id="Seg_9635" s="T880">adv</ta>
            <ta e="T882" id="Seg_9636" s="T881">dempro-n:num-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T1" id="Seg_9637" s="T0">adv</ta>
            <ta e="T2" id="Seg_9638" s="T1">dempro</ta>
            <ta e="T3" id="Seg_9639" s="T2">n</ta>
            <ta e="T4" id="Seg_9640" s="T3">n</ta>
            <ta e="T5" id="Seg_9641" s="T4">v</ta>
            <ta e="T6" id="Seg_9642" s="T5">v</ta>
            <ta e="T7" id="Seg_9643" s="T6">v</ta>
            <ta e="T8" id="Seg_9644" s="T7">adv</ta>
            <ta e="T9" id="Seg_9645" s="T8">n</ta>
            <ta e="T10" id="Seg_9646" s="T9">n</ta>
            <ta e="T11" id="Seg_9647" s="T10">n</ta>
            <ta e="T12" id="Seg_9648" s="T11">v</ta>
            <ta e="T13" id="Seg_9649" s="T12">v</ta>
            <ta e="T14" id="Seg_9650" s="T13">v</ta>
            <ta e="T15" id="Seg_9651" s="T14">n</ta>
            <ta e="T16" id="Seg_9652" s="T15">num</ta>
            <ta e="T17" id="Seg_9653" s="T16">n</ta>
            <ta e="T18" id="Seg_9654" s="T17">adj</ta>
            <ta e="T19" id="Seg_9655" s="T18">adv</ta>
            <ta e="T20" id="Seg_9656" s="T19">dempro</ta>
            <ta e="T21" id="Seg_9657" s="T20">ptcl</ta>
            <ta e="T23" id="Seg_9658" s="T21">adv</ta>
            <ta e="T24" id="Seg_9659" s="T23">adv</ta>
            <ta e="T25" id="Seg_9660" s="T24">num</ta>
            <ta e="T27" id="Seg_9661" s="T26">adv</ta>
            <ta e="T28" id="Seg_9662" s="T27">n</ta>
            <ta e="T29" id="Seg_9663" s="T28">ptcl</ta>
            <ta e="T31" id="Seg_9664" s="T30">n</ta>
            <ta e="T33" id="Seg_9665" s="T32">dempro</ta>
            <ta e="T34" id="Seg_9666" s="T33">n</ta>
            <ta e="T35" id="Seg_9667" s="T34">v</ta>
            <ta e="T36" id="Seg_9668" s="T35">n</ta>
            <ta e="T37" id="Seg_9669" s="T36">adv</ta>
            <ta e="T38" id="Seg_9670" s="T37">dempro</ta>
            <ta e="T39" id="Seg_9671" s="T38">n</ta>
            <ta e="T40" id="Seg_9672" s="T39">v</ta>
            <ta e="T41" id="Seg_9673" s="T40">dempro</ta>
            <ta e="T42" id="Seg_9674" s="T41">adv</ta>
            <ta e="T44" id="Seg_9675" s="T43">n</ta>
            <ta e="T45" id="Seg_9676" s="T44">v</ta>
            <ta e="T46" id="Seg_9677" s="T45">adv</ta>
            <ta e="T47" id="Seg_9678" s="T46">v</ta>
            <ta e="T48" id="Seg_9679" s="T47">adv</ta>
            <ta e="T49" id="Seg_9680" s="T48">v</ta>
            <ta e="T50" id="Seg_9681" s="T49">v</ta>
            <ta e="T51" id="Seg_9682" s="T50">v</ta>
            <ta e="T52" id="Seg_9683" s="T51">adv</ta>
            <ta e="T53" id="Seg_9684" s="T52">v</ta>
            <ta e="T54" id="Seg_9685" s="T53">adj</ta>
            <ta e="T55" id="Seg_9686" s="T54">n</ta>
            <ta e="T57" id="Seg_9687" s="T56">adj</ta>
            <ta e="T58" id="Seg_9688" s="T57">num</ta>
            <ta e="T59" id="Seg_9689" s="T58">num</ta>
            <ta e="T60" id="Seg_9690" s="T59">n</ta>
            <ta e="T61" id="Seg_9691" s="T60">adv</ta>
            <ta e="T62" id="Seg_9692" s="T61">dempro</ta>
            <ta e="T63" id="Seg_9693" s="T62">adv</ta>
            <ta e="T65" id="Seg_9694" s="T64">adv</ta>
            <ta e="T66" id="Seg_9695" s="T65">dempro</ta>
            <ta e="T67" id="Seg_9696" s="T66">v</ta>
            <ta e="T68" id="Seg_9697" s="T67">n</ta>
            <ta e="T69" id="Seg_9698" s="T68">dempro</ta>
            <ta e="T70" id="Seg_9699" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_9700" s="T70">v</ta>
            <ta e="T72" id="Seg_9701" s="T71">n</ta>
            <ta e="T73" id="Seg_9702" s="T72">adv</ta>
            <ta e="T74" id="Seg_9703" s="T73">adj</ta>
            <ta e="T75" id="Seg_9704" s="T74">adv</ta>
            <ta e="T76" id="Seg_9705" s="T75">pers</ta>
            <ta e="T77" id="Seg_9706" s="T76">v</ta>
            <ta e="T78" id="Seg_9707" s="T77">adv</ta>
            <ta e="T80" id="Seg_9708" s="T79">n</ta>
            <ta e="T81" id="Seg_9709" s="T80">n</ta>
            <ta e="T82" id="Seg_9710" s="T81">v</ta>
            <ta e="T83" id="Seg_9711" s="T82">v</ta>
            <ta e="T84" id="Seg_9712" s="T83">adv</ta>
            <ta e="T85" id="Seg_9713" s="T84">v</ta>
            <ta e="T86" id="Seg_9714" s="T85">v</ta>
            <ta e="T88" id="Seg_9715" s="T87">num</ta>
            <ta e="T89" id="Seg_9716" s="T88">n</ta>
            <ta e="T90" id="Seg_9717" s="T89">v</ta>
            <ta e="T91" id="Seg_9718" s="T90">dempro</ta>
            <ta e="T92" id="Seg_9719" s="T91">n</ta>
            <ta e="T93" id="Seg_9720" s="T92">adj</ta>
            <ta e="T94" id="Seg_9721" s="T93">dempro</ta>
            <ta e="T95" id="Seg_9722" s="T94">adv</ta>
            <ta e="T97" id="Seg_9723" s="T96">adv</ta>
            <ta e="T98" id="Seg_9724" s="T97">dempro</ta>
            <ta e="T99" id="Seg_9725" s="T98">n</ta>
            <ta e="T100" id="Seg_9726" s="T99">v</ta>
            <ta e="T101" id="Seg_9727" s="T100">v</ta>
            <ta e="T102" id="Seg_9728" s="T101">dempro</ta>
            <ta e="T103" id="Seg_9729" s="T102">n</ta>
            <ta e="T105" id="Seg_9730" s="T104">v</ta>
            <ta e="T106" id="Seg_9731" s="T105">dempro</ta>
            <ta e="T107" id="Seg_9732" s="T106">que</ta>
            <ta e="T108" id="Seg_9733" s="T107">n</ta>
            <ta e="T109" id="Seg_9734" s="T108">v</ta>
            <ta e="T110" id="Seg_9735" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_9736" s="T110">pers</ta>
            <ta e="T112" id="Seg_9737" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_9738" s="T112">pers</ta>
            <ta e="T114" id="Seg_9739" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_9740" s="T114">n</ta>
            <ta e="T116" id="Seg_9741" s="T115">v</ta>
            <ta e="T117" id="Seg_9742" s="T116">pers</ta>
            <ta e="T118" id="Seg_9743" s="T117">pers</ta>
            <ta e="T119" id="Seg_9744" s="T118">adv</ta>
            <ta e="T120" id="Seg_9745" s="T119">v</ta>
            <ta e="T121" id="Seg_9746" s="T120">adv</ta>
            <ta e="T122" id="Seg_9747" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_9748" s="T122">v</ta>
            <ta e="T124" id="Seg_9749" s="T123">adv</ta>
            <ta e="T125" id="Seg_9750" s="T124">dempro</ta>
            <ta e="T126" id="Seg_9751" s="T125">dempro</ta>
            <ta e="T127" id="Seg_9752" s="T126">v</ta>
            <ta e="T129" id="Seg_9753" s="T128">v</ta>
            <ta e="T130" id="Seg_9754" s="T129">n</ta>
            <ta e="T131" id="Seg_9755" s="T130">n</ta>
            <ta e="T132" id="Seg_9756" s="T131">v</ta>
            <ta e="T133" id="Seg_9757" s="T132">n</ta>
            <ta e="T134" id="Seg_9758" s="T133">v</ta>
            <ta e="T135" id="Seg_9759" s="T134">v</ta>
            <ta e="T136" id="Seg_9760" s="T135">adv</ta>
            <ta e="T137" id="Seg_9761" s="T136">pers</ta>
            <ta e="T138" id="Seg_9762" s="T137">v</ta>
            <ta e="T139" id="Seg_9763" s="T138">adj</ta>
            <ta e="T140" id="Seg_9764" s="T139">v</ta>
            <ta e="T141" id="Seg_9765" s="T140">pers</ta>
            <ta e="T142" id="Seg_9766" s="T141">v</ta>
            <ta e="T143" id="Seg_9767" s="T142">adv</ta>
            <ta e="T144" id="Seg_9768" s="T143">dempro</ta>
            <ta e="T145" id="Seg_9769" s="T144">v</ta>
            <ta e="T146" id="Seg_9770" s="T145">v</ta>
            <ta e="T147" id="Seg_9771" s="T146">num</ta>
            <ta e="T148" id="Seg_9772" s="T147">n</ta>
            <ta e="T149" id="Seg_9773" s="T148">num</ta>
            <ta e="T150" id="Seg_9774" s="T149">n</ta>
            <ta e="T151" id="Seg_9775" s="T150">v</ta>
            <ta e="T152" id="Seg_9776" s="T151">adj</ta>
            <ta e="T153" id="Seg_9777" s="T152">adv</ta>
            <ta e="T154" id="Seg_9778" s="T153">v</ta>
            <ta e="T155" id="Seg_9779" s="T154">adv</ta>
            <ta e="T156" id="Seg_9780" s="T155">n</ta>
            <ta e="T157" id="Seg_9781" s="T156">adv</ta>
            <ta e="T158" id="Seg_9782" s="T157">adj</ta>
            <ta e="T159" id="Seg_9783" s="T158">n</ta>
            <ta e="T160" id="Seg_9784" s="T159">v</ta>
            <ta e="T161" id="Seg_9785" s="T160">n</ta>
            <ta e="T162" id="Seg_9786" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_9787" s="T162">v</ta>
            <ta e="T164" id="Seg_9788" s="T163">v</ta>
            <ta e="T165" id="Seg_9789" s="T164">v</ta>
            <ta e="T166" id="Seg_9790" s="T165">adv</ta>
            <ta e="T167" id="Seg_9791" s="T166">n</ta>
            <ta e="T168" id="Seg_9792" s="T167">n</ta>
            <ta e="T169" id="Seg_9793" s="T168">v</ta>
            <ta e="T170" id="Seg_9794" s="T169">conj</ta>
            <ta e="T171" id="Seg_9795" s="T170">n</ta>
            <ta e="T172" id="Seg_9796" s="T171">v</ta>
            <ta e="T173" id="Seg_9797" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_9798" s="T173">v</ta>
            <ta e="T175" id="Seg_9799" s="T174">pers</ta>
            <ta e="T176" id="Seg_9800" s="T175">n</ta>
            <ta e="T177" id="Seg_9801" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_9802" s="T177">v</ta>
            <ta e="T179" id="Seg_9803" s="T178">conj</ta>
            <ta e="T180" id="Seg_9804" s="T179">pers</ta>
            <ta e="T181" id="Seg_9805" s="T180">v</ta>
            <ta e="T182" id="Seg_9806" s="T181">v</ta>
            <ta e="T183" id="Seg_9807" s="T182">dempro</ta>
            <ta e="T184" id="Seg_9808" s="T183">n</ta>
            <ta e="T185" id="Seg_9809" s="T184">num</ta>
            <ta e="T186" id="Seg_9810" s="T185">v</ta>
            <ta e="T187" id="Seg_9811" s="T186">n</ta>
            <ta e="T188" id="Seg_9812" s="T187">v</ta>
            <ta e="T189" id="Seg_9813" s="T188">n</ta>
            <ta e="T190" id="Seg_9814" s="T189">v</ta>
            <ta e="T192" id="Seg_9815" s="T191">n</ta>
            <ta e="T193" id="Seg_9816" s="T192">que</ta>
            <ta e="T194" id="Seg_9817" s="T193">v</ta>
            <ta e="T195" id="Seg_9818" s="T194">num</ta>
            <ta e="T196" id="Seg_9819" s="T195">num</ta>
            <ta e="T198" id="Seg_9820" s="T197">num</ta>
            <ta e="T199" id="Seg_9821" s="T198">n</ta>
            <ta e="T200" id="Seg_9822" s="T199">v</ta>
            <ta e="T201" id="Seg_9823" s="T200">dempro</ta>
            <ta e="T202" id="Seg_9824" s="T201">v</ta>
            <ta e="T205" id="Seg_9825" s="T204">v</ta>
            <ta e="T206" id="Seg_9826" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_9827" s="T206">n</ta>
            <ta e="T208" id="Seg_9828" s="T207">conj</ta>
            <ta e="T209" id="Seg_9829" s="T208">pers</ta>
            <ta e="T210" id="Seg_9830" s="T209">n</ta>
            <ta e="T211" id="Seg_9831" s="T210">ptcl</ta>
            <ta e="T212" id="Seg_9832" s="T211">num</ta>
            <ta e="T213" id="Seg_9833" s="T212">n</ta>
            <ta e="T214" id="Seg_9834" s="T213">v</ta>
            <ta e="T215" id="Seg_9835" s="T214">dempro</ta>
            <ta e="T216" id="Seg_9836" s="T215">n</ta>
            <ta e="T217" id="Seg_9837" s="T216">v</ta>
            <ta e="T218" id="Seg_9838" s="T217">conj</ta>
            <ta e="T219" id="Seg_9839" s="T218">pers</ta>
            <ta e="T220" id="Seg_9840" s="T219">n</ta>
            <ta e="T221" id="Seg_9841" s="T220">ptcl</ta>
            <ta e="T222" id="Seg_9842" s="T221">n</ta>
            <ta e="T223" id="Seg_9843" s="T222">v</ta>
            <ta e="T224" id="Seg_9844" s="T223">conj</ta>
            <ta e="T225" id="Seg_9845" s="T224">que</ta>
            <ta e="T226" id="Seg_9846" s="T225">dempro</ta>
            <ta e="T227" id="Seg_9847" s="T226">que</ta>
            <ta e="T384" id="Seg_9848" s="T227">v</ta>
            <ta e="T228" id="Seg_9849" s="T384">v</ta>
            <ta e="T229" id="Seg_9850" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_9851" s="T229">v</ta>
            <ta e="T231" id="Seg_9852" s="T230">adv</ta>
            <ta e="T232" id="Seg_9853" s="T231">dempro</ta>
            <ta e="T233" id="Seg_9854" s="T232">n</ta>
            <ta e="T234" id="Seg_9855" s="T233">v</ta>
            <ta e="T235" id="Seg_9856" s="T234">v</ta>
            <ta e="T236" id="Seg_9857" s="T235">v</ta>
            <ta e="T239" id="Seg_9858" s="T238">v</ta>
            <ta e="T240" id="Seg_9859" s="T239">v</ta>
            <ta e="T241" id="Seg_9860" s="T240">v</ta>
            <ta e="T242" id="Seg_9861" s="T241">adv</ta>
            <ta e="T243" id="Seg_9862" s="T242">adv</ta>
            <ta e="T244" id="Seg_9863" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_9864" s="T244">dempro</ta>
            <ta e="T246" id="Seg_9865" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_9866" s="T246">v</ta>
            <ta e="T248" id="Seg_9867" s="T247">dempro</ta>
            <ta e="T249" id="Seg_9868" s="T248">v</ta>
            <ta e="T250" id="Seg_9869" s="T249">dempro</ta>
            <ta e="T252" id="Seg_9870" s="T251">v</ta>
            <ta e="T253" id="Seg_9871" s="T252">n</ta>
            <ta e="T254" id="Seg_9872" s="T253">dempro</ta>
            <ta e="T255" id="Seg_9873" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_9874" s="T255">v</ta>
            <ta e="T257" id="Seg_9875" s="T256">aux</ta>
            <ta e="T258" id="Seg_9876" s="T257">v</ta>
            <ta e="T259" id="Seg_9877" s="T258">pers</ta>
            <ta e="T260" id="Seg_9878" s="T259">pers</ta>
            <ta e="T261" id="Seg_9879" s="T260">n</ta>
            <ta e="T262" id="Seg_9880" s="T261">v</ta>
            <ta e="T263" id="Seg_9881" s="T262">n</ta>
            <ta e="T264" id="Seg_9882" s="T263">v</ta>
            <ta e="T265" id="Seg_9883" s="T264">n</ta>
            <ta e="T266" id="Seg_9884" s="T265">conj</ta>
            <ta e="T269" id="Seg_9885" s="T267">n</ta>
            <ta e="T270" id="Seg_9886" s="T269">n</ta>
            <ta e="T271" id="Seg_9887" s="T270">v</ta>
            <ta e="T272" id="Seg_9888" s="T271">n</ta>
            <ta e="T273" id="Seg_9889" s="T272">pers</ta>
            <ta e="T274" id="Seg_9890" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_9891" s="T274">adj</ta>
            <ta e="T276" id="Seg_9892" s="T275">que</ta>
            <ta e="T277" id="Seg_9893" s="T276">n</ta>
            <ta e="T278" id="Seg_9894" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_9895" s="T278">adj</ta>
            <ta e="T280" id="Seg_9896" s="T279">conj</ta>
            <ta e="T281" id="Seg_9897" s="T280">pers</ta>
            <ta e="T282" id="Seg_9898" s="T281">adj</ta>
            <ta e="T283" id="Seg_9899" s="T282">v</ta>
            <ta e="T284" id="Seg_9900" s="T883">n</ta>
            <ta e="T285" id="Seg_9901" s="T284">n</ta>
            <ta e="T286" id="Seg_9902" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_9903" s="T286">v</ta>
            <ta e="T288" id="Seg_9904" s="T287">conj</ta>
            <ta e="T289" id="Seg_9905" s="T288">refl</ta>
            <ta e="T290" id="Seg_9906" s="T289">v</ta>
            <ta e="T292" id="Seg_9907" s="T290">n</ta>
            <ta e="T293" id="Seg_9908" s="T292">adv</ta>
            <ta e="T294" id="Seg_9909" s="T293">adj</ta>
            <ta e="T295" id="Seg_9910" s="T294">v</ta>
            <ta e="T296" id="Seg_9911" s="T295">adv</ta>
            <ta e="T298" id="Seg_9912" s="T297">n</ta>
            <ta e="T299" id="Seg_9913" s="T298">ptcl</ta>
            <ta e="T300" id="Seg_9914" s="T299">n</ta>
            <ta e="T301" id="Seg_9915" s="T300">v</ta>
            <ta e="T302" id="Seg_9916" s="T301">que</ta>
            <ta e="T303" id="Seg_9917" s="T302">dempro</ta>
            <ta e="T304" id="Seg_9918" s="T303">adj</ta>
            <ta e="T305" id="Seg_9919" s="T304">n</ta>
            <ta e="T308" id="Seg_9920" s="T306">n</ta>
            <ta e="T310" id="Seg_9921" s="T309">n</ta>
            <ta e="T311" id="Seg_9922" s="T310">n</ta>
            <ta e="T312" id="Seg_9923" s="T311">v</ta>
            <ta e="T313" id="Seg_9924" s="T312">que</ta>
            <ta e="T314" id="Seg_9925" s="T313">adj</ta>
            <ta e="T315" id="Seg_9926" s="T314">n</ta>
            <ta e="T317" id="Seg_9927" s="T316">refl</ta>
            <ta e="T319" id="Seg_9928" s="T318">n</ta>
            <ta e="T320" id="Seg_9929" s="T319">v</ta>
            <ta e="T321" id="Seg_9930" s="T320">conj</ta>
            <ta e="T322" id="Seg_9931" s="T321">n</ta>
            <ta e="T323" id="Seg_9932" s="T322">adj</ta>
            <ta e="T324" id="Seg_9933" s="T323">n</ta>
            <ta e="T325" id="Seg_9934" s="T324">v</ta>
            <ta e="T326" id="Seg_9935" s="T325">n</ta>
            <ta e="T330" id="Seg_9936" s="T329">num</ta>
            <ta e="T332" id="Seg_9937" s="T331">n</ta>
            <ta e="T333" id="Seg_9938" s="T332">v</ta>
            <ta e="T335" id="Seg_9939" s="T334">v</ta>
            <ta e="T338" id="Seg_9940" s="T337">n</ta>
            <ta e="T339" id="Seg_9941" s="T338">n</ta>
            <ta e="T340" id="Seg_9942" s="T339">adj</ta>
            <ta e="T341" id="Seg_9943" s="T340">dempro</ta>
            <ta e="T342" id="Seg_9944" s="T341">n</ta>
            <ta e="T343" id="Seg_9945" s="T342">n</ta>
            <ta e="T345" id="Seg_9946" s="T344">n</ta>
            <ta e="T346" id="Seg_9947" s="T345">v</ta>
            <ta e="T347" id="Seg_9948" s="T346">conj</ta>
            <ta e="T348" id="Seg_9949" s="T347">n</ta>
            <ta e="T349" id="Seg_9950" s="T348">v</ta>
            <ta e="T350" id="Seg_9951" s="T349">conj</ta>
            <ta e="T352" id="Seg_9952" s="T350">v</ta>
            <ta e="T354" id="Seg_9953" s="T353">n</ta>
            <ta e="T355" id="Seg_9954" s="T354">v</ta>
            <ta e="T356" id="Seg_9955" s="T355">dempro</ta>
            <ta e="T357" id="Seg_9956" s="T356">n</ta>
            <ta e="T359" id="Seg_9957" s="T358">quant</ta>
            <ta e="T360" id="Seg_9958" s="T359">n</ta>
            <ta e="T361" id="Seg_9959" s="T360">n</ta>
            <ta e="T362" id="Seg_9960" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_9961" s="T362">num</ta>
            <ta e="T364" id="Seg_9962" s="T363">num</ta>
            <ta e="T366" id="Seg_9963" s="T365">conj</ta>
            <ta e="T368" id="Seg_9964" s="T367">n</ta>
            <ta e="T369" id="Seg_9965" s="T368">ptcl</ta>
            <ta e="T370" id="Seg_9966" s="T369">adv</ta>
            <ta e="T371" id="Seg_9967" s="T370">num</ta>
            <ta e="T372" id="Seg_9968" s="T371">num</ta>
            <ta e="T373" id="Seg_9969" s="T372">dempro</ta>
            <ta e="T374" id="Seg_9970" s="T373">n</ta>
            <ta e="T376" id="Seg_9971" s="T375">n</ta>
            <ta e="T377" id="Seg_9972" s="T376">v</ta>
            <ta e="T379" id="Seg_9973" s="T378">adj</ta>
            <ta e="T380" id="Seg_9974" s="T379">n</ta>
            <ta e="T381" id="Seg_9975" s="T380">conj</ta>
            <ta e="T382" id="Seg_9976" s="T381">v</ta>
            <ta e="T383" id="Seg_9977" s="T382">dempro</ta>
            <ta e="T385" id="Seg_9978" s="T885">n</ta>
            <ta e="T388" id="Seg_9979" s="T387">n</ta>
            <ta e="T390" id="Seg_9980" s="T389">n</ta>
            <ta e="T391" id="Seg_9981" s="T390">n</ta>
            <ta e="T392" id="Seg_9982" s="T391">v</ta>
            <ta e="T394" id="Seg_9983" s="T393">dempro</ta>
            <ta e="T395" id="Seg_9984" s="T394">n</ta>
            <ta e="T397" id="Seg_9985" s="T396">n</ta>
            <ta e="T399" id="Seg_9986" s="T398">n</ta>
            <ta e="T400" id="Seg_9987" s="T399">v</ta>
            <ta e="T401" id="Seg_9988" s="T400">conj</ta>
            <ta e="T402" id="Seg_9989" s="T401">n</ta>
            <ta e="T403" id="Seg_9990" s="T402">v</ta>
            <ta e="T404" id="Seg_9991" s="T403">dempro</ta>
            <ta e="T407" id="Seg_9992" s="T406">n</ta>
            <ta e="T408" id="Seg_9993" s="T407">n</ta>
            <ta e="T409" id="Seg_9994" s="T408">v</ta>
            <ta e="T410" id="Seg_9995" s="T409">conj</ta>
            <ta e="T411" id="Seg_9996" s="T410">n</ta>
            <ta e="T412" id="Seg_9997" s="T411">v</ta>
            <ta e="T415" id="Seg_9998" s="T414">conj</ta>
            <ta e="T416" id="Seg_9999" s="T415">n</ta>
            <ta e="T417" id="Seg_10000" s="T416">v</ta>
            <ta e="T418" id="Seg_10001" s="T417">v</ta>
            <ta e="T419" id="Seg_10002" s="T418">dempro</ta>
            <ta e="T420" id="Seg_10003" s="T419">n</ta>
            <ta e="T422" id="Seg_10004" s="T421">n</ta>
            <ta e="T423" id="Seg_10005" s="T422">n</ta>
            <ta e="T424" id="Seg_10006" s="T423">v</ta>
            <ta e="T425" id="Seg_10007" s="T424">conj</ta>
            <ta e="T426" id="Seg_10008" s="T425">n</ta>
            <ta e="T427" id="Seg_10009" s="T426">ptcl</ta>
            <ta e="T428" id="Seg_10010" s="T427">v</ta>
            <ta e="T429" id="Seg_10011" s="T428">dempro</ta>
            <ta e="T430" id="Seg_10012" s="T429">n</ta>
            <ta e="T432" id="Seg_10013" s="T431">adj</ta>
            <ta e="T434" id="Seg_10014" s="T433">n</ta>
            <ta e="T435" id="Seg_10015" s="T434">n</ta>
            <ta e="T436" id="Seg_10016" s="T435">n</ta>
            <ta e="T437" id="Seg_10017" s="T436">v</ta>
            <ta e="T438" id="Seg_10018" s="T437">ptcl</ta>
            <ta e="T439" id="Seg_10019" s="T438">v</ta>
            <ta e="T440" id="Seg_10020" s="T439">conj</ta>
            <ta e="T442" id="Seg_10021" s="T441">que</ta>
            <ta e="T443" id="Seg_10022" s="T442">n</ta>
            <ta e="T445" id="Seg_10023" s="T443">ptcl</ta>
            <ta e="T447" id="Seg_10024" s="T446">que</ta>
            <ta e="T448" id="Seg_10025" s="T447">n</ta>
            <ta e="T449" id="Seg_10026" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_10027" s="T449">v</ta>
            <ta e="T451" id="Seg_10028" s="T450">dempro</ta>
            <ta e="T454" id="Seg_10029" s="T453">n</ta>
            <ta e="T455" id="Seg_10030" s="T454">n</ta>
            <ta e="T456" id="Seg_10031" s="T455">v</ta>
            <ta e="T457" id="Seg_10032" s="T456">conj</ta>
            <ta e="T458" id="Seg_10033" s="T457">adv</ta>
            <ta e="T459" id="Seg_10034" s="T458">n</ta>
            <ta e="T460" id="Seg_10035" s="T459">v</ta>
            <ta e="T461" id="Seg_10036" s="T460">dempro</ta>
            <ta e="T462" id="Seg_10037" s="T461">n</ta>
            <ta e="T463" id="Seg_10038" s="T462">v</ta>
            <ta e="T464" id="Seg_10039" s="T463">adv</ta>
            <ta e="T467" id="Seg_10040" s="T466">n</ta>
            <ta e="T468" id="Seg_10041" s="T467">v</ta>
            <ta e="T469" id="Seg_10042" s="T468">n</ta>
            <ta e="T470" id="Seg_10043" s="T469">v</ta>
            <ta e="T471" id="Seg_10044" s="T470">n</ta>
            <ta e="T472" id="Seg_10045" s="T471">v</ta>
            <ta e="T473" id="Seg_10046" s="T472">conj</ta>
            <ta e="T474" id="Seg_10047" s="T473">v</ta>
            <ta e="T475" id="Seg_10048" s="T474">adv</ta>
            <ta e="T476" id="Seg_10049" s="T475">n</ta>
            <ta e="T477" id="Seg_10050" s="T476">v</ta>
            <ta e="T478" id="Seg_10051" s="T477">dempro</ta>
            <ta e="T479" id="Seg_10052" s="T478">n</ta>
            <ta e="T481" id="Seg_10053" s="T480">v</ta>
            <ta e="T482" id="Seg_10054" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_10055" s="T482">adj</ta>
            <ta e="T484" id="Seg_10056" s="T483">n</ta>
            <ta e="T485" id="Seg_10057" s="T484">conj</ta>
            <ta e="T486" id="Seg_10058" s="T485">v</ta>
            <ta e="T487" id="Seg_10059" s="T486">ptcl</ta>
            <ta e="T488" id="Seg_10060" s="T487">adv</ta>
            <ta e="T489" id="Seg_10061" s="T488">adj</ta>
            <ta e="T490" id="Seg_10062" s="T489">adj</ta>
            <ta e="T491" id="Seg_10063" s="T490">n</ta>
            <ta e="T493" id="Seg_10064" s="T492">n</ta>
            <ta e="T495" id="Seg_10065" s="T494">v</ta>
            <ta e="T496" id="Seg_10066" s="T495">n</ta>
            <ta e="T497" id="Seg_10067" s="T496">quant</ta>
            <ta e="T498" id="Seg_10068" s="T497">n</ta>
            <ta e="T499" id="Seg_10069" s="T498">dempro</ta>
            <ta e="T500" id="Seg_10070" s="T499">conj</ta>
            <ta e="T501" id="Seg_10071" s="T500">dempro</ta>
            <ta e="T502" id="Seg_10072" s="T501">n</ta>
            <ta e="T504" id="Seg_10073" s="T503">v</ta>
            <ta e="T505" id="Seg_10074" s="T504">v</ta>
            <ta e="T506" id="Seg_10075" s="T505">n</ta>
            <ta e="T507" id="Seg_10076" s="T506">ptcl</ta>
            <ta e="T509" id="Seg_10077" s="T508">ptcl</ta>
            <ta e="T510" id="Seg_10078" s="T509">v</ta>
            <ta e="T511" id="Seg_10079" s="T510">v</ta>
            <ta e="T512" id="Seg_10080" s="T511">dempro</ta>
            <ta e="T513" id="Seg_10081" s="T512">n</ta>
            <ta e="T515" id="Seg_10082" s="T514">v</ta>
            <ta e="T516" id="Seg_10083" s="T515">n</ta>
            <ta e="T517" id="Seg_10084" s="T516">n</ta>
            <ta e="T518" id="Seg_10085" s="T517">ptcl</ta>
            <ta e="T519" id="Seg_10086" s="T518">quant</ta>
            <ta e="T520" id="Seg_10087" s="T519">n</ta>
            <ta e="T521" id="Seg_10088" s="T520">que</ta>
            <ta e="T523" id="Seg_10089" s="T522">v</ta>
            <ta e="T524" id="Seg_10090" s="T523">ptcl</ta>
            <ta e="T525" id="Seg_10091" s="T524">v</ta>
            <ta e="T526" id="Seg_10092" s="T525">dempro</ta>
            <ta e="T527" id="Seg_10093" s="T526">n</ta>
            <ta e="T529" id="Seg_10094" s="T528">v</ta>
            <ta e="T530" id="Seg_10095" s="T529">quant</ta>
            <ta e="T532" id="Seg_10096" s="T530">n</ta>
            <ta e="T535" id="Seg_10097" s="T534">num</ta>
            <ta e="T536" id="Seg_10098" s="T535">n</ta>
            <ta e="T540" id="Seg_10099" s="T539">num</ta>
            <ta e="T542" id="Seg_10100" s="T541">num</ta>
            <ta e="T543" id="Seg_10101" s="T542">n</ta>
            <ta e="T544" id="Seg_10102" s="T543">conj</ta>
            <ta e="T545" id="Seg_10103" s="T544">v</ta>
            <ta e="T546" id="Seg_10104" s="T545">v</ta>
            <ta e="T547" id="Seg_10105" s="T546">dempro</ta>
            <ta e="T548" id="Seg_10106" s="T547">n</ta>
            <ta e="T550" id="Seg_10107" s="T549">v</ta>
            <ta e="T551" id="Seg_10108" s="T550">n</ta>
            <ta e="T552" id="Seg_10109" s="T551">n</ta>
            <ta e="T553" id="Seg_10110" s="T552">refl</ta>
            <ta e="T554" id="Seg_10111" s="T553">adj</ta>
            <ta e="T555" id="Seg_10112" s="T554">conj</ta>
            <ta e="T556" id="Seg_10113" s="T555">n</ta>
            <ta e="T557" id="Seg_10114" s="T556">adv</ta>
            <ta e="T558" id="Seg_10115" s="T557">dempro</ta>
            <ta e="T561" id="Seg_10116" s="T560">pers</ta>
            <ta e="T562" id="Seg_10117" s="T561">v</ta>
            <ta e="T563" id="Seg_10118" s="T562">n</ta>
            <ta e="T564" id="Seg_10119" s="T563">conj</ta>
            <ta e="T565" id="Seg_10120" s="T564">v</ta>
            <ta e="T566" id="Seg_10121" s="T565">ptcl</ta>
            <ta e="T567" id="Seg_10122" s="T566">v</ta>
            <ta e="T568" id="Seg_10123" s="T567">conj</ta>
            <ta e="T569" id="Seg_10124" s="T568">dempro</ta>
            <ta e="T570" id="Seg_10125" s="T569">que</ta>
            <ta e="T571" id="Seg_10126" s="T570">n</ta>
            <ta e="T573" id="Seg_10127" s="T572">v</ta>
            <ta e="T574" id="Seg_10128" s="T573">n</ta>
            <ta e="T575" id="Seg_10129" s="T574">conj</ta>
            <ta e="T576" id="Seg_10130" s="T575">adv</ta>
            <ta e="T577" id="Seg_10131" s="T576">n</ta>
            <ta e="T578" id="Seg_10132" s="T577">ptcl</ta>
            <ta e="T580" id="Seg_10133" s="T579">adj</ta>
            <ta e="T581" id="Seg_10134" s="T580">ptcl</ta>
            <ta e="T583" id="Seg_10135" s="T582">dempro</ta>
            <ta e="T584" id="Seg_10136" s="T583">n</ta>
            <ta e="T585" id="Seg_10137" s="T584">v</ta>
            <ta e="T587" id="Seg_10138" s="T586">num</ta>
            <ta e="T588" id="Seg_10139" s="T587">v</ta>
            <ta e="T589" id="Seg_10140" s="T588">num</ta>
            <ta e="T590" id="Seg_10141" s="T589">v</ta>
            <ta e="T591" id="Seg_10142" s="T590">num</ta>
            <ta e="T592" id="Seg_10143" s="T591">v</ta>
            <ta e="T593" id="Seg_10144" s="T592">num</ta>
            <ta e="T594" id="Seg_10145" s="T593">v</ta>
            <ta e="T595" id="Seg_10146" s="T594">conj</ta>
            <ta e="T596" id="Seg_10147" s="T595">num</ta>
            <ta e="T598" id="Seg_10148" s="T597">v</ta>
            <ta e="T599" id="Seg_10149" s="T598">dempro</ta>
            <ta e="T600" id="Seg_10150" s="T599">n</ta>
            <ta e="T601" id="Seg_10151" s="T600">v</ta>
            <ta e="T604" id="Seg_10152" s="T603">n</ta>
            <ta e="T605" id="Seg_10153" s="T604">quant</ta>
            <ta e="T606" id="Seg_10154" s="T605">v</ta>
            <ta e="T607" id="Seg_10155" s="T606">conj</ta>
            <ta e="T608" id="Seg_10156" s="T607">n</ta>
            <ta e="T611" id="Seg_10157" s="T609">n</ta>
            <ta e="T612" id="Seg_10158" s="T611">dempro</ta>
            <ta e="T613" id="Seg_10159" s="T612">n</ta>
            <ta e="T614" id="Seg_10160" s="T613">conj</ta>
            <ta e="T615" id="Seg_10161" s="T614">n</ta>
            <ta e="T617" id="Seg_10162" s="T616">num</ta>
            <ta e="T618" id="Seg_10163" s="T617">n</ta>
            <ta e="T619" id="Seg_10164" s="T618">conj</ta>
            <ta e="T620" id="Seg_10165" s="T619">num</ta>
            <ta e="T621" id="Seg_10166" s="T620">adj</ta>
            <ta e="T622" id="Seg_10167" s="T621">conj</ta>
            <ta e="T623" id="Seg_10168" s="T622">n</ta>
            <ta e="T624" id="Seg_10169" s="T623">n</ta>
            <ta e="T625" id="Seg_10170" s="T624">n</ta>
            <ta e="T627" id="Seg_10171" s="T626">pers</ta>
            <ta e="T628" id="Seg_10172" s="T627">adv</ta>
            <ta e="T629" id="Seg_10173" s="T628">v</ta>
            <ta e="T630" id="Seg_10174" s="T629">v</ta>
            <ta e="T631" id="Seg_10175" s="T630">conj</ta>
            <ta e="T632" id="Seg_10176" s="T631">v</ta>
            <ta e="T633" id="Seg_10177" s="T632">ptcl</ta>
            <ta e="T634" id="Seg_10178" s="T633">v</ta>
            <ta e="T635" id="Seg_10179" s="T634">n</ta>
            <ta e="T637" id="Seg_10180" s="T636">n</ta>
            <ta e="T638" id="Seg_10181" s="T637">v</ta>
            <ta e="T639" id="Seg_10182" s="T638">n</ta>
            <ta e="T640" id="Seg_10183" s="T639">n</ta>
            <ta e="T641" id="Seg_10184" s="T640">n</ta>
            <ta e="T642" id="Seg_10185" s="T641">conj</ta>
            <ta e="T643" id="Seg_10186" s="T642">v</ta>
            <ta e="T644" id="Seg_10187" s="T643">n</ta>
            <ta e="T645" id="Seg_10188" s="T644">dempro</ta>
            <ta e="T646" id="Seg_10189" s="T645">n</ta>
            <ta e="T648" id="Seg_10190" s="T647">v</ta>
            <ta e="T649" id="Seg_10191" s="T648">n</ta>
            <ta e="T650" id="Seg_10192" s="T649">n</ta>
            <ta e="T652" id="Seg_10193" s="T651">v</ta>
            <ta e="T653" id="Seg_10194" s="T652">conj</ta>
            <ta e="T654" id="Seg_10195" s="T653">refl</ta>
            <ta e="T655" id="Seg_10196" s="T654">ptcl</ta>
            <ta e="T656" id="Seg_10197" s="T655">v</ta>
            <ta e="T657" id="Seg_10198" s="T656">dempro</ta>
            <ta e="T658" id="Seg_10199" s="T657">n</ta>
            <ta e="T660" id="Seg_10200" s="T659">num</ta>
            <ta e="T661" id="Seg_10201" s="T660">v</ta>
            <ta e="T662" id="Seg_10202" s="T661">num</ta>
            <ta e="T663" id="Seg_10203" s="T662">v</ta>
            <ta e="T664" id="Seg_10204" s="T663">num</ta>
            <ta e="T665" id="Seg_10205" s="T664">v</ta>
            <ta e="T666" id="Seg_10206" s="T665">dempro</ta>
            <ta e="T667" id="Seg_10207" s="T666">n</ta>
            <ta e="T669" id="Seg_10208" s="T668">v</ta>
            <ta e="T670" id="Seg_10209" s="T669">pers</ta>
            <ta e="T671" id="Seg_10210" s="T670">v</ta>
            <ta e="T672" id="Seg_10211" s="T671">pers</ta>
            <ta e="T673" id="Seg_10212" s="T672">v</ta>
            <ta e="T674" id="Seg_10213" s="T673">pers</ta>
            <ta e="T675" id="Seg_10214" s="T674">v</ta>
            <ta e="T676" id="Seg_10215" s="T675">pers</ta>
            <ta e="T677" id="Seg_10216" s="T676">dempro</ta>
            <ta e="T678" id="Seg_10217" s="T677">n</ta>
            <ta e="T680" id="Seg_10218" s="T679">v</ta>
            <ta e="T681" id="Seg_10219" s="T680">ptcl</ta>
            <ta e="T682" id="Seg_10220" s="T681">conj</ta>
            <ta e="T683" id="Seg_10221" s="T682">n</ta>
            <ta e="T684" id="Seg_10222" s="T683">ptcl</ta>
            <ta e="T685" id="Seg_10223" s="T684">v</ta>
            <ta e="T686" id="Seg_10224" s="T685">v</ta>
            <ta e="T687" id="Seg_10225" s="T686">v</ta>
            <ta e="T688" id="Seg_10226" s="T687">dempro</ta>
            <ta e="T689" id="Seg_10227" s="T688">n</ta>
            <ta e="T690" id="Seg_10228" s="T689">v</ta>
            <ta e="T692" id="Seg_10229" s="T888">n</ta>
            <ta e="T693" id="Seg_10230" s="T692">n</ta>
            <ta e="T694" id="Seg_10231" s="T693">v</ta>
            <ta e="T697" id="Seg_10232" s="T696">dempro</ta>
            <ta e="T698" id="Seg_10233" s="T697">n</ta>
            <ta e="T700" id="Seg_10234" s="T699">adj</ta>
            <ta e="T701" id="Seg_10235" s="T700">n</ta>
            <ta e="T702" id="Seg_10236" s="T701">v</ta>
            <ta e="T703" id="Seg_10237" s="T702">adv</ta>
            <ta e="T704" id="Seg_10238" s="T703">n</ta>
            <ta e="T705" id="Seg_10239" s="T704">ptcl</ta>
            <ta e="T706" id="Seg_10240" s="T705">v</ta>
            <ta e="T707" id="Seg_10241" s="T706">n</ta>
            <ta e="T708" id="Seg_10242" s="T707">dempro</ta>
            <ta e="T710" id="Seg_10243" s="T709">v</ta>
            <ta e="T711" id="Seg_10244" s="T710">v</ta>
            <ta e="T712" id="Seg_10245" s="T711">conj</ta>
            <ta e="T713" id="Seg_10246" s="T712">pers</ta>
            <ta e="T714" id="Seg_10247" s="T713">v</ta>
            <ta e="T715" id="Seg_10248" s="T714">n</ta>
            <ta e="T716" id="Seg_10249" s="T715">v</ta>
            <ta e="T717" id="Seg_10250" s="T716">conj</ta>
            <ta e="T718" id="Seg_10251" s="T717">adv</ta>
            <ta e="T719" id="Seg_10252" s="T718">num</ta>
            <ta e="T720" id="Seg_10253" s="T719">n</ta>
            <ta e="T721" id="Seg_10254" s="T720">v</ta>
            <ta e="T724" id="Seg_10255" s="T723">n</ta>
            <ta e="T727" id="Seg_10256" s="T726">v</ta>
            <ta e="T728" id="Seg_10257" s="T727">n</ta>
            <ta e="T729" id="Seg_10258" s="T728">adv</ta>
            <ta e="T730" id="Seg_10259" s="T729">v</ta>
            <ta e="T731" id="Seg_10260" s="T730">v</ta>
            <ta e="T732" id="Seg_10261" s="T731">n</ta>
            <ta e="T733" id="Seg_10262" s="T732">v</ta>
            <ta e="T734" id="Seg_10263" s="T733">conj</ta>
            <ta e="T735" id="Seg_10264" s="T734">adj</ta>
            <ta e="T736" id="Seg_10265" s="T735">v</ta>
            <ta e="T737" id="Seg_10266" s="T736">adv</ta>
            <ta e="T738" id="Seg_10267" s="T737">v</ta>
            <ta e="T739" id="Seg_10268" s="T738">que</ta>
            <ta e="T740" id="Seg_10269" s="T739">n</ta>
            <ta e="T741" id="Seg_10270" s="T740">v</ta>
            <ta e="T742" id="Seg_10271" s="T741">n</ta>
            <ta e="T743" id="Seg_10272" s="T742">v</ta>
            <ta e="T744" id="Seg_10273" s="T743">adv</ta>
            <ta e="T745" id="Seg_10274" s="T744">adj</ta>
            <ta e="T747" id="Seg_10275" s="T746">v</ta>
            <ta e="T748" id="Seg_10276" s="T747">ptcl</ta>
            <ta e="T749" id="Seg_10277" s="T748">v</ta>
            <ta e="T750" id="Seg_10278" s="T749">n</ta>
            <ta e="T751" id="Seg_10279" s="T750">ptcl</ta>
            <ta e="T752" id="Seg_10280" s="T751">v</ta>
            <ta e="T753" id="Seg_10281" s="T752">ptcl</ta>
            <ta e="T754" id="Seg_10282" s="T753">adj</ta>
            <ta e="T755" id="Seg_10283" s="T754">v</ta>
            <ta e="T756" id="Seg_10284" s="T755">adv</ta>
            <ta e="T757" id="Seg_10285" s="T756">n</ta>
            <ta e="T758" id="Seg_10286" s="T757">v</ta>
            <ta e="T759" id="Seg_10287" s="T758">v</ta>
            <ta e="T760" id="Seg_10288" s="T759">adj</ta>
            <ta e="T761" id="Seg_10289" s="T760">n</ta>
            <ta e="T762" id="Seg_10290" s="T761">v</ta>
            <ta e="T763" id="Seg_10291" s="T762">conj</ta>
            <ta e="T764" id="Seg_10292" s="T763">adv</ta>
            <ta e="T766" id="Seg_10293" s="T765">v</ta>
            <ta e="T767" id="Seg_10294" s="T766">n</ta>
            <ta e="T768" id="Seg_10295" s="T767">adv</ta>
            <ta e="T769" id="Seg_10296" s="T768">n</ta>
            <ta e="T771" id="Seg_10297" s="T770">n</ta>
            <ta e="T772" id="Seg_10298" s="T771">adv</ta>
            <ta e="T773" id="Seg_10299" s="T772">v</ta>
            <ta e="T774" id="Seg_10300" s="T773">v</ta>
            <ta e="T775" id="Seg_10301" s="T774">conj</ta>
            <ta e="T776" id="Seg_10302" s="T775">n</ta>
            <ta e="T777" id="Seg_10303" s="T776">ptcl</ta>
            <ta e="T778" id="Seg_10304" s="T777">adv</ta>
            <ta e="T779" id="Seg_10305" s="T778">adv</ta>
            <ta e="T780" id="Seg_10306" s="T779">n</ta>
            <ta e="T781" id="Seg_10307" s="T780">v</ta>
            <ta e="T782" id="Seg_10308" s="T781">v</ta>
            <ta e="T783" id="Seg_10309" s="T782">adv</ta>
            <ta e="T784" id="Seg_10310" s="T783">v</ta>
            <ta e="T785" id="Seg_10311" s="T784">ptcl</ta>
            <ta e="T786" id="Seg_10312" s="T785">v</ta>
            <ta e="T787" id="Seg_10313" s="T786">n</ta>
            <ta e="T788" id="Seg_10314" s="T787">v</ta>
            <ta e="T789" id="Seg_10315" s="T788">v</ta>
            <ta e="T790" id="Seg_10316" s="T789">n</ta>
            <ta e="T792" id="Seg_10317" s="T791">v</ta>
            <ta e="T793" id="Seg_10318" s="T792">n</ta>
            <ta e="T794" id="Seg_10319" s="T793">que</ta>
            <ta e="T795" id="Seg_10320" s="T794">ptcl</ta>
            <ta e="T796" id="Seg_10321" s="T795">v</ta>
            <ta e="T797" id="Seg_10322" s="T796">adv</ta>
            <ta e="T800" id="Seg_10323" s="T799">n</ta>
            <ta e="T801" id="Seg_10324" s="T800">v</ta>
            <ta e="T802" id="Seg_10325" s="T801">v</ta>
            <ta e="T803" id="Seg_10326" s="T802">adv</ta>
            <ta e="T804" id="Seg_10327" s="T803">v</ta>
            <ta e="T805" id="Seg_10328" s="T804">n</ta>
            <ta e="T806" id="Seg_10329" s="T805">n</ta>
            <ta e="T808" id="Seg_10330" s="T807">v</ta>
            <ta e="T809" id="Seg_10331" s="T808">conj</ta>
            <ta e="T810" id="Seg_10332" s="T809">n</ta>
            <ta e="T811" id="Seg_10333" s="T810">v</ta>
            <ta e="T814" id="Seg_10334" s="T813">pers</ta>
            <ta e="T815" id="Seg_10335" s="T814">adv</ta>
            <ta e="T816" id="Seg_10336" s="T815">n</ta>
            <ta e="T817" id="Seg_10337" s="T816">v</ta>
            <ta e="T818" id="Seg_10338" s="T817">n</ta>
            <ta e="T819" id="Seg_10339" s="T818">v</ta>
            <ta e="T821" id="Seg_10340" s="T889">v</ta>
            <ta e="T822" id="Seg_10341" s="T821">adv</ta>
            <ta e="T823" id="Seg_10342" s="T822">num</ta>
            <ta e="T824" id="Seg_10343" s="T823">num</ta>
            <ta e="T825" id="Seg_10344" s="T824">n</ta>
            <ta e="T826" id="Seg_10345" s="T825">v</ta>
            <ta e="T827" id="Seg_10346" s="T826">adv</ta>
            <ta e="T828" id="Seg_10347" s="T827">v</ta>
            <ta e="T829" id="Seg_10348" s="T828">n</ta>
            <ta e="T830" id="Seg_10349" s="T829">conj</ta>
            <ta e="T831" id="Seg_10350" s="T830">v</ta>
            <ta e="T832" id="Seg_10351" s="T831">n</ta>
            <ta e="T833" id="Seg_10352" s="T832">conj</ta>
            <ta e="T834" id="Seg_10353" s="T833">pers</ta>
            <ta e="T835" id="Seg_10354" s="T834">v</ta>
            <ta e="T837" id="Seg_10355" s="T836">adv</ta>
            <ta e="T838" id="Seg_10356" s="T837">pers</ta>
            <ta e="T839" id="Seg_10357" s="T838">v</ta>
            <ta e="T840" id="Seg_10358" s="T839">num</ta>
            <ta e="T841" id="Seg_10359" s="T840">n</ta>
            <ta e="T842" id="Seg_10360" s="T841">n</ta>
            <ta e="T843" id="Seg_10361" s="T842">v</ta>
            <ta e="T844" id="Seg_10362" s="T843">num</ta>
            <ta e="T845" id="Seg_10363" s="T844">n</ta>
            <ta e="T846" id="Seg_10364" s="T845">num</ta>
            <ta e="T847" id="Seg_10365" s="T846">num</ta>
            <ta e="T848" id="Seg_10366" s="T847">v</ta>
            <ta e="T849" id="Seg_10367" s="T848">num</ta>
            <ta e="T850" id="Seg_10368" s="T849">n</ta>
            <ta e="T851" id="Seg_10369" s="T850">num</ta>
            <ta e="T852" id="Seg_10370" s="T851">num</ta>
            <ta e="T854" id="Seg_10371" s="T853">adv</ta>
            <ta e="T855" id="Seg_10372" s="T854">ptcl</ta>
            <ta e="T856" id="Seg_10373" s="T855">n</ta>
            <ta e="T857" id="Seg_10374" s="T856">v</ta>
            <ta e="T858" id="Seg_10375" s="T857">v</ta>
            <ta e="T859" id="Seg_10376" s="T858">v</ta>
            <ta e="T860" id="Seg_10377" s="T859">adv</ta>
            <ta e="T861" id="Seg_10378" s="T860">n</ta>
            <ta e="T862" id="Seg_10379" s="T861">num</ta>
            <ta e="T864" id="Seg_10380" s="T863">num</ta>
            <ta e="T865" id="Seg_10381" s="T864">n</ta>
            <ta e="T866" id="Seg_10382" s="T865">n</ta>
            <ta e="T867" id="Seg_10383" s="T866">dempro</ta>
            <ta e="T868" id="Seg_10384" s="T867">v</ta>
            <ta e="T869" id="Seg_10385" s="T868">num</ta>
            <ta e="T870" id="Seg_10386" s="T869">ptcl</ta>
            <ta e="T871" id="Seg_10387" s="T870">n</ta>
            <ta e="T872" id="Seg_10388" s="T871">num</ta>
            <ta e="T873" id="Seg_10389" s="T872">n</ta>
            <ta e="T874" id="Seg_10390" s="T873">v</ta>
            <ta e="T875" id="Seg_10391" s="T874">v</ta>
            <ta e="T876" id="Seg_10392" s="T875">n</ta>
            <ta e="T877" id="Seg_10393" s="T876">v</ta>
            <ta e="T878" id="Seg_10394" s="T877">n</ta>
            <ta e="T879" id="Seg_10395" s="T878">n</ta>
            <ta e="T880" id="Seg_10396" s="T879">v</ta>
            <ta e="T881" id="Seg_10397" s="T880">adv</ta>
            <ta e="T882" id="Seg_10398" s="T881">dempro</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T3" id="Seg_10399" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_10400" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_10401" s="T5">v:pred 0.3.h:S</ta>
            <ta e="T7" id="Seg_10402" s="T6">v:pred 0.3.h:S</ta>
            <ta e="T12" id="Seg_10403" s="T11">v:pred 0.3.h:S</ta>
            <ta e="T13" id="Seg_10404" s="T12">v:pred 0.3.h:S</ta>
            <ta e="T14" id="Seg_10405" s="T13">v:pred</ta>
            <ta e="T15" id="Seg_10406" s="T14">np.h:S</ta>
            <ta e="T17" id="Seg_10407" s="T16">np:S</ta>
            <ta e="T18" id="Seg_10408" s="T17">adj:pred</ta>
            <ta e="T28" id="Seg_10409" s="T27">np:S</ta>
            <ta e="T34" id="Seg_10410" s="T33">np:S</ta>
            <ta e="T35" id="Seg_10411" s="T34">v:pred</ta>
            <ta e="T39" id="Seg_10412" s="T38">np.h:S</ta>
            <ta e="T40" id="Seg_10413" s="T39">v:pred</ta>
            <ta e="T44" id="Seg_10414" s="T43">np:S</ta>
            <ta e="T45" id="Seg_10415" s="T44">v:pred</ta>
            <ta e="T47" id="Seg_10416" s="T46">v:pred 0.1.h:S</ta>
            <ta e="T49" id="Seg_10417" s="T48">v:pred 0.3.h:S</ta>
            <ta e="T50" id="Seg_10418" s="T49">v:pred 0.3.h:S</ta>
            <ta e="T51" id="Seg_10419" s="T50">v:pred 0.3.h:S</ta>
            <ta e="T53" id="Seg_10420" s="T52">v:pred 0.3.h:S</ta>
            <ta e="T60" id="Seg_10421" s="T59">np.h:S</ta>
            <ta e="T62" id="Seg_10422" s="T61">pro.h:S</ta>
            <ta e="T66" id="Seg_10423" s="T65">pro:S</ta>
            <ta e="T67" id="Seg_10424" s="T66">v:pred</ta>
            <ta e="T69" id="Seg_10425" s="T68">pro.h:S</ta>
            <ta e="T71" id="Seg_10426" s="T70">v:pred</ta>
            <ta e="T72" id="Seg_10427" s="T71">np:S</ta>
            <ta e="T74" id="Seg_10428" s="T73">adj:pred</ta>
            <ta e="T76" id="Seg_10429" s="T75">pro.h:S</ta>
            <ta e="T77" id="Seg_10430" s="T76">v:pred</ta>
            <ta e="T81" id="Seg_10431" s="T80">np:S</ta>
            <ta e="T82" id="Seg_10432" s="T81">v:pred</ta>
            <ta e="T83" id="Seg_10433" s="T82">v:pred 0.3:S</ta>
            <ta e="T85" id="Seg_10434" s="T84">v:pred 0.3.h:S</ta>
            <ta e="T86" id="Seg_10435" s="T85">v:pred 0.3.h:S</ta>
            <ta e="T89" id="Seg_10436" s="T88">np:S</ta>
            <ta e="T90" id="Seg_10437" s="T89">v:pred</ta>
            <ta e="T92" id="Seg_10438" s="T91">np:S</ta>
            <ta e="T93" id="Seg_10439" s="T92">adj:pred</ta>
            <ta e="T94" id="Seg_10440" s="T93">pro.h:S</ta>
            <ta e="T99" id="Seg_10441" s="T98">np:S</ta>
            <ta e="T100" id="Seg_10442" s="T99">v:pred</ta>
            <ta e="T101" id="Seg_10443" s="T100">v:pred 0.3:S</ta>
            <ta e="T103" id="Seg_10444" s="T102">np.h:S</ta>
            <ta e="T105" id="Seg_10445" s="T104">v:pred</ta>
            <ta e="T108" id="Seg_10446" s="T107">np.h:S</ta>
            <ta e="T109" id="Seg_10447" s="T108">v:pred</ta>
            <ta e="T113" id="Seg_10448" s="T112">pro.h:O</ta>
            <ta e="T116" id="Seg_10449" s="T115">v:pred 0.3.h:S</ta>
            <ta e="T118" id="Seg_10450" s="T117">pro.h:S</ta>
            <ta e="T120" id="Seg_10451" s="T119">v:pred</ta>
            <ta e="T123" id="Seg_10452" s="T122">v:pred 0.2.h:S</ta>
            <ta e="T125" id="Seg_10453" s="T124">pro.h:S</ta>
            <ta e="T127" id="Seg_10454" s="T126">v:pred</ta>
            <ta e="T129" id="Seg_10455" s="T128">v:pred 0.3.h:S</ta>
            <ta e="T130" id="Seg_10456" s="T129">np:O</ta>
            <ta e="T131" id="Seg_10457" s="T130">np:O</ta>
            <ta e="T132" id="Seg_10458" s="T131">v:pred 0.3.h:S</ta>
            <ta e="T133" id="Seg_10459" s="T132">np:O</ta>
            <ta e="T134" id="Seg_10460" s="T133">v:pred 0.3.h:S</ta>
            <ta e="T135" id="Seg_10461" s="T134">v:pred 0.2.h:S</ta>
            <ta e="T137" id="Seg_10462" s="T136">pro.h:S</ta>
            <ta e="T138" id="Seg_10463" s="T137">v:pred</ta>
            <ta e="T139" id="Seg_10464" s="T138">adj:pred</ta>
            <ta e="T140" id="Seg_10465" s="T139">cop 0.1.h:S</ta>
            <ta e="T141" id="Seg_10466" s="T140">pro.h:O</ta>
            <ta e="T142" id="Seg_10467" s="T141">v:pred 0.1.h:S</ta>
            <ta e="T144" id="Seg_10468" s="T143">pro.h:S</ta>
            <ta e="T145" id="Seg_10469" s="T144">v:pred</ta>
            <ta e="T146" id="Seg_10470" s="T145">v:pred</ta>
            <ta e="T150" id="Seg_10471" s="T149">np:S</ta>
            <ta e="T151" id="Seg_10472" s="T150">v:pred</ta>
            <ta e="T154" id="Seg_10473" s="T153">v:pred</ta>
            <ta e="T156" id="Seg_10474" s="T155">np:S</ta>
            <ta e="T159" id="Seg_10475" s="T158">np:S</ta>
            <ta e="T160" id="Seg_10476" s="T159">v:pred</ta>
            <ta e="T161" id="Seg_10477" s="T160">np:S</ta>
            <ta e="T163" id="Seg_10478" s="T162">v:pred</ta>
            <ta e="T164" id="Seg_10479" s="T163">v:pred 0.3.h:S</ta>
            <ta e="T165" id="Seg_10480" s="T164">v:pred 0.3.h:S</ta>
            <ta e="T168" id="Seg_10481" s="T167">np:S</ta>
            <ta e="T169" id="Seg_10482" s="T168">v:pred</ta>
            <ta e="T171" id="Seg_10483" s="T170">np:S</ta>
            <ta e="T172" id="Seg_10484" s="T171">v:pred</ta>
            <ta e="T174" id="Seg_10485" s="T173">v:pred 0.1.h:S</ta>
            <ta e="T178" id="Seg_10486" s="T177">v:pred 0.2.h:S</ta>
            <ta e="T181" id="Seg_10487" s="T180">v:pred 0.2.h:S</ta>
            <ta e="T182" id="Seg_10488" s="T181">v:pred 0.3.h:S</ta>
            <ta e="T185" id="Seg_10489" s="T184">np.h:S</ta>
            <ta e="T186" id="Seg_10490" s="T185">v:pred</ta>
            <ta e="T188" id="Seg_10491" s="T187">v:pred 0.3.h:S</ta>
            <ta e="T190" id="Seg_10492" s="T189">v:pred</ta>
            <ta e="T192" id="Seg_10493" s="T191">np.h:S</ta>
            <ta e="T193" id="Seg_10494" s="T192">pro.h:S</ta>
            <ta e="T194" id="Seg_10495" s="T193">v:pred</ta>
            <ta e="T199" id="Seg_10496" s="T198">np:S</ta>
            <ta e="T200" id="Seg_10497" s="T199">v:pred</ta>
            <ta e="T201" id="Seg_10498" s="T200">pro.h:S</ta>
            <ta e="T202" id="Seg_10499" s="T201">v:pred</ta>
            <ta e="T205" id="Seg_10500" s="T204">v:pred</ta>
            <ta e="T207" id="Seg_10501" s="T206">np.h:S</ta>
            <ta e="T213" id="Seg_10502" s="T212">np.h:S</ta>
            <ta e="T214" id="Seg_10503" s="T213">v:pred</ta>
            <ta e="T215" id="Seg_10504" s="T214">pro.h:S</ta>
            <ta e="T217" id="Seg_10505" s="T216">v:pred</ta>
            <ta e="T220" id="Seg_10506" s="T219">np.h:S</ta>
            <ta e="T222" id="Seg_10507" s="T221">np.h:O</ta>
            <ta e="T223" id="Seg_10508" s="T222">v:pred</ta>
            <ta e="T226" id="Seg_10509" s="T225">pro.h:S</ta>
            <ta e="T384" id="Seg_10510" s="T227">conv:pred</ta>
            <ta e="T228" id="Seg_10511" s="T384">v:pred 0.3.h:S</ta>
            <ta e="T229" id="Seg_10512" s="T228">ptcl.neg</ta>
            <ta e="T230" id="Seg_10513" s="T229">v:pred 0.1.h:S</ta>
            <ta e="T232" id="Seg_10514" s="T231">pro.h:S</ta>
            <ta e="T233" id="Seg_10515" s="T232">np:O</ta>
            <ta e="T234" id="Seg_10516" s="T233">v:pred</ta>
            <ta e="T235" id="Seg_10517" s="T234">v:pred 0.3.h:S</ta>
            <ta e="T236" id="Seg_10518" s="T235">s:purp</ta>
            <ta e="T239" id="Seg_10519" s="T238">v:pred 0.3.h:S</ta>
            <ta e="T240" id="Seg_10520" s="T239">v:pred 0.3.h:S</ta>
            <ta e="T241" id="Seg_10521" s="T240">v:pred 0.3.h:S</ta>
            <ta e="T244" id="Seg_10522" s="T243">ptcl:pred</ta>
            <ta e="T249" id="Seg_10523" s="T248">ptcl:pred</ta>
            <ta e="T250" id="Seg_10524" s="T249">pro.h:O</ta>
            <ta e="T254" id="Seg_10525" s="T253">pro.h:S</ta>
            <ta e="T256" id="Seg_10526" s="T255">v:pred</ta>
            <ta e="T257" id="Seg_10527" s="T256">v:pred 0.2.h:S</ta>
            <ta e="T259" id="Seg_10528" s="T258">pro.h:S</ta>
            <ta e="T261" id="Seg_10529" s="T260">n:pred</ta>
            <ta e="T262" id="Seg_10530" s="T261">cop</ta>
            <ta e="T263" id="Seg_10531" s="T262">np.h:S</ta>
            <ta e="T264" id="Seg_10532" s="T263">v:pred</ta>
            <ta e="T270" id="Seg_10533" s="T269">np.h:S</ta>
            <ta e="T271" id="Seg_10534" s="T270">v:pred</ta>
            <ta e="T273" id="Seg_10535" s="T272">pro.h:S</ta>
            <ta e="T274" id="Seg_10536" s="T273">ptcl.neg</ta>
            <ta e="T275" id="Seg_10537" s="T274">adj:pred</ta>
            <ta e="T278" id="Seg_10538" s="T277">ptcl.neg</ta>
            <ta e="T279" id="Seg_10539" s="T278">adj:pred</ta>
            <ta e="T281" id="Seg_10540" s="T280">pro.h:S</ta>
            <ta e="T282" id="Seg_10541" s="T281">adj:pred</ta>
            <ta e="T283" id="Seg_10542" s="T282">cop</ta>
            <ta e="T285" id="Seg_10543" s="T284">np:O</ta>
            <ta e="T287" id="Seg_10544" s="T286">v:pred 0.1.h:S</ta>
            <ta e="T290" id="Seg_10545" s="T289">v:pred 0.1.h:S</ta>
            <ta e="T294" id="Seg_10546" s="T293">adj:pred</ta>
            <ta e="T295" id="Seg_10547" s="T294">cop 0.1.h:S</ta>
            <ta e="T300" id="Seg_10548" s="T299">np:S</ta>
            <ta e="T301" id="Seg_10549" s="T300">v:pred</ta>
            <ta e="T303" id="Seg_10550" s="T302">pro:S</ta>
            <ta e="T311" id="Seg_10551" s="T310">np:S</ta>
            <ta e="T312" id="Seg_10552" s="T311">v:pred</ta>
            <ta e="T317" id="Seg_10553" s="T316">pro.h:S</ta>
            <ta e="T320" id="Seg_10554" s="T319">v:pred</ta>
            <ta e="T322" id="Seg_10555" s="T321">np:S</ta>
            <ta e="T325" id="Seg_10556" s="T324">v:pred</ta>
            <ta e="T332" id="Seg_10557" s="T331">np.h:S</ta>
            <ta e="T333" id="Seg_10558" s="T332">v:pred</ta>
            <ta e="T335" id="Seg_10559" s="T334">v:pred 0.3.h:S</ta>
            <ta e="T340" id="Seg_10560" s="T339">adj:pred</ta>
            <ta e="T341" id="Seg_10561" s="T340">pro:S</ta>
            <ta e="T343" id="Seg_10562" s="T342">n:pred</ta>
            <ta e="T345" id="Seg_10563" s="T344">np:S</ta>
            <ta e="T346" id="Seg_10564" s="T345">v:pred</ta>
            <ta e="T348" id="Seg_10565" s="T347">np:S</ta>
            <ta e="T349" id="Seg_10566" s="T348">v:pred</ta>
            <ta e="T352" id="Seg_10567" s="T350">v:pred 0.3:S</ta>
            <ta e="T355" id="Seg_10568" s="T354">v:pred 0.3:S</ta>
            <ta e="T356" id="Seg_10569" s="T355">pro:S</ta>
            <ta e="T357" id="Seg_10570" s="T356">n:pred</ta>
            <ta e="T360" id="Seg_10571" s="T359">np:S</ta>
            <ta e="T368" id="Seg_10572" s="T367">np:S</ta>
            <ta e="T370" id="Seg_10573" s="T369">adj:pred</ta>
            <ta e="T373" id="Seg_10574" s="T372">pro:S</ta>
            <ta e="T374" id="Seg_10575" s="T373">n:pred</ta>
            <ta e="T377" id="Seg_10576" s="T376">v:pred</ta>
            <ta e="T380" id="Seg_10577" s="T379">np:S</ta>
            <ta e="T383" id="Seg_10578" s="T382">pro:S</ta>
            <ta e="T385" id="Seg_10579" s="T885">n:pred</ta>
            <ta e="T392" id="Seg_10580" s="T391">v:pred 0.3.h:S</ta>
            <ta e="T394" id="Seg_10581" s="T393">pro:S</ta>
            <ta e="T395" id="Seg_10582" s="T394">n:pred</ta>
            <ta e="T399" id="Seg_10583" s="T398">np:S</ta>
            <ta e="T400" id="Seg_10584" s="T399">v:pred</ta>
            <ta e="T403" id="Seg_10585" s="T402">v:pred 0.3:S</ta>
            <ta e="T407" id="Seg_10586" s="T406">np:S</ta>
            <ta e="T408" id="Seg_10587" s="T407">np:S</ta>
            <ta e="T409" id="Seg_10588" s="T408">v:pred</ta>
            <ta e="T412" id="Seg_10589" s="T411">v:pred 0.3.h:S</ta>
            <ta e="T417" id="Seg_10590" s="T416">v:pred 0.3.h:S</ta>
            <ta e="T418" id="Seg_10591" s="T417">v:pred 0.3.h:S</ta>
            <ta e="T419" id="Seg_10592" s="T418">pro:S</ta>
            <ta e="T420" id="Seg_10593" s="T419">n:pred</ta>
            <ta e="T422" id="Seg_10594" s="T421">np:S</ta>
            <ta e="T423" id="Seg_10595" s="T422">np:S</ta>
            <ta e="T424" id="Seg_10596" s="T423">v:pred</ta>
            <ta e="T426" id="Seg_10597" s="T425">np:O</ta>
            <ta e="T428" id="Seg_10598" s="T427">v:pred 0.3:S</ta>
            <ta e="T429" id="Seg_10599" s="T428">pro:S</ta>
            <ta e="T430" id="Seg_10600" s="T429">n:pred</ta>
            <ta e="T434" id="Seg_10601" s="T433">np:S</ta>
            <ta e="T437" id="Seg_10602" s="T436">v:pred</ta>
            <ta e="T438" id="Seg_10603" s="T437">ptcl.neg</ta>
            <ta e="T439" id="Seg_10604" s="T438">v:pred 0.3:S</ta>
            <ta e="T442" id="Seg_10605" s="T441">pro.h:O</ta>
            <ta e="T445" id="Seg_10606" s="T443">ptcl.neg</ta>
            <ta e="T447" id="Seg_10607" s="T446">pro.h:O</ta>
            <ta e="T449" id="Seg_10608" s="T448">ptcl.neg</ta>
            <ta e="T450" id="Seg_10609" s="T449">conv:pred</ta>
            <ta e="T459" id="Seg_10610" s="T458">np:S</ta>
            <ta e="T460" id="Seg_10611" s="T459">v:pred</ta>
            <ta e="T462" id="Seg_10612" s="T461">np:S</ta>
            <ta e="T463" id="Seg_10613" s="T462">v:pred</ta>
            <ta e="T467" id="Seg_10614" s="T466">np:S</ta>
            <ta e="T468" id="Seg_10615" s="T467">v:pred</ta>
            <ta e="T469" id="Seg_10616" s="T468">np:S</ta>
            <ta e="T470" id="Seg_10617" s="T469">v:pred</ta>
            <ta e="T471" id="Seg_10618" s="T470">np:S</ta>
            <ta e="T472" id="Seg_10619" s="T471">v:pred</ta>
            <ta e="T474" id="Seg_10620" s="T473">v:pred 0.3:S</ta>
            <ta e="T476" id="Seg_10621" s="T475">np:S</ta>
            <ta e="T477" id="Seg_10622" s="T476">v:pred</ta>
            <ta e="T478" id="Seg_10623" s="T477">pro:S</ta>
            <ta e="T479" id="Seg_10624" s="T478">n:pred</ta>
            <ta e="T481" id="Seg_10625" s="T480">v:pred 0.3:S</ta>
            <ta e="T486" id="Seg_10626" s="T485">v:pred 0.3:S</ta>
            <ta e="T489" id="Seg_10627" s="T488">adj:pred</ta>
            <ta e="T495" id="Seg_10628" s="T494">v:pred</ta>
            <ta e="T496" id="Seg_10629" s="T495">np.h:S</ta>
            <ta e="T501" id="Seg_10630" s="T500">pro:S</ta>
            <ta e="T502" id="Seg_10631" s="T501">n:pred</ta>
            <ta e="T504" id="Seg_10632" s="T503">v:pred 0.1.h:S</ta>
            <ta e="T505" id="Seg_10633" s="T504">v:pred 0.1.h:S</ta>
            <ta e="T506" id="Seg_10634" s="T505">np:O</ta>
            <ta e="T507" id="Seg_10635" s="T506">ptcl.neg</ta>
            <ta e="T509" id="Seg_10636" s="T508">ptcl.neg</ta>
            <ta e="T510" id="Seg_10637" s="T509">v:pred 0.1.h:S</ta>
            <ta e="T512" id="Seg_10638" s="T511">pro:S</ta>
            <ta e="T513" id="Seg_10639" s="T512">n:pred</ta>
            <ta e="T515" id="Seg_10640" s="T514">v:pred</ta>
            <ta e="T516" id="Seg_10641" s="T515">np.h:S</ta>
            <ta e="T521" id="Seg_10642" s="T520">pro:S</ta>
            <ta e="T523" id="Seg_10643" s="T522">v:pred</ta>
            <ta e="T525" id="Seg_10644" s="T524">v:pred 0.3.h:S</ta>
            <ta e="T526" id="Seg_10645" s="T525">pro:S</ta>
            <ta e="T527" id="Seg_10646" s="T526">n:pred</ta>
            <ta e="T529" id="Seg_10647" s="T528">v:pred 0.3:S</ta>
            <ta e="T545" id="Seg_10648" s="T544">s:purp</ta>
            <ta e="T546" id="Seg_10649" s="T545">v:pred 0.3:S</ta>
            <ta e="T547" id="Seg_10650" s="T546">pro:S</ta>
            <ta e="T548" id="Seg_10651" s="T547">n:pred</ta>
            <ta e="T550" id="Seg_10652" s="T549">v:pred</ta>
            <ta e="T551" id="Seg_10653" s="T550">np.h:S</ta>
            <ta e="T553" id="Seg_10654" s="T552">pro.h:S</ta>
            <ta e="T554" id="Seg_10655" s="T553">adj:pred</ta>
            <ta e="T556" id="Seg_10656" s="T555">np:S</ta>
            <ta e="T557" id="Seg_10657" s="T556">adj:pred</ta>
            <ta e="T561" id="Seg_10658" s="T560">pro.h:S</ta>
            <ta e="T562" id="Seg_10659" s="T561">v:pred</ta>
            <ta e="T563" id="Seg_10660" s="T562">np:O</ta>
            <ta e="T566" id="Seg_10661" s="T565">ptcl.neg</ta>
            <ta e="T567" id="Seg_10662" s="T566">v:pred 0.1.h:S</ta>
            <ta e="T569" id="Seg_10663" s="T568">pro:S</ta>
            <ta e="T573" id="Seg_10664" s="T572">v:pred</ta>
            <ta e="T574" id="Seg_10665" s="T573">np.h:S</ta>
            <ta e="T577" id="Seg_10666" s="T576">np:S</ta>
            <ta e="T583" id="Seg_10667" s="T582">pro:S</ta>
            <ta e="T584" id="Seg_10668" s="T583">n:pred</ta>
            <ta e="T585" id="Seg_10669" s="T584">cop</ta>
            <ta e="T587" id="Seg_10670" s="T586">np.h:S</ta>
            <ta e="T588" id="Seg_10671" s="T587">v:pred</ta>
            <ta e="T589" id="Seg_10672" s="T588">np.h:S</ta>
            <ta e="T590" id="Seg_10673" s="T589">v:pred</ta>
            <ta e="T593" id="Seg_10674" s="T592">np.h:S</ta>
            <ta e="T594" id="Seg_10675" s="T593">v:pred</ta>
            <ta e="T596" id="Seg_10676" s="T595">np.h:S</ta>
            <ta e="T598" id="Seg_10677" s="T597">v:pred</ta>
            <ta e="T599" id="Seg_10678" s="T598">pro:S</ta>
            <ta e="T600" id="Seg_10679" s="T599">n:pred</ta>
            <ta e="T601" id="Seg_10680" s="T600">cop</ta>
            <ta e="T604" id="Seg_10681" s="T603">np:S</ta>
            <ta e="T606" id="Seg_10682" s="T605">v:pred</ta>
            <ta e="T611" id="Seg_10683" s="T609">np:S</ta>
            <ta e="T612" id="Seg_10684" s="T611">pro:S</ta>
            <ta e="T613" id="Seg_10685" s="T612">n:pred</ta>
            <ta e="T615" id="Seg_10686" s="T614">n:pred</ta>
            <ta e="T624" id="Seg_10687" s="T623">np:S</ta>
            <ta e="T627" id="Seg_10688" s="T626">pro.h:S</ta>
            <ta e="T629" id="Seg_10689" s="T628">v:pred</ta>
            <ta e="T630" id="Seg_10690" s="T629">v:pred 0.1.h:S</ta>
            <ta e="T633" id="Seg_10691" s="T632">ptcl.neg</ta>
            <ta e="T634" id="Seg_10692" s="T633">v:pred 0.1.h:S</ta>
            <ta e="T637" id="Seg_10693" s="T636">np:S</ta>
            <ta e="T638" id="Seg_10694" s="T637">v:pred</ta>
            <ta e="T640" id="Seg_10695" s="T639">np:S</ta>
            <ta e="T641" id="Seg_10696" s="T640">n:pred</ta>
            <ta e="T643" id="Seg_10697" s="T642">v:pred 0.3:S</ta>
            <ta e="T648" id="Seg_10698" s="T647">v:pred</ta>
            <ta e="T649" id="Seg_10699" s="T648">np.h:S</ta>
            <ta e="T652" id="Seg_10700" s="T651">v:pred 0.3.h:S</ta>
            <ta e="T654" id="Seg_10701" s="T653">pro.h:S</ta>
            <ta e="T656" id="Seg_10702" s="T655">v:pred</ta>
            <ta e="T657" id="Seg_10703" s="T656">pro:S</ta>
            <ta e="T658" id="Seg_10704" s="T657">n:pred</ta>
            <ta e="T660" id="Seg_10705" s="T659">np.h:S</ta>
            <ta e="T661" id="Seg_10706" s="T660">v:pred</ta>
            <ta e="T662" id="Seg_10707" s="T661">np.h:S</ta>
            <ta e="T663" id="Seg_10708" s="T662">v:pred</ta>
            <ta e="T664" id="Seg_10709" s="T663">np.h:S</ta>
            <ta e="T665" id="Seg_10710" s="T664">v:pred</ta>
            <ta e="T666" id="Seg_10711" s="T665">pro:S</ta>
            <ta e="T667" id="Seg_10712" s="T666">n:pred</ta>
            <ta e="T669" id="Seg_10713" s="T668">v:pred 0.2.h:S</ta>
            <ta e="T670" id="Seg_10714" s="T669">pro.h:O</ta>
            <ta e="T671" id="Seg_10715" s="T670">v:pred 0.2.h:S</ta>
            <ta e="T672" id="Seg_10716" s="T671">pro.h:O</ta>
            <ta e="T673" id="Seg_10717" s="T672">v:pred 0.2.h:S</ta>
            <ta e="T677" id="Seg_10718" s="T676">pro:S</ta>
            <ta e="T678" id="Seg_10719" s="T677">n:pred</ta>
            <ta e="T680" id="Seg_10720" s="T679">v:pred 0.3:S</ta>
            <ta e="T683" id="Seg_10721" s="T682">np.h:S</ta>
            <ta e="T685" id="Seg_10722" s="T684">v:pred</ta>
            <ta e="T686" id="Seg_10723" s="T685">v:pred 0.3.h:S</ta>
            <ta e="T687" id="Seg_10724" s="T686">v:pred 0.3.h:S</ta>
            <ta e="T688" id="Seg_10725" s="T687">pro:S</ta>
            <ta e="T689" id="Seg_10726" s="T688">n:pred</ta>
            <ta e="T690" id="Seg_10727" s="T689">cop</ta>
            <ta e="T693" id="Seg_10728" s="T692">np:S</ta>
            <ta e="T694" id="Seg_10729" s="T693">v:pred</ta>
            <ta e="T697" id="Seg_10730" s="T696">pro:S</ta>
            <ta e="T698" id="Seg_10731" s="T697">n:pred</ta>
            <ta e="T701" id="Seg_10732" s="T700">np:S</ta>
            <ta e="T702" id="Seg_10733" s="T701">v:pred</ta>
            <ta e="T704" id="Seg_10734" s="T703">np:S</ta>
            <ta e="T705" id="Seg_10735" s="T704">ptcl.neg</ta>
            <ta e="T706" id="Seg_10736" s="T705">v:pred</ta>
            <ta e="T707" id="Seg_10737" s="T706">n:pred</ta>
            <ta e="T708" id="Seg_10738" s="T707">pro:S</ta>
            <ta e="T710" id="Seg_10739" s="T709">v:pred 0.3.h:S</ta>
            <ta e="T711" id="Seg_10740" s="T710">conv:pred</ta>
            <ta e="T713" id="Seg_10741" s="T712">pro.h:S</ta>
            <ta e="T714" id="Seg_10742" s="T713">v:pred</ta>
            <ta e="T715" id="Seg_10743" s="T714">np:O</ta>
            <ta e="T716" id="Seg_10744" s="T715">s:purp</ta>
            <ta e="T720" id="Seg_10745" s="T719">np.h:S</ta>
            <ta e="T721" id="Seg_10746" s="T720">v:pred</ta>
            <ta e="T727" id="Seg_10747" s="T726">v:pred 0.1.h:S</ta>
            <ta e="T730" id="Seg_10748" s="T729">v:pred 0.1.h:S</ta>
            <ta e="T732" id="Seg_10749" s="T731">np:O</ta>
            <ta e="T733" id="Seg_10750" s="T732">v:pred 0.1.h:S</ta>
            <ta e="T735" id="Seg_10751" s="T734">np:O</ta>
            <ta e="T736" id="Seg_10752" s="T735">v:pred 0.1.h:S</ta>
            <ta e="T738" id="Seg_10753" s="T737">v:pred 0.1.h:S</ta>
            <ta e="T740" id="Seg_10754" s="T739">np:S</ta>
            <ta e="T741" id="Seg_10755" s="T740">v:pred</ta>
            <ta e="T742" id="Seg_10756" s="T741">np.h:S</ta>
            <ta e="T743" id="Seg_10757" s="T742">v:pred</ta>
            <ta e="T745" id="Seg_10758" s="T744">np:S</ta>
            <ta e="T747" id="Seg_10759" s="T746">v:pred</ta>
            <ta e="T748" id="Seg_10760" s="T747">ptcl:pred</ta>
            <ta e="T750" id="Seg_10761" s="T749">np:O</ta>
            <ta e="T753" id="Seg_10762" s="T752">ptcl.neg</ta>
            <ta e="T754" id="Seg_10763" s="T753">adj:pred</ta>
            <ta e="T755" id="Seg_10764" s="T754">cop</ta>
            <ta e="T757" id="Seg_10765" s="T756">np:O</ta>
            <ta e="T758" id="Seg_10766" s="T757">v:pred 0.1.h:S</ta>
            <ta e="T759" id="Seg_10767" s="T758">v:pred 0.1.h:S</ta>
            <ta e="T761" id="Seg_10768" s="T760">np:O</ta>
            <ta e="T762" id="Seg_10769" s="T761">v:pred 0.1.h:S</ta>
            <ta e="T766" id="Seg_10770" s="T765">v:pred</ta>
            <ta e="T767" id="Seg_10771" s="T766">np:S</ta>
            <ta e="T769" id="Seg_10772" s="T768">np:O</ta>
            <ta e="T773" id="Seg_10773" s="T772">v:pred 0.1.h:S</ta>
            <ta e="T774" id="Seg_10774" s="T773">s:purp</ta>
            <ta e="T780" id="Seg_10775" s="T779">np:S</ta>
            <ta e="T781" id="Seg_10776" s="T780">v:pred</ta>
            <ta e="T782" id="Seg_10777" s="T781">v:pred 0.3:S</ta>
            <ta e="T784" id="Seg_10778" s="T783">v:pred 0.1.h:S</ta>
            <ta e="T786" id="Seg_10779" s="T785">v:pred 0.1.h:S</ta>
            <ta e="T787" id="Seg_10780" s="T786">np:O</ta>
            <ta e="T788" id="Seg_10781" s="T787">v:pred 0.1.h:S</ta>
            <ta e="T789" id="Seg_10782" s="T788">v:pred 0.1.h:S</ta>
            <ta e="T792" id="Seg_10783" s="T791">v:pred 0.1.h:S</ta>
            <ta e="T795" id="Seg_10784" s="T794">ptcl:pred</ta>
            <ta e="T801" id="Seg_10785" s="T800">v:pred 0.1.h:S</ta>
            <ta e="T802" id="Seg_10786" s="T801">v:pred 0.1.h:S</ta>
            <ta e="T804" id="Seg_10787" s="T803">v:pred 0.1.h:S</ta>
            <ta e="T805" id="Seg_10788" s="T804">np:O</ta>
            <ta e="T806" id="Seg_10789" s="T805">np:O</ta>
            <ta e="T808" id="Seg_10790" s="T807">v:pred 0.1.h:S</ta>
            <ta e="T811" id="Seg_10791" s="T810">v:pred 0.1.h:S</ta>
            <ta e="T814" id="Seg_10792" s="T813">pro.h:S</ta>
            <ta e="T817" id="Seg_10793" s="T816">v:pred</ta>
            <ta e="T819" id="Seg_10794" s="T818">v:pred 0.1.h:S</ta>
            <ta e="T821" id="Seg_10795" s="T889">v:pred 0.1.h:S</ta>
            <ta e="T826" id="Seg_10796" s="T825">v:pred 0.1.h:S</ta>
            <ta e="T828" id="Seg_10797" s="T827">v:pred 0.1.h:S</ta>
            <ta e="T829" id="Seg_10798" s="T828">np:O</ta>
            <ta e="T831" id="Seg_10799" s="T830">v:pred 0.1.h:S</ta>
            <ta e="T834" id="Seg_10800" s="T833">pro.h:S</ta>
            <ta e="T835" id="Seg_10801" s="T834">v:pred</ta>
            <ta e="T838" id="Seg_10802" s="T837">pro.h:S</ta>
            <ta e="T839" id="Seg_10803" s="T838">v:pred</ta>
            <ta e="T842" id="Seg_10804" s="T841">np:O</ta>
            <ta e="T843" id="Seg_10805" s="T842">v:pred 0.1.h:S</ta>
            <ta e="T845" id="Seg_10806" s="T844">np:O</ta>
            <ta e="T847" id="Seg_10807" s="T846">np:O</ta>
            <ta e="T848" id="Seg_10808" s="T847">v:pred 0.1.h:S</ta>
            <ta e="T856" id="Seg_10809" s="T855">np.h:S</ta>
            <ta e="T857" id="Seg_10810" s="T856">v:pred</ta>
            <ta e="T858" id="Seg_10811" s="T857">v:pred 0.3.h:S</ta>
            <ta e="T859" id="Seg_10812" s="T858">v:pred 0.3.h:S</ta>
            <ta e="T861" id="Seg_10813" s="T860">np:S</ta>
            <ta e="T865" id="Seg_10814" s="T864">np:O</ta>
            <ta e="T866" id="Seg_10815" s="T865">v:pred 0.3.h:S</ta>
            <ta e="T867" id="Seg_10816" s="T866">pro.h:S</ta>
            <ta e="T868" id="Seg_10817" s="T867">v:pred</ta>
            <ta e="T873" id="Seg_10818" s="T872">np:O</ta>
            <ta e="T874" id="Seg_10819" s="T873">v:pred 0.3.h:S</ta>
            <ta e="T875" id="Seg_10820" s="T874">v:pred 0.3.h:S</ta>
            <ta e="T876" id="Seg_10821" s="T875">np.h:S</ta>
            <ta e="T877" id="Seg_10822" s="T876">v:pred</ta>
            <ta e="T879" id="Seg_10823" s="T878">np:O</ta>
            <ta e="T881" id="Seg_10824" s="T880">adj:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T1" id="Seg_10825" s="T0">adv:Time</ta>
            <ta e="T3" id="Seg_10826" s="T2">np.h:A</ta>
            <ta e="T4" id="Seg_10827" s="T3">np:Pth</ta>
            <ta e="T6" id="Seg_10828" s="T5">0.3.h:A</ta>
            <ta e="T7" id="Seg_10829" s="T6">0.3.h:A</ta>
            <ta e="T8" id="Seg_10830" s="T7">adv:Time</ta>
            <ta e="T10" id="Seg_10831" s="T9">np:L</ta>
            <ta e="T11" id="Seg_10832" s="T10">np:L</ta>
            <ta e="T12" id="Seg_10833" s="T11">0.3.h:A</ta>
            <ta e="T13" id="Seg_10834" s="T12">0.3.h:E</ta>
            <ta e="T15" id="Seg_10835" s="T14">np.h:A</ta>
            <ta e="T17" id="Seg_10836" s="T16">np:Th</ta>
            <ta e="T19" id="Seg_10837" s="T18">adv:Time</ta>
            <ta e="T24" id="Seg_10838" s="T23">adv:Time</ta>
            <ta e="T28" id="Seg_10839" s="T27">np:E 0.3.h:Poss</ta>
            <ta e="T31" id="Seg_10840" s="T30">np:G</ta>
            <ta e="T34" id="Seg_10841" s="T33">np:E</ta>
            <ta e="T36" id="Seg_10842" s="T35">np:G</ta>
            <ta e="T37" id="Seg_10843" s="T36">adv:Time</ta>
            <ta e="T39" id="Seg_10844" s="T38">np.h:A</ta>
            <ta e="T42" id="Seg_10845" s="T41">adv:L</ta>
            <ta e="T44" id="Seg_10846" s="T43">np:Th</ta>
            <ta e="T47" id="Seg_10847" s="T46">0.1.h:E</ta>
            <ta e="T48" id="Seg_10848" s="T47">adv:Time</ta>
            <ta e="T49" id="Seg_10849" s="T48">0.3.h:A</ta>
            <ta e="T50" id="Seg_10850" s="T49">0.3.h:E</ta>
            <ta e="T51" id="Seg_10851" s="T50">0.3.h:E</ta>
            <ta e="T53" id="Seg_10852" s="T52">0.3.h:A</ta>
            <ta e="T55" id="Seg_10853" s="T54">np:Ins</ta>
            <ta e="T60" id="Seg_10854" s="T59">np.h:A</ta>
            <ta e="T61" id="Seg_10855" s="T60">adv:Time</ta>
            <ta e="T62" id="Seg_10856" s="T61">pro.h:A</ta>
            <ta e="T65" id="Seg_10857" s="T64">adv:Time</ta>
            <ta e="T66" id="Seg_10858" s="T65">pro:E</ta>
            <ta e="T68" id="Seg_10859" s="T67">np:G</ta>
            <ta e="T69" id="Seg_10860" s="T68">pro.h:A</ta>
            <ta e="T72" id="Seg_10861" s="T71">np:Th</ta>
            <ta e="T76" id="Seg_10862" s="T75">pro.h:E</ta>
            <ta e="T78" id="Seg_10863" s="T77">adv:Time</ta>
            <ta e="T81" id="Seg_10864" s="T80">np:A</ta>
            <ta e="T83" id="Seg_10865" s="T82">0.3:A</ta>
            <ta e="T84" id="Seg_10866" s="T83">adv:Time</ta>
            <ta e="T85" id="Seg_10867" s="T84">0.3.h:E</ta>
            <ta e="T86" id="Seg_10868" s="T85">0.3.h:E</ta>
            <ta e="T89" id="Seg_10869" s="T88">np:A</ta>
            <ta e="T92" id="Seg_10870" s="T91">np:Th</ta>
            <ta e="T94" id="Seg_10871" s="T93">pro.h:A</ta>
            <ta e="T97" id="Seg_10872" s="T96">adv:Time</ta>
            <ta e="T99" id="Seg_10873" s="T98">np:A</ta>
            <ta e="T101" id="Seg_10874" s="T100">0.3:Th</ta>
            <ta e="T103" id="Seg_10875" s="T102">np.h:A</ta>
            <ta e="T106" id="Seg_10876" s="T105">pro:G</ta>
            <ta e="T108" id="Seg_10877" s="T107">np.h:E</ta>
            <ta e="T113" id="Seg_10878" s="T112">pro.h:Th</ta>
            <ta e="T115" id="Seg_10879" s="T114">np:G</ta>
            <ta e="T116" id="Seg_10880" s="T115">0.3.h:A</ta>
            <ta e="T118" id="Seg_10881" s="T117">pro.h:A</ta>
            <ta e="T119" id="Seg_10882" s="T118">adv:L</ta>
            <ta e="T121" id="Seg_10883" s="T120">adv:Time</ta>
            <ta e="T123" id="Seg_10884" s="T122">0.2.h:A</ta>
            <ta e="T124" id="Seg_10885" s="T123">adv:L</ta>
            <ta e="T125" id="Seg_10886" s="T124">pro.h:A</ta>
            <ta e="T129" id="Seg_10887" s="T128">0.3.h:A</ta>
            <ta e="T130" id="Seg_10888" s="T129">np:P</ta>
            <ta e="T131" id="Seg_10889" s="T130">np:Th</ta>
            <ta e="T132" id="Seg_10890" s="T131">0.3.h:A</ta>
            <ta e="T133" id="Seg_10891" s="T132">np:Th</ta>
            <ta e="T134" id="Seg_10892" s="T133">0.3.h:A</ta>
            <ta e="T135" id="Seg_10893" s="T134">0.2.h:E</ta>
            <ta e="T136" id="Seg_10894" s="T135">adv:L</ta>
            <ta e="T137" id="Seg_10895" s="T136">pro.h:A</ta>
            <ta e="T140" id="Seg_10896" s="T139">0.1.h:P</ta>
            <ta e="T141" id="Seg_10897" s="T140">pro.h:Th</ta>
            <ta e="T142" id="Seg_10898" s="T141">0.1.h:A</ta>
            <ta e="T143" id="Seg_10899" s="T142">adv:Time</ta>
            <ta e="T144" id="Seg_10900" s="T143">pro.h:E</ta>
            <ta e="T150" id="Seg_10901" s="T149">np:A</ta>
            <ta e="T153" id="Seg_10902" s="T152">adv:Time</ta>
            <ta e="T155" id="Seg_10903" s="T154">adv:L</ta>
            <ta e="T156" id="Seg_10904" s="T155">np:Th</ta>
            <ta e="T157" id="Seg_10905" s="T156">adv:Time</ta>
            <ta e="T159" id="Seg_10906" s="T158">np:A</ta>
            <ta e="T161" id="Seg_10907" s="T160">np:Th</ta>
            <ta e="T164" id="Seg_10908" s="T163">0.3.h:E</ta>
            <ta e="T165" id="Seg_10909" s="T164">0.3.h:E</ta>
            <ta e="T166" id="Seg_10910" s="T165">adv:Time</ta>
            <ta e="T168" id="Seg_10911" s="T167">np:A</ta>
            <ta e="T171" id="Seg_10912" s="T170">np:Th</ta>
            <ta e="T174" id="Seg_10913" s="T173">0.1.h:A</ta>
            <ta e="T175" id="Seg_10914" s="T174">pro.h:Com</ta>
            <ta e="T176" id="Seg_10915" s="T175">np:G</ta>
            <ta e="T178" id="Seg_10916" s="T177">0.2.h:A</ta>
            <ta e="T180" id="Seg_10917" s="T179">pro:L</ta>
            <ta e="T181" id="Seg_10918" s="T180">0.2.h:E</ta>
            <ta e="T182" id="Seg_10919" s="T181">0.3.h:A</ta>
            <ta e="T184" id="Seg_10920" s="T183">np:G</ta>
            <ta e="T185" id="Seg_10921" s="T184">np.h:A</ta>
            <ta e="T187" id="Seg_10922" s="T186">np:G</ta>
            <ta e="T188" id="Seg_10923" s="T187">0.3.h:A</ta>
            <ta e="T189" id="Seg_10924" s="T188">np:G</ta>
            <ta e="T192" id="Seg_10925" s="T191">np.h:Th</ta>
            <ta e="T193" id="Seg_10926" s="T192">pro.h:Th</ta>
            <ta e="T199" id="Seg_10927" s="T198">np:Th</ta>
            <ta e="T201" id="Seg_10928" s="T200">pro.h:A</ta>
            <ta e="T207" id="Seg_10929" s="T206">np.h:A</ta>
            <ta e="T213" id="Seg_10930" s="T212">np.h:Th</ta>
            <ta e="T215" id="Seg_10931" s="T214">pro.h:A</ta>
            <ta e="T216" id="Seg_10932" s="T215">np:G</ta>
            <ta e="T219" id="Seg_10933" s="T218">pro.h:Poss</ta>
            <ta e="T220" id="Seg_10934" s="T219">np.h:A</ta>
            <ta e="T222" id="Seg_10935" s="T221">np.h:E</ta>
            <ta e="T225" id="Seg_10936" s="T224">pro:L</ta>
            <ta e="T226" id="Seg_10937" s="T225">pro.h:Th</ta>
            <ta e="T227" id="Seg_10938" s="T226">pro:G</ta>
            <ta e="T228" id="Seg_10939" s="T384">0.3.h:A</ta>
            <ta e="T230" id="Seg_10940" s="T229">0.1.h:E</ta>
            <ta e="T231" id="Seg_10941" s="T230">adv:Time</ta>
            <ta e="T232" id="Seg_10942" s="T231">pro.h:A</ta>
            <ta e="T233" id="Seg_10943" s="T232">np:Th</ta>
            <ta e="T235" id="Seg_10944" s="T234">0.3.h:A</ta>
            <ta e="T239" id="Seg_10945" s="T238">0.3.h:E</ta>
            <ta e="T240" id="Seg_10946" s="T239">0.3.h:E</ta>
            <ta e="T241" id="Seg_10947" s="T240">0.3.h:E</ta>
            <ta e="T242" id="Seg_10948" s="T241">adv:L</ta>
            <ta e="T243" id="Seg_10949" s="T242">adv:Time</ta>
            <ta e="T245" id="Seg_10950" s="T244">pro.h:Com</ta>
            <ta e="T248" id="Seg_10951" s="T247">pro.h:A</ta>
            <ta e="T250" id="Seg_10952" s="T249">pro.h:E</ta>
            <ta e="T253" id="Seg_10953" s="T252">np:Ins</ta>
            <ta e="T254" id="Seg_10954" s="T253">pro.h:A</ta>
            <ta e="T257" id="Seg_10955" s="T256">0.2.h:A</ta>
            <ta e="T259" id="Seg_10956" s="T258">pro.h:Th</ta>
            <ta e="T260" id="Seg_10957" s="T259">pro.h:Poss</ta>
            <ta e="T261" id="Seg_10958" s="T260">np.h:Th</ta>
            <ta e="T263" id="Seg_10959" s="T262">np.h:A</ta>
            <ta e="T270" id="Seg_10960" s="T269">np.h:A</ta>
            <ta e="T272" id="Seg_10961" s="T271">np.h:R</ta>
            <ta e="T273" id="Seg_10962" s="T272">pro.h:Th</ta>
            <ta e="T281" id="Seg_10963" s="T280">pro.h:Th</ta>
            <ta e="T284" id="Seg_10964" s="T883">np:So</ta>
            <ta e="T285" id="Seg_10965" s="T284">np:P</ta>
            <ta e="T287" id="Seg_10966" s="T286">0.1.h:A</ta>
            <ta e="T289" id="Seg_10967" s="T288">np:G</ta>
            <ta e="T290" id="Seg_10968" s="T289">0.1.h:A</ta>
            <ta e="T292" id="Seg_10969" s="T290">np:G</ta>
            <ta e="T295" id="Seg_10970" s="T294">0.1.h:Th</ta>
            <ta e="T296" id="Seg_10971" s="T295">adv:Time</ta>
            <ta e="T298" id="Seg_10972" s="T297">np:L</ta>
            <ta e="T300" id="Seg_10973" s="T299">np:Th</ta>
            <ta e="T303" id="Seg_10974" s="T302">pro:Th</ta>
            <ta e="T308" id="Seg_10975" s="T306">np:L</ta>
            <ta e="T310" id="Seg_10976" s="T309">np:L</ta>
            <ta e="T311" id="Seg_10977" s="T310">np:A</ta>
            <ta e="T317" id="Seg_10978" s="T316">pro.h:Th</ta>
            <ta e="T319" id="Seg_10979" s="T318">np:L</ta>
            <ta e="T322" id="Seg_10980" s="T321">np:Th</ta>
            <ta e="T332" id="Seg_10981" s="T331">np.h:A</ta>
            <ta e="T335" id="Seg_10982" s="T334">0.3.h:Th</ta>
            <ta e="T338" id="Seg_10983" s="T337">np:L</ta>
            <ta e="T339" id="Seg_10984" s="T338">np:L</ta>
            <ta e="T341" id="Seg_10985" s="T340">pro:Th</ta>
            <ta e="T342" id="Seg_10986" s="T341">np:Poss</ta>
            <ta e="T343" id="Seg_10987" s="T342">np:Th</ta>
            <ta e="T345" id="Seg_10988" s="T344">np:Th</ta>
            <ta e="T348" id="Seg_10989" s="T347">np:Th</ta>
            <ta e="T352" id="Seg_10990" s="T350">0.3:A</ta>
            <ta e="T354" id="Seg_10991" s="T353">np:G</ta>
            <ta e="T355" id="Seg_10992" s="T354">0.3:A</ta>
            <ta e="T356" id="Seg_10993" s="T355">pro:Th</ta>
            <ta e="T357" id="Seg_10994" s="T356">np:Th</ta>
            <ta e="T360" id="Seg_10995" s="T359">np:Th</ta>
            <ta e="T361" id="Seg_10996" s="T360">np:G</ta>
            <ta e="T368" id="Seg_10997" s="T367">np:Th</ta>
            <ta e="T373" id="Seg_10998" s="T372">pro:Th</ta>
            <ta e="T374" id="Seg_10999" s="T373">np:Th</ta>
            <ta e="T376" id="Seg_11000" s="T375">np:L</ta>
            <ta e="T380" id="Seg_11001" s="T379">np:Th</ta>
            <ta e="T383" id="Seg_11002" s="T382">pro:Th</ta>
            <ta e="T385" id="Seg_11003" s="T885">np:Th</ta>
            <ta e="T390" id="Seg_11004" s="T389">np:L</ta>
            <ta e="T392" id="Seg_11005" s="T391">0.3.h:E</ta>
            <ta e="T394" id="Seg_11006" s="T393">pro:Th</ta>
            <ta e="T395" id="Seg_11007" s="T394">np:Th</ta>
            <ta e="T399" id="Seg_11008" s="T398">np:Th</ta>
            <ta e="T402" id="Seg_11009" s="T401">np:R</ta>
            <ta e="T403" id="Seg_11010" s="T402">0.3:A</ta>
            <ta e="T407" id="Seg_11011" s="T406">np:Th</ta>
            <ta e="T408" id="Seg_11012" s="T407">np:Th</ta>
            <ta e="T411" id="Seg_11013" s="T410">np:G</ta>
            <ta e="T412" id="Seg_11014" s="T411">0.3.h:A</ta>
            <ta e="T416" id="Seg_11015" s="T415">np:G</ta>
            <ta e="T417" id="Seg_11016" s="T416">0.3.h:A</ta>
            <ta e="T418" id="Seg_11017" s="T417">0.3.h:Th</ta>
            <ta e="T419" id="Seg_11018" s="T418">pro:Th</ta>
            <ta e="T420" id="Seg_11019" s="T419">np:Th</ta>
            <ta e="T422" id="Seg_11020" s="T421">np:Th</ta>
            <ta e="T423" id="Seg_11021" s="T422">np:Th</ta>
            <ta e="T426" id="Seg_11022" s="T425">np:Th</ta>
            <ta e="T428" id="Seg_11023" s="T427">0.3:A</ta>
            <ta e="T429" id="Seg_11024" s="T428">pro:Th</ta>
            <ta e="T430" id="Seg_11025" s="T429">np:Th</ta>
            <ta e="T434" id="Seg_11026" s="T433">np:Th</ta>
            <ta e="T435" id="Seg_11027" s="T434">np:L</ta>
            <ta e="T436" id="Seg_11028" s="T435">np:L</ta>
            <ta e="T439" id="Seg_11029" s="T438">0.3:A</ta>
            <ta e="T442" id="Seg_11030" s="T441">pro.h:Th</ta>
            <ta e="T443" id="Seg_11031" s="T442">np:G</ta>
            <ta e="T447" id="Seg_11032" s="T446">pro.h:Th</ta>
            <ta e="T448" id="Seg_11033" s="T447">np:G</ta>
            <ta e="T458" id="Seg_11034" s="T457">adv:L</ta>
            <ta e="T459" id="Seg_11035" s="T458">np:Th</ta>
            <ta e="T462" id="Seg_11036" s="T461">np:Th</ta>
            <ta e="T464" id="Seg_11037" s="T463">adv:So</ta>
            <ta e="T467" id="Seg_11038" s="T466">np:Th</ta>
            <ta e="T469" id="Seg_11039" s="T468">np:Th</ta>
            <ta e="T471" id="Seg_11040" s="T470">np:Th</ta>
            <ta e="T474" id="Seg_11041" s="T473">0.3.h:A</ta>
            <ta e="T476" id="Seg_11042" s="T475">np:Th</ta>
            <ta e="T478" id="Seg_11043" s="T477">pro:Th</ta>
            <ta e="T479" id="Seg_11044" s="T478">np:Th</ta>
            <ta e="T481" id="Seg_11045" s="T480">0.3:Th</ta>
            <ta e="T486" id="Seg_11046" s="T485">0.3:Th</ta>
            <ta e="T496" id="Seg_11047" s="T495">np.h:Th</ta>
            <ta e="T498" id="Seg_11048" s="T497">np:Ins</ta>
            <ta e="T501" id="Seg_11049" s="T500">pro:Th</ta>
            <ta e="T502" id="Seg_11050" s="T501">np:Th</ta>
            <ta e="T504" id="Seg_11051" s="T503">0.1.h:A</ta>
            <ta e="T505" id="Seg_11052" s="T504">0.1.h:A</ta>
            <ta e="T506" id="Seg_11053" s="T505">np:Th</ta>
            <ta e="T510" id="Seg_11054" s="T509">0.1.h:A</ta>
            <ta e="T512" id="Seg_11055" s="T511">pro:Th</ta>
            <ta e="T513" id="Seg_11056" s="T512">np:Th</ta>
            <ta e="T516" id="Seg_11057" s="T515">np.h:Th</ta>
            <ta e="T517" id="Seg_11058" s="T516">np:L</ta>
            <ta e="T521" id="Seg_11059" s="T520">pro:E</ta>
            <ta e="T525" id="Seg_11060" s="T524">0.3.h:E</ta>
            <ta e="T526" id="Seg_11061" s="T525">pro:Th</ta>
            <ta e="T527" id="Seg_11062" s="T526">np:Th</ta>
            <ta e="T529" id="Seg_11063" s="T528">0.3:Th</ta>
            <ta e="T546" id="Seg_11064" s="T545">0.3:Th</ta>
            <ta e="T547" id="Seg_11065" s="T546">pro:Th</ta>
            <ta e="T548" id="Seg_11066" s="T547">np:Th</ta>
            <ta e="T551" id="Seg_11067" s="T550">np.h:Th</ta>
            <ta e="T552" id="Seg_11068" s="T551">np:L</ta>
            <ta e="T553" id="Seg_11069" s="T552">pro.h:Th</ta>
            <ta e="T556" id="Seg_11070" s="T555">np:Th</ta>
            <ta e="T561" id="Seg_11071" s="T560">pro.h:A</ta>
            <ta e="T563" id="Seg_11072" s="T562">np:Th</ta>
            <ta e="T567" id="Seg_11073" s="T566">0.1.h:A</ta>
            <ta e="T569" id="Seg_11074" s="T568">pro:Th</ta>
            <ta e="T574" id="Seg_11075" s="T573">np.h:A</ta>
            <ta e="T576" id="Seg_11076" s="T575">adv:L</ta>
            <ta e="T583" id="Seg_11077" s="T582">pro:Th</ta>
            <ta e="T584" id="Seg_11078" s="T583">np:Th</ta>
            <ta e="T587" id="Seg_11079" s="T586">np.h:Th</ta>
            <ta e="T589" id="Seg_11080" s="T588">np.h:Th</ta>
            <ta e="T593" id="Seg_11081" s="T592">np.h:A</ta>
            <ta e="T596" id="Seg_11082" s="T595">np.h:A</ta>
            <ta e="T599" id="Seg_11083" s="T598">pro:Th</ta>
            <ta e="T600" id="Seg_11084" s="T599">np:Th</ta>
            <ta e="T604" id="Seg_11085" s="T603">np:A</ta>
            <ta e="T612" id="Seg_11086" s="T611">pro:Th</ta>
            <ta e="T613" id="Seg_11087" s="T612">np:Th</ta>
            <ta e="T615" id="Seg_11088" s="T614">np:Th</ta>
            <ta e="T623" id="Seg_11089" s="T622">np:L</ta>
            <ta e="T624" id="Seg_11090" s="T623">np:Th</ta>
            <ta e="T627" id="Seg_11091" s="T626">pro.h:A</ta>
            <ta e="T630" id="Seg_11092" s="T629">0.1.h:A</ta>
            <ta e="T634" id="Seg_11093" s="T633">0.1.h:A</ta>
            <ta e="T637" id="Seg_11094" s="T636">np:Th</ta>
            <ta e="T639" id="Seg_11095" s="T638">np:L</ta>
            <ta e="T640" id="Seg_11096" s="T639">np:Th</ta>
            <ta e="T641" id="Seg_11097" s="T640">np:L</ta>
            <ta e="T643" id="Seg_11098" s="T642">0.3:A</ta>
            <ta e="T644" id="Seg_11099" s="T643">np:R</ta>
            <ta e="T649" id="Seg_11100" s="T648">np.h:A</ta>
            <ta e="T650" id="Seg_11101" s="T649">np:L</ta>
            <ta e="T652" id="Seg_11102" s="T651">0.3.h:A</ta>
            <ta e="T654" id="Seg_11103" s="T653">pro.h:A</ta>
            <ta e="T660" id="Seg_11104" s="T659">np.h:A</ta>
            <ta e="T662" id="Seg_11105" s="T661">np.h:A</ta>
            <ta e="T664" id="Seg_11106" s="T663">np.h:A</ta>
            <ta e="T666" id="Seg_11107" s="T665">pro:Th</ta>
            <ta e="T667" id="Seg_11108" s="T666">np:Th</ta>
            <ta e="T669" id="Seg_11109" s="T668">0.2.h:A</ta>
            <ta e="T670" id="Seg_11110" s="T669">pro.h:E</ta>
            <ta e="T671" id="Seg_11111" s="T670">0.2.h:A</ta>
            <ta e="T672" id="Seg_11112" s="T671">pro.h:E</ta>
            <ta e="T673" id="Seg_11113" s="T672">0.2.h:A</ta>
            <ta e="T674" id="Seg_11114" s="T673">pro:G</ta>
            <ta e="T677" id="Seg_11115" s="T676">pro:Th</ta>
            <ta e="T678" id="Seg_11116" s="T677">np:Th</ta>
            <ta e="T680" id="Seg_11117" s="T679">0.3:Th</ta>
            <ta e="T683" id="Seg_11118" s="T682">np.h:A</ta>
            <ta e="T686" id="Seg_11119" s="T685">0.3.h:A</ta>
            <ta e="T687" id="Seg_11120" s="T686">0.3.h:A</ta>
            <ta e="T688" id="Seg_11121" s="T687">pro:Th</ta>
            <ta e="T689" id="Seg_11122" s="T688">np:Th</ta>
            <ta e="T693" id="Seg_11123" s="T692">np:P</ta>
            <ta e="T697" id="Seg_11124" s="T696">pro:Th</ta>
            <ta e="T698" id="Seg_11125" s="T697">np:Th</ta>
            <ta e="T701" id="Seg_11126" s="T700">np:Th</ta>
            <ta e="T703" id="Seg_11127" s="T702">adv:L</ta>
            <ta e="T704" id="Seg_11128" s="T703">np:P</ta>
            <ta e="T707" id="Seg_11129" s="T706">np:Th</ta>
            <ta e="T708" id="Seg_11130" s="T707">pro:Th</ta>
            <ta e="T710" id="Seg_11131" s="T709">0.3.h:A</ta>
            <ta e="T713" id="Seg_11132" s="T712">pro.h:A</ta>
            <ta e="T715" id="Seg_11133" s="T714">np:P</ta>
            <ta e="T720" id="Seg_11134" s="T719">np.h:A</ta>
            <ta e="T727" id="Seg_11135" s="T726">0.1.h:A</ta>
            <ta e="T729" id="Seg_11136" s="T728">adv:L</ta>
            <ta e="T730" id="Seg_11137" s="T729">0.1.h:A</ta>
            <ta e="T732" id="Seg_11138" s="T731">np:Th</ta>
            <ta e="T733" id="Seg_11139" s="T732">0.1.h:A</ta>
            <ta e="T735" id="Seg_11140" s="T734">np:Th</ta>
            <ta e="T736" id="Seg_11141" s="T735">0.1.h:A</ta>
            <ta e="T737" id="Seg_11142" s="T736">adv:Time</ta>
            <ta e="T738" id="Seg_11143" s="T737">0.1.h:A</ta>
            <ta e="T739" id="Seg_11144" s="T738">pro:L</ta>
            <ta e="T740" id="Seg_11145" s="T739">np:A</ta>
            <ta e="T742" id="Seg_11146" s="T741">np.h:A</ta>
            <ta e="T745" id="Seg_11147" s="T744">np:Th</ta>
            <ta e="T750" id="Seg_11148" s="T749">np:P</ta>
            <ta e="T756" id="Seg_11149" s="T755">adv:Time</ta>
            <ta e="T757" id="Seg_11150" s="T756">np:P</ta>
            <ta e="T758" id="Seg_11151" s="T757">0.1.h:A</ta>
            <ta e="T759" id="Seg_11152" s="T758">0.1.h:A</ta>
            <ta e="T761" id="Seg_11153" s="T760">np:P</ta>
            <ta e="T762" id="Seg_11154" s="T761">0.1.h:A</ta>
            <ta e="T764" id="Seg_11155" s="T763">adv:L</ta>
            <ta e="T767" id="Seg_11156" s="T766">np:Th</ta>
            <ta e="T768" id="Seg_11157" s="T767">adv:L</ta>
            <ta e="T769" id="Seg_11158" s="T768">np:Th</ta>
            <ta e="T771" id="Seg_11159" s="T770">np:G</ta>
            <ta e="T772" id="Seg_11160" s="T771">adv:Time</ta>
            <ta e="T773" id="Seg_11161" s="T772">0.1.h:A</ta>
            <ta e="T776" id="Seg_11162" s="T775">n:Time</ta>
            <ta e="T780" id="Seg_11163" s="T779">np:Th</ta>
            <ta e="T782" id="Seg_11164" s="T781">0.3:Th</ta>
            <ta e="T783" id="Seg_11165" s="T782">n:Time</ta>
            <ta e="T784" id="Seg_11166" s="T783">0.1.h:A</ta>
            <ta e="T786" id="Seg_11167" s="T785">0.1.h:Th</ta>
            <ta e="T787" id="Seg_11168" s="T786">np:Th</ta>
            <ta e="T788" id="Seg_11169" s="T787">0.1.h:A</ta>
            <ta e="T789" id="Seg_11170" s="T788">0.1.h:A</ta>
            <ta e="T790" id="Seg_11171" s="T789">np:G</ta>
            <ta e="T792" id="Seg_11172" s="T791">0.1.h:A</ta>
            <ta e="T793" id="Seg_11173" s="T792">np:G</ta>
            <ta e="T797" id="Seg_11174" s="T796">adv:Time</ta>
            <ta e="T800" id="Seg_11175" s="T799">np:Pth</ta>
            <ta e="T801" id="Seg_11176" s="T800">0.1.h:A</ta>
            <ta e="T802" id="Seg_11177" s="T801">0.1.h:A</ta>
            <ta e="T803" id="Seg_11178" s="T802">adv:Time</ta>
            <ta e="T804" id="Seg_11179" s="T803">0.1.h:E</ta>
            <ta e="T805" id="Seg_11180" s="T804">np:Th</ta>
            <ta e="T806" id="Seg_11181" s="T805">np:Th</ta>
            <ta e="T808" id="Seg_11182" s="T807">0.1.h:A</ta>
            <ta e="T810" id="Seg_11183" s="T809">np:G</ta>
            <ta e="T811" id="Seg_11184" s="T810">0.1.h:A</ta>
            <ta e="T814" id="Seg_11185" s="T813">pro.h:A</ta>
            <ta e="T815" id="Seg_11186" s="T814">adv:Time</ta>
            <ta e="T816" id="Seg_11187" s="T815">n:Time</ta>
            <ta e="T818" id="Seg_11188" s="T817">np:G</ta>
            <ta e="T819" id="Seg_11189" s="T818">0.1.h:A</ta>
            <ta e="T821" id="Seg_11190" s="T889">0.1.h:A</ta>
            <ta e="T822" id="Seg_11191" s="T821">adv:Time</ta>
            <ta e="T826" id="Seg_11192" s="T825">0.1.h:Th</ta>
            <ta e="T827" id="Seg_11193" s="T826">adv:Time</ta>
            <ta e="T828" id="Seg_11194" s="T827">0.1.h:A</ta>
            <ta e="T829" id="Seg_11195" s="T828">np:Th</ta>
            <ta e="T831" id="Seg_11196" s="T830">0.1.h:A</ta>
            <ta e="T832" id="Seg_11197" s="T831">np:G</ta>
            <ta e="T834" id="Seg_11198" s="T833">pro.h:A</ta>
            <ta e="T837" id="Seg_11199" s="T836">adv:Time</ta>
            <ta e="T838" id="Seg_11200" s="T837">pro.h:A</ta>
            <ta e="T842" id="Seg_11201" s="T841">np:Th</ta>
            <ta e="T843" id="Seg_11202" s="T842">0.1.h:A</ta>
            <ta e="T845" id="Seg_11203" s="T844">np:Th</ta>
            <ta e="T847" id="Seg_11204" s="T846">np:Th</ta>
            <ta e="T848" id="Seg_11205" s="T847">0.1.h:A</ta>
            <ta e="T854" id="Seg_11206" s="T853">adv:L</ta>
            <ta e="T856" id="Seg_11207" s="T855">np.h:A</ta>
            <ta e="T858" id="Seg_11208" s="T857">0.3.h:A</ta>
            <ta e="T859" id="Seg_11209" s="T858">0.3.h:A</ta>
            <ta e="T861" id="Seg_11210" s="T860">np:Th</ta>
            <ta e="T865" id="Seg_11211" s="T864">np:Th</ta>
            <ta e="T866" id="Seg_11212" s="T865">0.3.h:A</ta>
            <ta e="T867" id="Seg_11213" s="T866">pro.h:A</ta>
            <ta e="T871" id="Seg_11214" s="T870">np.h:R</ta>
            <ta e="T873" id="Seg_11215" s="T872">np:Th</ta>
            <ta e="T874" id="Seg_11216" s="T873">0.3.h:A</ta>
            <ta e="T875" id="Seg_11217" s="T874">0.3.h:A</ta>
            <ta e="T876" id="Seg_11218" s="T875">np.h:A 0.1.h:Poss</ta>
            <ta e="T879" id="Seg_11219" s="T878">np:P</ta>
            <ta e="T882" id="Seg_11220" s="T881">pro.h:B</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T21" id="Seg_11221" s="T20">TURK:disc</ta>
            <ta e="T29" id="Seg_11222" s="T28">TURK:disc</ta>
            <ta e="T31" id="Seg_11223" s="T30">RUS:core</ta>
            <ta e="T36" id="Seg_11224" s="T35">RUS:core</ta>
            <ta e="T57" id="Seg_11225" s="T56">TURK:core</ta>
            <ta e="T68" id="Seg_11226" s="T67">RUS:core</ta>
            <ta e="T70" id="Seg_11227" s="T69">RUS:mod</ta>
            <ta e="T110" id="Seg_11228" s="T109">RUS:disc</ta>
            <ta e="T112" id="Seg_11229" s="T111">RUS:mod</ta>
            <ta e="T114" id="Seg_11230" s="T113">TURK:disc</ta>
            <ta e="T122" id="Seg_11231" s="T121">RUS:disc</ta>
            <ta e="T162" id="Seg_11232" s="T161">RUS:mod</ta>
            <ta e="T170" id="Seg_11233" s="T169">RUS:gram</ta>
            <ta e="T173" id="Seg_11234" s="T172">RUS:disc</ta>
            <ta e="T177" id="Seg_11235" s="T176">RUS:mod</ta>
            <ta e="T179" id="Seg_11236" s="T178">RUS:gram</ta>
            <ta e="T193" id="Seg_11237" s="T192">TURK:gram(INDEF)</ta>
            <ta e="T206" id="Seg_11238" s="T205">TURK:disc</ta>
            <ta e="T208" id="Seg_11239" s="T207">RUS:gram</ta>
            <ta e="T211" id="Seg_11240" s="T210">TURK:disc</ta>
            <ta e="T218" id="Seg_11241" s="T217">RUS:gram</ta>
            <ta e="T221" id="Seg_11242" s="T220">TURK:disc</ta>
            <ta e="T224" id="Seg_11243" s="T223">RUS:gram</ta>
            <ta e="T227" id="Seg_11244" s="T226">TURK:gram(INDEF)</ta>
            <ta e="T244" id="Seg_11245" s="T243">RUS:gram</ta>
            <ta e="T246" id="Seg_11246" s="T245">TURK:disc</ta>
            <ta e="T249" id="Seg_11247" s="T248">RUS:mod</ta>
            <ta e="T255" id="Seg_11248" s="T254">TURK:disc</ta>
            <ta e="T263" id="Seg_11249" s="T262">RUS:cult</ta>
            <ta e="T264" id="Seg_11250" s="T263">%TURK:core</ta>
            <ta e="T265" id="Seg_11251" s="T264">RUS:core</ta>
            <ta e="T266" id="Seg_11252" s="T265">RUS:gram</ta>
            <ta e="T269" id="Seg_11253" s="T267">RUS:core</ta>
            <ta e="T270" id="Seg_11254" s="T269">RUS:core</ta>
            <ta e="T272" id="Seg_11255" s="T271">RUS:core</ta>
            <ta e="T279" id="Seg_11256" s="T278">TURK:core</ta>
            <ta e="T280" id="Seg_11257" s="T279">RUS:gram</ta>
            <ta e="T286" id="Seg_11258" s="T285">TURK:disc</ta>
            <ta e="T288" id="Seg_11259" s="T287">RUS:gram</ta>
            <ta e="T299" id="Seg_11260" s="T298">TURK:disc</ta>
            <ta e="T305" id="Seg_11261" s="T304">RUS:core</ta>
            <ta e="T321" id="Seg_11262" s="T320">RUS:gram</ta>
            <ta e="T323" id="Seg_11263" s="T322">TURK:core</ta>
            <ta e="T326" id="Seg_11264" s="T325">RUS:cult</ta>
            <ta e="T343" id="Seg_11265" s="T342">RUS:core</ta>
            <ta e="T347" id="Seg_11266" s="T346">RUS:gram</ta>
            <ta e="T350" id="Seg_11267" s="T349">RUS:gram</ta>
            <ta e="T362" id="Seg_11268" s="T361">TURK:disc</ta>
            <ta e="T366" id="Seg_11269" s="T365">RUS:gram</ta>
            <ta e="T369" id="Seg_11270" s="T368">TURK:disc</ta>
            <ta e="T376" id="Seg_11271" s="T375">TAT:cult</ta>
            <ta e="T381" id="Seg_11272" s="T380">RUS:gram</ta>
            <ta e="T401" id="Seg_11273" s="T400">RUS:gram</ta>
            <ta e="T402" id="Seg_11274" s="T401">TURK:cult</ta>
            <ta e="T410" id="Seg_11275" s="T409">RUS:gram</ta>
            <ta e="T415" id="Seg_11276" s="T414">RUS:gram</ta>
            <ta e="T425" id="Seg_11277" s="T424">RUS:gram</ta>
            <ta e="T427" id="Seg_11278" s="T426">TURK:disc</ta>
            <ta e="T440" id="Seg_11279" s="T439">RUS:gram</ta>
            <ta e="T442" id="Seg_11280" s="T441">TURK:gram(INDEF)</ta>
            <ta e="T447" id="Seg_11281" s="T446">TURK:gram(INDEF)</ta>
            <ta e="T455" id="Seg_11282" s="T454">TAT:cult</ta>
            <ta e="T457" id="Seg_11283" s="T456">RUS:gram</ta>
            <ta e="T473" id="Seg_11284" s="T472">RUS:gram</ta>
            <ta e="T476" id="Seg_11285" s="T475">RUS:core</ta>
            <ta e="T479" id="Seg_11286" s="T478">RUS:cult</ta>
            <ta e="T482" id="Seg_11287" s="T481">RUS:gram</ta>
            <ta e="T485" id="Seg_11288" s="T484">RUS:gram</ta>
            <ta e="T487" id="Seg_11289" s="T486">RUS:gram</ta>
            <ta e="T497" id="Seg_11290" s="T496">TURK:disc</ta>
            <ta e="T498" id="Seg_11291" s="T497">RUS:cult</ta>
            <ta e="T500" id="Seg_11292" s="T499">RUS:gram</ta>
            <ta e="T506" id="Seg_11293" s="T505">RUS:cult</ta>
            <ta e="T517" id="Seg_11294" s="T516">RUS:cult</ta>
            <ta e="T518" id="Seg_11295" s="T517">TURK:disc</ta>
            <ta e="T520" id="Seg_11296" s="T519">RUS:core</ta>
            <ta e="T524" id="Seg_11297" s="T523">TURK:disc</ta>
            <ta e="T544" id="Seg_11298" s="T543">RUS:gram</ta>
            <ta e="T548" id="Seg_11299" s="T547">RUS:cult</ta>
            <ta e="T555" id="Seg_11300" s="T554">RUS:gram</ta>
            <ta e="T563" id="Seg_11301" s="T562">RUS:cult</ta>
            <ta e="T564" id="Seg_11302" s="T563">RUS:gram</ta>
            <ta e="T568" id="Seg_11303" s="T567">RUS:gram</ta>
            <ta e="T575" id="Seg_11304" s="T574">RUS:gram</ta>
            <ta e="T578" id="Seg_11305" s="T577">TURK:disc</ta>
            <ta e="T581" id="Seg_11306" s="T580">TURK:disc</ta>
            <ta e="T584" id="Seg_11307" s="T583">RUS:cult</ta>
            <ta e="T595" id="Seg_11308" s="T594">RUS:gram</ta>
            <ta e="T607" id="Seg_11309" s="T606">RUS:gram</ta>
            <ta e="T608" id="Seg_11310" s="T607">RUS:cult</ta>
            <ta e="T614" id="Seg_11311" s="T613">RUS:gram</ta>
            <ta e="T615" id="Seg_11312" s="T614">RUS:core</ta>
            <ta e="T619" id="Seg_11313" s="T618">RUS:gram</ta>
            <ta e="T622" id="Seg_11314" s="T621">RUS:gram</ta>
            <ta e="T631" id="Seg_11315" s="T630">RUS:gram</ta>
            <ta e="T637" id="Seg_11316" s="T636">RUS:core</ta>
            <ta e="T639" id="Seg_11317" s="T638">RUS:cult</ta>
            <ta e="T640" id="Seg_11318" s="T639">RUS:core</ta>
            <ta e="T642" id="Seg_11319" s="T641">RUS:gram</ta>
            <ta e="T653" id="Seg_11320" s="T652">RUS:gram</ta>
            <ta e="T655" id="Seg_11321" s="T654">TURK:disc</ta>
            <ta e="T658" id="Seg_11322" s="T657">%TURK:cult</ta>
            <ta e="T681" id="Seg_11323" s="T680">TURK:disc</ta>
            <ta e="T682" id="Seg_11324" s="T681">RUS:gram</ta>
            <ta e="T684" id="Seg_11325" s="T683">TURK:disc</ta>
            <ta e="T689" id="Seg_11326" s="T688">RUS:cult</ta>
            <ta e="T712" id="Seg_11327" s="T711">RUS:gram</ta>
            <ta e="T717" id="Seg_11328" s="T716">RUS:gram</ta>
            <ta e="T718" id="Seg_11329" s="T717">RUS:mod</ta>
            <ta e="T734" id="Seg_11330" s="T733">RUS:gram</ta>
            <ta e="T748" id="Seg_11331" s="T747">RUS:mod</ta>
            <ta e="T751" id="Seg_11332" s="T750">RUS:gram</ta>
            <ta e="T754" id="Seg_11333" s="T753">TURK:core</ta>
            <ta e="T763" id="Seg_11334" s="T762">RUS:gram</ta>
            <ta e="T767" id="Seg_11335" s="T766">RUS:cult</ta>
            <ta e="T769" id="Seg_11336" s="T768">RUS:cult</ta>
            <ta e="T775" id="Seg_11337" s="T774">RUS:gram</ta>
            <ta e="T777" id="Seg_11338" s="T776">TURK:disc</ta>
            <ta e="T785" id="Seg_11339" s="T784">TURK:disc</ta>
            <ta e="T794" id="Seg_11340" s="T793">TURK:gram(INDEF)</ta>
            <ta e="T795" id="Seg_11341" s="T794">RUS:mod</ta>
            <ta e="T805" id="Seg_11342" s="T804">RUS:cult</ta>
            <ta e="T806" id="Seg_11343" s="T805">RUS:cult</ta>
            <ta e="T809" id="Seg_11344" s="T808">RUS:gram</ta>
            <ta e="T818" id="Seg_11345" s="T817">RUS:cult</ta>
            <ta e="T825" id="Seg_11346" s="T824">RUS:cult</ta>
            <ta e="T830" id="Seg_11347" s="T829">RUS:gram</ta>
            <ta e="T833" id="Seg_11348" s="T832">RUS:gram</ta>
            <ta e="T841" id="Seg_11349" s="T840">RUS:cult</ta>
            <ta e="T845" id="Seg_11350" s="T844">RUS:cult</ta>
            <ta e="T850" id="Seg_11351" s="T849">RUS:cult</ta>
            <ta e="T855" id="Seg_11352" s="T854">TURK:disc</ta>
            <ta e="T865" id="Seg_11353" s="T864">RUS:cult</ta>
            <ta e="T870" id="Seg_11354" s="T869">TURK:disc</ta>
            <ta e="T873" id="Seg_11355" s="T872">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T26" id="Seg_11356" s="T25">RUS:int</ta>
            <ta e="T32" id="Seg_11357" s="T31">RUS:int</ta>
            <ta e="T64" id="Seg_11358" s="T63">RUS:int</ta>
            <ta e="T96" id="Seg_11359" s="T95">RUS:int</ta>
            <ta e="T883" id="Seg_11360" s="T283">RUS:int</ta>
            <ta e="T328" id="Seg_11361" s="T326">RUS:ext</ta>
            <ta e="T405" id="Seg_11362" s="T404">RUS:int</ta>
            <ta e="T414" id="Seg_11363" s="T412">RUS:int</ta>
            <ta e="T559" id="Seg_11364" s="T558">RUS:int</ta>
            <ta e="T888" id="Seg_11365" s="T691">RUS:int</ta>
            <ta e="T696" id="Seg_11366" s="T694">RUS:int</ta>
            <ta e="T746" id="Seg_11367" s="T745">RUS:int</ta>
            <ta e="T765" id="Seg_11368" s="T764">RUS:int</ta>
            <ta e="T799" id="Seg_11369" s="T798">RUS:int</ta>
            <ta e="T807" id="Seg_11370" s="T806">RUS:int</ta>
            <ta e="T812" id="Seg_11371" s="T811">RUS:ext</ta>
            <ta e="T889" id="Seg_11372" s="T819">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T5" id="Seg_11373" s="T0">Потом эти парни пошли на дорогу.</ta>
            <ta e="T12" id="Seg_11374" s="T5">Шли, шли, потом сели на краю дороги.</ta>
            <ta e="T18" id="Seg_11375" s="T12">Видят: колдун едет, его лошадь красная.</ta>
            <ta e="T23" id="Seg_11376" s="T18">Потом он сильно…</ta>
            <ta e="T27" id="Seg_11377" s="T23">Потом один [из них] громко свистнул.</ta>
            <ta e="T32" id="Seg_11378" s="T27">Лошадь [упала] на колени.</ta>
            <ta e="T36" id="Seg_11379" s="T32">Лошадь упала на колени.</ta>
            <ta e="T45" id="Seg_11380" s="T36">Колдун (сказал?): "Это дьявол там сидит.</ta>
            <ta e="T47" id="Seg_11381" s="T45">Мне очень страшно!"</ta>
            <ta e="T49" id="Seg_11382" s="T47">И он уехал.</ta>
            <ta e="T53" id="Seg_11383" s="T49">Сидят, сидят, опять [кто-то] едет.</ta>
            <ta e="T60" id="Seg_11384" s="T53">На черной лошади, второй колдун.</ta>
            <ta e="T68" id="Seg_11385" s="T60">Тогда они опять свистнули, она [=лошадь] упала на колени.</ta>
            <ta e="T74" id="Seg_11386" s="T68">Тот тоже говорит: "Дьявол, очень сильный.</ta>
            <ta e="T77" id="Seg_11387" s="T74">Я очень боюсь".</ta>
            <ta e="T83" id="Seg_11388" s="T77">Потом лошадь поднялась, [он] уехал.</ta>
            <ta e="T86" id="Seg_11389" s="T83">Потом они сидят, сидят.</ta>
            <ta e="T93" id="Seg_11390" s="T86">Третья лошадь идет, эта лошадь белая.</ta>
            <ta e="T96" id="Seg_11391" s="T93">Они опять свистнули.</ta>
            <ta e="T101" id="Seg_11392" s="T96">Тогда эта лошадь остановилась, стоит.</ta>
            <ta e="T106" id="Seg_11393" s="T101">Колдун пошел к ним.</ta>
            <ta e="T109" id="Seg_11394" s="T106">"Что за дети сидят?"</ta>
            <ta e="T116" id="Seg_11395" s="T109">"Это мы, нас бросили в воду.</ta>
            <ta e="T120" id="Seg_11396" s="T116">Мы сюда попали".</ta>
            <ta e="T124" id="Seg_11397" s="T120">Тогда [он говорит]: "Ну, живите здесь".</ta>
            <ta e="T128" id="Seg_11398" s="T124">Он им сделал…</ta>
            <ta e="T134" id="Seg_11399" s="T128">Сделал чум, дал хлеб, мясо дал.</ta>
            <ta e="T142" id="Seg_11400" s="T134">"Живите здесь, я вернусь; [если] буду жив, я вас заберу".</ta>
            <ta e="T146" id="Seg_11401" s="T142">Они жили, жили.</ta>
            <ta e="T152" id="Seg_11402" s="T146">Три дня [прошло], идёт одна лошадь, красная.</ta>
            <ta e="T156" id="Seg_11403" s="T152">Человека [на ней] нет.</ta>
            <ta e="T160" id="Seg_11404" s="T156">Потом черная лошадь проскакала.</ta>
            <ta e="T163" id="Seg_11405" s="T160">Тоже нет человека.</ta>
            <ta e="T169" id="Seg_11406" s="T163">Они живут, живут, потом белая лошадь скачет.</ta>
            <ta e="T172" id="Seg_11407" s="T169">И человек сидит.</ta>
            <ta e="T181" id="Seg_11408" s="T172">"Ну, пойдемте со мной, может, вы домой пойдете или у меня будете жить".</ta>
            <ta e="T184" id="Seg_11409" s="T181">Они пошли к этому человеку.</ta>
            <ta e="T187" id="Seg_11410" s="T184">Они пошел домой.</ta>
            <ta e="T200" id="Seg_11411" s="T187">Пришел домой, там никого нет, только чум стоит.</ta>
            <ta e="T202" id="Seg_11412" s="T200">Он вошел.</ta>
            <ta e="T207" id="Seg_11413" s="T202">"Куда все люди ушли?"</ta>
            <ta e="T214" id="Seg_11414" s="T207">"Было два сына.</ta>
            <ta e="T217" id="Seg_11415" s="T214">Их пустили по реке.</ta>
            <ta e="T223" id="Seg_11416" s="T217">А наша дочь всех зарезала".</ta>
            <ta e="T226" id="Seg_11417" s="T223">"А где она?"</ta>
            <ta e="T230" id="Seg_11418" s="T226">"Куда-то ушла, мы не знаем".</ta>
            <ta e="T234" id="Seg_11419" s="T230">Тогда он взял лошадь.</ta>
            <ta e="T242" id="Seg_11420" s="T234">Поехал искать [ее], увидел, они увидели друг друга.</ta>
            <ta e="T247" id="Seg_11421" s="T242">Он стал с ней драться.</ta>
            <ta e="T253" id="Seg_11422" s="T247">Она хотела его ударить ножом.</ta>
            <ta e="T262" id="Seg_11423" s="T253">Он говорит: "Не режь [меня], я твой брат!"</ta>
            <ta e="T264" id="Seg_11424" s="T262">Курицы разговаривают</ta>
            <ta e="T269" id="Seg_11425" s="T264">Капалуха и куропатка.</ta>
            <ta e="T272" id="Seg_11426" s="T269">Куропатка говорит капалухе:</ta>
            <ta e="T279" id="Seg_11427" s="T272">"Ты некрасивая, что у тебя за штаны, плохие.</ta>
            <ta e="T283" id="Seg_11428" s="T279">А я красивая.</ta>
            <ta e="T292" id="Seg_11429" s="T283">Я вырвала шерсть у гнедого коня и приделала ее себе на голову.</ta>
            <ta e="T296" id="Seg_11430" s="T292">Теперь я очень красивая.</ta>
            <ta e="T301" id="Seg_11431" s="T297">На горе котел кипит.</ta>
            <ta e="T304" id="Seg_11432" s="T301">Что это такое?</ta>
            <ta e="T305" id="Seg_11433" s="T304">Это муравьи.</ta>
            <ta e="T308" id="Seg_11434" s="T306">На горе…</ta>
            <ta e="T312" id="Seg_11435" s="T309">Над горой птица летает.</ta>
            <ta e="T314" id="Seg_11436" s="T312">Что это такое?</ta>
            <ta e="T315" id="Seg_11437" s="T314">Грудь.</ta>
            <ta e="T326" id="Seg_11438" s="T316">Сама в земле сидит, а уши с сережками в другом месте.</ta>
            <ta e="T328" id="Seg_11439" s="T326">Ответ: саранка. </ta>
            <ta e="T333" id="Seg_11440" s="T329">Две женщины лежат.</ta>
            <ta e="T340" id="Seg_11441" s="T334">Лежат около огня, беременные.</ta>
            <ta e="T343" id="Seg_11442" s="T340">Это ляжки ног.</ta>
            <ta e="T349" id="Seg_11443" s="T344">Ног нет, рук нет.</ta>
            <ta e="T352" id="Seg_11444" s="T349">А идет…</ta>
            <ta e="T355" id="Seg_11445" s="T352">На гору идет.</ta>
            <ta e="T357" id="Seg_11446" s="T355">Это солнце.</ta>
            <ta e="T364" id="Seg_11447" s="T358">Много птиц друг у друга на голове.</ta>
            <ta e="T372" id="Seg_11448" s="T365">А зады их далеко друг от друга.</ta>
            <ta e="T374" id="Seg_11449" s="T372">Это чум.</ta>
            <ta e="T380" id="Seg_11450" s="T375">В доме мокрый теленок лежит.</ta>
            <ta e="T385" id="Seg_11451" s="T380">А (сказать?) это язык.</ta>
            <ta e="T392" id="Seg_11452" s="T386">На березах (?) сидят.</ta>
            <ta e="T395" id="Seg_11453" s="T393">Это язык.</ta>
            <ta e="T403" id="Seg_11454" s="T396">Ног-рук нет, а Богу молится.</ta>
            <ta e="T405" id="Seg_11455" s="T403">Это колыбель.</ta>
            <ta e="T409" id="Seg_11456" s="T406">Ног-рук нет.</ta>
            <ta e="T418" id="Seg_11457" s="T409">А к деревьям ходит - кланяется, кланяется; а домой приходит - лежит.</ta>
            <ta e="T420" id="Seg_11458" s="T418">Это топор.</ta>
            <ta e="T424" id="Seg_11459" s="T421">Рук-ног нет.</ta>
            <ta e="T428" id="Seg_11460" s="T424">А дверь открывает.</ta>
            <ta e="T430" id="Seg_11461" s="T428">Это ветер.</ta>
            <ta e="T437" id="Seg_11462" s="T431">Маленькая собачка сидит около дома.</ta>
            <ta e="T445" id="Seg_11463" s="T437">Не лает и никого не … в дом.</ta>
            <ta e="T450" id="Seg_11464" s="T446">Никого в дом не пускает.</ta>
            <ta e="T452" id="Seg_11465" s="T450">Это замок.</ta>
            <ta e="T456" id="Seg_11466" s="T453">В Красноярске дома стучат.</ta>
            <ta e="T460" id="Seg_11467" s="T456">А здесь деревья слушают.</ta>
            <ta e="T464" id="Seg_11468" s="T460">Это письма идет оттуда.</ta>
            <ta e="T477" id="Seg_11469" s="T466">Языка нет, глаз нет, рта нет, а говорит, сколько времени прошло.</ta>
            <ta e="T479" id="Seg_11470" s="T477">Это часы.</ta>
            <ta e="T484" id="Seg_11471" s="T480">Когда лежит, она меньше кошки.</ta>
            <ta e="T491" id="Seg_11472" s="T484">А когда стоит, то больше лошади.</ta>
            <ta e="T493" id="Seg_11473" s="T492">Дуга.</ta>
            <ta e="T499" id="Seg_11474" s="T494">Сидит девица, вся в оспинках.</ta>
            <ta e="T502" id="Seg_11475" s="T499">Это наперсток.</ta>
            <ta e="T513" id="Seg_11476" s="T503">Я собирал-собирал нитку, но не смог собрать, - это дорога.</ta>
            <ta e="T517" id="Seg_11477" s="T514">Сидит старик на грядке.</ta>
            <ta e="T520" id="Seg_11478" s="T517">Весь в заплатках.</ta>
            <ta e="T525" id="Seg_11479" s="T520">Кто его увидит, плачет.</ta>
            <ta e="T527" id="Seg_11480" s="T525">Это лук.</ta>
            <ta e="T532" id="Seg_11481" s="T528">Сидит, одежды много…</ta>
            <ta e="T539" id="Seg_11482" s="T533">Семь… </ta>
            <ta e="T546" id="Seg_11483" s="T539">Семнадцать одежек, а застегнуть нечем.</ta>
            <ta e="T548" id="Seg_11484" s="T546">Это капуста.</ta>
            <ta e="T554" id="Seg_11485" s="T549">Сидит девица в земле, сама красная.</ta>
            <ta e="T557" id="Seg_11486" s="T554">А волосы снаружи.</ta>
            <ta e="T559" id="Seg_11487" s="T557">Это морковка.</ta>
            <ta e="T567" id="Seg_11488" s="T560">Я нашел плетку, но не могу ее (унести?).</ta>
            <ta e="T570" id="Seg_11489" s="T567">Что это?</ta>
            <ta e="T571" id="Seg_11490" s="T570">Змея.</ta>
            <ta e="T581" id="Seg_11491" s="T572">Идет девица, и ее волосы (?).</ta>
            <ta e="T585" id="Seg_11492" s="T582">Это сорока.</ta>
            <ta e="T598" id="Seg_11493" s="T586">Двое стоят, двое лежат, пятый идет, седьмой закрывает.</ta>
            <ta e="T601" id="Seg_11494" s="T598">Это дверь.</ta>
            <ta e="T611" id="Seg_11495" s="T602">Много овец пасется, а [у] пастуха рога.</ta>
            <ta e="T615" id="Seg_11496" s="T611">Это месяц и звезды.</ta>
            <ta e="T624" id="Seg_11497" s="T616">Две дыры, два острия, а в середине пуговка.</ta>
            <ta e="T625" id="Seg_11498" s="T624">Ножницы.</ta>
            <ta e="T635" id="Seg_11499" s="T626">Я все время ем, а испражняться не могу. - Мешок.</ta>
            <ta e="T639" id="Seg_11500" s="T636">Петух сидит на заборе.</ta>
            <ta e="T644" id="Seg_11501" s="T639">Хвост на земле, и Богу кричит.</ta>
            <ta e="T646" id="Seg_11502" s="T644">Это колокол.</ta>
            <ta e="T656" id="Seg_11503" s="T647">Стоит парень, внутри кипит, а сам писает.</ta>
            <ta e="T658" id="Seg_11504" s="T656">Это самовар.</ta>
            <ta e="T661" id="Seg_11505" s="T659">Двое идет.</ta>
            <ta e="T665" id="Seg_11506" s="T661">Один идет, другой догоняет.</ta>
            <ta e="T667" id="Seg_11507" s="T665">Это ноги.</ta>
            <ta e="T676" id="Seg_11508" s="T668">Ударь меня, бей меня, залезай на меня, у меня есть…</ta>
            <ta e="T678" id="Seg_11509" s="T676">Это орех.</ta>
            <ta e="T687" id="Seg_11510" s="T679">Оно висит, а люди моются и вытираются.</ta>
            <ta e="T690" id="Seg_11511" s="T687">Это полотенце.</ta>
            <ta e="T696" id="Seg_11512" s="T691">Вокруг дерева растет золотая трава.</ta>
            <ta e="T698" id="Seg_11513" s="T696">Это палец.</ta>
            <ta e="T706" id="Seg_11514" s="T699">[Где] красный козёл лежал, там трава не растёт.</ta>
            <ta e="T708" id="Seg_11515" s="T706">Это огонь.</ta>
            <ta e="T721" id="Seg_11516" s="T709">[Она] пошла за ягодами, и я пошла собирать ягоды, и ещё одна девушка пошла.</ta>
            <ta e="T728" id="Seg_11517" s="T721">Мы пошли в (?).</ta>
            <ta e="T730" id="Seg_11518" s="T728">Мы туда пришли.</ta>
            <ta e="T736" id="Seg_11519" s="T730">Ягоды (прячутся?), мы ищем и немного собрали.</ta>
            <ta e="T743" id="Seg_11520" s="T736">Потом мы пошли туда, где пасется лошадь, и мать сказала:</ta>
            <ta e="T747" id="Seg_11521" s="T743">"Очень темная туча идет.</ta>
            <ta e="T755" id="Seg_11522" s="T747">Надо поставить чум, иначе будет плохо спать".</ta>
            <ta e="T758" id="Seg_11523" s="T755">Тогда мы сделали чум.</ta>
            <ta e="T762" id="Seg_11524" s="T758">Согрели, выпили чаю.</ta>
            <ta e="T768" id="Seg_11525" s="T762">А там был половик.</ta>
            <ta e="T771" id="Seg_11526" s="T768">Мы (повесили?) половик на деревья.</ta>
            <ta e="T774" id="Seg_11527" s="T771">Потом легли спать.</ta>
            <ta e="T782" id="Seg_11528" s="T774">А ночью был очень сильный дождь.</ta>
            <ta e="T786" id="Seg_11529" s="T782">Утром мы встали мокрые.</ta>
            <ta e="T790" id="Seg_11530" s="T786">Мы поймали лошадей и поехали домой.</ta>
            <ta e="T793" id="Seg_11531" s="T790">Доехали до реки.</ta>
            <ta e="T796" id="Seg_11532" s="T793">Никак нельзя перейти.</ta>
            <ta e="T805" id="Seg_11533" s="T796">Тогда мы шли-шли вниз по реке и нашли мост.</ta>
            <ta e="T811" id="Seg_11534" s="T805">Мост перешли и пришли домой.</ta>
            <ta e="T812" id="Seg_11535" s="T811">Все. </ta>
            <ta e="T817" id="Seg_11536" s="T813">Я сегодня встала рано.</ta>
            <ta e="T819" id="Seg_11537" s="T817">Пошла в магазин.</ta>
            <ta e="T821" id="Seg_11538" s="T819">Встала в очередь.</ta>
            <ta e="T832" id="Seg_11539" s="T821">Стояла два часа, потом купила рыбу и пришла домой.</ta>
            <ta e="T835" id="Seg_11540" s="T832">И вы пришли.</ta>
            <ta e="T845" id="Seg_11541" s="T836">Я купила один килограмм, денег отдала один рубль.</ta>
            <ta e="T848" id="Seg_11542" s="T845">Четырнадцать [копеек] отдала.</ta>
            <ta e="T852" id="Seg_11543" s="T848">Один рубль четырнадцать [копеек].</ta>
            <ta e="T858" id="Seg_11544" s="T853">Там женщины ругаются, кричат.</ta>
            <ta e="T861" id="Seg_11545" s="T858">Говорят: "Мало рыбы.</ta>
            <ta e="T866" id="Seg_11546" s="T861">Один килограмм дают [в одни руки].</ta>
            <ta e="T874" id="Seg_11547" s="T866">Он(а) дал(а) каждой жещине один килограмм.</ta>
            <ta e="T877" id="Seg_11548" s="T874">Они говорят: "Мои дети кричат.</ta>
            <ta e="T882" id="Seg_11549" s="T877">Им надо рыбу есть".</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T5" id="Seg_11550" s="T0">Then those boy went on the road.</ta>
            <ta e="T12" id="Seg_11551" s="T5">They walked and walked, then they sat down by the side of the road.</ta>
            <ta e="T18" id="Seg_11552" s="T12">They see: a sorcerer comes, the horse is red.</ta>
            <ta e="T23" id="Seg_11553" s="T18">Then he strongly…</ta>
            <ta e="T27" id="Seg_11554" s="T23">Then one [of them?] whistled strongly.</ta>
            <ta e="T32" id="Seg_11555" s="T27">His horse [fell] on its knees.</ta>
            <ta e="T36" id="Seg_11556" s="T32">The horse fell down on its knees.</ta>
            <ta e="T45" id="Seg_11557" s="T36">The sorcerer (said?): "An evil spirit is sitting there.</ta>
            <ta e="T47" id="Seg_11558" s="T45">I'm very afraid!"</ta>
            <ta e="T49" id="Seg_11559" s="T47">Then he went away.</ta>
            <ta e="T53" id="Seg_11560" s="T49">They were sitting [there] for some time, [then someone] comes again.</ta>
            <ta e="T60" id="Seg_11561" s="T53">On a black horse, a second sorcerer (comes).</ta>
            <ta e="T68" id="Seg_11562" s="T60">Then they whistled again, then it [= the horse] fell on the knees.</ta>
            <ta e="T74" id="Seg_11563" s="T68">He also says: "The evil spirit is very powerful.</ta>
            <ta e="T77" id="Seg_11564" s="T74">I'm very afraid."</ta>
            <ta e="T83" id="Seg_11565" s="T77">Then his horse got up, [he] left.</ta>
            <ta e="T86" id="Seg_11566" s="T83">Then they are sitting, they are sitting.</ta>
            <ta e="T93" id="Seg_11567" s="T86">The third horse comes, this horse is white.</ta>
            <ta e="T96" id="Seg_11568" s="T93">They whistled again.</ta>
            <ta e="T101" id="Seg_11569" s="T96">Then this horse stopped, is standing.</ta>
            <ta e="T106" id="Seg_11570" s="T101">This sorcerer came to them.</ta>
            <ta e="T109" id="Seg_11571" s="T106">"What kind of children are sitting?"</ta>
            <ta e="T116" id="Seg_11572" s="T109">"It's us, we were thrown into the water.</ta>
            <ta e="T120" id="Seg_11573" s="T116">We came here."</ta>
            <ta e="T124" id="Seg_11574" s="T120">Then [he says]: "Well, you live here."</ta>
            <ta e="T128" id="Seg_11575" s="T124">He made them…</ta>
            <ta e="T134" id="Seg_11576" s="T128">He made a tent, gave [them] bread, gave meat.</ta>
            <ta e="T142" id="Seg_11577" s="T134">"Live here, I will return; [if] I am alive, I will take you [from here]."</ta>
            <ta e="T146" id="Seg_11578" s="T142">Then they lived [there] for some time.</ta>
            <ta e="T152" id="Seg_11579" s="T146">Three days [pass], one horse comes, a red one.</ta>
            <ta e="T156" id="Seg_11580" s="T152">Then there is no man.</ta>
            <ta e="T160" id="Seg_11581" s="T156">Then a black horse comes.</ta>
            <ta e="T163" id="Seg_11582" s="T160">There is also no man.</ta>
            <ta e="T169" id="Seg_11583" s="T163">They are living [there], then a white horse comes.</ta>
            <ta e="T172" id="Seg_11584" s="T169">And a man is sitting.</ta>
            <ta e="T181" id="Seg_11585" s="T172">"Well, let’s go with me, you maybe go home or you live at my place."</ta>
            <ta e="T184" id="Seg_11586" s="T181">They came to this man.</ta>
            <ta e="T187" id="Seg_11587" s="T184">One went to his home.</ta>
            <ta e="T200" id="Seg_11588" s="T187">He came home, nobody is there, [only] one tent stands.</ta>
            <ta e="T202" id="Seg_11589" s="T200">He came.</ta>
            <ta e="T207" id="Seg_11590" s="T202">"Where did all the people go?"</ta>
            <ta e="T214" id="Seg_11591" s="T207">"There were two sons.</ta>
            <ta e="T217" id="Seg_11592" s="T214">They were sent to the river.</ta>
            <ta e="T223" id="Seg_11593" s="T217">And our daughter killed all the people."</ta>
            <ta e="T226" id="Seg_11594" s="T223">"Where is she?"</ta>
            <ta e="T230" id="Seg_11595" s="T226">"She went somewhere, we don’t know."</ta>
            <ta e="T234" id="Seg_11596" s="T230">Then he took a horse.</ta>
            <ta e="T242" id="Seg_11597" s="T234">He went to look for [her], he saw [it], they saw (each other) there.</ta>
            <ta e="T247" id="Seg_11598" s="T242">Then he started fighting with it.</ta>
            <ta e="T253" id="Seg_11599" s="T247">She wanted to stab [him] with a knife.</ta>
            <ta e="T262" id="Seg_11600" s="T253">He says: "Don't stab [me], I am your brother!"</ta>
            <ta e="T264" id="Seg_11601" s="T262">Chickens are talking.</ta>
            <ta e="T269" id="Seg_11602" s="T264">A capercaillie hen and… a partridge.</ta>
            <ta e="T272" id="Seg_11603" s="T269">The partridge says:</ta>
            <ta e="T279" id="Seg_11604" s="T272">"You are not beautiful, what sort of pants, not good.</ta>
            <ta e="T283" id="Seg_11605" s="T279">But I am beautiful.</ta>
            <ta e="T292" id="Seg_11606" s="T283">I tore off hair of a brown horse for myself and put [it] on my own head.</ta>
            <ta e="T296" id="Seg_11607" s="T292">Now I am very beautiful.</ta>
            <ta e="T301" id="Seg_11608" s="T297">A cauldron is boiling on a mountain.</ta>
            <ta e="T304" id="Seg_11609" s="T301">What's that?</ta>
            <ta e="T305" id="Seg_11610" s="T304">Ants.</ta>
            <ta e="T308" id="Seg_11611" s="T306">On a mountain…</ta>
            <ta e="T312" id="Seg_11612" s="T309">A bird is flying above the mountain.</ta>
            <ta e="T314" id="Seg_11613" s="T312">What's that?</ta>
            <ta e="T315" id="Seg_11614" s="T314">A breast.</ta>
            <ta e="T326" id="Seg_11615" s="T316">It sits in the ground, and its ears with earrings is in another place.</ta>
            <ta e="T328" id="Seg_11616" s="T326">The answer is: Lilium martagon.</ta>
            <ta e="T333" id="Seg_11617" s="T329">Tho women are lying.</ta>
            <ta e="T340" id="Seg_11618" s="T334">They are lying near the fire, [they are] pregnant.</ta>
            <ta e="T343" id="Seg_11619" s="T340">This is the thighs of the legs.</ta>
            <ta e="T349" id="Seg_11620" s="T344">No legs, no arms.</ta>
            <ta e="T352" id="Seg_11621" s="T349">But it goes…</ta>
            <ta e="T355" id="Seg_11622" s="T352">It goes to the mountain.</ta>
            <ta e="T357" id="Seg_11623" s="T355">This is sun.</ta>
            <ta e="T364" id="Seg_11624" s="T358">Many birds [are sitting] on each other's head.</ta>
            <ta e="T372" id="Seg_11625" s="T365">And their backs are far from each another.</ta>
            <ta e="T374" id="Seg_11626" s="T372">It's a tent.</ta>
            <ta e="T380" id="Seg_11627" s="T375">A wet calf is lying in a house.</ta>
            <ta e="T385" id="Seg_11628" s="T380">And to say (?) this is the tongue.</ta>
            <ta e="T392" id="Seg_11629" s="T386">A (?) is sitting on birch trees.</ta>
            <ta e="T395" id="Seg_11630" s="T393">This is a tongue.</ta>
            <ta e="T403" id="Seg_11631" s="T396">No legs, no arms, but it prays to God.</ta>
            <ta e="T405" id="Seg_11632" s="T403">This is a cradle.</ta>
            <ta e="T409" id="Seg_11633" s="T406">No legs, no arms.</ta>
            <ta e="T418" id="Seg_11634" s="T409">But it goes to the trees and bows [to them]; and when it comes home, it lies.</ta>
            <ta e="T420" id="Seg_11635" s="T418">This is an axe.</ta>
            <ta e="T424" id="Seg_11636" s="T421">No arms, no legs.</ta>
            <ta e="T428" id="Seg_11637" s="T424">But it opens the door.</ta>
            <ta e="T430" id="Seg_11638" s="T428">This is the wind.</ta>
            <ta e="T437" id="Seg_11639" s="T431">A little dog is sitting near the house.</ta>
            <ta e="T445" id="Seg_11640" s="T437">It doesn't bark and doesn't … anybody in the house.</ta>
            <ta e="T450" id="Seg_11641" s="T446">Doesn't let anybody enter the house.</ta>
            <ta e="T452" id="Seg_11642" s="T450">This is a lock.</ta>
            <ta e="T460" id="Seg_11643" s="T456">And there, the trees are listening.</ta>
            <ta e="T464" id="Seg_11644" s="T460">It's a letter that comes from there.</ta>
            <ta e="T477" id="Seg_11645" s="T466">No tongue, no eyes, no mouth, and it says how much time has passed.</ta>
            <ta e="T479" id="Seg_11646" s="T477">This is a watch.</ta>
            <ta e="T484" id="Seg_11647" s="T480">When it lies, it is smaller than a cat.</ta>
            <ta e="T491" id="Seg_11648" s="T484">And when it stands, it is bigger than a horse.</ta>
            <ta e="T493" id="Seg_11649" s="T492">A shaft bow.</ta>
            <ta e="T499" id="Seg_11650" s="T494">A girl is sitting covered with spots of smallpox.</ta>
            <ta e="T502" id="Seg_11651" s="T499">This is a thimble.</ta>
            <ta e="T513" id="Seg_11652" s="T503">I reeled thread, but couldn't reel [all of it], this is a road.</ta>
            <ta e="T517" id="Seg_11653" s="T514">A man is sitting on a garden bed.</ta>
            <ta e="T520" id="Seg_11654" s="T517">He has a lot of patches.</ta>
            <ta e="T525" id="Seg_11655" s="T520">Who sees him, cries.</ta>
            <ta e="T527" id="Seg_11656" s="T525">This is onion.</ta>
            <ta e="T532" id="Seg_11657" s="T528">It sits, a lot of clothes…</ta>
            <ta e="T539" id="Seg_11658" s="T533">Seven… </ta>
            <ta e="T546" id="Seg_11659" s="T539">Seventeen clothes, but nothing to fasten [them = without buckles].</ta>
            <ta e="T548" id="Seg_11660" s="T546">This is cabbage.</ta>
            <ta e="T554" id="Seg_11661" s="T549">A girl is sitting in the ground, she is red.</ta>
            <ta e="T557" id="Seg_11662" s="T554">And her hair is outside.</ta>
            <ta e="T559" id="Seg_11663" s="T557">This is a carrot.</ta>
            <ta e="T567" id="Seg_11664" s="T560">I found a lash, but cannot take it (away?).</ta>
            <ta e="T570" id="Seg_11665" s="T567">What's that?</ta>
            <ta e="T571" id="Seg_11666" s="T570">A snake.</ta>
            <ta e="T581" id="Seg_11667" s="T572">A girl is coming, her hair is (?).</ta>
            <ta e="T585" id="Seg_11668" s="T582">It's a magpie.</ta>
            <ta e="T598" id="Seg_11669" s="T586">Two are standing, two are lying, the fifth is going, the seventh is closing.</ta>
            <ta e="T601" id="Seg_11670" s="T598">This is a door.</ta>
            <ta e="T611" id="Seg_11671" s="T602">Many sheep are going, and the shepherd [has] horns.</ta>
            <ta e="T615" id="Seg_11672" s="T611">These are moon and stars.</ta>
            <ta e="T624" id="Seg_11673" s="T616">Two holes and two points, and inside there is a button.</ta>
            <ta e="T625" id="Seg_11674" s="T624">Scissors.</ta>
            <ta e="T635" id="Seg_11675" s="T626">I always eat, but cannot defecate. - A bag.</ta>
            <ta e="T639" id="Seg_11676" s="T636">A rooster is sitting on a fence.</ta>
            <ta e="T644" id="Seg_11677" s="T639">[Its] tail is on the groung, and it's shouting to God.</ta>
            <ta e="T646" id="Seg_11678" s="T644">It's a bell.</ta>
            <ta e="T656" id="Seg_11679" s="T647">There stands a boy, inside he's boiling, and himself is pissing.</ta>
            <ta e="T658" id="Seg_11680" s="T656">That's a samovar.</ta>
            <ta e="T661" id="Seg_11681" s="T659">Two [persons] are going.</ta>
            <ta e="T665" id="Seg_11682" s="T661">One is going, another is catching up.</ta>
            <ta e="T667" id="Seg_11683" s="T665">It's the legs.</ta>
            <ta e="T676" id="Seg_11684" s="T668">Beat me, hit me, climb to me, I have…</ta>
            <ta e="T678" id="Seg_11685" s="T676">This is a nut.</ta>
            <ta e="T687" id="Seg_11686" s="T679">It hangs, and people wash and wipe oneself.</ta>
            <ta e="T690" id="Seg_11687" s="T687">This is a towel.</ta>
            <ta e="T696" id="Seg_11688" s="T691">Golden grass grows around [a] tree.</ta>
            <ta e="T698" id="Seg_11689" s="T696">It's a finger.</ta>
            <ta e="T706" id="Seg_11690" s="T699">[Where] a red goat was lying, there the grass doesn't grow.</ta>
            <ta e="T708" id="Seg_11691" s="T706">It's fire.</ta>
            <ta e="T721" id="Seg_11692" s="T709">[She] went to collect berries, and I went, and one more girl went.</ta>
            <ta e="T728" id="Seg_11693" s="T721">We went to (?).</ta>
            <ta e="T730" id="Seg_11694" s="T728">We came there.</ta>
            <ta e="T736" id="Seg_11695" s="T730">The berries are (hiding?), we searched [them] and gathered a few.</ta>
            <ta e="T743" id="Seg_11696" s="T736">Then we went where the horse was grazing, [and] my mother said:</ta>
            <ta e="T747" id="Seg_11697" s="T743">"A very black cloud is coming.</ta>
            <ta e="T755" id="Seg_11698" s="T747">[We] had to make a tent, otherwise it will be not good to sleep."</ta>
            <ta e="T758" id="Seg_11699" s="T755">Then we made a tent.</ta>
            <ta e="T762" id="Seg_11700" s="T758">We boiled, drank tea.</ta>
            <ta e="T768" id="Seg_11701" s="T762">There was a mat.</ta>
            <ta e="T771" id="Seg_11702" s="T768">We (hung?) the mat on the trees.</ta>
            <ta e="T774" id="Seg_11703" s="T771">Then we went to sleep.</ta>
            <ta e="T782" id="Seg_11704" s="T774">In the nicht there was a very strong rain.</ta>
            <ta e="T786" id="Seg_11705" s="T782">In the morning we got up, we were wet.</ta>
            <ta e="T790" id="Seg_11706" s="T786">We caught the horses and went home.</ta>
            <ta e="T793" id="Seg_11707" s="T790">We reached the river.</ta>
            <ta e="T796" id="Seg_11708" s="T793">There was no way to cross it].</ta>
            <ta e="T805" id="Seg_11709" s="T796">Then we walked down the river and found a bridge.</ta>
            <ta e="T811" id="Seg_11710" s="T805">We crossed the bridge and came home.</ta>
            <ta e="T812" id="Seg_11711" s="T811">That's all.</ta>
            <ta e="T817" id="Seg_11712" s="T813">Today I got up early.</ta>
            <ta e="T819" id="Seg_11713" s="T817">I went to the shop.</ta>
            <ta e="T821" id="Seg_11714" s="T819">I stood in a queue.</ta>
            <ta e="T832" id="Seg_11715" s="T821">Then… I stood for two hours, then I bought fish and came home.</ta>
            <ta e="T835" id="Seg_11716" s="T832">And you came.</ta>
            <ta e="T845" id="Seg_11717" s="T836">I bought one kilogramm, gave [= paid] one rouble.</ta>
            <ta e="T848" id="Seg_11718" s="T845">I gave forteen [kopecks].</ta>
            <ta e="T852" id="Seg_11719" s="T848">One rouble forteen.</ta>
            <ta e="T858" id="Seg_11720" s="T853">Women are scolding, shouting here.</ta>
            <ta e="T861" id="Seg_11721" s="T858">They say: "There is little fish</ta>
            <ta e="T866" id="Seg_11722" s="T861">[The salesperson] gives one kilogramm [to one person].</ta>
            <ta e="T874" id="Seg_11723" s="T866">S/he gave one kilogramm to every woman.</ta>
            <ta e="T877" id="Seg_11724" s="T874">They say: "My children are screaming.</ta>
            <ta e="T882" id="Seg_11725" s="T877">They need to eat fish".</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T5" id="Seg_11726" s="T0">Dann gingen diese Jungen auf die Straße.</ta>
            <ta e="T12" id="Seg_11727" s="T5">Sie kamen, sie kamen, dann setzten sie sich auf die Seite der Straße hin.</ta>
            <ta e="T18" id="Seg_11728" s="T12">Sie sahen: ein Zauberer kommt, das Pferd ist rot.</ta>
            <ta e="T23" id="Seg_11729" s="T18">Dann er… kräftig…</ta>
            <ta e="T27" id="Seg_11730" s="T23">Dann pfiff einer [von ihnen?] kräftig.</ta>
            <ta e="T32" id="Seg_11731" s="T27">Sein Pferd [fiel] in die Knie.</ta>
            <ta e="T36" id="Seg_11732" s="T32">Das Pferd fiel in die Knie hinab.</ta>
            <ta e="T45" id="Seg_11733" s="T36">Der Zauberer (sagte?): „Ein böser Geist sitzt dort.</ta>
            <ta e="T47" id="Seg_11734" s="T45">Ich habe große Angst!“</ta>
            <ta e="T49" id="Seg_11735" s="T47">Dann ging er weg.</ta>
            <ta e="T53" id="Seg_11736" s="T49">Sie saßen einiger Zeit [dort], [dann] kommt [jemand] wieder.</ta>
            <ta e="T60" id="Seg_11737" s="T53">Auf einem schwarzen Pferd, (kommt) ein zweiter Zauberer.</ta>
            <ta e="T68" id="Seg_11738" s="T60">Dann pfiffen sie wieder, dann fiel es [=das Pferd] in die Knie.</ta>
            <ta e="T74" id="Seg_11739" s="T68">Er sagte auch: „Der böse Geist ist sehr mächtig.</ta>
            <ta e="T77" id="Seg_11740" s="T74">Ich habe große Angst.“</ta>
            <ta e="T83" id="Seg_11741" s="T77">Dann richtete sein Pferd sich auf, ging.</ta>
            <ta e="T86" id="Seg_11742" s="T83">Dann sitzen sie, sie sitzen.</ta>
            <ta e="T93" id="Seg_11743" s="T86">Das dritte Pferd kommt, dieses Pferd ist weiß.</ta>
            <ta e="T96" id="Seg_11744" s="T93">Sie pfiffen wieder.</ta>
            <ta e="T101" id="Seg_11745" s="T96">Dann hielt dieses Pferd an, steht.</ta>
            <ta e="T106" id="Seg_11746" s="T101">Dieser Zauberer kam zu ihnen.</ta>
            <ta e="T109" id="Seg_11747" s="T106">„Welche Art von Kinder sitzen?“</ta>
            <ta e="T116" id="Seg_11748" s="T109">„Wir sind’s, wir wurden ins Wasser geworfen. </ta>
            <ta e="T120" id="Seg_11749" s="T116">Wir kamen her.“</ta>
            <ta e="T124" id="Seg_11750" s="T120">Dann [sagt er]: „Also, ihr lebt hier.“</ta>
            <ta e="T128" id="Seg_11751" s="T124">Er machte ihnen…</ta>
            <ta e="T134" id="Seg_11752" s="T128">Er machte ihnen ein Zelt, gab [ihnen] Brot, gab Fleisch.</ta>
            <ta e="T142" id="Seg_11753" s="T134">„Lebt hier, ich werde zurückkommen; [wenn] ich lebe, werde ich euch [von hier] nehmen.“</ta>
            <ta e="T146" id="Seg_11754" s="T142">Dann lebten sie [dort] einiger Zeit lang.</ta>
            <ta e="T152" id="Seg_11755" s="T146">Drei Tage [vergehen], ein Pferd kommt, ein rotes.</ta>
            <ta e="T156" id="Seg_11756" s="T152">Dann gibt es keinen Mann.</ta>
            <ta e="T160" id="Seg_11757" s="T156">Dann kommt ein schwarzes Pferd.</ta>
            <ta e="T163" id="Seg_11758" s="T160">Da ist auch kein Mann.</ta>
            <ta e="T169" id="Seg_11759" s="T163">Sie leben [dort], dann kommt ein weißes Pferd.</ta>
            <ta e="T172" id="Seg_11760" s="T169">Und ein Mann sitzt.</ta>
            <ta e="T181" id="Seg_11761" s="T172">„Also, gehen wir mit mir, ihr geht vielleicht nach Hause, oder ihr lebt bei mir.“</ta>
            <ta e="T184" id="Seg_11762" s="T181">Sie kamen zu diesem Mann.</ta>
            <ta e="T187" id="Seg_11763" s="T184">Einer ging zu sich nach Hause.</ta>
            <ta e="T200" id="Seg_11764" s="T187">Er kam nach Hause, keiner ist da, [nur] ein Zelt steht.</ta>
            <ta e="T202" id="Seg_11765" s="T200">Er kam.</ta>
            <ta e="T207" id="Seg_11766" s="T202">„Wo sind alle die Leute hingegangen?“</ta>
            <ta e="T214" id="Seg_11767" s="T207">„Es waren zwei Söhne.</ta>
            <ta e="T217" id="Seg_11768" s="T214">Sie worden zum Fluss geschickt.</ta>
            <ta e="T223" id="Seg_11769" s="T217">Und unsere Tochter stach alle Leute.“</ta>
            <ta e="T226" id="Seg_11770" s="T223">„Wo ist sie?“</ta>
            <ta e="T230" id="Seg_11771" s="T226">„Sie ging irgendwo hin, wir wissen’s nicht.“</ta>
            <ta e="T234" id="Seg_11772" s="T230">Dann nahm er ein Pferd.</ta>
            <ta e="T242" id="Seg_11773" s="T234">Er ging nach [ihm] zu schauen, er sah [es], sie sahen (sich) dort.</ta>
            <ta e="T247" id="Seg_11774" s="T242">Dann fing er an, damit zu kämpfen.</ta>
            <ta e="T253" id="Seg_11775" s="T247">Sie wollte [ihn] mit einem Messer stechen.</ta>
            <ta e="T262" id="Seg_11776" s="T253">Er sagt: „Stich [mich] nicht, ich bin dein Bruder!“</ta>
            <ta e="T264" id="Seg_11777" s="T262">Hühner(?) sprechen.</ta>
            <ta e="T269" id="Seg_11778" s="T264">Ein Auerhuhn und… ein Rebhuhn.</ta>
            <ta e="T272" id="Seg_11779" s="T269">Das Rebhuhn sagt:</ta>
            <ta e="T279" id="Seg_11780" s="T272">„Du bist nicht schön, welche Art von Hosen, nicht gut.</ta>
            <ta e="T283" id="Seg_11781" s="T279">Aber ich bin schön.</ta>
            <ta e="T292" id="Seg_11782" s="T283">Ich riss für mich Haar von einem braunen Pferd und setzte [es] auf meinen Kopf.</ta>
            <ta e="T296" id="Seg_11783" s="T292">Jetzt bin ich sehr schön.</ta>
            <ta e="T301" id="Seg_11784" s="T297">Ein Kessel kocht auf einem Berg.</ta>
            <ta e="T304" id="Seg_11785" s="T301">Was ist das?</ta>
            <ta e="T305" id="Seg_11786" s="T304">Ameisen.</ta>
            <ta e="T308" id="Seg_11787" s="T306">Auf einem Berg…</ta>
            <ta e="T312" id="Seg_11788" s="T309">Ein Vogel fliegt über dem Berg.</ta>
            <ta e="T314" id="Seg_11789" s="T312">Was ist das?</ta>
            <ta e="T315" id="Seg_11790" s="T314">Eine Brust.</ta>
            <ta e="T326" id="Seg_11791" s="T316">Sie sitzt in der Erde, und ihre Ohren samt Ohrringe sind in einem anderen Ort.</ta>
            <ta e="T328" id="Seg_11792" s="T326">Die Antwort ist: Lilium martagon.</ta>
            <ta e="T333" id="Seg_11793" s="T329">Zwei Frauen liegen.</ta>
            <ta e="T340" id="Seg_11794" s="T334">Sie liegen nahe dem Feuer, [sie sind] schwanger.</ta>
            <ta e="T343" id="Seg_11795" s="T340">Dieses sind the Oberschenkel der Beine.</ta>
            <ta e="T349" id="Seg_11796" s="T344">Keine Beine, keine Arme.</ta>
            <ta e="T352" id="Seg_11797" s="T349">Aber es geht…</ta>
            <ta e="T355" id="Seg_11798" s="T352">Es geht zu dem Berg.</ta>
            <ta e="T357" id="Seg_11799" s="T355">Das ist Sonne.</ta>
            <ta e="T364" id="Seg_11800" s="T358">Viele Vögel [setzen] sich gegenseitig auf dem Kopf.</ta>
            <ta e="T372" id="Seg_11801" s="T365">Und ihre Rücken sind weit von einander.</ta>
            <ta e="T374" id="Seg_11802" s="T372">Es ist ein Zelt.</ta>
            <ta e="T380" id="Seg_11803" s="T375">Ein nasses Kalb liegt in einem Haus.</ta>
            <ta e="T385" id="Seg_11804" s="T380">Und zu sagen (?) das ist die Zunge.</ta>
            <ta e="T392" id="Seg_11805" s="T386">Ein (?) sitzt in Birken.</ta>
            <ta e="T395" id="Seg_11806" s="T393">Das ist eine Zunge.</ta>
            <ta e="T403" id="Seg_11807" s="T396">Keine Beine, keine Arme, aber sie beugt sich vor Gott.</ta>
            <ta e="T405" id="Seg_11808" s="T403">Eine Wiege.</ta>
            <ta e="T409" id="Seg_11809" s="T406">Keine Beine, keine Arme.</ta>
            <ta e="T418" id="Seg_11810" s="T409">Aber sie geht zu den Bäumen und beugt sich [zu ihnen]; und wenn sie nach Hause kommt, liegt sie.</ta>
            <ta e="T420" id="Seg_11811" s="T418">Das ist aine Axt.</ta>
            <ta e="T424" id="Seg_11812" s="T421">Keine Arme, keine Beine.</ta>
            <ta e="T428" id="Seg_11813" s="T424">Aber sie sperrt die Tür.</ta>
            <ta e="T430" id="Seg_11814" s="T428">Das ist der Wind.</ta>
            <ta e="T437" id="Seg_11815" s="T431">Ein kleiner Hund sitzt nahe dem Haus.</ta>
            <ta e="T445" id="Seg_11816" s="T437">Er bellt nicht und … keinen ins Haus.</ta>
            <ta e="T450" id="Seg_11817" s="T446">Lässt niemanden das Haus betreten.</ta>
            <ta e="T452" id="Seg_11818" s="T450">Das ist ein Schloss</ta>
            <ta e="T456" id="Seg_11819" s="T453">Von Krasnojarsk, klopfen Häuser.</ta>
            <ta e="T460" id="Seg_11820" s="T456">Und da, die Bäume hören.</ta>
            <ta e="T464" id="Seg_11821" s="T460">Es ist ein Brief, der von dort kommt.</ta>
            <ta e="T477" id="Seg_11822" s="T466">Keine Zunge, keine Augen, kein Mund, und sie sagt, wieviel Zeit vergangen ist.</ta>
            <ta e="T479" id="Seg_11823" s="T477">Das ist eine Uhr.</ta>
            <ta e="T484" id="Seg_11824" s="T480">Wenn es liegt, ist es so klein wie eine Katze.</ta>
            <ta e="T491" id="Seg_11825" s="T484">Und wenn es steht, ist es so groß wie ein Pferd.</ta>
            <ta e="T493" id="Seg_11826" s="T492">Ein Pfeil-Bogen.</ta>
            <ta e="T499" id="Seg_11827" s="T494">Ein Mädchen sitzt/lebt ganz (?).</ta>
            <ta e="T502" id="Seg_11828" s="T499">Das ist ein Fingerhut.</ta>
            <ta e="T513" id="Seg_11829" s="T503">Ich spulte Faden, aber konnte nicht [alles] spulen, das ist eine Straße.</ta>
            <ta e="T517" id="Seg_11830" s="T514">Ein Mann sitzt im Gartenbeet.</ta>
            <ta e="T520" id="Seg_11831" s="T517">Er hat viele Flicken.</ta>
            <ta e="T525" id="Seg_11832" s="T520">Wer ihn sieht, weint.</ta>
            <ta e="T527" id="Seg_11833" s="T525">Das ist eine Zwiebel.</ta>
            <ta e="T532" id="Seg_11834" s="T528">Es sitz, viel Kleider…</ta>
            <ta e="T539" id="Seg_11835" s="T533">Sieben Straßen…</ta>
            <ta e="T546" id="Seg_11836" s="T539">Siebzehn Kleider, aber nichts um sie festzuziehen [sie=ohne Schnallen].</ta>
            <ta e="T548" id="Seg_11837" s="T546">Das ist Kohl.</ta>
            <ta e="T554" id="Seg_11838" s="T549">Ein Mädchen sitzt in der Erde, sie ist rot.</ta>
            <ta e="T557" id="Seg_11839" s="T554">Und ihr Haar ist im Freien.</ta>
            <ta e="T559" id="Seg_11840" s="T557">Das ist eine Karotte.</ta>
            <ta e="T567" id="Seg_11841" s="T560">Ich fand eine Peitsche, abe kann nicht (gehen?).</ta>
            <ta e="T570" id="Seg_11842" s="T567">Was ist das?</ta>
            <ta e="T571" id="Seg_11843" s="T570">Eine Schlange.</ta>
            <ta e="T581" id="Seg_11844" s="T572">Ein Mädchen kommt, ihr Haar ist (?).</ta>
            <ta e="T585" id="Seg_11845" s="T582">Es ist eine Elster.</ta>
            <ta e="T598" id="Seg_11846" s="T586">Zwei stehen, zwei liegen, die Fünfte geht, die Siebte schließt sich.</ta>
            <ta e="T601" id="Seg_11847" s="T598">Das ist eine Tür.</ta>
            <ta e="T611" id="Seg_11848" s="T602">Viele Schafe gehen, und der Schäfer [hat] Hörner.</ta>
            <ta e="T615" id="Seg_11849" s="T611">Das sind Mond und Sterne.</ta>
            <ta e="T624" id="Seg_11850" s="T616">Zwei (Löcher?) und zwei Spitzen, und unmitten ist ein Knopf.</ta>
            <ta e="T625" id="Seg_11851" s="T624">Schere.</ta>
            <ta e="T635" id="Seg_11852" s="T626">Ich esse immer, aber kann nicht koten – Eine Tüte.</ta>
            <ta e="T639" id="Seg_11853" s="T636">Ein Hahn sitzt am Zaun.</ta>
            <ta e="T644" id="Seg_11854" s="T639">[Sein] Schwanz hängt am Boden, und er ruft zu Gott.</ta>
            <ta e="T646" id="Seg_11855" s="T644">Es ist eine Glocke.</ta>
            <ta e="T656" id="Seg_11856" s="T647">Es steht ein Junge, drinnen kocht er, und selbst pisst er.</ta>
            <ta e="T658" id="Seg_11857" s="T656">Das ist ein Samovar.</ta>
            <ta e="T661" id="Seg_11858" s="T659">Zwei [Männer] gehen.</ta>
            <ta e="T665" id="Seg_11859" s="T661">Einer geht, der andere holt auf.</ta>
            <ta e="T667" id="Seg_11860" s="T665">Es sind Beine.</ta>
            <ta e="T676" id="Seg_11861" s="T668">Verprügel mich, schlag mich, (?) mich, ich habe…</ta>
            <ta e="T678" id="Seg_11862" s="T676">Das ist eine Nuß.</ta>
            <ta e="T687" id="Seg_11863" s="T679">Es hängt, und Leute nehmen es, wäscht und wischt sich.</ta>
            <ta e="T690" id="Seg_11864" s="T687">Das ist ein Handtuch.</ta>
            <ta e="T696" id="Seg_11865" s="T691">Goldenes Gras wächst um [einem] Baum.</ta>
            <ta e="T698" id="Seg_11866" s="T696">Es ist ein Finger.</ta>
            <ta e="T706" id="Seg_11867" s="T699">[Wo] eine rote Ziege lag, dort wächst kein Gras.</ta>
            <ta e="T708" id="Seg_11868" s="T706">Es ist Feuer.</ta>
            <ta e="T721" id="Seg_11869" s="T709">[Sie] ging um Beeren zu sammeln, und ich ging, und ein weiteres Mädchen ging.</ta>
            <ta e="T728" id="Seg_11870" s="T721">Wir gingen nach (?).</ta>
            <ta e="T730" id="Seg_11871" s="T728">Wir kamen dorthin.</ta>
            <ta e="T736" id="Seg_11872" s="T730">(?) Beeren, wir sahen (sie) und sammelten einige.</ta>
            <ta e="T743" id="Seg_11873" s="T736">Dann gingen wir wo das Pferd hin ging, [und] meine Mutter sagte:</ta>
            <ta e="T747" id="Seg_11874" s="T743">„Eine sehr schwarze Wolke kommt.</ta>
            <ta e="T755" id="Seg_11875" s="T747">[Wir] mussten ein Zelt aufbauen, sonst wird es sich nicht gut schlafen.“</ta>
            <ta e="T758" id="Seg_11876" s="T755">Dann bauten wir ein Zelt auf.</ta>
            <ta e="T762" id="Seg_11877" s="T758">Wir kochten [eine Mahlzeit], tranken Tee.</ta>
            <ta e="T768" id="Seg_11878" s="T762">Es war eine Matte.</ta>
            <ta e="T771" id="Seg_11879" s="T768">Wir (hingen?) die Matte an den Bäumen.</ta>
            <ta e="T774" id="Seg_11880" s="T771">Dann legten wir uns hin zum Schlafen.</ta>
            <ta e="T782" id="Seg_11881" s="T774">In der Nacht gab es einen sehr starken Regen.</ta>
            <ta e="T786" id="Seg_11882" s="T782">Am Morgen standen wir auf, wir wurden sehr nass.</ta>
            <ta e="T790" id="Seg_11883" s="T786">Wir fingen die Pferde ein und gingen nach Hause.</ta>
            <ta e="T793" id="Seg_11884" s="T790">Wir erreichten nden Fluss.</ta>
            <ta e="T796" id="Seg_11885" s="T793">Wir konnten keineswegs gehen [= ihn überqueren].</ta>
            <ta e="T805" id="Seg_11886" s="T796">Dann liefen wir dem Fluss hinab und fanden eine Brücke.</ta>
            <ta e="T811" id="Seg_11887" s="T805">Wir überquerten die Brücke und kamen nach Hause.</ta>
            <ta e="T812" id="Seg_11888" s="T811">Das war's.</ta>
            <ta e="T817" id="Seg_11889" s="T813">Heute bin ich früh aufgestanden.</ta>
            <ta e="T819" id="Seg_11890" s="T817">Ich ging zum Laden.</ta>
            <ta e="T821" id="Seg_11891" s="T819">Ich stand in einer Schlange.</ta>
            <ta e="T832" id="Seg_11892" s="T821">Dann… ich stand zwei Stunden, dann kaufte ich Fisch und kam nach Hause.</ta>
            <ta e="T835" id="Seg_11893" s="T832">Und du kamst.</ta>
            <ta e="T845" id="Seg_11894" s="T836">Ich kaufte ein Kilogram, gab [zahlte] einen Rubel.</ta>
            <ta e="T848" id="Seg_11895" s="T845">Ich gab vierzig [Kopecken]</ta>
            <ta e="T852" id="Seg_11896" s="T848">Ein Rubel vierzig.</ta>
            <ta e="T858" id="Seg_11897" s="T853">Frauen schimpfen, schreien hier.</ta>
            <ta e="T861" id="Seg_11898" s="T858">Sie sagen: „Es ist wenig Fisch</ta>
            <ta e="T866" id="Seg_11899" s="T861">[Die Verkäuferin] gibt einen Kilogram [an jeden].</ta>
            <ta e="T874" id="Seg_11900" s="T866">Sie gab jeder Frau einen Kilogram.</ta>
            <ta e="T877" id="Seg_11901" s="T874">Sie sagen: „Meine [unsere] Kinder schreien.</ta>
            <ta e="T882" id="Seg_11902" s="T877">Sie müssen Fisch essen”.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T32" id="Seg_11903" s="T27">[GVY:] Note the pronounciation of kal’enkanə: the e is accented and the second a lengthened.</ta>
            <ta e="T45" id="Seg_11904" s="T36">[GVY:] galdun = kaldun (колдун). Dĭne?</ta>
            <ta e="T68" id="Seg_11905" s="T60">[GVY:] gəttə = dĭgəttə</ta>
            <ta e="T264" id="Seg_11906" s="T262">[GVY:] [korezeʔi]</ta>
            <ta e="T304" id="Seg_11907" s="T301">[GVY:] Ambi</ta>
            <ta e="T305" id="Seg_11908" s="T304">[GVY:] мураши - small ants, but here it should rather be something like midges.</ta>
            <ta e="T314" id="Seg_11909" s="T312">[GVY:] Ambi</ta>
            <ta e="T372" id="Seg_11910" s="T365">[GVY:] köteŋzeŋdə</ta>
            <ta e="T380" id="Seg_11911" s="T375">[GVY:] dʼoʔpi?</ta>
            <ta e="T385" id="Seg_11912" s="T380">[GVY:] The syntax of this sentence is unclear</ta>
            <ta e="T437" id="Seg_11913" s="T431">[GVY:] amnolaʔbəʔjə = amnolaʔbə?</ta>
            <ta e="T456" id="Seg_11914" s="T453">[GVY:] Krasnojarskəʔi = Krasnojarskəgəʔ? Or is it an adjective (plural) from Красноярский?)?</ta>
            <ta e="T464" id="Seg_11915" s="T460">[GVY:] rather a telegram.</ta>
            <ta e="T484" id="Seg_11916" s="T480">[GVY:] Or as "small as a cat".</ta>
            <ta e="T499" id="Seg_11917" s="T494">[GVY:] this intepretation is tentative.</ta>
            <ta e="T546" id="Seg_11918" s="T539">[GVY:] Or seventy?</ta>
            <ta e="T567" id="Seg_11919" s="T560">[GVY:] kanzittə = kunzittə 'take away'?</ta>
            <ta e="T598" id="Seg_11920" s="T586">[GVY:] the sixth is missing.</ta>
            <ta e="T624" id="Seg_11921" s="T616">[GVY:] sĭ</ta>
            <ta e="T635" id="Seg_11922" s="T626">[GVY:] tüʔzittə.</ta>
            <ta e="T661" id="Seg_11923" s="T659">This riddle is not in Donner's collection.</ta>
            <ta e="T676" id="Seg_11924" s="T668">[GVY:] sʼa- 'climb'? In Donner, there is a continuation: ige măna tărzəbi 'I have [something] hairy'.</ta>
            <ta e="T698" id="Seg_11925" s="T696">[GVY:] In Donner, it's a penis.</ta>
            <ta e="T728" id="Seg_11926" s="T721">[GVY:] šomi iganə?</ta>
            <ta e="T736" id="Seg_11927" s="T730">[GVY:] šaʔla(?)jə 'a few'?</ta>
            <ta e="T848" id="Seg_11928" s="T845">[GVY:] Or forty</ta>
            <ta e="T858" id="Seg_11929" s="T853">[AAV] From here on speed is irregular.</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T267" id="Seg_11930" n="sc" s="T266">
               <ts e="T267" id="Seg_11932" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_11934" n="HIAT:w" s="T266">Kuropatka</ts>
                  <nts id="Seg_11935" n="HIAT:ip">.</nts>
                  <nts id="Seg_11936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T365" id="Seg_11937" n="sc" s="T364">
               <ts e="T365" id="Seg_11939" n="HIAT:u" s="T364">
                  <ts e="T365" id="Seg_11941" n="HIAT:w" s="T364">Жопы</ts>
                  <nts id="Seg_11942" n="HIAT:ip">.</nts>
                  <nts id="Seg_11943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T885" id="Seg_11944" n="sc" s="T383">
               <ts e="T885" id="Seg_11946" n="HIAT:u" s="T383">
                  <ts e="T885" id="Seg_11948" n="HIAT:w" s="T383">Язык</ts>
                  <nts id="Seg_11949" n="HIAT:ip">.</nts>
                  <nts id="Seg_11950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T492" id="Seg_11951" n="sc" s="T491">
               <ts e="T492" id="Seg_11953" n="HIAT:u" s="T491">
                  <nts id="Seg_11954" n="HIAT:ip">(</nts>
                  <ts e="T492" id="Seg_11956" n="HIAT:w" s="T491">Дуга</ts>
                  <nts id="Seg_11957" n="HIAT:ip">)</nts>
                  <nts id="Seg_11958" n="HIAT:ip">.</nts>
                  <nts id="Seg_11959" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T537" id="Seg_11960" n="sc" s="T536">
               <ts e="T537" id="Seg_11962" n="HIAT:u" s="T536">
                  <ts e="T537" id="Seg_11964" n="HIAT:w" s="T536">Oldʼa</ts>
                  <nts id="Seg_11965" n="HIAT:ip">.</nts>
                  <nts id="Seg_11966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T609" id="Seg_11967" n="sc" s="T608">
               <ts e="T609" id="Seg_11969" n="HIAT:u" s="T608">
                  <ts e="T887" id="Seg_11971" n="HIAT:w" s="T608">Am-</ts>
                  <nts id="Seg_11972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_11973" n="HIAT:ip">(</nts>
                  <ts e="T609" id="Seg_11975" n="HIAT:w" s="T887">nu-</ts>
                  <nts id="Seg_11976" n="HIAT:ip">)</nts>
                  <nts id="Seg_11977" n="HIAT:ip">.</nts>
                  <nts id="Seg_11978" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T267" id="Seg_11979" n="sc" s="T266">
               <ts e="T267" id="Seg_11981" n="e" s="T266">Kuropatka. </ts>
            </ts>
            <ts e="T365" id="Seg_11982" n="sc" s="T364">
               <ts e="T365" id="Seg_11984" n="e" s="T364">Жопы. </ts>
            </ts>
            <ts e="T885" id="Seg_11985" n="sc" s="T383">
               <ts e="T885" id="Seg_11987" n="e" s="T383">Язык. </ts>
            </ts>
            <ts e="T492" id="Seg_11988" n="sc" s="T491">
               <ts e="T492" id="Seg_11990" n="e" s="T491">(Дуга). </ts>
            </ts>
            <ts e="T537" id="Seg_11991" n="sc" s="T536">
               <ts e="T537" id="Seg_11993" n="e" s="T536">Oldʼa. </ts>
            </ts>
            <ts e="T609" id="Seg_11994" n="sc" s="T608">
               <ts e="T887" id="Seg_11996" n="e" s="T608">Am- </ts>
               <ts e="T609" id="Seg_11998" n="e" s="T887">(nu-). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T267" id="Seg_11999" s="T266">PKZ_196X_SU0223.KA.001 (054)</ta>
            <ta e="T365" id="Seg_12000" s="T364">PKZ_196X_SU0223.KA.002 (085)</ta>
            <ta e="T885" id="Seg_12001" s="T383">PKZ_196X_SU0223.KA.003 (091)</ta>
            <ta e="T492" id="Seg_12002" s="T491">PKZ_196X_SU0223.KA.004 (123)</ta>
            <ta e="T537" id="Seg_12003" s="T536">PKZ_196X_SU0223.KA.005 (139)</ta>
            <ta e="T609" id="Seg_12004" s="T608">PKZ_196X_SU0223.KA.006 (159)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T267" id="Seg_12005" s="T266">Kuropatka. </ta>
            <ta e="T365" id="Seg_12006" s="T364">Жопы. </ta>
            <ta e="T885" id="Seg_12007" s="T383">Язык. </ta>
            <ta e="T492" id="Seg_12008" s="T491">(Дуга). </ta>
            <ta e="T537" id="Seg_12009" s="T536">Oldʼa. </ta>
            <ta e="T609" id="Seg_12010" s="T608">Am- (nu-). </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA">
            <ta e="T885" id="Seg_12011" s="T383">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KA">
            <ta e="T267" id="Seg_12012" s="T266">Куропатка…</ta>
            <ta e="T885" id="Seg_12013" s="T383">Язык. </ta>
            <ta e="T537" id="Seg_12014" s="T536">Одежда.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-KA">
            <ta e="T885" id="Seg_12015" s="T383">The tongue.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KA">
            <ta e="T885" id="Seg_12016" s="T383">Die Zunge.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KA" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T384" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T884" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T883" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T885" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T891" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T886" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T887" />
            <conversion-tli id="T609" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T888" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T889" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T890" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
