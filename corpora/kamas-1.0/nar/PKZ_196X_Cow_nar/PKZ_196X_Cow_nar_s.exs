<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDE7425C78-1924-EDC9-AA22-12AA05934501">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Cow_nar.wav" />
         <referenced-file url="PKZ_196X_Cow_nar.mp3" />
         <referenced-file url="PKZ_196X_Cow_nar.wav" />
         <referenced-file url="PKZ_196X_Cow_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\nar\PKZ_196X_Cow_nar\PKZ_196X_Cow_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">65</ud-information>
            <ud-information attribute-name="# HIAT:w">45</ud-information>
            <ud-information attribute-name="# e">45</ud-information>
            <ud-information attribute-name="# HIAT:u">10</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.782" type="appl" />
         <tli id="T2" time="1.564" type="appl" />
         <tli id="T3" time="2.346" type="appl" />
         <tli id="T4" time="3.128" type="appl" />
         <tli id="T5" time="3.91" type="appl" />
         <tli id="T6" time="4.692" type="appl" />
         <tli id="T7" time="5.487332541195086" />
         <tli id="T8" time="7.139" type="appl" />
         <tli id="T9" time="8.804" type="appl" />
         <tli id="T10" time="10.469" type="appl" />
         <tli id="T11" time="12.134" type="appl" />
         <tli id="T12" time="13.818998518328701" />
         <tli id="T13" time="16.005" type="appl" />
         <tli id="T14" time="18.21" type="appl" />
         <tli id="T15" time="20.416" type="appl" />
         <tli id="T16" time="22.622" type="appl" />
         <tli id="T17" time="24.827" type="appl" />
         <tli id="T18" time="27.052998474266843" />
         <tli id="T19" time="27.93" type="appl" />
         <tli id="T20" time="28.826" type="appl" />
         <tli id="T21" time="29.723" type="appl" />
         <tli id="T22" time="30.619" type="appl" />
         <tli id="T23" time="31.516" type="appl" />
         <tli id="T24" time="32.412" type="appl" />
         <tli id="T25" time="33.309" type="appl" />
         <tli id="T26" time="34.091" type="appl" />
         <tli id="T27" time="34.874" type="appl" />
         <tli id="T28" time="36.08457007441927" />
         <tli id="T29" time="36.897" type="appl" />
         <tli id="T30" time="37.729" type="appl" />
         <tli id="T31" time="38.82441088369071" />
         <tli id="T32" time="40.127" type="appl" />
         <tli id="T33" time="41.403" type="appl" />
         <tli id="T34" time="42.68" type="appl" />
         <tli id="T35" time="43.229" type="appl" />
         <tli id="T36" time="43.779" type="appl" />
         <tli id="T37" time="44.328" type="appl" />
         <tli id="T38" time="44.877" type="appl" />
         <tli id="T39" time="45.427" type="appl" />
         <tli id="T40" time="45.976" type="appl" />
         <tli id="T41" time="46.766" type="appl" />
         <tli id="T42" time="47.556" type="appl" />
         <tli id="T43" time="48.935" type="appl" />
         <tli id="T44" time="50.21708227796053" />
         <tli id="T45" time="51.297" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T45" id="Seg_0" n="sc" s="T0">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Miʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">tüžöjbeʔ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">i</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">susʼedən</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">tüžöjdə</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">dĭn</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">mĭmbiʔi</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Dĭgəttə</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_30" n="HIAT:ip">(</nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">am-</ts>
                  <nts id="Seg_33" n="HIAT:ip">)</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">onʼiʔ</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">onʼiʔdə</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">mürerleʔbəbi</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_46" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">Dĭgəttə</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">dĭn</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">tüžöj</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">üjüttə</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">amnut</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">embi</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_67" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">Tibizeŋ</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">šobiʔi:</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">nada</ts>
                  <nts id="Seg_76" n="HIAT:ip">,</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">măndə</ts>
                  <nts id="Seg_80" n="HIAT:ip">,</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">băʔsittə</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">dĭ</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">amnut</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_93" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">Măn</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">măndəm:</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_100" n="HIAT:ip">"</nts>
                  <ts e="T28" id="Seg_102" n="HIAT:w" s="T27">Kăde</ts>
                  <nts id="Seg_103" n="HIAT:ip">?</nts>
                  <nts id="Seg_104" n="HIAT:ip">"</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_107" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">Dĭ</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">dĭbər</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">băʔluʔpi</ts>
                  <nts id="Seg_116" n="HIAT:ip">.</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_119" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_121" n="HIAT:w" s="T31">Dĭgəttə</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">dĭzeŋ</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">mămbiʔi:</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_129" n="HIAT:ip">"</nts>
                  <ts e="T35" id="Seg_131" n="HIAT:w" s="T34">Šənap</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_134" n="HIAT:w" s="T35">nada</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_136" n="HIAT:ip">(</nts>
                  <ts e="T37" id="Seg_138" n="HIAT:w" s="T36">dĭs-</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_141" n="HIAT:w" s="T37">dĭ-</ts>
                  <nts id="Seg_142" n="HIAT:ip">)</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">dăre</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_148" n="HIAT:w" s="T39">azittə</ts>
                  <nts id="Seg_149" n="HIAT:ip">"</nts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_153" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_155" n="HIAT:w" s="T40">Dĭgəttə</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_158" n="HIAT:w" s="T41">abiʔi</ts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_162" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">Üjübə</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_167" n="HIAT:w" s="T43">supsoluʔpiʔi</ts>
                  <nts id="Seg_168" n="HIAT:ip">.</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_171" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_173" n="HIAT:w" s="T44">Kabarləj</ts>
                  <nts id="Seg_174" n="HIAT:ip">.</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T45" id="Seg_176" n="sc" s="T0">
               <ts e="T1" id="Seg_178" n="e" s="T0">Miʔ </ts>
               <ts e="T2" id="Seg_180" n="e" s="T1">tüžöjbeʔ </ts>
               <ts e="T3" id="Seg_182" n="e" s="T2">i </ts>
               <ts e="T4" id="Seg_184" n="e" s="T3">susʼedən </ts>
               <ts e="T5" id="Seg_186" n="e" s="T4">tüžöjdə </ts>
               <ts e="T6" id="Seg_188" n="e" s="T5">dĭn </ts>
               <ts e="T7" id="Seg_190" n="e" s="T6">mĭmbiʔi. </ts>
               <ts e="T8" id="Seg_192" n="e" s="T7">Dĭgəttə </ts>
               <ts e="T9" id="Seg_194" n="e" s="T8">(am-) </ts>
               <ts e="T10" id="Seg_196" n="e" s="T9">onʼiʔ </ts>
               <ts e="T11" id="Seg_198" n="e" s="T10">onʼiʔdə </ts>
               <ts e="T12" id="Seg_200" n="e" s="T11">mürerleʔbəbi. </ts>
               <ts e="T13" id="Seg_202" n="e" s="T12">Dĭgəttə </ts>
               <ts e="T14" id="Seg_204" n="e" s="T13">dĭn </ts>
               <ts e="T15" id="Seg_206" n="e" s="T14">tüžöj </ts>
               <ts e="T16" id="Seg_208" n="e" s="T15">üjüttə </ts>
               <ts e="T17" id="Seg_210" n="e" s="T16">amnut </ts>
               <ts e="T18" id="Seg_212" n="e" s="T17">embi. </ts>
               <ts e="T19" id="Seg_214" n="e" s="T18">Tibizeŋ </ts>
               <ts e="T20" id="Seg_216" n="e" s="T19">šobiʔi: </ts>
               <ts e="T21" id="Seg_218" n="e" s="T20">nada, </ts>
               <ts e="T22" id="Seg_220" n="e" s="T21">măndə, </ts>
               <ts e="T23" id="Seg_222" n="e" s="T22">băʔsittə </ts>
               <ts e="T24" id="Seg_224" n="e" s="T23">dĭ </ts>
               <ts e="T25" id="Seg_226" n="e" s="T24">amnut. </ts>
               <ts e="T26" id="Seg_228" n="e" s="T25">Măn </ts>
               <ts e="T27" id="Seg_230" n="e" s="T26">măndəm: </ts>
               <ts e="T28" id="Seg_232" n="e" s="T27">"Kăde?" </ts>
               <ts e="T29" id="Seg_234" n="e" s="T28">Dĭ </ts>
               <ts e="T30" id="Seg_236" n="e" s="T29">dĭbər </ts>
               <ts e="T31" id="Seg_238" n="e" s="T30">băʔluʔpi. </ts>
               <ts e="T32" id="Seg_240" n="e" s="T31">Dĭgəttə </ts>
               <ts e="T33" id="Seg_242" n="e" s="T32">dĭzeŋ </ts>
               <ts e="T34" id="Seg_244" n="e" s="T33">mămbiʔi: </ts>
               <ts e="T35" id="Seg_246" n="e" s="T34">"Šənap </ts>
               <ts e="T36" id="Seg_248" n="e" s="T35">nada </ts>
               <ts e="T37" id="Seg_250" n="e" s="T36">(dĭs- </ts>
               <ts e="T38" id="Seg_252" n="e" s="T37">dĭ-) </ts>
               <ts e="T39" id="Seg_254" n="e" s="T38">dăre </ts>
               <ts e="T40" id="Seg_256" n="e" s="T39">azittə". </ts>
               <ts e="T41" id="Seg_258" n="e" s="T40">Dĭgəttə </ts>
               <ts e="T42" id="Seg_260" n="e" s="T41">abiʔi. </ts>
               <ts e="T43" id="Seg_262" n="e" s="T42">Üjübə </ts>
               <ts e="T44" id="Seg_264" n="e" s="T43">supsoluʔpiʔi. </ts>
               <ts e="T45" id="Seg_266" n="e" s="T44">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_267" s="T0">PKZ_196X_Cow_nar.001 (001)</ta>
            <ta e="T12" id="Seg_268" s="T7">PKZ_196X_Cow_nar.002 (003)</ta>
            <ta e="T18" id="Seg_269" s="T12">PKZ_196X_Cow_nar.003 (004)</ta>
            <ta e="T25" id="Seg_270" s="T18">PKZ_196X_Cow_nar.004 (005)</ta>
            <ta e="T28" id="Seg_271" s="T25">PKZ_196X_Cow_nar.005 (006)</ta>
            <ta e="T31" id="Seg_272" s="T28">PKZ_196X_Cow_nar.006 (007)</ta>
            <ta e="T40" id="Seg_273" s="T31">PKZ_196X_Cow_nar.007 (008) </ta>
            <ta e="T42" id="Seg_274" s="T40">PKZ_196X_Cow_nar.008 (010)</ta>
            <ta e="T44" id="Seg_275" s="T42">PKZ_196X_Cow_nar.009 (011)</ta>
            <ta e="T45" id="Seg_276" s="T44">PKZ_196X_Cow_nar.010 (012)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_277" s="T0">Miʔ tüžöjbeʔ i susʼedən tüžöjdə dĭn mĭmbiʔi. </ta>
            <ta e="T12" id="Seg_278" s="T7">Dĭgəttə (am-) onʼiʔ onʼiʔdə mürerleʔbəbi. </ta>
            <ta e="T18" id="Seg_279" s="T12">Dĭgəttə dĭn tüžöj üjüttə amnut embi. </ta>
            <ta e="T25" id="Seg_280" s="T18">Tibizeŋ šobiʔi: nada, măndə, băʔsittə dĭ amnut. </ta>
            <ta e="T28" id="Seg_281" s="T25">Măn măndəm: "Kăde?" </ta>
            <ta e="T31" id="Seg_282" s="T28">Dĭ dĭbər băʔluʔpi. </ta>
            <ta e="T40" id="Seg_283" s="T31">Dĭgəttə dĭzeŋ mămbiʔi: "Šənap nada (dĭs- dĭ-) dăre azittə". </ta>
            <ta e="T42" id="Seg_284" s="T40">Dĭgəttə abiʔi. </ta>
            <ta e="T44" id="Seg_285" s="T42">Üjübə supsoluʔpiʔi. </ta>
            <ta e="T45" id="Seg_286" s="T44">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_287" s="T0">miʔ</ta>
            <ta e="T2" id="Seg_288" s="T1">tüžöj-beʔ</ta>
            <ta e="T3" id="Seg_289" s="T2">i</ta>
            <ta e="T4" id="Seg_290" s="T3">susʼed-ə-n</ta>
            <ta e="T5" id="Seg_291" s="T4">tüžöj-də</ta>
            <ta e="T6" id="Seg_292" s="T5">dĭn</ta>
            <ta e="T7" id="Seg_293" s="T6">mĭm-bi-ʔi</ta>
            <ta e="T8" id="Seg_294" s="T7">dĭgəttə</ta>
            <ta e="T10" id="Seg_295" s="T9">onʼiʔ</ta>
            <ta e="T11" id="Seg_296" s="T10">onʼiʔ-də</ta>
            <ta e="T12" id="Seg_297" s="T11">müre-r-leʔbə-bi</ta>
            <ta e="T13" id="Seg_298" s="T12">dĭgəttə</ta>
            <ta e="T14" id="Seg_299" s="T13">dĭ-n</ta>
            <ta e="T15" id="Seg_300" s="T14">tüžöj</ta>
            <ta e="T16" id="Seg_301" s="T15">üjü-ttə</ta>
            <ta e="T17" id="Seg_302" s="T16">amnu-t</ta>
            <ta e="T18" id="Seg_303" s="T17">em-bi</ta>
            <ta e="T19" id="Seg_304" s="T18">tibi-zeŋ</ta>
            <ta e="T20" id="Seg_305" s="T19">šo-bi-ʔi</ta>
            <ta e="T21" id="Seg_306" s="T20">nada</ta>
            <ta e="T22" id="Seg_307" s="T21">măn-də</ta>
            <ta e="T23" id="Seg_308" s="T22">băʔ-sittə</ta>
            <ta e="T24" id="Seg_309" s="T23">dĭ</ta>
            <ta e="T25" id="Seg_310" s="T24">amnu-t</ta>
            <ta e="T26" id="Seg_311" s="T25">măn</ta>
            <ta e="T27" id="Seg_312" s="T26">măn-də-m</ta>
            <ta e="T28" id="Seg_313" s="T27">kăde</ta>
            <ta e="T29" id="Seg_314" s="T28">dĭ</ta>
            <ta e="T30" id="Seg_315" s="T29">dĭbər</ta>
            <ta e="T31" id="Seg_316" s="T30">băʔ-luʔ-pi</ta>
            <ta e="T32" id="Seg_317" s="T31">dĭgəttə</ta>
            <ta e="T33" id="Seg_318" s="T32">dĭ-zeŋ</ta>
            <ta e="T34" id="Seg_319" s="T33">măm-bi-ʔi</ta>
            <ta e="T35" id="Seg_320" s="T34">šənap</ta>
            <ta e="T36" id="Seg_321" s="T35">nada</ta>
            <ta e="T39" id="Seg_322" s="T38">dăre</ta>
            <ta e="T40" id="Seg_323" s="T39">a-zittə</ta>
            <ta e="T41" id="Seg_324" s="T40">dĭgəttə</ta>
            <ta e="T42" id="Seg_325" s="T41">a-bi-ʔi</ta>
            <ta e="T43" id="Seg_326" s="T42">üjü-bə</ta>
            <ta e="T44" id="Seg_327" s="T43">supso-luʔ-pi-ʔi</ta>
            <ta e="T45" id="Seg_328" s="T44">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_329" s="T0">miʔ</ta>
            <ta e="T2" id="Seg_330" s="T1">tüžöj-bAʔ</ta>
            <ta e="T3" id="Seg_331" s="T2">i</ta>
            <ta e="T4" id="Seg_332" s="T3">susʼed-ə-n</ta>
            <ta e="T5" id="Seg_333" s="T4">tüžöj-də</ta>
            <ta e="T6" id="Seg_334" s="T5">dĭn</ta>
            <ta e="T7" id="Seg_335" s="T6">mĭn-bi-jəʔ</ta>
            <ta e="T8" id="Seg_336" s="T7">dĭgəttə</ta>
            <ta e="T10" id="Seg_337" s="T9">onʼiʔ</ta>
            <ta e="T11" id="Seg_338" s="T10">onʼiʔ-Tə</ta>
            <ta e="T12" id="Seg_339" s="T11">müre-r-laʔbə-bi</ta>
            <ta e="T13" id="Seg_340" s="T12">dĭgəttə</ta>
            <ta e="T14" id="Seg_341" s="T13">dĭ-n</ta>
            <ta e="T15" id="Seg_342" s="T14">tüžöj</ta>
            <ta e="T16" id="Seg_343" s="T15">üjü-ttə</ta>
            <ta e="T17" id="Seg_344" s="T16">amnu-t</ta>
            <ta e="T18" id="Seg_345" s="T17">hen-bi</ta>
            <ta e="T19" id="Seg_346" s="T18">tibi-zAŋ</ta>
            <ta e="T20" id="Seg_347" s="T19">šo-bi-jəʔ</ta>
            <ta e="T21" id="Seg_348" s="T20">nadə</ta>
            <ta e="T22" id="Seg_349" s="T21">măn-ntə</ta>
            <ta e="T23" id="Seg_350" s="T22">băt-zittə</ta>
            <ta e="T24" id="Seg_351" s="T23">dĭ</ta>
            <ta e="T25" id="Seg_352" s="T24">amnu-t</ta>
            <ta e="T26" id="Seg_353" s="T25">măn</ta>
            <ta e="T27" id="Seg_354" s="T26">măn-ntə-m</ta>
            <ta e="T28" id="Seg_355" s="T27">kădaʔ</ta>
            <ta e="T29" id="Seg_356" s="T28">dĭ</ta>
            <ta e="T30" id="Seg_357" s="T29">dĭbər</ta>
            <ta e="T31" id="Seg_358" s="T30">băt-luʔbdə-bi</ta>
            <ta e="T32" id="Seg_359" s="T31">dĭgəttə</ta>
            <ta e="T33" id="Seg_360" s="T32">dĭ-zAŋ</ta>
            <ta e="T34" id="Seg_361" s="T33">măn-bi-jəʔ</ta>
            <ta e="T35" id="Seg_362" s="T34">šenap</ta>
            <ta e="T36" id="Seg_363" s="T35">nadə</ta>
            <ta e="T39" id="Seg_364" s="T38">dărəʔ</ta>
            <ta e="T40" id="Seg_365" s="T39">a-zittə</ta>
            <ta e="T41" id="Seg_366" s="T40">dĭgəttə</ta>
            <ta e="T42" id="Seg_367" s="T41">a-bi-jəʔ</ta>
            <ta e="T43" id="Seg_368" s="T42">üjü-bə</ta>
            <ta e="T44" id="Seg_369" s="T43">supso-luʔbdə-bi-jəʔ</ta>
            <ta e="T45" id="Seg_370" s="T44">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_371" s="T0">we.NOM</ta>
            <ta e="T2" id="Seg_372" s="T1">cow-NOM/GEN/ACC.1PL</ta>
            <ta e="T3" id="Seg_373" s="T2">and</ta>
            <ta e="T4" id="Seg_374" s="T3">neighbour-EP-GEN</ta>
            <ta e="T5" id="Seg_375" s="T4">cow-NOM/GEN/ACC.3SG</ta>
            <ta e="T6" id="Seg_376" s="T5">there</ta>
            <ta e="T7" id="Seg_377" s="T6">go-PST-3PL</ta>
            <ta e="T8" id="Seg_378" s="T7">then</ta>
            <ta e="T10" id="Seg_379" s="T9">one.[NOM.SG]</ta>
            <ta e="T11" id="Seg_380" s="T10">one-LAT</ta>
            <ta e="T12" id="Seg_381" s="T11">bellow-FRQ-DUR-PST.[3SG]</ta>
            <ta e="T13" id="Seg_382" s="T12">then</ta>
            <ta e="T14" id="Seg_383" s="T13">this-GEN</ta>
            <ta e="T15" id="Seg_384" s="T14">cow.[NOM.SG]</ta>
            <ta e="T16" id="Seg_385" s="T15">foot-ABL.3SG</ta>
            <ta e="T17" id="Seg_386" s="T16">horn-NOM/GEN.3SG</ta>
            <ta e="T18" id="Seg_387" s="T17">put-PST.[3SG]</ta>
            <ta e="T19" id="Seg_388" s="T18">man-PL</ta>
            <ta e="T20" id="Seg_389" s="T19">come-PST-3PL</ta>
            <ta e="T21" id="Seg_390" s="T20">one.should</ta>
            <ta e="T22" id="Seg_391" s="T21">say-IPFVZ.[3SG]</ta>
            <ta e="T23" id="Seg_392" s="T22">cut-INF.LAT</ta>
            <ta e="T24" id="Seg_393" s="T23">this.[NOM.SG]</ta>
            <ta e="T25" id="Seg_394" s="T24">horn-NOM/GEN.3SG</ta>
            <ta e="T26" id="Seg_395" s="T25">I.NOM</ta>
            <ta e="T27" id="Seg_396" s="T26">say-IPFVZ-1SG</ta>
            <ta e="T28" id="Seg_397" s="T27">how</ta>
            <ta e="T29" id="Seg_398" s="T28">this.[NOM.SG]</ta>
            <ta e="T30" id="Seg_399" s="T29">there</ta>
            <ta e="T31" id="Seg_400" s="T30">cut-MOM-PST.[3SG]</ta>
            <ta e="T32" id="Seg_401" s="T31">then</ta>
            <ta e="T33" id="Seg_402" s="T32">this-PL</ta>
            <ta e="T34" id="Seg_403" s="T33">say-PST-3PL</ta>
            <ta e="T35" id="Seg_404" s="T34">true</ta>
            <ta e="T36" id="Seg_405" s="T35">one.should</ta>
            <ta e="T39" id="Seg_406" s="T38">so</ta>
            <ta e="T40" id="Seg_407" s="T39">make-INF.LAT</ta>
            <ta e="T41" id="Seg_408" s="T40">then</ta>
            <ta e="T42" id="Seg_409" s="T41">make-PST-3PL</ta>
            <ta e="T43" id="Seg_410" s="T42">foot-ACC.3SG</ta>
            <ta e="T44" id="Seg_411" s="T43">depart-MOM-PST-3PL</ta>
            <ta e="T45" id="Seg_412" s="T44">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_413" s="T0">мы.NOM</ta>
            <ta e="T2" id="Seg_414" s="T1">корова-NOM/GEN/ACC.1PL</ta>
            <ta e="T3" id="Seg_415" s="T2">и</ta>
            <ta e="T4" id="Seg_416" s="T3">сосед-EP-GEN</ta>
            <ta e="T5" id="Seg_417" s="T4">корова-NOM/GEN/ACC.3SG</ta>
            <ta e="T6" id="Seg_418" s="T5">там</ta>
            <ta e="T7" id="Seg_419" s="T6">идти-PST-3PL</ta>
            <ta e="T8" id="Seg_420" s="T7">тогда</ta>
            <ta e="T10" id="Seg_421" s="T9">один.[NOM.SG]</ta>
            <ta e="T11" id="Seg_422" s="T10">один-LAT</ta>
            <ta e="T12" id="Seg_423" s="T11">мычать-FRQ-DUR-PST.[3SG]</ta>
            <ta e="T13" id="Seg_424" s="T12">тогда</ta>
            <ta e="T14" id="Seg_425" s="T13">этот-GEN</ta>
            <ta e="T15" id="Seg_426" s="T14">корова.[NOM.SG]</ta>
            <ta e="T16" id="Seg_427" s="T15">нога-ABL.3SG</ta>
            <ta e="T17" id="Seg_428" s="T16">рог-NOM/GEN.3SG</ta>
            <ta e="T18" id="Seg_429" s="T17">класть-PST.[3SG]</ta>
            <ta e="T19" id="Seg_430" s="T18">мужчина-PL</ta>
            <ta e="T20" id="Seg_431" s="T19">прийти-PST-3PL</ta>
            <ta e="T21" id="Seg_432" s="T20">надо</ta>
            <ta e="T22" id="Seg_433" s="T21">сказать-IPFVZ.[3SG]</ta>
            <ta e="T23" id="Seg_434" s="T22">резать-INF.LAT</ta>
            <ta e="T24" id="Seg_435" s="T23">этот.[NOM.SG]</ta>
            <ta e="T25" id="Seg_436" s="T24">рог-NOM/GEN.3SG</ta>
            <ta e="T26" id="Seg_437" s="T25">я.NOM</ta>
            <ta e="T27" id="Seg_438" s="T26">сказать-IPFVZ-1SG</ta>
            <ta e="T28" id="Seg_439" s="T27">как</ta>
            <ta e="T29" id="Seg_440" s="T28">этот.[NOM.SG]</ta>
            <ta e="T30" id="Seg_441" s="T29">там</ta>
            <ta e="T31" id="Seg_442" s="T30">резать-MOM-PST.[3SG]</ta>
            <ta e="T32" id="Seg_443" s="T31">тогда</ta>
            <ta e="T33" id="Seg_444" s="T32">этот-PL</ta>
            <ta e="T34" id="Seg_445" s="T33">сказать-PST-3PL</ta>
            <ta e="T35" id="Seg_446" s="T34">верный</ta>
            <ta e="T36" id="Seg_447" s="T35">надо</ta>
            <ta e="T39" id="Seg_448" s="T38">так</ta>
            <ta e="T40" id="Seg_449" s="T39">делать-INF.LAT</ta>
            <ta e="T41" id="Seg_450" s="T40">тогда</ta>
            <ta e="T42" id="Seg_451" s="T41">делать-PST-3PL</ta>
            <ta e="T43" id="Seg_452" s="T42">нога-ACC.3SG</ta>
            <ta e="T44" id="Seg_453" s="T43">уйти-MOM-PST-3PL</ta>
            <ta e="T45" id="Seg_454" s="T44">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_455" s="T0">pers</ta>
            <ta e="T2" id="Seg_456" s="T1">n-n:case.poss</ta>
            <ta e="T3" id="Seg_457" s="T2">conj</ta>
            <ta e="T4" id="Seg_458" s="T3">n-n:ins-n:case</ta>
            <ta e="T5" id="Seg_459" s="T4">n-n:case.poss</ta>
            <ta e="T6" id="Seg_460" s="T5">adv</ta>
            <ta e="T7" id="Seg_461" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_462" s="T7">adv</ta>
            <ta e="T10" id="Seg_463" s="T9">num-n:case</ta>
            <ta e="T11" id="Seg_464" s="T10">num-n:case</ta>
            <ta e="T12" id="Seg_465" s="T11">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_466" s="T12">adv</ta>
            <ta e="T14" id="Seg_467" s="T13">dempro-n:case</ta>
            <ta e="T15" id="Seg_468" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_469" s="T15">n-n:case.poss</ta>
            <ta e="T17" id="Seg_470" s="T16">n-n:case.poss</ta>
            <ta e="T18" id="Seg_471" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_472" s="T18">n-n:num</ta>
            <ta e="T20" id="Seg_473" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_474" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_475" s="T21">v-v&gt;v-v:pn</ta>
            <ta e="T23" id="Seg_476" s="T22">v-v:n.fin</ta>
            <ta e="T24" id="Seg_477" s="T23">dempro-n:case</ta>
            <ta e="T25" id="Seg_478" s="T24">n-n:case.poss</ta>
            <ta e="T26" id="Seg_479" s="T25">pers</ta>
            <ta e="T27" id="Seg_480" s="T26">v-v&gt;v-v:pn</ta>
            <ta e="T28" id="Seg_481" s="T27">que</ta>
            <ta e="T29" id="Seg_482" s="T28">dempro-n:case</ta>
            <ta e="T30" id="Seg_483" s="T29">adv</ta>
            <ta e="T31" id="Seg_484" s="T30">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_485" s="T31">adv</ta>
            <ta e="T33" id="Seg_486" s="T32">dempro-n:num</ta>
            <ta e="T34" id="Seg_487" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_488" s="T34">adj</ta>
            <ta e="T36" id="Seg_489" s="T35">ptcl</ta>
            <ta e="T39" id="Seg_490" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_491" s="T39">v-v:n.fin</ta>
            <ta e="T41" id="Seg_492" s="T40">adv</ta>
            <ta e="T42" id="Seg_493" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_494" s="T42">n-n:case.poss</ta>
            <ta e="T44" id="Seg_495" s="T43">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_496" s="T44">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_497" s="T0">pers</ta>
            <ta e="T2" id="Seg_498" s="T1">n</ta>
            <ta e="T3" id="Seg_499" s="T2">conj</ta>
            <ta e="T4" id="Seg_500" s="T3">n</ta>
            <ta e="T5" id="Seg_501" s="T4">n</ta>
            <ta e="T6" id="Seg_502" s="T5">adv</ta>
            <ta e="T7" id="Seg_503" s="T6">v</ta>
            <ta e="T8" id="Seg_504" s="T7">adv</ta>
            <ta e="T10" id="Seg_505" s="T9">num</ta>
            <ta e="T11" id="Seg_506" s="T10">num</ta>
            <ta e="T12" id="Seg_507" s="T11">v</ta>
            <ta e="T13" id="Seg_508" s="T12">adv</ta>
            <ta e="T14" id="Seg_509" s="T13">dempro</ta>
            <ta e="T15" id="Seg_510" s="T14">n</ta>
            <ta e="T16" id="Seg_511" s="T15">n</ta>
            <ta e="T17" id="Seg_512" s="T16">n</ta>
            <ta e="T18" id="Seg_513" s="T17">v</ta>
            <ta e="T19" id="Seg_514" s="T18">n</ta>
            <ta e="T20" id="Seg_515" s="T19">v</ta>
            <ta e="T21" id="Seg_516" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_517" s="T21">v</ta>
            <ta e="T23" id="Seg_518" s="T22">v</ta>
            <ta e="T24" id="Seg_519" s="T23">dempro</ta>
            <ta e="T25" id="Seg_520" s="T24">n</ta>
            <ta e="T26" id="Seg_521" s="T25">pers</ta>
            <ta e="T27" id="Seg_522" s="T26">v</ta>
            <ta e="T28" id="Seg_523" s="T27">que</ta>
            <ta e="T29" id="Seg_524" s="T28">dempro</ta>
            <ta e="T30" id="Seg_525" s="T29">adv</ta>
            <ta e="T31" id="Seg_526" s="T30">v</ta>
            <ta e="T32" id="Seg_527" s="T31">adv</ta>
            <ta e="T33" id="Seg_528" s="T32">dempro</ta>
            <ta e="T34" id="Seg_529" s="T33">v</ta>
            <ta e="T35" id="Seg_530" s="T34">adj</ta>
            <ta e="T36" id="Seg_531" s="T35">ptcl</ta>
            <ta e="T39" id="Seg_532" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_533" s="T39">v</ta>
            <ta e="T41" id="Seg_534" s="T40">adv</ta>
            <ta e="T42" id="Seg_535" s="T41">v</ta>
            <ta e="T43" id="Seg_536" s="T42">n</ta>
            <ta e="T44" id="Seg_537" s="T43">v</ta>
            <ta e="T45" id="Seg_538" s="T44">ptcl</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_539" s="T1">np:S</ta>
            <ta e="T5" id="Seg_540" s="T4">np:S</ta>
            <ta e="T7" id="Seg_541" s="T6">v:pred</ta>
            <ta e="T12" id="Seg_542" s="T11">0.3:S v:pred</ta>
            <ta e="T17" id="Seg_543" s="T16">np:S</ta>
            <ta e="T18" id="Seg_544" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_545" s="T18">np.h:S</ta>
            <ta e="T20" id="Seg_546" s="T19">v:pred</ta>
            <ta e="T22" id="Seg_547" s="T21">0.3.h:S v:pred</ta>
            <ta e="T26" id="Seg_548" s="T25">pro.h:S</ta>
            <ta e="T27" id="Seg_549" s="T26">v:pred</ta>
            <ta e="T29" id="Seg_550" s="T28">pro.h:S</ta>
            <ta e="T31" id="Seg_551" s="T30">v:pred</ta>
            <ta e="T33" id="Seg_552" s="T32">pro.h:S</ta>
            <ta e="T34" id="Seg_553" s="T33">v:pred</ta>
            <ta e="T42" id="Seg_554" s="T41">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_555" s="T42">np:O</ta>
            <ta e="T44" id="Seg_556" s="T43">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_557" s="T0">pro.h:Poss</ta>
            <ta e="T2" id="Seg_558" s="T1">np:A</ta>
            <ta e="T4" id="Seg_559" s="T3">np.h:Poss</ta>
            <ta e="T5" id="Seg_560" s="T4">np:A</ta>
            <ta e="T12" id="Seg_561" s="T11">0.3:A</ta>
            <ta e="T13" id="Seg_562" s="T12">adv:Time</ta>
            <ta e="T14" id="Seg_563" s="T13">pro.h:Poss</ta>
            <ta e="T15" id="Seg_564" s="T14">np:Poss</ta>
            <ta e="T17" id="Seg_565" s="T16">np:Th</ta>
            <ta e="T19" id="Seg_566" s="T18">np.h:A</ta>
            <ta e="T22" id="Seg_567" s="T21">0.3.h:A</ta>
            <ta e="T26" id="Seg_568" s="T25">pro.h:A</ta>
            <ta e="T29" id="Seg_569" s="T28">pro.h:A</ta>
            <ta e="T30" id="Seg_570" s="T29">adv:L</ta>
            <ta e="T32" id="Seg_571" s="T31">adv:Time</ta>
            <ta e="T33" id="Seg_572" s="T32">pro.h:A</ta>
            <ta e="T42" id="Seg_573" s="T41">0.3.h:A</ta>
            <ta e="T43" id="Seg_574" s="T42">np:P</ta>
            <ta e="T44" id="Seg_575" s="T43">0.3.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_576" s="T1">TURK:cult</ta>
            <ta e="T3" id="Seg_577" s="T2">RUS:gram</ta>
            <ta e="T4" id="Seg_578" s="T3">RUS:core</ta>
            <ta e="T5" id="Seg_579" s="T4">TURK:cult</ta>
            <ta e="T15" id="Seg_580" s="T14">TURK:cult</ta>
            <ta e="T21" id="Seg_581" s="T20">RUS:mod</ta>
            <ta e="T36" id="Seg_582" s="T35">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_583" s="T0">Наша корова и соседская корова пришли сюда.</ta>
            <ta e="T12" id="Seg_584" s="T7">Одна мычит на другую.</ta>
            <ta e="T18" id="Seg_585" s="T12">Потом его корова проткнула [той корове?] рогом ногу. [?]</ta>
            <ta e="T25" id="Seg_586" s="T18">Мужчины пришли: надо, говорят, отрезать рог.</ta>
            <ta e="T28" id="Seg_587" s="T25">Я говорю: "Как?"</ta>
            <ta e="T31" id="Seg_588" s="T28">Он там отрезал.</ta>
            <ta e="T34" id="Seg_589" s="T31">Потом они говорят:</ta>
            <ta e="T40" id="Seg_590" s="T34">"Правда надо так делать".</ta>
            <ta e="T42" id="Seg_591" s="T40">[Так] они и сделали.</ta>
            <ta e="T44" id="Seg_592" s="T42">Ногу вытащили. [?]</ta>
            <ta e="T45" id="Seg_593" s="T44">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_594" s="T0">Our cow and the neighbour's cow went there.</ta>
            <ta e="T12" id="Seg_595" s="T7">Then one was bellowing to the other.</ta>
            <ta e="T18" id="Seg_596" s="T12">Then his cow pierced [the other cow's?] leg with its horn. [?]</ta>
            <ta e="T25" id="Seg_597" s="T18">The men came: one should, they say, cut the horn.</ta>
            <ta e="T28" id="Seg_598" s="T25">I say: "How?"</ta>
            <ta e="T31" id="Seg_599" s="T28">He cut it off there.</ta>
            <ta e="T34" id="Seg_600" s="T31">Then they said:</ta>
            <ta e="T40" id="Seg_601" s="T34">"Really it has to be done so."</ta>
            <ta e="T42" id="Seg_602" s="T40">Then they made [so].</ta>
            <ta e="T44" id="Seg_603" s="T42">They pulled [its?] leg out. [?]</ta>
            <ta e="T45" id="Seg_604" s="T44">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_605" s="T0">Unsere Kuh und die Nachbarskuh liefen dorthin.</ta>
            <ta e="T12" id="Seg_606" s="T7">Dann brüllte eine die andere an.</ta>
            <ta e="T18" id="Seg_607" s="T12">Dann hat seine Kuh das Bein [dieser Kuh] mit seinem Horn gestochen. [?]</ta>
            <ta e="T25" id="Seg_608" s="T18">Die Männer kamen: man muss, sagen sie, das Horn abschneiden.</ta>
            <ta e="T28" id="Seg_609" s="T25">Ich sage: "Wie?"</ta>
            <ta e="T31" id="Seg_610" s="T28">Er schnitt es dort [ab].</ta>
            <ta e="T34" id="Seg_611" s="T31">Dann sagten sie:</ta>
            <ta e="T40" id="Seg_612" s="T34">"Es muss wirklich so gemacht werden."</ta>
            <ta e="T42" id="Seg_613" s="T40">Dann machten sie es [so].</ta>
            <ta e="T44" id="Seg_614" s="T42">Dann zogen sie [ihr?] Bein heraus. [?]</ta>
            <ta e="T45" id="Seg_615" s="T44">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T12" id="Seg_616" s="T7">[GVY:] mürerleʔkəbi?</ta>
            <ta e="T44" id="Seg_617" s="T42">[KlG] PKZ might have mixed up the verb with săbəj’(i)- ‘pull out’ (&lt; săbəj’ i- ‘take’?). </ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
