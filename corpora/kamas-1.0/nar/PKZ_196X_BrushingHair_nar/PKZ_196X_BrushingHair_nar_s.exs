<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDA1CB1905-3E4A-0D25-3237-EF4AB247CA89">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_BrushingHair_nar.wav" />
         <referenced-file url="PKZ_196X_BrushingHair_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\nar\PKZ_196X_BrushingHair_nar\PKZ_196X_BrushingHair_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">14</ud-information>
            <ud-information attribute-name="# HIAT:w">10</ud-information>
            <ud-information attribute-name="# e">10</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.037" type="appl" />
         <tli id="T1" time="0.757" type="appl" />
         <tli id="T2" time="1.478" type="appl" />
         <tli id="T3" time="2.3922075321924328" />
         <tli id="T4" time="3.044" type="appl" />
         <tli id="T5" time="3.89" type="appl" />
         <tli id="T6" time="7.156632004386274" />
         <tli id="T7" time="8.192" type="appl" />
         <tli id="T8" time="9.555503067309047" />
         <tli id="T9" time="10.699" type="appl" />
         <tli id="T10" time="11.821" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T10" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Nada</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">eʔbdəm</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">kadaʔsittə</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">I</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">tĭlejzittə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">eʔbdəm</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_26" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">Măn</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">kadaʔlaʔbəm</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_35" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_37" n="HIAT:w" s="T8">Miʔ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">kadaʔlaʔbəbaʔ</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T10" id="Seg_43" n="sc" s="T0">
               <ts e="T1" id="Seg_45" n="e" s="T0">Nada </ts>
               <ts e="T2" id="Seg_47" n="e" s="T1">eʔbdəm </ts>
               <ts e="T3" id="Seg_49" n="e" s="T2">kadaʔsittə. </ts>
               <ts e="T4" id="Seg_51" n="e" s="T3">I </ts>
               <ts e="T5" id="Seg_53" n="e" s="T4">tĭlejzittə </ts>
               <ts e="T6" id="Seg_55" n="e" s="T5">eʔbdəm. </ts>
               <ts e="T7" id="Seg_57" n="e" s="T6">Măn </ts>
               <ts e="T8" id="Seg_59" n="e" s="T7">kadaʔlaʔbəm. </ts>
               <ts e="T9" id="Seg_61" n="e" s="T8">Miʔ </ts>
               <ts e="T10" id="Seg_63" n="e" s="T9">kadaʔlaʔbəbaʔ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_64" s="T0">PKZ_196X_BrushingHair_nar.001 (002)</ta>
            <ta e="T6" id="Seg_65" s="T3">PKZ_196X_BrushingHair_nar.002 (003)</ta>
            <ta e="T8" id="Seg_66" s="T6">PKZ_196X_BrushingHair_nar.003 (004)</ta>
            <ta e="T10" id="Seg_67" s="T8">PKZ_196X_BrushingHair_nar.004 (005)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_68" s="T0">Nada eʔbdəm kadaʔsittə. </ta>
            <ta e="T6" id="Seg_69" s="T3">I tĭlejzittə eʔbdəm. </ta>
            <ta e="T8" id="Seg_70" s="T6">Măn kadaʔlaʔbəm. </ta>
            <ta e="T10" id="Seg_71" s="T8">Miʔ kadaʔlaʔbəbaʔ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_72" s="T0">nada</ta>
            <ta e="T2" id="Seg_73" s="T1">eʔbdə-m</ta>
            <ta e="T3" id="Seg_74" s="T2">kadaʔ-sittə</ta>
            <ta e="T4" id="Seg_75" s="T3">i</ta>
            <ta e="T5" id="Seg_76" s="T4">tĭlej-zittə</ta>
            <ta e="T6" id="Seg_77" s="T5">eʔbdə-m</ta>
            <ta e="T7" id="Seg_78" s="T6">măn</ta>
            <ta e="T8" id="Seg_79" s="T7">kadaʔ-laʔbə-m</ta>
            <ta e="T9" id="Seg_80" s="T8">miʔ</ta>
            <ta e="T10" id="Seg_81" s="T9">kadaʔ-laʔbə-baʔ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_82" s="T0">nadə</ta>
            <ta e="T2" id="Seg_83" s="T1">eʔbdə-m</ta>
            <ta e="T3" id="Seg_84" s="T2">kadaʔ-zittə</ta>
            <ta e="T4" id="Seg_85" s="T3">i</ta>
            <ta e="T5" id="Seg_86" s="T4">tĭle-zittə</ta>
            <ta e="T6" id="Seg_87" s="T5">eʔbdə-m</ta>
            <ta e="T7" id="Seg_88" s="T6">măn</ta>
            <ta e="T8" id="Seg_89" s="T7">kadaʔ-laʔbə-m</ta>
            <ta e="T9" id="Seg_90" s="T8">miʔ</ta>
            <ta e="T10" id="Seg_91" s="T9">kadaʔ-laʔbə-bAʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_92" s="T0">one.should</ta>
            <ta e="T2" id="Seg_93" s="T1">hair-NOM/GEN/ACC.1SG</ta>
            <ta e="T3" id="Seg_94" s="T2">scratch-INF.LAT</ta>
            <ta e="T4" id="Seg_95" s="T3">and</ta>
            <ta e="T5" id="Seg_96" s="T4">comb-INF.LAT</ta>
            <ta e="T6" id="Seg_97" s="T5">hair-NOM/GEN/ACC.1SG</ta>
            <ta e="T7" id="Seg_98" s="T6">I.NOM</ta>
            <ta e="T8" id="Seg_99" s="T7">scratch-DUR-1SG</ta>
            <ta e="T9" id="Seg_100" s="T8">we.NOM</ta>
            <ta e="T10" id="Seg_101" s="T9">scratch-DUR-1PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_102" s="T0">надо</ta>
            <ta e="T2" id="Seg_103" s="T1">волосы-NOM/GEN/ACC.1SG</ta>
            <ta e="T3" id="Seg_104" s="T2">чесать-INF.LAT</ta>
            <ta e="T4" id="Seg_105" s="T3">и</ta>
            <ta e="T5" id="Seg_106" s="T4">расчесывать-INF.LAT</ta>
            <ta e="T6" id="Seg_107" s="T5">волосы-NOM/GEN/ACC.1SG</ta>
            <ta e="T7" id="Seg_108" s="T6">я.NOM</ta>
            <ta e="T8" id="Seg_109" s="T7">чесать-DUR-1SG</ta>
            <ta e="T9" id="Seg_110" s="T8">мы.NOM</ta>
            <ta e="T10" id="Seg_111" s="T9">чесать-DUR-1PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_112" s="T0">ptcl</ta>
            <ta e="T2" id="Seg_113" s="T1">n-n:case.poss</ta>
            <ta e="T3" id="Seg_114" s="T2">v-v:n.fin</ta>
            <ta e="T4" id="Seg_115" s="T3">conj</ta>
            <ta e="T5" id="Seg_116" s="T4">v-v:n.fin</ta>
            <ta e="T6" id="Seg_117" s="T5">n-n:case.poss</ta>
            <ta e="T7" id="Seg_118" s="T6">pers</ta>
            <ta e="T8" id="Seg_119" s="T7">v-v&gt;v-v:pn</ta>
            <ta e="T9" id="Seg_120" s="T8">pers</ta>
            <ta e="T10" id="Seg_121" s="T9">v-v&gt;v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_122" s="T0">ptcl</ta>
            <ta e="T2" id="Seg_123" s="T1">n</ta>
            <ta e="T3" id="Seg_124" s="T2">v</ta>
            <ta e="T4" id="Seg_125" s="T3">conj</ta>
            <ta e="T5" id="Seg_126" s="T4">v</ta>
            <ta e="T6" id="Seg_127" s="T5">n</ta>
            <ta e="T7" id="Seg_128" s="T6">pers</ta>
            <ta e="T8" id="Seg_129" s="T7">v</ta>
            <ta e="T9" id="Seg_130" s="T8">pers</ta>
            <ta e="T10" id="Seg_131" s="T9">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_132" s="T0">ptcl:pred</ta>
            <ta e="T2" id="Seg_133" s="T1">np:O</ta>
            <ta e="T6" id="Seg_134" s="T5">np:O</ta>
            <ta e="T7" id="Seg_135" s="T6">pro.h:S</ta>
            <ta e="T8" id="Seg_136" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_137" s="T8">pro.h:S</ta>
            <ta e="T10" id="Seg_138" s="T9">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_139" s="T1">np:P</ta>
            <ta e="T6" id="Seg_140" s="T5">np:P</ta>
            <ta e="T7" id="Seg_141" s="T6">pro.h:A</ta>
            <ta e="T9" id="Seg_142" s="T8">pro.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1" id="Seg_143" s="T0">RUS:mod</ta>
            <ta e="T3" id="Seg_144" s="T2">RUS:calq</ta>
            <ta e="T4" id="Seg_145" s="T3">RUS:gram</ta>
            <ta e="T8" id="Seg_146" s="T7">RUS:calq</ta>
            <ta e="T10" id="Seg_147" s="T9">RUS:calq</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_148" s="T0">Надо волосы чесать.</ta>
            <ta e="T6" id="Seg_149" s="T3">И расчесать волосы.</ta>
            <ta e="T8" id="Seg_150" s="T6">Я чешу.</ta>
            <ta e="T10" id="Seg_151" s="T8">Мы чешем.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_152" s="T0">I need to brush my hear.</ta>
            <ta e="T6" id="Seg_153" s="T3">And to comb my hair.</ta>
            <ta e="T8" id="Seg_154" s="T6">I am brushing.</ta>
            <ta e="T10" id="Seg_155" s="T8">We are brushing.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_156" s="T0">Ich muss mein Haar bürsten.</ta>
            <ta e="T6" id="Seg_157" s="T3">Und mein Haar kämmen.</ta>
            <ta e="T8" id="Seg_158" s="T6">Ich bürste.</ta>
            <ta e="T10" id="Seg_159" s="T8">Wir bürsten.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_160" s="T0">[GVY:] Most probably, this use is due to the Russian polysemy: чесать 'scratch; comb'</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
