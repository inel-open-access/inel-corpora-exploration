<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID5A58ACA6-3841-68F7-1406-498C669C8A14">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Dream_nar.wav" />
         <referenced-file url="PKZ_196X_Dream_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\nar\PKZ_196X_Dream_nar\PKZ_196X_Dream_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">97</ud-information>
            <ud-information attribute-name="# HIAT:w">67</ud-information>
            <ud-information attribute-name="# e">67</ud-information>
            <ud-information attribute-name="# HIAT:u">15</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.017" type="appl" />
         <tli id="T1" time="0.744" type="appl" />
         <tli id="T2" time="1.471" type="appl" />
         <tli id="T3" time="2.197" type="appl" />
         <tli id="T4" time="2.924" type="appl" />
         <tli id="T5" time="3.651" type="appl" />
         <tli id="T6" time="4.695" type="appl" />
         <tli id="T7" time="6.9394602634261995" />
         <tli id="T8" time="7.68" type="appl" />
         <tli id="T9" time="8.198" type="appl" />
         <tli id="T10" time="8.715" type="appl" />
         <tli id="T11" time="9.232" type="appl" />
         <tli id="T12" time="9.75" type="appl" />
         <tli id="T13" time="10.267" type="appl" />
         <tli id="T14" time="10.784" type="appl" />
         <tli id="T15" time="11.302" type="appl" />
         <tli id="T16" time="13.165642670765365" />
         <tli id="T17" time="14.241" type="appl" />
         <tli id="T18" time="15.19" type="appl" />
         <tli id="T19" time="16.139" type="appl" />
         <tli id="T20" time="16.801" type="appl" />
         <tli id="T21" time="17.463" type="appl" />
         <tli id="T22" time="18.125" type="appl" />
         <tli id="T23" time="18.833" type="appl" />
         <tli id="T24" time="19.541" type="appl" />
         <tli id="T25" time="20.248" type="appl" />
         <tli id="T26" time="20.956" type="appl" />
         <tli id="T27" time="21.664" type="appl" />
         <tli id="T28" time="22.476" type="appl" />
         <tli id="T29" time="23.288" type="appl" />
         <tli id="T30" time="24.1" type="appl" />
         <tli id="T31" time="24.911" type="appl" />
         <tli id="T32" time="25.723" type="appl" />
         <tli id="T33" time="26.535" type="appl" />
         <tli id="T34" time="27.11" type="appl" />
         <tli id="T35" time="27.685" type="appl" />
         <tli id="T36" time="28.261" type="appl" />
         <tli id="T37" time="28.836" type="appl" />
         <tli id="T38" time="29.510992457705225" />
         <tli id="T39" time="30.166" type="appl" />
         <tli id="T40" time="30.92" type="appl" />
         <tli id="T41" time="31.675" type="appl" />
         <tli id="T42" time="33.1507549375778" />
         <tli id="T43" time="33.653" type="appl" />
         <tli id="T44" time="34.065" type="appl" />
         <tli id="T45" time="34.477" type="appl" />
         <tli id="T46" time="34.89" type="appl" />
         <tli id="T47" time="35.302" type="appl" />
         <tli id="T48" time="35.714" type="appl" />
         <tli id="T49" time="36.126" type="appl" />
         <tli id="T50" time="36.566" type="appl" />
         <tli id="T51" time="37.006" type="appl" />
         <tli id="T52" time="37.446" type="appl" />
         <tli id="T53" time="37.886" type="appl" />
         <tli id="T54" time="38.7703178598336" />
         <tli id="T55" time="39.82" type="appl" />
         <tli id="T56" time="40.418" type="appl" />
         <tli id="T57" time="41.016" type="appl" />
         <tli id="T58" time="41.614" type="appl" />
         <tli id="T59" time="42.481" type="appl" />
         <tli id="T60" time="43.074" type="appl" />
         <tli id="T61" time="43.668" type="appl" />
         <tli id="T62" time="44.261" type="appl" />
         <tli id="T63" time="45.269" type="appl" />
         <tli id="T64" time="46.277" type="appl" />
         <tli id="T65" time="47.457" type="appl" />
         <tli id="T66" time="48.638" type="appl" />
         <tli id="T67" time="49.818" type="appl" />
         <tli id="T68" time="50.876" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T68" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Măn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">taldʼen</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">dʼodənʼi</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_12" n="HIAT:ip">(</nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">kubiam=</ts>
                  <nts id="Seg_15" n="HIAT:ip">)</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_18" n="HIAT:w" s="T4">kubiam</ts>
                  <nts id="Seg_19" n="HIAT:ip">.</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_22" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_24" n="HIAT:w" s="T5">Kunolbiam</ts>
                  <nts id="Seg_25" n="HIAT:ip">,</nts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">kubiam</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_32" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_34" n="HIAT:w" s="T7">Sĭre</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_37" n="HIAT:w" s="T8">ine</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">bar</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">măna</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">davaj</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">üjüziʔ</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">toʔnarzittə</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T14">da</ts>
                  <nts id="Seg_56" n="HIAT:ip">…</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_59" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">Tĭmeziʔ</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">bar</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">talbəsʼtə</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_71" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">A</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">măn</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">kirgarlaʔbəm</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_83" n="HIAT:u" s="T22">
                  <nts id="Seg_84" n="HIAT:ip">(</nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">Dĭ</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">bɨl</ts>
                  <nts id="Seg_90" n="HIAT:ip">)</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_93" n="HIAT:w" s="T24">măna</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">üjütsiʔ</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_99" n="HIAT:w" s="T26">dʼaʔbluʔpi</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_103" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_105" n="HIAT:w" s="T27">Dĭgəttə</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_108" n="HIAT:w" s="T28">tibi</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_111" n="HIAT:w" s="T29">šobi</ts>
                  <nts id="Seg_112" n="HIAT:ip">,</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">davaj</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">dĭm</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">münörzittə</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_125" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">A</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">dĭ</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">măndə</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">kak</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">kuza:</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_141" n="HIAT:ip">"</nts>
                  <ts e="T39" id="Seg_143" n="HIAT:w" s="T38">Ĭmbi</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_145" n="HIAT:ip">(</nts>
                  <ts e="T40" id="Seg_147" n="HIAT:w" s="T39">dĭn</ts>
                  <nts id="Seg_148" n="HIAT:ip">)</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_151" n="HIAT:w" s="T40">tăn</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_154" n="HIAT:w" s="T41">tugandə</ts>
                  <nts id="Seg_155" n="HIAT:ip">?</nts>
                  <nts id="Seg_156" n="HIAT:ip">"</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_159" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_161" n="HIAT:w" s="T42">A</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_164" n="HIAT:w" s="T43">dĭ</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_167" n="HIAT:w" s="T44">măndə:</ts>
                  <nts id="Seg_168" n="HIAT:ip">"</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_171" n="HIAT:w" s="T45">Ĭmbi</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_174" n="HIAT:w" s="T46">tugan</ts>
                  <nts id="Seg_175" n="HIAT:ip">,</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_178" n="HIAT:w" s="T47">ej</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_181" n="HIAT:w" s="T48">tugan</ts>
                  <nts id="Seg_182" n="HIAT:ip">.</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_185" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_187" n="HIAT:w" s="T49">A</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_190" n="HIAT:w" s="T50">nʼe</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_193" n="HIAT:w" s="T51">nada</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_195" n="HIAT:ip">(</nts>
                  <ts e="T53" id="Seg_197" n="HIAT:w" s="T52">nʼe-</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_200" n="HIAT:w" s="T53">nʼ-</ts>
                  <nts id="Seg_201" n="HIAT:ip">)</nts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_205" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_207" n="HIAT:w" s="T54">Ĭmbidə</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_210" n="HIAT:w" s="T55">iʔ</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_213" n="HIAT:w" s="T56">aʔ</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_216" n="HIAT:w" s="T57">dĭʔnə</ts>
                  <nts id="Seg_217" n="HIAT:ip">"</nts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_221" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_223" n="HIAT:w" s="T58">A</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_226" n="HIAT:w" s="T59">măn</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_229" n="HIAT:w" s="T60">mĭlleʔbəm</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_232" n="HIAT:w" s="T61">bar</ts>
                  <nts id="Seg_233" n="HIAT:ip">.</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_236" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_238" n="HIAT:w" s="T62">Balgaš</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_241" n="HIAT:w" s="T63">vʼizʼdʼe</ts>
                  <nts id="Seg_242" n="HIAT:ip">.</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_245" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_247" n="HIAT:w" s="T64">Dĭgəttə</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_250" n="HIAT:w" s="T65">šüʔdərbim</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_253" n="HIAT:w" s="T66">bar</ts>
                  <nts id="Seg_254" n="HIAT:ip">.</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_257" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_259" n="HIAT:w" s="T67">Kabarləj</ts>
                  <nts id="Seg_260" n="HIAT:ip">.</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T68" id="Seg_262" n="sc" s="T0">
               <ts e="T1" id="Seg_264" n="e" s="T0">Măn </ts>
               <ts e="T2" id="Seg_266" n="e" s="T1">taldʼen </ts>
               <ts e="T3" id="Seg_268" n="e" s="T2">dʼodənʼi </ts>
               <ts e="T4" id="Seg_270" n="e" s="T3">(kubiam=) </ts>
               <ts e="T5" id="Seg_272" n="e" s="T4">kubiam. </ts>
               <ts e="T6" id="Seg_274" n="e" s="T5">Kunolbiam, </ts>
               <ts e="T7" id="Seg_276" n="e" s="T6">kubiam. </ts>
               <ts e="T8" id="Seg_278" n="e" s="T7">Sĭre </ts>
               <ts e="T9" id="Seg_280" n="e" s="T8">ine </ts>
               <ts e="T10" id="Seg_282" n="e" s="T9">bar </ts>
               <ts e="T11" id="Seg_284" n="e" s="T10">măna </ts>
               <ts e="T12" id="Seg_286" n="e" s="T11">davaj </ts>
               <ts e="T13" id="Seg_288" n="e" s="T12">üjüziʔ </ts>
               <ts e="T14" id="Seg_290" n="e" s="T13">toʔnarzittə </ts>
               <ts e="T16" id="Seg_292" n="e" s="T14">da… </ts>
               <ts e="T17" id="Seg_294" n="e" s="T16">Tĭmeziʔ </ts>
               <ts e="T18" id="Seg_296" n="e" s="T17">bar </ts>
               <ts e="T19" id="Seg_298" n="e" s="T18">talbəsʼtə. </ts>
               <ts e="T20" id="Seg_300" n="e" s="T19">A </ts>
               <ts e="T21" id="Seg_302" n="e" s="T20">măn </ts>
               <ts e="T22" id="Seg_304" n="e" s="T21">kirgarlaʔbəm. </ts>
               <ts e="T23" id="Seg_306" n="e" s="T22">(Dĭ </ts>
               <ts e="T24" id="Seg_308" n="e" s="T23">bɨl) </ts>
               <ts e="T25" id="Seg_310" n="e" s="T24">măna </ts>
               <ts e="T26" id="Seg_312" n="e" s="T25">üjütsiʔ </ts>
               <ts e="T27" id="Seg_314" n="e" s="T26">dʼaʔbluʔpi. </ts>
               <ts e="T28" id="Seg_316" n="e" s="T27">Dĭgəttə </ts>
               <ts e="T29" id="Seg_318" n="e" s="T28">tibi </ts>
               <ts e="T30" id="Seg_320" n="e" s="T29">šobi, </ts>
               <ts e="T31" id="Seg_322" n="e" s="T30">davaj </ts>
               <ts e="T32" id="Seg_324" n="e" s="T31">dĭm </ts>
               <ts e="T33" id="Seg_326" n="e" s="T32">münörzittə. </ts>
               <ts e="T34" id="Seg_328" n="e" s="T33">A </ts>
               <ts e="T35" id="Seg_330" n="e" s="T34">dĭ </ts>
               <ts e="T36" id="Seg_332" n="e" s="T35">măndə </ts>
               <ts e="T37" id="Seg_334" n="e" s="T36">kak </ts>
               <ts e="T38" id="Seg_336" n="e" s="T37">kuza: </ts>
               <ts e="T39" id="Seg_338" n="e" s="T38">"Ĭmbi </ts>
               <ts e="T40" id="Seg_340" n="e" s="T39">(dĭn) </ts>
               <ts e="T41" id="Seg_342" n="e" s="T40">tăn </ts>
               <ts e="T42" id="Seg_344" n="e" s="T41">tugandə?" </ts>
               <ts e="T43" id="Seg_346" n="e" s="T42">A </ts>
               <ts e="T44" id="Seg_348" n="e" s="T43">dĭ </ts>
               <ts e="T45" id="Seg_350" n="e" s="T44">măndə:" </ts>
               <ts e="T46" id="Seg_352" n="e" s="T45">Ĭmbi </ts>
               <ts e="T47" id="Seg_354" n="e" s="T46">tugan, </ts>
               <ts e="T48" id="Seg_356" n="e" s="T47">ej </ts>
               <ts e="T49" id="Seg_358" n="e" s="T48">tugan. </ts>
               <ts e="T50" id="Seg_360" n="e" s="T49">A </ts>
               <ts e="T51" id="Seg_362" n="e" s="T50">nʼe </ts>
               <ts e="T52" id="Seg_364" n="e" s="T51">nada </ts>
               <ts e="T53" id="Seg_366" n="e" s="T52">(nʼe- </ts>
               <ts e="T54" id="Seg_368" n="e" s="T53">nʼ-). </ts>
               <ts e="T55" id="Seg_370" n="e" s="T54">Ĭmbidə </ts>
               <ts e="T56" id="Seg_372" n="e" s="T55">iʔ </ts>
               <ts e="T57" id="Seg_374" n="e" s="T56">aʔ </ts>
               <ts e="T58" id="Seg_376" n="e" s="T57">dĭʔnə". </ts>
               <ts e="T59" id="Seg_378" n="e" s="T58">A </ts>
               <ts e="T60" id="Seg_380" n="e" s="T59">măn </ts>
               <ts e="T61" id="Seg_382" n="e" s="T60">mĭlleʔbəm </ts>
               <ts e="T62" id="Seg_384" n="e" s="T61">bar. </ts>
               <ts e="T63" id="Seg_386" n="e" s="T62">Balgaš </ts>
               <ts e="T64" id="Seg_388" n="e" s="T63">vʼizʼdʼe. </ts>
               <ts e="T65" id="Seg_390" n="e" s="T64">Dĭgəttə </ts>
               <ts e="T66" id="Seg_392" n="e" s="T65">šüʔdərbim </ts>
               <ts e="T67" id="Seg_394" n="e" s="T66">bar. </ts>
               <ts e="T68" id="Seg_396" n="e" s="T67">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_397" s="T0">PKZ_196X_Dream_nar.001 (002)</ta>
            <ta e="T7" id="Seg_398" s="T5">PKZ_196X_Dream_nar.002 (003)</ta>
            <ta e="T16" id="Seg_399" s="T7">PKZ_196X_Dream_nar.003 (004)</ta>
            <ta e="T19" id="Seg_400" s="T16">PKZ_196X_Dream_nar.004 (005)</ta>
            <ta e="T22" id="Seg_401" s="T19">PKZ_196X_Dream_nar.005 (006)</ta>
            <ta e="T27" id="Seg_402" s="T22">PKZ_196X_Dream_nar.006 (007)</ta>
            <ta e="T33" id="Seg_403" s="T27">PKZ_196X_Dream_nar.007 (008)</ta>
            <ta e="T42" id="Seg_404" s="T33">PKZ_196X_Dream_nar.008 (009) </ta>
            <ta e="T49" id="Seg_405" s="T42">PKZ_196X_Dream_nar.009 (011)</ta>
            <ta e="T54" id="Seg_406" s="T49">PKZ_196X_Dream_nar.010 (012)</ta>
            <ta e="T58" id="Seg_407" s="T54">PKZ_196X_Dream_nar.011 (013)</ta>
            <ta e="T62" id="Seg_408" s="T58">PKZ_196X_Dream_nar.012 (014)</ta>
            <ta e="T64" id="Seg_409" s="T62">PKZ_196X_Dream_nar.013 (015)</ta>
            <ta e="T67" id="Seg_410" s="T64">PKZ_196X_Dream_nar.014 (016)</ta>
            <ta e="T68" id="Seg_411" s="T67">PKZ_196X_Dream_nar.015 (017)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_412" s="T0">Măn taldʼen dʼodənʼi (kubiam=) kubiam. </ta>
            <ta e="T7" id="Seg_413" s="T5">Kunolbiam, kubiam. </ta>
            <ta e="T16" id="Seg_414" s="T7">Sĭre ine bar măna davaj üjüziʔ toʔnarzittə da … </ta>
            <ta e="T19" id="Seg_415" s="T16">Tĭmeziʔ bar talbəsʼtə. </ta>
            <ta e="T22" id="Seg_416" s="T19">A măn kirgarlaʔbəm. </ta>
            <ta e="T27" id="Seg_417" s="T22">(Dĭ bɨl) măna üjütsiʔ dʼaʔbluʔpi. </ta>
            <ta e="T33" id="Seg_418" s="T27">Dĭgəttə tibi šobi, davaj dĭm münörzittə. </ta>
            <ta e="T42" id="Seg_419" s="T33">A dĭ măndə kak kuza: "Ĭmbi (dĭn) tăn tugandə?" </ta>
            <ta e="T49" id="Seg_420" s="T42">A dĭ măndə:" Ĭmbi tugan, ej tugan. </ta>
            <ta e="T54" id="Seg_421" s="T49">A nʼe nada (nʼe- nʼ-). </ta>
            <ta e="T58" id="Seg_422" s="T54">Ĭmbidə iʔ aʔ dĭʔnə". </ta>
            <ta e="T62" id="Seg_423" s="T58">A măn mĭlleʔbəm bar. </ta>
            <ta e="T64" id="Seg_424" s="T62">Balgaš vʼizʼdʼe. </ta>
            <ta e="T67" id="Seg_425" s="T64">Dĭgəttə šüʔdərbim bar. </ta>
            <ta e="T68" id="Seg_426" s="T67">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_427" s="T0">măn</ta>
            <ta e="T2" id="Seg_428" s="T1">taldʼen</ta>
            <ta e="T3" id="Seg_429" s="T2">dʼodə-nʼi</ta>
            <ta e="T4" id="Seg_430" s="T3">ku-bia-m</ta>
            <ta e="T5" id="Seg_431" s="T4">ku-bia-m</ta>
            <ta e="T6" id="Seg_432" s="T5">kunol-bia-m</ta>
            <ta e="T7" id="Seg_433" s="T6">ku-bia-m</ta>
            <ta e="T8" id="Seg_434" s="T7">sĭre</ta>
            <ta e="T9" id="Seg_435" s="T8">ine</ta>
            <ta e="T10" id="Seg_436" s="T9">bar</ta>
            <ta e="T11" id="Seg_437" s="T10">măna</ta>
            <ta e="T12" id="Seg_438" s="T11">davaj</ta>
            <ta e="T13" id="Seg_439" s="T12">üjü-ziʔ</ta>
            <ta e="T14" id="Seg_440" s="T13">toʔ-nar-zittə</ta>
            <ta e="T16" id="Seg_441" s="T14">da</ta>
            <ta e="T17" id="Seg_442" s="T16">tĭme-ziʔ</ta>
            <ta e="T18" id="Seg_443" s="T17">bar</ta>
            <ta e="T19" id="Seg_444" s="T18">talbə-sʼtə</ta>
            <ta e="T20" id="Seg_445" s="T19">a</ta>
            <ta e="T21" id="Seg_446" s="T20">măn</ta>
            <ta e="T22" id="Seg_447" s="T21">kirgar-laʔbə-m</ta>
            <ta e="T23" id="Seg_448" s="T22">dĭ</ta>
            <ta e="T25" id="Seg_449" s="T24">măna</ta>
            <ta e="T26" id="Seg_450" s="T25">üjü-t-siʔ</ta>
            <ta e="T27" id="Seg_451" s="T26">dʼaʔb-luʔ-pi</ta>
            <ta e="T28" id="Seg_452" s="T27">dĭgəttə</ta>
            <ta e="T29" id="Seg_453" s="T28">tibi</ta>
            <ta e="T30" id="Seg_454" s="T29">šo-bi</ta>
            <ta e="T31" id="Seg_455" s="T30">davaj</ta>
            <ta e="T32" id="Seg_456" s="T31">dĭ-m</ta>
            <ta e="T33" id="Seg_457" s="T32">münör-zittə</ta>
            <ta e="T34" id="Seg_458" s="T33">a</ta>
            <ta e="T35" id="Seg_459" s="T34">dĭ</ta>
            <ta e="T36" id="Seg_460" s="T35">măn-də</ta>
            <ta e="T37" id="Seg_461" s="T36">kak</ta>
            <ta e="T38" id="Seg_462" s="T37">kuza</ta>
            <ta e="T39" id="Seg_463" s="T38">ĭmbi</ta>
            <ta e="T40" id="Seg_464" s="T39">dĭn</ta>
            <ta e="T41" id="Seg_465" s="T40">tăn</ta>
            <ta e="T42" id="Seg_466" s="T41">tugan-də</ta>
            <ta e="T43" id="Seg_467" s="T42">a</ta>
            <ta e="T44" id="Seg_468" s="T43">dĭ</ta>
            <ta e="T45" id="Seg_469" s="T44">măn-də</ta>
            <ta e="T46" id="Seg_470" s="T45">ĭmbi</ta>
            <ta e="T47" id="Seg_471" s="T46">tugan</ta>
            <ta e="T48" id="Seg_472" s="T47">ej</ta>
            <ta e="T49" id="Seg_473" s="T48">tugan</ta>
            <ta e="T50" id="Seg_474" s="T49">a</ta>
            <ta e="T51" id="Seg_475" s="T50">nʼe</ta>
            <ta e="T52" id="Seg_476" s="T51">nada</ta>
            <ta e="T55" id="Seg_477" s="T54">ĭmbi=də</ta>
            <ta e="T56" id="Seg_478" s="T55">i-ʔ</ta>
            <ta e="T57" id="Seg_479" s="T56">a-ʔ</ta>
            <ta e="T58" id="Seg_480" s="T57">dĭʔ-nə</ta>
            <ta e="T59" id="Seg_481" s="T58">a</ta>
            <ta e="T60" id="Seg_482" s="T59">măn</ta>
            <ta e="T61" id="Seg_483" s="T60">mĭl-leʔbə-m</ta>
            <ta e="T62" id="Seg_484" s="T61">bar</ta>
            <ta e="T63" id="Seg_485" s="T62">balgaš</ta>
            <ta e="T65" id="Seg_486" s="T64">dĭgəttə</ta>
            <ta e="T66" id="Seg_487" s="T65">šüʔdər-bi-m</ta>
            <ta e="T67" id="Seg_488" s="T66">bar</ta>
            <ta e="T68" id="Seg_489" s="T67">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_490" s="T0">măn</ta>
            <ta e="T2" id="Seg_491" s="T1">taldʼen</ta>
            <ta e="T3" id="Seg_492" s="T2">dʼodə-gənʼi</ta>
            <ta e="T4" id="Seg_493" s="T3">ku-bi-m</ta>
            <ta e="T5" id="Seg_494" s="T4">ku-bi-m</ta>
            <ta e="T6" id="Seg_495" s="T5">kunol-bi-m</ta>
            <ta e="T7" id="Seg_496" s="T6">ku-bi-m</ta>
            <ta e="T8" id="Seg_497" s="T7">sĭri</ta>
            <ta e="T9" id="Seg_498" s="T8">ine</ta>
            <ta e="T10" id="Seg_499" s="T9">bar</ta>
            <ta e="T11" id="Seg_500" s="T10">măna</ta>
            <ta e="T12" id="Seg_501" s="T11">davaj</ta>
            <ta e="T13" id="Seg_502" s="T12">üjü-ziʔ</ta>
            <ta e="T14" id="Seg_503" s="T13">toʔ-nar-zittə</ta>
            <ta e="T16" id="Seg_504" s="T14">da</ta>
            <ta e="T17" id="Seg_505" s="T16">tĭme-ziʔ</ta>
            <ta e="T18" id="Seg_506" s="T17">bar</ta>
            <ta e="T19" id="Seg_507" s="T18">talbə-zittə</ta>
            <ta e="T20" id="Seg_508" s="T19">a</ta>
            <ta e="T21" id="Seg_509" s="T20">măn</ta>
            <ta e="T22" id="Seg_510" s="T21">kirgaːr-laʔbə-m</ta>
            <ta e="T23" id="Seg_511" s="T22">dĭ</ta>
            <ta e="T25" id="Seg_512" s="T24">măna</ta>
            <ta e="T26" id="Seg_513" s="T25">üjü-t-ziʔ</ta>
            <ta e="T27" id="Seg_514" s="T26">dʼabə-luʔbdə-bi</ta>
            <ta e="T28" id="Seg_515" s="T27">dĭgəttə</ta>
            <ta e="T29" id="Seg_516" s="T28">tibi</ta>
            <ta e="T30" id="Seg_517" s="T29">šo-bi</ta>
            <ta e="T31" id="Seg_518" s="T30">davaj</ta>
            <ta e="T32" id="Seg_519" s="T31">dĭ-m</ta>
            <ta e="T33" id="Seg_520" s="T32">münör-zittə</ta>
            <ta e="T34" id="Seg_521" s="T33">a</ta>
            <ta e="T35" id="Seg_522" s="T34">dĭ</ta>
            <ta e="T36" id="Seg_523" s="T35">măn-ntə</ta>
            <ta e="T37" id="Seg_524" s="T36">kak</ta>
            <ta e="T38" id="Seg_525" s="T37">kuza</ta>
            <ta e="T39" id="Seg_526" s="T38">ĭmbi</ta>
            <ta e="T40" id="Seg_527" s="T39">dĭn</ta>
            <ta e="T41" id="Seg_528" s="T40">tăn</ta>
            <ta e="T42" id="Seg_529" s="T41">tugan-də</ta>
            <ta e="T43" id="Seg_530" s="T42">a</ta>
            <ta e="T44" id="Seg_531" s="T43">dĭ</ta>
            <ta e="T45" id="Seg_532" s="T44">măn-ntə</ta>
            <ta e="T46" id="Seg_533" s="T45">ĭmbi</ta>
            <ta e="T47" id="Seg_534" s="T46">tugan</ta>
            <ta e="T48" id="Seg_535" s="T47">ej</ta>
            <ta e="T49" id="Seg_536" s="T48">tugan</ta>
            <ta e="T50" id="Seg_537" s="T49">a</ta>
            <ta e="T51" id="Seg_538" s="T50">nʼe</ta>
            <ta e="T52" id="Seg_539" s="T51">nadə</ta>
            <ta e="T55" id="Seg_540" s="T54">ĭmbi=də</ta>
            <ta e="T56" id="Seg_541" s="T55">e-ʔ</ta>
            <ta e="T57" id="Seg_542" s="T56">a-ʔ</ta>
            <ta e="T58" id="Seg_543" s="T57">dĭ-Tə</ta>
            <ta e="T59" id="Seg_544" s="T58">a</ta>
            <ta e="T60" id="Seg_545" s="T59">măn</ta>
            <ta e="T61" id="Seg_546" s="T60">mĭn-laʔbə-m</ta>
            <ta e="T62" id="Seg_547" s="T61">bar</ta>
            <ta e="T63" id="Seg_548" s="T62">balgaš</ta>
            <ta e="T65" id="Seg_549" s="T64">dĭgəttə</ta>
            <ta e="T66" id="Seg_550" s="T65">šüʔdər-bi-m</ta>
            <ta e="T67" id="Seg_551" s="T66">bar</ta>
            <ta e="T68" id="Seg_552" s="T67">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_553" s="T0">I.NOM</ta>
            <ta e="T2" id="Seg_554" s="T1">yesterday</ta>
            <ta e="T3" id="Seg_555" s="T2">dream-LAT/LOC.1SG</ta>
            <ta e="T4" id="Seg_556" s="T3">see-PST-1SG</ta>
            <ta e="T5" id="Seg_557" s="T4">see-PST-1SG</ta>
            <ta e="T6" id="Seg_558" s="T5">sleep-PST-1SG</ta>
            <ta e="T7" id="Seg_559" s="T6">see-PST-1SG</ta>
            <ta e="T8" id="Seg_560" s="T7">snow.[NOM.SG]</ta>
            <ta e="T9" id="Seg_561" s="T8">horse.[NOM.SG]</ta>
            <ta e="T10" id="Seg_562" s="T9">PTCL</ta>
            <ta e="T11" id="Seg_563" s="T10">I.ACC</ta>
            <ta e="T12" id="Seg_564" s="T11">INCH</ta>
            <ta e="T13" id="Seg_565" s="T12">foot-INS</ta>
            <ta e="T14" id="Seg_566" s="T13">jolt-MULT-INF.LAT</ta>
            <ta e="T16" id="Seg_567" s="T14">and</ta>
            <ta e="T17" id="Seg_568" s="T16">tooth-INS</ta>
            <ta e="T18" id="Seg_569" s="T17">PTCL</ta>
            <ta e="T19" id="Seg_570" s="T18">bite-INF.LAT</ta>
            <ta e="T20" id="Seg_571" s="T19">and</ta>
            <ta e="T21" id="Seg_572" s="T20">I.NOM</ta>
            <ta e="T22" id="Seg_573" s="T21">shout-DUR-1SG</ta>
            <ta e="T23" id="Seg_574" s="T22">this</ta>
            <ta e="T25" id="Seg_575" s="T24">I.ACC</ta>
            <ta e="T26" id="Seg_576" s="T25">foot-3SG-INS</ta>
            <ta e="T27" id="Seg_577" s="T26">capture-MOM-PST</ta>
            <ta e="T28" id="Seg_578" s="T27">then</ta>
            <ta e="T29" id="Seg_579" s="T28">man.[NOM.SG]</ta>
            <ta e="T30" id="Seg_580" s="T29">come-PST.[3SG]</ta>
            <ta e="T31" id="Seg_581" s="T30">INCH</ta>
            <ta e="T32" id="Seg_582" s="T31">this-ACC</ta>
            <ta e="T33" id="Seg_583" s="T32">beat-INF.LAT</ta>
            <ta e="T34" id="Seg_584" s="T33">and</ta>
            <ta e="T35" id="Seg_585" s="T34">this.[NOM.SG]</ta>
            <ta e="T36" id="Seg_586" s="T35">say-IPFVZ.[3SG]</ta>
            <ta e="T37" id="Seg_587" s="T36">like</ta>
            <ta e="T38" id="Seg_588" s="T37">man.[NOM.SG]</ta>
            <ta e="T39" id="Seg_589" s="T38">what.[NOM.SG]</ta>
            <ta e="T40" id="Seg_590" s="T39">there</ta>
            <ta e="T41" id="Seg_591" s="T40">you.NOM</ta>
            <ta e="T42" id="Seg_592" s="T41">relative-NOM/GEN/ACC.3SG</ta>
            <ta e="T43" id="Seg_593" s="T42">and</ta>
            <ta e="T44" id="Seg_594" s="T43">this.[NOM.SG]</ta>
            <ta e="T45" id="Seg_595" s="T44">say-IPFVZ.[3SG]</ta>
            <ta e="T46" id="Seg_596" s="T45">what.[NOM.SG]</ta>
            <ta e="T47" id="Seg_597" s="T46">relative.[NOM.SG]</ta>
            <ta e="T48" id="Seg_598" s="T47">NEG</ta>
            <ta e="T49" id="Seg_599" s="T48">relative.[NOM.SG]</ta>
            <ta e="T50" id="Seg_600" s="T49">and</ta>
            <ta e="T51" id="Seg_601" s="T50">not</ta>
            <ta e="T52" id="Seg_602" s="T51">one.should</ta>
            <ta e="T55" id="Seg_603" s="T54">what.[NOM.SG]=INDEF</ta>
            <ta e="T56" id="Seg_604" s="T55">NEG.AUX-IMP.2SG</ta>
            <ta e="T57" id="Seg_605" s="T56">make-CNG</ta>
            <ta e="T58" id="Seg_606" s="T57">this-LAT</ta>
            <ta e="T59" id="Seg_607" s="T58">and</ta>
            <ta e="T60" id="Seg_608" s="T59">I.NOM</ta>
            <ta e="T61" id="Seg_609" s="T60">go-DUR-1SG</ta>
            <ta e="T62" id="Seg_610" s="T61">PTCL</ta>
            <ta e="T63" id="Seg_611" s="T62">dirty.[NOM.SG]</ta>
            <ta e="T65" id="Seg_612" s="T64">then</ta>
            <ta e="T66" id="Seg_613" s="T65">wake.up-PST-1SG</ta>
            <ta e="T67" id="Seg_614" s="T66">PTCL</ta>
            <ta e="T68" id="Seg_615" s="T67">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_616" s="T0">я.NOM</ta>
            <ta e="T2" id="Seg_617" s="T1">вчера</ta>
            <ta e="T3" id="Seg_618" s="T2">сон-LAT/LOC.1SG</ta>
            <ta e="T4" id="Seg_619" s="T3">видеть-PST-1SG</ta>
            <ta e="T5" id="Seg_620" s="T4">видеть-PST-1SG</ta>
            <ta e="T6" id="Seg_621" s="T5">спать-PST-1SG</ta>
            <ta e="T7" id="Seg_622" s="T6">видеть-PST-1SG</ta>
            <ta e="T8" id="Seg_623" s="T7">снег.[NOM.SG]</ta>
            <ta e="T9" id="Seg_624" s="T8">лошадь.[NOM.SG]</ta>
            <ta e="T10" id="Seg_625" s="T9">PTCL</ta>
            <ta e="T11" id="Seg_626" s="T10">я.ACC</ta>
            <ta e="T12" id="Seg_627" s="T11">INCH</ta>
            <ta e="T13" id="Seg_628" s="T12">нога-INS</ta>
            <ta e="T14" id="Seg_629" s="T13">трясти-MULT-INF.LAT</ta>
            <ta e="T16" id="Seg_630" s="T14">и</ta>
            <ta e="T17" id="Seg_631" s="T16">зуб-INS</ta>
            <ta e="T18" id="Seg_632" s="T17">PTCL</ta>
            <ta e="T19" id="Seg_633" s="T18">укусить-INF.LAT</ta>
            <ta e="T20" id="Seg_634" s="T19">а</ta>
            <ta e="T21" id="Seg_635" s="T20">я.NOM</ta>
            <ta e="T22" id="Seg_636" s="T21">кричать-DUR-1SG</ta>
            <ta e="T23" id="Seg_637" s="T22">этот</ta>
            <ta e="T25" id="Seg_638" s="T24">я.ACC</ta>
            <ta e="T26" id="Seg_639" s="T25">нога-3SG-INS</ta>
            <ta e="T27" id="Seg_640" s="T26">ловить-MOM-PST</ta>
            <ta e="T28" id="Seg_641" s="T27">тогда</ta>
            <ta e="T29" id="Seg_642" s="T28">мужчина.[NOM.SG]</ta>
            <ta e="T30" id="Seg_643" s="T29">прийти-PST.[3SG]</ta>
            <ta e="T31" id="Seg_644" s="T30">INCH</ta>
            <ta e="T32" id="Seg_645" s="T31">этот-ACC</ta>
            <ta e="T33" id="Seg_646" s="T32">бить-INF.LAT</ta>
            <ta e="T34" id="Seg_647" s="T33">а</ta>
            <ta e="T35" id="Seg_648" s="T34">этот.[NOM.SG]</ta>
            <ta e="T36" id="Seg_649" s="T35">сказать-IPFVZ.[3SG]</ta>
            <ta e="T37" id="Seg_650" s="T36">как</ta>
            <ta e="T38" id="Seg_651" s="T37">мужчина.[NOM.SG]</ta>
            <ta e="T39" id="Seg_652" s="T38">что.[NOM.SG]</ta>
            <ta e="T40" id="Seg_653" s="T39">там</ta>
            <ta e="T41" id="Seg_654" s="T40">ты.NOM</ta>
            <ta e="T42" id="Seg_655" s="T41">родственник-NOM/GEN/ACC.3SG</ta>
            <ta e="T43" id="Seg_656" s="T42">а</ta>
            <ta e="T44" id="Seg_657" s="T43">этот.[NOM.SG]</ta>
            <ta e="T45" id="Seg_658" s="T44">сказать-IPFVZ.[3SG]</ta>
            <ta e="T46" id="Seg_659" s="T45">что.[NOM.SG]</ta>
            <ta e="T47" id="Seg_660" s="T46">родственник.[NOM.SG]</ta>
            <ta e="T48" id="Seg_661" s="T47">NEG</ta>
            <ta e="T49" id="Seg_662" s="T48">родственник.[NOM.SG]</ta>
            <ta e="T50" id="Seg_663" s="T49">а</ta>
            <ta e="T51" id="Seg_664" s="T50">не</ta>
            <ta e="T52" id="Seg_665" s="T51">надо</ta>
            <ta e="T55" id="Seg_666" s="T54">что.[NOM.SG]=INDEF</ta>
            <ta e="T56" id="Seg_667" s="T55">NEG.AUX-IMP.2SG</ta>
            <ta e="T57" id="Seg_668" s="T56">делать-CNG</ta>
            <ta e="T58" id="Seg_669" s="T57">этот-LAT</ta>
            <ta e="T59" id="Seg_670" s="T58">а</ta>
            <ta e="T60" id="Seg_671" s="T59">я.NOM</ta>
            <ta e="T61" id="Seg_672" s="T60">идти-DUR-1SG</ta>
            <ta e="T62" id="Seg_673" s="T61">PTCL</ta>
            <ta e="T63" id="Seg_674" s="T62">грязный.[NOM.SG]</ta>
            <ta e="T65" id="Seg_675" s="T64">тогда</ta>
            <ta e="T66" id="Seg_676" s="T65">просыпаться-PST-1SG</ta>
            <ta e="T67" id="Seg_677" s="T66">PTCL</ta>
            <ta e="T68" id="Seg_678" s="T67">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_679" s="T0">pers</ta>
            <ta e="T2" id="Seg_680" s="T1">adv</ta>
            <ta e="T3" id="Seg_681" s="T2">n-n:case.poss</ta>
            <ta e="T4" id="Seg_682" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_683" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_684" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_685" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_686" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_687" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_688" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_689" s="T10">pers</ta>
            <ta e="T12" id="Seg_690" s="T11">v</ta>
            <ta e="T13" id="Seg_691" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_692" s="T13">v-v&gt;v-v:n.fin</ta>
            <ta e="T16" id="Seg_693" s="T14">conj</ta>
            <ta e="T17" id="Seg_694" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_695" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_696" s="T18">v-v:n.fin</ta>
            <ta e="T20" id="Seg_697" s="T19">conj</ta>
            <ta e="T21" id="Seg_698" s="T20">pers</ta>
            <ta e="T22" id="Seg_699" s="T21">v-v&gt;v-v:pn</ta>
            <ta e="T23" id="Seg_700" s="T22">dempro</ta>
            <ta e="T25" id="Seg_701" s="T24">pers</ta>
            <ta e="T26" id="Seg_702" s="T25">n-n:case.poss-n:case</ta>
            <ta e="T27" id="Seg_703" s="T26">v-v&gt;v-v:tense</ta>
            <ta e="T28" id="Seg_704" s="T27">adv</ta>
            <ta e="T29" id="Seg_705" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_706" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_707" s="T30">v</ta>
            <ta e="T32" id="Seg_708" s="T31">dempro-n:case</ta>
            <ta e="T33" id="Seg_709" s="T32">v-v:n.fin</ta>
            <ta e="T34" id="Seg_710" s="T33">conj</ta>
            <ta e="T35" id="Seg_711" s="T34">dempro-n:case</ta>
            <ta e="T36" id="Seg_712" s="T35">v-v&gt;v-v:pn</ta>
            <ta e="T37" id="Seg_713" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_714" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_715" s="T38">que-n:case</ta>
            <ta e="T40" id="Seg_716" s="T39">adv</ta>
            <ta e="T41" id="Seg_717" s="T40">pers</ta>
            <ta e="T42" id="Seg_718" s="T41">n-n:case.poss</ta>
            <ta e="T43" id="Seg_719" s="T42">conj</ta>
            <ta e="T44" id="Seg_720" s="T43">dempro-n:case</ta>
            <ta e="T45" id="Seg_721" s="T44">v-v&gt;v-v:pn</ta>
            <ta e="T46" id="Seg_722" s="T45">que-n:case</ta>
            <ta e="T47" id="Seg_723" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_724" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_725" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_726" s="T49">conj</ta>
            <ta e="T51" id="Seg_727" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_728" s="T51">ptcl</ta>
            <ta e="T55" id="Seg_729" s="T54">que-n:case=ptcl</ta>
            <ta e="T56" id="Seg_730" s="T55">aux-v:mood.pn</ta>
            <ta e="T57" id="Seg_731" s="T56">v-v:mood.pn</ta>
            <ta e="T58" id="Seg_732" s="T57">dempro-n:case</ta>
            <ta e="T59" id="Seg_733" s="T58">conj</ta>
            <ta e="T60" id="Seg_734" s="T59">pers</ta>
            <ta e="T61" id="Seg_735" s="T60">v-v&gt;v-v:pn</ta>
            <ta e="T62" id="Seg_736" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_737" s="T62">adj-n:case</ta>
            <ta e="T65" id="Seg_738" s="T64">adv</ta>
            <ta e="T66" id="Seg_739" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_740" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_741" s="T67">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_742" s="T0">pers</ta>
            <ta e="T2" id="Seg_743" s="T1">adv</ta>
            <ta e="T3" id="Seg_744" s="T2">n</ta>
            <ta e="T4" id="Seg_745" s="T3">v</ta>
            <ta e="T5" id="Seg_746" s="T4">v</ta>
            <ta e="T6" id="Seg_747" s="T5">v</ta>
            <ta e="T7" id="Seg_748" s="T6">v</ta>
            <ta e="T8" id="Seg_749" s="T7">n</ta>
            <ta e="T9" id="Seg_750" s="T8">n</ta>
            <ta e="T10" id="Seg_751" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_752" s="T10">pers</ta>
            <ta e="T12" id="Seg_753" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_754" s="T12">n</ta>
            <ta e="T14" id="Seg_755" s="T13">v</ta>
            <ta e="T16" id="Seg_756" s="T14">conj</ta>
            <ta e="T17" id="Seg_757" s="T16">n</ta>
            <ta e="T18" id="Seg_758" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_759" s="T18">v</ta>
            <ta e="T20" id="Seg_760" s="T19">conj</ta>
            <ta e="T21" id="Seg_761" s="T20">pers</ta>
            <ta e="T22" id="Seg_762" s="T21">v</ta>
            <ta e="T23" id="Seg_763" s="T22">dempro</ta>
            <ta e="T25" id="Seg_764" s="T24">pers</ta>
            <ta e="T26" id="Seg_765" s="T25">n</ta>
            <ta e="T27" id="Seg_766" s="T26">v</ta>
            <ta e="T28" id="Seg_767" s="T27">adv</ta>
            <ta e="T29" id="Seg_768" s="T28">n</ta>
            <ta e="T30" id="Seg_769" s="T29">v</ta>
            <ta e="T31" id="Seg_770" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_771" s="T31">dempro</ta>
            <ta e="T33" id="Seg_772" s="T32">v</ta>
            <ta e="T34" id="Seg_773" s="T33">conj</ta>
            <ta e="T35" id="Seg_774" s="T34">dempro</ta>
            <ta e="T36" id="Seg_775" s="T35">v</ta>
            <ta e="T37" id="Seg_776" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_777" s="T37">n</ta>
            <ta e="T39" id="Seg_778" s="T38">que</ta>
            <ta e="T40" id="Seg_779" s="T39">adv</ta>
            <ta e="T41" id="Seg_780" s="T40">pers</ta>
            <ta e="T42" id="Seg_781" s="T41">n</ta>
            <ta e="T43" id="Seg_782" s="T42">conj</ta>
            <ta e="T44" id="Seg_783" s="T43">dempro</ta>
            <ta e="T45" id="Seg_784" s="T44">v</ta>
            <ta e="T46" id="Seg_785" s="T45">que</ta>
            <ta e="T47" id="Seg_786" s="T46">n</ta>
            <ta e="T48" id="Seg_787" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_788" s="T48">n</ta>
            <ta e="T50" id="Seg_789" s="T49">conj</ta>
            <ta e="T51" id="Seg_790" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_791" s="T51">ptcl</ta>
            <ta e="T55" id="Seg_792" s="T54">que</ta>
            <ta e="T56" id="Seg_793" s="T55">aux</ta>
            <ta e="T57" id="Seg_794" s="T56">v</ta>
            <ta e="T58" id="Seg_795" s="T57">dempro</ta>
            <ta e="T59" id="Seg_796" s="T58">conj</ta>
            <ta e="T60" id="Seg_797" s="T59">pers</ta>
            <ta e="T61" id="Seg_798" s="T60">v</ta>
            <ta e="T62" id="Seg_799" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_800" s="T62">adj</ta>
            <ta e="T65" id="Seg_801" s="T64">adv</ta>
            <ta e="T66" id="Seg_802" s="T65">v</ta>
            <ta e="T67" id="Seg_803" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_804" s="T67">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_805" s="T0">pro.h:E</ta>
            <ta e="T2" id="Seg_806" s="T1">adv:Time</ta>
            <ta e="T3" id="Seg_807" s="T2">np:Th</ta>
            <ta e="T6" id="Seg_808" s="T5">0.1.h:E</ta>
            <ta e="T7" id="Seg_809" s="T6">0.1.h:E</ta>
            <ta e="T9" id="Seg_810" s="T8">np.h:A</ta>
            <ta e="T11" id="Seg_811" s="T10">pro.h:E</ta>
            <ta e="T13" id="Seg_812" s="T12">np:Ins</ta>
            <ta e="T17" id="Seg_813" s="T16">np:Ins</ta>
            <ta e="T21" id="Seg_814" s="T20">pro.h:A</ta>
            <ta e="T23" id="Seg_815" s="T22">pro.h:A</ta>
            <ta e="T25" id="Seg_816" s="T24">pro.h:Th</ta>
            <ta e="T26" id="Seg_817" s="T25">np:Ins</ta>
            <ta e="T28" id="Seg_818" s="T27">adv:Time</ta>
            <ta e="T29" id="Seg_819" s="T28">np.h:A</ta>
            <ta e="T32" id="Seg_820" s="T31">pro.h:E</ta>
            <ta e="T35" id="Seg_821" s="T34">pro.h:A</ta>
            <ta e="T40" id="Seg_822" s="T39">adv:L</ta>
            <ta e="T41" id="Seg_823" s="T40">pro.h:A</ta>
            <ta e="T42" id="Seg_824" s="T41">np.h:E</ta>
            <ta e="T44" id="Seg_825" s="T43">pro.h:A</ta>
            <ta e="T56" id="Seg_826" s="T55">0.2.h:A</ta>
            <ta e="T58" id="Seg_827" s="T57">pro.h:R</ta>
            <ta e="T60" id="Seg_828" s="T59">pro.h:A</ta>
            <ta e="T65" id="Seg_829" s="T64">adv:Time</ta>
            <ta e="T66" id="Seg_830" s="T65">0.1.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_831" s="T0">pro.h:S</ta>
            <ta e="T5" id="Seg_832" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_833" s="T5">v:pred 0.1.h:S</ta>
            <ta e="T7" id="Seg_834" s="T6">v:pred 0.1.h:S</ta>
            <ta e="T11" id="Seg_835" s="T10">pro.h:O</ta>
            <ta e="T12" id="Seg_836" s="T11">ptcl:pred</ta>
            <ta e="T21" id="Seg_837" s="T20">pro.h:S</ta>
            <ta e="T22" id="Seg_838" s="T21">v:pred</ta>
            <ta e="T23" id="Seg_839" s="T22">pro.h:S</ta>
            <ta e="T25" id="Seg_840" s="T24">pro.h:O</ta>
            <ta e="T27" id="Seg_841" s="T26">v:pred</ta>
            <ta e="T29" id="Seg_842" s="T28">np.h:S</ta>
            <ta e="T30" id="Seg_843" s="T29">v:pred</ta>
            <ta e="T31" id="Seg_844" s="T30">ptcl:pred</ta>
            <ta e="T32" id="Seg_845" s="T31">pro.h:O</ta>
            <ta e="T35" id="Seg_846" s="T34">pro.h:S</ta>
            <ta e="T36" id="Seg_847" s="T35">v:pred</ta>
            <ta e="T41" id="Seg_848" s="T40">pro.h:S</ta>
            <ta e="T42" id="Seg_849" s="T41">np.h:O</ta>
            <ta e="T44" id="Seg_850" s="T43">pro.h:S</ta>
            <ta e="T45" id="Seg_851" s="T44">v:pred</ta>
            <ta e="T48" id="Seg_852" s="T47">ptcl.neg</ta>
            <ta e="T51" id="Seg_853" s="T50">ptcl.neg</ta>
            <ta e="T52" id="Seg_854" s="T51">ptcl:pred</ta>
            <ta e="T56" id="Seg_855" s="T55">v:pred 0.2.h:S</ta>
            <ta e="T60" id="Seg_856" s="T59">pro.h:S</ta>
            <ta e="T61" id="Seg_857" s="T60">v:pred</ta>
            <ta e="T66" id="Seg_858" s="T65">v:pred 0.1.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T10" id="Seg_859" s="T9">TURK:disc</ta>
            <ta e="T12" id="Seg_860" s="T11">RUS:gram</ta>
            <ta e="T16" id="Seg_861" s="T14">RUS:gram</ta>
            <ta e="T18" id="Seg_862" s="T17">TURK:disc</ta>
            <ta e="T20" id="Seg_863" s="T19">RUS:gram</ta>
            <ta e="T31" id="Seg_864" s="T30">RUS:gram</ta>
            <ta e="T34" id="Seg_865" s="T33">RUS:gram</ta>
            <ta e="T37" id="Seg_866" s="T36">RUS:gram</ta>
            <ta e="T42" id="Seg_867" s="T41">TURK:core</ta>
            <ta e="T43" id="Seg_868" s="T42">RUS:gram</ta>
            <ta e="T47" id="Seg_869" s="T46">TURK:core</ta>
            <ta e="T49" id="Seg_870" s="T48">TURK:core</ta>
            <ta e="T50" id="Seg_871" s="T49">RUS:gram</ta>
            <ta e="T51" id="Seg_872" s="T50">RUS:gram</ta>
            <ta e="T52" id="Seg_873" s="T51">RUS:mod</ta>
            <ta e="T55" id="Seg_874" s="T54">TURK:gram(INDEF)</ta>
            <ta e="T59" id="Seg_875" s="T58">RUS:gram</ta>
            <ta e="T62" id="Seg_876" s="T61">TURK:disc</ta>
            <ta e="T67" id="Seg_877" s="T66">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_878" s="T0">Я вчера видела сон.</ta>
            <ta e="T7" id="Seg_879" s="T5">Я спала и видела.</ta>
            <ta e="T16" id="Seg_880" s="T7">Белая лошадаь стала бить меня ногами и…</ta>
            <ta e="T19" id="Seg_881" s="T16">И зубами кусать.</ta>
            <ta e="T22" id="Seg_882" s="T19">А я кричу.</ta>
            <ta e="T27" id="Seg_883" s="T22">Она (держала?) меня ногами.</ta>
            <ta e="T33" id="Seg_884" s="T27">Потом мужчина пришел и стал ее бить.</ta>
            <ta e="T38" id="Seg_885" s="T33">А она говорит, как человек:</ta>
            <ta e="T42" id="Seg_886" s="T38">"Зачем родственника [бьешь]? (?)"</ta>
            <ta e="T49" id="Seg_887" s="T42">А тот говорит: "Почему родственник, не родственник.</ta>
            <ta e="T54" id="Seg_888" s="T49">А не надо…</ta>
            <ta e="T58" id="Seg_889" s="T54">Не делай ей ничего".</ta>
            <ta e="T62" id="Seg_890" s="T58">А я иду.</ta>
            <ta e="T64" id="Seg_891" s="T62">Грязная везде.</ta>
            <ta e="T67" id="Seg_892" s="T64">Потом я проснулась.</ta>
            <ta e="T68" id="Seg_893" s="T67">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_894" s="T0">I had a dream yesterday, I saw.</ta>
            <ta e="T7" id="Seg_895" s="T5">I slept, I saw.</ta>
            <ta e="T16" id="Seg_896" s="T7">A white horse started to hit me with [its] feet and…</ta>
            <ta e="T19" id="Seg_897" s="T16">To bite with [its] teeth.</ta>
            <ta e="T22" id="Seg_898" s="T19">But I yelled.</ta>
            <ta e="T27" id="Seg_899" s="T22">It was (keeping?) me with its feet.</ta>
            <ta e="T33" id="Seg_900" s="T27">Then a man came and started to beat it.</ta>
            <ta e="T38" id="Seg_901" s="T33">But it says, like a human [= in a human voice]&gt;</ta>
            <ta e="T42" id="Seg_902" s="T38">"Why [do you beat] your relative? (?)"</ta>
            <ta e="T49" id="Seg_903" s="T42">And he said: "Why relative, not a relative.</ta>
            <ta e="T54" id="Seg_904" s="T49">But don't…</ta>
            <ta e="T58" id="Seg_905" s="T54">Don't do anything to her."</ta>
            <ta e="T62" id="Seg_906" s="T58">But I was going.</ta>
            <ta e="T64" id="Seg_907" s="T62">Dirt everywhere.</ta>
            <ta e="T67" id="Seg_908" s="T64">Then I woke up.</ta>
            <ta e="T68" id="Seg_909" s="T67">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_910" s="T0">Ich hatte gestern einen Traum, ich sah.</ta>
            <ta e="T7" id="Seg_911" s="T5">Ich schlief, ich sah.</ta>
            <ta e="T16" id="Seg_912" s="T7">Ein weißes Pferd fing an, mich mit [seinen] Füßen zu treten und…</ta>
            <ta e="T19" id="Seg_913" s="T16">Mit [seinen] Zähnen zu beißen.</ta>
            <ta e="T22" id="Seg_914" s="T19">Aber ich schrie.</ta>
            <ta e="T27" id="Seg_915" s="T22">Es (holte?) mich mit seinen Füßen.</ta>
            <ta e="T33" id="Seg_916" s="T27">Dann kam ein Mann und fing an, es zu schlagen.</ta>
            <ta e="T38" id="Seg_917" s="T33">Aber es sagt, wie ein Mensch [= mit menschlicher Stimme]:</ta>
            <ta e="T42" id="Seg_918" s="T38">"Warum [schlägst du] deinen Verwandten? (?)"</ta>
            <ta e="T54" id="Seg_919" s="T49">Aber nicht…</ta>
            <ta e="T58" id="Seg_920" s="T54">Tu ihr nichts."</ta>
            <ta e="T62" id="Seg_921" s="T58">Aber ich ging.</ta>
            <ta e="T64" id="Seg_922" s="T62">Schmutzig überall.</ta>
            <ta e="T67" id="Seg_923" s="T64">Dann wachte ich auf.</ta>
            <ta e="T68" id="Seg_924" s="T67">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T42" id="Seg_925" s="T38">[GVY:] ambi?</ta>
            <ta e="T64" id="Seg_926" s="T62">[GVY:] balkaš</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
