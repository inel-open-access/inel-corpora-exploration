<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID383605F5-7DFC-D779-4D88-61B9997C10B6">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_TripToAginskoe_nar.wav" />
         <referenced-file url="PKZ_196X_TripToAginskoe_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\nar\PKZ_196X_TripToAginskoe_nar\PKZ_196X_TripToAginskoe_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">235</ud-information>
            <ud-information attribute-name="# HIAT:w">154</ud-information>
            <ud-information attribute-name="# e">154</ud-information>
            <ud-information attribute-name="# HIAT:u">40</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.091" type="appl" />
         <tli id="T1" time="0.727" type="appl" />
         <tli id="T2" time="1.363" type="appl" />
         <tli id="T3" time="1.999" type="appl" />
         <tli id="T4" time="3.259927415763693" />
         <tli id="T5" time="4.23" type="appl" />
         <tli id="T6" time="5.194" type="appl" />
         <tli id="T154" time="5.605866054922328" type="intp" />
         <tli id="T7" time="6.223665137305819" />
         <tli id="T8" time="6.843" type="appl" />
         <tli id="T9" time="7.528" type="appl" />
         <tli id="T10" time="8.214" type="appl" />
         <tli id="T11" time="8.9" type="appl" />
         <tli id="T12" time="9.844" type="appl" />
         <tli id="T13" time="10.676" type="appl" />
         <tli id="T14" time="11.508" type="appl" />
         <tli id="T15" time="12.96637796249567" />
         <tli id="T16" time="13.604" type="appl" />
         <tli id="T17" time="14.218" type="appl" />
         <tli id="T18" time="14.831" type="appl" />
         <tli id="T19" time="15.445" type="appl" />
         <tli id="T20" time="16.059" type="appl" />
         <tli id="T21" time="16.922" type="appl" />
         <tli id="T22" time="17.647" type="appl" />
         <tli id="T23" time="18.373" type="appl" />
         <tli id="T24" time="19.099" type="appl" />
         <tli id="T25" time="19.824" type="appl" />
         <tli id="T26" time="20.55" type="appl" />
         <tli id="T27" time="21.333" type="appl" />
         <tli id="T28" time="21.997" type="appl" />
         <tli id="T29" time="22.662" type="appl" />
         <tli id="T30" time="23.326" type="appl" />
         <tli id="T31" time="23.991" type="appl" />
         <tli id="T32" time="24.892" type="appl" />
         <tli id="T33" time="25.664" type="appl" />
         <tli id="T34" time="26.437" type="appl" />
         <tli id="T35" time="27.209" type="appl" />
         <tli id="T36" time="28.932689129681854" />
         <tli id="T37" time="30.015" type="appl" />
         <tli id="T38" time="30.787" type="appl" />
         <tli id="T39" time="31.56" type="appl" />
         <tli id="T40" time="32.332" type="appl" />
         <tli id="T41" time="33.105" type="appl" />
         <tli id="T42" time="33.878" type="appl" />
         <tli id="T43" time="34.65" type="appl" />
         <tli id="T44" time="35.423" type="appl" />
         <tli id="T45" time="36.324" type="appl" />
         <tli id="T46" time="36.878" type="appl" />
         <tli id="T47" time="37.431" type="appl" />
         <tli id="T48" time="38.09915169956953" />
         <tli id="T49" time="38.874" type="appl" />
         <tli id="T50" time="39.764" type="appl" />
         <tli id="T51" time="40.653" type="appl" />
         <tli id="T52" time="41.542" type="appl" />
         <tli id="T53" time="42.432" type="appl" />
         <tli id="T54" time="44.945665924496566" />
         <tli id="T55" time="45.625" type="appl" />
         <tli id="T56" time="46.192" type="appl" />
         <tli id="T57" time="46.76" type="appl" />
         <tli id="T58" time="47.328" type="appl" />
         <tli id="T59" time="47.896" type="appl" />
         <tli id="T60" time="48.463" type="appl" />
         <tli id="T61" time="49.031" type="appl" />
         <tli id="T62" time="50.0988845183316" />
         <tli id="T63" time="50.747" type="appl" />
         <tli id="T64" time="51.322" type="appl" />
         <tli id="T65" time="51.898" type="appl" />
         <tli id="T66" time="52.473" type="appl" />
         <tli id="T67" time="53.40547756172381" />
         <tli id="T68" time="54.444" type="appl" />
         <tli id="T69" time="55.492" type="appl" />
         <tli id="T70" time="56.545668060168346" />
         <tli id="T71" time="57.088" type="appl" />
         <tli id="T72" time="57.638" type="appl" />
         <tli id="T73" time="58.34699514004209" />
         <tli id="T74" time="60.238658750185536" />
         <tli id="T75" time="61.126" type="appl" />
         <tli id="T76" time="61.948" type="appl" />
         <tli id="T77" time="63.218592400178125" />
         <tli id="T78" time="64.171" type="appl" />
         <tli id="T79" time="64.983" type="appl" />
         <tli id="T80" time="65.795" type="appl" />
         <tli id="T81" time="66.84517831873733" />
         <tli id="T82" time="67.711" type="appl" />
         <tli id="T83" time="68.567" type="appl" />
         <tli id="T84" time="69.423" type="appl" />
         <tli id="T85" time="70.28" type="appl" />
         <tli id="T86" time="71.136" type="appl" />
         <tli id="T87" time="71.992" type="appl" />
         <tli id="T88" time="72.848" type="appl" />
         <tli id="T89" time="73.704" type="appl" />
         <tli id="T90" time="74.744" type="appl" />
         <tli id="T91" time="76.42496501904904" />
         <tli id="T92" time="77.364" type="appl" />
         <tli id="T93" time="78.048" type="appl" />
         <tli id="T94" time="78.732" type="appl" />
         <tli id="T95" time="79.416" type="appl" />
         <tli id="T96" time="80.341" type="appl" />
         <tli id="T97" time="81.78484567809609" />
         <tli id="T98" time="83.134" type="appl" />
         <tli id="T99" time="84.21" type="appl" />
         <tli id="T100" time="85.287" type="appl" />
         <tli id="T101" time="86.364" type="appl" />
         <tli id="T102" time="86.982" type="appl" />
         <tli id="T103" time="87.6" type="appl" />
         <tli id="T104" time="88.893" type="appl" />
         <tli id="T105" time="90.185" type="appl" />
         <tli id="T106" time="91.478" type="appl" />
         <tli id="T107" time="93.04459497303448" />
         <tli id="T108" time="94.078" type="appl" />
         <tli id="T109" time="94.814" type="appl" />
         <tli id="T110" time="95.71732843177331" />
         <tli id="T111" time="96.371" type="appl" />
         <tli id="T112" time="97.117" type="appl" />
         <tli id="T113" time="100.03110608084707" />
         <tli id="T114" time="101.073" type="appl" />
         <tli id="T115" time="101.912" type="appl" />
         <tli id="T116" time="102.752" type="appl" />
         <tli id="T117" time="103.592" type="appl" />
         <tli id="T118" time="104.19" type="appl" />
         <tli id="T119" time="104.787" type="appl" />
         <tli id="T120" time="105.384" type="appl" />
         <tli id="T121" time="105.982" type="appl" />
         <tli id="T122" time="106.816" type="appl" />
         <tli id="T123" time="107.651" type="appl" />
         <tli id="T124" time="108.486" type="appl" />
         <tli id="T125" time="109.32" type="appl" />
         <tli id="T126" time="110.154" type="appl" />
         <tli id="T127" time="110.989" type="appl" />
         <tli id="T128" time="112.141" type="appl" />
         <tli id="T129" time="113.293" type="appl" />
         <tli id="T130" time="113.984" type="appl" />
         <tli id="T131" time="114.675" type="appl" />
         <tli id="T132" time="115.366" type="appl" />
         <tli id="T133" time="116.47740656078373" />
         <tli id="T134" time="117.4" type="appl" />
         <tli id="T135" time="118.162" type="appl" />
         <tli id="T136" time="118.924" type="appl" />
         <tli id="T137" time="119.91732996882885" />
         <tli id="T138" time="120.39" type="appl" />
         <tli id="T139" time="120.852" type="appl" />
         <tli id="T140" time="121.315" type="appl" />
         <tli id="T141" time="121.777" type="appl" />
         <tli id="T142" time="122.239" type="appl" />
         <tli id="T143" time="123.041" type="appl" />
         <tli id="T144" time="123.843" type="appl" />
         <tli id="T145" time="124.645" type="appl" />
         <tli id="T146" time="125.447" type="appl" />
         <tli id="T147" time="126.74384463905794" />
         <tli id="T148" time="127.957" type="appl" />
         <tli id="T149" time="128.946" type="appl" />
         <tli id="T150" time="130.11" type="appl" />
         <tli id="T151" time="131.87706367819507" />
         <tli id="T152" time="133.347" type="appl" />
         <tli id="T153" time="134.737" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T153" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Măn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ibiem</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">Kazan</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">turagən</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Dĭn</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">kudajdə</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_25" n="HIAT:w" s="T6">numan</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T154">üzübiem</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_32" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_34" n="HIAT:w" s="T7">Dĭgəttə</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_36" n="HIAT:ip">(</nts>
                  <ts e="T9" id="Seg_38" n="HIAT:w" s="T8">ka-</ts>
                  <nts id="Seg_39" n="HIAT:ip">)</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_42" n="HIAT:w" s="T9">kambiam</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_45" n="HIAT:w" s="T10">maʔnʼi</ts>
                  <nts id="Seg_46" n="HIAT:ip">.</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_49" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_51" n="HIAT:w" s="T11">Šobiam</ts>
                  <nts id="Seg_52" n="HIAT:ip">,</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_55" n="HIAT:w" s="T12">šobiam</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_57" n="HIAT:ip">(</nts>
                  <ts e="T14" id="Seg_59" n="HIAT:w" s="T13">aktʼi=</ts>
                  <nts id="Seg_60" n="HIAT:ip">)</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_63" n="HIAT:w" s="T14">aktʼigən</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_67" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_69" n="HIAT:w" s="T15">Girgitdə</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_72" n="HIAT:w" s="T16">ej</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_75" n="HIAT:w" s="T17">bĭdəbi</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_78" n="HIAT:w" s="T18">măna</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_81" n="HIAT:w" s="T19">mašina</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_85" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_87" n="HIAT:w" s="T20">Dĭgəttə</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_89" n="HIAT:ip">(</nts>
                  <ts e="T22" id="Seg_91" n="HIAT:w" s="T21">šu-</ts>
                  <nts id="Seg_92" n="HIAT:ip">)</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_95" n="HIAT:w" s="T22">surno</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_98" n="HIAT:w" s="T23">kambi</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_101" n="HIAT:w" s="T24">i</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_104" n="HIAT:w" s="T25">beržə</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_108" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_110" n="HIAT:w" s="T26">A</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_113" n="HIAT:w" s="T27">pimnal</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_116" n="HIAT:w" s="T28">ej</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_119" n="HIAT:w" s="T29">kumbi</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_122" n="HIAT:w" s="T30">beržə</ts>
                  <nts id="Seg_123" n="HIAT:ip">.</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_126" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_128" n="HIAT:w" s="T31">Măn</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_131" n="HIAT:w" s="T32">šobiam</ts>
                  <nts id="Seg_132" n="HIAT:ip">,</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_135" n="HIAT:w" s="T33">šobiam</ts>
                  <nts id="Seg_136" n="HIAT:ip">,</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_139" n="HIAT:w" s="T34">nulaʔbə</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_142" n="HIAT:w" s="T35">mašina</ts>
                  <nts id="Seg_143" n="HIAT:ip">.</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_146" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_148" n="HIAT:w" s="T36">Dĭgəttə</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_151" n="HIAT:w" s="T37">aktʼim</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_154" n="HIAT:w" s="T38">dʼürlaʔbiam</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_157" n="HIAT:w" s="T39">i</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_160" n="HIAT:w" s="T40">kalla</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_163" n="HIAT:w" s="T41">dʼürbiem</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_165" n="HIAT:ip">(</nts>
                  <ts e="T43" id="Seg_167" n="HIAT:w" s="T42">k</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_170" n="HIAT:w" s="T43">uražanəm</ts>
                  <nts id="Seg_171" n="HIAT:ip">)</nts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_175" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_177" n="HIAT:w" s="T44">Dibər</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_180" n="HIAT:w" s="T45">šobiem</ts>
                  <nts id="Seg_181" n="HIAT:ip">,</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_184" n="HIAT:w" s="T46">dĭn</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_187" n="HIAT:w" s="T47">šaːbiam</ts>
                  <nts id="Seg_188" n="HIAT:ip">.</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_191" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_193" n="HIAT:w" s="T48">Dĭn</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_196" n="HIAT:w" s="T49">bar</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_199" n="HIAT:w" s="T50">nüjleʔbəʔjə</ts>
                  <nts id="Seg_200" n="HIAT:ip">,</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_203" n="HIAT:w" s="T51">ara</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_206" n="HIAT:w" s="T52">bĭtleʔbəʔjə</ts>
                  <nts id="Seg_207" n="HIAT:ip">,</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_210" n="HIAT:w" s="T53">kudonzlaʔbəʔjə</ts>
                  <nts id="Seg_211" n="HIAT:ip">.</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_214" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_216" n="HIAT:w" s="T54">Dĭgəttə</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_219" n="HIAT:w" s="T55">kunolbiʔi</ts>
                  <nts id="Seg_220" n="HIAT:ip">,</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_223" n="HIAT:w" s="T56">a</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_226" n="HIAT:w" s="T57">măn</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_229" n="HIAT:w" s="T58">erte</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_232" n="HIAT:w" s="T59">uʔbdəbiam</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_235" n="HIAT:w" s="T60">i</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_238" n="HIAT:w" s="T61">kambiam</ts>
                  <nts id="Seg_239" n="HIAT:ip">.</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_242" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_244" n="HIAT:w" s="T62">Šobiam</ts>
                  <nts id="Seg_245" n="HIAT:ip">,</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_248" n="HIAT:w" s="T63">šobiam</ts>
                  <nts id="Seg_249" n="HIAT:ip">,</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_252" n="HIAT:w" s="T64">măn</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_255" n="HIAT:w" s="T65">ipek</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_258" n="HIAT:w" s="T66">ibi</ts>
                  <nts id="Seg_259" n="HIAT:ip">.</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_262" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_264" n="HIAT:w" s="T67">Dʼagan</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_267" n="HIAT:w" s="T68">toːndə</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_270" n="HIAT:w" s="T69">ambiam</ts>
                  <nts id="Seg_271" n="HIAT:ip">.</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_274" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_276" n="HIAT:w" s="T70">Dĭgəttə</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_279" n="HIAT:w" s="T71">bazoʔ</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_282" n="HIAT:w" s="T72">kambiam</ts>
                  <nts id="Seg_283" n="HIAT:ip">.</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_286" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_288" n="HIAT:w" s="T73">Šonəgam</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_292" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_294" n="HIAT:w" s="T74">Kuliom:</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_297" n="HIAT:w" s="T75">turaʔi</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_300" n="HIAT:w" s="T76">nulaʔbəʔjə</ts>
                  <nts id="Seg_301" n="HIAT:ip">.</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_304" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_306" n="HIAT:w" s="T77">Tüjö</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_309" n="HIAT:w" s="T78">dibər</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_312" n="HIAT:w" s="T79">turazaŋdə</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_315" n="HIAT:w" s="T80">kallam</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_319" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_321" n="HIAT:w" s="T81">Dĭgəttə</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_324" n="HIAT:w" s="T82">kandəgam</ts>
                  <nts id="Seg_325" n="HIAT:ip">,</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_328" n="HIAT:w" s="T83">kandəgam</ts>
                  <nts id="Seg_329" n="HIAT:ip">,</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_332" n="HIAT:w" s="T84">tenebiem</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_335" n="HIAT:w" s="T85">aktʼi</ts>
                  <nts id="Seg_336" n="HIAT:ip">,</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_339" n="HIAT:w" s="T86">dĭ</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_342" n="HIAT:w" s="T87">döbər</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_345" n="HIAT:w" s="T88">kalləj</ts>
                  <nts id="Seg_346" n="HIAT:ip">.</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_349" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_351" n="HIAT:w" s="T89">Dĭgəttə</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_354" n="HIAT:w" s="T90">kuliom</ts>
                  <nts id="Seg_355" n="HIAT:ip">.</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_358" n="HIAT:u" s="T91">
                  <nts id="Seg_359" n="HIAT:ip">(</nts>
                  <ts e="T92" id="Seg_361" n="HIAT:w" s="T91">Ej</ts>
                  <nts id="Seg_362" n="HIAT:ip">)</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_365" n="HIAT:w" s="T92">Ej</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_368" n="HIAT:w" s="T93">dĭʔnə</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_371" n="HIAT:w" s="T94">kallaʔbəm</ts>
                  <nts id="Seg_372" n="HIAT:ip">.</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_375" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_377" n="HIAT:w" s="T95">Dĭgəttə</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_380" n="HIAT:w" s="T96">parluʔbiam</ts>
                  <nts id="Seg_381" n="HIAT:ip">.</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_384" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_386" n="HIAT:w" s="T97">I</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_389" n="HIAT:w" s="T98">Malinovskoe</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_392" n="HIAT:w" s="T99">aktʼinə</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_395" n="HIAT:w" s="T100">šobiam</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_399" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_401" n="HIAT:w" s="T101">Dĭgəttə</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_404" n="HIAT:w" s="T102">šobiam</ts>
                  <nts id="Seg_405" n="HIAT:ip">.</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_408" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_410" n="HIAT:w" s="T103">Dĭn</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_412" n="HIAT:ip">(</nts>
                  <ts e="T105" id="Seg_414" n="HIAT:w" s="T104">budəj</ts>
                  <nts id="Seg_415" n="HIAT:ip">)</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_418" n="HIAT:w" s="T105">budəj</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_421" n="HIAT:w" s="T106">kuʔpiʔi</ts>
                  <nts id="Seg_422" n="HIAT:ip">.</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_425" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_427" n="HIAT:w" s="T107">Măn</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_430" n="HIAT:w" s="T108">kambiam</ts>
                  <nts id="Seg_431" n="HIAT:ip">.</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_434" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_436" n="HIAT:w" s="T109">Šobiam</ts>
                  <nts id="Seg_437" n="HIAT:ip">.</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_440" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_442" n="HIAT:w" s="T110">Dĭgəttə</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_445" n="HIAT:w" s="T111">šobiam</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_448" n="HIAT:w" s="T112">maːʔnʼi</ts>
                  <nts id="Seg_449" n="HIAT:ip">.</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_452" n="HIAT:u" s="T113">
                  <nts id="Seg_453" n="HIAT:ip">(</nts>
                  <ts e="T114" id="Seg_455" n="HIAT:w" s="T113">Biəʔ=</ts>
                  <nts id="Seg_456" n="HIAT:ip">)</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_459" n="HIAT:w" s="T114">Biəʔ</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_462" n="HIAT:w" s="T115">časɨʔi</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_465" n="HIAT:w" s="T116">ibiʔi</ts>
                  <nts id="Seg_466" n="HIAT:ip">.</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_469" n="HIAT:u" s="T117">
                  <nts id="Seg_470" n="HIAT:ip">(</nts>
                  <ts e="T118" id="Seg_472" n="HIAT:w" s="T117">Do-</ts>
                  <nts id="Seg_473" n="HIAT:ip">)</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_476" n="HIAT:w" s="T118">Dön</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_479" n="HIAT:w" s="T119">băzlaʔbəʔjə</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_481" n="HIAT:ip">(</nts>
                  <ts e="T121" id="Seg_483" n="HIAT:w" s="T120">sipara</ts>
                  <nts id="Seg_484" n="HIAT:ip">)</nts>
                  <nts id="Seg_485" n="HIAT:ip">.</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_488" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_490" n="HIAT:w" s="T121">Dĭgəttə</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_493" n="HIAT:w" s="T122">šobiam</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_496" n="HIAT:w" s="T123">maʔnʼi</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_499" n="HIAT:w" s="T124">i</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_502" n="HIAT:w" s="T125">ambiam</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_505" n="HIAT:w" s="T126">ipek</ts>
                  <nts id="Seg_506" n="HIAT:ip">.</nts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_509" n="HIAT:u" s="T127">
                  <nts id="Seg_510" n="HIAT:ip">(</nts>
                  <ts e="T128" id="Seg_512" n="HIAT:w" s="T127">Šalaʔi-</ts>
                  <nts id="Seg_513" n="HIAT:ip">)</nts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_516" n="HIAT:w" s="T128">Kunolbiam</ts>
                  <nts id="Seg_517" n="HIAT:ip">.</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_520" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_522" n="HIAT:w" s="T129">Dĭgəttə</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_525" n="HIAT:w" s="T130">kambiam</ts>
                  <nts id="Seg_526" n="HIAT:ip">,</nts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_529" n="HIAT:w" s="T131">dʼü</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_531" n="HIAT:ip">(</nts>
                  <ts e="T133" id="Seg_533" n="HIAT:w" s="T132">dʼazerbiam</ts>
                  <nts id="Seg_534" n="HIAT:ip">)</nts>
                  <nts id="Seg_535" n="HIAT:ip">.</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_538" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_540" n="HIAT:w" s="T133">I</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_543" n="HIAT:w" s="T134">amnolbiam</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_545" n="HIAT:ip">(</nts>
                  <ts e="T136" id="Seg_547" n="HIAT:w" s="T135">tol-</ts>
                  <nts id="Seg_548" n="HIAT:ip">)</nts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_551" n="HIAT:w" s="T136">kapustam</ts>
                  <nts id="Seg_552" n="HIAT:ip">.</nts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_555" n="HIAT:u" s="T137">
                  <nts id="Seg_556" n="HIAT:ip">(</nts>
                  <ts e="T138" id="Seg_558" n="HIAT:w" s="T137">I</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_561" n="HIAT:w" s="T138">bü=</ts>
                  <nts id="Seg_562" n="HIAT:ip">)</nts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_565" n="HIAT:w" s="T139">I</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_568" n="HIAT:w" s="T140">bü</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_571" n="HIAT:w" s="T141">kămnəbiam</ts>
                  <nts id="Seg_572" n="HIAT:ip">.</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_575" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_577" n="HIAT:w" s="T142">Dĭgəttə</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_580" n="HIAT:w" s="T143">nüdʼin</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_583" n="HIAT:w" s="T144">kunollaʔ</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_585" n="HIAT:ip">(</nts>
                  <ts e="T146" id="Seg_587" n="HIAT:w" s="T145">iʔbə-</ts>
                  <nts id="Seg_588" n="HIAT:ip">)</nts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_591" n="HIAT:w" s="T146">iʔbiem</ts>
                  <nts id="Seg_592" n="HIAT:ip">.</nts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_595" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_597" n="HIAT:w" s="T147">Karəldʼan</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_600" n="HIAT:w" s="T148">uʔbdəbiam</ts>
                  <nts id="Seg_601" n="HIAT:ip">.</nts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_604" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_606" n="HIAT:w" s="T149">Brʼukva</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_609" n="HIAT:w" s="T150">amnolbiam</ts>
                  <nts id="Seg_610" n="HIAT:ip">.</nts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_613" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_615" n="HIAT:w" s="T151">Dĭgəttə</ts>
                  <nts id="Seg_616" n="HIAT:ip">.</nts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_619" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_621" n="HIAT:w" s="T152">Kabarləj</ts>
                  <nts id="Seg_622" n="HIAT:ip">.</nts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T153" id="Seg_624" n="sc" s="T0">
               <ts e="T1" id="Seg_626" n="e" s="T0">Măn </ts>
               <ts e="T2" id="Seg_628" n="e" s="T1">ibiem </ts>
               <ts e="T3" id="Seg_630" n="e" s="T2">Kazan </ts>
               <ts e="T4" id="Seg_632" n="e" s="T3">turagən. </ts>
               <ts e="T5" id="Seg_634" n="e" s="T4">Dĭn </ts>
               <ts e="T6" id="Seg_636" n="e" s="T5">kudajdə </ts>
               <ts e="T154" id="Seg_638" n="e" s="T6">numan </ts>
               <ts e="T7" id="Seg_640" n="e" s="T154">üzübiem. </ts>
               <ts e="T8" id="Seg_642" n="e" s="T7">Dĭgəttə </ts>
               <ts e="T9" id="Seg_644" n="e" s="T8">(ka-) </ts>
               <ts e="T10" id="Seg_646" n="e" s="T9">kambiam </ts>
               <ts e="T11" id="Seg_648" n="e" s="T10">maʔnʼi. </ts>
               <ts e="T12" id="Seg_650" n="e" s="T11">Šobiam, </ts>
               <ts e="T13" id="Seg_652" n="e" s="T12">šobiam </ts>
               <ts e="T14" id="Seg_654" n="e" s="T13">(aktʼi=) </ts>
               <ts e="T15" id="Seg_656" n="e" s="T14">aktʼigən. </ts>
               <ts e="T16" id="Seg_658" n="e" s="T15">Girgitdə </ts>
               <ts e="T17" id="Seg_660" n="e" s="T16">ej </ts>
               <ts e="T18" id="Seg_662" n="e" s="T17">bĭdəbi </ts>
               <ts e="T19" id="Seg_664" n="e" s="T18">măna </ts>
               <ts e="T20" id="Seg_666" n="e" s="T19">mašina. </ts>
               <ts e="T21" id="Seg_668" n="e" s="T20">Dĭgəttə </ts>
               <ts e="T22" id="Seg_670" n="e" s="T21">(šu-) </ts>
               <ts e="T23" id="Seg_672" n="e" s="T22">surno </ts>
               <ts e="T24" id="Seg_674" n="e" s="T23">kambi </ts>
               <ts e="T25" id="Seg_676" n="e" s="T24">i </ts>
               <ts e="T26" id="Seg_678" n="e" s="T25">beržə. </ts>
               <ts e="T27" id="Seg_680" n="e" s="T26">A </ts>
               <ts e="T28" id="Seg_682" n="e" s="T27">pimnal </ts>
               <ts e="T29" id="Seg_684" n="e" s="T28">ej </ts>
               <ts e="T30" id="Seg_686" n="e" s="T29">kumbi </ts>
               <ts e="T31" id="Seg_688" n="e" s="T30">beržə. </ts>
               <ts e="T32" id="Seg_690" n="e" s="T31">Măn </ts>
               <ts e="T33" id="Seg_692" n="e" s="T32">šobiam, </ts>
               <ts e="T34" id="Seg_694" n="e" s="T33">šobiam, </ts>
               <ts e="T35" id="Seg_696" n="e" s="T34">nulaʔbə </ts>
               <ts e="T36" id="Seg_698" n="e" s="T35">mašina. </ts>
               <ts e="T37" id="Seg_700" n="e" s="T36">Dĭgəttə </ts>
               <ts e="T38" id="Seg_702" n="e" s="T37">aktʼim </ts>
               <ts e="T39" id="Seg_704" n="e" s="T38">dʼürlaʔbiam </ts>
               <ts e="T40" id="Seg_706" n="e" s="T39">i </ts>
               <ts e="T41" id="Seg_708" n="e" s="T40">kalla </ts>
               <ts e="T42" id="Seg_710" n="e" s="T41">dʼürbiem </ts>
               <ts e="T43" id="Seg_712" n="e" s="T42">(k </ts>
               <ts e="T44" id="Seg_714" n="e" s="T43">uražanəm). </ts>
               <ts e="T45" id="Seg_716" n="e" s="T44">Dibər </ts>
               <ts e="T46" id="Seg_718" n="e" s="T45">šobiem, </ts>
               <ts e="T47" id="Seg_720" n="e" s="T46">dĭn </ts>
               <ts e="T48" id="Seg_722" n="e" s="T47">šaːbiam. </ts>
               <ts e="T49" id="Seg_724" n="e" s="T48">Dĭn </ts>
               <ts e="T50" id="Seg_726" n="e" s="T49">bar </ts>
               <ts e="T51" id="Seg_728" n="e" s="T50">nüjleʔbəʔjə, </ts>
               <ts e="T52" id="Seg_730" n="e" s="T51">ara </ts>
               <ts e="T53" id="Seg_732" n="e" s="T52">bĭtleʔbəʔjə, </ts>
               <ts e="T54" id="Seg_734" n="e" s="T53">kudonzlaʔbəʔjə. </ts>
               <ts e="T55" id="Seg_736" n="e" s="T54">Dĭgəttə </ts>
               <ts e="T56" id="Seg_738" n="e" s="T55">kunolbiʔi, </ts>
               <ts e="T57" id="Seg_740" n="e" s="T56">a </ts>
               <ts e="T58" id="Seg_742" n="e" s="T57">măn </ts>
               <ts e="T59" id="Seg_744" n="e" s="T58">erte </ts>
               <ts e="T60" id="Seg_746" n="e" s="T59">uʔbdəbiam </ts>
               <ts e="T61" id="Seg_748" n="e" s="T60">i </ts>
               <ts e="T62" id="Seg_750" n="e" s="T61">kambiam. </ts>
               <ts e="T63" id="Seg_752" n="e" s="T62">Šobiam, </ts>
               <ts e="T64" id="Seg_754" n="e" s="T63">šobiam, </ts>
               <ts e="T65" id="Seg_756" n="e" s="T64">măn </ts>
               <ts e="T66" id="Seg_758" n="e" s="T65">ipek </ts>
               <ts e="T67" id="Seg_760" n="e" s="T66">ibi. </ts>
               <ts e="T68" id="Seg_762" n="e" s="T67">Dʼagan </ts>
               <ts e="T69" id="Seg_764" n="e" s="T68">toːndə </ts>
               <ts e="T70" id="Seg_766" n="e" s="T69">ambiam. </ts>
               <ts e="T71" id="Seg_768" n="e" s="T70">Dĭgəttə </ts>
               <ts e="T72" id="Seg_770" n="e" s="T71">bazoʔ </ts>
               <ts e="T73" id="Seg_772" n="e" s="T72">kambiam. </ts>
               <ts e="T74" id="Seg_774" n="e" s="T73">Šonəgam. </ts>
               <ts e="T75" id="Seg_776" n="e" s="T74">Kuliom: </ts>
               <ts e="T76" id="Seg_778" n="e" s="T75">turaʔi </ts>
               <ts e="T77" id="Seg_780" n="e" s="T76">nulaʔbəʔjə. </ts>
               <ts e="T78" id="Seg_782" n="e" s="T77">Tüjö </ts>
               <ts e="T79" id="Seg_784" n="e" s="T78">dibər </ts>
               <ts e="T80" id="Seg_786" n="e" s="T79">turazaŋdə </ts>
               <ts e="T81" id="Seg_788" n="e" s="T80">kallam. </ts>
               <ts e="T82" id="Seg_790" n="e" s="T81">Dĭgəttə </ts>
               <ts e="T83" id="Seg_792" n="e" s="T82">kandəgam, </ts>
               <ts e="T84" id="Seg_794" n="e" s="T83">kandəgam, </ts>
               <ts e="T85" id="Seg_796" n="e" s="T84">tenebiem </ts>
               <ts e="T86" id="Seg_798" n="e" s="T85">aktʼi, </ts>
               <ts e="T87" id="Seg_800" n="e" s="T86">dĭ </ts>
               <ts e="T88" id="Seg_802" n="e" s="T87">döbər </ts>
               <ts e="T89" id="Seg_804" n="e" s="T88">kalləj. </ts>
               <ts e="T90" id="Seg_806" n="e" s="T89">Dĭgəttə </ts>
               <ts e="T91" id="Seg_808" n="e" s="T90">kuliom. </ts>
               <ts e="T92" id="Seg_810" n="e" s="T91">(Ej) </ts>
               <ts e="T93" id="Seg_812" n="e" s="T92">Ej </ts>
               <ts e="T94" id="Seg_814" n="e" s="T93">dĭʔnə </ts>
               <ts e="T95" id="Seg_816" n="e" s="T94">kallaʔbəm. </ts>
               <ts e="T96" id="Seg_818" n="e" s="T95">Dĭgəttə </ts>
               <ts e="T97" id="Seg_820" n="e" s="T96">parluʔbiam. </ts>
               <ts e="T98" id="Seg_822" n="e" s="T97">I </ts>
               <ts e="T99" id="Seg_824" n="e" s="T98">Malinovskoe </ts>
               <ts e="T100" id="Seg_826" n="e" s="T99">aktʼinə </ts>
               <ts e="T101" id="Seg_828" n="e" s="T100">šobiam. </ts>
               <ts e="T102" id="Seg_830" n="e" s="T101">Dĭgəttə </ts>
               <ts e="T103" id="Seg_832" n="e" s="T102">šobiam. </ts>
               <ts e="T104" id="Seg_834" n="e" s="T103">Dĭn </ts>
               <ts e="T105" id="Seg_836" n="e" s="T104">(budəj) </ts>
               <ts e="T106" id="Seg_838" n="e" s="T105">budəj </ts>
               <ts e="T107" id="Seg_840" n="e" s="T106">kuʔpiʔi. </ts>
               <ts e="T108" id="Seg_842" n="e" s="T107">Măn </ts>
               <ts e="T109" id="Seg_844" n="e" s="T108">kambiam. </ts>
               <ts e="T110" id="Seg_846" n="e" s="T109">Šobiam. </ts>
               <ts e="T111" id="Seg_848" n="e" s="T110">Dĭgəttə </ts>
               <ts e="T112" id="Seg_850" n="e" s="T111">šobiam </ts>
               <ts e="T113" id="Seg_852" n="e" s="T112">maːʔnʼi. </ts>
               <ts e="T114" id="Seg_854" n="e" s="T113">(Biəʔ=) </ts>
               <ts e="T115" id="Seg_856" n="e" s="T114">Biəʔ </ts>
               <ts e="T116" id="Seg_858" n="e" s="T115">časɨʔi </ts>
               <ts e="T117" id="Seg_860" n="e" s="T116">ibiʔi. </ts>
               <ts e="T118" id="Seg_862" n="e" s="T117">(Do-) </ts>
               <ts e="T119" id="Seg_864" n="e" s="T118">Dön </ts>
               <ts e="T120" id="Seg_866" n="e" s="T119">băzlaʔbəʔjə </ts>
               <ts e="T121" id="Seg_868" n="e" s="T120">(sipara). </ts>
               <ts e="T122" id="Seg_870" n="e" s="T121">Dĭgəttə </ts>
               <ts e="T123" id="Seg_872" n="e" s="T122">šobiam </ts>
               <ts e="T124" id="Seg_874" n="e" s="T123">maʔnʼi </ts>
               <ts e="T125" id="Seg_876" n="e" s="T124">i </ts>
               <ts e="T126" id="Seg_878" n="e" s="T125">ambiam </ts>
               <ts e="T127" id="Seg_880" n="e" s="T126">ipek. </ts>
               <ts e="T128" id="Seg_882" n="e" s="T127">(Šalaʔi-) </ts>
               <ts e="T129" id="Seg_884" n="e" s="T128">Kunolbiam. </ts>
               <ts e="T130" id="Seg_886" n="e" s="T129">Dĭgəttə </ts>
               <ts e="T131" id="Seg_888" n="e" s="T130">kambiam, </ts>
               <ts e="T132" id="Seg_890" n="e" s="T131">dʼü </ts>
               <ts e="T133" id="Seg_892" n="e" s="T132">(dʼazerbiam). </ts>
               <ts e="T134" id="Seg_894" n="e" s="T133">I </ts>
               <ts e="T135" id="Seg_896" n="e" s="T134">amnolbiam </ts>
               <ts e="T136" id="Seg_898" n="e" s="T135">(tol-) </ts>
               <ts e="T137" id="Seg_900" n="e" s="T136">kapustam. </ts>
               <ts e="T138" id="Seg_902" n="e" s="T137">(I </ts>
               <ts e="T139" id="Seg_904" n="e" s="T138">bü=) </ts>
               <ts e="T140" id="Seg_906" n="e" s="T139">I </ts>
               <ts e="T141" id="Seg_908" n="e" s="T140">bü </ts>
               <ts e="T142" id="Seg_910" n="e" s="T141">kămnəbiam. </ts>
               <ts e="T143" id="Seg_912" n="e" s="T142">Dĭgəttə </ts>
               <ts e="T144" id="Seg_914" n="e" s="T143">nüdʼin </ts>
               <ts e="T145" id="Seg_916" n="e" s="T144">kunollaʔ </ts>
               <ts e="T146" id="Seg_918" n="e" s="T145">(iʔbə-) </ts>
               <ts e="T147" id="Seg_920" n="e" s="T146">iʔbiem. </ts>
               <ts e="T148" id="Seg_922" n="e" s="T147">Karəldʼan </ts>
               <ts e="T149" id="Seg_924" n="e" s="T148">uʔbdəbiam. </ts>
               <ts e="T150" id="Seg_926" n="e" s="T149">Brʼukva </ts>
               <ts e="T151" id="Seg_928" n="e" s="T150">amnolbiam. </ts>
               <ts e="T152" id="Seg_930" n="e" s="T151">Dĭgəttə. </ts>
               <ts e="T153" id="Seg_932" n="e" s="T152">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_933" s="T0">PKZ_196X_TripToAginskoe_nar.001 (001)</ta>
            <ta e="T7" id="Seg_934" s="T4">PKZ_196X_TripToAginskoe_nar.002 (002)</ta>
            <ta e="T11" id="Seg_935" s="T7">PKZ_196X_TripToAginskoe_nar.003 (003)</ta>
            <ta e="T15" id="Seg_936" s="T11">PKZ_196X_TripToAginskoe_nar.004 (004)</ta>
            <ta e="T20" id="Seg_937" s="T15">PKZ_196X_TripToAginskoe_nar.005 (005)</ta>
            <ta e="T26" id="Seg_938" s="T20">PKZ_196X_TripToAginskoe_nar.006 (006)</ta>
            <ta e="T31" id="Seg_939" s="T26">PKZ_196X_TripToAginskoe_nar.007 (007)</ta>
            <ta e="T36" id="Seg_940" s="T31">PKZ_196X_TripToAginskoe_nar.008 (008)</ta>
            <ta e="T44" id="Seg_941" s="T36">PKZ_196X_TripToAginskoe_nar.009 (009)</ta>
            <ta e="T48" id="Seg_942" s="T44">PKZ_196X_TripToAginskoe_nar.010 (010)</ta>
            <ta e="T54" id="Seg_943" s="T48">PKZ_196X_TripToAginskoe_nar.011 (011)</ta>
            <ta e="T62" id="Seg_944" s="T54">PKZ_196X_TripToAginskoe_nar.012 (012)</ta>
            <ta e="T67" id="Seg_945" s="T62">PKZ_196X_TripToAginskoe_nar.013 (013)</ta>
            <ta e="T70" id="Seg_946" s="T67">PKZ_196X_TripToAginskoe_nar.014 (014)</ta>
            <ta e="T73" id="Seg_947" s="T70">PKZ_196X_TripToAginskoe_nar.015 (015)</ta>
            <ta e="T74" id="Seg_948" s="T73">PKZ_196X_TripToAginskoe_nar.016 (016)</ta>
            <ta e="T77" id="Seg_949" s="T74">PKZ_196X_TripToAginskoe_nar.017 (017)</ta>
            <ta e="T81" id="Seg_950" s="T77">PKZ_196X_TripToAginskoe_nar.018 (018)</ta>
            <ta e="T89" id="Seg_951" s="T81">PKZ_196X_TripToAginskoe_nar.019 (019)</ta>
            <ta e="T91" id="Seg_952" s="T89">PKZ_196X_TripToAginskoe_nar.020 (020)</ta>
            <ta e="T95" id="Seg_953" s="T91">PKZ_196X_TripToAginskoe_nar.021 (021)</ta>
            <ta e="T97" id="Seg_954" s="T95">PKZ_196X_TripToAginskoe_nar.022 (022)</ta>
            <ta e="T101" id="Seg_955" s="T97">PKZ_196X_TripToAginskoe_nar.023 (023)</ta>
            <ta e="T103" id="Seg_956" s="T101">PKZ_196X_TripToAginskoe_nar.024 (024)</ta>
            <ta e="T107" id="Seg_957" s="T103">PKZ_196X_TripToAginskoe_nar.025 (025)</ta>
            <ta e="T109" id="Seg_958" s="T107">PKZ_196X_TripToAginskoe_nar.026 (026)</ta>
            <ta e="T110" id="Seg_959" s="T109">PKZ_196X_TripToAginskoe_nar.027 (027)</ta>
            <ta e="T113" id="Seg_960" s="T110">PKZ_196X_TripToAginskoe_nar.028 (028)</ta>
            <ta e="T117" id="Seg_961" s="T113">PKZ_196X_TripToAginskoe_nar.029 (029)</ta>
            <ta e="T121" id="Seg_962" s="T117">PKZ_196X_TripToAginskoe_nar.030 (030)</ta>
            <ta e="T127" id="Seg_963" s="T121">PKZ_196X_TripToAginskoe_nar.031 (031)</ta>
            <ta e="T129" id="Seg_964" s="T127">PKZ_196X_TripToAginskoe_nar.032 (032)</ta>
            <ta e="T133" id="Seg_965" s="T129">PKZ_196X_TripToAginskoe_nar.033 (033)</ta>
            <ta e="T137" id="Seg_966" s="T133">PKZ_196X_TripToAginskoe_nar.034 (034)</ta>
            <ta e="T142" id="Seg_967" s="T137">PKZ_196X_TripToAginskoe_nar.035 (035)</ta>
            <ta e="T147" id="Seg_968" s="T142">PKZ_196X_TripToAginskoe_nar.036 (036)</ta>
            <ta e="T149" id="Seg_969" s="T147">PKZ_196X_TripToAginskoe_nar.037 (037)</ta>
            <ta e="T151" id="Seg_970" s="T149">PKZ_196X_TripToAginskoe_nar.038 (038)</ta>
            <ta e="T152" id="Seg_971" s="T151">PKZ_196X_TripToAginskoe_nar.039 (039)</ta>
            <ta e="T153" id="Seg_972" s="T152">PKZ_196X_TripToAginskoe_nar.040 (040)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_973" s="T0">Măn ibiem Kazan turagən. </ta>
            <ta e="T7" id="Seg_974" s="T4">Dĭn kudajdə numan üzübiem. </ta>
            <ta e="T11" id="Seg_975" s="T7">Dĭgəttə (ka-) kambiam maʔnʼi. </ta>
            <ta e="T15" id="Seg_976" s="T11">Šobiam, šobiam (aktʼi=) aktʼigən. </ta>
            <ta e="T20" id="Seg_977" s="T15">Girgitdə ej bĭdəbi măna mašina. </ta>
            <ta e="T26" id="Seg_978" s="T20">Dĭgəttə (šu-) surno kambi i beržə. </ta>
            <ta e="T31" id="Seg_979" s="T26">A pimnal ej kumbi beržə. </ta>
            <ta e="T36" id="Seg_980" s="T31">Măn šobiam, šobiam, nulaʔbə mašina. </ta>
            <ta e="T44" id="Seg_981" s="T36">Dĭgəttə aktʼim dʼürlaʔbiam i kalla dʼürbiem (k uražanəm). </ta>
            <ta e="T48" id="Seg_982" s="T44">Dibər šobiem, dĭn šaːbiam. </ta>
            <ta e="T54" id="Seg_983" s="T48">Dĭn bar nüjleʔbəʔjə, ara bĭtleʔbəʔjə, kudonzlaʔbəʔjə. </ta>
            <ta e="T62" id="Seg_984" s="T54">Dĭgəttə kunolbiʔi, a măn erte uʔbdəbiam i kambiam. </ta>
            <ta e="T67" id="Seg_985" s="T62">Šobiam, šobiam, măn ipek ibi. </ta>
            <ta e="T70" id="Seg_986" s="T67">Dʼagan toːndə ambiam. </ta>
            <ta e="T73" id="Seg_987" s="T70">Dĭgəttə bazoʔ kambiam. </ta>
            <ta e="T74" id="Seg_988" s="T73">Šonəgam. </ta>
            <ta e="T77" id="Seg_989" s="T74">Kuliom: turaʔi nulaʔbəʔjə. </ta>
            <ta e="T81" id="Seg_990" s="T77">Tüjö dibər turazaŋdə kallam. </ta>
            <ta e="T89" id="Seg_991" s="T81">Dĭgəttə kandəgam, kandəgam, tenebiem aktʼi, dĭ döbər kalləj. </ta>
            <ta e="T91" id="Seg_992" s="T89">Dĭgəttə kuliom. </ta>
            <ta e="T95" id="Seg_993" s="T91">(Ej) Ej dĭʔnə kallaʔbəm. </ta>
            <ta e="T97" id="Seg_994" s="T95">Dĭgəttə parluʔbiam. </ta>
            <ta e="T101" id="Seg_995" s="T97">I Malinovskoe aktʼinə šobiam. </ta>
            <ta e="T103" id="Seg_996" s="T101">Dĭgəttə šobiam. </ta>
            <ta e="T107" id="Seg_997" s="T103">Dĭn (budəj) budəj kuʔpiʔi. </ta>
            <ta e="T109" id="Seg_998" s="T107">Măn kambiam. </ta>
            <ta e="T110" id="Seg_999" s="T109">Šobiam. </ta>
            <ta e="T113" id="Seg_1000" s="T110">Dĭgəttə šobiam maːʔnʼi. </ta>
            <ta e="T117" id="Seg_1001" s="T113">(Biəʔ=) Biəʔ časɨʔi ibiʔi. </ta>
            <ta e="T121" id="Seg_1002" s="T117">(Do-) Dön băzlaʔbəʔjə (sipara). </ta>
            <ta e="T127" id="Seg_1003" s="T121">Dĭgəttə šobiam maʔnʼi i ambiam ipek. </ta>
            <ta e="T129" id="Seg_1004" s="T127">(Šalaʔi-) Kunolbiam. </ta>
            <ta e="T133" id="Seg_1005" s="T129">Dĭgəttə kambiam, dʼü (dʼazerbiam). </ta>
            <ta e="T137" id="Seg_1006" s="T133">I amnolbiam (tol-) kapustam. </ta>
            <ta e="T142" id="Seg_1007" s="T137">(I bü=) I bü kămnəbiam. </ta>
            <ta e="T147" id="Seg_1008" s="T142">Dĭgəttə nüdʼin kunollaʔ (iʔbə-) iʔbiem. ((NOISE)) </ta>
            <ta e="T149" id="Seg_1009" s="T147">Karəldʼan uʔbdəbiam. </ta>
            <ta e="T151" id="Seg_1010" s="T149">Brʼukva amnolbiam. </ta>
            <ta e="T152" id="Seg_1011" s="T151">Dĭgəttə. </ta>
            <ta e="T153" id="Seg_1012" s="T152">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1013" s="T0">măn</ta>
            <ta e="T2" id="Seg_1014" s="T1">i-bie-m</ta>
            <ta e="T3" id="Seg_1015" s="T2">Kazan</ta>
            <ta e="T4" id="Seg_1016" s="T3">tura-gən</ta>
            <ta e="T5" id="Seg_1017" s="T4">dĭn</ta>
            <ta e="T6" id="Seg_1018" s="T5">kudaj-də</ta>
            <ta e="T154" id="Seg_1019" s="T6">num-a-n</ta>
            <ta e="T7" id="Seg_1020" s="T154">üzü-bie-m</ta>
            <ta e="T8" id="Seg_1021" s="T7">dĭgəttə</ta>
            <ta e="T10" id="Seg_1022" s="T9">kam-bia-m</ta>
            <ta e="T11" id="Seg_1023" s="T10">maʔ-nʼi</ta>
            <ta e="T12" id="Seg_1024" s="T11">šo-bia-m</ta>
            <ta e="T13" id="Seg_1025" s="T12">šo-bia-m</ta>
            <ta e="T14" id="Seg_1026" s="T13">aktʼi</ta>
            <ta e="T15" id="Seg_1027" s="T14">aktʼi-gən</ta>
            <ta e="T16" id="Seg_1028" s="T15">girgit=də</ta>
            <ta e="T17" id="Seg_1029" s="T16">ej</ta>
            <ta e="T18" id="Seg_1030" s="T17">bĭdə-bi</ta>
            <ta e="T19" id="Seg_1031" s="T18">măna</ta>
            <ta e="T20" id="Seg_1032" s="T19">mašina</ta>
            <ta e="T21" id="Seg_1033" s="T20">dĭgəttə</ta>
            <ta e="T23" id="Seg_1034" s="T22">surno</ta>
            <ta e="T24" id="Seg_1035" s="T23">kam-bi</ta>
            <ta e="T25" id="Seg_1036" s="T24">i</ta>
            <ta e="T26" id="Seg_1037" s="T25">beržə</ta>
            <ta e="T27" id="Seg_1038" s="T26">a</ta>
            <ta e="T28" id="Seg_1039" s="T27">pim-na-l</ta>
            <ta e="T29" id="Seg_1040" s="T28">ej</ta>
            <ta e="T30" id="Seg_1041" s="T29">kum-bi</ta>
            <ta e="T31" id="Seg_1042" s="T30">beržə</ta>
            <ta e="T32" id="Seg_1043" s="T31">măn</ta>
            <ta e="T33" id="Seg_1044" s="T32">šo-bia-m</ta>
            <ta e="T34" id="Seg_1045" s="T33">šo-bia-m</ta>
            <ta e="T35" id="Seg_1046" s="T34">nu-laʔbə</ta>
            <ta e="T36" id="Seg_1047" s="T35">mašina</ta>
            <ta e="T37" id="Seg_1048" s="T36">dĭgəttə</ta>
            <ta e="T38" id="Seg_1049" s="T37">aktʼi-m</ta>
            <ta e="T39" id="Seg_1050" s="T38">dʼür-laʔ-bia-m</ta>
            <ta e="T40" id="Seg_1051" s="T39">i</ta>
            <ta e="T41" id="Seg_1052" s="T40">kal-la</ta>
            <ta e="T42" id="Seg_1053" s="T41">dʼür-bie-m</ta>
            <ta e="T45" id="Seg_1054" s="T44">dibər</ta>
            <ta e="T46" id="Seg_1055" s="T45">šo-bie-m</ta>
            <ta e="T47" id="Seg_1056" s="T46">dĭn</ta>
            <ta e="T48" id="Seg_1057" s="T47">šaː-bia-m</ta>
            <ta e="T49" id="Seg_1058" s="T48">dĭn</ta>
            <ta e="T50" id="Seg_1059" s="T49">bar</ta>
            <ta e="T51" id="Seg_1060" s="T50">nüj-leʔbə-ʔjə</ta>
            <ta e="T52" id="Seg_1061" s="T51">ara</ta>
            <ta e="T53" id="Seg_1062" s="T52">bĭt-leʔbə-ʔjə</ta>
            <ta e="T54" id="Seg_1063" s="T53">kudo-nz-laʔbə-ʔjə</ta>
            <ta e="T55" id="Seg_1064" s="T54">dĭgəttə</ta>
            <ta e="T56" id="Seg_1065" s="T55">kunol-bi-ʔi</ta>
            <ta e="T57" id="Seg_1066" s="T56">a</ta>
            <ta e="T58" id="Seg_1067" s="T57">măn</ta>
            <ta e="T59" id="Seg_1068" s="T58">erte</ta>
            <ta e="T60" id="Seg_1069" s="T59">uʔbdə-bia-m</ta>
            <ta e="T61" id="Seg_1070" s="T60">i</ta>
            <ta e="T62" id="Seg_1071" s="T61">kam-bia-m</ta>
            <ta e="T63" id="Seg_1072" s="T62">šo-bia-m</ta>
            <ta e="T64" id="Seg_1073" s="T63">šo-bia-m</ta>
            <ta e="T65" id="Seg_1074" s="T64">măn</ta>
            <ta e="T66" id="Seg_1075" s="T65">ipek</ta>
            <ta e="T67" id="Seg_1076" s="T66">i-bi</ta>
            <ta e="T68" id="Seg_1077" s="T67">dʼaga-n</ta>
            <ta e="T69" id="Seg_1078" s="T68">toː-ndə</ta>
            <ta e="T70" id="Seg_1079" s="T69">am-bia-m</ta>
            <ta e="T71" id="Seg_1080" s="T70">dĭgəttə</ta>
            <ta e="T72" id="Seg_1081" s="T71">bazoʔ</ta>
            <ta e="T73" id="Seg_1082" s="T72">kam-bia-m</ta>
            <ta e="T74" id="Seg_1083" s="T73">šonə-ga-m</ta>
            <ta e="T75" id="Seg_1084" s="T74">ku-lio-m</ta>
            <ta e="T76" id="Seg_1085" s="T75">tura-ʔi</ta>
            <ta e="T77" id="Seg_1086" s="T76">nu-laʔbə-ʔjə</ta>
            <ta e="T78" id="Seg_1087" s="T77">tüjö</ta>
            <ta e="T79" id="Seg_1088" s="T78">dibər</ta>
            <ta e="T80" id="Seg_1089" s="T79">tura-zaŋ-də</ta>
            <ta e="T81" id="Seg_1090" s="T80">kal-la-m</ta>
            <ta e="T82" id="Seg_1091" s="T81">dĭgəttə</ta>
            <ta e="T83" id="Seg_1092" s="T82">kandə-ga-m</ta>
            <ta e="T84" id="Seg_1093" s="T83">kandə-ga-m</ta>
            <ta e="T85" id="Seg_1094" s="T84">tene-bie-m</ta>
            <ta e="T86" id="Seg_1095" s="T85">aktʼi</ta>
            <ta e="T87" id="Seg_1096" s="T86">dĭ</ta>
            <ta e="T88" id="Seg_1097" s="T87">döbər</ta>
            <ta e="T89" id="Seg_1098" s="T88">kal-lə-j</ta>
            <ta e="T90" id="Seg_1099" s="T89">dĭgəttə</ta>
            <ta e="T91" id="Seg_1100" s="T90">ku-lio-m</ta>
            <ta e="T92" id="Seg_1101" s="T91">ej</ta>
            <ta e="T93" id="Seg_1102" s="T92">ej</ta>
            <ta e="T94" id="Seg_1103" s="T93">dĭʔ-nə</ta>
            <ta e="T95" id="Seg_1104" s="T94">kal-laʔbə-m</ta>
            <ta e="T96" id="Seg_1105" s="T95">dĭgəttə</ta>
            <ta e="T97" id="Seg_1106" s="T96">par-luʔ-bia-m</ta>
            <ta e="T98" id="Seg_1107" s="T97">i</ta>
            <ta e="T99" id="Seg_1108" s="T98">Malinovskoe</ta>
            <ta e="T100" id="Seg_1109" s="T99">aktʼi-nə</ta>
            <ta e="T101" id="Seg_1110" s="T100">šo-bia-m</ta>
            <ta e="T102" id="Seg_1111" s="T101">dĭgəttə</ta>
            <ta e="T103" id="Seg_1112" s="T102">šo-bia-m</ta>
            <ta e="T104" id="Seg_1113" s="T103">dĭn</ta>
            <ta e="T105" id="Seg_1114" s="T104">budəj</ta>
            <ta e="T106" id="Seg_1115" s="T105">budəj</ta>
            <ta e="T107" id="Seg_1116" s="T106">kuʔ-pi-ʔi</ta>
            <ta e="T108" id="Seg_1117" s="T107">măn</ta>
            <ta e="T109" id="Seg_1118" s="T108">kam-bia-m</ta>
            <ta e="T110" id="Seg_1119" s="T109">šo-bia-m</ta>
            <ta e="T111" id="Seg_1120" s="T110">dĭgəttə</ta>
            <ta e="T112" id="Seg_1121" s="T111">šo-bia-m</ta>
            <ta e="T113" id="Seg_1122" s="T112">maːʔ-nʼi</ta>
            <ta e="T114" id="Seg_1123" s="T113">biəʔ</ta>
            <ta e="T115" id="Seg_1124" s="T114">biəʔ</ta>
            <ta e="T116" id="Seg_1125" s="T115">časɨ-ʔi</ta>
            <ta e="T117" id="Seg_1126" s="T116">i-bi-ʔi</ta>
            <ta e="T119" id="Seg_1127" s="T118">dön</ta>
            <ta e="T120" id="Seg_1128" s="T119">băz-laʔbə-ʔjə</ta>
            <ta e="T121" id="Seg_1129" s="T120">sipara</ta>
            <ta e="T122" id="Seg_1130" s="T121">dĭgəttə</ta>
            <ta e="T123" id="Seg_1131" s="T122">šo-bia-m</ta>
            <ta e="T124" id="Seg_1132" s="T123">maʔ-nʼi</ta>
            <ta e="T125" id="Seg_1133" s="T124">i</ta>
            <ta e="T126" id="Seg_1134" s="T125">am-bia-m</ta>
            <ta e="T127" id="Seg_1135" s="T126">ipek</ta>
            <ta e="T129" id="Seg_1136" s="T128">kunol-bia-m</ta>
            <ta e="T130" id="Seg_1137" s="T129">dĭgəttə</ta>
            <ta e="T131" id="Seg_1138" s="T130">kam-bia-m</ta>
            <ta e="T132" id="Seg_1139" s="T131">dʼü</ta>
            <ta e="T133" id="Seg_1140" s="T132">dʼazer-bia-m</ta>
            <ta e="T134" id="Seg_1141" s="T133">i</ta>
            <ta e="T135" id="Seg_1142" s="T134">amnol-bia-m</ta>
            <ta e="T137" id="Seg_1143" s="T136">kăpusta-m</ta>
            <ta e="T138" id="Seg_1144" s="T137">i</ta>
            <ta e="T139" id="Seg_1145" s="T138">bü</ta>
            <ta e="T140" id="Seg_1146" s="T139">i</ta>
            <ta e="T141" id="Seg_1147" s="T140">bü</ta>
            <ta e="T142" id="Seg_1148" s="T141">kămnə-bia-m</ta>
            <ta e="T143" id="Seg_1149" s="T142">dĭgəttə</ta>
            <ta e="T144" id="Seg_1150" s="T143">nüdʼi-n</ta>
            <ta e="T145" id="Seg_1151" s="T144">kunol-laʔ</ta>
            <ta e="T147" id="Seg_1152" s="T146">iʔ-bie-m</ta>
            <ta e="T148" id="Seg_1153" s="T147">karəldʼan</ta>
            <ta e="T149" id="Seg_1154" s="T148">uʔbdə-bia-m</ta>
            <ta e="T150" id="Seg_1155" s="T149">brʼukva</ta>
            <ta e="T151" id="Seg_1156" s="T150">amnol-bia-m</ta>
            <ta e="T152" id="Seg_1157" s="T151">dĭgəttə</ta>
            <ta e="T153" id="Seg_1158" s="T152">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1159" s="T0">măn</ta>
            <ta e="T2" id="Seg_1160" s="T1">i-bi-m</ta>
            <ta e="T3" id="Seg_1161" s="T2">Kazan</ta>
            <ta e="T4" id="Seg_1162" s="T3">tura-Kən</ta>
            <ta e="T5" id="Seg_1163" s="T4">dĭn</ta>
            <ta e="T6" id="Seg_1164" s="T5">kudaj-Tə</ta>
            <ta e="T154" id="Seg_1165" s="T6">num-ə-n</ta>
            <ta e="T7" id="Seg_1166" s="T154">üzü-bi-m</ta>
            <ta e="T8" id="Seg_1167" s="T7">dĭgəttə</ta>
            <ta e="T10" id="Seg_1168" s="T9">kan-bi-m</ta>
            <ta e="T11" id="Seg_1169" s="T10">maʔ-gənʼi</ta>
            <ta e="T12" id="Seg_1170" s="T11">šo-bi-m</ta>
            <ta e="T13" id="Seg_1171" s="T12">šo-bi-m</ta>
            <ta e="T14" id="Seg_1172" s="T13">aʔtʼi</ta>
            <ta e="T15" id="Seg_1173" s="T14">aʔtʼi-Kən</ta>
            <ta e="T16" id="Seg_1174" s="T15">girgit=də</ta>
            <ta e="T17" id="Seg_1175" s="T16">ej</ta>
            <ta e="T18" id="Seg_1176" s="T17">bĭdə-bi</ta>
            <ta e="T19" id="Seg_1177" s="T18">măna</ta>
            <ta e="T20" id="Seg_1178" s="T19">mašina</ta>
            <ta e="T21" id="Seg_1179" s="T20">dĭgəttə</ta>
            <ta e="T23" id="Seg_1180" s="T22">surno</ta>
            <ta e="T24" id="Seg_1181" s="T23">kan-bi</ta>
            <ta e="T25" id="Seg_1182" s="T24">i</ta>
            <ta e="T26" id="Seg_1183" s="T25">beržə</ta>
            <ta e="T27" id="Seg_1184" s="T26">a</ta>
            <ta e="T28" id="Seg_1185" s="T27">pim-lV-l</ta>
            <ta e="T29" id="Seg_1186" s="T28">ej</ta>
            <ta e="T30" id="Seg_1187" s="T29">kun-bi</ta>
            <ta e="T31" id="Seg_1188" s="T30">beržə</ta>
            <ta e="T32" id="Seg_1189" s="T31">măn</ta>
            <ta e="T33" id="Seg_1190" s="T32">šo-bi-m</ta>
            <ta e="T34" id="Seg_1191" s="T33">šo-bi-m</ta>
            <ta e="T35" id="Seg_1192" s="T34">nu-laʔbə</ta>
            <ta e="T36" id="Seg_1193" s="T35">mašina</ta>
            <ta e="T37" id="Seg_1194" s="T36">dĭgəttə</ta>
            <ta e="T38" id="Seg_1195" s="T37">aʔtʼi-m</ta>
            <ta e="T39" id="Seg_1196" s="T38">tʼür-laʔbə-bi-m</ta>
            <ta e="T40" id="Seg_1197" s="T39">i</ta>
            <ta e="T41" id="Seg_1198" s="T40">kan-lAʔ</ta>
            <ta e="T42" id="Seg_1199" s="T41">tʼür-bi-m</ta>
            <ta e="T45" id="Seg_1200" s="T44">dĭbər</ta>
            <ta e="T46" id="Seg_1201" s="T45">šo-bi-m</ta>
            <ta e="T47" id="Seg_1202" s="T46">dĭn</ta>
            <ta e="T48" id="Seg_1203" s="T47">šaː-bi-m</ta>
            <ta e="T49" id="Seg_1204" s="T48">dĭn</ta>
            <ta e="T50" id="Seg_1205" s="T49">bar</ta>
            <ta e="T51" id="Seg_1206" s="T50">nüj-laʔbə-jəʔ</ta>
            <ta e="T52" id="Seg_1207" s="T51">ara</ta>
            <ta e="T53" id="Seg_1208" s="T52">bĭs-laʔbə-jəʔ</ta>
            <ta e="T54" id="Seg_1209" s="T53">kudo-nzə-laʔbə-jəʔ</ta>
            <ta e="T55" id="Seg_1210" s="T54">dĭgəttə</ta>
            <ta e="T56" id="Seg_1211" s="T55">kunol-bi-jəʔ</ta>
            <ta e="T57" id="Seg_1212" s="T56">a</ta>
            <ta e="T58" id="Seg_1213" s="T57">măn</ta>
            <ta e="T59" id="Seg_1214" s="T58">ertə</ta>
            <ta e="T60" id="Seg_1215" s="T59">uʔbdə-bi-m</ta>
            <ta e="T61" id="Seg_1216" s="T60">i</ta>
            <ta e="T62" id="Seg_1217" s="T61">kan-bi-m</ta>
            <ta e="T63" id="Seg_1218" s="T62">šo-bi-m</ta>
            <ta e="T64" id="Seg_1219" s="T63">šo-bi-m</ta>
            <ta e="T65" id="Seg_1220" s="T64">măn</ta>
            <ta e="T66" id="Seg_1221" s="T65">ipek</ta>
            <ta e="T67" id="Seg_1222" s="T66">i-bi</ta>
            <ta e="T68" id="Seg_1223" s="T67">tʼăga-n</ta>
            <ta e="T69" id="Seg_1224" s="T68">toʔ-gəndə</ta>
            <ta e="T70" id="Seg_1225" s="T69">am-bi-m</ta>
            <ta e="T71" id="Seg_1226" s="T70">dĭgəttə</ta>
            <ta e="T72" id="Seg_1227" s="T71">bazoʔ</ta>
            <ta e="T73" id="Seg_1228" s="T72">kan-bi-m</ta>
            <ta e="T74" id="Seg_1229" s="T73">šonə-gA-m</ta>
            <ta e="T75" id="Seg_1230" s="T74">ku-liA-m</ta>
            <ta e="T76" id="Seg_1231" s="T75">tura-jəʔ</ta>
            <ta e="T77" id="Seg_1232" s="T76">nu-laʔbə-jəʔ</ta>
            <ta e="T78" id="Seg_1233" s="T77">tüjö</ta>
            <ta e="T79" id="Seg_1234" s="T78">dĭbər</ta>
            <ta e="T80" id="Seg_1235" s="T79">tura-zAŋ-Tə</ta>
            <ta e="T81" id="Seg_1236" s="T80">kan-lV-m</ta>
            <ta e="T82" id="Seg_1237" s="T81">dĭgəttə</ta>
            <ta e="T83" id="Seg_1238" s="T82">kandə-gA-m</ta>
            <ta e="T84" id="Seg_1239" s="T83">kandə-gA-m</ta>
            <ta e="T85" id="Seg_1240" s="T84">tene-bi-m</ta>
            <ta e="T86" id="Seg_1241" s="T85">aʔtʼi</ta>
            <ta e="T87" id="Seg_1242" s="T86">dĭ</ta>
            <ta e="T88" id="Seg_1243" s="T87">döbər</ta>
            <ta e="T89" id="Seg_1244" s="T88">kan-lV-j</ta>
            <ta e="T90" id="Seg_1245" s="T89">dĭgəttə</ta>
            <ta e="T91" id="Seg_1246" s="T90">ku-liA-m</ta>
            <ta e="T92" id="Seg_1247" s="T91">ej</ta>
            <ta e="T93" id="Seg_1248" s="T92">ej</ta>
            <ta e="T94" id="Seg_1249" s="T93">dĭ-Tə</ta>
            <ta e="T95" id="Seg_1250" s="T94">kan-laʔbə-m</ta>
            <ta e="T96" id="Seg_1251" s="T95">dĭgəttə</ta>
            <ta e="T97" id="Seg_1252" s="T96">par-luʔbdə-bi-m</ta>
            <ta e="T98" id="Seg_1253" s="T97">i</ta>
            <ta e="T99" id="Seg_1254" s="T98">Malinovskoe</ta>
            <ta e="T100" id="Seg_1255" s="T99">aʔtʼi-Tə</ta>
            <ta e="T101" id="Seg_1256" s="T100">šo-bi-m</ta>
            <ta e="T102" id="Seg_1257" s="T101">dĭgəttə</ta>
            <ta e="T103" id="Seg_1258" s="T102">šo-bi-m</ta>
            <ta e="T104" id="Seg_1259" s="T103">dĭn</ta>
            <ta e="T105" id="Seg_1260" s="T104">budəj</ta>
            <ta e="T106" id="Seg_1261" s="T105">budəj</ta>
            <ta e="T107" id="Seg_1262" s="T106">kuʔ-bi-jəʔ</ta>
            <ta e="T108" id="Seg_1263" s="T107">măn</ta>
            <ta e="T109" id="Seg_1264" s="T108">kan-bi-m</ta>
            <ta e="T110" id="Seg_1265" s="T109">šo-bi-m</ta>
            <ta e="T111" id="Seg_1266" s="T110">dĭgəttə</ta>
            <ta e="T112" id="Seg_1267" s="T111">šo-bi-m</ta>
            <ta e="T113" id="Seg_1268" s="T112">maʔ-gənʼi</ta>
            <ta e="T114" id="Seg_1269" s="T113">biəʔ</ta>
            <ta e="T115" id="Seg_1270" s="T114">biəʔ</ta>
            <ta e="T116" id="Seg_1271" s="T115">časɨ-jəʔ</ta>
            <ta e="T117" id="Seg_1272" s="T116">i-bi-jəʔ</ta>
            <ta e="T119" id="Seg_1273" s="T118">dön</ta>
            <ta e="T120" id="Seg_1274" s="T119">băzə-laʔbə-jəʔ</ta>
            <ta e="T121" id="Seg_1275" s="T120">sipara</ta>
            <ta e="T122" id="Seg_1276" s="T121">dĭgəttə</ta>
            <ta e="T123" id="Seg_1277" s="T122">šo-bi-m</ta>
            <ta e="T124" id="Seg_1278" s="T123">maʔ-gənʼi</ta>
            <ta e="T125" id="Seg_1279" s="T124">i</ta>
            <ta e="T126" id="Seg_1280" s="T125">am-bi-m</ta>
            <ta e="T127" id="Seg_1281" s="T126">ipek</ta>
            <ta e="T129" id="Seg_1282" s="T128">kunol-bi-m</ta>
            <ta e="T130" id="Seg_1283" s="T129">dĭgəttə</ta>
            <ta e="T131" id="Seg_1284" s="T130">kan-bi-m</ta>
            <ta e="T132" id="Seg_1285" s="T131">tʼo</ta>
            <ta e="T133" id="Seg_1286" s="T132">tajər-bi-m</ta>
            <ta e="T134" id="Seg_1287" s="T133">i</ta>
            <ta e="T135" id="Seg_1288" s="T134">amnol-bi-m</ta>
            <ta e="T137" id="Seg_1289" s="T136">kăpusta-m</ta>
            <ta e="T138" id="Seg_1290" s="T137">i</ta>
            <ta e="T139" id="Seg_1291" s="T138">bü</ta>
            <ta e="T140" id="Seg_1292" s="T139">i</ta>
            <ta e="T141" id="Seg_1293" s="T140">bü</ta>
            <ta e="T142" id="Seg_1294" s="T141">kămnə-bi-m</ta>
            <ta e="T143" id="Seg_1295" s="T142">dĭgəttə</ta>
            <ta e="T144" id="Seg_1296" s="T143">nüdʼi-n</ta>
            <ta e="T145" id="Seg_1297" s="T144">kunol-lAʔ</ta>
            <ta e="T147" id="Seg_1298" s="T146">iʔbə-bi-m</ta>
            <ta e="T148" id="Seg_1299" s="T147">karəldʼaːn</ta>
            <ta e="T149" id="Seg_1300" s="T148">uʔbdə-bi-m</ta>
            <ta e="T150" id="Seg_1301" s="T149">brʼukva</ta>
            <ta e="T151" id="Seg_1302" s="T150">amnol-bi-m</ta>
            <ta e="T152" id="Seg_1303" s="T151">dĭgəttə</ta>
            <ta e="T153" id="Seg_1304" s="T152">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1305" s="T0">I.NOM</ta>
            <ta e="T2" id="Seg_1306" s="T1">be-PST-1SG</ta>
            <ta e="T3" id="Seg_1307" s="T2">Aginskoe</ta>
            <ta e="T4" id="Seg_1308" s="T3">settlement-LOC</ta>
            <ta e="T5" id="Seg_1309" s="T4">there</ta>
            <ta e="T6" id="Seg_1310" s="T5">God-LAT</ta>
            <ta e="T154" id="Seg_1311" s="T6">God-EP-GEN</ta>
            <ta e="T7" id="Seg_1312" s="T154">pray-PST-1SG</ta>
            <ta e="T8" id="Seg_1313" s="T7">then</ta>
            <ta e="T10" id="Seg_1314" s="T9">go-PST-1SG</ta>
            <ta e="T11" id="Seg_1315" s="T10">tent-LAT/LOC.1SG</ta>
            <ta e="T12" id="Seg_1316" s="T11">come-PST-1SG</ta>
            <ta e="T13" id="Seg_1317" s="T12">come-PST-1SG</ta>
            <ta e="T14" id="Seg_1318" s="T13">road</ta>
            <ta e="T15" id="Seg_1319" s="T14">road-LOC</ta>
            <ta e="T16" id="Seg_1320" s="T15">what.kind=INDEF</ta>
            <ta e="T17" id="Seg_1321" s="T16">NEG</ta>
            <ta e="T18" id="Seg_1322" s="T17">catch.up-PST.[3SG]</ta>
            <ta e="T19" id="Seg_1323" s="T18">I.ACC</ta>
            <ta e="T20" id="Seg_1324" s="T19">machine.[NOM.SG]</ta>
            <ta e="T21" id="Seg_1325" s="T20">then</ta>
            <ta e="T23" id="Seg_1326" s="T22">rain.[NOM.SG]</ta>
            <ta e="T24" id="Seg_1327" s="T23">go-PST.[3SG]</ta>
            <ta e="T25" id="Seg_1328" s="T24">and</ta>
            <ta e="T26" id="Seg_1329" s="T25">wind.[NOM.SG]</ta>
            <ta e="T27" id="Seg_1330" s="T26">and</ta>
            <ta e="T28" id="Seg_1331" s="T27">fear-FUT-2SG</ta>
            <ta e="T29" id="Seg_1332" s="T28">NEG</ta>
            <ta e="T30" id="Seg_1333" s="T29">bring-PST.[3SG]</ta>
            <ta e="T31" id="Seg_1334" s="T30">wind.[NOM.SG]</ta>
            <ta e="T32" id="Seg_1335" s="T31">I.NOM</ta>
            <ta e="T33" id="Seg_1336" s="T32">come-PST-1SG</ta>
            <ta e="T34" id="Seg_1337" s="T33">come-PST-1SG</ta>
            <ta e="T35" id="Seg_1338" s="T34">stand-DUR.[3SG]</ta>
            <ta e="T36" id="Seg_1339" s="T35">machine.[NOM.SG]</ta>
            <ta e="T37" id="Seg_1340" s="T36">then</ta>
            <ta e="T38" id="Seg_1341" s="T37">road-ACC</ta>
            <ta e="T39" id="Seg_1342" s="T38">disappear-DUR-PST-1SG</ta>
            <ta e="T40" id="Seg_1343" s="T39">and</ta>
            <ta e="T41" id="Seg_1344" s="T40">go-CVB</ta>
            <ta e="T42" id="Seg_1345" s="T41">disappear-PST-1SG</ta>
            <ta e="T45" id="Seg_1346" s="T44">there</ta>
            <ta e="T46" id="Seg_1347" s="T45">come-PST-1SG</ta>
            <ta e="T47" id="Seg_1348" s="T46">there</ta>
            <ta e="T48" id="Seg_1349" s="T47">spend.night-PST-1SG</ta>
            <ta e="T49" id="Seg_1350" s="T48">there</ta>
            <ta e="T50" id="Seg_1351" s="T49">all</ta>
            <ta e="T51" id="Seg_1352" s="T50">sing-DUR-3PL</ta>
            <ta e="T52" id="Seg_1353" s="T51">vodka.[NOM.SG]</ta>
            <ta e="T53" id="Seg_1354" s="T52">drink-DUR-3PL</ta>
            <ta e="T54" id="Seg_1355" s="T53">scold-DES-DUR-3PL</ta>
            <ta e="T55" id="Seg_1356" s="T54">then</ta>
            <ta e="T56" id="Seg_1357" s="T55">sleep-PST-3PL</ta>
            <ta e="T57" id="Seg_1358" s="T56">and</ta>
            <ta e="T58" id="Seg_1359" s="T57">I.NOM</ta>
            <ta e="T59" id="Seg_1360" s="T58">morning</ta>
            <ta e="T60" id="Seg_1361" s="T59">get.up-PST-1SG</ta>
            <ta e="T61" id="Seg_1362" s="T60">and</ta>
            <ta e="T62" id="Seg_1363" s="T61">go-PST-1SG</ta>
            <ta e="T63" id="Seg_1364" s="T62">come-PST-1SG</ta>
            <ta e="T64" id="Seg_1365" s="T63">come-PST-1SG</ta>
            <ta e="T65" id="Seg_1366" s="T64">I.NOM</ta>
            <ta e="T66" id="Seg_1367" s="T65">bread.[NOM.SG]</ta>
            <ta e="T67" id="Seg_1368" s="T66">be-PST.[3SG]</ta>
            <ta e="T68" id="Seg_1369" s="T67">river-GEN</ta>
            <ta e="T69" id="Seg_1370" s="T68">edge-LAT/LOC.3SG</ta>
            <ta e="T70" id="Seg_1371" s="T69">sit-PST-1SG</ta>
            <ta e="T71" id="Seg_1372" s="T70">then</ta>
            <ta e="T72" id="Seg_1373" s="T71">again</ta>
            <ta e="T73" id="Seg_1374" s="T72">go-PST-1SG</ta>
            <ta e="T74" id="Seg_1375" s="T73">come-PRS-1SG</ta>
            <ta e="T75" id="Seg_1376" s="T74">see-PRS-1SG</ta>
            <ta e="T76" id="Seg_1377" s="T75">house-PL</ta>
            <ta e="T77" id="Seg_1378" s="T76">stand-DUR-3PL</ta>
            <ta e="T78" id="Seg_1379" s="T77">soon</ta>
            <ta e="T79" id="Seg_1380" s="T78">there</ta>
            <ta e="T80" id="Seg_1381" s="T79">house-PL-LAT</ta>
            <ta e="T81" id="Seg_1382" s="T80">go-FUT-1SG</ta>
            <ta e="T82" id="Seg_1383" s="T81">then</ta>
            <ta e="T83" id="Seg_1384" s="T82">walk-PRS-1SG</ta>
            <ta e="T84" id="Seg_1385" s="T83">walk-PRS-1SG</ta>
            <ta e="T85" id="Seg_1386" s="T84">think-PST-1SG</ta>
            <ta e="T86" id="Seg_1387" s="T85">road.[NOM.SG]</ta>
            <ta e="T87" id="Seg_1388" s="T86">this.[NOM.SG]</ta>
            <ta e="T88" id="Seg_1389" s="T87">here</ta>
            <ta e="T89" id="Seg_1390" s="T88">go-FUT-3SG</ta>
            <ta e="T90" id="Seg_1391" s="T89">then</ta>
            <ta e="T91" id="Seg_1392" s="T90">see-PRS-1SG</ta>
            <ta e="T92" id="Seg_1393" s="T91">NEG</ta>
            <ta e="T93" id="Seg_1394" s="T92">NEG</ta>
            <ta e="T94" id="Seg_1395" s="T93">this-LAT</ta>
            <ta e="T95" id="Seg_1396" s="T94">go-DUR-1SG</ta>
            <ta e="T96" id="Seg_1397" s="T95">then</ta>
            <ta e="T97" id="Seg_1398" s="T96">return-MOM-PST-1SG</ta>
            <ta e="T98" id="Seg_1399" s="T97">and</ta>
            <ta e="T99" id="Seg_1400" s="T98">Malinovskoye.[NOM.SG]</ta>
            <ta e="T100" id="Seg_1401" s="T99">road-LAT</ta>
            <ta e="T101" id="Seg_1402" s="T100">come-PST-1SG</ta>
            <ta e="T102" id="Seg_1403" s="T101">then</ta>
            <ta e="T103" id="Seg_1404" s="T102">come-PST-1SG</ta>
            <ta e="T104" id="Seg_1405" s="T103">there</ta>
            <ta e="T105" id="Seg_1406" s="T104">wheat.[NOM.SG]</ta>
            <ta e="T106" id="Seg_1407" s="T105">wheat.[NOM.SG]</ta>
            <ta e="T107" id="Seg_1408" s="T106">sow-PST-3PL</ta>
            <ta e="T108" id="Seg_1409" s="T107">I.NOM</ta>
            <ta e="T109" id="Seg_1410" s="T108">go-PST-1SG</ta>
            <ta e="T110" id="Seg_1411" s="T109">come-PST-1SG</ta>
            <ta e="T111" id="Seg_1412" s="T110">then</ta>
            <ta e="T112" id="Seg_1413" s="T111">come-PST-1SG</ta>
            <ta e="T113" id="Seg_1414" s="T112">tent-LAT/LOC.1SG</ta>
            <ta e="T114" id="Seg_1415" s="T113">ten.[NOM.SG]</ta>
            <ta e="T115" id="Seg_1416" s="T114">ten.[NOM.SG]</ta>
            <ta e="T116" id="Seg_1417" s="T115">hour.PL-PL</ta>
            <ta e="T117" id="Seg_1418" s="T116">be-PST-3PL</ta>
            <ta e="T119" id="Seg_1419" s="T118">here</ta>
            <ta e="T120" id="Seg_1420" s="T119">wash-DUR-3PL</ta>
            <ta e="T121" id="Seg_1421" s="T120">%%</ta>
            <ta e="T122" id="Seg_1422" s="T121">then</ta>
            <ta e="T123" id="Seg_1423" s="T122">come-PST-1SG</ta>
            <ta e="T124" id="Seg_1424" s="T123">tent-LAT/LOC.1SG</ta>
            <ta e="T125" id="Seg_1425" s="T124">and</ta>
            <ta e="T126" id="Seg_1426" s="T125">eat-PST-1SG</ta>
            <ta e="T127" id="Seg_1427" s="T126">bread.[NOM.SG]</ta>
            <ta e="T129" id="Seg_1428" s="T128">sleep-PST-1SG</ta>
            <ta e="T130" id="Seg_1429" s="T129">then</ta>
            <ta e="T131" id="Seg_1430" s="T130">go-PST-1SG</ta>
            <ta e="T132" id="Seg_1431" s="T131">earth.[NOM.SG]</ta>
            <ta e="T133" id="Seg_1432" s="T132">plough-PST-1SG</ta>
            <ta e="T134" id="Seg_1433" s="T133">and</ta>
            <ta e="T135" id="Seg_1434" s="T134">seat-PST-1SG</ta>
            <ta e="T137" id="Seg_1435" s="T136">cabbage-ACC</ta>
            <ta e="T138" id="Seg_1436" s="T137">and</ta>
            <ta e="T139" id="Seg_1437" s="T138">water.[NOM.SG]</ta>
            <ta e="T140" id="Seg_1438" s="T139">and</ta>
            <ta e="T141" id="Seg_1439" s="T140">water.[NOM.SG]</ta>
            <ta e="T142" id="Seg_1440" s="T141">pour-PST-1SG</ta>
            <ta e="T143" id="Seg_1441" s="T142">then</ta>
            <ta e="T144" id="Seg_1442" s="T143">evening-LOC.ADV</ta>
            <ta e="T145" id="Seg_1443" s="T144">sleep-CVB</ta>
            <ta e="T147" id="Seg_1444" s="T146">lie.down-PST-1SG</ta>
            <ta e="T148" id="Seg_1445" s="T147">tomorrow</ta>
            <ta e="T149" id="Seg_1446" s="T148">get.up-PST-1SG</ta>
            <ta e="T150" id="Seg_1447" s="T149">turnip.[NOM.SG]</ta>
            <ta e="T151" id="Seg_1448" s="T150">seat-PST-1SG</ta>
            <ta e="T152" id="Seg_1449" s="T151">then</ta>
            <ta e="T153" id="Seg_1450" s="T152">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1451" s="T0">я.NOM</ta>
            <ta e="T2" id="Seg_1452" s="T1">быть-PST-1SG</ta>
            <ta e="T3" id="Seg_1453" s="T2">Агинское</ta>
            <ta e="T4" id="Seg_1454" s="T3">поселение-LOC</ta>
            <ta e="T5" id="Seg_1455" s="T4">там</ta>
            <ta e="T6" id="Seg_1456" s="T5">Бог-LAT</ta>
            <ta e="T154" id="Seg_1457" s="T6">Бог-EP-GEN</ta>
            <ta e="T7" id="Seg_1458" s="T154">молиться-PST-1SG</ta>
            <ta e="T8" id="Seg_1459" s="T7">тогда</ta>
            <ta e="T10" id="Seg_1460" s="T9">пойти-PST-1SG</ta>
            <ta e="T11" id="Seg_1461" s="T10">чум-LAT/LOC.1SG</ta>
            <ta e="T12" id="Seg_1462" s="T11">прийти-PST-1SG</ta>
            <ta e="T13" id="Seg_1463" s="T12">прийти-PST-1SG</ta>
            <ta e="T14" id="Seg_1464" s="T13">дорога</ta>
            <ta e="T15" id="Seg_1465" s="T14">дорога-LOC</ta>
            <ta e="T16" id="Seg_1466" s="T15">какой=INDEF</ta>
            <ta e="T17" id="Seg_1467" s="T16">NEG</ta>
            <ta e="T18" id="Seg_1468" s="T17">догонять-PST.[3SG]</ta>
            <ta e="T19" id="Seg_1469" s="T18">я.ACC</ta>
            <ta e="T20" id="Seg_1470" s="T19">машина.[NOM.SG]</ta>
            <ta e="T21" id="Seg_1471" s="T20">тогда</ta>
            <ta e="T23" id="Seg_1472" s="T22">дождь.[NOM.SG]</ta>
            <ta e="T24" id="Seg_1473" s="T23">пойти-PST.[3SG]</ta>
            <ta e="T25" id="Seg_1474" s="T24">и</ta>
            <ta e="T26" id="Seg_1475" s="T25">ветер.[NOM.SG]</ta>
            <ta e="T27" id="Seg_1476" s="T26">а</ta>
            <ta e="T28" id="Seg_1477" s="T27">бояться-FUT-2SG</ta>
            <ta e="T29" id="Seg_1478" s="T28">NEG</ta>
            <ta e="T30" id="Seg_1479" s="T29">нести-PST.[3SG]</ta>
            <ta e="T31" id="Seg_1480" s="T30">ветер.[NOM.SG]</ta>
            <ta e="T32" id="Seg_1481" s="T31">я.NOM</ta>
            <ta e="T33" id="Seg_1482" s="T32">прийти-PST-1SG</ta>
            <ta e="T34" id="Seg_1483" s="T33">прийти-PST-1SG</ta>
            <ta e="T35" id="Seg_1484" s="T34">стоять-DUR.[3SG]</ta>
            <ta e="T36" id="Seg_1485" s="T35">машина.[NOM.SG]</ta>
            <ta e="T37" id="Seg_1486" s="T36">тогда</ta>
            <ta e="T38" id="Seg_1487" s="T37">дорога-ACC</ta>
            <ta e="T39" id="Seg_1488" s="T38">исчезнуть-DUR-PST-1SG</ta>
            <ta e="T40" id="Seg_1489" s="T39">и</ta>
            <ta e="T41" id="Seg_1490" s="T40">пойти-CVB</ta>
            <ta e="T42" id="Seg_1491" s="T41">исчезнуть-PST-1SG</ta>
            <ta e="T45" id="Seg_1492" s="T44">там</ta>
            <ta e="T46" id="Seg_1493" s="T45">прийти-PST-1SG</ta>
            <ta e="T47" id="Seg_1494" s="T46">там</ta>
            <ta e="T48" id="Seg_1495" s="T47">ночевать-PST-1SG</ta>
            <ta e="T49" id="Seg_1496" s="T48">там</ta>
            <ta e="T50" id="Seg_1497" s="T49">весь</ta>
            <ta e="T51" id="Seg_1498" s="T50">петь-DUR-3PL</ta>
            <ta e="T52" id="Seg_1499" s="T51">водка.[NOM.SG]</ta>
            <ta e="T53" id="Seg_1500" s="T52">пить-DUR-3PL</ta>
            <ta e="T54" id="Seg_1501" s="T53">ругать-DES-DUR-3PL</ta>
            <ta e="T55" id="Seg_1502" s="T54">тогда</ta>
            <ta e="T56" id="Seg_1503" s="T55">спать-PST-3PL</ta>
            <ta e="T57" id="Seg_1504" s="T56">а</ta>
            <ta e="T58" id="Seg_1505" s="T57">я.NOM</ta>
            <ta e="T59" id="Seg_1506" s="T58">утро</ta>
            <ta e="T60" id="Seg_1507" s="T59">встать-PST-1SG</ta>
            <ta e="T61" id="Seg_1508" s="T60">и</ta>
            <ta e="T62" id="Seg_1509" s="T61">пойти-PST-1SG</ta>
            <ta e="T63" id="Seg_1510" s="T62">прийти-PST-1SG</ta>
            <ta e="T64" id="Seg_1511" s="T63">прийти-PST-1SG</ta>
            <ta e="T65" id="Seg_1512" s="T64">я.NOM</ta>
            <ta e="T66" id="Seg_1513" s="T65">хлеб.[NOM.SG]</ta>
            <ta e="T67" id="Seg_1514" s="T66">быть-PST.[3SG]</ta>
            <ta e="T68" id="Seg_1515" s="T67">река-GEN</ta>
            <ta e="T69" id="Seg_1516" s="T68">край-LAT/LOC.3SG</ta>
            <ta e="T70" id="Seg_1517" s="T69">сидеть-PST-1SG</ta>
            <ta e="T71" id="Seg_1518" s="T70">тогда</ta>
            <ta e="T72" id="Seg_1519" s="T71">опять</ta>
            <ta e="T73" id="Seg_1520" s="T72">пойти-PST-1SG</ta>
            <ta e="T74" id="Seg_1521" s="T73">прийти-PRS-1SG</ta>
            <ta e="T75" id="Seg_1522" s="T74">видеть-PRS-1SG</ta>
            <ta e="T76" id="Seg_1523" s="T75">дом-PL</ta>
            <ta e="T77" id="Seg_1524" s="T76">стоять-DUR-3PL</ta>
            <ta e="T78" id="Seg_1525" s="T77">скоро</ta>
            <ta e="T79" id="Seg_1526" s="T78">там</ta>
            <ta e="T80" id="Seg_1527" s="T79">дом-PL-LAT</ta>
            <ta e="T81" id="Seg_1528" s="T80">пойти-FUT-1SG</ta>
            <ta e="T82" id="Seg_1529" s="T81">тогда</ta>
            <ta e="T83" id="Seg_1530" s="T82">идти-PRS-1SG</ta>
            <ta e="T84" id="Seg_1531" s="T83">идти-PRS-1SG</ta>
            <ta e="T85" id="Seg_1532" s="T84">думать-PST-1SG</ta>
            <ta e="T86" id="Seg_1533" s="T85">дорога.[NOM.SG]</ta>
            <ta e="T87" id="Seg_1534" s="T86">этот.[NOM.SG]</ta>
            <ta e="T88" id="Seg_1535" s="T87">здесь</ta>
            <ta e="T89" id="Seg_1536" s="T88">пойти-FUT-3SG</ta>
            <ta e="T90" id="Seg_1537" s="T89">тогда</ta>
            <ta e="T91" id="Seg_1538" s="T90">видеть-PRS-1SG</ta>
            <ta e="T92" id="Seg_1539" s="T91">NEG</ta>
            <ta e="T93" id="Seg_1540" s="T92">NEG</ta>
            <ta e="T94" id="Seg_1541" s="T93">этот-LAT</ta>
            <ta e="T95" id="Seg_1542" s="T94">пойти-DUR-1SG</ta>
            <ta e="T96" id="Seg_1543" s="T95">тогда</ta>
            <ta e="T97" id="Seg_1544" s="T96">вернуться-MOM-PST-1SG</ta>
            <ta e="T98" id="Seg_1545" s="T97">и</ta>
            <ta e="T99" id="Seg_1546" s="T98">Малиновское.[NOM.SG]</ta>
            <ta e="T100" id="Seg_1547" s="T99">дорога-LAT</ta>
            <ta e="T101" id="Seg_1548" s="T100">прийти-PST-1SG</ta>
            <ta e="T102" id="Seg_1549" s="T101">тогда</ta>
            <ta e="T103" id="Seg_1550" s="T102">прийти-PST-1SG</ta>
            <ta e="T104" id="Seg_1551" s="T103">там</ta>
            <ta e="T105" id="Seg_1552" s="T104">пшеница.[NOM.SG]</ta>
            <ta e="T106" id="Seg_1553" s="T105">пшеница.[NOM.SG]</ta>
            <ta e="T107" id="Seg_1554" s="T106">сеять-PST-3PL</ta>
            <ta e="T108" id="Seg_1555" s="T107">я.NOM</ta>
            <ta e="T109" id="Seg_1556" s="T108">пойти-PST-1SG</ta>
            <ta e="T110" id="Seg_1557" s="T109">прийти-PST-1SG</ta>
            <ta e="T111" id="Seg_1558" s="T110">тогда</ta>
            <ta e="T112" id="Seg_1559" s="T111">прийти-PST-1SG</ta>
            <ta e="T113" id="Seg_1560" s="T112">чум-LAT/LOC.1SG</ta>
            <ta e="T114" id="Seg_1561" s="T113">десять.[NOM.SG]</ta>
            <ta e="T115" id="Seg_1562" s="T114">десять.[NOM.SG]</ta>
            <ta e="T116" id="Seg_1563" s="T115">час.PL-PL</ta>
            <ta e="T117" id="Seg_1564" s="T116">быть-PST-3PL</ta>
            <ta e="T119" id="Seg_1565" s="T118">здесь</ta>
            <ta e="T120" id="Seg_1566" s="T119">мыть-DUR-3PL</ta>
            <ta e="T121" id="Seg_1567" s="T120">%%</ta>
            <ta e="T122" id="Seg_1568" s="T121">тогда</ta>
            <ta e="T123" id="Seg_1569" s="T122">прийти-PST-1SG</ta>
            <ta e="T124" id="Seg_1570" s="T123">чум-LAT/LOC.1SG</ta>
            <ta e="T125" id="Seg_1571" s="T124">и</ta>
            <ta e="T126" id="Seg_1572" s="T125">съесть-PST-1SG</ta>
            <ta e="T127" id="Seg_1573" s="T126">хлеб.[NOM.SG]</ta>
            <ta e="T129" id="Seg_1574" s="T128">спать-PST-1SG</ta>
            <ta e="T130" id="Seg_1575" s="T129">тогда</ta>
            <ta e="T131" id="Seg_1576" s="T130">пойти-PST-1SG</ta>
            <ta e="T132" id="Seg_1577" s="T131">земля.[NOM.SG]</ta>
            <ta e="T133" id="Seg_1578" s="T132">пахать-PST-1SG</ta>
            <ta e="T134" id="Seg_1579" s="T133">и</ta>
            <ta e="T135" id="Seg_1580" s="T134">сажать-PST-1SG</ta>
            <ta e="T137" id="Seg_1581" s="T136">капуста-ACC</ta>
            <ta e="T138" id="Seg_1582" s="T137">и</ta>
            <ta e="T139" id="Seg_1583" s="T138">вода.[NOM.SG]</ta>
            <ta e="T140" id="Seg_1584" s="T139">и</ta>
            <ta e="T141" id="Seg_1585" s="T140">вода.[NOM.SG]</ta>
            <ta e="T142" id="Seg_1586" s="T141">лить-PST-1SG</ta>
            <ta e="T143" id="Seg_1587" s="T142">тогда</ta>
            <ta e="T144" id="Seg_1588" s="T143">вечер-LOC.ADV</ta>
            <ta e="T145" id="Seg_1589" s="T144">спать-CVB</ta>
            <ta e="T147" id="Seg_1590" s="T146">ложиться-PST-1SG</ta>
            <ta e="T148" id="Seg_1591" s="T147">завтра</ta>
            <ta e="T149" id="Seg_1592" s="T148">встать-PST-1SG</ta>
            <ta e="T150" id="Seg_1593" s="T149">брюква.[NOM.SG]</ta>
            <ta e="T151" id="Seg_1594" s="T150">сажать-PST-1SG</ta>
            <ta e="T152" id="Seg_1595" s="T151">тогда</ta>
            <ta e="T153" id="Seg_1596" s="T152">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1597" s="T0">pers</ta>
            <ta e="T2" id="Seg_1598" s="T1">v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_1599" s="T2">propr</ta>
            <ta e="T4" id="Seg_1600" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_1601" s="T4">adv</ta>
            <ta e="T6" id="Seg_1602" s="T5">n-n:case</ta>
            <ta e="T154" id="Seg_1603" s="T6">n-n:ins-n:case</ta>
            <ta e="T7" id="Seg_1604" s="T154">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_1605" s="T7">adv</ta>
            <ta e="T10" id="Seg_1606" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_1607" s="T10">n-n:case.poss</ta>
            <ta e="T12" id="Seg_1608" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_1609" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_1610" s="T13">n</ta>
            <ta e="T15" id="Seg_1611" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_1612" s="T15">que=ptcl</ta>
            <ta e="T17" id="Seg_1613" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_1614" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_1615" s="T18">pers</ta>
            <ta e="T20" id="Seg_1616" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_1617" s="T20">adv</ta>
            <ta e="T23" id="Seg_1618" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_1619" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_1620" s="T24">conj</ta>
            <ta e="T26" id="Seg_1621" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_1622" s="T26">conj</ta>
            <ta e="T28" id="Seg_1623" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_1624" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_1625" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_1626" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_1627" s="T31">pers</ta>
            <ta e="T33" id="Seg_1628" s="T32">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_1629" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_1630" s="T34">v-v&gt;v-v:pn</ta>
            <ta e="T36" id="Seg_1631" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_1632" s="T36">adv</ta>
            <ta e="T38" id="Seg_1633" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_1634" s="T38">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_1635" s="T39">conj</ta>
            <ta e="T41" id="Seg_1636" s="T40">v-v:n.fin</ta>
            <ta e="T42" id="Seg_1637" s="T41">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_1638" s="T44">adv</ta>
            <ta e="T46" id="Seg_1639" s="T45">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_1640" s="T46">adv</ta>
            <ta e="T48" id="Seg_1641" s="T47">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_1642" s="T48">adv</ta>
            <ta e="T50" id="Seg_1643" s="T49">quant</ta>
            <ta e="T51" id="Seg_1644" s="T50">v-v&gt;v-v:pn</ta>
            <ta e="T52" id="Seg_1645" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_1646" s="T52">v-v&gt;v-v:pn</ta>
            <ta e="T54" id="Seg_1647" s="T53">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T55" id="Seg_1648" s="T54">adv</ta>
            <ta e="T56" id="Seg_1649" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_1650" s="T56">conj</ta>
            <ta e="T58" id="Seg_1651" s="T57">pers</ta>
            <ta e="T59" id="Seg_1652" s="T58">n</ta>
            <ta e="T60" id="Seg_1653" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_1654" s="T60">conj</ta>
            <ta e="T62" id="Seg_1655" s="T61">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_1656" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_1657" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_1658" s="T64">pers</ta>
            <ta e="T66" id="Seg_1659" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_1660" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_1661" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_1662" s="T68">n-n:case.poss</ta>
            <ta e="T70" id="Seg_1663" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_1664" s="T70">adv</ta>
            <ta e="T72" id="Seg_1665" s="T71">adv</ta>
            <ta e="T73" id="Seg_1666" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_1667" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_1668" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_1669" s="T75">n-n:num</ta>
            <ta e="T77" id="Seg_1670" s="T76">v-v&gt;v-v:pn</ta>
            <ta e="T78" id="Seg_1671" s="T77">adv</ta>
            <ta e="T79" id="Seg_1672" s="T78">adv</ta>
            <ta e="T80" id="Seg_1673" s="T79">n-n:num-n:case</ta>
            <ta e="T81" id="Seg_1674" s="T80">v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_1675" s="T81">adv</ta>
            <ta e="T83" id="Seg_1676" s="T82">v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_1677" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_1678" s="T84">v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_1679" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_1680" s="T86">dempro-n:case</ta>
            <ta e="T88" id="Seg_1681" s="T87">adv</ta>
            <ta e="T89" id="Seg_1682" s="T88">v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_1683" s="T89">adv</ta>
            <ta e="T91" id="Seg_1684" s="T90">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_1685" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_1686" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_1687" s="T93">dempro-n:case</ta>
            <ta e="T95" id="Seg_1688" s="T94">v-v&gt;v-v:pn</ta>
            <ta e="T96" id="Seg_1689" s="T95">adv</ta>
            <ta e="T97" id="Seg_1690" s="T96">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T98" id="Seg_1691" s="T97">conj</ta>
            <ta e="T99" id="Seg_1692" s="T98">propr-n:case</ta>
            <ta e="T100" id="Seg_1693" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_1694" s="T100">v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_1695" s="T101">adv</ta>
            <ta e="T103" id="Seg_1696" s="T102">v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_1697" s="T103">adv</ta>
            <ta e="T105" id="Seg_1698" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_1699" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_1700" s="T106">v-v:tense-v:pn</ta>
            <ta e="T108" id="Seg_1701" s="T107">pers</ta>
            <ta e="T109" id="Seg_1702" s="T108">v-v:tense-v:pn</ta>
            <ta e="T110" id="Seg_1703" s="T109">v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_1704" s="T110">adv</ta>
            <ta e="T112" id="Seg_1705" s="T111">v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_1706" s="T112">n-n:case.poss</ta>
            <ta e="T114" id="Seg_1707" s="T113">num-n:case</ta>
            <ta e="T115" id="Seg_1708" s="T114">num-n:case</ta>
            <ta e="T116" id="Seg_1709" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_1710" s="T116">v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_1711" s="T118">adv</ta>
            <ta e="T120" id="Seg_1712" s="T119">v-v&gt;v-v:pn</ta>
            <ta e="T121" id="Seg_1713" s="T120">%%</ta>
            <ta e="T122" id="Seg_1714" s="T121">adv</ta>
            <ta e="T123" id="Seg_1715" s="T122">v-v:tense-v:pn</ta>
            <ta e="T124" id="Seg_1716" s="T123">n-n:case.poss</ta>
            <ta e="T125" id="Seg_1717" s="T124">conj</ta>
            <ta e="T126" id="Seg_1718" s="T125">v-v:tense-v:pn</ta>
            <ta e="T127" id="Seg_1719" s="T126">n-n:case</ta>
            <ta e="T129" id="Seg_1720" s="T128">v-v:tense-v:pn</ta>
            <ta e="T130" id="Seg_1721" s="T129">adv</ta>
            <ta e="T131" id="Seg_1722" s="T130">v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_1723" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_1724" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_1725" s="T133">conj</ta>
            <ta e="T135" id="Seg_1726" s="T134">v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_1727" s="T136">n-n:case</ta>
            <ta e="T138" id="Seg_1728" s="T137">conj</ta>
            <ta e="T139" id="Seg_1729" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_1730" s="T139">conj</ta>
            <ta e="T141" id="Seg_1731" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_1732" s="T141">v-v:tense-v:pn</ta>
            <ta e="T143" id="Seg_1733" s="T142">adv</ta>
            <ta e="T144" id="Seg_1734" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_1735" s="T144">v-v:n.fin</ta>
            <ta e="T147" id="Seg_1736" s="T146">v-v:tense-v:pn</ta>
            <ta e="T148" id="Seg_1737" s="T147">adv</ta>
            <ta e="T149" id="Seg_1738" s="T148">v-v:tense-v:pn</ta>
            <ta e="T150" id="Seg_1739" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_1740" s="T150">v-v:tense-v:pn</ta>
            <ta e="T152" id="Seg_1741" s="T151">adv</ta>
            <ta e="T153" id="Seg_1742" s="T152">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1743" s="T0">pers</ta>
            <ta e="T2" id="Seg_1744" s="T1">v</ta>
            <ta e="T3" id="Seg_1745" s="T2">propr</ta>
            <ta e="T4" id="Seg_1746" s="T3">n</ta>
            <ta e="T5" id="Seg_1747" s="T4">adv</ta>
            <ta e="T6" id="Seg_1748" s="T5">n</ta>
            <ta e="T154" id="Seg_1749" s="T6">n</ta>
            <ta e="T7" id="Seg_1750" s="T154">v</ta>
            <ta e="T8" id="Seg_1751" s="T7">adv</ta>
            <ta e="T10" id="Seg_1752" s="T9">v</ta>
            <ta e="T11" id="Seg_1753" s="T10">n</ta>
            <ta e="T12" id="Seg_1754" s="T11">v</ta>
            <ta e="T13" id="Seg_1755" s="T12">v</ta>
            <ta e="T14" id="Seg_1756" s="T13">n</ta>
            <ta e="T15" id="Seg_1757" s="T14">n</ta>
            <ta e="T16" id="Seg_1758" s="T15">que</ta>
            <ta e="T17" id="Seg_1759" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_1760" s="T17">v</ta>
            <ta e="T19" id="Seg_1761" s="T18">pers</ta>
            <ta e="T20" id="Seg_1762" s="T19">n</ta>
            <ta e="T21" id="Seg_1763" s="T20">adv</ta>
            <ta e="T23" id="Seg_1764" s="T22">n</ta>
            <ta e="T24" id="Seg_1765" s="T23">v</ta>
            <ta e="T25" id="Seg_1766" s="T24">conj</ta>
            <ta e="T26" id="Seg_1767" s="T25">n</ta>
            <ta e="T27" id="Seg_1768" s="T26">conj</ta>
            <ta e="T28" id="Seg_1769" s="T27">v</ta>
            <ta e="T29" id="Seg_1770" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_1771" s="T29">v</ta>
            <ta e="T31" id="Seg_1772" s="T30">n</ta>
            <ta e="T32" id="Seg_1773" s="T31">pers</ta>
            <ta e="T33" id="Seg_1774" s="T32">v</ta>
            <ta e="T34" id="Seg_1775" s="T33">v</ta>
            <ta e="T35" id="Seg_1776" s="T34">v</ta>
            <ta e="T36" id="Seg_1777" s="T35">n</ta>
            <ta e="T37" id="Seg_1778" s="T36">adv</ta>
            <ta e="T38" id="Seg_1779" s="T37">n</ta>
            <ta e="T39" id="Seg_1780" s="T38">v</ta>
            <ta e="T40" id="Seg_1781" s="T39">conj</ta>
            <ta e="T41" id="Seg_1782" s="T40">v</ta>
            <ta e="T42" id="Seg_1783" s="T41">v</ta>
            <ta e="T45" id="Seg_1784" s="T44">adv</ta>
            <ta e="T46" id="Seg_1785" s="T45">v</ta>
            <ta e="T47" id="Seg_1786" s="T46">adv</ta>
            <ta e="T48" id="Seg_1787" s="T47">v</ta>
            <ta e="T49" id="Seg_1788" s="T48">adv</ta>
            <ta e="T50" id="Seg_1789" s="T49">quant</ta>
            <ta e="T51" id="Seg_1790" s="T50">v</ta>
            <ta e="T52" id="Seg_1791" s="T51">n</ta>
            <ta e="T53" id="Seg_1792" s="T52">v</ta>
            <ta e="T54" id="Seg_1793" s="T53">v</ta>
            <ta e="T55" id="Seg_1794" s="T54">adv</ta>
            <ta e="T56" id="Seg_1795" s="T55">v</ta>
            <ta e="T57" id="Seg_1796" s="T56">conj</ta>
            <ta e="T58" id="Seg_1797" s="T57">pers</ta>
            <ta e="T59" id="Seg_1798" s="T58">n</ta>
            <ta e="T60" id="Seg_1799" s="T59">v</ta>
            <ta e="T61" id="Seg_1800" s="T60">conj</ta>
            <ta e="T62" id="Seg_1801" s="T61">v</ta>
            <ta e="T63" id="Seg_1802" s="T62">v</ta>
            <ta e="T64" id="Seg_1803" s="T63">v</ta>
            <ta e="T65" id="Seg_1804" s="T64">pers</ta>
            <ta e="T66" id="Seg_1805" s="T65">n</ta>
            <ta e="T67" id="Seg_1806" s="T66">v</ta>
            <ta e="T68" id="Seg_1807" s="T67">n</ta>
            <ta e="T69" id="Seg_1808" s="T68">n</ta>
            <ta e="T70" id="Seg_1809" s="T69">v</ta>
            <ta e="T71" id="Seg_1810" s="T70">adv</ta>
            <ta e="T72" id="Seg_1811" s="T71">adv</ta>
            <ta e="T73" id="Seg_1812" s="T72">v</ta>
            <ta e="T74" id="Seg_1813" s="T73">v</ta>
            <ta e="T75" id="Seg_1814" s="T74">v</ta>
            <ta e="T76" id="Seg_1815" s="T75">n</ta>
            <ta e="T77" id="Seg_1816" s="T76">v</ta>
            <ta e="T78" id="Seg_1817" s="T77">adv</ta>
            <ta e="T79" id="Seg_1818" s="T78">adv</ta>
            <ta e="T80" id="Seg_1819" s="T79">n</ta>
            <ta e="T81" id="Seg_1820" s="T80">v</ta>
            <ta e="T82" id="Seg_1821" s="T81">adv</ta>
            <ta e="T83" id="Seg_1822" s="T82">v</ta>
            <ta e="T84" id="Seg_1823" s="T83">v</ta>
            <ta e="T85" id="Seg_1824" s="T84">v</ta>
            <ta e="T86" id="Seg_1825" s="T85">n</ta>
            <ta e="T87" id="Seg_1826" s="T86">dempro</ta>
            <ta e="T88" id="Seg_1827" s="T87">adv</ta>
            <ta e="T89" id="Seg_1828" s="T88">v</ta>
            <ta e="T90" id="Seg_1829" s="T89">adv</ta>
            <ta e="T91" id="Seg_1830" s="T90">v</ta>
            <ta e="T92" id="Seg_1831" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_1832" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_1833" s="T93">dempro</ta>
            <ta e="T95" id="Seg_1834" s="T94">v</ta>
            <ta e="T96" id="Seg_1835" s="T95">adv</ta>
            <ta e="T97" id="Seg_1836" s="T96">v</ta>
            <ta e="T98" id="Seg_1837" s="T97">conj</ta>
            <ta e="T99" id="Seg_1838" s="T98">propr</ta>
            <ta e="T100" id="Seg_1839" s="T99">n</ta>
            <ta e="T101" id="Seg_1840" s="T100">v</ta>
            <ta e="T102" id="Seg_1841" s="T101">adv</ta>
            <ta e="T103" id="Seg_1842" s="T102">v</ta>
            <ta e="T104" id="Seg_1843" s="T103">adv</ta>
            <ta e="T105" id="Seg_1844" s="T104">n</ta>
            <ta e="T106" id="Seg_1845" s="T105">n</ta>
            <ta e="T107" id="Seg_1846" s="T106">v</ta>
            <ta e="T108" id="Seg_1847" s="T107">pers</ta>
            <ta e="T109" id="Seg_1848" s="T108">v</ta>
            <ta e="T110" id="Seg_1849" s="T109">v</ta>
            <ta e="T111" id="Seg_1850" s="T110">adv</ta>
            <ta e="T112" id="Seg_1851" s="T111">v</ta>
            <ta e="T113" id="Seg_1852" s="T112">n</ta>
            <ta e="T114" id="Seg_1853" s="T113">num</ta>
            <ta e="T115" id="Seg_1854" s="T114">num</ta>
            <ta e="T116" id="Seg_1855" s="T115">n</ta>
            <ta e="T117" id="Seg_1856" s="T116">v</ta>
            <ta e="T119" id="Seg_1857" s="T118">adv</ta>
            <ta e="T120" id="Seg_1858" s="T119">v</ta>
            <ta e="T122" id="Seg_1859" s="T121">adv</ta>
            <ta e="T123" id="Seg_1860" s="T122">v</ta>
            <ta e="T124" id="Seg_1861" s="T123">n</ta>
            <ta e="T125" id="Seg_1862" s="T124">conj</ta>
            <ta e="T126" id="Seg_1863" s="T125">v</ta>
            <ta e="T127" id="Seg_1864" s="T126">n</ta>
            <ta e="T129" id="Seg_1865" s="T128">v</ta>
            <ta e="T130" id="Seg_1866" s="T129">adv</ta>
            <ta e="T131" id="Seg_1867" s="T130">v</ta>
            <ta e="T132" id="Seg_1868" s="T131">n</ta>
            <ta e="T133" id="Seg_1869" s="T132">v</ta>
            <ta e="T134" id="Seg_1870" s="T133">conj</ta>
            <ta e="T135" id="Seg_1871" s="T134">v</ta>
            <ta e="T137" id="Seg_1872" s="T136">n</ta>
            <ta e="T138" id="Seg_1873" s="T137">conj</ta>
            <ta e="T139" id="Seg_1874" s="T138">n</ta>
            <ta e="T140" id="Seg_1875" s="T139">conj</ta>
            <ta e="T141" id="Seg_1876" s="T140">n</ta>
            <ta e="T142" id="Seg_1877" s="T141">v</ta>
            <ta e="T143" id="Seg_1878" s="T142">adv</ta>
            <ta e="T144" id="Seg_1879" s="T143">n</ta>
            <ta e="T145" id="Seg_1880" s="T144">v</ta>
            <ta e="T147" id="Seg_1881" s="T146">v</ta>
            <ta e="T148" id="Seg_1882" s="T147">adv</ta>
            <ta e="T149" id="Seg_1883" s="T148">v</ta>
            <ta e="T150" id="Seg_1884" s="T149">n</ta>
            <ta e="T151" id="Seg_1885" s="T150">v</ta>
            <ta e="T152" id="Seg_1886" s="T151">adv</ta>
            <ta e="T153" id="Seg_1887" s="T152">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1888" s="T0">pro.h:Th</ta>
            <ta e="T4" id="Seg_1889" s="T3">np:L</ta>
            <ta e="T5" id="Seg_1890" s="T4">adv:L</ta>
            <ta e="T6" id="Seg_1891" s="T5">np:R</ta>
            <ta e="T7" id="Seg_1892" s="T154">0.1.h:A</ta>
            <ta e="T8" id="Seg_1893" s="T7">adv:Time</ta>
            <ta e="T10" id="Seg_1894" s="T9">0.1.h:A</ta>
            <ta e="T11" id="Seg_1895" s="T10">np:G</ta>
            <ta e="T12" id="Seg_1896" s="T11">0.1.h:A</ta>
            <ta e="T13" id="Seg_1897" s="T12">0.1.h:A</ta>
            <ta e="T15" id="Seg_1898" s="T14">np:Pth</ta>
            <ta e="T19" id="Seg_1899" s="T18">pro.h:Th</ta>
            <ta e="T20" id="Seg_1900" s="T19">np:A</ta>
            <ta e="T21" id="Seg_1901" s="T20">adv:Time</ta>
            <ta e="T23" id="Seg_1902" s="T22">np:Th</ta>
            <ta e="T26" id="Seg_1903" s="T25">np:Th</ta>
            <ta e="T28" id="Seg_1904" s="T27">0.2.h:E</ta>
            <ta e="T31" id="Seg_1905" s="T30">np:Cau</ta>
            <ta e="T32" id="Seg_1906" s="T31">pro.h:A</ta>
            <ta e="T34" id="Seg_1907" s="T33">0.1.h:A</ta>
            <ta e="T36" id="Seg_1908" s="T35">np:Th</ta>
            <ta e="T37" id="Seg_1909" s="T36">adv:Time</ta>
            <ta e="T38" id="Seg_1910" s="T37">np:Th</ta>
            <ta e="T39" id="Seg_1911" s="T38">0.1.h:E</ta>
            <ta e="T42" id="Seg_1912" s="T41">0.1.h:A</ta>
            <ta e="T45" id="Seg_1913" s="T44">adv:L</ta>
            <ta e="T46" id="Seg_1914" s="T45">0.1.h:A</ta>
            <ta e="T47" id="Seg_1915" s="T46">adv:L</ta>
            <ta e="T48" id="Seg_1916" s="T47">0.1.h:A</ta>
            <ta e="T49" id="Seg_1917" s="T48">adv:L</ta>
            <ta e="T50" id="Seg_1918" s="T49">np.h:A</ta>
            <ta e="T52" id="Seg_1919" s="T51">np:P</ta>
            <ta e="T53" id="Seg_1920" s="T52">0.3.h:A</ta>
            <ta e="T54" id="Seg_1921" s="T53">0.3.h:A</ta>
            <ta e="T55" id="Seg_1922" s="T54">adv:Time</ta>
            <ta e="T56" id="Seg_1923" s="T55">0.3.h:E</ta>
            <ta e="T58" id="Seg_1924" s="T57">pro.h:A</ta>
            <ta e="T59" id="Seg_1925" s="T58">n:Time</ta>
            <ta e="T62" id="Seg_1926" s="T61">0.1.h:A</ta>
            <ta e="T63" id="Seg_1927" s="T62">0.1.h:A</ta>
            <ta e="T64" id="Seg_1928" s="T63">0.1.h:A</ta>
            <ta e="T65" id="Seg_1929" s="T64">pro.h:Poss</ta>
            <ta e="T66" id="Seg_1930" s="T65">np:Th</ta>
            <ta e="T68" id="Seg_1931" s="T67">np:Poss</ta>
            <ta e="T69" id="Seg_1932" s="T68">np:L</ta>
            <ta e="T70" id="Seg_1933" s="T69">0.1.h:A</ta>
            <ta e="T71" id="Seg_1934" s="T70">adv:Time</ta>
            <ta e="T73" id="Seg_1935" s="T72">0.1.h:A</ta>
            <ta e="T74" id="Seg_1936" s="T73">0.1.h:A</ta>
            <ta e="T75" id="Seg_1937" s="T74">0.1.h:E</ta>
            <ta e="T76" id="Seg_1938" s="T75">np:Th</ta>
            <ta e="T79" id="Seg_1939" s="T78">adv:L</ta>
            <ta e="T80" id="Seg_1940" s="T79">np:G</ta>
            <ta e="T81" id="Seg_1941" s="T80">0.1.h:A</ta>
            <ta e="T82" id="Seg_1942" s="T81">adv:Time</ta>
            <ta e="T83" id="Seg_1943" s="T82">0.1.h:A</ta>
            <ta e="T84" id="Seg_1944" s="T83">0.1.h:A</ta>
            <ta e="T85" id="Seg_1945" s="T84">0.1.h:E</ta>
            <ta e="T86" id="Seg_1946" s="T85">np:Th</ta>
            <ta e="T87" id="Seg_1947" s="T86">pro:Th</ta>
            <ta e="T88" id="Seg_1948" s="T87">adv:L</ta>
            <ta e="T90" id="Seg_1949" s="T89">adv:Time</ta>
            <ta e="T91" id="Seg_1950" s="T90">0.1.h:E</ta>
            <ta e="T94" id="Seg_1951" s="T93">pro:G</ta>
            <ta e="T95" id="Seg_1952" s="T94">0.1.h:A</ta>
            <ta e="T96" id="Seg_1953" s="T95">adv:Time</ta>
            <ta e="T97" id="Seg_1954" s="T96">0.1.h:A</ta>
            <ta e="T100" id="Seg_1955" s="T99">np:G</ta>
            <ta e="T101" id="Seg_1956" s="T100">0.1.h:A</ta>
            <ta e="T102" id="Seg_1957" s="T101">adv:Time</ta>
            <ta e="T103" id="Seg_1958" s="T102">0.1.h:A</ta>
            <ta e="T104" id="Seg_1959" s="T103">adv:L</ta>
            <ta e="T106" id="Seg_1960" s="T105">np:Th</ta>
            <ta e="T107" id="Seg_1961" s="T106">0.3.h:A</ta>
            <ta e="T108" id="Seg_1962" s="T107">pro.h:A</ta>
            <ta e="T110" id="Seg_1963" s="T109">0.1.h:A</ta>
            <ta e="T111" id="Seg_1964" s="T110">adv:Time</ta>
            <ta e="T112" id="Seg_1965" s="T111">0.1.h:A</ta>
            <ta e="T113" id="Seg_1966" s="T112">np:G</ta>
            <ta e="T116" id="Seg_1967" s="T115">n:Time</ta>
            <ta e="T119" id="Seg_1968" s="T118">adv:L</ta>
            <ta e="T120" id="Seg_1969" s="T119">0.3.h:A</ta>
            <ta e="T122" id="Seg_1970" s="T121">adv:Time</ta>
            <ta e="T123" id="Seg_1971" s="T122">0.1.h:A</ta>
            <ta e="T124" id="Seg_1972" s="T123">np:G</ta>
            <ta e="T126" id="Seg_1973" s="T125">0.1.h:A</ta>
            <ta e="T127" id="Seg_1974" s="T126">np:P</ta>
            <ta e="T129" id="Seg_1975" s="T128">0.1.h:E</ta>
            <ta e="T130" id="Seg_1976" s="T129">adv:Time</ta>
            <ta e="T131" id="Seg_1977" s="T130">0.1.h:A</ta>
            <ta e="T132" id="Seg_1978" s="T131">np:P</ta>
            <ta e="T133" id="Seg_1979" s="T132">0.1.h:A</ta>
            <ta e="T135" id="Seg_1980" s="T134">0.1.h:A</ta>
            <ta e="T137" id="Seg_1981" s="T136">np:Th</ta>
            <ta e="T139" id="Seg_1982" s="T138">np:Th</ta>
            <ta e="T141" id="Seg_1983" s="T140">np:Th</ta>
            <ta e="T142" id="Seg_1984" s="T141">0.1.h:A</ta>
            <ta e="T143" id="Seg_1985" s="T142">adv:Time</ta>
            <ta e="T144" id="Seg_1986" s="T143">n:Time</ta>
            <ta e="T147" id="Seg_1987" s="T146">0.1.h:A</ta>
            <ta e="T148" id="Seg_1988" s="T147">adv:Time</ta>
            <ta e="T149" id="Seg_1989" s="T148">0.1.h:A</ta>
            <ta e="T150" id="Seg_1990" s="T149">np:Th</ta>
            <ta e="T151" id="Seg_1991" s="T150">0.1.h:A</ta>
            <ta e="T152" id="Seg_1992" s="T151">adv:Time</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_1993" s="T0">pro.h:S</ta>
            <ta e="T2" id="Seg_1994" s="T1">v:pred</ta>
            <ta e="T7" id="Seg_1995" s="T154">v:pred 0.1.h:S</ta>
            <ta e="T10" id="Seg_1996" s="T9">v:pred 0.1.h:S</ta>
            <ta e="T12" id="Seg_1997" s="T11">v:pred 0.1.h:S</ta>
            <ta e="T13" id="Seg_1998" s="T12">v:pred 0.1.h:S</ta>
            <ta e="T18" id="Seg_1999" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_2000" s="T18">pro.h:O</ta>
            <ta e="T20" id="Seg_2001" s="T19">np:S</ta>
            <ta e="T23" id="Seg_2002" s="T22">np:S</ta>
            <ta e="T24" id="Seg_2003" s="T23">v:pred</ta>
            <ta e="T26" id="Seg_2004" s="T25">np:S</ta>
            <ta e="T28" id="Seg_2005" s="T27">v:pred 0.2.h:S</ta>
            <ta e="T29" id="Seg_2006" s="T28">ptcl.neg</ta>
            <ta e="T30" id="Seg_2007" s="T29">v:pred</ta>
            <ta e="T31" id="Seg_2008" s="T30">np:S</ta>
            <ta e="T32" id="Seg_2009" s="T31">pro.h:S</ta>
            <ta e="T33" id="Seg_2010" s="T32">v:pred</ta>
            <ta e="T34" id="Seg_2011" s="T33">v:pred 0.1.h:S</ta>
            <ta e="T35" id="Seg_2012" s="T34">v:pred</ta>
            <ta e="T36" id="Seg_2013" s="T35">np:S</ta>
            <ta e="T38" id="Seg_2014" s="T37">np:O</ta>
            <ta e="T39" id="Seg_2015" s="T38">v:pred 0.1.h:S</ta>
            <ta e="T41" id="Seg_2016" s="T40">conv:pred</ta>
            <ta e="T42" id="Seg_2017" s="T41">v:pred 0.1.h:S</ta>
            <ta e="T46" id="Seg_2018" s="T45">v:pred 0.1.h:S</ta>
            <ta e="T48" id="Seg_2019" s="T47">v:pred 0.1.h:S</ta>
            <ta e="T50" id="Seg_2020" s="T49">np.h:S</ta>
            <ta e="T51" id="Seg_2021" s="T50">v:pred</ta>
            <ta e="T52" id="Seg_2022" s="T51">np:O</ta>
            <ta e="T53" id="Seg_2023" s="T52">v:pred 0.3.h:S</ta>
            <ta e="T54" id="Seg_2024" s="T53">v:pred 0.3.h:S</ta>
            <ta e="T56" id="Seg_2025" s="T55">v:pred 0.3.h:S</ta>
            <ta e="T58" id="Seg_2026" s="T57">pro.h:S</ta>
            <ta e="T60" id="Seg_2027" s="T59">v:pred</ta>
            <ta e="T62" id="Seg_2028" s="T61">v:pred 0.1.h:S</ta>
            <ta e="T63" id="Seg_2029" s="T62">v:pred 0.1.h:S</ta>
            <ta e="T64" id="Seg_2030" s="T63">v:pred 0.1.h:S</ta>
            <ta e="T66" id="Seg_2031" s="T65">np:S</ta>
            <ta e="T67" id="Seg_2032" s="T66">v:pred</ta>
            <ta e="T70" id="Seg_2033" s="T69">v:pred 0.1.h:S</ta>
            <ta e="T73" id="Seg_2034" s="T72">v:pred 0.1.h:S</ta>
            <ta e="T74" id="Seg_2035" s="T73">v:pred 0.1.h:S</ta>
            <ta e="T75" id="Seg_2036" s="T74">v:pred 0.1.h:S</ta>
            <ta e="T76" id="Seg_2037" s="T75">np:S</ta>
            <ta e="T77" id="Seg_2038" s="T76">v:pred</ta>
            <ta e="T81" id="Seg_2039" s="T80">v:pred 0.1.h:S</ta>
            <ta e="T83" id="Seg_2040" s="T82">v:pred 0.1.h:S</ta>
            <ta e="T84" id="Seg_2041" s="T83">v:pred 0.1.h:S</ta>
            <ta e="T85" id="Seg_2042" s="T84">v:pred 0.1.h:S</ta>
            <ta e="T86" id="Seg_2043" s="T85">np:O</ta>
            <ta e="T87" id="Seg_2044" s="T86">pro:S</ta>
            <ta e="T89" id="Seg_2045" s="T88">v:pred</ta>
            <ta e="T91" id="Seg_2046" s="T90">v:pred 0.1.h:S</ta>
            <ta e="T93" id="Seg_2047" s="T92">ptcl.neg</ta>
            <ta e="T95" id="Seg_2048" s="T94">v:pred 0.1.h:S</ta>
            <ta e="T97" id="Seg_2049" s="T96">v:pred 0.1.h:S</ta>
            <ta e="T101" id="Seg_2050" s="T100">v:pred 0.1.h:S</ta>
            <ta e="T103" id="Seg_2051" s="T102">v:pred 0.1.h:S</ta>
            <ta e="T106" id="Seg_2052" s="T105">np:O</ta>
            <ta e="T107" id="Seg_2053" s="T106">v:pred 0.3.h:S</ta>
            <ta e="T108" id="Seg_2054" s="T107">pro.h:S</ta>
            <ta e="T109" id="Seg_2055" s="T108">v:pred</ta>
            <ta e="T110" id="Seg_2056" s="T109">v:pred 0.1.h:S</ta>
            <ta e="T112" id="Seg_2057" s="T111">v:pred 0.1.h:S</ta>
            <ta e="T117" id="Seg_2058" s="T116">v:pred </ta>
            <ta e="T120" id="Seg_2059" s="T119">v:pred 0.3.h:S</ta>
            <ta e="T123" id="Seg_2060" s="T122">v:pred 0.1.h:S</ta>
            <ta e="T126" id="Seg_2061" s="T125">v:pred 0.1.h:S</ta>
            <ta e="T127" id="Seg_2062" s="T126">np:O</ta>
            <ta e="T129" id="Seg_2063" s="T128">v:pred 0.1.h:S</ta>
            <ta e="T131" id="Seg_2064" s="T130">v:pred 0.1.h:S</ta>
            <ta e="T132" id="Seg_2065" s="T131">np:O</ta>
            <ta e="T133" id="Seg_2066" s="T132">v:pred 0.1.h:S</ta>
            <ta e="T135" id="Seg_2067" s="T134">v:pred 0.1.h:S</ta>
            <ta e="T137" id="Seg_2068" s="T136">np:O</ta>
            <ta e="T139" id="Seg_2069" s="T138">np:O</ta>
            <ta e="T141" id="Seg_2070" s="T140">np:O</ta>
            <ta e="T142" id="Seg_2071" s="T141">v:pred 0.1.h:S</ta>
            <ta e="T145" id="Seg_2072" s="T144">conv:pred</ta>
            <ta e="T147" id="Seg_2073" s="T146">v:pred 0.1.h:S</ta>
            <ta e="T149" id="Seg_2074" s="T148">v:pred 0.1.h:S</ta>
            <ta e="T150" id="Seg_2075" s="T149">np:O</ta>
            <ta e="T151" id="Seg_2076" s="T150">v:pred 0.1.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_2077" s="T3">TAT:cult</ta>
            <ta e="T6" id="Seg_2078" s="T5">TURK:cult</ta>
            <ta e="T16" id="Seg_2079" s="T15">TURK:gram(INDEF)</ta>
            <ta e="T20" id="Seg_2080" s="T19">RUS:cult</ta>
            <ta e="T25" id="Seg_2081" s="T24">RUS:gram</ta>
            <ta e="T27" id="Seg_2082" s="T26">RUS:gram</ta>
            <ta e="T36" id="Seg_2083" s="T35">RUS:cult</ta>
            <ta e="T40" id="Seg_2084" s="T39">RUS:gram</ta>
            <ta e="T50" id="Seg_2085" s="T49">TURK:disc</ta>
            <ta e="T52" id="Seg_2086" s="T51">TURK:cult</ta>
            <ta e="T57" id="Seg_2087" s="T56">RUS:gram</ta>
            <ta e="T61" id="Seg_2088" s="T60">RUS:gram</ta>
            <ta e="T76" id="Seg_2089" s="T75">TAT:cult</ta>
            <ta e="T80" id="Seg_2090" s="T79">TAT:cult</ta>
            <ta e="T98" id="Seg_2091" s="T97">RUS:gram</ta>
            <ta e="T116" id="Seg_2092" s="T115">RUS:cult</ta>
            <ta e="T125" id="Seg_2093" s="T124">RUS:gram</ta>
            <ta e="T134" id="Seg_2094" s="T133">RUS:gram</ta>
            <ta e="T137" id="Seg_2095" s="T136">RUS:cult</ta>
            <ta e="T138" id="Seg_2096" s="T137">RUS:gram</ta>
            <ta e="T140" id="Seg_2097" s="T139">RUS:gram</ta>
            <ta e="T150" id="Seg_2098" s="T149">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T6" id="Seg_2099" s="T5">dir:infl</ta>
            <ta e="T20" id="Seg_2100" s="T19">dir:bare</ta>
            <ta e="T36" id="Seg_2101" s="T35">dir:bare</ta>
            <ta e="T52" id="Seg_2102" s="T51">dir:bare</ta>
            <ta e="T116" id="Seg_2103" s="T115">parad:infl</ta>
            <ta e="T150" id="Seg_2104" s="T149">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_2105" s="T0">Я была в Агинском.</ta>
            <ta e="T7" id="Seg_2106" s="T4">Там Богу молилась.</ta>
            <ta e="T11" id="Seg_2107" s="T7">Потом пошла домой.</ta>
            <ta e="T15" id="Seg_2108" s="T11">Шла, шла по дороге.</ta>
            <ta e="T20" id="Seg_2109" s="T15">Ни одна машина меня не догнала.</ta>
            <ta e="T26" id="Seg_2110" s="T20">Потом дождь начался и ветер.</ta>
            <ta e="T31" id="Seg_2111" s="T26">Испугаешься, [как бы тебя] не унесло ветром.</ta>
            <ta e="T36" id="Seg_2112" s="T31">Я шла, шла, стоит машина.</ta>
            <ta e="T44" id="Seg_2113" s="T36">Потом я сбилась с дороги и пошла (к местным?).</ta>
            <ta e="T48" id="Seg_2114" s="T44">Туда пришла, там переночевала.</ta>
            <ta e="T54" id="Seg_2115" s="T48">Там все поют, водку пьют, ругаются.</ta>
            <ta e="T62" id="Seg_2116" s="T54">Потом они заснули, а я утром встала и пошла.</ta>
            <ta e="T67" id="Seg_2117" s="T62">Я пришла, пришла, у меня был хлеб.</ta>
            <ta e="T70" id="Seg_2118" s="T67">На берегу реки села.</ta>
            <ta e="T73" id="Seg_2119" s="T70">Потом опять пошла.</ta>
            <ta e="T74" id="Seg_2120" s="T73">Иду.</ta>
            <ta e="T77" id="Seg_2121" s="T74">Вижу: дома стоят.</ta>
            <ta e="T81" id="Seg_2122" s="T77">Сейчас пойду туда, к домам.</ta>
            <ta e="T89" id="Seg_2123" s="T81">Потом иду, иду, я думала, дорога, она тут пройдёт.</ta>
            <ta e="T91" id="Seg_2124" s="T89">Потом вижу:</ta>
            <ta e="T95" id="Seg_2125" s="T91">Я к (ней?) не иду.</ta>
            <ta e="T97" id="Seg_2126" s="T95">Тогда я вернулась.</ta>
            <ta e="T101" id="Seg_2127" s="T97">И вышла на дорогу в Малиновское.</ta>
            <ta e="T103" id="Seg_2128" s="T101">Потом пришла.</ta>
            <ta e="T107" id="Seg_2129" s="T103">Там пшеницу сеяли.</ta>
            <ta e="T109" id="Seg_2130" s="T107">Я пошла.</ta>
            <ta e="T110" id="Seg_2131" s="T109">Пришла.</ta>
            <ta e="T113" id="Seg_2132" s="T110">Потом домой пришла.</ta>
            <ta e="T117" id="Seg_2133" s="T113">Было десять часов.</ta>
            <ta e="T121" id="Seg_2134" s="T117">Они там мыли. [?]</ta>
            <ta e="T127" id="Seg_2135" s="T121">Потом я пришла домой и съела хлеб.</ta>
            <ta e="T129" id="Seg_2136" s="T127">Поспала.</ta>
            <ta e="T133" id="Seg_2137" s="T129">Потом я пошла, (вскопала?) землю.</ta>
            <ta e="T137" id="Seg_2138" s="T133">И посадила капусту.</ta>
            <ta e="T142" id="Seg_2139" s="T137">И водой полила.</ta>
            <ta e="T147" id="Seg_2140" s="T142">Потом вечером спать легла.</ta>
            <ta e="T149" id="Seg_2141" s="T147">Назавтра встала.</ta>
            <ta e="T151" id="Seg_2142" s="T149">Посадила брюкву.</ta>
            <ta e="T152" id="Seg_2143" s="T151">Потом…</ta>
            <ta e="T153" id="Seg_2144" s="T152">Останови [плёнку].</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_2145" s="T0">I was in Aginskoye.</ta>
            <ta e="T7" id="Seg_2146" s="T4">I prayed to God there.</ta>
            <ta e="T11" id="Seg_2147" s="T7">Then I went home.</ta>
            <ta e="T15" id="Seg_2148" s="T11">I walked, I walked on the road.</ta>
            <ta e="T20" id="Seg_2149" s="T15">No car gave me a lift.</ta>
            <ta e="T26" id="Seg_2150" s="T20">Then it began to rain and the wind came.</ta>
            <ta e="T31" id="Seg_2151" s="T26">But you will be afraid [that] the wind will take you away.</ta>
            <ta e="T36" id="Seg_2152" s="T31">I walked, walked, a car is standing.</ta>
            <ta e="T44" id="Seg_2153" s="T36">Then I lost my way and I went off (to the locals?).</ta>
            <ta e="T48" id="Seg_2154" s="T44">I came there, I spent the night there.</ta>
            <ta e="T54" id="Seg_2155" s="T48">They are singing there, drinking vodka, swearing.</ta>
            <ta e="T62" id="Seg_2156" s="T54">Then they fell asleep, and I woke up in the morning and went.</ta>
            <ta e="T67" id="Seg_2157" s="T62">I come, I come, I had bread.</ta>
            <ta e="T70" id="Seg_2158" s="T67">I sat down on the shore of a river.</ta>
            <ta e="T73" id="Seg_2159" s="T70">Then I went again.</ta>
            <ta e="T74" id="Seg_2160" s="T73">I am walking.</ta>
            <ta e="T77" id="Seg_2161" s="T74">I see: houses are standing.</ta>
            <ta e="T81" id="Seg_2162" s="T77">Now I will go there to the houses.</ta>
            <ta e="T89" id="Seg_2163" s="T81">Then I walk, I walk, I thought the road, it will go there.</ta>
            <ta e="T91" id="Seg_2164" s="T89">Then I see.</ta>
            <ta e="T95" id="Seg_2165" s="T91">I am not going to it.</ta>
            <ta e="T97" id="Seg_2166" s="T95">Then I came back.</ta>
            <ta e="T101" id="Seg_2167" s="T97">And I came on the Malinovskoye road.</ta>
            <ta e="T103" id="Seg_2168" s="T101">Then I came.</ta>
            <ta e="T107" id="Seg_2169" s="T103">They sowed wheat there.</ta>
            <ta e="T109" id="Seg_2170" s="T107">I went.</ta>
            <ta e="T110" id="Seg_2171" s="T109">I came.</ta>
            <ta e="T113" id="Seg_2172" s="T110">Then I came home.</ta>
            <ta e="T117" id="Seg_2173" s="T113">It was ten o'clock.</ta>
            <ta e="T121" id="Seg_2174" s="T117">There they were washing. [?]</ta>
            <ta e="T127" id="Seg_2175" s="T121">Then I went home and ate bread.</ta>
            <ta e="T129" id="Seg_2176" s="T127">I slept.</ta>
            <ta e="T133" id="Seg_2177" s="T129">Then I went, (ploughed?) the ground.</ta>
            <ta e="T137" id="Seg_2178" s="T133">And I planted cabbage.</ta>
            <ta e="T142" id="Seg_2179" s="T137">And I watered [them].</ta>
            <ta e="T147" id="Seg_2180" s="T142">Then in the evening I lied down to sleep.</ta>
            <ta e="T149" id="Seg_2181" s="T147">The next day I got up.</ta>
            <ta e="T151" id="Seg_2182" s="T149">I planted turnip.</ta>
            <ta e="T152" id="Seg_2183" s="T151">Then…</ta>
            <ta e="T153" id="Seg_2184" s="T152">Stop [the tape].</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_2185" s="T0">Ich war in Aginskoye.</ta>
            <ta e="T7" id="Seg_2186" s="T4">Ich habe dort gebetet.</ta>
            <ta e="T11" id="Seg_2187" s="T7">Dann bin ich nach Hause gegangen.</ta>
            <ta e="T15" id="Seg_2188" s="T11">Ich lief, ich lief die Straße entlang.</ta>
            <ta e="T20" id="Seg_2189" s="T15">Kein Auto hat mich mitgenommen.</ta>
            <ta e="T26" id="Seg_2190" s="T20">Dann fing es an zu regnen und der Wind kam.</ta>
            <ta e="T31" id="Seg_2191" s="T26">Aber du wirst Angst haben, dass der Wind dich wegbläst.</ta>
            <ta e="T36" id="Seg_2192" s="T31">Ich lief, ich lief, ein Auto steht.</ta>
            <ta e="T44" id="Seg_2193" s="T36">Dann habe ich mich verlaufen und ich bin (zu den Einwohnern?) gegangen.</ta>
            <ta e="T48" id="Seg_2194" s="T44">Ich kam dorthin, ich verbrachte die Nacht dort.</ta>
            <ta e="T54" id="Seg_2195" s="T48">Sie singen dort, trinken Wodka, fluchen.</ta>
            <ta e="T62" id="Seg_2196" s="T54">Dann schliefen sie ein, und ich wachte am morgen auf und ging.</ta>
            <ta e="T67" id="Seg_2197" s="T62">Ich komme, ich komme, ich hatte ein Brot.</ta>
            <ta e="T70" id="Seg_2198" s="T67">Ich setzte mich am Flussufer hin.</ta>
            <ta e="T73" id="Seg_2199" s="T70">Dann ging ich weiter.</ta>
            <ta e="T74" id="Seg_2200" s="T73">Ich laufe.</ta>
            <ta e="T77" id="Seg_2201" s="T74">Ich sehe: Häuser stehen dort.</ta>
            <ta e="T81" id="Seg_2202" s="T77">Jetzt werde ich dorthin zu den Häusern gehen.</ta>
            <ta e="T89" id="Seg_2203" s="T81">Dann lief ich, ich lief, ich dachte die Straße, sie wird dorthin führen.</ta>
            <ta e="T91" id="Seg_2204" s="T89">Dann sehe ich.</ta>
            <ta e="T95" id="Seg_2205" s="T91">Ich gehe nicht dorthin.</ta>
            <ta e="T97" id="Seg_2206" s="T95">Dann kam ich zurück.</ta>
            <ta e="T101" id="Seg_2207" s="T97">Und dann kam ich zur Malinovskoye Straße.</ta>
            <ta e="T103" id="Seg_2208" s="T101">Dann kam ich. </ta>
            <ta e="T107" id="Seg_2209" s="T103">Sie säten dort Weizen.</ta>
            <ta e="T109" id="Seg_2210" s="T107">Ich ging.</ta>
            <ta e="T110" id="Seg_2211" s="T109">Ich kam.</ta>
            <ta e="T113" id="Seg_2212" s="T110">Dann kam ich nach Hause.</ta>
            <ta e="T117" id="Seg_2213" s="T113">Es war zehn Uhr.</ta>
            <ta e="T121" id="Seg_2214" s="T117">Dort wuschen sie. [?]</ta>
            <ta e="T127" id="Seg_2215" s="T121">Dann ging ich nach Hause und aß Brot.</ta>
            <ta e="T129" id="Seg_2216" s="T127">Ich schlief.</ta>
            <ta e="T133" id="Seg_2217" s="T129">Dann ging ich, (grub?) den Boden (um?).</ta>
            <ta e="T137" id="Seg_2218" s="T133">Und ich pflanzte Kohl,</ta>
            <ta e="T142" id="Seg_2219" s="T137">Und ich goß [ihn].</ta>
            <ta e="T147" id="Seg_2220" s="T142">Dann am Abend legte ich mich zum schlafen hin.</ta>
            <ta e="T149" id="Seg_2221" s="T147">Am nächsten Tag stand ich auf.</ta>
            <ta e="T151" id="Seg_2222" s="T149">Ich pflanzte Rüben.</ta>
            <ta e="T152" id="Seg_2223" s="T151">Dann…</ta>
            <ta e="T153" id="Seg_2224" s="T152">Halte [die Aufnahme] an.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_2225" s="T0">[AAV]: Tape SU0192</ta>
            <ta e="T26" id="Seg_2226" s="T20">Beržə - wind.</ta>
            <ta e="T44" id="Seg_2227" s="T36">[KlT:] Уроженец Ru. 'local person'. [GVY:] Cf. also uražə 'Menschen, die früher in Sibirien lebten' (Donner 81a).</ta>
            <ta e="T101" id="Seg_2228" s="T97">[GVY:] Here pronounced as šobiem.</ta>
            <ta e="T117" id="Seg_2229" s="T113">[WNB] časɨ is in plural. </ta>
            <ta e="T121" id="Seg_2230" s="T117">[KlG] cf. KW 58a səbərgö etc. ‘brush’</ta>
            <ta e="T133" id="Seg_2231" s="T129">[GVY:] d'ezerbiam(?) - unknown verb. Cf. d'azaŋ 'field' (Donner 14b)? [AAV] first vowel -a- [KlG] dʼezer- ‘repair’ pro tajər- ‘plough [dig over/umgraben]’</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T154" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
