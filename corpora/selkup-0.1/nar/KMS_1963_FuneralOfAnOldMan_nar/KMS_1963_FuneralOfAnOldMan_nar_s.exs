<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID6AC0129C-78FD-A0EE-BC41-DB031C5F8F63">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\KMS_1963_FuneralOfAnOldMan_nar\KMS_1963_FuneralOfAnOldMan_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">46</ud-information>
            <ud-information attribute-name="# HIAT:w">35</ud-information>
            <ud-information attribute-name="# e">35</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMS">
            <abbreviation>KMS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T35" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">qumbədi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">qum</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">ippa</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">säːɣɨn</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">kundar</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">ilɨŋ</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_23" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">me</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">taqnot</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">iram</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">kulʼdʼiŋ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">täp</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">tʼärɨs</ts>
                  <nts id="Seg_41" n="HIAT:ip">:</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">kanzamdə</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">päŋgu</ts>
                  <nts id="Seg_48" n="HIAT:ip">,</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">tappaɣə</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_54" n="HIAT:w" s="T15">kotčamdə</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_57" n="HIAT:w" s="T16">päŋgu</ts>
                  <nts id="Seg_58" n="HIAT:ip">,</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">paːmdə</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">pängu</ts>
                  <nts id="Seg_65" n="HIAT:ip">,</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">tissemdə</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">pängu</ts>
                  <nts id="Seg_72" n="HIAT:ip">,</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_75" n="HIAT:w" s="T21">säːrangam</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_78" n="HIAT:w" s="T22">i</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_81" n="HIAT:w" s="T23">sakkum</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_84" n="HIAT:w" s="T24">pangu</ts>
                  <nts id="Seg_85" n="HIAT:ip">,</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_88" n="HIAT:w" s="T25">nʼäim</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_91" n="HIAT:w" s="T26">pangu</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_94" n="HIAT:w" s="T27">i</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_97" n="HIAT:w" s="T28">qomdäm</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_101" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_103" n="HIAT:w" s="T29">aːw</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_106" n="HIAT:w" s="T30">swetqɨn</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_109" n="HIAT:w" s="T31">wes</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_112" n="HIAT:w" s="T32">täbɨnan</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_115" n="HIAT:w" s="T33">ei</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_117" n="HIAT:ip">(</nts>
                  <ts e="T35" id="Seg_119" n="HIAT:w" s="T34">eŋ</ts>
                  <nts id="Seg_120" n="HIAT:ip">)</nts>
                  <nts id="Seg_121" n="HIAT:ip">.</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T35" id="Seg_123" n="sc" s="T0">
               <ts e="T1" id="Seg_125" n="e" s="T0">qumbədi </ts>
               <ts e="T2" id="Seg_127" n="e" s="T1">qum </ts>
               <ts e="T3" id="Seg_129" n="e" s="T2">ippa </ts>
               <ts e="T4" id="Seg_131" n="e" s="T3">säːɣɨn </ts>
               <ts e="T5" id="Seg_133" n="e" s="T4">kundar </ts>
               <ts e="T6" id="Seg_135" n="e" s="T5">ilɨŋ. </ts>
               <ts e="T7" id="Seg_137" n="e" s="T6">me </ts>
               <ts e="T8" id="Seg_139" n="e" s="T7">taqnot </ts>
               <ts e="T9" id="Seg_141" n="e" s="T8">iram </ts>
               <ts e="T10" id="Seg_143" n="e" s="T9">kulʼdʼiŋ </ts>
               <ts e="T11" id="Seg_145" n="e" s="T10">täp </ts>
               <ts e="T12" id="Seg_147" n="e" s="T11">tʼärɨs: </ts>
               <ts e="T13" id="Seg_149" n="e" s="T12">kanzamdə </ts>
               <ts e="T14" id="Seg_151" n="e" s="T13">päŋgu, </ts>
               <ts e="T15" id="Seg_153" n="e" s="T14">tappaɣə </ts>
               <ts e="T16" id="Seg_155" n="e" s="T15">kotčamdə </ts>
               <ts e="T17" id="Seg_157" n="e" s="T16">päŋgu, </ts>
               <ts e="T18" id="Seg_159" n="e" s="T17">paːmdə </ts>
               <ts e="T19" id="Seg_161" n="e" s="T18">pängu, </ts>
               <ts e="T20" id="Seg_163" n="e" s="T19">tissemdə </ts>
               <ts e="T21" id="Seg_165" n="e" s="T20">pängu, </ts>
               <ts e="T22" id="Seg_167" n="e" s="T21">säːrangam </ts>
               <ts e="T23" id="Seg_169" n="e" s="T22">i </ts>
               <ts e="T24" id="Seg_171" n="e" s="T23">sakkum </ts>
               <ts e="T25" id="Seg_173" n="e" s="T24">pangu, </ts>
               <ts e="T26" id="Seg_175" n="e" s="T25">nʼäim </ts>
               <ts e="T27" id="Seg_177" n="e" s="T26">pangu </ts>
               <ts e="T28" id="Seg_179" n="e" s="T27">i </ts>
               <ts e="T29" id="Seg_181" n="e" s="T28">qomdäm. </ts>
               <ts e="T30" id="Seg_183" n="e" s="T29">aːw </ts>
               <ts e="T31" id="Seg_185" n="e" s="T30">swetqɨn </ts>
               <ts e="T32" id="Seg_187" n="e" s="T31">wes </ts>
               <ts e="T33" id="Seg_189" n="e" s="T32">täbɨnan </ts>
               <ts e="T34" id="Seg_191" n="e" s="T33">ei </ts>
               <ts e="T35" id="Seg_193" n="e" s="T34">(eŋ). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_194" s="T0">KMS_1963_FuneralOfAnOldMan_nar.001 (001.001)</ta>
            <ta e="T29" id="Seg_195" s="T6">KMS_1963_FuneralOfAnOldMan_nar.002 (001.002)</ta>
            <ta e="T35" id="Seg_196" s="T29">KMS_1963_FuneralOfAnOldMan_nar.003 (001.003)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_197" s="T0">′kумбъди kум и′ппа ′сӓ̄ɣын кун′дар ′илың.</ta>
            <ta e="T29" id="Seg_198" s="T6">ме таk′нот и′рам кулʼдʼиң тӓп ′тʼӓрыс: кан′замдъ пӓң(н)′гу, та′ппаɣъ кот′тшамдъ пӓң(н)гу, ′па̄мдъ пӓн′гу, ти′ссемдъ пӓн′гу, сӓ̄′рангам и сак′кум пан′гу, ′нʼӓим пан′гу и kом′дӓм.</ta>
            <ta e="T35" id="Seg_199" s="T29">а̄w светkын вес тӓбы′нан ′е̨и (ең).</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_200" s="T0">qumbədi qum ippa säːɣɨn kundar ilɨŋ.</ta>
            <ta e="T29" id="Seg_201" s="T6">me taqnot iram kulʼdʼiŋ täp tʼärɨs: kanzamdə päŋ(n)gu, tappaɣə kottšamdə päŋ(n)gu, paːmdə pängu, tissemdə pängu, säːrangam i sakkum pangu, nʼäim pangu i qomdäm.</ta>
            <ta e="T35" id="Seg_202" s="T29">aːw svetqɨn ves täbɨnan ei (eŋ).</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_203" s="T0">qumbədi qum ippa säːɣɨn kundar ilɨŋ. </ta>
            <ta e="T29" id="Seg_204" s="T6">me taqnot iram kulʼdʼiŋ täp tʼärɨs: kanzamdə päŋgu, tappaɣə kotčamdə päŋgu, paːmdə pängu, tissemdə pängu, säːrangam i sakkum pangu, nʼäim pangu i qomdäm. </ta>
            <ta e="T35" id="Seg_205" s="T29">aːw swetqɨn wes täbɨnan ei (eŋ). </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_206" s="T0">qu-mbədi</ta>
            <ta e="T2" id="Seg_207" s="T1">qum</ta>
            <ta e="T3" id="Seg_208" s="T2">ippa</ta>
            <ta e="T4" id="Seg_209" s="T3">säː-ɣɨn</ta>
            <ta e="T5" id="Seg_210" s="T4">kundar</ta>
            <ta e="T6" id="Seg_211" s="T5">ilɨŋ</ta>
            <ta e="T7" id="Seg_212" s="T6">me</ta>
            <ta e="T8" id="Seg_213" s="T7">taq-n-ot</ta>
            <ta e="T9" id="Seg_214" s="T8">ira-m</ta>
            <ta e="T10" id="Seg_215" s="T9">kulʼdʼi-ŋ</ta>
            <ta e="T11" id="Seg_216" s="T10">täp</ta>
            <ta e="T12" id="Seg_217" s="T11">tʼärɨ-s</ta>
            <ta e="T13" id="Seg_218" s="T12">kanza-m-də</ta>
            <ta e="T14" id="Seg_219" s="T13">päŋ-gu</ta>
            <ta e="T15" id="Seg_220" s="T14">tappaɣə</ta>
            <ta e="T16" id="Seg_221" s="T15">kotča-m-də</ta>
            <ta e="T17" id="Seg_222" s="T16">päŋ-gu</ta>
            <ta e="T18" id="Seg_223" s="T17">paː-m-də</ta>
            <ta e="T19" id="Seg_224" s="T18">pän-gu</ta>
            <ta e="T20" id="Seg_225" s="T19">tisse-m-də</ta>
            <ta e="T21" id="Seg_226" s="T20">pän-gu</ta>
            <ta e="T22" id="Seg_227" s="T21">säːranga-m</ta>
            <ta e="T23" id="Seg_228" s="T22">i</ta>
            <ta e="T24" id="Seg_229" s="T23">sakku-m</ta>
            <ta e="T25" id="Seg_230" s="T24">pan-gu</ta>
            <ta e="T26" id="Seg_231" s="T25">nʼäi-m</ta>
            <ta e="T27" id="Seg_232" s="T26">pan-gu</ta>
            <ta e="T28" id="Seg_233" s="T27">i</ta>
            <ta e="T29" id="Seg_234" s="T28">qomdä-m</ta>
            <ta e="T30" id="Seg_235" s="T29">aːw</ta>
            <ta e="T31" id="Seg_236" s="T30">swet-qɨn</ta>
            <ta e="T32" id="Seg_237" s="T31">wes</ta>
            <ta e="T33" id="Seg_238" s="T32">täb-ɨ-nan</ta>
            <ta e="T34" id="Seg_239" s="T33">e-i</ta>
            <ta e="T35" id="Seg_240" s="T34">e-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_241" s="T0">quː-mbɨdi</ta>
            <ta e="T2" id="Seg_242" s="T1">qum</ta>
            <ta e="T3" id="Seg_243" s="T2">ippi</ta>
            <ta e="T4" id="Seg_244" s="T3">säː-qən</ta>
            <ta e="T5" id="Seg_245" s="T4">kuttar</ta>
            <ta e="T6" id="Seg_246" s="T5">ilɨŋ</ta>
            <ta e="T7" id="Seg_247" s="T6">meː</ta>
            <ta e="T8" id="Seg_248" s="T7">taqqu-ŋɨ-ut</ta>
            <ta e="T9" id="Seg_249" s="T8">ira-m</ta>
            <ta e="T10" id="Seg_250" s="T9">kulʼdi-k</ta>
            <ta e="T11" id="Seg_251" s="T10">tap</ta>
            <ta e="T12" id="Seg_252" s="T11">tʼarɨ-sɨ</ta>
            <ta e="T13" id="Seg_253" s="T12">kanza-m-tɨ</ta>
            <ta e="T14" id="Seg_254" s="T13">pan-gu</ta>
            <ta e="T15" id="Seg_255" s="T14">tapaq</ta>
            <ta e="T16" id="Seg_256" s="T15">kotča-m-tɨ</ta>
            <ta e="T17" id="Seg_257" s="T16">pan-gu</ta>
            <ta e="T18" id="Seg_258" s="T17">pagɨ-m-tɨ</ta>
            <ta e="T19" id="Seg_259" s="T18">pan-gu</ta>
            <ta e="T20" id="Seg_260" s="T19">tissi-m-tɨ</ta>
            <ta e="T21" id="Seg_261" s="T20">pan-gu</ta>
            <ta e="T22" id="Seg_262" s="T21">saraŋə-m</ta>
            <ta e="T23" id="Seg_263" s="T22">i</ta>
            <ta e="T24" id="Seg_264" s="T23">šaqqu-m</ta>
            <ta e="T25" id="Seg_265" s="T24">pan-gu</ta>
            <ta e="T26" id="Seg_266" s="T25">nʼaj-m</ta>
            <ta e="T27" id="Seg_267" s="T26">pan-gu</ta>
            <ta e="T28" id="Seg_268" s="T27">i</ta>
            <ta e="T29" id="Seg_269" s="T28">qomdɛ-m</ta>
            <ta e="T30" id="Seg_270" s="T29">aːwi</ta>
            <ta e="T31" id="Seg_271" s="T30">swet-qɨnnɨ</ta>
            <ta e="T32" id="Seg_272" s="T31">wesʼ</ta>
            <ta e="T33" id="Seg_273" s="T32">tap-ɨ-nan</ta>
            <ta e="T34" id="Seg_274" s="T33">eː-ŋɨ</ta>
            <ta e="T35" id="Seg_275" s="T34">eː-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_276" s="T0">die-PTCP.PST</ta>
            <ta e="T2" id="Seg_277" s="T1">human.being.[NOM]</ta>
            <ta e="T3" id="Seg_278" s="T2">lie.[3SG.S]</ta>
            <ta e="T4" id="Seg_279" s="T3">coffin-LOC</ta>
            <ta e="T5" id="Seg_280" s="T4">how</ta>
            <ta e="T6" id="Seg_281" s="T5">living</ta>
            <ta e="T7" id="Seg_282" s="T6">we.NOM</ta>
            <ta e="T8" id="Seg_283" s="T7">bury-CO-1PL</ta>
            <ta e="T9" id="Seg_284" s="T8">old.man-ACC</ta>
            <ta e="T10" id="Seg_285" s="T9">which-ADVZ</ta>
            <ta e="T11" id="Seg_286" s="T10">(s)he</ta>
            <ta e="T12" id="Seg_287" s="T11">say-PST.[3SG.S]</ta>
            <ta e="T13" id="Seg_288" s="T12">tube-ACC-3SG</ta>
            <ta e="T14" id="Seg_289" s="T13">put-INF</ta>
            <ta e="T15" id="Seg_290" s="T14">tobacco.[NOM]</ta>
            <ta e="T16" id="Seg_291" s="T15">pouch-ACC-3SG</ta>
            <ta e="T17" id="Seg_292" s="T16">put-INF</ta>
            <ta e="T18" id="Seg_293" s="T17">knife-ACC-3SG</ta>
            <ta e="T19" id="Seg_294" s="T18">put-INF</ta>
            <ta e="T20" id="Seg_295" s="T19">dishes-ACC-3SG</ta>
            <ta e="T21" id="Seg_296" s="T20">put-INF</ta>
            <ta e="T22" id="Seg_297" s="T21">match-ACC</ta>
            <ta e="T23" id="Seg_298" s="T22">and</ta>
            <ta e="T24" id="Seg_299" s="T23">fire.iron-ACC</ta>
            <ta e="T25" id="Seg_300" s="T24">put-INF</ta>
            <ta e="T26" id="Seg_301" s="T25">bread-ACC</ta>
            <ta e="T27" id="Seg_302" s="T26">put-INF</ta>
            <ta e="T28" id="Seg_303" s="T27">and</ta>
            <ta e="T29" id="Seg_304" s="T28">money-ACC</ta>
            <ta e="T30" id="Seg_305" s="T29">other</ta>
            <ta e="T31" id="Seg_306" s="T30">light-EL</ta>
            <ta e="T32" id="Seg_307" s="T31">all</ta>
            <ta e="T33" id="Seg_308" s="T32">(s)he-EP-ADESS</ta>
            <ta e="T34" id="Seg_309" s="T33">be-CO.[3SG.S]</ta>
            <ta e="T35" id="Seg_310" s="T34">be-3SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_311" s="T0">умереть-PTCP.PST</ta>
            <ta e="T2" id="Seg_312" s="T1">человек.[NOM]</ta>
            <ta e="T3" id="Seg_313" s="T2">лежать.[3SG.S]</ta>
            <ta e="T4" id="Seg_314" s="T3">гроб-LOC</ta>
            <ta e="T5" id="Seg_315" s="T4">как</ta>
            <ta e="T6" id="Seg_316" s="T5">живой</ta>
            <ta e="T7" id="Seg_317" s="T6">мы.NOM</ta>
            <ta e="T8" id="Seg_318" s="T7">похоронить-CO-1PL</ta>
            <ta e="T9" id="Seg_319" s="T8">старик-ACC</ta>
            <ta e="T10" id="Seg_320" s="T9">какой-ADVZ</ta>
            <ta e="T11" id="Seg_321" s="T10">он(а)</ta>
            <ta e="T12" id="Seg_322" s="T11">сказать-PST.[3SG.S]</ta>
            <ta e="T13" id="Seg_323" s="T12">трубка-ACC-3SG</ta>
            <ta e="T14" id="Seg_324" s="T13">положить-INF</ta>
            <ta e="T15" id="Seg_325" s="T14">табак.[NOM]</ta>
            <ta e="T16" id="Seg_326" s="T15">кисет-ACC-3SG</ta>
            <ta e="T17" id="Seg_327" s="T16">положить-INF</ta>
            <ta e="T18" id="Seg_328" s="T17">нож-ACC-3SG</ta>
            <ta e="T19" id="Seg_329" s="T18">положить-INF</ta>
            <ta e="T20" id="Seg_330" s="T19">посуда-ACC-3SG</ta>
            <ta e="T21" id="Seg_331" s="T20">положить-INF</ta>
            <ta e="T22" id="Seg_332" s="T21">спичка-ACC</ta>
            <ta e="T23" id="Seg_333" s="T22">и</ta>
            <ta e="T24" id="Seg_334" s="T23">огниво-ACC</ta>
            <ta e="T25" id="Seg_335" s="T24">положить-INF</ta>
            <ta e="T26" id="Seg_336" s="T25">хлеб-ACC</ta>
            <ta e="T27" id="Seg_337" s="T26">положить-INF</ta>
            <ta e="T28" id="Seg_338" s="T27">и</ta>
            <ta e="T29" id="Seg_339" s="T28">деньги-ACC</ta>
            <ta e="T30" id="Seg_340" s="T29">другой</ta>
            <ta e="T31" id="Seg_341" s="T30">свет-ЕL</ta>
            <ta e="T32" id="Seg_342" s="T31">весь</ta>
            <ta e="T33" id="Seg_343" s="T32">он(а)-EP-ADESS</ta>
            <ta e="T34" id="Seg_344" s="T33">быть-CO.[3SG.S]</ta>
            <ta e="T35" id="Seg_345" s="T34">быть-3SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_346" s="T0">v-v&gt;ptcp</ta>
            <ta e="T2" id="Seg_347" s="T1">n-n:case1</ta>
            <ta e="T3" id="Seg_348" s="T2">v-v:pn</ta>
            <ta e="T4" id="Seg_349" s="T3">n-n:case2</ta>
            <ta e="T5" id="Seg_350" s="T4">interrog</ta>
            <ta e="T6" id="Seg_351" s="T5">adj</ta>
            <ta e="T7" id="Seg_352" s="T6">pers</ta>
            <ta e="T8" id="Seg_353" s="T7">v-v:ins-v:pn</ta>
            <ta e="T9" id="Seg_354" s="T8">n-n:case1</ta>
            <ta e="T10" id="Seg_355" s="T9">interrog-adj&gt;adv</ta>
            <ta e="T11" id="Seg_356" s="T10">pers</ta>
            <ta e="T12" id="Seg_357" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_358" s="T12">n-n:case1-n:poss</ta>
            <ta e="T14" id="Seg_359" s="T13">v-v:inf</ta>
            <ta e="T15" id="Seg_360" s="T14">n-n:case1</ta>
            <ta e="T16" id="Seg_361" s="T15">n-n:case1-n:poss</ta>
            <ta e="T17" id="Seg_362" s="T16">v-v:inf</ta>
            <ta e="T18" id="Seg_363" s="T17">n-n:case1-n:poss</ta>
            <ta e="T19" id="Seg_364" s="T18">v-v:inf</ta>
            <ta e="T20" id="Seg_365" s="T19">n-n:case1-n:poss</ta>
            <ta e="T21" id="Seg_366" s="T20">v-v:inf</ta>
            <ta e="T22" id="Seg_367" s="T21">n-n:case1</ta>
            <ta e="T23" id="Seg_368" s="T22">conj</ta>
            <ta e="T24" id="Seg_369" s="T23">n-n:case1</ta>
            <ta e="T25" id="Seg_370" s="T24">v-v:inf</ta>
            <ta e="T26" id="Seg_371" s="T25">n-n:case1</ta>
            <ta e="T27" id="Seg_372" s="T26">v-v:inf</ta>
            <ta e="T28" id="Seg_373" s="T27">conj</ta>
            <ta e="T29" id="Seg_374" s="T28">n-n:case1</ta>
            <ta e="T30" id="Seg_375" s="T29">adj</ta>
            <ta e="T31" id="Seg_376" s="T30">n-n:case2</ta>
            <ta e="T32" id="Seg_377" s="T31">quant</ta>
            <ta e="T33" id="Seg_378" s="T32">pers-n:ins-n:case2</ta>
            <ta e="T34" id="Seg_379" s="T33">v-v:ins-v:pn</ta>
            <ta e="T35" id="Seg_380" s="T34">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_381" s="T0">ptcp</ta>
            <ta e="T2" id="Seg_382" s="T1">n</ta>
            <ta e="T3" id="Seg_383" s="T2">v</ta>
            <ta e="T4" id="Seg_384" s="T3">n</ta>
            <ta e="T5" id="Seg_385" s="T4">interrog</ta>
            <ta e="T6" id="Seg_386" s="T5">adj</ta>
            <ta e="T7" id="Seg_387" s="T6">pers</ta>
            <ta e="T8" id="Seg_388" s="T7">v</ta>
            <ta e="T9" id="Seg_389" s="T8">n</ta>
            <ta e="T10" id="Seg_390" s="T9">adv</ta>
            <ta e="T11" id="Seg_391" s="T10">pers</ta>
            <ta e="T12" id="Seg_392" s="T11">v</ta>
            <ta e="T13" id="Seg_393" s="T12">n</ta>
            <ta e="T14" id="Seg_394" s="T13">v</ta>
            <ta e="T15" id="Seg_395" s="T14">n</ta>
            <ta e="T16" id="Seg_396" s="T15">n</ta>
            <ta e="T17" id="Seg_397" s="T16">v</ta>
            <ta e="T18" id="Seg_398" s="T17">n</ta>
            <ta e="T19" id="Seg_399" s="T18">v</ta>
            <ta e="T20" id="Seg_400" s="T19">n</ta>
            <ta e="T21" id="Seg_401" s="T20">v</ta>
            <ta e="T22" id="Seg_402" s="T21">n</ta>
            <ta e="T23" id="Seg_403" s="T22">conj</ta>
            <ta e="T24" id="Seg_404" s="T23">n</ta>
            <ta e="T25" id="Seg_405" s="T24">v</ta>
            <ta e="T26" id="Seg_406" s="T25">n</ta>
            <ta e="T27" id="Seg_407" s="T26">v</ta>
            <ta e="T28" id="Seg_408" s="T27">conj</ta>
            <ta e="T29" id="Seg_409" s="T28">n</ta>
            <ta e="T30" id="Seg_410" s="T29">adj</ta>
            <ta e="T31" id="Seg_411" s="T30">n</ta>
            <ta e="T32" id="Seg_412" s="T31">quant</ta>
            <ta e="T33" id="Seg_413" s="T32">pers</ta>
            <ta e="T34" id="Seg_414" s="T33">v</ta>
            <ta e="T35" id="Seg_415" s="T34">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_416" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_417" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_418" s="T5">0.3.h:S v:pred</ta>
            <ta e="T7" id="Seg_419" s="T6">pro.h:S</ta>
            <ta e="T8" id="Seg_420" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_421" s="T8">np.h:O</ta>
            <ta e="T11" id="Seg_422" s="T10">pro.h:S</ta>
            <ta e="T12" id="Seg_423" s="T11">v:pred</ta>
            <ta e="T13" id="Seg_424" s="T12">np:O</ta>
            <ta e="T16" id="Seg_425" s="T15">np:O</ta>
            <ta e="T18" id="Seg_426" s="T17">np:O</ta>
            <ta e="T20" id="Seg_427" s="T19">np:O</ta>
            <ta e="T22" id="Seg_428" s="T21">np:O</ta>
            <ta e="T24" id="Seg_429" s="T23">np:O</ta>
            <ta e="T26" id="Seg_430" s="T25">np:O</ta>
            <ta e="T29" id="Seg_431" s="T28">np:O</ta>
            <ta e="T32" id="Seg_432" s="T31">pro:S</ta>
            <ta e="T34" id="Seg_433" s="T33">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_434" s="T1">np.h:Th</ta>
            <ta e="T4" id="Seg_435" s="T3">np:L</ta>
            <ta e="T7" id="Seg_436" s="T6">pro.h:A</ta>
            <ta e="T9" id="Seg_437" s="T8">np.h:Th</ta>
            <ta e="T11" id="Seg_438" s="T10">pro.h:A</ta>
            <ta e="T31" id="Seg_439" s="T30">np:L</ta>
            <ta e="T32" id="Seg_440" s="T31">pro:Th</ta>
            <ta e="T33" id="Seg_441" s="T32">pro.h:Poss</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T23" id="Seg_442" s="T22">RUS:gram</ta>
            <ta e="T24" id="Seg_443" s="T23">TURK:core</ta>
            <ta e="T28" id="Seg_444" s="T27">RUS:gram</ta>
            <ta e="T32" id="Seg_445" s="T31">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_446" s="T0">Покойник лежит в гробу как живой.</ta>
            <ta e="T29" id="Seg_447" s="T6">Мы похоронили старика, как он сказал: положить трубку, кисет с табаком, нож, посуду, спички, огниво, хлеб и деньги.</ta>
            <ta e="T35" id="Seg_448" s="T29">[Чтобы] на том свете всё у него было.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_449" s="T0">The dead man is lying in his coffin like a living one.</ta>
            <ta e="T29" id="Seg_450" s="T6">We buried the old man as he told us [to do]: put [there] his pipe, his tobacco pouch, his knife, his dishes, matches and a tinderbox, bread and money.</ta>
            <ta e="T35" id="Seg_451" s="T29">[For] him to have everything in the other world.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_452" s="T0">Der Verstorbene liegt im Sarg wie ein Lebendiger.</ta>
            <ta e="T29" id="Seg_453" s="T6">Wir haben den alten Mann so begraben, wie er es sagte: eine Pfeife, einen Beutel mit Tabak, ein Messer, Geschirr, Streichhölzer, eine Zunderbüchse, Brot und Geld [legten wir hin].</ta>
            <ta e="T35" id="Seg_454" s="T29">[Damit] er in jener Welt alles hat.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_455" s="T0">покойник лежит в гробу как живой</ta>
            <ta e="T29" id="Seg_456" s="T6">мы похоронили старика как он сказал трубку положить кошелек (кисет) с табаком нож стрелу спички сикару хлеб деньги сигару?</ta>
            <ta e="T35" id="Seg_457" s="T29">(чтобы) на том свете все у него было (есть)</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
