<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID5FC381A5-3854-1CF8-4D18-A61A85E2F6BB">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="SMV_196X_Lifestory_nar.wav" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\SMV_196X_Lifestory_nar\SMV_196X_Lifestory_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">121</ud-information>
            <ud-information attribute-name="# HIAT:w">94</ud-information>
            <ud-information attribute-name="# e">95</ud-information>
            <ud-information attribute-name="# HIAT:u">25</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SMV">
            <abbreviation>SMV</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="1.333" type="appl" />
         <tli id="T1" time="2.811" type="appl" />
         <tli id="T2" time="4.288" type="appl" />
         <tli id="T3" time="5.766" type="appl" />
         <tli id="T4" time="7.586" type="appl" />
         <tli id="T5" time="9.722" type="appl" />
         <tli id="T6" time="11.357" type="appl" />
         <tli id="T7" time="12.992" type="appl" />
         <tli id="T8" time="14.628" type="appl" />
         <tli id="T9" time="20.073" type="appl" />
         <tli id="T10" time="21.593" type="appl" />
         <tli id="T11" time="23.113" type="appl" />
         <tli id="T12" time="24.38" type="appl" />
         <tli id="T13" time="25.206" type="appl" />
         <tli id="T14" time="26.033" type="appl" />
         <tli id="T15" time="26.859" type="appl" />
         <tli id="T16" time="29.325" type="appl" />
         <tli id="T17" time="30.279" type="appl" />
         <tli id="T18" time="31.232" type="appl" />
         <tli id="T19" time="32.186" type="appl" />
         <tli id="T20" time="33.139" type="appl" />
         <tli id="T21" time="34.093" type="appl" />
         <tli id="T22" time="35.046" type="appl" />
         <tli id="T23" time="36.0" type="appl" />
         <tli id="T24" time="36.953" type="appl" />
         <tli id="T25" time="37.907" type="appl" />
         <tli id="T26" time="38.86" type="appl" />
         <tli id="T27" time="40.364" type="appl" />
         <tli id="T28" time="41.288" type="appl" />
         <tli id="T29" time="42.212" type="appl" />
         <tli id="T30" time="43.136" type="appl" />
         <tli id="T31" time="44.06" type="appl" />
         <tli id="T32" time="46.597" type="appl" />
         <tli id="T33" time="47.769" type="appl" />
         <tli id="T34" time="48.94" type="appl" />
         <tli id="T35" time="52.874" type="appl" />
         <tli id="T36" time="53.543" type="appl" />
         <tli id="T37" time="54.211" type="appl" />
         <tli id="T38" time="54.879" type="appl" />
         <tli id="T39" time="56.494" type="appl" />
         <tli id="T40" time="57.782" type="appl" />
         <tli id="T41" time="59.071" type="appl" />
         <tli id="T42" time="60.359" type="appl" />
         <tli id="T43" time="61.356" type="appl" />
         <tli id="T44" time="62.172" type="appl" />
         <tli id="T45" time="63.576" type="appl" />
         <tli id="T46" time="64.493" type="appl" />
         <tli id="T97" time="64.7795" type="intp" />
         <tli id="T47" time="65.639" type="appl" />
         <tli id="T48" time="66.159" type="appl" />
         <tli id="T50" time="67.199" type="appl" />
         <tli id="T51" time="69.187" type="appl" />
         <tli id="T52" time="70.073" type="appl" />
         <tli id="T53" time="70.96" type="appl" />
         <tli id="T54" time="72.51" type="appl" />
         <tli id="T55" time="73.442" type="appl" />
         <tli id="T56" time="74.373" type="appl" />
         <tli id="T57" time="75.424" type="appl" />
         <tli id="T58" time="76.123" type="appl" />
         <tli id="T59" time="76.822" type="appl" />
         <tli id="T60" time="77.52" type="appl" />
         <tli id="T61" time="78.671" type="appl" />
         <tli id="T62" time="79.468" type="appl" />
         <tli id="T63" time="80.266" type="appl" />
         <tli id="T64" time="82.069" type="appl" />
         <tli id="T65" time="82.997" type="appl" />
         <tli id="T66" time="83.926" type="appl" />
         <tli id="T67" time="86.3" type="appl" />
         <tli id="T68" time="87.18" type="appl" />
         <tli id="T69" time="88.06" type="appl" />
         <tli id="T70" time="89.196" type="appl" />
         <tli id="T71" time="89.96" type="appl" />
         <tli id="T72" time="90.723" type="appl" />
         <tli id="T73" time="91.486" type="appl" />
         <tli id="T74" time="94.473" type="appl" />
         <tli id="T75" time="95.726" type="appl" />
         <tli id="T76" time="97.844" type="appl" />
         <tli id="T77" time="98.555" type="appl" />
         <tli id="T78" time="99.266" type="appl" />
         <tli id="T79" time="102.481" type="appl" />
         <tli id="T80" time="103.223" type="appl" />
         <tli id="T81" time="103.965" type="appl" />
         <tli id="T82" time="104.708" type="appl" />
         <tli id="T83" time="105.45" type="appl" />
         <tli id="T84" time="106.192" type="appl" />
         <tli id="T85" time="108.247" type="appl" />
         <tli id="T86" time="109.162" type="appl" />
         <tli id="T87" time="110.077" type="appl" />
         <tli id="T88" time="110.992" type="appl" />
         <tli id="T89" time="113.215" type="appl" />
         <tli id="T90" time="113.683" type="appl" />
         <tli id="T91" time="114.152" type="appl" />
         <tli id="T92" time="114.62" type="appl" />
         <tli id="T93" time="115.089" type="appl" />
         <tli id="T94" time="115.557" type="appl" />
         <tli id="T95" time="116.026" type="appl" />
         <tli id="T96" time="117.865" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SMV"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T95" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Саргаева</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">Марфа</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">Васильевна</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T4" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Говорите</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_20" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_22" n="HIAT:w" s="T4">Mat</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">čʼeːlʼiksanak</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">1924</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">poːqɨntɨ</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_35" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_37" n="HIAT:w" s="T8">Čʼeːlɨmpɨtɨlʼ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">təttap</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">Turuhanska</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_47" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_49" n="HIAT:w" s="T11">Apamɨn</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">nɨmtɨ</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">nʼuːtɨlʼ</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">pačʼčʼɨtɨsa</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_62" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_64" n="HIAT:w" s="T15">Nɨnɨ</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_67" n="HIAT:w" s="T16">mat</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">selʼčʼi</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_73" n="HIAT:w" s="T18">poːtɨmɨ</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">tuːrɨsɨtɨ</ts>
                  <nts id="Seg_77" n="HIAT:ip">,</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">školantɨ</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">šıp</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">qəntɨsɔːtɨt</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">Turuhanskij</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">rajon</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">Farkovontɨ</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_99" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">Nɨmtɨ</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_104" n="HIAT:w" s="T27">učʼittɨsak</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">sompola</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">poːt</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">kuntɨ</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_117" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_119" n="HIAT:w" s="T31">Tɨmnʼamɨ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_122" n="HIAT:w" s="T32">Igarkaqɨn</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_125" n="HIAT:w" s="T33">učʼittɨsa</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_129" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_131" n="HIAT:w" s="T34">Nɨnɨ</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_134" n="HIAT:w" s="T35">man</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_137" n="HIAT:w" s="T36">Igarkantɨ</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_140" n="HIAT:w" s="T37">qəssɨnak</ts>
                  <nts id="Seg_141" n="HIAT:ip">.</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_144" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_146" n="HIAT:w" s="T38">Igarkaqɨt</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_149" n="HIAT:w" s="T39">mašım</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">iːsɔːtɨt</ts>
                  <nts id="Seg_153" n="HIAT:ip">,</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_156" n="HIAT:w" s="T41">pedučʼilʼšʼantɨ</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_160" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_162" n="HIAT:w" s="T42">Nɨmtɨ</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_165" n="HIAT:w" s="T43">učʼittɨsak</ts>
                  <nts id="Seg_166" n="HIAT:ip">.</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_169" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_171" n="HIAT:w" s="T44">Pervɨj</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_174" n="HIAT:w" s="T45">kursaqɨt</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_178" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_180" n="HIAT:w" s="T46">Ina</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_183" n="HIAT:w" s="T47">vtoroj</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_186" n="HIAT:w" s="T48">kursaqɨt</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_190" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_192" n="HIAT:w" s="T50">Nɨnɨ</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_195" n="HIAT:w" s="T51">müːtɨ</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_198" n="HIAT:w" s="T52">ɛsɨsɨna</ts>
                  <nts id="Seg_199" n="HIAT:ip">.</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_202" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_204" n="HIAT:w" s="T53">Müːtontɨ</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_207" n="HIAT:w" s="T54">tɨmnʼamɨ</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_210" n="HIAT:w" s="T55">qəntɨsɔːtɨt</ts>
                  <nts id="Seg_211" n="HIAT:ip">.</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_214" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_216" n="HIAT:w" s="T56">Man</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_219" n="HIAT:w" s="T57">učʼitɨqo</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_222" n="HIAT:w" s="T58">toː</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_225" n="HIAT:w" s="T59">čʼattɨsɨŋak</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_229" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_231" n="HIAT:w" s="T60">Amamɨ</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_234" n="HIAT:w" s="T61">ontɨqɨt</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_237" n="HIAT:w" s="T62">qalɨmpa</ts>
                  <nts id="Seg_238" n="HIAT:ip">.</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_241" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_243" n="HIAT:w" s="T63">Apamɨ</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_246" n="HIAT:w" s="T64">okot</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_249" n="HIAT:w" s="T65">qusana</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_253" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_255" n="HIAT:w" s="T66">Nɨnɨ</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_258" n="HIAT:w" s="T67">moqɨnä</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_261" n="HIAT:w" s="T68">tüːsɨŋak</ts>
                  <nts id="Seg_262" n="HIAT:ip">.</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_265" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_267" n="HIAT:w" s="T69">Moqɨna</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_270" n="HIAT:w" s="T70">tüla</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_273" n="HIAT:w" s="T71">qumɨtɨp</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_276" n="HIAT:w" s="T72">učʼimpɨsak</ts>
                  <nts id="Seg_277" n="HIAT:ip">.</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_280" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_282" n="HIAT:w" s="T73">Malogramotnɨj</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_285" n="HIAT:w" s="T74">qumɨtɨp</ts>
                  <nts id="Seg_286" n="HIAT:ip">.</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_289" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_291" n="HIAT:w" s="T75">Nɨnɨ</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_294" n="HIAT:w" s="T76">qumɨtɨp</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_297" n="HIAT:w" s="T77">učʼimpɨsak</ts>
                  <nts id="Seg_298" n="HIAT:ip">.</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_301" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_303" n="HIAT:w" s="T78">Nɨnɨ</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_306" n="HIAT:w" s="T79">kolhoztɨ</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_309" n="HIAT:w" s="T80">qəssaŋɨk</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_312" n="HIAT:w" s="T81">uːčʼiqɨnoːqo</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_315" n="HIAT:w" s="T82">nʼuːtɨlʼ</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_318" n="HIAT:w" s="T83">pačʼčʼɨtɨsak</ts>
                  <nts id="Seg_319" n="HIAT:ip">.</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_322" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_324" n="HIAT:w" s="T84">Nɨmtɨ</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_327" n="HIAT:w" s="T85">tɨmnʼamɨ</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_330" n="HIAT:w" s="T86">armijaqɨt</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_333" n="HIAT:w" s="T87">qətpɔːtɨt</ts>
                  <nts id="Seg_334" n="HIAT:ip">.</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_337" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_339" n="HIAT:w" s="T88">Nɨn</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_342" n="HIAT:w" s="T89">me</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_345" n="HIAT:w" s="T90">amalʼmɨt</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_348" n="HIAT:w" s="T91">onıː</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_351" n="HIAT:w" s="T92">šittälʼ</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_354" n="HIAT:w" s="T93">me</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_357" n="HIAT:w" s="T94">ilisɔːmɨt</ts>
                  <nts id="Seg_358" n="HIAT:ip">.</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T95" id="Seg_360" n="sc" s="T0">
               <ts e="T1" id="Seg_362" n="e" s="T0">Саргаева </ts>
               <ts e="T2" id="Seg_364" n="e" s="T1">Марфа </ts>
               <ts e="T3" id="Seg_366" n="e" s="T2">Васильевна. </ts>
               <ts e="T4" id="Seg_368" n="e" s="T3">Говорите. </ts>
               <ts e="T5" id="Seg_370" n="e" s="T4">Mat </ts>
               <ts e="T6" id="Seg_372" n="e" s="T5">čʼeːlʼiksanak </ts>
               <ts e="T7" id="Seg_374" n="e" s="T6">1924 </ts>
               <ts e="T8" id="Seg_376" n="e" s="T7">poːqɨntɨ. </ts>
               <ts e="T9" id="Seg_378" n="e" s="T8">Čʼeːlɨmpɨtɨlʼ </ts>
               <ts e="T10" id="Seg_380" n="e" s="T9">təttap </ts>
               <ts e="T11" id="Seg_382" n="e" s="T10">Turuhanska. </ts>
               <ts e="T12" id="Seg_384" n="e" s="T11">Apamɨn </ts>
               <ts e="T13" id="Seg_386" n="e" s="T12">nɨmtɨ </ts>
               <ts e="T14" id="Seg_388" n="e" s="T13">nʼuːtɨlʼ </ts>
               <ts e="T15" id="Seg_390" n="e" s="T14">pačʼčʼɨtɨsa. </ts>
               <ts e="T16" id="Seg_392" n="e" s="T15">Nɨnɨ </ts>
               <ts e="T17" id="Seg_394" n="e" s="T16">mat </ts>
               <ts e="T18" id="Seg_396" n="e" s="T17">selʼčʼi </ts>
               <ts e="T19" id="Seg_398" n="e" s="T18">poːtɨmɨ </ts>
               <ts e="T20" id="Seg_400" n="e" s="T19">tuːrɨsɨtɨ, </ts>
               <ts e="T21" id="Seg_402" n="e" s="T20">školantɨ </ts>
               <ts e="T22" id="Seg_404" n="e" s="T21">šıp </ts>
               <ts e="T23" id="Seg_406" n="e" s="T22">qəntɨsɔːtɨt </ts>
               <ts e="T24" id="Seg_408" n="e" s="T23">Turuhanskij </ts>
               <ts e="T25" id="Seg_410" n="e" s="T24">rajon </ts>
               <ts e="T26" id="Seg_412" n="e" s="T25">Farkovontɨ. </ts>
               <ts e="T27" id="Seg_414" n="e" s="T26">Nɨmtɨ </ts>
               <ts e="T28" id="Seg_416" n="e" s="T27">učʼittɨsak </ts>
               <ts e="T29" id="Seg_418" n="e" s="T28">sompola </ts>
               <ts e="T30" id="Seg_420" n="e" s="T29">poːt </ts>
               <ts e="T31" id="Seg_422" n="e" s="T30">kuntɨ. </ts>
               <ts e="T32" id="Seg_424" n="e" s="T31">Tɨmnʼamɨ </ts>
               <ts e="T33" id="Seg_426" n="e" s="T32">Igarkaqɨn </ts>
               <ts e="T34" id="Seg_428" n="e" s="T33">učʼittɨsa. </ts>
               <ts e="T35" id="Seg_430" n="e" s="T34">Nɨnɨ </ts>
               <ts e="T36" id="Seg_432" n="e" s="T35">man </ts>
               <ts e="T37" id="Seg_434" n="e" s="T36">Igarkantɨ </ts>
               <ts e="T38" id="Seg_436" n="e" s="T37">qəssɨnak. </ts>
               <ts e="T39" id="Seg_438" n="e" s="T38">Igarkaqɨt </ts>
               <ts e="T40" id="Seg_440" n="e" s="T39">mašım </ts>
               <ts e="T41" id="Seg_442" n="e" s="T40">iːsɔːtɨt, </ts>
               <ts e="T42" id="Seg_444" n="e" s="T41">pedučʼilʼšʼantɨ. </ts>
               <ts e="T43" id="Seg_446" n="e" s="T42">Nɨmtɨ </ts>
               <ts e="T44" id="Seg_448" n="e" s="T43">učʼittɨsak. </ts>
               <ts e="T45" id="Seg_450" n="e" s="T44">Pervɨj </ts>
               <ts e="T46" id="Seg_452" n="e" s="T45">kursaqɨt. </ts>
               <ts e="T97" id="Seg_454" n="e" s="T46">I</ts>
               <ts e="T47" id="Seg_456" n="e" s="T97">na </ts>
               <ts e="T48" id="Seg_458" n="e" s="T47">vtoroj </ts>
               <ts e="T50" id="Seg_460" n="e" s="T48">kursaqɨt. </ts>
               <ts e="T51" id="Seg_462" n="e" s="T50">Nɨnɨ </ts>
               <ts e="T52" id="Seg_464" n="e" s="T51">müːtɨ </ts>
               <ts e="T53" id="Seg_466" n="e" s="T52">ɛsɨsɨna. </ts>
               <ts e="T54" id="Seg_468" n="e" s="T53">Müːtontɨ </ts>
               <ts e="T55" id="Seg_470" n="e" s="T54">tɨmnʼamɨ </ts>
               <ts e="T56" id="Seg_472" n="e" s="T55">qəntɨsɔːtɨt. </ts>
               <ts e="T57" id="Seg_474" n="e" s="T56">Man </ts>
               <ts e="T58" id="Seg_476" n="e" s="T57">učʼitɨqo </ts>
               <ts e="T59" id="Seg_478" n="e" s="T58">toː </ts>
               <ts e="T60" id="Seg_480" n="e" s="T59">čʼattɨsɨŋak. </ts>
               <ts e="T61" id="Seg_482" n="e" s="T60">Amamɨ </ts>
               <ts e="T62" id="Seg_484" n="e" s="T61">ontɨqɨt </ts>
               <ts e="T63" id="Seg_486" n="e" s="T62">qalɨmpa. </ts>
               <ts e="T64" id="Seg_488" n="e" s="T63">Apamɨ </ts>
               <ts e="T65" id="Seg_490" n="e" s="T64">okot </ts>
               <ts e="T66" id="Seg_492" n="e" s="T65">qusana. </ts>
               <ts e="T67" id="Seg_494" n="e" s="T66">Nɨnɨ </ts>
               <ts e="T68" id="Seg_496" n="e" s="T67">moqɨnä </ts>
               <ts e="T69" id="Seg_498" n="e" s="T68">tüːsɨŋak. </ts>
               <ts e="T70" id="Seg_500" n="e" s="T69">Moqɨna </ts>
               <ts e="T71" id="Seg_502" n="e" s="T70">tüla </ts>
               <ts e="T72" id="Seg_504" n="e" s="T71">qumɨtɨp </ts>
               <ts e="T73" id="Seg_506" n="e" s="T72">učʼimpɨsak. </ts>
               <ts e="T74" id="Seg_508" n="e" s="T73">Malogramotnɨj </ts>
               <ts e="T75" id="Seg_510" n="e" s="T74">qumɨtɨp. </ts>
               <ts e="T76" id="Seg_512" n="e" s="T75">Nɨnɨ </ts>
               <ts e="T77" id="Seg_514" n="e" s="T76">qumɨtɨp </ts>
               <ts e="T78" id="Seg_516" n="e" s="T77">učʼimpɨsak. </ts>
               <ts e="T79" id="Seg_518" n="e" s="T78">Nɨnɨ </ts>
               <ts e="T80" id="Seg_520" n="e" s="T79">kolhoztɨ </ts>
               <ts e="T81" id="Seg_522" n="e" s="T80">qəssaŋɨk </ts>
               <ts e="T82" id="Seg_524" n="e" s="T81">uːčʼiqɨnoːqo </ts>
               <ts e="T83" id="Seg_526" n="e" s="T82">nʼuːtɨlʼ </ts>
               <ts e="T84" id="Seg_528" n="e" s="T83">pačʼčʼɨtɨsak. </ts>
               <ts e="T85" id="Seg_530" n="e" s="T84">Nɨmtɨ </ts>
               <ts e="T86" id="Seg_532" n="e" s="T85">tɨmnʼamɨ </ts>
               <ts e="T87" id="Seg_534" n="e" s="T86">armijaqɨt </ts>
               <ts e="T88" id="Seg_536" n="e" s="T87">qətpɔːtɨt. </ts>
               <ts e="T89" id="Seg_538" n="e" s="T88">Nɨn </ts>
               <ts e="T90" id="Seg_540" n="e" s="T89">me </ts>
               <ts e="T91" id="Seg_542" n="e" s="T90">amalʼmɨt </ts>
               <ts e="T92" id="Seg_544" n="e" s="T91">onıː </ts>
               <ts e="T93" id="Seg_546" n="e" s="T92">šittälʼ </ts>
               <ts e="T94" id="Seg_548" n="e" s="T93">me </ts>
               <ts e="T95" id="Seg_550" n="e" s="T94">ilisɔːmɨt. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_551" s="T0">SMV_196X_Lifestory_nar.001 (001)</ta>
            <ta e="T4" id="Seg_552" s="T3">SMV_196X_Lifestory_nar.002 (002)</ta>
            <ta e="T8" id="Seg_553" s="T4">SMV_196X_Lifestory_nar.003 (003)</ta>
            <ta e="T11" id="Seg_554" s="T8">SMV_196X_Lifestory_nar.004 (004)</ta>
            <ta e="T15" id="Seg_555" s="T11">SMV_196X_Lifestory_nar.005 (005)</ta>
            <ta e="T26" id="Seg_556" s="T15">SMV_196X_Lifestory_nar.006 (006)</ta>
            <ta e="T31" id="Seg_557" s="T26">SMV_196X_Lifestory_nar.007 (007)</ta>
            <ta e="T34" id="Seg_558" s="T31">SMV_196X_Lifestory_nar.008 (008)</ta>
            <ta e="T38" id="Seg_559" s="T34">SMV_196X_Lifestory_nar.009 (009)</ta>
            <ta e="T42" id="Seg_560" s="T38">SMV_196X_Lifestory_nar.010 (010)</ta>
            <ta e="T44" id="Seg_561" s="T42">SMV_196X_Lifestory_nar.011 (011)</ta>
            <ta e="T46" id="Seg_562" s="T44">SMV_196X_Lifestory_nar.012 (012)</ta>
            <ta e="T50" id="Seg_563" s="T46">SMV_196X_Lifestory_nar.013 (013)</ta>
            <ta e="T53" id="Seg_564" s="T50">SMV_196X_Lifestory_nar.014 (014)</ta>
            <ta e="T56" id="Seg_565" s="T53">SMV_196X_Lifestory_nar.015 (015)</ta>
            <ta e="T60" id="Seg_566" s="T56">SMV_196X_Lifestory_nar.016 (016)</ta>
            <ta e="T63" id="Seg_567" s="T60">SMV_196X_Lifestory_nar.017 (017)</ta>
            <ta e="T66" id="Seg_568" s="T63">SMV_196X_Lifestory_nar.018 (018)</ta>
            <ta e="T69" id="Seg_569" s="T66">SMV_196X_Lifestory_nar.019 (019)</ta>
            <ta e="T73" id="Seg_570" s="T69">SMV_196X_Lifestory_nar.020 (020)</ta>
            <ta e="T75" id="Seg_571" s="T73">SMV_196X_Lifestory_nar.021 (021)</ta>
            <ta e="T78" id="Seg_572" s="T75">SMV_196X_Lifestory_nar.022 (022)</ta>
            <ta e="T84" id="Seg_573" s="T78">SMV_196X_Lifestory_nar.023 (023)</ta>
            <ta e="T88" id="Seg_574" s="T84">SMV_196X_Lifestory_nar.024 (024)</ta>
            <ta e="T95" id="Seg_575" s="T88">SMV_196X_Lifestory_nar.025 (025)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_576" s="T0">Саргаева Марфа Васильевна.</ta>
            <ta e="T4" id="Seg_577" s="T3">Говорите.</ta>
            <ta e="T8" id="Seg_578" s="T4">mat čʼelʼiksak 1924 poqɨntɨ.</ta>
            <ta e="T11" id="Seg_579" s="T8">čʼelɨmpɨtɨlʼ təttam Turuhanska.</ta>
            <ta e="T15" id="Seg_580" s="T11">apamɨn nɨmtɨ nʼutɨlʼ pačʼitɨsa.</ta>
            <ta e="T26" id="Seg_581" s="T15">nɨmtɨ mat selʼčʼi pomɨ turɨsɨtɨ školantɨ šʼim qəntɨsɔːtɨt Turuhanski rajon Farkovontɨ</ta>
            <ta e="T31" id="Seg_582" s="T26">nɨmtɨ učʼitɨsak sombola pot kuntɨ</ta>
            <ta e="T34" id="Seg_583" s="T31">tɨmnʼamɨ Igarkaqɨn učʼitɨsa.</ta>
            <ta e="T38" id="Seg_584" s="T34">nɨnɨ man Igarkantɨ qəssɨŋak.</ta>
            <ta e="T42" id="Seg_585" s="T38">Igarkaqɨn mašip iːsɔːtɨt. pedučʼilʼšʼantɨ.</ta>
            <ta e="T44" id="Seg_586" s="T42">nɨmtɨ učʼitɨsak.</ta>
            <ta e="T46" id="Seg_587" s="T44">pervyj kursaqɨt</ta>
            <ta e="T50" id="Seg_588" s="T46">i na vtoroj kursaqɨt</ta>
            <ta e="T53" id="Seg_589" s="T50">nɨnɨ mütɨ ɛːsɨsɨŋa.</ta>
            <ta e="T56" id="Seg_590" s="T53">mütontɨ tɨmnʼamɨ qəntɨsɔːtɨt.</ta>
            <ta e="T60" id="Seg_591" s="T56">man učʼitɨqo toːčʼatɨsɨŋak.</ta>
            <ta e="T63" id="Seg_592" s="T60">amamɨ ontɨqɨt qalɨmpa. </ta>
            <ta e="T66" id="Seg_593" s="T63">apamɨt okot qussa.</ta>
            <ta e="T69" id="Seg_594" s="T66">nɨnɨ moqɨnä tüsɨŋak.</ta>
            <ta e="T73" id="Seg_595" s="T69">moqɨna tüla qumɨtɨp učʼimpɨsak.</ta>
            <ta e="T75" id="Seg_596" s="T73">malogramotnɨj qumɨtɨp</ta>
            <ta e="T78" id="Seg_597" s="T75">nɨnɨ qumɨtɨp učʼimpɨsak.</ta>
            <ta e="T84" id="Seg_598" s="T78">nɨnɨ mačʼontɨ qəsaŋɨk učʼiqɨnoqo nʼutɨlʼ pačʼitɨsak. </ta>
            <ta e="T88" id="Seg_599" s="T84">nɨmtɨ tɨmnʼamɨ armijaqɨt qətpɔːtɨt.</ta>
            <ta e="T95" id="Seg_600" s="T88">nɨn me amalʼmɨt onɨ šʼittälʼ mɨ ilisɔːmɨt.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_601" s="T0">Sargaeva Marfa Vasilʼjevna. </ta>
            <ta e="T4" id="Seg_602" s="T3">Govoritʼe. </ta>
            <ta e="T8" id="Seg_603" s="T4">Mat čʼeːlʼiksanak 1924 poːqɨntɨ. </ta>
            <ta e="T11" id="Seg_604" s="T8">Čʼeːlɨmpɨtɨlʼ təttap Turuhanska. </ta>
            <ta e="T15" id="Seg_605" s="T11">Apamɨn nɨmtɨ nʼuːtɨlʼ pačʼčʼɨtɨsa. </ta>
            <ta e="T26" id="Seg_606" s="T15">Nɨnɨ mat selʼčʼi poːtɨmɨ tuːrɨsɨtɨ, školantɨ šıp qəntɨsɔːtɨt Turuhanskij rajon Farkovontɨ. </ta>
            <ta e="T31" id="Seg_607" s="T26">Nɨmtɨ učʼittɨsak sompola poːt kuntɨ. </ta>
            <ta e="T34" id="Seg_608" s="T31">Tɨmnʼamɨ Igarkaqɨn učʼittɨsa. </ta>
            <ta e="T38" id="Seg_609" s="T34">Nɨnɨ man Igarkantɨ qəssɨnak. </ta>
            <ta e="T42" id="Seg_610" s="T38">Igarkaqɨt mašım iːsɔːtɨt, pedučʼilʼšʼantɨ. </ta>
            <ta e="T44" id="Seg_611" s="T42">Nɨmtɨ učʼittɨsak. </ta>
            <ta e="T46" id="Seg_612" s="T44">Pervɨj kursaqɨt. </ta>
            <ta e="T50" id="Seg_613" s="T46">Ina vtoroj kursaqɨt. </ta>
            <ta e="T53" id="Seg_614" s="T50">Nɨnɨ müːtɨ ɛsɨsɨna. </ta>
            <ta e="T56" id="Seg_615" s="T53">Müːtontɨ tɨmnʼamɨ qəntɨsɔːtɨt. </ta>
            <ta e="T60" id="Seg_616" s="T56">Man učʼitɨqo toː čʼattɨsɨŋak. </ta>
            <ta e="T63" id="Seg_617" s="T60">Amamɨ ontɨqɨt qalɨmpa. </ta>
            <ta e="T66" id="Seg_618" s="T63">Apamɨ okot qusana. </ta>
            <ta e="T69" id="Seg_619" s="T66">Nɨnɨ moqɨnä tüːsɨŋak. </ta>
            <ta e="T73" id="Seg_620" s="T69">Moqɨna tüla qumɨtɨp učʼimpɨsak. </ta>
            <ta e="T75" id="Seg_621" s="T73">Malogramotnɨj qumɨtɨp. </ta>
            <ta e="T78" id="Seg_622" s="T75">Nɨnɨ qumɨtɨp učʼimpɨsak. </ta>
            <ta e="T84" id="Seg_623" s="T78">Nɨnɨ kolhoztɨ qəssaŋɨk uːčʼiqɨnoːqo nʼuːtɨlʼ pačʼčʼɨtɨsak. </ta>
            <ta e="T88" id="Seg_624" s="T84">Nɨmtɨ tɨmnʼamɨ armijaqɨt qətpɔːtɨt. </ta>
            <ta e="T95" id="Seg_625" s="T88">Nɨn me amalʼmɨt onıː šittälʼ me ilisɔːmɨt. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T5" id="Seg_626" s="T4">mat</ta>
            <ta e="T6" id="Seg_627" s="T5">čʼeːlʼik-sa-na-k</ta>
            <ta e="T8" id="Seg_628" s="T7">poː-qɨn-tɨ</ta>
            <ta e="T9" id="Seg_629" s="T8">čʼeːlɨm-pɨtɨlʼ</ta>
            <ta e="T10" id="Seg_630" s="T9">tətta-p</ta>
            <ta e="T11" id="Seg_631" s="T10">Turuhanska</ta>
            <ta e="T12" id="Seg_632" s="T11">apa-mɨn</ta>
            <ta e="T13" id="Seg_633" s="T12">nɨmtɨ</ta>
            <ta e="T14" id="Seg_634" s="T13">nʼuːtɨ-lʼ</ta>
            <ta e="T15" id="Seg_635" s="T14">pačʼčʼɨ-tɨ-sa</ta>
            <ta e="T16" id="Seg_636" s="T15">nɨnɨ</ta>
            <ta e="T17" id="Seg_637" s="T16">mat</ta>
            <ta e="T18" id="Seg_638" s="T17">selʼčʼi</ta>
            <ta e="T19" id="Seg_639" s="T18">poː-t-ɨ-mɨ</ta>
            <ta e="T20" id="Seg_640" s="T19">tuːrɨ-sɨ-tɨ</ta>
            <ta e="T21" id="Seg_641" s="T20">škola-ntɨ</ta>
            <ta e="T22" id="Seg_642" s="T21">šıp</ta>
            <ta e="T23" id="Seg_643" s="T22">qən-tɨ-sɔː-tɨt</ta>
            <ta e="T24" id="Seg_644" s="T23">Turuhanskij</ta>
            <ta e="T25" id="Seg_645" s="T24">rajon</ta>
            <ta e="T26" id="Seg_646" s="T25">Farkovo-ntɨ</ta>
            <ta e="T27" id="Seg_647" s="T26">nɨmtɨ</ta>
            <ta e="T28" id="Seg_648" s="T27">učʼit-tɨ-sa-k</ta>
            <ta e="T29" id="Seg_649" s="T28">sompola</ta>
            <ta e="T30" id="Seg_650" s="T29">poː-t</ta>
            <ta e="T31" id="Seg_651" s="T30">kuntɨ</ta>
            <ta e="T32" id="Seg_652" s="T31">tɨmnʼa-mɨ</ta>
            <ta e="T33" id="Seg_653" s="T32">Igarka-qɨn</ta>
            <ta e="T34" id="Seg_654" s="T33">učʼit-tɨ-sa</ta>
            <ta e="T35" id="Seg_655" s="T34">nɨnɨ</ta>
            <ta e="T36" id="Seg_656" s="T35">man</ta>
            <ta e="T37" id="Seg_657" s="T36">Igarka-ntɨ</ta>
            <ta e="T38" id="Seg_658" s="T37">qəs-sɨ-na-k</ta>
            <ta e="T39" id="Seg_659" s="T38">Igarka-qɨt</ta>
            <ta e="T40" id="Seg_660" s="T39">mašım</ta>
            <ta e="T41" id="Seg_661" s="T40">iː-sɔː-tɨt</ta>
            <ta e="T42" id="Seg_662" s="T41">pedučʼilʼšʼa-ntɨ</ta>
            <ta e="T43" id="Seg_663" s="T42">nɨmtɨ</ta>
            <ta e="T44" id="Seg_664" s="T43">učʼit-tɨ-sa-k</ta>
            <ta e="T45" id="Seg_665" s="T44">pervɨj</ta>
            <ta e="T46" id="Seg_666" s="T45">kursa-qɨt</ta>
            <ta e="T50" id="Seg_667" s="T48">kursa-qɨt</ta>
            <ta e="T51" id="Seg_668" s="T50">nɨnɨ</ta>
            <ta e="T52" id="Seg_669" s="T51">müːtɨ</ta>
            <ta e="T53" id="Seg_670" s="T52">ɛsɨ-sɨ-na</ta>
            <ta e="T54" id="Seg_671" s="T53">müːto-ntɨ</ta>
            <ta e="T55" id="Seg_672" s="T54">tɨmnʼa-mɨ</ta>
            <ta e="T56" id="Seg_673" s="T55">qən-tɨ-sɔː-tɨt</ta>
            <ta e="T57" id="Seg_674" s="T56">man</ta>
            <ta e="T58" id="Seg_675" s="T57">učʼitɨ-qo</ta>
            <ta e="T59" id="Seg_676" s="T58">toː</ta>
            <ta e="T60" id="Seg_677" s="T59">čʼattɨ-sɨ-ŋa-k</ta>
            <ta e="T61" id="Seg_678" s="T60">ama-mɨ</ta>
            <ta e="T62" id="Seg_679" s="T61">ontɨ-qɨt</ta>
            <ta e="T63" id="Seg_680" s="T62">qalɨ-mpa</ta>
            <ta e="T64" id="Seg_681" s="T63">apa-mɨ</ta>
            <ta e="T65" id="Seg_682" s="T64">okot</ta>
            <ta e="T66" id="Seg_683" s="T65">qu-sa-na</ta>
            <ta e="T67" id="Seg_684" s="T66">nɨnɨ</ta>
            <ta e="T68" id="Seg_685" s="T67">moqɨnä</ta>
            <ta e="T69" id="Seg_686" s="T68">tüː-sɨ-ŋa-k</ta>
            <ta e="T70" id="Seg_687" s="T69">moqɨna</ta>
            <ta e="T71" id="Seg_688" s="T70">tü-la</ta>
            <ta e="T72" id="Seg_689" s="T71">qum-ɨ-t-ɨ-p</ta>
            <ta e="T73" id="Seg_690" s="T72">učʼi-mpɨ-sa-k</ta>
            <ta e="T74" id="Seg_691" s="T73">malogramotnɨj</ta>
            <ta e="T75" id="Seg_692" s="T74">qum-ɨ-t-ɨ-p</ta>
            <ta e="T76" id="Seg_693" s="T75">nɨnɨ</ta>
            <ta e="T77" id="Seg_694" s="T76">qum-ɨ-t-ɨ-p</ta>
            <ta e="T78" id="Seg_695" s="T77">učʼi-mpɨ-sa-k</ta>
            <ta e="T79" id="Seg_696" s="T78">nɨnɨ</ta>
            <ta e="T80" id="Seg_697" s="T79">kolhoz-tɨ</ta>
            <ta e="T81" id="Seg_698" s="T80">qəs-sa-ŋɨ-k</ta>
            <ta e="T82" id="Seg_699" s="T81">uːčʼi-qɨnoːqo</ta>
            <ta e="T83" id="Seg_700" s="T82">nʼuːtɨ-lʼ</ta>
            <ta e="T84" id="Seg_701" s="T83">pačʼčʼɨ-tɨ-sa-k</ta>
            <ta e="T85" id="Seg_702" s="T84">nɨmtɨ</ta>
            <ta e="T86" id="Seg_703" s="T85">tɨmnʼa-mɨ</ta>
            <ta e="T87" id="Seg_704" s="T86">armija-qɨt</ta>
            <ta e="T88" id="Seg_705" s="T87">qət-pɔː-tɨt</ta>
            <ta e="T89" id="Seg_706" s="T88">nɨn</ta>
            <ta e="T90" id="Seg_707" s="T89">me</ta>
            <ta e="T91" id="Seg_708" s="T90">ama-lʼ-mɨ-t</ta>
            <ta e="T92" id="Seg_709" s="T91">onıː</ta>
            <ta e="T93" id="Seg_710" s="T92">šittä-lʼ</ta>
            <ta e="T94" id="Seg_711" s="T93">me</ta>
            <ta e="T95" id="Seg_712" s="T94">ili-sɔː-mɨt</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T5" id="Seg_713" s="T4">man</ta>
            <ta e="T6" id="Seg_714" s="T5">čʼeːlɨŋ-sɨ-ŋɨ-k</ta>
            <ta e="T8" id="Seg_715" s="T7">poː-qɨn-ntɨ</ta>
            <ta e="T9" id="Seg_716" s="T8">čʼeːlɨŋ-mpɨlʼ</ta>
            <ta e="T10" id="Seg_717" s="T9">təttɨ-mɨ</ta>
            <ta e="T11" id="Seg_718" s="T10">Turuhanska</ta>
            <ta e="T12" id="Seg_719" s="T11">apa-mɨt</ta>
            <ta e="T13" id="Seg_720" s="T12">nɨmtɨ</ta>
            <ta e="T14" id="Seg_721" s="T13">nʼuːtɨ-lʼ</ta>
            <ta e="T15" id="Seg_722" s="T14">pačʼčʼɨ-ntɨ-sɨ</ta>
            <ta e="T16" id="Seg_723" s="T15">nɨːnɨ</ta>
            <ta e="T17" id="Seg_724" s="T16">man</ta>
            <ta e="T18" id="Seg_725" s="T17">seːlʼčʼɨ</ta>
            <ta e="T19" id="Seg_726" s="T18">poː-t-ɨ-mɨ</ta>
            <ta e="T20" id="Seg_727" s="T19">tuːrɨ-sɨ-tɨ</ta>
            <ta e="T21" id="Seg_728" s="T20">škola-ntɨ</ta>
            <ta e="T22" id="Seg_729" s="T21">mašım</ta>
            <ta e="T23" id="Seg_730" s="T22">qən-tɨ-sɨ-tɨt</ta>
            <ta e="T24" id="Seg_731" s="T23">Turuhanska</ta>
            <ta e="T25" id="Seg_732" s="T24">rajon</ta>
            <ta e="T26" id="Seg_733" s="T25">Farkovo-ntɨ</ta>
            <ta e="T27" id="Seg_734" s="T26">nɨmtɨ</ta>
            <ta e="T28" id="Seg_735" s="T27">učʼitɨ-ntɨ-sɨ-k</ta>
            <ta e="T29" id="Seg_736" s="T28">sompɨla</ta>
            <ta e="T30" id="Seg_737" s="T29">poː-n</ta>
            <ta e="T31" id="Seg_738" s="T30">kuntɨ</ta>
            <ta e="T32" id="Seg_739" s="T31">timnʼa-mɨ</ta>
            <ta e="T33" id="Seg_740" s="T32">Igarka-qɨn</ta>
            <ta e="T34" id="Seg_741" s="T33">učʼitɨ-ntɨ-sɨ</ta>
            <ta e="T35" id="Seg_742" s="T34">nɨːnɨ</ta>
            <ta e="T36" id="Seg_743" s="T35">man</ta>
            <ta e="T37" id="Seg_744" s="T36">Igarka-ntɨ</ta>
            <ta e="T38" id="Seg_745" s="T37">qən-sɨ-ŋɨ-k</ta>
            <ta e="T39" id="Seg_746" s="T38">Igarka-qɨn</ta>
            <ta e="T40" id="Seg_747" s="T39">mašım</ta>
            <ta e="T41" id="Seg_748" s="T40">iː-sɨ-tɨt</ta>
            <ta e="T42" id="Seg_749" s="T41">pedučʼilʼšʼa-ntɨ</ta>
            <ta e="T43" id="Seg_750" s="T42">nɨmtɨ</ta>
            <ta e="T44" id="Seg_751" s="T43">učʼitɨ-ntɨ-sɨ-k</ta>
            <ta e="T45" id="Seg_752" s="T44">pervɨj</ta>
            <ta e="T46" id="Seg_753" s="T45">kursa-qɨn</ta>
            <ta e="T50" id="Seg_754" s="T48">kursa-qɨn</ta>
            <ta e="T51" id="Seg_755" s="T50">nɨːnɨ</ta>
            <ta e="T52" id="Seg_756" s="T51">mütɨ</ta>
            <ta e="T53" id="Seg_757" s="T52">ɛsɨ-sɨ-n</ta>
            <ta e="T54" id="Seg_758" s="T53">mütɨ-ntɨ</ta>
            <ta e="T55" id="Seg_759" s="T54">timnʼa-mɨ</ta>
            <ta e="T56" id="Seg_760" s="T55">qən-tɨ-sɨ-tɨt</ta>
            <ta e="T57" id="Seg_761" s="T56">man</ta>
            <ta e="T58" id="Seg_762" s="T57">učʼitɨ-qo</ta>
            <ta e="T59" id="Seg_763" s="T58">toː</ta>
            <ta e="T60" id="Seg_764" s="T59">čʼattɨ-sɨ-ŋɨ-k</ta>
            <ta e="T61" id="Seg_765" s="T60">ama-mɨ</ta>
            <ta e="T62" id="Seg_766" s="T61">ontɨ-qɨn</ta>
            <ta e="T63" id="Seg_767" s="T62">qalɨ-mpɨ</ta>
            <ta e="T64" id="Seg_768" s="T63">apa-mɨ</ta>
            <ta e="T65" id="Seg_769" s="T64">ukoːn</ta>
            <ta e="T66" id="Seg_770" s="T65">qu-sɨ-n</ta>
            <ta e="T67" id="Seg_771" s="T66">nɨːnɨ</ta>
            <ta e="T68" id="Seg_772" s="T67">moqɨnä</ta>
            <ta e="T69" id="Seg_773" s="T68">tü-sɨ-ŋɨ-k</ta>
            <ta e="T70" id="Seg_774" s="T69">moqɨnä</ta>
            <ta e="T71" id="Seg_775" s="T70">tü-lä</ta>
            <ta e="T72" id="Seg_776" s="T71">qum-ɨ-t-ɨ-m</ta>
            <ta e="T73" id="Seg_777" s="T72">učʼi-mpɨ-sɨ-k</ta>
            <ta e="T74" id="Seg_778" s="T73">malogramotnɨj</ta>
            <ta e="T75" id="Seg_779" s="T74">qum-ɨ-t-ɨ-m</ta>
            <ta e="T76" id="Seg_780" s="T75">nɨːnɨ</ta>
            <ta e="T77" id="Seg_781" s="T76">qum-ɨ-t-ɨ-m</ta>
            <ta e="T78" id="Seg_782" s="T77">učʼi-mpɨ-sɨ-k</ta>
            <ta e="T79" id="Seg_783" s="T78">nɨːnɨ</ta>
            <ta e="T80" id="Seg_784" s="T79">kolhoz-ntɨ</ta>
            <ta e="T81" id="Seg_785" s="T80">qən-sɨ-ŋɨ-k</ta>
            <ta e="T82" id="Seg_786" s="T81">uːčʼɨ-qɨnoːqo</ta>
            <ta e="T83" id="Seg_787" s="T82">nʼuːtɨ-lʼ</ta>
            <ta e="T84" id="Seg_788" s="T83">pačʼčʼɨ-tɨ-sɨ-k</ta>
            <ta e="T85" id="Seg_789" s="T84">nɨmtɨ</ta>
            <ta e="T86" id="Seg_790" s="T85">timnʼa-mɨ</ta>
            <ta e="T87" id="Seg_791" s="T86">armi-qɨn</ta>
            <ta e="T88" id="Seg_792" s="T87">qət-mpɨ-tɨt</ta>
            <ta e="T89" id="Seg_793" s="T88">nɨːnɨ</ta>
            <ta e="T90" id="Seg_794" s="T89">meː</ta>
            <ta e="T91" id="Seg_795" s="T90">ama-lʼ-mɨ-n</ta>
            <ta e="T92" id="Seg_796" s="T91">onıː</ta>
            <ta e="T93" id="Seg_797" s="T92">šittɨ-lʼ</ta>
            <ta e="T94" id="Seg_798" s="T93">meː</ta>
            <ta e="T95" id="Seg_799" s="T94">ilɨ-sɨ-mɨt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T5" id="Seg_800" s="T4">I.NOM</ta>
            <ta e="T6" id="Seg_801" s="T5">be.born-PST-CO-1SG.S</ta>
            <ta e="T8" id="Seg_802" s="T7">year-LOC-OBL.3SG</ta>
            <ta e="T9" id="Seg_803" s="T8">give.birth-PTCP.PST</ta>
            <ta e="T10" id="Seg_804" s="T9">earth.[NOM]-1SG</ta>
            <ta e="T11" id="Seg_805" s="T10">Turukhansk.[3SG.S]</ta>
            <ta e="T12" id="Seg_806" s="T11">daddy.[NOM]-1PL</ta>
            <ta e="T13" id="Seg_807" s="T12">there</ta>
            <ta e="T14" id="Seg_808" s="T13">hay-ADJZ</ta>
            <ta e="T15" id="Seg_809" s="T14">chop-IPFV-PST.[3SG.S]</ta>
            <ta e="T16" id="Seg_810" s="T15">then</ta>
            <ta e="T17" id="Seg_811" s="T16">I.NOM</ta>
            <ta e="T18" id="Seg_812" s="T17">seven</ta>
            <ta e="T19" id="Seg_813" s="T18">tree-PL.[NOM]-EP-1SG</ta>
            <ta e="T20" id="Seg_814" s="T19">come-PST-3SG.O</ta>
            <ta e="T21" id="Seg_815" s="T20">school-ILL</ta>
            <ta e="T22" id="Seg_816" s="T21">I.ACC</ta>
            <ta e="T23" id="Seg_817" s="T22">leave-TR-PST-3PL</ta>
            <ta e="T24" id="Seg_818" s="T23">Turukhansk</ta>
            <ta e="T25" id="Seg_819" s="T24">region.[NOM]</ta>
            <ta e="T26" id="Seg_820" s="T25">Farkovo-ILL</ta>
            <ta e="T27" id="Seg_821" s="T26">there</ta>
            <ta e="T28" id="Seg_822" s="T27">study-IPFV-PST-1SG.S</ta>
            <ta e="T29" id="Seg_823" s="T28">five</ta>
            <ta e="T30" id="Seg_824" s="T29">year-GEN</ta>
            <ta e="T31" id="Seg_825" s="T30">during</ta>
            <ta e="T32" id="Seg_826" s="T31">brother.[NOM]-1SG</ta>
            <ta e="T33" id="Seg_827" s="T32">Igarka-LOC</ta>
            <ta e="T34" id="Seg_828" s="T33">study-IPFV-PST.[3SG.S]</ta>
            <ta e="T35" id="Seg_829" s="T34">then</ta>
            <ta e="T36" id="Seg_830" s="T35">I.NOM</ta>
            <ta e="T37" id="Seg_831" s="T36">Igarka-ILL</ta>
            <ta e="T38" id="Seg_832" s="T37">leave-PST-CO-1SG.S</ta>
            <ta e="T39" id="Seg_833" s="T38">Igarka-LOC</ta>
            <ta e="T40" id="Seg_834" s="T39">I.ACC</ta>
            <ta e="T41" id="Seg_835" s="T40">take-PST-3PL</ta>
            <ta e="T42" id="Seg_836" s="T41">teacher.training.college-ILL</ta>
            <ta e="T43" id="Seg_837" s="T42">there</ta>
            <ta e="T44" id="Seg_838" s="T43">study-IPFV-PST-1SG.S</ta>
            <ta e="T45" id="Seg_839" s="T44">first</ta>
            <ta e="T46" id="Seg_840" s="T45">term-LOC</ta>
            <ta e="T50" id="Seg_841" s="T48">term-LOC</ta>
            <ta e="T51" id="Seg_842" s="T50">then</ta>
            <ta e="T52" id="Seg_843" s="T51">war.[NOM]</ta>
            <ta e="T53" id="Seg_844" s="T52">become-PST-3SG.S</ta>
            <ta e="T54" id="Seg_845" s="T53">war-ILL</ta>
            <ta e="T55" id="Seg_846" s="T54">brother.[NOM]-1SG</ta>
            <ta e="T56" id="Seg_847" s="T55">leave-TR-PST-3PL</ta>
            <ta e="T57" id="Seg_848" s="T56">I.NOM</ta>
            <ta e="T58" id="Seg_849" s="T57">study-INF</ta>
            <ta e="T59" id="Seg_850" s="T58">away</ta>
            <ta e="T60" id="Seg_851" s="T59">cease-PST-CO-1SG.S</ta>
            <ta e="T61" id="Seg_852" s="T60">mother.[NOM]-1SG</ta>
            <ta e="T62" id="Seg_853" s="T61">himself-LOC</ta>
            <ta e="T63" id="Seg_854" s="T62">stay-PST.NAR.[3SG.S]</ta>
            <ta e="T64" id="Seg_855" s="T63">daddy.[NOM]-1SG</ta>
            <ta e="T65" id="Seg_856" s="T64">earlier</ta>
            <ta e="T66" id="Seg_857" s="T65">die-PST-3SG.S</ta>
            <ta e="T67" id="Seg_858" s="T66">then</ta>
            <ta e="T68" id="Seg_859" s="T67">home</ta>
            <ta e="T69" id="Seg_860" s="T68">come-PST-CO-1SG.S</ta>
            <ta e="T70" id="Seg_861" s="T69">home</ta>
            <ta e="T71" id="Seg_862" s="T70">come-CVB</ta>
            <ta e="T72" id="Seg_863" s="T71">human.being-EP-PL-EP-ACC</ta>
            <ta e="T73" id="Seg_864" s="T72">teach-HAB-PST-1SG.S</ta>
            <ta e="T74" id="Seg_865" s="T73">uneducated</ta>
            <ta e="T75" id="Seg_866" s="T74">human.being-EP-PL-EP-ACC</ta>
            <ta e="T76" id="Seg_867" s="T75">then</ta>
            <ta e="T77" id="Seg_868" s="T76">human.being-EP-PL-EP-ACC</ta>
            <ta e="T78" id="Seg_869" s="T77">teach-HAB-PST-1SG.S</ta>
            <ta e="T79" id="Seg_870" s="T78">then</ta>
            <ta e="T80" id="Seg_871" s="T79">kolkhoz-ILL</ta>
            <ta e="T81" id="Seg_872" s="T80">leave-PST-CO-1SG.S</ta>
            <ta e="T82" id="Seg_873" s="T81">work-SUP.1SG</ta>
            <ta e="T83" id="Seg_874" s="T82">hay-ADJZ</ta>
            <ta e="T84" id="Seg_875" s="T83">chop-TR-PST-1SG.S</ta>
            <ta e="T85" id="Seg_876" s="T84">there</ta>
            <ta e="T86" id="Seg_877" s="T85">brother.[NOM]-1SG</ta>
            <ta e="T87" id="Seg_878" s="T86">army-LOC</ta>
            <ta e="T88" id="Seg_879" s="T87">kill-PST.NAR-3PL</ta>
            <ta e="T89" id="Seg_880" s="T88">then</ta>
            <ta e="T90" id="Seg_881" s="T89">we.PL.NOM</ta>
            <ta e="T91" id="Seg_882" s="T90">mother-ADJZ-something-GEN</ta>
            <ta e="T92" id="Seg_883" s="T91">ourselves.1DU.[NOM]</ta>
            <ta e="T93" id="Seg_884" s="T92">two-ADJZ</ta>
            <ta e="T94" id="Seg_885" s="T93">we.PL</ta>
            <ta e="T95" id="Seg_886" s="T94">live-PST-1PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T5" id="Seg_887" s="T4">я.NOM</ta>
            <ta e="T6" id="Seg_888" s="T5">родиться-PST-CO-1SG.S</ta>
            <ta e="T8" id="Seg_889" s="T7">год-LOC-OBL.3SG</ta>
            <ta e="T9" id="Seg_890" s="T8">родить-PTCP.PST</ta>
            <ta e="T10" id="Seg_891" s="T9">земля.[NOM]-1SG</ta>
            <ta e="T11" id="Seg_892" s="T10">Туруханский.[3SG.S]</ta>
            <ta e="T12" id="Seg_893" s="T11">папа.[NOM]-1PL</ta>
            <ta e="T13" id="Seg_894" s="T12">там</ta>
            <ta e="T14" id="Seg_895" s="T13">сено-ADJZ</ta>
            <ta e="T15" id="Seg_896" s="T14">рубить-IPFV-PST.[3SG.S]</ta>
            <ta e="T16" id="Seg_897" s="T15">потом</ta>
            <ta e="T17" id="Seg_898" s="T16">я.NOM</ta>
            <ta e="T18" id="Seg_899" s="T17">семь</ta>
            <ta e="T19" id="Seg_900" s="T18">дерево-PL.[NOM]-EP-1SG</ta>
            <ta e="T20" id="Seg_901" s="T19">наступить-PST-3SG.O</ta>
            <ta e="T21" id="Seg_902" s="T20">школа-ILL</ta>
            <ta e="T22" id="Seg_903" s="T21">я.ACC</ta>
            <ta e="T23" id="Seg_904" s="T22">отправиться-TR-PST-3PL</ta>
            <ta e="T24" id="Seg_905" s="T23">Туруханский</ta>
            <ta e="T25" id="Seg_906" s="T24">район.[NOM]</ta>
            <ta e="T26" id="Seg_907" s="T25">Фарково-ILL</ta>
            <ta e="T27" id="Seg_908" s="T26">там</ta>
            <ta e="T28" id="Seg_909" s="T27">учиться-IPFV-PST-1SG.S</ta>
            <ta e="T29" id="Seg_910" s="T28">пять</ta>
            <ta e="T30" id="Seg_911" s="T29">год-GEN</ta>
            <ta e="T31" id="Seg_912" s="T30">в.течение</ta>
            <ta e="T32" id="Seg_913" s="T31">брат.[NOM]-1SG</ta>
            <ta e="T33" id="Seg_914" s="T32">Игарка-LOC</ta>
            <ta e="T34" id="Seg_915" s="T33">учиться-IPFV-PST.[3SG.S]</ta>
            <ta e="T35" id="Seg_916" s="T34">потом</ta>
            <ta e="T36" id="Seg_917" s="T35">я.NOM</ta>
            <ta e="T37" id="Seg_918" s="T36">Игарка-ILL</ta>
            <ta e="T38" id="Seg_919" s="T37">отправиться-PST-CO-1SG.S</ta>
            <ta e="T39" id="Seg_920" s="T38">Игарка-LOC</ta>
            <ta e="T40" id="Seg_921" s="T39">я.ACC</ta>
            <ta e="T41" id="Seg_922" s="T40">взять-PST-3PL</ta>
            <ta e="T42" id="Seg_923" s="T41">педучилище-ILL</ta>
            <ta e="T43" id="Seg_924" s="T42">там</ta>
            <ta e="T44" id="Seg_925" s="T43">учиться-IPFV-PST-1SG.S</ta>
            <ta e="T45" id="Seg_926" s="T44">первый</ta>
            <ta e="T46" id="Seg_927" s="T45">курс-LOC</ta>
            <ta e="T50" id="Seg_928" s="T48">курс-LOC</ta>
            <ta e="T51" id="Seg_929" s="T50">потом</ta>
            <ta e="T52" id="Seg_930" s="T51">война.[NOM]</ta>
            <ta e="T53" id="Seg_931" s="T52">стать-PST-3SG.S</ta>
            <ta e="T54" id="Seg_932" s="T53">война-ILL</ta>
            <ta e="T55" id="Seg_933" s="T54">брат.[NOM]-1SG</ta>
            <ta e="T56" id="Seg_934" s="T55">отправиться-TR-PST-3PL</ta>
            <ta e="T57" id="Seg_935" s="T56">я.NOM</ta>
            <ta e="T58" id="Seg_936" s="T57">учиться-INF</ta>
            <ta e="T59" id="Seg_937" s="T58">прочь</ta>
            <ta e="T60" id="Seg_938" s="T59">перестать-PST-CO-1SG.S</ta>
            <ta e="T61" id="Seg_939" s="T60">мать.[NOM]-1SG</ta>
            <ta e="T62" id="Seg_940" s="T61">он.сам-LOC</ta>
            <ta e="T63" id="Seg_941" s="T62">остаться-PST.NAR.[3SG.S]</ta>
            <ta e="T64" id="Seg_942" s="T63">папа.[NOM]-1SG</ta>
            <ta e="T65" id="Seg_943" s="T64">раньше</ta>
            <ta e="T66" id="Seg_944" s="T65">умереть-PST-3SG.S</ta>
            <ta e="T67" id="Seg_945" s="T66">потом</ta>
            <ta e="T68" id="Seg_946" s="T67">домой</ta>
            <ta e="T69" id="Seg_947" s="T68">прийти-PST-CO-1SG.S</ta>
            <ta e="T70" id="Seg_948" s="T69">домой</ta>
            <ta e="T71" id="Seg_949" s="T70">прийти-CVB</ta>
            <ta e="T72" id="Seg_950" s="T71">человек-EP-PL-EP-ACC</ta>
            <ta e="T73" id="Seg_951" s="T72">учить-HAB-PST-1SG.S</ta>
            <ta e="T74" id="Seg_952" s="T73">малограмотный</ta>
            <ta e="T75" id="Seg_953" s="T74">человек-EP-PL-EP-ACC</ta>
            <ta e="T76" id="Seg_954" s="T75">потом</ta>
            <ta e="T77" id="Seg_955" s="T76">человек-EP-PL-EP-ACC</ta>
            <ta e="T78" id="Seg_956" s="T77">учить-HAB-PST-1SG.S</ta>
            <ta e="T79" id="Seg_957" s="T78">потом</ta>
            <ta e="T80" id="Seg_958" s="T79">колхоз-ILL</ta>
            <ta e="T81" id="Seg_959" s="T80">отправиться-PST-CO-1SG.S</ta>
            <ta e="T82" id="Seg_960" s="T81">работать-SUP.1SG</ta>
            <ta e="T83" id="Seg_961" s="T82">сено-ADJZ</ta>
            <ta e="T84" id="Seg_962" s="T83">рубить-TR-PST-1SG.S</ta>
            <ta e="T85" id="Seg_963" s="T84">там</ta>
            <ta e="T86" id="Seg_964" s="T85">брат.[NOM]-1SG</ta>
            <ta e="T87" id="Seg_965" s="T86">армия-LOC</ta>
            <ta e="T88" id="Seg_966" s="T87">убить-PST.NAR-3PL</ta>
            <ta e="T89" id="Seg_967" s="T88">потом</ta>
            <ta e="T90" id="Seg_968" s="T89">мы.PL.NOM</ta>
            <ta e="T91" id="Seg_969" s="T90">мать-ADJZ-нечто-GEN</ta>
            <ta e="T92" id="Seg_970" s="T91">мы.сами.1DU.[NOM]</ta>
            <ta e="T93" id="Seg_971" s="T92">два-ADJZ</ta>
            <ta e="T94" id="Seg_972" s="T93">we.PL</ta>
            <ta e="T95" id="Seg_973" s="T94">жить-PST-1PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T5" id="Seg_974" s="T4">pers</ta>
            <ta e="T6" id="Seg_975" s="T5">v-v:tense-v:ins-v:pn</ta>
            <ta e="T8" id="Seg_976" s="T7">n-n:case2-n:obl.poss</ta>
            <ta e="T9" id="Seg_977" s="T8">v-v&gt;ptcp</ta>
            <ta e="T10" id="Seg_978" s="T9">n-n:case1-n:poss</ta>
            <ta e="T11" id="Seg_979" s="T10">adj-v:pn</ta>
            <ta e="T12" id="Seg_980" s="T11">n-n:case1-n:poss</ta>
            <ta e="T13" id="Seg_981" s="T12">adv</ta>
            <ta e="T14" id="Seg_982" s="T13">n-n&gt;adj</ta>
            <ta e="T15" id="Seg_983" s="T14">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_984" s="T15">adv</ta>
            <ta e="T17" id="Seg_985" s="T16">pers</ta>
            <ta e="T18" id="Seg_986" s="T17">num</ta>
            <ta e="T19" id="Seg_987" s="T18">n-n:num-n:case1-n:(ins)-n:poss</ta>
            <ta e="T20" id="Seg_988" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_989" s="T20">n-n:case3</ta>
            <ta e="T22" id="Seg_990" s="T21">pers</ta>
            <ta e="T23" id="Seg_991" s="T22">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_992" s="T23">adj</ta>
            <ta e="T25" id="Seg_993" s="T24">n-n:case3</ta>
            <ta e="T26" id="Seg_994" s="T25">nprop-n:case3</ta>
            <ta e="T27" id="Seg_995" s="T26">adv</ta>
            <ta e="T28" id="Seg_996" s="T27">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_997" s="T28">num</ta>
            <ta e="T30" id="Seg_998" s="T29">n-n:case3</ta>
            <ta e="T31" id="Seg_999" s="T30">pp</ta>
            <ta e="T32" id="Seg_1000" s="T31">n-n:case1-n:poss</ta>
            <ta e="T33" id="Seg_1001" s="T32">nprop-n:case3</ta>
            <ta e="T34" id="Seg_1002" s="T33">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_1003" s="T34">adv</ta>
            <ta e="T36" id="Seg_1004" s="T35">pers</ta>
            <ta e="T37" id="Seg_1005" s="T36">nprop-n:case3</ta>
            <ta e="T38" id="Seg_1006" s="T37">v-v:tense-v:ins-v:pn</ta>
            <ta e="T39" id="Seg_1007" s="T38">nprop-n:case3</ta>
            <ta e="T40" id="Seg_1008" s="T39">pers</ta>
            <ta e="T41" id="Seg_1009" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_1010" s="T41">n-n:case3</ta>
            <ta e="T43" id="Seg_1011" s="T42">adv</ta>
            <ta e="T44" id="Seg_1012" s="T43">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_1013" s="T44">adj</ta>
            <ta e="T46" id="Seg_1014" s="T45">n-n:case3</ta>
            <ta e="T50" id="Seg_1015" s="T48">n-n:case3</ta>
            <ta e="T51" id="Seg_1016" s="T50">adv</ta>
            <ta e="T52" id="Seg_1017" s="T51">n-n:case3</ta>
            <ta e="T53" id="Seg_1018" s="T52">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_1019" s="T53">n-n:case3</ta>
            <ta e="T55" id="Seg_1020" s="T54">n-n:case1-n:poss</ta>
            <ta e="T56" id="Seg_1021" s="T55">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_1022" s="T56">pers</ta>
            <ta e="T58" id="Seg_1023" s="T57">v-v:inf</ta>
            <ta e="T59" id="Seg_1024" s="T58">preverb</ta>
            <ta e="T60" id="Seg_1025" s="T59">v-v:tense-v:ins-v:pn</ta>
            <ta e="T61" id="Seg_1026" s="T60">n-n:case1-n:poss</ta>
            <ta e="T62" id="Seg_1027" s="T61">pro-n:case3</ta>
            <ta e="T63" id="Seg_1028" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_1029" s="T63">n-n:case1-n:poss</ta>
            <ta e="T65" id="Seg_1030" s="T64">adv</ta>
            <ta e="T66" id="Seg_1031" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_1032" s="T66">adv</ta>
            <ta e="T68" id="Seg_1033" s="T67">adv</ta>
            <ta e="T69" id="Seg_1034" s="T68">v-v:tense-v:ins-v:pn</ta>
            <ta e="T70" id="Seg_1035" s="T69">adv</ta>
            <ta e="T71" id="Seg_1036" s="T70">v-v&gt;adv</ta>
            <ta e="T72" id="Seg_1037" s="T71">n-n:(ins)-n:num-n:(ins)-n:case3</ta>
            <ta e="T73" id="Seg_1038" s="T72">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_1039" s="T73">adj</ta>
            <ta e="T75" id="Seg_1040" s="T74">n-n:(ins)-n:num-n:(ins)-n:case3</ta>
            <ta e="T76" id="Seg_1041" s="T75">adv</ta>
            <ta e="T77" id="Seg_1042" s="T76">n-n:(ins)-n:num-n:(ins)-n:case3</ta>
            <ta e="T78" id="Seg_1043" s="T77">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_1044" s="T78">adv</ta>
            <ta e="T80" id="Seg_1045" s="T79">n-n:case3</ta>
            <ta e="T81" id="Seg_1046" s="T80">v-v:tense-v:ins-v:pn</ta>
            <ta e="T82" id="Seg_1047" s="T81">v-v:inf-poss</ta>
            <ta e="T83" id="Seg_1048" s="T82">n-n&gt;adj</ta>
            <ta e="T84" id="Seg_1049" s="T83">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_1050" s="T84">adv</ta>
            <ta e="T86" id="Seg_1051" s="T85">n-n:case1-n:poss</ta>
            <ta e="T87" id="Seg_1052" s="T86">n-n:case3</ta>
            <ta e="T88" id="Seg_1053" s="T87">v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_1054" s="T88">adv</ta>
            <ta e="T90" id="Seg_1055" s="T89">pers</ta>
            <ta e="T91" id="Seg_1056" s="T90">n-n&gt;adj-n-n:case3</ta>
            <ta e="T92" id="Seg_1057" s="T91">pro-n:case3</ta>
            <ta e="T93" id="Seg_1058" s="T92">num-n&gt;adj</ta>
            <ta e="T94" id="Seg_1059" s="T93">pers</ta>
            <ta e="T95" id="Seg_1060" s="T94">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T5" id="Seg_1061" s="T4">pers</ta>
            <ta e="T6" id="Seg_1062" s="T5">v</ta>
            <ta e="T8" id="Seg_1063" s="T7">n</ta>
            <ta e="T9" id="Seg_1064" s="T8">v</ta>
            <ta e="T10" id="Seg_1065" s="T9">n</ta>
            <ta e="T11" id="Seg_1066" s="T10">adj</ta>
            <ta e="T12" id="Seg_1067" s="T11">n</ta>
            <ta e="T13" id="Seg_1068" s="T12">adv</ta>
            <ta e="T14" id="Seg_1069" s="T13">adj</ta>
            <ta e="T15" id="Seg_1070" s="T14">v</ta>
            <ta e="T16" id="Seg_1071" s="T15">adv</ta>
            <ta e="T17" id="Seg_1072" s="T16">pers</ta>
            <ta e="T18" id="Seg_1073" s="T17">num</ta>
            <ta e="T19" id="Seg_1074" s="T18">n</ta>
            <ta e="T20" id="Seg_1075" s="T19">v</ta>
            <ta e="T21" id="Seg_1076" s="T20">n</ta>
            <ta e="T22" id="Seg_1077" s="T21">pers</ta>
            <ta e="T23" id="Seg_1078" s="T22">v</ta>
            <ta e="T24" id="Seg_1079" s="T23">adj</ta>
            <ta e="T25" id="Seg_1080" s="T24">n</ta>
            <ta e="T26" id="Seg_1081" s="T25">nprop</ta>
            <ta e="T27" id="Seg_1082" s="T26">adv</ta>
            <ta e="T28" id="Seg_1083" s="T27">v</ta>
            <ta e="T29" id="Seg_1084" s="T28">num</ta>
            <ta e="T30" id="Seg_1085" s="T29">n</ta>
            <ta e="T31" id="Seg_1086" s="T30">pp</ta>
            <ta e="T32" id="Seg_1087" s="T31">n</ta>
            <ta e="T33" id="Seg_1088" s="T32">nprop</ta>
            <ta e="T34" id="Seg_1089" s="T33">v</ta>
            <ta e="T35" id="Seg_1090" s="T34">adv</ta>
            <ta e="T36" id="Seg_1091" s="T35">pers</ta>
            <ta e="T37" id="Seg_1092" s="T36">nprop</ta>
            <ta e="T38" id="Seg_1093" s="T37">v</ta>
            <ta e="T39" id="Seg_1094" s="T38">nprop</ta>
            <ta e="T40" id="Seg_1095" s="T39">pers</ta>
            <ta e="T41" id="Seg_1096" s="T40">v</ta>
            <ta e="T42" id="Seg_1097" s="T41">n</ta>
            <ta e="T43" id="Seg_1098" s="T42">adv</ta>
            <ta e="T44" id="Seg_1099" s="T43">v</ta>
            <ta e="T45" id="Seg_1100" s="T44">adj</ta>
            <ta e="T46" id="Seg_1101" s="T45">n</ta>
            <ta e="T50" id="Seg_1102" s="T48">n</ta>
            <ta e="T51" id="Seg_1103" s="T50">adv</ta>
            <ta e="T52" id="Seg_1104" s="T51">n</ta>
            <ta e="T53" id="Seg_1105" s="T52">v</ta>
            <ta e="T54" id="Seg_1106" s="T53">n</ta>
            <ta e="T55" id="Seg_1107" s="T54">n</ta>
            <ta e="T56" id="Seg_1108" s="T55">v</ta>
            <ta e="T57" id="Seg_1109" s="T56">pers</ta>
            <ta e="T58" id="Seg_1110" s="T57">v</ta>
            <ta e="T59" id="Seg_1111" s="T58">preverb</ta>
            <ta e="T60" id="Seg_1112" s="T59">v</ta>
            <ta e="T61" id="Seg_1113" s="T60">n</ta>
            <ta e="T62" id="Seg_1114" s="T61">pro</ta>
            <ta e="T63" id="Seg_1115" s="T62">v</ta>
            <ta e="T64" id="Seg_1116" s="T63">n</ta>
            <ta e="T65" id="Seg_1117" s="T64">adv</ta>
            <ta e="T66" id="Seg_1118" s="T65">v</ta>
            <ta e="T67" id="Seg_1119" s="T66">adv</ta>
            <ta e="T68" id="Seg_1120" s="T67">adv</ta>
            <ta e="T69" id="Seg_1121" s="T68">v</ta>
            <ta e="T70" id="Seg_1122" s="T69">adv</ta>
            <ta e="T71" id="Seg_1123" s="T70">adv</ta>
            <ta e="T72" id="Seg_1124" s="T71">n</ta>
            <ta e="T73" id="Seg_1125" s="T72">v</ta>
            <ta e="T74" id="Seg_1126" s="T73">adj</ta>
            <ta e="T75" id="Seg_1127" s="T74">n</ta>
            <ta e="T76" id="Seg_1128" s="T75">adv</ta>
            <ta e="T77" id="Seg_1129" s="T76">n</ta>
            <ta e="T78" id="Seg_1130" s="T77">v</ta>
            <ta e="T79" id="Seg_1131" s="T78">adv</ta>
            <ta e="T80" id="Seg_1132" s="T79">n</ta>
            <ta e="T81" id="Seg_1133" s="T80">v</ta>
            <ta e="T82" id="Seg_1134" s="T81">v</ta>
            <ta e="T83" id="Seg_1135" s="T82">adj</ta>
            <ta e="T84" id="Seg_1136" s="T83">v</ta>
            <ta e="T85" id="Seg_1137" s="T84">adv</ta>
            <ta e="T86" id="Seg_1138" s="T85">n</ta>
            <ta e="T87" id="Seg_1139" s="T86">n</ta>
            <ta e="T88" id="Seg_1140" s="T87">v</ta>
            <ta e="T89" id="Seg_1141" s="T88">adv</ta>
            <ta e="T90" id="Seg_1142" s="T89">pers</ta>
            <ta e="T91" id="Seg_1143" s="T90">adj</ta>
            <ta e="T92" id="Seg_1144" s="T91">pro</ta>
            <ta e="T93" id="Seg_1145" s="T92">adv</ta>
            <ta e="T94" id="Seg_1146" s="T93">pers</ta>
            <ta e="T95" id="Seg_1147" s="T94">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T11" id="Seg_1148" s="T10">RUS:cult</ta>
            <ta e="T21" id="Seg_1149" s="T20">RUS:cult</ta>
            <ta e="T24" id="Seg_1150" s="T23">RUS:cult</ta>
            <ta e="T25" id="Seg_1151" s="T24">RUS:cult</ta>
            <ta e="T26" id="Seg_1152" s="T25">RUS:cult</ta>
            <ta e="T28" id="Seg_1153" s="T27">RUS:cult</ta>
            <ta e="T33" id="Seg_1154" s="T32">RUS:cult</ta>
            <ta e="T34" id="Seg_1155" s="T33">RUS:cult</ta>
            <ta e="T37" id="Seg_1156" s="T36">RUS:cult</ta>
            <ta e="T39" id="Seg_1157" s="T38">RUS:cult</ta>
            <ta e="T42" id="Seg_1158" s="T41">RUS:cult</ta>
            <ta e="T44" id="Seg_1159" s="T43">RUS:cult</ta>
            <ta e="T45" id="Seg_1160" s="T44">RUS:cult</ta>
            <ta e="T46" id="Seg_1161" s="T45">RUS:cult</ta>
            <ta e="T50" id="Seg_1162" s="T48">RUS:cult</ta>
            <ta e="T58" id="Seg_1163" s="T57">RUS:cult</ta>
            <ta e="T73" id="Seg_1164" s="T72">RUS:cult</ta>
            <ta e="T74" id="Seg_1165" s="T73">RUS:cult</ta>
            <ta e="T78" id="Seg_1166" s="T77">RUS:cult</ta>
            <ta e="T80" id="Seg_1167" s="T79">RUS:cult</ta>
            <ta e="T87" id="Seg_1168" s="T86">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_1169" s="T0">Саргаева Марфа Васильевна.</ta>
            <ta e="T4" id="Seg_1170" s="T3">Говорите.</ta>
            <ta e="T8" id="Seg_1171" s="T4">Я родилась в 1924 году.</ta>
            <ta e="T11" id="Seg_1172" s="T8">Я родилась в Туруханской земле.</ta>
            <ta e="T15" id="Seg_1173" s="T11">Наш отец там косил сено.</ta>
            <ta e="T26" id="Seg_1174" s="T15">Потом мне исполнилось семь лет, в школу меня повезли в Туруханский район в Фарково.</ta>
            <ta e="T31" id="Seg_1175" s="T26">Там я училась пять лет.</ta>
            <ta e="T34" id="Seg_1176" s="T31">Мой брат учился в Игарке.</ta>
            <ta e="T38" id="Seg_1177" s="T34">Потом я поехала в Игарку.</ta>
            <ta e="T42" id="Seg_1178" s="T38">В Игарке меня взяли, в педучилище.</ta>
            <ta e="T44" id="Seg_1179" s="T42">Там я училась.</ta>
            <ta e="T46" id="Seg_1180" s="T44">На первом курсе.</ta>
            <ta e="T50" id="Seg_1181" s="T46">И на втором курсе.</ta>
            <ta e="T53" id="Seg_1182" s="T50">Потом война пришла.</ta>
            <ta e="T56" id="Seg_1183" s="T53">На войну моего брата отправили.</ta>
            <ta e="T60" id="Seg_1184" s="T56">Я бросила учиться.</ta>
            <ta e="T63" id="Seg_1185" s="T60">Мать моя одна осталась.</ta>
            <ta e="T66" id="Seg_1186" s="T63">Отец мой давно умер.</ta>
            <ta e="T69" id="Seg_1187" s="T66">Потом домой приехала.</ta>
            <ta e="T73" id="Seg_1188" s="T69">Домой приехав, я людей учила.</ta>
            <ta e="T75" id="Seg_1189" s="T73">Малограмотных людей.</ta>
            <ta e="T78" id="Seg_1190" s="T75">Потом людей учила.</ta>
            <ta e="T84" id="Seg_1191" s="T78">Потом в колхоз отправилась работать, сено косила. </ta>
            <ta e="T88" id="Seg_1192" s="T84">Там моего брата в армии убили.</ta>
            <ta e="T95" id="Seg_1193" s="T88">Потом мы с матерью вдвоём жили.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_1194" s="T0">Sargaeva Marfa Vasilyevna.</ta>
            <ta e="T4" id="Seg_1195" s="T3">Speak.</ta>
            <ta e="T8" id="Seg_1196" s="T4">I was born in 1924.</ta>
            <ta e="T11" id="Seg_1197" s="T8">I was born in the land of Turukhansk.</ta>
            <ta e="T15" id="Seg_1198" s="T11">Our father had been cutting hay there.</ta>
            <ta e="T26" id="Seg_1199" s="T15">Then I turned seven years old, I was sent to school to Farkovo of the Turukhansk region.</ta>
            <ta e="T31" id="Seg_1200" s="T26">I studied there for five years.</ta>
            <ta e="T34" id="Seg_1201" s="T31">My brother studied in Igarka.</ta>
            <ta e="T38" id="Seg_1202" s="T34">Then I went to Igarka.</ta>
            <ta e="T42" id="Seg_1203" s="T38">In Igarka I was taken to a teacher college.</ta>
            <ta e="T44" id="Seg_1204" s="T42">I studied there.</ta>
            <ta e="T46" id="Seg_1205" s="T44">In the first year.</ta>
            <ta e="T50" id="Seg_1206" s="T46">And in the second year.</ta>
            <ta e="T53" id="Seg_1207" s="T50">Then the war began.</ta>
            <ta e="T56" id="Seg_1208" s="T53">My brother was sent to the war.</ta>
            <ta e="T60" id="Seg_1209" s="T56">I quited the studies.</ta>
            <ta e="T63" id="Seg_1210" s="T60">My mother stayed alone.</ta>
            <ta e="T66" id="Seg_1211" s="T63">My father had died earlier.</ta>
            <ta e="T69" id="Seg_1212" s="T66">Then I came home.</ta>
            <ta e="T73" id="Seg_1213" s="T69">Having come home, I taught people.</ta>
            <ta e="T75" id="Seg_1214" s="T73">Uneducated people.</ta>
            <ta e="T78" id="Seg_1215" s="T75">Then I taught people.</ta>
            <ta e="T84" id="Seg_1216" s="T78">Then I went to work in a kolkhoz, I cut hay.</ta>
            <ta e="T88" id="Seg_1217" s="T84">There my brother was killed in the army.</ta>
            <ta e="T95" id="Seg_1218" s="T88">Then I lived together with my mother.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_1219" s="T0">Sargaeva Marfa Vasiljevna.</ta>
            <ta e="T4" id="Seg_1220" s="T3">Sprechen Sie.</ta>
            <ta e="T8" id="Seg_1221" s="T4">Ich wurde 1924 geboren.</ta>
            <ta e="T11" id="Seg_1222" s="T8">Ich wurde im Land von Turuchansk geboren.</ta>
            <ta e="T15" id="Seg_1223" s="T11">Unser Vater schnitt dort Heu.</ta>
            <ta e="T26" id="Seg_1224" s="T15">Dann wurde ich sieben Jahre alt, ich wurde in die Schule nach Farkovo in der Region Turuchansk geschickt.</ta>
            <ta e="T31" id="Seg_1225" s="T26">Ich lernte dort fünf Jahre lang.</ta>
            <ta e="T34" id="Seg_1226" s="T31">Mein Bruder lernte in Igarka.</ta>
            <ta e="T38" id="Seg_1227" s="T34">Dann ging ich nach Igarka.</ta>
            <ta e="T42" id="Seg_1228" s="T38">In Igarka wurde ich an der pädagogischen Hochschule angenommen.</ta>
            <ta e="T44" id="Seg_1229" s="T42">Dort studierte ich.</ta>
            <ta e="T46" id="Seg_1230" s="T44">Im ersten Jahr.</ta>
            <ta e="T50" id="Seg_1231" s="T46">Und im zweiten Jahr.</ta>
            <ta e="T53" id="Seg_1232" s="T50">Dann fing der Krieg an.</ta>
            <ta e="T56" id="Seg_1233" s="T53">Mein Bruder wurde in den Krieg geschickt.</ta>
            <ta e="T60" id="Seg_1234" s="T56">Ich hört auf zu studieren.</ta>
            <ta e="T63" id="Seg_1235" s="T60">Meine Mutter blieb alleine.</ta>
            <ta e="T66" id="Seg_1236" s="T63">Mein Vater war früher gestorben.</ta>
            <ta e="T69" id="Seg_1237" s="T66">Dann kam ich nach Hause.</ta>
            <ta e="T73" id="Seg_1238" s="T69">Nachdem ich nach Hause gekommen war, unterrichete ich Leute.</ta>
            <ta e="T75" id="Seg_1239" s="T73">Ungebildete Leute.</ta>
            <ta e="T78" id="Seg_1240" s="T75">Dann unterrichtete ich Leute.</ta>
            <ta e="T84" id="Seg_1241" s="T78">Dann ging ich in eine Kolchose zum arbeiten, ich schnitt Heu.</ta>
            <ta e="T88" id="Seg_1242" s="T84">Da wurde mein Bruder in der Armee getötet.</ta>
            <ta e="T95" id="Seg_1243" s="T88">Dann lebte ich zusammen mit meiner Mutter.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_1244" s="T0">Саргаева Марфа Васильевна.</ta>
            <ta e="T4" id="Seg_1245" s="T3">Говорите.</ta>
            <ta e="T8" id="Seg_1246" s="T4">я родилась в 1924 году</ta>
            <ta e="T11" id="Seg_1247" s="T8">я родилась в Туруханске (= в Туруханской земле).</ta>
            <ta e="T15" id="Seg_1248" s="T11">мой отец там косил сено</ta>
            <ta e="T26" id="Seg_1249" s="T15">когда мне исполнилось семь лет в школу меня повезли в Туруханский район в Фарково</ta>
            <ta e="T31" id="Seg_1250" s="T26">там училась 5 лет</ta>
            <ta e="T34" id="Seg_1251" s="T31">мой брат учился в Игарке.</ta>
            <ta e="T38" id="Seg_1252" s="T34">потом я поехала в Игарку</ta>
            <ta e="T42" id="Seg_1253" s="T38">в Игарке меня взяли в педучилище.</ta>
            <ta e="T44" id="Seg_1254" s="T42">там училась.</ta>
            <ta e="T46" id="Seg_1255" s="T44">на первом курсе</ta>
            <ta e="T50" id="Seg_1256" s="T46">и втором курсе</ta>
            <ta e="T53" id="Seg_1257" s="T50">потом война пришла.</ta>
            <ta e="T56" id="Seg_1258" s="T53">на войну мой брат ушёл.</ta>
            <ta e="T60" id="Seg_1259" s="T56">я бросила учиться.</ta>
            <ta e="T63" id="Seg_1260" s="T60">мать одна осталась.</ta>
            <ta e="T66" id="Seg_1261" s="T63">отец давно умер.</ta>
            <ta e="T69" id="Seg_1262" s="T66">потом домой приехала.</ta>
            <ta e="T73" id="Seg_1263" s="T69">домой приехав, я людей учила</ta>
            <ta e="T75" id="Seg_1264" s="T73">малограмотных людей</ta>
            <ta e="T78" id="Seg_1265" s="T75">потом людей учила.</ta>
            <ta e="T84" id="Seg_1266" s="T78">потом в лес поехала работать, сено косила. </ta>
            <ta e="T88" id="Seg_1267" s="T84">там брата в армии убили.</ta>
            <ta e="T95" id="Seg_1268" s="T88">потом мы с матерью вдвоём жили.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_1269" s="T3">Said by A.I. Kuzmina.</ta>
            <ta e="T8" id="Seg_1270" s="T4">[BrM] Tentative transcription of 'čʼelʼiksanak'.</ta>
            <ta e="T26" id="Seg_1271" s="T15">[BrM] Unexpected PL marker in 'poːtɨmɨ'. Unexpected objective conjugation in 'tuːrɨsɨtɨ'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T97" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
