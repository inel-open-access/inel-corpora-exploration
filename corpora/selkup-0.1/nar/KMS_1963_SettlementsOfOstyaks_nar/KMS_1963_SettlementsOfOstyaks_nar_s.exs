<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID517C63C5-A9F5-E930-6B07-37DB65E48DDE">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\KMS_1963_SettlementsOfOstyaks_nar\KMS_1963_SettlementsOfOstyaks_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">29</ud-information>
            <ud-information attribute-name="# HIAT:w">23</ud-information>
            <ud-information attribute-name="# e">23</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMS">
            <abbreviation>KMS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T23" id="Seg_0" n="sc" s="T0">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">uːkon</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">süsüqula</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">äːduruguzattə</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">patʼälguzattə</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">maːdɨlam</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">aː</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">qädan</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">krajɣɨn</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">äːdurguzattə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">aːrɨtqö</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">qätqɨnnä</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">to</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">sündʼeːɣɨn</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">naŋo</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">lärumbɨkuzattə</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">sɨːnnelanni</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_59" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">sɨnnela</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">palʼdʼükuzattə</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">süsügü</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">äːdɨlawɨn</ts>
                  <nts id="Seg_71" n="HIAT:ip">,</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">qwätkuzattə</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">quːlam</ts>
                  <nts id="Seg_78" n="HIAT:ip">,</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">twäːlurguzattə</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T23" id="Seg_84" n="sc" s="T0">
               <ts e="T1" id="Seg_86" n="e" s="T0">uːkon </ts>
               <ts e="T2" id="Seg_88" n="e" s="T1">süsüqula </ts>
               <ts e="T3" id="Seg_90" n="e" s="T2">äːduruguzattə </ts>
               <ts e="T4" id="Seg_92" n="e" s="T3">patʼälguzattə </ts>
               <ts e="T5" id="Seg_94" n="e" s="T4">maːdɨlam </ts>
               <ts e="T6" id="Seg_96" n="e" s="T5">aː </ts>
               <ts e="T7" id="Seg_98" n="e" s="T6">qädan </ts>
               <ts e="T8" id="Seg_100" n="e" s="T7">krajɣɨn. </ts>
               <ts e="T9" id="Seg_102" n="e" s="T8">äːdurguzattə </ts>
               <ts e="T10" id="Seg_104" n="e" s="T9">aːrɨtqö </ts>
               <ts e="T11" id="Seg_106" n="e" s="T10">qätqɨnnä </ts>
               <ts e="T12" id="Seg_108" n="e" s="T11">to </ts>
               <ts e="T13" id="Seg_110" n="e" s="T12">sündʼeːɣɨn. </ts>
               <ts e="T14" id="Seg_112" n="e" s="T13">naŋo </ts>
               <ts e="T15" id="Seg_114" n="e" s="T14">lärumbɨkuzattə </ts>
               <ts e="T16" id="Seg_116" n="e" s="T15">sɨːnnelanni. </ts>
               <ts e="T17" id="Seg_118" n="e" s="T16">sɨnnela </ts>
               <ts e="T18" id="Seg_120" n="e" s="T17">palʼdʼükuzattə </ts>
               <ts e="T19" id="Seg_122" n="e" s="T18">süsügü </ts>
               <ts e="T20" id="Seg_124" n="e" s="T19">äːdɨlawɨn, </ts>
               <ts e="T21" id="Seg_126" n="e" s="T20">qwätkuzattə </ts>
               <ts e="T22" id="Seg_128" n="e" s="T21">quːlam, </ts>
               <ts e="T23" id="Seg_130" n="e" s="T22">twäːlurguzattə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T8" id="Seg_131" s="T0">KMS_1963_SettlementsOfOstyaks_nar.001 (001.001)</ta>
            <ta e="T13" id="Seg_132" s="T8">KMS_1963_SettlementsOfOstyaks_nar.002 (001.002)</ta>
            <ta e="T16" id="Seg_133" s="T13">KMS_1963_SettlementsOfOstyaks_nar.003 (001.003)</ta>
            <ta e="T23" id="Seg_134" s="T16">KMS_1963_SettlementsOfOstyaks_nar.004 (001.004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T8" id="Seg_135" s="T0">ӯ′кон ′сӱсӱkула ′ӓ̄дуругу′заттъ па′тʼӓлгузаттъ ′ма̄дылам а̄ ′kӓдан ′крайɣын.</ta>
            <ta e="T13" id="Seg_136" s="T8">′ӓ̄дургузаттъ ′а̄рытkӧ ′kӓтkыннӓ ′то сӱн′дʼе̄ɣын.</ta>
            <ta e="T16" id="Seg_137" s="T13">на′ңо ‵лӓрумбыку′заттъ ′сы̄ннеланни(е).</ta>
            <ta e="T23" id="Seg_138" s="T16">′сыннела ‵палʼдʼӱку′заттъ ′сӱсӱг(ɣ)ӱ ′ӓ̄дылаwын, kwӓтку′заттъ ′kӯлам, ′твӓ̄лургузаттъ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T8" id="Seg_139" s="T0">uːkon süsüqula äːduruguzattə patʼälguzattə maːdɨlam aː qädan krajɣɨn.</ta>
            <ta e="T13" id="Seg_140" s="T8">äːdurguzattə aːrɨtqö qätqɨnnä to sündʼeːɣɨn.</ta>
            <ta e="T16" id="Seg_141" s="T13">naŋo lärumbɨkuzattə sɨːnnelanni(e).</ta>
            <ta e="T23" id="Seg_142" s="T16">sɨnnela palʼdʼükuzattə süsüg(ɣ)ü äːdɨlawɨn, qwätkuzattə quːlam, tväːlurguzattə.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T8" id="Seg_143" s="T0">uːkon süsüqula äːduruguzattə patʼälguzattə maːdɨlam aː qädan krajɣɨn. </ta>
            <ta e="T13" id="Seg_144" s="T8">äːdurguzattə aːrɨtqö qätqɨnnä to sündʼeːɣɨn. </ta>
            <ta e="T16" id="Seg_145" s="T13">naŋo lärumbɨkuzattə sɨːnnelanni. </ta>
            <ta e="T23" id="Seg_146" s="T16">sɨnnela palʼdʼükuzattə süsügü äːdɨlawɨn, qwätkuzattə quːlam, twäːlurguzattə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_147" s="T0">uːkon</ta>
            <ta e="T2" id="Seg_148" s="T1">süsüqu-la</ta>
            <ta e="T3" id="Seg_149" s="T2">äːdu-r-u-gu-za-ttə</ta>
            <ta e="T4" id="Seg_150" s="T3">patʼäl-gu-za-ttə</ta>
            <ta e="T5" id="Seg_151" s="T4">maːd-ɨ-la-m</ta>
            <ta e="T6" id="Seg_152" s="T5">aː</ta>
            <ta e="T7" id="Seg_153" s="T6">qäd-a-n</ta>
            <ta e="T8" id="Seg_154" s="T7">kraj-ɣɨn</ta>
            <ta e="T9" id="Seg_155" s="T8">äːdu-r-gu-za-ttə</ta>
            <ta e="T10" id="Seg_156" s="T9">aːrɨtqö</ta>
            <ta e="T11" id="Seg_157" s="T10">qät-qɨnnä</ta>
            <ta e="T12" id="Seg_158" s="T11">to</ta>
            <ta e="T13" id="Seg_159" s="T12">sündʼeː-ɣɨn</ta>
            <ta e="T14" id="Seg_160" s="T13">naŋo</ta>
            <ta e="T15" id="Seg_161" s="T14">läru-mbɨ-ku-za-ttə</ta>
            <ta e="T16" id="Seg_162" s="T15">sɨːnne-la-nni</ta>
            <ta e="T17" id="Seg_163" s="T16">sɨnne-la</ta>
            <ta e="T18" id="Seg_164" s="T17">palʼdʼü-ku-za-ttə</ta>
            <ta e="T19" id="Seg_165" s="T18">süsügü</ta>
            <ta e="T20" id="Seg_166" s="T19">äːdɨ-la-wɨn</ta>
            <ta e="T21" id="Seg_167" s="T20">qwät-ku-za-ttə</ta>
            <ta e="T22" id="Seg_168" s="T21">quː-la-m</ta>
            <ta e="T23" id="Seg_169" s="T22">twäːl-u-r-gu-za-ttə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_170" s="T0">ugoːn</ta>
            <ta e="T2" id="Seg_171" s="T1">süsügu-la</ta>
            <ta e="T3" id="Seg_172" s="T2">eːtə-r-ɨ-gu-sɨ-tɨt</ta>
            <ta e="T4" id="Seg_173" s="T3">patʼäl-gu-sɨ-tɨt</ta>
            <ta e="T5" id="Seg_174" s="T4">maːt-ɨ-la-m</ta>
            <ta e="T6" id="Seg_175" s="T5">ašša</ta>
            <ta e="T7" id="Seg_176" s="T6">qäd-ɨ-n</ta>
            <ta e="T8" id="Seg_177" s="T7">kraj-qən</ta>
            <ta e="T9" id="Seg_178" s="T8">eːtə-r-kku-sɨ-tɨt</ta>
            <ta e="T10" id="Seg_179" s="T9">aːrɨtqö</ta>
            <ta e="T11" id="Seg_180" s="T10">qäd-qɨnnɨ</ta>
            <ta e="T12" id="Seg_181" s="T11">toː</ta>
            <ta e="T13" id="Seg_182" s="T12">sündʼi-qən</ta>
            <ta e="T14" id="Seg_183" s="T13">nanto</ta>
            <ta e="T15" id="Seg_184" s="T14">larɨ-mbɨ-kku-sɨ-tɨt</ta>
            <ta e="T16" id="Seg_185" s="T15">sɨnne-la-nɨ</ta>
            <ta e="T17" id="Seg_186" s="T16">sɨnne-la</ta>
            <ta e="T18" id="Seg_187" s="T17">paldʼu-kku-sɨ-tɨt</ta>
            <ta e="T19" id="Seg_188" s="T18">süsügu</ta>
            <ta e="T20" id="Seg_189" s="T19">eːtə-la-mɨn</ta>
            <ta e="T21" id="Seg_190" s="T20">qwat-kku-sɨ-tɨt</ta>
            <ta e="T22" id="Seg_191" s="T21">qum-la-m</ta>
            <ta e="T23" id="Seg_192" s="T22">twɨlʼ-ɨ-r-gu-sɨ-tɨt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_193" s="T0">earlier </ta>
            <ta e="T2" id="Seg_194" s="T1">Selkup-PL</ta>
            <ta e="T3" id="Seg_195" s="T2">village-VBLZ-EP-INF-PST-3PL</ta>
            <ta e="T4" id="Seg_196" s="T3">chop-INF-PST-3PL</ta>
            <ta e="T5" id="Seg_197" s="T4">house-EP-PL-ACC</ta>
            <ta e="T6" id="Seg_198" s="T5">NEG</ta>
            <ta e="T7" id="Seg_199" s="T6">Ket-EP-GEN</ta>
            <ta e="T8" id="Seg_200" s="T7">border-LOC</ta>
            <ta e="T9" id="Seg_201" s="T8">village-VBLZ-DUR-PST-3PL</ta>
            <ta e="T10" id="Seg_202" s="T9">aside</ta>
            <ta e="T11" id="Seg_203" s="T10">Ket-EL</ta>
            <ta e="T12" id="Seg_204" s="T11">lake.[NOM]</ta>
            <ta e="T13" id="Seg_205" s="T12">inside-LOC</ta>
            <ta e="T14" id="Seg_206" s="T13">after.that</ta>
            <ta e="T15" id="Seg_207" s="T14">be.afraid-HAB-DUR-PST-3PL</ta>
            <ta e="T16" id="Seg_208" s="T15">robber-PL-ALL</ta>
            <ta e="T17" id="Seg_209" s="T16">robber-PL</ta>
            <ta e="T18" id="Seg_210" s="T17">go-DUR-PST-3PL</ta>
            <ta e="T19" id="Seg_211" s="T18">Selkup.[NOM]</ta>
            <ta e="T20" id="Seg_212" s="T19">village-PL-PROL</ta>
            <ta e="T21" id="Seg_213" s="T20">kill-DUR-PST-3PL</ta>
            <ta e="T22" id="Seg_214" s="T21">human.being-PL.[NOM]-ACC</ta>
            <ta e="T23" id="Seg_215" s="T22">steal-EP-FRQ-INF-PST-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_216" s="T0">раньше </ta>
            <ta e="T2" id="Seg_217" s="T1">селькуп-PL</ta>
            <ta e="T3" id="Seg_218" s="T2">деревня-VBLZ-EP-INF-PST-3PL</ta>
            <ta e="T4" id="Seg_219" s="T3">срубить-INF-PST-3PL</ta>
            <ta e="T5" id="Seg_220" s="T4">дом-EP-PL-ACC</ta>
            <ta e="T6" id="Seg_221" s="T5">NEG</ta>
            <ta e="T7" id="Seg_222" s="T6">Кеть-EP-GEN</ta>
            <ta e="T8" id="Seg_223" s="T7">край-LOC</ta>
            <ta e="T9" id="Seg_224" s="T8">деревня-VBLZ-DUR-PST-3PL</ta>
            <ta e="T10" id="Seg_225" s="T9">в.сторону</ta>
            <ta e="T11" id="Seg_226" s="T10">Кеть-ЕL</ta>
            <ta e="T12" id="Seg_227" s="T11">озеро.[NOM]</ta>
            <ta e="T13" id="Seg_228" s="T12">нутро-LOC</ta>
            <ta e="T14" id="Seg_229" s="T13">после.этого</ta>
            <ta e="T15" id="Seg_230" s="T14">бояться-HAB-DUR-PST-3PL</ta>
            <ta e="T16" id="Seg_231" s="T15">разбойник-PL-ALL</ta>
            <ta e="T17" id="Seg_232" s="T16">разбойник-PL</ta>
            <ta e="T18" id="Seg_233" s="T17">ходить-DUR-PST-3PL</ta>
            <ta e="T19" id="Seg_234" s="T18">селькуп.[NOM]</ta>
            <ta e="T20" id="Seg_235" s="T19">деревня-PL-PROL</ta>
            <ta e="T21" id="Seg_236" s="T20">убить-DUR-PST-3PL</ta>
            <ta e="T22" id="Seg_237" s="T21">человек-PL.[NOM]-ACC</ta>
            <ta e="T23" id="Seg_238" s="T22">украсть-EP-FRQ-INF-PST-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_239" s="T0">adv</ta>
            <ta e="T2" id="Seg_240" s="T1">n-n:num</ta>
            <ta e="T3" id="Seg_241" s="T2">n-n&gt;v-n:ins-v:inf-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_242" s="T3">v-v:inf-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_243" s="T4">n-n:ins-n:num-n:case1</ta>
            <ta e="T6" id="Seg_244" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_245" s="T6">nprop-n:ins-n:case1</ta>
            <ta e="T8" id="Seg_246" s="T7">n-n:case2</ta>
            <ta e="T9" id="Seg_247" s="T8">n-n&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_248" s="T9">adv</ta>
            <ta e="T11" id="Seg_249" s="T10">nprop-n:case2</ta>
            <ta e="T12" id="Seg_250" s="T11">n-n:case1</ta>
            <ta e="T13" id="Seg_251" s="T12">n-n:case2</ta>
            <ta e="T14" id="Seg_252" s="T13">adv</ta>
            <ta e="T15" id="Seg_253" s="T14">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_254" s="T15">n-n:num-n:case2</ta>
            <ta e="T17" id="Seg_255" s="T16">n-n:num</ta>
            <ta e="T18" id="Seg_256" s="T17">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_257" s="T18">n-n:case1</ta>
            <ta e="T20" id="Seg_258" s="T19">n-n:num-n:case2</ta>
            <ta e="T21" id="Seg_259" s="T20">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_260" s="T21">n-n:num-n:case1-n:case</ta>
            <ta e="T23" id="Seg_261" s="T22">v-n:ins-v&gt;v-v:inf-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_262" s="T0">adv</ta>
            <ta e="T2" id="Seg_263" s="T1">n</ta>
            <ta e="T3" id="Seg_264" s="T2">v</ta>
            <ta e="T4" id="Seg_265" s="T3">v</ta>
            <ta e="T5" id="Seg_266" s="T4">n</ta>
            <ta e="T6" id="Seg_267" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_268" s="T6">nprop</ta>
            <ta e="T8" id="Seg_269" s="T7">n</ta>
            <ta e="T9" id="Seg_270" s="T8">v</ta>
            <ta e="T10" id="Seg_271" s="T9">adv</ta>
            <ta e="T11" id="Seg_272" s="T10">nprop</ta>
            <ta e="T12" id="Seg_273" s="T11">n</ta>
            <ta e="T13" id="Seg_274" s="T12">n</ta>
            <ta e="T14" id="Seg_275" s="T13">adv</ta>
            <ta e="T15" id="Seg_276" s="T14">v</ta>
            <ta e="T16" id="Seg_277" s="T15">n</ta>
            <ta e="T17" id="Seg_278" s="T16">n</ta>
            <ta e="T18" id="Seg_279" s="T17">v</ta>
            <ta e="T19" id="Seg_280" s="T18">n</ta>
            <ta e="T20" id="Seg_281" s="T19">n</ta>
            <ta e="T21" id="Seg_282" s="T20">v</ta>
            <ta e="T22" id="Seg_283" s="T21">n</ta>
            <ta e="T23" id="Seg_284" s="T22">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_285" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_286" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_287" s="T3">0.3.h:S v:pred</ta>
            <ta e="T5" id="Seg_288" s="T4">np:O</ta>
            <ta e="T9" id="Seg_289" s="T8">0.3.h:S v:pred</ta>
            <ta e="T15" id="Seg_290" s="T14">0.3.h:S v:pred</ta>
            <ta e="T17" id="Seg_291" s="T16">np.h:S</ta>
            <ta e="T18" id="Seg_292" s="T17">v:pred</ta>
            <ta e="T21" id="Seg_293" s="T20">0.3.h:S v:pred</ta>
            <ta e="T22" id="Seg_294" s="T21">np.h:O</ta>
            <ta e="T23" id="Seg_295" s="T22">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_296" s="T0">adv:Time</ta>
            <ta e="T2" id="Seg_297" s="T1">np.h:Th</ta>
            <ta e="T4" id="Seg_298" s="T3">0.3.h:A</ta>
            <ta e="T7" id="Seg_299" s="T6">np:Poss</ta>
            <ta e="T8" id="Seg_300" s="T7">np:L</ta>
            <ta e="T9" id="Seg_301" s="T8">0.3.h:Th</ta>
            <ta e="T10" id="Seg_302" s="T9">adv:G</ta>
            <ta e="T13" id="Seg_303" s="T11">pp:G</ta>
            <ta e="T15" id="Seg_304" s="T14">0.3.h:E</ta>
            <ta e="T17" id="Seg_305" s="T16">np.h:A</ta>
            <ta e="T19" id="Seg_306" s="T18">np.h:Poss</ta>
            <ta e="T20" id="Seg_307" s="T19">np:Path</ta>
            <ta e="T21" id="Seg_308" s="T20">0.3.h:A</ta>
            <ta e="T22" id="Seg_309" s="T21">np.h:P</ta>
            <ta e="T23" id="Seg_310" s="T22">0.3.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T8" id="Seg_311" s="T0">Раньше остяки селились, рубили дома не на краю Кети.</ta>
            <ta e="T13" id="Seg_312" s="T8">А селились в стороне от Кети на озере.</ta>
            <ta e="T16" id="Seg_313" s="T13">Поэтому боялись разбойников.</ta>
            <ta e="T23" id="Seg_314" s="T16">Разбойники ездили в селькупские деревни, убивали людей, воровали.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T8" id="Seg_315" s="T0">Earlier the Selkup settled, built their houses not at the rim of Ket [river].</ta>
            <ta e="T13" id="Seg_316" s="T8">They settled apart from Ket, on a lake.</ta>
            <ta e="T16" id="Seg_317" s="T13">This is why they feared bandits.</ta>
            <ta e="T23" id="Seg_318" s="T16">Bandits used to go to Selkup villages, to kill people, to steal.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T8" id="Seg_319" s="T0">Früher siedelten und hackten(?) die Selkupen keine Häuser am Ufer des Ket.</ta>
            <ta e="T13" id="Seg_320" s="T8">Sie siedelten neben dem Ket an einem See.</ta>
            <ta e="T16" id="Seg_321" s="T13">Deshalb hatten sie Angst vor Räubern.</ta>
            <ta e="T23" id="Seg_322" s="T16">Die Räuber gingen durch die selkupischen Dörfer, töteten die Leute und stahlen.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T8" id="Seg_323" s="T0">раньше остяки селились рубили дома не на краю Кети</ta>
            <ta e="T13" id="Seg_324" s="T8">а селились в стороне от Кети в озере</ta>
            <ta e="T16" id="Seg_325" s="T13">поэтому боялись бандитов (разбойников)</ta>
            <ta e="T23" id="Seg_326" s="T16">разбойники ездили в остяцких юртах убивали людей воровали</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
