<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID299919EF-B1C3-90BF-4585-C92A0A6D7206">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\KPG_1969_Bread_nar\KPG_1969_Bread_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">41</ud-information>
            <ud-information attribute-name="# HIAT:w">31</ud-information>
            <ud-information attribute-name="# e">31</ud-information>
            <ud-information attribute-name="# HIAT:u">6</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KPG">
            <abbreviation>KPG</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KPG"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T31" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Nʼäːnʼim</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">meːtɔːmɨn</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">šoːqorqɨn</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">sölʼ</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">šoːqorqɨn</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Šoːqorlʼačʼa</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">čʼɔːtɛntal</ts>
                  <nts id="Seg_26" n="HIAT:ip">,</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">poːsa</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">pintärtɛntal</ts>
                  <nts id="Seg_33" n="HIAT:ip">,</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">šʼittɨ</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">pɔːrɨ</ts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_43" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">Sıːtʼitɨ</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">šittə</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">qattaltɨ</ts>
                  <nts id="Seg_52" n="HIAT:ip">,</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">ɔːmɨ</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">sıːtʼitɨ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">ponä</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">qattaltɨ</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_68" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">Nʼajlʼä</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">korpattɨ</ts>
                  <nts id="Seg_74" n="HIAT:ip">,</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">štälʼ</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">skavartalʼ</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">ürsä</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">natqɨltɨ</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_90" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">Štälʼlʼe</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">šoːqortä</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">tottätə</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_102" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_104" n="HIAT:w" s="T27">Pillä</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">puːla</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">konnä</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">iːqɨltɨ</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T31" id="Seg_116" n="sc" s="T0">
               <ts e="T1" id="Seg_118" n="e" s="T0">Nʼäːnʼim </ts>
               <ts e="T2" id="Seg_120" n="e" s="T1">meːtɔːmɨn </ts>
               <ts e="T3" id="Seg_122" n="e" s="T2">šoːqorqɨn </ts>
               <ts e="T4" id="Seg_124" n="e" s="T3">sölʼ </ts>
               <ts e="T5" id="Seg_126" n="e" s="T4">šoːqorqɨn. </ts>
               <ts e="T6" id="Seg_128" n="e" s="T5">Šoːqorlʼačʼa </ts>
               <ts e="T7" id="Seg_130" n="e" s="T6">čʼɔːtɛntal, </ts>
               <ts e="T8" id="Seg_132" n="e" s="T7">poːsa </ts>
               <ts e="T9" id="Seg_134" n="e" s="T8">pintärtɛntal, </ts>
               <ts e="T10" id="Seg_136" n="e" s="T9">šʼittɨ </ts>
               <ts e="T11" id="Seg_138" n="e" s="T10">pɔːrɨ. </ts>
               <ts e="T12" id="Seg_140" n="e" s="T11">Sıːtʼitɨ </ts>
               <ts e="T13" id="Seg_142" n="e" s="T12">šittə </ts>
               <ts e="T14" id="Seg_144" n="e" s="T13">qattaltɨ, </ts>
               <ts e="T15" id="Seg_146" n="e" s="T14">ɔːmɨ </ts>
               <ts e="T16" id="Seg_148" n="e" s="T15">sıːtʼitɨ </ts>
               <ts e="T17" id="Seg_150" n="e" s="T16">ponä </ts>
               <ts e="T18" id="Seg_152" n="e" s="T17">qattaltɨ. </ts>
               <ts e="T19" id="Seg_154" n="e" s="T18">Nʼajlʼä </ts>
               <ts e="T20" id="Seg_156" n="e" s="T19">korpattɨ, </ts>
               <ts e="T21" id="Seg_158" n="e" s="T20">štälʼ </ts>
               <ts e="T22" id="Seg_160" n="e" s="T21">skavartalʼ </ts>
               <ts e="T23" id="Seg_162" n="e" s="T22">ürsä </ts>
               <ts e="T24" id="Seg_164" n="e" s="T23">natqɨltɨ. </ts>
               <ts e="T25" id="Seg_166" n="e" s="T24">Štälʼlʼe </ts>
               <ts e="T26" id="Seg_168" n="e" s="T25">šoːqortä </ts>
               <ts e="T27" id="Seg_170" n="e" s="T26">tottätə. </ts>
               <ts e="T28" id="Seg_172" n="e" s="T27">Pillä </ts>
               <ts e="T29" id="Seg_174" n="e" s="T28">puːla </ts>
               <ts e="T30" id="Seg_176" n="e" s="T29">konnä </ts>
               <ts e="T31" id="Seg_178" n="e" s="T30">iːqɨltɨ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_179" s="T0">KPG_1969_Bread_nar.001 (001.001)</ta>
            <ta e="T11" id="Seg_180" s="T5">KPG_1969_Bread_nar.002 (001.002)</ta>
            <ta e="T18" id="Seg_181" s="T11">KPG_1969_Bread_nar.003 (001.003)</ta>
            <ta e="T24" id="Seg_182" s="T18">KPG_1969_Bread_nar.004 (001.004)</ta>
            <ta e="T27" id="Seg_183" s="T24">KPG_1969_Bread_nar.005 (001.005)</ta>
            <ta e="T31" id="Seg_184" s="T27">KPG_1969_Bread_nar.006 (001.006)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_185" s="T0">′нʼӓ̄н̄ʼим ′ме̄′то̄мын ′ш̄о̄ɣорkын съ̊lʼ ′шо̄ɣорkын.</ta>
            <ta e="T11" id="Seg_186" s="T5">′шоɣорlа′ча ′чо̄тентаl ′поса пинтӓртӓнтаl шʼитты ′па[о̄]ры.</ta>
            <ta e="T18" id="Seg_187" s="T11">′сидʼиты шʼиттъ kаттаlты, ′омы ′сид̂ʼиты ′пона ′kаттаlʼд̂ы.</ta>
            <ta e="T24" id="Seg_188" s="T18">′нʼайlӓ kорпаты, штӓl скавартаl ′ӱрсӓ ′натkыlтӓ.</ta>
            <ta e="T27" id="Seg_189" s="T24">′штӓllе шо̄ɣортӓ ′тоттӓдъ.</ta>
            <ta e="T31" id="Seg_190" s="T27">′пиllа ′пӯlа ′kоннӓ ′ӣkылʼты.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_191" s="T0">nʼäːnʼim meːtoːmɨn šoːqorqɨn səlʼ šoːqorqɨn.</ta>
            <ta e="T11" id="Seg_192" s="T5">šoqorlʼačʼa čʼoːtentalʼ posa pintärtäntalʼ šittɨ pa[oː]rɨ.</ta>
            <ta e="T18" id="Seg_193" s="T11">sidʼitɨ šittə qattalʼtɨ, omɨ sid̂ʼitɨ pona qattalʼd̂ɨ.</ta>
            <ta e="T24" id="Seg_194" s="T18">nʼajlʼä qorpatɨ, štälʼ skavartalʼ ürsä natqɨlʼtä.</ta>
            <ta e="T27" id="Seg_195" s="T24">štälʼlʼe šoːqortä tottädə.</ta>
            <ta e="T31" id="Seg_196" s="T27">pilʼlʼa puːlʼa qonnä iːqɨlʼtɨ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_197" s="T0">Nʼäːnʼim meːtɔːmɨn šoːqorqɨn sölʼ šoːqorqɨn. </ta>
            <ta e="T11" id="Seg_198" s="T5">Šoːqorlʼačʼa čʼɔːtɛntal, poːsa pintärtɛntal, šʼittɨ pɔːrɨ. </ta>
            <ta e="T18" id="Seg_199" s="T11">Sıːtʼitɨ šittə qattaltɨ, ɔːmɨ sıːtʼitɨ ponä qattaltɨ. </ta>
            <ta e="T24" id="Seg_200" s="T18">Nʼajlʼä korpattɨ, štälʼ skavartalʼ ürsä natqɨltɨ. </ta>
            <ta e="T27" id="Seg_201" s="T24">Štälʼlʼe šoːqortä tottätə. </ta>
            <ta e="T31" id="Seg_202" s="T27">Pillä puːla konnä iːqɨltɨ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_203" s="T0">nʼäːnʼ-i-m</ta>
            <ta e="T2" id="Seg_204" s="T1">meː-tɔː-mɨn</ta>
            <ta e="T3" id="Seg_205" s="T2">šoːqor-qɨn</ta>
            <ta e="T4" id="Seg_206" s="T3">sö-lʼ</ta>
            <ta e="T5" id="Seg_207" s="T4">šoːqor-qɨn</ta>
            <ta e="T6" id="Seg_208" s="T5">šoːqor-lʼa-čʼa</ta>
            <ta e="T7" id="Seg_209" s="T6">čʼɔːt-ɛnta-l</ta>
            <ta e="T8" id="Seg_210" s="T7">poː-sa</ta>
            <ta e="T9" id="Seg_211" s="T8">pin-tä-r-tɛnta-l</ta>
            <ta e="T10" id="Seg_212" s="T9">šittɨ</ta>
            <ta e="T11" id="Seg_213" s="T10">pɔːrɨ</ta>
            <ta e="T12" id="Seg_214" s="T11">sıːtʼi-tɨ</ta>
            <ta e="T13" id="Seg_215" s="T12">šittə</ta>
            <ta e="T14" id="Seg_216" s="T13">qatt-al-tɨ</ta>
            <ta e="T15" id="Seg_217" s="T14">ɔːmɨ</ta>
            <ta e="T16" id="Seg_218" s="T15">sıːtʼi-tɨ</ta>
            <ta e="T17" id="Seg_219" s="T16">ponä</ta>
            <ta e="T18" id="Seg_220" s="T17">qatt-al-tɨ</ta>
            <ta e="T19" id="Seg_221" s="T18">nʼaj-lʼä</ta>
            <ta e="T20" id="Seg_222" s="T19">korpat-tɨ</ta>
            <ta e="T21" id="Seg_223" s="T20">štälʼ</ta>
            <ta e="T22" id="Seg_224" s="T21">skavarta-lʼ</ta>
            <ta e="T23" id="Seg_225" s="T22">ür-sä</ta>
            <ta e="T24" id="Seg_226" s="T23">nat-qɨl-tɨ</ta>
            <ta e="T25" id="Seg_227" s="T24">štälʼlʼe</ta>
            <ta e="T26" id="Seg_228" s="T25">šoːqor-tä</ta>
            <ta e="T27" id="Seg_229" s="T26">tott-ätə</ta>
            <ta e="T28" id="Seg_230" s="T27">pi-l-lä</ta>
            <ta e="T29" id="Seg_231" s="T28">puːla</ta>
            <ta e="T30" id="Seg_232" s="T29">konnä</ta>
            <ta e="T31" id="Seg_233" s="T30">iː-qɨl-tɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_234" s="T0">nʼanʼ-ɨ-m</ta>
            <ta e="T2" id="Seg_235" s="T1">meː-tɨ-mɨt</ta>
            <ta e="T3" id="Seg_236" s="T2">šoːqɨr-qɨn</ta>
            <ta e="T4" id="Seg_237" s="T3">sö-lʼ</ta>
            <ta e="T5" id="Seg_238" s="T4">šoːqɨr-qɨn</ta>
            <ta e="T6" id="Seg_239" s="T5">šoːqɨr-lʼa-čʼa</ta>
            <ta e="T7" id="Seg_240" s="T6">čʼɔːtɨ-ɛntɨ-l</ta>
            <ta e="T8" id="Seg_241" s="T7">poː-sä</ta>
            <ta e="T9" id="Seg_242" s="T8">pin-tɨ-r-ɛntɨ-l</ta>
            <ta e="T10" id="Seg_243" s="T9">šittɨ</ta>
            <ta e="T11" id="Seg_244" s="T10">pɔːrɨ</ta>
            <ta e="T12" id="Seg_245" s="T11">sıːčʼɨ-tɨ</ta>
            <ta e="T13" id="Seg_246" s="T12">šittɨ</ta>
            <ta e="T14" id="Seg_247" s="T13">qättɨ-ätɔːl-ätɨ</ta>
            <ta e="T15" id="Seg_248" s="T14">ɔːmɨ</ta>
            <ta e="T16" id="Seg_249" s="T15">sıːčʼɨ-tɨ</ta>
            <ta e="T17" id="Seg_250" s="T16">ponä</ta>
            <ta e="T18" id="Seg_251" s="T17">qättɨ-ätɔːl-ätɨ</ta>
            <ta e="T19" id="Seg_252" s="T18">nʼanʼ-lʼa</ta>
            <ta e="T20" id="Seg_253" s="T19">korpɨt-ätɨ</ta>
            <ta e="T21" id="Seg_254" s="T20">šittälʼ</ta>
            <ta e="T22" id="Seg_255" s="T21">iskavarta-lʼ</ta>
            <ta e="T23" id="Seg_256" s="T22">ür-sä</ta>
            <ta e="T24" id="Seg_257" s="T23">nat-qɨl-ätɨ</ta>
            <ta e="T25" id="Seg_258" s="T24">šittälʼ</ta>
            <ta e="T26" id="Seg_259" s="T25">šoːqɨr-ntɨ</ta>
            <ta e="T27" id="Seg_260" s="T26">tottɨ-ätɨ</ta>
            <ta e="T28" id="Seg_261" s="T27">pi-lɨ-lä</ta>
            <ta e="T29" id="Seg_262" s="T28">puːlä</ta>
            <ta e="T30" id="Seg_263" s="T29">konnä</ta>
            <ta e="T31" id="Seg_264" s="T30">iː-qɨl-ätɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_265" s="T0">bread-EP-ACC</ta>
            <ta e="T2" id="Seg_266" s="T1">make-TR-1PL</ta>
            <ta e="T3" id="Seg_267" s="T2">oven-LOC</ta>
            <ta e="T4" id="Seg_268" s="T3">clay-ADJZ</ta>
            <ta e="T5" id="Seg_269" s="T4">oven-LOC</ta>
            <ta e="T6" id="Seg_270" s="T5">oven-DIM.[NOM]-%%</ta>
            <ta e="T7" id="Seg_271" s="T6">set.fire-FUT-2SG.O</ta>
            <ta e="T8" id="Seg_272" s="T7">firewood-INSTR</ta>
            <ta e="T9" id="Seg_273" s="T8">put-HAB-FRQ-FUT-2SG.O</ta>
            <ta e="T10" id="Seg_274" s="T9">two</ta>
            <ta e="T11" id="Seg_275" s="T10">time.[NOM]</ta>
            <ta e="T12" id="Seg_276" s="T11">coal.[NOM]-3SG</ta>
            <ta e="T13" id="Seg_277" s="T12">every.which.way</ta>
            <ta e="T14" id="Seg_278" s="T13">hit-MOM-IMP.2SG.O</ta>
            <ta e="T15" id="Seg_279" s="T14">other</ta>
            <ta e="T16" id="Seg_280" s="T15">coal.[NOM]-3SG</ta>
            <ta e="T17" id="Seg_281" s="T16">outwards</ta>
            <ta e="T18" id="Seg_282" s="T17">hit-MOM-IMP.2SG.O</ta>
            <ta e="T19" id="Seg_283" s="T18">bread-DIM.[NOM]</ta>
            <ta e="T20" id="Seg_284" s="T19">mix-IMP.2SG.O</ta>
            <ta e="T21" id="Seg_285" s="T20">then</ta>
            <ta e="T22" id="Seg_286" s="T21">frying.pan-ADJZ</ta>
            <ta e="T23" id="Seg_287" s="T22">fat-INSTR</ta>
            <ta e="T24" id="Seg_288" s="T23">anoint-MULO-IMP.2SG.O</ta>
            <ta e="T25" id="Seg_289" s="T24">then</ta>
            <ta e="T26" id="Seg_290" s="T25">oven-ILL</ta>
            <ta e="T27" id="Seg_291" s="T26">put-IMP.2SG.O</ta>
            <ta e="T28" id="Seg_292" s="T27">be.ready-RES-CVB</ta>
            <ta e="T29" id="Seg_293" s="T28">after</ta>
            <ta e="T30" id="Seg_294" s="T29">upwards</ta>
            <ta e="T31" id="Seg_295" s="T30">take-MULO-IMP.2SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_296" s="T0">хлеб-EP-ACC</ta>
            <ta e="T2" id="Seg_297" s="T1">сделать-TR-1PL</ta>
            <ta e="T3" id="Seg_298" s="T2">печь-LOC</ta>
            <ta e="T4" id="Seg_299" s="T3">глина-ADJZ</ta>
            <ta e="T5" id="Seg_300" s="T4">печь-LOC</ta>
            <ta e="T6" id="Seg_301" s="T5">печь-DIM.[NOM]-%%</ta>
            <ta e="T7" id="Seg_302" s="T6">зажечь-FUT-2SG.O</ta>
            <ta e="T8" id="Seg_303" s="T7">дрова-INSTR</ta>
            <ta e="T9" id="Seg_304" s="T8">положить-HAB-FRQ-FUT-2SG.O</ta>
            <ta e="T10" id="Seg_305" s="T9">два</ta>
            <ta e="T11" id="Seg_306" s="T10">раз.[NOM]</ta>
            <ta e="T12" id="Seg_307" s="T11">уголь.[NOM]-3SG</ta>
            <ta e="T13" id="Seg_308" s="T12">в.разные.стороны</ta>
            <ta e="T14" id="Seg_309" s="T13">ударить-MOM-IMP.2SG.O</ta>
            <ta e="T15" id="Seg_310" s="T14">остальной</ta>
            <ta e="T16" id="Seg_311" s="T15">уголь.[NOM]-3SG</ta>
            <ta e="T17" id="Seg_312" s="T16">наружу</ta>
            <ta e="T18" id="Seg_313" s="T17">ударить-MOM-IMP.2SG.O</ta>
            <ta e="T19" id="Seg_314" s="T18">хлеб-DIM.[NOM]</ta>
            <ta e="T20" id="Seg_315" s="T19">смешать-IMP.2SG.O</ta>
            <ta e="T21" id="Seg_316" s="T20">потом</ta>
            <ta e="T22" id="Seg_317" s="T21">сковорода-ADJZ</ta>
            <ta e="T23" id="Seg_318" s="T22">жир-INSTR</ta>
            <ta e="T24" id="Seg_319" s="T23">мазать-MULO-IMP.2SG.O</ta>
            <ta e="T25" id="Seg_320" s="T24">потом</ta>
            <ta e="T26" id="Seg_321" s="T25">печь-ILL</ta>
            <ta e="T27" id="Seg_322" s="T26">положить-IMP.2SG.O</ta>
            <ta e="T28" id="Seg_323" s="T27">поспеть-RES-CVB</ta>
            <ta e="T29" id="Seg_324" s="T28">после</ta>
            <ta e="T30" id="Seg_325" s="T29">вверх</ta>
            <ta e="T31" id="Seg_326" s="T30">взять-MULO-IMP.2SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_327" s="T0">n-n:(ins)-n:case3</ta>
            <ta e="T2" id="Seg_328" s="T1">v-v&gt;v-v:pn</ta>
            <ta e="T3" id="Seg_329" s="T2">n-n:case3</ta>
            <ta e="T4" id="Seg_330" s="T3">n-n&gt;adj</ta>
            <ta e="T5" id="Seg_331" s="T4">n-n:case3</ta>
            <ta e="T6" id="Seg_332" s="T5">n-n&gt;n-n:case3</ta>
            <ta e="T7" id="Seg_333" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_334" s="T7">n-n:case3</ta>
            <ta e="T9" id="Seg_335" s="T8">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_336" s="T9">num</ta>
            <ta e="T11" id="Seg_337" s="T10">n-n:case3</ta>
            <ta e="T12" id="Seg_338" s="T11">n-n:case1-n:poss</ta>
            <ta e="T13" id="Seg_339" s="T12">preverb</ta>
            <ta e="T14" id="Seg_340" s="T13">v-v&gt;v-v:mood-pn</ta>
            <ta e="T15" id="Seg_341" s="T14">adj</ta>
            <ta e="T16" id="Seg_342" s="T15">n-n:case1-n:poss</ta>
            <ta e="T17" id="Seg_343" s="T16">adv</ta>
            <ta e="T18" id="Seg_344" s="T17">v-v&gt;v-v:mood-pn</ta>
            <ta e="T19" id="Seg_345" s="T18">n-n&gt;n-n:case3</ta>
            <ta e="T20" id="Seg_346" s="T19">v-v:mood-pn</ta>
            <ta e="T21" id="Seg_347" s="T20">adv</ta>
            <ta e="T22" id="Seg_348" s="T21">n-n&gt;adj</ta>
            <ta e="T23" id="Seg_349" s="T22">n-n:case3</ta>
            <ta e="T24" id="Seg_350" s="T23">v-v&gt;v-v:mood-pn</ta>
            <ta e="T25" id="Seg_351" s="T24">adv</ta>
            <ta e="T26" id="Seg_352" s="T25">n-n:case3</ta>
            <ta e="T27" id="Seg_353" s="T26">v-v:mood-pn</ta>
            <ta e="T28" id="Seg_354" s="T27">v-v&gt;v-v&gt;adv</ta>
            <ta e="T29" id="Seg_355" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_356" s="T29">adv</ta>
            <ta e="T31" id="Seg_357" s="T30">v-v&gt;v-v:mood-pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_358" s="T0">n</ta>
            <ta e="T2" id="Seg_359" s="T1">v</ta>
            <ta e="T3" id="Seg_360" s="T2">n</ta>
            <ta e="T4" id="Seg_361" s="T3">adj</ta>
            <ta e="T5" id="Seg_362" s="T4">n</ta>
            <ta e="T6" id="Seg_363" s="T5">n</ta>
            <ta e="T7" id="Seg_364" s="T6">v</ta>
            <ta e="T8" id="Seg_365" s="T7">n</ta>
            <ta e="T9" id="Seg_366" s="T8">v</ta>
            <ta e="T10" id="Seg_367" s="T9">num</ta>
            <ta e="T11" id="Seg_368" s="T10">n</ta>
            <ta e="T12" id="Seg_369" s="T11">n</ta>
            <ta e="T13" id="Seg_370" s="T12">preverb</ta>
            <ta e="T14" id="Seg_371" s="T13">v</ta>
            <ta e="T15" id="Seg_372" s="T14">adj</ta>
            <ta e="T16" id="Seg_373" s="T15">n</ta>
            <ta e="T17" id="Seg_374" s="T16">adv</ta>
            <ta e="T18" id="Seg_375" s="T17">v</ta>
            <ta e="T19" id="Seg_376" s="T18">n</ta>
            <ta e="T20" id="Seg_377" s="T19">v</ta>
            <ta e="T21" id="Seg_378" s="T20">adv</ta>
            <ta e="T22" id="Seg_379" s="T21">n</ta>
            <ta e="T23" id="Seg_380" s="T22">n</ta>
            <ta e="T24" id="Seg_381" s="T23">v</ta>
            <ta e="T25" id="Seg_382" s="T24">adv</ta>
            <ta e="T26" id="Seg_383" s="T25">n</ta>
            <ta e="T27" id="Seg_384" s="T26">v</ta>
            <ta e="T28" id="Seg_385" s="T27">adv</ta>
            <ta e="T29" id="Seg_386" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_387" s="T29">adv</ta>
            <ta e="T31" id="Seg_388" s="T30">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_389" s="T0">np:P</ta>
            <ta e="T2" id="Seg_390" s="T1">0.1.h:A</ta>
            <ta e="T3" id="Seg_391" s="T2">np:L</ta>
            <ta e="T5" id="Seg_392" s="T4">np:L</ta>
            <ta e="T6" id="Seg_393" s="T5">np:P</ta>
            <ta e="T7" id="Seg_394" s="T6">0.2.h:A</ta>
            <ta e="T8" id="Seg_395" s="T7">np:Th</ta>
            <ta e="T9" id="Seg_396" s="T8">0.2.h:A </ta>
            <ta e="T12" id="Seg_397" s="T11">np:P</ta>
            <ta e="T13" id="Seg_398" s="T12">adv:G</ta>
            <ta e="T14" id="Seg_399" s="T13">0.2.h:A</ta>
            <ta e="T16" id="Seg_400" s="T15">np:P</ta>
            <ta e="T17" id="Seg_401" s="T16">adv:G</ta>
            <ta e="T18" id="Seg_402" s="T17">0.2.h:A</ta>
            <ta e="T19" id="Seg_403" s="T18">np:P</ta>
            <ta e="T20" id="Seg_404" s="T19">0.2.h:A</ta>
            <ta e="T21" id="Seg_405" s="T20">adv:Time</ta>
            <ta e="T22" id="Seg_406" s="T21">np:P</ta>
            <ta e="T23" id="Seg_407" s="T22">np:Ins</ta>
            <ta e="T24" id="Seg_408" s="T23">0.2.h:A</ta>
            <ta e="T25" id="Seg_409" s="T24">adv:Time</ta>
            <ta e="T26" id="Seg_410" s="T25">np:G</ta>
            <ta e="T27" id="Seg_411" s="T26">0.2.h:A 0.3:Th</ta>
            <ta e="T31" id="Seg_412" s="T30">0.2.h:A 0.3:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_413" s="T0">np:O</ta>
            <ta e="T2" id="Seg_414" s="T1">0.1.h:S v:pred</ta>
            <ta e="T6" id="Seg_415" s="T5">np:O</ta>
            <ta e="T7" id="Seg_416" s="T6">0.2.h:S v:pred</ta>
            <ta e="T9" id="Seg_417" s="T8">0.2.h:S 0.3:O v:pred</ta>
            <ta e="T12" id="Seg_418" s="T11">np:O</ta>
            <ta e="T14" id="Seg_419" s="T13">0.2.h:S v:pred</ta>
            <ta e="T16" id="Seg_420" s="T15">np:O</ta>
            <ta e="T18" id="Seg_421" s="T17">0.2.h:S v:pred</ta>
            <ta e="T19" id="Seg_422" s="T18">np:O</ta>
            <ta e="T20" id="Seg_423" s="T19">0.2.h:S v:pred</ta>
            <ta e="T22" id="Seg_424" s="T21">np:O</ta>
            <ta e="T24" id="Seg_425" s="T23">0.2.h:S v:pred</ta>
            <ta e="T27" id="Seg_426" s="T26">0.2.h:S 0.3:O v:pred</ta>
            <ta e="T29" id="Seg_427" s="T27">s:temp</ta>
            <ta e="T31" id="Seg_428" s="T30">0.2.h:S 0.3:O v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_429" s="T0">new</ta>
            <ta e="T2" id="Seg_430" s="T1">0.new</ta>
            <ta e="T3" id="Seg_431" s="T2">accs-sit</ta>
            <ta e="T5" id="Seg_432" s="T4">accs-sit</ta>
            <ta e="T6" id="Seg_433" s="T5">giv-active-Q</ta>
            <ta e="T7" id="Seg_434" s="T6">0.new-Q</ta>
            <ta e="T8" id="Seg_435" s="T7">accs-sit-Q</ta>
            <ta e="T9" id="Seg_436" s="T8">0.giv-active-Q</ta>
            <ta e="T12" id="Seg_437" s="T11">accs-sit-Q</ta>
            <ta e="T14" id="Seg_438" s="T13">0.giv-active-Q</ta>
            <ta e="T16" id="Seg_439" s="T15">giv-active-Q</ta>
            <ta e="T18" id="Seg_440" s="T17">0.giv-active-Q</ta>
            <ta e="T19" id="Seg_441" s="T18">giv-inactive-Q</ta>
            <ta e="T20" id="Seg_442" s="T19">0.giv-active-Q</ta>
            <ta e="T22" id="Seg_443" s="T21">new-Q</ta>
            <ta e="T23" id="Seg_444" s="T22">new-Q</ta>
            <ta e="T24" id="Seg_445" s="T23">0.giv-active-Q</ta>
            <ta e="T26" id="Seg_446" s="T25">giv-inactive-Q</ta>
            <ta e="T27" id="Seg_447" s="T26">0.giv-active-Q</ta>
            <ta e="T31" id="Seg_448" s="T30">0.giv-active-Q.0</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T22" id="Seg_449" s="T21">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_450" s="T0">Хлеб делаем в печи, в глиняной печке.</ta>
            <ta e="T11" id="Seg_451" s="T5">Печку затопишь, дрова подкладывать будешь, два раза.</ta>
            <ta e="T18" id="Seg_452" s="T11">Угли в стороны разгреби, остальные угли на улицу выгреби.</ta>
            <ta e="T24" id="Seg_453" s="T18">Хлеб замешай, сковородку жиром намажь.</ta>
            <ta e="T27" id="Seg_454" s="T24">Затем в печку поставь.</ta>
            <ta e="T31" id="Seg_455" s="T27">После того как испечется, вытащи.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_456" s="T0">We make bread in oven, in a clay oven.</ta>
            <ta e="T11" id="Seg_457" s="T5">When you heat the oven, you will put firewood in there, twice.</ta>
            <ta e="T18" id="Seg_458" s="T11">Rake aside the coal, rake out the rest of the coal.</ta>
            <ta e="T24" id="Seg_459" s="T18">Knead the dough, and then grease a frying pan.</ta>
            <ta e="T27" id="Seg_460" s="T24">Then put it into the oven.</ta>
            <ta e="T31" id="Seg_461" s="T27">When it is ready, take it out.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_462" s="T0">Wir machen Brot im Ofen, im Lehmofen.</ta>
            <ta e="T11" id="Seg_463" s="T5">Du heizt den Ofen an und legst zweimal Holz hinein.</ta>
            <ta e="T18" id="Seg_464" s="T11">Kratz die Kohle raus, hol den Rest der Kohle raus.</ta>
            <ta e="T24" id="Seg_465" s="T18">Knete den Teig und fette dann die Pfanne ein.</ta>
            <ta e="T27" id="Seg_466" s="T24">Dann schieb es in den Ofen.</ta>
            <ta e="T31" id="Seg_467" s="T27">Wenn es ausgebacken ist, nimm es raus.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_468" s="T0">хлеб делаем в печи, в селькупской печке.</ta>
            <ta e="T11" id="Seg_469" s="T5">печку затопишь, дров подкладывать будешь, два раза.</ta>
            <ta e="T18" id="Seg_470" s="T11">угли в cтороны разгреби. остальные уuли на улицу выгреби.</ta>
            <ta e="T24" id="Seg_471" s="T18">хлеб замешивают, сковородку жиром намажь.</ta>
            <ta e="T27" id="Seg_472" s="T24">затем в печку посади.</ta>
            <ta e="T31" id="Seg_473" s="T27">выпечется [после того как выпечется], вытаскивают.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T11" id="Seg_474" s="T5">[BrM]: Unknown suffix -čʼa.</ta>
            <ta e="T24" id="Seg_475" s="T18">[BrM]: Unclear syntactic function of "skavarta-lʼ" in the clause "skavartalʼ ürsä natqɨltɨ".</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
