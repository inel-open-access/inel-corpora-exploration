<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID4E94B9C1-0446-2773-D727-8570D02B6A0F">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\KPG_1969_HowWeMakeBricks_nar\KPG_1969_HowWeMakeBricks_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">41</ud-information>
            <ud-information attribute-name="# HIAT:w">33</ud-information>
            <ud-information attribute-name="# e">33</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KPG">
            <abbreviation>KPG</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KPG"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T33" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">meːtam</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">kirpičʼim</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Poːsɨlʼ</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">pɔːr</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">təttɨt</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">qopɨm</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">ınn</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">iːtam</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">čʼoquntoːqa</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_38" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">Čʼulla</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">puːla</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">torqantə</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">toqultɛntam</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_53" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">Toqulla</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">puːla</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">čʼelʼčʼalʼtɛntam</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_65" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">Štälä</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">qattɛntam</ts>
                  <nts id="Seg_71" n="HIAT:ip">,</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">ponɨ</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">čʼeːli</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">ınna</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">ɔːmtɨtɔːltɛntam</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_87" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">Šitäla</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">ponɨ</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">tuːqoltɛntam</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">täːkɨquntaqa</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_102" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_104" n="HIAT:w" s="T27">Meːla</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">tuːrtuqulla</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">puːla</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">antɨsa</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">mestakɨntɨ</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">tuːqoltɛntɨŋɨtɨ</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T33" id="Seg_122" n="sc" s="T0">
               <ts e="T1" id="Seg_124" n="e" s="T0">Man </ts>
               <ts e="T2" id="Seg_126" n="e" s="T1">meːtam </ts>
               <ts e="T3" id="Seg_128" n="e" s="T2">kirpičʼim. </ts>
               <ts e="T4" id="Seg_130" n="e" s="T3">Poːsɨlʼ </ts>
               <ts e="T5" id="Seg_132" n="e" s="T4">pɔːr </ts>
               <ts e="T6" id="Seg_134" n="e" s="T5">təttɨt </ts>
               <ts e="T7" id="Seg_136" n="e" s="T6">qopɨm </ts>
               <ts e="T8" id="Seg_138" n="e" s="T7">ınn </ts>
               <ts e="T9" id="Seg_140" n="e" s="T8">iːtam </ts>
               <ts e="T10" id="Seg_142" n="e" s="T9">čʼoquntoːqa. </ts>
               <ts e="T11" id="Seg_144" n="e" s="T10">Čʼulla </ts>
               <ts e="T12" id="Seg_146" n="e" s="T11">puːla </ts>
               <ts e="T13" id="Seg_148" n="e" s="T12">torqantə </ts>
               <ts e="T14" id="Seg_150" n="e" s="T13">toqultɛntam. </ts>
               <ts e="T15" id="Seg_152" n="e" s="T14">Toqulla </ts>
               <ts e="T16" id="Seg_154" n="e" s="T15">puːla </ts>
               <ts e="T17" id="Seg_156" n="e" s="T16">čʼelʼčʼalʼtɛntam. </ts>
               <ts e="T18" id="Seg_158" n="e" s="T17">Štälä </ts>
               <ts e="T19" id="Seg_160" n="e" s="T18">qattɛntam, </ts>
               <ts e="T20" id="Seg_162" n="e" s="T19">ponɨ </ts>
               <ts e="T21" id="Seg_164" n="e" s="T20">čʼeːli </ts>
               <ts e="T22" id="Seg_166" n="e" s="T21">ınna </ts>
               <ts e="T23" id="Seg_168" n="e" s="T22">ɔːmtɨtɔːltɛntam. </ts>
               <ts e="T24" id="Seg_170" n="e" s="T23">Šitäla </ts>
               <ts e="T25" id="Seg_172" n="e" s="T24">ponɨ </ts>
               <ts e="T26" id="Seg_174" n="e" s="T25">tuːqoltɛntam </ts>
               <ts e="T27" id="Seg_176" n="e" s="T26">täːkɨquntaqa. </ts>
               <ts e="T28" id="Seg_178" n="e" s="T27">Meːla </ts>
               <ts e="T29" id="Seg_180" n="e" s="T28">tuːrtuqulla </ts>
               <ts e="T30" id="Seg_182" n="e" s="T29">puːla </ts>
               <ts e="T31" id="Seg_184" n="e" s="T30">antɨsa </ts>
               <ts e="T32" id="Seg_186" n="e" s="T31">mestakɨntɨ </ts>
               <ts e="T33" id="Seg_188" n="e" s="T32">tuːqoltɛntɨŋɨtɨ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_189" s="T0">KPG_1969_HowWeMakeBricks_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_190" s="T3">KPG_1969_HowWeMakeBricks_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_191" s="T10">KPG_1969_HowWeMakeBricks_nar.003 (001.003)</ta>
            <ta e="T17" id="Seg_192" s="T14">KPG_1969_HowWeMakeBricks_nar.004 (001.004)</ta>
            <ta e="T23" id="Seg_193" s="T17">KPG_1969_HowWeMakeBricks_nar.005 (001.005)</ta>
            <ta e="T27" id="Seg_194" s="T23">KPG_1969_HowWeMakeBricks_nar.006 (001.006)</ta>
            <ta e="T33" id="Seg_195" s="T27">KPG_1969_HowWeMakeBricks_nar.007 (001.007)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_196" s="T0">ман ме̄там кир′пичим.</ta>
            <ta e="T10" id="Seg_197" s="T3">′по̄сылʼпо̄р тъ̊ттыт ′kопым и′ннӣтам ‵чоkун′тоkа.</ta>
            <ta e="T14" id="Seg_198" s="T10">′чуllа пӯlа ′торkантъ тоɣуlтӓнтам.</ta>
            <ta e="T17" id="Seg_199" s="T14">тоɣуlа ′пӯlа ′челʼчалʼтӓнтам.</ta>
            <ta e="T23" id="Seg_200" s="T17">′штӓlӓ ′kаттӓнтам, по̄ны че̄lи и′нна ′омтыто̄lтентам.</ta>
            <ta e="T27" id="Seg_201" s="T23">шʼи′тӓlа по̄ны тӯɣоlтӓнтам ′тӓ̄kыкун‵таkа.</ta>
            <ta e="T33" id="Seg_202" s="T27">ме̄lа ′туртукуlа ′пӯlа ′антыса ′местаkынты тӯɣоlтӓнтыɣыты.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_203" s="T0">man meːtam kirpičim.</ta>
            <ta e="T10" id="Seg_204" s="T3">poːsɨlʼpoːr təttɨt qopɨm inniːtam čoquntoqa.</ta>
            <ta e="T14" id="Seg_205" s="T10">čulʼlʼa puːlʼa torqantə toqulʼtäntam.</ta>
            <ta e="T17" id="Seg_206" s="T14">toqulʼa puːlʼa čelʼčalʼtäntam.</ta>
            <ta e="T23" id="Seg_207" s="T17">štälʼä qattäntam, poːnɨ čeːlʼi inna omtɨtoːlʼtentam.</ta>
            <ta e="T27" id="Seg_208" s="T23">šitälʼa poːnɨ tuːqolʼtäntam täːqɨkuntaqa.</ta>
            <ta e="T33" id="Seg_209" s="T27">meːlʼa turtukulʼa puːlʼa antɨsa mestaqɨntɨ tuːqolʼtäntɨqɨtɨ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_210" s="T0">Man meːtam kirpičʼim. </ta>
            <ta e="T10" id="Seg_211" s="T3">Poːsɨlʼ pɔːr təttɨt qopɨm ınn iːtam čʼoquntoːqa. </ta>
            <ta e="T14" id="Seg_212" s="T10">Čʼulla puːla torqantə toqultɛntam. </ta>
            <ta e="T17" id="Seg_213" s="T14">Toqulla puːla čʼelʼčʼalʼtɛntam. </ta>
            <ta e="T23" id="Seg_214" s="T17">Štälä qattɛntam, ponɨ čʼeːli ınna ɔːmtɨtɔːltɛntam. </ta>
            <ta e="T27" id="Seg_215" s="T23">Šitäla ponɨ tuːqoltɛntam täːkɨquntaqa. </ta>
            <ta e="T33" id="Seg_216" s="T27">Meːla tuːrtuqulla puːla antɨsa mestakɨntɨ tuːqoltɛntɨŋɨtɨ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_217" s="T0">man</ta>
            <ta e="T2" id="Seg_218" s="T1">meː-ta-m</ta>
            <ta e="T3" id="Seg_219" s="T2">kirpičʼ-i-m</ta>
            <ta e="T4" id="Seg_220" s="T3">poːsɨ-lʼ</ta>
            <ta e="T5" id="Seg_221" s="T4">pɔːr</ta>
            <ta e="T6" id="Seg_222" s="T5">təttɨ-t</ta>
            <ta e="T7" id="Seg_223" s="T6">qopɨ-m</ta>
            <ta e="T8" id="Seg_224" s="T7">ınn</ta>
            <ta e="T9" id="Seg_225" s="T8">iː-ta-m</ta>
            <ta e="T10" id="Seg_226" s="T9">čʼo-quntoːqa</ta>
            <ta e="T11" id="Seg_227" s="T10">čʼu-lla</ta>
            <ta e="T12" id="Seg_228" s="T11">puːla</ta>
            <ta e="T13" id="Seg_229" s="T12">torqa-ntə</ta>
            <ta e="T14" id="Seg_230" s="T13">to-qul-tɛnta-m</ta>
            <ta e="T15" id="Seg_231" s="T14">to-qul-la</ta>
            <ta e="T16" id="Seg_232" s="T15">puːla</ta>
            <ta e="T17" id="Seg_233" s="T16">čʼelʼčʼ-alʼ-tɛnta-m</ta>
            <ta e="T18" id="Seg_234" s="T17">štälä</ta>
            <ta e="T19" id="Seg_235" s="T18">qatt-ɛnta-m</ta>
            <ta e="T20" id="Seg_236" s="T19">ponɨ</ta>
            <ta e="T21" id="Seg_237" s="T20">čʼeːli</ta>
            <ta e="T22" id="Seg_238" s="T21">ınna</ta>
            <ta e="T23" id="Seg_239" s="T22">ɔːmtɨ-t-ɔːl-tɛnta-m</ta>
            <ta e="T24" id="Seg_240" s="T23">šitäla</ta>
            <ta e="T25" id="Seg_241" s="T24">ponɨ</ta>
            <ta e="T26" id="Seg_242" s="T25">tuː-qol-tɛnta-m</ta>
            <ta e="T27" id="Seg_243" s="T26">täːkɨ-quntaqa</ta>
            <ta e="T28" id="Seg_244" s="T27">meː-la</ta>
            <ta e="T29" id="Seg_245" s="T28">tuːr-tu-qul-la</ta>
            <ta e="T30" id="Seg_246" s="T29">puːla</ta>
            <ta e="T31" id="Seg_247" s="T30">antɨ-sa</ta>
            <ta e="T32" id="Seg_248" s="T31">mesta-kɨntɨ</ta>
            <ta e="T33" id="Seg_249" s="T32">tuː-qol-tɛntɨŋɨ-tɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_250" s="T0">man</ta>
            <ta e="T2" id="Seg_251" s="T1">meː-tɨ-m</ta>
            <ta e="T3" id="Seg_252" s="T2">kirpičʼ-ɨ-m</ta>
            <ta e="T4" id="Seg_253" s="T3">poːsɨ-lʼ</ta>
            <ta e="T5" id="Seg_254" s="T4">pɔːrɨ</ta>
            <ta e="T6" id="Seg_255" s="T5">təttɨ-n</ta>
            <ta e="T7" id="Seg_256" s="T6">qopɨ-m</ta>
            <ta e="T8" id="Seg_257" s="T7">ınnä</ta>
            <ta e="T9" id="Seg_258" s="T8">iː-tɨ-m</ta>
            <ta e="T10" id="Seg_259" s="T9">čʼu-qɨntoːqo</ta>
            <ta e="T11" id="Seg_260" s="T10">čʼu-lä</ta>
            <ta e="T12" id="Seg_261" s="T11">puːlä</ta>
            <ta e="T13" id="Seg_262" s="T12">torqɨ-ntɨ</ta>
            <ta e="T14" id="Seg_263" s="T13">tuː-qɨl-ɛntɨ-m</ta>
            <ta e="T15" id="Seg_264" s="T14">tuː-qɨl-lä</ta>
            <ta e="T16" id="Seg_265" s="T15">puːlä</ta>
            <ta e="T17" id="Seg_266" s="T16">čʼelʼčʼɨ-alʼ-ɛntɨ-m</ta>
            <ta e="T18" id="Seg_267" s="T17">šittälʼ</ta>
            <ta e="T19" id="Seg_268" s="T18">qättɨ-ɛntɨ-m</ta>
            <ta e="T20" id="Seg_269" s="T19">ponä</ta>
            <ta e="T21" id="Seg_270" s="T20">čʼeːlɨ</ta>
            <ta e="T22" id="Seg_271" s="T21">ınnä</ta>
            <ta e="T23" id="Seg_272" s="T22">ɔːmtɨ-tɨ-qɨl-ɛntɨ-m</ta>
            <ta e="T24" id="Seg_273" s="T23">šittälʼ</ta>
            <ta e="T25" id="Seg_274" s="T24">ponä</ta>
            <ta e="T26" id="Seg_275" s="T25">tuː-qɨl-ɛntɨ-m</ta>
            <ta e="T27" id="Seg_276" s="T26">təːkɨ-qɨntoːqo</ta>
            <ta e="T28" id="Seg_277" s="T27">meː-lä</ta>
            <ta e="T29" id="Seg_278" s="T28">tuːrıː-tɨ-qɨl-lä</ta>
            <ta e="T30" id="Seg_279" s="T29">puːlä</ta>
            <ta e="T31" id="Seg_280" s="T30">antɨ-sä</ta>
            <ta e="T32" id="Seg_281" s="T31">mesta-nkinı</ta>
            <ta e="T33" id="Seg_282" s="T32">tuː-qɨl-ɛntɨ-tɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_283" s="T0">I.NOM</ta>
            <ta e="T2" id="Seg_284" s="T1">make-TR-1SG.O</ta>
            <ta e="T3" id="Seg_285" s="T2">brick-EP-ACC</ta>
            <ta e="T4" id="Seg_286" s="T3">most-ADJZ</ta>
            <ta e="T5" id="Seg_287" s="T4">time.[NOM]</ta>
            <ta e="T6" id="Seg_288" s="T5">earth-GEN</ta>
            <ta e="T7" id="Seg_289" s="T6">skin-ACC</ta>
            <ta e="T8" id="Seg_290" s="T7">up</ta>
            <ta e="T9" id="Seg_291" s="T8">take-TR-1SG.O</ta>
            <ta e="T10" id="Seg_292" s="T9">melt-SUP.2/3SG</ta>
            <ta e="T11" id="Seg_293" s="T10">melt-CVB</ta>
            <ta e="T12" id="Seg_294" s="T11">after</ta>
            <ta e="T13" id="Seg_295" s="T12">container-ILL</ta>
            <ta e="T14" id="Seg_296" s="T13">carry-MULO-FUT-1SG.O</ta>
            <ta e="T15" id="Seg_297" s="T14">carry-MULO-CVB</ta>
            <ta e="T16" id="Seg_298" s="T15">after</ta>
            <ta e="T17" id="Seg_299" s="T16">stamp-INCH-FUT-1SG.O</ta>
            <ta e="T18" id="Seg_300" s="T17">then</ta>
            <ta e="T19" id="Seg_301" s="T18">hit-FUT-1SG.O</ta>
            <ta e="T20" id="Seg_302" s="T19">outwards</ta>
            <ta e="T21" id="Seg_303" s="T20">day.[NOM]</ta>
            <ta e="T22" id="Seg_304" s="T21">up</ta>
            <ta e="T23" id="Seg_305" s="T22">stand-TR-MULO-FUT-1SG.O</ta>
            <ta e="T24" id="Seg_306" s="T23">then</ta>
            <ta e="T25" id="Seg_307" s="T24">outwards</ta>
            <ta e="T26" id="Seg_308" s="T25">carry-MULO-FUT-1SG.O</ta>
            <ta e="T27" id="Seg_309" s="T26">dry-SUP.2/3SG</ta>
            <ta e="T28" id="Seg_310" s="T27">make-CVB</ta>
            <ta e="T29" id="Seg_311" s="T28">be.over-TR-MULO-CVB</ta>
            <ta e="T30" id="Seg_312" s="T29">after</ta>
            <ta e="T31" id="Seg_313" s="T30">boat-INSTR</ta>
            <ta e="T32" id="Seg_314" s="T31">place-ALL</ta>
            <ta e="T33" id="Seg_315" s="T32">carry-MULO-FUT-3SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_316" s="T0">я.NOM</ta>
            <ta e="T2" id="Seg_317" s="T1">сделать-TR-1SG.O</ta>
            <ta e="T3" id="Seg_318" s="T2">кирпич-EP-ACC</ta>
            <ta e="T4" id="Seg_319" s="T3">самый-ADJZ</ta>
            <ta e="T5" id="Seg_320" s="T4">раз.[NOM]</ta>
            <ta e="T6" id="Seg_321" s="T5">земля-GEN</ta>
            <ta e="T7" id="Seg_322" s="T6">шкура-ACC</ta>
            <ta e="T8" id="Seg_323" s="T7">вверх</ta>
            <ta e="T9" id="Seg_324" s="T8">взять-TR-1SG.O</ta>
            <ta e="T10" id="Seg_325" s="T9">таять-SUP.2/3SG</ta>
            <ta e="T11" id="Seg_326" s="T10">таять-CVB</ta>
            <ta e="T12" id="Seg_327" s="T11">после</ta>
            <ta e="T13" id="Seg_328" s="T12">ёмкость-ILL</ta>
            <ta e="T14" id="Seg_329" s="T13">таскать-MULO-FUT-1SG.O</ta>
            <ta e="T15" id="Seg_330" s="T14">таскать-MULO-CVB</ta>
            <ta e="T16" id="Seg_331" s="T15">после</ta>
            <ta e="T17" id="Seg_332" s="T16">топтать-INCH-FUT-1SG.O</ta>
            <ta e="T18" id="Seg_333" s="T17">потом</ta>
            <ta e="T19" id="Seg_334" s="T18">ударить-FUT-1SG.O</ta>
            <ta e="T20" id="Seg_335" s="T19">наружу</ta>
            <ta e="T21" id="Seg_336" s="T20">день.[NOM]</ta>
            <ta e="T22" id="Seg_337" s="T21">вверх</ta>
            <ta e="T23" id="Seg_338" s="T22">стоять-TR-MULO-FUT-1SG.O</ta>
            <ta e="T24" id="Seg_339" s="T23">потом</ta>
            <ta e="T25" id="Seg_340" s="T24">наружу</ta>
            <ta e="T26" id="Seg_341" s="T25">таскать-MULO-FUT-1SG.O</ta>
            <ta e="T27" id="Seg_342" s="T26">сохнуть-SUP.2/3SG</ta>
            <ta e="T28" id="Seg_343" s="T27">сделать-CVB</ta>
            <ta e="T29" id="Seg_344" s="T28">закончиться-TR-MULO-CVB</ta>
            <ta e="T30" id="Seg_345" s="T29">после</ta>
            <ta e="T31" id="Seg_346" s="T30">ветка-INSTR</ta>
            <ta e="T32" id="Seg_347" s="T31">место-ALL</ta>
            <ta e="T33" id="Seg_348" s="T32">таскать-MULO-FUT-3SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_349" s="T0">pers</ta>
            <ta e="T2" id="Seg_350" s="T1">v-v&gt;v-v:pn</ta>
            <ta e="T3" id="Seg_351" s="T2">n-n:(ins)-n:case3</ta>
            <ta e="T4" id="Seg_352" s="T3">adv-adv&gt;adj</ta>
            <ta e="T5" id="Seg_353" s="T4">n-n:case3</ta>
            <ta e="T6" id="Seg_354" s="T5">n-n:case3</ta>
            <ta e="T7" id="Seg_355" s="T6">n-n:case3</ta>
            <ta e="T8" id="Seg_356" s="T7">preverb</ta>
            <ta e="T9" id="Seg_357" s="T8">v-v&gt;v-v:pn</ta>
            <ta e="T10" id="Seg_358" s="T9">v-v:inf-poss</ta>
            <ta e="T11" id="Seg_359" s="T10">v-v&gt;adv</ta>
            <ta e="T12" id="Seg_360" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_361" s="T12">n-n:case3</ta>
            <ta e="T14" id="Seg_362" s="T13">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_363" s="T14">v-v&gt;v-v&gt;adv</ta>
            <ta e="T16" id="Seg_364" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_365" s="T16">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_366" s="T17">adv</ta>
            <ta e="T19" id="Seg_367" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_368" s="T19">adv</ta>
            <ta e="T21" id="Seg_369" s="T20">n-n:case3</ta>
            <ta e="T22" id="Seg_370" s="T21">preverb</ta>
            <ta e="T23" id="Seg_371" s="T22">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_372" s="T23">adv</ta>
            <ta e="T25" id="Seg_373" s="T24">adv</ta>
            <ta e="T26" id="Seg_374" s="T25">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_375" s="T26">v-v:inf-poss</ta>
            <ta e="T28" id="Seg_376" s="T27">v-v&gt;adv</ta>
            <ta e="T29" id="Seg_377" s="T28">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T30" id="Seg_378" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_379" s="T30">n-n:case3</ta>
            <ta e="T32" id="Seg_380" s="T31">n-n:case3</ta>
            <ta e="T33" id="Seg_381" s="T32">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_382" s="T0">pers</ta>
            <ta e="T2" id="Seg_383" s="T1">v</ta>
            <ta e="T3" id="Seg_384" s="T2">n</ta>
            <ta e="T4" id="Seg_385" s="T3">adj</ta>
            <ta e="T5" id="Seg_386" s="T4">n</ta>
            <ta e="T6" id="Seg_387" s="T5">n</ta>
            <ta e="T7" id="Seg_388" s="T6">n</ta>
            <ta e="T8" id="Seg_389" s="T7">preverb</ta>
            <ta e="T9" id="Seg_390" s="T8">v</ta>
            <ta e="T10" id="Seg_391" s="T9">v</ta>
            <ta e="T11" id="Seg_392" s="T10">adv</ta>
            <ta e="T12" id="Seg_393" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_394" s="T12">n</ta>
            <ta e="T14" id="Seg_395" s="T13">v</ta>
            <ta e="T15" id="Seg_396" s="T14">adv</ta>
            <ta e="T16" id="Seg_397" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_398" s="T16">v</ta>
            <ta e="T18" id="Seg_399" s="T17">adv</ta>
            <ta e="T19" id="Seg_400" s="T18">v</ta>
            <ta e="T20" id="Seg_401" s="T19">adv</ta>
            <ta e="T21" id="Seg_402" s="T20">n</ta>
            <ta e="T22" id="Seg_403" s="T21">preverb</ta>
            <ta e="T23" id="Seg_404" s="T22">v</ta>
            <ta e="T24" id="Seg_405" s="T23">adv</ta>
            <ta e="T25" id="Seg_406" s="T24">adv</ta>
            <ta e="T26" id="Seg_407" s="T25">v</ta>
            <ta e="T27" id="Seg_408" s="T26">v</ta>
            <ta e="T28" id="Seg_409" s="T27">adv</ta>
            <ta e="T29" id="Seg_410" s="T28">adv</ta>
            <ta e="T30" id="Seg_411" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_412" s="T30">n</ta>
            <ta e="T32" id="Seg_413" s="T31">n</ta>
            <ta e="T33" id="Seg_414" s="T32">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_415" s="T0">pro.h:A</ta>
            <ta e="T3" id="Seg_416" s="T2">np:P</ta>
            <ta e="T5" id="Seg_417" s="T4">np:Time</ta>
            <ta e="T7" id="Seg_418" s="T6">np:Th</ta>
            <ta e="T9" id="Seg_419" s="T8">0.1.h:A</ta>
            <ta e="T13" id="Seg_420" s="T12">np:G</ta>
            <ta e="T14" id="Seg_421" s="T13">0.1.h:A 0.3:Th</ta>
            <ta e="T17" id="Seg_422" s="T16">0.1.h:A 0.3:P</ta>
            <ta e="T18" id="Seg_423" s="T17">adv:Time</ta>
            <ta e="T19" id="Seg_424" s="T18">0.1.h:A</ta>
            <ta e="T20" id="Seg_425" s="T19">adv:G</ta>
            <ta e="T23" id="Seg_426" s="T22">0.1.h:A 0.3:Th</ta>
            <ta e="T24" id="Seg_427" s="T23">adv:Time</ta>
            <ta e="T25" id="Seg_428" s="T24">adv:G</ta>
            <ta e="T26" id="Seg_429" s="T25">0.1.h:A 0.3:Th</ta>
            <ta e="T31" id="Seg_430" s="T30">np:Ins</ta>
            <ta e="T32" id="Seg_431" s="T31">np:G</ta>
            <ta e="T33" id="Seg_432" s="T32">0.3.h:A 0.3:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_433" s="T0">pro.h:S</ta>
            <ta e="T2" id="Seg_434" s="T1">v:pred</ta>
            <ta e="T3" id="Seg_435" s="T2">np:O</ta>
            <ta e="T7" id="Seg_436" s="T6">np:O</ta>
            <ta e="T9" id="Seg_437" s="T8">0.1.h:S v:pred</ta>
            <ta e="T10" id="Seg_438" s="T9">s:purp</ta>
            <ta e="T12" id="Seg_439" s="T10">s:temp</ta>
            <ta e="T14" id="Seg_440" s="T13">0.1.h:S 0.3:O v:pred</ta>
            <ta e="T16" id="Seg_441" s="T14">s:temp</ta>
            <ta e="T17" id="Seg_442" s="T16">0.1.h:S 0.3:O v:pred</ta>
            <ta e="T19" id="Seg_443" s="T18">0.1.h:S v:pred</ta>
            <ta e="T23" id="Seg_444" s="T22">0.1.h:S 0.3:O v:pred</ta>
            <ta e="T26" id="Seg_445" s="T25">0.1.h:S 0.3:O v:pred</ta>
            <ta e="T27" id="Seg_446" s="T26">s:compl</ta>
            <ta e="T30" id="Seg_447" s="T27">s:temp</ta>
            <ta e="T33" id="Seg_448" s="T32">0.3.h:S 0.3:O v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_449" s="T0">new</ta>
            <ta e="T3" id="Seg_450" s="T2">new</ta>
            <ta e="T7" id="Seg_451" s="T6">accs-gen</ta>
            <ta e="T9" id="Seg_452" s="T8">0.giv-active</ta>
            <ta e="T13" id="Seg_453" s="T12">new</ta>
            <ta e="T14" id="Seg_454" s="T13">0.giv-active</ta>
            <ta e="T17" id="Seg_455" s="T16">0.giv-active.0</ta>
            <ta e="T19" id="Seg_456" s="T18">0.giv-active.0</ta>
            <ta e="T21" id="Seg_457" s="T20">accs-gen</ta>
            <ta e="T23" id="Seg_458" s="T22">0.giv-active.0</ta>
            <ta e="T26" id="Seg_459" s="T25">0.giv-active.0</ta>
            <ta e="T31" id="Seg_460" s="T30">new</ta>
            <ta e="T32" id="Seg_461" s="T31">new</ta>
            <ta e="T33" id="Seg_462" s="T32">0.new</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_463" s="T2">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_464" s="T0">Я делаю кирпич[и]. </ta>
            <ta e="T10" id="Seg_465" s="T3">Сначала дёрн снимаю, чтобы он растаял.</ta>
            <ta e="T14" id="Seg_466" s="T10">После того, как растает, в ёмкости натаскаю.</ta>
            <ta e="T17" id="Seg_467" s="T14">После того, как натаскаю, начну топтать.</ta>
            <ta e="T23" id="Seg_468" s="T17">Потом бить буду [кирпичи], на улицу на день поставлю.</ta>
            <ta e="T27" id="Seg_469" s="T23">Потом на улицу перетаскаю, чтобы сохло.</ta>
            <ta e="T33" id="Seg_470" s="T27">Сделав, закончив, после этого на лодке на место [кто-то] будет таскать.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_471" s="T0">I make bricks.</ta>
            <ta e="T10" id="Seg_472" s="T3">First I cut off the surface turf, so that it could melt.</ta>
            <ta e="T14" id="Seg_473" s="T10">After it melts, I carry it into some containers.</ta>
            <ta e="T17" id="Seg_474" s="T14">After I having carried it there, I start trampling it down.</ta>
            <ta e="T23" id="Seg_475" s="T17">Then I hit it [the bricks] and put it outside for a day.</ta>
            <ta e="T27" id="Seg_476" s="T23">Then I carry it outside to dry it out.</ta>
            <ta e="T33" id="Seg_477" s="T27">After all this is done, someone will carry it by boat to some place.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_478" s="T0">Ich mache Ziegelsteine.</ta>
            <ta e="T10" id="Seg_479" s="T3">Zuerst nehme ich die Erdkrume ab, damit sie auftauen kann.</ta>
            <ta e="T14" id="Seg_480" s="T10">Nachdem sie aufgetaut ist, schleppe ich sie in einem Behälter rein.</ta>
            <ta e="T17" id="Seg_481" s="T14">Nachdem ich sie dahin geschleppt habe, fange ich an sie zu stampfen.</ta>
            <ta e="T23" id="Seg_482" s="T17">Dann haue ich auf sie drauf und bringe sie für einen Tag nach draußen.</ta>
            <ta e="T27" id="Seg_483" s="T23">Dann schleppe ich sie nach draußen, damit sie trocknen.</ta>
            <ta e="T33" id="Seg_484" s="T27">Nachdem ich das gemacht habe, bringt man sie mit einem Boot irgendwohin.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_485" s="T0">я делаю кирпичи</ta>
            <ta e="T10" id="Seg_486" s="T3">в начале дерн [шкуру земли] снимаю чтобы растаяла</ta>
            <ta e="T14" id="Seg_487" s="T10">после того как растает в виде корыта натаскаю</ta>
            <ta e="T17" id="Seg_488" s="T14">после того, как натаскаю, глину топтать буду</ta>
            <ta e="T23" id="Seg_489" s="T17">стукать буду (кирпичи) на следующий день на улицу поставлю</ta>
            <ta e="T27" id="Seg_490" s="T23">потом на улицу перетаскаю высушить</ta>
            <ta e="T33" id="Seg_491" s="T27">сделав докончив на лодке [лодкой] на место перевозить [таскать] буду</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T10" id="Seg_492" s="T3">[OSV]: 1) "poːsɨlʼ pɔːr" - "at first"; 2) "təttɨt qopɨ" - "the topsoil".</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T14" id="Seg_493" s="T10">в сосуде, где делают кирпичи</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
