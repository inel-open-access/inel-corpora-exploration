<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID83A50F26-119D-57BD-83ED-45DD3C0A0827">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\IAI_1968_Fishing_nar\IAI_1968_Fishing_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">47</ud-information>
            <ud-information attribute-name="# HIAT:w">39</ud-information>
            <ud-information attribute-name="# e">39</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="IAI">
            <abbreviation>IAI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="IAI"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T39" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">mat</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">tapčʼeːl</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">qwajaɣak</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">poqɨlap</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">mannɨmbɨgu</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">tabla</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">kenǯembat</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Käba</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">Nʼurgɨt</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_35" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">čaːǯak</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">naǯadelʼe</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">matalotkaɣe</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_47" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">okɨr</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">poqoɣɨt</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">kwassam</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">šədəja</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">pečap</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">i</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">nagur</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">pʼäːǯep</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_74" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">šədəǯel</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">poqoɣət</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">qwädɨmbat</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">naːgur</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">peča</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">i</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_94" n="HIAT:w" s="T26">naːgur</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_97" n="HIAT:w" s="T27">qaza</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_101" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_103" n="HIAT:w" s="T28">čwese</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_106" n="HIAT:w" s="T29">čaːǯak</ts>
                  <nts id="Seg_107" n="HIAT:ip">,</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">Kɨba</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">Nʼürowte</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">qoːlǯam</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_119" n="HIAT:w" s="T33">nagur</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_122" n="HIAT:w" s="T34">šibap</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_125" n="HIAT:w" s="T35">haːqoǯəp</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_129" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">tabla</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">oːrɨmbat</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_137" n="HIAT:w" s="T38">pagešak</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T39" id="Seg_140" n="sc" s="T0">
               <ts e="T1" id="Seg_142" n="e" s="T0">mat </ts>
               <ts e="T2" id="Seg_144" n="e" s="T1">tapčʼeːl </ts>
               <ts e="T3" id="Seg_146" n="e" s="T2">qwajaɣak </ts>
               <ts e="T4" id="Seg_148" n="e" s="T3">poqɨlap </ts>
               <ts e="T5" id="Seg_150" n="e" s="T4">mannɨmbɨgu. </ts>
               <ts e="T6" id="Seg_152" n="e" s="T5">tabla </ts>
               <ts e="T7" id="Seg_154" n="e" s="T6">kenǯembat </ts>
               <ts e="T8" id="Seg_156" n="e" s="T7">Käba </ts>
               <ts e="T9" id="Seg_158" n="e" s="T8">Nʼurgɨt. </ts>
               <ts e="T10" id="Seg_160" n="e" s="T9">čaːǯak </ts>
               <ts e="T11" id="Seg_162" n="e" s="T10">naǯadelʼe </ts>
               <ts e="T12" id="Seg_164" n="e" s="T11">matalotkaɣe. </ts>
               <ts e="T13" id="Seg_166" n="e" s="T12">okɨr </ts>
               <ts e="T14" id="Seg_168" n="e" s="T13">poqoɣɨt </ts>
               <ts e="T15" id="Seg_170" n="e" s="T14">kwassam </ts>
               <ts e="T16" id="Seg_172" n="e" s="T15">šədəja </ts>
               <ts e="T17" id="Seg_174" n="e" s="T16">pečap </ts>
               <ts e="T18" id="Seg_176" n="e" s="T17">i </ts>
               <ts e="T19" id="Seg_178" n="e" s="T18">nagur </ts>
               <ts e="T20" id="Seg_180" n="e" s="T19">pʼäːǯep. </ts>
               <ts e="T21" id="Seg_182" n="e" s="T20">šədəǯel </ts>
               <ts e="T22" id="Seg_184" n="e" s="T21">poqoɣət </ts>
               <ts e="T23" id="Seg_186" n="e" s="T22">qwädɨmbat </ts>
               <ts e="T24" id="Seg_188" n="e" s="T23">naːgur </ts>
               <ts e="T25" id="Seg_190" n="e" s="T24">peča </ts>
               <ts e="T26" id="Seg_192" n="e" s="T25">i </ts>
               <ts e="T27" id="Seg_194" n="e" s="T26">naːgur </ts>
               <ts e="T28" id="Seg_196" n="e" s="T27">qaza. </ts>
               <ts e="T29" id="Seg_198" n="e" s="T28">čwese </ts>
               <ts e="T30" id="Seg_200" n="e" s="T29">čaːǯak, </ts>
               <ts e="T31" id="Seg_202" n="e" s="T30">Kɨba </ts>
               <ts e="T32" id="Seg_204" n="e" s="T31">Nʼürowte </ts>
               <ts e="T33" id="Seg_206" n="e" s="T32">qoːlǯam </ts>
               <ts e="T34" id="Seg_208" n="e" s="T33">nagur </ts>
               <ts e="T35" id="Seg_210" n="e" s="T34">šibap </ts>
               <ts e="T36" id="Seg_212" n="e" s="T35">haːqoǯəp. </ts>
               <ts e="T37" id="Seg_214" n="e" s="T36">tabla </ts>
               <ts e="T38" id="Seg_216" n="e" s="T37">oːrɨmbat </ts>
               <ts e="T39" id="Seg_218" n="e" s="T38">pagešak. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_219" s="T0">IAI_1968_Fishing_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_220" s="T5">IAI_1968_Fishing_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_221" s="T9">IAI_1968_Fishing_nar.003 (001.003)</ta>
            <ta e="T20" id="Seg_222" s="T12">IAI_1968_Fishing_nar.004 (001.004)</ta>
            <ta e="T28" id="Seg_223" s="T20">IAI_1968_Fishing_nar.005 (001.005)</ta>
            <ta e="T36" id="Seg_224" s="T28">IAI_1968_Fishing_nar.006 (001.006)</ta>
            <ta e="T39" id="Seg_225" s="T36">IAI_1968_Fishing_nar.007 (001.007)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_226" s="T0">мат тап′чʼе̄l kwа′jаɣак ‵поkылап ‵маннымбы′гу.</ta>
            <ta e="T9" id="Seg_227" s="T5">та′бла кенджем′бат кӓ′ба нʼу(ӧ)ргыт.</ta>
            <ta e="T12" id="Seg_228" s="T9">′ча̄джак наджа′делʼе ‵мата′lоткаɣе.</ta>
            <ta e="T20" id="Seg_229" s="T12">окыр по′kоɣыт кwа′ссам шъдъjа пе′чап и ′нагур пʼӓ̄джеп.</ta>
            <ta e="T28" id="Seg_230" s="T20">шъдъ′джел по′kоɣ(k)ът ′kwӓдымбат ′на̄гур пе′ча и на̄гур ′kаза.</ta>
            <ta e="T36" id="Seg_231" s="T28">′чвесе ′ча̄дж(ж)ак, кыба ′нʼӱроф(в)те kо̄l′джам нагур ′шибап hа̄′kоджъп.</ta>
            <ta e="T39" id="Seg_232" s="T36">таб′ла о̄ру(ы)м′бат па′гешак.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_233" s="T0">mat tapčʼeːl qwajaɣak poqɨlap mannɨmbɨgu.</ta>
            <ta e="T9" id="Seg_234" s="T5">tabla kenǯembat käba nʼu(ö)rgɨt.</ta>
            <ta e="T12" id="Seg_235" s="T9">čaːǯak naǯadelʼe matalotkaɣe.</ta>
            <ta e="T20" id="Seg_236" s="T12">okɨr poqoɣɨt kwassam šədəja pečap i nagur pʼäːǯep.</ta>
            <ta e="T28" id="Seg_237" s="T20">šədəǯel poqoɣ(q)ət qwädɨmbat naːgur peča i naːgur qaza.</ta>
            <ta e="T36" id="Seg_238" s="T28">čvese čaːǯ(ʒ)ak, kɨba nʼürofte qoːlǯam nagur šibap haːqoǯəp.</ta>
            <ta e="T39" id="Seg_239" s="T36">tabla oːru(ɨ)mbat pagešak.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_240" s="T0">mat tapčʼeːl qwajaɣak poqɨlap mannɨmbɨgu. </ta>
            <ta e="T9" id="Seg_241" s="T5">tabla kenǯembat Käba Nʼurgɨt. </ta>
            <ta e="T12" id="Seg_242" s="T9">čaːǯak naǯadelʼe matalotkaɣe. </ta>
            <ta e="T20" id="Seg_243" s="T12">okɨr poqoɣɨt kwassam šədəja pečap i nagur pʼäːǯep. </ta>
            <ta e="T28" id="Seg_244" s="T20">šədəǯel poqoɣət qwädɨmbat naːgur peča i naːgur qaza. </ta>
            <ta e="T36" id="Seg_245" s="T28">čwese čaːǯak, kɨba nʼürowte qoːlǯam nagur šibap haːqoǯəp. </ta>
            <ta e="T39" id="Seg_246" s="T36">tabla oːrɨmbat pagešak. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_247" s="T0">mat</ta>
            <ta e="T2" id="Seg_248" s="T1">tap_čʼeːl</ta>
            <ta e="T3" id="Seg_249" s="T2">qwaja-ɣa-k</ta>
            <ta e="T4" id="Seg_250" s="T3">poq-ɨ-la-p</ta>
            <ta e="T5" id="Seg_251" s="T4">mannɨ-mbɨ-gu</ta>
            <ta e="T6" id="Seg_252" s="T5">tab-la</ta>
            <ta e="T7" id="Seg_253" s="T6">kenǯe-mba-t</ta>
            <ta e="T8" id="Seg_254" s="T7">Käba</ta>
            <ta e="T9" id="Seg_255" s="T8">Nʼur-gɨt</ta>
            <ta e="T10" id="Seg_256" s="T9">čaːǯa-k</ta>
            <ta e="T11" id="Seg_257" s="T10">naǯadelʼe</ta>
            <ta e="T12" id="Seg_258" s="T11">matalotka-ɣe</ta>
            <ta e="T13" id="Seg_259" s="T12">okɨr</ta>
            <ta e="T14" id="Seg_260" s="T13">poqo-qɨt</ta>
            <ta e="T15" id="Seg_261" s="T14">kwas-sa-m</ta>
            <ta e="T16" id="Seg_262" s="T15">šədə-ja</ta>
            <ta e="T17" id="Seg_263" s="T16">peča-p</ta>
            <ta e="T18" id="Seg_264" s="T17">i</ta>
            <ta e="T19" id="Seg_265" s="T18">nagur</ta>
            <ta e="T20" id="Seg_266" s="T19">pʼäːǯe-p</ta>
            <ta e="T21" id="Seg_267" s="T20">šədə-ǯel</ta>
            <ta e="T22" id="Seg_268" s="T21">poqo-ɣət</ta>
            <ta e="T23" id="Seg_269" s="T22">qwädɨ-mba-t</ta>
            <ta e="T24" id="Seg_270" s="T23">naːgur</ta>
            <ta e="T25" id="Seg_271" s="T24">peča</ta>
            <ta e="T26" id="Seg_272" s="T25">i</ta>
            <ta e="T27" id="Seg_273" s="T26">naːgur</ta>
            <ta e="T28" id="Seg_274" s="T27">qaza</ta>
            <ta e="T29" id="Seg_275" s="T28">čwese</ta>
            <ta e="T30" id="Seg_276" s="T29">čaːǯa-k</ta>
            <ta e="T31" id="Seg_277" s="T30">Kɨba</ta>
            <ta e="T32" id="Seg_278" s="T31">Nʼür-o-wte</ta>
            <ta e="T33" id="Seg_279" s="T32">qoː-lǯa-m</ta>
            <ta e="T34" id="Seg_280" s="T33">nagur</ta>
            <ta e="T35" id="Seg_281" s="T34">šiba-p</ta>
            <ta e="T36" id="Seg_282" s="T35">haːqoǯə-p</ta>
            <ta e="T37" id="Seg_283" s="T36">tab-la</ta>
            <ta e="T38" id="Seg_284" s="T37">oːr-ɨ-m-ba-t</ta>
            <ta e="T39" id="Seg_285" s="T38">page-šak</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_286" s="T0">man</ta>
            <ta e="T2" id="Seg_287" s="T1">taw_čeːl</ta>
            <ta e="T3" id="Seg_288" s="T2">qwaja-wa-k</ta>
            <ta e="T4" id="Seg_289" s="T3">poq-ɨ-la-p</ta>
            <ta e="T5" id="Seg_290" s="T4">mantɨ-mbɨ-gu</ta>
            <ta e="T6" id="Seg_291" s="T5">tab-la</ta>
            <ta e="T7" id="Seg_292" s="T6">kɨnǯe-mbɨ-dət</ta>
            <ta e="T8" id="Seg_293" s="T7">Käba</ta>
            <ta e="T9" id="Seg_294" s="T8">Nʼur-qɨt</ta>
            <ta e="T10" id="Seg_295" s="T9">čaːǯɨ-k</ta>
            <ta e="T11" id="Seg_296" s="T10">načidelʼi</ta>
            <ta e="T12" id="Seg_297" s="T11">matalotka-se</ta>
            <ta e="T13" id="Seg_298" s="T12">okkər</ta>
            <ta e="T14" id="Seg_299" s="T13">poq-qɨt</ta>
            <ta e="T15" id="Seg_300" s="T14">kwat-sɨ-m</ta>
            <ta e="T16" id="Seg_301" s="T15">šitə-štja</ta>
            <ta e="T17" id="Seg_302" s="T16">piča-m</ta>
            <ta e="T18" id="Seg_303" s="T17">i</ta>
            <ta e="T19" id="Seg_304" s="T18">nagur</ta>
            <ta e="T20" id="Seg_305" s="T19">pʼäːǯe-p</ta>
            <ta e="T21" id="Seg_306" s="T20">šitə-mǯel</ta>
            <ta e="T22" id="Seg_307" s="T21">poq-qɨt</ta>
            <ta e="T23" id="Seg_308" s="T22">qweːda-mbɨ-dət</ta>
            <ta e="T24" id="Seg_309" s="T23">nagur</ta>
            <ta e="T25" id="Seg_310" s="T24">piča</ta>
            <ta e="T26" id="Seg_311" s="T25">i</ta>
            <ta e="T27" id="Seg_312" s="T26">nagur</ta>
            <ta e="T28" id="Seg_313" s="T27">qaza</ta>
            <ta e="T29" id="Seg_314" s="T28">čwesse</ta>
            <ta e="T30" id="Seg_315" s="T29">čaːǯɨ-k</ta>
            <ta e="T31" id="Seg_316" s="T30">Kɨba</ta>
            <ta e="T32" id="Seg_317" s="T31">Nʼur-ɨ-ut</ta>
            <ta e="T33" id="Seg_318" s="T32">qo-lʼčǝ-m</ta>
            <ta e="T34" id="Seg_319" s="T33">nagur</ta>
            <ta e="T35" id="Seg_320" s="T34">šiba-p</ta>
            <ta e="T36" id="Seg_321" s="T35">haːqoǯə-p</ta>
            <ta e="T37" id="Seg_322" s="T36">tab-la</ta>
            <ta e="T38" id="Seg_323" s="T37">or-ɨ-m-mbɨ-dət</ta>
            <ta e="T39" id="Seg_324" s="T38">page-šak</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_325" s="T0">I.NOM</ta>
            <ta e="T2" id="Seg_326" s="T1">this_day.[NOM]</ta>
            <ta e="T3" id="Seg_327" s="T2">go-CO-1SG.S</ta>
            <ta e="T4" id="Seg_328" s="T3">net-EP-PL-ACC</ta>
            <ta e="T5" id="Seg_329" s="T4">look-HAB-INF</ta>
            <ta e="T6" id="Seg_330" s="T5">(s)he-PL.[NOM]</ta>
            <ta e="T7" id="Seg_331" s="T6">settle.net-PST.NAR-3PL</ta>
            <ta e="T8" id="Seg_332" s="T7">Kyba</ta>
            <ta e="T9" id="Seg_333" s="T8">Nyur-LOC</ta>
            <ta e="T10" id="Seg_334" s="T9">go-3SG.S</ta>
            <ta e="T11" id="Seg_335" s="T10">there</ta>
            <ta e="T12" id="Seg_336" s="T11">motor.boat-INSTR</ta>
            <ta e="T13" id="Seg_337" s="T12">one</ta>
            <ta e="T14" id="Seg_338" s="T13">net-LOC</ta>
            <ta e="T15" id="Seg_339" s="T14">catch-PST-1SG.O</ta>
            <ta e="T16" id="Seg_340" s="T15">two-DU</ta>
            <ta e="T17" id="Seg_341" s="T16">pike-ACC</ta>
            <ta e="T18" id="Seg_342" s="T17">and</ta>
            <ta e="T19" id="Seg_343" s="T18">three</ta>
            <ta e="T20" id="Seg_344" s="T19">roach-ACC</ta>
            <ta e="T21" id="Seg_345" s="T20">two-ORD</ta>
            <ta e="T22" id="Seg_346" s="T21">net-LOC</ta>
            <ta e="T23" id="Seg_347" s="T22">get.caught-PST.NAR-3PL</ta>
            <ta e="T24" id="Seg_348" s="T23">three</ta>
            <ta e="T25" id="Seg_349" s="T24">pike.[NOM]</ta>
            <ta e="T26" id="Seg_350" s="T25">and</ta>
            <ta e="T27" id="Seg_351" s="T26">three</ta>
            <ta e="T28" id="Seg_352" s="T27">perch.[NOM]</ta>
            <ta e="T29" id="Seg_353" s="T28">backward</ta>
            <ta e="T30" id="Seg_354" s="T29">go-3SG.S</ta>
            <ta e="T31" id="Seg_355" s="T30">Kyba</ta>
            <ta e="T32" id="Seg_356" s="T31">Nyur-EP-PROL</ta>
            <ta e="T33" id="Seg_357" s="T32">sight-PFV-1SG.O</ta>
            <ta e="T34" id="Seg_358" s="T33">three</ta>
            <ta e="T35" id="Seg_359" s="T34">chicken-ACC</ta>
            <ta e="T36" id="Seg_360" s="T35">drake-ACC</ta>
            <ta e="T37" id="Seg_361" s="T36">(s)he-PL.[NOM]</ta>
            <ta e="T38" id="Seg_362" s="T37">force-EP-TRL-PST.NAR-3PL</ta>
            <ta e="T39" id="Seg_363" s="T38">common_teal-COR</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_364" s="T0">я.NOM</ta>
            <ta e="T2" id="Seg_365" s="T1">этот_день.[NOM]</ta>
            <ta e="T3" id="Seg_366" s="T2">идти-CO-1SG.S</ta>
            <ta e="T4" id="Seg_367" s="T3">сеть-EP-PL-ACC</ta>
            <ta e="T5" id="Seg_368" s="T4">смотреть-HAB-INF</ta>
            <ta e="T6" id="Seg_369" s="T5">он(а)-PL.[NOM]</ta>
            <ta e="T7" id="Seg_370" s="T6">поставить.сеть-PST.NAR-3PL</ta>
            <ta e="T8" id="Seg_371" s="T7">Кыба</ta>
            <ta e="T9" id="Seg_372" s="T8">Нюр-LOC</ta>
            <ta e="T10" id="Seg_373" s="T9">идти-3SG.S</ta>
            <ta e="T11" id="Seg_374" s="T10">туда</ta>
            <ta e="T12" id="Seg_375" s="T11">мотолодка-INSTR</ta>
            <ta e="T13" id="Seg_376" s="T12">один</ta>
            <ta e="T14" id="Seg_377" s="T13">сеть-LOC</ta>
            <ta e="T15" id="Seg_378" s="T14">поймать-PST-1SG.O</ta>
            <ta e="T16" id="Seg_379" s="T15">два-DU</ta>
            <ta e="T17" id="Seg_380" s="T16">щука-ACC</ta>
            <ta e="T18" id="Seg_381" s="T17">и</ta>
            <ta e="T19" id="Seg_382" s="T18">три</ta>
            <ta e="T20" id="Seg_383" s="T19">чебак-ACC</ta>
            <ta e="T21" id="Seg_384" s="T20">два-ORD</ta>
            <ta e="T22" id="Seg_385" s="T21">сеть-LOC</ta>
            <ta e="T23" id="Seg_386" s="T22">попасться-PST.NAR-3PL</ta>
            <ta e="T24" id="Seg_387" s="T23">три</ta>
            <ta e="T25" id="Seg_388" s="T24">щука.[NOM]</ta>
            <ta e="T26" id="Seg_389" s="T25">и</ta>
            <ta e="T27" id="Seg_390" s="T26">три</ta>
            <ta e="T28" id="Seg_391" s="T27">окунь.[NOM]</ta>
            <ta e="T29" id="Seg_392" s="T28">назад</ta>
            <ta e="T30" id="Seg_393" s="T29">идти-3SG.S</ta>
            <ta e="T31" id="Seg_394" s="T30">Кыба</ta>
            <ta e="T32" id="Seg_395" s="T31">Нюр-EP-PROL</ta>
            <ta e="T33" id="Seg_396" s="T32">увидеть-PFV-1SG.O</ta>
            <ta e="T34" id="Seg_397" s="T33">три</ta>
            <ta e="T35" id="Seg_398" s="T34">цыплёнок-ACC</ta>
            <ta e="T36" id="Seg_399" s="T35">селезень-ACC</ta>
            <ta e="T37" id="Seg_400" s="T36">он(а)-PL.[NOM]</ta>
            <ta e="T38" id="Seg_401" s="T37">сила-EP-TRL-PST.NAR-3PL</ta>
            <ta e="T39" id="Seg_402" s="T38">утка_чирок-COR</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_403" s="T0">pers</ta>
            <ta e="T2" id="Seg_404" s="T1">n-n:case1</ta>
            <ta e="T3" id="Seg_405" s="T2">v-v:ins-v:pn</ta>
            <ta e="T4" id="Seg_406" s="T3">n-infl:ins-n:num-n:case1</ta>
            <ta e="T5" id="Seg_407" s="T4">v-v&gt;v-v:ninf</ta>
            <ta e="T6" id="Seg_408" s="T5">pers-n:num-n:case1</ta>
            <ta e="T7" id="Seg_409" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_410" s="T7">nprop</ta>
            <ta e="T9" id="Seg_411" s="T8">nprop-n:case1</ta>
            <ta e="T10" id="Seg_412" s="T9">v-v:pn</ta>
            <ta e="T11" id="Seg_413" s="T10">adv</ta>
            <ta e="T12" id="Seg_414" s="T11">n-n:case2</ta>
            <ta e="T13" id="Seg_415" s="T12">num</ta>
            <ta e="T14" id="Seg_416" s="T13">n-n:case1</ta>
            <ta e="T15" id="Seg_417" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_418" s="T15">num-n:num</ta>
            <ta e="T17" id="Seg_419" s="T16">n-n:case1</ta>
            <ta e="T18" id="Seg_420" s="T17">conj</ta>
            <ta e="T19" id="Seg_421" s="T18">num</ta>
            <ta e="T20" id="Seg_422" s="T19">n-n:case1</ta>
            <ta e="T21" id="Seg_423" s="T20">num-num&gt;adj</ta>
            <ta e="T22" id="Seg_424" s="T21">n-n:case1</ta>
            <ta e="T23" id="Seg_425" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_426" s="T23">num</ta>
            <ta e="T25" id="Seg_427" s="T24">n-n:case1</ta>
            <ta e="T26" id="Seg_428" s="T25">conj</ta>
            <ta e="T27" id="Seg_429" s="T26">num</ta>
            <ta e="T28" id="Seg_430" s="T27">n-n:case1</ta>
            <ta e="T29" id="Seg_431" s="T28">adv</ta>
            <ta e="T30" id="Seg_432" s="T29">v-v:pn</ta>
            <ta e="T31" id="Seg_433" s="T30">nprop</ta>
            <ta e="T32" id="Seg_434" s="T31">nprop-infl:ins-n:case1</ta>
            <ta e="T33" id="Seg_435" s="T32">v-v&gt;v-v:pn</ta>
            <ta e="T34" id="Seg_436" s="T33">num</ta>
            <ta e="T35" id="Seg_437" s="T34">n-n:case1</ta>
            <ta e="T36" id="Seg_438" s="T35">n-n:case1</ta>
            <ta e="T37" id="Seg_439" s="T36">pers-n:num-n:case1</ta>
            <ta e="T38" id="Seg_440" s="T37">n-infl:ins-n&gt;v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_441" s="T38">n-n:case1</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_442" s="T0">pers</ta>
            <ta e="T2" id="Seg_443" s="T1">n</ta>
            <ta e="T3" id="Seg_444" s="T2">v</ta>
            <ta e="T4" id="Seg_445" s="T3">n</ta>
            <ta e="T5" id="Seg_446" s="T4">v</ta>
            <ta e="T6" id="Seg_447" s="T5">pers</ta>
            <ta e="T7" id="Seg_448" s="T6">v</ta>
            <ta e="T8" id="Seg_449" s="T7">nprop</ta>
            <ta e="T9" id="Seg_450" s="T8">nprop</ta>
            <ta e="T10" id="Seg_451" s="T9">v</ta>
            <ta e="T11" id="Seg_452" s="T10">adv</ta>
            <ta e="T12" id="Seg_453" s="T11">n</ta>
            <ta e="T13" id="Seg_454" s="T12">num</ta>
            <ta e="T14" id="Seg_455" s="T13">n</ta>
            <ta e="T15" id="Seg_456" s="T14">v</ta>
            <ta e="T16" id="Seg_457" s="T15">num</ta>
            <ta e="T17" id="Seg_458" s="T16">n</ta>
            <ta e="T18" id="Seg_459" s="T17">conj</ta>
            <ta e="T19" id="Seg_460" s="T18">num</ta>
            <ta e="T20" id="Seg_461" s="T19">n</ta>
            <ta e="T21" id="Seg_462" s="T20">adj</ta>
            <ta e="T22" id="Seg_463" s="T21">n</ta>
            <ta e="T23" id="Seg_464" s="T22">v</ta>
            <ta e="T24" id="Seg_465" s="T23">num</ta>
            <ta e="T25" id="Seg_466" s="T24">n</ta>
            <ta e="T26" id="Seg_467" s="T25">conj</ta>
            <ta e="T27" id="Seg_468" s="T26">num</ta>
            <ta e="T28" id="Seg_469" s="T27">n</ta>
            <ta e="T29" id="Seg_470" s="T28">adv</ta>
            <ta e="T30" id="Seg_471" s="T29">v</ta>
            <ta e="T31" id="Seg_472" s="T30">nprop</ta>
            <ta e="T32" id="Seg_473" s="T31">nprop</ta>
            <ta e="T33" id="Seg_474" s="T32">v</ta>
            <ta e="T34" id="Seg_475" s="T33">num</ta>
            <ta e="T35" id="Seg_476" s="T34">n</ta>
            <ta e="T36" id="Seg_477" s="T35">n</ta>
            <ta e="T37" id="Seg_478" s="T36">pers</ta>
            <ta e="T38" id="Seg_479" s="T37">v</ta>
            <ta e="T39" id="Seg_480" s="T38">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T12" id="Seg_481" s="T11">RUS:cult</ta>
            <ta e="T18" id="Seg_482" s="T17">RUS:gram</ta>
            <ta e="T26" id="Seg_483" s="T25">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_484" s="T0">Я сегодня поехал сети смотреть.</ta>
            <ta e="T9" id="Seg_485" s="T5">Они стоят в Кыба Нюре.</ta>
            <ta e="T12" id="Seg_486" s="T9">Я ехал туда на мотолодке.</ta>
            <ta e="T20" id="Seg_487" s="T12">В одной сети поймал две щуки и три чебака.</ta>
            <ta e="T28" id="Seg_488" s="T20">Во второй сети попались три щуки и три окуня.</ta>
            <ta e="T36" id="Seg_489" s="T28">Обратно еду по [озеру] Кыба Нюр, увидел три цыпленка селезня.</ta>
            <ta e="T39" id="Seg_490" s="T36">Они выросли с чирка.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_491" s="T0">Today I went to look at the nets.</ta>
            <ta e="T9" id="Seg_492" s="T5">They are settled in Kyba Nyur.</ta>
            <ta e="T12" id="Seg_493" s="T9">I went there with a motor boat.</ta>
            <ta e="T20" id="Seg_494" s="T12">In one net, I caught two pikes and three roaches.</ta>
            <ta e="T28" id="Seg_495" s="T20">In the second net, there came three pikes and three perches.</ta>
            <ta e="T36" id="Seg_496" s="T28">I was going back by the Kyba Nyur [lake], I saw three drake chickens.</ta>
            <ta e="T39" id="Seg_497" s="T36">They were [of the size] of the teal.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_498" s="T0">Heute fuhr ich, um die Netze zu kontrollieren.</ta>
            <ta e="T9" id="Seg_499" s="T5">Sie stehen in Kyba Njur.</ta>
            <ta e="T12" id="Seg_500" s="T9">Ich fuhr mit einem Motorboot dorthin.</ta>
            <ta e="T20" id="Seg_501" s="T12">In einem Netz fing ich zwei Hechte und drei Rotaugen.</ta>
            <ta e="T28" id="Seg_502" s="T20">Ins zweite Netz waren drei Hechte und drei Rotaugen gegangen.</ta>
            <ta e="T36" id="Seg_503" s="T28">Ich fahre zurück über den Kyba Njur[-See], ich sah drei junge Erpel.</ta>
            <ta e="T39" id="Seg_504" s="T36">Sie waren [so groß] wie eine Krickente.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_505" s="T0">я сегодня поехал сети смотреть</ta>
            <ta e="T9" id="Seg_506" s="T5">они стоят (поставлены) в Кыба Нюре</ta>
            <ta e="T12" id="Seg_507" s="T9">я ехал туда на мотолодке</ta>
            <ta e="T20" id="Seg_508" s="T12">в одной сети добыл две щуки три чебака</ta>
            <ta e="T28" id="Seg_509" s="T20">во второй сети попались три щуки и три окуня</ta>
            <ta e="T36" id="Seg_510" s="T28">обратно еду по озеру Кыба Нюр увидел три цыпленка селезня</ta>
            <ta e="T39" id="Seg_511" s="T36">они выросли с чирка (утку)</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
