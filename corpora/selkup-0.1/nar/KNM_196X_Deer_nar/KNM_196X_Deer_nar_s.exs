<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDAC5CE049-43B0-6DBA-AFA2-8B23329CB066">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="KNM_196X_Deer_nar.wav" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\KNM_196X_Deer_nar\KNM_196X_Deer_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">249</ud-information>
            <ud-information attribute-name="# HIAT:w">175</ud-information>
            <ud-information attribute-name="# e">180</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">4</ud-information>
            <ud-information attribute-name="# HIAT:u">33</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KNM">
            <abbreviation>KNM</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="1.4" type="appl" />
         <tli id="T1" time="2.211" type="appl" />
         <tli id="T2" time="3.021" type="appl" />
         <tli id="T3" time="3.832" type="appl" />
         <tli id="T4" time="4.642" type="appl" />
         <tli id="T5" time="5.453" type="appl" />
         <tli id="T6" time="7.013" type="appl" />
         <tli id="T7" time="13.497" type="appl" />
         <tli id="T8" time="14.681" type="appl" />
         <tli id="T9" time="15.865" type="appl" />
         <tli id="T10" time="17.048" type="appl" />
         <tli id="T11" time="18.232" type="appl" />
         <tli id="T12" time="19.416" type="appl" />
         <tli id="T13" time="20.6" type="appl" />
         <tli id="T14" time="23.015" type="appl" />
         <tli id="T15" time="23.858" type="appl" />
         <tli id="T16" time="24.7" type="appl" />
         <tli id="T17" time="25.543" type="appl" />
         <tli id="T18" time="26.385" type="appl" />
         <tli id="T19" time="27.228" type="appl" />
         <tli id="T20" time="28.07" type="appl" />
         <tli id="T21" time="30.501" type="appl" />
         <tli id="T22" time="31.016" type="appl" />
         <tli id="T23" time="31.53" type="appl" />
         <tli id="T24" time="32.045" type="appl" />
         <tli id="T25" time="32.56" type="appl" />
         <tli id="T26" time="33.311" type="appl" />
         <tli id="T27" time="33.922" type="appl" />
         <tli id="T28" time="34.533" type="appl" />
         <tli id="T29" time="36.964" type="appl" />
         <tli id="T30" time="37.921" type="appl" />
         <tli id="T31" time="38.879" type="appl" />
         <tli id="T32" time="41.224" type="appl" />
         <tli id="T33" time="42.275" type="appl" />
         <tli id="T34" time="43.327" type="appl" />
         <tli id="T35" time="44.379" type="appl" />
         <tli id="T36" time="45.431" type="appl" />
         <tli id="T37" time="46.482" type="appl" />
         <tli id="T38" time="47.534" type="appl" />
         <tli id="T39" time="48.586" type="appl" />
         <tli id="T40" time="49.539" type="appl" />
         <tli id="T41" time="50.199" type="appl" />
         <tli id="T42" time="51.397" type="appl" />
         <tli id="T43" time="52.048" type="appl" />
         <tli id="T44" time="52.699" type="appl" />
         <tli id="T45" time="53.35" type="appl" />
         <tli id="T46" time="54.0" type="appl" />
         <tli id="T47" time="54.651" type="appl" />
         <tli id="T48" time="55.302" type="appl" />
         <tli id="T49" time="55.953" type="appl" />
         <tli id="T50" time="57.961" type="appl" />
         <tli id="T51" time="58.497" type="appl" />
         <tli id="T52" time="59.032" type="appl" />
         <tli id="T53" time="59.912" type="appl" />
         <tli id="T54" time="60.452" type="appl" />
         <tli id="T55" time="60.992" type="appl" />
         <tli id="T56" time="61.532" type="appl" />
         <tli id="T57" time="62.072" type="appl" />
         <tli id="T58" time="63.751" type="appl" />
         <tli id="T59" time="64.337" type="appl" />
         <tli id="T60" time="64.922" type="appl" />
         <tli id="T61" time="65.508" type="appl" />
         <tli id="T62" time="66.093" type="appl" />
         <tli id="T63" time="66.679" type="appl" />
         <tli id="T64" time="67.264" type="appl" />
         <tli id="T65" time="67.85" type="appl" />
         <tli id="T66" time="68.435" type="appl" />
         <tli id="T67" time="69.021" type="appl" />
         <tli id="T68" time="69.606" type="appl" />
         <tli id="T69" time="71.134" type="appl" />
         <tli id="T70" time="71.642" type="appl" />
         <tli id="T71" time="72.151" type="appl" />
         <tli id="T72" time="72.659" type="appl" />
         <tli id="T73" time="73.167" type="appl" />
         <tli id="T74" time="73.676" type="appl" />
         <tli id="T75" time="74.184" type="appl" />
         <tli id="T76" time="74.692" type="appl" />
         <tli id="T77" time="76.454" type="appl" />
         <tli id="T78" time="77.096" type="appl" />
         <tli id="T79" time="77.737" type="appl" />
         <tli id="T80" time="78.379" type="appl" />
         <tli id="T81" time="80.396" type="appl" />
         <tli id="T82" time="80.981" type="appl" />
         <tli id="T83" time="81.567" type="appl" />
         <tli id="T84" time="82.153" type="appl" />
         <tli id="T85" time="82.739" type="appl" />
         <tli id="T86" time="83.324" type="appl" />
         <tli id="T87" time="83.91" type="appl" />
         <tli id="T88" time="85.33" type="appl" />
         <tli id="T89" time="85.923" type="appl" />
         <tli id="T90" time="86.412" type="appl" />
         <tli id="T91" time="86.9" type="appl" />
         <tli id="T92" time="87.722" type="appl" />
         <tli id="T93" time="88.185" type="appl" />
         <tli id="T94" time="88.85" type="appl" />
         <tli id="T95" time="89.34" type="appl" />
         <tli id="T96" time="90.057" type="appl" />
         <tli id="T97" time="90.448" type="appl" />
         <tli id="T98" time="90.84" type="appl" />
         <tli id="T99" time="91.232" type="appl" />
         <tli id="T100" time="91.623" type="appl" />
         <tli id="T101" time="92.015" type="appl" />
         <tli id="T102" time="92.671" type="appl" />
         <tli id="T103" time="93.272" type="appl" />
         <tli id="T104" time="93.873" type="appl" />
         <tli id="T105" time="94.474" type="appl" />
         <tli id="T106" time="95.075" type="appl" />
         <tli id="T107" time="98.076" type="appl" />
         <tli id="T108" time="99.166" type="appl" />
         <tli id="T109" time="100.257" type="appl" />
         <tli id="T110" time="101.348" type="appl" />
         <tli id="T111" time="102.439" type="appl" />
         <tli id="T112" time="103.529" type="appl" />
         <tli id="T113" time="104.62" type="appl" />
         <tli id="T114" time="105.758" type="appl" />
         <tli id="T115" time="106.415" type="appl" />
         <tli id="T116" time="107.072" type="appl" />
         <tli id="T117" time="107.73" type="appl" />
         <tli id="T118" time="108.388" type="appl" />
         <tli id="T119" time="109.045" type="appl" />
         <tli id="T120" time="110.975" type="appl" />
         <tli id="T121" time="111.77" type="appl" />
         <tli id="T122" time="112.565" type="appl" />
         <tli id="T123" time="113.36" type="appl" />
         <tli id="T124" time="115.217" type="appl" />
         <tli id="T125" time="115.733" type="appl" />
         <tli id="T126" time="116.25" type="appl" />
         <tli id="T127" time="116.893" type="appl" />
         <tli id="T128" time="117.491" type="appl" />
         <tli id="T129" time="118.089" type="appl" />
         <tli id="T130" time="118.687" type="appl" />
         <tli id="T131" time="119.285" type="appl" />
         <tli id="T132" time="120.417" type="appl" />
         <tli id="T133" time="121.024" type="appl" />
         <tli id="T134" time="121.631" type="appl" />
         <tli id="T135" time="122.238" type="appl" />
         <tli id="T136" time="122.845" type="appl" />
         <tli id="T137" time="124.323" type="appl" />
         <tli id="T138" time="124.921" type="appl" />
         <tli id="T139" time="125.518" type="appl" />
         <tli id="T140" time="126.116" type="appl" />
         <tli id="T141" time="126.714" type="appl" />
         <tli id="T142" time="127.312" type="appl" />
         <tli id="T143" time="127.909" type="appl" />
         <tli id="T144" time="128.507" type="appl" />
         <tli id="T145" time="129.105" type="appl" />
         <tli id="T146" time="129.701" type="appl" />
         <tli id="T147" time="130.188" type="appl" />
         <tli id="T148" time="130.674" type="appl" />
         <tli id="T149" time="131.16" type="appl" />
         <tli id="T150" time="132.585" type="appl" />
         <tli id="T151" time="133.425" type="appl" />
         <tli id="T152" time="134.347" type="appl" />
         <tli id="T153" time="135.178" type="appl" />
         <tli id="T154" time="136.01" type="appl" />
         <tli id="T155" time="136.842" type="appl" />
         <tli id="T156" time="137.673" type="appl" />
         <tli id="T157" time="138.505" type="appl" />
         <tli id="T158" time="140.738" type="appl" />
         <tli id="T159" time="141.536" type="appl" />
         <tli id="T160" time="142.335" type="appl" />
         <tli id="T161" time="143.133" type="appl" />
         <tli id="T162" time="143.931" type="appl" />
         <tli id="T163" time="144.729" type="appl" />
         <tli id="T164" time="145.528" type="appl" />
         <tli id="T165" time="146.326" type="appl" />
         <tli id="T166" time="147.124" type="appl" />
         <tli id="T167" time="147.922" type="appl" />
         <tli id="T168" time="148.72" type="appl" />
         <tli id="T169" time="149.519" type="appl" />
         <tli id="T170" time="150.317" type="appl" />
         <tli id="T171" time="151.115" type="appl" />
         <tli id="T172" time="152.342" type="appl" />
         <tli id="T173" time="152.753" type="appl" />
         <tli id="T174" time="153.165" type="appl" />
         <tli id="T175" time="153.577" type="appl" />
         <tli id="T176" time="153.988" type="appl" />
         <tli id="T177" time="154.4" type="appl" />
         <tli id="T178" time="154.812" type="appl" />
         <tli id="T179" time="155.223" type="appl" />
         <tli id="T180" time="155.635" type="appl" />
         <tli id="T181" time="157.733" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KNM"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T180" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Рассказ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">про</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">пастухов</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">пуровского</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">района</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Оленеводов</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_26" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">Šettɨrqɨt</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">sɨrčʼip</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">säptaıːptäqɨn</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">teštɨm</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">ɔːtaiːmtɨ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">muntik</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">taqqɨlʼeıːmpɔːtɨn</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_50" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">Ɔːtaiːmtɨ</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">taqqɨlla</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">teštɨm</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">ɔːtamtɨ</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_63" n="HIAT:ip">(</nts>
                  <nts id="Seg_64" n="HIAT:ip">(</nts>
                  <ats e="T18" id="Seg_65" n="HIAT:non-pho" s="T17">…</ats>
                  <nts id="Seg_66" n="HIAT:ip">)</nts>
                  <nts id="Seg_67" n="HIAT:ip">)</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">qaralʼ</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">taqqɨlʼkɨjoıːmpɔːtɨn</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_77" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">To</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">toqqɨla</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">ɔːtaiːmtɨ</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">qaralʼqɨn</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_90" n="HIAT:ip">(</nts>
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">čʼaqɨ</ts>
                  <nts id="Seg_93" n="HIAT:ip">)</nts>
                  <nts id="Seg_94" n="HIAT:ip">.</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_97" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_99" n="HIAT:w" s="T25">Ukkɨr</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">mɨqɨn</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_105" n="HIAT:w" s="T27">mərqəıːmpɔːtɨn</ts>
                  <nts id="Seg_106" n="HIAT:ip">.</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_109" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_111" n="HIAT:w" s="T28">Čʼektɨtɨm</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_113" n="HIAT:ip">(</nts>
                  <ts e="T30" id="Seg_115" n="HIAT:w" s="T29">ketɨlʼtɨkɨjoıːmpɔːtɨn</ts>
                  <nts id="Seg_116" n="HIAT:ip">)</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_119" n="HIAT:w" s="T30">ɔːttɨla</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_123" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_125" n="HIAT:w" s="T31">Ɔːtɨm</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_128" n="HIAT:w" s="T32">kəːtɨmpɨla</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_131" n="HIAT:w" s="T33">pula</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_134" n="HIAT:w" s="T34">nänɨqa</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_137" n="HIAT:w" s="T35">məšeıːptäqin</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_140" n="HIAT:w" s="T36">ɔːtaiːmtɨ</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_143" n="HIAT:w" s="T37">purkɨsa</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_146" n="HIAT:w" s="T38">mɨrɨkoıːmpɔːtɨn</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_150" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_152" n="HIAT:w" s="T39">Tam</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_155" n="HIAT:w" s="T40">Pürqɨn</ts>
                  <nts id="Seg_156" n="HIAT:ip">.</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_159" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_161" n="HIAT:w" s="T41">Ɔːm</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">ɔːtaiːmɨn</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_167" n="HIAT:w" s="T43">na</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_170" n="HIAT:w" s="T44">nänɨqaqɨn</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_173" n="HIAT:w" s="T45">čʼaqɨ</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_175" n="HIAT:ip">(</nts>
                  <nts id="Seg_176" n="HIAT:ip">(</nts>
                  <ats e="T47" id="Seg_177" n="HIAT:non-pho" s="T46">…</ats>
                  <nts id="Seg_178" n="HIAT:ip">)</nts>
                  <nts id="Seg_179" n="HIAT:ip">)</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_182" n="HIAT:w" s="T47">Tolʼkɨntɨ</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_185" n="HIAT:w" s="T48">paktɨkoıːmpɔːtɨn</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_189" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_191" n="HIAT:w" s="T49">Taplʼaqɨn</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_194" n="HIAT:w" s="T50">čʼaq</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_197" n="HIAT:w" s="T51">paktɔːtɨn</ts>
                  <nts id="Seg_198" n="HIAT:ip">.</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_201" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_203" n="HIAT:w" s="T52">Təpɨt</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_206" n="HIAT:w" s="T53">tüla</ts>
                  <nts id="Seg_207" n="HIAT:ip">,</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_210" n="HIAT:w" s="T54">aj</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_213" n="HIAT:w" s="T55">sukkulʼta</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_216" n="HIAT:w" s="T56">tokkoıːmpɔːtɨn</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_219" n="HIAT:w" s="T57">Toː</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_222" n="HIAT:w" s="T58">paktɨlʼa</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_225" n="HIAT:w" s="T59">šʼittelʼa</ts>
                  <nts id="Seg_226" n="HIAT:ip">,</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_229" n="HIAT:w" s="T60">toj</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_232" n="HIAT:w" s="T61">pɨlaqɨn</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_235" n="HIAT:w" s="T62">Pürqɨn</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_238" n="HIAT:w" s="T63">ɔːtaiːmtɨ</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_241" n="HIAT:w" s="T64">čʼaqɨ</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_244" n="HIAT:w" s="T65">nɨmtɨ</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_247" n="HIAT:w" s="T66">čʼaqɨ</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_250" n="HIAT:w" s="T67">qälɨmpɔːtɨn</ts>
                  <nts id="Seg_251" n="HIAT:ip">.</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_254" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_256" n="HIAT:w" s="T68">Ɔːtalʼ</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_259" n="HIAT:w" s="T69">ta</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_262" n="HIAT:w" s="T70">tɨpot</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_265" n="HIAT:w" s="T71">kočʼet</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_268" n="HIAT:w" s="T72">ɛːqan</ts>
                  <nts id="Seg_269" n="HIAT:ip">,</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_272" n="HIAT:w" s="T73">ɔːtaj</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_275" n="HIAT:w" s="T74">mɔːt</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_277" n="HIAT:ip">(</nts>
                  <ts e="T76" id="Seg_279" n="HIAT:w" s="T75">mekijoıːmpɔːtɨn</ts>
                  <nts id="Seg_280" n="HIAT:ip">)</nts>
                  <nts id="Seg_281" n="HIAT:ip">.</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_284" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_286" n="HIAT:w" s="T76">Ɔːtan</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_289" n="HIAT:w" s="T77">ɔːqɨn</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_292" n="HIAT:w" s="T78">nɨmtɨ</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_294" n="HIAT:ip">(</nts>
                  <ts e="T80" id="Seg_296" n="HIAT:w" s="T79">čʼiqəlʼkijoıːmpɔːtɨn</ts>
                  <nts id="Seg_297" n="HIAT:ip">)</nts>
                  <nts id="Seg_298" n="HIAT:ip">.</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_301" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_303" n="HIAT:w" s="T80">Ɔːtɨ</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_306" n="HIAT:w" s="T81">samoj</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_309" n="HIAT:w" s="T82">koštɨ</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_312" n="HIAT:w" s="T83">vremʼatɨ</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_315" n="HIAT:w" s="T84">ɛːqan</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_318" n="HIAT:w" s="T85">šittɨ</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_321" n="HIAT:w" s="T86">iraqıtɨ</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_325" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_327" n="HIAT:w" s="T87">İjulʼ</ts>
                  <nts id="Seg_328" n="HIAT:ip">.</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_331" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_333" n="HIAT:w" s="T88">İjulʼ</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_336" n="HIAT:w" s="T89">mesʼats</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_339" n="HIAT:w" s="T90">ɛːqan</ts>
                  <nts id="Seg_340" n="HIAT:ip">.</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_343" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_345" n="HIAT:w" s="T91">Aj</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_348" n="HIAT:w" s="T92">avgust</ts>
                  <nts id="Seg_349" n="HIAT:ip">.</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_352" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_354" n="HIAT:w" s="T93">Aj</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_357" n="HIAT:w" s="T94">ijunʼ</ts>
                  <nts id="Seg_358" n="HIAT:ip">.</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_361" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_363" n="HIAT:w" s="T95">Na</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_366" n="HIAT:w" s="T96">nɔːkɨr</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_369" n="HIAT:w" s="T97">iraqɨntɨ</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_372" n="HIAT:w" s="T98">taqɨn</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_375" n="HIAT:w" s="T99">twəttɨ</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_378" n="HIAT:w" s="T100">meːkkɨjoıːmpat</ts>
                  <nts id="Seg_379" n="HIAT:ip">.</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_382" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_384" n="HIAT:w" s="T101">Twəttɨ</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_387" n="HIAT:w" s="T102">nɨmtɨ</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_390" n="HIAT:w" s="T103">kɨrapoːqɨn</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_393" n="HIAT:w" s="T104">šettɨ</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_396" n="HIAT:w" s="T105">ropɨtɨkkɨjoıːmpɔːtɨn</ts>
                  <nts id="Seg_397" n="HIAT:ip">.</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_400" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_402" n="HIAT:w" s="T106">Qara</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_405" n="HIAT:w" s="T107">montɨqɨn</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_408" n="HIAT:w" s="T108">qənnaıːtäqɨn</ts>
                  <nts id="Seg_409" n="HIAT:ip">,</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_412" n="HIAT:w" s="T109">kočʼe</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_415" n="HIAT:w" s="T110">ɔːtaiːmtɨ</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_418" n="HIAT:w" s="T111">nʼentɨ</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_421" n="HIAT:w" s="T112">taqqɨloıːmpɔːtɨn</ts>
                  <nts id="Seg_422" n="HIAT:ip">.</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_425" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_427" n="HIAT:w" s="T113">Ɔːtaiːmtɨ</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_430" n="HIAT:w" s="T114">keksa</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_433" n="HIAT:w" s="T115">muntɨk</ts>
                  <nts id="Seg_434" n="HIAT:ip">,</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_437" n="HIAT:w" s="T116">nʼentɨ</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_440" n="HIAT:w" s="T117">taqqilʼlʼa</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_443" n="HIAT:w" s="T118">tüntɔːtɨn</ts>
                  <nts id="Seg_444" n="HIAT:ip">.</nts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_447" n="HIAT:u" s="T119">
                  <ts e="T121" id="Seg_449" n="HIAT:w" s="T119">Ohotnʼikinɨmtɨ</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_452" n="HIAT:w" s="T121">ɔːtam</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_455" n="HIAT:w" s="T122">miːkkijoıːmpɔːtɨn</ts>
                  <nts id="Seg_456" n="HIAT:ip">.</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_459" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_461" n="HIAT:w" s="T123">Surɨčʼčʼɨj</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_464" n="HIAT:w" s="T124">qumɨn</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_467" n="HIAT:w" s="T125">šittalʼe</ts>
                  <nts id="Seg_468" n="HIAT:ip">.</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_471" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_473" n="HIAT:w" s="T126">Ɔːtam</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_476" n="HIAT:w" s="T127">ɨla</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_479" n="HIAT:w" s="T128">muntɨk</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_482" n="HIAT:w" s="T129">šottɨ</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_485" n="HIAT:w" s="T130">qənkɨjoıːmpɔːtɨn</ts>
                  <nts id="Seg_486" n="HIAT:ip">.</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_489" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_491" n="HIAT:w" s="T131">Qəlʼčʼaiːmɨn</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_494" n="HIAT:w" s="T132">muntɨk</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_497" n="HIAT:w" s="T133">qumiːsa</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_500" n="HIAT:w" s="T134">šotqɨn</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_503" n="HIAT:w" s="T135">korakkɔːtɨn</ts>
                  <nts id="Seg_504" n="HIAT:ip">.</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_507" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_509" n="HIAT:w" s="T136">Aj</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_512" n="HIAT:w" s="T137">na</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_515" n="HIAT:w" s="T138">ɔːtamtɨ</ts>
                  <nts id="Seg_516" n="HIAT:ip">,</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_519" n="HIAT:w" s="T139">surɨčʼa</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_522" n="HIAT:w" s="T140">qumɨn</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_525" n="HIAT:w" s="T141">iptäqɨn</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_528" n="HIAT:w" s="T142">muntɨk</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_530" n="HIAT:ip">(</nts>
                  <ts e="T144" id="Seg_532" n="HIAT:w" s="T143">nəkɨrɨlʼ-</ts>
                  <nts id="Seg_533" n="HIAT:ip">)</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_536" n="HIAT:w" s="T144">nəkɨrɨlʼimpɔːtɨn</ts>
                  <nts id="Seg_537" n="HIAT:ip">.</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_540" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_542" n="HIAT:w" s="T145">Kutɨ</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_545" n="HIAT:w" s="T146">kušša</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_548" n="HIAT:w" s="T147">ɔːta</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_551" n="HIAT:w" s="T148">minmɨntɨtɨ</ts>
                  <nts id="Seg_552" n="HIAT:ip">.</nts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_555" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_557" n="HIAT:w" s="T149">Natqa</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_560" n="HIAT:w" s="T150">nəqirkɨjoɨmpɔːtɨn</ts>
                  <nts id="Seg_561" n="HIAT:ip">.</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_564" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_566" n="HIAT:w" s="T151">Štobɨ</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_569" n="HIAT:w" s="T152">šente</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_572" n="HIAT:w" s="T153">poːqɨntɨ</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_575" n="HIAT:w" s="T154">promhoz</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_578" n="HIAT:w" s="T155">ɔːtam</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_581" n="HIAT:w" s="T156">tɔːqɨltɨmpɨkkijoıːmpɔːtɨn</ts>
                  <nts id="Seg_582" n="HIAT:ip">.</nts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_585" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_587" n="HIAT:w" s="T157">Šittalä</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_590" n="HIAT:w" s="T158">ɔːta</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_593" n="HIAT:w" s="T159">muntɨk</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_596" n="HIAT:w" s="T160">qälla</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_599" n="HIAT:w" s="T161">pula</ts>
                  <nts id="Seg_600" n="HIAT:ip">,</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_603" n="HIAT:w" s="T162">šente</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_606" n="HIAT:w" s="T163">poːqɨntɨ</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_609" n="HIAT:w" s="T164">promhoz</ts>
                  <nts id="Seg_610" n="HIAT:ip">,</nts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_613" n="HIAT:w" s="T165">nɨmtɨ</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_616" n="HIAT:w" s="T166">lapqaqɨn</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_619" n="HIAT:w" s="T167">ɔːtam</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_622" n="HIAT:w" s="T168">muntɨ</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_625" n="HIAT:w" s="T169">nʼantɨ</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_628" n="HIAT:w" s="T170">tɔːqɨltɨmpɨkkijoıːmpɔːtɨn</ts>
                  <nts id="Seg_629" n="HIAT:ip">.</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_632" n="HIAT:u" s="T171">
                  <nts id="Seg_633" n="HIAT:ip">(</nts>
                  <ts e="T172" id="Seg_635" n="HIAT:w" s="T171">Mɨta</ts>
                  <nts id="Seg_636" n="HIAT:ip">)</nts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_639" n="HIAT:w" s="T172">kuššaj</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_641" n="HIAT:ip">(</nts>
                  <nts id="Seg_642" n="HIAT:ip">(</nts>
                  <ats e="T174" id="Seg_643" n="HIAT:non-pho" s="T173">…</ats>
                  <nts id="Seg_644" n="HIAT:ip">)</nts>
                  <nts id="Seg_645" n="HIAT:ip">)</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_648" n="HIAT:w" s="T174">ɔːta</ts>
                  <nts id="Seg_649" n="HIAT:ip">,</nts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_652" n="HIAT:w" s="T175">a</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_655" n="HIAT:w" s="T176">kuššaj</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_657" n="HIAT:ip">(</nts>
                  <nts id="Seg_658" n="HIAT:ip">(</nts>
                  <ats e="T178" id="Seg_659" n="HIAT:non-pho" s="T177">…</ats>
                  <nts id="Seg_660" n="HIAT:ip">)</nts>
                  <nts id="Seg_661" n="HIAT:ip">)</nts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_664" n="HIAT:w" s="T178">ɔːta</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_667" n="HIAT:w" s="T179">čʼäŋɨntɨ</ts>
                  <nts id="Seg_668" n="HIAT:ip">.</nts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T180" id="Seg_670" n="sc" s="T0">
               <ts e="T1" id="Seg_672" n="e" s="T0">Рассказ </ts>
               <ts e="T2" id="Seg_674" n="e" s="T1">про </ts>
               <ts e="T3" id="Seg_676" n="e" s="T2">пастухов </ts>
               <ts e="T4" id="Seg_678" n="e" s="T3">пуровского </ts>
               <ts e="T5" id="Seg_680" n="e" s="T4">района. </ts>
               <ts e="T6" id="Seg_682" n="e" s="T5">Оленеводов. </ts>
               <ts e="T7" id="Seg_684" n="e" s="T6">Šettɨrqɨt </ts>
               <ts e="T8" id="Seg_686" n="e" s="T7">sɨrčʼip </ts>
               <ts e="T9" id="Seg_688" n="e" s="T8">säptaıːptäqɨn </ts>
               <ts e="T10" id="Seg_690" n="e" s="T9">teštɨm </ts>
               <ts e="T11" id="Seg_692" n="e" s="T10">ɔːtaiːmtɨ </ts>
               <ts e="T12" id="Seg_694" n="e" s="T11">muntik </ts>
               <ts e="T13" id="Seg_696" n="e" s="T12">taqqɨlʼeıːmpɔːtɨn. </ts>
               <ts e="T14" id="Seg_698" n="e" s="T13">Ɔːtaiːmtɨ </ts>
               <ts e="T15" id="Seg_700" n="e" s="T14">taqqɨlla </ts>
               <ts e="T16" id="Seg_702" n="e" s="T15">teštɨm </ts>
               <ts e="T17" id="Seg_704" n="e" s="T16">ɔːtamtɨ </ts>
               <ts e="T18" id="Seg_706" n="e" s="T17">((…)) </ts>
               <ts e="T19" id="Seg_708" n="e" s="T18">qaralʼ </ts>
               <ts e="T20" id="Seg_710" n="e" s="T19">taqqɨlʼkɨjoıːmpɔːtɨn. </ts>
               <ts e="T21" id="Seg_712" n="e" s="T20">To </ts>
               <ts e="T22" id="Seg_714" n="e" s="T21">toqqɨla </ts>
               <ts e="T23" id="Seg_716" n="e" s="T22">ɔːtaiːmtɨ </ts>
               <ts e="T24" id="Seg_718" n="e" s="T23">qaralʼqɨn </ts>
               <ts e="T25" id="Seg_720" n="e" s="T24">(čʼaqɨ). </ts>
               <ts e="T26" id="Seg_722" n="e" s="T25">Ukkɨr </ts>
               <ts e="T27" id="Seg_724" n="e" s="T26">mɨqɨn </ts>
               <ts e="T28" id="Seg_726" n="e" s="T27">mərqəıːmpɔːtɨn. </ts>
               <ts e="T29" id="Seg_728" n="e" s="T28">Čʼektɨtɨm </ts>
               <ts e="T30" id="Seg_730" n="e" s="T29">(ketɨlʼtɨkɨjoıːmpɔːtɨn) </ts>
               <ts e="T31" id="Seg_732" n="e" s="T30">ɔːttɨla. </ts>
               <ts e="T32" id="Seg_734" n="e" s="T31">Ɔːtɨm </ts>
               <ts e="T33" id="Seg_736" n="e" s="T32">kəːtɨmpɨla </ts>
               <ts e="T34" id="Seg_738" n="e" s="T33">pula </ts>
               <ts e="T35" id="Seg_740" n="e" s="T34">nänɨqa </ts>
               <ts e="T36" id="Seg_742" n="e" s="T35">məšeıːptäqin </ts>
               <ts e="T37" id="Seg_744" n="e" s="T36">ɔːtaiːmtɨ </ts>
               <ts e="T38" id="Seg_746" n="e" s="T37">purkɨsa </ts>
               <ts e="T39" id="Seg_748" n="e" s="T38">mɨrɨkoıːmpɔːtɨn. </ts>
               <ts e="T40" id="Seg_750" n="e" s="T39">Tam </ts>
               <ts e="T41" id="Seg_752" n="e" s="T40">Pürqɨn. </ts>
               <ts e="T42" id="Seg_754" n="e" s="T41">Ɔːm </ts>
               <ts e="T43" id="Seg_756" n="e" s="T42">ɔːtaiːmɨn </ts>
               <ts e="T44" id="Seg_758" n="e" s="T43">na </ts>
               <ts e="T45" id="Seg_760" n="e" s="T44">nänɨqaqɨn </ts>
               <ts e="T46" id="Seg_762" n="e" s="T45">čʼaqɨ </ts>
               <ts e="T47" id="Seg_764" n="e" s="T46">((…)) </ts>
               <ts e="T48" id="Seg_766" n="e" s="T47">Tolʼkɨntɨ </ts>
               <ts e="T49" id="Seg_768" n="e" s="T48">paktɨkoıːmpɔːtɨn. </ts>
               <ts e="T50" id="Seg_770" n="e" s="T49">Taplʼaqɨn </ts>
               <ts e="T51" id="Seg_772" n="e" s="T50">čʼaq </ts>
               <ts e="T52" id="Seg_774" n="e" s="T51">paktɔːtɨn. </ts>
               <ts e="T53" id="Seg_776" n="e" s="T52">Təpɨt </ts>
               <ts e="T54" id="Seg_778" n="e" s="T53">tüla, </ts>
               <ts e="T55" id="Seg_780" n="e" s="T54">aj </ts>
               <ts e="T56" id="Seg_782" n="e" s="T55">sukkulʼta </ts>
               <ts e="T57" id="Seg_784" n="e" s="T56">tokkoıːmpɔːtɨn </ts>
               <ts e="T58" id="Seg_786" n="e" s="T57">Toː </ts>
               <ts e="T59" id="Seg_788" n="e" s="T58">paktɨlʼa </ts>
               <ts e="T60" id="Seg_790" n="e" s="T59">šʼittelʼa, </ts>
               <ts e="T61" id="Seg_792" n="e" s="T60">toj </ts>
               <ts e="T62" id="Seg_794" n="e" s="T61">pɨlaqɨn </ts>
               <ts e="T63" id="Seg_796" n="e" s="T62">Pürqɨn </ts>
               <ts e="T64" id="Seg_798" n="e" s="T63">ɔːtaiːmtɨ </ts>
               <ts e="T65" id="Seg_800" n="e" s="T64">čʼaqɨ </ts>
               <ts e="T66" id="Seg_802" n="e" s="T65">nɨmtɨ </ts>
               <ts e="T67" id="Seg_804" n="e" s="T66">čʼaqɨ </ts>
               <ts e="T68" id="Seg_806" n="e" s="T67">qälɨmpɔːtɨn. </ts>
               <ts e="T69" id="Seg_808" n="e" s="T68">Ɔːtalʼ </ts>
               <ts e="T70" id="Seg_810" n="e" s="T69">ta </ts>
               <ts e="T71" id="Seg_812" n="e" s="T70">tɨpot </ts>
               <ts e="T72" id="Seg_814" n="e" s="T71">kočʼet </ts>
               <ts e="T73" id="Seg_816" n="e" s="T72">ɛːqan, </ts>
               <ts e="T74" id="Seg_818" n="e" s="T73">ɔːtaj </ts>
               <ts e="T75" id="Seg_820" n="e" s="T74">mɔːt </ts>
               <ts e="T76" id="Seg_822" n="e" s="T75">(mekijoıːmpɔːtɨn). </ts>
               <ts e="T77" id="Seg_824" n="e" s="T76">Ɔːtan </ts>
               <ts e="T78" id="Seg_826" n="e" s="T77">ɔːqɨn </ts>
               <ts e="T79" id="Seg_828" n="e" s="T78">nɨmtɨ </ts>
               <ts e="T80" id="Seg_830" n="e" s="T79">(čʼiqəlʼkijoıːmpɔːtɨn). </ts>
               <ts e="T81" id="Seg_832" n="e" s="T80">Ɔːtɨ </ts>
               <ts e="T82" id="Seg_834" n="e" s="T81">samoj </ts>
               <ts e="T83" id="Seg_836" n="e" s="T82">koštɨ </ts>
               <ts e="T84" id="Seg_838" n="e" s="T83">vremʼatɨ </ts>
               <ts e="T85" id="Seg_840" n="e" s="T84">ɛːqan </ts>
               <ts e="T86" id="Seg_842" n="e" s="T85">šittɨ </ts>
               <ts e="T87" id="Seg_844" n="e" s="T86">iraqıtɨ. </ts>
               <ts e="T88" id="Seg_846" n="e" s="T87">İjulʼ. </ts>
               <ts e="T89" id="Seg_848" n="e" s="T88">İjulʼ </ts>
               <ts e="T90" id="Seg_850" n="e" s="T89">mesʼats </ts>
               <ts e="T91" id="Seg_852" n="e" s="T90">ɛːqan. </ts>
               <ts e="T92" id="Seg_854" n="e" s="T91">Aj </ts>
               <ts e="T93" id="Seg_856" n="e" s="T92">avgust. </ts>
               <ts e="T94" id="Seg_858" n="e" s="T93">Aj </ts>
               <ts e="T95" id="Seg_860" n="e" s="T94">ijunʼ. </ts>
               <ts e="T96" id="Seg_862" n="e" s="T95">Na </ts>
               <ts e="T97" id="Seg_864" n="e" s="T96">nɔːkɨr </ts>
               <ts e="T98" id="Seg_866" n="e" s="T97">iraqɨntɨ </ts>
               <ts e="T99" id="Seg_868" n="e" s="T98">taqɨn </ts>
               <ts e="T100" id="Seg_870" n="e" s="T99">twəttɨ </ts>
               <ts e="T101" id="Seg_872" n="e" s="T100">meːkkɨjoıːmpat. </ts>
               <ts e="T102" id="Seg_874" n="e" s="T101">Twəttɨ </ts>
               <ts e="T103" id="Seg_876" n="e" s="T102">nɨmtɨ </ts>
               <ts e="T104" id="Seg_878" n="e" s="T103">kɨrapoːqɨn </ts>
               <ts e="T105" id="Seg_880" n="e" s="T104">šettɨ </ts>
               <ts e="T106" id="Seg_882" n="e" s="T105">ropɨtɨkkɨjoıːmpɔːtɨn. </ts>
               <ts e="T107" id="Seg_884" n="e" s="T106">Qara </ts>
               <ts e="T108" id="Seg_886" n="e" s="T107">montɨqɨn </ts>
               <ts e="T109" id="Seg_888" n="e" s="T108">qənnaıːtäqɨn, </ts>
               <ts e="T110" id="Seg_890" n="e" s="T109">kočʼe </ts>
               <ts e="T111" id="Seg_892" n="e" s="T110">ɔːtaiːmtɨ </ts>
               <ts e="T112" id="Seg_894" n="e" s="T111">nʼentɨ </ts>
               <ts e="T113" id="Seg_896" n="e" s="T112">taqqɨloıːmpɔːtɨn. </ts>
               <ts e="T114" id="Seg_898" n="e" s="T113">Ɔːtaiːmtɨ </ts>
               <ts e="T115" id="Seg_900" n="e" s="T114">keksa </ts>
               <ts e="T116" id="Seg_902" n="e" s="T115">muntɨk, </ts>
               <ts e="T117" id="Seg_904" n="e" s="T116">nʼentɨ </ts>
               <ts e="T118" id="Seg_906" n="e" s="T117">taqqilʼlʼa </ts>
               <ts e="T119" id="Seg_908" n="e" s="T118">tüntɔːtɨn. </ts>
               <ts e="T120" id="Seg_910" n="e" s="T119">Ohotnʼiki</ts>
               <ts e="T121" id="Seg_912" n="e" s="T120">nɨmtɨ </ts>
               <ts e="T122" id="Seg_914" n="e" s="T121">ɔːtam </ts>
               <ts e="T123" id="Seg_916" n="e" s="T122">miːkkijoıːmpɔːtɨn. </ts>
               <ts e="T124" id="Seg_918" n="e" s="T123">Surɨčʼčʼɨj </ts>
               <ts e="T125" id="Seg_920" n="e" s="T124">qumɨn </ts>
               <ts e="T126" id="Seg_922" n="e" s="T125">šittalʼe. </ts>
               <ts e="T127" id="Seg_924" n="e" s="T126">Ɔːtam </ts>
               <ts e="T128" id="Seg_926" n="e" s="T127">ɨla </ts>
               <ts e="T129" id="Seg_928" n="e" s="T128">muntɨk </ts>
               <ts e="T130" id="Seg_930" n="e" s="T129">šottɨ </ts>
               <ts e="T131" id="Seg_932" n="e" s="T130">qənkɨjoıːmpɔːtɨn. </ts>
               <ts e="T132" id="Seg_934" n="e" s="T131">Qəlʼčʼaiːmɨn </ts>
               <ts e="T133" id="Seg_936" n="e" s="T132">muntɨk </ts>
               <ts e="T134" id="Seg_938" n="e" s="T133">qumiːsa </ts>
               <ts e="T135" id="Seg_940" n="e" s="T134">šotqɨn </ts>
               <ts e="T136" id="Seg_942" n="e" s="T135">korakkɔːtɨn. </ts>
               <ts e="T137" id="Seg_944" n="e" s="T136">Aj </ts>
               <ts e="T138" id="Seg_946" n="e" s="T137">na </ts>
               <ts e="T139" id="Seg_948" n="e" s="T138">ɔːtamtɨ, </ts>
               <ts e="T140" id="Seg_950" n="e" s="T139">surɨčʼa </ts>
               <ts e="T141" id="Seg_952" n="e" s="T140">qumɨn </ts>
               <ts e="T142" id="Seg_954" n="e" s="T141">iptäqɨn </ts>
               <ts e="T143" id="Seg_956" n="e" s="T142">muntɨk </ts>
               <ts e="T144" id="Seg_958" n="e" s="T143">(nəkɨrɨlʼ-) </ts>
               <ts e="T145" id="Seg_960" n="e" s="T144">nəkɨrɨlʼimpɔːtɨn. </ts>
               <ts e="T146" id="Seg_962" n="e" s="T145">Kutɨ </ts>
               <ts e="T147" id="Seg_964" n="e" s="T146">kušša </ts>
               <ts e="T148" id="Seg_966" n="e" s="T147">ɔːta </ts>
               <ts e="T149" id="Seg_968" n="e" s="T148">minmɨntɨtɨ. </ts>
               <ts e="T150" id="Seg_970" n="e" s="T149">Natqa </ts>
               <ts e="T151" id="Seg_972" n="e" s="T150">nəqirkɨjoɨmpɔːtɨn. </ts>
               <ts e="T152" id="Seg_974" n="e" s="T151">Štobɨ </ts>
               <ts e="T153" id="Seg_976" n="e" s="T152">šente </ts>
               <ts e="T154" id="Seg_978" n="e" s="T153">poːqɨntɨ </ts>
               <ts e="T155" id="Seg_980" n="e" s="T154">promhoz </ts>
               <ts e="T156" id="Seg_982" n="e" s="T155">ɔːtam </ts>
               <ts e="T157" id="Seg_984" n="e" s="T156">tɔːqɨltɨmpɨkkijoıːmpɔːtɨn. </ts>
               <ts e="T158" id="Seg_986" n="e" s="T157">Šittalä </ts>
               <ts e="T159" id="Seg_988" n="e" s="T158">ɔːta </ts>
               <ts e="T160" id="Seg_990" n="e" s="T159">muntɨk </ts>
               <ts e="T161" id="Seg_992" n="e" s="T160">qälla </ts>
               <ts e="T162" id="Seg_994" n="e" s="T161">pula, </ts>
               <ts e="T163" id="Seg_996" n="e" s="T162">šente </ts>
               <ts e="T164" id="Seg_998" n="e" s="T163">poːqɨntɨ </ts>
               <ts e="T165" id="Seg_1000" n="e" s="T164">promhoz, </ts>
               <ts e="T166" id="Seg_1002" n="e" s="T165">nɨmtɨ </ts>
               <ts e="T167" id="Seg_1004" n="e" s="T166">lapqaqɨn </ts>
               <ts e="T168" id="Seg_1006" n="e" s="T167">ɔːtam </ts>
               <ts e="T169" id="Seg_1008" n="e" s="T168">muntɨ </ts>
               <ts e="T170" id="Seg_1010" n="e" s="T169">nʼantɨ </ts>
               <ts e="T171" id="Seg_1012" n="e" s="T170">tɔːqɨltɨmpɨkkijoıːmpɔːtɨn. </ts>
               <ts e="T172" id="Seg_1014" n="e" s="T171">(Mɨta) </ts>
               <ts e="T173" id="Seg_1016" n="e" s="T172">kuššaj </ts>
               <ts e="T174" id="Seg_1018" n="e" s="T173">((…)) </ts>
               <ts e="T175" id="Seg_1020" n="e" s="T174">ɔːta, </ts>
               <ts e="T176" id="Seg_1022" n="e" s="T175">a </ts>
               <ts e="T177" id="Seg_1024" n="e" s="T176">kuššaj </ts>
               <ts e="T178" id="Seg_1026" n="e" s="T177">((…)) </ts>
               <ts e="T179" id="Seg_1028" n="e" s="T178">ɔːta </ts>
               <ts e="T180" id="Seg_1030" n="e" s="T179">čʼäŋɨntɨ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_1031" s="T0">KNM_196X_Deer_nar.001 (001)</ta>
            <ta e="T6" id="Seg_1032" s="T5">KNM_196X_Deer_nar.002 (002)</ta>
            <ta e="T13" id="Seg_1033" s="T6">KNM_196X_Deer_nar.003 (003)</ta>
            <ta e="T20" id="Seg_1034" s="T13">KNM_196X_Deer_nar.004 (004)</ta>
            <ta e="T25" id="Seg_1035" s="T20">KNM_196X_Deer_nar.005 (005)</ta>
            <ta e="T28" id="Seg_1036" s="T25">KNM_196X_Deer_nar.006 (006)</ta>
            <ta e="T31" id="Seg_1037" s="T28">KNM_196X_Deer_nar.007 (007)</ta>
            <ta e="T39" id="Seg_1038" s="T31">KNM_196X_Deer_nar.008 (008)</ta>
            <ta e="T41" id="Seg_1039" s="T39">KNM_196X_Deer_nar.009 (009)</ta>
            <ta e="T49" id="Seg_1040" s="T41">KNM_196X_Deer_nar.010 (010)</ta>
            <ta e="T52" id="Seg_1041" s="T49">KNM_196X_Deer_nar.011 (011)</ta>
            <ta e="T57" id="Seg_1042" s="T52">KNM_196X_Deer_nar.012 (012)</ta>
            <ta e="T68" id="Seg_1043" s="T57">KNM_196X_Deer_nar.013 (013)</ta>
            <ta e="T76" id="Seg_1044" s="T68">KNM_196X_Deer_nar.014 (014)</ta>
            <ta e="T80" id="Seg_1045" s="T76">KNM_196X_Deer_nar.015 (015)</ta>
            <ta e="T87" id="Seg_1046" s="T80">KNM_196X_Deer_nar.016 (016)</ta>
            <ta e="T88" id="Seg_1047" s="T87">KNM_196X_Deer_nar.017 (017)</ta>
            <ta e="T91" id="Seg_1048" s="T88">KNM_196X_Deer_nar.018 (018)</ta>
            <ta e="T93" id="Seg_1049" s="T91">KNM_196X_Deer_nar.019 (019)</ta>
            <ta e="T95" id="Seg_1050" s="T93">KNM_196X_Deer_nar.020 (020)</ta>
            <ta e="T101" id="Seg_1051" s="T95">KNM_196X_Deer_nar.021 (021)</ta>
            <ta e="T106" id="Seg_1052" s="T101">KNM_196X_Deer_nar.022 (022)</ta>
            <ta e="T113" id="Seg_1053" s="T106">KNM_196X_Deer_nar.023 (023)</ta>
            <ta e="T119" id="Seg_1054" s="T113">KNM_196X_Deer_nar.024 (024)</ta>
            <ta e="T123" id="Seg_1055" s="T119">KNM_196X_Deer_nar.025 (025)</ta>
            <ta e="T126" id="Seg_1056" s="T123">KNM_196X_Deer_nar.026 (026)</ta>
            <ta e="T131" id="Seg_1057" s="T126">KNM_196X_Deer_nar.027 (027)</ta>
            <ta e="T136" id="Seg_1058" s="T131">KNM_196X_Deer_nar.028 (028)</ta>
            <ta e="T145" id="Seg_1059" s="T136">KNM_196X_Deer_nar.029 (029)</ta>
            <ta e="T149" id="Seg_1060" s="T145">KNM_196X_Deer_nar.030 (030)</ta>
            <ta e="T151" id="Seg_1061" s="T149">KNM_196X_Deer_nar.031 (031)</ta>
            <ta e="T157" id="Seg_1062" s="T151">KNM_196X_Deer_nar.032 (032)</ta>
            <ta e="T171" id="Seg_1063" s="T157">KNM_196X_Deer_nar.033 (033)</ta>
            <ta e="T180" id="Seg_1064" s="T171">KNM_196X_Deer_nar.034 (034)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_1065" s="T0">Рассказ про пастухов Пуровского района.</ta>
            <ta e="T6" id="Seg_1066" s="T5">Оленеводов.</ta>
            <ta e="T13" id="Seg_1067" s="T6">šʼettɨrqɨt sɨrčʼip säptaıːptäqɨn teštɨm ɔːtaiːmtɨ muntik taqqɨlʼeıːmpɔː(tɨn)</ta>
            <ta e="T20" id="Seg_1068" s="T13">ɔːtaıːmtɨ taqqɨla teštɨm ɔːtamtɨ qaralʼ toqɨlʼkɨjoıːmpɔːtɨn</ta>
            <ta e="T25" id="Seg_1069" s="T20">to toqqɨla ɔːtaıːmtɨ qaralʼqɨn čʼaqɨ</ta>
            <ta e="T28" id="Seg_1070" s="T25">ukkɨr mɨqɨn mərqəıːmpɔːtɨn </ta>
            <ta e="T31" id="Seg_1071" s="T28">čʼektɨtɨm ketɨlʼtɨkɨjoıːmpɔːtɨn ɔːttɨla</ta>
            <ta e="T39" id="Seg_1072" s="T31">ɔːtɨm kətɨmpɨla pula nänɨqa məšeıːptäqin ɔːtaıːmtɨ purkɨsa mɨrɨkoıːmpɔːtɨn</ta>
            <ta e="T41" id="Seg_1073" s="T39">tam pürqɨn</ta>
            <ta e="T49" id="Seg_1074" s="T41">om ɔːtaıːmɨn na nänɨqaqɨn čʼaqɨ tolʼkɨntɨ paktɨkoıːmpɔːtɨn</ta>
            <ta e="T52" id="Seg_1075" s="T49">taplʼaqɨn čʼaq paktɨmpɔːtɨn</ta>
            <ta e="T57" id="Seg_1076" s="T52">təpɨt tüla aj sukkulʼta tokkoıːmpɔːtɨn</ta>
            <ta e="T68" id="Seg_1077" s="T57">to paktɨlʼa šʼittelʼa toj pɨlaqɨn pürqɨn ɔːtaıːmtɨ čʼaqɨ nɨm(tɨ) čʼa(qɨ) qälɨmpɔːtɨn</ta>
            <ta e="T76" id="Seg_1078" s="T68">ɔːtalʼ tatɨpot kočʼet ɛːqan ɔːtaj mɔːt mekijoıːmpɔːtɨn</ta>
            <ta e="T80" id="Seg_1079" s="T76">ɔːtan ɔːqɨn nɨmtɨ čʼiqəlʼkijoıːmpɔːtɨn </ta>
            <ta e="T87" id="Seg_1080" s="T80">ɔːtɨ samoj koštɨ vremʼatɨ ɛqan šittɨ iraqitɨ</ta>
            <ta e="T88" id="Seg_1081" s="T87">ijulʼ</ta>
            <ta e="T91" id="Seg_1082" s="T88">ijulʼ mesʼats eqan</ta>
            <ta e="T93" id="Seg_1083" s="T91">aj avgust</ta>
            <ta e="T95" id="Seg_1084" s="T93">aj ijunʼ</ta>
            <ta e="T101" id="Seg_1085" s="T95">na nɔːkɨr iraqɨntɨ taqɨn twəttɨ meːkkɨjoıːmpat</ta>
            <ta e="T106" id="Seg_1086" s="T101">wəttɨ nɨmtɨ kɨrapoːqɨn šʼettɨ ropɨtɨkkɨjoıːmpɔːt(ɨn) </ta>
            <ta e="T113" id="Seg_1087" s="T106">qara montɨqɨn qənnaıːtäqɨn kočʼe ɔːtaıːmtɨ nʼentɨ taqqɨloıːmpɔːt(ɨn) </ta>
            <ta e="T119" id="Seg_1088" s="T113">ɔːtaıːmtɨ keksa muntɨk nʼentɨ takkilʼa tüntɔːtɨn</ta>
            <ta e="T123" id="Seg_1089" s="T119">ohotnʼiki nɨmtɨ ɔːtam miːkkijoıːmpɔːt(ɨn)</ta>
            <ta e="T126" id="Seg_1090" s="T123">surčʼitɨj qumɨn šʼittalʼe</ta>
            <ta e="T131" id="Seg_1091" s="T126">ɔːtam ɨla muntɨk šʼottɨ qənkɨjoıːmpɔːt(ɨn)</ta>
            <ta e="T136" id="Seg_1092" s="T131">qəlʼčʼaıːmɨn muntɨk qumisa šʼotqɨn koraqɔːtɨn </ta>
            <ta e="T145" id="Seg_1093" s="T136">aj na ɔːtamtɨ surɨčʼa qumɨn iptäqɨn muntɨk (nəkɨrɨlʼ) nəkɨrɨlʼimpɔːtɨn</ta>
            <ta e="T149" id="Seg_1094" s="T145">kutɨ kušša ɔːta minmɨntɨtɨ</ta>
            <ta e="T151" id="Seg_1095" s="T149">natqa nəqirkɨjoɨmpɔːtɨn</ta>
            <ta e="T157" id="Seg_1096" s="T151">štobɨ šʼente poːqɨntɨ promhoz ɔːtam tɔqqɨlʼtɨmpɨkkijoıːmpɔːtɨn</ta>
            <ta e="T171" id="Seg_1097" s="T157">šʼittalä ɔːta muntɨk qɛla pula šʼente poqɨntɨ promhoz nɨmtɨ lapqaqɨn ɔːtam muntɨ nʼantɨ taqqila tɔqqɨlʼtɨmpɨkkijoıːmpɔːt(ɨn)</ta>
            <ta e="T180" id="Seg_1098" s="T171">mɨta kuššaj ɔːta, kuššaj ɔːta čʼäŋɨntɨ</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_1099" s="T0">Рассказ про пастухов пуровского района. </ta>
            <ta e="T6" id="Seg_1100" s="T5">Оленеводов. </ta>
            <ta e="T13" id="Seg_1101" s="T6">Šettɨrqɨt sɨrčʼip säptaıːptäqɨn teštɨm ɔːtaiːmtɨ muntik taqqɨlʼeıːmpɔːtɨn. </ta>
            <ta e="T20" id="Seg_1102" s="T13">Ɔːtaiːmtɨ taqqɨlla teštɨm ɔːtamtɨ ((…)) qaralʼ taqqɨlʼkɨjoıːmpɔːtɨn. </ta>
            <ta e="T25" id="Seg_1103" s="T20">To toqqɨla ɔːtaiːmtɨ qaralʼqɨn (čʼaqɨ). </ta>
            <ta e="T28" id="Seg_1104" s="T25">Ukkɨr mɨqɨn mərqəıːmpɔːtɨn. </ta>
            <ta e="T31" id="Seg_1105" s="T28">Čʼektɨtɨm (ketɨlʼtɨkɨjoıːmpɔːtɨn) ɔːttɨla. </ta>
            <ta e="T39" id="Seg_1106" s="T31">Ɔːtɨm kəːtɨmpɨla pula nänɨqa məšeıːptäqin ɔːtaiːmtɨ purkɨsa mɨrɨkoıːmpɔːtɨn. </ta>
            <ta e="T41" id="Seg_1107" s="T39">Tam Pürqɨn. </ta>
            <ta e="T49" id="Seg_1108" s="T41">Ɔːm ɔːtaiːmɨn na nänɨqaqɨn čʼaqɨ ((…)) Tolʼkɨntɨ paktɨkoıːmpɔːtɨn. </ta>
            <ta e="T52" id="Seg_1109" s="T49">Taplʼaqɨn čʼaq paktɔːtɨn. </ta>
            <ta e="T57" id="Seg_1110" s="T52">Təpɨt tüla, aj sukkulʼta tokkoıːmpɔːtɨn </ta>
            <ta e="T68" id="Seg_1111" s="T57">Toː paktɨlʼa šʼittelʼa, toj pɨlaqɨn Pürqɨn ɔːtaiːmtɨ čʼaqɨ nɨmtɨ čʼaqɨ qälɨmpɔːtɨn. </ta>
            <ta e="T76" id="Seg_1112" s="T68">Ɔːtalʼ ta tɨpot kočʼet ɛːqan, ɔːtaj mɔːt (mekijoıːmpɔːtɨn). </ta>
            <ta e="T80" id="Seg_1113" s="T76">Ɔːtan ɔːqɨn nɨmtɨ (čʼiqəlʼkijoıːmpɔːtɨn). </ta>
            <ta e="T87" id="Seg_1114" s="T80">Ɔːtɨ samoj koštɨ vremʼatɨ ɛːqan šittɨ iraqıtɨ. </ta>
            <ta e="T88" id="Seg_1115" s="T87">İjulʼ. </ta>
            <ta e="T91" id="Seg_1116" s="T88">İjulʼ mesʼats ɛːqan. </ta>
            <ta e="T93" id="Seg_1117" s="T91">Aj avgust. </ta>
            <ta e="T95" id="Seg_1118" s="T93">Aj ijunʼ. </ta>
            <ta e="T101" id="Seg_1119" s="T95">Na nɔːkɨr iraqɨntɨ taqɨn twəttɨ meːkkɨjoıːmpat. </ta>
            <ta e="T106" id="Seg_1120" s="T101">Twəttɨ nɨmtɨ kɨrapoːqɨn šettɨ ropɨtɨkkɨjoıːmpɔːtɨn. </ta>
            <ta e="T113" id="Seg_1121" s="T106">Qara montɨqɨn qənnaıːtäqɨn, kočʼe ɔːtaiːmtɨ nʼentɨ taqqɨloıːmpɔːtɨn. </ta>
            <ta e="T119" id="Seg_1122" s="T113">Ɔːtaiːmtɨ keksa muntɨk, nʼentɨ taqqilʼlʼa tüntɔːtɨn. </ta>
            <ta e="T123" id="Seg_1123" s="T119">Ohotnʼiki nɨmtɨ ɔːtam miːkkijoıːmpɔːtɨn. </ta>
            <ta e="T126" id="Seg_1124" s="T123">Surɨčʼčʼɨj qumɨn šittalʼe. </ta>
            <ta e="T131" id="Seg_1125" s="T126">Ɔːtam ɨla muntɨk šottɨ qənkɨjoıːmpɔːtɨn. </ta>
            <ta e="T136" id="Seg_1126" s="T131">Qəlʼčʼaiːmɨn muntɨk qumiːsa šotqɨn korakkɔːtɨn. </ta>
            <ta e="T145" id="Seg_1127" s="T136">Aj na ɔːtamtɨ, surɨčʼa qumɨn iptäqɨn muntɨk (nəkɨrɨlʼ-) nəkɨrɨlʼimpɔːtɨn. </ta>
            <ta e="T149" id="Seg_1128" s="T145">Kutɨ kušša ɔːta minmɨntɨtɨ. </ta>
            <ta e="T151" id="Seg_1129" s="T149">Natqa nəqirkɨjoɨmpɔːtɨn. </ta>
            <ta e="T157" id="Seg_1130" s="T151">Štobɨ šente poːqɨntɨ promhoz ɔːtam tɔːqɨltɨmpɨkkijoıːmpɔːtɨn. </ta>
            <ta e="T171" id="Seg_1131" s="T157">Šittalä ɔːta muntɨk qälla pula, šente poːqɨntɨ promhoz, nɨmtɨ lapqaqɨn ɔːtam muntɨ nʼantɨ tɔːqɨltɨmpɨkkijoıːmpɔːtɨn. </ta>
            <ta e="T180" id="Seg_1132" s="T171">(Mɨta) kuššaj ((…)) ɔːta, a kuššaj ((…)) ɔːta čʼäŋɨntɨ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T7" id="Seg_1133" s="T6">šettɨr-qɨt</ta>
            <ta e="T8" id="Seg_1134" s="T7">sɨr-čʼip</ta>
            <ta e="T9" id="Seg_1135" s="T8">säp-t-aıː-ptä-qɨn</ta>
            <ta e="T10" id="Seg_1136" s="T9">teštɨm</ta>
            <ta e="T11" id="Seg_1137" s="T10">ɔːta-iː-m-tɨ</ta>
            <ta e="T12" id="Seg_1138" s="T11">muntik</ta>
            <ta e="T13" id="Seg_1139" s="T12">taq-qɨlʼ-eıː-mpɔː-tɨn</ta>
            <ta e="T14" id="Seg_1140" s="T13">ɔːta-iː-m-tɨ</ta>
            <ta e="T15" id="Seg_1141" s="T14">taq-qɨl-la</ta>
            <ta e="T16" id="Seg_1142" s="T15">teštɨm</ta>
            <ta e="T17" id="Seg_1143" s="T16">ɔːta-m-tɨ</ta>
            <ta e="T19" id="Seg_1144" s="T18">qaralʼ</ta>
            <ta e="T20" id="Seg_1145" s="T19">taq-qɨlʼ-kɨ-joıː-mpɔː-tɨn</ta>
            <ta e="T21" id="Seg_1146" s="T20">to</ta>
            <ta e="T22" id="Seg_1147" s="T21">toqqɨla</ta>
            <ta e="T23" id="Seg_1148" s="T22">ɔːta-iː-m-tɨ</ta>
            <ta e="T24" id="Seg_1149" s="T23">qaralʼ-qɨn</ta>
            <ta e="T25" id="Seg_1150" s="T24">čʼaqɨ</ta>
            <ta e="T26" id="Seg_1151" s="T25">ukkɨr</ta>
            <ta e="T27" id="Seg_1152" s="T26">mɨ-qɨn</ta>
            <ta e="T28" id="Seg_1153" s="T27">mərq-əıː-mpɔː-tɨn</ta>
            <ta e="T29" id="Seg_1154" s="T28">čʼektɨ-t-ɨ-m</ta>
            <ta e="T30" id="Seg_1155" s="T29">ketɨ-lʼtɨ-kɨ-joıː-mpɔː-tɨn</ta>
            <ta e="T31" id="Seg_1156" s="T30">ɔːt-tɨ-la</ta>
            <ta e="T32" id="Seg_1157" s="T31">ɔːtɨ-m</ta>
            <ta e="T33" id="Seg_1158" s="T32">kəːtɨ-mpɨ-la</ta>
            <ta e="T34" id="Seg_1159" s="T33">pula</ta>
            <ta e="T35" id="Seg_1160" s="T34">nänɨqa</ta>
            <ta e="T36" id="Seg_1161" s="T35">məš-eıː-ptä-qin</ta>
            <ta e="T37" id="Seg_1162" s="T36">ɔːta-iː-m-tɨ</ta>
            <ta e="T38" id="Seg_1163" s="T37">purkɨ-sa</ta>
            <ta e="T39" id="Seg_1164" s="T38">mɨrɨ-k-oıː-mpɔː-tɨn</ta>
            <ta e="T40" id="Seg_1165" s="T39">tam</ta>
            <ta e="T41" id="Seg_1166" s="T40">Pür-qɨn</ta>
            <ta e="T42" id="Seg_1167" s="T41">ɔːm</ta>
            <ta e="T43" id="Seg_1168" s="T42">ɔːta-iː-mɨn</ta>
            <ta e="T44" id="Seg_1169" s="T43">na</ta>
            <ta e="T45" id="Seg_1170" s="T44">nänɨqa-qɨn</ta>
            <ta e="T46" id="Seg_1171" s="T45">čʼaqɨ</ta>
            <ta e="T48" id="Seg_1172" s="T47">Tolʼkɨ-ntɨ</ta>
            <ta e="T49" id="Seg_1173" s="T48">paktɨ-k-oıː-mpɔː-tɨn</ta>
            <ta e="T50" id="Seg_1174" s="T49">taplʼa-qɨn</ta>
            <ta e="T51" id="Seg_1175" s="T50">čʼaq</ta>
            <ta e="T52" id="Seg_1176" s="T51">paktɔː-tɨn</ta>
            <ta e="T53" id="Seg_1177" s="T52">təp-ɨ-t</ta>
            <ta e="T54" id="Seg_1178" s="T53">tü-la</ta>
            <ta e="T55" id="Seg_1179" s="T54">aj</ta>
            <ta e="T56" id="Seg_1180" s="T55">sukkulʼta</ta>
            <ta e="T57" id="Seg_1181" s="T56">tok-k-oıː-mpɔː-tɨn</ta>
            <ta e="T58" id="Seg_1182" s="T57">toː</ta>
            <ta e="T59" id="Seg_1183" s="T58">paktɨ-lʼa</ta>
            <ta e="T60" id="Seg_1184" s="T59">šʼitte-lʼa</ta>
            <ta e="T61" id="Seg_1185" s="T60">to-j</ta>
            <ta e="T62" id="Seg_1186" s="T61">pɨla-qɨn</ta>
            <ta e="T63" id="Seg_1187" s="T62">Pür-qɨn</ta>
            <ta e="T64" id="Seg_1188" s="T63">ɔːta-iː-m-tɨ</ta>
            <ta e="T65" id="Seg_1189" s="T64">čʼaqɨ</ta>
            <ta e="T66" id="Seg_1190" s="T65">nɨmtɨ</ta>
            <ta e="T67" id="Seg_1191" s="T66">čʼaqɨ</ta>
            <ta e="T68" id="Seg_1192" s="T67">qäl-ɨ-mpɔː-tɨn</ta>
            <ta e="T69" id="Seg_1193" s="T68">ɔːta-lʼ</ta>
            <ta e="T70" id="Seg_1194" s="T69">ta</ta>
            <ta e="T71" id="Seg_1195" s="T70">tɨpo-t</ta>
            <ta e="T72" id="Seg_1196" s="T71">kočʼe-t</ta>
            <ta e="T73" id="Seg_1197" s="T72">ɛː-qa-n</ta>
            <ta e="T74" id="Seg_1198" s="T73">ɔːta-j</ta>
            <ta e="T75" id="Seg_1199" s="T74">mɔːt</ta>
            <ta e="T76" id="Seg_1200" s="T75">me-ki-joıː-mpɔː-tɨn</ta>
            <ta e="T77" id="Seg_1201" s="T76">ɔːta-n</ta>
            <ta e="T78" id="Seg_1202" s="T77">ɔː-qɨn</ta>
            <ta e="T79" id="Seg_1203" s="T78">nɨmtɨ</ta>
            <ta e="T80" id="Seg_1204" s="T79">*čʼiqə-lʼ-ki-joıː-mpɔː-tɨn</ta>
            <ta e="T81" id="Seg_1205" s="T80">ɔːtɨ</ta>
            <ta e="T82" id="Seg_1206" s="T81">samoj</ta>
            <ta e="T83" id="Seg_1207" s="T82">koštɨ</ta>
            <ta e="T84" id="Seg_1208" s="T83">vremʼa-tɨ</ta>
            <ta e="T85" id="Seg_1209" s="T84">ɛː-qa-n</ta>
            <ta e="T86" id="Seg_1210" s="T85">šittɨ</ta>
            <ta e="T87" id="Seg_1211" s="T86">ira-qı-tɨ</ta>
            <ta e="T88" id="Seg_1212" s="T87">ijulʼ</ta>
            <ta e="T89" id="Seg_1213" s="T88">ijulʼ</ta>
            <ta e="T90" id="Seg_1214" s="T89">mesʼats</ta>
            <ta e="T91" id="Seg_1215" s="T90">ɛː-qa-n</ta>
            <ta e="T92" id="Seg_1216" s="T91">aj</ta>
            <ta e="T93" id="Seg_1217" s="T92">avgust</ta>
            <ta e="T94" id="Seg_1218" s="T93">aj</ta>
            <ta e="T95" id="Seg_1219" s="T94">ijunʼ</ta>
            <ta e="T96" id="Seg_1220" s="T95">na</ta>
            <ta e="T97" id="Seg_1221" s="T96">nɔːkɨr</ta>
            <ta e="T98" id="Seg_1222" s="T97">ira-qɨn-tɨ</ta>
            <ta e="T99" id="Seg_1223" s="T98">taqɨ-n</ta>
            <ta e="T100" id="Seg_1224" s="T99">twəttɨ</ta>
            <ta e="T101" id="Seg_1225" s="T100">meː-kkɨ-joıː-mpa-t</ta>
            <ta e="T102" id="Seg_1226" s="T101">twəttɨ</ta>
            <ta e="T103" id="Seg_1227" s="T102">nɨmtɨ</ta>
            <ta e="T104" id="Seg_1228" s="T103">kɨrapoːqɨn</ta>
            <ta e="T105" id="Seg_1229" s="T104">šettɨ</ta>
            <ta e="T106" id="Seg_1230" s="T105">ropɨtɨ-kkɨ-joıː-mpɔː-tɨn</ta>
            <ta e="T107" id="Seg_1231" s="T106">qara</ta>
            <ta e="T108" id="Seg_1232" s="T107">montɨ-qɨn</ta>
            <ta e="T109" id="Seg_1233" s="T108">qənn-aıː-tä-qɨn</ta>
            <ta e="T110" id="Seg_1234" s="T109">kočʼe</ta>
            <ta e="T111" id="Seg_1235" s="T110">ɔːta-iː-m-tɨ</ta>
            <ta e="T112" id="Seg_1236" s="T111">nʼentɨ</ta>
            <ta e="T113" id="Seg_1237" s="T112">taq-qɨl-oıː-mpɔː-tɨn</ta>
            <ta e="T114" id="Seg_1238" s="T113">ɔːta-iː-m-tɨ</ta>
            <ta e="T115" id="Seg_1239" s="T114">keksa</ta>
            <ta e="T116" id="Seg_1240" s="T115">muntɨk</ta>
            <ta e="T117" id="Seg_1241" s="T116">nʼentɨ</ta>
            <ta e="T118" id="Seg_1242" s="T117">taq-qilʼ-lʼa</ta>
            <ta e="T119" id="Seg_1243" s="T118">tü-ntɔː-tɨn</ta>
            <ta e="T121" id="Seg_1244" s="T120">nɨmtɨ</ta>
            <ta e="T122" id="Seg_1245" s="T121">ɔːta-m</ta>
            <ta e="T123" id="Seg_1246" s="T122">miː-kki-joıː-mpɔː-tɨn. </ta>
            <ta e="T124" id="Seg_1247" s="T123">surɨčʼ-čʼɨj</ta>
            <ta e="T125" id="Seg_1248" s="T124">qum-ɨ-n</ta>
            <ta e="T126" id="Seg_1249" s="T125">šittalʼe</ta>
            <ta e="T127" id="Seg_1250" s="T126">ɔːta-m</ta>
            <ta e="T128" id="Seg_1251" s="T127">ɨla</ta>
            <ta e="T129" id="Seg_1252" s="T128">muntɨk</ta>
            <ta e="T130" id="Seg_1253" s="T129">šot-tɨ</ta>
            <ta e="T131" id="Seg_1254" s="T130">qən-kɨ-joıː-mpɔː-tɨn</ta>
            <ta e="T132" id="Seg_1255" s="T131">qəlʼčʼa-iː-mɨn</ta>
            <ta e="T133" id="Seg_1256" s="T132">muntɨk</ta>
            <ta e="T134" id="Seg_1257" s="T133">qum-iː-sa</ta>
            <ta e="T135" id="Seg_1258" s="T134">šot-qɨn</ta>
            <ta e="T136" id="Seg_1259" s="T135">kora-kkɔː-tɨn</ta>
            <ta e="T137" id="Seg_1260" s="T136">aj</ta>
            <ta e="T138" id="Seg_1261" s="T137">na</ta>
            <ta e="T139" id="Seg_1262" s="T138">ɔːta-m-tɨ</ta>
            <ta e="T140" id="Seg_1263" s="T139">surɨ-čʼa</ta>
            <ta e="T141" id="Seg_1264" s="T140">qum-ɨ-n</ta>
            <ta e="T142" id="Seg_1265" s="T141">i-ptä-qɨn</ta>
            <ta e="T143" id="Seg_1266" s="T142">muntɨk</ta>
            <ta e="T145" id="Seg_1267" s="T144">nəkɨ-r-ɨ-lʼi-mpɔː-tɨn</ta>
            <ta e="T146" id="Seg_1268" s="T145">kutɨ</ta>
            <ta e="T147" id="Seg_1269" s="T146">kušša</ta>
            <ta e="T148" id="Seg_1270" s="T147">ɔːta</ta>
            <ta e="T149" id="Seg_1271" s="T148">mi-nmɨ-ntɨ-tɨ</ta>
            <ta e="T150" id="Seg_1272" s="T149">natqa</ta>
            <ta e="T151" id="Seg_1273" s="T150">nəqi-r-kɨ-joɨ-mpɔː-tɨn</ta>
            <ta e="T152" id="Seg_1274" s="T151">štobɨ</ta>
            <ta e="T153" id="Seg_1275" s="T152">šente</ta>
            <ta e="T154" id="Seg_1276" s="T153">poː-qɨn-tɨ</ta>
            <ta e="T155" id="Seg_1277" s="T154">promhoz</ta>
            <ta e="T156" id="Seg_1278" s="T155">ɔːta-m</ta>
            <ta e="T157" id="Seg_1279" s="T156">tɔːqɨl-tɨ-mpɨ-kki-joıː-mpɔː-tɨn</ta>
            <ta e="T158" id="Seg_1280" s="T157">šittalä</ta>
            <ta e="T159" id="Seg_1281" s="T158">ɔːta</ta>
            <ta e="T160" id="Seg_1282" s="T159">muntɨk</ta>
            <ta e="T161" id="Seg_1283" s="T160">qäl-la</ta>
            <ta e="T162" id="Seg_1284" s="T161">pula</ta>
            <ta e="T163" id="Seg_1285" s="T162">šente</ta>
            <ta e="T164" id="Seg_1286" s="T163">poː-qɨn-tɨ</ta>
            <ta e="T165" id="Seg_1287" s="T164">promhoz</ta>
            <ta e="T166" id="Seg_1288" s="T165">nɨmtɨ</ta>
            <ta e="T167" id="Seg_1289" s="T166">lapqa-qɨn</ta>
            <ta e="T168" id="Seg_1290" s="T167">ɔːta-m</ta>
            <ta e="T169" id="Seg_1291" s="T168">muntɨ</ta>
            <ta e="T170" id="Seg_1292" s="T169">nʼantɨ</ta>
            <ta e="T171" id="Seg_1293" s="T170">tɔːqɨl-tɨ-mpɨ-kki-joıː-mpɔː-tɨn</ta>
            <ta e="T172" id="Seg_1294" s="T171">mɨta</ta>
            <ta e="T173" id="Seg_1295" s="T172">kušša-j</ta>
            <ta e="T175" id="Seg_1296" s="T174">ɔːta</ta>
            <ta e="T176" id="Seg_1297" s="T175">a</ta>
            <ta e="T177" id="Seg_1298" s="T176">kušša-j</ta>
            <ta e="T179" id="Seg_1299" s="T178">ɔːta</ta>
            <ta e="T180" id="Seg_1300" s="T179">čʼäŋɨ-ntɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T7" id="Seg_1301" s="T6">šettɨr-qɨn</ta>
            <ta e="T8" id="Seg_1302" s="T7">sɨrɨ-čʼip</ta>
            <ta e="T9" id="Seg_1303" s="T8">səpɨ-t-ıː-ptäː-qɨn</ta>
            <ta e="T10" id="Seg_1304" s="T9">teštɨm</ta>
            <ta e="T11" id="Seg_1305" s="T10">ɔːtä-iː-m-tɨ</ta>
            <ta e="T12" id="Seg_1306" s="T11">muntɨk</ta>
            <ta e="T13" id="Seg_1307" s="T12">*taqɨ-qɨl-ɛː-mpɨ-tɨt</ta>
            <ta e="T14" id="Seg_1308" s="T13">ɔːtä-iː-m-tɨ</ta>
            <ta e="T15" id="Seg_1309" s="T14">*taqɨ-qɨl-lä</ta>
            <ta e="T16" id="Seg_1310" s="T15">teštɨm</ta>
            <ta e="T17" id="Seg_1311" s="T16">ɔːtä-m-tɨ</ta>
            <ta e="T19" id="Seg_1312" s="T18">qaralʼ</ta>
            <ta e="T20" id="Seg_1313" s="T19">*taqɨ-qɨl-kkɨ-joıː-mpɨ-tɨt</ta>
            <ta e="T21" id="Seg_1314" s="T20">to</ta>
            <ta e="T22" id="Seg_1315" s="T21">toqqɨla</ta>
            <ta e="T23" id="Seg_1316" s="T22">ɔːtä-iː-m-tɨ</ta>
            <ta e="T24" id="Seg_1317" s="T23">qaralʼ-qɨn</ta>
            <ta e="T25" id="Seg_1318" s="T24">čʼaqɨ</ta>
            <ta e="T26" id="Seg_1319" s="T25">ukkɨr</ta>
            <ta e="T27" id="Seg_1320" s="T26">mɨ-qɨn</ta>
            <ta e="T28" id="Seg_1321" s="T27">wərkɨ-ɛː-mpɨ-tɨt</ta>
            <ta e="T29" id="Seg_1322" s="T28">čʼəktɨ-t-ɨ-m</ta>
            <ta e="T30" id="Seg_1323" s="T29">kəːtɨ-ltɨ-kkɨ-joıː-mpɨ-tɨt</ta>
            <ta e="T31" id="Seg_1324" s="T30">ətɨ-ntɨ-lä</ta>
            <ta e="T32" id="Seg_1325" s="T31">ɔːtä-m</ta>
            <ta e="T33" id="Seg_1326" s="T32">kəːtɨ-mpɨ-lä</ta>
            <ta e="T34" id="Seg_1327" s="T33">puːlä</ta>
            <ta e="T35" id="Seg_1328" s="T34">nɛnɨqa</ta>
            <ta e="T36" id="Seg_1329" s="T35">wəšɨ-ɛː-ptäː-qɨn</ta>
            <ta e="T37" id="Seg_1330" s="T36">ɔːtä-iː-m-tɨ</ta>
            <ta e="T38" id="Seg_1331" s="T37">purqɨ-sä</ta>
            <ta e="T39" id="Seg_1332" s="T38">mɨrɨ-kkɨ-joıː-mpɨ-tɨt</ta>
            <ta e="T40" id="Seg_1333" s="T39">tam</ta>
            <ta e="T41" id="Seg_1334" s="T40">Pür-qɨn</ta>
            <ta e="T42" id="Seg_1335" s="T41">ɔːmɨ</ta>
            <ta e="T43" id="Seg_1336" s="T42">ɔːtä-iː-mɨt</ta>
            <ta e="T44" id="Seg_1337" s="T43">na</ta>
            <ta e="T45" id="Seg_1338" s="T44">nɛnɨqa-qɨn</ta>
            <ta e="T46" id="Seg_1339" s="T45">čʼaqɨ</ta>
            <ta e="T48" id="Seg_1340" s="T47">Tolʼka-ntɨ</ta>
            <ta e="T49" id="Seg_1341" s="T48">paktɨ-kkɨ-joıː-mpɨ-tɨt</ta>
            <ta e="T50" id="Seg_1342" s="T49">*taplä-qɨn</ta>
            <ta e="T51" id="Seg_1343" s="T50">čʼaqɨ</ta>
            <ta e="T52" id="Seg_1344" s="T51">paktɨ-tɨt</ta>
            <ta e="T53" id="Seg_1345" s="T52">təp-ɨ-t</ta>
            <ta e="T54" id="Seg_1346" s="T53">tü-lä</ta>
            <ta e="T55" id="Seg_1347" s="T54">aj</ta>
            <ta e="T56" id="Seg_1348" s="T55">sukɨltä</ta>
            <ta e="T57" id="Seg_1349" s="T56">tok-kkɨ-joıː-mpɨ-tɨt</ta>
            <ta e="T58" id="Seg_1350" s="T57">toː</ta>
            <ta e="T59" id="Seg_1351" s="T58">paktɨ-lä</ta>
            <ta e="T60" id="Seg_1352" s="T59">šʼitte-lä</ta>
            <ta e="T61" id="Seg_1353" s="T60">to-lʼ</ta>
            <ta e="T62" id="Seg_1354" s="T61">pɛläk-qɨn</ta>
            <ta e="T63" id="Seg_1355" s="T62">Pür-qɨn</ta>
            <ta e="T64" id="Seg_1356" s="T63">ɔːtä-iː-m-tɨ</ta>
            <ta e="T65" id="Seg_1357" s="T64">čʼaqɨ</ta>
            <ta e="T66" id="Seg_1358" s="T65">nɨmtɨ</ta>
            <ta e="T67" id="Seg_1359" s="T66">čʼaqɨ</ta>
            <ta e="T68" id="Seg_1360" s="T67">qäl-ɨ-mpɨ-tɨt</ta>
            <ta e="T69" id="Seg_1361" s="T68">ɔːtä-lʼ</ta>
            <ta e="T70" id="Seg_1362" s="T69">ta</ta>
            <ta e="T71" id="Seg_1363" s="T70">tɨːpä-t</ta>
            <ta e="T72" id="Seg_1364" s="T71">kočʼčʼɨ-k</ta>
            <ta e="T73" id="Seg_1365" s="T72">ɛː-kkɨ-n</ta>
            <ta e="T74" id="Seg_1366" s="T73">ɔːtä-lʼ</ta>
            <ta e="T75" id="Seg_1367" s="T74">mɔːt</ta>
            <ta e="T76" id="Seg_1368" s="T75">mi-kkɨ-joıː-mpɨ-tɨt</ta>
            <ta e="T77" id="Seg_1369" s="T76">ɔːtä-n</ta>
            <ta e="T78" id="Seg_1370" s="T77">ɔːŋ-qɨn</ta>
            <ta e="T79" id="Seg_1371" s="T78">nɨmtɨ</ta>
            <ta e="T80" id="Seg_1372" s="T79">*čʼıqqɨ-lɨ-kkɨ-joıː-mpɨ-tɨt</ta>
            <ta e="T81" id="Seg_1373" s="T80">ɔːtä</ta>
            <ta e="T82" id="Seg_1374" s="T81">samoj</ta>
            <ta e="T83" id="Seg_1375" s="T82">qoštɨ</ta>
            <ta e="T84" id="Seg_1376" s="T83">vremʼa-tɨ</ta>
            <ta e="T85" id="Seg_1377" s="T84">ɛː-kkɨ-n</ta>
            <ta e="T86" id="Seg_1378" s="T85">šittɨ</ta>
            <ta e="T87" id="Seg_1379" s="T86">irä-qı-tɨ</ta>
            <ta e="T88" id="Seg_1380" s="T87">ijulʼ</ta>
            <ta e="T89" id="Seg_1381" s="T88">ijulʼ</ta>
            <ta e="T90" id="Seg_1382" s="T89">mesʼats</ta>
            <ta e="T91" id="Seg_1383" s="T90">ɛː-kkɨ-n</ta>
            <ta e="T92" id="Seg_1384" s="T91">aj</ta>
            <ta e="T93" id="Seg_1385" s="T92">avgust</ta>
            <ta e="T94" id="Seg_1386" s="T93">aj</ta>
            <ta e="T95" id="Seg_1387" s="T94">ijunʼ</ta>
            <ta e="T96" id="Seg_1388" s="T95">na</ta>
            <ta e="T97" id="Seg_1389" s="T96">nɔːkɨr</ta>
            <ta e="T98" id="Seg_1390" s="T97">irä-qɨn-ntɨ</ta>
            <ta e="T99" id="Seg_1391" s="T98">taŋɨ-n</ta>
            <ta e="T100" id="Seg_1392" s="T99">təttɨ</ta>
            <ta e="T101" id="Seg_1393" s="T100">meː-kkɨ-joıː-mpɨ-tɨ</ta>
            <ta e="T102" id="Seg_1394" s="T101">təttɨ</ta>
            <ta e="T103" id="Seg_1395" s="T102">nɨmtɨ</ta>
            <ta e="T104" id="Seg_1396" s="T103">kɨrapoːqɨn</ta>
            <ta e="T105" id="Seg_1397" s="T104">səttɨpɨŋ</ta>
            <ta e="T106" id="Seg_1398" s="T105">ropɨtɨ-kkɨ-joıː-mpɨ-tɨt</ta>
            <ta e="T107" id="Seg_1399" s="T106">qara</ta>
            <ta e="T108" id="Seg_1400" s="T107">montɨ-qɨn</ta>
            <ta e="T109" id="Seg_1401" s="T108">qən-ɛː-ptäː-qɨn</ta>
            <ta e="T110" id="Seg_1402" s="T109">kočʼčʼɨ</ta>
            <ta e="T111" id="Seg_1403" s="T110">ɔːtä-iː-m-tɨ</ta>
            <ta e="T112" id="Seg_1404" s="T111">nʼentɨ</ta>
            <ta e="T113" id="Seg_1405" s="T112">*taqɨ-qɨl-joıː-mpɨ-tɨt</ta>
            <ta e="T114" id="Seg_1406" s="T113">ɔːtä-iː-m-tɨ</ta>
            <ta e="T115" id="Seg_1407" s="T114">kekkɨsä</ta>
            <ta e="T116" id="Seg_1408" s="T115">muntɨk</ta>
            <ta e="T117" id="Seg_1409" s="T116">nʼentɨ</ta>
            <ta e="T118" id="Seg_1410" s="T117">*taqɨ-qɨl-lä</ta>
            <ta e="T119" id="Seg_1411" s="T118">tü-ntɨ-tɨt</ta>
            <ta e="T121" id="Seg_1412" s="T120">nɨmtɨ</ta>
            <ta e="T122" id="Seg_1413" s="T121">ɔːta-m</ta>
            <ta e="T123" id="Seg_1414" s="T122">mi-kkɨ-joıː-mpɨ-tɨt. </ta>
            <ta e="T124" id="Seg_1415" s="T123">suːrɨš-ntɨlʼ</ta>
            <ta e="T125" id="Seg_1416" s="T124">qum-ɨ-t</ta>
            <ta e="T126" id="Seg_1417" s="T125">šittälʼ</ta>
            <ta e="T127" id="Seg_1418" s="T126">ɔːtä-m</ta>
            <ta e="T128" id="Seg_1419" s="T127">ıllä</ta>
            <ta e="T129" id="Seg_1420" s="T128">muntɨk</ta>
            <ta e="T130" id="Seg_1421" s="T129">šöt-ntɨ</ta>
            <ta e="T131" id="Seg_1422" s="T130">qən-kkɨ-joıː-mpɨ-tɨt</ta>
            <ta e="T132" id="Seg_1423" s="T131">qəlʼčʼa-iː-mɨt</ta>
            <ta e="T133" id="Seg_1424" s="T132">muntɨk</ta>
            <ta e="T134" id="Seg_1425" s="T133">qum-iː-sä</ta>
            <ta e="T135" id="Seg_1426" s="T134">šöt-qɨn</ta>
            <ta e="T136" id="Seg_1427" s="T135">kora-kkɨ-tɨt</ta>
            <ta e="T137" id="Seg_1428" s="T136">aj</ta>
            <ta e="T138" id="Seg_1429" s="T137">na</ta>
            <ta e="T139" id="Seg_1430" s="T138">ɔːtä-m-tɨ</ta>
            <ta e="T140" id="Seg_1431" s="T139">suːrɨš-ntɨlʼ</ta>
            <ta e="T141" id="Seg_1432" s="T140">qum-ɨ-t</ta>
            <ta e="T142" id="Seg_1433" s="T141">iː-ptäː-qɨn</ta>
            <ta e="T143" id="Seg_1434" s="T142">muntɨk</ta>
            <ta e="T145" id="Seg_1435" s="T144">nəkɨ-r-ɨ-lɨ-mpɨ-tɨt</ta>
            <ta e="T146" id="Seg_1436" s="T145">kutɨ</ta>
            <ta e="T147" id="Seg_1437" s="T146">kuššak</ta>
            <ta e="T148" id="Seg_1438" s="T147">ɔːtä</ta>
            <ta e="T149" id="Seg_1439" s="T148">mi-mpɨ-ntɨ-tɨ</ta>
            <ta e="T150" id="Seg_1440" s="T149">nɔːtna</ta>
            <ta e="T151" id="Seg_1441" s="T150">nəkɨ-r-kkɨ-joıː-mpɨ-tɨt</ta>
            <ta e="T152" id="Seg_1442" s="T151">štobɨ</ta>
            <ta e="T153" id="Seg_1443" s="T152">šentɨ</ta>
            <ta e="T154" id="Seg_1444" s="T153">poː-qɨn-ntɨ</ta>
            <ta e="T155" id="Seg_1445" s="T154">promhoz</ta>
            <ta e="T156" id="Seg_1446" s="T155">ɔːtä-m</ta>
            <ta e="T157" id="Seg_1447" s="T156">*tɔːqɨl-tɨ-mpɨ-kkɨ-joıː-mpɨ-tɨt</ta>
            <ta e="T158" id="Seg_1448" s="T157">šittälʼ</ta>
            <ta e="T159" id="Seg_1449" s="T158">ɔːtä</ta>
            <ta e="T160" id="Seg_1450" s="T159">muntɨk</ta>
            <ta e="T161" id="Seg_1451" s="T160">qäl-lä</ta>
            <ta e="T162" id="Seg_1452" s="T161">puːlä</ta>
            <ta e="T163" id="Seg_1453" s="T162">šentɨ</ta>
            <ta e="T164" id="Seg_1454" s="T163">poː-qɨn-ntɨ</ta>
            <ta e="T165" id="Seg_1455" s="T164">promhoz</ta>
            <ta e="T166" id="Seg_1456" s="T165">nɨmtɨ</ta>
            <ta e="T167" id="Seg_1457" s="T166">lapqa-qɨn</ta>
            <ta e="T168" id="Seg_1458" s="T167">ɔːtä-m</ta>
            <ta e="T169" id="Seg_1459" s="T168">montɨ</ta>
            <ta e="T170" id="Seg_1460" s="T169">nʼentɨ</ta>
            <ta e="T171" id="Seg_1461" s="T170">*tɔːqɨl-tɨ-mpɨ-kkɨ-joıː-mpɨ-tɨt</ta>
            <ta e="T172" id="Seg_1462" s="T171">mɨta</ta>
            <ta e="T173" id="Seg_1463" s="T172">*kušša-lʼ</ta>
            <ta e="T175" id="Seg_1464" s="T174">ɔːtä</ta>
            <ta e="T176" id="Seg_1465" s="T175">a</ta>
            <ta e="T177" id="Seg_1466" s="T176">*kušša-lʼ</ta>
            <ta e="T179" id="Seg_1467" s="T178">ɔːtä</ta>
            <ta e="T180" id="Seg_1468" s="T179">čʼäːŋkɨ-ntɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T7" id="Seg_1469" s="T6">spring-LOC</ta>
            <ta e="T8" id="Seg_1470" s="T7">snow-%%</ta>
            <ta e="T9" id="Seg_1471" s="T8">break-TR-RFL.PFV-ACTN-LOC</ta>
            <ta e="T10" id="Seg_1472" s="T9">%%</ta>
            <ta e="T11" id="Seg_1473" s="T10">reindeer-PL-ACC-3SG</ta>
            <ta e="T12" id="Seg_1474" s="T11">all</ta>
            <ta e="T13" id="Seg_1475" s="T12">gather-MULO-PFV-HAB-3PL</ta>
            <ta e="T14" id="Seg_1476" s="T13">reindeer-PL-ACC-3SG</ta>
            <ta e="T15" id="Seg_1477" s="T14">gather-MULO-CVB</ta>
            <ta e="T16" id="Seg_1478" s="T15">%%</ta>
            <ta e="T17" id="Seg_1479" s="T16">reindeer-ACC-3SG</ta>
            <ta e="T19" id="Seg_1480" s="T18">fold.[NOM]</ta>
            <ta e="T20" id="Seg_1481" s="T19">gather-MULO-DUR-%%-HAB-3PL</ta>
            <ta e="T21" id="Seg_1482" s="T20">that</ta>
            <ta e="T22" id="Seg_1483" s="T21">%%</ta>
            <ta e="T23" id="Seg_1484" s="T22">reindeer-PL-ACC-3SG</ta>
            <ta e="T24" id="Seg_1485" s="T23">fold-LOC</ta>
            <ta e="T25" id="Seg_1486" s="T24">always?</ta>
            <ta e="T26" id="Seg_1487" s="T25">one</ta>
            <ta e="T27" id="Seg_1488" s="T26">something-LOC</ta>
            <ta e="T28" id="Seg_1489" s="T27">be.situated-PFV-HAB-3PL</ta>
            <ta e="T29" id="Seg_1490" s="T28">female.reindeer-PL-EP-ACC</ta>
            <ta e="T30" id="Seg_1491" s="T29">give.birth-TR-DUR-%%-HAB-3PL</ta>
            <ta e="T31" id="Seg_1492" s="T30">wait-IPFV-CVB</ta>
            <ta e="T32" id="Seg_1493" s="T31">reindeer-ACC</ta>
            <ta e="T33" id="Seg_1494" s="T32">give.birth-HAB-CVB</ta>
            <ta e="T34" id="Seg_1495" s="T33">after</ta>
            <ta e="T35" id="Seg_1496" s="T34">mosquito.[NOM]</ta>
            <ta e="T36" id="Seg_1497" s="T35">fly.up-PFV-ACTN-LOC</ta>
            <ta e="T37" id="Seg_1498" s="T36">reindeer-PL-ACC-3SG</ta>
            <ta e="T38" id="Seg_1499" s="T37">smoke-INSTR</ta>
            <ta e="T39" id="Seg_1500" s="T38">gnaw-DUR-%%-HAB-3PL</ta>
            <ta e="T40" id="Seg_1501" s="T39">this</ta>
            <ta e="T41" id="Seg_1502" s="T40">Pur-LOC</ta>
            <ta e="T42" id="Seg_1503" s="T41">some</ta>
            <ta e="T43" id="Seg_1504" s="T42">reindeer-PL.[NOM]-1PL</ta>
            <ta e="T44" id="Seg_1505" s="T43">this</ta>
            <ta e="T45" id="Seg_1506" s="T44">mosquito-LOC</ta>
            <ta e="T46" id="Seg_1507" s="T45">always?</ta>
            <ta e="T48" id="Seg_1508" s="T47">Tolka-ILL</ta>
            <ta e="T49" id="Seg_1509" s="T48">run-DUR-%%-HAB-3PL</ta>
            <ta e="T50" id="Seg_1510" s="T49">this.side-ADV.LOC</ta>
            <ta e="T51" id="Seg_1511" s="T50">always?</ta>
            <ta e="T52" id="Seg_1512" s="T51">run-3PL</ta>
            <ta e="T53" id="Seg_1513" s="T52">(s)he-EP-PL.[NOM]</ta>
            <ta e="T54" id="Seg_1514" s="T53">come-CVB</ta>
            <ta e="T55" id="Seg_1515" s="T54">and</ta>
            <ta e="T56" id="Seg_1516" s="T55">back</ta>
            <ta e="T57" id="Seg_1517" s="T56">%%-DUR-%%-HAB-3PL</ta>
            <ta e="T58" id="Seg_1518" s="T57">away</ta>
            <ta e="T59" id="Seg_1519" s="T58">run-CVB</ta>
            <ta e="T60" id="Seg_1520" s="T59">disperse?-CVB</ta>
            <ta e="T61" id="Seg_1521" s="T60">that-ADJZ</ta>
            <ta e="T62" id="Seg_1522" s="T61">side-LOC</ta>
            <ta e="T63" id="Seg_1523" s="T62">Pur-LOC</ta>
            <ta e="T64" id="Seg_1524" s="T63">reindeer-PL-ACC-3SG</ta>
            <ta e="T65" id="Seg_1525" s="T64">always?</ta>
            <ta e="T66" id="Seg_1526" s="T65">there</ta>
            <ta e="T67" id="Seg_1527" s="T66">always?</ta>
            <ta e="T68" id="Seg_1528" s="T67">go-EP-HAB-3PL</ta>
            <ta e="T69" id="Seg_1529" s="T68">reindeer-ADJZ</ta>
            <ta e="T70" id="Seg_1530" s="T69">%%</ta>
            <ta e="T71" id="Seg_1531" s="T70">gadfly-PL.[NOM]</ta>
            <ta e="T72" id="Seg_1532" s="T71">much-ADVZ</ta>
            <ta e="T73" id="Seg_1533" s="T72">be-DUR-3SG.S</ta>
            <ta e="T74" id="Seg_1534" s="T73">reindeer-ADJZ</ta>
            <ta e="T75" id="Seg_1535" s="T74">house.[NOM]</ta>
            <ta e="T76" id="Seg_1536" s="T75">give-DUR-%%-HAB-3PL</ta>
            <ta e="T77" id="Seg_1537" s="T76">reindeer-GEN</ta>
            <ta e="T78" id="Seg_1538" s="T77">mouth-LOC</ta>
            <ta e="T79" id="Seg_1539" s="T78">there</ta>
            <ta e="T80" id="Seg_1540" s="T79">emit.smoke-INCH-DUR-%%-HAB-3PL</ta>
            <ta e="T81" id="Seg_1541" s="T80">reindeer.[NOM]</ta>
            <ta e="T82" id="Seg_1542" s="T81">most</ta>
            <ta e="T83" id="Seg_1543" s="T82">bad</ta>
            <ta e="T84" id="Seg_1544" s="T83">time.[NOM]-3SG</ta>
            <ta e="T85" id="Seg_1545" s="T84">be-DUR-3SG.S</ta>
            <ta e="T86" id="Seg_1546" s="T85">two</ta>
            <ta e="T87" id="Seg_1547" s="T86">month-DU.[NOM]-3SG</ta>
            <ta e="T88" id="Seg_1548" s="T87">July.[NOM]</ta>
            <ta e="T89" id="Seg_1549" s="T88">July.[NOM]</ta>
            <ta e="T90" id="Seg_1550" s="T89">month.[NOM]</ta>
            <ta e="T91" id="Seg_1551" s="T90">be-DUR-3SG.S</ta>
            <ta e="T92" id="Seg_1552" s="T91">and</ta>
            <ta e="T93" id="Seg_1553" s="T92">August.[NOM]</ta>
            <ta e="T94" id="Seg_1554" s="T93">and</ta>
            <ta e="T95" id="Seg_1555" s="T94">June.[NOM]</ta>
            <ta e="T96" id="Seg_1556" s="T95">this</ta>
            <ta e="T97" id="Seg_1557" s="T96">three</ta>
            <ta e="T98" id="Seg_1558" s="T97">month-LOC-OBL.3SG</ta>
            <ta e="T99" id="Seg_1559" s="T98">summer-ADV.LOC</ta>
            <ta e="T100" id="Seg_1560" s="T99">earth.[NOM]</ta>
            <ta e="T101" id="Seg_1561" s="T100">make-DUR-%%-HAB-3SG.O</ta>
            <ta e="T102" id="Seg_1562" s="T101">earth.[NOM]</ta>
            <ta e="T103" id="Seg_1563" s="T102">then</ta>
            <ta e="T104" id="Seg_1564" s="T103">%%</ta>
            <ta e="T105" id="Seg_1565" s="T104">hard</ta>
            <ta e="T106" id="Seg_1566" s="T105">work-DUR-%%-HAB-3PL</ta>
            <ta e="T107" id="Seg_1567" s="T106">%%</ta>
            <ta e="T108" id="Seg_1568" s="T107">%%-LOC</ta>
            <ta e="T109" id="Seg_1569" s="T108">leave-PFV-ACTN-LOC</ta>
            <ta e="T110" id="Seg_1570" s="T109">much</ta>
            <ta e="T111" id="Seg_1571" s="T110">reindeer-PL-ACC-3SG.O</ta>
            <ta e="T112" id="Seg_1572" s="T111">together</ta>
            <ta e="T113" id="Seg_1573" s="T112">gather-MULO-%%-HAB-3PL</ta>
            <ta e="T114" id="Seg_1574" s="T113">reindeer-PL-ACC-3SG.O</ta>
            <ta e="T115" id="Seg_1575" s="T114">only</ta>
            <ta e="T116" id="Seg_1576" s="T115">all</ta>
            <ta e="T117" id="Seg_1577" s="T116">together</ta>
            <ta e="T118" id="Seg_1578" s="T117">gather-MULO-CVB</ta>
            <ta e="T119" id="Seg_1579" s="T118">come-IPFV-3PL</ta>
            <ta e="T121" id="Seg_1580" s="T120">then</ta>
            <ta e="T122" id="Seg_1581" s="T121">reindeer-ACC</ta>
            <ta e="T123" id="Seg_1582" s="T122">give-DUR-%%-HAB-3PL</ta>
            <ta e="T124" id="Seg_1583" s="T123">hunt-PTCP.PRS</ta>
            <ta e="T125" id="Seg_1584" s="T124">human.being-EP-PL.[NOM]</ta>
            <ta e="T126" id="Seg_1585" s="T125">then</ta>
            <ta e="T127" id="Seg_1586" s="T126">reindeer-ACC</ta>
            <ta e="T128" id="Seg_1587" s="T127">down</ta>
            <ta e="T129" id="Seg_1588" s="T128">all</ta>
            <ta e="T130" id="Seg_1589" s="T129">forest-ILL</ta>
            <ta e="T131" id="Seg_1590" s="T130">go.away-DUR-%%-HAB-3PL</ta>
            <ta e="T132" id="Seg_1591" s="T131">%%-PL-1PL</ta>
            <ta e="T133" id="Seg_1592" s="T132">all</ta>
            <ta e="T134" id="Seg_1593" s="T133">human.being-PL-COM</ta>
            <ta e="T135" id="Seg_1594" s="T134">forest-LOC</ta>
            <ta e="T136" id="Seg_1595" s="T135">go.hunting-DUR-3PL</ta>
            <ta e="T137" id="Seg_1596" s="T136">and</ta>
            <ta e="T138" id="Seg_1597" s="T137">this</ta>
            <ta e="T139" id="Seg_1598" s="T138">reindeer-ACC-3SG</ta>
            <ta e="T140" id="Seg_1599" s="T139">hunt-PTCP.PRS</ta>
            <ta e="T141" id="Seg_1600" s="T140">human.being-EP-PL.[NOM]</ta>
            <ta e="T142" id="Seg_1601" s="T141">take-ACTN-LOC</ta>
            <ta e="T143" id="Seg_1602" s="T142">all</ta>
            <ta e="T145" id="Seg_1603" s="T144">write-FRQ-EP-RES-HAB-3PL</ta>
            <ta e="T146" id="Seg_1604" s="T145">who.[NOM]</ta>
            <ta e="T147" id="Seg_1605" s="T146">how.many</ta>
            <ta e="T148" id="Seg_1606" s="T147">reindeer.[NOM]</ta>
            <ta e="T149" id="Seg_1607" s="T148">give-PST.NAR-INFER-3SG.O</ta>
            <ta e="T150" id="Seg_1608" s="T149">need</ta>
            <ta e="T151" id="Seg_1609" s="T150">write-FRQ-DUR-%%-HAB-3PL</ta>
            <ta e="T152" id="Seg_1610" s="T151">štobɨ</ta>
            <ta e="T153" id="Seg_1611" s="T152">new</ta>
            <ta e="T154" id="Seg_1612" s="T153">year-LOC-OBL.3SG</ta>
            <ta e="T155" id="Seg_1613" s="T154">promkhoz.[NOM]</ta>
            <ta e="T156" id="Seg_1614" s="T155">reindeer-ACC</ta>
            <ta e="T157" id="Seg_1615" s="T156">count-TR-HAB-DUR-%%-HAB-3PL</ta>
            <ta e="T158" id="Seg_1616" s="T157">then</ta>
            <ta e="T159" id="Seg_1617" s="T158">reindeer.[NOM]</ta>
            <ta e="T160" id="Seg_1618" s="T159">all</ta>
            <ta e="T161" id="Seg_1619" s="T160">go-CVB</ta>
            <ta e="T162" id="Seg_1620" s="T161">after</ta>
            <ta e="T163" id="Seg_1621" s="T162">new</ta>
            <ta e="T164" id="Seg_1622" s="T163">year-LOC-OBL.3SG</ta>
            <ta e="T165" id="Seg_1623" s="T164">promkhoz.[NOM]</ta>
            <ta e="T166" id="Seg_1624" s="T165">then</ta>
            <ta e="T167" id="Seg_1625" s="T166">village-LOC</ta>
            <ta e="T168" id="Seg_1626" s="T167">reindeer-ACC</ta>
            <ta e="T169" id="Seg_1627" s="T168">apparently</ta>
            <ta e="T170" id="Seg_1628" s="T169">together</ta>
            <ta e="T171" id="Seg_1629" s="T170">count-TR-HAB-DUR-%%-HAB-3PL</ta>
            <ta e="T172" id="Seg_1630" s="T171">as.if</ta>
            <ta e="T173" id="Seg_1631" s="T172">interrog.pron.stem-ADJZ</ta>
            <ta e="T175" id="Seg_1632" s="T174">reindeer.[NOM]</ta>
            <ta e="T176" id="Seg_1633" s="T175">and</ta>
            <ta e="T177" id="Seg_1634" s="T176">interrog.pron.stem-ADJZ</ta>
            <ta e="T179" id="Seg_1635" s="T178">reindeer.[NOM]</ta>
            <ta e="T180" id="Seg_1636" s="T179">NEG.EX-INFER.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T7" id="Seg_1637" s="T6">весна-LOC</ta>
            <ta e="T8" id="Seg_1638" s="T7">снег-%%</ta>
            <ta e="T9" id="Seg_1639" s="T8">ломать-TR-RFL.PFV-ACTN-LOC</ta>
            <ta e="T10" id="Seg_1640" s="T9">%%</ta>
            <ta e="T11" id="Seg_1641" s="T10">олень-PL-ACC-3SG</ta>
            <ta e="T12" id="Seg_1642" s="T11">всё</ta>
            <ta e="T13" id="Seg_1643" s="T12">собирать-MULO-PFV-HAB-3PL</ta>
            <ta e="T14" id="Seg_1644" s="T13">олень-PL-ACC-3SG</ta>
            <ta e="T15" id="Seg_1645" s="T14">собирать-MULO-CVB</ta>
            <ta e="T16" id="Seg_1646" s="T15">%%</ta>
            <ta e="T17" id="Seg_1647" s="T16">олень-ACC-3SG</ta>
            <ta e="T19" id="Seg_1648" s="T18">кораль.[NOM]</ta>
            <ta e="T20" id="Seg_1649" s="T19">собирать-MULO-DUR-%%-HAB-3PL</ta>
            <ta e="T21" id="Seg_1650" s="T20">тот</ta>
            <ta e="T22" id="Seg_1651" s="T21">%%</ta>
            <ta e="T23" id="Seg_1652" s="T22">олень-PL-ACC-3SG</ta>
            <ta e="T24" id="Seg_1653" s="T23">кораль-LOC</ta>
            <ta e="T25" id="Seg_1654" s="T24">всегда?</ta>
            <ta e="T26" id="Seg_1655" s="T25">один</ta>
            <ta e="T27" id="Seg_1656" s="T26">нечто-LOC</ta>
            <ta e="T28" id="Seg_1657" s="T27">находиться-PFV-HAB-3PL</ta>
            <ta e="T29" id="Seg_1658" s="T28">важенка-PL-EP-ACC</ta>
            <ta e="T30" id="Seg_1659" s="T29">родить-TR-DUR-%%-HAB-3PL</ta>
            <ta e="T31" id="Seg_1660" s="T30">подождать-IPFV-CVB</ta>
            <ta e="T32" id="Seg_1661" s="T31">олень-ACC</ta>
            <ta e="T33" id="Seg_1662" s="T32">родить-HAB-CVB</ta>
            <ta e="T34" id="Seg_1663" s="T33">после</ta>
            <ta e="T35" id="Seg_1664" s="T34">комар.[NOM]</ta>
            <ta e="T36" id="Seg_1665" s="T35">взлететь-PFV-ACTN-LOC</ta>
            <ta e="T37" id="Seg_1666" s="T36">олень-PL-ACC-3SG</ta>
            <ta e="T38" id="Seg_1667" s="T37">дым-INSTR</ta>
            <ta e="T39" id="Seg_1668" s="T38">грызть-DUR-%%-HAB-3PL</ta>
            <ta e="T40" id="Seg_1669" s="T39">этот</ta>
            <ta e="T41" id="Seg_1670" s="T40">Пур-LOC</ta>
            <ta e="T42" id="Seg_1671" s="T41">некоторый</ta>
            <ta e="T43" id="Seg_1672" s="T42">олень-PL.[NOM]-1PL</ta>
            <ta e="T44" id="Seg_1673" s="T43">этот</ta>
            <ta e="T45" id="Seg_1674" s="T44">комар-LOC</ta>
            <ta e="T46" id="Seg_1675" s="T45">всегда?</ta>
            <ta e="T48" id="Seg_1676" s="T47">Толька-ILL</ta>
            <ta e="T49" id="Seg_1677" s="T48">побежать-DUR-%%-HAB-3PL</ta>
            <ta e="T50" id="Seg_1678" s="T49">эта.сторона-ADV.LOC</ta>
            <ta e="T51" id="Seg_1679" s="T50">всегда?</ta>
            <ta e="T52" id="Seg_1680" s="T51">побежать-3PL</ta>
            <ta e="T53" id="Seg_1681" s="T52">он(а)-EP-PL.[NOM]</ta>
            <ta e="T54" id="Seg_1682" s="T53">прийти-CVB</ta>
            <ta e="T55" id="Seg_1683" s="T54">и</ta>
            <ta e="T56" id="Seg_1684" s="T55">обратно</ta>
            <ta e="T57" id="Seg_1685" s="T56">%%-DUR-%%-HAB-3PL</ta>
            <ta e="T58" id="Seg_1686" s="T57">прочь</ta>
            <ta e="T59" id="Seg_1687" s="T58">побежать-CVB</ta>
            <ta e="T60" id="Seg_1688" s="T59">расходиться?-CVB</ta>
            <ta e="T61" id="Seg_1689" s="T60">тот-ADJZ</ta>
            <ta e="T62" id="Seg_1690" s="T61">сторона-LOC</ta>
            <ta e="T63" id="Seg_1691" s="T62">Пур-LOC</ta>
            <ta e="T64" id="Seg_1692" s="T63">олень-PL-ACC-3SG</ta>
            <ta e="T65" id="Seg_1693" s="T64">всегда?</ta>
            <ta e="T66" id="Seg_1694" s="T65">там</ta>
            <ta e="T67" id="Seg_1695" s="T66">всегда?</ta>
            <ta e="T68" id="Seg_1696" s="T67">ходить-EP-HAB-3PL</ta>
            <ta e="T69" id="Seg_1697" s="T68">олень-ADJZ</ta>
            <ta e="T70" id="Seg_1698" s="T69">%%</ta>
            <ta e="T71" id="Seg_1699" s="T70">овод-PL.[NOM]</ta>
            <ta e="T72" id="Seg_1700" s="T71">много-ADVZ</ta>
            <ta e="T73" id="Seg_1701" s="T72">быть-DUR-3SG.S</ta>
            <ta e="T74" id="Seg_1702" s="T73">олень-ADJZ</ta>
            <ta e="T75" id="Seg_1703" s="T74">дом.[NOM]</ta>
            <ta e="T76" id="Seg_1704" s="T75">дать-DUR-%%-HAB-3PL</ta>
            <ta e="T77" id="Seg_1705" s="T76">олень-GEN</ta>
            <ta e="T78" id="Seg_1706" s="T77">устье-LOC</ta>
            <ta e="T79" id="Seg_1707" s="T78">там</ta>
            <ta e="T80" id="Seg_1708" s="T79">дымить-INCH-DUR-%%-HAB-3PL</ta>
            <ta e="T81" id="Seg_1709" s="T80">олень.[NOM]</ta>
            <ta e="T82" id="Seg_1710" s="T81">самый</ta>
            <ta e="T83" id="Seg_1711" s="T82">плохой</ta>
            <ta e="T84" id="Seg_1712" s="T83">время.[NOM]-3SG</ta>
            <ta e="T85" id="Seg_1713" s="T84">быть-DUR-3SG.S</ta>
            <ta e="T86" id="Seg_1714" s="T85">два</ta>
            <ta e="T87" id="Seg_1715" s="T86">месяц-DU.[NOM]-3SG</ta>
            <ta e="T88" id="Seg_1716" s="T87">июль.[NOM]</ta>
            <ta e="T89" id="Seg_1717" s="T88">июль.[NOM]</ta>
            <ta e="T90" id="Seg_1718" s="T89">месяц.[NOM]</ta>
            <ta e="T91" id="Seg_1719" s="T90">быть-DUR-3SG.S</ta>
            <ta e="T92" id="Seg_1720" s="T91">и</ta>
            <ta e="T93" id="Seg_1721" s="T92">август.[NOM]</ta>
            <ta e="T94" id="Seg_1722" s="T93">и</ta>
            <ta e="T95" id="Seg_1723" s="T94">июнь.[NOM]</ta>
            <ta e="T96" id="Seg_1724" s="T95">этот</ta>
            <ta e="T97" id="Seg_1725" s="T96">три</ta>
            <ta e="T98" id="Seg_1726" s="T97">месяц-LOC-OBL.3SG</ta>
            <ta e="T99" id="Seg_1727" s="T98">лето-ADV.LOC</ta>
            <ta e="T100" id="Seg_1728" s="T99">земля.[NOM]</ta>
            <ta e="T101" id="Seg_1729" s="T100">сделать-DUR-%%-HAB-3SG.O</ta>
            <ta e="T102" id="Seg_1730" s="T101">земля.[NOM]</ta>
            <ta e="T103" id="Seg_1731" s="T102">тогда</ta>
            <ta e="T104" id="Seg_1732" s="T103">%%</ta>
            <ta e="T105" id="Seg_1733" s="T104">тяжело</ta>
            <ta e="T106" id="Seg_1734" s="T105">работать-DUR-%%-HAB-3PL</ta>
            <ta e="T107" id="Seg_1735" s="T106">%%</ta>
            <ta e="T108" id="Seg_1736" s="T107">%%-LOC</ta>
            <ta e="T109" id="Seg_1737" s="T108">отправиться-PFV-ACTN-LOC</ta>
            <ta e="T110" id="Seg_1738" s="T109">много</ta>
            <ta e="T111" id="Seg_1739" s="T110">олень-PL-ACC-3SG.O</ta>
            <ta e="T112" id="Seg_1740" s="T111">вместе</ta>
            <ta e="T113" id="Seg_1741" s="T112">собирать-MULO-%%-HAB-3PL</ta>
            <ta e="T114" id="Seg_1742" s="T113">олень-PL-ACC-3SG.O</ta>
            <ta e="T115" id="Seg_1743" s="T114">только</ta>
            <ta e="T116" id="Seg_1744" s="T115">всё</ta>
            <ta e="T117" id="Seg_1745" s="T116">вместе</ta>
            <ta e="T118" id="Seg_1746" s="T117">собирать-MULO-CVB</ta>
            <ta e="T119" id="Seg_1747" s="T118">прийти-IPFV-3PL</ta>
            <ta e="T121" id="Seg_1748" s="T120">тогда</ta>
            <ta e="T122" id="Seg_1749" s="T121">олень-ACC</ta>
            <ta e="T123" id="Seg_1750" s="T122">дать-DUR-%%-HAB-3PL</ta>
            <ta e="T124" id="Seg_1751" s="T123">охотиться-PTCP.PRS</ta>
            <ta e="T125" id="Seg_1752" s="T124">человек-EP-PL.[NOM]</ta>
            <ta e="T126" id="Seg_1753" s="T125">потом</ta>
            <ta e="T127" id="Seg_1754" s="T126">олень-ACC</ta>
            <ta e="T128" id="Seg_1755" s="T127">вниз</ta>
            <ta e="T129" id="Seg_1756" s="T128">всё</ta>
            <ta e="T130" id="Seg_1757" s="T129">лес-ILL</ta>
            <ta e="T131" id="Seg_1758" s="T130">уйти-DUR-%%-HAB-3PL</ta>
            <ta e="T132" id="Seg_1759" s="T131">%%-PL-1PL</ta>
            <ta e="T133" id="Seg_1760" s="T132">всё</ta>
            <ta e="T134" id="Seg_1761" s="T133">человек-PL-COM</ta>
            <ta e="T135" id="Seg_1762" s="T134">лес-LOC</ta>
            <ta e="T136" id="Seg_1763" s="T135">отправиться.на.охоту-DUR-3PL</ta>
            <ta e="T137" id="Seg_1764" s="T136">и</ta>
            <ta e="T138" id="Seg_1765" s="T137">этот</ta>
            <ta e="T139" id="Seg_1766" s="T138">олень-ACC-3SG</ta>
            <ta e="T140" id="Seg_1767" s="T139">охотиться-PTCP.PRS</ta>
            <ta e="T141" id="Seg_1768" s="T140">человек-EP-PL.[NOM]</ta>
            <ta e="T142" id="Seg_1769" s="T141">взять-ACTN-LOC</ta>
            <ta e="T143" id="Seg_1770" s="T142">всё</ta>
            <ta e="T145" id="Seg_1771" s="T144">писать-FRQ-EP-RES-HAB-3PL</ta>
            <ta e="T146" id="Seg_1772" s="T145">кто.[NOM]</ta>
            <ta e="T147" id="Seg_1773" s="T146">сколько</ta>
            <ta e="T148" id="Seg_1774" s="T147">олень.[NOM]</ta>
            <ta e="T149" id="Seg_1775" s="T148">дать-PST.NAR-INFER-3SG.O</ta>
            <ta e="T150" id="Seg_1776" s="T149">нужно</ta>
            <ta e="T151" id="Seg_1777" s="T150">писать-FRQ-DUR-%%-HAB-3PL</ta>
            <ta e="T152" id="Seg_1778" s="T151">чтобы</ta>
            <ta e="T153" id="Seg_1779" s="T152">новый</ta>
            <ta e="T154" id="Seg_1780" s="T153">год-LOC-OBL.3SG</ta>
            <ta e="T155" id="Seg_1781" s="T154">промхоз.[NOM]</ta>
            <ta e="T156" id="Seg_1782" s="T155">олень-ACC</ta>
            <ta e="T157" id="Seg_1783" s="T156">считать-TR-HAB-DUR-%%-HAB-3PL</ta>
            <ta e="T158" id="Seg_1784" s="T157">потом</ta>
            <ta e="T159" id="Seg_1785" s="T158">олень.[NOM]</ta>
            <ta e="T160" id="Seg_1786" s="T159">всё</ta>
            <ta e="T161" id="Seg_1787" s="T160">ходить-CVB</ta>
            <ta e="T162" id="Seg_1788" s="T161">после</ta>
            <ta e="T163" id="Seg_1789" s="T162">новый</ta>
            <ta e="T164" id="Seg_1790" s="T163">год-LOC-OBL.3SG</ta>
            <ta e="T165" id="Seg_1791" s="T164">промхоз.[NOM]</ta>
            <ta e="T166" id="Seg_1792" s="T165">тогда</ta>
            <ta e="T167" id="Seg_1793" s="T166">поселок-LOC</ta>
            <ta e="T168" id="Seg_1794" s="T167">олень-ACC</ta>
            <ta e="T169" id="Seg_1795" s="T168">видать</ta>
            <ta e="T170" id="Seg_1796" s="T169">вместе</ta>
            <ta e="T171" id="Seg_1797" s="T170">считать-TR-HAB-DUR-%%-HAB-3PL</ta>
            <ta e="T172" id="Seg_1798" s="T171">будто</ta>
            <ta e="T173" id="Seg_1799" s="T172">interrog.pron.stem-ADJZ</ta>
            <ta e="T175" id="Seg_1800" s="T174">олень.[NOM]</ta>
            <ta e="T176" id="Seg_1801" s="T175">а</ta>
            <ta e="T177" id="Seg_1802" s="T176">interrog.pron.stem-ADJZ</ta>
            <ta e="T179" id="Seg_1803" s="T178">олень.[NOM]</ta>
            <ta e="T180" id="Seg_1804" s="T179">NEG.EX-INFER.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T7" id="Seg_1805" s="T6">n-n:case3</ta>
            <ta e="T8" id="Seg_1806" s="T7">n-any</ta>
            <ta e="T9" id="Seg_1807" s="T8">v-v&gt;v-v&gt;v-v&gt;n-n:case3</ta>
            <ta e="T10" id="Seg_1808" s="T9">%%</ta>
            <ta e="T11" id="Seg_1809" s="T10">n-n:num-n:case1-n:poss</ta>
            <ta e="T12" id="Seg_1810" s="T11">quant</ta>
            <ta e="T13" id="Seg_1811" s="T12">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T14" id="Seg_1812" s="T13">n-n:num-n:case1-n:poss</ta>
            <ta e="T15" id="Seg_1813" s="T14">v-v&gt;v-v&gt;adv</ta>
            <ta e="T16" id="Seg_1814" s="T15">%%</ta>
            <ta e="T17" id="Seg_1815" s="T16">n-n:case1-n:poss</ta>
            <ta e="T19" id="Seg_1816" s="T18">n-n:case3</ta>
            <ta e="T20" id="Seg_1817" s="T19">v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T21" id="Seg_1818" s="T20">dem</ta>
            <ta e="T22" id="Seg_1819" s="T21">%%</ta>
            <ta e="T23" id="Seg_1820" s="T22">n-n:num-n:case1-n:poss</ta>
            <ta e="T24" id="Seg_1821" s="T23">n-n:case3</ta>
            <ta e="T25" id="Seg_1822" s="T24">adv</ta>
            <ta e="T26" id="Seg_1823" s="T25">num</ta>
            <ta e="T27" id="Seg_1824" s="T26">n-n:case3</ta>
            <ta e="T28" id="Seg_1825" s="T27">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T29" id="Seg_1826" s="T28">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T30" id="Seg_1827" s="T29">v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T31" id="Seg_1828" s="T30">v-v&gt;v-v&gt;adv</ta>
            <ta e="T32" id="Seg_1829" s="T31">n-n:case3</ta>
            <ta e="T33" id="Seg_1830" s="T32">v-v&gt;v-v&gt;adv</ta>
            <ta e="T34" id="Seg_1831" s="T33">conj</ta>
            <ta e="T35" id="Seg_1832" s="T34">n-n:case3</ta>
            <ta e="T36" id="Seg_1833" s="T35">v-v&gt;v-v&gt;n-n:case3</ta>
            <ta e="T37" id="Seg_1834" s="T36">n-n:num-n:case1-n:poss</ta>
            <ta e="T38" id="Seg_1835" s="T37">n-n:case3</ta>
            <ta e="T39" id="Seg_1836" s="T38">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T40" id="Seg_1837" s="T39">dem</ta>
            <ta e="T41" id="Seg_1838" s="T40">nprop-n:case3</ta>
            <ta e="T42" id="Seg_1839" s="T41">adj</ta>
            <ta e="T43" id="Seg_1840" s="T42">n-n:num-n:case1-n:poss</ta>
            <ta e="T44" id="Seg_1841" s="T43">dem</ta>
            <ta e="T45" id="Seg_1842" s="T44">n-n:case3</ta>
            <ta e="T46" id="Seg_1843" s="T45">adv</ta>
            <ta e="T48" id="Seg_1844" s="T47">nprop-n:case3</ta>
            <ta e="T49" id="Seg_1845" s="T48">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T50" id="Seg_1846" s="T49">n-n&gt;adv</ta>
            <ta e="T51" id="Seg_1847" s="T50">adv</ta>
            <ta e="T52" id="Seg_1848" s="T51">v-v:pn</ta>
            <ta e="T53" id="Seg_1849" s="T52">pers-n:(ins)-n:num-n:case3</ta>
            <ta e="T54" id="Seg_1850" s="T53">v-v&gt;adv</ta>
            <ta e="T55" id="Seg_1851" s="T54">conj</ta>
            <ta e="T56" id="Seg_1852" s="T55">adv</ta>
            <ta e="T57" id="Seg_1853" s="T56">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T58" id="Seg_1854" s="T57">preverb</ta>
            <ta e="T59" id="Seg_1855" s="T58">v-v&gt;adv</ta>
            <ta e="T60" id="Seg_1856" s="T59">v-v&gt;adv</ta>
            <ta e="T61" id="Seg_1857" s="T60">dem-n&gt;adj</ta>
            <ta e="T62" id="Seg_1858" s="T61">n-n:case3</ta>
            <ta e="T63" id="Seg_1859" s="T62">nprop-n:case3</ta>
            <ta e="T64" id="Seg_1860" s="T63">n-n:num-n:case1-n:poss</ta>
            <ta e="T65" id="Seg_1861" s="T64">adv</ta>
            <ta e="T66" id="Seg_1862" s="T65">adv</ta>
            <ta e="T67" id="Seg_1863" s="T66">adv</ta>
            <ta e="T68" id="Seg_1864" s="T67">v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T69" id="Seg_1865" s="T68">n-n&gt;adj</ta>
            <ta e="T70" id="Seg_1866" s="T69">%%</ta>
            <ta e="T71" id="Seg_1867" s="T70">n-n:num-n:case3</ta>
            <ta e="T72" id="Seg_1868" s="T71">quant-adj&gt;adv</ta>
            <ta e="T73" id="Seg_1869" s="T72">v-v&gt;v-v:pn</ta>
            <ta e="T74" id="Seg_1870" s="T73">n-n&gt;adj</ta>
            <ta e="T75" id="Seg_1871" s="T74">n-n:case3</ta>
            <ta e="T76" id="Seg_1872" s="T75">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T77" id="Seg_1873" s="T76">n-n:case3</ta>
            <ta e="T78" id="Seg_1874" s="T77">n-n:case3</ta>
            <ta e="T79" id="Seg_1875" s="T78">adv</ta>
            <ta e="T80" id="Seg_1876" s="T79">v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T81" id="Seg_1877" s="T80">n-n:case3</ta>
            <ta e="T82" id="Seg_1878" s="T81">adj</ta>
            <ta e="T83" id="Seg_1879" s="T82">adj</ta>
            <ta e="T84" id="Seg_1880" s="T83">n-n:case1-n:poss</ta>
            <ta e="T85" id="Seg_1881" s="T84">v-v&gt;v-v:pn</ta>
            <ta e="T86" id="Seg_1882" s="T85">num</ta>
            <ta e="T87" id="Seg_1883" s="T86">n-n:num-n:case1-n:poss</ta>
            <ta e="T88" id="Seg_1884" s="T87">n-n:case3</ta>
            <ta e="T89" id="Seg_1885" s="T88">n-n:case3</ta>
            <ta e="T90" id="Seg_1886" s="T89">n-n:case3</ta>
            <ta e="T91" id="Seg_1887" s="T90">v-v&gt;v-v:pn</ta>
            <ta e="T92" id="Seg_1888" s="T91">conj</ta>
            <ta e="T93" id="Seg_1889" s="T92">n-n:case3</ta>
            <ta e="T94" id="Seg_1890" s="T93">conj</ta>
            <ta e="T95" id="Seg_1891" s="T94">n-n:case3</ta>
            <ta e="T96" id="Seg_1892" s="T95">dem</ta>
            <ta e="T97" id="Seg_1893" s="T96">num</ta>
            <ta e="T98" id="Seg_1894" s="T97">n-n:case2-n:obl.poss</ta>
            <ta e="T99" id="Seg_1895" s="T98">n-n&gt;adv</ta>
            <ta e="T100" id="Seg_1896" s="T99">n-n:case3</ta>
            <ta e="T101" id="Seg_1897" s="T100">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T102" id="Seg_1898" s="T101">n-n:case3</ta>
            <ta e="T103" id="Seg_1899" s="T102">adv</ta>
            <ta e="T104" id="Seg_1900" s="T103">%%</ta>
            <ta e="T105" id="Seg_1901" s="T104">adv</ta>
            <ta e="T106" id="Seg_1902" s="T105">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T107" id="Seg_1903" s="T106">%%</ta>
            <ta e="T108" id="Seg_1904" s="T107">%%-n:case3</ta>
            <ta e="T109" id="Seg_1905" s="T108">v-v&gt;v-v&gt;n-n:case3</ta>
            <ta e="T110" id="Seg_1906" s="T109">quant</ta>
            <ta e="T111" id="Seg_1907" s="T110">n-n:num-n:case3-v:pn</ta>
            <ta e="T112" id="Seg_1908" s="T111">adv</ta>
            <ta e="T113" id="Seg_1909" s="T112">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T114" id="Seg_1910" s="T113">n-n:num-n:case3-v:pn</ta>
            <ta e="T115" id="Seg_1911" s="T114">adv</ta>
            <ta e="T116" id="Seg_1912" s="T115">quant</ta>
            <ta e="T117" id="Seg_1913" s="T116">adv</ta>
            <ta e="T118" id="Seg_1914" s="T117">v-v&gt;v-v&gt;adv</ta>
            <ta e="T119" id="Seg_1915" s="T118">v-v&gt;v-v:pn</ta>
            <ta e="T121" id="Seg_1916" s="T120">adv</ta>
            <ta e="T122" id="Seg_1917" s="T121">n-n:case3</ta>
            <ta e="T123" id="Seg_1918" s="T122">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T124" id="Seg_1919" s="T123">v-v&gt;ptcp</ta>
            <ta e="T125" id="Seg_1920" s="T124">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T126" id="Seg_1921" s="T125">adv</ta>
            <ta e="T127" id="Seg_1922" s="T126">n-n:case3</ta>
            <ta e="T128" id="Seg_1923" s="T127">adv</ta>
            <ta e="T129" id="Seg_1924" s="T128">quant</ta>
            <ta e="T130" id="Seg_1925" s="T129">n-n:case3</ta>
            <ta e="T131" id="Seg_1926" s="T130">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T132" id="Seg_1927" s="T131">n-n:num-n:poss</ta>
            <ta e="T133" id="Seg_1928" s="T132">quant</ta>
            <ta e="T134" id="Seg_1929" s="T133">n-n:num-n:case3</ta>
            <ta e="T135" id="Seg_1930" s="T134">n-n:case3</ta>
            <ta e="T136" id="Seg_1931" s="T135">v-v&gt;v-v:pn</ta>
            <ta e="T137" id="Seg_1932" s="T136">conj</ta>
            <ta e="T138" id="Seg_1933" s="T137">dem</ta>
            <ta e="T139" id="Seg_1934" s="T138">n-n:case1-n:poss</ta>
            <ta e="T140" id="Seg_1935" s="T139">v-v&gt;ptcp</ta>
            <ta e="T141" id="Seg_1936" s="T140">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T142" id="Seg_1937" s="T141">v-v&gt;n-n:case3</ta>
            <ta e="T143" id="Seg_1938" s="T142">quant</ta>
            <ta e="T145" id="Seg_1939" s="T144">v-v&gt;v-v:ins-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T146" id="Seg_1940" s="T145">interrog-n:case3</ta>
            <ta e="T147" id="Seg_1941" s="T146">interrog</ta>
            <ta e="T148" id="Seg_1942" s="T147">n-n:case3</ta>
            <ta e="T149" id="Seg_1943" s="T148">v-v:tense-v:mood2-v:pn</ta>
            <ta e="T150" id="Seg_1944" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_1945" s="T150">v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T152" id="Seg_1946" s="T151">conj</ta>
            <ta e="T153" id="Seg_1947" s="T152">adj</ta>
            <ta e="T154" id="Seg_1948" s="T153">n-n:case2-n:obl.poss</ta>
            <ta e="T155" id="Seg_1949" s="T154">n-n:case3</ta>
            <ta e="T156" id="Seg_1950" s="T155">n-n:case3</ta>
            <ta e="T157" id="Seg_1951" s="T156">v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T158" id="Seg_1952" s="T157">adv</ta>
            <ta e="T159" id="Seg_1953" s="T158">n-n:case3</ta>
            <ta e="T160" id="Seg_1954" s="T159">quant</ta>
            <ta e="T161" id="Seg_1955" s="T160">v-v&gt;adv</ta>
            <ta e="T162" id="Seg_1956" s="T161">conj</ta>
            <ta e="T163" id="Seg_1957" s="T162">adj</ta>
            <ta e="T164" id="Seg_1958" s="T163">n-n:case2-n:obl.poss</ta>
            <ta e="T165" id="Seg_1959" s="T164">n-n:case3</ta>
            <ta e="T166" id="Seg_1960" s="T165">adv</ta>
            <ta e="T167" id="Seg_1961" s="T166">n-n:case3</ta>
            <ta e="T168" id="Seg_1962" s="T167">n-n:case3</ta>
            <ta e="T169" id="Seg_1963" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_1964" s="T169">adv</ta>
            <ta e="T171" id="Seg_1965" s="T170">v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T172" id="Seg_1966" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_1967" s="T172">interrog-n&gt;adj</ta>
            <ta e="T175" id="Seg_1968" s="T174">n-n:case3</ta>
            <ta e="T176" id="Seg_1969" s="T175">conj</ta>
            <ta e="T177" id="Seg_1970" s="T176">interrog-n&gt;adj</ta>
            <ta e="T179" id="Seg_1971" s="T178">n-n:case3</ta>
            <ta e="T180" id="Seg_1972" s="T179">v-v:tense-mood-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T7" id="Seg_1973" s="T6">n</ta>
            <ta e="T8" id="Seg_1974" s="T7">n</ta>
            <ta e="T9" id="Seg_1975" s="T8">n</ta>
            <ta e="T11" id="Seg_1976" s="T10">n</ta>
            <ta e="T12" id="Seg_1977" s="T11">quant</ta>
            <ta e="T13" id="Seg_1978" s="T12">v</ta>
            <ta e="T14" id="Seg_1979" s="T13">n</ta>
            <ta e="T15" id="Seg_1980" s="T14">adv</ta>
            <ta e="T17" id="Seg_1981" s="T16">n</ta>
            <ta e="T19" id="Seg_1982" s="T18">n</ta>
            <ta e="T20" id="Seg_1983" s="T19">v</ta>
            <ta e="T21" id="Seg_1984" s="T20">dem</ta>
            <ta e="T23" id="Seg_1985" s="T22">n</ta>
            <ta e="T24" id="Seg_1986" s="T23">n</ta>
            <ta e="T25" id="Seg_1987" s="T24">adv</ta>
            <ta e="T26" id="Seg_1988" s="T25">num</ta>
            <ta e="T27" id="Seg_1989" s="T26">n</ta>
            <ta e="T28" id="Seg_1990" s="T27">v</ta>
            <ta e="T29" id="Seg_1991" s="T28">n</ta>
            <ta e="T30" id="Seg_1992" s="T29">v</ta>
            <ta e="T31" id="Seg_1993" s="T30">v</ta>
            <ta e="T32" id="Seg_1994" s="T31">n</ta>
            <ta e="T33" id="Seg_1995" s="T32">v</ta>
            <ta e="T34" id="Seg_1996" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_1997" s="T34">n</ta>
            <ta e="T36" id="Seg_1998" s="T35">v</ta>
            <ta e="T37" id="Seg_1999" s="T36">n</ta>
            <ta e="T38" id="Seg_2000" s="T37">n</ta>
            <ta e="T39" id="Seg_2001" s="T38">v</ta>
            <ta e="T40" id="Seg_2002" s="T39">dem</ta>
            <ta e="T41" id="Seg_2003" s="T40">nprop</ta>
            <ta e="T42" id="Seg_2004" s="T41">n</ta>
            <ta e="T43" id="Seg_2005" s="T42">n</ta>
            <ta e="T44" id="Seg_2006" s="T43">dem</ta>
            <ta e="T45" id="Seg_2007" s="T44">n</ta>
            <ta e="T46" id="Seg_2008" s="T45">adv</ta>
            <ta e="T48" id="Seg_2009" s="T47">nprop</ta>
            <ta e="T49" id="Seg_2010" s="T48">v</ta>
            <ta e="T50" id="Seg_2011" s="T49">adv</ta>
            <ta e="T51" id="Seg_2012" s="T50">adv</ta>
            <ta e="T52" id="Seg_2013" s="T51">v</ta>
            <ta e="T53" id="Seg_2014" s="T52">pers</ta>
            <ta e="T54" id="Seg_2015" s="T53">adv</ta>
            <ta e="T55" id="Seg_2016" s="T54">conj</ta>
            <ta e="T56" id="Seg_2017" s="T55">adv</ta>
            <ta e="T58" id="Seg_2018" s="T57">preverb</ta>
            <ta e="T59" id="Seg_2019" s="T58">adv</ta>
            <ta e="T60" id="Seg_2020" s="T59">adv</ta>
            <ta e="T61" id="Seg_2021" s="T60">adj</ta>
            <ta e="T62" id="Seg_2022" s="T61">n</ta>
            <ta e="T63" id="Seg_2023" s="T62">nprop</ta>
            <ta e="T64" id="Seg_2024" s="T63">n</ta>
            <ta e="T65" id="Seg_2025" s="T64">adv</ta>
            <ta e="T66" id="Seg_2026" s="T65">adv</ta>
            <ta e="T67" id="Seg_2027" s="T66">adv</ta>
            <ta e="T68" id="Seg_2028" s="T67">v</ta>
            <ta e="T69" id="Seg_2029" s="T68">adj</ta>
            <ta e="T71" id="Seg_2030" s="T70">n</ta>
            <ta e="T72" id="Seg_2031" s="T71">adv</ta>
            <ta e="T73" id="Seg_2032" s="T72">v</ta>
            <ta e="T74" id="Seg_2033" s="T73">adj</ta>
            <ta e="T75" id="Seg_2034" s="T74">n</ta>
            <ta e="T76" id="Seg_2035" s="T75">v</ta>
            <ta e="T77" id="Seg_2036" s="T76">n</ta>
            <ta e="T78" id="Seg_2037" s="T77">n</ta>
            <ta e="T79" id="Seg_2038" s="T78">adv</ta>
            <ta e="T80" id="Seg_2039" s="T79">v</ta>
            <ta e="T81" id="Seg_2040" s="T80">n</ta>
            <ta e="T82" id="Seg_2041" s="T81">adj</ta>
            <ta e="T83" id="Seg_2042" s="T82">adj</ta>
            <ta e="T84" id="Seg_2043" s="T83">n</ta>
            <ta e="T85" id="Seg_2044" s="T84">v</ta>
            <ta e="T86" id="Seg_2045" s="T85">num</ta>
            <ta e="T87" id="Seg_2046" s="T86">n</ta>
            <ta e="T88" id="Seg_2047" s="T87">n</ta>
            <ta e="T89" id="Seg_2048" s="T88">n</ta>
            <ta e="T90" id="Seg_2049" s="T89">n</ta>
            <ta e="T91" id="Seg_2050" s="T90">v</ta>
            <ta e="T92" id="Seg_2051" s="T91">conj</ta>
            <ta e="T93" id="Seg_2052" s="T92">n</ta>
            <ta e="T94" id="Seg_2053" s="T93">conj</ta>
            <ta e="T95" id="Seg_2054" s="T94">n</ta>
            <ta e="T96" id="Seg_2055" s="T95">dem</ta>
            <ta e="T97" id="Seg_2056" s="T96">num</ta>
            <ta e="T98" id="Seg_2057" s="T97">n</ta>
            <ta e="T99" id="Seg_2058" s="T98">n</ta>
            <ta e="T100" id="Seg_2059" s="T99">n</ta>
            <ta e="T101" id="Seg_2060" s="T100">v</ta>
            <ta e="T102" id="Seg_2061" s="T101">n</ta>
            <ta e="T103" id="Seg_2062" s="T102">adv</ta>
            <ta e="T105" id="Seg_2063" s="T104">adv</ta>
            <ta e="T106" id="Seg_2064" s="T105">v</ta>
            <ta e="T110" id="Seg_2065" s="T109">quant</ta>
            <ta e="T111" id="Seg_2066" s="T110">n</ta>
            <ta e="T112" id="Seg_2067" s="T111">adv</ta>
            <ta e="T113" id="Seg_2068" s="T112">v</ta>
            <ta e="T114" id="Seg_2069" s="T113">n</ta>
            <ta e="T115" id="Seg_2070" s="T114">adv</ta>
            <ta e="T116" id="Seg_2071" s="T115">quant</ta>
            <ta e="T117" id="Seg_2072" s="T116">adv</ta>
            <ta e="T118" id="Seg_2073" s="T117">adv</ta>
            <ta e="T119" id="Seg_2074" s="T118">v</ta>
            <ta e="T121" id="Seg_2075" s="T120">adv</ta>
            <ta e="T122" id="Seg_2076" s="T121">n</ta>
            <ta e="T123" id="Seg_2077" s="T122">v</ta>
            <ta e="T124" id="Seg_2078" s="T123">ptcp</ta>
            <ta e="T125" id="Seg_2079" s="T124">n</ta>
            <ta e="T126" id="Seg_2080" s="T125">adv</ta>
            <ta e="T127" id="Seg_2081" s="T126">n</ta>
            <ta e="T128" id="Seg_2082" s="T127">adv</ta>
            <ta e="T129" id="Seg_2083" s="T128">quant</ta>
            <ta e="T130" id="Seg_2084" s="T129">n</ta>
            <ta e="T131" id="Seg_2085" s="T130">v</ta>
            <ta e="T132" id="Seg_2086" s="T131">n</ta>
            <ta e="T133" id="Seg_2087" s="T132">quant</ta>
            <ta e="T134" id="Seg_2088" s="T133">n</ta>
            <ta e="T135" id="Seg_2089" s="T134">n</ta>
            <ta e="T136" id="Seg_2090" s="T135">v</ta>
            <ta e="T137" id="Seg_2091" s="T136">conj</ta>
            <ta e="T138" id="Seg_2092" s="T137">dem</ta>
            <ta e="T139" id="Seg_2093" s="T138">n</ta>
            <ta e="T140" id="Seg_2094" s="T139">ptcp</ta>
            <ta e="T141" id="Seg_2095" s="T140">n</ta>
            <ta e="T142" id="Seg_2096" s="T141">n</ta>
            <ta e="T143" id="Seg_2097" s="T142">quant</ta>
            <ta e="T145" id="Seg_2098" s="T144">v</ta>
            <ta e="T146" id="Seg_2099" s="T145">interrog</ta>
            <ta e="T147" id="Seg_2100" s="T146">interrog</ta>
            <ta e="T148" id="Seg_2101" s="T147">n</ta>
            <ta e="T149" id="Seg_2102" s="T148">v</ta>
            <ta e="T150" id="Seg_2103" s="T149">conj</ta>
            <ta e="T151" id="Seg_2104" s="T150">v</ta>
            <ta e="T152" id="Seg_2105" s="T151">conj</ta>
            <ta e="T153" id="Seg_2106" s="T152">adj</ta>
            <ta e="T154" id="Seg_2107" s="T153">n</ta>
            <ta e="T155" id="Seg_2108" s="T154">n</ta>
            <ta e="T156" id="Seg_2109" s="T155">n</ta>
            <ta e="T157" id="Seg_2110" s="T156">v</ta>
            <ta e="T158" id="Seg_2111" s="T157">adv</ta>
            <ta e="T159" id="Seg_2112" s="T158">n</ta>
            <ta e="T160" id="Seg_2113" s="T159">quant</ta>
            <ta e="T161" id="Seg_2114" s="T160">v</ta>
            <ta e="T162" id="Seg_2115" s="T161">conj</ta>
            <ta e="T163" id="Seg_2116" s="T162">adj</ta>
            <ta e="T164" id="Seg_2117" s="T163">n</ta>
            <ta e="T165" id="Seg_2118" s="T164">n</ta>
            <ta e="T166" id="Seg_2119" s="T165">adv</ta>
            <ta e="T167" id="Seg_2120" s="T166">n</ta>
            <ta e="T168" id="Seg_2121" s="T167">n</ta>
            <ta e="T169" id="Seg_2122" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_2123" s="T169">adv</ta>
            <ta e="T171" id="Seg_2124" s="T170">v</ta>
            <ta e="T172" id="Seg_2125" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_2126" s="T172">interrog</ta>
            <ta e="T175" id="Seg_2127" s="T174">n</ta>
            <ta e="T176" id="Seg_2128" s="T175">conj</ta>
            <ta e="T177" id="Seg_2129" s="T176">interrog</ta>
            <ta e="T179" id="Seg_2130" s="T178">n</ta>
            <ta e="T180" id="Seg_2131" s="T179">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T18" id="Seg_2132" s="T17">RUS:cult</ta>
            <ta e="T24" id="Seg_2133" s="T23">RUS:cult</ta>
            <ta e="T82" id="Seg_2134" s="T81">RUS:cult</ta>
            <ta e="T84" id="Seg_2135" s="T83">RUS:cult</ta>
            <ta e="T88" id="Seg_2136" s="T87">RUS:cult</ta>
            <ta e="T89" id="Seg_2137" s="T88">RUS:cult</ta>
            <ta e="T90" id="Seg_2138" s="T89">RUS:cult</ta>
            <ta e="T93" id="Seg_2139" s="T92">RUS:cult</ta>
            <ta e="T95" id="Seg_2140" s="T94">RUS:cult</ta>
            <ta e="T106" id="Seg_2141" s="T105">RUS:cult</ta>
            <ta e="T150" id="Seg_2142" s="T149">RUS:mod</ta>
            <ta e="T152" id="Seg_2143" s="T151">RUS:gram</ta>
            <ta e="T155" id="Seg_2144" s="T154">RUS:cult</ta>
            <ta e="T165" id="Seg_2145" s="T164">RUS:cult</ta>
            <ta e="T167" id="Seg_2146" s="T166">RUS:cult</ta>
            <ta e="T176" id="Seg_2147" s="T175">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T120" id="Seg_2148" s="T119">RUS:int.ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_2149" s="T0">Рассказ про пастухов пуровского района.</ta>
            <ta e="T6" id="Seg_2150" s="T5">Оленеводов.</ta>
            <ta e="T13" id="Seg_2151" s="T6">Весной, когда снег лопнул, (?) оленей всех собирают.</ta>
            <ta e="T20" id="Seg_2152" s="T13">Оленей собрав, (?) оленей кораль собирают.</ta>
            <ta e="T25" id="Seg_2153" s="T20">В этом (месте)? оленей в корале всегда(?).</ta>
            <ta e="T28" id="Seg_2154" s="T25">В одном месте находятся.</ta>
            <ta e="T31" id="Seg_2155" s="T28">Ожидая, когда важенки родят.</ta>
            <ta e="T39" id="Seg_2156" s="T31">После рождения оленей, когда комары летят, оленей дымом обкуривают.</ta>
            <ta e="T41" id="Seg_2157" s="T39">На этом Пуре(?).</ta>
            <ta e="T49" id="Seg_2158" s="T41">Некоторые наши(?) олени в комарьё всегда(?) в Тольку бегут.</ta>
            <ta e="T52" id="Seg_2159" s="T49">Тогда (всегда)? бегут.</ta>
            <ta e="T57" id="Seg_2160" s="T52">Они приходят и обратно возвращаются.</ta>
            <ta e="T68" id="Seg_2161" s="T57">Прочь бегут, расходятся, на том берегу, на Пуре олени (всегда)? туда (всегда)? идут.</ta>
            <ta e="T76" id="Seg_2162" s="T68">Оленьих оводов много бывает, олений дом [для дымокура] делают.</ta>
            <ta e="T80" id="Seg_2163" s="T76">При входе оленей там разводят дымокур.</ta>
            <ta e="T87" id="Seg_2164" s="T80">У оленей самое плохое время - два месяца.</ta>
            <ta e="T88" id="Seg_2165" s="T87">Июль.</ta>
            <ta e="T91" id="Seg_2166" s="T88">Июль месяц бывает.</ta>
            <ta e="T93" id="Seg_2167" s="T91">И август.</ta>
            <ta e="T95" id="Seg_2168" s="T93">И июнь.</ta>
            <ta e="T101" id="Seg_2169" s="T95">В эти три месяца летом земля это делает.</ta>
            <ta e="T106" id="Seg_2170" s="T101">Земля тогда (бедные)? тяжело работают.</ta>
            <ta e="T113" id="Seg_2171" s="T106">Когда приходит (?), много оленей вместе собирают.</ta>
            <ta e="T119" id="Seg_2172" s="T113">Олени только все, когда их вместе собирают, приходят.</ta>
            <ta e="T123" id="Seg_2173" s="T119">Охотники тогда оленей отдают.</ta>
            <ta e="T126" id="Seg_2174" s="T123">Охотники потом.</ta>
            <ta e="T131" id="Seg_2175" s="T126">Оленей… (вниз)? все в лес уходят. </ta>
            <ta e="T136" id="Seg_2176" s="T131">(Друзья)? мои все вместе в лесу охотятся.</ta>
            <ta e="T145" id="Seg_2177" s="T136">И этих оленей охотники когда берут, все записывают.</ta>
            <ta e="T149" id="Seg_2178" s="T145">Кто сколько оленей дал.</ta>
            <ta e="T151" id="Seg_2179" s="T149">Надо, они записываются.</ta>
            <ta e="T157" id="Seg_2180" s="T151">Чтобы в новом году промхоз оленей подсчитывают.</ta>
            <ta e="T171" id="Seg_2181" s="T157">Потом, после того как все олени пойдут, в новом году промхоз, тогда в посёлке оленей всех вместе подсчитывают.</ta>
            <ta e="T180" id="Seg_2182" s="T171">То есть сколько оленей [есть], сколько оленей нет.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_2183" s="T0">The story about the shepherds of the Purovsky area.</ta>
            <ta e="T6" id="Seg_2184" s="T5">Reindeer herders.</ta>
            <ta e="T13" id="Seg_2185" s="T6">In spring, when the snow melts, (?) they gather the reindeers.</ta>
            <ta e="T20" id="Seg_2186" s="T13">They gather the reindeers, (?) they gather the herd. </ta>
            <ta e="T25" id="Seg_2187" s="T20">Here in this (place)? reindeers are always in herds.</ta>
            <ta e="T28" id="Seg_2188" s="T25">They are in one place. </ta>
            <ta e="T31" id="Seg_2189" s="T28">The female reindeers wait to give birth.</ta>
            <ta e="T39" id="Seg_2190" s="T31">After the birth of the calf, when the mosquitos are flying, they cense the reindeers (to get rid of the mosquitos). </ta>
            <ta e="T41" id="Seg_2191" s="T39">In this Pur(?).</ta>
            <ta e="T49" id="Seg_2192" s="T41">Some of our(?) reindeers always(?) run to Tolka. </ta>
            <ta e="T52" id="Seg_2193" s="T49">Then they always(?) run.</ta>
            <ta e="T57" id="Seg_2194" s="T52">They come and they come back. </ta>
            <ta e="T68" id="Seg_2195" s="T57">They run away, disperse themselves on this side, in Pur they always(?) run there always(?).</ta>
            <ta e="T76" id="Seg_2196" s="T68">There are many flies with the reindeers, they build a hut (to cense the reindeers). </ta>
            <ta e="T80" id="Seg_2197" s="T76">The smoke is started at the entry. </ta>
            <ta e="T87" id="Seg_2198" s="T80">Reindeers have a bad time in this two month.</ta>
            <ta e="T88" id="Seg_2199" s="T87">July. </ta>
            <ta e="T91" id="Seg_2200" s="T88">July is a monat.</ta>
            <ta e="T93" id="Seg_2201" s="T91">And august. </ta>
            <ta e="T95" id="Seg_2202" s="T93">And june.</ta>
            <ta e="T101" id="Seg_2203" s="T95">In this three month in summer, the earth makes it. </ta>
            <ta e="T106" id="Seg_2204" s="T101">The earth is working hard. </ta>
            <ta e="T113" id="Seg_2205" s="T106">When (?) is coming, they gather many reindeers. </ta>
            <ta e="T119" id="Seg_2206" s="T113">Reindeers are only all together, wenn they are gathered, they are coming. </ta>
            <ta e="T123" id="Seg_2207" s="T119">The hunters are giving the reindeer away. </ta>
            <ta e="T126" id="Seg_2208" s="T123">The hunters then.</ta>
            <ta e="T131" id="Seg_2209" s="T126">The reindeers are all going (down)? into the forest. </ta>
            <ta e="T136" id="Seg_2210" s="T131">All (my friends?) are going to the forest to hunt. </ta>
            <ta e="T145" id="Seg_2211" s="T136">And if they hunt a reindeer, they write everything down.</ta>
            <ta e="T149" id="Seg_2212" s="T145">Who has given how many reindeers. </ta>
            <ta e="T151" id="Seg_2213" s="T149">They have to write it down.</ta>
            <ta e="T157" id="Seg_2214" s="T151">So that in the new year they count the promkhoz reindeers. </ta>
            <ta e="T171" id="Seg_2215" s="T157">Then, after the reindeers left, in the new year the reindeers of the whole village will be counted together. </ta>
            <ta e="T180" id="Seg_2216" s="T171">That is how many deers there are and how many not. </ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_2217" s="T0">Die Geschichte der Hirten von Purovsky.</ta>
            <ta e="T6" id="Seg_2218" s="T5">Rentierhirten.</ta>
            <ta e="T13" id="Seg_2219" s="T6">Im Frühling, wenn der Schneeschmilzt,(?) versammeln sie die Rentiere.</ta>
            <ta e="T20" id="Seg_2220" s="T13">Sie versammeln die Rentiere, (?) versammeln die Herden.</ta>
            <ta e="T25" id="Seg_2221" s="T20">Hier an diesem (Platz)? sind Rentiere immer(?) in Herden.</ta>
            <ta e="T28" id="Seg_2222" s="T25">Sie sind an einem Ort.</ta>
            <ta e="T31" id="Seg_2223" s="T28">Die Rentierweibchen warten darauf die Jungen zu gebähren.</ta>
            <ta e="T39" id="Seg_2224" s="T31">Nach der Geburt des Kalbes, wenn die Mücken fliegen, beräuchern sie die Rentiere (um die Mücken zu vertreiben). </ta>
            <ta e="T41" id="Seg_2225" s="T39">In diesem Pur(?)</ta>
            <ta e="T49" id="Seg_2226" s="T41">Einige unserer(?) Rentiere rennen immer nach Tolka.</ta>
            <ta e="T52" id="Seg_2227" s="T49">Dann rennen sie immer(?).</ta>
            <ta e="T57" id="Seg_2228" s="T52">Sie kommen und kommen zurück.</ta>
            <ta e="T68" id="Seg_2229" s="T57">Sie rennen weg, verstreuen sich auf dieser Seite, in Pur gehen sie (immer)? dorthin (immer)?.</ta>
            <ta e="T76" id="Seg_2230" s="T68">Es gibt viele Fliegen bei den Rentieren, sie bauen einen Rentierstall/haus (zum beräuchern).</ta>
            <ta e="T80" id="Seg_2231" s="T76">Am Eingang wird der Rauch entzündet.</ta>
            <ta e="T87" id="Seg_2232" s="T80">Das Rentier hat eine schlechte Zeit in den zwei Monaten.</ta>
            <ta e="T88" id="Seg_2233" s="T87">Juli.</ta>
            <ta e="T91" id="Seg_2234" s="T88">Juli ist ein Monat.</ta>
            <ta e="T93" id="Seg_2235" s="T91">Und August.</ta>
            <ta e="T95" id="Seg_2236" s="T93">Und Juni. </ta>
            <ta e="T101" id="Seg_2237" s="T95">In diesen drei Monaten im Sommer macht die Erde es. </ta>
            <ta e="T106" id="Seg_2238" s="T101">Die Erde arbeitet dann hart. </ta>
            <ta e="T113" id="Seg_2239" s="T106">Wenn (?) kommt, versammeln sie viele Rentiere. </ta>
            <ta e="T119" id="Seg_2240" s="T113">Rentiere kommen nur alle zusammen, wenn sie versammelt werden, sie kommen. </ta>
            <ta e="T123" id="Seg_2241" s="T119">Die Jäger geben dann das Rentier weg. </ta>
            <ta e="T126" id="Seg_2242" s="T123">Die Jäger dann. </ta>
            <ta e="T131" id="Seg_2243" s="T126">Die Rentiere gehen dann alle (runter)? in den Wald.</ta>
            <ta e="T136" id="Seg_2244" s="T131">Alle (meine Freunde?) gehen zusammen in den Wald jagen.</ta>
            <ta e="T145" id="Seg_2245" s="T136">Und wenn sie ein Rentier jagen, dann schreiben sie alles auf. </ta>
            <ta e="T149" id="Seg_2246" s="T145">Wer wieviele Rentiere gegeben hat. </ta>
            <ta e="T151" id="Seg_2247" s="T149">Sie müssen es aufschreiben.</ta>
            <ta e="T157" id="Seg_2248" s="T151">Sodass sie im neuen Jahr die Promkhoz-Rentiere zählen.</ta>
            <ta e="T171" id="Seg_2249" s="T157">Dann, nachdem alle Rentiere gegangen sind, im neuen Jahr werden die Rentiere im Dorf zusammengezählt. </ta>
            <ta e="T180" id="Seg_2250" s="T171">Das ist wieviele Rentiere dort sind und wieviele nicht.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_2251" s="T0">Рассказ про пастухов Пуровского района.</ta>
            <ta e="T6" id="Seg_2252" s="T5">оленеводов</ta>
            <ta e="T13" id="Seg_2253" s="T6">весной когда снег лопнул ? оленей всех собирают</ta>
            <ta e="T20" id="Seg_2254" s="T13">оленей собирают ? оленей караль собирают</ta>
            <ta e="T25" id="Seg_2255" s="T20">в этом месте оленей в карале</ta>
            <ta e="T28" id="Seg_2256" s="T25">в одном месте находятся</ta>
            <ta e="T31" id="Seg_2257" s="T28">ждут когда важенки родят</ta>
            <ta e="T39" id="Seg_2258" s="T31">при рождении оленят когда комары летят оленей дымом обкуривают</ta>
            <ta e="T49" id="Seg_2259" s="T41">некоторые олени в комарьё всегда в Тольку бегут</ta>
            <ta e="T52" id="Seg_2260" s="T49">тогда здесь всегда бегут</ta>
            <ta e="T57" id="Seg_2261" s="T52">они приходят всегда обратно возвращаются</ta>
            <ta e="T68" id="Seg_2262" s="T57">бегут расходятся на том берегу Пура олени всегда туда всегда идут </ta>
            <ta e="T76" id="Seg_2263" s="T68">оленьи оводы если много олений дом (для дымокура) делают</ta>
            <ta e="T80" id="Seg_2264" s="T76">при входе оленей там разводят дымокур</ta>
            <ta e="T87" id="Seg_2265" s="T80">у оленей самое плохое время два месяца</ta>
            <ta e="T88" id="Seg_2266" s="T87">июль</ta>
            <ta e="T91" id="Seg_2267" s="T88">июль месяц есть</ta>
            <ta e="T93" id="Seg_2268" s="T91">и август</ta>
            <ta e="T95" id="Seg_2269" s="T93">и июнь</ta>
            <ta e="T101" id="Seg_2270" s="T95">в эти три месяца летом земля делает</ta>
            <ta e="T106" id="Seg_2271" s="T101">земля тогда бедные тяжело работают</ta>
            <ta e="T113" id="Seg_2272" s="T106">в осеннюю пору когда приходит зима много оленей вместе собираются</ta>
            <ta e="T119" id="Seg_2273" s="T113">олени все вместе вместе собираясь приходят</ta>
            <ta e="T123" id="Seg_2274" s="T119">охотники тогда оленей отдают</ta>
            <ta e="T126" id="Seg_2275" s="T123">охотники потом</ta>
            <ta e="T131" id="Seg_2276" s="T126">олени тут же все в лес идут </ta>
            <ta e="T136" id="Seg_2277" s="T131">друзья мои все вместе в лесу охотятся</ta>
            <ta e="T145" id="Seg_2278" s="T136">и этих оленей охотники когда получают все записывают</ta>
            <ta e="T149" id="Seg_2279" s="T145">кто сколько оленей дал</ta>
            <ta e="T151" id="Seg_2280" s="T149">надо записываться</ta>
            <ta e="T157" id="Seg_2281" s="T151">чтобы по новому сезону промхоз оленей подсчитывают</ta>
            <ta e="T171" id="Seg_2282" s="T157">значит после того как все олени пойдут по-новому подсчитывает промхоз всегда в посёлке оленей всех вместе подсчитывают</ta>
            <ta e="T180" id="Seg_2283" s="T171">то есть сколько оленей сколько оленей нет</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T39" id="Seg_2284" s="T31">[BrM]: Tentative analysis of the stem mɨrɨ-.</ta>
            <ta e="T49" id="Seg_2285" s="T41">[BrM]: Unexpected 1PL in ɔːtaiːmɨn.</ta>
            <ta e="T68" id="Seg_2286" s="T57">[BrM]: Unexpected Accusative in ɔːtaiːmtɨ.</ta>
            <ta e="T119" id="Seg_2287" s="T113">[BrM]: Unexpected Accusative in ɔːtaiːmtɨ.</ta>
            <ta e="T157" id="Seg_2288" s="T151">[BrM]: Unexpected two times HAB in the verb 'to count'.</ta>
            <ta e="T171" id="Seg_2289" s="T157">[BrM]: Unexpected two times HAB in the verb 'to count'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
