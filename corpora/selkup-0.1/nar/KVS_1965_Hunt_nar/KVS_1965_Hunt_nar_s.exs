<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID4ABD8697-E977-D0D2-0AEF-B34BEE4CED76">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\KVS_1965_Hunt_nar\KVS_1965_Hunt_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">168</ud-information>
            <ud-information attribute-name="# HIAT:w">133</ud-information>
            <ud-information attribute-name="# e">133</ud-information>
            <ud-information attribute-name="# HIAT:u">29</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KVS">
            <abbreviation>KVS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KVS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T133" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Kanak</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">poːqɨt</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">qonta</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Mat</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">mačʼä</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">qonʼišäk</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">suːrɨlʼlʼä</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">Mačʼoːqɨt</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">orsa</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">som</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">ɛːŋa</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_44" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">Mat</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">ütɨt</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">mačʼoːqɨt</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">suːrɨššäk</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_59" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">Kutar</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">ulqap</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">mišalsɨtɨ</ts>
                  <nts id="Seg_68" n="HIAT:ip">,</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">mat</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">moqonä</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">tüsak</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_81" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">Mat</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">ütät</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">koralʼä</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">qonʼišak</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_96" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">Mačʼoːqɨt</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">suːrup</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_104" n="HIAT:w" s="T27">kočʼeŋa</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_108" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">Kušaŋ</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">ulqa</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_116" n="HIAT:w" s="T30">mišalsɨtɨ</ts>
                  <nts id="Seg_117" n="HIAT:ip">,</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_120" n="HIAT:w" s="T31">meː</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_123" n="HIAT:w" s="T32">qonišem</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_126" n="HIAT:w" s="T33">qäːlɨlʼlʼä</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_129" n="HIAT:w" s="T34">Tuːtɨlʼ</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_132" n="HIAT:w" s="T35">toːntə</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_136" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_138" n="HIAT:w" s="T36">Tuːtɨp</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_141" n="HIAT:w" s="T37">orsa</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_144" n="HIAT:w" s="T38">kočʼik</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_147" n="HIAT:w" s="T39">qässämɨt</ts>
                  <nts id="Seg_148" n="HIAT:ip">,</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_151" n="HIAT:w" s="T40">nɨːnɨ</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_154" n="HIAT:w" s="T41">moqona</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_157" n="HIAT:w" s="T42">tüsɔːŋɨt</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_161" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_163" n="HIAT:w" s="T43">Taŋɨt</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_166" n="HIAT:w" s="T44">qumät</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">qänkɔːtɨn</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_172" n="HIAT:w" s="T46">nʼuːtɨlʼ</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_175" n="HIAT:w" s="T47">pačʼitɨlä</ts>
                  <nts id="Seg_176" n="HIAT:ip">.</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_179" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_181" n="HIAT:w" s="T48">Araqɨt</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_184" n="HIAT:w" s="T49">moqonä</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_187" n="HIAT:w" s="T50">tükkɔːtän</ts>
                  <nts id="Seg_188" n="HIAT:ip">.</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_191" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_193" n="HIAT:w" s="T51">Ukkɨr</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_196" n="HIAT:w" s="T52">ira</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_199" n="HIAT:w" s="T53">mačʼontɨ</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_202" n="HIAT:w" s="T54">suːrɨlʼlʼä</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_205" n="HIAT:w" s="T55">qänpa</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_209" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">Mačʼoːqɨk</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_214" n="HIAT:w" s="T57">suːrɨp</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_217" n="HIAT:w" s="T58">kočʼik</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_220" n="HIAT:w" s="T59">qätpatɨ</ts>
                  <nts id="Seg_221" n="HIAT:ip">.</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_224" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_226" n="HIAT:w" s="T60">Ukkɨr</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_229" n="HIAT:w" s="T61">ira</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_232" n="HIAT:w" s="T62">ilɨmpa</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_235" n="HIAT:w" s="T63">mačʼoːqɨt</ts>
                  <nts id="Seg_236" n="HIAT:ip">.</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_239" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_241" n="HIAT:w" s="T64">Kät</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_244" n="HIAT:w" s="T65">suːrɨšʼpä</ts>
                  <nts id="Seg_245" n="HIAT:ip">,</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_248" n="HIAT:w" s="T66">a</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_251" n="HIAT:w" s="T67">taŋɨt</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_254" n="HIAT:w" s="T68">meːkkolʼčʼimpatɨ</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_257" n="HIAT:w" s="T69">kınʼčʼip</ts>
                  <nts id="Seg_258" n="HIAT:ip">.</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_261" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_263" n="HIAT:w" s="T70">Taŋɨt</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_266" n="HIAT:w" s="T71">qätkɨtɨ</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_269" n="HIAT:w" s="T72">kočʼik</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_272" n="HIAT:w" s="T73">qäːlɨp</ts>
                  <nts id="Seg_273" n="HIAT:ip">.</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_276" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_278" n="HIAT:w" s="T74">Ukkɨr</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_281" n="HIAT:w" s="T75">ijalʼa</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_284" n="HIAT:w" s="T76">üttə</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_287" n="HIAT:w" s="T77">patqɨlpa</ts>
                  <nts id="Seg_288" n="HIAT:ip">,</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_291" n="HIAT:w" s="T78">nɨnɨ</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_294" n="HIAT:w" s="T79">qumɨt</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_297" n="HIAT:w" s="T80">konna</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_300" n="HIAT:w" s="T81">iːmpɔːtän</ts>
                  <nts id="Seg_301" n="HIAT:ip">.</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_304" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_306" n="HIAT:w" s="T82">Poːqɨt</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_309" n="HIAT:w" s="T83">qumɨt</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_312" n="HIAT:w" s="T84">kočʼ</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_315" n="HIAT:w" s="T85">ɛːŋɔːtɨn</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_319" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_321" n="HIAT:w" s="T86">Pit</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_324" n="HIAT:w" s="T87">qontɔːtɨn</ts>
                  <nts id="Seg_325" n="HIAT:ip">.</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_328" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_330" n="HIAT:w" s="T88">Muntɨk</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_333" n="HIAT:w" s="T89">qarrɨt</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_336" n="HIAT:w" s="T90">qumɨt</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_339" n="HIAT:w" s="T91">uːčʼila</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_342" n="HIAT:w" s="T92">qänkɔːtɨn</ts>
                  <nts id="Seg_343" n="HIAT:ip">,</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_346" n="HIAT:w" s="T93">a</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_349" n="HIAT:w" s="T94">üːtɨt</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_352" n="HIAT:w" s="T95">uːčʼila</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_355" n="HIAT:w" s="T96">qɨːqɨlkɔːtɨn</ts>
                  <nts id="Seg_356" n="HIAT:ip">.</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_359" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_361" n="HIAT:w" s="T97">A</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_364" n="HIAT:w" s="T98">čʼeːlʼit</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_367" n="HIAT:w" s="T99">amɨrqo</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_370" n="HIAT:w" s="T100">qonʼaškɔːten</ts>
                  <nts id="Seg_371" n="HIAT:ip">.</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_374" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_376" n="HIAT:w" s="T101">Meː</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_379" n="HIAT:w" s="T102">üːtɨt</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_382" n="HIAT:w" s="T103">qonʼišimɨt</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_385" n="HIAT:w" s="T104">qäːlɨlʼlʼä</ts>
                  <nts id="Seg_386" n="HIAT:ip">.</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_389" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_391" n="HIAT:w" s="T105">Qäːlɨp</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_394" n="HIAT:w" s="T106">qättɛːmɨt</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_397" n="HIAT:w" s="T107">čʼäːnka</ts>
                  <nts id="Seg_398" n="HIAT:ip">.</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_401" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_403" n="HIAT:w" s="T108">Taŋɨt</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_406" n="HIAT:w" s="T109">nɛnoqa</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_409" n="HIAT:w" s="T110">kočʼeŋa</ts>
                  <nts id="Seg_410" n="HIAT:ip">.</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_413" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_415" n="HIAT:w" s="T111">Nɛnoqa</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_418" n="HIAT:w" s="T112">qumɨp</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_421" n="HIAT:w" s="T113">satɨrnɨtɨ</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_424" n="HIAT:w" s="T114">orsa</ts>
                  <nts id="Seg_425" n="HIAT:ip">.</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_428" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_430" n="HIAT:w" s="T115">Ukoːt</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_433" n="HIAT:w" s="T116">mašım</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_436" n="HIAT:w" s="T117">ɛj</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_439" n="HIAT:w" s="T118">satɨrsa</ts>
                  <nts id="Seg_440" n="HIAT:ip">.</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_443" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_445" n="HIAT:w" s="T119">Täm</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_448" n="HIAT:w" s="T120">ɛːsa</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_451" n="HIAT:w" s="T121">ne</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_454" n="HIAT:w" s="T122">qai</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_457" n="HIAT:w" s="T123">nɔːnɨ</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_460" n="HIAT:w" s="T124">aša</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_463" n="HIAT:w" s="T125">ɛnɨsa</ts>
                  <nts id="Seg_464" n="HIAT:ip">.</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_467" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_469" n="HIAT:w" s="T126">Stolqɨn</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_472" n="HIAT:w" s="T127">ippa</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_475" n="HIAT:w" s="T128">kočʼik</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_478" n="HIAT:w" s="T129">näkɨr</ts>
                  <nts id="Seg_479" n="HIAT:ip">.</nts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_482" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_484" n="HIAT:w" s="T130">Näkɨrpatɨ</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_487" n="HIAT:w" s="T131">kɨpɨlʼa</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_490" n="HIAT:w" s="T132">ijalʼa</ts>
                  <nts id="Seg_491" n="HIAT:ip">.</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T133" id="Seg_493" n="sc" s="T0">
               <ts e="T1" id="Seg_495" n="e" s="T0">Kanak </ts>
               <ts e="T2" id="Seg_497" n="e" s="T1">poːqɨt </ts>
               <ts e="T3" id="Seg_499" n="e" s="T2">qonta. </ts>
               <ts e="T4" id="Seg_501" n="e" s="T3">Mat </ts>
               <ts e="T5" id="Seg_503" n="e" s="T4">mačʼä </ts>
               <ts e="T6" id="Seg_505" n="e" s="T5">qonʼišäk </ts>
               <ts e="T7" id="Seg_507" n="e" s="T6">suːrɨlʼlʼä. </ts>
               <ts e="T8" id="Seg_509" n="e" s="T7">Mačʼoːqɨt </ts>
               <ts e="T9" id="Seg_511" n="e" s="T8">orsa </ts>
               <ts e="T10" id="Seg_513" n="e" s="T9">som </ts>
               <ts e="T11" id="Seg_515" n="e" s="T10">ɛːŋa. </ts>
               <ts e="T12" id="Seg_517" n="e" s="T11">Mat </ts>
               <ts e="T13" id="Seg_519" n="e" s="T12">ütɨt </ts>
               <ts e="T14" id="Seg_521" n="e" s="T13">mačʼoːqɨt </ts>
               <ts e="T15" id="Seg_523" n="e" s="T14">suːrɨššäk. </ts>
               <ts e="T16" id="Seg_525" n="e" s="T15">Kutar </ts>
               <ts e="T17" id="Seg_527" n="e" s="T16">ulqap </ts>
               <ts e="T18" id="Seg_529" n="e" s="T17">mišalsɨtɨ, </ts>
               <ts e="T19" id="Seg_531" n="e" s="T18">mat </ts>
               <ts e="T20" id="Seg_533" n="e" s="T19">moqonä </ts>
               <ts e="T21" id="Seg_535" n="e" s="T20">tüsak. </ts>
               <ts e="T22" id="Seg_537" n="e" s="T21">Mat </ts>
               <ts e="T23" id="Seg_539" n="e" s="T22">ütät </ts>
               <ts e="T24" id="Seg_541" n="e" s="T23">koralʼä </ts>
               <ts e="T25" id="Seg_543" n="e" s="T24">qonʼišak. </ts>
               <ts e="T26" id="Seg_545" n="e" s="T25">Mačʼoːqɨt </ts>
               <ts e="T27" id="Seg_547" n="e" s="T26">suːrup </ts>
               <ts e="T28" id="Seg_549" n="e" s="T27">kočʼeŋa. </ts>
               <ts e="T29" id="Seg_551" n="e" s="T28">Kušaŋ </ts>
               <ts e="T30" id="Seg_553" n="e" s="T29">ulqa </ts>
               <ts e="T31" id="Seg_555" n="e" s="T30">mišalsɨtɨ, </ts>
               <ts e="T32" id="Seg_557" n="e" s="T31">meː </ts>
               <ts e="T33" id="Seg_559" n="e" s="T32">qonišem </ts>
               <ts e="T34" id="Seg_561" n="e" s="T33">qäːlɨlʼlʼä </ts>
               <ts e="T35" id="Seg_563" n="e" s="T34">Tuːtɨlʼ </ts>
               <ts e="T36" id="Seg_565" n="e" s="T35">toːntə. </ts>
               <ts e="T37" id="Seg_567" n="e" s="T36">Tuːtɨp </ts>
               <ts e="T38" id="Seg_569" n="e" s="T37">orsa </ts>
               <ts e="T39" id="Seg_571" n="e" s="T38">kočʼik </ts>
               <ts e="T40" id="Seg_573" n="e" s="T39">qässämɨt, </ts>
               <ts e="T41" id="Seg_575" n="e" s="T40">nɨːnɨ </ts>
               <ts e="T42" id="Seg_577" n="e" s="T41">moqona </ts>
               <ts e="T43" id="Seg_579" n="e" s="T42">tüsɔːŋɨt. </ts>
               <ts e="T44" id="Seg_581" n="e" s="T43">Taŋɨt </ts>
               <ts e="T45" id="Seg_583" n="e" s="T44">qumät </ts>
               <ts e="T46" id="Seg_585" n="e" s="T45">qänkɔːtɨn </ts>
               <ts e="T47" id="Seg_587" n="e" s="T46">nʼuːtɨlʼ </ts>
               <ts e="T48" id="Seg_589" n="e" s="T47">pačʼitɨlä. </ts>
               <ts e="T49" id="Seg_591" n="e" s="T48">Araqɨt </ts>
               <ts e="T50" id="Seg_593" n="e" s="T49">moqonä </ts>
               <ts e="T51" id="Seg_595" n="e" s="T50">tükkɔːtän. </ts>
               <ts e="T52" id="Seg_597" n="e" s="T51">Ukkɨr </ts>
               <ts e="T53" id="Seg_599" n="e" s="T52">ira </ts>
               <ts e="T54" id="Seg_601" n="e" s="T53">mačʼontɨ </ts>
               <ts e="T55" id="Seg_603" n="e" s="T54">suːrɨlʼlʼä </ts>
               <ts e="T56" id="Seg_605" n="e" s="T55">qänpa. </ts>
               <ts e="T57" id="Seg_607" n="e" s="T56">Mačʼoːqɨk </ts>
               <ts e="T58" id="Seg_609" n="e" s="T57">suːrɨp </ts>
               <ts e="T59" id="Seg_611" n="e" s="T58">kočʼik </ts>
               <ts e="T60" id="Seg_613" n="e" s="T59">qätpatɨ. </ts>
               <ts e="T61" id="Seg_615" n="e" s="T60">Ukkɨr </ts>
               <ts e="T62" id="Seg_617" n="e" s="T61">ira </ts>
               <ts e="T63" id="Seg_619" n="e" s="T62">ilɨmpa </ts>
               <ts e="T64" id="Seg_621" n="e" s="T63">mačʼoːqɨt. </ts>
               <ts e="T65" id="Seg_623" n="e" s="T64">Kät </ts>
               <ts e="T66" id="Seg_625" n="e" s="T65">suːrɨšʼpä, </ts>
               <ts e="T67" id="Seg_627" n="e" s="T66">a </ts>
               <ts e="T68" id="Seg_629" n="e" s="T67">taŋɨt </ts>
               <ts e="T69" id="Seg_631" n="e" s="T68">meːkkolʼčʼimpatɨ </ts>
               <ts e="T70" id="Seg_633" n="e" s="T69">kınʼčʼip. </ts>
               <ts e="T71" id="Seg_635" n="e" s="T70">Taŋɨt </ts>
               <ts e="T72" id="Seg_637" n="e" s="T71">qätkɨtɨ </ts>
               <ts e="T73" id="Seg_639" n="e" s="T72">kočʼik </ts>
               <ts e="T74" id="Seg_641" n="e" s="T73">qäːlɨp. </ts>
               <ts e="T75" id="Seg_643" n="e" s="T74">Ukkɨr </ts>
               <ts e="T76" id="Seg_645" n="e" s="T75">ijalʼa </ts>
               <ts e="T77" id="Seg_647" n="e" s="T76">üttə </ts>
               <ts e="T78" id="Seg_649" n="e" s="T77">patqɨlpa, </ts>
               <ts e="T79" id="Seg_651" n="e" s="T78">nɨnɨ </ts>
               <ts e="T80" id="Seg_653" n="e" s="T79">qumɨt </ts>
               <ts e="T81" id="Seg_655" n="e" s="T80">konna </ts>
               <ts e="T82" id="Seg_657" n="e" s="T81">iːmpɔːtän. </ts>
               <ts e="T83" id="Seg_659" n="e" s="T82">Poːqɨt </ts>
               <ts e="T84" id="Seg_661" n="e" s="T83">qumɨt </ts>
               <ts e="T85" id="Seg_663" n="e" s="T84">kočʼ </ts>
               <ts e="T86" id="Seg_665" n="e" s="T85">ɛːŋɔːtɨn. </ts>
               <ts e="T87" id="Seg_667" n="e" s="T86">Pit </ts>
               <ts e="T88" id="Seg_669" n="e" s="T87">qontɔːtɨn. </ts>
               <ts e="T89" id="Seg_671" n="e" s="T88">Muntɨk </ts>
               <ts e="T90" id="Seg_673" n="e" s="T89">qarrɨt </ts>
               <ts e="T91" id="Seg_675" n="e" s="T90">qumɨt </ts>
               <ts e="T92" id="Seg_677" n="e" s="T91">uːčʼila </ts>
               <ts e="T93" id="Seg_679" n="e" s="T92">qänkɔːtɨn, </ts>
               <ts e="T94" id="Seg_681" n="e" s="T93">a </ts>
               <ts e="T95" id="Seg_683" n="e" s="T94">üːtɨt </ts>
               <ts e="T96" id="Seg_685" n="e" s="T95">uːčʼila </ts>
               <ts e="T97" id="Seg_687" n="e" s="T96">qɨːqɨlkɔːtɨn. </ts>
               <ts e="T98" id="Seg_689" n="e" s="T97">A </ts>
               <ts e="T99" id="Seg_691" n="e" s="T98">čʼeːlʼit </ts>
               <ts e="T100" id="Seg_693" n="e" s="T99">amɨrqo </ts>
               <ts e="T101" id="Seg_695" n="e" s="T100">qonʼaškɔːten. </ts>
               <ts e="T102" id="Seg_697" n="e" s="T101">Meː </ts>
               <ts e="T103" id="Seg_699" n="e" s="T102">üːtɨt </ts>
               <ts e="T104" id="Seg_701" n="e" s="T103">qonʼišimɨt </ts>
               <ts e="T105" id="Seg_703" n="e" s="T104">qäːlɨlʼlʼä. </ts>
               <ts e="T106" id="Seg_705" n="e" s="T105">Qäːlɨp </ts>
               <ts e="T107" id="Seg_707" n="e" s="T106">qättɛːmɨt </ts>
               <ts e="T108" id="Seg_709" n="e" s="T107">čʼäːnka. </ts>
               <ts e="T109" id="Seg_711" n="e" s="T108">Taŋɨt </ts>
               <ts e="T110" id="Seg_713" n="e" s="T109">nɛnoqa </ts>
               <ts e="T111" id="Seg_715" n="e" s="T110">kočʼeŋa. </ts>
               <ts e="T112" id="Seg_717" n="e" s="T111">Nɛnoqa </ts>
               <ts e="T113" id="Seg_719" n="e" s="T112">qumɨp </ts>
               <ts e="T114" id="Seg_721" n="e" s="T113">satɨrnɨtɨ </ts>
               <ts e="T115" id="Seg_723" n="e" s="T114">orsa. </ts>
               <ts e="T116" id="Seg_725" n="e" s="T115">Ukoːt </ts>
               <ts e="T117" id="Seg_727" n="e" s="T116">mašım </ts>
               <ts e="T118" id="Seg_729" n="e" s="T117">ɛj </ts>
               <ts e="T119" id="Seg_731" n="e" s="T118">satɨrsa. </ts>
               <ts e="T120" id="Seg_733" n="e" s="T119">Täm </ts>
               <ts e="T121" id="Seg_735" n="e" s="T120">ɛːsa </ts>
               <ts e="T122" id="Seg_737" n="e" s="T121">ne </ts>
               <ts e="T123" id="Seg_739" n="e" s="T122">qai </ts>
               <ts e="T124" id="Seg_741" n="e" s="T123">nɔːnɨ </ts>
               <ts e="T125" id="Seg_743" n="e" s="T124">aša </ts>
               <ts e="T126" id="Seg_745" n="e" s="T125">ɛnɨsa. </ts>
               <ts e="T127" id="Seg_747" n="e" s="T126">Stolqɨn </ts>
               <ts e="T128" id="Seg_749" n="e" s="T127">ippa </ts>
               <ts e="T129" id="Seg_751" n="e" s="T128">kočʼik </ts>
               <ts e="T130" id="Seg_753" n="e" s="T129">näkɨr. </ts>
               <ts e="T131" id="Seg_755" n="e" s="T130">Näkɨrpatɨ </ts>
               <ts e="T132" id="Seg_757" n="e" s="T131">kɨpɨlʼa </ts>
               <ts e="T133" id="Seg_759" n="e" s="T132">ijalʼa. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_760" s="T0">KVS_1965_Hunt_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_761" s="T3">KVS_1965_Hunt_nar.002 (001.002)</ta>
            <ta e="T11" id="Seg_762" s="T7">KVS_1965_Hunt_nar.003 (001.003)</ta>
            <ta e="T15" id="Seg_763" s="T11">KVS_1965_Hunt_nar.004 (001.004)</ta>
            <ta e="T21" id="Seg_764" s="T15">KVS_1965_Hunt_nar.005 (001.005)</ta>
            <ta e="T25" id="Seg_765" s="T21">KVS_1965_Hunt_nar.006 (001.006)</ta>
            <ta e="T28" id="Seg_766" s="T25">KVS_1965_Hunt_nar.007 (001.007)</ta>
            <ta e="T36" id="Seg_767" s="T28">KVS_1965_Hunt_nar.008 (001.008)</ta>
            <ta e="T43" id="Seg_768" s="T36">KVS_1965_Hunt_nar.009 (001.009)</ta>
            <ta e="T48" id="Seg_769" s="T43">KVS_1965_Hunt_nar.010 (001.010)</ta>
            <ta e="T51" id="Seg_770" s="T48">KVS_1965_Hunt_nar.011 (001.011)</ta>
            <ta e="T56" id="Seg_771" s="T51">KVS_1965_Hunt_nar.012 (001.012)</ta>
            <ta e="T60" id="Seg_772" s="T56">KVS_1965_Hunt_nar.013 (001.013)</ta>
            <ta e="T64" id="Seg_773" s="T60">KVS_1965_Hunt_nar.014 (001.014)</ta>
            <ta e="T70" id="Seg_774" s="T64">KVS_1965_Hunt_nar.015 (001.015)</ta>
            <ta e="T74" id="Seg_775" s="T70">KVS_1965_Hunt_nar.016 (001.016)</ta>
            <ta e="T82" id="Seg_776" s="T74">KVS_1965_Hunt_nar.017 (001.017)</ta>
            <ta e="T86" id="Seg_777" s="T82">KVS_1965_Hunt_nar.018 (001.018)</ta>
            <ta e="T88" id="Seg_778" s="T86">KVS_1965_Hunt_nar.019 (001.019)</ta>
            <ta e="T97" id="Seg_779" s="T88">KVS_1965_Hunt_nar.020 (001.020)</ta>
            <ta e="T101" id="Seg_780" s="T97">KVS_1965_Hunt_nar.021 (001.021)</ta>
            <ta e="T105" id="Seg_781" s="T101">KVS_1965_Hunt_nar.022 (001.022)</ta>
            <ta e="T108" id="Seg_782" s="T105">KVS_1965_Hunt_nar.023 (001.023)</ta>
            <ta e="T111" id="Seg_783" s="T108">KVS_1965_Hunt_nar.024 (001.024)</ta>
            <ta e="T115" id="Seg_784" s="T111">KVS_1965_Hunt_nar.025 (001.025)</ta>
            <ta e="T119" id="Seg_785" s="T115">KVS_1965_Hunt_nar.026 (001.026)</ta>
            <ta e="T126" id="Seg_786" s="T119">KVS_1965_Hunt_nar.027 (001.027)</ta>
            <ta e="T130" id="Seg_787" s="T126">KVS_1965_Hunt_nar.028 (001.028)</ta>
            <ta e="T133" id="Seg_788" s="T130">KVS_1965_Hunt_nar.029 (001.029)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_789" s="T0">′канак ′по̄kыт ′kонда.</ta>
            <ta e="T7" id="Seg_790" s="T3">мат ′ма̄чʼӓ ′конʼишʼӓк ′сурылʼлʼӓ.</ta>
            <ta e="T11" id="Seg_791" s="T7">ма̄′чоɣ[k]ыт орса сом ′еңа.</ta>
            <ta e="T15" id="Seg_792" s="T11">мат ′ӱ̄тыт ′ма̄чоɣыт ′сурышʼӓ̄к.</ta>
            <ta e="T21" id="Seg_793" s="T15">′кутар ′уlкап ми′шалсыты мат мо′kонӓ ′тӱ̄зак.</ta>
            <ta e="T25" id="Seg_794" s="T21">мат ′ӱтӓт ′коралʼе ′конʼишʼак.</ta>
            <ta e="T28" id="Seg_795" s="T25">ма̄′чоɣыт ′суруп ′ко̄чеңа.</ta>
            <ta e="T36" id="Seg_796" s="T28">ку′шаң ′уlч[к]а ′мӣшʼалсыты ме ′конишем ′kӓlылʼлʼӓ ′тутыl′тонтъ.</ta>
            <ta e="T43" id="Seg_797" s="T36">′тутып [′туттып] ′орса ′кочик ′kӓссӓмыт ′ныны ′моkона ′тӱ̄со̄ңыт.</ta>
            <ta e="T48" id="Seg_798" s="T43">′таңыт ′kумӓт ′kӓ̄н′ко̄тын ′нʼӱ̄тыl ′па̄читылӓ.</ta>
            <ta e="T51" id="Seg_799" s="T48">а′раkыт ′моkонӓ тӱ̄′kо̄тӓн.</ta>
            <ta e="T56" id="Seg_800" s="T51">уккыр ′ира ′ма̄чонты ′сӯрылʼе kӓнпа.</ta>
            <ta e="T60" id="Seg_801" s="T56">ма̄′чоɣ[k]ык ′сурып ′кочик kӓт′паты.</ta>
            <ta e="T64" id="Seg_802" s="T60">′уккыр ′ира ′илымпа ′ма̄чоɣ[k]ыт.</ta>
            <ta e="T70" id="Seg_803" s="T64">kӓт ′суру[ы]шʼпӓ а таңыт ′ме̄коlчимбаты ′кинʼчип.</ta>
            <ta e="T74" id="Seg_804" s="T70">′таңыт ′kӓткыты ′кочʼик ′kӓлып.</ta>
            <ta e="T82" id="Seg_805" s="T74">′уккыр и′jаl[лʼ]а ′ӱттъ ′патkылпа, ′ныны ′kумыт ′конна ′ӣмпотӓн.</ta>
            <ta e="T86" id="Seg_806" s="T82">′поɣыт ′kумыт ′ко̄чʼеңотын.</ta>
            <ta e="T88" id="Seg_807" s="T86">′пит kон′дотын.</ta>
            <ta e="T97" id="Seg_808" s="T88">′мунтык ′kаррыт ′kумыт ′ӯчила ′kӓ̄н′котын, а ′ӱ̄тыт ′ӯчила ′kыɣ[k]ылко̄тын.</ta>
            <ta e="T101" id="Seg_809" s="T97">а ′чеlит ′а̄мырkо ′конʼашkотен.</ta>
            <ta e="T105" id="Seg_810" s="T101">ме ′ӱ̄тыт ′конʼишимыт ′кӓ̄лылʼе.</ta>
            <ta e="T108" id="Seg_811" s="T105">′kӓлып kӓ′тӓмыт ′ченка.</ta>
            <ta e="T111" id="Seg_812" s="T108">′таңыт ′нӓноkа кочеңа.</ta>
            <ta e="T115" id="Seg_813" s="T111">′нӓноɣа ′kумып ′сатырныты ′орса.</ta>
            <ta e="T119" id="Seg_814" s="T115">у′кот машим ей ′сатырса.</ta>
            <ta e="T126" id="Seg_815" s="T119">тӓм ′ӓса не ′kаиноны ′аша ′ӓныса.</ta>
            <ta e="T130" id="Seg_816" s="T126">′столkын ′иппа ′кочик ′нӓкыр.</ta>
            <ta e="T133" id="Seg_817" s="T130">′нӓкырпаты ′кыпыlа ′иjаlа.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_818" s="T0">kanak poːqɨt qonda.</ta>
            <ta e="T7" id="Seg_819" s="T3">mat maːčʼä konʼišäk surɨlʼlʼä.</ta>
            <ta e="T11" id="Seg_820" s="T7">maːčoq[q]ɨt orsa som eңa.</ta>
            <ta e="T15" id="Seg_821" s="T11">mat üːtɨt maːčoqɨt surɨšäːk.</ta>
            <ta e="T21" id="Seg_822" s="T15">kutar ulʼkap mišalsɨtɨ mat moqonä tüːzak.</ta>
            <ta e="T25" id="Seg_823" s="T21">mat ütät koralʼe konʼišak.</ta>
            <ta e="T28" id="Seg_824" s="T25">maːčoqɨt surup koːčeңa.</ta>
            <ta e="T36" id="Seg_825" s="T28">kušaң ulʼč[k]a miːšalsɨtɨ me konišem qälʼɨlʼlʼä tutɨlʼtontə.</ta>
            <ta e="T43" id="Seg_826" s="T36">tutɨp [tuttɨp] orsa kočik qässämɨt nɨnɨ moqona tüːsoːңɨt.</ta>
            <ta e="T48" id="Seg_827" s="T43">taңɨt qumät qäːnkoːtɨn nʼüːtɨlʼ paːčitɨlä.</ta>
            <ta e="T51" id="Seg_828" s="T48">araqɨt moqonä tüːqoːtän.</ta>
            <ta e="T56" id="Seg_829" s="T51">ukkɨr ira maːčontɨ suːrɨlʼe qänpa.</ta>
            <ta e="T60" id="Seg_830" s="T56">maːčoq[q]ɨk surɨp kočik qätpatɨ.</ta>
            <ta e="T64" id="Seg_831" s="T60">ukkɨr ira ilɨmpa maːčoq[q]ɨt.</ta>
            <ta e="T70" id="Seg_832" s="T64">qät suru[ɨ]šʼpä a taңɨt meːkolʼčimpatɨ kinʼčip.</ta>
            <ta e="T74" id="Seg_833" s="T70">taңɨt qätkɨtɨ kočʼik qälɨp.</ta>
            <ta e="T82" id="Seg_834" s="T74">ukkɨr ijalʼ[lʼ]a üttə patqɨlpa, nɨnɨ qumɨt konna iːmpotän.</ta>
            <ta e="T86" id="Seg_835" s="T82">poqɨt qumɨt koːčʼeңotɨn.</ta>
            <ta e="T88" id="Seg_836" s="T86">pit qondotɨn.</ta>
            <ta e="T97" id="Seg_837" s="T88">muntɨk qarrɨt qumɨt uːčila qäːnkotɨn, a üːtɨt uːčila qɨq[q]ɨlkoːtɨn.</ta>
            <ta e="T101" id="Seg_838" s="T97">a čelʼit aːmɨrqo konʼašqoten.</ta>
            <ta e="T105" id="Seg_839" s="T101">me üːtɨt konʼišimɨt käːlɨlʼe.</ta>
            <ta e="T108" id="Seg_840" s="T105">qälɨp qätämɨt čenka.</ta>
            <ta e="T111" id="Seg_841" s="T108">taңɨt nänoqa kočeңa.</ta>
            <ta e="T115" id="Seg_842" s="T111">nänoqa qumɨp satɨrnɨtɨ orsa.</ta>
            <ta e="T119" id="Seg_843" s="T115">ukot mašim ej satɨrsa.</ta>
            <ta e="T126" id="Seg_844" s="T119">täm äsa ne qainonɨ aša änɨsa.</ta>
            <ta e="T130" id="Seg_845" s="T126">stolqɨn ippa kočik näkɨr.</ta>
            <ta e="T133" id="Seg_846" s="T130">näkɨrpatɨ kɨpɨlʼa ijalʼa.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_847" s="T0">Kanak poːqɨt qonta. </ta>
            <ta e="T7" id="Seg_848" s="T3">Mat mačʼä qonʼišäk suːrɨlʼlʼä. </ta>
            <ta e="T11" id="Seg_849" s="T7">Mačʼoːqɨt orsa som ɛːŋa. </ta>
            <ta e="T15" id="Seg_850" s="T11">Mat ütɨt mačʼoːqɨt suːrɨššäk. </ta>
            <ta e="T21" id="Seg_851" s="T15">Kutar ulqap mišalsɨtɨ, mat moqonä tüsak. </ta>
            <ta e="T25" id="Seg_852" s="T21">Mat ütät koralʼä qonʼišak. </ta>
            <ta e="T28" id="Seg_853" s="T25">Mačʼoːqɨt suːrup kočʼeŋa. </ta>
            <ta e="T36" id="Seg_854" s="T28">Kušaŋ ulqa mišalsɨtɨ, meː qonišem qäːlɨlʼlʼä Tuːtɨlʼ toːntə. </ta>
            <ta e="T43" id="Seg_855" s="T36">Tuːtɨp orsa kočʼik qässämɨt, nɨːnɨ moqona tüsɔːŋɨt. </ta>
            <ta e="T48" id="Seg_856" s="T43">Taŋɨt qumät qänkɔːtɨn nʼuːtɨlʼ pačʼitɨlä. </ta>
            <ta e="T51" id="Seg_857" s="T48">Araqɨt moqonä tükkɔːtän. </ta>
            <ta e="T56" id="Seg_858" s="T51">Ukkɨr ira mačʼontɨ suːrɨlʼlʼä qänpa. </ta>
            <ta e="T60" id="Seg_859" s="T56">Mačʼoːqɨk suːrɨp kočʼik qätpatɨ. </ta>
            <ta e="T64" id="Seg_860" s="T60">Ukkɨr ira ilɨmpa mačʼoːqɨt. </ta>
            <ta e="T70" id="Seg_861" s="T64">Kät suːrɨšʼpä, a taŋɨt meːkkolʼčʼimpatɨ kınʼčʼip. </ta>
            <ta e="T74" id="Seg_862" s="T70">Taŋɨt qätkɨtɨ kočʼik qäːlɨp. </ta>
            <ta e="T82" id="Seg_863" s="T74">Ukkɨr ijalʼa üttə patqɨlpa, nɨnɨ qumɨt konna iːmpɔːtän. </ta>
            <ta e="T86" id="Seg_864" s="T82">Poːqɨt qumɨt kočʼ ɛːŋɔːtɨn. </ta>
            <ta e="T88" id="Seg_865" s="T86">Pit qontɔːtɨn. </ta>
            <ta e="T97" id="Seg_866" s="T88">Muntɨk qarrɨt qumɨt uːčʼila qänkɔːtɨn, a üːtɨt uːčʼila qɨːqɨlkɔːtɨn. </ta>
            <ta e="T101" id="Seg_867" s="T97">A čʼeːlʼit amɨrqo qonʼaškɔːten. </ta>
            <ta e="T105" id="Seg_868" s="T101">Meː üːtɨt qonʼišimɨt qäːlɨlʼlʼä. </ta>
            <ta e="T108" id="Seg_869" s="T105">Qäːlɨp qättɛːmɨt čʼäːnka. </ta>
            <ta e="T111" id="Seg_870" s="T108">Taŋɨt nɛnoqa kočʼeŋa. </ta>
            <ta e="T115" id="Seg_871" s="T111">Nɛnoqa qumɨp satɨrnɨtɨ orsa. </ta>
            <ta e="T119" id="Seg_872" s="T115">Ukoːt mašım ɛj satɨrsa. </ta>
            <ta e="T126" id="Seg_873" s="T119">Täm ɛːsa ne qai nɔːnɨ aša ɛnɨsa. </ta>
            <ta e="T130" id="Seg_874" s="T126">Stolqɨn ippa kočʼik näkɨr. </ta>
            <ta e="T133" id="Seg_875" s="T130">Näkɨrpatɨ kɨpɨlʼa ijalʼa. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_876" s="T0">kanak</ta>
            <ta e="T2" id="Seg_877" s="T1">poː-qɨt</ta>
            <ta e="T3" id="Seg_878" s="T2">qonta</ta>
            <ta e="T4" id="Seg_879" s="T3">mat</ta>
            <ta e="T5" id="Seg_880" s="T4">mačʼä</ta>
            <ta e="T6" id="Seg_881" s="T5">qonʼ-i-šä-k</ta>
            <ta e="T7" id="Seg_882" s="T6">suːrɨ-lʼ-lʼä</ta>
            <ta e="T8" id="Seg_883" s="T7">mačʼoː-qɨt</ta>
            <ta e="T9" id="Seg_884" s="T8">or-sa</ta>
            <ta e="T10" id="Seg_885" s="T9">som</ta>
            <ta e="T11" id="Seg_886" s="T10">ɛː-ŋa</ta>
            <ta e="T12" id="Seg_887" s="T11">mat</ta>
            <ta e="T13" id="Seg_888" s="T12">ütɨ-t</ta>
            <ta e="T14" id="Seg_889" s="T13">mačʼoː-qɨt</ta>
            <ta e="T15" id="Seg_890" s="T14">suːrɨ-š-šä-k</ta>
            <ta e="T16" id="Seg_891" s="T15">kutar</ta>
            <ta e="T17" id="Seg_892" s="T16">ulqa-p</ta>
            <ta e="T18" id="Seg_893" s="T17">mišal-sɨ-tɨ</ta>
            <ta e="T19" id="Seg_894" s="T18">mat</ta>
            <ta e="T20" id="Seg_895" s="T19">moqonä</ta>
            <ta e="T21" id="Seg_896" s="T20">tü-sa-k</ta>
            <ta e="T22" id="Seg_897" s="T21">mat</ta>
            <ta e="T23" id="Seg_898" s="T22">ütä-t</ta>
            <ta e="T24" id="Seg_899" s="T23">kora-lʼä</ta>
            <ta e="T25" id="Seg_900" s="T24">qonʼ-i-ša-k</ta>
            <ta e="T26" id="Seg_901" s="T25">mačʼoː-qɨt</ta>
            <ta e="T27" id="Seg_902" s="T26">suːrup</ta>
            <ta e="T28" id="Seg_903" s="T27">kočʼeŋa</ta>
            <ta e="T29" id="Seg_904" s="T28">kuša-ŋ</ta>
            <ta e="T30" id="Seg_905" s="T29">ulqa</ta>
            <ta e="T31" id="Seg_906" s="T30">mišal-sɨ-tɨ</ta>
            <ta e="T32" id="Seg_907" s="T31">meː</ta>
            <ta e="T33" id="Seg_908" s="T32">qon-i-še-m</ta>
            <ta e="T34" id="Seg_909" s="T33">qäːlɨ-lʼ-lʼä</ta>
            <ta e="T35" id="Seg_910" s="T34">tuːtɨ-lʼ</ta>
            <ta e="T36" id="Seg_911" s="T35">toː-ntə</ta>
            <ta e="T37" id="Seg_912" s="T36">tuːtɨ-p</ta>
            <ta e="T38" id="Seg_913" s="T37">or-sa</ta>
            <ta e="T39" id="Seg_914" s="T38">kočʼi-k</ta>
            <ta e="T40" id="Seg_915" s="T39">qäs-sä-mɨt</ta>
            <ta e="T41" id="Seg_916" s="T40">nɨːnɨ</ta>
            <ta e="T42" id="Seg_917" s="T41">moqona</ta>
            <ta e="T43" id="Seg_918" s="T42">tü-sɔː-ŋɨt</ta>
            <ta e="T44" id="Seg_919" s="T43">taŋɨ-t</ta>
            <ta e="T45" id="Seg_920" s="T44">qum-ä-t</ta>
            <ta e="T46" id="Seg_921" s="T45">qän-kɔː-tɨn</ta>
            <ta e="T47" id="Seg_922" s="T46">nʼuːtɨ-lʼ</ta>
            <ta e="T48" id="Seg_923" s="T47">pačʼi-tɨ-lä</ta>
            <ta e="T49" id="Seg_924" s="T48">ara-qɨt</ta>
            <ta e="T50" id="Seg_925" s="T49">moqonä</ta>
            <ta e="T51" id="Seg_926" s="T50">tü-kkɔː-tän</ta>
            <ta e="T52" id="Seg_927" s="T51">ukkɨr</ta>
            <ta e="T53" id="Seg_928" s="T52">ira</ta>
            <ta e="T54" id="Seg_929" s="T53">mačʼo-ntɨ</ta>
            <ta e="T55" id="Seg_930" s="T54">suːrɨ-lʼ-lʼä</ta>
            <ta e="T56" id="Seg_931" s="T55">qän-pa</ta>
            <ta e="T57" id="Seg_932" s="T56">mačʼoː-qɨk</ta>
            <ta e="T58" id="Seg_933" s="T57">suːrɨp</ta>
            <ta e="T59" id="Seg_934" s="T58">kočʼi-k</ta>
            <ta e="T60" id="Seg_935" s="T59">qät-pa-tɨ</ta>
            <ta e="T61" id="Seg_936" s="T60">ukkɨr</ta>
            <ta e="T62" id="Seg_937" s="T61">ira</ta>
            <ta e="T63" id="Seg_938" s="T62">ilɨ-mpa</ta>
            <ta e="T64" id="Seg_939" s="T63">mačʼoː-qɨt</ta>
            <ta e="T65" id="Seg_940" s="T64">kä-t</ta>
            <ta e="T66" id="Seg_941" s="T65">suːrɨ-šʼ-pä</ta>
            <ta e="T67" id="Seg_942" s="T66">a</ta>
            <ta e="T68" id="Seg_943" s="T67">taŋɨ-t</ta>
            <ta e="T69" id="Seg_944" s="T68">meː-kko-lʼčʼi-mpa-tɨ</ta>
            <ta e="T70" id="Seg_945" s="T69">kınʼčʼi-p</ta>
            <ta e="T71" id="Seg_946" s="T70">taŋɨ-t</ta>
            <ta e="T72" id="Seg_947" s="T71">qät-kɨ-tɨ</ta>
            <ta e="T73" id="Seg_948" s="T72">kočʼi-k</ta>
            <ta e="T74" id="Seg_949" s="T73">qäːlɨ-p</ta>
            <ta e="T75" id="Seg_950" s="T74">ukkɨr</ta>
            <ta e="T76" id="Seg_951" s="T75">ija-lʼa</ta>
            <ta e="T77" id="Seg_952" s="T76">üt-tə</ta>
            <ta e="T78" id="Seg_953" s="T77">pat-qɨl-pa</ta>
            <ta e="T79" id="Seg_954" s="T78">nɨnɨ</ta>
            <ta e="T80" id="Seg_955" s="T79">qum-ɨ-t</ta>
            <ta e="T81" id="Seg_956" s="T80">konna</ta>
            <ta e="T82" id="Seg_957" s="T81">iː-mpɔː-tän</ta>
            <ta e="T83" id="Seg_958" s="T82">poː-qɨt</ta>
            <ta e="T84" id="Seg_959" s="T83">qum-ɨ-t</ta>
            <ta e="T85" id="Seg_960" s="T84">kočʼ</ta>
            <ta e="T86" id="Seg_961" s="T85">ɛː-ŋɔː-tɨn</ta>
            <ta e="T87" id="Seg_962" s="T86">pi-t</ta>
            <ta e="T88" id="Seg_963" s="T87">qontɔː-tɨn</ta>
            <ta e="T89" id="Seg_964" s="T88">muntɨk</ta>
            <ta e="T90" id="Seg_965" s="T89">qarrɨ-t</ta>
            <ta e="T91" id="Seg_966" s="T90">qum-ɨ-t</ta>
            <ta e="T92" id="Seg_967" s="T91">uːčʼi-la</ta>
            <ta e="T93" id="Seg_968" s="T92">qän-kɔː-tɨn</ta>
            <ta e="T94" id="Seg_969" s="T93">a</ta>
            <ta e="T95" id="Seg_970" s="T94">üːtɨ-t</ta>
            <ta e="T96" id="Seg_971" s="T95">uːčʼi-la</ta>
            <ta e="T97" id="Seg_972" s="T96">qɨː-qɨl-kɔː-tɨn</ta>
            <ta e="T98" id="Seg_973" s="T97">a</ta>
            <ta e="T99" id="Seg_974" s="T98">čʼeːlʼi-t</ta>
            <ta e="T100" id="Seg_975" s="T99">am-ɨ-r-qo</ta>
            <ta e="T101" id="Seg_976" s="T100">qonʼ-a-š-kɔː-ten</ta>
            <ta e="T102" id="Seg_977" s="T101">meː</ta>
            <ta e="T103" id="Seg_978" s="T102">üːtɨ-t</ta>
            <ta e="T104" id="Seg_979" s="T103">qonʼ-i-ši-mɨt</ta>
            <ta e="T105" id="Seg_980" s="T104">qäːlɨ-lʼ-lʼä</ta>
            <ta e="T106" id="Seg_981" s="T105">qäːlɨ-p</ta>
            <ta e="T107" id="Seg_982" s="T106">qätt-ɛː-mɨt</ta>
            <ta e="T108" id="Seg_983" s="T107">čʼäːnka</ta>
            <ta e="T109" id="Seg_984" s="T108">taŋɨ-t</ta>
            <ta e="T110" id="Seg_985" s="T109">nɛnoqa</ta>
            <ta e="T111" id="Seg_986" s="T110">kočʼeŋa</ta>
            <ta e="T112" id="Seg_987" s="T111">nɛnoqa</ta>
            <ta e="T113" id="Seg_988" s="T112">qum-ɨ-p</ta>
            <ta e="T114" id="Seg_989" s="T113">satɨ-r-nɨ-tɨ</ta>
            <ta e="T115" id="Seg_990" s="T114">or-sa</ta>
            <ta e="T116" id="Seg_991" s="T115">ukoːt</ta>
            <ta e="T117" id="Seg_992" s="T116">mašım</ta>
            <ta e="T118" id="Seg_993" s="T117">ɛj</ta>
            <ta e="T119" id="Seg_994" s="T118">satɨ-r-sa</ta>
            <ta e="T120" id="Seg_995" s="T119">täm</ta>
            <ta e="T121" id="Seg_996" s="T120">ɛː-sa</ta>
            <ta e="T122" id="Seg_997" s="T121">ne</ta>
            <ta e="T123" id="Seg_998" s="T122">qai</ta>
            <ta e="T124" id="Seg_999" s="T123">nɔː-nɨ</ta>
            <ta e="T125" id="Seg_1000" s="T124">aša</ta>
            <ta e="T126" id="Seg_1001" s="T125">ɛnɨ-sa</ta>
            <ta e="T127" id="Seg_1002" s="T126">stol-qɨn</ta>
            <ta e="T128" id="Seg_1003" s="T127">ippa</ta>
            <ta e="T129" id="Seg_1004" s="T128">kočʼi-k</ta>
            <ta e="T130" id="Seg_1005" s="T129">näkɨr</ta>
            <ta e="T131" id="Seg_1006" s="T130">näkɨ-r-pa-tɨ</ta>
            <ta e="T132" id="Seg_1007" s="T131">kɨpɨ-lʼa</ta>
            <ta e="T133" id="Seg_1008" s="T132">ija-lʼa</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1009" s="T0">kanaŋ</ta>
            <ta e="T2" id="Seg_1010" s="T1">poː-qɨn</ta>
            <ta e="T3" id="Seg_1011" s="T2">qontɨ</ta>
            <ta e="T4" id="Seg_1012" s="T3">man</ta>
            <ta e="T5" id="Seg_1013" s="T4">mačʼä</ta>
            <ta e="T6" id="Seg_1014" s="T5">qən-ɨ-sɨ-k</ta>
            <ta e="T7" id="Seg_1015" s="T6">suːrɨm-š-lä</ta>
            <ta e="T8" id="Seg_1016" s="T7">mačʼɨ-qɨn</ta>
            <ta e="T9" id="Seg_1017" s="T8">orɨ-sä</ta>
            <ta e="T10" id="Seg_1018" s="T9">soma</ta>
            <ta e="T11" id="Seg_1019" s="T10">ɛː-ŋɨ</ta>
            <ta e="T12" id="Seg_1020" s="T11">man</ta>
            <ta e="T13" id="Seg_1021" s="T12">üttɨ-k</ta>
            <ta e="T14" id="Seg_1022" s="T13">mačʼɨ-qɨn</ta>
            <ta e="T15" id="Seg_1023" s="T14">suːrɨm-š-sɨ-k</ta>
            <ta e="T16" id="Seg_1024" s="T15">kuttar</ta>
            <ta e="T17" id="Seg_1025" s="T16">ulqa-m</ta>
            <ta e="T18" id="Seg_1026" s="T17">mišal-sɨ-tɨ</ta>
            <ta e="T19" id="Seg_1027" s="T18">man</ta>
            <ta e="T20" id="Seg_1028" s="T19">moqɨnä</ta>
            <ta e="T21" id="Seg_1029" s="T20">tü-sɨ-k</ta>
            <ta e="T22" id="Seg_1030" s="T21">man</ta>
            <ta e="T23" id="Seg_1031" s="T22">üttɨ-k</ta>
            <ta e="T24" id="Seg_1032" s="T23">kora-lä</ta>
            <ta e="T25" id="Seg_1033" s="T24">qən-ɨ-sɨ-k</ta>
            <ta e="T26" id="Seg_1034" s="T25">mačʼɨ-qɨn</ta>
            <ta e="T27" id="Seg_1035" s="T26">suːrɨm</ta>
            <ta e="T28" id="Seg_1036" s="T27">kočʼčʼɨ</ta>
            <ta e="T29" id="Seg_1037" s="T28">*kušša-k</ta>
            <ta e="T30" id="Seg_1038" s="T29">ulqa</ta>
            <ta e="T31" id="Seg_1039" s="T30">mišal-sɨ-tɨ</ta>
            <ta e="T32" id="Seg_1040" s="T31">meː</ta>
            <ta e="T33" id="Seg_1041" s="T32">qən-ɨ-sɨ-mɨt</ta>
            <ta e="T34" id="Seg_1042" s="T33">qəːlɨ-š-lä</ta>
            <ta e="T35" id="Seg_1043" s="T34">tuːtɨ-lʼ</ta>
            <ta e="T36" id="Seg_1044" s="T35">toː-ntɨ</ta>
            <ta e="T37" id="Seg_1045" s="T36">tuːtɨ-m</ta>
            <ta e="T38" id="Seg_1046" s="T37">orɨ-sä</ta>
            <ta e="T39" id="Seg_1047" s="T38">kočʼčʼɨ-k</ta>
            <ta e="T40" id="Seg_1048" s="T39">qət-sɨ-mɨt</ta>
            <ta e="T41" id="Seg_1049" s="T40">nɨːnɨ</ta>
            <ta e="T42" id="Seg_1050" s="T41">moqɨnä</ta>
            <ta e="T43" id="Seg_1051" s="T42">tü-sɨ-mɨt</ta>
            <ta e="T44" id="Seg_1052" s="T43">taŋɨ-n</ta>
            <ta e="T45" id="Seg_1053" s="T44">qum-ɨ-t</ta>
            <ta e="T46" id="Seg_1054" s="T45">qən-kkɨ-tɨt</ta>
            <ta e="T47" id="Seg_1055" s="T46">nʼuːtɨ-lʼ</ta>
            <ta e="T48" id="Seg_1056" s="T47">pačʼčʼɨ-tɨ-lä</ta>
            <ta e="T49" id="Seg_1057" s="T48">ara-qɨn</ta>
            <ta e="T50" id="Seg_1058" s="T49">moqɨnä</ta>
            <ta e="T51" id="Seg_1059" s="T50">tü-kkɨ-tɨt</ta>
            <ta e="T52" id="Seg_1060" s="T51">ukkɨr</ta>
            <ta e="T53" id="Seg_1061" s="T52">ira</ta>
            <ta e="T54" id="Seg_1062" s="T53">mačʼɨ-ntɨ</ta>
            <ta e="T55" id="Seg_1063" s="T54">suːrɨm-š-lä</ta>
            <ta e="T56" id="Seg_1064" s="T55">qən-mpɨ</ta>
            <ta e="T57" id="Seg_1065" s="T56">mačʼɨ-qɨn</ta>
            <ta e="T58" id="Seg_1066" s="T57">suːrɨm</ta>
            <ta e="T59" id="Seg_1067" s="T58">kočʼčʼɨ-k</ta>
            <ta e="T60" id="Seg_1068" s="T59">qət-mpɨ-tɨ</ta>
            <ta e="T61" id="Seg_1069" s="T60">ukkɨr</ta>
            <ta e="T62" id="Seg_1070" s="T61">ira</ta>
            <ta e="T63" id="Seg_1071" s="T62">ilɨ-mpɨ</ta>
            <ta e="T64" id="Seg_1072" s="T63">mačʼɨ-qɨn</ta>
            <ta e="T65" id="Seg_1073" s="T64">kə-k</ta>
            <ta e="T66" id="Seg_1074" s="T65">suːrɨm-š-mpɨ</ta>
            <ta e="T67" id="Seg_1075" s="T66">a</ta>
            <ta e="T68" id="Seg_1076" s="T67">taŋɨ-n</ta>
            <ta e="T69" id="Seg_1077" s="T68">meː-kkɨ-lʼčʼɨ-mpɨ-tɨ</ta>
            <ta e="T70" id="Seg_1078" s="T69">kınʼčʼɨ-m</ta>
            <ta e="T71" id="Seg_1079" s="T70">taŋɨ-n</ta>
            <ta e="T72" id="Seg_1080" s="T71">qət-kkɨ-tɨ</ta>
            <ta e="T73" id="Seg_1081" s="T72">kočʼčʼɨ-k</ta>
            <ta e="T74" id="Seg_1082" s="T73">qəːlɨ-m</ta>
            <ta e="T75" id="Seg_1083" s="T74">ukkɨr</ta>
            <ta e="T76" id="Seg_1084" s="T75">iːja-lʼa</ta>
            <ta e="T77" id="Seg_1085" s="T76">üt-ntɨ</ta>
            <ta e="T78" id="Seg_1086" s="T77">pat-qɨl-mpɨ</ta>
            <ta e="T79" id="Seg_1087" s="T78">nɨːnɨ</ta>
            <ta e="T80" id="Seg_1088" s="T79">qum-ɨ-t</ta>
            <ta e="T81" id="Seg_1089" s="T80">konnä</ta>
            <ta e="T82" id="Seg_1090" s="T81">iː-mpɨ-tɨt</ta>
            <ta e="T83" id="Seg_1091" s="T82">poː-qɨn</ta>
            <ta e="T84" id="Seg_1092" s="T83">qum-ɨ-t</ta>
            <ta e="T85" id="Seg_1093" s="T84">kočʼčʼɨ</ta>
            <ta e="T86" id="Seg_1094" s="T85">ɛː-ŋɨ-tɨt</ta>
            <ta e="T87" id="Seg_1095" s="T86">pi-n</ta>
            <ta e="T88" id="Seg_1096" s="T87">qontɨ-tɨt</ta>
            <ta e="T89" id="Seg_1097" s="T88">muntɨk</ta>
            <ta e="T90" id="Seg_1098" s="T89">qarɨ-n</ta>
            <ta e="T91" id="Seg_1099" s="T90">qum-ɨ-t</ta>
            <ta e="T92" id="Seg_1100" s="T91">uːčʼɨ-lä</ta>
            <ta e="T93" id="Seg_1101" s="T92">qən-kkɨ-tɨt</ta>
            <ta e="T94" id="Seg_1102" s="T93">a</ta>
            <ta e="T95" id="Seg_1103" s="T94">üːtɨ-n</ta>
            <ta e="T96" id="Seg_1104" s="T95">uːčʼɨ-lä</ta>
            <ta e="T97" id="Seg_1105" s="T96">qɨː-qɨl-kkɨ-tɨt</ta>
            <ta e="T98" id="Seg_1106" s="T97">a</ta>
            <ta e="T99" id="Seg_1107" s="T98">čʼeːlɨ-n</ta>
            <ta e="T100" id="Seg_1108" s="T99">am-ɨ-r-qo</ta>
            <ta e="T101" id="Seg_1109" s="T100">qən-ɨ-š-kkɨ-tɨt</ta>
            <ta e="T102" id="Seg_1110" s="T101">meː</ta>
            <ta e="T103" id="Seg_1111" s="T102">üːtɨ-n</ta>
            <ta e="T104" id="Seg_1112" s="T103">qən-ɨ-sɨ-mɨt</ta>
            <ta e="T105" id="Seg_1113" s="T104">qəːlɨ-š-lä</ta>
            <ta e="T106" id="Seg_1114" s="T105">qəːlɨ-m</ta>
            <ta e="T107" id="Seg_1115" s="T106">qət-ɛː-mɨt</ta>
            <ta e="T108" id="Seg_1116" s="T107">čʼäːŋkɨ</ta>
            <ta e="T109" id="Seg_1117" s="T108">taŋɨ-n</ta>
            <ta e="T110" id="Seg_1118" s="T109">nɛnɨqa</ta>
            <ta e="T111" id="Seg_1119" s="T110">kočʼčʼɨ</ta>
            <ta e="T112" id="Seg_1120" s="T111">nɛnɨqa</ta>
            <ta e="T113" id="Seg_1121" s="T112">qum-ɨ-m</ta>
            <ta e="T114" id="Seg_1122" s="T113">satɨ-r-ŋɨ-tɨ</ta>
            <ta e="T115" id="Seg_1123" s="T114">orɨ-sä</ta>
            <ta e="T116" id="Seg_1124" s="T115">ukoːn</ta>
            <ta e="T117" id="Seg_1125" s="T116">mašım</ta>
            <ta e="T118" id="Seg_1126" s="T117">aj</ta>
            <ta e="T119" id="Seg_1127" s="T118">satɨ-r-sɨ</ta>
            <ta e="T120" id="Seg_1128" s="T119">təp</ta>
            <ta e="T121" id="Seg_1129" s="T120">ɛː-sɨ</ta>
            <ta e="T122" id="Seg_1130" s="T121">nʼi</ta>
            <ta e="T123" id="Seg_1131" s="T122">qaj</ta>
            <ta e="T124" id="Seg_1132" s="T123">*nɔː-nɨ</ta>
            <ta e="T125" id="Seg_1133" s="T124">ašša</ta>
            <ta e="T126" id="Seg_1134" s="T125">ɛnɨ-sɨ</ta>
            <ta e="T127" id="Seg_1135" s="T126">ostol-qɨn</ta>
            <ta e="T128" id="Seg_1136" s="T127">ippɨ</ta>
            <ta e="T129" id="Seg_1137" s="T128">kočʼčʼɨ-k</ta>
            <ta e="T130" id="Seg_1138" s="T129">nəkɨ</ta>
            <ta e="T131" id="Seg_1139" s="T130">nəkɨ-r-mpɨ-tɨ</ta>
            <ta e="T132" id="Seg_1140" s="T131">kɨpa-lʼa</ta>
            <ta e="T133" id="Seg_1141" s="T132">iːja-lʼa</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1142" s="T0">dog.[NOM]</ta>
            <ta e="T2" id="Seg_1143" s="T1">space.outside-LOC</ta>
            <ta e="T3" id="Seg_1144" s="T2">sleep.[3SG.S]</ta>
            <ta e="T4" id="Seg_1145" s="T3">I.NOM</ta>
            <ta e="T5" id="Seg_1146" s="T4">to.the.forest</ta>
            <ta e="T6" id="Seg_1147" s="T5">go.away-EP-PST-1SG.S</ta>
            <ta e="T7" id="Seg_1148" s="T6">wild.animal-VBLZ-CVB</ta>
            <ta e="T8" id="Seg_1149" s="T7">forest-LOC</ta>
            <ta e="T9" id="Seg_1150" s="T8">force-INSTR</ta>
            <ta e="T10" id="Seg_1151" s="T9">good</ta>
            <ta e="T11" id="Seg_1152" s="T10">be-CO.[3SG.S]</ta>
            <ta e="T12" id="Seg_1153" s="T11">I.NOM</ta>
            <ta e="T13" id="Seg_1154" s="T12">spring-ADVZ</ta>
            <ta e="T14" id="Seg_1155" s="T13">forest-LOC</ta>
            <ta e="T15" id="Seg_1156" s="T14">wild.animal-VBLZ-PST-1SG.S</ta>
            <ta e="T16" id="Seg_1157" s="T15">how</ta>
            <ta e="T17" id="Seg_1158" s="T16">ice-ACC</ta>
            <ta e="T18" id="Seg_1159" s="T17">pull-PST-3SG.O</ta>
            <ta e="T19" id="Seg_1160" s="T18">I.NOM</ta>
            <ta e="T20" id="Seg_1161" s="T19">home</ta>
            <ta e="T21" id="Seg_1162" s="T20">come-PST-1SG.S</ta>
            <ta e="T22" id="Seg_1163" s="T21">I.NOM</ta>
            <ta e="T23" id="Seg_1164" s="T22">spring-ADVZ</ta>
            <ta e="T24" id="Seg_1165" s="T23">go.hunting-CVB</ta>
            <ta e="T25" id="Seg_1166" s="T24">leave-EP-PST-1SG.S</ta>
            <ta e="T26" id="Seg_1167" s="T25">forest-LOC</ta>
            <ta e="T27" id="Seg_1168" s="T26">wild.animal.[NOM]</ta>
            <ta e="T28" id="Seg_1169" s="T27">much</ta>
            <ta e="T29" id="Seg_1170" s="T28">interrog.pron.stem-ADVZ</ta>
            <ta e="T30" id="Seg_1171" s="T29">ice.[NOM]</ta>
            <ta e="T31" id="Seg_1172" s="T30">pull-PST-3SG.O</ta>
            <ta e="T32" id="Seg_1173" s="T31">we.PL.NOM</ta>
            <ta e="T33" id="Seg_1174" s="T32">leave-EP-PST-1PL</ta>
            <ta e="T34" id="Seg_1175" s="T33">fish-VBLZ-CVB</ta>
            <ta e="T35" id="Seg_1176" s="T34">crucian-ADJZ</ta>
            <ta e="T36" id="Seg_1177" s="T35">lake-ILL</ta>
            <ta e="T37" id="Seg_1178" s="T36">crucian-ACC</ta>
            <ta e="T38" id="Seg_1179" s="T37">force-INSTR</ta>
            <ta e="T39" id="Seg_1180" s="T38">much-ADVZ</ta>
            <ta e="T40" id="Seg_1181" s="T39">kill-PST-1PL</ta>
            <ta e="T41" id="Seg_1182" s="T40">then</ta>
            <ta e="T42" id="Seg_1183" s="T41">home</ta>
            <ta e="T43" id="Seg_1184" s="T42">come-PST-1PL</ta>
            <ta e="T44" id="Seg_1185" s="T43">summer-ADV.LOC</ta>
            <ta e="T45" id="Seg_1186" s="T44">human.being-EP-PL.[NOM]</ta>
            <ta e="T46" id="Seg_1187" s="T45">go.away-DUR-3PL</ta>
            <ta e="T47" id="Seg_1188" s="T46">hay-ADJZ</ta>
            <ta e="T48" id="Seg_1189" s="T47">chop-HAB-CVB</ta>
            <ta e="T49" id="Seg_1190" s="T48">autumn-ADV.LOC</ta>
            <ta e="T50" id="Seg_1191" s="T49">home</ta>
            <ta e="T51" id="Seg_1192" s="T50">come-DUR-3PL</ta>
            <ta e="T52" id="Seg_1193" s="T51">one</ta>
            <ta e="T53" id="Seg_1194" s="T52">old.man.[NOM]</ta>
            <ta e="T54" id="Seg_1195" s="T53">forest-ILL</ta>
            <ta e="T55" id="Seg_1196" s="T54">wild.animal-VBLZ-CVB</ta>
            <ta e="T56" id="Seg_1197" s="T55">go.away-PST.NAR.[3SG.S]</ta>
            <ta e="T57" id="Seg_1198" s="T56">forest-LOC</ta>
            <ta e="T58" id="Seg_1199" s="T57">wild.animal.[NOM]</ta>
            <ta e="T59" id="Seg_1200" s="T58">much-ADVZ</ta>
            <ta e="T60" id="Seg_1201" s="T59">kill-PST.NAR-3SG.O</ta>
            <ta e="T61" id="Seg_1202" s="T60">one</ta>
            <ta e="T62" id="Seg_1203" s="T61">old.man.[NOM]</ta>
            <ta e="T63" id="Seg_1204" s="T62">live-PST.NAR.[3SG.S]</ta>
            <ta e="T64" id="Seg_1205" s="T63">forest-LOC</ta>
            <ta e="T65" id="Seg_1206" s="T64">winter-ADVZ</ta>
            <ta e="T66" id="Seg_1207" s="T65">wild.animal-VBLZ-PST.NAR.[3SG.S]</ta>
            <ta e="T67" id="Seg_1208" s="T66">but</ta>
            <ta e="T68" id="Seg_1209" s="T67">summer-ADV.LOC</ta>
            <ta e="T69" id="Seg_1210" s="T68">make-DUR-PFV-PST.NAR-3SG.O</ta>
            <ta e="T70" id="Seg_1211" s="T69">bolt-ACC</ta>
            <ta e="T71" id="Seg_1212" s="T70">summer-ADV.LOC</ta>
            <ta e="T72" id="Seg_1213" s="T71">kill-DUR-3SG.O</ta>
            <ta e="T73" id="Seg_1214" s="T72">much-ADVZ</ta>
            <ta e="T74" id="Seg_1215" s="T73">fish-ACC</ta>
            <ta e="T75" id="Seg_1216" s="T74">one</ta>
            <ta e="T76" id="Seg_1217" s="T75">child-DIM.[NOM]</ta>
            <ta e="T77" id="Seg_1218" s="T76">water-ILL</ta>
            <ta e="T78" id="Seg_1219" s="T77">dive-MULO-PST.NAR.[3SG.S]</ta>
            <ta e="T79" id="Seg_1220" s="T78">then</ta>
            <ta e="T80" id="Seg_1221" s="T79">human.being-EP-PL.[NOM]</ta>
            <ta e="T81" id="Seg_1222" s="T80">upwards</ta>
            <ta e="T82" id="Seg_1223" s="T81">take-PST.NAR-3PL</ta>
            <ta e="T83" id="Seg_1224" s="T82">space.outside-LOC</ta>
            <ta e="T84" id="Seg_1225" s="T83">human.being-EP-PL.[NOM]</ta>
            <ta e="T85" id="Seg_1226" s="T84">much</ta>
            <ta e="T86" id="Seg_1227" s="T85">be-CO-3PL</ta>
            <ta e="T87" id="Seg_1228" s="T86">night-ADV.LOC</ta>
            <ta e="T88" id="Seg_1229" s="T87">sleep-3PL</ta>
            <ta e="T89" id="Seg_1230" s="T88">all</ta>
            <ta e="T90" id="Seg_1231" s="T89">morning-ADV.LOC</ta>
            <ta e="T91" id="Seg_1232" s="T90">human.being-EP-PL.[NOM]</ta>
            <ta e="T92" id="Seg_1233" s="T91">work-CVB</ta>
            <ta e="T93" id="Seg_1234" s="T92">go.away-DUR-3PL</ta>
            <ta e="T94" id="Seg_1235" s="T93">and</ta>
            <ta e="T95" id="Seg_1236" s="T94">evening-ADV.LOC</ta>
            <ta e="T96" id="Seg_1237" s="T95">work-CVB</ta>
            <ta e="T97" id="Seg_1238" s="T96">finish-MULO-DUR-3PL</ta>
            <ta e="T98" id="Seg_1239" s="T97">and</ta>
            <ta e="T99" id="Seg_1240" s="T98">day-ADV.LOC</ta>
            <ta e="T100" id="Seg_1241" s="T99">eat-EP-FRQ-INF</ta>
            <ta e="T101" id="Seg_1242" s="T100">leave-EP-US-DUR-3PL</ta>
            <ta e="T102" id="Seg_1243" s="T101">we.PL.NOM</ta>
            <ta e="T103" id="Seg_1244" s="T102">evening-ADV.LOC</ta>
            <ta e="T104" id="Seg_1245" s="T103">leave-EP-PST-1PL</ta>
            <ta e="T105" id="Seg_1246" s="T104">fish-VBLZ-CVB</ta>
            <ta e="T106" id="Seg_1247" s="T105">fish-ACC</ta>
            <ta e="T107" id="Seg_1248" s="T106">catch-PFV-1PL</ta>
            <ta e="T108" id="Seg_1249" s="T107">NEG</ta>
            <ta e="T109" id="Seg_1250" s="T108">summer-ADV.LOC</ta>
            <ta e="T110" id="Seg_1251" s="T109">mosquito.[NOM]</ta>
            <ta e="T111" id="Seg_1252" s="T110">much</ta>
            <ta e="T112" id="Seg_1253" s="T111">mosquito.[NOM]</ta>
            <ta e="T113" id="Seg_1254" s="T112">human.being-EP-ACC</ta>
            <ta e="T114" id="Seg_1255" s="T113">bite-FRQ-CO-3SG.O</ta>
            <ta e="T115" id="Seg_1256" s="T114">force-INSTR</ta>
            <ta e="T116" id="Seg_1257" s="T115">earlier</ta>
            <ta e="T117" id="Seg_1258" s="T116">I.ACC</ta>
            <ta e="T118" id="Seg_1259" s="T117">also</ta>
            <ta e="T119" id="Seg_1260" s="T118">bite-FRQ-PST.[3SG.S]</ta>
            <ta e="T120" id="Seg_1261" s="T119">(s)he.[NOM]</ta>
            <ta e="T121" id="Seg_1262" s="T120">be-PST.[3SG.S]</ta>
            <ta e="T122" id="Seg_1263" s="T121">NEG</ta>
            <ta e="T123" id="Seg_1264" s="T122">what.[NOM]</ta>
            <ta e="T124" id="Seg_1265" s="T123">out-ADV.EL</ta>
            <ta e="T125" id="Seg_1266" s="T124">NEG</ta>
            <ta e="T126" id="Seg_1267" s="T125">be.afraid-PST.[3SG.S]</ta>
            <ta e="T127" id="Seg_1268" s="T126">table-LOC</ta>
            <ta e="T128" id="Seg_1269" s="T127">lie.[3SG.S]</ta>
            <ta e="T129" id="Seg_1270" s="T128">much-ADVZ</ta>
            <ta e="T130" id="Seg_1271" s="T129">letter.[NOM]</ta>
            <ta e="T131" id="Seg_1272" s="T130">write-FRQ-PST.NAR-3SG.O</ta>
            <ta e="T132" id="Seg_1273" s="T131">small-DIM</ta>
            <ta e="T133" id="Seg_1274" s="T132">child-DIM.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1275" s="T0">собака.[NOM]</ta>
            <ta e="T2" id="Seg_1276" s="T1">пространство.снаружи-LOC</ta>
            <ta e="T3" id="Seg_1277" s="T2">спать.[3SG.S]</ta>
            <ta e="T4" id="Seg_1278" s="T3">я.NOM</ta>
            <ta e="T5" id="Seg_1279" s="T4">в.лес</ta>
            <ta e="T6" id="Seg_1280" s="T5">уйти-EP-PST-1SG.S</ta>
            <ta e="T7" id="Seg_1281" s="T6">зверь-VBLZ-CVB</ta>
            <ta e="T8" id="Seg_1282" s="T7">лес-LOC</ta>
            <ta e="T9" id="Seg_1283" s="T8">сила-INSTR</ta>
            <ta e="T10" id="Seg_1284" s="T9">хороший</ta>
            <ta e="T11" id="Seg_1285" s="T10">быть-CO.[3SG.S]</ta>
            <ta e="T12" id="Seg_1286" s="T11">я.NOM</ta>
            <ta e="T13" id="Seg_1287" s="T12">весна-ADVZ</ta>
            <ta e="T14" id="Seg_1288" s="T13">лес-LOC</ta>
            <ta e="T15" id="Seg_1289" s="T14">зверь-VBLZ-PST-1SG.S</ta>
            <ta e="T16" id="Seg_1290" s="T15">как</ta>
            <ta e="T17" id="Seg_1291" s="T16">лёд-ACC</ta>
            <ta e="T18" id="Seg_1292" s="T17">дернуть-PST-3SG.O</ta>
            <ta e="T19" id="Seg_1293" s="T18">я.NOM</ta>
            <ta e="T20" id="Seg_1294" s="T19">домой</ta>
            <ta e="T21" id="Seg_1295" s="T20">прийти-PST-1SG.S</ta>
            <ta e="T22" id="Seg_1296" s="T21">я.NOM</ta>
            <ta e="T23" id="Seg_1297" s="T22">весна-ADVZ</ta>
            <ta e="T24" id="Seg_1298" s="T23">отправиться.на.охоту-CVB</ta>
            <ta e="T25" id="Seg_1299" s="T24">отправиться-EP-PST-1SG.S</ta>
            <ta e="T26" id="Seg_1300" s="T25">лес-LOC</ta>
            <ta e="T27" id="Seg_1301" s="T26">зверь.[NOM]</ta>
            <ta e="T28" id="Seg_1302" s="T27">много</ta>
            <ta e="T29" id="Seg_1303" s="T28">interrog.pron.stem-ADVZ</ta>
            <ta e="T30" id="Seg_1304" s="T29">лёд.[NOM]</ta>
            <ta e="T31" id="Seg_1305" s="T30">дернуть-PST-3SG.O</ta>
            <ta e="T32" id="Seg_1306" s="T31">мы.PL.NOM</ta>
            <ta e="T33" id="Seg_1307" s="T32">отправиться-EP-PST-1PL</ta>
            <ta e="T34" id="Seg_1308" s="T33">рыба-VBLZ-CVB</ta>
            <ta e="T35" id="Seg_1309" s="T34">карась-ADJZ</ta>
            <ta e="T36" id="Seg_1310" s="T35">озеро-ILL</ta>
            <ta e="T37" id="Seg_1311" s="T36">карась-ACC</ta>
            <ta e="T38" id="Seg_1312" s="T37">сила-INSTR</ta>
            <ta e="T39" id="Seg_1313" s="T38">много-ADVZ</ta>
            <ta e="T40" id="Seg_1314" s="T39">убить-PST-1PL</ta>
            <ta e="T41" id="Seg_1315" s="T40">потом</ta>
            <ta e="T42" id="Seg_1316" s="T41">домой</ta>
            <ta e="T43" id="Seg_1317" s="T42">прийти-PST-1PL</ta>
            <ta e="T44" id="Seg_1318" s="T43">лето-ADV.LOC</ta>
            <ta e="T45" id="Seg_1319" s="T44">человек-EP-PL.[NOM]</ta>
            <ta e="T46" id="Seg_1320" s="T45">уйти-DUR-3PL</ta>
            <ta e="T47" id="Seg_1321" s="T46">сено-ADJZ</ta>
            <ta e="T48" id="Seg_1322" s="T47">рубить-HAB-CVB</ta>
            <ta e="T49" id="Seg_1323" s="T48">осень-ADV.LOC</ta>
            <ta e="T50" id="Seg_1324" s="T49">домой</ta>
            <ta e="T51" id="Seg_1325" s="T50">прийти-DUR-3PL</ta>
            <ta e="T52" id="Seg_1326" s="T51">один</ta>
            <ta e="T53" id="Seg_1327" s="T52">старик.[NOM]</ta>
            <ta e="T54" id="Seg_1328" s="T53">лес-ILL</ta>
            <ta e="T55" id="Seg_1329" s="T54">зверь-VBLZ-CVB</ta>
            <ta e="T56" id="Seg_1330" s="T55">уйти-PST.NAR.[3SG.S]</ta>
            <ta e="T57" id="Seg_1331" s="T56">лес-LOC</ta>
            <ta e="T58" id="Seg_1332" s="T57">зверь.[NOM]</ta>
            <ta e="T59" id="Seg_1333" s="T58">много-ADVZ</ta>
            <ta e="T60" id="Seg_1334" s="T59">убить-PST.NAR-3SG.O</ta>
            <ta e="T61" id="Seg_1335" s="T60">один</ta>
            <ta e="T62" id="Seg_1336" s="T61">старик.[NOM]</ta>
            <ta e="T63" id="Seg_1337" s="T62">жить-PST.NAR.[3SG.S]</ta>
            <ta e="T64" id="Seg_1338" s="T63">лес-LOC</ta>
            <ta e="T65" id="Seg_1339" s="T64">зима-ADVZ</ta>
            <ta e="T66" id="Seg_1340" s="T65">зверь-VBLZ-PST.NAR.[3SG.S]</ta>
            <ta e="T67" id="Seg_1341" s="T66">а</ta>
            <ta e="T68" id="Seg_1342" s="T67">лето-ADV.LOC</ta>
            <ta e="T69" id="Seg_1343" s="T68">сделать-DUR-PFV-PST.NAR-3SG.O</ta>
            <ta e="T70" id="Seg_1344" s="T69">запор-ACC</ta>
            <ta e="T71" id="Seg_1345" s="T70">лето-ADV.LOC</ta>
            <ta e="T72" id="Seg_1346" s="T71">убить-DUR-3SG.O</ta>
            <ta e="T73" id="Seg_1347" s="T72">много-ADVZ</ta>
            <ta e="T74" id="Seg_1348" s="T73">рыба-ACC</ta>
            <ta e="T75" id="Seg_1349" s="T74">один</ta>
            <ta e="T76" id="Seg_1350" s="T75">ребенок-DIM.[NOM]</ta>
            <ta e="T77" id="Seg_1351" s="T76">вода-ILL</ta>
            <ta e="T78" id="Seg_1352" s="T77">нырять-MULO-PST.NAR.[3SG.S]</ta>
            <ta e="T79" id="Seg_1353" s="T78">потом</ta>
            <ta e="T80" id="Seg_1354" s="T79">человек-EP-PL.[NOM]</ta>
            <ta e="T81" id="Seg_1355" s="T80">вверх</ta>
            <ta e="T82" id="Seg_1356" s="T81">взять-PST.NAR-3PL</ta>
            <ta e="T83" id="Seg_1357" s="T82">пространство.снаружи-LOC</ta>
            <ta e="T84" id="Seg_1358" s="T83">человек-EP-PL.[NOM]</ta>
            <ta e="T85" id="Seg_1359" s="T84">много</ta>
            <ta e="T86" id="Seg_1360" s="T85">быть-CO-3PL</ta>
            <ta e="T87" id="Seg_1361" s="T86">ночь-ADV.LOC</ta>
            <ta e="T88" id="Seg_1362" s="T87">спать-3PL</ta>
            <ta e="T89" id="Seg_1363" s="T88">всё</ta>
            <ta e="T90" id="Seg_1364" s="T89">утро-ADV.LOC</ta>
            <ta e="T91" id="Seg_1365" s="T90">человек-EP-PL.[NOM]</ta>
            <ta e="T92" id="Seg_1366" s="T91">работать-CVB</ta>
            <ta e="T93" id="Seg_1367" s="T92">уйти-DUR-3PL</ta>
            <ta e="T94" id="Seg_1368" s="T93">а</ta>
            <ta e="T95" id="Seg_1369" s="T94">вечер-ADV.LOC</ta>
            <ta e="T96" id="Seg_1370" s="T95">работать-CVB</ta>
            <ta e="T97" id="Seg_1371" s="T96">кончить-MULO-DUR-3PL</ta>
            <ta e="T98" id="Seg_1372" s="T97">а</ta>
            <ta e="T99" id="Seg_1373" s="T98">день-ADV.LOC</ta>
            <ta e="T100" id="Seg_1374" s="T99">съесть-EP-FRQ-INF</ta>
            <ta e="T101" id="Seg_1375" s="T100">отправиться-EP-US-DUR-3PL</ta>
            <ta e="T102" id="Seg_1376" s="T101">мы.PL.NOM</ta>
            <ta e="T103" id="Seg_1377" s="T102">вечер-ADV.LOC</ta>
            <ta e="T104" id="Seg_1378" s="T103">отправиться-EP-PST-1PL</ta>
            <ta e="T105" id="Seg_1379" s="T104">рыба-VBLZ-CVB</ta>
            <ta e="T106" id="Seg_1380" s="T105">рыба-ACC</ta>
            <ta e="T107" id="Seg_1381" s="T106">поймать-PFV-1PL</ta>
            <ta e="T108" id="Seg_1382" s="T107">NEG</ta>
            <ta e="T109" id="Seg_1383" s="T108">лето-ADV.LOC</ta>
            <ta e="T110" id="Seg_1384" s="T109">комар.[NOM]</ta>
            <ta e="T111" id="Seg_1385" s="T110">много</ta>
            <ta e="T112" id="Seg_1386" s="T111">комар.[NOM]</ta>
            <ta e="T113" id="Seg_1387" s="T112">человек-EP-ACC</ta>
            <ta e="T114" id="Seg_1388" s="T113">куснуть-FRQ-CO-3SG.O</ta>
            <ta e="T115" id="Seg_1389" s="T114">сила-INSTR</ta>
            <ta e="T116" id="Seg_1390" s="T115">раньше</ta>
            <ta e="T117" id="Seg_1391" s="T116">я.ACC</ta>
            <ta e="T118" id="Seg_1392" s="T117">тоже</ta>
            <ta e="T119" id="Seg_1393" s="T118">куснуть-FRQ-PST.[3SG.S]</ta>
            <ta e="T120" id="Seg_1394" s="T119">он(а).[NOM]</ta>
            <ta e="T121" id="Seg_1395" s="T120">быть-PST.[3SG.S]</ta>
            <ta e="T122" id="Seg_1396" s="T121">NEG</ta>
            <ta e="T123" id="Seg_1397" s="T122">что.[NOM]</ta>
            <ta e="T124" id="Seg_1398" s="T123">из-ADV.EL</ta>
            <ta e="T125" id="Seg_1399" s="T124">NEG</ta>
            <ta e="T126" id="Seg_1400" s="T125">бояться-PST.[3SG.S]</ta>
            <ta e="T127" id="Seg_1401" s="T126">стол-LOC</ta>
            <ta e="T128" id="Seg_1402" s="T127">лежать.[3SG.S]</ta>
            <ta e="T129" id="Seg_1403" s="T128">много-ADVZ</ta>
            <ta e="T130" id="Seg_1404" s="T129">письмо.[NOM]</ta>
            <ta e="T131" id="Seg_1405" s="T130">писать-FRQ-PST.NAR-3SG.O</ta>
            <ta e="T132" id="Seg_1406" s="T131">маленький-DIM</ta>
            <ta e="T133" id="Seg_1407" s="T132">ребенок-DIM.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1408" s="T0">n-n:case3</ta>
            <ta e="T2" id="Seg_1409" s="T1">n-n:case3</ta>
            <ta e="T3" id="Seg_1410" s="T2">v-v:pn</ta>
            <ta e="T4" id="Seg_1411" s="T3">pers</ta>
            <ta e="T5" id="Seg_1412" s="T4">adv</ta>
            <ta e="T6" id="Seg_1413" s="T5">v-v:ins-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_1414" s="T6">n-n&gt;v-v&gt;adv</ta>
            <ta e="T8" id="Seg_1415" s="T7">n-n:case3</ta>
            <ta e="T9" id="Seg_1416" s="T8">n-n:case3</ta>
            <ta e="T10" id="Seg_1417" s="T9">adj</ta>
            <ta e="T11" id="Seg_1418" s="T10">v-v:ins-v:pn</ta>
            <ta e="T12" id="Seg_1419" s="T11">pers</ta>
            <ta e="T13" id="Seg_1420" s="T12">n-n&gt;adv</ta>
            <ta e="T14" id="Seg_1421" s="T13">n-n:case3</ta>
            <ta e="T15" id="Seg_1422" s="T14">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_1423" s="T15">conj</ta>
            <ta e="T17" id="Seg_1424" s="T16">n-n:case3</ta>
            <ta e="T18" id="Seg_1425" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_1426" s="T18">pers</ta>
            <ta e="T20" id="Seg_1427" s="T19">adv</ta>
            <ta e="T21" id="Seg_1428" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_1429" s="T21">pers</ta>
            <ta e="T23" id="Seg_1430" s="T22">n-n&gt;adv</ta>
            <ta e="T24" id="Seg_1431" s="T23">v-v&gt;adv</ta>
            <ta e="T25" id="Seg_1432" s="T24">v-v:ins-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_1433" s="T25">n-n:case3</ta>
            <ta e="T27" id="Seg_1434" s="T26">n-n:case3</ta>
            <ta e="T28" id="Seg_1435" s="T27">quant</ta>
            <ta e="T29" id="Seg_1436" s="T28">interrog-adj&gt;adv</ta>
            <ta e="T30" id="Seg_1437" s="T29">n-n:case3</ta>
            <ta e="T31" id="Seg_1438" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_1439" s="T31">pers</ta>
            <ta e="T33" id="Seg_1440" s="T32">v-n:(ins)-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_1441" s="T33">n-n&gt;v-v&gt;adv</ta>
            <ta e="T35" id="Seg_1442" s="T34">n-n&gt;adj</ta>
            <ta e="T36" id="Seg_1443" s="T35">n-n:case3</ta>
            <ta e="T37" id="Seg_1444" s="T36">n-n:case3</ta>
            <ta e="T38" id="Seg_1445" s="T37">n-n:case3</ta>
            <ta e="T39" id="Seg_1446" s="T38">quant-adj&gt;adv</ta>
            <ta e="T40" id="Seg_1447" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_1448" s="T40">adv</ta>
            <ta e="T42" id="Seg_1449" s="T41">adv</ta>
            <ta e="T43" id="Seg_1450" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_1451" s="T43">n-n&gt;adv</ta>
            <ta e="T45" id="Seg_1452" s="T44">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T46" id="Seg_1453" s="T45">v-v&gt;v-v:pn</ta>
            <ta e="T47" id="Seg_1454" s="T46">n-n&gt;adj</ta>
            <ta e="T48" id="Seg_1455" s="T47">v-v&gt;v-v&gt;adv</ta>
            <ta e="T49" id="Seg_1456" s="T48">n-n&gt;adv</ta>
            <ta e="T50" id="Seg_1457" s="T49">adv</ta>
            <ta e="T51" id="Seg_1458" s="T50">v-v&gt;v-v:pn</ta>
            <ta e="T52" id="Seg_1459" s="T51">num</ta>
            <ta e="T53" id="Seg_1460" s="T52">n-n:case3</ta>
            <ta e="T54" id="Seg_1461" s="T53">n-n:case3</ta>
            <ta e="T55" id="Seg_1462" s="T54">n-n&gt;v-v&gt;adv</ta>
            <ta e="T56" id="Seg_1463" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_1464" s="T56">n-n:case3</ta>
            <ta e="T58" id="Seg_1465" s="T57">n-n:case3</ta>
            <ta e="T59" id="Seg_1466" s="T58">quant-adj&gt;adv</ta>
            <ta e="T60" id="Seg_1467" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_1468" s="T60">num</ta>
            <ta e="T62" id="Seg_1469" s="T61">n-n:case3</ta>
            <ta e="T63" id="Seg_1470" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_1471" s="T63">n-n:case3</ta>
            <ta e="T65" id="Seg_1472" s="T64">n-n&gt;adv</ta>
            <ta e="T66" id="Seg_1473" s="T65">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_1474" s="T66">conj</ta>
            <ta e="T68" id="Seg_1475" s="T67">n-n&gt;adv</ta>
            <ta e="T69" id="Seg_1476" s="T68">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_1477" s="T69">n-n:case3</ta>
            <ta e="T71" id="Seg_1478" s="T70">n-n&gt;adv</ta>
            <ta e="T72" id="Seg_1479" s="T71">v-v&gt;v-v:pn</ta>
            <ta e="T73" id="Seg_1480" s="T72">quant-adj&gt;adv</ta>
            <ta e="T74" id="Seg_1481" s="T73">n-n:case3</ta>
            <ta e="T75" id="Seg_1482" s="T74">num</ta>
            <ta e="T76" id="Seg_1483" s="T75">n-n&gt;n-n:case3</ta>
            <ta e="T77" id="Seg_1484" s="T76">n-n:case3</ta>
            <ta e="T78" id="Seg_1485" s="T77">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_1486" s="T78">adv</ta>
            <ta e="T80" id="Seg_1487" s="T79">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T81" id="Seg_1488" s="T80">adv</ta>
            <ta e="T82" id="Seg_1489" s="T81">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_1490" s="T82">n-n:case3</ta>
            <ta e="T84" id="Seg_1491" s="T83">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T85" id="Seg_1492" s="T84">quant</ta>
            <ta e="T86" id="Seg_1493" s="T85">v-v:ins-v:pn</ta>
            <ta e="T87" id="Seg_1494" s="T86">n-n&gt;adv</ta>
            <ta e="T88" id="Seg_1495" s="T87">v-v:pn</ta>
            <ta e="T89" id="Seg_1496" s="T88">quant</ta>
            <ta e="T90" id="Seg_1497" s="T89">n-n&gt;adv</ta>
            <ta e="T91" id="Seg_1498" s="T90">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T92" id="Seg_1499" s="T91">v-v&gt;adv</ta>
            <ta e="T93" id="Seg_1500" s="T92">v-v&gt;v-v:pn</ta>
            <ta e="T94" id="Seg_1501" s="T93">conj</ta>
            <ta e="T95" id="Seg_1502" s="T94">n-n&gt;adv</ta>
            <ta e="T96" id="Seg_1503" s="T95">v-v&gt;adv</ta>
            <ta e="T97" id="Seg_1504" s="T96">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T98" id="Seg_1505" s="T97">conj</ta>
            <ta e="T99" id="Seg_1506" s="T98">n-n&gt;adv</ta>
            <ta e="T100" id="Seg_1507" s="T99">v-v:ins-v&gt;v-v:inf</ta>
            <ta e="T101" id="Seg_1508" s="T100">v-v:ins-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T102" id="Seg_1509" s="T101">pers</ta>
            <ta e="T103" id="Seg_1510" s="T102">n-n&gt;adv</ta>
            <ta e="T104" id="Seg_1511" s="T103">v-v:ins-v:tense-v:pn</ta>
            <ta e="T105" id="Seg_1512" s="T104">n-n&gt;v-v&gt;adv</ta>
            <ta e="T106" id="Seg_1513" s="T105">n-n:case3</ta>
            <ta e="T107" id="Seg_1514" s="T106">v-v&gt;v-v:pn</ta>
            <ta e="T108" id="Seg_1515" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_1516" s="T108">n-n&gt;adv</ta>
            <ta e="T110" id="Seg_1517" s="T109">n-n:case3</ta>
            <ta e="T111" id="Seg_1518" s="T110">quant</ta>
            <ta e="T112" id="Seg_1519" s="T111">n-n:case3</ta>
            <ta e="T113" id="Seg_1520" s="T112">n-n:(ins)-n:case3</ta>
            <ta e="T114" id="Seg_1521" s="T113">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T115" id="Seg_1522" s="T114">n-n:case3</ta>
            <ta e="T116" id="Seg_1523" s="T115">adv</ta>
            <ta e="T117" id="Seg_1524" s="T116">pers</ta>
            <ta e="T118" id="Seg_1525" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_1526" s="T118">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T120" id="Seg_1527" s="T119">pers-n:case3</ta>
            <ta e="T121" id="Seg_1528" s="T120">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_1529" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_1530" s="T122">interrog-n:case3</ta>
            <ta e="T124" id="Seg_1531" s="T123">pp-adv:advcase</ta>
            <ta e="T125" id="Seg_1532" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_1533" s="T125">v-v:tense-v:pn</ta>
            <ta e="T127" id="Seg_1534" s="T126">n-n:case3</ta>
            <ta e="T128" id="Seg_1535" s="T127">v-v:pn</ta>
            <ta e="T129" id="Seg_1536" s="T128">quant-adj&gt;adv</ta>
            <ta e="T130" id="Seg_1537" s="T129">n-n:case3</ta>
            <ta e="T131" id="Seg_1538" s="T130">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_1539" s="T131">adj-n&gt;n</ta>
            <ta e="T133" id="Seg_1540" s="T132">n-n&gt;n-n:case3</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1541" s="T0">n</ta>
            <ta e="T2" id="Seg_1542" s="T1">n</ta>
            <ta e="T3" id="Seg_1543" s="T2">v</ta>
            <ta e="T4" id="Seg_1544" s="T3">pers</ta>
            <ta e="T5" id="Seg_1545" s="T4">adv</ta>
            <ta e="T6" id="Seg_1546" s="T5">v</ta>
            <ta e="T7" id="Seg_1547" s="T6">adv</ta>
            <ta e="T8" id="Seg_1548" s="T7">n</ta>
            <ta e="T9" id="Seg_1549" s="T8">n</ta>
            <ta e="T10" id="Seg_1550" s="T9">adj</ta>
            <ta e="T11" id="Seg_1551" s="T10">n</ta>
            <ta e="T12" id="Seg_1552" s="T11">pers</ta>
            <ta e="T13" id="Seg_1553" s="T12">adv</ta>
            <ta e="T14" id="Seg_1554" s="T13">n</ta>
            <ta e="T15" id="Seg_1555" s="T14">v</ta>
            <ta e="T16" id="Seg_1556" s="T15">conj</ta>
            <ta e="T17" id="Seg_1557" s="T16">n</ta>
            <ta e="T18" id="Seg_1558" s="T17">v</ta>
            <ta e="T19" id="Seg_1559" s="T18">pers</ta>
            <ta e="T20" id="Seg_1560" s="T19">adv</ta>
            <ta e="T21" id="Seg_1561" s="T20">v</ta>
            <ta e="T22" id="Seg_1562" s="T21">pers</ta>
            <ta e="T23" id="Seg_1563" s="T22">adv</ta>
            <ta e="T24" id="Seg_1564" s="T23">adv</ta>
            <ta e="T25" id="Seg_1565" s="T24">v</ta>
            <ta e="T26" id="Seg_1566" s="T25">n</ta>
            <ta e="T27" id="Seg_1567" s="T26">n</ta>
            <ta e="T28" id="Seg_1568" s="T27">quant</ta>
            <ta e="T29" id="Seg_1569" s="T28">interrog</ta>
            <ta e="T30" id="Seg_1570" s="T29">n</ta>
            <ta e="T31" id="Seg_1571" s="T30">v</ta>
            <ta e="T32" id="Seg_1572" s="T31">pers</ta>
            <ta e="T33" id="Seg_1573" s="T32">v</ta>
            <ta e="T34" id="Seg_1574" s="T33">adv</ta>
            <ta e="T35" id="Seg_1575" s="T34">adj</ta>
            <ta e="T36" id="Seg_1576" s="T35">n</ta>
            <ta e="T37" id="Seg_1577" s="T36">n</ta>
            <ta e="T38" id="Seg_1578" s="T37">n</ta>
            <ta e="T39" id="Seg_1579" s="T38">adv</ta>
            <ta e="T40" id="Seg_1580" s="T39">v</ta>
            <ta e="T41" id="Seg_1581" s="T40">adv</ta>
            <ta e="T42" id="Seg_1582" s="T41">adv</ta>
            <ta e="T43" id="Seg_1583" s="T42">v</ta>
            <ta e="T44" id="Seg_1584" s="T43">adv</ta>
            <ta e="T45" id="Seg_1585" s="T44">n</ta>
            <ta e="T46" id="Seg_1586" s="T45">v</ta>
            <ta e="T47" id="Seg_1587" s="T46">adj</ta>
            <ta e="T48" id="Seg_1588" s="T47">adv</ta>
            <ta e="T49" id="Seg_1589" s="T48">adv</ta>
            <ta e="T50" id="Seg_1590" s="T49">adv</ta>
            <ta e="T51" id="Seg_1591" s="T50">v</ta>
            <ta e="T52" id="Seg_1592" s="T51">num</ta>
            <ta e="T53" id="Seg_1593" s="T52">n</ta>
            <ta e="T54" id="Seg_1594" s="T53">n</ta>
            <ta e="T55" id="Seg_1595" s="T54">adv</ta>
            <ta e="T56" id="Seg_1596" s="T55">v</ta>
            <ta e="T57" id="Seg_1597" s="T56">n</ta>
            <ta e="T58" id="Seg_1598" s="T57">n</ta>
            <ta e="T59" id="Seg_1599" s="T58">adv</ta>
            <ta e="T60" id="Seg_1600" s="T59">v</ta>
            <ta e="T61" id="Seg_1601" s="T60">num</ta>
            <ta e="T62" id="Seg_1602" s="T61">n</ta>
            <ta e="T63" id="Seg_1603" s="T62">v</ta>
            <ta e="T64" id="Seg_1604" s="T63">n</ta>
            <ta e="T65" id="Seg_1605" s="T64">adv</ta>
            <ta e="T66" id="Seg_1606" s="T65">v</ta>
            <ta e="T67" id="Seg_1607" s="T66">conj</ta>
            <ta e="T68" id="Seg_1608" s="T67">adv</ta>
            <ta e="T69" id="Seg_1609" s="T68">v</ta>
            <ta e="T70" id="Seg_1610" s="T69">n</ta>
            <ta e="T71" id="Seg_1611" s="T70">adv</ta>
            <ta e="T72" id="Seg_1612" s="T71">v</ta>
            <ta e="T73" id="Seg_1613" s="T72">adv</ta>
            <ta e="T74" id="Seg_1614" s="T73">n</ta>
            <ta e="T75" id="Seg_1615" s="T74">num</ta>
            <ta e="T76" id="Seg_1616" s="T75">n</ta>
            <ta e="T77" id="Seg_1617" s="T76">n</ta>
            <ta e="T78" id="Seg_1618" s="T77">v</ta>
            <ta e="T79" id="Seg_1619" s="T78">adv</ta>
            <ta e="T80" id="Seg_1620" s="T79">n</ta>
            <ta e="T81" id="Seg_1621" s="T80">adv</ta>
            <ta e="T82" id="Seg_1622" s="T81">v</ta>
            <ta e="T83" id="Seg_1623" s="T82">n</ta>
            <ta e="T84" id="Seg_1624" s="T83">n</ta>
            <ta e="T85" id="Seg_1625" s="T84">quant</ta>
            <ta e="T86" id="Seg_1626" s="T85">v</ta>
            <ta e="T87" id="Seg_1627" s="T86">adv</ta>
            <ta e="T88" id="Seg_1628" s="T87">v</ta>
            <ta e="T89" id="Seg_1629" s="T88">quant</ta>
            <ta e="T90" id="Seg_1630" s="T89">adv</ta>
            <ta e="T91" id="Seg_1631" s="T90">n</ta>
            <ta e="T92" id="Seg_1632" s="T91">adv</ta>
            <ta e="T93" id="Seg_1633" s="T92">v</ta>
            <ta e="T94" id="Seg_1634" s="T93">conj</ta>
            <ta e="T95" id="Seg_1635" s="T94">adv</ta>
            <ta e="T96" id="Seg_1636" s="T95">adv</ta>
            <ta e="T97" id="Seg_1637" s="T96">v</ta>
            <ta e="T98" id="Seg_1638" s="T97">conj</ta>
            <ta e="T99" id="Seg_1639" s="T98">adv</ta>
            <ta e="T100" id="Seg_1640" s="T99">v</ta>
            <ta e="T101" id="Seg_1641" s="T100">v</ta>
            <ta e="T102" id="Seg_1642" s="T101">pers</ta>
            <ta e="T103" id="Seg_1643" s="T102">adv</ta>
            <ta e="T104" id="Seg_1644" s="T103">v</ta>
            <ta e="T105" id="Seg_1645" s="T104">adv</ta>
            <ta e="T106" id="Seg_1646" s="T105">n</ta>
            <ta e="T107" id="Seg_1647" s="T106">v</ta>
            <ta e="T108" id="Seg_1648" s="T107">v</ta>
            <ta e="T109" id="Seg_1649" s="T108">adv</ta>
            <ta e="T110" id="Seg_1650" s="T109">n</ta>
            <ta e="T111" id="Seg_1651" s="T110">quant</ta>
            <ta e="T112" id="Seg_1652" s="T111">n</ta>
            <ta e="T113" id="Seg_1653" s="T112">n</ta>
            <ta e="T114" id="Seg_1654" s="T113">v</ta>
            <ta e="T115" id="Seg_1655" s="T114">n</ta>
            <ta e="T116" id="Seg_1656" s="T115">adv</ta>
            <ta e="T117" id="Seg_1657" s="T116">pers</ta>
            <ta e="T118" id="Seg_1658" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_1659" s="T118">v</ta>
            <ta e="T120" id="Seg_1660" s="T119">pers</ta>
            <ta e="T121" id="Seg_1661" s="T120">v</ta>
            <ta e="T122" id="Seg_1662" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_1663" s="T122">interrog</ta>
            <ta e="T124" id="Seg_1664" s="T123">pp</ta>
            <ta e="T125" id="Seg_1665" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_1666" s="T125">v</ta>
            <ta e="T127" id="Seg_1667" s="T126">n</ta>
            <ta e="T128" id="Seg_1668" s="T127">v</ta>
            <ta e="T129" id="Seg_1669" s="T128">adv</ta>
            <ta e="T130" id="Seg_1670" s="T129">n</ta>
            <ta e="T131" id="Seg_1671" s="T130">v</ta>
            <ta e="T132" id="Seg_1672" s="T131">adj</ta>
            <ta e="T133" id="Seg_1673" s="T132">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1674" s="T0">np:Th</ta>
            <ta e="T2" id="Seg_1675" s="T1">np:L</ta>
            <ta e="T4" id="Seg_1676" s="T3">pro.h:A</ta>
            <ta e="T5" id="Seg_1677" s="T4">np:G</ta>
            <ta e="T8" id="Seg_1678" s="T7">np:L</ta>
            <ta e="T12" id="Seg_1679" s="T11">pro.h:A</ta>
            <ta e="T13" id="Seg_1680" s="T12">np:Time</ta>
            <ta e="T14" id="Seg_1681" s="T13">np:L</ta>
            <ta e="T19" id="Seg_1682" s="T18">pro.h:A</ta>
            <ta e="T20" id="Seg_1683" s="T19">adv:G</ta>
            <ta e="T22" id="Seg_1684" s="T21">pro.h:A</ta>
            <ta e="T23" id="Seg_1685" s="T22">np:Time</ta>
            <ta e="T26" id="Seg_1686" s="T25">np:L</ta>
            <ta e="T27" id="Seg_1687" s="T26">np:Th</ta>
            <ta e="T32" id="Seg_1688" s="T31">pro.h:A</ta>
            <ta e="T36" id="Seg_1689" s="T35">np:G</ta>
            <ta e="T37" id="Seg_1690" s="T36">np:P</ta>
            <ta e="T40" id="Seg_1691" s="T39">0.1.h:A</ta>
            <ta e="T41" id="Seg_1692" s="T40">adv:Time</ta>
            <ta e="T42" id="Seg_1693" s="T41">adv:G</ta>
            <ta e="T43" id="Seg_1694" s="T42">0.1.h:A</ta>
            <ta e="T44" id="Seg_1695" s="T43">np:Time</ta>
            <ta e="T45" id="Seg_1696" s="T44">np.h:A</ta>
            <ta e="T49" id="Seg_1697" s="T48">np:Time</ta>
            <ta e="T50" id="Seg_1698" s="T49">adv:G</ta>
            <ta e="T51" id="Seg_1699" s="T50">0.3.h:A</ta>
            <ta e="T53" id="Seg_1700" s="T52">np.h:A</ta>
            <ta e="T54" id="Seg_1701" s="T53">np:G</ta>
            <ta e="T57" id="Seg_1702" s="T56">np:L</ta>
            <ta e="T58" id="Seg_1703" s="T57">np:P</ta>
            <ta e="T60" id="Seg_1704" s="T59">0.3.h:A</ta>
            <ta e="T62" id="Seg_1705" s="T61">np.h:Th</ta>
            <ta e="T64" id="Seg_1706" s="T63">np:L</ta>
            <ta e="T65" id="Seg_1707" s="T64">np:Time</ta>
            <ta e="T66" id="Seg_1708" s="T65">0.3.h:A</ta>
            <ta e="T68" id="Seg_1709" s="T67">np:Time</ta>
            <ta e="T69" id="Seg_1710" s="T68">0.3.h:A</ta>
            <ta e="T70" id="Seg_1711" s="T69">np:P</ta>
            <ta e="T71" id="Seg_1712" s="T70">np:Time</ta>
            <ta e="T72" id="Seg_1713" s="T71">0.3.h:A</ta>
            <ta e="T74" id="Seg_1714" s="T73">np:P</ta>
            <ta e="T76" id="Seg_1715" s="T75">np.h:A</ta>
            <ta e="T77" id="Seg_1716" s="T76">np:G</ta>
            <ta e="T79" id="Seg_1717" s="T78">adv:Time</ta>
            <ta e="T80" id="Seg_1718" s="T79">np.h:A</ta>
            <ta e="T82" id="Seg_1719" s="T81">0.3.h:Th</ta>
            <ta e="T83" id="Seg_1720" s="T82">np:L</ta>
            <ta e="T84" id="Seg_1721" s="T83">np.h:Th</ta>
            <ta e="T87" id="Seg_1722" s="T86">np:Time</ta>
            <ta e="T88" id="Seg_1723" s="T87">0.3.h:Th</ta>
            <ta e="T90" id="Seg_1724" s="T89">np:Time</ta>
            <ta e="T91" id="Seg_1725" s="T90">np.h:A</ta>
            <ta e="T95" id="Seg_1726" s="T94">np:Time</ta>
            <ta e="T97" id="Seg_1727" s="T96">0.3.h:A</ta>
            <ta e="T99" id="Seg_1728" s="T98">np:Time</ta>
            <ta e="T101" id="Seg_1729" s="T100">0.3.h:A</ta>
            <ta e="T102" id="Seg_1730" s="T101">pro.h:A</ta>
            <ta e="T103" id="Seg_1731" s="T102">np:Time</ta>
            <ta e="T106" id="Seg_1732" s="T105">np:P</ta>
            <ta e="T107" id="Seg_1733" s="T106">0.1.h:A</ta>
            <ta e="T109" id="Seg_1734" s="T108">np:Time</ta>
            <ta e="T110" id="Seg_1735" s="T109">np:Th</ta>
            <ta e="T112" id="Seg_1736" s="T111">np:A</ta>
            <ta e="T113" id="Seg_1737" s="T112">np.h:P</ta>
            <ta e="T116" id="Seg_1738" s="T115">adv:Time</ta>
            <ta e="T117" id="Seg_1739" s="T116">pro.h:P</ta>
            <ta e="T119" id="Seg_1740" s="T118">0.3:A</ta>
            <ta e="T120" id="Seg_1741" s="T119">pro.h:Th</ta>
            <ta e="T126" id="Seg_1742" s="T125">0.3.h:E</ta>
            <ta e="T127" id="Seg_1743" s="T126">np:L</ta>
            <ta e="T130" id="Seg_1744" s="T129">np:Th</ta>
            <ta e="T131" id="Seg_1745" s="T130">0.3:P</ta>
            <ta e="T133" id="Seg_1746" s="T132">np.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_1747" s="T0">np:S</ta>
            <ta e="T3" id="Seg_1748" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_1749" s="T3">pro.h:S</ta>
            <ta e="T6" id="Seg_1750" s="T5">v:pred</ta>
            <ta e="T7" id="Seg_1751" s="T6">s:adv</ta>
            <ta e="T10" id="Seg_1752" s="T9">adj:pred</ta>
            <ta e="T11" id="Seg_1753" s="T10">cop</ta>
            <ta e="T12" id="Seg_1754" s="T11">pro.h:S</ta>
            <ta e="T15" id="Seg_1755" s="T14">v:pred</ta>
            <ta e="T18" id="Seg_1756" s="T15">s:temp</ta>
            <ta e="T19" id="Seg_1757" s="T18">pro.h:S</ta>
            <ta e="T21" id="Seg_1758" s="T20">v:pred</ta>
            <ta e="T22" id="Seg_1759" s="T21">pro.h:S</ta>
            <ta e="T24" id="Seg_1760" s="T23">s:adv</ta>
            <ta e="T25" id="Seg_1761" s="T24">v:pred</ta>
            <ta e="T27" id="Seg_1762" s="T26">np:S</ta>
            <ta e="T28" id="Seg_1763" s="T27">adj:pred</ta>
            <ta e="T31" id="Seg_1764" s="T28">s:temp</ta>
            <ta e="T32" id="Seg_1765" s="T31">pro.h:S</ta>
            <ta e="T33" id="Seg_1766" s="T32">v:pred</ta>
            <ta e="T34" id="Seg_1767" s="T33">s:adv</ta>
            <ta e="T37" id="Seg_1768" s="T36">np:O</ta>
            <ta e="T40" id="Seg_1769" s="T39">0.1.h:S v:pred</ta>
            <ta e="T43" id="Seg_1770" s="T42">0.1.h:S v:pred</ta>
            <ta e="T45" id="Seg_1771" s="T44">np.h:S</ta>
            <ta e="T46" id="Seg_1772" s="T45">v:pred</ta>
            <ta e="T48" id="Seg_1773" s="T46">s:purp</ta>
            <ta e="T51" id="Seg_1774" s="T50">0.3.h:S v:pred</ta>
            <ta e="T53" id="Seg_1775" s="T52">np.h:S</ta>
            <ta e="T55" id="Seg_1776" s="T54">s:purp</ta>
            <ta e="T56" id="Seg_1777" s="T55">v:pred</ta>
            <ta e="T58" id="Seg_1778" s="T57">np:O</ta>
            <ta e="T60" id="Seg_1779" s="T59">0.3.h:S v:pred</ta>
            <ta e="T62" id="Seg_1780" s="T61">np.h:S</ta>
            <ta e="T63" id="Seg_1781" s="T62">v:pred</ta>
            <ta e="T66" id="Seg_1782" s="T65">0.3.h:S v:pred</ta>
            <ta e="T69" id="Seg_1783" s="T68">0.3.h:S v:pred</ta>
            <ta e="T70" id="Seg_1784" s="T69">np:O</ta>
            <ta e="T72" id="Seg_1785" s="T71">0.3.h:S v:pred</ta>
            <ta e="T74" id="Seg_1786" s="T73">np:O</ta>
            <ta e="T76" id="Seg_1787" s="T75">np.h:S</ta>
            <ta e="T78" id="Seg_1788" s="T77">v:pred</ta>
            <ta e="T80" id="Seg_1789" s="T79">np.h:S</ta>
            <ta e="T82" id="Seg_1790" s="T81">0.3.h:O v:pred</ta>
            <ta e="T84" id="Seg_1791" s="T83">np.h:S</ta>
            <ta e="T85" id="Seg_1792" s="T84">adj:pred</ta>
            <ta e="T86" id="Seg_1793" s="T85">cop</ta>
            <ta e="T88" id="Seg_1794" s="T87">0.3.h:S v:pred</ta>
            <ta e="T91" id="Seg_1795" s="T90">np.h:S</ta>
            <ta e="T92" id="Seg_1796" s="T91">s:purp</ta>
            <ta e="T93" id="Seg_1797" s="T92">v:pred</ta>
            <ta e="T96" id="Seg_1798" s="T95">s:compl</ta>
            <ta e="T97" id="Seg_1799" s="T96">0.3.h:S v:pred</ta>
            <ta e="T100" id="Seg_1800" s="T99">s:purp</ta>
            <ta e="T101" id="Seg_1801" s="T100">0.3.h:S v:pred</ta>
            <ta e="T102" id="Seg_1802" s="T101">pro.h:S</ta>
            <ta e="T104" id="Seg_1803" s="T103">v:pred</ta>
            <ta e="T105" id="Seg_1804" s="T104">s:purp</ta>
            <ta e="T106" id="Seg_1805" s="T105">np:O</ta>
            <ta e="T107" id="Seg_1806" s="T106">0.1.h:S v:pred</ta>
            <ta e="T108" id="Seg_1807" s="T107">ptcl:pred</ta>
            <ta e="T110" id="Seg_1808" s="T109">np:S</ta>
            <ta e="T111" id="Seg_1809" s="T110">adj:pred</ta>
            <ta e="T112" id="Seg_1810" s="T111">np:S</ta>
            <ta e="T113" id="Seg_1811" s="T112">np.h:O</ta>
            <ta e="T114" id="Seg_1812" s="T113">v:pred</ta>
            <ta e="T117" id="Seg_1813" s="T116">pro.h:O</ta>
            <ta e="T119" id="Seg_1814" s="T118">0.3:S v:pred</ta>
            <ta e="T120" id="Seg_1815" s="T119">pro.h:S</ta>
            <ta e="T121" id="Seg_1816" s="T120">v:pred</ta>
            <ta e="T126" id="Seg_1817" s="T125">0.3.h:S v:pred</ta>
            <ta e="T128" id="Seg_1818" s="T127">v:pred</ta>
            <ta e="T130" id="Seg_1819" s="T129">np:S</ta>
            <ta e="T131" id="Seg_1820" s="T130">0.3:O v:pred</ta>
            <ta e="T133" id="Seg_1821" s="T132">np.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T67" id="Seg_1822" s="T66">RUS:gram</ta>
            <ta e="T94" id="Seg_1823" s="T93">RUS:gram</ta>
            <ta e="T98" id="Seg_1824" s="T97">RUS:gram</ta>
            <ta e="T122" id="Seg_1825" s="T121">RUS:mod</ta>
            <ta e="T127" id="Seg_1826" s="T126">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_1827" s="T0">Собака спит на улице.</ta>
            <ta e="T7" id="Seg_1828" s="T3">Я в лес ходил на охоту.</ta>
            <ta e="T11" id="Seg_1829" s="T7">В лесу очень хорошо.</ta>
            <ta e="T15" id="Seg_1830" s="T11">Я весной в лесу охотился.</ta>
            <ta e="T21" id="Seg_1831" s="T15">Когда лёд тронулся, я домой приехал.</ta>
            <ta e="T25" id="Seg_1832" s="T21">Я весной на охоту ездил.</ta>
            <ta e="T28" id="Seg_1833" s="T25">В лесу зверей много.</ta>
            <ta e="T36" id="Seg_1834" s="T28">Когда лёд тронулся, мы ездили рыбачить на Карасевое озеро.</ta>
            <ta e="T43" id="Seg_1835" s="T36">Очень много карасей наловили, потом домой приехали.</ta>
            <ta e="T48" id="Seg_1836" s="T43">Летом люди уезжают сено косить.</ta>
            <ta e="T51" id="Seg_1837" s="T48">Осенью домой приезжают.</ta>
            <ta e="T56" id="Seg_1838" s="T51">Один мужчина в лес на охоту ушёл.</ta>
            <ta e="T60" id="Seg_1839" s="T56">В лесу он много зверей убил.</ta>
            <ta e="T64" id="Seg_1840" s="T60">Один старик жил в лесу.</ta>
            <ta e="T70" id="Seg_1841" s="T64">Зимой он охотился, а летом делал запор.</ta>
            <ta e="T74" id="Seg_1842" s="T70">Летом он добывает много рыбы.</ta>
            <ta e="T82" id="Seg_1843" s="T74">Один мальчик в воду нырнул, потом его люди вытащили.</ta>
            <ta e="T86" id="Seg_1844" s="T82">На улице людей много.</ta>
            <ta e="T88" id="Seg_1845" s="T86">Ночью [люди] спят.</ta>
            <ta e="T97" id="Seg_1846" s="T88">Утром люди на работу идут, а вечером заканчивают работать.</ta>
            <ta e="T101" id="Seg_1847" s="T97">А днем кушать идут.</ta>
            <ta e="T105" id="Seg_1848" s="T101">Мы вечером ездили рыбачить.</ta>
            <ta e="T108" id="Seg_1849" s="T105">Рыбы мы не добыли.</ta>
            <ta e="T111" id="Seg_1850" s="T108">Летом комаров много.</ta>
            <ta e="T115" id="Seg_1851" s="T111">Комары человека сильно кусают.</ta>
            <ta e="T119" id="Seg_1852" s="T115">Раньше меня тоже кусали.</ta>
            <ta e="T126" id="Seg_1853" s="T119">Он [был?] ничего не боялся.</ta>
            <ta e="T130" id="Seg_1854" s="T126">На столе лежит много писем.</ta>
            <ta e="T133" id="Seg_1855" s="T130">Написал маленький мальчик.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_1856" s="T0">The dog sleeps outside.</ta>
            <ta e="T7" id="Seg_1857" s="T3">I went hunting to the forest.</ta>
            <ta e="T11" id="Seg_1858" s="T7">It is very good in the forest.</ta>
            <ta e="T15" id="Seg_1859" s="T11">In spring I went hunting.</ta>
            <ta e="T21" id="Seg_1860" s="T15">As the ice melted, I came back home.</ta>
            <ta e="T25" id="Seg_1861" s="T21">I went hunting in spring.</ta>
            <ta e="T28" id="Seg_1862" s="T25">There are many animals in the forest.</ta>
            <ta e="T36" id="Seg_1863" s="T28">As the ice melted, we went fishing on the Lake Karasevo.</ta>
            <ta e="T43" id="Seg_1864" s="T36">We caught many crucians, then we came home.</ta>
            <ta e="T48" id="Seg_1865" s="T43">In summer people go away to crop hay.</ta>
            <ta e="T51" id="Seg_1866" s="T48">In autumn they come back home.</ta>
            <ta e="T56" id="Seg_1867" s="T51">One man went hunting to the forest.</ta>
            <ta e="T60" id="Seg_1868" s="T56">In the forest he killed many animals.</ta>
            <ta e="T64" id="Seg_1869" s="T60">One old man lived in the forest.</ta>
            <ta e="T70" id="Seg_1870" s="T64">In winter he hunted, in summer he made fish weirs.</ta>
            <ta e="T74" id="Seg_1871" s="T70">In summer he caught a lot of fish.</ta>
            <ta e="T82" id="Seg_1872" s="T74">One boy dived into the water, then people got him out.</ta>
            <ta e="T86" id="Seg_1873" s="T82">There are many people outside.</ta>
            <ta e="T88" id="Seg_1874" s="T86">They sleep at night.</ta>
            <ta e="T97" id="Seg_1875" s="T88">In the morning people go to work, and in the evening they stop working.</ta>
            <ta e="T101" id="Seg_1876" s="T97">And in the daytime they go to eat.</ta>
            <ta e="T105" id="Seg_1877" s="T101">In the evening we went fishing.</ta>
            <ta e="T108" id="Seg_1878" s="T105">We didn't catch any fish.</ta>
            <ta e="T111" id="Seg_1879" s="T108">There are many mosquitoes in summer.</ta>
            <ta e="T115" id="Seg_1880" s="T111">Mosquitoes sting humans strongly.</ta>
            <ta e="T119" id="Seg_1881" s="T115">Earlier they used to bite me too.</ta>
            <ta e="T126" id="Seg_1882" s="T119">He [was?] not afraid of anything.</ta>
            <ta e="T130" id="Seg_1883" s="T126">There are many letters on the table.</ta>
            <ta e="T133" id="Seg_1884" s="T130">A small boy wrote [them].</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_1885" s="T0">Der Hund schläft draußen.</ta>
            <ta e="T7" id="Seg_1886" s="T3">Ich ging zur Jagd in den Wald.</ta>
            <ta e="T11" id="Seg_1887" s="T7">Es ist sehr schön im Wald.</ta>
            <ta e="T15" id="Seg_1888" s="T11">Ich jagte im Frühling im Wald.</ta>
            <ta e="T21" id="Seg_1889" s="T15">Als das Eis schmolz, kam ich nach Hause zurück.</ta>
            <ta e="T25" id="Seg_1890" s="T21">Ich bin im Frühling zur Jagd gegangen.</ta>
            <ta e="T28" id="Seg_1891" s="T25">Es gibt viele Tiere im Wald.</ta>
            <ta e="T36" id="Seg_1892" s="T28">Als das Eis schmolz, sind wir zum See Karasevo zum Fischen gefahren.</ta>
            <ta e="T43" id="Seg_1893" s="T36">Wir haben viele Karauschen gefangen, danach sind wir nach Hause gekommen.</ta>
            <ta e="T48" id="Seg_1894" s="T43">Im Sommer gehen Leute Gras mähen.</ta>
            <ta e="T51" id="Seg_1895" s="T48">Im Herbst kommen sie wieder nach Hause.</ta>
            <ta e="T56" id="Seg_1896" s="T51">Ein Mann ging zur Jagd in den Wald.</ta>
            <ta e="T60" id="Seg_1897" s="T56">Im Wald tötete er viele Tiere.</ta>
            <ta e="T64" id="Seg_1898" s="T60">Eine alte Frau lebte im Wald.</ta>
            <ta e="T70" id="Seg_1899" s="T64">Im Winter ging er zur Jagd, im Sommer machte er Fischwehre.</ta>
            <ta e="T74" id="Seg_1900" s="T70">Im Sommer fing er viel Fisch.</ta>
            <ta e="T82" id="Seg_1901" s="T74">Ein Junge tauchte ins Wasser, dann holten ihn Leute heraus.</ta>
            <ta e="T86" id="Seg_1902" s="T82">Draußen sind viele Leute.</ta>
            <ta e="T88" id="Seg_1903" s="T86">In der Nacht schlafen sie.</ta>
            <ta e="T97" id="Seg_1904" s="T88">Morgens gehen die Leute zur Arbeit und abends hören sie auf zu arbeiten.</ta>
            <ta e="T101" id="Seg_1905" s="T97">Und am Tage gehen sie essen.</ta>
            <ta e="T105" id="Seg_1906" s="T101">Am Abend sind wir fischen gefahren.</ta>
            <ta e="T108" id="Seg_1907" s="T105">Wir fingen keinen Fisch.</ta>
            <ta e="T111" id="Seg_1908" s="T108">Es gibt viele Mücken im Sommer.</ta>
            <ta e="T115" id="Seg_1909" s="T111">Mücken stechen Menschen sehr doll.</ta>
            <ta e="T119" id="Seg_1910" s="T115">Früher haben sie mich auch sehr doll gestochen.</ta>
            <ta e="T126" id="Seg_1911" s="T119">Er [hatte?] keine Angst vor irgendetwas.</ta>
            <ta e="T130" id="Seg_1912" s="T126">Auf dem Tisch liegen viele Briefe.</ta>
            <ta e="T133" id="Seg_1913" s="T130">Ein kleiner Junge hat [sie] geschrieben.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_1914" s="T0">собака на улице спит</ta>
            <ta e="T7" id="Seg_1915" s="T3">я в лес ходил на охоту</ta>
            <ta e="T11" id="Seg_1916" s="T7">в лесу очень хорошо.</ta>
            <ta e="T15" id="Seg_1917" s="T11">я весной в лесу охотился</ta>
            <ta e="T21" id="Seg_1918" s="T15">когда лед ушел я домой пришел [приехал].</ta>
            <ta e="T25" id="Seg_1919" s="T21">я весной на охоту ездил.</ta>
            <ta e="T28" id="Seg_1920" s="T25">В лесу зверей много.</ta>
            <ta e="T36" id="Seg_1921" s="T28">когда лёд ушел мы ездили рыбачить на карасьево озеро.</ta>
            <ta e="T43" id="Seg_1922" s="T36">карасей очень много добыли значит домой ехали.</ta>
            <ta e="T48" id="Seg_1923" s="T43">Летом люди едут сено косить.</ta>
            <ta e="T51" id="Seg_1924" s="T48">Осенью домой приезжают.</ta>
            <ta e="T56" id="Seg_1925" s="T51">один мужчина в лес охотиться ушел.</ta>
            <ta e="T60" id="Seg_1926" s="T56">в лесу зверей много убил [добыл].</ta>
            <ta e="T64" id="Seg_1927" s="T60">один старик жил в лесу.</ta>
            <ta e="T70" id="Seg_1928" s="T64">зимой охотился, а летом делал забор.</ta>
            <ta e="T74" id="Seg_1929" s="T70">летом добывает много рыбы.</ta>
            <ta e="T82" id="Seg_1930" s="T74">один мальчик в воду упал (утонул), значит люди вытащили его.</ta>
            <ta e="T86" id="Seg_1931" s="T82">на улице людей много.</ta>
            <ta e="T88" id="Seg_1932" s="T86">ночь спят</ta>
            <ta e="T97" id="Seg_1933" s="T88">все утром люди на работу идут а вечером работать кончают.</ta>
            <ta e="T101" id="Seg_1934" s="T97">а днем кушать идут.</ta>
            <ta e="T105" id="Seg_1935" s="T101">мы вечером ездили рыбачить.</ta>
            <ta e="T108" id="Seg_1936" s="T105">рыбы не добыли.</ta>
            <ta e="T111" id="Seg_1937" s="T108">летом комаров много.</ta>
            <ta e="T115" id="Seg_1938" s="T111">комары человека кусаются сильно.</ta>
            <ta e="T119" id="Seg_1939" s="T115">раньше мне тоже кусали.</ta>
            <ta e="T126" id="Seg_1940" s="T119">он бы ни чего не боялся.</ta>
            <ta e="T130" id="Seg_1941" s="T126">на столе лежат много писем.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
