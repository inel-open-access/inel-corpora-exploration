<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID3F8CFB91-DE3A-F58E-3FF5-226408ACCF24">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\SG_196X_Hunting_nar\SG_196X_Hunting_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">59</ud-information>
            <ud-information attribute-name="# HIAT:w">46</ud-information>
            <ud-information attribute-name="# e">46</ud-information>
            <ud-information attribute-name="# HIAT:u">10</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SG">
            <abbreviation>SG</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SG"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T46" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">pondəpod</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">mat</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">kojasak</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">täbešku</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">qaimnäj</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">aː</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">kwässak</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">mat</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">kojazak</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">kanakse</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">Lapkazo</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_44" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">mat</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">kojazak</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">šidə</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">äreɣand</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_59" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">na</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">kwäsap</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">sombɨla</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">šiːp</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_74" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">sugulʼǯʼe</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">tadelʼe</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">zdavaiksap</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">lapkondə</ts>
                  <nts id="Seg_86" n="HIAT:ip">.</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_89" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_91" n="HIAT:w" s="T23">na</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">šiːtqo</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_96" n="HIAT:ip">(</nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">polučiksap</ts>
                  <nts id="Seg_99" n="HIAT:ip">)</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">iːzap</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_105" n="HIAT:w" s="T27">qomdäp</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_108" n="HIAT:w" s="T28">tot</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_111" n="HIAT:w" s="T29">salkwap</ts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_115" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_117" n="HIAT:w" s="T30">na</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_120" n="HIAT:w" s="T31">qomdeʒe</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_123" n="HIAT:w" s="T32">tauzap</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_126" n="HIAT:w" s="T33">tülsep</ts>
                  <nts id="Seg_127" n="HIAT:ip">,</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">tüːlšep</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">šäqädɨl</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_137" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_139" n="HIAT:w" s="T36">na</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">tülšeze</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">peqap</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_148" n="HIAT:w" s="T39">čaǯʼisap</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_151" n="HIAT:w" s="T40">aː</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_154" n="HIAT:w" s="T41">čarnɨt</ts>
                  <nts id="Seg_155" n="HIAT:ip">.</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_158" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_160" n="HIAT:w" s="T42">peqom</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_163" n="HIAT:w" s="T43">niːk</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_166" n="HIAT:w" s="T44">i</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">qwänna</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T46" id="Seg_172" n="sc" s="T0">
               <ts e="T1" id="Seg_174" n="e" s="T0">pondəpod </ts>
               <ts e="T2" id="Seg_176" n="e" s="T1">mat </ts>
               <ts e="T3" id="Seg_178" n="e" s="T2">kojasak </ts>
               <ts e="T4" id="Seg_180" n="e" s="T3">täbešku. </ts>
               <ts e="T5" id="Seg_182" n="e" s="T4">qaimnäj </ts>
               <ts e="T6" id="Seg_184" n="e" s="T5">aː </ts>
               <ts e="T7" id="Seg_186" n="e" s="T6">kwässak. </ts>
               <ts e="T8" id="Seg_188" n="e" s="T7">mat </ts>
               <ts e="T9" id="Seg_190" n="e" s="T8">kojazak </ts>
               <ts e="T10" id="Seg_192" n="e" s="T9">kanakse </ts>
               <ts e="T11" id="Seg_194" n="e" s="T10">Lapkazo. </ts>
               <ts e="T12" id="Seg_196" n="e" s="T11">mat </ts>
               <ts e="T13" id="Seg_198" n="e" s="T12">kojazak </ts>
               <ts e="T14" id="Seg_200" n="e" s="T13">šidə </ts>
               <ts e="T15" id="Seg_202" n="e" s="T14">äreɣand. </ts>
               <ts e="T16" id="Seg_204" n="e" s="T15">na </ts>
               <ts e="T17" id="Seg_206" n="e" s="T16">kwäsap </ts>
               <ts e="T18" id="Seg_208" n="e" s="T17">sombɨla </ts>
               <ts e="T19" id="Seg_210" n="e" s="T18">šiːp. </ts>
               <ts e="T20" id="Seg_212" n="e" s="T19">sugulʼǯʼe </ts>
               <ts e="T21" id="Seg_214" n="e" s="T20">tadelʼe </ts>
               <ts e="T22" id="Seg_216" n="e" s="T21">zdavaiksap </ts>
               <ts e="T23" id="Seg_218" n="e" s="T22">lapkondə. </ts>
               <ts e="T24" id="Seg_220" n="e" s="T23">na </ts>
               <ts e="T25" id="Seg_222" n="e" s="T24">šiːtqo </ts>
               <ts e="T26" id="Seg_224" n="e" s="T25">(polučiksap) </ts>
               <ts e="T27" id="Seg_226" n="e" s="T26">iːzap </ts>
               <ts e="T28" id="Seg_228" n="e" s="T27">qomdäp </ts>
               <ts e="T29" id="Seg_230" n="e" s="T28">tot </ts>
               <ts e="T30" id="Seg_232" n="e" s="T29">salkwap. </ts>
               <ts e="T31" id="Seg_234" n="e" s="T30">na </ts>
               <ts e="T32" id="Seg_236" n="e" s="T31">qomdeʒe </ts>
               <ts e="T33" id="Seg_238" n="e" s="T32">tauzap </ts>
               <ts e="T34" id="Seg_240" n="e" s="T33">tülsep, </ts>
               <ts e="T35" id="Seg_242" n="e" s="T34">tüːlšep </ts>
               <ts e="T36" id="Seg_244" n="e" s="T35">šäqädɨl. </ts>
               <ts e="T37" id="Seg_246" n="e" s="T36">na </ts>
               <ts e="T38" id="Seg_248" n="e" s="T37">tülšeze </ts>
               <ts e="T39" id="Seg_250" n="e" s="T38">peqap </ts>
               <ts e="T40" id="Seg_252" n="e" s="T39">čaǯʼisap </ts>
               <ts e="T41" id="Seg_254" n="e" s="T40">aː </ts>
               <ts e="T42" id="Seg_256" n="e" s="T41">čarnɨt. </ts>
               <ts e="T43" id="Seg_258" n="e" s="T42">peqom </ts>
               <ts e="T44" id="Seg_260" n="e" s="T43">niːk </ts>
               <ts e="T45" id="Seg_262" n="e" s="T44">i </ts>
               <ts e="T46" id="Seg_264" n="e" s="T45">qwänna. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_265" s="T0">SG_196X_Hunting_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_266" s="T4">SG_196X_Hunting_nar.002 (001.002)</ta>
            <ta e="T11" id="Seg_267" s="T7">SG_196X_Hunting_nar.003 (001.003)</ta>
            <ta e="T15" id="Seg_268" s="T11">SG_196X_Hunting_nar.004 (001.004)</ta>
            <ta e="T19" id="Seg_269" s="T15">SG_196X_Hunting_nar.005 (001.005)</ta>
            <ta e="T23" id="Seg_270" s="T19">SG_196X_Hunting_nar.006 (001.006)</ta>
            <ta e="T30" id="Seg_271" s="T23">SG_196X_Hunting_nar.007 (001.007)</ta>
            <ta e="T36" id="Seg_272" s="T30">SG_196X_Hunting_nar.008 (001.008)</ta>
            <ta e="T42" id="Seg_273" s="T36">SG_196X_Hunting_nar.009 (001.009)</ta>
            <ta e="T46" id="Seg_274" s="T42">SG_196X_Hunting_nar.010 (001.010)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_275" s="T0">′пондъпод мат ко′jасак тӓ′бешку.</ta>
            <ta e="T7" id="Seg_276" s="T4">kаимнӓй а̄ ′квӓссак.</ta>
            <ta e="T11" id="Seg_277" s="T7">мат коjазак ка′наксе ′Лапказо.</ta>
            <ta e="T15" id="Seg_278" s="T11">мат ко′jазак ′шидъ ӓ′реɣанд.</ta>
            <ta e="T19" id="Seg_279" s="T15">на ′квӓсап ′сомбыла шӣп.</ta>
            <ta e="T23" id="Seg_280" s="T19">сугулʼ′дʼжʼе ‵таде′лʼе ′здаваиксап ′лапкондъ.</ta>
            <ta e="T30" id="Seg_281" s="T23">на шӣтkо получиксап ′ӣзап kомдӓп тот салк′вап.</ta>
            <ta e="T36" id="Seg_282" s="T30">на ′kомдеж[з]е ′таузап ′тӱlсеп, ′тӱ̄lшʼеп шʼӓkӓдыл.</ta>
            <ta e="T42" id="Seg_283" s="T36">на ′тӱlшʼезе ′пеkап ′чадʼжʼисап а̄ ′чарныт.</ta>
            <ta e="T46" id="Seg_284" s="T42">′пеkом нӣк и kwӓ′нна.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_285" s="T0">pondəpod mat kojasak täbešku.</ta>
            <ta e="T7" id="Seg_286" s="T4">qaimnäj aː kvässak.</ta>
            <ta e="T11" id="Seg_287" s="T7">mat kojazak kanakse Lapkazo.</ta>
            <ta e="T15" id="Seg_288" s="T11">mat kojazak šidə äreɣand.</ta>
            <ta e="T19" id="Seg_289" s="T15">na kväsap sombɨla šiːp.</ta>
            <ta e="T23" id="Seg_290" s="T19">sugulʼǯʼe tadelʼe zdavaiksap lapkondə.</ta>
            <ta e="T30" id="Seg_291" s="T23">na šiːtqo polučiksap iːzap qomdäp tot salkvap.</ta>
            <ta e="T36" id="Seg_292" s="T30">na qomdeʒ[z]e tauzap tülsep, tüːlšep šäqädɨl.</ta>
            <ta e="T42" id="Seg_293" s="T36">na tülšeze peqap čaǯʼisap aː čarnɨt.</ta>
            <ta e="T46" id="Seg_294" s="T42">peqom niːk i qwänna.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_295" s="T0">pondəpod mat kojasak täbešku. </ta>
            <ta e="T7" id="Seg_296" s="T4">qaimnäj aː kwässak. </ta>
            <ta e="T11" id="Seg_297" s="T7">mat kojazak kanakse Lapkazo. </ta>
            <ta e="T15" id="Seg_298" s="T11">mat kojazak šidə äreɣand. </ta>
            <ta e="T19" id="Seg_299" s="T15">na kwäsap sombɨla šiːp. </ta>
            <ta e="T23" id="Seg_300" s="T19">sugulʼǯʼe tadelʼe zdavaiksap lapkondə. </ta>
            <ta e="T30" id="Seg_301" s="T23">na šiːtqo (polučiksap) iːzap qomdäp tot salkwap. </ta>
            <ta e="T36" id="Seg_302" s="T30">na qomdeʒe tauzap tülsep, tüːlšep šäqädɨl. </ta>
            <ta e="T42" id="Seg_303" s="T36">na tülšeze peqap čaǯʼisap aː čarnɨt. </ta>
            <ta e="T46" id="Seg_304" s="T42">peqom niːk i qwänna. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_305" s="T0">pondə-po-d</ta>
            <ta e="T2" id="Seg_306" s="T1">mat</ta>
            <ta e="T3" id="Seg_307" s="T2">koja-sa-k</ta>
            <ta e="T4" id="Seg_308" s="T3">täbe-š-ku</ta>
            <ta e="T5" id="Seg_309" s="T4">qai-m-näj</ta>
            <ta e="T6" id="Seg_310" s="T5">aː</ta>
            <ta e="T7" id="Seg_311" s="T6">kwäs-sa-k</ta>
            <ta e="T8" id="Seg_312" s="T7">mat</ta>
            <ta e="T9" id="Seg_313" s="T8">koja-za-k</ta>
            <ta e="T10" id="Seg_314" s="T9">kanak-se</ta>
            <ta e="T11" id="Seg_315" s="T10">Lapka-zo</ta>
            <ta e="T12" id="Seg_316" s="T11">mat</ta>
            <ta e="T13" id="Seg_317" s="T12">koja-za-k</ta>
            <ta e="T14" id="Seg_318" s="T13">šidə</ta>
            <ta e="T15" id="Seg_319" s="T14">äre-ɣan-d</ta>
            <ta e="T16" id="Seg_320" s="T15">na</ta>
            <ta e="T17" id="Seg_321" s="T16">kwä-sa-p</ta>
            <ta e="T18" id="Seg_322" s="T17">sombɨla</ta>
            <ta e="T19" id="Seg_323" s="T18">šiː-p</ta>
            <ta e="T20" id="Seg_324" s="T19">sugulʼǯʼe</ta>
            <ta e="T21" id="Seg_325" s="T20">tade-lʼe</ta>
            <ta e="T22" id="Seg_326" s="T21">zdavai-k-sa-p</ta>
            <ta e="T23" id="Seg_327" s="T22">lapko-ndə</ta>
            <ta e="T24" id="Seg_328" s="T23">na</ta>
            <ta e="T25" id="Seg_329" s="T24">šiː-tqo</ta>
            <ta e="T26" id="Seg_330" s="T25">poluči-k-sa-p</ta>
            <ta e="T27" id="Seg_331" s="T26">iː-za-p</ta>
            <ta e="T28" id="Seg_332" s="T27">qomdä-p</ta>
            <ta e="T29" id="Seg_333" s="T28">ton</ta>
            <ta e="T30" id="Seg_334" s="T29">salkwa-p</ta>
            <ta e="T31" id="Seg_335" s="T30">na</ta>
            <ta e="T32" id="Seg_336" s="T31">qomde-ʒe</ta>
            <ta e="T33" id="Seg_337" s="T32">tau-za-p</ta>
            <ta e="T34" id="Seg_338" s="T33">tülse-p</ta>
            <ta e="T35" id="Seg_339" s="T34">tüːlše-p</ta>
            <ta e="T36" id="Seg_340" s="T35">šä-qädɨ-l</ta>
            <ta e="T37" id="Seg_341" s="T36">na</ta>
            <ta e="T38" id="Seg_342" s="T37">tülše-ze</ta>
            <ta e="T39" id="Seg_343" s="T38">peqa-p</ta>
            <ta e="T40" id="Seg_344" s="T39">čaǯʼi-sa-p</ta>
            <ta e="T41" id="Seg_345" s="T40">aː</ta>
            <ta e="T42" id="Seg_346" s="T41">čar-nɨ-t</ta>
            <ta e="T43" id="Seg_347" s="T42">peqo-m</ta>
            <ta e="T44" id="Seg_348" s="T43">niː-k</ta>
            <ta e="T45" id="Seg_349" s="T44">i</ta>
            <ta e="T46" id="Seg_350" s="T45">qwän-na</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_351" s="T0">pondə-po-t</ta>
            <ta e="T2" id="Seg_352" s="T1">man</ta>
            <ta e="T3" id="Seg_353" s="T2">koja-sɨ-k</ta>
            <ta e="T4" id="Seg_354" s="T3">tabek-š-gu</ta>
            <ta e="T5" id="Seg_355" s="T4">qaj-m-naj</ta>
            <ta e="T6" id="Seg_356" s="T5">aː</ta>
            <ta e="T7" id="Seg_357" s="T6">kwat-sɨ-k</ta>
            <ta e="T8" id="Seg_358" s="T7">man</ta>
            <ta e="T9" id="Seg_359" s="T8">koja-sɨ-k</ta>
            <ta e="T10" id="Seg_360" s="T9">kanak-se</ta>
            <ta e="T11" id="Seg_361" s="T10">Lapka-se</ta>
            <ta e="T12" id="Seg_362" s="T11">man</ta>
            <ta e="T13" id="Seg_363" s="T12">koja-sɨ-k</ta>
            <ta e="T14" id="Seg_364" s="T13">šitə</ta>
            <ta e="T15" id="Seg_365" s="T14">ärä-qɨn-t</ta>
            <ta e="T16" id="Seg_366" s="T15">na</ta>
            <ta e="T17" id="Seg_367" s="T16">kwat-sɨ-p</ta>
            <ta e="T18" id="Seg_368" s="T17">sombla</ta>
            <ta e="T19" id="Seg_369" s="T18">ši-p</ta>
            <ta e="T20" id="Seg_370" s="T19">sugulʼǯʼe</ta>
            <ta e="T21" id="Seg_371" s="T20">tade-le</ta>
            <ta e="T22" id="Seg_372" s="T21">zdavai-k-sɨ-m</ta>
            <ta e="T23" id="Seg_373" s="T22">laqqa-nde</ta>
            <ta e="T24" id="Seg_374" s="T23">na</ta>
            <ta e="T25" id="Seg_375" s="T24">ši-tqo</ta>
            <ta e="T26" id="Seg_376" s="T25">poluči-k-sɨ-m</ta>
            <ta e="T27" id="Seg_377" s="T26">iː-sɨ-m</ta>
            <ta e="T28" id="Seg_378" s="T27">qomde-p</ta>
            <ta e="T29" id="Seg_379" s="T28">ton</ta>
            <ta e="T30" id="Seg_380" s="T29">salkwa-p</ta>
            <ta e="T31" id="Seg_381" s="T30">na</ta>
            <ta e="T32" id="Seg_382" s="T31">qomde-se</ta>
            <ta e="T33" id="Seg_383" s="T32">taw-sɨ-m</ta>
            <ta e="T34" id="Seg_384" s="T33">tüːlʼde-p</ta>
            <ta e="T35" id="Seg_385" s="T34">tüːlʼde-p</ta>
            <ta e="T36" id="Seg_386" s="T35">šakut-qedɨ-lʼ</ta>
            <ta e="T37" id="Seg_387" s="T36">na</ta>
            <ta e="T38" id="Seg_388" s="T37">tüːlʼde-se</ta>
            <ta e="T39" id="Seg_389" s="T38">peqqa-m</ta>
            <ta e="T40" id="Seg_390" s="T39">čačɨ-sɨ-m</ta>
            <ta e="T41" id="Seg_391" s="T40">aː</ta>
            <ta e="T42" id="Seg_392" s="T41">čʼart-ŋɨ-t</ta>
            <ta e="T43" id="Seg_393" s="T42">peqqa-mɨ</ta>
            <ta e="T44" id="Seg_394" s="T43">nɨdi-k</ta>
            <ta e="T45" id="Seg_395" s="T44">i</ta>
            <ta e="T46" id="Seg_396" s="T45">qwän-ŋɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_397" s="T0">last-year-ADV.LOC</ta>
            <ta e="T2" id="Seg_398" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_399" s="T2">go-PST-1SG.S</ta>
            <ta e="T4" id="Seg_400" s="T3">squirrel-CAP-INF</ta>
            <ta e="T5" id="Seg_401" s="T4">what-ACC-EMPH</ta>
            <ta e="T6" id="Seg_402" s="T5">NEG</ta>
            <ta e="T7" id="Seg_403" s="T6">catch-PST-1SG.S</ta>
            <ta e="T8" id="Seg_404" s="T7">I.NOM</ta>
            <ta e="T9" id="Seg_405" s="T8">go-PST-1SG.S</ta>
            <ta e="T10" id="Seg_406" s="T9">dog-COM</ta>
            <ta e="T11" id="Seg_407" s="T10">Lapka-COM</ta>
            <ta e="T12" id="Seg_408" s="T11">I.NOM</ta>
            <ta e="T13" id="Seg_409" s="T12">go-PST-1SG.S</ta>
            <ta e="T14" id="Seg_410" s="T13">two</ta>
            <ta e="T15" id="Seg_411" s="T14">month-LOC-3SG</ta>
            <ta e="T16" id="Seg_412" s="T15">this</ta>
            <ta e="T17" id="Seg_413" s="T16">kill-PST-ACC</ta>
            <ta e="T18" id="Seg_414" s="T17">five</ta>
            <ta e="T19" id="Seg_415" s="T18">sable-ACC</ta>
            <ta e="T20" id="Seg_416" s="T19">home</ta>
            <ta e="T21" id="Seg_417" s="T20">bring-CVB</ta>
            <ta e="T22" id="Seg_418" s="T21">give-VBLZ-PST-1SG.O</ta>
            <ta e="T23" id="Seg_419" s="T22">shop-ILL</ta>
            <ta e="T24" id="Seg_420" s="T23">this</ta>
            <ta e="T25" id="Seg_421" s="T24">sable-TRL</ta>
            <ta e="T26" id="Seg_422" s="T25">get-VBLZ-PST-1SG.O</ta>
            <ta e="T27" id="Seg_423" s="T26">take-PST-1SG.O</ta>
            <ta e="T28" id="Seg_424" s="T27">money-ACC</ta>
            <ta e="T29" id="Seg_425" s="T28">hundred</ta>
            <ta e="T30" id="Seg_426" s="T29">ruble-ACC</ta>
            <ta e="T31" id="Seg_427" s="T30">this</ta>
            <ta e="T32" id="Seg_428" s="T31">money-INSTR</ta>
            <ta e="T33" id="Seg_429" s="T32">buy-PST-1SG.O</ta>
            <ta e="T34" id="Seg_430" s="T33">rifle-ACC</ta>
            <ta e="T35" id="Seg_431" s="T34">rifle-ACC</ta>
            <ta e="T36" id="Seg_432" s="T35">hammer-CAR-ADJZ</ta>
            <ta e="T37" id="Seg_433" s="T36">this</ta>
            <ta e="T38" id="Seg_434" s="T37">rifle-INSTR</ta>
            <ta e="T39" id="Seg_435" s="T38">elk-ACC</ta>
            <ta e="T40" id="Seg_436" s="T39">shoot-PST-1SG.O</ta>
            <ta e="T41" id="Seg_437" s="T40">NEG</ta>
            <ta e="T42" id="Seg_438" s="T41">hit-CO-3SG.O</ta>
            <ta e="T43" id="Seg_439" s="T42">elk-1SG</ta>
            <ta e="T44" id="Seg_440" s="T43">such-ADV</ta>
            <ta e="T45" id="Seg_441" s="T44">and</ta>
            <ta e="T46" id="Seg_442" s="T45">go.away-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_443" s="T0">прошлый-год-ADV.LOC</ta>
            <ta e="T2" id="Seg_444" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_445" s="T2">ходить-PST-1SG.S</ta>
            <ta e="T4" id="Seg_446" s="T3">белка-CAP-INF</ta>
            <ta e="T5" id="Seg_447" s="T4">что-ACC-EMPH</ta>
            <ta e="T6" id="Seg_448" s="T5">NEG</ta>
            <ta e="T7" id="Seg_449" s="T6">поймать-PST-1SG.S</ta>
            <ta e="T8" id="Seg_450" s="T7">я.NOM</ta>
            <ta e="T9" id="Seg_451" s="T8">ходить-PST-1SG.S</ta>
            <ta e="T10" id="Seg_452" s="T9">собака-COM</ta>
            <ta e="T11" id="Seg_453" s="T10">Лапка-COM</ta>
            <ta e="T12" id="Seg_454" s="T11">я.NOM</ta>
            <ta e="T13" id="Seg_455" s="T12">ходить-PST-1SG.S</ta>
            <ta e="T14" id="Seg_456" s="T13">два</ta>
            <ta e="T15" id="Seg_457" s="T14">месяц-LOC-3SG</ta>
            <ta e="T16" id="Seg_458" s="T15">этот</ta>
            <ta e="T17" id="Seg_459" s="T16">убить-PST-ACC</ta>
            <ta e="T18" id="Seg_460" s="T17">пять</ta>
            <ta e="T19" id="Seg_461" s="T18">соболь-ACC</ta>
            <ta e="T20" id="Seg_462" s="T19">домой</ta>
            <ta e="T21" id="Seg_463" s="T20">принести-CVB</ta>
            <ta e="T22" id="Seg_464" s="T21">сдавать-VBLZ-PST-1SG.O</ta>
            <ta e="T23" id="Seg_465" s="T22">лавка-ILL</ta>
            <ta e="T24" id="Seg_466" s="T23">этот</ta>
            <ta e="T25" id="Seg_467" s="T24">соболь-TRL</ta>
            <ta e="T26" id="Seg_468" s="T25">получить-VBLZ-PST-1SG.O</ta>
            <ta e="T27" id="Seg_469" s="T26">взять-PST-1SG.O</ta>
            <ta e="T28" id="Seg_470" s="T27">деньги-ACC</ta>
            <ta e="T29" id="Seg_471" s="T28">сто</ta>
            <ta e="T30" id="Seg_472" s="T29">рубль-ACC</ta>
            <ta e="T31" id="Seg_473" s="T30">этот</ta>
            <ta e="T32" id="Seg_474" s="T31">деньги-INSTR</ta>
            <ta e="T33" id="Seg_475" s="T32">купить-PST-1SG.O</ta>
            <ta e="T34" id="Seg_476" s="T33">ружье-ACC</ta>
            <ta e="T35" id="Seg_477" s="T34">ружье-ACC</ta>
            <ta e="T36" id="Seg_478" s="T35">курок-CAR-ADJZ</ta>
            <ta e="T37" id="Seg_479" s="T36">этот</ta>
            <ta e="T38" id="Seg_480" s="T37">ружье-INSTR</ta>
            <ta e="T39" id="Seg_481" s="T38">лось-ACC</ta>
            <ta e="T40" id="Seg_482" s="T39">стрелять-PST-1SG.O</ta>
            <ta e="T41" id="Seg_483" s="T40">NEG</ta>
            <ta e="T42" id="Seg_484" s="T41">попасть.в.цель-CO-3SG.O</ta>
            <ta e="T43" id="Seg_485" s="T42">лось-1SG</ta>
            <ta e="T44" id="Seg_486" s="T43">такой-ADV</ta>
            <ta e="T45" id="Seg_487" s="T44">и</ta>
            <ta e="T46" id="Seg_488" s="T45">пойти-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_489" s="T0">adj-n-adv:case</ta>
            <ta e="T2" id="Seg_490" s="T1">pers</ta>
            <ta e="T3" id="Seg_491" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_492" s="T3">n-n&gt;v-v:ninf</ta>
            <ta e="T5" id="Seg_493" s="T4">interrog-n:case1-clit</ta>
            <ta e="T6" id="Seg_494" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_495" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_496" s="T7">pers</ta>
            <ta e="T9" id="Seg_497" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_498" s="T9">n-n:case2</ta>
            <ta e="T11" id="Seg_499" s="T10">nprop-n:case2</ta>
            <ta e="T12" id="Seg_500" s="T11">pers</ta>
            <ta e="T13" id="Seg_501" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_502" s="T13">num</ta>
            <ta e="T15" id="Seg_503" s="T14">n-n:case1-n:poss</ta>
            <ta e="T16" id="Seg_504" s="T15">dem</ta>
            <ta e="T17" id="Seg_505" s="T16">v-v:tense-n:case1</ta>
            <ta e="T18" id="Seg_506" s="T17">num</ta>
            <ta e="T19" id="Seg_507" s="T18">n-n:case1</ta>
            <ta e="T20" id="Seg_508" s="T19">adv</ta>
            <ta e="T21" id="Seg_509" s="T20">v-v&gt;adv</ta>
            <ta e="T22" id="Seg_510" s="T21">v-n&gt;v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_511" s="T22">n-n:case2</ta>
            <ta e="T24" id="Seg_512" s="T23">dem</ta>
            <ta e="T25" id="Seg_513" s="T24">n-n:case1</ta>
            <ta e="T26" id="Seg_514" s="T25">v-n&gt;v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_515" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_516" s="T27">n-n:case1</ta>
            <ta e="T29" id="Seg_517" s="T28">num</ta>
            <ta e="T30" id="Seg_518" s="T29">n-n:case1</ta>
            <ta e="T31" id="Seg_519" s="T30">dem</ta>
            <ta e="T32" id="Seg_520" s="T31">n-n:case2</ta>
            <ta e="T33" id="Seg_521" s="T32">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_522" s="T33">n-n:case1</ta>
            <ta e="T35" id="Seg_523" s="T34">n-n:case1</ta>
            <ta e="T36" id="Seg_524" s="T35">n-n&gt;n-n&gt;adj</ta>
            <ta e="T37" id="Seg_525" s="T36">dem</ta>
            <ta e="T38" id="Seg_526" s="T37">n-n:case2</ta>
            <ta e="T39" id="Seg_527" s="T38">n-n:case1</ta>
            <ta e="T40" id="Seg_528" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_529" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_530" s="T41">v-v:ins-v:pn</ta>
            <ta e="T43" id="Seg_531" s="T42">n-n:poss</ta>
            <ta e="T44" id="Seg_532" s="T43">dem-adj&gt;adv</ta>
            <ta e="T45" id="Seg_533" s="T44">conj</ta>
            <ta e="T46" id="Seg_534" s="T45">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_535" s="T0">adv</ta>
            <ta e="T2" id="Seg_536" s="T1">pers</ta>
            <ta e="T3" id="Seg_537" s="T2">v</ta>
            <ta e="T4" id="Seg_538" s="T3">v</ta>
            <ta e="T5" id="Seg_539" s="T4">interrog</ta>
            <ta e="T6" id="Seg_540" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_541" s="T6">v</ta>
            <ta e="T8" id="Seg_542" s="T7">pers</ta>
            <ta e="T9" id="Seg_543" s="T8">v</ta>
            <ta e="T10" id="Seg_544" s="T9">n</ta>
            <ta e="T11" id="Seg_545" s="T10">nprop</ta>
            <ta e="T12" id="Seg_546" s="T11">pers</ta>
            <ta e="T13" id="Seg_547" s="T12">v</ta>
            <ta e="T14" id="Seg_548" s="T13">num</ta>
            <ta e="T15" id="Seg_549" s="T14">n</ta>
            <ta e="T16" id="Seg_550" s="T15">pro</ta>
            <ta e="T17" id="Seg_551" s="T16">v</ta>
            <ta e="T18" id="Seg_552" s="T17">num</ta>
            <ta e="T19" id="Seg_553" s="T18">n</ta>
            <ta e="T20" id="Seg_554" s="T19">adv</ta>
            <ta e="T21" id="Seg_555" s="T20">adv</ta>
            <ta e="T22" id="Seg_556" s="T21">v</ta>
            <ta e="T23" id="Seg_557" s="T22">n</ta>
            <ta e="T24" id="Seg_558" s="T23">pro</ta>
            <ta e="T25" id="Seg_559" s="T24">n</ta>
            <ta e="T26" id="Seg_560" s="T25">v</ta>
            <ta e="T27" id="Seg_561" s="T26">v</ta>
            <ta e="T28" id="Seg_562" s="T27">n</ta>
            <ta e="T29" id="Seg_563" s="T28">num</ta>
            <ta e="T30" id="Seg_564" s="T29">n</ta>
            <ta e="T31" id="Seg_565" s="T30">pro</ta>
            <ta e="T32" id="Seg_566" s="T31">n</ta>
            <ta e="T33" id="Seg_567" s="T32">v</ta>
            <ta e="T34" id="Seg_568" s="T33">n</ta>
            <ta e="T35" id="Seg_569" s="T34">n</ta>
            <ta e="T36" id="Seg_570" s="T35">adj</ta>
            <ta e="T37" id="Seg_571" s="T36">pro</ta>
            <ta e="T38" id="Seg_572" s="T37">n</ta>
            <ta e="T39" id="Seg_573" s="T38">n</ta>
            <ta e="T40" id="Seg_574" s="T39">v</ta>
            <ta e="T41" id="Seg_575" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_576" s="T41">v</ta>
            <ta e="T43" id="Seg_577" s="T42">n</ta>
            <ta e="T44" id="Seg_578" s="T43">dem</ta>
            <ta e="T45" id="Seg_579" s="T44">conj</ta>
            <ta e="T46" id="Seg_580" s="T45">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T22" id="Seg_581" s="T21">RUS:cult</ta>
            <ta e="T23" id="Seg_582" s="T22">RUS:cult</ta>
            <ta e="T26" id="Seg_583" s="T25">RUS:core</ta>
            <ta e="T45" id="Seg_584" s="T44">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T23" id="Seg_585" s="T22">Csub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T22" id="Seg_586" s="T21">indir:infl</ta>
            <ta e="T26" id="Seg_587" s="T25">indir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T46" id="Seg_588" s="T43">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_589" s="T0">В прошлом году я ходил белковать.</ta>
            <ta e="T7" id="Seg_590" s="T4">Никого не добыл.</ta>
            <ta e="T11" id="Seg_591" s="T7">Я ходил с собакой Лапкой.</ta>
            <ta e="T15" id="Seg_592" s="T11">Я ходил два месяца.</ta>
            <ta e="T19" id="Seg_593" s="T15">Я убил пять соболей.</ta>
            <ta e="T23" id="Seg_594" s="T19">Домой принеся сдал их в лавку.</ta>
            <ta e="T30" id="Seg_595" s="T23">За соболей я получил деньги, 100 рублей.</ta>
            <ta e="T36" id="Seg_596" s="T30">На эти деньги купил ружье, ружье беcкурковое.</ta>
            <ta e="T42" id="Seg_597" s="T36">Из ружья стрелял, лося не попал.</ta>
            <ta e="T46" id="Seg_598" s="T42">Лось так и ушел.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_599" s="T0">Last year I went to hunt squirrels.</ta>
            <ta e="T7" id="Seg_600" s="T4">I have caught nothing.</ta>
            <ta e="T11" id="Seg_601" s="T7">I went there with the dog, Lapka.</ta>
            <ta e="T15" id="Seg_602" s="T11">I went [hunting] for two months.</ta>
            <ta e="T19" id="Seg_603" s="T15">I killed five sables.</ta>
            <ta e="T23" id="Seg_604" s="T19">I brought them home and handed in to the shop.</ta>
            <ta e="T30" id="Seg_605" s="T23">For these sables, I got money, 100 roubles.</ta>
            <ta e="T36" id="Seg_606" s="T30">With this money, I bought a gun, a hammerless gun.</ta>
            <ta e="T42" id="Seg_607" s="T36">With this gun I shot a moose, missed it.</ta>
            <ta e="T46" id="Seg_608" s="T42">The moose went away.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_609" s="T0">Letztes Jahr ging ich Eichhörnchen jagen.</ta>
            <ta e="T7" id="Seg_610" s="T4">Ich habe kein einziges gefangen.</ta>
            <ta e="T11" id="Seg_611" s="T7">Ich ging mit dem Hund, mit Lapka.</ta>
            <ta e="T15" id="Seg_612" s="T11">Ich ging zwei Monate lang [jagen].</ta>
            <ta e="T19" id="Seg_613" s="T15">Ich habe fünf Zobel erlegt.</ta>
            <ta e="T23" id="Seg_614" s="T19">Ich brachte sie nach Hause und gab sie im Laden ab.</ta>
            <ta e="T30" id="Seg_615" s="T23">Für die Zobel bekam ich Geld, 100 Rubel.</ta>
            <ta e="T36" id="Seg_616" s="T30">Für das Geld habe ich ein Gewehr gekauft, ein Gewehr ohne Hahn.</ta>
            <ta e="T42" id="Seg_617" s="T36">Mit dem Gewehr schoss ich auf einen Elch, es hat ihn nicht erwischt.</ta>
            <ta e="T46" id="Seg_618" s="T42">Der Elch ging weg.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_619" s="T0">в прошлом году я ходил белковать</ta>
            <ta e="T7" id="Seg_620" s="T4">никого не добыл</ta>
            <ta e="T11" id="Seg_621" s="T7">я ходил с собакой Лапкой</ta>
            <ta e="T15" id="Seg_622" s="T11">я ходил два месяца</ta>
            <ta e="T19" id="Seg_623" s="T15">я убил пять соболей</ta>
            <ta e="T23" id="Seg_624" s="T19">домой принеся сдал в лавку </ta>
            <ta e="T30" id="Seg_625" s="T23">за соболей получил взял деньги 100 целковых</ta>
            <ta e="T36" id="Seg_626" s="T30">на деньги купил ружье ружье безкурковое</ta>
            <ta e="T42" id="Seg_627" s="T36">из ружья стрелял лося не попал</ta>
            <ta e="T46" id="Seg_628" s="T42">лось так и ушел</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T19" id="Seg_629" s="T15">[WNB:] na can be also inferential particle.</ta>
            <ta e="T23" id="Seg_630" s="T19">или ′здавамксап?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
