<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDED6B7D80-1CB3-1672-FF0F-D0D29A9884D4">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\SAlAl_1965_War_nar\SAlAl_1965_War_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">76</ud-information>
            <ud-information attribute-name="# HIAT:w">64</ud-information>
            <ud-information attribute-name="# e">64</ud-information>
            <ud-information attribute-name="# HIAT:u">10</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SAlAl">
            <abbreviation>SAlAl</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SAlAl"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T64" id="Seg_0" n="sc" s="T0">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Mat</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ütälʼqätɨlʼ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">iraqɨntɨ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">1942</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">poːqontɨ</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">qəssak</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">mütoːntɨ</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">A</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">moqɨnä</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">tüːsak</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">ütälʼqätɨlʼ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">iraqɨntɨ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">1946</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">poːqontɨ</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_50" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">Mat</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">ɛːsak</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">Polʼšaqɨt</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">Germanʼqɨt</ts>
                  <nts id="Seg_62" n="HIAT:ip">,</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">a</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">Polʼšaqɨt</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">me</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_74" n="HIAT:w" s="T21">iːsɨmɨt</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_77" n="HIAT:w" s="T22">Вaršava</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_81" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_83" n="HIAT:w" s="T23">A</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_86" n="HIAT:w" s="T24">Kermanʼqɨt</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_89" n="HIAT:w" s="T25">iːsɨmɨt</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_92" n="HIAT:w" s="T26">qəːttɨ</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_95" n="HIAT:w" s="T27">Perlʼin</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_99" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">Mütoːqɨt</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">mat</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_107" n="HIAT:w" s="T30">orsä</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_110" n="HIAT:w" s="T31">kɨrɨmčʼɨsɔːtɨt</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_114" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">Palʼnicaqɨt</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_119" n="HIAT:w" s="T33">ippɨsak</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_122" n="HIAT:w" s="T34">muktɨt</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_125" n="HIAT:w" s="T35">irantɨ</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_128" n="HIAT:w" s="T36">kuntɨ</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_132" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">Kušat</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_137" n="HIAT:w" s="T38">mašıp</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_140" n="HIAT:w" s="T39">toktärɨn</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_143" n="HIAT:w" s="T40">lʼečʼiksɔːtɨt</ts>
                  <nts id="Seg_144" n="HIAT:ip">,</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_147" n="HIAT:w" s="T41">mat</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_150" n="HIAT:w" s="T42">aj</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_153" n="HIAT:w" s="T43">mütoːntɨ</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_156" n="HIAT:w" s="T44">qəssak</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_160" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_162" n="HIAT:w" s="T45">A</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_165" n="HIAT:w" s="T46">mütɨp</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_168" n="HIAT:w" s="T47">säpɨrsak</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_171" n="HIAT:w" s="T48">Germanʼqɨt</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_175" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_177" n="HIAT:w" s="T49">Kɨqot</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_180" n="HIAT:w" s="T50">Еlʼpa</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_183" n="HIAT:w" s="T51">nımtɨ</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_186" n="HIAT:w" s="T52">čʼəːtɨmpɨsɨmɨt</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_189" n="HIAT:w" s="T53">ukkɨr</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_192" n="HIAT:w" s="T54">paretɨlʼ</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_195" n="HIAT:w" s="T55">amerikanskij</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_198" n="HIAT:w" s="T56">armisä</ts>
                  <nts id="Seg_199" n="HIAT:ip">.</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_202" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_204" n="HIAT:w" s="T57">Šittɨčʼäköt</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_207" n="HIAT:w" s="T58">čʼeːloːqɨt</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_210" n="HIAT:w" s="T59">pičʼčʼalʼ</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_213" n="HIAT:w" s="T60">tɨrɨlʼ</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_216" n="HIAT:w" s="T61">iraqɨntɨ</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_219" n="HIAT:w" s="T62">1945</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_222" n="HIAT:w" s="T63">poːqɨntɨ</ts>
                  <nts id="Seg_223" n="HIAT:ip">.</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T64" id="Seg_225" n="sc" s="T0">
               <ts e="T1" id="Seg_227" n="e" s="T0">Mat </ts>
               <ts e="T2" id="Seg_229" n="e" s="T1">ütälʼqätɨlʼ </ts>
               <ts e="T3" id="Seg_231" n="e" s="T2">iraqɨntɨ </ts>
               <ts e="T4" id="Seg_233" n="e" s="T3">1942 </ts>
               <ts e="T5" id="Seg_235" n="e" s="T4">poːqontɨ </ts>
               <ts e="T6" id="Seg_237" n="e" s="T5">qəssak </ts>
               <ts e="T7" id="Seg_239" n="e" s="T6">mütoːntɨ. </ts>
               <ts e="T8" id="Seg_241" n="e" s="T7">A </ts>
               <ts e="T9" id="Seg_243" n="e" s="T8">moqɨnä </ts>
               <ts e="T10" id="Seg_245" n="e" s="T9">tüːsak </ts>
               <ts e="T11" id="Seg_247" n="e" s="T10">ütälʼqätɨlʼ </ts>
               <ts e="T12" id="Seg_249" n="e" s="T11">iraqɨntɨ </ts>
               <ts e="T13" id="Seg_251" n="e" s="T12">1946 </ts>
               <ts e="T14" id="Seg_253" n="e" s="T13">poːqontɨ. </ts>
               <ts e="T15" id="Seg_255" n="e" s="T14">Mat </ts>
               <ts e="T16" id="Seg_257" n="e" s="T15">ɛːsak </ts>
               <ts e="T17" id="Seg_259" n="e" s="T16">Polʼšaqɨt </ts>
               <ts e="T18" id="Seg_261" n="e" s="T17">Germanʼqɨt, </ts>
               <ts e="T19" id="Seg_263" n="e" s="T18">a </ts>
               <ts e="T20" id="Seg_265" n="e" s="T19">Polʼšaqɨt </ts>
               <ts e="T21" id="Seg_267" n="e" s="T20">me </ts>
               <ts e="T22" id="Seg_269" n="e" s="T21">iːsɨmɨt </ts>
               <ts e="T23" id="Seg_271" n="e" s="T22">Вaršava. </ts>
               <ts e="T24" id="Seg_273" n="e" s="T23">A </ts>
               <ts e="T25" id="Seg_275" n="e" s="T24">Kermanʼqɨt </ts>
               <ts e="T26" id="Seg_277" n="e" s="T25">iːsɨmɨt </ts>
               <ts e="T27" id="Seg_279" n="e" s="T26">qəːttɨ </ts>
               <ts e="T28" id="Seg_281" n="e" s="T27">Perlʼin. </ts>
               <ts e="T29" id="Seg_283" n="e" s="T28">Mütoːqɨt </ts>
               <ts e="T30" id="Seg_285" n="e" s="T29">mat </ts>
               <ts e="T31" id="Seg_287" n="e" s="T30">orsä </ts>
               <ts e="T32" id="Seg_289" n="e" s="T31">kɨrɨmčʼɨsɔːtɨt. </ts>
               <ts e="T33" id="Seg_291" n="e" s="T32">Palʼnicaqɨt </ts>
               <ts e="T34" id="Seg_293" n="e" s="T33">ippɨsak </ts>
               <ts e="T35" id="Seg_295" n="e" s="T34">muktɨt </ts>
               <ts e="T36" id="Seg_297" n="e" s="T35">irantɨ </ts>
               <ts e="T37" id="Seg_299" n="e" s="T36">kuntɨ. </ts>
               <ts e="T38" id="Seg_301" n="e" s="T37">Kušat </ts>
               <ts e="T39" id="Seg_303" n="e" s="T38">mašıp </ts>
               <ts e="T40" id="Seg_305" n="e" s="T39">toktärɨn </ts>
               <ts e="T41" id="Seg_307" n="e" s="T40">lʼečʼiksɔːtɨt, </ts>
               <ts e="T42" id="Seg_309" n="e" s="T41">mat </ts>
               <ts e="T43" id="Seg_311" n="e" s="T42">aj </ts>
               <ts e="T44" id="Seg_313" n="e" s="T43">mütoːntɨ </ts>
               <ts e="T45" id="Seg_315" n="e" s="T44">qəssak. </ts>
               <ts e="T46" id="Seg_317" n="e" s="T45">A </ts>
               <ts e="T47" id="Seg_319" n="e" s="T46">mütɨp </ts>
               <ts e="T48" id="Seg_321" n="e" s="T47">säpɨrsak </ts>
               <ts e="T49" id="Seg_323" n="e" s="T48">Germanʼqɨt. </ts>
               <ts e="T50" id="Seg_325" n="e" s="T49">Kɨqot </ts>
               <ts e="T51" id="Seg_327" n="e" s="T50">Еlʼpa </ts>
               <ts e="T52" id="Seg_329" n="e" s="T51">nımtɨ </ts>
               <ts e="T53" id="Seg_331" n="e" s="T52">čʼəːtɨmpɨsɨmɨt </ts>
               <ts e="T54" id="Seg_333" n="e" s="T53">ukkɨr </ts>
               <ts e="T55" id="Seg_335" n="e" s="T54">paretɨlʼ </ts>
               <ts e="T56" id="Seg_337" n="e" s="T55">amerikanskij </ts>
               <ts e="T57" id="Seg_339" n="e" s="T56">armisä. </ts>
               <ts e="T58" id="Seg_341" n="e" s="T57">Šittɨčʼäköt </ts>
               <ts e="T59" id="Seg_343" n="e" s="T58">čʼeːloːqɨt </ts>
               <ts e="T60" id="Seg_345" n="e" s="T59">pičʼčʼalʼ </ts>
               <ts e="T61" id="Seg_347" n="e" s="T60">tɨrɨlʼ </ts>
               <ts e="T62" id="Seg_349" n="e" s="T61">iraqɨntɨ </ts>
               <ts e="T63" id="Seg_351" n="e" s="T62">1945 </ts>
               <ts e="T64" id="Seg_353" n="e" s="T63">poːqɨntɨ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_354" s="T0">SAlAl_1965_War_nar.001 (001.001)</ta>
            <ta e="T14" id="Seg_355" s="T7">SAlAl_1965_War_nar.002 (001.002)</ta>
            <ta e="T23" id="Seg_356" s="T14">SAlAl_1965_War_nar.003 (001.003)</ta>
            <ta e="T28" id="Seg_357" s="T23">SAlAl_1965_War_nar.004 (001.004)</ta>
            <ta e="T32" id="Seg_358" s="T28">SAlAl_1965_War_nar.005 (001.005)</ta>
            <ta e="T37" id="Seg_359" s="T32">SAlAl_1965_War_nar.006 (001.006)</ta>
            <ta e="T45" id="Seg_360" s="T37">SAlAl_1965_War_nar.007 (001.007)</ta>
            <ta e="T49" id="Seg_361" s="T45">SAlAl_1965_War_nar.008 (001.008)</ta>
            <ta e="T57" id="Seg_362" s="T49">SAlAl_1965_War_nar.009 (001.009)</ta>
            <ta e="T64" id="Seg_363" s="T57">SAlAl_1965_War_nar.010 (001.010)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_364" s="T0">мат ӱтӓлʼkӓтыl и′раkынты 1942 поkонты ′къ̊ссак ′мӱтонтъ.</ta>
            <ta e="T14" id="Seg_365" s="T7">а ′моkонӓ ′тӱ̄сак ӱ′тӓlkӓтыl и′раkынты 1946 поɣ[k]онты.</ta>
            <ta e="T23" id="Seg_366" s="T14">мат ′е̨сак ′полʼшʼаkыт гʼер′манʼkыт, а ′полʼшʼаkыт ′ме ′ӣсымыт Варшава.</ta>
            <ta e="T28" id="Seg_367" s="T23">а кер′манʼkыт ′ӣсымыт ′kъ̊ттъ пер′лʼин.</ta>
            <ta e="T32" id="Seg_368" s="T28">′мӱ̄тоɣыт мат ′орса кы‵рымчи′сотыт.</ta>
            <ta e="T37" id="Seg_369" s="T32">палʼ′ницʼаkыт ′ӣтпысак ′муктыт ′иранты‵кунты.</ta>
            <ta e="T45" id="Seg_370" s="T37">ку′шат ′машип ′токтӓрын ′лʼечик′сотыт мат ай ′мӱтонты kы′сак.</ta>
            <ta e="T49" id="Seg_371" s="T45">а ′мӱ̄тып ′сӓпырсак г‵ер′манʼkыт.</ta>
            <ta e="T57" id="Seg_372" s="T49">′кыкот ′Елʼпа ′нимты ′чъ̊̄том′пысымыт у′ккыр па′ре̨тыl амʼери′канский ′армиса.</ta>
            <ta e="T64" id="Seg_373" s="T57">шʼи′тӓче′кыт ′че̄лоkыт пи′чалтырыl и′раɣынты 1945 поkонты.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T7" id="Seg_374" s="T0">mat ütälʼqätɨlʼ iraqɨntɨ 1942 poqontɨ kəssak mütontə.</ta>
            <ta e="T14" id="Seg_375" s="T7">a moqonä tüːsak ütälʼqätɨlʼ iraqɨntɨ 1946 poqontɨ.</ta>
            <ta e="T23" id="Seg_376" s="T14">mat esak polʼšʼaqɨt gʼermanʼqɨt, a polʼšaqɨt me iːsɨmɨt Вaršava.</ta>
            <ta e="T28" id="Seg_377" s="T23"> a kermanʼqɨt iːsɨmɨt qəttə perlʼin.</ta>
            <ta e="T32" id="Seg_378" s="T28">müːtoqɨt mat orsa kɨrɨmčʼisotɨt.</ta>
            <ta e="T37" id="Seg_379" s="T32">palʼnicʼaqɨt iːtpɨsak muktɨt irantɨkuntɨ.</ta>
            <ta e="T45" id="Seg_380" s="T37">kušat mašip toktärɨn lʼečʼiksotɨt mat aj mütontɨ qɨsak.</ta>
            <ta e="T49" id="Seg_381" s="T45">a müːtɨp säpɨrsak germanʼqɨt.</ta>
            <ta e="T57" id="Seg_382" s="T49">kɨkot Еlʼpa nimtɨ čʼəːtompɨsɨmɨt ukkɨr paretɨlʼ amʼerikanskij armisa.</ta>
            <ta e="T64" id="Seg_383" s="T57">šʼitäčʼekɨt čʼeːloqɨt pičʼaltɨrɨlʼ iraqɨntɨ 1945 poqontɨ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_384" s="T0">Mat ütälʼqätɨlʼ iraqɨntɨ 1942 poːqontɨ qəssak mütoːntɨ. </ta>
            <ta e="T14" id="Seg_385" s="T7">A moqɨnä tüːsak ütälʼqätɨlʼ iraqɨntɨ 1946 poːqontɨ. </ta>
            <ta e="T23" id="Seg_386" s="T14">Mat ɛːsak Polʼšaqɨt Germanʼqɨt, a Polʼšaqɨt me iːsɨmɨt Вaršava. </ta>
            <ta e="T28" id="Seg_387" s="T23">A Kermanʼqɨt iːsɨmɨt qəːttɨ Perlʼin. </ta>
            <ta e="T32" id="Seg_388" s="T28">Mütoːqɨt mat orsä kɨrɨmčʼɨsɔːtɨt. </ta>
            <ta e="T37" id="Seg_389" s="T32">Palʼnicaqɨt ippɨsak muktɨt irantɨ kuntɨ. </ta>
            <ta e="T45" id="Seg_390" s="T37">Kušat mašıp toktärɨn lʼečʼiksɔːtɨt, mat aj mütoːntɨ qəssak. </ta>
            <ta e="T49" id="Seg_391" s="T45">A mütɨp säpɨrsak Germanʼqɨt. </ta>
            <ta e="T57" id="Seg_392" s="T49">Kɨqot Еlʼpa nımtɨ čʼəːtɨmpɨsɨmɨt ukkɨr paretɨlʼ amerikanskij armisä. </ta>
            <ta e="T64" id="Seg_393" s="T57">Šittɨčʼäköt čʼeːloːqɨt pičʼčʼalʼ tɨrɨlʼ iraqɨntɨ 1945 poːqɨntɨ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_394" s="T0">mat</ta>
            <ta e="T2" id="Seg_395" s="T1">üt-älʼqätɨ-lʼ</ta>
            <ta e="T3" id="Seg_396" s="T2">ira-qɨn-tɨ</ta>
            <ta e="T5" id="Seg_397" s="T4">poː-qon-tɨ</ta>
            <ta e="T6" id="Seg_398" s="T5">qəs-sa-k</ta>
            <ta e="T7" id="Seg_399" s="T6">mütoː-ntɨ</ta>
            <ta e="T8" id="Seg_400" s="T7">a</ta>
            <ta e="T9" id="Seg_401" s="T8">moqɨnä</ta>
            <ta e="T10" id="Seg_402" s="T9">tüː-sa-k</ta>
            <ta e="T11" id="Seg_403" s="T10">üt-älʼqätɨ-lʼ</ta>
            <ta e="T12" id="Seg_404" s="T11">ira-qɨn-tɨ</ta>
            <ta e="T14" id="Seg_405" s="T13">poː-qon-tɨ</ta>
            <ta e="T15" id="Seg_406" s="T14">mat</ta>
            <ta e="T16" id="Seg_407" s="T15">ɛː-sa-k</ta>
            <ta e="T17" id="Seg_408" s="T16">Polʼša-qɨt</ta>
            <ta e="T18" id="Seg_409" s="T17">Germanʼ-qɨt</ta>
            <ta e="T19" id="Seg_410" s="T18">a</ta>
            <ta e="T20" id="Seg_411" s="T19">Polʼša-qɨt</ta>
            <ta e="T21" id="Seg_412" s="T20">me</ta>
            <ta e="T22" id="Seg_413" s="T21">iː-sɨ-mɨt</ta>
            <ta e="T23" id="Seg_414" s="T22">Вaršava</ta>
            <ta e="T24" id="Seg_415" s="T23">a</ta>
            <ta e="T25" id="Seg_416" s="T24">Kermanʼ-qɨt</ta>
            <ta e="T26" id="Seg_417" s="T25">iː-sɨ-mɨt</ta>
            <ta e="T27" id="Seg_418" s="T26">qəːttɨ</ta>
            <ta e="T28" id="Seg_419" s="T27">Perlʼin</ta>
            <ta e="T29" id="Seg_420" s="T28">mütoː-qɨt</ta>
            <ta e="T30" id="Seg_421" s="T29">mat</ta>
            <ta e="T31" id="Seg_422" s="T30">or-sä</ta>
            <ta e="T32" id="Seg_423" s="T31">kɨr-ɨ-m-čʼɨ-sɔː-tɨt</ta>
            <ta e="T33" id="Seg_424" s="T32">palʼnica-qɨt</ta>
            <ta e="T34" id="Seg_425" s="T33">ippɨ-sa-k</ta>
            <ta e="T35" id="Seg_426" s="T34">muktɨt</ta>
            <ta e="T36" id="Seg_427" s="T35">ira-ntɨ</ta>
            <ta e="T37" id="Seg_428" s="T36">kuntɨ</ta>
            <ta e="T38" id="Seg_429" s="T37">kušat</ta>
            <ta e="T39" id="Seg_430" s="T38">mašıp</ta>
            <ta e="T40" id="Seg_431" s="T39">toktär-ɨ-n</ta>
            <ta e="T41" id="Seg_432" s="T40">lʼečʼik-sɔː-tɨt</ta>
            <ta e="T42" id="Seg_433" s="T41">mat</ta>
            <ta e="T43" id="Seg_434" s="T42">aj</ta>
            <ta e="T44" id="Seg_435" s="T43">mütoː-ntɨ</ta>
            <ta e="T45" id="Seg_436" s="T44">qəs-sa-k</ta>
            <ta e="T46" id="Seg_437" s="T45">a</ta>
            <ta e="T47" id="Seg_438" s="T46">mütɨ-p</ta>
            <ta e="T48" id="Seg_439" s="T47">säpɨ-r-sa-k</ta>
            <ta e="T49" id="Seg_440" s="T48">Germanʼ-qɨt</ta>
            <ta e="T50" id="Seg_441" s="T49">kɨ-qot</ta>
            <ta e="T51" id="Seg_442" s="T50">Еlʼpa</ta>
            <ta e="T52" id="Seg_443" s="T51">nım-tɨ</ta>
            <ta e="T53" id="Seg_444" s="T52">čʼəːtɨ-mpɨ-sɨ-mɨt</ta>
            <ta e="T54" id="Seg_445" s="T53">ukkɨr</ta>
            <ta e="T55" id="Seg_446" s="T54">pare-tɨlʼ</ta>
            <ta e="T56" id="Seg_447" s="T55">amerikanskij</ta>
            <ta e="T57" id="Seg_448" s="T56">armi-sä</ta>
            <ta e="T58" id="Seg_449" s="T57">šittɨ-čʼä-köt</ta>
            <ta e="T59" id="Seg_450" s="T58">čʼeːloː-qɨt</ta>
            <ta e="T60" id="Seg_451" s="T59">pičʼčʼa-lʼ</ta>
            <ta e="T61" id="Seg_452" s="T60">tɨr-ɨ-lʼ</ta>
            <ta e="T62" id="Seg_453" s="T61">ira-qɨn-tɨ</ta>
            <ta e="T64" id="Seg_454" s="T63">poː-qɨn-tɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_455" s="T0">man</ta>
            <ta e="T2" id="Seg_456" s="T1">üt-älʼqätɨ-lʼ</ta>
            <ta e="T3" id="Seg_457" s="T2">irä-qɨn-ntɨ</ta>
            <ta e="T5" id="Seg_458" s="T4">poː-qɨn-ntɨ</ta>
            <ta e="T6" id="Seg_459" s="T5">qən-sɨ-k</ta>
            <ta e="T7" id="Seg_460" s="T6">mütɨ-ntɨ</ta>
            <ta e="T8" id="Seg_461" s="T7">a</ta>
            <ta e="T9" id="Seg_462" s="T8">moqɨnä</ta>
            <ta e="T10" id="Seg_463" s="T9">tü-sɨ-k</ta>
            <ta e="T11" id="Seg_464" s="T10">üt-älʼqätɨ-lʼ</ta>
            <ta e="T12" id="Seg_465" s="T11">irä-qɨn-ntɨ</ta>
            <ta e="T14" id="Seg_466" s="T13">poː-qɨn-ntɨ</ta>
            <ta e="T15" id="Seg_467" s="T14">man</ta>
            <ta e="T16" id="Seg_468" s="T15">ɛː-sɨ-k</ta>
            <ta e="T17" id="Seg_469" s="T16">Polʼša-qɨn</ta>
            <ta e="T18" id="Seg_470" s="T17">Germanʼ-qɨn</ta>
            <ta e="T19" id="Seg_471" s="T18">a</ta>
            <ta e="T20" id="Seg_472" s="T19">Polʼša-qɨn</ta>
            <ta e="T21" id="Seg_473" s="T20">meː</ta>
            <ta e="T22" id="Seg_474" s="T21">iː-sɨ-mɨt</ta>
            <ta e="T23" id="Seg_475" s="T22">Вaršava</ta>
            <ta e="T24" id="Seg_476" s="T23">a</ta>
            <ta e="T25" id="Seg_477" s="T24">Germanʼ-qɨn</ta>
            <ta e="T26" id="Seg_478" s="T25">iː-sɨ-mɨt</ta>
            <ta e="T27" id="Seg_479" s="T26">qəːttɨ</ta>
            <ta e="T28" id="Seg_480" s="T27">Perlʼin</ta>
            <ta e="T29" id="Seg_481" s="T28">mütɨ-qɨn</ta>
            <ta e="T30" id="Seg_482" s="T29">man</ta>
            <ta e="T31" id="Seg_483" s="T30">orɨm-sä</ta>
            <ta e="T32" id="Seg_484" s="T31">kɨr-ɨ-m-čʼɨ-sɨ-tɨt</ta>
            <ta e="T33" id="Seg_485" s="T32">palʼnica-qɨn</ta>
            <ta e="T34" id="Seg_486" s="T33">ippɨ-sɨ-k</ta>
            <ta e="T35" id="Seg_487" s="T34">muktɨt</ta>
            <ta e="T36" id="Seg_488" s="T35">irä-ntɨ</ta>
            <ta e="T37" id="Seg_489" s="T36">kuntɨ</ta>
            <ta e="T38" id="Seg_490" s="T37">kuššat</ta>
            <ta e="T39" id="Seg_491" s="T38">mašım</ta>
            <ta e="T40" id="Seg_492" s="T39">toktär-ɨ-t</ta>
            <ta e="T41" id="Seg_493" s="T40">lʼečʼɨt-sɨ-tɨt</ta>
            <ta e="T42" id="Seg_494" s="T41">man</ta>
            <ta e="T43" id="Seg_495" s="T42">aj</ta>
            <ta e="T44" id="Seg_496" s="T43">mütɨ-ntɨ</ta>
            <ta e="T45" id="Seg_497" s="T44">qən-sɨ-k</ta>
            <ta e="T46" id="Seg_498" s="T45">a</ta>
            <ta e="T47" id="Seg_499" s="T46">mütɨ-m</ta>
            <ta e="T48" id="Seg_500" s="T47">*səpɨ-r-sɨ-k</ta>
            <ta e="T49" id="Seg_501" s="T48">Germanʼ-qɨn</ta>
            <ta e="T50" id="Seg_502" s="T49">kɨ-qɨn</ta>
            <ta e="T51" id="Seg_503" s="T50">Еlʼpa</ta>
            <ta e="T52" id="Seg_504" s="T51">nım-tɨ</ta>
            <ta e="T53" id="Seg_505" s="T52">čʼəːtɨ-mpɨ-sɨ-mɨt</ta>
            <ta e="T54" id="Seg_506" s="T53">ukkɨr</ta>
            <ta e="T55" id="Seg_507" s="T54">pare-ntɨlʼ</ta>
            <ta e="T56" id="Seg_508" s="T55">amerikanskij</ta>
            <ta e="T57" id="Seg_509" s="T56">armi-sä</ta>
            <ta e="T58" id="Seg_510" s="T57">šittɨ-čʼäːŋka-köt</ta>
            <ta e="T59" id="Seg_511" s="T58">čʼeːlɨ-qɨn</ta>
            <ta e="T60" id="Seg_512" s="T59">pičʼčʼa-lʼ</ta>
            <ta e="T61" id="Seg_513" s="T60">tɨr-ɨ-lʼ</ta>
            <ta e="T62" id="Seg_514" s="T61">irä-qɨn-ntɨ</ta>
            <ta e="T64" id="Seg_515" s="T63">poː-qɨn-ntɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_516" s="T0">I.NOM</ta>
            <ta e="T2" id="Seg_517" s="T1">water-%%-ADJZ</ta>
            <ta e="T3" id="Seg_518" s="T2">month-LOC-OBL.3SG</ta>
            <ta e="T5" id="Seg_519" s="T4">year-LOC-OBL.3SG</ta>
            <ta e="T6" id="Seg_520" s="T5">leave-PST-1SG.S</ta>
            <ta e="T7" id="Seg_521" s="T6">war-ILL</ta>
            <ta e="T8" id="Seg_522" s="T7">and</ta>
            <ta e="T9" id="Seg_523" s="T8">home</ta>
            <ta e="T10" id="Seg_524" s="T9">come-PST-1SG.S</ta>
            <ta e="T11" id="Seg_525" s="T10">water-%%-ADJZ</ta>
            <ta e="T12" id="Seg_526" s="T11">month-LOC-OBL.3SG</ta>
            <ta e="T14" id="Seg_527" s="T13">year-LOC-OBL.3SG</ta>
            <ta e="T15" id="Seg_528" s="T14">I.NOM</ta>
            <ta e="T16" id="Seg_529" s="T15">be-PST-1SG.S</ta>
            <ta e="T17" id="Seg_530" s="T16">Poland-LOC</ta>
            <ta e="T18" id="Seg_531" s="T17">Germany-LOC</ta>
            <ta e="T19" id="Seg_532" s="T18">and</ta>
            <ta e="T20" id="Seg_533" s="T19">Poland-LOC</ta>
            <ta e="T21" id="Seg_534" s="T20">we.PL.NOM</ta>
            <ta e="T22" id="Seg_535" s="T21">take-PST-1PL</ta>
            <ta e="T23" id="Seg_536" s="T22">Warsaw.[NOM]</ta>
            <ta e="T24" id="Seg_537" s="T23">but</ta>
            <ta e="T25" id="Seg_538" s="T24">Germany-LOC</ta>
            <ta e="T26" id="Seg_539" s="T25">take-PST-1PL</ta>
            <ta e="T27" id="Seg_540" s="T26">town.[NOM]</ta>
            <ta e="T28" id="Seg_541" s="T27">Berlin.[NOM]</ta>
            <ta e="T29" id="Seg_542" s="T28">war-LOC</ta>
            <ta e="T30" id="Seg_543" s="T29">I.NOM</ta>
            <ta e="T31" id="Seg_544" s="T30">force-INSTR</ta>
            <ta e="T32" id="Seg_545" s="T31">wound-EP-TRL-RFL-PST-3PL</ta>
            <ta e="T33" id="Seg_546" s="T32">hospital-LOC</ta>
            <ta e="T34" id="Seg_547" s="T33">lie-PST-1SG.S</ta>
            <ta e="T35" id="Seg_548" s="T34">six</ta>
            <ta e="T36" id="Seg_549" s="T35">month-ILL</ta>
            <ta e="T37" id="Seg_550" s="T36">during</ta>
            <ta e="T38" id="Seg_551" s="T37">when</ta>
            <ta e="T39" id="Seg_552" s="T38">I.ACC</ta>
            <ta e="T40" id="Seg_553" s="T39">doctor-EP-PL.[NOM]</ta>
            <ta e="T41" id="Seg_554" s="T40">cure-PST-3PL</ta>
            <ta e="T42" id="Seg_555" s="T41">I.NOM</ta>
            <ta e="T43" id="Seg_556" s="T42">again</ta>
            <ta e="T44" id="Seg_557" s="T43">war-ILL</ta>
            <ta e="T45" id="Seg_558" s="T44">leave-PST-1SG.S</ta>
            <ta e="T46" id="Seg_559" s="T45">and</ta>
            <ta e="T47" id="Seg_560" s="T46">war-ACC</ta>
            <ta e="T48" id="Seg_561" s="T47">break-FRQ-PST-1SG.S</ta>
            <ta e="T49" id="Seg_562" s="T48">Germany-LOC</ta>
            <ta e="T50" id="Seg_563" s="T49">river-LOC</ta>
            <ta e="T51" id="Seg_564" s="T50">the.Elbe.[NOM]</ta>
            <ta e="T52" id="Seg_565" s="T51">name.[NOM]-3SG</ta>
            <ta e="T53" id="Seg_566" s="T52">meet-HAB-PST-1PL</ta>
            <ta e="T54" id="Seg_567" s="T53">one</ta>
            <ta e="T55" id="Seg_568" s="T54">%%-PTCP.PRS</ta>
            <ta e="T56" id="Seg_569" s="T55">American</ta>
            <ta e="T57" id="Seg_570" s="T56">army-INSTR</ta>
            <ta e="T58" id="Seg_571" s="T57">two-NEG-ten</ta>
            <ta e="T59" id="Seg_572" s="T58">day-LOC</ta>
            <ta e="T60" id="Seg_573" s="T59">pike-ADJZ</ta>
            <ta e="T61" id="Seg_574" s="T60">caviare-EP-ADJZ</ta>
            <ta e="T62" id="Seg_575" s="T61">month-LOC-OBL.3SG</ta>
            <ta e="T64" id="Seg_576" s="T63">year-LOC-OBL.3SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_577" s="T0">я.NOM</ta>
            <ta e="T2" id="Seg_578" s="T1">вода-%%-ADJZ</ta>
            <ta e="T3" id="Seg_579" s="T2">месяц-LOC-OBL.3SG</ta>
            <ta e="T5" id="Seg_580" s="T4">год-LOC-OBL.3SG</ta>
            <ta e="T6" id="Seg_581" s="T5">отправиться-PST-1SG.S</ta>
            <ta e="T7" id="Seg_582" s="T6">война-ILL</ta>
            <ta e="T8" id="Seg_583" s="T7">а</ta>
            <ta e="T9" id="Seg_584" s="T8">домой</ta>
            <ta e="T10" id="Seg_585" s="T9">прийти-PST-1SG.S</ta>
            <ta e="T11" id="Seg_586" s="T10">вода-%%-ADJZ</ta>
            <ta e="T12" id="Seg_587" s="T11">месяц-LOC-OBL.3SG</ta>
            <ta e="T14" id="Seg_588" s="T13">год-LOC-OBL.3SG</ta>
            <ta e="T15" id="Seg_589" s="T14">я.NOM</ta>
            <ta e="T16" id="Seg_590" s="T15">быть-PST-1SG.S</ta>
            <ta e="T17" id="Seg_591" s="T16">Польша-LOC</ta>
            <ta e="T18" id="Seg_592" s="T17">Германия-LOC</ta>
            <ta e="T19" id="Seg_593" s="T18">а</ta>
            <ta e="T20" id="Seg_594" s="T19">Польша-LOC</ta>
            <ta e="T21" id="Seg_595" s="T20">мы.PL.NOM</ta>
            <ta e="T22" id="Seg_596" s="T21">взять-PST-1PL</ta>
            <ta e="T23" id="Seg_597" s="T22">Варшава.[NOM]</ta>
            <ta e="T24" id="Seg_598" s="T23">а</ta>
            <ta e="T25" id="Seg_599" s="T24">Германия-LOC</ta>
            <ta e="T26" id="Seg_600" s="T25">взять-PST-1PL</ta>
            <ta e="T27" id="Seg_601" s="T26">город.[NOM]</ta>
            <ta e="T28" id="Seg_602" s="T27">Берлин.[NOM]</ta>
            <ta e="T29" id="Seg_603" s="T28">война-LOC</ta>
            <ta e="T30" id="Seg_604" s="T29">я.NOM</ta>
            <ta e="T31" id="Seg_605" s="T30">сила-INSTR</ta>
            <ta e="T32" id="Seg_606" s="T31">рана-EP-TRL-RFL-PST-3PL</ta>
            <ta e="T33" id="Seg_607" s="T32">больница-LOC</ta>
            <ta e="T34" id="Seg_608" s="T33">лежать-PST-1SG.S</ta>
            <ta e="T35" id="Seg_609" s="T34">шесть</ta>
            <ta e="T36" id="Seg_610" s="T35">месяц-ILL</ta>
            <ta e="T37" id="Seg_611" s="T36">в.течение</ta>
            <ta e="T38" id="Seg_612" s="T37">когда</ta>
            <ta e="T39" id="Seg_613" s="T38">я.ACC</ta>
            <ta e="T40" id="Seg_614" s="T39">доктор-EP-PL.[NOM]</ta>
            <ta e="T41" id="Seg_615" s="T40">вылечить-PST-3PL</ta>
            <ta e="T42" id="Seg_616" s="T41">я.NOM</ta>
            <ta e="T43" id="Seg_617" s="T42">опять</ta>
            <ta e="T44" id="Seg_618" s="T43">война-ILL</ta>
            <ta e="T45" id="Seg_619" s="T44">отправиться-PST-1SG.S</ta>
            <ta e="T46" id="Seg_620" s="T45">а</ta>
            <ta e="T47" id="Seg_621" s="T46">война-ACC</ta>
            <ta e="T48" id="Seg_622" s="T47">ломать-FRQ-PST-1SG.S</ta>
            <ta e="T49" id="Seg_623" s="T48">Германия-LOC</ta>
            <ta e="T50" id="Seg_624" s="T49">река-LOC</ta>
            <ta e="T51" id="Seg_625" s="T50">Эльба.[NOM]</ta>
            <ta e="T52" id="Seg_626" s="T51">имя.[NOM]-3SG</ta>
            <ta e="T53" id="Seg_627" s="T52">встретить-HAB-PST-1PL</ta>
            <ta e="T54" id="Seg_628" s="T53">один</ta>
            <ta e="T55" id="Seg_629" s="T54">%%-PTCP.PRS</ta>
            <ta e="T56" id="Seg_630" s="T55">американский</ta>
            <ta e="T57" id="Seg_631" s="T56">армия-INSTR</ta>
            <ta e="T58" id="Seg_632" s="T57">два-NEG-десять</ta>
            <ta e="T59" id="Seg_633" s="T58">день-LOC</ta>
            <ta e="T60" id="Seg_634" s="T59">щука-ADJZ</ta>
            <ta e="T61" id="Seg_635" s="T60">икра-EP-ADJZ</ta>
            <ta e="T62" id="Seg_636" s="T61">месяц-LOC-OBL.3SG</ta>
            <ta e="T64" id="Seg_637" s="T63">год-LOC-OBL.3SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_638" s="T0">pers</ta>
            <ta e="T2" id="Seg_639" s="T1">n-any-n&gt;adj</ta>
            <ta e="T3" id="Seg_640" s="T2">n-n:case2-n:obl.poss</ta>
            <ta e="T5" id="Seg_641" s="T4">n-n:case2-n:obl.poss</ta>
            <ta e="T6" id="Seg_642" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_643" s="T6">n-n:case3</ta>
            <ta e="T8" id="Seg_644" s="T7">conj</ta>
            <ta e="T9" id="Seg_645" s="T8">adv</ta>
            <ta e="T10" id="Seg_646" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_647" s="T10">n-any-n&gt;adj</ta>
            <ta e="T12" id="Seg_648" s="T11">n-n:case2-n:obl.poss</ta>
            <ta e="T14" id="Seg_649" s="T13">n-n:case2-n:obl.poss</ta>
            <ta e="T15" id="Seg_650" s="T14">pers</ta>
            <ta e="T16" id="Seg_651" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_652" s="T16">nprop-n:case3</ta>
            <ta e="T18" id="Seg_653" s="T17">nprop-n:case3</ta>
            <ta e="T19" id="Seg_654" s="T18">conj</ta>
            <ta e="T20" id="Seg_655" s="T19">nprop-n:case3</ta>
            <ta e="T21" id="Seg_656" s="T20">pers</ta>
            <ta e="T22" id="Seg_657" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_658" s="T22">nprop-n:case3</ta>
            <ta e="T24" id="Seg_659" s="T23">conj</ta>
            <ta e="T25" id="Seg_660" s="T24">nprop-n:case3</ta>
            <ta e="T26" id="Seg_661" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_662" s="T26">n-n:case3</ta>
            <ta e="T28" id="Seg_663" s="T27">nprop-n:case3</ta>
            <ta e="T29" id="Seg_664" s="T28">n-n:case3</ta>
            <ta e="T30" id="Seg_665" s="T29">pers</ta>
            <ta e="T31" id="Seg_666" s="T30">n-n:case3</ta>
            <ta e="T32" id="Seg_667" s="T31">n-n:(ins)-n&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_668" s="T32">n-n:case3</ta>
            <ta e="T34" id="Seg_669" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_670" s="T34">num</ta>
            <ta e="T36" id="Seg_671" s="T35">n-n:case3</ta>
            <ta e="T37" id="Seg_672" s="T36">pp</ta>
            <ta e="T38" id="Seg_673" s="T37">interrog</ta>
            <ta e="T39" id="Seg_674" s="T38">pers</ta>
            <ta e="T40" id="Seg_675" s="T39">n-n:ins-n:num-n:case3</ta>
            <ta e="T41" id="Seg_676" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_677" s="T41">pers</ta>
            <ta e="T43" id="Seg_678" s="T42">adv</ta>
            <ta e="T44" id="Seg_679" s="T43">n-n:case3</ta>
            <ta e="T45" id="Seg_680" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_681" s="T45">conj</ta>
            <ta e="T47" id="Seg_682" s="T46">n-n:case3</ta>
            <ta e="T48" id="Seg_683" s="T47">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_684" s="T48">nprop-n:case3</ta>
            <ta e="T50" id="Seg_685" s="T49">n-n:case3</ta>
            <ta e="T51" id="Seg_686" s="T50">nprop-n:case3</ta>
            <ta e="T52" id="Seg_687" s="T51">n-n:case1-n:poss</ta>
            <ta e="T53" id="Seg_688" s="T52">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_689" s="T53">num</ta>
            <ta e="T55" id="Seg_690" s="T54">v-v&gt;ptcp</ta>
            <ta e="T56" id="Seg_691" s="T55">adj</ta>
            <ta e="T57" id="Seg_692" s="T56">n-n:case3</ta>
            <ta e="T58" id="Seg_693" s="T57">num-ptcl-num</ta>
            <ta e="T59" id="Seg_694" s="T58">n-n:case3</ta>
            <ta e="T60" id="Seg_695" s="T59">n-n&gt;adj</ta>
            <ta e="T61" id="Seg_696" s="T60">n-n:(ins)-n&gt;adj</ta>
            <ta e="T62" id="Seg_697" s="T61">n-n:case2-n:obl.poss</ta>
            <ta e="T64" id="Seg_698" s="T63">n-n:case2-n:obl.poss</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_699" s="T0">pers</ta>
            <ta e="T2" id="Seg_700" s="T1">n</ta>
            <ta e="T3" id="Seg_701" s="T2">n</ta>
            <ta e="T5" id="Seg_702" s="T4">n</ta>
            <ta e="T6" id="Seg_703" s="T5">v</ta>
            <ta e="T7" id="Seg_704" s="T6">n</ta>
            <ta e="T8" id="Seg_705" s="T7">conj</ta>
            <ta e="T9" id="Seg_706" s="T8">adv</ta>
            <ta e="T10" id="Seg_707" s="T9">v</ta>
            <ta e="T11" id="Seg_708" s="T10">n</ta>
            <ta e="T12" id="Seg_709" s="T11">n</ta>
            <ta e="T14" id="Seg_710" s="T13">n</ta>
            <ta e="T15" id="Seg_711" s="T14">pers</ta>
            <ta e="T16" id="Seg_712" s="T15">v</ta>
            <ta e="T17" id="Seg_713" s="T16">nprop</ta>
            <ta e="T18" id="Seg_714" s="T17">nprop</ta>
            <ta e="T19" id="Seg_715" s="T18">conj</ta>
            <ta e="T20" id="Seg_716" s="T19">nprop</ta>
            <ta e="T21" id="Seg_717" s="T20">pers</ta>
            <ta e="T22" id="Seg_718" s="T21">v</ta>
            <ta e="T23" id="Seg_719" s="T22">nprop</ta>
            <ta e="T24" id="Seg_720" s="T23">conj</ta>
            <ta e="T25" id="Seg_721" s="T24">nprop</ta>
            <ta e="T26" id="Seg_722" s="T25">v</ta>
            <ta e="T27" id="Seg_723" s="T26">n</ta>
            <ta e="T28" id="Seg_724" s="T27">nprop</ta>
            <ta e="T29" id="Seg_725" s="T28">n</ta>
            <ta e="T30" id="Seg_726" s="T29">pers</ta>
            <ta e="T31" id="Seg_727" s="T30">n</ta>
            <ta e="T32" id="Seg_728" s="T31">v</ta>
            <ta e="T33" id="Seg_729" s="T32">n</ta>
            <ta e="T34" id="Seg_730" s="T33">v</ta>
            <ta e="T35" id="Seg_731" s="T34">num</ta>
            <ta e="T36" id="Seg_732" s="T35">n</ta>
            <ta e="T37" id="Seg_733" s="T36">pp</ta>
            <ta e="T38" id="Seg_734" s="T37">interrog</ta>
            <ta e="T39" id="Seg_735" s="T38">pers</ta>
            <ta e="T40" id="Seg_736" s="T39">n</ta>
            <ta e="T41" id="Seg_737" s="T40">v</ta>
            <ta e="T42" id="Seg_738" s="T41">pers</ta>
            <ta e="T43" id="Seg_739" s="T42">adv</ta>
            <ta e="T44" id="Seg_740" s="T43">n</ta>
            <ta e="T45" id="Seg_741" s="T44">v</ta>
            <ta e="T46" id="Seg_742" s="T45">conj</ta>
            <ta e="T47" id="Seg_743" s="T46">n</ta>
            <ta e="T48" id="Seg_744" s="T47">v</ta>
            <ta e="T49" id="Seg_745" s="T48">nprop</ta>
            <ta e="T50" id="Seg_746" s="T49">n</ta>
            <ta e="T51" id="Seg_747" s="T50">nprop</ta>
            <ta e="T52" id="Seg_748" s="T51">n</ta>
            <ta e="T53" id="Seg_749" s="T52">v</ta>
            <ta e="T54" id="Seg_750" s="T53">num</ta>
            <ta e="T55" id="Seg_751" s="T54">ptcp</ta>
            <ta e="T56" id="Seg_752" s="T55">adj</ta>
            <ta e="T57" id="Seg_753" s="T56">n</ta>
            <ta e="T58" id="Seg_754" s="T57">num</ta>
            <ta e="T59" id="Seg_755" s="T58">n</ta>
            <ta e="T60" id="Seg_756" s="T59">adj</ta>
            <ta e="T61" id="Seg_757" s="T60">adj</ta>
            <ta e="T62" id="Seg_758" s="T61">n</ta>
            <ta e="T64" id="Seg_759" s="T63">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_760" s="T0">pro.h:A</ta>
            <ta e="T3" id="Seg_761" s="T2">np:Time</ta>
            <ta e="T5" id="Seg_762" s="T4">np:Time</ta>
            <ta e="T7" id="Seg_763" s="T6">np:G</ta>
            <ta e="T9" id="Seg_764" s="T8">adv:G</ta>
            <ta e="T10" id="Seg_765" s="T9">0.1.h:A</ta>
            <ta e="T12" id="Seg_766" s="T11">np:Time</ta>
            <ta e="T14" id="Seg_767" s="T13">np:Time</ta>
            <ta e="T15" id="Seg_768" s="T14">pro.h:Th</ta>
            <ta e="T17" id="Seg_769" s="T16">np:L</ta>
            <ta e="T18" id="Seg_770" s="T17">np:L</ta>
            <ta e="T20" id="Seg_771" s="T19">np:L</ta>
            <ta e="T21" id="Seg_772" s="T20">pro.h:A</ta>
            <ta e="T23" id="Seg_773" s="T22">np:Th</ta>
            <ta e="T25" id="Seg_774" s="T24">np:L</ta>
            <ta e="T26" id="Seg_775" s="T25">0.1.h:A</ta>
            <ta e="T28" id="Seg_776" s="T27">np:Th</ta>
            <ta e="T29" id="Seg_777" s="T28">np:L</ta>
            <ta e="T30" id="Seg_778" s="T29">pro.h:P</ta>
            <ta e="T31" id="Seg_779" s="T30">np:Ins</ta>
            <ta e="T33" id="Seg_780" s="T32">np:L</ta>
            <ta e="T34" id="Seg_781" s="T33">0.1.h:Th</ta>
            <ta e="T36" id="Seg_782" s="T35">np:Time</ta>
            <ta e="T39" id="Seg_783" s="T38">pro.h:P</ta>
            <ta e="T40" id="Seg_784" s="T39">np.h:A</ta>
            <ta e="T42" id="Seg_785" s="T41">pro.h:A</ta>
            <ta e="T44" id="Seg_786" s="T43">np:G</ta>
            <ta e="T47" id="Seg_787" s="T46">np:Th</ta>
            <ta e="T48" id="Seg_788" s="T47">0.1.h:A</ta>
            <ta e="T49" id="Seg_789" s="T48">np:L</ta>
            <ta e="T50" id="Seg_790" s="T49">np:L</ta>
            <ta e="T53" id="Seg_791" s="T52">0.1.h:A</ta>
            <ta e="T57" id="Seg_792" s="T56">np:Com</ta>
            <ta e="T59" id="Seg_793" s="T58">np:Time</ta>
            <ta e="T62" id="Seg_794" s="T61">np:Time</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_795" s="T0">pro.h:S</ta>
            <ta e="T6" id="Seg_796" s="T5">v:pred</ta>
            <ta e="T10" id="Seg_797" s="T9">0.1.h:S v:pred</ta>
            <ta e="T15" id="Seg_798" s="T14">pro.h:S</ta>
            <ta e="T16" id="Seg_799" s="T15">v:pred</ta>
            <ta e="T21" id="Seg_800" s="T20">pro.h:S</ta>
            <ta e="T22" id="Seg_801" s="T21">v:pred</ta>
            <ta e="T23" id="Seg_802" s="T22">np:O</ta>
            <ta e="T26" id="Seg_803" s="T25">0.1.h:S v:pred</ta>
            <ta e="T28" id="Seg_804" s="T27">np:O</ta>
            <ta e="T30" id="Seg_805" s="T29">pro.h:S</ta>
            <ta e="T32" id="Seg_806" s="T31">v:pred</ta>
            <ta e="T34" id="Seg_807" s="T33">0.1.h:S v:pred</ta>
            <ta e="T39" id="Seg_808" s="T38">pro.h:O</ta>
            <ta e="T40" id="Seg_809" s="T39">np.h:S</ta>
            <ta e="T41" id="Seg_810" s="T40">v:pred</ta>
            <ta e="T42" id="Seg_811" s="T41">pro.h:S</ta>
            <ta e="T45" id="Seg_812" s="T44">v:pred</ta>
            <ta e="T47" id="Seg_813" s="T46">np:O</ta>
            <ta e="T48" id="Seg_814" s="T47">0.1.h:S v:pred</ta>
            <ta e="T53" id="Seg_815" s="T52">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_816" s="T0">new</ta>
            <ta e="T3" id="Seg_817" s="T2">accs-gen</ta>
            <ta e="T5" id="Seg_818" s="T4">accs-gen</ta>
            <ta e="T10" id="Seg_819" s="T9">0.giv-active</ta>
            <ta e="T12" id="Seg_820" s="T11">accs-gen</ta>
            <ta e="T14" id="Seg_821" s="T13">accs-gen</ta>
            <ta e="T15" id="Seg_822" s="T14">giv-active</ta>
            <ta e="T17" id="Seg_823" s="T16">accs-gen</ta>
            <ta e="T18" id="Seg_824" s="T17">accs-gen</ta>
            <ta e="T20" id="Seg_825" s="T19">accs-gen</ta>
            <ta e="T21" id="Seg_826" s="T20">accs-aggr</ta>
            <ta e="T23" id="Seg_827" s="T22">accs-gen</ta>
            <ta e="T25" id="Seg_828" s="T24">accs-gen</ta>
            <ta e="T26" id="Seg_829" s="T25">0.giv-active</ta>
            <ta e="T28" id="Seg_830" s="T27">accs-gen</ta>
            <ta e="T29" id="Seg_831" s="T28">giv-inactive</ta>
            <ta e="T30" id="Seg_832" s="T29">giv-active</ta>
            <ta e="T33" id="Seg_833" s="T32">accs-sit</ta>
            <ta e="T34" id="Seg_834" s="T33">0.giv-active</ta>
            <ta e="T36" id="Seg_835" s="T35">accs-gen</ta>
            <ta e="T39" id="Seg_836" s="T38">giv-active</ta>
            <ta e="T40" id="Seg_837" s="T39">accs-sit</ta>
            <ta e="T42" id="Seg_838" s="T41">giv-active</ta>
            <ta e="T44" id="Seg_839" s="T43">accs-gen</ta>
            <ta e="T47" id="Seg_840" s="T46">accs-gen</ta>
            <ta e="T48" id="Seg_841" s="T47">0.giv-active</ta>
            <ta e="T49" id="Seg_842" s="T48">giv-inactive</ta>
            <ta e="T50" id="Seg_843" s="T49">accs-gen</ta>
            <ta e="T51" id="Seg_844" s="T50">accs-gen</ta>
            <ta e="T52" id="Seg_845" s="T51">accs-gen</ta>
            <ta e="T53" id="Seg_846" s="T52">0.giv-inactive</ta>
            <ta e="T57" id="Seg_847" s="T56">new</ta>
            <ta e="T59" id="Seg_848" s="T58">accs-gen</ta>
            <ta e="T62" id="Seg_849" s="T61">accs-gen</ta>
            <ta e="T64" id="Seg_850" s="T63">accs-gen</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_851" s="T7">RUS:gram</ta>
            <ta e="T19" id="Seg_852" s="T18">RUS:gram</ta>
            <ta e="T24" id="Seg_853" s="T23">RUS:gram</ta>
            <ta e="T33" id="Seg_854" s="T32">RUS:cult</ta>
            <ta e="T40" id="Seg_855" s="T39">RUS:cult</ta>
            <ta e="T41" id="Seg_856" s="T40">RUS:core</ta>
            <ta e="T46" id="Seg_857" s="T45">RUS:gram</ta>
            <ta e="T56" id="Seg_858" s="T55">RUS:cult</ta>
            <ta e="T57" id="Seg_859" s="T56">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_860" s="T0">Я в июне 1942 года ушел на войну.</ta>
            <ta e="T14" id="Seg_861" s="T7">А вернулся в июне 1946 года.</ta>
            <ta e="T23" id="Seg_862" s="T14">Я бывал в Польше, в Германии, а в Польше мы взяли Варшаву.</ta>
            <ta e="T28" id="Seg_863" s="T23">А в Германии [мы] взяли город Берлин.</ta>
            <ta e="T32" id="Seg_864" s="T28">На войне я был тяжело ранен.</ta>
            <ta e="T37" id="Seg_865" s="T32">В больнице лежал 6 месяцев.</ta>
            <ta e="T45" id="Seg_866" s="T37">Когда меня врачи вылечили, я опять на войну пошел.</ta>
            <ta e="T49" id="Seg_867" s="T45">А войну кончал в Германии.</ta>
            <ta e="T57" id="Seg_868" s="T49">На реке по имени Эльба повстречались с (первой?) американской армией.</ta>
            <ta e="T64" id="Seg_869" s="T57">Восьмого мая 1945 года.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_870" s="T0">In June 1942 I went to war.</ta>
            <ta e="T14" id="Seg_871" s="T7">And [I] came back in June 1946.</ta>
            <ta e="T23" id="Seg_872" s="T14">I have been to Poland, to Germany and in Poland we took Warsaw.</ta>
            <ta e="T28" id="Seg_873" s="T23">And in Germany we took Berlin.</ta>
            <ta e="T32" id="Seg_874" s="T28">I got seriously wounded in the war.</ta>
            <ta e="T37" id="Seg_875" s="T32">I was in the hospital for 6 months.</ta>
            <ta e="T45" id="Seg_876" s="T37">When the doctors had cured me, I went to war again.</ta>
            <ta e="T49" id="Seg_877" s="T45">And I finished the war in Germany.</ta>
            <ta e="T57" id="Seg_878" s="T49">On the river called Elbe, we met the (first?) American Army.</ta>
            <ta e="T64" id="Seg_879" s="T57">On the eighth of May 1945.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_880" s="T0">Im Juni 1942 zog ich in den Krieg. </ta>
            <ta e="T14" id="Seg_881" s="T7">Und [ich] kam im Juni 1946 zurück. </ta>
            <ta e="T23" id="Seg_882" s="T14">Ich war in Polen, in Deutschland, und in Polen nahmen wie Warschau. </ta>
            <ta e="T28" id="Seg_883" s="T23">Und in Deutschland nahmen wir Berlin. </ta>
            <ta e="T32" id="Seg_884" s="T28">Ich wurde schwer verletzt im Krieg. </ta>
            <ta e="T37" id="Seg_885" s="T32">Ich lag sechs Monate lang im Krankenhaus. </ta>
            <ta e="T45" id="Seg_886" s="T37">Als die Ärzte mich geheilt hatten, zog ich wieder in den Krieg. </ta>
            <ta e="T49" id="Seg_887" s="T45">Und ich beendete den Krieg[sdienst] in Deutschland. </ta>
            <ta e="T57" id="Seg_888" s="T49">Am Fluss namens Elbe trafen wir auf die (erste?) amerikanischen Armee. </ta>
            <ta e="T64" id="Seg_889" s="T57">Am achten Mai 1945. </ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_890" s="T0">я в июне месяце в 1942 году ушел на войну</ta>
            <ta e="T14" id="Seg_891" s="T7">вернулся в июне месяце 1946 года</ta>
            <ta e="T23" id="Seg_892" s="T14">я бывал в Польше Германии а в Польше мы взяли город Варшаву</ta>
            <ta e="T28" id="Seg_893" s="T23">а в Германии взяли город Берлин</ta>
            <ta e="T32" id="Seg_894" s="T28">В войне я (был) сильно [тяжело] ранен</ta>
            <ta e="T37" id="Seg_895" s="T32">в больнице лежал 6 месяцев.</ta>
            <ta e="T45" id="Seg_896" s="T37">когда меня врачи вылечили я опять на войну пошел</ta>
            <ta e="T49" id="Seg_897" s="T45">а войну кончал в Германии</ta>
            <ta e="T57" id="Seg_898" s="T49">на реке Эльба по имени повстречались с первой американской армией.</ta>
            <ta e="T64" id="Seg_899" s="T57">8 числа май месяц (потты – год)</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
