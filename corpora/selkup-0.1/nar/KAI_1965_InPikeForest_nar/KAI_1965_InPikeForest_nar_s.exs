<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID3EA060B6-F237-667D-7434-E55C3A464B2F">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\KAI_1965_InPikeForest_nar\KAI_1965_InPikeForest_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">30</ud-information>
            <ud-information attribute-name="# HIAT:w">24</ud-information>
            <ud-information attribute-name="# e">24</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KAI">
            <abbreviation>KAI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KAI"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T275" id="Seg_0" n="sc" s="T251">
               <ts e="T255" id="Seg_2" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_4" n="HIAT:w" s="T251">Pičʼelʼ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_7" n="HIAT:w" s="T252">Mačʼoːqɨt</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_10" n="HIAT:w" s="T253">nɨmtɨ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_13" n="HIAT:w" s="T254">ilʼesɨmɨt</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_17" n="HIAT:u" s="T255">
                  <ts e="T256" id="Seg_19" n="HIAT:w" s="T255">Ilʼtät</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_22" n="HIAT:w" s="T256">čʼontoːqɨt</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_25" n="HIAT:w" s="T257">ukkur</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_28" n="HIAT:w" s="T258">čʼontot</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_31" n="HIAT:w" s="T259">mompa</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_34" n="HIAT:w" s="T260">sovetskij</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_37" n="HIAT:w" s="T261">vlastʼ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_40" n="HIAT:w" s="T262">načʼalʼnik</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T269" id="Seg_44" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_46" n="HIAT:w" s="T263">Čʼap</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_49" n="HIAT:w" s="T264">tomnɔːtɨt</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_52" n="HIAT:w" s="T265">täpɨt</ts>
                  <nts id="Seg_53" n="HIAT:ip">:</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_56" n="HIAT:w" s="T266">Qapıje</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_59" n="HIAT:w" s="T267">qumɨt</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_62" n="HIAT:w" s="T268">koraŋɔːtɨt</ts>
                  <nts id="Seg_63" n="HIAT:ip">.</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T273" id="Seg_66" n="HIAT:u" s="T269">
                  <ts e="T270" id="Seg_68" n="HIAT:w" s="T269">Nado</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_71" n="HIAT:w" s="T270">šentɨ</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_74" n="HIAT:w" s="T271">zakonmɨt</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_77" n="HIAT:w" s="T272">iloqo</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_81" n="HIAT:u" s="T273">
                  <ts e="T274" id="Seg_83" n="HIAT:w" s="T273">Nılʼ</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_86" n="HIAT:w" s="T274">tomnɔːtɨt</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T275" id="Seg_89" n="sc" s="T251">
               <ts e="T252" id="Seg_91" n="e" s="T251">Pičʼelʼ </ts>
               <ts e="T253" id="Seg_93" n="e" s="T252">Mačʼoːqɨt </ts>
               <ts e="T254" id="Seg_95" n="e" s="T253">nɨmtɨ </ts>
               <ts e="T255" id="Seg_97" n="e" s="T254">ilʼesɨmɨt. </ts>
               <ts e="T256" id="Seg_99" n="e" s="T255">Ilʼtät </ts>
               <ts e="T257" id="Seg_101" n="e" s="T256">čʼontoːqɨt </ts>
               <ts e="T258" id="Seg_103" n="e" s="T257">ukkur </ts>
               <ts e="T259" id="Seg_105" n="e" s="T258">čʼontot </ts>
               <ts e="T260" id="Seg_107" n="e" s="T259">mompa </ts>
               <ts e="T261" id="Seg_109" n="e" s="T260">sovetskij </ts>
               <ts e="T262" id="Seg_111" n="e" s="T261">vlastʼ </ts>
               <ts e="T263" id="Seg_113" n="e" s="T262">načʼalʼnik. </ts>
               <ts e="T264" id="Seg_115" n="e" s="T263">Čʼap </ts>
               <ts e="T265" id="Seg_117" n="e" s="T264">tomnɔːtɨt </ts>
               <ts e="T266" id="Seg_119" n="e" s="T265">täpɨt: </ts>
               <ts e="T267" id="Seg_121" n="e" s="T266">Qapıje </ts>
               <ts e="T268" id="Seg_123" n="e" s="T267">qumɨt </ts>
               <ts e="T269" id="Seg_125" n="e" s="T268">koraŋɔːtɨt. </ts>
               <ts e="T270" id="Seg_127" n="e" s="T269">Nado </ts>
               <ts e="T271" id="Seg_129" n="e" s="T270">šentɨ </ts>
               <ts e="T272" id="Seg_131" n="e" s="T271">zakonmɨt </ts>
               <ts e="T273" id="Seg_133" n="e" s="T272">iloqo. </ts>
               <ts e="T274" id="Seg_135" n="e" s="T273">Nılʼ </ts>
               <ts e="T275" id="Seg_137" n="e" s="T274">tomnɔːtɨt. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T255" id="Seg_138" s="T251">KAI_1965_InPikeForest_nar.001 (001.001)</ta>
            <ta e="T263" id="Seg_139" s="T255">KAI_1965_InPikeForest_nar.002 (001.002)</ta>
            <ta e="T269" id="Seg_140" s="T263">KAI_1965_InPikeForest_nar.003 (001.003)</ta>
            <ta e="T273" id="Seg_141" s="T269">KAI_1965_InPikeForest_nar.004 (001.004)</ta>
            <ta e="T275" id="Seg_142" s="T273">KAI_1965_InPikeForest_nar.005 (001.005)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T255" id="Seg_143" s="T251">′пи′челʼ ма′чоkыт нымтӓ[ы] илʼлʼесымыт.</ta>
            <ta e="T263" id="Seg_144" s="T255">′илʼтетчон′тоkыт уккур чон′тот ′момпа советский власть началʼник.</ta>
            <ta e="T269" id="Seg_145" s="T263">чап′томнотыт тӓпыт. kа′пиjе kумыт kораңотыт.</ta>
            <ta e="T273" id="Seg_146" s="T269">надо шʼенты ′законмыт иllоkо.</ta>
            <ta e="T275" id="Seg_147" s="T273">ниl томнотыт.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T255" id="Seg_148" s="T251">pičelʼ mačoqɨt nɨmtä[ɨ] ilʼlʼesɨmɨt.</ta>
            <ta e="T263" id="Seg_149" s="T255">ilʼtetčontoqɨt ukkur čontot mompa sovetskij vlastʼ načalʼnik.</ta>
            <ta e="T269" id="Seg_150" s="T263">čaptomnotɨt täpɨt. qapije qumɨt qoraŋotɨt.</ta>
            <ta e="T273" id="Seg_151" s="T269">nado šentɨ zakonmɨt ilʼlʼoqo.</ta>
            <ta e="T275" id="Seg_152" s="T273">nilʼ tomnotɨt.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T255" id="Seg_153" s="T251">Pičʼelʼ Mačʼoːqɨt nɨmtɨ ilʼesɨmɨt. </ta>
            <ta e="T263" id="Seg_154" s="T255">Ilʼtät čʼontoːqɨt ukkur čʼontot mompa sovetskij vlastʼ načʼalʼnik. </ta>
            <ta e="T269" id="Seg_155" s="T263">Čʼap tomnɔːtɨt täpɨt: Qapıje qumɨt koraŋɔːtɨt. </ta>
            <ta e="T273" id="Seg_156" s="T269">Nado šentɨ zakonmɨt iloqo. </ta>
            <ta e="T275" id="Seg_157" s="T273">Nılʼ tomnɔːtɨt. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T252" id="Seg_158" s="T251">pičʼe-lʼ</ta>
            <ta e="T253" id="Seg_159" s="T252">mačʼoː-qɨt</ta>
            <ta e="T254" id="Seg_160" s="T253">nɨmtɨ</ta>
            <ta e="T255" id="Seg_161" s="T254">ilʼe-sɨ-mɨt</ta>
            <ta e="T256" id="Seg_162" s="T255">ilʼ-tä-t</ta>
            <ta e="T257" id="Seg_163" s="T256">čʼontoː-qɨt</ta>
            <ta e="T258" id="Seg_164" s="T257">ukkur</ta>
            <ta e="T259" id="Seg_165" s="T258">čʼonto-t</ta>
            <ta e="T260" id="Seg_166" s="T259">mompa</ta>
            <ta e="T261" id="Seg_167" s="T260">sovetskij</ta>
            <ta e="T262" id="Seg_168" s="T261">vlastʼ</ta>
            <ta e="T263" id="Seg_169" s="T262">načʼalʼnik</ta>
            <ta e="T264" id="Seg_170" s="T263">čʼap</ta>
            <ta e="T265" id="Seg_171" s="T264">tom-nɔː-tɨt</ta>
            <ta e="T266" id="Seg_172" s="T265">täp-ɨ-t</ta>
            <ta e="T267" id="Seg_173" s="T266">Qapıje</ta>
            <ta e="T268" id="Seg_174" s="T267">qum-ɨ-t</ta>
            <ta e="T269" id="Seg_175" s="T268">kora-ŋɔː-tɨt</ta>
            <ta e="T270" id="Seg_176" s="T269">nado</ta>
            <ta e="T271" id="Seg_177" s="T270">šentɨ</ta>
            <ta e="T272" id="Seg_178" s="T271">zakon-mɨt</ta>
            <ta e="T273" id="Seg_179" s="T272">ilo-qo</ta>
            <ta e="T274" id="Seg_180" s="T273">nı-lʼ</ta>
            <ta e="T275" id="Seg_181" s="T274">tom-nɔː-tɨt</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T252" id="Seg_182" s="T251">pičʼčʼa-lʼ</ta>
            <ta e="T253" id="Seg_183" s="T252">mačʼɨ-qɨn</ta>
            <ta e="T254" id="Seg_184" s="T253">nɨmtɨ</ta>
            <ta e="T255" id="Seg_185" s="T254">ilɨ-sɨ-mɨt</ta>
            <ta e="T256" id="Seg_186" s="T255">ilɨ-ptäː-n</ta>
            <ta e="T257" id="Seg_187" s="T256">čʼontɨ-qɨn</ta>
            <ta e="T258" id="Seg_188" s="T257">ukkɨr</ta>
            <ta e="T259" id="Seg_189" s="T258">čʼontɨ-k</ta>
            <ta e="T260" id="Seg_190" s="T259">mompa</ta>
            <ta e="T261" id="Seg_191" s="T260">sovetskij</ta>
            <ta e="T262" id="Seg_192" s="T261">vlastʼ</ta>
            <ta e="T263" id="Seg_193" s="T262">načʼalʼnik</ta>
            <ta e="T264" id="Seg_194" s="T263">čʼam</ta>
            <ta e="T265" id="Seg_195" s="T264">tom-ŋɨ-tɨt</ta>
            <ta e="T266" id="Seg_196" s="T265">təp-ɨ-t</ta>
            <ta e="T267" id="Seg_197" s="T266">qapı</ta>
            <ta e="T268" id="Seg_198" s="T267">qum-ɨ-t</ta>
            <ta e="T269" id="Seg_199" s="T268">kora-ŋɨ-tɨt</ta>
            <ta e="T270" id="Seg_200" s="T269">naːda</ta>
            <ta e="T271" id="Seg_201" s="T270">šentɨ</ta>
            <ta e="T272" id="Seg_202" s="T271">zakon-mɨn</ta>
            <ta e="T273" id="Seg_203" s="T272">ilɨ-qo</ta>
            <ta e="T274" id="Seg_204" s="T273">nılʼčʼɨ-lʼ</ta>
            <ta e="T275" id="Seg_205" s="T274">tom-ŋɨ-tɨt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T252" id="Seg_206" s="T251">pike-ADJZ</ta>
            <ta e="T253" id="Seg_207" s="T252">forest-LOC</ta>
            <ta e="T254" id="Seg_208" s="T253">there</ta>
            <ta e="T255" id="Seg_209" s="T254">live-PST-1PL</ta>
            <ta e="T256" id="Seg_210" s="T255">live-ACTN-GEN</ta>
            <ta e="T257" id="Seg_211" s="T256">in.the.middle-ADV.LOC</ta>
            <ta e="T258" id="Seg_212" s="T257">one</ta>
            <ta e="T259" id="Seg_213" s="T258">middle-ADVZ</ta>
            <ta e="T260" id="Seg_214" s="T259">it.is.said</ta>
            <ta e="T261" id="Seg_215" s="T260">Soviet</ta>
            <ta e="T262" id="Seg_216" s="T261">authority.[NOM]</ta>
            <ta e="T263" id="Seg_217" s="T262">head.[NOM]</ta>
            <ta e="T264" id="Seg_218" s="T263">only</ta>
            <ta e="T265" id="Seg_219" s="T264">speak-CO-3PL</ta>
            <ta e="T266" id="Seg_220" s="T265">(s)he-EP-PL</ta>
            <ta e="T267" id="Seg_221" s="T266">supposedly</ta>
            <ta e="T268" id="Seg_222" s="T267">human.being-EP-PL.[NOM]</ta>
            <ta e="T269" id="Seg_223" s="T268">go.hunting-CO-3PL</ta>
            <ta e="T270" id="Seg_224" s="T269">must</ta>
            <ta e="T271" id="Seg_225" s="T270">new</ta>
            <ta e="T272" id="Seg_226" s="T271">law-PROL</ta>
            <ta e="T273" id="Seg_227" s="T272">live-INF</ta>
            <ta e="T274" id="Seg_228" s="T273">such-ADJZ</ta>
            <ta e="T275" id="Seg_229" s="T274">speak-CO-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T252" id="Seg_230" s="T251">щука-ADJZ</ta>
            <ta e="T253" id="Seg_231" s="T252">лес-LOC</ta>
            <ta e="T254" id="Seg_232" s="T253">там</ta>
            <ta e="T255" id="Seg_233" s="T254">жить-PST-1PL</ta>
            <ta e="T256" id="Seg_234" s="T255">жить-ACTN-GEN</ta>
            <ta e="T257" id="Seg_235" s="T256">посередине-ADV.LOC</ta>
            <ta e="T258" id="Seg_236" s="T257">один</ta>
            <ta e="T259" id="Seg_237" s="T258">середина-ADVZ</ta>
            <ta e="T260" id="Seg_238" s="T259">мол</ta>
            <ta e="T261" id="Seg_239" s="T260">Советский</ta>
            <ta e="T262" id="Seg_240" s="T261">власть.[NOM]</ta>
            <ta e="T263" id="Seg_241" s="T262">начальник.[NOM]</ta>
            <ta e="T264" id="Seg_242" s="T263">только</ta>
            <ta e="T265" id="Seg_243" s="T264">сказать-CO-3PL</ta>
            <ta e="T266" id="Seg_244" s="T265">он(а)-EP-PL</ta>
            <ta e="T267" id="Seg_245" s="T266">вроде</ta>
            <ta e="T268" id="Seg_246" s="T267">человек-EP-PL.[NOM]</ta>
            <ta e="T269" id="Seg_247" s="T268">отправиться.на.охоту-CO-3PL</ta>
            <ta e="T270" id="Seg_248" s="T269">надо</ta>
            <ta e="T271" id="Seg_249" s="T270">новый</ta>
            <ta e="T272" id="Seg_250" s="T271">закон-PROL</ta>
            <ta e="T273" id="Seg_251" s="T272">жить-INF</ta>
            <ta e="T274" id="Seg_252" s="T273">такой-ADJZ</ta>
            <ta e="T275" id="Seg_253" s="T274">сказать-CO-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T252" id="Seg_254" s="T251">n-n&gt;adj</ta>
            <ta e="T253" id="Seg_255" s="T252">n-n:case3</ta>
            <ta e="T254" id="Seg_256" s="T253">adv</ta>
            <ta e="T255" id="Seg_257" s="T254">v-v:tense-v:pn</ta>
            <ta e="T256" id="Seg_258" s="T255">v-v&gt;n-n:case3</ta>
            <ta e="T257" id="Seg_259" s="T256">pp-n&gt;adv</ta>
            <ta e="T258" id="Seg_260" s="T257">num</ta>
            <ta e="T259" id="Seg_261" s="T258">n-n&gt;adv</ta>
            <ta e="T260" id="Seg_262" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_263" s="T260">adj</ta>
            <ta e="T262" id="Seg_264" s="T261">n-n:case3</ta>
            <ta e="T263" id="Seg_265" s="T262">n-n:case3</ta>
            <ta e="T264" id="Seg_266" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_267" s="T264">v-v:ins-v:pn</ta>
            <ta e="T266" id="Seg_268" s="T265">pers-n:(ins)-n:num</ta>
            <ta e="T267" id="Seg_269" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_270" s="T267">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T269" id="Seg_271" s="T268">v-v:ins-v:pn</ta>
            <ta e="T270" id="Seg_272" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_273" s="T270">adj</ta>
            <ta e="T272" id="Seg_274" s="T271">n-n:case3</ta>
            <ta e="T273" id="Seg_275" s="T272">v-v:inf</ta>
            <ta e="T274" id="Seg_276" s="T273">dem-adj&gt;adj</ta>
            <ta e="T275" id="Seg_277" s="T274">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T252" id="Seg_278" s="T251">adj</ta>
            <ta e="T253" id="Seg_279" s="T252">n</ta>
            <ta e="T254" id="Seg_280" s="T253">adv</ta>
            <ta e="T255" id="Seg_281" s="T254">v</ta>
            <ta e="T256" id="Seg_282" s="T255">n</ta>
            <ta e="T257" id="Seg_283" s="T256">pp</ta>
            <ta e="T258" id="Seg_284" s="T257">num</ta>
            <ta e="T259" id="Seg_285" s="T258">adv</ta>
            <ta e="T260" id="Seg_286" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_287" s="T260">adj</ta>
            <ta e="T262" id="Seg_288" s="T261">n</ta>
            <ta e="T263" id="Seg_289" s="T262">n</ta>
            <ta e="T264" id="Seg_290" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_291" s="T264">v</ta>
            <ta e="T266" id="Seg_292" s="T265">pers</ta>
            <ta e="T267" id="Seg_293" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_294" s="T267">n</ta>
            <ta e="T269" id="Seg_295" s="T268">v</ta>
            <ta e="T270" id="Seg_296" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_297" s="T270">adj</ta>
            <ta e="T272" id="Seg_298" s="T271">n</ta>
            <ta e="T273" id="Seg_299" s="T272">v</ta>
            <ta e="T274" id="Seg_300" s="T273">adj</ta>
            <ta e="T275" id="Seg_301" s="T274">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T253" id="Seg_302" s="T252">np:L</ta>
            <ta e="T255" id="Seg_303" s="T254">0.1.h:E</ta>
            <ta e="T257" id="Seg_304" s="T256">pp:Time</ta>
            <ta e="T259" id="Seg_305" s="T258">adv:Time</ta>
            <ta e="T263" id="Seg_306" s="T261">np:A</ta>
            <ta e="T266" id="Seg_307" s="T265">pro.h:A</ta>
            <ta e="T268" id="Seg_308" s="T267">np.h:A</ta>
            <ta e="T275" id="Seg_309" s="T274">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T255" id="Seg_310" s="T254">0.1.h:S v:pred</ta>
            <ta e="T263" id="Seg_311" s="T261">np:S</ta>
            <ta e="T265" id="Seg_312" s="T264">v:pred</ta>
            <ta e="T266" id="Seg_313" s="T265">pro.h:S</ta>
            <ta e="T268" id="Seg_314" s="T267">np.h:S</ta>
            <ta e="T269" id="Seg_315" s="T268">v:pred</ta>
            <ta e="T275" id="Seg_316" s="T274">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T253" id="Seg_317" s="T252">accs-gen</ta>
            <ta e="T255" id="Seg_318" s="T254">0.new</ta>
            <ta e="T263" id="Seg_319" s="T260">accs-gen</ta>
            <ta e="T265" id="Seg_320" s="T264">quot-S</ta>
            <ta e="T266" id="Seg_321" s="T265">accs-gen</ta>
            <ta e="T268" id="Seg_322" s="T267">accs-gen-Q</ta>
            <ta e="T272" id="Seg_323" s="T271">accs-gen-Q</ta>
            <ta e="T275" id="Seg_324" s="T274">0.giv-active</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T261" id="Seg_325" s="T260">RUS:cult</ta>
            <ta e="T262" id="Seg_326" s="T261">RUS:cult</ta>
            <ta e="T263" id="Seg_327" s="T262">RUS:cult</ta>
            <ta e="T270" id="Seg_328" s="T269">RUS:mod</ta>
            <ta e="T272" id="Seg_329" s="T271">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T261" id="Seg_330" s="T260">dir:bare</ta>
            <ta e="T262" id="Seg_331" s="T261">dir:bare</ta>
            <ta e="T263" id="Seg_332" s="T262">dir:bare</ta>
            <ta e="T270" id="Seg_333" s="T269">dir:bare</ta>
            <ta e="T272" id="Seg_334" s="T271">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T263" id="Seg_335" s="T260">RUS:int:alt</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T255" id="Seg_336" s="T251">В Щучьем лесу там [мы] жили.</ta>
            <ta e="T263" id="Seg_337" s="T255">Однажды [стали говорить, что] Советская власть [пришла].</ta>
            <ta e="T269" id="Seg_338" s="T263">Только говорят: "Люди вроде ездят на охоту.</ta>
            <ta e="T273" id="Seg_339" s="T269">Надо по новому закону жить".</ta>
            <ta e="T275" id="Seg_340" s="T273">Так говорят.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T255" id="Seg_341" s="T251">We lived there, in the pike forest.</ta>
            <ta e="T263" id="Seg_342" s="T255">One day they started saying that the Soviet power [had come].</ta>
            <ta e="T269" id="Seg_343" s="T263">They only say: "People supposedly go hunting.</ta>
            <ta e="T273" id="Seg_344" s="T269">We should live according to the new law".</ta>
            <ta e="T275" id="Seg_345" s="T273">They say so.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T255" id="Seg_346" s="T251">Wir lebten dort, im Hechtwald.</ta>
            <ta e="T263" id="Seg_347" s="T255">Irgendwann sagte man, dass die Sowjetmacht [gekommen war].</ta>
            <ta e="T269" id="Seg_348" s="T263">Man sagt nur: "Leute gehen wohl jagen.</ta>
            <ta e="T273" id="Seg_349" s="T269">Man muss nach dem neuem Gesetz leben."</ta>
            <ta e="T275" id="Seg_350" s="T273">So sagt man.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T255" id="Seg_351" s="T251">в щучьем лесу там жили</ta>
            <ta e="T269" id="Seg_352" s="T263">ему что говорят люди ездят</ta>
            <ta e="T273" id="Seg_353" s="T269">надо по новому закону жить</ta>
            <ta e="T275" id="Seg_354" s="T273">так говорят</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T255" id="Seg_355" s="T251">[OSV]: probably "Pičʼelʼ Mačʼɨ" is the name of some place.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
