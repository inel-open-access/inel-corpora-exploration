<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID2CE71937-1B04-A1AC-0C94-B058D9277494">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\KFN_1965_BearHunting1_nar\KFN_1965_BearHunting1_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">35</ud-information>
            <ud-information attribute-name="# HIAT:w">26</ud-information>
            <ud-information attribute-name="# e">26</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KFN">
            <abbreviation>KFN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KFN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T26" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">korɣom</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">mat</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">čadešpɨkak</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">mi</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">korkap</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">čaǯap</ts>
                  <nts id="Seg_23" n="HIAT:ip">,</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">korɣə</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">kuranna</ts>
                  <nts id="Seg_30" n="HIAT:ip">,</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">mat</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">tovarišam</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">kaːʒana</ts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_43" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">korɣa</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">nʼӧtɨt</ts>
                  <nts id="Seg_49" n="HIAT:ip">,</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">mat</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">kaʒannak</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">naj</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">načidelʼe</ts>
                  <nts id="Seg_62" n="HIAT:ip">,</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">kɨba</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">na</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">korɣa</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">orannɨt</ts>
                  <nts id="Seg_75" n="HIAT:ip">,</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">mat</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">čaǯap</ts>
                  <nts id="Seg_82" n="HIAT:ip">,</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">korɣə</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">ille</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">aːlča</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T26" id="Seg_94" n="sc" s="T0">
               <ts e="T1" id="Seg_96" n="e" s="T0">korɣom </ts>
               <ts e="T2" id="Seg_98" n="e" s="T1">mat </ts>
               <ts e="T3" id="Seg_100" n="e" s="T2">čadešpɨkak. </ts>
               <ts e="T4" id="Seg_102" n="e" s="T3">mi </ts>
               <ts e="T5" id="Seg_104" n="e" s="T4">korkap </ts>
               <ts e="T6" id="Seg_106" n="e" s="T5">čaǯap, </ts>
               <ts e="T7" id="Seg_108" n="e" s="T6">korɣə </ts>
               <ts e="T8" id="Seg_110" n="e" s="T7">kuranna, </ts>
               <ts e="T9" id="Seg_112" n="e" s="T8">mat </ts>
               <ts e="T10" id="Seg_114" n="e" s="T9">tovarišam </ts>
               <ts e="T11" id="Seg_116" n="e" s="T10">kaːʒana. </ts>
               <ts e="T12" id="Seg_118" n="e" s="T11">korɣa </ts>
               <ts e="T13" id="Seg_120" n="e" s="T12">nʼӧtɨt, </ts>
               <ts e="T14" id="Seg_122" n="e" s="T13">mat </ts>
               <ts e="T15" id="Seg_124" n="e" s="T14">kaʒannak </ts>
               <ts e="T16" id="Seg_126" n="e" s="T15">naj </ts>
               <ts e="T17" id="Seg_128" n="e" s="T16">načidelʼe, </ts>
               <ts e="T18" id="Seg_130" n="e" s="T17">kɨba </ts>
               <ts e="T19" id="Seg_132" n="e" s="T18">na </ts>
               <ts e="T20" id="Seg_134" n="e" s="T19">korɣa </ts>
               <ts e="T21" id="Seg_136" n="e" s="T20">orannɨt, </ts>
               <ts e="T22" id="Seg_138" n="e" s="T21">mat </ts>
               <ts e="T23" id="Seg_140" n="e" s="T22">čaǯap, </ts>
               <ts e="T24" id="Seg_142" n="e" s="T23">korɣə </ts>
               <ts e="T25" id="Seg_144" n="e" s="T24">ille </ts>
               <ts e="T26" id="Seg_146" n="e" s="T25">aːlča. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_147" s="T0">KFN_1965_BearHunting1_nar.001 (001.001)</ta>
            <ta e="T11" id="Seg_148" s="T3">KFN_1965_BearHunting1_nar.002 (001.002)</ta>
            <ta e="T26" id="Seg_149" s="T11">KFN_1965_BearHunting1_nar.003 (001.003)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_150" s="T0"> ‎‎′kорɣом мат ча′дешпыкак.</ta>
            <ta e="T11" id="Seg_151" s="T3">ми kо′ркап ча′джап, kорɣъ ку′ранна, мат товарищан kа̄жана. </ta>
            <ta e="T26" id="Seg_152" s="T11">′kорɣа ′нʼӧтыт, мат ′kажаннак най начи′делʼе, кыба на kорɣа о′ранныт, мат ′чад[т]жап, kорɣъ ′иllе а̄lча.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_153" s="T0">korɣom mat čadešpɨkak.</ta>
            <ta e="T11" id="Seg_154" s="T3">mi korkap čaǯap, korɣə kuranna, mat tovarišan kaːʒana.</ta>
            <ta e="T26" id="Seg_155" s="T11"> ‎‎korɣa nʼӧtɨt, mat kaʒannak naj načidelʼe, kɨba na korɣa orannɨt, mat čaǯap, korɣə ille aːlča.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_156" s="T0">korɣom mat čadešpɨkak. </ta>
            <ta e="T11" id="Seg_157" s="T3">mi korkap čaǯap, korɣə kuranna, mat tovarišam kaːʒana. </ta>
            <ta e="T26" id="Seg_158" s="T11">korɣa nʼӧtɨt, mat kaʒannak naj načidelʼe, kɨba na korɣa orannɨt, mat čaǯap, korɣə ille aːlča. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_159" s="T0">korɣo-m</ta>
            <ta e="T2" id="Seg_160" s="T1">mat</ta>
            <ta e="T3" id="Seg_161" s="T2">čade-špɨ-ka-k</ta>
            <ta e="T4" id="Seg_162" s="T3">mi</ta>
            <ta e="T5" id="Seg_163" s="T4">korka-p</ta>
            <ta e="T6" id="Seg_164" s="T5">čaǯa-p</ta>
            <ta e="T7" id="Seg_165" s="T6">korɣə</ta>
            <ta e="T8" id="Seg_166" s="T7">kur-ɨ-nna</ta>
            <ta e="T9" id="Seg_167" s="T8">mat</ta>
            <ta e="T10" id="Seg_168" s="T9">tovariš-a-m</ta>
            <ta e="T11" id="Seg_169" s="T10">kaːʒan-a</ta>
            <ta e="T12" id="Seg_170" s="T11">korɣa</ta>
            <ta e="T13" id="Seg_171" s="T12">nʼӧ-tɨ-t</ta>
            <ta e="T14" id="Seg_172" s="T13">mat</ta>
            <ta e="T15" id="Seg_173" s="T14">kaʒan-na-k</ta>
            <ta e="T16" id="Seg_174" s="T15">naj</ta>
            <ta e="T17" id="Seg_175" s="T16">načidelʼe</ta>
            <ta e="T18" id="Seg_176" s="T17">kɨba</ta>
            <ta e="T19" id="Seg_177" s="T18">na</ta>
            <ta e="T20" id="Seg_178" s="T19">korɣa</ta>
            <ta e="T21" id="Seg_179" s="T20">ora-nnɨ-t</ta>
            <ta e="T22" id="Seg_180" s="T21">mat</ta>
            <ta e="T23" id="Seg_181" s="T22">čaǯa-p</ta>
            <ta e="T24" id="Seg_182" s="T23">korɣə</ta>
            <ta e="T25" id="Seg_183" s="T24">ille</ta>
            <ta e="T26" id="Seg_184" s="T25">aːlča</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_185" s="T0">qorqɨ-m</ta>
            <ta e="T2" id="Seg_186" s="T1">man</ta>
            <ta e="T3" id="Seg_187" s="T2">čačɨ-špɨ-ku-k</ta>
            <ta e="T4" id="Seg_188" s="T3">mi</ta>
            <ta e="T5" id="Seg_189" s="T4">qorqɨ-p</ta>
            <ta e="T6" id="Seg_190" s="T5">čačɨ-m</ta>
            <ta e="T7" id="Seg_191" s="T6">qorqɨ</ta>
            <ta e="T8" id="Seg_192" s="T7">kur-ɨ-ŋɨ</ta>
            <ta e="T9" id="Seg_193" s="T8">man</ta>
            <ta e="T10" id="Seg_194" s="T9">tovariš-ɨ-mɨ</ta>
            <ta e="T11" id="Seg_195" s="T10">kaʒan-ɨ</ta>
            <ta e="T12" id="Seg_196" s="T11">qorqɨ</ta>
            <ta e="T13" id="Seg_197" s="T12">nʼeː-ntɨ-t</ta>
            <ta e="T14" id="Seg_198" s="T13">man</ta>
            <ta e="T15" id="Seg_199" s="T14">kaʒan-ŋɨ-k</ta>
            <ta e="T16" id="Seg_200" s="T15">naj</ta>
            <ta e="T17" id="Seg_201" s="T16">načidelʼi</ta>
            <ta e="T18" id="Seg_202" s="T17">kɨba</ta>
            <ta e="T19" id="Seg_203" s="T18">na</ta>
            <ta e="T20" id="Seg_204" s="T19">qorqɨ</ta>
            <ta e="T21" id="Seg_205" s="T20">ora-ŋɨ-t</ta>
            <ta e="T22" id="Seg_206" s="T21">man</ta>
            <ta e="T23" id="Seg_207" s="T22">čačɨ-m</ta>
            <ta e="T24" id="Seg_208" s="T23">qorqɨ</ta>
            <ta e="T25" id="Seg_209" s="T24">illä</ta>
            <ta e="T26" id="Seg_210" s="T25">alʼči</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_211" s="T0">bear-ACC</ta>
            <ta e="T2" id="Seg_212" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_213" s="T2">shoot-IPFV2-DUR-1SG.S</ta>
            <ta e="T4" id="Seg_214" s="T3">we.DU.NOM</ta>
            <ta e="T5" id="Seg_215" s="T4">bear-ACC</ta>
            <ta e="T6" id="Seg_216" s="T5">shoot-1SG.O</ta>
            <ta e="T7" id="Seg_217" s="T6">bear.[NOM]</ta>
            <ta e="T8" id="Seg_218" s="T7">run-EP-CO.[3SG.S]</ta>
            <ta e="T9" id="Seg_219" s="T8">I.GEN</ta>
            <ta e="T10" id="Seg_220" s="T9">companion-EP-1SG</ta>
            <ta e="T11" id="Seg_221" s="T10">run-EP.[3SG.S]</ta>
            <ta e="T12" id="Seg_222" s="T11">bear.[NOM]</ta>
            <ta e="T13" id="Seg_223" s="T12">hunt-IPFV-3SG.O</ta>
            <ta e="T14" id="Seg_224" s="T13">I.NOM</ta>
            <ta e="T15" id="Seg_225" s="T14">run-CO-1SG.S</ta>
            <ta e="T16" id="Seg_226" s="T15">also</ta>
            <ta e="T17" id="Seg_227" s="T16">there</ta>
            <ta e="T18" id="Seg_228" s="T17">small</ta>
            <ta e="T19" id="Seg_229" s="T18">INFER</ta>
            <ta e="T20" id="Seg_230" s="T19">bear.[NOM]</ta>
            <ta e="T21" id="Seg_231" s="T20">hold-CO-3SG.O</ta>
            <ta e="T22" id="Seg_232" s="T21">I.NOM</ta>
            <ta e="T23" id="Seg_233" s="T22">shoot-1SG.O</ta>
            <ta e="T24" id="Seg_234" s="T23">bear.[NOM]</ta>
            <ta e="T25" id="Seg_235" s="T24">down</ta>
            <ta e="T26" id="Seg_236" s="T25">fall.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_237" s="T0">медведь-ACC</ta>
            <ta e="T2" id="Seg_238" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_239" s="T2">стрелять-IPFV2-DUR-1SG.S</ta>
            <ta e="T4" id="Seg_240" s="T3">мы.DU.NOM</ta>
            <ta e="T5" id="Seg_241" s="T4">медведь-ACC</ta>
            <ta e="T6" id="Seg_242" s="T5">стрелять-1SG.O</ta>
            <ta e="T7" id="Seg_243" s="T6">медведь.[NOM]</ta>
            <ta e="T8" id="Seg_244" s="T7">бегать-EP-CO.[3SG.S]</ta>
            <ta e="T9" id="Seg_245" s="T8">я.GEN</ta>
            <ta e="T10" id="Seg_246" s="T9">товарищ-EP-1SG</ta>
            <ta e="T11" id="Seg_247" s="T10">бежать-EP.[3SG.S]</ta>
            <ta e="T12" id="Seg_248" s="T11">медведь.[NOM]</ta>
            <ta e="T13" id="Seg_249" s="T12">гнать-IPFV-3SG.O</ta>
            <ta e="T14" id="Seg_250" s="T13">я.NOM</ta>
            <ta e="T15" id="Seg_251" s="T14">бежать-CO-1SG.S</ta>
            <ta e="T16" id="Seg_252" s="T15">тоже</ta>
            <ta e="T17" id="Seg_253" s="T16">туда</ta>
            <ta e="T18" id="Seg_254" s="T17">маленький</ta>
            <ta e="T19" id="Seg_255" s="T18">INFER</ta>
            <ta e="T20" id="Seg_256" s="T19">медведь.[NOM]</ta>
            <ta e="T21" id="Seg_257" s="T20">держать-CO-3SG.O</ta>
            <ta e="T22" id="Seg_258" s="T21">я.NOM</ta>
            <ta e="T23" id="Seg_259" s="T22">стрелять-1SG.O</ta>
            <ta e="T24" id="Seg_260" s="T23">медведь.[NOM]</ta>
            <ta e="T25" id="Seg_261" s="T24">вниз</ta>
            <ta e="T26" id="Seg_262" s="T25">упасть.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_263" s="T0">n-n:case1</ta>
            <ta e="T2" id="Seg_264" s="T1">pers</ta>
            <ta e="T3" id="Seg_265" s="T2">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T4" id="Seg_266" s="T3">pers</ta>
            <ta e="T5" id="Seg_267" s="T4">n-n:case1</ta>
            <ta e="T6" id="Seg_268" s="T5">v-v:pn</ta>
            <ta e="T7" id="Seg_269" s="T6">n-n:case1</ta>
            <ta e="T8" id="Seg_270" s="T7">v-infl:ins-v:ins-v:pn</ta>
            <ta e="T9" id="Seg_271" s="T8">pers</ta>
            <ta e="T10" id="Seg_272" s="T9">n-infl:ins-n:poss</ta>
            <ta e="T11" id="Seg_273" s="T10">v-infl:ins-v:pn</ta>
            <ta e="T12" id="Seg_274" s="T11">n-n:case1</ta>
            <ta e="T13" id="Seg_275" s="T12">v-v&gt;v-v:pn</ta>
            <ta e="T14" id="Seg_276" s="T13">pers</ta>
            <ta e="T15" id="Seg_277" s="T14">v-v:ins-v:pn</ta>
            <ta e="T16" id="Seg_278" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_279" s="T16">adv</ta>
            <ta e="T18" id="Seg_280" s="T17">adj</ta>
            <ta e="T19" id="Seg_281" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_282" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_283" s="T20">v-v:ins-v:pn</ta>
            <ta e="T22" id="Seg_284" s="T21">pers</ta>
            <ta e="T23" id="Seg_285" s="T22">v-v:pn</ta>
            <ta e="T24" id="Seg_286" s="T23">n-n:case1</ta>
            <ta e="T25" id="Seg_287" s="T24">preverb</ta>
            <ta e="T26" id="Seg_288" s="T25">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_289" s="T0">n</ta>
            <ta e="T2" id="Seg_290" s="T1">pers</ta>
            <ta e="T3" id="Seg_291" s="T2">v</ta>
            <ta e="T4" id="Seg_292" s="T3">pers</ta>
            <ta e="T5" id="Seg_293" s="T4">n</ta>
            <ta e="T6" id="Seg_294" s="T5">v</ta>
            <ta e="T7" id="Seg_295" s="T6">n</ta>
            <ta e="T8" id="Seg_296" s="T7">v</ta>
            <ta e="T9" id="Seg_297" s="T8">pers</ta>
            <ta e="T10" id="Seg_298" s="T9">n</ta>
            <ta e="T11" id="Seg_299" s="T10">v</ta>
            <ta e="T12" id="Seg_300" s="T11">n</ta>
            <ta e="T13" id="Seg_301" s="T12">v</ta>
            <ta e="T14" id="Seg_302" s="T13">pers</ta>
            <ta e="T15" id="Seg_303" s="T14">v</ta>
            <ta e="T16" id="Seg_304" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_305" s="T16">adv</ta>
            <ta e="T18" id="Seg_306" s="T17">n</ta>
            <ta e="T19" id="Seg_307" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_308" s="T19">n</ta>
            <ta e="T21" id="Seg_309" s="T20">v</ta>
            <ta e="T22" id="Seg_310" s="T21">pers</ta>
            <ta e="T23" id="Seg_311" s="T22">v</ta>
            <ta e="T24" id="Seg_312" s="T23">n</ta>
            <ta e="T25" id="Seg_313" s="T24">preverb</ta>
            <ta e="T26" id="Seg_314" s="T25">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_315" s="T0">np:O</ta>
            <ta e="T2" id="Seg_316" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_317" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_318" s="T3">pro.h:S</ta>
            <ta e="T5" id="Seg_319" s="T4">np:O</ta>
            <ta e="T6" id="Seg_320" s="T5">v:pred</ta>
            <ta e="T7" id="Seg_321" s="T6">np:S</ta>
            <ta e="T8" id="Seg_322" s="T7">v:pred</ta>
            <ta e="T10" id="Seg_323" s="T9">np.h:S</ta>
            <ta e="T11" id="Seg_324" s="T10">v:pred</ta>
            <ta e="T12" id="Seg_325" s="T11">np:O</ta>
            <ta e="T13" id="Seg_326" s="T12">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_327" s="T13">pro.h:S</ta>
            <ta e="T15" id="Seg_328" s="T14">v:pred</ta>
            <ta e="T20" id="Seg_329" s="T19">np:O</ta>
            <ta e="T21" id="Seg_330" s="T20">0.3.h:S v:pred</ta>
            <ta e="T22" id="Seg_331" s="T21">pro.h:S</ta>
            <ta e="T23" id="Seg_332" s="T22">v:pred</ta>
            <ta e="T24" id="Seg_333" s="T23">np:S</ta>
            <ta e="T26" id="Seg_334" s="T25">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_335" s="T0">np:Th</ta>
            <ta e="T2" id="Seg_336" s="T1">pro.h:A</ta>
            <ta e="T4" id="Seg_337" s="T3">pro.h:A</ta>
            <ta e="T5" id="Seg_338" s="T4">np:Th</ta>
            <ta e="T7" id="Seg_339" s="T6">np:A</ta>
            <ta e="T9" id="Seg_340" s="T8">np.h:Poss</ta>
            <ta e="T10" id="Seg_341" s="T9">np.h:A</ta>
            <ta e="T12" id="Seg_342" s="T11">np:Th</ta>
            <ta e="T14" id="Seg_343" s="T13">pro.h:A</ta>
            <ta e="T17" id="Seg_344" s="T16">adv:G</ta>
            <ta e="T20" id="Seg_345" s="T19">np:Th</ta>
            <ta e="T21" id="Seg_346" s="T20">0.3.h:A</ta>
            <ta e="T22" id="Seg_347" s="T21">pro.h:A</ta>
            <ta e="T23" id="Seg_348" s="T22">0.3:Th</ta>
            <ta e="T24" id="Seg_349" s="T23">np:P</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T10" id="Seg_350" s="T9">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_351" s="T0">В медведя я стрелял.</ta>
            <ta e="T11" id="Seg_352" s="T3">Я выстрелил в медведя, медведь побежал, мой товарищ тоже побежал. </ta>
            <ta e="T26" id="Seg_353" s="T11">Он гонит медведя, я тоже побежал туда, он едва медведя не поймал, я выстрелил, медведь упал.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_354" s="T0">I shot at a bear.</ta>
            <ta e="T11" id="Seg_355" s="T3">I shot at the bear, the bear ran away, my companion ran away too.</ta>
            <ta e="T26" id="Seg_356" s="T11">He drives the bear, I ran there too, he barely missed the bear, I shot, the bear fell.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_357" s="T0">Ich habe auf einen Bären geschossen.</ta>
            <ta e="T11" id="Seg_358" s="T3">Ich habe auf den Bären geschossen, der Bär lief weg, mein Kamerad lief auch weg.</ta>
            <ta e="T26" id="Seg_359" s="T11">Er treibt den Bären, ich lief auch dorthin, er hat den Bären nicht getroffen, ich schoss, der Bär fiel um. </ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_360" s="T0">медведя стрелял я</ta>
            <ta e="T11" id="Seg_361" s="T3">я стрелял медведя медведь побежал мой товарищ побежал</ta>
            <ta e="T26" id="Seg_362" s="T11">медведя гонит я бежал тоже туда маленько медведя не поймал я стрелял медведь упал</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T11" id="Seg_363" s="T3">[WNB] tovarišan - must be tovarišam; the personal pronoun mi is not correct, it should be mat.</ta>
            <ta e="T26" id="Seg_364" s="T11">[WNB] negation is missing from the Selkup text.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
