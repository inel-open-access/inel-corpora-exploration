<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDF7A16C18-6E43-BEE8-4DF6-9306E5B4223B">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\KMS_1963_WoodGrouse_nar\KMS_1963_WoodGrouse_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">16</ud-information>
            <ud-information attribute-name="# HIAT:w">11</ud-information>
            <ud-information attribute-name="# e">11</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMS">
            <abbreviation>KMS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T11" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">säŋgɨm</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">qolʼdʼilʼewlʼe</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">qwenǯasaw</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">tüːləsän</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">qostəzaw</ts>
                  <nts id="Seg_23" n="HIAT:ip">,</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">tʼačaw</ts>
                  <nts id="Seg_27" n="HIAT:ip">.</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_30" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_32" n="HIAT:w" s="T7">säŋgɨ</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_35" n="HIAT:w" s="T8">pɨŋgɨlʼeŋ</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_39" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_41" n="HIAT:w" s="T9">täbɨm</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_44" n="HIAT:w" s="T10">iːjaw</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T11" id="Seg_47" n="sc" s="T0">
               <ts e="T1" id="Seg_49" n="e" s="T0">man </ts>
               <ts e="T2" id="Seg_51" n="e" s="T1">säŋgɨm </ts>
               <ts e="T3" id="Seg_53" n="e" s="T2">qolʼdʼilʼewlʼe </ts>
               <ts e="T4" id="Seg_55" n="e" s="T3">qwenǯasaw. </ts>
               <ts e="T5" id="Seg_57" n="e" s="T4">tüːləsän </ts>
               <ts e="T6" id="Seg_59" n="e" s="T5">qostəzaw, </ts>
               <ts e="T7" id="Seg_61" n="e" s="T6">tʼačaw. </ts>
               <ts e="T8" id="Seg_63" n="e" s="T7">säŋgɨ </ts>
               <ts e="T9" id="Seg_65" n="e" s="T8">pɨŋgɨlʼeŋ. </ts>
               <ts e="T10" id="Seg_67" n="e" s="T9">täbɨm </ts>
               <ts e="T11" id="Seg_69" n="e" s="T10">iːjaw. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_70" s="T0">KMS_1963_WoodGrouse_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_71" s="T4">KMS_1963_WoodGrouse_nar.002 (001.002)</ta>
            <ta e="T9" id="Seg_72" s="T7">KMS_1963_WoodGrouse_nar.003 (001.003)</ta>
            <ta e="T11" id="Seg_73" s="T9">KMS_1963_WoodGrouse_nar.004 (001.004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_74" s="T0">ман ′сӓңгым kолʼдʼи′лʼевлʼе ′kwенджасаw.</ta>
            <ta e="T7" id="Seg_75" s="T4">′тӱ̄лъсӓн kостъ′заw, тʼа′тшаw.</ta>
            <ta e="T9" id="Seg_76" s="T7">сӓңгы пыңгы′лʼең.</ta>
            <ta e="T11" id="Seg_77" s="T9">′тӓбым ӣ′jаw.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_78" s="T0">ман ′сӓңгым kолʼдʼи′лʼевлʼе ′kwенджасаw.</ta>
            <ta e="T7" id="Seg_79" s="T4">′тӱ̄лъсӓн kостъ′заw, тʼа′тшаw.</ta>
            <ta e="T9" id="Seg_80" s="T7">сӓңгы пыңгы′лʼең.</ta>
            <ta e="T11" id="Seg_81" s="T9">′тӓбым ӣ′jаw.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_82" s="T0">man säŋgɨm qolʼdʼilʼewlʼe qwenǯasaw. </ta>
            <ta e="T7" id="Seg_83" s="T4">tüːləsän qostəzaw, tʼačaw. </ta>
            <ta e="T9" id="Seg_84" s="T7">säŋgɨ pɨŋgɨlʼeŋ. </ta>
            <ta e="T11" id="Seg_85" s="T9">täbɨm iːjaw. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_86" s="T0">man</ta>
            <ta e="T2" id="Seg_87" s="T1">säŋgɨ-m</ta>
            <ta e="T3" id="Seg_88" s="T2">qolʼdʼi-lʼewlʼe</ta>
            <ta e="T4" id="Seg_89" s="T3">qwe-nǯa-sa-w</ta>
            <ta e="T5" id="Seg_90" s="T4">tüːləsä-n</ta>
            <ta e="T6" id="Seg_91" s="T5">qostə-za-w</ta>
            <ta e="T7" id="Seg_92" s="T6">tʼača-w</ta>
            <ta e="T8" id="Seg_93" s="T7">säŋgɨ</ta>
            <ta e="T9" id="Seg_94" s="T8">pɨŋgɨ-lʼe-ŋ</ta>
            <ta e="T10" id="Seg_95" s="T9">täb-ɨ-m</ta>
            <ta e="T11" id="Seg_96" s="T10">iː-ja-w</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_97" s="T0">man</ta>
            <ta e="T2" id="Seg_98" s="T1">säŋɨ-m</ta>
            <ta e="T3" id="Seg_99" s="T2">qolʼdʼi-lewle</ta>
            <ta e="T4" id="Seg_100" s="T3">qwən-nče-sɨ-m</ta>
            <ta e="T5" id="Seg_101" s="T4">tülʼse-n</ta>
            <ta e="T6" id="Seg_102" s="T5">qostə-sɨ-m</ta>
            <ta e="T7" id="Seg_103" s="T6">tʼaǯe-m</ta>
            <ta e="T8" id="Seg_104" s="T7">säŋɨ</ta>
            <ta e="T9" id="Seg_105" s="T8">puŋgə-lɨ-n</ta>
            <ta e="T10" id="Seg_106" s="T9">tap-ɨ-m</ta>
            <ta e="T11" id="Seg_107" s="T10">iː-ŋɨ-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_108" s="T0">I.NOM</ta>
            <ta e="T2" id="Seg_109" s="T1">capercaillie-ACC</ta>
            <ta e="T3" id="Seg_110" s="T2">sight-CVB</ta>
            <ta e="T4" id="Seg_111" s="T3">go.away-IPFV-PST-1SG.O</ta>
            <ta e="T5" id="Seg_112" s="T4">rifle-INSTR2</ta>
            <ta e="T6" id="Seg_113" s="T5">target-PST-1SG.O</ta>
            <ta e="T7" id="Seg_114" s="T6">shoot-1SG.O</ta>
            <ta e="T8" id="Seg_115" s="T7">capercaillie.[NOM]</ta>
            <ta e="T9" id="Seg_116" s="T8">fall-RES-3SG.S</ta>
            <ta e="T10" id="Seg_117" s="T9">he.NOM-EP-ACC</ta>
            <ta e="T11" id="Seg_118" s="T10">take-CO-1SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_119" s="T0">я.NOM</ta>
            <ta e="T2" id="Seg_120" s="T1">глухарь-ACC</ta>
            <ta e="T3" id="Seg_121" s="T2">увидеть-CVB</ta>
            <ta e="T4" id="Seg_122" s="T3">уйти-IPFV-PST-1SG.O</ta>
            <ta e="T5" id="Seg_123" s="T4">ружье-INSTR2</ta>
            <ta e="T6" id="Seg_124" s="T5">прицелиться-PST-1SG.O</ta>
            <ta e="T7" id="Seg_125" s="T6">стрелять-1SG.O</ta>
            <ta e="T8" id="Seg_126" s="T7">глухарь.[NOM]</ta>
            <ta e="T9" id="Seg_127" s="T8">упасть-RES-3SG.S</ta>
            <ta e="T10" id="Seg_128" s="T9">он.NOM-EP-ACC</ta>
            <ta e="T11" id="Seg_129" s="T10">взять-CO-1SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_130" s="T0">pers</ta>
            <ta e="T2" id="Seg_131" s="T1">n-n:case1</ta>
            <ta e="T3" id="Seg_132" s="T2">v-v&gt;adv</ta>
            <ta e="T4" id="Seg_133" s="T3">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_134" s="T4">n-n:case3</ta>
            <ta e="T6" id="Seg_135" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_136" s="T6">v-v:pn</ta>
            <ta e="T8" id="Seg_137" s="T7">n-n:case1</ta>
            <ta e="T9" id="Seg_138" s="T8">v-v&gt;v-v:pn</ta>
            <ta e="T10" id="Seg_139" s="T9">pers-n:ins-n:case1</ta>
            <ta e="T11" id="Seg_140" s="T10">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_141" s="T0">pers</ta>
            <ta e="T2" id="Seg_142" s="T1">n</ta>
            <ta e="T3" id="Seg_143" s="T2">adv</ta>
            <ta e="T4" id="Seg_144" s="T3">v</ta>
            <ta e="T5" id="Seg_145" s="T4">n</ta>
            <ta e="T6" id="Seg_146" s="T5">v</ta>
            <ta e="T7" id="Seg_147" s="T6">v</ta>
            <ta e="T8" id="Seg_148" s="T7">n</ta>
            <ta e="T9" id="Seg_149" s="T8">v</ta>
            <ta e="T10" id="Seg_150" s="T9">pers</ta>
            <ta e="T11" id="Seg_151" s="T10">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_152" s="T0">pro.h:A</ta>
            <ta e="T2" id="Seg_153" s="T1">np:Th</ta>
            <ta e="T5" id="Seg_154" s="T4">np:Ins</ta>
            <ta e="T6" id="Seg_155" s="T5">0.1.h:A</ta>
            <ta e="T7" id="Seg_156" s="T6">0.1.h:A</ta>
            <ta e="T8" id="Seg_157" s="T7">np:Th</ta>
            <ta e="T10" id="Seg_158" s="T9">np:P</ta>
            <ta e="T11" id="Seg_159" s="T10">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_160" s="T0">pro.h:S</ta>
            <ta e="T3" id="Seg_161" s="T1">s:adv</ta>
            <ta e="T4" id="Seg_162" s="T3">v:pred</ta>
            <ta e="T6" id="Seg_163" s="T5">0.1.h:S v:pred</ta>
            <ta e="T7" id="Seg_164" s="T6">0.1.h:S v:pred</ta>
            <ta e="T8" id="Seg_165" s="T7">np:S</ta>
            <ta e="T9" id="Seg_166" s="T8">v:pred</ta>
            <ta e="T10" id="Seg_167" s="T9">np:O</ta>
            <ta e="T11" id="Seg_168" s="T10">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_169" s="T0">Я увидав глухаря, подкрался.</ta>
            <ta e="T7" id="Seg_170" s="T4">Ружьем прицелился и стрелял.</ta>
            <ta e="T9" id="Seg_171" s="T7">Глухарь упал.</ta>
            <ta e="T11" id="Seg_172" s="T9">Его взял.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_173" s="T0">When I saw the capercaillie, I stalked it.</ta>
            <ta e="T7" id="Seg_174" s="T4">I targeted with the rifle and shot.</ta>
            <ta e="T9" id="Seg_175" s="T7">The capercaillie fell [down].</ta>
            <ta e="T11" id="Seg_176" s="T9">I took it.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_177" s="T0">Als ich das Auerhuhn bemerkte, schlich ich mich an.</ta>
            <ta e="T7" id="Seg_178" s="T4">Ich zielte mit dem Gewehr und schoss.</ta>
            <ta e="T9" id="Seg_179" s="T7">Das Auerhuhn ist runtergefallen.</ta>
            <ta e="T11" id="Seg_180" s="T9">Ich nahm es.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_181" s="T0">я глухаря увидав подкрался</ta>
            <ta e="T7" id="Seg_182" s="T4">ружьем прицелился стрелял</ta>
            <ta e="T9" id="Seg_183" s="T7">глухарь упал</ta>
            <ta e="T11" id="Seg_184" s="T9">его взял</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
