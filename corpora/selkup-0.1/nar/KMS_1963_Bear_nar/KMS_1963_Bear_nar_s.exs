<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID5A5F7272-E07C-D871-B0D7-80380AD98441">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\KMS_1963_Bear_nar\KMS_1963_Bear_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">93</ud-information>
            <ud-information attribute-name="# HIAT:w">74</ud-information>
            <ud-information attribute-name="# e">74</ud-information>
            <ud-information attribute-name="# HIAT:u">15</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMS">
            <abbreviation>KMS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T74" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Qwärɣa</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">tʼüumbädi</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">surɨm</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Täbɨnɨ</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">utoɣɨndə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">ɨkkə</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">mikuk</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">Oqqɨrɨŋ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">surulʼdʼi</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">quːla</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">qwännatə</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">qwätku</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">qwärɣɨm</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_50" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">Kanala</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">muːdɨnʼättə</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">surunni</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_62" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">Surum</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">ütälʼdiŋ</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">kanalanni</ts>
                  <nts id="Seg_71" n="HIAT:ip">,</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">surulʼdʼi</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">qula</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">tʼäčattə</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">surunni</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_87" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">Assə</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">šɨdɨŋ</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">naːrɨŋ</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_99" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">Qwärɣɨm</ts>
                  <nts id="Seg_102" n="HIAT:ip">,</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_105" n="HIAT:w" s="T27">surum</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_108" n="HIAT:w" s="T28">tʼärɨtnattə</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_112" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_114" n="HIAT:w" s="T29">Qwärɣa</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_117" n="HIAT:w" s="T30">tʼüuŋ</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_120" n="HIAT:w" s="T31">i</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_123" n="HIAT:w" s="T32">orannɨt</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_126" n="HIAT:w" s="T33">oqqɨr</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_129" n="HIAT:w" s="T34">surulʼdʼi</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_132" n="HIAT:w" s="T35">qum</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_136" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_138" n="HIAT:w" s="T36">Qwärɣa</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_141" n="HIAT:w" s="T37">qadi</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_144" n="HIAT:w" s="T38">udɨn</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_147" n="HIAT:w" s="T39">orannɨt</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_150" n="HIAT:w" s="T40">oloundə</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_153" n="HIAT:w" s="T41">na</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_156" n="HIAT:w" s="T42">surulʼdʼi</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_159" n="HIAT:w" s="T43">qum</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_163" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">Nɨškännɨt</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">oloɣä</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_171" n="HIAT:w" s="T46">qobɨm</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_175" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_177" n="HIAT:w" s="T47">Surulʼdʼi</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_180" n="HIAT:w" s="T48">qula</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_183" n="HIAT:w" s="T49">tʼäčattə</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_186" n="HIAT:w" s="T50">qwärɣɨm</ts>
                  <nts id="Seg_187" n="HIAT:ip">,</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_190" n="HIAT:w" s="T51">qwärɣa</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_193" n="HIAT:w" s="T52">čatšʼölʼdiŋ</ts>
                  <nts id="Seg_194" n="HIAT:ip">.</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_197" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_199" n="HIAT:w" s="T53">Qwärɣɨn</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_202" n="HIAT:w" s="T54">oralbädi</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_205" n="HIAT:w" s="T55">qum</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_208" n="HIAT:w" s="T56">qaːlɨŋ</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_211" n="HIAT:w" s="T57">iːllɨlʼe</ts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_215" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_217" n="HIAT:w" s="T58">Täbɨm</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_220" n="HIAT:w" s="T59">qwändattə</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_223" n="HIAT:w" s="T60">maksɨ</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_226" n="HIAT:w" s="T61">matʼtʼönda</ts>
                  <nts id="Seg_227" n="HIAT:ip">,</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_230" n="HIAT:w" s="T62">oloɣä</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_233" n="HIAT:w" s="T63">qobɨmdɨ</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_236" n="HIAT:w" s="T64">sütnattə</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_240" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_242" n="HIAT:w" s="T65">Tätta</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_245" n="HIAT:w" s="T66">somblʼe</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_248" n="HIAT:w" s="T67">tʼel</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_251" n="HIAT:w" s="T68">ippɨs</ts>
                  <nts id="Seg_252" n="HIAT:ip">.</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_255" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_257" n="HIAT:w" s="T69">Nänna</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_260" n="HIAT:w" s="T70">tüssan</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_263" n="HIAT:w" s="T71">ondɨ</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_266" n="HIAT:w" s="T72">ondä</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_269" n="HIAT:w" s="T73">äːtoɣondə</ts>
                  <nts id="Seg_270" n="HIAT:ip">.</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T74" id="Seg_272" n="sc" s="T0">
               <ts e="T1" id="Seg_274" n="e" s="T0">Qwärɣa </ts>
               <ts e="T2" id="Seg_276" n="e" s="T1">tʼüumbädi </ts>
               <ts e="T3" id="Seg_278" n="e" s="T2">surɨm. </ts>
               <ts e="T4" id="Seg_280" n="e" s="T3">Täbɨnɨ </ts>
               <ts e="T5" id="Seg_282" n="e" s="T4">utoɣɨndə </ts>
               <ts e="T6" id="Seg_284" n="e" s="T5">ɨkkə </ts>
               <ts e="T7" id="Seg_286" n="e" s="T6">mikuk. </ts>
               <ts e="T8" id="Seg_288" n="e" s="T7">Oqqɨrɨŋ </ts>
               <ts e="T9" id="Seg_290" n="e" s="T8">surulʼdʼi </ts>
               <ts e="T10" id="Seg_292" n="e" s="T9">quːla </ts>
               <ts e="T11" id="Seg_294" n="e" s="T10">qwännatə </ts>
               <ts e="T12" id="Seg_296" n="e" s="T11">qwätku </ts>
               <ts e="T13" id="Seg_298" n="e" s="T12">qwärɣɨm. </ts>
               <ts e="T14" id="Seg_300" n="e" s="T13">Kanala </ts>
               <ts e="T15" id="Seg_302" n="e" s="T14">muːdɨnʼättə </ts>
               <ts e="T16" id="Seg_304" n="e" s="T15">surunni. </ts>
               <ts e="T17" id="Seg_306" n="e" s="T16">Surum </ts>
               <ts e="T18" id="Seg_308" n="e" s="T17">ütälʼdiŋ </ts>
               <ts e="T19" id="Seg_310" n="e" s="T18">kanalanni, </ts>
               <ts e="T20" id="Seg_312" n="e" s="T19">surulʼdʼi </ts>
               <ts e="T21" id="Seg_314" n="e" s="T20">qula </ts>
               <ts e="T22" id="Seg_316" n="e" s="T21">tʼäčattə </ts>
               <ts e="T23" id="Seg_318" n="e" s="T22">surunni. </ts>
               <ts e="T24" id="Seg_320" n="e" s="T23">Assə </ts>
               <ts e="T25" id="Seg_322" n="e" s="T24">šɨdɨŋ </ts>
               <ts e="T26" id="Seg_324" n="e" s="T25">naːrɨŋ. </ts>
               <ts e="T27" id="Seg_326" n="e" s="T26">Qwärɣɨm, </ts>
               <ts e="T28" id="Seg_328" n="e" s="T27">surum </ts>
               <ts e="T29" id="Seg_330" n="e" s="T28">tʼärɨtnattə. </ts>
               <ts e="T30" id="Seg_332" n="e" s="T29">Qwärɣa </ts>
               <ts e="T31" id="Seg_334" n="e" s="T30">tʼüuŋ </ts>
               <ts e="T32" id="Seg_336" n="e" s="T31">i </ts>
               <ts e="T33" id="Seg_338" n="e" s="T32">orannɨt </ts>
               <ts e="T34" id="Seg_340" n="e" s="T33">oqqɨr </ts>
               <ts e="T35" id="Seg_342" n="e" s="T34">surulʼdʼi </ts>
               <ts e="T36" id="Seg_344" n="e" s="T35">qum. </ts>
               <ts e="T37" id="Seg_346" n="e" s="T36">Qwärɣa </ts>
               <ts e="T38" id="Seg_348" n="e" s="T37">qadi </ts>
               <ts e="T39" id="Seg_350" n="e" s="T38">udɨn </ts>
               <ts e="T40" id="Seg_352" n="e" s="T39">orannɨt </ts>
               <ts e="T41" id="Seg_354" n="e" s="T40">oloundə </ts>
               <ts e="T42" id="Seg_356" n="e" s="T41">na </ts>
               <ts e="T43" id="Seg_358" n="e" s="T42">surulʼdʼi </ts>
               <ts e="T44" id="Seg_360" n="e" s="T43">qum. </ts>
               <ts e="T45" id="Seg_362" n="e" s="T44">Nɨškännɨt </ts>
               <ts e="T46" id="Seg_364" n="e" s="T45">oloɣä </ts>
               <ts e="T47" id="Seg_366" n="e" s="T46">qobɨm. </ts>
               <ts e="T48" id="Seg_368" n="e" s="T47">Surulʼdʼi </ts>
               <ts e="T49" id="Seg_370" n="e" s="T48">qula </ts>
               <ts e="T50" id="Seg_372" n="e" s="T49">tʼäčattə </ts>
               <ts e="T51" id="Seg_374" n="e" s="T50">qwärɣɨm, </ts>
               <ts e="T52" id="Seg_376" n="e" s="T51">qwärɣa </ts>
               <ts e="T53" id="Seg_378" n="e" s="T52">čatšʼölʼdiŋ. </ts>
               <ts e="T54" id="Seg_380" n="e" s="T53">Qwärɣɨn </ts>
               <ts e="T55" id="Seg_382" n="e" s="T54">oralbädi </ts>
               <ts e="T56" id="Seg_384" n="e" s="T55">qum </ts>
               <ts e="T57" id="Seg_386" n="e" s="T56">qaːlɨŋ </ts>
               <ts e="T58" id="Seg_388" n="e" s="T57">iːllɨlʼe. </ts>
               <ts e="T59" id="Seg_390" n="e" s="T58">Täbɨm </ts>
               <ts e="T60" id="Seg_392" n="e" s="T59">qwändattə </ts>
               <ts e="T61" id="Seg_394" n="e" s="T60">maksɨ </ts>
               <ts e="T62" id="Seg_396" n="e" s="T61">matʼtʼönda, </ts>
               <ts e="T63" id="Seg_398" n="e" s="T62">oloɣä </ts>
               <ts e="T64" id="Seg_400" n="e" s="T63">qobɨmdɨ </ts>
               <ts e="T65" id="Seg_402" n="e" s="T64">sütnattə. </ts>
               <ts e="T66" id="Seg_404" n="e" s="T65">Tätta </ts>
               <ts e="T67" id="Seg_406" n="e" s="T66">somblʼe </ts>
               <ts e="T68" id="Seg_408" n="e" s="T67">tʼel </ts>
               <ts e="T69" id="Seg_410" n="e" s="T68">ippɨs. </ts>
               <ts e="T70" id="Seg_412" n="e" s="T69">Nänna </ts>
               <ts e="T71" id="Seg_414" n="e" s="T70">tüssan </ts>
               <ts e="T72" id="Seg_416" n="e" s="T71">ondɨ </ts>
               <ts e="T73" id="Seg_418" n="e" s="T72">ondä </ts>
               <ts e="T74" id="Seg_420" n="e" s="T73">äːtoɣondə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_421" s="T0">KMS_1963_Bear_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_422" s="T3">KMS_1963_Bear_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_423" s="T7">KMS_1963_Bear_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_424" s="T13">KMS_1963_Bear_nar.004 (001.004)</ta>
            <ta e="T23" id="Seg_425" s="T16">KMS_1963_Bear_nar.005 (001.005)</ta>
            <ta e="T26" id="Seg_426" s="T23">KMS_1963_Bear_nar.006 (001.006)</ta>
            <ta e="T29" id="Seg_427" s="T26">KMS_1963_Bear_nar.007 (001.007)</ta>
            <ta e="T36" id="Seg_428" s="T29">KMS_1963_Bear_nar.008 (001.008)</ta>
            <ta e="T44" id="Seg_429" s="T36">KMS_1963_Bear_nar.009 (001.009)</ta>
            <ta e="T47" id="Seg_430" s="T44">KMS_1963_Bear_nar.010 (001.010)</ta>
            <ta e="T53" id="Seg_431" s="T47">KMS_1963_Bear_nar.011 (001.011)</ta>
            <ta e="T58" id="Seg_432" s="T53">KMS_1963_Bear_nar.012 (001.012)</ta>
            <ta e="T65" id="Seg_433" s="T58">KMS_1963_Bear_nar.013 (001.013)</ta>
            <ta e="T69" id="Seg_434" s="T65">KMS_1963_Bear_nar.014 (001.014)</ta>
            <ta e="T74" id="Seg_435" s="T69">KMS_1963_Bear_nar.015 (001.015)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_436" s="T0">´kwäрɣа ′т′ӱмбäди ′сурым.</ta>
            <ta e="T7" id="Seg_437" s="T3">тäбы′ны у′тоɣындъ ыккъ ми′кук.</ta>
            <ta e="T13" id="Seg_438" s="T7">′оkkырың ′сурулʼдʼи ′kӯла kwäннатъ kwäт′ку ′kwäрɣым.</ta>
            <ta e="T16" id="Seg_439" s="T13">ка′нала ′мӯдынʼäттъ ′сурунни</ta>
            <ta e="T23" id="Seg_440" s="T16">′сурум ′ӱтäлʼдиң ка′наланни ′сурулʼдʼи ′kула тʼäт′wаттъ ′сурунни</ta>
            <ta e="T26" id="Seg_441" s="T23">′ассъ шыдың ′на̄рың</ta>
            <ta e="T29" id="Seg_442" s="T26">kwäрɣым, сурум тʼäрытнаттъ [тʼäрытнаттъä].</ta>
            <ta e="T36" id="Seg_443" s="T29">′kwäрɣä ′тʼӱуң и о′ранныт о′kkыр ′сурулʼдʼи kум.</ta>
            <ta e="T47" id="Seg_444" s="T44">‵нышкä′нныт о′лоɣä ′kобым</ta>
            <ta e="T53" id="Seg_445" s="T47">′сурулʼдʼи ′kула тʼä′тшаттъ ′kwäрɣым, kwäрɣä тша′тшʼöлʼдиң.</ta>
            <ta e="T58" id="Seg_446" s="T53">‎‎′kwäрɣын оралбäди ′kум ′kа̄лың ӣллы′лʼе.</ta>
            <ta e="T65" id="Seg_447" s="T58">′тäбым kwäн′даттъ ма′ксы ма′тʼтʼöнда, о′lɣä ′kобымды ′сӱтнаттъ.</ta>
            <ta e="T69" id="Seg_448" s="T65">′тäтта ′сомблʼе ′тʼел иппыс.</ta>
            <ta e="T74" id="Seg_449" s="T69">′нäнна тӱссан ′онды он′дä ǟтоɣондъ</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_450" s="T0">Qwärɣa tʼüumbädi surɨm.</ta>
            <ta e="T7" id="Seg_451" s="T3">Täbɨnɨ utoɣɨndə ɨkkə mikuk.</ta>
            <ta e="T13" id="Seg_452" s="T7">Oqqɨrɨŋ surulʼdʼi quːla qwännatə qwätku qwärɣɨm.</ta>
            <ta e="T16" id="Seg_453" s="T13">kanala muːdɨnʼättə surunni.</ta>
            <ta e="T23" id="Seg_454" s="T16">surum ütälʼdiŋ kanalanni, surulʼdʼi qula tʼätšattə surunni. </ta>
            <ta e="T26" id="Seg_455" s="T23">assə šɨdɨŋ naːrɨŋ.</ta>
            <ta e="T29" id="Seg_456" s="T26">qwärɣɨm, surum tʼärɨtnattə [tʼärɨtnattä]</ta>
            <ta e="T36" id="Seg_457" s="T29">qwärɣa tʼüuŋ i orannɨt oqqɨr surulʼdʼi qum.</ta>
            <ta e="T44" id="Seg_458" s="T36">‎‎qwärɣa qadi udɨn orannɨt oloundə na surulʼdʼi qum</ta>
            <ta e="T47" id="Seg_459" s="T44">nɨškännɨt oloɣä qobɨm.</ta>
            <ta e="T53" id="Seg_460" s="T47">Surulʼdʼi qula tʼätšattə qwärɣɨm, qwärɣa tšatšʼölʼdiŋ.</ta>
            <ta e="T58" id="Seg_461" s="T53">Qwärɣɨn oralbädi qum qaːlɨŋ iːllɨlʼe.</ta>
            <ta e="T65" id="Seg_462" s="T58">täbɨm qwändattə maksɨ matʼtʼönda, oloɣä qobɨmdɨ sütnattə.</ta>
            <ta e="T69" id="Seg_463" s="T65">tätta somblʼe tʼel ippɨs. </ta>
            <ta e="T74" id="Seg_464" s="T69">nänna tüssan ondɨ ondä äːtoɣondə.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_465" s="T0">Qwärɣa tʼüumbädi surɨm. </ta>
            <ta e="T7" id="Seg_466" s="T3">Täbɨnɨ utoɣɨndə ɨkkə mikuk. </ta>
            <ta e="T13" id="Seg_467" s="T7">Oqqɨrɨŋ surulʼdʼi quːla qwännatə qwätku qwärɣɨm. </ta>
            <ta e="T16" id="Seg_468" s="T13">Kanala muːdɨnʼättə surunni. </ta>
            <ta e="T23" id="Seg_469" s="T16">Surum ütälʼdiŋ kanalanni, surulʼdʼi qula tʼätšattə surunni. </ta>
            <ta e="T26" id="Seg_470" s="T23">Assə šɨdɨŋ naːrɨŋ. </ta>
            <ta e="T29" id="Seg_471" s="T26">Qwärɣɨm, surum tʼärɨtnattə. </ta>
            <ta e="T36" id="Seg_472" s="T29">Qwärɣa tʼüuŋ i orannɨt oqqɨr surulʼdʼi qum. </ta>
            <ta e="T44" id="Seg_473" s="T36">Qwärɣa qadi udɨn orannɨt oloundə na surulʼdʼi qum. </ta>
            <ta e="T47" id="Seg_474" s="T44">Nɨškännɨt oloɣä qobɨm. </ta>
            <ta e="T53" id="Seg_475" s="T47">Surulʼdʼi qula tʼäčattə qwärɣɨm, qwärɣa čatšʼölʼdiŋ. </ta>
            <ta e="T58" id="Seg_476" s="T53">Qwärɣɨn oralbädi qum qaːlɨŋ iːllɨlʼe. </ta>
            <ta e="T65" id="Seg_477" s="T58">Täbɨm qwändattə maksɨ matʼtʼönda, oloɣä qobɨmdɨ sütnattə. </ta>
            <ta e="T69" id="Seg_478" s="T65">Tätta somblʼe tʼel ippɨs. </ta>
            <ta e="T74" id="Seg_479" s="T69">Nänna tüssan ondɨ ondä äːtoɣondə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_480" s="T0">qwärɣa</ta>
            <ta e="T2" id="Seg_481" s="T1">tʼüu-mbädi</ta>
            <ta e="T3" id="Seg_482" s="T2">surɨm</ta>
            <ta e="T4" id="Seg_483" s="T3">täb-ɨ-nɨ</ta>
            <ta e="T5" id="Seg_484" s="T4">uto-ɣɨndə</ta>
            <ta e="T6" id="Seg_485" s="T5">ɨkkə</ta>
            <ta e="T7" id="Seg_486" s="T6">mi-ku-k</ta>
            <ta e="T8" id="Seg_487" s="T7">oqqɨr-ɨ-ŋ</ta>
            <ta e="T9" id="Seg_488" s="T8">suru-lʼdʼi</ta>
            <ta e="T10" id="Seg_489" s="T9">quː-la</ta>
            <ta e="T11" id="Seg_490" s="T10">qwän-na-tə</ta>
            <ta e="T12" id="Seg_491" s="T11">qwät-ku</ta>
            <ta e="T13" id="Seg_492" s="T12">qwärɣɨ-m</ta>
            <ta e="T14" id="Seg_493" s="T13">kana-la</ta>
            <ta e="T15" id="Seg_494" s="T14">muːdɨ-nʼä-ttə</ta>
            <ta e="T16" id="Seg_495" s="T15">suru-nni</ta>
            <ta e="T17" id="Seg_496" s="T16">surum</ta>
            <ta e="T18" id="Seg_497" s="T17">ütä-lʼdi-ŋ</ta>
            <ta e="T19" id="Seg_498" s="T18">kana-la-nni</ta>
            <ta e="T20" id="Seg_499" s="T19">suru-lʼdʼi</ta>
            <ta e="T21" id="Seg_500" s="T20">qu-la</ta>
            <ta e="T22" id="Seg_501" s="T21">tʼäča-ttə</ta>
            <ta e="T23" id="Seg_502" s="T22">suru-nni</ta>
            <ta e="T24" id="Seg_503" s="T23">assə</ta>
            <ta e="T25" id="Seg_504" s="T24">šɨdɨ-ŋ</ta>
            <ta e="T26" id="Seg_505" s="T25">naːr-ɨ-ŋ</ta>
            <ta e="T27" id="Seg_506" s="T26">qwärɣɨ-m</ta>
            <ta e="T28" id="Seg_507" s="T27">suru-m</ta>
            <ta e="T29" id="Seg_508" s="T28">tʼä-rɨ-t-na-ttə</ta>
            <ta e="T30" id="Seg_509" s="T29">qwärqa</ta>
            <ta e="T31" id="Seg_510" s="T30">tʼü-u-ŋ</ta>
            <ta e="T32" id="Seg_511" s="T31">i</ta>
            <ta e="T33" id="Seg_512" s="T32">oran-nɨ-t</ta>
            <ta e="T34" id="Seg_513" s="T33">oqqɨr</ta>
            <ta e="T35" id="Seg_514" s="T34">suru-lʼdʼi</ta>
            <ta e="T36" id="Seg_515" s="T35">qum</ta>
            <ta e="T37" id="Seg_516" s="T36">qwärqa</ta>
            <ta e="T38" id="Seg_517" s="T37">qadi</ta>
            <ta e="T39" id="Seg_518" s="T38">udɨ-n</ta>
            <ta e="T40" id="Seg_519" s="T39">oral-nɨ-t</ta>
            <ta e="T41" id="Seg_520" s="T40">olo-un-də</ta>
            <ta e="T42" id="Seg_521" s="T41">na</ta>
            <ta e="T43" id="Seg_522" s="T42">suru-lʼdʼi</ta>
            <ta e="T44" id="Seg_523" s="T43">qum</ta>
            <ta e="T45" id="Seg_524" s="T44">nɨškä-nnɨ-t</ta>
            <ta e="T46" id="Seg_525" s="T45">olo-ɣä</ta>
            <ta e="T47" id="Seg_526" s="T46">qobɨ-m</ta>
            <ta e="T48" id="Seg_527" s="T47">suru-lʼdʼi</ta>
            <ta e="T49" id="Seg_528" s="T48">qu-la</ta>
            <ta e="T50" id="Seg_529" s="T49">tʼäča-ttə</ta>
            <ta e="T51" id="Seg_530" s="T50">qwärɣɨ-m</ta>
            <ta e="T52" id="Seg_531" s="T51">qwärɣa</ta>
            <ta e="T53" id="Seg_532" s="T52">čatšʼö-lʼdi-ŋ</ta>
            <ta e="T54" id="Seg_533" s="T53">qwärɣɨ-n</ta>
            <ta e="T55" id="Seg_534" s="T54">oral-bädi</ta>
            <ta e="T56" id="Seg_535" s="T55">qum</ta>
            <ta e="T57" id="Seg_536" s="T56">qaːlɨ-ŋ</ta>
            <ta e="T58" id="Seg_537" s="T57">iːllɨ-lʼe</ta>
            <ta e="T59" id="Seg_538" s="T58">täb-ɨ-m</ta>
            <ta e="T60" id="Seg_539" s="T59">qwända-ttə</ta>
            <ta e="T61" id="Seg_540" s="T60">maksɨ</ta>
            <ta e="T62" id="Seg_541" s="T61">matʼtʼö-nda</ta>
            <ta e="T63" id="Seg_542" s="T62">olo-ɣä</ta>
            <ta e="T64" id="Seg_543" s="T63">qobɨ-m-dɨ</ta>
            <ta e="T65" id="Seg_544" s="T64">süt-na-ttə</ta>
            <ta e="T66" id="Seg_545" s="T65">tätta</ta>
            <ta e="T67" id="Seg_546" s="T66">somblʼe</ta>
            <ta e="T68" id="Seg_547" s="T67">tʼel</ta>
            <ta e="T69" id="Seg_548" s="T68">ippɨ-s</ta>
            <ta e="T70" id="Seg_549" s="T69">nänna</ta>
            <ta e="T71" id="Seg_550" s="T70">tü-ssa-n</ta>
            <ta e="T72" id="Seg_551" s="T71">ondɨ</ta>
            <ta e="T73" id="Seg_552" s="T72">ondä</ta>
            <ta e="T74" id="Seg_553" s="T73">äːto-ɣondə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_554" s="T0">qwärqa</ta>
            <ta e="T2" id="Seg_555" s="T1">töu-mbɨdi</ta>
            <ta e="T3" id="Seg_556" s="T2">suːrǝm</ta>
            <ta e="T4" id="Seg_557" s="T3">tap-ɨ-nɨ</ta>
            <ta e="T5" id="Seg_558" s="T4">utǝ-ɣonde</ta>
            <ta e="T6" id="Seg_559" s="T5">ɨkɨ</ta>
            <ta e="T7" id="Seg_560" s="T6">mi-kku-k</ta>
            <ta e="T8" id="Seg_561" s="T7">okkɨr-ɨ-k</ta>
            <ta e="T9" id="Seg_562" s="T8">suːrɨ-lʼdi</ta>
            <ta e="T10" id="Seg_563" s="T9">qum-la</ta>
            <ta e="T11" id="Seg_564" s="T10">qwən-ŋɨ-tɨt</ta>
            <ta e="T12" id="Seg_565" s="T11">qwat-gu</ta>
            <ta e="T13" id="Seg_566" s="T12">qwärqa-m</ta>
            <ta e="T14" id="Seg_567" s="T13">kanak-la</ta>
            <ta e="T15" id="Seg_568" s="T14">mudɨ-ŋɨ-tɨt</ta>
            <ta e="T16" id="Seg_569" s="T15">suːrǝm-nɨ</ta>
            <ta e="T17" id="Seg_570" s="T16">suːrǝm</ta>
            <ta e="T18" id="Seg_571" s="T17">üːtǝ-lʼčǝ-ŋ</ta>
            <ta e="T19" id="Seg_572" s="T18">kanak-la-nɨ</ta>
            <ta e="T20" id="Seg_573" s="T19">suːrɨ-lʼdi</ta>
            <ta e="T21" id="Seg_574" s="T20">qum-la</ta>
            <ta e="T22" id="Seg_575" s="T21">tʼaǯe-tɨt</ta>
            <ta e="T23" id="Seg_576" s="T22">suːrǝm-nɨ</ta>
            <ta e="T24" id="Seg_577" s="T23">ašša</ta>
            <ta e="T25" id="Seg_578" s="T24">šittə-k</ta>
            <ta e="T26" id="Seg_579" s="T25">nakkɨr-ɨ-k</ta>
            <ta e="T27" id="Seg_580" s="T26">qwärqa-m</ta>
            <ta e="T28" id="Seg_581" s="T27">suːrǝm-m</ta>
            <ta e="T29" id="Seg_582" s="T28">tʼabə-rɨ-ntɨ-ŋɨ-tɨt</ta>
            <ta e="T30" id="Seg_583" s="T29">qwärqa</ta>
            <ta e="T31" id="Seg_584" s="T30">töu-ŋɨ-ŋ</ta>
            <ta e="T32" id="Seg_585" s="T31">i</ta>
            <ta e="T33" id="Seg_586" s="T32">oral-ŋɨ-tɨ</ta>
            <ta e="T34" id="Seg_587" s="T33">okkɨr</ta>
            <ta e="T35" id="Seg_588" s="T34">suːrɨ-lʼdi</ta>
            <ta e="T36" id="Seg_589" s="T35">qum</ta>
            <ta e="T37" id="Seg_590" s="T36">qwärqa</ta>
            <ta e="T38" id="Seg_591" s="T37">qattə</ta>
            <ta e="T39" id="Seg_592" s="T38">utǝ-n</ta>
            <ta e="T40" id="Seg_593" s="T39">oral-ŋɨ-tɨ</ta>
            <ta e="T41" id="Seg_594" s="T40">olɨ-un-t</ta>
            <ta e="T42" id="Seg_595" s="T41">na</ta>
            <ta e="T43" id="Seg_596" s="T42">suːrɨ-lʼdi</ta>
            <ta e="T44" id="Seg_597" s="T43">qum</ta>
            <ta e="T45" id="Seg_598" s="T44">nɨškä-ŋɨ-tɨ</ta>
            <ta e="T46" id="Seg_599" s="T45">olɨ-ka</ta>
            <ta e="T47" id="Seg_600" s="T46">qobɨ-m</ta>
            <ta e="T48" id="Seg_601" s="T47">suːrɨ-lʼdi</ta>
            <ta e="T49" id="Seg_602" s="T48">qum-la</ta>
            <ta e="T50" id="Seg_603" s="T49">tʼaǯe-tɨt</ta>
            <ta e="T51" id="Seg_604" s="T50">qwärqa-m</ta>
            <ta e="T52" id="Seg_605" s="T51">qwärqa</ta>
            <ta e="T53" id="Seg_606" s="T52">čäčo-lʼčǝ-ŋ</ta>
            <ta e="T54" id="Seg_607" s="T53">qwärqa-n</ta>
            <ta e="T55" id="Seg_608" s="T54">oral-mbɨdi</ta>
            <ta e="T56" id="Seg_609" s="T55">qum</ta>
            <ta e="T57" id="Seg_610" s="T56">qalɨ-ŋ</ta>
            <ta e="T58" id="Seg_611" s="T57">ilɨ-le</ta>
            <ta e="T59" id="Seg_612" s="T58">tap-ɨ-m</ta>
            <ta e="T60" id="Seg_613" s="T59">qwandɛ-tɨt</ta>
            <ta e="T61" id="Seg_614" s="T60">maksɨ</ta>
            <ta e="T62" id="Seg_615" s="T61">maǯə-ndɨ</ta>
            <ta e="T63" id="Seg_616" s="T62">olɨ-ka</ta>
            <ta e="T64" id="Seg_617" s="T63">qobɨ-m-t</ta>
            <ta e="T65" id="Seg_618" s="T64">šüt-ŋɨ-tɨt</ta>
            <ta e="T66" id="Seg_619" s="T65">tättɨ</ta>
            <ta e="T67" id="Seg_620" s="T66">sombɨlʼe</ta>
            <ta e="T68" id="Seg_621" s="T67">tʼeːlɨ</ta>
            <ta e="T69" id="Seg_622" s="T68">ippi-sɨ</ta>
            <ta e="T70" id="Seg_623" s="T69">nɨːnɨ</ta>
            <ta e="T71" id="Seg_624" s="T70">tüː-sɨ-ŋ</ta>
            <ta e="T72" id="Seg_625" s="T71">ontɨ</ta>
            <ta e="T73" id="Seg_626" s="T72">ontɨ</ta>
            <ta e="T74" id="Seg_627" s="T73">eːtə-qɨntɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_628" s="T0">bear.[NOM]</ta>
            <ta e="T2" id="Seg_629" s="T1">get.angry-PTCP.PST</ta>
            <ta e="T3" id="Seg_630" s="T2">wild.animal.[NOM]</ta>
            <ta e="T4" id="Seg_631" s="T3">he.NOM-EP-ALL</ta>
            <ta e="T5" id="Seg_632" s="T4">hand-3SG.ILL</ta>
            <ta e="T6" id="Seg_633" s="T5">NEG.IMP</ta>
            <ta e="T7" id="Seg_634" s="T6">give-DUR-IMP.2SG.S</ta>
            <ta e="T8" id="Seg_635" s="T7">one-EP-ADVZ</ta>
            <ta e="T9" id="Seg_636" s="T8">hunt-PTCP.PRS</ta>
            <ta e="T10" id="Seg_637" s="T9">human.being-PL.[NOM]</ta>
            <ta e="T11" id="Seg_638" s="T10">go.away-CO-3PL</ta>
            <ta e="T12" id="Seg_639" s="T11">kill-INF</ta>
            <ta e="T13" id="Seg_640" s="T12">bear-ACC</ta>
            <ta e="T14" id="Seg_641" s="T13">dog-PL.[NOM]</ta>
            <ta e="T15" id="Seg_642" s="T14">bark-CO-3PL</ta>
            <ta e="T16" id="Seg_643" s="T15">wild.animal-ALL</ta>
            <ta e="T17" id="Seg_644" s="T16">wild.animal.[NOM]</ta>
            <ta e="T18" id="Seg_645" s="T17">send-PFV-3SG.S</ta>
            <ta e="T19" id="Seg_646" s="T18">dog-PL-ALL</ta>
            <ta e="T20" id="Seg_647" s="T19">hunt-PTCP.PRS</ta>
            <ta e="T21" id="Seg_648" s="T20">human.being-PL.[NOM]</ta>
            <ta e="T22" id="Seg_649" s="T21">shoot-3PL</ta>
            <ta e="T23" id="Seg_650" s="T22">wild.animal-ALL</ta>
            <ta e="T24" id="Seg_651" s="T23">NEG</ta>
            <ta e="T25" id="Seg_652" s="T24">two-ADVZ</ta>
            <ta e="T26" id="Seg_653" s="T25">three-EP-ADVZ</ta>
            <ta e="T27" id="Seg_654" s="T26">bear-ACC</ta>
            <ta e="T28" id="Seg_655" s="T27">wild.animal-ACC</ta>
            <ta e="T29" id="Seg_656" s="T28">catch-CAUS-IPFV-CO-3PL</ta>
            <ta e="T30" id="Seg_657" s="T29">bear.[NOM]</ta>
            <ta e="T31" id="Seg_658" s="T30">get.angry-CO-3SG.S</ta>
            <ta e="T32" id="Seg_659" s="T31">and</ta>
            <ta e="T33" id="Seg_660" s="T32">catch-CO-3SG.O</ta>
            <ta e="T34" id="Seg_661" s="T33">one</ta>
            <ta e="T35" id="Seg_662" s="T34">hunt-PTCP.PRS</ta>
            <ta e="T36" id="Seg_663" s="T35">human.being.[NOM]</ta>
            <ta e="T37" id="Seg_664" s="T36">bear.[NOM]</ta>
            <ta e="T38" id="Seg_665" s="T37">claw.[NOM]</ta>
            <ta e="T39" id="Seg_666" s="T38">hand-GEN</ta>
            <ta e="T40" id="Seg_667" s="T39">catch-CO-3SG.O</ta>
            <ta e="T41" id="Seg_668" s="T40">head-PROL-3SG</ta>
            <ta e="T42" id="Seg_669" s="T41">this</ta>
            <ta e="T43" id="Seg_670" s="T42">hunt-PTCP.PRS</ta>
            <ta e="T44" id="Seg_671" s="T43">human.being.[NOM]</ta>
            <ta e="T45" id="Seg_672" s="T44">tear-CO-3SG.O</ta>
            <ta e="T46" id="Seg_673" s="T45">head-DIM.[NOM]</ta>
            <ta e="T47" id="Seg_674" s="T46">skin-ACC</ta>
            <ta e="T48" id="Seg_675" s="T47">hunt-PTCP.PRS</ta>
            <ta e="T49" id="Seg_676" s="T48">human.being-PL.[NOM]</ta>
            <ta e="T50" id="Seg_677" s="T49">shoot-3PL</ta>
            <ta e="T51" id="Seg_678" s="T50">bear-ACC</ta>
            <ta e="T52" id="Seg_679" s="T51">bear.[NOM]</ta>
            <ta e="T53" id="Seg_680" s="T52">fall-PFV-3SG.S</ta>
            <ta e="T54" id="Seg_681" s="T53">bear-GEN</ta>
            <ta e="T55" id="Seg_682" s="T54">catch-PTCP.PST</ta>
            <ta e="T56" id="Seg_683" s="T55">human.being.[NOM]</ta>
            <ta e="T57" id="Seg_684" s="T56">stay-3SG.S</ta>
            <ta e="T58" id="Seg_685" s="T57">live-CVB</ta>
            <ta e="T59" id="Seg_686" s="T58">he.NOM-EP-ACC</ta>
            <ta e="T60" id="Seg_687" s="T59">carry.away-3PL</ta>
            <ta e="T61" id="Seg_688" s="T60">Maksymkiy.Yar</ta>
            <ta e="T62" id="Seg_689" s="T61">taiga-ILL</ta>
            <ta e="T63" id="Seg_690" s="T62">head-ADJZ.[NOM]</ta>
            <ta e="T64" id="Seg_691" s="T63">skin-ACC-3SG</ta>
            <ta e="T65" id="Seg_692" s="T64">sew-CO-3PL</ta>
            <ta e="T66" id="Seg_693" s="T65">four</ta>
            <ta e="T67" id="Seg_694" s="T66">five</ta>
            <ta e="T68" id="Seg_695" s="T67">day.[NOM]</ta>
            <ta e="T69" id="Seg_696" s="T68">lie-PST.[3SG.S]</ta>
            <ta e="T70" id="Seg_697" s="T69">then</ta>
            <ta e="T71" id="Seg_698" s="T70">come-PST-3SG.S</ta>
            <ta e="T72" id="Seg_699" s="T71">himself</ta>
            <ta e="T73" id="Seg_700" s="T72">himself</ta>
            <ta e="T74" id="Seg_701" s="T73">village-3SG.ILL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_702" s="T0">медведь.[NOM]</ta>
            <ta e="T2" id="Seg_703" s="T1">рассердиться-PTCP.PST</ta>
            <ta e="T3" id="Seg_704" s="T2">зверь.[NOM]</ta>
            <ta e="T4" id="Seg_705" s="T3">он.NOM-EP-ALL</ta>
            <ta e="T5" id="Seg_706" s="T4">рука-3SG.ILL</ta>
            <ta e="T6" id="Seg_707" s="T5">NEG.IMP</ta>
            <ta e="T7" id="Seg_708" s="T6">дать-DUR-IMP.2SG.S</ta>
            <ta e="T8" id="Seg_709" s="T7">один-EP-ADVZ</ta>
            <ta e="T9" id="Seg_710" s="T8">охотиться-PTCP.PRS</ta>
            <ta e="T10" id="Seg_711" s="T9">человек-PL.[NOM]</ta>
            <ta e="T11" id="Seg_712" s="T10">уйти-CO-3PL</ta>
            <ta e="T12" id="Seg_713" s="T11">убить-INF</ta>
            <ta e="T13" id="Seg_714" s="T12">медведь-ACC</ta>
            <ta e="T14" id="Seg_715" s="T13">собака-PL.[NOM]</ta>
            <ta e="T15" id="Seg_716" s="T14">лаять-CO-3PL</ta>
            <ta e="T16" id="Seg_717" s="T15">зверь-ALL</ta>
            <ta e="T17" id="Seg_718" s="T16">зверь.[NOM]</ta>
            <ta e="T18" id="Seg_719" s="T17">посылать-PFV-3SG.S</ta>
            <ta e="T19" id="Seg_720" s="T18">собака-PL-ALL</ta>
            <ta e="T20" id="Seg_721" s="T19">охотиться-PTCP.PRS</ta>
            <ta e="T21" id="Seg_722" s="T20">человек-PL.[NOM]</ta>
            <ta e="T22" id="Seg_723" s="T21">стрелять-3PL</ta>
            <ta e="T23" id="Seg_724" s="T22">зверь-ALL</ta>
            <ta e="T24" id="Seg_725" s="T23">NEG</ta>
            <ta e="T25" id="Seg_726" s="T24">два-ADVZ</ta>
            <ta e="T26" id="Seg_727" s="T25">три-EP-ADVZ</ta>
            <ta e="T27" id="Seg_728" s="T26">медведь-ACC</ta>
            <ta e="T28" id="Seg_729" s="T27">зверь-ACC</ta>
            <ta e="T29" id="Seg_730" s="T28">поймать-CAUS-IPFV-CO-3PL</ta>
            <ta e="T30" id="Seg_731" s="T29">медведь.[NOM]</ta>
            <ta e="T31" id="Seg_732" s="T30">рассердиться-CO-3SG.S</ta>
            <ta e="T32" id="Seg_733" s="T31">и</ta>
            <ta e="T33" id="Seg_734" s="T32">поймать-CO-3SG.O</ta>
            <ta e="T34" id="Seg_735" s="T33">один</ta>
            <ta e="T35" id="Seg_736" s="T34">охотиться-PTCP.PRS</ta>
            <ta e="T36" id="Seg_737" s="T35">человек.[NOM]</ta>
            <ta e="T37" id="Seg_738" s="T36">медведь.[NOM]</ta>
            <ta e="T38" id="Seg_739" s="T37">коготь.[NOM]</ta>
            <ta e="T39" id="Seg_740" s="T38">рука-GEN</ta>
            <ta e="T40" id="Seg_741" s="T39">поймать-CO-3SG.O</ta>
            <ta e="T41" id="Seg_742" s="T40">голова-PROL-3SG</ta>
            <ta e="T42" id="Seg_743" s="T41">этот</ta>
            <ta e="T43" id="Seg_744" s="T42">охотиться-PTCP.PRS</ta>
            <ta e="T44" id="Seg_745" s="T43">человек.[NOM]</ta>
            <ta e="T45" id="Seg_746" s="T44">сорвать-CO-3SG.O</ta>
            <ta e="T46" id="Seg_747" s="T45">голова-DIM.[NOM]</ta>
            <ta e="T47" id="Seg_748" s="T46">шкура-ACC</ta>
            <ta e="T48" id="Seg_749" s="T47">охотиться-PTCP.PRS</ta>
            <ta e="T49" id="Seg_750" s="T48">человек-PL.[NOM]</ta>
            <ta e="T50" id="Seg_751" s="T49">стрелять-3PL</ta>
            <ta e="T51" id="Seg_752" s="T50">медведь-ACC</ta>
            <ta e="T52" id="Seg_753" s="T51">медведь.[NOM]</ta>
            <ta e="T53" id="Seg_754" s="T52">упасть-PFV-3SG.S</ta>
            <ta e="T54" id="Seg_755" s="T53">медведь-GEN</ta>
            <ta e="T55" id="Seg_756" s="T54">поймать-PTCP.PST</ta>
            <ta e="T56" id="Seg_757" s="T55">человек.[NOM]</ta>
            <ta e="T57" id="Seg_758" s="T56">остаться-3SG.S</ta>
            <ta e="T58" id="Seg_759" s="T57">жить-CVB</ta>
            <ta e="T59" id="Seg_760" s="T58">он.NOM-EP-ACC</ta>
            <ta e="T60" id="Seg_761" s="T59">отнести-3PL</ta>
            <ta e="T61" id="Seg_762" s="T60">Максымкий.Яр</ta>
            <ta e="T62" id="Seg_763" s="T61">тайга-ILL</ta>
            <ta e="T63" id="Seg_764" s="T62">голова-ADJZ.[NOM]</ta>
            <ta e="T64" id="Seg_765" s="T63">шкура-ACC-3SG</ta>
            <ta e="T65" id="Seg_766" s="T64">сшить-CO-3PL</ta>
            <ta e="T66" id="Seg_767" s="T65">четыре</ta>
            <ta e="T67" id="Seg_768" s="T66">пять</ta>
            <ta e="T68" id="Seg_769" s="T67">день.[NOM]</ta>
            <ta e="T69" id="Seg_770" s="T68">лежать-PST.[3SG.S]</ta>
            <ta e="T70" id="Seg_771" s="T69">потом</ta>
            <ta e="T71" id="Seg_772" s="T70">прийти-PST-3SG.S</ta>
            <ta e="T72" id="Seg_773" s="T71">он.сам</ta>
            <ta e="T73" id="Seg_774" s="T72">он.сам</ta>
            <ta e="T74" id="Seg_775" s="T73">деревня-3SG.ILL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_776" s="T0">n-n:case1</ta>
            <ta e="T2" id="Seg_777" s="T1">v-v&gt;ptcp</ta>
            <ta e="T3" id="Seg_778" s="T2">n-n:case1</ta>
            <ta e="T4" id="Seg_779" s="T3">pers-n:ins-n:case3</ta>
            <ta e="T5" id="Seg_780" s="T4">n-n:poss-case</ta>
            <ta e="T6" id="Seg_781" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_782" s="T6">v-v&gt;v-v:mood-pn</ta>
            <ta e="T8" id="Seg_783" s="T7">num-n:ins-adj&gt;adv</ta>
            <ta e="T9" id="Seg_784" s="T8">v-v&gt;ptcp</ta>
            <ta e="T10" id="Seg_785" s="T9">n-n:num-n:case1</ta>
            <ta e="T11" id="Seg_786" s="T10">v-v:ins-v:pn</ta>
            <ta e="T12" id="Seg_787" s="T11">v-v:inf</ta>
            <ta e="T13" id="Seg_788" s="T12">n-n:case1</ta>
            <ta e="T14" id="Seg_789" s="T13">n-n:num-n:case1</ta>
            <ta e="T15" id="Seg_790" s="T14">v-v:ins-v:pn</ta>
            <ta e="T16" id="Seg_791" s="T15">n-n:case2</ta>
            <ta e="T17" id="Seg_792" s="T16">n-n:case1</ta>
            <ta e="T18" id="Seg_793" s="T17">v-v&gt;v-v:pn</ta>
            <ta e="T19" id="Seg_794" s="T18">n-n:num-n:case2</ta>
            <ta e="T20" id="Seg_795" s="T19">v-v&gt;ptcp</ta>
            <ta e="T21" id="Seg_796" s="T20">n-n:num-n:case1</ta>
            <ta e="T22" id="Seg_797" s="T21">v-v:pn</ta>
            <ta e="T23" id="Seg_798" s="T22">n-n:case2</ta>
            <ta e="T24" id="Seg_799" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_800" s="T24">num-adj&gt;adv</ta>
            <ta e="T26" id="Seg_801" s="T25">num-n:ins-adj&gt;adv</ta>
            <ta e="T27" id="Seg_802" s="T26">n-n:case1</ta>
            <ta e="T28" id="Seg_803" s="T27">n-n:case1</ta>
            <ta e="T29" id="Seg_804" s="T28">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T30" id="Seg_805" s="T29">n-n:case1</ta>
            <ta e="T31" id="Seg_806" s="T30">v-v:ins-v:pn</ta>
            <ta e="T32" id="Seg_807" s="T31">conj</ta>
            <ta e="T33" id="Seg_808" s="T32">v-v:ins-v:pn</ta>
            <ta e="T34" id="Seg_809" s="T33">num</ta>
            <ta e="T35" id="Seg_810" s="T34">v-v&gt;ptcp</ta>
            <ta e="T36" id="Seg_811" s="T35">n-n:case1</ta>
            <ta e="T37" id="Seg_812" s="T36">n-n:case1</ta>
            <ta e="T38" id="Seg_813" s="T37">n-n:case1</ta>
            <ta e="T39" id="Seg_814" s="T38">n-n:case1</ta>
            <ta e="T40" id="Seg_815" s="T39">v-v:ins-v:pn</ta>
            <ta e="T41" id="Seg_816" s="T40">n-n:case2-n:poss</ta>
            <ta e="T42" id="Seg_817" s="T41">dem</ta>
            <ta e="T43" id="Seg_818" s="T42">v-v&gt;ptcp</ta>
            <ta e="T44" id="Seg_819" s="T43">n-n:case1</ta>
            <ta e="T45" id="Seg_820" s="T44">v-v:ins-v:pn</ta>
            <ta e="T46" id="Seg_821" s="T45">n-n&gt;n-n:case1</ta>
            <ta e="T47" id="Seg_822" s="T46">n-n:case1</ta>
            <ta e="T48" id="Seg_823" s="T47">v-v&gt;ptcp</ta>
            <ta e="T49" id="Seg_824" s="T48">n-n:num-n:case1</ta>
            <ta e="T50" id="Seg_825" s="T49">v-v:pn</ta>
            <ta e="T51" id="Seg_826" s="T50">n-n:case1</ta>
            <ta e="T52" id="Seg_827" s="T51">n-n:case1</ta>
            <ta e="T53" id="Seg_828" s="T52">v-v&gt;v-v:pn</ta>
            <ta e="T54" id="Seg_829" s="T53">n-n:case1</ta>
            <ta e="T55" id="Seg_830" s="T54">v-v&gt;ptcp</ta>
            <ta e="T56" id="Seg_831" s="T55">n-n:case1</ta>
            <ta e="T57" id="Seg_832" s="T56">v-v:pn</ta>
            <ta e="T58" id="Seg_833" s="T57">v-v&gt;adv</ta>
            <ta e="T59" id="Seg_834" s="T58">pers-n:ins-n:case1</ta>
            <ta e="T60" id="Seg_835" s="T59">v-v:pn</ta>
            <ta e="T61" id="Seg_836" s="T60">nprop</ta>
            <ta e="T62" id="Seg_837" s="T61">n-n:case2</ta>
            <ta e="T63" id="Seg_838" s="T62">n-n&gt;adj-n:case1</ta>
            <ta e="T64" id="Seg_839" s="T63">n-n:case1-n:poss</ta>
            <ta e="T65" id="Seg_840" s="T64">v-v:ins-v:pn</ta>
            <ta e="T66" id="Seg_841" s="T65">num</ta>
            <ta e="T67" id="Seg_842" s="T66">num</ta>
            <ta e="T68" id="Seg_843" s="T67">n-n:case1</ta>
            <ta e="T69" id="Seg_844" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_845" s="T69">adv</ta>
            <ta e="T71" id="Seg_846" s="T70">v:tense-v:pn</ta>
            <ta e="T72" id="Seg_847" s="T71">pro</ta>
            <ta e="T73" id="Seg_848" s="T72">pro</ta>
            <ta e="T74" id="Seg_849" s="T73">n-n:poss-case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_850" s="T0">n</ta>
            <ta e="T2" id="Seg_851" s="T1">ptcp</ta>
            <ta e="T3" id="Seg_852" s="T2">n</ta>
            <ta e="T4" id="Seg_853" s="T3">pers</ta>
            <ta e="T5" id="Seg_854" s="T4">n</ta>
            <ta e="T6" id="Seg_855" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_856" s="T6">v</ta>
            <ta e="T8" id="Seg_857" s="T7">num</ta>
            <ta e="T9" id="Seg_858" s="T8">ptcp</ta>
            <ta e="T10" id="Seg_859" s="T9">n</ta>
            <ta e="T11" id="Seg_860" s="T10">v</ta>
            <ta e="T12" id="Seg_861" s="T11">v</ta>
            <ta e="T13" id="Seg_862" s="T12">n</ta>
            <ta e="T14" id="Seg_863" s="T13">n</ta>
            <ta e="T15" id="Seg_864" s="T14">v</ta>
            <ta e="T16" id="Seg_865" s="T15">n</ta>
            <ta e="T17" id="Seg_866" s="T16">n</ta>
            <ta e="T18" id="Seg_867" s="T17">v</ta>
            <ta e="T19" id="Seg_868" s="T18">n</ta>
            <ta e="T20" id="Seg_869" s="T19">ptcp</ta>
            <ta e="T21" id="Seg_870" s="T20">n</ta>
            <ta e="T22" id="Seg_871" s="T21">v</ta>
            <ta e="T23" id="Seg_872" s="T22">n</ta>
            <ta e="T24" id="Seg_873" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_874" s="T24">adv</ta>
            <ta e="T26" id="Seg_875" s="T25">adv</ta>
            <ta e="T27" id="Seg_876" s="T26">n</ta>
            <ta e="T28" id="Seg_877" s="T27">n</ta>
            <ta e="T29" id="Seg_878" s="T28">v</ta>
            <ta e="T30" id="Seg_879" s="T29">n</ta>
            <ta e="T31" id="Seg_880" s="T30">v</ta>
            <ta e="T32" id="Seg_881" s="T31">conj</ta>
            <ta e="T33" id="Seg_882" s="T32">v</ta>
            <ta e="T34" id="Seg_883" s="T33">num</ta>
            <ta e="T35" id="Seg_884" s="T34">ptcp</ta>
            <ta e="T36" id="Seg_885" s="T35">n</ta>
            <ta e="T37" id="Seg_886" s="T36">n</ta>
            <ta e="T38" id="Seg_887" s="T37">n</ta>
            <ta e="T39" id="Seg_888" s="T38">n</ta>
            <ta e="T40" id="Seg_889" s="T39">v</ta>
            <ta e="T41" id="Seg_890" s="T40">n</ta>
            <ta e="T42" id="Seg_891" s="T41">dem</ta>
            <ta e="T43" id="Seg_892" s="T42">ptcp</ta>
            <ta e="T44" id="Seg_893" s="T43">n</ta>
            <ta e="T45" id="Seg_894" s="T44">v</ta>
            <ta e="T46" id="Seg_895" s="T45">n</ta>
            <ta e="T47" id="Seg_896" s="T46">n</ta>
            <ta e="T48" id="Seg_897" s="T47">ptcp</ta>
            <ta e="T49" id="Seg_898" s="T48">n</ta>
            <ta e="T50" id="Seg_899" s="T49">v</ta>
            <ta e="T51" id="Seg_900" s="T50">n</ta>
            <ta e="T52" id="Seg_901" s="T51">n</ta>
            <ta e="T53" id="Seg_902" s="T52">v</ta>
            <ta e="T54" id="Seg_903" s="T53">n</ta>
            <ta e="T55" id="Seg_904" s="T54">ptcp</ta>
            <ta e="T56" id="Seg_905" s="T55">n</ta>
            <ta e="T57" id="Seg_906" s="T56">v</ta>
            <ta e="T58" id="Seg_907" s="T57">adv</ta>
            <ta e="T59" id="Seg_908" s="T58">pers</ta>
            <ta e="T60" id="Seg_909" s="T59">v</ta>
            <ta e="T61" id="Seg_910" s="T60">nprop</ta>
            <ta e="T62" id="Seg_911" s="T61">n</ta>
            <ta e="T63" id="Seg_912" s="T62">n</ta>
            <ta e="T64" id="Seg_913" s="T63">n</ta>
            <ta e="T66" id="Seg_914" s="T65">num</ta>
            <ta e="T67" id="Seg_915" s="T66">num</ta>
            <ta e="T68" id="Seg_916" s="T67">n</ta>
            <ta e="T69" id="Seg_917" s="T68">v</ta>
            <ta e="T70" id="Seg_918" s="T69">adv</ta>
            <ta e="T71" id="Seg_919" s="T70">v</ta>
            <ta e="T72" id="Seg_920" s="T71">pro</ta>
            <ta e="T73" id="Seg_921" s="T72">emphpers</ta>
            <ta e="T74" id="Seg_922" s="T73">n</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_923" s="T0">np:S</ta>
            <ta e="T3" id="Seg_924" s="T2">n:pred</ta>
            <ta e="T7" id="Seg_925" s="T6">0.2.h:S v:pred</ta>
            <ta e="T10" id="Seg_926" s="T9">np.h:S</ta>
            <ta e="T11" id="Seg_927" s="T10">v:pred</ta>
            <ta e="T13" id="Seg_928" s="T11">s:purp</ta>
            <ta e="T14" id="Seg_929" s="T13">np:S</ta>
            <ta e="T15" id="Seg_930" s="T14">v:pred</ta>
            <ta e="T17" id="Seg_931" s="T16">np:S</ta>
            <ta e="T18" id="Seg_932" s="T17">v:pred</ta>
            <ta e="T21" id="Seg_933" s="T20">np.h:S</ta>
            <ta e="T22" id="Seg_934" s="T21">v:pred</ta>
            <ta e="T27" id="Seg_935" s="T26">np:O</ta>
            <ta e="T28" id="Seg_936" s="T27">np:O</ta>
            <ta e="T29" id="Seg_937" s="T28">0.3.h:S</ta>
            <ta e="T30" id="Seg_938" s="T29">np:S</ta>
            <ta e="T31" id="Seg_939" s="T30">v:pred</ta>
            <ta e="T33" id="Seg_940" s="T32">0.3:S v:pred</ta>
            <ta e="T36" id="Seg_941" s="T35">np.h:O</ta>
            <ta e="T37" id="Seg_942" s="T36">np:S</ta>
            <ta e="T40" id="Seg_943" s="T39">v:pred</ta>
            <ta e="T45" id="Seg_944" s="T44">np:S v:pred</ta>
            <ta e="T47" id="Seg_945" s="T46">np:O</ta>
            <ta e="T49" id="Seg_946" s="T48">np.h:S</ta>
            <ta e="T50" id="Seg_947" s="T49">v:pred</ta>
            <ta e="T51" id="Seg_948" s="T50">np:O</ta>
            <ta e="T52" id="Seg_949" s="T51">np:S</ta>
            <ta e="T53" id="Seg_950" s="T52">v:pred</ta>
            <ta e="T55" id="Seg_951" s="T53">s:rel</ta>
            <ta e="T56" id="Seg_952" s="T55">np.h:S</ta>
            <ta e="T57" id="Seg_953" s="T56">v:pred</ta>
            <ta e="T59" id="Seg_954" s="T58">pro.h:O</ta>
            <ta e="T60" id="Seg_955" s="T59">0.3.h:S v:pred</ta>
            <ta e="T64" id="Seg_956" s="T63">np:O</ta>
            <ta e="T65" id="Seg_957" s="T64">0.3.h:S v:pred</ta>
            <ta e="T69" id="Seg_958" s="T68">0.3.h:S v:pred</ta>
            <ta e="T71" id="Seg_959" s="T70">v:pred</ta>
            <ta e="T72" id="Seg_960" s="T71">pro.h:S</ta>
            <ta e="T73" id="Seg_961" s="T72">pro.h:S</ta>
            <ta e="T74" id="Seg_962" s="T73">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_963" s="T0">np:Th</ta>
            <ta e="T5" id="Seg_964" s="T4">np:G</ta>
            <ta e="T7" id="Seg_965" s="T6">0.2.h:Th</ta>
            <ta e="T10" id="Seg_966" s="T9">np.h:A</ta>
            <ta e="T13" id="Seg_967" s="T12">np:P</ta>
            <ta e="T14" id="Seg_968" s="T13">np:A</ta>
            <ta e="T27" id="Seg_969" s="T26">np:P</ta>
            <ta e="T28" id="Seg_970" s="T27">np:P</ta>
            <ta e="T29" id="Seg_971" s="T28">0.3.h:A</ta>
            <ta e="T30" id="Seg_972" s="T29">np:E</ta>
            <ta e="T33" id="Seg_973" s="T32">0.3:A</ta>
            <ta e="T36" id="Seg_974" s="T35">np.h:P</ta>
            <ta e="T37" id="Seg_975" s="T36">np:A</ta>
            <ta e="T39" id="Seg_976" s="T37">np:Ins</ta>
            <ta e="T41" id="Seg_977" s="T40">np:Path 0.3.h:Poss</ta>
            <ta e="T45" id="Seg_978" s="T44">np:A</ta>
            <ta e="T47" id="Seg_979" s="T46">np:P</ta>
            <ta e="T49" id="Seg_980" s="T48">np.h:A</ta>
            <ta e="T52" id="Seg_981" s="T51">np:Th</ta>
            <ta e="T56" id="Seg_982" s="T55">np.h:Th</ta>
            <ta e="T59" id="Seg_983" s="T58">pro.h:P</ta>
            <ta e="T60" id="Seg_984" s="T59">0.3.h:A</ta>
            <ta e="T62" id="Seg_985" s="T61">np:G</ta>
            <ta e="T64" id="Seg_986" s="T63">np:P</ta>
            <ta e="T65" id="Seg_987" s="T64">0.3.h:A</ta>
            <ta e="T69" id="Seg_988" s="T68">0.3.h:Th</ta>
            <ta e="T72" id="Seg_989" s="T71">pro.h:A</ta>
            <ta e="T73" id="Seg_990" s="T72">pro.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T32" id="Seg_991" s="T31">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_992" s="T0">Медведь зверь сердитый. </ta>
            <ta e="T7" id="Seg_993" s="T3">Ему в руки не давайся. </ta>
            <ta e="T13" id="Seg_994" s="T7">Однажды охотники пошли убить медведя.</ta>
            <ta e="T16" id="Seg_995" s="T13">‎‎Собаки залаяли на медведя.</ta>
            <ta e="T23" id="Seg_996" s="T16">Медведь побежал с собаками, охотники стреляли в медведя. </ta>
            <ta e="T26" id="Seg_997" s="T23">Не два, три раза.</ta>
            <ta e="T29" id="Seg_998" s="T26">Попали в медведя.</ta>
            <ta e="T36" id="Seg_999" s="T29">Медведь рассвирепел, поймал одного охотника.</ta>
            <ta e="T44" id="Seg_1000" s="T36">Kогтистыми руками схватил за голову охотника.</ta>
            <ta e="T47" id="Seg_1001" s="T44">Ободрал с головы кожу.</ta>
            <ta e="T53" id="Seg_1002" s="T47">Охотники выстрелили в медведя, медведь упал.</ta>
            <ta e="T58" id="Seg_1003" s="T53">Пойманный медведем человек остался жив.</ta>
            <ta e="T65" id="Seg_1004" s="T58">Его увезли в Максимкин Яр, кожу на голове сшили.</ta>
            <ta e="T69" id="Seg_1005" s="T65">Четыре, пять дней лежал.</ta>
            <ta e="T74" id="Seg_1006" s="T69">Потом сам приехал в свою деревню.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_1007" s="T0">The bear is an evil animal.</ta>
            <ta e="T7" id="Seg_1008" s="T3">Don't fall into its clutches!</ta>
            <ta e="T13" id="Seg_1009" s="T7">One day the hunters go to kill a bear.</ta>
            <ta e="T16" id="Seg_1010" s="T13">The dogs bark at the wild animal.</ta>
            <ta e="T23" id="Seg_1011" s="T16">The bear ran to the dogs, the hunters shoot at the bear.</ta>
            <ta e="T26" id="Seg_1012" s="T23">Not twice, thrice.</ta>
            <ta e="T29" id="Seg_1013" s="T26">They catch the bear.</ta>
            <ta e="T36" id="Seg_1014" s="T29">The bear gets angry and captivates a hunter.</ta>
            <ta e="T44" id="Seg_1015" s="T36">The bear holds the head of this hunter with his claws.</ta>
            <ta e="T47" id="Seg_1016" s="T44">He tears the scalp.</ta>
            <ta e="T53" id="Seg_1017" s="T47">The hunters shoot at the bear, the bear fell.</ta>
            <ta e="T58" id="Seg_1018" s="T53">The man, who got caught by the bear, stays alive.</ta>
            <ta e="T65" id="Seg_1019" s="T58">They bring him to Maksimkin Yar, they stich the scalp.</ta>
            <ta e="T69" id="Seg_1020" s="T65">For four, five days he lay down.</ta>
            <ta e="T74" id="Seg_1021" s="T69">Then he came to his own village.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_1022" s="T0">Der Bär ist ein böses Tier.</ta>
            <ta e="T7" id="Seg_1023" s="T3">Falle ihm nicht in die Hände!</ta>
            <ta e="T13" id="Seg_1024" s="T7">Einmal gingen die Jäger einen Bären töten.</ta>
            <ta e="T16" id="Seg_1025" s="T13">Die Hunde bellten den Bären an.</ta>
            <ta e="T23" id="Seg_1026" s="T16">Der Bär rannte zu den Hunden, die Jäger schossen auf den Bären.</ta>
            <ta e="T26" id="Seg_1027" s="T23">Nicht zweimal, dreimal.</ta>
            <ta e="T29" id="Seg_1028" s="T26">Sie fingen den Bären.</ta>
            <ta e="T36" id="Seg_1029" s="T29">Der Bär wurde wütend und hielt einen Jäger fest.</ta>
            <ta e="T44" id="Seg_1030" s="T36">Der Bär hielt mit der Krallenhand den Kopf dieses Jägers fest. </ta>
            <ta e="T47" id="Seg_1031" s="T44">Er zerriss die Kopfhaut.</ta>
            <ta e="T53" id="Seg_1032" s="T47">Die Jäger schossen auf den Bären, der Bär fiel [um].</ta>
            <ta e="T58" id="Seg_1033" s="T53">Der Mann, den der Bär erwischt hat, blieb am Leben.</ta>
            <ta e="T65" id="Seg_1034" s="T58">Sie brachten ihn nach Maksimkin Jar, die Kopfhaut nähten sie.</ta>
            <ta e="T69" id="Seg_1035" s="T65">Vier, fünf Tage lag er.</ta>
            <ta e="T74" id="Seg_1036" s="T69">Dann kam er selbst in sein eigenes Dorf.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_1037" s="T0">медведь сердитый зверь.</ta>
            <ta e="T7" id="Seg_1038" s="T3">Ему в руки не давайся.</ta>
            <ta e="T13" id="Seg_1039" s="T7">однажды охотники пошли убить медведя.</ta>
            <ta e="T16" id="Seg_1040" s="T13">Собаки залаяли на медведя.</ta>
            <ta e="T23" id="Seg_1041" s="T16">медведь побежал к собакам охотники стреляли в медведя.</ta>
            <ta e="T26" id="Seg_1042" s="T23">Не два, три раза.</ta>
            <ta e="T29" id="Seg_1043" s="T26">В медведя попали.</ta>
            <ta e="T36" id="Seg_1044" s="T29">медведь [осердился] рассердился поймал одного охотника</ta>
            <ta e="T44" id="Seg_1045" s="T36">медведь когтявыми руками [поймал] схватил за голову </ta>
            <ta e="T47" id="Seg_1046" s="T44">ободрал [сорвал] с головы головную кожу</ta>
            <ta e="T53" id="Seg_1047" s="T47">Охотники выстрелили (/стреляли) медведя, медведь упал.</ta>
            <ta e="T58" id="Seg_1048" s="T53">медведем пойманный человек остался жив.</ta>
            <ta e="T65" id="Seg_1049" s="T58">его увезли в Максимкин Яр, (головную) кожу сшили.</ta>
            <ta e="T69" id="Seg_1050" s="T65">четыре, пять дней лежал.</ta>
            <ta e="T74" id="Seg_1051" s="T69">потом приехал сам в свою деревню.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
