<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID11E9CADE-111B-7C82-F196-6C3EDF191D15">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\KuLP_1976_Hospital_nar\KuLP_1976_Hospital_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">54</ud-information>
            <ud-information attribute-name="# HIAT:w">40</ud-information>
            <ud-information attribute-name="# e">40</ud-information>
            <ud-information attribute-name="# HIAT:u">12</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KuLP">
            <abbreviation>KuLP</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KuLP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T185" id="Seg_0" n="sc" s="T145">
               <ts e="T148" id="Seg_2" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_4" n="HIAT:w" s="T145">Asa</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_7" n="HIAT:w" s="T146">kɨčʼaŋ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_10" n="HIAT:w" s="T147">qučʼonoːqo</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_14" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_16" n="HIAT:w" s="T148">Iloqo</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_19" n="HIAT:w" s="T149">kɨkaŋ</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_23" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_25" n="HIAT:w" s="T150">Mat</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_28" n="HIAT:w" s="T151">polʼnicaqɨt</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_31" n="HIAT:w" s="T152">ippɨsam</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_34" n="HIAT:w" s="T153">nɔːkur</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_37" n="HIAT:w" s="T154">iräntɨ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_40" n="HIAT:w" s="T155">kuntɨ</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T159" id="Seg_44" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_46" n="HIAT:w" s="T156">Qoitqo</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_49" n="HIAT:w" s="T157">čʼasɨk</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_52" n="HIAT:w" s="T158">ɛsa</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_56" n="HIAT:u" s="T159">
                  <ts e="T160" id="Seg_58" n="HIAT:w" s="T159">Qəntam</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_61" n="HIAT:w" s="T160">palʼtom</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_64" n="HIAT:w" s="T161">tokaltɛntam</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_68" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_70" n="HIAT:w" s="T162">Mat</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_73" n="HIAT:w" s="T163">ütem</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_76" n="HIAT:w" s="T164">ütɛntam</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_80" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_82" n="HIAT:w" s="T165">Tınolaka</ts>
                  <nts id="Seg_83" n="HIAT:ip">,</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_86" n="HIAT:w" s="T166">qɔːtä</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_89" n="HIAT:w" s="T167">säːrɛnta</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_93" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_95" n="HIAT:w" s="T168">Ütem</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_98" n="HIAT:w" s="T169">iːsam</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_102" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_104" n="HIAT:w" s="T170">Man</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_107" n="HIAT:w" s="T171">polʼnicaqɨn</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_110" n="HIAT:w" s="T172">ippɨsaŋ</ts>
                  <nts id="Seg_111" n="HIAT:ip">,</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_114" n="HIAT:w" s="T173">čʼösɨŋ</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_117" n="HIAT:w" s="T174">ɛsɨsam</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_121" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_123" n="HIAT:w" s="T175">Samohotka</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_126" n="HIAT:w" s="T176">tüːnta</ts>
                  <nts id="Seg_127" n="HIAT:ip">.</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_130" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_132" n="HIAT:w" s="T177">Tälʼ</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_135" n="HIAT:w" s="T178">teːl</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_138" n="HIAT:w" s="T179">qɔːtä</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_141" n="HIAT:w" s="T180">qəntɨmɨn</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T185" id="Seg_145" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_147" n="HIAT:w" s="T181">Mat</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_150" n="HIAT:w" s="T182">šäqɨsaŋ</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_153" n="HIAT:w" s="T183">znakomɨj</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_156" n="HIAT:w" s="T184">iman</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T185" id="Seg_159" n="sc" s="T145">
               <ts e="T146" id="Seg_161" n="e" s="T145">Asa </ts>
               <ts e="T147" id="Seg_163" n="e" s="T146">kɨčʼaŋ </ts>
               <ts e="T148" id="Seg_165" n="e" s="T147">qučʼonoːqo. </ts>
               <ts e="T149" id="Seg_167" n="e" s="T148">Iloqo </ts>
               <ts e="T150" id="Seg_169" n="e" s="T149">kɨkaŋ. </ts>
               <ts e="T151" id="Seg_171" n="e" s="T150">Mat </ts>
               <ts e="T152" id="Seg_173" n="e" s="T151">polʼnicaqɨt </ts>
               <ts e="T153" id="Seg_175" n="e" s="T152">ippɨsam </ts>
               <ts e="T154" id="Seg_177" n="e" s="T153">nɔːkur </ts>
               <ts e="T155" id="Seg_179" n="e" s="T154">iräntɨ </ts>
               <ts e="T156" id="Seg_181" n="e" s="T155">kuntɨ. </ts>
               <ts e="T157" id="Seg_183" n="e" s="T156">Qoitqo </ts>
               <ts e="T158" id="Seg_185" n="e" s="T157">čʼasɨk </ts>
               <ts e="T159" id="Seg_187" n="e" s="T158">ɛsa. </ts>
               <ts e="T160" id="Seg_189" n="e" s="T159">Qəntam </ts>
               <ts e="T161" id="Seg_191" n="e" s="T160">palʼtom </ts>
               <ts e="T162" id="Seg_193" n="e" s="T161">tokaltɛntam. </ts>
               <ts e="T163" id="Seg_195" n="e" s="T162">Mat </ts>
               <ts e="T164" id="Seg_197" n="e" s="T163">ütem </ts>
               <ts e="T165" id="Seg_199" n="e" s="T164">ütɛntam. </ts>
               <ts e="T166" id="Seg_201" n="e" s="T165">Tınolaka, </ts>
               <ts e="T167" id="Seg_203" n="e" s="T166">qɔːtä </ts>
               <ts e="T168" id="Seg_205" n="e" s="T167">säːrɛnta. </ts>
               <ts e="T169" id="Seg_207" n="e" s="T168">Ütem </ts>
               <ts e="T170" id="Seg_209" n="e" s="T169">iːsam. </ts>
               <ts e="T171" id="Seg_211" n="e" s="T170">Man </ts>
               <ts e="T172" id="Seg_213" n="e" s="T171">polʼnicaqɨn </ts>
               <ts e="T173" id="Seg_215" n="e" s="T172">ippɨsaŋ, </ts>
               <ts e="T174" id="Seg_217" n="e" s="T173">čʼösɨŋ </ts>
               <ts e="T175" id="Seg_219" n="e" s="T174">ɛsɨsam. </ts>
               <ts e="T176" id="Seg_221" n="e" s="T175">Samohotka </ts>
               <ts e="T177" id="Seg_223" n="e" s="T176">tüːnta. </ts>
               <ts e="T178" id="Seg_225" n="e" s="T177">Tälʼ </ts>
               <ts e="T179" id="Seg_227" n="e" s="T178">teːl </ts>
               <ts e="T180" id="Seg_229" n="e" s="T179">qɔːtä </ts>
               <ts e="T181" id="Seg_231" n="e" s="T180">qəntɨmɨn. </ts>
               <ts e="T182" id="Seg_233" n="e" s="T181">Mat </ts>
               <ts e="T183" id="Seg_235" n="e" s="T182">šäqɨsaŋ </ts>
               <ts e="T184" id="Seg_237" n="e" s="T183">znakomɨj </ts>
               <ts e="T185" id="Seg_239" n="e" s="T184">iman. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T148" id="Seg_240" s="T145">KuLP_1976_Hospital_nar.001 (001.001)</ta>
            <ta e="T150" id="Seg_241" s="T148">KuLP_1976_Hospital_nar.002 (001.002)</ta>
            <ta e="T156" id="Seg_242" s="T150">KuLP_1976_Hospital_nar.003 (001.003)</ta>
            <ta e="T159" id="Seg_243" s="T156">KuLP_1976_Hospital_nar.004 (001.004)</ta>
            <ta e="T162" id="Seg_244" s="T159">KuLP_1976_Hospital_nar.005 (001.005)</ta>
            <ta e="T165" id="Seg_245" s="T162">KuLP_1976_Hospital_nar.006 (001.006)</ta>
            <ta e="T168" id="Seg_246" s="T165">KuLP_1976_Hospital_nar.007 (001.007)</ta>
            <ta e="T170" id="Seg_247" s="T168">KuLP_1976_Hospital_nar.008 (001.008)</ta>
            <ta e="T175" id="Seg_248" s="T170">KuLP_1976_Hospital_nar.009 (001.009)</ta>
            <ta e="T177" id="Seg_249" s="T175">KuLP_1976_Hospital_nar.010 (001.010)</ta>
            <ta e="T181" id="Seg_250" s="T177">KuLP_1976_Hospital_nar.011 (001.011)</ta>
            <ta e="T185" id="Seg_251" s="T181">KuLP_1976_Hospital_nar.012 (001.012)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T148" id="Seg_252" s="T145">а′са кы′чаң kучоноkо.</ta>
            <ta e="T150" id="Seg_253" s="T148">′иlоkо ′кыкаң.</ta>
            <ta e="T156" id="Seg_254" s="T150">мат поlʼницаɣыт ′иппысам ′но̄к̊ур и′ренты кунты.</ta>
            <ta e="T159" id="Seg_255" s="T156">kоитkо часык ′еса.</ta>
            <ta e="T162" id="Seg_256" s="T159">′kъ̊нтам палʼ′том то̄′г̂аlтемтам.</ta>
            <ta e="T165" id="Seg_257" s="T162">мат ′ӱ̄дем ӱ̄дентам.</ta>
            <ta e="T168" id="Seg_258" s="T165">′тӣноlаkа ′kо̄тӓ сӓрӓнта.</ta>
            <ta e="T170" id="Seg_259" s="T168">′ӱ̄д̂[т]ем ӣзам.</ta>
            <ta e="T175" id="Seg_260" s="T170">ман полʼ′ницаɣын ′иппысаң, ′чӧзың ӓсысам.</ta>
            <ta e="T177" id="Seg_261" s="T175">‵само′хотка тӱ̄нта.</ta>
            <ta e="T181" id="Seg_262" s="T177">′тɛлʼделʼ ′котӓ ′kъ̊[ы]нтымын.</ta>
            <ta e="T185" id="Seg_263" s="T181">мат ′ше̄kысаң знакомыйиман.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T148" id="Seg_264" s="T145">asa kɨčʼaŋ qučʼonoqo.</ta>
            <ta e="T150" id="Seg_265" s="T148">ilʼoqo kɨkaŋ.</ta>
            <ta e="T156" id="Seg_266" s="T150">mat polʼʼnicaqɨt ippɨsam noːkur irentɨ kuntɨ.</ta>
            <ta e="T159" id="Seg_267" s="T156">qoitqo čʼasɨk esa.</ta>
            <ta e="T162" id="Seg_268" s="T159">qəntam palʼtom toːĝalʼtemtam.</ta>
            <ta e="T165" id="Seg_269" s="T162">mat üːdem üːdentam.</ta>
            <ta e="T168" id="Seg_270" s="T165">tiːnolʼaqa qoːtä säränta.</ta>
            <ta e="T170" id="Seg_271" s="T168">üːd̂[t]em iːzam.</ta>
            <ta e="T175" id="Seg_272" s="T170">man polʼnicaqɨn ippɨsaŋ, čʼözɨŋ äsɨsam.</ta>
            <ta e="T177" id="Seg_273" s="T175">samohotka tüːnta.</ta>
            <ta e="T181" id="Seg_274" s="T177">tɛlʼdelʼ kotä qə[ɨ]ntɨmɨn.</ta>
            <ta e="T185" id="Seg_275" s="T181">mat šeːqɨsaŋ znakomɨjiman.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T148" id="Seg_276" s="T145">Asa kɨčʼaŋ qučʼonoːqo. </ta>
            <ta e="T150" id="Seg_277" s="T148">Iloqo kɨkaŋ. </ta>
            <ta e="T156" id="Seg_278" s="T150">Mat polʼnicaqɨt ippɨsam nɔːkur iräntɨ kuntɨ. </ta>
            <ta e="T159" id="Seg_279" s="T156">Qoitqo čʼasɨk ɛsa. </ta>
            <ta e="T162" id="Seg_280" s="T159">Qəntam palʼtom tokaltɛntam. </ta>
            <ta e="T165" id="Seg_281" s="T162">Mat ütem ütɛntam. </ta>
            <ta e="T168" id="Seg_282" s="T165">Tınolaka, qɔːtä säːrɛnta. </ta>
            <ta e="T170" id="Seg_283" s="T168">Ütem iːsam. </ta>
            <ta e="T175" id="Seg_284" s="T170">Man polʼnicaqɨn ippɨsaŋ, čʼösɨŋ ɛsɨsam. </ta>
            <ta e="T177" id="Seg_285" s="T175">Samohotka tüːnta. </ta>
            <ta e="T181" id="Seg_286" s="T177">Tälʼ teːl qɔːtä qəntɨmɨn. </ta>
            <ta e="T185" id="Seg_287" s="T181">Mat šäqɨsaŋ znakomɨj iman. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T146" id="Seg_288" s="T145">asa</ta>
            <ta e="T147" id="Seg_289" s="T146">kɨčʼa-ŋ</ta>
            <ta e="T148" id="Seg_290" s="T147">qu-čʼonoːqo</ta>
            <ta e="T149" id="Seg_291" s="T148">ilo-qo</ta>
            <ta e="T150" id="Seg_292" s="T149">kɨka-ŋ</ta>
            <ta e="T151" id="Seg_293" s="T150">mat</ta>
            <ta e="T152" id="Seg_294" s="T151">polʼnica-qɨt</ta>
            <ta e="T153" id="Seg_295" s="T152">ippɨ-sa-m</ta>
            <ta e="T154" id="Seg_296" s="T153">nɔːkur</ta>
            <ta e="T155" id="Seg_297" s="T154">irä-n-tɨ</ta>
            <ta e="T156" id="Seg_298" s="T155">kuntɨ</ta>
            <ta e="T157" id="Seg_299" s="T156">qoi-tqo</ta>
            <ta e="T158" id="Seg_300" s="T157">čʼasɨ-k</ta>
            <ta e="T159" id="Seg_301" s="T158">ɛsa</ta>
            <ta e="T160" id="Seg_302" s="T159">qən-ta-m</ta>
            <ta e="T161" id="Seg_303" s="T160">palʼto-m</ta>
            <ta e="T162" id="Seg_304" s="T161">tok-alt-ɛnta-m</ta>
            <ta e="T163" id="Seg_305" s="T162">mat</ta>
            <ta e="T164" id="Seg_306" s="T163">üt-e-m</ta>
            <ta e="T165" id="Seg_307" s="T164">üt-ɛnta-m</ta>
            <ta e="T166" id="Seg_308" s="T165">tınola-ka</ta>
            <ta e="T167" id="Seg_309" s="T166">qɔːtä</ta>
            <ta e="T168" id="Seg_310" s="T167">säːr-ɛnta</ta>
            <ta e="T169" id="Seg_311" s="T168">üt-e-m</ta>
            <ta e="T170" id="Seg_312" s="T169">iː-sa-m</ta>
            <ta e="T171" id="Seg_313" s="T170">man</ta>
            <ta e="T172" id="Seg_314" s="T171">polʼnica-qɨn</ta>
            <ta e="T173" id="Seg_315" s="T172">ippɨ-sa-ŋ</ta>
            <ta e="T174" id="Seg_316" s="T173">čʼös-ɨ-ŋ</ta>
            <ta e="T175" id="Seg_317" s="T174">ɛsɨ-sa-m</ta>
            <ta e="T176" id="Seg_318" s="T175">samohotka</ta>
            <ta e="T177" id="Seg_319" s="T176">tüː-nta</ta>
            <ta e="T178" id="Seg_320" s="T177">tälʼ</ta>
            <ta e="T179" id="Seg_321" s="T178">teːl</ta>
            <ta e="T180" id="Seg_322" s="T179">qɔːtä</ta>
            <ta e="T181" id="Seg_323" s="T180">qə-ntɨ-mɨn</ta>
            <ta e="T182" id="Seg_324" s="T181">mat</ta>
            <ta e="T183" id="Seg_325" s="T182">šäqɨ-sa-ŋ</ta>
            <ta e="T184" id="Seg_326" s="T183">znakomɨj</ta>
            <ta e="T185" id="Seg_327" s="T184">ima-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T146" id="Seg_328" s="T145">aššă</ta>
            <ta e="T147" id="Seg_329" s="T146">kɨkɨ-k</ta>
            <ta e="T148" id="Seg_330" s="T147">qu-qɨnoːqo</ta>
            <ta e="T149" id="Seg_331" s="T148">ilɨ-qo</ta>
            <ta e="T150" id="Seg_332" s="T149">kɨkɨ-k</ta>
            <ta e="T151" id="Seg_333" s="T150">man</ta>
            <ta e="T152" id="Seg_334" s="T151">palʼnica-qɨn</ta>
            <ta e="T153" id="Seg_335" s="T152">ippɨ-sɨ-m</ta>
            <ta e="T154" id="Seg_336" s="T153">nɔːkɨr</ta>
            <ta e="T155" id="Seg_337" s="T154">irä-n-tɨ</ta>
            <ta e="T156" id="Seg_338" s="T155">kuntɨ</ta>
            <ta e="T157" id="Seg_339" s="T156">qaj-tqo</ta>
            <ta e="T158" id="Seg_340" s="T157">*čʼasɨ-k</ta>
            <ta e="T159" id="Seg_341" s="T158">ɛsɨ</ta>
            <ta e="T160" id="Seg_342" s="T159">qən-ɛntɨ-m</ta>
            <ta e="T161" id="Seg_343" s="T160">palʼto-m</ta>
            <ta e="T162" id="Seg_344" s="T161">tokk-altɨ-ɛntɨ-m</ta>
            <ta e="T163" id="Seg_345" s="T162">man</ta>
            <ta e="T164" id="Seg_346" s="T163">üt-ɨ-m</ta>
            <ta e="T165" id="Seg_347" s="T164">ütɨ-ɛntɨ-m</ta>
            <ta e="T166" id="Seg_348" s="T165">tınola-ka</ta>
            <ta e="T167" id="Seg_349" s="T166">qɔːtɨ</ta>
            <ta e="T168" id="Seg_350" s="T167">səːrɨ-ɛntɨ</ta>
            <ta e="T169" id="Seg_351" s="T168">üt-ɨ-m</ta>
            <ta e="T170" id="Seg_352" s="T169">iː-sɨ-m</ta>
            <ta e="T171" id="Seg_353" s="T170">man</ta>
            <ta e="T172" id="Seg_354" s="T171">palʼnica-qɨn</ta>
            <ta e="T173" id="Seg_355" s="T172">ippɨ-sɨ-k</ta>
            <ta e="T174" id="Seg_356" s="T173">čʼoš-ɨ-k</ta>
            <ta e="T175" id="Seg_357" s="T174">ɛsɨ-sɨ-m</ta>
            <ta e="T176" id="Seg_358" s="T175">samohotka</ta>
            <ta e="T177" id="Seg_359" s="T176">tü-ntɨ</ta>
            <ta e="T178" id="Seg_360" s="T177">täːlɨ</ta>
            <ta e="T179" id="Seg_361" s="T178">čʼeːlɨ</ta>
            <ta e="T180" id="Seg_362" s="T179">qɔːtɨ</ta>
            <ta e="T181" id="Seg_363" s="T180">qən-ɛntɨ-mɨt</ta>
            <ta e="T182" id="Seg_364" s="T181">man</ta>
            <ta e="T183" id="Seg_365" s="T182">šäqqɨ-sɨ-k</ta>
            <ta e="T184" id="Seg_366" s="T183">znakomɨj</ta>
            <ta e="T185" id="Seg_367" s="T184">ima-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T146" id="Seg_368" s="T145">NEG</ta>
            <ta e="T147" id="Seg_369" s="T146">want-1SG.S</ta>
            <ta e="T148" id="Seg_370" s="T147">die-SUP.1SG</ta>
            <ta e="T149" id="Seg_371" s="T148">live-INF</ta>
            <ta e="T150" id="Seg_372" s="T149">want-1SG.S</ta>
            <ta e="T151" id="Seg_373" s="T150">I.NOM</ta>
            <ta e="T152" id="Seg_374" s="T151">hospital-LOC</ta>
            <ta e="T153" id="Seg_375" s="T152">lie-PST-1SG.O</ta>
            <ta e="T154" id="Seg_376" s="T153">three</ta>
            <ta e="T155" id="Seg_377" s="T154">month-GEN-3SG</ta>
            <ta e="T156" id="Seg_378" s="T155">during</ta>
            <ta e="T157" id="Seg_379" s="T156">what-TRL</ta>
            <ta e="T158" id="Seg_380" s="T157">cold-ADVZ</ta>
            <ta e="T159" id="Seg_381" s="T158">become.[3SG.S]</ta>
            <ta e="T160" id="Seg_382" s="T159">leave-FUT-1SG.O</ta>
            <ta e="T161" id="Seg_383" s="T160">coat-ACC</ta>
            <ta e="T162" id="Seg_384" s="T161">put.on-TR-FUT-1SG.O</ta>
            <ta e="T163" id="Seg_385" s="T162">I.NOM</ta>
            <ta e="T164" id="Seg_386" s="T163">spirit-EP-ACC</ta>
            <ta e="T165" id="Seg_387" s="T164">drink-FUT-1SG.O</ta>
            <ta e="T166" id="Seg_388" s="T165">cloud-DIM.[NOM]</ta>
            <ta e="T167" id="Seg_389" s="T166">probably</ta>
            <ta e="T168" id="Seg_390" s="T167">rain-FUT.[3SG.S]</ta>
            <ta e="T169" id="Seg_391" s="T168">spirit-EP-ACC</ta>
            <ta e="T170" id="Seg_392" s="T169">take-PST-1SG.O</ta>
            <ta e="T171" id="Seg_393" s="T170">I.NOM</ta>
            <ta e="T172" id="Seg_394" s="T171">hospital-LOC</ta>
            <ta e="T173" id="Seg_395" s="T172">lie-PST-1SG.S</ta>
            <ta e="T174" id="Seg_396" s="T173">fat-EP-ADVZ</ta>
            <ta e="T175" id="Seg_397" s="T174">become-PST-1SG.O</ta>
            <ta e="T176" id="Seg_398" s="T175">self_propelled.vehicle.[NOM]</ta>
            <ta e="T177" id="Seg_399" s="T176">come-IPFV.[3SG.S]</ta>
            <ta e="T178" id="Seg_400" s="T177">tomorrow</ta>
            <ta e="T179" id="Seg_401" s="T178">day.[NOM]</ta>
            <ta e="T180" id="Seg_402" s="T179">probably</ta>
            <ta e="T181" id="Seg_403" s="T180">leave-FUT-1PL</ta>
            <ta e="T182" id="Seg_404" s="T181">I.NOM</ta>
            <ta e="T183" id="Seg_405" s="T182">spend.night-PST-1SG.S</ta>
            <ta e="T184" id="Seg_406" s="T183">known</ta>
            <ta e="T185" id="Seg_407" s="T184">woman-GEN</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T146" id="Seg_408" s="T145">NEG</ta>
            <ta e="T147" id="Seg_409" s="T146">хотеть-1SG.S</ta>
            <ta e="T148" id="Seg_410" s="T147">умереть-SUP.1SG</ta>
            <ta e="T149" id="Seg_411" s="T148">жить-INF</ta>
            <ta e="T150" id="Seg_412" s="T149">хотеть-1SG.S</ta>
            <ta e="T151" id="Seg_413" s="T150">я.NOM</ta>
            <ta e="T152" id="Seg_414" s="T151">больница-LOC</ta>
            <ta e="T153" id="Seg_415" s="T152">лежать-PST-1SG.O</ta>
            <ta e="T154" id="Seg_416" s="T153">три</ta>
            <ta e="T155" id="Seg_417" s="T154">месяц-GEN-3SG</ta>
            <ta e="T156" id="Seg_418" s="T155">в.течение</ta>
            <ta e="T157" id="Seg_419" s="T156">что-TRL</ta>
            <ta e="T158" id="Seg_420" s="T157">холодный-ADVZ</ta>
            <ta e="T159" id="Seg_421" s="T158">стать.[3SG.S]</ta>
            <ta e="T160" id="Seg_422" s="T159">отправиться-FUT-1SG.O</ta>
            <ta e="T161" id="Seg_423" s="T160">пальто-ACC</ta>
            <ta e="T162" id="Seg_424" s="T161">надеть-TR-FUT-1SG.O</ta>
            <ta e="T163" id="Seg_425" s="T162">я.NOM</ta>
            <ta e="T164" id="Seg_426" s="T163">алкоголь-EP-ACC</ta>
            <ta e="T165" id="Seg_427" s="T164">пить-FUT-1SG.O</ta>
            <ta e="T166" id="Seg_428" s="T165">облако-DIM.[NOM]</ta>
            <ta e="T167" id="Seg_429" s="T166">наверно</ta>
            <ta e="T168" id="Seg_430" s="T167">дождить-FUT.[3SG.S]</ta>
            <ta e="T169" id="Seg_431" s="T168">алкоголь-EP-ACC</ta>
            <ta e="T170" id="Seg_432" s="T169">взять-PST-1SG.O</ta>
            <ta e="T171" id="Seg_433" s="T170">я.NOM</ta>
            <ta e="T172" id="Seg_434" s="T171">больница-LOC</ta>
            <ta e="T173" id="Seg_435" s="T172">лежать-PST-1SG.S</ta>
            <ta e="T174" id="Seg_436" s="T173">сало-EP-ADVZ</ta>
            <ta e="T175" id="Seg_437" s="T174">стать-PST-1SG.O</ta>
            <ta e="T176" id="Seg_438" s="T175">самоходка.[NOM]</ta>
            <ta e="T177" id="Seg_439" s="T176">прийти-IPFV.[3SG.S]</ta>
            <ta e="T178" id="Seg_440" s="T177">завтра</ta>
            <ta e="T179" id="Seg_441" s="T178">день.[NOM]</ta>
            <ta e="T180" id="Seg_442" s="T179">наверно</ta>
            <ta e="T181" id="Seg_443" s="T180">отправиться-FUT-1PL</ta>
            <ta e="T182" id="Seg_444" s="T181">я.NOM</ta>
            <ta e="T183" id="Seg_445" s="T182">ночевать-PST-1SG.S</ta>
            <ta e="T184" id="Seg_446" s="T183">знакомый</ta>
            <ta e="T185" id="Seg_447" s="T184">женщина-GEN</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T146" id="Seg_448" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_449" s="T146">v-v:pn</ta>
            <ta e="T148" id="Seg_450" s="T147">v-v:inf-poss</ta>
            <ta e="T149" id="Seg_451" s="T148">v-v:inf</ta>
            <ta e="T150" id="Seg_452" s="T149">v-v:pn</ta>
            <ta e="T151" id="Seg_453" s="T150">pers</ta>
            <ta e="T152" id="Seg_454" s="T151">n-n:case3</ta>
            <ta e="T153" id="Seg_455" s="T152">v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_456" s="T153">num</ta>
            <ta e="T155" id="Seg_457" s="T154">n-n:case3-n:poss</ta>
            <ta e="T156" id="Seg_458" s="T155">pp</ta>
            <ta e="T157" id="Seg_459" s="T156">interrog-n:case3</ta>
            <ta e="T158" id="Seg_460" s="T157">adj-adj&gt;adv</ta>
            <ta e="T159" id="Seg_461" s="T158">v-v:pn</ta>
            <ta e="T160" id="Seg_462" s="T159">v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_463" s="T160">n-n:case3</ta>
            <ta e="T162" id="Seg_464" s="T161">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T163" id="Seg_465" s="T162">pers</ta>
            <ta e="T164" id="Seg_466" s="T163">n-n:(ins)-n:case3</ta>
            <ta e="T165" id="Seg_467" s="T164">v-v:tense-v:pn</ta>
            <ta e="T166" id="Seg_468" s="T165">n-n&gt;n-n:case3</ta>
            <ta e="T167" id="Seg_469" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_470" s="T167">v-v:tense-v:pn</ta>
            <ta e="T169" id="Seg_471" s="T168">n-n:(ins)-n:case3</ta>
            <ta e="T170" id="Seg_472" s="T169">v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_473" s="T170">pers</ta>
            <ta e="T172" id="Seg_474" s="T171">n-n:case3</ta>
            <ta e="T173" id="Seg_475" s="T172">v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_476" s="T173">n-n:(ins)-n&gt;adv</ta>
            <ta e="T175" id="Seg_477" s="T174">v-v:tense-v:pn</ta>
            <ta e="T176" id="Seg_478" s="T175">n-n:case3</ta>
            <ta e="T177" id="Seg_479" s="T176">v-v&gt;v-v:pn</ta>
            <ta e="T178" id="Seg_480" s="T177">adv</ta>
            <ta e="T179" id="Seg_481" s="T178">n-n:case3</ta>
            <ta e="T180" id="Seg_482" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_483" s="T180">v-v:tense-v:pn</ta>
            <ta e="T182" id="Seg_484" s="T181">pers</ta>
            <ta e="T183" id="Seg_485" s="T182">v-v:tense-v:pn</ta>
            <ta e="T184" id="Seg_486" s="T183">adj</ta>
            <ta e="T185" id="Seg_487" s="T184">n-n:case3</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T146" id="Seg_488" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_489" s="T146">v</ta>
            <ta e="T148" id="Seg_490" s="T147">v</ta>
            <ta e="T149" id="Seg_491" s="T148">v</ta>
            <ta e="T150" id="Seg_492" s="T149">v</ta>
            <ta e="T151" id="Seg_493" s="T150">pers</ta>
            <ta e="T152" id="Seg_494" s="T151">n</ta>
            <ta e="T153" id="Seg_495" s="T152">v</ta>
            <ta e="T154" id="Seg_496" s="T153">num</ta>
            <ta e="T155" id="Seg_497" s="T154">n</ta>
            <ta e="T156" id="Seg_498" s="T155">pp</ta>
            <ta e="T157" id="Seg_499" s="T156">interrog</ta>
            <ta e="T158" id="Seg_500" s="T157">adv</ta>
            <ta e="T159" id="Seg_501" s="T158">v</ta>
            <ta e="T160" id="Seg_502" s="T159">v</ta>
            <ta e="T161" id="Seg_503" s="T160">n</ta>
            <ta e="T162" id="Seg_504" s="T161">v</ta>
            <ta e="T163" id="Seg_505" s="T162">pers</ta>
            <ta e="T164" id="Seg_506" s="T163">n</ta>
            <ta e="T165" id="Seg_507" s="T164">v</ta>
            <ta e="T166" id="Seg_508" s="T165">n</ta>
            <ta e="T167" id="Seg_509" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_510" s="T167">v</ta>
            <ta e="T169" id="Seg_511" s="T168">n</ta>
            <ta e="T170" id="Seg_512" s="T169">v</ta>
            <ta e="T171" id="Seg_513" s="T170">pers</ta>
            <ta e="T172" id="Seg_514" s="T171">n</ta>
            <ta e="T173" id="Seg_515" s="T172">v</ta>
            <ta e="T174" id="Seg_516" s="T173">n</ta>
            <ta e="T175" id="Seg_517" s="T174">v</ta>
            <ta e="T176" id="Seg_518" s="T175">n</ta>
            <ta e="T177" id="Seg_519" s="T176">v</ta>
            <ta e="T178" id="Seg_520" s="T177">adv</ta>
            <ta e="T179" id="Seg_521" s="T178">n</ta>
            <ta e="T180" id="Seg_522" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_523" s="T180">v</ta>
            <ta e="T182" id="Seg_524" s="T181">pers</ta>
            <ta e="T183" id="Seg_525" s="T182">v</ta>
            <ta e="T184" id="Seg_526" s="T183">adj</ta>
            <ta e="T185" id="Seg_527" s="T184">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T147" id="Seg_528" s="T146">0.1.h:E</ta>
            <ta e="T150" id="Seg_529" s="T149">0.1.h:E</ta>
            <ta e="T151" id="Seg_530" s="T150">pro.h:Th</ta>
            <ta e="T152" id="Seg_531" s="T151">np:L</ta>
            <ta e="T160" id="Seg_532" s="T159">0.1.h:A</ta>
            <ta e="T161" id="Seg_533" s="T160">np:Th</ta>
            <ta e="T162" id="Seg_534" s="T161">0.1.h:A</ta>
            <ta e="T163" id="Seg_535" s="T162">pro.h:A</ta>
            <ta e="T164" id="Seg_536" s="T163">np:P</ta>
            <ta e="T166" id="Seg_537" s="T165">np:Th</ta>
            <ta e="T169" id="Seg_538" s="T168">np:P</ta>
            <ta e="T170" id="Seg_539" s="T169">0.1.h:A</ta>
            <ta e="T171" id="Seg_540" s="T170">pro.h:E</ta>
            <ta e="T172" id="Seg_541" s="T171">np:L</ta>
            <ta e="T175" id="Seg_542" s="T174">0.1.h:P</ta>
            <ta e="T176" id="Seg_543" s="T175">np:Th</ta>
            <ta e="T179" id="Seg_544" s="T177">np:Time</ta>
            <ta e="T181" id="Seg_545" s="T180">0.1.h:A</ta>
            <ta e="T182" id="Seg_546" s="T181">pro.h:A</ta>
            <ta e="T185" id="Seg_547" s="T184">np:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T147" id="Seg_548" s="T146">0.1.h:S v:pred</ta>
            <ta e="T148" id="Seg_549" s="T147">s:compl</ta>
            <ta e="T149" id="Seg_550" s="T148">s:compl</ta>
            <ta e="T150" id="Seg_551" s="T149">0.1.h:S v:pred</ta>
            <ta e="T151" id="Seg_552" s="T150">pro.h:S</ta>
            <ta e="T153" id="Seg_553" s="T152">v:pred</ta>
            <ta e="T159" id="Seg_554" s="T158">0.3:S v:pred</ta>
            <ta e="T160" id="Seg_555" s="T159">0.1.h:S v:pred</ta>
            <ta e="T161" id="Seg_556" s="T160">np:O</ta>
            <ta e="T162" id="Seg_557" s="T161">0.1.h:S v:pred</ta>
            <ta e="T163" id="Seg_558" s="T162">pro.h:S</ta>
            <ta e="T164" id="Seg_559" s="T163">np:O</ta>
            <ta e="T165" id="Seg_560" s="T164">v:pred</ta>
            <ta e="T166" id="Seg_561" s="T165">np:S</ta>
            <ta e="T168" id="Seg_562" s="T167">0.3:S v:pred</ta>
            <ta e="T169" id="Seg_563" s="T168">np:O</ta>
            <ta e="T170" id="Seg_564" s="T169">0.1.h:S v:pred</ta>
            <ta e="T171" id="Seg_565" s="T170">pro.h:S</ta>
            <ta e="T173" id="Seg_566" s="T172">v:pred</ta>
            <ta e="T175" id="Seg_567" s="T174">0.1.h:S v:pred</ta>
            <ta e="T176" id="Seg_568" s="T175">np:S</ta>
            <ta e="T177" id="Seg_569" s="T176">v:pred</ta>
            <ta e="T181" id="Seg_570" s="T180">0.1.h:S v:pred</ta>
            <ta e="T182" id="Seg_571" s="T181">pro.h:S</ta>
            <ta e="T183" id="Seg_572" s="T182">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T147" id="Seg_573" s="T146">0.new</ta>
            <ta e="T150" id="Seg_574" s="T149">0.new</ta>
            <ta e="T151" id="Seg_575" s="T150">giv-active</ta>
            <ta e="T152" id="Seg_576" s="T151">accs-gen</ta>
            <ta e="T161" id="Seg_577" s="T160">new</ta>
            <ta e="T162" id="Seg_578" s="T161">0.giv-inactive</ta>
            <ta e="T163" id="Seg_579" s="T162">giv-active</ta>
            <ta e="T164" id="Seg_580" s="T163">new</ta>
            <ta e="T166" id="Seg_581" s="T165">accs-gen</ta>
            <ta e="T169" id="Seg_582" s="T168">giv-inactive</ta>
            <ta e="T170" id="Seg_583" s="T169">0.giv-active</ta>
            <ta e="T171" id="Seg_584" s="T170">giv-active</ta>
            <ta e="T172" id="Seg_585" s="T171">giv-inactive</ta>
            <ta e="T176" id="Seg_586" s="T175">new</ta>
            <ta e="T179" id="Seg_587" s="T178">accs-gen</ta>
            <ta e="T181" id="Seg_588" s="T180">0.accs-aggr</ta>
            <ta e="T182" id="Seg_589" s="T181">giv-inactive</ta>
            <ta e="T185" id="Seg_590" s="T184">new</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T152" id="Seg_591" s="T151">RUS:cult</ta>
            <ta e="T161" id="Seg_592" s="T160">RUS:cult</ta>
            <ta e="T172" id="Seg_593" s="T171">RUS:cult</ta>
            <ta e="T176" id="Seg_594" s="T175">RUS:cult</ta>
            <ta e="T184" id="Seg_595" s="T183">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T152" id="Seg_596" s="T151">Csub</ta>
            <ta e="T172" id="Seg_597" s="T171">Csub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T152" id="Seg_598" s="T151">dir:infl</ta>
            <ta e="T161" id="Seg_599" s="T160">dir:infl</ta>
            <ta e="T172" id="Seg_600" s="T171">dir:infl</ta>
            <ta e="T176" id="Seg_601" s="T175">dir:bare</ta>
            <ta e="T184" id="Seg_602" s="T183">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T148" id="Seg_603" s="T145">Не хочу умирать.</ta>
            <ta e="T150" id="Seg_604" s="T148">Жить хочу.</ta>
            <ta e="T156" id="Seg_605" s="T150">Я лежал в больнице три месяца.</ta>
            <ta e="T159" id="Seg_606" s="T156">Что-то холодно становится.</ta>
            <ta e="T162" id="Seg_607" s="T159">Пойду пальто надену.</ta>
            <ta e="T165" id="Seg_608" s="T162">Я вино выпью.</ta>
            <ta e="T168" id="Seg_609" s="T165">Тучка, наверное дождь пойдёт.</ta>
            <ta e="T170" id="Seg_610" s="T168">[Я] вино взял.</ta>
            <ta e="T175" id="Seg_611" s="T170">Я в больнице лежал, жирный стал.</ta>
            <ta e="T177" id="Seg_612" s="T175">Самоходка идет.</ta>
            <ta e="T181" id="Seg_613" s="T177">Завтра, наверно, поедем.</ta>
            <ta e="T185" id="Seg_614" s="T181">Я ночевал у знакомой.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T148" id="Seg_615" s="T145">I don't want to die.</ta>
            <ta e="T150" id="Seg_616" s="T148">I want to live.</ta>
            <ta e="T156" id="Seg_617" s="T150">I've been in the hospital for three months.</ta>
            <ta e="T159" id="Seg_618" s="T156">It is getting cold somehow.</ta>
            <ta e="T162" id="Seg_619" s="T159">I am going to put on the coat.</ta>
            <ta e="T165" id="Seg_620" s="T162">I will drink some alcohol.</ta>
            <ta e="T168" id="Seg_621" s="T165">There is a small cloud, it will probably rain.</ta>
            <ta e="T170" id="Seg_622" s="T168">I drank the spirit.</ta>
            <ta e="T175" id="Seg_623" s="T170">I have been in the hospital, I got fat.</ta>
            <ta e="T177" id="Seg_624" s="T175">A self-propelled vehicle is coming.</ta>
            <ta e="T181" id="Seg_625" s="T177">We will probably leave tomorrow.</ta>
            <ta e="T185" id="Seg_626" s="T181">I've spent the night at a female friend.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T148" id="Seg_627" s="T145">Ich will nicht sterben.</ta>
            <ta e="T150" id="Seg_628" s="T148">Ich will leben.</ta>
            <ta e="T156" id="Seg_629" s="T150">Ich habe drei Monate im Krankenhaus verbracht.</ta>
            <ta e="T159" id="Seg_630" s="T156">Es wird irgendwie kalt.</ta>
            <ta e="T162" id="Seg_631" s="T159">Ich werde den Mantel anziehen.</ta>
            <ta e="T165" id="Seg_632" s="T162">Ich werde etwas Alkohol trinken.</ta>
            <ta e="T168" id="Seg_633" s="T165">Dort ist ein Wölkchen, es wird wohl regnen.</ta>
            <ta e="T170" id="Seg_634" s="T168">Ich trank Alkohol.</ta>
            <ta e="T175" id="Seg_635" s="T170">Ich lag im Krankenhaus, ich bin dick geworden.</ta>
            <ta e="T177" id="Seg_636" s="T175">Eine selbstfahrendes Fahrzeug kommt.</ta>
            <ta e="T181" id="Seg_637" s="T177">Morgen fahren wir wohl los.</ta>
            <ta e="T185" id="Seg_638" s="T181">Ich habe die Nacht bei einer Bekannten verbracht.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T148" id="Seg_639" s="T145">не хочу умирать</ta>
            <ta e="T150" id="Seg_640" s="T148">жить хочу</ta>
            <ta e="T156" id="Seg_641" s="T150">я в больнице лежал 3 месяца.</ta>
            <ta e="T159" id="Seg_642" s="T156">что-то холодно стало.</ta>
            <ta e="T162" id="Seg_643" s="T159">пойду пальто надену</ta>
            <ta e="T185" id="Seg_644" s="T181">я ночевал у знакомого</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T162" id="Seg_645" s="T159">[OSV]: the form "toːĝalʼtemtam" has been edited into "tokaltɛntam".</ta>
            <ta e="T185" id="Seg_646" s="T181">[WNB]: iman -- perhaps ADV.LOC? or from RUS у+GEN?</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T148" id="Seg_647" s="T145">kаррэ тӱ′эс иди сюда</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
