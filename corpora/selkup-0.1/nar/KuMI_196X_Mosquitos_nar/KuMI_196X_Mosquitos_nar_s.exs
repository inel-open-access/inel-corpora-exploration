<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDF5BE1AFA-E0D7-75D8-466A-F7C2368D957A">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="KuMI_196X_Mosquitos_nar.wav" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\KuMI_196X_Mosquitos_nar\KuMI_196X_Mosquitos_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">110</ud-information>
            <ud-information attribute-name="# HIAT:w">88</ud-information>
            <ud-information attribute-name="# e">83</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">14</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KuMI">
            <abbreviation>KuMI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.321" type="appl" />
         <tli id="T1" time="1.003" type="appl" />
         <tli id="T2" time="1.684" type="appl" />
         <tli id="T3" time="2.366" type="appl" />
         <tli id="T4" time="3.048" type="appl" />
         <tli id="T5" time="3.73" type="appl" />
         <tli id="T6" time="4.411" type="appl" />
         <tli id="T7" time="5.093" type="appl" />
         <tli id="T8" time="7.902" type="appl" />
         <tli id="T9" time="8.179" type="appl" />
         <tli id="T10" time="8.456" type="appl" />
         <tli id="T11" time="8.732" type="appl" />
         <tli id="T12" time="9.008" type="appl" />
         <tli id="T13" time="9.285" type="appl" />
         <tli id="T14" time="9.561" type="appl" />
         <tli id="T15" time="9.838" type="appl" />
         <tli id="T16" time="10.899" type="appl" />
         <tli id="T17" time="11.532" type="appl" />
         <tli id="T18" time="12.291" type="appl" />
         <tli id="T19" time="12.702" type="appl" />
         <tli id="T20" time="13.114" type="appl" />
         <tli id="T21" time="13.526" type="appl" />
         <tli id="T22" time="14.62" type="appl" />
         <tli id="T23" time="14.97" type="appl" />
         <tli id="T24" time="15.32" type="appl" />
         <tli id="T25" time="15.67" type="appl" />
         <tli id="T26" time="15.991" type="appl" />
         <tli id="T27" time="16.312" type="appl" />
         <tli id="T28" time="16.633" type="appl" />
         <tli id="T29" time="16.954" type="appl" />
         <tli id="T30" time="17.275" type="appl" />
         <tli id="T31" time="17.596" type="appl" />
         <tli id="T32" time="17.917" type="appl" />
         <tli id="T33" time="18.238" type="appl" />
         <tli id="T34" time="18.559" type="appl" />
         <tli id="T35" time="18.88" type="appl" />
         <tli id="T36" time="21.278" type="appl" />
         <tli id="T37" time="21.547" type="appl" />
         <tli id="T38" time="21.816" type="appl" />
         <tli id="T39" time="22.085" type="appl" />
         <tli id="T40" time="22.353" type="appl" />
         <tli id="T41" time="22.622" type="appl" />
         <tli id="T42" time="22.891" type="appl" />
         <tli id="T43" time="23.16" type="appl" />
         <tli id="T44" time="23.429" type="appl" />
         <tli id="T45" time="24.811" type="appl" />
         <tli id="T46" time="25.217" type="appl" />
         <tli id="T47" time="25.622" type="appl" />
         <tli id="T48" time="26.027" type="appl" />
         <tli id="T49" time="26.433" type="appl" />
         <tli id="T50" time="26.838" type="appl" />
         <tli id="T51" time="27.243" type="appl" />
         <tli id="T52" time="27.648" type="appl" />
         <tli id="T53" time="28.054" type="appl" />
         <tli id="T54" time="28.459" type="appl" />
         <tli id="T55" time="28.864" type="appl" />
         <tli id="T56" time="29.27" type="appl" />
         <tli id="T57" time="29.675" type="appl" />
         <tli id="T58" time="30.711" type="appl" />
         <tli id="T59" time="31.002" type="appl" />
         <tli id="T60" time="31.293" type="appl" />
         <tli id="T61" time="31.583" type="appl" />
         <tli id="T62" time="31.874" type="appl" />
         <tli id="T63" time="32.165" type="appl" />
         <tli id="T64" time="32.456" type="appl" />
         <tli id="T65" time="32.904" type="appl" />
         <tli id="T66" time="33.206" type="appl" />
         <tli id="T67" time="33.507" type="appl" />
         <tli id="T68" time="33.809" type="appl" />
         <tli id="T69" time="34.11" type="appl" />
         <tli id="T70" time="34.412" type="appl" />
         <tli id="T71" time="34.713" type="appl" />
         <tli id="T72" time="36.373" type="appl" />
         <tli id="T73" time="36.765" type="appl" />
         <tli id="T74" time="37.156" type="appl" />
         <tli id="T75" time="37.548" type="appl" />
         <tli id="T76" time="37.939" type="appl" />
         <tli id="T77" time="39.392" type="appl" />
         <tli id="T78" time="39.949" type="appl" />
         <tli id="T79" time="41.102" type="appl" />
         <tli id="T80" time="41.332" type="appl" />
         <tli id="T81" time="41.561" type="appl" />
         <tli id="T82" time="41.791" type="appl" />
         <tli id="T83" time="42.02" type="appl" />
         <tli id="T84" time="42.25" type="appl" />
         <tli id="T92" time="42.7645" type="intp" />
         <tli id="T85" time="43.279" type="appl" />
         <tli id="T86" time="43.521" type="appl" />
         <tli id="T87" time="43.764" type="appl" />
         <tli id="T88" time="44.006" type="appl" />
         <tli id="T89" time="44.249" type="appl" />
         <tli id="T90" time="48.328" />
         <tli id="T91" time="48.374" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KuMI"
                      type="t">
         <timeline-fork end="T7" start="T0">
            <tli id="T0.tx.1" />
            <tli id="T0.tx.2" />
            <tli id="T0.tx.3" />
            <tli id="T0.tx.4" />
            <tli id="T0.tx.5" />
            <tli id="T0.tx.6" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T89" id="Seg_0" n="sc" s="T0">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T0.tx.1" id="Seg_4" n="HIAT:w" s="T0">Марья</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx.2" id="Seg_7" n="HIAT:w" s="T0.tx.1">Ивановна</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx.3" id="Seg_10" n="HIAT:w" s="T0.tx.2">Кунина</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx.4" id="Seg_13" n="HIAT:w" s="T0.tx.3">расскажет</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx.5" id="Seg_17" n="HIAT:w" s="T0.tx.4">как</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx.6" id="Seg_20" n="HIAT:w" s="T0.tx.5">делает</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T0.tx.6">кирпичи</ts>
                  <nts id="Seg_24" n="HIAT:ip">.</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_27" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">Tɨmta</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">qar</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">nɨlčʼik</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">nat</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">qaj</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">qarɨn</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">ınna</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">wəšisaɣ</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_54" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">Or</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">sära</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_63" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">Nɛniqa</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">qap</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">kočʼe</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">ɛːɣa</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_78" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">Taqɨn</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">topɨsa</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">čʼaɣ</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">qonıšʼah</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_93" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">Tɨ</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">nɛnäqa</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">nannɛr</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">šip</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">sattaltɛːnta</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">ompašim</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">sukkulta</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">tüla</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_119" n="HIAT:w" s="T33">ılla</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_122" n="HIAT:w" s="T34">qontɛːjsaɣ</ts>
                  <nts id="Seg_123" n="HIAT:ip">.</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_126" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">Tap</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">šʼol</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">čʼeːl</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_137" n="HIAT:w" s="T38">merqɨja</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_140" n="HIAT:w" s="T39">puːla</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_143" n="HIAT:w" s="T40">tɨ</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_146" n="HIAT:w" s="T41">nɔːt</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_149" n="HIAT:w" s="T42">näčʼa</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_152" n="HIAT:w" s="T43">qonɨšʼaɣ</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_156" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_158" n="HIAT:w" s="T44">Šʼoiːmɨ</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_161" n="HIAT:w" s="T45">čʼaɣ</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_164" n="HIAT:w" s="T46">čʼapɨla</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_167" n="HIAT:w" s="T47">nı</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_170" n="HIAT:w" s="T48">meːqatɨ</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_173" n="HIAT:w" s="T49">qaj</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_176" n="HIAT:w" s="T50">ɔːmɨ</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_179" n="HIAT:w" s="T51">muntɨ</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_182" n="HIAT:w" s="T52">nɨttɔːtɨn</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_185" n="HIAT:w" s="T53">keksa</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_188" n="HIAT:w" s="T54">pɛlʼaktɨ</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_191" n="HIAT:w" s="T55">lıːpije</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_194" n="HIAT:w" s="T56">ippalimmɨnta</ts>
                  <nts id="Seg_195" n="HIAT:ip">.</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_198" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_200" n="HIAT:w" s="T57">Tam</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_203" n="HIAT:w" s="T58">lʼimtɨ</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_205" n="HIAT:ip">(</nts>
                  <ts e="T60" id="Seg_207" n="HIAT:w" s="T59">keksa</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_210" n="HIAT:w" s="T60">nʼant</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_213" n="HIAT:w" s="T61">pinɨlsa</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_216" n="HIAT:w" s="T62">kišta</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_219" n="HIAT:w" s="T63">lʼipɨ</ts>
                  <nts id="Seg_220" n="HIAT:ip">)</nts>
                  <nts id="Seg_221" n="HIAT:ip">.</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_224" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_226" n="HIAT:w" s="T64">Moqonä</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_229" n="HIAT:w" s="T65">tüla</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_232" n="HIAT:w" s="T66">tam</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_235" n="HIAT:w" s="T67">ɔːmnɛnta</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_238" n="HIAT:w" s="T68">nannɛr</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_241" n="HIAT:w" s="T69">nɛnäqa</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_244" n="HIAT:w" s="T70">kočʼe</ts>
                  <nts id="Seg_245" n="HIAT:ip">.</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_248" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_250" n="HIAT:w" s="T71">Ɔːmpa</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_253" n="HIAT:w" s="T72">tap</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_256" n="HIAT:w" s="T73">purqɨ</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_259" n="HIAT:w" s="T74">poje</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_262" n="HIAT:w" s="T75">qənqɨnoːqolampaɣ</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_266" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_268" n="HIAT:w" s="T76">Natqa</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_271" n="HIAT:w" s="T77">taqtaltɨsa</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_275" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_277" n="HIAT:w" s="T78">Aj</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_280" n="HIAT:w" s="T79">ukkɨr</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_283" n="HIAT:w" s="T80">na</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_286" n="HIAT:w" s="T81">nɛnäqi</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_289" n="HIAT:w" s="T82">qap</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_292" n="HIAT:w" s="T83">kočʼe</ts>
                  <nts id="Seg_293" n="HIAT:ip">.</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_296" n="HIAT:u" s="T84">
                  <nts id="Seg_297" n="HIAT:ip">(</nts>
                  <nts id="Seg_298" n="HIAT:ip">(</nts>
                  <ats e="T92" id="Seg_299" n="HIAT:non-pho" s="T84">…</ats>
                  <nts id="Seg_300" n="HIAT:ip">)</nts>
                  <nts id="Seg_301" n="HIAT:ip">)</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_304" n="HIAT:w" s="T92">Purqɨ</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_307" n="HIAT:w" s="T85">poje</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_310" n="HIAT:w" s="T86">kuttar</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_313" n="HIAT:w" s="T87">qončʼɛntaɣ</ts>
                  <nts id="Seg_314" n="HIAT:ip">.</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T89" id="Seg_316" n="sc" s="T0">
               <ts e="T7" id="Seg_318" n="e" s="T0">Марья Ивановна Кунина расскажет, как делает кирпичи. </ts>
               <ts e="T8" id="Seg_320" n="e" s="T7">Tɨmta </ts>
               <ts e="T9" id="Seg_322" n="e" s="T8">qar </ts>
               <ts e="T10" id="Seg_324" n="e" s="T9">nɨlčʼik </ts>
               <ts e="T11" id="Seg_326" n="e" s="T10">nat </ts>
               <ts e="T12" id="Seg_328" n="e" s="T11">qaj </ts>
               <ts e="T13" id="Seg_330" n="e" s="T12">qarɨn </ts>
               <ts e="T14" id="Seg_332" n="e" s="T13">ınna </ts>
               <ts e="T15" id="Seg_334" n="e" s="T14">wəšisaɣ. </ts>
               <ts e="T16" id="Seg_336" n="e" s="T15">Or </ts>
               <ts e="T17" id="Seg_338" n="e" s="T16">sära. </ts>
               <ts e="T18" id="Seg_340" n="e" s="T17">Nɛniqa </ts>
               <ts e="T19" id="Seg_342" n="e" s="T18">qap </ts>
               <ts e="T20" id="Seg_344" n="e" s="T19">kočʼe </ts>
               <ts e="T21" id="Seg_346" n="e" s="T20">ɛːɣa. </ts>
               <ts e="T22" id="Seg_348" n="e" s="T21">Taqɨn </ts>
               <ts e="T23" id="Seg_350" n="e" s="T22">topɨsa </ts>
               <ts e="T24" id="Seg_352" n="e" s="T23">čʼaɣ </ts>
               <ts e="T25" id="Seg_354" n="e" s="T24">qonıšʼah. </ts>
               <ts e="T26" id="Seg_356" n="e" s="T25">Tɨ </ts>
               <ts e="T27" id="Seg_358" n="e" s="T26">nɛnäqa </ts>
               <ts e="T28" id="Seg_360" n="e" s="T27">nannɛr </ts>
               <ts e="T29" id="Seg_362" n="e" s="T28">šip </ts>
               <ts e="T30" id="Seg_364" n="e" s="T29">sattaltɛːnta </ts>
               <ts e="T31" id="Seg_366" n="e" s="T30">ompašim </ts>
               <ts e="T32" id="Seg_368" n="e" s="T31">sukkulta </ts>
               <ts e="T33" id="Seg_370" n="e" s="T32">tüla </ts>
               <ts e="T34" id="Seg_372" n="e" s="T33">ılla </ts>
               <ts e="T35" id="Seg_374" n="e" s="T34">qontɛːjsaɣ. </ts>
               <ts e="T36" id="Seg_376" n="e" s="T35">Tap </ts>
               <ts e="T37" id="Seg_378" n="e" s="T36">šʼol </ts>
               <ts e="T38" id="Seg_380" n="e" s="T37">čʼeːl </ts>
               <ts e="T39" id="Seg_382" n="e" s="T38">merqɨja </ts>
               <ts e="T40" id="Seg_384" n="e" s="T39">puːla </ts>
               <ts e="T41" id="Seg_386" n="e" s="T40">tɨ </ts>
               <ts e="T42" id="Seg_388" n="e" s="T41">nɔːt </ts>
               <ts e="T43" id="Seg_390" n="e" s="T42">näčʼa </ts>
               <ts e="T44" id="Seg_392" n="e" s="T43">qonɨšʼaɣ. </ts>
               <ts e="T45" id="Seg_394" n="e" s="T44">Šʼoiːmɨ </ts>
               <ts e="T46" id="Seg_396" n="e" s="T45">čʼaɣ </ts>
               <ts e="T47" id="Seg_398" n="e" s="T46">čʼapɨla </ts>
               <ts e="T48" id="Seg_400" n="e" s="T47">nı </ts>
               <ts e="T49" id="Seg_402" n="e" s="T48">meːqatɨ </ts>
               <ts e="T50" id="Seg_404" n="e" s="T49">qaj </ts>
               <ts e="T51" id="Seg_406" n="e" s="T50">ɔːmɨ </ts>
               <ts e="T52" id="Seg_408" n="e" s="T51">muntɨ </ts>
               <ts e="T53" id="Seg_410" n="e" s="T52">nɨttɔːtɨn </ts>
               <ts e="T54" id="Seg_412" n="e" s="T53">keksa </ts>
               <ts e="T55" id="Seg_414" n="e" s="T54">pɛlʼaktɨ </ts>
               <ts e="T56" id="Seg_416" n="e" s="T55">lıːpije </ts>
               <ts e="T57" id="Seg_418" n="e" s="T56">ippalimmɨnta. </ts>
               <ts e="T58" id="Seg_420" n="e" s="T57">Tam </ts>
               <ts e="T59" id="Seg_422" n="e" s="T58">lʼimtɨ </ts>
               <ts e="T60" id="Seg_424" n="e" s="T59">(keksa </ts>
               <ts e="T61" id="Seg_426" n="e" s="T60">nʼant </ts>
               <ts e="T62" id="Seg_428" n="e" s="T61">pinɨlsa </ts>
               <ts e="T63" id="Seg_430" n="e" s="T62">kišta </ts>
               <ts e="T64" id="Seg_432" n="e" s="T63">lʼipɨ). </ts>
               <ts e="T65" id="Seg_434" n="e" s="T64">Moqonä </ts>
               <ts e="T66" id="Seg_436" n="e" s="T65">tüla </ts>
               <ts e="T67" id="Seg_438" n="e" s="T66">tam </ts>
               <ts e="T68" id="Seg_440" n="e" s="T67">ɔːmnɛnta </ts>
               <ts e="T69" id="Seg_442" n="e" s="T68">nannɛr </ts>
               <ts e="T70" id="Seg_444" n="e" s="T69">nɛnäqa </ts>
               <ts e="T71" id="Seg_446" n="e" s="T70">kočʼe. </ts>
               <ts e="T72" id="Seg_448" n="e" s="T71">Ɔːmpa </ts>
               <ts e="T73" id="Seg_450" n="e" s="T72">tap </ts>
               <ts e="T74" id="Seg_452" n="e" s="T73">purqɨ </ts>
               <ts e="T75" id="Seg_454" n="e" s="T74">poje </ts>
               <ts e="T76" id="Seg_456" n="e" s="T75">qənqɨnoːqolampaɣ. </ts>
               <ts e="T77" id="Seg_458" n="e" s="T76">Natqa </ts>
               <ts e="T78" id="Seg_460" n="e" s="T77">taqtaltɨsa. </ts>
               <ts e="T79" id="Seg_462" n="e" s="T78">Aj </ts>
               <ts e="T80" id="Seg_464" n="e" s="T79">ukkɨr </ts>
               <ts e="T81" id="Seg_466" n="e" s="T80">na </ts>
               <ts e="T82" id="Seg_468" n="e" s="T81">nɛnäqi </ts>
               <ts e="T83" id="Seg_470" n="e" s="T82">qap </ts>
               <ts e="T84" id="Seg_472" n="e" s="T83">kočʼe. </ts>
               <ts e="T92" id="Seg_474" n="e" s="T84">((…)) </ts>
               <ts e="T85" id="Seg_476" n="e" s="T92">Purqɨ </ts>
               <ts e="T86" id="Seg_478" n="e" s="T85">poje </ts>
               <ts e="T87" id="Seg_480" n="e" s="T86">kuttar </ts>
               <ts e="T89" id="Seg_482" n="e" s="T87">qončʼɛntaɣ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_483" s="T0">KuMI_196X_Mosquitos_nar.001 (001)</ta>
            <ta e="T15" id="Seg_484" s="T7">KuMI_196X_Mosquitos_nar.002 (002)</ta>
            <ta e="T17" id="Seg_485" s="T15">KuMI_196X_Mosquitos_nar.003 (003)</ta>
            <ta e="T21" id="Seg_486" s="T17">KuMI_196X_Mosquitos_nar.004 (004)</ta>
            <ta e="T25" id="Seg_487" s="T21">KuMI_196X_Mosquitos_nar.005 (005)</ta>
            <ta e="T35" id="Seg_488" s="T25">KuMI_196X_Mosquitos_nar.006 (006)</ta>
            <ta e="T44" id="Seg_489" s="T35">KuMI_196X_Mosquitos_nar.007 (007)</ta>
            <ta e="T57" id="Seg_490" s="T44">KuMI_196X_Mosquitos_nar.008 (008)</ta>
            <ta e="T64" id="Seg_491" s="T57">KuMI_196X_Mosquitos_nar.009 (009)</ta>
            <ta e="T71" id="Seg_492" s="T64">KuMI_196X_Mosquitos_nar.010 (010)</ta>
            <ta e="T76" id="Seg_493" s="T71">KuMI_196X_Mosquitos_nar.011 (011)</ta>
            <ta e="T78" id="Seg_494" s="T76">KuMI_196X_Mosquitos_nar.012 (012)</ta>
            <ta e="T84" id="Seg_495" s="T78">KuMI_196X_Mosquitos_nar.013 (013)</ta>
            <ta e="T89" id="Seg_496" s="T84">KuMI_196X_Mosquitos_nar.014 (014)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl">
            <ta e="T7" id="Seg_497" s="T0">Марья Ивановна Кунина расскажет, как делает кирпичи.</ta>
            <ta e="T15" id="Seg_498" s="T7">Tɨmta qar nɨlčʼi(k) nat qaj qar(ɨn) ınna wəšisaɣ.</ta>
            <ta e="T17" id="Seg_499" s="T15">Or sära.</ta>
            <ta e="T21" id="Seg_500" s="T17">Näniqa qap kočʼe ɛː(ɣa).</ta>
            <ta e="T25" id="Seg_501" s="T21">Taqɨn topɨsa čʼaɣ qonišʼah.</ta>
            <ta e="T35" id="Seg_502" s="T25">Tɨ nänäqa nannɛr šip sattaltɛːnta ompašim sukkulta tüla ılla qontɛːj(saɣ).</ta>
            <ta e="T44" id="Seg_503" s="T35">Tap šʼol (čʼeːl) merqɨja pula tɨ nɔːt nečʼa qonɨšʼaɣ.</ta>
            <ta e="T57" id="Seg_504" s="T44">Šʼoiːmɨ čʼaɣ čʼapɨla nı meːqatɨ qaj omɨ munt tɨnɨttɔːtɨn keksa pelʼaktɨ lʼipije ippalʼimmɨnta.</ta>
            <ta e="T64" id="Seg_505" s="T57">Tam lʼimtɨ keksa nʼant piŋɨlsa kišta lʼipɨ.</ta>
            <ta e="T71" id="Seg_506" s="T64">Moqonä tüla tam omnɛnta nänäqa nannɛr kočʼe.</ta>
            <ta e="T76" id="Seg_507" s="T71">Ɔːmpa tap purqɨ poje qənqɨnɔqolampaɣ.</ta>
            <ta e="T78" id="Seg_508" s="T76">Natqa taqtaltɨsa.</ta>
            <ta e="T84" id="Seg_509" s="T78">Aj ukkɨr na nänäqi qap kočʼe.</ta>
            <ta e="T89" id="Seg_510" s="T84">Purqɨ poje kuttar qončʼentaɣ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_511" s="T0">Марья Ивановна Кунина расскажет, как делает кирпичи. </ta>
            <ta e="T15" id="Seg_512" s="T7">Tɨmta qar nɨlčʼik nat qaj qarɨn ınna wəšisaɣ. </ta>
            <ta e="T17" id="Seg_513" s="T15">Or sära. </ta>
            <ta e="T21" id="Seg_514" s="T17">Nɛniqa qap kočʼe ɛːɣa. </ta>
            <ta e="T25" id="Seg_515" s="T21">Taqɨn topɨsa čʼaɣ qonıšʼah. </ta>
            <ta e="T35" id="Seg_516" s="T25">Tɨ nɛnäqa nannɛr šip sattaltɛːnta ompašim sukkulta tüla ılla qontɛːjsaɣ. </ta>
            <ta e="T44" id="Seg_517" s="T35">Tap šʼol čʼeːl merqɨja puːla tɨ nɔːt näčʼa qonɨšʼaɣ. </ta>
            <ta e="T57" id="Seg_518" s="T44">Šʼoiːmɨ čʼaɣ čʼapɨla nı meːqatɨ qaj ɔːmɨ muntɨ nɨttɔːtɨn keksa pɛlʼaktɨ lıːpije ippalimmɨnta. </ta>
            <ta e="T64" id="Seg_519" s="T57">Tam lʼimtɨ (keksa nʼant pinɨlsa kišta lʼipɨ). </ta>
            <ta e="T71" id="Seg_520" s="T64">Moqonä tüla tam ɔːmnɛnta nannɛr nɛnäqa kočʼe. </ta>
            <ta e="T76" id="Seg_521" s="T71">Ɔːmpa tap purqɨ poje qənqɨnoːqolampaɣ. </ta>
            <ta e="T78" id="Seg_522" s="T76">Natqa taqtaltɨsa. </ta>
            <ta e="T84" id="Seg_523" s="T78">Aj ukkɨr na nɛnäqi qap kočʼe. </ta>
            <ta e="T89" id="Seg_524" s="T84">((…)) Purqɨ poje kuttar qončʼɛntaɣ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T8" id="Seg_525" s="T7">tɨmta</ta>
            <ta e="T9" id="Seg_526" s="T8">qar</ta>
            <ta e="T10" id="Seg_527" s="T9">nɨlčʼi-k</ta>
            <ta e="T11" id="Seg_528" s="T10">na-t</ta>
            <ta e="T12" id="Seg_529" s="T11">qaj</ta>
            <ta e="T13" id="Seg_530" s="T12">qarɨ-n</ta>
            <ta e="T14" id="Seg_531" s="T13">ınna</ta>
            <ta e="T15" id="Seg_532" s="T14">wəši-sa-ɣ</ta>
            <ta e="T16" id="Seg_533" s="T15">or</ta>
            <ta e="T17" id="Seg_534" s="T16">sära</ta>
            <ta e="T18" id="Seg_535" s="T17">nɛniqa</ta>
            <ta e="T19" id="Seg_536" s="T18">qap</ta>
            <ta e="T20" id="Seg_537" s="T19">kočʼe</ta>
            <ta e="T21" id="Seg_538" s="T20">ɛː-ɣa</ta>
            <ta e="T22" id="Seg_539" s="T21">taqɨ-n</ta>
            <ta e="T23" id="Seg_540" s="T22">topɨ-sa</ta>
            <ta e="T24" id="Seg_541" s="T23">čʼaɣ</ta>
            <ta e="T25" id="Seg_542" s="T24">qonı-šʼa-h</ta>
            <ta e="T26" id="Seg_543" s="T25">tɨ</ta>
            <ta e="T27" id="Seg_544" s="T26">nɛnäqa</ta>
            <ta e="T28" id="Seg_545" s="T27">nannɛr</ta>
            <ta e="T29" id="Seg_546" s="T28">šip</ta>
            <ta e="T30" id="Seg_547" s="T29">satt-alt-ɛː-nta</ta>
            <ta e="T31" id="Seg_548" s="T30">ompašim</ta>
            <ta e="T32" id="Seg_549" s="T31">sukkulta</ta>
            <ta e="T33" id="Seg_550" s="T32">tü-la</ta>
            <ta e="T34" id="Seg_551" s="T33">ılla</ta>
            <ta e="T35" id="Seg_552" s="T34">qont-ɛːj-sa-ɣ</ta>
            <ta e="T36" id="Seg_553" s="T35">tap</ta>
            <ta e="T37" id="Seg_554" s="T36">šʼol</ta>
            <ta e="T38" id="Seg_555" s="T37">čʼeːl</ta>
            <ta e="T39" id="Seg_556" s="T38">merqɨ-ja</ta>
            <ta e="T40" id="Seg_557" s="T39">puːla</ta>
            <ta e="T41" id="Seg_558" s="T40">tɨ</ta>
            <ta e="T42" id="Seg_559" s="T41">nɔːt</ta>
            <ta e="T43" id="Seg_560" s="T42">näčʼa</ta>
            <ta e="T44" id="Seg_561" s="T43">qonɨ-šʼa-ɣ</ta>
            <ta e="T45" id="Seg_562" s="T44">šʼo-iː-mɨ</ta>
            <ta e="T46" id="Seg_563" s="T45">čʼaɣ</ta>
            <ta e="T47" id="Seg_564" s="T46">čʼapɨ-la</ta>
            <ta e="T48" id="Seg_565" s="T47">nı</ta>
            <ta e="T49" id="Seg_566" s="T48">meː-qa-tɨ</ta>
            <ta e="T50" id="Seg_567" s="T49">qaj</ta>
            <ta e="T51" id="Seg_568" s="T50">ɔːmɨ</ta>
            <ta e="T52" id="Seg_569" s="T51">muntɨ</ta>
            <ta e="T53" id="Seg_570" s="T52">nɨt-tɔː-tɨn</ta>
            <ta e="T54" id="Seg_571" s="T53">keksa</ta>
            <ta e="T55" id="Seg_572" s="T54">pɛlʼak-tɨ</ta>
            <ta e="T56" id="Seg_573" s="T55">lıːpi-je</ta>
            <ta e="T57" id="Seg_574" s="T56">ipp-ali-mmɨ-nta</ta>
            <ta e="T58" id="Seg_575" s="T57">tam</ta>
            <ta e="T59" id="Seg_576" s="T58">lʼi-m-tɨ</ta>
            <ta e="T60" id="Seg_577" s="T59">keksa</ta>
            <ta e="T61" id="Seg_578" s="T60">nʼant</ta>
            <ta e="T62" id="Seg_579" s="T61">pin-ɨ-l-sa</ta>
            <ta e="T63" id="Seg_580" s="T62">kišta</ta>
            <ta e="T64" id="Seg_581" s="T63">lʼipɨ</ta>
            <ta e="T65" id="Seg_582" s="T64">moqonä</ta>
            <ta e="T66" id="Seg_583" s="T65">tü-la</ta>
            <ta e="T67" id="Seg_584" s="T66">tam</ta>
            <ta e="T68" id="Seg_585" s="T67">ɔːmnɛ-nta</ta>
            <ta e="T69" id="Seg_586" s="T68">nannɛr</ta>
            <ta e="T70" id="Seg_587" s="T69">nɛnäqa</ta>
            <ta e="T71" id="Seg_588" s="T70">kočʼe</ta>
            <ta e="T72" id="Seg_589" s="T71">ɔːmpa</ta>
            <ta e="T73" id="Seg_590" s="T72">tap</ta>
            <ta e="T74" id="Seg_591" s="T73">purqɨ</ta>
            <ta e="T75" id="Seg_592" s="T74">po-je</ta>
            <ta e="T76" id="Seg_593" s="T75">qən-qɨ-noːq-olam-pa-ɣ</ta>
            <ta e="T77" id="Seg_594" s="T76">na-tqa</ta>
            <ta e="T78" id="Seg_595" s="T77">taqt-altɨ-sa</ta>
            <ta e="T79" id="Seg_596" s="T78">aj</ta>
            <ta e="T80" id="Seg_597" s="T79">ukkɨr</ta>
            <ta e="T81" id="Seg_598" s="T80">na</ta>
            <ta e="T82" id="Seg_599" s="T81">nɛnäqi</ta>
            <ta e="T83" id="Seg_600" s="T82">qap</ta>
            <ta e="T84" id="Seg_601" s="T83">kočʼe</ta>
            <ta e="T85" id="Seg_602" s="T92">purqɨ</ta>
            <ta e="T86" id="Seg_603" s="T85">po-je</ta>
            <ta e="T87" id="Seg_604" s="T86">kuttar</ta>
            <ta e="T89" id="Seg_605" s="T87">qon-čʼɛnta-ɣ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T8" id="Seg_606" s="T7">tɨmtɨ</ta>
            <ta e="T9" id="Seg_607" s="T8">qarɨ</ta>
            <ta e="T10" id="Seg_608" s="T9">nılʼčʼɨ-k</ta>
            <ta e="T11" id="Seg_609" s="T10">na-n</ta>
            <ta e="T12" id="Seg_610" s="T11">qaj</ta>
            <ta e="T13" id="Seg_611" s="T12">qarɨ-n</ta>
            <ta e="T14" id="Seg_612" s="T13">ınnä</ta>
            <ta e="T15" id="Seg_613" s="T14">wəšɨ-sɨ-k</ta>
            <ta e="T16" id="Seg_614" s="T15">orɨ</ta>
            <ta e="T17" id="Seg_615" s="T16">səːrɨ</ta>
            <ta e="T18" id="Seg_616" s="T17">nɛnɨqa</ta>
            <ta e="T19" id="Seg_617" s="T18">qapı</ta>
            <ta e="T20" id="Seg_618" s="T19">kočʼčʼɨ</ta>
            <ta e="T21" id="Seg_619" s="T20">ɛː-ŋɨ</ta>
            <ta e="T22" id="Seg_620" s="T21">taŋɨ-n</ta>
            <ta e="T23" id="Seg_621" s="T22">topɨ-sä</ta>
            <ta e="T24" id="Seg_622" s="T23">čʼaɣ</ta>
            <ta e="T25" id="Seg_623" s="T24">*qonı-sɨ-k</ta>
            <ta e="T26" id="Seg_624" s="T25">tıː</ta>
            <ta e="T27" id="Seg_625" s="T26">nɛnɨqa</ta>
            <ta e="T28" id="Seg_626" s="T27">nannɛr</ta>
            <ta e="T29" id="Seg_627" s="T28">mašım</ta>
            <ta e="T30" id="Seg_628" s="T29">satɨ-altɨ-ɛː-ntɨ</ta>
            <ta e="T31" id="Seg_629" s="T30">ompä</ta>
            <ta e="T32" id="Seg_630" s="T31">sukɨltä</ta>
            <ta e="T33" id="Seg_631" s="T32">tü-lä</ta>
            <ta e="T34" id="Seg_632" s="T33">ıllä</ta>
            <ta e="T35" id="Seg_633" s="T34">qontɨ-ɛː-sɨ-k</ta>
            <ta e="T36" id="Seg_634" s="T35">tam</ta>
            <ta e="T37" id="Seg_635" s="T36">šʼol</ta>
            <ta e="T38" id="Seg_636" s="T37">čʼeːlɨ</ta>
            <ta e="T39" id="Seg_637" s="T38">merqɨ-lä</ta>
            <ta e="T40" id="Seg_638" s="T39">puːlä</ta>
            <ta e="T41" id="Seg_639" s="T40">tıː</ta>
            <ta e="T42" id="Seg_640" s="T41">nɔːtɨ</ta>
            <ta e="T43" id="Seg_641" s="T42">näčʼčʼä</ta>
            <ta e="T44" id="Seg_642" s="T43">*qonı-sɨ-k</ta>
            <ta e="T45" id="Seg_643" s="T44">sö-iː-mɨ</ta>
            <ta e="T46" id="Seg_644" s="T45">čʼaɣ</ta>
            <ta e="T47" id="Seg_645" s="T46">čʼapɨ-lä</ta>
            <ta e="T48" id="Seg_646" s="T47">nık</ta>
            <ta e="T49" id="Seg_647" s="T48">meː-kkɨ-tɨ</ta>
            <ta e="T50" id="Seg_648" s="T49">qaj</ta>
            <ta e="T51" id="Seg_649" s="T50">ɔːmɨ</ta>
            <ta e="T52" id="Seg_650" s="T51">muntɨk</ta>
            <ta e="T53" id="Seg_651" s="T52">nɨta-ttɨ-tɨt</ta>
            <ta e="T54" id="Seg_652" s="T53">kekkɨsä</ta>
            <ta e="T55" id="Seg_653" s="T54">pɛläk-tɨ</ta>
            <ta e="T56" id="Seg_654" s="T55">lıːpɨ-je</ta>
            <ta e="T57" id="Seg_655" s="T56">ippɨ-kolʼčʼimpɨ-mpɨ-ntɨ</ta>
            <ta e="T58" id="Seg_656" s="T57">tam</ta>
            <ta e="T59" id="Seg_657" s="T58">lıːpɨ-m-tɨ</ta>
            <ta e="T60" id="Seg_658" s="T59">kekkɨsä</ta>
            <ta e="T61" id="Seg_659" s="T60">nʼentɨ</ta>
            <ta e="T62" id="Seg_660" s="T61">pin-ɨ-lɨ-sɨ</ta>
            <ta e="T63" id="Seg_661" s="T62">kišta</ta>
            <ta e="T64" id="Seg_662" s="T63">lıːpɨ</ta>
            <ta e="T65" id="Seg_663" s="T64">moqɨnä</ta>
            <ta e="T66" id="Seg_664" s="T65">tü-lä</ta>
            <ta e="T67" id="Seg_665" s="T66">tam</ta>
            <ta e="T68" id="Seg_666" s="T67">ɔːmtɨ-ntɨ</ta>
            <ta e="T69" id="Seg_667" s="T68">nannɛr</ta>
            <ta e="T70" id="Seg_668" s="T69">nɛnɨqa</ta>
            <ta e="T71" id="Seg_669" s="T70">kočʼčʼɨ</ta>
            <ta e="T72" id="Seg_670" s="T71">ompä</ta>
            <ta e="T73" id="Seg_671" s="T72">tam</ta>
            <ta e="T74" id="Seg_672" s="T73">purqɨ</ta>
            <ta e="T75" id="Seg_673" s="T74">poː-je</ta>
            <ta e="T76" id="Seg_674" s="T75">qən-kkɨ-noːqo-olam-mpɨ-k</ta>
            <ta e="T77" id="Seg_675" s="T76">na-tqo</ta>
            <ta e="T78" id="Seg_676" s="T77">*taqtɨ-altɨ-sɨ</ta>
            <ta e="T79" id="Seg_677" s="T78">aj</ta>
            <ta e="T80" id="Seg_678" s="T79">ukkɨr</ta>
            <ta e="T81" id="Seg_679" s="T80">na</ta>
            <ta e="T82" id="Seg_680" s="T81">nɛnɨqa</ta>
            <ta e="T83" id="Seg_681" s="T82">qapı</ta>
            <ta e="T84" id="Seg_682" s="T83">kočʼčʼɨ</ta>
            <ta e="T85" id="Seg_683" s="T92">purqɨ</ta>
            <ta e="T86" id="Seg_684" s="T85">poː-je</ta>
            <ta e="T87" id="Seg_685" s="T86">kuttar</ta>
            <ta e="T89" id="Seg_686" s="T87">qən-ɛntɨ-k</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T8" id="Seg_687" s="T7">here</ta>
            <ta e="T9" id="Seg_688" s="T8">morning.[NOM]</ta>
            <ta e="T10" id="Seg_689" s="T9">such-ADVZ</ta>
            <ta e="T11" id="Seg_690" s="T10">this-GEN</ta>
            <ta e="T12" id="Seg_691" s="T11">what.[NOM]</ta>
            <ta e="T13" id="Seg_692" s="T12">morning-ADV.LOC</ta>
            <ta e="T14" id="Seg_693" s="T13">up</ta>
            <ta e="T15" id="Seg_694" s="T14">get.up-PST-1SG.S</ta>
            <ta e="T16" id="Seg_695" s="T15">force.[NOM]</ta>
            <ta e="T17" id="Seg_696" s="T16">rain.[3SG.S]</ta>
            <ta e="T18" id="Seg_697" s="T17">mosquito.[NOM]</ta>
            <ta e="T19" id="Seg_698" s="T18">supposedly</ta>
            <ta e="T20" id="Seg_699" s="T19">much</ta>
            <ta e="T21" id="Seg_700" s="T20">be-CO.[3SG.S]</ta>
            <ta e="T22" id="Seg_701" s="T21">summer-ADV.LOC</ta>
            <ta e="T23" id="Seg_702" s="T22">leg-INSTR</ta>
            <ta e="T24" id="Seg_703" s="T23">%%</ta>
            <ta e="T25" id="Seg_704" s="T24">go.to-PST-1SG.S</ta>
            <ta e="T26" id="Seg_705" s="T25">now</ta>
            <ta e="T27" id="Seg_706" s="T26">mosquito.[NOM]</ta>
            <ta e="T28" id="Seg_707" s="T27">so.much</ta>
            <ta e="T29" id="Seg_708" s="T28">I.ACC</ta>
            <ta e="T30" id="Seg_709" s="T29">bite-TR-PFV-INFER.[3SG.S]</ta>
            <ta e="T31" id="Seg_710" s="T30">soon</ta>
            <ta e="T32" id="Seg_711" s="T31">back</ta>
            <ta e="T33" id="Seg_712" s="T32">come-CVB</ta>
            <ta e="T34" id="Seg_713" s="T33">down</ta>
            <ta e="T35" id="Seg_714" s="T34">sleep-PFV-PST-1SG.S</ta>
            <ta e="T36" id="Seg_715" s="T35">this</ta>
            <ta e="T37" id="Seg_716" s="T36">%%</ta>
            <ta e="T38" id="Seg_717" s="T37">day.[NOM]</ta>
            <ta e="T39" id="Seg_718" s="T38">blow-CVB</ta>
            <ta e="T40" id="Seg_719" s="T39">after</ta>
            <ta e="T41" id="Seg_720" s="T40">now</ta>
            <ta e="T42" id="Seg_721" s="T41">then</ta>
            <ta e="T43" id="Seg_722" s="T42">there</ta>
            <ta e="T44" id="Seg_723" s="T43">go.to-PST-1SG.S</ta>
            <ta e="T45" id="Seg_724" s="T44">ground-PL.[NOM]-1SG</ta>
            <ta e="T46" id="Seg_725" s="T45">%%</ta>
            <ta e="T47" id="Seg_726" s="T46">drop-CVB</ta>
            <ta e="T48" id="Seg_727" s="T47">so</ta>
            <ta e="T49" id="Seg_728" s="T48">make-DUR-3SG.O</ta>
            <ta e="T50" id="Seg_729" s="T49">what.[NOM]</ta>
            <ta e="T51" id="Seg_730" s="T50">some</ta>
            <ta e="T52" id="Seg_731" s="T51">all</ta>
            <ta e="T53" id="Seg_732" s="T52">tear-DETR-3PL</ta>
            <ta e="T54" id="Seg_733" s="T53">barely</ta>
            <ta e="T55" id="Seg_734" s="T54">half.[NOM]-3SG</ta>
            <ta e="T56" id="Seg_735" s="T55">piece-%%</ta>
            <ta e="T57" id="Seg_736" s="T56">lie-MULT-HAB-INFER.[3SG.S]</ta>
            <ta e="T58" id="Seg_737" s="T57">this</ta>
            <ta e="T59" id="Seg_738" s="T58">piece-ACC-3SG</ta>
            <ta e="T60" id="Seg_739" s="T59">barely</ta>
            <ta e="T61" id="Seg_740" s="T60">together</ta>
            <ta e="T62" id="Seg_741" s="T61">put-EP-RES-PST.[3SG.S]</ta>
            <ta e="T63" id="Seg_742" s="T62">%%</ta>
            <ta e="T64" id="Seg_743" s="T63">piece.[NOM]</ta>
            <ta e="T65" id="Seg_744" s="T64">home</ta>
            <ta e="T66" id="Seg_745" s="T65">come-CVB</ta>
            <ta e="T67" id="Seg_746" s="T66">this</ta>
            <ta e="T68" id="Seg_747" s="T67">sit-INFER.[3SG.S]</ta>
            <ta e="T69" id="Seg_748" s="T68">so.much</ta>
            <ta e="T70" id="Seg_749" s="T69">mosquito.[NOM]</ta>
            <ta e="T71" id="Seg_750" s="T70">much</ta>
            <ta e="T72" id="Seg_751" s="T71">soon</ta>
            <ta e="T73" id="Seg_752" s="T72">this</ta>
            <ta e="T74" id="Seg_753" s="T73">smoke.[NOM]</ta>
            <ta e="T75" id="Seg_754" s="T74">tree-%%</ta>
            <ta e="T76" id="Seg_755" s="T75">leave-DUR-1SG.TRL-be.going.to-HAB-1SG.S</ta>
            <ta e="T77" id="Seg_756" s="T76">this-TRL</ta>
            <ta e="T78" id="Seg_757" s="T77">dress-TR-PST.[3SG.S]</ta>
            <ta e="T79" id="Seg_758" s="T78">again</ta>
            <ta e="T80" id="Seg_759" s="T79">one</ta>
            <ta e="T81" id="Seg_760" s="T80">this</ta>
            <ta e="T82" id="Seg_761" s="T81">mosquito.[NOM]</ta>
            <ta e="T83" id="Seg_762" s="T82">supposedly</ta>
            <ta e="T84" id="Seg_763" s="T83">much</ta>
            <ta e="T85" id="Seg_764" s="T92">smoke.[NOM]</ta>
            <ta e="T86" id="Seg_765" s="T85">tree-%%</ta>
            <ta e="T87" id="Seg_766" s="T86">how</ta>
            <ta e="T89" id="Seg_767" s="T87">leave-FUT-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T8" id="Seg_768" s="T7">здесь</ta>
            <ta e="T9" id="Seg_769" s="T8">утро.[NOM]</ta>
            <ta e="T10" id="Seg_770" s="T9">такой-ADVZ</ta>
            <ta e="T11" id="Seg_771" s="T10">это-GEN</ta>
            <ta e="T12" id="Seg_772" s="T11">что.[NOM]</ta>
            <ta e="T13" id="Seg_773" s="T12">утро-ADV.LOC</ta>
            <ta e="T14" id="Seg_774" s="T13">вверх</ta>
            <ta e="T15" id="Seg_775" s="T14">встать-PST-1SG.S</ta>
            <ta e="T16" id="Seg_776" s="T15">сила.[NOM]</ta>
            <ta e="T17" id="Seg_777" s="T16">дождить.[3SG.S]</ta>
            <ta e="T18" id="Seg_778" s="T17">комар.[NOM]</ta>
            <ta e="T19" id="Seg_779" s="T18">вроде</ta>
            <ta e="T20" id="Seg_780" s="T19">много</ta>
            <ta e="T21" id="Seg_781" s="T20">быть-CO.[3SG.S]</ta>
            <ta e="T22" id="Seg_782" s="T21">лето-ADV.LOC</ta>
            <ta e="T23" id="Seg_783" s="T22">нога-INSTR</ta>
            <ta e="T24" id="Seg_784" s="T23">%%</ta>
            <ta e="T25" id="Seg_785" s="T24">ходить.за.чем_либо-PST-1SG.S</ta>
            <ta e="T26" id="Seg_786" s="T25">сейчас</ta>
            <ta e="T27" id="Seg_787" s="T26">комар.[NOM]</ta>
            <ta e="T28" id="Seg_788" s="T27">настолько</ta>
            <ta e="T29" id="Seg_789" s="T28">я.ACC</ta>
            <ta e="T30" id="Seg_790" s="T29">куснуть-TR-PFV-INFER.[3SG.S]</ta>
            <ta e="T31" id="Seg_791" s="T30">скоро</ta>
            <ta e="T32" id="Seg_792" s="T31">обратно</ta>
            <ta e="T33" id="Seg_793" s="T32">прийти-CVB</ta>
            <ta e="T34" id="Seg_794" s="T33">вниз</ta>
            <ta e="T35" id="Seg_795" s="T34">спать-PFV-PST-1SG.S</ta>
            <ta e="T36" id="Seg_796" s="T35">этот</ta>
            <ta e="T37" id="Seg_797" s="T36">%%</ta>
            <ta e="T38" id="Seg_798" s="T37">день.[NOM]</ta>
            <ta e="T39" id="Seg_799" s="T38">ветрить-CVB</ta>
            <ta e="T40" id="Seg_800" s="T39">после</ta>
            <ta e="T41" id="Seg_801" s="T40">сейчас</ta>
            <ta e="T42" id="Seg_802" s="T41">затем</ta>
            <ta e="T43" id="Seg_803" s="T42">туда</ta>
            <ta e="T44" id="Seg_804" s="T43">ходить.за.чем_либо-PST-1SG.S</ta>
            <ta e="T45" id="Seg_805" s="T44">земля-PL.[NOM]-1SG</ta>
            <ta e="T46" id="Seg_806" s="T45">%%</ta>
            <ta e="T47" id="Seg_807" s="T46">капать-CVB</ta>
            <ta e="T48" id="Seg_808" s="T47">так</ta>
            <ta e="T49" id="Seg_809" s="T48">сделать-DUR-3SG.O</ta>
            <ta e="T50" id="Seg_810" s="T49">что.[NOM]</ta>
            <ta e="T51" id="Seg_811" s="T50">некоторый</ta>
            <ta e="T52" id="Seg_812" s="T51">всё</ta>
            <ta e="T53" id="Seg_813" s="T52">рвать-DETR-3PL</ta>
            <ta e="T54" id="Seg_814" s="T53">еле.еле</ta>
            <ta e="T55" id="Seg_815" s="T54">половина.[NOM]-3SG</ta>
            <ta e="T56" id="Seg_816" s="T55">кусок-%%</ta>
            <ta e="T57" id="Seg_817" s="T56">лежать-MULT-HAB-INFER.[3SG.S]</ta>
            <ta e="T58" id="Seg_818" s="T57">этот</ta>
            <ta e="T59" id="Seg_819" s="T58">кусок-ACC-3SG</ta>
            <ta e="T60" id="Seg_820" s="T59">еле.еле</ta>
            <ta e="T61" id="Seg_821" s="T60">вместе</ta>
            <ta e="T62" id="Seg_822" s="T61">положить-EP-RES-PST.[3SG.S]</ta>
            <ta e="T63" id="Seg_823" s="T62">%%</ta>
            <ta e="T64" id="Seg_824" s="T63">кусок.[NOM]</ta>
            <ta e="T65" id="Seg_825" s="T64">домой</ta>
            <ta e="T66" id="Seg_826" s="T65">прийти-CVB</ta>
            <ta e="T67" id="Seg_827" s="T66">этот</ta>
            <ta e="T68" id="Seg_828" s="T67">сидеть-INFER.[3SG.S]</ta>
            <ta e="T69" id="Seg_829" s="T68">настолько</ta>
            <ta e="T70" id="Seg_830" s="T69">комар.[NOM]</ta>
            <ta e="T71" id="Seg_831" s="T70">много</ta>
            <ta e="T72" id="Seg_832" s="T71">скоро</ta>
            <ta e="T73" id="Seg_833" s="T72">этот</ta>
            <ta e="T74" id="Seg_834" s="T73">дым.[NOM]</ta>
            <ta e="T75" id="Seg_835" s="T74">дерево-%%</ta>
            <ta e="T76" id="Seg_836" s="T75">отправиться-DUR-1SG.TRL-собраться-HAB-1SG.S</ta>
            <ta e="T77" id="Seg_837" s="T76">этот-TRL</ta>
            <ta e="T78" id="Seg_838" s="T77">одеть(ся)-TR-PST.[3SG.S]</ta>
            <ta e="T79" id="Seg_839" s="T78">опять</ta>
            <ta e="T80" id="Seg_840" s="T79">один</ta>
            <ta e="T81" id="Seg_841" s="T80">это</ta>
            <ta e="T82" id="Seg_842" s="T81">комар.[NOM]</ta>
            <ta e="T83" id="Seg_843" s="T82">вроде</ta>
            <ta e="T84" id="Seg_844" s="T83">много</ta>
            <ta e="T85" id="Seg_845" s="T92">дым.[NOM]</ta>
            <ta e="T86" id="Seg_846" s="T85">дерево-%%</ta>
            <ta e="T87" id="Seg_847" s="T86">как</ta>
            <ta e="T89" id="Seg_848" s="T87">отправиться-FUT-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T8" id="Seg_849" s="T7">adv</ta>
            <ta e="T9" id="Seg_850" s="T8">n-n:case3</ta>
            <ta e="T10" id="Seg_851" s="T9">dem-adj&gt;adv</ta>
            <ta e="T11" id="Seg_852" s="T10">pro-n:case3</ta>
            <ta e="T12" id="Seg_853" s="T11">interrog-n:case3</ta>
            <ta e="T13" id="Seg_854" s="T12">n-n&gt;adv</ta>
            <ta e="T14" id="Seg_855" s="T13">preverb</ta>
            <ta e="T15" id="Seg_856" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_857" s="T15">n-n:case3</ta>
            <ta e="T17" id="Seg_858" s="T16">v-v:pn</ta>
            <ta e="T18" id="Seg_859" s="T17">n-n:case3</ta>
            <ta e="T19" id="Seg_860" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_861" s="T19">quant</ta>
            <ta e="T21" id="Seg_862" s="T20">v-v:ins-v:pn</ta>
            <ta e="T22" id="Seg_863" s="T21">n-n&gt;adv</ta>
            <ta e="T23" id="Seg_864" s="T22">n-n:case3</ta>
            <ta e="T24" id="Seg_865" s="T23">adv</ta>
            <ta e="T25" id="Seg_866" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_867" s="T25">adv</ta>
            <ta e="T27" id="Seg_868" s="T26">n-n:case3</ta>
            <ta e="T28" id="Seg_869" s="T27">adv</ta>
            <ta e="T29" id="Seg_870" s="T28">pers</ta>
            <ta e="T30" id="Seg_871" s="T29">v-v&gt;v-v&gt;v-v:tense-mood-v:pn</ta>
            <ta e="T31" id="Seg_872" s="T30">adv</ta>
            <ta e="T32" id="Seg_873" s="T31">adv</ta>
            <ta e="T33" id="Seg_874" s="T32">v-v&gt;adv</ta>
            <ta e="T34" id="Seg_875" s="T33">preverb</ta>
            <ta e="T35" id="Seg_876" s="T34">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_877" s="T35">dem</ta>
            <ta e="T37" id="Seg_878" s="T36">%%</ta>
            <ta e="T38" id="Seg_879" s="T37">n-n:case3</ta>
            <ta e="T39" id="Seg_880" s="T38">v-v&gt;adv</ta>
            <ta e="T40" id="Seg_881" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_882" s="T40">adv</ta>
            <ta e="T42" id="Seg_883" s="T41">adv</ta>
            <ta e="T43" id="Seg_884" s="T42">adv</ta>
            <ta e="T44" id="Seg_885" s="T43">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_886" s="T44">n-n:num-n:case1-n:poss</ta>
            <ta e="T46" id="Seg_887" s="T45">adv</ta>
            <ta e="T47" id="Seg_888" s="T46">v-v&gt;adv</ta>
            <ta e="T48" id="Seg_889" s="T47">adv</ta>
            <ta e="T49" id="Seg_890" s="T48">v-v&gt;v-v:pn</ta>
            <ta e="T50" id="Seg_891" s="T49">interrog-n:case3</ta>
            <ta e="T51" id="Seg_892" s="T50">adj</ta>
            <ta e="T52" id="Seg_893" s="T51">quant</ta>
            <ta e="T53" id="Seg_894" s="T52">v-v&gt;v-v:pn</ta>
            <ta e="T54" id="Seg_895" s="T53">adv</ta>
            <ta e="T55" id="Seg_896" s="T54">n-n:case3-n:poss</ta>
            <ta e="T56" id="Seg_897" s="T55">n-n:case3</ta>
            <ta e="T57" id="Seg_898" s="T56">v-v&gt;v-v&gt;v-v:tense-mood-v:pn</ta>
            <ta e="T58" id="Seg_899" s="T57">dem</ta>
            <ta e="T59" id="Seg_900" s="T58">n-n:case1-n:poss</ta>
            <ta e="T60" id="Seg_901" s="T59">adv</ta>
            <ta e="T61" id="Seg_902" s="T60">adv</ta>
            <ta e="T62" id="Seg_903" s="T61">v-n:(ins)-v&gt;v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_904" s="T62">%%</ta>
            <ta e="T64" id="Seg_905" s="T63">n-n:case3</ta>
            <ta e="T65" id="Seg_906" s="T64">adv</ta>
            <ta e="T66" id="Seg_907" s="T65">v-v&gt;adv</ta>
            <ta e="T67" id="Seg_908" s="T66">dem</ta>
            <ta e="T68" id="Seg_909" s="T67">v-v:tense-mood-v:pn</ta>
            <ta e="T69" id="Seg_910" s="T68">adv</ta>
            <ta e="T70" id="Seg_911" s="T69">n-n:case3</ta>
            <ta e="T71" id="Seg_912" s="T70">quant</ta>
            <ta e="T72" id="Seg_913" s="T71">adv</ta>
            <ta e="T73" id="Seg_914" s="T72">dem</ta>
            <ta e="T74" id="Seg_915" s="T73">n-n:case3</ta>
            <ta e="T75" id="Seg_916" s="T74">n-n:case3</ta>
            <ta e="T76" id="Seg_917" s="T75">v-v&gt;v-n:poss-case-v-v&gt;v-v:pn</ta>
            <ta e="T77" id="Seg_918" s="T76">dem-n:case3</ta>
            <ta e="T78" id="Seg_919" s="T77">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_920" s="T78">adv</ta>
            <ta e="T80" id="Seg_921" s="T79">num</ta>
            <ta e="T81" id="Seg_922" s="T80">pro</ta>
            <ta e="T82" id="Seg_923" s="T81">n-n:case3</ta>
            <ta e="T83" id="Seg_924" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_925" s="T83">quant</ta>
            <ta e="T85" id="Seg_926" s="T92">n-n:case3</ta>
            <ta e="T86" id="Seg_927" s="T85">n-n:case3</ta>
            <ta e="T87" id="Seg_928" s="T86">conj</ta>
            <ta e="T89" id="Seg_929" s="T87">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T8" id="Seg_930" s="T7">adv</ta>
            <ta e="T9" id="Seg_931" s="T8">n</ta>
            <ta e="T10" id="Seg_932" s="T9">dem</ta>
            <ta e="T11" id="Seg_933" s="T10">pro</ta>
            <ta e="T12" id="Seg_934" s="T11">interrog</ta>
            <ta e="T13" id="Seg_935" s="T12">adv</ta>
            <ta e="T14" id="Seg_936" s="T13">preverb</ta>
            <ta e="T15" id="Seg_937" s="T14">v</ta>
            <ta e="T16" id="Seg_938" s="T15">n</ta>
            <ta e="T17" id="Seg_939" s="T16">v</ta>
            <ta e="T18" id="Seg_940" s="T17">n</ta>
            <ta e="T19" id="Seg_941" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_942" s="T19">quant</ta>
            <ta e="T21" id="Seg_943" s="T20">v</ta>
            <ta e="T22" id="Seg_944" s="T21">n</ta>
            <ta e="T23" id="Seg_945" s="T22">n</ta>
            <ta e="T24" id="Seg_946" s="T23">adv</ta>
            <ta e="T25" id="Seg_947" s="T24">v</ta>
            <ta e="T26" id="Seg_948" s="T25">adv</ta>
            <ta e="T27" id="Seg_949" s="T26">n</ta>
            <ta e="T28" id="Seg_950" s="T27">adv</ta>
            <ta e="T29" id="Seg_951" s="T28">pers</ta>
            <ta e="T30" id="Seg_952" s="T29">v</ta>
            <ta e="T31" id="Seg_953" s="T30">adv</ta>
            <ta e="T32" id="Seg_954" s="T31">adv</ta>
            <ta e="T33" id="Seg_955" s="T32">adv</ta>
            <ta e="T34" id="Seg_956" s="T33">preverb</ta>
            <ta e="T35" id="Seg_957" s="T34">v</ta>
            <ta e="T36" id="Seg_958" s="T35">dem</ta>
            <ta e="T38" id="Seg_959" s="T37">n</ta>
            <ta e="T39" id="Seg_960" s="T38">n</ta>
            <ta e="T40" id="Seg_961" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_962" s="T40">adv</ta>
            <ta e="T42" id="Seg_963" s="T41">adv</ta>
            <ta e="T43" id="Seg_964" s="T42">adv</ta>
            <ta e="T44" id="Seg_965" s="T43">v</ta>
            <ta e="T45" id="Seg_966" s="T44">n</ta>
            <ta e="T46" id="Seg_967" s="T45">adv</ta>
            <ta e="T47" id="Seg_968" s="T46">adv</ta>
            <ta e="T48" id="Seg_969" s="T47">adv</ta>
            <ta e="T49" id="Seg_970" s="T48">v</ta>
            <ta e="T50" id="Seg_971" s="T49">interrog</ta>
            <ta e="T51" id="Seg_972" s="T50">adj</ta>
            <ta e="T52" id="Seg_973" s="T51">quant</ta>
            <ta e="T53" id="Seg_974" s="T52">v</ta>
            <ta e="T54" id="Seg_975" s="T53">adv</ta>
            <ta e="T55" id="Seg_976" s="T54">n</ta>
            <ta e="T56" id="Seg_977" s="T55">n</ta>
            <ta e="T57" id="Seg_978" s="T56">v</ta>
            <ta e="T58" id="Seg_979" s="T57">dem</ta>
            <ta e="T59" id="Seg_980" s="T58">n</ta>
            <ta e="T60" id="Seg_981" s="T59">adv</ta>
            <ta e="T61" id="Seg_982" s="T60">adv</ta>
            <ta e="T62" id="Seg_983" s="T61">v</ta>
            <ta e="T64" id="Seg_984" s="T63">n</ta>
            <ta e="T65" id="Seg_985" s="T64">adv</ta>
            <ta e="T66" id="Seg_986" s="T65">adv</ta>
            <ta e="T67" id="Seg_987" s="T66">dem</ta>
            <ta e="T68" id="Seg_988" s="T67">v</ta>
            <ta e="T69" id="Seg_989" s="T68">adv</ta>
            <ta e="T70" id="Seg_990" s="T69">n</ta>
            <ta e="T71" id="Seg_991" s="T70">quant</ta>
            <ta e="T72" id="Seg_992" s="T71">adv</ta>
            <ta e="T73" id="Seg_993" s="T72">dem</ta>
            <ta e="T74" id="Seg_994" s="T73">n</ta>
            <ta e="T75" id="Seg_995" s="T74">n</ta>
            <ta e="T76" id="Seg_996" s="T75">v</ta>
            <ta e="T77" id="Seg_997" s="T76">conj</ta>
            <ta e="T78" id="Seg_998" s="T77">v</ta>
            <ta e="T79" id="Seg_999" s="T78">adv</ta>
            <ta e="T80" id="Seg_1000" s="T79">num</ta>
            <ta e="T81" id="Seg_1001" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_1002" s="T81">n</ta>
            <ta e="T83" id="Seg_1003" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_1004" s="T83">quant</ta>
            <ta e="T85" id="Seg_1005" s="T92">n</ta>
            <ta e="T86" id="Seg_1006" s="T85">n</ta>
            <ta e="T87" id="Seg_1007" s="T86">conj</ta>
            <ta e="T89" id="Seg_1008" s="T87">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T7" id="Seg_1009" s="T0">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_1010" s="T0">[KuAI:] Мария Ивановна Кунина расскажет, как делает кирпичи.</ta>
            <ta e="T15" id="Seg_1011" s="T7">Здесь утром, это что утром (рано?) встала.</ta>
            <ta e="T17" id="Seg_1012" s="T15">Сильно дождь идёт.</ta>
            <ta e="T21" id="Seg_1013" s="T17">Комаров вроде много.</ta>
            <ta e="T25" id="Seg_1014" s="T21">Летом я (всегда?) пешком ходила.</ta>
            <ta e="T35" id="Seg_1015" s="T25">Теперь комаров столько меня покусало, [что] скоро обратно придя я спать легла.</ta>
            <ta e="T44" id="Seg_1016" s="T35">(кирпич?) После того как ветер подул, вот потом я туда пошла.</ta>
            <ta e="T57" id="Seg_1017" s="T44">Когда (на землю/на кирпичи?) (столько?) накапало, так сделал [дождь], что некоторые все (обломились?), только половина кусочками лежит.</ta>
            <ta e="T64" id="Seg_1018" s="T57">Эти кусочки (еле-еле вместе положила (она?), (?) кусочки ?).</ta>
            <ta e="T71" id="Seg_1019" s="T64">Домой придя, вот сидит [она/я], столько комаров много.</ta>
            <ta e="T76" id="Seg_1020" s="T71">Вот-вот собираюсь за дымокурным деревом идти.</ta>
            <ta e="T78" id="Seg_1021" s="T76">Поэтому оделась [она/я].</ta>
            <ta e="T84" id="Seg_1022" s="T78">Опять (?) этих комаров, вроде, много.</ta>
            <ta e="T89" id="Seg_1023" s="T84">(…) За дымокурным деревом как пойду.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_1024" s="T0">[KuAI:] Maria Ivanovna Kunina will tell how to make bricks.</ta>
            <ta e="T15" id="Seg_1025" s="T7">Here in the morning, I got up (early?) in the morning.</ta>
            <ta e="T17" id="Seg_1026" s="T15">It is raining heavily. </ta>
            <ta e="T21" id="Seg_1027" s="T17">There are many mosquitos. </ta>
            <ta e="T25" id="Seg_1028" s="T21">In the summer, I (always?) traveled on foot. </ta>
            <ta e="T35" id="Seg_1029" s="T25">Now the mosquiots bite me so much, [that] I soon came back home and went to sleep. </ta>
            <ta e="T44" id="Seg_1030" s="T35">(Brick?)? After the wind has blown, I went there. </ta>
            <ta e="T57" id="Seg_1031" s="T44">When (so much?) rain drops (on the ground/on the bricks?), that some are broken, only half of them are lying here in pieces. </ta>
            <ta e="T64" id="Seg_1032" s="T57">These pieces (barely put together, (?) the pieces ?). </ta>
            <ta e="T71" id="Seg_1033" s="T64">[I/She] come(s) home, [I/she] sit(s), so many mosquitos. </ta>
            <ta e="T76" id="Seg_1034" s="T71">I will leave soon to get fire wood. </ta>
            <ta e="T78" id="Seg_1035" s="T76">Because of that [I/she] got dressed. </ta>
            <ta e="T84" id="Seg_1036" s="T78">(?) These mosquitos are many again.</ta>
            <ta e="T89" id="Seg_1037" s="T84">(…) I will leave to get fire wood.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_1038" s="T0">[KuAI:] Maria Ivanovna Kunina wird erzählen, wie man Ziegelsteine herstellt.</ta>
            <ta e="T15" id="Seg_1039" s="T7">Hier am morgen, ich bin sehr (früh?) am morgen aufgestanden. </ta>
            <ta e="T17" id="Seg_1040" s="T15">Es regnet stark. </ta>
            <ta e="T21" id="Seg_1041" s="T17">Es gibt sehr viele Mücken.</ta>
            <ta e="T25" id="Seg_1042" s="T21">Im Sommer bin ich (immer?) zu Fuß gegangen. </ta>
            <ta e="T35" id="Seg_1043" s="T25">Jetzt habe mich die Mücken so sehr gebissen, [dass] ich wieder zurückgekommen und schlafengegangen bin.</ta>
            <ta e="T44" id="Seg_1044" s="T35">(Ziegelstein?) Nachdem der Wind wehte, bin ich dorthin gegangen.</ta>
            <ta e="T57" id="Seg_1045" s="T44">Wenn auf (den Boden/den Ziegelstein?) (so viel?) Regen tropft, dass einige zerbrochen sind, nur die Hälfte liegt hier in Stücken.</ta>
            <ta e="T64" id="Seg_1046" s="T57">Diese Stücke (kaum zusammengefügt, (?) die Stücke ?). </ta>
            <ta e="T71" id="Seg_1047" s="T64">[Ich/Sie] komme/kommt nach Hause, [ich/sie] sitze/sitzt, so viele Mücken. </ta>
            <ta e="T76" id="Seg_1048" s="T71">Ich werde bald gehen, um Feuerholz zu holen.</ta>
            <ta e="T78" id="Seg_1049" s="T76">Deswegen zog [sie sich/ich mich] an. </ta>
            <ta e="T84" id="Seg_1050" s="T78">Wieder sind (?) diese Mücken sehr viele. </ta>
            <ta e="T89" id="Seg_1051" s="T84">(…) Ich werde gehen um Feuerholz zu holen.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_1052" s="T0">Кунина Мария Ивановна расскажет, как делает кирпичи.</ta>
            <ta e="T15" id="Seg_1053" s="T7">в это утро что-то так рано встала</ta>
            <ta e="T17" id="Seg_1054" s="T15">сильно дождь идёт</ta>
            <ta e="T21" id="Seg_1055" s="T17">комаров ой как много </ta>
            <ta e="T25" id="Seg_1056" s="T21">летом всегда пешком ходила</ta>
            <ta e="T35" id="Seg_1057" s="T25">комаров так много меня покусали пришлось придя обратно и спать легла</ta>
            <ta e="T44" id="Seg_1058" s="T35">тут ветер подул после и вот туда пошла </ta>
            <ta e="T57" id="Seg_1059" s="T44">на кирпичи столько накапало так сделал (дождь) половина большинство обломились только кусочками лежат</ta>
            <ta e="T64" id="Seg_1060" s="T57">вот кусочки вместе собрала (бедные)</ta>
            <ta e="T71" id="Seg_1061" s="T64">домой пришла вот сижу комаров так много</ta>
            <ta e="T76" id="Seg_1062" s="T71">сейчас собираюсь за дровами идти</ta>
            <ta e="T78" id="Seg_1063" s="T76">поэтому оделась</ta>
            <ta e="T84" id="Seg_1064" s="T78">ой опять комаров наверное много</ta>
            <ta e="T89" id="Seg_1065" s="T84">за дымокурным деревом как пойду</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T7" id="Seg_1066" s="T0">[BrM]: Said by Kuzmina A.I.</ta>
            <ta e="T15" id="Seg_1067" s="T7">[BrM]: Glosses of nat uncertain.</ta>
            <ta e="T17" id="Seg_1068" s="T15">[BrM]: orsä 'force-INSTR' would be expected instead of or 'force-NOM'.</ta>
            <ta e="T25" id="Seg_1069" s="T21">[BrM]: čʼaɣ could be a variant of čʼəŋ 'soon'.</ta>
            <ta e="T44" id="Seg_1070" s="T35">[BrM]: 1. One more syllable after tɨ? 2. šol could be possibly interpreted as sö-l 'ground-ADJZ'.</ta>
            <ta e="T57" id="Seg_1071" s="T44">[BrM]: 1. (pelʼaktɨ lʼipije)?? 2. čʼaɣ could be a variant of čʼəŋ 'soon' 3. the original phrase munt tɨnɨttɔːtɨn has been reinterpreted as muntɨ nɨttɔːtɨn, this is a tentative analysis.</ta>
            <ta e="T64" id="Seg_1072" s="T57">[BrM]: Unexpected 3SG.S instead of 1SG.S.</ta>
            <ta e="T71" id="Seg_1073" s="T64">[BrM]: Unexpected 3SG.S instead of 1SG.S.</ta>
            <ta e="T78" id="Seg_1074" s="T76">[BrM]: Unexpected 3SG.S instead of 1SG.S. [AAV:] relisten the ending</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T92" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
