<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID7C1D9BEF-A4BD-507A-9937-E901C3F5914B">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\TVP_1965_IWasBornInChaselka_nar\TVP_1965_IWasBornInChaselka_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">82</ud-information>
            <ud-information attribute-name="# HIAT:w">66</ud-information>
            <ud-information attribute-name="# e">68</ud-information>
            <ud-information attribute-name="# HIAT:u">14</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="TVP">
            <abbreviation>TVP</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="TVP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T251" id="Seg_0" n="sc" s="T185">
               <ts e="T190" id="Seg_2" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_4" n="HIAT:w" s="T185">Mat</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_7" n="HIAT:w" s="T186">čʼeːlɨŋsak</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_10" n="HIAT:w" s="T187">Čʼosalʼkɨqɨt</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_13" n="HIAT:w" s="T188">1945</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_16" n="HIAT:w" s="T189">godu</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_20" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_22" n="HIAT:w" s="T190">1954</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_25" n="HIAT:w" s="T191">godu</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_28" n="HIAT:w" s="T192">qessak</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_31" n="HIAT:w" s="T193">Nʼaralʼ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_34" n="HIAT:w" s="T194">Mačʼontɨ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_37" n="HIAT:w" s="T195">učʼitɨqa</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_41" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_43" n="HIAT:w" s="T196">Nʼarɨlʼ</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_46" n="HIAT:w" s="T197">Mačʼoːqɨt</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_49" n="HIAT:w" s="T198">učʼitɨsak</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_52" n="HIAT:w" s="T199">muktɨt</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_55" n="HIAT:w" s="T200">poːntɨ</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_58" n="HIAT:w" s="T201">kuntɨ</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_62" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_64" n="HIAT:w" s="T202">1960</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_67" n="HIAT:w" s="T203">godu</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_70" n="HIAT:w" s="T204">qessak</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_73" n="HIAT:w" s="T205">robɨtɨnoːqa</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_76" n="HIAT:w" s="T206">Lelʼqeːltoːtɨ</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_79" n="HIAT:w" s="T207">qeːlɨšqunoːqa</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_83" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_85" n="HIAT:w" s="T208">I</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_88" n="HIAT:w" s="T209">totɨ</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_91" n="HIAT:w" s="T210">nɨmtɨ</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_94" n="HIAT:w" s="T211">qeːlɨnʼnʼak</ts>
                  <nts id="Seg_95" n="HIAT:ip">.</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_98" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_100" n="HIAT:w" s="T212">Äsʼamɨ</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_103" n="HIAT:w" s="T213">ɛj</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_106" n="HIAT:w" s="T214">qeːlɨnʼnʼa</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_110" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_112" n="HIAT:w" s="T215">Amʼamɨ</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_115" n="HIAT:w" s="T216">aša</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_118" n="HIAT:w" s="T217">qeːlɨnʼnʼa</ts>
                  <nts id="Seg_119" n="HIAT:ip">,</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_122" n="HIAT:w" s="T218">moqɨnʼat</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_125" n="HIAT:w" s="T219">mɔːtqɨt</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_128" n="HIAT:w" s="T220">uːčʼa</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_132" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_134" n="HIAT:w" s="T221">Timnʼamɨ</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_137" n="HIAT:w" s="T222">werqɨ</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_140" n="HIAT:w" s="T223">qerɔːtɨt</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_143" n="HIAT:w" s="T224">Volodʼatqa</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_147" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_149" n="HIAT:w" s="T225">Tɛm</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_152" n="HIAT:w" s="T226">ɛj</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_155" n="HIAT:w" s="T227">ropɨjta</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_158" n="HIAT:w" s="T228">sovhozqɨt</ts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_162" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_164" n="HIAT:w" s="T229">Kɨpa</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_167" n="HIAT:w" s="T230">timnʼamɨ</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_170" n="HIAT:w" s="T231">qerɔːtɨt</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_173" n="HIAT:w" s="T232">Juritqa</ts>
                  <nts id="Seg_174" n="HIAT:ip">.</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_177" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_179" n="HIAT:w" s="T233">Tɛp</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_182" n="HIAT:w" s="T234">školaqɨt</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_185" n="HIAT:w" s="T235">učʼita</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_189" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_191" n="HIAT:w" s="T236">Nenʼamɨ</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_194" n="HIAT:w" s="T237">ɛj</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_197" n="HIAT:w" s="T238">školaqɨt</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_200" n="HIAT:w" s="T239">učʼita</ts>
                  <nts id="Seg_201" n="HIAT:ip">.</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_204" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_206" n="HIAT:w" s="T240">Mat</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_209" n="HIAT:w" s="T241">nɔːtɨ</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_212" n="HIAT:w" s="T242">muntɨk</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_215" n="HIAT:w" s="T243">na</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_218" n="HIAT:w" s="T244">nekɨrap</ts>
                  <nts id="Seg_219" n="HIAT:ip">,</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_222" n="HIAT:w" s="T245">qaip</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_225" n="HIAT:w" s="T246">kɨkɨsak</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_228" n="HIAT:w" s="T247">nekɨrqa</ts>
                  <nts id="Seg_229" n="HIAT:ip">.</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_232" n="HIAT:u" s="T248">
                  <ts e="T249" id="Seg_234" n="HIAT:w" s="T248">Mašıp</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_237" n="HIAT:w" s="T249">qerɔːtɨt</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_240" n="HIAT:w" s="T250">Vitʼatqa</ts>
                  <nts id="Seg_241" n="HIAT:ip">.</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T251" id="Seg_243" n="sc" s="T185">
               <ts e="T186" id="Seg_245" n="e" s="T185">Mat </ts>
               <ts e="T187" id="Seg_247" n="e" s="T186">čʼeːlɨŋsak </ts>
               <ts e="T188" id="Seg_249" n="e" s="T187">Čʼosalʼkɨqɨt </ts>
               <ts e="T189" id="Seg_251" n="e" s="T188">1945 </ts>
               <ts e="T190" id="Seg_253" n="e" s="T189">godu. </ts>
               <ts e="T191" id="Seg_255" n="e" s="T190">1954 </ts>
               <ts e="T192" id="Seg_257" n="e" s="T191">godu </ts>
               <ts e="T193" id="Seg_259" n="e" s="T192">qessak </ts>
               <ts e="T194" id="Seg_261" n="e" s="T193">Nʼaralʼ </ts>
               <ts e="T195" id="Seg_263" n="e" s="T194">Mačʼontɨ </ts>
               <ts e="T196" id="Seg_265" n="e" s="T195">učʼitɨqa. </ts>
               <ts e="T197" id="Seg_267" n="e" s="T196">Nʼarɨlʼ </ts>
               <ts e="T198" id="Seg_269" n="e" s="T197">Mačʼoːqɨt </ts>
               <ts e="T199" id="Seg_271" n="e" s="T198">učʼitɨsak </ts>
               <ts e="T200" id="Seg_273" n="e" s="T199">muktɨt </ts>
               <ts e="T201" id="Seg_275" n="e" s="T200">poːntɨ </ts>
               <ts e="T202" id="Seg_277" n="e" s="T201">kuntɨ. </ts>
               <ts e="T203" id="Seg_279" n="e" s="T202">1960 </ts>
               <ts e="T204" id="Seg_281" n="e" s="T203">godu </ts>
               <ts e="T205" id="Seg_283" n="e" s="T204">qessak </ts>
               <ts e="T206" id="Seg_285" n="e" s="T205">robɨtɨnoːqa </ts>
               <ts e="T0" id="Seg_287" n="e" s="T206">Lelʼ</ts>
               <ts e="T1" id="Seg_289" n="e" s="T0">qeːl</ts>
               <ts e="T207" id="Seg_291" n="e" s="T1">toːtɨ </ts>
               <ts e="T208" id="Seg_293" n="e" s="T207">qeːlɨšqunoːqa. </ts>
               <ts e="T209" id="Seg_295" n="e" s="T208">I </ts>
               <ts e="T210" id="Seg_297" n="e" s="T209">totɨ </ts>
               <ts e="T211" id="Seg_299" n="e" s="T210">nɨmtɨ </ts>
               <ts e="T212" id="Seg_301" n="e" s="T211">qeːlɨnʼnʼak. </ts>
               <ts e="T213" id="Seg_303" n="e" s="T212">Äsʼamɨ </ts>
               <ts e="T214" id="Seg_305" n="e" s="T213">ɛj </ts>
               <ts e="T215" id="Seg_307" n="e" s="T214">qeːlɨnʼnʼa. </ts>
               <ts e="T216" id="Seg_309" n="e" s="T215">Amʼamɨ </ts>
               <ts e="T217" id="Seg_311" n="e" s="T216">aša </ts>
               <ts e="T218" id="Seg_313" n="e" s="T217">qeːlɨnʼnʼa, </ts>
               <ts e="T219" id="Seg_315" n="e" s="T218">moqɨnʼat </ts>
               <ts e="T220" id="Seg_317" n="e" s="T219">mɔːtqɨt </ts>
               <ts e="T221" id="Seg_319" n="e" s="T220">uːčʼa. </ts>
               <ts e="T222" id="Seg_321" n="e" s="T221">Timnʼamɨ </ts>
               <ts e="T223" id="Seg_323" n="e" s="T222">werqɨ </ts>
               <ts e="T224" id="Seg_325" n="e" s="T223">qerɔːtɨt </ts>
               <ts e="T225" id="Seg_327" n="e" s="T224">Volodʼatqa. </ts>
               <ts e="T226" id="Seg_329" n="e" s="T225">Tɛm </ts>
               <ts e="T227" id="Seg_331" n="e" s="T226">ɛj </ts>
               <ts e="T228" id="Seg_333" n="e" s="T227">ropɨjta </ts>
               <ts e="T229" id="Seg_335" n="e" s="T228">sovhozqɨt. </ts>
               <ts e="T230" id="Seg_337" n="e" s="T229">Kɨpa </ts>
               <ts e="T231" id="Seg_339" n="e" s="T230">timnʼamɨ </ts>
               <ts e="T232" id="Seg_341" n="e" s="T231">qerɔːtɨt </ts>
               <ts e="T233" id="Seg_343" n="e" s="T232">Juritqa. </ts>
               <ts e="T234" id="Seg_345" n="e" s="T233">Tɛp </ts>
               <ts e="T235" id="Seg_347" n="e" s="T234">školaqɨt </ts>
               <ts e="T236" id="Seg_349" n="e" s="T235">učʼita. </ts>
               <ts e="T237" id="Seg_351" n="e" s="T236">Nenʼamɨ </ts>
               <ts e="T238" id="Seg_353" n="e" s="T237">ɛj </ts>
               <ts e="T239" id="Seg_355" n="e" s="T238">školaqɨt </ts>
               <ts e="T240" id="Seg_357" n="e" s="T239">učʼita. </ts>
               <ts e="T241" id="Seg_359" n="e" s="T240">Mat </ts>
               <ts e="T242" id="Seg_361" n="e" s="T241">nɔːtɨ </ts>
               <ts e="T243" id="Seg_363" n="e" s="T242">muntɨk </ts>
               <ts e="T244" id="Seg_365" n="e" s="T243">na </ts>
               <ts e="T245" id="Seg_367" n="e" s="T244">nekɨrap, </ts>
               <ts e="T246" id="Seg_369" n="e" s="T245">qaip </ts>
               <ts e="T247" id="Seg_371" n="e" s="T246">kɨkɨsak </ts>
               <ts e="T248" id="Seg_373" n="e" s="T247">nekɨrqa. </ts>
               <ts e="T249" id="Seg_375" n="e" s="T248">Mašıp </ts>
               <ts e="T250" id="Seg_377" n="e" s="T249">qerɔːtɨt </ts>
               <ts e="T251" id="Seg_379" n="e" s="T250">Vitʼatqa. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T190" id="Seg_380" s="T185">TVP_1965_IWasBornInChaselka_nar.001 (001.001)</ta>
            <ta e="T196" id="Seg_381" s="T190">TVP_1965_IWasBornInChaselka_nar.002 (001.002)</ta>
            <ta e="T202" id="Seg_382" s="T196">TVP_1965_IWasBornInChaselka_nar.003 (001.003)</ta>
            <ta e="T208" id="Seg_383" s="T202">TVP_1965_IWasBornInChaselka_nar.004 (001.004)</ta>
            <ta e="T212" id="Seg_384" s="T208">TVP_1965_IWasBornInChaselka_nar.005 (001.005)</ta>
            <ta e="T215" id="Seg_385" s="T212">TVP_1965_IWasBornInChaselka_nar.006 (001.006)</ta>
            <ta e="T221" id="Seg_386" s="T215">TVP_1965_IWasBornInChaselka_nar.007 (001.007)</ta>
            <ta e="T225" id="Seg_387" s="T221">TVP_1965_IWasBornInChaselka_nar.008 (001.008)</ta>
            <ta e="T229" id="Seg_388" s="T225">TVP_1965_IWasBornInChaselka_nar.009 (001.009)</ta>
            <ta e="T233" id="Seg_389" s="T229">TVP_1965_IWasBornInChaselka_nar.010 (001.010)</ta>
            <ta e="T236" id="Seg_390" s="T233">TVP_1965_IWasBornInChaselka_nar.011 (001.011)</ta>
            <ta e="T240" id="Seg_391" s="T236">TVP_1965_IWasBornInChaselka_nar.012 (001.012)</ta>
            <ta e="T248" id="Seg_392" s="T240">TVP_1965_IWasBornInChaselka_nar.013 (001.013)</ta>
            <ta e="T251" id="Seg_393" s="T248">TVP_1965_IWasBornInChaselka_nar.014 (001.014)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T190" id="Seg_394" s="T185">мат делынсак чосалькыкыт 1945 году.</ta>
            <ta e="T196" id="Seg_395" s="T190">1954 году кесак няряльмачонты учитыка.</ta>
            <ta e="T202" id="Seg_396" s="T196">нярыльмачокыт учитысак муктыт понтыкунты.</ta>
            <ta e="T208" id="Seg_397" s="T202">1960 году кесак робытынока лелькелтоты келышкунока. </ta>
            <ta e="T212" id="Seg_398" s="T208">и тоты нымты келыньняк.</ta>
            <ta e="T215" id="Seg_399" s="T212">эсямы эй келыня.</ta>
            <ta e="T221" id="Seg_400" s="T215">амямы аша келыня мокынят моткыт уча.</ta>
            <ta e="T225" id="Seg_401" s="T221">тимнямы веркы керотыт Володятка.</ta>
            <ta e="T229" id="Seg_402" s="T225">тэмей робыйта совхозкыт.</ta>
            <ta e="T233" id="Seg_403" s="T229">кыпа тимнямы керотыт Юритка.</ta>
            <ta e="T236" id="Seg_404" s="T233">тэп школакыт учита.</ta>
            <ta e="T240" id="Seg_405" s="T236">ненямы эй школакыт учита.</ta>
            <ta e="T248" id="Seg_406" s="T240">мат ноты мунтык на некырап каип кыкысак некырка.</ta>
            <ta e="T251" id="Seg_407" s="T248">машип керотыт Витятка.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T190" id="Seg_408" s="T185">mat delɨnsak čosalʼkɨkɨt 1945 godu.</ta>
            <ta e="T196" id="Seg_409" s="T190">1954 godu kesak nяrяlʼmačontɨ učitɨka.</ta>
            <ta e="T202" id="Seg_410" s="T196">nяrɨlʼmačokɨt učitɨsak muktɨt pontɨkuntɨ.</ta>
            <ta e="T208" id="Seg_411" s="T202">1960 godu kesak ropɨtɨnoka lelʼkeltotɨ kelɨškunoka. </ta>
            <ta e="T212" id="Seg_412" s="T208">i totɨ nɨmtɨ kelɨnʼnяk.</ta>
            <ta e="T215" id="Seg_413" s="T212">ɛsяmɨ ɛj kelɨnя.</ta>
            <ta e="T221" id="Seg_414" s="T215">amяmɨ aša kelɨnя mokɨnяt motkɨt uča.</ta>
            <ta e="T225" id="Seg_415" s="T221">timnяmɨ verkɨ kerotɨt Volodяtka.</ta>
            <ta e="T229" id="Seg_416" s="T225">tɛmej ropɨjta sovhozkɨt.</ta>
            <ta e="T233" id="Seg_417" s="T229">kɨpa timnяmɨ kerotɨt Юritka.</ta>
            <ta e="T236" id="Seg_418" s="T233">tɛp školakɨt učita.</ta>
            <ta e="T240" id="Seg_419" s="T236">nenяmɨ ɛj školakɨt učita.</ta>
            <ta e="T248" id="Seg_420" s="T240">mat notɨ muntɨk na nekɨrap kaip kɨkɨsak nekɨrka.</ta>
            <ta e="T251" id="Seg_421" s="T248">mašip kerotɨt Vitяtka.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T190" id="Seg_422" s="T185">Mat čʼeːlɨŋsak Čʼosalʼkɨqɨt 1945 godu. </ta>
            <ta e="T196" id="Seg_423" s="T190">1954 godu qessak Nʼaralʼ Mačʼontɨ učʼitɨqa. </ta>
            <ta e="T202" id="Seg_424" s="T196">Nʼarɨlʼ Mačʼoːqɨt učʼitɨsak muktɨt poːntɨ kuntɨ. </ta>
            <ta e="T208" id="Seg_425" s="T202">1960 godu qessak robɨtɨnoːqa Lelʼ qeːl toːtɨ qeːlɨšqunoːqa. </ta>
            <ta e="T212" id="Seg_426" s="T208">I totɨ nɨmtɨ qeːlɨnʼnʼak. </ta>
            <ta e="T215" id="Seg_427" s="T212">Äsʼamɨ ɛj qeːlɨnʼnʼa. </ta>
            <ta e="T221" id="Seg_428" s="T215">Amʼamɨ aša qeːlɨnʼnʼa, moqɨnʼat mɔːtqɨt uːčʼa. </ta>
            <ta e="T225" id="Seg_429" s="T221">Timnʼamɨ werqɨ qerɔːtɨt Volodʼatqa. </ta>
            <ta e="T229" id="Seg_430" s="T225">Tɛm ɛj ropɨjta sovhozqɨt. </ta>
            <ta e="T233" id="Seg_431" s="T229">Kɨpa timnʼamɨ qerɔːtɨt Juritqa. </ta>
            <ta e="T236" id="Seg_432" s="T233">Tɛp školaqɨt učʼita. </ta>
            <ta e="T240" id="Seg_433" s="T236">Nenʼamɨ ɛj školaqɨt učʼita. </ta>
            <ta e="T248" id="Seg_434" s="T240">Mat nɔːtɨ muntɨk na nekɨrap, qaip kɨkɨsak nekɨrqa. </ta>
            <ta e="T251" id="Seg_435" s="T248">Mašıp qerɔːtɨt Vitʼatqa. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T186" id="Seg_436" s="T185">mat</ta>
            <ta e="T187" id="Seg_437" s="T186">čʼeːlɨŋ-sa-k</ta>
            <ta e="T188" id="Seg_438" s="T187">Čʼosalʼkɨ-qɨt</ta>
            <ta e="T189" id="Seg_439" s="T188">1945</ta>
            <ta e="T190" id="Seg_440" s="T189">godu</ta>
            <ta e="T191" id="Seg_441" s="T190">1954</ta>
            <ta e="T192" id="Seg_442" s="T191">godu</ta>
            <ta e="T193" id="Seg_443" s="T192">qes-sa-k</ta>
            <ta e="T194" id="Seg_444" s="T193">nʼara-lʼ</ta>
            <ta e="T195" id="Seg_445" s="T194">mačʼo-ntɨ</ta>
            <ta e="T196" id="Seg_446" s="T195">učʼitɨ-qa</ta>
            <ta e="T197" id="Seg_447" s="T196">nʼarɨ-lʼ</ta>
            <ta e="T198" id="Seg_448" s="T197">mačʼoː-qɨt</ta>
            <ta e="T199" id="Seg_449" s="T198">učʼitɨ-sa-k</ta>
            <ta e="T200" id="Seg_450" s="T199">muktɨt</ta>
            <ta e="T201" id="Seg_451" s="T200">poː-n-tɨ</ta>
            <ta e="T202" id="Seg_452" s="T201">kuntɨ</ta>
            <ta e="T203" id="Seg_453" s="T202">1960</ta>
            <ta e="T204" id="Seg_454" s="T203">godu</ta>
            <ta e="T205" id="Seg_455" s="T204">qes-sa-k</ta>
            <ta e="T206" id="Seg_456" s="T205">robɨtɨ-noːqa</ta>
            <ta e="T0" id="Seg_457" s="T206">Le-lʼ</ta>
            <ta e="T1" id="Seg_458" s="T0">qeːl</ta>
            <ta e="T207" id="Seg_459" s="T1">toː-tɨ</ta>
            <ta e="T208" id="Seg_460" s="T207">qeːlɨ-š-qunoːqa</ta>
            <ta e="T209" id="Seg_461" s="T208">i</ta>
            <ta e="T210" id="Seg_462" s="T209">totɨ</ta>
            <ta e="T211" id="Seg_463" s="T210">nɨmtɨ</ta>
            <ta e="T212" id="Seg_464" s="T211">qeːlɨ-nʼ-nʼa-k</ta>
            <ta e="T213" id="Seg_465" s="T212">äsʼa-mɨ</ta>
            <ta e="T214" id="Seg_466" s="T213">ɛj</ta>
            <ta e="T215" id="Seg_467" s="T214">qeːlɨ-nʼ-nʼa</ta>
            <ta e="T216" id="Seg_468" s="T215">amʼa-mɨ</ta>
            <ta e="T217" id="Seg_469" s="T216">aša</ta>
            <ta e="T218" id="Seg_470" s="T217">qeːlɨ-nʼ-nʼa</ta>
            <ta e="T219" id="Seg_471" s="T218">moqɨnʼa-t</ta>
            <ta e="T220" id="Seg_472" s="T219">mɔːt-qɨt</ta>
            <ta e="T221" id="Seg_473" s="T220">uːčʼa</ta>
            <ta e="T222" id="Seg_474" s="T221">timnʼa-mɨ</ta>
            <ta e="T223" id="Seg_475" s="T222">werqɨ</ta>
            <ta e="T224" id="Seg_476" s="T223">qerɔː-tɨt</ta>
            <ta e="T225" id="Seg_477" s="T224">Volodʼa-tqa</ta>
            <ta e="T226" id="Seg_478" s="T225">tɛm</ta>
            <ta e="T227" id="Seg_479" s="T226">ɛj</ta>
            <ta e="T228" id="Seg_480" s="T227">ropɨjta</ta>
            <ta e="T229" id="Seg_481" s="T228">sovhoz-qɨt</ta>
            <ta e="T230" id="Seg_482" s="T229">kɨpa</ta>
            <ta e="T231" id="Seg_483" s="T230">timnʼa-mɨ</ta>
            <ta e="T232" id="Seg_484" s="T231">qerɔː-tɨt</ta>
            <ta e="T233" id="Seg_485" s="T232">Juri-tqa</ta>
            <ta e="T234" id="Seg_486" s="T233">tɛp</ta>
            <ta e="T235" id="Seg_487" s="T234">škola-qɨt</ta>
            <ta e="T236" id="Seg_488" s="T235">učʼita</ta>
            <ta e="T237" id="Seg_489" s="T236">nenʼa-mɨ</ta>
            <ta e="T238" id="Seg_490" s="T237">ɛj</ta>
            <ta e="T239" id="Seg_491" s="T238">škola-qɨt</ta>
            <ta e="T240" id="Seg_492" s="T239">učʼita</ta>
            <ta e="T241" id="Seg_493" s="T240">mat</ta>
            <ta e="T242" id="Seg_494" s="T241">nɔːtɨ</ta>
            <ta e="T243" id="Seg_495" s="T242">muntɨk</ta>
            <ta e="T244" id="Seg_496" s="T243">na</ta>
            <ta e="T245" id="Seg_497" s="T244">nekɨ-r-a-p</ta>
            <ta e="T246" id="Seg_498" s="T245">qa-i-p</ta>
            <ta e="T247" id="Seg_499" s="T246">kɨkɨ-sa-k</ta>
            <ta e="T248" id="Seg_500" s="T247">nekɨ-r-qa</ta>
            <ta e="T249" id="Seg_501" s="T248">mašıp</ta>
            <ta e="T250" id="Seg_502" s="T249">qerɔː-tɨt</ta>
            <ta e="T251" id="Seg_503" s="T250">Vitʼa-tqa</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T186" id="Seg_504" s="T185">man</ta>
            <ta e="T187" id="Seg_505" s="T186">čʼeːlɨŋ-sɨ-k</ta>
            <ta e="T188" id="Seg_506" s="T187">Čʼosalkɨ-qɨn</ta>
            <ta e="T189" id="Seg_507" s="T188">1945</ta>
            <ta e="T190" id="Seg_508" s="T189">godu</ta>
            <ta e="T191" id="Seg_509" s="T190">1954</ta>
            <ta e="T192" id="Seg_510" s="T191">godu</ta>
            <ta e="T193" id="Seg_511" s="T192">qən-sɨ-k</ta>
            <ta e="T194" id="Seg_512" s="T193">nʼarɨ-lʼ</ta>
            <ta e="T195" id="Seg_513" s="T194">mačʼɨ-ntɨ</ta>
            <ta e="T196" id="Seg_514" s="T195">učʼitɨ-qo</ta>
            <ta e="T197" id="Seg_515" s="T196">nʼarɨ-lʼ</ta>
            <ta e="T198" id="Seg_516" s="T197">mačʼɨ-qɨn</ta>
            <ta e="T199" id="Seg_517" s="T198">učʼitɨ-sɨ-k</ta>
            <ta e="T200" id="Seg_518" s="T199">muktɨt</ta>
            <ta e="T201" id="Seg_519" s="T200">poː-n-tɨ</ta>
            <ta e="T202" id="Seg_520" s="T201">kuntɨ</ta>
            <ta e="T203" id="Seg_521" s="T202">1960</ta>
            <ta e="T204" id="Seg_522" s="T203">godu</ta>
            <ta e="T205" id="Seg_523" s="T204">qən-sɨ-k</ta>
            <ta e="T206" id="Seg_524" s="T205">robɨtɨ-noːqo</ta>
            <ta e="T0" id="Seg_525" s="T206">lə-lʼ</ta>
            <ta e="T1" id="Seg_526" s="T0">qəːlɨ</ta>
            <ta e="T207" id="Seg_527" s="T1">toː-ntɨ</ta>
            <ta e="T208" id="Seg_528" s="T207">qəːlɨ-š-qɨnoːqo</ta>
            <ta e="T209" id="Seg_529" s="T208">i</ta>
            <ta e="T210" id="Seg_530" s="T209">totɨ</ta>
            <ta e="T211" id="Seg_531" s="T210">nɨmtɨ</ta>
            <ta e="T212" id="Seg_532" s="T211">qəːlɨ-š-ŋɨ-k</ta>
            <ta e="T213" id="Seg_533" s="T212">əsɨ-mɨ</ta>
            <ta e="T214" id="Seg_534" s="T213">aj</ta>
            <ta e="T215" id="Seg_535" s="T214">qəːlɨ-š-ŋɨ</ta>
            <ta e="T216" id="Seg_536" s="T215">ama-mɨ</ta>
            <ta e="T217" id="Seg_537" s="T216">ašša</ta>
            <ta e="T218" id="Seg_538" s="T217">qəːlɨ-š-ŋɨ</ta>
            <ta e="T219" id="Seg_539" s="T218">moqɨnä-n</ta>
            <ta e="T220" id="Seg_540" s="T219">mɔːt-qɨn</ta>
            <ta e="T221" id="Seg_541" s="T220">uːčʼɨ</ta>
            <ta e="T222" id="Seg_542" s="T221">timnʼa-mɨ</ta>
            <ta e="T223" id="Seg_543" s="T222">wərqɨ</ta>
            <ta e="T224" id="Seg_544" s="T223">qərɨ-tɨt</ta>
            <ta e="T225" id="Seg_545" s="T224">Volodʼa-tqo</ta>
            <ta e="T226" id="Seg_546" s="T225">təp</ta>
            <ta e="T227" id="Seg_547" s="T226">aj</ta>
            <ta e="T228" id="Seg_548" s="T227">robɨtɨ</ta>
            <ta e="T229" id="Seg_549" s="T228">sovhoz-qɨn</ta>
            <ta e="T230" id="Seg_550" s="T229">kɨpa</ta>
            <ta e="T231" id="Seg_551" s="T230">timnʼa-mɨ</ta>
            <ta e="T232" id="Seg_552" s="T231">qərɨ-tɨt</ta>
            <ta e="T233" id="Seg_553" s="T232">Juri-tqo</ta>
            <ta e="T234" id="Seg_554" s="T233">təp</ta>
            <ta e="T235" id="Seg_555" s="T234">škola-qɨn</ta>
            <ta e="T236" id="Seg_556" s="T235">učʼitɨ</ta>
            <ta e="T237" id="Seg_557" s="T236">nʼenʼnʼa-mɨ</ta>
            <ta e="T238" id="Seg_558" s="T237">aj</ta>
            <ta e="T239" id="Seg_559" s="T238">škola-qɨn</ta>
            <ta e="T240" id="Seg_560" s="T239">učʼitɨ</ta>
            <ta e="T241" id="Seg_561" s="T240">man</ta>
            <ta e="T242" id="Seg_562" s="T241">nɔːtɨ</ta>
            <ta e="T243" id="Seg_563" s="T242">muntɨk</ta>
            <ta e="T244" id="Seg_564" s="T243">na</ta>
            <ta e="T245" id="Seg_565" s="T244">nəkɨ-r-ɨ-m</ta>
            <ta e="T246" id="Seg_566" s="T245">qaj-ɨ-m</ta>
            <ta e="T247" id="Seg_567" s="T246">kɨkɨ-sɨ-k</ta>
            <ta e="T248" id="Seg_568" s="T247">nəkɨ-r-qo</ta>
            <ta e="T249" id="Seg_569" s="T248">mašım</ta>
            <ta e="T250" id="Seg_570" s="T249">qərɨ-tɨt</ta>
            <ta e="T251" id="Seg_571" s="T250">Vičʼa-tqo</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T186" id="Seg_572" s="T185">I.NOM</ta>
            <ta e="T187" id="Seg_573" s="T186">be.born-PST-1SG.S</ta>
            <ta e="T188" id="Seg_574" s="T187">Chaselka-LOC</ta>
            <ta e="T189" id="Seg_575" s="T188">1945</ta>
            <ta e="T190" id="Seg_576" s="T189">year</ta>
            <ta e="T191" id="Seg_577" s="T190">1954</ta>
            <ta e="T192" id="Seg_578" s="T191">year</ta>
            <ta e="T193" id="Seg_579" s="T192">leave-PST-1SG.S</ta>
            <ta e="T194" id="Seg_580" s="T193">tundra-ADJZ</ta>
            <ta e="T195" id="Seg_581" s="T194">forest-ILL</ta>
            <ta e="T196" id="Seg_582" s="T195">study-INF</ta>
            <ta e="T197" id="Seg_583" s="T196">tundra-ADJZ</ta>
            <ta e="T198" id="Seg_584" s="T197">forest-LOC</ta>
            <ta e="T199" id="Seg_585" s="T198">study-PST-1SG.S</ta>
            <ta e="T200" id="Seg_586" s="T199">six</ta>
            <ta e="T201" id="Seg_587" s="T200">year-GEN-3SG</ta>
            <ta e="T202" id="Seg_588" s="T201">during</ta>
            <ta e="T203" id="Seg_589" s="T202">1960</ta>
            <ta e="T204" id="Seg_590" s="T203">year</ta>
            <ta e="T205" id="Seg_591" s="T204">leave-PST-1SG.S</ta>
            <ta e="T206" id="Seg_592" s="T205">work-1SG.TRL</ta>
            <ta e="T0" id="Seg_593" s="T206">bone-ADJZ</ta>
            <ta e="T1" id="Seg_594" s="T0">fish.[NOM]</ta>
            <ta e="T207" id="Seg_595" s="T1">lake-ILL</ta>
            <ta e="T208" id="Seg_596" s="T207">fish-VBLZ-SUP.1SG</ta>
            <ta e="T209" id="Seg_597" s="T208">and</ta>
            <ta e="T210" id="Seg_598" s="T209">%%</ta>
            <ta e="T211" id="Seg_599" s="T210">there</ta>
            <ta e="T212" id="Seg_600" s="T211">fish-VBLZ-CO-1SG.S</ta>
            <ta e="T213" id="Seg_601" s="T212">father.[NOM]-1SG</ta>
            <ta e="T214" id="Seg_602" s="T213">also</ta>
            <ta e="T215" id="Seg_603" s="T214">fish-VBLZ-CO.[3SG.S]</ta>
            <ta e="T216" id="Seg_604" s="T215">mother.[NOM]-1SG</ta>
            <ta e="T217" id="Seg_605" s="T216">NEG</ta>
            <ta e="T218" id="Seg_606" s="T217">fish-VBLZ-CO.[3SG.S]</ta>
            <ta e="T219" id="Seg_607" s="T218">home-ADV.LOC</ta>
            <ta e="T220" id="Seg_608" s="T219">house-LOC</ta>
            <ta e="T221" id="Seg_609" s="T220">work.[3SG.S]</ta>
            <ta e="T222" id="Seg_610" s="T221">brother.[NOM]-1SG</ta>
            <ta e="T223" id="Seg_611" s="T222">elder</ta>
            <ta e="T224" id="Seg_612" s="T223">call-3PL</ta>
            <ta e="T225" id="Seg_613" s="T224">Volodya-TRL</ta>
            <ta e="T226" id="Seg_614" s="T225">(s)he.[NOM]</ta>
            <ta e="T227" id="Seg_615" s="T226">also</ta>
            <ta e="T228" id="Seg_616" s="T227">work.[3SG.S]</ta>
            <ta e="T229" id="Seg_617" s="T228">sovkhoz-LOC</ta>
            <ta e="T230" id="Seg_618" s="T229">young</ta>
            <ta e="T231" id="Seg_619" s="T230">brother.[NOM]-1SG</ta>
            <ta e="T232" id="Seg_620" s="T231">call-3PL</ta>
            <ta e="T233" id="Seg_621" s="T232">Jurij-TRL</ta>
            <ta e="T234" id="Seg_622" s="T233">(s)he.[NOM]</ta>
            <ta e="T235" id="Seg_623" s="T234">school-LOC</ta>
            <ta e="T236" id="Seg_624" s="T235">study.[3SG.S]</ta>
            <ta e="T237" id="Seg_625" s="T236">sister.[NOM]-1SG</ta>
            <ta e="T238" id="Seg_626" s="T237">also</ta>
            <ta e="T239" id="Seg_627" s="T238">school-LOC</ta>
            <ta e="T240" id="Seg_628" s="T239">study.[3SG.S]</ta>
            <ta e="T241" id="Seg_629" s="T240">I.NOM</ta>
            <ta e="T242" id="Seg_630" s="T241">then</ta>
            <ta e="T243" id="Seg_631" s="T242">all</ta>
            <ta e="T244" id="Seg_632" s="T243">this</ta>
            <ta e="T245" id="Seg_633" s="T244">write-FRQ-EP-1SG.O</ta>
            <ta e="T246" id="Seg_634" s="T245">what-EP-ACC</ta>
            <ta e="T247" id="Seg_635" s="T246">want-PST-1SG.S</ta>
            <ta e="T248" id="Seg_636" s="T247">write-FRQ-INF</ta>
            <ta e="T249" id="Seg_637" s="T248">I.ACC</ta>
            <ta e="T250" id="Seg_638" s="T249">call-3PL</ta>
            <ta e="T251" id="Seg_639" s="T250">Vitya-TRL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T186" id="Seg_640" s="T185">я.NOM</ta>
            <ta e="T187" id="Seg_641" s="T186">родиться-PST-1SG.S</ta>
            <ta e="T188" id="Seg_642" s="T187">Часелька-LOC</ta>
            <ta e="T189" id="Seg_643" s="T188">1945</ta>
            <ta e="T190" id="Seg_644" s="T189">в.году</ta>
            <ta e="T191" id="Seg_645" s="T190">1954</ta>
            <ta e="T192" id="Seg_646" s="T191">в.году</ta>
            <ta e="T193" id="Seg_647" s="T192">отправиться-PST-1SG.S</ta>
            <ta e="T194" id="Seg_648" s="T193">тундра-ADJZ</ta>
            <ta e="T195" id="Seg_649" s="T194">лес-ILL</ta>
            <ta e="T196" id="Seg_650" s="T195">учиться-INF</ta>
            <ta e="T197" id="Seg_651" s="T196">тундра-ADJZ</ta>
            <ta e="T198" id="Seg_652" s="T197">лес-LOC</ta>
            <ta e="T199" id="Seg_653" s="T198">учиться-PST-1SG.S</ta>
            <ta e="T200" id="Seg_654" s="T199">шесть</ta>
            <ta e="T201" id="Seg_655" s="T200">год-GEN-3SG</ta>
            <ta e="T202" id="Seg_656" s="T201">в.течение</ta>
            <ta e="T203" id="Seg_657" s="T202">1960</ta>
            <ta e="T204" id="Seg_658" s="T203">в.году</ta>
            <ta e="T205" id="Seg_659" s="T204">отправиться-PST-1SG.S</ta>
            <ta e="T206" id="Seg_660" s="T205">работа-1SG.TRL</ta>
            <ta e="T0" id="Seg_661" s="T206">кость-ADJZ</ta>
            <ta e="T1" id="Seg_662" s="T0">рыба.[NOM]</ta>
            <ta e="T207" id="Seg_663" s="T1">озеро-ILL</ta>
            <ta e="T208" id="Seg_664" s="T207">рыба-VBLZ-SUP.1SG</ta>
            <ta e="T209" id="Seg_665" s="T208">и</ta>
            <ta e="T210" id="Seg_666" s="T209">%%</ta>
            <ta e="T211" id="Seg_667" s="T210">там</ta>
            <ta e="T212" id="Seg_668" s="T211">рыба-VBLZ-CO-1SG.S</ta>
            <ta e="T213" id="Seg_669" s="T212">отец.[NOM]-1SG</ta>
            <ta e="T214" id="Seg_670" s="T213">тоже</ta>
            <ta e="T215" id="Seg_671" s="T214">рыба-VBLZ-CO.[3SG.S]</ta>
            <ta e="T216" id="Seg_672" s="T215">мать.[NOM]-1SG</ta>
            <ta e="T217" id="Seg_673" s="T216">NEG</ta>
            <ta e="T218" id="Seg_674" s="T217">рыба-VBLZ-CO.[3SG.S]</ta>
            <ta e="T219" id="Seg_675" s="T218">домой-ADV.LOC</ta>
            <ta e="T220" id="Seg_676" s="T219">дом-LOC</ta>
            <ta e="T221" id="Seg_677" s="T220">работать.[3SG.S]</ta>
            <ta e="T222" id="Seg_678" s="T221">брат.[NOM]-1SG</ta>
            <ta e="T223" id="Seg_679" s="T222">старший</ta>
            <ta e="T224" id="Seg_680" s="T223">звать-3PL</ta>
            <ta e="T225" id="Seg_681" s="T224">Володя-TRL</ta>
            <ta e="T226" id="Seg_682" s="T225">он(а).[NOM]</ta>
            <ta e="T227" id="Seg_683" s="T226">тоже</ta>
            <ta e="T228" id="Seg_684" s="T227">работать.[3SG.S]</ta>
            <ta e="T229" id="Seg_685" s="T228">совхоз-LOC</ta>
            <ta e="T230" id="Seg_686" s="T229">младший</ta>
            <ta e="T231" id="Seg_687" s="T230">брат.[NOM]-1SG</ta>
            <ta e="T232" id="Seg_688" s="T231">звать-3PL</ta>
            <ta e="T233" id="Seg_689" s="T232">Юрий-TRL</ta>
            <ta e="T234" id="Seg_690" s="T233">он(а).[NOM]</ta>
            <ta e="T235" id="Seg_691" s="T234">школа-LOC</ta>
            <ta e="T236" id="Seg_692" s="T235">учиться.[3SG.S]</ta>
            <ta e="T237" id="Seg_693" s="T236">сестра.[NOM]-1SG</ta>
            <ta e="T238" id="Seg_694" s="T237">тоже</ta>
            <ta e="T239" id="Seg_695" s="T238">школа-LOC</ta>
            <ta e="T240" id="Seg_696" s="T239">учиться.[3SG.S]</ta>
            <ta e="T241" id="Seg_697" s="T240">я.NOM</ta>
            <ta e="T242" id="Seg_698" s="T241">затем</ta>
            <ta e="T243" id="Seg_699" s="T242">всё</ta>
            <ta e="T244" id="Seg_700" s="T243">это</ta>
            <ta e="T245" id="Seg_701" s="T244">писать-FRQ-EP-1SG.O</ta>
            <ta e="T246" id="Seg_702" s="T245">что-EP-ACC</ta>
            <ta e="T247" id="Seg_703" s="T246">хотеть-PST-1SG.S</ta>
            <ta e="T248" id="Seg_704" s="T247">писать-FRQ-INF</ta>
            <ta e="T249" id="Seg_705" s="T248">я.ACC</ta>
            <ta e="T250" id="Seg_706" s="T249">звать-3PL</ta>
            <ta e="T251" id="Seg_707" s="T250">Витя-TRL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T186" id="Seg_708" s="T185">pers</ta>
            <ta e="T187" id="Seg_709" s="T186">v-v:tense-v:pn</ta>
            <ta e="T188" id="Seg_710" s="T187">nprop-n:case3</ta>
            <ta e="T189" id="Seg_711" s="T188">num</ta>
            <ta e="T190" id="Seg_712" s="T189">%%</ta>
            <ta e="T191" id="Seg_713" s="T190">num</ta>
            <ta e="T192" id="Seg_714" s="T191">%%</ta>
            <ta e="T193" id="Seg_715" s="T192">v-v:tense-v:pn</ta>
            <ta e="T194" id="Seg_716" s="T193">n-n&gt;adj</ta>
            <ta e="T195" id="Seg_717" s="T194">n-n:case3</ta>
            <ta e="T196" id="Seg_718" s="T195">v-v:inf</ta>
            <ta e="T197" id="Seg_719" s="T196">n-n&gt;adj</ta>
            <ta e="T198" id="Seg_720" s="T197">n-n:case3</ta>
            <ta e="T199" id="Seg_721" s="T198">v-v:tense-v:pn</ta>
            <ta e="T200" id="Seg_722" s="T199">num</ta>
            <ta e="T201" id="Seg_723" s="T200">n-n:case1-n:poss</ta>
            <ta e="T202" id="Seg_724" s="T201">pp</ta>
            <ta e="T203" id="Seg_725" s="T202">num</ta>
            <ta e="T204" id="Seg_726" s="T203">%%</ta>
            <ta e="T205" id="Seg_727" s="T204">v-v:tense-v:pn</ta>
            <ta e="T206" id="Seg_728" s="T205">n-n:poss-case</ta>
            <ta e="T0" id="Seg_729" s="T206">n-n&gt;adj</ta>
            <ta e="T1" id="Seg_730" s="T0">n</ta>
            <ta e="T207" id="Seg_731" s="T1">n-n:case3</ta>
            <ta e="T208" id="Seg_732" s="T207">n-n&gt;v-v:inf-poss</ta>
            <ta e="T209" id="Seg_733" s="T208">conj</ta>
            <ta e="T210" id="Seg_734" s="T209">adv</ta>
            <ta e="T211" id="Seg_735" s="T210">adv</ta>
            <ta e="T212" id="Seg_736" s="T211">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T213" id="Seg_737" s="T212">n-n:case1-n:poss</ta>
            <ta e="T214" id="Seg_738" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_739" s="T214">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T216" id="Seg_740" s="T215">n-n:case1-n:poss</ta>
            <ta e="T217" id="Seg_741" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_742" s="T217">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T219" id="Seg_743" s="T218">adv-adv:advcase</ta>
            <ta e="T220" id="Seg_744" s="T219">n-n:case3</ta>
            <ta e="T221" id="Seg_745" s="T220">v-v:pn</ta>
            <ta e="T222" id="Seg_746" s="T221">n-n:case1-n:poss</ta>
            <ta e="T223" id="Seg_747" s="T222">adj</ta>
            <ta e="T224" id="Seg_748" s="T223">v-v:pn</ta>
            <ta e="T225" id="Seg_749" s="T224">nprop-n:case3</ta>
            <ta e="T226" id="Seg_750" s="T225">pers-n:case3</ta>
            <ta e="T227" id="Seg_751" s="T226">ptcl</ta>
            <ta e="T228" id="Seg_752" s="T227">v-v:pn</ta>
            <ta e="T229" id="Seg_753" s="T228">n-n:case3</ta>
            <ta e="T230" id="Seg_754" s="T229">adj</ta>
            <ta e="T231" id="Seg_755" s="T230">n-n:case1-n:poss</ta>
            <ta e="T232" id="Seg_756" s="T231">v-v:pn</ta>
            <ta e="T233" id="Seg_757" s="T232">nprop-n:case3</ta>
            <ta e="T234" id="Seg_758" s="T233">pers-n:case3</ta>
            <ta e="T235" id="Seg_759" s="T234">n-n:case3</ta>
            <ta e="T236" id="Seg_760" s="T235">v-v:pn</ta>
            <ta e="T237" id="Seg_761" s="T236">n-n:case1-n:poss</ta>
            <ta e="T238" id="Seg_762" s="T237">ptcl</ta>
            <ta e="T239" id="Seg_763" s="T238">n-n:case3</ta>
            <ta e="T240" id="Seg_764" s="T239">v-v:pn</ta>
            <ta e="T241" id="Seg_765" s="T240">pers</ta>
            <ta e="T242" id="Seg_766" s="T241">adv</ta>
            <ta e="T243" id="Seg_767" s="T242">quant</ta>
            <ta e="T244" id="Seg_768" s="T243">pro</ta>
            <ta e="T245" id="Seg_769" s="T244">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T246" id="Seg_770" s="T245">interrog-n:(ins)-n:case3</ta>
            <ta e="T247" id="Seg_771" s="T246">v-v:tense-v:pn</ta>
            <ta e="T248" id="Seg_772" s="T247">v-v&gt;v-v:inf</ta>
            <ta e="T249" id="Seg_773" s="T248">pers</ta>
            <ta e="T250" id="Seg_774" s="T249">v-v:pn</ta>
            <ta e="T251" id="Seg_775" s="T250">nprop-n:case3</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T186" id="Seg_776" s="T185">pers</ta>
            <ta e="T187" id="Seg_777" s="T186">v</ta>
            <ta e="T188" id="Seg_778" s="T187">nprop</ta>
            <ta e="T189" id="Seg_779" s="T188">num</ta>
            <ta e="T190" id="Seg_780" s="T189">%%</ta>
            <ta e="T191" id="Seg_781" s="T190">num</ta>
            <ta e="T192" id="Seg_782" s="T191">%%</ta>
            <ta e="T193" id="Seg_783" s="T192">v</ta>
            <ta e="T194" id="Seg_784" s="T193">adj</ta>
            <ta e="T195" id="Seg_785" s="T194">n</ta>
            <ta e="T196" id="Seg_786" s="T195">v</ta>
            <ta e="T197" id="Seg_787" s="T196">n</ta>
            <ta e="T198" id="Seg_788" s="T197">n</ta>
            <ta e="T199" id="Seg_789" s="T198">v</ta>
            <ta e="T200" id="Seg_790" s="T199">num</ta>
            <ta e="T201" id="Seg_791" s="T200">n</ta>
            <ta e="T202" id="Seg_792" s="T201">pp</ta>
            <ta e="T203" id="Seg_793" s="T202">num</ta>
            <ta e="T204" id="Seg_794" s="T203">%%</ta>
            <ta e="T205" id="Seg_795" s="T204">v</ta>
            <ta e="T206" id="Seg_796" s="T205">n</ta>
            <ta e="T0" id="Seg_797" s="T206">adj</ta>
            <ta e="T1" id="Seg_798" s="T0">n</ta>
            <ta e="T207" id="Seg_799" s="T1">n</ta>
            <ta e="T208" id="Seg_800" s="T207">v</ta>
            <ta e="T209" id="Seg_801" s="T208">conj</ta>
            <ta e="T210" id="Seg_802" s="T209">adv</ta>
            <ta e="T211" id="Seg_803" s="T210">adv</ta>
            <ta e="T212" id="Seg_804" s="T211">v</ta>
            <ta e="T213" id="Seg_805" s="T212">n</ta>
            <ta e="T214" id="Seg_806" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_807" s="T214">v</ta>
            <ta e="T216" id="Seg_808" s="T215">n</ta>
            <ta e="T217" id="Seg_809" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_810" s="T217">v</ta>
            <ta e="T219" id="Seg_811" s="T218">adv</ta>
            <ta e="T220" id="Seg_812" s="T219">n</ta>
            <ta e="T221" id="Seg_813" s="T220">v</ta>
            <ta e="T222" id="Seg_814" s="T221">n</ta>
            <ta e="T223" id="Seg_815" s="T222">adj</ta>
            <ta e="T224" id="Seg_816" s="T223">v</ta>
            <ta e="T225" id="Seg_817" s="T224">nprop</ta>
            <ta e="T226" id="Seg_818" s="T225">pers</ta>
            <ta e="T227" id="Seg_819" s="T226">adv</ta>
            <ta e="T228" id="Seg_820" s="T227">v</ta>
            <ta e="T229" id="Seg_821" s="T228">n</ta>
            <ta e="T230" id="Seg_822" s="T229">adj</ta>
            <ta e="T231" id="Seg_823" s="T230">n</ta>
            <ta e="T232" id="Seg_824" s="T231">v</ta>
            <ta e="T233" id="Seg_825" s="T232">nprop</ta>
            <ta e="T234" id="Seg_826" s="T233">pers</ta>
            <ta e="T235" id="Seg_827" s="T234">n</ta>
            <ta e="T236" id="Seg_828" s="T235">v</ta>
            <ta e="T237" id="Seg_829" s="T236">n</ta>
            <ta e="T238" id="Seg_830" s="T237">ptcl</ta>
            <ta e="T239" id="Seg_831" s="T238">n</ta>
            <ta e="T240" id="Seg_832" s="T239">v</ta>
            <ta e="T241" id="Seg_833" s="T240">pers</ta>
            <ta e="T242" id="Seg_834" s="T241">adv</ta>
            <ta e="T243" id="Seg_835" s="T242">quant</ta>
            <ta e="T244" id="Seg_836" s="T243">pro</ta>
            <ta e="T245" id="Seg_837" s="T244">v</ta>
            <ta e="T246" id="Seg_838" s="T245">interrog</ta>
            <ta e="T247" id="Seg_839" s="T246">v</ta>
            <ta e="T248" id="Seg_840" s="T247">v</ta>
            <ta e="T249" id="Seg_841" s="T248">pers</ta>
            <ta e="T250" id="Seg_842" s="T249">v</ta>
            <ta e="T251" id="Seg_843" s="T250">nprop</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T186" id="Seg_844" s="T185">np.h:E</ta>
            <ta e="T188" id="Seg_845" s="T187">np:L</ta>
            <ta e="T190" id="Seg_846" s="T188">np:Time</ta>
            <ta e="T192" id="Seg_847" s="T190">np:Time</ta>
            <ta e="T193" id="Seg_848" s="T192">0.1.h:A</ta>
            <ta e="T195" id="Seg_849" s="T193">np:L</ta>
            <ta e="T198" id="Seg_850" s="T196">np:L</ta>
            <ta e="T199" id="Seg_851" s="T198">0.1.h:A</ta>
            <ta e="T202" id="Seg_852" s="T201">pp:Time</ta>
            <ta e="T204" id="Seg_853" s="T202">np:Time</ta>
            <ta e="T205" id="Seg_854" s="T204">0.1.h:A</ta>
            <ta e="T207" id="Seg_855" s="T1">np:G</ta>
            <ta e="T211" id="Seg_856" s="T210">adv:L</ta>
            <ta e="T212" id="Seg_857" s="T211">0.1.h:A</ta>
            <ta e="T213" id="Seg_858" s="T212">np.h:A</ta>
            <ta e="T216" id="Seg_859" s="T215">np.h:A</ta>
            <ta e="T219" id="Seg_860" s="T218">adv:L</ta>
            <ta e="T220" id="Seg_861" s="T219">np:L</ta>
            <ta e="T221" id="Seg_862" s="T220">0.3.h:A</ta>
            <ta e="T222" id="Seg_863" s="T221">np.h:R</ta>
            <ta e="T224" id="Seg_864" s="T223">0.3.h:A</ta>
            <ta e="T226" id="Seg_865" s="T225">pro.h:A</ta>
            <ta e="T229" id="Seg_866" s="T228">np:L</ta>
            <ta e="T231" id="Seg_867" s="T230">np.h:R</ta>
            <ta e="T232" id="Seg_868" s="T231">0.3.h:A</ta>
            <ta e="T234" id="Seg_869" s="T233">pro.h:A</ta>
            <ta e="T235" id="Seg_870" s="T234">np:L</ta>
            <ta e="T237" id="Seg_871" s="T236">np.h:A</ta>
            <ta e="T239" id="Seg_872" s="T238">np:L</ta>
            <ta e="T241" id="Seg_873" s="T240">pro.h:A</ta>
            <ta e="T246" id="Seg_874" s="T245">np:Th</ta>
            <ta e="T249" id="Seg_875" s="T248">pro.h:R</ta>
            <ta e="T250" id="Seg_876" s="T249">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T186" id="Seg_877" s="T185">pro.h:S</ta>
            <ta e="T187" id="Seg_878" s="T186">v:pred</ta>
            <ta e="T193" id="Seg_879" s="T192">0.1.h:S v:pred</ta>
            <ta e="T196" id="Seg_880" s="T195">s:purp</ta>
            <ta e="T199" id="Seg_881" s="T198">0.1.h:S v:pred</ta>
            <ta e="T205" id="Seg_882" s="T204">0.1.h:S v:pred</ta>
            <ta e="T208" id="Seg_883" s="T207">s:purp</ta>
            <ta e="T212" id="Seg_884" s="T211">0.1.h:S v:pred</ta>
            <ta e="T213" id="Seg_885" s="T212">np.h:S</ta>
            <ta e="T215" id="Seg_886" s="T214">v:pred</ta>
            <ta e="T216" id="Seg_887" s="T215">np.h:S</ta>
            <ta e="T218" id="Seg_888" s="T217">v:pred</ta>
            <ta e="T221" id="Seg_889" s="T220">0.3.h:S v:pred</ta>
            <ta e="T222" id="Seg_890" s="T221">np.h:O</ta>
            <ta e="T224" id="Seg_891" s="T223">0.3.h:S v:pred</ta>
            <ta e="T226" id="Seg_892" s="T225">pro.h:S</ta>
            <ta e="T228" id="Seg_893" s="T227">v:pred</ta>
            <ta e="T231" id="Seg_894" s="T230">np.h:O</ta>
            <ta e="T232" id="Seg_895" s="T231">0.3.h:S v:pred</ta>
            <ta e="T234" id="Seg_896" s="T233">pro.h:S</ta>
            <ta e="T236" id="Seg_897" s="T235">v:pred</ta>
            <ta e="T237" id="Seg_898" s="T236">np.h:S</ta>
            <ta e="T240" id="Seg_899" s="T239">v:pred</ta>
            <ta e="T241" id="Seg_900" s="T240">pro.h:S</ta>
            <ta e="T245" id="Seg_901" s="T244">v:pred</ta>
            <ta e="T246" id="Seg_902" s="T245">pro:O</ta>
            <ta e="T247" id="Seg_903" s="T246">0.1.h:S v:pred</ta>
            <ta e="T248" id="Seg_904" s="T247">s:compl</ta>
            <ta e="T249" id="Seg_905" s="T248">np.h:O</ta>
            <ta e="T250" id="Seg_906" s="T249">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T186" id="Seg_907" s="T185">new</ta>
            <ta e="T188" id="Seg_908" s="T187">accs-gen</ta>
            <ta e="T193" id="Seg_909" s="T192">0.giv-active</ta>
            <ta e="T195" id="Seg_910" s="T194">accs-gen</ta>
            <ta e="T198" id="Seg_911" s="T197">giv-active</ta>
            <ta e="T199" id="Seg_912" s="T198">0.giv-active</ta>
            <ta e="T201" id="Seg_913" s="T200">accs-gen</ta>
            <ta e="T205" id="Seg_914" s="T204">0.giv-active</ta>
            <ta e="T206" id="Seg_915" s="T205">accs-gen</ta>
            <ta e="T207" id="Seg_916" s="T1">accs-gen</ta>
            <ta e="T212" id="Seg_917" s="T211">0.giv-active</ta>
            <ta e="T213" id="Seg_918" s="T212">new</ta>
            <ta e="T216" id="Seg_919" s="T215">new</ta>
            <ta e="T221" id="Seg_920" s="T220">0.giv-active</ta>
            <ta e="T222" id="Seg_921" s="T221">new</ta>
            <ta e="T226" id="Seg_922" s="T225">giv-active</ta>
            <ta e="T229" id="Seg_923" s="T228">accs-gen</ta>
            <ta e="T231" id="Seg_924" s="T230">new</ta>
            <ta e="T234" id="Seg_925" s="T233">giv-active</ta>
            <ta e="T235" id="Seg_926" s="T234">accs-gen</ta>
            <ta e="T237" id="Seg_927" s="T236">new</ta>
            <ta e="T239" id="Seg_928" s="T238">accs-gen</ta>
            <ta e="T241" id="Seg_929" s="T240">giv-inactive</ta>
            <ta e="T246" id="Seg_930" s="T245">accs-inf</ta>
            <ta e="T247" id="Seg_931" s="T246">0.giv-active</ta>
            <ta e="T249" id="Seg_932" s="T248">giv-active</ta>
            <ta e="T250" id="Seg_933" s="T249">0.accs-gen</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T190" id="Seg_934" s="T189">RUS:cult</ta>
            <ta e="T192" id="Seg_935" s="T191">RUS:cult</ta>
            <ta e="T196" id="Seg_936" s="T195">RUS:cult</ta>
            <ta e="T199" id="Seg_937" s="T198">RUS:cult</ta>
            <ta e="T204" id="Seg_938" s="T203">RUS:cult</ta>
            <ta e="T206" id="Seg_939" s="T205">RUS:cult</ta>
            <ta e="T209" id="Seg_940" s="T208">RUS:gram</ta>
            <ta e="T225" id="Seg_941" s="T224">RUS:cult</ta>
            <ta e="T228" id="Seg_942" s="T227">RUS:cult</ta>
            <ta e="T229" id="Seg_943" s="T228">RUS:cult</ta>
            <ta e="T233" id="Seg_944" s="T232">RUS:cult</ta>
            <ta e="T235" id="Seg_945" s="T234">RUS:cult</ta>
            <ta e="T236" id="Seg_946" s="T235">RUS:cult</ta>
            <ta e="T239" id="Seg_947" s="T238">RUS:cult</ta>
            <ta e="T240" id="Seg_948" s="T239">RUS:cult</ta>
            <ta e="T251" id="Seg_949" s="T250">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T190" id="Seg_950" s="T189">parad:bare</ta>
            <ta e="T192" id="Seg_951" s="T191">parad:bare</ta>
            <ta e="T196" id="Seg_952" s="T195">indir:infl</ta>
            <ta e="T199" id="Seg_953" s="T198">indir:infl</ta>
            <ta e="T204" id="Seg_954" s="T203">parad:bare</ta>
            <ta e="T206" id="Seg_955" s="T205">indir:infl</ta>
            <ta e="T209" id="Seg_956" s="T208">dir:bare</ta>
            <ta e="T225" id="Seg_957" s="T224">dir:infl</ta>
            <ta e="T228" id="Seg_958" s="T227">indir:infl</ta>
            <ta e="T229" id="Seg_959" s="T228">dir:infl</ta>
            <ta e="T233" id="Seg_960" s="T232">dir:infl</ta>
            <ta e="T235" id="Seg_961" s="T234">dir:infl</ta>
            <ta e="T236" id="Seg_962" s="T235">indir:infl</ta>
            <ta e="T239" id="Seg_963" s="T238">dir:infl</ta>
            <ta e="T240" id="Seg_964" s="T239">indir:infl</ta>
            <ta e="T251" id="Seg_965" s="T250">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T190" id="Seg_966" s="T188">int:ins</ta>
            <ta e="T192" id="Seg_967" s="T190">int:ins</ta>
            <ta e="T204" id="Seg_968" s="T202">int:ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T190" id="Seg_969" s="T185">Я родился в Часельке в 1945 году.</ta>
            <ta e="T196" id="Seg_970" s="T190">В 1954 году [я] поехал в Красноселькуп учиться.</ta>
            <ta e="T202" id="Seg_971" s="T196">В Красноселькупе [я] учился шесть лет.</ta>
            <ta e="T208" id="Seg_972" s="T202">В 1960 году [я] поехал работать на озеро Хале то рыбачить. </ta>
            <ta e="T212" id="Seg_973" s="T208">И теперь там рыбачу.</ta>
            <ta e="T215" id="Seg_974" s="T212">[Мой] отец тоже рыбачит.</ta>
            <ta e="T221" id="Seg_975" s="T215">[Моя] мать не рыбачит, дома, в доме работает.</ta>
            <ta e="T225" id="Seg_976" s="T221">Старшего брата зовут Володей.</ta>
            <ta e="T229" id="Seg_977" s="T225">Он тоже работает в совхозе.</ta>
            <ta e="T233" id="Seg_978" s="T229">Младшего брата зовут Юрием.</ta>
            <ta e="T236" id="Seg_979" s="T233">Он в школе учится.</ta>
            <ta e="T240" id="Seg_980" s="T236">[Моя] сестра тоже в школе учится.</ta>
            <ta e="T248" id="Seg_981" s="T240">Я всё написал, что хотел написать.</ta>
            <ta e="T251" id="Seg_982" s="T248">Меня зовут Витей.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T190" id="Seg_983" s="T185">I was born in Chaselka in 1945. </ta>
            <ta e="T196" id="Seg_984" s="T190">In 1954 I went to school in Krasnoselkup.</ta>
            <ta e="T202" id="Seg_985" s="T196">I went to school in Krasnoselkup for six years.</ta>
            <ta e="T208" id="Seg_986" s="T202">In 1960 I went to the Fishbone Lake to work fishing.</ta>
            <ta e="T212" id="Seg_987" s="T208">And I still go fishing there. </ta>
            <ta e="T215" id="Seg_988" s="T212">My father goes fishing too.</ta>
            <ta e="T221" id="Seg_989" s="T215">My mother doesn't go fishing, at home, she is working at home.</ta>
            <ta e="T225" id="Seg_990" s="T221">My elder brother is called Volodya.</ta>
            <ta e="T229" id="Seg_991" s="T225">He also works in sovkhoz.</ta>
            <ta e="T233" id="Seg_992" s="T229">My younger brother is called Juriy.</ta>
            <ta e="T236" id="Seg_993" s="T233">He goes to school.</ta>
            <ta e="T240" id="Seg_994" s="T236">My sister also goes to school.</ta>
            <ta e="T248" id="Seg_995" s="T240">I've written down everything that I wanted to.</ta>
            <ta e="T251" id="Seg_996" s="T248">They call me Vitya.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T190" id="Seg_997" s="T185">Ich wurde 1945 in Chaselka geboren.</ta>
            <ta e="T196" id="Seg_998" s="T190">1954 bin ich zum Lernen nach Krasnoselkup gegangen.</ta>
            <ta e="T202" id="Seg_999" s="T196">In Krasnoselkup habe ich sechs Jahre lang gelernt.</ta>
            <ta e="T208" id="Seg_1000" s="T202">1960 bin ich zur Arbeit zum Grätensee gefahren, um zu fischen.</ta>
            <ta e="T212" id="Seg_1001" s="T208">Und jetzt fische ich dort auch.</ta>
            <ta e="T215" id="Seg_1002" s="T212">Mein Vater geht auch fischen.</ta>
            <ta e="T221" id="Seg_1003" s="T215">Meine Mutter geht nicht fischen, [sie ist] zu Hause, sie arbeitet zu Hause.</ta>
            <ta e="T225" id="Seg_1004" s="T221">Mein älterer Bruder heißt Volodja.</ta>
            <ta e="T229" id="Seg_1005" s="T225">Er arbeitet auch in der Sowchose.</ta>
            <ta e="T233" id="Seg_1006" s="T229">Mein jüngerer Bruder heißt Jurij.</ta>
            <ta e="T236" id="Seg_1007" s="T233">Er geht zur Schule.</ta>
            <ta e="T240" id="Seg_1008" s="T236">Meine Schwester geht auch zur Schule.</ta>
            <ta e="T248" id="Seg_1009" s="T240">Ich habe alles aufgeschrieben, was ich wollte.</ta>
            <ta e="T251" id="Seg_1010" s="T248">Ich heiße Vitja.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T190" id="Seg_1011" s="T185">я родился в Часальке</ta>
            <ta e="T196" id="Seg_1012" s="T190">поехал в красноселькупск учиться</ta>
            <ta e="T202" id="Seg_1013" s="T196">в Красноселькупске учился шесть лет.</ta>
            <ta e="T208" id="Seg_1014" s="T202">поехал работать в озеро халето рыбачить </ta>
            <ta e="T212" id="Seg_1015" s="T208">и теперь там рыбачу</ta>
            <ta e="T215" id="Seg_1016" s="T212">отец тоже рыбачит</ta>
            <ta e="T221" id="Seg_1017" s="T215">мать не рыбачит дома в доме работает</ta>
            <ta e="T225" id="Seg_1018" s="T221">брат старший зовут Володей</ta>
            <ta e="T229" id="Seg_1019" s="T225">он работает совхозе</ta>
            <ta e="T233" id="Seg_1020" s="T229">младшего брата зовут Юрием</ta>
            <ta e="T236" id="Seg_1021" s="T233">он школе учится</ta>
            <ta e="T240" id="Seg_1022" s="T236">сестра тоже школе учится</ta>
            <ta e="T248" id="Seg_1023" s="T240">я все написал что хотел написать</ta>
            <ta e="T251" id="Seg_1024" s="T248">меня зовут Виктором</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T196" id="Seg_1025" s="T190">[OSV]: "Nʼarɨlʼ Mačʼɨ" - Krasnoselkup.</ta>
            <ta e="T202" id="Seg_1026" s="T196">[OSV]: "Nʼarɨlʼ Mačʼɨ" - Krasnoselkup.</ta>
            <ta e="T208" id="Seg_1027" s="T202"> ‎‎[OSV]: "Ləlʼ qəːl toː" is the name of a lake (= the Fishbone Lake), in Russian the Nenets name is used - "Хале то".</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
