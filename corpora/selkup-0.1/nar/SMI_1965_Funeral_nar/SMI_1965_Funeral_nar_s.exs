<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID77AB61A4-4576-AE8A-4E63-487607F881ED">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\SMI_1965_Funeral_nar\SMI_1965_Funeral_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">16</ud-information>
            <ud-information attribute-name="# HIAT:w">12</ud-information>
            <ud-information attribute-name="# e">12</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SMI">
            <abbreviation>SMI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SMI"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T12" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Mej</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">qumɨt</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">poːp</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">meːsɔːmɨn</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Aj</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">pissɨmɨt</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">poːntɨ</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">Nɨːnɨ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">qɨltɨ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">meːsɔːmɨn</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">İllä</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">üːtɨsɔːmɨn</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T12" id="Seg_49" n="sc" s="T0">
               <ts e="T1" id="Seg_51" n="e" s="T0">Mej </ts>
               <ts e="T2" id="Seg_53" n="e" s="T1">qumɨt </ts>
               <ts e="T3" id="Seg_55" n="e" s="T2">poːp </ts>
               <ts e="T4" id="Seg_57" n="e" s="T3">meːsɔːmɨn. </ts>
               <ts e="T5" id="Seg_59" n="e" s="T4">Aj </ts>
               <ts e="T6" id="Seg_61" n="e" s="T5">pissɨmɨt </ts>
               <ts e="T7" id="Seg_63" n="e" s="T6">poːntɨ. </ts>
               <ts e="T8" id="Seg_65" n="e" s="T7">Nɨːnɨ </ts>
               <ts e="T9" id="Seg_67" n="e" s="T8">qɨltɨ </ts>
               <ts e="T10" id="Seg_69" n="e" s="T9">meːsɔːmɨn. </ts>
               <ts e="T11" id="Seg_71" n="e" s="T10">İllä </ts>
               <ts e="T12" id="Seg_73" n="e" s="T11">üːtɨsɔːmɨn. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_74" s="T0">SMI_1965_Funeral_nar.001</ta>
            <ta e="T7" id="Seg_75" s="T4">SMI_1965_Funeral_nar.002</ta>
            <ta e="T10" id="Seg_76" s="T7">SMI_1965_Funeral_nar.003</ta>
            <ta e="T12" id="Seg_77" s="T10">SMI_1965_Funeral_nar.004</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_78" s="T0">Мей ′kумытпоп ′месамын.</ta>
            <ta e="T7" id="Seg_79" s="T4">ӓй ′писсемыт ′понтъ.</ta>
            <ta e="T10" id="Seg_80" s="T7">′ныны ′kыlтӓ ′ме̄самын.</ta>
            <ta e="T12" id="Seg_81" s="T10">′иllа′ӱ̄тасамын.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_82" s="T0">Мej qumɨtpop mesamɨn.</ta>
            <ta e="T7" id="Seg_83" s="T4">äj pissemɨt pontə.</ta>
            <ta e="T10" id="Seg_84" s="T7">nɨnɨ qɨlʼtä meːsamɨn.</ta>
            <ta e="T12" id="Seg_85" s="T10">ilʼlʼaüːtasamɨn.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_86" s="T0">Mej qumɨt poːp meːsɔːmɨn. </ta>
            <ta e="T7" id="Seg_87" s="T4">Aj pissɨmɨt poːntɨ. </ta>
            <ta e="T10" id="Seg_88" s="T7">Nɨːnɨ qɨltɨ meːsɔːmɨn. </ta>
            <ta e="T12" id="Seg_89" s="T10">İllä üːtɨsɔːmɨn. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_90" s="T0">mej</ta>
            <ta e="T2" id="Seg_91" s="T1">qum-ɨ-t</ta>
            <ta e="T3" id="Seg_92" s="T2">poː-p</ta>
            <ta e="T4" id="Seg_93" s="T3">meː-sɔː-mɨn</ta>
            <ta e="T5" id="Seg_94" s="T4">aj</ta>
            <ta e="T6" id="Seg_95" s="T5">pis-sɨ-mɨt</ta>
            <ta e="T7" id="Seg_96" s="T6">poː-ntɨ</ta>
            <ta e="T8" id="Seg_97" s="T7">nɨːnɨ</ta>
            <ta e="T9" id="Seg_98" s="T8">qɨl-tɨ</ta>
            <ta e="T10" id="Seg_99" s="T9">meː-sɔː-mɨn</ta>
            <ta e="T11" id="Seg_100" s="T10">i̇llä</ta>
            <ta e="T12" id="Seg_101" s="T11">üːtɨ-sɔː-mɨn</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_102" s="T0">meː</ta>
            <ta e="T2" id="Seg_103" s="T1">qum-ɨ-n</ta>
            <ta e="T3" id="Seg_104" s="T2">poː-m</ta>
            <ta e="T4" id="Seg_105" s="T3">meː-sɨ-mɨt</ta>
            <ta e="T5" id="Seg_106" s="T4">aj</ta>
            <ta e="T6" id="Seg_107" s="T5">pin-sɨ-mɨt</ta>
            <ta e="T7" id="Seg_108" s="T6">poː-ntɨ</ta>
            <ta e="T8" id="Seg_109" s="T7">nɨːnɨ</ta>
            <ta e="T9" id="Seg_110" s="T8">qɨl-tɨ</ta>
            <ta e="T10" id="Seg_111" s="T9">meː-sɨ-mɨt</ta>
            <ta e="T11" id="Seg_112" s="T10">ıllä</ta>
            <ta e="T12" id="Seg_113" s="T11">üːtɨ-sɨ-mɨt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_114" s="T0">we.DU.NOM</ta>
            <ta e="T2" id="Seg_115" s="T1">human.being-EP-GEN</ta>
            <ta e="T3" id="Seg_116" s="T2">tree-ACC</ta>
            <ta e="T4" id="Seg_117" s="T3">make-PST-1PL</ta>
            <ta e="T5" id="Seg_118" s="T4">and</ta>
            <ta e="T6" id="Seg_119" s="T5">put-PST-1PL</ta>
            <ta e="T7" id="Seg_120" s="T6">tree-ILL</ta>
            <ta e="T8" id="Seg_121" s="T7">then</ta>
            <ta e="T9" id="Seg_122" s="T8">hole.[NOM]-3SG</ta>
            <ta e="T10" id="Seg_123" s="T9">make-PST-1PL</ta>
            <ta e="T11" id="Seg_124" s="T10">down</ta>
            <ta e="T12" id="Seg_125" s="T11">let.go-PST-1PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_126" s="T0">мы.DU.NOM</ta>
            <ta e="T2" id="Seg_127" s="T1">человек-EP-GEN</ta>
            <ta e="T3" id="Seg_128" s="T2">дерево-ACC</ta>
            <ta e="T4" id="Seg_129" s="T3">сделать-PST-1PL</ta>
            <ta e="T5" id="Seg_130" s="T4">и</ta>
            <ta e="T6" id="Seg_131" s="T5">положить-PST-1PL</ta>
            <ta e="T7" id="Seg_132" s="T6">дерево-ILL</ta>
            <ta e="T8" id="Seg_133" s="T7">потом</ta>
            <ta e="T9" id="Seg_134" s="T8">яма.[NOM]-3SG</ta>
            <ta e="T10" id="Seg_135" s="T9">сделать-PST-1PL</ta>
            <ta e="T11" id="Seg_136" s="T10">вниз</ta>
            <ta e="T12" id="Seg_137" s="T11">пустить-PST-1PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_138" s="T0">pers</ta>
            <ta e="T2" id="Seg_139" s="T1">n-n:(ins)-n:case3</ta>
            <ta e="T3" id="Seg_140" s="T2">n-n:case3</ta>
            <ta e="T4" id="Seg_141" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_142" s="T4">conj</ta>
            <ta e="T6" id="Seg_143" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_144" s="T6">n-n:case3</ta>
            <ta e="T8" id="Seg_145" s="T7">adv</ta>
            <ta e="T9" id="Seg_146" s="T8">n-n:case1-n:poss</ta>
            <ta e="T10" id="Seg_147" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_148" s="T10">adv</ta>
            <ta e="T12" id="Seg_149" s="T11">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_150" s="T0">pers</ta>
            <ta e="T2" id="Seg_151" s="T1">n</ta>
            <ta e="T3" id="Seg_152" s="T2">n</ta>
            <ta e="T4" id="Seg_153" s="T3">v</ta>
            <ta e="T5" id="Seg_154" s="T4">conj</ta>
            <ta e="T6" id="Seg_155" s="T5">v</ta>
            <ta e="T7" id="Seg_156" s="T6">n</ta>
            <ta e="T8" id="Seg_157" s="T7">adv</ta>
            <ta e="T9" id="Seg_158" s="T8">n</ta>
            <ta e="T10" id="Seg_159" s="T9">v</ta>
            <ta e="T11" id="Seg_160" s="T10">adv</ta>
            <ta e="T12" id="Seg_161" s="T11">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_162" s="T0">pro.h:A</ta>
            <ta e="T3" id="Seg_163" s="T2">np:Th</ta>
            <ta e="T6" id="Seg_164" s="T5">0.1.h:A 0.3.h:P</ta>
            <ta e="T7" id="Seg_165" s="T6">np:G</ta>
            <ta e="T9" id="Seg_166" s="T8">np:Th</ta>
            <ta e="T10" id="Seg_167" s="T9">0.1.h:A</ta>
            <ta e="T12" id="Seg_168" s="T11">0.1.h:A 0.3.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_169" s="T0">pro.h:S</ta>
            <ta e="T3" id="Seg_170" s="T2">np:O</ta>
            <ta e="T4" id="Seg_171" s="T3">v:pred</ta>
            <ta e="T6" id="Seg_172" s="T5">0.1.h:S 0.3.h:O v:pred</ta>
            <ta e="T9" id="Seg_173" s="T8">np:O</ta>
            <ta e="T10" id="Seg_174" s="T9">0.1.h:S v:pred</ta>
            <ta e="T12" id="Seg_175" s="T11">0.1.h:S 0.3.h:O v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_176" s="T0">new</ta>
            <ta e="T3" id="Seg_177" s="T2">new</ta>
            <ta e="T6" id="Seg_178" s="T5">0.giv-active 0.new</ta>
            <ta e="T7" id="Seg_179" s="T6">giv-active</ta>
            <ta e="T9" id="Seg_180" s="T8">accs-sit</ta>
            <ta e="T10" id="Seg_181" s="T9">0.giv-active</ta>
            <ta e="T12" id="Seg_182" s="T11">0.giv-active 0.giv-inactive</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_183" s="T0">Мы гроб сделали.</ta>
            <ta e="T7" id="Seg_184" s="T4">И положили в гроб.</ta>
            <ta e="T10" id="Seg_185" s="T7">Потом яму сделали.</ta>
            <ta e="T12" id="Seg_186" s="T10">Похоронили.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_187" s="T0">We made a coffin.</ta>
            <ta e="T7" id="Seg_188" s="T4">And put him into the coffin.</ta>
            <ta e="T10" id="Seg_189" s="T7">Then we made a hole.</ta>
            <ta e="T12" id="Seg_190" s="T10">We buried him.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_191" s="T0">Wir bauten einen Sarg. </ta>
            <ta e="T7" id="Seg_192" s="T4">Und legten ihn in den Sarg. </ta>
            <ta e="T10" id="Seg_193" s="T7">Dann machten gruben wir ein Loch. </ta>
            <ta e="T12" id="Seg_194" s="T10">Wir begruben ihn. </ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_195" s="T0">Мы гроб делали.</ta>
            <ta e="T7" id="Seg_196" s="T4">положили в гроб.</ta>
            <ta e="T10" id="Seg_197" s="T7">потом ямы копали.</ta>
            <ta e="T12" id="Seg_198" s="T10">похоронили.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
