<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDFCD53E82-828F-BFD7-B0BF-4A4937E69415">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\KMS_1963_BearAteTwoWomen_nar\KMS_1963_BearAteTwoWomen_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">385</ud-information>
            <ud-information attribute-name="# HIAT:w">294</ud-information>
            <ud-information attribute-name="# e">294</ud-information>
            <ud-information attribute-name="# HIAT:u">57</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMS">
            <abbreviation>KMS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T294" id="Seg_0" n="sc" s="T0">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">äːsan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">Мarkovoqon</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_11" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">nɨtʼan</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">matʼtʼöqɨn</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">äːsan</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">oqqɨ</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">äːda</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">nimdə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">täbɨn</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">Мarqovo</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">nɨtʼän</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">illɨqus</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">paja</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">nändɨzä</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_56" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">irat</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">täbɨnan</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">čäŋus</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_68" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">illɨqqus</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_73" n="HIAT:w" s="T18">ondä</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_77" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_79" n="HIAT:w" s="T19">nʼäjajgus</ts>
                  <nts id="Seg_80" n="HIAT:ip">,</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_83" n="HIAT:w" s="T20">čoppɨrɨjgus</ts>
                  <nts id="Seg_84" n="HIAT:ip">,</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_87" n="HIAT:w" s="T21">sväŋqɨjgus</ts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_91" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_93" n="HIAT:w" s="T22">oqqɨrɨŋ</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_96" n="HIAT:w" s="T23">äran</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_99" n="HIAT:w" s="T24">nädə</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_102" n="HIAT:w" s="T25">qwannä</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_105" n="HIAT:w" s="T26">peːmɨgu</ts>
                  <nts id="Seg_106" n="HIAT:ip">.</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_109" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_111" n="HIAT:w" s="T27">äːwɨt</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_114" n="HIAT:w" s="T28">äːqus</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_117" n="HIAT:w" s="T29">maːtqɨn</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_121" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_123" n="HIAT:w" s="T30">qaroqənnä</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_126" n="HIAT:w" s="T31">to</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_129" n="HIAT:w" s="T32">ütättä</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_132" n="HIAT:w" s="T33">täp</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_135" n="HIAT:w" s="T34">palʼdʼüs</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_138" n="HIAT:w" s="T35">nʼäjalʼlʼe</ts>
                  <nts id="Seg_139" n="HIAT:ip">.</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_142" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_144" n="HIAT:w" s="T36">tʼeːlɨn</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_147" n="HIAT:w" s="T37">palʼdʼulʼewlʼe</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_150" n="HIAT:w" s="T38">üːdomɨqän</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_153" n="HIAT:w" s="T39">laqqonä</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_156" n="HIAT:w" s="T40">maːtqɨndə</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_160" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_162" n="HIAT:w" s="T41">tüːmbɨŋ</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_165" n="HIAT:w" s="T42">alʼi</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_168" n="HIAT:w" s="T43">moqauqaŋ</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_171" n="HIAT:w" s="T44">čaːǯimba</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_174" n="HIAT:w" s="T45">qottä</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_177" n="HIAT:w" s="T46">maːtqɨndə</ts>
                  <nts id="Seg_178" n="HIAT:ip">.</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_181" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_183" n="HIAT:w" s="T47">tʼelʼeqɨndə</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_186" n="HIAT:w" s="T48">täbɨm</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_189" n="HIAT:w" s="T49">qɨːrɨmbat</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_192" n="HIAT:w" s="T50">qwärqä</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_195" n="HIAT:w" s="T51">näjqun</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_198" n="HIAT:w" s="T52">wättoun</ts>
                  <nts id="Seg_199" n="HIAT:ip">.</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_202" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_204" n="HIAT:w" s="T53">midəmbat</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_207" n="HIAT:w" s="T54">üːdəmɨqɨn</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_210" n="HIAT:w" s="T55">surulʼdi</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_213" n="HIAT:w" s="T56">pajam</ts>
                  <nts id="Seg_214" n="HIAT:ip">.</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_217" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_219" n="HIAT:w" s="T57">oralbat</ts>
                  <nts id="Seg_220" n="HIAT:ip">,</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_223" n="HIAT:w" s="T58">qwätpat</ts>
                  <nts id="Seg_224" n="HIAT:ip">,</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_227" n="HIAT:w" s="T59">aːmbat</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_230" n="HIAT:w" s="T60">täbɨm</ts>
                  <nts id="Seg_231" n="HIAT:ip">.</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_234" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_236" n="HIAT:w" s="T61">qajmɨlamdə</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_239" n="HIAT:w" s="T62">nɨšqɨlgɨlʼbat</ts>
                  <nts id="Seg_240" n="HIAT:ip">,</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_243" n="HIAT:w" s="T63">tülʼəsämdə</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_246" n="HIAT:w" s="T64">toː</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_249" n="HIAT:w" s="T65">tʼätǯimbat</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_253" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_255" n="HIAT:w" s="T66">qalɨmba</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_258" n="HIAT:w" s="T67">oqqə</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_261" n="HIAT:w" s="T68">toːbɨt</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_264" n="HIAT:w" s="T69">taw</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_267" n="HIAT:w" s="T70">nädänanna</ts>
                  <nts id="Seg_268" n="HIAT:ip">.</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_271" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_273" n="HIAT:w" s="T71">üːdəmɨn</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_276" n="HIAT:w" s="T72">äwät</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_279" n="HIAT:w" s="T73">ädəlǯis</ts>
                  <nts id="Seg_280" n="HIAT:ip">.</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_283" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_285" n="HIAT:w" s="T74">nädə</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_288" n="HIAT:w" s="T75">assə</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_291" n="HIAT:w" s="T76">tümba</ts>
                  <nts id="Seg_292" n="HIAT:ip">.</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_295" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_297" n="HIAT:w" s="T77">wes</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_300" n="HIAT:w" s="T78">pin</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_303" n="HIAT:w" s="T79">äwɨt</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_306" n="HIAT:w" s="T80">nändɨgo</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_309" n="HIAT:w" s="T81">täːrbɨlʼe</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_312" n="HIAT:w" s="T82">assə</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_315" n="HIAT:w" s="T83">qondɨs</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_319" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_321" n="HIAT:w" s="T84">taːr</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_324" n="HIAT:w" s="T85">assə</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_327" n="HIAT:w" s="T86">tʼeːlɨmbɨs</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_330" n="HIAT:w" s="T87">äːwɨt</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_333" n="HIAT:w" s="T88">qwannɨ</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_336" n="HIAT:w" s="T89">näːmdə</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_339" n="HIAT:w" s="T90">pergu</ts>
                  <nts id="Seg_340" n="HIAT:ip">.</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_343" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_345" n="HIAT:w" s="T91">telʼdʼelʼi</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_348" n="HIAT:w" s="T92">qwänbədi</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_351" n="HIAT:w" s="T93">wättoːwɨn</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_354" n="HIAT:w" s="T94">äːwɨt</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_357" n="HIAT:w" s="T95">čaːǯiqus</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_360" n="HIAT:w" s="T96">matʼtʼöqɨn</ts>
                  <nts id="Seg_361" n="HIAT:ip">.</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_364" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_366" n="HIAT:w" s="T97">qunnɨ</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_369" n="HIAT:w" s="T98">da</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_372" n="HIAT:w" s="T99">sappɨziŋ</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_375" n="HIAT:w" s="T100">qwäːrɣə</ts>
                  <nts id="Seg_376" n="HIAT:ip">.</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_379" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_381" n="HIAT:w" s="T101">orannɨt</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_384" n="HIAT:w" s="T102">pajam</ts>
                  <nts id="Seg_385" n="HIAT:ip">,</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_388" n="HIAT:w" s="T103">aːmnɨt</ts>
                  <nts id="Seg_389" n="HIAT:ip">.</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_392" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_394" n="HIAT:w" s="T104">pajan</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_397" n="HIAT:w" s="T105">tüːmbədi</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_400" n="HIAT:w" s="T106">wättowɨn</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_403" n="HIAT:w" s="T107">qwärɣə</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_406" n="HIAT:w" s="T108">čaːǯɨs</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_409" n="HIAT:w" s="T109">pajan</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_412" n="HIAT:w" s="T110">maːttə</ts>
                  <nts id="Seg_413" n="HIAT:ip">.</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_416" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_418" n="HIAT:w" s="T111">maːtqɨn</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_421" n="HIAT:w" s="T112">quttɨna</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_424" n="HIAT:w" s="T113">tʼäŋus</ts>
                  <nts id="Seg_425" n="HIAT:ip">.</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_428" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_430" n="HIAT:w" s="T114">maːttam</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_433" n="HIAT:w" s="T115">nʼüit</ts>
                  <nts id="Seg_434" n="HIAT:ip">,</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_437" n="HIAT:w" s="T116">sernä</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_440" n="HIAT:w" s="T117">maːttə</ts>
                  <nts id="Seg_441" n="HIAT:ip">.</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_444" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_446" n="HIAT:w" s="T118">maːdə</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_449" n="HIAT:w" s="T119">sundʼeqɨn</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_452" n="HIAT:w" s="T120">palʼdʼulʼewlʼe</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_455" n="HIAT:w" s="T121">lʼewɨm</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_458" n="HIAT:w" s="T122">innä</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_461" n="HIAT:w" s="T123">wätʼtʼit</ts>
                  <nts id="Seg_462" n="HIAT:ip">,</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_465" n="HIAT:w" s="T124">pannɨ</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_468" n="HIAT:w" s="T125">potpolǯə</ts>
                  <nts id="Seg_469" n="HIAT:ip">.</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_472" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_474" n="HIAT:w" s="T126">sɨːri</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_477" n="HIAT:w" s="T127">maːtqɨn</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_480" n="HIAT:w" s="T128">nɨŋgɨzattə</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_483" n="HIAT:w" s="T129">sɨr</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_486" n="HIAT:w" s="T130">tʼölʼaze</ts>
                  <nts id="Seg_487" n="HIAT:ip">.</nts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_490" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_492" n="HIAT:w" s="T131">täpqim</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_495" n="HIAT:w" s="T132">qwärɣə</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_498" n="HIAT:w" s="T133">assə</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_501" n="HIAT:w" s="T134">laqqərɨzɨt</ts>
                  <nts id="Seg_502" n="HIAT:ip">.</nts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_505" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_507" n="HIAT:w" s="T135">potpolqən</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_510" n="HIAT:w" s="T136">qwärɣə</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_513" n="HIAT:w" s="T137">hozʼain</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_516" n="HIAT:w" s="T138">äːsan</ts>
                  <nts id="Seg_517" n="HIAT:ip">.</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_520" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_522" n="HIAT:w" s="T139">niːn</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_525" n="HIAT:w" s="T140">äːssan</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_528" n="HIAT:w" s="T141">saːq</ts>
                  <nts id="Seg_529" n="HIAT:ip">,</nts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_532" n="HIAT:w" s="T142">qwälɨn</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_535" n="HIAT:w" s="T143">üːr</ts>
                  <nts id="Seg_536" n="HIAT:ip">,</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_539" n="HIAT:w" s="T144">saqətpədi</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_542" n="HIAT:w" s="T145">wätʼtʼe</ts>
                  <nts id="Seg_543" n="HIAT:ip">,</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_546" n="HIAT:w" s="T146">mʼödə</ts>
                  <nts id="Seg_547" n="HIAT:ip">.</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_550" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_552" n="HIAT:w" s="T147">saːqɨm</ts>
                  <nts id="Seg_553" n="HIAT:ip">,</nts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_556" n="HIAT:w" s="T148">saqətpədi</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_559" n="HIAT:w" s="T149">wädʼim</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_562" n="HIAT:w" s="T150">täp</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_565" n="HIAT:w" s="T151">qamǯimbat</ts>
                  <nts id="Seg_566" n="HIAT:ip">,</nts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_569" n="HIAT:w" s="T152">qäːlqɨlbat</ts>
                  <nts id="Seg_570" n="HIAT:ip">.</nts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_573" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_575" n="HIAT:w" s="T153">a</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_578" n="HIAT:w" s="T154">qwälɨn</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_581" n="HIAT:w" s="T155">üːrɨm</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_584" n="HIAT:w" s="T156">i</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_587" n="HIAT:w" s="T157">mʼödəm</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_590" n="HIAT:w" s="T158">qwärɣə</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_593" n="HIAT:w" s="T159">ütpat</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_595" n="HIAT:ip">(</nts>
                  <ts e="T161" id="Seg_597" n="HIAT:w" s="T160">aːmbat</ts>
                  <nts id="Seg_598" n="HIAT:ip">)</nts>
                  <nts id="Seg_599" n="HIAT:ip">.</nts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_602" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_604" n="HIAT:w" s="T161">surulʼdʼi</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_607" n="HIAT:w" s="T162">qula</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_610" n="HIAT:w" s="T163">tüːwattə</ts>
                  <nts id="Seg_611" n="HIAT:ip">.</nts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_614" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_616" n="HIAT:w" s="T164">maːdɨm</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_619" n="HIAT:w" s="T165">assə</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_622" n="HIAT:w" s="T166">qosolǯiqwattə</ts>
                  <nts id="Seg_623" n="HIAT:ip">.</nts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_626" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_628" n="HIAT:w" s="T167">qaiŋgo</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_631" n="HIAT:w" s="T168">daqqa</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_634" n="HIAT:w" s="T169">lärumbɨsɨn</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_637" n="HIAT:w" s="T170">äːattə</ts>
                  <nts id="Seg_638" n="HIAT:ip">.</nts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_641" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_643" n="HIAT:w" s="T171">oqqɨr</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_646" n="HIAT:w" s="T172">surulʼdʼi</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_649" n="HIAT:w" s="T173">qum</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_652" n="HIAT:w" s="T174">maːttə</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_655" n="HIAT:w" s="T175">särna</ts>
                  <nts id="Seg_656" n="HIAT:ip">.</nts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_659" n="HIAT:u" s="T176">
                  <ts e="T177" id="Seg_661" n="HIAT:w" s="T176">maːdə</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_664" n="HIAT:w" s="T177">sündʼeqɨn</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_667" n="HIAT:w" s="T178">püruŋ</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_670" n="HIAT:w" s="T179">palʼdʼölʼeːan</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_673" n="HIAT:w" s="T180">qujqan</ts>
                  <nts id="Seg_674" n="HIAT:ip">,</nts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_677" n="HIAT:w" s="T181">täːrbɨlʼe</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_680" n="HIAT:w" s="T182">nɨngeːen</ts>
                  <nts id="Seg_681" n="HIAT:ip">.</nts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_684" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_686" n="HIAT:w" s="T183">qajdaqa</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_689" n="HIAT:w" s="T184">potpolqən</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_692" n="HIAT:w" s="T185">rüqonʼnʼe</ts>
                  <nts id="Seg_693" n="HIAT:ip">.</nts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_696" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_698" n="HIAT:w" s="T186">surulʼdʼi</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_701" n="HIAT:w" s="T187">qum</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_704" n="HIAT:w" s="T188">ündɨzäɨt</ts>
                  <nts id="Seg_705" n="HIAT:ip">,</nts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_708" n="HIAT:w" s="T189">qoːsolǯit</ts>
                  <nts id="Seg_709" n="HIAT:ip">:</nts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_712" n="HIAT:w" s="T190">potpolqɨn</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_715" n="HIAT:w" s="T191">eiŋ</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_718" n="HIAT:w" s="T192">qwärɣa</ts>
                  <nts id="Seg_719" n="HIAT:ip">.</nts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_722" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_724" n="HIAT:w" s="T193">ponä</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_727" n="HIAT:w" s="T194">čanǯiŋ</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_730" n="HIAT:w" s="T195">lʼäqalɨqɨndə</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_733" n="HIAT:w" s="T196">tʼärɨn</ts>
                  <nts id="Seg_734" n="HIAT:ip">:</nts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_737" n="HIAT:w" s="T197">potpolqɨn</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_740" n="HIAT:w" s="T198">ippa</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_743" n="HIAT:w" s="T199">qwärɣə</ts>
                  <nts id="Seg_744" n="HIAT:ip">.</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_747" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_749" n="HIAT:w" s="T200">qwärqelʼdi</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_752" n="HIAT:w" s="T201">qula</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_755" n="HIAT:w" s="T202">sernattə</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_758" n="HIAT:w" s="T203">maːttə</ts>
                  <nts id="Seg_759" n="HIAT:ip">,</nts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_762" n="HIAT:w" s="T204">qättattə</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_765" n="HIAT:w" s="T205">potpɨlɨm</ts>
                  <nts id="Seg_766" n="HIAT:ip">.</nts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_769" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_771" n="HIAT:w" s="T206">ponä</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_774" n="HIAT:w" s="T207">čanǯattə</ts>
                  <nts id="Seg_775" n="HIAT:ip">,</nts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_778" n="HIAT:w" s="T208">maːtamdə</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_781" n="HIAT:w" s="T209">qättattə</ts>
                  <nts id="Seg_782" n="HIAT:ip">.</nts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_785" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_787" n="HIAT:w" s="T210">oqqɨ</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_790" n="HIAT:w" s="T211">lʼewɨm</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_793" n="HIAT:w" s="T212">qaqɨnnattə</ts>
                  <nts id="Seg_794" n="HIAT:ip">,</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_797" n="HIAT:w" s="T213">qwärqɨm</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_800" n="HIAT:w" s="T214">potpolqɨn</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_803" n="HIAT:w" s="T215">qolʼdʼättə</ts>
                  <nts id="Seg_804" n="HIAT:ip">.</nts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_807" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_809" n="HIAT:w" s="T216">surulʼdʼi</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_812" n="HIAT:w" s="T217">qula</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_815" n="HIAT:w" s="T218">assə</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_818" n="HIAT:w" s="T219">naːrɨŋ</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_821" n="HIAT:w" s="T220">tättɨŋ</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_824" n="HIAT:w" s="T221">tʼätčattə</ts>
                  <nts id="Seg_825" n="HIAT:ip">.</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_828" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_830" n="HIAT:w" s="T222">täp</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_833" n="HIAT:w" s="T223">i</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_836" n="HIAT:w" s="T224">qaːrɨssä</ts>
                  <nts id="Seg_837" n="HIAT:ip">.</nts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_840" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_842" n="HIAT:w" s="T225">nännä</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_845" n="HIAT:w" s="T226">suːrum</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_848" n="HIAT:w" s="T227">qujqalǯiŋ</ts>
                  <nts id="Seg_849" n="HIAT:ip">.</nts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_852" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_854" n="HIAT:w" s="T228">šɨttə</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_857" n="HIAT:w" s="T229">lʼewɨm</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_860" n="HIAT:w" s="T230">qaqənnattə</ts>
                  <nts id="Seg_861" n="HIAT:ip">,</nts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_864" n="HIAT:w" s="T231">manǯimbatt</ts>
                  <nts id="Seg_865" n="HIAT:ip">,</nts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_868" n="HIAT:w" s="T232">qwärɣə</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_871" n="HIAT:w" s="T233">qumbaː</ts>
                  <nts id="Seg_872" n="HIAT:ip">.</nts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_875" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_877" n="HIAT:w" s="T234">täbɨm</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_880" n="HIAT:w" s="T235">innä</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_883" n="HIAT:w" s="T236">sabɨnʼättə</ts>
                  <nts id="Seg_884" n="HIAT:ip">.</nts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_887" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_889" n="HIAT:w" s="T237">qobɨmdə</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_892" n="HIAT:w" s="T238">qɨrattə</ts>
                  <nts id="Seg_893" n="HIAT:ip">,</nts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_896" n="HIAT:w" s="T239">wädʼimdə</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_899" n="HIAT:w" s="T240">toː</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_902" n="HIAT:w" s="T241">tʼätčattə</ts>
                  <nts id="Seg_903" n="HIAT:ip">.</nts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_906" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_908" n="HIAT:w" s="T242">wädʼit</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_911" n="HIAT:w" s="T243">äːsan</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_914" n="HIAT:w" s="T244">säqə</ts>
                  <nts id="Seg_915" n="HIAT:ip">,</nts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_918" n="HIAT:w" s="T245">assə</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_921" n="HIAT:w" s="T246">qappərɨmbädi</ts>
                  <nts id="Seg_922" n="HIAT:ip">.</nts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_925" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_927" n="HIAT:w" s="T247">nännä</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_930" n="HIAT:w" s="T248">qwälewle</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_933" n="HIAT:w" s="T249">surulʼdi</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_936" n="HIAT:w" s="T250">quːla</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_939" n="HIAT:w" s="T251">äwɨmdä</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_942" n="HIAT:w" s="T252">qombɨzattə</ts>
                  <nts id="Seg_943" n="HIAT:ip">,</nts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_946" n="HIAT:w" s="T253">nʼärnän</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_949" n="HIAT:w" s="T254">nämdə</ts>
                  <nts id="Seg_950" n="HIAT:ip">,</nts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_953" n="HIAT:w" s="T255">na</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_956" n="HIAT:w" s="T256">qwätpɨdi</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_959" n="HIAT:w" s="T257">qwärɣə</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_962" n="HIAT:w" s="T258">ambat</ts>
                  <nts id="Seg_963" n="HIAT:ip">,</nts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_966" n="HIAT:w" s="T259">tʼeːlɨn</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_969" n="HIAT:w" s="T260">suːrulʼdʼeqɨn</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_972" n="HIAT:w" s="T261">äːwɨmdə</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_975" n="HIAT:w" s="T262">nändɨzä</ts>
                  <nts id="Seg_976" n="HIAT:ip">.</nts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_979" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_981" n="HIAT:w" s="T263">iːdə</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_984" n="HIAT:w" s="T264">ilʼlʼɨqqus</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_987" n="HIAT:w" s="T265">piːrʼäqən</ts>
                  <nts id="Seg_988" n="HIAT:ip">.</nts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_991" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_993" n="HIAT:w" s="T266">täp</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_996" n="HIAT:w" s="T267">qwässɨ</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_999" n="HIAT:w" s="T268">qrandə</ts>
                  <nts id="Seg_1000" n="HIAT:ip">,</nts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1003" n="HIAT:w" s="T269">qajqɨn</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1006" n="HIAT:w" s="T270">ilɨs</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1009" n="HIAT:w" s="T271">äwɨt</ts>
                  <nts id="Seg_1010" n="HIAT:ip">,</nts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1013" n="HIAT:w" s="T272">täbɨn</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1016" n="HIAT:w" s="T273">obɨt</ts>
                  <nts id="Seg_1017" n="HIAT:ip">.</nts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1020" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_1022" n="HIAT:w" s="T274">täp</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1025" n="HIAT:w" s="T275">nilʼdʼin</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1028" n="HIAT:w" s="T276">tʼärɨs</ts>
                  <nts id="Seg_1029" n="HIAT:ip">:</nts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1032" n="HIAT:w" s="T277">man</ts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1035" n="HIAT:w" s="T278">bə</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1038" n="HIAT:w" s="T279">assə</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1041" n="HIAT:w" s="T280">qwätnäw</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1044" n="HIAT:w" s="T281">taw</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1047" n="HIAT:w" s="T282">qwärɣɨm</ts>
                  <nts id="Seg_1048" n="HIAT:ip">,</nts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1051" n="HIAT:w" s="T283">a</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1054" n="HIAT:w" s="T284">tʼädɨnäw</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1057" n="HIAT:w" s="T285">äwennɨ</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1060" n="HIAT:w" s="T286">maːdəm</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1063" n="HIAT:w" s="T287">oqqɨmɨqɨn</ts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1066" n="HIAT:w" s="T288">qwärɣəzä</ts>
                  <nts id="Seg_1067" n="HIAT:ip">.</nts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_1070" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_1072" n="HIAT:w" s="T289">man</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1075" n="HIAT:w" s="T290">nʼärnä</ts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1078" n="HIAT:w" s="T291">qwärɣɨlam</ts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1081" n="HIAT:w" s="T292">qwätqweːnǯaw</ts>
                  <nts id="Seg_1082" n="HIAT:ip">,</nts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1085" n="HIAT:w" s="T293">tʼätǯiqwenǯaw</ts>
                  <nts id="Seg_1086" n="HIAT:ip">.</nts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T294" id="Seg_1088" n="sc" s="T0">
               <ts e="T1" id="Seg_1090" n="e" s="T0">äːsan </ts>
               <ts e="T2" id="Seg_1092" n="e" s="T1">Мarkovoqon. </ts>
               <ts e="T3" id="Seg_1094" n="e" s="T2">nɨtʼan </ts>
               <ts e="T4" id="Seg_1096" n="e" s="T3">matʼtʼöqɨn </ts>
               <ts e="T5" id="Seg_1098" n="e" s="T4">äːsan </ts>
               <ts e="T6" id="Seg_1100" n="e" s="T5">oqqɨ </ts>
               <ts e="T7" id="Seg_1102" n="e" s="T6">äːda. </ts>
               <ts e="T8" id="Seg_1104" n="e" s="T7">nimdə </ts>
               <ts e="T9" id="Seg_1106" n="e" s="T8">täbɨn </ts>
               <ts e="T10" id="Seg_1108" n="e" s="T9">Мarqovo. </ts>
               <ts e="T11" id="Seg_1110" n="e" s="T10">nɨtʼän </ts>
               <ts e="T12" id="Seg_1112" n="e" s="T11">illɨqus </ts>
               <ts e="T13" id="Seg_1114" n="e" s="T12">paja </ts>
               <ts e="T14" id="Seg_1116" n="e" s="T13">nändɨzä. </ts>
               <ts e="T15" id="Seg_1118" n="e" s="T14">irat </ts>
               <ts e="T16" id="Seg_1120" n="e" s="T15">täbɨnan </ts>
               <ts e="T17" id="Seg_1122" n="e" s="T16">čäŋus. </ts>
               <ts e="T18" id="Seg_1124" n="e" s="T17">illɨqqus </ts>
               <ts e="T19" id="Seg_1126" n="e" s="T18">ondä. </ts>
               <ts e="T20" id="Seg_1128" n="e" s="T19">nʼäjajgus, </ts>
               <ts e="T21" id="Seg_1130" n="e" s="T20">čoppɨrɨjgus, </ts>
               <ts e="T22" id="Seg_1132" n="e" s="T21">sväŋqɨjgus. </ts>
               <ts e="T23" id="Seg_1134" n="e" s="T22">oqqɨrɨŋ </ts>
               <ts e="T24" id="Seg_1136" n="e" s="T23">äran </ts>
               <ts e="T25" id="Seg_1138" n="e" s="T24">nädə </ts>
               <ts e="T26" id="Seg_1140" n="e" s="T25">qwannä </ts>
               <ts e="T27" id="Seg_1142" n="e" s="T26">peːmɨgu. </ts>
               <ts e="T28" id="Seg_1144" n="e" s="T27">äːwɨt </ts>
               <ts e="T29" id="Seg_1146" n="e" s="T28">äːqus </ts>
               <ts e="T30" id="Seg_1148" n="e" s="T29">maːtqɨn. </ts>
               <ts e="T31" id="Seg_1150" n="e" s="T30">qaroqənnä </ts>
               <ts e="T32" id="Seg_1152" n="e" s="T31">to </ts>
               <ts e="T33" id="Seg_1154" n="e" s="T32">ütättä </ts>
               <ts e="T34" id="Seg_1156" n="e" s="T33">täp </ts>
               <ts e="T35" id="Seg_1158" n="e" s="T34">palʼdʼüs </ts>
               <ts e="T36" id="Seg_1160" n="e" s="T35">nʼäjalʼlʼe. </ts>
               <ts e="T37" id="Seg_1162" n="e" s="T36">tʼeːlɨn </ts>
               <ts e="T38" id="Seg_1164" n="e" s="T37">palʼdʼulʼewlʼe </ts>
               <ts e="T39" id="Seg_1166" n="e" s="T38">üːdomɨqän </ts>
               <ts e="T40" id="Seg_1168" n="e" s="T39">laqqonä </ts>
               <ts e="T41" id="Seg_1170" n="e" s="T40">maːtqɨndə. </ts>
               <ts e="T42" id="Seg_1172" n="e" s="T41">tüːmbɨŋ </ts>
               <ts e="T43" id="Seg_1174" n="e" s="T42">alʼi </ts>
               <ts e="T44" id="Seg_1176" n="e" s="T43">moqauqaŋ </ts>
               <ts e="T45" id="Seg_1178" n="e" s="T44">čaːǯimba </ts>
               <ts e="T46" id="Seg_1180" n="e" s="T45">qottä </ts>
               <ts e="T47" id="Seg_1182" n="e" s="T46">maːtqɨndə. </ts>
               <ts e="T48" id="Seg_1184" n="e" s="T47">tʼelʼeqɨndə </ts>
               <ts e="T49" id="Seg_1186" n="e" s="T48">täbɨm </ts>
               <ts e="T50" id="Seg_1188" n="e" s="T49">qɨːrɨmbat </ts>
               <ts e="T51" id="Seg_1190" n="e" s="T50">qwärqä </ts>
               <ts e="T52" id="Seg_1192" n="e" s="T51">näjqun </ts>
               <ts e="T53" id="Seg_1194" n="e" s="T52">wättoun. </ts>
               <ts e="T54" id="Seg_1196" n="e" s="T53">midəmbat </ts>
               <ts e="T55" id="Seg_1198" n="e" s="T54">üːdəmɨqɨn </ts>
               <ts e="T56" id="Seg_1200" n="e" s="T55">surulʼdi </ts>
               <ts e="T57" id="Seg_1202" n="e" s="T56">pajam. </ts>
               <ts e="T58" id="Seg_1204" n="e" s="T57">oralbat, </ts>
               <ts e="T59" id="Seg_1206" n="e" s="T58">qwätpat, </ts>
               <ts e="T60" id="Seg_1208" n="e" s="T59">aːmbat </ts>
               <ts e="T61" id="Seg_1210" n="e" s="T60">täbɨm. </ts>
               <ts e="T62" id="Seg_1212" n="e" s="T61">qajmɨlamdə </ts>
               <ts e="T63" id="Seg_1214" n="e" s="T62">nɨšqɨlgɨlʼbat, </ts>
               <ts e="T64" id="Seg_1216" n="e" s="T63">tülʼəsämdə </ts>
               <ts e="T65" id="Seg_1218" n="e" s="T64">toː </ts>
               <ts e="T66" id="Seg_1220" n="e" s="T65">tʼätǯimbat. </ts>
               <ts e="T67" id="Seg_1222" n="e" s="T66">qalɨmba </ts>
               <ts e="T68" id="Seg_1224" n="e" s="T67">oqqə </ts>
               <ts e="T69" id="Seg_1226" n="e" s="T68">toːbɨt </ts>
               <ts e="T70" id="Seg_1228" n="e" s="T69">taw </ts>
               <ts e="T71" id="Seg_1230" n="e" s="T70">nädänanna. </ts>
               <ts e="T72" id="Seg_1232" n="e" s="T71">üːdəmɨn </ts>
               <ts e="T73" id="Seg_1234" n="e" s="T72">äwät </ts>
               <ts e="T74" id="Seg_1236" n="e" s="T73">ädəlǯis. </ts>
               <ts e="T75" id="Seg_1238" n="e" s="T74">nädə </ts>
               <ts e="T76" id="Seg_1240" n="e" s="T75">assə </ts>
               <ts e="T77" id="Seg_1242" n="e" s="T76">tümba. </ts>
               <ts e="T78" id="Seg_1244" n="e" s="T77">wes </ts>
               <ts e="T79" id="Seg_1246" n="e" s="T78">pin </ts>
               <ts e="T80" id="Seg_1248" n="e" s="T79">äwɨt </ts>
               <ts e="T81" id="Seg_1250" n="e" s="T80">nändɨgo </ts>
               <ts e="T82" id="Seg_1252" n="e" s="T81">täːrbɨlʼe </ts>
               <ts e="T83" id="Seg_1254" n="e" s="T82">assə </ts>
               <ts e="T84" id="Seg_1256" n="e" s="T83">qondɨs. </ts>
               <ts e="T85" id="Seg_1258" n="e" s="T84">taːr </ts>
               <ts e="T86" id="Seg_1260" n="e" s="T85">assə </ts>
               <ts e="T87" id="Seg_1262" n="e" s="T86">tʼeːlɨmbɨs </ts>
               <ts e="T88" id="Seg_1264" n="e" s="T87">äːwɨt </ts>
               <ts e="T89" id="Seg_1266" n="e" s="T88">qwannɨ </ts>
               <ts e="T90" id="Seg_1268" n="e" s="T89">näːmdə </ts>
               <ts e="T91" id="Seg_1270" n="e" s="T90">pergu. </ts>
               <ts e="T92" id="Seg_1272" n="e" s="T91">telʼdʼelʼi </ts>
               <ts e="T93" id="Seg_1274" n="e" s="T92">qwänbədi </ts>
               <ts e="T94" id="Seg_1276" n="e" s="T93">wättoːwɨn </ts>
               <ts e="T95" id="Seg_1278" n="e" s="T94">äːwɨt </ts>
               <ts e="T96" id="Seg_1280" n="e" s="T95">čaːǯiqus </ts>
               <ts e="T97" id="Seg_1282" n="e" s="T96">matʼtʼöqɨn. </ts>
               <ts e="T98" id="Seg_1284" n="e" s="T97">qunnɨ </ts>
               <ts e="T99" id="Seg_1286" n="e" s="T98">da </ts>
               <ts e="T100" id="Seg_1288" n="e" s="T99">sappɨziŋ </ts>
               <ts e="T101" id="Seg_1290" n="e" s="T100">qwäːrɣə. </ts>
               <ts e="T102" id="Seg_1292" n="e" s="T101">orannɨt </ts>
               <ts e="T103" id="Seg_1294" n="e" s="T102">pajam, </ts>
               <ts e="T104" id="Seg_1296" n="e" s="T103">aːmnɨt. </ts>
               <ts e="T105" id="Seg_1298" n="e" s="T104">pajan </ts>
               <ts e="T106" id="Seg_1300" n="e" s="T105">tüːmbədi </ts>
               <ts e="T107" id="Seg_1302" n="e" s="T106">wättowɨn </ts>
               <ts e="T108" id="Seg_1304" n="e" s="T107">qwärɣə </ts>
               <ts e="T109" id="Seg_1306" n="e" s="T108">čaːǯɨs </ts>
               <ts e="T110" id="Seg_1308" n="e" s="T109">pajan </ts>
               <ts e="T111" id="Seg_1310" n="e" s="T110">maːttə. </ts>
               <ts e="T112" id="Seg_1312" n="e" s="T111">maːtqɨn </ts>
               <ts e="T113" id="Seg_1314" n="e" s="T112">quttɨna </ts>
               <ts e="T114" id="Seg_1316" n="e" s="T113">tʼäŋus. </ts>
               <ts e="T115" id="Seg_1318" n="e" s="T114">maːttam </ts>
               <ts e="T116" id="Seg_1320" n="e" s="T115">nʼüit, </ts>
               <ts e="T117" id="Seg_1322" n="e" s="T116">sernä </ts>
               <ts e="T118" id="Seg_1324" n="e" s="T117">maːttə. </ts>
               <ts e="T119" id="Seg_1326" n="e" s="T118">maːdə </ts>
               <ts e="T120" id="Seg_1328" n="e" s="T119">sundʼeqɨn </ts>
               <ts e="T121" id="Seg_1330" n="e" s="T120">palʼdʼulʼewlʼe </ts>
               <ts e="T122" id="Seg_1332" n="e" s="T121">lʼewɨm </ts>
               <ts e="T123" id="Seg_1334" n="e" s="T122">innä </ts>
               <ts e="T124" id="Seg_1336" n="e" s="T123">wätʼtʼit, </ts>
               <ts e="T125" id="Seg_1338" n="e" s="T124">pannɨ </ts>
               <ts e="T126" id="Seg_1340" n="e" s="T125">potpolǯə. </ts>
               <ts e="T127" id="Seg_1342" n="e" s="T126">sɨːri </ts>
               <ts e="T128" id="Seg_1344" n="e" s="T127">maːtqɨn </ts>
               <ts e="T129" id="Seg_1346" n="e" s="T128">nɨŋgɨzattə </ts>
               <ts e="T130" id="Seg_1348" n="e" s="T129">sɨr </ts>
               <ts e="T131" id="Seg_1350" n="e" s="T130">tʼölʼaze. </ts>
               <ts e="T132" id="Seg_1352" n="e" s="T131">täpqim </ts>
               <ts e="T133" id="Seg_1354" n="e" s="T132">qwärɣə </ts>
               <ts e="T134" id="Seg_1356" n="e" s="T133">assə </ts>
               <ts e="T135" id="Seg_1358" n="e" s="T134">laqqərɨzɨt. </ts>
               <ts e="T136" id="Seg_1360" n="e" s="T135">potpolqən </ts>
               <ts e="T137" id="Seg_1362" n="e" s="T136">qwärɣə </ts>
               <ts e="T138" id="Seg_1364" n="e" s="T137">hozʼain </ts>
               <ts e="T139" id="Seg_1366" n="e" s="T138">äːsan. </ts>
               <ts e="T140" id="Seg_1368" n="e" s="T139">niːn </ts>
               <ts e="T141" id="Seg_1370" n="e" s="T140">äːssan </ts>
               <ts e="T142" id="Seg_1372" n="e" s="T141">saːq, </ts>
               <ts e="T143" id="Seg_1374" n="e" s="T142">qwälɨn </ts>
               <ts e="T144" id="Seg_1376" n="e" s="T143">üːr, </ts>
               <ts e="T145" id="Seg_1378" n="e" s="T144">saqətpədi </ts>
               <ts e="T146" id="Seg_1380" n="e" s="T145">wätʼtʼe, </ts>
               <ts e="T147" id="Seg_1382" n="e" s="T146">mʼödə. </ts>
               <ts e="T148" id="Seg_1384" n="e" s="T147">saːqɨm, </ts>
               <ts e="T149" id="Seg_1386" n="e" s="T148">saqətpədi </ts>
               <ts e="T150" id="Seg_1388" n="e" s="T149">wädʼim </ts>
               <ts e="T151" id="Seg_1390" n="e" s="T150">täp </ts>
               <ts e="T152" id="Seg_1392" n="e" s="T151">qamǯimbat, </ts>
               <ts e="T153" id="Seg_1394" n="e" s="T152">qäːlqɨlbat. </ts>
               <ts e="T154" id="Seg_1396" n="e" s="T153">a </ts>
               <ts e="T155" id="Seg_1398" n="e" s="T154">qwälɨn </ts>
               <ts e="T156" id="Seg_1400" n="e" s="T155">üːrɨm </ts>
               <ts e="T157" id="Seg_1402" n="e" s="T156">i </ts>
               <ts e="T158" id="Seg_1404" n="e" s="T157">mʼödəm </ts>
               <ts e="T159" id="Seg_1406" n="e" s="T158">qwärɣə </ts>
               <ts e="T160" id="Seg_1408" n="e" s="T159">ütpat </ts>
               <ts e="T161" id="Seg_1410" n="e" s="T160">(aːmbat). </ts>
               <ts e="T162" id="Seg_1412" n="e" s="T161">surulʼdʼi </ts>
               <ts e="T163" id="Seg_1414" n="e" s="T162">qula </ts>
               <ts e="T164" id="Seg_1416" n="e" s="T163">tüːwattə. </ts>
               <ts e="T165" id="Seg_1418" n="e" s="T164">maːdɨm </ts>
               <ts e="T166" id="Seg_1420" n="e" s="T165">assə </ts>
               <ts e="T167" id="Seg_1422" n="e" s="T166">qosolǯiqwattə. </ts>
               <ts e="T168" id="Seg_1424" n="e" s="T167">qaiŋgo </ts>
               <ts e="T169" id="Seg_1426" n="e" s="T168">daqqa </ts>
               <ts e="T170" id="Seg_1428" n="e" s="T169">lärumbɨsɨn </ts>
               <ts e="T171" id="Seg_1430" n="e" s="T170">äːattə. </ts>
               <ts e="T172" id="Seg_1432" n="e" s="T171">oqqɨr </ts>
               <ts e="T173" id="Seg_1434" n="e" s="T172">surulʼdʼi </ts>
               <ts e="T174" id="Seg_1436" n="e" s="T173">qum </ts>
               <ts e="T175" id="Seg_1438" n="e" s="T174">maːttə </ts>
               <ts e="T176" id="Seg_1440" n="e" s="T175">särna. </ts>
               <ts e="T177" id="Seg_1442" n="e" s="T176">maːdə </ts>
               <ts e="T178" id="Seg_1444" n="e" s="T177">sündʼeqɨn </ts>
               <ts e="T179" id="Seg_1446" n="e" s="T178">püruŋ </ts>
               <ts e="T180" id="Seg_1448" n="e" s="T179">palʼdʼölʼeːan </ts>
               <ts e="T181" id="Seg_1450" n="e" s="T180">qujqan, </ts>
               <ts e="T182" id="Seg_1452" n="e" s="T181">täːrbɨlʼe </ts>
               <ts e="T183" id="Seg_1454" n="e" s="T182">nɨngeːen. </ts>
               <ts e="T184" id="Seg_1456" n="e" s="T183">qajdaqa </ts>
               <ts e="T185" id="Seg_1458" n="e" s="T184">potpolqən </ts>
               <ts e="T186" id="Seg_1460" n="e" s="T185">rüqonʼnʼe. </ts>
               <ts e="T187" id="Seg_1462" n="e" s="T186">surulʼdʼi </ts>
               <ts e="T188" id="Seg_1464" n="e" s="T187">qum </ts>
               <ts e="T189" id="Seg_1466" n="e" s="T188">ündɨzäɨt, </ts>
               <ts e="T190" id="Seg_1468" n="e" s="T189">qoːsolǯit: </ts>
               <ts e="T191" id="Seg_1470" n="e" s="T190">potpolqɨn </ts>
               <ts e="T192" id="Seg_1472" n="e" s="T191">eiŋ </ts>
               <ts e="T193" id="Seg_1474" n="e" s="T192">qwärɣa. </ts>
               <ts e="T194" id="Seg_1476" n="e" s="T193">ponä </ts>
               <ts e="T195" id="Seg_1478" n="e" s="T194">čanǯiŋ </ts>
               <ts e="T196" id="Seg_1480" n="e" s="T195">lʼäqalɨqɨndə </ts>
               <ts e="T197" id="Seg_1482" n="e" s="T196">tʼärɨn: </ts>
               <ts e="T198" id="Seg_1484" n="e" s="T197">potpolqɨn </ts>
               <ts e="T199" id="Seg_1486" n="e" s="T198">ippa </ts>
               <ts e="T200" id="Seg_1488" n="e" s="T199">qwärɣə. </ts>
               <ts e="T201" id="Seg_1490" n="e" s="T200">qwärqelʼdi </ts>
               <ts e="T202" id="Seg_1492" n="e" s="T201">qula </ts>
               <ts e="T203" id="Seg_1494" n="e" s="T202">sernattə </ts>
               <ts e="T204" id="Seg_1496" n="e" s="T203">maːttə, </ts>
               <ts e="T205" id="Seg_1498" n="e" s="T204">qättattə </ts>
               <ts e="T206" id="Seg_1500" n="e" s="T205">potpɨlɨm. </ts>
               <ts e="T207" id="Seg_1502" n="e" s="T206">ponä </ts>
               <ts e="T208" id="Seg_1504" n="e" s="T207">čanǯattə, </ts>
               <ts e="T209" id="Seg_1506" n="e" s="T208">maːtamdə </ts>
               <ts e="T210" id="Seg_1508" n="e" s="T209">qättattə. </ts>
               <ts e="T211" id="Seg_1510" n="e" s="T210">oqqɨ </ts>
               <ts e="T212" id="Seg_1512" n="e" s="T211">lʼewɨm </ts>
               <ts e="T213" id="Seg_1514" n="e" s="T212">qaqɨnnattə, </ts>
               <ts e="T214" id="Seg_1516" n="e" s="T213">qwärqɨm </ts>
               <ts e="T215" id="Seg_1518" n="e" s="T214">potpolqɨn </ts>
               <ts e="T216" id="Seg_1520" n="e" s="T215">qolʼdʼättə. </ts>
               <ts e="T217" id="Seg_1522" n="e" s="T216">surulʼdʼi </ts>
               <ts e="T218" id="Seg_1524" n="e" s="T217">qula </ts>
               <ts e="T219" id="Seg_1526" n="e" s="T218">assə </ts>
               <ts e="T220" id="Seg_1528" n="e" s="T219">naːrɨŋ </ts>
               <ts e="T221" id="Seg_1530" n="e" s="T220">tättɨŋ </ts>
               <ts e="T222" id="Seg_1532" n="e" s="T221">tʼätčattə. </ts>
               <ts e="T223" id="Seg_1534" n="e" s="T222">täp </ts>
               <ts e="T224" id="Seg_1536" n="e" s="T223">i </ts>
               <ts e="T225" id="Seg_1538" n="e" s="T224">qaːrɨssä. </ts>
               <ts e="T226" id="Seg_1540" n="e" s="T225">nännä </ts>
               <ts e="T227" id="Seg_1542" n="e" s="T226">suːrum </ts>
               <ts e="T228" id="Seg_1544" n="e" s="T227">qujqalǯiŋ. </ts>
               <ts e="T229" id="Seg_1546" n="e" s="T228">šɨttə </ts>
               <ts e="T230" id="Seg_1548" n="e" s="T229">lʼewɨm </ts>
               <ts e="T231" id="Seg_1550" n="e" s="T230">qaqənnattə, </ts>
               <ts e="T232" id="Seg_1552" n="e" s="T231">manǯimbatt, </ts>
               <ts e="T233" id="Seg_1554" n="e" s="T232">qwärɣə </ts>
               <ts e="T234" id="Seg_1556" n="e" s="T233">qumbaː. </ts>
               <ts e="T235" id="Seg_1558" n="e" s="T234">täbɨm </ts>
               <ts e="T236" id="Seg_1560" n="e" s="T235">innä </ts>
               <ts e="T237" id="Seg_1562" n="e" s="T236">sabɨnʼättə. </ts>
               <ts e="T238" id="Seg_1564" n="e" s="T237">qobɨmdə </ts>
               <ts e="T239" id="Seg_1566" n="e" s="T238">qɨrattə, </ts>
               <ts e="T240" id="Seg_1568" n="e" s="T239">wädʼimdə </ts>
               <ts e="T241" id="Seg_1570" n="e" s="T240">toː </ts>
               <ts e="T242" id="Seg_1572" n="e" s="T241">tʼätčattə. </ts>
               <ts e="T243" id="Seg_1574" n="e" s="T242">wädʼit </ts>
               <ts e="T244" id="Seg_1576" n="e" s="T243">äːsan </ts>
               <ts e="T245" id="Seg_1578" n="e" s="T244">säqə, </ts>
               <ts e="T246" id="Seg_1580" n="e" s="T245">assə </ts>
               <ts e="T247" id="Seg_1582" n="e" s="T246">qappərɨmbädi. </ts>
               <ts e="T248" id="Seg_1584" n="e" s="T247">nännä </ts>
               <ts e="T249" id="Seg_1586" n="e" s="T248">qwälewle </ts>
               <ts e="T250" id="Seg_1588" n="e" s="T249">surulʼdi </ts>
               <ts e="T251" id="Seg_1590" n="e" s="T250">quːla </ts>
               <ts e="T252" id="Seg_1592" n="e" s="T251">äwɨmdä </ts>
               <ts e="T253" id="Seg_1594" n="e" s="T252">qombɨzattə, </ts>
               <ts e="T254" id="Seg_1596" n="e" s="T253">nʼärnän </ts>
               <ts e="T255" id="Seg_1598" n="e" s="T254">nämdə, </ts>
               <ts e="T256" id="Seg_1600" n="e" s="T255">na </ts>
               <ts e="T257" id="Seg_1602" n="e" s="T256">qwätpɨdi </ts>
               <ts e="T258" id="Seg_1604" n="e" s="T257">qwärɣə </ts>
               <ts e="T259" id="Seg_1606" n="e" s="T258">ambat, </ts>
               <ts e="T260" id="Seg_1608" n="e" s="T259">tʼeːlɨn </ts>
               <ts e="T261" id="Seg_1610" n="e" s="T260">suːrulʼdʼeqɨn </ts>
               <ts e="T262" id="Seg_1612" n="e" s="T261">äːwɨmdə </ts>
               <ts e="T263" id="Seg_1614" n="e" s="T262">nändɨzä. </ts>
               <ts e="T264" id="Seg_1616" n="e" s="T263">iːdə </ts>
               <ts e="T265" id="Seg_1618" n="e" s="T264">ilʼlʼɨqqus </ts>
               <ts e="T266" id="Seg_1620" n="e" s="T265">piːrʼäqən. </ts>
               <ts e="T267" id="Seg_1622" n="e" s="T266">täp </ts>
               <ts e="T268" id="Seg_1624" n="e" s="T267">qwässɨ </ts>
               <ts e="T269" id="Seg_1626" n="e" s="T268">qrandə, </ts>
               <ts e="T270" id="Seg_1628" n="e" s="T269">qajqɨn </ts>
               <ts e="T271" id="Seg_1630" n="e" s="T270">ilɨs </ts>
               <ts e="T272" id="Seg_1632" n="e" s="T271">äwɨt, </ts>
               <ts e="T273" id="Seg_1634" n="e" s="T272">täbɨn </ts>
               <ts e="T274" id="Seg_1636" n="e" s="T273">obɨt. </ts>
               <ts e="T275" id="Seg_1638" n="e" s="T274">täp </ts>
               <ts e="T276" id="Seg_1640" n="e" s="T275">nilʼdʼin </ts>
               <ts e="T277" id="Seg_1642" n="e" s="T276">tʼärɨs: </ts>
               <ts e="T278" id="Seg_1644" n="e" s="T277">man </ts>
               <ts e="T279" id="Seg_1646" n="e" s="T278">bə </ts>
               <ts e="T280" id="Seg_1648" n="e" s="T279">assə </ts>
               <ts e="T281" id="Seg_1650" n="e" s="T280">qwätnäw </ts>
               <ts e="T282" id="Seg_1652" n="e" s="T281">taw </ts>
               <ts e="T283" id="Seg_1654" n="e" s="T282">qwärɣɨm, </ts>
               <ts e="T284" id="Seg_1656" n="e" s="T283">a </ts>
               <ts e="T285" id="Seg_1658" n="e" s="T284">tʼädɨnäw </ts>
               <ts e="T286" id="Seg_1660" n="e" s="T285">äwennɨ </ts>
               <ts e="T287" id="Seg_1662" n="e" s="T286">maːdəm </ts>
               <ts e="T288" id="Seg_1664" n="e" s="T287">oqqɨmɨqɨn </ts>
               <ts e="T289" id="Seg_1666" n="e" s="T288">qwärɣəzä. </ts>
               <ts e="T290" id="Seg_1668" n="e" s="T289">man </ts>
               <ts e="T291" id="Seg_1670" n="e" s="T290">nʼärnä </ts>
               <ts e="T292" id="Seg_1672" n="e" s="T291">qwärɣɨlam </ts>
               <ts e="T293" id="Seg_1674" n="e" s="T292">qwätqweːnǯaw, </ts>
               <ts e="T294" id="Seg_1676" n="e" s="T293">tʼätǯiqwenǯaw. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2" id="Seg_1677" s="T0">KMS_1963_BearAteTwoWomen_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_1678" s="T2">KMS_1963_BearAteTwoWomen_nar.002 (001.002)</ta>
            <ta e="T10" id="Seg_1679" s="T7">KMS_1963_BearAteTwoWomen_nar.003 (001.003)</ta>
            <ta e="T14" id="Seg_1680" s="T10">KMS_1963_BearAteTwoWomen_nar.004 (001.004)</ta>
            <ta e="T17" id="Seg_1681" s="T14">KMS_1963_BearAteTwoWomen_nar.005 (001.005)</ta>
            <ta e="T19" id="Seg_1682" s="T17">KMS_1963_BearAteTwoWomen_nar.006 (001.006)</ta>
            <ta e="T22" id="Seg_1683" s="T19">KMS_1963_BearAteTwoWomen_nar.007 (001.007)</ta>
            <ta e="T27" id="Seg_1684" s="T22">KMS_1963_BearAteTwoWomen_nar.008 (001.008)</ta>
            <ta e="T30" id="Seg_1685" s="T27">KMS_1963_BearAteTwoWomen_nar.009 (001.009)</ta>
            <ta e="T36" id="Seg_1686" s="T30">KMS_1963_BearAteTwoWomen_nar.010 (001.010)</ta>
            <ta e="T41" id="Seg_1687" s="T36">KMS_1963_BearAteTwoWomen_nar.011 (001.011)</ta>
            <ta e="T47" id="Seg_1688" s="T41">KMS_1963_BearAteTwoWomen_nar.012 (001.012)</ta>
            <ta e="T53" id="Seg_1689" s="T47">KMS_1963_BearAteTwoWomen_nar.013 (001.013)</ta>
            <ta e="T57" id="Seg_1690" s="T53">KMS_1963_BearAteTwoWomen_nar.014 (001.014)</ta>
            <ta e="T61" id="Seg_1691" s="T57">KMS_1963_BearAteTwoWomen_nar.015 (001.015)</ta>
            <ta e="T66" id="Seg_1692" s="T61">KMS_1963_BearAteTwoWomen_nar.016 (001.016)</ta>
            <ta e="T71" id="Seg_1693" s="T66">KMS_1963_BearAteTwoWomen_nar.017 (001.017)</ta>
            <ta e="T74" id="Seg_1694" s="T71">KMS_1963_BearAteTwoWomen_nar.018 (001.018)</ta>
            <ta e="T77" id="Seg_1695" s="T74">KMS_1963_BearAteTwoWomen_nar.019 (001.019)</ta>
            <ta e="T84" id="Seg_1696" s="T77">KMS_1963_BearAteTwoWomen_nar.020 (001.020)</ta>
            <ta e="T91" id="Seg_1697" s="T84">KMS_1963_BearAteTwoWomen_nar.021 (001.021)</ta>
            <ta e="T97" id="Seg_1698" s="T91">KMS_1963_BearAteTwoWomen_nar.022 (001.022)</ta>
            <ta e="T101" id="Seg_1699" s="T97">KMS_1963_BearAteTwoWomen_nar.023 (001.023)</ta>
            <ta e="T104" id="Seg_1700" s="T101">KMS_1963_BearAteTwoWomen_nar.024 (001.024)</ta>
            <ta e="T111" id="Seg_1701" s="T104">KMS_1963_BearAteTwoWomen_nar.025 (001.025)</ta>
            <ta e="T114" id="Seg_1702" s="T111">KMS_1963_BearAteTwoWomen_nar.026 (001.026)</ta>
            <ta e="T118" id="Seg_1703" s="T114">KMS_1963_BearAteTwoWomen_nar.027 (001.027)</ta>
            <ta e="T126" id="Seg_1704" s="T118">KMS_1963_BearAteTwoWomen_nar.028 (001.028)</ta>
            <ta e="T131" id="Seg_1705" s="T126">KMS_1963_BearAteTwoWomen_nar.029 (001.029)</ta>
            <ta e="T135" id="Seg_1706" s="T131">KMS_1963_BearAteTwoWomen_nar.030 (001.030)</ta>
            <ta e="T139" id="Seg_1707" s="T135">KMS_1963_BearAteTwoWomen_nar.031 (001.031)</ta>
            <ta e="T147" id="Seg_1708" s="T139">KMS_1963_BearAteTwoWomen_nar.032 (001.032)</ta>
            <ta e="T153" id="Seg_1709" s="T147">KMS_1963_BearAteTwoWomen_nar.033 (001.033)</ta>
            <ta e="T161" id="Seg_1710" s="T153">KMS_1963_BearAteTwoWomen_nar.034 (001.034)</ta>
            <ta e="T164" id="Seg_1711" s="T161">KMS_1963_BearAteTwoWomen_nar.035 (001.035)</ta>
            <ta e="T167" id="Seg_1712" s="T164">KMS_1963_BearAteTwoWomen_nar.036 (001.036)</ta>
            <ta e="T171" id="Seg_1713" s="T167">KMS_1963_BearAteTwoWomen_nar.037 (001.037)</ta>
            <ta e="T176" id="Seg_1714" s="T171">KMS_1963_BearAteTwoWomen_nar.038 (001.038)</ta>
            <ta e="T183" id="Seg_1715" s="T176">KMS_1963_BearAteTwoWomen_nar.039 (001.039)</ta>
            <ta e="T186" id="Seg_1716" s="T183">KMS_1963_BearAteTwoWomen_nar.040 (001.040)</ta>
            <ta e="T193" id="Seg_1717" s="T186">KMS_1963_BearAteTwoWomen_nar.041 (001.041)</ta>
            <ta e="T200" id="Seg_1718" s="T193">KMS_1963_BearAteTwoWomen_nar.042 (001.042)</ta>
            <ta e="T206" id="Seg_1719" s="T200">KMS_1963_BearAteTwoWomen_nar.043 (001.043)</ta>
            <ta e="T210" id="Seg_1720" s="T206">KMS_1963_BearAteTwoWomen_nar.044 (001.044)</ta>
            <ta e="T216" id="Seg_1721" s="T210">KMS_1963_BearAteTwoWomen_nar.045 (001.045)</ta>
            <ta e="T222" id="Seg_1722" s="T216">KMS_1963_BearAteTwoWomen_nar.046 (001.046)</ta>
            <ta e="T225" id="Seg_1723" s="T222">KMS_1963_BearAteTwoWomen_nar.047 (001.047)</ta>
            <ta e="T228" id="Seg_1724" s="T225">KMS_1963_BearAteTwoWomen_nar.048 (001.048)</ta>
            <ta e="T234" id="Seg_1725" s="T228">KMS_1963_BearAteTwoWomen_nar.049 (001.049)</ta>
            <ta e="T237" id="Seg_1726" s="T234">KMS_1963_BearAteTwoWomen_nar.050 (001.050)</ta>
            <ta e="T242" id="Seg_1727" s="T237">KMS_1963_BearAteTwoWomen_nar.051 (001.051)</ta>
            <ta e="T247" id="Seg_1728" s="T242">KMS_1963_BearAteTwoWomen_nar.052 (001.052)</ta>
            <ta e="T263" id="Seg_1729" s="T247">KMS_1963_BearAteTwoWomen_nar.053 (001.053)</ta>
            <ta e="T266" id="Seg_1730" s="T263">KMS_1963_BearAteTwoWomen_nar.054 (001.054)</ta>
            <ta e="T274" id="Seg_1731" s="T266">KMS_1963_BearAteTwoWomen_nar.055 (001.055)</ta>
            <ta e="T289" id="Seg_1732" s="T274">KMS_1963_BearAteTwoWomen_nar.056 (001.056)</ta>
            <ta e="T294" id="Seg_1733" s="T289">KMS_1963_BearAteTwoWomen_nar.057 (001.057)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T2" id="Seg_1734" s="T0">′ӓ̄сан Марковоɣон.</ta>
            <ta e="T7" id="Seg_1735" s="T2">ны′тʼан ма′тʼтʼӧɣын ′ӓ̄сан ′оккы ′ӓ̄да.</ta>
            <ta e="T10" id="Seg_1736" s="T7">′нимдъ ′тӓбын Марково.</ta>
            <ta e="T14" id="Seg_1737" s="T10">ны′тʼӓн иллыкус па′jа ′нӓндызӓ.</ta>
            <ta e="T17" id="Seg_1738" s="T14">и′рат тӓбы′нан ′тʼӓңус.</ta>
            <ta e="T19" id="Seg_1739" s="T17">иллы′к(к)ус он′дӓ.</ta>
            <ta e="T22" id="Seg_1740" s="T19">′нʼӓjайгус, ′тшоппырый′гус, свӓң(ɣ)ыйгус</ta>
            <ta e="T27" id="Seg_1741" s="T22">оккы′рың ӓ′ран ′нӓдъ(ӓ) kwан′нӓ ′пе̄мыгу.</ta>
            <ta e="T30" id="Seg_1742" s="T27">′ӓ̄wыт ′ӓ̄кус ′ма̄тkын.</ta>
            <ta e="T36" id="Seg_1743" s="T30">kа′роɣъннӓ ‵то ӱ′тӓттӓ тӓп палʼ′дʼӱс ′нʼӓjалʼ(лʼ)е.</ta>
            <ta e="T41" id="Seg_1744" s="T36">′тʼе̄лын палʼдʼу′лʼевлʼе ′ӱ̄домыɣӓн ла′kkонӓ ′ма̄тkындъ.</ta>
            <ta e="T47" id="Seg_1745" s="T41">′тӱ̄мбың алʼи мо′kаукаң ′тша̄джимба kо′ттӓ ′ма̄тkындъ.</ta>
            <ta e="T53" id="Seg_1746" s="T47">′тʼелʼеɣы(ӓ)ндъ тӓбым ′kы̄рымбат ′kwӓрɣӓ нӓйɣун вӓ′ттоун.</ta>
            <ta e="T57" id="Seg_1747" s="T53">мидъм′бат ′ӱ̄дъмыɣын ′сурулʼди па′jам.</ta>
            <ta e="T61" id="Seg_1748" s="T57">о′ралбат, kwӓт′пат, а̄м′бат ′тӓбым.</ta>
            <ta e="T66" id="Seg_1749" s="T61">kаймы′ламдъ ‵нышкылгыл′бат, ‵тӱлъ′сӓмдъ то̄ ′тʼӓтджимбат.</ta>
            <ta e="T71" id="Seg_1750" s="T66">kалым′ба оккъ ′то̄быт таw нӓ′дӓнанна.</ta>
            <ta e="T74" id="Seg_1751" s="T71">′ӱ̄дъмын ′ӓwӓт ӓдълд′жис.</ta>
            <ta e="T77" id="Seg_1752" s="T74">нӓдъ ассъ ′тӱмба.</ta>
            <ta e="T84" id="Seg_1753" s="T77">вес пин ′ӓwыт ′нӓндыго ′тӓ̄рбылʼе ′ассъ ′kондыс.</ta>
            <ta e="T91" id="Seg_1754" s="T84">та̄р ′ассъ ′тʼе̄лымбыс ′ӓ̄выт kwа′нны ′нӓ̄мдъ ′пергу.</ta>
            <ta e="T97" id="Seg_1755" s="T91">телʼ′дʼелʼи ‵kwӓнбъ′ди wӓ′тто̄вын ′ӓ̄выт ′тша̄джикус ма′тʼтʼӧɣын.</ta>
            <ta e="T101" id="Seg_1756" s="T97">′kунны да ‵саппы′зиң ′kwӓ̄рɣъ.</ta>
            <ta e="T104" id="Seg_1757" s="T101">о′ранныт па′jам, ′а̄мныт.</ta>
            <ta e="T111" id="Seg_1758" s="T104">па′jан ′тӱ̄мбъди вӓ′ттовын ′kwӓрɣъ ′тша̄джыс па′jан ′ма̄ттъ.</ta>
            <ta e="T114" id="Seg_1759" s="T111">′ма̄тkын кутты′на тʼӓ′ңус.</ta>
            <ta e="T118" id="Seg_1760" s="T114">′ма̄ттам нʼӱит, ′се(ӓ)рнӓ ′ма̄ттъ.</ta>
            <ta e="T126" id="Seg_1761" s="T118">ма̄дъ сун′дʼеɣын ′палʼдʼулʼеwлʼе ′лʼевым и′ннӓ вӓ′тʼтʼит, па′нны потполджъ(а).</ta>
            <ta e="T131" id="Seg_1762" s="T126">′сы̄ри ′ма̄тkын ′ныңгызаттъ ′сыр ′тʼӧлазе.</ta>
            <ta e="T135" id="Seg_1763" s="T131">‵тӓп′kим ′kwӓрɣъ ′ассъ лаkkърызыт.</ta>
            <ta e="T139" id="Seg_1764" s="T135">пот′полɣън ′kwӓрɣъ хо′зʼаин ′ӓ̄сан.</ta>
            <ta e="T147" id="Seg_1765" s="T139">нӣн ӓ̄ссан ′са̄k, kwӓлын ӱ̄р, ′саɣътпъди ′вӓтʼтʼе, ′мʼӧдъ.</ta>
            <ta e="T153" id="Seg_1766" s="T147">′са̄ɣым, ′саɣътпъди вӓ′дʼим тӓп kамджим′бат, kӓ̄лɣылбат.</ta>
            <ta e="T161" id="Seg_1767" s="T153">а ′kwӓлын ′ӱ̄рым и ′мʼӧдъм kwӓрɣъ ӱт′пат (′а̄мбат).</ta>
            <ta e="T164" id="Seg_1768" s="T161">′сурулʼдʼи ′kула ′тӱ̄wаттъ.</ta>
            <ta e="T167" id="Seg_1769" s="T164">′ма̄дым ′ассъ kо′солджиkwаттъ.</ta>
            <ta e="T171" id="Seg_1770" s="T167">kаиң′го дак′ка ‵лӓрумбы′сын ′ӓ̄аттъ.</ta>
            <ta e="T176" id="Seg_1771" s="T171">′оккыр ′сурулʼдʼи kум ′ма̄ттъ ′сӓрна.</ta>
            <ta e="T183" id="Seg_1772" s="T176">′ма̄дъ сӱн′дʼеɣын ′пӱруң палʼдʼӧ′лʼе̄ан kуй′ɣан, ′тӓ̄рбылʼе нын′ге̄ен.</ta>
            <ta e="T186" id="Seg_1773" s="T183">′kайдака пот ′полɣън ′рӱконʼнʼе.</ta>
            <ta e="T193" id="Seg_1774" s="T186">′сурулʼдʼи kум ӱнды′зӓыт, kо̄′солджит: потполɣын еиң ′kwӓрɣа.</ta>
            <ta e="T200" id="Seg_1775" s="T193">′понӓ тшан′джиң лʼӓ′ɣалыɣындъ тʼӓ′рын: ′потполɣын и′ппа ′kwӓрɣъ.</ta>
            <ta e="T206" id="Seg_1776" s="T200">′kwӓрɣелʼди kула ′сернаттъ ′ма̄ттъ, ′kӓттаттъ ′потпылым.</ta>
            <ta e="T210" id="Seg_1777" s="T206">(′понӓ тшан′джаттъ, ′ма̄тамдъ ′kӓттаттъ.)</ta>
            <ta e="T216" id="Seg_1778" s="T210">оккы′ лʼевым ′kаɣыннаттъ, ′kwӓрɣым пот′полɣын kолʼдʼӓттъ.</ta>
            <ta e="T222" id="Seg_1779" s="T216">′сурулʼдʼи kула ′ассъ ′на̄рың ′тӓттың ′тʼӓттшаттъ.</ta>
            <ta e="T225" id="Seg_1780" s="T222">тӓп и ′kа̄рыссӓ.</ta>
            <ta e="T228" id="Seg_1781" s="T225">′нӓннӓ ′сӯрум kуй′ɣалджиң.</ta>
            <ta e="T234" id="Seg_1782" s="T228">′шыттъ ′лʼев(w)ым ′kаɣъннаттъ, манджим′батт, ′kwӓрɣъ(ӓ) ′kумба̄.</ta>
            <ta e="T237" id="Seg_1783" s="T234">′тӓбым и′ннӓ сабы′нʼӓттъ.</ta>
            <ta e="T242" id="Seg_1784" s="T237">′kобымдъ кы′раттъ, ′вӓдʼимдъ то̄ тʼӓттшаттъ.</ta>
            <ta e="T247" id="Seg_1785" s="T242">′вӓдʼит ӓ̄сан ′сӓɣъ, ′ассъ ‵kаппърымбӓ′ди.</ta>
            <ta e="T263" id="Seg_1786" s="T247">′нӓннӓ kwӓ′лʼевлʼе ′сурулʼди ′kӯла ӓвым′дӓ kомбы′заттъ, нʼӓр′нӓн ′нӓмдъ, на ′kwӓтпыди ′kwӓрɣъ ам′бат, ′тʼе̄лын ′сӯрулʼ′дʼеɣын ′ӓ̄вымдъ ′нӓндызӓ.</ta>
            <ta e="T266" id="Seg_1787" s="T263">′ӣдъ иллык′кус ′пӣрʼӓɣън.</ta>
            <ta e="T274" id="Seg_1788" s="T266">тӓп kwӓ′ссы ′kрандъ, kайɣын и′лыс ′ӓвыт, ′тӓбын ′обыт.</ta>
            <ta e="T289" id="Seg_1789" s="T274">тӓп нилʼ′дʼин тʼӓ′рыс: ман бъ ассъ kwӓтнӓw таw ′kwӓрɣым, а ′тʼӓдынӓw ӓ′wенны(ъ) ′ма̄дъм ‵оккымы′ɣын ′kwӓрɣъзӓ.</ta>
            <ta e="T294" id="Seg_1790" s="T289">ман нʼӓр′нӓ ‵kwӓрɣы′лам kwӓт′kwе̄нджаw, тʼӓтджи′квенджаw.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T2" id="Seg_1791" s="T0">äːsan Мarkovoqon.</ta>
            <ta e="T7" id="Seg_1792" s="T2">nɨčan maččöqɨn äːsan oqqɨ äːda.</ta>
            <ta e="T10" id="Seg_1793" s="T7">nimdə täbɨn Мarqovo.</ta>
            <ta e="T14" id="Seg_1794" s="T10">nɨčän illɨqus paja nändɨzä.</ta>
            <ta e="T17" id="Seg_1795" s="T14">irat täbɨnan čäŋus.</ta>
            <ta e="T19" id="Seg_1796" s="T17">illɨq(q)us ondä.</ta>
            <ta e="T22" id="Seg_1797" s="T19">nʼäjajgus, tšoppɨrɨjgus, sväŋ(q)ɨjgus</ta>
            <ta e="T27" id="Seg_1798" s="T22">oqqɨrɨŋ äran nädə(ä) qwannä peːmɨgu.</ta>
            <ta e="T30" id="Seg_1799" s="T27">äːwɨt äːqus maːtqɨn.</ta>
            <ta e="T36" id="Seg_1800" s="T30">qaroqənnä to ütättä täp palʼdʼüs nʼäjalʼ(lʼ)e.</ta>
            <ta e="T41" id="Seg_1801" s="T36">tʼeːlɨn palʼdʼulʼewlʼe üːdomɨqän laqqonä maːtqɨndə.</ta>
            <ta e="T47" id="Seg_1802" s="T41">tüːmbɨŋ alʼi moqauqaŋ čaːǯimba qottä maːtqɨndə.</ta>
            <ta e="T53" id="Seg_1803" s="T47">tʼelʼeqɨ(ä)ndə täbɨm qɨːrɨmbat qwärqä näjɣun wättoun.</ta>
            <ta e="T57" id="Seg_1804" s="T53">midəmbat üːdəmɨɣɨn surulʼdi pajam.</ta>
            <ta e="T61" id="Seg_1805" s="T57">oralbat, qwätpat, aːmbat täbɨm.</ta>
            <ta e="T66" id="Seg_1806" s="T61">qajmɨlamdə nɨšqɨlʼgɨlʼbat, tülʼəsämdə toː tʼätǯimbat.</ta>
            <ta e="T71" id="Seg_1807" s="T66">qalɨmba oqqə toːbɨt taw nädänanna.</ta>
            <ta e="T74" id="Seg_1808" s="T71">üːdəmɨn äwät ädəlʼǯis.</ta>
            <ta e="T77" id="Seg_1809" s="T74">nädə assə tümba.</ta>
            <ta e="T84" id="Seg_1810" s="T77">wes pin äwɨt nändɨgo täːrbɨlʼe assə qondɨs.</ta>
            <ta e="T91" id="Seg_1811" s="T84">taːr assə čeːlʼɨmbɨs äːwɨt qwannɨ näːmdə pergu.</ta>
            <ta e="T97" id="Seg_1812" s="T91">telʼdʼelʼi qwänbədi wättoːvɨn äːvɨt tšaːǯiqus maččöqɨn.</ta>
            <ta e="T101" id="Seg_1813" s="T97">qunnɨ da sappɨziŋ qwäːrɣə.</ta>
            <ta e="T104" id="Seg_1814" s="T101">orannɨt pajam, aːmnɨt.</ta>
            <ta e="T111" id="Seg_1815" s="T104">pajan tüːmbədi wättovɨn qwärɣə tšaːǯɨs pajan maːttə.</ta>
            <ta e="T114" id="Seg_1816" s="T111">maːtqɨn quttɨna čäŋus.</ta>
            <ta e="T118" id="Seg_1817" s="T114">maːttam nʼüit, se(ä)rnä maːttə.</ta>
            <ta e="T126" id="Seg_1818" s="T118">maːdə sundʼeqɨn palʼdʼulʼewlʼe lʼewɨm innä wätʼtʼit, pannɨ potpolǯə(a).</ta>
            <ta e="T131" id="Seg_1819" s="T126">sɨːri maːtqɨn nɨŋgɨzattə sɨr tʼölʼaze.</ta>
            <ta e="T135" id="Seg_1820" s="T131">täpqim qwärɣə assə laqqərɨzɨt.</ta>
            <ta e="T139" id="Seg_1821" s="T135">potpolqən qwärɣə hozʼain äːsan.</ta>
            <ta e="T147" id="Seg_1822" s="T139">niːn äːssan saːq, qwälʼɨn üːr, saqətpədi väčče, mʼödə.</ta>
            <ta e="T153" id="Seg_1823" s="T147">saːqɨm, saqətpədi vädʼim täp qamǯimbat, qäːlʼqɨlʼbat.</ta>
            <ta e="T161" id="Seg_1824" s="T153">a qwälʼɨn üːrɨm i mʼödəm qwärɣə ütpat (aːmbat).</ta>
            <ta e="T164" id="Seg_1825" s="T161">surulʼdʼi qula tüːwattə.</ta>
            <ta e="T167" id="Seg_1826" s="T164">maːdɨm assə qosolǯiqwattə.</ta>
            <ta e="T171" id="Seg_1827" s="T167">qaiŋgo daqqa lärumbɨsɨn äːattə.</ta>
            <ta e="T176" id="Seg_1828" s="T171">oqqɨr surulʼdʼi qum maːttə särna.</ta>
            <ta e="T183" id="Seg_1829" s="T176">maːdə sündʼeqɨn püruŋ palʼdʼölʼeːan qujqan, täːrbɨlʼe nɨngeːen.</ta>
            <ta e="T186" id="Seg_1830" s="T183">qajdaqa pot polqən rüqonʼnʼe.</ta>
            <ta e="T193" id="Seg_1831" s="T186">surulʼdʼi qum ündɨzäɨt, qoːsolǯit: potpolqɨn eiŋ qwärɣa.</ta>
            <ta e="T200" id="Seg_1832" s="T193">ponä tšanǯiŋ lʼäqalɨqɨndə tʼärɨn: potpolqɨn ippa qwärɣə.</ta>
            <ta e="T206" id="Seg_1833" s="T200">qwärqelʼdi qula sernattə maːttə, qättattə potpɨlɨm.</ta>
            <ta e="T210" id="Seg_1834" s="T206">(ponä tšanǯattə, maːtamdə qättattə.)</ta>
            <ta e="T216" id="Seg_1835" s="T210">oqqɨ lʼewɨm qaqɨnnattə, qwärqɨm potpolqɨn qolʼdʼättə.</ta>
            <ta e="T222" id="Seg_1836" s="T216">surulʼdʼi qulʼa assə naːrɨŋ tättɨŋ tʼättšattə.</ta>
            <ta e="T225" id="Seg_1837" s="T222">täp i qaːrɨssä.</ta>
            <ta e="T228" id="Seg_1838" s="T225">nännä suːrum qujqalǯiŋ.</ta>
            <ta e="T234" id="Seg_1839" s="T228">šɨttə lʼewɨm qaqənnattə, manǯimbatt, qwärɣə(ä) qumbaː.</ta>
            <ta e="T237" id="Seg_1840" s="T234">täbɨm innä sabɨnʼättə.</ta>
            <ta e="T242" id="Seg_1841" s="T237">qobɨmdə qɨrattə, vädʼimdə toː tʼättšattə.</ta>
            <ta e="T247" id="Seg_1842" s="T242">vädʼit äːsan säqə, assə qappərɨmbädi.</ta>
            <ta e="T263" id="Seg_1843" s="T247">nännä qwälevle surulʼdi quːla ävɨmdä qombɨzattə, nʼärnän nämdə, na qwätpɨdi qwärɣə ambat, čeːlɨn suːrulʼdʼeqɨn äːvɨmdə nändɨzä.</ta>
            <ta e="T266" id="Seg_1844" s="T263">iːdə ilʼlʼɨqqus piːrʼäqən.</ta>
            <ta e="T274" id="Seg_1845" s="T266">täp qwässɨ qrandə, qajqɨn ilɨs ävɨt, täbɨn obɨt.</ta>
            <ta e="T289" id="Seg_1846" s="T274">täp nilʼdʼin čärɨs: man bə assə qwätnäw taw qwärqɨm, a čädɨnäw äwennɨ(ə) maːdəm oqqɨmɨqɨn qwärɣəzä.</ta>
            <ta e="T294" id="Seg_1847" s="T289">man nʼärnä qwärɣɨlʼam qwätqweːnǯaw, tʼätǯiqvenǯaw.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2" id="Seg_1848" s="T0">äːsan Мarkovoqon. </ta>
            <ta e="T7" id="Seg_1849" s="T2">nɨtʼan matʼtʼöqɨn äːsan oqqɨ äːda. </ta>
            <ta e="T10" id="Seg_1850" s="T7">nimdə täbɨn Мarqovo. </ta>
            <ta e="T14" id="Seg_1851" s="T10">nɨtʼän illɨqus paja nändɨzä. </ta>
            <ta e="T17" id="Seg_1852" s="T14">irat täbɨnan čäŋus. </ta>
            <ta e="T19" id="Seg_1853" s="T17">illɨqqus ondä. </ta>
            <ta e="T22" id="Seg_1854" s="T19">nʼäjajgus, tšoppɨrɨjgus, sväŋqɨjgus. </ta>
            <ta e="T27" id="Seg_1855" s="T22">oqqɨrɨŋ äran nädə qwannä peːmɨgu. </ta>
            <ta e="T30" id="Seg_1856" s="T27">äːwɨt äːqus maːtqɨn. </ta>
            <ta e="T36" id="Seg_1857" s="T30">qaroqənnä to ütättä täp palʼdʼüs nʼäjalʼlʼe. </ta>
            <ta e="T41" id="Seg_1858" s="T36">tʼeːlɨn palʼdʼulʼewlʼe üːdomɨqän laqqonä maːtqɨndə. </ta>
            <ta e="T47" id="Seg_1859" s="T41">tüːmbɨŋ alʼi moqauqaŋ čaːǯimba qottä maːtqɨndə. </ta>
            <ta e="T53" id="Seg_1860" s="T47">tʼelʼeqɨndə täbɨm qɨːrɨmbat qwärqä näjqun wättoun. </ta>
            <ta e="T57" id="Seg_1861" s="T53">midəmbat üːdəmɨqɨn surulʼdi pajam. </ta>
            <ta e="T61" id="Seg_1862" s="T57">oralbat, qwätpat, aːmbat täbɨm. </ta>
            <ta e="T66" id="Seg_1863" s="T61">qajmɨlamdə nɨšqɨlgɨlʼbat, tülʼəsämdə toː tʼätǯimbat. </ta>
            <ta e="T71" id="Seg_1864" s="T66">qalɨmba oqqə toːbɨt taw nädänanna. </ta>
            <ta e="T74" id="Seg_1865" s="T71">üːdəmɨn äwät ädəlǯis. </ta>
            <ta e="T77" id="Seg_1866" s="T74">nädə assə tümba. </ta>
            <ta e="T84" id="Seg_1867" s="T77">wes pin äwɨt nändɨgo täːrbɨlʼe assə qondɨs. </ta>
            <ta e="T91" id="Seg_1868" s="T84">taːr assə tʼeːlɨmbɨs äːwɨt qwannɨ näːmdə pergu. </ta>
            <ta e="T97" id="Seg_1869" s="T91">telʼdʼelʼi qwänbədi wättoːwɨn äːwɨt čaːǯiqus matʼtʼöqɨn. </ta>
            <ta e="T101" id="Seg_1870" s="T97">qunnɨ da sappɨziŋ qwäːrɣə. </ta>
            <ta e="T104" id="Seg_1871" s="T101">orannɨt pajam, aːmnɨt. </ta>
            <ta e="T111" id="Seg_1872" s="T104">pajan tüːmbədi wättowɨn qwärɣə čaːǯɨs pajan maːttə. </ta>
            <ta e="T114" id="Seg_1873" s="T111">maːtqɨn quttɨna tʼäŋus. </ta>
            <ta e="T118" id="Seg_1874" s="T114">maːttam nʼüit, sernä maːttə. </ta>
            <ta e="T126" id="Seg_1875" s="T118">maːdə sundʼeqɨn palʼdʼulʼewlʼe lʼewɨm innä wätʼtʼit, pannɨ potpolǯə. </ta>
            <ta e="T131" id="Seg_1876" s="T126">sɨːri maːtqɨn nɨŋgɨzattə sɨr tʼölʼaze. </ta>
            <ta e="T135" id="Seg_1877" s="T131">täpqim qwärɣə assə laqqərɨzɨt. </ta>
            <ta e="T139" id="Seg_1878" s="T135">potpolqən qwärɣə hozʼain äːsan. </ta>
            <ta e="T147" id="Seg_1879" s="T139">niːn äːssan saːq, qwälɨn üːr, saqətpədi wätʼtʼe, mʼödə. </ta>
            <ta e="T153" id="Seg_1880" s="T147">saːqɨm, saqətpədi wädʼim täp qamǯimbat, qäːlqɨlbat. </ta>
            <ta e="T161" id="Seg_1881" s="T153">a qwälɨn üːrɨm i mʼödəm qwärɣə ütpat (aːmbat). </ta>
            <ta e="T164" id="Seg_1882" s="T161">surulʼdʼi qula tüːwattə. </ta>
            <ta e="T167" id="Seg_1883" s="T164">maːdɨm assə qosolǯiqwattə. </ta>
            <ta e="T171" id="Seg_1884" s="T167">qaiŋgo daqqa lärumbɨsɨn äːattə. </ta>
            <ta e="T176" id="Seg_1885" s="T171">oqqɨr surulʼdʼi qum maːttə särna. </ta>
            <ta e="T183" id="Seg_1886" s="T176">maːdə sündʼeqɨn püruŋ palʼdʼölʼeːan qujqan, täːrbɨlʼe nɨngeːen. </ta>
            <ta e="T186" id="Seg_1887" s="T183">qajdaqa potpolqən rüqonʼnʼe. </ta>
            <ta e="T193" id="Seg_1888" s="T186">surulʼdʼi qum ündɨzäɨt, qoːsolǯit: potpolqɨn eiŋ qwärɣa. </ta>
            <ta e="T200" id="Seg_1889" s="T193">ponä tšanǯiŋ lʼäqalɨqɨndə tʼärɨn: potpolqɨn ippa qwärɣə. </ta>
            <ta e="T206" id="Seg_1890" s="T200">qwärqelʼdi qula sernattə maːttə, qättattə potpɨlɨm. </ta>
            <ta e="T210" id="Seg_1891" s="T206">(ponä tšanǯattə, maːtamdə qättattə.) </ta>
            <ta e="T216" id="Seg_1892" s="T210">oqqɨ lʼewɨm qaqɨnnattə, qwärqɨm potpolqɨn qolʼdʼättə. </ta>
            <ta e="T222" id="Seg_1893" s="T216">surulʼdʼi qula assə naːrɨŋ tättɨŋ tʼättšattə. </ta>
            <ta e="T225" id="Seg_1894" s="T222">täp i qaːrɨssä. </ta>
            <ta e="T228" id="Seg_1895" s="T225">nännä suːrum qujqalǯiŋ. </ta>
            <ta e="T234" id="Seg_1896" s="T228">šɨttə lʼewɨm qaqənnattə, manǯimbatt, qwärɣə qumbaː. </ta>
            <ta e="T237" id="Seg_1897" s="T234">täbɨm innä sabɨnʼättə. </ta>
            <ta e="T242" id="Seg_1898" s="T237">qobɨmdə qɨrattə, wädʼimdə toː tʼättšattə. </ta>
            <ta e="T247" id="Seg_1899" s="T242">wädʼit äːsan säqə, assə qappərɨmbädi. </ta>
            <ta e="T263" id="Seg_1900" s="T247">nännä qwälewle surulʼdi quːla äwɨmdä qombɨzattə, nʼärnän nämdə, na qwätpɨdi qwärɣə ambat, tʼeːlɨn suːrulʼdʼeqɨn äːwɨmdə nändɨzä. </ta>
            <ta e="T266" id="Seg_1901" s="T263">iːdə ilʼlʼɨqqus piːrʼäqən. </ta>
            <ta e="T274" id="Seg_1902" s="T266">täp qwässɨ qrandə, qajqɨn ilɨs äwɨt, täbɨn obɨt. </ta>
            <ta e="T289" id="Seg_1903" s="T274">täp nilʼdʼin tʼärɨs: man bə assə qwätnäw taw qwärɣɨm, a tʼädɨnäw äwennɨ maːdəm oqqɨmɨqɨn qwärɣəzä. </ta>
            <ta e="T294" id="Seg_1904" s="T289">man nʼärnä qwärɣɨlam qwätqweːnǯaw, tʼätǯiqwenǯaw. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1905" s="T0">äː-sa-n</ta>
            <ta e="T2" id="Seg_1906" s="T1">Мarkovo-qon</ta>
            <ta e="T3" id="Seg_1907" s="T2">nɨtʼan</ta>
            <ta e="T4" id="Seg_1908" s="T3">matʼtʼö-qɨn</ta>
            <ta e="T5" id="Seg_1909" s="T4">äː-sa-n</ta>
            <ta e="T6" id="Seg_1910" s="T5">oqqɨ</ta>
            <ta e="T7" id="Seg_1911" s="T6">äːda</ta>
            <ta e="T8" id="Seg_1912" s="T7">nim-də</ta>
            <ta e="T9" id="Seg_1913" s="T8">täb-ɨ-n</ta>
            <ta e="T10" id="Seg_1914" s="T9">Мarqovo</ta>
            <ta e="T11" id="Seg_1915" s="T10">nɨtʼän</ta>
            <ta e="T12" id="Seg_1916" s="T11">illɨ-qu-s</ta>
            <ta e="T13" id="Seg_1917" s="T12">paja</ta>
            <ta e="T14" id="Seg_1918" s="T13">nändɨ-zä</ta>
            <ta e="T15" id="Seg_1919" s="T14">ira-t</ta>
            <ta e="T16" id="Seg_1920" s="T15">täb-ɨ-nan</ta>
            <ta e="T17" id="Seg_1921" s="T16">čäŋu-s</ta>
            <ta e="T18" id="Seg_1922" s="T17">illɨ-qqu-s</ta>
            <ta e="T19" id="Seg_1923" s="T18">ondä</ta>
            <ta e="T20" id="Seg_1924" s="T19">nʼäja-j-gu-s</ta>
            <ta e="T21" id="Seg_1925" s="T20">čoppɨr-ɨ-j-gu-s</ta>
            <ta e="T22" id="Seg_1926" s="T21">sväŋqɨ-j-gu-s</ta>
            <ta e="T23" id="Seg_1927" s="T22">oqqɨr-ɨ-ŋ</ta>
            <ta e="T24" id="Seg_1928" s="T23">ära-n</ta>
            <ta e="T25" id="Seg_1929" s="T24">nädə</ta>
            <ta e="T26" id="Seg_1930" s="T25">qwan-nä</ta>
            <ta e="T27" id="Seg_1931" s="T26">peːmɨ-gu</ta>
            <ta e="T28" id="Seg_1932" s="T27">äːwɨ-t</ta>
            <ta e="T29" id="Seg_1933" s="T28">äː-qu-s</ta>
            <ta e="T30" id="Seg_1934" s="T29">maːt-qən</ta>
            <ta e="T31" id="Seg_1935" s="T30">qaro-qənnä</ta>
            <ta e="T32" id="Seg_1936" s="T31">to</ta>
            <ta e="T33" id="Seg_1937" s="T32">ütä-ttä</ta>
            <ta e="T34" id="Seg_1938" s="T33">täp</ta>
            <ta e="T35" id="Seg_1939" s="T34">palʼdʼü-s</ta>
            <ta e="T36" id="Seg_1940" s="T35">nʼäja-lʼ-lʼe</ta>
            <ta e="T37" id="Seg_1941" s="T36">tʼeːlɨ-n</ta>
            <ta e="T38" id="Seg_1942" s="T37">palʼdʼu-lʼewlʼe</ta>
            <ta e="T39" id="Seg_1943" s="T38">üːdo-mɨ-qän</ta>
            <ta e="T40" id="Seg_1944" s="T39">laqqo-nä</ta>
            <ta e="T41" id="Seg_1945" s="T40">maːt-qɨndə</ta>
            <ta e="T42" id="Seg_1946" s="T41">tüːmbɨ-ŋ</ta>
            <ta e="T43" id="Seg_1947" s="T42">alʼi</ta>
            <ta e="T44" id="Seg_1948" s="T43">moqa-uqaŋ</ta>
            <ta e="T45" id="Seg_1949" s="T44">čaːǯi-mba</ta>
            <ta e="T46" id="Seg_1950" s="T45">qottä</ta>
            <ta e="T47" id="Seg_1951" s="T46">maːt-qɨndə</ta>
            <ta e="T48" id="Seg_1952" s="T47">tʼelʼe-qɨndə</ta>
            <ta e="T49" id="Seg_1953" s="T48">täb-ɨ-m</ta>
            <ta e="T50" id="Seg_1954" s="T49">qɨːrɨ-mba-t</ta>
            <ta e="T51" id="Seg_1955" s="T50">qwärqä</ta>
            <ta e="T52" id="Seg_1956" s="T51">nä-j-qu-n</ta>
            <ta e="T53" id="Seg_1957" s="T52">wätto-un</ta>
            <ta e="T54" id="Seg_1958" s="T53">midə-mba-t</ta>
            <ta e="T55" id="Seg_1959" s="T54">üːdə-mɨqɨn</ta>
            <ta e="T56" id="Seg_1960" s="T55">suru-lʼdi</ta>
            <ta e="T57" id="Seg_1961" s="T56">paja-m</ta>
            <ta e="T58" id="Seg_1962" s="T57">ora-l-ba-t</ta>
            <ta e="T59" id="Seg_1963" s="T58">qwät-pa-t</ta>
            <ta e="T60" id="Seg_1964" s="T59">aːm-ba-t</ta>
            <ta e="T61" id="Seg_1965" s="T60">täb-ɨ-m</ta>
            <ta e="T62" id="Seg_1966" s="T61">qaj-mɨ-la-m-də</ta>
            <ta e="T63" id="Seg_1967" s="T62">nɨšqɨlgɨlʼ-ba-t</ta>
            <ta e="T64" id="Seg_1968" s="T63">tülʼəsä-m-də</ta>
            <ta e="T65" id="Seg_1969" s="T64">toː</ta>
            <ta e="T66" id="Seg_1970" s="T65">tʼätǯi-mba-t</ta>
            <ta e="T67" id="Seg_1971" s="T66">qalɨ-mba</ta>
            <ta e="T68" id="Seg_1972" s="T67">oqqə</ta>
            <ta e="T69" id="Seg_1973" s="T68">toːbɨ-t</ta>
            <ta e="T70" id="Seg_1974" s="T69">taw</ta>
            <ta e="T71" id="Seg_1975" s="T70">nädä-nanna</ta>
            <ta e="T72" id="Seg_1976" s="T71">üːdə-mɨn</ta>
            <ta e="T73" id="Seg_1977" s="T72">äwä-t</ta>
            <ta e="T74" id="Seg_1978" s="T73">ädəlǯi-s</ta>
            <ta e="T75" id="Seg_1979" s="T74">nädə</ta>
            <ta e="T76" id="Seg_1980" s="T75">assə</ta>
            <ta e="T77" id="Seg_1981" s="T76">tü-mba</ta>
            <ta e="T78" id="Seg_1982" s="T77">wes</ta>
            <ta e="T79" id="Seg_1983" s="T78">pi-n</ta>
            <ta e="T80" id="Seg_1984" s="T79">äwɨ-t</ta>
            <ta e="T81" id="Seg_1985" s="T80">nä-ndɨ-go</ta>
            <ta e="T82" id="Seg_1986" s="T81">täːrbɨ-lʼe</ta>
            <ta e="T83" id="Seg_1987" s="T82">assə</ta>
            <ta e="T84" id="Seg_1988" s="T83">qondɨ-s</ta>
            <ta e="T85" id="Seg_1989" s="T84">taːr</ta>
            <ta e="T86" id="Seg_1990" s="T85">assə</ta>
            <ta e="T87" id="Seg_1991" s="T86">tʼeːlɨ-m-bɨ-s</ta>
            <ta e="T88" id="Seg_1992" s="T87">äːwɨ-t</ta>
            <ta e="T89" id="Seg_1993" s="T88">qwan-nɨ</ta>
            <ta e="T90" id="Seg_1994" s="T89">näː-m-də</ta>
            <ta e="T91" id="Seg_1995" s="T90">pe-r-gu</ta>
            <ta e="T92" id="Seg_1996" s="T91">telʼdʼelʼi</ta>
            <ta e="T93" id="Seg_1997" s="T92">qwän-bədi</ta>
            <ta e="T94" id="Seg_1998" s="T93">wättoː-wɨn</ta>
            <ta e="T95" id="Seg_1999" s="T94">äːwɨ-t</ta>
            <ta e="T96" id="Seg_2000" s="T95">čaːǯi-qu-s</ta>
            <ta e="T97" id="Seg_2001" s="T96">matʼtʼö-qɨn</ta>
            <ta e="T98" id="Seg_2002" s="T97">qun-nɨ</ta>
            <ta e="T99" id="Seg_2003" s="T98">da</ta>
            <ta e="T100" id="Seg_2004" s="T99">sappɨ-zi-ŋ</ta>
            <ta e="T101" id="Seg_2005" s="T100">qwäːrɣə</ta>
            <ta e="T102" id="Seg_2006" s="T101">oral-nɨ-t</ta>
            <ta e="T103" id="Seg_2007" s="T102">paja-m</ta>
            <ta e="T104" id="Seg_2008" s="T103">aːm-nɨ-t</ta>
            <ta e="T105" id="Seg_2009" s="T104">paja-n</ta>
            <ta e="T106" id="Seg_2010" s="T105">tüː-mbədi</ta>
            <ta e="T107" id="Seg_2011" s="T106">wätto-wɨn</ta>
            <ta e="T108" id="Seg_2012" s="T107">qwärɣə</ta>
            <ta e="T109" id="Seg_2013" s="T108">čaːǯɨ</ta>
            <ta e="T110" id="Seg_2014" s="T109">paja-n</ta>
            <ta e="T111" id="Seg_2015" s="T110">maːt-tə</ta>
            <ta e="T112" id="Seg_2016" s="T111">maːt-qən</ta>
            <ta e="T113" id="Seg_2017" s="T112">quttɨ-na</ta>
            <ta e="T114" id="Seg_2018" s="T113">tʼäŋu-s</ta>
            <ta e="T115" id="Seg_2019" s="T114">maːtta-m</ta>
            <ta e="T116" id="Seg_2020" s="T115">nʼü-i-t</ta>
            <ta e="T117" id="Seg_2021" s="T116">ser-nä</ta>
            <ta e="T118" id="Seg_2022" s="T117">maːt-tə</ta>
            <ta e="T119" id="Seg_2023" s="T118">maːdə</ta>
            <ta e="T120" id="Seg_2024" s="T119">sundʼe-qɨn</ta>
            <ta e="T121" id="Seg_2025" s="T120">palʼdʼu-lʼewlʼe</ta>
            <ta e="T122" id="Seg_2026" s="T121">lʼew-ɨ-m</ta>
            <ta e="T123" id="Seg_2027" s="T122">innä</ta>
            <ta e="T124" id="Seg_2028" s="T123">wätʼtʼi-t</ta>
            <ta e="T125" id="Seg_2029" s="T124">pan-nɨ</ta>
            <ta e="T126" id="Seg_2030" s="T125">potpol-ǯə</ta>
            <ta e="T127" id="Seg_2031" s="T126">sɨːr-i</ta>
            <ta e="T128" id="Seg_2032" s="T127">maːt-qən</ta>
            <ta e="T129" id="Seg_2033" s="T128">nɨ-ŋgɨ-za-ttə</ta>
            <ta e="T130" id="Seg_2034" s="T129">sɨr</ta>
            <ta e="T131" id="Seg_2035" s="T130">tʼölʼa-ze</ta>
            <ta e="T132" id="Seg_2036" s="T131">täp-qi-m</ta>
            <ta e="T133" id="Seg_2037" s="T132">qwärɣə</ta>
            <ta e="T134" id="Seg_2038" s="T133">assə</ta>
            <ta e="T135" id="Seg_2039" s="T134">laqqə-r-ɨ-zɨ-t</ta>
            <ta e="T136" id="Seg_2040" s="T135">potpol-qən</ta>
            <ta e="T137" id="Seg_2041" s="T136">qwärɣə</ta>
            <ta e="T138" id="Seg_2042" s="T137">hozʼain</ta>
            <ta e="T139" id="Seg_2043" s="T138">äː-sa-n</ta>
            <ta e="T140" id="Seg_2044" s="T139">niː-n</ta>
            <ta e="T141" id="Seg_2045" s="T140">äː-ssa-n</ta>
            <ta e="T142" id="Seg_2046" s="T141">saːq</ta>
            <ta e="T143" id="Seg_2047" s="T142">qwäl-ɨ-n</ta>
            <ta e="T144" id="Seg_2048" s="T143">üːr</ta>
            <ta e="T145" id="Seg_2049" s="T144">saq-ə-t-pədi</ta>
            <ta e="T146" id="Seg_2050" s="T145">wätʼtʼe</ta>
            <ta e="T147" id="Seg_2051" s="T146">mʼödə</ta>
            <ta e="T148" id="Seg_2052" s="T147">saːq-ɨ-m</ta>
            <ta e="T149" id="Seg_2053" s="T148">saq-ə-t-pədi</ta>
            <ta e="T150" id="Seg_2054" s="T149">wädʼi-m</ta>
            <ta e="T151" id="Seg_2055" s="T150">täp</ta>
            <ta e="T152" id="Seg_2056" s="T151">qamǯi-mba-t</ta>
            <ta e="T153" id="Seg_2057" s="T152">qäːl-qɨl-ba-t</ta>
            <ta e="T154" id="Seg_2058" s="T153">a</ta>
            <ta e="T155" id="Seg_2059" s="T154">qwäl-ɨ-n</ta>
            <ta e="T156" id="Seg_2060" s="T155">üːr-ɨ-m</ta>
            <ta e="T157" id="Seg_2061" s="T156">i</ta>
            <ta e="T158" id="Seg_2062" s="T157">mʼödə-m</ta>
            <ta e="T159" id="Seg_2063" s="T158">qwärɣə</ta>
            <ta e="T160" id="Seg_2064" s="T159">üt-pa-t</ta>
            <ta e="T161" id="Seg_2065" s="T160">aːm-ba-t</ta>
            <ta e="T162" id="Seg_2066" s="T161">suru-lʼdʼi</ta>
            <ta e="T163" id="Seg_2067" s="T162">qu-la</ta>
            <ta e="T164" id="Seg_2068" s="T163">tüː-wa-ttə</ta>
            <ta e="T165" id="Seg_2069" s="T164">maːd-ɨ-m</ta>
            <ta e="T166" id="Seg_2070" s="T165">assə</ta>
            <ta e="T167" id="Seg_2071" s="T166">qosolǯi-q-wa-ttə</ta>
            <ta e="T168" id="Seg_2072" s="T167">qai-ŋgo</ta>
            <ta e="T169" id="Seg_2073" s="T168">da-qqa</ta>
            <ta e="T170" id="Seg_2074" s="T169">läru-mbɨ-sɨ-n</ta>
            <ta e="T171" id="Seg_2075" s="T170">äː-a-ttə</ta>
            <ta e="T172" id="Seg_2076" s="T171">oqqɨr</ta>
            <ta e="T173" id="Seg_2077" s="T172">suru-lʼdʼi</ta>
            <ta e="T174" id="Seg_2078" s="T173">qum</ta>
            <ta e="T175" id="Seg_2079" s="T174">maːt-tə</ta>
            <ta e="T176" id="Seg_2080" s="T175">sär-na</ta>
            <ta e="T177" id="Seg_2081" s="T176">maːdə</ta>
            <ta e="T178" id="Seg_2082" s="T177">sündʼe-qɨn</ta>
            <ta e="T179" id="Seg_2083" s="T178">pür-u-ŋ</ta>
            <ta e="T180" id="Seg_2084" s="T179">palʼdʼö-lʼeː-a-n</ta>
            <ta e="T181" id="Seg_2085" s="T180">qujqan</ta>
            <ta e="T182" id="Seg_2086" s="T181">täːrbɨ-lʼe</ta>
            <ta e="T183" id="Seg_2087" s="T182">nɨ-ngeː-e-n</ta>
            <ta e="T184" id="Seg_2088" s="T183">qaj-da-qa</ta>
            <ta e="T185" id="Seg_2089" s="T184">potpol-qən</ta>
            <ta e="T186" id="Seg_2090" s="T185">rüqo-nʼ-nʼe</ta>
            <ta e="T187" id="Seg_2091" s="T186">suru-lʼdʼi</ta>
            <ta e="T188" id="Seg_2092" s="T187">qum</ta>
            <ta e="T189" id="Seg_2093" s="T188">ündɨ-zäɨ-t</ta>
            <ta e="T190" id="Seg_2094" s="T189">qoːsolǯi-t</ta>
            <ta e="T191" id="Seg_2095" s="T190">potpol-qɨn</ta>
            <ta e="T192" id="Seg_2096" s="T191">e-i-ŋ</ta>
            <ta e="T193" id="Seg_2097" s="T192">qwärɣa</ta>
            <ta e="T194" id="Seg_2098" s="T193">ponä</ta>
            <ta e="T195" id="Seg_2099" s="T194">čanǯi-ŋ</ta>
            <ta e="T196" id="Seg_2100" s="T195">lʼäqa-lɨ-qɨndə</ta>
            <ta e="T197" id="Seg_2101" s="T196">tʼärɨ-n</ta>
            <ta e="T198" id="Seg_2102" s="T197">potpol-qɨn</ta>
            <ta e="T199" id="Seg_2103" s="T198">ippa</ta>
            <ta e="T200" id="Seg_2104" s="T199">qwärɣə</ta>
            <ta e="T201" id="Seg_2105" s="T200">qwärqe-lʼdi</ta>
            <ta e="T202" id="Seg_2106" s="T201">qu-la</ta>
            <ta e="T203" id="Seg_2107" s="T202">ser-na-ttə</ta>
            <ta e="T204" id="Seg_2108" s="T203">maːt-tə</ta>
            <ta e="T205" id="Seg_2109" s="T204">qätta-ttə</ta>
            <ta e="T206" id="Seg_2110" s="T205">potpɨl-ɨ-m</ta>
            <ta e="T207" id="Seg_2111" s="T206">ponä</ta>
            <ta e="T208" id="Seg_2112" s="T207">čanǯa-ttə</ta>
            <ta e="T209" id="Seg_2113" s="T208">maːta-m-də</ta>
            <ta e="T210" id="Seg_2114" s="T209">qätta-ttə</ta>
            <ta e="T211" id="Seg_2115" s="T210">oqqɨ</ta>
            <ta e="T212" id="Seg_2116" s="T211">lʼew-ɨ-m</ta>
            <ta e="T213" id="Seg_2117" s="T212">qaqɨn-na-ttə</ta>
            <ta e="T214" id="Seg_2118" s="T213">qwärqɨ-m</ta>
            <ta e="T215" id="Seg_2119" s="T214">potpol-qɨn</ta>
            <ta e="T216" id="Seg_2120" s="T215">qolʼdʼä-ttə</ta>
            <ta e="T217" id="Seg_2121" s="T216">suru-lʼdʼi</ta>
            <ta e="T218" id="Seg_2122" s="T217">qu-la</ta>
            <ta e="T219" id="Seg_2123" s="T218">assə</ta>
            <ta e="T220" id="Seg_2124" s="T219">naːr-ɨ-ŋ</ta>
            <ta e="T221" id="Seg_2125" s="T220">tättɨ-ŋ</ta>
            <ta e="T222" id="Seg_2126" s="T221">tʼätča-ttə</ta>
            <ta e="T223" id="Seg_2127" s="T222">täp</ta>
            <ta e="T224" id="Seg_2128" s="T223">i</ta>
            <ta e="T225" id="Seg_2129" s="T224">qaːrɨ-s-sä</ta>
            <ta e="T226" id="Seg_2130" s="T225">nännä</ta>
            <ta e="T227" id="Seg_2131" s="T226">suːrum</ta>
            <ta e="T228" id="Seg_2132" s="T227">qujqa-lǯi-ŋ</ta>
            <ta e="T229" id="Seg_2133" s="T228">šɨttə</ta>
            <ta e="T230" id="Seg_2134" s="T229">lʼew-ɨ-m</ta>
            <ta e="T231" id="Seg_2135" s="T230">qaqən-na-ttə</ta>
            <ta e="T232" id="Seg_2136" s="T231">manǯi-mba-tt</ta>
            <ta e="T233" id="Seg_2137" s="T232">qwärɣə</ta>
            <ta e="T234" id="Seg_2138" s="T233">qu-mbaː</ta>
            <ta e="T235" id="Seg_2139" s="T234">täb-ɨ-m</ta>
            <ta e="T236" id="Seg_2140" s="T235">innä</ta>
            <ta e="T237" id="Seg_2141" s="T236">sabɨ-nʼä-ttə</ta>
            <ta e="T238" id="Seg_2142" s="T237">qobɨ-m-də</ta>
            <ta e="T239" id="Seg_2143" s="T238">qɨr-a-ttə</ta>
            <ta e="T240" id="Seg_2144" s="T239">wädʼi-m-də</ta>
            <ta e="T241" id="Seg_2145" s="T240">toː</ta>
            <ta e="T242" id="Seg_2146" s="T241">tʼätča-ttə</ta>
            <ta e="T243" id="Seg_2147" s="T242">wädʼi-t</ta>
            <ta e="T244" id="Seg_2148" s="T243">äː-sa-n</ta>
            <ta e="T245" id="Seg_2149" s="T244">säqə</ta>
            <ta e="T246" id="Seg_2150" s="T245">assə</ta>
            <ta e="T247" id="Seg_2151" s="T246">qappərɨ-mbädi</ta>
            <ta e="T248" id="Seg_2152" s="T247">nännä</ta>
            <ta e="T249" id="Seg_2153" s="T248">qwä-lewle</ta>
            <ta e="T250" id="Seg_2154" s="T249">suru-lʼdi</ta>
            <ta e="T251" id="Seg_2155" s="T250">quː-la</ta>
            <ta e="T252" id="Seg_2156" s="T251">äwɨ-m-dä</ta>
            <ta e="T253" id="Seg_2157" s="T252">qo-mbɨ-za-ttə</ta>
            <ta e="T254" id="Seg_2158" s="T253">nʼärnä-n</ta>
            <ta e="T255" id="Seg_2159" s="T254">nä-m-də</ta>
            <ta e="T256" id="Seg_2160" s="T255">na</ta>
            <ta e="T257" id="Seg_2161" s="T256">qwät-pɨdi</ta>
            <ta e="T258" id="Seg_2162" s="T257">qwärɣə</ta>
            <ta e="T259" id="Seg_2163" s="T258">am-ɨ-ba-t</ta>
            <ta e="T260" id="Seg_2164" s="T259">tʼeːlɨ-n</ta>
            <ta e="T261" id="Seg_2165" s="T260">suːru-lʼdʼe-qɨn</ta>
            <ta e="T262" id="Seg_2166" s="T261">äːwɨ-m-də</ta>
            <ta e="T263" id="Seg_2167" s="T262">nändɨ-zä</ta>
            <ta e="T264" id="Seg_2168" s="T263">iː-də</ta>
            <ta e="T265" id="Seg_2169" s="T264">ilʼlʼɨ-qqu-s</ta>
            <ta e="T266" id="Seg_2170" s="T265">piːrʼä-qən</ta>
            <ta e="T267" id="Seg_2171" s="T266">täp</ta>
            <ta e="T268" id="Seg_2172" s="T267">qwäs-sɨ</ta>
            <ta e="T269" id="Seg_2173" s="T268">qra-ndə</ta>
            <ta e="T270" id="Seg_2174" s="T269">qaj-qən</ta>
            <ta e="T271" id="Seg_2175" s="T270">ilɨ-s</ta>
            <ta e="T272" id="Seg_2176" s="T271">äwɨ-t</ta>
            <ta e="T273" id="Seg_2177" s="T272">täb-ɨ-n</ta>
            <ta e="T274" id="Seg_2178" s="T273">obɨ-t</ta>
            <ta e="T275" id="Seg_2179" s="T274">täp</ta>
            <ta e="T276" id="Seg_2180" s="T275">nilʼdʼi-n</ta>
            <ta e="T277" id="Seg_2181" s="T276">tʼärɨ-s</ta>
            <ta e="T278" id="Seg_2182" s="T277">man</ta>
            <ta e="T279" id="Seg_2183" s="T278">bə</ta>
            <ta e="T280" id="Seg_2184" s="T279">assə</ta>
            <ta e="T281" id="Seg_2185" s="T280">qwät-nä-w</ta>
            <ta e="T282" id="Seg_2186" s="T281">taw</ta>
            <ta e="T283" id="Seg_2187" s="T282">qwärɣɨ-m</ta>
            <ta e="T284" id="Seg_2188" s="T283">a</ta>
            <ta e="T285" id="Seg_2189" s="T284">tʼädɨ-nä-w</ta>
            <ta e="T286" id="Seg_2190" s="T285">äwe-nn-ɨ</ta>
            <ta e="T287" id="Seg_2191" s="T286">maːd-ə-m</ta>
            <ta e="T288" id="Seg_2192" s="T287">oqqɨ-mɨqɨn</ta>
            <ta e="T289" id="Seg_2193" s="T288">qwärɣə-zä</ta>
            <ta e="T290" id="Seg_2194" s="T289">man</ta>
            <ta e="T291" id="Seg_2195" s="T290">nʼärnä</ta>
            <ta e="T292" id="Seg_2196" s="T291">qwärɣɨ-la-m</ta>
            <ta e="T293" id="Seg_2197" s="T292">qwät-qweː-nǯa-w</ta>
            <ta e="T294" id="Seg_2198" s="T293">tʼätǯi-qwe-nǯa-w</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2199" s="T0">eː-sɨ-ŋ</ta>
            <ta e="T2" id="Seg_2200" s="T1">Мarkovo-qən</ta>
            <ta e="T3" id="Seg_2201" s="T2">nɨtʼan</ta>
            <ta e="T4" id="Seg_2202" s="T3">maǯə-qən</ta>
            <ta e="T5" id="Seg_2203" s="T4">eː-sɨ-ŋ</ta>
            <ta e="T6" id="Seg_2204" s="T5">okkɨr</ta>
            <ta e="T7" id="Seg_2205" s="T6">eːde</ta>
            <ta e="T8" id="Seg_2206" s="T7">nim-t</ta>
            <ta e="T9" id="Seg_2207" s="T8">tap-ɨ-n</ta>
            <ta e="T10" id="Seg_2208" s="T9">Мarkovo</ta>
            <ta e="T11" id="Seg_2209" s="T10">nɨtʼan</ta>
            <ta e="T12" id="Seg_2210" s="T11">ilɨ-kku-sɨ</ta>
            <ta e="T13" id="Seg_2211" s="T12">paja</ta>
            <ta e="T14" id="Seg_2212" s="T13">näde-se</ta>
            <ta e="T15" id="Seg_2213" s="T14">ira-t</ta>
            <ta e="T16" id="Seg_2214" s="T15">tap-ɨ-nan</ta>
            <ta e="T17" id="Seg_2215" s="T16">čaŋgu-sɨ</ta>
            <ta e="T18" id="Seg_2216" s="T17">ilɨ-kku-sɨ</ta>
            <ta e="T19" id="Seg_2217" s="T18">ontɨ</ta>
            <ta e="T20" id="Seg_2218" s="T19">nʼaja-j-kku-sɨ</ta>
            <ta e="T21" id="Seg_2219" s="T20">čopər-ɨ-j-kku-sɨ</ta>
            <ta e="T22" id="Seg_2220" s="T21">sväŋgə-j-kku-sɨ</ta>
            <ta e="T23" id="Seg_2221" s="T22">okkɨr-ɨ-k</ta>
            <ta e="T24" id="Seg_2222" s="T23">ara-n</ta>
            <ta e="T25" id="Seg_2223" s="T24">näde</ta>
            <ta e="T26" id="Seg_2224" s="T25">qwən-ŋɨ</ta>
            <ta e="T27" id="Seg_2225" s="T26">pemɨ-gu</ta>
            <ta e="T28" id="Seg_2226" s="T27">awa-t</ta>
            <ta e="T29" id="Seg_2227" s="T28">eː-kku-sɨ</ta>
            <ta e="T30" id="Seg_2228" s="T29">maːt-qən</ta>
            <ta e="T31" id="Seg_2229" s="T30">qarɨ-qɨnnɨ</ta>
            <ta e="T32" id="Seg_2230" s="T31">to</ta>
            <ta e="T33" id="Seg_2231" s="T32">üːtǝ-ndɨ</ta>
            <ta e="T34" id="Seg_2232" s="T33">tap</ta>
            <ta e="T35" id="Seg_2233" s="T34">paldʼu-sɨ</ta>
            <ta e="T36" id="Seg_2234" s="T35">nʼaja-j-le</ta>
            <ta e="T37" id="Seg_2235" s="T36">tʼeːlɨ-n</ta>
            <ta e="T38" id="Seg_2236" s="T37">paldʼu-lewle</ta>
            <ta e="T39" id="Seg_2237" s="T38">üːtǝ-mɨ-qən</ta>
            <ta e="T40" id="Seg_2238" s="T39">laqǝ-ŋɨ</ta>
            <ta e="T41" id="Seg_2239" s="T40">maːt-qɨntɨ</ta>
            <ta e="T42" id="Seg_2240" s="T41">tʼumbɨ-k</ta>
            <ta e="T43" id="Seg_2241" s="T42">ilʼi</ta>
            <ta e="T44" id="Seg_2242" s="T43">moqə-%%</ta>
            <ta e="T45" id="Seg_2243" s="T44">čaːǯɨ-mbɨ</ta>
            <ta e="T46" id="Seg_2244" s="T45">qotä</ta>
            <ta e="T47" id="Seg_2245" s="T46">maːt-qɨntɨ</ta>
            <ta e="T48" id="Seg_2246" s="T47">tʼeːlɨ-qɨntɨ</ta>
            <ta e="T49" id="Seg_2247" s="T48">tap-ɨ-m</ta>
            <ta e="T50" id="Seg_2248" s="T49">qɨːrɨ-mbɨ-tɨ</ta>
            <ta e="T51" id="Seg_2249" s="T50">qwärɣa</ta>
            <ta e="T52" id="Seg_2250" s="T51">neː-lʼ-qum-n</ta>
            <ta e="T53" id="Seg_2251" s="T52">wattə-un</ta>
            <ta e="T54" id="Seg_2252" s="T53">mitɨ-mbɨ-tɨ</ta>
            <ta e="T55" id="Seg_2253" s="T54">üːtǝ-mɨqɨn</ta>
            <ta e="T56" id="Seg_2254" s="T55">suːrɨ-lʼdi</ta>
            <ta e="T57" id="Seg_2255" s="T56">paja-m</ta>
            <ta e="T58" id="Seg_2256" s="T57">ora-l-mbɨ-tɨ</ta>
            <ta e="T59" id="Seg_2257" s="T58">qwat-mbɨ-tɨ</ta>
            <ta e="T60" id="Seg_2258" s="T59">amɨ-mbɨ-tɨ</ta>
            <ta e="T61" id="Seg_2259" s="T60">tap-ɨ-m</ta>
            <ta e="T62" id="Seg_2260" s="T61">qaj-mɨ-la-m-ndɨ</ta>
            <ta e="T63" id="Seg_2261" s="T62">nɨšqɨlgɨlʼ-mba-t</ta>
            <ta e="T64" id="Seg_2262" s="T63">%%-m-t</ta>
            <ta e="T65" id="Seg_2263" s="T64">teː</ta>
            <ta e="T66" id="Seg_2264" s="T65">tʼaǯe-mbɨ-tɨ</ta>
            <ta e="T67" id="Seg_2265" s="T66">qalɨ-mbɨ</ta>
            <ta e="T68" id="Seg_2266" s="T67">okkɨr</ta>
            <ta e="T69" id="Seg_2267" s="T68">topǝ-t</ta>
            <ta e="T70" id="Seg_2268" s="T69">taw</ta>
            <ta e="T71" id="Seg_2269" s="T70">näde-nannɨ</ta>
            <ta e="T72" id="Seg_2270" s="T71">üːtǝ-mɨn</ta>
            <ta e="T73" id="Seg_2271" s="T72">awa-t</ta>
            <ta e="T74" id="Seg_2272" s="T73">aädolǯə-sɨ</ta>
            <ta e="T75" id="Seg_2273" s="T74">näde</ta>
            <ta e="T76" id="Seg_2274" s="T75">ašša</ta>
            <ta e="T77" id="Seg_2275" s="T76">tü-mbɨ</ta>
            <ta e="T78" id="Seg_2276" s="T77">wesʼ</ta>
            <ta e="T79" id="Seg_2277" s="T78">pi-n</ta>
            <ta e="T80" id="Seg_2278" s="T79">awa-t</ta>
            <ta e="T81" id="Seg_2279" s="T80">neː-ndɨ-nqo</ta>
            <ta e="T82" id="Seg_2280" s="T81">tärba-le</ta>
            <ta e="T83" id="Seg_2281" s="T82">ašša</ta>
            <ta e="T84" id="Seg_2282" s="T83">qontə-sɨ</ta>
            <ta e="T85" id="Seg_2283" s="T84">tap</ta>
            <ta e="T86" id="Seg_2284" s="T85">ašša</ta>
            <ta e="T87" id="Seg_2285" s="T86">tʼeːlɨ-m-mbɨ-sɨ</ta>
            <ta e="T88" id="Seg_2286" s="T87">awa-t</ta>
            <ta e="T89" id="Seg_2287" s="T88">qwən-ŋɨ</ta>
            <ta e="T90" id="Seg_2288" s="T89">neː-m-ndɨ</ta>
            <ta e="T91" id="Seg_2289" s="T90">peː-r-gu</ta>
            <ta e="T92" id="Seg_2290" s="T91">telʼdʼelʼi</ta>
            <ta e="T93" id="Seg_2291" s="T92">qwən-mbɨdi</ta>
            <ta e="T94" id="Seg_2292" s="T93">wattə-mɨn</ta>
            <ta e="T95" id="Seg_2293" s="T94">awa-t</ta>
            <ta e="T96" id="Seg_2294" s="T95">čaːǯɨ-kku-sɨ</ta>
            <ta e="T97" id="Seg_2295" s="T96">maǯə-qən</ta>
            <ta e="T98" id="Seg_2296" s="T97">kuːn-nɨ</ta>
            <ta e="T99" id="Seg_2297" s="T98">ta</ta>
            <ta e="T100" id="Seg_2298" s="T99">sabɨ-sɨ-ŋ</ta>
            <ta e="T101" id="Seg_2299" s="T100">qwärqa</ta>
            <ta e="T102" id="Seg_2300" s="T101">oral-ŋɨ-tɨ</ta>
            <ta e="T103" id="Seg_2301" s="T102">paja-m</ta>
            <ta e="T104" id="Seg_2302" s="T103">am-ŋɨ-tɨ</ta>
            <ta e="T105" id="Seg_2303" s="T104">paja-n</ta>
            <ta e="T106" id="Seg_2304" s="T105">tüː-mbɨdi</ta>
            <ta e="T107" id="Seg_2305" s="T106">wattə-mɨn</ta>
            <ta e="T108" id="Seg_2306" s="T107">qwärqa</ta>
            <ta e="T109" id="Seg_2307" s="T108">čaːǯɨ</ta>
            <ta e="T110" id="Seg_2308" s="T109">paja-n</ta>
            <ta e="T111" id="Seg_2309" s="T110">maːt-ndɨ</ta>
            <ta e="T112" id="Seg_2310" s="T111">maːt-qən</ta>
            <ta e="T113" id="Seg_2311" s="T112">kutɨ -naj</ta>
            <ta e="T114" id="Seg_2312" s="T113">čaŋgu-sɨ</ta>
            <ta e="T115" id="Seg_2313" s="T114">maːta-m</ta>
            <ta e="T116" id="Seg_2314" s="T115">nʼoː-ŋɨ-tɨ</ta>
            <ta e="T117" id="Seg_2315" s="T116">šer-ŋɨ</ta>
            <ta e="T118" id="Seg_2316" s="T117">maːt-ndɨ</ta>
            <ta e="T119" id="Seg_2317" s="T118">maːt</ta>
            <ta e="T120" id="Seg_2318" s="T119">sündʼi-qɨn</ta>
            <ta e="T121" id="Seg_2319" s="T120">paldʼu-lewle</ta>
            <ta e="T122" id="Seg_2320" s="T121">ləm-ɨ-m</ta>
            <ta e="T123" id="Seg_2321" s="T122">innä</ta>
            <ta e="T124" id="Seg_2322" s="T123">watti-tɨ</ta>
            <ta e="T125" id="Seg_2323" s="T124">pat-ŋɨ</ta>
            <ta e="T126" id="Seg_2324" s="T125">potpol-ǯen</ta>
            <ta e="T127" id="Seg_2325" s="T126">sɨr-lʼ</ta>
            <ta e="T128" id="Seg_2326" s="T127">maːt-qən</ta>
            <ta e="T129" id="Seg_2327" s="T128">nɨ-ŋgɨ-sɨ-tɨt</ta>
            <ta e="T130" id="Seg_2328" s="T129">sɨr</ta>
            <ta e="T131" id="Seg_2329" s="T130">tʼölʼa-se</ta>
            <ta e="T132" id="Seg_2330" s="T131">tap-qi-m</ta>
            <ta e="T133" id="Seg_2331" s="T132">qwärqa</ta>
            <ta e="T134" id="Seg_2332" s="T133">ašša</ta>
            <ta e="T135" id="Seg_2333" s="T134">laqǝ-r-ɨ-sɨ-tɨ</ta>
            <ta e="T136" id="Seg_2334" s="T135">potpol-qən</ta>
            <ta e="T137" id="Seg_2335" s="T136">qwärqa</ta>
            <ta e="T138" id="Seg_2336" s="T137">hozʼain</ta>
            <ta e="T139" id="Seg_2337" s="T138">eː-sɨ-ŋ</ta>
            <ta e="T140" id="Seg_2338" s="T139">na-n</ta>
            <ta e="T141" id="Seg_2339" s="T140">eː-sɨ-ŋ</ta>
            <ta e="T142" id="Seg_2340" s="T141">saq</ta>
            <ta e="T143" id="Seg_2341" s="T142">quǝlǝ-ɨ-n</ta>
            <ta e="T144" id="Seg_2342" s="T143">ür</ta>
            <ta e="T145" id="Seg_2343" s="T144">saq-ɨ-tɨ-mbɨdi</ta>
            <ta e="T146" id="Seg_2344" s="T145">wätʼtʼi</ta>
            <ta e="T147" id="Seg_2345" s="T146">mʼödə</ta>
            <ta e="T148" id="Seg_2346" s="T147">saq-ɨ-m</ta>
            <ta e="T149" id="Seg_2347" s="T148">saq-ɨ-tɨ-mbɨdi</ta>
            <ta e="T150" id="Seg_2348" s="T149">wätʼtʼi-m</ta>
            <ta e="T151" id="Seg_2349" s="T150">tap</ta>
            <ta e="T152" id="Seg_2350" s="T151">qamǯə-mbɨ-tɨ</ta>
            <ta e="T153" id="Seg_2351" s="T152">qäːl-qɨl-mbɨ-tɨ</ta>
            <ta e="T154" id="Seg_2352" s="T153">a</ta>
            <ta e="T155" id="Seg_2353" s="T154">quǝlǝ-ɨ-n</ta>
            <ta e="T156" id="Seg_2354" s="T155">ür-ɨ-m</ta>
            <ta e="T157" id="Seg_2355" s="T156">i</ta>
            <ta e="T158" id="Seg_2356" s="T157">mʼödə-m</ta>
            <ta e="T159" id="Seg_2357" s="T158">qwärqa</ta>
            <ta e="T160" id="Seg_2358" s="T159">ütɨ-mbɨ-tɨ</ta>
            <ta e="T161" id="Seg_2359" s="T160">am-mbɨ-tɨ</ta>
            <ta e="T162" id="Seg_2360" s="T161">suːrɨ-lʼdi</ta>
            <ta e="T163" id="Seg_2361" s="T162">qum-la</ta>
            <ta e="T164" id="Seg_2362" s="T163">tüː-ŋɨ-tɨt</ta>
            <ta e="T165" id="Seg_2363" s="T164">maːt-ɨ-m</ta>
            <ta e="T166" id="Seg_2364" s="T165">ašša</ta>
            <ta e="T167" id="Seg_2365" s="T166">qosolǯi-kku-ŋɨ-tɨt</ta>
            <ta e="T168" id="Seg_2366" s="T167">qaj-nqo</ta>
            <ta e="T169" id="Seg_2367" s="T168">ta-ka</ta>
            <ta e="T170" id="Seg_2368" s="T169">larɨ-mbɨ-sɨ-ŋ</ta>
            <ta e="T171" id="Seg_2369" s="T170">eː-ɨ-tɨt</ta>
            <ta e="T172" id="Seg_2370" s="T171">okkɨr</ta>
            <ta e="T173" id="Seg_2371" s="T172">suːrɨ-lʼdi</ta>
            <ta e="T174" id="Seg_2372" s="T173">qum</ta>
            <ta e="T175" id="Seg_2373" s="T174">maːt-ndɨ</ta>
            <ta e="T176" id="Seg_2374" s="T175">šeːr-ŋɨ</ta>
            <ta e="T177" id="Seg_2375" s="T176">maːt</ta>
            <ta e="T178" id="Seg_2376" s="T177">sündʼi-qən</ta>
            <ta e="T179" id="Seg_2377" s="T178">pör-ɨ-k</ta>
            <ta e="T180" id="Seg_2378" s="T179">paldʼu-lɨ-ɨ-ŋ</ta>
            <ta e="T181" id="Seg_2379" s="T180">qujqaŋ</ta>
            <ta e="T182" id="Seg_2380" s="T181">tärba-le</ta>
            <ta e="T183" id="Seg_2381" s="T182">nɨ-ŋgɨ-ɨ-ŋ</ta>
            <ta e="T184" id="Seg_2382" s="T183">qaj-ta-ka</ta>
            <ta e="T185" id="Seg_2383" s="T184">potpol-qən</ta>
            <ta e="T186" id="Seg_2384" s="T185">rüqo-š-ŋɨ</ta>
            <ta e="T187" id="Seg_2385" s="T186">suːrɨ-lʼdi</ta>
            <ta e="T188" id="Seg_2386" s="T187">qum</ta>
            <ta e="T189" id="Seg_2387" s="T188">ündɨ-sɨ-tɨ</ta>
            <ta e="T190" id="Seg_2388" s="T189">qosolǯi-tɨ</ta>
            <ta e="T191" id="Seg_2389" s="T190">potpol-qən</ta>
            <ta e="T192" id="Seg_2390" s="T191">eː-ŋɨ-ŋ</ta>
            <ta e="T193" id="Seg_2391" s="T192">qwärqa</ta>
            <ta e="T194" id="Seg_2392" s="T193">poːne</ta>
            <ta e="T195" id="Seg_2393" s="T194">čanǯe-ŋ</ta>
            <ta e="T196" id="Seg_2394" s="T195">lʼäqa-la-qɨntɨ</ta>
            <ta e="T197" id="Seg_2395" s="T196">tʼarɨ-n</ta>
            <ta e="T198" id="Seg_2396" s="T197">potpol-qən</ta>
            <ta e="T199" id="Seg_2397" s="T198">ippi</ta>
            <ta e="T200" id="Seg_2398" s="T199">qwärqa</ta>
            <ta e="T201" id="Seg_2399" s="T200">qwärɣa-lʼdi</ta>
            <ta e="T202" id="Seg_2400" s="T201">qum-la</ta>
            <ta e="T203" id="Seg_2401" s="T202">šer-ŋɨ-tɨt</ta>
            <ta e="T204" id="Seg_2402" s="T203">maːt-ndɨ</ta>
            <ta e="T205" id="Seg_2403" s="T204">qätə-tɨt</ta>
            <ta e="T206" id="Seg_2404" s="T205">potpol-ɨ-m</ta>
            <ta e="T207" id="Seg_2405" s="T206">poːne</ta>
            <ta e="T208" id="Seg_2406" s="T207">čanǯe-tɨt</ta>
            <ta e="T209" id="Seg_2407" s="T208">maːta-m-ndɨ</ta>
            <ta e="T210" id="Seg_2408" s="T209">qätə-tɨt</ta>
            <ta e="T211" id="Seg_2409" s="T210">okkɨr</ta>
            <ta e="T212" id="Seg_2410" s="T211">ləm-ɨ-m</ta>
            <ta e="T213" id="Seg_2411" s="T212">qaːqɨl-ŋɨ-tɨt</ta>
            <ta e="T214" id="Seg_2412" s="T213">qwärɣa-m</ta>
            <ta e="T215" id="Seg_2413" s="T214">potpol-qən</ta>
            <ta e="T216" id="Seg_2414" s="T215">qolʼdʼi-tɨt</ta>
            <ta e="T217" id="Seg_2415" s="T216">suːrɨ-lʼdi</ta>
            <ta e="T218" id="Seg_2416" s="T217">qum-la</ta>
            <ta e="T219" id="Seg_2417" s="T218">ašša</ta>
            <ta e="T220" id="Seg_2418" s="T219">nakkɨr-ɨ-k</ta>
            <ta e="T221" id="Seg_2419" s="T220">tättɨ-k</ta>
            <ta e="T222" id="Seg_2420" s="T221">tʼatčəː-tɨt</ta>
            <ta e="T223" id="Seg_2421" s="T222">tap</ta>
            <ta e="T224" id="Seg_2422" s="T223">i</ta>
            <ta e="T225" id="Seg_2423" s="T224">qaːrɨ-sɨ-sɨ</ta>
            <ta e="T226" id="Seg_2424" s="T225">nɨːnɨ</ta>
            <ta e="T227" id="Seg_2425" s="T226">suːrǝm</ta>
            <ta e="T228" id="Seg_2426" s="T227">qujqa-lʼčǝ-ŋ</ta>
            <ta e="T229" id="Seg_2427" s="T228">šittə</ta>
            <ta e="T230" id="Seg_2428" s="T229">ləm-ɨ-m</ta>
            <ta e="T231" id="Seg_2429" s="T230">qaqən-ŋɨ-tɨt</ta>
            <ta e="T232" id="Seg_2430" s="T231">manǯu-mbɨ-tɨt</ta>
            <ta e="T233" id="Seg_2431" s="T232">qwärqa</ta>
            <ta e="T234" id="Seg_2432" s="T233">qo-mbɨ</ta>
            <ta e="T235" id="Seg_2433" s="T234">tap-ɨ-m</ta>
            <ta e="T236" id="Seg_2434" s="T235">innä</ta>
            <ta e="T237" id="Seg_2435" s="T236">sabɨ-ŋɨ-tɨt</ta>
            <ta e="T238" id="Seg_2436" s="T237">qobɨ-m-ndɨ</ta>
            <ta e="T239" id="Seg_2437" s="T238">qɨr-ŋɨ-tɨt</ta>
            <ta e="T240" id="Seg_2438" s="T239">wätʼtʼi-m-ndɨ</ta>
            <ta e="T241" id="Seg_2439" s="T240">teː</ta>
            <ta e="T242" id="Seg_2440" s="T241">tʼatčəː-tɨt</ta>
            <ta e="T243" id="Seg_2441" s="T242">wätʼtʼi-t</ta>
            <ta e="T244" id="Seg_2442" s="T243">eː-sɨ-ŋ</ta>
            <ta e="T245" id="Seg_2443" s="T244">säɣə</ta>
            <ta e="T246" id="Seg_2444" s="T245">ašša</ta>
            <ta e="T247" id="Seg_2445" s="T246">qapɨrɨ-mbɨdi</ta>
            <ta e="T248" id="Seg_2446" s="T247">nɨːnɨ</ta>
            <ta e="T249" id="Seg_2447" s="T248">qwən-lewle</ta>
            <ta e="T250" id="Seg_2448" s="T249">suːrɨ-lʼdi</ta>
            <ta e="T251" id="Seg_2449" s="T250">qum-la</ta>
            <ta e="T252" id="Seg_2450" s="T251">awa-m-ndɨ</ta>
            <ta e="T253" id="Seg_2451" s="T252">qo-mbɨ-sɨ-tɨt</ta>
            <ta e="T254" id="Seg_2452" s="T253">nʼannä-n</ta>
            <ta e="T255" id="Seg_2453" s="T254">neː-m-ndɨ</ta>
            <ta e="T256" id="Seg_2454" s="T255">na</ta>
            <ta e="T257" id="Seg_2455" s="T256">qwat-mbɨdi</ta>
            <ta e="T258" id="Seg_2456" s="T257">qwärqa</ta>
            <ta e="T259" id="Seg_2457" s="T258">am-ɨ-mbɨ-tɨ</ta>
            <ta e="T260" id="Seg_2458" s="T259">tʼeːlɨ-n</ta>
            <ta e="T261" id="Seg_2459" s="T260">suːrɨ-lʼdi-qɨnnɨ</ta>
            <ta e="T262" id="Seg_2460" s="T261">awa-m-ndɨ</ta>
            <ta e="T263" id="Seg_2461" s="T262">näde-se</ta>
            <ta e="T264" id="Seg_2462" s="T263">iː-t</ta>
            <ta e="T265" id="Seg_2463" s="T264">ilɨ-kku-sɨ</ta>
            <ta e="T266" id="Seg_2464" s="T265">Piːrʼä-qən</ta>
            <ta e="T267" id="Seg_2465" s="T266">tap</ta>
            <ta e="T268" id="Seg_2466" s="T267">qwən-sɨ</ta>
            <ta e="T269" id="Seg_2467" s="T268">qɨrra-ndɨ</ta>
            <ta e="T270" id="Seg_2468" s="T269">qaj-qən</ta>
            <ta e="T271" id="Seg_2469" s="T270">ilɨ-sɨ</ta>
            <ta e="T272" id="Seg_2470" s="T271">awa-t</ta>
            <ta e="T273" id="Seg_2471" s="T272">tap-ɨ-n</ta>
            <ta e="T274" id="Seg_2472" s="T273">oba-t</ta>
            <ta e="T275" id="Seg_2473" s="T274">tap</ta>
            <ta e="T276" id="Seg_2474" s="T275">nik-k</ta>
            <ta e="T277" id="Seg_2475" s="T276">tʼarɨ-sɨ</ta>
            <ta e="T278" id="Seg_2476" s="T277">man</ta>
            <ta e="T279" id="Seg_2477" s="T278">bɨ</ta>
            <ta e="T280" id="Seg_2478" s="T279">ašša</ta>
            <ta e="T281" id="Seg_2479" s="T280">qwat-ne-m</ta>
            <ta e="T282" id="Seg_2480" s="T281">taw</ta>
            <ta e="T283" id="Seg_2481" s="T282">qwärqa-m</ta>
            <ta e="T284" id="Seg_2482" s="T283">a</ta>
            <ta e="T285" id="Seg_2483" s="T284">čʼadɨ-ne-m</ta>
            <ta e="T286" id="Seg_2484" s="T285">awa-nan-lʼ</ta>
            <ta e="T287" id="Seg_2485" s="T286">maːt-ɨ-m</ta>
            <ta e="T288" id="Seg_2486" s="T287">okkɨr-mɨqɨn</ta>
            <ta e="T289" id="Seg_2487" s="T288">qwärqa-se</ta>
            <ta e="T290" id="Seg_2488" s="T289">man</ta>
            <ta e="T291" id="Seg_2489" s="T290">nʼannä</ta>
            <ta e="T292" id="Seg_2490" s="T291">qwärqa-la-m</ta>
            <ta e="T293" id="Seg_2491" s="T292">qwat-kku-nǯɨ-m</ta>
            <ta e="T294" id="Seg_2492" s="T293">tʼaǯe-kku-nǯɨ-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2493" s="T0">be-PST-3SG.S</ta>
            <ta e="T2" id="Seg_2494" s="T1">Markovo-LOC</ta>
            <ta e="T3" id="Seg_2495" s="T2">there</ta>
            <ta e="T4" id="Seg_2496" s="T3">taiga-LOC</ta>
            <ta e="T5" id="Seg_2497" s="T4">be-PST-3SG.S</ta>
            <ta e="T6" id="Seg_2498" s="T5">one</ta>
            <ta e="T7" id="Seg_2499" s="T6">village.[NOM]</ta>
            <ta e="T8" id="Seg_2500" s="T7">name.[NOM]-3SG</ta>
            <ta e="T9" id="Seg_2501" s="T8">he.NOM-EP-GEN</ta>
            <ta e="T10" id="Seg_2502" s="T9">Markovo.[NOM]</ta>
            <ta e="T11" id="Seg_2503" s="T10">there</ta>
            <ta e="T12" id="Seg_2504" s="T11">live-DUR-PST.[3SG.S]</ta>
            <ta e="T13" id="Seg_2505" s="T12">old.woman.[NOM]</ta>
            <ta e="T14" id="Seg_2506" s="T13">girl-COM</ta>
            <ta e="T15" id="Seg_2507" s="T14">husband-3SG</ta>
            <ta e="T16" id="Seg_2508" s="T15">he.NOM-EP-ADESS</ta>
            <ta e="T17" id="Seg_2509" s="T16">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T18" id="Seg_2510" s="T17">live-DUR-PST.[3SG.S]</ta>
            <ta e="T19" id="Seg_2511" s="T18">himself</ta>
            <ta e="T20" id="Seg_2512" s="T19">squirrel-CAP-DUR-PST.[3SG.S]</ta>
            <ta e="T21" id="Seg_2513" s="T20">berry-EP-CAP-DUR-PST.[3SG.S]</ta>
            <ta e="T22" id="Seg_2514" s="T21">nut-CAP-DUR-PST.[3SG.S]</ta>
            <ta e="T23" id="Seg_2515" s="T22">one-EP-ADVZ</ta>
            <ta e="T24" id="Seg_2516" s="T23">autumn-ADV.LOC</ta>
            <ta e="T25" id="Seg_2517" s="T24">girl.[NOM]</ta>
            <ta e="T26" id="Seg_2518" s="T25">go.away-CO.[3SG.S]</ta>
            <ta e="T27" id="Seg_2519" s="T26">hunt-INF</ta>
            <ta e="T28" id="Seg_2520" s="T27">mother-3SG</ta>
            <ta e="T29" id="Seg_2521" s="T28">be-DUR-PST.[3SG.S]</ta>
            <ta e="T30" id="Seg_2522" s="T29">house-LOC</ta>
            <ta e="T31" id="Seg_2523" s="T30">morning-EL</ta>
            <ta e="T32" id="Seg_2524" s="T31">up.to</ta>
            <ta e="T33" id="Seg_2525" s="T32">evening-ILL</ta>
            <ta e="T34" id="Seg_2526" s="T33">he.NOM</ta>
            <ta e="T35" id="Seg_2527" s="T34">go-PST.[3SG.S]</ta>
            <ta e="T36" id="Seg_2528" s="T35">squirrel-CAP-CVB</ta>
            <ta e="T37" id="Seg_2529" s="T36">day-ADV.LOC</ta>
            <ta e="T38" id="Seg_2530" s="T37">go-CVB</ta>
            <ta e="T39" id="Seg_2531" s="T38">evening-something-LOC</ta>
            <ta e="T40" id="Seg_2532" s="T39">move-CO.[3SG.S]</ta>
            <ta e="T41" id="Seg_2533" s="T40">tent-3SG.ILL</ta>
            <ta e="T42" id="Seg_2534" s="T41">long-ADVZ</ta>
            <ta e="T43" id="Seg_2535" s="T42">or</ta>
            <ta e="T44" id="Seg_2536" s="T43">back-%%</ta>
            <ta e="T45" id="Seg_2537" s="T44">go-PST.NAR.[3SG.S]</ta>
            <ta e="T46" id="Seg_2538" s="T45">back</ta>
            <ta e="T47" id="Seg_2539" s="T46">tent-3SG.ILL</ta>
            <ta e="T48" id="Seg_2540" s="T47">day-3SG.ILL</ta>
            <ta e="T49" id="Seg_2541" s="T48">he.NOM-EP-ACC</ta>
            <ta e="T50" id="Seg_2542" s="T49">%%-PST.NAR-3SG.O</ta>
            <ta e="T51" id="Seg_2543" s="T50">bear.[NOM]</ta>
            <ta e="T52" id="Seg_2544" s="T51">woman-ADJZ-human.being-GEN</ta>
            <ta e="T53" id="Seg_2545" s="T52">trace-PROL</ta>
            <ta e="T54" id="Seg_2546" s="T53">overhaul-PST.NAR-3SG.O</ta>
            <ta e="T55" id="Seg_2547" s="T54">evening-near</ta>
            <ta e="T56" id="Seg_2548" s="T55">hunt-PTCP.PRS</ta>
            <ta e="T57" id="Seg_2549" s="T56">old.woman-ACC</ta>
            <ta e="T58" id="Seg_2550" s="T57">hold-INCH-PST.NAR-3SG.O</ta>
            <ta e="T59" id="Seg_2551" s="T58">kill-PST.NAR-3SG.O</ta>
            <ta e="T60" id="Seg_2552" s="T59">burn-PST.NAR-3SG.O</ta>
            <ta e="T61" id="Seg_2553" s="T60">he.NOM-EP-ACC</ta>
            <ta e="T62" id="Seg_2554" s="T61">what-something-PL-ACC-OBL.3SG</ta>
            <ta e="T63" id="Seg_2555" s="T62">%%-PST.NAR-3SG.O</ta>
            <ta e="T64" id="Seg_2556" s="T63">%%-ACC-3SG</ta>
            <ta e="T65" id="Seg_2557" s="T64">away</ta>
            <ta e="T66" id="Seg_2558" s="T65">throw-PST.NAR-3SG.O</ta>
            <ta e="T67" id="Seg_2559" s="T66">stay-PST.NAR.[3SG.S]</ta>
            <ta e="T68" id="Seg_2560" s="T67">one</ta>
            <ta e="T69" id="Seg_2561" s="T68">leg-3SG</ta>
            <ta e="T70" id="Seg_2562" s="T69">this</ta>
            <ta e="T71" id="Seg_2563" s="T70">girl-EL.AN</ta>
            <ta e="T72" id="Seg_2564" s="T71">evening-PROL</ta>
            <ta e="T73" id="Seg_2565" s="T72">mother-3SG</ta>
            <ta e="T74" id="Seg_2566" s="T73">wait-PST.[3SG.S]</ta>
            <ta e="T75" id="Seg_2567" s="T74">girl.[NOM]</ta>
            <ta e="T76" id="Seg_2568" s="T75">NEG</ta>
            <ta e="T77" id="Seg_2569" s="T76">come-PST.NAR.[3SG.S]</ta>
            <ta e="T78" id="Seg_2570" s="T77">all</ta>
            <ta e="T79" id="Seg_2571" s="T78">night-ADV.LOC</ta>
            <ta e="T80" id="Seg_2572" s="T79">mother-3SG</ta>
            <ta e="T81" id="Seg_2573" s="T80">daughter-OBL.3SG-TRL</ta>
            <ta e="T82" id="Seg_2574" s="T81">think-CVB</ta>
            <ta e="T83" id="Seg_2575" s="T82">NEG</ta>
            <ta e="T84" id="Seg_2576" s="T83">sleep-PST.[3SG.S]</ta>
            <ta e="T85" id="Seg_2577" s="T84">still</ta>
            <ta e="T86" id="Seg_2578" s="T85">NEG</ta>
            <ta e="T87" id="Seg_2579" s="T86">day-TRL-HAB-PST.[3SG.S]</ta>
            <ta e="T88" id="Seg_2580" s="T87">mother-3SG</ta>
            <ta e="T89" id="Seg_2581" s="T88">go.away-CO.[3SG.S]</ta>
            <ta e="T90" id="Seg_2582" s="T89">daughter-ACC-OBL.3SG</ta>
            <ta e="T91" id="Seg_2583" s="T90">look.for-FRQ-INF</ta>
            <ta e="T92" id="Seg_2584" s="T91">night</ta>
            <ta e="T93" id="Seg_2585" s="T92">go.away-PTCP.PST</ta>
            <ta e="T94" id="Seg_2586" s="T93">trace-PROL</ta>
            <ta e="T95" id="Seg_2587" s="T94">mother-3SG</ta>
            <ta e="T96" id="Seg_2588" s="T95">go-DUR-PST.[3SG.S]</ta>
            <ta e="T97" id="Seg_2589" s="T96">taiga-LOC</ta>
            <ta e="T98" id="Seg_2590" s="T97">where-ADV.EL</ta>
            <ta e="T99" id="Seg_2591" s="T98">INDEF</ta>
            <ta e="T100" id="Seg_2592" s="T99">jump.out-PST-3SG.S</ta>
            <ta e="T101" id="Seg_2593" s="T100">bear.[NOM]</ta>
            <ta e="T102" id="Seg_2594" s="T101">catch-CO-3SG.O</ta>
            <ta e="T103" id="Seg_2595" s="T102">old.woman-ACC</ta>
            <ta e="T104" id="Seg_2596" s="T103">eat-CO-3SG.O</ta>
            <ta e="T105" id="Seg_2597" s="T104">old.woman-GEN</ta>
            <ta e="T106" id="Seg_2598" s="T105">come-PTCP.PST</ta>
            <ta e="T107" id="Seg_2599" s="T106">trace-PROL</ta>
            <ta e="T108" id="Seg_2600" s="T107">bear</ta>
            <ta e="T109" id="Seg_2601" s="T108">go.[3SG.S]</ta>
            <ta e="T110" id="Seg_2602" s="T109">old.woman-GEN</ta>
            <ta e="T111" id="Seg_2603" s="T110">house-ILL</ta>
            <ta e="T112" id="Seg_2604" s="T111">house-LOC</ta>
            <ta e="T113" id="Seg_2605" s="T112">who-EMPH</ta>
            <ta e="T114" id="Seg_2606" s="T113">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T115" id="Seg_2607" s="T114">door-ACC</ta>
            <ta e="T116" id="Seg_2608" s="T115">open-CO-3SG.O</ta>
            <ta e="T117" id="Seg_2609" s="T116">come.in-CO.[3SG.S]</ta>
            <ta e="T118" id="Seg_2610" s="T117">house-ILL</ta>
            <ta e="T119" id="Seg_2611" s="T118">house.[NOM]</ta>
            <ta e="T120" id="Seg_2612" s="T119">inside-LOC</ta>
            <ta e="T121" id="Seg_2613" s="T120">go-CVB</ta>
            <ta e="T122" id="Seg_2614" s="T121">board-EP-ACC</ta>
            <ta e="T123" id="Seg_2615" s="T122">up</ta>
            <ta e="T124" id="Seg_2616" s="T123">lift-3SG.O</ta>
            <ta e="T125" id="Seg_2617" s="T124">go.down-CO.[3SG.S]</ta>
            <ta e="T126" id="Seg_2618" s="T125">cellar-%%</ta>
            <ta e="T127" id="Seg_2619" s="T126">cow-ADJZ</ta>
            <ta e="T128" id="Seg_2620" s="T127">house-LOC</ta>
            <ta e="T129" id="Seg_2621" s="T128">stand-DRV-PST-3PL</ta>
            <ta e="T130" id="Seg_2622" s="T129">cow.[NOM]</ta>
            <ta e="T131" id="Seg_2623" s="T130">calf-COM</ta>
            <ta e="T132" id="Seg_2624" s="T131">he.NOM-DU-ACC</ta>
            <ta e="T133" id="Seg_2625" s="T132">bear.[NOM]</ta>
            <ta e="T134" id="Seg_2626" s="T133">NEG</ta>
            <ta e="T135" id="Seg_2627" s="T134">touch-FRQ-EP-PST-3SG.O</ta>
            <ta e="T136" id="Seg_2628" s="T135">cellar-LOC</ta>
            <ta e="T137" id="Seg_2629" s="T136">bear.[NOM]</ta>
            <ta e="T138" id="Seg_2630" s="T137">master.[NOM]</ta>
            <ta e="T139" id="Seg_2631" s="T138">be-PST-3SG.S</ta>
            <ta e="T140" id="Seg_2632" s="T139">this-ADV.LOC</ta>
            <ta e="T141" id="Seg_2633" s="T140">be-PST-3SG.S</ta>
            <ta e="T142" id="Seg_2634" s="T141">salt.[NOM]</ta>
            <ta e="T143" id="Seg_2635" s="T142">fish-EP-GEN</ta>
            <ta e="T144" id="Seg_2636" s="T143">fat.[NOM]</ta>
            <ta e="T145" id="Seg_2637" s="T144">salt-EP-TR-PTCP.PST</ta>
            <ta e="T146" id="Seg_2638" s="T145">meat.[NOM]</ta>
            <ta e="T147" id="Seg_2639" s="T146">honey.[NOM]</ta>
            <ta e="T148" id="Seg_2640" s="T147">salt-EP-ACC</ta>
            <ta e="T149" id="Seg_2641" s="T148">salt-EP-TR-PTCP.PST</ta>
            <ta e="T150" id="Seg_2642" s="T149">meat-ACC</ta>
            <ta e="T151" id="Seg_2643" s="T150">he.NOM</ta>
            <ta e="T152" id="Seg_2644" s="T151">pour-PST.NAR-3SG.O</ta>
            <ta e="T153" id="Seg_2645" s="T152">scatter-MULO-PST.NAR-3SG.O</ta>
            <ta e="T154" id="Seg_2646" s="T153">but</ta>
            <ta e="T155" id="Seg_2647" s="T154">fish-EP-GEN</ta>
            <ta e="T156" id="Seg_2648" s="T155">fat-EP-ACC</ta>
            <ta e="T157" id="Seg_2649" s="T156">and</ta>
            <ta e="T158" id="Seg_2650" s="T157">honey-ACC</ta>
            <ta e="T159" id="Seg_2651" s="T158">bear.[NOM]</ta>
            <ta e="T160" id="Seg_2652" s="T159">drink-PST.NAR-3SG.O</ta>
            <ta e="T161" id="Seg_2653" s="T160">eat-PST.NAR-3SG.O</ta>
            <ta e="T162" id="Seg_2654" s="T161">hunt-PTCP.PRS</ta>
            <ta e="T163" id="Seg_2655" s="T162">human.being-PL.[NOM]</ta>
            <ta e="T164" id="Seg_2656" s="T163">come-CO-3PL</ta>
            <ta e="T165" id="Seg_2657" s="T164">house-EP-ACC</ta>
            <ta e="T166" id="Seg_2658" s="T165">NEG</ta>
            <ta e="T167" id="Seg_2659" s="T166">discover-DUR-CO-3PL</ta>
            <ta e="T168" id="Seg_2660" s="T167">what-TRL</ta>
            <ta e="T169" id="Seg_2661" s="T168">INDEF-DIM</ta>
            <ta e="T170" id="Seg_2662" s="T169">be.afraid-HAB-PST-3SG.S</ta>
            <ta e="T171" id="Seg_2663" s="T170">be-EP-3PL</ta>
            <ta e="T172" id="Seg_2664" s="T171">one</ta>
            <ta e="T173" id="Seg_2665" s="T172">hunt-PTCP.PRS</ta>
            <ta e="T174" id="Seg_2666" s="T173">human.being.[NOM]</ta>
            <ta e="T175" id="Seg_2667" s="T174">house-ILL</ta>
            <ta e="T176" id="Seg_2668" s="T175">dress-CO.[3SG.S]</ta>
            <ta e="T177" id="Seg_2669" s="T176">house.[NOM]</ta>
            <ta e="T178" id="Seg_2670" s="T177">inside-LOC</ta>
            <ta e="T179" id="Seg_2671" s="T178">round-EP-ADVZ</ta>
            <ta e="T180" id="Seg_2672" s="T179">go-RES-EP-3SG.S</ta>
            <ta e="T181" id="Seg_2673" s="T180">silently</ta>
            <ta e="T182" id="Seg_2674" s="T181">think-CVB</ta>
            <ta e="T183" id="Seg_2675" s="T182">stand-DRV-EP-3SG.S</ta>
            <ta e="T184" id="Seg_2676" s="T183">what-INDEF-DIM</ta>
            <ta e="T185" id="Seg_2677" s="T184">cellar-LOC</ta>
            <ta e="T186" id="Seg_2678" s="T185">%%-US-CO.[3SG.S]</ta>
            <ta e="T187" id="Seg_2679" s="T186">hunt-PTCP.PRS</ta>
            <ta e="T188" id="Seg_2680" s="T187">human.being.[NOM]</ta>
            <ta e="T189" id="Seg_2681" s="T188">hear-PST-3SG.O</ta>
            <ta e="T190" id="Seg_2682" s="T189">discover-3SG.O</ta>
            <ta e="T191" id="Seg_2683" s="T190">cellar-LOC</ta>
            <ta e="T192" id="Seg_2684" s="T191">be-CO-3SG.S</ta>
            <ta e="T193" id="Seg_2685" s="T192">bear.[NOM]</ta>
            <ta e="T194" id="Seg_2686" s="T193">outwards</ta>
            <ta e="T195" id="Seg_2687" s="T194">go.out-3SG.S</ta>
            <ta e="T196" id="Seg_2688" s="T195">friend-PL-3SG.ILL</ta>
            <ta e="T197" id="Seg_2689" s="T196">say-3SG.S</ta>
            <ta e="T198" id="Seg_2690" s="T197">cellar-LOC</ta>
            <ta e="T199" id="Seg_2691" s="T198">lie.[3SG.S]</ta>
            <ta e="T200" id="Seg_2692" s="T199">bear.[NOM]</ta>
            <ta e="T201" id="Seg_2693" s="T200">bear-PTCP.PRS</ta>
            <ta e="T202" id="Seg_2694" s="T201">human.being-PL.[NOM]</ta>
            <ta e="T203" id="Seg_2695" s="T202">come.in-CO-3PL</ta>
            <ta e="T204" id="Seg_2696" s="T203">house-ILL</ta>
            <ta e="T205" id="Seg_2697" s="T204">beat-3PL</ta>
            <ta e="T206" id="Seg_2698" s="T205">cellar-EP-ACC</ta>
            <ta e="T207" id="Seg_2699" s="T206">outwards</ta>
            <ta e="T208" id="Seg_2700" s="T207">go.out-3PL</ta>
            <ta e="T209" id="Seg_2701" s="T208">door-ACC-OBL.3SG</ta>
            <ta e="T210" id="Seg_2702" s="T209">beat-3PL</ta>
            <ta e="T211" id="Seg_2703" s="T210">one</ta>
            <ta e="T212" id="Seg_2704" s="T211">board-EP-ACC</ta>
            <ta e="T213" id="Seg_2705" s="T212">ruin-CO-3PL</ta>
            <ta e="T214" id="Seg_2706" s="T213">bear-ACC</ta>
            <ta e="T215" id="Seg_2707" s="T214">cellar-LOC</ta>
            <ta e="T216" id="Seg_2708" s="T215">see-3PL</ta>
            <ta e="T217" id="Seg_2709" s="T216">hunt-PTCP.PRS</ta>
            <ta e="T218" id="Seg_2710" s="T217">human.being-PL.[NOM]</ta>
            <ta e="T219" id="Seg_2711" s="T218">NEG</ta>
            <ta e="T220" id="Seg_2712" s="T219">three-EP-ADVZ</ta>
            <ta e="T221" id="Seg_2713" s="T220">four-ADVZ</ta>
            <ta e="T222" id="Seg_2714" s="T221">shoot-3PL</ta>
            <ta e="T223" id="Seg_2715" s="T222">he.NOM</ta>
            <ta e="T224" id="Seg_2716" s="T223">and</ta>
            <ta e="T225" id="Seg_2717" s="T224">shout-PST-PST.[3SG.S]</ta>
            <ta e="T226" id="Seg_2718" s="T225">then</ta>
            <ta e="T227" id="Seg_2719" s="T226">wild.animal.[NOM]</ta>
            <ta e="T228" id="Seg_2720" s="T227">hush-PFV-3SG.S</ta>
            <ta e="T229" id="Seg_2721" s="T228">two</ta>
            <ta e="T230" id="Seg_2722" s="T229">board-EP-ACC</ta>
            <ta e="T231" id="Seg_2723" s="T230">%%-CO-3PL</ta>
            <ta e="T232" id="Seg_2724" s="T231">look-PST.NAR-3PL</ta>
            <ta e="T233" id="Seg_2725" s="T232">bear.[NOM]</ta>
            <ta e="T234" id="Seg_2726" s="T233">see-HAB.[3SG.S]</ta>
            <ta e="T235" id="Seg_2727" s="T234">he.NOM-EP-ACC</ta>
            <ta e="T236" id="Seg_2728" s="T235">up</ta>
            <ta e="T237" id="Seg_2729" s="T236">pull.out-CO-3PL</ta>
            <ta e="T238" id="Seg_2730" s="T237">skin-ACC-OBL.3SG</ta>
            <ta e="T239" id="Seg_2731" s="T238">tear.off-CO-3PL</ta>
            <ta e="T240" id="Seg_2732" s="T239">meat-ACC-OBL.3SG</ta>
            <ta e="T241" id="Seg_2733" s="T240">away</ta>
            <ta e="T242" id="Seg_2734" s="T241">throw-3PL</ta>
            <ta e="T243" id="Seg_2735" s="T242">meat-3SG</ta>
            <ta e="T244" id="Seg_2736" s="T243">be-PST-3SG.S</ta>
            <ta e="T245" id="Seg_2737" s="T244">black</ta>
            <ta e="T246" id="Seg_2738" s="T245">NEG</ta>
            <ta e="T247" id="Seg_2739" s="T246">become.fat-PTCP.PST</ta>
            <ta e="T248" id="Seg_2740" s="T247">then</ta>
            <ta e="T249" id="Seg_2741" s="T248">go.away-CVB</ta>
            <ta e="T250" id="Seg_2742" s="T249">hunt-PTCP.PRS</ta>
            <ta e="T251" id="Seg_2743" s="T250">human.being-PL.[NOM]</ta>
            <ta e="T252" id="Seg_2744" s="T251">mother-ACC-OBL.3SG</ta>
            <ta e="T253" id="Seg_2745" s="T252">see-HAB-PST-3PL</ta>
            <ta e="T254" id="Seg_2746" s="T253">forward-ADV.LOC</ta>
            <ta e="T255" id="Seg_2747" s="T254">daughter-ACC-OBL.3SG</ta>
            <ta e="T256" id="Seg_2748" s="T255">this</ta>
            <ta e="T257" id="Seg_2749" s="T256">kill-PTCP.PST</ta>
            <ta e="T258" id="Seg_2750" s="T257">bear.[NOM]</ta>
            <ta e="T259" id="Seg_2751" s="T258">eat-EP-PST.NAR-3SG.O</ta>
            <ta e="T260" id="Seg_2752" s="T259">day-ADV.LOC</ta>
            <ta e="T261" id="Seg_2753" s="T260">hunt-PTCP.PRS-EL</ta>
            <ta e="T262" id="Seg_2754" s="T261">mother-ACC-OBL.3SG</ta>
            <ta e="T263" id="Seg_2755" s="T262">girl-COM</ta>
            <ta e="T264" id="Seg_2756" s="T263">son-3SG</ta>
            <ta e="T265" id="Seg_2757" s="T264">live-DUR-PST.[3SG.S]</ta>
            <ta e="T266" id="Seg_2758" s="T265">Pirij-LOC</ta>
            <ta e="T267" id="Seg_2759" s="T266">he.NOM</ta>
            <ta e="T268" id="Seg_2760" s="T267">go.away-PST.[3SG.S]</ta>
            <ta e="T269" id="Seg_2761" s="T268">village-ILL</ta>
            <ta e="T270" id="Seg_2762" s="T269">what-LOC</ta>
            <ta e="T271" id="Seg_2763" s="T270">live-PST.[3SG.S]</ta>
            <ta e="T272" id="Seg_2764" s="T271">mother-3SG</ta>
            <ta e="T273" id="Seg_2765" s="T272">he.NOM-EP-GEN</ta>
            <ta e="T274" id="Seg_2766" s="T273">sister-3SG</ta>
            <ta e="T275" id="Seg_2767" s="T274">he.NOM</ta>
            <ta e="T276" id="Seg_2768" s="T275">so-ADVZ</ta>
            <ta e="T277" id="Seg_2769" s="T276">say-PST.[3SG.S]</ta>
            <ta e="T278" id="Seg_2770" s="T277">I.NOM</ta>
            <ta e="T279" id="Seg_2771" s="T278">if</ta>
            <ta e="T280" id="Seg_2772" s="T279">NEG</ta>
            <ta e="T281" id="Seg_2773" s="T280">kill-SUBJ-1SG.O</ta>
            <ta e="T282" id="Seg_2774" s="T281">this</ta>
            <ta e="T283" id="Seg_2775" s="T282">bear-ACC</ta>
            <ta e="T284" id="Seg_2776" s="T283">but</ta>
            <ta e="T285" id="Seg_2777" s="T284">burn-SUBJ-1SG.O</ta>
            <ta e="T286" id="Seg_2778" s="T285">mother-ADESS-ADJZ</ta>
            <ta e="T287" id="Seg_2779" s="T286">house-EP-ACC</ta>
            <ta e="T288" id="Seg_2780" s="T287">one-near</ta>
            <ta e="T289" id="Seg_2781" s="T288">bear-COM</ta>
            <ta e="T290" id="Seg_2782" s="T289">I.NOM</ta>
            <ta e="T291" id="Seg_2783" s="T290">forward</ta>
            <ta e="T292" id="Seg_2784" s="T291">bear-PL-ACC</ta>
            <ta e="T293" id="Seg_2785" s="T292">kill-DUR-FUT-1SG.O</ta>
            <ta e="T294" id="Seg_2786" s="T293">shoot-DUR-FUT-1SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2787" s="T0">быть-PST-3SG.S</ta>
            <ta e="T2" id="Seg_2788" s="T1">Марково-LOC</ta>
            <ta e="T3" id="Seg_2789" s="T2">там</ta>
            <ta e="T4" id="Seg_2790" s="T3">тайга-LOC</ta>
            <ta e="T5" id="Seg_2791" s="T4">быть-PST-3SG.S</ta>
            <ta e="T6" id="Seg_2792" s="T5">один</ta>
            <ta e="T7" id="Seg_2793" s="T6">деревня.[NOM]</ta>
            <ta e="T8" id="Seg_2794" s="T7">имя.[NOM]-3SG</ta>
            <ta e="T9" id="Seg_2795" s="T8">он.NOM-EP-GEN</ta>
            <ta e="T10" id="Seg_2796" s="T9">Марково.[NOM]</ta>
            <ta e="T11" id="Seg_2797" s="T10">там</ta>
            <ta e="T12" id="Seg_2798" s="T11">жить-DUR-PST.[3SG.S]</ta>
            <ta e="T13" id="Seg_2799" s="T12">старуха.[NOM]</ta>
            <ta e="T14" id="Seg_2800" s="T13">девушка-COM</ta>
            <ta e="T15" id="Seg_2801" s="T14">муж-3SG</ta>
            <ta e="T16" id="Seg_2802" s="T15">он.NOM-EP-ADESS</ta>
            <ta e="T17" id="Seg_2803" s="T16">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T18" id="Seg_2804" s="T17">жить-DUR-PST.[3SG.S]</ta>
            <ta e="T19" id="Seg_2805" s="T18">он.сам</ta>
            <ta e="T20" id="Seg_2806" s="T19">белка-CAP-DUR-PST.[3SG.S]</ta>
            <ta e="T21" id="Seg_2807" s="T20">ягода-EP-CAP-DUR-PST.[3SG.S]</ta>
            <ta e="T22" id="Seg_2808" s="T21">орех-CAP-DUR-PST.[3SG.S]</ta>
            <ta e="T23" id="Seg_2809" s="T22">один-EP-ADVZ</ta>
            <ta e="T24" id="Seg_2810" s="T23">осень-ADV.LOC</ta>
            <ta e="T25" id="Seg_2811" s="T24">девушка.[NOM]</ta>
            <ta e="T26" id="Seg_2812" s="T25">уйти-CO.[3SG.S]</ta>
            <ta e="T27" id="Seg_2813" s="T26">охотиться-INF</ta>
            <ta e="T28" id="Seg_2814" s="T27">мать-3SG</ta>
            <ta e="T29" id="Seg_2815" s="T28">быть-DUR-PST.[3SG.S]</ta>
            <ta e="T30" id="Seg_2816" s="T29">дом-LOC</ta>
            <ta e="T31" id="Seg_2817" s="T30">утро-ЕL</ta>
            <ta e="T32" id="Seg_2818" s="T31">до</ta>
            <ta e="T33" id="Seg_2819" s="T32">вечер-ILL</ta>
            <ta e="T34" id="Seg_2820" s="T33">он.NOM</ta>
            <ta e="T35" id="Seg_2821" s="T34">ходить-PST.[3SG.S]</ta>
            <ta e="T36" id="Seg_2822" s="T35">белка-CAP-CVB</ta>
            <ta e="T37" id="Seg_2823" s="T36">день-ADV.LOC</ta>
            <ta e="T38" id="Seg_2824" s="T37">ходить-CVB</ta>
            <ta e="T39" id="Seg_2825" s="T38">вечер-нечто-LOC</ta>
            <ta e="T40" id="Seg_2826" s="T39">двигаться-CO.[3SG.S]</ta>
            <ta e="T41" id="Seg_2827" s="T40">чум-3SG.ILL</ta>
            <ta e="T42" id="Seg_2828" s="T41">длинный-ADVZ</ta>
            <ta e="T43" id="Seg_2829" s="T42">или</ta>
            <ta e="T44" id="Seg_2830" s="T43">спина-%%</ta>
            <ta e="T45" id="Seg_2831" s="T44">ехать-PST.NAR.[3SG.S]</ta>
            <ta e="T46" id="Seg_2832" s="T45">назад</ta>
            <ta e="T47" id="Seg_2833" s="T46">чум-3SG.ILL</ta>
            <ta e="T48" id="Seg_2834" s="T47">день-3SG.ILL</ta>
            <ta e="T49" id="Seg_2835" s="T48">он.NOM-EP-ACC</ta>
            <ta e="T50" id="Seg_2836" s="T49">%%-PST.NAR-3SG.O</ta>
            <ta e="T51" id="Seg_2837" s="T50">медведь.[NOM]</ta>
            <ta e="T52" id="Seg_2838" s="T51">женщина-ADJZ-человек-GEN</ta>
            <ta e="T53" id="Seg_2839" s="T52">след-PROL</ta>
            <ta e="T54" id="Seg_2840" s="T53">догнать-PST.NAR-3SG.O</ta>
            <ta e="T55" id="Seg_2841" s="T54">вечер-у</ta>
            <ta e="T56" id="Seg_2842" s="T55">охотиться-PTCP.PRS</ta>
            <ta e="T57" id="Seg_2843" s="T56">старуха-ACC</ta>
            <ta e="T58" id="Seg_2844" s="T57">держать-INCH-PST.NAR-3SG.O</ta>
            <ta e="T59" id="Seg_2845" s="T58">убить-PST.NAR-3SG.O</ta>
            <ta e="T60" id="Seg_2846" s="T59">гореть-PST.NAR-3SG.O</ta>
            <ta e="T61" id="Seg_2847" s="T60">он.NOM-EP-ACC</ta>
            <ta e="T62" id="Seg_2848" s="T61">что-нечто-PL-ACC-OBL.3SG</ta>
            <ta e="T63" id="Seg_2849" s="T62">%%-PST.NAR-3SG.O</ta>
            <ta e="T64" id="Seg_2850" s="T63">%%-ACC-3SG</ta>
            <ta e="T65" id="Seg_2851" s="T64">прочь</ta>
            <ta e="T66" id="Seg_2852" s="T65">бросать-PST.NAR-3SG.O</ta>
            <ta e="T67" id="Seg_2853" s="T66">остаться-PST.NAR.[3SG.S]</ta>
            <ta e="T68" id="Seg_2854" s="T67">один</ta>
            <ta e="T69" id="Seg_2855" s="T68">нога-3SG</ta>
            <ta e="T70" id="Seg_2856" s="T69">этот</ta>
            <ta e="T71" id="Seg_2857" s="T70">девушка-EL.AN</ta>
            <ta e="T72" id="Seg_2858" s="T71">вечер-PROL</ta>
            <ta e="T73" id="Seg_2859" s="T72">мать-3SG</ta>
            <ta e="T74" id="Seg_2860" s="T73">ждать-PST.[3SG.S]</ta>
            <ta e="T75" id="Seg_2861" s="T74">девушка.[NOM]</ta>
            <ta e="T76" id="Seg_2862" s="T75">NEG</ta>
            <ta e="T77" id="Seg_2863" s="T76">прийти-PST.NAR.[3SG.S]</ta>
            <ta e="T78" id="Seg_2864" s="T77">весь</ta>
            <ta e="T79" id="Seg_2865" s="T78">ночь-ADV.LOC</ta>
            <ta e="T80" id="Seg_2866" s="T79">мать-3SG</ta>
            <ta e="T81" id="Seg_2867" s="T80">дочь-OBL.3SG-TRL</ta>
            <ta e="T82" id="Seg_2868" s="T81">думать-CVB</ta>
            <ta e="T83" id="Seg_2869" s="T82">NEG</ta>
            <ta e="T84" id="Seg_2870" s="T83">спать-PST.[3SG.S]</ta>
            <ta e="T85" id="Seg_2871" s="T84">всё.ещё</ta>
            <ta e="T86" id="Seg_2872" s="T85">NEG</ta>
            <ta e="T87" id="Seg_2873" s="T86">день-TRL-HAB-PST.[3SG.S]</ta>
            <ta e="T88" id="Seg_2874" s="T87">мать-3SG</ta>
            <ta e="T89" id="Seg_2875" s="T88">уйти-CO.[3SG.S]</ta>
            <ta e="T90" id="Seg_2876" s="T89">дочь-ACC-OBL.3SG</ta>
            <ta e="T91" id="Seg_2877" s="T90">искать-FRQ-INF</ta>
            <ta e="T92" id="Seg_2878" s="T91">вчерашний</ta>
            <ta e="T93" id="Seg_2879" s="T92">уйти-PTCP.PST</ta>
            <ta e="T94" id="Seg_2880" s="T93">след-PROL</ta>
            <ta e="T95" id="Seg_2881" s="T94">мать-3SG</ta>
            <ta e="T96" id="Seg_2882" s="T95">ехать-DUR-PST.[3SG.S]</ta>
            <ta e="T97" id="Seg_2883" s="T96">тайга-LOC</ta>
            <ta e="T98" id="Seg_2884" s="T97">где-ADV.EL</ta>
            <ta e="T99" id="Seg_2885" s="T98">INDEF</ta>
            <ta e="T100" id="Seg_2886" s="T99">выскочить-PST-3SG.S</ta>
            <ta e="T101" id="Seg_2887" s="T100">медведь.[NOM]</ta>
            <ta e="T102" id="Seg_2888" s="T101">поймать-CO-3SG.O</ta>
            <ta e="T103" id="Seg_2889" s="T102">старуха-ACC</ta>
            <ta e="T104" id="Seg_2890" s="T103">съесть-CO-3SG.O</ta>
            <ta e="T105" id="Seg_2891" s="T104">старуха-GEN</ta>
            <ta e="T106" id="Seg_2892" s="T105">прийти-PTCP.PST</ta>
            <ta e="T107" id="Seg_2893" s="T106">след-PROL</ta>
            <ta e="T108" id="Seg_2894" s="T107">медведь</ta>
            <ta e="T109" id="Seg_2895" s="T108">ехать.[3SG.S]</ta>
            <ta e="T110" id="Seg_2896" s="T109">старуха-GEN</ta>
            <ta e="T111" id="Seg_2897" s="T110">дом-ILL</ta>
            <ta e="T112" id="Seg_2898" s="T111">дом-LOC</ta>
            <ta e="T113" id="Seg_2899" s="T112">кто-EMPH</ta>
            <ta e="T114" id="Seg_2900" s="T113">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T115" id="Seg_2901" s="T114">дверь-ACC</ta>
            <ta e="T116" id="Seg_2902" s="T115">открыть-CO-3SG.O</ta>
            <ta e="T117" id="Seg_2903" s="T116">войти-CO.[3SG.S]</ta>
            <ta e="T118" id="Seg_2904" s="T117">дом-ILL</ta>
            <ta e="T119" id="Seg_2905" s="T118">дом.[NOM]</ta>
            <ta e="T120" id="Seg_2906" s="T119">нутро-LOC</ta>
            <ta e="T121" id="Seg_2907" s="T120">ходить-CVB</ta>
            <ta e="T122" id="Seg_2908" s="T121">доска-EP-ACC</ta>
            <ta e="T123" id="Seg_2909" s="T122">вверх</ta>
            <ta e="T124" id="Seg_2910" s="T123">поднять-3SG.O</ta>
            <ta e="T125" id="Seg_2911" s="T124">залезть-CO.[3SG.S]</ta>
            <ta e="T126" id="Seg_2912" s="T125">подпол-%%</ta>
            <ta e="T127" id="Seg_2913" s="T126">корова-ADJZ</ta>
            <ta e="T128" id="Seg_2914" s="T127">дом-LOC</ta>
            <ta e="T129" id="Seg_2915" s="T128">стоять-DRV-PST-3PL</ta>
            <ta e="T130" id="Seg_2916" s="T129">корова.[NOM]</ta>
            <ta e="T131" id="Seg_2917" s="T130">теленок-COM</ta>
            <ta e="T132" id="Seg_2918" s="T131">он.NOM-DU-ACC</ta>
            <ta e="T133" id="Seg_2919" s="T132">медведь.[NOM]</ta>
            <ta e="T134" id="Seg_2920" s="T133">NEG</ta>
            <ta e="T135" id="Seg_2921" s="T134">трогать-FRQ-EP-PST-3SG.O</ta>
            <ta e="T136" id="Seg_2922" s="T135">подпол-LOC</ta>
            <ta e="T137" id="Seg_2923" s="T136">медведь.[NOM]</ta>
            <ta e="T138" id="Seg_2924" s="T137">хозяин.[NOM]</ta>
            <ta e="T139" id="Seg_2925" s="T138">быть-PST-3SG.S</ta>
            <ta e="T140" id="Seg_2926" s="T139">этот-ADV.LOC</ta>
            <ta e="T141" id="Seg_2927" s="T140">быть-PST-3SG.S</ta>
            <ta e="T142" id="Seg_2928" s="T141">соль.[NOM]</ta>
            <ta e="T143" id="Seg_2929" s="T142">рыба-EP-GEN</ta>
            <ta e="T144" id="Seg_2930" s="T143">жир.[NOM]</ta>
            <ta e="T145" id="Seg_2931" s="T144">соль-EP-TR-PTCP.PST</ta>
            <ta e="T146" id="Seg_2932" s="T145">мясо.[NOM]</ta>
            <ta e="T147" id="Seg_2933" s="T146">мед.[NOM]</ta>
            <ta e="T148" id="Seg_2934" s="T147">соль-EP-ACC</ta>
            <ta e="T149" id="Seg_2935" s="T148">соль-EP-TR-PTCP.PST</ta>
            <ta e="T150" id="Seg_2936" s="T149">мясо-ACC</ta>
            <ta e="T151" id="Seg_2937" s="T150">он.NOM</ta>
            <ta e="T152" id="Seg_2938" s="T151">лить-PST.NAR-3SG.O</ta>
            <ta e="T153" id="Seg_2939" s="T152">раскидать-MULO-PST.NAR-3SG.O</ta>
            <ta e="T154" id="Seg_2940" s="T153">а</ta>
            <ta e="T155" id="Seg_2941" s="T154">рыба-EP-GEN</ta>
            <ta e="T156" id="Seg_2942" s="T155">жир-EP-ACC</ta>
            <ta e="T157" id="Seg_2943" s="T156">и</ta>
            <ta e="T158" id="Seg_2944" s="T157">мёд-ACC</ta>
            <ta e="T159" id="Seg_2945" s="T158">медведь.[NOM]</ta>
            <ta e="T160" id="Seg_2946" s="T159">пить-PST.NAR-3SG.O</ta>
            <ta e="T161" id="Seg_2947" s="T160">съесть-PST.NAR-3SG.O</ta>
            <ta e="T162" id="Seg_2948" s="T161">охотиться-PTCP.PRS</ta>
            <ta e="T163" id="Seg_2949" s="T162">человек-PL.[NOM]</ta>
            <ta e="T164" id="Seg_2950" s="T163">прийти-CO-3PL</ta>
            <ta e="T165" id="Seg_2951" s="T164">дом-EP-ACC</ta>
            <ta e="T166" id="Seg_2952" s="T165">NEG</ta>
            <ta e="T167" id="Seg_2953" s="T166">узнать-DUR-CO-3PL</ta>
            <ta e="T168" id="Seg_2954" s="T167">что-TRL</ta>
            <ta e="T169" id="Seg_2955" s="T168">INDEF-DIM</ta>
            <ta e="T170" id="Seg_2956" s="T169">бояться-HAB-PST-3SG.S</ta>
            <ta e="T171" id="Seg_2957" s="T170">быть-EP-3PL</ta>
            <ta e="T172" id="Seg_2958" s="T171">один</ta>
            <ta e="T173" id="Seg_2959" s="T172">охотиться-PTCP.PRS</ta>
            <ta e="T174" id="Seg_2960" s="T173">человек.[NOM]</ta>
            <ta e="T175" id="Seg_2961" s="T174">дом-ILL</ta>
            <ta e="T176" id="Seg_2962" s="T175">одеться-CO.[3SG.S]</ta>
            <ta e="T177" id="Seg_2963" s="T176">дом.[NOM]</ta>
            <ta e="T178" id="Seg_2964" s="T177">нутро-LOC</ta>
            <ta e="T179" id="Seg_2965" s="T178">круглый-EP-ADVZ</ta>
            <ta e="T180" id="Seg_2966" s="T179">ходить-RES-EP-3SG.S</ta>
            <ta e="T181" id="Seg_2967" s="T180">молча</ta>
            <ta e="T182" id="Seg_2968" s="T181">думать-CVB</ta>
            <ta e="T183" id="Seg_2969" s="T182">стоять-DRV-EP-3SG.S</ta>
            <ta e="T184" id="Seg_2970" s="T183">что-INDEF-DIM</ta>
            <ta e="T185" id="Seg_2971" s="T184">подпол-LOC</ta>
            <ta e="T186" id="Seg_2972" s="T185">%%-US-CO.[3SG.S]</ta>
            <ta e="T187" id="Seg_2973" s="T186">охотиться-PTCP.PRS</ta>
            <ta e="T188" id="Seg_2974" s="T187">человек.[NOM]</ta>
            <ta e="T189" id="Seg_2975" s="T188">слышать-PST-3SG.O</ta>
            <ta e="T190" id="Seg_2976" s="T189">узнать-3SG.O</ta>
            <ta e="T191" id="Seg_2977" s="T190">подпол-LOC</ta>
            <ta e="T192" id="Seg_2978" s="T191">быть-CO-3SG.S</ta>
            <ta e="T193" id="Seg_2979" s="T192">медведь.[NOM]</ta>
            <ta e="T194" id="Seg_2980" s="T193">наружу</ta>
            <ta e="T195" id="Seg_2981" s="T194">выйти-3SG.S</ta>
            <ta e="T196" id="Seg_2982" s="T195">друг-PL-3SG.ILL</ta>
            <ta e="T197" id="Seg_2983" s="T196">сказать-3SG.S</ta>
            <ta e="T198" id="Seg_2984" s="T197">подпол-LOC</ta>
            <ta e="T199" id="Seg_2985" s="T198">лежать.[3SG.S]</ta>
            <ta e="T200" id="Seg_2986" s="T199">медведь.[NOM]</ta>
            <ta e="T201" id="Seg_2987" s="T200">медведь-PTCP.PRS</ta>
            <ta e="T202" id="Seg_2988" s="T201">человек-PL.[NOM]</ta>
            <ta e="T203" id="Seg_2989" s="T202">войти-CO-3PL</ta>
            <ta e="T204" id="Seg_2990" s="T203">дом-ILL</ta>
            <ta e="T205" id="Seg_2991" s="T204">бить-3PL</ta>
            <ta e="T206" id="Seg_2992" s="T205">подпол-EP-ACC</ta>
            <ta e="T207" id="Seg_2993" s="T206">наружу</ta>
            <ta e="T208" id="Seg_2994" s="T207">выйти-3PL</ta>
            <ta e="T209" id="Seg_2995" s="T208">дверь-ACC-OBL.3SG</ta>
            <ta e="T210" id="Seg_2996" s="T209">бить-3PL</ta>
            <ta e="T211" id="Seg_2997" s="T210">один</ta>
            <ta e="T212" id="Seg_2998" s="T211">доска-EP-ACC</ta>
            <ta e="T213" id="Seg_2999" s="T212">развалить-CO-3PL</ta>
            <ta e="T214" id="Seg_3000" s="T213">медведь-ACC</ta>
            <ta e="T215" id="Seg_3001" s="T214">подпол-LOC</ta>
            <ta e="T216" id="Seg_3002" s="T215">увидеть-3PL</ta>
            <ta e="T217" id="Seg_3003" s="T216">охотиться-PTCP.PRS</ta>
            <ta e="T218" id="Seg_3004" s="T217">человек-PL.[NOM]</ta>
            <ta e="T219" id="Seg_3005" s="T218">NEG</ta>
            <ta e="T220" id="Seg_3006" s="T219">три-EP-ADVZ</ta>
            <ta e="T221" id="Seg_3007" s="T220">четыре-ADVZ</ta>
            <ta e="T222" id="Seg_3008" s="T221">стрелять-3PL</ta>
            <ta e="T223" id="Seg_3009" s="T222">он.NOM</ta>
            <ta e="T224" id="Seg_3010" s="T223">и</ta>
            <ta e="T225" id="Seg_3011" s="T224">реветь-PST-PST.[3SG.S]</ta>
            <ta e="T226" id="Seg_3012" s="T225">потом</ta>
            <ta e="T227" id="Seg_3013" s="T226">зверь.[NOM]</ta>
            <ta e="T228" id="Seg_3014" s="T227">замолчать-PFV-3SG.S</ta>
            <ta e="T229" id="Seg_3015" s="T228">два</ta>
            <ta e="T230" id="Seg_3016" s="T229">доска-EP-ACC</ta>
            <ta e="T231" id="Seg_3017" s="T230">%%-CO-3PL</ta>
            <ta e="T232" id="Seg_3018" s="T231">смотреть-PST.NAR-3PL</ta>
            <ta e="T233" id="Seg_3019" s="T232">медведь.[NOM]</ta>
            <ta e="T234" id="Seg_3020" s="T233">видеть-HAB.[3SG.S]</ta>
            <ta e="T235" id="Seg_3021" s="T234">он.NOM-EP-ACC</ta>
            <ta e="T236" id="Seg_3022" s="T235">вверх</ta>
            <ta e="T237" id="Seg_3023" s="T236">вытащить-CO-3PL</ta>
            <ta e="T238" id="Seg_3024" s="T237">шкура-ACC-OBL.3SG</ta>
            <ta e="T239" id="Seg_3025" s="T238">ободрать-CO-3PL</ta>
            <ta e="T240" id="Seg_3026" s="T239">мясо-ACC-OBL.3SG</ta>
            <ta e="T241" id="Seg_3027" s="T240">прочь</ta>
            <ta e="T242" id="Seg_3028" s="T241">бросать-3PL</ta>
            <ta e="T243" id="Seg_3029" s="T242">мясо-3SG</ta>
            <ta e="T244" id="Seg_3030" s="T243">быть-PST-3SG.S</ta>
            <ta e="T245" id="Seg_3031" s="T244">чёрный</ta>
            <ta e="T246" id="Seg_3032" s="T245">NEG</ta>
            <ta e="T247" id="Seg_3033" s="T246">стать.толстым-PTCP.PST</ta>
            <ta e="T248" id="Seg_3034" s="T247">потом</ta>
            <ta e="T249" id="Seg_3035" s="T248">уйти-CVB</ta>
            <ta e="T250" id="Seg_3036" s="T249">охотиться-PTCP.PRS</ta>
            <ta e="T251" id="Seg_3037" s="T250">человек-PL.[NOM]</ta>
            <ta e="T252" id="Seg_3038" s="T251">мать-ACC-OBL.3SG</ta>
            <ta e="T253" id="Seg_3039" s="T252">видеть-HAB-PST-3PL</ta>
            <ta e="T254" id="Seg_3040" s="T253">вперёд-ADV.LOC</ta>
            <ta e="T255" id="Seg_3041" s="T254">дочь-ACC-OBL.3SG</ta>
            <ta e="T256" id="Seg_3042" s="T255">этот</ta>
            <ta e="T257" id="Seg_3043" s="T256">убить-PTCP.PST</ta>
            <ta e="T258" id="Seg_3044" s="T257">медведь.[NOM]</ta>
            <ta e="T259" id="Seg_3045" s="T258">съесть-EP-PST.NAR-3SG.O</ta>
            <ta e="T260" id="Seg_3046" s="T259">день-ADV.LOC</ta>
            <ta e="T261" id="Seg_3047" s="T260">охотиться-PTCP.PRS-ЕL</ta>
            <ta e="T262" id="Seg_3048" s="T261">мать-ACC-OBL.3SG</ta>
            <ta e="T263" id="Seg_3049" s="T262">девушка-COM</ta>
            <ta e="T264" id="Seg_3050" s="T263">сын-3SG</ta>
            <ta e="T265" id="Seg_3051" s="T264">жить-DUR-PST.[3SG.S]</ta>
            <ta e="T266" id="Seg_3052" s="T265">Пирий-LOC</ta>
            <ta e="T267" id="Seg_3053" s="T266">он.NOM</ta>
            <ta e="T268" id="Seg_3054" s="T267">уйти-PST.[3SG.S]</ta>
            <ta e="T269" id="Seg_3055" s="T268">деревня-ILL</ta>
            <ta e="T270" id="Seg_3056" s="T269">что-LOC</ta>
            <ta e="T271" id="Seg_3057" s="T270">жить-PST.[3SG.S]</ta>
            <ta e="T272" id="Seg_3058" s="T271">мать-3SG</ta>
            <ta e="T273" id="Seg_3059" s="T272">он.NOM-EP-GEN</ta>
            <ta e="T274" id="Seg_3060" s="T273">сестра-3SG</ta>
            <ta e="T275" id="Seg_3061" s="T274">он.NOM</ta>
            <ta e="T276" id="Seg_3062" s="T275">так-ADVZ</ta>
            <ta e="T277" id="Seg_3063" s="T276">сказать-PST.[3SG.S]</ta>
            <ta e="T278" id="Seg_3064" s="T277">я.NOM</ta>
            <ta e="T279" id="Seg_3065" s="T278">бы</ta>
            <ta e="T280" id="Seg_3066" s="T279">NEG</ta>
            <ta e="T281" id="Seg_3067" s="T280">убить-SUBJ-1SG.O</ta>
            <ta e="T282" id="Seg_3068" s="T281">этот</ta>
            <ta e="T283" id="Seg_3069" s="T282">медведь-ACC</ta>
            <ta e="T284" id="Seg_3070" s="T283">а</ta>
            <ta e="T285" id="Seg_3071" s="T284">гореть-SUBJ-1SG.O</ta>
            <ta e="T286" id="Seg_3072" s="T285">мать-ADESS-ADJZ</ta>
            <ta e="T287" id="Seg_3073" s="T286">дом-EP-ACC</ta>
            <ta e="T288" id="Seg_3074" s="T287">один-у</ta>
            <ta e="T289" id="Seg_3075" s="T288">медведь-COM</ta>
            <ta e="T290" id="Seg_3076" s="T289">я.NOM</ta>
            <ta e="T291" id="Seg_3077" s="T290">вперёд</ta>
            <ta e="T292" id="Seg_3078" s="T291">медведь-PL-ACC</ta>
            <ta e="T293" id="Seg_3079" s="T292">убить-DUR-FUT-1SG.O</ta>
            <ta e="T294" id="Seg_3080" s="T293">стрелять-DUR-FUT-1SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3081" s="T0">v-v:tense-v:pn</ta>
            <ta e="T2" id="Seg_3082" s="T1">nprop-n:case2</ta>
            <ta e="T3" id="Seg_3083" s="T2">adv</ta>
            <ta e="T4" id="Seg_3084" s="T3">n-n:case2</ta>
            <ta e="T5" id="Seg_3085" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_3086" s="T5">num</ta>
            <ta e="T7" id="Seg_3087" s="T6">n-n:case1</ta>
            <ta e="T8" id="Seg_3088" s="T7">n-n:case1-n:poss</ta>
            <ta e="T9" id="Seg_3089" s="T8">pers-n:ins-n:case1</ta>
            <ta e="T10" id="Seg_3090" s="T9">nprop-n:case1</ta>
            <ta e="T11" id="Seg_3091" s="T10">adv</ta>
            <ta e="T12" id="Seg_3092" s="T11">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_3093" s="T12">n-n:case1</ta>
            <ta e="T14" id="Seg_3094" s="T13">n-n:case2</ta>
            <ta e="T15" id="Seg_3095" s="T14">n-n:poss</ta>
            <ta e="T16" id="Seg_3096" s="T15">pers-n:ins-n:case2</ta>
            <ta e="T17" id="Seg_3097" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_3098" s="T17">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_3099" s="T18">pro</ta>
            <ta e="T20" id="Seg_3100" s="T19">n-n&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_3101" s="T20">n-n:ins-n&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_3102" s="T21">n-n&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_3103" s="T22">num-n:ins-adj&gt;adv</ta>
            <ta e="T24" id="Seg_3104" s="T23">n-adv:case</ta>
            <ta e="T25" id="Seg_3105" s="T24">n-n:case1</ta>
            <ta e="T26" id="Seg_3106" s="T25">v-v:ins-v:pn</ta>
            <ta e="T27" id="Seg_3107" s="T26">v-v:inf</ta>
            <ta e="T28" id="Seg_3108" s="T27">n-n:poss</ta>
            <ta e="T29" id="Seg_3109" s="T28">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_3110" s="T29">n-n:case2</ta>
            <ta e="T31" id="Seg_3111" s="T30">n-n:case2</ta>
            <ta e="T32" id="Seg_3112" s="T31">prep</ta>
            <ta e="T33" id="Seg_3113" s="T32">n-n:case2</ta>
            <ta e="T34" id="Seg_3114" s="T33">pers</ta>
            <ta e="T35" id="Seg_3115" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_3116" s="T35">n-n&gt;v-v&gt;adv</ta>
            <ta e="T37" id="Seg_3117" s="T36">n-adv:case</ta>
            <ta e="T38" id="Seg_3118" s="T37">v-v&gt;adv</ta>
            <ta e="T39" id="Seg_3119" s="T38">n-n-n:case2</ta>
            <ta e="T40" id="Seg_3120" s="T39">v-v:ins-v:pn</ta>
            <ta e="T41" id="Seg_3121" s="T40">n-n:poss-case</ta>
            <ta e="T42" id="Seg_3122" s="T41">adj-adj&gt;adv</ta>
            <ta e="T43" id="Seg_3123" s="T42">conj</ta>
            <ta e="T44" id="Seg_3124" s="T43">n</ta>
            <ta e="T45" id="Seg_3125" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_3126" s="T45">adv</ta>
            <ta e="T47" id="Seg_3127" s="T46">n-n:poss-case</ta>
            <ta e="T48" id="Seg_3128" s="T47">n-n:poss-case</ta>
            <ta e="T49" id="Seg_3129" s="T48">pers-n:ins-n:case1</ta>
            <ta e="T50" id="Seg_3130" s="T49">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_3131" s="T50">n-n:case1</ta>
            <ta e="T52" id="Seg_3132" s="T51">n-n&gt;adj-n-n:case1</ta>
            <ta e="T53" id="Seg_3133" s="T52">n-n:case2</ta>
            <ta e="T54" id="Seg_3134" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_3135" s="T54">n-pp</ta>
            <ta e="T56" id="Seg_3136" s="T55">v-v&gt;ptcp</ta>
            <ta e="T57" id="Seg_3137" s="T56">n-n:case1</ta>
            <ta e="T58" id="Seg_3138" s="T57">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_3139" s="T58">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_3140" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_3141" s="T60">pers-n:ins-n:case1</ta>
            <ta e="T62" id="Seg_3142" s="T61">interrog-n-n:num-n:case1-n:obl.poss</ta>
            <ta e="T63" id="Seg_3143" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_3144" s="T63">n-n:case1-n:poss</ta>
            <ta e="T65" id="Seg_3145" s="T64">preverb</ta>
            <ta e="T66" id="Seg_3146" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_3147" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_3148" s="T67">num</ta>
            <ta e="T69" id="Seg_3149" s="T68">n-n:poss</ta>
            <ta e="T70" id="Seg_3150" s="T69">dem</ta>
            <ta e="T71" id="Seg_3151" s="T70">n-n:case3</ta>
            <ta e="T72" id="Seg_3152" s="T71">n-n:case2</ta>
            <ta e="T73" id="Seg_3153" s="T72">n-n:poss</ta>
            <ta e="T74" id="Seg_3154" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_3155" s="T74">n-n:case1</ta>
            <ta e="T76" id="Seg_3156" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_3157" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_3158" s="T77">quant</ta>
            <ta e="T79" id="Seg_3159" s="T78">n-adv:case</ta>
            <ta e="T80" id="Seg_3160" s="T79">n-n:poss</ta>
            <ta e="T81" id="Seg_3161" s="T80">n-n:obl.poss-n:case2</ta>
            <ta e="T82" id="Seg_3162" s="T81">v-v&gt;adv</ta>
            <ta e="T83" id="Seg_3163" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_3164" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_3165" s="T84">adv</ta>
            <ta e="T86" id="Seg_3166" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_3167" s="T86">n-n&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_3168" s="T87">n-n:poss</ta>
            <ta e="T89" id="Seg_3169" s="T88">v-v:ins-v:pn</ta>
            <ta e="T90" id="Seg_3170" s="T89">n-n:case1-n:obl.poss</ta>
            <ta e="T91" id="Seg_3171" s="T90">v-v&gt;v-v:inf</ta>
            <ta e="T92" id="Seg_3172" s="T91">adj</ta>
            <ta e="T93" id="Seg_3173" s="T92">v-v&gt;ptcp</ta>
            <ta e="T94" id="Seg_3174" s="T93">n-n:case2</ta>
            <ta e="T95" id="Seg_3175" s="T94">n-n:poss</ta>
            <ta e="T96" id="Seg_3176" s="T95">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_3177" s="T96">n-n:case2</ta>
            <ta e="T98" id="Seg_3178" s="T97">interrog-adv:case</ta>
            <ta e="T99" id="Seg_3179" s="T98">Clitic</ta>
            <ta e="T100" id="Seg_3180" s="T99">v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_3181" s="T100">n-n:case1</ta>
            <ta e="T102" id="Seg_3182" s="T101">v-v:ins-v:pn</ta>
            <ta e="T103" id="Seg_3183" s="T102">n-n:case1</ta>
            <ta e="T104" id="Seg_3184" s="T103">v-v:ins-v:pn</ta>
            <ta e="T105" id="Seg_3185" s="T104">n-n:case1</ta>
            <ta e="T106" id="Seg_3186" s="T105">v&gt;ptcp</ta>
            <ta e="T107" id="Seg_3187" s="T106">n-n:case2</ta>
            <ta e="T108" id="Seg_3188" s="T107">n</ta>
            <ta e="T109" id="Seg_3189" s="T108">v-v:pn</ta>
            <ta e="T110" id="Seg_3190" s="T109">n-n:case1</ta>
            <ta e="T111" id="Seg_3191" s="T110">n-n:case2</ta>
            <ta e="T112" id="Seg_3192" s="T111">n-n:case2</ta>
            <ta e="T113" id="Seg_3193" s="T112">interrog-clit</ta>
            <ta e="T114" id="Seg_3194" s="T113">v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_3195" s="T114">n-n:case1</ta>
            <ta e="T116" id="Seg_3196" s="T115">v-v:ins-v:pn</ta>
            <ta e="T117" id="Seg_3197" s="T116">v-v:ins-v:pn</ta>
            <ta e="T118" id="Seg_3198" s="T117">n-n:case2</ta>
            <ta e="T119" id="Seg_3199" s="T118">n-n:case1</ta>
            <ta e="T120" id="Seg_3200" s="T119">n-n:case2</ta>
            <ta e="T121" id="Seg_3201" s="T120">v-v&gt;adv</ta>
            <ta e="T122" id="Seg_3202" s="T121">n-n:ins-n:case1</ta>
            <ta e="T123" id="Seg_3203" s="T122">preverb</ta>
            <ta e="T124" id="Seg_3204" s="T123">v-v:pn</ta>
            <ta e="T125" id="Seg_3205" s="T124">v-v:ins-v:pn</ta>
            <ta e="T126" id="Seg_3206" s="T125">n-n:case3</ta>
            <ta e="T127" id="Seg_3207" s="T126">n-adj&gt;adj</ta>
            <ta e="T128" id="Seg_3208" s="T127">n-n:case2</ta>
            <ta e="T129" id="Seg_3209" s="T128">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T130" id="Seg_3210" s="T129">n-n:case1</ta>
            <ta e="T131" id="Seg_3211" s="T130">n-n:case2</ta>
            <ta e="T132" id="Seg_3212" s="T131">pers-n:num-n:case1</ta>
            <ta e="T133" id="Seg_3213" s="T132">n-n:case1</ta>
            <ta e="T134" id="Seg_3214" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_3215" s="T134">v-v&gt;v-n:ins-v:tense-v:pn</ta>
            <ta e="T136" id="Seg_3216" s="T135">n-n:case2</ta>
            <ta e="T137" id="Seg_3217" s="T136">n-n:case1</ta>
            <ta e="T138" id="Seg_3218" s="T137">n-n:case1</ta>
            <ta e="T139" id="Seg_3219" s="T138">v-v:tense-v:pn</ta>
            <ta e="T140" id="Seg_3220" s="T139">dem-adv:case</ta>
            <ta e="T141" id="Seg_3221" s="T140">v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_3222" s="T141">n-n:case1</ta>
            <ta e="T143" id="Seg_3223" s="T142">n-n:ins-n:case1</ta>
            <ta e="T144" id="Seg_3224" s="T143">n-n:case1</ta>
            <ta e="T145" id="Seg_3225" s="T144">n-n:ins-n&gt;v-v&gt;ptcp</ta>
            <ta e="T146" id="Seg_3226" s="T145">n-n:case1</ta>
            <ta e="T147" id="Seg_3227" s="T146">n-n:case1</ta>
            <ta e="T148" id="Seg_3228" s="T147">n-n:ins-n:case1</ta>
            <ta e="T149" id="Seg_3229" s="T148">n-n:ins-n&gt;v-v&gt;ptcp</ta>
            <ta e="T150" id="Seg_3230" s="T149">n-n:case1</ta>
            <ta e="T151" id="Seg_3231" s="T150">pers</ta>
            <ta e="T152" id="Seg_3232" s="T151">v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_3233" s="T152">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_3234" s="T153">conj</ta>
            <ta e="T155" id="Seg_3235" s="T154">n-n:ins-n:case1</ta>
            <ta e="T156" id="Seg_3236" s="T155">n-n:ins-n:case1</ta>
            <ta e="T157" id="Seg_3237" s="T156">conj</ta>
            <ta e="T158" id="Seg_3238" s="T157">n-n:case1</ta>
            <ta e="T159" id="Seg_3239" s="T158">n-n:case1</ta>
            <ta e="T160" id="Seg_3240" s="T159">v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_3241" s="T160">v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_3242" s="T161">v-v&gt;ptcp</ta>
            <ta e="T163" id="Seg_3243" s="T162">n-n:num-n:case1</ta>
            <ta e="T164" id="Seg_3244" s="T163">v-v:ins-v:pn</ta>
            <ta e="T165" id="Seg_3245" s="T164">n-n:ins-n:case1</ta>
            <ta e="T166" id="Seg_3246" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_3247" s="T166">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T168" id="Seg_3248" s="T167">interrog-n:case2</ta>
            <ta e="T169" id="Seg_3249" s="T168">Clitic-n&gt;n</ta>
            <ta e="T170" id="Seg_3250" s="T169">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_3251" s="T170">v-n:ins-v:pn</ta>
            <ta e="T172" id="Seg_3252" s="T171">num</ta>
            <ta e="T173" id="Seg_3253" s="T172">v-v&gt;ptcp</ta>
            <ta e="T174" id="Seg_3254" s="T173">n-n:case1</ta>
            <ta e="T175" id="Seg_3255" s="T174">n-n:case2</ta>
            <ta e="T176" id="Seg_3256" s="T175">v-v:ins-v:pn</ta>
            <ta e="T177" id="Seg_3257" s="T176">n-n:case1</ta>
            <ta e="T178" id="Seg_3258" s="T177">n-n:case2</ta>
            <ta e="T179" id="Seg_3259" s="T178">adj-n:ins-adj&gt;adv</ta>
            <ta e="T180" id="Seg_3260" s="T179">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T181" id="Seg_3261" s="T180">adv</ta>
            <ta e="T182" id="Seg_3262" s="T181">v-v&gt;adv</ta>
            <ta e="T183" id="Seg_3263" s="T182">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T184" id="Seg_3264" s="T183">interrog-Clitic-n&gt;n</ta>
            <ta e="T185" id="Seg_3265" s="T184">n-n:case2</ta>
            <ta e="T186" id="Seg_3266" s="T185">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T187" id="Seg_3267" s="T186">v-v&gt;ptcp</ta>
            <ta e="T188" id="Seg_3268" s="T187">n-n:case1</ta>
            <ta e="T189" id="Seg_3269" s="T188">v-v:tense-v:pn</ta>
            <ta e="T190" id="Seg_3270" s="T189">v-v:pn</ta>
            <ta e="T191" id="Seg_3271" s="T190">n-n:case2</ta>
            <ta e="T192" id="Seg_3272" s="T191">v-v:ins-v:pn</ta>
            <ta e="T193" id="Seg_3273" s="T192">n-n:case1</ta>
            <ta e="T194" id="Seg_3274" s="T193">adv</ta>
            <ta e="T195" id="Seg_3275" s="T194">v-v:pn</ta>
            <ta e="T196" id="Seg_3276" s="T195">n-n:num-n:poss-case</ta>
            <ta e="T197" id="Seg_3277" s="T196">v-v:pn</ta>
            <ta e="T198" id="Seg_3278" s="T197">n-n:case2</ta>
            <ta e="T199" id="Seg_3279" s="T198">v-v:pn</ta>
            <ta e="T200" id="Seg_3280" s="T199">n-n:case1</ta>
            <ta e="T201" id="Seg_3281" s="T200">n-v&gt;ptcp</ta>
            <ta e="T202" id="Seg_3282" s="T201">n-n:num-n:case1</ta>
            <ta e="T203" id="Seg_3283" s="T202">v-v:ins-v:pn</ta>
            <ta e="T204" id="Seg_3284" s="T203">n-n:case2</ta>
            <ta e="T205" id="Seg_3285" s="T204">v-v:pn</ta>
            <ta e="T206" id="Seg_3286" s="T205">n-n:ins-n:case1</ta>
            <ta e="T207" id="Seg_3287" s="T206">adv</ta>
            <ta e="T208" id="Seg_3288" s="T207">v-v:pn</ta>
            <ta e="T209" id="Seg_3289" s="T208">n-n:case1-n:obl.poss</ta>
            <ta e="T210" id="Seg_3290" s="T209">v-v:pn</ta>
            <ta e="T211" id="Seg_3291" s="T210">num</ta>
            <ta e="T212" id="Seg_3292" s="T211">n-n:ins-n:case1</ta>
            <ta e="T213" id="Seg_3293" s="T212">v-v:ins-v:pn</ta>
            <ta e="T214" id="Seg_3294" s="T213">n-n:case1</ta>
            <ta e="T215" id="Seg_3295" s="T214">n-n:case2</ta>
            <ta e="T216" id="Seg_3296" s="T215">v-v:pn</ta>
            <ta e="T217" id="Seg_3297" s="T216">v-v&gt;ptcp</ta>
            <ta e="T218" id="Seg_3298" s="T217">n-n:num-n:case1</ta>
            <ta e="T219" id="Seg_3299" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_3300" s="T219">num-n:ins-adj&gt;adv</ta>
            <ta e="T221" id="Seg_3301" s="T220">num-adj&gt;adv</ta>
            <ta e="T222" id="Seg_3302" s="T221">v-v:pn</ta>
            <ta e="T223" id="Seg_3303" s="T222">pers</ta>
            <ta e="T224" id="Seg_3304" s="T223">conj</ta>
            <ta e="T225" id="Seg_3305" s="T224">v-v:tense-v:tense-v:pn</ta>
            <ta e="T226" id="Seg_3306" s="T225">adv</ta>
            <ta e="T227" id="Seg_3307" s="T226">n-n:case1</ta>
            <ta e="T228" id="Seg_3308" s="T227">v-v&gt;v-v:pn</ta>
            <ta e="T229" id="Seg_3309" s="T228">num</ta>
            <ta e="T230" id="Seg_3310" s="T229">n-n:ins-n:case1</ta>
            <ta e="T231" id="Seg_3311" s="T230">v-v:ins-v:pn</ta>
            <ta e="T232" id="Seg_3312" s="T231">v-v:tense-v:pn</ta>
            <ta e="T233" id="Seg_3313" s="T232">n-n:case1</ta>
            <ta e="T234" id="Seg_3314" s="T233">v-v&gt;v-v:pn</ta>
            <ta e="T235" id="Seg_3315" s="T234">pers-n:ins-n:case1</ta>
            <ta e="T236" id="Seg_3316" s="T235">adv</ta>
            <ta e="T237" id="Seg_3317" s="T236">v-v:ins-v:pn</ta>
            <ta e="T238" id="Seg_3318" s="T237">n-n:case1-n:obl.poss</ta>
            <ta e="T239" id="Seg_3319" s="T238">v-v:ins-v:pn</ta>
            <ta e="T240" id="Seg_3320" s="T239">n-n:case1-n:obl.poss</ta>
            <ta e="T241" id="Seg_3321" s="T240">preverb</ta>
            <ta e="T242" id="Seg_3322" s="T241">v-v:pn</ta>
            <ta e="T243" id="Seg_3323" s="T242">n-n:poss</ta>
            <ta e="T244" id="Seg_3324" s="T243">v-v:tense-v:pn</ta>
            <ta e="T245" id="Seg_3325" s="T244">adj</ta>
            <ta e="T246" id="Seg_3326" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_3327" s="T246">v-v&gt;ptcp</ta>
            <ta e="T248" id="Seg_3328" s="T247">adv</ta>
            <ta e="T249" id="Seg_3329" s="T248">v-v&gt;adv</ta>
            <ta e="T250" id="Seg_3330" s="T249">v-v&gt;ptcp</ta>
            <ta e="T251" id="Seg_3331" s="T250">n-n:num-n:case1</ta>
            <ta e="T252" id="Seg_3332" s="T251">n-n:case1-n:obl.poss</ta>
            <ta e="T253" id="Seg_3333" s="T252">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T254" id="Seg_3334" s="T253">adv-adv:case</ta>
            <ta e="T255" id="Seg_3335" s="T254">n-n:case1-n:obl.poss</ta>
            <ta e="T256" id="Seg_3336" s="T255">dem</ta>
            <ta e="T257" id="Seg_3337" s="T256">v-v&gt;ptcp</ta>
            <ta e="T258" id="Seg_3338" s="T257">n-n:case1</ta>
            <ta e="T259" id="Seg_3339" s="T258">v-n:ins-v:tense-v:pn</ta>
            <ta e="T260" id="Seg_3340" s="T259">n-adv:case</ta>
            <ta e="T261" id="Seg_3341" s="T260">v-v&gt;ptcp-n:case2</ta>
            <ta e="T262" id="Seg_3342" s="T261">n-n:case1-n:obl.poss</ta>
            <ta e="T263" id="Seg_3343" s="T262">n-n:case2</ta>
            <ta e="T264" id="Seg_3344" s="T263">n-n:poss</ta>
            <ta e="T265" id="Seg_3345" s="T264">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T266" id="Seg_3346" s="T265">nprop-n:case2</ta>
            <ta e="T267" id="Seg_3347" s="T266">pers</ta>
            <ta e="T268" id="Seg_3348" s="T267">v-v:tense-v:pn</ta>
            <ta e="T269" id="Seg_3349" s="T268">n-n:case2</ta>
            <ta e="T270" id="Seg_3350" s="T269">interrog-n:case2</ta>
            <ta e="T271" id="Seg_3351" s="T270">v-v:tense-v:pn</ta>
            <ta e="T272" id="Seg_3352" s="T271">n-n:poss</ta>
            <ta e="T273" id="Seg_3353" s="T272">pers-n:ins-n:case1</ta>
            <ta e="T274" id="Seg_3354" s="T273">n-n:poss</ta>
            <ta e="T275" id="Seg_3355" s="T274">pers</ta>
            <ta e="T276" id="Seg_3356" s="T275">adv-adj&gt;adv</ta>
            <ta e="T277" id="Seg_3357" s="T276">v-v:tense-v:pn</ta>
            <ta e="T278" id="Seg_3358" s="T277">pers</ta>
            <ta e="T279" id="Seg_3359" s="T278">ptcl</ta>
            <ta e="T280" id="Seg_3360" s="T279">ptcl</ta>
            <ta e="T281" id="Seg_3361" s="T280">v-v:mood-v:pn</ta>
            <ta e="T282" id="Seg_3362" s="T281">dem</ta>
            <ta e="T283" id="Seg_3363" s="T282">n-n:case1</ta>
            <ta e="T284" id="Seg_3364" s="T283">conj</ta>
            <ta e="T285" id="Seg_3365" s="T284">v-v:mood-v:pn</ta>
            <ta e="T286" id="Seg_3366" s="T285">n-n:case2-n&gt;adj</ta>
            <ta e="T287" id="Seg_3367" s="T286">n-n:ins-n:case1</ta>
            <ta e="T288" id="Seg_3368" s="T287">num-pp</ta>
            <ta e="T289" id="Seg_3369" s="T288">n-n:case2</ta>
            <ta e="T290" id="Seg_3370" s="T289">pers</ta>
            <ta e="T291" id="Seg_3371" s="T290">adv</ta>
            <ta e="T292" id="Seg_3372" s="T291">n-n:num-n:case1</ta>
            <ta e="T293" id="Seg_3373" s="T292">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T294" id="Seg_3374" s="T293">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_3375" s="T0">v</ta>
            <ta e="T2" id="Seg_3376" s="T1">nprop</ta>
            <ta e="T3" id="Seg_3377" s="T2">adv</ta>
            <ta e="T4" id="Seg_3378" s="T3">n</ta>
            <ta e="T5" id="Seg_3379" s="T4">v</ta>
            <ta e="T6" id="Seg_3380" s="T5">num</ta>
            <ta e="T7" id="Seg_3381" s="T6">n</ta>
            <ta e="T8" id="Seg_3382" s="T7">n</ta>
            <ta e="T9" id="Seg_3383" s="T8">pers</ta>
            <ta e="T10" id="Seg_3384" s="T9">nprop</ta>
            <ta e="T11" id="Seg_3385" s="T10">adv</ta>
            <ta e="T12" id="Seg_3386" s="T11">v</ta>
            <ta e="T13" id="Seg_3387" s="T12">n</ta>
            <ta e="T14" id="Seg_3388" s="T13">n</ta>
            <ta e="T15" id="Seg_3389" s="T14">n</ta>
            <ta e="T16" id="Seg_3390" s="T15">pers</ta>
            <ta e="T17" id="Seg_3391" s="T16">v</ta>
            <ta e="T18" id="Seg_3392" s="T17">v</ta>
            <ta e="T19" id="Seg_3393" s="T18">emphpers</ta>
            <ta e="T20" id="Seg_3394" s="T19">v</ta>
            <ta e="T21" id="Seg_3395" s="T20">adj</ta>
            <ta e="T22" id="Seg_3396" s="T21">adj</ta>
            <ta e="T23" id="Seg_3397" s="T22">num</ta>
            <ta e="T24" id="Seg_3398" s="T23">n</ta>
            <ta e="T25" id="Seg_3399" s="T24">n</ta>
            <ta e="T26" id="Seg_3400" s="T25">v</ta>
            <ta e="T27" id="Seg_3401" s="T26">v</ta>
            <ta e="T28" id="Seg_3402" s="T27">n</ta>
            <ta e="T29" id="Seg_3403" s="T28">v</ta>
            <ta e="T30" id="Seg_3404" s="T29">n</ta>
            <ta e="T31" id="Seg_3405" s="T30">n</ta>
            <ta e="T32" id="Seg_3406" s="T31">prep</ta>
            <ta e="T33" id="Seg_3407" s="T32">n</ta>
            <ta e="T34" id="Seg_3408" s="T33">pers</ta>
            <ta e="T35" id="Seg_3409" s="T34">v</ta>
            <ta e="T36" id="Seg_3410" s="T35">adv</ta>
            <ta e="T37" id="Seg_3411" s="T36">n</ta>
            <ta e="T38" id="Seg_3412" s="T37">adv</ta>
            <ta e="T39" id="Seg_3413" s="T38">n</ta>
            <ta e="T40" id="Seg_3414" s="T39">v</ta>
            <ta e="T41" id="Seg_3415" s="T40">n</ta>
            <ta e="T42" id="Seg_3416" s="T41">adv</ta>
            <ta e="T43" id="Seg_3417" s="T42">conj</ta>
            <ta e="T45" id="Seg_3418" s="T44">v</ta>
            <ta e="T46" id="Seg_3419" s="T45">v</ta>
            <ta e="T47" id="Seg_3420" s="T46">n</ta>
            <ta e="T48" id="Seg_3421" s="T47">n</ta>
            <ta e="T49" id="Seg_3422" s="T48">pers</ta>
            <ta e="T50" id="Seg_3423" s="T49">v</ta>
            <ta e="T51" id="Seg_3424" s="T50">n</ta>
            <ta e="T52" id="Seg_3425" s="T51">adj</ta>
            <ta e="T53" id="Seg_3426" s="T52">n</ta>
            <ta e="T54" id="Seg_3427" s="T53">v</ta>
            <ta e="T55" id="Seg_3428" s="T54">adv</ta>
            <ta e="T56" id="Seg_3429" s="T55">ptcp</ta>
            <ta e="T57" id="Seg_3430" s="T56">n</ta>
            <ta e="T58" id="Seg_3431" s="T57">v</ta>
            <ta e="T59" id="Seg_3432" s="T58">v</ta>
            <ta e="T60" id="Seg_3433" s="T59">v</ta>
            <ta e="T61" id="Seg_3434" s="T60">pers</ta>
            <ta e="T62" id="Seg_3435" s="T61">n</ta>
            <ta e="T63" id="Seg_3436" s="T62">v</ta>
            <ta e="T64" id="Seg_3437" s="T63">n</ta>
            <ta e="T65" id="Seg_3438" s="T64">preverb</ta>
            <ta e="T66" id="Seg_3439" s="T65">v</ta>
            <ta e="T67" id="Seg_3440" s="T66">v</ta>
            <ta e="T68" id="Seg_3441" s="T67">num</ta>
            <ta e="T69" id="Seg_3442" s="T68">n</ta>
            <ta e="T70" id="Seg_3443" s="T69">dem</ta>
            <ta e="T71" id="Seg_3444" s="T70">n</ta>
            <ta e="T72" id="Seg_3445" s="T71">n</ta>
            <ta e="T73" id="Seg_3446" s="T72">n</ta>
            <ta e="T74" id="Seg_3447" s="T73">v</ta>
            <ta e="T75" id="Seg_3448" s="T74">n</ta>
            <ta e="T76" id="Seg_3449" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_3450" s="T76">v</ta>
            <ta e="T78" id="Seg_3451" s="T77">quant</ta>
            <ta e="T79" id="Seg_3452" s="T78">v</ta>
            <ta e="T80" id="Seg_3453" s="T79">n</ta>
            <ta e="T81" id="Seg_3454" s="T80">n</ta>
            <ta e="T82" id="Seg_3455" s="T81">adv</ta>
            <ta e="T83" id="Seg_3456" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_3457" s="T83">v</ta>
            <ta e="T85" id="Seg_3458" s="T84">v</ta>
            <ta e="T86" id="Seg_3459" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_3460" s="T86">v</ta>
            <ta e="T88" id="Seg_3461" s="T87">n</ta>
            <ta e="T89" id="Seg_3462" s="T88">v</ta>
            <ta e="T90" id="Seg_3463" s="T89">n</ta>
            <ta e="T91" id="Seg_3464" s="T90">v</ta>
            <ta e="T92" id="Seg_3465" s="T91">adj</ta>
            <ta e="T93" id="Seg_3466" s="T92">ptcp</ta>
            <ta e="T94" id="Seg_3467" s="T93">n</ta>
            <ta e="T95" id="Seg_3468" s="T94">n</ta>
            <ta e="T96" id="Seg_3469" s="T95">v</ta>
            <ta e="T97" id="Seg_3470" s="T96">n</ta>
            <ta e="T98" id="Seg_3471" s="T97">interrog</ta>
            <ta e="T99" id="Seg_3472" s="T98">clit</ta>
            <ta e="T100" id="Seg_3473" s="T99">v</ta>
            <ta e="T101" id="Seg_3474" s="T100">n</ta>
            <ta e="T102" id="Seg_3475" s="T101">v</ta>
            <ta e="T103" id="Seg_3476" s="T102">n</ta>
            <ta e="T104" id="Seg_3477" s="T103">v</ta>
            <ta e="T105" id="Seg_3478" s="T104">n</ta>
            <ta e="T106" id="Seg_3479" s="T105">ptcp</ta>
            <ta e="T107" id="Seg_3480" s="T106">n</ta>
            <ta e="T108" id="Seg_3481" s="T107">n</ta>
            <ta e="T109" id="Seg_3482" s="T108">v</ta>
            <ta e="T110" id="Seg_3483" s="T109">n</ta>
            <ta e="T111" id="Seg_3484" s="T110">n</ta>
            <ta e="T112" id="Seg_3485" s="T111">n</ta>
            <ta e="T113" id="Seg_3486" s="T112">interrog</ta>
            <ta e="T114" id="Seg_3487" s="T113">v</ta>
            <ta e="T115" id="Seg_3488" s="T114">n</ta>
            <ta e="T116" id="Seg_3489" s="T115">v</ta>
            <ta e="T117" id="Seg_3490" s="T116">v</ta>
            <ta e="T118" id="Seg_3491" s="T117">n</ta>
            <ta e="T119" id="Seg_3492" s="T118">n</ta>
            <ta e="T120" id="Seg_3493" s="T119">n</ta>
            <ta e="T121" id="Seg_3494" s="T120">adv</ta>
            <ta e="T122" id="Seg_3495" s="T121">n</ta>
            <ta e="T123" id="Seg_3496" s="T122">preverb</ta>
            <ta e="T124" id="Seg_3497" s="T123">v</ta>
            <ta e="T125" id="Seg_3498" s="T124">v</ta>
            <ta e="T126" id="Seg_3499" s="T125">n</ta>
            <ta e="T127" id="Seg_3500" s="T126">n</ta>
            <ta e="T128" id="Seg_3501" s="T127">n</ta>
            <ta e="T129" id="Seg_3502" s="T128">v</ta>
            <ta e="T130" id="Seg_3503" s="T129">n</ta>
            <ta e="T131" id="Seg_3504" s="T130">n</ta>
            <ta e="T132" id="Seg_3505" s="T131">pers</ta>
            <ta e="T133" id="Seg_3506" s="T132">n</ta>
            <ta e="T134" id="Seg_3507" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_3508" s="T134">v</ta>
            <ta e="T136" id="Seg_3509" s="T135">n</ta>
            <ta e="T137" id="Seg_3510" s="T136">n</ta>
            <ta e="T138" id="Seg_3511" s="T137">n</ta>
            <ta e="T139" id="Seg_3512" s="T138">v</ta>
            <ta e="T140" id="Seg_3513" s="T139">adv</ta>
            <ta e="T141" id="Seg_3514" s="T140">v</ta>
            <ta e="T142" id="Seg_3515" s="T141">n</ta>
            <ta e="T143" id="Seg_3516" s="T142">n</ta>
            <ta e="T144" id="Seg_3517" s="T143">n</ta>
            <ta e="T145" id="Seg_3518" s="T144">ptcp</ta>
            <ta e="T146" id="Seg_3519" s="T145">n</ta>
            <ta e="T147" id="Seg_3520" s="T146">n</ta>
            <ta e="T148" id="Seg_3521" s="T147">n</ta>
            <ta e="T149" id="Seg_3522" s="T148">ptcp</ta>
            <ta e="T150" id="Seg_3523" s="T149">n</ta>
            <ta e="T151" id="Seg_3524" s="T150">pers</ta>
            <ta e="T152" id="Seg_3525" s="T151">v</ta>
            <ta e="T153" id="Seg_3526" s="T152">v</ta>
            <ta e="T154" id="Seg_3527" s="T153">conj</ta>
            <ta e="T155" id="Seg_3528" s="T154">n</ta>
            <ta e="T156" id="Seg_3529" s="T155">n</ta>
            <ta e="T157" id="Seg_3530" s="T156">conj</ta>
            <ta e="T158" id="Seg_3531" s="T157">n</ta>
            <ta e="T159" id="Seg_3532" s="T158">n</ta>
            <ta e="T160" id="Seg_3533" s="T159">v</ta>
            <ta e="T161" id="Seg_3534" s="T160">v</ta>
            <ta e="T162" id="Seg_3535" s="T161">ptcp</ta>
            <ta e="T163" id="Seg_3536" s="T162">n</ta>
            <ta e="T164" id="Seg_3537" s="T163">v</ta>
            <ta e="T165" id="Seg_3538" s="T164">n</ta>
            <ta e="T166" id="Seg_3539" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_3540" s="T166">v</ta>
            <ta e="T168" id="Seg_3541" s="T167">interrog</ta>
            <ta e="T169" id="Seg_3542" s="T168">clit</ta>
            <ta e="T170" id="Seg_3543" s="T169">v</ta>
            <ta e="T171" id="Seg_3544" s="T170">v</ta>
            <ta e="T172" id="Seg_3545" s="T171">num</ta>
            <ta e="T173" id="Seg_3546" s="T172">v</ta>
            <ta e="T174" id="Seg_3547" s="T173">n</ta>
            <ta e="T175" id="Seg_3548" s="T174">n</ta>
            <ta e="T176" id="Seg_3549" s="T175">v</ta>
            <ta e="T177" id="Seg_3550" s="T176">n</ta>
            <ta e="T178" id="Seg_3551" s="T177">n</ta>
            <ta e="T179" id="Seg_3552" s="T178">n</ta>
            <ta e="T180" id="Seg_3553" s="T179">v</ta>
            <ta e="T181" id="Seg_3554" s="T180">adv</ta>
            <ta e="T182" id="Seg_3555" s="T181">adv</ta>
            <ta e="T183" id="Seg_3556" s="T182">v</ta>
            <ta e="T184" id="Seg_3557" s="T183">interrog</ta>
            <ta e="T185" id="Seg_3558" s="T184">n</ta>
            <ta e="T186" id="Seg_3559" s="T185">v</ta>
            <ta e="T187" id="Seg_3560" s="T186">v</ta>
            <ta e="T188" id="Seg_3561" s="T187">n</ta>
            <ta e="T189" id="Seg_3562" s="T188">v</ta>
            <ta e="T190" id="Seg_3563" s="T189">v</ta>
            <ta e="T191" id="Seg_3564" s="T190">n</ta>
            <ta e="T192" id="Seg_3565" s="T191">v</ta>
            <ta e="T193" id="Seg_3566" s="T192">n</ta>
            <ta e="T194" id="Seg_3567" s="T193">adv</ta>
            <ta e="T195" id="Seg_3568" s="T194">v</ta>
            <ta e="T196" id="Seg_3569" s="T195">v</ta>
            <ta e="T197" id="Seg_3570" s="T196">v</ta>
            <ta e="T198" id="Seg_3571" s="T197">n</ta>
            <ta e="T199" id="Seg_3572" s="T198">v</ta>
            <ta e="T200" id="Seg_3573" s="T199">n</ta>
            <ta e="T201" id="Seg_3574" s="T200">ptcp</ta>
            <ta e="T202" id="Seg_3575" s="T201">n</ta>
            <ta e="T203" id="Seg_3576" s="T202">v</ta>
            <ta e="T204" id="Seg_3577" s="T203">n</ta>
            <ta e="T205" id="Seg_3578" s="T204">v</ta>
            <ta e="T206" id="Seg_3579" s="T205">n</ta>
            <ta e="T207" id="Seg_3580" s="T206">adv</ta>
            <ta e="T208" id="Seg_3581" s="T207">v</ta>
            <ta e="T209" id="Seg_3582" s="T208">n</ta>
            <ta e="T210" id="Seg_3583" s="T209">v</ta>
            <ta e="T211" id="Seg_3584" s="T210">num</ta>
            <ta e="T212" id="Seg_3585" s="T211">n</ta>
            <ta e="T213" id="Seg_3586" s="T212">v</ta>
            <ta e="T214" id="Seg_3587" s="T213">n</ta>
            <ta e="T215" id="Seg_3588" s="T214">n</ta>
            <ta e="T216" id="Seg_3589" s="T215">v</ta>
            <ta e="T217" id="Seg_3590" s="T216">ptcp</ta>
            <ta e="T218" id="Seg_3591" s="T217">n</ta>
            <ta e="T219" id="Seg_3592" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_3593" s="T219">adv</ta>
            <ta e="T221" id="Seg_3594" s="T220">adv</ta>
            <ta e="T222" id="Seg_3595" s="T221">v</ta>
            <ta e="T223" id="Seg_3596" s="T222">pers</ta>
            <ta e="T224" id="Seg_3597" s="T223">conj</ta>
            <ta e="T225" id="Seg_3598" s="T224">v</ta>
            <ta e="T226" id="Seg_3599" s="T225">adv</ta>
            <ta e="T227" id="Seg_3600" s="T226">n</ta>
            <ta e="T228" id="Seg_3601" s="T227">v</ta>
            <ta e="T229" id="Seg_3602" s="T228">num</ta>
            <ta e="T230" id="Seg_3603" s="T229">n</ta>
            <ta e="T231" id="Seg_3604" s="T230">v</ta>
            <ta e="T232" id="Seg_3605" s="T231">v</ta>
            <ta e="T233" id="Seg_3606" s="T232">n</ta>
            <ta e="T234" id="Seg_3607" s="T233">v</ta>
            <ta e="T235" id="Seg_3608" s="T234">pers</ta>
            <ta e="T236" id="Seg_3609" s="T235">adv</ta>
            <ta e="T237" id="Seg_3610" s="T236">v</ta>
            <ta e="T238" id="Seg_3611" s="T237">n</ta>
            <ta e="T239" id="Seg_3612" s="T238">v</ta>
            <ta e="T240" id="Seg_3613" s="T239">n</ta>
            <ta e="T241" id="Seg_3614" s="T240">preverb</ta>
            <ta e="T242" id="Seg_3615" s="T241">v</ta>
            <ta e="T243" id="Seg_3616" s="T242">n</ta>
            <ta e="T244" id="Seg_3617" s="T243">v</ta>
            <ta e="T245" id="Seg_3618" s="T244">adj</ta>
            <ta e="T246" id="Seg_3619" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_3620" s="T246">ptcp</ta>
            <ta e="T248" id="Seg_3621" s="T247">adv</ta>
            <ta e="T249" id="Seg_3622" s="T248">adv</ta>
            <ta e="T250" id="Seg_3623" s="T249">ptcp</ta>
            <ta e="T251" id="Seg_3624" s="T250">n</ta>
            <ta e="T252" id="Seg_3625" s="T251">n</ta>
            <ta e="T253" id="Seg_3626" s="T252">v</ta>
            <ta e="T254" id="Seg_3627" s="T253">adv</ta>
            <ta e="T255" id="Seg_3628" s="T254">n</ta>
            <ta e="T256" id="Seg_3629" s="T255">dem</ta>
            <ta e="T257" id="Seg_3630" s="T256">ptcp</ta>
            <ta e="T258" id="Seg_3631" s="T257">n</ta>
            <ta e="T259" id="Seg_3632" s="T258">v</ta>
            <ta e="T260" id="Seg_3633" s="T259">n</ta>
            <ta e="T261" id="Seg_3634" s="T260">n</ta>
            <ta e="T262" id="Seg_3635" s="T261">v</ta>
            <ta e="T263" id="Seg_3636" s="T262">n</ta>
            <ta e="T264" id="Seg_3637" s="T263">n</ta>
            <ta e="T265" id="Seg_3638" s="T264">v</ta>
            <ta e="T266" id="Seg_3639" s="T265">nprop</ta>
            <ta e="T267" id="Seg_3640" s="T266">pers</ta>
            <ta e="T268" id="Seg_3641" s="T267">v</ta>
            <ta e="T269" id="Seg_3642" s="T268">n</ta>
            <ta e="T270" id="Seg_3643" s="T269">interrog</ta>
            <ta e="T271" id="Seg_3644" s="T270">v</ta>
            <ta e="T272" id="Seg_3645" s="T271">n</ta>
            <ta e="T274" id="Seg_3646" s="T273">n</ta>
            <ta e="T275" id="Seg_3647" s="T274">pers</ta>
            <ta e="T276" id="Seg_3648" s="T275">adv</ta>
            <ta e="T277" id="Seg_3649" s="T276">v</ta>
            <ta e="T279" id="Seg_3650" s="T278">ptcl</ta>
            <ta e="T280" id="Seg_3651" s="T279">ptcl</ta>
            <ta e="T281" id="Seg_3652" s="T280">v</ta>
            <ta e="T282" id="Seg_3653" s="T281">dem</ta>
            <ta e="T283" id="Seg_3654" s="T282">n</ta>
            <ta e="T284" id="Seg_3655" s="T283">conj</ta>
            <ta e="T285" id="Seg_3656" s="T284">v</ta>
            <ta e="T286" id="Seg_3657" s="T285">n</ta>
            <ta e="T287" id="Seg_3658" s="T286">n</ta>
            <ta e="T288" id="Seg_3659" s="T287">adv</ta>
            <ta e="T289" id="Seg_3660" s="T288">n</ta>
            <ta e="T290" id="Seg_3661" s="T289">pers</ta>
            <ta e="T291" id="Seg_3662" s="T290">adv</ta>
            <ta e="T292" id="Seg_3663" s="T291">n</ta>
            <ta e="T293" id="Seg_3664" s="T292">v</ta>
            <ta e="T294" id="Seg_3665" s="T293">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_3666" s="T0">0.3:S v:pred</ta>
            <ta e="T5" id="Seg_3667" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_3668" s="T6">np:S</ta>
            <ta e="T8" id="Seg_3669" s="T7">np:S</ta>
            <ta e="T10" id="Seg_3670" s="T9">n:pred</ta>
            <ta e="T12" id="Seg_3671" s="T11">v:pred</ta>
            <ta e="T13" id="Seg_3672" s="T12">np.h:S</ta>
            <ta e="T15" id="Seg_3673" s="T14">np.h:S</ta>
            <ta e="T17" id="Seg_3674" s="T16">v:pred</ta>
            <ta e="T18" id="Seg_3675" s="T17">0.3.h:S v:pred</ta>
            <ta e="T20" id="Seg_3676" s="T19">0.3.h:S v:pred</ta>
            <ta e="T21" id="Seg_3677" s="T20">0.3.h:S v:pred</ta>
            <ta e="T22" id="Seg_3678" s="T21">0.3.h:S v:pred</ta>
            <ta e="T25" id="Seg_3679" s="T24">np.h:S</ta>
            <ta e="T26" id="Seg_3680" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_3681" s="T26">s:purp</ta>
            <ta e="T28" id="Seg_3682" s="T27">np.h:S</ta>
            <ta e="T29" id="Seg_3683" s="T28">v:pred</ta>
            <ta e="T34" id="Seg_3684" s="T33">pro.h:S</ta>
            <ta e="T35" id="Seg_3685" s="T34">v:pred</ta>
            <ta e="T36" id="Seg_3686" s="T35">s:purp</ta>
            <ta e="T38" id="Seg_3687" s="T37">s:adv</ta>
            <ta e="T40" id="Seg_3688" s="T39">0.3.h:S v:pred</ta>
            <ta e="T45" id="Seg_3689" s="T44">0.3.h:S v:pred</ta>
            <ta e="T49" id="Seg_3690" s="T48">pro.h:O</ta>
            <ta e="T50" id="Seg_3691" s="T49"> v:pred</ta>
            <ta e="T51" id="Seg_3692" s="T50">np:S</ta>
            <ta e="T54" id="Seg_3693" s="T53">0.3:S</ta>
            <ta e="T57" id="Seg_3694" s="T56">np.h:O</ta>
            <ta e="T58" id="Seg_3695" s="T57">0.3:S 0.3.h:O v:pred</ta>
            <ta e="T59" id="Seg_3696" s="T58">0.3:S 0.3.h:O v:pred</ta>
            <ta e="T60" id="Seg_3697" s="T59">0.3:S 0.3.h:O v:pred</ta>
            <ta e="T62" id="Seg_3698" s="T61">np:O</ta>
            <ta e="T63" id="Seg_3699" s="T62">0.3:S v:pred</ta>
            <ta e="T64" id="Seg_3700" s="T63">np:O</ta>
            <ta e="T66" id="Seg_3701" s="T65">0.3:S v:pred</ta>
            <ta e="T67" id="Seg_3702" s="T66">v:pred</ta>
            <ta e="T69" id="Seg_3703" s="T68">np:S</ta>
            <ta e="T73" id="Seg_3704" s="T72">np.h:S</ta>
            <ta e="T74" id="Seg_3705" s="T73">v:pred</ta>
            <ta e="T75" id="Seg_3706" s="T74">np.h:S</ta>
            <ta e="T77" id="Seg_3707" s="T76">v:pred</ta>
            <ta e="T80" id="Seg_3708" s="T79">np.h:S</ta>
            <ta e="T82" id="Seg_3709" s="T81">s:adv</ta>
            <ta e="T84" id="Seg_3710" s="T83">v:pred</ta>
            <ta e="T87" id="Seg_3711" s="T86">0.3:S v:pred</ta>
            <ta e="T88" id="Seg_3712" s="T87">np.h:S</ta>
            <ta e="T89" id="Seg_3713" s="T88">v:pred</ta>
            <ta e="T91" id="Seg_3714" s="T90">s:purp</ta>
            <ta e="T95" id="Seg_3715" s="T94">np.h:S</ta>
            <ta e="T96" id="Seg_3716" s="T95">v:pred</ta>
            <ta e="T100" id="Seg_3717" s="T99">v:pred</ta>
            <ta e="T101" id="Seg_3718" s="T100">np:S</ta>
            <ta e="T102" id="Seg_3719" s="T101">0.3:S v:pred</ta>
            <ta e="T103" id="Seg_3720" s="T102">np.h:O</ta>
            <ta e="T104" id="Seg_3721" s="T103">0.3:S 0.3.h:O v:pred</ta>
            <ta e="T108" id="Seg_3722" s="T107">np:S</ta>
            <ta e="T109" id="Seg_3723" s="T108">v:pred</ta>
            <ta e="T113" id="Seg_3724" s="T112">pro.h:S</ta>
            <ta e="T114" id="Seg_3725" s="T113">v:pred</ta>
            <ta e="T115" id="Seg_3726" s="T114">pro:O</ta>
            <ta e="T116" id="Seg_3727" s="T115">0.3:S v:pred</ta>
            <ta e="T117" id="Seg_3728" s="T116">0.3:S v:pred</ta>
            <ta e="T121" id="Seg_3729" s="T120">s:adv</ta>
            <ta e="T122" id="Seg_3730" s="T121">np:O</ta>
            <ta e="T124" id="Seg_3731" s="T123">0.3:S v:pred</ta>
            <ta e="T125" id="Seg_3732" s="T124">0.3:S v:pred</ta>
            <ta e="T129" id="Seg_3733" s="T128">v:pred</ta>
            <ta e="T130" id="Seg_3734" s="T129">np:S</ta>
            <ta e="T132" id="Seg_3735" s="T131">pro.h:O</ta>
            <ta e="T133" id="Seg_3736" s="T132">np:S</ta>
            <ta e="T135" id="Seg_3737" s="T134">v:pred</ta>
            <ta e="T137" id="Seg_3738" s="T136">np:S</ta>
            <ta e="T138" id="Seg_3739" s="T137">n:pred</ta>
            <ta e="T139" id="Seg_3740" s="T138">cop</ta>
            <ta e="T141" id="Seg_3741" s="T140">v:pred</ta>
            <ta e="T142" id="Seg_3742" s="T141">np:S</ta>
            <ta e="T144" id="Seg_3743" s="T143">np:S</ta>
            <ta e="T146" id="Seg_3744" s="T145">np:S</ta>
            <ta e="T147" id="Seg_3745" s="T146">np:S</ta>
            <ta e="T148" id="Seg_3746" s="T147">np:O</ta>
            <ta e="T150" id="Seg_3747" s="T149">np:O</ta>
            <ta e="T151" id="Seg_3748" s="T150">pro.h:S</ta>
            <ta e="T152" id="Seg_3749" s="T151">v:pred</ta>
            <ta e="T153" id="Seg_3750" s="T152">0.3:S v:pred</ta>
            <ta e="T156" id="Seg_3751" s="T155">np:O</ta>
            <ta e="T158" id="Seg_3752" s="T157">np:O</ta>
            <ta e="T159" id="Seg_3753" s="T158">np:S</ta>
            <ta e="T160" id="Seg_3754" s="T159">v:pred</ta>
            <ta e="T161" id="Seg_3755" s="T160">v:pred</ta>
            <ta e="T163" id="Seg_3756" s="T162">np.h:S</ta>
            <ta e="T164" id="Seg_3757" s="T163">v:pred</ta>
            <ta e="T165" id="Seg_3758" s="T164">np:O</ta>
            <ta e="T167" id="Seg_3759" s="T166">0.3.h:S v:pred</ta>
            <ta e="T170" id="Seg_3760" s="T169">0.3.h:S v:pred</ta>
            <ta e="T171" id="Seg_3761" s="T170">0.3.h:S v:pred</ta>
            <ta e="T174" id="Seg_3762" s="T173">np.h:S</ta>
            <ta e="T176" id="Seg_3763" s="T175">v:pred</ta>
            <ta e="T180" id="Seg_3764" s="T179">0.3.h:S v:pred</ta>
            <ta e="T182" id="Seg_3765" s="T181">s:adv</ta>
            <ta e="T183" id="Seg_3766" s="T182">0.3.h:S v:pred</ta>
            <ta e="T186" id="Seg_3767" s="T185">0.3:S v:pred</ta>
            <ta e="T188" id="Seg_3768" s="T187">np.h:S</ta>
            <ta e="T189" id="Seg_3769" s="T188">v:pred</ta>
            <ta e="T190" id="Seg_3770" s="T189">0.3.h:S v:pred</ta>
            <ta e="T192" id="Seg_3771" s="T191">v:pred</ta>
            <ta e="T193" id="Seg_3772" s="T192">np:S</ta>
            <ta e="T195" id="Seg_3773" s="T194">0.3.h:S v:pred</ta>
            <ta e="T197" id="Seg_3774" s="T196">0.3.h:S v:pred</ta>
            <ta e="T199" id="Seg_3775" s="T198">v:pred</ta>
            <ta e="T200" id="Seg_3776" s="T199">np:S</ta>
            <ta e="T202" id="Seg_3777" s="T201">np.h:S</ta>
            <ta e="T203" id="Seg_3778" s="T202">v:pred</ta>
            <ta e="T206" id="Seg_3779" s="T205">np:O</ta>
            <ta e="T208" id="Seg_3780" s="T207">0.3.h:S v:pred</ta>
            <ta e="T209" id="Seg_3781" s="T208">np:O</ta>
            <ta e="T210" id="Seg_3782" s="T209">0.3.h:S v:pred</ta>
            <ta e="T212" id="Seg_3783" s="T211">np:O</ta>
            <ta e="T213" id="Seg_3784" s="T212">0.3.h:S </ta>
            <ta e="T214" id="Seg_3785" s="T213">np:O</ta>
            <ta e="T216" id="Seg_3786" s="T215">0.3.h:S v:pred</ta>
            <ta e="T218" id="Seg_3787" s="T217">np.h:S</ta>
            <ta e="T222" id="Seg_3788" s="T221">v:pred</ta>
            <ta e="T223" id="Seg_3789" s="T222">np:S</ta>
            <ta e="T225" id="Seg_3790" s="T224">v:pred</ta>
            <ta e="T227" id="Seg_3791" s="T226">np:S</ta>
            <ta e="T228" id="Seg_3792" s="T227">v:pred</ta>
            <ta e="T230" id="Seg_3793" s="T229">np:O</ta>
            <ta e="T231" id="Seg_3794" s="T230">0.3.h:S v:pred</ta>
            <ta e="T232" id="Seg_3795" s="T231">0.3.h:S v:pred</ta>
            <ta e="T233" id="Seg_3796" s="T232">np:S</ta>
            <ta e="T234" id="Seg_3797" s="T233">v:pred</ta>
            <ta e="T235" id="Seg_3798" s="T234">pro.h:O</ta>
            <ta e="T237" id="Seg_3799" s="T236">0.3.h:S v:pred</ta>
            <ta e="T238" id="Seg_3800" s="T237">np:O</ta>
            <ta e="T239" id="Seg_3801" s="T238">0.3.h:S v:pred</ta>
            <ta e="T240" id="Seg_3802" s="T239">np:O</ta>
            <ta e="T242" id="Seg_3803" s="T241">0.3.h:S v:pred</ta>
            <ta e="T243" id="Seg_3804" s="T242">np:S</ta>
            <ta e="T244" id="Seg_3805" s="T243">cop</ta>
            <ta e="T245" id="Seg_3806" s="T244">adj:pred</ta>
            <ta e="T247" id="Seg_3807" s="T246">adj:pred</ta>
            <ta e="T251" id="Seg_3808" s="T248">s:temp</ta>
            <ta e="T252" id="Seg_3809" s="T251">np.h:O</ta>
            <ta e="T253" id="Seg_3810" s="T252">0.3.h:S v:pred</ta>
            <ta e="T255" id="Seg_3811" s="T254">np.h:O</ta>
            <ta e="T258" id="Seg_3812" s="T257">np:S</ta>
            <ta e="T259" id="Seg_3813" s="T258">v:pred</ta>
            <ta e="T262" id="Seg_3814" s="T261">np:O</ta>
            <ta e="T264" id="Seg_3815" s="T263">np.h:S</ta>
            <ta e="T265" id="Seg_3816" s="T264">v:pred</ta>
            <ta e="T267" id="Seg_3817" s="T266">pro.h:S</ta>
            <ta e="T268" id="Seg_3818" s="T267">v:pred</ta>
            <ta e="T274" id="Seg_3819" s="T269">s:rel</ta>
            <ta e="T275" id="Seg_3820" s="T274">pro.h:S</ta>
            <ta e="T277" id="Seg_3821" s="T276">v:pred</ta>
            <ta e="T278" id="Seg_3822" s="T277">pro.h:S</ta>
            <ta e="T281" id="Seg_3823" s="T280">v:pred</ta>
            <ta e="T283" id="Seg_3824" s="T282">np:O</ta>
            <ta e="T285" id="Seg_3825" s="T284">0.3.h:S v:pred</ta>
            <ta e="T287" id="Seg_3826" s="T286">np:O</ta>
            <ta e="T290" id="Seg_3827" s="T289">pro.h:S</ta>
            <ta e="T292" id="Seg_3828" s="T291">np:O</ta>
            <ta e="T293" id="Seg_3829" s="T292">v:pred</ta>
            <ta e="T294" id="Seg_3830" s="T293">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_3831" s="T0">np:Th</ta>
            <ta e="T2" id="Seg_3832" s="T1">np:L</ta>
            <ta e="T4" id="Seg_3833" s="T3">np:L</ta>
            <ta e="T7" id="Seg_3834" s="T6">np:Th</ta>
            <ta e="T8" id="Seg_3835" s="T7">np:Th</ta>
            <ta e="T9" id="Seg_3836" s="T8">pro.h:Poss</ta>
            <ta e="T11" id="Seg_3837" s="T10">adv:L</ta>
            <ta e="T14" id="Seg_3838" s="T13">np:Com</ta>
            <ta e="T15" id="Seg_3839" s="T14">np.h:Th</ta>
            <ta e="T16" id="Seg_3840" s="T15">pro.h:Poss</ta>
            <ta e="T18" id="Seg_3841" s="T17">0.3.h:Th</ta>
            <ta e="T20" id="Seg_3842" s="T19">0.3.h:A</ta>
            <ta e="T21" id="Seg_3843" s="T20">0.3.h:A</ta>
            <ta e="T22" id="Seg_3844" s="T21">0.3.h:A</ta>
            <ta e="T24" id="Seg_3845" s="T23">n:Time</ta>
            <ta e="T25" id="Seg_3846" s="T24">np.h:A</ta>
            <ta e="T28" id="Seg_3847" s="T27">np.h:Th</ta>
            <ta e="T30" id="Seg_3848" s="T29">np:L</ta>
            <ta e="T31" id="Seg_3849" s="T30">n:Time</ta>
            <ta e="T33" id="Seg_3850" s="T32">n:Time</ta>
            <ta e="T34" id="Seg_3851" s="T33">pro.h:Th</ta>
            <ta e="T37" id="Seg_3852" s="T36">n:Time</ta>
            <ta e="T39" id="Seg_3853" s="T38">n:Time</ta>
            <ta e="T40" id="Seg_3854" s="T39">0.3.h:A</ta>
            <ta e="T41" id="Seg_3855" s="T40">np:G</ta>
            <ta e="T45" id="Seg_3856" s="T44">0.3.h:A</ta>
            <ta e="T47" id="Seg_3857" s="T46">np:G</ta>
            <ta e="T48" id="Seg_3858" s="T47">n:Time</ta>
            <ta e="T49" id="Seg_3859" s="T48">np.h:P</ta>
            <ta e="T51" id="Seg_3860" s="T50">np:A</ta>
            <ta e="T52" id="Seg_3861" s="T51">np.h:Poss</ta>
            <ta e="T53" id="Seg_3862" s="T52">np:Path</ta>
            <ta e="T54" id="Seg_3863" s="T53">0.3:A</ta>
            <ta e="T55" id="Seg_3864" s="T54">pp:Time</ta>
            <ta e="T57" id="Seg_3865" s="T56">np.h:P</ta>
            <ta e="T58" id="Seg_3866" s="T57">0.3:A 0.3.h:P</ta>
            <ta e="T59" id="Seg_3867" s="T58">0.3:A 0.3.h:P</ta>
            <ta e="T60" id="Seg_3868" s="T59">0.3:A 0.3.h:P</ta>
            <ta e="T62" id="Seg_3869" s="T61">np:P</ta>
            <ta e="T63" id="Seg_3870" s="T62">0.3:A</ta>
            <ta e="T64" id="Seg_3871" s="T63">np:P</ta>
            <ta e="T66" id="Seg_3872" s="T65">0.3:A</ta>
            <ta e="T69" id="Seg_3873" s="T68">np:Th 0.3.h:Poss</ta>
            <ta e="T71" id="Seg_3874" s="T70">np.h:So</ta>
            <ta e="T72" id="Seg_3875" s="T71">n:Time</ta>
            <ta e="T73" id="Seg_3876" s="T72">np.h:A</ta>
            <ta e="T75" id="Seg_3877" s="T74">np.h:A</ta>
            <ta e="T79" id="Seg_3878" s="T78">n:Time</ta>
            <ta e="T80" id="Seg_3879" s="T79">np.h:E</ta>
            <ta e="T87" id="Seg_3880" s="T86">0.3:Th</ta>
            <ta e="T88" id="Seg_3881" s="T87">np.h:A</ta>
            <ta e="T90" id="Seg_3882" s="T89">np.h:P 0.3.h:Poss</ta>
            <ta e="T94" id="Seg_3883" s="T93">np:Path</ta>
            <ta e="T95" id="Seg_3884" s="T94">np.h:A</ta>
            <ta e="T97" id="Seg_3885" s="T96">np:L</ta>
            <ta e="T99" id="Seg_3886" s="T97">np:So</ta>
            <ta e="T101" id="Seg_3887" s="T100">np:A</ta>
            <ta e="T102" id="Seg_3888" s="T101">0.3:A</ta>
            <ta e="T103" id="Seg_3889" s="T102">np.h:P</ta>
            <ta e="T104" id="Seg_3890" s="T103">0.3:A 0.3.h:P</ta>
            <ta e="T105" id="Seg_3891" s="T104">np.h:Poss</ta>
            <ta e="T107" id="Seg_3892" s="T106">np:Path</ta>
            <ta e="T108" id="Seg_3893" s="T107">np:A</ta>
            <ta e="T110" id="Seg_3894" s="T109">np.h:Poss</ta>
            <ta e="T111" id="Seg_3895" s="T110">np:G</ta>
            <ta e="T112" id="Seg_3896" s="T111">np:L</ta>
            <ta e="T113" id="Seg_3897" s="T112">pro.h:Th</ta>
            <ta e="T115" id="Seg_3898" s="T114">np:P</ta>
            <ta e="T116" id="Seg_3899" s="T115">0.3:A</ta>
            <ta e="T117" id="Seg_3900" s="T116">0.3:A</ta>
            <ta e="T118" id="Seg_3901" s="T117">np:G</ta>
            <ta e="T119" id="Seg_3902" s="T118">np:Poss</ta>
            <ta e="T124" id="Seg_3903" s="T123">0.3:A</ta>
            <ta e="T125" id="Seg_3904" s="T124">0.3:A</ta>
            <ta e="T128" id="Seg_3905" s="T127">np:L</ta>
            <ta e="T130" id="Seg_3906" s="T129">np:A</ta>
            <ta e="T131" id="Seg_3907" s="T130">np:Com</ta>
            <ta e="T132" id="Seg_3908" s="T131">pro.h:P</ta>
            <ta e="T133" id="Seg_3909" s="T132">np:A</ta>
            <ta e="T136" id="Seg_3910" s="T135">np:L</ta>
            <ta e="T137" id="Seg_3911" s="T136">np:Th</ta>
            <ta e="T140" id="Seg_3912" s="T139">adv:L</ta>
            <ta e="T142" id="Seg_3913" s="T141">np:Th</ta>
            <ta e="T143" id="Seg_3914" s="T142">np:Poss</ta>
            <ta e="T144" id="Seg_3915" s="T143">np:Th</ta>
            <ta e="T146" id="Seg_3916" s="T145">np:Th</ta>
            <ta e="T147" id="Seg_3917" s="T146">np:Th</ta>
            <ta e="T148" id="Seg_3918" s="T147">np:P</ta>
            <ta e="T150" id="Seg_3919" s="T149">np:P</ta>
            <ta e="T151" id="Seg_3920" s="T150">pro.h:A</ta>
            <ta e="T153" id="Seg_3921" s="T152">0.3:A</ta>
            <ta e="T155" id="Seg_3922" s="T154">np:Poss</ta>
            <ta e="T156" id="Seg_3923" s="T155">np:P</ta>
            <ta e="T158" id="Seg_3924" s="T157">np:P</ta>
            <ta e="T159" id="Seg_3925" s="T158">np:A</ta>
            <ta e="T163" id="Seg_3926" s="T162">np.h:A</ta>
            <ta e="T165" id="Seg_3927" s="T164">np:P</ta>
            <ta e="T167" id="Seg_3928" s="T166">0.3.h:E</ta>
            <ta e="T170" id="Seg_3929" s="T169">0.3.h:E</ta>
            <ta e="T171" id="Seg_3930" s="T170">0.3.h:Th</ta>
            <ta e="T174" id="Seg_3931" s="T173">np.h:A</ta>
            <ta e="T175" id="Seg_3932" s="T174">np:G</ta>
            <ta e="T177" id="Seg_3933" s="T176">np:Poss</ta>
            <ta e="T178" id="Seg_3934" s="T177">np:L</ta>
            <ta e="T180" id="Seg_3935" s="T179">0.3.h:A</ta>
            <ta e="T183" id="Seg_3936" s="T182">0.3.h:A</ta>
            <ta e="T185" id="Seg_3937" s="T184">np:L</ta>
            <ta e="T186" id="Seg_3938" s="T185">0.3:A</ta>
            <ta e="T188" id="Seg_3939" s="T187">np.h:E</ta>
            <ta e="T190" id="Seg_3940" s="T189">0.3.h:E</ta>
            <ta e="T191" id="Seg_3941" s="T190">np:L</ta>
            <ta e="T193" id="Seg_3942" s="T192">np:Th</ta>
            <ta e="T195" id="Seg_3943" s="T194">0.3.h:A</ta>
            <ta e="T196" id="Seg_3944" s="T195">np.h:R 0.3.h:Poss</ta>
            <ta e="T198" id="Seg_3945" s="T197">np:L</ta>
            <ta e="T200" id="Seg_3946" s="T199">np:Th</ta>
            <ta e="T202" id="Seg_3947" s="T201">np.h:A</ta>
            <ta e="T204" id="Seg_3948" s="T203">np:G</ta>
            <ta e="T206" id="Seg_3949" s="T205">np:P</ta>
            <ta e="T208" id="Seg_3950" s="T207">0.3.h:A</ta>
            <ta e="T209" id="Seg_3951" s="T208">np:P</ta>
            <ta e="T210" id="Seg_3952" s="T209">0.3.h:A</ta>
            <ta e="T212" id="Seg_3953" s="T211">np:P</ta>
            <ta e="T213" id="Seg_3954" s="T212">0.3.h:A</ta>
            <ta e="T214" id="Seg_3955" s="T213">np:Th</ta>
            <ta e="T215" id="Seg_3956" s="T214">np:L</ta>
            <ta e="T216" id="Seg_3957" s="T215">0.3.h:E</ta>
            <ta e="T218" id="Seg_3958" s="T217">np.h:A</ta>
            <ta e="T223" id="Seg_3959" s="T222">0.3:A</ta>
            <ta e="T227" id="Seg_3960" s="T226">np:E</ta>
            <ta e="T230" id="Seg_3961" s="T229">np:P</ta>
            <ta e="T231" id="Seg_3962" s="T230">0.3.h:A</ta>
            <ta e="T232" id="Seg_3963" s="T231">0.3.h:E</ta>
            <ta e="T233" id="Seg_3964" s="T232">np:Th</ta>
            <ta e="T235" id="Seg_3965" s="T234">pro.h:P</ta>
            <ta e="T237" id="Seg_3966" s="T236">0.3.h:A</ta>
            <ta e="T238" id="Seg_3967" s="T237">np:P 0.3:Poss</ta>
            <ta e="T239" id="Seg_3968" s="T238">0.3.h:A s</ta>
            <ta e="T240" id="Seg_3969" s="T239">np:P 0.3:Pos</ta>
            <ta e="T242" id="Seg_3970" s="T241">0.3.h:A</ta>
            <ta e="T243" id="Seg_3971" s="T242">np:Th</ta>
            <ta e="T252" id="Seg_3972" s="T251">np.h:P 0.3.h:Poss</ta>
            <ta e="T253" id="Seg_3973" s="T252">0.3.h:E</ta>
            <ta e="T255" id="Seg_3974" s="T254">np.h:P 0.3.h:Poss</ta>
            <ta e="T258" id="Seg_3975" s="T257">np:A</ta>
            <ta e="T260" id="Seg_3976" s="T259">adv:L</ta>
            <ta e="T262" id="Seg_3977" s="T261">np:P 0.3.h:Poss</ta>
            <ta e="T263" id="Seg_3978" s="T262">np:Com</ta>
            <ta e="T264" id="Seg_3979" s="T263">np.h:Th</ta>
            <ta e="T266" id="Seg_3980" s="T265">np:L</ta>
            <ta e="T267" id="Seg_3981" s="T266">pro.h:A</ta>
            <ta e="T269" id="Seg_3982" s="T268">np:G</ta>
            <ta e="T272" id="Seg_3983" s="T271">np.h:Th 0.3.h:Poss</ta>
            <ta e="T273" id="Seg_3984" s="T272">pro.h:Poss</ta>
            <ta e="T274" id="Seg_3985" s="T273">np.h:Th</ta>
            <ta e="T278" id="Seg_3986" s="T277">pro.h:A</ta>
            <ta e="T283" id="Seg_3987" s="T282">np:P</ta>
            <ta e="T285" id="Seg_3988" s="T284">0.3.h:A</ta>
            <ta e="T286" id="Seg_3989" s="T285">np.h:Poss</ta>
            <ta e="T287" id="Seg_3990" s="T286">np:P</ta>
            <ta e="T289" id="Seg_3991" s="T288">np:Com</ta>
            <ta e="T290" id="Seg_3992" s="T289">pro.h:A</ta>
            <ta e="T292" id="Seg_3993" s="T291">np:P</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T43" id="Seg_3994" s="T42">RUS:gram</ta>
            <ta e="T78" id="Seg_3995" s="T77">RUS:core</ta>
            <ta e="T99" id="Seg_3996" s="T98">RUS:gram</ta>
            <ta e="T126" id="Seg_3997" s="T125">RUS:cult</ta>
            <ta e="T136" id="Seg_3998" s="T135">RUS:cult</ta>
            <ta e="T138" id="Seg_3999" s="T137">RUS:cult</ta>
            <ta e="T154" id="Seg_4000" s="T153">RUS:gram</ta>
            <ta e="T157" id="Seg_4001" s="T156">RUS:gram</ta>
            <ta e="T185" id="Seg_4002" s="T184">RUS:cult</ta>
            <ta e="T191" id="Seg_4003" s="T190">RUS:cult</ta>
            <ta e="T198" id="Seg_4004" s="T197">RUS:cult</ta>
            <ta e="T206" id="Seg_4005" s="T205">RUS:cult</ta>
            <ta e="T215" id="Seg_4006" s="T214">RUS:cult</ta>
            <ta e="T224" id="Seg_4007" s="T223">RUS:gram</ta>
            <ta e="T279" id="Seg_4008" s="T278">RUS:gram</ta>
            <ta e="T284" id="Seg_4009" s="T283">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T2" id="Seg_4010" s="T0">Это случилось в Марково.</ta>
            <ta e="T7" id="Seg_4011" s="T2">Там в тайге была одна деревня.</ta>
            <ta e="T10" id="Seg_4012" s="T7">Ее называли Марково.</ta>
            <ta e="T14" id="Seg_4013" s="T10">Там жила одна женщина с дочерью.</ta>
            <ta e="T17" id="Seg_4014" s="T14">Мужа у нее не было.</ta>
            <ta e="T19" id="Seg_4015" s="T17">Жила одна.</ta>
            <ta e="T22" id="Seg_4016" s="T19">Белковала, за ягодами ходила, за орехами.</ta>
            <ta e="T27" id="Seg_4017" s="T22">Однажды осенью дочь ушла на охоту.</ta>
            <ta e="T30" id="Seg_4018" s="T27">Мать была дома.</ta>
            <ta e="T36" id="Seg_4019" s="T30">С утра до вечера она ходила белковать.</ta>
            <ta e="T41" id="Seg_4020" s="T36">Днем проходив, к вечеру отправилась домой.</ta>
            <ta e="T47" id="Seg_4021" s="T41">Долго ли коротко шла обратно домой.</ta>
            <ta e="T53" id="Seg_4022" s="T47">Весь день ее караулил медведь, по следам женщины [шёл].</ta>
            <ta e="T57" id="Seg_4023" s="T53">Догнал вечером охотницу.</ta>
            <ta e="T61" id="Seg_4024" s="T57">Схватил, убил и съел ее.</ta>
            <ta e="T66" id="Seg_4025" s="T61">Вещи изорвал, ружье в сторону бросил.</ta>
            <ta e="T71" id="Seg_4026" s="T66">Осталась одна нога от этой девушки.</ta>
            <ta e="T74" id="Seg_4027" s="T71">Вечером мать ждала.</ta>
            <ta e="T77" id="Seg_4028" s="T74">Дочь не пришла.</ta>
            <ta e="T84" id="Seg_4029" s="T77">Всю ночь мать о дочери думая не спала.</ta>
            <ta e="T91" id="Seg_4030" s="T84">До рассвета мать ушла дочь искать.</ta>
            <ta e="T97" id="Seg_4031" s="T91">По вчерашнему следу мать шла по тайге.</ta>
            <ta e="T101" id="Seg_4032" s="T97">Откуда-то выскочил медведь.</ta>
            <ta e="T104" id="Seg_4033" s="T101">Схватил женщину съел.</ta>
            <ta e="T111" id="Seg_4034" s="T104">По следу пришедшей женщины медведь пришел к дому женщины.</ta>
            <ta e="T114" id="Seg_4035" s="T111">Дома никого не было.</ta>
            <ta e="T118" id="Seg_4036" s="T114">Дверь открыл, зашел в избу.</ta>
            <ta e="T126" id="Seg_4037" s="T118">Внутри дома походив доску вверх поднял залез в подпол.</ta>
            <ta e="T131" id="Seg_4038" s="T126">В коровнике стояли корова с теленком.</ta>
            <ta e="T135" id="Seg_4039" s="T131">Их медведь не трогал.</ta>
            <ta e="T139" id="Seg_4040" s="T135">В подполе медведь хозяином был.</ta>
            <ta e="T147" id="Seg_4041" s="T139">Там были соль, рыбий жир, соленое мясо, мед.</ta>
            <ta e="T153" id="Seg_4042" s="T147">Соль, соленое мясо он вывалил, раскидал.</ta>
            <ta e="T161" id="Seg_4043" s="T153">А рыбий жир и мед медведь выпил.</ta>
            <ta e="T164" id="Seg_4044" s="T161">Охотники пришли.</ta>
            <ta e="T167" id="Seg_4045" s="T164">Дом не узнают.</ta>
            <ta e="T171" id="Seg_4046" s="T167">Почему-то опасались.</ta>
            <ta e="T176" id="Seg_4047" s="T171">Один охотник вошел в дом.</ta>
            <ta e="T183" id="Seg_4048" s="T176">Внутри дома кругом обошел, молча подумав, постоял.</ta>
            <ta e="T186" id="Seg_4049" s="T183">Кто-то рявкнул в подполе.</ta>
            <ta e="T193" id="Seg_4050" s="T186">Охотник услышал, понял: в подполе есть медведь.</ta>
            <ta e="T200" id="Seg_4051" s="T193">На улицу вышел, товарищам сказал: в подполе лежит медведь.</ta>
            <ta e="T206" id="Seg_4052" s="T200">Охотники вошли в дом, забили подпол.</ta>
            <ta e="T210" id="Seg_4053" s="T206">Они вышли и закрыли дверь.</ta>
            <ta e="T216" id="Seg_4054" s="T210">Одну половицу выворотили, медведя в подполье увидели.</ta>
            <ta e="T222" id="Seg_4055" s="T216">Охотники не три, четыре раза стреляли.</ta>
            <ta e="T225" id="Seg_4056" s="T222">Он и ревел.</ta>
            <ta e="T228" id="Seg_4057" s="T225">Потом медведь замолчал.</ta>
            <ta e="T234" id="Seg_4058" s="T228">Две половицы выворотили, глядят, медведь умер.</ta>
            <ta e="T237" id="Seg_4059" s="T234">Его вверх вытащили.</ta>
            <ta e="T242" id="Seg_4060" s="T237">Шкуру ободрали, мясо прочь выбросили.</ta>
            <ta e="T247" id="Seg_4061" s="T242">Мясо было черное, не жирное.</ta>
            <ta e="T263" id="Seg_4062" s="T247">Потом, когда пошли охотники, мать нашли, подальше дочь, которую убитый медведь съел днем во время охоты на мать с дочерью.</ta>
            <ta e="T266" id="Seg_4063" s="T263">Сын жил в Пирию.</ta>
            <ta e="T274" id="Seg_4064" s="T266">Он ушел в деревню, где жила мать и его сестра.</ta>
            <ta e="T289" id="Seg_4065" s="T274">Он так сказал: я бы не стал убивать этого медведя, а сжег бы материн дом вместе с медведем.</ta>
            <ta e="T294" id="Seg_4066" s="T289">Я в будущем медведей убивать буду, стрелять буду.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2" id="Seg_4067" s="T0">This happened in Markovo.</ta>
            <ta e="T7" id="Seg_4068" s="T2">There was a village in the taiga.</ta>
            <ta e="T10" id="Seg_4069" s="T7">It was called Markovo.</ta>
            <ta e="T14" id="Seg_4070" s="T10">There lived a woman with her daughter.</ta>
            <ta e="T17" id="Seg_4071" s="T14">She had no husband.</ta>
            <ta e="T19" id="Seg_4072" s="T17">She lived alone.</ta>
            <ta e="T22" id="Seg_4073" s="T19">She hunted squirrels, picked berries and nuts.</ta>
            <ta e="T27" id="Seg_4074" s="T22">Once in autumn the girl went hunting.</ta>
            <ta e="T30" id="Seg_4075" s="T27">Her mother was at home.</ta>
            <ta e="T36" id="Seg_4076" s="T30">She was hunting from the morning until the evening.</ta>
            <ta e="T41" id="Seg_4077" s="T36">Having walked the whole day, by the evening she went home.</ta>
            <ta e="T47" id="Seg_4078" s="T41">She was going home, whether a long or a short way.</ta>
            <ta e="T53" id="Seg_4079" s="T47">The whole day, a bear was following the girl's trace.</ta>
            <ta e="T57" id="Seg_4080" s="T53">In the evening it overtook her.</ta>
            <ta e="T61" id="Seg_4081" s="T57">It grabbed her, killed her and ate her.</ta>
            <ta e="T66" id="Seg_4082" s="T61">It tore her clothes, threw her rifle aside.</ta>
            <ta e="T71" id="Seg_4083" s="T66">Only one leg remained from this girl.</ta>
            <ta e="T74" id="Seg_4084" s="T71">Her mother was waiting in the evening.</ta>
            <ta e="T77" id="Seg_4085" s="T74">The daughter did not come.</ta>
            <ta e="T84" id="Seg_4086" s="T77">All the night the mother did not sleep, thinking about her daughter.</ta>
            <ta e="T91" id="Seg_4087" s="T84">Before the dawn the mother went to look for her daughter.</ta>
            <ta e="T97" id="Seg_4088" s="T91">The mother went into the taiga following yesterday’s trail.</ta>
            <ta e="T101" id="Seg_4089" s="T97">The bear jumped out from somewhere.</ta>
            <ta e="T104" id="Seg_4090" s="T101">It grabbed the woman and ate her.</ta>
            <ta e="T111" id="Seg_4091" s="T104">By the traces of the woman, the bear went to her house.</ta>
            <ta e="T114" id="Seg_4092" s="T111">There was nobody at home.</ta>
            <ta e="T118" id="Seg_4093" s="T114">It opened the door and went inside.</ta>
            <ta e="T126" id="Seg_4094" s="T118">It walked in the house, lifted a board and went down into the cellar.</ta>
            <ta e="T131" id="Seg_4095" s="T126">In the barn there were a cow and her calf.</ta>
            <ta e="T135" id="Seg_4096" s="T131">The bear haven’t touched them.</ta>
            <ta e="T139" id="Seg_4097" s="T135">In the cellar, the bear was the master.</ta>
            <ta e="T147" id="Seg_4098" s="T139">There were salt, fish oil, salted meat, honey.</ta>
            <ta e="T153" id="Seg_4099" s="T147">It poured out, scattered the salt, the salted meat.</ta>
            <ta e="T161" id="Seg_4100" s="T153">And [the bear] drank the fish oil and the honey.</ta>
            <ta e="T164" id="Seg_4101" s="T161">There came hunters.</ta>
            <ta e="T167" id="Seg_4102" s="T164">They do not recognize the house.</ta>
            <ta e="T171" id="Seg_4103" s="T167">Somehow they were afraid.</ta>
            <ta e="T176" id="Seg_4104" s="T171">One hunter went into the house.</ta>
            <ta e="T183" id="Seg_4105" s="T176">He went around the house inside, stood silent for a while, thinking.</ta>
            <ta e="T186" id="Seg_4106" s="T183">Someone roared in the cellar.</ta>
            <ta e="T193" id="Seg_4107" s="T186">The hunter heard [it] and knew that there was a bear in the cellar.</ta>
            <ta e="T200" id="Seg_4108" s="T193">He went outside, told his comrades: there is a bear lyingin the cellar.</ta>
            <ta e="T206" id="Seg_4109" s="T200">The hunters went into the house, blocked the cellar.</ta>
            <ta e="T210" id="Seg_4110" s="T206">They went out and closed the door.</ta>
            <ta e="T216" id="Seg_4111" s="T210">They broke one board in the floor and saw the bear in the cellar.</ta>
            <ta e="T222" id="Seg_4112" s="T216">The hunters shot not three, four times.</ta>
            <ta e="T225" id="Seg_4113" s="T222">And it roared.</ta>
            <ta e="T228" id="Seg_4114" s="T225">Then the beast got silent.</ta>
            <ta e="T234" id="Seg_4115" s="T228">They took out two boards, they saw that the bear was dead.</ta>
            <ta e="T237" id="Seg_4116" s="T234">They took it upstairs.</ta>
            <ta e="T242" id="Seg_4117" s="T237">They skinned it, threw the meat away.</ta>
            <ta e="T247" id="Seg_4118" s="T242">The meat was black, not fat.</ta>
            <ta e="T263" id="Seg_4119" s="T247">Then as the hunters went on, they found the mother, further away the daughter, which the bear killed while hunting the mother and her daughter.</ta>
            <ta e="T266" id="Seg_4120" s="T263">The son lived in Piriy.</ta>
            <ta e="T274" id="Seg_4121" s="T266">He went to the village where his mother and sister lived.</ta>
            <ta e="T289" id="Seg_4122" s="T274">He said so: I would not kill this bear but rather burned mother’s house together with the bear.</ta>
            <ta e="T294" id="Seg_4123" s="T289">From now on I will kill bears, I will shoot them.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2" id="Seg_4124" s="T0">Das passierte in Markovo.</ta>
            <ta e="T7" id="Seg_4125" s="T2">Dort in der Taiga gab es ein Dorf.</ta>
            <ta e="T10" id="Seg_4126" s="T7">Sein Name ist Markovo.</ta>
            <ta e="T14" id="Seg_4127" s="T10">Dort lebte eine Frau mit ihrer Tochter.</ta>
            <ta e="T17" id="Seg_4128" s="T14">Sie hatte keinen Mann.</ta>
            <ta e="T19" id="Seg_4129" s="T17">Sie lebte alleine.</ta>
            <ta e="T22" id="Seg_4130" s="T19">Sie jagte Eichhörnchen, sammelte Beeren und Nüsse.</ta>
            <ta e="T27" id="Seg_4131" s="T22">Einmal im Herbst ging das Mädchen jagen.</ta>
            <ta e="T30" id="Seg_4132" s="T27">Die Mutter war zu Hause.</ta>
            <ta e="T36" id="Seg_4133" s="T30">Vom Morgen bis zum Abend ging sie Eichhörnchen jagen.</ta>
            <ta e="T41" id="Seg_4134" s="T36">Den Tag über war sie weg, gegen Abend kam sie nach Hause.</ta>
            <ta e="T47" id="Seg_4135" s="T41">Über kurz oder lang ging sie nach Hause zurück.</ta>
            <ta e="T53" id="Seg_4136" s="T47">Den ganzen Tag hatte ein Bär die Spuren der Frau verfolgt.</ta>
            <ta e="T57" id="Seg_4137" s="T53">Am Abend holte er die Frau ein.</ta>
            <ta e="T61" id="Seg_4138" s="T57">Er fing sie, tötete und fraß sie.</ta>
            <ta e="T66" id="Seg_4139" s="T61">Er zerriss [ihre] Sachen und warf ihr Gewehr zur Seite.</ta>
            <ta e="T71" id="Seg_4140" s="T66">Von diesem Mädchen blieb nur ein Bein übrig. </ta>
            <ta e="T74" id="Seg_4141" s="T71">Am Abend wartete die Mutter auf sie.</ta>
            <ta e="T77" id="Seg_4142" s="T74">Das Mädchen kam nicht.</ta>
            <ta e="T84" id="Seg_4143" s="T77">Die ganze Nacht dachte die Mutter an das Mädchen, sie schlief nicht.</ta>
            <ta e="T91" id="Seg_4144" s="T84">Am Morgen ging die Mutter das Mädchen suchen.</ta>
            <ta e="T97" id="Seg_4145" s="T91">Entlang den gestrigen Spuren ging die Mutter durch die Taiga.</ta>
            <ta e="T101" id="Seg_4146" s="T97">Von irgendwoher kam der Bär gesprungen.</ta>
            <ta e="T104" id="Seg_4147" s="T101">Er fing die Frau und fraß sie.</ta>
            <ta e="T111" id="Seg_4148" s="T104">Den Spuren der Frau folgend ging der Bär zum Haus der Frau.</ta>
            <ta e="T114" id="Seg_4149" s="T111">Im Haus war niemand.</ta>
            <ta e="T118" id="Seg_4150" s="T114">Er öffnete die Tür und betrat das Haus.</ta>
            <ta e="T126" id="Seg_4151" s="T118">Er hob das Brett an und ging in den Keller.</ta>
            <ta e="T131" id="Seg_4152" s="T126">Im Kuhstall stand eine Kuh mit ihrem Kalb.</ta>
            <ta e="T135" id="Seg_4153" s="T131">Sie hat der Bär nicht angerührt.</ta>
            <ta e="T139" id="Seg_4154" s="T135">Im Keller war der Bär der Hausherr.</ta>
            <ta e="T147" id="Seg_4155" s="T139">Dort gab es Salz, Fischfett, gesalzenes Fleisch und Honig.</ta>
            <ta e="T153" id="Seg_4156" s="T147">Er verstreute das Salz und das gesalzene Fleisch.</ta>
            <ta e="T161" id="Seg_4157" s="T153">Der Bär fraß das Fischfett und trank den Honig.</ta>
            <ta e="T164" id="Seg_4158" s="T161">Es kamen Jäger.</ta>
            <ta e="T167" id="Seg_4159" s="T164">Sie erkennen das Haus nicht.</ta>
            <ta e="T171" id="Seg_4160" s="T167">Irgendwie waren sie ängstlich.</ta>
            <ta e="T176" id="Seg_4161" s="T171">Einer der Jäger ging ins Haus hinein.</ta>
            <ta e="T183" id="Seg_4162" s="T176">Im Haus ging er umher und dachte schweigend nach.</ta>
            <ta e="T186" id="Seg_4163" s="T183">Irgendjemand bellte im Keller.</ta>
            <ta e="T193" id="Seg_4164" s="T186">Der Jäger hörte, er begriff: im Keller ist ein Bär.</ta>
            <ta e="T200" id="Seg_4165" s="T193">Er ging nach draußen zu seinen Kameraden und erzählte, dass ein Bär im Keller liegt.</ta>
            <ta e="T206" id="Seg_4166" s="T200">Die Jäger gingen ins Haus und sie verschlossen den Keller.</ta>
            <ta e="T210" id="Seg_4167" s="T206">Sie gingen raus und schlossen die Tür.</ta>
            <ta e="T216" id="Seg_4168" s="T210">Sie drehten das Brett um und sahen den Bären im Keller.</ta>
            <ta e="T222" id="Seg_4169" s="T216">Die Jäger schoss drei vier Mal.</ta>
            <ta e="T225" id="Seg_4170" s="T222">Er schrie auch.</ta>
            <ta e="T228" id="Seg_4171" s="T225">Dann verstummte der Bär.</ta>
            <ta e="T234" id="Seg_4172" s="T228">Sie drehten zwei Bretter um, sie sahen, dass der Bär gestorben war.</ta>
            <ta e="T237" id="Seg_4173" s="T234">Sie zogen ihn hinaus.</ta>
            <ta e="T242" id="Seg_4174" s="T237">Sie zogen ihm das Fell ab und warfen das Fleisch hinaus.</ta>
            <ta e="T247" id="Seg_4175" s="T242">Sein Fleisch war schwarz, nicht fettig.</ta>
            <ta e="T263" id="Seg_4176" s="T247">Dann, als die Jäger gingen, fanden sie die Mutter und die Tochter, die der getötete Bär tagsüber, während er die Mutter und Tochter gejagt hatte, gefressen hatte.</ta>
            <ta e="T266" id="Seg_4177" s="T263">Der Sohn lebte in Pirij.</ta>
            <ta e="T274" id="Seg_4178" s="T266">Er ging in das Dorf, wo seine Mutter und Schwester gelebt hatten.</ta>
            <ta e="T289" id="Seg_4179" s="T274">Er sagte: ich hätte diesen Bären nicht getötet, sondern das Haus der Mutter mit dem Bären verbrannt.</ta>
            <ta e="T294" id="Seg_4180" s="T289">In der Zukunft werde ich Bären töten, erschießen.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_4181" s="T2">там в тайге была одна деревня</ta>
            <ta e="T10" id="Seg_4182" s="T7">ее звали Марково</ta>
            <ta e="T14" id="Seg_4183" s="T10">там жила одна женщина с дочерью</ta>
            <ta e="T17" id="Seg_4184" s="T14">мужа у нее не было</ta>
            <ta e="T19" id="Seg_4185" s="T17">жила одна</ta>
            <ta e="T22" id="Seg_4186" s="T19">белковала за ягодами ходила ореховала</ta>
            <ta e="T27" id="Seg_4187" s="T22">однажды осенью дочь ушла на охоту</ta>
            <ta e="T30" id="Seg_4188" s="T27">мать была дома</ta>
            <ta e="T36" id="Seg_4189" s="T30">с утра до вечера она ходила белковать</ta>
            <ta e="T41" id="Seg_4190" s="T36">днем проходив к вечеру отправилась домой</ta>
            <ta e="T47" id="Seg_4191" s="T41">долго ли коротко шла обратно домой</ta>
            <ta e="T53" id="Seg_4192" s="T47">весь день ее караулил (следил за ней) медведь по следам женщины</ta>
            <ta e="T57" id="Seg_4193" s="T53">догнал вечером охотницу</ta>
            <ta e="T61" id="Seg_4194" s="T57">схватил убил съел ее</ta>
            <ta e="T66" id="Seg_4195" s="T61">вещи изорвал ружье в сторону бросил</ta>
            <ta e="T71" id="Seg_4196" s="T66">осталась одна нога от этой девушки</ta>
            <ta e="T74" id="Seg_4197" s="T71">вечером мать ждала</ta>
            <ta e="T77" id="Seg_4198" s="T74">дочь не пришла</ta>
            <ta e="T84" id="Seg_4199" s="T77">всю ночь мать о дочери думая не спала</ta>
            <ta e="T91" id="Seg_4200" s="T84">до рассвета мать ушла дочь искать</ta>
            <ta e="T97" id="Seg_4201" s="T91">по вчерашнему ушедшему следу мать шла по тайге</ta>
            <ta e="T101" id="Seg_4202" s="T97">откуда-то выскочил медведь</ta>
            <ta e="T104" id="Seg_4203" s="T101">схватил женщину съел</ta>
            <ta e="T111" id="Seg_4204" s="T104">по следу пришедшей женщины медведь пришел к дому женщины</ta>
            <ta e="T114" id="Seg_4205" s="T111">дома никого не было</ta>
            <ta e="T118" id="Seg_4206" s="T114">дверь открыл зашел в избу</ta>
            <ta e="T126" id="Seg_4207" s="T118">дома походив доску вверх поднял залез в подпол.</ta>
            <ta e="T131" id="Seg_4208" s="T126">в коровнике стояли корова с теленком</ta>
            <ta e="T135" id="Seg_4209" s="T131">их медведь не трогал</ta>
            <ta e="T139" id="Seg_4210" s="T135">в подполе медведь хозяином был</ta>
            <ta e="T147" id="Seg_4211" s="T139">там были соль рыбий жир соленое мясо мед</ta>
            <ta e="T153" id="Seg_4212" s="T147">соль соленое мясо он вывалил раскидал</ta>
            <ta e="T161" id="Seg_4213" s="T153">а рыбий жир и мед медведь выпил</ta>
            <ta e="T164" id="Seg_4214" s="T161">охотники пришли</ta>
            <ta e="T167" id="Seg_4215" s="T164">дом не узнают</ta>
            <ta e="T171" id="Seg_4216" s="T167">почему-то боязливыми были (чувствовали)</ta>
            <ta e="T176" id="Seg_4217" s="T171">один охотник вошел в дом</ta>
            <ta e="T183" id="Seg_4218" s="T176">внутри дома кругом обошел смирно (молча) подумав постоял</ta>
            <ta e="T186" id="Seg_4219" s="T183">кто-то в подполе рявкнул</ta>
            <ta e="T193" id="Seg_4220" s="T186">охотник услышал узнал в подполе есть медведь</ta>
            <ta e="T200" id="Seg_4221" s="T193">на улицу вышел товарищам сказал в подполье лежит медведь</ta>
            <ta e="T206" id="Seg_4222" s="T200">охотники вошли в дом закрыли (забили) подполье</ta>
            <ta e="T216" id="Seg_4223" s="T210">одну доску (половицу) выворотили медведя в подполье увидели</ta>
            <ta e="T222" id="Seg_4224" s="T216">охотники не три четыре раза стреляли</ta>
            <ta e="T225" id="Seg_4225" s="T222">он и ревел</ta>
            <ta e="T228" id="Seg_4226" s="T225">потом медведь замолчал</ta>
            <ta e="T234" id="Seg_4227" s="T228">две половицы (доски) выворотили глядят медведь умер</ta>
            <ta e="T237" id="Seg_4228" s="T234">его вверх вытащили</ta>
            <ta e="T242" id="Seg_4229" s="T237">шкуру ободрали мясо прочь выбросили</ta>
            <ta e="T247" id="Seg_4230" s="T242">мясо было черное не жирное (постное)</ta>
            <ta e="T263" id="Seg_4231" s="T247">потом когда пошли охотники мать нашли подальше дочь которую убитый медведь съел днем во время охоты мать с дочерью</ta>
            <ta e="T266" id="Seg_4232" s="T263">сын жил в Пирию</ta>
            <ta e="T274" id="Seg_4233" s="T266">он ушел в деревню где жила мать его сестра</ta>
            <ta e="T289" id="Seg_4234" s="T274">он так сказал я бы убивать не стал этого медведя а сжег бы материн дом вместе с медведем</ta>
            <ta e="T294" id="Seg_4235" s="T289">я в будущем медведей убивать буду стрелять буду</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T206" id="Seg_4236" s="T200">The first word is not clear.</ta>
            <ta e="T210" id="Seg_4237" s="T206">There is no Russian translation of this sentence.</ta>
            <ta e="T266" id="Seg_4238" s="T263">[AAV:] в Пирино?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
