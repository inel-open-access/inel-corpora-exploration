<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID5A9BCAFD-F85A-DCEA-6383-6F69DEC0C357">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\SAlAn_1965_Soldatka_nar\SAlAn_1965_Soldatka_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">202</ud-information>
            <ud-information attribute-name="# HIAT:w">161</ud-information>
            <ud-information attribute-name="# e">161</ud-information>
            <ud-information attribute-name="# HIAT:u">29</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SAlAn">
            <abbreviation>SAlAn</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SAlAn"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T161" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">qonɨšak</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">täːlʼ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">čʼeːlɨ</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Vičʼasa</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">kinontɨ</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_23" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">Mat</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">täːlʼ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">čʼeːlɨ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">qontɨrsak</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">kinop</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">Soldatka</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_44" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">Na</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">ɛːsa</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">mütɨn</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">moqoːqɨn</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_59" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">Muntɨk</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">soldatɨn</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">tüqɨlɨsɔːtɨn</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">moqona</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_74" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">Stancijantɨ</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">tüsa</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">poest</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_86" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">Müttɨlʼ</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">qumɨtɨp</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">čʼoːtɨsɔːtɨn</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">ijaiːtə</ts>
                  <nts id="Seg_98" n="HIAT:ip">,</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">nälʼaiːtɨ</ts>
                  <nts id="Seg_102" n="HIAT:ip">,</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">imajiːtɨ</ts>
                  <nts id="Seg_106" n="HIAT:ip">,</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">əsiːtə</ts>
                  <nts id="Seg_110" n="HIAT:ip">,</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">nʼenʼäiːtɨ</ts>
                  <nts id="Seg_114" n="HIAT:ip">,</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">əmiːtɨ</ts>
                  <nts id="Seg_118" n="HIAT:ip">,</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">čʼopaiːtɨ</ts>
                  <nts id="Seg_122" n="HIAT:ip">,</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_125" n="HIAT:w" s="T33">ilʼčʼaiːtɨ</ts>
                  <nts id="Seg_126" n="HIAT:ip">,</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_129" n="HIAT:w" s="T34">imɨlʼaiːtɨ</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_132" n="HIAT:w" s="T35">i</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_135" n="HIAT:w" s="T36">kətsaiːtɨ</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_139" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_141" n="HIAT:w" s="T37">Kočʼinʼeiːtɨ</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_144" n="HIAT:w" s="T38">pisɨnʼnʼɔːtɨn</ts>
                  <nts id="Seg_145" n="HIAT:ip">,</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_148" n="HIAT:w" s="T39">ɔːmiːtɨ</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_151" n="HIAT:w" s="T40">čʼuːrɔːtɨn</ts>
                  <nts id="Seg_152" n="HIAT:ip">.</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_155" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_157" n="HIAT:w" s="T41">Meː</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_160" n="HIAT:w" s="T42">qontɨrsɨmɨt</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_163" n="HIAT:w" s="T43">sompɨla</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_166" n="HIAT:w" s="T44">müttɨlʼ</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">qumɨp</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_173" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_175" n="HIAT:w" s="T46">Täpɨn</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_178" n="HIAT:w" s="T47">mulimpɨsɔːtɨn</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_181" n="HIAT:w" s="T48">pissɛːla</ts>
                  <nts id="Seg_182" n="HIAT:ip">.</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_185" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_187" n="HIAT:w" s="T49">Pɔːpɨ</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_190" n="HIAT:w" s="T50">ukkɨr</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_193" n="HIAT:w" s="T51">mütɨlʼ</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_196" n="HIAT:w" s="T52">qup</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_199" n="HIAT:w" s="T53">qolʼčʼisatɨ</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_202" n="HIAT:w" s="T54">ukkɨr</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_205" n="HIAT:w" s="T55">mütɨlʼ</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_208" n="HIAT:w" s="T56">nätap</ts>
                  <nts id="Seg_209" n="HIAT:ip">.</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_212" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_214" n="HIAT:w" s="T57">Nätak</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_217" n="HIAT:w" s="T58">tokaltimpɨsɨtɨ</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_220" n="HIAT:w" s="T59">mütɨlʼ</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_223" n="HIAT:w" s="T60">kostʼumɨp</ts>
                  <nts id="Seg_224" n="HIAT:ip">.</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_227" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_229" n="HIAT:w" s="T61">Täm</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_232" n="HIAT:w" s="T62">ɛːsa</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_235" n="HIAT:w" s="T63">orsa</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_238" n="HIAT:w" s="T64">soma</ts>
                  <nts id="Seg_239" n="HIAT:ip">.</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_242" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_244" n="HIAT:w" s="T65">Täp</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_247" n="HIAT:w" s="T66">qälimpɨsa</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_250" n="HIAT:w" s="T67">moqɨna</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_253" n="HIAT:w" s="T68">mɔːtqɨntɨ</ts>
                  <nts id="Seg_254" n="HIAT:ip">.</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_257" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_259" n="HIAT:w" s="T69">Čʼap</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_262" n="HIAT:w" s="T70">tüsa</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_265" n="HIAT:w" s="T71">mɔːtqɨntɨ</ts>
                  <nts id="Seg_266" n="HIAT:ip">,</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_269" n="HIAT:w" s="T72">qosɨtɨ</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_272" n="HIAT:w" s="T73">ıllə</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_275" n="HIAT:w" s="T74">ɔːlʼčʼimpa</ts>
                  <nts id="Seg_276" n="HIAT:ip">.</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_279" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_281" n="HIAT:w" s="T75">Əsɨtɨ</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_284" n="HIAT:w" s="T76">aj</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_287" n="HIAT:w" s="T77">əmɨtɨ</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_290" n="HIAT:w" s="T78">qumpɔːtɨn</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_293" n="HIAT:w" s="T79">mütoːqɨn</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_297" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_299" n="HIAT:w" s="T80">Täp</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_302" n="HIAT:w" s="T81">qässa</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_305" n="HIAT:w" s="T82">podrugantɨ</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_308" n="HIAT:w" s="T83">mɔːttə</ts>
                  <nts id="Seg_309" n="HIAT:ip">.</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_312" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_314" n="HIAT:w" s="T84">Ontɨ</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_317" n="HIAT:w" s="T85">jap</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_320" n="HIAT:w" s="T86">qola</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_323" n="HIAT:w" s="T87">ɔːntalsɔːtɨn</ts>
                  <nts id="Seg_324" n="HIAT:ip">.</nts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_327" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_329" n="HIAT:w" s="T88">Nɨːnɨ</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_332" n="HIAT:w" s="T89">täp</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_335" n="HIAT:w" s="T90">qəssa</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_338" n="HIAT:w" s="T91">uːčʼila</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_341" n="HIAT:w" s="T92">savhostɨ</ts>
                  <nts id="Seg_342" n="HIAT:ip">,</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_345" n="HIAT:w" s="T93">kut</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_348" n="HIAT:w" s="T94">taqpa</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_351" n="HIAT:w" s="T95">irattɨ</ts>
                  <nts id="Seg_352" n="HIAT:ip">.</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_355" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_357" n="HIAT:w" s="T96">Täpɨn</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_360" n="HIAT:w" s="T97">irat</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_363" n="HIAT:w" s="T98">qusa</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_366" n="HIAT:w" s="T99">mütaːqɨn</ts>
                  <nts id="Seg_367" n="HIAT:ip">.</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_370" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_372" n="HIAT:w" s="T100">Täpɨnɨk</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_375" n="HIAT:w" s="T101">uːčʼiqo</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_378" n="HIAT:w" s="T102">savhosqɨn</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_381" n="HIAT:w" s="T103">sätɨm</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_384" n="HIAT:w" s="T104">ɛːjsa</ts>
                  <nts id="Seg_385" n="HIAT:ip">.</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_388" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_390" n="HIAT:w" s="T105">Täpɨn</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_393" n="HIAT:w" s="T106">nʼi</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_396" n="HIAT:w" s="T107">kutɨ</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_399" n="HIAT:w" s="T108">aša</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_402" n="HIAT:w" s="T109">tɛnɨmɨsɨtɨ</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_405" n="HIAT:w" s="T110">i</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_408" n="HIAT:w" s="T111">täp</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_411" n="HIAT:w" s="T112">nʼi</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_414" n="HIAT:w" s="T113">kutɨp</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_417" n="HIAT:w" s="T114">aša</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_420" n="HIAT:w" s="T115">tɛnɨmɨsɨtɨ</ts>
                  <nts id="Seg_421" n="HIAT:ip">.</nts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_424" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_426" n="HIAT:w" s="T116">Täpɨp</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_429" n="HIAT:w" s="T117">peːlʼa</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_432" n="HIAT:w" s="T118">tüsa</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_435" n="HIAT:w" s="T119">ukkɨr</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_438" n="HIAT:w" s="T120">mütɨlʼ</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_441" n="HIAT:w" s="T121">qup</ts>
                  <nts id="Seg_442" n="HIAT:ip">.</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_445" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_447" n="HIAT:w" s="T122">Tap</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_450" n="HIAT:w" s="T123">čʼeːlʼɨ</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_453" n="HIAT:w" s="T124">qailʼ</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_456" n="HIAT:w" s="T125">kino</ts>
                  <nts id="Seg_457" n="HIAT:ip">?</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_460" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_462" n="HIAT:w" s="T126">Mat</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_465" n="HIAT:w" s="T127">aša</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_468" n="HIAT:w" s="T128">tɛnɨmak</ts>
                  <nts id="Seg_469" n="HIAT:ip">.</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_472" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_474" n="HIAT:w" s="T129">Mat</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_477" n="HIAT:w" s="T130">tap</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_480" n="HIAT:w" s="T131">čʼeːlɨ</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_483" n="HIAT:w" s="T132">qəntak</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_486" n="HIAT:w" s="T133">kinontɨ</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_489" n="HIAT:w" s="T134">seːlʼčʼi</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_492" n="HIAT:w" s="T135">čʼasaqɨlʼ</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_495" n="HIAT:w" s="T136">kinontɨ</ts>
                  <nts id="Seg_496" n="HIAT:ip">.</nts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_499" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_501" n="HIAT:w" s="T137">Meː</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_504" n="HIAT:w" s="T138">klupmɨn</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_507" n="HIAT:w" s="T139">orsa</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_510" n="HIAT:w" s="T140">som</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_513" n="HIAT:w" s="T141">ɛːŋa</ts>
                  <nts id="Seg_514" n="HIAT:ip">.</nts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_517" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_519" n="HIAT:w" s="T142">Som</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_522" n="HIAT:w" s="T143">ɛːŋa</ts>
                  <nts id="Seg_523" n="HIAT:ip">,</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_526" n="HIAT:w" s="T144">naqot</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_529" n="HIAT:w" s="T145">talʼlʼimɔːtpɔːtɨm</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_532" n="HIAT:w" s="T146">kočʼek</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_535" n="HIAT:w" s="T147">tuparetkatan</ts>
                  <nts id="Seg_536" n="HIAT:ip">.</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_539" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_541" n="HIAT:w" s="T148">Vičʼa</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_544" n="HIAT:w" s="T149">qonɨša</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_547" n="HIAT:w" s="T150">lapkontɨ</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_550" n="HIAT:w" s="T151">i</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_553" n="HIAT:w" s="T152">taːttɨsɨtɨ</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_556" n="HIAT:w" s="T153">nʼäjep</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_559" n="HIAT:w" s="T154">i</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_562" n="HIAT:w" s="T155">čʼɔːpɨ</ts>
                  <nts id="Seg_563" n="HIAT:ip">.</nts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_566" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_568" n="HIAT:w" s="T156">Meː</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_571" n="HIAT:w" s="T157">tiː</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_574" n="HIAT:w" s="T158">amɨrtɛntɔːmɨn</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_577" n="HIAT:w" s="T159">i</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_580" n="HIAT:w" s="T160">näkäntɛntɔːmɨn</ts>
                  <nts id="Seg_581" n="HIAT:ip">.</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T161" id="Seg_583" n="sc" s="T0">
               <ts e="T1" id="Seg_585" n="e" s="T0">Man </ts>
               <ts e="T2" id="Seg_587" n="e" s="T1">qonɨšak </ts>
               <ts e="T3" id="Seg_589" n="e" s="T2">täːlʼ </ts>
               <ts e="T4" id="Seg_591" n="e" s="T3">čʼeːlɨ </ts>
               <ts e="T5" id="Seg_593" n="e" s="T4">Vičʼasa </ts>
               <ts e="T6" id="Seg_595" n="e" s="T5">kinontɨ. </ts>
               <ts e="T7" id="Seg_597" n="e" s="T6">Mat </ts>
               <ts e="T8" id="Seg_599" n="e" s="T7">täːlʼ </ts>
               <ts e="T9" id="Seg_601" n="e" s="T8">čʼeːlɨ </ts>
               <ts e="T10" id="Seg_603" n="e" s="T9">qontɨrsak </ts>
               <ts e="T11" id="Seg_605" n="e" s="T10">kinop </ts>
               <ts e="T12" id="Seg_607" n="e" s="T11">Soldatka. </ts>
               <ts e="T13" id="Seg_609" n="e" s="T12">Na </ts>
               <ts e="T14" id="Seg_611" n="e" s="T13">ɛːsa </ts>
               <ts e="T15" id="Seg_613" n="e" s="T14">mütɨn </ts>
               <ts e="T16" id="Seg_615" n="e" s="T15">moqoːqɨn. </ts>
               <ts e="T17" id="Seg_617" n="e" s="T16">Muntɨk </ts>
               <ts e="T18" id="Seg_619" n="e" s="T17">soldatɨn </ts>
               <ts e="T19" id="Seg_621" n="e" s="T18">tüqɨlɨsɔːtɨn </ts>
               <ts e="T20" id="Seg_623" n="e" s="T19">moqona. </ts>
               <ts e="T21" id="Seg_625" n="e" s="T20">Stancijantɨ </ts>
               <ts e="T22" id="Seg_627" n="e" s="T21">tüsa </ts>
               <ts e="T23" id="Seg_629" n="e" s="T22">poest. </ts>
               <ts e="T24" id="Seg_631" n="e" s="T23">Müttɨlʼ </ts>
               <ts e="T25" id="Seg_633" n="e" s="T24">qumɨtɨp </ts>
               <ts e="T26" id="Seg_635" n="e" s="T25">čʼoːtɨsɔːtɨn </ts>
               <ts e="T27" id="Seg_637" n="e" s="T26">ijaiːtə, </ts>
               <ts e="T28" id="Seg_639" n="e" s="T27">nälʼaiːtɨ, </ts>
               <ts e="T29" id="Seg_641" n="e" s="T28">imajiːtɨ, </ts>
               <ts e="T30" id="Seg_643" n="e" s="T29">əsiːtə, </ts>
               <ts e="T31" id="Seg_645" n="e" s="T30">nʼenʼäiːtɨ, </ts>
               <ts e="T32" id="Seg_647" n="e" s="T31">əmiːtɨ, </ts>
               <ts e="T33" id="Seg_649" n="e" s="T32">čʼopaiːtɨ, </ts>
               <ts e="T34" id="Seg_651" n="e" s="T33">ilʼčʼaiːtɨ, </ts>
               <ts e="T35" id="Seg_653" n="e" s="T34">imɨlʼaiːtɨ </ts>
               <ts e="T36" id="Seg_655" n="e" s="T35">i </ts>
               <ts e="T37" id="Seg_657" n="e" s="T36">kətsaiːtɨ. </ts>
               <ts e="T38" id="Seg_659" n="e" s="T37">Kočʼinʼeiːtɨ </ts>
               <ts e="T39" id="Seg_661" n="e" s="T38">pisɨnʼnʼɔːtɨn, </ts>
               <ts e="T40" id="Seg_663" n="e" s="T39">ɔːmiːtɨ </ts>
               <ts e="T41" id="Seg_665" n="e" s="T40">čʼuːrɔːtɨn. </ts>
               <ts e="T42" id="Seg_667" n="e" s="T41">Meː </ts>
               <ts e="T43" id="Seg_669" n="e" s="T42">qontɨrsɨmɨt </ts>
               <ts e="T44" id="Seg_671" n="e" s="T43">sompɨla </ts>
               <ts e="T45" id="Seg_673" n="e" s="T44">müttɨlʼ </ts>
               <ts e="T46" id="Seg_675" n="e" s="T45">qumɨp. </ts>
               <ts e="T47" id="Seg_677" n="e" s="T46">Täpɨn </ts>
               <ts e="T48" id="Seg_679" n="e" s="T47">mulimpɨsɔːtɨn </ts>
               <ts e="T49" id="Seg_681" n="e" s="T48">pissɛːla. </ts>
               <ts e="T50" id="Seg_683" n="e" s="T49">Pɔːpɨ </ts>
               <ts e="T51" id="Seg_685" n="e" s="T50">ukkɨr </ts>
               <ts e="T52" id="Seg_687" n="e" s="T51">mütɨlʼ </ts>
               <ts e="T53" id="Seg_689" n="e" s="T52">qup </ts>
               <ts e="T54" id="Seg_691" n="e" s="T53">qolʼčʼisatɨ </ts>
               <ts e="T55" id="Seg_693" n="e" s="T54">ukkɨr </ts>
               <ts e="T56" id="Seg_695" n="e" s="T55">mütɨlʼ </ts>
               <ts e="T57" id="Seg_697" n="e" s="T56">nätap. </ts>
               <ts e="T58" id="Seg_699" n="e" s="T57">Nätak </ts>
               <ts e="T59" id="Seg_701" n="e" s="T58">tokaltimpɨsɨtɨ </ts>
               <ts e="T60" id="Seg_703" n="e" s="T59">mütɨlʼ </ts>
               <ts e="T61" id="Seg_705" n="e" s="T60">kostʼumɨp. </ts>
               <ts e="T62" id="Seg_707" n="e" s="T61">Täm </ts>
               <ts e="T63" id="Seg_709" n="e" s="T62">ɛːsa </ts>
               <ts e="T64" id="Seg_711" n="e" s="T63">orsa </ts>
               <ts e="T65" id="Seg_713" n="e" s="T64">soma. </ts>
               <ts e="T66" id="Seg_715" n="e" s="T65">Täp </ts>
               <ts e="T67" id="Seg_717" n="e" s="T66">qälimpɨsa </ts>
               <ts e="T68" id="Seg_719" n="e" s="T67">moqɨna </ts>
               <ts e="T69" id="Seg_721" n="e" s="T68">mɔːtqɨntɨ. </ts>
               <ts e="T70" id="Seg_723" n="e" s="T69">Čʼap </ts>
               <ts e="T71" id="Seg_725" n="e" s="T70">tüsa </ts>
               <ts e="T72" id="Seg_727" n="e" s="T71">mɔːtqɨntɨ, </ts>
               <ts e="T73" id="Seg_729" n="e" s="T72">qosɨtɨ </ts>
               <ts e="T74" id="Seg_731" n="e" s="T73">ıllə </ts>
               <ts e="T75" id="Seg_733" n="e" s="T74">ɔːlʼčʼimpa. </ts>
               <ts e="T76" id="Seg_735" n="e" s="T75">Əsɨtɨ </ts>
               <ts e="T77" id="Seg_737" n="e" s="T76">aj </ts>
               <ts e="T78" id="Seg_739" n="e" s="T77">əmɨtɨ </ts>
               <ts e="T79" id="Seg_741" n="e" s="T78">qumpɔːtɨn </ts>
               <ts e="T80" id="Seg_743" n="e" s="T79">mütoːqɨn. </ts>
               <ts e="T81" id="Seg_745" n="e" s="T80">Täp </ts>
               <ts e="T82" id="Seg_747" n="e" s="T81">qässa </ts>
               <ts e="T83" id="Seg_749" n="e" s="T82">podrugantɨ </ts>
               <ts e="T84" id="Seg_751" n="e" s="T83">mɔːttə. </ts>
               <ts e="T85" id="Seg_753" n="e" s="T84">Ontɨ </ts>
               <ts e="T86" id="Seg_755" n="e" s="T85">jap </ts>
               <ts e="T87" id="Seg_757" n="e" s="T86">qola </ts>
               <ts e="T88" id="Seg_759" n="e" s="T87">ɔːntalsɔːtɨn. </ts>
               <ts e="T89" id="Seg_761" n="e" s="T88">Nɨːnɨ </ts>
               <ts e="T90" id="Seg_763" n="e" s="T89">täp </ts>
               <ts e="T91" id="Seg_765" n="e" s="T90">qəssa </ts>
               <ts e="T92" id="Seg_767" n="e" s="T91">uːčʼila </ts>
               <ts e="T93" id="Seg_769" n="e" s="T92">savhostɨ, </ts>
               <ts e="T94" id="Seg_771" n="e" s="T93">kut </ts>
               <ts e="T95" id="Seg_773" n="e" s="T94">taqpa </ts>
               <ts e="T96" id="Seg_775" n="e" s="T95">irattɨ. </ts>
               <ts e="T97" id="Seg_777" n="e" s="T96">Täpɨn </ts>
               <ts e="T98" id="Seg_779" n="e" s="T97">irat </ts>
               <ts e="T99" id="Seg_781" n="e" s="T98">qusa </ts>
               <ts e="T100" id="Seg_783" n="e" s="T99">mütaːqɨn. </ts>
               <ts e="T101" id="Seg_785" n="e" s="T100">Täpɨnɨk </ts>
               <ts e="T102" id="Seg_787" n="e" s="T101">uːčʼiqo </ts>
               <ts e="T103" id="Seg_789" n="e" s="T102">savhosqɨn </ts>
               <ts e="T104" id="Seg_791" n="e" s="T103">sätɨm </ts>
               <ts e="T105" id="Seg_793" n="e" s="T104">ɛːjsa. </ts>
               <ts e="T106" id="Seg_795" n="e" s="T105">Täpɨn </ts>
               <ts e="T107" id="Seg_797" n="e" s="T106">nʼi </ts>
               <ts e="T108" id="Seg_799" n="e" s="T107">kutɨ </ts>
               <ts e="T109" id="Seg_801" n="e" s="T108">aša </ts>
               <ts e="T110" id="Seg_803" n="e" s="T109">tɛnɨmɨsɨtɨ </ts>
               <ts e="T111" id="Seg_805" n="e" s="T110">i </ts>
               <ts e="T112" id="Seg_807" n="e" s="T111">täp </ts>
               <ts e="T113" id="Seg_809" n="e" s="T112">nʼi </ts>
               <ts e="T114" id="Seg_811" n="e" s="T113">kutɨp </ts>
               <ts e="T115" id="Seg_813" n="e" s="T114">aša </ts>
               <ts e="T116" id="Seg_815" n="e" s="T115">tɛnɨmɨsɨtɨ. </ts>
               <ts e="T117" id="Seg_817" n="e" s="T116">Täpɨp </ts>
               <ts e="T118" id="Seg_819" n="e" s="T117">peːlʼa </ts>
               <ts e="T119" id="Seg_821" n="e" s="T118">tüsa </ts>
               <ts e="T120" id="Seg_823" n="e" s="T119">ukkɨr </ts>
               <ts e="T121" id="Seg_825" n="e" s="T120">mütɨlʼ </ts>
               <ts e="T122" id="Seg_827" n="e" s="T121">qup. </ts>
               <ts e="T123" id="Seg_829" n="e" s="T122">Tap </ts>
               <ts e="T124" id="Seg_831" n="e" s="T123">čʼeːlʼɨ </ts>
               <ts e="T125" id="Seg_833" n="e" s="T124">qailʼ </ts>
               <ts e="T126" id="Seg_835" n="e" s="T125">kino? </ts>
               <ts e="T127" id="Seg_837" n="e" s="T126">Mat </ts>
               <ts e="T128" id="Seg_839" n="e" s="T127">aša </ts>
               <ts e="T129" id="Seg_841" n="e" s="T128">tɛnɨmak. </ts>
               <ts e="T130" id="Seg_843" n="e" s="T129">Mat </ts>
               <ts e="T131" id="Seg_845" n="e" s="T130">tap </ts>
               <ts e="T132" id="Seg_847" n="e" s="T131">čʼeːlɨ </ts>
               <ts e="T133" id="Seg_849" n="e" s="T132">qəntak </ts>
               <ts e="T134" id="Seg_851" n="e" s="T133">kinontɨ </ts>
               <ts e="T135" id="Seg_853" n="e" s="T134">seːlʼčʼi </ts>
               <ts e="T136" id="Seg_855" n="e" s="T135">čʼasaqɨlʼ </ts>
               <ts e="T137" id="Seg_857" n="e" s="T136">kinontɨ. </ts>
               <ts e="T138" id="Seg_859" n="e" s="T137">Meː </ts>
               <ts e="T139" id="Seg_861" n="e" s="T138">klupmɨn </ts>
               <ts e="T140" id="Seg_863" n="e" s="T139">orsa </ts>
               <ts e="T141" id="Seg_865" n="e" s="T140">som </ts>
               <ts e="T142" id="Seg_867" n="e" s="T141">ɛːŋa. </ts>
               <ts e="T143" id="Seg_869" n="e" s="T142">Som </ts>
               <ts e="T144" id="Seg_871" n="e" s="T143">ɛːŋa, </ts>
               <ts e="T145" id="Seg_873" n="e" s="T144">naqot </ts>
               <ts e="T146" id="Seg_875" n="e" s="T145">talʼlʼimɔːtpɔːtɨm </ts>
               <ts e="T147" id="Seg_877" n="e" s="T146">kočʼek </ts>
               <ts e="T148" id="Seg_879" n="e" s="T147">tuparetkatan. </ts>
               <ts e="T149" id="Seg_881" n="e" s="T148">Vičʼa </ts>
               <ts e="T150" id="Seg_883" n="e" s="T149">qonɨša </ts>
               <ts e="T151" id="Seg_885" n="e" s="T150">lapkontɨ </ts>
               <ts e="T152" id="Seg_887" n="e" s="T151">i </ts>
               <ts e="T153" id="Seg_889" n="e" s="T152">taːttɨsɨtɨ </ts>
               <ts e="T154" id="Seg_891" n="e" s="T153">nʼäjep </ts>
               <ts e="T155" id="Seg_893" n="e" s="T154">i </ts>
               <ts e="T156" id="Seg_895" n="e" s="T155">čʼɔːpɨ. </ts>
               <ts e="T157" id="Seg_897" n="e" s="T156">Meː </ts>
               <ts e="T158" id="Seg_899" n="e" s="T157">tiː </ts>
               <ts e="T159" id="Seg_901" n="e" s="T158">amɨrtɛntɔːmɨn </ts>
               <ts e="T160" id="Seg_903" n="e" s="T159">i </ts>
               <ts e="T161" id="Seg_905" n="e" s="T160">näkäntɛntɔːmɨn. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_906" s="T0">SAlAn_1965_Soldatka_nar.001 (001.001)</ta>
            <ta e="T12" id="Seg_907" s="T6">SAlAn_1965_Soldatka_nar.002 (001.002)</ta>
            <ta e="T16" id="Seg_908" s="T12">SAlAn_1965_Soldatka_nar.003 (002.001)</ta>
            <ta e="T20" id="Seg_909" s="T16">SAlAn_1965_Soldatka_nar.004 (002.002)</ta>
            <ta e="T23" id="Seg_910" s="T20">SAlAn_1965_Soldatka_nar.005 (002.003)</ta>
            <ta e="T37" id="Seg_911" s="T23">SAlAn_1965_Soldatka_nar.006 (002.004)</ta>
            <ta e="T41" id="Seg_912" s="T37">SAlAn_1965_Soldatka_nar.007 (002.005)</ta>
            <ta e="T46" id="Seg_913" s="T41">SAlAn_1965_Soldatka_nar.008 (002.006)</ta>
            <ta e="T49" id="Seg_914" s="T46">SAlAn_1965_Soldatka_nar.009 (002.007)</ta>
            <ta e="T57" id="Seg_915" s="T49">SAlAn_1965_Soldatka_nar.010 (002.008)</ta>
            <ta e="T61" id="Seg_916" s="T57">SAlAn_1965_Soldatka_nar.011 (002.009)</ta>
            <ta e="T65" id="Seg_917" s="T61">SAlAn_1965_Soldatka_nar.012 (002.010)</ta>
            <ta e="T69" id="Seg_918" s="T65">SAlAn_1965_Soldatka_nar.013 (002.011)</ta>
            <ta e="T75" id="Seg_919" s="T69">SAlAn_1965_Soldatka_nar.014 (002.012)</ta>
            <ta e="T80" id="Seg_920" s="T75">SAlAn_1965_Soldatka_nar.015 (002.013)</ta>
            <ta e="T84" id="Seg_921" s="T80">SAlAn_1965_Soldatka_nar.016 (002.014)</ta>
            <ta e="T88" id="Seg_922" s="T84">SAlAn_1965_Soldatka_nar.017 (002.015)</ta>
            <ta e="T96" id="Seg_923" s="T88">SAlAn_1965_Soldatka_nar.018 (002.016)</ta>
            <ta e="T100" id="Seg_924" s="T96">SAlAn_1965_Soldatka_nar.019 (002.017)</ta>
            <ta e="T105" id="Seg_925" s="T100">SAlAn_1965_Soldatka_nar.020 (002.018)</ta>
            <ta e="T116" id="Seg_926" s="T105">SAlAn_1965_Soldatka_nar.021 (002.019)</ta>
            <ta e="T122" id="Seg_927" s="T116">SAlAn_1965_Soldatka_nar.022 (002.020)</ta>
            <ta e="T126" id="Seg_928" s="T122">SAlAn_1965_Soldatka_nar.023 (003.001)</ta>
            <ta e="T129" id="Seg_929" s="T126">SAlAn_1965_Soldatka_nar.024 (003.002)</ta>
            <ta e="T137" id="Seg_930" s="T129">SAlAn_1965_Soldatka_nar.025 (003.003)</ta>
            <ta e="T142" id="Seg_931" s="T137">SAlAn_1965_Soldatka_nar.026 (003.004)</ta>
            <ta e="T148" id="Seg_932" s="T142">SAlAn_1965_Soldatka_nar.027 (003.005)</ta>
            <ta e="T156" id="Seg_933" s="T148">SAlAn_1965_Soldatka_nar.028 (003.006)</ta>
            <ta e="T161" id="Seg_934" s="T156">SAlAn_1965_Soldatka_nar.029 (003.007)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_935" s="T0">ман ′kонышак ′тӓlче̄лы ′вичаса ки′нонты.</ta>
            <ta e="T12" id="Seg_936" s="T6">Мат ′тӓl′че̄лы ′kондырсак ′киноп солдатка.</ta>
            <ta e="T16" id="Seg_937" s="T12">на ′ӓ̄са ′мӱтын[т] мо′kоkын.</ta>
            <ta e="T20" id="Seg_938" s="T16">мунд̂ык солд̂атын ′тӱ̄kылысотын ′моkона.</ta>
            <ta e="T23" id="Seg_939" s="T20">станциjанды ′тӱса ′поест.</ta>
            <ta e="T37" id="Seg_940" s="T23">′мӱ̄ттыl ′kумытып ′чо̄тысотын ′иjаитъ, ′нӓlа′иты, ′имаjиты, ъ̊̄ситъ, ′нʼӓнʼӓӣты, ъ̊′миты, ′чопаиты, ′иlджаиты ′имыlаиты и ′къ̊тцаиты.</ta>
            <ta e="T41" id="Seg_941" s="T37">′кочи неиты ′писынʼӧтын о̨̄′миты чӯ′ротын.</ta>
            <ta e="T46" id="Seg_942" s="T41">ме ′kондырсымыт ′с́омбыlа ′мӱ̄ттыl kумып.</ta>
            <ta e="T49" id="Seg_943" s="T46">′тӓпын ′мӯlимб̂ысотын ′писӓlа.</ta>
            <ta e="T57" id="Seg_944" s="T49">′по̄пы ′уккыр ′мӱ̄тыl kуп ′kоlчисаты уккыр ′мӱ̄тыl ′нӓтап.</ta>
            <ta e="T61" id="Seg_945" s="T57">′нӓтак ′токаlд̂имб̂ысыты ′мӱ̄дыl костʼӱ′мып.</ta>
            <ta e="T65" id="Seg_946" s="T61">тӓм ′ӓ̄са ′орса ′сома.</ta>
            <ta e="T69" id="Seg_947" s="T65">тӓп ′kӓ̄lимбыса ′моkына ′мотkынды.</ta>
            <ta e="T75" id="Seg_948" s="T69">чап ′тӱ̄с[з̂]а ′мотkынты. ′kо̄сыты ′иллъ[а] ′оlджимба.</ta>
            <ta e="T80" id="Seg_949" s="T75">′ъ̊̄сыты ′ай ′ъ̊̄мыты ′kумп[б̂]о̄тын ′мӱ̄то‵ɣ[k]ын.</ta>
            <ta e="T84" id="Seg_950" s="T80">тӓп ′kӓсса по′друганд̂ы ′мо̄ттъ.</ta>
            <ta e="T88" id="Seg_951" s="T84">′ондыjап ′kо̄lа ′ондаl‵сотын.</ta>
            <ta e="T96" id="Seg_952" s="T88">′ныны ′тӓп ′kъ̊сса ′ӯчиlа сав′хосты, кут ′такпа ′иратды.</ta>
            <ta e="T100" id="Seg_953" s="T96">′тӓпын ′ират ′kӯса мӱ̄таɣ[k]ын.</ta>
            <ta e="T105" id="Seg_954" s="T100">′тӓпынык ′ӯчиkо сав′хосkын ′сӓтым ′ӓ̄йса.</ta>
            <ta e="T116" id="Seg_955" s="T105">′тӓпын ни′куты ′аша ′тӓнымысыты и тӓп ни′кутып ′аша ′тӓ̄нымы сыты.</ta>
            <ta e="T122" id="Seg_956" s="T116">′тӓпып ′пе̄лʼа ′тӱ̄са ′уккыр ′мӱ̄дыl kуп.</ta>
            <ta e="T126" id="Seg_957" s="T122">тап че̄лʼы ′kаилʼ ки′но?</ta>
            <ta e="T129" id="Seg_958" s="T126">мат ′аша ′тӓ̄нымак.</ta>
            <ta e="T137" id="Seg_959" s="T129">мат ′тапче̄лы ′kъ̊ндак ки′нонды ′се̨lджи часаkыl ки′нонды.</ta>
            <ta e="T142" id="Seg_960" s="T137">ме̄ ′клупмын ′орса со̄м еңа.</ta>
            <ta e="T148" id="Seg_961" s="T142">′сом ′еңа ′накот ′та̄лʼимот′потым ′кочʼек туба′реткатан.</ta>
            <ta e="T156" id="Seg_962" s="T148">′Вӣча ′kоныша ′лапконды и ′таттысыд̂ы ′н'ӓjеп и ′чо̨̄п[б̂]ы.</ta>
            <ta e="T161" id="Seg_963" s="T156">ме тӣ ′амыртенд̂омын и ′нӓкӓнтӓнтомын.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_964" s="T0">man qonɨšak tälʼčʼeːlɨ vičʼasa kinontɨ.</ta>
            <ta e="T12" id="Seg_965" s="T6">mat tälʼčʼeːlɨ qondɨrsak kinop soldatka.</ta>
            <ta e="T16" id="Seg_966" s="T12">na äːsa mütɨn[t] moqoqɨn.</ta>
            <ta e="T20" id="Seg_967" s="T16">mund̂ɨk sold̂atɨn tüːqɨlɨsotɨn moqona.</ta>
            <ta e="T23" id="Seg_968" s="T20">stancijandɨ tüsa poest.</ta>
            <ta e="T37" id="Seg_969" s="T23">müːttɨlʼ qumɨtɨp čʼoːtɨsotɨn ijaitə, nälʼaitɨ, imajitɨ, əːsitə, nʼänʼäiːtɨ, əmitɨ, čʼopaitɨ, ilʼdжaitɨ imɨlʼaitɨ i kətcaitɨ.</ta>
            <ta e="T41" id="Seg_970" s="T37">kočʼi neitɨ pisɨnʼötɨn oːmitɨ čʼuːrotɨn.</ta>
            <ta e="T46" id="Seg_971" s="T41">me qondɨrsɨmɨt śompɨlʼa müːttɨlʼ qumɨp.</ta>
            <ta e="T49" id="Seg_972" s="T46">täpɨn muːlʼimp̂ɨsotɨn pisälʼa.</ta>
            <ta e="T57" id="Seg_973" s="T49">poːpɨ ukkɨr müːtɨlʼ qup qolʼčʼisatɨ ukkɨr müːtɨlʼ nätap. </ta>
            <ta e="T61" id="Seg_974" s="T57">nätak tokalʼd̂imp̂ɨsɨtɨ müːdɨlʼ kosčʼümɨp.</ta>
            <ta e="T65" id="Seg_975" s="T61">täm äːsa orsa soma.</ta>
            <ta e="T69" id="Seg_976" s="T65">täp qäːlʼimpɨsa moqɨna motqɨndɨ.</ta>
            <ta e="T75" id="Seg_977" s="T69">čʼap tüːs [ẑ] a motqɨntɨ qoːsɨtɨ illə [a] olʼdжimpa.</ta>
            <ta e="T80" id="Seg_978" s="T75">əːsɨtɨ aj əːmɨtɨ qump [p̂] oːtɨn müːtoq [q] ɨn.</ta>
            <ta e="T84" id="Seg_979" s="T80">täp qässa podrugand̂ɨ moːttə.</ta>
            <ta e="T88" id="Seg_980" s="T84">täp qässa podrugand̂ɨ moːttə.</ta>
            <ta e="T96" id="Seg_981" s="T88">nɨnɨ täp qəssa uːčʼilʼa savhostɨ, kut takpa iratdɨ.</ta>
            <ta e="T100" id="Seg_982" s="T96">täpɨn rat quːsa müːtaq [q] ɨn.</ta>
            <ta e="T105" id="Seg_983" s="T100">täpɨnɨk uːčʼiqo savhosqɨn sätɨm äːjsa.</ta>
            <ta e="T116" id="Seg_984" s="T105">täpɨn nikutɨ aša tänɨmɨsɨtɨ i täp nikutɨp aša täːnɨmɨ sɨtɨ.</ta>
            <ta e="T122" id="Seg_985" s="T116">täpɨp peːlʼa tüːsa ukkɨr müːdɨlʼ qup.</ta>
            <ta e="T126" id="Seg_986" s="T122">tap čʼeːlʼɨ qailʼ kino?</ta>
            <ta e="T129" id="Seg_987" s="T126">mat aša täːnɨmak.</ta>
            <ta e="T137" id="Seg_988" s="T129">mat tapčʼeːlɨ qəndak kinondɨ selʼdжi čʼasaqɨlʼ kinondɨ.</ta>
            <ta e="T142" id="Seg_989" s="T137">meː klupmɨn orsa soːm eŋa.</ta>
            <ta e="T148" id="Seg_990" s="T142">som eŋa nakot taːlʼimotpotɨm kočʼʼek tuparetkatan.</ta>
            <ta e="T156" id="Seg_991" s="T148">Viːčʼa qonɨša lapkondɨ i tattɨsɨd̂ɨ nʼäjep i čʼoːp [p̂] ɨ.</ta>
            <ta e="T161" id="Seg_992" s="T156">me tiː amɨrtend̂omɨn i näkäntäntomɨn. </ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_993" s="T0">Man qonɨšak täːlʼ čʼeːlɨ Vičʼasa kinontɨ. </ta>
            <ta e="T12" id="Seg_994" s="T6">Mat täːlʼ čʼeːlɨ qontɨrsak kinop Soldatka. </ta>
            <ta e="T16" id="Seg_995" s="T12">Na ɛːsa mütɨn moqoːqɨn. </ta>
            <ta e="T20" id="Seg_996" s="T16">Muntɨk soldatɨn tüqɨlɨsɔːtɨn moqona. </ta>
            <ta e="T23" id="Seg_997" s="T20">Stancijantɨ tüsa poest. </ta>
            <ta e="T37" id="Seg_998" s="T23">Müttɨlʼ qumɨtɨp čʼoːtɨsɔːtɨn ijaiːtə, nälʼaiːtɨ, imajiːtɨ, əsiːtə, nʼenʼäiːtɨ, əmiːtɨ, čʼopaiːtɨ, ilʼčʼaiːtɨ, imɨlʼaiːtɨ i kətsaiːtɨ. </ta>
            <ta e="T41" id="Seg_999" s="T37">Kočʼinʼeiːtɨ pisɨnʼnʼɔːtɨn, ɔːmiːtɨ čʼuːrɔːtɨn. </ta>
            <ta e="T46" id="Seg_1000" s="T41">Meː qontɨrsɨmɨt sompɨla müttɨlʼ qumɨp. </ta>
            <ta e="T49" id="Seg_1001" s="T46">Täpɨn mulimpɨsɔːtɨn pissɛːla. </ta>
            <ta e="T57" id="Seg_1002" s="T49">Pɔːpɨ ukkɨr mütɨlʼ qup qolʼčʼisatɨ ukkɨr mütɨlʼ nätap. </ta>
            <ta e="T61" id="Seg_1003" s="T57">Nätak tokaltimpɨsɨtɨ mütɨlʼ kostʼumɨp. </ta>
            <ta e="T65" id="Seg_1004" s="T61">Täm ɛːsa orsa soma. </ta>
            <ta e="T69" id="Seg_1005" s="T65">Täp qälimpɨsa moqɨna mɔːtqɨntɨ. </ta>
            <ta e="T75" id="Seg_1006" s="T69">Čʼap tüsa mɔːtqɨntɨ, qosɨtɨ ıllə ɔːlʼčʼimpa. </ta>
            <ta e="T80" id="Seg_1007" s="T75">Əsɨtɨ aj əmɨtɨ qumpɔːtɨn mütoːqɨn. </ta>
            <ta e="T84" id="Seg_1008" s="T80">Täp qässa podrugantɨ mɔːttə. </ta>
            <ta e="T88" id="Seg_1009" s="T84">Ontɨ jap qola ɔːntalsɔːtɨn. </ta>
            <ta e="T96" id="Seg_1010" s="T88">Nɨːnɨ täp qəssa uːčʼila savhostɨ, kut taqpa irattɨ. </ta>
            <ta e="T100" id="Seg_1011" s="T96">Täpɨn irat qusa mütaːqɨn. </ta>
            <ta e="T105" id="Seg_1012" s="T100">Täpɨnɨk uːčʼiqo savhosqɨn sätɨm ɛːjsa. </ta>
            <ta e="T116" id="Seg_1013" s="T105">Täpɨn nʼi kutɨ aša tɛnɨmɨsɨtɨ i täp nʼi kutɨp aša tɛnɨmɨsɨtɨ. </ta>
            <ta e="T122" id="Seg_1014" s="T116">Täpɨp peːlʼa tüsa ukkɨr mütɨlʼ qup. </ta>
            <ta e="T126" id="Seg_1015" s="T122">Tap čʼeːlʼɨ qailʼ kino? </ta>
            <ta e="T129" id="Seg_1016" s="T126">Mat aša tɛnɨmak. </ta>
            <ta e="T137" id="Seg_1017" s="T129">Mat tap čʼeːlɨ qəntak kinontɨ seːlʼčʼi čʼasaqɨlʼ kinontɨ. </ta>
            <ta e="T142" id="Seg_1018" s="T137">Meː klupmɨn orsa som ɛːŋa. </ta>
            <ta e="T148" id="Seg_1019" s="T142">Som ɛːŋa, naqot talʼlʼimɔːtpɔːtɨm kočʼek tuparetkatan. </ta>
            <ta e="T156" id="Seg_1020" s="T148">Vičʼa qonɨša lapkontɨ i taːttɨsɨtɨ nʼäjep i čʼɔːpɨ. </ta>
            <ta e="T161" id="Seg_1021" s="T156">Meː tiː amɨrtɛntɔːmɨn i näkäntɛntɔːmɨn. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1022" s="T0">man</ta>
            <ta e="T2" id="Seg_1023" s="T1">qon-ɨ-ša-k</ta>
            <ta e="T3" id="Seg_1024" s="T2">täːlʼ</ta>
            <ta e="T4" id="Seg_1025" s="T3">čʼeːlɨ</ta>
            <ta e="T5" id="Seg_1026" s="T4">Vičʼa-sa</ta>
            <ta e="T6" id="Seg_1027" s="T5">kino-ntɨ</ta>
            <ta e="T7" id="Seg_1028" s="T6">mat</ta>
            <ta e="T8" id="Seg_1029" s="T7">täːlʼ</ta>
            <ta e="T9" id="Seg_1030" s="T8">čʼeːlɨ</ta>
            <ta e="T10" id="Seg_1031" s="T9">qo-ntɨr-sa-k</ta>
            <ta e="T11" id="Seg_1032" s="T10">kino-p</ta>
            <ta e="T12" id="Seg_1033" s="T11">soldatka</ta>
            <ta e="T13" id="Seg_1034" s="T12">na</ta>
            <ta e="T14" id="Seg_1035" s="T13">ɛː-sa</ta>
            <ta e="T15" id="Seg_1036" s="T14">mütɨ-n</ta>
            <ta e="T16" id="Seg_1037" s="T15">moqoː-qɨn</ta>
            <ta e="T17" id="Seg_1038" s="T16">muntɨk</ta>
            <ta e="T18" id="Seg_1039" s="T17">soldat-ɨ-n</ta>
            <ta e="T19" id="Seg_1040" s="T18">tü-qɨlɨ-sɔː-tɨn</ta>
            <ta e="T20" id="Seg_1041" s="T19">moqona</ta>
            <ta e="T21" id="Seg_1042" s="T20">stancija-ntɨ</ta>
            <ta e="T22" id="Seg_1043" s="T21">tü-sa</ta>
            <ta e="T23" id="Seg_1044" s="T22">poest</ta>
            <ta e="T24" id="Seg_1045" s="T23">müttɨ-lʼ</ta>
            <ta e="T25" id="Seg_1046" s="T24">qum-ɨ-t-ɨ-p</ta>
            <ta e="T26" id="Seg_1047" s="T25">čʼoːtɨ-sɔː-tɨn</ta>
            <ta e="T27" id="Seg_1048" s="T26">ija-iː-tə</ta>
            <ta e="T28" id="Seg_1049" s="T27">nälʼa-iː-tɨ</ta>
            <ta e="T29" id="Seg_1050" s="T28">ima-jiː-tɨ</ta>
            <ta e="T30" id="Seg_1051" s="T29">əs-iː-tə</ta>
            <ta e="T31" id="Seg_1052" s="T30">nʼenʼä-iː-tɨ</ta>
            <ta e="T32" id="Seg_1053" s="T31">əm-iː-tɨ</ta>
            <ta e="T33" id="Seg_1054" s="T32">čʼopa-iː-tɨ</ta>
            <ta e="T34" id="Seg_1055" s="T33">ilʼčʼa-iː-tɨ</ta>
            <ta e="T35" id="Seg_1056" s="T34">imɨlʼa-iː-tɨ</ta>
            <ta e="T36" id="Seg_1057" s="T35">i</ta>
            <ta e="T37" id="Seg_1058" s="T36">kətsa-iː-tɨ</ta>
            <ta e="T38" id="Seg_1059" s="T37">kočʼinʼe-iː-tɨ</ta>
            <ta e="T39" id="Seg_1060" s="T38">pisɨ-nʼ-nʼɔː-tɨn</ta>
            <ta e="T40" id="Seg_1061" s="T39">ɔːm-iː-tɨ</ta>
            <ta e="T41" id="Seg_1062" s="T40">čʼuːrɔː-tɨn</ta>
            <ta e="T42" id="Seg_1063" s="T41">meː</ta>
            <ta e="T43" id="Seg_1064" s="T42">qo-ntɨr-sɨ-mɨt</ta>
            <ta e="T44" id="Seg_1065" s="T43">sompɨla</ta>
            <ta e="T45" id="Seg_1066" s="T44">müttɨ-lʼ</ta>
            <ta e="T46" id="Seg_1067" s="T45">qum-ɨ-p</ta>
            <ta e="T47" id="Seg_1068" s="T46">täp-ɨ-n</ta>
            <ta e="T48" id="Seg_1069" s="T47">muli-mpɨ-sɔː-tɨn</ta>
            <ta e="T49" id="Seg_1070" s="T48">piss-ɛː-la</ta>
            <ta e="T50" id="Seg_1071" s="T49">pɔːpɨ</ta>
            <ta e="T51" id="Seg_1072" s="T50">ukkɨr</ta>
            <ta e="T52" id="Seg_1073" s="T51">mütɨ-lʼ</ta>
            <ta e="T53" id="Seg_1074" s="T52">qup</ta>
            <ta e="T54" id="Seg_1075" s="T53">qo-lʼčʼi-sa-tɨ</ta>
            <ta e="T55" id="Seg_1076" s="T54">ukkɨr</ta>
            <ta e="T56" id="Seg_1077" s="T55">mütɨ-lʼ</ta>
            <ta e="T57" id="Seg_1078" s="T56">näta-p</ta>
            <ta e="T58" id="Seg_1079" s="T57">nätak</ta>
            <ta e="T59" id="Seg_1080" s="T58">tok-alti-mpɨ-sɨ-tɨ</ta>
            <ta e="T60" id="Seg_1081" s="T59">mütɨ-lʼ</ta>
            <ta e="T61" id="Seg_1082" s="T60">kostʼum-ɨ-p</ta>
            <ta e="T62" id="Seg_1083" s="T61">täm</ta>
            <ta e="T63" id="Seg_1084" s="T62">ɛː-sa</ta>
            <ta e="T64" id="Seg_1085" s="T63">or-sa</ta>
            <ta e="T65" id="Seg_1086" s="T64">soma</ta>
            <ta e="T66" id="Seg_1087" s="T65">täp</ta>
            <ta e="T67" id="Seg_1088" s="T66">qäli-mpɨ-sa</ta>
            <ta e="T68" id="Seg_1089" s="T67">moqɨna</ta>
            <ta e="T69" id="Seg_1090" s="T68">mɔːt-qɨn-tɨ</ta>
            <ta e="T70" id="Seg_1091" s="T69">čʼap</ta>
            <ta e="T71" id="Seg_1092" s="T70">tü-sa</ta>
            <ta e="T72" id="Seg_1093" s="T71">mɔːt-qɨn-tɨ</ta>
            <ta e="T73" id="Seg_1094" s="T72">qo-sɨ-tɨ</ta>
            <ta e="T74" id="Seg_1095" s="T73">ıllə</ta>
            <ta e="T75" id="Seg_1096" s="T74">ɔːlʼčʼi-mpa</ta>
            <ta e="T76" id="Seg_1097" s="T75">əsɨ-tɨ</ta>
            <ta e="T77" id="Seg_1098" s="T76">aj</ta>
            <ta e="T78" id="Seg_1099" s="T77">əmɨ-tɨ</ta>
            <ta e="T79" id="Seg_1100" s="T78">qu-mpɔː-tɨn</ta>
            <ta e="T80" id="Seg_1101" s="T79">mütoː-qɨn</ta>
            <ta e="T81" id="Seg_1102" s="T80">täp</ta>
            <ta e="T82" id="Seg_1103" s="T81">qäs-sa</ta>
            <ta e="T83" id="Seg_1104" s="T82">podruga-n-tɨ</ta>
            <ta e="T84" id="Seg_1105" s="T83">mɔːt-tə</ta>
            <ta e="T85" id="Seg_1106" s="T84">ontɨ</ta>
            <ta e="T86" id="Seg_1107" s="T85">jap</ta>
            <ta e="T87" id="Seg_1108" s="T86">qo-la</ta>
            <ta e="T88" id="Seg_1109" s="T87">ɔːnt-al-sɔː-tɨn</ta>
            <ta e="T89" id="Seg_1110" s="T88">nɨːnɨ</ta>
            <ta e="T90" id="Seg_1111" s="T89">täp</ta>
            <ta e="T91" id="Seg_1112" s="T90">qəs-sa</ta>
            <ta e="T92" id="Seg_1113" s="T91">uːčʼi-la</ta>
            <ta e="T93" id="Seg_1114" s="T92">savhos-tɨ</ta>
            <ta e="T94" id="Seg_1115" s="T93">kut</ta>
            <ta e="T95" id="Seg_1116" s="T94">taq-pa</ta>
            <ta e="T96" id="Seg_1117" s="T95">ira-ttɨ</ta>
            <ta e="T97" id="Seg_1118" s="T96">täp-ɨ-n</ta>
            <ta e="T98" id="Seg_1119" s="T97">ira-t</ta>
            <ta e="T99" id="Seg_1120" s="T98">qu-sa</ta>
            <ta e="T100" id="Seg_1121" s="T99">mütaː-qɨn</ta>
            <ta e="T101" id="Seg_1122" s="T100">täp-ɨ-nɨk</ta>
            <ta e="T102" id="Seg_1123" s="T101">uːčʼi-qo</ta>
            <ta e="T103" id="Seg_1124" s="T102">savhos-qɨn</ta>
            <ta e="T104" id="Seg_1125" s="T103">sätɨm</ta>
            <ta e="T105" id="Seg_1126" s="T104">ɛːj-sa</ta>
            <ta e="T106" id="Seg_1127" s="T105">täp-ɨ-n</ta>
            <ta e="T107" id="Seg_1128" s="T106">nʼi</ta>
            <ta e="T108" id="Seg_1129" s="T107">kutɨ</ta>
            <ta e="T109" id="Seg_1130" s="T108">aša</ta>
            <ta e="T110" id="Seg_1131" s="T109">tɛnɨmɨ-sɨ-tɨ</ta>
            <ta e="T111" id="Seg_1132" s="T110">i</ta>
            <ta e="T112" id="Seg_1133" s="T111">täp</ta>
            <ta e="T113" id="Seg_1134" s="T112">nʼi</ta>
            <ta e="T114" id="Seg_1135" s="T113">kutɨ-p</ta>
            <ta e="T115" id="Seg_1136" s="T114">aša</ta>
            <ta e="T116" id="Seg_1137" s="T115">tɛnɨmɨ-sɨ-tɨ</ta>
            <ta e="T117" id="Seg_1138" s="T116">täp-ɨ-p</ta>
            <ta e="T118" id="Seg_1139" s="T117">peː-lʼa</ta>
            <ta e="T119" id="Seg_1140" s="T118">tü-sa</ta>
            <ta e="T120" id="Seg_1141" s="T119">ukkɨr</ta>
            <ta e="T121" id="Seg_1142" s="T120">mütɨ-lʼ</ta>
            <ta e="T122" id="Seg_1143" s="T121">qup</ta>
            <ta e="T123" id="Seg_1144" s="T122">tap</ta>
            <ta e="T124" id="Seg_1145" s="T123">čʼeːlʼɨ</ta>
            <ta e="T125" id="Seg_1146" s="T124">qai-lʼ</ta>
            <ta e="T126" id="Seg_1147" s="T125">kino</ta>
            <ta e="T127" id="Seg_1148" s="T126">mat</ta>
            <ta e="T128" id="Seg_1149" s="T127">aša</ta>
            <ta e="T129" id="Seg_1150" s="T128">tɛnɨma-k</ta>
            <ta e="T130" id="Seg_1151" s="T129">mat</ta>
            <ta e="T131" id="Seg_1152" s="T130">tap</ta>
            <ta e="T132" id="Seg_1153" s="T131">čʼeːlɨ</ta>
            <ta e="T133" id="Seg_1154" s="T132">qən-ta-k</ta>
            <ta e="T134" id="Seg_1155" s="T133">kino-ntɨ</ta>
            <ta e="T135" id="Seg_1156" s="T134">seːlʼčʼi</ta>
            <ta e="T136" id="Seg_1157" s="T135">čʼas-a-qɨ-lʼ</ta>
            <ta e="T137" id="Seg_1158" s="T136">kino-ntɨ</ta>
            <ta e="T138" id="Seg_1159" s="T137">meː</ta>
            <ta e="T139" id="Seg_1160" s="T138">klup-mɨn</ta>
            <ta e="T140" id="Seg_1161" s="T139">or-sa</ta>
            <ta e="T141" id="Seg_1162" s="T140">som</ta>
            <ta e="T142" id="Seg_1163" s="T141">ɛː-ŋa</ta>
            <ta e="T143" id="Seg_1164" s="T142">som</ta>
            <ta e="T144" id="Seg_1165" s="T143">ɛː-ŋa</ta>
            <ta e="T145" id="Seg_1166" s="T144">naqot</ta>
            <ta e="T146" id="Seg_1167" s="T145">talʼlʼi-mɔːt-pɔː-tɨm</ta>
            <ta e="T147" id="Seg_1168" s="T146">kočʼe-k</ta>
            <ta e="T148" id="Seg_1169" s="T147">tuparetka-t-a-n</ta>
            <ta e="T149" id="Seg_1170" s="T148">Vičʼa</ta>
            <ta e="T150" id="Seg_1171" s="T149">qon-ɨ-ša</ta>
            <ta e="T151" id="Seg_1172" s="T150">lapko-ntɨ</ta>
            <ta e="T152" id="Seg_1173" s="T151">i</ta>
            <ta e="T153" id="Seg_1174" s="T152">taːttɨ-sɨ-tɨ</ta>
            <ta e="T154" id="Seg_1175" s="T153">nʼäj-e-p</ta>
            <ta e="T155" id="Seg_1176" s="T154">i</ta>
            <ta e="T156" id="Seg_1177" s="T155">čʼɔːpɨ</ta>
            <ta e="T157" id="Seg_1178" s="T156">meː</ta>
            <ta e="T158" id="Seg_1179" s="T157">tiː</ta>
            <ta e="T159" id="Seg_1180" s="T158">am-ɨ-r-tɛntɔː-mɨn</ta>
            <ta e="T160" id="Seg_1181" s="T159">i</ta>
            <ta e="T161" id="Seg_1182" s="T160">näkä-nt-ɛntɔː-mɨn</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1183" s="T0">man</ta>
            <ta e="T2" id="Seg_1184" s="T1">qən-ɨ-sɨ-k</ta>
            <ta e="T3" id="Seg_1185" s="T2">täːlɨ</ta>
            <ta e="T4" id="Seg_1186" s="T3">čʼeːlɨ</ta>
            <ta e="T5" id="Seg_1187" s="T4">Vičʼa-sä</ta>
            <ta e="T6" id="Seg_1188" s="T5">kino-ntɨ</ta>
            <ta e="T7" id="Seg_1189" s="T6">man</ta>
            <ta e="T8" id="Seg_1190" s="T7">täːlɨ</ta>
            <ta e="T9" id="Seg_1191" s="T8">čʼeːlɨ</ta>
            <ta e="T10" id="Seg_1192" s="T9">qo-ntɨr-sɨ-k</ta>
            <ta e="T11" id="Seg_1193" s="T10">kino-m</ta>
            <ta e="T12" id="Seg_1194" s="T11">soldatka</ta>
            <ta e="T13" id="Seg_1195" s="T12">na</ta>
            <ta e="T14" id="Seg_1196" s="T13">ɛː-sɨ</ta>
            <ta e="T15" id="Seg_1197" s="T14">mütɨ-n</ta>
            <ta e="T16" id="Seg_1198" s="T15">moqɨ-qɨn</ta>
            <ta e="T17" id="Seg_1199" s="T16">muntɨk</ta>
            <ta e="T18" id="Seg_1200" s="T17">soltat-ɨ-t</ta>
            <ta e="T19" id="Seg_1201" s="T18">tü-qɨlɨ-sɨ-tɨt</ta>
            <ta e="T20" id="Seg_1202" s="T19">moqɨnä</ta>
            <ta e="T21" id="Seg_1203" s="T20">stancija-ntɨ</ta>
            <ta e="T22" id="Seg_1204" s="T21">tü-sɨ</ta>
            <ta e="T23" id="Seg_1205" s="T22">poest</ta>
            <ta e="T24" id="Seg_1206" s="T23">mütɨ-lʼ</ta>
            <ta e="T25" id="Seg_1207" s="T24">qum-ɨ-t-ɨ-m</ta>
            <ta e="T26" id="Seg_1208" s="T25">čʼəːtɨ-sɨ-tɨt</ta>
            <ta e="T27" id="Seg_1209" s="T26">iːja-iː-tɨ</ta>
            <ta e="T28" id="Seg_1210" s="T27">nälʼa-iː-tɨ</ta>
            <ta e="T29" id="Seg_1211" s="T28">ima-iː-tɨ</ta>
            <ta e="T30" id="Seg_1212" s="T29">əsɨ-iː-tɨ</ta>
            <ta e="T31" id="Seg_1213" s="T30">nʼenʼnʼa-iː-tɨ</ta>
            <ta e="T32" id="Seg_1214" s="T31">ama-iː-tɨ</ta>
            <ta e="T33" id="Seg_1215" s="T32">čʼopa-iː-tɨ</ta>
            <ta e="T34" id="Seg_1216" s="T33">ilʼčʼa-iː-tɨ</ta>
            <ta e="T35" id="Seg_1217" s="T34">imɨlʼa-iː-tɨ</ta>
            <ta e="T36" id="Seg_1218" s="T35">i</ta>
            <ta e="T37" id="Seg_1219" s="T36">kətsan-iː-tɨ</ta>
            <ta e="T38" id="Seg_1220" s="T37">kočʼčʼɨ-iː-tɨ</ta>
            <ta e="T39" id="Seg_1221" s="T38">pisɨ-š-ŋɨ-tɨt</ta>
            <ta e="T40" id="Seg_1222" s="T39">ɔːmɨ-iː-tɨ</ta>
            <ta e="T41" id="Seg_1223" s="T40">čʼuːrɨ-tɨt</ta>
            <ta e="T42" id="Seg_1224" s="T41">meː</ta>
            <ta e="T43" id="Seg_1225" s="T42">qo-ntɨr-sɨ-mɨt</ta>
            <ta e="T44" id="Seg_1226" s="T43">sompɨla</ta>
            <ta e="T45" id="Seg_1227" s="T44">mütɨ-lʼ</ta>
            <ta e="T46" id="Seg_1228" s="T45">qum-ɨ-m</ta>
            <ta e="T47" id="Seg_1229" s="T46">təp-ɨ-t</ta>
            <ta e="T48" id="Seg_1230" s="T47">mulɨ-mpɨ-sɨ-tɨt</ta>
            <ta e="T49" id="Seg_1231" s="T48">pisɨ-ɛː-lä</ta>
            <ta e="T50" id="Seg_1232" s="T49">pɔːpɨ</ta>
            <ta e="T51" id="Seg_1233" s="T50">ukkɨr</ta>
            <ta e="T52" id="Seg_1234" s="T51">mütɨ-lʼ</ta>
            <ta e="T53" id="Seg_1235" s="T52">qum</ta>
            <ta e="T54" id="Seg_1236" s="T53">qo-lʼčʼɨ-sɨ-tɨ</ta>
            <ta e="T55" id="Seg_1237" s="T54">ukkɨr</ta>
            <ta e="T56" id="Seg_1238" s="T55">mütɨ-lʼ</ta>
            <ta e="T57" id="Seg_1239" s="T56">nätäk-m</ta>
            <ta e="T58" id="Seg_1240" s="T57">nätäk</ta>
            <ta e="T59" id="Seg_1241" s="T58">tokk-altɨ-mpɨ-sɨ-tɨ</ta>
            <ta e="T60" id="Seg_1242" s="T59">mütɨ-lʼ</ta>
            <ta e="T61" id="Seg_1243" s="T60">kostʼum-ɨ-m</ta>
            <ta e="T62" id="Seg_1244" s="T61">təp</ta>
            <ta e="T63" id="Seg_1245" s="T62">ɛː-sɨ</ta>
            <ta e="T64" id="Seg_1246" s="T63">orɨ-sä</ta>
            <ta e="T65" id="Seg_1247" s="T64">soma</ta>
            <ta e="T66" id="Seg_1248" s="T65">təp</ta>
            <ta e="T67" id="Seg_1249" s="T66">qälɨ-mpɨ-sɨ</ta>
            <ta e="T68" id="Seg_1250" s="T67">moqɨnä</ta>
            <ta e="T69" id="Seg_1251" s="T68">mɔːt-qɨn-tɨ</ta>
            <ta e="T70" id="Seg_1252" s="T69">čʼam</ta>
            <ta e="T71" id="Seg_1253" s="T70">tü-sɨ</ta>
            <ta e="T72" id="Seg_1254" s="T71">mɔːt-qɨn-tɨ</ta>
            <ta e="T73" id="Seg_1255" s="T72">qo-sɨ-tɨ</ta>
            <ta e="T74" id="Seg_1256" s="T73">ıllä</ta>
            <ta e="T75" id="Seg_1257" s="T74">ɔːlʼčʼɨ-mpɨ</ta>
            <ta e="T76" id="Seg_1258" s="T75">əsɨ-tɨ</ta>
            <ta e="T77" id="Seg_1259" s="T76">aj</ta>
            <ta e="T78" id="Seg_1260" s="T77">ama-tɨ</ta>
            <ta e="T79" id="Seg_1261" s="T78">qu-mpɨ-tɨt</ta>
            <ta e="T80" id="Seg_1262" s="T79">mütɨ-qɨn</ta>
            <ta e="T81" id="Seg_1263" s="T80">təp</ta>
            <ta e="T82" id="Seg_1264" s="T81">qən-sɨ</ta>
            <ta e="T83" id="Seg_1265" s="T82">podruga-n-tɨ</ta>
            <ta e="T84" id="Seg_1266" s="T83">mɔːt-ntɨ</ta>
            <ta e="T85" id="Seg_1267" s="T84">ontɨ</ta>
            <ta e="T86" id="Seg_1268" s="T85">jam</ta>
            <ta e="T87" id="Seg_1269" s="T86">qo-lä</ta>
            <ta e="T88" id="Seg_1270" s="T87">ɔːntɨ-al-sɨ-tɨt</ta>
            <ta e="T89" id="Seg_1271" s="T88">nɨːnɨ</ta>
            <ta e="T90" id="Seg_1272" s="T89">təp</ta>
            <ta e="T91" id="Seg_1273" s="T90">qən-sɨ</ta>
            <ta e="T92" id="Seg_1274" s="T91">uːčʼɨ-lä</ta>
            <ta e="T93" id="Seg_1275" s="T92">sovhoz-ntɨ</ta>
            <ta e="T94" id="Seg_1276" s="T93">kun</ta>
            <ta e="T95" id="Seg_1277" s="T94">taqɨ-mpɨ</ta>
            <ta e="T96" id="Seg_1278" s="T95">ira-tɨ</ta>
            <ta e="T97" id="Seg_1279" s="T96">təp-ɨ-n</ta>
            <ta e="T98" id="Seg_1280" s="T97">ira-tɨ</ta>
            <ta e="T99" id="Seg_1281" s="T98">qu-sɨ</ta>
            <ta e="T100" id="Seg_1282" s="T99">mütɨ-qɨn</ta>
            <ta e="T101" id="Seg_1283" s="T100">təp-ɨ-nɨŋ</ta>
            <ta e="T102" id="Seg_1284" s="T101">uːčʼɨ-qo</ta>
            <ta e="T103" id="Seg_1285" s="T102">sovhoz-qɨn</ta>
            <ta e="T104" id="Seg_1286" s="T103">səttɨm</ta>
            <ta e="T105" id="Seg_1287" s="T104">ɛː-sɨ</ta>
            <ta e="T106" id="Seg_1288" s="T105">təp-ɨ-n</ta>
            <ta e="T107" id="Seg_1289" s="T106">nʼi</ta>
            <ta e="T108" id="Seg_1290" s="T107">kutɨ</ta>
            <ta e="T109" id="Seg_1291" s="T108">ašša</ta>
            <ta e="T110" id="Seg_1292" s="T109">tɛnɨmɨ-sɨ-tɨ</ta>
            <ta e="T111" id="Seg_1293" s="T110">i</ta>
            <ta e="T112" id="Seg_1294" s="T111">təp</ta>
            <ta e="T113" id="Seg_1295" s="T112">nʼi</ta>
            <ta e="T114" id="Seg_1296" s="T113">kutɨ-m</ta>
            <ta e="T115" id="Seg_1297" s="T114">ašša</ta>
            <ta e="T116" id="Seg_1298" s="T115">tɛnɨmɨ-sɨ-tɨ</ta>
            <ta e="T117" id="Seg_1299" s="T116">təp-ɨ-m</ta>
            <ta e="T118" id="Seg_1300" s="T117">peː-lä</ta>
            <ta e="T119" id="Seg_1301" s="T118">tü-sɨ</ta>
            <ta e="T120" id="Seg_1302" s="T119">ukkɨr</ta>
            <ta e="T121" id="Seg_1303" s="T120">mütɨ-lʼ</ta>
            <ta e="T122" id="Seg_1304" s="T121">qum</ta>
            <ta e="T123" id="Seg_1305" s="T122">tam</ta>
            <ta e="T124" id="Seg_1306" s="T123">čʼeːlɨ</ta>
            <ta e="T125" id="Seg_1307" s="T124">qaj-lʼ</ta>
            <ta e="T126" id="Seg_1308" s="T125">kino</ta>
            <ta e="T127" id="Seg_1309" s="T126">man</ta>
            <ta e="T128" id="Seg_1310" s="T127">ašša</ta>
            <ta e="T129" id="Seg_1311" s="T128">tɛnɨmɨ-k</ta>
            <ta e="T130" id="Seg_1312" s="T129">man</ta>
            <ta e="T131" id="Seg_1313" s="T130">tam</ta>
            <ta e="T132" id="Seg_1314" s="T131">čʼeːlɨ</ta>
            <ta e="T133" id="Seg_1315" s="T132">qən-ɛntɨ-k</ta>
            <ta e="T134" id="Seg_1316" s="T133">kino-ntɨ</ta>
            <ta e="T135" id="Seg_1317" s="T134">seːlʼčʼɨ</ta>
            <ta e="T136" id="Seg_1318" s="T135">čʼas-ɨ-qɨn-lʼ</ta>
            <ta e="T137" id="Seg_1319" s="T136">kino-ntɨ</ta>
            <ta e="T138" id="Seg_1320" s="T137">meː</ta>
            <ta e="T139" id="Seg_1321" s="T138">klup-mɨt</ta>
            <ta e="T140" id="Seg_1322" s="T139">orɨ-sä</ta>
            <ta e="T141" id="Seg_1323" s="T140">soma</ta>
            <ta e="T142" id="Seg_1324" s="T141">ɛː-ŋɨ</ta>
            <ta e="T143" id="Seg_1325" s="T142">soma</ta>
            <ta e="T144" id="Seg_1326" s="T143">ɛː-ŋɨ</ta>
            <ta e="T145" id="Seg_1327" s="T144">natqo</ta>
            <ta e="T146" id="Seg_1328" s="T145">talʼ-mɔːt-mpɨ-tɨt</ta>
            <ta e="T147" id="Seg_1329" s="T146">kočʼčʼɨ-k</ta>
            <ta e="T148" id="Seg_1330" s="T147">tuparetka-t-ɨ-n</ta>
            <ta e="T149" id="Seg_1331" s="T148">Vičʼa</ta>
            <ta e="T150" id="Seg_1332" s="T149">qən-ɨ-sɨ</ta>
            <ta e="T151" id="Seg_1333" s="T150">lapka-ntɨ</ta>
            <ta e="T152" id="Seg_1334" s="T151">i</ta>
            <ta e="T153" id="Seg_1335" s="T152">taːtɨ-sɨ-tɨ</ta>
            <ta e="T154" id="Seg_1336" s="T153">nʼanʼ-ɨ-m</ta>
            <ta e="T155" id="Seg_1337" s="T154">i</ta>
            <ta e="T156" id="Seg_1338" s="T155">čʼɔːpɨ</ta>
            <ta e="T157" id="Seg_1339" s="T156">meː</ta>
            <ta e="T158" id="Seg_1340" s="T157">tiː</ta>
            <ta e="T159" id="Seg_1341" s="T158">am-ɨ-r-ɛntɨ-mɨt</ta>
            <ta e="T160" id="Seg_1342" s="T159">i</ta>
            <ta e="T161" id="Seg_1343" s="T160">näkä-ntɨ-ɛntɨ-mɨt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1344" s="T0">I.NOM</ta>
            <ta e="T2" id="Seg_1345" s="T1">leave-EP-PST-1SG.S</ta>
            <ta e="T3" id="Seg_1346" s="T2">yesterday</ta>
            <ta e="T4" id="Seg_1347" s="T3">day.[NOM]</ta>
            <ta e="T5" id="Seg_1348" s="T4">Vitya-COM</ta>
            <ta e="T6" id="Seg_1349" s="T5">cinema-ILL</ta>
            <ta e="T7" id="Seg_1350" s="T6">I.NOM</ta>
            <ta e="T8" id="Seg_1351" s="T7">yesterday</ta>
            <ta e="T9" id="Seg_1352" s="T8">day.[NOM]</ta>
            <ta e="T10" id="Seg_1353" s="T9">sight-DRV-PST-1SG.S</ta>
            <ta e="T11" id="Seg_1354" s="T10">film-ACC</ta>
            <ta e="T12" id="Seg_1355" s="T11">soldier's.wife.[NOM]</ta>
            <ta e="T13" id="Seg_1356" s="T12">this.[NOM]</ta>
            <ta e="T14" id="Seg_1357" s="T13">be-PST.[3SG.S]</ta>
            <ta e="T15" id="Seg_1358" s="T14">war-GEN</ta>
            <ta e="T16" id="Seg_1359" s="T15">after-LOC</ta>
            <ta e="T17" id="Seg_1360" s="T16">all</ta>
            <ta e="T18" id="Seg_1361" s="T17">soldier-EP-PL.[NOM]</ta>
            <ta e="T19" id="Seg_1362" s="T18">come-MULS-PST-3PL</ta>
            <ta e="T20" id="Seg_1363" s="T19">home</ta>
            <ta e="T21" id="Seg_1364" s="T20">station-ILL</ta>
            <ta e="T22" id="Seg_1365" s="T21">come-PST.[3SG.S]</ta>
            <ta e="T23" id="Seg_1366" s="T22">train.[NOM]</ta>
            <ta e="T24" id="Seg_1367" s="T23">war-ADJZ</ta>
            <ta e="T25" id="Seg_1368" s="T24">human.being-EP-PL-EP-ACC</ta>
            <ta e="T26" id="Seg_1369" s="T25">meet-PST-3PL</ta>
            <ta e="T27" id="Seg_1370" s="T26">son-PL.[NOM]-3SG</ta>
            <ta e="T28" id="Seg_1371" s="T27">daughter-PL.[NOM]-3SG</ta>
            <ta e="T29" id="Seg_1372" s="T28">wife-PL.[NOM]-3SG</ta>
            <ta e="T30" id="Seg_1373" s="T29">father-PL.[NOM]-3SG</ta>
            <ta e="T31" id="Seg_1374" s="T30">sister-PL.[NOM]-3SG</ta>
            <ta e="T32" id="Seg_1375" s="T31">mother-PL.[NOM]-3SG</ta>
            <ta e="T33" id="Seg_1376" s="T32">brother-PL.[NOM]-3SG</ta>
            <ta e="T34" id="Seg_1377" s="T33">grandfather-PL.[NOM]-3SG</ta>
            <ta e="T35" id="Seg_1378" s="T34">grandmother-PL.[NOM]-3SG</ta>
            <ta e="T36" id="Seg_1379" s="T35">and</ta>
            <ta e="T37" id="Seg_1380" s="T36">grandson-PL.[NOM]-3SG</ta>
            <ta e="T38" id="Seg_1381" s="T37">much-PL.[NOM]-3SG</ta>
            <ta e="T39" id="Seg_1382" s="T38">laugh-US-CO-3PL</ta>
            <ta e="T40" id="Seg_1383" s="T39">other-PL.[NOM]-3SG</ta>
            <ta e="T41" id="Seg_1384" s="T40">cry-3PL</ta>
            <ta e="T42" id="Seg_1385" s="T41">we.PL.NOM</ta>
            <ta e="T43" id="Seg_1386" s="T42">sight-DRV-PST-1PL</ta>
            <ta e="T44" id="Seg_1387" s="T43">five</ta>
            <ta e="T45" id="Seg_1388" s="T44">war-ADJZ</ta>
            <ta e="T46" id="Seg_1389" s="T45">human.being-EP-ACC</ta>
            <ta e="T47" id="Seg_1390" s="T46">(s)he-EP-PL</ta>
            <ta e="T48" id="Seg_1391" s="T47">tell-HAB-PST-3PL</ta>
            <ta e="T49" id="Seg_1392" s="T48">laugh-PFV-CVB</ta>
            <ta e="T50" id="Seg_1393" s="T49">suddenly</ta>
            <ta e="T51" id="Seg_1394" s="T50">one</ta>
            <ta e="T52" id="Seg_1395" s="T51">war-ADJZ</ta>
            <ta e="T53" id="Seg_1396" s="T52">human.being.[NOM]</ta>
            <ta e="T54" id="Seg_1397" s="T53">sight-PFV-PST-3SG.O</ta>
            <ta e="T55" id="Seg_1398" s="T54">one</ta>
            <ta e="T56" id="Seg_1399" s="T55">war-ADJZ</ta>
            <ta e="T57" id="Seg_1400" s="T56">girl-ACC</ta>
            <ta e="T58" id="Seg_1401" s="T57">girl.[NOM]</ta>
            <ta e="T59" id="Seg_1402" s="T58">put.on-TR-HAB-PST-3SG.O</ta>
            <ta e="T60" id="Seg_1403" s="T59">war-ADJZ</ta>
            <ta e="T61" id="Seg_1404" s="T60">suit-EP-ACC</ta>
            <ta e="T62" id="Seg_1405" s="T61">(s)he</ta>
            <ta e="T63" id="Seg_1406" s="T62">be-PST.[3SG.S]</ta>
            <ta e="T64" id="Seg_1407" s="T63">force-INSTR</ta>
            <ta e="T65" id="Seg_1408" s="T64">beautiful</ta>
            <ta e="T66" id="Seg_1409" s="T65">(s)he.[NOM]</ta>
            <ta e="T67" id="Seg_1410" s="T66">go-HAB-PST.[3SG.S]</ta>
            <ta e="T68" id="Seg_1411" s="T67">home</ta>
            <ta e="T69" id="Seg_1412" s="T68">house-ILL-3SG</ta>
            <ta e="T70" id="Seg_1413" s="T69">hardly</ta>
            <ta e="T71" id="Seg_1414" s="T70">come-PST.[3SG.S]</ta>
            <ta e="T72" id="Seg_1415" s="T71">house-ILL-3SG</ta>
            <ta e="T73" id="Seg_1416" s="T72">sight-PST-3SG.O</ta>
            <ta e="T74" id="Seg_1417" s="T73">down</ta>
            <ta e="T75" id="Seg_1418" s="T74">fall.down-PST.NAR.[3SG.S]</ta>
            <ta e="T76" id="Seg_1419" s="T75">father.[NOM]-3SG</ta>
            <ta e="T77" id="Seg_1420" s="T76">and</ta>
            <ta e="T78" id="Seg_1421" s="T77">mother.[NOM]-3SG</ta>
            <ta e="T79" id="Seg_1422" s="T78">die-PST.NAR-3PL</ta>
            <ta e="T80" id="Seg_1423" s="T79">war-LOC</ta>
            <ta e="T81" id="Seg_1424" s="T80">(s)he.[NOM]</ta>
            <ta e="T82" id="Seg_1425" s="T81">leave-PST.[3SG.S]</ta>
            <ta e="T83" id="Seg_1426" s="T82">female.friend-GEN-3SG</ta>
            <ta e="T84" id="Seg_1427" s="T83">house-ILL</ta>
            <ta e="T85" id="Seg_1428" s="T84">himself.[NOM]</ta>
            <ta e="T86" id="Seg_1429" s="T85">RFL</ta>
            <ta e="T87" id="Seg_1430" s="T86">sight-CVB</ta>
            <ta e="T88" id="Seg_1431" s="T87">happiness-TR-PST-3PL</ta>
            <ta e="T89" id="Seg_1432" s="T88">then</ta>
            <ta e="T90" id="Seg_1433" s="T89">(s)he.[NOM]</ta>
            <ta e="T91" id="Seg_1434" s="T90">leave-PST.[3SG.S]</ta>
            <ta e="T92" id="Seg_1435" s="T91">work-CVB</ta>
            <ta e="T93" id="Seg_1436" s="T92">sovkhoz-ILL</ta>
            <ta e="T94" id="Seg_1437" s="T93">where</ta>
            <ta e="T95" id="Seg_1438" s="T94">close-PST.NAR.[3SG.S]</ta>
            <ta e="T96" id="Seg_1439" s="T95">husband.[NOM]-3SG</ta>
            <ta e="T97" id="Seg_1440" s="T96">(s)he-EP-GEN</ta>
            <ta e="T98" id="Seg_1441" s="T97">husband.[NOM]-3SG</ta>
            <ta e="T99" id="Seg_1442" s="T98">die-PST.[3SG.S]</ta>
            <ta e="T100" id="Seg_1443" s="T99">war-LOC</ta>
            <ta e="T101" id="Seg_1444" s="T100">(s)he-EP-ALL</ta>
            <ta e="T102" id="Seg_1445" s="T101">work-INF</ta>
            <ta e="T103" id="Seg_1446" s="T102">sovkhoz-LOC</ta>
            <ta e="T104" id="Seg_1447" s="T103">difficult</ta>
            <ta e="T105" id="Seg_1448" s="T104">be-PST.[3SG.S]</ta>
            <ta e="T106" id="Seg_1449" s="T105">(s)he-EP-GEN</ta>
            <ta e="T107" id="Seg_1450" s="T106">NEG</ta>
            <ta e="T108" id="Seg_1451" s="T107">who.[NOM]</ta>
            <ta e="T109" id="Seg_1452" s="T108">NEG</ta>
            <ta e="T110" id="Seg_1453" s="T109">know-PST-3SG.O</ta>
            <ta e="T111" id="Seg_1454" s="T110">and</ta>
            <ta e="T112" id="Seg_1455" s="T111">(s)he.[NOM]</ta>
            <ta e="T113" id="Seg_1456" s="T112">NEG</ta>
            <ta e="T114" id="Seg_1457" s="T113">who-ACC</ta>
            <ta e="T115" id="Seg_1458" s="T114">NEG</ta>
            <ta e="T116" id="Seg_1459" s="T115">know-PST-3SG.O</ta>
            <ta e="T117" id="Seg_1460" s="T116">(s)he-EP-ACC</ta>
            <ta e="T118" id="Seg_1461" s="T117">look.for-CVB</ta>
            <ta e="T119" id="Seg_1462" s="T118">come-PST.[3SG.S]</ta>
            <ta e="T120" id="Seg_1463" s="T119">one</ta>
            <ta e="T121" id="Seg_1464" s="T120">war-ADJZ</ta>
            <ta e="T122" id="Seg_1465" s="T121">human.being.[NOM]</ta>
            <ta e="T123" id="Seg_1466" s="T122">this</ta>
            <ta e="T124" id="Seg_1467" s="T123">day.[NOM]</ta>
            <ta e="T125" id="Seg_1468" s="T124">what-ADJZ</ta>
            <ta e="T126" id="Seg_1469" s="T125">film.[NOM]</ta>
            <ta e="T127" id="Seg_1470" s="T126">I.NOM</ta>
            <ta e="T128" id="Seg_1471" s="T127">NEG</ta>
            <ta e="T129" id="Seg_1472" s="T128">know-1SG.S</ta>
            <ta e="T130" id="Seg_1473" s="T129">I.NOM</ta>
            <ta e="T131" id="Seg_1474" s="T130">this</ta>
            <ta e="T132" id="Seg_1475" s="T131">day.[NOM]</ta>
            <ta e="T133" id="Seg_1476" s="T132">go.away-FUT-1SG.S</ta>
            <ta e="T134" id="Seg_1477" s="T133">cinema-ILL</ta>
            <ta e="T135" id="Seg_1478" s="T134">seven</ta>
            <ta e="T136" id="Seg_1479" s="T135">hour-EP-LOC-ADJZ</ta>
            <ta e="T137" id="Seg_1480" s="T136">film-ILL</ta>
            <ta e="T138" id="Seg_1481" s="T137">we.PL.GEN</ta>
            <ta e="T139" id="Seg_1482" s="T138">club.[NOM]-1PL</ta>
            <ta e="T140" id="Seg_1483" s="T139">force-INSTR</ta>
            <ta e="T141" id="Seg_1484" s="T140">good</ta>
            <ta e="T142" id="Seg_1485" s="T141">be-CO.[3SG.S]</ta>
            <ta e="T143" id="Seg_1486" s="T142">good</ta>
            <ta e="T144" id="Seg_1487" s="T143">be-CO.[3SG.S]</ta>
            <ta e="T145" id="Seg_1488" s="T144">that's.why</ta>
            <ta e="T146" id="Seg_1489" s="T145">break-DECAUS-HAB-3PL</ta>
            <ta e="T147" id="Seg_1490" s="T146">much-ADVZ</ta>
            <ta e="T148" id="Seg_1491" s="T147">stool-PL-EP-GEN</ta>
            <ta e="T149" id="Seg_1492" s="T148">Vitya.[NOM]</ta>
            <ta e="T150" id="Seg_1493" s="T149">leave-EP-PST.[3SG.S]</ta>
            <ta e="T151" id="Seg_1494" s="T150">shop-ILL</ta>
            <ta e="T152" id="Seg_1495" s="T151">and</ta>
            <ta e="T153" id="Seg_1496" s="T152">bring-PST-3SG.O</ta>
            <ta e="T154" id="Seg_1497" s="T153">bread-EP-ACC</ta>
            <ta e="T155" id="Seg_1498" s="T154">and</ta>
            <ta e="T156" id="Seg_1499" s="T155">tobacco.[NOM]</ta>
            <ta e="T157" id="Seg_1500" s="T156">we.PL.NOM</ta>
            <ta e="T158" id="Seg_1501" s="T157">now</ta>
            <ta e="T159" id="Seg_1502" s="T158">eat-EP-FRQ-FUT-1PL</ta>
            <ta e="T160" id="Seg_1503" s="T159">and</ta>
            <ta e="T161" id="Seg_1504" s="T160">smoke-IPFV-FUT-1PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1505" s="T0">я.NOM</ta>
            <ta e="T2" id="Seg_1506" s="T1">отправиться-EP-PST-1SG.S</ta>
            <ta e="T3" id="Seg_1507" s="T2">вчера</ta>
            <ta e="T4" id="Seg_1508" s="T3">день.[NOM]</ta>
            <ta e="T5" id="Seg_1509" s="T4">Витя-COM</ta>
            <ta e="T6" id="Seg_1510" s="T5">кино-ILL</ta>
            <ta e="T7" id="Seg_1511" s="T6">я.NOM</ta>
            <ta e="T8" id="Seg_1512" s="T7">вчера</ta>
            <ta e="T9" id="Seg_1513" s="T8">день.[NOM]</ta>
            <ta e="T10" id="Seg_1514" s="T9">видеть-DRV-PST-1SG.S</ta>
            <ta e="T11" id="Seg_1515" s="T10">кино-ACC</ta>
            <ta e="T12" id="Seg_1516" s="T11">солдатка.[NOM]</ta>
            <ta e="T13" id="Seg_1517" s="T12">это.[NOM]</ta>
            <ta e="T14" id="Seg_1518" s="T13">быть-PST.[3SG.S]</ta>
            <ta e="T15" id="Seg_1519" s="T14">война-GEN</ta>
            <ta e="T16" id="Seg_1520" s="T15">после-LOC</ta>
            <ta e="T17" id="Seg_1521" s="T16">всё</ta>
            <ta e="T18" id="Seg_1522" s="T17">солдат-EP-PL.[NOM]</ta>
            <ta e="T19" id="Seg_1523" s="T18">прийти-MULS-PST-3PL</ta>
            <ta e="T20" id="Seg_1524" s="T19">домой</ta>
            <ta e="T21" id="Seg_1525" s="T20">станция-ILL</ta>
            <ta e="T22" id="Seg_1526" s="T21">прийти-PST.[3SG.S]</ta>
            <ta e="T23" id="Seg_1527" s="T22">поезд.[NOM]</ta>
            <ta e="T24" id="Seg_1528" s="T23">война-ADJZ</ta>
            <ta e="T25" id="Seg_1529" s="T24">человек-EP-PL-EP-ACC</ta>
            <ta e="T26" id="Seg_1530" s="T25">встретить-PST-3PL</ta>
            <ta e="T27" id="Seg_1531" s="T26">сын-PL.[NOM]-3SG</ta>
            <ta e="T28" id="Seg_1532" s="T27">дочь-PL.[NOM]-3SG</ta>
            <ta e="T29" id="Seg_1533" s="T28">жена-PL.[NOM]-3SG</ta>
            <ta e="T30" id="Seg_1534" s="T29">отец-PL.[NOM]-3SG</ta>
            <ta e="T31" id="Seg_1535" s="T30">сестра-PL.[NOM]-3SG</ta>
            <ta e="T32" id="Seg_1536" s="T31">мать-PL.[NOM]-3SG</ta>
            <ta e="T33" id="Seg_1537" s="T32">брат-PL.[NOM]-3SG</ta>
            <ta e="T34" id="Seg_1538" s="T33">дедушка-PL.[NOM]-3SG</ta>
            <ta e="T35" id="Seg_1539" s="T34">бабушка-PL.[NOM]-3SG</ta>
            <ta e="T36" id="Seg_1540" s="T35">и</ta>
            <ta e="T37" id="Seg_1541" s="T36">внук-PL.[NOM]-3SG</ta>
            <ta e="T38" id="Seg_1542" s="T37">много-PL.[NOM]-3SG</ta>
            <ta e="T39" id="Seg_1543" s="T38">смеяться-US-CO-3PL</ta>
            <ta e="T40" id="Seg_1544" s="T39">остальной-PL.[NOM]-3SG</ta>
            <ta e="T41" id="Seg_1545" s="T40">плакать-3PL</ta>
            <ta e="T42" id="Seg_1546" s="T41">мы.PL.NOM</ta>
            <ta e="T43" id="Seg_1547" s="T42">видеть-DRV-PST-1PL</ta>
            <ta e="T44" id="Seg_1548" s="T43">пять</ta>
            <ta e="T45" id="Seg_1549" s="T44">война-ADJZ</ta>
            <ta e="T46" id="Seg_1550" s="T45">человек-EP-ACC</ta>
            <ta e="T47" id="Seg_1551" s="T46">он(а)-EP-PL</ta>
            <ta e="T48" id="Seg_1552" s="T47">сказать-HAB-PST-3PL</ta>
            <ta e="T49" id="Seg_1553" s="T48">смеяться-PFV-CVB</ta>
            <ta e="T50" id="Seg_1554" s="T49">вдруг</ta>
            <ta e="T51" id="Seg_1555" s="T50">один</ta>
            <ta e="T52" id="Seg_1556" s="T51">война-ADJZ</ta>
            <ta e="T53" id="Seg_1557" s="T52">человек.[NOM]</ta>
            <ta e="T54" id="Seg_1558" s="T53">видеть-PFV-PST-3SG.O</ta>
            <ta e="T55" id="Seg_1559" s="T54">один</ta>
            <ta e="T56" id="Seg_1560" s="T55">война-ADJZ</ta>
            <ta e="T57" id="Seg_1561" s="T56">девушка-ACC</ta>
            <ta e="T58" id="Seg_1562" s="T57">девушка.[NOM]</ta>
            <ta e="T59" id="Seg_1563" s="T58">надеть-TR-HAB-PST-3SG.O</ta>
            <ta e="T60" id="Seg_1564" s="T59">война-ADJZ</ta>
            <ta e="T61" id="Seg_1565" s="T60">костюм-EP-ACC</ta>
            <ta e="T62" id="Seg_1566" s="T61">он(а)</ta>
            <ta e="T63" id="Seg_1567" s="T62">быть-PST.[3SG.S]</ta>
            <ta e="T64" id="Seg_1568" s="T63">сила-INSTR</ta>
            <ta e="T65" id="Seg_1569" s="T64">красивый</ta>
            <ta e="T66" id="Seg_1570" s="T65">он(а).[NOM]</ta>
            <ta e="T67" id="Seg_1571" s="T66">ходить-HAB-PST.[3SG.S]</ta>
            <ta e="T68" id="Seg_1572" s="T67">домой</ta>
            <ta e="T69" id="Seg_1573" s="T68">дом-ILL-3SG</ta>
            <ta e="T70" id="Seg_1574" s="T69">едва</ta>
            <ta e="T71" id="Seg_1575" s="T70">прийти-PST.[3SG.S]</ta>
            <ta e="T72" id="Seg_1576" s="T71">дом-ILL-3SG</ta>
            <ta e="T73" id="Seg_1577" s="T72">видеть-PST-3SG.O</ta>
            <ta e="T74" id="Seg_1578" s="T73">вниз</ta>
            <ta e="T75" id="Seg_1579" s="T74">упасть-PST.NAR.[3SG.S]</ta>
            <ta e="T76" id="Seg_1580" s="T75">отец.[NOM]-3SG</ta>
            <ta e="T77" id="Seg_1581" s="T76">и</ta>
            <ta e="T78" id="Seg_1582" s="T77">мать.[NOM]-3SG</ta>
            <ta e="T79" id="Seg_1583" s="T78">умереть-PST.NAR-3PL</ta>
            <ta e="T80" id="Seg_1584" s="T79">война-LOC</ta>
            <ta e="T81" id="Seg_1585" s="T80">он(а).[NOM]</ta>
            <ta e="T82" id="Seg_1586" s="T81">отправиться-PST.[3SG.S]</ta>
            <ta e="T83" id="Seg_1587" s="T82">подруга-GEN-3SG</ta>
            <ta e="T84" id="Seg_1588" s="T83">дом-ILL</ta>
            <ta e="T85" id="Seg_1589" s="T84">он.сам.[NOM]</ta>
            <ta e="T86" id="Seg_1590" s="T85">RFL</ta>
            <ta e="T87" id="Seg_1591" s="T86">видеть-CVB</ta>
            <ta e="T88" id="Seg_1592" s="T87">радость-TR-PST-3PL</ta>
            <ta e="T89" id="Seg_1593" s="T88">потом</ta>
            <ta e="T90" id="Seg_1594" s="T89">он(а).[NOM]</ta>
            <ta e="T91" id="Seg_1595" s="T90">отправиться-PST.[3SG.S]</ta>
            <ta e="T92" id="Seg_1596" s="T91">работать-CVB</ta>
            <ta e="T93" id="Seg_1597" s="T92">совхоз-ILL</ta>
            <ta e="T94" id="Seg_1598" s="T93">где</ta>
            <ta e="T95" id="Seg_1599" s="T94">закрыть-PST.NAR.[3SG.S]</ta>
            <ta e="T96" id="Seg_1600" s="T95">муж.[NOM]-3SG</ta>
            <ta e="T97" id="Seg_1601" s="T96">он(а)-EP-GEN</ta>
            <ta e="T98" id="Seg_1602" s="T97">муж.[NOM]-3SG</ta>
            <ta e="T99" id="Seg_1603" s="T98">умереть-PST.[3SG.S]</ta>
            <ta e="T100" id="Seg_1604" s="T99">война-LOC</ta>
            <ta e="T101" id="Seg_1605" s="T100">он(а)-EP-ALL</ta>
            <ta e="T102" id="Seg_1606" s="T101">работать-INF</ta>
            <ta e="T103" id="Seg_1607" s="T102">совхоз-LOC</ta>
            <ta e="T104" id="Seg_1608" s="T103">трудный</ta>
            <ta e="T105" id="Seg_1609" s="T104">быть-PST.[3SG.S]</ta>
            <ta e="T106" id="Seg_1610" s="T105">он(а)-EP-GEN</ta>
            <ta e="T107" id="Seg_1611" s="T106">NEG</ta>
            <ta e="T108" id="Seg_1612" s="T107">кто.[NOM]</ta>
            <ta e="T109" id="Seg_1613" s="T108">NEG</ta>
            <ta e="T110" id="Seg_1614" s="T109">знать-PST-3SG.O</ta>
            <ta e="T111" id="Seg_1615" s="T110">и</ta>
            <ta e="T112" id="Seg_1616" s="T111">он(а).[NOM]</ta>
            <ta e="T113" id="Seg_1617" s="T112">NEG</ta>
            <ta e="T114" id="Seg_1618" s="T113">кто-ACC</ta>
            <ta e="T115" id="Seg_1619" s="T114">NEG</ta>
            <ta e="T116" id="Seg_1620" s="T115">знать-PST-3SG.O</ta>
            <ta e="T117" id="Seg_1621" s="T116">он(а)-EP-ACC</ta>
            <ta e="T118" id="Seg_1622" s="T117">искать-CVB</ta>
            <ta e="T119" id="Seg_1623" s="T118">прийти-PST.[3SG.S]</ta>
            <ta e="T120" id="Seg_1624" s="T119">один</ta>
            <ta e="T121" id="Seg_1625" s="T120">война-ADJZ</ta>
            <ta e="T122" id="Seg_1626" s="T121">человек.[NOM]</ta>
            <ta e="T123" id="Seg_1627" s="T122">этот</ta>
            <ta e="T124" id="Seg_1628" s="T123">день.[NOM]</ta>
            <ta e="T125" id="Seg_1629" s="T124">что-ADJZ</ta>
            <ta e="T126" id="Seg_1630" s="T125">кино.[NOM]</ta>
            <ta e="T127" id="Seg_1631" s="T126">я.NOM</ta>
            <ta e="T128" id="Seg_1632" s="T127">NEG</ta>
            <ta e="T129" id="Seg_1633" s="T128">знать-1SG.S</ta>
            <ta e="T130" id="Seg_1634" s="T129">я.NOM</ta>
            <ta e="T131" id="Seg_1635" s="T130">этот</ta>
            <ta e="T132" id="Seg_1636" s="T131">день.[NOM]</ta>
            <ta e="T133" id="Seg_1637" s="T132">уйти-FUT-1SG.S</ta>
            <ta e="T134" id="Seg_1638" s="T133">кино-ILL</ta>
            <ta e="T135" id="Seg_1639" s="T134">семь</ta>
            <ta e="T136" id="Seg_1640" s="T135">час-EP-LOC-ADJZ</ta>
            <ta e="T137" id="Seg_1641" s="T136">кино-ILL</ta>
            <ta e="T138" id="Seg_1642" s="T137">мы.PL.GEN</ta>
            <ta e="T139" id="Seg_1643" s="T138">клуб.[NOM]-1PL</ta>
            <ta e="T140" id="Seg_1644" s="T139">сила-INSTR</ta>
            <ta e="T141" id="Seg_1645" s="T140">хороший</ta>
            <ta e="T142" id="Seg_1646" s="T141">быть-CO.[3SG.S]</ta>
            <ta e="T143" id="Seg_1647" s="T142">хороший</ta>
            <ta e="T144" id="Seg_1648" s="T143">быть-CO.[3SG.S]</ta>
            <ta e="T145" id="Seg_1649" s="T144">поэтому</ta>
            <ta e="T146" id="Seg_1650" s="T145">сломать-DECAUS-HAB-3PL</ta>
            <ta e="T147" id="Seg_1651" s="T146">много-ADVZ</ta>
            <ta e="T148" id="Seg_1652" s="T147">табуретка-PL-EP-GEN</ta>
            <ta e="T149" id="Seg_1653" s="T148">Витя.[NOM]</ta>
            <ta e="T150" id="Seg_1654" s="T149">отправиться-EP-PST.[3SG.S]</ta>
            <ta e="T151" id="Seg_1655" s="T150">магазин-ILL</ta>
            <ta e="T152" id="Seg_1656" s="T151">и</ta>
            <ta e="T153" id="Seg_1657" s="T152">принести-PST-3SG.O</ta>
            <ta e="T154" id="Seg_1658" s="T153">хлеб-EP-ACC</ta>
            <ta e="T155" id="Seg_1659" s="T154">и</ta>
            <ta e="T156" id="Seg_1660" s="T155">табак.[NOM]</ta>
            <ta e="T157" id="Seg_1661" s="T156">мы.PL.NOM</ta>
            <ta e="T158" id="Seg_1662" s="T157">теперь</ta>
            <ta e="T159" id="Seg_1663" s="T158">съесть-EP-FRQ-FUT-1PL</ta>
            <ta e="T160" id="Seg_1664" s="T159">и</ta>
            <ta e="T161" id="Seg_1665" s="T160">курить-IPFV-FUT-1PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1666" s="T0">pers</ta>
            <ta e="T2" id="Seg_1667" s="T1">v-v:ins-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_1668" s="T2">adv</ta>
            <ta e="T4" id="Seg_1669" s="T3">n-n:case3</ta>
            <ta e="T5" id="Seg_1670" s="T4">nprop-n:case3</ta>
            <ta e="T6" id="Seg_1671" s="T5">n-n:case3</ta>
            <ta e="T7" id="Seg_1672" s="T6">pers</ta>
            <ta e="T8" id="Seg_1673" s="T7">adv</ta>
            <ta e="T9" id="Seg_1674" s="T8">n-n:case3</ta>
            <ta e="T10" id="Seg_1675" s="T9">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_1676" s="T10">n-n:case3</ta>
            <ta e="T12" id="Seg_1677" s="T11">n-n:case3</ta>
            <ta e="T13" id="Seg_1678" s="T12">pro-n:case3</ta>
            <ta e="T14" id="Seg_1679" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_1680" s="T14">n-n:case3</ta>
            <ta e="T16" id="Seg_1681" s="T15">pp-n:case3</ta>
            <ta e="T17" id="Seg_1682" s="T16">quant</ta>
            <ta e="T18" id="Seg_1683" s="T17">n-n:ins-n:num-n:case3</ta>
            <ta e="T19" id="Seg_1684" s="T18">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_1685" s="T19">adv</ta>
            <ta e="T21" id="Seg_1686" s="T20">n-n:case3</ta>
            <ta e="T22" id="Seg_1687" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_1688" s="T22">n-n:case3</ta>
            <ta e="T24" id="Seg_1689" s="T23">n-n&gt;adj</ta>
            <ta e="T25" id="Seg_1690" s="T24">n-n:(ins)-n:num-n:(ins)-n:case3</ta>
            <ta e="T26" id="Seg_1691" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_1692" s="T26">n-n:num-n:case1-n:poss</ta>
            <ta e="T28" id="Seg_1693" s="T27">n-n:num-n:case1-n:poss</ta>
            <ta e="T29" id="Seg_1694" s="T28">n-n:num-n:case1-n:poss</ta>
            <ta e="T30" id="Seg_1695" s="T29">n-n:num-n:case1-n:poss</ta>
            <ta e="T31" id="Seg_1696" s="T30">n-n:num-n:case1-n:poss</ta>
            <ta e="T32" id="Seg_1697" s="T31">n-n:num-n:case1-n:poss</ta>
            <ta e="T33" id="Seg_1698" s="T32">n-n:num-n:case1-n:poss</ta>
            <ta e="T34" id="Seg_1699" s="T33">n-n:num-n:case1-n:poss</ta>
            <ta e="T35" id="Seg_1700" s="T34">n-n:num-n:case1-n:poss</ta>
            <ta e="T36" id="Seg_1701" s="T35">conj</ta>
            <ta e="T37" id="Seg_1702" s="T36">n-n:num-n:case1-n:poss</ta>
            <ta e="T38" id="Seg_1703" s="T37">quant-n:num-n:case1-n:poss</ta>
            <ta e="T39" id="Seg_1704" s="T38">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T40" id="Seg_1705" s="T39">adj-n:num-n:case1-n:poss</ta>
            <ta e="T41" id="Seg_1706" s="T40">v-v:pn</ta>
            <ta e="T42" id="Seg_1707" s="T41">pers</ta>
            <ta e="T43" id="Seg_1708" s="T42">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_1709" s="T43">num</ta>
            <ta e="T45" id="Seg_1710" s="T44">n-n&gt;adj</ta>
            <ta e="T46" id="Seg_1711" s="T45">n-n:(ins)-n:case3</ta>
            <ta e="T47" id="Seg_1712" s="T46">pers-n:(ins)-n:num</ta>
            <ta e="T48" id="Seg_1713" s="T47">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_1714" s="T48">v-v&gt;v-v&gt;adv</ta>
            <ta e="T50" id="Seg_1715" s="T49">adv</ta>
            <ta e="T51" id="Seg_1716" s="T50">num</ta>
            <ta e="T52" id="Seg_1717" s="T51">n-n&gt;adj</ta>
            <ta e="T53" id="Seg_1718" s="T52">n-n:case3</ta>
            <ta e="T54" id="Seg_1719" s="T53">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_1720" s="T54">num</ta>
            <ta e="T56" id="Seg_1721" s="T55">n-n&gt;adj</ta>
            <ta e="T57" id="Seg_1722" s="T56">n-n:case3</ta>
            <ta e="T58" id="Seg_1723" s="T57">n-n:case3</ta>
            <ta e="T59" id="Seg_1724" s="T58">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_1725" s="T59">n-n&gt;adj</ta>
            <ta e="T61" id="Seg_1726" s="T60">n-n:(ins)-n:case3</ta>
            <ta e="T62" id="Seg_1727" s="T61">pers</ta>
            <ta e="T63" id="Seg_1728" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_1729" s="T63">n-n:case3</ta>
            <ta e="T65" id="Seg_1730" s="T64">adj</ta>
            <ta e="T66" id="Seg_1731" s="T65">pers-n:case3</ta>
            <ta e="T67" id="Seg_1732" s="T66">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_1733" s="T67">adv</ta>
            <ta e="T69" id="Seg_1734" s="T68">n-n:case2-n:poss</ta>
            <ta e="T70" id="Seg_1735" s="T69">conj</ta>
            <ta e="T71" id="Seg_1736" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_1737" s="T71">n-n:case2-n:poss</ta>
            <ta e="T73" id="Seg_1738" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_1739" s="T73">preverb</ta>
            <ta e="T75" id="Seg_1740" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_1741" s="T75">n-n:case1-n:poss</ta>
            <ta e="T77" id="Seg_1742" s="T76">conj</ta>
            <ta e="T78" id="Seg_1743" s="T77">n-n:case1-n:poss</ta>
            <ta e="T79" id="Seg_1744" s="T78">v-v:tense-v:pn</ta>
            <ta e="T80" id="Seg_1745" s="T79">n-n:case3</ta>
            <ta e="T81" id="Seg_1746" s="T80">pers-n:case3</ta>
            <ta e="T82" id="Seg_1747" s="T81">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_1748" s="T82">n-n:case1-n:poss</ta>
            <ta e="T84" id="Seg_1749" s="T83">n-n:case3</ta>
            <ta e="T85" id="Seg_1750" s="T84">pro-n:case3</ta>
            <ta e="T86" id="Seg_1751" s="T85">preverb</ta>
            <ta e="T87" id="Seg_1752" s="T86">v-v&gt;adv</ta>
            <ta e="T88" id="Seg_1753" s="T87">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_1754" s="T88">adv</ta>
            <ta e="T90" id="Seg_1755" s="T89">pers-n:case3</ta>
            <ta e="T91" id="Seg_1756" s="T90">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_1757" s="T91">v-v&gt;adv</ta>
            <ta e="T93" id="Seg_1758" s="T92">n-n:case3</ta>
            <ta e="T94" id="Seg_1759" s="T93">interrog</ta>
            <ta e="T95" id="Seg_1760" s="T94">v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_1761" s="T95">n-n:case1-n:poss</ta>
            <ta e="T97" id="Seg_1762" s="T96">pers-n:(ins)-n:case3</ta>
            <ta e="T98" id="Seg_1763" s="T97">n-n:case1-n:poss</ta>
            <ta e="T99" id="Seg_1764" s="T98">v-v:tense-v:pn</ta>
            <ta e="T100" id="Seg_1765" s="T99">n-n:case3</ta>
            <ta e="T101" id="Seg_1766" s="T100">pers-n:(ins)-n:case3</ta>
            <ta e="T102" id="Seg_1767" s="T101">v-v:inf</ta>
            <ta e="T103" id="Seg_1768" s="T102">n-n:case3</ta>
            <ta e="T104" id="Seg_1769" s="T103">adj</ta>
            <ta e="T105" id="Seg_1770" s="T104">v-v:tense-v:pn</ta>
            <ta e="T106" id="Seg_1771" s="T105">pers-n:ins-n:case3</ta>
            <ta e="T107" id="Seg_1772" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_1773" s="T107">interrog-n:case3</ta>
            <ta e="T109" id="Seg_1774" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_1775" s="T109">v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_1776" s="T110">conj</ta>
            <ta e="T112" id="Seg_1777" s="T111">pers-n:case3</ta>
            <ta e="T113" id="Seg_1778" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_1779" s="T113">interrog-n:case3</ta>
            <ta e="T115" id="Seg_1780" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_1781" s="T115">v-v:tense-v:pn</ta>
            <ta e="T117" id="Seg_1782" s="T116">pers-n:(ins)-n:case3</ta>
            <ta e="T118" id="Seg_1783" s="T117">v-v&gt;adv</ta>
            <ta e="T119" id="Seg_1784" s="T118">v-v:tense-v:pn</ta>
            <ta e="T120" id="Seg_1785" s="T119">num</ta>
            <ta e="T121" id="Seg_1786" s="T120">n-n&gt;adj</ta>
            <ta e="T122" id="Seg_1787" s="T121">n-n:case3</ta>
            <ta e="T123" id="Seg_1788" s="T122">dem</ta>
            <ta e="T124" id="Seg_1789" s="T123">n-n:case3</ta>
            <ta e="T125" id="Seg_1790" s="T124">interrog-n&gt;adj</ta>
            <ta e="T126" id="Seg_1791" s="T125">n-n:case3</ta>
            <ta e="T127" id="Seg_1792" s="T126">pers</ta>
            <ta e="T128" id="Seg_1793" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_1794" s="T128">v-v:pn</ta>
            <ta e="T130" id="Seg_1795" s="T129">pers</ta>
            <ta e="T131" id="Seg_1796" s="T130">dem</ta>
            <ta e="T132" id="Seg_1797" s="T131">n-n:case3</ta>
            <ta e="T133" id="Seg_1798" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_1799" s="T133">n-n:case3</ta>
            <ta e="T135" id="Seg_1800" s="T134">num</ta>
            <ta e="T136" id="Seg_1801" s="T135">n-n:(ins)-n:case2-n&gt;adj</ta>
            <ta e="T137" id="Seg_1802" s="T136">n-n:case3</ta>
            <ta e="T138" id="Seg_1803" s="T137">pers</ta>
            <ta e="T139" id="Seg_1804" s="T138">n-n:case1-n:poss</ta>
            <ta e="T140" id="Seg_1805" s="T139">n-n:case3</ta>
            <ta e="T141" id="Seg_1806" s="T140">adj</ta>
            <ta e="T142" id="Seg_1807" s="T141">v-v:ins-v:pn</ta>
            <ta e="T143" id="Seg_1808" s="T142">adj</ta>
            <ta e="T144" id="Seg_1809" s="T143">v-v:ins-v:pn</ta>
            <ta e="T145" id="Seg_1810" s="T144">conj</ta>
            <ta e="T146" id="Seg_1811" s="T145">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T147" id="Seg_1812" s="T146">quant-adj&gt;adv</ta>
            <ta e="T148" id="Seg_1813" s="T147">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T149" id="Seg_1814" s="T148">nprop-n:case3</ta>
            <ta e="T150" id="Seg_1815" s="T149">v-v:ins-v:tense-v:pn</ta>
            <ta e="T151" id="Seg_1816" s="T150">n-n:case3</ta>
            <ta e="T152" id="Seg_1817" s="T151">conj</ta>
            <ta e="T153" id="Seg_1818" s="T152">v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_1819" s="T153">n-n:(ins)-n:case3</ta>
            <ta e="T155" id="Seg_1820" s="T154">conj</ta>
            <ta e="T156" id="Seg_1821" s="T155">n-n:case3</ta>
            <ta e="T157" id="Seg_1822" s="T156">pers</ta>
            <ta e="T158" id="Seg_1823" s="T157">adv</ta>
            <ta e="T159" id="Seg_1824" s="T158">v-n:(ins)-v&gt;v-v:tense-v:pn</ta>
            <ta e="T160" id="Seg_1825" s="T159">conj</ta>
            <ta e="T161" id="Seg_1826" s="T160">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1827" s="T0">pers</ta>
            <ta e="T2" id="Seg_1828" s="T1">v</ta>
            <ta e="T3" id="Seg_1829" s="T2">adv</ta>
            <ta e="T4" id="Seg_1830" s="T3">n</ta>
            <ta e="T5" id="Seg_1831" s="T4">nprop</ta>
            <ta e="T6" id="Seg_1832" s="T5">n</ta>
            <ta e="T7" id="Seg_1833" s="T6">pers</ta>
            <ta e="T8" id="Seg_1834" s="T7">adv</ta>
            <ta e="T9" id="Seg_1835" s="T8">n</ta>
            <ta e="T10" id="Seg_1836" s="T9">v</ta>
            <ta e="T11" id="Seg_1837" s="T10">n</ta>
            <ta e="T12" id="Seg_1838" s="T11">n</ta>
            <ta e="T13" id="Seg_1839" s="T12">dem</ta>
            <ta e="T14" id="Seg_1840" s="T13">n</ta>
            <ta e="T15" id="Seg_1841" s="T14">n</ta>
            <ta e="T16" id="Seg_1842" s="T15">pp</ta>
            <ta e="T17" id="Seg_1843" s="T16">quant</ta>
            <ta e="T18" id="Seg_1844" s="T17">n</ta>
            <ta e="T19" id="Seg_1845" s="T18">v</ta>
            <ta e="T20" id="Seg_1846" s="T19">adv</ta>
            <ta e="T21" id="Seg_1847" s="T20">n</ta>
            <ta e="T22" id="Seg_1848" s="T21">v</ta>
            <ta e="T23" id="Seg_1849" s="T22">n</ta>
            <ta e="T24" id="Seg_1850" s="T23">adj</ta>
            <ta e="T25" id="Seg_1851" s="T24">n</ta>
            <ta e="T26" id="Seg_1852" s="T25">v</ta>
            <ta e="T27" id="Seg_1853" s="T26">n</ta>
            <ta e="T28" id="Seg_1854" s="T27">n</ta>
            <ta e="T29" id="Seg_1855" s="T28">n</ta>
            <ta e="T30" id="Seg_1856" s="T29">n</ta>
            <ta e="T31" id="Seg_1857" s="T30">n</ta>
            <ta e="T32" id="Seg_1858" s="T31">n</ta>
            <ta e="T33" id="Seg_1859" s="T32">n</ta>
            <ta e="T34" id="Seg_1860" s="T33">n</ta>
            <ta e="T35" id="Seg_1861" s="T34">n</ta>
            <ta e="T36" id="Seg_1862" s="T35">conj</ta>
            <ta e="T37" id="Seg_1863" s="T36">n</ta>
            <ta e="T38" id="Seg_1864" s="T37">quant</ta>
            <ta e="T39" id="Seg_1865" s="T38">v</ta>
            <ta e="T40" id="Seg_1866" s="T39">adj</ta>
            <ta e="T41" id="Seg_1867" s="T40">v</ta>
            <ta e="T42" id="Seg_1868" s="T41">pers</ta>
            <ta e="T43" id="Seg_1869" s="T42">v</ta>
            <ta e="T44" id="Seg_1870" s="T43">num</ta>
            <ta e="T45" id="Seg_1871" s="T44">adj</ta>
            <ta e="T46" id="Seg_1872" s="T45">n</ta>
            <ta e="T47" id="Seg_1873" s="T46">pers</ta>
            <ta e="T48" id="Seg_1874" s="T47">v</ta>
            <ta e="T49" id="Seg_1875" s="T48">adv</ta>
            <ta e="T50" id="Seg_1876" s="T49">adv</ta>
            <ta e="T51" id="Seg_1877" s="T50">num</ta>
            <ta e="T52" id="Seg_1878" s="T51">adj</ta>
            <ta e="T53" id="Seg_1879" s="T52">n</ta>
            <ta e="T54" id="Seg_1880" s="T53">v</ta>
            <ta e="T55" id="Seg_1881" s="T54">num</ta>
            <ta e="T56" id="Seg_1882" s="T55">adj</ta>
            <ta e="T57" id="Seg_1883" s="T56">n</ta>
            <ta e="T58" id="Seg_1884" s="T57">n</ta>
            <ta e="T59" id="Seg_1885" s="T58">v</ta>
            <ta e="T60" id="Seg_1886" s="T59">adj</ta>
            <ta e="T61" id="Seg_1887" s="T60">n</ta>
            <ta e="T62" id="Seg_1888" s="T61">pers</ta>
            <ta e="T63" id="Seg_1889" s="T62">n</ta>
            <ta e="T64" id="Seg_1890" s="T63">n</ta>
            <ta e="T65" id="Seg_1891" s="T64">adj</ta>
            <ta e="T66" id="Seg_1892" s="T65">pers</ta>
            <ta e="T67" id="Seg_1893" s="T66">v</ta>
            <ta e="T68" id="Seg_1894" s="T67">adv</ta>
            <ta e="T69" id="Seg_1895" s="T68">n</ta>
            <ta e="T70" id="Seg_1896" s="T69">conj</ta>
            <ta e="T71" id="Seg_1897" s="T70">v</ta>
            <ta e="T72" id="Seg_1898" s="T71">n</ta>
            <ta e="T73" id="Seg_1899" s="T72">v</ta>
            <ta e="T74" id="Seg_1900" s="T73">preverb</ta>
            <ta e="T75" id="Seg_1901" s="T74">v</ta>
            <ta e="T76" id="Seg_1902" s="T75">n</ta>
            <ta e="T77" id="Seg_1903" s="T76">conj</ta>
            <ta e="T78" id="Seg_1904" s="T77">n</ta>
            <ta e="T79" id="Seg_1905" s="T78">v</ta>
            <ta e="T80" id="Seg_1906" s="T79">n</ta>
            <ta e="T81" id="Seg_1907" s="T80">pers</ta>
            <ta e="T82" id="Seg_1908" s="T81">v</ta>
            <ta e="T83" id="Seg_1909" s="T82">n</ta>
            <ta e="T84" id="Seg_1910" s="T83">n</ta>
            <ta e="T85" id="Seg_1911" s="T84">pro</ta>
            <ta e="T86" id="Seg_1912" s="T85">preverb</ta>
            <ta e="T87" id="Seg_1913" s="T86">adv</ta>
            <ta e="T88" id="Seg_1914" s="T87">v</ta>
            <ta e="T89" id="Seg_1915" s="T88">adv</ta>
            <ta e="T90" id="Seg_1916" s="T89">pers</ta>
            <ta e="T91" id="Seg_1917" s="T90">v</ta>
            <ta e="T92" id="Seg_1918" s="T91">adv</ta>
            <ta e="T93" id="Seg_1919" s="T92">n</ta>
            <ta e="T94" id="Seg_1920" s="T93">interrog</ta>
            <ta e="T95" id="Seg_1921" s="T94">v</ta>
            <ta e="T96" id="Seg_1922" s="T95">n</ta>
            <ta e="T97" id="Seg_1923" s="T96">pers</ta>
            <ta e="T98" id="Seg_1924" s="T97">n</ta>
            <ta e="T99" id="Seg_1925" s="T98">v</ta>
            <ta e="T100" id="Seg_1926" s="T99">n</ta>
            <ta e="T101" id="Seg_1927" s="T100">pers</ta>
            <ta e="T102" id="Seg_1928" s="T101">v</ta>
            <ta e="T103" id="Seg_1929" s="T102">n</ta>
            <ta e="T104" id="Seg_1930" s="T103">adj</ta>
            <ta e="T105" id="Seg_1931" s="T104">v</ta>
            <ta e="T106" id="Seg_1932" s="T105">pers</ta>
            <ta e="T107" id="Seg_1933" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_1934" s="T107">interrog</ta>
            <ta e="T109" id="Seg_1935" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_1936" s="T109">v</ta>
            <ta e="T111" id="Seg_1937" s="T110">conj</ta>
            <ta e="T112" id="Seg_1938" s="T111">pers</ta>
            <ta e="T113" id="Seg_1939" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_1940" s="T113">interrog</ta>
            <ta e="T115" id="Seg_1941" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_1942" s="T115">v</ta>
            <ta e="T117" id="Seg_1943" s="T116">pers</ta>
            <ta e="T118" id="Seg_1944" s="T117">adv</ta>
            <ta e="T119" id="Seg_1945" s="T118">v</ta>
            <ta e="T120" id="Seg_1946" s="T119">num</ta>
            <ta e="T121" id="Seg_1947" s="T120">adj</ta>
            <ta e="T122" id="Seg_1948" s="T121">n</ta>
            <ta e="T123" id="Seg_1949" s="T122">dem</ta>
            <ta e="T124" id="Seg_1950" s="T123">n</ta>
            <ta e="T125" id="Seg_1951" s="T124">adj</ta>
            <ta e="T126" id="Seg_1952" s="T125">n</ta>
            <ta e="T127" id="Seg_1953" s="T126">pers</ta>
            <ta e="T128" id="Seg_1954" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_1955" s="T128">v</ta>
            <ta e="T130" id="Seg_1956" s="T129">pers</ta>
            <ta e="T131" id="Seg_1957" s="T130">dem</ta>
            <ta e="T132" id="Seg_1958" s="T131">n</ta>
            <ta e="T133" id="Seg_1959" s="T132">v</ta>
            <ta e="T134" id="Seg_1960" s="T133">n</ta>
            <ta e="T135" id="Seg_1961" s="T134">num</ta>
            <ta e="T136" id="Seg_1962" s="T135">adj</ta>
            <ta e="T137" id="Seg_1963" s="T136">n</ta>
            <ta e="T138" id="Seg_1964" s="T137">pers</ta>
            <ta e="T139" id="Seg_1965" s="T138">n</ta>
            <ta e="T140" id="Seg_1966" s="T139">n</ta>
            <ta e="T141" id="Seg_1967" s="T140">adj</ta>
            <ta e="T142" id="Seg_1968" s="T141">v</ta>
            <ta e="T143" id="Seg_1969" s="T142">adj</ta>
            <ta e="T144" id="Seg_1970" s="T143">n</ta>
            <ta e="T145" id="Seg_1971" s="T144">conj</ta>
            <ta e="T146" id="Seg_1972" s="T145">v</ta>
            <ta e="T147" id="Seg_1973" s="T146">adv</ta>
            <ta e="T148" id="Seg_1974" s="T147">n</ta>
            <ta e="T149" id="Seg_1975" s="T148">nprop</ta>
            <ta e="T150" id="Seg_1976" s="T149">v</ta>
            <ta e="T151" id="Seg_1977" s="T150">n</ta>
            <ta e="T152" id="Seg_1978" s="T151">conj</ta>
            <ta e="T153" id="Seg_1979" s="T152">v</ta>
            <ta e="T154" id="Seg_1980" s="T153">n</ta>
            <ta e="T155" id="Seg_1981" s="T154">conj</ta>
            <ta e="T156" id="Seg_1982" s="T155">n</ta>
            <ta e="T157" id="Seg_1983" s="T156">pers</ta>
            <ta e="T158" id="Seg_1984" s="T157">adv</ta>
            <ta e="T159" id="Seg_1985" s="T158">v</ta>
            <ta e="T160" id="Seg_1986" s="T159">conj</ta>
            <ta e="T161" id="Seg_1987" s="T160">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1988" s="T0">pro.h:A</ta>
            <ta e="T4" id="Seg_1989" s="T3">np:Time</ta>
            <ta e="T5" id="Seg_1990" s="T4">np:Com</ta>
            <ta e="T6" id="Seg_1991" s="T5">np:G</ta>
            <ta e="T7" id="Seg_1992" s="T6">pro.h:E</ta>
            <ta e="T9" id="Seg_1993" s="T8">np:Time</ta>
            <ta e="T11" id="Seg_1994" s="T10">np:Th</ta>
            <ta e="T13" id="Seg_1995" s="T12">pro:Th</ta>
            <ta e="T15" id="Seg_1996" s="T14">pp:Time</ta>
            <ta e="T18" id="Seg_1997" s="T17">np.h:A</ta>
            <ta e="T20" id="Seg_1998" s="T19">adv:G</ta>
            <ta e="T21" id="Seg_1999" s="T20">np:G</ta>
            <ta e="T23" id="Seg_2000" s="T22">np:A</ta>
            <ta e="T25" id="Seg_2001" s="T24">np.h:Th</ta>
            <ta e="T27" id="Seg_2002" s="T26">np.h:A</ta>
            <ta e="T28" id="Seg_2003" s="T27">np.h:A</ta>
            <ta e="T29" id="Seg_2004" s="T28">np.h:A</ta>
            <ta e="T30" id="Seg_2005" s="T29">np.h:A</ta>
            <ta e="T31" id="Seg_2006" s="T30">np.h:A</ta>
            <ta e="T32" id="Seg_2007" s="T31">np.h:A</ta>
            <ta e="T33" id="Seg_2008" s="T32">np.h:A</ta>
            <ta e="T34" id="Seg_2009" s="T33">np.h:A</ta>
            <ta e="T35" id="Seg_2010" s="T34">np.h:A</ta>
            <ta e="T37" id="Seg_2011" s="T36">np.h:A</ta>
            <ta e="T38" id="Seg_2012" s="T37">np.h:A</ta>
            <ta e="T40" id="Seg_2013" s="T39">np.h:A</ta>
            <ta e="T42" id="Seg_2014" s="T41">pro.h:E</ta>
            <ta e="T46" id="Seg_2015" s="T45">np.h:Th</ta>
            <ta e="T47" id="Seg_2016" s="T46">pro.h:A</ta>
            <ta e="T50" id="Seg_2017" s="T49">adv:Time</ta>
            <ta e="T53" id="Seg_2018" s="T52">pro.h:E</ta>
            <ta e="T57" id="Seg_2019" s="T56">np.h:Th</ta>
            <ta e="T58" id="Seg_2020" s="T57">pro.h:A</ta>
            <ta e="T61" id="Seg_2021" s="T60">np:Th</ta>
            <ta e="T62" id="Seg_2022" s="T61">pro.h:Th</ta>
            <ta e="T66" id="Seg_2023" s="T65">pro.h:A</ta>
            <ta e="T68" id="Seg_2024" s="T67">adv:Time</ta>
            <ta e="T69" id="Seg_2025" s="T68">np:Time</ta>
            <ta e="T73" id="Seg_2026" s="T72">0.1.h:E</ta>
            <ta e="T76" id="Seg_2027" s="T75">np.h:P</ta>
            <ta e="T78" id="Seg_2028" s="T77">np.h:P</ta>
            <ta e="T80" id="Seg_2029" s="T79">np:Time</ta>
            <ta e="T81" id="Seg_2030" s="T80">pro.h:A</ta>
            <ta e="T83" id="Seg_2031" s="T82">np.h:Poss np:G</ta>
            <ta e="T84" id="Seg_2032" s="T83">np:G</ta>
            <ta e="T88" id="Seg_2033" s="T87">0.3.h:E</ta>
            <ta e="T89" id="Seg_2034" s="T88">adv:Time</ta>
            <ta e="T90" id="Seg_2035" s="T89">pro.h:A</ta>
            <ta e="T93" id="Seg_2036" s="T92">np:G</ta>
            <ta e="T97" id="Seg_2037" s="T96">pro.h:Poss</ta>
            <ta e="T98" id="Seg_2038" s="T97">np.h:P</ta>
            <ta e="T100" id="Seg_2039" s="T99">np:Time</ta>
            <ta e="T101" id="Seg_2040" s="T100">np.h:E</ta>
            <ta e="T106" id="Seg_2041" s="T105">pro.h:Th</ta>
            <ta e="T108" id="Seg_2042" s="T107">pro.h:E</ta>
            <ta e="T112" id="Seg_2043" s="T111">pro.h:E</ta>
            <ta e="T114" id="Seg_2044" s="T113">pro.h:Th</ta>
            <ta e="T122" id="Seg_2045" s="T121">np.h:A</ta>
            <ta e="T124" id="Seg_2046" s="T123">np:Time</ta>
            <ta e="T126" id="Seg_2047" s="T125">np:Th</ta>
            <ta e="T127" id="Seg_2048" s="T126">pro.h:E</ta>
            <ta e="T130" id="Seg_2049" s="T129">pro.h:A</ta>
            <ta e="T132" id="Seg_2050" s="T131">np:Time</ta>
            <ta e="T134" id="Seg_2051" s="T133">np:G</ta>
            <ta e="T137" id="Seg_2052" s="T136">np:G</ta>
            <ta e="T138" id="Seg_2053" s="T137">pro.h:Poss</ta>
            <ta e="T139" id="Seg_2054" s="T138">np:Th</ta>
            <ta e="T144" id="Seg_2055" s="T143">0.3:Th</ta>
            <ta e="T148" id="Seg_2056" s="T147">np:P</ta>
            <ta e="T149" id="Seg_2057" s="T148">np.h:A</ta>
            <ta e="T151" id="Seg_2058" s="T150">np:G</ta>
            <ta e="T153" id="Seg_2059" s="T152">0.3.h:A</ta>
            <ta e="T154" id="Seg_2060" s="T153">np:Th</ta>
            <ta e="T156" id="Seg_2061" s="T155">np:Th</ta>
            <ta e="T157" id="Seg_2062" s="T156">pro.h:A</ta>
            <ta e="T161" id="Seg_2063" s="T160">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_2064" s="T0">pro.h:S</ta>
            <ta e="T2" id="Seg_2065" s="T1">v:pred</ta>
            <ta e="T7" id="Seg_2066" s="T6">pro.h:S</ta>
            <ta e="T10" id="Seg_2067" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_2068" s="T10">np:O</ta>
            <ta e="T13" id="Seg_2069" s="T12">pro:S</ta>
            <ta e="T14" id="Seg_2070" s="T13">v:pred</ta>
            <ta e="T18" id="Seg_2071" s="T17">np.h:S</ta>
            <ta e="T19" id="Seg_2072" s="T18">v:pred</ta>
            <ta e="T22" id="Seg_2073" s="T21">v:pred</ta>
            <ta e="T23" id="Seg_2074" s="T22">np:S</ta>
            <ta e="T25" id="Seg_2075" s="T24">np.h:O</ta>
            <ta e="T26" id="Seg_2076" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_2077" s="T26">np.h:S</ta>
            <ta e="T28" id="Seg_2078" s="T27">np.h:S</ta>
            <ta e="T29" id="Seg_2079" s="T28">np.h:S</ta>
            <ta e="T30" id="Seg_2080" s="T29">np.h:S</ta>
            <ta e="T31" id="Seg_2081" s="T30">np.h:S</ta>
            <ta e="T32" id="Seg_2082" s="T31">np.h:S</ta>
            <ta e="T33" id="Seg_2083" s="T32">np.h:S</ta>
            <ta e="T34" id="Seg_2084" s="T33">np.h:S</ta>
            <ta e="T35" id="Seg_2085" s="T34">np.h:S</ta>
            <ta e="T37" id="Seg_2086" s="T36">np.h:S</ta>
            <ta e="T38" id="Seg_2087" s="T37">np.h:S</ta>
            <ta e="T39" id="Seg_2088" s="T38">v:pred</ta>
            <ta e="T40" id="Seg_2089" s="T39">np.h:S</ta>
            <ta e="T41" id="Seg_2090" s="T40">v:pred</ta>
            <ta e="T42" id="Seg_2091" s="T41">pro.h:S</ta>
            <ta e="T43" id="Seg_2092" s="T42">v:pred</ta>
            <ta e="T46" id="Seg_2093" s="T45">np.h:O</ta>
            <ta e="T47" id="Seg_2094" s="T46">pro.h:S</ta>
            <ta e="T48" id="Seg_2095" s="T47">v:pred</ta>
            <ta e="T49" id="Seg_2096" s="T48">s:adv</ta>
            <ta e="T53" id="Seg_2097" s="T52">np.h:S</ta>
            <ta e="T54" id="Seg_2098" s="T53">v:pred</ta>
            <ta e="T57" id="Seg_2099" s="T56">np.h:O</ta>
            <ta e="T58" id="Seg_2100" s="T57">np.h:S</ta>
            <ta e="T59" id="Seg_2101" s="T58">v:pred</ta>
            <ta e="T61" id="Seg_2102" s="T60">np:O</ta>
            <ta e="T62" id="Seg_2103" s="T61">pro.h:S</ta>
            <ta e="T63" id="Seg_2104" s="T62">cop</ta>
            <ta e="T65" id="Seg_2105" s="T64">adj:pred</ta>
            <ta e="T66" id="Seg_2106" s="T65">pro.h:S</ta>
            <ta e="T67" id="Seg_2107" s="T66">v:pred</ta>
            <ta e="T72" id="Seg_2108" s="T69">s:temp</ta>
            <ta e="T73" id="Seg_2109" s="T72">0.3.h:S v:pred</ta>
            <ta e="T75" id="Seg_2110" s="T73">s:compl</ta>
            <ta e="T76" id="Seg_2111" s="T75">np.h:S</ta>
            <ta e="T78" id="Seg_2112" s="T77">np.h:S</ta>
            <ta e="T79" id="Seg_2113" s="T78">v:pred</ta>
            <ta e="T81" id="Seg_2114" s="T80">pro.h:S</ta>
            <ta e="T82" id="Seg_2115" s="T81">v:pred</ta>
            <ta e="T87" id="Seg_2116" s="T84">s:adv</ta>
            <ta e="T88" id="Seg_2117" s="T87">0.3.h:S v:pred</ta>
            <ta e="T90" id="Seg_2118" s="T89">pro.h:S</ta>
            <ta e="T91" id="Seg_2119" s="T90">v:pred</ta>
            <ta e="T92" id="Seg_2120" s="T91">s:purp</ta>
            <ta e="T96" id="Seg_2121" s="T93">s:rel</ta>
            <ta e="T98" id="Seg_2122" s="T97">np.h:S</ta>
            <ta e="T99" id="Seg_2123" s="T98">v:pred</ta>
            <ta e="T103" id="Seg_2124" s="T101">s:compl</ta>
            <ta e="T104" id="Seg_2125" s="T103">adj:pred</ta>
            <ta e="T105" id="Seg_2126" s="T104">cop</ta>
            <ta e="T106" id="Seg_2127" s="T105">pro.h:O</ta>
            <ta e="T108" id="Seg_2128" s="T107">pro.h:S</ta>
            <ta e="T110" id="Seg_2129" s="T109">v:pred</ta>
            <ta e="T112" id="Seg_2130" s="T111">pro.h:S</ta>
            <ta e="T114" id="Seg_2131" s="T113">pro.h:O</ta>
            <ta e="T116" id="Seg_2132" s="T115">v:pred</ta>
            <ta e="T118" id="Seg_2133" s="T116">s:adv</ta>
            <ta e="T119" id="Seg_2134" s="T118">v:pred</ta>
            <ta e="T122" id="Seg_2135" s="T121">np.h:S</ta>
            <ta e="T125" id="Seg_2136" s="T124">pro:pred</ta>
            <ta e="T126" id="Seg_2137" s="T125">np:S</ta>
            <ta e="T127" id="Seg_2138" s="T126">pro.h:S</ta>
            <ta e="T129" id="Seg_2139" s="T128">v:pred</ta>
            <ta e="T130" id="Seg_2140" s="T129">pro.h:S</ta>
            <ta e="T133" id="Seg_2141" s="T132">v:pred</ta>
            <ta e="T139" id="Seg_2142" s="T138">np:S</ta>
            <ta e="T141" id="Seg_2143" s="T140">adj:pred</ta>
            <ta e="T142" id="Seg_2144" s="T141">cop</ta>
            <ta e="T143" id="Seg_2145" s="T142">adj:pred</ta>
            <ta e="T144" id="Seg_2146" s="T143">0.3:S cop</ta>
            <ta e="T146" id="Seg_2147" s="T145">v:pred</ta>
            <ta e="T148" id="Seg_2148" s="T147">np:S</ta>
            <ta e="T149" id="Seg_2149" s="T148">np.h:S</ta>
            <ta e="T150" id="Seg_2150" s="T149">v:pred</ta>
            <ta e="T153" id="Seg_2151" s="T152">0.3.h:S v:pred</ta>
            <ta e="T154" id="Seg_2152" s="T153">np:O</ta>
            <ta e="T156" id="Seg_2153" s="T155">np:O</ta>
            <ta e="T157" id="Seg_2154" s="T156">pro.h:S</ta>
            <ta e="T159" id="Seg_2155" s="T158">v:pred</ta>
            <ta e="T161" id="Seg_2156" s="T160">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_2157" s="T4">RUS:cult</ta>
            <ta e="T6" id="Seg_2158" s="T5">RUS:cult</ta>
            <ta e="T11" id="Seg_2159" s="T10">RUS:cult</ta>
            <ta e="T12" id="Seg_2160" s="T11">RUS:cult</ta>
            <ta e="T18" id="Seg_2161" s="T17">RUS:cult</ta>
            <ta e="T21" id="Seg_2162" s="T20">RUS:cult</ta>
            <ta e="T23" id="Seg_2163" s="T22">RUS:cult</ta>
            <ta e="T36" id="Seg_2164" s="T35">RUS:gram</ta>
            <ta e="T77" id="Seg_2165" s="T76">RUS:gram</ta>
            <ta e="T93" id="Seg_2166" s="T92">RUS:cult</ta>
            <ta e="T103" id="Seg_2167" s="T102">RUS:cult</ta>
            <ta e="T107" id="Seg_2168" s="T106">RUS:gram</ta>
            <ta e="T111" id="Seg_2169" s="T110">RUS:gram</ta>
            <ta e="T113" id="Seg_2170" s="T112">RUS:gram</ta>
            <ta e="T126" id="Seg_2171" s="T125">RUS:cult</ta>
            <ta e="T134" id="Seg_2172" s="T133">RUS:cult</ta>
            <ta e="T136" id="Seg_2173" s="T135">RUS:cult</ta>
            <ta e="T137" id="Seg_2174" s="T136">RUS:cult</ta>
            <ta e="T139" id="Seg_2175" s="T138">RUS:cult</ta>
            <ta e="T148" id="Seg_2176" s="T147">RUS:cult</ta>
            <ta e="T149" id="Seg_2177" s="T148">RUS:cult</ta>
            <ta e="T151" id="Seg_2178" s="T150">RUS:cult</ta>
            <ta e="T152" id="Seg_2179" s="T151">RUS:gram</ta>
            <ta e="T155" id="Seg_2180" s="T154">RUS:gram</ta>
            <ta e="T160" id="Seg_2181" s="T159">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_2182" s="T0">Я ходил вчера с Витей в кино.</ta>
            <ta e="T12" id="Seg_2183" s="T6">Я вчера смотрел кино "Солдатка".</ta>
            <ta e="T16" id="Seg_2184" s="T12">Это было после войны.</ta>
            <ta e="T20" id="Seg_2185" s="T16">Все солдаты возвращались домой.</ta>
            <ta e="T23" id="Seg_2186" s="T20">На станцию приехал поезд.</ta>
            <ta e="T37" id="Seg_2187" s="T23">Солдат встречали сыновья, дочери, жёны, отцы, сёстры, матери, братья, дедушки, бабушки и внуки.</ta>
            <ta e="T41" id="Seg_2188" s="T37">Многие смеются, некоторые плачут.</ta>
            <ta e="T46" id="Seg_2189" s="T41">Мы видели пять военных.</ta>
            <ta e="T49" id="Seg_2190" s="T46">Они разговаривали, смеясь.</ta>
            <ta e="T57" id="Seg_2191" s="T49">Вдруг один военный увидел одну военную девушку.</ta>
            <ta e="T61" id="Seg_2192" s="T57">Девушка была одета в военный костюм.</ta>
            <ta e="T65" id="Seg_2193" s="T61">Она была очень красивая.</ta>
            <ta e="T69" id="Seg_2194" s="T65">Она шла домой, в свой дом.</ta>
            <ta e="T75" id="Seg_2195" s="T69">Когда пришла к дому, увидела, что он рухнул.</ta>
            <ta e="T80" id="Seg_2196" s="T75">Её отец и мать погибли на войне.</ta>
            <ta e="T84" id="Seg_2197" s="T80">Она пошла к подруге домой.</ta>
            <ta e="T88" id="Seg_2198" s="T84">Увидев друг друга, они обрадовались.</ta>
            <ta e="T96" id="Seg_2199" s="T88">Потом она пошла работать в совхоз, где похоронен её муж.</ta>
            <ta e="T100" id="Seg_2200" s="T96">Её муж погиб на войне.</ta>
            <ta e="T105" id="Seg_2201" s="T100">Ей было тяжело работать в совхозе.</ta>
            <ta e="T116" id="Seg_2202" s="T105">Её никто не знал, и она никого не знала.</ta>
            <ta e="T122" id="Seg_2203" s="T116">Её приехал искать один военный.</ta>
            <ta e="T126" id="Seg_2204" s="T122">— "Сегодня какое кино?"</ta>
            <ta e="T129" id="Seg_2205" s="T126">— "Я не знаю".</ta>
            <ta e="T137" id="Seg_2206" s="T129">— "Я сегодня пойду в кино на семичасовое кино".</ta>
            <ta e="T142" id="Seg_2207" s="T137">Наш клуб очень хороший.</ta>
            <ta e="T148" id="Seg_2208" s="T142">Он хороший, поэтому сломано много табуреток.</ta>
            <ta e="T156" id="Seg_2209" s="T148">Витя ходил в магазин и принес хлеб и табак.</ta>
            <ta e="T161" id="Seg_2210" s="T156">Мы сейчас будем есть и курить.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_2211" s="T0">Yesterday I went with Vitya to the cinema.</ta>
            <ta e="T12" id="Seg_2212" s="T6">I've seen the film "The soldier's wife".</ta>
            <ta e="T16" id="Seg_2213" s="T12">It was after the war.</ta>
            <ta e="T20" id="Seg_2214" s="T16">All soldiers were coming back home.</ta>
            <ta e="T23" id="Seg_2215" s="T20">A train arrived at the station.</ta>
            <ta e="T37" id="Seg_2216" s="T23">The soldiers were met by their sons, daughters, wives, fathers, sisters, mothers, brothers, grandfathers, grandmothers and grandchildren.</ta>
            <ta e="T41" id="Seg_2217" s="T37">Many laugh, others cry.</ta>
            <ta e="T46" id="Seg_2218" s="T41">We saw five soldiers.</ta>
            <ta e="T49" id="Seg_2219" s="T46">They were talking, laughing.</ta>
            <ta e="T57" id="Seg_2220" s="T49">Suddenly one soldier saw one soldier girl.</ta>
            <ta e="T61" id="Seg_2221" s="T57">The girl had a war suit on.</ta>
            <ta e="T65" id="Seg_2222" s="T61">She was very beautiful.</ta>
            <ta e="T69" id="Seg_2223" s="T65">She went home, her home.</ta>
            <ta e="T75" id="Seg_2224" s="T69">As she came to her house, she saw, that it had been destroyed.</ta>
            <ta e="T80" id="Seg_2225" s="T75">Her mother and father had died in the war.</ta>
            <ta e="T84" id="Seg_2226" s="T80">She went to her girlfriend's house.</ta>
            <ta e="T88" id="Seg_2227" s="T84">As they saw each other, they were happy.</ta>
            <ta e="T96" id="Seg_2228" s="T88">Then she went to work to sovkhoz, where her husband had been buried.</ta>
            <ta e="T100" id="Seg_2229" s="T96">Her husband died in the war.</ta>
            <ta e="T105" id="Seg_2230" s="T100">It was hard for her to work in sovkhoz.</ta>
            <ta e="T116" id="Seg_2231" s="T105">Nobody knew her and she didn't know anybody.</ta>
            <ta e="T122" id="Seg_2232" s="T116">One soldier came to search for her.</ta>
            <ta e="T126" id="Seg_2233" s="T122">— "What film is it today?"</ta>
            <ta e="T129" id="Seg_2234" s="T126">— "I don't know".</ta>
            <ta e="T137" id="Seg_2235" s="T129">— "Today I will go to the cinema to the film at seven p.m."</ta>
            <ta e="T142" id="Seg_2236" s="T137">Our club is very good.</ta>
            <ta e="T148" id="Seg_2237" s="T142">It is good, that's why a lot of stools are broken.</ta>
            <ta e="T156" id="Seg_2238" s="T148">Vitya went to the shop and brought some bread and tobacco.</ta>
            <ta e="T161" id="Seg_2239" s="T156">We will eat and smoke now.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_2240" s="T0">Ich bin gestern mit Vitja ins Kino gegangen.</ta>
            <ta e="T12" id="Seg_2241" s="T6">Ich habe gestern den Film "Die Soldatin" gesehen.</ta>
            <ta e="T16" id="Seg_2242" s="T12">Es war nach dem Krieg.</ta>
            <ta e="T20" id="Seg_2243" s="T16">Alle Soldaten kamen nach Hause.</ta>
            <ta e="T23" id="Seg_2244" s="T20">Ein Zug ist auf dem Bahnhof angekommen.</ta>
            <ta e="T37" id="Seg_2245" s="T23">Die Soldaten wurden von ihren Söhnen, Töchtern, Frauen, Vätern, Schwestern, Müttern, Brüdern, Großvätern, Großmüttern und Enkelkindern empfangen.</ta>
            <ta e="T41" id="Seg_2246" s="T37">Viele lachen, manche weinen.</ta>
            <ta e="T46" id="Seg_2247" s="T41">Wir sahen fünf Soldaten.</ta>
            <ta e="T49" id="Seg_2248" s="T46">Sie unterhielten sich lachend.</ta>
            <ta e="T57" id="Seg_2249" s="T49">Plötzlich sah ein Soldat eine Soldatin.</ta>
            <ta e="T61" id="Seg_2250" s="T57">Das Mädchen hatte eine Kriegsuniform an.</ta>
            <ta e="T65" id="Seg_2251" s="T61">Sie war sehr schön.</ta>
            <ta e="T69" id="Seg_2252" s="T65">Sie ging nach Hause, zu ihrem Haus.</ta>
            <ta e="T75" id="Seg_2253" s="T69">Als sie zum Haus kam, sah sie, dass es eingestürzt war.</ta>
            <ta e="T80" id="Seg_2254" s="T75">Ihr Vater und ihre Mutter waren im Krieg gestorben.</ta>
            <ta e="T84" id="Seg_2255" s="T80">Sie ging zum Haus ihrer Freundin.</ta>
            <ta e="T88" id="Seg_2256" s="T84">Als sie sich sahen, freuten sie sich.</ta>
            <ta e="T96" id="Seg_2257" s="T88">Dann ist sie in die Sowchose arbeiten gegangen, wo ihr Mann begraben geworden war.</ta>
            <ta e="T100" id="Seg_2258" s="T96">Ihr Mann war im Krieg gefallen.</ta>
            <ta e="T105" id="Seg_2259" s="T100">Es war schwierig für sie in der Sowchose zu arbeiten.</ta>
            <ta e="T116" id="Seg_2260" s="T105">Niemand kannte sie und sie kannte niemanden.</ta>
            <ta e="T122" id="Seg_2261" s="T116">Ein Soldat kam, um sie zu suchen.</ta>
            <ta e="T126" id="Seg_2262" s="T122">— "Welcher Film läuft heute?"</ta>
            <ta e="T129" id="Seg_2263" s="T126">— "Ich weiß es nicht".</ta>
            <ta e="T137" id="Seg_2264" s="T129">— "Heute gehe ich ins Kino zum Film um sieben Uhr."</ta>
            <ta e="T142" id="Seg_2265" s="T137">Unser Klub ist sehr gut.</ta>
            <ta e="T148" id="Seg_2266" s="T142">Der ist gut, deswegen sind viele Hocker kaputt.</ta>
            <ta e="T156" id="Seg_2267" s="T148">Vitja ist in den Laden gegangen und hat Brot und Tabak mitgebracht.</ta>
            <ta e="T161" id="Seg_2268" s="T156">Jetzt werden wir essen und rauchen.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_2269" s="T0">я ходил вчера с в Вичей [Витей] в кино.</ta>
            <ta e="T12" id="Seg_2270" s="T6">я вчера смотрел кино солдатка.</ta>
            <ta e="T16" id="Seg_2271" s="T12">это было после войны.</ta>
            <ta e="T20" id="Seg_2272" s="T16">все солдаты возвращались домой.</ta>
            <ta e="T37" id="Seg_2273" s="T23">солдат встречают сыновья, дочери и жены, отцы сестры матери братья, дедушки бабушки внуки.</ta>
            <ta e="T41" id="Seg_2274" s="T37">многие смеются некоторые плачут.</ta>
            <ta e="T46" id="Seg_2275" s="T41">мы видели пять военных.</ta>
            <ta e="T49" id="Seg_2276" s="T46">они разговаривали смеясь.</ta>
            <ta e="T57" id="Seg_2277" s="T49">вдруг один военный увидел одну военную девушку.</ta>
            <ta e="T61" id="Seg_2278" s="T57">девушка одета была в военный костюм.</ta>
            <ta e="T65" id="Seg_2279" s="T61">она была очень хорошая.</ta>
            <ta e="T69" id="Seg_2280" s="T65">она шла [возвращалась] домой.</ta>
            <ta e="T75" id="Seg_2281" s="T69">пришла домой. увидела развалина (упал).</ta>
            <ta e="T80" id="Seg_2282" s="T75">отец и мать умерли в войне.</ta>
            <ta e="T84" id="Seg_2283" s="T80">она пошла к подруге домой.</ta>
            <ta e="T88" id="Seg_2284" s="T84">друг друга (сами себя) (увидев) увидали и обрадовались [развеселились].</ta>
            <ta e="T96" id="Seg_2285" s="T88">потом она пришла работать в совхоз где похоронен (ее) муж.</ta>
            <ta e="T100" id="Seg_2286" s="T96">ее муж погиб на войне.</ta>
            <ta e="T105" id="Seg_2287" s="T100">ей работать в совхозе тяжело было</ta>
            <ta e="T116" id="Seg_2288" s="T105">ее никто не знал она никого не знала.</ta>
            <ta e="T122" id="Seg_2289" s="T116">ее ища приехал один военный человек.</ta>
            <ta e="T126" id="Seg_2290" s="T122">сегодня какое кино</ta>
            <ta e="T129" id="Seg_2291" s="T126">я не знаю.</ta>
            <ta e="T137" id="Seg_2292" s="T129">я сегодня пойду на 7 часовое кино.</ta>
            <ta e="T142" id="Seg_2293" s="T137">наш клуб очень хороший.</ta>
            <ta e="T148" id="Seg_2294" s="T142">хорошо потому сломано много тубареток.</ta>
            <ta e="T156" id="Seg_2295" s="T148">Витя ходил в магазин принес хлеб и папиросы.</ta>
            <ta e="T161" id="Seg_2296" s="T156">мы сейчас будем кушать и курить.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T41" id="Seg_2297" s="T37">[OSV]: The possessive flexions in the pronominal forms "kočʼinʼe-iː-^0-tɨ" (much-PL-NOM-3SG) and "ɔːm-iː-^0-tɨ" (other-PL-NOM-3SG) have here probably a referential function.</ta>
            <ta e="T49" id="Seg_2298" s="T46">[OSV]: The form "pisäla" has been edited into "pissɛːla" though the usage of the Perfective suffix "ɛː" is here not clear.</ta>
            <ta e="T65" id="Seg_2299" s="T61">[OSV]: "orsä" - "very".</ta>
            <ta e="T96" id="Seg_2300" s="T88">[OSV]: "taqɨqo" has here the meaning "to bury".</ta>
            <ta e="T142" id="Seg_2301" s="T137">[OSV]: "orsa" — "very".</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
