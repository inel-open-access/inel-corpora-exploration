<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID3D579DF9-548D-BF7E-92F3-EF81C3931520">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\PES_1964_SquirrelHunting_nar\PES_1964_SquirrelHunting_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">39</ud-information>
            <ud-information attribute-name="# HIAT:w">30</ud-information>
            <ud-information attribute-name="# e">30</ud-information>
            <ud-information attribute-name="# HIAT:u">8</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PES">
            <abbreviation>PES</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PES"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T30" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ärran</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">palʼdʼuzaŋ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">matʼtʼöndə</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">man</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">nʼäjässan</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_26" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">kännaŋ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">nʼäjam</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">qozɨt</ts>
                  <nts id="Seg_35" n="HIAT:ip">,</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">muːdaŋ</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_42" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_44" n="HIAT:w" s="T10">man</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_47" n="HIAT:w" s="T11">nʼäjam</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">tʼätʼčam</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">nʼäja</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">assɨ</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">pɨŋalɨŋ</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_63" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">nʼäjam</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">aːčezam</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_72" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">kännaŋ</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">suːrum</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">tʼäj</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">mat</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">qoɨt</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_90" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">man</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_95" n="HIAT:w" s="T24">qwannaŋ</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">nitʼä</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_102" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_104" n="HIAT:w" s="T26">täp</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_107" n="HIAT:w" s="T27">tʼün</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">pučondə</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">muːdaŋ</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T30" id="Seg_116" n="sc" s="T0">
               <ts e="T1" id="Seg_118" n="e" s="T0">man </ts>
               <ts e="T2" id="Seg_120" n="e" s="T1">ärran </ts>
               <ts e="T3" id="Seg_122" n="e" s="T2">palʼdʼuzaŋ </ts>
               <ts e="T4" id="Seg_124" n="e" s="T3">matʼtʼöndə. </ts>
               <ts e="T5" id="Seg_126" n="e" s="T4">man </ts>
               <ts e="T6" id="Seg_128" n="e" s="T5">nʼäjässan. </ts>
               <ts e="T7" id="Seg_130" n="e" s="T6">kännaŋ </ts>
               <ts e="T8" id="Seg_132" n="e" s="T7">nʼäjam </ts>
               <ts e="T9" id="Seg_134" n="e" s="T8">qozɨt, </ts>
               <ts e="T10" id="Seg_136" n="e" s="T9">muːdaŋ. </ts>
               <ts e="T11" id="Seg_138" n="e" s="T10">man </ts>
               <ts e="T12" id="Seg_140" n="e" s="T11">nʼäjam </ts>
               <ts e="T13" id="Seg_142" n="e" s="T12">tʼätʼčam </ts>
               <ts e="T14" id="Seg_144" n="e" s="T13">nʼäja </ts>
               <ts e="T15" id="Seg_146" n="e" s="T14">assɨ </ts>
               <ts e="T16" id="Seg_148" n="e" s="T15">pɨŋalɨŋ. </ts>
               <ts e="T17" id="Seg_150" n="e" s="T16">nʼäjam </ts>
               <ts e="T18" id="Seg_152" n="e" s="T17">aːčezam. </ts>
               <ts e="T19" id="Seg_154" n="e" s="T18">kännaŋ </ts>
               <ts e="T20" id="Seg_156" n="e" s="T19">suːrum </ts>
               <ts e="T21" id="Seg_158" n="e" s="T20">tʼäj </ts>
               <ts e="T22" id="Seg_160" n="e" s="T21">mat </ts>
               <ts e="T23" id="Seg_162" n="e" s="T22">qoɨt. </ts>
               <ts e="T24" id="Seg_164" n="e" s="T23">man </ts>
               <ts e="T25" id="Seg_166" n="e" s="T24">qwannaŋ </ts>
               <ts e="T26" id="Seg_168" n="e" s="T25">nitʼä. </ts>
               <ts e="T27" id="Seg_170" n="e" s="T26">täp </ts>
               <ts e="T28" id="Seg_172" n="e" s="T27">tʼün </ts>
               <ts e="T29" id="Seg_174" n="e" s="T28">pučondə </ts>
               <ts e="T30" id="Seg_176" n="e" s="T29">muːdaŋ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_177" s="T0">PES_1964_SquirrelHunting_nar.001 (001.001)</ta>
            <ta e="T6" id="Seg_178" s="T4">PES_1964_SquirrelHunting_nar.002 (001.002)</ta>
            <ta e="T10" id="Seg_179" s="T6">PES_1964_SquirrelHunting_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_180" s="T10">PES_1964_SquirrelHunting_nar.004 (001.004)</ta>
            <ta e="T18" id="Seg_181" s="T16">PES_1964_SquirrelHunting_nar.005 (001.005)</ta>
            <ta e="T23" id="Seg_182" s="T18">PES_1964_SquirrelHunting_nar.006 (001.006)</ta>
            <ta e="T26" id="Seg_183" s="T23">PES_1964_SquirrelHunting_nar.007 (001.007)</ta>
            <ta e="T30" id="Seg_184" s="T26">PES_1964_SquirrelHunting_nar.008 (001.008)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_185" s="T0">ман ӓ′р(р)ан палʼдʼу′заң ма′тʼтʼӧндъ.</ta>
            <ta e="T6" id="Seg_186" s="T4">ман ′нʼӓjӓссан.</ta>
            <ta e="T10" id="Seg_187" s="T6">кӓ′ннаң нʼӓjам kо′зыт, ′мӯдаң.</ta>
            <ta e="T16" id="Seg_188" s="T10">ман нʼӓjам тʼӓтʼчам(w). ′нʼӓjа а′ссы ′пыңалың.</ta>
            <ta e="T18" id="Seg_189" s="T16">′нʼӓjам ′а̄тшезам.</ta>
            <ta e="T23" id="Seg_190" s="T18">кӓ′ннаң ′сӯрум ′тʼӓй ′мат ′kоыт.</ta>
            <ta e="T26" id="Seg_191" s="T23">ман kwа′ннаң ни′тʼӓ.</ta>
            <ta e="T30" id="Seg_192" s="T26">тӓп тʼӱн ′путшондъ ′мӯдаң.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_193" s="T0">man är(r)an palʼdʼuzaŋ matʼtʼöndə.</ta>
            <ta e="T6" id="Seg_194" s="T4">man nʼäjässan.</ta>
            <ta e="T10" id="Seg_195" s="T6">kännaŋ nʼäjam qozɨt, muːdaŋ.</ta>
            <ta e="T16" id="Seg_196" s="T10">man nʼäjam tʼätʼčam(w). nʼäja assɨ pɨŋalɨŋ.</ta>
            <ta e="T18" id="Seg_197" s="T16">nʼäjam aːtšezam.</ta>
            <ta e="T23" id="Seg_198" s="T18">kännaŋ suːrum tʼäj mat qoɨt.</ta>
            <ta e="T26" id="Seg_199" s="T23">man qwannaŋ nitʼä.</ta>
            <ta e="T30" id="Seg_200" s="T26">täp tʼün putšondə muːdaŋ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_201" s="T0">man ärran palʼdʼuzaŋ matʼtʼöndə. </ta>
            <ta e="T6" id="Seg_202" s="T4">man nʼäjässan. </ta>
            <ta e="T10" id="Seg_203" s="T6">kännaŋ nʼäjam qozɨt, muːdaŋ. </ta>
            <ta e="T16" id="Seg_204" s="T10">man nʼäjam tʼätʼčam nʼäja assɨ pɨŋalɨŋ. </ta>
            <ta e="T18" id="Seg_205" s="T16">nʼäjam aːčezam. </ta>
            <ta e="T23" id="Seg_206" s="T18">kännaŋ suːrum tʼäj mat qoɨt. </ta>
            <ta e="T26" id="Seg_207" s="T23">man qwannaŋ nitʼä. </ta>
            <ta e="T30" id="Seg_208" s="T26">täp tʼün pučondə muːdaŋ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_209" s="T0">man</ta>
            <ta e="T2" id="Seg_210" s="T1">ärra-n</ta>
            <ta e="T3" id="Seg_211" s="T2">palʼdʼu-za-ŋ</ta>
            <ta e="T4" id="Seg_212" s="T3">matʼtʼö-ndə</ta>
            <ta e="T5" id="Seg_213" s="T4">man</ta>
            <ta e="T6" id="Seg_214" s="T5">nʼäjä-s-sa-n</ta>
            <ta e="T7" id="Seg_215" s="T6">kännaŋ</ta>
            <ta e="T8" id="Seg_216" s="T7">nʼäja-m</ta>
            <ta e="T9" id="Seg_217" s="T8">qo-zɨ-t</ta>
            <ta e="T10" id="Seg_218" s="T9">muːda-ŋ</ta>
            <ta e="T11" id="Seg_219" s="T10">man</ta>
            <ta e="T12" id="Seg_220" s="T11">nʼäja-m</ta>
            <ta e="T13" id="Seg_221" s="T12">tʼätʼča-m</ta>
            <ta e="T14" id="Seg_222" s="T13">nʼäja</ta>
            <ta e="T15" id="Seg_223" s="T14">assɨ</ta>
            <ta e="T16" id="Seg_224" s="T15">pɨŋa-lɨ-ŋ</ta>
            <ta e="T17" id="Seg_225" s="T16">nʼäja-m</ta>
            <ta e="T18" id="Seg_226" s="T17">aːče-za-m</ta>
            <ta e="T19" id="Seg_227" s="T18">kännaŋ</ta>
            <ta e="T20" id="Seg_228" s="T19">suːrum</ta>
            <ta e="T21" id="Seg_229" s="T20">tʼäj</ta>
            <ta e="T22" id="Seg_230" s="T21">mat</ta>
            <ta e="T23" id="Seg_231" s="T22">qo-ɨ-t</ta>
            <ta e="T24" id="Seg_232" s="T23">man</ta>
            <ta e="T25" id="Seg_233" s="T24">qwan-na-ŋ</ta>
            <ta e="T26" id="Seg_234" s="T25">nitʼä</ta>
            <ta e="T27" id="Seg_235" s="T26">täp</ta>
            <ta e="T28" id="Seg_236" s="T27">tʼü-n</ta>
            <ta e="T29" id="Seg_237" s="T28">pučo-ndə</ta>
            <ta e="T30" id="Seg_238" s="T29">muːda-ŋ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_239" s="T0">man</ta>
            <ta e="T2" id="Seg_240" s="T1">ara-n</ta>
            <ta e="T3" id="Seg_241" s="T2">paldʼu-sɨ-ŋ</ta>
            <ta e="T4" id="Seg_242" s="T3">matʼtʼi-ndɨ</ta>
            <ta e="T5" id="Seg_243" s="T4">man</ta>
            <ta e="T6" id="Seg_244" s="T5">nʼaja-j-sɨ-ŋ</ta>
            <ta e="T7" id="Seg_245" s="T6">kanak</ta>
            <ta e="T8" id="Seg_246" s="T7">nʼaja-m</ta>
            <ta e="T9" id="Seg_247" s="T8">qo-sɨ-tɨ</ta>
            <ta e="T10" id="Seg_248" s="T9">mudoj-n</ta>
            <ta e="T11" id="Seg_249" s="T10">man</ta>
            <ta e="T12" id="Seg_250" s="T11">nʼaja-m</ta>
            <ta e="T13" id="Seg_251" s="T12">tʼaččɨ-m</ta>
            <ta e="T14" id="Seg_252" s="T13">nʼaja</ta>
            <ta e="T15" id="Seg_253" s="T14">assɨ</ta>
            <ta e="T16" id="Seg_254" s="T15">puŋgə-lɨ-n</ta>
            <ta e="T17" id="Seg_255" s="T16">nʼaja-m</ta>
            <ta e="T18" id="Seg_256" s="T17">atʼɨ-sɨ-m</ta>
            <ta e="T19" id="Seg_257" s="T18">kanak</ta>
            <ta e="T20" id="Seg_258" s="T19">suːrǝm</ta>
            <ta e="T21" id="Seg_259" s="T20">taji</ta>
            <ta e="T22" id="Seg_260" s="T21">maːt</ta>
            <ta e="T23" id="Seg_261" s="T22">qo-ɨ-tɨ</ta>
            <ta e="T24" id="Seg_262" s="T23">man</ta>
            <ta e="T25" id="Seg_263" s="T24">qwən-ŋɨ-ŋ</ta>
            <ta e="T26" id="Seg_264" s="T25">natʼtʼa</ta>
            <ta e="T27" id="Seg_265" s="T26">tap</ta>
            <ta e="T28" id="Seg_266" s="T27">tʼü-n</ta>
            <ta e="T29" id="Seg_267" s="T28">puːčə-ndɨ</ta>
            <ta e="T30" id="Seg_268" s="T29">mudoj-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_269" s="T0">I.NOM</ta>
            <ta e="T2" id="Seg_270" s="T1">autumn-ADV.LOC</ta>
            <ta e="T3" id="Seg_271" s="T2">go-PST-1SG.S</ta>
            <ta e="T4" id="Seg_272" s="T3">taiga-ILL</ta>
            <ta e="T5" id="Seg_273" s="T4">I.NOM</ta>
            <ta e="T6" id="Seg_274" s="T5">squirrel-CAP-PST-1SG.S</ta>
            <ta e="T7" id="Seg_275" s="T6">dog.[NOM]</ta>
            <ta e="T8" id="Seg_276" s="T7">squirrel-ACC</ta>
            <ta e="T9" id="Seg_277" s="T8">find-PST-3SG.O</ta>
            <ta e="T10" id="Seg_278" s="T9">bark-3SG.S</ta>
            <ta e="T11" id="Seg_279" s="T10">I.NOM</ta>
            <ta e="T12" id="Seg_280" s="T11">squirrel-ACC</ta>
            <ta e="T13" id="Seg_281" s="T12">shoot-1SG.O</ta>
            <ta e="T14" id="Seg_282" s="T13">squirrel.[NOM]</ta>
            <ta e="T15" id="Seg_283" s="T14">NEG</ta>
            <ta e="T16" id="Seg_284" s="T15">fall-RES-3SG.S</ta>
            <ta e="T17" id="Seg_285" s="T16">squirrel-ACC</ta>
            <ta e="T18" id="Seg_286" s="T17">watch.for-PST-1SG.O</ta>
            <ta e="T19" id="Seg_287" s="T18">dog.[NOM]</ta>
            <ta e="T20" id="Seg_288" s="T19">wild.animal.[NOM]</ta>
            <ta e="T21" id="Seg_289" s="T20">taiga</ta>
            <ta e="T22" id="Seg_290" s="T21">house</ta>
            <ta e="T23" id="Seg_291" s="T22">find-EP-3SG.O</ta>
            <ta e="T24" id="Seg_292" s="T23">I.NOM</ta>
            <ta e="T25" id="Seg_293" s="T24">go.away-CO-1SG.S</ta>
            <ta e="T26" id="Seg_294" s="T25">there</ta>
            <ta e="T27" id="Seg_295" s="T26">he.NOM</ta>
            <ta e="T28" id="Seg_296" s="T27">earth-GEN</ta>
            <ta e="T29" id="Seg_297" s="T28">inside-ILL</ta>
            <ta e="T30" id="Seg_298" s="T29">bark-3SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_299" s="T0">я.NOM</ta>
            <ta e="T2" id="Seg_300" s="T1">осень-ADV.LOC</ta>
            <ta e="T3" id="Seg_301" s="T2">ходить-PST-1SG.S</ta>
            <ta e="T4" id="Seg_302" s="T3">тайга-ILL</ta>
            <ta e="T5" id="Seg_303" s="T4">я.NOM</ta>
            <ta e="T6" id="Seg_304" s="T5">белка-CAP-PST-1SG.S</ta>
            <ta e="T7" id="Seg_305" s="T6">собака.[NOM]</ta>
            <ta e="T8" id="Seg_306" s="T7">белка-ACC</ta>
            <ta e="T9" id="Seg_307" s="T8">найти-PST-3SG.O</ta>
            <ta e="T10" id="Seg_308" s="T9">лаять-3SG.S</ta>
            <ta e="T11" id="Seg_309" s="T10">я.NOM</ta>
            <ta e="T12" id="Seg_310" s="T11">белка-ACC</ta>
            <ta e="T13" id="Seg_311" s="T12">стрелять-1SG.O</ta>
            <ta e="T14" id="Seg_312" s="T13">белка.[NOM]</ta>
            <ta e="T15" id="Seg_313" s="T14">NEG</ta>
            <ta e="T16" id="Seg_314" s="T15">упасть-RES-3SG.S</ta>
            <ta e="T17" id="Seg_315" s="T16">белка-ACC</ta>
            <ta e="T18" id="Seg_316" s="T17">караулить-PST-1SG.O</ta>
            <ta e="T19" id="Seg_317" s="T18">собака.[NOM]</ta>
            <ta e="T20" id="Seg_318" s="T19">зверь.[NOM]</ta>
            <ta e="T21" id="Seg_319" s="T20">таёжный</ta>
            <ta e="T22" id="Seg_320" s="T21">дом</ta>
            <ta e="T23" id="Seg_321" s="T22">найти-EP-3SG.O</ta>
            <ta e="T24" id="Seg_322" s="T23">я.NOM</ta>
            <ta e="T25" id="Seg_323" s="T24">уйти-CO-1SG.S</ta>
            <ta e="T26" id="Seg_324" s="T25">туда</ta>
            <ta e="T27" id="Seg_325" s="T26">он.NOM</ta>
            <ta e="T28" id="Seg_326" s="T27">земля-GEN</ta>
            <ta e="T29" id="Seg_327" s="T28">внутренность-ILL</ta>
            <ta e="T30" id="Seg_328" s="T29">лаять-3SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_329" s="T0">pers</ta>
            <ta e="T2" id="Seg_330" s="T1">n-adv:case</ta>
            <ta e="T3" id="Seg_331" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_332" s="T3">n-n:case2</ta>
            <ta e="T5" id="Seg_333" s="T4">pers</ta>
            <ta e="T6" id="Seg_334" s="T5">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_335" s="T6">n-n:case1</ta>
            <ta e="T8" id="Seg_336" s="T7">n-n:case1</ta>
            <ta e="T9" id="Seg_337" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_338" s="T9">v-v:pn</ta>
            <ta e="T11" id="Seg_339" s="T10">pers</ta>
            <ta e="T12" id="Seg_340" s="T11">n-n:case1</ta>
            <ta e="T13" id="Seg_341" s="T12">v-v:pn</ta>
            <ta e="T14" id="Seg_342" s="T13">n-n:case1</ta>
            <ta e="T15" id="Seg_343" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_344" s="T15">v-v&gt;v-v:pn</ta>
            <ta e="T17" id="Seg_345" s="T16">n-n:case1</ta>
            <ta e="T18" id="Seg_346" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_347" s="T18">n-n:case1</ta>
            <ta e="T20" id="Seg_348" s="T19">n-n:case1</ta>
            <ta e="T21" id="Seg_349" s="T20">adj</ta>
            <ta e="T22" id="Seg_350" s="T21">n</ta>
            <ta e="T23" id="Seg_351" s="T22">v-n:ins-v:pn</ta>
            <ta e="T24" id="Seg_352" s="T23">pers</ta>
            <ta e="T25" id="Seg_353" s="T24">v-v:ins-v:pn</ta>
            <ta e="T26" id="Seg_354" s="T25">adv</ta>
            <ta e="T27" id="Seg_355" s="T26">pers</ta>
            <ta e="T28" id="Seg_356" s="T27">n-n:case1</ta>
            <ta e="T29" id="Seg_357" s="T28">n-n:case2</ta>
            <ta e="T30" id="Seg_358" s="T29">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_359" s="T0">pers</ta>
            <ta e="T2" id="Seg_360" s="T1">adv</ta>
            <ta e="T3" id="Seg_361" s="T2">v</ta>
            <ta e="T4" id="Seg_362" s="T3">n</ta>
            <ta e="T5" id="Seg_363" s="T4">pers</ta>
            <ta e="T6" id="Seg_364" s="T5">n</ta>
            <ta e="T7" id="Seg_365" s="T6">n</ta>
            <ta e="T8" id="Seg_366" s="T7">n</ta>
            <ta e="T9" id="Seg_367" s="T8">v</ta>
            <ta e="T10" id="Seg_368" s="T9">v</ta>
            <ta e="T11" id="Seg_369" s="T10">pers</ta>
            <ta e="T12" id="Seg_370" s="T11">n</ta>
            <ta e="T13" id="Seg_371" s="T12">v</ta>
            <ta e="T14" id="Seg_372" s="T13">n</ta>
            <ta e="T15" id="Seg_373" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_374" s="T15">v</ta>
            <ta e="T17" id="Seg_375" s="T16">n</ta>
            <ta e="T18" id="Seg_376" s="T17">v</ta>
            <ta e="T19" id="Seg_377" s="T18">n</ta>
            <ta e="T20" id="Seg_378" s="T19">n</ta>
            <ta e="T21" id="Seg_379" s="T20">adj</ta>
            <ta e="T22" id="Seg_380" s="T21">n</ta>
            <ta e="T23" id="Seg_381" s="T22">v</ta>
            <ta e="T24" id="Seg_382" s="T23">pers</ta>
            <ta e="T25" id="Seg_383" s="T24">v</ta>
            <ta e="T26" id="Seg_384" s="T25">adv</ta>
            <ta e="T27" id="Seg_385" s="T26">pers</ta>
            <ta e="T28" id="Seg_386" s="T27">n</ta>
            <ta e="T29" id="Seg_387" s="T28">n</ta>
            <ta e="T30" id="Seg_388" s="T29">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_389" s="T0">pro.h:S</ta>
            <ta e="T3" id="Seg_390" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_391" s="T4">pro.h:</ta>
            <ta e="T6" id="Seg_392" s="T5">Sv:pred</ta>
            <ta e="T7" id="Seg_393" s="T6">np:S</ta>
            <ta e="T8" id="Seg_394" s="T7">pro:O</ta>
            <ta e="T9" id="Seg_395" s="T8">v:pred</ta>
            <ta e="T10" id="Seg_396" s="T9">0.3:S v:pred</ta>
            <ta e="T11" id="Seg_397" s="T10">pro.h:S</ta>
            <ta e="T12" id="Seg_398" s="T11">np:O</ta>
            <ta e="T13" id="Seg_399" s="T12">v:pred</ta>
            <ta e="T14" id="Seg_400" s="T13">np:S</ta>
            <ta e="T16" id="Seg_401" s="T15">v:pred</ta>
            <ta e="T17" id="Seg_402" s="T16">pro.h:S</ta>
            <ta e="T18" id="Seg_403" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_404" s="T18">np:S</ta>
            <ta e="T22" id="Seg_405" s="T21">np:O</ta>
            <ta e="T23" id="Seg_406" s="T22">v:pred</ta>
            <ta e="T24" id="Seg_407" s="T23">pro.h:S</ta>
            <ta e="T25" id="Seg_408" s="T24">v:pred</ta>
            <ta e="T27" id="Seg_409" s="T26">pro.h:S</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_410" s="T0">pro.h:A</ta>
            <ta e="T2" id="Seg_411" s="T1">adv:Time</ta>
            <ta e="T4" id="Seg_412" s="T3">np:G</ta>
            <ta e="T5" id="Seg_413" s="T4">pro.h:A</ta>
            <ta e="T7" id="Seg_414" s="T6">np:A</ta>
            <ta e="T8" id="Seg_415" s="T7">np:Th</ta>
            <ta e="T10" id="Seg_416" s="T9">0.3:A</ta>
            <ta e="T11" id="Seg_417" s="T10">pro.h:A</ta>
            <ta e="T12" id="Seg_418" s="T11">np:Th</ta>
            <ta e="T14" id="Seg_419" s="T13">np:Th</ta>
            <ta e="T17" id="Seg_420" s="T16">0.3.h:A</ta>
            <ta e="T19" id="Seg_421" s="T18">np:A</ta>
            <ta e="T20" id="Seg_422" s="T19">np:Poss</ta>
            <ta e="T21" id="Seg_423" s="T20">np:Th</ta>
            <ta e="T24" id="Seg_424" s="T23">pro.h:A</ta>
            <ta e="T26" id="Seg_425" s="T25">np:G</ta>
            <ta e="T27" id="Seg_426" s="T26">pro.h:A</ta>
            <ta e="T28" id="Seg_427" s="T27">pp:G</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_428" s="T0">Я осенью пошла в тайгу.</ta>
            <ta e="T6" id="Seg_429" s="T4">Я белковала.</ta>
            <ta e="T10" id="Seg_430" s="T6">Собака белку нашла, лает.</ta>
            <ta e="T16" id="Seg_431" s="T10">Я в белку выстрелила, но белка не упала.</ta>
            <ta e="T18" id="Seg_432" s="T16">Белку следила.</ta>
            <ta e="T23" id="Seg_433" s="T18">Собака берлогу зверя нашла.</ta>
            <ta e="T26" id="Seg_434" s="T23">Я пошла туда.</ta>
            <ta e="T30" id="Seg_435" s="T26">Она в берлогу лает.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_436" s="T0">I went to the Taiga in the autumn.</ta>
            <ta e="T6" id="Seg_437" s="T4">I hunted squirrels.</ta>
            <ta e="T10" id="Seg_438" s="T6">The dog found a squirrel, it barks.</ta>
            <ta e="T16" id="Seg_439" s="T10">I shot a squirrel, but the squirrel didn't fall.</ta>
            <ta e="T18" id="Seg_440" s="T16">I followed the squirrel.</ta>
            <ta e="T23" id="Seg_441" s="T18">The dog found the cave of the animal.</ta>
            <ta e="T26" id="Seg_442" s="T23">I went there.</ta>
            <ta e="T30" id="Seg_443" s="T26">The dog is barking in the cave.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_444" s="T0">Ich bin im Herbst in die Taiga gegangen.</ta>
            <ta e="T6" id="Seg_445" s="T4">Ich jagte auf Eichhörnchen.</ta>
            <ta e="T10" id="Seg_446" s="T6">Der Hund fand ein Eichhörnchen, er bellt.</ta>
            <ta e="T16" id="Seg_447" s="T10">Ich schoss auf das Eichhörnchen, aber das Eichhörnchen fiel nicht herunter.</ta>
            <ta e="T18" id="Seg_448" s="T16">Ich folgte dem Eichhörnchen.</ta>
            <ta e="T23" id="Seg_449" s="T18">Der Hund fand die Höhle des Tieres.</ta>
            <ta e="T26" id="Seg_450" s="T23">Ich ging dorthin.</ta>
            <ta e="T30" id="Seg_451" s="T26">Er bellt in die Höhle.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_452" s="T0">я осенью пошла в тайгу</ta>
            <ta e="T6" id="Seg_453" s="T4">я белковала</ta>
            <ta e="T10" id="Seg_454" s="T6">собака белку нашла лает</ta>
            <ta e="T16" id="Seg_455" s="T10">я в белку выстрелила белка не упала</ta>
            <ta e="T18" id="Seg_456" s="T16">белку караулила (следила)</ta>
            <ta e="T23" id="Seg_457" s="T18">собака зверю берлогу нашла</ta>
            <ta e="T26" id="Seg_458" s="T23">Я пошла туда.</ta>
            <ta e="T30" id="Seg_459" s="T26">она в берлогу (туда) лает</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
