<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID61831314-2B9D-AAFF-F71E-3E298A18BE2B">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">nar\TAV_1965_Bear_nar\TAV_1965_Bear_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">60</ud-information>
            <ud-information attribute-name="# HIAT:w">43</ud-information>
            <ud-information attribute-name="# e">44</ud-information>
            <ud-information attribute-name="# HIAT:u">14</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="TAV">
            <abbreviation>TAV</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T44" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="TAV"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T43" id="Seg_0" n="sc" s="T0">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Mat</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">tɛnɨmam</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_11" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">Mat</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">onak</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">ilak</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_23" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">Šetɨrqöt</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">qorqot</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">wätɨp</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">qokkap</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_38" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">Qänkak</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">kanaknasä</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_47" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_49" n="HIAT:w" s="T11">Kanakmɨ</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_52" n="HIAT:w" s="T12">nʼoːtɨkkɨt</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T44">məːwättoːmɨntə</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_59" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_61" n="HIAT:w" s="T14">Mat</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_64" n="HIAT:w" s="T15">namɨp</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_67" n="HIAT:w" s="T16">nʼoːtɨkkap</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_71" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_73" n="HIAT:w" s="T17">Kanaŋmɨ</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_76" n="HIAT:w" s="T18">uːkət</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_79" n="HIAT:w" s="T19">mitɨkkɨtɨ</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_82" n="HIAT:w" s="T20">qorqop</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_86" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_88" n="HIAT:w" s="T21">Muːttɨlkkətɨ</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_92" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_94" n="HIAT:w" s="T22">Mat</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_97" n="HIAT:w" s="T23">tüːkkak</ts>
                  <nts id="Seg_98" n="HIAT:ip">,</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_101" n="HIAT:w" s="T24">kəntalkap</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_105" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_107" n="HIAT:w" s="T25">Čʼattɨkkap</ts>
                  <nts id="Seg_108" n="HIAT:ip">,</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_111" n="HIAT:w" s="T26">nɨːnɨ</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_114" n="HIAT:w" s="T27">paktɨna</ts>
                  <nts id="Seg_115" n="HIAT:ip">.</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_118" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_120" n="HIAT:w" s="T28">Nɨːn</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_123" n="HIAT:w" s="T29">ɛj</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_126" n="HIAT:w" s="T30">očʼik</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_129" n="HIAT:w" s="T31">čʼattɨkkap</ts>
                  <nts id="Seg_130" n="HIAT:ip">.</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_133" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_135" n="HIAT:w" s="T32">Alʼčʼikka</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_138" n="HIAT:w" s="T33">mertɨ</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_141" n="HIAT:w" s="T34">nat</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_145" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_147" n="HIAT:w" s="T35">Qula</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_150" n="HIAT:w" s="T36">mertɨ</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_153" n="HIAT:w" s="T37">alʼčʼikka</ts>
                  <nts id="Seg_154" n="HIAT:ip">.</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_157" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_159" n="HIAT:w" s="T38">Nɨːnɨ</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_162" n="HIAT:w" s="T39">kırəkkap</ts>
                  <nts id="Seg_163" n="HIAT:ip">,</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_166" n="HIAT:w" s="T40">moqonä</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_169" n="HIAT:w" s="T41">tattɨkkap</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_172" n="HIAT:w" s="T42">qumiqak</ts>
                  <nts id="Seg_173" n="HIAT:ip">.</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T43" id="Seg_175" n="sc" s="T0">
               <ts e="T1" id="Seg_177" n="e" s="T0">Mat </ts>
               <ts e="T2" id="Seg_179" n="e" s="T1">tɛnɨmam. </ts>
               <ts e="T3" id="Seg_181" n="e" s="T2">Mat </ts>
               <ts e="T4" id="Seg_183" n="e" s="T3">onak </ts>
               <ts e="T5" id="Seg_185" n="e" s="T4">ilak. </ts>
               <ts e="T6" id="Seg_187" n="e" s="T5">Šetɨrqöt </ts>
               <ts e="T7" id="Seg_189" n="e" s="T6">qorqot </ts>
               <ts e="T8" id="Seg_191" n="e" s="T7">wätɨp </ts>
               <ts e="T9" id="Seg_193" n="e" s="T8">qokkap. </ts>
               <ts e="T10" id="Seg_195" n="e" s="T9">Qänkak </ts>
               <ts e="T11" id="Seg_197" n="e" s="T10">kanaknasä. </ts>
               <ts e="T12" id="Seg_199" n="e" s="T11">Kanakmɨ </ts>
               <ts e="T44" id="Seg_201" n="e" s="T12">nʼoːtɨkkɨt </ts>
               <ts e="T13" id="Seg_203" n="e" s="T44">məː</ts>
               <ts e="T14" id="Seg_205" n="e" s="T13">wättoːmɨntə. </ts>
               <ts e="T15" id="Seg_207" n="e" s="T14">Mat </ts>
               <ts e="T16" id="Seg_209" n="e" s="T15">namɨp </ts>
               <ts e="T17" id="Seg_211" n="e" s="T16">nʼoːtɨkkap. </ts>
               <ts e="T18" id="Seg_213" n="e" s="T17">Kanaŋmɨ </ts>
               <ts e="T19" id="Seg_215" n="e" s="T18">uːkət </ts>
               <ts e="T20" id="Seg_217" n="e" s="T19">mitɨkkɨtɨ </ts>
               <ts e="T21" id="Seg_219" n="e" s="T20">qorqop. </ts>
               <ts e="T22" id="Seg_221" n="e" s="T21">Muːttɨlkkətɨ. </ts>
               <ts e="T23" id="Seg_223" n="e" s="T22">Mat </ts>
               <ts e="T24" id="Seg_225" n="e" s="T23">tüːkkak, </ts>
               <ts e="T25" id="Seg_227" n="e" s="T24">kəntalkap. </ts>
               <ts e="T26" id="Seg_229" n="e" s="T25">Čʼattɨkkap, </ts>
               <ts e="T27" id="Seg_231" n="e" s="T26">nɨːnɨ </ts>
               <ts e="T28" id="Seg_233" n="e" s="T27">paktɨna. </ts>
               <ts e="T29" id="Seg_235" n="e" s="T28">Nɨːn </ts>
               <ts e="T30" id="Seg_237" n="e" s="T29">ɛj </ts>
               <ts e="T31" id="Seg_239" n="e" s="T30">očʼik </ts>
               <ts e="T32" id="Seg_241" n="e" s="T31">čʼattɨkkap. </ts>
               <ts e="T33" id="Seg_243" n="e" s="T32">Alʼčʼikka </ts>
               <ts e="T34" id="Seg_245" n="e" s="T33">mertɨ </ts>
               <ts e="T35" id="Seg_247" n="e" s="T34">nat. </ts>
               <ts e="T36" id="Seg_249" n="e" s="T35">Qula </ts>
               <ts e="T37" id="Seg_251" n="e" s="T36">mertɨ </ts>
               <ts e="T38" id="Seg_253" n="e" s="T37">alʼčʼikka. </ts>
               <ts e="T39" id="Seg_255" n="e" s="T38">Nɨːnɨ </ts>
               <ts e="T40" id="Seg_257" n="e" s="T39">kırəkkap, </ts>
               <ts e="T41" id="Seg_259" n="e" s="T40">moqonä </ts>
               <ts e="T42" id="Seg_261" n="e" s="T41">tattɨkkap </ts>
               <ts e="T43" id="Seg_263" n="e" s="T42">qumiqak. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2" id="Seg_264" s="T0">TAV_1965_Bear_nar.001 (001.001)</ta>
            <ta e="T5" id="Seg_265" s="T2">TAV_1965_Bear_nar.002 (001.002)</ta>
            <ta e="T9" id="Seg_266" s="T5">TAV_1965_Bear_nar.003 (001.003)</ta>
            <ta e="T11" id="Seg_267" s="T9">TAV_1965_Bear_nar.004 (001.004)</ta>
            <ta e="T14" id="Seg_268" s="T11">TAV_1965_Bear_nar.005 (001.005)</ta>
            <ta e="T17" id="Seg_269" s="T14">TAV_1965_Bear_nar.006 (001.006)</ta>
            <ta e="T21" id="Seg_270" s="T17">TAV_1965_Bear_nar.007 (001.007)</ta>
            <ta e="T22" id="Seg_271" s="T21">TAV_1965_Bear_nar.008 (001.008)</ta>
            <ta e="T25" id="Seg_272" s="T22">TAV_1965_Bear_nar.009 (001.009)</ta>
            <ta e="T28" id="Seg_273" s="T25">TAV_1965_Bear_nar.010 (001.010)</ta>
            <ta e="T32" id="Seg_274" s="T28">TAV_1965_Bear_nar.011 (001.011)</ta>
            <ta e="T35" id="Seg_275" s="T32">TAV_1965_Bear_nar.012 (001.012)</ta>
            <ta e="T38" id="Seg_276" s="T35">TAV_1965_Bear_nar.013 (001.013)</ta>
            <ta e="T43" id="Seg_277" s="T38">TAV_1965_Bear_nar.014 (001.014)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T2" id="Seg_278" s="T0">мат ′тӓ̄нымам.</ta>
            <ta e="T5" id="Seg_279" s="T2">мат ′онак ′иllак</ta>
            <ta e="T9" id="Seg_280" s="T5">′шʼетыркӧт kорɣот wӓтып ′коккап.</ta>
            <ta e="T11" id="Seg_281" s="T9">′kӓнкак ′канакна′сӓ.</ta>
            <ta e="T14" id="Seg_282" s="T11">ка′наkмы нӧтыкытмъ вӓ′ттомынтъ.</ta>
            <ta e="T17" id="Seg_283" s="T14">мат ′намып ′нʼӧтыккап.</ta>
            <ta e="T21" id="Seg_284" s="T17">ка′наңмы ӯкът митык[k]ыты ′корɣоп.</ta>
            <ta e="T22" id="Seg_285" s="T21">′мӯтыlкъ′ты.</ta>
            <ta e="T25" id="Seg_286" s="T22">мат ′тӱ̄ккак ′къ̊нтаlкап.</ta>
            <ta e="T28" id="Seg_287" s="T25">′чаттыкап ′ныны ′паkтынна.</ta>
            <ta e="T32" id="Seg_288" s="T28">ны′нӓй ′очик ′чаттыкап.</ta>
            <ta e="T35" id="Seg_289" s="T32">′аlчика ′мертынат.</ta>
            <ta e="T38" id="Seg_290" s="T35">kӯlа мерты ′аlчика.</ta>
            <ta e="T43" id="Seg_291" s="T38">ныны ′кирък‵кап, ′моkонӓ ′таттыkап kу′микак.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T2" id="Seg_292" s="T0">mat täːnɨmam.</ta>
            <ta e="T5" id="Seg_293" s="T2">mat onak ilʼlʼak </ta>
            <ta e="T9" id="Seg_294" s="T5">šʼetɨrköt qorqot wätɨp kokkap.</ta>
            <ta e="T11" id="Seg_295" s="T9">qänkak kanaknasä.</ta>
            <ta e="T14" id="Seg_296" s="T11">kanaqmɨ nötɨkɨtmə vättomɨntə.</ta>
            <ta e="T17" id="Seg_297" s="T14">mat namɨp nʼötɨkkap.</ta>
            <ta e="T21" id="Seg_298" s="T17">kanaŋmɨ uːkət mitɨk[q]ɨtɨ korqop.</ta>
            <ta e="T22" id="Seg_299" s="T21">muːtɨlʼkətɨ.</ta>
            <ta e="T25" id="Seg_300" s="T22">mat tüːkkak kəntalʼkap.</ta>
            <ta e="T28" id="Seg_301" s="T25">čʼattɨkap nɨnɨ paqtɨnna.</ta>
            <ta e="T32" id="Seg_302" s="T28">nɨnäj očʼik čʼattɨkap.</ta>
            <ta e="T35" id="Seg_303" s="T32">alʼčʼika mertɨnat.</ta>
            <ta e="T38" id="Seg_304" s="T35">quːlʼa mertɨ alʼčʼika.</ta>
            <ta e="T43" id="Seg_305" s="T38">nɨnɨ kirəkkap, moqonä tattɨqap qumikak.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2" id="Seg_306" s="T0">Mat tɛnɨmam. </ta>
            <ta e="T5" id="Seg_307" s="T2">Mat onak ilak. </ta>
            <ta e="T9" id="Seg_308" s="T5">Šetɨrqöt qorqot wätɨp qokkap. </ta>
            <ta e="T11" id="Seg_309" s="T9">Qänkak kanaknasä. </ta>
            <ta e="T14" id="Seg_310" s="T11">Kanakmɨ nʼoːtɨkkɨt məː wättoːmɨntə. </ta>
            <ta e="T17" id="Seg_311" s="T14">Mat namɨp nʼoːtɨkkap. </ta>
            <ta e="T21" id="Seg_312" s="T17">Kanaŋmɨ uːkət mitɨkkɨtɨ qorqop. </ta>
            <ta e="T22" id="Seg_313" s="T21">Muːttɨlkkətɨ. </ta>
            <ta e="T25" id="Seg_314" s="T22">Mat tüːkkak, kəntalkap. </ta>
            <ta e="T28" id="Seg_315" s="T25">Čʼattɨkkap, nɨːnɨ paktɨna. </ta>
            <ta e="T32" id="Seg_316" s="T28">Nɨːn ɛj očʼik čʼattɨkkap. </ta>
            <ta e="T35" id="Seg_317" s="T32">Alʼčʼikka mertɨ nat. </ta>
            <ta e="T38" id="Seg_318" s="T35">Qula mertɨ alʼčʼikka. </ta>
            <ta e="T43" id="Seg_319" s="T38">Nɨːnɨ kırəkkap, moqonä tattɨkkap qumiqak. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_320" s="T0">mat</ta>
            <ta e="T2" id="Seg_321" s="T1">tɛnɨma-m</ta>
            <ta e="T3" id="Seg_322" s="T2">mat</ta>
            <ta e="T4" id="Seg_323" s="T3">onak</ta>
            <ta e="T5" id="Seg_324" s="T4">ila-k</ta>
            <ta e="T6" id="Seg_325" s="T5">šetɨr-qöt</ta>
            <ta e="T7" id="Seg_326" s="T6">qorqo-t</ta>
            <ta e="T8" id="Seg_327" s="T7">wätɨ-p</ta>
            <ta e="T9" id="Seg_328" s="T8">qo-kka-p</ta>
            <ta e="T10" id="Seg_329" s="T9">qän-ka-k</ta>
            <ta e="T11" id="Seg_330" s="T10">kanak-na-sä</ta>
            <ta e="T12" id="Seg_331" s="T11">kanak-mɨ</ta>
            <ta e="T44" id="Seg_332" s="T12">nʼoː-tɨ-kkɨ-t</ta>
            <ta e="T13" id="Seg_333" s="T44">məː</ta>
            <ta e="T14" id="Seg_334" s="T13">wättoː-mɨn-tə</ta>
            <ta e="T15" id="Seg_335" s="T14">mat</ta>
            <ta e="T16" id="Seg_336" s="T15">namɨ-p</ta>
            <ta e="T17" id="Seg_337" s="T16">nʼoː-tɨ-kka-p</ta>
            <ta e="T18" id="Seg_338" s="T17">kanaŋ-mɨ</ta>
            <ta e="T19" id="Seg_339" s="T18">uːkə-t</ta>
            <ta e="T20" id="Seg_340" s="T19">mitɨ-kkɨ-tɨ</ta>
            <ta e="T21" id="Seg_341" s="T20">qorqo-p</ta>
            <ta e="T22" id="Seg_342" s="T21">muːtt-ɨl-kkə-tɨ</ta>
            <ta e="T23" id="Seg_343" s="T22">mat</ta>
            <ta e="T24" id="Seg_344" s="T23">tüː-kka-k</ta>
            <ta e="T25" id="Seg_345" s="T24">kəntal-ka-p</ta>
            <ta e="T26" id="Seg_346" s="T25">čʼattɨ-kka-p</ta>
            <ta e="T27" id="Seg_347" s="T26">nɨːnɨ</ta>
            <ta e="T28" id="Seg_348" s="T27">paktɨ-na</ta>
            <ta e="T29" id="Seg_349" s="T28">nɨːn</ta>
            <ta e="T30" id="Seg_350" s="T29">ɛj</ta>
            <ta e="T31" id="Seg_351" s="T30">očʼik</ta>
            <ta e="T32" id="Seg_352" s="T31">čʼattɨ-kka-p</ta>
            <ta e="T33" id="Seg_353" s="T32">alʼčʼi-kka</ta>
            <ta e="T34" id="Seg_354" s="T33">mertɨ</ta>
            <ta e="T35" id="Seg_355" s="T34">na-t</ta>
            <ta e="T36" id="Seg_356" s="T35">qu-la</ta>
            <ta e="T37" id="Seg_357" s="T36">mertɨ</ta>
            <ta e="T38" id="Seg_358" s="T37">alʼčʼi-kka</ta>
            <ta e="T39" id="Seg_359" s="T38">nɨːnɨ</ta>
            <ta e="T40" id="Seg_360" s="T39">kırə-kka-p</ta>
            <ta e="T41" id="Seg_361" s="T40">moqonä</ta>
            <ta e="T42" id="Seg_362" s="T41">tattɨ-kka-p</ta>
            <ta e="T43" id="Seg_363" s="T42">qum-i-qak</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_364" s="T0">man</ta>
            <ta e="T2" id="Seg_365" s="T1">tɛnɨmɨ-m</ta>
            <ta e="T3" id="Seg_366" s="T2">man</ta>
            <ta e="T4" id="Seg_367" s="T3">onäk</ta>
            <ta e="T5" id="Seg_368" s="T4">ilɨ-k</ta>
            <ta e="T6" id="Seg_369" s="T5">šettɨr-qɨn</ta>
            <ta e="T7" id="Seg_370" s="T6">qorqɨ-n</ta>
            <ta e="T8" id="Seg_371" s="T7">wəttɨ-m</ta>
            <ta e="T9" id="Seg_372" s="T8">qo-kkɨ-m</ta>
            <ta e="T10" id="Seg_373" s="T9">qən-kkɨ-k</ta>
            <ta e="T11" id="Seg_374" s="T10">kanaŋ-nɨ-sä</ta>
            <ta e="T12" id="Seg_375" s="T11">kanaŋ-mɨ</ta>
            <ta e="T44" id="Seg_376" s="T12">nʼoː-tɨ-kkɨ-tɨ</ta>
            <ta e="T13" id="Seg_377" s="T44">meː</ta>
            <ta e="T14" id="Seg_378" s="T13">wəttɨ-mɨn-ntɨ</ta>
            <ta e="T15" id="Seg_379" s="T14">man</ta>
            <ta e="T16" id="Seg_380" s="T15">na-m</ta>
            <ta e="T17" id="Seg_381" s="T16">nʼoː-tɨ-kkɨ-m</ta>
            <ta e="T18" id="Seg_382" s="T17">kanaŋ-mɨ</ta>
            <ta e="T19" id="Seg_383" s="T18">uːkɨ-k</ta>
            <ta e="T20" id="Seg_384" s="T19">mitɨ-kkɨ-tɨ</ta>
            <ta e="T21" id="Seg_385" s="T20">qorqɨ-m</ta>
            <ta e="T22" id="Seg_386" s="T21">muːt-ɔːl-kkɨ-tɨ</ta>
            <ta e="T23" id="Seg_387" s="T22">man</ta>
            <ta e="T24" id="Seg_388" s="T23">tü-kkɨ-k</ta>
            <ta e="T25" id="Seg_389" s="T24">kəntal-kkɨ-m</ta>
            <ta e="T26" id="Seg_390" s="T25">čʼattɨ-kkɨ-m</ta>
            <ta e="T27" id="Seg_391" s="T26">nɨːnɨ</ta>
            <ta e="T28" id="Seg_392" s="T27">paktɨ-ŋɨ</ta>
            <ta e="T29" id="Seg_393" s="T28">nɨːnɨ</ta>
            <ta e="T30" id="Seg_394" s="T29">aj</ta>
            <ta e="T31" id="Seg_395" s="T30">očʼɨŋ</ta>
            <ta e="T32" id="Seg_396" s="T31">čʼattɨ-kkɨ-m</ta>
            <ta e="T33" id="Seg_397" s="T32">alʼčʼɨ-kkɨ</ta>
            <ta e="T34" id="Seg_398" s="T33">mertɨ</ta>
            <ta e="T35" id="Seg_399" s="T34">na-tɨ</ta>
            <ta e="T36" id="Seg_400" s="T35">qu-lä</ta>
            <ta e="T37" id="Seg_401" s="T36">mertɨ</ta>
            <ta e="T38" id="Seg_402" s="T37">alʼčʼɨ-kkɨ</ta>
            <ta e="T39" id="Seg_403" s="T38">nɨːnɨ</ta>
            <ta e="T40" id="Seg_404" s="T39">kırɨ-kkɨ-m</ta>
            <ta e="T41" id="Seg_405" s="T40">moqɨnä</ta>
            <ta e="T42" id="Seg_406" s="T41">taːtɨ-kkɨ-m</ta>
            <ta e="T43" id="Seg_407" s="T42">qum-ɨ-qäk</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_408" s="T0">I.NOM</ta>
            <ta e="T2" id="Seg_409" s="T1">know-1SG.O</ta>
            <ta e="T3" id="Seg_410" s="T2">I.NOM</ta>
            <ta e="T4" id="Seg_411" s="T3">myself.[NOM]</ta>
            <ta e="T5" id="Seg_412" s="T4">live-1SG.S</ta>
            <ta e="T6" id="Seg_413" s="T5">spring-ADV.LOC</ta>
            <ta e="T7" id="Seg_414" s="T6">bear-GEN</ta>
            <ta e="T8" id="Seg_415" s="T7">road-ACC</ta>
            <ta e="T9" id="Seg_416" s="T8">find-DUR-1SG.O</ta>
            <ta e="T10" id="Seg_417" s="T9">leave-DUR-1SG.S</ta>
            <ta e="T11" id="Seg_418" s="T10">dog-OBL.1SG-COM</ta>
            <ta e="T12" id="Seg_419" s="T11">dog.[NOM]-1SG</ta>
            <ta e="T44" id="Seg_420" s="T12">catch.up-TR-DUR-3SG.O</ta>
            <ta e="T13" id="Seg_421" s="T44">we.DU.NOM</ta>
            <ta e="T14" id="Seg_422" s="T13">road-PROL-OBL.3SG</ta>
            <ta e="T15" id="Seg_423" s="T14">I.NOM</ta>
            <ta e="T16" id="Seg_424" s="T15">this-ACC</ta>
            <ta e="T17" id="Seg_425" s="T16">catch.up-TR-DUR-1SG.O</ta>
            <ta e="T18" id="Seg_426" s="T17">dog.[NOM]-1SG</ta>
            <ta e="T19" id="Seg_427" s="T18">front.part-ADVZ</ta>
            <ta e="T20" id="Seg_428" s="T19">catch.up-DUR-3SG.O</ta>
            <ta e="T21" id="Seg_429" s="T20">bear-ACC</ta>
            <ta e="T22" id="Seg_430" s="T21">bark-MOM-DUR-3SG.O</ta>
            <ta e="T23" id="Seg_431" s="T22">I.NOM</ta>
            <ta e="T24" id="Seg_432" s="T23">come-DUR-1SG.S</ta>
            <ta e="T25" id="Seg_433" s="T24">steal.up-DUR-1SG.O</ta>
            <ta e="T26" id="Seg_434" s="T25">shoot-DUR-1SG.O</ta>
            <ta e="T27" id="Seg_435" s="T26">then</ta>
            <ta e="T28" id="Seg_436" s="T27">run-CO.[3SG.S]</ta>
            <ta e="T29" id="Seg_437" s="T28">then</ta>
            <ta e="T30" id="Seg_438" s="T29">again</ta>
            <ta e="T31" id="Seg_439" s="T30">again</ta>
            <ta e="T32" id="Seg_440" s="T31">shoot-DUR-1SG.O</ta>
            <ta e="T33" id="Seg_441" s="T32">fall-DUR.[3SG.S]</ta>
            <ta e="T34" id="Seg_442" s="T33">dead</ta>
            <ta e="T35" id="Seg_443" s="T34">this-3SG</ta>
            <ta e="T36" id="Seg_444" s="T35">die-CVB</ta>
            <ta e="T37" id="Seg_445" s="T36">dead</ta>
            <ta e="T38" id="Seg_446" s="T37">fall-DUR.[3SG.S]</ta>
            <ta e="T39" id="Seg_447" s="T38">then</ta>
            <ta e="T40" id="Seg_448" s="T39">skin-DUR-1SG.O</ta>
            <ta e="T41" id="Seg_449" s="T40">home</ta>
            <ta e="T42" id="Seg_450" s="T41">bring-DUR-1SG.O</ta>
            <ta e="T43" id="Seg_451" s="T42">human.being-EP-1SG.ILL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_452" s="T0">я.NOM</ta>
            <ta e="T2" id="Seg_453" s="T1">знать-1SG.O</ta>
            <ta e="T3" id="Seg_454" s="T2">я.NOM</ta>
            <ta e="T4" id="Seg_455" s="T3">я.сам.[NOM]</ta>
            <ta e="T5" id="Seg_456" s="T4">жить-1SG.S</ta>
            <ta e="T6" id="Seg_457" s="T5">весна-ADV.LOC</ta>
            <ta e="T7" id="Seg_458" s="T6">медведь-GEN</ta>
            <ta e="T8" id="Seg_459" s="T7">дорога-ACC</ta>
            <ta e="T9" id="Seg_460" s="T8">находить-DUR-1SG.O</ta>
            <ta e="T10" id="Seg_461" s="T9">отправиться-DUR-1SG.S</ta>
            <ta e="T11" id="Seg_462" s="T10">собака-OBL.1SG-COM</ta>
            <ta e="T12" id="Seg_463" s="T11">собака.[NOM]-1SG</ta>
            <ta e="T44" id="Seg_464" s="T12">догонять-TR-DUR-3SG.O</ta>
            <ta e="T13" id="Seg_465" s="T44">мы.DU.NOM</ta>
            <ta e="T14" id="Seg_466" s="T13">дорога-PROL-OBL.3SG</ta>
            <ta e="T15" id="Seg_467" s="T14">я.NOM</ta>
            <ta e="T16" id="Seg_468" s="T15">это-ACC</ta>
            <ta e="T17" id="Seg_469" s="T16">догонять-TR-DUR-1SG.O</ta>
            <ta e="T18" id="Seg_470" s="T17">собака.[NOM]-1SG</ta>
            <ta e="T19" id="Seg_471" s="T18">перед-ADVZ</ta>
            <ta e="T20" id="Seg_472" s="T19">догнать-DUR-3SG.O</ta>
            <ta e="T21" id="Seg_473" s="T20">медведь-ACC</ta>
            <ta e="T22" id="Seg_474" s="T21">лаять-MOM-DUR-3SG.O</ta>
            <ta e="T23" id="Seg_475" s="T22">я.NOM</ta>
            <ta e="T24" id="Seg_476" s="T23">прийти-DUR-1SG.S</ta>
            <ta e="T25" id="Seg_477" s="T24">подкрасться-DUR-1SG.O</ta>
            <ta e="T26" id="Seg_478" s="T25">стрелять-DUR-1SG.O</ta>
            <ta e="T27" id="Seg_479" s="T26">потом</ta>
            <ta e="T28" id="Seg_480" s="T27">побежать-CO.[3SG.S]</ta>
            <ta e="T29" id="Seg_481" s="T28">потом</ta>
            <ta e="T30" id="Seg_482" s="T29">опять</ta>
            <ta e="T31" id="Seg_483" s="T30">снова</ta>
            <ta e="T32" id="Seg_484" s="T31">стрелять-DUR-1SG.O</ta>
            <ta e="T33" id="Seg_485" s="T32">упасть-DUR.[3SG.S]</ta>
            <ta e="T34" id="Seg_486" s="T33">мёртвый</ta>
            <ta e="T35" id="Seg_487" s="T34">это-3SG</ta>
            <ta e="T36" id="Seg_488" s="T35">умереть-CVB</ta>
            <ta e="T37" id="Seg_489" s="T36">мёртвый</ta>
            <ta e="T38" id="Seg_490" s="T37">упасть-DUR.[3SG.S]</ta>
            <ta e="T39" id="Seg_491" s="T38">потом</ta>
            <ta e="T40" id="Seg_492" s="T39">ободрать-DUR-1SG.O</ta>
            <ta e="T41" id="Seg_493" s="T40">домой</ta>
            <ta e="T42" id="Seg_494" s="T41">принести-DUR-1SG.O</ta>
            <ta e="T43" id="Seg_495" s="T42">человек-EP-1SG.ILL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_496" s="T0">pers</ta>
            <ta e="T2" id="Seg_497" s="T1">v-v:pn</ta>
            <ta e="T3" id="Seg_498" s="T2">pers</ta>
            <ta e="T4" id="Seg_499" s="T3">pro-n:case3</ta>
            <ta e="T5" id="Seg_500" s="T4">v-v:pn</ta>
            <ta e="T6" id="Seg_501" s="T5">n-n&gt;adv</ta>
            <ta e="T7" id="Seg_502" s="T6">n-n:case3</ta>
            <ta e="T8" id="Seg_503" s="T7">n-n:case3</ta>
            <ta e="T9" id="Seg_504" s="T8">v-v&gt;v-v:pn</ta>
            <ta e="T10" id="Seg_505" s="T9">v-v&gt;v-v:pn</ta>
            <ta e="T11" id="Seg_506" s="T10">n-n:obl.poss-n:case2</ta>
            <ta e="T12" id="Seg_507" s="T11">n-n:case1-n:poss</ta>
            <ta e="T44" id="Seg_508" s="T12">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T13" id="Seg_509" s="T44">pers</ta>
            <ta e="T14" id="Seg_510" s="T13">n-n:case2-n:obl.poss</ta>
            <ta e="T15" id="Seg_511" s="T14">pers</ta>
            <ta e="T16" id="Seg_512" s="T15">pro-n:case3</ta>
            <ta e="T17" id="Seg_513" s="T16">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T18" id="Seg_514" s="T17">n-n:case1-n:poss</ta>
            <ta e="T19" id="Seg_515" s="T18">n-n&gt;adv</ta>
            <ta e="T20" id="Seg_516" s="T19">v-v&gt;v-v:pn</ta>
            <ta e="T21" id="Seg_517" s="T20">n-n:case3</ta>
            <ta e="T22" id="Seg_518" s="T21">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T23" id="Seg_519" s="T22">pers</ta>
            <ta e="T24" id="Seg_520" s="T23">v-v&gt;v-v:pn</ta>
            <ta e="T25" id="Seg_521" s="T24">v-v&gt;v-v:pn</ta>
            <ta e="T26" id="Seg_522" s="T25">v-v&gt;v-v:pn</ta>
            <ta e="T27" id="Seg_523" s="T26">adv</ta>
            <ta e="T28" id="Seg_524" s="T27">v-v:ins-v:pn</ta>
            <ta e="T29" id="Seg_525" s="T28">adv</ta>
            <ta e="T30" id="Seg_526" s="T29">adv</ta>
            <ta e="T31" id="Seg_527" s="T30">adv</ta>
            <ta e="T32" id="Seg_528" s="T31">v-v&gt;v-v:pn</ta>
            <ta e="T33" id="Seg_529" s="T32">v-v&gt;v-v:pn</ta>
            <ta e="T34" id="Seg_530" s="T33">adj</ta>
            <ta e="T35" id="Seg_531" s="T34">pro-n:poss</ta>
            <ta e="T36" id="Seg_532" s="T35">v-v&gt;adv</ta>
            <ta e="T37" id="Seg_533" s="T36">adj</ta>
            <ta e="T38" id="Seg_534" s="T37">v-v&gt;v-v:pn</ta>
            <ta e="T39" id="Seg_535" s="T38">adv</ta>
            <ta e="T40" id="Seg_536" s="T39">v-v&gt;v-v:pn</ta>
            <ta e="T41" id="Seg_537" s="T40">adv</ta>
            <ta e="T42" id="Seg_538" s="T41">v-v&gt;v-v:pn</ta>
            <ta e="T43" id="Seg_539" s="T42">n-n:(ins)-n:poss-case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_540" s="T0">pers</ta>
            <ta e="T2" id="Seg_541" s="T1">v</ta>
            <ta e="T3" id="Seg_542" s="T2">pers</ta>
            <ta e="T4" id="Seg_543" s="T3">pro</ta>
            <ta e="T5" id="Seg_544" s="T4">v</ta>
            <ta e="T6" id="Seg_545" s="T5">adv</ta>
            <ta e="T7" id="Seg_546" s="T6">n</ta>
            <ta e="T8" id="Seg_547" s="T7">n</ta>
            <ta e="T9" id="Seg_548" s="T8">v</ta>
            <ta e="T10" id="Seg_549" s="T9">v</ta>
            <ta e="T11" id="Seg_550" s="T10">n</ta>
            <ta e="T12" id="Seg_551" s="T11">n</ta>
            <ta e="T44" id="Seg_552" s="T12">v</ta>
            <ta e="T13" id="Seg_553" s="T44">pers</ta>
            <ta e="T14" id="Seg_554" s="T13">n</ta>
            <ta e="T15" id="Seg_555" s="T14">pers</ta>
            <ta e="T16" id="Seg_556" s="T15">pro</ta>
            <ta e="T17" id="Seg_557" s="T16">v</ta>
            <ta e="T18" id="Seg_558" s="T17">n</ta>
            <ta e="T19" id="Seg_559" s="T18">adv</ta>
            <ta e="T20" id="Seg_560" s="T19">v</ta>
            <ta e="T21" id="Seg_561" s="T20">n</ta>
            <ta e="T22" id="Seg_562" s="T21">v</ta>
            <ta e="T23" id="Seg_563" s="T22">pers</ta>
            <ta e="T24" id="Seg_564" s="T23">v</ta>
            <ta e="T25" id="Seg_565" s="T24">v</ta>
            <ta e="T26" id="Seg_566" s="T25">v</ta>
            <ta e="T27" id="Seg_567" s="T26">adv</ta>
            <ta e="T28" id="Seg_568" s="T27">v</ta>
            <ta e="T29" id="Seg_569" s="T28">adv</ta>
            <ta e="T30" id="Seg_570" s="T29">adv</ta>
            <ta e="T31" id="Seg_571" s="T30">adv</ta>
            <ta e="T32" id="Seg_572" s="T31">v</ta>
            <ta e="T33" id="Seg_573" s="T32">v</ta>
            <ta e="T34" id="Seg_574" s="T33">adj</ta>
            <ta e="T35" id="Seg_575" s="T34">pro</ta>
            <ta e="T36" id="Seg_576" s="T35">v</ta>
            <ta e="T37" id="Seg_577" s="T36">adj</ta>
            <ta e="T38" id="Seg_578" s="T37">v</ta>
            <ta e="T39" id="Seg_579" s="T38">adv</ta>
            <ta e="T40" id="Seg_580" s="T39">v</ta>
            <ta e="T41" id="Seg_581" s="T40">adv</ta>
            <ta e="T42" id="Seg_582" s="T41">v</ta>
            <ta e="T43" id="Seg_583" s="T42">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_584" s="T0">pro.h:E</ta>
            <ta e="T3" id="Seg_585" s="T2">pro.h:E</ta>
            <ta e="T6" id="Seg_586" s="T5">adv:Time</ta>
            <ta e="T7" id="Seg_587" s="T6">np:Poss</ta>
            <ta e="T8" id="Seg_588" s="T7">np:Th</ta>
            <ta e="T9" id="Seg_589" s="T8">0.1.h:E</ta>
            <ta e="T10" id="Seg_590" s="T9">0.1.h:A</ta>
            <ta e="T11" id="Seg_591" s="T10">np:Com</ta>
            <ta e="T12" id="Seg_592" s="T11">np:A</ta>
            <ta e="T13" id="Seg_593" s="T44">pro.h:Th</ta>
            <ta e="T14" id="Seg_594" s="T13">np:Path</ta>
            <ta e="T15" id="Seg_595" s="T14">pro.h:A</ta>
            <ta e="T16" id="Seg_596" s="T15">pro:Th</ta>
            <ta e="T18" id="Seg_597" s="T17">np:A</ta>
            <ta e="T19" id="Seg_598" s="T18">adv:L</ta>
            <ta e="T21" id="Seg_599" s="T20">np:Th</ta>
            <ta e="T22" id="Seg_600" s="T21">0.3:A</ta>
            <ta e="T23" id="Seg_601" s="T22">pro.h:A</ta>
            <ta e="T26" id="Seg_602" s="T25">0.1.h:A</ta>
            <ta e="T27" id="Seg_603" s="T26">adv:Time</ta>
            <ta e="T28" id="Seg_604" s="T27">0.1.h:A</ta>
            <ta e="T29" id="Seg_605" s="T28">adv:Time</ta>
            <ta e="T32" id="Seg_606" s="T31">0.1.h:A</ta>
            <ta e="T35" id="Seg_607" s="T34">pro:P</ta>
            <ta e="T38" id="Seg_608" s="T37">0.3:P</ta>
            <ta e="T39" id="Seg_609" s="T38">adv:Time</ta>
            <ta e="T40" id="Seg_610" s="T39">0.1.h:A</ta>
            <ta e="T41" id="Seg_611" s="T40">adv:G</ta>
            <ta e="T42" id="Seg_612" s="T41">0.1.h:A </ta>
            <ta e="T43" id="Seg_613" s="T42">np.h:R</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_614" s="T0">pro.h:S</ta>
            <ta e="T2" id="Seg_615" s="T1">v:pred</ta>
            <ta e="T3" id="Seg_616" s="T2">pro.h:S</ta>
            <ta e="T5" id="Seg_617" s="T4">v:pred</ta>
            <ta e="T8" id="Seg_618" s="T7">np:O</ta>
            <ta e="T9" id="Seg_619" s="T8">0.1.h:S v:pred</ta>
            <ta e="T10" id="Seg_620" s="T9">0.1.h:S v:pred</ta>
            <ta e="T12" id="Seg_621" s="T11">np:S</ta>
            <ta e="T44" id="Seg_622" s="T12">v:pred</ta>
            <ta e="T13" id="Seg_623" s="T44">pro.h:O</ta>
            <ta e="T15" id="Seg_624" s="T14">pro.h:S</ta>
            <ta e="T16" id="Seg_625" s="T15">pro:O</ta>
            <ta e="T17" id="Seg_626" s="T16">v:pred</ta>
            <ta e="T18" id="Seg_627" s="T17">np:S</ta>
            <ta e="T20" id="Seg_628" s="T19">v:pred</ta>
            <ta e="T21" id="Seg_629" s="T20">np:O</ta>
            <ta e="T22" id="Seg_630" s="T21">0.3:S v:pred</ta>
            <ta e="T23" id="Seg_631" s="T22">pro.h:S</ta>
            <ta e="T24" id="Seg_632" s="T23">v:pred</ta>
            <ta e="T25" id="Seg_633" s="T24">0.1.h:S v:pred</ta>
            <ta e="T26" id="Seg_634" s="T25">0.1.h:S v:pred</ta>
            <ta e="T28" id="Seg_635" s="T27">0.1.h:S v:pred</ta>
            <ta e="T32" id="Seg_636" s="T31">0.1.h:S v:pred</ta>
            <ta e="T33" id="Seg_637" s="T32">v:pred</ta>
            <ta e="T35" id="Seg_638" s="T34">pro:S</ta>
            <ta e="T38" id="Seg_639" s="T37">0.3:S v:pred</ta>
            <ta e="T40" id="Seg_640" s="T39">0.1.h:S v:pred</ta>
            <ta e="T42" id="Seg_641" s="T41">0.1.h:S 0.3:O v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_642" s="T0">new</ta>
            <ta e="T2" id="Seg_643" s="T1">0.new</ta>
            <ta e="T3" id="Seg_644" s="T2">giv-active</ta>
            <ta e="T8" id="Seg_645" s="T7">new</ta>
            <ta e="T9" id="Seg_646" s="T8">0.giv-active</ta>
            <ta e="T10" id="Seg_647" s="T9">0.giv-active</ta>
            <ta e="T11" id="Seg_648" s="T10">new</ta>
            <ta e="T12" id="Seg_649" s="T11">giv-active</ta>
            <ta e="T13" id="Seg_650" s="T44">giv-active</ta>
            <ta e="T14" id="Seg_651" s="T13">accs-gen</ta>
            <ta e="T15" id="Seg_652" s="T14">giv-inactive</ta>
            <ta e="T16" id="Seg_653" s="T15">giv-active</ta>
            <ta e="T18" id="Seg_654" s="T17">giv-active</ta>
            <ta e="T21" id="Seg_655" s="T20">new</ta>
            <ta e="T22" id="Seg_656" s="T21">0.giv-active</ta>
            <ta e="T23" id="Seg_657" s="T22">giv-inactive</ta>
            <ta e="T25" id="Seg_658" s="T24">0.giv-inactive</ta>
            <ta e="T26" id="Seg_659" s="T25">0.giv-active.0</ta>
            <ta e="T28" id="Seg_660" s="T27">0.giv-active</ta>
            <ta e="T32" id="Seg_661" s="T31">0.giv-active.0</ta>
            <ta e="T35" id="Seg_662" s="T34">giv-active</ta>
            <ta e="T38" id="Seg_663" s="T37">0.giv-active</ta>
            <ta e="T40" id="Seg_664" s="T39">0.giv-inactive</ta>
            <ta e="T42" id="Seg_665" s="T41">0.giv-active</ta>
            <ta e="T43" id="Seg_666" s="T42">accs-gen</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T2" id="Seg_667" s="T0">Я знаю.</ta>
            <ta e="T5" id="Seg_668" s="T2">Я живу один. </ta>
            <ta e="T9" id="Seg_669" s="T5">Весной медвежью тропу нашел.</ta>
            <ta e="T11" id="Seg_670" s="T9">[Я] поехал с [моей] собакой.</ta>
            <ta e="T14" id="Seg_671" s="T11">Собака догоняет нас по дороге вперед.</ta>
            <ta e="T17" id="Seg_672" s="T14">Я её догоняю.</ta>
            <ta e="T21" id="Seg_673" s="T17">Собака впереди догнала медведя.</ta>
            <ta e="T22" id="Seg_674" s="T21">Лаять начала.</ta>
            <ta e="T25" id="Seg_675" s="T22">Я пришел, подкрался.</ta>
            <ta e="T28" id="Seg_676" s="T25">Стрелял, потом побежал.</ta>
            <ta e="T32" id="Seg_677" s="T28">Потом еще снова стрелял.</ta>
            <ta e="T35" id="Seg_678" s="T32">Упал мертвый.</ta>
            <ta e="T38" id="Seg_679" s="T35">Мёртвый упал.</ta>
            <ta e="T43" id="Seg_680" s="T38">Потом ободрал, домой принес людям.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2" id="Seg_681" s="T0">I know.</ta>
            <ta e="T5" id="Seg_682" s="T2">I live alone.</ta>
            <ta e="T9" id="Seg_683" s="T5">In spring I found a bear path.</ta>
            <ta e="T11" id="Seg_684" s="T9">I went off with my dog.</ta>
            <ta e="T14" id="Seg_685" s="T11">My dog was catching up with us in front.</ta>
            <ta e="T17" id="Seg_686" s="T14">I was catching up with it.</ta>
            <ta e="T21" id="Seg_687" s="T17">My dog reached a bear in front.</ta>
            <ta e="T22" id="Seg_688" s="T21">It started to bark.</ta>
            <ta e="T25" id="Seg_689" s="T22">I steeled up.</ta>
            <ta e="T28" id="Seg_690" s="T25">I was shooting, then it ran.</ta>
            <ta e="T32" id="Seg_691" s="T28">Then I was shooting again.</ta>
            <ta e="T35" id="Seg_692" s="T32">It dropped dead.</ta>
            <ta e="T38" id="Seg_693" s="T35">It dropped dead.</ta>
            <ta e="T43" id="Seg_694" s="T38">Then I skinned it and brought it to people.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2" id="Seg_695" s="T0">Ich weiß.</ta>
            <ta e="T5" id="Seg_696" s="T2">Ich lebe allein.</ta>
            <ta e="T9" id="Seg_697" s="T5">Im Frühling habe ich einen Bärenweg gefunden.</ta>
            <ta e="T11" id="Seg_698" s="T9">Ich ging mit meinem Hund los.</ta>
            <ta e="T14" id="Seg_699" s="T11">Mein Hund holt uns ein vorne.</ta>
            <ta e="T17" id="Seg_700" s="T14">Ich hole ihn ein.</ta>
            <ta e="T21" id="Seg_701" s="T17">Mein Hund holt vorne den Bären ein.</ta>
            <ta e="T22" id="Seg_702" s="T21">Der Hund fing an zu bellen.</ta>
            <ta e="T25" id="Seg_703" s="T22">Ich kam, schlich mich an ihn an.</ta>
            <ta e="T28" id="Seg_704" s="T25">Ich schoss, dann lief er davon.</ta>
            <ta e="T32" id="Seg_705" s="T28">Dann schoss ich wieder.</ta>
            <ta e="T35" id="Seg_706" s="T32">Er fiel tot um.</ta>
            <ta e="T38" id="Seg_707" s="T35">Er fiel tot um.</ta>
            <ta e="T43" id="Seg_708" s="T38">Dann häutete ich ihn und brachte ihn nach Hause, zu meinen Leuten.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T2" id="Seg_709" s="T0">я расскажу рассказ.</ta>
            <ta e="T5" id="Seg_710" s="T2">я сам живу </ta>
            <ta e="T9" id="Seg_711" s="T5">весной медведя дорога нашел</ta>
            <ta e="T11" id="Seg_712" s="T9">ехал собаком.</ta>
            <ta e="T14" id="Seg_713" s="T11">собакы ушел за дорогу вперед.</ta>
            <ta e="T17" id="Seg_714" s="T14">я это гоняю.</ta>
            <ta e="T21" id="Seg_715" s="T17">собака вперед догнать медведя.</ta>
            <ta e="T22" id="Seg_716" s="T21">лаять начал</ta>
            <ta e="T25" id="Seg_717" s="T22">я пришел тихонько</ta>
            <ta e="T28" id="Seg_718" s="T25">стрелял раненный убежал</ta>
            <ta e="T32" id="Seg_719" s="T28">второй раз стрелял</ta>
            <ta e="T35" id="Seg_720" s="T32">упал мертвый</ta>
            <ta e="T38" id="Seg_721" s="T35">мертвый упал.</ta>
            <ta e="T43" id="Seg_722" s="T38">стал обдирать, домой принес ребятам.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T44" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
