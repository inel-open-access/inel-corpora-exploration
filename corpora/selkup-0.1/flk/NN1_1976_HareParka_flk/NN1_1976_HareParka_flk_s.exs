<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDDAB4DEEA-FB7A-5BAE-8CA9-ED5BD901A507">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">flk\NN1_1976_HareParka_flk\NN1_1976_HareParka_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">35</ud-information>
            <ud-information attribute-name="# HIAT:w">27</ud-information>
            <ud-information attribute-name="# e">27</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="NN1">
            <abbreviation>NN1</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T27" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="NN1"
                      type="t">
         <timeline-fork end="T12" start="T11">
            <tli id="T11.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T26" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Nʼomalʼ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">porqɨ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">ira</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Šitɨ</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">amnʼatɨ</ts>
                  <nts id="Seg_20" n="HIAT:ip">,</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">qum</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">iːjaiːt</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">šöttakkij</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">nɔːt</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">kuralaqın</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_39" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">Iran</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11.tx.1" id="Seg_44" n="HIAT:w" s="T11">mɔːt</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_47" n="HIAT:w" s="T11.tx.1">—</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">tʼulʼ</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">mɔːt</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_57" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_59" n="HIAT:w" s="T14">Qaːlʼe</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_62" n="HIAT:w" s="T15">müttə</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">tümpa</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_69" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_71" n="HIAT:w" s="T17">Ira</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">täː</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">tulʼti</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">porsasä</ts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_84" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_86" n="HIAT:w" s="T21">Qumɨn</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">amertetot</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_93" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_95" n="HIAT:w" s="T23">Ül</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_98" n="HIAT:w" s="T24">tülpat</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_101" n="HIAT:w" s="T25">mɔːtte</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T26" id="Seg_104" n="sc" s="T0">
               <ts e="T1" id="Seg_106" n="e" s="T0">Nʼomalʼ </ts>
               <ts e="T2" id="Seg_108" n="e" s="T1">porqɨ </ts>
               <ts e="T3" id="Seg_110" n="e" s="T2">ira. </ts>
               <ts e="T4" id="Seg_112" n="e" s="T3">Šitɨ </ts>
               <ts e="T5" id="Seg_114" n="e" s="T4">amnʼatɨ, </ts>
               <ts e="T6" id="Seg_116" n="e" s="T5">qum </ts>
               <ts e="T7" id="Seg_118" n="e" s="T6">iːjaiːt </ts>
               <ts e="T27" id="Seg_120" n="e" s="T7">šöt</ts>
               <ts e="T8" id="Seg_122" n="e" s="T27">takkij </ts>
               <ts e="T9" id="Seg_124" n="e" s="T8">nɔːt </ts>
               <ts e="T10" id="Seg_126" n="e" s="T9">kuralaqın. </ts>
               <ts e="T11" id="Seg_128" n="e" s="T10">Iran </ts>
               <ts e="T12" id="Seg_130" n="e" s="T11">mɔːt — </ts>
               <ts e="T13" id="Seg_132" n="e" s="T12">tʼulʼ </ts>
               <ts e="T14" id="Seg_134" n="e" s="T13">mɔːt. </ts>
               <ts e="T15" id="Seg_136" n="e" s="T14">Qaːlʼe </ts>
               <ts e="T16" id="Seg_138" n="e" s="T15">müttə </ts>
               <ts e="T17" id="Seg_140" n="e" s="T16">tümpa. </ts>
               <ts e="T18" id="Seg_142" n="e" s="T17">Ira </ts>
               <ts e="T19" id="Seg_144" n="e" s="T18">täː </ts>
               <ts e="T20" id="Seg_146" n="e" s="T19">tulʼti </ts>
               <ts e="T21" id="Seg_148" n="e" s="T20">porsasä. </ts>
               <ts e="T22" id="Seg_150" n="e" s="T21">Qumɨn </ts>
               <ts e="T23" id="Seg_152" n="e" s="T22">amertetot. </ts>
               <ts e="T24" id="Seg_154" n="e" s="T23">Ül </ts>
               <ts e="T25" id="Seg_156" n="e" s="T24">tülpat </ts>
               <ts e="T26" id="Seg_158" n="e" s="T25">mɔːtte. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_159" s="T0">NN1_1976_HareParka_flk.001 (001)</ta>
            <ta e="T10" id="Seg_160" s="T3">NN1_1976_HareParka_flk.002 (002.001)</ta>
            <ta e="T14" id="Seg_161" s="T10">NN1_1976_HareParka_flk.003 (002.002)</ta>
            <ta e="T17" id="Seg_162" s="T14">NN1_1976_HareParka_flk.004 (002.003)</ta>
            <ta e="T21" id="Seg_163" s="T17">NN1_1976_HareParka_flk.005 (002.004)</ta>
            <ta e="T23" id="Seg_164" s="T21">NN1_1976_HareParka_flk.006 (002.005)</ta>
            <ta e="T26" id="Seg_165" s="T23">NN1_1976_HareParka_flk.007 (002.006)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_166" s="T0">но′маl ′порkы и′ра.</ta>
            <ta e="T10" id="Seg_167" s="T3">шʼиты а′мнʼаты, kум ′иjа ӣт шътаkий нот kу′раlаkын.</ta>
            <ta e="T14" id="Seg_168" s="T10">′ӣран мо̄т тӱl мо̄т.</ta>
            <ta e="T17" id="Seg_169" s="T14">kа̄лʼе мӱ̄дъ ′тӱмба.</ta>
            <ta e="T21" id="Seg_170" s="T17">и′ра тӓ̄тулʼди порсасӓ.</ta>
            <ta e="T23" id="Seg_171" s="T21">′kумын ′амертедот.</ta>
            <ta e="T26" id="Seg_172" s="T23">ӱ̄l тӱlбат мо̄тте.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_173" s="T0">nomalʼ porqɨ ira.</ta>
            <ta e="T10" id="Seg_174" s="T3">šʼitɨ amnʼatɨ, qum ija iːt šətaqij not quralʼaqɨn.</ta>
            <ta e="T14" id="Seg_175" s="T10">iːran moːt tülʼ moːt.</ta>
            <ta e="T17" id="Seg_176" s="T14">qaːlʼe müːdə tümpa.</ta>
            <ta e="T21" id="Seg_177" s="T17">ira täːtulʼdi porsasä.</ta>
            <ta e="T23" id="Seg_178" s="T21">qumɨn amertedot.</ta>
            <ta e="T26" id="Seg_179" s="T23">üːlʼ tülʼpat moːtte.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_180" s="T0">Nʼomalʼ porqɨ ira. </ta>
            <ta e="T10" id="Seg_181" s="T3">Šitɨ amnʼatɨ, qum iːjaiːt šöt takkij nɔːt kuralaqın. </ta>
            <ta e="T14" id="Seg_182" s="T10">Iran mɔːt — tʼulʼ mɔːt. </ta>
            <ta e="T17" id="Seg_183" s="T14">Qaːlʼe müttə tümpa. </ta>
            <ta e="T21" id="Seg_184" s="T17">Ira täː tulʼti porsasä. </ta>
            <ta e="T23" id="Seg_185" s="T21">Qumɨn amertetot. </ta>
            <ta e="T26" id="Seg_186" s="T23">Ül tülpat mɔːtte. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_187" s="T0">nʼoma-lʼ</ta>
            <ta e="T2" id="Seg_188" s="T1">porqɨ</ta>
            <ta e="T3" id="Seg_189" s="T2">ira</ta>
            <ta e="T4" id="Seg_190" s="T3">šitɨ</ta>
            <ta e="T5" id="Seg_191" s="T4">amnʼa-tɨ</ta>
            <ta e="T6" id="Seg_192" s="T5">qum</ta>
            <ta e="T7" id="Seg_193" s="T6">iːja-iː-t</ta>
            <ta e="T27" id="Seg_194" s="T7">šöt</ta>
            <ta e="T8" id="Seg_195" s="T27">takki-j </ta>
            <ta e="T9" id="Seg_196" s="T8">nɔːt</ta>
            <ta e="T10" id="Seg_197" s="T9">kur-al-a-qı-n</ta>
            <ta e="T11" id="Seg_198" s="T10">ira-n</ta>
            <ta e="T12" id="Seg_199" s="T11">mɔːt</ta>
            <ta e="T13" id="Seg_200" s="T12">tʼu-lʼ</ta>
            <ta e="T14" id="Seg_201" s="T13">mɔːt</ta>
            <ta e="T15" id="Seg_202" s="T14">qaːlʼe</ta>
            <ta e="T16" id="Seg_203" s="T15">müt-tə</ta>
            <ta e="T17" id="Seg_204" s="T16">tü-mpa</ta>
            <ta e="T18" id="Seg_205" s="T17">ira</ta>
            <ta e="T19" id="Seg_206" s="T18">täː</ta>
            <ta e="T20" id="Seg_207" s="T19">tulʼ-ti</ta>
            <ta e="T21" id="Seg_208" s="T20">porsa-sä</ta>
            <ta e="T22" id="Seg_209" s="T21">qum-ɨ-n</ta>
            <ta e="T23" id="Seg_210" s="T22">am-e-r-te-tot</ta>
            <ta e="T24" id="Seg_211" s="T23">ül</ta>
            <ta e="T25" id="Seg_212" s="T24">tül-pa-t</ta>
            <ta e="T26" id="Seg_213" s="T25">mɔːt-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_214" s="T0">nʼoma-lʼ</ta>
            <ta e="T2" id="Seg_215" s="T1">porqɨ</ta>
            <ta e="T3" id="Seg_216" s="T2">ira</ta>
            <ta e="T4" id="Seg_217" s="T3">šittɨ</ta>
            <ta e="T5" id="Seg_218" s="T4">ämnä-tɨ</ta>
            <ta e="T6" id="Seg_219" s="T5">qum</ta>
            <ta e="T7" id="Seg_220" s="T6">iːja-iː-tɨ</ta>
            <ta e="T27" id="Seg_221" s="T7">šöt</ta>
            <ta e="T8" id="Seg_222" s="T27">takkɨ-lʼ</ta>
            <ta e="T9" id="Seg_223" s="T8">nɔːtɨ</ta>
            <ta e="T10" id="Seg_224" s="T9">*kurɨ-ätɔːl-ɨ-qı-naj</ta>
            <ta e="T11" id="Seg_225" s="T10">ira-n</ta>
            <ta e="T12" id="Seg_226" s="T11">mɔːt</ta>
            <ta e="T13" id="Seg_227" s="T12">čʼu-lʼ</ta>
            <ta e="T14" id="Seg_228" s="T13">mɔːt</ta>
            <ta e="T15" id="Seg_229" s="T14">qälɨk</ta>
            <ta e="T16" id="Seg_230" s="T15">mütɨ-ntɨ</ta>
            <ta e="T17" id="Seg_231" s="T16">tü-mpɨ</ta>
            <ta e="T18" id="Seg_232" s="T17">ira</ta>
            <ta e="T19" id="Seg_233" s="T18">tö</ta>
            <ta e="T20" id="Seg_234" s="T19">tul-tɨ</ta>
            <ta e="T21" id="Seg_235" s="T20">porsa-sä</ta>
            <ta e="T22" id="Seg_236" s="T21">qum-ɨ-t</ta>
            <ta e="T23" id="Seg_237" s="T22">am-ɨ-r-ɛntɨ-tɨt</ta>
            <ta e="T24" id="Seg_238" s="T23">ür</ta>
            <ta e="T25" id="Seg_239" s="T24">tul-mpɨ-tɨ</ta>
            <ta e="T26" id="Seg_240" s="T25">mɔːt-ntɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_241" s="T0">hare-ADJZ</ta>
            <ta e="T2" id="Seg_242" s="T1">clothing.[NOM]</ta>
            <ta e="T3" id="Seg_243" s="T2">old.man.[NOM]</ta>
            <ta e="T4" id="Seg_244" s="T3">two</ta>
            <ta e="T5" id="Seg_245" s="T4">daughter.in.law.[NOM]-3SG</ta>
            <ta e="T6" id="Seg_246" s="T5">human.being.[NOM]</ta>
            <ta e="T7" id="Seg_247" s="T6">child-PL.[NOM]-3SG</ta>
            <ta e="T27" id="Seg_248" s="T7">forest.[NOM]</ta>
            <ta e="T8" id="Seg_249" s="T27">down.the.river-ADJZ</ta>
            <ta e="T9" id="Seg_250" s="T8">then</ta>
            <ta e="T10" id="Seg_251" s="T9">go-MOM-EP-3DU.S-EMPH</ta>
            <ta e="T11" id="Seg_252" s="T10">old.man-GEN</ta>
            <ta e="T12" id="Seg_253" s="T11">house.[NOM]</ta>
            <ta e="T13" id="Seg_254" s="T12">earth-ADJZ</ta>
            <ta e="T14" id="Seg_255" s="T13">house.[NOM]</ta>
            <ta e="T15" id="Seg_256" s="T14">Nenets.[NOM]</ta>
            <ta e="T16" id="Seg_257" s="T15">war-ILL</ta>
            <ta e="T17" id="Seg_258" s="T16">come-PST.NAR.[3SG.S]</ta>
            <ta e="T18" id="Seg_259" s="T17">old.man.[NOM]</ta>
            <ta e="T19" id="Seg_260" s="T18">birchbark.[NOM]</ta>
            <ta e="T20" id="Seg_261" s="T19">bring-3SG.O</ta>
            <ta e="T21" id="Seg_262" s="T20">meal.of.dried.fish-COM</ta>
            <ta e="T22" id="Seg_263" s="T21">human.being-EP-PL.[NOM]</ta>
            <ta e="T23" id="Seg_264" s="T22">eat-EP-FRQ-FUT-3PL</ta>
            <ta e="T24" id="Seg_265" s="T23">fat.[NOM]</ta>
            <ta e="T25" id="Seg_266" s="T24">bring-PST.NAR-3SG.O</ta>
            <ta e="T26" id="Seg_267" s="T25">house-ILL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_268" s="T0">заяц-ADJZ</ta>
            <ta e="T2" id="Seg_269" s="T1">одежда.[NOM]</ta>
            <ta e="T3" id="Seg_270" s="T2">старик.[NOM]</ta>
            <ta e="T4" id="Seg_271" s="T3">два</ta>
            <ta e="T5" id="Seg_272" s="T4">жена.сына.[NOM]-3SG</ta>
            <ta e="T6" id="Seg_273" s="T5">человек.[NOM]</ta>
            <ta e="T7" id="Seg_274" s="T6">ребенок-PL.[NOM]-3SG</ta>
            <ta e="T27" id="Seg_275" s="T7">лес.[NOM]</ta>
            <ta e="T8" id="Seg_276" s="T27">вниз.по.течению.реки-ADJZ</ta>
            <ta e="T9" id="Seg_277" s="T8">затем</ta>
            <ta e="T10" id="Seg_278" s="T9">идти-MOM-EP-3DU.S-EMPH</ta>
            <ta e="T11" id="Seg_279" s="T10">старик-GEN</ta>
            <ta e="T12" id="Seg_280" s="T11">дом.[NOM]</ta>
            <ta e="T13" id="Seg_281" s="T12">земля-ADJZ</ta>
            <ta e="T14" id="Seg_282" s="T13">дом.[NOM]</ta>
            <ta e="T15" id="Seg_283" s="T14">ненец.[NOM]</ta>
            <ta e="T16" id="Seg_284" s="T15">война-ILL</ta>
            <ta e="T17" id="Seg_285" s="T16">прийти-PST.NAR.[3SG.S]</ta>
            <ta e="T18" id="Seg_286" s="T17">старик.[NOM]</ta>
            <ta e="T19" id="Seg_287" s="T18">береста.[NOM]</ta>
            <ta e="T20" id="Seg_288" s="T19">занести-3SG.O</ta>
            <ta e="T21" id="Seg_289" s="T20">порса-COM</ta>
            <ta e="T22" id="Seg_290" s="T21">человек-EP-PL.[NOM]</ta>
            <ta e="T23" id="Seg_291" s="T22">съесть-EP-FRQ-FUT-3PL</ta>
            <ta e="T24" id="Seg_292" s="T23">жир.[NOM]</ta>
            <ta e="T25" id="Seg_293" s="T24">занести-PST.NAR-3SG.O</ta>
            <ta e="T26" id="Seg_294" s="T25">дом-ILL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_295" s="T0">n-n&gt;adj</ta>
            <ta e="T2" id="Seg_296" s="T1">n-n:case3</ta>
            <ta e="T3" id="Seg_297" s="T2">n-n:case3</ta>
            <ta e="T4" id="Seg_298" s="T3">num</ta>
            <ta e="T5" id="Seg_299" s="T4">n-n:case1-n:poss</ta>
            <ta e="T6" id="Seg_300" s="T5">n-n:case3</ta>
            <ta e="T7" id="Seg_301" s="T6">n-n:num-n:case1-n:poss</ta>
            <ta e="T27" id="Seg_302" s="T7">n-n:case3</ta>
            <ta e="T8" id="Seg_303" s="T27">adv-adv&gt;adj</ta>
            <ta e="T9" id="Seg_304" s="T8">adv</ta>
            <ta e="T10" id="Seg_305" s="T9">v-v&gt;v-v:ins-v:pn-clit</ta>
            <ta e="T11" id="Seg_306" s="T10">n-n:case3</ta>
            <ta e="T12" id="Seg_307" s="T11">n-n:case3</ta>
            <ta e="T13" id="Seg_308" s="T12">n-n&gt;adj</ta>
            <ta e="T14" id="Seg_309" s="T13">n-n:case3</ta>
            <ta e="T15" id="Seg_310" s="T14">n-n:case3</ta>
            <ta e="T16" id="Seg_311" s="T15">n-n:case3</ta>
            <ta e="T17" id="Seg_312" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_313" s="T17">n-n:case3</ta>
            <ta e="T19" id="Seg_314" s="T18">n-n:case3</ta>
            <ta e="T20" id="Seg_315" s="T19">v-v:pn</ta>
            <ta e="T21" id="Seg_316" s="T20">n-n:case3</ta>
            <ta e="T22" id="Seg_317" s="T21">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T23" id="Seg_318" s="T22">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_319" s="T23">n-n:case3</ta>
            <ta e="T25" id="Seg_320" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_321" s="T25">n-n:case3</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_322" s="T0">n</ta>
            <ta e="T2" id="Seg_323" s="T1">n</ta>
            <ta e="T3" id="Seg_324" s="T2">n</ta>
            <ta e="T4" id="Seg_325" s="T3">num</ta>
            <ta e="T5" id="Seg_326" s="T4">n</ta>
            <ta e="T6" id="Seg_327" s="T5">n</ta>
            <ta e="T7" id="Seg_328" s="T6">n</ta>
            <ta e="T27" id="Seg_329" s="T7">np:G</ta>
            <ta e="T8" id="Seg_330" s="T27">adj</ta>
            <ta e="T9" id="Seg_331" s="T8">adv</ta>
            <ta e="T10" id="Seg_332" s="T9">v</ta>
            <ta e="T11" id="Seg_333" s="T10">n</ta>
            <ta e="T12" id="Seg_334" s="T11">n</ta>
            <ta e="T13" id="Seg_335" s="T12">adj</ta>
            <ta e="T14" id="Seg_336" s="T13">n</ta>
            <ta e="T15" id="Seg_337" s="T14">n</ta>
            <ta e="T16" id="Seg_338" s="T15">n</ta>
            <ta e="T17" id="Seg_339" s="T16">v</ta>
            <ta e="T18" id="Seg_340" s="T17">n</ta>
            <ta e="T19" id="Seg_341" s="T18">n</ta>
            <ta e="T20" id="Seg_342" s="T19">v</ta>
            <ta e="T21" id="Seg_343" s="T20">n</ta>
            <ta e="T22" id="Seg_344" s="T21">n</ta>
            <ta e="T23" id="Seg_345" s="T22">v</ta>
            <ta e="T24" id="Seg_346" s="T23">n</ta>
            <ta e="T25" id="Seg_347" s="T24">v</ta>
            <ta e="T26" id="Seg_348" s="T25">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T5" id="Seg_349" s="T4">np.h:A</ta>
            <ta e="T7" id="Seg_350" s="T6">np.h:A</ta>
            <ta e="T9" id="Seg_351" s="T8">adv:Time</ta>
            <ta e="T11" id="Seg_352" s="T10">np.h:Poss</ta>
            <ta e="T12" id="Seg_353" s="T11">np:Th</ta>
            <ta e="T13" id="Seg_354" s="T12">np:Poss</ta>
            <ta e="T15" id="Seg_355" s="T14">np.h:A</ta>
            <ta e="T16" id="Seg_356" s="T15">np:G</ta>
            <ta e="T18" id="Seg_357" s="T17">np.h:A</ta>
            <ta e="T19" id="Seg_358" s="T18">np:Th</ta>
            <ta e="T21" id="Seg_359" s="T20">np:Com</ta>
            <ta e="T22" id="Seg_360" s="T21">np.h:A</ta>
            <ta e="T23" id="Seg_361" s="T22">0.3:P</ta>
            <ta e="T24" id="Seg_362" s="T23">np:Th</ta>
            <ta e="T25" id="Seg_363" s="T24">0.3.h:A</ta>
            <ta e="T26" id="Seg_364" s="T25">np:G</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T5" id="Seg_365" s="T4">np.h:S</ta>
            <ta e="T7" id="Seg_366" s="T6">np.h:S</ta>
            <ta e="T10" id="Seg_367" s="T9">v:pred</ta>
            <ta e="T12" id="Seg_368" s="T11">np:S</ta>
            <ta e="T14" id="Seg_369" s="T13">n:pred</ta>
            <ta e="T15" id="Seg_370" s="T14">np.h:S</ta>
            <ta e="T17" id="Seg_371" s="T16">v:pred</ta>
            <ta e="T18" id="Seg_372" s="T17">np.h:S</ta>
            <ta e="T19" id="Seg_373" s="T18">np:O</ta>
            <ta e="T20" id="Seg_374" s="T19">v:pred</ta>
            <ta e="T22" id="Seg_375" s="T21">np.h:S</ta>
            <ta e="T23" id="Seg_376" s="T22">0.3:O v:pred</ta>
            <ta e="T24" id="Seg_377" s="T23">np:O</ta>
            <ta e="T25" id="Seg_378" s="T24">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_379" s="T0">Старик "заячья шуба".</ta>
            <ta e="T10" id="Seg_380" s="T3">Две его невестки, его сыновья [вдвоём] в лес вниз по течению реки идут.</ta>
            <ta e="T14" id="Seg_381" s="T10">Дом старика – землянка.</ta>
            <ta e="T17" id="Seg_382" s="T14">Ненцы пришли воевать.</ta>
            <ta e="T21" id="Seg_383" s="T17">Старик приносит бересту и порсу.</ta>
            <ta e="T23" id="Seg_384" s="T21">"Люди есть будут".</ta>
            <ta e="T26" id="Seg_385" s="T23">Он принёс жир в дом.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_386" s="T0">The old man "Hare Parka".</ta>
            <ta e="T10" id="Seg_387" s="T3">Two his daughters-in-law, his sons [the two of them] go down the river to the forest.</ta>
            <ta e="T14" id="Seg_388" s="T10">A dugout is the old man's house.</ta>
            <ta e="T17" id="Seg_389" s="T14">Nenets came with war.</ta>
            <ta e="T21" id="Seg_390" s="T17">The old man brings birchbark and some meal of dried fish.</ta>
            <ta e="T23" id="Seg_391" s="T21">"People will eat".</ta>
            <ta e="T26" id="Seg_392" s="T23">He took fat into the house.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_393" s="T0">Der alte Mann "Hasenparka".</ta>
            <ta e="T10" id="Seg_394" s="T3">Zwei seiner Schwiegertöchter, seine Söhne gehen [zu zweit] flussabwärts in den Wald.</ta>
            <ta e="T14" id="Seg_395" s="T10">Das Haus des Alten ist eine Erdhütte.</ta>
            <ta e="T17" id="Seg_396" s="T14">Nenzen kamen zum Kämpfen.</ta>
            <ta e="T21" id="Seg_397" s="T17">Der Alte bringt Birkenrinde und eine Mahlzeit aus getrocknetem Fisch.</ta>
            <ta e="T23" id="Seg_398" s="T21">"Die Leute werden essen."</ta>
            <ta e="T26" id="Seg_399" s="T23">Er brachte Fett ins Haus hinein.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T10" id="Seg_400" s="T3">две невестки дети его в лес ушли</ta>
            <ta e="T14" id="Seg_401" s="T10">дом старика – землянка</ta>
            <ta e="T17" id="Seg_402" s="T14">юрак пришел войной</ta>
            <ta e="T21" id="Seg_403" s="T17">старик принес тиски, постелил</ta>
            <ta e="T23" id="Seg_404" s="T21">люди есть будут</ta>
            <ta e="T26" id="Seg_405" s="T23">жир принес в дом.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_406" s="T0">[OSV]: "Nomalʼ porqɨ" - the nickname of an old man in Selkup fairytales.</ta>
            <ta e="T21" id="Seg_407" s="T17">[OSV]: 1) "täː" could be an idiolectal variant or uncorrect transcription of "tö" ("birchbark"); 2) The speaker alternates between Past and Present tenses.</ta>
            <ta e="T26" id="Seg_408" s="T23">[OSV]: The text is not finished.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T27" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
