<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDC184BB7C-B081-6A2E-89D4-2AD351A0A638">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">flk\KPM_1977_IchakechikaAndQolsaqo_flk\KPM_1977_IchakechikaAndQolsaqo_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">999</ud-information>
            <ud-information attribute-name="# HIAT:w">784</ud-information>
            <ud-information attribute-name="# e">753</ud-information>
            <ud-information attribute-name="# HIAT:u">137</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KPM">
            <abbreviation>KPM</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
         <tli id="T318" />
         <tli id="T319" />
         <tli id="T320" />
         <tli id="T321" />
         <tli id="T322" />
         <tli id="T323" />
         <tli id="T324" />
         <tli id="T325" />
         <tli id="T326" />
         <tli id="T327" />
         <tli id="T328" />
         <tli id="T329" />
         <tli id="T330" />
         <tli id="T331" />
         <tli id="T332" />
         <tli id="T333" />
         <tli id="T334" />
         <tli id="T335" />
         <tli id="T336" />
         <tli id="T337" />
         <tli id="T338" />
         <tli id="T339" />
         <tli id="T340" />
         <tli id="T341" />
         <tli id="T342" />
         <tli id="T343" />
         <tli id="T344" />
         <tli id="T345" />
         <tli id="T346" />
         <tli id="T347" />
         <tli id="T348" />
         <tli id="T349" />
         <tli id="T350" />
         <tli id="T351" />
         <tli id="T352" />
         <tli id="T353" />
         <tli id="T354" />
         <tli id="T355" />
         <tli id="T356" />
         <tli id="T357" />
         <tli id="T358" />
         <tli id="T359" />
         <tli id="T360" />
         <tli id="T361" />
         <tli id="T362" />
         <tli id="T363" />
         <tli id="T364" />
         <tli id="T365" />
         <tli id="T366" />
         <tli id="T367" />
         <tli id="T368" />
         <tli id="T369" />
         <tli id="T370" />
         <tli id="T371" />
         <tli id="T372" />
         <tli id="T373" />
         <tli id="T374" />
         <tli id="T375" />
         <tli id="T376" />
         <tli id="T377" />
         <tli id="T378" />
         <tli id="T379" />
         <tli id="T380" />
         <tli id="T381" />
         <tli id="T382" />
         <tli id="T383" />
         <tli id="T384" />
         <tli id="T385" />
         <tli id="T386" />
         <tli id="T387" />
         <tli id="T388" />
         <tli id="T389" />
         <tli id="T390" />
         <tli id="T391" />
         <tli id="T392" />
         <tli id="T393" />
         <tli id="T394" />
         <tli id="T395" />
         <tli id="T396" />
         <tli id="T397" />
         <tli id="T398" />
         <tli id="T399" />
         <tli id="T400" />
         <tli id="T401" />
         <tli id="T402" />
         <tli id="T403" />
         <tli id="T404" />
         <tli id="T405" />
         <tli id="T406" />
         <tli id="T407" />
         <tli id="T408" />
         <tli id="T409" />
         <tli id="T410" />
         <tli id="T411" />
         <tli id="T412" />
         <tli id="T413" />
         <tli id="T414" />
         <tli id="T415" />
         <tli id="T416" />
         <tli id="T417" />
         <tli id="T418" />
         <tli id="T419" />
         <tli id="T420" />
         <tli id="T421" />
         <tli id="T422" />
         <tli id="T423" />
         <tli id="T424" />
         <tli id="T425" />
         <tli id="T426" />
         <tli id="T427" />
         <tli id="T428" />
         <tli id="T429" />
         <tli id="T430" />
         <tli id="T431" />
         <tli id="T432" />
         <tli id="T433" />
         <tli id="T434" />
         <tli id="T435" />
         <tli id="T436" />
         <tli id="T437" />
         <tli id="T438" />
         <tli id="T439" />
         <tli id="T440" />
         <tli id="T441" />
         <tli id="T442" />
         <tli id="T443" />
         <tli id="T444" />
         <tli id="T445" />
         <tli id="T446" />
         <tli id="T447" />
         <tli id="T448" />
         <tli id="T449" />
         <tli id="T450" />
         <tli id="T451" />
         <tli id="T452" />
         <tli id="T453" />
         <tli id="T454" />
         <tli id="T455" />
         <tli id="T456" />
         <tli id="T457" />
         <tli id="T458" />
         <tli id="T459" />
         <tli id="T460" />
         <tli id="T461" />
         <tli id="T753" />
         <tli id="T462" />
         <tli id="T463" />
         <tli id="T464" />
         <tli id="T465" />
         <tli id="T466" />
         <tli id="T467" />
         <tli id="T468" />
         <tli id="T469" />
         <tli id="T470" />
         <tli id="T471" />
         <tli id="T472" />
         <tli id="T473" />
         <tli id="T474" />
         <tli id="T475" />
         <tli id="T476" />
         <tli id="T477" />
         <tli id="T478" />
         <tli id="T479" />
         <tli id="T480" />
         <tli id="T481" />
         <tli id="T482" />
         <tli id="T483" />
         <tli id="T484" />
         <tli id="T485" />
         <tli id="T486" />
         <tli id="T487" />
         <tli id="T488" />
         <tli id="T489" />
         <tli id="T490" />
         <tli id="T491" />
         <tli id="T492" />
         <tli id="T493" />
         <tli id="T494" />
         <tli id="T495" />
         <tli id="T496" />
         <tli id="T497" />
         <tli id="T498" />
         <tli id="T499" />
         <tli id="T500" />
         <tli id="T501" />
         <tli id="T502" />
         <tli id="T503" />
         <tli id="T504" />
         <tli id="T505" />
         <tli id="T506" />
         <tli id="T507" />
         <tli id="T508" />
         <tli id="T509" />
         <tli id="T510" />
         <tli id="T511" />
         <tli id="T512" />
         <tli id="T513" />
         <tli id="T514" />
         <tli id="T515" />
         <tli id="T516" />
         <tli id="T517" />
         <tli id="T518" />
         <tli id="T519" />
         <tli id="T520" />
         <tli id="T521" />
         <tli id="T522" />
         <tli id="T523" />
         <tli id="T524" />
         <tli id="T525" />
         <tli id="T526" />
         <tli id="T527" />
         <tli id="T528" />
         <tli id="T529" />
         <tli id="T530" />
         <tli id="T531" />
         <tli id="T532" />
         <tli id="T533" />
         <tli id="T534" />
         <tli id="T535" />
         <tli id="T536" />
         <tli id="T537" />
         <tli id="T538" />
         <tli id="T539" />
         <tli id="T540" />
         <tli id="T541" />
         <tli id="T542" />
         <tli id="T543" />
         <tli id="T544" />
         <tli id="T545" />
         <tli id="T546" />
         <tli id="T547" />
         <tli id="T548" />
         <tli id="T549" />
         <tli id="T550" />
         <tli id="T551" />
         <tli id="T552" />
         <tli id="T553" />
         <tli id="T554" />
         <tli id="T555" />
         <tli id="T556" />
         <tli id="T557" />
         <tli id="T558" />
         <tli id="T559" />
         <tli id="T560" />
         <tli id="T561" />
         <tli id="T562" />
         <tli id="T563" />
         <tli id="T564" />
         <tli id="T565" />
         <tli id="T566" />
         <tli id="T567" />
         <tli id="T568" />
         <tli id="T569" />
         <tli id="T570" />
         <tli id="T571" />
         <tli id="T572" />
         <tli id="T573" />
         <tli id="T574" />
         <tli id="T575" />
         <tli id="T576" />
         <tli id="T577" />
         <tli id="T578" />
         <tli id="T579" />
         <tli id="T580" />
         <tli id="T581" />
         <tli id="T582" />
         <tli id="T583" />
         <tli id="T584" />
         <tli id="T585" />
         <tli id="T586" />
         <tli id="T587" />
         <tli id="T588" />
         <tli id="T589" />
         <tli id="T590" />
         <tli id="T591" />
         <tli id="T592" />
         <tli id="T593" />
         <tli id="T594" />
         <tli id="T595" />
         <tli id="T596" />
         <tli id="T597" />
         <tli id="T598" />
         <tli id="T599" />
         <tli id="T600" />
         <tli id="T601" />
         <tli id="T602" />
         <tli id="T603" />
         <tli id="T604" />
         <tli id="T605" />
         <tli id="T606" />
         <tli id="T607" />
         <tli id="T608" />
         <tli id="T609" />
         <tli id="T610" />
         <tli id="T611" />
         <tli id="T612" />
         <tli id="T613" />
         <tli id="T614" />
         <tli id="T615" />
         <tli id="T616" />
         <tli id="T617" />
         <tli id="T618" />
         <tli id="T619" />
         <tli id="T620" />
         <tli id="T621" />
         <tli id="T622" />
         <tli id="T623" />
         <tli id="T624" />
         <tli id="T625" />
         <tli id="T626" />
         <tli id="T627" />
         <tli id="T628" />
         <tli id="T629" />
         <tli id="T630" />
         <tli id="T631" />
         <tli id="T632" />
         <tli id="T633" />
         <tli id="T634" />
         <tli id="T635" />
         <tli id="T636" />
         <tli id="T637" />
         <tli id="T638" />
         <tli id="T639" />
         <tli id="T640" />
         <tli id="T641" />
         <tli id="T642" />
         <tli id="T643" />
         <tli id="T644" />
         <tli id="T645" />
         <tli id="T646" />
         <tli id="T647" />
         <tli id="T648" />
         <tli id="T649" />
         <tli id="T650" />
         <tli id="T651" />
         <tli id="T652" />
         <tli id="T653" />
         <tli id="T654" />
         <tli id="T655" />
         <tli id="T656" />
         <tli id="T657" />
         <tli id="T658" />
         <tli id="T659" />
         <tli id="T660" />
         <tli id="T661" />
         <tli id="T662" />
         <tli id="T663" />
         <tli id="T664" />
         <tli id="T665" />
         <tli id="T666" />
         <tli id="T667" />
         <tli id="T668" />
         <tli id="T669" />
         <tli id="T670" />
         <tli id="T671" />
         <tli id="T672" />
         <tli id="T673" />
         <tli id="T674" />
         <tli id="T675" />
         <tli id="T676" />
         <tli id="T677" />
         <tli id="T678" />
         <tli id="T679" />
         <tli id="T680" />
         <tli id="T681" />
         <tli id="T682" />
         <tli id="T683" />
         <tli id="T684" />
         <tli id="T685" />
         <tli id="T686" />
         <tli id="T687" />
         <tli id="T688" />
         <tli id="T689" />
         <tli id="T690" />
         <tli id="T691" />
         <tli id="T692" />
         <tli id="T693" />
         <tli id="T694" />
         <tli id="T695" />
         <tli id="T696" />
         <tli id="T697" />
         <tli id="T698" />
         <tli id="T699" />
         <tli id="T700" />
         <tli id="T701" />
         <tli id="T702" />
         <tli id="T703" />
         <tli id="T704" />
         <tli id="T705" />
         <tli id="T706" />
         <tli id="T707" />
         <tli id="T708" />
         <tli id="T709" />
         <tli id="T710" />
         <tli id="T711" />
         <tli id="T712" />
         <tli id="T713" />
         <tli id="T714" />
         <tli id="T715" />
         <tli id="T716" />
         <tli id="T717" />
         <tli id="T718" />
         <tli id="T719" />
         <tli id="T720" />
         <tli id="T721" />
         <tli id="T722" />
         <tli id="T723" />
         <tli id="T724" />
         <tli id="T725" />
         <tli id="T726" />
         <tli id="T727" />
         <tli id="T728" />
         <tli id="T729" />
         <tli id="T730" />
         <tli id="T731" />
         <tli id="T732" />
         <tli id="T733" />
         <tli id="T734" />
         <tli id="T735" />
         <tli id="T736" />
         <tli id="T737" />
         <tli id="T738" />
         <tli id="T739" />
         <tli id="T740" />
         <tli id="T741" />
         <tli id="T742" />
         <tli id="T743" />
         <tli id="T744" />
         <tli id="T745" />
         <tli id="T746" />
         <tli id="T747" />
         <tli id="T748" />
         <tli id="T749" />
         <tli id="T750" />
         <tli id="T751" />
         <tli id="T752" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KPM"
                      type="t">
         <timeline-fork end="T92" start="T91">
            <tli id="T91.tx.1" />
         </timeline-fork>
         <timeline-fork end="T101" start="T100">
            <tli id="T100.tx.1" />
         </timeline-fork>
         <timeline-fork end="T120" start="T119">
            <tli id="T119.tx.1" />
         </timeline-fork>
         <timeline-fork end="T154" start="T153">
            <tli id="T153.tx.1" />
         </timeline-fork>
         <timeline-fork end="T184" start="T183">
            <tli id="T183.tx.1" />
         </timeline-fork>
         <timeline-fork end="T201" start="T200">
            <tli id="T200.tx.1" />
         </timeline-fork>
         <timeline-fork end="T219" start="T218">
            <tli id="T218.tx.1" />
         </timeline-fork>
         <timeline-fork end="T246" start="T245">
            <tli id="T245.tx.1" />
         </timeline-fork>
         <timeline-fork end="T284" start="T283">
            <tli id="T283.tx.1" />
         </timeline-fork>
         <timeline-fork end="T287" start="T286">
            <tli id="T286.tx.1" />
         </timeline-fork>
         <timeline-fork end="T301" start="T300">
            <tli id="T300.tx.1" />
         </timeline-fork>
         <timeline-fork end="T360" start="T359">
            <tli id="T359.tx.1" />
         </timeline-fork>
         <timeline-fork end="T402" start="T401">
            <tli id="T401.tx.1" />
         </timeline-fork>
         <timeline-fork end="T403" start="T402">
            <tli id="T402.tx.1" />
         </timeline-fork>
         <timeline-fork end="T404" start="T403">
            <tli id="T403.tx.1" />
         </timeline-fork>
         <timeline-fork end="T406" start="T405">
            <tli id="T405.tx.1" />
         </timeline-fork>
         <timeline-fork end="T408" start="T407">
            <tli id="T407.tx.1" />
         </timeline-fork>
         <timeline-fork end="T413" start="T412">
            <tli id="T412.tx.1" />
         </timeline-fork>
         <timeline-fork end="T429" start="T428">
            <tli id="T428.tx.1" />
         </timeline-fork>
         <timeline-fork end="T431" start="T430">
            <tli id="T430.tx.1" />
         </timeline-fork>
         <timeline-fork end="T451" start="T450">
            <tli id="T450.tx.1" />
         </timeline-fork>
         <timeline-fork end="T477" start="T476">
            <tli id="T476.tx.1" />
         </timeline-fork>
         <timeline-fork end="T505" start="T504">
            <tli id="T504.tx.1" />
         </timeline-fork>
         <timeline-fork end="T513" start="T512">
            <tli id="T512.tx.1" />
         </timeline-fork>
         <timeline-fork end="T584" start="T583">
            <tli id="T583.tx.1" />
         </timeline-fork>
         <timeline-fork end="T590" start="T589">
            <tli id="T589.tx.1" />
         </timeline-fork>
         <timeline-fork end="T618" start="T617">
            <tli id="T617.tx.1" />
         </timeline-fork>
         <timeline-fork end="T624" start="T623">
            <tli id="T623.tx.1" />
         </timeline-fork>
         <timeline-fork end="T654" start="T653">
            <tli id="T653.tx.1" />
         </timeline-fork>
         <timeline-fork end="T709" start="T708">
            <tli id="T708.tx.1" />
         </timeline-fork>
         <timeline-fork end="T712" start="T711">
            <tli id="T711.tx.1" />
         </timeline-fork>
         <timeline-fork end="T732" start="T731">
            <tli id="T731.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T752" id="Seg_0" n="sc" s="T0">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Qolʼsaqo</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ira</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_11" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">İlʼimpa</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">İčʼakäčʼika</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">imlʼantɨsä</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_23" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">Qolʼsaq</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">iralʼ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">qəːttɨ–</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">asa</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">kuntaːqɨn</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">ɛːsa</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_44" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">Okkɨr</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">čʼontoːqɨt</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">Qolʼsaqo</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">iralʼ</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">sɨːrɨtɨ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">üra</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_63" n="HIAT:ip">(</nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">ürɨsa</ts>
                  <nts id="Seg_66" n="HIAT:ip">)</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">telʼde</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">ürɨkka</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_76" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">Täp</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">tɛnɨmɨtɨ</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">što</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">İčʼakäčʼikat</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">tälʼɨsɨt</ts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_94" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">Üːtɔːtɨ</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_99" n="HIAT:w" s="T26">täpɨnɨk</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_102" n="HIAT:w" s="T27">kazatɨp</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">sitatɨp</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_108" n="HIAT:w" s="T29">männɨmpɨqo</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_111" n="HIAT:w" s="T30">qoip</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_114" n="HIAT:w" s="T31">meːtɨt</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_117" n="HIAT:w" s="T32">İčʼakäčʼika</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_121" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_123" n="HIAT:w" s="T33">Täp</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_126" n="HIAT:w" s="T34">tɛnɨmɨtɨ</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_129" n="HIAT:w" s="T35">tüntɔːtɨt</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_132" n="HIAT:w" s="T36">rusʼaksä</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_134" n="HIAT:ip">(</nts>
                  <ts e="T38" id="Seg_136" n="HIAT:w" s="T37">puškatsä</ts>
                  <nts id="Seg_137" n="HIAT:ip">)</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_140" n="HIAT:w" s="T38">täpɨp</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">orqəlʼtɛntɔːtɨt</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_146" n="HIAT:w" s="T40">omtɨltɛntɔːtɨt</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_149" n="HIAT:w" s="T41">tʼurmantɨ</ts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_153" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_155" n="HIAT:w" s="T42">Na</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_158" n="HIAT:w" s="T43">vremʼaqɨt</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_160" n="HIAT:ip">(</nts>
                  <ts e="T45" id="Seg_162" n="HIAT:w" s="T44">na</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_165" n="HIAT:w" s="T45">čʼeːlʼe</ts>
                  <nts id="Seg_166" n="HIAT:ip">)</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_169" n="HIAT:w" s="T46">sɨːrɨp</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_172" n="HIAT:w" s="T47">qəssɨtɨ</ts>
                  <nts id="Seg_173" n="HIAT:ip">,</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_176" n="HIAT:w" s="T48">ketɨmtɨ</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_179" n="HIAT:w" s="T49">kɨmsa</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_182" n="HIAT:w" s="T50">qamtɨsɨtɨ</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_186" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_188" n="HIAT:w" s="T51">Merɨsɨtɨ</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_191" n="HIAT:w" s="T52">marqɨ</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_194" n="HIAT:w" s="T53">paŋɨp</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_197" n="HIAT:w" s="T54">mɔːta</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_200" n="HIAT:w" s="T55">pɔːrɔːqot</ts>
                  <nts id="Seg_201" n="HIAT:ip">.</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_204" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_206" n="HIAT:w" s="T56">İmlʼamtɨ</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_209" n="HIAT:w" s="T57">tamtɨlsɨtɨ</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_212" n="HIAT:w" s="T58">kämɨlʼ</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_215" n="HIAT:w" s="T59">sɨːrɨt</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_218" n="HIAT:w" s="T60">ketɨsa</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_222" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_224" n="HIAT:w" s="T61">İmlʼat</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_227" n="HIAT:w" s="T62">tokkaltistɨ</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_230" n="HIAT:w" s="T63">porqat</ts>
                  <nts id="Seg_231" n="HIAT:ip">.</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_234" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_236" n="HIAT:w" s="T64">Kazat</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_238" n="HIAT:ip">(</nts>
                  <ts e="T66" id="Seg_240" n="HIAT:w" s="T65">kazaqɨt</ts>
                  <nts id="Seg_241" n="HIAT:ip">)</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_244" n="HIAT:w" s="T66">tüsɔːtɨt</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_247" n="HIAT:w" s="T67">İčʼakäčʼikanɨk</ts>
                  <nts id="Seg_248" n="HIAT:ip">.</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_251" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_253" n="HIAT:w" s="T68">Marqa</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_256" n="HIAT:w" s="T69">qup</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_259" n="HIAT:w" s="T70">kätsɨtɨ</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_262" n="HIAT:w" s="T71">İčʼakäčʼikanɨ</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_266" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_268" n="HIAT:w" s="T72">“Tasɨntɨ</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_271" n="HIAT:w" s="T73">omtɨlʼtɨqo</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_274" n="HIAT:w" s="T74">nɔːtna</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_277" n="HIAT:w" s="T75">türʼmantɨ</ts>
                  <nts id="Seg_278" n="HIAT:ip">.</nts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_281" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_283" n="HIAT:w" s="T76">Tat</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_286" n="HIAT:w" s="T77">sɨːrɨtɨp</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_289" n="HIAT:w" s="T78">qättal</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_292" n="HIAT:w" s="T79">amnal”</ts>
                  <nts id="Seg_293" n="HIAT:ip">.</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_296" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_298" n="HIAT:w" s="T80">Täp</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_301" n="HIAT:w" s="T81">nʼenʼnʼamɔːssa</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_304" n="HIAT:w" s="T82">imlʼantɨkinı</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_307" n="HIAT:w" s="T83">kätsɨtɨ</ts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_311" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_313" n="HIAT:w" s="T84">“Massɨp</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_316" n="HIAT:w" s="T85">omtalʼtɨntɔːtɨt</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_319" n="HIAT:w" s="T86">tʼürmantɨ</ts>
                  <nts id="Seg_320" n="HIAT:ip">.</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_323" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_325" n="HIAT:w" s="T87">Čʼajnik</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_328" n="HIAT:w" s="T88">mušerät</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_331" n="HIAT:w" s="T89">känpɨlʼa”</ts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_335" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_337" n="HIAT:w" s="T90">İmlʼatɨ</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91.tx.1" id="Seg_340" n="HIAT:w" s="T91">toːtälʼna</ts>
                  <nts id="Seg_341" n="HIAT:ip">:</nts>
                  <ts e="T92" id="Seg_343" n="HIAT:w" s="T91.tx.1">“</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_346" n="HIAT:w" s="T92">Topop</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_349" n="HIAT:w" s="T93">utop</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_352" n="HIAT:w" s="T94">čʼüssa”</ts>
                  <nts id="Seg_353" n="HIAT:ip">.</nts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_356" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_358" n="HIAT:w" s="T95">Kazat</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_361" n="HIAT:w" s="T96">nɨŋnɔːtɨt</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_364" n="HIAT:w" s="T97">mɔːtan</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_367" n="HIAT:w" s="T98">ɔːqqɨt</ts>
                  <nts id="Seg_368" n="HIAT:ip">.</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_371" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_373" n="HIAT:w" s="T99">İčakäčika</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100.tx.1" id="Seg_376" n="HIAT:w" s="T100">imlʼantɨkinı</ts>
                  <nts id="Seg_377" n="HIAT:ip">:</nts>
                  <ts e="T101" id="Seg_379" n="HIAT:w" s="T100.tx.1">“</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_382" n="HIAT:w" s="T101">Ta</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_385" n="HIAT:w" s="T102">lʼentʼajŋɔːnt</ts>
                  <nts id="Seg_386" n="HIAT:ip">,</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_389" n="HIAT:w" s="T103">čʼajnik</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_392" n="HIAT:w" s="T104">asa</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_395" n="HIAT:w" s="T105">kɨkantɨ</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_398" n="HIAT:w" s="T106">musɨrɨqo”</ts>
                  <nts id="Seg_399" n="HIAT:ip">.</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_402" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_404" n="HIAT:w" s="T107">Nʼenʼnʼimɔːssa</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_407" n="HIAT:w" s="T108">İčʼäkäčʼika</ts>
                  <nts id="Seg_408" n="HIAT:ip">,</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_411" n="HIAT:w" s="T109">paŋɨmtɨ</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_414" n="HIAT:w" s="T110">ılla</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_417" n="HIAT:w" s="T111">iːsɨtɨ</ts>
                  <nts id="Seg_418" n="HIAT:ip">,</nts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_421" n="HIAT:w" s="T112">imlʼamtɨ</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_424" n="HIAT:w" s="T113">mattɨrsɨtɨ</ts>
                  <nts id="Seg_425" n="HIAT:ip">,</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_428" n="HIAT:w" s="T114">imlʼatɨ</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_431" n="HIAT:w" s="T115">qulʼčʼisa</ts>
                  <nts id="Seg_432" n="HIAT:ip">.</nts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_435" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_437" n="HIAT:w" s="T116">Nɨnɨ</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_440" n="HIAT:w" s="T117">İčʼäkäčʼika</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_443" n="HIAT:w" s="T118">kätsɨtɨ</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119.tx.1" id="Seg_446" n="HIAT:w" s="T119">kazatkinı</ts>
                  <nts id="Seg_447" n="HIAT:ip">:</nts>
                  <ts e="T120" id="Seg_449" n="HIAT:w" s="T119.tx.1">“</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_452" n="HIAT:w" s="T120">İmlʼam</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_455" n="HIAT:w" s="T121">melʼte</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_458" n="HIAT:w" s="T122">nılʼčʼik</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_461" n="HIAT:w" s="T123">orɨnʼnʼa”</ts>
                  <nts id="Seg_462" n="HIAT:ip">.</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_465" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_467" n="HIAT:w" s="T124">Paŋɨtɨ</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_470" n="HIAT:w" s="T125">topɔːqɨntɨ</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_473" n="HIAT:w" s="T126">loːqɨrɨsɨtɨ</ts>
                  <nts id="Seg_474" n="HIAT:ip">.</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_477" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_479" n="HIAT:w" s="T127">“İmlʼa</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_482" n="HIAT:w" s="T128">ınnä</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_485" n="HIAT:w" s="T129">paktäš</ts>
                  <nts id="Seg_486" n="HIAT:ip">,</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_489" n="HIAT:w" s="T130">paŋɨp</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_492" n="HIAT:w" s="T131">ilʼaptɛnta”</ts>
                  <nts id="Seg_493" n="HIAT:ip">.</nts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_496" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_498" n="HIAT:w" s="T132">Nɨnɨ</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_501" n="HIAT:w" s="T133">imlʼatɨ</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_504" n="HIAT:w" s="T134">nɨlleisa</ts>
                  <nts id="Seg_505" n="HIAT:ip">,</nts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_508" n="HIAT:w" s="T135">känpɨlä</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_511" n="HIAT:w" s="T136">orqɨlsɨtɨ</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_514" n="HIAT:w" s="T137">čʼajniktɨ</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_517" n="HIAT:w" s="T138">musɨrɨqo</ts>
                  <nts id="Seg_518" n="HIAT:ip">.</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_521" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_523" n="HIAT:w" s="T139">Kansap</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_526" n="HIAT:w" s="T140">naqqɨlʼčʼat</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_529" n="HIAT:w" s="T141">kuntɨ</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_532" n="HIAT:w" s="T142">čʼajnik</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_535" n="HIAT:w" s="T143">musejsa</ts>
                  <nts id="Seg_536" n="HIAT:ip">.</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_539" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_541" n="HIAT:w" s="T144">Nʼanʼɨm</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_544" n="HIAT:w" s="T145">apsɨp</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_547" n="HIAT:w" s="T146">tottɨsɨtɨ</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_550" n="HIAT:w" s="T147">lʼemtɨ</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_553" n="HIAT:w" s="T148">känpɨlä</ts>
                  <nts id="Seg_554" n="HIAT:ip">.</nts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_557" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_559" n="HIAT:w" s="T149">Kazat</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_562" n="HIAT:w" s="T150">amɨrqolapsɔːtɨt</ts>
                  <nts id="Seg_563" n="HIAT:ip">.</nts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_566" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_568" n="HIAT:w" s="T151">Täpɨt</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_571" n="HIAT:w" s="T152">nılʼčʼik</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153.tx.1" id="Seg_574" n="HIAT:w" s="T153">kätɨsɔːtɨt</ts>
                  <nts id="Seg_575" n="HIAT:ip">:</nts>
                  <ts e="T154" id="Seg_577" n="HIAT:w" s="T153.tx.1">“</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_580" n="HIAT:w" s="T154">Me</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_583" n="HIAT:w" s="T155">tat</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_586" n="HIAT:w" s="T156">paŋɨp</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_589" n="HIAT:w" s="T157">täːmɛntɔːmɨt</ts>
                  <nts id="Seg_590" n="HIAT:ip">.</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_593" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_595" n="HIAT:w" s="T158">Me</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_598" n="HIAT:w" s="T159">iːmaiːmat</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_601" n="HIAT:w" s="T160">lʼentʼajtɔːtɨt</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_604" n="HIAT:w" s="T161">i</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_607" n="HIAT:w" s="T162">quntɔːtɨt”</ts>
                  <nts id="Seg_608" n="HIAT:ip">.</nts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_611" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_613" n="HIAT:w" s="T163">Kazat</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_616" n="HIAT:w" s="T164">asä</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_619" n="HIAT:w" s="T165">laqɨrqolapsɔːtɨt</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_622" n="HIAT:w" s="T166">İčʼakäčʼikap</ts>
                  <nts id="Seg_623" n="HIAT:ip">,</nts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_626" n="HIAT:w" s="T167">kätɛntɔːtɨt</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_628" n="HIAT:ip">(</nts>
                  <nts id="Seg_629" n="HIAT:ip">/</nts>
                  <ts e="T169" id="Seg_631" n="HIAT:w" s="T168">kätɨsɔːtɨt</ts>
                  <nts id="Seg_632" n="HIAT:ip">)</nts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_635" n="HIAT:w" s="T169">Qolʼsak</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_638" n="HIAT:w" s="T170">iːranɨk</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_641" n="HIAT:w" s="T171">qɨssɔːtɨt</ts>
                  <nts id="Seg_642" n="HIAT:ip">.</nts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_645" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_647" n="HIAT:w" s="T172">No</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_650" n="HIAT:w" s="T173">kätɨmpɔːtɨt</ts>
                  <nts id="Seg_651" n="HIAT:ip">.</nts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_654" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_656" n="HIAT:w" s="T174">Načʼalʼnik</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_659" n="HIAT:w" s="T175">kuralʼtɨstɨ</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_662" n="HIAT:w" s="T176">tämɨqo</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_665" n="HIAT:w" s="T177">paŋɨp</ts>
                  <nts id="Seg_666" n="HIAT:ip">.</nts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_669" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_671" n="HIAT:w" s="T178">Sintelɨl</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_674" n="HIAT:w" s="T179">čʼeːlʼ</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_677" n="HIAT:w" s="T180">täːmɔːtɨt</ts>
                  <nts id="Seg_678" n="HIAT:ip">.</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_681" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_683" n="HIAT:w" s="T181">Qolʼsaq</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_686" n="HIAT:w" s="T182">ira</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183.tx.1" id="Seg_689" n="HIAT:w" s="T183">kättɨsɨtɨ</ts>
                  <nts id="Seg_690" n="HIAT:ip">:</nts>
                  <ts e="T184" id="Seg_692" n="HIAT:w" s="T183.tx.1">“</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_695" n="HIAT:w" s="T184">Paŋɨp</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_698" n="HIAT:w" s="T185">makke</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_701" n="HIAT:w" s="T186">miŋelɨt</ts>
                  <nts id="Seg_702" n="HIAT:ip">,</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_705" n="HIAT:w" s="T187">mat</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_708" n="HIAT:w" s="T188">sʼaqɨlʼtɛntap</ts>
                  <nts id="Seg_709" n="HIAT:ip">.</nts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_712" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_714" n="HIAT:w" s="T189">Ma</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_717" n="HIAT:w" s="T190">imam</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_720" n="HIAT:w" s="T191">i</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_723" n="HIAT:w" s="T192">rapotnikit</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_726" n="HIAT:w" s="T193">lʼentʼajtɔːtɨt</ts>
                  <nts id="Seg_727" n="HIAT:ip">.</nts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_730" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_732" n="HIAT:w" s="T194">Čʼeːlʼit</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_735" n="HIAT:w" s="T195">i</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_738" n="HIAT:w" s="T196">piːt</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_741" n="HIAT:w" s="T197">qontɔːtɨt”</ts>
                  <nts id="Seg_742" n="HIAT:ip">.</nts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_745" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_747" n="HIAT:w" s="T198">İčʼakäčʼika</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_750" n="HIAT:w" s="T199">kätɨmpatɨ</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_752" n="HIAT:ip">(</nts>
                  <ts e="T200.tx.1" id="Seg_754" n="HIAT:w" s="T200">katɨsɨt</ts>
                  <nts id="Seg_755" n="HIAT:ip">)</nts>
                  <nts id="Seg_756" n="HIAT:ip">:</nts>
                  <ts e="T201" id="Seg_758" n="HIAT:w" s="T200.tx.1">“</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_761" n="HIAT:w" s="T201">Paŋɨp</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_764" n="HIAT:w" s="T202">čʼistaŋ</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_767" n="HIAT:w" s="T203">märɨŋɨlʼɨt</ts>
                  <nts id="Seg_768" n="HIAT:ip">.</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_771" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_773" n="HIAT:w" s="T204">Soma</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_776" n="HIAT:w" s="T205">qumɨp</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_779" n="HIAT:w" s="T206">matɨrqo”</ts>
                  <nts id="Seg_780" n="HIAT:ip">.</nts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_783" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_785" n="HIAT:w" s="T207">Qolʼsaq</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_788" n="HIAT:w" s="T208">ira</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_791" n="HIAT:w" s="T209">qontɨsa</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_794" n="HIAT:w" s="T210">qarɨt</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_797" n="HIAT:w" s="T211">čʼeŋ</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_800" n="HIAT:w" s="T212">omtɨsa</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_802" n="HIAT:ip">(</nts>
                  <nts id="Seg_803" n="HIAT:ip">/</nts>
                  <ts e="T214" id="Seg_805" n="HIAT:w" s="T213">mäsɨsa</ts>
                  <nts id="Seg_806" n="HIAT:ip">)</nts>
                  <nts id="Seg_807" n="HIAT:ip">.</nts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218.tx.1" id="Seg_810" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_812" n="HIAT:w" s="T214">“İma</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_815" n="HIAT:w" s="T215">omtäšik</ts>
                  <nts id="Seg_816" n="HIAT:ip">,</nts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_819" n="HIAT:w" s="T216">i</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_822" n="HIAT:w" s="T217">rapotnikit</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218.tx.1" id="Seg_825" n="HIAT:w" s="T218">omtäŋɨlʼɨt</ts>
                  <nts id="Seg_826" n="HIAT:ip">!</nts>
               </ts>
               <ts e="T223" id="Seg_828" n="HIAT:u" s="T218.tx.1">
                  <ts e="T219" id="Seg_830" n="HIAT:w" s="T218.tx.1">”</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_833" n="HIAT:w" s="T219">Täpɨt</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_836" n="HIAT:w" s="T220">čʼek</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_839" n="HIAT:w" s="T221">assa</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_842" n="HIAT:w" s="T222">omnäntɔːtɨt</ts>
                  <nts id="Seg_843" n="HIAT:ip">.</nts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_846" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_848" n="HIAT:w" s="T223">Nʼenʼnʼimɔːssa</ts>
                  <nts id="Seg_849" n="HIAT:ip">.</nts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_852" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_854" n="HIAT:w" s="T224">Illa</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_857" n="HIAT:w" s="T225">iːsɨtɨ</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_860" n="HIAT:w" s="T226">paŋamt</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_863" n="HIAT:w" s="T227">moːtälsɨtɨ</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_866" n="HIAT:w" s="T228">muntɨk</ts>
                  <nts id="Seg_867" n="HIAT:ip">.</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_870" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_872" n="HIAT:w" s="T229">Paŋɨp</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_875" n="HIAT:w" s="T230">loːqɨrsɨtɨ</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_878" n="HIAT:w" s="T231">topɔːqɨntɨ</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_881" n="HIAT:w" s="T232">muntɨk</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_884" n="HIAT:w" s="T233">mattɨrpɨlʼ</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_887" n="HIAT:w" s="T234">qumiːqantɨ</ts>
                  <nts id="Seg_888" n="HIAT:ip">.</nts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_891" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_893" n="HIAT:w" s="T235">Čʼap</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_896" n="HIAT:w" s="T236">kättɨqɨtɨ</ts>
                  <nts id="Seg_897" n="HIAT:ip">.</nts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_900" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_902" n="HIAT:w" s="T237">Melʼte</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_905" n="HIAT:w" s="T238">qulʼčʼɔːtɨt</ts>
                  <nts id="Seg_906" n="HIAT:ip">.</nts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_909" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_911" n="HIAT:w" s="T239">İra</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_914" n="HIAT:w" s="T240">nʼenʼnʼmɔːssa</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_917" n="HIAT:w" s="T241">kazatɨp</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_920" n="HIAT:w" s="T242">qärɨsɨt</ts>
                  <nts id="Seg_921" n="HIAT:ip">.</nts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245.tx.1" id="Seg_924" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_926" n="HIAT:w" s="T243">“Kun</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_929" n="HIAT:w" s="T244">iːsaqɨt</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245.tx.1" id="Seg_932" n="HIAT:w" s="T245">paŋɨp</ts>
                  <nts id="Seg_933" n="HIAT:ip">?</nts>
               </ts>
               <ts e="T251" id="Seg_935" n="HIAT:u" s="T245.tx.1">
                  <ts e="T246" id="Seg_937" n="HIAT:w" s="T245.tx.1">”</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_940" n="HIAT:w" s="T246">“Täp</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_942" n="HIAT:ip">(</nts>
                  <ts e="T248" id="Seg_944" n="HIAT:w" s="T247">İčʼäk</ts>
                  <nts id="Seg_945" n="HIAT:ip">)</nts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_948" n="HIAT:w" s="T248">mekɨnı</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_951" n="HIAT:w" s="T249">paŋɨp</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_954" n="HIAT:w" s="T250">misɨtɨ</ts>
                  <nts id="Seg_955" n="HIAT:ip">.</nts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_958" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_960" n="HIAT:w" s="T251">Täp</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_963" n="HIAT:w" s="T252">İčʼäkäːčʼika</ts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_966" n="HIAT:w" s="T253">ɔːläksa”</ts>
                  <nts id="Seg_967" n="HIAT:ip">.</nts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T257" id="Seg_970" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_972" n="HIAT:w" s="T254">“Qälʼlʼa</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_975" n="HIAT:w" s="T255">orqɨlʼqo</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_978" n="HIAT:w" s="T256">sudʼinqo</ts>
                  <nts id="Seg_979" n="HIAT:ip">.</nts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_982" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_984" n="HIAT:w" s="T257">İmaiːmɨt</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_986" n="HIAT:ip">(</nts>
                  <ts e="T259" id="Seg_988" n="HIAT:w" s="T258">imam</ts>
                  <nts id="Seg_989" n="HIAT:ip">,</nts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_992" n="HIAT:w" s="T259">iːmat</ts>
                  <nts id="Seg_993" n="HIAT:ip">)</nts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_996" n="HIAT:w" s="T260">muntɨk</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_999" n="HIAT:w" s="T261">qusɔːtɨt</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1002" n="HIAT:w" s="T262">paŋɨn</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1005" n="HIAT:w" s="T263">nɔːn”</ts>
                  <nts id="Seg_1006" n="HIAT:ip">.</nts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T269" id="Seg_1009" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_1011" n="HIAT:w" s="T264">“Tat</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1014" n="HIAT:w" s="T265">onnäka</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1017" n="HIAT:w" s="T266">asa</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1020" n="HIAT:w" s="T267">tɔːtɨk</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1023" n="HIAT:w" s="T268">mattɨrsa</ts>
                  <nts id="Seg_1024" n="HIAT:ip">.</nts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_1027" n="HIAT:u" s="T269">
                  <ts e="T270" id="Seg_1029" n="HIAT:w" s="T269">Täpɨp</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1032" n="HIAT:w" s="T270">qäːčʼiqo</ts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1035" n="HIAT:w" s="T271">okkur</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1038" n="HIAT:w" s="T272">čʼeːl</ts>
                  <nts id="Seg_1039" n="HIAT:ip">,</nts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1042" n="HIAT:w" s="T273">meː</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1045" n="HIAT:w" s="T274">aj</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1048" n="HIAT:w" s="T275">saŋartɔːmɨt</ts>
                  <nts id="Seg_1049" n="HIAT:ip">.</nts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_1052" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_1054" n="HIAT:w" s="T276">Me</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1057" n="HIAT:w" s="T277">iːmaiːmɨt</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1060" n="HIAT:w" s="T278">aj</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1063" n="HIAT:w" s="T279">lʼentʼajtɔːtɨt”</ts>
                  <nts id="Seg_1064" n="HIAT:ip">.</nts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286.tx.1" id="Seg_1067" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_1069" n="HIAT:w" s="T280">Qolʼsaq</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1072" n="HIAT:w" s="T281">ira</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1075" n="HIAT:w" s="T282">aj</ts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283.tx.1" id="Seg_1078" n="HIAT:w" s="T283">kätɨsɨtɨ</ts>
                  <nts id="Seg_1079" n="HIAT:ip">:</nts>
                  <ts e="T284" id="Seg_1081" n="HIAT:w" s="T283.tx.1">“</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1084" n="HIAT:w" s="T284">Lanno</ts>
                  <nts id="Seg_1085" n="HIAT:ip">,</nts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1088" n="HIAT:w" s="T285">saŋarŋɨlɨt</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286.tx.1" id="Seg_1091" n="HIAT:w" s="T286">paŋɨp</ts>
                  <nts id="Seg_1092" n="HIAT:ip">!</nts>
               </ts>
               <ts e="T293" id="Seg_1094" n="HIAT:u" s="T286.tx.1">
                  <ts e="T287" id="Seg_1096" n="HIAT:w" s="T286.tx.1">”</ts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1099" n="HIAT:w" s="T287">Kazat</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1102" n="HIAT:w" s="T288">šität</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1105" n="HIAT:w" s="T289">nılʼčʼik</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1108" n="HIAT:w" s="T290">mattälsɨtɨ</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1111" n="HIAT:w" s="T291">onti</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1114" n="HIAT:w" s="T292">imaiːt</ts>
                  <nts id="Seg_1115" n="HIAT:ip">.</nts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1118" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1120" n="HIAT:w" s="T293">İːmaiːt</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1123" n="HIAT:w" s="T294">qusɔːtɨt</ts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1126" n="HIAT:w" s="T295">kämɨt</ts>
                  <nts id="Seg_1127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1129" n="HIAT:w" s="T296">čʼɔːrɨk</ts>
                  <nts id="Seg_1130" n="HIAT:ip">.</nts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_1133" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1135" n="HIAT:w" s="T297">Qarɨt</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1138" n="HIAT:w" s="T298">kätɨsɔːtɨt</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1141" n="HIAT:w" s="T299">Qolʼsaq</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300.tx.1" id="Seg_1144" n="HIAT:w" s="T300">iranɨk</ts>
                  <nts id="Seg_1145" n="HIAT:ip">:</nts>
                  <ts e="T301" id="Seg_1147" n="HIAT:w" s="T300.tx.1">“</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1150" n="HIAT:w" s="T301">Meː</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1153" n="HIAT:w" s="T302">imaiːmɨt</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1156" n="HIAT:w" s="T303">melʼte</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1159" n="HIAT:w" s="T304">qusɔːtɨt”</ts>
                  <nts id="Seg_1160" n="HIAT:ip">.</nts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1163" n="HIAT:u" s="T305">
                  <ts e="T306" id="Seg_1165" n="HIAT:w" s="T305">İra</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1168" n="HIAT:w" s="T306">nʼenʼnʼamɔːssa</ts>
                  <nts id="Seg_1169" n="HIAT:ip">.</nts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T310" id="Seg_1172" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_1174" n="HIAT:w" s="T307">“İčʼakäčʼikap</ts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1177" n="HIAT:w" s="T308">orqɨlʼqo</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1180" n="HIAT:w" s="T309">nɔːtna”</ts>
                  <nts id="Seg_1181" n="HIAT:ip">.</nts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1184" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_1186" n="HIAT:w" s="T310">Kazat</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1189" n="HIAT:w" s="T311">qässɔːtɨt</ts>
                  <nts id="Seg_1190" n="HIAT:ip">.</nts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_1193" n="HIAT:u" s="T312">
                  <ts e="T313" id="Seg_1195" n="HIAT:w" s="T312">İčʼakäčʼika</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1198" n="HIAT:w" s="T313">na</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1201" n="HIAT:w" s="T314">vremʼaqɨt</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1204" n="HIAT:w" s="T315">mɔːtqɨntɨ</ts>
                  <nts id="Seg_1205" n="HIAT:ip">.</nts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T318" id="Seg_1208" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_1210" n="HIAT:w" s="T316">İčʼakäčʼikap</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1213" n="HIAT:w" s="T317">orqɨlʼsɔːtɨt</ts>
                  <nts id="Seg_1214" n="HIAT:ip">.</nts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T320" id="Seg_1217" n="HIAT:u" s="T318">
                  <ts e="T319" id="Seg_1219" n="HIAT:w" s="T318">“Seːp</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1222" n="HIAT:w" s="T319">moːlmɨttɨsantɨ</ts>
                  <nts id="Seg_1223" n="HIAT:ip">.</nts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T330" id="Seg_1226" n="HIAT:u" s="T320">
                  <ts e="T321" id="Seg_1228" n="HIAT:w" s="T320">Me</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1231" n="HIAT:w" s="T321">muntɨk</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1234" n="HIAT:w" s="T322">iːmaiːmɨt</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1237" n="HIAT:w" s="T323">i</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1240" n="HIAT:w" s="T324">rapotnikit</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1243" n="HIAT:w" s="T325">mattälsimɨt</ts>
                  <nts id="Seg_1244" n="HIAT:ip">,</nts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1247" n="HIAT:w" s="T326">täpɨt</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1250" n="HIAT:w" s="T327">qusɔːtɨt</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1253" n="HIAT:w" s="T328">kämɨt</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1256" n="HIAT:w" s="T329">čʼɔːrɨk”</ts>
                  <nts id="Seg_1257" n="HIAT:ip">.</nts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_1260" n="HIAT:u" s="T330">
                  <ts e="T331" id="Seg_1262" n="HIAT:w" s="T330">İčʼakäčʼikap</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1265" n="HIAT:w" s="T331">qäntɨsɔːtɨt</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1268" n="HIAT:w" s="T332">türmantɨ</ts>
                  <nts id="Seg_1269" n="HIAT:ip">.</nts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T339" id="Seg_1272" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_1274" n="HIAT:w" s="T333">Kolʼsaq</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1277" n="HIAT:w" s="T334">ira</ts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1280" n="HIAT:w" s="T335">kuraltistɨ</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1283" n="HIAT:w" s="T336">ıllä</ts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1286" n="HIAT:w" s="T337">qottɨrɨqo</ts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1289" n="HIAT:w" s="T338">üttɨ</ts>
                  <nts id="Seg_1290" n="HIAT:ip">.</nts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T345" id="Seg_1293" n="HIAT:u" s="T339">
                  <ts e="T340" id="Seg_1295" n="HIAT:w" s="T339">Kätɨsɨtɨ</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1298" n="HIAT:w" s="T340">sɨːrɨt</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1301" n="HIAT:w" s="T341">qopoːntɨ</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1304" n="HIAT:w" s="T342">šünʼnʼontɨ</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1307" n="HIAT:w" s="T343">šütqo</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1310" n="HIAT:w" s="T344">ilʼelä</ts>
                  <nts id="Seg_1311" n="HIAT:ip">.</nts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T354" id="Seg_1314" n="HIAT:u" s="T345">
                  <ts e="T346" id="Seg_1316" n="HIAT:w" s="T345">Kazat</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1319" n="HIAT:w" s="T346">täpɨp</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1322" n="HIAT:w" s="T347">qäntɨsɔːtɨt</ts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1325" n="HIAT:w" s="T348">čʼuntɨlʼ</ts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1328" n="HIAT:w" s="T349">qaqlɨsä</ts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1331" n="HIAT:w" s="T350">üt</ts>
                  <nts id="Seg_1332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1334" n="HIAT:w" s="T351">toːptɨ</ts>
                  <nts id="Seg_1335" n="HIAT:ip">,</nts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1338" n="HIAT:w" s="T352">moret</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1341" n="HIAT:w" s="T353">qanɨktɨ</ts>
                  <nts id="Seg_1342" n="HIAT:ip">.</nts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1345" n="HIAT:u" s="T354">
                  <ts e="T355" id="Seg_1347" n="HIAT:w" s="T354">Täp</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1350" n="HIAT:w" s="T355">kota</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1353" n="HIAT:w" s="T356">čʼuntoːqɨt</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1356" n="HIAT:w" s="T357">ilɨla</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1359" n="HIAT:w" s="T358">ippa</ts>
                  <nts id="Seg_1360" n="HIAT:ip">.</nts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T367" id="Seg_1363" n="HIAT:u" s="T359">
                  <ts e="T359.tx.1" id="Seg_1365" n="HIAT:w" s="T359">Kätɨsɨtɨ</ts>
                  <nts id="Seg_1366" n="HIAT:ip">:</nts>
                  <ts e="T360" id="Seg_1368" n="HIAT:w" s="T359.tx.1">“</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1371" n="HIAT:w" s="T360">Masɨp</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1374" n="HIAT:w" s="T361">qoptɨrantɨlʼɨt</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1377" n="HIAT:w" s="T362">marqɨ</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1380" n="HIAT:w" s="T363">püsa</ts>
                  <nts id="Seg_1381" n="HIAT:ip">,</nts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1384" n="HIAT:w" s="T364">marqɨ</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1387" n="HIAT:w" s="T365">pü</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1390" n="HIAT:w" s="T366">peːqɨlʼnɨkɨt”</ts>
                  <nts id="Seg_1391" n="HIAT:ip">.</nts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T372" id="Seg_1394" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_1396" n="HIAT:w" s="T367">Täpɨt</ts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1399" n="HIAT:w" s="T368">qässɔːtɨt</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1402" n="HIAT:w" s="T369">peːqo</ts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1405" n="HIAT:w" s="T370">marqɨ</ts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1408" n="HIAT:w" s="T371">püp</ts>
                  <nts id="Seg_1409" n="HIAT:ip">.</nts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T380" id="Seg_1412" n="HIAT:u" s="T372">
                  <ts e="T373" id="Seg_1414" n="HIAT:w" s="T372">Kuntɨ</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1417" n="HIAT:w" s="T373">lʼi</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1420" n="HIAT:w" s="T374">qɔːmɨčʼa</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1423" n="HIAT:w" s="T375">ippɨmpa</ts>
                  <nts id="Seg_1424" n="HIAT:ip">,</nts>
                  <nts id="Seg_1425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1427" n="HIAT:w" s="T376">üŋkeltɨmpat</ts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1430" n="HIAT:w" s="T377">čʼuntɨlʼ</ts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1433" n="HIAT:w" s="T378">qaqlɨ</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1436" n="HIAT:w" s="T379">tünta</ts>
                  <nts id="Seg_1437" n="HIAT:ip">.</nts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_1440" n="HIAT:u" s="T380">
                  <ts e="T381" id="Seg_1442" n="HIAT:w" s="T380">Tɛːttɨ</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1445" n="HIAT:w" s="T381">lʼi</ts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1448" n="HIAT:w" s="T382">sompɨlʼa</ts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1451" n="HIAT:w" s="T383">qaqlɨt</ts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1454" n="HIAT:w" s="T384">tüːnta</ts>
                  <nts id="Seg_1455" n="HIAT:ip">.</nts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T386" id="Seg_1458" n="HIAT:u" s="T385">
                  <ts e="T386" id="Seg_1460" n="HIAT:w" s="T385">Utɨrɨsɔːtɨt</ts>
                  <nts id="Seg_1461" n="HIAT:ip">.</nts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1464" n="HIAT:u" s="T386">
                  <ts e="T387" id="Seg_1466" n="HIAT:w" s="T386">Tämqup</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1469" n="HIAT:w" s="T387">tɔːqsä</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1471" n="HIAT:ip">(</nts>
                  <ts e="T389" id="Seg_1473" n="HIAT:w" s="T388">tawarsä</ts>
                  <nts id="Seg_1474" n="HIAT:ip">,</nts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1477" n="HIAT:w" s="T389">lɨptɨksä</ts>
                  <nts id="Seg_1478" n="HIAT:ip">)</nts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1481" n="HIAT:w" s="T390">qänta</ts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1484" n="HIAT:w" s="T391">moqɨnä</ts>
                  <nts id="Seg_1485" n="HIAT:ip">.</nts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T397" id="Seg_1488" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1490" n="HIAT:w" s="T392">Tämqup</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1493" n="HIAT:w" s="T393">topɔːlsɨtɨ</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1496" n="HIAT:w" s="T394">sɨːrɨt</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1499" n="HIAT:w" s="T395">qopɨlʼ</ts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1502" n="HIAT:w" s="T396">kotap</ts>
                  <nts id="Seg_1503" n="HIAT:ip">.</nts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T400" id="Seg_1506" n="HIAT:u" s="T397">
                  <ts e="T398" id="Seg_1508" n="HIAT:w" s="T397">Qoi</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1511" n="HIAT:w" s="T398">tɨmtɨ</ts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1514" n="HIAT:w" s="T399">ippa</ts>
                  <nts id="Seg_1515" n="HIAT:ip">?</nts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T402.tx.1" id="Seg_1518" n="HIAT:u" s="T400">
                  <ts e="T401" id="Seg_1520" n="HIAT:w" s="T400">İčʼikäčʼika</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401.tx.1" id="Seg_1523" n="HIAT:w" s="T401">laŋkalʼsa</ts>
                  <nts id="Seg_1524" n="HIAT:ip">:</nts>
                  <ts e="T402" id="Seg_1526" n="HIAT:w" s="T401.tx.1">“</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402.tx.1" id="Seg_1529" n="HIAT:w" s="T402">İjoː</ts>
                  <nts id="Seg_1530" n="HIAT:ip">!</nts>
               </ts>
               <ts e="T405.tx.1" id="Seg_1532" n="HIAT:u" s="T402.tx.1">
                  <ts e="T403" id="Seg_1534" n="HIAT:w" s="T402.tx.1">”</ts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403.tx.1" id="Seg_1537" n="HIAT:w" s="T403">Tämqup</ts>
                  <nts id="Seg_1538" n="HIAT:ip">:</nts>
                  <ts e="T404" id="Seg_1540" n="HIAT:w" s="T403.tx.1">“</ts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1543" n="HIAT:w" s="T404">Qoitqo</ts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405.tx.1" id="Seg_1546" n="HIAT:w" s="T405">ippant</ts>
                  <nts id="Seg_1547" n="HIAT:ip">?</nts>
               </ts>
               <ts e="T412.tx.1" id="Seg_1549" n="HIAT:u" s="T405.tx.1">
                  <ts e="T406" id="Seg_1551" n="HIAT:w" s="T405.tx.1">”</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1554" n="HIAT:w" s="T406">İčʼakäčʼika</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407.tx.1" id="Seg_1557" n="HIAT:w" s="T407">kätɨsɨtɨ</ts>
                  <nts id="Seg_1558" n="HIAT:ip">:</nts>
                  <ts e="T408" id="Seg_1560" n="HIAT:w" s="T407.tx.1">“</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1563" n="HIAT:w" s="T408">Innä</ts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1566" n="HIAT:w" s="T409">sım</ts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1569" n="HIAT:w" s="T410">üːtas</ts>
                  <nts id="Seg_1570" n="HIAT:ip">,</nts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1573" n="HIAT:w" s="T411">toː</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412.tx.1" id="Seg_1576" n="HIAT:w" s="T412">maːtaltɨ</ts>
                  <nts id="Seg_1577" n="HIAT:ip">!</nts>
               </ts>
               <ts e="T416" id="Seg_1579" n="HIAT:u" s="T412.tx.1">
                  <ts e="T413" id="Seg_1581" n="HIAT:w" s="T412.tx.1">”</ts>
                  <nts id="Seg_1582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1584" n="HIAT:w" s="T413">Täp</ts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1587" n="HIAT:w" s="T414">toː</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1590" n="HIAT:w" s="T415">maːtalʼolʼsɨt</ts>
                  <nts id="Seg_1591" n="HIAT:ip">.</nts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T420" id="Seg_1594" n="HIAT:u" s="T416">
                  <ts e="T417" id="Seg_1596" n="HIAT:w" s="T416">Täp</ts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1599" n="HIAT:w" s="T417">ınnä</ts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1602" n="HIAT:w" s="T418">putalʼmɔːssa</ts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1604" n="HIAT:ip">(</nts>
                  <ts e="T420" id="Seg_1606" n="HIAT:w" s="T419">tantɨsa</ts>
                  <nts id="Seg_1607" n="HIAT:ip">)</nts>
                  <nts id="Seg_1608" n="HIAT:ip">.</nts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T427" id="Seg_1611" n="HIAT:u" s="T420">
                  <ts e="T421" id="Seg_1613" n="HIAT:w" s="T420">Täpɨn</ts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1616" n="HIAT:w" s="T421">mɨqɨt</ts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1619" n="HIAT:w" s="T422">sıːt</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1622" n="HIAT:w" s="T423">qopɨlʼ</ts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1625" n="HIAT:w" s="T424">mɨ</ts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1628" n="HIAT:w" s="T425">ɛppa</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1631" n="HIAT:w" s="T426">muŋkɨntɨ</ts>
                  <nts id="Seg_1632" n="HIAT:ip">.</nts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T430.tx.1" id="Seg_1635" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_1637" n="HIAT:w" s="T427">Tämqup</ts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428.tx.1" id="Seg_1640" n="HIAT:w" s="T428">soqonʼnʼet</ts>
                  <nts id="Seg_1641" n="HIAT:ip">:</nts>
                  <ts e="T429" id="Seg_1643" n="HIAT:w" s="T428.tx.1">“</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1646" n="HIAT:w" s="T429">Qojtqo</ts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430.tx.1" id="Seg_1649" n="HIAT:w" s="T430">ippantɨ</ts>
                  <nts id="Seg_1650" n="HIAT:ip">?</nts>
               </ts>
               <ts e="T438" id="Seg_1652" n="HIAT:u" s="T430.tx.1">
                  <ts e="T431" id="Seg_1654" n="HIAT:w" s="T430.tx.1">”</ts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1657" n="HIAT:w" s="T431">“Mat</ts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1660" n="HIAT:w" s="T432">ippap</ts>
                  <nts id="Seg_1661" n="HIAT:ip">,</nts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1664" n="HIAT:w" s="T433">üttɨ</ts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1667" n="HIAT:w" s="T434">pattɨrsak</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1670" n="HIAT:w" s="T435">sıːt</ts>
                  <nts id="Seg_1671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1673" n="HIAT:w" s="T436">qopɨp</ts>
                  <nts id="Seg_1674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1676" n="HIAT:w" s="T437">iːsap</ts>
                  <nts id="Seg_1677" n="HIAT:ip">.</nts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_1680" n="HIAT:u" s="T438">
                  <ts e="T439" id="Seg_1682" n="HIAT:w" s="T438">Ormɨ</ts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1685" n="HIAT:w" s="T439">čʼäːŋkɨsa</ts>
                  <nts id="Seg_1686" n="HIAT:ip">,</nts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1689" n="HIAT:w" s="T440">tɔːq</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1691" n="HIAT:ip">(</nts>
                  <ts e="T442" id="Seg_1693" n="HIAT:w" s="T441">lɨptɨk</ts>
                  <nts id="Seg_1694" n="HIAT:ip">)</nts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1697" n="HIAT:w" s="T442">kočʼčʼɨ</ts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1700" n="HIAT:w" s="T443">ütqɨt</ts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1703" n="HIAT:w" s="T444">ınnä”</ts>
                  <nts id="Seg_1704" n="HIAT:ip">.</nts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T449" id="Seg_1707" n="HIAT:u" s="T445">
                  <ts e="T446" id="Seg_1709" n="HIAT:w" s="T445">Atɨlʼsitɨ</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1712" n="HIAT:w" s="T446">sıːt</ts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1715" n="HIAT:w" s="T447">qopɨtɨp</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1718" n="HIAT:w" s="T448">tämqumnɨk</ts>
                  <nts id="Seg_1719" n="HIAT:ip">.</nts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T455" id="Seg_1722" n="HIAT:u" s="T449">
                  <ts e="T450" id="Seg_1724" n="HIAT:w" s="T449">Tämqum</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450.tx.1" id="Seg_1727" n="HIAT:w" s="T450">ɔːntalʼsa</ts>
                  <nts id="Seg_1728" n="HIAT:ip">:</nts>
                  <ts e="T451" id="Seg_1730" n="HIAT:w" s="T450.tx.1">“</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1733" n="HIAT:w" s="T451">Ma</ts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1736" n="HIAT:w" s="T452">aj</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1739" n="HIAT:w" s="T453">pattɨrläk</ts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1742" n="HIAT:w" s="T454">ütt”</ts>
                  <nts id="Seg_1743" n="HIAT:ip">.</nts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T459" id="Seg_1746" n="HIAT:u" s="T455">
                  <ts e="T456" id="Seg_1748" n="HIAT:w" s="T455">Täpɨp</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1751" n="HIAT:w" s="T456">süttɛːsitɨ</ts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1754" n="HIAT:w" s="T457">sɨːrɨt</ts>
                  <nts id="Seg_1755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1757" n="HIAT:w" s="T458">kotantɨ</ts>
                  <nts id="Seg_1758" n="HIAT:ip">.</nts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T463" id="Seg_1761" n="HIAT:u" s="T459">
                  <ts e="T460" id="Seg_1763" n="HIAT:w" s="T459">Tämqup</ts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1766" n="HIAT:w" s="T460">ippa</ts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1769" n="HIAT:w" s="T461">üttotqɨp</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1772" n="HIAT:w" s="T462">kotaqɨt</ts>
                  <nts id="Seg_1773" n="HIAT:ip">.</nts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T469" id="Seg_1776" n="HIAT:u" s="T463">
                  <ts e="T464" id="Seg_1778" n="HIAT:w" s="T463">İčʼakäčʼika</ts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1781" n="HIAT:w" s="T464">qənnajsa</ts>
                  <nts id="Seg_1782" n="HIAT:ip">,</nts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1785" n="HIAT:w" s="T465">čʼuntɨlʼ</ts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1788" n="HIAT:w" s="T466">qaqlɨntɨsa</ts>
                  <nts id="Seg_1789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1791" n="HIAT:w" s="T467">i</ts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1794" n="HIAT:w" s="T468">tɔːqsa</ts>
                  <nts id="Seg_1795" n="HIAT:ip">.</nts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T474" id="Seg_1798" n="HIAT:u" s="T469">
                  <ts e="T470" id="Seg_1800" n="HIAT:w" s="T469">Kazat</ts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1803" n="HIAT:w" s="T470">taːtɨsɔːtɨt</ts>
                  <nts id="Seg_1804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1806" n="HIAT:w" s="T471">marqɨ</ts>
                  <nts id="Seg_1807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1809" n="HIAT:w" s="T472">püp</ts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1812" n="HIAT:w" s="T473">sɔːrɨqolapsɔːtɨt</ts>
                  <nts id="Seg_1813" n="HIAT:ip">.</nts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T480" id="Seg_1816" n="HIAT:u" s="T474">
                  <ts e="T475" id="Seg_1818" n="HIAT:w" s="T474">Täntɔːtɨt</ts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1821" n="HIAT:w" s="T475">ontɨ</ts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476.tx.1" id="Seg_1824" n="HIAT:w" s="T476">nʼantɨ</ts>
                  <nts id="Seg_1825" n="HIAT:ip">:</nts>
                  <ts e="T477" id="Seg_1827" n="HIAT:w" s="T476.tx.1">“</ts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1830" n="HIAT:w" s="T477">Seːp</ts>
                  <nts id="Seg_1831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1833" n="HIAT:w" s="T478">İčʼakäčʼika</ts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1836" n="HIAT:w" s="T479">olɨŋɨrsal</ts>
                  <nts id="Seg_1837" n="HIAT:ip">.</nts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T482" id="Seg_1840" n="HIAT:u" s="T480">
                  <ts e="T481" id="Seg_1842" n="HIAT:w" s="T480">Ütqɨt</ts>
                  <nts id="Seg_1843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1845" n="HIAT:w" s="T481">qunnant”</ts>
                  <nts id="Seg_1846" n="HIAT:ip">.</nts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T488" id="Seg_1849" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_1851" n="HIAT:w" s="T482">Tämqup</ts>
                  <nts id="Seg_1852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1854" n="HIAT:w" s="T483">üŋkeltɨmpat</ts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1857" n="HIAT:w" s="T484">sɔːrɨqantaltɨmpɔːtɨt</ts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1860" n="HIAT:w" s="T485">püp</ts>
                  <nts id="Seg_1861" n="HIAT:ip">,</nts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1864" n="HIAT:w" s="T486">marqɨ</ts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1867" n="HIAT:w" s="T487">püp</ts>
                  <nts id="Seg_1868" n="HIAT:ip">.</nts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T491" id="Seg_1871" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_1873" n="HIAT:w" s="T488">“Asa</ts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1876" n="HIAT:w" s="T489">mat</ts>
                  <nts id="Seg_1877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1879" n="HIAT:w" s="T490">İčʼikäčʼika</ts>
                  <nts id="Seg_1880" n="HIAT:ip">.</nts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T493" id="Seg_1883" n="HIAT:u" s="T491">
                  <ts e="T492" id="Seg_1885" n="HIAT:w" s="T491">İčʼäkäčʼika</ts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1888" n="HIAT:w" s="T492">qəssa</ts>
                  <nts id="Seg_1889" n="HIAT:ip">.</nts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T496" id="Seg_1892" n="HIAT:u" s="T493">
                  <ts e="T494" id="Seg_1894" n="HIAT:w" s="T493">Tap</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1897" n="HIAT:w" s="T494">masım</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1900" n="HIAT:w" s="T495">oːlʼɨksa</ts>
                  <nts id="Seg_1901" n="HIAT:ip">.</nts>
                  <nts id="Seg_1902" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T498" id="Seg_1904" n="HIAT:u" s="T496">
                  <ts e="T497" id="Seg_1906" n="HIAT:w" s="T496">Mat</ts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1909" n="HIAT:w" s="T497">tämqumoŋok</ts>
                  <nts id="Seg_1910" n="HIAT:ip">.</nts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T503" id="Seg_1913" n="HIAT:u" s="T498">
                  <ts e="T499" id="Seg_1915" n="HIAT:w" s="T498">Olʼɨklä</ts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1918" n="HIAT:w" s="T499">masɨp</ts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1921" n="HIAT:w" s="T500">süssa</ts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1924" n="HIAT:w" s="T501">qopɨlʼ</ts>
                  <nts id="Seg_1925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1927" n="HIAT:w" s="T502">kotantɨ”</ts>
                  <nts id="Seg_1928" n="HIAT:ip">.</nts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T509" id="Seg_1931" n="HIAT:u" s="T503">
                  <ts e="T504" id="Seg_1933" n="HIAT:w" s="T503">Kasat</ts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504.tx.1" id="Seg_1936" n="HIAT:w" s="T504">tomnat</ts>
                  <nts id="Seg_1937" n="HIAT:ip">:</nts>
                  <ts e="T505" id="Seg_1939" n="HIAT:w" s="T504.tx.1">“</ts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1942" n="HIAT:w" s="T505">Tat</ts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1945" n="HIAT:w" s="T506">İčʼäkäčʼikantɨ</ts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1948" n="HIAT:w" s="T507">seːp</ts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1951" n="HIAT:w" s="T508">moːlmɨtɨsantɨ”</ts>
                  <nts id="Seg_1952" n="HIAT:ip">.</nts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T515" id="Seg_1955" n="HIAT:u" s="T509">
                  <ts e="T510" id="Seg_1957" n="HIAT:w" s="T509">Toːnna</ts>
                  <nts id="Seg_1958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1960" n="HIAT:w" s="T510">čʼuːrɨla</ts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1963" n="HIAT:w" s="T511">laŋkɨnʼnʼa</ts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512.tx.1" id="Seg_1966" n="HIAT:w" s="T512">kotaqɨt</ts>
                  <nts id="Seg_1967" n="HIAT:ip">:</nts>
                  <ts e="T513" id="Seg_1969" n="HIAT:w" s="T512.tx.1">“</ts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1972" n="HIAT:w" s="T513">Mat</ts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1975" n="HIAT:w" s="T514">tämqumoŋok</ts>
                  <nts id="Seg_1976" n="HIAT:ip">.</nts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T518" id="Seg_1979" n="HIAT:u" s="T515">
                  <ts e="T516" id="Seg_1981" n="HIAT:w" s="T515">Tüntɨsak</ts>
                  <nts id="Seg_1982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1984" n="HIAT:w" s="T516">čʼuntɨsa</ts>
                  <nts id="Seg_1985" n="HIAT:ip">,</nts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1988" n="HIAT:w" s="T517">qaqlɨsa”</ts>
                  <nts id="Seg_1989" n="HIAT:ip">.</nts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T523" id="Seg_1992" n="HIAT:u" s="T518">
                  <ts e="T519" id="Seg_1994" n="HIAT:w" s="T518">Üttɨ</ts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1997" n="HIAT:w" s="T519">tulʼtɨlʼa</ts>
                  <nts id="Seg_1998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2000" n="HIAT:w" s="T520">čʼattɨsɔːtɨt</ts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2003" n="HIAT:w" s="T521">qoptɨlʼsɔːtɨt</ts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2006" n="HIAT:w" s="T522">püːsa</ts>
                  <nts id="Seg_2007" n="HIAT:ip">.</nts>
                  <nts id="Seg_2008" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T525" id="Seg_2010" n="HIAT:u" s="T523">
                  <ts e="T524" id="Seg_2012" n="HIAT:w" s="T523">Moqɨnä</ts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2015" n="HIAT:w" s="T524">qässɔːtɨt</ts>
                  <nts id="Seg_2016" n="HIAT:ip">.</nts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T528" id="Seg_2019" n="HIAT:u" s="T525">
                  <ts e="T526" id="Seg_2021" n="HIAT:w" s="T525">“Meː</ts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2024" n="HIAT:w" s="T526">qoptɨrsɨmɨt</ts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2027" n="HIAT:w" s="T527">üttɨ”</ts>
                  <nts id="Seg_2028" n="HIAT:ip">.</nts>
                  <nts id="Seg_2029" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T534" id="Seg_2031" n="HIAT:u" s="T528">
                  <ts e="T529" id="Seg_2033" n="HIAT:w" s="T528">İčʼikäčʼika</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2036" n="HIAT:w" s="T529">tüsa</ts>
                  <nts id="Seg_2037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2039" n="HIAT:w" s="T530">moqɨna</ts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2042" n="HIAT:w" s="T531">čʼuntɨsa</ts>
                  <nts id="Seg_2043" n="HIAT:ip">,</nts>
                  <nts id="Seg_2044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2046" n="HIAT:w" s="T532">čʼuntɨlʼ</ts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2049" n="HIAT:w" s="T533">qaqlɨsa</ts>
                  <nts id="Seg_2050" n="HIAT:ip">.</nts>
                  <nts id="Seg_2051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T541" id="Seg_2053" n="HIAT:u" s="T534">
                  <ts e="T535" id="Seg_2055" n="HIAT:w" s="T534">Tɔːqtɨ</ts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2058" n="HIAT:w" s="T535">üssä</ts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2061" n="HIAT:w" s="T536">čʼontaptɨstɨ</ts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2064" n="HIAT:w" s="T537">tɛːmnoːntɨ</ts>
                  <nts id="Seg_2065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2067" n="HIAT:w" s="T538">ınnä</ts>
                  <nts id="Seg_2068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2070" n="HIAT:w" s="T539">ɨːtälsitɨ</ts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2073" n="HIAT:w" s="T540">täkɨqo</ts>
                  <nts id="Seg_2074" n="HIAT:ip">.</nts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T556" id="Seg_2077" n="HIAT:u" s="T541">
                  <ts e="T542" id="Seg_2079" n="HIAT:w" s="T541">Na</ts>
                  <nts id="Seg_2080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2082" n="HIAT:w" s="T542">vremʼaqɨt</ts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2085" n="HIAT:w" s="T543">načʼalʼnik</ts>
                  <nts id="Seg_2086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2088" n="HIAT:w" s="T544">Qolʼsak</ts>
                  <nts id="Seg_2089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2091" n="HIAT:w" s="T545">ira</ts>
                  <nts id="Seg_2092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2094" n="HIAT:w" s="T546">mennɨmpa</ts>
                  <nts id="Seg_2095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2097" n="HIAT:w" s="T547">İčʼakäčʼikat</ts>
                  <nts id="Seg_2098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2100" n="HIAT:w" s="T548">mɔːtɨlʼ</ts>
                  <nts id="Seg_2101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2103" n="HIAT:w" s="T549">pɛlʼaktɨ</ts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2106" n="HIAT:w" s="T550">šoːqɨr</ts>
                  <nts id="Seg_2107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2109" n="HIAT:w" s="T551">čʼɔːtɨmpɨt</ts>
                  <nts id="Seg_2110" n="HIAT:ip">,</nts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2113" n="HIAT:w" s="T552">purqɨ</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2116" n="HIAT:w" s="T553">qatqa</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2119" n="HIAT:w" s="T554">šoːqɨr</ts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2122" n="HIAT:w" s="T555">nɔːnɨ</ts>
                  <nts id="Seg_2123" n="HIAT:ip">.</nts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T563" id="Seg_2126" n="HIAT:u" s="T556">
                  <ts e="T557" id="Seg_2128" n="HIAT:w" s="T556">Tɨmtɨsä</ts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2130" n="HIAT:ip">(</nts>
                  <ts e="T558" id="Seg_2132" n="HIAT:w" s="T557">nɨmtɨsä</ts>
                  <nts id="Seg_2133" n="HIAT:ip">)</nts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2136" n="HIAT:w" s="T558">kättɨsɨtɨ</ts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2139" n="HIAT:w" s="T559">kazatɨqınik</ts>
                  <nts id="Seg_2140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2142" n="HIAT:w" s="T560">qälʼlʼa</ts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2145" n="HIAT:w" s="T561">mennɨmpɨqo</ts>
                  <nts id="Seg_2146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2148" n="HIAT:w" s="T562">imaqotap</ts>
                  <nts id="Seg_2149" n="HIAT:ip">.</nts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T565" id="Seg_2152" n="HIAT:u" s="T563">
                  <ts e="T564" id="Seg_2154" n="HIAT:w" s="T563">Täpɨt</ts>
                  <nts id="Seg_2155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2157" n="HIAT:w" s="T564">qäːssɔːtɨt</ts>
                  <nts id="Seg_2158" n="HIAT:ip">.</nts>
                  <nts id="Seg_2159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T575" id="Seg_2161" n="HIAT:u" s="T565">
                  <ts e="T566" id="Seg_2163" n="HIAT:w" s="T565">Mɔːttɨ</ts>
                  <nts id="Seg_2164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2166" n="HIAT:w" s="T566">seːrsɔːtɨt</ts>
                  <nts id="Seg_2167" n="HIAT:ip">,</nts>
                  <nts id="Seg_2168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2170" n="HIAT:w" s="T567">imaqotat</ts>
                  <nts id="Seg_2171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2173" n="HIAT:w" s="T568">koptɨ</ts>
                  <nts id="Seg_2174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2176" n="HIAT:w" s="T569">čʼɔːt</ts>
                  <nts id="Seg_2177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2179" n="HIAT:w" s="T570">omta</ts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2182" n="HIAT:w" s="T571">stolɨn</ts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2185" n="HIAT:w" s="T572">iːqɨt</ts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2188" n="HIAT:w" s="T573">İčʼäkäčʼika</ts>
                  <nts id="Seg_2189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2191" n="HIAT:w" s="T574">nʼenʼnʼamɔːtpa</ts>
                  <nts id="Seg_2192" n="HIAT:ip">.</nts>
                  <nts id="Seg_2193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T581" id="Seg_2195" n="HIAT:u" s="T575">
                  <ts e="T576" id="Seg_2197" n="HIAT:w" s="T575">Muntɨk</ts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2200" n="HIAT:w" s="T576">mäntɨtɨ</ts>
                  <nts id="Seg_2201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2203" n="HIAT:w" s="T577">qatolʼpat</ts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2206" n="HIAT:w" s="T578">kämčʼa</ts>
                  <nts id="Seg_2207" n="HIAT:ip">,</nts>
                  <nts id="Seg_2208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2210" n="HIAT:w" s="T579">porqɨmtɨ</ts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2213" n="HIAT:w" s="T580">nɨtɨmpɨlʼa</ts>
                  <nts id="Seg_2214" n="HIAT:ip">.</nts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T586" id="Seg_2217" n="HIAT:u" s="T581">
                  <ts e="T582" id="Seg_2219" n="HIAT:w" s="T581">Täpɨt</ts>
                  <nts id="Seg_2220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2222" n="HIAT:w" s="T582">kätɨsɔːt</ts>
                  <nts id="Seg_2223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2224" n="HIAT:ip">(</nts>
                  <ts e="T583.tx.1" id="Seg_2226" n="HIAT:w" s="T583">seːrsɔːtɨt</ts>
                  <nts id="Seg_2227" n="HIAT:ip">)</nts>
                  <ts e="T584" id="Seg_2229" n="HIAT:w" s="T583.tx.1">–“</ts>
                  <nts id="Seg_2230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2232" n="HIAT:w" s="T584">Dorova</ts>
                  <nts id="Seg_2233" n="HIAT:ip">,</nts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2236" n="HIAT:w" s="T585">lʼaqa”</ts>
                  <nts id="Seg_2237" n="HIAT:ip">.</nts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T601" id="Seg_2240" n="HIAT:u" s="T586">
                  <ts e="T587" id="Seg_2242" n="HIAT:w" s="T586">A</ts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2245" n="HIAT:w" s="T587">İčʼekäčʼika</ts>
                  <nts id="Seg_2246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2248" n="HIAT:w" s="T588">nenʼnʼimɔːlla</ts>
                  <nts id="Seg_2249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589.tx.1" id="Seg_2251" n="HIAT:w" s="T589">omta</ts>
                  <nts id="Seg_2252" n="HIAT:ip">:</nts>
                  <ts e="T590" id="Seg_2254" n="HIAT:w" s="T589.tx.1">“</ts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2257" n="HIAT:w" s="T590">Tɛː</ts>
                  <nts id="Seg_2258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2260" n="HIAT:w" s="T591">masɨp</ts>
                  <nts id="Seg_2261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2263" n="HIAT:w" s="T592">ütɨsɨp</ts>
                  <nts id="Seg_2264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2266" n="HIAT:w" s="T593">qoptɨrɨsɨlɨt</ts>
                  <nts id="Seg_2267" n="HIAT:ip">,</nts>
                  <nts id="Seg_2268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2270" n="HIAT:w" s="T594">mat</ts>
                  <nts id="Seg_2271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2273" n="HIAT:w" s="T595">morʼan</ts>
                  <nts id="Seg_2274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2276" n="HIAT:w" s="T596">ılqɨt</ts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2279" n="HIAT:w" s="T597">kotčʼik</ts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2282" n="HIAT:w" s="T598">tɔːq</ts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2284" n="HIAT:ip">(</nts>
                  <ts e="T600" id="Seg_2286" n="HIAT:w" s="T599">lɨptɨk</ts>
                  <nts id="Seg_2287" n="HIAT:ip">)</nts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2290" n="HIAT:w" s="T600">qosap</ts>
                  <nts id="Seg_2291" n="HIAT:ip">.</nts>
                  <nts id="Seg_2292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T611" id="Seg_2294" n="HIAT:u" s="T601">
                  <ts e="T602" id="Seg_2296" n="HIAT:w" s="T601">Näsaŋ</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2299" n="HIAT:w" s="T602">eŋa</ts>
                  <nts id="Seg_2300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2302" n="HIAT:w" s="T603">kusak</ts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2305" n="HIAT:w" s="T604">orɨm</ts>
                  <nts id="Seg_2306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2308" n="HIAT:w" s="T605">ɛːsa</ts>
                  <nts id="Seg_2309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2311" n="HIAT:w" s="T606">iːsap</ts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2313" n="HIAT:ip">(</nts>
                  <ts e="T608" id="Seg_2315" n="HIAT:w" s="T607">ma</ts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2318" n="HIAT:w" s="T608">orɨmɨ</ts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2321" n="HIAT:w" s="T609">asa</ts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2324" n="HIAT:w" s="T610">iːsɨtɨ</ts>
                  <nts id="Seg_2325" n="HIAT:ip">)</nts>
                  <nts id="Seg_2326" n="HIAT:ip">.</nts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T615" id="Seg_2329" n="HIAT:u" s="T611">
                  <ts e="T612" id="Seg_2331" n="HIAT:w" s="T611">Bɨtta</ts>
                  <nts id="Seg_2332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2334" n="HIAT:w" s="T612">ütenɨlʼ</ts>
                  <nts id="Seg_2335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2337" n="HIAT:w" s="T613">nomqɨn</ts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2340" n="HIAT:w" s="T614">ɛːsak”</ts>
                  <nts id="Seg_2341" n="HIAT:ip">.</nts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T621" id="Seg_2344" n="HIAT:u" s="T615">
                  <ts e="T616" id="Seg_2346" n="HIAT:w" s="T615">Täpɨt</ts>
                  <nts id="Seg_2347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2349" n="HIAT:w" s="T616">qässɔːtɨt</ts>
                  <nts id="Seg_2350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617.tx.1" id="Seg_2352" n="HIAT:w" s="T617">nɨrkɨmɔːllä</ts>
                  <nts id="Seg_2353" n="HIAT:ip">:</nts>
                  <ts e="T618" id="Seg_2355" n="HIAT:w" s="T617.tx.1">“</ts>
                  <nts id="Seg_2356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2358" n="HIAT:w" s="T618">Qoilʼ</ts>
                  <nts id="Seg_2359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2361" n="HIAT:w" s="T619">jabol</ts>
                  <nts id="Seg_2362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2364" n="HIAT:w" s="T620">ɛːsa”</ts>
                  <nts id="Seg_2365" n="HIAT:ip">.</nts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T643" id="Seg_2368" n="HIAT:u" s="T621">
                  <ts e="T622" id="Seg_2370" n="HIAT:w" s="T621">Qolʼsak</ts>
                  <nts id="Seg_2371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2373" n="HIAT:w" s="T622">iranɨk</ts>
                  <nts id="Seg_2374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623.tx.1" id="Seg_2376" n="HIAT:w" s="T623">kätɨqo</ts>
                  <nts id="Seg_2377" n="HIAT:ip">:</nts>
                  <ts e="T624" id="Seg_2379" n="HIAT:w" s="T623.tx.1">“</ts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2382" n="HIAT:w" s="T624">İčʼakäčʼikap</ts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2385" n="HIAT:w" s="T625">üttɨ</ts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2388" n="HIAT:w" s="T626">qoptɨrsɨmɨt</ts>
                  <nts id="Seg_2389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2391" n="HIAT:w" s="T627">ikolʼ</ts>
                  <nts id="Seg_2392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2394" n="HIAT:w" s="T628">čʼeːlʼe</ts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2396" n="HIAT:ip">(</nts>
                  <ts e="T630" id="Seg_2398" n="HIAT:w" s="T629">okot</ts>
                  <nts id="Seg_2399" n="HIAT:ip">)</nts>
                  <nts id="Seg_2400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2402" n="HIAT:w" s="T630">täp</ts>
                  <nts id="Seg_2403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2405" n="HIAT:w" s="T631">nassa</ts>
                  <nts id="Seg_2406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2408" n="HIAT:w" s="T632">qompatɨ</ts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2411" n="HIAT:w" s="T633">tɔːq</ts>
                  <nts id="Seg_2412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2413" n="HIAT:ip">(</nts>
                  <ts e="T635" id="Seg_2415" n="HIAT:w" s="T634">lɨptɨk</ts>
                  <nts id="Seg_2416" n="HIAT:ip">)</nts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2419" n="HIAT:w" s="T635">kušalʼ</ts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2422" n="HIAT:w" s="T636">tätaqoj</ts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2425" n="HIAT:w" s="T637">muntɨk</ts>
                  <nts id="Seg_2426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2428" n="HIAT:w" s="T638">qompatɨ</ts>
                  <nts id="Seg_2429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2431" n="HIAT:w" s="T639">i</ts>
                  <nts id="Seg_2432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2434" n="HIAT:w" s="T640">sıːt</ts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2437" n="HIAT:w" s="T641">qopɨlʼ</ts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2440" n="HIAT:w" s="T642">mɨt”</ts>
                  <nts id="Seg_2441" n="HIAT:ip">.</nts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T647" id="Seg_2444" n="HIAT:u" s="T643">
                  <ts e="T644" id="Seg_2446" n="HIAT:w" s="T643">Qolʼsak</ts>
                  <nts id="Seg_2447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2449" n="HIAT:w" s="T644">ira</ts>
                  <nts id="Seg_2450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2452" n="HIAT:w" s="T645">tɛnɨtɨ</ts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2455" n="HIAT:w" s="T646">ürɨsa</ts>
                  <nts id="Seg_2456" n="HIAT:ip">.</nts>
                  <nts id="Seg_2457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T650" id="Seg_2459" n="HIAT:u" s="T647">
                  <ts e="T648" id="Seg_2461" n="HIAT:w" s="T647">“Qäntɨk</ts>
                  <nts id="Seg_2462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2464" n="HIAT:w" s="T648">nılʼčʼik</ts>
                  <nts id="Seg_2465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2467" n="HIAT:w" s="T649">ɛsɨsa</ts>
                  <nts id="Seg_2468" n="HIAT:ip">?</nts>
                  <nts id="Seg_2469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T654" id="Seg_2471" n="HIAT:u" s="T650">
                  <ts e="T651" id="Seg_2473" n="HIAT:w" s="T650">İčʼekäčʼika</ts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2476" n="HIAT:w" s="T651">jabəlʼ</ts>
                  <nts id="Seg_2477" n="HIAT:ip">,</nts>
                  <nts id="Seg_2478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2480" n="HIAT:w" s="T652">mannon</ts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2482" n="HIAT:ip">(</nts>
                  <ts e="T653.tx.1" id="Seg_2484" n="HIAT:w" s="T653">menon</ts>
                  <nts id="Seg_2485" n="HIAT:ip">)</nts>
                  <ts e="T654" id="Seg_2487" n="HIAT:w" s="T653.tx.1">”</ts>
                  <nts id="Seg_2488" n="HIAT:ip">.</nts>
                  <nts id="Seg_2489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T671" id="Seg_2491" n="HIAT:u" s="T654">
                  <ts e="T655" id="Seg_2493" n="HIAT:w" s="T654">Šintelʼ</ts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2496" n="HIAT:w" s="T655">čʼeːlʼ</ts>
                  <nts id="Seg_2497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2499" n="HIAT:w" s="T656">ontɨ</ts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2502" n="HIAT:w" s="T657">šität</ts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2505" n="HIAT:w" s="T658">kasat</ts>
                  <nts id="Seg_2506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2508" n="HIAT:w" s="T659">tüŋčʼɔːtɨt</ts>
                  <nts id="Seg_2509" n="HIAT:ip">,</nts>
                  <nts id="Seg_2510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2512" n="HIAT:w" s="T660">İčʼekäčʼikantɨ</ts>
                  <nts id="Seg_2513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2515" n="HIAT:w" s="T661">üːtɨtɨ</ts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2518" n="HIAT:w" s="T662">kasak</ts>
                  <nts id="Seg_2519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2521" n="HIAT:w" s="T663">ira</ts>
                  <nts id="Seg_2522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2524" n="HIAT:w" s="T664">soqɨšqo</ts>
                  <nts id="Seg_2525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2527" n="HIAT:w" s="T665">İčʼakäčʼikap</ts>
                  <nts id="Seg_2528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2530" n="HIAT:w" s="T666">kuttar</ts>
                  <nts id="Seg_2531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2533" n="HIAT:w" s="T667">qosɨtɨ</ts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2536" n="HIAT:w" s="T668">na</ts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2539" n="HIAT:w" s="T669">to</ts>
                  <nts id="Seg_2540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2542" n="HIAT:w" s="T670">lɨptɨtɨp</ts>
                  <nts id="Seg_2543" n="HIAT:ip">.</nts>
                  <nts id="Seg_2544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T676" id="Seg_2546" n="HIAT:u" s="T671">
                  <ts e="T672" id="Seg_2548" n="HIAT:w" s="T671">Meː</ts>
                  <nts id="Seg_2549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2551" n="HIAT:w" s="T672">aj</ts>
                  <nts id="Seg_2552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2554" n="HIAT:w" s="T673">üttɨ</ts>
                  <nts id="Seg_2555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2557" n="HIAT:w" s="T674">pattɨrtɛntɨmɨt</ts>
                  <nts id="Seg_2558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2560" n="HIAT:w" s="T675">tɔːqɨtqo</ts>
                  <nts id="Seg_2561" n="HIAT:ip">.</nts>
                  <nts id="Seg_2562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T701" id="Seg_2564" n="HIAT:u" s="T676">
                  <ts e="T677" id="Seg_2566" n="HIAT:w" s="T676">İčʼakäčʼika</ts>
                  <nts id="Seg_2567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2569" n="HIAT:w" s="T677">kätɨsɨtɨ</ts>
                  <nts id="Seg_2570" n="HIAT:ip">:</nts>
                  <nts id="Seg_2571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2573" n="HIAT:w" s="T678">Sütqonɨŋɨlʼɨt</ts>
                  <nts id="Seg_2574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2575" n="HIAT:ip">(</nts>
                  <ts e="T680" id="Seg_2577" n="HIAT:w" s="T679">sütqolʼnɔːtɨt</ts>
                  <nts id="Seg_2578" n="HIAT:ip">,</nts>
                  <nts id="Seg_2579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2581" n="HIAT:w" s="T680">sütqɨlʼnɔːlʼit</ts>
                  <nts id="Seg_2582" n="HIAT:ip">)</nts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2585" n="HIAT:w" s="T681">sɨːrɨt</ts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2588" n="HIAT:w" s="T682">qopɨ</ts>
                  <nts id="Seg_2589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2591" n="HIAT:w" s="T683">nɔːnɨ</ts>
                  <nts id="Seg_2592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2594" n="HIAT:w" s="T684">kotatɨp</ts>
                  <nts id="Seg_2595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2596" n="HIAT:ip">(</nts>
                  <ts e="T686" id="Seg_2598" n="HIAT:w" s="T685">sünnelʼɨt</ts>
                  <nts id="Seg_2599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2601" n="HIAT:w" s="T686">kotatɨp</ts>
                  <nts id="Seg_2602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2604" n="HIAT:w" s="T687">sɨːrɨt</ts>
                  <nts id="Seg_2605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2607" n="HIAT:w" s="T688">qopɨlʼ</ts>
                  <nts id="Seg_2608" n="HIAT:ip">)</nts>
                  <nts id="Seg_2609" n="HIAT:ip">,</nts>
                  <nts id="Seg_2610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2612" n="HIAT:w" s="T689">ɔːlʼčʼiŋɨlʼɨt</ts>
                  <nts id="Seg_2613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2615" n="HIAT:w" s="T690">morʼat</ts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2618" n="HIAT:w" s="T691">qanɨqtɨ</ts>
                  <nts id="Seg_2619" n="HIAT:ip">,</nts>
                  <nts id="Seg_2620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2622" n="HIAT:w" s="T692">mat</ts>
                  <nts id="Seg_2623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2625" n="HIAT:w" s="T693">tɛːsintɨ</ts>
                  <nts id="Seg_2626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2628" n="HIAT:w" s="T694">süttap</ts>
                  <nts id="Seg_2629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2631" n="HIAT:w" s="T695">kotantɨ</ts>
                  <nts id="Seg_2632" n="HIAT:ip">,</nts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2635" n="HIAT:w" s="T696">käntap</ts>
                  <nts id="Seg_2636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2638" n="HIAT:w" s="T697">üttɨ</ts>
                  <nts id="Seg_2639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2641" n="HIAT:w" s="T698">püsa</ts>
                  <nts id="Seg_2642" n="HIAT:ip">,</nts>
                  <nts id="Seg_2643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2645" n="HIAT:w" s="T699">marqɨ</ts>
                  <nts id="Seg_2646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2648" n="HIAT:w" s="T700">püsa</ts>
                  <nts id="Seg_2649" n="HIAT:ip">.</nts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T705" id="Seg_2652" n="HIAT:u" s="T701">
                  <ts e="T702" id="Seg_2654" n="HIAT:w" s="T701">Tɛː</ts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2657" n="HIAT:w" s="T702">qontelʼit</ts>
                  <nts id="Seg_2658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2660" n="HIAT:w" s="T703">muntɨk</ts>
                  <nts id="Seg_2661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2663" n="HIAT:w" s="T704">tɔːq”</ts>
                  <nts id="Seg_2664" n="HIAT:ip">.</nts>
                  <nts id="Seg_2665" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T711.tx.1" id="Seg_2667" n="HIAT:u" s="T705">
                  <ts e="T706" id="Seg_2669" n="HIAT:w" s="T705">Ukoːt</ts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2672" n="HIAT:w" s="T706">Qolʼsak</ts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2675" n="HIAT:w" s="T707">ira</ts>
                  <nts id="Seg_2676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2677" n="HIAT:ip">(</nts>
                  <ts e="T708.tx.1" id="Seg_2679" n="HIAT:w" s="T708">načʼalʼnik</ts>
                  <nts id="Seg_2680" n="HIAT:ip">)</nts>
                  <nts id="Seg_2681" n="HIAT:ip">:</nts>
                  <ts e="T709" id="Seg_2683" n="HIAT:w" s="T708.tx.1">“</ts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2686" n="HIAT:w" s="T709">Kutə</ts>
                  <nts id="Seg_2687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2689" n="HIAT:w" s="T710">kɨka</ts>
                  <nts id="Seg_2690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711.tx.1" id="Seg_2692" n="HIAT:w" s="T711">qättoːqonɨ</ts>
                  <nts id="Seg_2693" n="HIAT:ip">?</nts>
               </ts>
               <ts e="T721" id="Seg_2695" n="HIAT:u" s="T711.tx.1">
                  <ts e="T712" id="Seg_2697" n="HIAT:w" s="T711.tx.1">”</ts>
                  <nts id="Seg_2698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2700" n="HIAT:w" s="T712">Täp</ts>
                  <nts id="Seg_2701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2703" n="HIAT:w" s="T713">sütkolʼsitɨ</ts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2706" n="HIAT:w" s="T714">ukoːt</ts>
                  <nts id="Seg_2707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2709" n="HIAT:w" s="T715">Qolʼsak</ts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2712" n="HIAT:w" s="T716">irap</ts>
                  <nts id="Seg_2713" n="HIAT:ip">,</nts>
                  <nts id="Seg_2714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2716" n="HIAT:w" s="T717">nɨnɨ</ts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2719" n="HIAT:w" s="T718">mänɨlʼ</ts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2722" n="HIAT:w" s="T719">qumiːmtɨ</ts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2724" n="HIAT:ip">(</nts>
                  <ts e="T721" id="Seg_2726" n="HIAT:w" s="T720">kasatɨp</ts>
                  <nts id="Seg_2727" n="HIAT:ip">)</nts>
                  <nts id="Seg_2728" n="HIAT:ip">.</nts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T729" id="Seg_2731" n="HIAT:u" s="T721">
                  <ts e="T722" id="Seg_2733" n="HIAT:w" s="T721">Nɨnɨ</ts>
                  <nts id="Seg_2734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2736" n="HIAT:w" s="T722">İčʼakäčʼika</ts>
                  <nts id="Seg_2737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2739" n="HIAT:w" s="T723">ontɨ</ts>
                  <nts id="Seg_2740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2742" n="HIAT:w" s="T724">käqolapsɨtɨ</ts>
                  <nts id="Seg_2743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2745" n="HIAT:w" s="T725">sütɨpɨlʼ</ts>
                  <nts id="Seg_2746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2748" n="HIAT:w" s="T726">kotatɨp</ts>
                  <nts id="Seg_2749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2751" n="HIAT:w" s="T727">qumɨtsä</ts>
                  <nts id="Seg_2752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2754" n="HIAT:w" s="T728">üttɨ</ts>
                  <nts id="Seg_2755" n="HIAT:ip">.</nts>
                  <nts id="Seg_2756" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T736" id="Seg_2758" n="HIAT:u" s="T729">
                  <ts e="T730" id="Seg_2760" n="HIAT:w" s="T729">Kätɨsɨtɨ</ts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2763" n="HIAT:w" s="T730">Qolʼsak</ts>
                  <nts id="Seg_2764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731.tx.1" id="Seg_2766" n="HIAT:w" s="T731">iranɨk</ts>
                  <nts id="Seg_2767" n="HIAT:ip">:</nts>
                  <ts e="T732" id="Seg_2769" n="HIAT:w" s="T731.tx.1">“</ts>
                  <nts id="Seg_2770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2772" n="HIAT:w" s="T732">Peːtɨ</ts>
                  <nts id="Seg_2773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2775" n="HIAT:w" s="T733">tɔːq</ts>
                  <nts id="Seg_2776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2778" n="HIAT:w" s="T734">morʼan</ts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2781" n="HIAT:w" s="T735">ılqɨt</ts>
                  <nts id="Seg_2782" n="HIAT:ip">.</nts>
                  <nts id="Seg_2783" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T743" id="Seg_2785" n="HIAT:u" s="T736">
                  <ts e="T737" id="Seg_2787" n="HIAT:w" s="T736">Nʼi</ts>
                  <nts id="Seg_2788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2790" n="HIAT:w" s="T737">kun</ts>
                  <nts id="Seg_2791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2793" n="HIAT:w" s="T738">asa</ts>
                  <nts id="Seg_2794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2796" n="HIAT:w" s="T739">qontal</ts>
                  <nts id="Seg_2797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2799" n="HIAT:w" s="T740">tɔːqɨtɨ</ts>
                  <nts id="Seg_2800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2802" n="HIAT:w" s="T741">i</ts>
                  <nts id="Seg_2803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2805" n="HIAT:w" s="T742">tovarɨt</ts>
                  <nts id="Seg_2806" n="HIAT:ip">.</nts>
                  <nts id="Seg_2807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T745" id="Seg_2809" n="HIAT:u" s="T743">
                  <ts e="T744" id="Seg_2811" n="HIAT:w" s="T743">Meːltɨ</ts>
                  <nts id="Seg_2812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2814" n="HIAT:w" s="T744">qunnantɨ</ts>
                  <nts id="Seg_2815" n="HIAT:ip">.</nts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T752" id="Seg_2818" n="HIAT:u" s="T745">
                  <ts e="T746" id="Seg_2820" n="HIAT:w" s="T745">Tɛː</ts>
                  <nts id="Seg_2821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2823" n="HIAT:w" s="T746">na</ts>
                  <nts id="Seg_2824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2826" n="HIAT:w" s="T747">masɨp</ts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2829" n="HIAT:w" s="T748">käkkɨsɨlɨt</ts>
                  <nts id="Seg_2830" n="HIAT:ip">,</nts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2833" n="HIAT:w" s="T749">üttɨ</ts>
                  <nts id="Seg_2834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2836" n="HIAT:w" s="T750">sɨp</ts>
                  <nts id="Seg_2837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2839" n="HIAT:w" s="T751">käːsɨlɨt</ts>
                  <nts id="Seg_2840" n="HIAT:ip">.</nts>
                  <nts id="Seg_2841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T752" id="Seg_2842" n="sc" s="T0">
               <ts e="T1" id="Seg_2844" n="e" s="T0">Qolʼsaqo </ts>
               <ts e="T2" id="Seg_2846" n="e" s="T1">ira. </ts>
               <ts e="T3" id="Seg_2848" n="e" s="T2">İlʼimpa </ts>
               <ts e="T4" id="Seg_2850" n="e" s="T3">İčʼakäčʼika </ts>
               <ts e="T5" id="Seg_2852" n="e" s="T4">imlʼantɨsä. </ts>
               <ts e="T6" id="Seg_2854" n="e" s="T5">Qolʼsaq </ts>
               <ts e="T7" id="Seg_2856" n="e" s="T6">iralʼ </ts>
               <ts e="T8" id="Seg_2858" n="e" s="T7">qəːttɨ– </ts>
               <ts e="T9" id="Seg_2860" n="e" s="T8">asa </ts>
               <ts e="T10" id="Seg_2862" n="e" s="T9">kuntaːqɨn </ts>
               <ts e="T11" id="Seg_2864" n="e" s="T10">ɛːsa. </ts>
               <ts e="T12" id="Seg_2866" n="e" s="T11">Okkɨr </ts>
               <ts e="T13" id="Seg_2868" n="e" s="T12">čʼontoːqɨt </ts>
               <ts e="T14" id="Seg_2870" n="e" s="T13">Qolʼsaqo </ts>
               <ts e="T15" id="Seg_2872" n="e" s="T14">iralʼ </ts>
               <ts e="T16" id="Seg_2874" n="e" s="T15">sɨːrɨtɨ </ts>
               <ts e="T17" id="Seg_2876" n="e" s="T16">üra </ts>
               <ts e="T18" id="Seg_2878" n="e" s="T17">(ürɨsa) </ts>
               <ts e="T19" id="Seg_2880" n="e" s="T18">telʼde </ts>
               <ts e="T20" id="Seg_2882" n="e" s="T19">ürɨkka. </ts>
               <ts e="T21" id="Seg_2884" n="e" s="T20">Täp </ts>
               <ts e="T22" id="Seg_2886" n="e" s="T21">tɛnɨmɨtɨ </ts>
               <ts e="T23" id="Seg_2888" n="e" s="T22">što </ts>
               <ts e="T24" id="Seg_2890" n="e" s="T23">İčʼakäčʼikat </ts>
               <ts e="T25" id="Seg_2892" n="e" s="T24">tälʼɨsɨt. </ts>
               <ts e="T26" id="Seg_2894" n="e" s="T25">Üːtɔːtɨ </ts>
               <ts e="T27" id="Seg_2896" n="e" s="T26">täpɨnɨk </ts>
               <ts e="T28" id="Seg_2898" n="e" s="T27">kazatɨp </ts>
               <ts e="T29" id="Seg_2900" n="e" s="T28">sitatɨp </ts>
               <ts e="T30" id="Seg_2902" n="e" s="T29">männɨmpɨqo </ts>
               <ts e="T31" id="Seg_2904" n="e" s="T30">qoip </ts>
               <ts e="T32" id="Seg_2906" n="e" s="T31">meːtɨt </ts>
               <ts e="T33" id="Seg_2908" n="e" s="T32">İčʼakäčʼika. </ts>
               <ts e="T34" id="Seg_2910" n="e" s="T33">Täp </ts>
               <ts e="T35" id="Seg_2912" n="e" s="T34">tɛnɨmɨtɨ </ts>
               <ts e="T36" id="Seg_2914" n="e" s="T35">tüntɔːtɨt </ts>
               <ts e="T37" id="Seg_2916" n="e" s="T36">rusʼaksä </ts>
               <ts e="T38" id="Seg_2918" n="e" s="T37">(puškatsä) </ts>
               <ts e="T39" id="Seg_2920" n="e" s="T38">täpɨp </ts>
               <ts e="T40" id="Seg_2922" n="e" s="T39">orqəlʼtɛntɔːtɨt </ts>
               <ts e="T41" id="Seg_2924" n="e" s="T40">omtɨltɛntɔːtɨt </ts>
               <ts e="T42" id="Seg_2926" n="e" s="T41">tʼurmantɨ. </ts>
               <ts e="T43" id="Seg_2928" n="e" s="T42">Na </ts>
               <ts e="T44" id="Seg_2930" n="e" s="T43">vremʼaqɨt </ts>
               <ts e="T45" id="Seg_2932" n="e" s="T44">(na </ts>
               <ts e="T46" id="Seg_2934" n="e" s="T45">čʼeːlʼe) </ts>
               <ts e="T47" id="Seg_2936" n="e" s="T46">sɨːrɨp </ts>
               <ts e="T48" id="Seg_2938" n="e" s="T47">qəssɨtɨ, </ts>
               <ts e="T49" id="Seg_2940" n="e" s="T48">ketɨmtɨ </ts>
               <ts e="T50" id="Seg_2942" n="e" s="T49">kɨmsa </ts>
               <ts e="T51" id="Seg_2944" n="e" s="T50">qamtɨsɨtɨ. </ts>
               <ts e="T52" id="Seg_2946" n="e" s="T51">Merɨsɨtɨ </ts>
               <ts e="T53" id="Seg_2948" n="e" s="T52">marqɨ </ts>
               <ts e="T54" id="Seg_2950" n="e" s="T53">paŋɨp </ts>
               <ts e="T55" id="Seg_2952" n="e" s="T54">mɔːta </ts>
               <ts e="T56" id="Seg_2954" n="e" s="T55">pɔːrɔːqot. </ts>
               <ts e="T57" id="Seg_2956" n="e" s="T56">İmlʼamtɨ </ts>
               <ts e="T58" id="Seg_2958" n="e" s="T57">tamtɨlsɨtɨ </ts>
               <ts e="T59" id="Seg_2960" n="e" s="T58">kämɨlʼ </ts>
               <ts e="T60" id="Seg_2962" n="e" s="T59">sɨːrɨt </ts>
               <ts e="T61" id="Seg_2964" n="e" s="T60">ketɨsa. </ts>
               <ts e="T62" id="Seg_2966" n="e" s="T61">İmlʼat </ts>
               <ts e="T63" id="Seg_2968" n="e" s="T62">tokkaltistɨ </ts>
               <ts e="T64" id="Seg_2970" n="e" s="T63">porqat. </ts>
               <ts e="T65" id="Seg_2972" n="e" s="T64">Kazat </ts>
               <ts e="T66" id="Seg_2974" n="e" s="T65">(kazaqɨt) </ts>
               <ts e="T67" id="Seg_2976" n="e" s="T66">tüsɔːtɨt </ts>
               <ts e="T68" id="Seg_2978" n="e" s="T67">İčʼakäčʼikanɨk. </ts>
               <ts e="T69" id="Seg_2980" n="e" s="T68">Marqa </ts>
               <ts e="T70" id="Seg_2982" n="e" s="T69">qup </ts>
               <ts e="T71" id="Seg_2984" n="e" s="T70">kätsɨtɨ </ts>
               <ts e="T72" id="Seg_2986" n="e" s="T71">İčʼakäčʼikanɨ. </ts>
               <ts e="T73" id="Seg_2988" n="e" s="T72">“Tasɨntɨ </ts>
               <ts e="T74" id="Seg_2990" n="e" s="T73">omtɨlʼtɨqo </ts>
               <ts e="T75" id="Seg_2992" n="e" s="T74">nɔːtna </ts>
               <ts e="T76" id="Seg_2994" n="e" s="T75">türʼmantɨ. </ts>
               <ts e="T77" id="Seg_2996" n="e" s="T76">Tat </ts>
               <ts e="T78" id="Seg_2998" n="e" s="T77">sɨːrɨtɨp </ts>
               <ts e="T79" id="Seg_3000" n="e" s="T78">qättal </ts>
               <ts e="T80" id="Seg_3002" n="e" s="T79">amnal”. </ts>
               <ts e="T81" id="Seg_3004" n="e" s="T80">Täp </ts>
               <ts e="T82" id="Seg_3006" n="e" s="T81">nʼenʼnʼamɔːssa </ts>
               <ts e="T83" id="Seg_3008" n="e" s="T82">imlʼantɨkinı </ts>
               <ts e="T84" id="Seg_3010" n="e" s="T83">kätsɨtɨ. </ts>
               <ts e="T85" id="Seg_3012" n="e" s="T84">“Massɨp </ts>
               <ts e="T86" id="Seg_3014" n="e" s="T85">omtalʼtɨntɔːtɨt </ts>
               <ts e="T87" id="Seg_3016" n="e" s="T86">tʼürmantɨ. </ts>
               <ts e="T88" id="Seg_3018" n="e" s="T87">Čʼajnik </ts>
               <ts e="T89" id="Seg_3020" n="e" s="T88">mušerät </ts>
               <ts e="T90" id="Seg_3022" n="e" s="T89">känpɨlʼa”. </ts>
               <ts e="T91" id="Seg_3024" n="e" s="T90">İmlʼatɨ </ts>
               <ts e="T92" id="Seg_3026" n="e" s="T91">toːtälʼna:“ </ts>
               <ts e="T93" id="Seg_3028" n="e" s="T92">Topop </ts>
               <ts e="T94" id="Seg_3030" n="e" s="T93">utop </ts>
               <ts e="T95" id="Seg_3032" n="e" s="T94">čʼüssa”. </ts>
               <ts e="T96" id="Seg_3034" n="e" s="T95">Kazat </ts>
               <ts e="T97" id="Seg_3036" n="e" s="T96">nɨŋnɔːtɨt </ts>
               <ts e="T98" id="Seg_3038" n="e" s="T97">mɔːtan </ts>
               <ts e="T99" id="Seg_3040" n="e" s="T98">ɔːqqɨt. </ts>
               <ts e="T100" id="Seg_3042" n="e" s="T99">İčakäčika </ts>
               <ts e="T101" id="Seg_3044" n="e" s="T100">imlʼantɨkinı:“ </ts>
               <ts e="T102" id="Seg_3046" n="e" s="T101">Ta </ts>
               <ts e="T103" id="Seg_3048" n="e" s="T102">lʼentʼajŋɔːnt, </ts>
               <ts e="T104" id="Seg_3050" n="e" s="T103">čʼajnik </ts>
               <ts e="T105" id="Seg_3052" n="e" s="T104">asa </ts>
               <ts e="T106" id="Seg_3054" n="e" s="T105">kɨkantɨ </ts>
               <ts e="T107" id="Seg_3056" n="e" s="T106">musɨrɨqo”. </ts>
               <ts e="T108" id="Seg_3058" n="e" s="T107">Nʼenʼnʼimɔːssa </ts>
               <ts e="T109" id="Seg_3060" n="e" s="T108">İčʼäkäčʼika, </ts>
               <ts e="T110" id="Seg_3062" n="e" s="T109">paŋɨmtɨ </ts>
               <ts e="T111" id="Seg_3064" n="e" s="T110">ılla </ts>
               <ts e="T112" id="Seg_3066" n="e" s="T111">iːsɨtɨ, </ts>
               <ts e="T113" id="Seg_3068" n="e" s="T112">imlʼamtɨ </ts>
               <ts e="T114" id="Seg_3070" n="e" s="T113">mattɨrsɨtɨ, </ts>
               <ts e="T115" id="Seg_3072" n="e" s="T114">imlʼatɨ </ts>
               <ts e="T116" id="Seg_3074" n="e" s="T115">qulʼčʼisa. </ts>
               <ts e="T117" id="Seg_3076" n="e" s="T116">Nɨnɨ </ts>
               <ts e="T118" id="Seg_3078" n="e" s="T117">İčʼäkäčʼika </ts>
               <ts e="T119" id="Seg_3080" n="e" s="T118">kätsɨtɨ </ts>
               <ts e="T120" id="Seg_3082" n="e" s="T119">kazatkinı:“ </ts>
               <ts e="T121" id="Seg_3084" n="e" s="T120">İmlʼam </ts>
               <ts e="T122" id="Seg_3086" n="e" s="T121">melʼte </ts>
               <ts e="T123" id="Seg_3088" n="e" s="T122">nılʼčʼik </ts>
               <ts e="T124" id="Seg_3090" n="e" s="T123">orɨnʼnʼa”. </ts>
               <ts e="T125" id="Seg_3092" n="e" s="T124">Paŋɨtɨ </ts>
               <ts e="T126" id="Seg_3094" n="e" s="T125">topɔːqɨntɨ </ts>
               <ts e="T127" id="Seg_3096" n="e" s="T126">loːqɨrɨsɨtɨ. </ts>
               <ts e="T128" id="Seg_3098" n="e" s="T127">“İmlʼa </ts>
               <ts e="T129" id="Seg_3100" n="e" s="T128">ınnä </ts>
               <ts e="T130" id="Seg_3102" n="e" s="T129">paktäš, </ts>
               <ts e="T131" id="Seg_3104" n="e" s="T130">paŋɨp </ts>
               <ts e="T132" id="Seg_3106" n="e" s="T131">ilʼaptɛnta”. </ts>
               <ts e="T133" id="Seg_3108" n="e" s="T132">Nɨnɨ </ts>
               <ts e="T134" id="Seg_3110" n="e" s="T133">imlʼatɨ </ts>
               <ts e="T135" id="Seg_3112" n="e" s="T134">nɨlleisa, </ts>
               <ts e="T136" id="Seg_3114" n="e" s="T135">känpɨlä </ts>
               <ts e="T137" id="Seg_3116" n="e" s="T136">orqɨlsɨtɨ </ts>
               <ts e="T138" id="Seg_3118" n="e" s="T137">čʼajniktɨ </ts>
               <ts e="T139" id="Seg_3120" n="e" s="T138">musɨrɨqo. </ts>
               <ts e="T140" id="Seg_3122" n="e" s="T139">Kansap </ts>
               <ts e="T141" id="Seg_3124" n="e" s="T140">naqqɨlʼčʼat </ts>
               <ts e="T142" id="Seg_3126" n="e" s="T141">kuntɨ </ts>
               <ts e="T143" id="Seg_3128" n="e" s="T142">čʼajnik </ts>
               <ts e="T144" id="Seg_3130" n="e" s="T143">musejsa. </ts>
               <ts e="T145" id="Seg_3132" n="e" s="T144">Nʼanʼɨm </ts>
               <ts e="T146" id="Seg_3134" n="e" s="T145">apsɨp </ts>
               <ts e="T147" id="Seg_3136" n="e" s="T146">tottɨsɨtɨ </ts>
               <ts e="T148" id="Seg_3138" n="e" s="T147">lʼemtɨ </ts>
               <ts e="T149" id="Seg_3140" n="e" s="T148">känpɨlä. </ts>
               <ts e="T150" id="Seg_3142" n="e" s="T149">Kazat </ts>
               <ts e="T151" id="Seg_3144" n="e" s="T150">amɨrqolapsɔːtɨt. </ts>
               <ts e="T152" id="Seg_3146" n="e" s="T151">Täpɨt </ts>
               <ts e="T153" id="Seg_3148" n="e" s="T152">nılʼčʼik </ts>
               <ts e="T154" id="Seg_3150" n="e" s="T153">kätɨsɔːtɨt:“ </ts>
               <ts e="T155" id="Seg_3152" n="e" s="T154">Me </ts>
               <ts e="T156" id="Seg_3154" n="e" s="T155">tat </ts>
               <ts e="T157" id="Seg_3156" n="e" s="T156">paŋɨp </ts>
               <ts e="T158" id="Seg_3158" n="e" s="T157">täːmɛntɔːmɨt. </ts>
               <ts e="T159" id="Seg_3160" n="e" s="T158">Me </ts>
               <ts e="T160" id="Seg_3162" n="e" s="T159">iːmaiːmat </ts>
               <ts e="T161" id="Seg_3164" n="e" s="T160">lʼentʼajtɔːtɨt </ts>
               <ts e="T162" id="Seg_3166" n="e" s="T161">i </ts>
               <ts e="T163" id="Seg_3168" n="e" s="T162">quntɔːtɨt”. </ts>
               <ts e="T164" id="Seg_3170" n="e" s="T163">Kazat </ts>
               <ts e="T165" id="Seg_3172" n="e" s="T164">asä </ts>
               <ts e="T166" id="Seg_3174" n="e" s="T165">laqɨrqolapsɔːtɨt </ts>
               <ts e="T167" id="Seg_3176" n="e" s="T166">İčʼakäčʼikap, </ts>
               <ts e="T168" id="Seg_3178" n="e" s="T167">kätɛntɔːtɨt </ts>
               <ts e="T169" id="Seg_3180" n="e" s="T168">(/kätɨsɔːtɨt) </ts>
               <ts e="T170" id="Seg_3182" n="e" s="T169">Qolʼsak </ts>
               <ts e="T171" id="Seg_3184" n="e" s="T170">iːranɨk </ts>
               <ts e="T172" id="Seg_3186" n="e" s="T171">qɨssɔːtɨt. </ts>
               <ts e="T173" id="Seg_3188" n="e" s="T172">No </ts>
               <ts e="T174" id="Seg_3190" n="e" s="T173">kätɨmpɔːtɨt. </ts>
               <ts e="T175" id="Seg_3192" n="e" s="T174">Načʼalʼnik </ts>
               <ts e="T176" id="Seg_3194" n="e" s="T175">kuralʼtɨstɨ </ts>
               <ts e="T177" id="Seg_3196" n="e" s="T176">tämɨqo </ts>
               <ts e="T178" id="Seg_3198" n="e" s="T177">paŋɨp. </ts>
               <ts e="T179" id="Seg_3200" n="e" s="T178">Sintelɨl </ts>
               <ts e="T180" id="Seg_3202" n="e" s="T179">čʼeːlʼ </ts>
               <ts e="T181" id="Seg_3204" n="e" s="T180">täːmɔːtɨt. </ts>
               <ts e="T182" id="Seg_3206" n="e" s="T181">Qolʼsaq </ts>
               <ts e="T183" id="Seg_3208" n="e" s="T182">ira </ts>
               <ts e="T184" id="Seg_3210" n="e" s="T183">kättɨsɨtɨ:“ </ts>
               <ts e="T185" id="Seg_3212" n="e" s="T184">Paŋɨp </ts>
               <ts e="T186" id="Seg_3214" n="e" s="T185">makke </ts>
               <ts e="T187" id="Seg_3216" n="e" s="T186">miŋelɨt, </ts>
               <ts e="T188" id="Seg_3218" n="e" s="T187">mat </ts>
               <ts e="T189" id="Seg_3220" n="e" s="T188">sʼaqɨlʼtɛntap. </ts>
               <ts e="T190" id="Seg_3222" n="e" s="T189">Ma </ts>
               <ts e="T191" id="Seg_3224" n="e" s="T190">imam </ts>
               <ts e="T192" id="Seg_3226" n="e" s="T191">i </ts>
               <ts e="T193" id="Seg_3228" n="e" s="T192">rapotnikit </ts>
               <ts e="T194" id="Seg_3230" n="e" s="T193">lʼentʼajtɔːtɨt. </ts>
               <ts e="T195" id="Seg_3232" n="e" s="T194">Čʼeːlʼit </ts>
               <ts e="T196" id="Seg_3234" n="e" s="T195">i </ts>
               <ts e="T197" id="Seg_3236" n="e" s="T196">piːt </ts>
               <ts e="T198" id="Seg_3238" n="e" s="T197">qontɔːtɨt”. </ts>
               <ts e="T199" id="Seg_3240" n="e" s="T198">İčʼakäčʼika </ts>
               <ts e="T200" id="Seg_3242" n="e" s="T199">kätɨmpatɨ </ts>
               <ts e="T201" id="Seg_3244" n="e" s="T200">(katɨsɨt):“ </ts>
               <ts e="T202" id="Seg_3246" n="e" s="T201">Paŋɨp </ts>
               <ts e="T203" id="Seg_3248" n="e" s="T202">čʼistaŋ </ts>
               <ts e="T204" id="Seg_3250" n="e" s="T203">märɨŋɨlʼɨt. </ts>
               <ts e="T205" id="Seg_3252" n="e" s="T204">Soma </ts>
               <ts e="T206" id="Seg_3254" n="e" s="T205">qumɨp </ts>
               <ts e="T207" id="Seg_3256" n="e" s="T206">matɨrqo”. </ts>
               <ts e="T208" id="Seg_3258" n="e" s="T207">Qolʼsaq </ts>
               <ts e="T209" id="Seg_3260" n="e" s="T208">ira </ts>
               <ts e="T210" id="Seg_3262" n="e" s="T209">qontɨsa </ts>
               <ts e="T211" id="Seg_3264" n="e" s="T210">qarɨt </ts>
               <ts e="T212" id="Seg_3266" n="e" s="T211">čʼeŋ </ts>
               <ts e="T213" id="Seg_3268" n="e" s="T212">omtɨsa </ts>
               <ts e="T214" id="Seg_3270" n="e" s="T213">(/mäsɨsa). </ts>
               <ts e="T215" id="Seg_3272" n="e" s="T214">“İma </ts>
               <ts e="T216" id="Seg_3274" n="e" s="T215">omtäšik, </ts>
               <ts e="T217" id="Seg_3276" n="e" s="T216">i </ts>
               <ts e="T218" id="Seg_3278" n="e" s="T217">rapotnikit </ts>
               <ts e="T219" id="Seg_3280" n="e" s="T218">omtäŋɨlʼɨt!” </ts>
               <ts e="T220" id="Seg_3282" n="e" s="T219">Täpɨt </ts>
               <ts e="T221" id="Seg_3284" n="e" s="T220">čʼek </ts>
               <ts e="T222" id="Seg_3286" n="e" s="T221">assa </ts>
               <ts e="T223" id="Seg_3288" n="e" s="T222">omnäntɔːtɨt. </ts>
               <ts e="T224" id="Seg_3290" n="e" s="T223">Nʼenʼnʼimɔːssa. </ts>
               <ts e="T225" id="Seg_3292" n="e" s="T224">Illa </ts>
               <ts e="T226" id="Seg_3294" n="e" s="T225">iːsɨtɨ </ts>
               <ts e="T227" id="Seg_3296" n="e" s="T226">paŋamt </ts>
               <ts e="T228" id="Seg_3298" n="e" s="T227">moːtälsɨtɨ </ts>
               <ts e="T229" id="Seg_3300" n="e" s="T228">muntɨk. </ts>
               <ts e="T230" id="Seg_3302" n="e" s="T229">Paŋɨp </ts>
               <ts e="T231" id="Seg_3304" n="e" s="T230">loːqɨrsɨtɨ </ts>
               <ts e="T232" id="Seg_3306" n="e" s="T231">topɔːqɨntɨ </ts>
               <ts e="T233" id="Seg_3308" n="e" s="T232">muntɨk </ts>
               <ts e="T234" id="Seg_3310" n="e" s="T233">mattɨrpɨlʼ </ts>
               <ts e="T235" id="Seg_3312" n="e" s="T234">qumiːqantɨ. </ts>
               <ts e="T236" id="Seg_3314" n="e" s="T235">Čʼap </ts>
               <ts e="T237" id="Seg_3316" n="e" s="T236">kättɨqɨtɨ. </ts>
               <ts e="T238" id="Seg_3318" n="e" s="T237">Melʼte </ts>
               <ts e="T239" id="Seg_3320" n="e" s="T238">qulʼčʼɔːtɨt. </ts>
               <ts e="T240" id="Seg_3322" n="e" s="T239">İra </ts>
               <ts e="T241" id="Seg_3324" n="e" s="T240">nʼenʼnʼmɔːssa </ts>
               <ts e="T242" id="Seg_3326" n="e" s="T241">kazatɨp </ts>
               <ts e="T243" id="Seg_3328" n="e" s="T242">qärɨsɨt. </ts>
               <ts e="T244" id="Seg_3330" n="e" s="T243">“Kun </ts>
               <ts e="T245" id="Seg_3332" n="e" s="T244">iːsaqɨt </ts>
               <ts e="T246" id="Seg_3334" n="e" s="T245">paŋɨp?” </ts>
               <ts e="T247" id="Seg_3336" n="e" s="T246">“Täp </ts>
               <ts e="T248" id="Seg_3338" n="e" s="T247">(İčʼäk) </ts>
               <ts e="T249" id="Seg_3340" n="e" s="T248">mekɨnı </ts>
               <ts e="T250" id="Seg_3342" n="e" s="T249">paŋɨp </ts>
               <ts e="T251" id="Seg_3344" n="e" s="T250">misɨtɨ. </ts>
               <ts e="T252" id="Seg_3346" n="e" s="T251">Täp </ts>
               <ts e="T253" id="Seg_3348" n="e" s="T252">İčʼäkäːčʼika </ts>
               <ts e="T254" id="Seg_3350" n="e" s="T253">ɔːläksa”. </ts>
               <ts e="T255" id="Seg_3352" n="e" s="T254">“Qälʼlʼa </ts>
               <ts e="T256" id="Seg_3354" n="e" s="T255">orqɨlʼqo </ts>
               <ts e="T257" id="Seg_3356" n="e" s="T256">sudʼinqo. </ts>
               <ts e="T258" id="Seg_3358" n="e" s="T257">İmaiːmɨt </ts>
               <ts e="T259" id="Seg_3360" n="e" s="T258">(imam, </ts>
               <ts e="T260" id="Seg_3362" n="e" s="T259">iːmat) </ts>
               <ts e="T261" id="Seg_3364" n="e" s="T260">muntɨk </ts>
               <ts e="T262" id="Seg_3366" n="e" s="T261">qusɔːtɨt </ts>
               <ts e="T263" id="Seg_3368" n="e" s="T262">paŋɨn </ts>
               <ts e="T264" id="Seg_3370" n="e" s="T263">nɔːn”. </ts>
               <ts e="T265" id="Seg_3372" n="e" s="T264">“Tat </ts>
               <ts e="T266" id="Seg_3374" n="e" s="T265">onnäka </ts>
               <ts e="T267" id="Seg_3376" n="e" s="T266">asa </ts>
               <ts e="T268" id="Seg_3378" n="e" s="T267">tɔːtɨk </ts>
               <ts e="T269" id="Seg_3380" n="e" s="T268">mattɨrsa. </ts>
               <ts e="T270" id="Seg_3382" n="e" s="T269">Täpɨp </ts>
               <ts e="T271" id="Seg_3384" n="e" s="T270">qäːčʼiqo </ts>
               <ts e="T272" id="Seg_3386" n="e" s="T271">okkur </ts>
               <ts e="T273" id="Seg_3388" n="e" s="T272">čʼeːl, </ts>
               <ts e="T274" id="Seg_3390" n="e" s="T273">meː </ts>
               <ts e="T275" id="Seg_3392" n="e" s="T274">aj </ts>
               <ts e="T276" id="Seg_3394" n="e" s="T275">saŋartɔːmɨt. </ts>
               <ts e="T277" id="Seg_3396" n="e" s="T276">Me </ts>
               <ts e="T278" id="Seg_3398" n="e" s="T277">iːmaiːmɨt </ts>
               <ts e="T279" id="Seg_3400" n="e" s="T278">aj </ts>
               <ts e="T280" id="Seg_3402" n="e" s="T279">lʼentʼajtɔːtɨt”. </ts>
               <ts e="T281" id="Seg_3404" n="e" s="T280">Qolʼsaq </ts>
               <ts e="T282" id="Seg_3406" n="e" s="T281">ira </ts>
               <ts e="T283" id="Seg_3408" n="e" s="T282">aj </ts>
               <ts e="T284" id="Seg_3410" n="e" s="T283">kätɨsɨtɨ:“ </ts>
               <ts e="T285" id="Seg_3412" n="e" s="T284">Lanno, </ts>
               <ts e="T286" id="Seg_3414" n="e" s="T285">saŋarŋɨlɨt </ts>
               <ts e="T287" id="Seg_3416" n="e" s="T286">paŋɨp!” </ts>
               <ts e="T288" id="Seg_3418" n="e" s="T287">Kazat </ts>
               <ts e="T289" id="Seg_3420" n="e" s="T288">šität </ts>
               <ts e="T290" id="Seg_3422" n="e" s="T289">nılʼčʼik </ts>
               <ts e="T291" id="Seg_3424" n="e" s="T290">mattälsɨtɨ </ts>
               <ts e="T292" id="Seg_3426" n="e" s="T291">onti </ts>
               <ts e="T293" id="Seg_3428" n="e" s="T292">imaiːt. </ts>
               <ts e="T294" id="Seg_3430" n="e" s="T293">İːmaiːt </ts>
               <ts e="T295" id="Seg_3432" n="e" s="T294">qusɔːtɨt </ts>
               <ts e="T296" id="Seg_3434" n="e" s="T295">kämɨt </ts>
               <ts e="T297" id="Seg_3436" n="e" s="T296">čʼɔːrɨk. </ts>
               <ts e="T298" id="Seg_3438" n="e" s="T297">Qarɨt </ts>
               <ts e="T299" id="Seg_3440" n="e" s="T298">kätɨsɔːtɨt </ts>
               <ts e="T300" id="Seg_3442" n="e" s="T299">Qolʼsaq </ts>
               <ts e="T301" id="Seg_3444" n="e" s="T300">iranɨk:“ </ts>
               <ts e="T302" id="Seg_3446" n="e" s="T301">Meː </ts>
               <ts e="T303" id="Seg_3448" n="e" s="T302">imaiːmɨt </ts>
               <ts e="T304" id="Seg_3450" n="e" s="T303">melʼte </ts>
               <ts e="T305" id="Seg_3452" n="e" s="T304">qusɔːtɨt”. </ts>
               <ts e="T306" id="Seg_3454" n="e" s="T305">İra </ts>
               <ts e="T307" id="Seg_3456" n="e" s="T306">nʼenʼnʼamɔːssa. </ts>
               <ts e="T308" id="Seg_3458" n="e" s="T307">“İčʼakäčʼikap </ts>
               <ts e="T309" id="Seg_3460" n="e" s="T308">orqɨlʼqo </ts>
               <ts e="T310" id="Seg_3462" n="e" s="T309">nɔːtna”. </ts>
               <ts e="T311" id="Seg_3464" n="e" s="T310">Kazat </ts>
               <ts e="T312" id="Seg_3466" n="e" s="T311">qässɔːtɨt. </ts>
               <ts e="T313" id="Seg_3468" n="e" s="T312">İčʼakäčʼika </ts>
               <ts e="T314" id="Seg_3470" n="e" s="T313">na </ts>
               <ts e="T315" id="Seg_3472" n="e" s="T314">vremʼaqɨt </ts>
               <ts e="T316" id="Seg_3474" n="e" s="T315">mɔːtqɨntɨ. </ts>
               <ts e="T317" id="Seg_3476" n="e" s="T316">İčʼakäčʼikap </ts>
               <ts e="T318" id="Seg_3478" n="e" s="T317">orqɨlʼsɔːtɨt. </ts>
               <ts e="T319" id="Seg_3480" n="e" s="T318">“Seːp </ts>
               <ts e="T320" id="Seg_3482" n="e" s="T319">moːlmɨttɨsantɨ. </ts>
               <ts e="T321" id="Seg_3484" n="e" s="T320">Me </ts>
               <ts e="T322" id="Seg_3486" n="e" s="T321">muntɨk </ts>
               <ts e="T323" id="Seg_3488" n="e" s="T322">iːmaiːmɨt </ts>
               <ts e="T324" id="Seg_3490" n="e" s="T323">i </ts>
               <ts e="T325" id="Seg_3492" n="e" s="T324">rapotnikit </ts>
               <ts e="T326" id="Seg_3494" n="e" s="T325">mattälsimɨt, </ts>
               <ts e="T327" id="Seg_3496" n="e" s="T326">täpɨt </ts>
               <ts e="T328" id="Seg_3498" n="e" s="T327">qusɔːtɨt </ts>
               <ts e="T329" id="Seg_3500" n="e" s="T328">kämɨt </ts>
               <ts e="T330" id="Seg_3502" n="e" s="T329">čʼɔːrɨk”. </ts>
               <ts e="T331" id="Seg_3504" n="e" s="T330">İčʼakäčʼikap </ts>
               <ts e="T332" id="Seg_3506" n="e" s="T331">qäntɨsɔːtɨt </ts>
               <ts e="T333" id="Seg_3508" n="e" s="T332">türmantɨ. </ts>
               <ts e="T334" id="Seg_3510" n="e" s="T333">Kolʼsaq </ts>
               <ts e="T335" id="Seg_3512" n="e" s="T334">ira </ts>
               <ts e="T336" id="Seg_3514" n="e" s="T335">kuraltistɨ </ts>
               <ts e="T337" id="Seg_3516" n="e" s="T336">ıllä </ts>
               <ts e="T338" id="Seg_3518" n="e" s="T337">qottɨrɨqo </ts>
               <ts e="T339" id="Seg_3520" n="e" s="T338">üttɨ. </ts>
               <ts e="T340" id="Seg_3522" n="e" s="T339">Kätɨsɨtɨ </ts>
               <ts e="T341" id="Seg_3524" n="e" s="T340">sɨːrɨt </ts>
               <ts e="T342" id="Seg_3526" n="e" s="T341">qopoːntɨ </ts>
               <ts e="T343" id="Seg_3528" n="e" s="T342">šünʼnʼontɨ </ts>
               <ts e="T344" id="Seg_3530" n="e" s="T343">šütqo </ts>
               <ts e="T345" id="Seg_3532" n="e" s="T344">ilʼelä. </ts>
               <ts e="T346" id="Seg_3534" n="e" s="T345">Kazat </ts>
               <ts e="T347" id="Seg_3536" n="e" s="T346">täpɨp </ts>
               <ts e="T348" id="Seg_3538" n="e" s="T347">qäntɨsɔːtɨt </ts>
               <ts e="T349" id="Seg_3540" n="e" s="T348">čʼuntɨlʼ </ts>
               <ts e="T350" id="Seg_3542" n="e" s="T349">qaqlɨsä </ts>
               <ts e="T351" id="Seg_3544" n="e" s="T350">üt </ts>
               <ts e="T352" id="Seg_3546" n="e" s="T351">toːptɨ, </ts>
               <ts e="T353" id="Seg_3548" n="e" s="T352">moret </ts>
               <ts e="T354" id="Seg_3550" n="e" s="T353">qanɨktɨ. </ts>
               <ts e="T355" id="Seg_3552" n="e" s="T354">Täp </ts>
               <ts e="T356" id="Seg_3554" n="e" s="T355">kota </ts>
               <ts e="T357" id="Seg_3556" n="e" s="T356">čʼuntoːqɨt </ts>
               <ts e="T358" id="Seg_3558" n="e" s="T357">ilɨla </ts>
               <ts e="T359" id="Seg_3560" n="e" s="T358">ippa. </ts>
               <ts e="T360" id="Seg_3562" n="e" s="T359">Kätɨsɨtɨ:“ </ts>
               <ts e="T361" id="Seg_3564" n="e" s="T360">Masɨp </ts>
               <ts e="T362" id="Seg_3566" n="e" s="T361">qoptɨrantɨlʼɨt </ts>
               <ts e="T363" id="Seg_3568" n="e" s="T362">marqɨ </ts>
               <ts e="T364" id="Seg_3570" n="e" s="T363">püsa, </ts>
               <ts e="T365" id="Seg_3572" n="e" s="T364">marqɨ </ts>
               <ts e="T366" id="Seg_3574" n="e" s="T365">pü </ts>
               <ts e="T367" id="Seg_3576" n="e" s="T366">peːqɨlʼnɨkɨt”. </ts>
               <ts e="T368" id="Seg_3578" n="e" s="T367">Täpɨt </ts>
               <ts e="T369" id="Seg_3580" n="e" s="T368">qässɔːtɨt </ts>
               <ts e="T370" id="Seg_3582" n="e" s="T369">peːqo </ts>
               <ts e="T371" id="Seg_3584" n="e" s="T370">marqɨ </ts>
               <ts e="T372" id="Seg_3586" n="e" s="T371">püp. </ts>
               <ts e="T373" id="Seg_3588" n="e" s="T372">Kuntɨ </ts>
               <ts e="T374" id="Seg_3590" n="e" s="T373">lʼi </ts>
               <ts e="T375" id="Seg_3592" n="e" s="T374">qɔːmɨčʼa </ts>
               <ts e="T376" id="Seg_3594" n="e" s="T375">ippɨmpa, </ts>
               <ts e="T377" id="Seg_3596" n="e" s="T376">üŋkeltɨmpat </ts>
               <ts e="T378" id="Seg_3598" n="e" s="T377">čʼuntɨlʼ </ts>
               <ts e="T379" id="Seg_3600" n="e" s="T378">qaqlɨ </ts>
               <ts e="T380" id="Seg_3602" n="e" s="T379">tünta. </ts>
               <ts e="T381" id="Seg_3604" n="e" s="T380">Tɛːttɨ </ts>
               <ts e="T382" id="Seg_3606" n="e" s="T381">lʼi </ts>
               <ts e="T383" id="Seg_3608" n="e" s="T382">sompɨlʼa </ts>
               <ts e="T384" id="Seg_3610" n="e" s="T383">qaqlɨt </ts>
               <ts e="T385" id="Seg_3612" n="e" s="T384">tüːnta. </ts>
               <ts e="T386" id="Seg_3614" n="e" s="T385">Utɨrɨsɔːtɨt. </ts>
               <ts e="T387" id="Seg_3616" n="e" s="T386">Tämqup </ts>
               <ts e="T388" id="Seg_3618" n="e" s="T387">tɔːqsä </ts>
               <ts e="T389" id="Seg_3620" n="e" s="T388">(tawarsä, </ts>
               <ts e="T390" id="Seg_3622" n="e" s="T389">lɨptɨksä) </ts>
               <ts e="T391" id="Seg_3624" n="e" s="T390">qänta </ts>
               <ts e="T392" id="Seg_3626" n="e" s="T391">moqɨnä. </ts>
               <ts e="T393" id="Seg_3628" n="e" s="T392">Tämqup </ts>
               <ts e="T394" id="Seg_3630" n="e" s="T393">topɔːlsɨtɨ </ts>
               <ts e="T395" id="Seg_3632" n="e" s="T394">sɨːrɨt </ts>
               <ts e="T396" id="Seg_3634" n="e" s="T395">qopɨlʼ </ts>
               <ts e="T397" id="Seg_3636" n="e" s="T396">kotap. </ts>
               <ts e="T398" id="Seg_3638" n="e" s="T397">Qoi </ts>
               <ts e="T399" id="Seg_3640" n="e" s="T398">tɨmtɨ </ts>
               <ts e="T400" id="Seg_3642" n="e" s="T399">ippa? </ts>
               <ts e="T401" id="Seg_3644" n="e" s="T400">İčʼikäčʼika </ts>
               <ts e="T402" id="Seg_3646" n="e" s="T401">laŋkalʼsa:“ </ts>
               <ts e="T403" id="Seg_3648" n="e" s="T402">İjoː!” </ts>
               <ts e="T404" id="Seg_3650" n="e" s="T403">Tämqup:“ </ts>
               <ts e="T405" id="Seg_3652" n="e" s="T404">Qoitqo </ts>
               <ts e="T406" id="Seg_3654" n="e" s="T405">ippant?” </ts>
               <ts e="T407" id="Seg_3656" n="e" s="T406">İčʼakäčʼika </ts>
               <ts e="T408" id="Seg_3658" n="e" s="T407">kätɨsɨtɨ:“ </ts>
               <ts e="T409" id="Seg_3660" n="e" s="T408">Innä </ts>
               <ts e="T410" id="Seg_3662" n="e" s="T409">sım </ts>
               <ts e="T411" id="Seg_3664" n="e" s="T410">üːtas, </ts>
               <ts e="T412" id="Seg_3666" n="e" s="T411">toː </ts>
               <ts e="T413" id="Seg_3668" n="e" s="T412">maːtaltɨ!” </ts>
               <ts e="T414" id="Seg_3670" n="e" s="T413">Täp </ts>
               <ts e="T415" id="Seg_3672" n="e" s="T414">toː </ts>
               <ts e="T416" id="Seg_3674" n="e" s="T415">maːtalʼolʼsɨt. </ts>
               <ts e="T417" id="Seg_3676" n="e" s="T416">Täp </ts>
               <ts e="T418" id="Seg_3678" n="e" s="T417">ınnä </ts>
               <ts e="T419" id="Seg_3680" n="e" s="T418">putalʼmɔːssa </ts>
               <ts e="T420" id="Seg_3682" n="e" s="T419">(tantɨsa). </ts>
               <ts e="T421" id="Seg_3684" n="e" s="T420">Täpɨn </ts>
               <ts e="T422" id="Seg_3686" n="e" s="T421">mɨqɨt </ts>
               <ts e="T423" id="Seg_3688" n="e" s="T422">sıːt </ts>
               <ts e="T424" id="Seg_3690" n="e" s="T423">qopɨlʼ </ts>
               <ts e="T425" id="Seg_3692" n="e" s="T424">mɨ </ts>
               <ts e="T426" id="Seg_3694" n="e" s="T425">ɛppa </ts>
               <ts e="T427" id="Seg_3696" n="e" s="T426">muŋkɨntɨ. </ts>
               <ts e="T428" id="Seg_3698" n="e" s="T427">Tämqup </ts>
               <ts e="T429" id="Seg_3700" n="e" s="T428">soqonʼnʼet:“ </ts>
               <ts e="T430" id="Seg_3702" n="e" s="T429">Qojtqo </ts>
               <ts e="T431" id="Seg_3704" n="e" s="T430">ippantɨ?” </ts>
               <ts e="T432" id="Seg_3706" n="e" s="T431">“Mat </ts>
               <ts e="T433" id="Seg_3708" n="e" s="T432">ippap, </ts>
               <ts e="T434" id="Seg_3710" n="e" s="T433">üttɨ </ts>
               <ts e="T435" id="Seg_3712" n="e" s="T434">pattɨrsak </ts>
               <ts e="T436" id="Seg_3714" n="e" s="T435">sıːt </ts>
               <ts e="T437" id="Seg_3716" n="e" s="T436">qopɨp </ts>
               <ts e="T438" id="Seg_3718" n="e" s="T437">iːsap. </ts>
               <ts e="T439" id="Seg_3720" n="e" s="T438">Ormɨ </ts>
               <ts e="T440" id="Seg_3722" n="e" s="T439">čʼäːŋkɨsa, </ts>
               <ts e="T441" id="Seg_3724" n="e" s="T440">tɔːq </ts>
               <ts e="T442" id="Seg_3726" n="e" s="T441">(lɨptɨk) </ts>
               <ts e="T443" id="Seg_3728" n="e" s="T442">kočʼčʼɨ </ts>
               <ts e="T444" id="Seg_3730" n="e" s="T443">ütqɨt </ts>
               <ts e="T445" id="Seg_3732" n="e" s="T444">ınnä”. </ts>
               <ts e="T446" id="Seg_3734" n="e" s="T445">Atɨlʼsitɨ </ts>
               <ts e="T447" id="Seg_3736" n="e" s="T446">sıːt </ts>
               <ts e="T448" id="Seg_3738" n="e" s="T447">qopɨtɨp </ts>
               <ts e="T449" id="Seg_3740" n="e" s="T448">tämqumnɨk. </ts>
               <ts e="T450" id="Seg_3742" n="e" s="T449">Tämqum </ts>
               <ts e="T451" id="Seg_3744" n="e" s="T450">ɔːntalʼsa:“ </ts>
               <ts e="T452" id="Seg_3746" n="e" s="T451">Ma </ts>
               <ts e="T453" id="Seg_3748" n="e" s="T452">aj </ts>
               <ts e="T454" id="Seg_3750" n="e" s="T453">pattɨrläk </ts>
               <ts e="T455" id="Seg_3752" n="e" s="T454">ütt”. </ts>
               <ts e="T456" id="Seg_3754" n="e" s="T455">Täpɨp </ts>
               <ts e="T457" id="Seg_3756" n="e" s="T456">süttɛːsitɨ </ts>
               <ts e="T458" id="Seg_3758" n="e" s="T457">sɨːrɨt </ts>
               <ts e="T459" id="Seg_3760" n="e" s="T458">kotantɨ. </ts>
               <ts e="T460" id="Seg_3762" n="e" s="T459">Tämqup </ts>
               <ts e="T461" id="Seg_3764" n="e" s="T460">ippa </ts>
               <ts e="T753" id="Seg_3766" n="e" s="T461">üt</ts>
               <ts e="T462" id="Seg_3768" n="e" s="T753">totqɨp </ts>
               <ts e="T463" id="Seg_3770" n="e" s="T462">kotaqɨt. </ts>
               <ts e="T464" id="Seg_3772" n="e" s="T463">İčʼakäčʼika </ts>
               <ts e="T465" id="Seg_3774" n="e" s="T464">qənnajsa, </ts>
               <ts e="T466" id="Seg_3776" n="e" s="T465">čʼuntɨlʼ </ts>
               <ts e="T467" id="Seg_3778" n="e" s="T466">qaqlɨntɨsa </ts>
               <ts e="T468" id="Seg_3780" n="e" s="T467">i </ts>
               <ts e="T469" id="Seg_3782" n="e" s="T468">tɔːqsa. </ts>
               <ts e="T470" id="Seg_3784" n="e" s="T469">Kazat </ts>
               <ts e="T471" id="Seg_3786" n="e" s="T470">taːtɨsɔːtɨt </ts>
               <ts e="T472" id="Seg_3788" n="e" s="T471">marqɨ </ts>
               <ts e="T473" id="Seg_3790" n="e" s="T472">püp </ts>
               <ts e="T474" id="Seg_3792" n="e" s="T473">sɔːrɨqolapsɔːtɨt. </ts>
               <ts e="T475" id="Seg_3794" n="e" s="T474">Täntɔːtɨt </ts>
               <ts e="T476" id="Seg_3796" n="e" s="T475">ontɨ </ts>
               <ts e="T477" id="Seg_3798" n="e" s="T476">nʼantɨ:“ </ts>
               <ts e="T478" id="Seg_3800" n="e" s="T477">Seːp </ts>
               <ts e="T479" id="Seg_3802" n="e" s="T478">İčʼakäčʼika </ts>
               <ts e="T480" id="Seg_3804" n="e" s="T479">olɨŋɨrsal. </ts>
               <ts e="T481" id="Seg_3806" n="e" s="T480">Ütqɨt </ts>
               <ts e="T482" id="Seg_3808" n="e" s="T481">qunnant”. </ts>
               <ts e="T483" id="Seg_3810" n="e" s="T482">Tämqup </ts>
               <ts e="T484" id="Seg_3812" n="e" s="T483">üŋkeltɨmpat </ts>
               <ts e="T485" id="Seg_3814" n="e" s="T484">sɔːrɨqantaltɨmpɔːtɨt </ts>
               <ts e="T486" id="Seg_3816" n="e" s="T485">püp, </ts>
               <ts e="T487" id="Seg_3818" n="e" s="T486">marqɨ </ts>
               <ts e="T488" id="Seg_3820" n="e" s="T487">püp. </ts>
               <ts e="T489" id="Seg_3822" n="e" s="T488">“Asa </ts>
               <ts e="T490" id="Seg_3824" n="e" s="T489">mat </ts>
               <ts e="T491" id="Seg_3826" n="e" s="T490">İčʼikäčʼika. </ts>
               <ts e="T492" id="Seg_3828" n="e" s="T491">İčʼäkäčʼika </ts>
               <ts e="T493" id="Seg_3830" n="e" s="T492">qəssa. </ts>
               <ts e="T494" id="Seg_3832" n="e" s="T493">Tap </ts>
               <ts e="T495" id="Seg_3834" n="e" s="T494">masım </ts>
               <ts e="T496" id="Seg_3836" n="e" s="T495">oːlʼɨksa. </ts>
               <ts e="T497" id="Seg_3838" n="e" s="T496">Mat </ts>
               <ts e="T498" id="Seg_3840" n="e" s="T497">tämqumoŋok. </ts>
               <ts e="T499" id="Seg_3842" n="e" s="T498">Olʼɨklä </ts>
               <ts e="T500" id="Seg_3844" n="e" s="T499">masɨp </ts>
               <ts e="T501" id="Seg_3846" n="e" s="T500">süssa </ts>
               <ts e="T502" id="Seg_3848" n="e" s="T501">qopɨlʼ </ts>
               <ts e="T503" id="Seg_3850" n="e" s="T502">kotantɨ”. </ts>
               <ts e="T504" id="Seg_3852" n="e" s="T503">Kasat </ts>
               <ts e="T505" id="Seg_3854" n="e" s="T504">tomnat:“ </ts>
               <ts e="T506" id="Seg_3856" n="e" s="T505">Tat </ts>
               <ts e="T507" id="Seg_3858" n="e" s="T506">İčʼäkäčʼikantɨ </ts>
               <ts e="T508" id="Seg_3860" n="e" s="T507">seːp </ts>
               <ts e="T509" id="Seg_3862" n="e" s="T508">moːlmɨtɨsantɨ”. </ts>
               <ts e="T510" id="Seg_3864" n="e" s="T509">Toːnna </ts>
               <ts e="T511" id="Seg_3866" n="e" s="T510">čʼuːrɨla </ts>
               <ts e="T512" id="Seg_3868" n="e" s="T511">laŋkɨnʼnʼa </ts>
               <ts e="T513" id="Seg_3870" n="e" s="T512">kotaqɨt:“ </ts>
               <ts e="T514" id="Seg_3872" n="e" s="T513">Mat </ts>
               <ts e="T515" id="Seg_3874" n="e" s="T514">tämqumoŋok. </ts>
               <ts e="T516" id="Seg_3876" n="e" s="T515">Tüntɨsak </ts>
               <ts e="T517" id="Seg_3878" n="e" s="T516">čʼuntɨsa, </ts>
               <ts e="T518" id="Seg_3880" n="e" s="T517">qaqlɨsa”. </ts>
               <ts e="T519" id="Seg_3882" n="e" s="T518">Üttɨ </ts>
               <ts e="T520" id="Seg_3884" n="e" s="T519">tulʼtɨlʼa </ts>
               <ts e="T521" id="Seg_3886" n="e" s="T520">čʼattɨsɔːtɨt </ts>
               <ts e="T522" id="Seg_3888" n="e" s="T521">qoptɨlʼsɔːtɨt </ts>
               <ts e="T523" id="Seg_3890" n="e" s="T522">püːsa. </ts>
               <ts e="T524" id="Seg_3892" n="e" s="T523">Moqɨnä </ts>
               <ts e="T525" id="Seg_3894" n="e" s="T524">qässɔːtɨt. </ts>
               <ts e="T526" id="Seg_3896" n="e" s="T525">“Meː </ts>
               <ts e="T527" id="Seg_3898" n="e" s="T526">qoptɨrsɨmɨt </ts>
               <ts e="T528" id="Seg_3900" n="e" s="T527">üttɨ”. </ts>
               <ts e="T529" id="Seg_3902" n="e" s="T528">İčʼikäčʼika </ts>
               <ts e="T530" id="Seg_3904" n="e" s="T529">tüsa </ts>
               <ts e="T531" id="Seg_3906" n="e" s="T530">moqɨna </ts>
               <ts e="T532" id="Seg_3908" n="e" s="T531">čʼuntɨsa, </ts>
               <ts e="T533" id="Seg_3910" n="e" s="T532">čʼuntɨlʼ </ts>
               <ts e="T534" id="Seg_3912" n="e" s="T533">qaqlɨsa. </ts>
               <ts e="T535" id="Seg_3914" n="e" s="T534">Tɔːqtɨ </ts>
               <ts e="T536" id="Seg_3916" n="e" s="T535">üssä </ts>
               <ts e="T537" id="Seg_3918" n="e" s="T536">čʼontaptɨstɨ </ts>
               <ts e="T538" id="Seg_3920" n="e" s="T537">tɛːmnoːntɨ </ts>
               <ts e="T539" id="Seg_3922" n="e" s="T538">ınnä </ts>
               <ts e="T540" id="Seg_3924" n="e" s="T539">ɨːtälsitɨ </ts>
               <ts e="T541" id="Seg_3926" n="e" s="T540">täkɨqo. </ts>
               <ts e="T542" id="Seg_3928" n="e" s="T541">Na </ts>
               <ts e="T543" id="Seg_3930" n="e" s="T542">vremʼaqɨt </ts>
               <ts e="T544" id="Seg_3932" n="e" s="T543">načʼalʼnik </ts>
               <ts e="T545" id="Seg_3934" n="e" s="T544">Qolʼsak </ts>
               <ts e="T546" id="Seg_3936" n="e" s="T545">ira </ts>
               <ts e="T547" id="Seg_3938" n="e" s="T546">mennɨmpa </ts>
               <ts e="T548" id="Seg_3940" n="e" s="T547">İčʼakäčʼikat </ts>
               <ts e="T549" id="Seg_3942" n="e" s="T548">mɔːtɨlʼ </ts>
               <ts e="T550" id="Seg_3944" n="e" s="T549">pɛlʼaktɨ </ts>
               <ts e="T551" id="Seg_3946" n="e" s="T550">šoːqɨr </ts>
               <ts e="T552" id="Seg_3948" n="e" s="T551">čʼɔːtɨmpɨt, </ts>
               <ts e="T553" id="Seg_3950" n="e" s="T552">purqɨ </ts>
               <ts e="T554" id="Seg_3952" n="e" s="T553">qatqa </ts>
               <ts e="T555" id="Seg_3954" n="e" s="T554">šoːqɨr </ts>
               <ts e="T556" id="Seg_3956" n="e" s="T555">nɔːnɨ. </ts>
               <ts e="T557" id="Seg_3958" n="e" s="T556">Tɨmtɨsä </ts>
               <ts e="T558" id="Seg_3960" n="e" s="T557">(nɨmtɨsä) </ts>
               <ts e="T559" id="Seg_3962" n="e" s="T558">kättɨsɨtɨ </ts>
               <ts e="T560" id="Seg_3964" n="e" s="T559">kazatɨqınik </ts>
               <ts e="T561" id="Seg_3966" n="e" s="T560">qälʼlʼa </ts>
               <ts e="T562" id="Seg_3968" n="e" s="T561">mennɨmpɨqo </ts>
               <ts e="T563" id="Seg_3970" n="e" s="T562">imaqotap. </ts>
               <ts e="T564" id="Seg_3972" n="e" s="T563">Täpɨt </ts>
               <ts e="T565" id="Seg_3974" n="e" s="T564">qäːssɔːtɨt. </ts>
               <ts e="T566" id="Seg_3976" n="e" s="T565">Mɔːttɨ </ts>
               <ts e="T567" id="Seg_3978" n="e" s="T566">seːrsɔːtɨt, </ts>
               <ts e="T568" id="Seg_3980" n="e" s="T567">imaqotat </ts>
               <ts e="T569" id="Seg_3982" n="e" s="T568">koptɨ </ts>
               <ts e="T570" id="Seg_3984" n="e" s="T569">čʼɔːt </ts>
               <ts e="T571" id="Seg_3986" n="e" s="T570">omta </ts>
               <ts e="T572" id="Seg_3988" n="e" s="T571">stolɨn </ts>
               <ts e="T573" id="Seg_3990" n="e" s="T572">iːqɨt </ts>
               <ts e="T574" id="Seg_3992" n="e" s="T573">İčʼäkäčʼika </ts>
               <ts e="T575" id="Seg_3994" n="e" s="T574">nʼenʼnʼamɔːtpa. </ts>
               <ts e="T576" id="Seg_3996" n="e" s="T575">Muntɨk </ts>
               <ts e="T577" id="Seg_3998" n="e" s="T576">mäntɨtɨ </ts>
               <ts e="T578" id="Seg_4000" n="e" s="T577">qatolʼpat </ts>
               <ts e="T579" id="Seg_4002" n="e" s="T578">kämčʼa, </ts>
               <ts e="T580" id="Seg_4004" n="e" s="T579">porqɨmtɨ </ts>
               <ts e="T581" id="Seg_4006" n="e" s="T580">nɨtɨmpɨlʼa. </ts>
               <ts e="T582" id="Seg_4008" n="e" s="T581">Täpɨt </ts>
               <ts e="T583" id="Seg_4010" n="e" s="T582">kätɨsɔːt </ts>
               <ts e="T584" id="Seg_4012" n="e" s="T583">(seːrsɔːtɨt)–“ </ts>
               <ts e="T585" id="Seg_4014" n="e" s="T584">Dorova, </ts>
               <ts e="T586" id="Seg_4016" n="e" s="T585">lʼaqa”. </ts>
               <ts e="T587" id="Seg_4018" n="e" s="T586">A </ts>
               <ts e="T588" id="Seg_4020" n="e" s="T587">İčʼekäčʼika </ts>
               <ts e="T589" id="Seg_4022" n="e" s="T588">nenʼnʼimɔːlla </ts>
               <ts e="T590" id="Seg_4024" n="e" s="T589">omta:“ </ts>
               <ts e="T591" id="Seg_4026" n="e" s="T590">Tɛː </ts>
               <ts e="T592" id="Seg_4028" n="e" s="T591">masɨp </ts>
               <ts e="T593" id="Seg_4030" n="e" s="T592">ütɨsɨp </ts>
               <ts e="T594" id="Seg_4032" n="e" s="T593">qoptɨrɨsɨlɨt, </ts>
               <ts e="T595" id="Seg_4034" n="e" s="T594">mat </ts>
               <ts e="T596" id="Seg_4036" n="e" s="T595">morʼan </ts>
               <ts e="T597" id="Seg_4038" n="e" s="T596">ılqɨt </ts>
               <ts e="T598" id="Seg_4040" n="e" s="T597">kotčʼik </ts>
               <ts e="T599" id="Seg_4042" n="e" s="T598">tɔːq </ts>
               <ts e="T600" id="Seg_4044" n="e" s="T599">(lɨptɨk) </ts>
               <ts e="T601" id="Seg_4046" n="e" s="T600">qosap. </ts>
               <ts e="T602" id="Seg_4048" n="e" s="T601">Näsaŋ </ts>
               <ts e="T603" id="Seg_4050" n="e" s="T602">eŋa </ts>
               <ts e="T604" id="Seg_4052" n="e" s="T603">kusak </ts>
               <ts e="T605" id="Seg_4054" n="e" s="T604">orɨm </ts>
               <ts e="T606" id="Seg_4056" n="e" s="T605">ɛːsa </ts>
               <ts e="T607" id="Seg_4058" n="e" s="T606">iːsap </ts>
               <ts e="T608" id="Seg_4060" n="e" s="T607">(ma </ts>
               <ts e="T609" id="Seg_4062" n="e" s="T608">orɨmɨ </ts>
               <ts e="T610" id="Seg_4064" n="e" s="T609">asa </ts>
               <ts e="T611" id="Seg_4066" n="e" s="T610">iːsɨtɨ). </ts>
               <ts e="T612" id="Seg_4068" n="e" s="T611">Bɨtta </ts>
               <ts e="T613" id="Seg_4070" n="e" s="T612">ütenɨlʼ </ts>
               <ts e="T614" id="Seg_4072" n="e" s="T613">nomqɨn </ts>
               <ts e="T615" id="Seg_4074" n="e" s="T614">ɛːsak”. </ts>
               <ts e="T616" id="Seg_4076" n="e" s="T615">Täpɨt </ts>
               <ts e="T617" id="Seg_4078" n="e" s="T616">qässɔːtɨt </ts>
               <ts e="T618" id="Seg_4080" n="e" s="T617">nɨrkɨmɔːllä:“ </ts>
               <ts e="T619" id="Seg_4082" n="e" s="T618">Qoilʼ </ts>
               <ts e="T620" id="Seg_4084" n="e" s="T619">jabol </ts>
               <ts e="T621" id="Seg_4086" n="e" s="T620">ɛːsa”. </ts>
               <ts e="T622" id="Seg_4088" n="e" s="T621">Qolʼsak </ts>
               <ts e="T623" id="Seg_4090" n="e" s="T622">iranɨk </ts>
               <ts e="T624" id="Seg_4092" n="e" s="T623">kätɨqo:“ </ts>
               <ts e="T625" id="Seg_4094" n="e" s="T624">İčʼakäčʼikap </ts>
               <ts e="T626" id="Seg_4096" n="e" s="T625">üttɨ </ts>
               <ts e="T627" id="Seg_4098" n="e" s="T626">qoptɨrsɨmɨt </ts>
               <ts e="T628" id="Seg_4100" n="e" s="T627">ikolʼ </ts>
               <ts e="T629" id="Seg_4102" n="e" s="T628">čʼeːlʼe </ts>
               <ts e="T630" id="Seg_4104" n="e" s="T629">(okot) </ts>
               <ts e="T631" id="Seg_4106" n="e" s="T630">täp </ts>
               <ts e="T632" id="Seg_4108" n="e" s="T631">nassa </ts>
               <ts e="T633" id="Seg_4110" n="e" s="T632">qompatɨ </ts>
               <ts e="T634" id="Seg_4112" n="e" s="T633">tɔːq </ts>
               <ts e="T635" id="Seg_4114" n="e" s="T634">(lɨptɨk) </ts>
               <ts e="T636" id="Seg_4116" n="e" s="T635">kušalʼ </ts>
               <ts e="T637" id="Seg_4118" n="e" s="T636">tätaqoj </ts>
               <ts e="T638" id="Seg_4120" n="e" s="T637">muntɨk </ts>
               <ts e="T639" id="Seg_4122" n="e" s="T638">qompatɨ </ts>
               <ts e="T640" id="Seg_4124" n="e" s="T639">i </ts>
               <ts e="T641" id="Seg_4126" n="e" s="T640">sıːt </ts>
               <ts e="T642" id="Seg_4128" n="e" s="T641">qopɨlʼ </ts>
               <ts e="T643" id="Seg_4130" n="e" s="T642">mɨt”. </ts>
               <ts e="T644" id="Seg_4132" n="e" s="T643">Qolʼsak </ts>
               <ts e="T645" id="Seg_4134" n="e" s="T644">ira </ts>
               <ts e="T646" id="Seg_4136" n="e" s="T645">tɛnɨtɨ </ts>
               <ts e="T647" id="Seg_4138" n="e" s="T646">ürɨsa. </ts>
               <ts e="T648" id="Seg_4140" n="e" s="T647">“Qäntɨk </ts>
               <ts e="T649" id="Seg_4142" n="e" s="T648">nılʼčʼik </ts>
               <ts e="T650" id="Seg_4144" n="e" s="T649">ɛsɨsa? </ts>
               <ts e="T651" id="Seg_4146" n="e" s="T650">İčʼekäčʼika </ts>
               <ts e="T652" id="Seg_4148" n="e" s="T651">jabəlʼ, </ts>
               <ts e="T653" id="Seg_4150" n="e" s="T652">mannon </ts>
               <ts e="T654" id="Seg_4152" n="e" s="T653">(menon)”. </ts>
               <ts e="T655" id="Seg_4154" n="e" s="T654">Šintelʼ </ts>
               <ts e="T656" id="Seg_4156" n="e" s="T655">čʼeːlʼ </ts>
               <ts e="T657" id="Seg_4158" n="e" s="T656">ontɨ </ts>
               <ts e="T658" id="Seg_4160" n="e" s="T657">šität </ts>
               <ts e="T659" id="Seg_4162" n="e" s="T658">kasat </ts>
               <ts e="T660" id="Seg_4164" n="e" s="T659">tüŋčʼɔːtɨt, </ts>
               <ts e="T661" id="Seg_4166" n="e" s="T660">İčʼekäčʼikantɨ </ts>
               <ts e="T662" id="Seg_4168" n="e" s="T661">üːtɨtɨ </ts>
               <ts e="T663" id="Seg_4170" n="e" s="T662">kasak </ts>
               <ts e="T664" id="Seg_4172" n="e" s="T663">ira </ts>
               <ts e="T665" id="Seg_4174" n="e" s="T664">soqɨšqo </ts>
               <ts e="T666" id="Seg_4176" n="e" s="T665">İčʼakäčʼikap </ts>
               <ts e="T667" id="Seg_4178" n="e" s="T666">kuttar </ts>
               <ts e="T668" id="Seg_4180" n="e" s="T667">qosɨtɨ </ts>
               <ts e="T669" id="Seg_4182" n="e" s="T668">na </ts>
               <ts e="T670" id="Seg_4184" n="e" s="T669">to </ts>
               <ts e="T671" id="Seg_4186" n="e" s="T670">lɨptɨtɨp. </ts>
               <ts e="T672" id="Seg_4188" n="e" s="T671">Meː </ts>
               <ts e="T673" id="Seg_4190" n="e" s="T672">aj </ts>
               <ts e="T674" id="Seg_4192" n="e" s="T673">üttɨ </ts>
               <ts e="T675" id="Seg_4194" n="e" s="T674">pattɨrtɛntɨmɨt </ts>
               <ts e="T676" id="Seg_4196" n="e" s="T675">tɔːqɨtqo. </ts>
               <ts e="T677" id="Seg_4198" n="e" s="T676">İčʼakäčʼika </ts>
               <ts e="T678" id="Seg_4200" n="e" s="T677">kätɨsɨtɨ: </ts>
               <ts e="T679" id="Seg_4202" n="e" s="T678">Sütqonɨŋɨlʼɨt </ts>
               <ts e="T680" id="Seg_4204" n="e" s="T679">(sütqolʼnɔːtɨt, </ts>
               <ts e="T681" id="Seg_4206" n="e" s="T680">sütqɨlʼnɔːlʼit) </ts>
               <ts e="T682" id="Seg_4208" n="e" s="T681">sɨːrɨt </ts>
               <ts e="T683" id="Seg_4210" n="e" s="T682">qopɨ </ts>
               <ts e="T684" id="Seg_4212" n="e" s="T683">nɔːnɨ </ts>
               <ts e="T685" id="Seg_4214" n="e" s="T684">kotatɨp </ts>
               <ts e="T686" id="Seg_4216" n="e" s="T685">(sünnelʼɨt </ts>
               <ts e="T687" id="Seg_4218" n="e" s="T686">kotatɨp </ts>
               <ts e="T688" id="Seg_4220" n="e" s="T687">sɨːrɨt </ts>
               <ts e="T689" id="Seg_4222" n="e" s="T688">qopɨlʼ), </ts>
               <ts e="T690" id="Seg_4224" n="e" s="T689">ɔːlʼčʼiŋɨlʼɨt </ts>
               <ts e="T691" id="Seg_4226" n="e" s="T690">morʼat </ts>
               <ts e="T692" id="Seg_4228" n="e" s="T691">qanɨqtɨ, </ts>
               <ts e="T693" id="Seg_4230" n="e" s="T692">mat </ts>
               <ts e="T694" id="Seg_4232" n="e" s="T693">tɛːsintɨ </ts>
               <ts e="T695" id="Seg_4234" n="e" s="T694">süttap </ts>
               <ts e="T696" id="Seg_4236" n="e" s="T695">kotantɨ, </ts>
               <ts e="T697" id="Seg_4238" n="e" s="T696">käntap </ts>
               <ts e="T698" id="Seg_4240" n="e" s="T697">üttɨ </ts>
               <ts e="T699" id="Seg_4242" n="e" s="T698">püsa, </ts>
               <ts e="T700" id="Seg_4244" n="e" s="T699">marqɨ </ts>
               <ts e="T701" id="Seg_4246" n="e" s="T700">püsa. </ts>
               <ts e="T702" id="Seg_4248" n="e" s="T701">Tɛː </ts>
               <ts e="T703" id="Seg_4250" n="e" s="T702">qontelʼit </ts>
               <ts e="T704" id="Seg_4252" n="e" s="T703">muntɨk </ts>
               <ts e="T705" id="Seg_4254" n="e" s="T704">tɔːq”. </ts>
               <ts e="T706" id="Seg_4256" n="e" s="T705">Ukoːt </ts>
               <ts e="T707" id="Seg_4258" n="e" s="T706">Qolʼsak </ts>
               <ts e="T708" id="Seg_4260" n="e" s="T707">ira </ts>
               <ts e="T709" id="Seg_4262" n="e" s="T708">(načʼalʼnik):“ </ts>
               <ts e="T710" id="Seg_4264" n="e" s="T709">Kutə </ts>
               <ts e="T711" id="Seg_4266" n="e" s="T710">kɨka </ts>
               <ts e="T712" id="Seg_4268" n="e" s="T711">qättoːqonɨ?” </ts>
               <ts e="T713" id="Seg_4270" n="e" s="T712">Täp </ts>
               <ts e="T714" id="Seg_4272" n="e" s="T713">sütkolʼsitɨ </ts>
               <ts e="T715" id="Seg_4274" n="e" s="T714">ukoːt </ts>
               <ts e="T716" id="Seg_4276" n="e" s="T715">Qolʼsak </ts>
               <ts e="T717" id="Seg_4278" n="e" s="T716">irap, </ts>
               <ts e="T718" id="Seg_4280" n="e" s="T717">nɨnɨ </ts>
               <ts e="T719" id="Seg_4282" n="e" s="T718">mänɨlʼ </ts>
               <ts e="T720" id="Seg_4284" n="e" s="T719">qumiːmtɨ </ts>
               <ts e="T721" id="Seg_4286" n="e" s="T720">(kasatɨp). </ts>
               <ts e="T722" id="Seg_4288" n="e" s="T721">Nɨnɨ </ts>
               <ts e="T723" id="Seg_4290" n="e" s="T722">İčʼakäčʼika </ts>
               <ts e="T724" id="Seg_4292" n="e" s="T723">ontɨ </ts>
               <ts e="T725" id="Seg_4294" n="e" s="T724">käqolapsɨtɨ </ts>
               <ts e="T726" id="Seg_4296" n="e" s="T725">sütɨpɨlʼ </ts>
               <ts e="T727" id="Seg_4298" n="e" s="T726">kotatɨp </ts>
               <ts e="T728" id="Seg_4300" n="e" s="T727">qumɨtsä </ts>
               <ts e="T729" id="Seg_4302" n="e" s="T728">üttɨ. </ts>
               <ts e="T730" id="Seg_4304" n="e" s="T729">Kätɨsɨtɨ </ts>
               <ts e="T731" id="Seg_4306" n="e" s="T730">Qolʼsak </ts>
               <ts e="T732" id="Seg_4308" n="e" s="T731">iranɨk:“ </ts>
               <ts e="T733" id="Seg_4310" n="e" s="T732">Peːtɨ </ts>
               <ts e="T734" id="Seg_4312" n="e" s="T733">tɔːq </ts>
               <ts e="T735" id="Seg_4314" n="e" s="T734">morʼan </ts>
               <ts e="T736" id="Seg_4316" n="e" s="T735">ılqɨt. </ts>
               <ts e="T737" id="Seg_4318" n="e" s="T736">Nʼi </ts>
               <ts e="T738" id="Seg_4320" n="e" s="T737">kun </ts>
               <ts e="T739" id="Seg_4322" n="e" s="T738">asa </ts>
               <ts e="T740" id="Seg_4324" n="e" s="T739">qontal </ts>
               <ts e="T741" id="Seg_4326" n="e" s="T740">tɔːqɨtɨ </ts>
               <ts e="T742" id="Seg_4328" n="e" s="T741">i </ts>
               <ts e="T743" id="Seg_4330" n="e" s="T742">tovarɨt. </ts>
               <ts e="T744" id="Seg_4332" n="e" s="T743">Meːltɨ </ts>
               <ts e="T745" id="Seg_4334" n="e" s="T744">qunnantɨ. </ts>
               <ts e="T746" id="Seg_4336" n="e" s="T745">Tɛː </ts>
               <ts e="T747" id="Seg_4338" n="e" s="T746">na </ts>
               <ts e="T748" id="Seg_4340" n="e" s="T747">masɨp </ts>
               <ts e="T749" id="Seg_4342" n="e" s="T748">käkkɨsɨlɨt, </ts>
               <ts e="T750" id="Seg_4344" n="e" s="T749">üttɨ </ts>
               <ts e="T751" id="Seg_4346" n="e" s="T750">sɨp </ts>
               <ts e="T752" id="Seg_4348" n="e" s="T751">käːsɨlɨt. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2" id="Seg_4349" s="T0">KPM_1977_IchakechikaAndQolsaqo_flk.001 (001.001)</ta>
            <ta e="T5" id="Seg_4350" s="T2">KPM_1977_IchakechikaAndQolsaqo_flk.002 (001.002)</ta>
            <ta e="T11" id="Seg_4351" s="T5">KPM_1977_IchakechikaAndQolsaqo_flk.003 (001.003)</ta>
            <ta e="T20" id="Seg_4352" s="T11">KPM_1977_IchakechikaAndQolsaqo_flk.004 (001.004)</ta>
            <ta e="T25" id="Seg_4353" s="T20">KPM_1977_IchakechikaAndQolsaqo_flk.005 (001.005)</ta>
            <ta e="T33" id="Seg_4354" s="T25">KPM_1977_IchakechikaAndQolsaqo_flk.006 (001.006)</ta>
            <ta e="T42" id="Seg_4355" s="T33">KPM_1977_IchakechikaAndQolsaqo_flk.007 (001.007)</ta>
            <ta e="T51" id="Seg_4356" s="T42">KPM_1977_IchakechikaAndQolsaqo_flk.008 (001.008)</ta>
            <ta e="T56" id="Seg_4357" s="T51">KPM_1977_IchakechikaAndQolsaqo_flk.009 (001.009)</ta>
            <ta e="T61" id="Seg_4358" s="T56">KPM_1977_IchakechikaAndQolsaqo_flk.010 (001.010)</ta>
            <ta e="T64" id="Seg_4359" s="T61">KPM_1977_IchakechikaAndQolsaqo_flk.011 (001.011)</ta>
            <ta e="T68" id="Seg_4360" s="T64">KPM_1977_IchakechikaAndQolsaqo_flk.012 (001.012)</ta>
            <ta e="T72" id="Seg_4361" s="T68">KPM_1977_IchakechikaAndQolsaqo_flk.013 (001.013)</ta>
            <ta e="T76" id="Seg_4362" s="T72">KPM_1977_IchakechikaAndQolsaqo_flk.014 (001.014)</ta>
            <ta e="T80" id="Seg_4363" s="T76">KPM_1977_IchakechikaAndQolsaqo_flk.015 (001.015)</ta>
            <ta e="T84" id="Seg_4364" s="T80">KPM_1977_IchakechikaAndQolsaqo_flk.016 (001.016)</ta>
            <ta e="T87" id="Seg_4365" s="T84">KPM_1977_IchakechikaAndQolsaqo_flk.017 (001.017)</ta>
            <ta e="T90" id="Seg_4366" s="T87">KPM_1977_IchakechikaAndQolsaqo_flk.018 (001.018)</ta>
            <ta e="T95" id="Seg_4367" s="T90">KPM_1977_IchakechikaAndQolsaqo_flk.019 (001.019)</ta>
            <ta e="T99" id="Seg_4368" s="T95">KPM_1977_IchakechikaAndQolsaqo_flk.020 (001.020)</ta>
            <ta e="T107" id="Seg_4369" s="T99">KPM_1977_IchakechikaAndQolsaqo_flk.021 (001.021)</ta>
            <ta e="T116" id="Seg_4370" s="T107">KPM_1977_IchakechikaAndQolsaqo_flk.022 (001.022)</ta>
            <ta e="T124" id="Seg_4371" s="T116">KPM_1977_IchakechikaAndQolsaqo_flk.023 (001.023)</ta>
            <ta e="T127" id="Seg_4372" s="T124">KPM_1977_IchakechikaAndQolsaqo_flk.024 (001.024)</ta>
            <ta e="T132" id="Seg_4373" s="T127">KPM_1977_IchakechikaAndQolsaqo_flk.025 (001.025)</ta>
            <ta e="T139" id="Seg_4374" s="T132">KPM_1977_IchakechikaAndQolsaqo_flk.026 (001.026)</ta>
            <ta e="T144" id="Seg_4375" s="T139">KPM_1977_IchakechikaAndQolsaqo_flk.027 (001.027)</ta>
            <ta e="T149" id="Seg_4376" s="T144">KPM_1977_IchakechikaAndQolsaqo_flk.028 (001.028)</ta>
            <ta e="T151" id="Seg_4377" s="T149">KPM_1977_IchakechikaAndQolsaqo_flk.029 (001.029)</ta>
            <ta e="T158" id="Seg_4378" s="T151">KPM_1977_IchakechikaAndQolsaqo_flk.030 (001.030)</ta>
            <ta e="T163" id="Seg_4379" s="T158">KPM_1977_IchakechikaAndQolsaqo_flk.031 (001.031)</ta>
            <ta e="T172" id="Seg_4380" s="T163">KPM_1977_IchakechikaAndQolsaqo_flk.032 (001.032)</ta>
            <ta e="T174" id="Seg_4381" s="T172">KPM_1977_IchakechikaAndQolsaqo_flk.033 (001.033)</ta>
            <ta e="T178" id="Seg_4382" s="T174">KPM_1977_IchakechikaAndQolsaqo_flk.034 (001.034)</ta>
            <ta e="T181" id="Seg_4383" s="T178">KPM_1977_IchakechikaAndQolsaqo_flk.035 (001.035)</ta>
            <ta e="T189" id="Seg_4384" s="T181">KPM_1977_IchakechikaAndQolsaqo_flk.036 (001.036)</ta>
            <ta e="T194" id="Seg_4385" s="T189">KPM_1977_IchakechikaAndQolsaqo_flk.037 (001.037)</ta>
            <ta e="T198" id="Seg_4386" s="T194">KPM_1977_IchakechikaAndQolsaqo_flk.038 (001.038)</ta>
            <ta e="T204" id="Seg_4387" s="T198">KPM_1977_IchakechikaAndQolsaqo_flk.039 (001.039)</ta>
            <ta e="T207" id="Seg_4388" s="T204">KPM_1977_IchakechikaAndQolsaqo_flk.040 (001.040)</ta>
            <ta e="T214" id="Seg_4389" s="T207">KPM_1977_IchakechikaAndQolsaqo_flk.041 (001.041)</ta>
            <ta e="T219" id="Seg_4390" s="T214">KPM_1977_IchakechikaAndQolsaqo_flk.042 (001.042)</ta>
            <ta e="T223" id="Seg_4391" s="T219">KPM_1977_IchakechikaAndQolsaqo_flk.043 (001.043)</ta>
            <ta e="T224" id="Seg_4392" s="T223">KPM_1977_IchakechikaAndQolsaqo_flk.044 (001.044)</ta>
            <ta e="T229" id="Seg_4393" s="T224">KPM_1977_IchakechikaAndQolsaqo_flk.045 (001.045)</ta>
            <ta e="T235" id="Seg_4394" s="T229">KPM_1977_IchakechikaAndQolsaqo_flk.046 (001.046)</ta>
            <ta e="T237" id="Seg_4395" s="T235">KPM_1977_IchakechikaAndQolsaqo_flk.047 (001.047)</ta>
            <ta e="T239" id="Seg_4396" s="T237">KPM_1977_IchakechikaAndQolsaqo_flk.048 (001.048)</ta>
            <ta e="T243" id="Seg_4397" s="T239">KPM_1977_IchakechikaAndQolsaqo_flk.049 (001.049)</ta>
            <ta e="T246" id="Seg_4398" s="T243">KPM_1977_IchakechikaAndQolsaqo_flk.050 (001.050)</ta>
            <ta e="T251" id="Seg_4399" s="T246">KPM_1977_IchakechikaAndQolsaqo_flk.051 (001.051)</ta>
            <ta e="T254" id="Seg_4400" s="T251">KPM_1977_IchakechikaAndQolsaqo_flk.052 (001.052)</ta>
            <ta e="T257" id="Seg_4401" s="T254">KPM_1977_IchakechikaAndQolsaqo_flk.053 (001.053)</ta>
            <ta e="T264" id="Seg_4402" s="T257">KPM_1977_IchakechikaAndQolsaqo_flk.054 (001.054)</ta>
            <ta e="T269" id="Seg_4403" s="T264">KPM_1977_IchakechikaAndQolsaqo_flk.055 (001.055)</ta>
            <ta e="T276" id="Seg_4404" s="T269">KPM_1977_IchakechikaAndQolsaqo_flk.056 (001.056)</ta>
            <ta e="T280" id="Seg_4405" s="T276">KPM_1977_IchakechikaAndQolsaqo_flk.057 (001.057)</ta>
            <ta e="T287" id="Seg_4406" s="T280">KPM_1977_IchakechikaAndQolsaqo_flk.058 (001.058)</ta>
            <ta e="T293" id="Seg_4407" s="T287">KPM_1977_IchakechikaAndQolsaqo_flk.059 (001.059)</ta>
            <ta e="T297" id="Seg_4408" s="T293">KPM_1977_IchakechikaAndQolsaqo_flk.060 (001.060)</ta>
            <ta e="T305" id="Seg_4409" s="T297">KPM_1977_IchakechikaAndQolsaqo_flk.061 (001.061)</ta>
            <ta e="T307" id="Seg_4410" s="T305">KPM_1977_IchakechikaAndQolsaqo_flk.062 (001.062)</ta>
            <ta e="T310" id="Seg_4411" s="T307">KPM_1977_IchakechikaAndQolsaqo_flk.063 (001.063)</ta>
            <ta e="T312" id="Seg_4412" s="T310">KPM_1977_IchakechikaAndQolsaqo_flk.064 (001.064)</ta>
            <ta e="T316" id="Seg_4413" s="T312">KPM_1977_IchakechikaAndQolsaqo_flk.065 (001.065)</ta>
            <ta e="T318" id="Seg_4414" s="T316">KPM_1977_IchakechikaAndQolsaqo_flk.066 (001.066)</ta>
            <ta e="T320" id="Seg_4415" s="T318">KPM_1977_IchakechikaAndQolsaqo_flk.067 (001.067)</ta>
            <ta e="T330" id="Seg_4416" s="T320">KPM_1977_IchakechikaAndQolsaqo_flk.068 (001.068)</ta>
            <ta e="T333" id="Seg_4417" s="T330">KPM_1977_IchakechikaAndQolsaqo_flk.069 (001.069)</ta>
            <ta e="T339" id="Seg_4418" s="T333">KPM_1977_IchakechikaAndQolsaqo_flk.070 (001.070)</ta>
            <ta e="T345" id="Seg_4419" s="T339">KPM_1977_IchakechikaAndQolsaqo_flk.071 (001.071)</ta>
            <ta e="T354" id="Seg_4420" s="T345">KPM_1977_IchakechikaAndQolsaqo_flk.072 (001.072)</ta>
            <ta e="T359" id="Seg_4421" s="T354">KPM_1977_IchakechikaAndQolsaqo_flk.073 (001.073)</ta>
            <ta e="T367" id="Seg_4422" s="T359">KPM_1977_IchakechikaAndQolsaqo_flk.074 (001.074)</ta>
            <ta e="T372" id="Seg_4423" s="T367">KPM_1977_IchakechikaAndQolsaqo_flk.075 (001.075)</ta>
            <ta e="T380" id="Seg_4424" s="T372">KPM_1977_IchakechikaAndQolsaqo_flk.076 (001.076)</ta>
            <ta e="T385" id="Seg_4425" s="T380">KPM_1977_IchakechikaAndQolsaqo_flk.077 (001.077)</ta>
            <ta e="T386" id="Seg_4426" s="T385">KPM_1977_IchakechikaAndQolsaqo_flk.078 (001.078)</ta>
            <ta e="T392" id="Seg_4427" s="T386">KPM_1977_IchakechikaAndQolsaqo_flk.079 (001.079)</ta>
            <ta e="T397" id="Seg_4428" s="T392">KPM_1977_IchakechikaAndQolsaqo_flk.080 (001.080)</ta>
            <ta e="T400" id="Seg_4429" s="T397">KPM_1977_IchakechikaAndQolsaqo_flk.081 (001.081)</ta>
            <ta e="T403" id="Seg_4430" s="T400">KPM_1977_IchakechikaAndQolsaqo_flk.082 (001.082)</ta>
            <ta e="T406" id="Seg_4431" s="T403">KPM_1977_IchakechikaAndQolsaqo_flk.083 (001.083)</ta>
            <ta e="T413" id="Seg_4432" s="T406">KPM_1977_IchakechikaAndQolsaqo_flk.084 (001.084)</ta>
            <ta e="T416" id="Seg_4433" s="T413">KPM_1977_IchakechikaAndQolsaqo_flk.085 (001.085)</ta>
            <ta e="T420" id="Seg_4434" s="T416">KPM_1977_IchakechikaAndQolsaqo_flk.086 (001.086)</ta>
            <ta e="T427" id="Seg_4435" s="T420">KPM_1977_IchakechikaAndQolsaqo_flk.087 (001.087)</ta>
            <ta e="T431" id="Seg_4436" s="T427">KPM_1977_IchakechikaAndQolsaqo_flk.088 (001.088)</ta>
            <ta e="T438" id="Seg_4437" s="T431">KPM_1977_IchakechikaAndQolsaqo_flk.089 (001.089)</ta>
            <ta e="T445" id="Seg_4438" s="T438">KPM_1977_IchakechikaAndQolsaqo_flk.090 (001.090)</ta>
            <ta e="T449" id="Seg_4439" s="T445">KPM_1977_IchakechikaAndQolsaqo_flk.091 (001.091)</ta>
            <ta e="T455" id="Seg_4440" s="T449">KPM_1977_IchakechikaAndQolsaqo_flk.092 (001.092)</ta>
            <ta e="T459" id="Seg_4441" s="T455">KPM_1977_IchakechikaAndQolsaqo_flk.093 (001.093)</ta>
            <ta e="T463" id="Seg_4442" s="T459">KPM_1977_IchakechikaAndQolsaqo_flk.094 (001.094)</ta>
            <ta e="T469" id="Seg_4443" s="T463">KPM_1977_IchakechikaAndQolsaqo_flk.095 (001.095)</ta>
            <ta e="T474" id="Seg_4444" s="T469">KPM_1977_IchakechikaAndQolsaqo_flk.096 (001.096)</ta>
            <ta e="T480" id="Seg_4445" s="T474">KPM_1977_IchakechikaAndQolsaqo_flk.097 (001.097)</ta>
            <ta e="T482" id="Seg_4446" s="T480">KPM_1977_IchakechikaAndQolsaqo_flk.098 (001.098)</ta>
            <ta e="T488" id="Seg_4447" s="T482">KPM_1977_IchakechikaAndQolsaqo_flk.099 (001.099)</ta>
            <ta e="T491" id="Seg_4448" s="T488">KPM_1977_IchakechikaAndQolsaqo_flk.100 (001.100)</ta>
            <ta e="T493" id="Seg_4449" s="T491">KPM_1977_IchakechikaAndQolsaqo_flk.101 (001.101)</ta>
            <ta e="T496" id="Seg_4450" s="T493">KPM_1977_IchakechikaAndQolsaqo_flk.102 (001.102)</ta>
            <ta e="T498" id="Seg_4451" s="T496">KPM_1977_IchakechikaAndQolsaqo_flk.103 (001.103)</ta>
            <ta e="T503" id="Seg_4452" s="T498">KPM_1977_IchakechikaAndQolsaqo_flk.104 (001.104)</ta>
            <ta e="T509" id="Seg_4453" s="T503">KPM_1977_IchakechikaAndQolsaqo_flk.105 (001.105)</ta>
            <ta e="T515" id="Seg_4454" s="T509">KPM_1977_IchakechikaAndQolsaqo_flk.106 (001.106)</ta>
            <ta e="T518" id="Seg_4455" s="T515">KPM_1977_IchakechikaAndQolsaqo_flk.107 (001.107)</ta>
            <ta e="T523" id="Seg_4456" s="T518">KPM_1977_IchakechikaAndQolsaqo_flk.108 (001.108)</ta>
            <ta e="T525" id="Seg_4457" s="T523">KPM_1977_IchakechikaAndQolsaqo_flk.109 (001.109)</ta>
            <ta e="T528" id="Seg_4458" s="T525">KPM_1977_IchakechikaAndQolsaqo_flk.110 (001.110)</ta>
            <ta e="T534" id="Seg_4459" s="T528">KPM_1977_IchakechikaAndQolsaqo_flk.111 (001.111)</ta>
            <ta e="T541" id="Seg_4460" s="T534">KPM_1977_IchakechikaAndQolsaqo_flk.112 (001.112)</ta>
            <ta e="T556" id="Seg_4461" s="T541">KPM_1977_IchakechikaAndQolsaqo_flk.113 (001.113)</ta>
            <ta e="T563" id="Seg_4462" s="T556">KPM_1977_IchakechikaAndQolsaqo_flk.114 (001.114)</ta>
            <ta e="T565" id="Seg_4463" s="T563">KPM_1977_IchakechikaAndQolsaqo_flk.115 (001.115)</ta>
            <ta e="T575" id="Seg_4464" s="T565">KPM_1977_IchakechikaAndQolsaqo_flk.116 (001.116)</ta>
            <ta e="T581" id="Seg_4465" s="T575">KPM_1977_IchakechikaAndQolsaqo_flk.117 (001.117)</ta>
            <ta e="T586" id="Seg_4466" s="T581">KPM_1977_IchakechikaAndQolsaqo_flk.118 (001.118)</ta>
            <ta e="T601" id="Seg_4467" s="T586">KPM_1977_IchakechikaAndQolsaqo_flk.119 (001.119)</ta>
            <ta e="T611" id="Seg_4468" s="T601">KPM_1977_IchakechikaAndQolsaqo_flk.120 (001.120)</ta>
            <ta e="T615" id="Seg_4469" s="T611">KPM_1977_IchakechikaAndQolsaqo_flk.121 (001.121)</ta>
            <ta e="T621" id="Seg_4470" s="T615">KPM_1977_IchakechikaAndQolsaqo_flk.122 (001.122)</ta>
            <ta e="T643" id="Seg_4471" s="T621">KPM_1977_IchakechikaAndQolsaqo_flk.123 (001.123)</ta>
            <ta e="T647" id="Seg_4472" s="T643">KPM_1977_IchakechikaAndQolsaqo_flk.124 (001.124)</ta>
            <ta e="T650" id="Seg_4473" s="T647">KPM_1977_IchakechikaAndQolsaqo_flk.125 (001.125)</ta>
            <ta e="T654" id="Seg_4474" s="T650">KPM_1977_IchakechikaAndQolsaqo_flk.126 (001.126)</ta>
            <ta e="T671" id="Seg_4475" s="T654">KPM_1977_IchakechikaAndQolsaqo_flk.127 (001.127)</ta>
            <ta e="T676" id="Seg_4476" s="T671">KPM_1977_IchakechikaAndQolsaqo_flk.128 (001.128)</ta>
            <ta e="T701" id="Seg_4477" s="T676">KPM_1977_IchakechikaAndQolsaqo_flk.129 (001.129)</ta>
            <ta e="T705" id="Seg_4478" s="T701">KPM_1977_IchakechikaAndQolsaqo_flk.130 (001.130)</ta>
            <ta e="T712" id="Seg_4479" s="T705">KPM_1977_IchakechikaAndQolsaqo_flk.131 (001.131)</ta>
            <ta e="T721" id="Seg_4480" s="T712">KPM_1977_IchakechikaAndQolsaqo_flk.132 (001.132)</ta>
            <ta e="T729" id="Seg_4481" s="T721">KPM_1977_IchakechikaAndQolsaqo_flk.133 (001.133)</ta>
            <ta e="T736" id="Seg_4482" s="T729">KPM_1977_IchakechikaAndQolsaqo_flk.134 (001.134)</ta>
            <ta e="T743" id="Seg_4483" s="T736">KPM_1977_IchakechikaAndQolsaqo_flk.135 (001.135)</ta>
            <ta e="T745" id="Seg_4484" s="T743">KPM_1977_IchakechikaAndQolsaqo_flk.136 (001.136)</ta>
            <ta e="T752" id="Seg_4485" s="T745">KPM_1977_IchakechikaAndQolsaqo_flk.137 (001.137)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T2" id="Seg_4486" s="T0">′kолʼсаkо и′ра.</ta>
            <ta e="T5" id="Seg_4487" s="T2">′илʼимпа ′ичак ӓчика им′лʼантысӓ.</ta>
            <ta e="T11" id="Seg_4488" s="T5">′колʼ′саk и′раl kъ̊тты – аса кунда′kын е̄са.</ta>
            <ta e="T20" id="Seg_4489" s="T11">оккыр чондоkыт kолʼсаkо ираl сы′рыты ′ӱ̄ра (ӱ̄рыса) теlде ӱ̄ры[у]kа.</ta>
            <ta e="T25" id="Seg_4490" s="T20">тӓп ′тӓнымыты што ′ичак ′ӓчикат тӓlысыт.</ta>
            <ta e="T33" id="Seg_4491" s="T25">′ӱ̄тоты ′тӓбынык ка′затып си′татып мӓннымб̂ыkо kоип ′метыт ′ӣчак ′ӓчика.</ta>
            <ta e="T42" id="Seg_4492" s="T33">тӓп ′тӓнымыты ′тӱнто̄тыт ру′сʼаксӓ (пуш′катсӓ) тӓбып орkълʼтӓнтотыт [орkыlkо] ′омдылʼдентотыт тʼур′манты.</ta>
            <ta e="T51" id="Seg_4493" s="T42">на времʼаɣыт (на чʼелʼе) ′сырып ′kъ̊[ə]ссыты, кʼӓтымты ′кымса ′kамдысыты.</ta>
            <ta e="T56" id="Seg_4494" s="T51">′мерысыты марkъ паңып ′мо̄та (по̄рɣ[k]ынт) [по̄роɣот].</ta>
            <ta e="T61" id="Seg_4495" s="T56">им′лʼамты ′тамдыlсыты кӓмылʼ ′сырыт кʼетыса.</ta>
            <ta e="T64" id="Seg_4496" s="T61">имлʼат то′kалʼдисты ′порkат.</ta>
            <ta e="T68" id="Seg_4497" s="T64">ка′зат (ка′заkыт) тӱ̄′со̄тыт ′ича′кӓчиканык.</ta>
            <ta e="T72" id="Seg_4498" s="T68">′марkа kуп ′kӓтсыты ′ичакӓчиканы.</ta>
            <ta e="T76" id="Seg_4499" s="T72">′тасынты ′омдыlʼтыkо нотна тӱрʼ′манты.</ta>
            <ta e="T80" id="Seg_4500" s="T76">тат сырытып ′kӓттаl ′амнал.</ta>
            <ta e="T84" id="Seg_4501" s="T80">тӓп ′нʼенʼнʼа мо̄сса им′лʼандыкини kӓтсыты.</ta>
            <ta e="T87" id="Seg_4502" s="T84">мас′сып ′омдалʼтынто̄тыт тʼӱрманты.</ta>
            <ta e="T90" id="Seg_4503" s="T87">ча̊йник ′мужерет кӓнбылʼа.</ta>
            <ta e="T95" id="Seg_4504" s="T90">′имлʼаты то̄тӓлʼна то′боп у′топ ′тʼӱсса.</ta>
            <ta e="T99" id="Seg_4505" s="T95">ка′зат нын′kотыт ′мотан о̄kыт.</ta>
            <ta e="T107" id="Seg_4506" s="T99">′ичак ӓчика имлʼандыкини - та лʼентʼаймонт ча̊йник а′са кы′канты ′мусырыkо.</ta>
            <ta e="T116" id="Seg_4507" s="T107">нʼенʼнʼимосса ′ичʼӓк ӓчика паңымты ′илʼлʼа ′ӣсыты имʼлʼамды мат′тӓрсыты [′маттырсыты] имʼлʼаты kуlч[тʼ]иса.</ta>
            <ta e="T124" id="Seg_4508" s="T116">ныны и. kӓтсыты ка′заткини - имʼлʼам меlде ′ниlчик ′орынʼнʼа.</ta>
            <ta e="T127" id="Seg_4509" s="T124">′паңыты то′б̂оkынты ′логыр[ы]сыты.</ta>
            <ta e="T132" id="Seg_4510" s="T127">имʼлʼа и′ннӓ пактӓш, паңып илʼлʼаптента.</ta>
            <ta e="T139" id="Seg_4511" s="T132">ныны имʼ′лʼаты нылʼлʼеиса kӓнбылʼлʼе ′орkылсыты ′чайникты ′мусырыkо.</ta>
            <ta e="T144" id="Seg_4512" s="T139">′канзап ′наkkылʼдʼат кунты чайник му′сейса.</ta>
            <ta e="T149" id="Seg_4513" s="T144">нʼанʼ м ′апсып ′тоттысыты лʼемты kӓнпылʼлʼӓ.</ta>
            <ta e="T151" id="Seg_4514" s="T149">ка′зат ′амырkолапсотыт.</ta>
            <ta e="T158" id="Seg_4515" s="T151">тӓбыт нилʼдʼик ′kӓтысотыт ме тат ′паңып тӓ̄′ментомыт.</ta>
            <ta e="T163" id="Seg_4516" s="T158">ме ′ӣма′ӣмат лʼентʼайто̄тыт и ′kунто̄тыт.</ta>
            <ta e="T172" id="Seg_4517" s="T163">kа′зат а′зӓ лагы[р]kолапсотыт ′ичак ′ӓчикап, ′kӓтӓнто̄тыт [′kӓтысотыт] kолʼ′сак ′ӣра′нык kы′со̄тыт.</ta>
            <ta e="T174" id="Seg_4518" s="T172">но ′kӓтым′б̂о̄тыт.</ta>
            <ta e="T178" id="Seg_4519" s="T174">на′чалʼник ку′ралʼдысты ′тӓмыkо ′паңып.</ta>
            <ta e="T181" id="Seg_4520" s="T178">син′делыл че̄лʼ тӓ̄′мотыт.</ta>
            <ta e="T189" id="Seg_4521" s="T181">kолʼсаk и′ра kӓттысыты паңып ма′кке ′миңел[l]ыт, мат сʼаkылʼтентап.</ta>
            <ta e="T194" id="Seg_4522" s="T189">ма и′мам и ра′ботникит лʼентʼайто̄тыт.</ta>
            <ta e="T198" id="Seg_4523" s="T194">′че̄лʼит и пӣт kонтотыт.</ta>
            <ta e="T204" id="Seg_4524" s="T198">′ичак ӓчика kӓтымпаты [kатысыт]: паңып ′чистаң[к] ′мӓрыңыlыт.</ta>
            <ta e="T207" id="Seg_4525" s="T204">′сома kумып ′матырkо.</ta>
            <ta e="T214" id="Seg_4526" s="T207">kолʼсаk ′ира ′kондыса kарыт чең[к] ′омд̂ыса [мӓсыса].</ta>
            <ta e="T219" id="Seg_4527" s="T214">и′ма ом′д̂ӓшик, и ра′ботникит ом′д̂ӓңыlыт!</ta>
            <ta e="T223" id="Seg_4528" s="T219">′тӓбыт чек ′асса ‵омнӓнтотыт.</ta>
            <ta e="T224" id="Seg_4529" s="T223">′нʼенʼнʼимос[с]а.</ta>
            <ta e="T229" id="Seg_4530" s="T224">′илʼлʼа ′ӣсыты паңамт мо̄′тӓр[лʼ]сыты мунтык.</ta>
            <ta e="T235" id="Seg_4531" s="T229">паңып лоГырсыты то′боɣынты мунтык маттырпыl kумиɣанты.</ta>
            <ta e="T237" id="Seg_4532" s="T235">чап kӓттыkыты.</ta>
            <ta e="T239" id="Seg_4533" s="T237">меlде ′kулʼчо̄тыт.</ta>
            <ta e="T243" id="Seg_4534" s="T239">′ира нʼенʼнʼмосса ка′затып ′kӓрысыт.</ta>
            <ta e="T246" id="Seg_4535" s="T243">кун ӣ′заkыт ′паңып. </ta>
            <ta e="T251" id="Seg_4536" s="T246">тӓп (ичӓк) меkыни паңып мисыты.</ta>
            <ta e="T254" id="Seg_4537" s="T251">тӓп ′ичӓк ′ӓ̄чика ′о̄lӓкса.</ta>
            <ta e="T257" id="Seg_4538" s="T254">kӓлʼлʼа ′орГыlkо су′дʼинkо.</ta>
            <ta e="T264" id="Seg_4539" s="T257">имаӣмыт [имам ′ӣмат]мундык kӯсо̄тыт паңынон.</ta>
            <ta e="T269" id="Seg_4540" s="T264">тат о′ннӓка а′за ′тотык ′маттырса.</ta>
            <ta e="T276" id="Seg_4541" s="T269">тӓбып kӓ̄чиkо оккур че̄лʼ ме ай саңартомыт.</ta>
            <ta e="T280" id="Seg_4542" s="T276">ме ′ӣма‵ӣмыт ай лʼентʼайтотыт.</ta>
            <ta e="T287" id="Seg_4543" s="T280">kолʼсаk и′ра ай kӓтысыты ′ланно са′ңарныlыт паңып!</ta>
            <ta e="T293" id="Seg_4544" s="T287">ка′зат ши′тӓт нилʼдʼик ма′тӓлʼсыты онди ′имаӣт.</ta>
            <ta e="T297" id="Seg_4545" s="T293">′ӣмаӣт kӯсо̄тыт ‵kӓмыт ′чо̄рык.</ta>
            <ta e="T305" id="Seg_4546" s="T297">kарыт kӓтысотыт kолʼсаk иранык ме ′има′имыт меlде kӯ′со̄тыт.</ta>
            <ta e="T307" id="Seg_4547" s="T305">и′ра нʼенʼнʼамосса.</ta>
            <ta e="T310" id="Seg_4548" s="T307">′ичак ӓчикап ′орkыlkо нотна.</ta>
            <ta e="T312" id="Seg_4549" s="T310">ка′зат kӓ̄′со̄тыт.</ta>
            <ta e="T316" id="Seg_4550" s="T312">′ичак ӓчика на времʼаɣыт ′мотkынты.</ta>
            <ta e="T318" id="Seg_4551" s="T316">ичак ӓчикап ′орkыlсотыт.</ta>
            <ta e="T320" id="Seg_4552" s="T318">′сеп ′моlмытысанты.</ta>
            <ta e="T330" id="Seg_4553" s="T320">ме ′мундык ӣмаимыт и ра′ботникит ма′тӓлʼсимыт, ′тӓбыт kӯ′со̄тыт ′kӓмытчорык.</ta>
            <ta e="T333" id="Seg_4554" s="T330">ичак ӓчикап ′kӓнтысо̄тыт тӱр′манты.</ta>
            <ta e="T339" id="Seg_4555" s="T333">колʼ′саk и′ра ′куралʼдисты ′илʼлʼе ′kоттырыkо ′ӱттъ.</ta>
            <ta e="T345" id="Seg_4556" s="T339">kӓтысыты ′сырыт kобонты шӱ′нʼнʼонты шʼӱтkо ′илʼелʼӓ.</ta>
            <ta e="T354" id="Seg_4557" s="T345">ка′зат ′тӓбып kӓндысотыт ′чӱндыl kаɣlысӓ ӱт топты морет kаныкты.</ta>
            <ta e="T359" id="Seg_4558" s="T354">тӓп kо′тачӱ′ндӓ(о?)ɣыт ′иlыlа ′иппа.</ta>
            <ta e="T367" id="Seg_4559" s="T359">kӓтысыты масып kоптырʼантыlыт марГъ пӱса марГъ пӱ ′пе̄ɣыlныкыт.</ta>
            <ta e="T372" id="Seg_4560" s="T367">тӓбыт ′kӓ̄со̄тыт ′пе̄kо марГъ пӱп.</ta>
            <ta e="T380" id="Seg_4561" s="T372">кунды лʼи kомыча ′ӣпымпа ӱңеlдымбат чӱндыl kаɣlы тӱнта.</ta>
            <ta e="T385" id="Seg_4562" s="T380">тӓтты лʼи ′сомбыlа kаɣlыт ′тӱ̄нта.</ta>
            <ta e="T386" id="Seg_4563" s="T385">′утыр[ы]со̄тыт.</ta>
            <ta e="T392" id="Seg_4564" s="T386">тӓмkуп тоkсӓ [та′wарсӓ, ′лыптыксӓ] kӓнта моkынӓ.</ta>
            <ta e="T397" id="Seg_4565" s="T392">тӓмkуп тополʼсыты ′сырыт кобылʼ ′kотап.</ta>
            <ta e="T400" id="Seg_4566" s="T397">kои тымды иппа?</ta>
            <ta e="T403" id="Seg_4567" s="T400">′ичик ӓчика ланкалʼса иjо̄!</ta>
            <ta e="T406" id="Seg_4568" s="T403">тӓмkуп – kоитkо ′ӣппант.</ta>
            <ta e="T413" id="Seg_4569" s="T406">И.-и. kӓтысыты, и′ннӓ сим ′ӱ̄тас! то̄ ма′талʼты. </ta>
            <ta e="T416" id="Seg_4570" s="T413">тӓп то̄ ма′талʼоlсыт.</ta>
            <ta e="T420" id="Seg_4571" s="T416">тӓп и′ннӓ ′путалʼмосса [тантыса].</ta>
            <ta e="T427" id="Seg_4572" s="T420">тӓбын мыɣыт сӣт kобылʼ мы ′еппа ′мунkынты.</ta>
            <ta e="T431" id="Seg_4573" s="T427">тӓмkуп соɣонʼет kойтkо ′ӣпанты.</ta>
            <ta e="T438" id="Seg_4574" s="T431">мат ӣппап, ′ӱтты ′патырсак сит kопып ӣсап.</ta>
            <ta e="T445" id="Seg_4575" s="T438">′ормы че̄ңkыса, тоk [лыптык] kоччи ′ӱтkът и′ннӓ.</ta>
            <ta e="T449" id="Seg_4576" s="T445">адылʼситы сит kопытып тӓмkум[ы]нык.</ta>
            <ta e="T455" id="Seg_4577" s="T449">тӓмkум ′о[а̊]ндаlса ма ай ′паттырлʼек ӱтт.</ta>
            <ta e="T459" id="Seg_4578" s="T455">тӓпып ′сӱтӓситы ′сырытkотанты.</ta>
            <ta e="T463" id="Seg_4579" s="T459">тӓмkуп ′иппа ӱттотkып kотаkыт.</ta>
            <ta e="T469" id="Seg_4580" s="T463">′ичак ӓчика ‵kъ̊н′найса, чӱндыl kаɣlынтыса и тоkса.</ta>
            <ta e="T474" id="Seg_4581" s="T469">ка′зат татысотыт марГъ пӱп ′сорыkолапсотыт.</ta>
            <ta e="T480" id="Seg_4582" s="T474">тӓнто̄тыт ондынʼанды сеп ичак ӓчика ′олыңырсал.</ta>
            <ta e="T482" id="Seg_4583" s="T480">′ӱтkыт ′kуннант.</ta>
            <ta e="T488" id="Seg_4584" s="T482">′тӓмkуп ӱңгелʼдымбат сорыkандалʼдимботыт пӱп, марГъ пӱп.</ta>
            <ta e="T491" id="Seg_4585" s="T488">а′са мат ′ичик ӓчика.</ta>
            <ta e="T493" id="Seg_4586" s="T491">ичӓк ӓчика ′kъ[ӓ]сса.</ta>
            <ta e="T496" id="Seg_4587" s="T493">тап масым ′о̄lыкса.</ta>
            <ta e="T498" id="Seg_4588" s="T496">мат тӓмkумоңок.</ta>
            <ta e="T503" id="Seg_4589" s="T498">оlыГlӓ масып сӱсса kобыl kо′танты.</ta>
            <ta e="T509" id="Seg_4590" s="T503">ка′сат ′томнат, тат ичӓк ӓчиканты сеп ′моlмытысанты.</ta>
            <ta e="T515" id="Seg_4591" s="T509">тона чурыlʼа лаң[к]ынʼа kо′таkыт мат тӓмkумоңок.</ta>
            <ta e="T518" id="Seg_4592" s="T515">тӱнтысак чӱнтыса, kаɣlыса.</ta>
            <ta e="T523" id="Seg_4593" s="T518">ӱттъ ′туlдыlа ′чаттысо̄тыт ′kоптыlсо̄тыт пӱ̄са.</ta>
            <ta e="T525" id="Seg_4594" s="T523">моkынӓ kӓссотыт.</ta>
            <ta e="T528" id="Seg_4595" s="T525">ме kоптырсымыт ′ӱттъ.</ta>
            <ta e="T534" id="Seg_4596" s="T528">ичик ӓчика тӱса ′моkына ′чӱндыса, чӱндыl kаɣlыса.</ta>
            <ta e="T541" id="Seg_4597" s="T534">тоkты ′ӱссе чон′даптысты тӓмнонты и′ннӓ ы′таlситы тӓkыkо.</ta>
            <ta e="T556" id="Seg_4598" s="T541">на времʼаɣыт началʼник kолʼсак и′ра ′меннымб̂а ичак ӓчикат ′мотылʼ пелʼакты шоɣыр ′чотымбыт, ′пурkыkатkа шоɣырноны.</ta>
            <ta e="T563" id="Seg_4599" s="T556">′тымдысӓ [нымдысӓ] ′kӓттысыты kа′з̂атыкиник kӓлʼлʼа ′меннымбыkо ′имаkотап.</ta>
            <ta e="T565" id="Seg_4600" s="T563">тӓбыт ′kӓ̄со̄тыт.</ta>
            <ta e="T575" id="Seg_4601" s="T565">мотты ′сӓрсотыт, имаkотат ′kоптычо̄т омта ′столыниɣыт ′ичӓк ′ӓчика нʼенʼнʼамотпа.</ta>
            <ta e="T581" id="Seg_4602" s="T575">мундык мӓндыты kа′толʼбат ′kӓмча, порГымты ′нытымпылʼа.</ta>
            <ta e="T586" id="Seg_4603" s="T581">тӓбыт ′kӓтысо̄т [серсотыт] – д̂о′рова, лʼака.</ta>
            <ta e="T601" id="Seg_4604" s="T586">а ичек ӓчика ненʼнʼимолʼа ′о̄мта, тӓ ′масып ӱтысып kоптырысылыт мат ′морʼа′нӣлɣыт котчик тоk [лыптыk] kосап.</ta>
            <ta e="T611" id="Seg_4605" s="T601">нӓ′саң еңа кусак ′орым ӓ̄′са ′ӣсап [ма орымы а′са ′ӣсыты].</ta>
            <ta e="T615" id="Seg_4606" s="T611">бытта ӱ̄деныl номГын е̄сак.</ta>
            <ta e="T621" id="Seg_4607" s="T615">тӓбыт kӓ̄′со̄тыт ныргымолʼлʼӓ kоилʼ ′jабол ӓса.</ta>
            <ta e="T643" id="Seg_4608" s="T621">kоl′сак и′ранык kӓтыkо и′чак ′ӓчикап ӱттъ kоптырсымыт и′колʼ? чʼе̄лʼе (о′кот) тӓп на′сса kомбаты тоk [′лыптык] ку′шаl ′тӓтаkой мундык kумпаты и сит kобыlмыт.</ta>
            <ta e="T647" id="Seg_4609" s="T643">kоl′сак и′ра ′тӓныты ′ӱ̄рыса.</ta>
            <ta e="T650" id="Seg_4610" s="T647">kӓндык нилʼдʼик ӓсыса?</ta>
            <ta e="T654" id="Seg_4611" s="T650">′ичек ӓчика jабы[ъ]l, маннон [менон].</ta>
            <ta e="T671" id="Seg_4612" s="T654">′шиндеl че̄l онды шитӓт ка′сат тӱңчотыт, ′ичек ӓчиканты ′ӱтыты ка′сак и′ра соkысʼkо – ичак ӓчикап куттар kосыты на то лыптытып.</ta>
            <ta e="T676" id="Seg_4613" s="T671">ме ай ӱттъ ′паттыртентымыт тоkытkо.</ta>
            <ta e="T701" id="Seg_4614" s="T676">и.-ӓ. kӓт[ы]сыты: сӱтkоныңыlыт [сӱтkолʼнотыт, сӱтkылʼ′нолʼит] сырыт kобыноны kо′татып [сӱннеlыт kотатып сырыт kобылʼ], оlчиңыlыт ′морʼат kаныkты, мат ′тезинды ′сӱттап kо̄танты, kӓнтап ӱттъ пӱса, марГъ пӱса.</ta>
            <ta e="T705" id="Seg_4615" s="T701">тӓ kондеlит мунтык тоk.</ta>
            <ta e="T712" id="Seg_4616" s="T705">′уɣот коl′сак и′ра (началʼник) ′кудъ ′кыка ′kӓттоɣоны.</ta>
            <ta e="T721" id="Seg_4617" s="T712">тӓп сӱткоlситы ′ӯго[ы]т коlсак и′рап, ныны ′мӓныl kумимты (ка′сатып).</ta>
            <ta e="T729" id="Seg_4618" s="T721">ныны ичак ӓчика онты ′kӓkолапсыты сӱтыпыl kотатып kумытсӓ ′ӱттъ.</ta>
            <ta e="T736" id="Seg_4619" s="T729">kӓтысыты kолʼсак и′ранык ′пе̄ты тоk морʼан ′илɣыт.</ta>
            <ta e="T743" id="Seg_4620" s="T736">никун а′за kонтаl тоkыты и то′варыт.</ta>
            <ta e="T745" id="Seg_4621" s="T743">меlды ′kунанты.</ta>
            <ta e="T752" id="Seg_4622" s="T745">те на масып ′кӓкысыlыт, ӱттъ сып kӓ̄сылыт.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T2" id="Seg_4623" s="T0">qolʼsaqo ira.</ta>
            <ta e="T5" id="Seg_4624" s="T2">ilʼimpa ičak äčika imlʼantɨsä.</ta>
            <ta e="T11" id="Seg_4625" s="T5">kolʼsaq iralʼ qəttɨ – asa kundaqɨn eːsa.</ta>
            <ta e="T20" id="Seg_4626" s="T11">okkɨr čondoqɨt qolʼsaqo iralʼ sɨrɨtɨ üːra (üːrɨsa) telʼde üːrɨ[u]qa.</ta>
            <ta e="T25" id="Seg_4627" s="T20">täp tänɨmɨtɨ što ičak äčikat tälʼɨsɨt.</ta>
            <ta e="T33" id="Seg_4628" s="T25">üːtotɨ täpɨnɨk kazatɨp sitatɨp männɨmp̂ɨqo qoip metɨt iːčak äčika.</ta>
            <ta e="T42" id="Seg_4629" s="T33">täp tänɨmɨtɨ tüntoːtɨt rusʼaksä (puškatsä) täpɨp orqəlʼtäntotɨt [orqɨlʼqo] omdɨlʼdentotɨt čurmantɨ.</ta>
            <ta e="T51" id="Seg_4630" s="T42">na vremʼaqɨt (na čʼelʼe) sɨrɨp qə[ə]ssɨtɨ, kʼätɨmtɨ kɨmsa qamdɨsɨtɨ.</ta>
            <ta e="T56" id="Seg_4631" s="T51">merɨsɨtɨ marqə paŋɨp moːta (poːrq[q]ɨnt) [poːroqot].</ta>
            <ta e="T61" id="Seg_4632" s="T56">imlʼamtɨ tamdɨlʼsɨtɨ kämɨlʼ sɨrɨt kʼetɨsa.</ta>
            <ta e="T64" id="Seg_4633" s="T61">imlʼat toqalʼdistɨ porqat.</ta>
            <ta e="T68" id="Seg_4634" s="T64">kazat (kazaqɨt) tüːsoːtɨt ičakäčikanɨk.</ta>
            <ta e="T72" id="Seg_4635" s="T68">marqa qup qätsɨtɨ ičakäčikanɨ.</ta>
            <ta e="T76" id="Seg_4636" s="T72">tasɨntɨ omdɨlʼʼtɨqo notna türʼmantɨ.</ta>
            <ta e="T80" id="Seg_4637" s="T76">tat sɨrɨtɨp qättalʼ amnal.</ta>
            <ta e="T84" id="Seg_4638" s="T80">täp nʼenʼnʼa moːssa imlʼandɨkini qätsɨtɨ.</ta>
            <ta e="T87" id="Seg_4639" s="T84">massɨp omdalʼtɨntoːtɨt čürmantɨ.</ta>
            <ta e="T90" id="Seg_4640" s="T87">čajnik mužeret känpɨlʼa.</ta>
            <ta e="T95" id="Seg_4641" s="T90">imlʼatɨ toːtälʼna topop utop čüssa.</ta>
            <ta e="T99" id="Seg_4642" s="T95">kazat nɨnqotɨt motan oːqɨt.</ta>
            <ta e="T107" id="Seg_4643" s="T99">ičak äčika imlʼandɨkini - ta lʼenčajmont čajnik asa kɨkantɨ musɨrɨqo.</ta>
            <ta e="T116" id="Seg_4644" s="T107">nʼenʼnʼimossa ičʼäk äčika paŋɨmtɨ ilʼlʼa iːsɨtɨ imʼlʼamdɨ mattärsɨtɨ [mattɨrsɨtɨ] imʼlʼatɨ qulʼč[č]isa.</ta>
            <ta e="T124" id="Seg_4645" s="T116">nɨnɨ i. qätsɨtɨ kazatkini - imʼlʼam melʼde nilʼčik orɨnʼnʼa.</ta>
            <ta e="T127" id="Seg_4646" s="T124">paŋɨtɨ top̂oqɨntɨ logɨr[ɨ]sɨtɨ.</ta>
            <ta e="T132" id="Seg_4647" s="T127">imʼlʼa innä paktäš, paŋɨp ilʼlʼaptenta.</ta>
            <ta e="T139" id="Seg_4648" s="T132">nɨnɨ imʼlʼatɨ nɨlʼlʼeisa qänpɨlʼlʼe orqɨlsɨtɨ čajniktɨ musɨrɨqo.</ta>
            <ta e="T144" id="Seg_4649" s="T139">kanzap naqqɨlʼdʼat kuntɨ čajnik musejsa.</ta>
            <ta e="T149" id="Seg_4650" s="T144">nʼanʼ m apsɨp tottɨsɨtɨ lʼemtɨ qänpɨlʼlʼä.</ta>
            <ta e="T151" id="Seg_4651" s="T149">kazat amɨrqolapsotɨt.</ta>
            <ta e="T158" id="Seg_4652" s="T151">täpɨt nilʼdʼik qätɨsotɨt me tat paŋɨp täːmentomɨt.</ta>
            <ta e="T163" id="Seg_4653" s="T158">me iːmaiːmat lʼenčajtoːtɨt i quntoːtɨt.</ta>
            <ta e="T172" id="Seg_4654" s="T163">qazat azä lagɨ[r]qolapsotɨt ičak äčikap, qätäntoːtɨt [qätɨsotɨt] qolʼsak iːranɨk qɨsoːtɨt.</ta>
            <ta e="T174" id="Seg_4655" s="T172">no qätɨmp̂oːtɨt.</ta>
            <ta e="T178" id="Seg_4656" s="T174">načalʼnik kuralʼdɨstɨ tämɨqo paŋɨp.</ta>
            <ta e="T181" id="Seg_4657" s="T178">sindelɨl čeːlʼ täːmotɨt.</ta>
            <ta e="T189" id="Seg_4658" s="T181">qolʼsaq ira qättɨsɨtɨ paŋɨp makke miŋel[lʼ]ɨt, mat sʼaqɨlʼtentap.</ta>
            <ta e="T194" id="Seg_4659" s="T189">ma imam i rapotnikit lʼenčajtoːtɨt.</ta>
            <ta e="T198" id="Seg_4660" s="T194">čeːlʼit i piːt qontotɨt.</ta>
            <ta e="T204" id="Seg_4661" s="T198">ičak äčika qätɨmpatɨ [qatɨsɨt]: paŋɨp čistaŋ[k] märɨŋɨlʼɨt.</ta>
            <ta e="T207" id="Seg_4662" s="T204">soma qumɨp matɨrqo.</ta>
            <ta e="T214" id="Seg_4663" s="T207">qolʼsaq ira qondɨsa qarɨt čeŋ[k] omd̂ɨsa [mäsɨsa].</ta>
            <ta e="T219" id="Seg_4664" s="T214">ima omd̂äšik, i rapotnikit omd̂äŋɨlʼɨt!</ta>
            <ta e="T223" id="Seg_4665" s="T219">täpɨt ček assa omnäntotɨt.</ta>
            <ta e="T224" id="Seg_4666" s="T223">nʼenʼnʼimos[s]a.</ta>
            <ta e="T229" id="Seg_4667" s="T224">ilʼlʼa iːsɨtɨ paŋamt moːtär[lʼ]sɨtɨ muntɨk.</ta>
            <ta e="T235" id="Seg_4668" s="T229">paŋɨp loГɨrsɨtɨ topoqɨntɨ muntɨk mattɨrpɨlʼ qumiqantɨ.</ta>
            <ta e="T237" id="Seg_4669" s="T235">čap qättɨqɨtɨ.</ta>
            <ta e="T239" id="Seg_4670" s="T237">melʼde qulʼčoːtɨt.</ta>
            <ta e="T243" id="Seg_4671" s="T239">ira nʼenʼnʼmossa kazatɨp qärɨsɨt.</ta>
            <ta e="T246" id="Seg_4672" s="T243">kun iːzaqɨt paŋɨp. </ta>
            <ta e="T251" id="Seg_4673" s="T246">täp (ičäk) meqɨni paŋɨp misɨtɨ.</ta>
            <ta e="T254" id="Seg_4674" s="T251">täp ičäk äːčika oːlʼäksa.</ta>
            <ta e="T257" id="Seg_4675" s="T254">qälʼlʼa orГɨlʼqo sudʼinqo.</ta>
            <ta e="T264" id="Seg_4676" s="T257">imaiːmɨt [imam iːmat]mundɨk quːsoːtɨt paŋɨnon.</ta>
            <ta e="T269" id="Seg_4677" s="T264">tat onnäka aza totɨk mattɨrsa.</ta>
            <ta e="T276" id="Seg_4678" s="T269">täpɨp qäːčiqo okkur čeːlʼ me aj saŋartomɨt.</ta>
            <ta e="T280" id="Seg_4679" s="T276">me iːmaiːmɨt aj lʼenčajtotɨt.</ta>
            <ta e="T287" id="Seg_4680" s="T280">qolʼsaq ira aj qätɨsɨtɨ lanno saŋarnɨlʼɨt paŋɨp!</ta>
            <ta e="T293" id="Seg_4681" s="T287">kazat šität nilʼdʼik matälʼsɨtɨ ondi imaiːt.</ta>
            <ta e="T297" id="Seg_4682" s="T293">iːmaiːt quːsoːtɨt qämɨt čoːrɨk.</ta>
            <ta e="T305" id="Seg_4683" s="T297">qarɨt qätɨsotɨt qolʼsaq iranɨk me imaimɨt melʼde quːsoːtɨt.</ta>
            <ta e="T307" id="Seg_4684" s="T305">ira nʼenʼnʼamossa.</ta>
            <ta e="T310" id="Seg_4685" s="T307">ičak äčikap orqɨlʼqo notna.</ta>
            <ta e="T312" id="Seg_4686" s="T310">kazat qäːsoːtɨt.</ta>
            <ta e="T316" id="Seg_4687" s="T312">ičak äčika na vremʼaqɨt motqɨntɨ.</ta>
            <ta e="T318" id="Seg_4688" s="T316">ičak äčikap orqɨlʼsotɨt.</ta>
            <ta e="T320" id="Seg_4689" s="T318">sep molʼmɨtɨsantɨ.</ta>
            <ta e="T330" id="Seg_4690" s="T320">me mundɨk iːmaimɨt i rapotnikit matälʼsimɨt, täpɨt quːsoːtɨt qämɨtčorɨk.</ta>
            <ta e="T333" id="Seg_4691" s="T330">ičak äčikap qäntɨsoːtɨt türmantɨ.</ta>
            <ta e="T339" id="Seg_4692" s="T333">kolʼsaq ira kuralʼdistɨ ilʼlʼe qottɨrɨqo üttə.</ta>
            <ta e="T345" id="Seg_4693" s="T339">qätɨsɨtɨ sɨrɨt qopontɨ šünʼnʼontɨ šütqo ilʼelʼä.</ta>
            <ta e="T354" id="Seg_4694" s="T345">kazat täpɨp qändɨsotɨt čündɨlʼ qaqlʼɨsä üt toptɨ moret qanɨktɨ.</ta>
            <ta e="T359" id="Seg_4695" s="T354">täp qotačündä(o?)qɨt ilʼɨlʼa ippa.</ta>
            <ta e="T367" id="Seg_4696" s="T359">qätɨsɨtɨ masɨp qoptɨrʼantɨlʼɨt marГə püsa marГə pü peːqɨlʼnɨkɨt.</ta>
            <ta e="T372" id="Seg_4697" s="T367">täpɨt qäːsoːtɨt peːqo marГə püp.</ta>
            <ta e="T380" id="Seg_4698" s="T372">kundɨ lʼi qomɨča iːpɨmpa üŋelʼdɨmpat čündɨlʼ qaqlʼɨ tünta.</ta>
            <ta e="T385" id="Seg_4699" s="T380">tättɨ lʼi sompɨlʼa qaqlʼɨt tüːnta.</ta>
            <ta e="T386" id="Seg_4700" s="T385">utɨr[ɨ]soːtɨt.</ta>
            <ta e="T392" id="Seg_4701" s="T386">tämqup toqsä [tawarsä, lɨptɨksä] qänta moqɨnä.</ta>
            <ta e="T397" id="Seg_4702" s="T392">tämqup topolʼsɨtɨ sɨrɨt kopɨlʼ qotap.</ta>
            <ta e="T400" id="Seg_4703" s="T397">qoi tɨmdɨ ippa?</ta>
            <ta e="T403" id="Seg_4704" s="T400">ičik äčika lankalʼsa ijoː!</ta>
            <ta e="T406" id="Seg_4705" s="T403">tämqup – qoitqo iːppant.</ta>
            <ta e="T413" id="Seg_4706" s="T406">И.-i. qätɨsɨtɨ, innä sim üːtas! toː matalʼtɨ. </ta>
            <ta e="T416" id="Seg_4707" s="T413">täp toː matalʼolʼsɨt.</ta>
            <ta e="T420" id="Seg_4708" s="T416">täp innä putalʼmossa [tantɨsa].</ta>
            <ta e="T427" id="Seg_4709" s="T420">täpɨn mɨqɨt siːt qopɨlʼ mɨ eppa munqɨntɨ.</ta>
            <ta e="T431" id="Seg_4710" s="T427">tämqup soqonʼet qojtqo iːpantɨ.</ta>
            <ta e="T438" id="Seg_4711" s="T431">mat iːppap, üttɨ patɨrsak sit qopɨp iːsap.</ta>
            <ta e="T445" id="Seg_4712" s="T438">ormɨ čeːŋqɨsa, toq [lɨptɨk] qočči ütqət innä.</ta>
            <ta e="T449" id="Seg_4713" s="T445">adɨlʼsitɨ sit qopɨtɨp tämqum[ɨ]nɨk.</ta>
            <ta e="T455" id="Seg_4714" s="T449">tämqum o[a]ndalʼsa ma aj pattɨrlʼek ütt.</ta>
            <ta e="T459" id="Seg_4715" s="T455">täpɨp sütäsitɨ sɨrɨtqotantɨ.</ta>
            <ta e="T463" id="Seg_4716" s="T459">tämqup ippa üttotqɨp qotaqɨt.</ta>
            <ta e="T469" id="Seg_4717" s="T463">ičak äčika qənnajsa, čündɨlʼ qaqlʼɨntɨsa i toqsa.</ta>
            <ta e="T474" id="Seg_4718" s="T469">kazat tatɨsotɨt marГə püp sorɨqolapsotɨt.</ta>
            <ta e="T480" id="Seg_4719" s="T474">täntoːtɨt ondɨnʼandɨ sep ičak äčika olɨŋɨrsal.</ta>
            <ta e="T482" id="Seg_4720" s="T480">ütqɨt qunnant.</ta>
            <ta e="T488" id="Seg_4721" s="T482">tämqup üŋgelʼdɨmpat sorɨqandalʼdimpotɨt püp, marГə püp.</ta>
            <ta e="T491" id="Seg_4722" s="T488">asa mat ičik äčika.</ta>
            <ta e="T493" id="Seg_4723" s="T491">ičäk äčika qə[ä]ssa.</ta>
            <ta e="T496" id="Seg_4724" s="T493">tap masɨm oːlʼɨksa.</ta>
            <ta e="T498" id="Seg_4725" s="T496">mat tämqumoŋok.</ta>
            <ta e="T503" id="Seg_4726" s="T498">olʼɨГlʼä masɨp süssa qopɨlʼ qotantɨ.</ta>
            <ta e="T509" id="Seg_4727" s="T503">kasat tomnat, tat ičäk äčikantɨ sep molʼmɨtɨsantɨ.</ta>
            <ta e="T515" id="Seg_4728" s="T509">tona čurɨlʼʼa laŋ[k]ɨnʼa qotaqɨt mat tämqumoŋok.</ta>
            <ta e="T518" id="Seg_4729" s="T515">tüntɨsak čüntɨsa, qaqlʼɨsa.</ta>
            <ta e="T523" id="Seg_4730" s="T518">üttə tulʼdɨlʼa čattɨsoːtɨt qoptɨlʼsoːtɨt püːsa.</ta>
            <ta e="T525" id="Seg_4731" s="T523">moqɨnä qässotɨt.</ta>
            <ta e="T528" id="Seg_4732" s="T525">me qoptɨrsɨmɨt üttə.</ta>
            <ta e="T534" id="Seg_4733" s="T528">ičik äčika tüsa moqɨna čündɨsa, čündɨlʼ qaqlʼɨsa.</ta>
            <ta e="T541" id="Seg_4734" s="T534">toqtɨ üsse čondaptɨstɨ tämnontɨ innä ɨtalʼsitɨ täqɨqo.</ta>
            <ta e="T556" id="Seg_4735" s="T541">na vremʼaqɨt načalʼnik qolʼsak ira mennɨmp̂a ičak äčikat motɨlʼ pelʼaktɨ šoqɨr čotɨmpɨt, purqɨqatqa šoqɨrnonɨ.</ta>
            <ta e="T563" id="Seg_4736" s="T556">tɨmdɨsä [nɨmdɨsä] qättɨsɨtɨ qaẑatɨkinik qälʼlʼa mennɨmpɨqo imaqotap.</ta>
            <ta e="T565" id="Seg_4737" s="T563">täpɨt qäːsoːtɨt.</ta>
            <ta e="T575" id="Seg_4738" s="T565">mottɨ särsotɨt, imaqotat qoptɨčoːt omta stolɨniqɨt ičäk äčika nʼenʼnʼamotpa.</ta>
            <ta e="T581" id="Seg_4739" s="T575">mundɨk mändɨtɨ qatolʼpat qämča, porГɨmtɨ nɨtɨmpɨlʼa.</ta>
            <ta e="T586" id="Seg_4740" s="T581">täpɨt qätɨsoːt [sersotɨt] – d̂orova, lʼaka.</ta>
            <ta e="T601" id="Seg_4741" s="T586">a iček äčika nenʼnʼimolʼa oːmta, tä masɨp ütɨsɨp qoptɨrɨsɨlɨt mat morʼaniːlqɨt kotčik toq [lɨptɨq] qosap.</ta>
            <ta e="T611" id="Seg_4742" s="T601">näsaŋ eŋa kusak orɨm äːsa iːsap [ma orɨmɨ asa iːsɨtɨ].</ta>
            <ta e="T615" id="Seg_4743" s="T611">pɨtta üːdenɨlʼ nomГɨn eːsak.</ta>
            <ta e="T621" id="Seg_4744" s="T615">täpɨt qäːsoːtɨt nɨrgɨmolʼlʼä qoilʼ japol äsa.</ta>
            <ta e="T643" id="Seg_4745" s="T621">qolʼsak iranɨk qätɨqo ičak äčikap üttə qoptɨrsɨmɨt ikolʼ? čʼeːlʼe (okot) täp nassa qompatɨ toq [lɨptɨk] kušalʼ tätaqoj mundɨk qumpatɨ i sit qopɨlʼmɨt.</ta>
            <ta e="T647" id="Seg_4746" s="T643">qolʼsak ira tänɨtɨ üːrɨsa.</ta>
            <ta e="T650" id="Seg_4747" s="T647">qändɨk nilʼdʼik äsɨsa?</ta>
            <ta e="T654" id="Seg_4748" s="T650">iček äčika japɨ[ə]lʼ, mannon [menon].</ta>
            <ta e="T671" id="Seg_4749" s="T654">šindelʼ čeːlʼ ondɨ šität kasat tüŋčotɨt, iček äčikantɨ ütɨtɨ kasak ira soqɨsʼqo – ičak äčikap kuttar qosɨtɨ na to lɨptɨtɨp.</ta>
            <ta e="T676" id="Seg_4750" s="T671">me aj üttə pattɨrtentɨmɨt toqɨtqo.</ta>
            <ta e="T701" id="Seg_4751" s="T676">i.-ä. qät[ɨ]sɨtɨ: sütqonɨŋɨlʼɨt [sütqolʼnotɨt, sütqɨlʼnolʼit] sɨrɨt qopɨnonɨ qotatɨp [sünnelʼɨt qotatɨp sɨrɨt qopɨlʼ], olʼčiŋɨlʼɨt morʼat qanɨqtɨ, mat tezindɨ süttap qoːtantɨ, qäntap üttə püsa, marГə püsa.</ta>
            <ta e="T705" id="Seg_4752" s="T701">tä qondelʼit muntɨk toq.</ta>
            <ta e="T712" id="Seg_4753" s="T705">uqot kolʼsak ira (načalʼnik) kudə kɨka qättoqonɨ.</ta>
            <ta e="T721" id="Seg_4754" s="T712">täp sütkolʼsitɨ uːgo[ɨ]t kolʼsak irap, nɨnɨ mänɨlʼ qumimtɨ (kasatɨp).</ta>
            <ta e="T729" id="Seg_4755" s="T721">nɨnɨ ičak äčika ontɨ qäqolapsɨtɨ sütɨpɨlʼ qotatɨp qumɨtsä üttə.</ta>
            <ta e="T736" id="Seg_4756" s="T729">qätɨsɨtɨ qolʼsak iranɨk peːtɨ toq morʼan ilqɨt.</ta>
            <ta e="T743" id="Seg_4757" s="T736">nikun aza qontalʼ toqɨtɨ i tovarɨt.</ta>
            <ta e="T745" id="Seg_4758" s="T743">melʼdɨ qunantɨ.</ta>
            <ta e="T752" id="Seg_4759" s="T745">te na masɨp käkɨsɨlʼɨt, üttə sɨp qäːsɨlɨt.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2" id="Seg_4760" s="T0">Qolʼsaqo ira. </ta>
            <ta e="T5" id="Seg_4761" s="T2">İlʼimpa İčʼakäčʼika imlʼantɨsä. </ta>
            <ta e="T11" id="Seg_4762" s="T5">Qolʼsaq iralʼ qəːttɨ– asa kuntaːqɨn ɛːsa. </ta>
            <ta e="T20" id="Seg_4763" s="T11">Okkɨr čʼontoːqɨt Qolʼsaqo iralʼ sɨːrɨtɨ üra (ürɨsa) telʼde ürɨkka. </ta>
            <ta e="T25" id="Seg_4764" s="T20">Täp tɛnɨmɨtɨ što İčʼakäčʼikat tälʼɨsɨt. </ta>
            <ta e="T33" id="Seg_4765" s="T25">Üːtɔːtɨ täpɨnɨk kazatɨp sitatɨp männɨmpɨqo qoip meːtɨt İčʼakäčʼika. </ta>
            <ta e="T42" id="Seg_4766" s="T33">Täp tɛnɨmɨtɨ tüntɔːtɨt rusʼaksä (puškatsä) täpɨp orqəlʼtɛntɔːtɨt omtɨltɛntɔːtɨt tʼurmantɨ. </ta>
            <ta e="T51" id="Seg_4767" s="T42">Na vremʼaqɨt (na čʼeːlʼe) sɨːrɨp qəssɨtɨ, ketɨmtɨ kɨmsa qamtɨsɨtɨ. </ta>
            <ta e="T56" id="Seg_4768" s="T51">Merɨsɨtɨ marqɨ paŋɨp mɔːta pɔːrɔːqot. </ta>
            <ta e="T61" id="Seg_4769" s="T56">İmlʼamtɨ tamtɨlsɨtɨ kämɨlʼ sɨːrɨt ketɨsa. </ta>
            <ta e="T64" id="Seg_4770" s="T61">İmlʼat tokkaltistɨ porqat. </ta>
            <ta e="T68" id="Seg_4771" s="T64">Kazat (kazaqɨt) tüsɔːtɨt İčʼakäčʼikanɨk. </ta>
            <ta e="T72" id="Seg_4772" s="T68">Marqa qup kätsɨtɨ İčʼakäčʼikanɨ. </ta>
            <ta e="T76" id="Seg_4773" s="T72">“Tasɨntɨ omtɨlʼtɨqo nɔːtna türʼmantɨ. </ta>
            <ta e="T80" id="Seg_4774" s="T76">Tat sɨːrɨtɨp qättal amnal”. </ta>
            <ta e="T84" id="Seg_4775" s="T80">Täp nʼenʼnʼamɔːssa imlʼantɨkinı kätsɨtɨ. </ta>
            <ta e="T87" id="Seg_4776" s="T84">“Massɨp omtalʼtɨntɔːtɨt tʼürmantɨ. </ta>
            <ta e="T90" id="Seg_4777" s="T87">Čʼajnik mušerät känpɨlʼa”. </ta>
            <ta e="T95" id="Seg_4778" s="T90">İmlʼatɨ toːtälʼna:“ Topop utop čʼüssa”. </ta>
            <ta e="T99" id="Seg_4779" s="T95">Kazat nɨŋnɔːtɨt mɔːtan ɔːqqɨt. </ta>
            <ta e="T107" id="Seg_4780" s="T99">İčakäčika imlʼantɨkinı:“ Ta lʼentʼajŋɔːnt, čʼajnik asa kɨkantɨ musɨrɨqo”. </ta>
            <ta e="T116" id="Seg_4781" s="T107">Nʼenʼnʼimɔːssa İčʼäkäčʼika, paŋɨmtɨ ılla iːsɨtɨ, imlʼamtɨ mattɨrsɨtɨ, imlʼatɨ qulʼčʼisa. </ta>
            <ta e="T124" id="Seg_4782" s="T116">Nɨnɨ İčʼäkäčʼika kätsɨtɨ kazatkinı:“ İmlʼam melʼte nılʼčʼik orɨnʼnʼa”. </ta>
            <ta e="T127" id="Seg_4783" s="T124">Paŋɨtɨ topɔːqɨntɨ loːqɨrɨsɨtɨ. </ta>
            <ta e="T132" id="Seg_4784" s="T127">“İmlʼa ınnä paktäš, paŋɨp ilʼaptɛnta”. </ta>
            <ta e="T139" id="Seg_4785" s="T132">Nɨnɨ imlʼatɨ nɨlleisa, känpɨlä orqɨlsɨtɨ čʼajniktɨ musɨrɨqo. </ta>
            <ta e="T144" id="Seg_4786" s="T139">Kansap naqqɨlʼčʼat kuntɨ čʼajnik musejsa. </ta>
            <ta e="T149" id="Seg_4787" s="T144">Nʼanʼɨm apsɨp tottɨsɨtɨ lʼemtɨ känpɨlä. </ta>
            <ta e="T151" id="Seg_4788" s="T149">Kazat amɨrqolapsɔːtɨt. </ta>
            <ta e="T158" id="Seg_4789" s="T151">Täpɨt nılʼčʼik kätɨsɔːtɨt:“ Me tat paŋɨp täːmɛntɔːmɨt. </ta>
            <ta e="T163" id="Seg_4790" s="T158">Me iːmaiːmat lʼentʼajtɔːtɨt i quntɔːtɨt”. </ta>
            <ta e="T172" id="Seg_4791" s="T163">Kazat asä laqɨrqolapsɔːtɨt İčʼakäčʼikap, kätɛntɔːtɨt (/kätɨsɔːtɨt) Qolʼsak iːranɨk qɨssɔːtɨt. </ta>
            <ta e="T174" id="Seg_4792" s="T172">No kätɨmpɔːtɨt. </ta>
            <ta e="T178" id="Seg_4793" s="T174">Načʼalʼnik kuralʼtɨstɨ tämɨqo paŋɨp. </ta>
            <ta e="T181" id="Seg_4794" s="T178">Sintelɨl čʼeːlʼ täːmɔːtɨt. </ta>
            <ta e="T189" id="Seg_4795" s="T181">Qolʼsaq ira kättɨsɨtɨ:“ Paŋɨp makke miŋelɨt, mat sʼaqɨlʼtɛntap. </ta>
            <ta e="T194" id="Seg_4796" s="T189">Ma imam i rapotnikit lʼentʼajtɔːtɨt. </ta>
            <ta e="T198" id="Seg_4797" s="T194">Čʼeːlʼit i piːt qontɔːtɨt”. </ta>
            <ta e="T204" id="Seg_4798" s="T198">İčʼakäčʼika kätɨmpatɨ (katɨsɨt):“ Paŋɨp čʼistaŋ märɨŋɨlʼɨt. </ta>
            <ta e="T207" id="Seg_4799" s="T204">Soma qumɨp matɨrqo”. </ta>
            <ta e="T214" id="Seg_4800" s="T207">Qolʼsaq ira qontɨsa qarɨt čʼeŋ omtɨsa (mäsɨsa). </ta>
            <ta e="T219" id="Seg_4801" s="T214">“İma omtäšik, i rapotnikit omtäŋɨlʼɨt!” </ta>
            <ta e="T223" id="Seg_4802" s="T219">Täpɨt čʼek assa omnäntɔːtɨt. </ta>
            <ta e="T224" id="Seg_4803" s="T223">Nʼenʼnʼimɔːssa. </ta>
            <ta e="T229" id="Seg_4804" s="T224">Illa iːsɨtɨ paŋamt moːtälsɨtɨ muntɨk. </ta>
            <ta e="T235" id="Seg_4805" s="T229">Paŋɨp loːqɨrsɨtɨ topɔːqɨntɨ muntɨk mattɨrpɨlʼ qumiːqantɨ. </ta>
            <ta e="T237" id="Seg_4806" s="T235">Čʼap kättɨqɨtɨ. </ta>
            <ta e="T239" id="Seg_4807" s="T237">Melʼte qulʼčʼɔːtɨt. </ta>
            <ta e="T243" id="Seg_4808" s="T239">İra nʼenʼnʼmɔːssa kazatɨp qärɨsɨt. </ta>
            <ta e="T246" id="Seg_4809" s="T243">“Kun iːsaqɨt paŋɨp?” </ta>
            <ta e="T251" id="Seg_4810" s="T246">“Täp (İčʼäk) mekɨnı paŋɨp misɨtɨ. </ta>
            <ta e="T254" id="Seg_4811" s="T251">Täp İčʼäkäːčʼika ɔːläksa”. </ta>
            <ta e="T257" id="Seg_4812" s="T254">“Qälʼlʼa orqɨlʼqo sudʼinqo. </ta>
            <ta e="T264" id="Seg_4813" s="T257">İmaiːmɨt (imam, iːmat) muntɨk qusɔːtɨt paŋɨn nɔːn”. </ta>
            <ta e="T269" id="Seg_4814" s="T264">“Tat onnäka asa tɔːtɨk mattɨrsa. </ta>
            <ta e="T276" id="Seg_4815" s="T269">Täpɨp qäːčʼiqo okkur čʼeːl, meː aj saŋartɔːmɨt. </ta>
            <ta e="T280" id="Seg_4816" s="T276">Me iːmaiːmɨt aj lʼentʼajtɔːtɨt”. </ta>
            <ta e="T287" id="Seg_4817" s="T280">Qolʼsaq ira aj kätɨsɨtɨ:“ Lanno, saŋarŋɨlɨt paŋɨp!” </ta>
            <ta e="T293" id="Seg_4818" s="T287">Kazat šität nılʼčʼik mattälsɨtɨ onti imaiːt. </ta>
            <ta e="T297" id="Seg_4819" s="T293">İːmaiːt qusɔːtɨt kämɨt čʼɔːrɨk. </ta>
            <ta e="T305" id="Seg_4820" s="T297">Qarɨt kätɨsɔːtɨt Qolʼsaq iranɨk:“ Meː imaiːmɨt melʼte qusɔːtɨt”. </ta>
            <ta e="T307" id="Seg_4821" s="T305">İra nʼenʼnʼamɔːssa. </ta>
            <ta e="T310" id="Seg_4822" s="T307">“İčʼakäčʼikap orqɨlʼqo nɔːtna”. </ta>
            <ta e="T312" id="Seg_4823" s="T310">Kazat qässɔːtɨt. </ta>
            <ta e="T316" id="Seg_4824" s="T312">İčʼakäčʼika na vremʼaqɨt mɔːtqɨntɨ. </ta>
            <ta e="T318" id="Seg_4825" s="T316">İčʼakäčʼikap orqɨlʼsɔːtɨt. </ta>
            <ta e="T320" id="Seg_4826" s="T318">“Seːp moːlmɨttɨsantɨ. </ta>
            <ta e="T330" id="Seg_4827" s="T320">Me muntɨk iːmaiːmɨt i rapotnikit mattälsimɨt, täpɨt qusɔːtɨt kämɨt čʼɔːrɨk”. </ta>
            <ta e="T333" id="Seg_4828" s="T330">İčʼakäčʼikap qäntɨsɔːtɨt türmantɨ. </ta>
            <ta e="T339" id="Seg_4829" s="T333">Kolʼsaq ira kuraltistɨ ıllä qottɨrɨqo üttɨ. </ta>
            <ta e="T345" id="Seg_4830" s="T339">Kätɨsɨtɨ sɨːrɨt qopoːntɨ šünʼnʼontɨ šütqo ilʼelä. </ta>
            <ta e="T354" id="Seg_4831" s="T345">Kazat täpɨp qäntɨsɔːtɨt čʼuntɨlʼ qaqlɨsä üt toːptɨ, moret qanɨktɨ. </ta>
            <ta e="T359" id="Seg_4832" s="T354">Täp kota čʼuntoːqɨt ilɨla ippa. </ta>
            <ta e="T367" id="Seg_4833" s="T359">Kätɨsɨtɨ:“ Masɨp qoptɨrantɨlʼɨt marqɨ püsa, marqɨ pü peːqɨlʼnɨkɨt”. </ta>
            <ta e="T372" id="Seg_4834" s="T367">Täpɨt qässɔːtɨt peːqo marqɨ püp. </ta>
            <ta e="T380" id="Seg_4835" s="T372">Kuntɨ lʼi qɔːmɨčʼa ippɨmpa, üŋkeltɨmpat čʼuntɨlʼ qaqlɨ tünta. </ta>
            <ta e="T385" id="Seg_4836" s="T380">Tɛːttɨ lʼi sompɨlʼa qaqlɨt tüːnta. </ta>
            <ta e="T386" id="Seg_4837" s="T385">Utɨrɨsɔːtɨt. </ta>
            <ta e="T392" id="Seg_4838" s="T386">Tämqup tɔːqsä (/tawarsä, lɨptɨksä) qänta moqɨnä. </ta>
            <ta e="T397" id="Seg_4839" s="T392">Tämqup topɔːlsɨtɨ sɨːrɨt qopɨlʼ kotap. </ta>
            <ta e="T400" id="Seg_4840" s="T397">Qoi tɨmtɨ ippa? </ta>
            <ta e="T403" id="Seg_4841" s="T400">İčʼikäčʼika laŋkalʼsa:“ İjoː!” </ta>
            <ta e="T406" id="Seg_4842" s="T403">Tämqup:“ Qoitqo ippant?” </ta>
            <ta e="T413" id="Seg_4843" s="T406">İčʼakäčʼika kätɨsɨtɨ:“ Innä sım üːtas, toː maːtaltɨ!” </ta>
            <ta e="T416" id="Seg_4844" s="T413">Täp toː maːtalʼolʼsɨt. </ta>
            <ta e="T420" id="Seg_4845" s="T416">Täp ınnä putalʼmɔːssa (tantɨsa). </ta>
            <ta e="T427" id="Seg_4846" s="T420">Täpɨn mɨqɨt sıːt qopɨlʼ mɨ ɛppa muŋkɨntɨ. </ta>
            <ta e="T431" id="Seg_4847" s="T427">Tämqup soqonʼnʼet:“ Qojtqo ippantɨ?” </ta>
            <ta e="T438" id="Seg_4848" s="T431">“Mat ippap, üttɨ pattɨrsak sıːt qopɨp iːsap. </ta>
            <ta e="T445" id="Seg_4849" s="T438">Ormɨ čʼäːŋkɨsa, tɔːq (lɨptɨk) kočʼčʼɨ ütqɨt ınnä”. </ta>
            <ta e="T449" id="Seg_4850" s="T445">Atɨlʼsitɨ sıːt qopɨtɨp tämqumnɨk. </ta>
            <ta e="T455" id="Seg_4851" s="T449">Tämqum ɔːntalʼsa:“ Ma aj pattɨrläk ütt”. </ta>
            <ta e="T459" id="Seg_4852" s="T455">Täpɨp süttɛːsitɨ sɨːrɨt kotantɨ. </ta>
            <ta e="T463" id="Seg_4853" s="T459">Tämqup ippa üttotqɨp kotaqɨt. </ta>
            <ta e="T469" id="Seg_4854" s="T463">İčʼakäčʼika qənnajsa, čʼuntɨlʼ qaqlɨntɨsa i tɔːqsa. </ta>
            <ta e="T474" id="Seg_4855" s="T469">Kazat taːtɨsɔːtɨt marqɨ püp sɔːrɨqolapsɔːtɨt. </ta>
            <ta e="T480" id="Seg_4856" s="T474">Täntɔːtɨt ontɨ nʼantɨ:“ Seːp İčʼakäčʼika olɨŋɨrsal. </ta>
            <ta e="T482" id="Seg_4857" s="T480">Ütqɨt qunnant”. </ta>
            <ta e="T488" id="Seg_4858" s="T482">Tämqup üŋkeltɨmpat sɔːrɨqantaltɨmpɔːtɨt püp, marqɨ püp. </ta>
            <ta e="T491" id="Seg_4859" s="T488">“Asa mat İčʼikäčʼika. </ta>
            <ta e="T493" id="Seg_4860" s="T491">İčʼäkäčʼika qəssa. </ta>
            <ta e="T496" id="Seg_4861" s="T493">Tap masım oːlʼɨksa. </ta>
            <ta e="T498" id="Seg_4862" s="T496">Mat tämqumoŋok. </ta>
            <ta e="T503" id="Seg_4863" s="T498">Olʼɨklä masɨp süssa qopɨlʼ kotantɨ”. </ta>
            <ta e="T509" id="Seg_4864" s="T503">Kasat tomnat:“ Tat İčʼäkäčʼikantɨ seːp moːlmɨtɨsantɨ”. </ta>
            <ta e="T515" id="Seg_4865" s="T509">Toːnna čʼuːrɨla laŋkɨnʼnʼa kotaqɨt:“ Mat tämqumoŋok. </ta>
            <ta e="T518" id="Seg_4866" s="T515">Tüntɨsak čʼuntɨsa, qaqlɨsa”. </ta>
            <ta e="T523" id="Seg_4867" s="T518">Üttɨ tulʼtɨlʼa čʼattɨsɔːtɨt qoptɨlʼsɔːtɨt püːsa. </ta>
            <ta e="T525" id="Seg_4868" s="T523">Moqɨnä qässɔːtɨt. </ta>
            <ta e="T528" id="Seg_4869" s="T525">“Meː qoptɨrsɨmɨt üttɨ”. </ta>
            <ta e="T534" id="Seg_4870" s="T528">İčʼikäčʼika tüsa moqɨna čʼuntɨsa, čʼuntɨlʼ qaqlɨsa. </ta>
            <ta e="T541" id="Seg_4871" s="T534">Tɔːqtɨ üssä čʼontaptɨstɨ tɛːmnoːntɨ ınnä ɨːtälsitɨ täkɨqo. </ta>
            <ta e="T556" id="Seg_4872" s="T541">Na vremʼaqɨt načʼalʼnik Qolʼsak ira mennɨmpa İčʼakäčʼikat mɔːtɨlʼ pɛlʼaktɨ šoːqɨr čʼɔːtɨmpɨt, purqɨ qatqa šoːqɨr nɔːnɨ. </ta>
            <ta e="T563" id="Seg_4873" s="T556">Tɨmtɨsä (nɨmtɨsä) kättɨsɨtɨ kazatɨqınik qälʼlʼa mennɨmpɨqo imaqotap. </ta>
            <ta e="T565" id="Seg_4874" s="T563">Täpɨt qäːssɔːtɨt. </ta>
            <ta e="T575" id="Seg_4875" s="T565">Mɔːttɨ seːrsɔːtɨt, imaqotat koptɨ čʼɔːt omta stolɨn iːqɨt İčʼäkäčʼika nʼenʼnʼamɔːtpa. </ta>
            <ta e="T581" id="Seg_4876" s="T575">Muntɨk mäntɨtɨ qatolʼpat kämčʼa, porqɨmtɨ nɨtɨmpɨlʼa. </ta>
            <ta e="T586" id="Seg_4877" s="T581">Täpɨt kätɨsɔːt (seːrsɔːtɨt)–“ Dorova, lʼaqa”. </ta>
            <ta e="T601" id="Seg_4878" s="T586">A İčʼekäčʼika nenʼnʼimɔːlla omta:“ Tɛː masɨp ütɨsɨp qoptɨrɨsɨlɨt, mat morʼan ılqɨt kotčʼik tɔːq (lɨptɨk) qosap. </ta>
            <ta e="T611" id="Seg_4879" s="T601">Näsaŋ eŋa kusak orɨm ɛːsa iːsap (ma orɨmɨ asa iːsɨtɨ). </ta>
            <ta e="T615" id="Seg_4880" s="T611">Bɨtta ütenɨlʼ nomqɨn ɛːsak”. </ta>
            <ta e="T621" id="Seg_4881" s="T615">Täpɨt qässɔːtɨt nɨrkɨmɔːllä:“ Qoilʼ jabol ɛːsa”. </ta>
            <ta e="T643" id="Seg_4882" s="T621">Qolʼsak iranɨk kätɨqo:“ İčʼakäčʼikap üttɨ qoptɨrsɨmɨt ikolʼ čʼeːlʼe (okot) täp nassa qompatɨ tɔːq (lɨptɨk) kušalʼ tätaqoj muntɨk qompatɨ i sıːt qopɨlʼ mɨt”. </ta>
            <ta e="T647" id="Seg_4883" s="T643">Qolʼsak ira tɛnɨtɨ ürɨsa. </ta>
            <ta e="T650" id="Seg_4884" s="T647">“Qäntɨk nılʼčʼik ɛsɨsa? </ta>
            <ta e="T654" id="Seg_4885" s="T650">İčʼekäčʼika jabəlʼ, mannon (menon)”. </ta>
            <ta e="T671" id="Seg_4886" s="T654">Šintelʼ čʼeːlʼ ontɨ šität kasat tüŋčʼɔːtɨt, İčʼekäčʼikantɨ üːtɨtɨ kasak ira soqɨšqo İčʼakäčʼikap kuttar qosɨtɨ na to lɨptɨtɨp. </ta>
            <ta e="T676" id="Seg_4887" s="T671">Meː aj üttɨ pattɨrtɛntɨmɨt tɔːqɨtqo. </ta>
            <ta e="T701" id="Seg_4888" s="T676">İčʼakäčʼika kätɨsɨtɨ: Sütqonɨŋɨlʼɨt (sütqolʼnɔːtɨt, sütqɨlʼnɔːlʼit) sɨːrɨt qopɨ nɔːnɨ kotatɨp (sünnelʼɨt kotatɨp sɨːrɨt qopɨlʼ), ɔːlʼčʼiŋɨlʼɨt morʼat qanɨqtɨ, mat tɛːsintɨ süttap kotantɨ, käntap üttɨ püsa, marqɨ püsa. </ta>
            <ta e="T705" id="Seg_4889" s="T701">Tɛː qontelʼit muntɨk tɔːq”. </ta>
            <ta e="T712" id="Seg_4890" s="T705">Ukoːt Qolʼsak ira (načʼalʼnik):“ Kutə kɨka qättoːqonɨ?” </ta>
            <ta e="T721" id="Seg_4891" s="T712">Täp sütkolʼsitɨ ukoːt Qolʼsak irap, nɨnɨ mänɨlʼ qumiːmtɨ (kasatɨp). </ta>
            <ta e="T729" id="Seg_4892" s="T721">Nɨnɨ İčʼakäčʼika ontɨ käqolapsɨtɨ sütɨpɨlʼ kotatɨp qumɨtsä üttɨ. </ta>
            <ta e="T736" id="Seg_4893" s="T729">Kätɨsɨtɨ Qolʼsak iranɨk:“ Peːtɨ tɔːq morʼan ılqɨt. </ta>
            <ta e="T743" id="Seg_4894" s="T736">Nʼi kun asa qontal tɔːqɨtɨ i tovarɨt. </ta>
            <ta e="T745" id="Seg_4895" s="T743">Meːltɨ qunnantɨ. </ta>
            <ta e="T752" id="Seg_4896" s="T745">Tɛː na masɨp käkkɨsɨlɨt, üttɨ sɨp käːsɨlɨt. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_4897" s="T0">Qolʼsaqo</ta>
            <ta e="T2" id="Seg_4898" s="T1">ira</ta>
            <ta e="T3" id="Seg_4899" s="T2">ilʼi-mpa</ta>
            <ta e="T4" id="Seg_4900" s="T3">İčʼakäčʼika</ta>
            <ta e="T5" id="Seg_4901" s="T4">imlʼa-ntɨ-sä</ta>
            <ta e="T6" id="Seg_4902" s="T5">Qolʼsaq</ta>
            <ta e="T7" id="Seg_4903" s="T6">ira-lʼ</ta>
            <ta e="T8" id="Seg_4904" s="T7">qəːttɨ</ta>
            <ta e="T9" id="Seg_4905" s="T8">asa</ta>
            <ta e="T10" id="Seg_4906" s="T9">kuntaːqɨn</ta>
            <ta e="T11" id="Seg_4907" s="T10">ɛː-sa</ta>
            <ta e="T12" id="Seg_4908" s="T11">okkɨr</ta>
            <ta e="T13" id="Seg_4909" s="T12">čʼontoː-qɨt</ta>
            <ta e="T14" id="Seg_4910" s="T13">Qolʼsaqo</ta>
            <ta e="T15" id="Seg_4911" s="T14">ira-lʼ</ta>
            <ta e="T16" id="Seg_4912" s="T15">sɨːrɨ-tɨ</ta>
            <ta e="T17" id="Seg_4913" s="T16">üra</ta>
            <ta e="T18" id="Seg_4914" s="T17">ürɨ-sa</ta>
            <ta e="T20" id="Seg_4915" s="T19">ürɨ-kka</ta>
            <ta e="T21" id="Seg_4916" s="T20">täp</ta>
            <ta e="T22" id="Seg_4917" s="T21">tɛnɨmɨ-tɨ</ta>
            <ta e="T23" id="Seg_4918" s="T22">što</ta>
            <ta e="T24" id="Seg_4919" s="T23">İčʼakäčʼika-t</ta>
            <ta e="T25" id="Seg_4920" s="T24">tälʼɨ-sɨ-t</ta>
            <ta e="T26" id="Seg_4921" s="T25">üːtɔː-tɨ</ta>
            <ta e="T27" id="Seg_4922" s="T26">täp-ɨ-nɨk</ta>
            <ta e="T28" id="Seg_4923" s="T27">kaza-t-ɨ-p</ta>
            <ta e="T29" id="Seg_4924" s="T28">sita-t-ɨ-p</ta>
            <ta e="T30" id="Seg_4925" s="T29">männɨ-mpɨ-qo</ta>
            <ta e="T31" id="Seg_4926" s="T30">qo-i-p</ta>
            <ta e="T32" id="Seg_4927" s="T31">meː-tɨ-t</ta>
            <ta e="T33" id="Seg_4928" s="T32">İčʼakäčʼika</ta>
            <ta e="T34" id="Seg_4929" s="T33">täp</ta>
            <ta e="T35" id="Seg_4930" s="T34">tɛnɨmɨ-tɨ</ta>
            <ta e="T36" id="Seg_4931" s="T35">tü-ntɔː-tɨt</ta>
            <ta e="T37" id="Seg_4932" s="T36">rusʼak-sä</ta>
            <ta e="T38" id="Seg_4933" s="T37">puška-t-sä</ta>
            <ta e="T39" id="Seg_4934" s="T38">täp-ɨ-p</ta>
            <ta e="T40" id="Seg_4935" s="T39">orqəlʼ-tɛntɔː-tɨt</ta>
            <ta e="T41" id="Seg_4936" s="T40">omtɨ-lt-ɛntɔː-tɨt</ta>
            <ta e="T42" id="Seg_4937" s="T41">tʼurma-ntɨ</ta>
            <ta e="T43" id="Seg_4938" s="T42">na</ta>
            <ta e="T44" id="Seg_4939" s="T43">vremʼa-qɨt</ta>
            <ta e="T45" id="Seg_4940" s="T44">na</ta>
            <ta e="T46" id="Seg_4941" s="T45">čʼeːlʼe</ta>
            <ta e="T47" id="Seg_4942" s="T46">sɨːrɨ-p</ta>
            <ta e="T48" id="Seg_4943" s="T47">qəs-sɨ-tɨ</ta>
            <ta e="T49" id="Seg_4944" s="T48">ketɨ-m-tɨ</ta>
            <ta e="T50" id="Seg_4945" s="T49">kɨm-sa</ta>
            <ta e="T51" id="Seg_4946" s="T50">qamtɨ-sɨ-tɨ</ta>
            <ta e="T52" id="Seg_4947" s="T51">merɨ-sɨ-tɨ</ta>
            <ta e="T53" id="Seg_4948" s="T52">marqɨ</ta>
            <ta e="T54" id="Seg_4949" s="T53">paŋɨ-p</ta>
            <ta e="T55" id="Seg_4950" s="T54">mɔːt-a</ta>
            <ta e="T56" id="Seg_4951" s="T55">pɔːrɔː-qot</ta>
            <ta e="T57" id="Seg_4952" s="T56">imlʼa-m-tɨ</ta>
            <ta e="T58" id="Seg_4953" s="T57">tamtɨl-sɨ-tɨ</ta>
            <ta e="T59" id="Seg_4954" s="T58">käm-ɨ-lʼ</ta>
            <ta e="T60" id="Seg_4955" s="T59">sɨːrɨ-t</ta>
            <ta e="T61" id="Seg_4956" s="T60">ketɨ-sa</ta>
            <ta e="T62" id="Seg_4957" s="T61">imlʼa-t</ta>
            <ta e="T63" id="Seg_4958" s="T62">tokk-alti-s-tɨ</ta>
            <ta e="T64" id="Seg_4959" s="T63">porqa-t</ta>
            <ta e="T65" id="Seg_4960" s="T64">kaza-t</ta>
            <ta e="T66" id="Seg_4961" s="T65">kazaq-ɨ-t</ta>
            <ta e="T67" id="Seg_4962" s="T66">tü-sɔː-tɨt</ta>
            <ta e="T68" id="Seg_4963" s="T67">İčʼakäčʼika-nɨk</ta>
            <ta e="T69" id="Seg_4964" s="T68">marqa</ta>
            <ta e="T70" id="Seg_4965" s="T69">qup</ta>
            <ta e="T71" id="Seg_4966" s="T70">kät-sɨ-tɨ</ta>
            <ta e="T72" id="Seg_4967" s="T71">İčʼakäčʼika-nɨ</ta>
            <ta e="T73" id="Seg_4968" s="T72">tasɨntɨ</ta>
            <ta e="T74" id="Seg_4969" s="T73">omtɨ-lʼtɨ-qo</ta>
            <ta e="T75" id="Seg_4970" s="T74">nɔːtna</ta>
            <ta e="T76" id="Seg_4971" s="T75">türʼma-ntɨ</ta>
            <ta e="T77" id="Seg_4972" s="T76">tan</ta>
            <ta e="T78" id="Seg_4973" s="T77">sɨːrɨ-t-ɨ-p</ta>
            <ta e="T79" id="Seg_4974" s="T78">qät-ta-l</ta>
            <ta e="T80" id="Seg_4975" s="T79">am-na-l</ta>
            <ta e="T81" id="Seg_4976" s="T80">täp</ta>
            <ta e="T82" id="Seg_4977" s="T81">nʼenʼnʼa-mɔːs-sa</ta>
            <ta e="T83" id="Seg_4978" s="T82">imlʼa-ntɨ-kinı</ta>
            <ta e="T84" id="Seg_4979" s="T83">kät-sɨ-tɨ</ta>
            <ta e="T85" id="Seg_4980" s="T84">massɨp</ta>
            <ta e="T86" id="Seg_4981" s="T85">omt-alʼtɨ-ntɔː-tɨt</ta>
            <ta e="T87" id="Seg_4982" s="T86">tʼürma-ntɨ</ta>
            <ta e="T88" id="Seg_4983" s="T87">čʼajnik</ta>
            <ta e="T89" id="Seg_4984" s="T88">muše-r-ät</ta>
            <ta e="T90" id="Seg_4985" s="T89">*känpɨ-lʼa</ta>
            <ta e="T91" id="Seg_4986" s="T90">imlʼa-tɨ</ta>
            <ta e="T92" id="Seg_4987" s="T91">toːt-älʼ-na</ta>
            <ta e="T93" id="Seg_4988" s="T92">topo-p</ta>
            <ta e="T94" id="Seg_4989" s="T93">uto-p</ta>
            <ta e="T95" id="Seg_4990" s="T94">čʼüssa</ta>
            <ta e="T96" id="Seg_4991" s="T95">kaza-t</ta>
            <ta e="T97" id="Seg_4992" s="T96">nɨŋ-nɔː-tɨt</ta>
            <ta e="T98" id="Seg_4993" s="T97">mɔːta-n</ta>
            <ta e="T99" id="Seg_4994" s="T98">ɔːq-qɨt</ta>
            <ta e="T100" id="Seg_4995" s="T99">İčakäčika</ta>
            <ta e="T101" id="Seg_4996" s="T100">imlʼa-ntɨ-kinı</ta>
            <ta e="T102" id="Seg_4997" s="T101">ta</ta>
            <ta e="T103" id="Seg_4998" s="T102">lʼentʼaj-ŋɔː-nt</ta>
            <ta e="T104" id="Seg_4999" s="T103">čʼajnik</ta>
            <ta e="T105" id="Seg_5000" s="T104">asa</ta>
            <ta e="T106" id="Seg_5001" s="T105">kɨka-ntɨ</ta>
            <ta e="T107" id="Seg_5002" s="T106">musɨ-rɨ-qo</ta>
            <ta e="T108" id="Seg_5003" s="T107">nʼenʼnʼi-mɔːs-sa</ta>
            <ta e="T109" id="Seg_5004" s="T108">İčʼäkäčʼika</ta>
            <ta e="T110" id="Seg_5005" s="T109">paŋɨ-m-tɨ</ta>
            <ta e="T111" id="Seg_5006" s="T110">ılla</ta>
            <ta e="T112" id="Seg_5007" s="T111">iː-sɨ-tɨ</ta>
            <ta e="T113" id="Seg_5008" s="T112">imlʼa-m-tɨ</ta>
            <ta e="T114" id="Seg_5009" s="T113">mattɨ-r-sɨ-tɨ</ta>
            <ta e="T115" id="Seg_5010" s="T114">imlʼa-tɨ</ta>
            <ta e="T116" id="Seg_5011" s="T115">qu-lʼčʼi-sa</ta>
            <ta e="T117" id="Seg_5012" s="T116">nɨnɨ</ta>
            <ta e="T118" id="Seg_5013" s="T117">İčʼäkäčʼika</ta>
            <ta e="T119" id="Seg_5014" s="T118">kät-sɨ-tɨ</ta>
            <ta e="T120" id="Seg_5015" s="T119">kaza-t-kinı</ta>
            <ta e="T121" id="Seg_5016" s="T120">imlʼa-m</ta>
            <ta e="T122" id="Seg_5017" s="T121">melʼte</ta>
            <ta e="T123" id="Seg_5018" s="T122">nılʼčʼi-k</ta>
            <ta e="T124" id="Seg_5019" s="T123">orɨ-nʼ-nʼa</ta>
            <ta e="T125" id="Seg_5020" s="T124">paŋɨ-tɨ</ta>
            <ta e="T126" id="Seg_5021" s="T125">topɔː-qɨn-tɨ</ta>
            <ta e="T127" id="Seg_5022" s="T126">loːqɨ-rɨ-sɨ-tɨ</ta>
            <ta e="T128" id="Seg_5023" s="T127">imlʼa</ta>
            <ta e="T129" id="Seg_5024" s="T128">ınnä</ta>
            <ta e="T130" id="Seg_5025" s="T129">pakt-äš</ta>
            <ta e="T131" id="Seg_5026" s="T130">paŋɨ-p</ta>
            <ta e="T132" id="Seg_5027" s="T131">ilʼ-apt-ɛnta</ta>
            <ta e="T133" id="Seg_5028" s="T132">nɨnɨ</ta>
            <ta e="T134" id="Seg_5029" s="T133">imlʼa-tɨ</ta>
            <ta e="T135" id="Seg_5030" s="T134">nɨll-ei-sa</ta>
            <ta e="T136" id="Seg_5031" s="T135">*känpɨ-lä</ta>
            <ta e="T137" id="Seg_5032" s="T136">orqɨl-sɨ-tɨ</ta>
            <ta e="T138" id="Seg_5033" s="T137">čʼajnik-tɨ</ta>
            <ta e="T139" id="Seg_5034" s="T138">musɨ-rɨ-qo</ta>
            <ta e="T140" id="Seg_5035" s="T139">kansa-p</ta>
            <ta e="T141" id="Seg_5036" s="T140">naq-qɨlʼ-čʼa-t</ta>
            <ta e="T142" id="Seg_5037" s="T141">kuntɨ</ta>
            <ta e="T143" id="Seg_5038" s="T142">čʼajnik</ta>
            <ta e="T144" id="Seg_5039" s="T143">mus-ej-sa</ta>
            <ta e="T145" id="Seg_5040" s="T144">nʼanʼ-ɨ-m</ta>
            <ta e="T146" id="Seg_5041" s="T145">apsɨ-p</ta>
            <ta e="T147" id="Seg_5042" s="T146">tottɨ-sɨ-tɨ</ta>
            <ta e="T148" id="Seg_5043" s="T147">lʼem-tɨ</ta>
            <ta e="T149" id="Seg_5044" s="T148">*känpɨ-lä</ta>
            <ta e="T150" id="Seg_5045" s="T149">kaza-t</ta>
            <ta e="T151" id="Seg_5046" s="T150">am-ɨ-r-q-olap-sɔː-tɨt</ta>
            <ta e="T152" id="Seg_5047" s="T151">täp-ɨ-t</ta>
            <ta e="T153" id="Seg_5048" s="T152">nılʼčʼi-k</ta>
            <ta e="T154" id="Seg_5049" s="T153">kätɨ-sɔː-tɨt</ta>
            <ta e="T155" id="Seg_5050" s="T154">me</ta>
            <ta e="T156" id="Seg_5051" s="T155">tan</ta>
            <ta e="T157" id="Seg_5052" s="T156">paŋɨ-p</ta>
            <ta e="T158" id="Seg_5053" s="T157">täːm-ɛntɔː-mɨt</ta>
            <ta e="T159" id="Seg_5054" s="T158">me</ta>
            <ta e="T160" id="Seg_5055" s="T159">iːma-iː-mat</ta>
            <ta e="T161" id="Seg_5056" s="T160">lʼentʼaj-tɔː-tɨt</ta>
            <ta e="T162" id="Seg_5057" s="T161">i</ta>
            <ta e="T163" id="Seg_5058" s="T162">quntɔː-tɨt</ta>
            <ta e="T164" id="Seg_5059" s="T163">kaza-t</ta>
            <ta e="T165" id="Seg_5060" s="T164">asä</ta>
            <ta e="T166" id="Seg_5061" s="T165">laqɨ-r-q-olap-sɔː-tɨt</ta>
            <ta e="T167" id="Seg_5062" s="T166">İčʼakäčʼika-p</ta>
            <ta e="T168" id="Seg_5063" s="T167">kät-ɛntɔː-tɨt</ta>
            <ta e="T169" id="Seg_5064" s="T168">kätɨ-sɔː-tɨt</ta>
            <ta e="T170" id="Seg_5065" s="T169">Qolʼsak</ta>
            <ta e="T171" id="Seg_5066" s="T170">iːra-nɨk</ta>
            <ta e="T172" id="Seg_5067" s="T171">qɨs-sɔː-tɨt</ta>
            <ta e="T173" id="Seg_5068" s="T172">no</ta>
            <ta e="T174" id="Seg_5069" s="T173">kätɨ-mpɔː-tɨt</ta>
            <ta e="T175" id="Seg_5070" s="T174">načʼalʼnik</ta>
            <ta e="T176" id="Seg_5071" s="T175">kur-alʼtɨ-s-tɨ</ta>
            <ta e="T177" id="Seg_5072" s="T176">tämɨ-qo</ta>
            <ta e="T178" id="Seg_5073" s="T177">paŋɨ-p</ta>
            <ta e="T179" id="Seg_5074" s="T178">sintelɨl</ta>
            <ta e="T180" id="Seg_5075" s="T179">čʼeːlʼ</ta>
            <ta e="T181" id="Seg_5076" s="T180">täːmɔː-tɨt</ta>
            <ta e="T182" id="Seg_5077" s="T181">Qolʼsaq</ta>
            <ta e="T183" id="Seg_5078" s="T182">ira</ta>
            <ta e="T184" id="Seg_5079" s="T183">kättɨ-sɨ-tɨ</ta>
            <ta e="T185" id="Seg_5080" s="T184">paŋɨ-p</ta>
            <ta e="T186" id="Seg_5081" s="T185">makke</ta>
            <ta e="T187" id="Seg_5082" s="T186">mi-ŋelɨt</ta>
            <ta e="T188" id="Seg_5083" s="T187">mat</ta>
            <ta e="T189" id="Seg_5084" s="T188">sʼaqɨ-lʼt-ɛnta-p</ta>
            <ta e="T190" id="Seg_5085" s="T189">ma</ta>
            <ta e="T191" id="Seg_5086" s="T190">ima-m</ta>
            <ta e="T192" id="Seg_5087" s="T191">i</ta>
            <ta e="T193" id="Seg_5088" s="T192">rapotnik-i-t</ta>
            <ta e="T194" id="Seg_5089" s="T193">lʼentʼaj-tɔː-tɨt</ta>
            <ta e="T195" id="Seg_5090" s="T194">čʼeːlʼi-t</ta>
            <ta e="T196" id="Seg_5091" s="T195">i</ta>
            <ta e="T197" id="Seg_5092" s="T196">piː-t</ta>
            <ta e="T198" id="Seg_5093" s="T197">qontɔː-tɨt</ta>
            <ta e="T199" id="Seg_5094" s="T198">İčʼakäčʼika</ta>
            <ta e="T200" id="Seg_5095" s="T199">kätɨ-mpa-tɨ</ta>
            <ta e="T201" id="Seg_5096" s="T200">katɨ-sɨ-t</ta>
            <ta e="T202" id="Seg_5097" s="T201">paŋɨ-p</ta>
            <ta e="T203" id="Seg_5098" s="T202">čʼista-ŋ</ta>
            <ta e="T204" id="Seg_5099" s="T203">märɨ-ŋɨlʼɨt</ta>
            <ta e="T205" id="Seg_5100" s="T204">soma</ta>
            <ta e="T206" id="Seg_5101" s="T205">qum-ɨ-p</ta>
            <ta e="T207" id="Seg_5102" s="T206">matɨ-r-qo</ta>
            <ta e="T208" id="Seg_5103" s="T207">Qolʼsaq</ta>
            <ta e="T209" id="Seg_5104" s="T208">ira</ta>
            <ta e="T210" id="Seg_5105" s="T209">qontɨ-sa</ta>
            <ta e="T211" id="Seg_5106" s="T210">qarɨ-t</ta>
            <ta e="T212" id="Seg_5107" s="T211">čʼeŋ</ta>
            <ta e="T213" id="Seg_5108" s="T212">omtɨ-sa</ta>
            <ta e="T214" id="Seg_5109" s="T213">mäsɨ-sa</ta>
            <ta e="T215" id="Seg_5110" s="T214">ima</ta>
            <ta e="T216" id="Seg_5111" s="T215">omt-äšik</ta>
            <ta e="T217" id="Seg_5112" s="T216">i</ta>
            <ta e="T218" id="Seg_5113" s="T217">rapotnik-i-t</ta>
            <ta e="T219" id="Seg_5114" s="T218">omtä-ŋɨlʼɨt</ta>
            <ta e="T220" id="Seg_5115" s="T219">täp-ɨ-t</ta>
            <ta e="T221" id="Seg_5116" s="T220">čʼek</ta>
            <ta e="T222" id="Seg_5117" s="T221">assa</ta>
            <ta e="T223" id="Seg_5118" s="T222">omnä-ntɔː-tɨt</ta>
            <ta e="T224" id="Seg_5119" s="T223">nʼenʼnʼi-mɔːs-sa</ta>
            <ta e="T225" id="Seg_5120" s="T224">ılla</ta>
            <ta e="T226" id="Seg_5121" s="T225">iː-sɨ-tɨ</ta>
            <ta e="T227" id="Seg_5122" s="T226">paŋa-m-t</ta>
            <ta e="T228" id="Seg_5123" s="T227">moːt-äl-sɨ-tɨ</ta>
            <ta e="T229" id="Seg_5124" s="T228">muntɨk</ta>
            <ta e="T230" id="Seg_5125" s="T229">paŋɨ-p</ta>
            <ta e="T231" id="Seg_5126" s="T230">loːqɨ-r-sɨ-tɨ</ta>
            <ta e="T232" id="Seg_5127" s="T231">topɔː-qɨn-tɨ</ta>
            <ta e="T233" id="Seg_5128" s="T232">muntɨk</ta>
            <ta e="T234" id="Seg_5129" s="T233">mattɨ-r-pɨlʼ</ta>
            <ta e="T235" id="Seg_5130" s="T234">qum-iː-qan-tɨ</ta>
            <ta e="T236" id="Seg_5131" s="T235">čʼap</ta>
            <ta e="T237" id="Seg_5132" s="T236">kättɨ-qɨ-tɨ</ta>
            <ta e="T238" id="Seg_5133" s="T237">melʼte</ta>
            <ta e="T239" id="Seg_5134" s="T238">qu-lʼčʼɔː-tɨt</ta>
            <ta e="T240" id="Seg_5135" s="T239">ira</ta>
            <ta e="T241" id="Seg_5136" s="T240">nʼenʼnʼ-mɔːs-sa</ta>
            <ta e="T242" id="Seg_5137" s="T241">kaza-t-ɨ-p</ta>
            <ta e="T243" id="Seg_5138" s="T242">qärɨ-sɨ-t</ta>
            <ta e="T244" id="Seg_5139" s="T243">kun</ta>
            <ta e="T245" id="Seg_5140" s="T244">iː-sa-qɨt</ta>
            <ta e="T246" id="Seg_5141" s="T245">paŋɨ-p</ta>
            <ta e="T247" id="Seg_5142" s="T246">täp</ta>
            <ta e="T248" id="Seg_5143" s="T247">İčʼäk</ta>
            <ta e="T249" id="Seg_5144" s="T248">me-kɨnı</ta>
            <ta e="T250" id="Seg_5145" s="T249">paŋɨ-p</ta>
            <ta e="T251" id="Seg_5146" s="T250">mi-sɨ-tɨ</ta>
            <ta e="T252" id="Seg_5147" s="T251">täp</ta>
            <ta e="T253" id="Seg_5148" s="T252">İčʼäkäːčʼika</ta>
            <ta e="T254" id="Seg_5149" s="T253">ɔːläk-sa</ta>
            <ta e="T255" id="Seg_5150" s="T254">qäl-lʼa</ta>
            <ta e="T256" id="Seg_5151" s="T255">orqɨlʼ-qo</ta>
            <ta e="T257" id="Seg_5152" s="T256">sudʼin-qo</ta>
            <ta e="T258" id="Seg_5153" s="T257">ima-iː-mɨt</ta>
            <ta e="T259" id="Seg_5154" s="T258">ima-m</ta>
            <ta e="T260" id="Seg_5155" s="T259">iːma-t</ta>
            <ta e="T261" id="Seg_5156" s="T260">muntɨk</ta>
            <ta e="T262" id="Seg_5157" s="T261">qu-sɔː-tɨt</ta>
            <ta e="T263" id="Seg_5158" s="T262">paŋɨ-n</ta>
            <ta e="T264" id="Seg_5159" s="T263">nɔː-n</ta>
            <ta e="T265" id="Seg_5160" s="T264">tat</ta>
            <ta e="T266" id="Seg_5161" s="T265">onnäka</ta>
            <ta e="T267" id="Seg_5162" s="T266">asa</ta>
            <ta e="T268" id="Seg_5163" s="T267">tɔːt-ɨ-k</ta>
            <ta e="T269" id="Seg_5164" s="T268">mattɨ-r-sa</ta>
            <ta e="T270" id="Seg_5165" s="T269">täp-ɨ-p</ta>
            <ta e="T271" id="Seg_5166" s="T270">qäːčʼi-qo</ta>
            <ta e="T272" id="Seg_5167" s="T271">okkur</ta>
            <ta e="T273" id="Seg_5168" s="T272">čʼeːl</ta>
            <ta e="T274" id="Seg_5169" s="T273">meː</ta>
            <ta e="T275" id="Seg_5170" s="T274">aj</ta>
            <ta e="T276" id="Seg_5171" s="T275">saŋa-r-tɔː-mɨt</ta>
            <ta e="T277" id="Seg_5172" s="T276">me</ta>
            <ta e="T278" id="Seg_5173" s="T277">iːma-iː-mɨt</ta>
            <ta e="T279" id="Seg_5174" s="T278">aj</ta>
            <ta e="T280" id="Seg_5175" s="T279">lʼentʼaj-tɔː-tɨt</ta>
            <ta e="T281" id="Seg_5176" s="T280">Qolʼsaq</ta>
            <ta e="T282" id="Seg_5177" s="T281">ira</ta>
            <ta e="T283" id="Seg_5178" s="T282">aj</ta>
            <ta e="T284" id="Seg_5179" s="T283">kätɨ-sɨ-tɨ</ta>
            <ta e="T285" id="Seg_5180" s="T284">lanno</ta>
            <ta e="T286" id="Seg_5181" s="T285">saŋa-r-ŋɨlɨt</ta>
            <ta e="T287" id="Seg_5182" s="T286">paŋɨ-p</ta>
            <ta e="T288" id="Seg_5183" s="T287">kaza-t</ta>
            <ta e="T289" id="Seg_5184" s="T288">šitä-t</ta>
            <ta e="T290" id="Seg_5185" s="T289">nılʼčʼi-k</ta>
            <ta e="T291" id="Seg_5186" s="T290">matt-äl-sɨ-tɨ</ta>
            <ta e="T292" id="Seg_5187" s="T291">onti</ta>
            <ta e="T293" id="Seg_5188" s="T292">ima-iː-t</ta>
            <ta e="T294" id="Seg_5189" s="T293">iːma-iː-t</ta>
            <ta e="T295" id="Seg_5190" s="T294">qu-sɔː-tɨt</ta>
            <ta e="T296" id="Seg_5191" s="T295">käm-ɨ-t</ta>
            <ta e="T297" id="Seg_5192" s="T296">čʼɔːr-ɨ-k</ta>
            <ta e="T298" id="Seg_5193" s="T297">qarɨ-t</ta>
            <ta e="T299" id="Seg_5194" s="T298">kätɨ-sɔː-tɨt</ta>
            <ta e="T300" id="Seg_5195" s="T299">Qolʼsaq</ta>
            <ta e="T301" id="Seg_5196" s="T300">ira-nɨk</ta>
            <ta e="T302" id="Seg_5197" s="T301">meː</ta>
            <ta e="T303" id="Seg_5198" s="T302">ima-iː-mɨt</ta>
            <ta e="T304" id="Seg_5199" s="T303">melʼte</ta>
            <ta e="T305" id="Seg_5200" s="T304">qu-sɔː-tɨt</ta>
            <ta e="T306" id="Seg_5201" s="T305">ira</ta>
            <ta e="T307" id="Seg_5202" s="T306">nʼenʼnʼa-mɔːs-sa</ta>
            <ta e="T308" id="Seg_5203" s="T307">İčʼakäčʼika-p</ta>
            <ta e="T309" id="Seg_5204" s="T308">orqɨlʼ-qo</ta>
            <ta e="T310" id="Seg_5205" s="T309">nɔːtna</ta>
            <ta e="T311" id="Seg_5206" s="T310">kaza-t</ta>
            <ta e="T312" id="Seg_5207" s="T311">qäs-sɔː-tɨt</ta>
            <ta e="T313" id="Seg_5208" s="T312">İčʼakäčʼika</ta>
            <ta e="T314" id="Seg_5209" s="T313">na</ta>
            <ta e="T315" id="Seg_5210" s="T314">vremʼa-qɨt</ta>
            <ta e="T316" id="Seg_5211" s="T315">mɔːt-qɨn-tɨ</ta>
            <ta e="T317" id="Seg_5212" s="T316">İčʼakäčʼika-p</ta>
            <ta e="T318" id="Seg_5213" s="T317">orqɨlʼ-sɔː-tɨt</ta>
            <ta e="T319" id="Seg_5214" s="T318">seːp</ta>
            <ta e="T320" id="Seg_5215" s="T319">moːlmɨ-ttɨ-sa-ntɨ</ta>
            <ta e="T321" id="Seg_5216" s="T320">me</ta>
            <ta e="T322" id="Seg_5217" s="T321">muntɨk</ta>
            <ta e="T323" id="Seg_5218" s="T322">iːma-iː-mɨt</ta>
            <ta e="T324" id="Seg_5219" s="T323">i</ta>
            <ta e="T325" id="Seg_5220" s="T324">rapotnik-i-t</ta>
            <ta e="T326" id="Seg_5221" s="T325">matt-äl-si-mɨt</ta>
            <ta e="T327" id="Seg_5222" s="T326">täp-ɨ-t</ta>
            <ta e="T328" id="Seg_5223" s="T327">qu-sɔː-tɨt</ta>
            <ta e="T329" id="Seg_5224" s="T328">käm-ɨ-t</ta>
            <ta e="T330" id="Seg_5225" s="T329">čʼɔːr-ɨ-k</ta>
            <ta e="T331" id="Seg_5226" s="T330">İčʼakäčʼika-p</ta>
            <ta e="T332" id="Seg_5227" s="T331">qän-tɨ-sɔː-tɨt</ta>
            <ta e="T333" id="Seg_5228" s="T332">türma-ntɨ</ta>
            <ta e="T334" id="Seg_5229" s="T333">Kolʼsaq</ta>
            <ta e="T335" id="Seg_5230" s="T334">ira</ta>
            <ta e="T336" id="Seg_5231" s="T335">kur-alti-s-tɨ</ta>
            <ta e="T337" id="Seg_5232" s="T336">ıllä</ta>
            <ta e="T338" id="Seg_5233" s="T337">qottɨ-rɨ-qo</ta>
            <ta e="T339" id="Seg_5234" s="T338">üt-tɨ</ta>
            <ta e="T340" id="Seg_5235" s="T339">kätɨ-sɨ-tɨ</ta>
            <ta e="T341" id="Seg_5236" s="T340">sɨːrɨ-t</ta>
            <ta e="T342" id="Seg_5237" s="T341">qopoː-ntɨ</ta>
            <ta e="T343" id="Seg_5238" s="T342">šünʼnʼo-ntɨ</ta>
            <ta e="T344" id="Seg_5239" s="T343">šüt-qo</ta>
            <ta e="T345" id="Seg_5240" s="T344">ilʼe-lä</ta>
            <ta e="T346" id="Seg_5241" s="T345">kaza-t</ta>
            <ta e="T347" id="Seg_5242" s="T346">täp-ɨ-p</ta>
            <ta e="T348" id="Seg_5243" s="T347">qän-tɨ-sɔː-tɨt</ta>
            <ta e="T349" id="Seg_5244" s="T348">čʼuntɨ-lʼ</ta>
            <ta e="T350" id="Seg_5245" s="T349">qaqlɨ-sä</ta>
            <ta e="T351" id="Seg_5246" s="T350">üt</ta>
            <ta e="T352" id="Seg_5247" s="T351">toːp-tɨ</ta>
            <ta e="T353" id="Seg_5248" s="T352">more-t</ta>
            <ta e="T354" id="Seg_5249" s="T353">qanɨk-tɨ</ta>
            <ta e="T355" id="Seg_5250" s="T354">täp</ta>
            <ta e="T356" id="Seg_5251" s="T355">kota</ta>
            <ta e="T357" id="Seg_5252" s="T356">čʼuntoː-qɨt</ta>
            <ta e="T358" id="Seg_5253" s="T357">ilɨ-la</ta>
            <ta e="T359" id="Seg_5254" s="T358">ippa</ta>
            <ta e="T360" id="Seg_5255" s="T359">kätɨ-sɨ-tɨ</ta>
            <ta e="T361" id="Seg_5256" s="T360">masɨp</ta>
            <ta e="T362" id="Seg_5257" s="T361">qoptɨ-ra-ntɨlʼɨt</ta>
            <ta e="T363" id="Seg_5258" s="T362">marqɨ</ta>
            <ta e="T364" id="Seg_5259" s="T363">pü-sa</ta>
            <ta e="T365" id="Seg_5260" s="T364">marqɨ</ta>
            <ta e="T366" id="Seg_5261" s="T365">pü</ta>
            <ta e="T367" id="Seg_5262" s="T366">peː-qɨlʼ-nɨkɨt</ta>
            <ta e="T368" id="Seg_5263" s="T367">täp-ɨ-t</ta>
            <ta e="T369" id="Seg_5264" s="T368">qäs-sɔː-tɨt</ta>
            <ta e="T370" id="Seg_5265" s="T369">peː-qo</ta>
            <ta e="T371" id="Seg_5266" s="T370">marqɨ</ta>
            <ta e="T372" id="Seg_5267" s="T371">pü-p</ta>
            <ta e="T373" id="Seg_5268" s="T372">kuntɨ</ta>
            <ta e="T374" id="Seg_5269" s="T373">lʼi</ta>
            <ta e="T375" id="Seg_5270" s="T374">qɔːmɨčʼa</ta>
            <ta e="T376" id="Seg_5271" s="T375">ippɨ-mpa</ta>
            <ta e="T377" id="Seg_5272" s="T376">üŋkel-tɨ-mpa-t</ta>
            <ta e="T378" id="Seg_5273" s="T377">čʼuntɨ-lʼ</ta>
            <ta e="T379" id="Seg_5274" s="T378">qaqlɨ</ta>
            <ta e="T380" id="Seg_5275" s="T379">tü-nta</ta>
            <ta e="T381" id="Seg_5276" s="T380">tɛːttɨ</ta>
            <ta e="T382" id="Seg_5277" s="T381">lʼi</ta>
            <ta e="T383" id="Seg_5278" s="T382">sompɨlʼa</ta>
            <ta e="T384" id="Seg_5279" s="T383">qaqlɨ-t</ta>
            <ta e="T385" id="Seg_5280" s="T384">tüː-nta</ta>
            <ta e="T386" id="Seg_5281" s="T385">utɨ-rɨ-sɔː-tɨt</ta>
            <ta e="T387" id="Seg_5282" s="T386">tämqup</ta>
            <ta e="T388" id="Seg_5283" s="T387">tɔːq-sä</ta>
            <ta e="T389" id="Seg_5284" s="T388">tawar-sä</ta>
            <ta e="T390" id="Seg_5285" s="T389">lɨptɨk-sä</ta>
            <ta e="T391" id="Seg_5286" s="T390">qän-ta</ta>
            <ta e="T392" id="Seg_5287" s="T391">moqɨnä</ta>
            <ta e="T393" id="Seg_5288" s="T392">tämqup</ta>
            <ta e="T394" id="Seg_5289" s="T393">top-ɔːl-sɨ-tɨ</ta>
            <ta e="T395" id="Seg_5290" s="T394">sɨːrɨ-t</ta>
            <ta e="T396" id="Seg_5291" s="T395">qopɨ-lʼ</ta>
            <ta e="T397" id="Seg_5292" s="T396">kota-p</ta>
            <ta e="T398" id="Seg_5293" s="T397">qoi</ta>
            <ta e="T399" id="Seg_5294" s="T398">tɨmtɨ</ta>
            <ta e="T400" id="Seg_5295" s="T399">ippa</ta>
            <ta e="T401" id="Seg_5296" s="T400">İčʼikäčʼika</ta>
            <ta e="T402" id="Seg_5297" s="T401">laŋk-alʼ-sa</ta>
            <ta e="T403" id="Seg_5298" s="T402">ijoː</ta>
            <ta e="T404" id="Seg_5299" s="T403">tämqup</ta>
            <ta e="T405" id="Seg_5300" s="T404">qoi-tqo</ta>
            <ta e="T406" id="Seg_5301" s="T405">ippa-nt</ta>
            <ta e="T407" id="Seg_5302" s="T406">İčʼakäčʼika</ta>
            <ta e="T408" id="Seg_5303" s="T407">kätɨ-sɨ-tɨ</ta>
            <ta e="T409" id="Seg_5304" s="T408">ınnä</ta>
            <ta e="T410" id="Seg_5305" s="T409">sım</ta>
            <ta e="T411" id="Seg_5306" s="T410">üːt-as</ta>
            <ta e="T412" id="Seg_5307" s="T411">toː</ta>
            <ta e="T413" id="Seg_5308" s="T412">maːt-al-tɨ</ta>
            <ta e="T414" id="Seg_5309" s="T413">täp</ta>
            <ta e="T415" id="Seg_5310" s="T414">toː</ta>
            <ta e="T416" id="Seg_5311" s="T415">maːt-alʼ-olʼ-sɨ-t</ta>
            <ta e="T417" id="Seg_5312" s="T416">täp</ta>
            <ta e="T418" id="Seg_5313" s="T417">ınnä</ta>
            <ta e="T419" id="Seg_5314" s="T418">*putalʼ-mɔːs-sa</ta>
            <ta e="T420" id="Seg_5315" s="T419">tantɨ-sa</ta>
            <ta e="T421" id="Seg_5316" s="T420">täp-ɨ-n</ta>
            <ta e="T422" id="Seg_5317" s="T421">mɨ-qɨt</ta>
            <ta e="T423" id="Seg_5318" s="T422">sıː-t</ta>
            <ta e="T424" id="Seg_5319" s="T423">qopɨ-lʼ</ta>
            <ta e="T425" id="Seg_5320" s="T424">mɨ</ta>
            <ta e="T426" id="Seg_5321" s="T425">ɛ-ppa</ta>
            <ta e="T427" id="Seg_5322" s="T426">muŋkɨ-n-tɨ</ta>
            <ta e="T428" id="Seg_5323" s="T427">tämqup</ta>
            <ta e="T429" id="Seg_5324" s="T428">soqonʼ-nʼe-t</ta>
            <ta e="T430" id="Seg_5325" s="T429">qoj-tqo</ta>
            <ta e="T431" id="Seg_5326" s="T430">ippa-ntɨ</ta>
            <ta e="T432" id="Seg_5327" s="T431">Mat</ta>
            <ta e="T433" id="Seg_5328" s="T432">ippa-p</ta>
            <ta e="T434" id="Seg_5329" s="T433">üt-tɨ</ta>
            <ta e="T435" id="Seg_5330" s="T434">pat-tɨ-r-sa-k</ta>
            <ta e="T436" id="Seg_5331" s="T435">sıː-t</ta>
            <ta e="T437" id="Seg_5332" s="T436">qopɨ-p</ta>
            <ta e="T438" id="Seg_5333" s="T437">iː-sa-p</ta>
            <ta e="T439" id="Seg_5334" s="T438">or-mɨ</ta>
            <ta e="T440" id="Seg_5335" s="T439">čʼäːŋkɨ-sa</ta>
            <ta e="T441" id="Seg_5336" s="T440">tɔːq</ta>
            <ta e="T442" id="Seg_5337" s="T441">lɨptɨk</ta>
            <ta e="T443" id="Seg_5338" s="T442">kočʼčʼɨ</ta>
            <ta e="T444" id="Seg_5339" s="T443">üt-qɨt</ta>
            <ta e="T445" id="Seg_5340" s="T444">ınnä</ta>
            <ta e="T446" id="Seg_5341" s="T445">atɨ-lʼ-si-tɨ</ta>
            <ta e="T447" id="Seg_5342" s="T446">sıː-t</ta>
            <ta e="T448" id="Seg_5343" s="T447">qopɨ-t-ɨ-p</ta>
            <ta e="T449" id="Seg_5344" s="T448">tämqum-nɨk</ta>
            <ta e="T450" id="Seg_5345" s="T449">tämqum</ta>
            <ta e="T451" id="Seg_5346" s="T450">ɔːnt-alʼ-sa</ta>
            <ta e="T452" id="Seg_5347" s="T451">ma</ta>
            <ta e="T453" id="Seg_5348" s="T452">aj</ta>
            <ta e="T454" id="Seg_5349" s="T453">pat-tɨ-r-lä-k</ta>
            <ta e="T455" id="Seg_5350" s="T454">üt-t</ta>
            <ta e="T456" id="Seg_5351" s="T455">täp-ɨ-p</ta>
            <ta e="T457" id="Seg_5352" s="T456">sütt-ɛː-si-tɨ</ta>
            <ta e="T458" id="Seg_5353" s="T457">sɨːrɨ-t</ta>
            <ta e="T459" id="Seg_5354" s="T458">kota-ntɨ</ta>
            <ta e="T460" id="Seg_5355" s="T459">tämqup</ta>
            <ta e="T461" id="Seg_5356" s="T460">ippa</ta>
            <ta e="T753" id="Seg_5357" s="T461">üt</ta>
            <ta e="T462" id="Seg_5358" s="T753">totqɨp</ta>
            <ta e="T463" id="Seg_5359" s="T462">kota-qɨt</ta>
            <ta e="T464" id="Seg_5360" s="T463">İčʼakäčʼika</ta>
            <ta e="T465" id="Seg_5361" s="T464">qənn-aj-sa</ta>
            <ta e="T466" id="Seg_5362" s="T465">čʼuntɨ-lʼ</ta>
            <ta e="T467" id="Seg_5363" s="T466">qaqlɨ-ntɨ-sa</ta>
            <ta e="T468" id="Seg_5364" s="T467">i</ta>
            <ta e="T469" id="Seg_5365" s="T468">tɔːq-sa</ta>
            <ta e="T470" id="Seg_5366" s="T469">kaza-t</ta>
            <ta e="T471" id="Seg_5367" s="T470">taːtɨ-sɔː-tɨt</ta>
            <ta e="T472" id="Seg_5368" s="T471">marqɨ</ta>
            <ta e="T473" id="Seg_5369" s="T472">pü-p</ta>
            <ta e="T474" id="Seg_5370" s="T473">sɔːrɨ-q-olap-sɔː-tɨt</ta>
            <ta e="T475" id="Seg_5371" s="T474">täntɔː-tɨt</ta>
            <ta e="T476" id="Seg_5372" s="T475">ontɨ</ta>
            <ta e="T477" id="Seg_5373" s="T476">nʼantɨ</ta>
            <ta e="T478" id="Seg_5374" s="T477">seːp</ta>
            <ta e="T479" id="Seg_5375" s="T478">İčʼakäčʼika</ta>
            <ta e="T480" id="Seg_5376" s="T479">olɨŋ-ɨ-r-sa-l</ta>
            <ta e="T481" id="Seg_5377" s="T480">üt-qɨt</ta>
            <ta e="T482" id="Seg_5378" s="T481">qu-nna-nt</ta>
            <ta e="T483" id="Seg_5379" s="T482">tämqup</ta>
            <ta e="T484" id="Seg_5380" s="T483">üŋkel-tɨ-mpa-t</ta>
            <ta e="T485" id="Seg_5381" s="T484">sɔːrɨ-q-antaltɨ-mpɔː-tɨt</ta>
            <ta e="T486" id="Seg_5382" s="T485">pü-p</ta>
            <ta e="T487" id="Seg_5383" s="T486">marqɨ</ta>
            <ta e="T488" id="Seg_5384" s="T487">pü-p</ta>
            <ta e="T489" id="Seg_5385" s="T488">asa</ta>
            <ta e="T490" id="Seg_5386" s="T489">mat</ta>
            <ta e="T491" id="Seg_5387" s="T490">İčʼikäčʼika</ta>
            <ta e="T492" id="Seg_5388" s="T491">İčʼäkäčʼika</ta>
            <ta e="T493" id="Seg_5389" s="T492">qəs-sa</ta>
            <ta e="T494" id="Seg_5390" s="T493">tap</ta>
            <ta e="T495" id="Seg_5391" s="T494">masım</ta>
            <ta e="T496" id="Seg_5392" s="T495">oːlʼɨk-sa</ta>
            <ta e="T497" id="Seg_5393" s="T496">mat</ta>
            <ta e="T498" id="Seg_5394" s="T497">tämqum-o-ŋo-k</ta>
            <ta e="T499" id="Seg_5395" s="T498">olʼɨk-lä</ta>
            <ta e="T500" id="Seg_5396" s="T499">masɨp</ta>
            <ta e="T501" id="Seg_5397" s="T500">süs-sa</ta>
            <ta e="T502" id="Seg_5398" s="T501">qopɨ-lʼ</ta>
            <ta e="T503" id="Seg_5399" s="T502">kota-ntɨ</ta>
            <ta e="T504" id="Seg_5400" s="T503">kasa-t</ta>
            <ta e="T505" id="Seg_5401" s="T504">tom-na-t</ta>
            <ta e="T506" id="Seg_5402" s="T505">tat</ta>
            <ta e="T507" id="Seg_5403" s="T506">İčʼäkäčʼika-ntɨ</ta>
            <ta e="T508" id="Seg_5404" s="T507">seːp</ta>
            <ta e="T509" id="Seg_5405" s="T508">moːlmɨ-tɨ-sa-ntɨ</ta>
            <ta e="T510" id="Seg_5406" s="T509">toːnna</ta>
            <ta e="T511" id="Seg_5407" s="T510">čʼuːrɨ-la</ta>
            <ta e="T512" id="Seg_5408" s="T511">laŋkɨ-nʼ-nʼa</ta>
            <ta e="T513" id="Seg_5409" s="T512">kota-qɨt</ta>
            <ta e="T514" id="Seg_5410" s="T513">Mat</ta>
            <ta e="T515" id="Seg_5411" s="T514">tämqum-o-ŋo-k</ta>
            <ta e="T516" id="Seg_5412" s="T515">tü-ntɨ-sa-k</ta>
            <ta e="T517" id="Seg_5413" s="T516">čʼuntɨ-sa</ta>
            <ta e="T518" id="Seg_5414" s="T517">qaqlɨ-sa</ta>
            <ta e="T519" id="Seg_5415" s="T518">üt-tɨ</ta>
            <ta e="T520" id="Seg_5416" s="T519">tulʼ-tɨ-lʼa</ta>
            <ta e="T521" id="Seg_5417" s="T520">čʼattɨ-sɔː-tɨt</ta>
            <ta e="T522" id="Seg_5418" s="T521">qoptɨ-lʼ-sɔː-tɨt</ta>
            <ta e="T523" id="Seg_5419" s="T522">püː-sa</ta>
            <ta e="T524" id="Seg_5420" s="T523">moqɨnä</ta>
            <ta e="T525" id="Seg_5421" s="T524">qäs-sɔː-tɨt</ta>
            <ta e="T526" id="Seg_5422" s="T525">meː</ta>
            <ta e="T527" id="Seg_5423" s="T526">qoptɨ-r-sɨ-mɨt</ta>
            <ta e="T528" id="Seg_5424" s="T527">üt-tɨ</ta>
            <ta e="T529" id="Seg_5425" s="T528">İčʼikäčʼika</ta>
            <ta e="T530" id="Seg_5426" s="T529">tü-sa</ta>
            <ta e="T531" id="Seg_5427" s="T530">moqɨna</ta>
            <ta e="T532" id="Seg_5428" s="T531">čʼuntɨ-sa</ta>
            <ta e="T533" id="Seg_5429" s="T532">čʼuntɨ-lʼ</ta>
            <ta e="T534" id="Seg_5430" s="T533">qaqlɨ-sa</ta>
            <ta e="T535" id="Seg_5431" s="T534">tɔːq-tɨ</ta>
            <ta e="T536" id="Seg_5432" s="T535">üs-sä</ta>
            <ta e="T537" id="Seg_5433" s="T536">čʼontɨ-ptɨ-s-tɨ</ta>
            <ta e="T538" id="Seg_5434" s="T537">tɛːmnoː-ntɨ</ta>
            <ta e="T539" id="Seg_5435" s="T538">ınnä</ta>
            <ta e="T540" id="Seg_5436" s="T539">ɨːt-äl-si-tɨ</ta>
            <ta e="T541" id="Seg_5437" s="T540">täkɨ-qo</ta>
            <ta e="T542" id="Seg_5438" s="T541">na</ta>
            <ta e="T543" id="Seg_5439" s="T542">vremʼa-qɨt</ta>
            <ta e="T544" id="Seg_5440" s="T543">načʼalʼnik</ta>
            <ta e="T545" id="Seg_5441" s="T544">Qolʼsak</ta>
            <ta e="T546" id="Seg_5442" s="T545">ira</ta>
            <ta e="T547" id="Seg_5443" s="T546">mennɨ-mpa</ta>
            <ta e="T548" id="Seg_5444" s="T547">İčʼakäčʼika-t</ta>
            <ta e="T549" id="Seg_5445" s="T548">mɔːt-ɨ-lʼ</ta>
            <ta e="T550" id="Seg_5446" s="T549">pɛlʼak-tɨ</ta>
            <ta e="T551" id="Seg_5447" s="T550">šoːqɨr</ta>
            <ta e="T552" id="Seg_5448" s="T551">čʼɔːtɨ-mpɨ-t</ta>
            <ta e="T553" id="Seg_5449" s="T552">purqɨ</ta>
            <ta e="T554" id="Seg_5450" s="T553">qatqa</ta>
            <ta e="T555" id="Seg_5451" s="T554">šoːqɨr</ta>
            <ta e="T556" id="Seg_5452" s="T555">nɔː-nɨ</ta>
            <ta e="T557" id="Seg_5453" s="T556">tɨmtɨ-sä</ta>
            <ta e="T558" id="Seg_5454" s="T557">nɨmtɨ-sä</ta>
            <ta e="T559" id="Seg_5455" s="T558">kättɨ-sɨ-tɨ</ta>
            <ta e="T560" id="Seg_5456" s="T559">kazat-ɨ-qı-nik</ta>
            <ta e="T561" id="Seg_5457" s="T560">qäl-lʼa</ta>
            <ta e="T562" id="Seg_5458" s="T561">mennɨ-mpɨ-qo</ta>
            <ta e="T563" id="Seg_5459" s="T562">imaqota-p</ta>
            <ta e="T564" id="Seg_5460" s="T563">täp-ɨ-t</ta>
            <ta e="T565" id="Seg_5461" s="T564">qäːs-sɔː-tɨt</ta>
            <ta e="T566" id="Seg_5462" s="T565">mɔːt-tɨ</ta>
            <ta e="T567" id="Seg_5463" s="T566">seːr-sɔː-tɨt</ta>
            <ta e="T568" id="Seg_5464" s="T567">imaqota-t</ta>
            <ta e="T569" id="Seg_5465" s="T568">koptɨ</ta>
            <ta e="T570" id="Seg_5466" s="T569">čʼɔːt</ta>
            <ta e="T571" id="Seg_5467" s="T570">omta</ta>
            <ta e="T572" id="Seg_5468" s="T571">stol-ɨ-n</ta>
            <ta e="T573" id="Seg_5469" s="T572">iː-qɨt</ta>
            <ta e="T574" id="Seg_5470" s="T573">İčʼäkäčʼika</ta>
            <ta e="T575" id="Seg_5471" s="T574">nʼenʼnʼa-mɔːt-pa</ta>
            <ta e="T576" id="Seg_5472" s="T575">muntɨk</ta>
            <ta e="T577" id="Seg_5473" s="T576">mäntɨ-tɨ</ta>
            <ta e="T578" id="Seg_5474" s="T577">qatolʼ-pa-t</ta>
            <ta e="T579" id="Seg_5475" s="T578">käm-čʼa</ta>
            <ta e="T580" id="Seg_5476" s="T579">porqɨ-m-tɨ</ta>
            <ta e="T581" id="Seg_5477" s="T580">nɨtɨ-mpɨlʼ-a</ta>
            <ta e="T582" id="Seg_5478" s="T581">täp-ɨ-t</ta>
            <ta e="T583" id="Seg_5479" s="T582">kätɨ-sɔː-t</ta>
            <ta e="T584" id="Seg_5480" s="T583">seːr-sɔː-tɨt</ta>
            <ta e="T585" id="Seg_5481" s="T584">dorova</ta>
            <ta e="T586" id="Seg_5482" s="T585">lʼaqa</ta>
            <ta e="T587" id="Seg_5483" s="T586">a</ta>
            <ta e="T588" id="Seg_5484" s="T587">İčʼekäčʼika</ta>
            <ta e="T589" id="Seg_5485" s="T588">nenʼnʼi-mɔːl-la</ta>
            <ta e="T590" id="Seg_5486" s="T589">omta</ta>
            <ta e="T591" id="Seg_5487" s="T590">Tɛː</ta>
            <ta e="T592" id="Seg_5488" s="T591">masɨp</ta>
            <ta e="T593" id="Seg_5489" s="T592">üt-ɨsɨp</ta>
            <ta e="T594" id="Seg_5490" s="T593">qoptɨ-rɨ-sɨ-lɨt</ta>
            <ta e="T595" id="Seg_5491" s="T594">mat</ta>
            <ta e="T596" id="Seg_5492" s="T595">morʼa-n</ta>
            <ta e="T597" id="Seg_5493" s="T596">ılqɨt</ta>
            <ta e="T598" id="Seg_5494" s="T597">kotčʼi-k</ta>
            <ta e="T599" id="Seg_5495" s="T598">tɔːq</ta>
            <ta e="T600" id="Seg_5496" s="T599">lɨptɨk</ta>
            <ta e="T601" id="Seg_5497" s="T600">qo-sa-p</ta>
            <ta e="T602" id="Seg_5498" s="T601">näsaŋ</ta>
            <ta e="T603" id="Seg_5499" s="T602">e-ŋa</ta>
            <ta e="T604" id="Seg_5500" s="T603">kusak</ta>
            <ta e="T605" id="Seg_5501" s="T604">orɨ-m</ta>
            <ta e="T606" id="Seg_5502" s="T605">ɛː-sa</ta>
            <ta e="T607" id="Seg_5503" s="T606">iː-sa-p</ta>
            <ta e="T608" id="Seg_5504" s="T607">ma</ta>
            <ta e="T609" id="Seg_5505" s="T608">orɨ-mɨ</ta>
            <ta e="T610" id="Seg_5506" s="T609">asa</ta>
            <ta e="T611" id="Seg_5507" s="T610">iː-sɨ-tɨ</ta>
            <ta e="T612" id="Seg_5508" s="T611">bɨtta</ta>
            <ta e="T613" id="Seg_5509" s="T612">üt-en-ɨ-lʼ</ta>
            <ta e="T614" id="Seg_5510" s="T613">nom-qɨn</ta>
            <ta e="T615" id="Seg_5511" s="T614">ɛː-sa-k</ta>
            <ta e="T616" id="Seg_5512" s="T615">täp-ɨ-t</ta>
            <ta e="T617" id="Seg_5513" s="T616">qäs-sɔː-tɨt</ta>
            <ta e="T618" id="Seg_5514" s="T617">nɨrkɨ-mɔːl-lä</ta>
            <ta e="T619" id="Seg_5515" s="T618">qoi-lʼ</ta>
            <ta e="T620" id="Seg_5516" s="T619">jabol</ta>
            <ta e="T621" id="Seg_5517" s="T620">ɛː-sa</ta>
            <ta e="T622" id="Seg_5518" s="T621">Qolʼsak</ta>
            <ta e="T623" id="Seg_5519" s="T622">ira-nɨk</ta>
            <ta e="T624" id="Seg_5520" s="T623">kätɨ-qo</ta>
            <ta e="T625" id="Seg_5521" s="T624">İčʼakäčʼika-p</ta>
            <ta e="T626" id="Seg_5522" s="T625">üt-tɨ</ta>
            <ta e="T627" id="Seg_5523" s="T626">qoptɨ-r-sɨ-mɨt</ta>
            <ta e="T629" id="Seg_5524" s="T628">čʼeːlʼe</ta>
            <ta e="T630" id="Seg_5525" s="T629">okot</ta>
            <ta e="T631" id="Seg_5526" s="T630">täp</ta>
            <ta e="T632" id="Seg_5527" s="T631">nassa</ta>
            <ta e="T633" id="Seg_5528" s="T632">qo-mpa-tɨ</ta>
            <ta e="T634" id="Seg_5529" s="T633">tɔːq</ta>
            <ta e="T635" id="Seg_5530" s="T634">lɨptɨk</ta>
            <ta e="T636" id="Seg_5531" s="T635">kuša-lʼ</ta>
            <ta e="T637" id="Seg_5532" s="T636">tätaqoj</ta>
            <ta e="T638" id="Seg_5533" s="T637">muntɨk</ta>
            <ta e="T639" id="Seg_5534" s="T638">qo-mpa-tɨ</ta>
            <ta e="T640" id="Seg_5535" s="T639">i</ta>
            <ta e="T641" id="Seg_5536" s="T640">sıː-t</ta>
            <ta e="T642" id="Seg_5537" s="T641">qopɨ-lʼ</ta>
            <ta e="T643" id="Seg_5538" s="T642">mɨ-t</ta>
            <ta e="T644" id="Seg_5539" s="T643">Qolʼsak</ta>
            <ta e="T645" id="Seg_5540" s="T644">ira</ta>
            <ta e="T646" id="Seg_5541" s="T645">tɛnɨ-tɨ</ta>
            <ta e="T647" id="Seg_5542" s="T646">ürɨ-sa</ta>
            <ta e="T648" id="Seg_5543" s="T647">qäntɨk</ta>
            <ta e="T649" id="Seg_5544" s="T648">nılʼčʼi-k</ta>
            <ta e="T650" id="Seg_5545" s="T649">ɛsɨ-sa</ta>
            <ta e="T651" id="Seg_5546" s="T650">İčʼekäčʼika</ta>
            <ta e="T652" id="Seg_5547" s="T651">jabəlʼ</ta>
            <ta e="T655" id="Seg_5548" s="T654">šinte-lʼ</ta>
            <ta e="T656" id="Seg_5549" s="T655">čʼeːlʼ</ta>
            <ta e="T657" id="Seg_5550" s="T656">ontɨ</ta>
            <ta e="T658" id="Seg_5551" s="T657">šitä-t</ta>
            <ta e="T659" id="Seg_5552" s="T658">kasa-t</ta>
            <ta e="T660" id="Seg_5553" s="T659">tü-ŋčʼɔː-tɨt</ta>
            <ta e="T661" id="Seg_5554" s="T660">İčʼekäčʼika-ntɨ</ta>
            <ta e="T662" id="Seg_5555" s="T661">üːtɨ-tɨ</ta>
            <ta e="T663" id="Seg_5556" s="T662">kasak</ta>
            <ta e="T664" id="Seg_5557" s="T663">ira</ta>
            <ta e="T665" id="Seg_5558" s="T664">soqɨš-qo</ta>
            <ta e="T666" id="Seg_5559" s="T665">İčʼakäčʼika-p</ta>
            <ta e="T667" id="Seg_5560" s="T666">kuttar</ta>
            <ta e="T668" id="Seg_5561" s="T667">qo-sɨ-tɨ</ta>
            <ta e="T669" id="Seg_5562" s="T668">na</ta>
            <ta e="T670" id="Seg_5563" s="T669">to</ta>
            <ta e="T671" id="Seg_5564" s="T670">lɨptɨ-t-ɨ-p</ta>
            <ta e="T672" id="Seg_5565" s="T671">meː</ta>
            <ta e="T673" id="Seg_5566" s="T672">aj</ta>
            <ta e="T674" id="Seg_5567" s="T673">üt-tɨ</ta>
            <ta e="T675" id="Seg_5568" s="T674">pat-tɨr-tɛntɨ-mɨt</ta>
            <ta e="T676" id="Seg_5569" s="T675">tɔːq-ɨ-tqo</ta>
            <ta e="T677" id="Seg_5570" s="T676">İčʼakäčʼika</ta>
            <ta e="T678" id="Seg_5571" s="T677">kätɨ-sɨ-tɨ</ta>
            <ta e="T679" id="Seg_5572" s="T678">süt-qonɨ-ŋɨlʼɨt</ta>
            <ta e="T680" id="Seg_5573" s="T679">süt-qolʼ-nɔː-tɨt</ta>
            <ta e="T681" id="Seg_5574" s="T680">süt-qɨlʼ-nɔː-lʼit</ta>
            <ta e="T682" id="Seg_5575" s="T681">sɨːrɨ-t</ta>
            <ta e="T683" id="Seg_5576" s="T682">qopɨ</ta>
            <ta e="T684" id="Seg_5577" s="T683">nɔː-nɨ</ta>
            <ta e="T685" id="Seg_5578" s="T684">kota-t-ɨ-p</ta>
            <ta e="T686" id="Seg_5579" s="T685">sün-nelʼɨt</ta>
            <ta e="T687" id="Seg_5580" s="T686">kota-t-ɨ-p</ta>
            <ta e="T688" id="Seg_5581" s="T687">sɨːrɨ-t</ta>
            <ta e="T689" id="Seg_5582" s="T688">qopɨ-lʼ</ta>
            <ta e="T690" id="Seg_5583" s="T689">ɔːlʼčʼi-ŋɨlʼɨt</ta>
            <ta e="T691" id="Seg_5584" s="T690">morʼa-t</ta>
            <ta e="T692" id="Seg_5585" s="T691">qanɨq-tɨ</ta>
            <ta e="T693" id="Seg_5586" s="T692">mat</ta>
            <ta e="T694" id="Seg_5587" s="T693">tɛːsintɨ</ta>
            <ta e="T695" id="Seg_5588" s="T694">süt-ta-p</ta>
            <ta e="T696" id="Seg_5589" s="T695">kota-ntɨ</ta>
            <ta e="T697" id="Seg_5590" s="T696">kä-nta-p</ta>
            <ta e="T698" id="Seg_5591" s="T697">üt-tɨ</ta>
            <ta e="T699" id="Seg_5592" s="T698">pü-sa</ta>
            <ta e="T700" id="Seg_5593" s="T699">marqɨ</ta>
            <ta e="T701" id="Seg_5594" s="T700">pü-sa</ta>
            <ta e="T702" id="Seg_5595" s="T701">tɛː</ta>
            <ta e="T703" id="Seg_5596" s="T702">qo-nte-lʼit</ta>
            <ta e="T704" id="Seg_5597" s="T703">muntɨk</ta>
            <ta e="T705" id="Seg_5598" s="T704">tɔːq</ta>
            <ta e="T706" id="Seg_5599" s="T705">ukoːt</ta>
            <ta e="T707" id="Seg_5600" s="T706">Qolʼsak</ta>
            <ta e="T708" id="Seg_5601" s="T707">ira</ta>
            <ta e="T709" id="Seg_5602" s="T708">načʼalʼnik</ta>
            <ta e="T710" id="Seg_5603" s="T709">kutə</ta>
            <ta e="T711" id="Seg_5604" s="T710">kɨka</ta>
            <ta e="T712" id="Seg_5605" s="T711">qättoː-qonɨ</ta>
            <ta e="T713" id="Seg_5606" s="T712">täp</ta>
            <ta e="T714" id="Seg_5607" s="T713">süt-kolʼ-si-tɨ</ta>
            <ta e="T715" id="Seg_5608" s="T714">ukoːt</ta>
            <ta e="T716" id="Seg_5609" s="T715">Qolʼsak</ta>
            <ta e="T717" id="Seg_5610" s="T716">ira-p</ta>
            <ta e="T718" id="Seg_5611" s="T717">nɨnɨ</ta>
            <ta e="T719" id="Seg_5612" s="T718">mänɨlʼ</ta>
            <ta e="T720" id="Seg_5613" s="T719">qum-iː-m-tɨ</ta>
            <ta e="T721" id="Seg_5614" s="T720">kasa-t-ɨ-p</ta>
            <ta e="T722" id="Seg_5615" s="T721">nɨnɨ</ta>
            <ta e="T723" id="Seg_5616" s="T722">İčʼakäčʼika</ta>
            <ta e="T724" id="Seg_5617" s="T723">ontɨ</ta>
            <ta e="T725" id="Seg_5618" s="T724">kä-q-olap-sɨ-tɨ</ta>
            <ta e="T726" id="Seg_5619" s="T725">süt-ɨ-pɨlʼ</ta>
            <ta e="T727" id="Seg_5620" s="T726">kota-t-ɨ-p</ta>
            <ta e="T728" id="Seg_5621" s="T727">qum-ɨ-t-sä</ta>
            <ta e="T729" id="Seg_5622" s="T728">üt-tɨ</ta>
            <ta e="T730" id="Seg_5623" s="T729">kätɨ-sɨ-tɨ</ta>
            <ta e="T731" id="Seg_5624" s="T730">Qolʼsak</ta>
            <ta e="T732" id="Seg_5625" s="T731">ira-nɨk</ta>
            <ta e="T733" id="Seg_5626" s="T732">peː-tɨ</ta>
            <ta e="T734" id="Seg_5627" s="T733">tɔːq</ta>
            <ta e="T735" id="Seg_5628" s="T734">morʼa-n</ta>
            <ta e="T736" id="Seg_5629" s="T735">ılqɨt</ta>
            <ta e="T737" id="Seg_5630" s="T736">nʼi</ta>
            <ta e="T738" id="Seg_5631" s="T737">kun</ta>
            <ta e="T739" id="Seg_5632" s="T738">asa</ta>
            <ta e="T740" id="Seg_5633" s="T739">qo-nta-l</ta>
            <ta e="T741" id="Seg_5634" s="T740">tɔːq-ɨ-tɨ</ta>
            <ta e="T742" id="Seg_5635" s="T741">i</ta>
            <ta e="T743" id="Seg_5636" s="T742">tovar-ɨ-t</ta>
            <ta e="T744" id="Seg_5637" s="T743">meːltɨ</ta>
            <ta e="T745" id="Seg_5638" s="T744">qu-nna-ntɨ</ta>
            <ta e="T746" id="Seg_5639" s="T745">tɛː</ta>
            <ta e="T747" id="Seg_5640" s="T746">na</ta>
            <ta e="T748" id="Seg_5641" s="T747">masɨp</ta>
            <ta e="T749" id="Seg_5642" s="T748">kä-kkɨ-sɨ-lɨt</ta>
            <ta e="T750" id="Seg_5643" s="T749">üt-tɨ</ta>
            <ta e="T751" id="Seg_5644" s="T750">sɨp</ta>
            <ta e="T752" id="Seg_5645" s="T751">käː-sɨ-lɨt</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_5646" s="T0">Qolʼsaqo</ta>
            <ta e="T2" id="Seg_5647" s="T1">ira</ta>
            <ta e="T3" id="Seg_5648" s="T2">ilɨ-mpɨ</ta>
            <ta e="T4" id="Seg_5649" s="T3">İčʼakɨčʼɨka</ta>
            <ta e="T5" id="Seg_5650" s="T4">imɨlʼa-ntɨ-sä</ta>
            <ta e="T6" id="Seg_5651" s="T5">Qolʼsaqo</ta>
            <ta e="T7" id="Seg_5652" s="T6">ira-lʼ</ta>
            <ta e="T8" id="Seg_5653" s="T7">qəːttɨ</ta>
            <ta e="T9" id="Seg_5654" s="T8">ašša</ta>
            <ta e="T10" id="Seg_5655" s="T9">kuntaːqɨn</ta>
            <ta e="T11" id="Seg_5656" s="T10">ɛː-sɨ</ta>
            <ta e="T12" id="Seg_5657" s="T11">ukkɨr</ta>
            <ta e="T13" id="Seg_5658" s="T12">čʼontɨ-qɨn</ta>
            <ta e="T14" id="Seg_5659" s="T13">Qolʼsaqo</ta>
            <ta e="T15" id="Seg_5660" s="T14">ira-lʼ</ta>
            <ta e="T16" id="Seg_5661" s="T15">sɨːrɨ-tɨ</ta>
            <ta e="T17" id="Seg_5662" s="T16">ürɨ</ta>
            <ta e="T18" id="Seg_5663" s="T17">ürɨ-sɨ</ta>
            <ta e="T20" id="Seg_5664" s="T19">ürɨ-kkɨ</ta>
            <ta e="T21" id="Seg_5665" s="T20">təp</ta>
            <ta e="T22" id="Seg_5666" s="T21">tɛnɨmɨ-tɨ</ta>
            <ta e="T23" id="Seg_5667" s="T22">što</ta>
            <ta e="T24" id="Seg_5668" s="T23">İčʼakɨčʼɨka-tɨ</ta>
            <ta e="T25" id="Seg_5669" s="T24">təːlɨ-sɨ-tɨ</ta>
            <ta e="T26" id="Seg_5670" s="T25">üːtɨ-tɨ</ta>
            <ta e="T27" id="Seg_5671" s="T26">təp-ɨ-nɨŋ</ta>
            <ta e="T28" id="Seg_5672" s="T27">kazak-t-ɨ-m</ta>
            <ta e="T29" id="Seg_5673" s="T28">šittɨ-t-ɨ-m</ta>
            <ta e="T30" id="Seg_5674" s="T29">mantɨ-mpɨ-qo</ta>
            <ta e="T31" id="Seg_5675" s="T30">qaj-ɨ-m</ta>
            <ta e="T32" id="Seg_5676" s="T31">meː-ntɨ-tɨ</ta>
            <ta e="T33" id="Seg_5677" s="T32">İčʼakɨčʼɨka</ta>
            <ta e="T34" id="Seg_5678" s="T33">təp</ta>
            <ta e="T35" id="Seg_5679" s="T34">tɛnɨmɨ-tɨ</ta>
            <ta e="T36" id="Seg_5680" s="T35">tü-ɛntɨ-tɨt</ta>
            <ta e="T37" id="Seg_5681" s="T36">rusʼak-sä</ta>
            <ta e="T38" id="Seg_5682" s="T37">puška-ntɨ-sä</ta>
            <ta e="T39" id="Seg_5683" s="T38">təp-ɨ-m</ta>
            <ta e="T40" id="Seg_5684" s="T39">orqɨl-ɛntɨ-tɨt</ta>
            <ta e="T41" id="Seg_5685" s="T40">omtɨ-ltɨ-ɛntɨ-tɨt</ta>
            <ta e="T42" id="Seg_5686" s="T41">tʼurma-ntɨ</ta>
            <ta e="T43" id="Seg_5687" s="T42">na</ta>
            <ta e="T44" id="Seg_5688" s="T43">vremʼa-qɨn</ta>
            <ta e="T45" id="Seg_5689" s="T44">na</ta>
            <ta e="T46" id="Seg_5690" s="T45">čʼeːlɨ</ta>
            <ta e="T47" id="Seg_5691" s="T46">sɨːrɨ-m</ta>
            <ta e="T48" id="Seg_5692" s="T47">qət-sɨ-tɨ</ta>
            <ta e="T49" id="Seg_5693" s="T48">ketɨ-m-tɨ</ta>
            <ta e="T50" id="Seg_5694" s="T49">kəm-sä</ta>
            <ta e="T51" id="Seg_5695" s="T50">qamtɨ-sɨ-tɨ</ta>
            <ta e="T52" id="Seg_5696" s="T51">wərɨ-sɨ-tɨ</ta>
            <ta e="T53" id="Seg_5697" s="T52">wərqɨ</ta>
            <ta e="T54" id="Seg_5698" s="T53">paŋɨ-m</ta>
            <ta e="T55" id="Seg_5699" s="T54">mɔːt-ɨ</ta>
            <ta e="T56" id="Seg_5700" s="T55">pɔːrɔː-qɨn</ta>
            <ta e="T57" id="Seg_5701" s="T56">imɨlʼa-m-tɨ</ta>
            <ta e="T58" id="Seg_5702" s="T57">tamtɨl-sɨ-tɨ</ta>
            <ta e="T59" id="Seg_5703" s="T58">kəm-ɨ-lʼ</ta>
            <ta e="T60" id="Seg_5704" s="T59">sɨːrɨ-n</ta>
            <ta e="T61" id="Seg_5705" s="T60">ketɨ-sä</ta>
            <ta e="T62" id="Seg_5706" s="T61">imɨlʼa-tɨ</ta>
            <ta e="T63" id="Seg_5707" s="T62">*tokk-altɨ-sɨ-tɨ</ta>
            <ta e="T64" id="Seg_5708" s="T63">porqɨ-t</ta>
            <ta e="T65" id="Seg_5709" s="T64">kazak-t</ta>
            <ta e="T66" id="Seg_5710" s="T65">kazak-ɨ-t</ta>
            <ta e="T67" id="Seg_5711" s="T66">tü-sɨ-tɨt</ta>
            <ta e="T68" id="Seg_5712" s="T67">İčʼakɨčʼɨka-nɨŋ</ta>
            <ta e="T69" id="Seg_5713" s="T68">wərqɨ</ta>
            <ta e="T70" id="Seg_5714" s="T69">qum</ta>
            <ta e="T71" id="Seg_5715" s="T70">kətɨ-sɨ-tɨ</ta>
            <ta e="T72" id="Seg_5716" s="T71">İčʼakɨčʼɨka-nɨŋ</ta>
            <ta e="T73" id="Seg_5717" s="T72">tašıntɨ</ta>
            <ta e="T74" id="Seg_5718" s="T73">omtɨ-ltɨ-qo</ta>
            <ta e="T75" id="Seg_5719" s="T74">nɔːtna</ta>
            <ta e="T76" id="Seg_5720" s="T75">tʼurma-ntɨ</ta>
            <ta e="T77" id="Seg_5721" s="T76">tan</ta>
            <ta e="T78" id="Seg_5722" s="T77">sɨːrɨ-t-ɨ-m</ta>
            <ta e="T79" id="Seg_5723" s="T78">qət-tɨ-l</ta>
            <ta e="T80" id="Seg_5724" s="T79">am-ŋɨ-l</ta>
            <ta e="T81" id="Seg_5725" s="T80">təp</ta>
            <ta e="T82" id="Seg_5726" s="T81">nʼenʼnʼɨ-mɔːt-sɨ</ta>
            <ta e="T83" id="Seg_5727" s="T82">imɨlʼa-ntɨ-nkinı</ta>
            <ta e="T84" id="Seg_5728" s="T83">kətɨ-sɨ-tɨ</ta>
            <ta e="T85" id="Seg_5729" s="T84">mašım</ta>
            <ta e="T86" id="Seg_5730" s="T85">omtɨ-altɨ-ɛntɨ-tɨt</ta>
            <ta e="T87" id="Seg_5731" s="T86">tʼurma-ntɨ</ta>
            <ta e="T88" id="Seg_5732" s="T87">čʼajnik</ta>
            <ta e="T89" id="Seg_5733" s="T88">mušɨ-rɨ-ätɨ</ta>
            <ta e="T90" id="Seg_5734" s="T89">*kənpɨ-lä</ta>
            <ta e="T91" id="Seg_5735" s="T90">imɨlʼa-tɨ</ta>
            <ta e="T92" id="Seg_5736" s="T91">toːtɨ-alʼ-ŋɨ</ta>
            <ta e="T93" id="Seg_5737" s="T92">topɨ-mɨ</ta>
            <ta e="T94" id="Seg_5738" s="T93">utɨ-mɨ</ta>
            <ta e="T95" id="Seg_5739" s="T94">čʼüšɨ</ta>
            <ta e="T96" id="Seg_5740" s="T95">kazak-t</ta>
            <ta e="T97" id="Seg_5741" s="T96">nɨŋ-ŋɨ-tɨt</ta>
            <ta e="T98" id="Seg_5742" s="T97">mɔːta-n</ta>
            <ta e="T99" id="Seg_5743" s="T98">ɔːŋ-qɨn</ta>
            <ta e="T100" id="Seg_5744" s="T99">İčʼakɨčʼɨka</ta>
            <ta e="T101" id="Seg_5745" s="T100">imɨlʼa-ntɨ-nkinı</ta>
            <ta e="T102" id="Seg_5746" s="T101">tan</ta>
            <ta e="T103" id="Seg_5747" s="T102">lʼentʼaj-ŋɨ-ntɨ</ta>
            <ta e="T104" id="Seg_5748" s="T103">čʼajnik</ta>
            <ta e="T105" id="Seg_5749" s="T104">ašša</ta>
            <ta e="T106" id="Seg_5750" s="T105">kɨkɨ-ntɨ</ta>
            <ta e="T107" id="Seg_5751" s="T106">mušɨ-rɨ-qo</ta>
            <ta e="T108" id="Seg_5752" s="T107">nʼenʼnʼɨ-mɔːt-sɨ</ta>
            <ta e="T109" id="Seg_5753" s="T108">İčʼakɨčʼɨka</ta>
            <ta e="T110" id="Seg_5754" s="T109">paŋɨ-m-tɨ</ta>
            <ta e="T111" id="Seg_5755" s="T110">ıllä</ta>
            <ta e="T112" id="Seg_5756" s="T111">iː-sɨ-tɨ</ta>
            <ta e="T113" id="Seg_5757" s="T112">imɨlʼa-m-tɨ</ta>
            <ta e="T114" id="Seg_5758" s="T113">mattɨ-r-sɨ-tɨ</ta>
            <ta e="T115" id="Seg_5759" s="T114">imɨlʼa-tɨ</ta>
            <ta e="T116" id="Seg_5760" s="T115">qu-lʼčʼɨ-sɨ</ta>
            <ta e="T117" id="Seg_5761" s="T116">nɨːnɨ</ta>
            <ta e="T118" id="Seg_5762" s="T117">İčʼakɨčʼɨka</ta>
            <ta e="T119" id="Seg_5763" s="T118">kətɨ-sɨ-tɨ</ta>
            <ta e="T120" id="Seg_5764" s="T119">kazak-t-nkinı</ta>
            <ta e="T121" id="Seg_5765" s="T120">imɨlʼa-mɨ</ta>
            <ta e="T122" id="Seg_5766" s="T121">meːltɨ</ta>
            <ta e="T123" id="Seg_5767" s="T122">nılʼčʼɨ-k</ta>
            <ta e="T124" id="Seg_5768" s="T123">orɨ-š-ŋɨ</ta>
            <ta e="T125" id="Seg_5769" s="T124">paŋɨ-tɨ</ta>
            <ta e="T126" id="Seg_5770" s="T125">topɨ-qɨn-ntɨ</ta>
            <ta e="T127" id="Seg_5771" s="T126">loːq-rɨ-sɨ-tɨ</ta>
            <ta e="T128" id="Seg_5772" s="T127">imɨlʼa</ta>
            <ta e="T129" id="Seg_5773" s="T128">ınnä</ta>
            <ta e="T130" id="Seg_5774" s="T129">paktɨ-äšɨk</ta>
            <ta e="T131" id="Seg_5775" s="T130">paŋɨ-mɨ</ta>
            <ta e="T132" id="Seg_5776" s="T131">ilɨ-äptɨ-ɛntɨ</ta>
            <ta e="T133" id="Seg_5777" s="T132">nɨːnɨ</ta>
            <ta e="T134" id="Seg_5778" s="T133">imɨlʼa-tɨ</ta>
            <ta e="T135" id="Seg_5779" s="T134">*nɨl-ɛː-sɨ</ta>
            <ta e="T136" id="Seg_5780" s="T135">*kənpɨ-lä</ta>
            <ta e="T137" id="Seg_5781" s="T136">orqɨl-sɨ-tɨ</ta>
            <ta e="T138" id="Seg_5782" s="T137">čʼajnik-tɨ</ta>
            <ta e="T139" id="Seg_5783" s="T138">mušɨ-rɨ-qo</ta>
            <ta e="T140" id="Seg_5784" s="T139">kɔːnsan-m</ta>
            <ta e="T141" id="Seg_5785" s="T140">näkä-qɨl-ptäː-n</ta>
            <ta e="T142" id="Seg_5786" s="T141">kuntɨ</ta>
            <ta e="T143" id="Seg_5787" s="T142">čʼajnik</ta>
            <ta e="T144" id="Seg_5788" s="T143">mušɨ-ɛː-sɨ</ta>
            <ta e="T145" id="Seg_5789" s="T144">nʼanʼ-ɨ-m</ta>
            <ta e="T146" id="Seg_5790" s="T145">apsɨ-m</ta>
            <ta e="T147" id="Seg_5791" s="T146">tottɨ-sɨ-tɨ</ta>
            <ta e="T148" id="Seg_5792" s="T147">lem-ntɨ</ta>
            <ta e="T149" id="Seg_5793" s="T148">*kənpɨ-lä</ta>
            <ta e="T150" id="Seg_5794" s="T149">kazak-t</ta>
            <ta e="T151" id="Seg_5795" s="T150">am-ɨ-r-qo-olam-sɨ-tɨt</ta>
            <ta e="T152" id="Seg_5796" s="T151">təp-ɨ-t</ta>
            <ta e="T153" id="Seg_5797" s="T152">nılʼčʼɨ-k</ta>
            <ta e="T154" id="Seg_5798" s="T153">kətɨ-sɨ-tɨt</ta>
            <ta e="T155" id="Seg_5799" s="T154">meː</ta>
            <ta e="T156" id="Seg_5800" s="T155">tan</ta>
            <ta e="T157" id="Seg_5801" s="T156">paŋɨ-m</ta>
            <ta e="T158" id="Seg_5802" s="T157">təmɨ-ɛntɨ-mɨt</ta>
            <ta e="T159" id="Seg_5803" s="T158">meː</ta>
            <ta e="T160" id="Seg_5804" s="T159">ima-iː-mɨt</ta>
            <ta e="T161" id="Seg_5805" s="T160">lʼentʼaj-tɨ-tɨt</ta>
            <ta e="T162" id="Seg_5806" s="T161">i</ta>
            <ta e="T163" id="Seg_5807" s="T162">qontɨ-tɨt</ta>
            <ta e="T164" id="Seg_5808" s="T163">kazak-t</ta>
            <ta e="T165" id="Seg_5809" s="T164">ašša</ta>
            <ta e="T166" id="Seg_5810" s="T165">laqɨ-rɨ-qo-olam-sɨ-tɨt</ta>
            <ta e="T167" id="Seg_5811" s="T166">İčʼakɨčʼɨka-m</ta>
            <ta e="T168" id="Seg_5812" s="T167">kətɨ-ɛntɨ-tɨt</ta>
            <ta e="T169" id="Seg_5813" s="T168">kətɨ-sɨ-tɨt</ta>
            <ta e="T170" id="Seg_5814" s="T169">Qolʼsaqo</ta>
            <ta e="T171" id="Seg_5815" s="T170">ira-nɨŋ</ta>
            <ta e="T172" id="Seg_5816" s="T171">qət-sɨ-tɨt</ta>
            <ta e="T173" id="Seg_5817" s="T172">na</ta>
            <ta e="T174" id="Seg_5818" s="T173">kətɨ-mpɨ-tɨt</ta>
            <ta e="T175" id="Seg_5819" s="T174">načʼalʼnik</ta>
            <ta e="T176" id="Seg_5820" s="T175">*kurɨ-altɨ-sɨ-tɨ</ta>
            <ta e="T177" id="Seg_5821" s="T176">təmɨ-qo</ta>
            <ta e="T178" id="Seg_5822" s="T177">paŋɨ-m</ta>
            <ta e="T179" id="Seg_5823" s="T178">sintälɨlʼ</ta>
            <ta e="T180" id="Seg_5824" s="T179">čʼeːlɨ</ta>
            <ta e="T181" id="Seg_5825" s="T180">təmɨ-tɨt</ta>
            <ta e="T182" id="Seg_5826" s="T181">Qolʼsaqo</ta>
            <ta e="T183" id="Seg_5827" s="T182">ira</ta>
            <ta e="T184" id="Seg_5828" s="T183">kətɨ-sɨ-tɨ</ta>
            <ta e="T185" id="Seg_5829" s="T184">paŋɨ-m</ta>
            <ta e="T186" id="Seg_5830" s="T185">mäkkä</ta>
            <ta e="T187" id="Seg_5831" s="T186">mi-ŋɨlɨt</ta>
            <ta e="T188" id="Seg_5832" s="T187">man</ta>
            <ta e="T189" id="Seg_5833" s="T188">šäqqɨ-ltɨ-ɛntɨ-m</ta>
            <ta e="T190" id="Seg_5834" s="T189">man</ta>
            <ta e="T191" id="Seg_5835" s="T190">ima-mɨ</ta>
            <ta e="T192" id="Seg_5836" s="T191">i</ta>
            <ta e="T193" id="Seg_5837" s="T192">rapotnik-ɨ-t</ta>
            <ta e="T194" id="Seg_5838" s="T193">lʼentʼaj-tɨ-tɨt</ta>
            <ta e="T195" id="Seg_5839" s="T194">čʼeːlɨ-t</ta>
            <ta e="T196" id="Seg_5840" s="T195">i</ta>
            <ta e="T197" id="Seg_5841" s="T196">pi-t</ta>
            <ta e="T198" id="Seg_5842" s="T197">qontɨ-tɨt</ta>
            <ta e="T199" id="Seg_5843" s="T198">İčʼakɨčʼɨka</ta>
            <ta e="T200" id="Seg_5844" s="T199">kətɨ-mpɨ-tɨ</ta>
            <ta e="T201" id="Seg_5845" s="T200">kətɨ-sɨ-tɨ</ta>
            <ta e="T202" id="Seg_5846" s="T201">paŋɨ-m</ta>
            <ta e="T203" id="Seg_5847" s="T202">čʼista-k</ta>
            <ta e="T204" id="Seg_5848" s="T203">wərɨ-ŋɨlɨt</ta>
            <ta e="T205" id="Seg_5849" s="T204">soma</ta>
            <ta e="T206" id="Seg_5850" s="T205">qum-ɨ-m</ta>
            <ta e="T207" id="Seg_5851" s="T206">mattɨ-r-qo</ta>
            <ta e="T208" id="Seg_5852" s="T207">Qolʼsaqo</ta>
            <ta e="T209" id="Seg_5853" s="T208">ira</ta>
            <ta e="T210" id="Seg_5854" s="T209">qontɨ-sɨ</ta>
            <ta e="T211" id="Seg_5855" s="T210">qarɨ-n</ta>
            <ta e="T212" id="Seg_5856" s="T211">čʼəŋ</ta>
            <ta e="T213" id="Seg_5857" s="T212">omtɨ-sɨ</ta>
            <ta e="T214" id="Seg_5858" s="T213">wəšɨ-sɨ</ta>
            <ta e="T215" id="Seg_5859" s="T214">ima</ta>
            <ta e="T216" id="Seg_5860" s="T215">omtɨ-äšɨk</ta>
            <ta e="T217" id="Seg_5861" s="T216">i</ta>
            <ta e="T218" id="Seg_5862" s="T217">rapotnik-ɨ-t</ta>
            <ta e="T219" id="Seg_5863" s="T218">omtɨ-ŋɨlɨt</ta>
            <ta e="T220" id="Seg_5864" s="T219">təp-ɨ-t</ta>
            <ta e="T221" id="Seg_5865" s="T220">čʼəŋ</ta>
            <ta e="T222" id="Seg_5866" s="T221">ašša</ta>
            <ta e="T223" id="Seg_5867" s="T222">omtɨ-ntɨ-tɨt</ta>
            <ta e="T224" id="Seg_5868" s="T223">nʼenʼnʼɨ-mɔːt-sɨ</ta>
            <ta e="T225" id="Seg_5869" s="T224">ıllä</ta>
            <ta e="T226" id="Seg_5870" s="T225">iː-sɨ-tɨ</ta>
            <ta e="T227" id="Seg_5871" s="T226">paŋɨ-m-tɨ</ta>
            <ta e="T228" id="Seg_5872" s="T227">mattɨ-äl-sɨ-tɨ</ta>
            <ta e="T229" id="Seg_5873" s="T228">muntɨk</ta>
            <ta e="T230" id="Seg_5874" s="T229">paŋɨ-m</ta>
            <ta e="T231" id="Seg_5875" s="T230">loːq-rɨ-sɨ-tɨ</ta>
            <ta e="T232" id="Seg_5876" s="T231">topɨ-qɨn-ntɨ</ta>
            <ta e="T233" id="Seg_5877" s="T232">muntɨk</ta>
            <ta e="T234" id="Seg_5878" s="T233">mattɨ-r-mpɨlʼ</ta>
            <ta e="T235" id="Seg_5879" s="T234">qum-iː-qɨn-ntɨ</ta>
            <ta e="T236" id="Seg_5880" s="T235">čʼam</ta>
            <ta e="T237" id="Seg_5881" s="T236">kətɨ-kkɨ-tɨ</ta>
            <ta e="T238" id="Seg_5882" s="T237">meːltɨ</ta>
            <ta e="T239" id="Seg_5883" s="T238">qu-lʼčʼɨ-tɨt</ta>
            <ta e="T240" id="Seg_5884" s="T239">ira</ta>
            <ta e="T241" id="Seg_5885" s="T240">nʼenʼnʼɨ-mɔːt-sɨ</ta>
            <ta e="T242" id="Seg_5886" s="T241">kazak-t-ɨ-m</ta>
            <ta e="T243" id="Seg_5887" s="T242">qərɨ-sɨ-tɨ</ta>
            <ta e="T244" id="Seg_5888" s="T243">kun</ta>
            <ta e="T245" id="Seg_5889" s="T244">iː-sɨ-qɨt</ta>
            <ta e="T246" id="Seg_5890" s="T245">paŋɨ-m</ta>
            <ta e="T247" id="Seg_5891" s="T246">təp</ta>
            <ta e="T248" id="Seg_5892" s="T247">İčʼakɨčʼɨka</ta>
            <ta e="T249" id="Seg_5893" s="T248">meː-nkinı</ta>
            <ta e="T250" id="Seg_5894" s="T249">paŋɨ-m</ta>
            <ta e="T251" id="Seg_5895" s="T250">mi-sɨ-tɨ</ta>
            <ta e="T252" id="Seg_5896" s="T251">təp</ta>
            <ta e="T253" id="Seg_5897" s="T252">İčʼakɨčʼɨka</ta>
            <ta e="T254" id="Seg_5898" s="T253">ɔːlɨk-sɨ</ta>
            <ta e="T255" id="Seg_5899" s="T254">qən-lä</ta>
            <ta e="T256" id="Seg_5900" s="T255">orqɨl-qo</ta>
            <ta e="T257" id="Seg_5901" s="T256">sudʼin-qo</ta>
            <ta e="T258" id="Seg_5902" s="T257">ima-iː-mɨt</ta>
            <ta e="T259" id="Seg_5903" s="T258">ima-mɨ</ta>
            <ta e="T260" id="Seg_5904" s="T259">ima-t</ta>
            <ta e="T261" id="Seg_5905" s="T260">muntɨk</ta>
            <ta e="T262" id="Seg_5906" s="T261">qu-sɨ-tɨt</ta>
            <ta e="T263" id="Seg_5907" s="T262">paŋɨ-n</ta>
            <ta e="T264" id="Seg_5908" s="T263">*nɔː-nɨ</ta>
            <ta e="T265" id="Seg_5909" s="T264">tan</ta>
            <ta e="T266" id="Seg_5910" s="T265">onnäka</ta>
            <ta e="T267" id="Seg_5911" s="T266">ašša</ta>
            <ta e="T268" id="Seg_5912" s="T267">tɔːt-ɨ-k</ta>
            <ta e="T269" id="Seg_5913" s="T268">mattɨ-r-sɨ</ta>
            <ta e="T270" id="Seg_5914" s="T269">təp-ɨ-m</ta>
            <ta e="T271" id="Seg_5915" s="T270">qəːčʼɨ-qo</ta>
            <ta e="T272" id="Seg_5916" s="T271">ukkɨr</ta>
            <ta e="T273" id="Seg_5917" s="T272">čʼeːlɨ</ta>
            <ta e="T274" id="Seg_5918" s="T273">meː</ta>
            <ta e="T275" id="Seg_5919" s="T274">aj</ta>
            <ta e="T276" id="Seg_5920" s="T275">saŋa-r-ɛntɨ-mɨt</ta>
            <ta e="T277" id="Seg_5921" s="T276">meː</ta>
            <ta e="T278" id="Seg_5922" s="T277">ima-iː-mɨt</ta>
            <ta e="T279" id="Seg_5923" s="T278">aj</ta>
            <ta e="T280" id="Seg_5924" s="T279">lʼentʼaj-tɨ-tɨt</ta>
            <ta e="T281" id="Seg_5925" s="T280">Qolʼsaqo</ta>
            <ta e="T282" id="Seg_5926" s="T281">ira</ta>
            <ta e="T283" id="Seg_5927" s="T282">aj</ta>
            <ta e="T284" id="Seg_5928" s="T283">kətɨ-sɨ-tɨ</ta>
            <ta e="T285" id="Seg_5929" s="T284">latno</ta>
            <ta e="T286" id="Seg_5930" s="T285">saŋa-r-ŋɨlɨt</ta>
            <ta e="T287" id="Seg_5931" s="T286">paŋɨ-m</ta>
            <ta e="T288" id="Seg_5932" s="T287">kazak-t</ta>
            <ta e="T289" id="Seg_5933" s="T288">šittɨ-tɨt</ta>
            <ta e="T290" id="Seg_5934" s="T289">nılʼčʼɨ-k</ta>
            <ta e="T291" id="Seg_5935" s="T290">mattɨ-äl-sɨ-tɨ</ta>
            <ta e="T292" id="Seg_5936" s="T291">ontɨ</ta>
            <ta e="T293" id="Seg_5937" s="T292">ima-iː-tɨt</ta>
            <ta e="T294" id="Seg_5938" s="T293">ima-iː-tɨt</ta>
            <ta e="T295" id="Seg_5939" s="T294">qu-sɨ-tɨt</ta>
            <ta e="T296" id="Seg_5940" s="T295">kəm-ɨ-n</ta>
            <ta e="T297" id="Seg_5941" s="T296">čʼɔːr-ɨ-k</ta>
            <ta e="T298" id="Seg_5942" s="T297">qarɨ-ntɨ</ta>
            <ta e="T299" id="Seg_5943" s="T298">kətɨ-sɨ-tɨt</ta>
            <ta e="T300" id="Seg_5944" s="T299">Qolʼsaqo</ta>
            <ta e="T301" id="Seg_5945" s="T300">ira-nɨŋ</ta>
            <ta e="T302" id="Seg_5946" s="T301">meː</ta>
            <ta e="T303" id="Seg_5947" s="T302">ima-iː-mɨt</ta>
            <ta e="T304" id="Seg_5948" s="T303">meːltɨ</ta>
            <ta e="T305" id="Seg_5949" s="T304">qu-sɨ-tɨt</ta>
            <ta e="T306" id="Seg_5950" s="T305">ira</ta>
            <ta e="T307" id="Seg_5951" s="T306">nʼenʼnʼɨ-mɔːt-sɨ</ta>
            <ta e="T308" id="Seg_5952" s="T307">İčʼakɨčʼɨka-m</ta>
            <ta e="T309" id="Seg_5953" s="T308">orqɨl-qo</ta>
            <ta e="T310" id="Seg_5954" s="T309">nɔːtna</ta>
            <ta e="T311" id="Seg_5955" s="T310">kazak-t</ta>
            <ta e="T312" id="Seg_5956" s="T311">qən-sɨ-tɨt</ta>
            <ta e="T313" id="Seg_5957" s="T312">İčʼakɨčʼɨka</ta>
            <ta e="T314" id="Seg_5958" s="T313">na</ta>
            <ta e="T315" id="Seg_5959" s="T314">vremʼa-qɨn</ta>
            <ta e="T316" id="Seg_5960" s="T315">mɔːt-qɨn-ntɨ</ta>
            <ta e="T317" id="Seg_5961" s="T316">İčʼakɨčʼɨka-m</ta>
            <ta e="T318" id="Seg_5962" s="T317">orqɨl-sɨ-tɨt</ta>
            <ta e="T319" id="Seg_5963" s="T318">seːpɨŋ</ta>
            <ta e="T320" id="Seg_5964" s="T319">moːlmɨ-ttɨ-sɨ-ntɨ</ta>
            <ta e="T321" id="Seg_5965" s="T320">meː</ta>
            <ta e="T322" id="Seg_5966" s="T321">muntɨk</ta>
            <ta e="T323" id="Seg_5967" s="T322">ima-iː-mɨt</ta>
            <ta e="T324" id="Seg_5968" s="T323">i</ta>
            <ta e="T325" id="Seg_5969" s="T324">rapotnik-ɨ-t</ta>
            <ta e="T326" id="Seg_5970" s="T325">mattɨ-äl-sɨ-mɨt</ta>
            <ta e="T327" id="Seg_5971" s="T326">təp-ɨ-t</ta>
            <ta e="T328" id="Seg_5972" s="T327">qu-sɨ-tɨt</ta>
            <ta e="T329" id="Seg_5973" s="T328">kəm-ɨ-n</ta>
            <ta e="T330" id="Seg_5974" s="T329">čʼɔːr-ɨ-k</ta>
            <ta e="T331" id="Seg_5975" s="T330">İčʼakɨčʼɨka-m</ta>
            <ta e="T332" id="Seg_5976" s="T331">qən-tɨ-sɨ-tɨt</ta>
            <ta e="T333" id="Seg_5977" s="T332">tʼurma-ntɨ</ta>
            <ta e="T334" id="Seg_5978" s="T333">Qolʼsaqo</ta>
            <ta e="T335" id="Seg_5979" s="T334">ira</ta>
            <ta e="T336" id="Seg_5980" s="T335">*kurɨ-altɨ-sɨ-tɨ</ta>
            <ta e="T337" id="Seg_5981" s="T336">ıllä</ta>
            <ta e="T338" id="Seg_5982" s="T337">qoptɨ-rɨ-qo</ta>
            <ta e="T339" id="Seg_5983" s="T338">üt-ntɨ</ta>
            <ta e="T340" id="Seg_5984" s="T339">kətɨ-sɨ-tɨ</ta>
            <ta e="T341" id="Seg_5985" s="T340">sɨːrɨ-n</ta>
            <ta e="T342" id="Seg_5986" s="T341">qopɨ-ntɨ</ta>
            <ta e="T343" id="Seg_5987" s="T342">šünʼčʼɨ-ntɨ</ta>
            <ta e="T344" id="Seg_5988" s="T343">šüt-qo</ta>
            <ta e="T345" id="Seg_5989" s="T344">ilɨ-lä</ta>
            <ta e="T346" id="Seg_5990" s="T345">kazak-t</ta>
            <ta e="T347" id="Seg_5991" s="T346">təp-ɨ-m</ta>
            <ta e="T348" id="Seg_5992" s="T347">qən-tɨ-sɨ-tɨt</ta>
            <ta e="T349" id="Seg_5993" s="T348">čʼuntɨ-lʼ</ta>
            <ta e="T350" id="Seg_5994" s="T349">qaqlɨ-sä</ta>
            <ta e="T351" id="Seg_5995" s="T350">üt</ta>
            <ta e="T352" id="Seg_5996" s="T351">toːp-ntɨ</ta>
            <ta e="T353" id="Seg_5997" s="T352">morä-n</ta>
            <ta e="T354" id="Seg_5998" s="T353">qanɨŋ-ntɨ</ta>
            <ta e="T355" id="Seg_5999" s="T354">təp</ta>
            <ta e="T356" id="Seg_6000" s="T355">kota</ta>
            <ta e="T357" id="Seg_6001" s="T356">čʼontɨ-qɨn</ta>
            <ta e="T358" id="Seg_6002" s="T357">ilɨ-lä</ta>
            <ta e="T359" id="Seg_6003" s="T358">ippɨ</ta>
            <ta e="T360" id="Seg_6004" s="T359">kətɨ-sɨ-tɨ</ta>
            <ta e="T361" id="Seg_6005" s="T360">mašım</ta>
            <ta e="T362" id="Seg_6006" s="T361">qoptɨ-rɨ-ŋɨlɨt</ta>
            <ta e="T363" id="Seg_6007" s="T362">wərqɨ</ta>
            <ta e="T364" id="Seg_6008" s="T363">pü-sä</ta>
            <ta e="T365" id="Seg_6009" s="T364">wərqɨ</ta>
            <ta e="T366" id="Seg_6010" s="T365">pü</ta>
            <ta e="T367" id="Seg_6011" s="T366">peː-qɨl-ŋɨlɨt</ta>
            <ta e="T368" id="Seg_6012" s="T367">təp-ɨ-t</ta>
            <ta e="T369" id="Seg_6013" s="T368">qən-sɨ-tɨt</ta>
            <ta e="T370" id="Seg_6014" s="T369">peː-qo</ta>
            <ta e="T371" id="Seg_6015" s="T370">wərqɨ</ta>
            <ta e="T372" id="Seg_6016" s="T371">pü-m</ta>
            <ta e="T373" id="Seg_6017" s="T372">kuntɨ</ta>
            <ta e="T374" id="Seg_6018" s="T373">lʼi</ta>
            <ta e="T375" id="Seg_6019" s="T374">qɔːmɨčʼä</ta>
            <ta e="T376" id="Seg_6020" s="T375">ippɨ-mpɨ</ta>
            <ta e="T377" id="Seg_6021" s="T376">üŋkɨl-tɨ-mpɨ-tɨ</ta>
            <ta e="T378" id="Seg_6022" s="T377">čʼuntɨ-lʼ</ta>
            <ta e="T379" id="Seg_6023" s="T378">qaqlɨ</ta>
            <ta e="T380" id="Seg_6024" s="T379">tü-ntɨ</ta>
            <ta e="T381" id="Seg_6025" s="T380">tɛːttɨ</ta>
            <ta e="T382" id="Seg_6026" s="T381">lʼi</ta>
            <ta e="T383" id="Seg_6027" s="T382">sompɨla</ta>
            <ta e="T384" id="Seg_6028" s="T383">qaqlɨ-t</ta>
            <ta e="T385" id="Seg_6029" s="T384">tü-ntɨ</ta>
            <ta e="T386" id="Seg_6030" s="T385">utɨ-rɨ-sɨ-tɨt</ta>
            <ta e="T387" id="Seg_6031" s="T386">təmqup</ta>
            <ta e="T388" id="Seg_6032" s="T387">tɔːq-sä</ta>
            <ta e="T389" id="Seg_6033" s="T388">tawar-sä</ta>
            <ta e="T390" id="Seg_6034" s="T389">lɨptɨŋ-sä</ta>
            <ta e="T391" id="Seg_6035" s="T390">qən-ntɨ</ta>
            <ta e="T392" id="Seg_6036" s="T391">moqɨnä</ta>
            <ta e="T393" id="Seg_6037" s="T392">təmqup</ta>
            <ta e="T394" id="Seg_6038" s="T393">*tapɨ-ɔːl-sɨ-tɨ</ta>
            <ta e="T395" id="Seg_6039" s="T394">sɨːrɨ-n</ta>
            <ta e="T396" id="Seg_6040" s="T395">qopɨ-lʼ</ta>
            <ta e="T397" id="Seg_6041" s="T396">kota-m</ta>
            <ta e="T398" id="Seg_6042" s="T397">qaj</ta>
            <ta e="T399" id="Seg_6043" s="T398">tɨmtɨ</ta>
            <ta e="T400" id="Seg_6044" s="T399">ippɨ</ta>
            <ta e="T401" id="Seg_6045" s="T400">İčʼakɨčʼɨka</ta>
            <ta e="T402" id="Seg_6046" s="T401">laŋkɨ-alʼ-sɨ</ta>
            <ta e="T403" id="Seg_6047" s="T402">ijoː</ta>
            <ta e="T404" id="Seg_6048" s="T403">təmqup</ta>
            <ta e="T405" id="Seg_6049" s="T404">qaj-tqo</ta>
            <ta e="T406" id="Seg_6050" s="T405">ippɨ-ntɨ</ta>
            <ta e="T407" id="Seg_6051" s="T406">İčʼakɨčʼɨka</ta>
            <ta e="T408" id="Seg_6052" s="T407">kətɨ-sɨ-tɨ</ta>
            <ta e="T409" id="Seg_6053" s="T408">ınnä</ta>
            <ta e="T410" id="Seg_6054" s="T409">mašım</ta>
            <ta e="T411" id="Seg_6055" s="T410">üːtɨ-äšɨk</ta>
            <ta e="T412" id="Seg_6056" s="T411">toː</ta>
            <ta e="T413" id="Seg_6057" s="T412">maːtɨ-ätɔːl-ätɨ</ta>
            <ta e="T414" id="Seg_6058" s="T413">təp</ta>
            <ta e="T415" id="Seg_6059" s="T414">toː</ta>
            <ta e="T416" id="Seg_6060" s="T415">maːtɨ-alʼ-olʼ-sɨ-tɨ</ta>
            <ta e="T417" id="Seg_6061" s="T416">təp</ta>
            <ta e="T418" id="Seg_6062" s="T417">ınnä</ta>
            <ta e="T419" id="Seg_6063" s="T418">*putɨl-mɔːt-sɨ</ta>
            <ta e="T420" id="Seg_6064" s="T419">tantɨ-sɨ</ta>
            <ta e="T421" id="Seg_6065" s="T420">təp-ɨ-n</ta>
            <ta e="T422" id="Seg_6066" s="T421">mɨ-qɨn</ta>
            <ta e="T423" id="Seg_6067" s="T422">sıː-n</ta>
            <ta e="T424" id="Seg_6068" s="T423">qopɨ-lʼ</ta>
            <ta e="T425" id="Seg_6069" s="T424">mɨ</ta>
            <ta e="T426" id="Seg_6070" s="T425">ɛː-mpɨ</ta>
            <ta e="T427" id="Seg_6071" s="T426">muŋkɨ-n-ntɨ</ta>
            <ta e="T428" id="Seg_6072" s="T427">təmqup</ta>
            <ta e="T429" id="Seg_6073" s="T428">soqɨš-ŋɨ-tɨ</ta>
            <ta e="T430" id="Seg_6074" s="T429">qaj-tqo</ta>
            <ta e="T431" id="Seg_6075" s="T430">ippɨ-ntɨ</ta>
            <ta e="T432" id="Seg_6076" s="T431">man</ta>
            <ta e="T433" id="Seg_6077" s="T432">ippɨ-m</ta>
            <ta e="T434" id="Seg_6078" s="T433">üt-ntɨ</ta>
            <ta e="T435" id="Seg_6079" s="T434">pat-ntɨ-r-sɨ-k</ta>
            <ta e="T436" id="Seg_6080" s="T435">sıː-n</ta>
            <ta e="T437" id="Seg_6081" s="T436">qopɨ-m</ta>
            <ta e="T438" id="Seg_6082" s="T437">iː-sɨ-m</ta>
            <ta e="T439" id="Seg_6083" s="T438">orɨ-mɨ</ta>
            <ta e="T440" id="Seg_6084" s="T439">čʼäːŋkɨ-sɨ</ta>
            <ta e="T441" id="Seg_6085" s="T440">tɔːq</ta>
            <ta e="T442" id="Seg_6086" s="T441">lɨptɨŋ</ta>
            <ta e="T443" id="Seg_6087" s="T442">kočʼčʼɨ</ta>
            <ta e="T444" id="Seg_6088" s="T443">üt-qɨn</ta>
            <ta e="T445" id="Seg_6089" s="T444">ınnä</ta>
            <ta e="T446" id="Seg_6090" s="T445">atɨ-lɨ-sɨ-tɨ</ta>
            <ta e="T447" id="Seg_6091" s="T446">sıː-n</ta>
            <ta e="T448" id="Seg_6092" s="T447">qopɨ-t-ɨ-m</ta>
            <ta e="T449" id="Seg_6093" s="T448">təmqup-nɨŋ</ta>
            <ta e="T450" id="Seg_6094" s="T449">təmqup</ta>
            <ta e="T451" id="Seg_6095" s="T450">ɔːntɨ-al-sɨ</ta>
            <ta e="T452" id="Seg_6096" s="T451">man</ta>
            <ta e="T453" id="Seg_6097" s="T452">aj</ta>
            <ta e="T454" id="Seg_6098" s="T453">pat-ntɨ-r-lä-k</ta>
            <ta e="T455" id="Seg_6099" s="T454">üt-ntɨ</ta>
            <ta e="T456" id="Seg_6100" s="T455">təp-ɨ-m</ta>
            <ta e="T457" id="Seg_6101" s="T456">šüt-ɛː-sɨ-tɨ</ta>
            <ta e="T458" id="Seg_6102" s="T457">sɨːrɨ-n</ta>
            <ta e="T459" id="Seg_6103" s="T458">kota-ntɨ</ta>
            <ta e="T460" id="Seg_6104" s="T459">təmqup</ta>
            <ta e="T461" id="Seg_6105" s="T460">ippɨ</ta>
            <ta e="T753" id="Seg_6106" s="T461">üt</ta>
            <ta e="T462" id="Seg_6107" s="T753">totqɨp</ta>
            <ta e="T463" id="Seg_6108" s="T462">kota-qɨn</ta>
            <ta e="T464" id="Seg_6109" s="T463">İčʼakɨčʼɨka</ta>
            <ta e="T465" id="Seg_6110" s="T464">qən-ätɔːl-sɨ</ta>
            <ta e="T466" id="Seg_6111" s="T465">čʼuntɨ-lʼ</ta>
            <ta e="T467" id="Seg_6112" s="T466">qaqlɨ-ntɨ-sä</ta>
            <ta e="T468" id="Seg_6113" s="T467">i</ta>
            <ta e="T469" id="Seg_6114" s="T468">tɔːq-sä</ta>
            <ta e="T470" id="Seg_6115" s="T469">kazak-t</ta>
            <ta e="T471" id="Seg_6116" s="T470">taːtɨ-sɨ-tɨt</ta>
            <ta e="T472" id="Seg_6117" s="T471">wərqɨ</ta>
            <ta e="T473" id="Seg_6118" s="T472">pü-m</ta>
            <ta e="T474" id="Seg_6119" s="T473">sɔːrɨ-qo-olam-sɨ-tɨt</ta>
            <ta e="T475" id="Seg_6120" s="T474">täntɔ-tɨt</ta>
            <ta e="T476" id="Seg_6121" s="T475">ontɨ</ta>
            <ta e="T477" id="Seg_6122" s="T476">nʼentɨ</ta>
            <ta e="T478" id="Seg_6123" s="T477">seːpɨŋ</ta>
            <ta e="T479" id="Seg_6124" s="T478">İčʼakɨčʼɨka</ta>
            <ta e="T480" id="Seg_6125" s="T479">ɔːlɨk-ɨ-r-sɨ-l</ta>
            <ta e="T481" id="Seg_6126" s="T480">üt-qɨn</ta>
            <ta e="T482" id="Seg_6127" s="T481">qu-ɛntɨ-ntɨ</ta>
            <ta e="T483" id="Seg_6128" s="T482">təmqup</ta>
            <ta e="T484" id="Seg_6129" s="T483">üŋkɨl-tɨ-mpɨ-tɨ</ta>
            <ta e="T485" id="Seg_6130" s="T484">sɔːrɨ-qo-antaltɨ-mpɨ-tɨt</ta>
            <ta e="T486" id="Seg_6131" s="T485">pü-m</ta>
            <ta e="T487" id="Seg_6132" s="T486">wərqɨ</ta>
            <ta e="T488" id="Seg_6133" s="T487">pü-m</ta>
            <ta e="T489" id="Seg_6134" s="T488">ašša</ta>
            <ta e="T490" id="Seg_6135" s="T489">man</ta>
            <ta e="T491" id="Seg_6136" s="T490">İčʼakɨčʼɨka</ta>
            <ta e="T492" id="Seg_6137" s="T491">İčʼakɨčʼɨka</ta>
            <ta e="T493" id="Seg_6138" s="T492">qən-sɨ</ta>
            <ta e="T494" id="Seg_6139" s="T493">tam</ta>
            <ta e="T495" id="Seg_6140" s="T494">mašım</ta>
            <ta e="T496" id="Seg_6141" s="T495">ɔːlɨk-sɨ</ta>
            <ta e="T497" id="Seg_6142" s="T496">man</ta>
            <ta e="T498" id="Seg_6143" s="T497">təmqup-ɨ-ŋɨ-k</ta>
            <ta e="T499" id="Seg_6144" s="T498">ɔːlɨk-lä</ta>
            <ta e="T500" id="Seg_6145" s="T499">mašım</ta>
            <ta e="T501" id="Seg_6146" s="T500">šüt-sɨ</ta>
            <ta e="T502" id="Seg_6147" s="T501">qopɨ-lʼ</ta>
            <ta e="T503" id="Seg_6148" s="T502">kota-ntɨ</ta>
            <ta e="T504" id="Seg_6149" s="T503">kazak-t</ta>
            <ta e="T505" id="Seg_6150" s="T504">tom-ŋɨ-tɨt</ta>
            <ta e="T506" id="Seg_6151" s="T505">tan</ta>
            <ta e="T507" id="Seg_6152" s="T506">İčʼakɨčʼɨka-ntɨ</ta>
            <ta e="T508" id="Seg_6153" s="T507">seːpɨŋ</ta>
            <ta e="T509" id="Seg_6154" s="T508">moːlmɨ-tɨ-sɨ-ntɨ</ta>
            <ta e="T510" id="Seg_6155" s="T509">toːnna</ta>
            <ta e="T511" id="Seg_6156" s="T510">čʼuːrɨ-lä</ta>
            <ta e="T512" id="Seg_6157" s="T511">laŋkɨ-š-ŋɨ</ta>
            <ta e="T513" id="Seg_6158" s="T512">kota-qɨn</ta>
            <ta e="T514" id="Seg_6159" s="T513">man</ta>
            <ta e="T515" id="Seg_6160" s="T514">təmqup-ɨ-ŋɨ-k</ta>
            <ta e="T516" id="Seg_6161" s="T515">tü-ntɨ-sɨ-k</ta>
            <ta e="T517" id="Seg_6162" s="T516">čʼuntɨ-sä</ta>
            <ta e="T518" id="Seg_6163" s="T517">qaqlɨ-sä</ta>
            <ta e="T519" id="Seg_6164" s="T518">üt-ntɨ</ta>
            <ta e="T520" id="Seg_6165" s="T519">*tul-tɨ-lä</ta>
            <ta e="T521" id="Seg_6166" s="T520">čʼattɨ-sɨ-tɨt</ta>
            <ta e="T522" id="Seg_6167" s="T521">qoptɨ-lɨ-sɨ-tɨt</ta>
            <ta e="T523" id="Seg_6168" s="T522">pü-sä</ta>
            <ta e="T524" id="Seg_6169" s="T523">moqɨnä</ta>
            <ta e="T525" id="Seg_6170" s="T524">qən-sɨ-tɨt</ta>
            <ta e="T526" id="Seg_6171" s="T525">meː</ta>
            <ta e="T527" id="Seg_6172" s="T526">qoptɨ-rɨ-sɨ-mɨt</ta>
            <ta e="T528" id="Seg_6173" s="T527">üt-ntɨ</ta>
            <ta e="T529" id="Seg_6174" s="T528">İčʼakɨčʼɨka</ta>
            <ta e="T530" id="Seg_6175" s="T529">tü-sɨ</ta>
            <ta e="T531" id="Seg_6176" s="T530">moqɨnä</ta>
            <ta e="T532" id="Seg_6177" s="T531">čʼuntɨ-sä</ta>
            <ta e="T533" id="Seg_6178" s="T532">čʼuntɨ-lʼ</ta>
            <ta e="T534" id="Seg_6179" s="T533">qaqlɨ-sä</ta>
            <ta e="T535" id="Seg_6180" s="T534">tɔːq-tɨ</ta>
            <ta e="T536" id="Seg_6181" s="T535">üt-sä</ta>
            <ta e="T537" id="Seg_6182" s="T536">čʼontɨ-ptɨ-sɨ-tɨ</ta>
            <ta e="T538" id="Seg_6183" s="T537">tɛːmnɨ-ntɨ</ta>
            <ta e="T539" id="Seg_6184" s="T538">ınnä</ta>
            <ta e="T540" id="Seg_6185" s="T539">ɨːtɨ-äl-sɨ-tɨ</ta>
            <ta e="T541" id="Seg_6186" s="T540">təːkɨ-qo</ta>
            <ta e="T542" id="Seg_6187" s="T541">na</ta>
            <ta e="T543" id="Seg_6188" s="T542">vremʼa-qɨn</ta>
            <ta e="T544" id="Seg_6189" s="T543">načʼalʼnik</ta>
            <ta e="T545" id="Seg_6190" s="T544">Qolʼsaqo</ta>
            <ta e="T546" id="Seg_6191" s="T545">ira</ta>
            <ta e="T547" id="Seg_6192" s="T546">mantɨ-mpɨ</ta>
            <ta e="T548" id="Seg_6193" s="T547">İčʼakɨčʼɨka-n</ta>
            <ta e="T549" id="Seg_6194" s="T548">mɔːt-ɨ-lʼ</ta>
            <ta e="T550" id="Seg_6195" s="T549">pɛläk-ntɨ</ta>
            <ta e="T551" id="Seg_6196" s="T550">šoːqɨr</ta>
            <ta e="T552" id="Seg_6197" s="T551">čʼɔːtɨ-mpɨ-tɨ</ta>
            <ta e="T553" id="Seg_6198" s="T552">purqɨ</ta>
            <ta e="T554" id="Seg_6199" s="T553">qatqɨ</ta>
            <ta e="T555" id="Seg_6200" s="T554">šoːqɨr</ta>
            <ta e="T556" id="Seg_6201" s="T555">*nɔː-nɨ</ta>
            <ta e="T557" id="Seg_6202" s="T556">tɨmtɨ-sä</ta>
            <ta e="T558" id="Seg_6203" s="T557">nɨmtɨ-sä</ta>
            <ta e="T559" id="Seg_6204" s="T558">kətɨ-sɨ-tɨ</ta>
            <ta e="T560" id="Seg_6205" s="T559">kazak-ɨ-qı-nɨŋ</ta>
            <ta e="T561" id="Seg_6206" s="T560">qən-lä</ta>
            <ta e="T562" id="Seg_6207" s="T561">mantɨ-mpɨ-qo</ta>
            <ta e="T563" id="Seg_6208" s="T562">imaqota-m</ta>
            <ta e="T564" id="Seg_6209" s="T563">təp-ɨ-t</ta>
            <ta e="T565" id="Seg_6210" s="T564">qən-sɨ-tɨt</ta>
            <ta e="T566" id="Seg_6211" s="T565">mɔːt-ntɨ</ta>
            <ta e="T567" id="Seg_6212" s="T566">šeːr-sɨ-tɨt</ta>
            <ta e="T568" id="Seg_6213" s="T567">imaqota-n</ta>
            <ta e="T569" id="Seg_6214" s="T568">koptɨ</ta>
            <ta e="T570" id="Seg_6215" s="T569">čʼɔːtɨ</ta>
            <ta e="T571" id="Seg_6216" s="T570">omtɨ</ta>
            <ta e="T572" id="Seg_6217" s="T571">ostol-ɨ-n</ta>
            <ta e="T573" id="Seg_6218" s="T572">*iː-qɨn</ta>
            <ta e="T574" id="Seg_6219" s="T573">İčʼakɨčʼɨka</ta>
            <ta e="T575" id="Seg_6220" s="T574">nʼenʼnʼɨ-mɔːt-mpɨ</ta>
            <ta e="T576" id="Seg_6221" s="T575">muntɨk</ta>
            <ta e="T577" id="Seg_6222" s="T576">wəntɨ-tɨ</ta>
            <ta e="T578" id="Seg_6223" s="T577">qatal-mpɨ-tɨ</ta>
            <ta e="T579" id="Seg_6224" s="T578">kəm-čʼa</ta>
            <ta e="T580" id="Seg_6225" s="T579">porqɨ-m-tɨ</ta>
            <ta e="T581" id="Seg_6226" s="T580">nɨta-mpɨlʼ-ŋɨ</ta>
            <ta e="T582" id="Seg_6227" s="T581">təp-ɨ-t</ta>
            <ta e="T583" id="Seg_6228" s="T582">kətɨ-sɨ-tɨt</ta>
            <ta e="T584" id="Seg_6229" s="T583">šeːr-sɨ-tɨt</ta>
            <ta e="T585" id="Seg_6230" s="T584">dorova</ta>
            <ta e="T586" id="Seg_6231" s="T585">lʼaqa</ta>
            <ta e="T587" id="Seg_6232" s="T586">a</ta>
            <ta e="T588" id="Seg_6233" s="T587">İčʼakɨčʼɨka</ta>
            <ta e="T589" id="Seg_6234" s="T588">nʼenʼnʼɨ-mɔːt-lä</ta>
            <ta e="T590" id="Seg_6235" s="T589">omtɨ</ta>
            <ta e="T591" id="Seg_6236" s="T590">tɛː</ta>
            <ta e="T592" id="Seg_6237" s="T591">mašım</ta>
            <ta e="T593" id="Seg_6238" s="T592">üt-ɨsɨp</ta>
            <ta e="T594" id="Seg_6239" s="T593">qoptɨ-rɨ-sɨ-lɨt</ta>
            <ta e="T595" id="Seg_6240" s="T594">man</ta>
            <ta e="T596" id="Seg_6241" s="T595">morä-n</ta>
            <ta e="T597" id="Seg_6242" s="T596">ılqɨn</ta>
            <ta e="T598" id="Seg_6243" s="T597">kočʼčʼɨ-k</ta>
            <ta e="T599" id="Seg_6244" s="T598">tɔːq</ta>
            <ta e="T600" id="Seg_6245" s="T599">lɨptɨŋ</ta>
            <ta e="T601" id="Seg_6246" s="T600">qo-sɨ-m</ta>
            <ta e="T602" id="Seg_6247" s="T601">naššak</ta>
            <ta e="T603" id="Seg_6248" s="T602">ɛː-ŋɨ</ta>
            <ta e="T604" id="Seg_6249" s="T603">kuššak</ta>
            <ta e="T605" id="Seg_6250" s="T604">orɨ-mɨ</ta>
            <ta e="T606" id="Seg_6251" s="T605">ɛː-sɨ</ta>
            <ta e="T607" id="Seg_6252" s="T606">iː-sɨ-m</ta>
            <ta e="T608" id="Seg_6253" s="T607">man</ta>
            <ta e="T609" id="Seg_6254" s="T608">orɨ-mɨ</ta>
            <ta e="T610" id="Seg_6255" s="T609">ašša</ta>
            <ta e="T611" id="Seg_6256" s="T610">iː-sɨ-tɨ</ta>
            <ta e="T612" id="Seg_6257" s="T611">bɨtta</ta>
            <ta e="T613" id="Seg_6258" s="T612">üt-en-ɨ-lʼ</ta>
            <ta e="T614" id="Seg_6259" s="T613">nom-qɨn</ta>
            <ta e="T615" id="Seg_6260" s="T614">ɛː-sɨ-k</ta>
            <ta e="T616" id="Seg_6261" s="T615">təp-ɨ-t</ta>
            <ta e="T617" id="Seg_6262" s="T616">qən-sɨ-tɨt</ta>
            <ta e="T618" id="Seg_6263" s="T617">nɨrkɨ-mɔːt-lä</ta>
            <ta e="T619" id="Seg_6264" s="T618">qaj-lʼ</ta>
            <ta e="T620" id="Seg_6265" s="T619">jabəlʼ</ta>
            <ta e="T621" id="Seg_6266" s="T620">ɛː-sɨ</ta>
            <ta e="T622" id="Seg_6267" s="T621">Qolʼsaqo</ta>
            <ta e="T623" id="Seg_6268" s="T622">ira-nɨŋ</ta>
            <ta e="T624" id="Seg_6269" s="T623">kətɨ-qo</ta>
            <ta e="T625" id="Seg_6270" s="T624">İčʼakɨčʼɨka-m</ta>
            <ta e="T626" id="Seg_6271" s="T625">üt-ntɨ</ta>
            <ta e="T627" id="Seg_6272" s="T626">qoptɨ-rɨ-sɨ-mɨt</ta>
            <ta e="T629" id="Seg_6273" s="T628">čʼeːlɨ</ta>
            <ta e="T630" id="Seg_6274" s="T629">ukoːn</ta>
            <ta e="T631" id="Seg_6275" s="T630">təp</ta>
            <ta e="T632" id="Seg_6276" s="T631">našša</ta>
            <ta e="T633" id="Seg_6277" s="T632">qo-mpɨ-tɨ</ta>
            <ta e="T634" id="Seg_6278" s="T633">tɔːq</ta>
            <ta e="T635" id="Seg_6279" s="T634">lɨptɨŋ</ta>
            <ta e="T636" id="Seg_6280" s="T635">*kušša-lʼ</ta>
            <ta e="T637" id="Seg_6281" s="T636">tɛtaqaj</ta>
            <ta e="T638" id="Seg_6282" s="T637">muntɨk</ta>
            <ta e="T639" id="Seg_6283" s="T638">qo-mpɨ-tɨ</ta>
            <ta e="T640" id="Seg_6284" s="T639">i</ta>
            <ta e="T641" id="Seg_6285" s="T640">sıː-n</ta>
            <ta e="T642" id="Seg_6286" s="T641">qopɨ-lʼ</ta>
            <ta e="T643" id="Seg_6287" s="T642">mɨ-t</ta>
            <ta e="T644" id="Seg_6288" s="T643">Qolʼsaqo</ta>
            <ta e="T645" id="Seg_6289" s="T644">ira</ta>
            <ta e="T646" id="Seg_6290" s="T645">tɛnɨ-tɨ</ta>
            <ta e="T647" id="Seg_6291" s="T646">ürɨ-sɨ</ta>
            <ta e="T648" id="Seg_6292" s="T647">qäntɨk</ta>
            <ta e="T649" id="Seg_6293" s="T648">nılʼčʼɨ-k</ta>
            <ta e="T650" id="Seg_6294" s="T649">ɛsɨ-sɨ</ta>
            <ta e="T651" id="Seg_6295" s="T650">İčʼakɨčʼɨka</ta>
            <ta e="T652" id="Seg_6296" s="T651">jabəlʼ</ta>
            <ta e="T655" id="Seg_6297" s="T654">šentɨ-lʼ</ta>
            <ta e="T656" id="Seg_6298" s="T655">čʼeːlɨ</ta>
            <ta e="T657" id="Seg_6299" s="T656">ontɨ</ta>
            <ta e="T658" id="Seg_6300" s="T657">šittɨ-t</ta>
            <ta e="T659" id="Seg_6301" s="T658">kazak-t</ta>
            <ta e="T660" id="Seg_6302" s="T659">tü-ŋčʼɔː-tɨt</ta>
            <ta e="T661" id="Seg_6303" s="T660">İčʼakɨčʼɨka-ntɨ</ta>
            <ta e="T662" id="Seg_6304" s="T661">üːtɨ-tɨ</ta>
            <ta e="T663" id="Seg_6305" s="T662">kazak</ta>
            <ta e="T664" id="Seg_6306" s="T663">ira</ta>
            <ta e="T665" id="Seg_6307" s="T664">soqɨš-qo</ta>
            <ta e="T666" id="Seg_6308" s="T665">İčʼakɨčʼɨka-m</ta>
            <ta e="T667" id="Seg_6309" s="T666">kuttar</ta>
            <ta e="T668" id="Seg_6310" s="T667">qo-sɨ-tɨ</ta>
            <ta e="T669" id="Seg_6311" s="T668">na</ta>
            <ta e="T670" id="Seg_6312" s="T669">to</ta>
            <ta e="T671" id="Seg_6313" s="T670">lɨptɨŋ-t-ɨ-m</ta>
            <ta e="T672" id="Seg_6314" s="T671">meː</ta>
            <ta e="T673" id="Seg_6315" s="T672">aj</ta>
            <ta e="T674" id="Seg_6316" s="T673">üt-ntɨ</ta>
            <ta e="T675" id="Seg_6317" s="T674">pat-ntɨr-ɛntɨ-mɨt</ta>
            <ta e="T676" id="Seg_6318" s="T675">tɔːq-ɨ-tqo</ta>
            <ta e="T677" id="Seg_6319" s="T676">İčʼakɨčʼɨka</ta>
            <ta e="T678" id="Seg_6320" s="T677">kətɨ-sɨ-tɨ</ta>
            <ta e="T679" id="Seg_6321" s="T678">šüt-qonɨ-ŋɨlɨt</ta>
            <ta e="T680" id="Seg_6322" s="T679">šüt-qɨl-ŋɨ-tɨt</ta>
            <ta e="T681" id="Seg_6323" s="T680">šüt-qɨl-ŋɨ-lɨt</ta>
            <ta e="T682" id="Seg_6324" s="T681">sɨːrɨ-n</ta>
            <ta e="T683" id="Seg_6325" s="T682">qopɨ</ta>
            <ta e="T684" id="Seg_6326" s="T683">*nɔː-nɨ</ta>
            <ta e="T685" id="Seg_6327" s="T684">kota-t-ɨ-m</ta>
            <ta e="T686" id="Seg_6328" s="T685">šüt-ŋɨlɨt</ta>
            <ta e="T687" id="Seg_6329" s="T686">kota-t-ɨ-m</ta>
            <ta e="T688" id="Seg_6330" s="T687">sɨːrɨ-n</ta>
            <ta e="T689" id="Seg_6331" s="T688">qopɨ-lʼ</ta>
            <ta e="T690" id="Seg_6332" s="T689">ɔːlʼčʼɨ-ŋɨlɨt</ta>
            <ta e="T691" id="Seg_6333" s="T690">morä-n</ta>
            <ta e="T692" id="Seg_6334" s="T691">qanɨŋ-ntɨ</ta>
            <ta e="T693" id="Seg_6335" s="T692">man</ta>
            <ta e="T694" id="Seg_6336" s="T693">tɛːšıntɨn</ta>
            <ta e="T695" id="Seg_6337" s="T694">šüt-ɛntɨ-m</ta>
            <ta e="T696" id="Seg_6338" s="T695">kota-ntɨ</ta>
            <ta e="T697" id="Seg_6339" s="T696">kəː-ɛntɨ-m</ta>
            <ta e="T698" id="Seg_6340" s="T697">üt-ntɨ</ta>
            <ta e="T699" id="Seg_6341" s="T698">pü-sä</ta>
            <ta e="T700" id="Seg_6342" s="T699">wərqɨ</ta>
            <ta e="T701" id="Seg_6343" s="T700">pü-sä</ta>
            <ta e="T702" id="Seg_6344" s="T701">tɛː</ta>
            <ta e="T703" id="Seg_6345" s="T702">qo-ɛntɨ-lɨt</ta>
            <ta e="T704" id="Seg_6346" s="T703">muntɨk</ta>
            <ta e="T705" id="Seg_6347" s="T704">tɔːq</ta>
            <ta e="T706" id="Seg_6348" s="T705">ukoːn</ta>
            <ta e="T707" id="Seg_6349" s="T706">Qolʼsaqo</ta>
            <ta e="T708" id="Seg_6350" s="T707">ira</ta>
            <ta e="T709" id="Seg_6351" s="T708">načʼalʼnik</ta>
            <ta e="T710" id="Seg_6352" s="T709">kutɨ</ta>
            <ta e="T711" id="Seg_6353" s="T710">kɨkɨ</ta>
            <ta e="T712" id="Seg_6354" s="T711">qəːttɨ-qɨnɨ</ta>
            <ta e="T713" id="Seg_6355" s="T712">təp</ta>
            <ta e="T714" id="Seg_6356" s="T713">šüt-kolʼ-sɨ-tɨ</ta>
            <ta e="T715" id="Seg_6357" s="T714">ukoːn</ta>
            <ta e="T716" id="Seg_6358" s="T715">Qolʼsaqo</ta>
            <ta e="T717" id="Seg_6359" s="T716">ira-m</ta>
            <ta e="T718" id="Seg_6360" s="T717">nɨːnɨ</ta>
            <ta e="T719" id="Seg_6361" s="T718">mənɨlʼ</ta>
            <ta e="T720" id="Seg_6362" s="T719">qum-iː-m-tɨ</ta>
            <ta e="T721" id="Seg_6363" s="T720">kazak-t-ɨ-m</ta>
            <ta e="T722" id="Seg_6364" s="T721">nɨːnɨ</ta>
            <ta e="T723" id="Seg_6365" s="T722">İčʼakɨčʼɨka</ta>
            <ta e="T724" id="Seg_6366" s="T723">ontɨ</ta>
            <ta e="T725" id="Seg_6367" s="T724">kəː-qo-olam-sɨ-tɨ</ta>
            <ta e="T726" id="Seg_6368" s="T725">šüt-ɨ-mpɨlʼ</ta>
            <ta e="T727" id="Seg_6369" s="T726">kota-t-ɨ-m</ta>
            <ta e="T728" id="Seg_6370" s="T727">qum-ɨ-t-sä</ta>
            <ta e="T729" id="Seg_6371" s="T728">üt-ntɨ</ta>
            <ta e="T730" id="Seg_6372" s="T729">kətɨ-sɨ-tɨ</ta>
            <ta e="T731" id="Seg_6373" s="T730">Qolʼsaqo</ta>
            <ta e="T732" id="Seg_6374" s="T731">ira-nɨŋ</ta>
            <ta e="T733" id="Seg_6375" s="T732">peː-ätɨ</ta>
            <ta e="T734" id="Seg_6376" s="T733">tɔːq</ta>
            <ta e="T735" id="Seg_6377" s="T734">morä-n</ta>
            <ta e="T736" id="Seg_6378" s="T735">ılqɨn</ta>
            <ta e="T737" id="Seg_6379" s="T736">nʼi</ta>
            <ta e="T738" id="Seg_6380" s="T737">kun</ta>
            <ta e="T739" id="Seg_6381" s="T738">ašša</ta>
            <ta e="T740" id="Seg_6382" s="T739">qo-ɛntɨ-l</ta>
            <ta e="T741" id="Seg_6383" s="T740">tɔːq-ɨ-tɨ</ta>
            <ta e="T742" id="Seg_6384" s="T741">i</ta>
            <ta e="T743" id="Seg_6385" s="T742">tawar-ɨ-t</ta>
            <ta e="T744" id="Seg_6386" s="T743">meːltɨ</ta>
            <ta e="T745" id="Seg_6387" s="T744">qu-ɛntɨ-ntɨ</ta>
            <ta e="T746" id="Seg_6388" s="T745">tɛː</ta>
            <ta e="T747" id="Seg_6389" s="T746">na</ta>
            <ta e="T748" id="Seg_6390" s="T747">mašım</ta>
            <ta e="T749" id="Seg_6391" s="T748">kəː-kkɨ-sɨ-lɨt</ta>
            <ta e="T750" id="Seg_6392" s="T749">üt-ntɨ</ta>
            <ta e="T751" id="Seg_6393" s="T750">mašım</ta>
            <ta e="T752" id="Seg_6394" s="T751">kəː-sɨ-lɨt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_6395" s="T0">Kolsako.[NOM]</ta>
            <ta e="T2" id="Seg_6396" s="T1">old.man.[NOM]</ta>
            <ta e="T3" id="Seg_6397" s="T2">live-PST.NAR.[3SG.S]</ta>
            <ta e="T4" id="Seg_6398" s="T3">Ichakichika.[NOM]</ta>
            <ta e="T5" id="Seg_6399" s="T4">grandmother-OBL.3SG-COM</ta>
            <ta e="T6" id="Seg_6400" s="T5">Kolsako.[NOM]</ta>
            <ta e="T7" id="Seg_6401" s="T6">old.man-ADJZ</ta>
            <ta e="T8" id="Seg_6402" s="T7">town.[NOM]</ta>
            <ta e="T9" id="Seg_6403" s="T8">NEG</ta>
            <ta e="T10" id="Seg_6404" s="T9">far.away</ta>
            <ta e="T11" id="Seg_6405" s="T10">be-PST.[3SG.S]</ta>
            <ta e="T12" id="Seg_6406" s="T11">one</ta>
            <ta e="T13" id="Seg_6407" s="T12">middle-LOC</ta>
            <ta e="T14" id="Seg_6408" s="T13">Kolsako.[NOM]</ta>
            <ta e="T15" id="Seg_6409" s="T14">old.man-ADJZ</ta>
            <ta e="T16" id="Seg_6410" s="T15">cow.[NOM]-3SG</ta>
            <ta e="T17" id="Seg_6411" s="T16">get.lost.[3SG.S]</ta>
            <ta e="T18" id="Seg_6412" s="T17">get.lost-PST.[3SG.S]</ta>
            <ta e="T20" id="Seg_6413" s="T19">get.lost-DUR.[3SG.S]</ta>
            <ta e="T21" id="Seg_6414" s="T20">(s)he.[NOM]</ta>
            <ta e="T22" id="Seg_6415" s="T21">know-3SG.O</ta>
            <ta e="T23" id="Seg_6416" s="T22">that</ta>
            <ta e="T24" id="Seg_6417" s="T23">Ichakichika.[NOM]-3SG</ta>
            <ta e="T25" id="Seg_6418" s="T24">steal-PST-3SG.O</ta>
            <ta e="T26" id="Seg_6419" s="T25">sent-3SG.O</ta>
            <ta e="T27" id="Seg_6420" s="T26">(s)he-EP-ALL</ta>
            <ta e="T28" id="Seg_6421" s="T27">Cossack-PL-EP-ACC</ta>
            <ta e="T29" id="Seg_6422" s="T28">two-PL-EP-ACC</ta>
            <ta e="T30" id="Seg_6423" s="T29">give.a.look-HAB-INF</ta>
            <ta e="T31" id="Seg_6424" s="T30">what-EP-ACC</ta>
            <ta e="T32" id="Seg_6425" s="T31">make-INFER-3SG.O</ta>
            <ta e="T33" id="Seg_6426" s="T32">Ichakichika.[NOM]</ta>
            <ta e="T34" id="Seg_6427" s="T33">(s)he.[NOM]</ta>
            <ta e="T35" id="Seg_6428" s="T34">know-3SG.O</ta>
            <ta e="T36" id="Seg_6429" s="T35">come-FUT-3PL</ta>
            <ta e="T37" id="Seg_6430" s="T36">rifle-COM</ta>
            <ta e="T38" id="Seg_6431" s="T37">rifle-OBL.3SG-COM</ta>
            <ta e="T39" id="Seg_6432" s="T38">(s)he-EP-ACC</ta>
            <ta e="T40" id="Seg_6433" s="T39">catch-FUT-3PL</ta>
            <ta e="T41" id="Seg_6434" s="T40">sit.down-TR-FUT-3PL</ta>
            <ta e="T42" id="Seg_6435" s="T41">prison-ILL</ta>
            <ta e="T43" id="Seg_6436" s="T42">this</ta>
            <ta e="T44" id="Seg_6437" s="T43">time-LOC</ta>
            <ta e="T45" id="Seg_6438" s="T44">this</ta>
            <ta e="T46" id="Seg_6439" s="T45">day.[NOM]</ta>
            <ta e="T47" id="Seg_6440" s="T46">cow-ACC</ta>
            <ta e="T48" id="Seg_6441" s="T47">kill-PST-3SG.O</ta>
            <ta e="T49" id="Seg_6442" s="T48">intestine-ACC-3SG</ta>
            <ta e="T50" id="Seg_6443" s="T49">blood-INSTR</ta>
            <ta e="T51" id="Seg_6444" s="T50">pour.out-PST-3SG.O</ta>
            <ta e="T52" id="Seg_6445" s="T51">keep-PST-3SG.O</ta>
            <ta e="T53" id="Seg_6446" s="T52">big</ta>
            <ta e="T54" id="Seg_6447" s="T53">knife-ACC</ta>
            <ta e="T55" id="Seg_6448" s="T54">tent-EP.GEN</ta>
            <ta e="T56" id="Seg_6449" s="T55">%%-LOC</ta>
            <ta e="T57" id="Seg_6450" s="T56">grandmother-ACC-3SG</ta>
            <ta e="T58" id="Seg_6451" s="T57">wrap-PST-3SG.O</ta>
            <ta e="T59" id="Seg_6452" s="T58">blood-EP-ADJZ</ta>
            <ta e="T60" id="Seg_6453" s="T59">cow-GEN</ta>
            <ta e="T61" id="Seg_6454" s="T60">intestine-INSTR</ta>
            <ta e="T62" id="Seg_6455" s="T61">grandmother.[NOM]-3SG</ta>
            <ta e="T63" id="Seg_6456" s="T62">put.on-TR-PST-3SG.O</ta>
            <ta e="T64" id="Seg_6457" s="T63">clothing-PL.[NOM]</ta>
            <ta e="T65" id="Seg_6458" s="T64">Cossack-PL.[NOM]</ta>
            <ta e="T66" id="Seg_6459" s="T65">Cossack-EP-PL.[NOM]</ta>
            <ta e="T67" id="Seg_6460" s="T66">come-PST-3PL</ta>
            <ta e="T68" id="Seg_6461" s="T67">Ichakichika-ALL</ta>
            <ta e="T69" id="Seg_6462" s="T68">big</ta>
            <ta e="T70" id="Seg_6463" s="T69">human.being.[NOM]</ta>
            <ta e="T71" id="Seg_6464" s="T70">say-PST-3SG.O</ta>
            <ta e="T72" id="Seg_6465" s="T71">Ichakichika-ALL</ta>
            <ta e="T73" id="Seg_6466" s="T72">you.SG.ACC</ta>
            <ta e="T74" id="Seg_6467" s="T73">sit.down-TR-INF</ta>
            <ta e="T75" id="Seg_6468" s="T74">need</ta>
            <ta e="T76" id="Seg_6469" s="T75">prison-ILL</ta>
            <ta e="T77" id="Seg_6470" s="T76">you.SG.NOM</ta>
            <ta e="T78" id="Seg_6471" s="T77">cow-PL-EP-ACC</ta>
            <ta e="T79" id="Seg_6472" s="T78">kill-HAB-2SG.O</ta>
            <ta e="T80" id="Seg_6473" s="T79">eat-CO-2SG.O</ta>
            <ta e="T81" id="Seg_6474" s="T80">(s)he.[NOM]</ta>
            <ta e="T82" id="Seg_6475" s="T81">get.angry-DECAUS-PST.[3SG.S]</ta>
            <ta e="T83" id="Seg_6476" s="T82">grandmother-OBL.3SG-ALL</ta>
            <ta e="T84" id="Seg_6477" s="T83">say-PST-3SG.O</ta>
            <ta e="T85" id="Seg_6478" s="T84">I.ACC</ta>
            <ta e="T86" id="Seg_6479" s="T85">sit.down-CAUS-FUT-3PL</ta>
            <ta e="T87" id="Seg_6480" s="T86">prison-ILL</ta>
            <ta e="T88" id="Seg_6481" s="T87">teapot.[NOM]</ta>
            <ta e="T89" id="Seg_6482" s="T88">be.cooking-TR-IMP.2SG.O</ta>
            <ta e="T90" id="Seg_6483" s="T89">hurry.up-CVB</ta>
            <ta e="T91" id="Seg_6484" s="T90">grandmother.[NOM]-3SG</ta>
            <ta e="T92" id="Seg_6485" s="T91">swear-INCH-CO.[3SG.S]</ta>
            <ta e="T93" id="Seg_6486" s="T92">leg.[NOM]-1SG</ta>
            <ta e="T94" id="Seg_6487" s="T93">hand.[NOM]-1SG</ta>
            <ta e="T95" id="Seg_6488" s="T94">hurt.[3SG.S]</ta>
            <ta e="T96" id="Seg_6489" s="T95">Cossack-PL.[NOM]</ta>
            <ta e="T97" id="Seg_6490" s="T96">stand-CO-3PL</ta>
            <ta e="T98" id="Seg_6491" s="T97">door-GEN</ta>
            <ta e="T99" id="Seg_6492" s="T98">opening-LOC</ta>
            <ta e="T100" id="Seg_6493" s="T99">Ichakichika.[NOM]</ta>
            <ta e="T101" id="Seg_6494" s="T100">grandmother-OBL.3SG-ALL</ta>
            <ta e="T102" id="Seg_6495" s="T101">you.SG.NOM</ta>
            <ta e="T103" id="Seg_6496" s="T102">lazy.person-CO-2SG.S</ta>
            <ta e="T104" id="Seg_6497" s="T103">teapot.[NOM]</ta>
            <ta e="T105" id="Seg_6498" s="T104">NEG</ta>
            <ta e="T106" id="Seg_6499" s="T105">want-2SG.S</ta>
            <ta e="T107" id="Seg_6500" s="T106">be.cooking-CAUS-INF</ta>
            <ta e="T108" id="Seg_6501" s="T107">get.angry-DECAUS-PST.[3SG.S]</ta>
            <ta e="T109" id="Seg_6502" s="T108">Ichakichika.[NOM]</ta>
            <ta e="T110" id="Seg_6503" s="T109">knife-ACC-3SG</ta>
            <ta e="T111" id="Seg_6504" s="T110">down</ta>
            <ta e="T112" id="Seg_6505" s="T111">take-PST-3SG.O</ta>
            <ta e="T113" id="Seg_6506" s="T112">grandmother-ACC-3SG</ta>
            <ta e="T114" id="Seg_6507" s="T113">cut-FRQ-PST-3SG.O</ta>
            <ta e="T115" id="Seg_6508" s="T114">grandmother.[NOM]-3SG</ta>
            <ta e="T116" id="Seg_6509" s="T115">die-PFV-PST.[3SG.S]</ta>
            <ta e="T117" id="Seg_6510" s="T116">then</ta>
            <ta e="T118" id="Seg_6511" s="T117">Ichakichika.[NOM]</ta>
            <ta e="T119" id="Seg_6512" s="T118">say-PST-3SG.O</ta>
            <ta e="T120" id="Seg_6513" s="T119">Cossack-PL-ALL</ta>
            <ta e="T121" id="Seg_6514" s="T120">grandmother.[NOM]-1SG</ta>
            <ta e="T122" id="Seg_6515" s="T121">always</ta>
            <ta e="T123" id="Seg_6516" s="T122">such-ADVZ</ta>
            <ta e="T124" id="Seg_6517" s="T123">force-VBLZ-CO.[3SG.S]</ta>
            <ta e="T125" id="Seg_6518" s="T124">knife.[NOM]-3SG</ta>
            <ta e="T126" id="Seg_6519" s="T125">leg-LOC-OBL.3SG</ta>
            <ta e="T127" id="Seg_6520" s="T126">stick.up-CAUS-PST-3SG.O</ta>
            <ta e="T128" id="Seg_6521" s="T127">grandmother.[NOM]</ta>
            <ta e="T129" id="Seg_6522" s="T128">up</ta>
            <ta e="T130" id="Seg_6523" s="T129">jump-IMP.2SG.S</ta>
            <ta e="T131" id="Seg_6524" s="T130">knife.[NOM]-1SG</ta>
            <ta e="T132" id="Seg_6525" s="T131">live-TR-FUT.[3SG.S]</ta>
            <ta e="T133" id="Seg_6526" s="T132">then</ta>
            <ta e="T134" id="Seg_6527" s="T133">grandmother.[NOM]-3SG</ta>
            <ta e="T135" id="Seg_6528" s="T134">stand-PFV-PST.[3SG.S]</ta>
            <ta e="T136" id="Seg_6529" s="T135">hurry.up-CVB</ta>
            <ta e="T137" id="Seg_6530" s="T136">catch-PST-3SG.O</ta>
            <ta e="T138" id="Seg_6531" s="T137">teapot.ACC-3SG</ta>
            <ta e="T139" id="Seg_6532" s="T138">be.cooking-CAUS-INF</ta>
            <ta e="T140" id="Seg_6533" s="T139">pipe-ACC</ta>
            <ta e="T141" id="Seg_6534" s="T140">smoke-MULO-ACTN-GEN</ta>
            <ta e="T142" id="Seg_6535" s="T141">during</ta>
            <ta e="T143" id="Seg_6536" s="T142">teapot.[NOM]</ta>
            <ta e="T144" id="Seg_6537" s="T143">be.cooking-PFV-PST.[3SG.S]</ta>
            <ta e="T145" id="Seg_6538" s="T144">bread-EP-ACC</ta>
            <ta e="T146" id="Seg_6539" s="T145">food-ACC</ta>
            <ta e="T147" id="Seg_6540" s="T146">put-PST-3SG.O</ta>
            <ta e="T148" id="Seg_6541" s="T147">table.in.a.tent-ILL</ta>
            <ta e="T149" id="Seg_6542" s="T148">hurry.up-CVB</ta>
            <ta e="T150" id="Seg_6543" s="T149">Cossack-PL.[NOM]</ta>
            <ta e="T151" id="Seg_6544" s="T150">eat-EP-FRQ-INF-be.going.to-PST-3PL</ta>
            <ta e="T152" id="Seg_6545" s="T151">(s)he-EP-PL.[NOM]</ta>
            <ta e="T153" id="Seg_6546" s="T152">such-ADVZ</ta>
            <ta e="T154" id="Seg_6547" s="T153">say-PST-3PL</ta>
            <ta e="T155" id="Seg_6548" s="T154">we.PL.NOM</ta>
            <ta e="T156" id="Seg_6549" s="T155">you.SG.GEN</ta>
            <ta e="T157" id="Seg_6550" s="T156">knife-ACC</ta>
            <ta e="T158" id="Seg_6551" s="T157">buy-FUT-1PL</ta>
            <ta e="T159" id="Seg_6552" s="T158">we.PL.GEN</ta>
            <ta e="T160" id="Seg_6553" s="T159">wife-PL.[NOM]-1PL</ta>
            <ta e="T161" id="Seg_6554" s="T160">lazy.person-TR-3PL</ta>
            <ta e="T162" id="Seg_6555" s="T161">and</ta>
            <ta e="T163" id="Seg_6556" s="T162">sleep-3PL</ta>
            <ta e="T164" id="Seg_6557" s="T163">Cossack-PL.[NOM]</ta>
            <ta e="T165" id="Seg_6558" s="T164">NEG</ta>
            <ta e="T166" id="Seg_6559" s="T165">move-CAUS-INF-be.going.to-PST-3PL</ta>
            <ta e="T167" id="Seg_6560" s="T166">Ichakichika-ACC</ta>
            <ta e="T168" id="Seg_6561" s="T167">say-FUT-3PL</ta>
            <ta e="T169" id="Seg_6562" s="T168">say-PST-3PL</ta>
            <ta e="T170" id="Seg_6563" s="T169">Kolsako.[NOM]</ta>
            <ta e="T171" id="Seg_6564" s="T170">old.man-ALL</ta>
            <ta e="T172" id="Seg_6565" s="T171">kill-PST-3PL</ta>
            <ta e="T173" id="Seg_6566" s="T172">here</ta>
            <ta e="T174" id="Seg_6567" s="T173">say-PST.NAR-3PL</ta>
            <ta e="T175" id="Seg_6568" s="T174">head.[NOM]</ta>
            <ta e="T176" id="Seg_6569" s="T175">go-CAUS-PST-3SG.O</ta>
            <ta e="T177" id="Seg_6570" s="T176">buy-INF</ta>
            <ta e="T178" id="Seg_6571" s="T177">knife-ACC</ta>
            <ta e="T179" id="Seg_6572" s="T178">next</ta>
            <ta e="T180" id="Seg_6573" s="T179">day.[NOM]</ta>
            <ta e="T181" id="Seg_6574" s="T180">buy-3PL</ta>
            <ta e="T182" id="Seg_6575" s="T181">Kolsako.[NOM]</ta>
            <ta e="T183" id="Seg_6576" s="T182">old.man.[NOM]</ta>
            <ta e="T184" id="Seg_6577" s="T183">say-PST-3SG.O</ta>
            <ta e="T185" id="Seg_6578" s="T184">knife-ACC</ta>
            <ta e="T186" id="Seg_6579" s="T185">I.DAT</ta>
            <ta e="T187" id="Seg_6580" s="T186">give-IMP.2PL</ta>
            <ta e="T188" id="Seg_6581" s="T187">I.NOM</ta>
            <ta e="T189" id="Seg_6582" s="T188">spend.night-TR-FUT-1SG.O</ta>
            <ta e="T190" id="Seg_6583" s="T189">I.GEN</ta>
            <ta e="T191" id="Seg_6584" s="T190">wife.[NOM]-1SG</ta>
            <ta e="T192" id="Seg_6585" s="T191">and</ta>
            <ta e="T193" id="Seg_6586" s="T192">worker-EP-PL.[NOM]</ta>
            <ta e="T194" id="Seg_6587" s="T193">lazy.person-TR-3PL</ta>
            <ta e="T195" id="Seg_6588" s="T194">day-PL.[NOM]</ta>
            <ta e="T196" id="Seg_6589" s="T195">and</ta>
            <ta e="T197" id="Seg_6590" s="T196">night-PL.[NOM]</ta>
            <ta e="T198" id="Seg_6591" s="T197">sleep-3PL</ta>
            <ta e="T199" id="Seg_6592" s="T198">Ichakichika.[NOM]</ta>
            <ta e="T200" id="Seg_6593" s="T199">say-PST.NAR-3SG.O</ta>
            <ta e="T201" id="Seg_6594" s="T200">say-PST-3SG.O</ta>
            <ta e="T202" id="Seg_6595" s="T201">knife-ACC</ta>
            <ta e="T203" id="Seg_6596" s="T202">clean-ADVZ</ta>
            <ta e="T204" id="Seg_6597" s="T203">keep-IMP.2PL</ta>
            <ta e="T205" id="Seg_6598" s="T204">good</ta>
            <ta e="T206" id="Seg_6599" s="T205">human.being-EP-ACC</ta>
            <ta e="T207" id="Seg_6600" s="T206">cut-FRQ-INF</ta>
            <ta e="T208" id="Seg_6601" s="T207">Kolsako.[NOM]</ta>
            <ta e="T209" id="Seg_6602" s="T208">old.man.[NOM]</ta>
            <ta e="T210" id="Seg_6603" s="T209">sleep-PST.[3SG.S]</ta>
            <ta e="T211" id="Seg_6604" s="T210">morning-ADV.LOC</ta>
            <ta e="T212" id="Seg_6605" s="T211">soon</ta>
            <ta e="T213" id="Seg_6606" s="T212">sit.down-PST.[3SG.S]</ta>
            <ta e="T214" id="Seg_6607" s="T213">get.up-PST.[3SG.S]</ta>
            <ta e="T215" id="Seg_6608" s="T214">wife.[NOM]</ta>
            <ta e="T216" id="Seg_6609" s="T215">sit.down-IMP.2SG.S</ta>
            <ta e="T217" id="Seg_6610" s="T216">and</ta>
            <ta e="T218" id="Seg_6611" s="T217">worker-EP-PL.[NOM]</ta>
            <ta e="T219" id="Seg_6612" s="T218">sit.down-IMP.2PL</ta>
            <ta e="T220" id="Seg_6613" s="T219">(s)he-EP-PL.[NOM]</ta>
            <ta e="T221" id="Seg_6614" s="T220">soon</ta>
            <ta e="T222" id="Seg_6615" s="T221">NEG</ta>
            <ta e="T223" id="Seg_6616" s="T222">sit.down-IPFV-3PL</ta>
            <ta e="T224" id="Seg_6617" s="T223">get.angry-DECAUS-PST.[3SG.S]</ta>
            <ta e="T225" id="Seg_6618" s="T224">down</ta>
            <ta e="T226" id="Seg_6619" s="T225">take-PST-3SG.O</ta>
            <ta e="T227" id="Seg_6620" s="T226">knife-ACC-3SG</ta>
            <ta e="T228" id="Seg_6621" s="T227">cut-MULO-PST-3SG.O</ta>
            <ta e="T229" id="Seg_6622" s="T228">all</ta>
            <ta e="T230" id="Seg_6623" s="T229">knife-ACC</ta>
            <ta e="T231" id="Seg_6624" s="T230">stick.up-CAUS-PST-3SG.O</ta>
            <ta e="T232" id="Seg_6625" s="T231">leg-ILL-OBL.3SG</ta>
            <ta e="T233" id="Seg_6626" s="T232">all</ta>
            <ta e="T234" id="Seg_6627" s="T233">cut-FRQ-PTCP.PST</ta>
            <ta e="T235" id="Seg_6628" s="T234">human.being-PL-ILL-OBL.3SG</ta>
            <ta e="T236" id="Seg_6629" s="T235">only</ta>
            <ta e="T237" id="Seg_6630" s="T236">say-DUR-3SG.O</ta>
            <ta e="T238" id="Seg_6631" s="T237">always</ta>
            <ta e="T239" id="Seg_6632" s="T238">die-PFV-3PL</ta>
            <ta e="T240" id="Seg_6633" s="T239">old.man.[NOM]</ta>
            <ta e="T241" id="Seg_6634" s="T240">get.angry-DECAUS-PST.[3SG.S]</ta>
            <ta e="T242" id="Seg_6635" s="T241">Cossack-PL-EP-ACC</ta>
            <ta e="T243" id="Seg_6636" s="T242">invite-PST-3SG.O</ta>
            <ta e="T244" id="Seg_6637" s="T243">where</ta>
            <ta e="T245" id="Seg_6638" s="T244">take-PST-%%</ta>
            <ta e="T246" id="Seg_6639" s="T245">knife-ACC</ta>
            <ta e="T247" id="Seg_6640" s="T246">(s)he.[NOM]</ta>
            <ta e="T248" id="Seg_6641" s="T247">Ichakichika.[NOM]</ta>
            <ta e="T249" id="Seg_6642" s="T248">we.PL-ALL</ta>
            <ta e="T250" id="Seg_6643" s="T249">knife-ACC</ta>
            <ta e="T251" id="Seg_6644" s="T250">give-PST-3SG.O</ta>
            <ta e="T252" id="Seg_6645" s="T251">(s)he.[NOM]</ta>
            <ta e="T253" id="Seg_6646" s="T252">Ichakichika.[NOM]</ta>
            <ta e="T254" id="Seg_6647" s="T253">deceive-PST.[3SG.S]</ta>
            <ta e="T255" id="Seg_6648" s="T254">go.away-OPT.[3SG.S]</ta>
            <ta e="T256" id="Seg_6649" s="T255">catch-INF</ta>
            <ta e="T257" id="Seg_6650" s="T256">judge-INF</ta>
            <ta e="T258" id="Seg_6651" s="T257">woman-PL.[NOM]-1PL</ta>
            <ta e="T259" id="Seg_6652" s="T258">wife.[NOM]-1SG</ta>
            <ta e="T260" id="Seg_6653" s="T259">woman-PL.[NOM]</ta>
            <ta e="T261" id="Seg_6654" s="T260">all</ta>
            <ta e="T262" id="Seg_6655" s="T261">die-PST-3PL</ta>
            <ta e="T263" id="Seg_6656" s="T262">knife-GEN</ta>
            <ta e="T264" id="Seg_6657" s="T263">from-ADV.EL</ta>
            <ta e="T265" id="Seg_6658" s="T264">you.SG.NOM</ta>
            <ta e="T266" id="Seg_6659" s="T265">though</ta>
            <ta e="T267" id="Seg_6660" s="T266">NEG</ta>
            <ta e="T268" id="Seg_6661" s="T267">right-EP-ADVZ</ta>
            <ta e="T269" id="Seg_6662" s="T268">cut-FRQ-PST.[3SG.S]</ta>
            <ta e="T270" id="Seg_6663" s="T269">(s)he-EP-ACC</ta>
            <ta e="T271" id="Seg_6664" s="T270">leave-INF</ta>
            <ta e="T272" id="Seg_6665" s="T271">one</ta>
            <ta e="T273" id="Seg_6666" s="T272">day.[NOM]</ta>
            <ta e="T274" id="Seg_6667" s="T273">we.PL.NOM</ta>
            <ta e="T275" id="Seg_6668" s="T274">also</ta>
            <ta e="T276" id="Seg_6669" s="T275">try-FRQ-FUT-1PL</ta>
            <ta e="T277" id="Seg_6670" s="T276">we.PL.GEN</ta>
            <ta e="T278" id="Seg_6671" s="T277">woman-PL.[NOM]-1PL</ta>
            <ta e="T279" id="Seg_6672" s="T278">also</ta>
            <ta e="T280" id="Seg_6673" s="T279">lazy.person-TR-3PL</ta>
            <ta e="T281" id="Seg_6674" s="T280">Kolsako.[NOM]</ta>
            <ta e="T282" id="Seg_6675" s="T281">old.man.[NOM]</ta>
            <ta e="T283" id="Seg_6676" s="T282">and</ta>
            <ta e="T284" id="Seg_6677" s="T283">say-PST-3SG.O</ta>
            <ta e="T285" id="Seg_6678" s="T284">all.right</ta>
            <ta e="T286" id="Seg_6679" s="T285">try-FRQ-IMP.2PL</ta>
            <ta e="T287" id="Seg_6680" s="T286">knife-ACC</ta>
            <ta e="T288" id="Seg_6681" s="T287">Cossack-PL.[NOM]</ta>
            <ta e="T289" id="Seg_6682" s="T288">two-3PL</ta>
            <ta e="T290" id="Seg_6683" s="T289">such-ADVZ</ta>
            <ta e="T291" id="Seg_6684" s="T290">cut-MULO-PST-3SG.O</ta>
            <ta e="T292" id="Seg_6685" s="T291">one's.own</ta>
            <ta e="T293" id="Seg_6686" s="T292">wife-PL.[NOM]-3PL</ta>
            <ta e="T294" id="Seg_6687" s="T293">wife-PL.[NOM]-3PL</ta>
            <ta e="T295" id="Seg_6688" s="T294">die-PST-3PL</ta>
            <ta e="T296" id="Seg_6689" s="T295">blood-EP-GEN</ta>
            <ta e="T297" id="Seg_6690" s="T296">between-EP-ADVZ</ta>
            <ta e="T298" id="Seg_6691" s="T297">morning-ADV.LOC</ta>
            <ta e="T299" id="Seg_6692" s="T298">say-PST-3PL</ta>
            <ta e="T300" id="Seg_6693" s="T299">Kolsako.[NOM]</ta>
            <ta e="T301" id="Seg_6694" s="T300">old.man-ALL</ta>
            <ta e="T302" id="Seg_6695" s="T301">we.PL.GEN</ta>
            <ta e="T303" id="Seg_6696" s="T302">wife-PL.[NOM]-1PL</ta>
            <ta e="T304" id="Seg_6697" s="T303">always</ta>
            <ta e="T305" id="Seg_6698" s="T304">die-PST-3PL</ta>
            <ta e="T306" id="Seg_6699" s="T305">old.man.[NOM]</ta>
            <ta e="T307" id="Seg_6700" s="T306">get.angry-DECAUS-PST.[3SG.S]</ta>
            <ta e="T308" id="Seg_6701" s="T307">Ichakichika-ACC</ta>
            <ta e="T309" id="Seg_6702" s="T308">catch-INF</ta>
            <ta e="T310" id="Seg_6703" s="T309">need</ta>
            <ta e="T311" id="Seg_6704" s="T310">Cossack-PL.[NOM]</ta>
            <ta e="T312" id="Seg_6705" s="T311">leave-PST-3PL</ta>
            <ta e="T313" id="Seg_6706" s="T312">Ichakichika.[NOM]</ta>
            <ta e="T314" id="Seg_6707" s="T313">this</ta>
            <ta e="T315" id="Seg_6708" s="T314">time-LOC</ta>
            <ta e="T316" id="Seg_6709" s="T315">house-LOC-OBL.3SG</ta>
            <ta e="T317" id="Seg_6710" s="T316">Ichakichika-ACC</ta>
            <ta e="T318" id="Seg_6711" s="T317">catch-PST-3PL</ta>
            <ta e="T319" id="Seg_6712" s="T318">enough</ta>
            <ta e="T320" id="Seg_6713" s="T319">lies-VBLZ-PST-2SG.S</ta>
            <ta e="T321" id="Seg_6714" s="T320">we.PL.NOM</ta>
            <ta e="T322" id="Seg_6715" s="T321">all</ta>
            <ta e="T323" id="Seg_6716" s="T322">woman-PL.[NOM]-1PL</ta>
            <ta e="T324" id="Seg_6717" s="T323">and</ta>
            <ta e="T325" id="Seg_6718" s="T324">worker-EP-PL.[NOM]</ta>
            <ta e="T326" id="Seg_6719" s="T325">cut-MULO-PST-1PL</ta>
            <ta e="T327" id="Seg_6720" s="T326">(s)he-EP-PL.[NOM]</ta>
            <ta e="T328" id="Seg_6721" s="T327">die-PST-3PL</ta>
            <ta e="T329" id="Seg_6722" s="T328">blood-EP-GEN</ta>
            <ta e="T330" id="Seg_6723" s="T329">between-EP-ADVZ</ta>
            <ta e="T331" id="Seg_6724" s="T330">Ichakichika-ACC</ta>
            <ta e="T332" id="Seg_6725" s="T331">leave-TR-PST-3PL</ta>
            <ta e="T333" id="Seg_6726" s="T332">prison-ILL</ta>
            <ta e="T334" id="Seg_6727" s="T333">Kolsako.[NOM]</ta>
            <ta e="T335" id="Seg_6728" s="T334">old.man.[NOM]</ta>
            <ta e="T336" id="Seg_6729" s="T335">go-CAUS-PST-3SG.O</ta>
            <ta e="T337" id="Seg_6730" s="T336">down</ta>
            <ta e="T338" id="Seg_6731" s="T337">sink-CAUS-INF</ta>
            <ta e="T339" id="Seg_6732" s="T338">water-ILL</ta>
            <ta e="T340" id="Seg_6733" s="T339">say-PST-3SG.O</ta>
            <ta e="T341" id="Seg_6734" s="T340">cow-GEN</ta>
            <ta e="T342" id="Seg_6735" s="T341">skin-ILL</ta>
            <ta e="T343" id="Seg_6736" s="T342">inside-ILL</ta>
            <ta e="T344" id="Seg_6737" s="T343">sew-INF</ta>
            <ta e="T345" id="Seg_6738" s="T344">live-CVB</ta>
            <ta e="T346" id="Seg_6739" s="T345">Cossack-PL.[NOM]</ta>
            <ta e="T347" id="Seg_6740" s="T346">(s)he-EP-ACC</ta>
            <ta e="T348" id="Seg_6741" s="T347">leave-TR-PST-3PL</ta>
            <ta e="T349" id="Seg_6742" s="T348">horse-ADJZ</ta>
            <ta e="T350" id="Seg_6743" s="T349">sledge-INSTR</ta>
            <ta e="T351" id="Seg_6744" s="T350">water.[NOM]</ta>
            <ta e="T352" id="Seg_6745" s="T351">edge-ILL</ta>
            <ta e="T353" id="Seg_6746" s="T352">sea-GEN</ta>
            <ta e="T354" id="Seg_6747" s="T353">bank-ILL</ta>
            <ta e="T355" id="Seg_6748" s="T354">(s)he.[NOM]</ta>
            <ta e="T356" id="Seg_6749" s="T355">sack.[NOM]</ta>
            <ta e="T357" id="Seg_6750" s="T356">in.the.middle-ADV.LOC</ta>
            <ta e="T358" id="Seg_6751" s="T357">live-CVB</ta>
            <ta e="T359" id="Seg_6752" s="T358">lie.[3SG.S]</ta>
            <ta e="T360" id="Seg_6753" s="T359">say-PST-3SG.O</ta>
            <ta e="T361" id="Seg_6754" s="T360">I.ACC</ta>
            <ta e="T362" id="Seg_6755" s="T361">sink-CAUS-IMP.2PL</ta>
            <ta e="T363" id="Seg_6756" s="T362">big</ta>
            <ta e="T364" id="Seg_6757" s="T363">stone-INSTR</ta>
            <ta e="T365" id="Seg_6758" s="T364">big</ta>
            <ta e="T366" id="Seg_6759" s="T365">stone.[NOM]</ta>
            <ta e="T367" id="Seg_6760" s="T366">look.for-MULO-IMP.2PL</ta>
            <ta e="T368" id="Seg_6761" s="T367">(s)he-EP-PL.[NOM]</ta>
            <ta e="T369" id="Seg_6762" s="T368">leave-PST-3PL</ta>
            <ta e="T370" id="Seg_6763" s="T369">look.for-INF</ta>
            <ta e="T371" id="Seg_6764" s="T370">big</ta>
            <ta e="T372" id="Seg_6765" s="T371">stone-ACC</ta>
            <ta e="T373" id="Seg_6766" s="T372">long</ta>
            <ta e="T374" id="Seg_6767" s="T373">whether</ta>
            <ta e="T375" id="Seg_6768" s="T374">short</ta>
            <ta e="T376" id="Seg_6769" s="T375">lie-PST.NAR.[3SG.S]</ta>
            <ta e="T377" id="Seg_6770" s="T376">hear-TR-PST.NAR-3SG.O</ta>
            <ta e="T378" id="Seg_6771" s="T377">horse-ADJZ</ta>
            <ta e="T379" id="Seg_6772" s="T378">sledge.[NOM]</ta>
            <ta e="T380" id="Seg_6773" s="T379">come-IPFV.[3SG.S]</ta>
            <ta e="T381" id="Seg_6774" s="T380">four</ta>
            <ta e="T382" id="Seg_6775" s="T381">whether</ta>
            <ta e="T383" id="Seg_6776" s="T382">five</ta>
            <ta e="T384" id="Seg_6777" s="T383">sledge-PL.[NOM]</ta>
            <ta e="T385" id="Seg_6778" s="T384">come-IPFV.[3SG.S]</ta>
            <ta e="T386" id="Seg_6779" s="T385">stop-TR-PST-3PL</ta>
            <ta e="T387" id="Seg_6780" s="T386">merchant.[NOM]</ta>
            <ta e="T388" id="Seg_6781" s="T387">goods-COM</ta>
            <ta e="T389" id="Seg_6782" s="T388">goods-COM</ta>
            <ta e="T390" id="Seg_6783" s="T389">material-COM</ta>
            <ta e="T391" id="Seg_6784" s="T390">leave-IPFV.[3SG.S]</ta>
            <ta e="T392" id="Seg_6785" s="T391">home</ta>
            <ta e="T393" id="Seg_6786" s="T392">merchant.[NOM]</ta>
            <ta e="T394" id="Seg_6787" s="T393">kick-MOM-PST-3SG.O</ta>
            <ta e="T395" id="Seg_6788" s="T394">cow-GEN</ta>
            <ta e="T396" id="Seg_6789" s="T395">skin-ADJZ</ta>
            <ta e="T397" id="Seg_6790" s="T396">sack-ACC</ta>
            <ta e="T398" id="Seg_6791" s="T397">what</ta>
            <ta e="T399" id="Seg_6792" s="T398">here</ta>
            <ta e="T400" id="Seg_6793" s="T399">lie.[3SG.S]</ta>
            <ta e="T401" id="Seg_6794" s="T400">Ichakichika.[NOM]</ta>
            <ta e="T402" id="Seg_6795" s="T401">cry-INCH-PST.[3SG.S]</ta>
            <ta e="T403" id="Seg_6796" s="T402">oh</ta>
            <ta e="T404" id="Seg_6797" s="T403">merchant.[NOM]</ta>
            <ta e="T405" id="Seg_6798" s="T404">what-TRL</ta>
            <ta e="T406" id="Seg_6799" s="T405">lie-2SG.S</ta>
            <ta e="T407" id="Seg_6800" s="T406">Ichakichika.[NOM]</ta>
            <ta e="T408" id="Seg_6801" s="T407">say-PST-3SG.O</ta>
            <ta e="T409" id="Seg_6802" s="T408">up</ta>
            <ta e="T410" id="Seg_6803" s="T409">I.ACC</ta>
            <ta e="T411" id="Seg_6804" s="T410">let.go-IMP.2SG.S</ta>
            <ta e="T412" id="Seg_6805" s="T411">away</ta>
            <ta e="T413" id="Seg_6806" s="T412">cut-MOM-IMP.2SG.O</ta>
            <ta e="T414" id="Seg_6807" s="T413">(s)he.[NOM]</ta>
            <ta e="T415" id="Seg_6808" s="T414">away</ta>
            <ta e="T416" id="Seg_6809" s="T415">cut-INCH-FRQ-PST-3SG.O</ta>
            <ta e="T417" id="Seg_6810" s="T416">(s)he.[NOM]</ta>
            <ta e="T418" id="Seg_6811" s="T417">up</ta>
            <ta e="T419" id="Seg_6812" s="T418">jump.out-DECAUS-PST.[3SG.S]</ta>
            <ta e="T420" id="Seg_6813" s="T419">go.out-PST.[3SG.S]</ta>
            <ta e="T421" id="Seg_6814" s="T420">(s)he-EP-GEN</ta>
            <ta e="T422" id="Seg_6815" s="T421">something-LOC</ta>
            <ta e="T423" id="Seg_6816" s="T422">sable-GEN</ta>
            <ta e="T424" id="Seg_6817" s="T423">skin-ADJZ</ta>
            <ta e="T425" id="Seg_6818" s="T424">something.[NOM]</ta>
            <ta e="T426" id="Seg_6819" s="T425">be-PST.NAR.[3SG.S]</ta>
            <ta e="T427" id="Seg_6820" s="T426">bosom-ADV.LOC-OBL.3SG</ta>
            <ta e="T428" id="Seg_6821" s="T427">merchant.[NOM]</ta>
            <ta e="T429" id="Seg_6822" s="T428">ask-CO-3SG.O</ta>
            <ta e="T430" id="Seg_6823" s="T429">what-TRL</ta>
            <ta e="T431" id="Seg_6824" s="T430">lie-2SG.S</ta>
            <ta e="T432" id="Seg_6825" s="T431">I.NOM</ta>
            <ta e="T433" id="Seg_6826" s="T432">lie-1SG.O</ta>
            <ta e="T434" id="Seg_6827" s="T433">water-ILL</ta>
            <ta e="T435" id="Seg_6828" s="T434">go.down-IPFV-FRQ-PST-1SG.S</ta>
            <ta e="T436" id="Seg_6829" s="T435">sable-GEN</ta>
            <ta e="T437" id="Seg_6830" s="T436">skin-ACC</ta>
            <ta e="T438" id="Seg_6831" s="T437">take-PST-1SG.O</ta>
            <ta e="T439" id="Seg_6832" s="T438">force.[NOM]-1SG</ta>
            <ta e="T440" id="Seg_6833" s="T439">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T441" id="Seg_6834" s="T440">goods.[NOM]</ta>
            <ta e="T442" id="Seg_6835" s="T441">material.[NOM]</ta>
            <ta e="T443" id="Seg_6836" s="T442">much</ta>
            <ta e="T444" id="Seg_6837" s="T443">water-LOC</ta>
            <ta e="T445" id="Seg_6838" s="T444">up</ta>
            <ta e="T446" id="Seg_6839" s="T445">be.visible-RES-PST-3SG.O</ta>
            <ta e="T447" id="Seg_6840" s="T446">sable-GEN</ta>
            <ta e="T448" id="Seg_6841" s="T447">skin-PL-EP-ACC</ta>
            <ta e="T449" id="Seg_6842" s="T448">merchant-ALL</ta>
            <ta e="T450" id="Seg_6843" s="T449">merchant.[NOM]</ta>
            <ta e="T451" id="Seg_6844" s="T450">happiness-TR-PST.[3SG.S]</ta>
            <ta e="T452" id="Seg_6845" s="T451">I.NOM</ta>
            <ta e="T453" id="Seg_6846" s="T452">also</ta>
            <ta e="T454" id="Seg_6847" s="T453">go.down-IPFV-FRQ-OPT-1SG.S</ta>
            <ta e="T455" id="Seg_6848" s="T454">water-ILL</ta>
            <ta e="T456" id="Seg_6849" s="T455">(s)he-EP-ACC</ta>
            <ta e="T457" id="Seg_6850" s="T456">sew-PFV-PST-3SG.O</ta>
            <ta e="T458" id="Seg_6851" s="T457">cow-GEN</ta>
            <ta e="T459" id="Seg_6852" s="T458">sack-ILL</ta>
            <ta e="T460" id="Seg_6853" s="T459">merchant.[NOM]</ta>
            <ta e="T461" id="Seg_6854" s="T460">lie.[3SG.S]</ta>
            <ta e="T753" id="Seg_6855" s="T461">water</ta>
            <ta e="T462" id="Seg_6856" s="T753">%%</ta>
            <ta e="T463" id="Seg_6857" s="T462">sack-LOC</ta>
            <ta e="T464" id="Seg_6858" s="T463">Ichakichika.[NOM]</ta>
            <ta e="T465" id="Seg_6859" s="T464">leave-PFV-PST.[3SG.S]</ta>
            <ta e="T466" id="Seg_6860" s="T465">horse-ADJZ</ta>
            <ta e="T467" id="Seg_6861" s="T466">sledge-OBL.3SG-COM</ta>
            <ta e="T468" id="Seg_6862" s="T467">and</ta>
            <ta e="T469" id="Seg_6863" s="T468">goods-COM</ta>
            <ta e="T470" id="Seg_6864" s="T469">Cossack-PL.[NOM]</ta>
            <ta e="T471" id="Seg_6865" s="T470">bring-PST-3PL</ta>
            <ta e="T472" id="Seg_6866" s="T471">big</ta>
            <ta e="T473" id="Seg_6867" s="T472">stone-ACC</ta>
            <ta e="T474" id="Seg_6868" s="T473">bind-INF-be.going.to-PST-3PL</ta>
            <ta e="T475" id="Seg_6869" s="T474">%%-3PL</ta>
            <ta e="T476" id="Seg_6870" s="T475">himself</ta>
            <ta e="T477" id="Seg_6871" s="T476">together</ta>
            <ta e="T478" id="Seg_6872" s="T477">enough</ta>
            <ta e="T479" id="Seg_6873" s="T478">Ichakichika.[NOM]</ta>
            <ta e="T480" id="Seg_6874" s="T479">deceive-EP-FRQ-PST-2SG.O</ta>
            <ta e="T481" id="Seg_6875" s="T480">water-LOC</ta>
            <ta e="T482" id="Seg_6876" s="T481">die-FUT-2SG.S</ta>
            <ta e="T483" id="Seg_6877" s="T482">merchant.[NOM]</ta>
            <ta e="T484" id="Seg_6878" s="T483">hear-TR-PST.NAR-3SG.O</ta>
            <ta e="T485" id="Seg_6879" s="T484">bind-INF-try-HAB-3PL</ta>
            <ta e="T486" id="Seg_6880" s="T485">stone-ACC</ta>
            <ta e="T487" id="Seg_6881" s="T486">big</ta>
            <ta e="T488" id="Seg_6882" s="T487">stone-ACC</ta>
            <ta e="T489" id="Seg_6883" s="T488">NEG</ta>
            <ta e="T490" id="Seg_6884" s="T489">I.NOM</ta>
            <ta e="T491" id="Seg_6885" s="T490">Ichakichika.[NOM]</ta>
            <ta e="T492" id="Seg_6886" s="T491">Ichakichika.[NOM]</ta>
            <ta e="T493" id="Seg_6887" s="T492">leave-PST.[3SG.S]</ta>
            <ta e="T494" id="Seg_6888" s="T493">this</ta>
            <ta e="T495" id="Seg_6889" s="T494">I.ACC</ta>
            <ta e="T496" id="Seg_6890" s="T495">deceive-PST.[3SG.S]</ta>
            <ta e="T497" id="Seg_6891" s="T496">I.NOM</ta>
            <ta e="T498" id="Seg_6892" s="T497">merchant-EP-CO-1SG.S</ta>
            <ta e="T499" id="Seg_6893" s="T498">deceive-CVB</ta>
            <ta e="T500" id="Seg_6894" s="T499">I.ACC</ta>
            <ta e="T501" id="Seg_6895" s="T500">sew-PST.[3SG.S]</ta>
            <ta e="T502" id="Seg_6896" s="T501">skin-ADJZ</ta>
            <ta e="T503" id="Seg_6897" s="T502">sack-ILL</ta>
            <ta e="T504" id="Seg_6898" s="T503">Cossack-PL.[NOM]</ta>
            <ta e="T505" id="Seg_6899" s="T504">speak-CO-3PL</ta>
            <ta e="T506" id="Seg_6900" s="T505">you.SG.NOM</ta>
            <ta e="T507" id="Seg_6901" s="T506">Ichakichika-2SG.S</ta>
            <ta e="T508" id="Seg_6902" s="T507">enough</ta>
            <ta e="T509" id="Seg_6903" s="T508">lies-TR-PST-2SG.S</ta>
            <ta e="T510" id="Seg_6904" s="T509">that</ta>
            <ta e="T511" id="Seg_6905" s="T510">cry-CVB</ta>
            <ta e="T512" id="Seg_6906" s="T511">cry-VBLZ-CO.[3SG.S]</ta>
            <ta e="T513" id="Seg_6907" s="T512">sack-LOC</ta>
            <ta e="T514" id="Seg_6908" s="T513">I.NOM</ta>
            <ta e="T515" id="Seg_6909" s="T514">merchant-EP-CO-1SG.S</ta>
            <ta e="T516" id="Seg_6910" s="T515">come-IPFV-PST-1SG.S</ta>
            <ta e="T517" id="Seg_6911" s="T516">horse-INSTR</ta>
            <ta e="T518" id="Seg_6912" s="T517">sledge-INSTR</ta>
            <ta e="T519" id="Seg_6913" s="T518">water-ILL</ta>
            <ta e="T520" id="Seg_6914" s="T519">bring-TR-CVB</ta>
            <ta e="T521" id="Seg_6915" s="T520">throw-PST-3PL</ta>
            <ta e="T522" id="Seg_6916" s="T521">sink-RES-PST-3PL</ta>
            <ta e="T523" id="Seg_6917" s="T522">stone-COM</ta>
            <ta e="T524" id="Seg_6918" s="T523">home</ta>
            <ta e="T525" id="Seg_6919" s="T524">leave-PST-3PL</ta>
            <ta e="T526" id="Seg_6920" s="T525">we.PL.NOM</ta>
            <ta e="T527" id="Seg_6921" s="T526">sink-CAUS-PST-1PL</ta>
            <ta e="T528" id="Seg_6922" s="T527">water-ILL</ta>
            <ta e="T529" id="Seg_6923" s="T528">Ichakichika.[NOM]</ta>
            <ta e="T530" id="Seg_6924" s="T529">come-PST.[3SG.S]</ta>
            <ta e="T531" id="Seg_6925" s="T530">home</ta>
            <ta e="T532" id="Seg_6926" s="T531">horse-INSTR</ta>
            <ta e="T533" id="Seg_6927" s="T532">horse-ADJZ</ta>
            <ta e="T534" id="Seg_6928" s="T533">sledge-INSTR</ta>
            <ta e="T535" id="Seg_6929" s="T534">goods.[NOM]-3SG</ta>
            <ta e="T536" id="Seg_6930" s="T535">water-INSTR</ta>
            <ta e="T537" id="Seg_6931" s="T536">middle-CAUS-PST-3SG.O</ta>
            <ta e="T538" id="Seg_6932" s="T537">rope-ILL</ta>
            <ta e="T539" id="Seg_6933" s="T538">up</ta>
            <ta e="T540" id="Seg_6934" s="T539">hang-MULO-PST-3SG.O</ta>
            <ta e="T541" id="Seg_6935" s="T540">dry-INF</ta>
            <ta e="T542" id="Seg_6936" s="T541">this</ta>
            <ta e="T543" id="Seg_6937" s="T542">time-LOC</ta>
            <ta e="T544" id="Seg_6938" s="T543">head.[NOM]</ta>
            <ta e="T545" id="Seg_6939" s="T544">Kolsako.[NOM]</ta>
            <ta e="T546" id="Seg_6940" s="T545">old.man.[NOM]</ta>
            <ta e="T547" id="Seg_6941" s="T546">give.a.look-HAB.[3SG.S]</ta>
            <ta e="T548" id="Seg_6942" s="T547">Ichakichika-GEN</ta>
            <ta e="T549" id="Seg_6943" s="T548">tent-EP-ADJZ</ta>
            <ta e="T550" id="Seg_6944" s="T549">side-ILL</ta>
            <ta e="T551" id="Seg_6945" s="T550">oven.[NOM]</ta>
            <ta e="T552" id="Seg_6946" s="T551">set.fire-HAB-3SG.O</ta>
            <ta e="T553" id="Seg_6947" s="T552">smoke.[NOM]</ta>
            <ta e="T554" id="Seg_6948" s="T553">fill.with.smoke.[3SG.S]</ta>
            <ta e="T555" id="Seg_6949" s="T554">oven.GEN</ta>
            <ta e="T556" id="Seg_6950" s="T555">from-ADV.EL</ta>
            <ta e="T557" id="Seg_6951" s="T556">here-COM</ta>
            <ta e="T558" id="Seg_6952" s="T557">here-COM</ta>
            <ta e="T559" id="Seg_6953" s="T558">say-PST-3SG.O</ta>
            <ta e="T560" id="Seg_6954" s="T559">Cossack-EP-DU-ALL</ta>
            <ta e="T561" id="Seg_6955" s="T560">go.away-OPT.[3SG.S]</ta>
            <ta e="T562" id="Seg_6956" s="T561">give.a.look-HAB-INF</ta>
            <ta e="T563" id="Seg_6957" s="T562">old.woman-ACC</ta>
            <ta e="T564" id="Seg_6958" s="T563">(s)he-EP-PL.[NOM]</ta>
            <ta e="T565" id="Seg_6959" s="T564">go.away-PST-3PL</ta>
            <ta e="T566" id="Seg_6960" s="T565">tent-ILL</ta>
            <ta e="T567" id="Seg_6961" s="T566">come.in-PST-3PL</ta>
            <ta e="T568" id="Seg_6962" s="T567">old.woman-GEN</ta>
            <ta e="T569" id="Seg_6963" s="T568">place.[NOM]</ta>
            <ta e="T570" id="Seg_6964" s="T569">instead.of</ta>
            <ta e="T571" id="Seg_6965" s="T570">sit.down.[3SG.S]</ta>
            <ta e="T572" id="Seg_6966" s="T571">table-EP-GEN</ta>
            <ta e="T573" id="Seg_6967" s="T572">on-ADV.LOC</ta>
            <ta e="T574" id="Seg_6968" s="T573">Ichakichika.[NOM]</ta>
            <ta e="T575" id="Seg_6969" s="T574">get.angry-DECAUS-HAB.[3SG.S]</ta>
            <ta e="T576" id="Seg_6970" s="T575">all</ta>
            <ta e="T577" id="Seg_6971" s="T576">face.[NOM]-3SG</ta>
            <ta e="T578" id="Seg_6972" s="T577">scratch-HAB-3SG.O</ta>
            <ta e="T579" id="Seg_6973" s="T578">blood-%%</ta>
            <ta e="T580" id="Seg_6974" s="T579">clothing-ACC-3SG</ta>
            <ta e="T581" id="Seg_6975" s="T580">tear-PTCP.PST-CO.[3SG.S]</ta>
            <ta e="T582" id="Seg_6976" s="T581">(s)he-EP-PL.[NOM]</ta>
            <ta e="T583" id="Seg_6977" s="T582">say-PST-3PL</ta>
            <ta e="T584" id="Seg_6978" s="T583">come.in-PST-3PL</ta>
            <ta e="T585" id="Seg_6979" s="T584">hi</ta>
            <ta e="T586" id="Seg_6980" s="T585">friend.[NOM]</ta>
            <ta e="T587" id="Seg_6981" s="T586">but</ta>
            <ta e="T588" id="Seg_6982" s="T587">Ichakichika.[NOM]</ta>
            <ta e="T589" id="Seg_6983" s="T588">get.angry-DECAUS-CVB</ta>
            <ta e="T590" id="Seg_6984" s="T589">sit.down.[3SG.S]</ta>
            <ta e="T591" id="Seg_6985" s="T590">you.PL.NOM</ta>
            <ta e="T592" id="Seg_6986" s="T591">I.ACC</ta>
            <ta e="T593" id="Seg_6987" s="T592">water-%%</ta>
            <ta e="T594" id="Seg_6988" s="T593">sink-CAUS-PST-2PL</ta>
            <ta e="T595" id="Seg_6989" s="T594">I.NOM</ta>
            <ta e="T596" id="Seg_6990" s="T595">sea-GEN</ta>
            <ta e="T597" id="Seg_6991" s="T596">below</ta>
            <ta e="T598" id="Seg_6992" s="T597">much-ADVZ</ta>
            <ta e="T599" id="Seg_6993" s="T598">goods.[NOM]</ta>
            <ta e="T600" id="Seg_6994" s="T599">material.[NOM]</ta>
            <ta e="T601" id="Seg_6995" s="T600">find-PST-1SG.O</ta>
            <ta e="T602" id="Seg_6996" s="T601">so.much</ta>
            <ta e="T603" id="Seg_6997" s="T602">be-CO.[3SG.S]</ta>
            <ta e="T604" id="Seg_6998" s="T603">how.many</ta>
            <ta e="T605" id="Seg_6999" s="T604">force.[NOM]-1SG</ta>
            <ta e="T606" id="Seg_7000" s="T605">be-PST.[3SG.S]</ta>
            <ta e="T607" id="Seg_7001" s="T606">take-PST-1SG.O</ta>
            <ta e="T608" id="Seg_7002" s="T607">I.GEN</ta>
            <ta e="T609" id="Seg_7003" s="T608">force.[NOM]-1SG</ta>
            <ta e="T610" id="Seg_7004" s="T609">NEG</ta>
            <ta e="T611" id="Seg_7005" s="T610">take-PST-3SG.O</ta>
            <ta e="T612" id="Seg_7006" s="T611">as.if</ta>
            <ta e="T613" id="Seg_7007" s="T612">water-%%-EP-ADJZ</ta>
            <ta e="T614" id="Seg_7008" s="T613">god-LOC</ta>
            <ta e="T615" id="Seg_7009" s="T614">be-PST-1SG.S</ta>
            <ta e="T616" id="Seg_7010" s="T615">(s)he-EP-PL.[NOM]</ta>
            <ta e="T617" id="Seg_7011" s="T616">leave-PST-3PL</ta>
            <ta e="T618" id="Seg_7012" s="T617">be.frightened-DECAUS-CVB</ta>
            <ta e="T619" id="Seg_7013" s="T618">what-ADJZ</ta>
            <ta e="T620" id="Seg_7014" s="T619">devil.[NOM]</ta>
            <ta e="T621" id="Seg_7015" s="T620">be-PST.[3SG.S]</ta>
            <ta e="T622" id="Seg_7016" s="T621">Kolsako.[NOM]</ta>
            <ta e="T623" id="Seg_7017" s="T622">old.man-ALL</ta>
            <ta e="T624" id="Seg_7018" s="T623">say-INF</ta>
            <ta e="T625" id="Seg_7019" s="T624">Ichakichika-ACC</ta>
            <ta e="T626" id="Seg_7020" s="T625">water-ILL</ta>
            <ta e="T627" id="Seg_7021" s="T626">sink-CAUS-PST-1PL</ta>
            <ta e="T629" id="Seg_7022" s="T628">day.[NOM]</ta>
            <ta e="T630" id="Seg_7023" s="T629">earlier</ta>
            <ta e="T631" id="Seg_7024" s="T630">(s)he.[NOM]</ta>
            <ta e="T632" id="Seg_7025" s="T631">numerous</ta>
            <ta e="T633" id="Seg_7026" s="T632">find-PST.NAR-3SG.O</ta>
            <ta e="T634" id="Seg_7027" s="T633">goods.[NOM]</ta>
            <ta e="T635" id="Seg_7028" s="T634">material.[NOM]</ta>
            <ta e="T636" id="Seg_7029" s="T635">interrog.pron.stem-ADJZ</ta>
            <ta e="T637" id="Seg_7030" s="T636">belongings.[NOM]</ta>
            <ta e="T638" id="Seg_7031" s="T637">all</ta>
            <ta e="T639" id="Seg_7032" s="T638">find-PST.NAR-3SG.O</ta>
            <ta e="T640" id="Seg_7033" s="T639">and</ta>
            <ta e="T641" id="Seg_7034" s="T640">sable-GEN</ta>
            <ta e="T642" id="Seg_7035" s="T641">skin-ADJZ</ta>
            <ta e="T643" id="Seg_7036" s="T642">something-PL.[NOM]</ta>
            <ta e="T644" id="Seg_7037" s="T643">Kolsako.[NOM]</ta>
            <ta e="T645" id="Seg_7038" s="T644">old.man.[NOM]</ta>
            <ta e="T646" id="Seg_7039" s="T645">mind.[NOM]-3SG</ta>
            <ta e="T647" id="Seg_7040" s="T646">get.lost-PST.[3SG.S]</ta>
            <ta e="T648" id="Seg_7041" s="T647">how</ta>
            <ta e="T649" id="Seg_7042" s="T648">such-ADVZ</ta>
            <ta e="T650" id="Seg_7043" s="T649">become-PST.[3SG.S]</ta>
            <ta e="T651" id="Seg_7044" s="T650">Ichakichika.[NOM]</ta>
            <ta e="T652" id="Seg_7045" s="T651">devil.[NOM]</ta>
            <ta e="T655" id="Seg_7046" s="T654">new-ADJZ</ta>
            <ta e="T656" id="Seg_7047" s="T655">day.[NOM]</ta>
            <ta e="T657" id="Seg_7048" s="T656">himself</ta>
            <ta e="T658" id="Seg_7049" s="T657">two-PL</ta>
            <ta e="T659" id="Seg_7050" s="T658">Cossack-PL.[NOM]</ta>
            <ta e="T660" id="Seg_7051" s="T659">come-%%-3PL</ta>
            <ta e="T661" id="Seg_7052" s="T660">Ichakichika-ILL</ta>
            <ta e="T662" id="Seg_7053" s="T661">sent-3SG.O</ta>
            <ta e="T663" id="Seg_7054" s="T662">Cossack.[NOM]</ta>
            <ta e="T664" id="Seg_7055" s="T663">old.man.[NOM]</ta>
            <ta e="T665" id="Seg_7056" s="T664">ask-INF</ta>
            <ta e="T666" id="Seg_7057" s="T665">Ichakichika-ACC</ta>
            <ta e="T667" id="Seg_7058" s="T666">how</ta>
            <ta e="T668" id="Seg_7059" s="T667">find-PST-3SG.O</ta>
            <ta e="T669" id="Seg_7060" s="T668">this</ta>
            <ta e="T670" id="Seg_7061" s="T669">that</ta>
            <ta e="T671" id="Seg_7062" s="T670">material-PL-EP-ACC</ta>
            <ta e="T672" id="Seg_7063" s="T671">we.PL.NOM</ta>
            <ta e="T673" id="Seg_7064" s="T672">also</ta>
            <ta e="T674" id="Seg_7065" s="T673">water-ILL</ta>
            <ta e="T675" id="Seg_7066" s="T674">dive-DRV-FUT-1PL</ta>
            <ta e="T676" id="Seg_7067" s="T675">goods-EP-TRL</ta>
            <ta e="T677" id="Seg_7068" s="T676">Ichakichika.[NOM]</ta>
            <ta e="T678" id="Seg_7069" s="T677">say-PST-3SG.O</ta>
            <ta e="T679" id="Seg_7070" s="T678">sew-%%-IMP.2PL</ta>
            <ta e="T680" id="Seg_7071" s="T679">sew-MULO-CO-3PL</ta>
            <ta e="T681" id="Seg_7072" s="T680">sew-MULO-CO-2PL</ta>
            <ta e="T682" id="Seg_7073" s="T681">cow-GEN</ta>
            <ta e="T683" id="Seg_7074" s="T682">skin.GEN</ta>
            <ta e="T684" id="Seg_7075" s="T683">out-ADV.EL</ta>
            <ta e="T685" id="Seg_7076" s="T684">sack-PL-EP-ACC</ta>
            <ta e="T686" id="Seg_7077" s="T685">sew-IMP.2PL</ta>
            <ta e="T687" id="Seg_7078" s="T686">sack-PL-EP-ACC</ta>
            <ta e="T688" id="Seg_7079" s="T687">cow-GEN</ta>
            <ta e="T689" id="Seg_7080" s="T688">skin-ADJZ</ta>
            <ta e="T690" id="Seg_7081" s="T689">lie.down-IMP.2PL</ta>
            <ta e="T691" id="Seg_7082" s="T690">sea-GEN</ta>
            <ta e="T692" id="Seg_7083" s="T691">bank-ILL</ta>
            <ta e="T693" id="Seg_7084" s="T692">I.NOM</ta>
            <ta e="T694" id="Seg_7085" s="T693">you.PL.ACC</ta>
            <ta e="T695" id="Seg_7086" s="T694">sew-FUT-1SG.O</ta>
            <ta e="T696" id="Seg_7087" s="T695">sack-ILL</ta>
            <ta e="T697" id="Seg_7088" s="T696">throw-FUT-1SG.O</ta>
            <ta e="T698" id="Seg_7089" s="T697">water-ILL</ta>
            <ta e="T699" id="Seg_7090" s="T698">stone-INSTR</ta>
            <ta e="T700" id="Seg_7091" s="T699">big</ta>
            <ta e="T701" id="Seg_7092" s="T700">stone-INSTR</ta>
            <ta e="T702" id="Seg_7093" s="T701">you.PL.NOM</ta>
            <ta e="T703" id="Seg_7094" s="T702">find-FUT-2PL</ta>
            <ta e="T704" id="Seg_7095" s="T703">all</ta>
            <ta e="T705" id="Seg_7096" s="T704">goods.[NOM]</ta>
            <ta e="T706" id="Seg_7097" s="T705">earlier</ta>
            <ta e="T707" id="Seg_7098" s="T706">Kolsako.[NOM]</ta>
            <ta e="T708" id="Seg_7099" s="T707">old.man.[NOM]</ta>
            <ta e="T709" id="Seg_7100" s="T708">head.[NOM]</ta>
            <ta e="T710" id="Seg_7101" s="T709">who.[NOM]</ta>
            <ta e="T711" id="Seg_7102" s="T710">want.[3SG.S]</ta>
            <ta e="T712" id="Seg_7103" s="T711">town-EL</ta>
            <ta e="T713" id="Seg_7104" s="T712">(s)he.[NOM]</ta>
            <ta e="T714" id="Seg_7105" s="T713">sew-%%-PST-3SG.O</ta>
            <ta e="T715" id="Seg_7106" s="T714">earlier</ta>
            <ta e="T716" id="Seg_7107" s="T715">Kolsako.[NOM]</ta>
            <ta e="T717" id="Seg_7108" s="T716">old.man-ACC</ta>
            <ta e="T718" id="Seg_7109" s="T717">then</ta>
            <ta e="T719" id="Seg_7110" s="T718">foreign</ta>
            <ta e="T720" id="Seg_7111" s="T719">human.being-PL-ACC-3SG</ta>
            <ta e="T721" id="Seg_7112" s="T720">Cossack-PL-EP-ACC</ta>
            <ta e="T722" id="Seg_7113" s="T721">then</ta>
            <ta e="T723" id="Seg_7114" s="T722">Ichakichika.[NOM]</ta>
            <ta e="T724" id="Seg_7115" s="T723">himself</ta>
            <ta e="T725" id="Seg_7116" s="T724">throw-INF-be.going.to-PST-3SG.O</ta>
            <ta e="T726" id="Seg_7117" s="T725">sew-EP-PTCP.PST</ta>
            <ta e="T727" id="Seg_7118" s="T726">sack-PL-EP-ACC</ta>
            <ta e="T728" id="Seg_7119" s="T727">human.being-EP-PL-COM</ta>
            <ta e="T729" id="Seg_7120" s="T728">water-ILL</ta>
            <ta e="T730" id="Seg_7121" s="T729">say-PST-3SG.O</ta>
            <ta e="T731" id="Seg_7122" s="T730">Kolsako.[NOM]</ta>
            <ta e="T732" id="Seg_7123" s="T731">old.man-ALL</ta>
            <ta e="T733" id="Seg_7124" s="T732">look.for-IMP.2SG.O</ta>
            <ta e="T734" id="Seg_7125" s="T733">goods.[NOM]</ta>
            <ta e="T735" id="Seg_7126" s="T734">sea-GEN</ta>
            <ta e="T736" id="Seg_7127" s="T735">below</ta>
            <ta e="T737" id="Seg_7128" s="T736">NEG</ta>
            <ta e="T738" id="Seg_7129" s="T737">where</ta>
            <ta e="T739" id="Seg_7130" s="T738">NEG</ta>
            <ta e="T740" id="Seg_7131" s="T739">find-FUT-2SG.O</ta>
            <ta e="T741" id="Seg_7132" s="T740">goods.[NOM]-EP-3SG</ta>
            <ta e="T742" id="Seg_7133" s="T741">and</ta>
            <ta e="T743" id="Seg_7134" s="T742">goods-EP-PL.[NOM]</ta>
            <ta e="T744" id="Seg_7135" s="T743">always</ta>
            <ta e="T745" id="Seg_7136" s="T744">die-FUT-2SG.S</ta>
            <ta e="T746" id="Seg_7137" s="T745">you.PL.NOM</ta>
            <ta e="T747" id="Seg_7138" s="T746">here</ta>
            <ta e="T748" id="Seg_7139" s="T747">I.ACC</ta>
            <ta e="T749" id="Seg_7140" s="T748">throw-DUR-PST-2PL</ta>
            <ta e="T750" id="Seg_7141" s="T749">water-ILL</ta>
            <ta e="T751" id="Seg_7142" s="T750">I.ACC</ta>
            <ta e="T752" id="Seg_7143" s="T751">throw-PST-2PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_7144" s="T0">Кольсако.[NOM]</ta>
            <ta e="T2" id="Seg_7145" s="T1">старик.[NOM]</ta>
            <ta e="T3" id="Seg_7146" s="T2">жить-PST.NAR.[3SG.S]</ta>
            <ta e="T4" id="Seg_7147" s="T3">Ичакичика.[NOM]</ta>
            <ta e="T5" id="Seg_7148" s="T4">бабушка-OBL.3SG-COM</ta>
            <ta e="T6" id="Seg_7149" s="T5">Кольсако.[NOM]</ta>
            <ta e="T7" id="Seg_7150" s="T6">старик-ADJZ</ta>
            <ta e="T8" id="Seg_7151" s="T7">город.[NOM]</ta>
            <ta e="T9" id="Seg_7152" s="T8">NEG</ta>
            <ta e="T10" id="Seg_7153" s="T9">далеко</ta>
            <ta e="T11" id="Seg_7154" s="T10">быть-PST.[3SG.S]</ta>
            <ta e="T12" id="Seg_7155" s="T11">один</ta>
            <ta e="T13" id="Seg_7156" s="T12">середина-LOC</ta>
            <ta e="T14" id="Seg_7157" s="T13">Кольсако.[NOM]</ta>
            <ta e="T15" id="Seg_7158" s="T14">старик-ADJZ</ta>
            <ta e="T16" id="Seg_7159" s="T15">корова.[NOM]-3SG</ta>
            <ta e="T17" id="Seg_7160" s="T16">потеряться.[3SG.S]</ta>
            <ta e="T18" id="Seg_7161" s="T17">потеряться-PST.[3SG.S]</ta>
            <ta e="T20" id="Seg_7162" s="T19">потеряться-DUR.[3SG.S]</ta>
            <ta e="T21" id="Seg_7163" s="T20">он(а).[NOM]</ta>
            <ta e="T22" id="Seg_7164" s="T21">знать-3SG.O</ta>
            <ta e="T23" id="Seg_7165" s="T22">что</ta>
            <ta e="T24" id="Seg_7166" s="T23">Ичакичика.[NOM]-3SG</ta>
            <ta e="T25" id="Seg_7167" s="T24">украсть-PST-3SG.O</ta>
            <ta e="T26" id="Seg_7168" s="T25">послать-3SG.O</ta>
            <ta e="T27" id="Seg_7169" s="T26">он(а)-EP-ALL</ta>
            <ta e="T28" id="Seg_7170" s="T27">казак-PL-EP-ACC</ta>
            <ta e="T29" id="Seg_7171" s="T28">два-PL-EP-ACC</ta>
            <ta e="T30" id="Seg_7172" s="T29">взглянуть-HAB-INF</ta>
            <ta e="T31" id="Seg_7173" s="T30">что-EP-ACC</ta>
            <ta e="T32" id="Seg_7174" s="T31">сделать-INFER-3SG.O</ta>
            <ta e="T33" id="Seg_7175" s="T32">Ичакичика.[NOM]</ta>
            <ta e="T34" id="Seg_7176" s="T33">он(а).[NOM]</ta>
            <ta e="T35" id="Seg_7177" s="T34">знать-3SG.O</ta>
            <ta e="T36" id="Seg_7178" s="T35">прийти-FUT-3PL</ta>
            <ta e="T37" id="Seg_7179" s="T36">ружье-COM</ta>
            <ta e="T38" id="Seg_7180" s="T37">ружьё-OBL.3SG-COM</ta>
            <ta e="T39" id="Seg_7181" s="T38">он(а)-EP-ACC</ta>
            <ta e="T40" id="Seg_7182" s="T39">схватить-FUT-3PL</ta>
            <ta e="T41" id="Seg_7183" s="T40">сесть-TR-FUT-3PL</ta>
            <ta e="T42" id="Seg_7184" s="T41">тюрьма-ILL</ta>
            <ta e="T43" id="Seg_7185" s="T42">этот</ta>
            <ta e="T44" id="Seg_7186" s="T43">время-LOC</ta>
            <ta e="T45" id="Seg_7187" s="T44">этот</ta>
            <ta e="T46" id="Seg_7188" s="T45">день.[NOM]</ta>
            <ta e="T47" id="Seg_7189" s="T46">корова-ACC</ta>
            <ta e="T48" id="Seg_7190" s="T47">убить-PST-3SG.O</ta>
            <ta e="T49" id="Seg_7191" s="T48">кишка-ACC-3SG</ta>
            <ta e="T50" id="Seg_7192" s="T49">кровь-INSTR</ta>
            <ta e="T51" id="Seg_7193" s="T50">вылить-PST-3SG.O</ta>
            <ta e="T52" id="Seg_7194" s="T51">держать-PST-3SG.O</ta>
            <ta e="T53" id="Seg_7195" s="T52">большой</ta>
            <ta e="T54" id="Seg_7196" s="T53">нож-ACC</ta>
            <ta e="T55" id="Seg_7197" s="T54">чум-EP.GEN</ta>
            <ta e="T56" id="Seg_7198" s="T55">%%-LOC</ta>
            <ta e="T57" id="Seg_7199" s="T56">бабушка-ACC-3SG</ta>
            <ta e="T58" id="Seg_7200" s="T57">завернуть-PST-3SG.O</ta>
            <ta e="T59" id="Seg_7201" s="T58">кровь-EP-ADJZ</ta>
            <ta e="T60" id="Seg_7202" s="T59">корова-GEN</ta>
            <ta e="T61" id="Seg_7203" s="T60">кишка-INSTR</ta>
            <ta e="T62" id="Seg_7204" s="T61">бабушка.[NOM]-3SG</ta>
            <ta e="T63" id="Seg_7205" s="T62">надеть-TR-PST-3SG.O</ta>
            <ta e="T64" id="Seg_7206" s="T63">одежда-PL.[NOM]</ta>
            <ta e="T65" id="Seg_7207" s="T64">казак-PL.[NOM]</ta>
            <ta e="T66" id="Seg_7208" s="T65">казак-EP-PL.[NOM]</ta>
            <ta e="T67" id="Seg_7209" s="T66">прийти-PST-3PL</ta>
            <ta e="T68" id="Seg_7210" s="T67">Ичакичика-ALL</ta>
            <ta e="T69" id="Seg_7211" s="T68">большой</ta>
            <ta e="T70" id="Seg_7212" s="T69">человек.[NOM]</ta>
            <ta e="T71" id="Seg_7213" s="T70">сказать-PST-3SG.O</ta>
            <ta e="T72" id="Seg_7214" s="T71">Ичакичика-ALL</ta>
            <ta e="T73" id="Seg_7215" s="T72">ты.ACC</ta>
            <ta e="T74" id="Seg_7216" s="T73">сесть-TR-INF</ta>
            <ta e="T75" id="Seg_7217" s="T74">нужно</ta>
            <ta e="T76" id="Seg_7218" s="T75">тюрьма-ILL</ta>
            <ta e="T77" id="Seg_7219" s="T76">ты.NOM</ta>
            <ta e="T78" id="Seg_7220" s="T77">корова-PL-EP-ACC</ta>
            <ta e="T79" id="Seg_7221" s="T78">убить-HAB-2SG.O</ta>
            <ta e="T80" id="Seg_7222" s="T79">съесть-CO-2SG.O</ta>
            <ta e="T81" id="Seg_7223" s="T80">он(а).[NOM]</ta>
            <ta e="T82" id="Seg_7224" s="T81">сердиться-DECAUS-PST.[3SG.S]</ta>
            <ta e="T83" id="Seg_7225" s="T82">бабушка-OBL.3SG-ALL</ta>
            <ta e="T84" id="Seg_7226" s="T83">сказать-PST-3SG.O</ta>
            <ta e="T85" id="Seg_7227" s="T84">я.ACC</ta>
            <ta e="T86" id="Seg_7228" s="T85">сесть-CAUS-FUT-3PL</ta>
            <ta e="T87" id="Seg_7229" s="T86">тюрьма-ILL</ta>
            <ta e="T88" id="Seg_7230" s="T87">чайник.[NOM]</ta>
            <ta e="T89" id="Seg_7231" s="T88">свариться-TR-IMP.2SG.O</ta>
            <ta e="T90" id="Seg_7232" s="T89">спешить-CVB</ta>
            <ta e="T91" id="Seg_7233" s="T90">бабушка.[NOM]-3SG</ta>
            <ta e="T92" id="Seg_7234" s="T91">ругаться-INCH-CO.[3SG.S]</ta>
            <ta e="T93" id="Seg_7235" s="T92">нога.[NOM]-1SG</ta>
            <ta e="T94" id="Seg_7236" s="T93">рука.[NOM]-1SG</ta>
            <ta e="T95" id="Seg_7237" s="T94">болеть.[3SG.S]</ta>
            <ta e="T96" id="Seg_7238" s="T95">казак-PL.[NOM]</ta>
            <ta e="T97" id="Seg_7239" s="T96">стоять-CO-3PL</ta>
            <ta e="T98" id="Seg_7240" s="T97">дверь-GEN</ta>
            <ta e="T99" id="Seg_7241" s="T98">отверстие-LOC</ta>
            <ta e="T100" id="Seg_7242" s="T99">Ичакичика.[NOM]</ta>
            <ta e="T101" id="Seg_7243" s="T100">бабушка-OBL.3SG-ALL</ta>
            <ta e="T102" id="Seg_7244" s="T101">ты.NOM</ta>
            <ta e="T103" id="Seg_7245" s="T102">лентяй-CO-2SG.S</ta>
            <ta e="T104" id="Seg_7246" s="T103">чайник.[NOM]</ta>
            <ta e="T105" id="Seg_7247" s="T104">NEG</ta>
            <ta e="T106" id="Seg_7248" s="T105">хотеть-2SG.S</ta>
            <ta e="T107" id="Seg_7249" s="T106">свариться-CAUS-INF</ta>
            <ta e="T108" id="Seg_7250" s="T107">сердиться-DECAUS-PST.[3SG.S]</ta>
            <ta e="T109" id="Seg_7251" s="T108">Ичакичика.[NOM]</ta>
            <ta e="T110" id="Seg_7252" s="T109">нож-ACC-3SG</ta>
            <ta e="T111" id="Seg_7253" s="T110">вниз</ta>
            <ta e="T112" id="Seg_7254" s="T111">взять-PST-3SG.O</ta>
            <ta e="T113" id="Seg_7255" s="T112">бабушка-ACC-3SG</ta>
            <ta e="T114" id="Seg_7256" s="T113">резать-FRQ-PST-3SG.O</ta>
            <ta e="T115" id="Seg_7257" s="T114">бабушка.[NOM]-3SG</ta>
            <ta e="T116" id="Seg_7258" s="T115">умереть-PFV-PST.[3SG.S]</ta>
            <ta e="T117" id="Seg_7259" s="T116">потом</ta>
            <ta e="T118" id="Seg_7260" s="T117">Ичакичика.[NOM]</ta>
            <ta e="T119" id="Seg_7261" s="T118">сказать-PST-3SG.O</ta>
            <ta e="T120" id="Seg_7262" s="T119">казак-PL-ALL</ta>
            <ta e="T121" id="Seg_7263" s="T120">бабушка.[NOM]-1SG</ta>
            <ta e="T122" id="Seg_7264" s="T121">всегда</ta>
            <ta e="T123" id="Seg_7265" s="T122">такой-ADVZ</ta>
            <ta e="T124" id="Seg_7266" s="T123">сила-VBLZ-CO.[3SG.S]</ta>
            <ta e="T125" id="Seg_7267" s="T124">нож.[NOM]-3SG</ta>
            <ta e="T126" id="Seg_7268" s="T125">нога-LOC-OBL.3SG</ta>
            <ta e="T127" id="Seg_7269" s="T126">стоять.торчком-CAUS-PST-3SG.O</ta>
            <ta e="T128" id="Seg_7270" s="T127">бабушка.[NOM]</ta>
            <ta e="T129" id="Seg_7271" s="T128">вверх</ta>
            <ta e="T130" id="Seg_7272" s="T129">прыгнуть-IMP.2SG.S</ta>
            <ta e="T131" id="Seg_7273" s="T130">нож.[NOM]-1SG</ta>
            <ta e="T132" id="Seg_7274" s="T131">жить-TR-FUT.[3SG.S]</ta>
            <ta e="T133" id="Seg_7275" s="T132">потом</ta>
            <ta e="T134" id="Seg_7276" s="T133">бабушка.[NOM]-3SG</ta>
            <ta e="T135" id="Seg_7277" s="T134">встать-PFV-PST.[3SG.S]</ta>
            <ta e="T136" id="Seg_7278" s="T135">спешить-CVB</ta>
            <ta e="T137" id="Seg_7279" s="T136">схватить-PST-3SG.O</ta>
            <ta e="T138" id="Seg_7280" s="T137">чайник.ACC-3SG</ta>
            <ta e="T139" id="Seg_7281" s="T138">свариться-CAUS-INF</ta>
            <ta e="T140" id="Seg_7282" s="T139">трубка-ACC</ta>
            <ta e="T141" id="Seg_7283" s="T140">курить-MULO-ACTN-GEN</ta>
            <ta e="T142" id="Seg_7284" s="T141">в.течение</ta>
            <ta e="T143" id="Seg_7285" s="T142">чайник.[NOM]</ta>
            <ta e="T144" id="Seg_7286" s="T143">свариться-PFV-PST.[3SG.S]</ta>
            <ta e="T145" id="Seg_7287" s="T144">хлеб-EP-ACC</ta>
            <ta e="T146" id="Seg_7288" s="T145">еда-ACC</ta>
            <ta e="T147" id="Seg_7289" s="T146">положить-PST-3SG.O</ta>
            <ta e="T148" id="Seg_7290" s="T147">столик.в.чуме-ILL</ta>
            <ta e="T149" id="Seg_7291" s="T148">спешить-CVB</ta>
            <ta e="T150" id="Seg_7292" s="T149">казак-PL.[NOM]</ta>
            <ta e="T151" id="Seg_7293" s="T150">съесть-EP-FRQ-INF-собраться-PST-3PL</ta>
            <ta e="T152" id="Seg_7294" s="T151">он(а)-EP-PL.[NOM]</ta>
            <ta e="T153" id="Seg_7295" s="T152">такой-ADVZ</ta>
            <ta e="T154" id="Seg_7296" s="T153">сказать-PST-3PL</ta>
            <ta e="T155" id="Seg_7297" s="T154">мы.PL.NOM</ta>
            <ta e="T156" id="Seg_7298" s="T155">ты.GEN</ta>
            <ta e="T157" id="Seg_7299" s="T156">нож-ACC</ta>
            <ta e="T158" id="Seg_7300" s="T157">купить-FUT-1PL</ta>
            <ta e="T159" id="Seg_7301" s="T158">мы.PL.GEN</ta>
            <ta e="T160" id="Seg_7302" s="T159">жена-PL.[NOM]-1PL</ta>
            <ta e="T161" id="Seg_7303" s="T160">лентяй-TR-3PL</ta>
            <ta e="T162" id="Seg_7304" s="T161">и</ta>
            <ta e="T163" id="Seg_7305" s="T162">спать-3PL</ta>
            <ta e="T164" id="Seg_7306" s="T163">казак-PL.[NOM]</ta>
            <ta e="T165" id="Seg_7307" s="T164">NEG</ta>
            <ta e="T166" id="Seg_7308" s="T165">двигаться-CAUS-INF-собраться-PST-3PL</ta>
            <ta e="T167" id="Seg_7309" s="T166">Ичакичика-ACC</ta>
            <ta e="T168" id="Seg_7310" s="T167">сказать-FUT-3PL</ta>
            <ta e="T169" id="Seg_7311" s="T168">сказать-PST-3PL</ta>
            <ta e="T170" id="Seg_7312" s="T169">Кольсако.[NOM]</ta>
            <ta e="T171" id="Seg_7313" s="T170">старик-ALL</ta>
            <ta e="T172" id="Seg_7314" s="T171">убить-PST-3PL</ta>
            <ta e="T173" id="Seg_7315" s="T172">вот</ta>
            <ta e="T174" id="Seg_7316" s="T173">сказать-PST.NAR-3PL</ta>
            <ta e="T175" id="Seg_7317" s="T174">начальник.[NOM]</ta>
            <ta e="T176" id="Seg_7318" s="T175">идти-CAUS-PST-3SG.O</ta>
            <ta e="T177" id="Seg_7319" s="T176">купить-INF</ta>
            <ta e="T178" id="Seg_7320" s="T177">нож-ACC</ta>
            <ta e="T179" id="Seg_7321" s="T178">следующий</ta>
            <ta e="T180" id="Seg_7322" s="T179">день.[NOM]</ta>
            <ta e="T181" id="Seg_7323" s="T180">купить-3PL</ta>
            <ta e="T182" id="Seg_7324" s="T181">Кольсако.[NOM]</ta>
            <ta e="T183" id="Seg_7325" s="T182">старик.[NOM]</ta>
            <ta e="T184" id="Seg_7326" s="T183">сказать-PST-3SG.O</ta>
            <ta e="T185" id="Seg_7327" s="T184">нож-ACC</ta>
            <ta e="T186" id="Seg_7328" s="T185">я.DAT</ta>
            <ta e="T187" id="Seg_7329" s="T186">дать-IMP.2PL</ta>
            <ta e="T188" id="Seg_7330" s="T187">я.NOM</ta>
            <ta e="T189" id="Seg_7331" s="T188">ночевать-TR-FUT-1SG.O</ta>
            <ta e="T190" id="Seg_7332" s="T189">я.GEN</ta>
            <ta e="T191" id="Seg_7333" s="T190">жена.[NOM]-1SG</ta>
            <ta e="T192" id="Seg_7334" s="T191">и</ta>
            <ta e="T193" id="Seg_7335" s="T192">работник-EP-PL.[NOM]</ta>
            <ta e="T194" id="Seg_7336" s="T193">лентяй-TR-3PL</ta>
            <ta e="T195" id="Seg_7337" s="T194">день-PL.[NOM]</ta>
            <ta e="T196" id="Seg_7338" s="T195">и</ta>
            <ta e="T197" id="Seg_7339" s="T196">ночь-PL.[NOM]</ta>
            <ta e="T198" id="Seg_7340" s="T197">спать-3PL</ta>
            <ta e="T199" id="Seg_7341" s="T198">Ичакичика.[NOM]</ta>
            <ta e="T200" id="Seg_7342" s="T199">сказать-PST.NAR-3SG.O</ta>
            <ta e="T201" id="Seg_7343" s="T200">сказать-PST-3SG.O</ta>
            <ta e="T202" id="Seg_7344" s="T201">нож-ACC</ta>
            <ta e="T203" id="Seg_7345" s="T202">чисто-ADVZ</ta>
            <ta e="T204" id="Seg_7346" s="T203">держать-IMP.2PL</ta>
            <ta e="T205" id="Seg_7347" s="T204">хороший</ta>
            <ta e="T206" id="Seg_7348" s="T205">человек-EP-ACC</ta>
            <ta e="T207" id="Seg_7349" s="T206">резать-FRQ-INF</ta>
            <ta e="T208" id="Seg_7350" s="T207">Кольсако.[NOM]</ta>
            <ta e="T209" id="Seg_7351" s="T208">старик.[NOM]</ta>
            <ta e="T210" id="Seg_7352" s="T209">спать-PST.[3SG.S]</ta>
            <ta e="T211" id="Seg_7353" s="T210">утро-ADV.LOC</ta>
            <ta e="T212" id="Seg_7354" s="T211">скоро</ta>
            <ta e="T213" id="Seg_7355" s="T212">сесть-PST.[3SG.S]</ta>
            <ta e="T214" id="Seg_7356" s="T213">встать-PST.[3SG.S]</ta>
            <ta e="T215" id="Seg_7357" s="T214">жена.[NOM]</ta>
            <ta e="T216" id="Seg_7358" s="T215">сесть-IMP.2SG.S</ta>
            <ta e="T217" id="Seg_7359" s="T216">и</ta>
            <ta e="T218" id="Seg_7360" s="T217">работник-EP-PL.[NOM]</ta>
            <ta e="T219" id="Seg_7361" s="T218">сесть-IMP.2PL</ta>
            <ta e="T220" id="Seg_7362" s="T219">он(а)-EP-PL.[NOM]</ta>
            <ta e="T221" id="Seg_7363" s="T220">скоро</ta>
            <ta e="T222" id="Seg_7364" s="T221">NEG</ta>
            <ta e="T223" id="Seg_7365" s="T222">сесть-IPFV-3PL</ta>
            <ta e="T224" id="Seg_7366" s="T223">сердиться-DECAUS-PST.[3SG.S]</ta>
            <ta e="T225" id="Seg_7367" s="T224">вниз</ta>
            <ta e="T226" id="Seg_7368" s="T225">взять-PST-3SG.O</ta>
            <ta e="T227" id="Seg_7369" s="T226">нож-ACC-3SG</ta>
            <ta e="T228" id="Seg_7370" s="T227">резать-MULO-PST-3SG.O</ta>
            <ta e="T229" id="Seg_7371" s="T228">всё</ta>
            <ta e="T230" id="Seg_7372" s="T229">нож-ACC</ta>
            <ta e="T231" id="Seg_7373" s="T230">стоять.торчком-CAUS-PST-3SG.O</ta>
            <ta e="T232" id="Seg_7374" s="T231">нога-ILL-OBL.3SG</ta>
            <ta e="T233" id="Seg_7375" s="T232">всё</ta>
            <ta e="T234" id="Seg_7376" s="T233">резать-FRQ-PTCP.PST</ta>
            <ta e="T235" id="Seg_7377" s="T234">человек-PL-ILL-OBL.3SG</ta>
            <ta e="T236" id="Seg_7378" s="T235">только</ta>
            <ta e="T237" id="Seg_7379" s="T236">сказать-DUR-3SG.O</ta>
            <ta e="T238" id="Seg_7380" s="T237">всегда</ta>
            <ta e="T239" id="Seg_7381" s="T238">умереть-PFV-3PL</ta>
            <ta e="T240" id="Seg_7382" s="T239">старик.[NOM]</ta>
            <ta e="T241" id="Seg_7383" s="T240">сердиться-DECAUS-PST.[3SG.S]</ta>
            <ta e="T242" id="Seg_7384" s="T241">казак-PL-EP-ACC</ta>
            <ta e="T243" id="Seg_7385" s="T242">позвать-PST-3SG.O</ta>
            <ta e="T244" id="Seg_7386" s="T243">где</ta>
            <ta e="T245" id="Seg_7387" s="T244">взять-PST-%%</ta>
            <ta e="T246" id="Seg_7388" s="T245">нож-ACC</ta>
            <ta e="T247" id="Seg_7389" s="T246">он(а).[NOM]</ta>
            <ta e="T248" id="Seg_7390" s="T247">Ичакичика.[NOM]</ta>
            <ta e="T249" id="Seg_7391" s="T248">we.PL-ALL</ta>
            <ta e="T250" id="Seg_7392" s="T249">нож-ACC</ta>
            <ta e="T251" id="Seg_7393" s="T250">дать-PST-3SG.O</ta>
            <ta e="T252" id="Seg_7394" s="T251">он(а).[NOM]</ta>
            <ta e="T253" id="Seg_7395" s="T252">Ичакичика.[NOM]</ta>
            <ta e="T254" id="Seg_7396" s="T253">обманывать-PST.[3SG.S]</ta>
            <ta e="T255" id="Seg_7397" s="T254">уйти-OPT.[3SG.S]</ta>
            <ta e="T256" id="Seg_7398" s="T255">схватить-INF</ta>
            <ta e="T257" id="Seg_7399" s="T256">судить-INF</ta>
            <ta e="T258" id="Seg_7400" s="T257">женщина-PL.[NOM]-1PL</ta>
            <ta e="T259" id="Seg_7401" s="T258">жена.[NOM]-1SG</ta>
            <ta e="T260" id="Seg_7402" s="T259">женщина-PL.[NOM]</ta>
            <ta e="T261" id="Seg_7403" s="T260">всё</ta>
            <ta e="T262" id="Seg_7404" s="T261">умереть-PST-3PL</ta>
            <ta e="T263" id="Seg_7405" s="T262">нож-GEN</ta>
            <ta e="T264" id="Seg_7406" s="T263">от-ADV.EL</ta>
            <ta e="T265" id="Seg_7407" s="T264">ты.NOM</ta>
            <ta e="T266" id="Seg_7408" s="T265">однако</ta>
            <ta e="T267" id="Seg_7409" s="T266">NEG</ta>
            <ta e="T268" id="Seg_7410" s="T267">правильный-EP-ADVZ</ta>
            <ta e="T269" id="Seg_7411" s="T268">резать-FRQ-PST.[3SG.S]</ta>
            <ta e="T270" id="Seg_7412" s="T269">он(а)-EP-ACC</ta>
            <ta e="T271" id="Seg_7413" s="T270">оставить-INF</ta>
            <ta e="T272" id="Seg_7414" s="T271">один</ta>
            <ta e="T273" id="Seg_7415" s="T272">день.[NOM]</ta>
            <ta e="T274" id="Seg_7416" s="T273">мы.PL.NOM</ta>
            <ta e="T275" id="Seg_7417" s="T274">тоже</ta>
            <ta e="T276" id="Seg_7418" s="T275">пробовать-FRQ-FUT-1PL</ta>
            <ta e="T277" id="Seg_7419" s="T276">мы.PL.GEN</ta>
            <ta e="T278" id="Seg_7420" s="T277">женщина-PL.[NOM]-1PL</ta>
            <ta e="T279" id="Seg_7421" s="T278">тоже</ta>
            <ta e="T280" id="Seg_7422" s="T279">лентяй-TR-3PL</ta>
            <ta e="T281" id="Seg_7423" s="T280">Кольсако.[NOM]</ta>
            <ta e="T282" id="Seg_7424" s="T281">старик.[NOM]</ta>
            <ta e="T283" id="Seg_7425" s="T282">и</ta>
            <ta e="T284" id="Seg_7426" s="T283">сказать-PST-3SG.O</ta>
            <ta e="T285" id="Seg_7427" s="T284">ладно</ta>
            <ta e="T286" id="Seg_7428" s="T285">пробовать-FRQ-IMP.2PL</ta>
            <ta e="T287" id="Seg_7429" s="T286">нож-ACC</ta>
            <ta e="T288" id="Seg_7430" s="T287">казак-PL.[NOM]</ta>
            <ta e="T289" id="Seg_7431" s="T288">два-3PL</ta>
            <ta e="T290" id="Seg_7432" s="T289">такой-ADVZ</ta>
            <ta e="T291" id="Seg_7433" s="T290">резать-MULO-PST-3SG.O</ta>
            <ta e="T292" id="Seg_7434" s="T291">свой</ta>
            <ta e="T293" id="Seg_7435" s="T292">жена-PL.[NOM]-3PL</ta>
            <ta e="T294" id="Seg_7436" s="T293">жена-PL.[NOM]-3PL</ta>
            <ta e="T295" id="Seg_7437" s="T294">умереть-PST-3PL</ta>
            <ta e="T296" id="Seg_7438" s="T295">кровь-EP-GEN</ta>
            <ta e="T297" id="Seg_7439" s="T296">между-EP-ADVZ</ta>
            <ta e="T298" id="Seg_7440" s="T297">утро-ADV.LOC</ta>
            <ta e="T299" id="Seg_7441" s="T298">сказать-PST-3PL</ta>
            <ta e="T300" id="Seg_7442" s="T299">Кольсако.[NOM]</ta>
            <ta e="T301" id="Seg_7443" s="T300">старик-ALL</ta>
            <ta e="T302" id="Seg_7444" s="T301">мы.PL.GEN</ta>
            <ta e="T303" id="Seg_7445" s="T302">жена-PL.[NOM]-1PL</ta>
            <ta e="T304" id="Seg_7446" s="T303">всегда</ta>
            <ta e="T305" id="Seg_7447" s="T304">умереть-PST-3PL</ta>
            <ta e="T306" id="Seg_7448" s="T305">старик.[NOM]</ta>
            <ta e="T307" id="Seg_7449" s="T306">сердиться-DECAUS-PST.[3SG.S]</ta>
            <ta e="T308" id="Seg_7450" s="T307">Ичакичика-ACC</ta>
            <ta e="T309" id="Seg_7451" s="T308">схватить-INF</ta>
            <ta e="T310" id="Seg_7452" s="T309">нужно</ta>
            <ta e="T311" id="Seg_7453" s="T310">казак-PL.[NOM]</ta>
            <ta e="T312" id="Seg_7454" s="T311">отправиться-PST-3PL</ta>
            <ta e="T313" id="Seg_7455" s="T312">Ичакичика.[NOM]</ta>
            <ta e="T314" id="Seg_7456" s="T313">этот</ta>
            <ta e="T315" id="Seg_7457" s="T314">время-LOC</ta>
            <ta e="T316" id="Seg_7458" s="T315">дом-LOC-OBL.3SG</ta>
            <ta e="T317" id="Seg_7459" s="T316">Ичакичика-ACC</ta>
            <ta e="T318" id="Seg_7460" s="T317">схватить-PST-3PL</ta>
            <ta e="T319" id="Seg_7461" s="T318">достаточно</ta>
            <ta e="T320" id="Seg_7462" s="T319">ложь-VBLZ-PST-2SG.S</ta>
            <ta e="T321" id="Seg_7463" s="T320">мы.PL.NOM</ta>
            <ta e="T322" id="Seg_7464" s="T321">всё</ta>
            <ta e="T323" id="Seg_7465" s="T322">женщина-PL.[NOM]-1PL</ta>
            <ta e="T324" id="Seg_7466" s="T323">и</ta>
            <ta e="T325" id="Seg_7467" s="T324">работник-EP-PL.[NOM]</ta>
            <ta e="T326" id="Seg_7468" s="T325">резать-MULO-PST-1PL</ta>
            <ta e="T327" id="Seg_7469" s="T326">он(а)-EP-PL.[NOM]</ta>
            <ta e="T328" id="Seg_7470" s="T327">умереть-PST-3PL</ta>
            <ta e="T329" id="Seg_7471" s="T328">кровь-EP-GEN</ta>
            <ta e="T330" id="Seg_7472" s="T329">между-EP-ADVZ</ta>
            <ta e="T331" id="Seg_7473" s="T330">Ичакичика-ACC</ta>
            <ta e="T332" id="Seg_7474" s="T331">отправиться-TR-PST-3PL</ta>
            <ta e="T333" id="Seg_7475" s="T332">тюрьма-ILL</ta>
            <ta e="T334" id="Seg_7476" s="T333">Кольсако.[NOM]</ta>
            <ta e="T335" id="Seg_7477" s="T334">старик.[NOM]</ta>
            <ta e="T336" id="Seg_7478" s="T335">идти-CAUS-PST-3SG.O</ta>
            <ta e="T337" id="Seg_7479" s="T336">вниз</ta>
            <ta e="T338" id="Seg_7480" s="T337">погрузиться.в.воду-CAUS-INF</ta>
            <ta e="T339" id="Seg_7481" s="T338">вода-ILL</ta>
            <ta e="T340" id="Seg_7482" s="T339">сказать-PST-3SG.O</ta>
            <ta e="T341" id="Seg_7483" s="T340">корова-GEN</ta>
            <ta e="T342" id="Seg_7484" s="T341">шкура-ILL</ta>
            <ta e="T343" id="Seg_7485" s="T342">нутро-ILL</ta>
            <ta e="T344" id="Seg_7486" s="T343">сшить-INF</ta>
            <ta e="T345" id="Seg_7487" s="T344">жить-CVB</ta>
            <ta e="T346" id="Seg_7488" s="T345">казак-PL.[NOM]</ta>
            <ta e="T347" id="Seg_7489" s="T346">он(а)-EP-ACC</ta>
            <ta e="T348" id="Seg_7490" s="T347">отправиться-TR-PST-3PL</ta>
            <ta e="T349" id="Seg_7491" s="T348">лошадь-ADJZ</ta>
            <ta e="T350" id="Seg_7492" s="T349">нарты-INSTR</ta>
            <ta e="T351" id="Seg_7493" s="T350">вода.[NOM]</ta>
            <ta e="T352" id="Seg_7494" s="T351">край-ILL</ta>
            <ta e="T353" id="Seg_7495" s="T352">море-GEN</ta>
            <ta e="T354" id="Seg_7496" s="T353">берег-ILL</ta>
            <ta e="T355" id="Seg_7497" s="T354">он(а).[NOM]</ta>
            <ta e="T356" id="Seg_7498" s="T355">мешок.[NOM]</ta>
            <ta e="T357" id="Seg_7499" s="T356">посередине-ADV.LOC</ta>
            <ta e="T358" id="Seg_7500" s="T357">жить-CVB</ta>
            <ta e="T359" id="Seg_7501" s="T358">лежать.[3SG.S]</ta>
            <ta e="T360" id="Seg_7502" s="T359">сказать-PST-3SG.O</ta>
            <ta e="T361" id="Seg_7503" s="T360">я.ACC</ta>
            <ta e="T362" id="Seg_7504" s="T361">погрузиться.в.воду-CAUS-IMP.2PL</ta>
            <ta e="T363" id="Seg_7505" s="T362">большой</ta>
            <ta e="T364" id="Seg_7506" s="T363">камень-INSTR</ta>
            <ta e="T365" id="Seg_7507" s="T364">большой</ta>
            <ta e="T366" id="Seg_7508" s="T365">камень.[NOM]</ta>
            <ta e="T367" id="Seg_7509" s="T366">искать-MULO-IMP.2PL</ta>
            <ta e="T368" id="Seg_7510" s="T367">он(а)-EP-PL.[NOM]</ta>
            <ta e="T369" id="Seg_7511" s="T368">отправиться-PST-3PL</ta>
            <ta e="T370" id="Seg_7512" s="T369">искать-INF</ta>
            <ta e="T371" id="Seg_7513" s="T370">большой</ta>
            <ta e="T372" id="Seg_7514" s="T371">камень-ACC</ta>
            <ta e="T373" id="Seg_7515" s="T372">долго</ta>
            <ta e="T374" id="Seg_7516" s="T373">ли</ta>
            <ta e="T375" id="Seg_7517" s="T374">короткий</ta>
            <ta e="T376" id="Seg_7518" s="T375">лежать-PST.NAR.[3SG.S]</ta>
            <ta e="T377" id="Seg_7519" s="T376">слушать-TR-PST.NAR-3SG.O</ta>
            <ta e="T378" id="Seg_7520" s="T377">лошадь-ADJZ</ta>
            <ta e="T379" id="Seg_7521" s="T378">нарты.[NOM]</ta>
            <ta e="T380" id="Seg_7522" s="T379">прийти-IPFV.[3SG.S]</ta>
            <ta e="T381" id="Seg_7523" s="T380">четыре</ta>
            <ta e="T382" id="Seg_7524" s="T381">ли</ta>
            <ta e="T383" id="Seg_7525" s="T382">пять</ta>
            <ta e="T384" id="Seg_7526" s="T383">нарты-PL.[NOM]</ta>
            <ta e="T385" id="Seg_7527" s="T384">прийти-IPFV.[3SG.S]</ta>
            <ta e="T386" id="Seg_7528" s="T385">остановиться-TR-PST-3PL</ta>
            <ta e="T387" id="Seg_7529" s="T386">купец.[NOM]</ta>
            <ta e="T388" id="Seg_7530" s="T387">товар-COM</ta>
            <ta e="T389" id="Seg_7531" s="T388">товар-COM</ta>
            <ta e="T390" id="Seg_7532" s="T389">материя-COM</ta>
            <ta e="T391" id="Seg_7533" s="T390">отправиться-IPFV.[3SG.S]</ta>
            <ta e="T392" id="Seg_7534" s="T391">домой</ta>
            <ta e="T393" id="Seg_7535" s="T392">купец.[NOM]</ta>
            <ta e="T394" id="Seg_7536" s="T393">пинать-MOM-PST-3SG.O</ta>
            <ta e="T395" id="Seg_7537" s="T394">корова-GEN</ta>
            <ta e="T396" id="Seg_7538" s="T395">шкура-ADJZ</ta>
            <ta e="T397" id="Seg_7539" s="T396">мешок-ACC</ta>
            <ta e="T398" id="Seg_7540" s="T397">что</ta>
            <ta e="T399" id="Seg_7541" s="T398">здесь</ta>
            <ta e="T400" id="Seg_7542" s="T399">лежать.[3SG.S]</ta>
            <ta e="T401" id="Seg_7543" s="T400">Ичакичика.[NOM]</ta>
            <ta e="T402" id="Seg_7544" s="T401">кричать-INCH-PST.[3SG.S]</ta>
            <ta e="T403" id="Seg_7545" s="T402">ой</ta>
            <ta e="T404" id="Seg_7546" s="T403">купец.[NOM]</ta>
            <ta e="T405" id="Seg_7547" s="T404">что-TRL</ta>
            <ta e="T406" id="Seg_7548" s="T405">лежать-2SG.S</ta>
            <ta e="T407" id="Seg_7549" s="T406">Ичакичика.[NOM]</ta>
            <ta e="T408" id="Seg_7550" s="T407">сказать-PST-3SG.O</ta>
            <ta e="T409" id="Seg_7551" s="T408">вверх</ta>
            <ta e="T410" id="Seg_7552" s="T409">я.ACC</ta>
            <ta e="T411" id="Seg_7553" s="T410">пустить-IMP.2SG.S</ta>
            <ta e="T412" id="Seg_7554" s="T411">прочь</ta>
            <ta e="T413" id="Seg_7555" s="T412">отрезать-MOM-IMP.2SG.O</ta>
            <ta e="T414" id="Seg_7556" s="T413">он(а).[NOM]</ta>
            <ta e="T415" id="Seg_7557" s="T414">прочь</ta>
            <ta e="T416" id="Seg_7558" s="T415">отрезать-INCH-FRQ-PST-3SG.O</ta>
            <ta e="T417" id="Seg_7559" s="T416">он(а).[NOM]</ta>
            <ta e="T418" id="Seg_7560" s="T417">вверх</ta>
            <ta e="T419" id="Seg_7561" s="T418">выскочить-DECAUS-PST.[3SG.S]</ta>
            <ta e="T420" id="Seg_7562" s="T419">выйти-PST.[3SG.S]</ta>
            <ta e="T421" id="Seg_7563" s="T420">он(а)-EP-GEN</ta>
            <ta e="T422" id="Seg_7564" s="T421">нечто-LOC</ta>
            <ta e="T423" id="Seg_7565" s="T422">соболь-GEN</ta>
            <ta e="T424" id="Seg_7566" s="T423">шкура-ADJZ</ta>
            <ta e="T425" id="Seg_7567" s="T424">нечто.[NOM]</ta>
            <ta e="T426" id="Seg_7568" s="T425">быть-PST.NAR.[3SG.S]</ta>
            <ta e="T427" id="Seg_7569" s="T426">пазуха-ADV.LOC-OBL.3SG</ta>
            <ta e="T428" id="Seg_7570" s="T427">купец.[NOM]</ta>
            <ta e="T429" id="Seg_7571" s="T428">спросить-CO-3SG.O</ta>
            <ta e="T430" id="Seg_7572" s="T429">что-TRL</ta>
            <ta e="T431" id="Seg_7573" s="T430">лежать-2SG.S</ta>
            <ta e="T432" id="Seg_7574" s="T431">я.NOM</ta>
            <ta e="T433" id="Seg_7575" s="T432">лежать-1SG.O</ta>
            <ta e="T434" id="Seg_7576" s="T433">вода-ILL</ta>
            <ta e="T435" id="Seg_7577" s="T434">залезть-IPFV-FRQ-PST-1SG.S</ta>
            <ta e="T436" id="Seg_7578" s="T435">соболь-GEN</ta>
            <ta e="T437" id="Seg_7579" s="T436">шкура-ACC</ta>
            <ta e="T438" id="Seg_7580" s="T437">взять-PST-1SG.O</ta>
            <ta e="T439" id="Seg_7581" s="T438">сила.[NOM]-1SG</ta>
            <ta e="T440" id="Seg_7582" s="T439">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T441" id="Seg_7583" s="T440">товар.[NOM]</ta>
            <ta e="T442" id="Seg_7584" s="T441">материя.[NOM]</ta>
            <ta e="T443" id="Seg_7585" s="T442">много</ta>
            <ta e="T444" id="Seg_7586" s="T443">вода-LOC</ta>
            <ta e="T445" id="Seg_7587" s="T444">вверх</ta>
            <ta e="T446" id="Seg_7588" s="T445">виднеться-RES-PST-3SG.O</ta>
            <ta e="T447" id="Seg_7589" s="T446">соболь-GEN</ta>
            <ta e="T448" id="Seg_7590" s="T447">шкура-PL-EP-ACC</ta>
            <ta e="T449" id="Seg_7591" s="T448">купец-ALL</ta>
            <ta e="T450" id="Seg_7592" s="T449">купец.[NOM]</ta>
            <ta e="T451" id="Seg_7593" s="T450">радость-TR-PST.[3SG.S]</ta>
            <ta e="T452" id="Seg_7594" s="T451">я.NOM</ta>
            <ta e="T453" id="Seg_7595" s="T452">тоже</ta>
            <ta e="T454" id="Seg_7596" s="T453">залезть-IPFV-FRQ-OPT-1SG.S</ta>
            <ta e="T455" id="Seg_7597" s="T454">вода-ILL</ta>
            <ta e="T456" id="Seg_7598" s="T455">он(а)-EP-ACC</ta>
            <ta e="T457" id="Seg_7599" s="T456">сшить-PFV-PST-3SG.O</ta>
            <ta e="T458" id="Seg_7600" s="T457">корова-GEN</ta>
            <ta e="T459" id="Seg_7601" s="T458">мешок-ILL</ta>
            <ta e="T460" id="Seg_7602" s="T459">купец.[NOM]</ta>
            <ta e="T461" id="Seg_7603" s="T460">лежать.[3SG.S]</ta>
            <ta e="T753" id="Seg_7604" s="T461">вода</ta>
            <ta e="T462" id="Seg_7605" s="T753">%%</ta>
            <ta e="T463" id="Seg_7606" s="T462">мешок-LOC</ta>
            <ta e="T464" id="Seg_7607" s="T463">Ичакичика.[NOM]</ta>
            <ta e="T465" id="Seg_7608" s="T464">отправиться-PFV-PST.[3SG.S]</ta>
            <ta e="T466" id="Seg_7609" s="T465">лошадь-ADJZ</ta>
            <ta e="T467" id="Seg_7610" s="T466">нарты-OBL.3SG-COM</ta>
            <ta e="T468" id="Seg_7611" s="T467">и</ta>
            <ta e="T469" id="Seg_7612" s="T468">товар-COM</ta>
            <ta e="T470" id="Seg_7613" s="T469">казак-PL.[NOM]</ta>
            <ta e="T471" id="Seg_7614" s="T470">принести-PST-3PL</ta>
            <ta e="T472" id="Seg_7615" s="T471">большой</ta>
            <ta e="T473" id="Seg_7616" s="T472">камень-ACC</ta>
            <ta e="T474" id="Seg_7617" s="T473">привязать-INF-собраться-PST-3PL</ta>
            <ta e="T475" id="Seg_7618" s="T474">%%-3PL</ta>
            <ta e="T476" id="Seg_7619" s="T475">он.сам</ta>
            <ta e="T477" id="Seg_7620" s="T476">вместе</ta>
            <ta e="T478" id="Seg_7621" s="T477">достаточно</ta>
            <ta e="T479" id="Seg_7622" s="T478">Ичакичика.[NOM]</ta>
            <ta e="T480" id="Seg_7623" s="T479">обманывать-EP-FRQ-PST-2SG.O</ta>
            <ta e="T481" id="Seg_7624" s="T480">вода-LOC</ta>
            <ta e="T482" id="Seg_7625" s="T481">умереть-FUT-2SG.S</ta>
            <ta e="T483" id="Seg_7626" s="T482">купец.[NOM]</ta>
            <ta e="T484" id="Seg_7627" s="T483">слушать-TR-PST.NAR-3SG.O</ta>
            <ta e="T485" id="Seg_7628" s="T484">привязать-INF-попытаться-HAB-3PL</ta>
            <ta e="T486" id="Seg_7629" s="T485">камень-ACC</ta>
            <ta e="T487" id="Seg_7630" s="T486">большой</ta>
            <ta e="T488" id="Seg_7631" s="T487">камень-ACC</ta>
            <ta e="T489" id="Seg_7632" s="T488">NEG</ta>
            <ta e="T490" id="Seg_7633" s="T489">я.NOM</ta>
            <ta e="T491" id="Seg_7634" s="T490">Ичакичика.[NOM]</ta>
            <ta e="T492" id="Seg_7635" s="T491">Ичакичика.[NOM]</ta>
            <ta e="T493" id="Seg_7636" s="T492">отправиться-PST.[3SG.S]</ta>
            <ta e="T494" id="Seg_7637" s="T493">этот</ta>
            <ta e="T495" id="Seg_7638" s="T494">я.ACC</ta>
            <ta e="T496" id="Seg_7639" s="T495">обманывать-PST.[3SG.S]</ta>
            <ta e="T497" id="Seg_7640" s="T496">я.NOM</ta>
            <ta e="T498" id="Seg_7641" s="T497">купец-EP-CO-1SG.S</ta>
            <ta e="T499" id="Seg_7642" s="T498">обманывать-CVB</ta>
            <ta e="T500" id="Seg_7643" s="T499">я.ACC</ta>
            <ta e="T501" id="Seg_7644" s="T500">сшить-PST.[3SG.S]</ta>
            <ta e="T502" id="Seg_7645" s="T501">шкура-ADJZ</ta>
            <ta e="T503" id="Seg_7646" s="T502">мешок-ILL</ta>
            <ta e="T504" id="Seg_7647" s="T503">казак-PL.[NOM]</ta>
            <ta e="T505" id="Seg_7648" s="T504">сказать-CO-3PL</ta>
            <ta e="T506" id="Seg_7649" s="T505">ты.NOM</ta>
            <ta e="T507" id="Seg_7650" s="T506">Ичакичика-2SG.S</ta>
            <ta e="T508" id="Seg_7651" s="T507">достаточно</ta>
            <ta e="T509" id="Seg_7652" s="T508">ложь-TR-PST-2SG.S</ta>
            <ta e="T510" id="Seg_7653" s="T509">тот</ta>
            <ta e="T511" id="Seg_7654" s="T510">плакать-CVB</ta>
            <ta e="T512" id="Seg_7655" s="T511">крик-VBLZ-CO.[3SG.S]</ta>
            <ta e="T513" id="Seg_7656" s="T512">мешок-LOC</ta>
            <ta e="T514" id="Seg_7657" s="T513">я.NOM</ta>
            <ta e="T515" id="Seg_7658" s="T514">купец-EP-CO-1SG.S</ta>
            <ta e="T516" id="Seg_7659" s="T515">прийти-IPFV-PST-1SG.S</ta>
            <ta e="T517" id="Seg_7660" s="T516">лошадь-INSTR</ta>
            <ta e="T518" id="Seg_7661" s="T517">нарты-INSTR</ta>
            <ta e="T519" id="Seg_7662" s="T518">вода-ILL</ta>
            <ta e="T520" id="Seg_7663" s="T519">занести-TR-CVB</ta>
            <ta e="T521" id="Seg_7664" s="T520">бросать-PST-3PL</ta>
            <ta e="T522" id="Seg_7665" s="T521">погрузиться.в.воду-RES-PST-3PL</ta>
            <ta e="T523" id="Seg_7666" s="T522">камень-COM</ta>
            <ta e="T524" id="Seg_7667" s="T523">домой</ta>
            <ta e="T525" id="Seg_7668" s="T524">отправиться-PST-3PL</ta>
            <ta e="T526" id="Seg_7669" s="T525">мы.PL.NOM</ta>
            <ta e="T527" id="Seg_7670" s="T526">погрузиться.в.воду-CAUS-PST-1PL</ta>
            <ta e="T528" id="Seg_7671" s="T527">вода-ILL</ta>
            <ta e="T529" id="Seg_7672" s="T528">Ичакичика.[NOM]</ta>
            <ta e="T530" id="Seg_7673" s="T529">прийти-PST.[3SG.S]</ta>
            <ta e="T531" id="Seg_7674" s="T530">домой</ta>
            <ta e="T532" id="Seg_7675" s="T531">лошадь-INSTR</ta>
            <ta e="T533" id="Seg_7676" s="T532">лошадь-ADJZ</ta>
            <ta e="T534" id="Seg_7677" s="T533">нарты-INSTR</ta>
            <ta e="T535" id="Seg_7678" s="T534">товар.[NOM]-3SG</ta>
            <ta e="T536" id="Seg_7679" s="T535">вода-INSTR</ta>
            <ta e="T537" id="Seg_7680" s="T536">середина-CAUS-PST-3SG.O</ta>
            <ta e="T538" id="Seg_7681" s="T537">верёвка-ILL</ta>
            <ta e="T539" id="Seg_7682" s="T538">вверх</ta>
            <ta e="T540" id="Seg_7683" s="T539">повесить-MULO-PST-3SG.O</ta>
            <ta e="T541" id="Seg_7684" s="T540">сохнуть-INF</ta>
            <ta e="T542" id="Seg_7685" s="T541">этот</ta>
            <ta e="T543" id="Seg_7686" s="T542">время-LOC</ta>
            <ta e="T544" id="Seg_7687" s="T543">начальник.[NOM]</ta>
            <ta e="T545" id="Seg_7688" s="T544">Кольсако.[NOM]</ta>
            <ta e="T546" id="Seg_7689" s="T545">старик.[NOM]</ta>
            <ta e="T547" id="Seg_7690" s="T546">взглянуть-HAB.[3SG.S]</ta>
            <ta e="T548" id="Seg_7691" s="T547">Ичакичика-GEN</ta>
            <ta e="T549" id="Seg_7692" s="T548">чум-EP-ADJZ</ta>
            <ta e="T550" id="Seg_7693" s="T549">сторона-ILL</ta>
            <ta e="T551" id="Seg_7694" s="T550">печь.[NOM]</ta>
            <ta e="T552" id="Seg_7695" s="T551">зажечь-HAB-3SG.O</ta>
            <ta e="T553" id="Seg_7696" s="T552">дым.[NOM]</ta>
            <ta e="T554" id="Seg_7697" s="T553">дымить.[3SG.S]</ta>
            <ta e="T555" id="Seg_7698" s="T554">печь.GEN</ta>
            <ta e="T556" id="Seg_7699" s="T555">от-ADV.EL</ta>
            <ta e="T557" id="Seg_7700" s="T556">здесь-COM</ta>
            <ta e="T558" id="Seg_7701" s="T557">здесь-COM</ta>
            <ta e="T559" id="Seg_7702" s="T558">сказать-PST-3SG.O</ta>
            <ta e="T560" id="Seg_7703" s="T559">казак-EP-DU-ALL</ta>
            <ta e="T561" id="Seg_7704" s="T560">уйти-OPT.[3SG.S]</ta>
            <ta e="T562" id="Seg_7705" s="T561">взглянуть-HAB-INF</ta>
            <ta e="T563" id="Seg_7706" s="T562">старуха-ACC</ta>
            <ta e="T564" id="Seg_7707" s="T563">он(а)-EP-PL.[NOM]</ta>
            <ta e="T565" id="Seg_7708" s="T564">уйти-PST-3PL</ta>
            <ta e="T566" id="Seg_7709" s="T565">чум-ILL</ta>
            <ta e="T567" id="Seg_7710" s="T566">войти-PST-3PL</ta>
            <ta e="T568" id="Seg_7711" s="T567">старуха-GEN</ta>
            <ta e="T569" id="Seg_7712" s="T568">место.[NOM]</ta>
            <ta e="T570" id="Seg_7713" s="T569">вместо</ta>
            <ta e="T571" id="Seg_7714" s="T570">сесть.[3SG.S]</ta>
            <ta e="T572" id="Seg_7715" s="T571">стол-EP-GEN</ta>
            <ta e="T573" id="Seg_7716" s="T572">на-ADV.LOC</ta>
            <ta e="T574" id="Seg_7717" s="T573">Ичакичика.[NOM]</ta>
            <ta e="T575" id="Seg_7718" s="T574">сердиться-DECAUS-HAB.[3SG.S]</ta>
            <ta e="T576" id="Seg_7719" s="T575">всё</ta>
            <ta e="T577" id="Seg_7720" s="T576">лицо.[NOM]-3SG</ta>
            <ta e="T578" id="Seg_7721" s="T577">расцарапать-HAB-3SG.O</ta>
            <ta e="T579" id="Seg_7722" s="T578">кровь-%%</ta>
            <ta e="T580" id="Seg_7723" s="T579">одежда-ACC-3SG</ta>
            <ta e="T581" id="Seg_7724" s="T580">рвать-PTCP.PST-CO.[3SG.S]</ta>
            <ta e="T582" id="Seg_7725" s="T581">он(а)-EP-PL.[NOM]</ta>
            <ta e="T583" id="Seg_7726" s="T582">сказать-PST-3PL</ta>
            <ta e="T584" id="Seg_7727" s="T583">войти-PST-3PL</ta>
            <ta e="T585" id="Seg_7728" s="T584">здорово</ta>
            <ta e="T586" id="Seg_7729" s="T585">товарищ.[NOM]</ta>
            <ta e="T587" id="Seg_7730" s="T586">а</ta>
            <ta e="T588" id="Seg_7731" s="T587">Ичакичика.[NOM]</ta>
            <ta e="T589" id="Seg_7732" s="T588">сердиться-DECAUS-CVB</ta>
            <ta e="T590" id="Seg_7733" s="T589">сесть.[3SG.S]</ta>
            <ta e="T591" id="Seg_7734" s="T590">вы.PL.NOM</ta>
            <ta e="T592" id="Seg_7735" s="T591">я.ACC</ta>
            <ta e="T593" id="Seg_7736" s="T592">вода-%%</ta>
            <ta e="T594" id="Seg_7737" s="T593">погрузиться.в.воду-CAUS-PST-2PL</ta>
            <ta e="T595" id="Seg_7738" s="T594">я.NOM</ta>
            <ta e="T596" id="Seg_7739" s="T595">море-GEN</ta>
            <ta e="T597" id="Seg_7740" s="T596">внизу</ta>
            <ta e="T598" id="Seg_7741" s="T597">много-ADVZ</ta>
            <ta e="T599" id="Seg_7742" s="T598">товар.[NOM]</ta>
            <ta e="T600" id="Seg_7743" s="T599">материя.[NOM]</ta>
            <ta e="T601" id="Seg_7744" s="T600">найти-PST-1SG.O</ta>
            <ta e="T602" id="Seg_7745" s="T601">столько</ta>
            <ta e="T603" id="Seg_7746" s="T602">быть-CO.[3SG.S]</ta>
            <ta e="T604" id="Seg_7747" s="T603">сколько</ta>
            <ta e="T605" id="Seg_7748" s="T604">сила.[NOM]-1SG</ta>
            <ta e="T606" id="Seg_7749" s="T605">быть-PST.[3SG.S]</ta>
            <ta e="T607" id="Seg_7750" s="T606">взять-PST-1SG.O</ta>
            <ta e="T608" id="Seg_7751" s="T607">я.GEN</ta>
            <ta e="T609" id="Seg_7752" s="T608">сила.[NOM]-1SG</ta>
            <ta e="T610" id="Seg_7753" s="T609">NEG</ta>
            <ta e="T611" id="Seg_7754" s="T610">взять-PST-3SG.O</ta>
            <ta e="T612" id="Seg_7755" s="T611">будто</ta>
            <ta e="T613" id="Seg_7756" s="T612">вода-%%-EP-ADJZ</ta>
            <ta e="T614" id="Seg_7757" s="T613">бог-LOC</ta>
            <ta e="T615" id="Seg_7758" s="T614">быть-PST-1SG.S</ta>
            <ta e="T616" id="Seg_7759" s="T615">он(а)-EP-PL.[NOM]</ta>
            <ta e="T617" id="Seg_7760" s="T616">отправиться-PST-3PL</ta>
            <ta e="T618" id="Seg_7761" s="T617">испугаться-DECAUS-CVB</ta>
            <ta e="T619" id="Seg_7762" s="T618">что-ADJZ</ta>
            <ta e="T620" id="Seg_7763" s="T619">дьявол.[NOM]</ta>
            <ta e="T621" id="Seg_7764" s="T620">быть-PST.[3SG.S]</ta>
            <ta e="T622" id="Seg_7765" s="T621">Кольсако.[NOM]</ta>
            <ta e="T623" id="Seg_7766" s="T622">старик-ALL</ta>
            <ta e="T624" id="Seg_7767" s="T623">сказать-INF</ta>
            <ta e="T625" id="Seg_7768" s="T624">Ичакичика-ACC</ta>
            <ta e="T626" id="Seg_7769" s="T625">вода-ILL</ta>
            <ta e="T627" id="Seg_7770" s="T626">погрузиться.в.воду-CAUS-PST-1PL</ta>
            <ta e="T629" id="Seg_7771" s="T628">день.[NOM]</ta>
            <ta e="T630" id="Seg_7772" s="T629">раньше</ta>
            <ta e="T631" id="Seg_7773" s="T630">он(а).[NOM]</ta>
            <ta e="T632" id="Seg_7774" s="T631">многочисленный</ta>
            <ta e="T633" id="Seg_7775" s="T632">найти-PST.NAR-3SG.O</ta>
            <ta e="T634" id="Seg_7776" s="T633">товар.[NOM]</ta>
            <ta e="T635" id="Seg_7777" s="T634">материя.[NOM]</ta>
            <ta e="T636" id="Seg_7778" s="T635">interrog.pron.stem-ADJZ</ta>
            <ta e="T637" id="Seg_7779" s="T636">имущество.[NOM]</ta>
            <ta e="T638" id="Seg_7780" s="T637">всё</ta>
            <ta e="T639" id="Seg_7781" s="T638">найти-PST.NAR-3SG.O</ta>
            <ta e="T640" id="Seg_7782" s="T639">и</ta>
            <ta e="T641" id="Seg_7783" s="T640">соболь-GEN</ta>
            <ta e="T642" id="Seg_7784" s="T641">шкура-ADJZ</ta>
            <ta e="T643" id="Seg_7785" s="T642">нечто-PL.[NOM]</ta>
            <ta e="T644" id="Seg_7786" s="T643">Кольсако.[NOM]</ta>
            <ta e="T645" id="Seg_7787" s="T644">старик.[NOM]</ta>
            <ta e="T646" id="Seg_7788" s="T645">ум.[NOM]-3SG</ta>
            <ta e="T647" id="Seg_7789" s="T646">потеряться-PST.[3SG.S]</ta>
            <ta e="T648" id="Seg_7790" s="T647">как</ta>
            <ta e="T649" id="Seg_7791" s="T648">такой-ADVZ</ta>
            <ta e="T650" id="Seg_7792" s="T649">стать-PST.[3SG.S]</ta>
            <ta e="T651" id="Seg_7793" s="T650">Ичакичика.[NOM]</ta>
            <ta e="T652" id="Seg_7794" s="T651">дьявол.[NOM]</ta>
            <ta e="T655" id="Seg_7795" s="T654">новый-ADJZ</ta>
            <ta e="T656" id="Seg_7796" s="T655">день.[NOM]</ta>
            <ta e="T657" id="Seg_7797" s="T656">он.сам</ta>
            <ta e="T658" id="Seg_7798" s="T657">два-PL</ta>
            <ta e="T659" id="Seg_7799" s="T658">казак-PL.[NOM]</ta>
            <ta e="T660" id="Seg_7800" s="T659">прийти-%%-3PL</ta>
            <ta e="T661" id="Seg_7801" s="T660">Ичакичика-ILL</ta>
            <ta e="T662" id="Seg_7802" s="T661">послать-3SG.O</ta>
            <ta e="T663" id="Seg_7803" s="T662">казак.[NOM]</ta>
            <ta e="T664" id="Seg_7804" s="T663">старик.[NOM]</ta>
            <ta e="T665" id="Seg_7805" s="T664">спросить-INF</ta>
            <ta e="T666" id="Seg_7806" s="T665">Ичакичика-ACC</ta>
            <ta e="T667" id="Seg_7807" s="T666">как</ta>
            <ta e="T668" id="Seg_7808" s="T667">найти-PST-3SG.O</ta>
            <ta e="T669" id="Seg_7809" s="T668">этот</ta>
            <ta e="T670" id="Seg_7810" s="T669">тот</ta>
            <ta e="T671" id="Seg_7811" s="T670">материя-PL-EP-ACC</ta>
            <ta e="T672" id="Seg_7812" s="T671">мы.PL.NOM</ta>
            <ta e="T673" id="Seg_7813" s="T672">тоже</ta>
            <ta e="T674" id="Seg_7814" s="T673">вода-ILL</ta>
            <ta e="T675" id="Seg_7815" s="T674">нырять-DRV-FUT-1PL</ta>
            <ta e="T676" id="Seg_7816" s="T675">товар-EP-TRL</ta>
            <ta e="T677" id="Seg_7817" s="T676">Ичакичика.[NOM]</ta>
            <ta e="T678" id="Seg_7818" s="T677">сказать-PST-3SG.O</ta>
            <ta e="T679" id="Seg_7819" s="T678">сшить-%%-IMP.2PL</ta>
            <ta e="T680" id="Seg_7820" s="T679">сшить-MULO-CO-3PL</ta>
            <ta e="T681" id="Seg_7821" s="T680">сшить-MULO-CO-2PL</ta>
            <ta e="T682" id="Seg_7822" s="T681">корова-GEN</ta>
            <ta e="T683" id="Seg_7823" s="T682">шкура.GEN</ta>
            <ta e="T684" id="Seg_7824" s="T683">из-ADV.EL</ta>
            <ta e="T685" id="Seg_7825" s="T684">мешок-PL-EP-ACC</ta>
            <ta e="T686" id="Seg_7826" s="T685">сшить-IMP.2PL</ta>
            <ta e="T687" id="Seg_7827" s="T686">мешок-PL-EP-ACC</ta>
            <ta e="T688" id="Seg_7828" s="T687">корова-GEN</ta>
            <ta e="T689" id="Seg_7829" s="T688">шкура-ADJZ</ta>
            <ta e="T690" id="Seg_7830" s="T689">лечь-IMP.2PL</ta>
            <ta e="T691" id="Seg_7831" s="T690">море-GEN</ta>
            <ta e="T692" id="Seg_7832" s="T691">берег-ILL</ta>
            <ta e="T693" id="Seg_7833" s="T692">я.NOM</ta>
            <ta e="T694" id="Seg_7834" s="T693">вы.PL.ACC</ta>
            <ta e="T695" id="Seg_7835" s="T694">сшить-FUT-1SG.O</ta>
            <ta e="T696" id="Seg_7836" s="T695">мешок-ILL</ta>
            <ta e="T697" id="Seg_7837" s="T696">бросать-FUT-1SG.O</ta>
            <ta e="T698" id="Seg_7838" s="T697">вода-ILL</ta>
            <ta e="T699" id="Seg_7839" s="T698">камень-INSTR</ta>
            <ta e="T700" id="Seg_7840" s="T699">большой</ta>
            <ta e="T701" id="Seg_7841" s="T700">камень-INSTR</ta>
            <ta e="T702" id="Seg_7842" s="T701">вы.PL.NOM</ta>
            <ta e="T703" id="Seg_7843" s="T702">найти-FUT-2PL</ta>
            <ta e="T704" id="Seg_7844" s="T703">всё</ta>
            <ta e="T705" id="Seg_7845" s="T704">товар.[NOM]</ta>
            <ta e="T706" id="Seg_7846" s="T705">раньше</ta>
            <ta e="T707" id="Seg_7847" s="T706">Кольсако.[NOM]</ta>
            <ta e="T708" id="Seg_7848" s="T707">старик.[NOM]</ta>
            <ta e="T709" id="Seg_7849" s="T708">начальник.[NOM]</ta>
            <ta e="T710" id="Seg_7850" s="T709">кто.[NOM]</ta>
            <ta e="T711" id="Seg_7851" s="T710">хотеть.[3SG.S]</ta>
            <ta e="T712" id="Seg_7852" s="T711">город-EL</ta>
            <ta e="T713" id="Seg_7853" s="T712">он(а).[NOM]</ta>
            <ta e="T714" id="Seg_7854" s="T713">сшить-%%-PST-3SG.O</ta>
            <ta e="T715" id="Seg_7855" s="T714">раньше</ta>
            <ta e="T716" id="Seg_7856" s="T715">Кольсако.[NOM]</ta>
            <ta e="T717" id="Seg_7857" s="T716">старик-ACC</ta>
            <ta e="T718" id="Seg_7858" s="T717">потом</ta>
            <ta e="T719" id="Seg_7859" s="T718">чужой</ta>
            <ta e="T720" id="Seg_7860" s="T719">человек-PL-ACC-3SG</ta>
            <ta e="T721" id="Seg_7861" s="T720">казак-PL-EP-ACC</ta>
            <ta e="T722" id="Seg_7862" s="T721">потом</ta>
            <ta e="T723" id="Seg_7863" s="T722">Ичакичика.[NOM]</ta>
            <ta e="T724" id="Seg_7864" s="T723">он.сам</ta>
            <ta e="T725" id="Seg_7865" s="T724">бросать-INF-собраться-PST-3SG.O</ta>
            <ta e="T726" id="Seg_7866" s="T725">сшить-EP-PTCP.PST</ta>
            <ta e="T727" id="Seg_7867" s="T726">мешок-PL-EP-ACC</ta>
            <ta e="T728" id="Seg_7868" s="T727">человек-EP-PL-COM</ta>
            <ta e="T729" id="Seg_7869" s="T728">вода-ILL</ta>
            <ta e="T730" id="Seg_7870" s="T729">сказать-PST-3SG.O</ta>
            <ta e="T731" id="Seg_7871" s="T730">Кольсако.[NOM]</ta>
            <ta e="T732" id="Seg_7872" s="T731">старик-ALL</ta>
            <ta e="T733" id="Seg_7873" s="T732">искать-IMP.2SG.O</ta>
            <ta e="T734" id="Seg_7874" s="T733">товар.[NOM]</ta>
            <ta e="T735" id="Seg_7875" s="T734">море-GEN</ta>
            <ta e="T736" id="Seg_7876" s="T735">внизу</ta>
            <ta e="T737" id="Seg_7877" s="T736">NEG</ta>
            <ta e="T738" id="Seg_7878" s="T737">где</ta>
            <ta e="T739" id="Seg_7879" s="T738">NEG</ta>
            <ta e="T740" id="Seg_7880" s="T739">найти-FUT-2SG.O</ta>
            <ta e="T741" id="Seg_7881" s="T740">товар.[NOM]-EP-3SG</ta>
            <ta e="T742" id="Seg_7882" s="T741">и</ta>
            <ta e="T743" id="Seg_7883" s="T742">товар-EP-PL.[NOM]</ta>
            <ta e="T744" id="Seg_7884" s="T743">всегда</ta>
            <ta e="T745" id="Seg_7885" s="T744">умереть-FUT-2SG.S</ta>
            <ta e="T746" id="Seg_7886" s="T745">вы.PL.NOM</ta>
            <ta e="T747" id="Seg_7887" s="T746">вот</ta>
            <ta e="T748" id="Seg_7888" s="T747">я.ACC</ta>
            <ta e="T749" id="Seg_7889" s="T748">бросать-DUR-PST-2PL</ta>
            <ta e="T750" id="Seg_7890" s="T749">вода-ILL</ta>
            <ta e="T751" id="Seg_7891" s="T750">я.ACC</ta>
            <ta e="T752" id="Seg_7892" s="T751">бросать-PST-2PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_7893" s="T0">nprop-n:case3</ta>
            <ta e="T2" id="Seg_7894" s="T1">n-n:case3</ta>
            <ta e="T3" id="Seg_7895" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_7896" s="T3">nprop-n:case3</ta>
            <ta e="T5" id="Seg_7897" s="T4">n-n:obl.poss-n:case2</ta>
            <ta e="T6" id="Seg_7898" s="T5">nprop-n:case3</ta>
            <ta e="T7" id="Seg_7899" s="T6">n-n&gt;adj</ta>
            <ta e="T8" id="Seg_7900" s="T7">n-n:case3</ta>
            <ta e="T9" id="Seg_7901" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_7902" s="T9">adv</ta>
            <ta e="T11" id="Seg_7903" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_7904" s="T11">num</ta>
            <ta e="T13" id="Seg_7905" s="T12">n-n:case3</ta>
            <ta e="T14" id="Seg_7906" s="T13">nprop-n:case3</ta>
            <ta e="T15" id="Seg_7907" s="T14">n-n&gt;adj</ta>
            <ta e="T16" id="Seg_7908" s="T15">n-n:case3-n:poss</ta>
            <ta e="T17" id="Seg_7909" s="T16">v-v:pn</ta>
            <ta e="T18" id="Seg_7910" s="T17">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_7911" s="T19">v-v&gt;v-v:pn</ta>
            <ta e="T21" id="Seg_7912" s="T20">pers-n:case3</ta>
            <ta e="T22" id="Seg_7913" s="T21">v-v:pn</ta>
            <ta e="T23" id="Seg_7914" s="T22">conj</ta>
            <ta e="T24" id="Seg_7915" s="T23">nprop-n:case3-n:poss</ta>
            <ta e="T25" id="Seg_7916" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_7917" s="T25">v-v:pn</ta>
            <ta e="T27" id="Seg_7918" s="T26">pers-n:(ins)-n:case2</ta>
            <ta e="T28" id="Seg_7919" s="T27">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T29" id="Seg_7920" s="T28">num-n:num-n:(ins)-n:case3</ta>
            <ta e="T30" id="Seg_7921" s="T29">v-v&gt;v-v:inf</ta>
            <ta e="T31" id="Seg_7922" s="T30">interrog-n:(ins)-n:case3</ta>
            <ta e="T32" id="Seg_7923" s="T31">v-v:tense-mood-v:pn</ta>
            <ta e="T33" id="Seg_7924" s="T32">nprop-n:case3</ta>
            <ta e="T34" id="Seg_7925" s="T33">pers-n:case3</ta>
            <ta e="T35" id="Seg_7926" s="T34">v-v:pn</ta>
            <ta e="T36" id="Seg_7927" s="T35">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_7928" s="T36">n-n:case3</ta>
            <ta e="T38" id="Seg_7929" s="T37">n-n:obl.poss-n:case2</ta>
            <ta e="T39" id="Seg_7930" s="T38">pers-n:(ins)-n:case3</ta>
            <ta e="T40" id="Seg_7931" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_7932" s="T40">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_7933" s="T41">n-n:case3</ta>
            <ta e="T43" id="Seg_7934" s="T42">dem</ta>
            <ta e="T44" id="Seg_7935" s="T43">n-n:case3</ta>
            <ta e="T45" id="Seg_7936" s="T44">dem</ta>
            <ta e="T46" id="Seg_7937" s="T45">n-n:case3</ta>
            <ta e="T47" id="Seg_7938" s="T46">n-n:case3</ta>
            <ta e="T48" id="Seg_7939" s="T47">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_7940" s="T48">n-n:case1-n:poss</ta>
            <ta e="T50" id="Seg_7941" s="T49">n-n:case3</ta>
            <ta e="T51" id="Seg_7942" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_7943" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_7944" s="T52">adj</ta>
            <ta e="T54" id="Seg_7945" s="T53">n-n:case3</ta>
            <ta e="T55" id="Seg_7946" s="T54">n-n:(ins)-n:case3</ta>
            <ta e="T56" id="Seg_7947" s="T55">n-n:case3</ta>
            <ta e="T57" id="Seg_7948" s="T56">n-n:case1-n:poss</ta>
            <ta e="T58" id="Seg_7949" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_7950" s="T58">n-n:(ins)-n&gt;adj</ta>
            <ta e="T60" id="Seg_7951" s="T59">n-n:case3</ta>
            <ta e="T61" id="Seg_7952" s="T60">n-n:case3</ta>
            <ta e="T62" id="Seg_7953" s="T61">n-n:case1-n:poss</ta>
            <ta e="T63" id="Seg_7954" s="T62">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_7955" s="T63">n-n:num-n:case3</ta>
            <ta e="T65" id="Seg_7956" s="T64">n-n:num-n:case3</ta>
            <ta e="T66" id="Seg_7957" s="T65">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T67" id="Seg_7958" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_7959" s="T67">nprop-n:case3</ta>
            <ta e="T69" id="Seg_7960" s="T68">adj</ta>
            <ta e="T70" id="Seg_7961" s="T69">n-n:case3</ta>
            <ta e="T71" id="Seg_7962" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_7963" s="T71">nprop-n:case3</ta>
            <ta e="T73" id="Seg_7964" s="T72">pers</ta>
            <ta e="T74" id="Seg_7965" s="T73">v-v&gt;v-v:inf</ta>
            <ta e="T75" id="Seg_7966" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_7967" s="T75">n-n:case3</ta>
            <ta e="T77" id="Seg_7968" s="T76">pers</ta>
            <ta e="T78" id="Seg_7969" s="T77">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T79" id="Seg_7970" s="T78">v-v&gt;v-v:pn</ta>
            <ta e="T80" id="Seg_7971" s="T79">v-v:ins-v:pn</ta>
            <ta e="T81" id="Seg_7972" s="T80">pers-n:case3</ta>
            <ta e="T82" id="Seg_7973" s="T81">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_7974" s="T82">n-n:obl.poss-n:case2</ta>
            <ta e="T84" id="Seg_7975" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_7976" s="T84">pers</ta>
            <ta e="T86" id="Seg_7977" s="T85">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_7978" s="T86">n-n:case3</ta>
            <ta e="T88" id="Seg_7979" s="T87">n-n:case3</ta>
            <ta e="T89" id="Seg_7980" s="T88">v-v&gt;v-v:mood-pn</ta>
            <ta e="T90" id="Seg_7981" s="T89">v-v&gt;adv</ta>
            <ta e="T91" id="Seg_7982" s="T90">n-n:case3-n:poss</ta>
            <ta e="T92" id="Seg_7983" s="T91">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T93" id="Seg_7984" s="T92">n-n:case1-n:poss</ta>
            <ta e="T94" id="Seg_7985" s="T93">n-n:case1-n:poss</ta>
            <ta e="T95" id="Seg_7986" s="T94">v-v:pn</ta>
            <ta e="T96" id="Seg_7987" s="T95">n-n:num-n:case3</ta>
            <ta e="T97" id="Seg_7988" s="T96">v-v:ins-v:pn</ta>
            <ta e="T98" id="Seg_7989" s="T97">n-n:case3</ta>
            <ta e="T99" id="Seg_7990" s="T98">n-n:case3</ta>
            <ta e="T100" id="Seg_7991" s="T99">nprop-n:case3</ta>
            <ta e="T101" id="Seg_7992" s="T100">n-n:obl.poss-n:case2</ta>
            <ta e="T102" id="Seg_7993" s="T101">pers</ta>
            <ta e="T103" id="Seg_7994" s="T102">n-v:ins-v:pn</ta>
            <ta e="T104" id="Seg_7995" s="T103">n-n:case3</ta>
            <ta e="T105" id="Seg_7996" s="T104">ptcl</ta>
            <ta e="T106" id="Seg_7997" s="T105">v-v:pn</ta>
            <ta e="T107" id="Seg_7998" s="T106">v-v&gt;v-v:inf</ta>
            <ta e="T108" id="Seg_7999" s="T107">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_8000" s="T108">nprop-n:case3</ta>
            <ta e="T110" id="Seg_8001" s="T109">n-n:case1-n:poss</ta>
            <ta e="T111" id="Seg_8002" s="T110">adv</ta>
            <ta e="T112" id="Seg_8003" s="T111">v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_8004" s="T112">n-n:case1-n:poss</ta>
            <ta e="T114" id="Seg_8005" s="T113">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_8006" s="T114">n-n:case1-n:poss</ta>
            <ta e="T116" id="Seg_8007" s="T115">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T117" id="Seg_8008" s="T116">adv</ta>
            <ta e="T118" id="Seg_8009" s="T117">nprop-n:case3</ta>
            <ta e="T119" id="Seg_8010" s="T118">v-v:tense-v:pn</ta>
            <ta e="T120" id="Seg_8011" s="T119">n-n:num-n:case3</ta>
            <ta e="T121" id="Seg_8012" s="T120">n-n:case1-n:poss</ta>
            <ta e="T122" id="Seg_8013" s="T121">adv</ta>
            <ta e="T123" id="Seg_8014" s="T122">dem-adj&gt;adv</ta>
            <ta e="T124" id="Seg_8015" s="T123">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T125" id="Seg_8016" s="T124">n-n:case1-n:poss</ta>
            <ta e="T126" id="Seg_8017" s="T125">n-n:case2-n:obl.poss</ta>
            <ta e="T127" id="Seg_8018" s="T126">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_8019" s="T127">n-n:case3</ta>
            <ta e="T129" id="Seg_8020" s="T128">preverb</ta>
            <ta e="T130" id="Seg_8021" s="T129">v-v:mood-pn</ta>
            <ta e="T131" id="Seg_8022" s="T130">n-n:case1-n:poss</ta>
            <ta e="T132" id="Seg_8023" s="T131">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T133" id="Seg_8024" s="T132">adv</ta>
            <ta e="T134" id="Seg_8025" s="T133">n-n:case1-n:poss</ta>
            <ta e="T135" id="Seg_8026" s="T134">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T136" id="Seg_8027" s="T135">v-v&gt;adv</ta>
            <ta e="T137" id="Seg_8028" s="T136">v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_8029" s="T137">n-n:case1-n:poss</ta>
            <ta e="T139" id="Seg_8030" s="T138">v-v&gt;v-v:inf</ta>
            <ta e="T140" id="Seg_8031" s="T139">n-n:case3</ta>
            <ta e="T141" id="Seg_8032" s="T140">v-v&gt;v-v&gt;n-n:case3</ta>
            <ta e="T142" id="Seg_8033" s="T141">pp</ta>
            <ta e="T143" id="Seg_8034" s="T142">n-n:case3</ta>
            <ta e="T144" id="Seg_8035" s="T143">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_8036" s="T144">n-n:(ins)-n:case3</ta>
            <ta e="T146" id="Seg_8037" s="T145">n-n:case3</ta>
            <ta e="T147" id="Seg_8038" s="T146">v-v:tense-v:pn</ta>
            <ta e="T148" id="Seg_8039" s="T147">n-n:case3</ta>
            <ta e="T149" id="Seg_8040" s="T148">v-v&gt;adv</ta>
            <ta e="T150" id="Seg_8041" s="T149">n-n:num-n:case3</ta>
            <ta e="T151" id="Seg_8042" s="T150">v-n:(ins)-v&gt;v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T152" id="Seg_8043" s="T151">pers-n:(ins)-n:num-n:case3</ta>
            <ta e="T153" id="Seg_8044" s="T152">dem-adj&gt;adv</ta>
            <ta e="T154" id="Seg_8045" s="T153">v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_8046" s="T154">pers</ta>
            <ta e="T156" id="Seg_8047" s="T155">pers</ta>
            <ta e="T157" id="Seg_8048" s="T156">n-n:case3</ta>
            <ta e="T158" id="Seg_8049" s="T157">v-v:tense-v:pn</ta>
            <ta e="T159" id="Seg_8050" s="T158">pers</ta>
            <ta e="T160" id="Seg_8051" s="T159">n-n:num-n:case1-n:poss</ta>
            <ta e="T161" id="Seg_8052" s="T160">n-n&gt;v-v:pn</ta>
            <ta e="T162" id="Seg_8053" s="T161">conj</ta>
            <ta e="T163" id="Seg_8054" s="T162">v-v:pn</ta>
            <ta e="T164" id="Seg_8055" s="T163">n-n:num-n:case3</ta>
            <ta e="T165" id="Seg_8056" s="T164">ptcl</ta>
            <ta e="T166" id="Seg_8057" s="T165">v-v&gt;v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T167" id="Seg_8058" s="T166">nprop-n:case3</ta>
            <ta e="T168" id="Seg_8059" s="T167">v-v:tense-v:pn</ta>
            <ta e="T169" id="Seg_8060" s="T168">v-v:tense-v:pn</ta>
            <ta e="T170" id="Seg_8061" s="T169">nprop-n:case3</ta>
            <ta e="T171" id="Seg_8062" s="T170">n-n:case3</ta>
            <ta e="T172" id="Seg_8063" s="T171">v-v:tense-v:pn</ta>
            <ta e="T173" id="Seg_8064" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_8065" s="T173">v-v:tense-v:pn</ta>
            <ta e="T175" id="Seg_8066" s="T174">n-n:case3</ta>
            <ta e="T176" id="Seg_8067" s="T175">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T177" id="Seg_8068" s="T176">v-v:inf</ta>
            <ta e="T178" id="Seg_8069" s="T177">n-n:case3</ta>
            <ta e="T179" id="Seg_8070" s="T178">adj</ta>
            <ta e="T180" id="Seg_8071" s="T179">n-n:case3</ta>
            <ta e="T181" id="Seg_8072" s="T180">v-v:pn</ta>
            <ta e="T182" id="Seg_8073" s="T181">nprop-n:case3</ta>
            <ta e="T183" id="Seg_8074" s="T182">n-n:case3</ta>
            <ta e="T184" id="Seg_8075" s="T183">v-v:tense-v:pn</ta>
            <ta e="T185" id="Seg_8076" s="T184">n-n:case3</ta>
            <ta e="T186" id="Seg_8077" s="T185">pers</ta>
            <ta e="T187" id="Seg_8078" s="T186">v-v:mood-pn</ta>
            <ta e="T188" id="Seg_8079" s="T187">pers</ta>
            <ta e="T189" id="Seg_8080" s="T188">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T190" id="Seg_8081" s="T189">pers</ta>
            <ta e="T191" id="Seg_8082" s="T190">n-n:case1-n:poss</ta>
            <ta e="T192" id="Seg_8083" s="T191">conj</ta>
            <ta e="T193" id="Seg_8084" s="T192">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T194" id="Seg_8085" s="T193">n-n&gt;v-v:pn</ta>
            <ta e="T195" id="Seg_8086" s="T194">n-n:num-n:case3</ta>
            <ta e="T196" id="Seg_8087" s="T195">conj</ta>
            <ta e="T197" id="Seg_8088" s="T196">n-n:num-n:case3</ta>
            <ta e="T198" id="Seg_8089" s="T197">v-v:pn</ta>
            <ta e="T199" id="Seg_8090" s="T198">nprop-n:case3</ta>
            <ta e="T200" id="Seg_8091" s="T199">v-v:tense-v:pn</ta>
            <ta e="T201" id="Seg_8092" s="T200">v-v:tense-v:pn</ta>
            <ta e="T202" id="Seg_8093" s="T201">n-n:case3</ta>
            <ta e="T203" id="Seg_8094" s="T202">adv-adj&gt;adv</ta>
            <ta e="T204" id="Seg_8095" s="T203">v-v:mood-pn</ta>
            <ta e="T205" id="Seg_8096" s="T204">adj</ta>
            <ta e="T206" id="Seg_8097" s="T205">n-n:(ins)-n:case3</ta>
            <ta e="T207" id="Seg_8098" s="T206">v-v&gt;v-v:inf</ta>
            <ta e="T208" id="Seg_8099" s="T207">nprop-n:case3</ta>
            <ta e="T209" id="Seg_8100" s="T208">n-n:case3</ta>
            <ta e="T210" id="Seg_8101" s="T209">v-v:tense-v:pn</ta>
            <ta e="T211" id="Seg_8102" s="T210">n-adv:advcase</ta>
            <ta e="T212" id="Seg_8103" s="T211">adv</ta>
            <ta e="T213" id="Seg_8104" s="T212">v-v:tense-v:pn</ta>
            <ta e="T214" id="Seg_8105" s="T213">v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_8106" s="T214">n-n:case3</ta>
            <ta e="T216" id="Seg_8107" s="T215">v-v:mood-pn</ta>
            <ta e="T217" id="Seg_8108" s="T216">conj</ta>
            <ta e="T218" id="Seg_8109" s="T217">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T219" id="Seg_8110" s="T218">v-v:mood-pn</ta>
            <ta e="T220" id="Seg_8111" s="T219">pers-n:(ins)-n:num-n:case3</ta>
            <ta e="T221" id="Seg_8112" s="T220">adv</ta>
            <ta e="T222" id="Seg_8113" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_8114" s="T222">v-v&gt;v-v:pn</ta>
            <ta e="T224" id="Seg_8115" s="T223">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T225" id="Seg_8116" s="T224">adv</ta>
            <ta e="T226" id="Seg_8117" s="T225">v-v:tense-v:pn</ta>
            <ta e="T227" id="Seg_8118" s="T226">n-n:case1-n:poss</ta>
            <ta e="T228" id="Seg_8119" s="T227">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T229" id="Seg_8120" s="T228">quant</ta>
            <ta e="T230" id="Seg_8121" s="T229">n-n:case3</ta>
            <ta e="T231" id="Seg_8122" s="T230">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T232" id="Seg_8123" s="T231">n-n:case2-n:obl.poss</ta>
            <ta e="T233" id="Seg_8124" s="T232">quant</ta>
            <ta e="T234" id="Seg_8125" s="T233">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T235" id="Seg_8126" s="T234">n-n:num-n:case2-n:obl.poss</ta>
            <ta e="T236" id="Seg_8127" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_8128" s="T236">v-v&gt;v-v:pn</ta>
            <ta e="T238" id="Seg_8129" s="T237">adv</ta>
            <ta e="T239" id="Seg_8130" s="T238">v-v&gt;v-v:pn</ta>
            <ta e="T240" id="Seg_8131" s="T239">n-n:case3</ta>
            <ta e="T241" id="Seg_8132" s="T240">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T242" id="Seg_8133" s="T241">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T243" id="Seg_8134" s="T242">v-v:tense-v:pn</ta>
            <ta e="T244" id="Seg_8135" s="T243">interrog</ta>
            <ta e="T245" id="Seg_8136" s="T244">v-v:tense-v:pn</ta>
            <ta e="T246" id="Seg_8137" s="T245">n-n:case3</ta>
            <ta e="T247" id="Seg_8138" s="T246">pers-n:case3</ta>
            <ta e="T248" id="Seg_8139" s="T247">nprop-n:case3</ta>
            <ta e="T249" id="Seg_8140" s="T248">pers-n:case3</ta>
            <ta e="T250" id="Seg_8141" s="T249">n-n:case3</ta>
            <ta e="T251" id="Seg_8142" s="T250">v-v:tense-v:pn</ta>
            <ta e="T252" id="Seg_8143" s="T251">pers-n:case3</ta>
            <ta e="T253" id="Seg_8144" s="T252">nprop-n:case3</ta>
            <ta e="T254" id="Seg_8145" s="T253">v-v:tense-v:pn</ta>
            <ta e="T255" id="Seg_8146" s="T254">v-v:mood-v:pn</ta>
            <ta e="T256" id="Seg_8147" s="T255">v-v:inf</ta>
            <ta e="T257" id="Seg_8148" s="T256">v-v:inf</ta>
            <ta e="T258" id="Seg_8149" s="T257">n-n:num-n:case1-n:poss</ta>
            <ta e="T259" id="Seg_8150" s="T258">n-n:case1-n:poss</ta>
            <ta e="T260" id="Seg_8151" s="T259">n-n:num-n:case3</ta>
            <ta e="T261" id="Seg_8152" s="T260">quant</ta>
            <ta e="T262" id="Seg_8153" s="T261">v-v:tense-v:pn</ta>
            <ta e="T263" id="Seg_8154" s="T262">n-n:case3</ta>
            <ta e="T264" id="Seg_8155" s="T263">pp-n&gt;adv</ta>
            <ta e="T265" id="Seg_8156" s="T264">pers</ta>
            <ta e="T266" id="Seg_8157" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_8158" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_8159" s="T267">adj-n:(ins)-adj&gt;adv</ta>
            <ta e="T269" id="Seg_8160" s="T268">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T270" id="Seg_8161" s="T269">pers-n:(ins)-n:case3</ta>
            <ta e="T271" id="Seg_8162" s="T270">v-v:inf</ta>
            <ta e="T272" id="Seg_8163" s="T271">num</ta>
            <ta e="T273" id="Seg_8164" s="T272">n-n:case3</ta>
            <ta e="T274" id="Seg_8165" s="T273">pers</ta>
            <ta e="T275" id="Seg_8166" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_8167" s="T275">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T277" id="Seg_8168" s="T276">pers</ta>
            <ta e="T278" id="Seg_8169" s="T277">n-n:num-n:case1-n:poss</ta>
            <ta e="T279" id="Seg_8170" s="T278">ptcl</ta>
            <ta e="T280" id="Seg_8171" s="T279">n-n&gt;v-v:pn</ta>
            <ta e="T281" id="Seg_8172" s="T280">nprop-n:case3</ta>
            <ta e="T282" id="Seg_8173" s="T281">n-n:case3</ta>
            <ta e="T283" id="Seg_8174" s="T282">conj</ta>
            <ta e="T284" id="Seg_8175" s="T283">v-v:tense-v:pn</ta>
            <ta e="T285" id="Seg_8176" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_8177" s="T285">v-v&gt;v-v:mood-pn</ta>
            <ta e="T287" id="Seg_8178" s="T286">n-n:case3</ta>
            <ta e="T288" id="Seg_8179" s="T287">n-n:num-n:case3</ta>
            <ta e="T289" id="Seg_8180" s="T288">num-n:poss</ta>
            <ta e="T290" id="Seg_8181" s="T289">dem-adj&gt;adv</ta>
            <ta e="T291" id="Seg_8182" s="T290">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T292" id="Seg_8183" s="T291">pro</ta>
            <ta e="T293" id="Seg_8184" s="T292">n-n:num-n:case1-n:poss</ta>
            <ta e="T294" id="Seg_8185" s="T293">n-n:num-n:case1-n:poss</ta>
            <ta e="T295" id="Seg_8186" s="T294">v-v:tense-v:pn</ta>
            <ta e="T296" id="Seg_8187" s="T295">n-n:(ins)-n:case3</ta>
            <ta e="T297" id="Seg_8188" s="T296">pp-n:(ins)-n&gt;adv</ta>
            <ta e="T298" id="Seg_8189" s="T297">n-adv:advcase</ta>
            <ta e="T299" id="Seg_8190" s="T298">v-v:tense-v:pn</ta>
            <ta e="T300" id="Seg_8191" s="T299">nprop-n:case3</ta>
            <ta e="T301" id="Seg_8192" s="T300">n-n:case3</ta>
            <ta e="T302" id="Seg_8193" s="T301">pers</ta>
            <ta e="T303" id="Seg_8194" s="T302">n-n:num-n:case1-n:poss</ta>
            <ta e="T304" id="Seg_8195" s="T303">adv</ta>
            <ta e="T305" id="Seg_8196" s="T304">v-v:tense-v:pn</ta>
            <ta e="T306" id="Seg_8197" s="T305">n-n:case3</ta>
            <ta e="T307" id="Seg_8198" s="T306">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T308" id="Seg_8199" s="T307">nprop-n:case3</ta>
            <ta e="T309" id="Seg_8200" s="T308">v-v:inf</ta>
            <ta e="T310" id="Seg_8201" s="T309">ptcl</ta>
            <ta e="T311" id="Seg_8202" s="T310">n-n:num-n:case3</ta>
            <ta e="T312" id="Seg_8203" s="T311">v-v:tense-v:pn</ta>
            <ta e="T313" id="Seg_8204" s="T312">nprop-n:case3</ta>
            <ta e="T314" id="Seg_8205" s="T313">dem</ta>
            <ta e="T315" id="Seg_8206" s="T314">n-n:case3</ta>
            <ta e="T316" id="Seg_8207" s="T315">n-n:case2-n:obl.poss</ta>
            <ta e="T317" id="Seg_8208" s="T316">nprop-n:case3</ta>
            <ta e="T318" id="Seg_8209" s="T317">v-v:tense-v:pn</ta>
            <ta e="T319" id="Seg_8210" s="T318">adv</ta>
            <ta e="T320" id="Seg_8211" s="T319">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T321" id="Seg_8212" s="T320">pers</ta>
            <ta e="T322" id="Seg_8213" s="T321">quant</ta>
            <ta e="T323" id="Seg_8214" s="T322">n-n:num-n:case1-n:poss</ta>
            <ta e="T324" id="Seg_8215" s="T323">conj</ta>
            <ta e="T325" id="Seg_8216" s="T324">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T326" id="Seg_8217" s="T325">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T327" id="Seg_8218" s="T326">pers-n:(ins)-n:num-n:case3</ta>
            <ta e="T328" id="Seg_8219" s="T327">v-v:tense-v:pn</ta>
            <ta e="T329" id="Seg_8220" s="T328">n-n:(ins)-n:case3</ta>
            <ta e="T330" id="Seg_8221" s="T329">pp-n:(ins)-n&gt;adv</ta>
            <ta e="T331" id="Seg_8222" s="T330">nprop-n:case3</ta>
            <ta e="T332" id="Seg_8223" s="T331">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T333" id="Seg_8224" s="T332">n-n:case3</ta>
            <ta e="T334" id="Seg_8225" s="T333">nprop-n:case3</ta>
            <ta e="T335" id="Seg_8226" s="T334">n-n:case3</ta>
            <ta e="T336" id="Seg_8227" s="T335">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T337" id="Seg_8228" s="T336">preverb</ta>
            <ta e="T338" id="Seg_8229" s="T337">v-v&gt;v-v:inf</ta>
            <ta e="T339" id="Seg_8230" s="T338">n-n:case3</ta>
            <ta e="T340" id="Seg_8231" s="T339">v-v:tense-v:pn</ta>
            <ta e="T341" id="Seg_8232" s="T340">n-n:case3</ta>
            <ta e="T342" id="Seg_8233" s="T341">n-n:case3</ta>
            <ta e="T343" id="Seg_8234" s="T342">n-n:case3</ta>
            <ta e="T344" id="Seg_8235" s="T343">v-v:inf</ta>
            <ta e="T345" id="Seg_8236" s="T344">v-v&gt;adv</ta>
            <ta e="T346" id="Seg_8237" s="T345">n-n:num-n:case3</ta>
            <ta e="T347" id="Seg_8238" s="T346">pers-n:(ins)-n:case3</ta>
            <ta e="T348" id="Seg_8239" s="T347">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T349" id="Seg_8240" s="T348">n-n&gt;adj</ta>
            <ta e="T350" id="Seg_8241" s="T349">n-n:case3</ta>
            <ta e="T351" id="Seg_8242" s="T350">n-n:case3</ta>
            <ta e="T352" id="Seg_8243" s="T351">n-n:case3</ta>
            <ta e="T353" id="Seg_8244" s="T352">n-n:case3</ta>
            <ta e="T354" id="Seg_8245" s="T353">n-n:case3</ta>
            <ta e="T355" id="Seg_8246" s="T354">pers-n:case3</ta>
            <ta e="T356" id="Seg_8247" s="T355">n-n:case3</ta>
            <ta e="T357" id="Seg_8248" s="T356">pp-adv&gt;adv</ta>
            <ta e="T358" id="Seg_8249" s="T357">v-v&gt;adv</ta>
            <ta e="T359" id="Seg_8250" s="T358">v-v:pn</ta>
            <ta e="T360" id="Seg_8251" s="T359">v-v:tense-v:pn</ta>
            <ta e="T361" id="Seg_8252" s="T360">pers</ta>
            <ta e="T362" id="Seg_8253" s="T361">v-v&gt;v-v:mood-pn</ta>
            <ta e="T363" id="Seg_8254" s="T362">adj</ta>
            <ta e="T364" id="Seg_8255" s="T363">n-n:case3</ta>
            <ta e="T365" id="Seg_8256" s="T364">adj</ta>
            <ta e="T366" id="Seg_8257" s="T365">n-n:case3</ta>
            <ta e="T367" id="Seg_8258" s="T366">v-v&gt;v-v:mood-pn</ta>
            <ta e="T368" id="Seg_8259" s="T367">pers-n:(ins)-n:num-n:case3</ta>
            <ta e="T369" id="Seg_8260" s="T368">v-v:tense-v:pn</ta>
            <ta e="T370" id="Seg_8261" s="T369">v-v:inf</ta>
            <ta e="T371" id="Seg_8262" s="T370">adj</ta>
            <ta e="T372" id="Seg_8263" s="T371">n-n:case3</ta>
            <ta e="T373" id="Seg_8264" s="T372">adv</ta>
            <ta e="T374" id="Seg_8265" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_8266" s="T374">adj</ta>
            <ta e="T376" id="Seg_8267" s="T375">v-v:tense-v:pn</ta>
            <ta e="T377" id="Seg_8268" s="T376">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T378" id="Seg_8269" s="T377">n-n&gt;adj</ta>
            <ta e="T379" id="Seg_8270" s="T378">n-n:case3</ta>
            <ta e="T380" id="Seg_8271" s="T379">v-v&gt;v-v:pn</ta>
            <ta e="T381" id="Seg_8272" s="T380">num</ta>
            <ta e="T382" id="Seg_8273" s="T381">ptcl</ta>
            <ta e="T383" id="Seg_8274" s="T382">num</ta>
            <ta e="T384" id="Seg_8275" s="T383">n-n:num-n:case3</ta>
            <ta e="T385" id="Seg_8276" s="T384">v-v&gt;v-v:pn</ta>
            <ta e="T386" id="Seg_8277" s="T385">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T387" id="Seg_8278" s="T386">n-n:case3</ta>
            <ta e="T388" id="Seg_8279" s="T387">n-n:case3</ta>
            <ta e="T389" id="Seg_8280" s="T388">n-n:case3</ta>
            <ta e="T390" id="Seg_8281" s="T389">n-n:case3</ta>
            <ta e="T391" id="Seg_8282" s="T390">v-v&gt;v-v:pn</ta>
            <ta e="T392" id="Seg_8283" s="T391">adv</ta>
            <ta e="T393" id="Seg_8284" s="T392">n-n:case3</ta>
            <ta e="T394" id="Seg_8285" s="T393">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T395" id="Seg_8286" s="T394">n-n:case3</ta>
            <ta e="T396" id="Seg_8287" s="T395">n-n&gt;adj</ta>
            <ta e="T397" id="Seg_8288" s="T396">n-n:case3</ta>
            <ta e="T398" id="Seg_8289" s="T397">interrog</ta>
            <ta e="T399" id="Seg_8290" s="T398">adv</ta>
            <ta e="T400" id="Seg_8291" s="T399">v-v:pn</ta>
            <ta e="T401" id="Seg_8292" s="T400">nprop-n:case3</ta>
            <ta e="T402" id="Seg_8293" s="T401">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T403" id="Seg_8294" s="T402">interj</ta>
            <ta e="T404" id="Seg_8295" s="T403">n-n:case3</ta>
            <ta e="T405" id="Seg_8296" s="T404">interrog-n:case3</ta>
            <ta e="T406" id="Seg_8297" s="T405">v-v:pn</ta>
            <ta e="T407" id="Seg_8298" s="T406">nprop-n:case3</ta>
            <ta e="T408" id="Seg_8299" s="T407">v-v:tense-v:pn</ta>
            <ta e="T409" id="Seg_8300" s="T408">adv</ta>
            <ta e="T410" id="Seg_8301" s="T409">pers</ta>
            <ta e="T411" id="Seg_8302" s="T410">v-v:mood-pn</ta>
            <ta e="T412" id="Seg_8303" s="T411">preverb</ta>
            <ta e="T413" id="Seg_8304" s="T412">v-v&gt;v-v:mood-pn</ta>
            <ta e="T414" id="Seg_8305" s="T413">pers-n:case3</ta>
            <ta e="T415" id="Seg_8306" s="T414">preverb</ta>
            <ta e="T416" id="Seg_8307" s="T415">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T417" id="Seg_8308" s="T416">pers-n:case3</ta>
            <ta e="T418" id="Seg_8309" s="T417">preverb</ta>
            <ta e="T419" id="Seg_8310" s="T418">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T420" id="Seg_8311" s="T419">v-v:tense-v:pn</ta>
            <ta e="T421" id="Seg_8312" s="T420">pers-n:(ins)-n:case3</ta>
            <ta e="T422" id="Seg_8313" s="T421">n-n:case3</ta>
            <ta e="T423" id="Seg_8314" s="T422">n-n:case3</ta>
            <ta e="T424" id="Seg_8315" s="T423">n-n&gt;adj</ta>
            <ta e="T425" id="Seg_8316" s="T424">n-n:case3</ta>
            <ta e="T426" id="Seg_8317" s="T425">v-v:tense-v:pn</ta>
            <ta e="T427" id="Seg_8318" s="T426">n-n&gt;adv-n:obl.poss</ta>
            <ta e="T428" id="Seg_8319" s="T427">n-n:case3</ta>
            <ta e="T429" id="Seg_8320" s="T428">v-v:ins-v:pn</ta>
            <ta e="T430" id="Seg_8321" s="T429">interrog-n:case3</ta>
            <ta e="T431" id="Seg_8322" s="T430">v-v:pn</ta>
            <ta e="T432" id="Seg_8323" s="T431">pers</ta>
            <ta e="T433" id="Seg_8324" s="T432">v-v:pn</ta>
            <ta e="T434" id="Seg_8325" s="T433">n-n:case3</ta>
            <ta e="T435" id="Seg_8326" s="T434">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T436" id="Seg_8327" s="T435">n-n:case3</ta>
            <ta e="T437" id="Seg_8328" s="T436">n-n:case3</ta>
            <ta e="T438" id="Seg_8329" s="T437">v-v:tense-v:pn</ta>
            <ta e="T439" id="Seg_8330" s="T438">n-n:case1-n:poss</ta>
            <ta e="T440" id="Seg_8331" s="T439">v-v:tense-v:pn</ta>
            <ta e="T441" id="Seg_8332" s="T440">n-n:case3</ta>
            <ta e="T442" id="Seg_8333" s="T441">n-n:case3</ta>
            <ta e="T443" id="Seg_8334" s="T442">quant</ta>
            <ta e="T444" id="Seg_8335" s="T443">n-n:case3</ta>
            <ta e="T445" id="Seg_8336" s="T444">preverb</ta>
            <ta e="T446" id="Seg_8337" s="T445">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T447" id="Seg_8338" s="T446">n-n:case3</ta>
            <ta e="T448" id="Seg_8339" s="T447">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T449" id="Seg_8340" s="T448">n-n:case3</ta>
            <ta e="T450" id="Seg_8341" s="T449">n-n:case3</ta>
            <ta e="T451" id="Seg_8342" s="T450">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T452" id="Seg_8343" s="T451">pers</ta>
            <ta e="T453" id="Seg_8344" s="T452">ptcl</ta>
            <ta e="T454" id="Seg_8345" s="T453">v-v&gt;v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T455" id="Seg_8346" s="T454">n-n:case3</ta>
            <ta e="T456" id="Seg_8347" s="T455">pers-n:(ins)-n:case3</ta>
            <ta e="T457" id="Seg_8348" s="T456">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T458" id="Seg_8349" s="T457">n-n:case3</ta>
            <ta e="T459" id="Seg_8350" s="T458">n-n:case3</ta>
            <ta e="T460" id="Seg_8351" s="T459">n-n:case3</ta>
            <ta e="T461" id="Seg_8352" s="T460">v-v:pn</ta>
            <ta e="T753" id="Seg_8353" s="T461">n-n:case3</ta>
            <ta e="T462" id="Seg_8354" s="T753">%%</ta>
            <ta e="T463" id="Seg_8355" s="T462">n-n:case3</ta>
            <ta e="T464" id="Seg_8356" s="T463">nprop-n:case3</ta>
            <ta e="T465" id="Seg_8357" s="T464">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T466" id="Seg_8358" s="T465">n-n&gt;adj</ta>
            <ta e="T467" id="Seg_8359" s="T466">n-n:obl.poss-n:case2</ta>
            <ta e="T468" id="Seg_8360" s="T467">conj</ta>
            <ta e="T469" id="Seg_8361" s="T468">n-n:case3</ta>
            <ta e="T470" id="Seg_8362" s="T469">n-n:num-n:case3</ta>
            <ta e="T471" id="Seg_8363" s="T470">v-v:tense-v:pn</ta>
            <ta e="T472" id="Seg_8364" s="T471">adj</ta>
            <ta e="T473" id="Seg_8365" s="T472">n-n:case3</ta>
            <ta e="T474" id="Seg_8366" s="T473">v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T475" id="Seg_8367" s="T474">v-v:pn</ta>
            <ta e="T476" id="Seg_8368" s="T475">pro</ta>
            <ta e="T477" id="Seg_8369" s="T476">adv</ta>
            <ta e="T478" id="Seg_8370" s="T477">adv</ta>
            <ta e="T479" id="Seg_8371" s="T478">nprop-n:case3</ta>
            <ta e="T480" id="Seg_8372" s="T479">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T481" id="Seg_8373" s="T480">n-n:case3</ta>
            <ta e="T482" id="Seg_8374" s="T481">v-v:tense-v:pn</ta>
            <ta e="T483" id="Seg_8375" s="T482">n-n:case3</ta>
            <ta e="T484" id="Seg_8376" s="T483">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T485" id="Seg_8377" s="T484">v-v:inf-v-v&gt;v-v:pn</ta>
            <ta e="T486" id="Seg_8378" s="T485">n-n:case3</ta>
            <ta e="T487" id="Seg_8379" s="T486">adj</ta>
            <ta e="T488" id="Seg_8380" s="T487">n-n:case3</ta>
            <ta e="T489" id="Seg_8381" s="T488">ptcl</ta>
            <ta e="T490" id="Seg_8382" s="T489">pers</ta>
            <ta e="T491" id="Seg_8383" s="T490">nprop-n:case3</ta>
            <ta e="T492" id="Seg_8384" s="T491">nprop-n:case3</ta>
            <ta e="T493" id="Seg_8385" s="T492">v-v:tense-v:pn</ta>
            <ta e="T494" id="Seg_8386" s="T493">dem</ta>
            <ta e="T495" id="Seg_8387" s="T494">pers</ta>
            <ta e="T496" id="Seg_8388" s="T495">v-v:tense-v:pn</ta>
            <ta e="T497" id="Seg_8389" s="T496">pers</ta>
            <ta e="T498" id="Seg_8390" s="T497">n-v:ins-v:ins-v:pn</ta>
            <ta e="T499" id="Seg_8391" s="T498">v-v&gt;adv</ta>
            <ta e="T500" id="Seg_8392" s="T499">pers</ta>
            <ta e="T501" id="Seg_8393" s="T500">v-v:tense-v:pn</ta>
            <ta e="T502" id="Seg_8394" s="T501">n-n&gt;adj</ta>
            <ta e="T503" id="Seg_8395" s="T502">n-n:case3</ta>
            <ta e="T504" id="Seg_8396" s="T503">n-n:num-n:case3</ta>
            <ta e="T505" id="Seg_8397" s="T504">v-v:ins-v:pn</ta>
            <ta e="T506" id="Seg_8398" s="T505">pers</ta>
            <ta e="T507" id="Seg_8399" s="T506">nprop-v:pn</ta>
            <ta e="T508" id="Seg_8400" s="T507">adv</ta>
            <ta e="T509" id="Seg_8401" s="T508">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T510" id="Seg_8402" s="T509">dem</ta>
            <ta e="T511" id="Seg_8403" s="T510">v-v&gt;adv</ta>
            <ta e="T512" id="Seg_8404" s="T511">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T513" id="Seg_8405" s="T512">n-n:case3</ta>
            <ta e="T514" id="Seg_8406" s="T513">pers</ta>
            <ta e="T515" id="Seg_8407" s="T514">n-v:ins-v:ins-v:pn</ta>
            <ta e="T516" id="Seg_8408" s="T515">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T517" id="Seg_8409" s="T516">n-n:case3</ta>
            <ta e="T518" id="Seg_8410" s="T517">n-n:case3</ta>
            <ta e="T519" id="Seg_8411" s="T518">n-n:case3</ta>
            <ta e="T520" id="Seg_8412" s="T519">v-v&gt;v-v&gt;adv</ta>
            <ta e="T521" id="Seg_8413" s="T520">v-v:tense-v:pn</ta>
            <ta e="T522" id="Seg_8414" s="T521">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T523" id="Seg_8415" s="T522">n-n:case3</ta>
            <ta e="T524" id="Seg_8416" s="T523">adv</ta>
            <ta e="T525" id="Seg_8417" s="T524">v-v:tense-v:pn</ta>
            <ta e="T526" id="Seg_8418" s="T525">pers</ta>
            <ta e="T527" id="Seg_8419" s="T526">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T528" id="Seg_8420" s="T527">n-n:case3</ta>
            <ta e="T529" id="Seg_8421" s="T528">nprop-n:case3</ta>
            <ta e="T530" id="Seg_8422" s="T529">v-v:tense-v:pn</ta>
            <ta e="T531" id="Seg_8423" s="T530">adv</ta>
            <ta e="T532" id="Seg_8424" s="T531">n-n:case3</ta>
            <ta e="T533" id="Seg_8425" s="T532">n-n&gt;adj</ta>
            <ta e="T534" id="Seg_8426" s="T533">n-n:case3</ta>
            <ta e="T535" id="Seg_8427" s="T534">n-n:case1-n:poss</ta>
            <ta e="T536" id="Seg_8428" s="T535">n-n:case3</ta>
            <ta e="T537" id="Seg_8429" s="T536">n-v&gt;v-v:tense-v:pn</ta>
            <ta e="T538" id="Seg_8430" s="T537">n-n:case3</ta>
            <ta e="T539" id="Seg_8431" s="T538">adv</ta>
            <ta e="T540" id="Seg_8432" s="T539">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T541" id="Seg_8433" s="T540">v-v:inf</ta>
            <ta e="T542" id="Seg_8434" s="T541">dem</ta>
            <ta e="T543" id="Seg_8435" s="T542">n-n:case3</ta>
            <ta e="T544" id="Seg_8436" s="T543">n-n:case3</ta>
            <ta e="T545" id="Seg_8437" s="T544">nprop-n:case3</ta>
            <ta e="T546" id="Seg_8438" s="T545">n-n:case3</ta>
            <ta e="T547" id="Seg_8439" s="T546">v-v&gt;v-v:pn</ta>
            <ta e="T548" id="Seg_8440" s="T547">nprop-n:case3</ta>
            <ta e="T549" id="Seg_8441" s="T548">n-n:(ins)-n&gt;adj</ta>
            <ta e="T550" id="Seg_8442" s="T549">n-n:case3</ta>
            <ta e="T551" id="Seg_8443" s="T550">n-n:case3</ta>
            <ta e="T552" id="Seg_8444" s="T551">v-v&gt;v-v:pn</ta>
            <ta e="T553" id="Seg_8445" s="T552">n-n:case3</ta>
            <ta e="T554" id="Seg_8446" s="T553">v-v:pn</ta>
            <ta e="T555" id="Seg_8447" s="T554">n-n:case3</ta>
            <ta e="T556" id="Seg_8448" s="T555">pp-adv:advcase</ta>
            <ta e="T557" id="Seg_8449" s="T556">adv-n:case3</ta>
            <ta e="T558" id="Seg_8450" s="T557">adv-n:case3</ta>
            <ta e="T559" id="Seg_8451" s="T558">v-v:tense-v:pn</ta>
            <ta e="T560" id="Seg_8452" s="T559">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T561" id="Seg_8453" s="T560">v-v:mood-v:pn</ta>
            <ta e="T562" id="Seg_8454" s="T561">v-v&gt;v-v:inf</ta>
            <ta e="T563" id="Seg_8455" s="T562">n-n:case3</ta>
            <ta e="T564" id="Seg_8456" s="T563">pers-n:(ins)-n:num-n:case3</ta>
            <ta e="T565" id="Seg_8457" s="T564">v-v:tense-v:pn</ta>
            <ta e="T566" id="Seg_8458" s="T565">n-n:case3</ta>
            <ta e="T567" id="Seg_8459" s="T566">v-v:tense-v:pn</ta>
            <ta e="T568" id="Seg_8460" s="T567">n-n:case3</ta>
            <ta e="T569" id="Seg_8461" s="T568">n-n:case3</ta>
            <ta e="T570" id="Seg_8462" s="T569">pp</ta>
            <ta e="T571" id="Seg_8463" s="T570">v-v:pn</ta>
            <ta e="T572" id="Seg_8464" s="T571">n-n:(ins)-n:case3</ta>
            <ta e="T573" id="Seg_8465" s="T572">pp-adv&gt;adv</ta>
            <ta e="T574" id="Seg_8466" s="T573">nprop-n:case3</ta>
            <ta e="T575" id="Seg_8467" s="T574">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T576" id="Seg_8468" s="T575">quant</ta>
            <ta e="T577" id="Seg_8469" s="T576">n-n:case1-n:poss</ta>
            <ta e="T578" id="Seg_8470" s="T577">v-v&gt;v-v:pn</ta>
            <ta e="T579" id="Seg_8471" s="T578">n-any</ta>
            <ta e="T580" id="Seg_8472" s="T579">n-n:case1-n:poss</ta>
            <ta e="T581" id="Seg_8473" s="T580">v-v&gt;ptcp-v:ins-v:pn</ta>
            <ta e="T582" id="Seg_8474" s="T581">pers-n:(ins)-n:num-n:case3</ta>
            <ta e="T583" id="Seg_8475" s="T582">v-v:tense-v:pn</ta>
            <ta e="T584" id="Seg_8476" s="T583">v-v:tense-v:pn</ta>
            <ta e="T585" id="Seg_8477" s="T584">interj</ta>
            <ta e="T586" id="Seg_8478" s="T585">n-n:case3</ta>
            <ta e="T587" id="Seg_8479" s="T586">conj</ta>
            <ta e="T588" id="Seg_8480" s="T587">nprop-n:case3</ta>
            <ta e="T589" id="Seg_8481" s="T588">v-v&gt;v-v&gt;adv</ta>
            <ta e="T590" id="Seg_8482" s="T589">v-v:pn</ta>
            <ta e="T591" id="Seg_8483" s="T590">pers</ta>
            <ta e="T592" id="Seg_8484" s="T591">pers</ta>
            <ta e="T593" id="Seg_8485" s="T592">n-any</ta>
            <ta e="T594" id="Seg_8486" s="T593">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T595" id="Seg_8487" s="T594">pers</ta>
            <ta e="T596" id="Seg_8488" s="T595">n-n:case3</ta>
            <ta e="T597" id="Seg_8489" s="T596">pp</ta>
            <ta e="T598" id="Seg_8490" s="T597">quant-adj&gt;adv</ta>
            <ta e="T599" id="Seg_8491" s="T598">n-n:case3</ta>
            <ta e="T600" id="Seg_8492" s="T599">n-n:case3</ta>
            <ta e="T601" id="Seg_8493" s="T600">v-v:tense-v:pn</ta>
            <ta e="T602" id="Seg_8494" s="T601">quant</ta>
            <ta e="T603" id="Seg_8495" s="T602">v-v:ins-v:pn</ta>
            <ta e="T604" id="Seg_8496" s="T603">interrog</ta>
            <ta e="T605" id="Seg_8497" s="T604">n-n:case1-n:poss</ta>
            <ta e="T606" id="Seg_8498" s="T605">v-v:tense-v:pn</ta>
            <ta e="T607" id="Seg_8499" s="T606">v-v:tense-v:pn</ta>
            <ta e="T608" id="Seg_8500" s="T607">pers</ta>
            <ta e="T609" id="Seg_8501" s="T608">n-n:case1-n:poss</ta>
            <ta e="T610" id="Seg_8502" s="T609">ptcl</ta>
            <ta e="T611" id="Seg_8503" s="T610">v-v:tense-v:pn</ta>
            <ta e="T612" id="Seg_8504" s="T611">ptcl</ta>
            <ta e="T613" id="Seg_8505" s="T612">n-any-n:(ins)-n&gt;adj</ta>
            <ta e="T614" id="Seg_8506" s="T613">n-n:case3</ta>
            <ta e="T615" id="Seg_8507" s="T614">v-v:tense-v:pn</ta>
            <ta e="T616" id="Seg_8508" s="T615">pers-n:(ins)-n:num-n:case3</ta>
            <ta e="T617" id="Seg_8509" s="T616">v-v:tense-v:pn</ta>
            <ta e="T618" id="Seg_8510" s="T617">v-v&gt;v-v&gt;adv</ta>
            <ta e="T619" id="Seg_8511" s="T618">interrog-n&gt;adj</ta>
            <ta e="T620" id="Seg_8512" s="T619">n-n:case3</ta>
            <ta e="T621" id="Seg_8513" s="T620">v-v:tense-v:pn</ta>
            <ta e="T622" id="Seg_8514" s="T621">nprop-n:case3</ta>
            <ta e="T623" id="Seg_8515" s="T622">n-n:case3</ta>
            <ta e="T624" id="Seg_8516" s="T623">v-v:inf</ta>
            <ta e="T625" id="Seg_8517" s="T624">nprop-n:case3</ta>
            <ta e="T626" id="Seg_8518" s="T625">n-n:case3</ta>
            <ta e="T627" id="Seg_8519" s="T626">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T629" id="Seg_8520" s="T628">n-n:case3</ta>
            <ta e="T630" id="Seg_8521" s="T629">adv</ta>
            <ta e="T631" id="Seg_8522" s="T630">pers-n:case3</ta>
            <ta e="T632" id="Seg_8523" s="T631">adj</ta>
            <ta e="T633" id="Seg_8524" s="T632">v-v:tense-v:pn</ta>
            <ta e="T634" id="Seg_8525" s="T633">n-n:case3</ta>
            <ta e="T635" id="Seg_8526" s="T634">n-n:case3</ta>
            <ta e="T636" id="Seg_8527" s="T635">interrog-n&gt;adj</ta>
            <ta e="T637" id="Seg_8528" s="T636">n-n:case3</ta>
            <ta e="T638" id="Seg_8529" s="T637">quant</ta>
            <ta e="T639" id="Seg_8530" s="T638">v-v:tense-v:pn</ta>
            <ta e="T640" id="Seg_8531" s="T639">conj</ta>
            <ta e="T641" id="Seg_8532" s="T640">n-n:case3</ta>
            <ta e="T642" id="Seg_8533" s="T641">n-n&gt;adj</ta>
            <ta e="T643" id="Seg_8534" s="T642">n-n:num-n:case3</ta>
            <ta e="T644" id="Seg_8535" s="T643">nprop-n:case3</ta>
            <ta e="T645" id="Seg_8536" s="T644">n-n:case3</ta>
            <ta e="T646" id="Seg_8537" s="T645">n-n:case1-n:poss</ta>
            <ta e="T647" id="Seg_8538" s="T646">v-v:tense-v:pn</ta>
            <ta e="T648" id="Seg_8539" s="T647">interrog</ta>
            <ta e="T649" id="Seg_8540" s="T648">dem-adj&gt;adv</ta>
            <ta e="T650" id="Seg_8541" s="T649">v-v:tense-v:pn</ta>
            <ta e="T651" id="Seg_8542" s="T650">nprop-n:case3</ta>
            <ta e="T652" id="Seg_8543" s="T651">n-n:case3</ta>
            <ta e="T655" id="Seg_8544" s="T654">adj-n&gt;adj</ta>
            <ta e="T656" id="Seg_8545" s="T655">n-n:case3</ta>
            <ta e="T657" id="Seg_8546" s="T656">pro</ta>
            <ta e="T658" id="Seg_8547" s="T657">num-n:num</ta>
            <ta e="T659" id="Seg_8548" s="T658">n-n:num-n:case3</ta>
            <ta e="T660" id="Seg_8549" s="T659">v-any-v:pn</ta>
            <ta e="T661" id="Seg_8550" s="T660">nprop-n:case3</ta>
            <ta e="T662" id="Seg_8551" s="T661">v-v:pn</ta>
            <ta e="T663" id="Seg_8552" s="T662">n-n:case3</ta>
            <ta e="T664" id="Seg_8553" s="T663">n-n:case3</ta>
            <ta e="T665" id="Seg_8554" s="T664">v-v:inf</ta>
            <ta e="T666" id="Seg_8555" s="T665">nprop-n:case3</ta>
            <ta e="T667" id="Seg_8556" s="T666">conj</ta>
            <ta e="T668" id="Seg_8557" s="T667">v-v:tense-v:pn</ta>
            <ta e="T669" id="Seg_8558" s="T668">dem</ta>
            <ta e="T670" id="Seg_8559" s="T669">dem</ta>
            <ta e="T671" id="Seg_8560" s="T670">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T672" id="Seg_8561" s="T671">pers</ta>
            <ta e="T673" id="Seg_8562" s="T672">ptcl</ta>
            <ta e="T674" id="Seg_8563" s="T673">n-n:case3</ta>
            <ta e="T675" id="Seg_8564" s="T674">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T676" id="Seg_8565" s="T675">n-n:(ins)-n:case3</ta>
            <ta e="T677" id="Seg_8566" s="T676">nprop-n:case3</ta>
            <ta e="T678" id="Seg_8567" s="T677">v-v:tense-v:pn</ta>
            <ta e="T679" id="Seg_8568" s="T678">v-v&gt;v-v:mood-pn</ta>
            <ta e="T680" id="Seg_8569" s="T679">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T681" id="Seg_8570" s="T680">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T682" id="Seg_8571" s="T681">n-n:case3</ta>
            <ta e="T683" id="Seg_8572" s="T682">n-n:case3</ta>
            <ta e="T684" id="Seg_8573" s="T683">pp-adv:advcase</ta>
            <ta e="T685" id="Seg_8574" s="T684">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T686" id="Seg_8575" s="T685">v-v:mood-pn</ta>
            <ta e="T687" id="Seg_8576" s="T686">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T688" id="Seg_8577" s="T687">n-n:case3</ta>
            <ta e="T689" id="Seg_8578" s="T688">n-n&gt;adj</ta>
            <ta e="T690" id="Seg_8579" s="T689">v-v:mood-pn</ta>
            <ta e="T691" id="Seg_8580" s="T690">n-n:case3</ta>
            <ta e="T692" id="Seg_8581" s="T691">n-n:case3</ta>
            <ta e="T693" id="Seg_8582" s="T692">pers</ta>
            <ta e="T694" id="Seg_8583" s="T693">pers</ta>
            <ta e="T695" id="Seg_8584" s="T694">v-v:tense-v:pn</ta>
            <ta e="T696" id="Seg_8585" s="T695">n-n:case3</ta>
            <ta e="T697" id="Seg_8586" s="T696">v-v:tense-v:pn</ta>
            <ta e="T698" id="Seg_8587" s="T697">n-n:case3</ta>
            <ta e="T699" id="Seg_8588" s="T698">n-n:case3</ta>
            <ta e="T700" id="Seg_8589" s="T699">adj</ta>
            <ta e="T701" id="Seg_8590" s="T700">n-n:case3</ta>
            <ta e="T702" id="Seg_8591" s="T701">pers</ta>
            <ta e="T703" id="Seg_8592" s="T702">v-v:tense-v:pn</ta>
            <ta e="T704" id="Seg_8593" s="T703">quant</ta>
            <ta e="T705" id="Seg_8594" s="T704">n-n:case3</ta>
            <ta e="T706" id="Seg_8595" s="T705">adv</ta>
            <ta e="T707" id="Seg_8596" s="T706">nprop-n:case3</ta>
            <ta e="T708" id="Seg_8597" s="T707">n-n:case3</ta>
            <ta e="T709" id="Seg_8598" s="T708">n-n:case3</ta>
            <ta e="T710" id="Seg_8599" s="T709">interrog-n:case3</ta>
            <ta e="T711" id="Seg_8600" s="T710">v-v:pn</ta>
            <ta e="T712" id="Seg_8601" s="T711">n-n:case3</ta>
            <ta e="T713" id="Seg_8602" s="T712">pers-n:case3</ta>
            <ta e="T714" id="Seg_8603" s="T713">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T715" id="Seg_8604" s="T714">adv</ta>
            <ta e="T716" id="Seg_8605" s="T715">nprop-n:case3</ta>
            <ta e="T717" id="Seg_8606" s="T716">n-n:case3</ta>
            <ta e="T718" id="Seg_8607" s="T717">adv</ta>
            <ta e="T719" id="Seg_8608" s="T718">adj</ta>
            <ta e="T720" id="Seg_8609" s="T719">n-n:num-n:case1-n:poss</ta>
            <ta e="T721" id="Seg_8610" s="T720">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T722" id="Seg_8611" s="T721">adv</ta>
            <ta e="T723" id="Seg_8612" s="T722">nprop-n:case3</ta>
            <ta e="T724" id="Seg_8613" s="T723">pro</ta>
            <ta e="T725" id="Seg_8614" s="T724">v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T726" id="Seg_8615" s="T725">v-n:(ins)-v&gt;ptcp</ta>
            <ta e="T727" id="Seg_8616" s="T726">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T728" id="Seg_8617" s="T727">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T729" id="Seg_8618" s="T728">n-n:case3</ta>
            <ta e="T730" id="Seg_8619" s="T729">v-v:tense-v:pn</ta>
            <ta e="T731" id="Seg_8620" s="T730">nprop-n:case3</ta>
            <ta e="T732" id="Seg_8621" s="T731">n-n:case3</ta>
            <ta e="T733" id="Seg_8622" s="T732">v-v:mood-pn</ta>
            <ta e="T734" id="Seg_8623" s="T733">n-n:case3</ta>
            <ta e="T735" id="Seg_8624" s="T734">n-n:case3</ta>
            <ta e="T736" id="Seg_8625" s="T735">pp</ta>
            <ta e="T737" id="Seg_8626" s="T736">ptcl</ta>
            <ta e="T738" id="Seg_8627" s="T737">interrog</ta>
            <ta e="T739" id="Seg_8628" s="T738">ptcl</ta>
            <ta e="T740" id="Seg_8629" s="T739">v-v:tense-v:pn</ta>
            <ta e="T741" id="Seg_8630" s="T740">n-n:case1-n:(ins)-n:poss</ta>
            <ta e="T742" id="Seg_8631" s="T741">conj</ta>
            <ta e="T743" id="Seg_8632" s="T742">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T744" id="Seg_8633" s="T743">adv</ta>
            <ta e="T745" id="Seg_8634" s="T744">v-v:tense-v:pn</ta>
            <ta e="T746" id="Seg_8635" s="T745">pers</ta>
            <ta e="T747" id="Seg_8636" s="T746">ptcl</ta>
            <ta e="T748" id="Seg_8637" s="T747">pers</ta>
            <ta e="T749" id="Seg_8638" s="T748">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T750" id="Seg_8639" s="T749">n-n:case3</ta>
            <ta e="T751" id="Seg_8640" s="T750">pers</ta>
            <ta e="T752" id="Seg_8641" s="T751">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_8642" s="T0">nprop</ta>
            <ta e="T2" id="Seg_8643" s="T1">n</ta>
            <ta e="T3" id="Seg_8644" s="T2">v</ta>
            <ta e="T4" id="Seg_8645" s="T3">nprop</ta>
            <ta e="T5" id="Seg_8646" s="T4">n</ta>
            <ta e="T6" id="Seg_8647" s="T5">nprop</ta>
            <ta e="T7" id="Seg_8648" s="T6">adj</ta>
            <ta e="T8" id="Seg_8649" s="T7">n</ta>
            <ta e="T9" id="Seg_8650" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_8651" s="T9">adv</ta>
            <ta e="T11" id="Seg_8652" s="T10">v</ta>
            <ta e="T12" id="Seg_8653" s="T11">num</ta>
            <ta e="T13" id="Seg_8654" s="T12">pp</ta>
            <ta e="T14" id="Seg_8655" s="T13">nprop</ta>
            <ta e="T15" id="Seg_8656" s="T14">adj</ta>
            <ta e="T16" id="Seg_8657" s="T15">n</ta>
            <ta e="T17" id="Seg_8658" s="T16">v</ta>
            <ta e="T18" id="Seg_8659" s="T17">v</ta>
            <ta e="T20" id="Seg_8660" s="T19">v</ta>
            <ta e="T21" id="Seg_8661" s="T20">pers</ta>
            <ta e="T22" id="Seg_8662" s="T21">v</ta>
            <ta e="T23" id="Seg_8663" s="T22">conj</ta>
            <ta e="T24" id="Seg_8664" s="T23">nprop</ta>
            <ta e="T25" id="Seg_8665" s="T24">v</ta>
            <ta e="T26" id="Seg_8666" s="T25">v</ta>
            <ta e="T27" id="Seg_8667" s="T26">pers</ta>
            <ta e="T28" id="Seg_8668" s="T27">n</ta>
            <ta e="T29" id="Seg_8669" s="T28">num</ta>
            <ta e="T30" id="Seg_8670" s="T29">v</ta>
            <ta e="T31" id="Seg_8671" s="T30">interrog</ta>
            <ta e="T32" id="Seg_8672" s="T31">v</ta>
            <ta e="T33" id="Seg_8673" s="T32">nprop</ta>
            <ta e="T34" id="Seg_8674" s="T33">pers</ta>
            <ta e="T35" id="Seg_8675" s="T34">v</ta>
            <ta e="T36" id="Seg_8676" s="T35">v</ta>
            <ta e="T37" id="Seg_8677" s="T36">n</ta>
            <ta e="T38" id="Seg_8678" s="T37">n</ta>
            <ta e="T39" id="Seg_8679" s="T38">pers</ta>
            <ta e="T40" id="Seg_8680" s="T39">v</ta>
            <ta e="T41" id="Seg_8681" s="T40">v</ta>
            <ta e="T42" id="Seg_8682" s="T41">n</ta>
            <ta e="T43" id="Seg_8683" s="T42">dem</ta>
            <ta e="T44" id="Seg_8684" s="T43">n</ta>
            <ta e="T45" id="Seg_8685" s="T44">dem</ta>
            <ta e="T46" id="Seg_8686" s="T45">n</ta>
            <ta e="T47" id="Seg_8687" s="T46">n</ta>
            <ta e="T48" id="Seg_8688" s="T47">v</ta>
            <ta e="T49" id="Seg_8689" s="T48">n</ta>
            <ta e="T50" id="Seg_8690" s="T49">n</ta>
            <ta e="T51" id="Seg_8691" s="T50">v</ta>
            <ta e="T52" id="Seg_8692" s="T51">v</ta>
            <ta e="T53" id="Seg_8693" s="T52">adj</ta>
            <ta e="T54" id="Seg_8694" s="T53">n</ta>
            <ta e="T55" id="Seg_8695" s="T54">n</ta>
            <ta e="T56" id="Seg_8696" s="T55">n</ta>
            <ta e="T57" id="Seg_8697" s="T56">n</ta>
            <ta e="T58" id="Seg_8698" s="T57">v</ta>
            <ta e="T59" id="Seg_8699" s="T58">adj</ta>
            <ta e="T60" id="Seg_8700" s="T59">n</ta>
            <ta e="T61" id="Seg_8701" s="T60">n</ta>
            <ta e="T62" id="Seg_8702" s="T61">n</ta>
            <ta e="T63" id="Seg_8703" s="T62">v</ta>
            <ta e="T64" id="Seg_8704" s="T63">n</ta>
            <ta e="T65" id="Seg_8705" s="T64">n</ta>
            <ta e="T66" id="Seg_8706" s="T65">n</ta>
            <ta e="T67" id="Seg_8707" s="T66">v</ta>
            <ta e="T68" id="Seg_8708" s="T67">nprop</ta>
            <ta e="T69" id="Seg_8709" s="T68">adj</ta>
            <ta e="T70" id="Seg_8710" s="T69">n</ta>
            <ta e="T71" id="Seg_8711" s="T70">v</ta>
            <ta e="T72" id="Seg_8712" s="T71">nprop</ta>
            <ta e="T73" id="Seg_8713" s="T72">pers</ta>
            <ta e="T74" id="Seg_8714" s="T73">v</ta>
            <ta e="T75" id="Seg_8715" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_8716" s="T75">n</ta>
            <ta e="T77" id="Seg_8717" s="T76">pers</ta>
            <ta e="T78" id="Seg_8718" s="T77">n</ta>
            <ta e="T79" id="Seg_8719" s="T78">v</ta>
            <ta e="T80" id="Seg_8720" s="T79">v</ta>
            <ta e="T81" id="Seg_8721" s="T80">pers</ta>
            <ta e="T82" id="Seg_8722" s="T81">v</ta>
            <ta e="T83" id="Seg_8723" s="T82">n</ta>
            <ta e="T84" id="Seg_8724" s="T83">v</ta>
            <ta e="T85" id="Seg_8725" s="T84">pers</ta>
            <ta e="T86" id="Seg_8726" s="T85">v</ta>
            <ta e="T87" id="Seg_8727" s="T86">n</ta>
            <ta e="T88" id="Seg_8728" s="T87">n</ta>
            <ta e="T89" id="Seg_8729" s="T88">v</ta>
            <ta e="T90" id="Seg_8730" s="T89">adv</ta>
            <ta e="T91" id="Seg_8731" s="T90">n</ta>
            <ta e="T92" id="Seg_8732" s="T91">v</ta>
            <ta e="T93" id="Seg_8733" s="T92">n</ta>
            <ta e="T94" id="Seg_8734" s="T93">n</ta>
            <ta e="T95" id="Seg_8735" s="T94">v</ta>
            <ta e="T96" id="Seg_8736" s="T95">n</ta>
            <ta e="T97" id="Seg_8737" s="T96">v</ta>
            <ta e="T98" id="Seg_8738" s="T97">n</ta>
            <ta e="T99" id="Seg_8739" s="T98">n</ta>
            <ta e="T100" id="Seg_8740" s="T99">nprop</ta>
            <ta e="T101" id="Seg_8741" s="T100">n</ta>
            <ta e="T102" id="Seg_8742" s="T101">pers</ta>
            <ta e="T103" id="Seg_8743" s="T102">n</ta>
            <ta e="T104" id="Seg_8744" s="T103">n</ta>
            <ta e="T105" id="Seg_8745" s="T104">ptcl</ta>
            <ta e="T106" id="Seg_8746" s="T105">v</ta>
            <ta e="T107" id="Seg_8747" s="T106">v</ta>
            <ta e="T108" id="Seg_8748" s="T107">v</ta>
            <ta e="T109" id="Seg_8749" s="T108">nprop</ta>
            <ta e="T110" id="Seg_8750" s="T109">n</ta>
            <ta e="T111" id="Seg_8751" s="T110">adv</ta>
            <ta e="T112" id="Seg_8752" s="T111">v</ta>
            <ta e="T113" id="Seg_8753" s="T112">n</ta>
            <ta e="T114" id="Seg_8754" s="T113">v</ta>
            <ta e="T115" id="Seg_8755" s="T114">n</ta>
            <ta e="T116" id="Seg_8756" s="T115">v</ta>
            <ta e="T117" id="Seg_8757" s="T116">adv</ta>
            <ta e="T118" id="Seg_8758" s="T117">nprop</ta>
            <ta e="T119" id="Seg_8759" s="T118">v</ta>
            <ta e="T120" id="Seg_8760" s="T119">n</ta>
            <ta e="T121" id="Seg_8761" s="T120">n</ta>
            <ta e="T122" id="Seg_8762" s="T121">adv</ta>
            <ta e="T123" id="Seg_8763" s="T122">adv</ta>
            <ta e="T124" id="Seg_8764" s="T123">v</ta>
            <ta e="T125" id="Seg_8765" s="T124">n</ta>
            <ta e="T126" id="Seg_8766" s="T125">n</ta>
            <ta e="T127" id="Seg_8767" s="T126">v</ta>
            <ta e="T128" id="Seg_8768" s="T127">n</ta>
            <ta e="T129" id="Seg_8769" s="T128">preverb</ta>
            <ta e="T130" id="Seg_8770" s="T129">v</ta>
            <ta e="T131" id="Seg_8771" s="T130">n</ta>
            <ta e="T132" id="Seg_8772" s="T131">v</ta>
            <ta e="T133" id="Seg_8773" s="T132">adv</ta>
            <ta e="T134" id="Seg_8774" s="T133">n</ta>
            <ta e="T135" id="Seg_8775" s="T134">v</ta>
            <ta e="T136" id="Seg_8776" s="T135">adv</ta>
            <ta e="T137" id="Seg_8777" s="T136">v</ta>
            <ta e="T138" id="Seg_8778" s="T137">n</ta>
            <ta e="T139" id="Seg_8779" s="T138">v</ta>
            <ta e="T140" id="Seg_8780" s="T139">n</ta>
            <ta e="T141" id="Seg_8781" s="T140">n</ta>
            <ta e="T142" id="Seg_8782" s="T141">pp</ta>
            <ta e="T143" id="Seg_8783" s="T142">n</ta>
            <ta e="T144" id="Seg_8784" s="T143">v</ta>
            <ta e="T145" id="Seg_8785" s="T144">n</ta>
            <ta e="T146" id="Seg_8786" s="T145">n</ta>
            <ta e="T147" id="Seg_8787" s="T146">v</ta>
            <ta e="T148" id="Seg_8788" s="T147">n</ta>
            <ta e="T149" id="Seg_8789" s="T148">adv</ta>
            <ta e="T150" id="Seg_8790" s="T149">n</ta>
            <ta e="T151" id="Seg_8791" s="T150">v</ta>
            <ta e="T152" id="Seg_8792" s="T151">pers</ta>
            <ta e="T153" id="Seg_8793" s="T152">adv</ta>
            <ta e="T154" id="Seg_8794" s="T153">v</ta>
            <ta e="T155" id="Seg_8795" s="T154">pers</ta>
            <ta e="T156" id="Seg_8796" s="T155">pers</ta>
            <ta e="T157" id="Seg_8797" s="T156">n</ta>
            <ta e="T158" id="Seg_8798" s="T157">v</ta>
            <ta e="T159" id="Seg_8799" s="T158">pers</ta>
            <ta e="T160" id="Seg_8800" s="T159">n</ta>
            <ta e="T161" id="Seg_8801" s="T160">v</ta>
            <ta e="T162" id="Seg_8802" s="T161">conj</ta>
            <ta e="T163" id="Seg_8803" s="T162">v</ta>
            <ta e="T164" id="Seg_8804" s="T163">n</ta>
            <ta e="T165" id="Seg_8805" s="T164">ptcl</ta>
            <ta e="T166" id="Seg_8806" s="T165">v</ta>
            <ta e="T167" id="Seg_8807" s="T166">nprop</ta>
            <ta e="T168" id="Seg_8808" s="T167">v</ta>
            <ta e="T169" id="Seg_8809" s="T168">v</ta>
            <ta e="T170" id="Seg_8810" s="T169">nprop</ta>
            <ta e="T171" id="Seg_8811" s="T170">n</ta>
            <ta e="T172" id="Seg_8812" s="T171">v</ta>
            <ta e="T173" id="Seg_8813" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_8814" s="T173">v</ta>
            <ta e="T175" id="Seg_8815" s="T174">n</ta>
            <ta e="T176" id="Seg_8816" s="T175">v</ta>
            <ta e="T177" id="Seg_8817" s="T176">v</ta>
            <ta e="T178" id="Seg_8818" s="T177">n</ta>
            <ta e="T179" id="Seg_8819" s="T178">adj</ta>
            <ta e="T180" id="Seg_8820" s="T179">n</ta>
            <ta e="T181" id="Seg_8821" s="T180">v</ta>
            <ta e="T182" id="Seg_8822" s="T181">nprop</ta>
            <ta e="T183" id="Seg_8823" s="T182">n</ta>
            <ta e="T184" id="Seg_8824" s="T183">v</ta>
            <ta e="T185" id="Seg_8825" s="T184">n</ta>
            <ta e="T186" id="Seg_8826" s="T185">pers</ta>
            <ta e="T187" id="Seg_8827" s="T186">v</ta>
            <ta e="T188" id="Seg_8828" s="T187">pers</ta>
            <ta e="T189" id="Seg_8829" s="T188">v</ta>
            <ta e="T190" id="Seg_8830" s="T189">pers</ta>
            <ta e="T191" id="Seg_8831" s="T190">n</ta>
            <ta e="T192" id="Seg_8832" s="T191">conj</ta>
            <ta e="T193" id="Seg_8833" s="T192">n</ta>
            <ta e="T194" id="Seg_8834" s="T193">v</ta>
            <ta e="T195" id="Seg_8835" s="T194">adv</ta>
            <ta e="T196" id="Seg_8836" s="T195">conj</ta>
            <ta e="T197" id="Seg_8837" s="T196">n</ta>
            <ta e="T198" id="Seg_8838" s="T197">v</ta>
            <ta e="T199" id="Seg_8839" s="T198">nprop</ta>
            <ta e="T200" id="Seg_8840" s="T199">v</ta>
            <ta e="T201" id="Seg_8841" s="T200">v</ta>
            <ta e="T202" id="Seg_8842" s="T201">n</ta>
            <ta e="T203" id="Seg_8843" s="T202">adv</ta>
            <ta e="T204" id="Seg_8844" s="T203">v</ta>
            <ta e="T205" id="Seg_8845" s="T204">adj</ta>
            <ta e="T206" id="Seg_8846" s="T205">n</ta>
            <ta e="T207" id="Seg_8847" s="T206">v</ta>
            <ta e="T208" id="Seg_8848" s="T207">nprop</ta>
            <ta e="T209" id="Seg_8849" s="T208">n</ta>
            <ta e="T210" id="Seg_8850" s="T209">v</ta>
            <ta e="T211" id="Seg_8851" s="T210">n</ta>
            <ta e="T212" id="Seg_8852" s="T211">adv</ta>
            <ta e="T213" id="Seg_8853" s="T212">v</ta>
            <ta e="T214" id="Seg_8854" s="T213">v</ta>
            <ta e="T215" id="Seg_8855" s="T214">n</ta>
            <ta e="T216" id="Seg_8856" s="T215">v</ta>
            <ta e="T217" id="Seg_8857" s="T216">conj</ta>
            <ta e="T218" id="Seg_8858" s="T217">n</ta>
            <ta e="T219" id="Seg_8859" s="T218">v</ta>
            <ta e="T220" id="Seg_8860" s="T219">pers</ta>
            <ta e="T221" id="Seg_8861" s="T220">adv</ta>
            <ta e="T222" id="Seg_8862" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_8863" s="T222">v</ta>
            <ta e="T224" id="Seg_8864" s="T223">v</ta>
            <ta e="T225" id="Seg_8865" s="T224">v</ta>
            <ta e="T226" id="Seg_8866" s="T225">v</ta>
            <ta e="T227" id="Seg_8867" s="T226">n</ta>
            <ta e="T228" id="Seg_8868" s="T227">v</ta>
            <ta e="T229" id="Seg_8869" s="T228">quant</ta>
            <ta e="T230" id="Seg_8870" s="T229">n</ta>
            <ta e="T231" id="Seg_8871" s="T230">v</ta>
            <ta e="T232" id="Seg_8872" s="T231">n</ta>
            <ta e="T233" id="Seg_8873" s="T232">quant</ta>
            <ta e="T234" id="Seg_8874" s="T233">ptcp</ta>
            <ta e="T235" id="Seg_8875" s="T234">n</ta>
            <ta e="T236" id="Seg_8876" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_8877" s="T236">v</ta>
            <ta e="T238" id="Seg_8878" s="T237">adv</ta>
            <ta e="T239" id="Seg_8879" s="T238">v</ta>
            <ta e="T240" id="Seg_8880" s="T239">n</ta>
            <ta e="T241" id="Seg_8881" s="T240">v</ta>
            <ta e="T242" id="Seg_8882" s="T241">n</ta>
            <ta e="T243" id="Seg_8883" s="T242">v</ta>
            <ta e="T244" id="Seg_8884" s="T243">interrog</ta>
            <ta e="T245" id="Seg_8885" s="T244">v</ta>
            <ta e="T246" id="Seg_8886" s="T245">n</ta>
            <ta e="T247" id="Seg_8887" s="T246">pers</ta>
            <ta e="T248" id="Seg_8888" s="T247">nprop</ta>
            <ta e="T249" id="Seg_8889" s="T248">pers</ta>
            <ta e="T250" id="Seg_8890" s="T249">n</ta>
            <ta e="T251" id="Seg_8891" s="T250">v</ta>
            <ta e="T252" id="Seg_8892" s="T251">pers</ta>
            <ta e="T253" id="Seg_8893" s="T252">nprop</ta>
            <ta e="T254" id="Seg_8894" s="T253">v</ta>
            <ta e="T255" id="Seg_8895" s="T254">n</ta>
            <ta e="T256" id="Seg_8896" s="T255">v</ta>
            <ta e="T257" id="Seg_8897" s="T256">v</ta>
            <ta e="T258" id="Seg_8898" s="T257">n</ta>
            <ta e="T259" id="Seg_8899" s="T258">n</ta>
            <ta e="T260" id="Seg_8900" s="T259">n</ta>
            <ta e="T261" id="Seg_8901" s="T260">quant</ta>
            <ta e="T262" id="Seg_8902" s="T261">v</ta>
            <ta e="T263" id="Seg_8903" s="T262">n</ta>
            <ta e="T264" id="Seg_8904" s="T263">adv</ta>
            <ta e="T265" id="Seg_8905" s="T264">pers</ta>
            <ta e="T266" id="Seg_8906" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_8907" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_8908" s="T267">adv</ta>
            <ta e="T269" id="Seg_8909" s="T268">v</ta>
            <ta e="T270" id="Seg_8910" s="T269">pers</ta>
            <ta e="T271" id="Seg_8911" s="T270">v</ta>
            <ta e="T272" id="Seg_8912" s="T271">num</ta>
            <ta e="T273" id="Seg_8913" s="T272">n</ta>
            <ta e="T274" id="Seg_8914" s="T273">pers</ta>
            <ta e="T275" id="Seg_8915" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_8916" s="T275">v</ta>
            <ta e="T277" id="Seg_8917" s="T276">pers</ta>
            <ta e="T278" id="Seg_8918" s="T277">n</ta>
            <ta e="T279" id="Seg_8919" s="T278">ptcl</ta>
            <ta e="T280" id="Seg_8920" s="T279">v</ta>
            <ta e="T281" id="Seg_8921" s="T280">nprop</ta>
            <ta e="T282" id="Seg_8922" s="T281">n</ta>
            <ta e="T283" id="Seg_8923" s="T282">adv</ta>
            <ta e="T284" id="Seg_8924" s="T283">v</ta>
            <ta e="T285" id="Seg_8925" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_8926" s="T285">v</ta>
            <ta e="T287" id="Seg_8927" s="T286">n</ta>
            <ta e="T288" id="Seg_8928" s="T287">n</ta>
            <ta e="T289" id="Seg_8929" s="T288">adv</ta>
            <ta e="T290" id="Seg_8930" s="T289">adv</ta>
            <ta e="T291" id="Seg_8931" s="T290">v</ta>
            <ta e="T292" id="Seg_8932" s="T291">pro</ta>
            <ta e="T293" id="Seg_8933" s="T292">n</ta>
            <ta e="T294" id="Seg_8934" s="T293">n</ta>
            <ta e="T295" id="Seg_8935" s="T294">v</ta>
            <ta e="T296" id="Seg_8936" s="T295">n</ta>
            <ta e="T297" id="Seg_8937" s="T296">adv</ta>
            <ta e="T298" id="Seg_8938" s="T297">adv</ta>
            <ta e="T299" id="Seg_8939" s="T298">v</ta>
            <ta e="T300" id="Seg_8940" s="T299">nprop</ta>
            <ta e="T301" id="Seg_8941" s="T300">n</ta>
            <ta e="T302" id="Seg_8942" s="T301">pers</ta>
            <ta e="T303" id="Seg_8943" s="T302">n</ta>
            <ta e="T304" id="Seg_8944" s="T303">adv</ta>
            <ta e="T305" id="Seg_8945" s="T304">v</ta>
            <ta e="T306" id="Seg_8946" s="T305">n</ta>
            <ta e="T307" id="Seg_8947" s="T306">v</ta>
            <ta e="T308" id="Seg_8948" s="T307">nprop</ta>
            <ta e="T309" id="Seg_8949" s="T308">v</ta>
            <ta e="T310" id="Seg_8950" s="T309">ptcl</ta>
            <ta e="T311" id="Seg_8951" s="T310">n</ta>
            <ta e="T312" id="Seg_8952" s="T311">v</ta>
            <ta e="T313" id="Seg_8953" s="T312">nprop</ta>
            <ta e="T314" id="Seg_8954" s="T313">dem</ta>
            <ta e="T315" id="Seg_8955" s="T314">n</ta>
            <ta e="T316" id="Seg_8956" s="T315">n</ta>
            <ta e="T317" id="Seg_8957" s="T316">nprop</ta>
            <ta e="T318" id="Seg_8958" s="T317">v</ta>
            <ta e="T319" id="Seg_8959" s="T318">adv</ta>
            <ta e="T320" id="Seg_8960" s="T319">v</ta>
            <ta e="T321" id="Seg_8961" s="T320">pers</ta>
            <ta e="T322" id="Seg_8962" s="T321">quant</ta>
            <ta e="T323" id="Seg_8963" s="T322">n</ta>
            <ta e="T324" id="Seg_8964" s="T323">conj</ta>
            <ta e="T325" id="Seg_8965" s="T324">n</ta>
            <ta e="T326" id="Seg_8966" s="T325">v</ta>
            <ta e="T327" id="Seg_8967" s="T326">pers</ta>
            <ta e="T328" id="Seg_8968" s="T327">v</ta>
            <ta e="T329" id="Seg_8969" s="T328">n</ta>
            <ta e="T330" id="Seg_8970" s="T329">adv</ta>
            <ta e="T331" id="Seg_8971" s="T330">nprop</ta>
            <ta e="T332" id="Seg_8972" s="T331">v</ta>
            <ta e="T333" id="Seg_8973" s="T332">n</ta>
            <ta e="T334" id="Seg_8974" s="T333">nprop</ta>
            <ta e="T335" id="Seg_8975" s="T334">n</ta>
            <ta e="T336" id="Seg_8976" s="T335">v</ta>
            <ta e="T337" id="Seg_8977" s="T336">preverb</ta>
            <ta e="T338" id="Seg_8978" s="T337">v</ta>
            <ta e="T339" id="Seg_8979" s="T338">n</ta>
            <ta e="T340" id="Seg_8980" s="T339">v</ta>
            <ta e="T341" id="Seg_8981" s="T340">n</ta>
            <ta e="T342" id="Seg_8982" s="T341">n</ta>
            <ta e="T343" id="Seg_8983" s="T342">pp</ta>
            <ta e="T344" id="Seg_8984" s="T343">v</ta>
            <ta e="T345" id="Seg_8985" s="T344">adv</ta>
            <ta e="T346" id="Seg_8986" s="T345">n</ta>
            <ta e="T347" id="Seg_8987" s="T346">pers</ta>
            <ta e="T348" id="Seg_8988" s="T347">v</ta>
            <ta e="T349" id="Seg_8989" s="T348">adj</ta>
            <ta e="T350" id="Seg_8990" s="T349">n</ta>
            <ta e="T351" id="Seg_8991" s="T350">n</ta>
            <ta e="T352" id="Seg_8992" s="T351">n</ta>
            <ta e="T353" id="Seg_8993" s="T352">n</ta>
            <ta e="T354" id="Seg_8994" s="T353">n</ta>
            <ta e="T355" id="Seg_8995" s="T354">pers</ta>
            <ta e="T356" id="Seg_8996" s="T355">n</ta>
            <ta e="T357" id="Seg_8997" s="T356">pp</ta>
            <ta e="T358" id="Seg_8998" s="T357">adv</ta>
            <ta e="T359" id="Seg_8999" s="T358">v</ta>
            <ta e="T360" id="Seg_9000" s="T359">v</ta>
            <ta e="T361" id="Seg_9001" s="T360">pers</ta>
            <ta e="T362" id="Seg_9002" s="T361">v</ta>
            <ta e="T363" id="Seg_9003" s="T362">adj</ta>
            <ta e="T364" id="Seg_9004" s="T363">n</ta>
            <ta e="T365" id="Seg_9005" s="T364">adj</ta>
            <ta e="T366" id="Seg_9006" s="T365">n</ta>
            <ta e="T367" id="Seg_9007" s="T366">v</ta>
            <ta e="T368" id="Seg_9008" s="T367">pers</ta>
            <ta e="T369" id="Seg_9009" s="T368">v</ta>
            <ta e="T370" id="Seg_9010" s="T369">v</ta>
            <ta e="T371" id="Seg_9011" s="T370">adj</ta>
            <ta e="T372" id="Seg_9012" s="T371">n</ta>
            <ta e="T373" id="Seg_9013" s="T372">adv</ta>
            <ta e="T374" id="Seg_9014" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_9015" s="T374">adj</ta>
            <ta e="T376" id="Seg_9016" s="T375">v</ta>
            <ta e="T377" id="Seg_9017" s="T376">v</ta>
            <ta e="T378" id="Seg_9018" s="T377">adj</ta>
            <ta e="T379" id="Seg_9019" s="T378">n</ta>
            <ta e="T380" id="Seg_9020" s="T379">v</ta>
            <ta e="T381" id="Seg_9021" s="T380">num</ta>
            <ta e="T382" id="Seg_9022" s="T381">ptcl</ta>
            <ta e="T383" id="Seg_9023" s="T382">num</ta>
            <ta e="T384" id="Seg_9024" s="T383">n</ta>
            <ta e="T385" id="Seg_9025" s="T384">v</ta>
            <ta e="T386" id="Seg_9026" s="T385">v</ta>
            <ta e="T387" id="Seg_9027" s="T386">n</ta>
            <ta e="T388" id="Seg_9028" s="T387">n</ta>
            <ta e="T389" id="Seg_9029" s="T388">n</ta>
            <ta e="T390" id="Seg_9030" s="T389">n</ta>
            <ta e="T391" id="Seg_9031" s="T390">v</ta>
            <ta e="T392" id="Seg_9032" s="T391">adv</ta>
            <ta e="T393" id="Seg_9033" s="T392">n</ta>
            <ta e="T394" id="Seg_9034" s="T393">v</ta>
            <ta e="T395" id="Seg_9035" s="T394">n</ta>
            <ta e="T396" id="Seg_9036" s="T395">adj</ta>
            <ta e="T397" id="Seg_9037" s="T396">n</ta>
            <ta e="T398" id="Seg_9038" s="T397">interrog</ta>
            <ta e="T399" id="Seg_9039" s="T398">adv</ta>
            <ta e="T400" id="Seg_9040" s="T399">v</ta>
            <ta e="T401" id="Seg_9041" s="T400">nprop</ta>
            <ta e="T402" id="Seg_9042" s="T401">v</ta>
            <ta e="T403" id="Seg_9043" s="T402">interj</ta>
            <ta e="T404" id="Seg_9044" s="T403">n</ta>
            <ta e="T405" id="Seg_9045" s="T404">interrog</ta>
            <ta e="T406" id="Seg_9046" s="T405">v</ta>
            <ta e="T407" id="Seg_9047" s="T406">nprop</ta>
            <ta e="T408" id="Seg_9048" s="T407">v</ta>
            <ta e="T409" id="Seg_9049" s="T408">adv</ta>
            <ta e="T410" id="Seg_9050" s="T409">pers</ta>
            <ta e="T411" id="Seg_9051" s="T410">v</ta>
            <ta e="T412" id="Seg_9052" s="T411">preverb</ta>
            <ta e="T413" id="Seg_9053" s="T412">v</ta>
            <ta e="T414" id="Seg_9054" s="T413">pers</ta>
            <ta e="T415" id="Seg_9055" s="T414">preverb</ta>
            <ta e="T416" id="Seg_9056" s="T415">v</ta>
            <ta e="T417" id="Seg_9057" s="T416">pers</ta>
            <ta e="T418" id="Seg_9058" s="T417">preverb</ta>
            <ta e="T419" id="Seg_9059" s="T418">v</ta>
            <ta e="T420" id="Seg_9060" s="T419">v</ta>
            <ta e="T421" id="Seg_9061" s="T420">pers</ta>
            <ta e="T422" id="Seg_9062" s="T421">n</ta>
            <ta e="T423" id="Seg_9063" s="T422">n</ta>
            <ta e="T424" id="Seg_9064" s="T423">adj</ta>
            <ta e="T425" id="Seg_9065" s="T424">n</ta>
            <ta e="T426" id="Seg_9066" s="T425">v</ta>
            <ta e="T427" id="Seg_9067" s="T426">n</ta>
            <ta e="T428" id="Seg_9068" s="T427">n</ta>
            <ta e="T429" id="Seg_9069" s="T428">v</ta>
            <ta e="T430" id="Seg_9070" s="T429">interrog</ta>
            <ta e="T431" id="Seg_9071" s="T430">v</ta>
            <ta e="T432" id="Seg_9072" s="T431">pers</ta>
            <ta e="T433" id="Seg_9073" s="T432">v</ta>
            <ta e="T434" id="Seg_9074" s="T433">n</ta>
            <ta e="T435" id="Seg_9075" s="T434">v</ta>
            <ta e="T436" id="Seg_9076" s="T435">n</ta>
            <ta e="T437" id="Seg_9077" s="T436">n</ta>
            <ta e="T438" id="Seg_9078" s="T437">v</ta>
            <ta e="T439" id="Seg_9079" s="T438">n</ta>
            <ta e="T440" id="Seg_9080" s="T439">v</ta>
            <ta e="T441" id="Seg_9081" s="T440">n</ta>
            <ta e="T442" id="Seg_9082" s="T441">n</ta>
            <ta e="T443" id="Seg_9083" s="T442">quant</ta>
            <ta e="T444" id="Seg_9084" s="T443">n</ta>
            <ta e="T445" id="Seg_9085" s="T444">adv</ta>
            <ta e="T446" id="Seg_9086" s="T445">v</ta>
            <ta e="T447" id="Seg_9087" s="T446">n</ta>
            <ta e="T448" id="Seg_9088" s="T447">n</ta>
            <ta e="T449" id="Seg_9089" s="T448">n</ta>
            <ta e="T450" id="Seg_9090" s="T449">n</ta>
            <ta e="T451" id="Seg_9091" s="T450">v</ta>
            <ta e="T452" id="Seg_9092" s="T451">pers</ta>
            <ta e="T453" id="Seg_9093" s="T452">ptcl</ta>
            <ta e="T454" id="Seg_9094" s="T453">v</ta>
            <ta e="T455" id="Seg_9095" s="T454">n</ta>
            <ta e="T456" id="Seg_9096" s="T455">pers</ta>
            <ta e="T457" id="Seg_9097" s="T456">v</ta>
            <ta e="T458" id="Seg_9098" s="T457">n</ta>
            <ta e="T459" id="Seg_9099" s="T458">n</ta>
            <ta e="T460" id="Seg_9100" s="T459">n</ta>
            <ta e="T461" id="Seg_9101" s="T460">v</ta>
            <ta e="T753" id="Seg_9102" s="T461">n</ta>
            <ta e="T463" id="Seg_9103" s="T462">n</ta>
            <ta e="T464" id="Seg_9104" s="T463">nprop</ta>
            <ta e="T465" id="Seg_9105" s="T464">v</ta>
            <ta e="T466" id="Seg_9106" s="T465">adj</ta>
            <ta e="T467" id="Seg_9107" s="T466">n</ta>
            <ta e="T468" id="Seg_9108" s="T467">conj</ta>
            <ta e="T469" id="Seg_9109" s="T468">n</ta>
            <ta e="T470" id="Seg_9110" s="T469">n</ta>
            <ta e="T471" id="Seg_9111" s="T470">v</ta>
            <ta e="T472" id="Seg_9112" s="T471">adj</ta>
            <ta e="T473" id="Seg_9113" s="T472">n</ta>
            <ta e="T474" id="Seg_9114" s="T473">v</ta>
            <ta e="T475" id="Seg_9115" s="T474">v</ta>
            <ta e="T476" id="Seg_9116" s="T475">emphpers</ta>
            <ta e="T477" id="Seg_9117" s="T476">adv</ta>
            <ta e="T478" id="Seg_9118" s="T477">adv</ta>
            <ta e="T479" id="Seg_9119" s="T478">nprop</ta>
            <ta e="T480" id="Seg_9120" s="T479">v</ta>
            <ta e="T481" id="Seg_9121" s="T480">n</ta>
            <ta e="T482" id="Seg_9122" s="T481">v</ta>
            <ta e="T483" id="Seg_9123" s="T482">n</ta>
            <ta e="T484" id="Seg_9124" s="T483">v</ta>
            <ta e="T485" id="Seg_9125" s="T484">v</ta>
            <ta e="T486" id="Seg_9126" s="T485">n</ta>
            <ta e="T487" id="Seg_9127" s="T486">adj</ta>
            <ta e="T488" id="Seg_9128" s="T487">n</ta>
            <ta e="T489" id="Seg_9129" s="T488">ptcl</ta>
            <ta e="T490" id="Seg_9130" s="T489">pers</ta>
            <ta e="T491" id="Seg_9131" s="T490">nprop</ta>
            <ta e="T492" id="Seg_9132" s="T491">nprop</ta>
            <ta e="T493" id="Seg_9133" s="T492">v</ta>
            <ta e="T494" id="Seg_9134" s="T493">dem</ta>
            <ta e="T495" id="Seg_9135" s="T494">pers</ta>
            <ta e="T496" id="Seg_9136" s="T495">v</ta>
            <ta e="T497" id="Seg_9137" s="T496">pers</ta>
            <ta e="T498" id="Seg_9138" s="T497">n</ta>
            <ta e="T499" id="Seg_9139" s="T498">adv</ta>
            <ta e="T500" id="Seg_9140" s="T499">pers</ta>
            <ta e="T501" id="Seg_9141" s="T500">v</ta>
            <ta e="T502" id="Seg_9142" s="T501">adj</ta>
            <ta e="T503" id="Seg_9143" s="T502">n</ta>
            <ta e="T504" id="Seg_9144" s="T503">n</ta>
            <ta e="T505" id="Seg_9145" s="T504">v</ta>
            <ta e="T506" id="Seg_9146" s="T505">pers</ta>
            <ta e="T507" id="Seg_9147" s="T506">nprop</ta>
            <ta e="T508" id="Seg_9148" s="T507">adv</ta>
            <ta e="T509" id="Seg_9149" s="T508">v</ta>
            <ta e="T510" id="Seg_9150" s="T509">dem</ta>
            <ta e="T511" id="Seg_9151" s="T510">adv</ta>
            <ta e="T512" id="Seg_9152" s="T511">v</ta>
            <ta e="T513" id="Seg_9153" s="T512">n</ta>
            <ta e="T514" id="Seg_9154" s="T513">pers</ta>
            <ta e="T515" id="Seg_9155" s="T514">n</ta>
            <ta e="T516" id="Seg_9156" s="T515">v</ta>
            <ta e="T517" id="Seg_9157" s="T516">n</ta>
            <ta e="T518" id="Seg_9158" s="T517">n</ta>
            <ta e="T519" id="Seg_9159" s="T518">n</ta>
            <ta e="T520" id="Seg_9160" s="T519">v</ta>
            <ta e="T521" id="Seg_9161" s="T520">v</ta>
            <ta e="T522" id="Seg_9162" s="T521">adj</ta>
            <ta e="T523" id="Seg_9163" s="T522">n</ta>
            <ta e="T524" id="Seg_9164" s="T523">adv</ta>
            <ta e="T525" id="Seg_9165" s="T524">v</ta>
            <ta e="T526" id="Seg_9166" s="T525">pers</ta>
            <ta e="T527" id="Seg_9167" s="T526">v</ta>
            <ta e="T528" id="Seg_9168" s="T527">n</ta>
            <ta e="T529" id="Seg_9169" s="T528">nprop</ta>
            <ta e="T530" id="Seg_9170" s="T529">v</ta>
            <ta e="T531" id="Seg_9171" s="T530">adv</ta>
            <ta e="T532" id="Seg_9172" s="T531">n</ta>
            <ta e="T533" id="Seg_9173" s="T532">adj</ta>
            <ta e="T534" id="Seg_9174" s="T533">n</ta>
            <ta e="T535" id="Seg_9175" s="T534">n</ta>
            <ta e="T536" id="Seg_9176" s="T535">n</ta>
            <ta e="T537" id="Seg_9177" s="T536">v</ta>
            <ta e="T538" id="Seg_9178" s="T537">n</ta>
            <ta e="T539" id="Seg_9179" s="T538">adv</ta>
            <ta e="T540" id="Seg_9180" s="T539">v</ta>
            <ta e="T541" id="Seg_9181" s="T540">v</ta>
            <ta e="T542" id="Seg_9182" s="T541">dem</ta>
            <ta e="T543" id="Seg_9183" s="T542">n</ta>
            <ta e="T544" id="Seg_9184" s="T543">n</ta>
            <ta e="T545" id="Seg_9185" s="T544">nprop</ta>
            <ta e="T546" id="Seg_9186" s="T545">n</ta>
            <ta e="T547" id="Seg_9187" s="T546">v</ta>
            <ta e="T548" id="Seg_9188" s="T547">nprop</ta>
            <ta e="T549" id="Seg_9189" s="T548">adj</ta>
            <ta e="T550" id="Seg_9190" s="T549">n</ta>
            <ta e="T551" id="Seg_9191" s="T550">n</ta>
            <ta e="T552" id="Seg_9192" s="T551">v</ta>
            <ta e="T553" id="Seg_9193" s="T552">n</ta>
            <ta e="T554" id="Seg_9194" s="T553">v</ta>
            <ta e="T555" id="Seg_9195" s="T554">n</ta>
            <ta e="T556" id="Seg_9196" s="T555">pp</ta>
            <ta e="T557" id="Seg_9197" s="T556">adv</ta>
            <ta e="T558" id="Seg_9198" s="T557">adv</ta>
            <ta e="T559" id="Seg_9199" s="T558">v</ta>
            <ta e="T560" id="Seg_9200" s="T559">n</ta>
            <ta e="T561" id="Seg_9201" s="T560">n</ta>
            <ta e="T562" id="Seg_9202" s="T561">v</ta>
            <ta e="T563" id="Seg_9203" s="T562">n</ta>
            <ta e="T564" id="Seg_9204" s="T563">pers</ta>
            <ta e="T565" id="Seg_9205" s="T564">v</ta>
            <ta e="T566" id="Seg_9206" s="T565">n</ta>
            <ta e="T567" id="Seg_9207" s="T566">v</ta>
            <ta e="T568" id="Seg_9208" s="T567">n</ta>
            <ta e="T569" id="Seg_9209" s="T568">n</ta>
            <ta e="T570" id="Seg_9210" s="T569">pp</ta>
            <ta e="T571" id="Seg_9211" s="T570">v</ta>
            <ta e="T572" id="Seg_9212" s="T571">n</ta>
            <ta e="T573" id="Seg_9213" s="T572">adv</ta>
            <ta e="T574" id="Seg_9214" s="T573">nprop</ta>
            <ta e="T575" id="Seg_9215" s="T574">v</ta>
            <ta e="T576" id="Seg_9216" s="T575">quant</ta>
            <ta e="T577" id="Seg_9217" s="T576">n</ta>
            <ta e="T578" id="Seg_9218" s="T577">v</ta>
            <ta e="T579" id="Seg_9219" s="T578">n</ta>
            <ta e="T580" id="Seg_9220" s="T579">n</ta>
            <ta e="T581" id="Seg_9221" s="T580">ptcp</ta>
            <ta e="T582" id="Seg_9222" s="T581">pers</ta>
            <ta e="T583" id="Seg_9223" s="T582">v</ta>
            <ta e="T584" id="Seg_9224" s="T583">v</ta>
            <ta e="T585" id="Seg_9225" s="T584">interj</ta>
            <ta e="T586" id="Seg_9226" s="T585">n</ta>
            <ta e="T587" id="Seg_9227" s="T586">conj</ta>
            <ta e="T588" id="Seg_9228" s="T587">nprop</ta>
            <ta e="T589" id="Seg_9229" s="T588">adv</ta>
            <ta e="T590" id="Seg_9230" s="T589">v</ta>
            <ta e="T591" id="Seg_9231" s="T590">pers</ta>
            <ta e="T592" id="Seg_9232" s="T591">pers</ta>
            <ta e="T593" id="Seg_9233" s="T592">n</ta>
            <ta e="T594" id="Seg_9234" s="T593">v</ta>
            <ta e="T595" id="Seg_9235" s="T594">pers</ta>
            <ta e="T596" id="Seg_9236" s="T595">n</ta>
            <ta e="T597" id="Seg_9237" s="T596">pp</ta>
            <ta e="T598" id="Seg_9238" s="T597">adv</ta>
            <ta e="T599" id="Seg_9239" s="T598">n</ta>
            <ta e="T600" id="Seg_9240" s="T599">n</ta>
            <ta e="T601" id="Seg_9241" s="T600">v</ta>
            <ta e="T602" id="Seg_9242" s="T601">quant</ta>
            <ta e="T603" id="Seg_9243" s="T602">v</ta>
            <ta e="T604" id="Seg_9244" s="T603">interrog</ta>
            <ta e="T605" id="Seg_9245" s="T604">n</ta>
            <ta e="T606" id="Seg_9246" s="T605">n</ta>
            <ta e="T607" id="Seg_9247" s="T606">v</ta>
            <ta e="T608" id="Seg_9248" s="T607">pers</ta>
            <ta e="T609" id="Seg_9249" s="T608">n</ta>
            <ta e="T610" id="Seg_9250" s="T609">ptcl</ta>
            <ta e="T611" id="Seg_9251" s="T610">v</ta>
            <ta e="T612" id="Seg_9252" s="T611">ptcl</ta>
            <ta e="T613" id="Seg_9253" s="T612">adj</ta>
            <ta e="T614" id="Seg_9254" s="T613">n</ta>
            <ta e="T615" id="Seg_9255" s="T614">v</ta>
            <ta e="T616" id="Seg_9256" s="T615">pers</ta>
            <ta e="T617" id="Seg_9257" s="T616">v</ta>
            <ta e="T618" id="Seg_9258" s="T617">adv</ta>
            <ta e="T619" id="Seg_9259" s="T618">adj</ta>
            <ta e="T620" id="Seg_9260" s="T619">n</ta>
            <ta e="T621" id="Seg_9261" s="T620">n</ta>
            <ta e="T622" id="Seg_9262" s="T621">nprop</ta>
            <ta e="T623" id="Seg_9263" s="T622">n</ta>
            <ta e="T624" id="Seg_9264" s="T623">v</ta>
            <ta e="T625" id="Seg_9265" s="T624">nprop</ta>
            <ta e="T626" id="Seg_9266" s="T625">n</ta>
            <ta e="T627" id="Seg_9267" s="T626">v</ta>
            <ta e="T629" id="Seg_9268" s="T628">n</ta>
            <ta e="T630" id="Seg_9269" s="T629">adv</ta>
            <ta e="T631" id="Seg_9270" s="T630">pers</ta>
            <ta e="T632" id="Seg_9271" s="T631">adj</ta>
            <ta e="T633" id="Seg_9272" s="T632">v</ta>
            <ta e="T634" id="Seg_9273" s="T633">n</ta>
            <ta e="T635" id="Seg_9274" s="T634">n</ta>
            <ta e="T636" id="Seg_9275" s="T635">adj</ta>
            <ta e="T637" id="Seg_9276" s="T636">n</ta>
            <ta e="T638" id="Seg_9277" s="T637">quant</ta>
            <ta e="T639" id="Seg_9278" s="T638">v</ta>
            <ta e="T640" id="Seg_9279" s="T639">conj</ta>
            <ta e="T641" id="Seg_9280" s="T640">n</ta>
            <ta e="T642" id="Seg_9281" s="T641">adj</ta>
            <ta e="T643" id="Seg_9282" s="T642">n</ta>
            <ta e="T644" id="Seg_9283" s="T643">nprop</ta>
            <ta e="T645" id="Seg_9284" s="T644">n</ta>
            <ta e="T646" id="Seg_9285" s="T645">n</ta>
            <ta e="T647" id="Seg_9286" s="T646">v</ta>
            <ta e="T648" id="Seg_9287" s="T647">interrog</ta>
            <ta e="T649" id="Seg_9288" s="T648">adv</ta>
            <ta e="T650" id="Seg_9289" s="T649">v</ta>
            <ta e="T651" id="Seg_9290" s="T650">nprop</ta>
            <ta e="T652" id="Seg_9291" s="T651">n</ta>
            <ta e="T655" id="Seg_9292" s="T654">adj</ta>
            <ta e="T656" id="Seg_9293" s="T655">n</ta>
            <ta e="T657" id="Seg_9294" s="T656">emphpers</ta>
            <ta e="T658" id="Seg_9295" s="T657">num</ta>
            <ta e="T659" id="Seg_9296" s="T658">n</ta>
            <ta e="T660" id="Seg_9297" s="T659">v</ta>
            <ta e="T661" id="Seg_9298" s="T660">nprop</ta>
            <ta e="T662" id="Seg_9299" s="T661">v</ta>
            <ta e="T663" id="Seg_9300" s="T662">n</ta>
            <ta e="T664" id="Seg_9301" s="T663">n</ta>
            <ta e="T665" id="Seg_9302" s="T664">v</ta>
            <ta e="T666" id="Seg_9303" s="T665">nprop</ta>
            <ta e="T667" id="Seg_9304" s="T666">conj</ta>
            <ta e="T668" id="Seg_9305" s="T667">v</ta>
            <ta e="T669" id="Seg_9306" s="T668">dem</ta>
            <ta e="T670" id="Seg_9307" s="T669">dem</ta>
            <ta e="T671" id="Seg_9308" s="T670">n</ta>
            <ta e="T672" id="Seg_9309" s="T671">pers</ta>
            <ta e="T673" id="Seg_9310" s="T672">ptcl</ta>
            <ta e="T674" id="Seg_9311" s="T673">n</ta>
            <ta e="T675" id="Seg_9312" s="T674">v</ta>
            <ta e="T676" id="Seg_9313" s="T675">n</ta>
            <ta e="T677" id="Seg_9314" s="T676">nprop</ta>
            <ta e="T678" id="Seg_9315" s="T677">v</ta>
            <ta e="T679" id="Seg_9316" s="T678">v</ta>
            <ta e="T680" id="Seg_9317" s="T679">v</ta>
            <ta e="T681" id="Seg_9318" s="T680">v</ta>
            <ta e="T682" id="Seg_9319" s="T681">n</ta>
            <ta e="T683" id="Seg_9320" s="T682">n</ta>
            <ta e="T684" id="Seg_9321" s="T683">pp</ta>
            <ta e="T685" id="Seg_9322" s="T684">n</ta>
            <ta e="T686" id="Seg_9323" s="T685">v</ta>
            <ta e="T687" id="Seg_9324" s="T686">n</ta>
            <ta e="T688" id="Seg_9325" s="T687">n</ta>
            <ta e="T689" id="Seg_9326" s="T688">adj</ta>
            <ta e="T690" id="Seg_9327" s="T689">v</ta>
            <ta e="T691" id="Seg_9328" s="T690">n</ta>
            <ta e="T692" id="Seg_9329" s="T691">n</ta>
            <ta e="T693" id="Seg_9330" s="T692">pers</ta>
            <ta e="T694" id="Seg_9331" s="T693">pers</ta>
            <ta e="T695" id="Seg_9332" s="T694">v</ta>
            <ta e="T696" id="Seg_9333" s="T695">n</ta>
            <ta e="T697" id="Seg_9334" s="T696">v</ta>
            <ta e="T698" id="Seg_9335" s="T697">n</ta>
            <ta e="T699" id="Seg_9336" s="T698">n</ta>
            <ta e="T700" id="Seg_9337" s="T699">adj</ta>
            <ta e="T701" id="Seg_9338" s="T700">n</ta>
            <ta e="T702" id="Seg_9339" s="T701">pers</ta>
            <ta e="T703" id="Seg_9340" s="T702">v</ta>
            <ta e="T704" id="Seg_9341" s="T703">quant</ta>
            <ta e="T705" id="Seg_9342" s="T704">n</ta>
            <ta e="T706" id="Seg_9343" s="T705">adv</ta>
            <ta e="T707" id="Seg_9344" s="T706">nprop</ta>
            <ta e="T708" id="Seg_9345" s="T707">n</ta>
            <ta e="T709" id="Seg_9346" s="T708">n</ta>
            <ta e="T710" id="Seg_9347" s="T709">interrog</ta>
            <ta e="T711" id="Seg_9348" s="T710">v</ta>
            <ta e="T712" id="Seg_9349" s="T711">n</ta>
            <ta e="T713" id="Seg_9350" s="T712">pers</ta>
            <ta e="T714" id="Seg_9351" s="T713">num</ta>
            <ta e="T715" id="Seg_9352" s="T714">adv</ta>
            <ta e="T716" id="Seg_9353" s="T715">nprop</ta>
            <ta e="T717" id="Seg_9354" s="T716">n</ta>
            <ta e="T718" id="Seg_9355" s="T717">adv</ta>
            <ta e="T719" id="Seg_9356" s="T718">adj</ta>
            <ta e="T720" id="Seg_9357" s="T719">n</ta>
            <ta e="T721" id="Seg_9358" s="T720">n</ta>
            <ta e="T722" id="Seg_9359" s="T721">adv</ta>
            <ta e="T723" id="Seg_9360" s="T722">nprop</ta>
            <ta e="T724" id="Seg_9361" s="T723">emphpers</ta>
            <ta e="T725" id="Seg_9362" s="T724">v</ta>
            <ta e="T726" id="Seg_9363" s="T725">ptcp</ta>
            <ta e="T727" id="Seg_9364" s="T726">n</ta>
            <ta e="T728" id="Seg_9365" s="T727">n</ta>
            <ta e="T729" id="Seg_9366" s="T728">n</ta>
            <ta e="T730" id="Seg_9367" s="T729">v</ta>
            <ta e="T731" id="Seg_9368" s="T730">nprop</ta>
            <ta e="T732" id="Seg_9369" s="T731">n</ta>
            <ta e="T733" id="Seg_9370" s="T732">v</ta>
            <ta e="T734" id="Seg_9371" s="T733">n</ta>
            <ta e="T735" id="Seg_9372" s="T734">n</ta>
            <ta e="T736" id="Seg_9373" s="T735">pp</ta>
            <ta e="T737" id="Seg_9374" s="T736">ptcl</ta>
            <ta e="T738" id="Seg_9375" s="T737">interrog</ta>
            <ta e="T739" id="Seg_9376" s="T738">ptcl</ta>
            <ta e="T740" id="Seg_9377" s="T739">v</ta>
            <ta e="T741" id="Seg_9378" s="T740">n</ta>
            <ta e="T742" id="Seg_9379" s="T741">conj</ta>
            <ta e="T743" id="Seg_9380" s="T742">n</ta>
            <ta e="T744" id="Seg_9381" s="T743">adv</ta>
            <ta e="T745" id="Seg_9382" s="T744">v</ta>
            <ta e="T746" id="Seg_9383" s="T745">pers</ta>
            <ta e="T747" id="Seg_9384" s="T746">clit</ta>
            <ta e="T748" id="Seg_9385" s="T747">pers</ta>
            <ta e="T749" id="Seg_9386" s="T748">v</ta>
            <ta e="T750" id="Seg_9387" s="T749">n</ta>
            <ta e="T751" id="Seg_9388" s="T750">pers</ta>
            <ta e="T752" id="Seg_9389" s="T751">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T23" id="Seg_9390" s="T22">RUS:gram</ta>
            <ta e="T28" id="Seg_9391" s="T27">RUS:cult</ta>
            <ta e="T37" id="Seg_9392" s="T36">RUS:cult</ta>
            <ta e="T38" id="Seg_9393" s="T37">RUS:cult</ta>
            <ta e="T42" id="Seg_9394" s="T41">RUS:cult</ta>
            <ta e="T44" id="Seg_9395" s="T43">RUS:cult</ta>
            <ta e="T65" id="Seg_9396" s="T64">RUS:cult</ta>
            <ta e="T66" id="Seg_9397" s="T65">RUS:cult</ta>
            <ta e="T75" id="Seg_9398" s="T74">RUS:cult</ta>
            <ta e="T76" id="Seg_9399" s="T75">RUS:cult</ta>
            <ta e="T87" id="Seg_9400" s="T86">RUS:cult</ta>
            <ta e="T88" id="Seg_9401" s="T87">RUS:cult</ta>
            <ta e="T96" id="Seg_9402" s="T95">RUS:cult</ta>
            <ta e="T103" id="Seg_9403" s="T102">RUS:cult</ta>
            <ta e="T104" id="Seg_9404" s="T103">RUS:cult</ta>
            <ta e="T120" id="Seg_9405" s="T119">RUS:cult</ta>
            <ta e="T138" id="Seg_9406" s="T137">RUS:cult</ta>
            <ta e="T143" id="Seg_9407" s="T142">RUS:cult</ta>
            <ta e="T150" id="Seg_9408" s="T149">RUS:cult</ta>
            <ta e="T161" id="Seg_9409" s="T160">RUS:cult</ta>
            <ta e="T162" id="Seg_9410" s="T161">RUS:gram</ta>
            <ta e="T164" id="Seg_9411" s="T163">RUS:cult</ta>
            <ta e="T175" id="Seg_9412" s="T174">RUS:cult</ta>
            <ta e="T192" id="Seg_9413" s="T191">RUS:gram</ta>
            <ta e="T193" id="Seg_9414" s="T192">RUS:cult</ta>
            <ta e="T194" id="Seg_9415" s="T193">RUS:cult</ta>
            <ta e="T196" id="Seg_9416" s="T195">RUS:gram</ta>
            <ta e="T203" id="Seg_9417" s="T202">RUS:cult</ta>
            <ta e="T217" id="Seg_9418" s="T216">RUS:gram</ta>
            <ta e="T218" id="Seg_9419" s="T217">RUS:cult</ta>
            <ta e="T242" id="Seg_9420" s="T241">RUS:cult</ta>
            <ta e="T257" id="Seg_9421" s="T256">RUS:cult</ta>
            <ta e="T266" id="Seg_9422" s="T265">RUS:disc</ta>
            <ta e="T280" id="Seg_9423" s="T279">RUS:cult</ta>
            <ta e="T285" id="Seg_9424" s="T284">RUS:disc</ta>
            <ta e="T288" id="Seg_9425" s="T287">RUS:cult</ta>
            <ta e="T311" id="Seg_9426" s="T310">RUS:cult</ta>
            <ta e="T315" id="Seg_9427" s="T314">RUS:cult</ta>
            <ta e="T324" id="Seg_9428" s="T323">RUS:gram</ta>
            <ta e="T325" id="Seg_9429" s="T324">RUS:cult</ta>
            <ta e="T333" id="Seg_9430" s="T332">RUS:cult</ta>
            <ta e="T346" id="Seg_9431" s="T345">RUS:cult</ta>
            <ta e="T353" id="Seg_9432" s="T352">RUS:cult</ta>
            <ta e="T374" id="Seg_9433" s="T373">RUS:gram</ta>
            <ta e="T382" id="Seg_9434" s="T381">RUS:gram</ta>
            <ta e="T389" id="Seg_9435" s="T388">RUS:cult</ta>
            <ta e="T468" id="Seg_9436" s="T467">RUS:gram</ta>
            <ta e="T470" id="Seg_9437" s="T469">RUS:cult</ta>
            <ta e="T504" id="Seg_9438" s="T503">RUS:cult</ta>
            <ta e="T543" id="Seg_9439" s="T542">RUS:cult</ta>
            <ta e="T544" id="Seg_9440" s="T543">RUS:cult</ta>
            <ta e="T560" id="Seg_9441" s="T559">RUS:cult</ta>
            <ta e="T572" id="Seg_9442" s="T571">RUS:cult</ta>
            <ta e="T585" id="Seg_9443" s="T584">RUS:disc</ta>
            <ta e="T587" id="Seg_9444" s="T586">RUS:gram</ta>
            <ta e="T596" id="Seg_9445" s="T595">RUS:cult</ta>
            <ta e="T612" id="Seg_9446" s="T611">RUS:gram</ta>
            <ta e="T620" id="Seg_9447" s="T619">RUS:cult</ta>
            <ta e="T640" id="Seg_9448" s="T639">RUS:gram</ta>
            <ta e="T652" id="Seg_9449" s="T651">RUS:cult</ta>
            <ta e="T659" id="Seg_9450" s="T658">RUS:cult</ta>
            <ta e="T663" id="Seg_9451" s="T662">RUS:cult</ta>
            <ta e="T691" id="Seg_9452" s="T690">RUS:cult</ta>
            <ta e="T709" id="Seg_9453" s="T708">RUS:cult</ta>
            <ta e="T721" id="Seg_9454" s="T720">RUS:cult</ta>
            <ta e="T735" id="Seg_9455" s="T734">RUS:cult</ta>
            <ta e="T742" id="Seg_9456" s="T741">RUS:gram</ta>
            <ta e="T743" id="Seg_9457" s="T742">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T2" id="Seg_9458" s="T0">Старик Кольсако.</ta>
            <ta e="T5" id="Seg_9459" s="T2">Жил Ичакычика с бабушкой.</ta>
            <ta e="T11" id="Seg_9460" s="T5">Город старика Кольсако недалеко был.</ta>
            <ta e="T20" id="Seg_9461" s="T11">Однажды корова старика Кольсако потерялась, постоянно теряется.</ta>
            <ta e="T25" id="Seg_9462" s="T20">Он знает, что Ичакычика украл.</ta>
            <ta e="T33" id="Seg_9463" s="T25">Послал к нему двух казаков, посмотреть, что делает Ичакычика.</ta>
            <ta e="T42" id="Seg_9464" s="T33">Он [Ичакычика] знает: придут с ружьем, схватят его, посадят в тюрьму.</ta>
            <ta e="T51" id="Seg_9465" s="T42">В это время он корову забил, кишки кровью наполнил.</ta>
            <ta e="T56" id="Seg_9466" s="T51">Он держал большой нож на потолке.</ta>
            <ta e="T61" id="Seg_9467" s="T56">Бабушку свою обернул кровавыми кишками коровы.</ta>
            <ta e="T64" id="Seg_9468" s="T61">Бабушка надела одежду.</ta>
            <ta e="T68" id="Seg_9469" s="T64">Казаки пришли к Ичакычике.</ta>
            <ta e="T72" id="Seg_9470" s="T68">Начальник сказал Ичакычику:</ta>
            <ta e="T76" id="Seg_9471" s="T72">“Тебя надо посадить в тюрьму.</ta>
            <ta e="T80" id="Seg_9472" s="T76">Ты коров убиваешь и ешь”.</ta>
            <ta e="T84" id="Seg_9473" s="T80">Он рассердился и сказал бабушке:</ta>
            <ta e="T87" id="Seg_9474" s="T84">“Меня посадят в тюрьму.</ta>
            <ta e="T90" id="Seg_9475" s="T87">Чайник кипяти скорей”.</ta>
            <ta e="T95" id="Seg_9476" s="T90">Бабушка заругалась: “У меня ноги, руки болят”.</ta>
            <ta e="T99" id="Seg_9477" s="T95">Казаки стоят в дверном проеме.</ta>
            <ta e="T107" id="Seg_9478" s="T99">Ичакычика бабушке [говорит]: “Ты лентяйка, чайник не хочешь кипятить”.</ta>
            <ta e="T116" id="Seg_9479" s="T107">Рассердился Ичакычика, ножик сверху взял, бабушку разрезал, бабушка умерла.</ta>
            <ta e="T124" id="Seg_9480" s="T116">Потом Ичакычика сказал казакам: “Моя бабушка всегда так себя ведет”.</ta>
            <ta e="T127" id="Seg_9481" s="T124">Ножик в ее ногу воткнул.</ta>
            <ta e="T132" id="Seg_9482" s="T127">“Бабушка, вставай, мой нож оживит [тебя]”.</ta>
            <ta e="T139" id="Seg_9483" s="T132">Потом бабушка вскочила, скорее схватила чайник кипятить.</ta>
            <ta e="T144" id="Seg_9484" s="T139">За время [чтобы] трубку выкурить чайник вскипел.</ta>
            <ta e="T149" id="Seg_9485" s="T144">Хлеб и еду поставила на стол быстро.</ta>
            <ta e="T151" id="Seg_9486" s="T149">Казаки начали есть.</ta>
            <ta e="T158" id="Seg_9487" s="T151">Они так сказали: “Мы твой нож купим.</ta>
            <ta e="T163" id="Seg_9488" s="T158">Наши жены лентяйничают и спят”.</ta>
            <ta e="T172" id="Seg_9489" s="T163">Казаки не стали трогать Иачкычику, скажут (/сказали) старику Кольсако – убили.</ta>
            <ta e="T174" id="Seg_9490" s="T172">Вот сказали.</ta>
            <ta e="T178" id="Seg_9491" s="T174">Начальник послал [их] купить нож.</ta>
            <ta e="T181" id="Seg_9492" s="T178">На другой день купили.</ta>
            <ta e="T189" id="Seg_9493" s="T181">Старик Кольсако сказал: “Вы нож мне дайте, я продержу его ночь.</ta>
            <ta e="T194" id="Seg_9494" s="T189">Моя жена и работники лентяйничают.</ta>
            <ta e="T198" id="Seg_9495" s="T194">Днями и ночами спят”.</ta>
            <ta e="T204" id="Seg_9496" s="T198">Ичакычика говорил: “Нож в чистоте содержите.</ta>
            <ta e="T207" id="Seg_9497" s="T204">Чтобы хорошего человека резать”.</ta>
            <ta e="T214" id="Seg_9498" s="T207">Старик Кольсако поспал, утром рано поднялся.</ta>
            <ta e="T219" id="Seg_9499" s="T214">“Жена, поднимайся, и работники, поднимайтесь!”</ta>
            <ta e="T223" id="Seg_9500" s="T219">Они быстро не поднимались.</ta>
            <ta e="T224" id="Seg_9501" s="T223">Он рассердился.</ta>
            <ta e="T229" id="Seg_9502" s="T224">Сверху взял ножик, порезал всех.</ta>
            <ta e="T235" id="Seg_9503" s="T229">Ножик воткнул в ноги всем порезанным людям.</ta>
            <ta e="T237" id="Seg_9504" s="T235">Тоже говорил [волшебное слово].</ta>
            <ta e="T239" id="Seg_9505" s="T237">Навсегда умерли.</ta>
            <ta e="T243" id="Seg_9506" s="T239">Cтарик рассердился, казаков позвал.</ta>
            <ta e="T246" id="Seg_9507" s="T243">“Где вы брали ножик?”</ta>
            <ta e="T251" id="Seg_9508" s="T246">“Он (/Ичак) нам нож дал.</ta>
            <ta e="T254" id="Seg_9509" s="T251">Он, Ичакычика, обманул”.</ta>
            <ta e="T257" id="Seg_9510" s="T254">“Поймать и судить.</ta>
            <ta e="T264" id="Seg_9511" s="T257">Наши женщины (/моя жена и женщины), все умерли от ножа”.</ta>
            <ta e="T269" id="Seg_9512" s="T264">“Ты, однако, неправильно резал. [говорят его солдаты]</ta>
            <ta e="T276" id="Seg_9513" s="T269">[Надо] его оставить [еще] на один день, мы тоже попробуем.</ta>
            <ta e="T280" id="Seg_9514" s="T276">Наши жены тоже лентяйничают”.</ta>
            <ta e="T287" id="Seg_9515" s="T280">Тогда старик Кольсако сказал: “Ладно, попробуйте ножик”.</ta>
            <ta e="T293" id="Seg_9516" s="T287">Казаки вдвоем так же резали своих жен.</ta>
            <ta e="T297" id="Seg_9517" s="T293">Жены их умерли в крови.</ta>
            <ta e="T305" id="Seg_9518" s="T297">Утром сказали старику Кольсако: “Наши жены навсегда умерли”.</ta>
            <ta e="T307" id="Seg_9519" s="T305">Старик рассердился.</ta>
            <ta e="T310" id="Seg_9520" s="T307">“Ичакычику схватить надо”.</ta>
            <ta e="T312" id="Seg_9521" s="T310">Казаки пошли.</ta>
            <ta e="T316" id="Seg_9522" s="T312">Ичакычика в это время у себя дома сидит.</ta>
            <ta e="T318" id="Seg_9523" s="T316">Ичакычику схватили.</ta>
            <ta e="T320" id="Seg_9524" s="T318">“Достаточно ты обманывал.</ta>
            <ta e="T330" id="Seg_9525" s="T320">Мы всех наших жен и работников порезали, они умерли в крови”.</ta>
            <ta e="T333" id="Seg_9526" s="T330">Ичакычику увезли в тюрьму.</ta>
            <ta e="T339" id="Seg_9527" s="T333">Старик Кольсако приказал пойти утопить его в воде.</ta>
            <ta e="T345" id="Seg_9528" s="T339">Сказал внутрь коровьей шкуры зашить живьем.</ta>
            <ta e="T354" id="Seg_9529" s="T345">Казаки его увезли на телеге [на санке с конем] к краю воды, на берег моря.</ta>
            <ta e="T359" id="Seg_9530" s="T354">Он в мешке живой лежит.</ta>
            <ta e="T367" id="Seg_9531" s="T359">Сказал: “Меня утопите большим камнем, большой камень найдите”.</ta>
            <ta e="T372" id="Seg_9532" s="T367">Они пошли искать большой камень.</ta>
            <ta e="T380" id="Seg_9533" s="T372">Долго ли, коротко ли лежал он – слышит, конская телега идет.</ta>
            <ta e="T385" id="Seg_9534" s="T380">Четыре или пять телег едет.</ta>
            <ta e="T386" id="Seg_9535" s="T385">Остановились.</ta>
            <ta e="T392" id="Seg_9536" s="T386">Купец с товаром (/с товаром, с материалом) едет домой.</ta>
            <ta e="T397" id="Seg_9537" s="T392">Купец пнул ногой мешок из шкуры коровы.</ta>
            <ta e="T400" id="Seg_9538" s="T397">“Что тут лежит?”</ta>
            <ta e="T403" id="Seg_9539" s="T400">Ичакычика вскрикнул: “Ой!”</ta>
            <ta e="T406" id="Seg_9540" s="T403">Купец: “Почему лежишь?”</ta>
            <ta e="T413" id="Seg_9541" s="T406">Ичакычика сказал: “Выпусти меня, разрежь [мешок]”.</ta>
            <ta e="T416" id="Seg_9542" s="T413">Он разрезал.</ta>
            <ta e="T420" id="Seg_9543" s="T416">[Ичакычика] выскочил (/вышел).</ta>
            <ta e="T427" id="Seg_9544" s="T420">У него соболиные шкуры были за пазухой.</ta>
            <ta e="T431" id="Seg_9545" s="T427">Купец спрашивает: “Зачем лежишь?”</ta>
            <ta e="T438" id="Seg_9546" s="T431"> “Я лежу, в воду нырял, соболиные шкуры взял (/нашел).</ta>
            <ta e="T445" id="Seg_9547" s="T438">Силы у меня не было, товаров (/материалов) много в воде вверх(?)”.</ta>
            <ta e="T449" id="Seg_9548" s="T445">Показал соболиные шкуры купцу.</ta>
            <ta e="T455" id="Seg_9549" s="T449">Купец обрадовался: “Я тоже хочу нырять в воду”.</ta>
            <ta e="T459" id="Seg_9550" s="T455">[Ичакычика] его зашил в мешок из коровьей шкуры.</ta>
            <ta e="T463" id="Seg_9551" s="T459">Купец лежит у воды в мешке.</ta>
            <ta e="T469" id="Seg_9552" s="T463">Ичакычика уехал, на лошадиной телеге и с товаром.</ta>
            <ta e="T474" id="Seg_9553" s="T469">Казаки принесли большой камень, начали привязывать.</ta>
            <ta e="T480" id="Seg_9554" s="T474">Говорят друг другу: “Достаточно, Ичакычика, ты обманывал.</ta>
            <ta e="T482" id="Seg_9555" s="T480">В воде умрешь”.</ta>
            <ta e="T488" id="Seg_9556" s="T482">Купец слышит – привязывают камень, большой камень.</ta>
            <ta e="T491" id="Seg_9557" s="T488">“Я не Ичакычика.</ta>
            <ta e="T493" id="Seg_9558" s="T491">Ичакычика ушел.</ta>
            <ta e="T496" id="Seg_9559" s="T493">Он меня обманул.</ta>
            <ta e="T498" id="Seg_9560" s="T496">Я купец.</ta>
            <ta e="T503" id="Seg_9561" s="T498">Обманом меня зашил в мешок из шкуры”.</ta>
            <ta e="T509" id="Seg_9562" s="T503">Казаки говорят: “Ты, Ичакычика, достаточно обманывал”.</ta>
            <ta e="T515" id="Seg_9563" s="T509">Тот плача кричит в мешке: “Я купец.</ta>
            <ta e="T518" id="Seg_9564" s="T515">Я ехал на лошадях, на телеге”.</ta>
            <ta e="T523" id="Seg_9565" s="T518">В воду занеся бросили, утопили с камнем.</ta>
            <ta e="T525" id="Seg_9566" s="T523">Домой ушли.</ta>
            <ta e="T528" id="Seg_9567" s="T525">“Мы [его] утопили в воде”.</ta>
            <ta e="T534" id="Seg_9568" s="T528">Ичакычика приехал домой на лошади, на телеге.</ta>
            <ta e="T541" id="Seg_9569" s="T534">Товар свой водой намочил, на веревку вверх развесил сушиться.</ta>
            <ta e="T556" id="Seg_9570" s="T541">В это время начальник, старик Кольсако смотрит в сторону дома Ичакычики – печь топится, дым валом идет от печки.</ta>
            <ta e="T563" id="Seg_9571" s="T556">Тут же сказал двум казакам, чтобы они пошли взглянуть на старуху.</ta>
            <ta e="T565" id="Seg_9572" s="T563">Они ушли.</ta>
            <ta e="T575" id="Seg_9573" s="T565">В дом зашли – вместо старухи сидит за столом Ичакычика сердитый.</ta>
            <ta e="T581" id="Seg_9574" s="T575">Все лицо его исцарапано в крови, одежда его изорвана.</ta>
            <ta e="T586" id="Seg_9575" s="T581">Они сказали (/зашли): “Здорово, товарищ”.</ta>
            <ta e="T601" id="Seg_9576" s="T586">А Ичакычика сердитый сидит: “Вы меня в воду пустили, утопили, я в море много товара нашел.</ta>
            <ta e="T611" id="Seg_9577" s="T601">Столько есть, сколько силы у меня было, я взял (/моя сила не взяла [больше]).</ta>
            <ta e="T615" id="Seg_9578" s="T611">Как будто я у подводного бога был”.</ta>
            <ta e="T621" id="Seg_9579" s="T615">Они пошли испуганные: “Какой дьявол был”.</ta>
            <ta e="T643" id="Seg_9580" s="T621">Старику Кольсако рассказать: “Ичакычику мы в воде утопили, (?) (/раньше) он столько нашел товара (/материала), какие вещи все нашел и соболиные шкуры”.</ta>
            <ta e="T647" id="Seg_9581" s="T643">Старик Кольсако разум потерял.</ta>
            <ta e="T650" id="Seg_9582" s="T647">“Как это так получилось?</ta>
            <ta e="T654" id="Seg_9583" s="T650">Ичакычика дьявол, меня(/нас) (?)”.</ta>
            <ta e="T671" id="Seg_9584" s="T654">На другой день сами двое казаков пришли, к Ичакычике послал казаков старик спросить Ичакычику, как он достал эти материалы.</ta>
            <ta e="T676" id="Seg_9585" s="T671">Мы тоже в воду нырять будем за товаром.</ta>
            <ta e="T701" id="Seg_9586" s="T676">Ичакычика сказал: “Зашивайте (/они зашивают, вы зашиваете) из коровьей шкуры мешки (/сшивайте мешки из коровьей шкуры), ложитесь на берег моря, я вас зашью в мешок, брошу в воду с камнем, с большим камнем.</ta>
            <ta e="T705" id="Seg_9587" s="T701">Вы найдете весь товар”.</ta>
            <ta e="T712" id="Seg_9588" s="T705">Вначале старик Кольсако (/начальник): “Кто хочет из города?”</ta>
            <ta e="T721" id="Seg_9589" s="T712">Он зашил вначале старика Кольсако, потом других людей (/казаков).</ta>
            <ta e="T729" id="Seg_9590" s="T721">Потом Ичакычика стал сам бросать зашитые мешки с людьми в воду.</ta>
            <ta e="T736" id="Seg_9591" s="T729">Сказал старику Кольсако: “Ищи товар на дне моря.</ta>
            <ta e="T743" id="Seg_9592" s="T736">Нигде не найдешь богатства и товаров.</ta>
            <ta e="T745" id="Seg_9593" s="T743">Навсегда умрешь.</ta>
            <ta e="T752" id="Seg_9594" s="T745">Вы вот меня бросали, в воду меня бросали”.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2" id="Seg_9595" s="T0">Old man Qolsaqo.</ta>
            <ta e="T5" id="Seg_9596" s="T2">Ichakechika lived with his grandmother.</ta>
            <ta e="T11" id="Seg_9597" s="T5">Old man Qolsaqo’s town was not far away.</ta>
            <ta e="T20" id="Seg_9598" s="T11">At one time, old man Qolsaqo’s cow got lost, it always gets lost.</ta>
            <ta e="T25" id="Seg_9599" s="T20">He knows Ichakechika stole it.</ta>
            <ta e="T33" id="Seg_9600" s="T25">He sent two Cossacks to him to see what Ichakechika is doing.</ta>
            <ta e="T42" id="Seg_9601" s="T33">He [Ichakechika] knows: they’ll come with a rifle, catch him, put him in jail.</ta>
            <ta e="T51" id="Seg_9602" s="T42">In the meantime, he killed the cow and filled its bowels with blood.</ta>
            <ta e="T56" id="Seg_9603" s="T51">He kept a big knife on the ceiling.</ta>
            <ta e="T61" id="Seg_9604" s="T56">He covered his grandma with the bloody bowels of the cow.</ta>
            <ta e="T64" id="Seg_9605" s="T61">Grandma got dressed.</ta>
            <ta e="T68" id="Seg_9606" s="T64">The Cossacks came to Ichakechika.</ta>
            <ta e="T72" id="Seg_9607" s="T68">The chief told Ichakechika:</ta>
            <ta e="T76" id="Seg_9608" s="T72">“You need to be put in jail.</ta>
            <ta e="T80" id="Seg_9609" s="T76">You kill cows and eat them.”</ta>
            <ta e="T84" id="Seg_9610" s="T80">He got angry and told grandma:</ta>
            <ta e="T87" id="Seg_9611" s="T84">“They’ll put me in jail.</ta>
            <ta e="T90" id="Seg_9612" s="T87">Boil the kettle, quick!”</ta>
            <ta e="T95" id="Seg_9613" s="T90">Grandma started to complain: “My feet and hands ache!”</ta>
            <ta e="T99" id="Seg_9614" s="T95">The Cossacks are standing in the doorway.</ta>
            <ta e="T107" id="Seg_9615" s="T99">Ichakechika [says] to grandma: “You lazybones, you don’t want to boil the kettle”. </ta>
            <ta e="T116" id="Seg_9616" s="T107">Ichakechika got angry, took the knife from high up, cut up grandma, grandma died.</ta>
            <ta e="T124" id="Seg_9617" s="T116">Then Ichakechika said to the Cossacks: “My grandma always behaves like this.”</ta>
            <ta e="T127" id="Seg_9618" s="T124">He stuck the knife into her leg.</ta>
            <ta e="T132" id="Seg_9619" s="T127">“Grandma, get up, my knife will enliven [you].”</ta>
            <ta e="T139" id="Seg_9620" s="T132">Then grandma jumped to her feet and quickly grabbed the kettle to boil it.</ta>
            <ta e="T144" id="Seg_9621" s="T139">In [a time] to smoke one's pipe the kettle boiled.</ta>
            <ta e="T149" id="Seg_9622" s="T144">She put the bread and the food on the table quickly.</ta>
            <ta e="T151" id="Seg_9623" s="T149">The Cossacks started to eat.</ta>
            <ta e="T158" id="Seg_9624" s="T151">They said so: “We’ll buy your knife.”</ta>
            <ta e="T163" id="Seg_9625" s="T158">Our wives are lazy and sleep.</ta>
            <ta e="T172" id="Seg_9626" s="T163">The Cossacks didn’t touch Ichakechika, they’ll say (/said) to the old man Qol’saqo that he was killed.</ta>
            <ta e="T174" id="Seg_9627" s="T172">They said that.</ta>
            <ta e="T178" id="Seg_9628" s="T174">The chief sent [them] to buy the knife.</ta>
            <ta e="T181" id="Seg_9629" s="T178">They bought it the next day.</ta>
            <ta e="T189" id="Seg_9630" s="T181">The old man Qolsaqo said: “Give me the knife, I’ll keep it for a night.</ta>
            <ta e="T194" id="Seg_9631" s="T189">My wife and the workers are being lazy.</ta>
            <ta e="T198" id="Seg_9632" s="T194">They sleep day and night.”</ta>
            <ta e="T204" id="Seg_9633" s="T198">Ichakechika said: “Keep the knife clean.</ta>
            <ta e="T207" id="Seg_9634" s="T204">To cut a good person.”</ta>
            <ta e="T214" id="Seg_9635" s="T207">The old man Qolsaqo slept a little, he got up early in the morning.</ta>
            <ta e="T219" id="Seg_9636" s="T214">“Wife, get up, and workers, get up!”</ta>
            <ta e="T223" id="Seg_9637" s="T219">They didn’t get up quickly.</ta>
            <ta e="T224" id="Seg_9638" s="T223">He became angry.</ta>
            <ta e="T229" id="Seg_9639" s="T224">He took the knife from up high and stabbed all of them.</ta>
            <ta e="T235" id="Seg_9640" s="T229">He stuck the knife in the legs of all slain people.</ta>
            <ta e="T237" id="Seg_9641" s="T235">He also said [the magic word].</ta>
            <ta e="T239" id="Seg_9642" s="T237">They died forever.</ta>
            <ta e="T243" id="Seg_9643" s="T239">The old man got angry and called the Cossacks.</ta>
            <ta e="T246" id="Seg_9644" s="T243">“Where did you take the knife?”</ta>
            <ta e="T251" id="Seg_9645" s="T246">“He (/Ichak) gave us the knife.</ta>
            <ta e="T254" id="Seg_9646" s="T251">He, Ichakechika, cheated us.”</ta>
            <ta e="T257" id="Seg_9647" s="T254">“Catch him and judge him.”</ta>
            <ta e="T264" id="Seg_9648" s="T257">Our wives (/my wife and the women) all died from the knife.”</ta>
            <ta e="T269" id="Seg_9649" s="T264">“You, however, stabbed the wrong way [his soldiers say].</ta>
            <ta e="T276" id="Seg_9650" s="T269">[You have to] leave him for one [more] day, we’ll try too.</ta>
            <ta e="T280" id="Seg_9651" s="T276">Our wives are also being lazy.”</ta>
            <ta e="T287" id="Seg_9652" s="T280">Then the old man Qolsaqo said: “All right, try the knife.”</ta>
            <ta e="T293" id="Seg_9653" s="T287">Two Cossacks stabbed their wives also.</ta>
            <ta e="T297" id="Seg_9654" s="T293">Their wives died (covered) in blood.</ta>
            <ta e="T305" id="Seg_9655" s="T297">In the morning they said to the old man Qolsaqo: “Our wives died forever.”</ta>
            <ta e="T307" id="Seg_9656" s="T305">The old man became angry.</ta>
            <ta e="T310" id="Seg_9657" s="T307">“Ichakechika has to be caught.”</ta>
            <ta e="T312" id="Seg_9658" s="T310">The Cossacks set out.</ta>
            <ta e="T316" id="Seg_9659" s="T312">At this time Ichakechika is sitting at home.</ta>
            <ta e="T318" id="Seg_9660" s="T316">They grabbed Ichakechika.</ta>
            <ta e="T320" id="Seg_9661" s="T318">“You’ve cheated enough.</ta>
            <ta e="T330" id="Seg_9662" s="T320">We stabbed all of our wives and workers, they died in blood.”</ta>
            <ta e="T333" id="Seg_9663" s="T330">They took Ichakechika to the jail.</ta>
            <ta e="T339" id="Seg_9664" s="T333">The old man Qolsaqo ordered that they go and drown him.</ta>
            <ta e="T345" id="Seg_9665" s="T339">He told them to sew him inside a cow hide alive.</ta>
            <ta e="T354" id="Seg_9666" s="T345">The Cossacks took him on a cart [on a sled with a horse] to the edge of the water, to the sea. </ta>
            <ta e="T359" id="Seg_9667" s="T354">He is lying in the sack alive.</ta>
            <ta e="T367" id="Seg_9668" s="T359">He said: “You drown me with a big rock, find a big rock.”</ta>
            <ta e="T372" id="Seg_9669" s="T367">They went to look for a big rock.</ta>
            <ta e="T380" id="Seg_9670" s="T372">He lay long or not – he heard a horse drawn cart approaching.</ta>
            <ta e="T385" id="Seg_9671" s="T380">Four or five carts are coming.</ta>
            <ta e="T386" id="Seg_9672" s="T385">They stopped.</ta>
            <ta e="T392" id="Seg_9673" s="T386">A merchant is going home with his wares (/with his wares, with his material). </ta>
            <ta e="T397" id="Seg_9674" s="T392">The merchant kicked the sack made of a cow’s hide.</ta>
            <ta e="T400" id="Seg_9675" s="T397">“What’s this lying here?”</ta>
            <ta e="T403" id="Seg_9676" s="T400">Ichakechika cried out: “Ouch!”</ta>
            <ta e="T406" id="Seg_9677" s="T403">The merchant: “Why are you lying here?”</ta>
            <ta e="T413" id="Seg_9678" s="T406">Ichakechika said: “Let me out, cut [the sack]”.</ta>
            <ta e="T416" id="Seg_9679" s="T413">He cut it.</ta>
            <ta e="T420" id="Seg_9680" s="T416">[Ichakechika] jumped out (/got out).</ta>
            <ta e="T427" id="Seg_9681" s="T420">He had sable hides in his shirt.</ta>
            <ta e="T431" id="Seg_9682" s="T427">The merchant asks: “Why are you lying here?”</ta>
            <ta e="T438" id="Seg_9683" s="T431">“I am lying here, I was diving, I took (/found) sable hides.</ta>
            <ta e="T445" id="Seg_9684" s="T438">I didn’t have any strength, there are lots of wares (/materials) in the water up(?)”.</ta>
            <ta e="T449" id="Seg_9685" s="T445">He showed the sable hides to the merchant.</ta>
            <ta e="T455" id="Seg_9686" s="T449">The merchant became happy: “I also want to dive in the water”.</ta>
            <ta e="T459" id="Seg_9687" s="T455">[Ichakechika] sewed him up in the sack of cow hide.</ta>
            <ta e="T463" id="Seg_9688" s="T459">The merchant is lying in the sack by the water.</ta>
            <ta e="T469" id="Seg_9689" s="T463">Ichakechika took off on the horse drawn cart and with the wares.</ta>
            <ta e="T474" id="Seg_9690" s="T469">The Cossacks brought a big rock and started to tie it.</ta>
            <ta e="T480" id="Seg_9691" s="T474">They say to each other: “Ichakechika, you have cheated enough.</ta>
            <ta e="T482" id="Seg_9692" s="T480">You’ll die in the water.”</ta>
            <ta e="T488" id="Seg_9693" s="T482">The merchant hears this – they are tying a rock, a big rock.</ta>
            <ta e="T491" id="Seg_9694" s="T488">“I’m not Ichakechika.</ta>
            <ta e="T493" id="Seg_9695" s="T491">Ichakechika left.</ta>
            <ta e="T496" id="Seg_9696" s="T493">He cheated me.</ta>
            <ta e="T498" id="Seg_9697" s="T496">I’m a merchant.</ta>
            <ta e="T503" id="Seg_9698" s="T498">He tricked me and sewed me into the sack made of hide”.</ta>
            <ta e="T509" id="Seg_9699" s="T503">The Cossacks say: “You, Ichakechika, have cheated enough.”</ta>
            <ta e="T515" id="Seg_9700" s="T509">The man is crying and shouting in the sack: “I’m a merchant.</ta>
            <ta e="T518" id="Seg_9701" s="T515">I was riding on my cart, with horses.”</ta>
            <ta e="T523" id="Seg_9702" s="T518">They carried him to the water, threw him in, and drowned him with the rock.</ta>
            <ta e="T525" id="Seg_9703" s="T523">They went home.</ta>
            <ta e="T528" id="Seg_9704" s="T525">“We drowned [him] in the water.”</ta>
            <ta e="T534" id="Seg_9705" s="T528">Ichakechika came home on the cart, with the horse.</ta>
            <ta e="T541" id="Seg_9706" s="T534">He got his wares wet, he hung it up on a rope to dry.</ta>
            <ta e="T556" id="Seg_9707" s="T541">At this time, the leader, the old man Qolsaqo is looking towards Ichakechika’s house – the stove is on, the smoke is bellowing from the stove.</ta>
            <ta e="T563" id="Seg_9708" s="T556">He told two Cossacks at once to go and look at the old woman.</ta>
            <ta e="T565" id="Seg_9709" s="T563">They left.</ta>
            <ta e="T575" id="Seg_9710" s="T565">They went into the house – instead of the old woman, it is Ichakechika sitting at the table, angry.</ta>
            <ta e="T581" id="Seg_9711" s="T575">His whole face is all scratched up and bloody, his clothes in tatters.</ta>
            <ta e="T586" id="Seg_9712" s="T581">They said (/came in): “Good day, comrade!”</ta>
            <ta e="T601" id="Seg_9713" s="T586">Ichakechika is sitting there angry: “You put me in the water, you drowned me, and I found lots of wares in the sea.</ta>
            <ta e="T611" id="Seg_9714" s="T601">I have as much as I had strength for, I took it (/my strength didn’t take [more]).</ta>
            <ta e="T615" id="Seg_9715" s="T611">Looks like I've visited the underwater god.”</ta>
            <ta e="T621" id="Seg_9716" s="T615">They left all scared: “He was such a devil.”</ta>
            <ta e="T643" id="Seg_9717" s="T621">To tell the old man Qolsaqo: “We drowned Ichakechika in the water, (?) (/earlier) he found such a lot of wares (/material), he found such things and sable hides.”</ta>
            <ta e="T647" id="Seg_9718" s="T643">The old man Qolsaqo lost his mind.</ta>
            <ta e="T650" id="Seg_9719" s="T647">“How did this happen?”</ta>
            <ta e="T654" id="Seg_9720" s="T650">Ichakechika is a devil, me(/us) (?)”.</ta>
            <ta e="T671" id="Seg_9721" s="T654">The next day two Cossacks came, the old man sent Cossacks to Ichakechika to ask Ichakechika how he got those materials.</ta>
            <ta e="T676" id="Seg_9722" s="T671">We’ll dive into the water for wares too.</ta>
            <ta e="T701" id="Seg_9723" s="T676">Ichakechika said: “Sew inside (/they sew, you sew) cow hide sacks (/sew cow hide sacks up), lie down on the shore of the sea, I’ll sew you up in the sack, I’ll throw you into the water with a rock, with a big rock.</ta>
            <ta e="T705" id="Seg_9724" s="T701">You’ll find all the wares.”</ta>
            <ta e="T712" id="Seg_9725" s="T705">In the beginning the old man Qolsaqo (/the chief): “Who from the town wants [to go]?”</ta>
            <ta e="T721" id="Seg_9726" s="T712">He sewed up the old man Qolsaqo first, and then the other people (/the Cossacks). </ta>
            <ta e="T729" id="Seg_9727" s="T721">Then Ichakechika started throwing the sewed up sacks with the people inside into the water.</ta>
            <ta e="T736" id="Seg_9728" s="T729">He said to the old man Qolsaqo: “Look for the wares on the bottom of the sea.</ta>
            <ta e="T743" id="Seg_9729" s="T736">You will not find wealth and wares anywhere.</ta>
            <ta e="T745" id="Seg_9730" s="T743">You’ll die forever.</ta>
            <ta e="T752" id="Seg_9731" s="T745">You threw me in, you threw me into the water.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2" id="Seg_9732" s="T0">Alter Mann Qolsaqo. </ta>
            <ta e="T5" id="Seg_9733" s="T2">Ichakechika lebte bei seiner Großmutter. </ta>
            <ta e="T11" id="Seg_9734" s="T5">Die Stadt des alten Mannes Qolsago war nicht weit weg. </ta>
            <ta e="T20" id="Seg_9735" s="T11">Einmal war die Kuh des alten Mannes Qolsaqo weg, sie war immer weg. </ta>
            <ta e="T25" id="Seg_9736" s="T20">Er weiß, dass Ichakechika sie gestohlen hat. </ta>
            <ta e="T33" id="Seg_9737" s="T25">Er schickte zwei Kosaken zu ihm, um zu sehen, was Ichakechika macht. </ta>
            <ta e="T42" id="Seg_9738" s="T33">Er [Ichakechika] weiß: sie werden mit einem Gewehr kommen, ihn gefangen nehmen, ihn ins Gefägnis stecken.</ta>
            <ta e="T51" id="Seg_9739" s="T42">Währendessen, brachte er die Kuh auf und füllte ihre Eingeweide mit Blut. </ta>
            <ta e="T56" id="Seg_9740" s="T51">Er hatte ein großes Messer in der Decke. </ta>
            <ta e="T61" id="Seg_9741" s="T56">Er bedeckte seine Großmutter mit den blutigen Eingeweiden der Kuh.</ta>
            <ta e="T64" id="Seg_9742" s="T61">Die Großmutter zog sich an. </ta>
            <ta e="T68" id="Seg_9743" s="T64">Die Kossaken kamen zu Ichakechika.</ta>
            <ta e="T72" id="Seg_9744" s="T68">Der Chef sagte Ichakechika: </ta>
            <ta e="T76" id="Seg_9745" s="T72">"Du musst ins Gefängnis. </ta>
            <ta e="T80" id="Seg_9746" s="T76">Du tötest Kühe und isst sie."</ta>
            <ta e="T84" id="Seg_9747" s="T80">Er wurde sauer und sagte zur Großmutter: </ta>
            <ta e="T87" id="Seg_9748" s="T84">"Sie werden mich ins Gefängnis einsperren.</ta>
            <ta e="T90" id="Seg_9749" s="T87">Heiz den Topf, schnell!"</ta>
            <ta e="T95" id="Seg_9750" s="T90">Die Großmutter fing an sich zu beschweren: "Meine Füße und Hände schmerzen!"</ta>
            <ta e="T99" id="Seg_9751" s="T95">Die Kossaken stehen in der Tür. </ta>
            <ta e="T107" id="Seg_9752" s="T99">Ichakechika [sagt] zur Großmutter: "Du faules Stück, du willst den Kessel nicht heizen".</ta>
            <ta e="T116" id="Seg_9753" s="T107">Ichakechika wurde sauer, nahmen das Messer aus der Decke, zerschnitt die Großmutter, die Großmutter starb. </ta>
            <ta e="T124" id="Seg_9754" s="T116">Dann Ichakechika sagte zu den Kossaken: "Meine Großmutter hat sich immer so benommen."</ta>
            <ta e="T127" id="Seg_9755" s="T124">Er steckte das Messer in ihr Bein. </ta>
            <ta e="T132" id="Seg_9756" s="T127">"Großmutter, steh auf, mein Messer wird [dich] beleben."</ta>
            <ta e="T139" id="Seg_9757" s="T132">Die Großmutter sprang auf die Beine und nahm schnell den Kessel um ihn zu heizen. </ta>
            <ta e="T144" id="Seg_9758" s="T139">In [der Zeit] in der man eine Pfeife raucht, war der Kessel geheizt. </ta>
            <ta e="T149" id="Seg_9759" s="T144">Sie stellte schnell das Brot und das Essen auf den Tisch. </ta>
            <ta e="T151" id="Seg_9760" s="T149">Die Kossaken fingen an zu essen. </ta>
            <ta e="T158" id="Seg_9761" s="T151">Sie sagten: "Wir werden dein Messer kaufen."</ta>
            <ta e="T163" id="Seg_9762" s="T158">Unsere Ehefrauen sind faul und schlafen. </ta>
            <ta e="T172" id="Seg_9763" s="T163">Die Kossaken rührten Ichakechika nicht an, sie werden sagen (/sagten) zum alten Mann Qolsago, dass er getötet wurde. </ta>
            <ta e="T174" id="Seg_9764" s="T172">Sie sagten das. </ta>
            <ta e="T178" id="Seg_9765" s="T174">Der Chef schickte [sie] los um das Messer zu kaufen. </ta>
            <ta e="T181" id="Seg_9766" s="T178">Sie kauften es am nächsten Tag. </ta>
            <ta e="T189" id="Seg_9767" s="T181">Der alte Mann Qolsago sagte: "Gebt mir das Messer, Ich werde es für eine Nacht behalten.</ta>
            <ta e="T194" id="Seg_9768" s="T189">Meine Ehefrau und die Arbeiter sind faul. </ta>
            <ta e="T198" id="Seg_9769" s="T194">Sie schlafen tag und nacht."</ta>
            <ta e="T204" id="Seg_9770" s="T198">Ichakechika sagte: "Halte das Messer sauber.</ta>
            <ta e="T207" id="Seg_9771" s="T204">Um eine gute Person zu schneiden."</ta>
            <ta e="T214" id="Seg_9772" s="T207">Der alte Mann Qolsaqo schlief ein wenig, er stand früh am morgen auf. </ta>
            <ta e="T219" id="Seg_9773" s="T214">"Frau, steh auf, und Arbeiter, steht auf!"</ta>
            <ta e="T223" id="Seg_9774" s="T219">Sie sind nicht schnell aufgestanden. </ta>
            <ta e="T224" id="Seg_9775" s="T223">Er wurde wütend. </ta>
            <ta e="T229" id="Seg_9776" s="T224">Er nahm das Messer von oben und erstach sie alle. </ta>
            <ta e="T235" id="Seg_9777" s="T229">Er stach das Messer in das Bein der erschlagenen Menschen. </ta>
            <ta e="T237" id="Seg_9778" s="T235">Er sagte auch [die magischen Wörter]. </ta>
            <ta e="T239" id="Seg_9779" s="T237">Sie starben für immer. </ta>
            <ta e="T243" id="Seg_9780" s="T239">Der alte Mann wurde wütend und rief die Kossaken.</ta>
            <ta e="T246" id="Seg_9781" s="T243">"Woher habt ihr das Messer?"</ta>
            <ta e="T251" id="Seg_9782" s="T246">"Er (/Ichakechika) gab uns das Messer. </ta>
            <ta e="T254" id="Seg_9783" s="T251">Er, Ichakechika, hat uns betrogen."</ta>
            <ta e="T257" id="Seg_9784" s="T254">"Fangt ihn und verurteilt ihn."</ta>
            <ta e="T264" id="Seg_9785" s="T257">Unsere Frauen (/meine Frau und die Frauen) sind alle von diesem Messer getötet worden."</ta>
            <ta e="T269" id="Seg_9786" s="T264">"Du hast sie jedenfalls auf die falsche Weise erstochen" [, sagten seine Soldaten.]</ta>
            <ta e="T276" id="Seg_9787" s="T269">Du musst ihn noch einen Tag in lassen, wir werden es auch versuchen.</ta>
            <ta e="T280" id="Seg_9788" s="T276">Unsere Ehefrauen sind auch faul.</ta>
            <ta e="T287" id="Seg_9789" s="T280">Dann sagte der alte Mann Qolsaqo: "Gut, probiert das Messer."</ta>
            <ta e="T293" id="Seg_9790" s="T287">Zwei Kossaken erstachen auch ihre Ehefrauen.</ta>
            <ta e="T297" id="Seg_9791" s="T293">Ihre Ehefrauen starben (bedeckt) mit Blut. </ta>
            <ta e="T305" id="Seg_9792" s="T297">Am morgen sagten sie auch zum alten Mann Qolsaqo: "Unsere Ehefrauen sind für immer gestorben."</ta>
            <ta e="T307" id="Seg_9793" s="T305">Der alte Mann wurde wütend.</ta>
            <ta e="T310" id="Seg_9794" s="T307">"Ichakechika muss gefangen genommen werden."</ta>
            <ta e="T312" id="Seg_9795" s="T310">Die Kossaken zogen los. </ta>
            <ta e="T316" id="Seg_9796" s="T312">Zu dieser Zeit sitzt Ichakechika zu Hause. </ta>
            <ta e="T318" id="Seg_9797" s="T316">Sie fingen Ichakechika. </ta>
            <ta e="T320" id="Seg_9798" s="T318">"Du hast genug betrogen.</ta>
            <ta e="T330" id="Seg_9799" s="T320">Wir haben all unsere Frauen und Arbeiter erstochen, sie alle starben in Blut."</ta>
            <ta e="T333" id="Seg_9800" s="T330">Sie brachten Ichakechika ins Gefängnis. </ta>
            <ta e="T339" id="Seg_9801" s="T333">Der alte Mann Qolsaqo bestimmte, dass sie gehen sollten und ihn ertränken. </ta>
            <ta e="T345" id="Seg_9802" s="T339">Er befahl ihnen ihn lebendig in eine Kuhhaut einzunähen. </ta>
            <ta e="T354" id="Seg_9803" s="T345">The Kossaken nahmen ihn auf einem Wagen [auf einem Schlitten mit einem Pferd] zum Ufer des Wasser, zum Meer. </ta>
            <ta e="T359" id="Seg_9804" s="T354">Er liegt lebendig im Sack. </ta>
            <ta e="T367" id="Seg_9805" s="T359">Er sagte: "Ihr versenkt mich mit einem großen Stein, findet einen großen Stein."</ta>
            <ta e="T372" id="Seg_9806" s="T367">Sie gingen los um einen großen Stein zu finden. </ta>
            <ta e="T380" id="Seg_9807" s="T372">Er lag dort lange oder nicht, er hörte, dass sich ein Pferdewagen näherte. </ta>
            <ta e="T385" id="Seg_9808" s="T380">Vier oder fünf Wagen kommen. </ta>
            <ta e="T386" id="Seg_9809" s="T385">Sie hielten an. </ta>
            <ta e="T392" id="Seg_9810" s="T386">Ein Kaufmann geht mit seinen Waren (/mit seinen Waren, mit seinem Material) nach Hause. </ta>
            <ta e="T397" id="Seg_9811" s="T392">Der Kaufmann trat den aus Kuhhaut gemachten Sack. </ta>
            <ta e="T400" id="Seg_9812" s="T397">"Was liegt hier?"</ta>
            <ta e="T403" id="Seg_9813" s="T400">Ichakechika schrie auf: "Aua!"</ta>
            <ta e="T406" id="Seg_9814" s="T403">Der Kaufmann: "Warum liegt du hier?"</ta>
            <ta e="T413" id="Seg_9815" s="T406">Ichakechika sagte: "Lass mich raus, zerschneid [den Sack]".</ta>
            <ta e="T416" id="Seg_9816" s="T413">Er zerschnitt ihn.</ta>
            <ta e="T420" id="Seg_9817" s="T416">[Ichakechika] sprang raus (/kam heraus). </ta>
            <ta e="T427" id="Seg_9818" s="T420">Er hatte Zobelhäute in seinem Shirt. </ta>
            <ta e="T431" id="Seg_9819" s="T427">Der Kaufmann sagte: "Warum liegt du hier?"</ta>
            <ta e="T438" id="Seg_9820" s="T431">"Ich liege hier, tauchte ins Wasser, Ich nahm (/fand) Zobelhäute.</ta>
            <ta e="T445" id="Seg_9821" s="T438">Ich hatte keine Kraft, da sind noch viele Waren (/Materialien) im Wasser. </ta>
            <ta e="T449" id="Seg_9822" s="T445">Er zeigte dem Kaufmann die Zobelhäute.</ta>
            <ta e="T455" id="Seg_9823" s="T449">Der Kaufmann wurde glücklich: "Ich will auch ins Wasser tauchen."</ta>
            <ta e="T459" id="Seg_9824" s="T455">[Ichakechika] nähte ihn in einen Sack aus Kuhhäuten. </ta>
            <ta e="T463" id="Seg_9825" s="T459">Der Kaufmann liegt in einem Sack beim Wasser. </ta>
            <ta e="T469" id="Seg_9826" s="T463">Ichakechika fuhr auf dem Pferdewagen mit den Waren weg. </ta>
            <ta e="T474" id="Seg_9827" s="T469">Die Kossaken brachten einen großen Stein und fingen an ihn festzubinden. </ta>
            <ta e="T480" id="Seg_9828" s="T474">Sie sagten zu einander: "Ichakechika, du hast genug betrogen.</ta>
            <ta e="T482" id="Seg_9829" s="T480">Du wirst im Wasser sterben."</ta>
            <ta e="T488" id="Seg_9830" s="T482">Der Kaufmann hört das - sie binden einen Stein fest, einen großen Stein. </ta>
            <ta e="T491" id="Seg_9831" s="T488">"Ich bin nicht Ichakechika.</ta>
            <ta e="T493" id="Seg_9832" s="T491">Ichakechika ist weggegangen.</ta>
            <ta e="T496" id="Seg_9833" s="T493">Er hat mich betrogen.</ta>
            <ta e="T498" id="Seg_9834" s="T496">Ich bin ein Kaufmann. </ta>
            <ta e="T503" id="Seg_9835" s="T498">Er hat mich hereingelegt und mich in den Sack aus Häuten genäht."</ta>
            <ta e="T509" id="Seg_9836" s="T503">Die Kossaken sagen: "Du, Ichakechika, hast genug betrogen."</ta>
            <ta e="T515" id="Seg_9837" s="T509">Der Mann weint und schreit im Sack: "Ich bin ein Kaufmann. </ta>
            <ta e="T518" id="Seg_9838" s="T515">Ich bin meinen Wagen mit Pferden gefahren."</ta>
            <ta e="T523" id="Seg_9839" s="T518">Sie trugen ihn zum Wasser, warfen ihn rein, und ertränkten ihn mit dem Stein.</ta>
            <ta e="T525" id="Seg_9840" s="T523">Sie gingen nach Hause. </ta>
            <ta e="T528" id="Seg_9841" s="T525">"Wir haben [ihn] ertränkt."</ta>
            <ta e="T534" id="Seg_9842" s="T528">Ichakechika kam mit dem Wagen mit den Pferden nach Hause. </ta>
            <ta e="T541" id="Seg_9843" s="T534">Seine Ware war nass geworden, er hing sie an ein Seil zum trocknen. </ta>
            <ta e="T556" id="Seg_9844" s="T541">Zu dieser Zeit guckte der Anführer, der alte Mann Qolsaqo in Richtung von Ichakechikas Haus - der Ofen ist an, Rauch kommt aus dem Ofen. </ta>
            <ta e="T563" id="Seg_9845" s="T556">Er befahl den Kossaken sofort zu gehen und nach der alten Frau zu gucken.</ta>
            <ta e="T565" id="Seg_9846" s="T563">Sie gingen. </ta>
            <ta e="T575" id="Seg_9847" s="T565">Sie gingen ins Haus - anstatt der alten Frau sitzt Ichakechika am Tisch, wütend. </ta>
            <ta e="T581" id="Seg_9848" s="T575">Sein ganzes Gesicht ist zerkratzt und blutig, seine Kleidung ist zerissen. </ta>
            <ta e="T586" id="Seg_9849" s="T581">Sie sagten (/kamen rein): "Guten Tag, Kamerad!"</ta>
            <ta e="T601" id="Seg_9850" s="T586">Ichakechika sitzt wütend da: "Ihr habt mich ins Wasser geworfen, mich ertränkt, und ich habe viele Waren im Meer gefunden."</ta>
            <ta e="T611" id="Seg_9851" s="T601">Ich habe so viel genommen wie ich tragen konnte, meine Kraft hat nicht für mehr gereicht.</ta>
            <ta e="T615" id="Seg_9852" s="T611">Sieht aus als hätte ich den Unterwassergott besucht."</ta>
            <ta e="T621" id="Seg_9853" s="T615">Sie gingen, fürchteten sich: "Er war so ein Teufel."</ta>
            <ta e="T643" id="Seg_9854" s="T621">Sie erzählten dem alten Mann Qolsaqo: "Wir haben Ichakechika im Wasser ertränkt, (?) (/früher) fand er so viele Waren (/Materialien), er fand solche Dinge und Zobelhäute. </ta>
            <ta e="T647" id="Seg_9855" s="T643">Der alte Mann Qolsaqo verlor seinen Verstand.</ta>
            <ta e="T650" id="Seg_9856" s="T647">"Wie ist das passiert?"</ta>
            <ta e="T654" id="Seg_9857" s="T650">Ichakechika ist ein Teufel, mir(/uns) (?)". </ta>
            <ta e="T671" id="Seg_9858" s="T654">Am nächsten Tag kamen zwei Kossaken, der alte Mann schickte Kossaken zu Ichakechika um Ichakechika zu fragen, wie er diese Dinge bekommen hatte. </ta>
            <ta e="T676" id="Seg_9859" s="T671">Wir tauchen auf für Ware. </ta>
            <ta e="T701" id="Seg_9860" s="T676">Ichakechika sagte: "Näht (/sie nähten, ihr näht) Säcke aus Kuhhaut (/näht Kuhhautsäcke), legt euch ans Meeresufer, ich werde euch einnähen, ich werde euch mit einem Stein, einem großen Stein, ins Wasser werfen. </ta>
            <ta e="T705" id="Seg_9861" s="T701">Ihr werdet die Ware finden."</ta>
            <ta e="T712" id="Seg_9862" s="T705">Am Anfang sagte der alte Mann Qolsaqo (/der Anführer): "Wer aus der Stadt möchte [gehen]?"</ta>
            <ta e="T721" id="Seg_9863" s="T712">Er nähte erst den alten Mann Qolsaqo ein, dann die anderen Leute (/die Kossaiken). </ta>
            <ta e="T729" id="Seg_9864" s="T721">Dann fing Ichakechika an die zunähten Säcke mit den Menschen darin ins Wasser zu werfen. </ta>
            <ta e="T736" id="Seg_9865" s="T729">Er sagte zum alten Mann Qolsaqo: "Such auf dem Grund des Meeres nach den Waren.</ta>
            <ta e="T743" id="Seg_9866" s="T736">Du wirst weder Reichtum noch Waren finden.</ta>
            <ta e="T745" id="Seg_9867" s="T743">Du wirst für immer sterben.</ta>
            <ta e="T752" id="Seg_9868" s="T745">Du hast ich reingeworfen, du hast mich ins Wasser geworfen."</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T2" id="Seg_9869" s="T0">урядник</ta>
            <ta e="T5" id="Seg_9870" s="T2">жил-был Ичак Ечика с бабушкой</ta>
            <ta e="T11" id="Seg_9871" s="T5">царский город недалеко был</ta>
            <ta e="T20" id="Seg_9872" s="T11">однажды корова всегда терялась постоянно теряется</ta>
            <ta e="T25" id="Seg_9873" s="T20">он знает что ворует</ta>
            <ta e="T33" id="Seg_9874" s="T25">послал к нему слугу двоих смотреть что делает и.-е.</ta>
            <ta e="T42" id="Seg_9875" s="T33">он (Ичак) знает придут с ружьем его поймают посадят в тюрьму</ta>
            <ta e="T51" id="Seg_9876" s="T42">в это время корову забил кишки кровью наполнил (вылил)</ta>
            <ta e="T56" id="Seg_9877" s="T51">держал большой нож на потолке</ta>
            <ta e="T61" id="Seg_9878" s="T56">бабушку обкрутил кишками коровы, заполненные кровью</ta>
            <ta e="T64" id="Seg_9879" s="T61">бабушка одела парку</ta>
            <ta e="T68" id="Seg_9880" s="T64">пришли к ичику</ta>
            <ta e="T72" id="Seg_9881" s="T68">начальник сказал ичеку</ta>
            <ta e="T76" id="Seg_9882" s="T72">тебя посадить надо в тюрьму</ta>
            <ta e="T80" id="Seg_9883" s="T76">ты коров убиваешь и ешь</ta>
            <ta e="T84" id="Seg_9884" s="T80">он рассердился бабушке сказал</ta>
            <ta e="T87" id="Seg_9885" s="T84">меня посадят в тюрьму</ta>
            <ta e="T90" id="Seg_9886" s="T87">чайник кипяти скорей</ta>
            <ta e="T95" id="Seg_9887" s="T90">бабушка заругалась ноги руки болят</ta>
            <ta e="T99" id="Seg_9888" s="T95">стоят около дверей</ta>
            <ta e="T107" id="Seg_9889" s="T99">бабушка ты лентяйка чайник не хочешь кипятить</ta>
            <ta e="T116" id="Seg_9890" s="T107">рассердился внучек ножик сверху взял до клочка разрезал [резал] бабушка умерла</ta>
            <ta e="T124" id="Seg_9891" s="T116">потом сказал солдатам [слугам] бабушка всегда так лентяйничает</ta>
            <ta e="T127" id="Seg_9892" s="T124">ножик в ногу (бабушки) воткнул</ta>
            <ta e="T132" id="Seg_9893" s="T127">бабушка вставай нож оживит тебя</ta>
            <ta e="T139" id="Seg_9894" s="T132">потом бабушка поднялась (на ноги) скорее схватила чайник кипятить</ta>
            <ta e="T144" id="Seg_9895" s="T139">трубку за время искурить вскипел</ta>
            <ta e="T149" id="Seg_9896" s="T144">хлеб и еду поставила на стол быстро</ta>
            <ta e="T151" id="Seg_9897" s="T149">солдаты начали есть</ta>
            <ta e="T158" id="Seg_9898" s="T151">они так сказали мы твой нож купим</ta>
            <ta e="T163" id="Seg_9899" s="T158">наши жены лентяйничают и спят</ta>
            <ta e="T172" id="Seg_9900" s="T163">солдаты не стали трогать его скажут [сказали] убили</ta>
            <ta e="T174" id="Seg_9901" s="T172">сказали</ta>
            <ta e="T178" id="Seg_9902" s="T174">велел купить нож</ta>
            <ta e="T181" id="Seg_9903" s="T178">на другой день купили</ta>
            <ta e="T189" id="Seg_9904" s="T181">сказал нож мне дайте на ночь у меня пусть будет [я продержу ночь его]</ta>
            <ta e="T194" id="Seg_9905" s="T189">моя жена лентяйничают</ta>
            <ta e="T198" id="Seg_9906" s="T194">день и ночь спят</ta>
            <ta e="T204" id="Seg_9907" s="T198">наказал [говорил] нож в чистоте (со)держите</ta>
            <ta e="T207" id="Seg_9908" s="T204">хорошего человека резать</ta>
            <ta e="T214" id="Seg_9909" s="T207">спал утром рано встал [поднялся]</ta>
            <ta e="T219" id="Seg_9910" s="T214">жена поднимись поднимайтесь</ta>
            <ta e="T223" id="Seg_9911" s="T219">они быстро не поднимались</ta>
            <ta e="T224" id="Seg_9912" s="T223">он рассердился</ta>
            <ta e="T229" id="Seg_9913" s="T224">снял (сверху) ножик порезал всех</ta>
            <ta e="T235" id="Seg_9914" s="T229">ножик воткнул в ноги всем резанным людям</ta>
            <ta e="T237" id="Seg_9915" s="T235">тоже говорил (волшебное слово)</ta>
            <ta e="T239" id="Seg_9916" s="T237">навек умерли</ta>
            <ta e="T243" id="Seg_9917" s="T239">старик рассердился (на Ичик Ичика)</ta>
            <ta e="T246" id="Seg_9918" s="T243">откуда вы брали ножик? </ta>
            <ta e="T251" id="Seg_9919" s="T246">он нам нож дал</ta>
            <ta e="T254" id="Seg_9920" s="T251">он Ичек-Ич. обманул</ta>
            <ta e="T257" id="Seg_9921" s="T254">идти поймать судить</ta>
            <ta e="T264" id="Seg_9922" s="T257">женщины [жена и женщины] все умерли от ножа</ta>
            <ta e="T269" id="Seg_9923" s="T264">ты однако неправильно резал (говорят его солдаты)</ta>
            <ta e="T276" id="Seg_9924" s="T269">его оставить еще на один день мы тоже попробуем</ta>
            <ta e="T280" id="Seg_9925" s="T276">наши жены тоже лентяйничают</ta>
            <ta e="T287" id="Seg_9926" s="T280">тоже сказал попробуйте ножик</ta>
            <ta e="T293" id="Seg_9927" s="T287">вдвоем также резали своих жен</ta>
            <ta e="T297" id="Seg_9928" s="T293">жены их умерли резанный в крови [исстекая кровью]</ta>
            <ta e="T305" id="Seg_9929" s="T297">утром сказали начальнику наши жены навек умерли</ta>
            <ta e="T307" id="Seg_9930" s="T305">старик рассердился</ta>
            <ta e="T310" id="Seg_9931" s="T307">схватить надо</ta>
            <ta e="T312" id="Seg_9932" s="T310">слуги пошли</ta>
            <ta e="T316" id="Seg_9933" s="T312">в это время дома сидит</ta>
            <ta e="T318" id="Seg_9934" s="T316">поймали</ta>
            <ta e="T320" id="Seg_9935" s="T318">хватит обманывал</ta>
            <ta e="T330" id="Seg_9936" s="T320">мы всех жен работников порезали они умерли истекая кровью</ta>
            <ta e="T333" id="Seg_9937" s="T330">увезли в тюрьму</ta>
            <ta e="T339" id="Seg_9938" s="T333">начальник велел утопить</ta>
            <ta e="T345" id="Seg_9939" s="T339">сказал в коровью шкуру внутрь зашить живьем</ta>
            <ta e="T354" id="Seg_9940" s="T345">слуги его увезли на телеге на край воды на край моря</ta>
            <ta e="T359" id="Seg_9941" s="T354">он в мешке живой лежит</ta>
            <ta e="T367" id="Seg_9942" s="T359">сказал меня утопите большим камнем большой камень найдите</ta>
            <ta e="T372" id="Seg_9943" s="T367">они пошли искать большой камень</ta>
            <ta e="T380" id="Seg_9944" s="T372">долго ли коротко лежал он – слышит конская телега идет</ta>
            <ta e="T385" id="Seg_9945" s="T380">четыре или пять телег едет</ta>
            <ta e="T386" id="Seg_9946" s="T385">остановились</ta>
            <ta e="T392" id="Seg_9947" s="T386">купец с товаром [с товаром материалом] едет домой</ta>
            <ta e="T397" id="Seg_9948" s="T392">купец пнул ногой мешок из шкуры коровы</ta>
            <ta e="T400" id="Seg_9949" s="T397">что тут лежит?</ta>
            <ta e="T403" id="Seg_9950" s="T400">вскрикнул ой! (от боли)</ta>
            <ta e="T406" id="Seg_9951" s="T403">купец почему лежишь?</ta>
            <ta e="T413" id="Seg_9952" s="T406">сказал отпусти меня [вверх пусти] отрезай (разрешь (мешок)) </ta>
            <ta e="T416" id="Seg_9953" s="T413">он разрезал [распорол]</ta>
            <ta e="T420" id="Seg_9954" s="T416">он выскочил [вышел] (из мешка)</ta>
            <ta e="T427" id="Seg_9955" s="T420">у него соболиные шкуры были (за пазухой)</ta>
            <ta e="T431" id="Seg_9956" s="T427">купец спрашивает зачем лежишь </ta>
            <ta e="T438" id="Seg_9957" s="T431">я лежу в воду нырял соболиные шкуры взял (нашел)</ta>
            <ta e="T445" id="Seg_9958" s="T438">силы не было товаров много в воде</ta>
            <ta e="T449" id="Seg_9959" s="T445">показал соболиные шкуры купцу</ta>
            <ta e="T455" id="Seg_9960" s="T449">купец радовался я тоже нырну (хочу нырять) в воду</ta>
            <ta e="T459" id="Seg_9961" s="T455">его зашил в коровей шкуры мешок</ta>
            <ta e="T463" id="Seg_9962" s="T459">купец лежит около берега в мешке</ta>
            <ta e="T469" id="Seg_9963" s="T463">уехал на лошадиной телеге</ta>
            <ta e="T474" id="Seg_9964" s="T469">приволокли большой камень начинают привязывать</ta>
            <ta e="T480" id="Seg_9965" s="T474">говорят друг другу [между собой] хватит обманывал</ta>
            <ta e="T482" id="Seg_9966" s="T480">в воде [в море] умрешь</ta>
            <ta e="T488" id="Seg_9967" s="T482">купец слышит (разговоры) привязывают камень, большой камень</ta>
            <ta e="T491" id="Seg_9968" s="T488">я не Ичик Ечика</ta>
            <ta e="T493" id="Seg_9969" s="T491">Ичик ушел</ta>
            <ta e="T496" id="Seg_9970" s="T493">он меня обманул</ta>
            <ta e="T498" id="Seg_9971" s="T496">я купец</ta>
            <ta e="T503" id="Seg_9972" s="T498">обманом меня зашил из коровей шкуры в мешок</ta>
            <ta e="T509" id="Seg_9973" s="T503">говорят ты Ичек достаточно обманывал</ta>
            <ta e="T515" id="Seg_9974" s="T509">он плача кричит в мешке я купец</ta>
            <ta e="T518" id="Seg_9975" s="T515">ехал на лошадях на телеге</ta>
            <ta e="T523" id="Seg_9976" s="T518">в воду увезя бросили утопили с камнем</ta>
            <ta e="T525" id="Seg_9977" s="T523">домой ушли</ta>
            <ta e="T528" id="Seg_9978" s="T525">мы утопили в воде</ta>
            <ta e="T534" id="Seg_9979" s="T528">Ичек приехал домой на лошади на телеге</ta>
            <ta e="T541" id="Seg_9980" s="T534">товар [мануфактуру] водой намочил на веревку вверх повесил сушить</ta>
            <ta e="T556" id="Seg_9981" s="T541">в это время смотрит со стороны чума [дома] Ичека печь топится густо [сильно] дым идет от печки</ta>
            <ta e="T563" id="Seg_9982" s="T556">тут же сказал своим слугам [двум] идти смотреть бабушку</ta>
            <ta e="T565" id="Seg_9983" s="T563">они ушли</ta>
            <ta e="T575" id="Seg_9984" s="T565">в дом зашли вместо старухи на месте сидит за столом Ичек сердитый</ta>
            <ta e="T581" id="Seg_9985" s="T575">все лицо исцарапано в крови одежда рваная [изорванная]</ta>
            <ta e="T586" id="Seg_9986" s="T581">они зашли здорово, товарищ</ta>
            <ta e="T601" id="Seg_9987" s="T586">сердитый сидит вы меня в воду пустили, утопили я в море много товара нашел</ta>
            <ta e="T611" id="Seg_9988" s="T601">столько есть сколько силы было взял [моя сила не взяла]</ta>
            <ta e="T615" id="Seg_9989" s="T611">как вроде у подводного бога [в рае, на небе] был</ta>
            <ta e="T621" id="Seg_9990" s="T615">они пошли испуганные какой черт был</ta>
            <ta e="T643" id="Seg_9991" s="T621">начальнику рассказы мы в воде утопили он столько нашел товару вещи все нашел соболиные шкуры</ta>
            <ta e="T647" id="Seg_9992" s="T643">начальник разум [ум] потерял</ta>
            <ta e="T650" id="Seg_9993" s="T647">как это так получилось</ta>
            <ta e="T654" id="Seg_9994" s="T650">колдун [дьявол] меня [нас]</ta>
            <ta e="T671" id="Seg_9995" s="T654">на другой день сами двое слуг пришли к ичеку послал слуг старик спросить как он достал товар</ta>
            <ta e="T676" id="Seg_9996" s="T671">мы тоже в воду нырять будем за товаром</ta>
            <ta e="T701" id="Seg_9997" s="T676">рассказывает зашивайте [больше зашивайте] из коровьей шкуры [кожи] мешки ложитесь на берег моря я вас зашью в куль брошу в воду с большими камнями</ta>
            <ta e="T705" id="Seg_9998" s="T701">вы найдете весь товар [богатство]</ta>
            <ta e="T712" id="Seg_9999" s="T705">вначале начальник кто согласен (хочет) из города</ta>
            <ta e="T721" id="Seg_10000" s="T712">он зашил вначале начальника потом других людей (слуг)</ta>
            <ta e="T729" id="Seg_10001" s="T721">потом сам бросать стал зашитые мешки с людьми в воду</ta>
            <ta e="T736" id="Seg_10002" s="T729">сказал начальнику ищи товар на дне моря</ta>
            <ta e="T743" id="Seg_10003" s="T736">нигде не найдешь богатство и товара</ta>
            <ta e="T745" id="Seg_10004" s="T743">навек умрешь</ta>
            <ta e="T752" id="Seg_10005" s="T745">вы меня замучивали (бросали) в воду меня бросали</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T5" id="Seg_10006" s="T2">[OSV]: The sentence segmentation is kept according to the numbering in the manuscript.</ta>
            <ta e="T20" id="Seg_10007" s="T11">[BrM]: telʼde might be a misprint of melʼde 'always, constantly'.</ta>
            <ta e="T72" id="Seg_10008" s="T68">[OSV]: The ending of the word “′ичакӓчиканы” can be badly distinguished.</ta>
            <ta e="T107" id="Seg_10009" s="T99">[BrM]: In "lʼenčajmont" m was changed to ŋ, because the form seems to be Present, and it is not known to have m at the beginning of the suffix.</ta>
            <ta e="T229" id="Seg_10010" s="T224">[BrM]: The root moːt- might also be the main allomorph of the verb moːt- 'to hit', but in this case one would expect multiobjective suffix -qɨl, and not -äl.</ta>
            <ta e="T246" id="Seg_10011" s="T243">[BrM] One would expext the form iː-sa-lɨt 'take-PST-2PL'.</ta>
            <ta e="T264" id="Seg_10012" s="T257">The word "paŋɨnon" has been changed into two words paŋɨn nɔːn.</ta>
            <ta e="T293" id="Seg_10013" s="T287">[BrM]: Unclear verb-subject agreement. Probably, kazat might be also the nominative singular form, then šität would be 'two-3SG'.</ta>
            <ta e="T438" id="Seg_10014" s="T431">Unclear usage of object conjugation in "ippap"-</ta>
            <ta e="T449" id="Seg_10015" s="T445">[BrM]: Unclear usage of the stem "atɨlʼ-", actually the stem is known to be intransitive with the meaning "to be visible", one would expect the stem "at-altɨ-" with the meaning "to show" to be used in this context.</ta>
            <ta e="T463" id="Seg_10016" s="T459">[BrM]: Unknown form '(-)totqip'.</ta>
            <ta e="T480" id="Seg_10017" s="T474">[BrM]: Unknown stem 'täntɔː-'. tom- 'speak'?</ta>
            <ta e="T523" id="Seg_10018" s="T518">[BrM]: Unclear usage of the stem "qoptɨlʼ-". This stem is known to be intransitive with the meaning "to sink", one would expect the transitive stem "qoptɨ-rɨ-" with the meaning "to drown" to be used in this context.</ta>
            <ta e="T563" id="Seg_10019" s="T556">[BrM]: Unclear forms 'tɨmtɨ-sä (nɨmtɨ-sä)', Comitative here is in question.</ta>
            <ta e="T575" id="Seg_10020" s="T565">[BrM]: Unclear construction 'imaqotat koptɨ čɔːt', rough glossing.</ta>
            <ta e="T601" id="Seg_10021" s="T586">[BrM]: Unclear form 'ütɨsɨp'.</ta>
            <ta e="T615" id="Seg_10022" s="T611">[BrM]: Unclear form 'ütenɨlʼ'.</ta>
            <ta e="T643" id="Seg_10023" s="T621">[BrM]: Unclear usage of infinitive form "kätɨqo". [OSV]: in the manuscript before “о′кот” something like “и′колʼ чʼе̄лʼе” is written, but is hardly readable.</ta>
            <ta e="T654" id="Seg_10024" s="T650">[BrM]: 'mannon (menon)' might be Locative forms of 1SG (1PL) pronouns used in Middle Ob dialects.</ta>
            <ta e="T671" id="Seg_10025" s="T654">[BrM]: -ŋčʼɔː in 'tüŋčʼɔːtɨt' might be an allomorph of intensive perfective suffix -lʼčʼɨ.</ta>
            <ta e="T721" id="Seg_10026" s="T712">[BrM]: Unknown suffix -kolʼ might be a variant of multiobjective derivational suffix -qɨl.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T11" id="Seg_10027" s="T5">′kъ̊ттъ - город</ta>
            <ta e="T90" id="Seg_10028" s="T87">мушереkо - кипятить</ta>
            <ta e="T107" id="Seg_10029" s="T99">лен′тʼатыkо - лентяйничать</ta>
            <ta e="T701" id="Seg_10030" s="T676">сравн. kӓнтак - найду</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T753" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
