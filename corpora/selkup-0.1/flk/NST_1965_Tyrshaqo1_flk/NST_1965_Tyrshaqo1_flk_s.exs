<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDEA70D80C-DB78-C80E-71BB-2D602D8BA1C9">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">flk\NST_1965_Tyrshaqo1_flk\NST_1965_Tyrshaqo1_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">223</ud-information>
            <ud-information attribute-name="# HIAT:w">189</ud-information>
            <ud-information attribute-name="# e">184</ud-information>
            <ud-information attribute-name="# HIAT:u">30</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="NST">
            <abbreviation>NST</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T184" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="NST"
                      type="t">
         <timeline-fork end="T8" start="T7">
            <tli id="T7.tx.1" />
         </timeline-fork>
         <timeline-fork end="T18" start="T17">
            <tli id="T17.tx.1" />
         </timeline-fork>
         <timeline-fork end="T22" start="T21">
            <tli id="T21.tx.1" />
         </timeline-fork>
         <timeline-fork end="T26" start="T25">
            <tli id="T25.tx.1" />
         </timeline-fork>
         <timeline-fork end="T112" start="T111">
            <tli id="T111.tx.1" />
         </timeline-fork>
         <timeline-fork end="T126" start="T125">
            <tli id="T125.tx.1" />
         </timeline-fork>
         <timeline-fork end="T135" start="T134">
            <tli id="T134.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T183" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">İčʼikičʼika</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ilɨmpa</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">imlʼantɨsa</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Puːn</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">İčʼikičʼika</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">nılʼčʼɨk</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">qätɨnɨt</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7.tx.1" id="Seg_28" n="HIAT:w" s="T7">imlʼaqɨntɨ</ts>
                  <nts id="Seg_29" n="HIAT:ip">:</nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7.tx.1">“</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">Mat</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">qəntak</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">Tɨršaːqoː</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">iran</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">mɔːttɨ”</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21.tx.1" id="Seg_50" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">Imɨlʼ</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">imilʼä</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">nılʼčʼɨk</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">kətɨnit</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17.tx.1" id="Seg_64" n="HIAT:w" s="T17">İčʼikičʼikanɨk</ts>
                  <nts id="Seg_65" n="HIAT:ip">:</nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17.tx.1">“</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">Kuttar</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">iːtal</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">qumɨtɨt</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21.tx.1" id="Seg_79" n="HIAT:w" s="T21">nälʼew</ts>
                  <nts id="Seg_80" n="HIAT:ip">?</nts>
               </ts>
               <ts e="T33" id="Seg_82" n="HIAT:u" s="T21.tx.1">
                  <ts e="T22" id="Seg_84" n="HIAT:w" s="T21.tx.1">”</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_87" n="HIAT:w" s="T22">A</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_90" n="HIAT:w" s="T23">İčʼika</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_93" n="HIAT:w" s="T24">nılʼčʼɨk</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25.tx.1" id="Seg_96" n="HIAT:w" s="T25">kättɨŋɨt</ts>
                  <nts id="Seg_97" n="HIAT:ip">:</nts>
                  <ts e="T26" id="Seg_99" n="HIAT:w" s="T25.tx.1">“</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">Man</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_105" n="HIAT:w" s="T27">olʼa</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_108" n="HIAT:w" s="T28">olʼɨka</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_111" n="HIAT:w" s="T29">mannɨmmɛntam</ts>
                  <nts id="Seg_112" n="HIAT:ip">,</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">kuttar</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">qumat</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">imanʼnʼɔːtɨt”</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_125" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">İčʼikičʼika</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">karrä</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">qännä</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_136" n="HIAT:w" s="T36">qɔːsɨlʼannɨtɨ</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_139" n="HIAT:w" s="T38">lʼiːpɨlʼa</ts>
                  <nts id="Seg_140" n="HIAT:ip">.</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_143" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">Karra</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_148" n="HIAT:w" s="T40">nukalnɨt</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_151" n="HIAT:w" s="T41">i</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_154" n="HIAT:w" s="T42">ninɨ</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_157" n="HIAT:w" s="T43">pinɨčʼčʼanarräpɨlʼat</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_160" n="HIAT:w" s="T45">qarr</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_163" n="HIAT:w" s="T46">omta</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_166" n="HIAT:w" s="T47">antaqɨntɨ</ts>
                  <nts id="Seg_167" n="HIAT:ip">.</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_170" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_172" n="HIAT:w" s="T48">Nɨnɨ</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_175" n="HIAT:w" s="T49">qännɨ</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_178" n="HIAT:w" s="T50">Tɨrsaqo</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_181" n="HIAT:w" s="T51">iːranɨk</ts>
                  <nts id="Seg_182" n="HIAT:ip">.</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_185" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_187" n="HIAT:w" s="T52">Ukur</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_190" n="HIAT:w" s="T53">čʼontoːt</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_193" n="HIAT:w" s="T54">toːt</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_196" n="HIAT:w" s="T55">kɨqɨt</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_199" n="HIAT:w" s="T56">pɛrqɨmtɨ</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_202" n="HIAT:w" s="T57">na</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_205" n="HIAT:w" s="T58">šurtuqɨlnɨt</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_209" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_211" n="HIAT:w" s="T59">Toːt</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_214" n="HIAT:w" s="T60">kɨqɨt</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_217" n="HIAT:w" s="T61">pülʼ</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_220" n="HIAT:w" s="T62">laka</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_223" n="HIAT:w" s="T63">qontɨt</ts>
                  <nts id="Seg_224" n="HIAT:ip">.</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_227" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_229" n="HIAT:w" s="T64">Na</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_232" n="HIAT:w" s="T65">pülʼ</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_235" n="HIAT:w" s="T66">lakantɨ</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_238" n="HIAT:w" s="T67">omtan</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_241" n="HIAT:w" s="T68">ukur</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_244" n="HIAT:w" s="T69">čʼontoːt</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_247" n="HIAT:w" s="T70">pičʼa</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_250" n="HIAT:w" s="T71">nɨ</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_253" n="HIAT:w" s="T72">satainɨt</ts>
                  <nts id="Seg_254" n="HIAT:ip">.</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_257" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_259" n="HIAT:w" s="T73">Narapulʼamtɨ</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_262" n="HIAT:w" s="T74">orqɨlʼlʼä</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_265" n="HIAT:w" s="T75">pičʼap</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_268" n="HIAT:w" s="T76">olɨ</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_271" n="HIAT:w" s="T77">čʼer</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_274" n="HIAT:w" s="T78">qättɨmit</ts>
                  <nts id="Seg_275" n="HIAT:ip">.</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_278" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_280" n="HIAT:w" s="T79">Ninɨ</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_283" n="HIAT:w" s="T80">pičʼamtɨ</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_286" n="HIAT:w" s="T81">antɨqɨnt</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_289" n="HIAT:w" s="T82">čʼattɨmɨt</ts>
                  <nts id="Seg_290" n="HIAT:ip">,</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_293" n="HIAT:w" s="T83">antaqɨntä</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_296" n="HIAT:w" s="T84">tıŋa</ts>
                  <nts id="Seg_297" n="HIAT:ip">.</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_300" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_302" n="HIAT:w" s="T85">Nɨna</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_305" n="HIAT:w" s="T86">qänna</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_308" n="HIAT:w" s="T87">tiŋa</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_311" n="HIAT:w" s="T88">Tɨršaːqoː</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_314" n="HIAT:w" s="T89">irat</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_317" n="HIAT:w" s="T90">mottɨ</ts>
                  <nts id="Seg_318" n="HIAT:ip">.</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_321" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_323" n="HIAT:w" s="T91">Šittɨ</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_326" n="HIAT:w" s="T92">antɨ</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_329" n="HIAT:w" s="T93">tottɨnta</ts>
                  <nts id="Seg_330" n="HIAT:ip">.</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_333" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_335" n="HIAT:w" s="T94">Tüŋä</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_338" n="HIAT:w" s="T95">anɨmtɨ</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_341" n="HIAT:w" s="T96">konna</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_344" n="HIAT:w" s="T97">nʼäqɨlnɨtä</ts>
                  <nts id="Seg_345" n="HIAT:ip">.</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_348" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_350" n="HIAT:w" s="T98">Qonna</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_353" n="HIAT:w" s="T99">tanta</ts>
                  <nts id="Seg_354" n="HIAT:ip">,</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_357" n="HIAT:w" s="T100">mɔːttɨ</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_360" n="HIAT:w" s="T101">čʼap</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_363" n="HIAT:w" s="T102">šerna</ts>
                  <nts id="Seg_364" n="HIAT:ip">.</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_367" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_369" n="HIAT:w" s="T103">Moːtqɨt</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_372" n="HIAT:w" s="T104">täːtti</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_375" n="HIAT:w" s="T105">qum</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_378" n="HIAT:w" s="T106">omta</ts>
                  <nts id="Seg_379" n="HIAT:ip">.</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_382" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_384" n="HIAT:w" s="T107">Puːt</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_387" n="HIAT:w" s="T108">Tɨršaːqoː</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_390" n="HIAT:w" s="T109">ira</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_393" n="HIAT:w" s="T110">nilʼčʼik</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111.tx.1" id="Seg_396" n="HIAT:w" s="T111">kättɨŋɨtɨ</ts>
                  <nts id="Seg_397" n="HIAT:ip">:</nts>
                  <ts e="T112" id="Seg_399" n="HIAT:w" s="T111.tx.1">“</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_402" n="HIAT:w" s="T112">İčʼikičʼika</ts>
                  <nts id="Seg_403" n="HIAT:ip">,</nts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_406" n="HIAT:w" s="T113">ıl</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_409" n="HIAT:w" s="T184">omtäsik</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_412" n="HIAT:w" s="T114">Qumɨt</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_415" n="HIAT:w" s="T115">imanʼnʼɔːtɨt</ts>
                  <nts id="Seg_416" n="HIAT:ip">.</nts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_419" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_421" n="HIAT:w" s="T116">Iraqotat</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_424" n="HIAT:w" s="T117">aj</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_427" n="HIAT:w" s="T118">imaqota</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_430" n="HIAT:w" s="T119">ontɔːqij</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_433" n="HIAT:w" s="T120">nɨmtä</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_436" n="HIAT:w" s="T121">əːtɨkɔːlʼ</ts>
                  <nts id="Seg_437" n="HIAT:ip">.</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_440" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_442" n="HIAT:w" s="T122">Put</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_445" n="HIAT:w" s="T123">irra</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_448" n="HIAT:w" s="T124">nilʼčʼik</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125.tx.1" id="Seg_451" n="HIAT:w" s="T125">kättɨŋɨt</ts>
                  <nts id="Seg_452" n="HIAT:ip">:</nts>
                  <ts e="T126" id="Seg_454" n="HIAT:w" s="T125.tx.1">“</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_457" n="HIAT:w" s="T126">Kutaj</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_460" n="HIAT:w" s="T127">šım</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_463" n="HIAT:w" s="T128">pisaltɛnta</ts>
                  <nts id="Seg_464" n="HIAT:ip">,</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_467" n="HIAT:w" s="T129">nammɨnɨk</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_470" n="HIAT:w" s="T130">nälʼam</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_473" n="HIAT:w" s="T131">minɨntam“</ts>
                  <nts id="Seg_474" n="HIAT:ip">.</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_477" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_479" n="HIAT:w" s="T132">İčʼikičʼika</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_482" n="HIAT:w" s="T133">nilʼčʼik</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134.tx.1" id="Seg_485" n="HIAT:w" s="T134">kätɨŋɨt</ts>
                  <nts id="Seg_486" n="HIAT:ip">:</nts>
                  <ts e="T135" id="Seg_488" n="HIAT:w" s="T134.tx.1">“</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_491" n="HIAT:w" s="T135">Mat</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_494" n="HIAT:w" s="T136">toːt</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_497" n="HIAT:w" s="T137">kɨqɨt</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_500" n="HIAT:w" s="T138">pɛrqam</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_503" n="HIAT:w" s="T139">nɨ</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_506" n="HIAT:w" s="T140">šurroqulsɨt</ts>
                  <nts id="Seg_507" n="HIAT:ip">.</nts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_510" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_512" n="HIAT:w" s="T141">Nɨnä</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_515" n="HIAT:w" s="T142">mat</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_518" n="HIAT:w" s="T143">qosa</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_521" n="HIAT:w" s="T144">pülʼ</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_524" n="HIAT:w" s="T145">laka</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_527" n="HIAT:w" s="T146">na</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_530" n="HIAT:w" s="T147">pülʼ</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_533" n="HIAT:w" s="T148">lakantɨ</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_536" n="HIAT:w" s="T149">konna</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_539" n="HIAT:w" s="T150">paktɨsa</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_542" n="HIAT:w" s="T151">tütqɨlʼsam</ts>
                  <nts id="Seg_543" n="HIAT:ip">.</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_546" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_548" n="HIAT:w" s="T152">Pičʼa</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_551" n="HIAT:w" s="T153">nɨ</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_554" n="HIAT:w" s="T154">šım</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_557" n="HIAT:w" s="T155">sattɛːsa</ts>
                  <nts id="Seg_558" n="HIAT:ip">.</nts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T159" id="Seg_561" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_563" n="HIAT:w" s="T156">Mat</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_566" n="HIAT:w" s="T157">narapuːlʼäm</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_569" n="HIAT:w" s="T158">orkɨsam</ts>
                  <nts id="Seg_570" n="HIAT:ip">.</nts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_573" n="HIAT:u" s="T159">
                  <ts e="T160" id="Seg_575" n="HIAT:w" s="T159">Nɨna</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_578" n="HIAT:w" s="T160">pičʼar</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_581" n="HIAT:w" s="T161">olɨ</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_584" n="HIAT:w" s="T162">čʼir</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_587" n="HIAT:w" s="T163">qätsa</ts>
                  <nts id="Seg_588" n="HIAT:ip">.</nts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_591" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_593" n="HIAT:w" s="T164">Nɨna</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_596" n="HIAT:w" s="T165">pičʼap</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_599" n="HIAT:w" s="T166">antaqan</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_602" n="HIAT:w" s="T167">nɨna</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_605" n="HIAT:w" s="T168">čʼatsam</ts>
                  <nts id="Seg_606" n="HIAT:ip">.</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_609" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_611" n="HIAT:w" s="T169">Nɨnɨ</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_614" n="HIAT:w" s="T170">tiː</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_617" n="HIAT:w" s="T171">tüːsak“</ts>
                  <nts id="Seg_618" n="HIAT:ip">.</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_621" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_623" n="HIAT:w" s="T172">İrra</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_626" n="HIAT:w" s="T173">laqqɨmɔːtaiːs</ts>
                  <nts id="Seg_627" n="HIAT:ip">.</nts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_630" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_632" n="HIAT:w" s="T174">Nɨnɨ</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_635" n="HIAT:w" s="T175">irra</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_638" n="HIAT:w" s="T176">nälʼamtä</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_641" n="HIAT:w" s="T177">miːmpat</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_644" n="HIAT:w" s="T178">İčʼikičʼikamik</ts>
                  <nts id="Seg_645" n="HIAT:ip">.</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_648" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_650" n="HIAT:w" s="T179">Qumɨt</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_653" n="HIAT:w" s="T180">nɨnɨ</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_656" n="HIAT:w" s="T181">qənnɔːtɨt</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_659" n="HIAT:w" s="T182">imakɔːlɨk</ts>
                  <nts id="Seg_660" n="HIAT:ip">.</nts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T183" id="Seg_662" n="sc" s="T0">
               <ts e="T1" id="Seg_664" n="e" s="T0">İčʼikičʼika </ts>
               <ts e="T2" id="Seg_666" n="e" s="T1">ilɨmpa </ts>
               <ts e="T3" id="Seg_668" n="e" s="T2">imlʼantɨsa. </ts>
               <ts e="T4" id="Seg_670" n="e" s="T3">Puːn </ts>
               <ts e="T5" id="Seg_672" n="e" s="T4">İčʼikičʼika </ts>
               <ts e="T6" id="Seg_674" n="e" s="T5">nılʼčʼɨk </ts>
               <ts e="T7" id="Seg_676" n="e" s="T6">qätɨnɨt </ts>
               <ts e="T8" id="Seg_678" n="e" s="T7">imlʼaqɨntɨ:“ </ts>
               <ts e="T9" id="Seg_680" n="e" s="T8">Mat </ts>
               <ts e="T10" id="Seg_682" n="e" s="T9">qəntak </ts>
               <ts e="T11" id="Seg_684" n="e" s="T10">Tɨršaːqoː </ts>
               <ts e="T12" id="Seg_686" n="e" s="T11">iran </ts>
               <ts e="T13" id="Seg_688" n="e" s="T12">mɔːttɨ”. </ts>
               <ts e="T14" id="Seg_690" n="e" s="T13">Imɨlʼ </ts>
               <ts e="T15" id="Seg_692" n="e" s="T14">imilʼä </ts>
               <ts e="T16" id="Seg_694" n="e" s="T15">nılʼčʼɨk </ts>
               <ts e="T17" id="Seg_696" n="e" s="T16">kətɨnit </ts>
               <ts e="T18" id="Seg_698" n="e" s="T17">İčʼikičʼikanɨk:“ </ts>
               <ts e="T19" id="Seg_700" n="e" s="T18">Kuttar </ts>
               <ts e="T20" id="Seg_702" n="e" s="T19">iːtal </ts>
               <ts e="T21" id="Seg_704" n="e" s="T20">qumɨtɨt </ts>
               <ts e="T22" id="Seg_706" n="e" s="T21">nälʼew?” </ts>
               <ts e="T23" id="Seg_708" n="e" s="T22">A </ts>
               <ts e="T24" id="Seg_710" n="e" s="T23">İčʼika </ts>
               <ts e="T25" id="Seg_712" n="e" s="T24">nılʼčʼɨk </ts>
               <ts e="T26" id="Seg_714" n="e" s="T25">kättɨŋɨt:“ </ts>
               <ts e="T27" id="Seg_716" n="e" s="T26">Man </ts>
               <ts e="T28" id="Seg_718" n="e" s="T27">olʼa </ts>
               <ts e="T29" id="Seg_720" n="e" s="T28">olʼɨka </ts>
               <ts e="T30" id="Seg_722" n="e" s="T29">mannɨmmɛntam, </ts>
               <ts e="T31" id="Seg_724" n="e" s="T30">kuttar </ts>
               <ts e="T32" id="Seg_726" n="e" s="T31">qumat </ts>
               <ts e="T33" id="Seg_728" n="e" s="T32">imanʼnʼɔːtɨt”. </ts>
               <ts e="T34" id="Seg_730" n="e" s="T33">İčʼikičʼika </ts>
               <ts e="T35" id="Seg_732" n="e" s="T34">karrä </ts>
               <ts e="T36" id="Seg_734" n="e" s="T35">qännä </ts>
               <ts e="T37" id="Seg_736" n="e" s="T36">qɔːsɨlʼ</ts>
               <ts e="T38" id="Seg_738" n="e" s="T37">annɨtɨ </ts>
               <ts e="T39" id="Seg_740" n="e" s="T38">lʼiːpɨlʼa. </ts>
               <ts e="T40" id="Seg_742" n="e" s="T39">Karra </ts>
               <ts e="T41" id="Seg_744" n="e" s="T40">nukalnɨt </ts>
               <ts e="T42" id="Seg_746" n="e" s="T41">i </ts>
               <ts e="T43" id="Seg_748" n="e" s="T42">ninɨ </ts>
               <ts e="T44" id="Seg_750" n="e" s="T43">pinɨčʼčʼa</ts>
               <ts e="T45" id="Seg_752" n="e" s="T44">narräpɨlʼat </ts>
               <ts e="T46" id="Seg_754" n="e" s="T45">qarr </ts>
               <ts e="T47" id="Seg_756" n="e" s="T46">omta </ts>
               <ts e="T48" id="Seg_758" n="e" s="T47">antaqɨntɨ. </ts>
               <ts e="T49" id="Seg_760" n="e" s="T48">Nɨnɨ </ts>
               <ts e="T50" id="Seg_762" n="e" s="T49">qännɨ </ts>
               <ts e="T51" id="Seg_764" n="e" s="T50">Tɨrsaqo </ts>
               <ts e="T52" id="Seg_766" n="e" s="T51">iːranɨk. </ts>
               <ts e="T53" id="Seg_768" n="e" s="T52">Ukur </ts>
               <ts e="T54" id="Seg_770" n="e" s="T53">čʼontoːt </ts>
               <ts e="T55" id="Seg_772" n="e" s="T54">toːt </ts>
               <ts e="T56" id="Seg_774" n="e" s="T55">kɨqɨt </ts>
               <ts e="T57" id="Seg_776" n="e" s="T56">pɛrqɨmtɨ </ts>
               <ts e="T58" id="Seg_778" n="e" s="T57">na </ts>
               <ts e="T59" id="Seg_780" n="e" s="T58">šurtuqɨlnɨt. </ts>
               <ts e="T60" id="Seg_782" n="e" s="T59">Toːt </ts>
               <ts e="T61" id="Seg_784" n="e" s="T60">kɨqɨt </ts>
               <ts e="T62" id="Seg_786" n="e" s="T61">pülʼ </ts>
               <ts e="T63" id="Seg_788" n="e" s="T62">laka </ts>
               <ts e="T64" id="Seg_790" n="e" s="T63">qontɨt. </ts>
               <ts e="T65" id="Seg_792" n="e" s="T64">Na </ts>
               <ts e="T66" id="Seg_794" n="e" s="T65">pülʼ </ts>
               <ts e="T67" id="Seg_796" n="e" s="T66">lakantɨ </ts>
               <ts e="T68" id="Seg_798" n="e" s="T67">omtan </ts>
               <ts e="T69" id="Seg_800" n="e" s="T68">ukur </ts>
               <ts e="T70" id="Seg_802" n="e" s="T69">čʼontoːt </ts>
               <ts e="T71" id="Seg_804" n="e" s="T70">pičʼa </ts>
               <ts e="T72" id="Seg_806" n="e" s="T71">nɨ </ts>
               <ts e="T73" id="Seg_808" n="e" s="T72">satainɨt. </ts>
               <ts e="T74" id="Seg_810" n="e" s="T73">Narapulʼamtɨ </ts>
               <ts e="T75" id="Seg_812" n="e" s="T74">orqɨlʼlʼä </ts>
               <ts e="T76" id="Seg_814" n="e" s="T75">pičʼap </ts>
               <ts e="T77" id="Seg_816" n="e" s="T76">olɨ </ts>
               <ts e="T78" id="Seg_818" n="e" s="T77">čʼer </ts>
               <ts e="T79" id="Seg_820" n="e" s="T78">qättɨmit. </ts>
               <ts e="T80" id="Seg_822" n="e" s="T79">Ninɨ </ts>
               <ts e="T81" id="Seg_824" n="e" s="T80">pičʼamtɨ </ts>
               <ts e="T82" id="Seg_826" n="e" s="T81">antɨqɨnt </ts>
               <ts e="T83" id="Seg_828" n="e" s="T82">čʼattɨmɨt, </ts>
               <ts e="T84" id="Seg_830" n="e" s="T83">antaqɨntä </ts>
               <ts e="T85" id="Seg_832" n="e" s="T84">tıŋa. </ts>
               <ts e="T86" id="Seg_834" n="e" s="T85">Nɨna </ts>
               <ts e="T87" id="Seg_836" n="e" s="T86">qänna </ts>
               <ts e="T88" id="Seg_838" n="e" s="T87">tiŋa </ts>
               <ts e="T89" id="Seg_840" n="e" s="T88">Tɨršaːqoː </ts>
               <ts e="T90" id="Seg_842" n="e" s="T89">irat </ts>
               <ts e="T91" id="Seg_844" n="e" s="T90">mottɨ. </ts>
               <ts e="T92" id="Seg_846" n="e" s="T91">Šittɨ </ts>
               <ts e="T93" id="Seg_848" n="e" s="T92">antɨ </ts>
               <ts e="T94" id="Seg_850" n="e" s="T93">tottɨnta. </ts>
               <ts e="T95" id="Seg_852" n="e" s="T94">Tüŋä </ts>
               <ts e="T96" id="Seg_854" n="e" s="T95">anɨmtɨ </ts>
               <ts e="T97" id="Seg_856" n="e" s="T96">konna </ts>
               <ts e="T98" id="Seg_858" n="e" s="T97">nʼäqɨlnɨtä. </ts>
               <ts e="T99" id="Seg_860" n="e" s="T98">Qonna </ts>
               <ts e="T100" id="Seg_862" n="e" s="T99">tanta, </ts>
               <ts e="T101" id="Seg_864" n="e" s="T100">mɔːttɨ </ts>
               <ts e="T102" id="Seg_866" n="e" s="T101">čʼap </ts>
               <ts e="T103" id="Seg_868" n="e" s="T102">šerna. </ts>
               <ts e="T104" id="Seg_870" n="e" s="T103">Moːtqɨt </ts>
               <ts e="T105" id="Seg_872" n="e" s="T104">täːtti </ts>
               <ts e="T106" id="Seg_874" n="e" s="T105">qum </ts>
               <ts e="T107" id="Seg_876" n="e" s="T106">omta. </ts>
               <ts e="T108" id="Seg_878" n="e" s="T107">Puːt </ts>
               <ts e="T109" id="Seg_880" n="e" s="T108">Tɨršaːqoː </ts>
               <ts e="T110" id="Seg_882" n="e" s="T109">ira </ts>
               <ts e="T111" id="Seg_884" n="e" s="T110">nilʼčʼik </ts>
               <ts e="T112" id="Seg_886" n="e" s="T111">kättɨŋɨtɨ:“ </ts>
               <ts e="T113" id="Seg_888" n="e" s="T112">İčʼikičʼika, </ts>
               <ts e="T184" id="Seg_890" n="e" s="T113">ıl </ts>
               <ts e="T114" id="Seg_892" n="e" s="T184">omtäsik </ts>
               <ts e="T115" id="Seg_894" n="e" s="T114">Qumɨt </ts>
               <ts e="T116" id="Seg_896" n="e" s="T115">imanʼnʼɔːtɨt. </ts>
               <ts e="T117" id="Seg_898" n="e" s="T116">Iraqotat </ts>
               <ts e="T118" id="Seg_900" n="e" s="T117">aj </ts>
               <ts e="T119" id="Seg_902" n="e" s="T118">imaqota </ts>
               <ts e="T120" id="Seg_904" n="e" s="T119">ontɔːqij </ts>
               <ts e="T121" id="Seg_906" n="e" s="T120">nɨmtä </ts>
               <ts e="T122" id="Seg_908" n="e" s="T121">əːtɨkɔːlʼ. </ts>
               <ts e="T123" id="Seg_910" n="e" s="T122">Put </ts>
               <ts e="T124" id="Seg_912" n="e" s="T123">irra </ts>
               <ts e="T125" id="Seg_914" n="e" s="T124">nilʼčʼik </ts>
               <ts e="T126" id="Seg_916" n="e" s="T125">kättɨŋɨt:“ </ts>
               <ts e="T127" id="Seg_918" n="e" s="T126">Kutaj </ts>
               <ts e="T128" id="Seg_920" n="e" s="T127">šım </ts>
               <ts e="T129" id="Seg_922" n="e" s="T128">pisaltɛnta, </ts>
               <ts e="T130" id="Seg_924" n="e" s="T129">nammɨnɨk </ts>
               <ts e="T131" id="Seg_926" n="e" s="T130">nälʼam </ts>
               <ts e="T132" id="Seg_928" n="e" s="T131">minɨntam“. </ts>
               <ts e="T133" id="Seg_930" n="e" s="T132">İčʼikičʼika </ts>
               <ts e="T134" id="Seg_932" n="e" s="T133">nilʼčʼik </ts>
               <ts e="T135" id="Seg_934" n="e" s="T134">kätɨŋɨt:“ </ts>
               <ts e="T136" id="Seg_936" n="e" s="T135">Mat </ts>
               <ts e="T137" id="Seg_938" n="e" s="T136">toːt </ts>
               <ts e="T138" id="Seg_940" n="e" s="T137">kɨqɨt </ts>
               <ts e="T139" id="Seg_942" n="e" s="T138">pɛrqam </ts>
               <ts e="T140" id="Seg_944" n="e" s="T139">nɨ </ts>
               <ts e="T141" id="Seg_946" n="e" s="T140">šurroqulsɨt. </ts>
               <ts e="T142" id="Seg_948" n="e" s="T141">Nɨnä </ts>
               <ts e="T143" id="Seg_950" n="e" s="T142">mat </ts>
               <ts e="T144" id="Seg_952" n="e" s="T143">qosa </ts>
               <ts e="T145" id="Seg_954" n="e" s="T144">pülʼ </ts>
               <ts e="T146" id="Seg_956" n="e" s="T145">laka </ts>
               <ts e="T147" id="Seg_958" n="e" s="T146">na </ts>
               <ts e="T148" id="Seg_960" n="e" s="T147">pülʼ </ts>
               <ts e="T149" id="Seg_962" n="e" s="T148">lakantɨ </ts>
               <ts e="T150" id="Seg_964" n="e" s="T149">konna </ts>
               <ts e="T151" id="Seg_966" n="e" s="T150">paktɨsa </ts>
               <ts e="T152" id="Seg_968" n="e" s="T151">tütqɨlʼsam. </ts>
               <ts e="T153" id="Seg_970" n="e" s="T152">Pičʼa </ts>
               <ts e="T154" id="Seg_972" n="e" s="T153">nɨ </ts>
               <ts e="T155" id="Seg_974" n="e" s="T154">šım </ts>
               <ts e="T156" id="Seg_976" n="e" s="T155">sattɛːsa. </ts>
               <ts e="T157" id="Seg_978" n="e" s="T156">Mat </ts>
               <ts e="T158" id="Seg_980" n="e" s="T157">narapuːlʼäm </ts>
               <ts e="T159" id="Seg_982" n="e" s="T158">orkɨsam. </ts>
               <ts e="T160" id="Seg_984" n="e" s="T159">Nɨna </ts>
               <ts e="T161" id="Seg_986" n="e" s="T160">pičʼar </ts>
               <ts e="T162" id="Seg_988" n="e" s="T161">olɨ </ts>
               <ts e="T163" id="Seg_990" n="e" s="T162">čʼir </ts>
               <ts e="T164" id="Seg_992" n="e" s="T163">qätsa. </ts>
               <ts e="T165" id="Seg_994" n="e" s="T164">Nɨna </ts>
               <ts e="T166" id="Seg_996" n="e" s="T165">pičʼap </ts>
               <ts e="T167" id="Seg_998" n="e" s="T166">antaqan </ts>
               <ts e="T168" id="Seg_1000" n="e" s="T167">nɨna </ts>
               <ts e="T169" id="Seg_1002" n="e" s="T168">čʼatsam. </ts>
               <ts e="T170" id="Seg_1004" n="e" s="T169">Nɨnɨ </ts>
               <ts e="T171" id="Seg_1006" n="e" s="T170">tiː </ts>
               <ts e="T172" id="Seg_1008" n="e" s="T171">tüːsak“. </ts>
               <ts e="T173" id="Seg_1010" n="e" s="T172">İrra </ts>
               <ts e="T174" id="Seg_1012" n="e" s="T173">laqqɨmɔːtaiːs. </ts>
               <ts e="T175" id="Seg_1014" n="e" s="T174">Nɨnɨ </ts>
               <ts e="T176" id="Seg_1016" n="e" s="T175">irra </ts>
               <ts e="T177" id="Seg_1018" n="e" s="T176">nälʼamtä </ts>
               <ts e="T178" id="Seg_1020" n="e" s="T177">miːmpat </ts>
               <ts e="T179" id="Seg_1022" n="e" s="T178">İčʼikičʼikamik. </ts>
               <ts e="T180" id="Seg_1024" n="e" s="T179">Qumɨt </ts>
               <ts e="T181" id="Seg_1026" n="e" s="T180">nɨnɨ </ts>
               <ts e="T182" id="Seg_1028" n="e" s="T181">qənnɔːtɨt </ts>
               <ts e="T183" id="Seg_1030" n="e" s="T182">imakɔːlɨk. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_1031" s="T0">NST_1965_Tyrshaqo1_flk.001 (001.001)</ta>
            <ta e="T13" id="Seg_1032" s="T3">NST_1965_Tyrshaqo1_flk.002 (001.002)</ta>
            <ta e="T22" id="Seg_1033" s="T13">NST_1965_Tyrshaqo1_flk.003 (001.003)</ta>
            <ta e="T33" id="Seg_1034" s="T22">NST_1965_Tyrshaqo1_flk.004 (001.004)</ta>
            <ta e="T39" id="Seg_1035" s="T33">NST_1965_Tyrshaqo1_flk.005 (001.005)</ta>
            <ta e="T48" id="Seg_1036" s="T39">NST_1965_Tyrshaqo1_flk.006 (001.006)</ta>
            <ta e="T52" id="Seg_1037" s="T48">NST_1965_Tyrshaqo1_flk.007 (001.007)</ta>
            <ta e="T59" id="Seg_1038" s="T52">NST_1965_Tyrshaqo1_flk.008 (001.008)</ta>
            <ta e="T64" id="Seg_1039" s="T59">NST_1965_Tyrshaqo1_flk.009 (001.009)</ta>
            <ta e="T73" id="Seg_1040" s="T64">NST_1965_Tyrshaqo1_flk.010 (001.010)</ta>
            <ta e="T79" id="Seg_1041" s="T73">NST_1965_Tyrshaqo1_flk.011 (001.011)</ta>
            <ta e="T85" id="Seg_1042" s="T79">NST_1965_Tyrshaqo1_flk.012 (001.012)</ta>
            <ta e="T91" id="Seg_1043" s="T85">NST_1965_Tyrshaqo1_flk.013 (001.013)</ta>
            <ta e="T94" id="Seg_1044" s="T91">NST_1965_Tyrshaqo1_flk.014 (001.014)</ta>
            <ta e="T98" id="Seg_1045" s="T94">NST_1965_Tyrshaqo1_flk.015 (001.015)</ta>
            <ta e="T103" id="Seg_1046" s="T98">NST_1965_Tyrshaqo1_flk.016 (001.016)</ta>
            <ta e="T107" id="Seg_1047" s="T103">NST_1965_Tyrshaqo1_flk.017 (001.017)</ta>
            <ta e="T114" id="Seg_1048" s="T107">NST_1965_Tyrshaqo1_flk.018 (001.018)</ta>
            <ta e="T116" id="Seg_1049" s="T114">NST_1965_Tyrshaqo1_flk.019 (001.019)</ta>
            <ta e="T122" id="Seg_1050" s="T116">NST_1965_Tyrshaqo1_flk.020 (001.020)</ta>
            <ta e="T132" id="Seg_1051" s="T122">NST_1965_Tyrshaqo1_flk.021 (001.021)</ta>
            <ta e="T141" id="Seg_1052" s="T132">NST_1965_Tyrshaqo1_flk.022 (001.022)</ta>
            <ta e="T152" id="Seg_1053" s="T141">NST_1965_Tyrshaqo1_flk.023 (001.023)</ta>
            <ta e="T156" id="Seg_1054" s="T152">NST_1965_Tyrshaqo1_flk.024 (001.024)</ta>
            <ta e="T159" id="Seg_1055" s="T156">NST_1965_Tyrshaqo1_flk.025 (001.025)</ta>
            <ta e="T164" id="Seg_1056" s="T159">NST_1965_Tyrshaqo1_flk.026 (001.026)</ta>
            <ta e="T169" id="Seg_1057" s="T164">NST_1965_Tyrshaqo1_flk.027 (001.027)</ta>
            <ta e="T172" id="Seg_1058" s="T169">NST_1965_Tyrshaqo1_flk.028 (001.028)</ta>
            <ta e="T174" id="Seg_1059" s="T172">NST_1965_Tyrshaqo1_flk.029 (001.029)</ta>
            <ta e="T179" id="Seg_1060" s="T174">NST_1965_Tyrshaqo1_flk.030 (001.030)</ta>
            <ta e="T183" id="Seg_1061" s="T179">NST_1965_Tyrshaqo1_flk.031 (001.031)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_1062" s="T0">′ичик ичи′ка ′иlымпа им′лʼантыса.</ta>
            <ta e="T13" id="Seg_1063" s="T3">пун ′ичик ичи′ка ′ниlчик ′kӓтыныт имлӓɣынты. мат ′kъ̊̄нтак ′тыршʼа̄′kо̄ и′ран ′мотты.</ta>
            <ta e="T22" id="Seg_1064" s="T13">′имили ми′лʼӓ ′ниlчик ′kӓттынит: ′ичик ичи′канык ку′тар ′ӣтаl ′kумытыт ′нӓlеw. </ta>
            <ta e="T33" id="Seg_1065" s="T22">а ′ичика ниl′чик ′kӓттыңыт мано′lа ′о̨lыка ′маннымен′там кут′тар ′kумат има′нʼӧтыт.</ta>
            <ta e="T39" id="Seg_1066" s="T33">′ичик ичи′ка kаррӓkӓнӓ ′козыl анныты ′лʼӣбы‵лʼа́.</ta>
            <ta e="T48" id="Seg_1067" s="T39">′kарра ′нукаlныт и нины ′пиннытʼа наррӓпылʼат kа′рромта ′антаkынты.</ta>
            <ta e="T52" id="Seg_1068" s="T48">ныны ′kӓнны[ӓ] тыр′са kо ′ӣранык.</ta>
            <ta e="T59" id="Seg_1069" s="T52">у′курчонтот тот ′kыкыт ′перkымты на ′шуртуkылныт.</ta>
            <ta e="T64" id="Seg_1070" s="T59">тот ′kыɣыт ′пӱllака kонтыт.</ta>
            <ta e="T73" id="Seg_1071" s="T64">на ′пӱllаканты онтанʼ укурчонтот ′пича ны са′та‵иныт.</ta>
            <ta e="T79" id="Seg_1072" s="T73">нара пу′лʼамты ′оркылʼа ′пичаф ′о̨lычер ′kӓттымит.</ta>
            <ta e="T85" id="Seg_1073" s="T79">нины ′пичамты ан′таɣынт ′чаттымыт, ′антаkынтӓ ′тиңа.</ta>
            <ta e="T91" id="Seg_1074" s="T85">′нына ′kӓнна ′тиңа тыр′ща kо ′ират ′мотты.</ta>
            <ta e="T94" id="Seg_1075" s="T91">′шʼитанты тоттынта.</ta>
            <ta e="T98" id="Seg_1076" s="T94">′тӱңӓ ′анымты ′kонна ′нʼӓkыlнытӓ.</ta>
            <ta e="T103" id="Seg_1077" s="T98">′kонна′танта, ′мо̄тъ чап ′шʼерна.</ta>
            <ta e="T107" id="Seg_1078" s="T103">′мо̄тkыт ′тӓ̄тти kум ′омта.</ta>
            <ta e="T114" id="Seg_1079" s="T107">пӯт ′тырща ′kо̄ира ′ниlчик ′kӓттыңыты ′ичик ичи′ка, lум′тӓ̄сик.</ta>
            <ta e="T116" id="Seg_1080" s="T114">′kумыт ‵има′нʼӧтыт.</ta>
            <ta e="T122" id="Seg_1081" s="T116">′ираkотат ай ′има‵k̂ота ′онтоkий ′нымтӓ ъ̊̄тыколʼ.</ta>
            <ta e="T132" id="Seg_1082" s="T122">пут ′ирра ′ниlчик kӓттыңыт ′кутай ′шим ′пӣсалтӓнтӓ ′наммынык ′нелʼам ′минынтам.</ta>
            <ta e="T141" id="Seg_1083" s="T132">′ичик ичи′ка ниlчик ′kӓтыңыт мат тот kыкыт ′пе̨рkам ны шу′рроkулсыт.</ta>
            <ta e="T152" id="Seg_1084" s="T141">′нынӓ мат ′kоса ′пӱllака на ′пуllаканты kонна пактыса ′тӱтkыlсам.</ta>
            <ta e="T156" id="Seg_1085" s="T152">′пича ны ш′им са′таиса.</ta>
            <ta e="T159" id="Seg_1086" s="T156">мат нарапӯлʼям ′оркысам.</ta>
            <ta e="T164" id="Seg_1087" s="T159">нына ′пичар оlычир kӓтса.</ta>
            <ta e="T169" id="Seg_1088" s="T164">нына ′пичаф ′антаkан нына чат′сам.</ta>
            <ta e="T172" id="Seg_1089" s="T169">ныны тӣ ′тӱ̄сак.</ta>
            <ta e="T174" id="Seg_1090" s="T172">′ирра ′lакы′мотаӣс.</ta>
            <ta e="T179" id="Seg_1091" s="T174">ныны ′ирра ′нӓlамтӓ мӣмпат ′ичикичи′камик.</ta>
            <ta e="T183" id="Seg_1092" s="T179">′kумыт ныны kӓ′ннотыт ′имаkолык.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_1093" s="T0">İčʼik İčʼika ilʼɨmpa imlʼantɨsa.</ta>
            <ta e="T13" id="Seg_1094" s="T3"> ‎‎pun ičʼik ičʼika nilʼčʼik qätɨnɨt imläqɨntɨ. mat qəːntak tɨršʼaːqoː iran mottɨ.</ta>
            <ta e="T22" id="Seg_1095" s="T13">imili milʼä nilʼčʼik qättɨnit: ičʼik ičʼikanɨk kutar iːtalʼ qumɨtɨt nälʼew.</ta>
            <ta e="T33" id="Seg_1096" s="T22">‎‎a ičʼika nilʼčʼik qättɨŋɨt manolʼa olʼɨka mannɨmentam kuttar qumat imanʼötɨt.</ta>
            <ta e="T39" id="Seg_1097" s="T33">ičʼik ičʼika qarräqänä kozɨlʼ annɨtɨ lʼiːpɨlʼá</ta>
            <ta e="T48" id="Seg_1098" s="T39">qarra nukalʼnɨt i ninɨ pinnɨtʼa narräpɨlʼat qarromta antaqɨntɨ.</ta>
            <ta e="T52" id="Seg_1099" s="T48">nɨnɨ qännɨ [ä] tɨrsa qo iːranɨk.</ta>
            <ta e="T59" id="Seg_1100" s="T52">ukurčʼontot tot qɨkɨt perqɨmtɨ na šurtuqɨlnɨt.</ta>
            <ta e="T64" id="Seg_1101" s="T59">tot qɨqɨt pülʼlʼaka qontɨt.</ta>
            <ta e="T73" id="Seg_1102" s="T64">na pülʼlʼakantɨ ontanʼ ukurčʼontot pičʼa nɨ satainɨt.</ta>
            <ta e="T79" id="Seg_1103" s="T73"> ‎‎nara pulʼamtɨ orkɨlʼa pičʼaf olʼɨčʼer qättɨmit.</ta>
            <ta e="T85" id="Seg_1104" s="T79">ninɨ pičʼamtɨ antaqɨnt čʼattɨmɨt, antaqɨntä tiŋa.</ta>
            <ta e="T91" id="Seg_1105" s="T85">nɨna qänna tiŋa tɨrщa qo irat mottɨ.</ta>
            <ta e="T94" id="Seg_1106" s="T91"> ‎‎šʼitantɨ tottɨnta.</ta>
            <ta e="T98" id="Seg_1107" s="T94">tüŋä anɨmtɨ qonna nʼäqɨlʼnɨtä.</ta>
            <ta e="T103" id="Seg_1108" s="T98">qonnatanta, moːtə čʼap šerna.</ta>
            <ta e="T107" id="Seg_1109" s="T103"> ‎‎moːtqɨt täːtti qum omta.</ta>
            <ta e="T114" id="Seg_1110" s="T107">puːt tɨrщa qoːira nilʼčʼik qättɨŋɨtɨ ičʼik ičʼika, lʼumtäːsik.</ta>
            <ta e="T116" id="Seg_1111" s="T114">qumɨt imanʼötɨt.</ta>
            <ta e="T122" id="Seg_1112" s="T116">iraqotat aj imaq̂ota ontoqij nɨmtä əːtɨkolʼ.</ta>
            <ta e="T132" id="Seg_1113" s="T122">put irra nilʼčʼik qättɨŋɨt kutaj šim piːsaltäntä nammɨnɨk nelʼam minɨntam.</ta>
            <ta e="T141" id="Seg_1114" s="T132">ičʼik ičʼika nilʼčʼik qätɨŋɨt mat tot qɨkɨt perqam nɨ šurroqulsɨt.</ta>
            <ta e="T152" id="Seg_1115" s="T141">nɨnä mat qosa pülʼlʼaka na pulʼlʼakantɨ qonna paktɨsa tütqɨlʼsam.</ta>
            <ta e="T156" id="Seg_1116" s="T152">pičʼa nɨ šim sataisa.</ta>
            <ta e="T159" id="Seg_1117" s="T156">mat narapuːlʼяm orkɨsam.</ta>
            <ta e="T164" id="Seg_1118" s="T159">nɨna pičʼar olʼɨčʼir qätsa. </ta>
            <ta e="T169" id="Seg_1119" s="T164">nɨna pičʼaf antaqan nɨna čʼatsam.</ta>
            <ta e="T172" id="Seg_1120" s="T169">nɨnɨ tiː tüːsak. </ta>
            <ta e="T174" id="Seg_1121" s="T172">irra lʼakɨmotaiːs.</ta>
            <ta e="T179" id="Seg_1122" s="T174">nɨnɨ irra nälʼamtä miːmpat ičʼikičʼikamik.</ta>
            <ta e="T183" id="Seg_1123" s="T179">qumɨt nɨnɨ qännotɨt imaqolɨk.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_1124" s="T0">İčʼikičʼika ilɨmpa imlʼantɨsa. </ta>
            <ta e="T13" id="Seg_1125" s="T3">Puːn İčʼikičʼika nılʼčʼɨk qätɨnɨt imlʼaqɨntɨ:“ Mat qəntak Tɨršaːqoː iran mɔːttɨ”. </ta>
            <ta e="T22" id="Seg_1126" s="T13">Imɨlʼ imilʼä nılʼčʼɨk kətɨnit İčʼikičʼikanɨk:“ Kuttar iːtal qumɨtɨt nälʼew?” </ta>
            <ta e="T33" id="Seg_1127" s="T22">A İčʼika nılʼčʼɨk kättɨŋɨt:“ Man olʼa olʼɨka mannɨmmɛntam, kuttar qumat imanʼnʼɔːtɨt”. </ta>
            <ta e="T39" id="Seg_1128" s="T33">İčʼikičʼika karrä qännä qɔːsɨlʼ annɨtɨ lʼiːpɨlʼa. </ta>
            <ta e="T48" id="Seg_1129" s="T39">Karra nukalnɨt i ninɨ pinɨčʼčʼa narräpɨlʼat qarr omta antaqɨntɨ. </ta>
            <ta e="T52" id="Seg_1130" s="T48">Nɨnɨ qännɨ Tɨrsaqo iːranɨk. </ta>
            <ta e="T59" id="Seg_1131" s="T52">Ukur čʼontoːt toːt kɨqɨt pɛrqɨmtɨ na šurtuqɨlnɨt. </ta>
            <ta e="T64" id="Seg_1132" s="T59">Toːt kɨqɨt pülʼ laka qontɨt. </ta>
            <ta e="T73" id="Seg_1133" s="T64">Na pülʼ lakantɨ omtan ukur čʼontoːt pičʼa nɨ satainɨt. </ta>
            <ta e="T79" id="Seg_1134" s="T73">Narapulʼamtɨ orqɨlʼlʼä pičʼap olɨ čʼer qättɨmit. </ta>
            <ta e="T85" id="Seg_1135" s="T79">Ninɨ pičʼamtɨ antɨqɨnt čʼattɨmɨt, antaqɨntä tıŋa. </ta>
            <ta e="T91" id="Seg_1136" s="T85">Nɨna qänna tiŋa Tɨršaːqoː irat mottɨ. </ta>
            <ta e="T94" id="Seg_1137" s="T91">Šittɨ antɨ tottɨnta. </ta>
            <ta e="T98" id="Seg_1138" s="T94">Tüŋä anɨmtɨ konna nʼäqɨlnɨtä. </ta>
            <ta e="T103" id="Seg_1139" s="T98">Qonna tanta, mɔːttɨ čʼap šerna. </ta>
            <ta e="T107" id="Seg_1140" s="T103">Moːtqɨt täːtti qum omta. </ta>
            <ta e="T114" id="Seg_1141" s="T107">Puːt Tɨršaːqoː ira nilʼčʼik kättɨŋɨtɨ:“ İčʼikičʼika, ıl omtäsik”. </ta>
            <ta e="T116" id="Seg_1142" s="T114">Qumɨt imanʼnʼɔːtɨt. </ta>
            <ta e="T122" id="Seg_1143" s="T116">Iraqotat aj imaqota ontɔːqij nɨmtä əːtɨkɔːlʼ. </ta>
            <ta e="T132" id="Seg_1144" s="T122">Put irra nilʼčʼik kättɨŋɨt:“ Kutaj šım pisaltɛnta, nammɨnɨk nälʼam minɨntam“. </ta>
            <ta e="T141" id="Seg_1145" s="T132">İčʼikičʼika nilʼčʼik kätɨŋɨt:“ Mat toːt kɨqɨt pɛrqam nɨ šurroqulsɨt. </ta>
            <ta e="T152" id="Seg_1146" s="T141">Nɨnä mat qosa pülʼ laka na pülʼ lakantɨ konna paktɨsa tütqɨlʼsam. </ta>
            <ta e="T156" id="Seg_1147" s="T152">Pičʼa nɨ šım sattɛːsa. </ta>
            <ta e="T159" id="Seg_1148" s="T156">Mat narapuːlʼäm orkɨsam. </ta>
            <ta e="T164" id="Seg_1149" s="T159">Nɨna pičʼar olɨ čʼir qätsa. </ta>
            <ta e="T169" id="Seg_1150" s="T164">Nɨna pičʼap antaqan nɨna čʼatsam. </ta>
            <ta e="T172" id="Seg_1151" s="T169">Nɨnɨ tiː tüːsak“. </ta>
            <ta e="T174" id="Seg_1152" s="T172">İrra laqqɨmɔːtaiːs. </ta>
            <ta e="T179" id="Seg_1153" s="T174">Nɨnɨ irra nälʼamtä miːmpat İčʼikičʼikamik. </ta>
            <ta e="T183" id="Seg_1154" s="T179">Qumɨt nɨnɨ qənnɔːtɨt imakɔːlɨk. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1155" s="T0">İčʼikičʼika</ta>
            <ta e="T2" id="Seg_1156" s="T1">ilɨ-mpa</ta>
            <ta e="T3" id="Seg_1157" s="T2">imlʼa-ntɨ-sa</ta>
            <ta e="T4" id="Seg_1158" s="T3">puːn</ta>
            <ta e="T5" id="Seg_1159" s="T4">İčʼikičʼika</ta>
            <ta e="T6" id="Seg_1160" s="T5">nılʼčʼɨ-k</ta>
            <ta e="T7" id="Seg_1161" s="T6">qätɨ-nɨ-t</ta>
            <ta e="T8" id="Seg_1162" s="T7">imlʼa-qɨn-tɨ</ta>
            <ta e="T9" id="Seg_1163" s="T8">mat</ta>
            <ta e="T10" id="Seg_1164" s="T9">qən-ta-k</ta>
            <ta e="T11" id="Seg_1165" s="T10">Tɨršaːqoː</ta>
            <ta e="T12" id="Seg_1166" s="T11">ira-n</ta>
            <ta e="T13" id="Seg_1167" s="T12">mɔːt-tɨ</ta>
            <ta e="T14" id="Seg_1168" s="T13">imɨ-lʼ</ta>
            <ta e="T15" id="Seg_1169" s="T14">imilʼä</ta>
            <ta e="T16" id="Seg_1170" s="T15">nılʼčʼɨ-k</ta>
            <ta e="T17" id="Seg_1171" s="T16">kətɨ-ni-t</ta>
            <ta e="T18" id="Seg_1172" s="T17">Ičʼikičʼika-nɨk</ta>
            <ta e="T19" id="Seg_1173" s="T18">kuttar</ta>
            <ta e="T20" id="Seg_1174" s="T19">iː-ta-l</ta>
            <ta e="T21" id="Seg_1175" s="T20">qum-ɨ-t-ɨ-t</ta>
            <ta e="T22" id="Seg_1176" s="T21">nälʼe-w</ta>
            <ta e="T23" id="Seg_1177" s="T22">a</ta>
            <ta e="T24" id="Seg_1178" s="T23">Ičʼika</ta>
            <ta e="T25" id="Seg_1179" s="T24">nılʼčʼɨ-k</ta>
            <ta e="T26" id="Seg_1180" s="T25">kättɨ-ŋɨ-t</ta>
            <ta e="T27" id="Seg_1181" s="T26">man</ta>
            <ta e="T28" id="Seg_1182" s="T27">olʼa</ta>
            <ta e="T29" id="Seg_1183" s="T28">olʼɨka</ta>
            <ta e="T30" id="Seg_1184" s="T29">mannɨ-mm-ɛnta-m</ta>
            <ta e="T31" id="Seg_1185" s="T30">kuttar</ta>
            <ta e="T32" id="Seg_1186" s="T31">qum-a-t</ta>
            <ta e="T33" id="Seg_1187" s="T32">ima-nʼ-nʼɔː-tɨt</ta>
            <ta e="T34" id="Seg_1188" s="T33">İčʼikičʼika</ta>
            <ta e="T35" id="Seg_1189" s="T34">karrä</ta>
            <ta e="T36" id="Seg_1190" s="T35">qän-nä</ta>
            <ta e="T37" id="Seg_1191" s="T36">qɔːsɨ-lʼ</ta>
            <ta e="T38" id="Seg_1192" s="T37">annɨ-tɨ</ta>
            <ta e="T39" id="Seg_1193" s="T38">lʼiːpɨ-lʼa</ta>
            <ta e="T40" id="Seg_1194" s="T39">karra</ta>
            <ta e="T41" id="Seg_1195" s="T40">nuka-l-nɨ-t</ta>
            <ta e="T42" id="Seg_1196" s="T41">i</ta>
            <ta e="T43" id="Seg_1197" s="T42">ninɨ</ta>
            <ta e="T44" id="Seg_1198" s="T43">pin-ɨ-čʼ-čʼa</ta>
            <ta e="T45" id="Seg_1199" s="T44">narräpɨ-lʼa-t</ta>
            <ta e="T46" id="Seg_1200" s="T45">qarr</ta>
            <ta e="T47" id="Seg_1201" s="T46">omta</ta>
            <ta e="T48" id="Seg_1202" s="T47">anta-qɨn-tɨ</ta>
            <ta e="T49" id="Seg_1203" s="T48">nɨnɨ</ta>
            <ta e="T50" id="Seg_1204" s="T49">qän-nɨ</ta>
            <ta e="T51" id="Seg_1205" s="T50">Tɨrsaqo</ta>
            <ta e="T52" id="Seg_1206" s="T51">iːra-nɨk</ta>
            <ta e="T53" id="Seg_1207" s="T52">ukur</ta>
            <ta e="T54" id="Seg_1208" s="T53">čʼontoː-t</ta>
            <ta e="T55" id="Seg_1209" s="T54">toː-t</ta>
            <ta e="T56" id="Seg_1210" s="T55">kɨ-qɨt</ta>
            <ta e="T57" id="Seg_1211" s="T56">pɛrqɨ-m-tɨ</ta>
            <ta e="T58" id="Seg_1212" s="T57">na</ta>
            <ta e="T59" id="Seg_1213" s="T58">šur-tu-qɨl-nɨ-t</ta>
            <ta e="T60" id="Seg_1214" s="T59">toː-t</ta>
            <ta e="T61" id="Seg_1215" s="T60">kɨ-qɨt</ta>
            <ta e="T62" id="Seg_1216" s="T61">pü-lʼ</ta>
            <ta e="T63" id="Seg_1217" s="T62">laka</ta>
            <ta e="T64" id="Seg_1218" s="T63">qo-ntɨ-t</ta>
            <ta e="T65" id="Seg_1219" s="T64">na</ta>
            <ta e="T66" id="Seg_1220" s="T65">pü-lʼ</ta>
            <ta e="T67" id="Seg_1221" s="T66">laka-ntɨ</ta>
            <ta e="T68" id="Seg_1222" s="T67">omta-n</ta>
            <ta e="T69" id="Seg_1223" s="T68">ukur</ta>
            <ta e="T70" id="Seg_1224" s="T69">čʼontoː-t</ta>
            <ta e="T71" id="Seg_1225" s="T70">pičʼa</ta>
            <ta e="T72" id="Seg_1226" s="T71">nɨ</ta>
            <ta e="T73" id="Seg_1227" s="T72">sat-ai-nɨ-t</ta>
            <ta e="T74" id="Seg_1228" s="T73">narapu-lʼa-m-tɨ</ta>
            <ta e="T75" id="Seg_1229" s="T74">orqɨlʼ-lʼä</ta>
            <ta e="T76" id="Seg_1230" s="T75">pičʼa-p</ta>
            <ta e="T77" id="Seg_1231" s="T76">olɨ</ta>
            <ta e="T78" id="Seg_1232" s="T77">čʼer</ta>
            <ta e="T79" id="Seg_1233" s="T78">qättɨ-mi-t</ta>
            <ta e="T80" id="Seg_1234" s="T79">ninɨ</ta>
            <ta e="T81" id="Seg_1235" s="T80">pičʼa-m-tɨ</ta>
            <ta e="T82" id="Seg_1236" s="T81">antɨ-qɨn-t</ta>
            <ta e="T83" id="Seg_1237" s="T82">čʼattɨ-mɨ-t</ta>
            <ta e="T84" id="Seg_1238" s="T83">anta-qɨn-tä</ta>
            <ta e="T85" id="Seg_1239" s="T84">tı-ŋa</ta>
            <ta e="T86" id="Seg_1240" s="T85">nɨna</ta>
            <ta e="T87" id="Seg_1241" s="T86">qän-na</ta>
            <ta e="T88" id="Seg_1242" s="T87">ti-ŋa</ta>
            <ta e="T89" id="Seg_1243" s="T88">Tɨršaːqoː</ta>
            <ta e="T90" id="Seg_1244" s="T89">ira-t</ta>
            <ta e="T91" id="Seg_1245" s="T90">mot-tɨ</ta>
            <ta e="T92" id="Seg_1246" s="T91">šittɨ</ta>
            <ta e="T93" id="Seg_1247" s="T92">antɨ</ta>
            <ta e="T94" id="Seg_1248" s="T93">tottɨ-nta</ta>
            <ta e="T95" id="Seg_1249" s="T94">tü-ŋä</ta>
            <ta e="T96" id="Seg_1250" s="T95">anɨ-m-tɨ</ta>
            <ta e="T97" id="Seg_1251" s="T96">konna</ta>
            <ta e="T98" id="Seg_1252" s="T97">nʼäqɨ-l-nɨ-tä</ta>
            <ta e="T99" id="Seg_1253" s="T98">qon-na</ta>
            <ta e="T100" id="Seg_1254" s="T99">tanta</ta>
            <ta e="T101" id="Seg_1255" s="T100">mɔːt-tɨ</ta>
            <ta e="T102" id="Seg_1256" s="T101">čʼap</ta>
            <ta e="T103" id="Seg_1257" s="T102">šer-na</ta>
            <ta e="T104" id="Seg_1258" s="T103">moːt-qɨt</ta>
            <ta e="T105" id="Seg_1259" s="T104">täːtti</ta>
            <ta e="T106" id="Seg_1260" s="T105">qum</ta>
            <ta e="T107" id="Seg_1261" s="T106">omta</ta>
            <ta e="T108" id="Seg_1262" s="T107">puːt</ta>
            <ta e="T109" id="Seg_1263" s="T108">Tɨršaːqoː</ta>
            <ta e="T110" id="Seg_1264" s="T109">ira</ta>
            <ta e="T111" id="Seg_1265" s="T110">nilʼčʼi-k</ta>
            <ta e="T112" id="Seg_1266" s="T111">kättɨ-ŋɨ-tɨ</ta>
            <ta e="T113" id="Seg_1267" s="T112">İčʼikičʼika</ta>
            <ta e="T184" id="Seg_1268" s="T113">ıl </ta>
            <ta e="T114" id="Seg_1269" s="T184">omt-äsik</ta>
            <ta e="T115" id="Seg_1270" s="T114">qum-ɨ-t</ta>
            <ta e="T116" id="Seg_1271" s="T115">ima-nʼ-nʼɔː-tɨt</ta>
            <ta e="T117" id="Seg_1272" s="T116">iraqota-t</ta>
            <ta e="T118" id="Seg_1273" s="T117">aj</ta>
            <ta e="T119" id="Seg_1274" s="T118">imaqota</ta>
            <ta e="T120" id="Seg_1275" s="T119">ontɔː-qij</ta>
            <ta e="T121" id="Seg_1276" s="T120">nɨmtä</ta>
            <ta e="T122" id="Seg_1277" s="T121">əːtɨ-kɔːlʼ</ta>
            <ta e="T123" id="Seg_1278" s="T122">put</ta>
            <ta e="T124" id="Seg_1279" s="T123">irra</ta>
            <ta e="T125" id="Seg_1280" s="T124">nilʼčʼi-k</ta>
            <ta e="T126" id="Seg_1281" s="T125">kättɨ-ŋɨ-t</ta>
            <ta e="T127" id="Seg_1282" s="T126">kutaj</ta>
            <ta e="T128" id="Seg_1283" s="T127">šım</ta>
            <ta e="T129" id="Seg_1284" s="T128">pis-alt-ɛnta</ta>
            <ta e="T130" id="Seg_1285" s="T129">nammɨ-nɨk</ta>
            <ta e="T131" id="Seg_1286" s="T130">nälʼa-m</ta>
            <ta e="T132" id="Seg_1287" s="T131">mi-nɨ-nta-m</ta>
            <ta e="T133" id="Seg_1288" s="T132">İčʼikičʼika</ta>
            <ta e="T134" id="Seg_1289" s="T133">nilʼčʼi-k</ta>
            <ta e="T135" id="Seg_1290" s="T134">kätɨ-ŋɨ-t</ta>
            <ta e="T136" id="Seg_1291" s="T135">Mat</ta>
            <ta e="T137" id="Seg_1292" s="T136">toː-t</ta>
            <ta e="T138" id="Seg_1293" s="T137">kɨ-qɨt</ta>
            <ta e="T139" id="Seg_1294" s="T138">pɛrqa-m</ta>
            <ta e="T140" id="Seg_1295" s="T139">nɨ</ta>
            <ta e="T141" id="Seg_1296" s="T140">šurro-qul-sɨ-t</ta>
            <ta e="T142" id="Seg_1297" s="T141">nɨnä</ta>
            <ta e="T143" id="Seg_1298" s="T142">mat</ta>
            <ta e="T144" id="Seg_1299" s="T143">qo-sa</ta>
            <ta e="T145" id="Seg_1300" s="T144">pü-lʼ</ta>
            <ta e="T146" id="Seg_1301" s="T145">laka</ta>
            <ta e="T147" id="Seg_1302" s="T146">na</ta>
            <ta e="T148" id="Seg_1303" s="T147">pü-lʼ</ta>
            <ta e="T149" id="Seg_1304" s="T148">laka-ntɨ</ta>
            <ta e="T150" id="Seg_1305" s="T149">konna</ta>
            <ta e="T151" id="Seg_1306" s="T150">paktɨ-sa</ta>
            <ta e="T152" id="Seg_1307" s="T151">tüt-qɨlʼ-sa-m</ta>
            <ta e="T153" id="Seg_1308" s="T152">pičʼa</ta>
            <ta e="T154" id="Seg_1309" s="T153">nɨ</ta>
            <ta e="T155" id="Seg_1310" s="T154">šım</ta>
            <ta e="T156" id="Seg_1311" s="T155">satt-ɛː-sa</ta>
            <ta e="T157" id="Seg_1312" s="T156">mat</ta>
            <ta e="T158" id="Seg_1313" s="T157">narapuː-lʼä-m</ta>
            <ta e="T159" id="Seg_1314" s="T158">orkɨ-sa-m</ta>
            <ta e="T160" id="Seg_1315" s="T159">nɨna</ta>
            <ta e="T161" id="Seg_1316" s="T160">pičʼar</ta>
            <ta e="T162" id="Seg_1317" s="T161">olɨ</ta>
            <ta e="T163" id="Seg_1318" s="T162">čʼir</ta>
            <ta e="T164" id="Seg_1319" s="T163">qät-sa</ta>
            <ta e="T165" id="Seg_1320" s="T164">nɨna</ta>
            <ta e="T166" id="Seg_1321" s="T165">pičʼa-p</ta>
            <ta e="T167" id="Seg_1322" s="T166">anta-qan</ta>
            <ta e="T168" id="Seg_1323" s="T167">nɨna</ta>
            <ta e="T169" id="Seg_1324" s="T168">čʼat-sa-m</ta>
            <ta e="T170" id="Seg_1325" s="T169">nɨnɨ</ta>
            <ta e="T171" id="Seg_1326" s="T170">tiː</ta>
            <ta e="T172" id="Seg_1327" s="T171">tüː-sa-k</ta>
            <ta e="T173" id="Seg_1328" s="T172">i̇rra</ta>
            <ta e="T174" id="Seg_1329" s="T173">laqqɨ-mɔːt-aiː-s</ta>
            <ta e="T175" id="Seg_1330" s="T174">nɨnɨ</ta>
            <ta e="T176" id="Seg_1331" s="T175">irra</ta>
            <ta e="T177" id="Seg_1332" s="T176">nälʼa-m-tä</ta>
            <ta e="T178" id="Seg_1333" s="T177">miː-mpa-t</ta>
            <ta e="T179" id="Seg_1334" s="T178">İčʼikičʼika-mik</ta>
            <ta e="T180" id="Seg_1335" s="T179">qum-ɨ-t</ta>
            <ta e="T181" id="Seg_1336" s="T180">nɨnɨ</ta>
            <ta e="T182" id="Seg_1337" s="T181">qən-nɔː-tɨt</ta>
            <ta e="T183" id="Seg_1338" s="T182">ima-kɔːlɨ-k</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1339" s="T0">Ičʼakɨčʼɨka</ta>
            <ta e="T2" id="Seg_1340" s="T1">ilɨ-mpɨ</ta>
            <ta e="T3" id="Seg_1341" s="T2">imɨlʼa-ntɨ-sä</ta>
            <ta e="T4" id="Seg_1342" s="T3">puːn</ta>
            <ta e="T5" id="Seg_1343" s="T4">Ičʼakɨčʼɨka</ta>
            <ta e="T6" id="Seg_1344" s="T5">nılʼčʼɨ-k</ta>
            <ta e="T7" id="Seg_1345" s="T6">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T8" id="Seg_1346" s="T7">imɨlʼa-qɨn-ntɨ</ta>
            <ta e="T9" id="Seg_1347" s="T8">man</ta>
            <ta e="T10" id="Seg_1348" s="T9">qən-ɛntɨ-k</ta>
            <ta e="T11" id="Seg_1349" s="T10">Tɨršaqoː</ta>
            <ta e="T12" id="Seg_1350" s="T11">ira-n</ta>
            <ta e="T13" id="Seg_1351" s="T12">mɔːt-ntɨ</ta>
            <ta e="T14" id="Seg_1352" s="T13">ima-lʼ</ta>
            <ta e="T15" id="Seg_1353" s="T14">imɨlʼa</ta>
            <ta e="T16" id="Seg_1354" s="T15">nılʼčʼɨ-k</ta>
            <ta e="T17" id="Seg_1355" s="T16">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T18" id="Seg_1356" s="T17">Ičʼakɨčʼɨka-nɨŋ</ta>
            <ta e="T19" id="Seg_1357" s="T18">kuttar</ta>
            <ta e="T20" id="Seg_1358" s="T19">iː-ɛntɨ-l</ta>
            <ta e="T21" id="Seg_1359" s="T20">qum-ɨ-t-ɨ-n</ta>
            <ta e="T22" id="Seg_1360" s="T21">nälʼa-m</ta>
            <ta e="T23" id="Seg_1361" s="T22">a</ta>
            <ta e="T24" id="Seg_1362" s="T23">Ičʼakɨčʼɨka</ta>
            <ta e="T25" id="Seg_1363" s="T24">nılʼčʼɨ-k</ta>
            <ta e="T26" id="Seg_1364" s="T25">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T27" id="Seg_1365" s="T26">man</ta>
            <ta e="T28" id="Seg_1366" s="T27">olqa</ta>
            <ta e="T29" id="Seg_1367" s="T28">olqa</ta>
            <ta e="T30" id="Seg_1368" s="T29">mantɨ-mpɨ-ɛntɨ-m</ta>
            <ta e="T31" id="Seg_1369" s="T30">kuttar</ta>
            <ta e="T32" id="Seg_1370" s="T31">qum-ɨ-t</ta>
            <ta e="T33" id="Seg_1371" s="T32">ima-š-ŋɨ-tɨt</ta>
            <ta e="T34" id="Seg_1372" s="T33">Ičʼakɨčʼɨka</ta>
            <ta e="T35" id="Seg_1373" s="T34">karrä</ta>
            <ta e="T36" id="Seg_1374" s="T35">qən-ŋɨ</ta>
            <ta e="T37" id="Seg_1375" s="T36">qɔːsɨ-lʼ</ta>
            <ta e="T38" id="Seg_1376" s="T37">antɨ-ntɨ</ta>
            <ta e="T39" id="Seg_1377" s="T38">lıːpɨ-lʼa</ta>
            <ta e="T40" id="Seg_1378" s="T39">karrä</ta>
            <ta e="T41" id="Seg_1379" s="T40">noqqo-ɔːl-ŋɨ-tɨ</ta>
            <ta e="T42" id="Seg_1380" s="T41">i</ta>
            <ta e="T43" id="Seg_1381" s="T42">nɨːnɨ</ta>
            <ta e="T44" id="Seg_1382" s="T43">pin-ɨ-š-ntɨ</ta>
            <ta e="T45" id="Seg_1383" s="T44">narapo-lʼa-n</ta>
            <ta e="T46" id="Seg_1384" s="T45">karrä</ta>
            <ta e="T47" id="Seg_1385" s="T46">omtɨ</ta>
            <ta e="T48" id="Seg_1386" s="T47">antɨ-qɨn-ntɨ</ta>
            <ta e="T49" id="Seg_1387" s="T48">nɨːnɨ</ta>
            <ta e="T50" id="Seg_1388" s="T49">qən-ŋɨ</ta>
            <ta e="T51" id="Seg_1389" s="T50">Tɨršaqoː</ta>
            <ta e="T52" id="Seg_1390" s="T51">ira-nɨŋ</ta>
            <ta e="T53" id="Seg_1391" s="T52">ukkɨr</ta>
            <ta e="T54" id="Seg_1392" s="T53">čʼontɨ-n</ta>
            <ta e="T55" id="Seg_1393" s="T54">toː-n</ta>
            <ta e="T56" id="Seg_1394" s="T55">kɨ-qɨn</ta>
            <ta e="T57" id="Seg_1395" s="T56">pɛrqɨ-m-tɨ</ta>
            <ta e="T58" id="Seg_1396" s="T57">na</ta>
            <ta e="T59" id="Seg_1397" s="T58">šur-tɨ-qɨl-ŋɨ-tɨ</ta>
            <ta e="T60" id="Seg_1398" s="T59">toː-n</ta>
            <ta e="T61" id="Seg_1399" s="T60">kɨ-qɨn</ta>
            <ta e="T62" id="Seg_1400" s="T61">pü-lʼ</ta>
            <ta e="T63" id="Seg_1401" s="T62">laka</ta>
            <ta e="T64" id="Seg_1402" s="T63">qo-ntɨ-tɨ</ta>
            <ta e="T65" id="Seg_1403" s="T64">na</ta>
            <ta e="T66" id="Seg_1404" s="T65">pü-lʼ</ta>
            <ta e="T67" id="Seg_1405" s="T66">laka-ntɨ</ta>
            <ta e="T68" id="Seg_1406" s="T67">omtɨ-n</ta>
            <ta e="T69" id="Seg_1407" s="T68">ukkɨr</ta>
            <ta e="T70" id="Seg_1408" s="T69">čʼontɨ-n</ta>
            <ta e="T71" id="Seg_1409" s="T70">pičʼčʼa</ta>
            <ta e="T72" id="Seg_1410" s="T71">na</ta>
            <ta e="T73" id="Seg_1411" s="T72">satɨ-ɛː-ŋɨ-tɨ</ta>
            <ta e="T74" id="Seg_1412" s="T73">narapo-lʼa-m-tɨ</ta>
            <ta e="T75" id="Seg_1413" s="T74">orqɨl-lä</ta>
            <ta e="T76" id="Seg_1414" s="T75">pičʼčʼa-m</ta>
            <ta e="T77" id="Seg_1415" s="T76">olɨ</ta>
            <ta e="T78" id="Seg_1416" s="T77">čʼiːrɨ</ta>
            <ta e="T79" id="Seg_1417" s="T78">qättɨ-mpɨ-tɨ</ta>
            <ta e="T80" id="Seg_1418" s="T79">nɨːnɨ</ta>
            <ta e="T81" id="Seg_1419" s="T80">pičʼčʼa-m-tɨ</ta>
            <ta e="T82" id="Seg_1420" s="T81">antɨ-qɨn-ntɨ</ta>
            <ta e="T83" id="Seg_1421" s="T82">čʼattɨ-mpɨ-tɨ</ta>
            <ta e="T84" id="Seg_1422" s="T83">antɨ-qɨn-tɨ</ta>
            <ta e="T85" id="Seg_1423" s="T84">tıː-ŋɨ</ta>
            <ta e="T86" id="Seg_1424" s="T85">nɨːnɨ</ta>
            <ta e="T87" id="Seg_1425" s="T86">qən-ŋɨ</ta>
            <ta e="T88" id="Seg_1426" s="T87">tü-ŋɨ</ta>
            <ta e="T89" id="Seg_1427" s="T88">Tɨršaqoː</ta>
            <ta e="T90" id="Seg_1428" s="T89">ira-n</ta>
            <ta e="T91" id="Seg_1429" s="T90">mɔːt-ntɨ</ta>
            <ta e="T92" id="Seg_1430" s="T91">šittɨ</ta>
            <ta e="T93" id="Seg_1431" s="T92">antɨ</ta>
            <ta e="T94" id="Seg_1432" s="T93">tottɨ-ntɨ</ta>
            <ta e="T95" id="Seg_1433" s="T94">tü-ŋɨ</ta>
            <ta e="T96" id="Seg_1434" s="T95">antɨ-m-tɨ</ta>
            <ta e="T97" id="Seg_1435" s="T96">konnä</ta>
            <ta e="T98" id="Seg_1436" s="T97">näkä-ɔːl-ŋɨ-tɨ</ta>
            <ta e="T99" id="Seg_1437" s="T98">qən-ŋɨ</ta>
            <ta e="T100" id="Seg_1438" s="T99">tantɨ</ta>
            <ta e="T101" id="Seg_1439" s="T100">mɔːt-ntɨ</ta>
            <ta e="T102" id="Seg_1440" s="T101">čʼam</ta>
            <ta e="T103" id="Seg_1441" s="T102">šeːr-ŋɨ</ta>
            <ta e="T104" id="Seg_1442" s="T103">mɔːt-qɨn</ta>
            <ta e="T105" id="Seg_1443" s="T104">tɛːttɨ</ta>
            <ta e="T106" id="Seg_1444" s="T105">qum</ta>
            <ta e="T107" id="Seg_1445" s="T106">omtɨ</ta>
            <ta e="T108" id="Seg_1446" s="T107">puːn</ta>
            <ta e="T109" id="Seg_1447" s="T108">Tɨršaqoː</ta>
            <ta e="T110" id="Seg_1448" s="T109">ira</ta>
            <ta e="T111" id="Seg_1449" s="T110">nılʼčʼɨ-k</ta>
            <ta e="T112" id="Seg_1450" s="T111">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T113" id="Seg_1451" s="T112">Ičʼakɨčʼɨka</ta>
            <ta e="T184" id="Seg_1452" s="T113">ıllä</ta>
            <ta e="T114" id="Seg_1453" s="T184">omtɨ-äšɨk</ta>
            <ta e="T115" id="Seg_1454" s="T114">qum-ɨ-t</ta>
            <ta e="T116" id="Seg_1455" s="T115">ima-š-ŋɨ-tɨt</ta>
            <ta e="T117" id="Seg_1456" s="T116">iraqota-t</ta>
            <ta e="T118" id="Seg_1457" s="T117">aj</ta>
            <ta e="T119" id="Seg_1458" s="T118">imaqota</ta>
            <ta e="T120" id="Seg_1459" s="T119">omtɨ-qı</ta>
            <ta e="T121" id="Seg_1460" s="T120">nɨmtɨ</ta>
            <ta e="T122" id="Seg_1461" s="T121">əːtɨ-kɔːlɨ</ta>
            <ta e="T123" id="Seg_1462" s="T122">puːn</ta>
            <ta e="T124" id="Seg_1463" s="T123">ira</ta>
            <ta e="T125" id="Seg_1464" s="T124">nılʼčʼɨ-k</ta>
            <ta e="T126" id="Seg_1465" s="T125">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T127" id="Seg_1466" s="T126">kutɨ</ta>
            <ta e="T128" id="Seg_1467" s="T127">mašım</ta>
            <ta e="T129" id="Seg_1468" s="T128">pisɨ-altɨ-ɛntɨ</ta>
            <ta e="T130" id="Seg_1469" s="T129">na-nɨŋ</ta>
            <ta e="T131" id="Seg_1470" s="T130">nälʼa-m</ta>
            <ta e="T132" id="Seg_1471" s="T131">mi-ntɨ-ɛntɨ-m</ta>
            <ta e="T133" id="Seg_1472" s="T132">Ičʼakɨčʼɨka</ta>
            <ta e="T134" id="Seg_1473" s="T133">nılʼčʼɨ-k</ta>
            <ta e="T135" id="Seg_1474" s="T134">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T136" id="Seg_1475" s="T135">man</ta>
            <ta e="T137" id="Seg_1476" s="T136">toː-n</ta>
            <ta e="T138" id="Seg_1477" s="T137">kɨ-qɨn</ta>
            <ta e="T139" id="Seg_1478" s="T138">pɛrqɨ-mɨ</ta>
            <ta e="T140" id="Seg_1479" s="T139">na</ta>
            <ta e="T141" id="Seg_1480" s="T140">šur-qɨl-sɨ-tɨ</ta>
            <ta e="T142" id="Seg_1481" s="T141">nɨːnɨ</ta>
            <ta e="T143" id="Seg_1482" s="T142">man</ta>
            <ta e="T144" id="Seg_1483" s="T143">qo-sɨ</ta>
            <ta e="T145" id="Seg_1484" s="T144">pü-lʼ</ta>
            <ta e="T146" id="Seg_1485" s="T145">laka</ta>
            <ta e="T147" id="Seg_1486" s="T146">na</ta>
            <ta e="T148" id="Seg_1487" s="T147">pü-lʼ</ta>
            <ta e="T149" id="Seg_1488" s="T148">laka-ntɨ</ta>
            <ta e="T150" id="Seg_1489" s="T149">konnä</ta>
            <ta e="T151" id="Seg_1490" s="T150">paktɨ-sɨ</ta>
            <ta e="T152" id="Seg_1491" s="T151">tüt-qɨl-sɨ-m</ta>
            <ta e="T153" id="Seg_1492" s="T152">pičʼčʼa</ta>
            <ta e="T154" id="Seg_1493" s="T153">na</ta>
            <ta e="T155" id="Seg_1494" s="T154">mašım</ta>
            <ta e="T156" id="Seg_1495" s="T155">satɨ-ɛː-sɨ</ta>
            <ta e="T157" id="Seg_1496" s="T156">man</ta>
            <ta e="T158" id="Seg_1497" s="T157">narapo-lʼa-m</ta>
            <ta e="T159" id="Seg_1498" s="T158">orqɨl-sɨ-m</ta>
            <ta e="T160" id="Seg_1499" s="T159">nɨːnɨ</ta>
            <ta e="T161" id="Seg_1500" s="T160">pičʼčʼa</ta>
            <ta e="T162" id="Seg_1501" s="T161">olɨ</ta>
            <ta e="T163" id="Seg_1502" s="T162">čʼiːrɨ</ta>
            <ta e="T164" id="Seg_1503" s="T163">qättɨ-sɨ</ta>
            <ta e="T165" id="Seg_1504" s="T164">nɨːnɨ</ta>
            <ta e="T166" id="Seg_1505" s="T165">pičʼčʼa-m</ta>
            <ta e="T167" id="Seg_1506" s="T166">antɨ-qɨn</ta>
            <ta e="T168" id="Seg_1507" s="T167">nɨːnɨ</ta>
            <ta e="T169" id="Seg_1508" s="T168">čʼattɨ-sɨ-m</ta>
            <ta e="T170" id="Seg_1509" s="T169">nɨːnɨ</ta>
            <ta e="T171" id="Seg_1510" s="T170">tɨː</ta>
            <ta e="T172" id="Seg_1511" s="T171">tü-sɨ-k</ta>
            <ta e="T173" id="Seg_1512" s="T172">ira</ta>
            <ta e="T174" id="Seg_1513" s="T173">laqɨ-mɔːt-ɛː-sɨ</ta>
            <ta e="T175" id="Seg_1514" s="T174">nɨːnɨ</ta>
            <ta e="T176" id="Seg_1515" s="T175">ira</ta>
            <ta e="T177" id="Seg_1516" s="T176">nälʼa-m-tɨ</ta>
            <ta e="T178" id="Seg_1517" s="T177">mi-mpɨ-tɨ</ta>
            <ta e="T179" id="Seg_1518" s="T178">Ičʼakɨčʼɨka-nɨŋ</ta>
            <ta e="T180" id="Seg_1519" s="T179">qum-ɨ-t</ta>
            <ta e="T181" id="Seg_1520" s="T180">nɨːnɨ</ta>
            <ta e="T182" id="Seg_1521" s="T181">qən-ŋɨ-tɨt</ta>
            <ta e="T183" id="Seg_1522" s="T182">ima-kɔːlɨ-k</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1523" s="T0">Ichakichika.[NOM]</ta>
            <ta e="T2" id="Seg_1524" s="T1">live-PST.NAR.[3SG.S]</ta>
            <ta e="T3" id="Seg_1525" s="T2">grandmother-OBL.3SG-COM</ta>
            <ta e="T4" id="Seg_1526" s="T3">afterwards</ta>
            <ta e="T5" id="Seg_1527" s="T4">Ichakichika.[NOM]</ta>
            <ta e="T6" id="Seg_1528" s="T5">such-ADVZ</ta>
            <ta e="T7" id="Seg_1529" s="T6">say-CO-3SG.O</ta>
            <ta e="T8" id="Seg_1530" s="T7">grandmother-ILL-OBL.3SG</ta>
            <ta e="T9" id="Seg_1531" s="T8">I.NOM</ta>
            <ta e="T10" id="Seg_1532" s="T9">leave-FUT-1SG.S</ta>
            <ta e="T11" id="Seg_1533" s="T10">Tyrshako.[NOM]</ta>
            <ta e="T12" id="Seg_1534" s="T11">husband-GEN</ta>
            <ta e="T13" id="Seg_1535" s="T12">house-ILL</ta>
            <ta e="T14" id="Seg_1536" s="T13">woman-ADJZ</ta>
            <ta e="T15" id="Seg_1537" s="T14">grandmother.[NOM]</ta>
            <ta e="T16" id="Seg_1538" s="T15">such-ADVZ</ta>
            <ta e="T17" id="Seg_1539" s="T16">say-CO-3SG.O</ta>
            <ta e="T18" id="Seg_1540" s="T17">Ichakichika-ALL</ta>
            <ta e="T19" id="Seg_1541" s="T18">how</ta>
            <ta e="T20" id="Seg_1542" s="T19">take-FUT-2SG.O</ta>
            <ta e="T21" id="Seg_1543" s="T20">human.being-EP-PL-EP-GEN</ta>
            <ta e="T22" id="Seg_1544" s="T21">daughter-ACC</ta>
            <ta e="T23" id="Seg_1545" s="T22">and</ta>
            <ta e="T24" id="Seg_1546" s="T23">Ichakichika.[NOM]</ta>
            <ta e="T25" id="Seg_1547" s="T24">such-ADVZ</ta>
            <ta e="T26" id="Seg_1548" s="T25">say-CO-3SG.O</ta>
            <ta e="T27" id="Seg_1549" s="T26">I.NOM</ta>
            <ta e="T28" id="Seg_1550" s="T27">simply</ta>
            <ta e="T29" id="Seg_1551" s="T28">simply</ta>
            <ta e="T30" id="Seg_1552" s="T29">give.a.look-HAB-FUT-1SG.O</ta>
            <ta e="T31" id="Seg_1553" s="T30">how</ta>
            <ta e="T32" id="Seg_1554" s="T31">human.being-EP-PL.[NOM]</ta>
            <ta e="T33" id="Seg_1555" s="T32">woman-VBLZ-CO-3PL</ta>
            <ta e="T34" id="Seg_1556" s="T33">Ichakichika.[NOM]</ta>
            <ta e="T35" id="Seg_1557" s="T34">down</ta>
            <ta e="T36" id="Seg_1558" s="T35">leave-CO.[3SG.S]</ta>
            <ta e="T37" id="Seg_1559" s="T36">crust-ADJZ</ta>
            <ta e="T38" id="Seg_1560" s="T37">boat-ILL</ta>
            <ta e="T39" id="Seg_1561" s="T38">piece-DIM.[NOM]</ta>
            <ta e="T40" id="Seg_1562" s="T39">down</ta>
            <ta e="T41" id="Seg_1563" s="T40">push-MOM-CO-3SG.O</ta>
            <ta e="T42" id="Seg_1564" s="T41">and</ta>
            <ta e="T43" id="Seg_1565" s="T42">then</ta>
            <ta e="T44" id="Seg_1566" s="T43">put-EP-US-IPFV.[3SG.S]</ta>
            <ta e="T45" id="Seg_1567" s="T44">stick-DIM-GEN</ta>
            <ta e="T46" id="Seg_1568" s="T45">down</ta>
            <ta e="T47" id="Seg_1569" s="T46">sit.down.[3SG.S]</ta>
            <ta e="T48" id="Seg_1570" s="T47">boat-LOC-OBL.3SG</ta>
            <ta e="T49" id="Seg_1571" s="T48">then</ta>
            <ta e="T50" id="Seg_1572" s="T49">go.away-CO.[3SG.S]</ta>
            <ta e="T51" id="Seg_1573" s="T50">Tyrshako.[NOM]</ta>
            <ta e="T52" id="Seg_1574" s="T51">old.man-ALL</ta>
            <ta e="T53" id="Seg_1575" s="T52">one</ta>
            <ta e="T54" id="Seg_1576" s="T53">middle-ADV.LOC</ta>
            <ta e="T55" id="Seg_1577" s="T54">lake-GEN</ta>
            <ta e="T56" id="Seg_1578" s="T55">middle-LOC</ta>
            <ta e="T57" id="Seg_1579" s="T56">stomach-ACC-3SG</ta>
            <ta e="T58" id="Seg_1580" s="T57">INFER</ta>
            <ta e="T59" id="Seg_1581" s="T58">poke-HAB-MULO-CO-3SG.O</ta>
            <ta e="T60" id="Seg_1582" s="T59">lake-GEN</ta>
            <ta e="T61" id="Seg_1583" s="T60">middle-LOC</ta>
            <ta e="T62" id="Seg_1584" s="T61">stone-ADJZ</ta>
            <ta e="T63" id="Seg_1585" s="T62">piece.[NOM]</ta>
            <ta e="T64" id="Seg_1586" s="T63">sight-INFER-3SG.O</ta>
            <ta e="T65" id="Seg_1587" s="T64">this</ta>
            <ta e="T66" id="Seg_1588" s="T65">stone-ADJZ</ta>
            <ta e="T67" id="Seg_1589" s="T66">piece-ILL</ta>
            <ta e="T68" id="Seg_1590" s="T67">sit.down-3SG.S</ta>
            <ta e="T69" id="Seg_1591" s="T68">one</ta>
            <ta e="T70" id="Seg_1592" s="T69">middle-ADV.LOC</ta>
            <ta e="T71" id="Seg_1593" s="T70">pike.[NOM]</ta>
            <ta e="T72" id="Seg_1594" s="T71">INFER</ta>
            <ta e="T73" id="Seg_1595" s="T72">bite-PFV-CO-3SG.O</ta>
            <ta e="T74" id="Seg_1596" s="T73">stick-DIM-ACC-3SG</ta>
            <ta e="T75" id="Seg_1597" s="T74">catch-CVB</ta>
            <ta e="T76" id="Seg_1598" s="T75">pike-ACC</ta>
            <ta e="T77" id="Seg_1599" s="T76">head.[NOM]</ta>
            <ta e="T78" id="Seg_1600" s="T77">on</ta>
            <ta e="T79" id="Seg_1601" s="T78">hit-PST.NAR-3SG.O</ta>
            <ta e="T80" id="Seg_1602" s="T79">then</ta>
            <ta e="T81" id="Seg_1603" s="T80">pike-ACC-3SG</ta>
            <ta e="T82" id="Seg_1604" s="T81">boat-LOC-OBL.3SG</ta>
            <ta e="T83" id="Seg_1605" s="T82">throw-PST.NAR-3SG.O</ta>
            <ta e="T84" id="Seg_1606" s="T83">boat-LOC-3SG</ta>
            <ta e="T85" id="Seg_1607" s="T84">sit.down-CO.[3SG.S]</ta>
            <ta e="T86" id="Seg_1608" s="T85">then</ta>
            <ta e="T87" id="Seg_1609" s="T86">leave-CO.[3SG.S]</ta>
            <ta e="T88" id="Seg_1610" s="T87">come-CO.[3SG.S]</ta>
            <ta e="T89" id="Seg_1611" s="T88">Tyrshako.[NOM]</ta>
            <ta e="T90" id="Seg_1612" s="T89">old.man-GEN</ta>
            <ta e="T91" id="Seg_1613" s="T90">tent-ILL</ta>
            <ta e="T92" id="Seg_1614" s="T91">two</ta>
            <ta e="T93" id="Seg_1615" s="T92">boat.[NOM]</ta>
            <ta e="T94" id="Seg_1616" s="T93">stand-INFER.[3SG.S]</ta>
            <ta e="T95" id="Seg_1617" s="T94">come-CO.[3SG.S]</ta>
            <ta e="T96" id="Seg_1618" s="T95">boat-ACC-3SG</ta>
            <ta e="T97" id="Seg_1619" s="T96">upwards</ta>
            <ta e="T98" id="Seg_1620" s="T97">pull-MOM-CO-3SG.O</ta>
            <ta e="T99" id="Seg_1621" s="T98">leave-CO.[3SG.S]</ta>
            <ta e="T100" id="Seg_1622" s="T99">go.out.[3SG.S]</ta>
            <ta e="T101" id="Seg_1623" s="T100">tent-ILL</ta>
            <ta e="T102" id="Seg_1624" s="T101">hardly</ta>
            <ta e="T103" id="Seg_1625" s="T102">come.in-CO.[3SG.S]</ta>
            <ta e="T104" id="Seg_1626" s="T103">tent-LOC</ta>
            <ta e="T105" id="Seg_1627" s="T104">four</ta>
            <ta e="T106" id="Seg_1628" s="T105">human.being.[NOM]</ta>
            <ta e="T107" id="Seg_1629" s="T106">sit.down.[3SG.S]</ta>
            <ta e="T108" id="Seg_1630" s="T107">afterwards</ta>
            <ta e="T109" id="Seg_1631" s="T108">Tyrshako.[NOM]</ta>
            <ta e="T110" id="Seg_1632" s="T109">old.man.[NOM]</ta>
            <ta e="T111" id="Seg_1633" s="T110">such-ADVZ</ta>
            <ta e="T112" id="Seg_1634" s="T111">say-CO-3SG.O</ta>
            <ta e="T113" id="Seg_1635" s="T112">Ichakichika.[NOM]</ta>
            <ta e="T184" id="Seg_1636" s="T113">down</ta>
            <ta e="T114" id="Seg_1637" s="T184">sit.down-IMP.2SG.S</ta>
            <ta e="T115" id="Seg_1638" s="T114">human.being-EP-PL.[NOM]</ta>
            <ta e="T116" id="Seg_1639" s="T115">woman-VBLZ-CO-3PL</ta>
            <ta e="T117" id="Seg_1640" s="T116">old.man-PL.[NOM]</ta>
            <ta e="T118" id="Seg_1641" s="T117">and</ta>
            <ta e="T119" id="Seg_1642" s="T118">old.woman.[NOM]</ta>
            <ta e="T120" id="Seg_1643" s="T119">sit.down-3DU.S</ta>
            <ta e="T121" id="Seg_1644" s="T120">here</ta>
            <ta e="T122" id="Seg_1645" s="T121">word-CAR</ta>
            <ta e="T123" id="Seg_1646" s="T122">afterwards</ta>
            <ta e="T124" id="Seg_1647" s="T123">husband.[NOM]</ta>
            <ta e="T125" id="Seg_1648" s="T124">such-ADVZ</ta>
            <ta e="T126" id="Seg_1649" s="T125">say-CO-3SG.O</ta>
            <ta e="T127" id="Seg_1650" s="T126">who</ta>
            <ta e="T128" id="Seg_1651" s="T127">I.ACC</ta>
            <ta e="T129" id="Seg_1652" s="T128">laugh-CAUS-FUT.[3SG.S]</ta>
            <ta e="T130" id="Seg_1653" s="T129">this-ALL</ta>
            <ta e="T131" id="Seg_1654" s="T130">daughter-ACC</ta>
            <ta e="T132" id="Seg_1655" s="T131">give-INFER-FUT-1SG.O</ta>
            <ta e="T133" id="Seg_1656" s="T132">Ichakichika.[NOM]</ta>
            <ta e="T134" id="Seg_1657" s="T133">such-ADVZ</ta>
            <ta e="T135" id="Seg_1658" s="T134">say-CO-3SG.O</ta>
            <ta e="T136" id="Seg_1659" s="T135">I.NOM</ta>
            <ta e="T137" id="Seg_1660" s="T136">lake-GEN</ta>
            <ta e="T138" id="Seg_1661" s="T137">middle-LOC</ta>
            <ta e="T139" id="Seg_1662" s="T138">stomach.[NOM]-1SG</ta>
            <ta e="T140" id="Seg_1663" s="T139">INFER</ta>
            <ta e="T141" id="Seg_1664" s="T140">poke-MULO-PST-3SG.O</ta>
            <ta e="T142" id="Seg_1665" s="T141">then</ta>
            <ta e="T143" id="Seg_1666" s="T142">I.NOM</ta>
            <ta e="T144" id="Seg_1667" s="T143">sight-PST.[3SG.S]</ta>
            <ta e="T145" id="Seg_1668" s="T144">stone-ADJZ</ta>
            <ta e="T146" id="Seg_1669" s="T145">piece.[NOM]</ta>
            <ta e="T147" id="Seg_1670" s="T146">this</ta>
            <ta e="T148" id="Seg_1671" s="T147">stone-ADJZ</ta>
            <ta e="T149" id="Seg_1672" s="T148">piece-ILL</ta>
            <ta e="T150" id="Seg_1673" s="T149">upwards</ta>
            <ta e="T151" id="Seg_1674" s="T150">run-PST.[3SG.S]</ta>
            <ta e="T152" id="Seg_1675" s="T151">poo-MULO-PST-1SG.O</ta>
            <ta e="T153" id="Seg_1676" s="T152">pike.[NOM]</ta>
            <ta e="T154" id="Seg_1677" s="T153">INFER</ta>
            <ta e="T155" id="Seg_1678" s="T154">I.ACC</ta>
            <ta e="T156" id="Seg_1679" s="T155">bite-PFV-PST.[3SG.S]</ta>
            <ta e="T157" id="Seg_1680" s="T156">I.NOM</ta>
            <ta e="T158" id="Seg_1681" s="T157">stick-DIM-ACC</ta>
            <ta e="T159" id="Seg_1682" s="T158">catch-PST-1SG.O</ta>
            <ta e="T160" id="Seg_1683" s="T159">then</ta>
            <ta e="T161" id="Seg_1684" s="T160">pike.[NOM]</ta>
            <ta e="T162" id="Seg_1685" s="T161">head.[NOM]</ta>
            <ta e="T163" id="Seg_1686" s="T162">on</ta>
            <ta e="T164" id="Seg_1687" s="T163">hit-PST.[3SG.S]</ta>
            <ta e="T165" id="Seg_1688" s="T164">then</ta>
            <ta e="T166" id="Seg_1689" s="T165">pike-ACC</ta>
            <ta e="T167" id="Seg_1690" s="T166">boat-LOC</ta>
            <ta e="T168" id="Seg_1691" s="T167">then</ta>
            <ta e="T169" id="Seg_1692" s="T168">throw-PST-1SG.O</ta>
            <ta e="T170" id="Seg_1693" s="T169">then</ta>
            <ta e="T171" id="Seg_1694" s="T170">here</ta>
            <ta e="T172" id="Seg_1695" s="T171">come-PST-1SG.S</ta>
            <ta e="T173" id="Seg_1696" s="T172">old.man.[NOM]</ta>
            <ta e="T174" id="Seg_1697" s="T173">laugh-DECAUS-PFV-PST.[3SG.S]</ta>
            <ta e="T175" id="Seg_1698" s="T174">then</ta>
            <ta e="T176" id="Seg_1699" s="T175">old.man.[NOM]</ta>
            <ta e="T177" id="Seg_1700" s="T176">daughter-ACC-3SG</ta>
            <ta e="T178" id="Seg_1701" s="T177">give-PST.NAR-3SG.O</ta>
            <ta e="T179" id="Seg_1702" s="T178">Ichakichika-ALL</ta>
            <ta e="T180" id="Seg_1703" s="T179">human.being-EP-PL.[NOM]</ta>
            <ta e="T181" id="Seg_1704" s="T180">then</ta>
            <ta e="T182" id="Seg_1705" s="T181">leave-CO-3PL</ta>
            <ta e="T183" id="Seg_1706" s="T182">woman-CAR-ADVZ</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1707" s="T0">Ичакичика.[NOM]</ta>
            <ta e="T2" id="Seg_1708" s="T1">жить-PST.NAR.[3SG.S]</ta>
            <ta e="T3" id="Seg_1709" s="T2">бабушка-OBL.3SG-COM</ta>
            <ta e="T4" id="Seg_1710" s="T3">потом</ta>
            <ta e="T5" id="Seg_1711" s="T4">Ичакичика.[NOM]</ta>
            <ta e="T6" id="Seg_1712" s="T5">такой-ADVZ</ta>
            <ta e="T7" id="Seg_1713" s="T6">сказать-CO-3SG.O</ta>
            <ta e="T8" id="Seg_1714" s="T7">бабушка-ILL-OBL.3SG</ta>
            <ta e="T9" id="Seg_1715" s="T8">я.NOM</ta>
            <ta e="T10" id="Seg_1716" s="T9">отправиться-FUT-1SG.S</ta>
            <ta e="T11" id="Seg_1717" s="T10">Тыршако.[NOM]</ta>
            <ta e="T12" id="Seg_1718" s="T11">муж-GEN</ta>
            <ta e="T13" id="Seg_1719" s="T12">дом-ILL</ta>
            <ta e="T14" id="Seg_1720" s="T13">женщина-ADJZ</ta>
            <ta e="T15" id="Seg_1721" s="T14">бабушка.[NOM]</ta>
            <ta e="T16" id="Seg_1722" s="T15">такой-ADVZ</ta>
            <ta e="T17" id="Seg_1723" s="T16">сказать-CO-3SG.O</ta>
            <ta e="T18" id="Seg_1724" s="T17">Ичакичика-ALL</ta>
            <ta e="T19" id="Seg_1725" s="T18">как</ta>
            <ta e="T20" id="Seg_1726" s="T19">взять-FUT-2SG.O</ta>
            <ta e="T21" id="Seg_1727" s="T20">человек-EP-PL-EP-GEN</ta>
            <ta e="T22" id="Seg_1728" s="T21">дочь-ACC</ta>
            <ta e="T23" id="Seg_1729" s="T22">а</ta>
            <ta e="T24" id="Seg_1730" s="T23">Ичакичика.[NOM]</ta>
            <ta e="T25" id="Seg_1731" s="T24">такой-ADVZ</ta>
            <ta e="T26" id="Seg_1732" s="T25">сказать-CO-3SG.O</ta>
            <ta e="T27" id="Seg_1733" s="T26">я.NOM</ta>
            <ta e="T28" id="Seg_1734" s="T27">просто.так</ta>
            <ta e="T29" id="Seg_1735" s="T28">просто.так</ta>
            <ta e="T30" id="Seg_1736" s="T29">взглянуть-HAB-FUT-1SG.O</ta>
            <ta e="T31" id="Seg_1737" s="T30">как</ta>
            <ta e="T32" id="Seg_1738" s="T31">человек-EP-PL.[NOM]</ta>
            <ta e="T33" id="Seg_1739" s="T32">женщина-VBLZ-CO-3PL</ta>
            <ta e="T34" id="Seg_1740" s="T33">Ичакичика.[NOM]</ta>
            <ta e="T35" id="Seg_1741" s="T34">вниз</ta>
            <ta e="T36" id="Seg_1742" s="T35">отправиться-CO.[3SG.S]</ta>
            <ta e="T37" id="Seg_1743" s="T36">кора-ADJZ</ta>
            <ta e="T38" id="Seg_1744" s="T37">ветка-ILL</ta>
            <ta e="T39" id="Seg_1745" s="T38">кусок-DIM.[NOM]</ta>
            <ta e="T40" id="Seg_1746" s="T39">вниз</ta>
            <ta e="T41" id="Seg_1747" s="T40">толкать-MOM-CO-3SG.O</ta>
            <ta e="T42" id="Seg_1748" s="T41">и</ta>
            <ta e="T43" id="Seg_1749" s="T42">потом</ta>
            <ta e="T44" id="Seg_1750" s="T43">положить-EP-US-IPFV.[3SG.S]</ta>
            <ta e="T45" id="Seg_1751" s="T44">хорей-DIM-GEN</ta>
            <ta e="T46" id="Seg_1752" s="T45">вниз</ta>
            <ta e="T47" id="Seg_1753" s="T46">сесть.[3SG.S]</ta>
            <ta e="T48" id="Seg_1754" s="T47">ветка-LOC-OBL.3SG</ta>
            <ta e="T49" id="Seg_1755" s="T48">потом</ta>
            <ta e="T50" id="Seg_1756" s="T49">уйти-CO.[3SG.S]</ta>
            <ta e="T51" id="Seg_1757" s="T50">Тыршако.[NOM]</ta>
            <ta e="T52" id="Seg_1758" s="T51">старик-ALL</ta>
            <ta e="T53" id="Seg_1759" s="T52">один</ta>
            <ta e="T54" id="Seg_1760" s="T53">середина-ADV.LOC</ta>
            <ta e="T55" id="Seg_1761" s="T54">озеро-GEN</ta>
            <ta e="T56" id="Seg_1762" s="T55">середина-LOC</ta>
            <ta e="T57" id="Seg_1763" s="T56">живот-ACC-3SG</ta>
            <ta e="T58" id="Seg_1764" s="T57">INFER</ta>
            <ta e="T59" id="Seg_1765" s="T58">шуровать-HAB-MULO-CO-3SG.O</ta>
            <ta e="T60" id="Seg_1766" s="T59">озеро-GEN</ta>
            <ta e="T61" id="Seg_1767" s="T60">середина-LOC</ta>
            <ta e="T62" id="Seg_1768" s="T61">камень-ADJZ</ta>
            <ta e="T63" id="Seg_1769" s="T62">кусок.[NOM]</ta>
            <ta e="T64" id="Seg_1770" s="T63">видеть-INFER-3SG.O</ta>
            <ta e="T65" id="Seg_1771" s="T64">этот</ta>
            <ta e="T66" id="Seg_1772" s="T65">камень-ADJZ</ta>
            <ta e="T67" id="Seg_1773" s="T66">кусок-ILL</ta>
            <ta e="T68" id="Seg_1774" s="T67">сесть-3SG.S</ta>
            <ta e="T69" id="Seg_1775" s="T68">один</ta>
            <ta e="T70" id="Seg_1776" s="T69">середина-ADV.LOC</ta>
            <ta e="T71" id="Seg_1777" s="T70">щука.[NOM]</ta>
            <ta e="T72" id="Seg_1778" s="T71">INFER</ta>
            <ta e="T73" id="Seg_1779" s="T72">куснуть-PFV-CO-3SG.O</ta>
            <ta e="T74" id="Seg_1780" s="T73">хорей-DIM-ACC-3SG</ta>
            <ta e="T75" id="Seg_1781" s="T74">схватить-CVB</ta>
            <ta e="T76" id="Seg_1782" s="T75">щука-ACC</ta>
            <ta e="T77" id="Seg_1783" s="T76">голова.[NOM]</ta>
            <ta e="T78" id="Seg_1784" s="T77">по</ta>
            <ta e="T79" id="Seg_1785" s="T78">ударить-PST.NAR-3SG.O</ta>
            <ta e="T80" id="Seg_1786" s="T79">потом</ta>
            <ta e="T81" id="Seg_1787" s="T80">щука-ACC-3SG</ta>
            <ta e="T82" id="Seg_1788" s="T81">ветка-LOC-OBL.3SG</ta>
            <ta e="T83" id="Seg_1789" s="T82">бросать-PST.NAR-3SG.O</ta>
            <ta e="T84" id="Seg_1790" s="T83">ветка-LOC-3SG</ta>
            <ta e="T85" id="Seg_1791" s="T84">сесть-CO.[3SG.S]</ta>
            <ta e="T86" id="Seg_1792" s="T85">потом</ta>
            <ta e="T87" id="Seg_1793" s="T86">отправиться-CO.[3SG.S]</ta>
            <ta e="T88" id="Seg_1794" s="T87">прийти-CO.[3SG.S]</ta>
            <ta e="T89" id="Seg_1795" s="T88">Тыршако.[NOM]</ta>
            <ta e="T90" id="Seg_1796" s="T89">старик-GEN</ta>
            <ta e="T91" id="Seg_1797" s="T90">чум-ILL</ta>
            <ta e="T92" id="Seg_1798" s="T91">два</ta>
            <ta e="T93" id="Seg_1799" s="T92">ветка.[NOM]</ta>
            <ta e="T94" id="Seg_1800" s="T93">стоять-INFER.[3SG.S]</ta>
            <ta e="T95" id="Seg_1801" s="T94">прийти-CO.[3SG.S]</ta>
            <ta e="T96" id="Seg_1802" s="T95">ветка-ACC-3SG</ta>
            <ta e="T97" id="Seg_1803" s="T96">вверх</ta>
            <ta e="T98" id="Seg_1804" s="T97">тянуть-MOM-CO-3SG.O</ta>
            <ta e="T99" id="Seg_1805" s="T98">отправиться-CO.[3SG.S]</ta>
            <ta e="T100" id="Seg_1806" s="T99">выйти.[3SG.S]</ta>
            <ta e="T101" id="Seg_1807" s="T100">чум-ILL</ta>
            <ta e="T102" id="Seg_1808" s="T101">едва</ta>
            <ta e="T103" id="Seg_1809" s="T102">войти-CO.[3SG.S]</ta>
            <ta e="T104" id="Seg_1810" s="T103">чум-LOC</ta>
            <ta e="T105" id="Seg_1811" s="T104">четыре</ta>
            <ta e="T106" id="Seg_1812" s="T105">человек.[NOM]</ta>
            <ta e="T107" id="Seg_1813" s="T106">сесть.[3SG.S]</ta>
            <ta e="T108" id="Seg_1814" s="T107">потом</ta>
            <ta e="T109" id="Seg_1815" s="T108">Тыршако.[NOM]</ta>
            <ta e="T110" id="Seg_1816" s="T109">старик.[NOM]</ta>
            <ta e="T111" id="Seg_1817" s="T110">такой-ADVZ</ta>
            <ta e="T112" id="Seg_1818" s="T111">сказать-CO-3SG.O</ta>
            <ta e="T113" id="Seg_1819" s="T112">Ичакичика.[NOM]</ta>
            <ta e="T184" id="Seg_1820" s="T113">вниз</ta>
            <ta e="T114" id="Seg_1821" s="T184">сесть-IMP.2SG.S</ta>
            <ta e="T115" id="Seg_1822" s="T114">человек-EP-PL.[NOM]</ta>
            <ta e="T116" id="Seg_1823" s="T115">женщина-VBLZ-CO-3PL</ta>
            <ta e="T117" id="Seg_1824" s="T116">старик-PL.[NOM]</ta>
            <ta e="T118" id="Seg_1825" s="T117">и</ta>
            <ta e="T119" id="Seg_1826" s="T118">старуха.[NOM]</ta>
            <ta e="T120" id="Seg_1827" s="T119">сесть-3DU.S</ta>
            <ta e="T121" id="Seg_1828" s="T120">здесь</ta>
            <ta e="T122" id="Seg_1829" s="T121">слово-CAR</ta>
            <ta e="T123" id="Seg_1830" s="T122">потом</ta>
            <ta e="T124" id="Seg_1831" s="T123">муж.[NOM]</ta>
            <ta e="T125" id="Seg_1832" s="T124">такой-ADVZ</ta>
            <ta e="T126" id="Seg_1833" s="T125">сказать-CO-3SG.O</ta>
            <ta e="T127" id="Seg_1834" s="T126">кто</ta>
            <ta e="T128" id="Seg_1835" s="T127">я.ACC</ta>
            <ta e="T129" id="Seg_1836" s="T128">смеяться-CAUS-FUT.[3SG.S]</ta>
            <ta e="T130" id="Seg_1837" s="T129">этот-ALL</ta>
            <ta e="T131" id="Seg_1838" s="T130">дочь-ACC</ta>
            <ta e="T132" id="Seg_1839" s="T131">дать-INFER-FUT-1SG.O</ta>
            <ta e="T133" id="Seg_1840" s="T132">Ичакичика.[NOM]</ta>
            <ta e="T134" id="Seg_1841" s="T133">такой-ADVZ</ta>
            <ta e="T135" id="Seg_1842" s="T134">сказать-CO-3SG.O</ta>
            <ta e="T136" id="Seg_1843" s="T135">я.NOM</ta>
            <ta e="T137" id="Seg_1844" s="T136">озеро-GEN</ta>
            <ta e="T138" id="Seg_1845" s="T137">середина-LOC</ta>
            <ta e="T139" id="Seg_1846" s="T138">живот.[NOM]-1SG</ta>
            <ta e="T140" id="Seg_1847" s="T139">INFER</ta>
            <ta e="T141" id="Seg_1848" s="T140">шуровать-MULO-PST-3SG.O</ta>
            <ta e="T142" id="Seg_1849" s="T141">потом</ta>
            <ta e="T143" id="Seg_1850" s="T142">я.NOM</ta>
            <ta e="T144" id="Seg_1851" s="T143">видеть-PST.[3SG.S]</ta>
            <ta e="T145" id="Seg_1852" s="T144">камень-ADJZ</ta>
            <ta e="T146" id="Seg_1853" s="T145">кусок.[NOM]</ta>
            <ta e="T147" id="Seg_1854" s="T146">этот</ta>
            <ta e="T148" id="Seg_1855" s="T147">камень-ADJZ</ta>
            <ta e="T149" id="Seg_1856" s="T148">кусок-ILL</ta>
            <ta e="T150" id="Seg_1857" s="T149">вверх</ta>
            <ta e="T151" id="Seg_1858" s="T150">побежать-PST.[3SG.S]</ta>
            <ta e="T152" id="Seg_1859" s="T151">покакать-MULO-PST-1SG.O</ta>
            <ta e="T153" id="Seg_1860" s="T152">щука.[NOM]</ta>
            <ta e="T154" id="Seg_1861" s="T153">INFER</ta>
            <ta e="T155" id="Seg_1862" s="T154">я.ACC</ta>
            <ta e="T156" id="Seg_1863" s="T155">куснуть-PFV-PST.[3SG.S]</ta>
            <ta e="T157" id="Seg_1864" s="T156">я.NOM</ta>
            <ta e="T158" id="Seg_1865" s="T157">хорей-DIM-ACC</ta>
            <ta e="T159" id="Seg_1866" s="T158">схватить-PST-1SG.O</ta>
            <ta e="T160" id="Seg_1867" s="T159">потом</ta>
            <ta e="T161" id="Seg_1868" s="T160">щука.[NOM]</ta>
            <ta e="T162" id="Seg_1869" s="T161">голова.[NOM]</ta>
            <ta e="T163" id="Seg_1870" s="T162">по</ta>
            <ta e="T164" id="Seg_1871" s="T163">ударить-PST.[3SG.S]</ta>
            <ta e="T165" id="Seg_1872" s="T164">потом</ta>
            <ta e="T166" id="Seg_1873" s="T165">щука-ACC</ta>
            <ta e="T167" id="Seg_1874" s="T166">ветка-LOC</ta>
            <ta e="T168" id="Seg_1875" s="T167">потом</ta>
            <ta e="T169" id="Seg_1876" s="T168">бросать-PST-1SG.O</ta>
            <ta e="T170" id="Seg_1877" s="T169">потом</ta>
            <ta e="T171" id="Seg_1878" s="T170">сюда</ta>
            <ta e="T172" id="Seg_1879" s="T171">прийти-PST-1SG.S</ta>
            <ta e="T173" id="Seg_1880" s="T172">старик.[NOM]</ta>
            <ta e="T174" id="Seg_1881" s="T173">хохотать-DECAUS-PFV-PST.[3SG.S]</ta>
            <ta e="T175" id="Seg_1882" s="T174">потом</ta>
            <ta e="T176" id="Seg_1883" s="T175">старик.[NOM]</ta>
            <ta e="T177" id="Seg_1884" s="T176">дочь-ACC-3SG</ta>
            <ta e="T178" id="Seg_1885" s="T177">дать-PST.NAR-3SG.O</ta>
            <ta e="T179" id="Seg_1886" s="T178">Ичакичика-ALL</ta>
            <ta e="T180" id="Seg_1887" s="T179">человек-EP-PL.[NOM]</ta>
            <ta e="T181" id="Seg_1888" s="T180">потом</ta>
            <ta e="T182" id="Seg_1889" s="T181">отправиться-CO-3PL</ta>
            <ta e="T183" id="Seg_1890" s="T182">женщина-CAR-ADVZ</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1891" s="T0">nprop-n:case3</ta>
            <ta e="T2" id="Seg_1892" s="T1">v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_1893" s="T2">n-n:obl.poss-n:case2</ta>
            <ta e="T4" id="Seg_1894" s="T3">adv</ta>
            <ta e="T5" id="Seg_1895" s="T4">nprop-n:case3</ta>
            <ta e="T6" id="Seg_1896" s="T5">dem-adj&gt;adv</ta>
            <ta e="T7" id="Seg_1897" s="T6">v-v:ins-v:pn</ta>
            <ta e="T8" id="Seg_1898" s="T7">n-n:case2-n:obl.poss</ta>
            <ta e="T9" id="Seg_1899" s="T8">pers</ta>
            <ta e="T10" id="Seg_1900" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_1901" s="T10">nprop-n:case3</ta>
            <ta e="T12" id="Seg_1902" s="T11">n-n:case3</ta>
            <ta e="T13" id="Seg_1903" s="T12">n-n:case3</ta>
            <ta e="T14" id="Seg_1904" s="T13">n-n&gt;adj</ta>
            <ta e="T15" id="Seg_1905" s="T14">n-n:case3</ta>
            <ta e="T16" id="Seg_1906" s="T15">dem-adj&gt;adv</ta>
            <ta e="T17" id="Seg_1907" s="T16">v-v:ins-v:pn</ta>
            <ta e="T18" id="Seg_1908" s="T17">nprop-n:case3</ta>
            <ta e="T19" id="Seg_1909" s="T18">interrog</ta>
            <ta e="T20" id="Seg_1910" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_1911" s="T20">n-n:(ins)-n:num-n:(ins)-n:case3</ta>
            <ta e="T22" id="Seg_1912" s="T21">n-n:case3</ta>
            <ta e="T23" id="Seg_1913" s="T22">conj</ta>
            <ta e="T24" id="Seg_1914" s="T23">nprop-n:case3</ta>
            <ta e="T25" id="Seg_1915" s="T24">dem-adj&gt;adv</ta>
            <ta e="T26" id="Seg_1916" s="T25">v-v:ins-v:pn</ta>
            <ta e="T27" id="Seg_1917" s="T26">pers</ta>
            <ta e="T28" id="Seg_1918" s="T27">adv</ta>
            <ta e="T29" id="Seg_1919" s="T28">adv</ta>
            <ta e="T30" id="Seg_1920" s="T29">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_1921" s="T30">interrog</ta>
            <ta e="T32" id="Seg_1922" s="T31">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T33" id="Seg_1923" s="T32">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T34" id="Seg_1924" s="T33">nprop-n:case3</ta>
            <ta e="T35" id="Seg_1925" s="T34">adv</ta>
            <ta e="T36" id="Seg_1926" s="T35">v-v:ins-v:pn</ta>
            <ta e="T37" id="Seg_1927" s="T36">n-n&gt;adj</ta>
            <ta e="T38" id="Seg_1928" s="T37">n-n:case3</ta>
            <ta e="T39" id="Seg_1929" s="T38">n-n&gt;n-n:case3</ta>
            <ta e="T40" id="Seg_1930" s="T39">adv</ta>
            <ta e="T41" id="Seg_1931" s="T40">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T42" id="Seg_1932" s="T41">conj</ta>
            <ta e="T43" id="Seg_1933" s="T42">adv</ta>
            <ta e="T44" id="Seg_1934" s="T43">v-v:ins-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T45" id="Seg_1935" s="T44">n-n&gt;n-n:case3</ta>
            <ta e="T46" id="Seg_1936" s="T45">preverb</ta>
            <ta e="T47" id="Seg_1937" s="T46">v-v:pn</ta>
            <ta e="T48" id="Seg_1938" s="T47">n-n:case2-n:obl.poss</ta>
            <ta e="T49" id="Seg_1939" s="T48">adv</ta>
            <ta e="T50" id="Seg_1940" s="T49">v-v:ins-v:pn</ta>
            <ta e="T51" id="Seg_1941" s="T50">nprop-n:case3</ta>
            <ta e="T52" id="Seg_1942" s="T51">n-n:case2</ta>
            <ta e="T53" id="Seg_1943" s="T52">num</ta>
            <ta e="T54" id="Seg_1944" s="T53">n-n&gt;adv</ta>
            <ta e="T55" id="Seg_1945" s="T54">n-n:case3</ta>
            <ta e="T56" id="Seg_1946" s="T55">n-n:case2</ta>
            <ta e="T57" id="Seg_1947" s="T56">n-n:case1-n:poss</ta>
            <ta e="T58" id="Seg_1948" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_1949" s="T58">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T60" id="Seg_1950" s="T59">n-n:case3</ta>
            <ta e="T61" id="Seg_1951" s="T60">n-n:case2</ta>
            <ta e="T62" id="Seg_1952" s="T61">n-n&gt;adj</ta>
            <ta e="T63" id="Seg_1953" s="T62">n-n:case3</ta>
            <ta e="T64" id="Seg_1954" s="T63">v-v:tense-mood-v:pn</ta>
            <ta e="T65" id="Seg_1955" s="T64">dem</ta>
            <ta e="T66" id="Seg_1956" s="T65">n-n&gt;adj</ta>
            <ta e="T67" id="Seg_1957" s="T66">n-n:case3</ta>
            <ta e="T68" id="Seg_1958" s="T67">v-v:pn</ta>
            <ta e="T69" id="Seg_1959" s="T68">num</ta>
            <ta e="T70" id="Seg_1960" s="T69">n-n&gt;adv</ta>
            <ta e="T71" id="Seg_1961" s="T70">n-n:case3</ta>
            <ta e="T72" id="Seg_1962" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_1963" s="T72">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T74" id="Seg_1964" s="T73">n-n&gt;n-n:case3-n:poss</ta>
            <ta e="T75" id="Seg_1965" s="T74">v-v&gt;adv</ta>
            <ta e="T76" id="Seg_1966" s="T75">n-n:case3</ta>
            <ta e="T77" id="Seg_1967" s="T76">n-n:case3</ta>
            <ta e="T78" id="Seg_1968" s="T77">pp</ta>
            <ta e="T79" id="Seg_1969" s="T78">v-v:tense-v:pn</ta>
            <ta e="T80" id="Seg_1970" s="T79">adv</ta>
            <ta e="T81" id="Seg_1971" s="T80">n-n:case1-n:poss</ta>
            <ta e="T82" id="Seg_1972" s="T81">n-n:case2-n:obl.poss</ta>
            <ta e="T83" id="Seg_1973" s="T82">v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_1974" s="T83">n-n:case2-n:poss</ta>
            <ta e="T85" id="Seg_1975" s="T84">v-v:ins-v:pn</ta>
            <ta e="T86" id="Seg_1976" s="T85">adv</ta>
            <ta e="T87" id="Seg_1977" s="T86">v-v:ins-v:pn</ta>
            <ta e="T88" id="Seg_1978" s="T87">v-v:ins-v:pn</ta>
            <ta e="T89" id="Seg_1979" s="T88">nprop-n:case3</ta>
            <ta e="T90" id="Seg_1980" s="T89">n-n:case3</ta>
            <ta e="T91" id="Seg_1981" s="T90">n-n:case3</ta>
            <ta e="T92" id="Seg_1982" s="T91">num</ta>
            <ta e="T93" id="Seg_1983" s="T92">n-n:case3</ta>
            <ta e="T94" id="Seg_1984" s="T93">v-v:tense-mood-v:pn</ta>
            <ta e="T95" id="Seg_1985" s="T94">v-v:ins-v:pn</ta>
            <ta e="T96" id="Seg_1986" s="T95">n-n:case1-n:poss</ta>
            <ta e="T97" id="Seg_1987" s="T96">adv</ta>
            <ta e="T98" id="Seg_1988" s="T97">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T99" id="Seg_1989" s="T98">v-v:ins-v:pn</ta>
            <ta e="T100" id="Seg_1990" s="T99">v-v:pn</ta>
            <ta e="T101" id="Seg_1991" s="T100">n-n:case3</ta>
            <ta e="T102" id="Seg_1992" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_1993" s="T102">v-v:ins-v:pn</ta>
            <ta e="T104" id="Seg_1994" s="T103">n-n:case3</ta>
            <ta e="T105" id="Seg_1995" s="T104">num</ta>
            <ta e="T106" id="Seg_1996" s="T105">n-n:case3</ta>
            <ta e="T107" id="Seg_1997" s="T106">v-v:pn</ta>
            <ta e="T108" id="Seg_1998" s="T107">adv</ta>
            <ta e="T109" id="Seg_1999" s="T108">nprop-n:case3</ta>
            <ta e="T110" id="Seg_2000" s="T109">n-n:case3</ta>
            <ta e="T111" id="Seg_2001" s="T110">dem-adj&gt;adv</ta>
            <ta e="T112" id="Seg_2002" s="T111">v-v:ins-v:pn</ta>
            <ta e="T113" id="Seg_2003" s="T112">nprop-n:case3</ta>
            <ta e="T184" id="Seg_2004" s="T113">preverb</ta>
            <ta e="T114" id="Seg_2005" s="T184">v-v:mood-pn</ta>
            <ta e="T115" id="Seg_2006" s="T114">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T116" id="Seg_2007" s="T115">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T117" id="Seg_2008" s="T116">n-n:num-n:case3</ta>
            <ta e="T118" id="Seg_2009" s="T117">conj</ta>
            <ta e="T119" id="Seg_2010" s="T118">n-n:case3</ta>
            <ta e="T120" id="Seg_2011" s="T119">v-v:pn</ta>
            <ta e="T121" id="Seg_2012" s="T120">adv</ta>
            <ta e="T122" id="Seg_2013" s="T121">n-n&gt;adj</ta>
            <ta e="T123" id="Seg_2014" s="T122">adv</ta>
            <ta e="T124" id="Seg_2015" s="T123">n-n:case3</ta>
            <ta e="T125" id="Seg_2016" s="T124">dem-adj&gt;adv</ta>
            <ta e="T126" id="Seg_2017" s="T125">v-v:ins-v:pn</ta>
            <ta e="T127" id="Seg_2018" s="T126">interrog</ta>
            <ta e="T128" id="Seg_2019" s="T127">pers</ta>
            <ta e="T129" id="Seg_2020" s="T128">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T130" id="Seg_2021" s="T129">dem-n:case3</ta>
            <ta e="T131" id="Seg_2022" s="T130">n-n:case3</ta>
            <ta e="T132" id="Seg_2023" s="T131">v-v:mood2-v:tense-v:pn</ta>
            <ta e="T133" id="Seg_2024" s="T132">nprop-n:case3</ta>
            <ta e="T134" id="Seg_2025" s="T133">dem-adj&gt;adv</ta>
            <ta e="T135" id="Seg_2026" s="T134">v-v:ins-v:pn</ta>
            <ta e="T136" id="Seg_2027" s="T135">pers</ta>
            <ta e="T137" id="Seg_2028" s="T136">n-n:case3</ta>
            <ta e="T138" id="Seg_2029" s="T137">n-n:case3</ta>
            <ta e="T139" id="Seg_2030" s="T138">n-n:case1-n:poss</ta>
            <ta e="T140" id="Seg_2031" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_2032" s="T140">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_2033" s="T141">adv</ta>
            <ta e="T143" id="Seg_2034" s="T142">pers</ta>
            <ta e="T144" id="Seg_2035" s="T143">v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_2036" s="T144">n-n&gt;adj</ta>
            <ta e="T146" id="Seg_2037" s="T145">n-n:case3</ta>
            <ta e="T147" id="Seg_2038" s="T146">dem</ta>
            <ta e="T148" id="Seg_2039" s="T147">n-n&gt;adj</ta>
            <ta e="T149" id="Seg_2040" s="T148">n-n:case3</ta>
            <ta e="T150" id="Seg_2041" s="T149">adv</ta>
            <ta e="T151" id="Seg_2042" s="T150">v-v:tense-v:pn</ta>
            <ta e="T152" id="Seg_2043" s="T151">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_2044" s="T152">n-n:case3</ta>
            <ta e="T154" id="Seg_2045" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_2046" s="T154">pers</ta>
            <ta e="T156" id="Seg_2047" s="T155">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T157" id="Seg_2048" s="T156">pers</ta>
            <ta e="T158" id="Seg_2049" s="T157">n-n&gt;n-n:case3</ta>
            <ta e="T159" id="Seg_2050" s="T158">v-v:tense-v:pn</ta>
            <ta e="T160" id="Seg_2051" s="T159">adv</ta>
            <ta e="T161" id="Seg_2052" s="T160">n-n:case3</ta>
            <ta e="T162" id="Seg_2053" s="T161">n-n:case3</ta>
            <ta e="T163" id="Seg_2054" s="T162">pp</ta>
            <ta e="T164" id="Seg_2055" s="T163">v-v:tense-v:pn</ta>
            <ta e="T165" id="Seg_2056" s="T164">adv</ta>
            <ta e="T166" id="Seg_2057" s="T165">n-n:case3</ta>
            <ta e="T167" id="Seg_2058" s="T166">n-n:case3</ta>
            <ta e="T168" id="Seg_2059" s="T167">adv</ta>
            <ta e="T169" id="Seg_2060" s="T168">v-v:tense-v:pn</ta>
            <ta e="T170" id="Seg_2061" s="T169">adv</ta>
            <ta e="T171" id="Seg_2062" s="T170">adv</ta>
            <ta e="T172" id="Seg_2063" s="T171">v-v:tense-v:pn</ta>
            <ta e="T173" id="Seg_2064" s="T172">n-n:case3</ta>
            <ta e="T174" id="Seg_2065" s="T173">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T175" id="Seg_2066" s="T174">adv</ta>
            <ta e="T176" id="Seg_2067" s="T175">n-n:case3</ta>
            <ta e="T177" id="Seg_2068" s="T176">n-n:case1-n:poss</ta>
            <ta e="T178" id="Seg_2069" s="T177">v-v:tense-v:pn</ta>
            <ta e="T179" id="Seg_2070" s="T178">nprop-n:case3</ta>
            <ta e="T180" id="Seg_2071" s="T179">n-v:ins-n:num-n:case3</ta>
            <ta e="T181" id="Seg_2072" s="T180">adv</ta>
            <ta e="T182" id="Seg_2073" s="T181">v-v:ins-v:pn</ta>
            <ta e="T183" id="Seg_2074" s="T182">n-n&gt;adj-adj&gt;adv</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2075" s="T0">nprop</ta>
            <ta e="T2" id="Seg_2076" s="T1">v</ta>
            <ta e="T3" id="Seg_2077" s="T2">n</ta>
            <ta e="T4" id="Seg_2078" s="T3">adv</ta>
            <ta e="T5" id="Seg_2079" s="T4">nprop</ta>
            <ta e="T6" id="Seg_2080" s="T5">adv</ta>
            <ta e="T7" id="Seg_2081" s="T6">v</ta>
            <ta e="T8" id="Seg_2082" s="T7">n</ta>
            <ta e="T9" id="Seg_2083" s="T8">pers</ta>
            <ta e="T10" id="Seg_2084" s="T9">v</ta>
            <ta e="T11" id="Seg_2085" s="T10">nprop</ta>
            <ta e="T12" id="Seg_2086" s="T11">n</ta>
            <ta e="T13" id="Seg_2087" s="T12">n</ta>
            <ta e="T14" id="Seg_2088" s="T13">adj</ta>
            <ta e="T15" id="Seg_2089" s="T14">n</ta>
            <ta e="T16" id="Seg_2090" s="T15">adv</ta>
            <ta e="T17" id="Seg_2091" s="T16">v</ta>
            <ta e="T18" id="Seg_2092" s="T17">nprop</ta>
            <ta e="T19" id="Seg_2093" s="T18">interrog</ta>
            <ta e="T20" id="Seg_2094" s="T19">v</ta>
            <ta e="T21" id="Seg_2095" s="T20">n</ta>
            <ta e="T22" id="Seg_2096" s="T21">n</ta>
            <ta e="T23" id="Seg_2097" s="T22">conj</ta>
            <ta e="T24" id="Seg_2098" s="T23">nprop</ta>
            <ta e="T25" id="Seg_2099" s="T24">adv</ta>
            <ta e="T26" id="Seg_2100" s="T25">v</ta>
            <ta e="T27" id="Seg_2101" s="T26">pers</ta>
            <ta e="T28" id="Seg_2102" s="T27">adv</ta>
            <ta e="T29" id="Seg_2103" s="T28">adv</ta>
            <ta e="T30" id="Seg_2104" s="T29">v</ta>
            <ta e="T31" id="Seg_2105" s="T30">interrog</ta>
            <ta e="T32" id="Seg_2106" s="T31">n</ta>
            <ta e="T33" id="Seg_2107" s="T32">v</ta>
            <ta e="T34" id="Seg_2108" s="T33">nprop</ta>
            <ta e="T35" id="Seg_2109" s="T34">adv</ta>
            <ta e="T36" id="Seg_2110" s="T35">v</ta>
            <ta e="T37" id="Seg_2111" s="T36">adj</ta>
            <ta e="T38" id="Seg_2112" s="T37">n</ta>
            <ta e="T39" id="Seg_2113" s="T38">n</ta>
            <ta e="T40" id="Seg_2114" s="T39">adv</ta>
            <ta e="T41" id="Seg_2115" s="T40">v</ta>
            <ta e="T42" id="Seg_2116" s="T41">conj</ta>
            <ta e="T43" id="Seg_2117" s="T42">adv</ta>
            <ta e="T44" id="Seg_2118" s="T43">v</ta>
            <ta e="T45" id="Seg_2119" s="T44">n</ta>
            <ta e="T46" id="Seg_2120" s="T45">preverb</ta>
            <ta e="T47" id="Seg_2121" s="T46">v</ta>
            <ta e="T48" id="Seg_2122" s="T47">n</ta>
            <ta e="T49" id="Seg_2123" s="T48">adv</ta>
            <ta e="T50" id="Seg_2124" s="T49">v</ta>
            <ta e="T51" id="Seg_2125" s="T50">nprop</ta>
            <ta e="T52" id="Seg_2126" s="T51">n</ta>
            <ta e="T53" id="Seg_2127" s="T52">num</ta>
            <ta e="T54" id="Seg_2128" s="T53">n</ta>
            <ta e="T55" id="Seg_2129" s="T54">v</ta>
            <ta e="T56" id="Seg_2130" s="T55">n</ta>
            <ta e="T57" id="Seg_2131" s="T56">n</ta>
            <ta e="T58" id="Seg_2132" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_2133" s="T58">v</ta>
            <ta e="T60" id="Seg_2134" s="T59">v</ta>
            <ta e="T61" id="Seg_2135" s="T60">n</ta>
            <ta e="T62" id="Seg_2136" s="T61">adj</ta>
            <ta e="T63" id="Seg_2137" s="T62">n</ta>
            <ta e="T64" id="Seg_2138" s="T63">v</ta>
            <ta e="T65" id="Seg_2139" s="T64">dem</ta>
            <ta e="T66" id="Seg_2140" s="T65">adj</ta>
            <ta e="T67" id="Seg_2141" s="T66">n</ta>
            <ta e="T68" id="Seg_2142" s="T67">v</ta>
            <ta e="T69" id="Seg_2143" s="T68">num</ta>
            <ta e="T70" id="Seg_2144" s="T69">n</ta>
            <ta e="T71" id="Seg_2145" s="T70">n</ta>
            <ta e="T72" id="Seg_2146" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_2147" s="T72">v</ta>
            <ta e="T74" id="Seg_2148" s="T73">n</ta>
            <ta e="T75" id="Seg_2149" s="T74">adv</ta>
            <ta e="T76" id="Seg_2150" s="T75">n</ta>
            <ta e="T77" id="Seg_2151" s="T76">n</ta>
            <ta e="T78" id="Seg_2152" s="T77">pp</ta>
            <ta e="T79" id="Seg_2153" s="T78">v</ta>
            <ta e="T80" id="Seg_2154" s="T79">adv</ta>
            <ta e="T81" id="Seg_2155" s="T80">n</ta>
            <ta e="T82" id="Seg_2156" s="T81">n</ta>
            <ta e="T83" id="Seg_2157" s="T82">v</ta>
            <ta e="T84" id="Seg_2158" s="T83">n</ta>
            <ta e="T85" id="Seg_2159" s="T84">v</ta>
            <ta e="T86" id="Seg_2160" s="T85">adv</ta>
            <ta e="T87" id="Seg_2161" s="T86">v</ta>
            <ta e="T88" id="Seg_2162" s="T87">v</ta>
            <ta e="T89" id="Seg_2163" s="T88">nprop</ta>
            <ta e="T90" id="Seg_2164" s="T89">n</ta>
            <ta e="T91" id="Seg_2165" s="T90">n</ta>
            <ta e="T92" id="Seg_2166" s="T91">num</ta>
            <ta e="T93" id="Seg_2167" s="T92">n</ta>
            <ta e="T94" id="Seg_2168" s="T93">v</ta>
            <ta e="T95" id="Seg_2169" s="T94">v</ta>
            <ta e="T96" id="Seg_2170" s="T95">n</ta>
            <ta e="T97" id="Seg_2171" s="T96">adv</ta>
            <ta e="T98" id="Seg_2172" s="T97">v</ta>
            <ta e="T99" id="Seg_2173" s="T98">v</ta>
            <ta e="T100" id="Seg_2174" s="T99">v</ta>
            <ta e="T101" id="Seg_2175" s="T100">n</ta>
            <ta e="T102" id="Seg_2176" s="T101">conj</ta>
            <ta e="T103" id="Seg_2177" s="T102">v</ta>
            <ta e="T104" id="Seg_2178" s="T103">n</ta>
            <ta e="T105" id="Seg_2179" s="T104">num</ta>
            <ta e="T106" id="Seg_2180" s="T105">n</ta>
            <ta e="T107" id="Seg_2181" s="T106">v</ta>
            <ta e="T108" id="Seg_2182" s="T107">adv</ta>
            <ta e="T109" id="Seg_2183" s="T108">nprop</ta>
            <ta e="T110" id="Seg_2184" s="T109">n</ta>
            <ta e="T111" id="Seg_2185" s="T110">adv</ta>
            <ta e="T112" id="Seg_2186" s="T111">v</ta>
            <ta e="T113" id="Seg_2187" s="T112">nprop</ta>
            <ta e="T184" id="Seg_2188" s="T113">preverb</ta>
            <ta e="T114" id="Seg_2189" s="T184">v</ta>
            <ta e="T115" id="Seg_2190" s="T114">n</ta>
            <ta e="T116" id="Seg_2191" s="T115">v</ta>
            <ta e="T117" id="Seg_2192" s="T116">n</ta>
            <ta e="T118" id="Seg_2193" s="T117">conj</ta>
            <ta e="T119" id="Seg_2194" s="T118">n</ta>
            <ta e="T120" id="Seg_2195" s="T119">v</ta>
            <ta e="T121" id="Seg_2196" s="T120">adv</ta>
            <ta e="T122" id="Seg_2197" s="T121">adj</ta>
            <ta e="T123" id="Seg_2198" s="T122">adv</ta>
            <ta e="T124" id="Seg_2199" s="T123">n</ta>
            <ta e="T125" id="Seg_2200" s="T124">adv</ta>
            <ta e="T126" id="Seg_2201" s="T125">v</ta>
            <ta e="T127" id="Seg_2202" s="T126">interrog</ta>
            <ta e="T128" id="Seg_2203" s="T127">pers</ta>
            <ta e="T129" id="Seg_2204" s="T128">v</ta>
            <ta e="T130" id="Seg_2205" s="T129">dem</ta>
            <ta e="T131" id="Seg_2206" s="T130">n</ta>
            <ta e="T132" id="Seg_2207" s="T131">v</ta>
            <ta e="T133" id="Seg_2208" s="T132">nprop</ta>
            <ta e="T134" id="Seg_2209" s="T133">adv</ta>
            <ta e="T135" id="Seg_2210" s="T134">v</ta>
            <ta e="T136" id="Seg_2211" s="T135">pers</ta>
            <ta e="T137" id="Seg_2212" s="T136">v</ta>
            <ta e="T138" id="Seg_2213" s="T137">n</ta>
            <ta e="T139" id="Seg_2214" s="T138">n</ta>
            <ta e="T140" id="Seg_2215" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_2216" s="T140">v</ta>
            <ta e="T142" id="Seg_2217" s="T141">adv</ta>
            <ta e="T143" id="Seg_2218" s="T142">pers</ta>
            <ta e="T144" id="Seg_2219" s="T143">v</ta>
            <ta e="T145" id="Seg_2220" s="T144">adj</ta>
            <ta e="T146" id="Seg_2221" s="T145">n</ta>
            <ta e="T147" id="Seg_2222" s="T146">dem</ta>
            <ta e="T148" id="Seg_2223" s="T147">adj</ta>
            <ta e="T149" id="Seg_2224" s="T148">n</ta>
            <ta e="T150" id="Seg_2225" s="T149">adv</ta>
            <ta e="T151" id="Seg_2226" s="T150">v</ta>
            <ta e="T152" id="Seg_2227" s="T151">v</ta>
            <ta e="T153" id="Seg_2228" s="T152">n</ta>
            <ta e="T154" id="Seg_2229" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_2230" s="T154">pers</ta>
            <ta e="T156" id="Seg_2231" s="T155">v</ta>
            <ta e="T157" id="Seg_2232" s="T156">pers</ta>
            <ta e="T158" id="Seg_2233" s="T157">adv</ta>
            <ta e="T159" id="Seg_2234" s="T158">v</ta>
            <ta e="T160" id="Seg_2235" s="T159">adv</ta>
            <ta e="T161" id="Seg_2236" s="T160">n</ta>
            <ta e="T162" id="Seg_2237" s="T161">n</ta>
            <ta e="T163" id="Seg_2238" s="T162">pp</ta>
            <ta e="T164" id="Seg_2239" s="T163">v</ta>
            <ta e="T165" id="Seg_2240" s="T164">adv</ta>
            <ta e="T166" id="Seg_2241" s="T165">n</ta>
            <ta e="T167" id="Seg_2242" s="T166">n</ta>
            <ta e="T168" id="Seg_2243" s="T167">adv</ta>
            <ta e="T169" id="Seg_2244" s="T168">v</ta>
            <ta e="T170" id="Seg_2245" s="T169">adv</ta>
            <ta e="T171" id="Seg_2246" s="T170">adv</ta>
            <ta e="T172" id="Seg_2247" s="T171">v</ta>
            <ta e="T173" id="Seg_2248" s="T172">n</ta>
            <ta e="T174" id="Seg_2249" s="T173">v</ta>
            <ta e="T175" id="Seg_2250" s="T174">adv</ta>
            <ta e="T176" id="Seg_2251" s="T175">n</ta>
            <ta e="T177" id="Seg_2252" s="T176">n</ta>
            <ta e="T178" id="Seg_2253" s="T177">v</ta>
            <ta e="T179" id="Seg_2254" s="T178">nprop</ta>
            <ta e="T180" id="Seg_2255" s="T179">n</ta>
            <ta e="T181" id="Seg_2256" s="T180">adv</ta>
            <ta e="T182" id="Seg_2257" s="T181">v</ta>
            <ta e="T183" id="Seg_2258" s="T182">adv</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T59" id="Seg_2259" s="T58">RUS:cult </ta>
            <ta e="T141" id="Seg_2260" s="T140">RUS:cult </ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_2261" s="T0">Ичикычика жил с бабушкой.</ta>
            <ta e="T13" id="Seg_2262" s="T3">Потом Ичикычика так своей бабушке сказал: "Я поеду к дедушке Тыршако домой".</ta>
            <ta e="T22" id="Seg_2263" s="T13">Бабушка так сказала Ичикычику: "Как ты возьмешь чужую дочку?"</ta>
            <ta e="T33" id="Seg_2264" s="T22">А Ичика так сказал: "Я просто так посмотрю, как люди сватаются".</ta>
            <ta e="T39" id="Seg_2265" s="T33">Ичикычика вниз пошел к ветке из коры.</ta>
            <ta e="T48" id="Seg_2266" s="T39">Вниз столкнул [ветку] и потом положил палку (палочку) на ветку сел на ветку.</ta>
            <ta e="T52" id="Seg_2267" s="T48">Потом поехал к Тыршако-старику.</ta>
            <ta e="T59" id="Seg_2268" s="T52">Потом в середине озера желудок заболел.</ta>
            <ta e="T64" id="Seg_2269" s="T59">Среди озера большой камень увидел.</ta>
            <ta e="T73" id="Seg_2270" s="T64">Он на камень сел, вдруг щука откуда ни возьмись укусила.</ta>
            <ta e="T79" id="Seg_2271" s="T73">Палку схватив, щуку по голове стукнул.</ta>
            <ta e="T85" id="Seg_2272" s="T79">Потом щуку в ветку положил, на ветку сел.</ta>
            <ta e="T91" id="Seg_2273" s="T85">Потом отправился-приехал в чум Тыршако-старика.</ta>
            <ta e="T94" id="Seg_2274" s="T91">Две ветки стоят.</ta>
            <ta e="T98" id="Seg_2275" s="T94">Подъехал, ветку наверх вытащил.</ta>
            <ta e="T103" id="Seg_2276" s="T98">Пошел вышел [?], только вошел в чум.</ta>
            <ta e="T107" id="Seg_2277" s="T103">В чуме четыре человека сидит.</ta>
            <ta e="T114" id="Seg_2278" s="T107">Потом Тыршако-старик так говорит: “Ичикичика, садись”.</ta>
            <ta e="T116" id="Seg_2279" s="T114">Люди сватаются.</ta>
            <ta e="T122" id="Seg_2280" s="T116">Старик со старухой все сидят молча.</ta>
            <ta e="T132" id="Seg_2281" s="T122">Потом старик так говорит: "Кто меня рассмешит, тому дочь дам".</ta>
            <ta e="T141" id="Seg_2282" s="T132">Ичикичика так говорит: "Я [ехал] среди озера, желудок у меня заболел.</ta>
            <ta e="T152" id="Seg_2283" s="T141">Потом (я) нашел камень, на этот камень залез и покакал.</ta>
            <ta e="T156" id="Seg_2284" s="T152">Щука тут меня схватила.</ta>
            <ta e="T159" id="Seg_2285" s="T156">Я за палку схватился.</ta>
            <ta e="T164" id="Seg_2286" s="T159">Потом щуку по голове стукнул.</ta>
            <ta e="T169" id="Seg_2287" s="T164">Потом щуку на ветку бросил.</ta>
            <ta e="T172" id="Seg_2288" s="T169">Потом я сюда приехал".</ta>
            <ta e="T174" id="Seg_2289" s="T172">Старик засмеялся.</ta>
            <ta e="T179" id="Seg_2290" s="T174">Потом старик дочь выдал замуж за Ичикычику.</ta>
            <ta e="T183" id="Seg_2291" s="T179">Люди тогда уехали без невесты.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_2292" s="T0">Ichakichika lived with his grandmother.</ta>
            <ta e="T13" id="Seg_2293" s="T3">Then Ichakichika said to his grandmother: "I will go to the house of grandfather Tyrshako".</ta>
            <ta e="T22" id="Seg_2294" s="T13">Grandmother said to Ichakichika: "How will you take a foreign human's daughter?"</ta>
            <ta e="T33" id="Seg_2295" s="T22">But Ichakichika said: "I will simply take a look on how people court".</ta>
            <ta e="T39" id="Seg_2296" s="T33">Ichakichika went down to the crust boat.</ta>
            <ta e="T48" id="Seg_2297" s="T39">He pushed down the [boat], then he put a (small) stick into it and sat into the boat.</ta>
            <ta e="T52" id="Seg_2298" s="T48">Then he went to the old man Tyrshako.</ta>
            <ta e="T59" id="Seg_2299" s="T52">In the middle of the lake his stomach started to hurt.</ta>
            <ta e="T64" id="Seg_2300" s="T59">He saw a big stone in the middle of the lake.</ta>
            <ta e="T73" id="Seg_2301" s="T64">He sat down on the stone, suddenly a pike appeared and bit him.</ta>
            <ta e="T79" id="Seg_2302" s="T73">Having grabbed the stick, he hit over the pike's head.</ta>
            <ta e="T85" id="Seg_2303" s="T79">Then he put the pike into the boat, he sat down into the boat.</ta>
            <ta e="T91" id="Seg_2304" s="T85">Then he went-came to the tent of the old man Tyrshako.</ta>
            <ta e="T94" id="Seg_2305" s="T91">Two boats are standing.</ta>
            <ta e="T98" id="Seg_2306" s="T94">He came up, pulled the boat out.</ta>
            <ta e="T103" id="Seg_2307" s="T98">He went, came out [?], just came in.</ta>
            <ta e="T107" id="Seg_2308" s="T103">Four humans are sitting in the tent.</ta>
            <ta e="T114" id="Seg_2309" s="T107">Then the old man Tyrshako says: "Ichakichika, take a seat".</ta>
            <ta e="T116" id="Seg_2310" s="T114">People are courting.</ta>
            <ta e="T122" id="Seg_2311" s="T116">The old man and his old woman are sitting without a word.</ta>
            <ta e="T132" id="Seg_2312" s="T122">Then the old man says: "To the one who makes me laugh I will give my daughter".</ta>
            <ta e="T141" id="Seg_2313" s="T132">Ichakichika says: "I [went] along the lake, my stomach hurt.</ta>
            <ta e="T152" id="Seg_2314" s="T141">Then I found a stone, climbed upon it and pooped.</ta>
            <ta e="T156" id="Seg_2315" s="T152">A pike bit me.</ta>
            <ta e="T159" id="Seg_2316" s="T156">I grabbed a stick.</ta>
            <ta e="T164" id="Seg_2317" s="T159">Then I hit over the pike's head.</ta>
            <ta e="T169" id="Seg_2318" s="T164">Then I threw the pike into the boat.</ta>
            <ta e="T172" id="Seg_2319" s="T169">Then I came here".</ta>
            <ta e="T174" id="Seg_2320" s="T172">The old man started laughing.</ta>
            <ta e="T179" id="Seg_2321" s="T174">Then the old man gave his daughter to Ichakichika.</ta>
            <ta e="T183" id="Seg_2322" s="T179">People went off without a bride.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_2323" s="T0">Itschakitschika lebte mit seiner Großmutter.</ta>
            <ta e="T13" id="Seg_2324" s="T3">Dann sagte Itschikitschika zu seiner Großmutter: "Ich fahre zum Tyrschako-Alten nach Hause."</ta>
            <ta e="T22" id="Seg_2325" s="T13">Die Großmutter sagte zu Itschikitschika: "Wie nimmst du die Tochter eines Fremden?"</ta>
            <ta e="T33" id="Seg_2326" s="T22">Aber Itschakitschika sagte so: "Ich möchte nur sehen, wie man um eine Frau wirbt."</ta>
            <ta e="T39" id="Seg_2327" s="T33">Itschakitschika ging runter zum Borkenboot.</ta>
            <ta e="T48" id="Seg_2328" s="T39">Er stieß [das Boot] hinunter, dann legte er einen Stock (Stöckchen) ins Boot, setzte sich ins Boot.</ta>
            <ta e="T52" id="Seg_2329" s="T48">Dann fuhr er zum Tyrschako-Alten.</ta>
            <ta e="T59" id="Seg_2330" s="T52">Dann in der Mitte des Sees tat ihm sein Magen weh.</ta>
            <ta e="T64" id="Seg_2331" s="T59">In der Mitte des Sees sah er einen großen Stein liegen.</ta>
            <ta e="T73" id="Seg_2332" s="T64">Er setzte sich auf den Stein, plötzlich erschien ein Hecht und biss ihn.</ta>
            <ta e="T79" id="Seg_2333" s="T73">Er nahm den Stock und schlug dem Hecht auf den Kopf.</ta>
            <ta e="T85" id="Seg_2334" s="T79">Dann legte er den Hecht ins Boot und setzte sich ins Boot.</ta>
            <ta e="T91" id="Seg_2335" s="T85">Dann fuhr und kam er zum Zelt des Tyrschako-Alten.</ta>
            <ta e="T94" id="Seg_2336" s="T91">Zwei Boote liegen dort.</ta>
            <ta e="T98" id="Seg_2337" s="T94">Er legte an und zog das Boot ans Ufer.</ta>
            <ta e="T103" id="Seg_2338" s="T98">Er ging, ging hinaus, betrat kaum das Zelt.</ta>
            <ta e="T107" id="Seg_2339" s="T103">Im Zelt sitzen vier Menschen.</ta>
            <ta e="T114" id="Seg_2340" s="T107">Dann sagt der Tyrschako-Alte so: "Itschakitschika, setz dich."</ta>
            <ta e="T116" id="Seg_2341" s="T114">Die Menschen werben um ein Mädchen.</ta>
            <ta e="T122" id="Seg_2342" s="T116">Der Alte und die Alte sitzen schweigend.</ta>
            <ta e="T132" id="Seg_2343" s="T122">Plötzlich sagt der Alte so: "Wer mich zum Lachen bringt, dem gebe ich meine Tochter."</ta>
            <ta e="T141" id="Seg_2344" s="T132">Itschakitschika sagt so: "In der Mitte des Sees hat mit der Magen wehgetan.</ta>
            <ta e="T152" id="Seg_2345" s="T141">Dann habe ich einen Stein gefunden, bin auf diesen Stein gestiegen und habe gekackt.</ta>
            <ta e="T156" id="Seg_2346" s="T152">Ein Hecht hat mich gebissen.</ta>
            <ta e="T159" id="Seg_2347" s="T156">Ich griff nach dem Stock.</ta>
            <ta e="T164" id="Seg_2348" s="T159">Dann habe ich dem Hecht auf den Kopf geschlagen.</ta>
            <ta e="T169" id="Seg_2349" s="T164">Dann habe ich den Hecht ins Boot geworfen.</ta>
            <ta e="T172" id="Seg_2350" s="T169">Dann bin ich hierher gekommen."</ta>
            <ta e="T174" id="Seg_2351" s="T172">Der Alte lachte.</ta>
            <ta e="T179" id="Seg_2352" s="T174">Dann gab der Alte seine Tochter Itschikitschika zur Frau.</ta>
            <ta e="T183" id="Seg_2353" s="T179">Die Menschen fuhren dann ohne Braut fort.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_2354" s="T0">Ичик жил с бабушкой.</ta>
            <ta e="T13" id="Seg_2355" s="T3">потом Ичик-ичак так своей бабушке сказал. я поеду к дедушке (дедушку так звали) домой.</ta>
            <ta e="T22" id="Seg_2356" s="T13">бабушка так сказала: Ичик-ичика как ты возьмешь чужую дочку.</ta>
            <ta e="T33" id="Seg_2357" s="T22">а Ичик ичик так сказал я просто так посмотрю как люди сватаются</ta>
            <ta e="T39" id="Seg_2358" s="T33">И-И. утром пошел ветка с горы.</ta>
            <ta e="T48" id="Seg_2359" s="T39">затолкнул ветку потом положил палку (палочку) на ветку сел на ветку.</ta>
            <ta e="T52" id="Seg_2360" s="T48">потом поехал к Тырсу старику (вчум).</ta>
            <ta e="T59" id="Seg_2361" s="T52">потом в озере в середине реки желудок заболел.</ta>
            <ta e="T64" id="Seg_2362" s="T59">среди озера лежит большой камень (видит).</ta>
            <ta e="T73" id="Seg_2363" s="T64">он на камень сел (((unknown))), вдруг щука откуда возьмись укусила.</ta>
            <ta e="T79" id="Seg_2364" s="T73">он палкой взял щуку по голове стукнул.</ta>
            <ta e="T85" id="Seg_2365" s="T79">потом щуку в ветку положил, на ветку сел.</ta>
            <ta e="T91" id="Seg_2366" s="T85">потом поехал в чум тырщи kо-старика в чум.</ta>
            <ta e="T94" id="Seg_2367" s="T91">две ветки стоят.</ta>
            <ta e="T98" id="Seg_2368" s="T94">подъехал, ветку вытащил.</ta>
            <ta e="T103" id="Seg_2369" s="T98">пошел [вышел] вошел в чум.</ta>
            <ta e="T107" id="Seg_2370" s="T103">в чуме четыре человека сидит.</ta>
            <ta e="T114" id="Seg_2371" s="T107">Потом т-старик так говорит садись</ta>
            <ta e="T116" id="Seg_2372" s="T114">народ сватается.</ta>
            <ta e="T122" id="Seg_2373" s="T116">старик со старухой все сидят молча.</ta>
            <ta e="T132" id="Seg_2374" s="T122">Вдруг старик так говорит. кто бы меня рассмешил тому бы дочь дал.</ta>
            <ta e="T141" id="Seg_2375" s="T132">и-и так говорит. я ехал среди озера желудок заболел.</ta>
            <ta e="T152" id="Seg_2376" s="T141">я нашел камень. залез на этот камень. управился (покакал)</ta>
            <ta e="T156" id="Seg_2377" s="T152">щука тут меня схватила.</ta>
            <ta e="T159" id="Seg_2378" s="T156">я за палку схватился.</ta>
            <ta e="T164" id="Seg_2379" s="T159">щуку по голове стукнул.</ta>
            <ta e="T169" id="Seg_2380" s="T164">потом щуку на ветку бросил.</ta>
            <ta e="T172" id="Seg_2381" s="T169">потом сюда приехал.</ta>
            <ta e="T174" id="Seg_2382" s="T172">старик засмеялся.</ta>
            <ta e="T179" id="Seg_2383" s="T174">старик дочь выдал замуж за Ичу-Ичика.</ta>
            <ta e="T183" id="Seg_2384" s="T179">люди уехали без невесты.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T39" id="Seg_2385" s="T33">[BrM]: Unclear usage and uncertain glossing of "lʼiːpɨlʼa". </ta>
            <ta e="T48" id="Seg_2386" s="T39">[BrM]: The word "narapo" actually means a special long stick used to drive reindeers, but in the whole text it is translated by the author as simple stick, and we preserve this translation.</ta>
            <ta e="T73" id="Seg_2387" s="T64">[BrM]: The orginal word "ontanʼ" was changed to "omtan".</ta>
            <ta e="T114" id="Seg_2388" s="T107">[OSV]: The word "lʼumtäːsik" has been edited into "ıl omtäsik".</ta>
            <ta e="T152" id="Seg_2389" s="T141">[BrM]: Unclear usage of the form of 3SG in "qosa", "paktɨsa".</ta>
            <ta e="T159" id="Seg_2390" s="T156">[BrM]: Unusual allomorph "orkɨ" for the stem "orqɨl".</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T184" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
