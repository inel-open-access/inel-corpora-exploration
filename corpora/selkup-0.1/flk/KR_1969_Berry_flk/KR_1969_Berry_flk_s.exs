<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID6EAFDFC2-447A-DDC1-B0AE-260FB6C55C88">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">flk\KR_1969_Berry_flk\KR_1969_Berry_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">68</ud-information>
            <ud-information attribute-name="# HIAT:w">53</ud-information>
            <ud-information attribute-name="# e">53</ud-information>
            <ud-information attribute-name="# HIAT:u">12</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KR">
            <abbreviation>KR</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="SPK0">
            <abbreviation>KR</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KR"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T53" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Topɨrsailaka</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">aj</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">nʼuːtɨlʼ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">tamtɨr</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Ilimpoːqı</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">topɨrsailaka</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">aj</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">nʼuːtɨlʼ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">tamtɨr</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_35" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">Qarɨlʼalʼ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">kalʼlʼih</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">nʼentɨ</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">antɔːqı</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_50" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">Kutɨ</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">tü</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">čʼɔːtɛntɨt</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_62" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">Ukkur</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">qarɨn</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">aj</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">nʼentɨ</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">antalʼalimpɔːqı</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_80" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">Topɨrsailaka</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">nıː</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">kəttɨkɨtɨ</ts>
                  <nts id="Seg_89" n="HIAT:ip">:</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">nʼuːtɨlʼ</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">tamtɨr</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">tü</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">čʼɔːtašik</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_105" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">Nʼuːtɨlʼ</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">tamtɨr</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">nıː</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">kəttɨkɨtɨ</ts>
                  <nts id="Seg_117" n="HIAT:ip">:</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">onantɨ</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_123" n="HIAT:w" s="T33">čʼɔːtatɨ</ts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_127" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_129" n="HIAT:w" s="T34">Ippɨmpɔːqı</ts>
                  <nts id="Seg_130" n="HIAT:ip">,</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">ompašim</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">nʼuːtɨlʼ</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">tamtɨr</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T38">ontɨ</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">ınnä</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_148" n="HIAT:w" s="T40">wešimpa</ts>
                  <nts id="Seg_149" n="HIAT:ip">.</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_152" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_154" n="HIAT:w" s="T41">Sɔːlʼpo</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_157" n="HIAT:w" s="T42">ılʼlʼa</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_160" n="HIAT:w" s="T43">čʼap</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_163" n="HIAT:w" s="T44">qəttɔːlpatɨ</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_167" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">Čʼɔːppaıːmpatɨ</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_173" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_175" n="HIAT:w" s="T46">Topɨrsailaka</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_178" n="HIAT:w" s="T47">ınnä</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_181" n="HIAT:w" s="T48">wešila</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_184" n="HIAT:w" s="T49">nannar</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_187" n="HIAT:w" s="T50">pisešpa</ts>
                  <nts id="Seg_188" n="HIAT:ip">.</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_191" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_193" n="HIAT:w" s="T51">To</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_196" n="HIAT:w" s="T52">passaıːmpa</ts>
                  <nts id="Seg_197" n="HIAT:ip">.</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T53" id="Seg_199" n="sc" s="T0">
               <ts e="T1" id="Seg_201" n="e" s="T0">Topɨrsailaka </ts>
               <ts e="T2" id="Seg_203" n="e" s="T1">aj </ts>
               <ts e="T3" id="Seg_205" n="e" s="T2">nʼuːtɨlʼ </ts>
               <ts e="T4" id="Seg_207" n="e" s="T3">tamtɨr. </ts>
               <ts e="T5" id="Seg_209" n="e" s="T4">Ilimpoːqı </ts>
               <ts e="T6" id="Seg_211" n="e" s="T5">topɨrsailaka </ts>
               <ts e="T7" id="Seg_213" n="e" s="T6">aj </ts>
               <ts e="T8" id="Seg_215" n="e" s="T7">nʼuːtɨlʼ </ts>
               <ts e="T9" id="Seg_217" n="e" s="T8">tamtɨr. </ts>
               <ts e="T10" id="Seg_219" n="e" s="T9">Qarɨlʼalʼ </ts>
               <ts e="T11" id="Seg_221" n="e" s="T10">kalʼlʼih </ts>
               <ts e="T12" id="Seg_223" n="e" s="T11">nʼentɨ </ts>
               <ts e="T13" id="Seg_225" n="e" s="T12">antɔːqı. </ts>
               <ts e="T14" id="Seg_227" n="e" s="T13">Kutɨ </ts>
               <ts e="T15" id="Seg_229" n="e" s="T14">tü </ts>
               <ts e="T16" id="Seg_231" n="e" s="T15">čʼɔːtɛntɨt. </ts>
               <ts e="T17" id="Seg_233" n="e" s="T16">Ukkur </ts>
               <ts e="T18" id="Seg_235" n="e" s="T17">qarɨn </ts>
               <ts e="T19" id="Seg_237" n="e" s="T18">aj </ts>
               <ts e="T20" id="Seg_239" n="e" s="T19">nʼentɨ </ts>
               <ts e="T21" id="Seg_241" n="e" s="T20">antalʼalimpɔːqı. </ts>
               <ts e="T22" id="Seg_243" n="e" s="T21">Topɨrsailaka </ts>
               <ts e="T23" id="Seg_245" n="e" s="T22">nıː </ts>
               <ts e="T24" id="Seg_247" n="e" s="T23">kəttɨkɨtɨ: </ts>
               <ts e="T25" id="Seg_249" n="e" s="T24">nʼuːtɨlʼ </ts>
               <ts e="T26" id="Seg_251" n="e" s="T25">tamtɨr </ts>
               <ts e="T27" id="Seg_253" n="e" s="T26">tü </ts>
               <ts e="T28" id="Seg_255" n="e" s="T27">čʼɔːtašik. </ts>
               <ts e="T29" id="Seg_257" n="e" s="T28">Nʼuːtɨlʼ </ts>
               <ts e="T30" id="Seg_259" n="e" s="T29">tamtɨr </ts>
               <ts e="T31" id="Seg_261" n="e" s="T30">nıː </ts>
               <ts e="T32" id="Seg_263" n="e" s="T31">kəttɨkɨtɨ: </ts>
               <ts e="T33" id="Seg_265" n="e" s="T32">onantɨ </ts>
               <ts e="T34" id="Seg_267" n="e" s="T33">čʼɔːtatɨ. </ts>
               <ts e="T35" id="Seg_269" n="e" s="T34">Ippɨmpɔːqı, </ts>
               <ts e="T36" id="Seg_271" n="e" s="T35">ompašim </ts>
               <ts e="T37" id="Seg_273" n="e" s="T36">nʼuːtɨlʼ </ts>
               <ts e="T38" id="Seg_275" n="e" s="T37">tamtɨr </ts>
               <ts e="T39" id="Seg_277" n="e" s="T38">ontɨ </ts>
               <ts e="T40" id="Seg_279" n="e" s="T39">ınnä </ts>
               <ts e="T41" id="Seg_281" n="e" s="T40">wešimpa. </ts>
               <ts e="T42" id="Seg_283" n="e" s="T41">Sɔːlʼpo </ts>
               <ts e="T43" id="Seg_285" n="e" s="T42">ılʼlʼa </ts>
               <ts e="T44" id="Seg_287" n="e" s="T43">čʼap </ts>
               <ts e="T45" id="Seg_289" n="e" s="T44">qəttɔːlpatɨ. </ts>
               <ts e="T46" id="Seg_291" n="e" s="T45">Čʼɔːppaıːmpatɨ. </ts>
               <ts e="T47" id="Seg_293" n="e" s="T46">Topɨrsailaka </ts>
               <ts e="T48" id="Seg_295" n="e" s="T47">ınnä </ts>
               <ts e="T49" id="Seg_297" n="e" s="T48">wešila </ts>
               <ts e="T50" id="Seg_299" n="e" s="T49">nannar </ts>
               <ts e="T51" id="Seg_301" n="e" s="T50">pisešpa. </ts>
               <ts e="T52" id="Seg_303" n="e" s="T51">To </ts>
               <ts e="T53" id="Seg_305" n="e" s="T52">passaıːmpa. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_306" s="T0">KR_1969_Berry_flk.001 (001)</ta>
            <ta e="T9" id="Seg_307" s="T4">KR_1969_Berry_flk.002 (002.001)</ta>
            <ta e="T13" id="Seg_308" s="T9">KR_1969_Berry_flk.003 (002.002)</ta>
            <ta e="T16" id="Seg_309" s="T13">KR_1969_Berry_flk.004 (002.003)</ta>
            <ta e="T21" id="Seg_310" s="T16">KR_1969_Berry_flk.005 (002.004)</ta>
            <ta e="T28" id="Seg_311" s="T21">KR_1969_Berry_flk.006 (002.005)</ta>
            <ta e="T34" id="Seg_312" s="T28">KR_1969_Berry_flk.007 (002.006)</ta>
            <ta e="T41" id="Seg_313" s="T34">KR_1969_Berry_flk.008 (002.007)</ta>
            <ta e="T45" id="Seg_314" s="T41">KR_1969_Berry_flk.009 (002.008)</ta>
            <ta e="T46" id="Seg_315" s="T45">KR_1969_Berry_flk.010 (002.009)</ta>
            <ta e="T51" id="Seg_316" s="T46">KR_1969_Berry_flk.011 (002.010)</ta>
            <ta e="T53" id="Seg_317" s="T51">KR_1969_Berry_flk.012 (002.011)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_318" s="T0">топыр′саиlака ай ′нʼӯтылʼ′тамтыр.</ta>
            <ta e="T9" id="Seg_319" s="T4">′иlʼимпо̄kи ′топырсаиlака ай нʼӯтылʼ′тамтыр.</ta>
            <ta e="T13" id="Seg_320" s="T9">х̥[k]а̄рылʼалʼ каllих нʼӓнты ан′та̄̄̊kи. </ta>
            <ta e="T16" id="Seg_321" s="T13">куты тӱ̄ ч′о̄тӓнтыт.</ta>
            <ta e="T21" id="Seg_322" s="T16">′уккур k[х̥]а̄рын ай ′нʼенты ′анталʼалʼим‵по̄kи.</ta>
            <ta e="T28" id="Seg_323" s="T21">′топырсаиlака нӣ ′kъ̊ттыɣыты: нʼӯтыl′та[о]мтыр тӱ̄ ′чо̄′тащʼих. </ta>
            <ta e="T34" id="Seg_324" s="T28">нʼӯтыl ′тӓмтыр нӣ ′kъ̊ттыɣыты: о′нанты ′чо̄таты.</ta>
            <ta e="T41" id="Seg_325" s="T34">′иппымпо̄kи, ′омб̂ащим нʼӯтылʼ ′тамтыр ′онд̂ы ′иннӓ ′wӓ̄[ə]шʼимпа.</ta>
            <ta e="T45" id="Seg_326" s="T41">′со̄[а̊̄]lпо ′ӣlʼа чап k[х̥]ът′тоlб̂аты.</ta>
            <ta e="T46" id="Seg_327" s="T45">′ча̊ппа′ӣмпаты.</ta>
            <ta e="T51" id="Seg_328" s="T46">′топыр саи′lака и′ннӓ ′wə̄[ӓ̄]шʼиlа ′наннар ′писʼешʼпа.</ta>
            <ta e="T53" id="Seg_329" s="T51">то̄ па′ссаимпа</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_330" s="T0">topɨrsailʼaka aj nʼuːtɨlʼtamtɨr.</ta>
            <ta e="T9" id="Seg_331" s="T4">ilʼimpoːqi topɨrsailʼaka aj nʼuːtɨlʼtamtɨr.</ta>
            <ta e="T13" id="Seg_332" s="T9">q[q]aːrɨlʼalʼ kalʼlʼih nʼäntɨ antaːqi. </ta>
            <ta e="T16" id="Seg_333" s="T13">kutɨ tüː čoːtäntɨt.</ta>
            <ta e="T21" id="Seg_334" s="T16">ukkur q[q]aːrɨn aj nʼentɨ antalʼalʼimpoːqi.</ta>
            <ta e="T28" id="Seg_335" s="T21">topɨrsailʼaka niː qəttɨqɨtɨ. nʼuːtɨlʼta[o]mtɨr tüː čoːtašʼih </ta>
            <ta e="T34" id="Seg_336" s="T28">nʼuːtɨlʼ tämtɨr niː qəttɨqɨtɨ: onantɨ čoːtatɨ.</ta>
            <ta e="T41" id="Seg_337" s="T34">ippɨmpoːqi, omp̂ašim nʼuːtɨlʼ tamtɨr ond̂ɨ innä wäː[ə]šimpa.</ta>
            <ta e="T45" id="Seg_338" s="T41">soː[aː]lʼpo iːlʼa čap q[q]əttolʼp̂atɨ.</ta>
            <ta e="T46" id="Seg_339" s="T45">čappaiːmpatɨ </ta>
            <ta e="T51" id="Seg_340" s="T46">topɨr sailʼaka innä wəː[äː]šilʼa nannar pisʼešpa.</ta>
            <ta e="T53" id="Seg_341" s="T51">toː passaimpa</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_342" s="T0">Topɨrsailaka aj nʼuːtɨlʼ tamtɨr. </ta>
            <ta e="T9" id="Seg_343" s="T4">Ilimpoːqı topɨrsailaka aj nʼuːtɨlʼ tamtɨr. </ta>
            <ta e="T13" id="Seg_344" s="T9">Qarɨlʼalʼ kalʼlʼih nʼentɨ antɔːqı. </ta>
            <ta e="T16" id="Seg_345" s="T13">Kutɨ tü čʼɔːtɛntɨt. </ta>
            <ta e="T21" id="Seg_346" s="T16">Ukkur qarɨn aj nʼentɨ antalʼalimpɔːqı. </ta>
            <ta e="T28" id="Seg_347" s="T21">Topɨrsailaka nıː kəttɨkɨtɨ: nʼuːtɨlʼ tamtɨr tü čʼɔːtašik. </ta>
            <ta e="T34" id="Seg_348" s="T28">Nʼuːtɨlʼ tamtɨr nıː kəttɨkɨtɨ: onantɨ čʼɔːtatɨ. </ta>
            <ta e="T41" id="Seg_349" s="T34">Ippɨmpɔːqı, ompašim nʼuːtɨlʼ tamtɨr ontɨ ınnä wešimpa. </ta>
            <ta e="T45" id="Seg_350" s="T41">Sɔːlʼpo ılʼlʼa čʼap qəttɔːlpatɨ. </ta>
            <ta e="T46" id="Seg_351" s="T45">Čʼɔːppaıːmpatɨ. </ta>
            <ta e="T51" id="Seg_352" s="T46">Topɨrsailaka ınnä wešila nannar pisešpa. </ta>
            <ta e="T53" id="Seg_353" s="T51">To passaıːmpa. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_354" s="T0">topɨr-sai-laka</ta>
            <ta e="T2" id="Seg_355" s="T1">aj</ta>
            <ta e="T3" id="Seg_356" s="T2">nʼuːtɨ-lʼ</ta>
            <ta e="T4" id="Seg_357" s="T3">tamtɨr</ta>
            <ta e="T5" id="Seg_358" s="T4">ili-mpoː-qı</ta>
            <ta e="T6" id="Seg_359" s="T5">topɨr-sai-laka</ta>
            <ta e="T7" id="Seg_360" s="T6">aj</ta>
            <ta e="T8" id="Seg_361" s="T7">nʼuːtɨ-lʼ</ta>
            <ta e="T9" id="Seg_362" s="T8">tamtɨr</ta>
            <ta e="T10" id="Seg_363" s="T9">qarɨ-lʼa-lʼ</ta>
            <ta e="T11" id="Seg_364" s="T10">kalʼlʼih</ta>
            <ta e="T12" id="Seg_365" s="T11">nʼentɨ</ta>
            <ta e="T13" id="Seg_366" s="T12">antɔː-qı</ta>
            <ta e="T14" id="Seg_367" s="T13">kutɨ</ta>
            <ta e="T15" id="Seg_368" s="T14">tü</ta>
            <ta e="T16" id="Seg_369" s="T15">čʼɔːt-ɛntɨ-t</ta>
            <ta e="T17" id="Seg_370" s="T16">ukkur</ta>
            <ta e="T18" id="Seg_371" s="T17">qarɨ-n</ta>
            <ta e="T19" id="Seg_372" s="T18">aj</ta>
            <ta e="T20" id="Seg_373" s="T19">nʼentɨ</ta>
            <ta e="T21" id="Seg_374" s="T20">ant-alʼ-ali-mpɔː-qı</ta>
            <ta e="T22" id="Seg_375" s="T21">topɨr-sai-laka</ta>
            <ta e="T23" id="Seg_376" s="T22">nıː</ta>
            <ta e="T24" id="Seg_377" s="T23">kəttɨ-kɨ-tɨ</ta>
            <ta e="T25" id="Seg_378" s="T24">nʼuːtɨ-lʼ</ta>
            <ta e="T26" id="Seg_379" s="T25">tamtɨr</ta>
            <ta e="T27" id="Seg_380" s="T26">tü</ta>
            <ta e="T28" id="Seg_381" s="T27">čʼɔːt-ašik</ta>
            <ta e="T29" id="Seg_382" s="T28">nʼuːtɨ-lʼ</ta>
            <ta e="T30" id="Seg_383" s="T29">tamtɨr</ta>
            <ta e="T31" id="Seg_384" s="T30">nıː</ta>
            <ta e="T32" id="Seg_385" s="T31">kəttɨ-kɨ-tɨ</ta>
            <ta e="T33" id="Seg_386" s="T32">onantɨ</ta>
            <ta e="T34" id="Seg_387" s="T33">čʼɔːt-atɨ</ta>
            <ta e="T35" id="Seg_388" s="T34">ippɨ-mpɔː-qı</ta>
            <ta e="T36" id="Seg_389" s="T35">ompašim</ta>
            <ta e="T37" id="Seg_390" s="T36">nʼuːtɨ-lʼ</ta>
            <ta e="T38" id="Seg_391" s="T37">tamtɨr</ta>
            <ta e="T39" id="Seg_392" s="T38">ontɨ</ta>
            <ta e="T40" id="Seg_393" s="T39">ınnä</ta>
            <ta e="T41" id="Seg_394" s="T40">weši-mpa</ta>
            <ta e="T42" id="Seg_395" s="T41">sɔːlʼpo</ta>
            <ta e="T43" id="Seg_396" s="T42">ılʼlʼa</ta>
            <ta e="T44" id="Seg_397" s="T43">čʼap</ta>
            <ta e="T45" id="Seg_398" s="T44">qətt-ɔːl-pa-tɨ</ta>
            <ta e="T46" id="Seg_399" s="T45">čʼɔːpp-aıː-mpa-tɨ</ta>
            <ta e="T47" id="Seg_400" s="T46">topɨr-sai-laka</ta>
            <ta e="T48" id="Seg_401" s="T47">ınnä</ta>
            <ta e="T49" id="Seg_402" s="T48">weši-la</ta>
            <ta e="T50" id="Seg_403" s="T49">nannar</ta>
            <ta e="T51" id="Seg_404" s="T50">pise-š-pa</ta>
            <ta e="T52" id="Seg_405" s="T51">to</ta>
            <ta e="T53" id="Seg_406" s="T52">pass-aıː-mpa</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_407" s="T0">topɨr-sajɨ-laka</ta>
            <ta e="T2" id="Seg_408" s="T1">aj</ta>
            <ta e="T3" id="Seg_409" s="T2">nʼuːtɨ-lʼ</ta>
            <ta e="T4" id="Seg_410" s="T3">tamtɨr</ta>
            <ta e="T5" id="Seg_411" s="T4">ilɨ-mpɨ-qı</ta>
            <ta e="T6" id="Seg_412" s="T5">topɨr-sajɨ-laka</ta>
            <ta e="T7" id="Seg_413" s="T6">aj</ta>
            <ta e="T8" id="Seg_414" s="T7">nʼuːtɨ-lʼ</ta>
            <ta e="T9" id="Seg_415" s="T8">tamtɨr</ta>
            <ta e="T10" id="Seg_416" s="T9">qarɨ-lʼa-lʼ</ta>
            <ta e="T11" id="Seg_417" s="T10">kalʼlʼih</ta>
            <ta e="T12" id="Seg_418" s="T11">nʼentɨ</ta>
            <ta e="T13" id="Seg_419" s="T12">antɨ-qı</ta>
            <ta e="T14" id="Seg_420" s="T13">kutɨ</ta>
            <ta e="T15" id="Seg_421" s="T14">tü</ta>
            <ta e="T16" id="Seg_422" s="T15">čʼɔːtɨ-ɛntɨ-tɨ</ta>
            <ta e="T17" id="Seg_423" s="T16">ukkɨr</ta>
            <ta e="T18" id="Seg_424" s="T17">qarɨ-n</ta>
            <ta e="T19" id="Seg_425" s="T18">aj</ta>
            <ta e="T20" id="Seg_426" s="T19">nʼentɨ</ta>
            <ta e="T21" id="Seg_427" s="T20">antɨ-alʼ-alɨ-mpɨ-qı</ta>
            <ta e="T22" id="Seg_428" s="T21">topɨr-sajɨ-laka</ta>
            <ta e="T23" id="Seg_429" s="T22">nık</ta>
            <ta e="T24" id="Seg_430" s="T23">kətɨ-kkɨ-tɨ</ta>
            <ta e="T25" id="Seg_431" s="T24">nʼuːtɨ-lʼ</ta>
            <ta e="T26" id="Seg_432" s="T25">tamtɨr</ta>
            <ta e="T27" id="Seg_433" s="T26">tü</ta>
            <ta e="T28" id="Seg_434" s="T27">čʼɔːtɨ-äšɨk</ta>
            <ta e="T29" id="Seg_435" s="T28">nʼuːtɨ-lʼ</ta>
            <ta e="T30" id="Seg_436" s="T29">tamtɨr</ta>
            <ta e="T31" id="Seg_437" s="T30">nık</ta>
            <ta e="T32" id="Seg_438" s="T31">kətɨ-kkɨ-tɨ</ta>
            <ta e="T33" id="Seg_439" s="T32">onäntɨ</ta>
            <ta e="T34" id="Seg_440" s="T33">čʼɔːtɨ-ätɨ</ta>
            <ta e="T35" id="Seg_441" s="T34">ippɨ-mpɨ-qı</ta>
            <ta e="T36" id="Seg_442" s="T35">ompä</ta>
            <ta e="T37" id="Seg_443" s="T36">nʼuːtɨ-lʼ</ta>
            <ta e="T38" id="Seg_444" s="T37">tamtɨr</ta>
            <ta e="T39" id="Seg_445" s="T38">ontɨ</ta>
            <ta e="T40" id="Seg_446" s="T39">ınnä</ta>
            <ta e="T41" id="Seg_447" s="T40">wešɨ-mpɨ</ta>
            <ta e="T42" id="Seg_448" s="T41">sɔːlʼpo</ta>
            <ta e="T43" id="Seg_449" s="T42">ıllä</ta>
            <ta e="T44" id="Seg_450" s="T43">čʼam</ta>
            <ta e="T45" id="Seg_451" s="T44">qättɨ-ɔːl-mpɨ-tɨ</ta>
            <ta e="T46" id="Seg_452" s="T45">čʼɔːpɨ-ɛː-mpɨ-tɨ</ta>
            <ta e="T47" id="Seg_453" s="T46">topɨr-sajɨ-laka</ta>
            <ta e="T48" id="Seg_454" s="T47">ınnä</ta>
            <ta e="T49" id="Seg_455" s="T48">wešɨ-lä</ta>
            <ta e="T50" id="Seg_456" s="T49">nannɛr</ta>
            <ta e="T51" id="Seg_457" s="T50">pisɨ-š-mpɨ</ta>
            <ta e="T52" id="Seg_458" s="T51">to</ta>
            <ta e="T53" id="Seg_459" s="T52">pasɨ-ɛː-mpɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_460" s="T0">berry-eye-SNGL.[NOM]</ta>
            <ta e="T2" id="Seg_461" s="T1">and</ta>
            <ta e="T3" id="Seg_462" s="T2">grass-ADJZ</ta>
            <ta e="T4" id="Seg_463" s="T3">bunch.[NOM]</ta>
            <ta e="T5" id="Seg_464" s="T4">live-PST.NAR-3DU.S</ta>
            <ta e="T6" id="Seg_465" s="T5">berry-eye-SNGL.[NOM]</ta>
            <ta e="T7" id="Seg_466" s="T6">and</ta>
            <ta e="T8" id="Seg_467" s="T7">grass-ADJZ</ta>
            <ta e="T9" id="Seg_468" s="T8">bunch.[NOM]</ta>
            <ta e="T10" id="Seg_469" s="T9">morning-DIM-ADJZ</ta>
            <ta e="T11" id="Seg_470" s="T10">%%</ta>
            <ta e="T12" id="Seg_471" s="T11">together</ta>
            <ta e="T13" id="Seg_472" s="T12">dispute-3DU.S</ta>
            <ta e="T14" id="Seg_473" s="T13">who</ta>
            <ta e="T15" id="Seg_474" s="T14">fire.[NOM]</ta>
            <ta e="T16" id="Seg_475" s="T15">set.fire-FUT-3SG.O</ta>
            <ta e="T17" id="Seg_476" s="T16">one</ta>
            <ta e="T18" id="Seg_477" s="T17">morning-ADV.LOC</ta>
            <ta e="T19" id="Seg_478" s="T18">again</ta>
            <ta e="T20" id="Seg_479" s="T19">together</ta>
            <ta e="T21" id="Seg_480" s="T20">dispute-INCH-RFL-PST.NAR-3DU.S</ta>
            <ta e="T22" id="Seg_481" s="T21">berry-eye-SNGL.[NOM]</ta>
            <ta e="T23" id="Seg_482" s="T22">so</ta>
            <ta e="T24" id="Seg_483" s="T23">say-DUR-3SG.O</ta>
            <ta e="T25" id="Seg_484" s="T24">grass-ADJZ</ta>
            <ta e="T26" id="Seg_485" s="T25">bunch.[NOM]</ta>
            <ta e="T27" id="Seg_486" s="T26">fire.[NOM]</ta>
            <ta e="T28" id="Seg_487" s="T27">set.fire-IMP.2SG.S</ta>
            <ta e="T29" id="Seg_488" s="T28">grass-ADJZ</ta>
            <ta e="T30" id="Seg_489" s="T29">bunch.[NOM]</ta>
            <ta e="T31" id="Seg_490" s="T30">so</ta>
            <ta e="T32" id="Seg_491" s="T31">say-DUR-3SG.O</ta>
            <ta e="T33" id="Seg_492" s="T32">yourself</ta>
            <ta e="T34" id="Seg_493" s="T33">set.fire-IMP.2SG.O</ta>
            <ta e="T35" id="Seg_494" s="T34">lie-PST.NAR-3DU.S</ta>
            <ta e="T36" id="Seg_495" s="T35">soon</ta>
            <ta e="T37" id="Seg_496" s="T36">grass-ADJZ</ta>
            <ta e="T38" id="Seg_497" s="T37">bunch.[NOM]</ta>
            <ta e="T39" id="Seg_498" s="T38">himself</ta>
            <ta e="T40" id="Seg_499" s="T39">up</ta>
            <ta e="T41" id="Seg_500" s="T40">get.up-PST.NAR.[3SG.S]</ta>
            <ta e="T42" id="Seg_501" s="T41">match.[NOM]</ta>
            <ta e="T43" id="Seg_502" s="T42">down</ta>
            <ta e="T44" id="Seg_503" s="T43">only</ta>
            <ta e="T45" id="Seg_504" s="T44">hit-MOM-PST.NAR-3SG.O</ta>
            <ta e="T46" id="Seg_505" s="T45">burn-PFV-PST.NAR-3SG.O</ta>
            <ta e="T47" id="Seg_506" s="T46">berry-eye-SNGL.[NOM]</ta>
            <ta e="T48" id="Seg_507" s="T47">up</ta>
            <ta e="T49" id="Seg_508" s="T48">get.up-CVB</ta>
            <ta e="T50" id="Seg_509" s="T49">so.much</ta>
            <ta e="T51" id="Seg_510" s="T50">laugh-US-PST.NAR.[3SG.S]</ta>
            <ta e="T52" id="Seg_511" s="T51">that</ta>
            <ta e="T53" id="Seg_512" s="T52">break-PFV-PST.NAR.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_513" s="T0">ягода-глаз-SNGL.[NOM]</ta>
            <ta e="T2" id="Seg_514" s="T1">и</ta>
            <ta e="T3" id="Seg_515" s="T2">трава-ADJZ</ta>
            <ta e="T4" id="Seg_516" s="T3">связка.[NOM]</ta>
            <ta e="T5" id="Seg_517" s="T4">жить-PST.NAR-3DU.S</ta>
            <ta e="T6" id="Seg_518" s="T5">ягода-глаз-SNGL.[NOM]</ta>
            <ta e="T7" id="Seg_519" s="T6">и</ta>
            <ta e="T8" id="Seg_520" s="T7">трава-ADJZ</ta>
            <ta e="T9" id="Seg_521" s="T8">связка.[NOM]</ta>
            <ta e="T10" id="Seg_522" s="T9">утро-DIM-ADJZ</ta>
            <ta e="T11" id="Seg_523" s="T10">%%</ta>
            <ta e="T12" id="Seg_524" s="T11">вместе</ta>
            <ta e="T13" id="Seg_525" s="T12">спорить-3DU.S</ta>
            <ta e="T14" id="Seg_526" s="T13">кто</ta>
            <ta e="T15" id="Seg_527" s="T14">огонь.[NOM]</ta>
            <ta e="T16" id="Seg_528" s="T15">зажечь-FUT-3SG.O</ta>
            <ta e="T17" id="Seg_529" s="T16">один</ta>
            <ta e="T18" id="Seg_530" s="T17">утро-ADV.LOC</ta>
            <ta e="T19" id="Seg_531" s="T18">опять</ta>
            <ta e="T20" id="Seg_532" s="T19">вместе</ta>
            <ta e="T21" id="Seg_533" s="T20">спорить-INCH-RFL-PST.NAR-3DU.S</ta>
            <ta e="T22" id="Seg_534" s="T21">ягода-глаз-SNGL.[NOM]</ta>
            <ta e="T23" id="Seg_535" s="T22">так</ta>
            <ta e="T24" id="Seg_536" s="T23">сказать-DUR-3SG.O</ta>
            <ta e="T25" id="Seg_537" s="T24">трава-ADJZ</ta>
            <ta e="T26" id="Seg_538" s="T25">связка.[NOM]</ta>
            <ta e="T27" id="Seg_539" s="T26">огонь.[NOM]</ta>
            <ta e="T28" id="Seg_540" s="T27">зажечь-IMP.2SG.S</ta>
            <ta e="T29" id="Seg_541" s="T28">трава-ADJZ</ta>
            <ta e="T30" id="Seg_542" s="T29">связка.[NOM]</ta>
            <ta e="T31" id="Seg_543" s="T30">так</ta>
            <ta e="T32" id="Seg_544" s="T31">сказать-DUR-3SG.O</ta>
            <ta e="T33" id="Seg_545" s="T32">ты.сам</ta>
            <ta e="T34" id="Seg_546" s="T33">зажечь-IMP.2SG.O</ta>
            <ta e="T35" id="Seg_547" s="T34">лежать-PST.NAR-3DU.S</ta>
            <ta e="T36" id="Seg_548" s="T35">скоро</ta>
            <ta e="T37" id="Seg_549" s="T36">трава-ADJZ</ta>
            <ta e="T38" id="Seg_550" s="T37">связка.[NOM]</ta>
            <ta e="T39" id="Seg_551" s="T38">он.сам</ta>
            <ta e="T40" id="Seg_552" s="T39">вверх</ta>
            <ta e="T41" id="Seg_553" s="T40">встать-PST.NAR.[3SG.S]</ta>
            <ta e="T42" id="Seg_554" s="T41">спичка.[NOM]</ta>
            <ta e="T43" id="Seg_555" s="T42">вниз</ta>
            <ta e="T44" id="Seg_556" s="T43">только</ta>
            <ta e="T45" id="Seg_557" s="T44">ударить-MOM-PST.NAR-3SG.O</ta>
            <ta e="T46" id="Seg_558" s="T45">гореть-PFV-PST.NAR-3SG.O</ta>
            <ta e="T47" id="Seg_559" s="T46">ягода-глаз-SNGL.[NOM]</ta>
            <ta e="T48" id="Seg_560" s="T47">вверх</ta>
            <ta e="T49" id="Seg_561" s="T48">встать-CVB</ta>
            <ta e="T50" id="Seg_562" s="T49">настолько</ta>
            <ta e="T51" id="Seg_563" s="T50">смеяться-US-PST.NAR.[3SG.S]</ta>
            <ta e="T52" id="Seg_564" s="T51">то</ta>
            <ta e="T53" id="Seg_565" s="T52">лопнуть-PFV-PST.NAR.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_566" s="T0">n-n-n&gt;n-n:case3</ta>
            <ta e="T2" id="Seg_567" s="T1">conj</ta>
            <ta e="T3" id="Seg_568" s="T2">n-n&gt;adj</ta>
            <ta e="T4" id="Seg_569" s="T3">n-n:case3</ta>
            <ta e="T5" id="Seg_570" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_571" s="T5">n-n-n&gt;n-n:case3</ta>
            <ta e="T7" id="Seg_572" s="T6">conj</ta>
            <ta e="T8" id="Seg_573" s="T7">n-n&gt;adj</ta>
            <ta e="T9" id="Seg_574" s="T8">n-n:case3</ta>
            <ta e="T10" id="Seg_575" s="T9">n-n&gt;n-n&gt;adj</ta>
            <ta e="T11" id="Seg_576" s="T10">%%</ta>
            <ta e="T12" id="Seg_577" s="T11">adv</ta>
            <ta e="T13" id="Seg_578" s="T12">v-v:pn</ta>
            <ta e="T14" id="Seg_579" s="T13">interrog</ta>
            <ta e="T15" id="Seg_580" s="T14">n-n:case3</ta>
            <ta e="T16" id="Seg_581" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_582" s="T16">num</ta>
            <ta e="T18" id="Seg_583" s="T17">n-n:advcase</ta>
            <ta e="T19" id="Seg_584" s="T18">adv</ta>
            <ta e="T20" id="Seg_585" s="T19">adv</ta>
            <ta e="T21" id="Seg_586" s="T20">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_587" s="T21">n-n-n&gt;n-n:case3</ta>
            <ta e="T23" id="Seg_588" s="T22">adv</ta>
            <ta e="T24" id="Seg_589" s="T23">v-v&gt;v-v:pn</ta>
            <ta e="T25" id="Seg_590" s="T24">n-n&gt;adj</ta>
            <ta e="T26" id="Seg_591" s="T25">n-n:case3</ta>
            <ta e="T27" id="Seg_592" s="T26">n-n:case3</ta>
            <ta e="T28" id="Seg_593" s="T27">v-v:mood-pn</ta>
            <ta e="T29" id="Seg_594" s="T28">n-n&gt;adj</ta>
            <ta e="T30" id="Seg_595" s="T29">n-n:case3</ta>
            <ta e="T31" id="Seg_596" s="T30">adv</ta>
            <ta e="T32" id="Seg_597" s="T31">v-v&gt;v-v:pn</ta>
            <ta e="T33" id="Seg_598" s="T32">pro</ta>
            <ta e="T34" id="Seg_599" s="T33">v-v:mood-pn</ta>
            <ta e="T35" id="Seg_600" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_601" s="T35">adv</ta>
            <ta e="T37" id="Seg_602" s="T36">n-n&gt;adj</ta>
            <ta e="T38" id="Seg_603" s="T37">n-n:case3</ta>
            <ta e="T39" id="Seg_604" s="T38">pro</ta>
            <ta e="T40" id="Seg_605" s="T39">preverb</ta>
            <ta e="T41" id="Seg_606" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_607" s="T41">n-n:case3</ta>
            <ta e="T43" id="Seg_608" s="T42">preverb</ta>
            <ta e="T44" id="Seg_609" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_610" s="T44">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_611" s="T45">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_612" s="T46">n-n-n&gt;n-n:case3</ta>
            <ta e="T48" id="Seg_613" s="T47">preverb</ta>
            <ta e="T49" id="Seg_614" s="T48">v-v&gt;adv</ta>
            <ta e="T50" id="Seg_615" s="T49">adv</ta>
            <ta e="T51" id="Seg_616" s="T50">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_617" s="T51">dem</ta>
            <ta e="T53" id="Seg_618" s="T52">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_619" s="T0">n</ta>
            <ta e="T2" id="Seg_620" s="T1">conj</ta>
            <ta e="T3" id="Seg_621" s="T2">adj</ta>
            <ta e="T4" id="Seg_622" s="T3">n</ta>
            <ta e="T5" id="Seg_623" s="T4">v</ta>
            <ta e="T6" id="Seg_624" s="T5">n</ta>
            <ta e="T7" id="Seg_625" s="T6">conj</ta>
            <ta e="T8" id="Seg_626" s="T7">adj</ta>
            <ta e="T9" id="Seg_627" s="T8">n</ta>
            <ta e="T10" id="Seg_628" s="T9">adj</ta>
            <ta e="T11" id="Seg_629" s="T10">%%</ta>
            <ta e="T12" id="Seg_630" s="T11">adv</ta>
            <ta e="T13" id="Seg_631" s="T12">v</ta>
            <ta e="T14" id="Seg_632" s="T13">interrog</ta>
            <ta e="T15" id="Seg_633" s="T14">n</ta>
            <ta e="T16" id="Seg_634" s="T15">v</ta>
            <ta e="T17" id="Seg_635" s="T16">num</ta>
            <ta e="T18" id="Seg_636" s="T17">adv</ta>
            <ta e="T19" id="Seg_637" s="T18">adv</ta>
            <ta e="T20" id="Seg_638" s="T19">adv</ta>
            <ta e="T21" id="Seg_639" s="T20">v</ta>
            <ta e="T22" id="Seg_640" s="T21">n</ta>
            <ta e="T23" id="Seg_641" s="T22">adv</ta>
            <ta e="T24" id="Seg_642" s="T23">v</ta>
            <ta e="T25" id="Seg_643" s="T24">adj</ta>
            <ta e="T26" id="Seg_644" s="T25">n</ta>
            <ta e="T27" id="Seg_645" s="T26">n</ta>
            <ta e="T28" id="Seg_646" s="T27">v</ta>
            <ta e="T29" id="Seg_647" s="T28">adj</ta>
            <ta e="T30" id="Seg_648" s="T29">n</ta>
            <ta e="T31" id="Seg_649" s="T30">adv</ta>
            <ta e="T32" id="Seg_650" s="T31">v</ta>
            <ta e="T33" id="Seg_651" s="T32">pro</ta>
            <ta e="T34" id="Seg_652" s="T33">v</ta>
            <ta e="T35" id="Seg_653" s="T34">v</ta>
            <ta e="T36" id="Seg_654" s="T35">adv</ta>
            <ta e="T37" id="Seg_655" s="T36">adj</ta>
            <ta e="T38" id="Seg_656" s="T37">n</ta>
            <ta e="T39" id="Seg_657" s="T38">pro</ta>
            <ta e="T40" id="Seg_658" s="T39">preverb</ta>
            <ta e="T41" id="Seg_659" s="T40">v</ta>
            <ta e="T42" id="Seg_660" s="T41">n</ta>
            <ta e="T43" id="Seg_661" s="T42">preverb</ta>
            <ta e="T44" id="Seg_662" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_663" s="T44">v</ta>
            <ta e="T46" id="Seg_664" s="T45">v</ta>
            <ta e="T47" id="Seg_665" s="T46">n</ta>
            <ta e="T48" id="Seg_666" s="T47">preverb</ta>
            <ta e="T49" id="Seg_667" s="T48">adv</ta>
            <ta e="T50" id="Seg_668" s="T49">adv</ta>
            <ta e="T51" id="Seg_669" s="T50">v</ta>
            <ta e="T52" id="Seg_670" s="T51">dem</ta>
            <ta e="T53" id="Seg_671" s="T52">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T6" id="Seg_672" s="T5">np:E</ta>
            <ta e="T9" id="Seg_673" s="T8">np:E</ta>
            <ta e="T13" id="Seg_674" s="T12">0.3:A</ta>
            <ta e="T14" id="Seg_675" s="T13">pro:A</ta>
            <ta e="T15" id="Seg_676" s="T14">np:Th</ta>
            <ta e="T18" id="Seg_677" s="T17">adv:Time</ta>
            <ta e="T21" id="Seg_678" s="T20">0.3:A</ta>
            <ta e="T22" id="Seg_679" s="T21">np:A</ta>
            <ta e="T27" id="Seg_680" s="T26">np:Th</ta>
            <ta e="T28" id="Seg_681" s="T27">0.2:A</ta>
            <ta e="T30" id="Seg_682" s="T29">np:A</ta>
            <ta e="T33" id="Seg_683" s="T32">pro:A</ta>
            <ta e="T34" id="Seg_684" s="T33">0.3:np:Th</ta>
            <ta e="T35" id="Seg_685" s="T34">0.3:A</ta>
            <ta e="T38" id="Seg_686" s="T37">np:A</ta>
            <ta e="T42" id="Seg_687" s="T41">np:Th</ta>
            <ta e="T45" id="Seg_688" s="T44">0.3:A</ta>
            <ta e="T46" id="Seg_689" s="T45">0.3:P</ta>
            <ta e="T47" id="Seg_690" s="T46">np:E</ta>
            <ta e="T53" id="Seg_691" s="T52">0.3:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T5" id="Seg_692" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_693" s="T5">np:S</ta>
            <ta e="T9" id="Seg_694" s="T8">np:S</ta>
            <ta e="T13" id="Seg_695" s="T12">0.3:S v:pred</ta>
            <ta e="T14" id="Seg_696" s="T13">pro:S</ta>
            <ta e="T15" id="Seg_697" s="T14">np:O</ta>
            <ta e="T16" id="Seg_698" s="T15">v:pred</ta>
            <ta e="T21" id="Seg_699" s="T20">0.3:S v:pred</ta>
            <ta e="T22" id="Seg_700" s="T21">np:S</ta>
            <ta e="T24" id="Seg_701" s="T23">v:pred</ta>
            <ta e="T27" id="Seg_702" s="T26">np:O</ta>
            <ta e="T28" id="Seg_703" s="T27">0.2:S v:pred</ta>
            <ta e="T30" id="Seg_704" s="T29">np:S</ta>
            <ta e="T32" id="Seg_705" s="T31">v:pred</ta>
            <ta e="T33" id="Seg_706" s="T32">pro:S</ta>
            <ta e="T34" id="Seg_707" s="T33">0.3:O v:pred</ta>
            <ta e="T35" id="Seg_708" s="T34">0.3:S v:pred</ta>
            <ta e="T38" id="Seg_709" s="T37">np:S</ta>
            <ta e="T41" id="Seg_710" s="T40">v:pred</ta>
            <ta e="T42" id="Seg_711" s="T41">np:O</ta>
            <ta e="T45" id="Seg_712" s="T44">0.3:S v:pred</ta>
            <ta e="T46" id="Seg_713" s="T45">0.3:S 0.3:O v:pred</ta>
            <ta e="T47" id="Seg_714" s="T46">np:S</ta>
            <ta e="T49" id="Seg_715" s="T48">s:adv</ta>
            <ta e="T51" id="Seg_716" s="T50">v:pred</ta>
            <ta e="T53" id="Seg_717" s="T52">0.3:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_718" s="T0">new</ta>
            <ta e="T4" id="Seg_719" s="T3">new</ta>
            <ta e="T6" id="Seg_720" s="T5">new</ta>
            <ta e="T9" id="Seg_721" s="T8">new</ta>
            <ta e="T13" id="Seg_722" s="T12">0.accs-aggr</ta>
            <ta e="T15" id="Seg_723" s="T14">accs-gen</ta>
            <ta e="T21" id="Seg_724" s="T20">0.giv-active</ta>
            <ta e="T22" id="Seg_725" s="T21">giv-active</ta>
            <ta e="T24" id="Seg_726" s="T23">quot-S</ta>
            <ta e="T26" id="Seg_727" s="T25">giv-active-Q</ta>
            <ta e="T27" id="Seg_728" s="T26">accs-gen-Q</ta>
            <ta e="T30" id="Seg_729" s="T29">giv-active</ta>
            <ta e="T32" id="Seg_730" s="T31">quot-S</ta>
            <ta e="T34" id="Seg_731" s="T33">accs-gen-Q.0</ta>
            <ta e="T35" id="Seg_732" s="T34">0.giv-inactive</ta>
            <ta e="T38" id="Seg_733" s="T37">giv-active</ta>
            <ta e="T42" id="Seg_734" s="T41">new</ta>
            <ta e="T45" id="Seg_735" s="T44">0.giv-active</ta>
            <ta e="T46" id="Seg_736" s="T45">0.giv-active</ta>
            <ta e="T47" id="Seg_737" s="T46">giv-inactive</ta>
            <ta e="T53" id="Seg_738" s="T52">0.giv-active</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_739" s="T0">Ягодка и пучок сена</ta>
            <ta e="T9" id="Seg_740" s="T4">Жили ягодка и пучок сена.</ta>
            <ta e="T13" id="Seg_741" s="T9">Каждое утро друг с другом спорят.</ta>
            <ta e="T16" id="Seg_742" s="T13">Кто будет огонь разжигать.</ta>
            <ta e="T21" id="Seg_743" s="T16">В одно утро опять друг с другом спорить стали.</ta>
            <ta e="T28" id="Seg_744" s="T21">Ягодка так говорит: "Пучок сена, разожги огонь". </ta>
            <ta e="T34" id="Seg_745" s="T28">Пучок сена так говорит: "Сама разожги".</ta>
            <ta e="T41" id="Seg_746" s="T34">Полежали, в конце концов, пучок сена сам встал.</ta>
            <ta e="T45" id="Seg_747" s="T41">Спичкой только чиркнул.</ta>
            <ta e="T46" id="Seg_748" s="T45">И сгорел. </ta>
            <ta e="T51" id="Seg_749" s="T46">Ягодка, встав, так смеялась.</ta>
            <ta e="T53" id="Seg_750" s="T51">И лопнула.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_751" s="T0">A small berry and a bunch of hay</ta>
            <ta e="T9" id="Seg_752" s="T4">There lived a small berry and a bunch of hay.</ta>
            <ta e="T13" id="Seg_753" s="T9">Every morning they argue with each other.</ta>
            <ta e="T16" id="Seg_754" s="T13">about who will set the fire.</ta>
            <ta e="T21" id="Seg_755" s="T16">One morning they started to argue again.</ta>
            <ta e="T28" id="Seg_756" s="T21">The small berry says so: "Bunch of hay, set the fire." </ta>
            <ta e="T34" id="Seg_757" s="T28">The bunch of hay says: "Do it yourself".</ta>
            <ta e="T41" id="Seg_758" s="T34">They were laying for a while, finally the bunch of hay got up alone.</ta>
            <ta e="T45" id="Seg_759" s="T41">It just struck a match. </ta>
            <ta e="T46" id="Seg_760" s="T45">And burned up.</ta>
            <ta e="T51" id="Seg_761" s="T46">The small berry got up and laughed so much, laughed so much.</ta>
            <ta e="T53" id="Seg_762" s="T51">And burst.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_763" s="T0">Eine kleine Beere und ein Heubüschel</ta>
            <ta e="T9" id="Seg_764" s="T4">Es lebten eine kleine Beere und ein Heubüschel. </ta>
            <ta e="T13" id="Seg_765" s="T9">Jeden Morgen streiten sie miteinander. </ta>
            <ta e="T16" id="Seg_766" s="T13">Darüber, wer Feuer macht. </ta>
            <ta e="T21" id="Seg_767" s="T16">Eines Morgens fingen sie wieder an, sich zu streiten. </ta>
            <ta e="T28" id="Seg_768" s="T21">Die kleine Beere sagt: "Heubüschel, mach Feuer!" </ta>
            <ta e="T34" id="Seg_769" s="T28">Das Heubüschel sagt: "Mach es selbst." </ta>
            <ta e="T41" id="Seg_770" s="T34">Sie lagen eine Weile herum, bald stand das Heubüschel selber auf. </ta>
            <ta e="T45" id="Seg_771" s="T41">Es entzündete nur ein Zündholz. </ta>
            <ta e="T46" id="Seg_772" s="T45">Und verbrannte sich. </ta>
            <ta e="T51" id="Seg_773" s="T46">Die kleine Beere stand auf und lachte so viel, lachte so viel. </ta>
            <ta e="T53" id="Seg_774" s="T51">Und platzte sie. </ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_775" s="T0">ягодка пучок сена (сплетенного как косичка)</ta>
            <ta e="T9" id="Seg_776" s="T4">‎жили ягодка и пучок сена</ta>
            <ta e="T13" id="Seg_777" s="T9">каждое утро между собой спорят</ta>
            <ta e="T16" id="Seg_778" s="T13">‎‎кому костер разжигать.</ta>
            <ta e="T21" id="Seg_779" s="T16">в одно утро опять друг с другом спорить стали</ta>
            <ta e="T28" id="Seg_780" s="T21">ягодка так говорит: пучок сена костер разведи </ta>
            <ta e="T34" id="Seg_781" s="T28">пучок сена так говорит сама разведи</ta>
            <ta e="T41" id="Seg_782" s="T34">полежали (двое), в конце концов пучок сена сам встал.</ta>
            <ta e="T45" id="Seg_783" s="T41">спички взяла</ta>
            <ta e="T46" id="Seg_784" s="T45">вспыхнуло. </ta>
            <ta e="T51" id="Seg_785" s="T46">ягодка встала до того смеялась</ta>
            <ta e="T53" id="Seg_786" s="T51">лопнула</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_787" s="T0">[OSV]: "nʼuːtɨlʼtamtɨr" - "a sheaf of hay".</ta>
            <ta e="T13" id="Seg_788" s="T9">[OSV]: The form "qarɨlʼalʼ" can be also interpreted as containing the double adjectival affix "-lʼ" with an epenthesis between.</ta>
            <ta e="T45" id="Seg_789" s="T41">[OSV]: "qättɔ:lqo" - "to strike (a match)".</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
