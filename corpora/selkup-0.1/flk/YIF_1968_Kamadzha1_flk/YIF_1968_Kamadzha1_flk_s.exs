<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDE7A06BBD-7376-205B-DAD1-7E59B0195A41">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">flk\YIF_1968_Kamadzha1_flk\YIF_1968_Kamadzha1_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">338</ud-information>
            <ud-information attribute-name="# HIAT:w">260</ud-information>
            <ud-information attribute-name="# e">260</ud-information>
            <ud-information attribute-name="# HIAT:u">54</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="YIF">
            <abbreviation>YIF</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="YIF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T260" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">šidə</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">tɨmnʼäsɨɣa</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">warkəkundaq</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">warɣe</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">timnʼä</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">nadɨmbɨlʼä</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">warga</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">ütčuga</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">tɨmnʼad</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">Qaːmaǯʼa</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">anʼǯʼad</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">tabɨp</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">obiʒajemblʼä</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">warɨt</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_56" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">timnʼasɨɣ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">tʼaŋgočukwaɣ</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_65" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_67" n="HIAT:w" s="T16">aŋə</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">qwadäšpukɨndɨdi</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_74" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_76" n="HIAT:w" s="T18">warɣə</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_79" n="HIAT:w" s="T19">timnʼädə</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_82" n="HIAT:w" s="T20">onǯe</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_85" n="HIAT:w" s="T21">pelʼgalɨk</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_88" n="HIAT:w" s="T22">qwälla</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_91" n="HIAT:w" s="T23">čaŋgɨl</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">watoɣɨnd</ts>
                  <nts id="Seg_95" n="HIAT:ip">.</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_98" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_100" n="HIAT:w" s="T25">anǯʼad</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_103" n="HIAT:w" s="T26">Qaːmačap</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_106" n="HIAT:w" s="T27">abədəqudə</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">aŋɨt</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">qoqaɣe</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">i</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">qɨmbaɣi</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_122" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">teper</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">okur</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">kudät</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">qwällaɣe</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">šedəqut</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">Qaːmačan</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T38">opti</ts>
                  <nts id="Seg_143" n="HIAT:ip">.</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_146" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_148" n="HIAT:w" s="T39">aŋgə</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_151" n="HIAT:w" s="T40">wašešpɨnda</ts>
                  <nts id="Seg_152" n="HIAT:ip">.</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_155" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_157" n="HIAT:w" s="T41">Qaːmača</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_160" n="HIAT:w" s="T42">parkwa</ts>
                  <nts id="Seg_161" n="HIAT:ip">:</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_164" n="HIAT:w" s="T43">aŋgeːe</ts>
                  <nts id="Seg_165" n="HIAT:ip">,</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_168" n="HIAT:w" s="T44">aŋgeːe</ts>
                  <nts id="Seg_169" n="HIAT:ip">!</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_172" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_174" n="HIAT:w" s="T45">qɨmbal</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_177" n="HIAT:w" s="T46">iː</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_180" n="HIAT:w" s="T47">qoqal</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_183" n="HIAT:w" s="T48">meka</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_186" n="HIAT:w" s="T49">qwäǯʼaš</ts>
                  <nts id="Seg_187" n="HIAT:ip">!</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_190" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_192" n="HIAT:w" s="T50">Tɨmnʼad</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_195" n="HIAT:w" s="T51">oqončila</ts>
                  <nts id="Seg_196" n="HIAT:ip">:</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_199" n="HIAT:w" s="T52">qajko</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_202" n="HIAT:w" s="T53">nilǯʼik</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_205" n="HIAT:w" s="T54">parqwandə</ts>
                  <nts id="Seg_206" n="HIAT:ip">?</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_209" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_211" n="HIAT:w" s="T55">a</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_214" n="HIAT:w" s="T56">Qaːmača</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_217" n="HIAT:w" s="T57">äǯalgwa</ts>
                  <nts id="Seg_218" n="HIAT:ip">:</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_221" n="HIAT:w" s="T58">maʒək</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_224" n="HIAT:w" s="T59">anʼdʼam</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_227" n="HIAT:w" s="T60">qoqaɣe</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_230" n="HIAT:w" s="T61">i</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_233" n="HIAT:w" s="T62">qɨmbaɣe</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_236" n="HIAT:w" s="T63">aːbɨtɨkwa</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_240" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_242" n="HIAT:w" s="T64">üdet</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_245" n="HIAT:w" s="T65">ugulǯe</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_248" n="HIAT:w" s="T66">töːlak</ts>
                  <nts id="Seg_249" n="HIAT:ip">.</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_252" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_254" n="HIAT:w" s="T67">Tɨmnʼadə</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_257" n="HIAT:w" s="T68">pajamdə</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_260" n="HIAT:w" s="T69">oralle</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_263" n="HIAT:w" s="T70">qatäldi</ts>
                  <nts id="Seg_264" n="HIAT:ip">.</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_267" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_269" n="HIAT:w" s="T71">talǯʼel</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_272" n="HIAT:w" s="T72">tɨmnʼäd</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_275" n="HIAT:w" s="T73">aj</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_278" n="HIAT:w" s="T74">onǯe</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_281" n="HIAT:w" s="T75">pelgalɨk</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_284" n="HIAT:w" s="T76">qwälla</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_287" n="HIAT:w" s="T77">čaŋgɨl</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_290" n="HIAT:w" s="T78">wattoɣond</ts>
                  <nts id="Seg_291" n="HIAT:ip">.</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_294" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_296" n="HIAT:w" s="T79">anǯʼa</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_299" n="HIAT:w" s="T80">oralle</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_302" n="HIAT:w" s="T81">Qaːmaǯʼap</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_305" n="HIAT:w" s="T82">qwalelde</ts>
                  <nts id="Seg_306" n="HIAT:ip">.</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_309" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_311" n="HIAT:w" s="T83">Qaːmaǯʼa</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_314" n="HIAT:w" s="T84">pon</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_317" n="HIAT:w" s="T85">čanǯʼile</ts>
                  <nts id="Seg_318" n="HIAT:ip">,</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_321" n="HIAT:w" s="T86">qwälla</ts>
                  <nts id="Seg_322" n="HIAT:ip">,</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_325" n="HIAT:w" s="T87">kuː</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_328" n="HIAT:w" s="T88">hajda</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_331" n="HIAT:w" s="T89">ada</ts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_335" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_337" n="HIAT:w" s="T90">kundə</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_340" n="HIAT:w" s="T91">lʼi</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_343" n="HIAT:w" s="T92">ilʼi</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_346" n="HIAT:w" s="T93">qaːwkak</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_349" n="HIAT:w" s="T94">čaːǯila</ts>
                  <nts id="Seg_350" n="HIAT:ip">.</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_353" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_355" n="HIAT:w" s="T95">Qonǯernɨt</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_358" n="HIAT:w" s="T96">kuläː</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_361" n="HIAT:w" s="T97">wašešpɨnda</ts>
                  <nts id="Seg_362" n="HIAT:ip">,</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_365" n="HIAT:w" s="T98">aːqandə</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_368" n="HIAT:w" s="T99">qwäl</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_371" n="HIAT:w" s="T100">taːdərɨndɨt</ts>
                  <nts id="Seg_372" n="HIAT:ip">.</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_375" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_377" n="HIAT:w" s="T101">qajmut</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_380" n="HIAT:w" s="T102">kulʼä</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_383" n="HIAT:w" s="T103">wašedʼišpa</ts>
                  <nts id="Seg_384" n="HIAT:ip">,</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_387" n="HIAT:w" s="T104">Qamača</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_390" n="HIAT:w" s="T105">nɨčid</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_393" n="HIAT:w" s="T106">ille</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_396" n="HIAT:w" s="T107">laɣalǯila</ts>
                  <nts id="Seg_397" n="HIAT:ip">.</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_400" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_402" n="HIAT:w" s="T108">meːdɨla</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_405" n="HIAT:w" s="T109">kɨge</ts>
                  <nts id="Seg_406" n="HIAT:ip">.</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_409" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_411" n="HIAT:w" s="T110">kɨgeɣɨt</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_414" n="HIAT:w" s="T111">qwäl</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_417" n="HIAT:w" s="T112">kočeɣe</ts>
                  <nts id="Seg_418" n="HIAT:ip">.</nts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_421" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_423" n="HIAT:w" s="T113">tabə</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_426" n="HIAT:w" s="T114">pačaleːlʼčildə</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_429" n="HIAT:w" s="T115">mugeːp</ts>
                  <nts id="Seg_430" n="HIAT:ip">.</nts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_433" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_435" n="HIAT:w" s="T116">kujap</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_438" n="HIAT:w" s="T117">meːlčila</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_441" n="HIAT:w" s="T118">i</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_444" n="HIAT:w" s="T119">dawaj</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_447" n="HIAT:w" s="T120">qwälɨp</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_450" n="HIAT:w" s="T121">oːɣulʼešpugu</ts>
                  <nts id="Seg_451" n="HIAT:ip">.</nts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_454" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_456" n="HIAT:w" s="T122">tüp</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_459" n="HIAT:w" s="T123">čaːdelde</ts>
                  <nts id="Seg_460" n="HIAT:ip">.</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_463" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_465" n="HIAT:w" s="T124">qaːzap</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_468" n="HIAT:w" s="T125">qare</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_471" n="HIAT:w" s="T126">maškelǯʼiɣɨlldä</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_474" n="HIAT:w" s="T127">čaːbəhe</ts>
                  <nts id="Seg_475" n="HIAT:ip">.</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_478" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_480" n="HIAT:w" s="T128">trug</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_483" n="HIAT:w" s="T129">mančeːǯʼila</ts>
                  <nts id="Seg_484" n="HIAT:ip">:</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_487" n="HIAT:w" s="T130">uːrop</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_490" n="HIAT:w" s="T131">töːšpant</ts>
                  <nts id="Seg_491" n="HIAT:ip">.</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_494" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_496" n="HIAT:w" s="T132">Qaːmača</ts>
                  <nts id="Seg_497" n="HIAT:ip">,</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_500" n="HIAT:w" s="T133">qaj</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_503" n="HIAT:w" s="T134">mešpɨndal</ts>
                  <nts id="Seg_504" n="HIAT:ip">?</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_507" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_509" n="HIAT:w" s="T135">maqqɨlm</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_512" n="HIAT:w" s="T136">tä</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_515" n="HIAT:w" s="T137">paqɨhak</ts>
                  <nts id="Seg_516" n="HIAT:ip">,</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_519" n="HIAT:w" s="T138">tün</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_522" n="HIAT:w" s="T139">čaːbɨhap</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_525" n="HIAT:w" s="T140">i</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_528" n="HIAT:w" s="T141">amgu</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_531" n="HIAT:w" s="T142">kɨgak</ts>
                  <nts id="Seg_532" n="HIAT:ip">.</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_535" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_537" n="HIAT:w" s="T143">a</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_540" n="HIAT:w" s="T144">uːrop</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_543" n="HIAT:w" s="T145">äčalgwa</ts>
                  <nts id="Seg_544" n="HIAT:ip">:</nts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_547" n="HIAT:w" s="T146">mannaj</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_550" n="HIAT:w" s="T147">qwäʒak</ts>
                  <nts id="Seg_551" n="HIAT:ip">.</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_554" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_556" n="HIAT:w" s="T148">tüt</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_559" n="HIAT:w" s="T149">taɣ</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_562" n="HIAT:w" s="T150">omdäš</ts>
                  <nts id="Seg_563" n="HIAT:ip">!</nts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_566" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_568" n="HIAT:w" s="T151">qwäl</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_571" n="HIAT:w" s="T152">perča</ts>
                  <nts id="Seg_572" n="HIAT:ip">.</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_575" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_577" n="HIAT:w" s="T153">okɨr</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_580" n="HIAT:w" s="T154">čabɨp</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_583" n="HIAT:w" s="T155">uːromn</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_586" n="HIAT:w" s="T156">čačildä</ts>
                  <nts id="Seg_587" n="HIAT:ip">.</nts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_590" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_592" n="HIAT:w" s="T157">uːrom</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_595" n="HIAT:w" s="T158">aːbɨldä</ts>
                  <nts id="Seg_596" n="HIAT:ip">,</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_599" n="HIAT:w" s="T159">äčalgwa</ts>
                  <nts id="Seg_600" n="HIAT:ip">:</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_603" n="HIAT:w" s="T160">waː</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_606" n="HIAT:w" s="T161">maqqəl</ts>
                  <nts id="Seg_607" n="HIAT:ip">!</nts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_610" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_612" n="HIAT:w" s="T162">manan</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_615" n="HIAT:w" s="T163">tä</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_618" n="HIAT:w" s="T164">paqəlǯ</ts>
                  <nts id="Seg_619" n="HIAT:ip">!</nts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_622" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_624" n="HIAT:w" s="T165">a</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_627" n="HIAT:w" s="T166">Qaːmača</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_630" n="HIAT:w" s="T167">əčalgwa</ts>
                  <nts id="Seg_631" n="HIAT:ip">:</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_634" n="HIAT:w" s="T168">tat</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_637" n="HIAT:w" s="T169">maʒek</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_640" n="HIAT:w" s="T170">aː</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_643" n="HIAT:w" s="T171">abɨlʼendə</ts>
                  <nts id="Seg_644" n="HIAT:ip">.</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_647" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_649" n="HIAT:w" s="T172">čoʒəlʼe</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_652" n="HIAT:w" s="T173">paqɨlʼešplʼebe</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_655" n="HIAT:w" s="T174">maqqəl</ts>
                  <nts id="Seg_656" n="HIAT:ip">.</nts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_659" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_661" n="HIAT:w" s="T175">uːrop</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_664" n="HIAT:w" s="T176">əčalgwa</ts>
                  <nts id="Seg_665" n="HIAT:ip">:</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_668" n="HIAT:w" s="T177">paqqəlǯ</ts>
                  <nts id="Seg_669" n="HIAT:ip">,</nts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_672" n="HIAT:w" s="T178">lattɨlʼebe</ts>
                  <nts id="Seg_673" n="HIAT:ip">.</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_676" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_678" n="HIAT:w" s="T179">Qaːmača</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_681" n="HIAT:w" s="T180">əčalgwa</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_684" n="HIAT:w" s="T181">uːromn</ts>
                  <nts id="Seg_685" n="HIAT:ip">:</nts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_688" n="HIAT:w" s="T182">qotä</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_691" n="HIAT:w" s="T183">qondäš</ts>
                  <nts id="Seg_692" n="HIAT:ip">!</nts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_695" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_697" n="HIAT:w" s="T184">uːrop</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_700" n="HIAT:w" s="T185">qondəla</ts>
                  <nts id="Seg_701" n="HIAT:ip">.</nts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_704" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_706" n="HIAT:w" s="T186">tabə</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_709" n="HIAT:w" s="T187">aːŋdol</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_712" n="HIAT:w" s="T188">paɨɣe</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_715" n="HIAT:w" s="T189">nanǯʼimd</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_718" n="HIAT:w" s="T190">tä</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_721" n="HIAT:w" s="T191">qorreǯʼilde</ts>
                  <nts id="Seg_722" n="HIAT:ip">.</nts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_725" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_727" n="HIAT:w" s="T192">i</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_730" n="HIAT:w" s="T193">uːrop</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_733" n="HIAT:w" s="T194">nɨka</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_736" n="HIAT:w" s="T195">quːla</ts>
                  <nts id="Seg_737" n="HIAT:ip">.</nts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_740" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_742" n="HIAT:w" s="T196">qaːmača</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_745" n="HIAT:w" s="T197">tä</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_748" n="HIAT:w" s="T198">qobomdə</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_751" n="HIAT:w" s="T199">kɨrɨltä</ts>
                  <nts id="Seg_752" n="HIAT:ip">,</nts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_755" n="HIAT:w" s="T200">waǯʼimdə</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_758" n="HIAT:w" s="T201">wes</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_761" n="HIAT:w" s="T202">ubiraildə</ts>
                  <nts id="Seg_762" n="HIAT:ip">,</nts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_765" n="HIAT:w" s="T203">tiril</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_768" n="HIAT:w" s="T204">koǯan</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_771" n="HIAT:w" s="T205">waǯʼip</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_774" n="HIAT:w" s="T206">palla</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_777" n="HIAT:w" s="T207">i</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_780" n="HIAT:w" s="T208">qwälɨp</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_783" n="HIAT:w" s="T209">palla</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_785" n="HIAT:ip">(</nts>
                  <ts e="T211" id="Seg_787" n="HIAT:w" s="T210">palalde</ts>
                  <nts id="Seg_788" n="HIAT:ip">)</nts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_791" n="HIAT:w" s="T211">i</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_794" n="HIAT:w" s="T212">ugulǯe</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_797" n="HIAT:w" s="T213">üppezila</ts>
                  <nts id="Seg_798" n="HIAT:ip">.</nts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_801" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_803" n="HIAT:w" s="T214">ugulǯeɣɨnd</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_806" n="HIAT:w" s="T215">medəšpa</ts>
                  <nts id="Seg_807" n="HIAT:ip">.</nts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_810" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_812" n="HIAT:w" s="T216">qašqɨdə</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_815" n="HIAT:w" s="T217">aː</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_818" n="HIAT:w" s="T218">qaškolʼba</ts>
                  <nts id="Seg_819" n="HIAT:ip">.</nts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_822" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_824" n="HIAT:w" s="T219">madə</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_827" n="HIAT:w" s="T220">parɣɨnt</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_830" n="HIAT:w" s="T221">innä</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_833" n="HIAT:w" s="T222">čarla</ts>
                  <nts id="Seg_834" n="HIAT:ip">.</nts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_837" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_839" n="HIAT:w" s="T223">šʼoːld</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_842" n="HIAT:w" s="T224">par</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_845" n="HIAT:w" s="T225">ilʼlʼe</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_848" n="HIAT:w" s="T226">mannɨmba</ts>
                  <nts id="Seg_849" n="HIAT:ip">.</nts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_852" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_854" n="HIAT:w" s="T227">tämnʼäd</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_857" n="HIAT:w" s="T228">i</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_860" n="HIAT:w" s="T229">anʼǯʼad</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_863" n="HIAT:w" s="T230">okɨr</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_866" n="HIAT:w" s="T231">tü</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_869" n="HIAT:w" s="T232">haj</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_872" n="HIAT:w" s="T233">piːdertɨdi</ts>
                  <nts id="Seg_873" n="HIAT:ip">.</nts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_876" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_878" n="HIAT:w" s="T234">tau</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_881" n="HIAT:w" s="T235">mat</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_884" n="HIAT:w" s="T236">Qaːmaǯʼam</ts>
                  <nts id="Seg_885" n="HIAT:ip">.</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_888" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_890" n="HIAT:w" s="T237">tabə</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_893" n="HIAT:w" s="T238">nɨčaut</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_896" n="HIAT:w" s="T239">ilʼlʼe</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_899" n="HIAT:w" s="T240">parǯila</ts>
                  <nts id="Seg_900" n="HIAT:ip">:</nts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_903" n="HIAT:w" s="T241">mat</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_906" n="HIAT:w" s="T242">tawejak</ts>
                  <nts id="Seg_907" n="HIAT:ip">!</nts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_910" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_912" n="HIAT:w" s="T243">nɨndə</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_915" n="HIAT:w" s="T244">tabɨstjaɣɨn</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_918" n="HIAT:w" s="T245">waǯʼep</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_921" n="HIAT:w" s="T246">i</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_924" n="HIAT:w" s="T247">qwälap</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_927" n="HIAT:w" s="T248">meːɣak</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_930" n="HIAT:w" s="T249">i</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_933" n="HIAT:w" s="T250">qwälɣe</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_936" n="HIAT:w" s="T251">aːbətɨldɨ</ts>
                  <nts id="Seg_937" n="HIAT:ip">.</nts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_940" n="HIAT:u" s="T252">
                  <ts e="T253" id="Seg_942" n="HIAT:w" s="T252">i</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_945" n="HIAT:w" s="T253">patom</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_948" n="HIAT:w" s="T254">wargelkwat</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_951" n="HIAT:w" s="T255">i</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_954" n="HIAT:w" s="T256">elelkwat</ts>
                  <nts id="Seg_955" n="HIAT:ip">,</nts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_958" n="HIAT:w" s="T257">näp</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_961" n="HIAT:w" s="T258">ip</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_964" n="HIAT:w" s="T259">qontequndat</ts>
                  <nts id="Seg_965" n="HIAT:ip">.</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T260" id="Seg_967" n="sc" s="T0">
               <ts e="T1" id="Seg_969" n="e" s="T0">šidə </ts>
               <ts e="T2" id="Seg_971" n="e" s="T1">tɨmnʼäsɨɣa </ts>
               <ts e="T3" id="Seg_973" n="e" s="T2">warkəkundaq. </ts>
               <ts e="T4" id="Seg_975" n="e" s="T3">warɣe </ts>
               <ts e="T5" id="Seg_977" n="e" s="T4">timnʼä </ts>
               <ts e="T6" id="Seg_979" n="e" s="T5">nadɨmbɨlʼä </ts>
               <ts e="T7" id="Seg_981" n="e" s="T6">warga. </ts>
               <ts e="T8" id="Seg_983" n="e" s="T7">ütčuga </ts>
               <ts e="T9" id="Seg_985" n="e" s="T8">tɨmnʼad </ts>
               <ts e="T10" id="Seg_987" n="e" s="T9">Qaːmaǯʼa. </ts>
               <ts e="T11" id="Seg_989" n="e" s="T10">anʼǯʼad </ts>
               <ts e="T12" id="Seg_991" n="e" s="T11">tabɨp </ts>
               <ts e="T13" id="Seg_993" n="e" s="T12">obiʒajemblʼä </ts>
               <ts e="T14" id="Seg_995" n="e" s="T13">warɨt. </ts>
               <ts e="T15" id="Seg_997" n="e" s="T14">timnʼasɨɣ </ts>
               <ts e="T16" id="Seg_999" n="e" s="T15">tʼaŋgočukwaɣ. </ts>
               <ts e="T17" id="Seg_1001" n="e" s="T16">aŋə </ts>
               <ts e="T18" id="Seg_1003" n="e" s="T17">qwadäšpukɨndɨdi. </ts>
               <ts e="T19" id="Seg_1005" n="e" s="T18">warɣə </ts>
               <ts e="T20" id="Seg_1007" n="e" s="T19">timnʼädə </ts>
               <ts e="T21" id="Seg_1009" n="e" s="T20">onǯe </ts>
               <ts e="T22" id="Seg_1011" n="e" s="T21">pelʼgalɨk </ts>
               <ts e="T23" id="Seg_1013" n="e" s="T22">qwälla </ts>
               <ts e="T24" id="Seg_1015" n="e" s="T23">čaŋgɨl </ts>
               <ts e="T25" id="Seg_1017" n="e" s="T24">watoɣɨnd. </ts>
               <ts e="T26" id="Seg_1019" n="e" s="T25">anǯʼad </ts>
               <ts e="T27" id="Seg_1021" n="e" s="T26">Qaːmačap </ts>
               <ts e="T28" id="Seg_1023" n="e" s="T27">abədəqudə </ts>
               <ts e="T29" id="Seg_1025" n="e" s="T28">aŋɨt </ts>
               <ts e="T30" id="Seg_1027" n="e" s="T29">qoqaɣe </ts>
               <ts e="T31" id="Seg_1029" n="e" s="T30">i </ts>
               <ts e="T32" id="Seg_1031" n="e" s="T31">qɨmbaɣi. </ts>
               <ts e="T33" id="Seg_1033" n="e" s="T32">teper </ts>
               <ts e="T34" id="Seg_1035" n="e" s="T33">okur </ts>
               <ts e="T35" id="Seg_1037" n="e" s="T34">kudät </ts>
               <ts e="T36" id="Seg_1039" n="e" s="T35">qwällaɣe </ts>
               <ts e="T37" id="Seg_1041" n="e" s="T36">šedəqut </ts>
               <ts e="T38" id="Seg_1043" n="e" s="T37">Qaːmačan </ts>
               <ts e="T39" id="Seg_1045" n="e" s="T38">opti. </ts>
               <ts e="T40" id="Seg_1047" n="e" s="T39">aŋgə </ts>
               <ts e="T41" id="Seg_1049" n="e" s="T40">wašešpɨnda. </ts>
               <ts e="T42" id="Seg_1051" n="e" s="T41">Qaːmača </ts>
               <ts e="T43" id="Seg_1053" n="e" s="T42">parkwa: </ts>
               <ts e="T44" id="Seg_1055" n="e" s="T43">aŋgeːe, </ts>
               <ts e="T45" id="Seg_1057" n="e" s="T44">aŋgeːe! </ts>
               <ts e="T46" id="Seg_1059" n="e" s="T45">qɨmbal </ts>
               <ts e="T47" id="Seg_1061" n="e" s="T46">iː </ts>
               <ts e="T48" id="Seg_1063" n="e" s="T47">qoqal </ts>
               <ts e="T49" id="Seg_1065" n="e" s="T48">meka </ts>
               <ts e="T50" id="Seg_1067" n="e" s="T49">qwäǯʼaš! </ts>
               <ts e="T51" id="Seg_1069" n="e" s="T50">Tɨmnʼad </ts>
               <ts e="T52" id="Seg_1071" n="e" s="T51">oqončila: </ts>
               <ts e="T53" id="Seg_1073" n="e" s="T52">qajko </ts>
               <ts e="T54" id="Seg_1075" n="e" s="T53">nilǯʼik </ts>
               <ts e="T55" id="Seg_1077" n="e" s="T54">parqwandə? </ts>
               <ts e="T56" id="Seg_1079" n="e" s="T55">a </ts>
               <ts e="T57" id="Seg_1081" n="e" s="T56">Qaːmača </ts>
               <ts e="T58" id="Seg_1083" n="e" s="T57">äǯalgwa: </ts>
               <ts e="T59" id="Seg_1085" n="e" s="T58">maʒək </ts>
               <ts e="T60" id="Seg_1087" n="e" s="T59">anʼdʼam </ts>
               <ts e="T61" id="Seg_1089" n="e" s="T60">qoqaɣe </ts>
               <ts e="T62" id="Seg_1091" n="e" s="T61">i </ts>
               <ts e="T63" id="Seg_1093" n="e" s="T62">qɨmbaɣe </ts>
               <ts e="T64" id="Seg_1095" n="e" s="T63">aːbɨtɨkwa. </ts>
               <ts e="T65" id="Seg_1097" n="e" s="T64">üdet </ts>
               <ts e="T66" id="Seg_1099" n="e" s="T65">ugulǯe </ts>
               <ts e="T67" id="Seg_1101" n="e" s="T66">töːlak. </ts>
               <ts e="T68" id="Seg_1103" n="e" s="T67">Tɨmnʼadə </ts>
               <ts e="T69" id="Seg_1105" n="e" s="T68">pajamdə </ts>
               <ts e="T70" id="Seg_1107" n="e" s="T69">oralle </ts>
               <ts e="T71" id="Seg_1109" n="e" s="T70">qatäldi. </ts>
               <ts e="T72" id="Seg_1111" n="e" s="T71">talǯʼel </ts>
               <ts e="T73" id="Seg_1113" n="e" s="T72">tɨmnʼäd </ts>
               <ts e="T74" id="Seg_1115" n="e" s="T73">aj </ts>
               <ts e="T75" id="Seg_1117" n="e" s="T74">onǯe </ts>
               <ts e="T76" id="Seg_1119" n="e" s="T75">pelgalɨk </ts>
               <ts e="T77" id="Seg_1121" n="e" s="T76">qwälla </ts>
               <ts e="T78" id="Seg_1123" n="e" s="T77">čaŋgɨl </ts>
               <ts e="T79" id="Seg_1125" n="e" s="T78">wattoɣond. </ts>
               <ts e="T80" id="Seg_1127" n="e" s="T79">anǯʼa </ts>
               <ts e="T81" id="Seg_1129" n="e" s="T80">oralle </ts>
               <ts e="T82" id="Seg_1131" n="e" s="T81">Qaːmaǯʼap </ts>
               <ts e="T83" id="Seg_1133" n="e" s="T82">qwalelde. </ts>
               <ts e="T84" id="Seg_1135" n="e" s="T83">Qaːmaǯʼa </ts>
               <ts e="T85" id="Seg_1137" n="e" s="T84">pon </ts>
               <ts e="T86" id="Seg_1139" n="e" s="T85">čanǯʼile, </ts>
               <ts e="T87" id="Seg_1141" n="e" s="T86">qwälla, </ts>
               <ts e="T88" id="Seg_1143" n="e" s="T87">kuː </ts>
               <ts e="T89" id="Seg_1145" n="e" s="T88">hajda </ts>
               <ts e="T90" id="Seg_1147" n="e" s="T89">ada. </ts>
               <ts e="T91" id="Seg_1149" n="e" s="T90">kundə </ts>
               <ts e="T92" id="Seg_1151" n="e" s="T91">lʼi </ts>
               <ts e="T93" id="Seg_1153" n="e" s="T92">ilʼi </ts>
               <ts e="T94" id="Seg_1155" n="e" s="T93">qaːwkak </ts>
               <ts e="T95" id="Seg_1157" n="e" s="T94">čaːǯila. </ts>
               <ts e="T96" id="Seg_1159" n="e" s="T95">Qonǯernɨt </ts>
               <ts e="T97" id="Seg_1161" n="e" s="T96">kuläː </ts>
               <ts e="T98" id="Seg_1163" n="e" s="T97">wašešpɨnda, </ts>
               <ts e="T99" id="Seg_1165" n="e" s="T98">aːqandə </ts>
               <ts e="T100" id="Seg_1167" n="e" s="T99">qwäl </ts>
               <ts e="T101" id="Seg_1169" n="e" s="T100">taːdərɨndɨt. </ts>
               <ts e="T102" id="Seg_1171" n="e" s="T101">qajmut </ts>
               <ts e="T103" id="Seg_1173" n="e" s="T102">kulʼä </ts>
               <ts e="T104" id="Seg_1175" n="e" s="T103">wašedʼišpa, </ts>
               <ts e="T105" id="Seg_1177" n="e" s="T104">Qamača </ts>
               <ts e="T106" id="Seg_1179" n="e" s="T105">nɨčid </ts>
               <ts e="T107" id="Seg_1181" n="e" s="T106">ille </ts>
               <ts e="T108" id="Seg_1183" n="e" s="T107">laɣalǯila. </ts>
               <ts e="T109" id="Seg_1185" n="e" s="T108">meːdɨla </ts>
               <ts e="T110" id="Seg_1187" n="e" s="T109">kɨge. </ts>
               <ts e="T111" id="Seg_1189" n="e" s="T110">kɨgeɣɨt </ts>
               <ts e="T112" id="Seg_1191" n="e" s="T111">qwäl </ts>
               <ts e="T113" id="Seg_1193" n="e" s="T112">kočeɣe. </ts>
               <ts e="T114" id="Seg_1195" n="e" s="T113">tabə </ts>
               <ts e="T115" id="Seg_1197" n="e" s="T114">pačaleːlʼčildə </ts>
               <ts e="T116" id="Seg_1199" n="e" s="T115">mugeːp. </ts>
               <ts e="T117" id="Seg_1201" n="e" s="T116">kujap </ts>
               <ts e="T118" id="Seg_1203" n="e" s="T117">meːlčila </ts>
               <ts e="T119" id="Seg_1205" n="e" s="T118">i </ts>
               <ts e="T120" id="Seg_1207" n="e" s="T119">dawaj </ts>
               <ts e="T121" id="Seg_1209" n="e" s="T120">qwälɨp </ts>
               <ts e="T122" id="Seg_1211" n="e" s="T121">oːɣulʼešpugu. </ts>
               <ts e="T123" id="Seg_1213" n="e" s="T122">tüp </ts>
               <ts e="T124" id="Seg_1215" n="e" s="T123">čaːdelde. </ts>
               <ts e="T125" id="Seg_1217" n="e" s="T124">qaːzap </ts>
               <ts e="T126" id="Seg_1219" n="e" s="T125">qare </ts>
               <ts e="T127" id="Seg_1221" n="e" s="T126">maškelǯʼiɣɨlldä </ts>
               <ts e="T128" id="Seg_1223" n="e" s="T127">čaːbəhe. </ts>
               <ts e="T129" id="Seg_1225" n="e" s="T128">trug </ts>
               <ts e="T130" id="Seg_1227" n="e" s="T129">mančeːǯʼila: </ts>
               <ts e="T131" id="Seg_1229" n="e" s="T130">uːrop </ts>
               <ts e="T132" id="Seg_1231" n="e" s="T131">töːšpant. </ts>
               <ts e="T133" id="Seg_1233" n="e" s="T132">Qaːmača, </ts>
               <ts e="T134" id="Seg_1235" n="e" s="T133">qaj </ts>
               <ts e="T135" id="Seg_1237" n="e" s="T134">mešpɨndal? </ts>
               <ts e="T136" id="Seg_1239" n="e" s="T135">maqqɨlm </ts>
               <ts e="T137" id="Seg_1241" n="e" s="T136">tä </ts>
               <ts e="T138" id="Seg_1243" n="e" s="T137">paqɨhak, </ts>
               <ts e="T139" id="Seg_1245" n="e" s="T138">tün </ts>
               <ts e="T140" id="Seg_1247" n="e" s="T139">čaːbɨhap </ts>
               <ts e="T141" id="Seg_1249" n="e" s="T140">i </ts>
               <ts e="T142" id="Seg_1251" n="e" s="T141">amgu </ts>
               <ts e="T143" id="Seg_1253" n="e" s="T142">kɨgak. </ts>
               <ts e="T144" id="Seg_1255" n="e" s="T143">a </ts>
               <ts e="T145" id="Seg_1257" n="e" s="T144">uːrop </ts>
               <ts e="T146" id="Seg_1259" n="e" s="T145">äčalgwa: </ts>
               <ts e="T147" id="Seg_1261" n="e" s="T146">mannaj </ts>
               <ts e="T148" id="Seg_1263" n="e" s="T147">qwäʒak. </ts>
               <ts e="T149" id="Seg_1265" n="e" s="T148">tüt </ts>
               <ts e="T150" id="Seg_1267" n="e" s="T149">taɣ </ts>
               <ts e="T151" id="Seg_1269" n="e" s="T150">omdäš! </ts>
               <ts e="T152" id="Seg_1271" n="e" s="T151">qwäl </ts>
               <ts e="T153" id="Seg_1273" n="e" s="T152">perča. </ts>
               <ts e="T154" id="Seg_1275" n="e" s="T153">okɨr </ts>
               <ts e="T155" id="Seg_1277" n="e" s="T154">čabɨp </ts>
               <ts e="T156" id="Seg_1279" n="e" s="T155">uːromn </ts>
               <ts e="T157" id="Seg_1281" n="e" s="T156">čačildä. </ts>
               <ts e="T158" id="Seg_1283" n="e" s="T157">uːrom </ts>
               <ts e="T159" id="Seg_1285" n="e" s="T158">aːbɨldä, </ts>
               <ts e="T160" id="Seg_1287" n="e" s="T159">äčalgwa: </ts>
               <ts e="T161" id="Seg_1289" n="e" s="T160">waː </ts>
               <ts e="T162" id="Seg_1291" n="e" s="T161">maqqəl! </ts>
               <ts e="T163" id="Seg_1293" n="e" s="T162">manan </ts>
               <ts e="T164" id="Seg_1295" n="e" s="T163">tä </ts>
               <ts e="T165" id="Seg_1297" n="e" s="T164">paqəlǯ! </ts>
               <ts e="T166" id="Seg_1299" n="e" s="T165">a </ts>
               <ts e="T167" id="Seg_1301" n="e" s="T166">Qaːmača </ts>
               <ts e="T168" id="Seg_1303" n="e" s="T167">əčalgwa: </ts>
               <ts e="T169" id="Seg_1305" n="e" s="T168">tat </ts>
               <ts e="T170" id="Seg_1307" n="e" s="T169">maʒek </ts>
               <ts e="T171" id="Seg_1309" n="e" s="T170">aː </ts>
               <ts e="T172" id="Seg_1311" n="e" s="T171">abɨlʼendə. </ts>
               <ts e="T173" id="Seg_1313" n="e" s="T172">čoʒəlʼe </ts>
               <ts e="T174" id="Seg_1315" n="e" s="T173">paqɨlʼešplʼebe </ts>
               <ts e="T175" id="Seg_1317" n="e" s="T174">maqqəl. </ts>
               <ts e="T176" id="Seg_1319" n="e" s="T175">uːrop </ts>
               <ts e="T177" id="Seg_1321" n="e" s="T176">əčalgwa: </ts>
               <ts e="T178" id="Seg_1323" n="e" s="T177">paqqəlǯ, </ts>
               <ts e="T179" id="Seg_1325" n="e" s="T178">lattɨlʼebe. </ts>
               <ts e="T180" id="Seg_1327" n="e" s="T179">Qaːmača </ts>
               <ts e="T181" id="Seg_1329" n="e" s="T180">əčalgwa </ts>
               <ts e="T182" id="Seg_1331" n="e" s="T181">uːromn: </ts>
               <ts e="T183" id="Seg_1333" n="e" s="T182">qotä </ts>
               <ts e="T184" id="Seg_1335" n="e" s="T183">qondäš! </ts>
               <ts e="T185" id="Seg_1337" n="e" s="T184">uːrop </ts>
               <ts e="T186" id="Seg_1339" n="e" s="T185">qondəla. </ts>
               <ts e="T187" id="Seg_1341" n="e" s="T186">tabə </ts>
               <ts e="T188" id="Seg_1343" n="e" s="T187">aːŋdol </ts>
               <ts e="T189" id="Seg_1345" n="e" s="T188">paɨɣe </ts>
               <ts e="T190" id="Seg_1347" n="e" s="T189">nanǯʼimd </ts>
               <ts e="T191" id="Seg_1349" n="e" s="T190">tä </ts>
               <ts e="T192" id="Seg_1351" n="e" s="T191">qorreǯʼilde. </ts>
               <ts e="T193" id="Seg_1353" n="e" s="T192">i </ts>
               <ts e="T194" id="Seg_1355" n="e" s="T193">uːrop </ts>
               <ts e="T195" id="Seg_1357" n="e" s="T194">nɨka </ts>
               <ts e="T196" id="Seg_1359" n="e" s="T195">quːla. </ts>
               <ts e="T197" id="Seg_1361" n="e" s="T196">qaːmača </ts>
               <ts e="T198" id="Seg_1363" n="e" s="T197">tä </ts>
               <ts e="T199" id="Seg_1365" n="e" s="T198">qobomdə </ts>
               <ts e="T200" id="Seg_1367" n="e" s="T199">kɨrɨltä, </ts>
               <ts e="T201" id="Seg_1369" n="e" s="T200">waǯʼimdə </ts>
               <ts e="T202" id="Seg_1371" n="e" s="T201">wes </ts>
               <ts e="T203" id="Seg_1373" n="e" s="T202">ubiraildə, </ts>
               <ts e="T204" id="Seg_1375" n="e" s="T203">tiril </ts>
               <ts e="T205" id="Seg_1377" n="e" s="T204">koǯan </ts>
               <ts e="T206" id="Seg_1379" n="e" s="T205">waǯʼip </ts>
               <ts e="T207" id="Seg_1381" n="e" s="T206">palla </ts>
               <ts e="T208" id="Seg_1383" n="e" s="T207">i </ts>
               <ts e="T209" id="Seg_1385" n="e" s="T208">qwälɨp </ts>
               <ts e="T210" id="Seg_1387" n="e" s="T209">palla </ts>
               <ts e="T211" id="Seg_1389" n="e" s="T210">(palalde) </ts>
               <ts e="T212" id="Seg_1391" n="e" s="T211">i </ts>
               <ts e="T213" id="Seg_1393" n="e" s="T212">ugulǯe </ts>
               <ts e="T214" id="Seg_1395" n="e" s="T213">üppezila. </ts>
               <ts e="T215" id="Seg_1397" n="e" s="T214">ugulǯeɣɨnd </ts>
               <ts e="T216" id="Seg_1399" n="e" s="T215">medəšpa. </ts>
               <ts e="T217" id="Seg_1401" n="e" s="T216">qašqɨdə </ts>
               <ts e="T218" id="Seg_1403" n="e" s="T217">aː </ts>
               <ts e="T219" id="Seg_1405" n="e" s="T218">qaškolʼba. </ts>
               <ts e="T220" id="Seg_1407" n="e" s="T219">madə </ts>
               <ts e="T221" id="Seg_1409" n="e" s="T220">parɣɨnt </ts>
               <ts e="T222" id="Seg_1411" n="e" s="T221">innä </ts>
               <ts e="T223" id="Seg_1413" n="e" s="T222">čarla. </ts>
               <ts e="T224" id="Seg_1415" n="e" s="T223">šʼoːld </ts>
               <ts e="T225" id="Seg_1417" n="e" s="T224">par </ts>
               <ts e="T226" id="Seg_1419" n="e" s="T225">ilʼlʼe </ts>
               <ts e="T227" id="Seg_1421" n="e" s="T226">mannɨmba. </ts>
               <ts e="T228" id="Seg_1423" n="e" s="T227">tämnʼäd </ts>
               <ts e="T229" id="Seg_1425" n="e" s="T228">i </ts>
               <ts e="T230" id="Seg_1427" n="e" s="T229">anʼǯʼad </ts>
               <ts e="T231" id="Seg_1429" n="e" s="T230">okɨr </ts>
               <ts e="T232" id="Seg_1431" n="e" s="T231">tü </ts>
               <ts e="T233" id="Seg_1433" n="e" s="T232">haj </ts>
               <ts e="T234" id="Seg_1435" n="e" s="T233">piːdertɨdi. </ts>
               <ts e="T235" id="Seg_1437" n="e" s="T234">tau </ts>
               <ts e="T236" id="Seg_1439" n="e" s="T235">mat </ts>
               <ts e="T237" id="Seg_1441" n="e" s="T236">Qaːmaǯʼam. </ts>
               <ts e="T238" id="Seg_1443" n="e" s="T237">tabə </ts>
               <ts e="T239" id="Seg_1445" n="e" s="T238">nɨčaut </ts>
               <ts e="T240" id="Seg_1447" n="e" s="T239">ilʼlʼe </ts>
               <ts e="T241" id="Seg_1449" n="e" s="T240">parǯila: </ts>
               <ts e="T242" id="Seg_1451" n="e" s="T241">mat </ts>
               <ts e="T243" id="Seg_1453" n="e" s="T242">tawejak! </ts>
               <ts e="T244" id="Seg_1455" n="e" s="T243">nɨndə </ts>
               <ts e="T245" id="Seg_1457" n="e" s="T244">tabɨstjaɣɨn </ts>
               <ts e="T246" id="Seg_1459" n="e" s="T245">waǯʼep </ts>
               <ts e="T247" id="Seg_1461" n="e" s="T246">i </ts>
               <ts e="T248" id="Seg_1463" n="e" s="T247">qwälap </ts>
               <ts e="T249" id="Seg_1465" n="e" s="T248">meːɣak </ts>
               <ts e="T250" id="Seg_1467" n="e" s="T249">i </ts>
               <ts e="T251" id="Seg_1469" n="e" s="T250">qwälɣe </ts>
               <ts e="T252" id="Seg_1471" n="e" s="T251">aːbətɨldɨ. </ts>
               <ts e="T253" id="Seg_1473" n="e" s="T252">i </ts>
               <ts e="T254" id="Seg_1475" n="e" s="T253">patom </ts>
               <ts e="T255" id="Seg_1477" n="e" s="T254">wargelkwat </ts>
               <ts e="T256" id="Seg_1479" n="e" s="T255">i </ts>
               <ts e="T257" id="Seg_1481" n="e" s="T256">elelkwat, </ts>
               <ts e="T258" id="Seg_1483" n="e" s="T257">näp </ts>
               <ts e="T259" id="Seg_1485" n="e" s="T258">ip </ts>
               <ts e="T260" id="Seg_1487" n="e" s="T259">qontequndat. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_1488" s="T0">YIF_1968_Kamadzha1_flk.001 (001.001)</ta>
            <ta e="T7" id="Seg_1489" s="T3">YIF_1968_Kamadzha1_flk.002 (001.002)</ta>
            <ta e="T10" id="Seg_1490" s="T7">YIF_1968_Kamadzha1_flk.003 (001.003)</ta>
            <ta e="T14" id="Seg_1491" s="T10">YIF_1968_Kamadzha1_flk.004 (001.004)</ta>
            <ta e="T16" id="Seg_1492" s="T14">YIF_1968_Kamadzha1_flk.005 (001.005)</ta>
            <ta e="T18" id="Seg_1493" s="T16">YIF_1968_Kamadzha1_flk.006 (001.006)</ta>
            <ta e="T25" id="Seg_1494" s="T18">YIF_1968_Kamadzha1_flk.007 (001.007)</ta>
            <ta e="T32" id="Seg_1495" s="T25">YIF_1968_Kamadzha1_flk.008 (001.008)</ta>
            <ta e="T39" id="Seg_1496" s="T32">YIF_1968_Kamadzha1_flk.009 (001.009)</ta>
            <ta e="T41" id="Seg_1497" s="T39">YIF_1968_Kamadzha1_flk.010 (001.010)</ta>
            <ta e="T45" id="Seg_1498" s="T41">YIF_1968_Kamadzha1_flk.011 (001.011)</ta>
            <ta e="T50" id="Seg_1499" s="T45">YIF_1968_Kamadzha1_flk.012 (001.012)</ta>
            <ta e="T55" id="Seg_1500" s="T50">YIF_1968_Kamadzha1_flk.013 (001.013)</ta>
            <ta e="T64" id="Seg_1501" s="T55">YIF_1968_Kamadzha1_flk.014 (001.014)</ta>
            <ta e="T67" id="Seg_1502" s="T64">YIF_1968_Kamadzha1_flk.015 (001.015)</ta>
            <ta e="T71" id="Seg_1503" s="T67">YIF_1968_Kamadzha1_flk.016 (001.016)</ta>
            <ta e="T79" id="Seg_1504" s="T71">YIF_1968_Kamadzha1_flk.017 (001.017)</ta>
            <ta e="T83" id="Seg_1505" s="T79">YIF_1968_Kamadzha1_flk.018 (001.018)</ta>
            <ta e="T90" id="Seg_1506" s="T83">YIF_1968_Kamadzha1_flk.019 (001.019)</ta>
            <ta e="T95" id="Seg_1507" s="T90">YIF_1968_Kamadzha1_flk.020 (001.020)</ta>
            <ta e="T101" id="Seg_1508" s="T95">YIF_1968_Kamadzha1_flk.021 (001.021)</ta>
            <ta e="T108" id="Seg_1509" s="T101">YIF_1968_Kamadzha1_flk.022 (001.022)</ta>
            <ta e="T110" id="Seg_1510" s="T108">YIF_1968_Kamadzha1_flk.023 (001.023)</ta>
            <ta e="T113" id="Seg_1511" s="T110">YIF_1968_Kamadzha1_flk.024 (001.024)</ta>
            <ta e="T116" id="Seg_1512" s="T113">YIF_1968_Kamadzha1_flk.025 (001.025)</ta>
            <ta e="T122" id="Seg_1513" s="T116">YIF_1968_Kamadzha1_flk.026 (001.026)</ta>
            <ta e="T124" id="Seg_1514" s="T122">YIF_1968_Kamadzha1_flk.027 (001.027)</ta>
            <ta e="T128" id="Seg_1515" s="T124">YIF_1968_Kamadzha1_flk.028 (001.028)</ta>
            <ta e="T132" id="Seg_1516" s="T128">YIF_1968_Kamadzha1_flk.029 (001.029)</ta>
            <ta e="T135" id="Seg_1517" s="T132">YIF_1968_Kamadzha1_flk.030 (001.030)</ta>
            <ta e="T143" id="Seg_1518" s="T135">YIF_1968_Kamadzha1_flk.031 (001.031)</ta>
            <ta e="T148" id="Seg_1519" s="T143">YIF_1968_Kamadzha1_flk.032 (001.032)</ta>
            <ta e="T151" id="Seg_1520" s="T148">YIF_1968_Kamadzha1_flk.033 (001.033)</ta>
            <ta e="T153" id="Seg_1521" s="T151">YIF_1968_Kamadzha1_flk.034 (001.034)</ta>
            <ta e="T157" id="Seg_1522" s="T153">YIF_1968_Kamadzha1_flk.035 (001.035)</ta>
            <ta e="T162" id="Seg_1523" s="T157">YIF_1968_Kamadzha1_flk.036 (001.036)</ta>
            <ta e="T165" id="Seg_1524" s="T162">YIF_1968_Kamadzha1_flk.037 (001.037)</ta>
            <ta e="T172" id="Seg_1525" s="T165">YIF_1968_Kamadzha1_flk.038 (001.038)</ta>
            <ta e="T175" id="Seg_1526" s="T172">YIF_1968_Kamadzha1_flk.039 (001.039)</ta>
            <ta e="T179" id="Seg_1527" s="T175">YIF_1968_Kamadzha1_flk.040 (001.040)</ta>
            <ta e="T184" id="Seg_1528" s="T179">YIF_1968_Kamadzha1_flk.041 (001.041)</ta>
            <ta e="T186" id="Seg_1529" s="T184">YIF_1968_Kamadzha1_flk.042 (001.042)</ta>
            <ta e="T192" id="Seg_1530" s="T186">YIF_1968_Kamadzha1_flk.043 (001.043)</ta>
            <ta e="T196" id="Seg_1531" s="T192">YIF_1968_Kamadzha1_flk.044 (001.044)</ta>
            <ta e="T214" id="Seg_1532" s="T196">YIF_1968_Kamadzha1_flk.045 (001.045)</ta>
            <ta e="T216" id="Seg_1533" s="T214">YIF_1968_Kamadzha1_flk.046 (001.046)</ta>
            <ta e="T219" id="Seg_1534" s="T216">YIF_1968_Kamadzha1_flk.047 (001.047)</ta>
            <ta e="T223" id="Seg_1535" s="T219">YIF_1968_Kamadzha1_flk.048 (001.048)</ta>
            <ta e="T227" id="Seg_1536" s="T223">YIF_1968_Kamadzha1_flk.049 (001.049)</ta>
            <ta e="T234" id="Seg_1537" s="T227">YIF_1968_Kamadzha1_flk.050 (001.050)</ta>
            <ta e="T237" id="Seg_1538" s="T234">YIF_1968_Kamadzha1_flk.051 (001.051)</ta>
            <ta e="T243" id="Seg_1539" s="T237">YIF_1968_Kamadzha1_flk.052 (001.052)</ta>
            <ta e="T252" id="Seg_1540" s="T243">YIF_1968_Kamadzha1_flk.053 (001.053)</ta>
            <ta e="T260" id="Seg_1541" s="T252">YIF_1968_Kamadzha1_flk.054 (001.054)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_1542" s="T0">шидъ тымнʼäɣa варкъкундаk.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_1543" s="T0">šidə tɨmnʼäsɨɣa warkəkundaq</ta>
            <ta e="T7" id="Seg_1544" s="T3">warɣe timnä nadɨmbɨlʼä warga.</ta>
            <ta e="T10" id="Seg_1545" s="T7">ütčuga tɨmnʼad Qaːmaǯʼa.</ta>
            <ta e="T14" id="Seg_1546" s="T10">anʼǯʼad tabɨp obiʒajemblʼä warɨt.</ta>
            <ta e="T16" id="Seg_1547" s="T14">timnʼasɨɣ tʼ(č)aŋgočukwaɣ. </ta>
            <ta e="T18" id="Seg_1548" s="T16">aŋə qwadäšpukɨndɨdi.</ta>
            <ta e="T25" id="Seg_1549" s="T18">warɣə timnʼädə onǯe pelʼgalɨk qwälla čaŋgɨl watoɣɨnd.</ta>
            <ta e="T32" id="Seg_1550" s="T25">anǯʼad Qaːmačap abədəqudə aŋɨt qoqaɣe i qɨmbaɣi.</ta>
            <ta e="T39" id="Seg_1551" s="T32">teper okur kudät qwällaɣe šedəqut qaːmača nopti.</ta>
            <ta e="T41" id="Seg_1552" s="T39">aŋgə wašešpɨnda</ta>
            <ta e="T45" id="Seg_1553" s="T41">qaːmača parkwa: aŋgeːe, aŋgeːe</ta>
            <ta e="T50" id="Seg_1554" s="T45">q(k)ɨmbal iː qoqal meka qwäǯʼaš</ta>
            <ta e="T55" id="Seg_1555" s="T50">tɨmnʼad oqončila: qajko nilǯʼik parqwandə</ta>
            <ta e="T64" id="Seg_1556" s="T55">a qaːmača äǯalgwa maʒək anʼdʼam qoqaɣe i qɨmbaɣe aːbɨtɨkwa</ta>
            <ta e="T67" id="Seg_1557" s="T64">üdet ugulǯe töːlak. </ta>
            <ta e="T71" id="Seg_1558" s="T67">tɨmnʼadə pajamdə oralle qatäldi. </ta>
            <ta e="T79" id="Seg_1559" s="T71">talǯʼel tɨmnʼäd aj onǯe pelgalɨk qwälla čaŋgɨl wattoɣond</ta>
            <ta e="T83" id="Seg_1560" s="T79">anǯʼa oralle Qaːmaǯʼap qwalelde</ta>
            <ta e="T90" id="Seg_1561" s="T83">qaːmaǯʼa pon čanǯʼile, qwälla, kuː hajda ada</ta>
            <ta e="T95" id="Seg_1562" s="T90">kundə lʼi ilʼi qaːwkak čaːǯila.</ta>
            <ta e="T101" id="Seg_1563" s="T95">qonǯernɨt kuläː wašešpɨnda aːqandə qwäl taːdərɨndɨt.</ta>
            <ta e="T108" id="Seg_1564" s="T101">qajmut kulʼä wašedʼišpa, qamača nɨčid ille laɣalǯila. </ta>
            <ta e="T110" id="Seg_1565" s="T108">meːdɨla kɨge</ta>
            <ta e="T113" id="Seg_1566" s="T110">kɨgeɣɨt qwäl kočeɣe</ta>
            <ta e="T122" id="Seg_1567" s="T116">kujap meːlčila, i dawaj qwälɨp oːɣulʼešpugu. </ta>
            <ta e="T124" id="Seg_1568" s="T122">tüp čaːdelde</ta>
            <ta e="T128" id="Seg_1569" s="T124">qaːzap qare maškelǯʼiɣɨldä čaːbəhe</ta>
            <ta e="T143" id="Seg_1570" s="T135">maqqɨlm tä paqɨ(ə)pak, tün čaːbɨ(ə)hap i amgu kɨgak</ta>
            <ta e="T148" id="Seg_1571" s="T143">a uːrop ä(ɨ)čalgwa mannaj qwäʒak.</ta>
            <ta e="T151" id="Seg_1572" s="T148">tüt taɣ omdäš!</ta>
            <ta e="T153" id="Seg_1573" s="T151">qwäl perča</ta>
            <ta e="T157" id="Seg_1574" s="T153">okɨr čabɨp uːromn čačildä.</ta>
            <ta e="T162" id="Seg_1575" s="T157">uːrom aːbɨldä, äčalgwa: waː maqqəl! </ta>
            <ta e="T165" id="Seg_1576" s="T162">nanan tä paq(e)əlǯ!</ta>
            <ta e="T172" id="Seg_1577" s="T165">a qaːmača ə(o)čalgwa tat maʒek aː abɨlʼendə. </ta>
            <ta e="T175" id="Seg_1578" s="T172">čoʒ(ə)lʼe paqɨlʼešplʼebe maqqəl.</ta>
            <ta e="T179" id="Seg_1579" s="T175">uːrop ə(o)čalgwa: paqqəlǯ, lattɨlʼebe.</ta>
            <ta e="T184" id="Seg_1580" s="T179">qaːmača ə(o)čalgwa uːromn qotä qondäš!</ta>
            <ta e="T186" id="Seg_1581" s="T184">uːrop qondəla.</ta>
            <ta e="T192" id="Seg_1582" s="T186">tabə aːŋdol paɨɣe nanǯʼimd tä qorreǯʼilde</ta>
            <ta e="T196" id="Seg_1583" s="T192">i uːrop nɨka quːla. </ta>
            <ta e="T214" id="Seg_1584" s="T196">qaːmača tä qobomdə kɨrɨltä, waǯʼimdə wes ubiraildə, tiril koǯan waǯʼip palla i qwälɨp palla (palalde) i ugulǯe üppezila.</ta>
            <ta e="T216" id="Seg_1585" s="T214">ugulǯeɣɨnd medəšpa.</ta>
            <ta e="T219" id="Seg_1586" s="T216">qašqɨdə aː qaškolʼba. </ta>
            <ta e="T223" id="Seg_1587" s="T219">madə parɣɨnt i(e)nnä čarla. </ta>
            <ta e="T227" id="Seg_1588" s="T223">šʼoːld par ilʼlʼe mannɨmba</ta>
            <ta e="T234" id="Seg_1589" s="T227">Tämnʼäd i anʼǯʼad okɨr tü haj piːdertɨdi.</ta>
            <ta e="T237" id="Seg_1590" s="T234">tau mat Qaːmaǯʼam.</ta>
            <ta e="T243" id="Seg_1591" s="T237">tabə nɨčaut ilʼlʼe parǯila: mat tawejak! </ta>
            <ta e="T252" id="Seg_1592" s="T243">nɨndə tabɨstjaɣɨn waǯʼep i qwälap meːɣak i qwälɣe aːbətɨldɨ. </ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_1593" s="T0">šidə tɨmnʼäsɨɣa warkəkundaq. </ta>
            <ta e="T7" id="Seg_1594" s="T3">warɣe timnʼä nadɨmbɨlʼä warga. </ta>
            <ta e="T10" id="Seg_1595" s="T7">ütčuga tɨmnʼad Qaːmaǯʼa. </ta>
            <ta e="T14" id="Seg_1596" s="T10">anʼǯʼad tabɨp obiʒajemblʼä warɨt. </ta>
            <ta e="T16" id="Seg_1597" s="T14">timnʼasɨɣ tʼaŋgočukwaɣ. </ta>
            <ta e="T18" id="Seg_1598" s="T16">aŋə qwadäšpukɨndɨdi. </ta>
            <ta e="T25" id="Seg_1599" s="T18">warɣə timnʼädə onǯe pelʼgalɨk qwälla čaŋgɨl watoɣɨnd. </ta>
            <ta e="T32" id="Seg_1600" s="T25">anǯʼad Qaːmačap abədəqudə aŋɨt qoqaɣe i qɨmbaɣi. </ta>
            <ta e="T39" id="Seg_1601" s="T32">teper okur kudät qwällaɣe šedəqut Qaːmačan opti. </ta>
            <ta e="T41" id="Seg_1602" s="T39">aŋgə wašešpɨnda. </ta>
            <ta e="T45" id="Seg_1603" s="T41">Qaːmača parkwa: aŋgeːe, aŋgeːe! </ta>
            <ta e="T50" id="Seg_1604" s="T45">qɨmbal iː qoqal meka qwäǯʼaš! </ta>
            <ta e="T55" id="Seg_1605" s="T50">Tɨmnʼad oqončila: qajko nilǯʼik parqwandə? </ta>
            <ta e="T64" id="Seg_1606" s="T55">a Qaːmača äǯalgwa: maʒək anʼdʼam qoqaɣe i qɨmbaɣe aːbɨtɨkwa. </ta>
            <ta e="T67" id="Seg_1607" s="T64">üdet ugulǯe töːlak. </ta>
            <ta e="T71" id="Seg_1608" s="T67">Tɨmnʼadə pajamdə oralle qatäldi. </ta>
            <ta e="T79" id="Seg_1609" s="T71">talǯʼel tɨmnʼäd aj onǯe pelgalɨk qwälla čaŋgɨl wattoɣond. </ta>
            <ta e="T83" id="Seg_1610" s="T79">anǯʼa oralle Qaːmaǯʼap qwalelde. </ta>
            <ta e="T90" id="Seg_1611" s="T83">Qaːmaǯʼa pon čanǯʼile, qwälla, kuː hajda ada. </ta>
            <ta e="T95" id="Seg_1612" s="T90">kundə lʼi ilʼi qaːwkak čaːǯila. </ta>
            <ta e="T101" id="Seg_1613" s="T95">Qonǯernɨt kuläː wašešpɨnda, aːqandə qwäl taːdərɨndɨt. </ta>
            <ta e="T108" id="Seg_1614" s="T101">qajmut kulʼä wašedʼišpa, Qamača nɨčid ille laɣalǯila. </ta>
            <ta e="T110" id="Seg_1615" s="T108">meːdɨla kɨge. </ta>
            <ta e="T113" id="Seg_1616" s="T110">kɨgeɣɨt qwäl kočeɣe. </ta>
            <ta e="T116" id="Seg_1617" s="T113">tabə pačaleːlʼčildə mugeːp. </ta>
            <ta e="T122" id="Seg_1618" s="T116">kujap meːlčila i dawaj qwälɨp oːɣulʼešpugu. </ta>
            <ta e="T124" id="Seg_1619" s="T122">tüp čaːdelde. </ta>
            <ta e="T128" id="Seg_1620" s="T124">qaːzap qare maškelǯʼiɣɨlldä čaːbəhe. </ta>
            <ta e="T132" id="Seg_1621" s="T128">trug mančeːǯʼila: uːrop töːšpant. </ta>
            <ta e="T135" id="Seg_1622" s="T132">Qaːmača, qaj mešpɨndal? </ta>
            <ta e="T143" id="Seg_1623" s="T135">maqqɨlm tä paqɨhak, tün čaːbɨhap i amgu kɨgak. </ta>
            <ta e="T148" id="Seg_1624" s="T143">a uːrop äčalgwa: mannaj qwäʒak. </ta>
            <ta e="T151" id="Seg_1625" s="T148">tüt taɣ omdäš! </ta>
            <ta e="T153" id="Seg_1626" s="T151">qwäl perča. </ta>
            <ta e="T157" id="Seg_1627" s="T153">okɨr čabɨp uːromn čačildä. </ta>
            <ta e="T162" id="Seg_1628" s="T157">uːrom aːbɨldä, äčalgwa: waː maqqəl! </ta>
            <ta e="T165" id="Seg_1629" s="T162">manan tä paqəlǯ! </ta>
            <ta e="T172" id="Seg_1630" s="T165">a Qaːmača əčalgwa: tat maʒek aː abɨlʼendə. </ta>
            <ta e="T175" id="Seg_1631" s="T172">čoʒəlʼe paqɨlʼešplʼebe maqqəl. </ta>
            <ta e="T179" id="Seg_1632" s="T175">uːrop əčalgwa: paqqəlǯ, lattɨlʼebe. </ta>
            <ta e="T184" id="Seg_1633" s="T179">Qaːmača əčalgwa uːromn: qotä qondäš! </ta>
            <ta e="T186" id="Seg_1634" s="T184">uːrop qondəla. </ta>
            <ta e="T192" id="Seg_1635" s="T186">tabə aːŋdol paɨɣe nanǯʼimd tä qorreǯʼilde. </ta>
            <ta e="T196" id="Seg_1636" s="T192">i uːrop nɨka quːla. </ta>
            <ta e="T214" id="Seg_1637" s="T196">qaːmača tä qobomdə kɨrɨltä, waǯʼimdə wes ubiraildə, tiril koǯan waǯʼip palla i qwälɨp palla (/palalde) i ugulǯe üppezila. </ta>
            <ta e="T216" id="Seg_1638" s="T214">ugulǯeɣɨnd medəšpa. </ta>
            <ta e="T219" id="Seg_1639" s="T216">qašqɨdə aː qaškolʼba. </ta>
            <ta e="T223" id="Seg_1640" s="T219">madə parɣɨnt innä čarla. </ta>
            <ta e="T227" id="Seg_1641" s="T223">šʼoːld par ilʼlʼe mannɨmba. </ta>
            <ta e="T234" id="Seg_1642" s="T227">tämnʼäd i anʼǯʼad okɨr tü haj piːdertɨdi. </ta>
            <ta e="T237" id="Seg_1643" s="T234">tau mat Qaːmaǯʼam. </ta>
            <ta e="T243" id="Seg_1644" s="T237">tabə nɨčaut ilʼlʼe parǯila: mat tawejak! </ta>
            <ta e="T252" id="Seg_1645" s="T243">nɨndə tabɨstjaɣɨn waǯʼep i qwälap meːɣak i qwälɣe aːbətɨldɨ. </ta>
            <ta e="T260" id="Seg_1646" s="T252">i patom wargelkwat i elelkwat, näp ip qontequndat. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1647" s="T0">šidə</ta>
            <ta e="T2" id="Seg_1648" s="T1">tɨmnʼä-sɨ-ɣa</ta>
            <ta e="T3" id="Seg_1649" s="T2">warkə-ku-ndɨ-q</ta>
            <ta e="T4" id="Seg_1650" s="T3">warɣe</ta>
            <ta e="T5" id="Seg_1651" s="T4">timnʼä</ta>
            <ta e="T6" id="Seg_1652" s="T5">nadɨ-mbɨ-lʼä</ta>
            <ta e="T7" id="Seg_1653" s="T6">warga</ta>
            <ta e="T8" id="Seg_1654" s="T7">ütčuga</ta>
            <ta e="T9" id="Seg_1655" s="T8">tɨmnʼa-d</ta>
            <ta e="T10" id="Seg_1656" s="T9">Qaːmaǯʼa</ta>
            <ta e="T11" id="Seg_1657" s="T10">anʼǯʼa-d</ta>
            <ta e="T12" id="Seg_1658" s="T11">tab-ɨ-p</ta>
            <ta e="T13" id="Seg_1659" s="T12">obiʒa-je-mb-lʼä</ta>
            <ta e="T14" id="Seg_1660" s="T13">warɨ-t</ta>
            <ta e="T15" id="Seg_1661" s="T14">timnʼa-sɨ-ɣ</ta>
            <ta e="T16" id="Seg_1662" s="T15">tʼaŋgo-ču-k-wa-ɣ</ta>
            <ta e="T17" id="Seg_1663" s="T16">aŋə</ta>
            <ta e="T18" id="Seg_1664" s="T17">qwad-ä-špu-kɨ-nda-di</ta>
            <ta e="T19" id="Seg_1665" s="T18">warɣə</ta>
            <ta e="T20" id="Seg_1666" s="T19">timnʼä-də</ta>
            <ta e="T21" id="Seg_1667" s="T20">onǯe</ta>
            <ta e="T22" id="Seg_1668" s="T21">pelʼ-galɨ-k</ta>
            <ta e="T23" id="Seg_1669" s="T22">qwäl-la</ta>
            <ta e="T24" id="Seg_1670" s="T23">čaŋg-ɨ-lʼ</ta>
            <ta e="T25" id="Seg_1671" s="T24">wat-o-ɣɨn-d</ta>
            <ta e="T26" id="Seg_1672" s="T25">anǯʼa-d</ta>
            <ta e="T27" id="Seg_1673" s="T26">Qaːmača-p</ta>
            <ta e="T28" id="Seg_1674" s="T27">ab-ə-də-qu-də</ta>
            <ta e="T29" id="Seg_1675" s="T28">aŋɨ-t</ta>
            <ta e="T30" id="Seg_1676" s="T29">qoqa-ɣe</ta>
            <ta e="T31" id="Seg_1677" s="T30">i</ta>
            <ta e="T32" id="Seg_1678" s="T31">qɨmba-ɣi</ta>
            <ta e="T33" id="Seg_1679" s="T32">teper</ta>
            <ta e="T34" id="Seg_1680" s="T33">okur</ta>
            <ta e="T35" id="Seg_1681" s="T34">kud-ä-t</ta>
            <ta e="T36" id="Seg_1682" s="T35">qwäl-le-ɣe</ta>
            <ta e="T37" id="Seg_1683" s="T36">šedə-qu-t</ta>
            <ta e="T38" id="Seg_1684" s="T37">Qaːmača-n</ta>
            <ta e="T39" id="Seg_1685" s="T38">opti</ta>
            <ta e="T40" id="Seg_1686" s="T39">aŋgə</ta>
            <ta e="T41" id="Seg_1687" s="T40">waše-špɨ-nda</ta>
            <ta e="T42" id="Seg_1688" s="T41">Qaːmača</ta>
            <ta e="T43" id="Seg_1689" s="T42">park-wa</ta>
            <ta e="T44" id="Seg_1690" s="T43">aŋge-ee</ta>
            <ta e="T45" id="Seg_1691" s="T44">aŋge-ee</ta>
            <ta e="T46" id="Seg_1692" s="T45">qɨmba-l</ta>
            <ta e="T47" id="Seg_1693" s="T46">iː</ta>
            <ta e="T48" id="Seg_1694" s="T47">qoqa-l</ta>
            <ta e="T49" id="Seg_1695" s="T48">meka</ta>
            <ta e="T50" id="Seg_1696" s="T49">qwäǯʼa-š</ta>
            <ta e="T51" id="Seg_1697" s="T50">tɨmnʼa-d</ta>
            <ta e="T52" id="Seg_1698" s="T51">oqo-nči-la</ta>
            <ta e="T53" id="Seg_1699" s="T52">qaj-ko</ta>
            <ta e="T54" id="Seg_1700" s="T53">nilǯʼi-k</ta>
            <ta e="T55" id="Seg_1701" s="T54">parq-wa-ndə</ta>
            <ta e="T56" id="Seg_1702" s="T55">a</ta>
            <ta e="T57" id="Seg_1703" s="T56">Qaːmača</ta>
            <ta e="T58" id="Seg_1704" s="T57">äǯal-g-wa</ta>
            <ta e="T59" id="Seg_1705" s="T58">maʒək</ta>
            <ta e="T60" id="Seg_1706" s="T59">anʼdʼa-m</ta>
            <ta e="T61" id="Seg_1707" s="T60">qoqa-ɣe</ta>
            <ta e="T62" id="Seg_1708" s="T61">i</ta>
            <ta e="T63" id="Seg_1709" s="T62">qɨmba-ɣe</ta>
            <ta e="T64" id="Seg_1710" s="T63">aːb-ɨ-tɨ-k-wa</ta>
            <ta e="T65" id="Seg_1711" s="T64">üde-t</ta>
            <ta e="T66" id="Seg_1712" s="T65">ugulǯe</ta>
            <ta e="T67" id="Seg_1713" s="T66">töː-le-k</ta>
            <ta e="T68" id="Seg_1714" s="T67">tɨmnʼa-də</ta>
            <ta e="T69" id="Seg_1715" s="T68">paja-m-də</ta>
            <ta e="T70" id="Seg_1716" s="T69">oral-le</ta>
            <ta e="T71" id="Seg_1717" s="T70">qatä-lə-di</ta>
            <ta e="T72" id="Seg_1718" s="T71">talǯʼel</ta>
            <ta e="T73" id="Seg_1719" s="T72">tɨmnʼä-d</ta>
            <ta e="T74" id="Seg_1720" s="T73">aj</ta>
            <ta e="T75" id="Seg_1721" s="T74">onǯe</ta>
            <ta e="T76" id="Seg_1722" s="T75">pel-galɨ-k</ta>
            <ta e="T77" id="Seg_1723" s="T76">qwäl-la</ta>
            <ta e="T78" id="Seg_1724" s="T77">čaŋg-ɨ-lʼ</ta>
            <ta e="T79" id="Seg_1725" s="T78">watto-ɣon-d</ta>
            <ta e="T80" id="Seg_1726" s="T79">anǯʼa</ta>
            <ta e="T81" id="Seg_1727" s="T80">oral-le</ta>
            <ta e="T82" id="Seg_1728" s="T81">Qaːmaǯʼa-p</ta>
            <ta e="T83" id="Seg_1729" s="T82">qwa-lel-de</ta>
            <ta e="T84" id="Seg_1730" s="T83">Qaːmaǯʼa</ta>
            <ta e="T85" id="Seg_1731" s="T84">po-n</ta>
            <ta e="T86" id="Seg_1732" s="T85">čanǯʼi-le</ta>
            <ta e="T87" id="Seg_1733" s="T86">qwäl-la</ta>
            <ta e="T88" id="Seg_1734" s="T87">kuː</ta>
            <ta e="T89" id="Seg_1735" s="T88">haj-da</ta>
            <ta e="T90" id="Seg_1736" s="T89">ada</ta>
            <ta e="T91" id="Seg_1737" s="T90">kundə</ta>
            <ta e="T92" id="Seg_1738" s="T91">lʼi</ta>
            <ta e="T93" id="Seg_1739" s="T92">ilʼi</ta>
            <ta e="T94" id="Seg_1740" s="T93">qaːwka-k</ta>
            <ta e="T95" id="Seg_1741" s="T94">čaːǯi-le</ta>
            <ta e="T96" id="Seg_1742" s="T95">qo-nǯe-r-nɨ-t</ta>
            <ta e="T97" id="Seg_1743" s="T96">kuläː</ta>
            <ta e="T98" id="Seg_1744" s="T97">waše-špɨ-ndɨ</ta>
            <ta e="T99" id="Seg_1745" s="T98">aːq-a-ndə</ta>
            <ta e="T100" id="Seg_1746" s="T99">qwäl</ta>
            <ta e="T101" id="Seg_1747" s="T100">taːdə-r-ɨ-nda-t</ta>
            <ta e="T102" id="Seg_1748" s="T101">qaj-mut</ta>
            <ta e="T103" id="Seg_1749" s="T102">kulʼä</ta>
            <ta e="T104" id="Seg_1750" s="T103">waše-dʼi-špa</ta>
            <ta e="T105" id="Seg_1751" s="T104">Qamača</ta>
            <ta e="T106" id="Seg_1752" s="T105">nɨčid</ta>
            <ta e="T107" id="Seg_1753" s="T106">ille</ta>
            <ta e="T108" id="Seg_1754" s="T107">laɣa-lǯi-la</ta>
            <ta e="T109" id="Seg_1755" s="T108">meːdɨ-la</ta>
            <ta e="T110" id="Seg_1756" s="T109">kɨ-gǝ</ta>
            <ta e="T111" id="Seg_1757" s="T110">kɨ-ge-ɣɨt</ta>
            <ta e="T112" id="Seg_1758" s="T111">qwäl</ta>
            <ta e="T113" id="Seg_1759" s="T112">koče-ɣe</ta>
            <ta e="T114" id="Seg_1760" s="T113">tabə</ta>
            <ta e="T115" id="Seg_1761" s="T114">pačal-eː-lʼči-l-də</ta>
            <ta e="T116" id="Seg_1762" s="T115">mugeː-p</ta>
            <ta e="T117" id="Seg_1763" s="T116">kuja-p</ta>
            <ta e="T118" id="Seg_1764" s="T117">meː-lči-la</ta>
            <ta e="T119" id="Seg_1765" s="T118">i</ta>
            <ta e="T120" id="Seg_1766" s="T119">dawaj</ta>
            <ta e="T121" id="Seg_1767" s="T120">qwälɨ-p</ta>
            <ta e="T122" id="Seg_1768" s="T121">oːɣu-lʼe-špu-gu</ta>
            <ta e="T123" id="Seg_1769" s="T122">tü-p</ta>
            <ta e="T124" id="Seg_1770" s="T123">čaːde-lə-de</ta>
            <ta e="T125" id="Seg_1771" s="T124">qaːza-p</ta>
            <ta e="T126" id="Seg_1772" s="T125">qare</ta>
            <ta e="T127" id="Seg_1773" s="T126">maške-lǯʼi-ɣɨl-l-dä</ta>
            <ta e="T128" id="Seg_1774" s="T127">čaːbə-he</ta>
            <ta e="T129" id="Seg_1775" s="T128">trug</ta>
            <ta e="T130" id="Seg_1776" s="T129">mančeː-ǯʼi-le</ta>
            <ta e="T131" id="Seg_1777" s="T130">uːrop</ta>
            <ta e="T132" id="Seg_1778" s="T131">töː-špa-nt</ta>
            <ta e="T133" id="Seg_1779" s="T132">Qaːmača</ta>
            <ta e="T134" id="Seg_1780" s="T133">qaj</ta>
            <ta e="T135" id="Seg_1781" s="T134">me-špɨ-nda-l</ta>
            <ta e="T136" id="Seg_1782" s="T135">maqqɨl-m</ta>
            <ta e="T137" id="Seg_1783" s="T136">tä</ta>
            <ta e="T138" id="Seg_1784" s="T137">paqɨh-a-k</ta>
            <ta e="T139" id="Seg_1785" s="T138">tü-n</ta>
            <ta e="T140" id="Seg_1786" s="T139">čaːbɨ-ha-p</ta>
            <ta e="T141" id="Seg_1787" s="T140">i</ta>
            <ta e="T142" id="Seg_1788" s="T141">am-gu</ta>
            <ta e="T143" id="Seg_1789" s="T142">kɨga-k</ta>
            <ta e="T144" id="Seg_1790" s="T143">a</ta>
            <ta e="T145" id="Seg_1791" s="T144">uːrop</ta>
            <ta e="T146" id="Seg_1792" s="T145">äča-lə-g-wa</ta>
            <ta e="T147" id="Seg_1793" s="T146">man-naj</ta>
            <ta e="T148" id="Seg_1794" s="T147">qwäʒa-k</ta>
            <ta e="T149" id="Seg_1795" s="T148">tü-t</ta>
            <ta e="T150" id="Seg_1796" s="T149">taɣ</ta>
            <ta e="T151" id="Seg_1797" s="T150">omdä-š</ta>
            <ta e="T152" id="Seg_1798" s="T151">qwäl</ta>
            <ta e="T153" id="Seg_1799" s="T152">per-ča</ta>
            <ta e="T154" id="Seg_1800" s="T153">okɨr</ta>
            <ta e="T155" id="Seg_1801" s="T154">čab-ɨ-p</ta>
            <ta e="T156" id="Seg_1802" s="T155">uːrom-n</ta>
            <ta e="T157" id="Seg_1803" s="T156">čači-l-dä</ta>
            <ta e="T158" id="Seg_1804" s="T157">uːrom</ta>
            <ta e="T159" id="Seg_1805" s="T158">aːb-ɨ-l-dä</ta>
            <ta e="T160" id="Seg_1806" s="T159">äča-lə-g-wa</ta>
            <ta e="T161" id="Seg_1807" s="T160">waː</ta>
            <ta e="T162" id="Seg_1808" s="T161">maqqəl</ta>
            <ta e="T163" id="Seg_1809" s="T162">ma-nan</ta>
            <ta e="T164" id="Seg_1810" s="T163">tä</ta>
            <ta e="T165" id="Seg_1811" s="T164">paqəl-ǯ</ta>
            <ta e="T166" id="Seg_1812" s="T165">a</ta>
            <ta e="T167" id="Seg_1813" s="T166">Qaːmača</ta>
            <ta e="T168" id="Seg_1814" s="T167">əča-lə-g-wa</ta>
            <ta e="T169" id="Seg_1815" s="T168">tat</ta>
            <ta e="T170" id="Seg_1816" s="T169">maʒek</ta>
            <ta e="T171" id="Seg_1817" s="T170">aː</ta>
            <ta e="T172" id="Seg_1818" s="T171">ab-ɨ-lʼe-ndə</ta>
            <ta e="T173" id="Seg_1819" s="T172">čoʒəlʼe</ta>
            <ta e="T174" id="Seg_1820" s="T173">paqɨlʼ-e-šp-lʼe-be</ta>
            <ta e="T175" id="Seg_1821" s="T174">maqqəl</ta>
            <ta e="T176" id="Seg_1822" s="T175">uːrop</ta>
            <ta e="T177" id="Seg_1823" s="T176">əča-lə-g-wa</ta>
            <ta e="T178" id="Seg_1824" s="T177">paqqəl-ǯ</ta>
            <ta e="T179" id="Seg_1825" s="T178">lattɨ-lʼe-be</ta>
            <ta e="T180" id="Seg_1826" s="T179">Qaːmača</ta>
            <ta e="T181" id="Seg_1827" s="T180">əča-lə-g-wa</ta>
            <ta e="T182" id="Seg_1828" s="T181">uːrom-n</ta>
            <ta e="T183" id="Seg_1829" s="T182">qotä</ta>
            <ta e="T184" id="Seg_1830" s="T183">qond-äš</ta>
            <ta e="T185" id="Seg_1831" s="T184">uːrop</ta>
            <ta e="T186" id="Seg_1832" s="T185">qondə-la</ta>
            <ta e="T187" id="Seg_1833" s="T186">tabə</ta>
            <ta e="T188" id="Seg_1834" s="T187">aːŋdol</ta>
            <ta e="T189" id="Seg_1835" s="T188">paɨ-ɣe</ta>
            <ta e="T190" id="Seg_1836" s="T189">nanǯʼi-m-d</ta>
            <ta e="T191" id="Seg_1837" s="T190">tä</ta>
            <ta e="T192" id="Seg_1838" s="T191">qorre-ǯʼi-l-de</ta>
            <ta e="T193" id="Seg_1839" s="T192">i</ta>
            <ta e="T194" id="Seg_1840" s="T193">uːrop</ta>
            <ta e="T195" id="Seg_1841" s="T194">nɨka</ta>
            <ta e="T196" id="Seg_1842" s="T195">quː-le</ta>
            <ta e="T197" id="Seg_1843" s="T196">Qaːmača</ta>
            <ta e="T198" id="Seg_1844" s="T197">tä</ta>
            <ta e="T199" id="Seg_1845" s="T198">qobo-m-də</ta>
            <ta e="T200" id="Seg_1846" s="T199">kɨrɨ-ltä</ta>
            <ta e="T201" id="Seg_1847" s="T200">waǯʼi-m-də</ta>
            <ta e="T202" id="Seg_1848" s="T201">wes</ta>
            <ta e="T203" id="Seg_1849" s="T202">ubirai-lə-də</ta>
            <ta e="T204" id="Seg_1850" s="T203">tiri-lʼ</ta>
            <ta e="T205" id="Seg_1851" s="T204">koǯa-n</ta>
            <ta e="T206" id="Seg_1852" s="T205">waǯʼi-p</ta>
            <ta e="T207" id="Seg_1853" s="T206">pal-le</ta>
            <ta e="T208" id="Seg_1854" s="T207">i</ta>
            <ta e="T209" id="Seg_1855" s="T208">qwälɨ-p</ta>
            <ta e="T210" id="Seg_1856" s="T209">pal-le</ta>
            <ta e="T211" id="Seg_1857" s="T210">pal-a-l-de</ta>
            <ta e="T212" id="Seg_1858" s="T211">i</ta>
            <ta e="T213" id="Seg_1859" s="T212">ugulǯe</ta>
            <ta e="T214" id="Seg_1860" s="T213">üppe-zi-le</ta>
            <ta e="T215" id="Seg_1861" s="T214">ugulǯe-ɣɨn-d</ta>
            <ta e="T216" id="Seg_1862" s="T215">medə-špa</ta>
            <ta e="T217" id="Seg_1863" s="T216">qašqɨ-də</ta>
            <ta e="T218" id="Seg_1864" s="T217">aː</ta>
            <ta e="T219" id="Seg_1865" s="T218">qaškolʼ-ba</ta>
            <ta e="T220" id="Seg_1866" s="T219">madə</ta>
            <ta e="T221" id="Seg_1867" s="T220">par-ɣɨnt</ta>
            <ta e="T222" id="Seg_1868" s="T221">innä</ta>
            <ta e="T223" id="Seg_1869" s="T222">čar-le</ta>
            <ta e="T224" id="Seg_1870" s="T223">šʼoːl-d</ta>
            <ta e="T225" id="Seg_1871" s="T224">par</ta>
            <ta e="T226" id="Seg_1872" s="T225">ilʼlʼe</ta>
            <ta e="T227" id="Seg_1873" s="T226">mannɨ-mba</ta>
            <ta e="T228" id="Seg_1874" s="T227">tämnʼä-d</ta>
            <ta e="T229" id="Seg_1875" s="T228">i</ta>
            <ta e="T230" id="Seg_1876" s="T229">anǯʼa-d</ta>
            <ta e="T231" id="Seg_1877" s="T230">okɨr</ta>
            <ta e="T232" id="Seg_1878" s="T231">tü</ta>
            <ta e="T233" id="Seg_1879" s="T232">haj</ta>
            <ta e="T234" id="Seg_1880" s="T233">piːder-tɨ-di</ta>
            <ta e="T235" id="Seg_1881" s="T234">tau</ta>
            <ta e="T236" id="Seg_1882" s="T235">mat</ta>
            <ta e="T237" id="Seg_1883" s="T236">Qaːmaǯʼa-m</ta>
            <ta e="T238" id="Seg_1884" s="T237">tabə</ta>
            <ta e="T239" id="Seg_1885" s="T238">nɨča-ut</ta>
            <ta e="T240" id="Seg_1886" s="T239">ilʼlʼe</ta>
            <ta e="T241" id="Seg_1887" s="T240">parǯi-la</ta>
            <ta e="T242" id="Seg_1888" s="T241">mat</ta>
            <ta e="T243" id="Seg_1889" s="T242">tawe-ja-k</ta>
            <ta e="T244" id="Seg_1890" s="T243">nɨndə</ta>
            <ta e="T245" id="Seg_1891" s="T244">tab-ɨ-stja-ɣɨn</ta>
            <ta e="T246" id="Seg_1892" s="T245">waǯʼe-p</ta>
            <ta e="T247" id="Seg_1893" s="T246">i</ta>
            <ta e="T248" id="Seg_1894" s="T247">qwäla-p</ta>
            <ta e="T249" id="Seg_1895" s="T248">meː-ɣa-k</ta>
            <ta e="T250" id="Seg_1896" s="T249">i</ta>
            <ta e="T251" id="Seg_1897" s="T250">qwäl-ɣe</ta>
            <ta e="T252" id="Seg_1898" s="T251">aːb-ə-tɨ-l-dɨ</ta>
            <ta e="T253" id="Seg_1899" s="T252">i</ta>
            <ta e="T254" id="Seg_1900" s="T253">patom</ta>
            <ta e="T255" id="Seg_1901" s="T254">warge-lə-k-wa-t</ta>
            <ta e="T256" id="Seg_1902" s="T255">i</ta>
            <ta e="T257" id="Seg_1903" s="T256">ele-lə-k-wa-t</ta>
            <ta e="T258" id="Seg_1904" s="T257">nä-p</ta>
            <ta e="T259" id="Seg_1905" s="T258">i-p</ta>
            <ta e="T260" id="Seg_1906" s="T259">qonte-qu-ndɨ-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1907" s="T0">šitə</ta>
            <ta e="T2" id="Seg_1908" s="T1">temnʼa-sɨ-qi</ta>
            <ta e="T3" id="Seg_1909" s="T2">wargɨ-ku-ndɨ-q</ta>
            <ta e="T4" id="Seg_1910" s="T3">wargɨ</ta>
            <ta e="T5" id="Seg_1911" s="T4">temnʼa</ta>
            <ta e="T6" id="Seg_1912" s="T5">nadɨ-mbɨ-le</ta>
            <ta e="T7" id="Seg_1913" s="T6">wargɨ</ta>
            <ta e="T8" id="Seg_1914" s="T7">üčega</ta>
            <ta e="T9" id="Seg_1915" s="T8">temnʼa-t</ta>
            <ta e="T10" id="Seg_1916" s="T9">Qaːmaǯʼa</ta>
            <ta e="T11" id="Seg_1917" s="T10">anǯʼa-t</ta>
            <ta e="T12" id="Seg_1918" s="T11">tab-ɨ-m</ta>
            <ta e="T13" id="Seg_1919" s="T12">abiʒaː-je-mbɨ-le</ta>
            <ta e="T14" id="Seg_1920" s="T13">warɨ-t</ta>
            <ta e="T15" id="Seg_1921" s="T14">temnʼa-sɨ-qi</ta>
            <ta e="T16" id="Seg_1922" s="T15">tʼaŋgo-ču-kku-ŋɨ-q</ta>
            <ta e="T17" id="Seg_1923" s="T16">aŋa</ta>
            <ta e="T18" id="Seg_1924" s="T17">kwat-ɨ-špɨ-kku-ndɨ-di</ta>
            <ta e="T19" id="Seg_1925" s="T18">wargɨ</ta>
            <ta e="T20" id="Seg_1926" s="T19">temnʼa-t</ta>
            <ta e="T21" id="Seg_1927" s="T20">onǯe</ta>
            <ta e="T22" id="Seg_1928" s="T21">pelka-galɨ-k</ta>
            <ta e="T23" id="Seg_1929" s="T22">qwan-le</ta>
            <ta e="T24" id="Seg_1930" s="T23">čaŋg-ɨ-lʼ</ta>
            <ta e="T25" id="Seg_1931" s="T24">wat-ɨ-ɣɨn-t</ta>
            <ta e="T26" id="Seg_1932" s="T25">anǯʼa-t</ta>
            <ta e="T27" id="Seg_1933" s="T26">Qaːmaǯʼa-p</ta>
            <ta e="T28" id="Seg_1934" s="T27">am-ɨ-dɨ-ku-t</ta>
            <ta e="T29" id="Seg_1935" s="T28">aŋa-n</ta>
            <ta e="T30" id="Seg_1936" s="T29">qoqa-se</ta>
            <ta e="T31" id="Seg_1937" s="T30">i</ta>
            <ta e="T32" id="Seg_1938" s="T31">qɨmba-se</ta>
            <ta e="T33" id="Seg_1939" s="T32">taper</ta>
            <ta e="T34" id="Seg_1940" s="T33">okkər</ta>
            <ta e="T35" id="Seg_1941" s="T34">kutɨ-ɨ-n</ta>
            <ta e="T36" id="Seg_1942" s="T35">qwan-le-q</ta>
            <ta e="T37" id="Seg_1943" s="T36">šitə-qum-t</ta>
            <ta e="T38" id="Seg_1944" s="T37">Qaːmaǯʼa-n</ta>
            <ta e="T39" id="Seg_1945" s="T38">opti</ta>
            <ta e="T40" id="Seg_1946" s="T39">aŋa</ta>
            <ta e="T41" id="Seg_1947" s="T40">waše-špɨ-ndɨ</ta>
            <ta e="T42" id="Seg_1948" s="T41">Qaːmaǯʼa</ta>
            <ta e="T43" id="Seg_1949" s="T42">parka-wa</ta>
            <ta e="T44" id="Seg_1950" s="T43">aŋa-ee</ta>
            <ta e="T45" id="Seg_1951" s="T44">aŋa-ee</ta>
            <ta e="T46" id="Seg_1952" s="T45">qɨmba-l</ta>
            <ta e="T47" id="Seg_1953" s="T46">i</ta>
            <ta e="T48" id="Seg_1954" s="T47">qoqa-l</ta>
            <ta e="T49" id="Seg_1955" s="T48">mäkkä</ta>
            <ta e="T50" id="Seg_1956" s="T49">kwešɨ-äšɨk</ta>
            <ta e="T51" id="Seg_1957" s="T50">temnʼa-t</ta>
            <ta e="T52" id="Seg_1958" s="T51">oqo-nǯe-le</ta>
            <ta e="T53" id="Seg_1959" s="T52">qaj-tqo</ta>
            <ta e="T54" id="Seg_1960" s="T53">nɨlʼǯi-k</ta>
            <ta e="T55" id="Seg_1961" s="T54">parka-ŋɨ-nd</ta>
            <ta e="T56" id="Seg_1962" s="T55">a</ta>
            <ta e="T57" id="Seg_1963" s="T56">Qaːmaǯʼa</ta>
            <ta e="T58" id="Seg_1964" s="T57">ɛːǯal-ku-ŋɨ</ta>
            <ta e="T59" id="Seg_1965" s="T58">maᴣik</ta>
            <ta e="T60" id="Seg_1966" s="T59">anǯʼa-mɨ</ta>
            <ta e="T61" id="Seg_1967" s="T60">qoqa-se</ta>
            <ta e="T62" id="Seg_1968" s="T61">i</ta>
            <ta e="T63" id="Seg_1969" s="T62">qɨmba-se</ta>
            <ta e="T64" id="Seg_1970" s="T63">am-ɨ-dɨ-kku-ŋɨ</ta>
            <ta e="T65" id="Seg_1971" s="T64">üːtǝ-n</ta>
            <ta e="T66" id="Seg_1972" s="T65">sugulʼǯʼe</ta>
            <ta e="T67" id="Seg_1973" s="T66">töː-la-q</ta>
            <ta e="T68" id="Seg_1974" s="T67">temnʼa-t</ta>
            <ta e="T69" id="Seg_1975" s="T68">paja-m-t</ta>
            <ta e="T70" id="Seg_1976" s="T69">oral-le</ta>
            <ta e="T71" id="Seg_1977" s="T70">kettɨ-lə-t</ta>
            <ta e="T72" id="Seg_1978" s="T71">talǯʼel</ta>
            <ta e="T73" id="Seg_1979" s="T72">temnʼa-t</ta>
            <ta e="T74" id="Seg_1980" s="T73">aj</ta>
            <ta e="T75" id="Seg_1981" s="T74">onǯe</ta>
            <ta e="T76" id="Seg_1982" s="T75">pelka-galɨ-k</ta>
            <ta e="T77" id="Seg_1983" s="T76">qwan-la</ta>
            <ta e="T78" id="Seg_1984" s="T77">čaŋg-ɨ-lʼ</ta>
            <ta e="T79" id="Seg_1985" s="T78">wat-ɣɨn-t</ta>
            <ta e="T80" id="Seg_1986" s="T79">anǯʼa</ta>
            <ta e="T81" id="Seg_1987" s="T80">oral-le</ta>
            <ta e="T82" id="Seg_1988" s="T81">Qaːmaǯʼa-m</ta>
            <ta e="T83" id="Seg_1989" s="T82">kwat-lel-t</ta>
            <ta e="T84" id="Seg_1990" s="T83">Qaːmaǯʼa</ta>
            <ta e="T85" id="Seg_1991" s="T84">po-nde</ta>
            <ta e="T86" id="Seg_1992" s="T85">čanǯe-le</ta>
            <ta e="T87" id="Seg_1993" s="T86">qwan-le</ta>
            <ta e="T88" id="Seg_1994" s="T87">ku</ta>
            <ta e="T89" id="Seg_1995" s="T88">haj-t</ta>
            <ta e="T90" id="Seg_1996" s="T89">adɨ</ta>
            <ta e="T91" id="Seg_1997" s="T90">kundɨ</ta>
            <ta e="T92" id="Seg_1998" s="T91">lʼi</ta>
            <ta e="T93" id="Seg_1999" s="T92">ali</ta>
            <ta e="T94" id="Seg_2000" s="T93">qaːwka-k</ta>
            <ta e="T95" id="Seg_2001" s="T94">čaːǯɨ-le</ta>
            <ta e="T96" id="Seg_2002" s="T95">qo-nǯe-r-ŋɨ-t</ta>
            <ta e="T97" id="Seg_2003" s="T96">kuläː</ta>
            <ta e="T98" id="Seg_2004" s="T97">waše-špɨ-ndɨ</ta>
            <ta e="T99" id="Seg_2005" s="T98">ak-ɨ-nde</ta>
            <ta e="T100" id="Seg_2006" s="T99">kwel</ta>
            <ta e="T101" id="Seg_2007" s="T100">tade-r-ɨ-ndɨ-t</ta>
            <ta e="T102" id="Seg_2008" s="T101">qaj-mun</ta>
            <ta e="T103" id="Seg_2009" s="T102">kuläː</ta>
            <ta e="T104" id="Seg_2010" s="T103">waše-dʼi-špɨ</ta>
            <ta e="T105" id="Seg_2011" s="T104">Qaːmaǯʼa</ta>
            <ta e="T106" id="Seg_2012" s="T105">načidelʼi</ta>
            <ta e="T107" id="Seg_2013" s="T106">illä</ta>
            <ta e="T108" id="Seg_2014" s="T107">laɣe-lʼčǝ-la</ta>
            <ta e="T109" id="Seg_2015" s="T108">medɨ-la</ta>
            <ta e="T110" id="Seg_2016" s="T109">kɨ-ka</ta>
            <ta e="T111" id="Seg_2017" s="T110">kɨ-ka-ɣɨt</ta>
            <ta e="T112" id="Seg_2018" s="T111">kwel</ta>
            <ta e="T113" id="Seg_2019" s="T112">koče-ɣe</ta>
            <ta e="T114" id="Seg_2020" s="T113">tab</ta>
            <ta e="T115" id="Seg_2021" s="T114">pačal-ɨ-lʼčǝ-lɨ-t</ta>
            <ta e="T116" id="Seg_2022" s="T115">mugeː-m</ta>
            <ta e="T117" id="Seg_2023" s="T116">kuja-p</ta>
            <ta e="T118" id="Seg_2024" s="T117">me-lʼčǝ-la</ta>
            <ta e="T119" id="Seg_2025" s="T118">i</ta>
            <ta e="T120" id="Seg_2026" s="T119">dawaj</ta>
            <ta e="T121" id="Seg_2027" s="T120">kwel-p</ta>
            <ta e="T122" id="Seg_2028" s="T121">oːɣu-lɨ-špɨ-gu</ta>
            <ta e="T123" id="Seg_2029" s="T122">tüː-m</ta>
            <ta e="T124" id="Seg_2030" s="T123">čadu-lə-t</ta>
            <ta e="T125" id="Seg_2031" s="T124">qaza-p</ta>
            <ta e="T126" id="Seg_2032" s="T125">qarɨ</ta>
            <ta e="T127" id="Seg_2033" s="T126">maške-lʼčǝ-qəl-lɨ-t</ta>
            <ta e="T128" id="Seg_2034" s="T127">čaːbə-se</ta>
            <ta e="T129" id="Seg_2035" s="T128">trug</ta>
            <ta e="T130" id="Seg_2036" s="T129">manǯu-lʼčǝ-la</ta>
            <ta e="T131" id="Seg_2037" s="T130">surup</ta>
            <ta e="T132" id="Seg_2038" s="T131">töː-špɨ-ndɨ</ta>
            <ta e="T133" id="Seg_2039" s="T132">Qaːmaǯʼa</ta>
            <ta e="T134" id="Seg_2040" s="T133">qaj</ta>
            <ta e="T135" id="Seg_2041" s="T134">me-špɨ-ndɨ-l</ta>
            <ta e="T136" id="Seg_2042" s="T135">maqqɨl-mɨ</ta>
            <ta e="T137" id="Seg_2043" s="T136">teː</ta>
            <ta e="T138" id="Seg_2044" s="T137">paqɨl-ɨ-k</ta>
            <ta e="T139" id="Seg_2045" s="T138">tüː-nde</ta>
            <ta e="T140" id="Seg_2046" s="T139">čabɨ-ŋɨ-m</ta>
            <ta e="T141" id="Seg_2047" s="T140">i</ta>
            <ta e="T142" id="Seg_2048" s="T141">am-gu</ta>
            <ta e="T143" id="Seg_2049" s="T142">kɨga-k</ta>
            <ta e="T144" id="Seg_2050" s="T143">a</ta>
            <ta e="T145" id="Seg_2051" s="T144">surup</ta>
            <ta e="T146" id="Seg_2052" s="T145">ɛːǯal-lə-ku-ŋɨ</ta>
            <ta e="T147" id="Seg_2053" s="T146">man-naj</ta>
            <ta e="T148" id="Seg_2054" s="T147">qwäʒa-k</ta>
            <ta e="T149" id="Seg_2055" s="T148">tüː-nde</ta>
            <ta e="T150" id="Seg_2056" s="T149">ta</ta>
            <ta e="T151" id="Seg_2057" s="T150">omde-äšɨk</ta>
            <ta e="T152" id="Seg_2058" s="T151">kwel</ta>
            <ta e="T153" id="Seg_2059" s="T152">per-če</ta>
            <ta e="T154" id="Seg_2060" s="T153">okkər</ta>
            <ta e="T155" id="Seg_2061" s="T154">čab-ɨ-p</ta>
            <ta e="T156" id="Seg_2062" s="T155">surup-ni</ta>
            <ta e="T157" id="Seg_2063" s="T156">čačɨ-l-t</ta>
            <ta e="T158" id="Seg_2064" s="T157">surup</ta>
            <ta e="T159" id="Seg_2065" s="T158">am-ɨ-lɨ-t</ta>
            <ta e="T160" id="Seg_2066" s="T159">ɛːǯal-lə-ku-ŋɨ</ta>
            <ta e="T161" id="Seg_2067" s="T160">swa</ta>
            <ta e="T162" id="Seg_2068" s="T161">maqqɨl</ta>
            <ta e="T163" id="Seg_2069" s="T162">man-nan</ta>
            <ta e="T164" id="Seg_2070" s="T163">teː</ta>
            <ta e="T165" id="Seg_2071" s="T164">paqɨl-äšɨk</ta>
            <ta e="T166" id="Seg_2072" s="T165">a</ta>
            <ta e="T167" id="Seg_2073" s="T166">Qaːmaǯʼa</ta>
            <ta e="T168" id="Seg_2074" s="T167">ɛːǯal-lə-ku-ŋɨ</ta>
            <ta e="T169" id="Seg_2075" s="T168">tan</ta>
            <ta e="T170" id="Seg_2076" s="T169">maᴣik</ta>
            <ta e="T171" id="Seg_2077" s="T170">aː</ta>
            <ta e="T172" id="Seg_2078" s="T171">am-ɨ-la-nd</ta>
            <ta e="T173" id="Seg_2079" s="T172">čoʒəlʼe</ta>
            <ta e="T174" id="Seg_2080" s="T173">paqɨl-ɨ-špɨ-le-mbɨ</ta>
            <ta e="T175" id="Seg_2081" s="T174">maqqɨl</ta>
            <ta e="T176" id="Seg_2082" s="T175">surup</ta>
            <ta e="T177" id="Seg_2083" s="T176">ɛːǯal-lə-ku-ŋɨ</ta>
            <ta e="T178" id="Seg_2084" s="T177">paqɨl-äšɨk</ta>
            <ta e="T179" id="Seg_2085" s="T178">lattɨ-la-m</ta>
            <ta e="T180" id="Seg_2086" s="T179">Qaːmaǯʼa</ta>
            <ta e="T181" id="Seg_2087" s="T180">ɛːǯal-lə-ku-ŋɨ</ta>
            <ta e="T182" id="Seg_2088" s="T181">surup-ni</ta>
            <ta e="T183" id="Seg_2089" s="T182">qotä</ta>
            <ta e="T184" id="Seg_2090" s="T183">qontɨ-äšɨk</ta>
            <ta e="T185" id="Seg_2091" s="T184">surup</ta>
            <ta e="T186" id="Seg_2092" s="T185">qontɨ-la</ta>
            <ta e="T187" id="Seg_2093" s="T186">tab</ta>
            <ta e="T188" id="Seg_2094" s="T187">aːŋdɨl</ta>
            <ta e="T189" id="Seg_2095" s="T188">paɣɨ-se</ta>
            <ta e="T190" id="Seg_2096" s="T189">nanǯʼi-m-t</ta>
            <ta e="T191" id="Seg_2097" s="T190">teː</ta>
            <ta e="T192" id="Seg_2098" s="T191">koro-lʼčǝ-lɨ-t</ta>
            <ta e="T193" id="Seg_2099" s="T192">i</ta>
            <ta e="T194" id="Seg_2100" s="T193">surup</ta>
            <ta e="T195" id="Seg_2101" s="T194">nɨka</ta>
            <ta e="T196" id="Seg_2102" s="T195">qu-la</ta>
            <ta e="T197" id="Seg_2103" s="T196">Qaːmaǯʼa</ta>
            <ta e="T198" id="Seg_2104" s="T197">teː</ta>
            <ta e="T199" id="Seg_2105" s="T198">kobɨ-m-t</ta>
            <ta e="T200" id="Seg_2106" s="T199">kɨrɨ-lʼčǝ</ta>
            <ta e="T201" id="Seg_2107" s="T200">waǯʼe-m-t</ta>
            <ta e="T202" id="Seg_2108" s="T201">wesʼ</ta>
            <ta e="T203" id="Seg_2109" s="T202">ubirain-lə-t</ta>
            <ta e="T204" id="Seg_2110" s="T203">tɨrɨ-lʼ</ta>
            <ta e="T205" id="Seg_2111" s="T204">koʒa-nde</ta>
            <ta e="T206" id="Seg_2112" s="T205">waǯʼe-p</ta>
            <ta e="T207" id="Seg_2113" s="T206">pat-le</ta>
            <ta e="T208" id="Seg_2114" s="T207">i</ta>
            <ta e="T209" id="Seg_2115" s="T208">kwel-p</ta>
            <ta e="T210" id="Seg_2116" s="T209">pat-le</ta>
            <ta e="T211" id="Seg_2117" s="T210">pat-ɨ-lɨ-t</ta>
            <ta e="T212" id="Seg_2118" s="T211">i</ta>
            <ta e="T213" id="Seg_2119" s="T212">sugulʼǯʼe</ta>
            <ta e="T214" id="Seg_2120" s="T213">üːppɨ-zi-le</ta>
            <ta e="T215" id="Seg_2121" s="T214">sugulʼǯʼe-ɣɨn-t</ta>
            <ta e="T216" id="Seg_2122" s="T215">medɨ-špɨ</ta>
            <ta e="T217" id="Seg_2123" s="T216">kašqɨ-t</ta>
            <ta e="T218" id="Seg_2124" s="T217">aː</ta>
            <ta e="T219" id="Seg_2125" s="T218">qaškolʼ-mbɨ</ta>
            <ta e="T220" id="Seg_2126" s="T219">maːt</ta>
            <ta e="T221" id="Seg_2127" s="T220">par-ɣɨnt</ta>
            <ta e="T222" id="Seg_2128" s="T221">inne</ta>
            <ta e="T223" id="Seg_2129" s="T222">čʼara-la</ta>
            <ta e="T224" id="Seg_2130" s="T223">šʼoːl-n</ta>
            <ta e="T225" id="Seg_2131" s="T224">par</ta>
            <ta e="T226" id="Seg_2132" s="T225">illä</ta>
            <ta e="T227" id="Seg_2133" s="T226">mantɨ-mbɨ</ta>
            <ta e="T228" id="Seg_2134" s="T227">temnʼa-t</ta>
            <ta e="T229" id="Seg_2135" s="T228">i</ta>
            <ta e="T230" id="Seg_2136" s="T229">anǯʼa-t</ta>
            <ta e="T231" id="Seg_2137" s="T230">okkər</ta>
            <ta e="T232" id="Seg_2138" s="T231">tüː</ta>
            <ta e="T233" id="Seg_2139" s="T232">saj</ta>
            <ta e="T234" id="Seg_2140" s="T233">piːder-ntɨ-di</ta>
            <ta e="T235" id="Seg_2141" s="T234">taw</ta>
            <ta e="T236" id="Seg_2142" s="T235">man</ta>
            <ta e="T237" id="Seg_2143" s="T236">Qaːmaǯʼa-mɨ</ta>
            <ta e="T238" id="Seg_2144" s="T237">tab</ta>
            <ta e="T239" id="Seg_2145" s="T238">nɨča-un</ta>
            <ta e="T240" id="Seg_2146" s="T239">illä</ta>
            <ta e="T241" id="Seg_2147" s="T240">parčɛ-la</ta>
            <ta e="T242" id="Seg_2148" s="T241">man</ta>
            <ta e="T243" id="Seg_2149" s="T242">tawe-ja-k</ta>
            <ta e="T244" id="Seg_2150" s="T243">nɨndɨ</ta>
            <ta e="T245" id="Seg_2151" s="T244">tab-ɨ-štja-ɣɨn</ta>
            <ta e="T246" id="Seg_2152" s="T245">waǯʼe-m</ta>
            <ta e="T247" id="Seg_2153" s="T246">i</ta>
            <ta e="T248" id="Seg_2154" s="T247">kwel-p</ta>
            <ta e="T249" id="Seg_2155" s="T248">me-ŋɨ-k</ta>
            <ta e="T250" id="Seg_2156" s="T249">i</ta>
            <ta e="T251" id="Seg_2157" s="T250">kwel-se</ta>
            <ta e="T252" id="Seg_2158" s="T251">am-ɨ-dɨ-lɨ-t</ta>
            <ta e="T253" id="Seg_2159" s="T252">i</ta>
            <ta e="T254" id="Seg_2160" s="T253">patom</ta>
            <ta e="T255" id="Seg_2161" s="T254">wargɨ-lə-kku-ŋɨ-dət</ta>
            <ta e="T256" id="Seg_2162" s="T255">i</ta>
            <ta e="T257" id="Seg_2163" s="T256">ele-lə-kku-ŋɨ-dət</ta>
            <ta e="T258" id="Seg_2164" s="T257">neː-p</ta>
            <ta e="T259" id="Seg_2165" s="T258">iː-p</ta>
            <ta e="T260" id="Seg_2166" s="T259">qontɨ-ku-ndɨ-t</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2167" s="T0">two</ta>
            <ta e="T2" id="Seg_2168" s="T1">brother-CRC-DU</ta>
            <ta e="T3" id="Seg_2169" s="T2">live-DUR-INFER-3DU.S</ta>
            <ta e="T4" id="Seg_2170" s="T3">big</ta>
            <ta e="T5" id="Seg_2171" s="T4">brother.[NOM]</ta>
            <ta e="T6" id="Seg_2172" s="T5">get.married-HAB-CVB</ta>
            <ta e="T7" id="Seg_2173" s="T6">live.[3SG.S]</ta>
            <ta e="T8" id="Seg_2174" s="T7">small</ta>
            <ta e="T9" id="Seg_2175" s="T8">brother-3SG</ta>
            <ta e="T10" id="Seg_2176" s="T9">Kamacha.[3SG.S]</ta>
            <ta e="T11" id="Seg_2177" s="T10">sister.in.law-3SG</ta>
            <ta e="T12" id="Seg_2178" s="T11">(s)he-EP-ACC</ta>
            <ta e="T13" id="Seg_2179" s="T12">offend-DRV-HAB-CVB</ta>
            <ta e="T14" id="Seg_2180" s="T13">hold-3SG.O</ta>
            <ta e="T15" id="Seg_2181" s="T14">brother-CRC-DU</ta>
            <ta e="T16" id="Seg_2182" s="T15">capercaillie-TR-DUR-CO-3DU.S</ta>
            <ta e="T17" id="Seg_2183" s="T16">capercaillie.[NOM]</ta>
            <ta e="T18" id="Seg_2184" s="T17">kill-EP-IPFV2-DUR-INFER-3DU.O</ta>
            <ta e="T19" id="Seg_2185" s="T18">big.[NOM]</ta>
            <ta e="T20" id="Seg_2186" s="T19">brother-3SG</ta>
            <ta e="T21" id="Seg_2187" s="T20">himself</ta>
            <ta e="T22" id="Seg_2188" s="T21">half-CAR-ADV</ta>
            <ta e="T23" id="Seg_2189" s="T22">go.away-CVB</ta>
            <ta e="T24" id="Seg_2190" s="T23">deadfall.trap-EP-ADJZ</ta>
            <ta e="T25" id="Seg_2191" s="T24">road-EP-LOC-3SG</ta>
            <ta e="T26" id="Seg_2192" s="T25">sister.in.law-3SG</ta>
            <ta e="T27" id="Seg_2193" s="T26">Kamacha-ACC</ta>
            <ta e="T28" id="Seg_2194" s="T27">eat-EP-TR-DUR-3SG.O</ta>
            <ta e="T29" id="Seg_2195" s="T28">capercaillie-GEN</ta>
            <ta e="T30" id="Seg_2196" s="T29">rib-INSTR</ta>
            <ta e="T31" id="Seg_2197" s="T30">and</ta>
            <ta e="T32" id="Seg_2198" s="T31">capercaillie's.head-INSTR</ta>
            <ta e="T33" id="Seg_2199" s="T32">now</ta>
            <ta e="T34" id="Seg_2200" s="T33">one</ta>
            <ta e="T35" id="Seg_2201" s="T34">where-EP-ADV.LOC</ta>
            <ta e="T36" id="Seg_2202" s="T35">go.away-OPT-3DU.S</ta>
            <ta e="T37" id="Seg_2203" s="T36">two-human.being-PL.[NOM]</ta>
            <ta e="T38" id="Seg_2204" s="T37">Kamacha-GEN</ta>
            <ta e="T39" id="Seg_2205" s="T38">with</ta>
            <ta e="T40" id="Seg_2206" s="T39">capercaillie.[NOM]</ta>
            <ta e="T41" id="Seg_2207" s="T40">fly-IPFV2-INFER.[3SG.S]</ta>
            <ta e="T42" id="Seg_2208" s="T41">Kamacha.[NOM]</ta>
            <ta e="T43" id="Seg_2209" s="T42">shout-CO.[3SG.S]</ta>
            <ta e="T44" id="Seg_2210" s="T43">capercaillie-VOC</ta>
            <ta e="T45" id="Seg_2211" s="T44">capercaillie-VOC</ta>
            <ta e="T46" id="Seg_2212" s="T45">capercaillie's.head-2SG</ta>
            <ta e="T47" id="Seg_2213" s="T46">and</ta>
            <ta e="T48" id="Seg_2214" s="T47">rib-2SG</ta>
            <ta e="T49" id="Seg_2215" s="T48">I.DAT</ta>
            <ta e="T50" id="Seg_2216" s="T49">leave-IMP.2SG.S</ta>
            <ta e="T51" id="Seg_2217" s="T50">brother-3SG</ta>
            <ta e="T52" id="Seg_2218" s="T51">ask-IPFV3-CVB</ta>
            <ta e="T53" id="Seg_2219" s="T52">what-TRL</ta>
            <ta e="T54" id="Seg_2220" s="T53">such-ADV</ta>
            <ta e="T55" id="Seg_2221" s="T54">shout-CO-2SG.S</ta>
            <ta e="T56" id="Seg_2222" s="T55">but</ta>
            <ta e="T57" id="Seg_2223" s="T56">Kamacha.[NOM]</ta>
            <ta e="T58" id="Seg_2224" s="T57">say-DUR-CO.[3SG.S]</ta>
            <ta e="T59" id="Seg_2225" s="T58">I.ACC</ta>
            <ta e="T60" id="Seg_2226" s="T59">sister.in.law-1SG</ta>
            <ta e="T61" id="Seg_2227" s="T60">rib-INSTR</ta>
            <ta e="T62" id="Seg_2228" s="T61">and</ta>
            <ta e="T63" id="Seg_2229" s="T62">capercaillie's.head-INSTR</ta>
            <ta e="T64" id="Seg_2230" s="T63">eat-EP-TR-DUR-CO.[3SG.S]</ta>
            <ta e="T65" id="Seg_2231" s="T64">evening-ADV.LOC</ta>
            <ta e="T66" id="Seg_2232" s="T65">home</ta>
            <ta e="T67" id="Seg_2233" s="T66">come-FUT-3DU.S</ta>
            <ta e="T68" id="Seg_2234" s="T67">brother-3SG</ta>
            <ta e="T69" id="Seg_2235" s="T68">wife-ACC-3SG</ta>
            <ta e="T70" id="Seg_2236" s="T69">catch-CVB</ta>
            <ta e="T71" id="Seg_2237" s="T70">beat-INCH-3SG.O</ta>
            <ta e="T72" id="Seg_2238" s="T71">tomorrow</ta>
            <ta e="T73" id="Seg_2239" s="T72">brother-3SG</ta>
            <ta e="T74" id="Seg_2240" s="T73">again</ta>
            <ta e="T75" id="Seg_2241" s="T74">himself</ta>
            <ta e="T76" id="Seg_2242" s="T75">half-CAR-ADV</ta>
            <ta e="T77" id="Seg_2243" s="T76">go.away-FUT.[3SG.S]</ta>
            <ta e="T78" id="Seg_2244" s="T77">deadfall.trap-EP-ADJZ</ta>
            <ta e="T79" id="Seg_2245" s="T78">road-LOC-3SG</ta>
            <ta e="T80" id="Seg_2246" s="T79">sister.in.law.[NOM]</ta>
            <ta e="T81" id="Seg_2247" s="T80">catch-CVB</ta>
            <ta e="T82" id="Seg_2248" s="T81">Kamacha-ACC</ta>
            <ta e="T83" id="Seg_2249" s="T82">kill-INCH-3SG.O</ta>
            <ta e="T84" id="Seg_2250" s="T83">Kamacha.[NOM]</ta>
            <ta e="T85" id="Seg_2251" s="T84">street-ILL</ta>
            <ta e="T86" id="Seg_2252" s="T85">go.out-CVB</ta>
            <ta e="T87" id="Seg_2253" s="T86">go.away-CVB</ta>
            <ta e="T88" id="Seg_2254" s="T87">where</ta>
            <ta e="T89" id="Seg_2255" s="T88">eye-3SG</ta>
            <ta e="T90" id="Seg_2256" s="T89">see.[3SG.S]</ta>
            <ta e="T91" id="Seg_2257" s="T90">long</ta>
            <ta e="T92" id="Seg_2258" s="T91">whether</ta>
            <ta e="T93" id="Seg_2259" s="T92">or</ta>
            <ta e="T94" id="Seg_2260" s="T93">short-ADV</ta>
            <ta e="T95" id="Seg_2261" s="T94">run-OPT.[3SG.S]</ta>
            <ta e="T96" id="Seg_2262" s="T95">sight-IPFV3-FRQ-CO-3SG.O</ta>
            <ta e="T97" id="Seg_2263" s="T96">raven.[NOM]</ta>
            <ta e="T98" id="Seg_2264" s="T97">fly-IPFV2-INFER.[3SG.S]</ta>
            <ta e="T99" id="Seg_2265" s="T98">mouth-EP-ILL</ta>
            <ta e="T100" id="Seg_2266" s="T99">fish.[NOM]</ta>
            <ta e="T101" id="Seg_2267" s="T100">bring-FRQ-EP-INFER-3SG.O</ta>
            <ta e="T102" id="Seg_2268" s="T101">what-PROL</ta>
            <ta e="T103" id="Seg_2269" s="T102">raven.[NOM]</ta>
            <ta e="T104" id="Seg_2270" s="T103">fly-DRV-IPFV2.[3SG.S]</ta>
            <ta e="T105" id="Seg_2271" s="T104">Kamacha.[NOM]</ta>
            <ta e="T106" id="Seg_2272" s="T105">there</ta>
            <ta e="T107" id="Seg_2273" s="T106">down</ta>
            <ta e="T108" id="Seg_2274" s="T107">move-PFV-FUT.[3SG.S]</ta>
            <ta e="T109" id="Seg_2275" s="T108">reach-FUT.[3SG.S]</ta>
            <ta e="T110" id="Seg_2276" s="T109">river-DIM.[NOM]</ta>
            <ta e="T111" id="Seg_2277" s="T110">river-DIM-LOC</ta>
            <ta e="T112" id="Seg_2278" s="T111">fish.[NOM]</ta>
            <ta e="T113" id="Seg_2279" s="T112">much-%%</ta>
            <ta e="T114" id="Seg_2280" s="T113">(s)he.[NOM]</ta>
            <ta e="T115" id="Seg_2281" s="T114">cut.down-EP-PFV-RES-3SG.O</ta>
            <ta e="T116" id="Seg_2282" s="T115">bird.cherry-ACC</ta>
            <ta e="T117" id="Seg_2283" s="T116">scoop-ACC</ta>
            <ta e="T118" id="Seg_2284" s="T117">do-PFV-FUT.[3SG.S]</ta>
            <ta e="T119" id="Seg_2285" s="T118">and</ta>
            <ta e="T120" id="Seg_2286" s="T119">let</ta>
            <ta e="T121" id="Seg_2287" s="T120">fish-ACC</ta>
            <ta e="T122" id="Seg_2288" s="T121">scoop-RES-IPFV2-INF</ta>
            <ta e="T123" id="Seg_2289" s="T122">fire-ACC</ta>
            <ta e="T124" id="Seg_2290" s="T123">strike-INCH-3SG.O</ta>
            <ta e="T125" id="Seg_2291" s="T124">perch-ACC</ta>
            <ta e="T126" id="Seg_2292" s="T125">morning.[NOM]</ta>
            <ta e="T127" id="Seg_2293" s="T126">put-PFV-MULTS-RES-3SG.O</ta>
            <ta e="T128" id="Seg_2294" s="T127">%%-INSTR</ta>
            <ta e="T129" id="Seg_2295" s="T128">suddenly</ta>
            <ta e="T130" id="Seg_2296" s="T129">look-PFV-FUT.[3SG.S]</ta>
            <ta e="T131" id="Seg_2297" s="T130">wild.animal.[NOM]</ta>
            <ta e="T132" id="Seg_2298" s="T131">come-IPFV2-INFER.[3SG.S]</ta>
            <ta e="T133" id="Seg_2299" s="T132">Kamacha.[NOM]</ta>
            <ta e="T134" id="Seg_2300" s="T133">what</ta>
            <ta e="T135" id="Seg_2301" s="T134">do-IPFV2-INFER-2SG.O</ta>
            <ta e="T136" id="Seg_2302" s="T135">kidney-1SG</ta>
            <ta e="T137" id="Seg_2303" s="T136">away</ta>
            <ta e="T138" id="Seg_2304" s="T137">dig-EP-1SG.S</ta>
            <ta e="T139" id="Seg_2305" s="T138">fire-ILL</ta>
            <ta e="T140" id="Seg_2306" s="T139">burn-CO-1SG.O</ta>
            <ta e="T141" id="Seg_2307" s="T140">and</ta>
            <ta e="T142" id="Seg_2308" s="T141">eat-INF</ta>
            <ta e="T143" id="Seg_2309" s="T142">want-1SG.S</ta>
            <ta e="T144" id="Seg_2310" s="T143">but</ta>
            <ta e="T145" id="Seg_2311" s="T144">wild.animal.[NOM]</ta>
            <ta e="T146" id="Seg_2312" s="T145">say-INCH-DUR-CO.[3SG.S]</ta>
            <ta e="T147" id="Seg_2313" s="T146">I.NOM-EMPH</ta>
            <ta e="T148" id="Seg_2314" s="T147">be.hungry-1SG.S</ta>
            <ta e="T149" id="Seg_2315" s="T148">fire-ILL</ta>
            <ta e="T150" id="Seg_2316" s="T149">on.other.side</ta>
            <ta e="T151" id="Seg_2317" s="T150">sit.down-IMP.2SG.S</ta>
            <ta e="T152" id="Seg_2318" s="T151">fish.[NOM]</ta>
            <ta e="T153" id="Seg_2319" s="T152">fry-DRV.[3SG.S]</ta>
            <ta e="T154" id="Seg_2320" s="T153">one</ta>
            <ta e="T155" id="Seg_2321" s="T154">%%-EP-ACC</ta>
            <ta e="T156" id="Seg_2322" s="T155">wild.animal-ALL</ta>
            <ta e="T157" id="Seg_2323" s="T156">throw-2SG.O-3SG.O</ta>
            <ta e="T158" id="Seg_2324" s="T157">wild.animal.[NOM]</ta>
            <ta e="T159" id="Seg_2325" s="T158">eat-EP-RES-3SG.O</ta>
            <ta e="T160" id="Seg_2326" s="T159">say-INCH-DUR-CO.[3SG.S]</ta>
            <ta e="T161" id="Seg_2327" s="T160">good</ta>
            <ta e="T162" id="Seg_2328" s="T161">kidney.[NOM]</ta>
            <ta e="T163" id="Seg_2329" s="T162">I.NOM-ADES</ta>
            <ta e="T164" id="Seg_2330" s="T163">away</ta>
            <ta e="T165" id="Seg_2331" s="T164">dig-IMP.2SG.S</ta>
            <ta e="T166" id="Seg_2332" s="T165">but</ta>
            <ta e="T167" id="Seg_2333" s="T166">Kamacha.[NOM]</ta>
            <ta e="T168" id="Seg_2334" s="T167">say-INCH-DUR-CO.[3SG.S]</ta>
            <ta e="T169" id="Seg_2335" s="T168">you.SG.[NOM]</ta>
            <ta e="T170" id="Seg_2336" s="T169">I.ACC</ta>
            <ta e="T171" id="Seg_2337" s="T170">NEG</ta>
            <ta e="T172" id="Seg_2338" s="T171">eat-EP-FUT-2SG.S</ta>
            <ta e="T173" id="Seg_2339" s="T172">painful</ta>
            <ta e="T174" id="Seg_2340" s="T173">dig-EP-IPFV2-OPT-HAB</ta>
            <ta e="T175" id="Seg_2341" s="T174">kidney.[NOM]</ta>
            <ta e="T176" id="Seg_2342" s="T175">wild.animal.[NOM]</ta>
            <ta e="T177" id="Seg_2343" s="T176">say-INCH-DUR-CO.[3SG.S]</ta>
            <ta e="T178" id="Seg_2344" s="T177">dig-IMP.2SG.S</ta>
            <ta e="T179" id="Seg_2345" s="T178">endure-FUT-1SG.O</ta>
            <ta e="T180" id="Seg_2346" s="T179">Kamacha.[NOM]</ta>
            <ta e="T181" id="Seg_2347" s="T180">say-INCH-DUR-CO.[3SG.S]</ta>
            <ta e="T182" id="Seg_2348" s="T181">wild.animal-ALL</ta>
            <ta e="T183" id="Seg_2349" s="T182">backwards</ta>
            <ta e="T184" id="Seg_2350" s="T183">lie.down-IMP.2SG.S</ta>
            <ta e="T185" id="Seg_2351" s="T184">wild.animal.[NOM]</ta>
            <ta e="T186" id="Seg_2352" s="T185">lie.down-FUT.[3SG.S]</ta>
            <ta e="T187" id="Seg_2353" s="T186">(s)he.[NOM]</ta>
            <ta e="T188" id="Seg_2354" s="T187">sharp</ta>
            <ta e="T189" id="Seg_2355" s="T188">knife-INSTR</ta>
            <ta e="T190" id="Seg_2356" s="T189">stomach-ACC-3SG</ta>
            <ta e="T191" id="Seg_2357" s="T190">away</ta>
            <ta e="T192" id="Seg_2358" s="T191">cut-PFV-RES-3SG</ta>
            <ta e="T193" id="Seg_2359" s="T192">and</ta>
            <ta e="T194" id="Seg_2360" s="T193">wild.animal.[NOM]</ta>
            <ta e="T195" id="Seg_2361" s="T194">here</ta>
            <ta e="T196" id="Seg_2362" s="T195">die-FUT.[3SG.S]</ta>
            <ta e="T197" id="Seg_2363" s="T196">Kamacha.[NOM]</ta>
            <ta e="T198" id="Seg_2364" s="T197">away</ta>
            <ta e="T199" id="Seg_2365" s="T198">skin-ACC-3SG</ta>
            <ta e="T200" id="Seg_2366" s="T199">pull.off-PFV.[3SG.S]</ta>
            <ta e="T201" id="Seg_2367" s="T200">meat-ACC-3SG</ta>
            <ta e="T202" id="Seg_2368" s="T201">all</ta>
            <ta e="T203" id="Seg_2369" s="T202">remove-INCH-3SG.O</ta>
            <ta e="T204" id="Seg_2370" s="T203">fullness-ADJZ</ta>
            <ta e="T205" id="Seg_2371" s="T204">bag-ILL</ta>
            <ta e="T206" id="Seg_2372" s="T205">meat-ACC</ta>
            <ta e="T207" id="Seg_2373" s="T206">put-CVB</ta>
            <ta e="T208" id="Seg_2374" s="T207">and</ta>
            <ta e="T209" id="Seg_2375" s="T208">fish-ACC</ta>
            <ta e="T210" id="Seg_2376" s="T209">put-CVB</ta>
            <ta e="T211" id="Seg_2377" s="T210">put-EP-RES-3SG.O</ta>
            <ta e="T212" id="Seg_2378" s="T211">and</ta>
            <ta e="T213" id="Seg_2379" s="T212">at.home</ta>
            <ta e="T214" id="Seg_2380" s="T213">leave-%%-OPT.[3SG.S]</ta>
            <ta e="T215" id="Seg_2381" s="T214">at.home-LOC-3SG</ta>
            <ta e="T216" id="Seg_2382" s="T215">reach-IPFV2.[3SG.S]</ta>
            <ta e="T217" id="Seg_2383" s="T216">smoke-3SG</ta>
            <ta e="T218" id="Seg_2384" s="T217">NEG</ta>
            <ta e="T219" id="Seg_2385" s="T218">smoke-PST.NAR.[3SG.S]</ta>
            <ta e="T220" id="Seg_2386" s="T219">house.[NOM]</ta>
            <ta e="T221" id="Seg_2387" s="T220">top-ILL.3SG</ta>
            <ta e="T222" id="Seg_2388" s="T221">up</ta>
            <ta e="T223" id="Seg_2389" s="T222">run.out-FUT.[3SG.S]</ta>
            <ta e="T224" id="Seg_2390" s="T223">##%%-GEN</ta>
            <ta e="T225" id="Seg_2391" s="T224">top.[NOM]</ta>
            <ta e="T226" id="Seg_2392" s="T225">down</ta>
            <ta e="T227" id="Seg_2393" s="T226">look-PST.NAR.[3SG.S]</ta>
            <ta e="T228" id="Seg_2394" s="T227">brother-3SG</ta>
            <ta e="T229" id="Seg_2395" s="T228">and</ta>
            <ta e="T230" id="Seg_2396" s="T229">daughter.in.law-3SG</ta>
            <ta e="T231" id="Seg_2397" s="T230">one</ta>
            <ta e="T232" id="Seg_2398" s="T231">fire.[NOM]</ta>
            <ta e="T233" id="Seg_2399" s="T232">eye.[NOM]</ta>
            <ta e="T234" id="Seg_2400" s="T233">share-IPFV-3DU.O</ta>
            <ta e="T235" id="Seg_2401" s="T234">this</ta>
            <ta e="T236" id="Seg_2402" s="T235">I.GEN</ta>
            <ta e="T237" id="Seg_2403" s="T236">Kamacha-1SG</ta>
            <ta e="T238" id="Seg_2404" s="T237">(s)he.[NOM]</ta>
            <ta e="T239" id="Seg_2405" s="T238">thereto-PROL</ta>
            <ta e="T240" id="Seg_2406" s="T239">down</ta>
            <ta e="T241" id="Seg_2407" s="T240">shout-FUT.[3SG.S]</ta>
            <ta e="T242" id="Seg_2408" s="T241">I.NOM</ta>
            <ta e="T243" id="Seg_2409" s="T242">there_be-CO-1SG.S</ta>
            <ta e="T244" id="Seg_2410" s="T243">here</ta>
            <ta e="T245" id="Seg_2411" s="T244">(s)he-EP-DU-LOC</ta>
            <ta e="T246" id="Seg_2412" s="T245">meat-ACC</ta>
            <ta e="T247" id="Seg_2413" s="T246">and</ta>
            <ta e="T248" id="Seg_2414" s="T247">fish-ACC</ta>
            <ta e="T249" id="Seg_2415" s="T248">give-CO-1SG.S</ta>
            <ta e="T250" id="Seg_2416" s="T249">and</ta>
            <ta e="T251" id="Seg_2417" s="T250">fish-INSTR</ta>
            <ta e="T252" id="Seg_2418" s="T251">eat-EP-TR-RES-3SG.O</ta>
            <ta e="T253" id="Seg_2419" s="T252">and</ta>
            <ta e="T254" id="Seg_2420" s="T253">then</ta>
            <ta e="T255" id="Seg_2421" s="T254">live-INCH-DUR-CO-3PL</ta>
            <ta e="T256" id="Seg_2422" s="T255">and</ta>
            <ta e="T257" id="Seg_2423" s="T256">live-INCH-DUR-CO-3PL</ta>
            <ta e="T258" id="Seg_2424" s="T257">daughter-ACC</ta>
            <ta e="T259" id="Seg_2425" s="T258">son-ACC</ta>
            <ta e="T260" id="Seg_2426" s="T259">lie.down-DUR-INFER-3SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2427" s="T0">два</ta>
            <ta e="T2" id="Seg_2428" s="T1">брат-CRC-DU</ta>
            <ta e="T3" id="Seg_2429" s="T2">жить-DUR-INFER-3DU.S</ta>
            <ta e="T4" id="Seg_2430" s="T3">большой</ta>
            <ta e="T5" id="Seg_2431" s="T4">брат.[NOM]</ta>
            <ta e="T6" id="Seg_2432" s="T5">жениться-HAB-CVB</ta>
            <ta e="T7" id="Seg_2433" s="T6">жить.[3SG.S]</ta>
            <ta e="T8" id="Seg_2434" s="T7">маленький</ta>
            <ta e="T9" id="Seg_2435" s="T8">брат-3SG</ta>
            <ta e="T10" id="Seg_2436" s="T9">Камача.[3SG.S]</ta>
            <ta e="T11" id="Seg_2437" s="T10">сноха-3SG</ta>
            <ta e="T12" id="Seg_2438" s="T11">он(а)-EP-ACC</ta>
            <ta e="T13" id="Seg_2439" s="T12">обижать-DRV-HAB-CVB</ta>
            <ta e="T14" id="Seg_2440" s="T13">держать-3SG.O</ta>
            <ta e="T15" id="Seg_2441" s="T14">брат-CRC-DU</ta>
            <ta e="T16" id="Seg_2442" s="T15">глухарь-TR-DUR-CO-3DU.S</ta>
            <ta e="T17" id="Seg_2443" s="T16">глухарь.[NOM]</ta>
            <ta e="T18" id="Seg_2444" s="T17">убить-EP-IPFV2-DUR-INFER-3DU.O</ta>
            <ta e="T19" id="Seg_2445" s="T18">большой.[NOM]</ta>
            <ta e="T20" id="Seg_2446" s="T19">брат-3SG</ta>
            <ta e="T21" id="Seg_2447" s="T20">он.сам</ta>
            <ta e="T22" id="Seg_2448" s="T21">половина-CAR-ADV</ta>
            <ta e="T23" id="Seg_2449" s="T22">пойти-CVB</ta>
            <ta e="T24" id="Seg_2450" s="T23">слопец-EP-ADJZ</ta>
            <ta e="T25" id="Seg_2451" s="T24">путь-EP-LOC-3SG</ta>
            <ta e="T26" id="Seg_2452" s="T25">сноха-3SG</ta>
            <ta e="T27" id="Seg_2453" s="T26">Камача-ACC</ta>
            <ta e="T28" id="Seg_2454" s="T27">есть-EP-TR-DUR-3SG.O</ta>
            <ta e="T29" id="Seg_2455" s="T28">глухарь-GEN</ta>
            <ta e="T30" id="Seg_2456" s="T29">ребро-INSTR</ta>
            <ta e="T31" id="Seg_2457" s="T30">и</ta>
            <ta e="T32" id="Seg_2458" s="T31">голова.глухаря-INSTR</ta>
            <ta e="T33" id="Seg_2459" s="T32">теперь</ta>
            <ta e="T34" id="Seg_2460" s="T33">один</ta>
            <ta e="T35" id="Seg_2461" s="T34">где-EP-ADV.LOC</ta>
            <ta e="T36" id="Seg_2462" s="T35">пойти-OPT-3DU.S</ta>
            <ta e="T37" id="Seg_2463" s="T36">два-человек-PL.[NOM]</ta>
            <ta e="T38" id="Seg_2464" s="T37">Камача-GEN</ta>
            <ta e="T39" id="Seg_2465" s="T38">с</ta>
            <ta e="T40" id="Seg_2466" s="T39">глухарь.[NOM]</ta>
            <ta e="T41" id="Seg_2467" s="T40">лететь-IPFV2-INFER.[3SG.S]</ta>
            <ta e="T42" id="Seg_2468" s="T41">Камача.[NOM]</ta>
            <ta e="T43" id="Seg_2469" s="T42">кричать-CO.[3SG.S]</ta>
            <ta e="T44" id="Seg_2470" s="T43">глухарь-VOC</ta>
            <ta e="T45" id="Seg_2471" s="T44">глухарь-VOC</ta>
            <ta e="T46" id="Seg_2472" s="T45">голова.глухаря-2SG</ta>
            <ta e="T47" id="Seg_2473" s="T46">и</ta>
            <ta e="T48" id="Seg_2474" s="T47">ребро-2SG</ta>
            <ta e="T49" id="Seg_2475" s="T48">я.DAT</ta>
            <ta e="T50" id="Seg_2476" s="T49">оставить-IMP.2SG.S</ta>
            <ta e="T51" id="Seg_2477" s="T50">брат-3SG</ta>
            <ta e="T52" id="Seg_2478" s="T51">спрашивать-IPFV3-CVB</ta>
            <ta e="T53" id="Seg_2479" s="T52">что-TRL</ta>
            <ta e="T54" id="Seg_2480" s="T53">такой-ADV</ta>
            <ta e="T55" id="Seg_2481" s="T54">кричать-CO-2SG.S</ta>
            <ta e="T56" id="Seg_2482" s="T55">а</ta>
            <ta e="T57" id="Seg_2483" s="T56">Камача.[NOM]</ta>
            <ta e="T58" id="Seg_2484" s="T57">сказать-DUR-CO.[3SG.S]</ta>
            <ta e="T59" id="Seg_2485" s="T58">я.ACC</ta>
            <ta e="T60" id="Seg_2486" s="T59">сноха-1SG</ta>
            <ta e="T61" id="Seg_2487" s="T60">ребро-INSTR</ta>
            <ta e="T62" id="Seg_2488" s="T61">и</ta>
            <ta e="T63" id="Seg_2489" s="T62">голова.глухаря-INSTR</ta>
            <ta e="T64" id="Seg_2490" s="T63">есть-EP-TR-DUR-CO.[3SG.S]</ta>
            <ta e="T65" id="Seg_2491" s="T64">вечер-ADV.LOC</ta>
            <ta e="T66" id="Seg_2492" s="T65">домой</ta>
            <ta e="T67" id="Seg_2493" s="T66">прийти-FUT-3DU.S</ta>
            <ta e="T68" id="Seg_2494" s="T67">брат-3SG</ta>
            <ta e="T69" id="Seg_2495" s="T68">жена-ACC-3SG</ta>
            <ta e="T70" id="Seg_2496" s="T69">поймать-CVB</ta>
            <ta e="T71" id="Seg_2497" s="T70">бить-INCH-3SG.O</ta>
            <ta e="T72" id="Seg_2498" s="T71">завтраг</ta>
            <ta e="T73" id="Seg_2499" s="T72">брат-3SG</ta>
            <ta e="T74" id="Seg_2500" s="T73">опять</ta>
            <ta e="T75" id="Seg_2501" s="T74">он.сам</ta>
            <ta e="T76" id="Seg_2502" s="T75">половина-CAR-ADV</ta>
            <ta e="T77" id="Seg_2503" s="T76">пойти-FUT.[3SG.S]</ta>
            <ta e="T78" id="Seg_2504" s="T77">слопец-EP-ADJZ</ta>
            <ta e="T79" id="Seg_2505" s="T78">путь-LOC-3SG</ta>
            <ta e="T80" id="Seg_2506" s="T79">сноха.[NOM]</ta>
            <ta e="T81" id="Seg_2507" s="T80">поймать-CVB</ta>
            <ta e="T82" id="Seg_2508" s="T81">Камача-ACC</ta>
            <ta e="T83" id="Seg_2509" s="T82">убить-INCH-3SG.O</ta>
            <ta e="T84" id="Seg_2510" s="T83">Камача.[NOM]</ta>
            <ta e="T85" id="Seg_2511" s="T84">улица-ILL</ta>
            <ta e="T86" id="Seg_2512" s="T85">выходить-CVB</ta>
            <ta e="T87" id="Seg_2513" s="T86">пойти-CVB</ta>
            <ta e="T88" id="Seg_2514" s="T87">куда</ta>
            <ta e="T89" id="Seg_2515" s="T88">глаз-3SG</ta>
            <ta e="T90" id="Seg_2516" s="T89">видеть.[3SG.S]</ta>
            <ta e="T91" id="Seg_2517" s="T90">длинный</ta>
            <ta e="T92" id="Seg_2518" s="T91">ли</ta>
            <ta e="T93" id="Seg_2519" s="T92">или</ta>
            <ta e="T94" id="Seg_2520" s="T93">короткий-ADV</ta>
            <ta e="T95" id="Seg_2521" s="T94">бегать-OPT.[3SG.S]</ta>
            <ta e="T96" id="Seg_2522" s="T95">увидеть-IPFV3-FRQ-CO-3SG.O</ta>
            <ta e="T97" id="Seg_2523" s="T96">ворон.[NOM]</ta>
            <ta e="T98" id="Seg_2524" s="T97">лететь-IPFV2-INFER.[3SG.S]</ta>
            <ta e="T99" id="Seg_2525" s="T98">рот-EP-ILL</ta>
            <ta e="T100" id="Seg_2526" s="T99">рыба.[NOM]</ta>
            <ta e="T101" id="Seg_2527" s="T100">принести-FRQ-EP-INFER-3SG.O</ta>
            <ta e="T102" id="Seg_2528" s="T101">что-PROL</ta>
            <ta e="T103" id="Seg_2529" s="T102">ворон.[NOM]</ta>
            <ta e="T104" id="Seg_2530" s="T103">лететь-DRV-IPFV2.[3SG.S]</ta>
            <ta e="T105" id="Seg_2531" s="T104">Камача.[NOM]</ta>
            <ta e="T106" id="Seg_2532" s="T105">туда</ta>
            <ta e="T107" id="Seg_2533" s="T106">вниз</ta>
            <ta e="T108" id="Seg_2534" s="T107">шевелиться-PFV-FUT.[3SG.S]</ta>
            <ta e="T109" id="Seg_2535" s="T108">достичь-FUT.[3SG.S]</ta>
            <ta e="T110" id="Seg_2536" s="T109">река-DIM.[NOM]</ta>
            <ta e="T111" id="Seg_2537" s="T110">река-DIM-LOC</ta>
            <ta e="T112" id="Seg_2538" s="T111">рыба.[NOM]</ta>
            <ta e="T113" id="Seg_2539" s="T112">много-%%</ta>
            <ta e="T114" id="Seg_2540" s="T113">он(а).[NOM]</ta>
            <ta e="T115" id="Seg_2541" s="T114">срубить-EP-PFV-RES-3SG.O</ta>
            <ta e="T116" id="Seg_2542" s="T115">черёмуха-ACC</ta>
            <ta e="T117" id="Seg_2543" s="T116">‎‎сачок-ACC</ta>
            <ta e="T118" id="Seg_2544" s="T117">делать-PFV-FUT.[3SG.S]</ta>
            <ta e="T119" id="Seg_2545" s="T118">и</ta>
            <ta e="T120" id="Seg_2546" s="T119">давай</ta>
            <ta e="T121" id="Seg_2547" s="T120">рыба-ACC</ta>
            <ta e="T122" id="Seg_2548" s="T121">черпать-RES-IPFV2-INF</ta>
            <ta e="T123" id="Seg_2549" s="T122">огонь-ACC</ta>
            <ta e="T124" id="Seg_2550" s="T123">зажечь.огонь-INCH-3SG.O</ta>
            <ta e="T125" id="Seg_2551" s="T124">окунь-ACC</ta>
            <ta e="T126" id="Seg_2552" s="T125">утро.[NOM]</ta>
            <ta e="T127" id="Seg_2553" s="T126">класть-PFV-MULTS-RES-3SG.O</ta>
            <ta e="T128" id="Seg_2554" s="T127">%%-INSTR</ta>
            <ta e="T129" id="Seg_2555" s="T128">вдруг</ta>
            <ta e="T130" id="Seg_2556" s="T129">смотреть-PFV-FUT.[3SG.S]</ta>
            <ta e="T131" id="Seg_2557" s="T130">зверь.[NOM]</ta>
            <ta e="T132" id="Seg_2558" s="T131">прийти-IPFV2-INFER.[3SG.S]</ta>
            <ta e="T133" id="Seg_2559" s="T132">Камача.[NOM]</ta>
            <ta e="T134" id="Seg_2560" s="T133">что</ta>
            <ta e="T135" id="Seg_2561" s="T134">делать-IPFV2-INFER-2SG.O</ta>
            <ta e="T136" id="Seg_2562" s="T135">почка-1SG</ta>
            <ta e="T137" id="Seg_2563" s="T136">прочь</ta>
            <ta e="T138" id="Seg_2564" s="T137">вырыть-EP-1SG.S</ta>
            <ta e="T139" id="Seg_2565" s="T138">огонь-ILL</ta>
            <ta e="T140" id="Seg_2566" s="T139">гореть-CO-1SG.O</ta>
            <ta e="T141" id="Seg_2567" s="T140">и</ta>
            <ta e="T142" id="Seg_2568" s="T141">есть-INF</ta>
            <ta e="T143" id="Seg_2569" s="T142">хотеть-1SG.S</ta>
            <ta e="T144" id="Seg_2570" s="T143">а</ta>
            <ta e="T145" id="Seg_2571" s="T144">зверь.[NOM]</ta>
            <ta e="T146" id="Seg_2572" s="T145">сказать-INCH-DUR-CO.[3SG.S]</ta>
            <ta e="T147" id="Seg_2573" s="T146">я.NOM-EMPH</ta>
            <ta e="T148" id="Seg_2574" s="T147">быть.голодным-1SG.S</ta>
            <ta e="T149" id="Seg_2575" s="T148">огонь-ILL</ta>
            <ta e="T150" id="Seg_2576" s="T149">на.другую.сторону</ta>
            <ta e="T151" id="Seg_2577" s="T150">сесть-IMP.2SG.S</ta>
            <ta e="T152" id="Seg_2578" s="T151">рыба.[NOM]</ta>
            <ta e="T153" id="Seg_2579" s="T152">поджарить-DRV.[3SG.S]</ta>
            <ta e="T154" id="Seg_2580" s="T153">один</ta>
            <ta e="T155" id="Seg_2581" s="T154">%%-EP-ACC</ta>
            <ta e="T156" id="Seg_2582" s="T155">зверь-ALL</ta>
            <ta e="T157" id="Seg_2583" s="T156">бросать-2SG.O-3SG.O</ta>
            <ta e="T158" id="Seg_2584" s="T157">зверь.[NOM]</ta>
            <ta e="T159" id="Seg_2585" s="T158">есть-EP-RES-3SG.O</ta>
            <ta e="T160" id="Seg_2586" s="T159">сказать-INCH-DUR-CO.[3SG.S]</ta>
            <ta e="T161" id="Seg_2587" s="T160">хороший</ta>
            <ta e="T162" id="Seg_2588" s="T161">почка.[NOM]</ta>
            <ta e="T163" id="Seg_2589" s="T162">я.NOM-ADES</ta>
            <ta e="T164" id="Seg_2590" s="T163">прочь</ta>
            <ta e="T165" id="Seg_2591" s="T164">вырыть-IMP.2SG.S</ta>
            <ta e="T166" id="Seg_2592" s="T165">а</ta>
            <ta e="T167" id="Seg_2593" s="T166">Камача.[NOM]</ta>
            <ta e="T168" id="Seg_2594" s="T167">сказать-INCH-DUR-CO.[3SG.S]</ta>
            <ta e="T169" id="Seg_2595" s="T168">ты.[NOM]</ta>
            <ta e="T170" id="Seg_2596" s="T169">я.ACC</ta>
            <ta e="T171" id="Seg_2597" s="T170">NEG</ta>
            <ta e="T172" id="Seg_2598" s="T171">есть-EP-FUT-2SG.S</ta>
            <ta e="T173" id="Seg_2599" s="T172">больно</ta>
            <ta e="T174" id="Seg_2600" s="T173">вырыть-EP-IPFV2-OPT-HAB</ta>
            <ta e="T175" id="Seg_2601" s="T174">почка.[NOM]</ta>
            <ta e="T176" id="Seg_2602" s="T175">зверь.[NOM]</ta>
            <ta e="T177" id="Seg_2603" s="T176">сказать-INCH-DUR-CO.[3SG.S]</ta>
            <ta e="T178" id="Seg_2604" s="T177">вырыть-IMP.2SG.S</ta>
            <ta e="T179" id="Seg_2605" s="T178">терпеть-FUT-1SG.O</ta>
            <ta e="T180" id="Seg_2606" s="T179">Камача.[NOM]</ta>
            <ta e="T181" id="Seg_2607" s="T180">сказать-INCH-DUR-CO.[3SG.S]</ta>
            <ta e="T182" id="Seg_2608" s="T181">зверь-ALL</ta>
            <ta e="T183" id="Seg_2609" s="T182">навзничь</ta>
            <ta e="T184" id="Seg_2610" s="T183">лечь-IMP.2SG.S</ta>
            <ta e="T185" id="Seg_2611" s="T184">зверь.[NOM]</ta>
            <ta e="T186" id="Seg_2612" s="T185">лечь-FUT.[3SG.S]</ta>
            <ta e="T187" id="Seg_2613" s="T186">он(а).[NOM]</ta>
            <ta e="T188" id="Seg_2614" s="T187">острый</ta>
            <ta e="T189" id="Seg_2615" s="T188">нож-INSTR</ta>
            <ta e="T190" id="Seg_2616" s="T189">живот-ACC-3SG</ta>
            <ta e="T191" id="Seg_2617" s="T190">прочь</ta>
            <ta e="T192" id="Seg_2618" s="T191">распороть-PFV-RES-3SG</ta>
            <ta e="T193" id="Seg_2619" s="T192">и</ta>
            <ta e="T194" id="Seg_2620" s="T193">зверь.[NOM]</ta>
            <ta e="T195" id="Seg_2621" s="T194">здесь</ta>
            <ta e="T196" id="Seg_2622" s="T195">умирать-FUT.[3SG.S]</ta>
            <ta e="T197" id="Seg_2623" s="T196">Камача.[NOM]</ta>
            <ta e="T198" id="Seg_2624" s="T197">прочь</ta>
            <ta e="T199" id="Seg_2625" s="T198">шкура-ACC-3SG</ta>
            <ta e="T200" id="Seg_2626" s="T199">ободрать-PFV.[3SG.S]</ta>
            <ta e="T201" id="Seg_2627" s="T200">мясо-ACC-3SG</ta>
            <ta e="T202" id="Seg_2628" s="T201">весь</ta>
            <ta e="T203" id="Seg_2629" s="T202">убрать-INCH-3SG.O</ta>
            <ta e="T204" id="Seg_2630" s="T203">полнота-ADJZ</ta>
            <ta e="T205" id="Seg_2631" s="T204">мешок-ILL</ta>
            <ta e="T206" id="Seg_2632" s="T205">мясо-ACC</ta>
            <ta e="T207" id="Seg_2633" s="T206">положить-CVB</ta>
            <ta e="T208" id="Seg_2634" s="T207">и</ta>
            <ta e="T209" id="Seg_2635" s="T208">рыба-ACC</ta>
            <ta e="T210" id="Seg_2636" s="T209">положить-CVB</ta>
            <ta e="T211" id="Seg_2637" s="T210">положить-EP-RES-3SG.O</ta>
            <ta e="T212" id="Seg_2638" s="T211">и</ta>
            <ta e="T213" id="Seg_2639" s="T212">дома</ta>
            <ta e="T214" id="Seg_2640" s="T213">пойти-%%-OPT.[3SG.S]</ta>
            <ta e="T215" id="Seg_2641" s="T214">дома-LOC-3SG</ta>
            <ta e="T216" id="Seg_2642" s="T215">достичь-IPFV2.[3SG.S]</ta>
            <ta e="T217" id="Seg_2643" s="T216">дым-3SG</ta>
            <ta e="T218" id="Seg_2644" s="T217">NEG</ta>
            <ta e="T219" id="Seg_2645" s="T218">дымить-PST.NAR.[3SG.S]</ta>
            <ta e="T220" id="Seg_2646" s="T219">дом.[NOM]</ta>
            <ta e="T221" id="Seg_2647" s="T220">верхняя.часть-ILL.3SG</ta>
            <ta e="T222" id="Seg_2648" s="T221">наверх</ta>
            <ta e="T223" id="Seg_2649" s="T222">выбежать-FUT.[3SG.S]</ta>
            <ta e="T224" id="Seg_2650" s="T223">%%-GEN</ta>
            <ta e="T225" id="Seg_2651" s="T224">верхняя.часть.[NOM]</ta>
            <ta e="T226" id="Seg_2652" s="T225">вниз</ta>
            <ta e="T227" id="Seg_2653" s="T226">смотреть-PST.NAR.[3SG.S]</ta>
            <ta e="T228" id="Seg_2654" s="T227">брат-3SG</ta>
            <ta e="T229" id="Seg_2655" s="T228">и</ta>
            <ta e="T230" id="Seg_2656" s="T229">сноха-3SG</ta>
            <ta e="T231" id="Seg_2657" s="T230">один</ta>
            <ta e="T232" id="Seg_2658" s="T231">огонь.[NOM]</ta>
            <ta e="T233" id="Seg_2659" s="T232">глаз.[NOM]</ta>
            <ta e="T234" id="Seg_2660" s="T233">делить-IPFV-3DU.O</ta>
            <ta e="T235" id="Seg_2661" s="T234">этот</ta>
            <ta e="T236" id="Seg_2662" s="T235">I.GEN</ta>
            <ta e="T237" id="Seg_2663" s="T236">Камача-1SG</ta>
            <ta e="T238" id="Seg_2664" s="T237">он(а).[NOM]</ta>
            <ta e="T239" id="Seg_2665" s="T238">туда-PROL</ta>
            <ta e="T240" id="Seg_2666" s="T239">вниз</ta>
            <ta e="T241" id="Seg_2667" s="T240">кричать-FUT.[3SG.S]</ta>
            <ta e="T242" id="Seg_2668" s="T241">я.NOM</ta>
            <ta e="T243" id="Seg_2669" s="T242">там_быть-CO-1SG.S</ta>
            <ta e="T244" id="Seg_2670" s="T243">здесь</ta>
            <ta e="T245" id="Seg_2671" s="T244">он(а)-EP-DU-LOC</ta>
            <ta e="T246" id="Seg_2672" s="T245">мясо-ACC</ta>
            <ta e="T247" id="Seg_2673" s="T246">и</ta>
            <ta e="T248" id="Seg_2674" s="T247">рыба-ACC</ta>
            <ta e="T249" id="Seg_2675" s="T248">дать-CO-1SG.S</ta>
            <ta e="T250" id="Seg_2676" s="T249">и</ta>
            <ta e="T251" id="Seg_2677" s="T250">рыба-INSTR</ta>
            <ta e="T252" id="Seg_2678" s="T251">есть-EP-TR-RES-3SG.O</ta>
            <ta e="T253" id="Seg_2679" s="T252">и</ta>
            <ta e="T254" id="Seg_2680" s="T253">потом</ta>
            <ta e="T255" id="Seg_2681" s="T254">жить-INCH-DUR-CO-3PL</ta>
            <ta e="T256" id="Seg_2682" s="T255">и</ta>
            <ta e="T257" id="Seg_2683" s="T256">жить-INCH-DUR-CO-3PL</ta>
            <ta e="T258" id="Seg_2684" s="T257">дочь-ACC</ta>
            <ta e="T259" id="Seg_2685" s="T258">сын-ACC</ta>
            <ta e="T260" id="Seg_2686" s="T259">лечь-DUR-INFER-3SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2687" s="T0">num</ta>
            <ta e="T2" id="Seg_2688" s="T1">n-n&gt;n-n:num</ta>
            <ta e="T3" id="Seg_2689" s="T2">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T4" id="Seg_2690" s="T3">adj</ta>
            <ta e="T5" id="Seg_2691" s="T4">n-n:case1</ta>
            <ta e="T6" id="Seg_2692" s="T5">v-v&gt;v-v&gt;adv</ta>
            <ta e="T7" id="Seg_2693" s="T6">v-v:pn</ta>
            <ta e="T8" id="Seg_2694" s="T7">adj</ta>
            <ta e="T9" id="Seg_2695" s="T8">n-n:poss</ta>
            <ta e="T10" id="Seg_2696" s="T9">nprop-v:pn</ta>
            <ta e="T11" id="Seg_2697" s="T10">n-n:poss</ta>
            <ta e="T12" id="Seg_2698" s="T11">pers-infl:ins-n:case1</ta>
            <ta e="T13" id="Seg_2699" s="T12">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T14" id="Seg_2700" s="T13">v-v:pn</ta>
            <ta e="T15" id="Seg_2701" s="T14">n-n&gt;n-n:num</ta>
            <ta e="T16" id="Seg_2702" s="T15">n-n&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T17" id="Seg_2703" s="T16">n-n:case1</ta>
            <ta e="T18" id="Seg_2704" s="T17">v-infl:ins-v&gt;v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T19" id="Seg_2705" s="T18">adj-n:case1</ta>
            <ta e="T20" id="Seg_2706" s="T19">n-n:poss</ta>
            <ta e="T21" id="Seg_2707" s="T20">emph</ta>
            <ta e="T22" id="Seg_2708" s="T21">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T23" id="Seg_2709" s="T22">v-v&gt;adv</ta>
            <ta e="T24" id="Seg_2710" s="T23">n-infl:ins-n&gt;adj</ta>
            <ta e="T25" id="Seg_2711" s="T24">n-infl:ins-n:case1-n:poss</ta>
            <ta e="T26" id="Seg_2712" s="T25">n-n:poss</ta>
            <ta e="T27" id="Seg_2713" s="T26">nprop-n:case1</ta>
            <ta e="T28" id="Seg_2714" s="T27">v-infl:ins-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T29" id="Seg_2715" s="T28">n-n:case2</ta>
            <ta e="T30" id="Seg_2716" s="T29">n-n:case2</ta>
            <ta e="T31" id="Seg_2717" s="T30">conj</ta>
            <ta e="T32" id="Seg_2718" s="T31">n-n:case2</ta>
            <ta e="T33" id="Seg_2719" s="T32">adv</ta>
            <ta e="T34" id="Seg_2720" s="T33">num</ta>
            <ta e="T35" id="Seg_2721" s="T34">interrog-infl:ins-adv:case</ta>
            <ta e="T36" id="Seg_2722" s="T35">v-v:mood-v:pn</ta>
            <ta e="T37" id="Seg_2723" s="T36">num-n-n:num-n:case1</ta>
            <ta e="T38" id="Seg_2724" s="T37">nprop-n:case2</ta>
            <ta e="T39" id="Seg_2725" s="T38">pp</ta>
            <ta e="T40" id="Seg_2726" s="T39">n-n:case1</ta>
            <ta e="T41" id="Seg_2727" s="T40">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T42" id="Seg_2728" s="T41">nprop-n:case1</ta>
            <ta e="T43" id="Seg_2729" s="T42">v-v:ins-v:pn</ta>
            <ta e="T44" id="Seg_2730" s="T43">n-n:case1</ta>
            <ta e="T45" id="Seg_2731" s="T44">n-n:case1</ta>
            <ta e="T46" id="Seg_2732" s="T45">n-n:poss</ta>
            <ta e="T47" id="Seg_2733" s="T46">conj</ta>
            <ta e="T48" id="Seg_2734" s="T47">n-n:poss</ta>
            <ta e="T49" id="Seg_2735" s="T48">pers</ta>
            <ta e="T50" id="Seg_2736" s="T49">v-v:mood-pn</ta>
            <ta e="T51" id="Seg_2737" s="T50">n-n:poss</ta>
            <ta e="T52" id="Seg_2738" s="T51">v-v&gt;v-v&gt;adv</ta>
            <ta e="T53" id="Seg_2739" s="T52">interrog-n:case1</ta>
            <ta e="T54" id="Seg_2740" s="T53">dem-adj&gt;adv</ta>
            <ta e="T55" id="Seg_2741" s="T54">v-v:ins-v:pn</ta>
            <ta e="T56" id="Seg_2742" s="T55">conj</ta>
            <ta e="T57" id="Seg_2743" s="T56">nprop-n:case1</ta>
            <ta e="T58" id="Seg_2744" s="T57">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T59" id="Seg_2745" s="T58">pers</ta>
            <ta e="T60" id="Seg_2746" s="T59">n-n:poss</ta>
            <ta e="T61" id="Seg_2747" s="T60">n-n:case2</ta>
            <ta e="T62" id="Seg_2748" s="T61">conj</ta>
            <ta e="T63" id="Seg_2749" s="T62">n-n:case2</ta>
            <ta e="T64" id="Seg_2750" s="T63">v-infl:ins-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T65" id="Seg_2751" s="T64">n-adv:case</ta>
            <ta e="T66" id="Seg_2752" s="T65">adv</ta>
            <ta e="T67" id="Seg_2753" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_2754" s="T67">n-n:poss</ta>
            <ta e="T69" id="Seg_2755" s="T68">n-n:case1-n:poss</ta>
            <ta e="T70" id="Seg_2756" s="T69">v-v&gt;adv</ta>
            <ta e="T71" id="Seg_2757" s="T70">v-v&gt;v-v:pn</ta>
            <ta e="T72" id="Seg_2758" s="T71">adv</ta>
            <ta e="T73" id="Seg_2759" s="T72">n-n:poss</ta>
            <ta e="T74" id="Seg_2760" s="T73">adv</ta>
            <ta e="T75" id="Seg_2761" s="T74">emph</ta>
            <ta e="T76" id="Seg_2762" s="T75">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T77" id="Seg_2763" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_2764" s="T77">n-infl:ins-n&gt;adj</ta>
            <ta e="T79" id="Seg_2765" s="T78">n-n:case1-n:poss</ta>
            <ta e="T80" id="Seg_2766" s="T79">n-n:case1</ta>
            <ta e="T81" id="Seg_2767" s="T80">v-v&gt;adv</ta>
            <ta e="T82" id="Seg_2768" s="T81">nprop-n:case1</ta>
            <ta e="T83" id="Seg_2769" s="T82">v-v&gt;v-v:pn</ta>
            <ta e="T84" id="Seg_2770" s="T83">nprop-n:case1</ta>
            <ta e="T85" id="Seg_2771" s="T84">n-n:case2</ta>
            <ta e="T86" id="Seg_2772" s="T85">v-v&gt;adv</ta>
            <ta e="T87" id="Seg_2773" s="T86">v-v&gt;adv</ta>
            <ta e="T88" id="Seg_2774" s="T87">interrog</ta>
            <ta e="T89" id="Seg_2775" s="T88">n-n:poss</ta>
            <ta e="T90" id="Seg_2776" s="T89">v-v:pn</ta>
            <ta e="T91" id="Seg_2777" s="T90">adj</ta>
            <ta e="T92" id="Seg_2778" s="T91">conj</ta>
            <ta e="T93" id="Seg_2779" s="T92">conj</ta>
            <ta e="T94" id="Seg_2780" s="T93">adj-adj&gt;adv</ta>
            <ta e="T95" id="Seg_2781" s="T94">v-v:mood-v:pn</ta>
            <ta e="T96" id="Seg_2782" s="T95">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T97" id="Seg_2783" s="T96">n-n:case1</ta>
            <ta e="T98" id="Seg_2784" s="T97">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T99" id="Seg_2785" s="T98">n-infl:ins-n:case2</ta>
            <ta e="T100" id="Seg_2786" s="T99">n-n:case1</ta>
            <ta e="T101" id="Seg_2787" s="T100">v-v&gt;v-infl:ins-v:mood-v:pn</ta>
            <ta e="T102" id="Seg_2788" s="T101">interrog-n:case1</ta>
            <ta e="T103" id="Seg_2789" s="T102">n-n:case1</ta>
            <ta e="T104" id="Seg_2790" s="T103">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T105" id="Seg_2791" s="T104">nprop-n:case1</ta>
            <ta e="T106" id="Seg_2792" s="T105">adv</ta>
            <ta e="T107" id="Seg_2793" s="T106">preverb</ta>
            <ta e="T108" id="Seg_2794" s="T107">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_2795" s="T108">v-v:tense-v:pn</ta>
            <ta e="T110" id="Seg_2796" s="T109">n-n&gt;n-n:case1</ta>
            <ta e="T111" id="Seg_2797" s="T110">n-n&gt;n-n:case1</ta>
            <ta e="T112" id="Seg_2798" s="T111">n-n:case1</ta>
            <ta e="T113" id="Seg_2799" s="T112">quant-any</ta>
            <ta e="T114" id="Seg_2800" s="T113">pers-n:case1</ta>
            <ta e="T115" id="Seg_2801" s="T114">v-infl:ins-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T116" id="Seg_2802" s="T115">n-n:case1</ta>
            <ta e="T117" id="Seg_2803" s="T116">n-n:case1</ta>
            <ta e="T118" id="Seg_2804" s="T117">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_2805" s="T118">conj</ta>
            <ta e="T120" id="Seg_2806" s="T119">ptcp</ta>
            <ta e="T121" id="Seg_2807" s="T120">n-n:case1</ta>
            <ta e="T122" id="Seg_2808" s="T121">v-v&gt;v-v&gt;v-v:ninf</ta>
            <ta e="T123" id="Seg_2809" s="T122">n-n:case1</ta>
            <ta e="T124" id="Seg_2810" s="T123">v-v&gt;v-v:pn</ta>
            <ta e="T125" id="Seg_2811" s="T124">n-n:case1</ta>
            <ta e="T126" id="Seg_2812" s="T125">n-n:case1</ta>
            <ta e="T127" id="Seg_2813" s="T126">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T128" id="Seg_2814" s="T127">n-n:case2</ta>
            <ta e="T129" id="Seg_2815" s="T128">adv</ta>
            <ta e="T130" id="Seg_2816" s="T129">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_2817" s="T130">n-n:case1</ta>
            <ta e="T132" id="Seg_2818" s="T131">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T133" id="Seg_2819" s="T132">nprop-n:case1</ta>
            <ta e="T134" id="Seg_2820" s="T133">interrog</ta>
            <ta e="T135" id="Seg_2821" s="T134">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T136" id="Seg_2822" s="T135">n-n:poss</ta>
            <ta e="T137" id="Seg_2823" s="T136">preverb</ta>
            <ta e="T138" id="Seg_2824" s="T137">v-infl:ins-v:pn</ta>
            <ta e="T139" id="Seg_2825" s="T138">n-n:case2</ta>
            <ta e="T140" id="Seg_2826" s="T139">v-v:ins-v:pn</ta>
            <ta e="T141" id="Seg_2827" s="T140">conj</ta>
            <ta e="T142" id="Seg_2828" s="T141">v-v:ninf</ta>
            <ta e="T143" id="Seg_2829" s="T142">v-v:pn</ta>
            <ta e="T144" id="Seg_2830" s="T143">conj</ta>
            <ta e="T145" id="Seg_2831" s="T144">n-n:case1</ta>
            <ta e="T146" id="Seg_2832" s="T145">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T147" id="Seg_2833" s="T146">pers-clit</ta>
            <ta e="T148" id="Seg_2834" s="T147">v-v:pn</ta>
            <ta e="T149" id="Seg_2835" s="T148">n-n:case2</ta>
            <ta e="T150" id="Seg_2836" s="T149">adv</ta>
            <ta e="T151" id="Seg_2837" s="T150">v-v:mood-pn</ta>
            <ta e="T152" id="Seg_2838" s="T151">n-n:case1</ta>
            <ta e="T153" id="Seg_2839" s="T152">v-v&gt;v-v:pn</ta>
            <ta e="T154" id="Seg_2840" s="T153">num</ta>
            <ta e="T155" id="Seg_2841" s="T154">n-infl:ins-n:case1</ta>
            <ta e="T156" id="Seg_2842" s="T155">n-n:case2</ta>
            <ta e="T157" id="Seg_2843" s="T156">v-v:pn-v:pn</ta>
            <ta e="T158" id="Seg_2844" s="T157">n-n:case1</ta>
            <ta e="T159" id="Seg_2845" s="T158">v-infl:ins-v&gt;v-v:pn</ta>
            <ta e="T160" id="Seg_2846" s="T159">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T161" id="Seg_2847" s="T160">adj</ta>
            <ta e="T162" id="Seg_2848" s="T161">n-n:case1</ta>
            <ta e="T163" id="Seg_2849" s="T162">pers-n:case2</ta>
            <ta e="T164" id="Seg_2850" s="T163">preverb</ta>
            <ta e="T165" id="Seg_2851" s="T164">v-v:mood-pn</ta>
            <ta e="T166" id="Seg_2852" s="T165">conj</ta>
            <ta e="T167" id="Seg_2853" s="T166">nprop-n:case1</ta>
            <ta e="T168" id="Seg_2854" s="T167">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T169" id="Seg_2855" s="T168">pers-n:case1</ta>
            <ta e="T170" id="Seg_2856" s="T169">pers</ta>
            <ta e="T171" id="Seg_2857" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_2858" s="T171">v-infl:ins-v:tense-v:pn</ta>
            <ta e="T173" id="Seg_2859" s="T172">adv</ta>
            <ta e="T174" id="Seg_2860" s="T173">v-infl:ins-v&gt;v-v:mood-v&gt;v</ta>
            <ta e="T175" id="Seg_2861" s="T174">n-n:case1</ta>
            <ta e="T176" id="Seg_2862" s="T175">n-n:case1</ta>
            <ta e="T177" id="Seg_2863" s="T176">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T178" id="Seg_2864" s="T177">v-v:mood-pn</ta>
            <ta e="T179" id="Seg_2865" s="T178">v-v:tense-v:pn</ta>
            <ta e="T180" id="Seg_2866" s="T179">nprop-n:case1</ta>
            <ta e="T181" id="Seg_2867" s="T180">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T182" id="Seg_2868" s="T181">n-n:case2</ta>
            <ta e="T183" id="Seg_2869" s="T182">adv</ta>
            <ta e="T184" id="Seg_2870" s="T183">v-v:mood-pn</ta>
            <ta e="T185" id="Seg_2871" s="T184">n-n:case1</ta>
            <ta e="T186" id="Seg_2872" s="T185">v-v:tense-v:pn</ta>
            <ta e="T187" id="Seg_2873" s="T186">pers-n:case1</ta>
            <ta e="T188" id="Seg_2874" s="T187">adj</ta>
            <ta e="T189" id="Seg_2875" s="T188">n-n:case2</ta>
            <ta e="T190" id="Seg_2876" s="T189">n-n:case1-n:poss</ta>
            <ta e="T191" id="Seg_2877" s="T190">preverb</ta>
            <ta e="T192" id="Seg_2878" s="T191">v-v&gt;v-v&gt;v-n:poss</ta>
            <ta e="T193" id="Seg_2879" s="T192">conj</ta>
            <ta e="T194" id="Seg_2880" s="T193">n-n:case1</ta>
            <ta e="T195" id="Seg_2881" s="T194">adv</ta>
            <ta e="T196" id="Seg_2882" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_2883" s="T196">nprop-n:case1</ta>
            <ta e="T198" id="Seg_2884" s="T197">preverb</ta>
            <ta e="T199" id="Seg_2885" s="T198">n-n:case1-n:poss</ta>
            <ta e="T200" id="Seg_2886" s="T199">v-v&gt;v-v:pn</ta>
            <ta e="T201" id="Seg_2887" s="T200">n-n:case1-n:poss</ta>
            <ta e="T202" id="Seg_2888" s="T201">quant</ta>
            <ta e="T203" id="Seg_2889" s="T202">v-v&gt;v-v:pn</ta>
            <ta e="T204" id="Seg_2890" s="T203">n-n&gt;adj</ta>
            <ta e="T205" id="Seg_2891" s="T204">n-n:case2</ta>
            <ta e="T206" id="Seg_2892" s="T205">n-n:case1</ta>
            <ta e="T207" id="Seg_2893" s="T206">v-v&gt;adv</ta>
            <ta e="T208" id="Seg_2894" s="T207">conj</ta>
            <ta e="T209" id="Seg_2895" s="T208">n-n:case1</ta>
            <ta e="T210" id="Seg_2896" s="T209">v-v&gt;adv</ta>
            <ta e="T211" id="Seg_2897" s="T210">v-infl:ins-v&gt;v-v:pn</ta>
            <ta e="T212" id="Seg_2898" s="T211">conj</ta>
            <ta e="T213" id="Seg_2899" s="T212">adv</ta>
            <ta e="T214" id="Seg_2900" s="T213">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T215" id="Seg_2901" s="T214">adv-n:case1-n:poss</ta>
            <ta e="T216" id="Seg_2902" s="T215">v-v&gt;v-v:pn</ta>
            <ta e="T217" id="Seg_2903" s="T216">n-n:poss</ta>
            <ta e="T218" id="Seg_2904" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_2905" s="T218">v-v:tense-v:pn</ta>
            <ta e="T220" id="Seg_2906" s="T219">n-n:case1</ta>
            <ta e="T221" id="Seg_2907" s="T220">n-n:poss-case</ta>
            <ta e="T222" id="Seg_2908" s="T221">adv</ta>
            <ta e="T223" id="Seg_2909" s="T222">v-v:tense-v:pn</ta>
            <ta e="T224" id="Seg_2910" s="T223">n-n:case2</ta>
            <ta e="T225" id="Seg_2911" s="T224">n-n:case1</ta>
            <ta e="T226" id="Seg_2912" s="T225">preverb</ta>
            <ta e="T227" id="Seg_2913" s="T226">v-v:tense-v:pn</ta>
            <ta e="T228" id="Seg_2914" s="T227">n-n:poss</ta>
            <ta e="T229" id="Seg_2915" s="T228">conj</ta>
            <ta e="T230" id="Seg_2916" s="T229">n-n:poss</ta>
            <ta e="T231" id="Seg_2917" s="T230">num</ta>
            <ta e="T232" id="Seg_2918" s="T231">n-n:case1</ta>
            <ta e="T233" id="Seg_2919" s="T232">n-n:case1</ta>
            <ta e="T234" id="Seg_2920" s="T233">v-v&gt;v-v:pn</ta>
            <ta e="T235" id="Seg_2921" s="T234">dem</ta>
            <ta e="T236" id="Seg_2922" s="T235">pers</ta>
            <ta e="T237" id="Seg_2923" s="T236">nprop-n:poss</ta>
            <ta e="T238" id="Seg_2924" s="T237">pers-n:case1</ta>
            <ta e="T239" id="Seg_2925" s="T238">adv-n:case2</ta>
            <ta e="T240" id="Seg_2926" s="T239">preverb</ta>
            <ta e="T241" id="Seg_2927" s="T240">v-v:tense-v:pn</ta>
            <ta e="T242" id="Seg_2928" s="T241">pers</ta>
            <ta e="T243" id="Seg_2929" s="T242">adv-v-v:ins-v:pn</ta>
            <ta e="T244" id="Seg_2930" s="T243">adv</ta>
            <ta e="T245" id="Seg_2931" s="T244">pers-infl:ins-n:num-n:case1</ta>
            <ta e="T246" id="Seg_2932" s="T245">n-n:case1</ta>
            <ta e="T247" id="Seg_2933" s="T246">conj</ta>
            <ta e="T248" id="Seg_2934" s="T247">n-n:case1</ta>
            <ta e="T249" id="Seg_2935" s="T248">v-v:ins-v:pn</ta>
            <ta e="T250" id="Seg_2936" s="T249">conj</ta>
            <ta e="T251" id="Seg_2937" s="T250">n-n:case2</ta>
            <ta e="T252" id="Seg_2938" s="T251">v-infl:ins-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T253" id="Seg_2939" s="T252">conj</ta>
            <ta e="T254" id="Seg_2940" s="T253">adv</ta>
            <ta e="T255" id="Seg_2941" s="T254">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T256" id="Seg_2942" s="T255">conj</ta>
            <ta e="T257" id="Seg_2943" s="T256">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T258" id="Seg_2944" s="T257">n-n:case1</ta>
            <ta e="T259" id="Seg_2945" s="T258">n-n:case1</ta>
            <ta e="T260" id="Seg_2946" s="T259">v-v&gt;v-v:mood-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2947" s="T0">num</ta>
            <ta e="T2" id="Seg_2948" s="T1">n</ta>
            <ta e="T3" id="Seg_2949" s="T2">v</ta>
            <ta e="T4" id="Seg_2950" s="T3">adj</ta>
            <ta e="T5" id="Seg_2951" s="T4">n</ta>
            <ta e="T6" id="Seg_2952" s="T5">adv</ta>
            <ta e="T7" id="Seg_2953" s="T6">v</ta>
            <ta e="T8" id="Seg_2954" s="T7">v</ta>
            <ta e="T9" id="Seg_2955" s="T8">n</ta>
            <ta e="T10" id="Seg_2956" s="T9">nprop</ta>
            <ta e="T11" id="Seg_2957" s="T10">n</ta>
            <ta e="T12" id="Seg_2958" s="T11">pers</ta>
            <ta e="T13" id="Seg_2959" s="T12">adv</ta>
            <ta e="T14" id="Seg_2960" s="T13">v</ta>
            <ta e="T15" id="Seg_2961" s="T14">n</ta>
            <ta e="T16" id="Seg_2962" s="T15">v</ta>
            <ta e="T17" id="Seg_2963" s="T16">n</ta>
            <ta e="T18" id="Seg_2964" s="T17">v</ta>
            <ta e="T19" id="Seg_2965" s="T18">adj</ta>
            <ta e="T20" id="Seg_2966" s="T19">n</ta>
            <ta e="T21" id="Seg_2967" s="T20">emph</ta>
            <ta e="T22" id="Seg_2968" s="T21">adv</ta>
            <ta e="T23" id="Seg_2969" s="T22">adv</ta>
            <ta e="T24" id="Seg_2970" s="T23">v</ta>
            <ta e="T25" id="Seg_2971" s="T24">n</ta>
            <ta e="T26" id="Seg_2972" s="T25">n</ta>
            <ta e="T27" id="Seg_2973" s="T26">nprop</ta>
            <ta e="T28" id="Seg_2974" s="T27">v</ta>
            <ta e="T29" id="Seg_2975" s="T28">n</ta>
            <ta e="T30" id="Seg_2976" s="T29">n</ta>
            <ta e="T31" id="Seg_2977" s="T30">conj</ta>
            <ta e="T32" id="Seg_2978" s="T31">n</ta>
            <ta e="T33" id="Seg_2979" s="T32">adv</ta>
            <ta e="T34" id="Seg_2980" s="T33">num</ta>
            <ta e="T35" id="Seg_2981" s="T34">interrog</ta>
            <ta e="T36" id="Seg_2982" s="T35">v</ta>
            <ta e="T37" id="Seg_2983" s="T36">n</ta>
            <ta e="T38" id="Seg_2984" s="T37">nprop</ta>
            <ta e="T39" id="Seg_2985" s="T38">pp</ta>
            <ta e="T40" id="Seg_2986" s="T39">n</ta>
            <ta e="T41" id="Seg_2987" s="T40">v</ta>
            <ta e="T42" id="Seg_2988" s="T41">nprop</ta>
            <ta e="T43" id="Seg_2989" s="T42">v</ta>
            <ta e="T44" id="Seg_2990" s="T43">n</ta>
            <ta e="T45" id="Seg_2991" s="T44">n</ta>
            <ta e="T46" id="Seg_2992" s="T45">n</ta>
            <ta e="T47" id="Seg_2993" s="T46">conj</ta>
            <ta e="T48" id="Seg_2994" s="T47">n</ta>
            <ta e="T49" id="Seg_2995" s="T48">pers</ta>
            <ta e="T50" id="Seg_2996" s="T49">v</ta>
            <ta e="T51" id="Seg_2997" s="T50">n</ta>
            <ta e="T52" id="Seg_2998" s="T51">adv</ta>
            <ta e="T53" id="Seg_2999" s="T52">interrog</ta>
            <ta e="T54" id="Seg_3000" s="T53">dem</ta>
            <ta e="T55" id="Seg_3001" s="T54">v</ta>
            <ta e="T56" id="Seg_3002" s="T55">conj</ta>
            <ta e="T57" id="Seg_3003" s="T56">nprop</ta>
            <ta e="T58" id="Seg_3004" s="T57">v</ta>
            <ta e="T59" id="Seg_3005" s="T58">pers</ta>
            <ta e="T60" id="Seg_3006" s="T59">n</ta>
            <ta e="T61" id="Seg_3007" s="T60">n</ta>
            <ta e="T62" id="Seg_3008" s="T61">conj</ta>
            <ta e="T63" id="Seg_3009" s="T62">n</ta>
            <ta e="T64" id="Seg_3010" s="T63">v</ta>
            <ta e="T65" id="Seg_3011" s="T64">n</ta>
            <ta e="T66" id="Seg_3012" s="T65">adv</ta>
            <ta e="T67" id="Seg_3013" s="T66">v</ta>
            <ta e="T68" id="Seg_3014" s="T67">n</ta>
            <ta e="T69" id="Seg_3015" s="T68">n</ta>
            <ta e="T70" id="Seg_3016" s="T69">adv</ta>
            <ta e="T71" id="Seg_3017" s="T70">v</ta>
            <ta e="T72" id="Seg_3018" s="T71">n</ta>
            <ta e="T73" id="Seg_3019" s="T72">n</ta>
            <ta e="T74" id="Seg_3020" s="T73">adv</ta>
            <ta e="T75" id="Seg_3021" s="T74">emph</ta>
            <ta e="T76" id="Seg_3022" s="T75">adj</ta>
            <ta e="T77" id="Seg_3023" s="T76">v</ta>
            <ta e="T78" id="Seg_3024" s="T77">v</ta>
            <ta e="T79" id="Seg_3025" s="T78">n</ta>
            <ta e="T80" id="Seg_3026" s="T79">n</ta>
            <ta e="T81" id="Seg_3027" s="T80">v</ta>
            <ta e="T82" id="Seg_3028" s="T81">nprop</ta>
            <ta e="T83" id="Seg_3029" s="T82">v</ta>
            <ta e="T84" id="Seg_3030" s="T83">nprop</ta>
            <ta e="T85" id="Seg_3031" s="T84">n</ta>
            <ta e="T86" id="Seg_3032" s="T85">v</ta>
            <ta e="T87" id="Seg_3033" s="T86">v</ta>
            <ta e="T88" id="Seg_3034" s="T87">interrog</ta>
            <ta e="T89" id="Seg_3035" s="T88">n</ta>
            <ta e="T90" id="Seg_3036" s="T89">v</ta>
            <ta e="T91" id="Seg_3037" s="T90">adj</ta>
            <ta e="T92" id="Seg_3038" s="T91">conj</ta>
            <ta e="T93" id="Seg_3039" s="T92">conj</ta>
            <ta e="T94" id="Seg_3040" s="T93">adj</ta>
            <ta e="T95" id="Seg_3041" s="T94">v</ta>
            <ta e="T96" id="Seg_3042" s="T95">v</ta>
            <ta e="T97" id="Seg_3043" s="T96">n</ta>
            <ta e="T98" id="Seg_3044" s="T97">v</ta>
            <ta e="T99" id="Seg_3045" s="T98">n</ta>
            <ta e="T100" id="Seg_3046" s="T99">n</ta>
            <ta e="T101" id="Seg_3047" s="T100">v</ta>
            <ta e="T102" id="Seg_3048" s="T101">interrog</ta>
            <ta e="T103" id="Seg_3049" s="T102">n</ta>
            <ta e="T104" id="Seg_3050" s="T103">v</ta>
            <ta e="T105" id="Seg_3051" s="T104">nprop</ta>
            <ta e="T106" id="Seg_3052" s="T105">adv</ta>
            <ta e="T107" id="Seg_3053" s="T106">preverb</ta>
            <ta e="T108" id="Seg_3054" s="T107">v</ta>
            <ta e="T109" id="Seg_3055" s="T108">v</ta>
            <ta e="T110" id="Seg_3056" s="T109">v</ta>
            <ta e="T111" id="Seg_3057" s="T110">n</ta>
            <ta e="T112" id="Seg_3058" s="T111">n</ta>
            <ta e="T113" id="Seg_3059" s="T112">quant</ta>
            <ta e="T114" id="Seg_3060" s="T113">pers</ta>
            <ta e="T115" id="Seg_3061" s="T114">v</ta>
            <ta e="T116" id="Seg_3062" s="T115">n</ta>
            <ta e="T117" id="Seg_3063" s="T116">n</ta>
            <ta e="T118" id="Seg_3064" s="T117">v</ta>
            <ta e="T119" id="Seg_3065" s="T118">conj</ta>
            <ta e="T120" id="Seg_3066" s="T119">ptcp</ta>
            <ta e="T121" id="Seg_3067" s="T120">n</ta>
            <ta e="T122" id="Seg_3068" s="T121">v</ta>
            <ta e="T123" id="Seg_3069" s="T122">n</ta>
            <ta e="T124" id="Seg_3070" s="T123">v</ta>
            <ta e="T125" id="Seg_3071" s="T124">n</ta>
            <ta e="T126" id="Seg_3072" s="T125">n</ta>
            <ta e="T127" id="Seg_3073" s="T126">v</ta>
            <ta e="T128" id="Seg_3074" s="T127">n</ta>
            <ta e="T129" id="Seg_3075" s="T128">adv</ta>
            <ta e="T130" id="Seg_3076" s="T129">v</ta>
            <ta e="T131" id="Seg_3077" s="T130">n</ta>
            <ta e="T132" id="Seg_3078" s="T131">v</ta>
            <ta e="T133" id="Seg_3079" s="T132">nprop</ta>
            <ta e="T134" id="Seg_3080" s="T133">interrog</ta>
            <ta e="T135" id="Seg_3081" s="T134">v</ta>
            <ta e="T136" id="Seg_3082" s="T135">n</ta>
            <ta e="T137" id="Seg_3083" s="T136">preverb</ta>
            <ta e="T138" id="Seg_3084" s="T137">v</ta>
            <ta e="T139" id="Seg_3085" s="T138">n</ta>
            <ta e="T140" id="Seg_3086" s="T139">v</ta>
            <ta e="T141" id="Seg_3087" s="T140">conj</ta>
            <ta e="T142" id="Seg_3088" s="T141">v</ta>
            <ta e="T143" id="Seg_3089" s="T142">v</ta>
            <ta e="T144" id="Seg_3090" s="T143">conj</ta>
            <ta e="T145" id="Seg_3091" s="T144">n</ta>
            <ta e="T146" id="Seg_3092" s="T145">v</ta>
            <ta e="T147" id="Seg_3093" s="T146">pers</ta>
            <ta e="T148" id="Seg_3094" s="T147">v</ta>
            <ta e="T149" id="Seg_3095" s="T148">n</ta>
            <ta e="T150" id="Seg_3096" s="T149">adv</ta>
            <ta e="T151" id="Seg_3097" s="T150">v</ta>
            <ta e="T152" id="Seg_3098" s="T151">n</ta>
            <ta e="T153" id="Seg_3099" s="T152">v</ta>
            <ta e="T154" id="Seg_3100" s="T153">num</ta>
            <ta e="T155" id="Seg_3101" s="T154">n</ta>
            <ta e="T156" id="Seg_3102" s="T155">n</ta>
            <ta e="T157" id="Seg_3103" s="T156">v</ta>
            <ta e="T158" id="Seg_3104" s="T157">n</ta>
            <ta e="T159" id="Seg_3105" s="T158">v</ta>
            <ta e="T160" id="Seg_3106" s="T159">v</ta>
            <ta e="T161" id="Seg_3107" s="T160">adj</ta>
            <ta e="T162" id="Seg_3108" s="T161">n</ta>
            <ta e="T163" id="Seg_3109" s="T162">pers</ta>
            <ta e="T164" id="Seg_3110" s="T163">preverb</ta>
            <ta e="T165" id="Seg_3111" s="T164">v</ta>
            <ta e="T166" id="Seg_3112" s="T165">conj</ta>
            <ta e="T167" id="Seg_3113" s="T166">nprop</ta>
            <ta e="T168" id="Seg_3114" s="T167">v</ta>
            <ta e="T169" id="Seg_3115" s="T168">pers</ta>
            <ta e="T170" id="Seg_3116" s="T169">pers</ta>
            <ta e="T171" id="Seg_3117" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_3118" s="T171">v</ta>
            <ta e="T173" id="Seg_3119" s="T172">adj</ta>
            <ta e="T174" id="Seg_3120" s="T173">v</ta>
            <ta e="T175" id="Seg_3121" s="T174">n</ta>
            <ta e="T176" id="Seg_3122" s="T175">n</ta>
            <ta e="T177" id="Seg_3123" s="T176">v</ta>
            <ta e="T178" id="Seg_3124" s="T177">v</ta>
            <ta e="T179" id="Seg_3125" s="T178">v</ta>
            <ta e="T180" id="Seg_3126" s="T179">nprop</ta>
            <ta e="T181" id="Seg_3127" s="T180">v</ta>
            <ta e="T182" id="Seg_3128" s="T181">n</ta>
            <ta e="T183" id="Seg_3129" s="T182">adv</ta>
            <ta e="T184" id="Seg_3130" s="T183">v</ta>
            <ta e="T185" id="Seg_3131" s="T184">n</ta>
            <ta e="T186" id="Seg_3132" s="T185">v</ta>
            <ta e="T187" id="Seg_3133" s="T186">pers</ta>
            <ta e="T188" id="Seg_3134" s="T187">adj</ta>
            <ta e="T189" id="Seg_3135" s="T188">n</ta>
            <ta e="T190" id="Seg_3136" s="T189">n</ta>
            <ta e="T191" id="Seg_3137" s="T190">preverb</ta>
            <ta e="T192" id="Seg_3138" s="T191">v</ta>
            <ta e="T193" id="Seg_3139" s="T192">conj</ta>
            <ta e="T194" id="Seg_3140" s="T193">n</ta>
            <ta e="T195" id="Seg_3141" s="T194">adv</ta>
            <ta e="T196" id="Seg_3142" s="T195">v</ta>
            <ta e="T197" id="Seg_3143" s="T196">nprop</ta>
            <ta e="T198" id="Seg_3144" s="T197">preverb</ta>
            <ta e="T199" id="Seg_3145" s="T198">n</ta>
            <ta e="T200" id="Seg_3146" s="T199">v</ta>
            <ta e="T201" id="Seg_3147" s="T200">n</ta>
            <ta e="T202" id="Seg_3148" s="T201">quant</ta>
            <ta e="T203" id="Seg_3149" s="T202">v</ta>
            <ta e="T204" id="Seg_3150" s="T203">v</ta>
            <ta e="T205" id="Seg_3151" s="T204">n</ta>
            <ta e="T206" id="Seg_3152" s="T205">n</ta>
            <ta e="T207" id="Seg_3153" s="T206">v</ta>
            <ta e="T208" id="Seg_3154" s="T207">conj</ta>
            <ta e="T209" id="Seg_3155" s="T208">n</ta>
            <ta e="T210" id="Seg_3156" s="T209">v</ta>
            <ta e="T211" id="Seg_3157" s="T210">v</ta>
            <ta e="T212" id="Seg_3158" s="T211">conj</ta>
            <ta e="T213" id="Seg_3159" s="T212">adv</ta>
            <ta e="T214" id="Seg_3160" s="T213">v</ta>
            <ta e="T215" id="Seg_3161" s="T214">adv</ta>
            <ta e="T216" id="Seg_3162" s="T215">v</ta>
            <ta e="T217" id="Seg_3163" s="T216">n</ta>
            <ta e="T218" id="Seg_3164" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_3165" s="T218">v</ta>
            <ta e="T220" id="Seg_3166" s="T219">n</ta>
            <ta e="T221" id="Seg_3167" s="T220">n</ta>
            <ta e="T222" id="Seg_3168" s="T221">adv</ta>
            <ta e="T223" id="Seg_3169" s="T222">v</ta>
            <ta e="T224" id="Seg_3170" s="T223">n</ta>
            <ta e="T225" id="Seg_3171" s="T224">n</ta>
            <ta e="T226" id="Seg_3172" s="T225">preverb</ta>
            <ta e="T227" id="Seg_3173" s="T226">v</ta>
            <ta e="T228" id="Seg_3174" s="T227">n</ta>
            <ta e="T229" id="Seg_3175" s="T228">conj</ta>
            <ta e="T230" id="Seg_3176" s="T229">n</ta>
            <ta e="T231" id="Seg_3177" s="T230">num</ta>
            <ta e="T232" id="Seg_3178" s="T231">n</ta>
            <ta e="T233" id="Seg_3179" s="T232">n</ta>
            <ta e="T234" id="Seg_3180" s="T233">v</ta>
            <ta e="T235" id="Seg_3181" s="T234">adv</ta>
            <ta e="T236" id="Seg_3182" s="T235">pers</ta>
            <ta e="T237" id="Seg_3183" s="T236">nprop</ta>
            <ta e="T238" id="Seg_3184" s="T237">pers</ta>
            <ta e="T239" id="Seg_3185" s="T238">adv</ta>
            <ta e="T240" id="Seg_3186" s="T239">preverb</ta>
            <ta e="T241" id="Seg_3187" s="T240">v</ta>
            <ta e="T242" id="Seg_3188" s="T241">pers</ta>
            <ta e="T243" id="Seg_3189" s="T242">v</ta>
            <ta e="T244" id="Seg_3190" s="T243">adv</ta>
            <ta e="T245" id="Seg_3191" s="T244">pers</ta>
            <ta e="T246" id="Seg_3192" s="T245">n</ta>
            <ta e="T247" id="Seg_3193" s="T246">conj</ta>
            <ta e="T248" id="Seg_3194" s="T247">n</ta>
            <ta e="T249" id="Seg_3195" s="T248">v</ta>
            <ta e="T250" id="Seg_3196" s="T249">conj</ta>
            <ta e="T251" id="Seg_3197" s="T250">n</ta>
            <ta e="T252" id="Seg_3198" s="T251">v</ta>
            <ta e="T253" id="Seg_3199" s="T252">conj</ta>
            <ta e="T254" id="Seg_3200" s="T253">adv</ta>
            <ta e="T255" id="Seg_3201" s="T254">v</ta>
            <ta e="T256" id="Seg_3202" s="T255">conj</ta>
            <ta e="T257" id="Seg_3203" s="T256">v</ta>
            <ta e="T258" id="Seg_3204" s="T257">n</ta>
            <ta e="T259" id="Seg_3205" s="T258">n</ta>
            <ta e="T260" id="Seg_3206" s="T259">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T31" id="Seg_3207" s="T30">RUS:gram</ta>
            <ta e="T33" id="Seg_3208" s="T32">RUS:core</ta>
            <ta e="T47" id="Seg_3209" s="T46">RUS:gram</ta>
            <ta e="T56" id="Seg_3210" s="T55">RUS:gram</ta>
            <ta e="T62" id="Seg_3211" s="T61">RUS:gram</ta>
            <ta e="T92" id="Seg_3212" s="T91">RUS:gram</ta>
            <ta e="T93" id="Seg_3213" s="T92">RUS:gram</ta>
            <ta e="T119" id="Seg_3214" s="T118">RUS:gram</ta>
            <ta e="T120" id="Seg_3215" s="T119">RUS:gram</ta>
            <ta e="T129" id="Seg_3216" s="T128">RUS:core</ta>
            <ta e="T141" id="Seg_3217" s="T140">RUS:gram</ta>
            <ta e="T144" id="Seg_3218" s="T143">RUS:gram</ta>
            <ta e="T166" id="Seg_3219" s="T165">RUS:gram</ta>
            <ta e="T193" id="Seg_3220" s="T192">RUS:gram</ta>
            <ta e="T202" id="Seg_3221" s="T201">RUS:core</ta>
            <ta e="T208" id="Seg_3222" s="T207">RUS:gram</ta>
            <ta e="T212" id="Seg_3223" s="T211">RUS:gram</ta>
            <ta e="T229" id="Seg_3224" s="T228">RUS:gram</ta>
            <ta e="T247" id="Seg_3225" s="T246">RUS:gram</ta>
            <ta e="T250" id="Seg_3226" s="T249">RUS:gram</ta>
            <ta e="T253" id="Seg_3227" s="T252">RUS:gram</ta>
            <ta e="T254" id="Seg_3228" s="T253">RUS:core</ta>
            <ta e="T256" id="Seg_3229" s="T255">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T129" id="Seg_3230" s="T128">inCdel Csub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_3231" s="T0">Жили-были два брата. </ta>
            <ta e="T7" id="Seg_3232" s="T3">Старший брат женатый был [=живёт]. </ta>
            <ta e="T10" id="Seg_3233" s="T7">Младшего брата звали Камача.</ta>
            <ta e="T14" id="Seg_3234" s="T10">Сноха его всё время обижала. </ta>
            <ta e="T16" id="Seg_3235" s="T14">Братья ходили охотиться на глухарей. </ta>
            <ta e="T18" id="Seg_3236" s="T16">Глухарей добывали.</ta>
            <ta e="T25" id="Seg_3237" s="T18">Старший брат пойдёт один по дороге со слопцами.</ta>
            <ta e="T32" id="Seg_3238" s="T25">[Пока его не было,] сноха кормила Камачу только рёбрами и головами от глухарей. </ta>
            <ta e="T39" id="Seg_3239" s="T32">Иногда, когда пойдут они вдвоем с Камачей.</ta>
            <ta e="T41" id="Seg_3240" s="T39">Глухарь летит.</ta>
            <ta e="T45" id="Seg_3241" s="T41">Kамача кричит: «Глухарь!</ta>
            <ta e="T50" id="Seg_3242" s="T45">Голову и ребра мне оставь!»</ta>
            <ta e="T55" id="Seg_3243" s="T50">Брат спросит: «Почему ты так кричишь?»</ta>
            <ta e="T64" id="Seg_3244" s="T55">Камача скажет: «Меня сноха ребрами и головой кормит.» </ta>
            <ta e="T67" id="Seg_3245" s="T64">Вечером домой придут.</ta>
            <ta e="T71" id="Seg_3246" s="T67">Брат жену схватит, поколотит.</ta>
            <ta e="T79" id="Seg_3247" s="T71">Назавтра брат опять один пойдёт по дороге со слопцами. </ta>
            <ta e="T83" id="Seg_3248" s="T79">Сноха поймает Камачу и побьёт.</ta>
            <ta e="T90" id="Seg_3249" s="T83">Камача на улицу выйдет и уйдёт, куда глаза глядят. </ta>
            <ta e="T95" id="Seg_3250" s="T90">Долго ли, коротко ли идёт.</ta>
            <ta e="T101" id="Seg_3251" s="T95">Видит, ворон летит, во рту рыбу несёт. </ta>
            <ta e="T108" id="Seg_3252" s="T101">Откуда ворон летел, туда Камача пойдёт.</ta>
            <ta e="T110" id="Seg_3253" s="T108">Дойдёт до речки. </ta>
            <ta e="T113" id="Seg_3254" s="T110">В речке рыбы много. </ta>
            <ta e="T116" id="Seg_3255" s="T113">Он срубит черёмуху.</ta>
            <ta e="T122" id="Seg_3256" s="T116">Сачок сделает, и давай рыбу черпать.</ta>
            <ta e="T124" id="Seg_3257" s="T122">Огонь разведёт.</ta>
            <ta e="T128" id="Seg_3258" s="T124">Окуней наставит на чапсах. </ta>
            <ta e="T132" id="Seg_3259" s="T128">Вдруг глядит, медведь идёт. </ta>
            <ta e="T135" id="Seg_3260" s="T132">«Камача, что ты делаешь?» </ta>
            <ta e="T143" id="Seg_3261" s="T135">«Я свою почку отрезал, на огне чапсу жарю, есть хочу».</ta>
            <ta e="T148" id="Seg_3262" s="T143">Медведь говорит: «Я тоже голодный».</ta>
            <ta e="T151" id="Seg_3263" s="T148">«По ту сторону от костра садись!»</ta>
            <ta e="T153" id="Seg_3264" s="T151">Рыба жарится. </ta>
            <ta e="T157" id="Seg_3265" s="T153">Одну чапсу медведю бросил. </ta>
            <ta e="T162" id="Seg_3266" s="T157">Медведь съел, говорит: «Какая хорошая почка!</ta>
            <ta e="T165" id="Seg_3267" s="T162">У меня тоже отрежь!» </ta>
            <ta e="T172" id="Seg_3268" s="T165">А Камача говорит: «Ты меня не съешь? </ta>
            <ta e="T175" id="Seg_3269" s="T172">Больно буду почку вырезать».</ta>
            <ta e="T179" id="Seg_3270" s="T175">Медведь говорит: «Вырезай, вытерплю!» </ta>
            <ta e="T184" id="Seg_3271" s="T179">Камача говорит медведю: «Ложись на спину!» </ta>
            <ta e="T186" id="Seg_3272" s="T184">Медведь лёг.</ta>
            <ta e="T192" id="Seg_3273" s="T186">Он острым ножом брюхо распорол.</ta>
            <ta e="T196" id="Seg_3274" s="T192">Медведь тут и умер.</ta>
            <ta e="T214" id="Seg_3275" s="T196">Камача шкуру ободрал, мясо всё убрал, полный мешок мяса наложил и рыбу положил, домой отправился. </ta>
            <ta e="T216" id="Seg_3276" s="T214">До дому доходит. </ta>
            <ta e="T219" id="Seg_3277" s="T216">Дым не дымится. </ta>
            <ta e="T223" id="Seg_3278" s="T219">На крышу вверх залезает. </ta>
            <ta e="T227" id="Seg_3279" s="T223">Из трубы вниз сморит.</ta>
            <ta e="T234" id="Seg_3280" s="T227">Брат и сноха одну искру делят. </ta>
            <ta e="T237" id="Seg_3281" s="T234">«Это мой Камача!»</ta>
            <ta e="T243" id="Seg_3282" s="T237">Он оттуда вниз как закричит: «Я тут! </ta>
            <ta e="T252" id="Seg_3283" s="T243">Для вас двоих мясо и рыбу принёс. Вас рыбой накрмлю». </ta>
            <ta e="T260" id="Seg_3284" s="T252">Потом они вместе жили и будут жить, дочь и сына растить.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_3285" s="T0">Once there were two brothers.</ta>
            <ta e="T7" id="Seg_3286" s="T3">The older brother was married.</ta>
            <ta e="T10" id="Seg_3287" s="T7">The younger brother was called Kamacha.</ta>
            <ta e="T14" id="Seg_3288" s="T10">The sister-in-law kept hurting him.</ta>
            <ta e="T16" id="Seg_3289" s="T14">The brothers went to go hunting wood grouses. </ta>
            <ta e="T18" id="Seg_3290" s="T16">They got wood grouses. </ta>
            <ta e="T25" id="Seg_3291" s="T18">The elder brother would go alone along the path with deadfall traps.</ta>
            <ta e="T32" id="Seg_3292" s="T25">[While he was away,] the sister-in-law only fed Kamacha with ribs and heads of grouses. </ta>
            <ta e="T39" id="Seg_3293" s="T32">Once, when the brothers (with Kamacha) go together.</ta>
            <ta e="T41" id="Seg_3294" s="T39">A grouse flies around.</ta>
            <ta e="T45" id="Seg_3295" s="T41">Kamacha yells: "Grouse! </ta>
            <ta e="T50" id="Seg_3296" s="T45">Leave me your head and ribs!"</ta>
            <ta e="T55" id="Seg_3297" s="T50">His brother would ask: "Why are you yelling so?"</ta>
            <ta e="T64" id="Seg_3298" s="T55">Kamacha would answer: "The sister-in-law feeds me with heads and ribs."</ta>
            <ta e="T67" id="Seg_3299" s="T64">They would come home in the evening.</ta>
            <ta e="T71" id="Seg_3300" s="T67">The brother would catch his wife, beat her. </ta>
            <ta e="T79" id="Seg_3301" s="T71">The next day, the brother would go alone again on the path with deadfall traps.</ta>
            <ta e="T83" id="Seg_3302" s="T79">The sister-in-law would catch Kamacha and beat him. </ta>
            <ta e="T90" id="Seg_3303" s="T83">Kamacha would run outstide and wander aimlessly around. </ta>
            <ta e="T95" id="Seg_3304" s="T90">He walks for a long or short time. </ta>
            <ta e="T101" id="Seg_3305" s="T95">For a while, he sees flying crows, it carries fish in his mouth. </ta>
            <ta e="T108" id="Seg_3306" s="T101">Where the crow flies from, there goes Kamacha.</ta>
            <ta e="T110" id="Seg_3307" s="T108">He comes to a river.</ta>
            <ta e="T113" id="Seg_3308" s="T110">There is a lot of fish in the river. </ta>
            <ta e="T116" id="Seg_3309" s="T113">He chops a bird cherry tree.</ta>
            <ta e="T122" id="Seg_3310" s="T116">He makes himself a scoop and goes catching fish.</ta>
            <ta e="T124" id="Seg_3311" s="T122">He sets fire.</ta>
            <ta e="T128" id="Seg_3312" s="T124">He puts the perch in (?).</ta>
            <ta e="T132" id="Seg_3313" s="T128">Suddenly he looks, a bear comes. </ta>
            <ta e="T135" id="Seg_3314" s="T132">"Kamacha, what are you doing?" </ta>
            <ta e="T143" id="Seg_3315" s="T135"> "I cut off my kidney, I boil it over the fire, I want to eat."</ta>
            <ta e="T148" id="Seg_3316" s="T143">The bear says: "I am hungry too."</ta>
            <ta e="T151" id="Seg_3317" s="T148">"Sit down on the other side of the fire!" </ta>
            <ta e="T153" id="Seg_3318" s="T151">The fish are frying. </ta>
            <ta e="T157" id="Seg_3319" s="T153">He throws one (?) to the bear.</ta>
            <ta e="T162" id="Seg_3320" s="T157">The bear is eating, he says: "That is good kidney!</ta>
            <ta e="T165" id="Seg_3321" s="T162">Cut one from me too!"</ta>
            <ta e="T172" id="Seg_3322" s="T165">And Kamacha says. "Are you not going to eat me? </ta>
            <ta e="T175" id="Seg_3323" s="T172">I will painfully cut off your flesh." </ta>
            <ta e="T179" id="Seg_3324" s="T175"> The bear says: "Cut it, I will endure!" </ta>
            <ta e="T184" id="Seg_3325" s="T179">Kamacha says to the bear: "Lie down on your back!" </ta>
            <ta e="T186" id="Seg_3326" s="T184">The bear lay down.</ta>
            <ta e="T192" id="Seg_3327" s="T186">He cuts his belly with a sharp knife.</ta>
            <ta e="T196" id="Seg_3328" s="T192">The bear dies.</ta>
            <ta e="T214" id="Seg_3329" s="T196">Kamacha rips his skin off, puts all the meat in the bag and puts the fish inside, he goes home.</ta>
            <ta e="T216" id="Seg_3330" s="T214">He comes home. </ta>
            <ta e="T219" id="Seg_3331" s="T216">The smoke does not steam. </ta>
            <ta e="T223" id="Seg_3332" s="T219">He climbs up on the roof.</ta>
            <ta e="T227" id="Seg_3333" s="T223">He looks down the tube.</ta>
            <ta e="T234" id="Seg_3334" s="T227">His brother and his sister-in-law make sparks.</ta>
            <ta e="T237" id="Seg_3335" s="T234">"This is my Kamacha!"</ta>
            <ta e="T243" id="Seg_3336" s="T237">Then he yells down: "I am here!</ta>
            <ta e="T252" id="Seg_3337" s="T243">I brought meat and fish for you. I will feed you with fish."</ta>
            <ta e="T260" id="Seg_3338" s="T252">Then they lived together and will live, a daughter and a son to raise.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_3339" s="T0">Es lebten zwei Brüder.</ta>
            <ta e="T7" id="Seg_3340" s="T3">Der ältere Bruder war verheiratet.</ta>
            <ta e="T10" id="Seg_3341" s="T7">Der jüngere Bruder hieß Kamacha.</ta>
            <ta e="T14" id="Seg_3342" s="T10">Seine Schwägerin pflegte ihn schlecht zu behandeln.</ta>
            <ta e="T16" id="Seg_3343" s="T14">Die Brüder gingen Auerhähne jagen.</ta>
            <ta e="T18" id="Seg_3344" s="T16">Sie töteten Auerhähne.</ta>
            <ta e="T25" id="Seg_3345" s="T18">Der ältere Bruder ging allein, ging auf dem Weg mit Totfallen.</ta>
            <ta e="T32" id="Seg_3346" s="T25">[Solange er weg war,] ließ die Schwägerin Kamacha nur die Rippen und Köpfe der Auerhähne essen.</ta>
            <ta e="T39" id="Seg_3347" s="T32">Einmal als der Bruder mit Kamacha zusammen geht.</ta>
            <ta e="T41" id="Seg_3348" s="T39">Ein Auerhahn fliegt [umher].</ta>
            <ta e="T45" id="Seg_3349" s="T41">Kamacha ruft: "Ein Auerhahn, ein Auerhahn!</ta>
            <ta e="T50" id="Seg_3350" s="T45">Lass mir Kopf und Rippen!"</ta>
            <ta e="T55" id="Seg_3351" s="T50">Sein Bruder fragt: "Warum schreist du so?"</ta>
            <ta e="T64" id="Seg_3352" s="T55">Kamacha antwortet: "Die Schwägerin gibt mir Köpfe und Rippen zu essen."</ta>
            <ta e="T67" id="Seg_3353" s="T64">Am Abend kommen sie nach Hause.</ta>
            <ta e="T71" id="Seg_3354" s="T67">Der Bruder greift seine Frau, schlägt sie.</ta>
            <ta e="T79" id="Seg_3355" s="T71">Am nächsten Tag geht der Bruder wieder alleine mit Totfallen auf dem Weg.</ta>
            <ta e="T83" id="Seg_3356" s="T79">Die Schwägerin fängt Kamacha und schlägt ihn.</ta>
            <ta e="T90" id="Seg_3357" s="T83">Kamacha rennt nach draußen auf die Straße und läuft ziellos umher.</ta>
            <ta e="T95" id="Seg_3358" s="T90">Er rennt eine lange oder kurze Zeit.</ta>
            <ta e="T101" id="Seg_3359" s="T95">Er sieht einen Raben fliegen, im Mund trägt er Fisch.</ta>
            <ta e="T108" id="Seg_3360" s="T101">Woher der Rabe geflogen kam, dorthin geht Kamacha.</ta>
            <ta e="T110" id="Seg_3361" s="T108">Er erreicht einen Fluss.</ta>
            <ta e="T113" id="Seg_3362" s="T110">Im Fluss sind viele Fische.</ta>
            <ta e="T116" id="Seg_3363" s="T113">Er fällt einen Traubenkirschenbaum.</ta>
            <ta e="T122" id="Seg_3364" s="T116">Er macht einen Kescher und lass ihn Fische fangen.</ta>
            <ta e="T124" id="Seg_3365" s="T122">Er entzündet ein Feuer. </ta>
            <ta e="T128" id="Seg_3366" s="T124">Den Barsch legt er am Morgen den Barsch in (?).</ta>
            <ta e="T132" id="Seg_3367" s="T128">Plötzlich sieht er ein wildes Tier kommen.</ta>
            <ta e="T135" id="Seg_3368" s="T132">"Kamacha, was machst du?"</ta>
            <ta e="T143" id="Seg_3369" s="T135">"Ich schneide meine Niere raus, koche sie über dem Feuer und will (sie) essen."</ta>
            <ta e="T148" id="Seg_3370" s="T143">Der Bär sagt: "Ich bin auch hungrig."</ta>
            <ta e="T151" id="Seg_3371" s="T148">"Setz dich auf die andere Seite des Feuers!"</ta>
            <ta e="T153" id="Seg_3372" s="T151">Die Fische braten.</ta>
            <ta e="T157" id="Seg_3373" s="T153">Er wirft eine/n (?) dem Bären hin.</ta>
            <ta e="T162" id="Seg_3374" s="T157">Der Bär frisst, er sagt: „Eine gute Niere!</ta>
            <ta e="T165" id="Seg_3375" s="T162">Schneide auch etwas von mir ab!“</ta>
            <ta e="T172" id="Seg_3376" s="T165">Und Kamacha sagt: „Du frisst mich nicht?</ta>
            <ta e="T175" id="Seg_3377" s="T172">Ich schneide dir schmerzhaft ins Fleisch."</ta>
            <ta e="T179" id="Seg_3378" s="T175">Der Bär sagt: "Schneide, ich werde es ertragen."</ta>
            <ta e="T184" id="Seg_3379" s="T179">Kamacha sagt zum Bären: "Leg dich auf den Rücken!"</ta>
            <ta e="T186" id="Seg_3380" s="T184">Der Bär legt sich hin.</ta>
            <ta e="T192" id="Seg_3381" s="T186">Er schneidet seinen Bauch mit einem scharfen Messer auf.</ta>
            <ta e="T196" id="Seg_3382" s="T192">Und da starb der Bär auch.</ta>
            <ta e="T214" id="Seg_3383" s="T196">Kamacha reißt seine Haut herunter, packt das ganze Fleisch in seine Tasche, legt den Fisch hinein, bricht nach Hause auf.</ta>
            <ta e="T216" id="Seg_3384" s="T214">Er kommt nach Hause.</ta>
            <ta e="T219" id="Seg_3385" s="T216">Es qualmt kein Rauch.</ta>
            <ta e="T223" id="Seg_3386" s="T219">Er klettert auf das Dach des Hauses.</ta>
            <ta e="T227" id="Seg_3387" s="T223">Er schaut in den Schornstein.</ta>
            <ta e="T234" id="Seg_3388" s="T227">Der Bruder und die Schwägerin teilen einen Funken.</ta>
            <ta e="T237" id="Seg_3389" s="T234">"Das ist mein Kamacha!"</ta>
            <ta e="T243" id="Seg_3390" s="T237">Er ruft hinunter: "Ich bin hier!</ta>
            <ta e="T252" id="Seg_3391" s="T243">Ich gebe euch Fleisch und Fisch. Ich gebe euch Fisch zu essen."</ta>
            <ta e="T260" id="Seg_3392" s="T252">Dann leben sie gemeinsam und leben um einen Sohn oder eine Tochter großzuziehen.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_3393" s="T0">Два брата жили.</ta>
            <ta e="T7" id="Seg_3394" s="T3">старший женатый живет</ta>
            <ta e="T10" id="Seg_3395" s="T7">маленького брата (звали) Камаджя</ta>
            <ta e="T14" id="Seg_3396" s="T10">сноха его обижая держала</ta>
            <ta e="T16" id="Seg_3397" s="T14">братья ходили охотиться на глухаря славсаям (?)</ta>
            <ta e="T18" id="Seg_3398" s="T16">глухарей добывали</ta>
            <ta e="T25" id="Seg_3399" s="T18">старший брат сам один пойдет по славсовой дороге (по славсам)</ta>
            <ta e="T32" id="Seg_3400" s="T25">сноха Камача кормила от глухарей ребрами головой</ta>
            <ta e="T39" id="Seg_3401" s="T32">пойдут двоем с Камачей </ta>
            <ta e="T41" id="Seg_3402" s="T39">глухарь летит</ta>
            <ta e="T45" id="Seg_3403" s="T41">Камача кричит глухар глухарь</ta>
            <ta e="T50" id="Seg_3404" s="T45">голову и ребра оставь</ta>
            <ta e="T55" id="Seg_3405" s="T50">брат спросит почему ты кричишь</ta>
            <ta e="T64" id="Seg_3406" s="T55">а камача скажет меня сноха ребрами головой кормит</ta>
            <ta e="T67" id="Seg_3407" s="T64">вечером домой придут</ta>
            <ta e="T71" id="Seg_3408" s="T67">брат жену поймает и наколотит</ta>
            <ta e="T79" id="Seg_3409" s="T71">завтра брат опять один пойдет по славсам</ta>
            <ta e="T83" id="Seg_3410" s="T79">сноха поймает Камажу и набьет</ta>
            <ta e="T90" id="Seg_3411" s="T83">К. на улицу выйдет и уйдет куда глаза глядят</ta>
            <ta e="T95" id="Seg_3412" s="T90">долго ли или коротко идет</ta>
            <ta e="T101" id="Seg_3413" s="T95">видит ворон летит во рту рыбу несет</ta>
            <ta e="T108" id="Seg_3414" s="T101">откуда ворон летел, Камача туда пойдет.</ta>
            <ta e="T110" id="Seg_3415" s="T108">дойдет до речки</ta>
            <ta e="T113" id="Seg_3416" s="T110">в речке рыбы много</ta>
            <ta e="T116" id="Seg_3417" s="T113">он срубит черемушку</ta>
            <ta e="T122" id="Seg_3418" s="T116">сочок сделает рыбу черпать</ta>
            <ta e="T124" id="Seg_3419" s="T122">огонь рас…</ta>
            <ta e="T128" id="Seg_3420" s="T124">окуней наставит на чапсах</ta>
            <ta e="T132" id="Seg_3421" s="T128">вдруг взглянет, медведь идёт</ta>
            <ta e="T135" id="Seg_3422" s="T132">К. что делаешь?</ta>
            <ta e="T143" id="Seg_3423" s="T135">яйцо свои выкопал на огонь чабсу делаю жарю есть хочу</ta>
            <ta e="T148" id="Seg_3424" s="T143">а медведь говорит я тоже есть хочу</ta>
            <ta e="T151" id="Seg_3425" s="T148">по ту сторону огня садишь</ta>
            <ta e="T153" id="Seg_3426" s="T151">рыба жарится</ta>
            <ta e="T157" id="Seg_3427" s="T153">одну чабсу медведю бросил.</ta>
            <ta e="T162" id="Seg_3428" s="T157">медведь съест скажет: какой хорошее яйцо.</ta>
            <ta e="T165" id="Seg_3429" s="T162">у меня тоже выкопай</ta>
            <ta e="T172" id="Seg_3430" s="T165">а К. скажет ты меня не съешь.</ta>
            <ta e="T175" id="Seg_3431" s="T172">больно буду копать яйцо</ta>
            <ta e="T179" id="Seg_3432" s="T175">медведь скажет выкопай вытерплю</ta>
            <ta e="T184" id="Seg_3433" s="T179">К скажет медведю навзничь ложись</ta>
            <ta e="T186" id="Seg_3434" s="T184">медведь ляжет</ta>
            <ta e="T192" id="Seg_3435" s="T186">он острым ножом брюхо распорит</ta>
            <ta e="T196" id="Seg_3436" s="T192">медведь тут умрёт</ta>
            <ta e="T214" id="Seg_3437" s="T196">К шкуру обдерет (снимает), мясо все уберёт в рюкзак мясо наложит и рыбу домой отправится.</ta>
            <ta e="T216" id="Seg_3438" s="T214">домой доходит</ta>
            <ta e="T219" id="Seg_3439" s="T216">дым не дымится.</ta>
            <ta e="T223" id="Seg_3440" s="T219">на крыу вверх залезет.</ta>
            <ta e="T227" id="Seg_3441" s="T223">с трубы вниз смотрит.</ta>
            <ta e="T234" id="Seg_3442" s="T227">брат и сноха одну искру делят.</ta>
            <ta e="T237" id="Seg_3443" s="T234">это мой Камаджʼя.</ta>
            <ta e="T243" id="Seg_3444" s="T237">он оттуда (вниз) закричит. я вот!</ta>
            <ta e="T252" id="Seg_3445" s="T243">для вас двоих мясо и рыбу принес рыбой накормлю.</ta>
            <ta e="T260" id="Seg_3446" s="T252">а потом жили а жить будут дочь и сына ростить.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T25" id="Seg_3447" s="T18">[AAV:] слопец, pl. слопцы -- a kind of deadfall trap for hunting birds or animals.</ta>
            <ta e="T128" id="Seg_3448" s="T124">WNB: multsubjective is wrong here</ta>
            <ta e="T252" id="Seg_3449" s="T243">last verb - why 3sg? </ta>
            <ta e="T260" id="Seg_3450" s="T252">WNB: the last verb is not clear.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
