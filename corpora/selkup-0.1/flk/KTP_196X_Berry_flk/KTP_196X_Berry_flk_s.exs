<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDD5CFF91F-AB20-4951-0A55-82AD09247D5B">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="KTP_196X_Berry_flk.wav" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">flk\KTP_196X_Berry_flk\KTP_196X_Berry_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">104</ud-information>
            <ud-information attribute-name="# HIAT:w">76</ud-information>
            <ud-information attribute-name="# e">75</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">13</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KTP">
            <abbreviation>KTP</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="13.253" type="appl" />
         <tli id="T1" time="14.118" type="appl" />
         <tli id="T2" time="14.984" type="appl" />
         <tli id="T3" time="15.849" type="appl" />
         <tli id="T4" time="16.715" type="appl" />
         <tli id="T5" time="17.58" type="appl" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" time="21.257" type="appl" />
         <tli id="T11" time="21.679" type="appl" />
         <tli id="T12" time="22.101" type="appl" />
         <tli id="T13" time="22.523" type="appl" />
         <tli id="T14" time="22.945" type="appl" />
         <tli id="T15" time="23.367" type="appl" />
         <tli id="T16" time="23.789" type="appl" />
         <tli id="T17" time="24.408" type="appl" />
         <tli id="T18" time="24.777" type="appl" />
         <tli id="T19" time="25.146" type="appl" />
         <tli id="T20" time="25.514" type="appl" />
         <tli id="T21" time="25.883" type="appl" />
         <tli id="T22" time="26.252" type="appl" />
         <tli id="T23" time="27.252" type="appl" />
         <tli id="T24" time="27.766" type="appl" />
         <tli id="T25" time="28.279" type="appl" />
         <tli id="T26" time="28.792" type="appl" />
         <tli id="T27" time="29.306" type="appl" />
         <tli id="T28" time="29.819" type="appl" />
         <tli id="T29" time="30.332" type="appl" />
         <tli id="T30" time="30.845" type="appl" />
         <tli id="T31" time="31.359" type="appl" />
         <tli id="T32" time="31.872" type="appl" />
         <tli id="T33" time="33.021" type="appl" />
         <tli id="T34" time="33.462" type="appl" />
         <tli id="T35" time="33.902" type="appl" />
         <tli id="T36" time="34.343" type="appl" />
         <tli id="T37" time="34.784" type="appl" />
         <tli id="T38" time="35.224" type="appl" />
         <tli id="T39" time="35.665" type="appl" />
         <tli id="T40" time="36.106" type="appl" />
         <tli id="T41" time="36.642" type="appl" />
         <tli id="T42" time="37.079" type="appl" />
         <tli id="T43" time="37.515" type="appl" />
         <tli id="T44" time="37.951" type="appl" />
         <tli id="T45" time="38.388" type="appl" />
         <tli id="T46" time="38.824" type="appl" />
         <tli id="T47" time="39.26" type="appl" />
         <tli id="T48" time="39.697" type="appl" />
         <tli id="T49" time="40.133" type="appl" />
         <tli id="T50" time="40.915" type="appl" />
         <tli id="T51" time="41.376" type="appl" />
         <tli id="T52" time="41.838" type="appl" />
         <tli id="T53" time="42.3" type="appl" />
         <tli id="T54" time="44.093" type="appl" />
         <tli id="T55" time="44.873" type="appl" />
         <tli id="T56" time="45.653" type="appl" />
         <tli id="T57" time="48.03" type="appl" />
         <tli id="T58" time="48.481" type="appl" />
         <tli id="T59" time="48.933" type="appl" />
         <tli id="T60" time="49.384" type="appl" />
         <tli id="T61" time="49.835" type="appl" />
         <tli id="T62" time="50.286" type="appl" />
         <tli id="T63" time="51.144" type="appl" />
         <tli id="T64" time="51.862" type="appl" />
         <tli id="T65" time="52.58" type="appl" />
         <tli id="T66" time="53.299" type="appl" />
         <tli id="T67" time="54.017" type="appl" />
         <tli id="T68" time="54.735" type="appl" />
         <tli id="T69" time="55.453" type="appl" />
         <tli id="T70" time="56.204" type="appl" />
         <tli id="T71" time="56.921" type="appl" />
         <tli id="T72" time="57.639" type="appl" />
         <tli id="T73" time="58.56" type="appl" />
         <tli id="T74" time="59.297" type="appl" />
         <tli id="T75" time="60.035" type="appl" />
         <tli id="T76" time="63.36" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KTP"
                      type="t">
         <timeline-fork end="T17" start="T16">
            <tli id="T16.tx.1" />
         </timeline-fork>
         <timeline-fork end="T19" start="T18">
            <tli id="T18.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T75" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Ilɨmpɔːqı</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">topɨrsajlaka</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">aj</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">nʼuːtɨlʼ</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">tamtɨr</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Qarɨ</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">jarkolʼ</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">nʼentɨ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">antɔːqı</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_35" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">To</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">nam</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">nı</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">kɨtɨɣɨt</ts>
                  <nts id="Seg_47" n="HIAT:ip">:</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">Tat</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">tü</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">čʼɔːtaš</ts>
                  <nts id="Seg_57" n="HIAT:ip">!</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_60" n="HIAT:u" s="T16">
                  <ts e="T16.tx.1" id="Seg_62" n="HIAT:w" s="T16">Tan</ts>
                  <nts id="Seg_63" n="HIAT:ip">,</nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16.tx.1">–</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">nı</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18.tx.1" id="Seg_71" n="HIAT:w" s="T18">kɨtɨɣit</ts>
                  <nts id="Seg_72" n="HIAT:ip">,</nts>
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18.tx.1">–</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">tat</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">tü</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">čʼɔːtaš</ts>
                  <nts id="Seg_84" n="HIAT:ip">!</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_87" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">Ukkɨr</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">čʼontaqɨn</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_95" n="HIAT:w" s="T24">qarɨn</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">topɨrsajlaka</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">nı</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_104" n="HIAT:w" s="T27">kətɨɣit</ts>
                  <nts id="Seg_105" n="HIAT:ip">:</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_108" n="HIAT:w" s="T28">Nʼuːtɨlʼ</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_111" n="HIAT:w" s="T29">tamtɨr</ts>
                  <nts id="Seg_112" n="HIAT:ip">,</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">tü</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">čʼɔːtašʼ</ts>
                  <nts id="Seg_119" n="HIAT:ip">!</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_122" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">Nʼuːtɨlʼ</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">tamtɨr</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">nı</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">kətɨɣit</ts>
                  <nts id="Seg_134" n="HIAT:ip">:</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_137" n="HIAT:w" s="T36">Topɨrsajlaka</ts>
                  <nts id="Seg_138" n="HIAT:ip">,</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_141" n="HIAT:w" s="T37">onantɨ</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_144" n="HIAT:w" s="T38">tü</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_147" n="HIAT:w" s="T39">čʼɔːtaš</ts>
                  <nts id="Seg_148" n="HIAT:ip">!</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_151" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_153" n="HIAT:w" s="T40">Šʼittalʼa</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_156" n="HIAT:w" s="T41">to</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_159" n="HIAT:w" s="T42">nʼuːtɨlʼ</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_162" n="HIAT:w" s="T43">tamtɨr</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">ompašʼim</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_167" n="HIAT:ip">(</nts>
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">on-</ts>
                  <nts id="Seg_170" n="HIAT:ip">)</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_173" n="HIAT:w" s="T46">ontɨ</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_176" n="HIAT:w" s="T47">na</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_179" n="HIAT:w" s="T48">wəšimpa</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_183" n="HIAT:u" s="T49">
                  <nts id="Seg_184" n="HIAT:ip">(</nts>
                  <nts id="Seg_185" n="HIAT:ip">(</nts>
                  <ats e="T50" id="Seg_186" n="HIAT:non-pho" s="T49">…</ats>
                  <nts id="Seg_187" n="HIAT:ip">)</nts>
                  <nts id="Seg_188" n="HIAT:ip">)</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_191" n="HIAT:w" s="T50">Ta</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_194" n="HIAT:w" s="T51">tü</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_197" n="HIAT:w" s="T52">čʼɔːtɨqolampat</ts>
                  <nts id="Seg_198" n="HIAT:ip">.</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_201" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_203" n="HIAT:w" s="T53">Sɔːlʼpasa</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_206" n="HIAT:w" s="T54">čʼap</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_209" n="HIAT:w" s="T55">qattɔːlʼpatɨ</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_213" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_215" n="HIAT:w" s="T56">Čʼɔːppaıːmpat</ts>
                  <nts id="Seg_216" n="HIAT:ip">,</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_219" n="HIAT:w" s="T57">šʼittalʼ</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_222" n="HIAT:w" s="T58">mukulʼtɨr</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_225" n="HIAT:w" s="T59">tü</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_228" n="HIAT:w" s="T60">ınna</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_231" n="HIAT:w" s="T61">ampat</ts>
                  <nts id="Seg_232" n="HIAT:ip">.</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_235" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_237" n="HIAT:w" s="T62">Topɨrsajlaka</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_240" n="HIAT:w" s="T63">ınna</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_243" n="HIAT:w" s="T64">wəšila</ts>
                  <nts id="Seg_244" n="HIAT:ip">,</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_247" n="HIAT:w" s="T65">nannɛr</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_250" n="HIAT:w" s="T66">pisɨšpa</ts>
                  <nts id="Seg_251" n="HIAT:ip">,</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_254" n="HIAT:w" s="T67">nannɛr</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_257" n="HIAT:w" s="T68">pisɨšpa</ts>
                  <nts id="Seg_258" n="HIAT:ip">.</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_261" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_263" n="HIAT:w" s="T69">Šʼittalʼ</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_266" n="HIAT:w" s="T70">to</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_269" n="HIAT:w" s="T71">passaıːmpa</ts>
                  <nts id="Seg_270" n="HIAT:ip">.</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_273" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_275" n="HIAT:w" s="T72">Čʼaptä</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_278" n="HIAT:w" s="T73">namɨqan</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_281" n="HIAT:w" s="T74">šünʼčʼaıːnt</ts>
                  <nts id="Seg_282" n="HIAT:ip">.</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T75" id="Seg_284" n="sc" s="T0">
               <ts e="T1" id="Seg_286" n="e" s="T0">Ilɨmpɔːqı </ts>
               <ts e="T2" id="Seg_288" n="e" s="T1">topɨrsajlaka </ts>
               <ts e="T3" id="Seg_290" n="e" s="T2">aj </ts>
               <ts e="T4" id="Seg_292" n="e" s="T3">nʼuːtɨlʼ </ts>
               <ts e="T5" id="Seg_294" n="e" s="T4">tamtɨr. </ts>
               <ts e="T6" id="Seg_296" n="e" s="T5">Qarɨ </ts>
               <ts e="T7" id="Seg_298" n="e" s="T6">jarkolʼ </ts>
               <ts e="T8" id="Seg_300" n="e" s="T7">nʼentɨ </ts>
               <ts e="T9" id="Seg_302" n="e" s="T8">antɔːqı. </ts>
               <ts e="T10" id="Seg_304" n="e" s="T9">To </ts>
               <ts e="T11" id="Seg_306" n="e" s="T10">nam </ts>
               <ts e="T12" id="Seg_308" n="e" s="T11">nı </ts>
               <ts e="T13" id="Seg_310" n="e" s="T12">kɨtɨɣɨt: </ts>
               <ts e="T14" id="Seg_312" n="e" s="T13">Tat </ts>
               <ts e="T15" id="Seg_314" n="e" s="T14">tü </ts>
               <ts e="T16" id="Seg_316" n="e" s="T15">čʼɔːtaš! </ts>
               <ts e="T17" id="Seg_318" n="e" s="T16">Tan,– </ts>
               <ts e="T18" id="Seg_320" n="e" s="T17">nı </ts>
               <ts e="T19" id="Seg_322" n="e" s="T18">kɨtɨɣit,– </ts>
               <ts e="T20" id="Seg_324" n="e" s="T19">tat </ts>
               <ts e="T21" id="Seg_326" n="e" s="T20">tü </ts>
               <ts e="T22" id="Seg_328" n="e" s="T21">čʼɔːtaš! </ts>
               <ts e="T23" id="Seg_330" n="e" s="T22">Ukkɨr </ts>
               <ts e="T24" id="Seg_332" n="e" s="T23">čʼontaqɨn </ts>
               <ts e="T25" id="Seg_334" n="e" s="T24">qarɨn </ts>
               <ts e="T26" id="Seg_336" n="e" s="T25">topɨrsajlaka </ts>
               <ts e="T27" id="Seg_338" n="e" s="T26">nı </ts>
               <ts e="T28" id="Seg_340" n="e" s="T27">kətɨɣit: </ts>
               <ts e="T29" id="Seg_342" n="e" s="T28">Nʼuːtɨlʼ </ts>
               <ts e="T30" id="Seg_344" n="e" s="T29">tamtɨr, </ts>
               <ts e="T31" id="Seg_346" n="e" s="T30">tü </ts>
               <ts e="T32" id="Seg_348" n="e" s="T31">čʼɔːtašʼ! </ts>
               <ts e="T33" id="Seg_350" n="e" s="T32">Nʼuːtɨlʼ </ts>
               <ts e="T34" id="Seg_352" n="e" s="T33">tamtɨr </ts>
               <ts e="T35" id="Seg_354" n="e" s="T34">nı </ts>
               <ts e="T36" id="Seg_356" n="e" s="T35">kətɨɣit: </ts>
               <ts e="T37" id="Seg_358" n="e" s="T36">Topɨrsajlaka, </ts>
               <ts e="T38" id="Seg_360" n="e" s="T37">onantɨ </ts>
               <ts e="T39" id="Seg_362" n="e" s="T38">tü </ts>
               <ts e="T40" id="Seg_364" n="e" s="T39">čʼɔːtaš! </ts>
               <ts e="T41" id="Seg_366" n="e" s="T40">Šʼittalʼa </ts>
               <ts e="T42" id="Seg_368" n="e" s="T41">to </ts>
               <ts e="T43" id="Seg_370" n="e" s="T42">nʼuːtɨlʼ </ts>
               <ts e="T44" id="Seg_372" n="e" s="T43">tamtɨr </ts>
               <ts e="T45" id="Seg_374" n="e" s="T44">ompašʼim </ts>
               <ts e="T46" id="Seg_376" n="e" s="T45">(on-) </ts>
               <ts e="T47" id="Seg_378" n="e" s="T46">ontɨ </ts>
               <ts e="T48" id="Seg_380" n="e" s="T47">na </ts>
               <ts e="T49" id="Seg_382" n="e" s="T48">wəšimpa. </ts>
               <ts e="T50" id="Seg_384" n="e" s="T49">((…)) </ts>
               <ts e="T51" id="Seg_386" n="e" s="T50">Ta </ts>
               <ts e="T52" id="Seg_388" n="e" s="T51">tü </ts>
               <ts e="T53" id="Seg_390" n="e" s="T52">čʼɔːtɨqolampat. </ts>
               <ts e="T54" id="Seg_392" n="e" s="T53">Sɔːlʼpasa </ts>
               <ts e="T55" id="Seg_394" n="e" s="T54">čʼap </ts>
               <ts e="T56" id="Seg_396" n="e" s="T55">qattɔːlʼpatɨ. </ts>
               <ts e="T57" id="Seg_398" n="e" s="T56">Čʼɔːppaıːmpat, </ts>
               <ts e="T58" id="Seg_400" n="e" s="T57">šʼittalʼ </ts>
               <ts e="T59" id="Seg_402" n="e" s="T58">mukulʼtɨr </ts>
               <ts e="T60" id="Seg_404" n="e" s="T59">tü </ts>
               <ts e="T61" id="Seg_406" n="e" s="T60">ınna </ts>
               <ts e="T62" id="Seg_408" n="e" s="T61">ampat. </ts>
               <ts e="T63" id="Seg_410" n="e" s="T62">Topɨrsajlaka </ts>
               <ts e="T64" id="Seg_412" n="e" s="T63">ınna </ts>
               <ts e="T65" id="Seg_414" n="e" s="T64">wəšila, </ts>
               <ts e="T66" id="Seg_416" n="e" s="T65">nannɛr </ts>
               <ts e="T67" id="Seg_418" n="e" s="T66">pisɨšpa, </ts>
               <ts e="T68" id="Seg_420" n="e" s="T67">nannɛr </ts>
               <ts e="T69" id="Seg_422" n="e" s="T68">pisɨšpa. </ts>
               <ts e="T70" id="Seg_424" n="e" s="T69">Šʼittalʼ </ts>
               <ts e="T71" id="Seg_426" n="e" s="T70">to </ts>
               <ts e="T72" id="Seg_428" n="e" s="T71">passaıːmpa. </ts>
               <ts e="T73" id="Seg_430" n="e" s="T72">Čʼaptä </ts>
               <ts e="T74" id="Seg_432" n="e" s="T73">namɨqan </ts>
               <ts e="T75" id="Seg_434" n="e" s="T74">šünʼčʼaıːnt. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_435" s="T0">KTP_196X_Berry_flk.001 (001.001)</ta>
            <ta e="T9" id="Seg_436" s="T5">KTP_196X_Berry_flk.002 (001.002)</ta>
            <ta e="T16" id="Seg_437" s="T9">KTP_196X_Berry_flk.003 (001.003)</ta>
            <ta e="T22" id="Seg_438" s="T16">KTP_196X_Berry_flk.004 (001.004)</ta>
            <ta e="T32" id="Seg_439" s="T22">KTP_196X_Berry_flk.005 (001.005)</ta>
            <ta e="T40" id="Seg_440" s="T32">KTP_196X_Berry_flk.006 (001.006)</ta>
            <ta e="T49" id="Seg_441" s="T40">KTP_196X_Berry_flk.007 (001.007)</ta>
            <ta e="T53" id="Seg_442" s="T49">KTP_196X_Berry_flk.008 (001.008)</ta>
            <ta e="T56" id="Seg_443" s="T53">KTP_196X_Berry_flk.009 (001.009)</ta>
            <ta e="T62" id="Seg_444" s="T56">KTP_196X_Berry_flk.010 (001.010)</ta>
            <ta e="T69" id="Seg_445" s="T62">KTP_196X_Berry_flk.011 (001.011)</ta>
            <ta e="T72" id="Seg_446" s="T69">KTP_196X_Berry_flk.012 (001.012)</ta>
            <ta e="T75" id="Seg_447" s="T72">KTP_196X_Berry_flk.013 (001.013)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_448" s="T0">Ilʼimpoqı topɨr sajlaka aj nʼutɨlʼ tamtɨr.</ta>
            <ta e="T9" id="Seg_449" s="T5">Qarɨ jarkol nʼäntɨ antɔːqı.</ta>
            <ta e="T16" id="Seg_450" s="T9">Tonam ni kɨtɨ(ɣɨ)tɨ: Ta tü čʼotaš!</ta>
            <ta e="T22" id="Seg_451" s="T16">Ta(n), - nʼi kɨtiɣit, - ta tü čʼotaš!</ta>
            <ta e="T32" id="Seg_452" s="T22">Ukkɨr čʼontaqɨn qarɨn topɨr sajlaka nʼi kətɨɣit: Nʼutɨlʼ tamtɨr, tü čʼotašʼ!</ta>
            <ta e="T40" id="Seg_453" s="T32">Nʼutɨlʼ tamtɨr nʼi kətɨɣit: Topɨr sajlaka, onantɨ tü čʼotaš!</ta>
            <ta e="T49" id="Seg_454" s="T40">Šʼittalʼa to nʼutɨlʼ tamtɨr ompašʼim ontɨ na wəšimpa.</ta>
            <ta e="T53" id="Seg_455" s="T49">Ta tü čʼotɨqolampat.</ta>
            <ta e="T56" id="Seg_456" s="T53">Solʼpasa čʼap qattolʼpatɨ.</ta>
            <ta e="T62" id="Seg_457" s="T56">Čʼappaıːmpat, šʼittalʼ mukulʼtɨr tü ınna ampat.</ta>
            <ta e="T69" id="Seg_458" s="T62">Topɨr sajlaka ınna wəšila, nannɛr pisıːšpa, nannɛr pisıːšpa.</ta>
            <ta e="T72" id="Seg_459" s="T69">Šʼittalʼ to passaıːmpa.</ta>
            <ta e="T75" id="Seg_460" s="T72">Čʼaptä namɨqan šʼunčʼaıːnt.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_461" s="T0">Ilɨmpɔːqı topɨrsajlaka aj nʼuːtɨlʼ tamtɨr. </ta>
            <ta e="T9" id="Seg_462" s="T5">Qarɨ jarkolʼ nʼentɨ antɔːqı. </ta>
            <ta e="T16" id="Seg_463" s="T9">To nam nı kɨtɨɣɨt: Tat tü čʼɔːtaš! </ta>
            <ta e="T22" id="Seg_464" s="T16">Tan,– nı kɨtɨɣit,– tat tü čʼɔːtaš! </ta>
            <ta e="T32" id="Seg_465" s="T22">Ukkɨr čʼontaqɨn qarɨn topɨrsajlaka nı kətɨɣit: Nʼuːtɨlʼ tamtɨr, tü čʼɔːtašʼ! </ta>
            <ta e="T40" id="Seg_466" s="T32">Nʼuːtɨlʼ tamtɨr nı kətɨɣit: Topɨrsajlaka, onantɨ tü čʼɔːtaš! </ta>
            <ta e="T49" id="Seg_467" s="T40">Šʼittalʼa to nʼuːtɨlʼ tamtɨr ompašʼim (on-) ontɨ na wəšimpa. </ta>
            <ta e="T53" id="Seg_468" s="T49">((…)) Ta tü čʼɔːtɨqolampat. </ta>
            <ta e="T56" id="Seg_469" s="T53">Sɔːlʼpasa čʼap qattɔːlʼpatɨ. </ta>
            <ta e="T62" id="Seg_470" s="T56">Čʼɔːppaıːmpat, šʼittalʼ mukulʼtɨr tü ınna ampat. </ta>
            <ta e="T69" id="Seg_471" s="T62">Topɨrsajlaka ınna wəšila, nannɛr pisɨšpa, nannɛr pisɨšpa. </ta>
            <ta e="T72" id="Seg_472" s="T69">Šʼittalʼ to passaıːmpa. </ta>
            <ta e="T75" id="Seg_473" s="T72">Čʼaptä namɨqan šünʼčʼaıːnt. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_474" s="T0">ilɨ-mpɔː-qı</ta>
            <ta e="T2" id="Seg_475" s="T1">topɨr-saj-laka</ta>
            <ta e="T3" id="Seg_476" s="T2">aj</ta>
            <ta e="T4" id="Seg_477" s="T3">nʼuːtɨ-lʼ</ta>
            <ta e="T5" id="Seg_478" s="T4">tamtɨr</ta>
            <ta e="T6" id="Seg_479" s="T5">qarɨ</ta>
            <ta e="T7" id="Seg_480" s="T6">jark-o-lʼ</ta>
            <ta e="T8" id="Seg_481" s="T7">nʼentɨ</ta>
            <ta e="T9" id="Seg_482" s="T8">antɔː-qı</ta>
            <ta e="T10" id="Seg_483" s="T9">to</ta>
            <ta e="T11" id="Seg_484" s="T10">na-m</ta>
            <ta e="T12" id="Seg_485" s="T11">nı</ta>
            <ta e="T13" id="Seg_486" s="T12">kɨtɨ-ɣɨ-t</ta>
            <ta e="T14" id="Seg_487" s="T13">tat</ta>
            <ta e="T15" id="Seg_488" s="T14">tü</ta>
            <ta e="T16" id="Seg_489" s="T15">čʼɔːt-aš</ta>
            <ta e="T17" id="Seg_490" s="T16">tat</ta>
            <ta e="T18" id="Seg_491" s="T17">nı</ta>
            <ta e="T19" id="Seg_492" s="T18">kɨtɨ-ɣi-t</ta>
            <ta e="T20" id="Seg_493" s="T19">tan</ta>
            <ta e="T21" id="Seg_494" s="T20">tü</ta>
            <ta e="T22" id="Seg_495" s="T21">čʼɔːt-aš</ta>
            <ta e="T23" id="Seg_496" s="T22">ukkɨr</ta>
            <ta e="T24" id="Seg_497" s="T23">čʼonta-qɨn</ta>
            <ta e="T25" id="Seg_498" s="T24">qarɨ-n</ta>
            <ta e="T26" id="Seg_499" s="T25">topɨr-saj-laka</ta>
            <ta e="T27" id="Seg_500" s="T26">nı</ta>
            <ta e="T28" id="Seg_501" s="T27">kətɨ-ɣi-t</ta>
            <ta e="T29" id="Seg_502" s="T28">nʼuːtɨ-lʼ</ta>
            <ta e="T30" id="Seg_503" s="T29">tamtɨr</ta>
            <ta e="T31" id="Seg_504" s="T30">tü</ta>
            <ta e="T32" id="Seg_505" s="T31">čʼɔːt-ašʼ</ta>
            <ta e="T33" id="Seg_506" s="T32">nʼuːtɨ-lʼ</ta>
            <ta e="T34" id="Seg_507" s="T33">tamtɨr</ta>
            <ta e="T35" id="Seg_508" s="T34">nı</ta>
            <ta e="T36" id="Seg_509" s="T35">kətɨ-ɣi-t</ta>
            <ta e="T37" id="Seg_510" s="T36">topɨr-saj-laka</ta>
            <ta e="T38" id="Seg_511" s="T37">onantɨ</ta>
            <ta e="T39" id="Seg_512" s="T38">tü</ta>
            <ta e="T40" id="Seg_513" s="T39">čʼɔːt-aš</ta>
            <ta e="T41" id="Seg_514" s="T40">šʼittalʼa</ta>
            <ta e="T42" id="Seg_515" s="T41">to</ta>
            <ta e="T43" id="Seg_516" s="T42">nʼuːtɨ-lʼ</ta>
            <ta e="T44" id="Seg_517" s="T43">tamtɨr</ta>
            <ta e="T45" id="Seg_518" s="T44">ompašʼim</ta>
            <ta e="T47" id="Seg_519" s="T46">ontɨ</ta>
            <ta e="T48" id="Seg_520" s="T47">na</ta>
            <ta e="T49" id="Seg_521" s="T48">wəši-mpa</ta>
            <ta e="T51" id="Seg_522" s="T50">ta</ta>
            <ta e="T52" id="Seg_523" s="T51">tü</ta>
            <ta e="T53" id="Seg_524" s="T52">čʼɔːtɨ-q-olam-pa-t</ta>
            <ta e="T54" id="Seg_525" s="T53">sɔːlʼpa-sa</ta>
            <ta e="T55" id="Seg_526" s="T54">čʼap</ta>
            <ta e="T56" id="Seg_527" s="T55">qatt-ɔːlʼ-pa-tɨ</ta>
            <ta e="T57" id="Seg_528" s="T56">čʼɔːppa-ıː-mpa-t</ta>
            <ta e="T58" id="Seg_529" s="T57">šʼittalʼ</ta>
            <ta e="T59" id="Seg_530" s="T58">mukulʼtɨr</ta>
            <ta e="T60" id="Seg_531" s="T59">tü</ta>
            <ta e="T61" id="Seg_532" s="T60">ınna</ta>
            <ta e="T62" id="Seg_533" s="T61">am-pa-t</ta>
            <ta e="T63" id="Seg_534" s="T62">topɨr-saj-laka</ta>
            <ta e="T64" id="Seg_535" s="T63">ınna</ta>
            <ta e="T65" id="Seg_536" s="T64">wəši-la</ta>
            <ta e="T66" id="Seg_537" s="T65">nannɛr</ta>
            <ta e="T67" id="Seg_538" s="T66">pisɨ-š-pa</ta>
            <ta e="T68" id="Seg_539" s="T67">nannɛr</ta>
            <ta e="T69" id="Seg_540" s="T68">pisɨ-š-pa</ta>
            <ta e="T70" id="Seg_541" s="T69">šʼittalʼ</ta>
            <ta e="T71" id="Seg_542" s="T70">to</ta>
            <ta e="T72" id="Seg_543" s="T71">pass-aıː-mpa</ta>
            <ta e="T73" id="Seg_544" s="T72">čʼaptä</ta>
            <ta e="T74" id="Seg_545" s="T73">namɨ-qan</ta>
            <ta e="T75" id="Seg_546" s="T74">šünʼčʼ-aıː-nt</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_547" s="T0">ilɨ-mpɨ-qı</ta>
            <ta e="T2" id="Seg_548" s="T1">topɨr-sajɨ-laka</ta>
            <ta e="T3" id="Seg_549" s="T2">aj</ta>
            <ta e="T4" id="Seg_550" s="T3">nʼuːtɨ-lʼ</ta>
            <ta e="T5" id="Seg_551" s="T4">tamtɨr</ta>
            <ta e="T6" id="Seg_552" s="T5">qarɨ</ta>
            <ta e="T7" id="Seg_553" s="T6">jarɨk-ɨ-lʼ</ta>
            <ta e="T8" id="Seg_554" s="T7">nʼentɨ</ta>
            <ta e="T9" id="Seg_555" s="T8">antɨ-qı</ta>
            <ta e="T10" id="Seg_556" s="T9">to</ta>
            <ta e="T11" id="Seg_557" s="T10">na-m</ta>
            <ta e="T12" id="Seg_558" s="T11">nık</ta>
            <ta e="T13" id="Seg_559" s="T12">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T14" id="Seg_560" s="T13">tan</ta>
            <ta e="T15" id="Seg_561" s="T14">tü</ta>
            <ta e="T16" id="Seg_562" s="T15">čʼɔːtɨ-äšɨk</ta>
            <ta e="T17" id="Seg_563" s="T16">tan</ta>
            <ta e="T18" id="Seg_564" s="T17">nık</ta>
            <ta e="T19" id="Seg_565" s="T18">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T20" id="Seg_566" s="T19">tan</ta>
            <ta e="T21" id="Seg_567" s="T20">tü</ta>
            <ta e="T22" id="Seg_568" s="T21">čʼɔːtɨ-äšɨk</ta>
            <ta e="T23" id="Seg_569" s="T22">ukkɨr</ta>
            <ta e="T24" id="Seg_570" s="T23">čʼontɨ-qɨn</ta>
            <ta e="T25" id="Seg_571" s="T24">qarɨ-n</ta>
            <ta e="T26" id="Seg_572" s="T25">topɨr-sajɨ-laka</ta>
            <ta e="T27" id="Seg_573" s="T26">nık</ta>
            <ta e="T28" id="Seg_574" s="T27">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T29" id="Seg_575" s="T28">nʼuːtɨ-lʼ</ta>
            <ta e="T30" id="Seg_576" s="T29">tamtɨr</ta>
            <ta e="T31" id="Seg_577" s="T30">tü</ta>
            <ta e="T32" id="Seg_578" s="T31">čʼɔːtɨ-äšɨk</ta>
            <ta e="T33" id="Seg_579" s="T32">nʼuːtɨ-lʼ</ta>
            <ta e="T34" id="Seg_580" s="T33">tamtɨr</ta>
            <ta e="T35" id="Seg_581" s="T34">nık</ta>
            <ta e="T36" id="Seg_582" s="T35">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T37" id="Seg_583" s="T36">topɨr-sajɨ-laka</ta>
            <ta e="T38" id="Seg_584" s="T37">onäntɨ</ta>
            <ta e="T39" id="Seg_585" s="T38">tü</ta>
            <ta e="T40" id="Seg_586" s="T39">čʼɔːtɨ-äšɨk</ta>
            <ta e="T41" id="Seg_587" s="T40">šittälʼ</ta>
            <ta e="T42" id="Seg_588" s="T41">to</ta>
            <ta e="T43" id="Seg_589" s="T42">nʼuːtɨ-lʼ</ta>
            <ta e="T44" id="Seg_590" s="T43">tamtɨr</ta>
            <ta e="T45" id="Seg_591" s="T44">ompä</ta>
            <ta e="T47" id="Seg_592" s="T46">ontɨ</ta>
            <ta e="T48" id="Seg_593" s="T47">na</ta>
            <ta e="T49" id="Seg_594" s="T48">wəšɨ-mpɨ</ta>
            <ta e="T51" id="Seg_595" s="T50">to</ta>
            <ta e="T52" id="Seg_596" s="T51">tü</ta>
            <ta e="T53" id="Seg_597" s="T52">čʼɔːtɨ-qo-olam-mpɨ-tɨ</ta>
            <ta e="T54" id="Seg_598" s="T53">sɔːlʼpo-sä</ta>
            <ta e="T55" id="Seg_599" s="T54">čʼam</ta>
            <ta e="T56" id="Seg_600" s="T55">qättɨ-ɔːl-mpɨ-tɨ</ta>
            <ta e="T57" id="Seg_601" s="T56">čʼɔːpɨ-ıː-mpɨ-tɨ</ta>
            <ta e="T58" id="Seg_602" s="T57">šittälʼ</ta>
            <ta e="T59" id="Seg_603" s="T58">*muqɨltıːrɨ</ta>
            <ta e="T60" id="Seg_604" s="T59">tü</ta>
            <ta e="T61" id="Seg_605" s="T60">ınnä</ta>
            <ta e="T62" id="Seg_606" s="T61">am-mpɨ-tɨ</ta>
            <ta e="T63" id="Seg_607" s="T62">topɨr-sajɨ-laka</ta>
            <ta e="T64" id="Seg_608" s="T63">ınnä</ta>
            <ta e="T65" id="Seg_609" s="T64">wəšɨ-lä</ta>
            <ta e="T66" id="Seg_610" s="T65">nannɛr</ta>
            <ta e="T67" id="Seg_611" s="T66">pisɨ-š-mpɨ</ta>
            <ta e="T68" id="Seg_612" s="T67">nannɛr</ta>
            <ta e="T69" id="Seg_613" s="T68">pisɨ-š-mpɨ</ta>
            <ta e="T70" id="Seg_614" s="T69">šittälʼ</ta>
            <ta e="T71" id="Seg_615" s="T70">to</ta>
            <ta e="T72" id="Seg_616" s="T71">pasɨ-ɛː-mpɨ</ta>
            <ta e="T73" id="Seg_617" s="T72">čʼaptä</ta>
            <ta e="T74" id="Seg_618" s="T73">na-qɨn</ta>
            <ta e="T75" id="Seg_619" s="T74">šünʼčʼɨ-ɛː-ntɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_620" s="T0">live-PST.NAR-3DU.S</ta>
            <ta e="T2" id="Seg_621" s="T1">berry-eye-SNGL.[NOM]</ta>
            <ta e="T3" id="Seg_622" s="T2">and</ta>
            <ta e="T4" id="Seg_623" s="T3">grass-ADJZ</ta>
            <ta e="T5" id="Seg_624" s="T4">bunch.[NOM]</ta>
            <ta e="T6" id="Seg_625" s="T5">morning.[NOM]</ta>
            <ta e="T7" id="Seg_626" s="T6">other-EP-ADJZ</ta>
            <ta e="T8" id="Seg_627" s="T7">together</ta>
            <ta e="T9" id="Seg_628" s="T8">dispute-3DU.S</ta>
            <ta e="T10" id="Seg_629" s="T9">that</ta>
            <ta e="T11" id="Seg_630" s="T10">this-ACC</ta>
            <ta e="T12" id="Seg_631" s="T11">so</ta>
            <ta e="T13" id="Seg_632" s="T12">say-CO-3SG.O</ta>
            <ta e="T14" id="Seg_633" s="T13">you.SG.NOM</ta>
            <ta e="T15" id="Seg_634" s="T14">fire.[NOM]</ta>
            <ta e="T16" id="Seg_635" s="T15">set.fire-IMP.2SG.S</ta>
            <ta e="T17" id="Seg_636" s="T16">you.SG.NOM</ta>
            <ta e="T18" id="Seg_637" s="T17">so</ta>
            <ta e="T19" id="Seg_638" s="T18">say-CO-3SG.O</ta>
            <ta e="T20" id="Seg_639" s="T19">you.SG.NOM</ta>
            <ta e="T21" id="Seg_640" s="T20">fire.[NOM]</ta>
            <ta e="T22" id="Seg_641" s="T21">set.fire-IMP.2SG.S</ta>
            <ta e="T23" id="Seg_642" s="T22">one</ta>
            <ta e="T24" id="Seg_643" s="T23">middle-LOC</ta>
            <ta e="T25" id="Seg_644" s="T24">morning-ADV.LOC</ta>
            <ta e="T26" id="Seg_645" s="T25">berry-eye-SNGL.[NOM]</ta>
            <ta e="T27" id="Seg_646" s="T26">so</ta>
            <ta e="T28" id="Seg_647" s="T27">say-CO-3SG.O</ta>
            <ta e="T29" id="Seg_648" s="T28">grass-ADJZ</ta>
            <ta e="T30" id="Seg_649" s="T29">bunch.[NOM]</ta>
            <ta e="T31" id="Seg_650" s="T30">fire.[NOM]</ta>
            <ta e="T32" id="Seg_651" s="T31">set.fire-IMP.2SG.S</ta>
            <ta e="T33" id="Seg_652" s="T32">grass-ADJZ</ta>
            <ta e="T34" id="Seg_653" s="T33">bunch.[NOM]</ta>
            <ta e="T35" id="Seg_654" s="T34">so</ta>
            <ta e="T36" id="Seg_655" s="T35">say-CO-3SG.O</ta>
            <ta e="T37" id="Seg_656" s="T36">berry-eye-SNGL.[NOM]</ta>
            <ta e="T38" id="Seg_657" s="T37">yourself.[NOM]</ta>
            <ta e="T39" id="Seg_658" s="T38">fire.[NOM]</ta>
            <ta e="T40" id="Seg_659" s="T39">set.fire-IMP.2SG.S</ta>
            <ta e="T41" id="Seg_660" s="T40">then</ta>
            <ta e="T42" id="Seg_661" s="T41">that</ta>
            <ta e="T43" id="Seg_662" s="T42">grass-ADJZ</ta>
            <ta e="T44" id="Seg_663" s="T43">bunch.[NOM]</ta>
            <ta e="T45" id="Seg_664" s="T44">soon</ta>
            <ta e="T47" id="Seg_665" s="T46">himself</ta>
            <ta e="T48" id="Seg_666" s="T47">here</ta>
            <ta e="T49" id="Seg_667" s="T48">get.up-PST.NAR.[3SG.S]</ta>
            <ta e="T51" id="Seg_668" s="T50">that</ta>
            <ta e="T52" id="Seg_669" s="T51">fire.[NOM]</ta>
            <ta e="T53" id="Seg_670" s="T52">set.fire-INF-begin-PST.NAR-3SG.O</ta>
            <ta e="T54" id="Seg_671" s="T53">match-INSTR</ta>
            <ta e="T55" id="Seg_672" s="T54">only</ta>
            <ta e="T56" id="Seg_673" s="T55">hit-MOM-PST.NAR-3SG.O</ta>
            <ta e="T57" id="Seg_674" s="T56">burn-RFL.PFV-PST.NAR-3SG.O</ta>
            <ta e="T58" id="Seg_675" s="T57">then</ta>
            <ta e="T59" id="Seg_676" s="T58">whole</ta>
            <ta e="T60" id="Seg_677" s="T59">fire.[NOM]</ta>
            <ta e="T61" id="Seg_678" s="T60">up</ta>
            <ta e="T62" id="Seg_679" s="T61">eat-PST.NAR-3SG.O</ta>
            <ta e="T63" id="Seg_680" s="T62">berry-eye-SNGL.[NOM]</ta>
            <ta e="T64" id="Seg_681" s="T63">up</ta>
            <ta e="T65" id="Seg_682" s="T64">get.up-CVB</ta>
            <ta e="T66" id="Seg_683" s="T65">so.much</ta>
            <ta e="T67" id="Seg_684" s="T66">laugh-US-PST.NAR.[3SG.S]</ta>
            <ta e="T68" id="Seg_685" s="T67">so.much</ta>
            <ta e="T69" id="Seg_686" s="T68">laugh-US-PST.NAR.[3SG.S]</ta>
            <ta e="T70" id="Seg_687" s="T69">then</ta>
            <ta e="T71" id="Seg_688" s="T70">that</ta>
            <ta e="T72" id="Seg_689" s="T71">burst-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T73" id="Seg_690" s="T72">tale.[NOM]</ta>
            <ta e="T74" id="Seg_691" s="T73">this-LOC</ta>
            <ta e="T75" id="Seg_692" s="T74">run.out-PFV-INFER.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_693" s="T0">жить-PST.NAR-3DU.S</ta>
            <ta e="T2" id="Seg_694" s="T1">ягода-глаз-SNGL.[NOM]</ta>
            <ta e="T3" id="Seg_695" s="T2">и</ta>
            <ta e="T4" id="Seg_696" s="T3">трава-ADJZ</ta>
            <ta e="T5" id="Seg_697" s="T4">связка.[NOM]</ta>
            <ta e="T6" id="Seg_698" s="T5">утро.[NOM]</ta>
            <ta e="T7" id="Seg_699" s="T6">другой-EP-ADJZ</ta>
            <ta e="T8" id="Seg_700" s="T7">вместе</ta>
            <ta e="T9" id="Seg_701" s="T8">спорить-3DU.S</ta>
            <ta e="T10" id="Seg_702" s="T9">тот</ta>
            <ta e="T11" id="Seg_703" s="T10">это-ACC</ta>
            <ta e="T12" id="Seg_704" s="T11">так</ta>
            <ta e="T13" id="Seg_705" s="T12">сказать-CO-3SG.O</ta>
            <ta e="T14" id="Seg_706" s="T13">ты.NOM</ta>
            <ta e="T15" id="Seg_707" s="T14">огонь.[NOM]</ta>
            <ta e="T16" id="Seg_708" s="T15">зажечь-IMP.2SG.S</ta>
            <ta e="T17" id="Seg_709" s="T16">ты.NOM</ta>
            <ta e="T18" id="Seg_710" s="T17">так</ta>
            <ta e="T19" id="Seg_711" s="T18">сказать-CO-3SG.O</ta>
            <ta e="T20" id="Seg_712" s="T19">ты.NOM</ta>
            <ta e="T21" id="Seg_713" s="T20">огонь.[NOM]</ta>
            <ta e="T22" id="Seg_714" s="T21">зажечь-IMP.2SG.S</ta>
            <ta e="T23" id="Seg_715" s="T22">один</ta>
            <ta e="T24" id="Seg_716" s="T23">середина-LOC</ta>
            <ta e="T25" id="Seg_717" s="T24">утро-ADV.LOC</ta>
            <ta e="T26" id="Seg_718" s="T25">ягода-глаз-SNGL.[NOM]</ta>
            <ta e="T27" id="Seg_719" s="T26">так</ta>
            <ta e="T28" id="Seg_720" s="T27">сказать-CO-3SG.O</ta>
            <ta e="T29" id="Seg_721" s="T28">трава-ADJZ</ta>
            <ta e="T30" id="Seg_722" s="T29">связка.[NOM]</ta>
            <ta e="T31" id="Seg_723" s="T30">огонь.[NOM]</ta>
            <ta e="T32" id="Seg_724" s="T31">зажечь-IMP.2SG.S</ta>
            <ta e="T33" id="Seg_725" s="T32">трава-ADJZ</ta>
            <ta e="T34" id="Seg_726" s="T33">связка.[NOM]</ta>
            <ta e="T35" id="Seg_727" s="T34">так</ta>
            <ta e="T36" id="Seg_728" s="T35">сказать-CO-3SG.O</ta>
            <ta e="T37" id="Seg_729" s="T36">ягода-глаз-SNGL.[NOM]</ta>
            <ta e="T38" id="Seg_730" s="T37">ты.сам.[NOM]</ta>
            <ta e="T39" id="Seg_731" s="T38">огонь.[NOM]</ta>
            <ta e="T40" id="Seg_732" s="T39">зажечь-IMP.2SG.S</ta>
            <ta e="T41" id="Seg_733" s="T40">потом</ta>
            <ta e="T42" id="Seg_734" s="T41">тот</ta>
            <ta e="T43" id="Seg_735" s="T42">трава-ADJZ</ta>
            <ta e="T44" id="Seg_736" s="T43">связка.[NOM]</ta>
            <ta e="T45" id="Seg_737" s="T44">скоро</ta>
            <ta e="T47" id="Seg_738" s="T46">он.сам</ta>
            <ta e="T48" id="Seg_739" s="T47">вот</ta>
            <ta e="T49" id="Seg_740" s="T48">встать-PST.NAR.[3SG.S]</ta>
            <ta e="T51" id="Seg_741" s="T50">тот</ta>
            <ta e="T52" id="Seg_742" s="T51">огонь.[NOM]</ta>
            <ta e="T53" id="Seg_743" s="T52">зажечь-INF-начать-PST.NAR-3SG.O</ta>
            <ta e="T54" id="Seg_744" s="T53">спичка-INSTR</ta>
            <ta e="T55" id="Seg_745" s="T54">только</ta>
            <ta e="T56" id="Seg_746" s="T55">ударить-MOM-PST.NAR-3SG.O</ta>
            <ta e="T57" id="Seg_747" s="T56">гореть-RFL.PFV-PST.NAR-3SG.O</ta>
            <ta e="T58" id="Seg_748" s="T57">потом</ta>
            <ta e="T59" id="Seg_749" s="T58">целый</ta>
            <ta e="T60" id="Seg_750" s="T59">огонь.[NOM]</ta>
            <ta e="T61" id="Seg_751" s="T60">вверх</ta>
            <ta e="T62" id="Seg_752" s="T61">съесть-PST.NAR-3SG.O</ta>
            <ta e="T63" id="Seg_753" s="T62">ягода-глаз-SNGL.[NOM]</ta>
            <ta e="T64" id="Seg_754" s="T63">вверх</ta>
            <ta e="T65" id="Seg_755" s="T64">встать-CVB</ta>
            <ta e="T66" id="Seg_756" s="T65">настолько</ta>
            <ta e="T67" id="Seg_757" s="T66">смеяться-US-PST.NAR.[3SG.S]</ta>
            <ta e="T68" id="Seg_758" s="T67">настолько</ta>
            <ta e="T69" id="Seg_759" s="T68">смеяться-US-PST.NAR.[3SG.S]</ta>
            <ta e="T70" id="Seg_760" s="T69">потом</ta>
            <ta e="T71" id="Seg_761" s="T70">тот</ta>
            <ta e="T72" id="Seg_762" s="T71">лопнуть-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T73" id="Seg_763" s="T72">сказка.[NOM]</ta>
            <ta e="T74" id="Seg_764" s="T73">этот-LOC</ta>
            <ta e="T75" id="Seg_765" s="T74">кончиться-PFV-INFER.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_766" s="T0">v-v:tense-v:pn</ta>
            <ta e="T2" id="Seg_767" s="T1">n-n-n&gt;n-n:case3</ta>
            <ta e="T3" id="Seg_768" s="T2">conj</ta>
            <ta e="T4" id="Seg_769" s="T3">n-n&gt;adj</ta>
            <ta e="T5" id="Seg_770" s="T4">n-n:case3</ta>
            <ta e="T6" id="Seg_771" s="T5">n-n:case3</ta>
            <ta e="T7" id="Seg_772" s="T6">adj-n:(ins)-adj&gt;adj</ta>
            <ta e="T8" id="Seg_773" s="T7">adv</ta>
            <ta e="T9" id="Seg_774" s="T8">v-v:pn</ta>
            <ta e="T10" id="Seg_775" s="T9">dem</ta>
            <ta e="T11" id="Seg_776" s="T10">pro-n:case3</ta>
            <ta e="T12" id="Seg_777" s="T11">adv</ta>
            <ta e="T13" id="Seg_778" s="T12">v-v:ins-v:pn</ta>
            <ta e="T14" id="Seg_779" s="T13">pers</ta>
            <ta e="T15" id="Seg_780" s="T14">n-n:case3</ta>
            <ta e="T16" id="Seg_781" s="T15">v-v:mood-pn</ta>
            <ta e="T17" id="Seg_782" s="T16">pers</ta>
            <ta e="T18" id="Seg_783" s="T17">adv</ta>
            <ta e="T19" id="Seg_784" s="T18">v-v:ins-v:pn</ta>
            <ta e="T20" id="Seg_785" s="T19">pers</ta>
            <ta e="T21" id="Seg_786" s="T20">n-n:case3</ta>
            <ta e="T22" id="Seg_787" s="T21">v-v:mood-pn</ta>
            <ta e="T23" id="Seg_788" s="T22">num</ta>
            <ta e="T24" id="Seg_789" s="T23">n-n:case3</ta>
            <ta e="T25" id="Seg_790" s="T24">n-n&gt;adv</ta>
            <ta e="T26" id="Seg_791" s="T25">n-n-n&gt;n-n:case3</ta>
            <ta e="T27" id="Seg_792" s="T26">adv</ta>
            <ta e="T28" id="Seg_793" s="T27">v-v:ins-v:pn</ta>
            <ta e="T29" id="Seg_794" s="T28">n-n&gt;adj</ta>
            <ta e="T30" id="Seg_795" s="T29">n-n:case3</ta>
            <ta e="T31" id="Seg_796" s="T30">n-n:case3</ta>
            <ta e="T32" id="Seg_797" s="T31">v-v:mood-pn</ta>
            <ta e="T33" id="Seg_798" s="T32">n-n&gt;adj</ta>
            <ta e="T34" id="Seg_799" s="T33">n-n:case3</ta>
            <ta e="T35" id="Seg_800" s="T34">adv</ta>
            <ta e="T36" id="Seg_801" s="T35">v-v:ins-v:pn</ta>
            <ta e="T37" id="Seg_802" s="T36">n-n-n&gt;n-n:case3</ta>
            <ta e="T38" id="Seg_803" s="T37">pro-n:case3</ta>
            <ta e="T39" id="Seg_804" s="T38">n-n:case3</ta>
            <ta e="T40" id="Seg_805" s="T39">v-v:mood-pn</ta>
            <ta e="T41" id="Seg_806" s="T40">adv</ta>
            <ta e="T42" id="Seg_807" s="T41">dem</ta>
            <ta e="T43" id="Seg_808" s="T42">n-n&gt;adj</ta>
            <ta e="T44" id="Seg_809" s="T43">n-n:case3</ta>
            <ta e="T45" id="Seg_810" s="T44">adv</ta>
            <ta e="T47" id="Seg_811" s="T46">pro</ta>
            <ta e="T48" id="Seg_812" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_813" s="T48">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_814" s="T50">dem</ta>
            <ta e="T52" id="Seg_815" s="T51">n-n:case3</ta>
            <ta e="T53" id="Seg_816" s="T52">v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_817" s="T53">n-n:case3</ta>
            <ta e="T55" id="Seg_818" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_819" s="T55">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_820" s="T56">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_821" s="T57">adv</ta>
            <ta e="T59" id="Seg_822" s="T58">adj</ta>
            <ta e="T60" id="Seg_823" s="T59">n-n:case3</ta>
            <ta e="T61" id="Seg_824" s="T60">preverb</ta>
            <ta e="T62" id="Seg_825" s="T61">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_826" s="T62">n-n-n&gt;n-n:case3</ta>
            <ta e="T64" id="Seg_827" s="T63">preverb</ta>
            <ta e="T65" id="Seg_828" s="T64">v-v&gt;adv</ta>
            <ta e="T66" id="Seg_829" s="T65">adv</ta>
            <ta e="T67" id="Seg_830" s="T66">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_831" s="T67">adv</ta>
            <ta e="T69" id="Seg_832" s="T68">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_833" s="T69">adv</ta>
            <ta e="T71" id="Seg_834" s="T70">dem</ta>
            <ta e="T72" id="Seg_835" s="T71">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T73" id="Seg_836" s="T72">n-n:case3</ta>
            <ta e="T74" id="Seg_837" s="T73">dem-n:case3</ta>
            <ta e="T75" id="Seg_838" s="T74">v-v&gt;v-v:tense-mood-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_839" s="T0">v</ta>
            <ta e="T2" id="Seg_840" s="T1">n</ta>
            <ta e="T3" id="Seg_841" s="T2">conj</ta>
            <ta e="T4" id="Seg_842" s="T3">adj</ta>
            <ta e="T5" id="Seg_843" s="T4">n</ta>
            <ta e="T6" id="Seg_844" s="T5">n</ta>
            <ta e="T7" id="Seg_845" s="T6">adj</ta>
            <ta e="T8" id="Seg_846" s="T7">adv</ta>
            <ta e="T9" id="Seg_847" s="T8">v</ta>
            <ta e="T10" id="Seg_848" s="T9">dem</ta>
            <ta e="T11" id="Seg_849" s="T10">pro</ta>
            <ta e="T12" id="Seg_850" s="T11">adv</ta>
            <ta e="T13" id="Seg_851" s="T12">v</ta>
            <ta e="T14" id="Seg_852" s="T13">pers</ta>
            <ta e="T15" id="Seg_853" s="T14">n</ta>
            <ta e="T16" id="Seg_854" s="T15">v</ta>
            <ta e="T17" id="Seg_855" s="T16">pers</ta>
            <ta e="T18" id="Seg_856" s="T17">adv</ta>
            <ta e="T19" id="Seg_857" s="T18">v</ta>
            <ta e="T20" id="Seg_858" s="T19">pers</ta>
            <ta e="T21" id="Seg_859" s="T20">n</ta>
            <ta e="T22" id="Seg_860" s="T21">v</ta>
            <ta e="T23" id="Seg_861" s="T22">num</ta>
            <ta e="T24" id="Seg_862" s="T23">n</ta>
            <ta e="T25" id="Seg_863" s="T24">adv</ta>
            <ta e="T26" id="Seg_864" s="T25">n</ta>
            <ta e="T27" id="Seg_865" s="T26">adv</ta>
            <ta e="T28" id="Seg_866" s="T27">v</ta>
            <ta e="T29" id="Seg_867" s="T28">adj</ta>
            <ta e="T30" id="Seg_868" s="T29">n</ta>
            <ta e="T31" id="Seg_869" s="T30">n</ta>
            <ta e="T32" id="Seg_870" s="T31">v</ta>
            <ta e="T33" id="Seg_871" s="T32">adj</ta>
            <ta e="T34" id="Seg_872" s="T33">n</ta>
            <ta e="T35" id="Seg_873" s="T34">adv</ta>
            <ta e="T36" id="Seg_874" s="T35">v</ta>
            <ta e="T37" id="Seg_875" s="T36">n</ta>
            <ta e="T38" id="Seg_876" s="T37">pro</ta>
            <ta e="T39" id="Seg_877" s="T38">n</ta>
            <ta e="T40" id="Seg_878" s="T39">v</ta>
            <ta e="T41" id="Seg_879" s="T40">adv</ta>
            <ta e="T42" id="Seg_880" s="T41">dem</ta>
            <ta e="T43" id="Seg_881" s="T42">adj</ta>
            <ta e="T44" id="Seg_882" s="T43">n</ta>
            <ta e="T45" id="Seg_883" s="T44">adv</ta>
            <ta e="T47" id="Seg_884" s="T46">emphpers</ta>
            <ta e="T48" id="Seg_885" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_886" s="T48">v</ta>
            <ta e="T51" id="Seg_887" s="T50">dem</ta>
            <ta e="T52" id="Seg_888" s="T51">n</ta>
            <ta e="T53" id="Seg_889" s="T52">v</ta>
            <ta e="T54" id="Seg_890" s="T53">n</ta>
            <ta e="T55" id="Seg_891" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_892" s="T55">v</ta>
            <ta e="T57" id="Seg_893" s="T56">v</ta>
            <ta e="T58" id="Seg_894" s="T57">adv</ta>
            <ta e="T59" id="Seg_895" s="T58">adj</ta>
            <ta e="T60" id="Seg_896" s="T59">n</ta>
            <ta e="T61" id="Seg_897" s="T60">preverb</ta>
            <ta e="T62" id="Seg_898" s="T61">v</ta>
            <ta e="T63" id="Seg_899" s="T62">n</ta>
            <ta e="T64" id="Seg_900" s="T63">preverb</ta>
            <ta e="T65" id="Seg_901" s="T64">adv</ta>
            <ta e="T66" id="Seg_902" s="T65">adv</ta>
            <ta e="T67" id="Seg_903" s="T66">v</ta>
            <ta e="T68" id="Seg_904" s="T67">adv</ta>
            <ta e="T69" id="Seg_905" s="T68">v</ta>
            <ta e="T70" id="Seg_906" s="T69">adv</ta>
            <ta e="T71" id="Seg_907" s="T70">dem</ta>
            <ta e="T72" id="Seg_908" s="T71">v</ta>
            <ta e="T73" id="Seg_909" s="T72">n</ta>
            <ta e="T74" id="Seg_910" s="T73">dem</ta>
            <ta e="T75" id="Seg_911" s="T74">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_912" s="T0">Жили ягодка и пучок сена.</ta>
            <ta e="T9" id="Seg_913" s="T5">Каждое утро друг с другом спорят.</ta>
            <ta e="T16" id="Seg_914" s="T9">Тот это так говорит: “Ты разожги огонь!”</ta>
            <ta e="T22" id="Seg_915" s="T16">“Ты, - [другой] так говорит, - ты огонь разожги!”</ta>
            <ta e="T32" id="Seg_916" s="T22">Oднажды утром ягодка так говорит: “Пучок сена, разожги огонь!”</ta>
            <ta e="T40" id="Seg_917" s="T32">Пучок сена так говорит: “Ягодка, ты сама разожги огонь!”</ta>
            <ta e="T49" id="Seg_918" s="T40">Потом тот пучок сена вскоре сам встал.</ta>
            <ta e="T53" id="Seg_919" s="T49">Он начал огонь разжигать.</ta>
            <ta e="T56" id="Seg_920" s="T53">Спичкой только чиркнул.</ta>
            <ta e="T62" id="Seg_921" s="T56">Сгорел, потом полностью [его] огонь съел.</ta>
            <ta e="T69" id="Seg_922" s="T62">Ягодка, встав, так смеялась, так смеялась.</ta>
            <ta e="T72" id="Seg_923" s="T69">Потом она лопнула.</ta>
            <ta e="T75" id="Seg_924" s="T72">Сказка на этом кончилась.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_925" s="T0">There lived a berry and a bunch of grass.</ta>
            <ta e="T9" id="Seg_926" s="T5">Every morning they are quarreling.</ta>
            <ta e="T16" id="Seg_927" s="T9">The one says: "You light up a fire!"</ta>
            <ta e="T22" id="Seg_928" s="T16">"You", [the other] says, "you light up a fire!"</ta>
            <ta e="T32" id="Seg_929" s="T22">Once in a morning the berry says: "Bunch of grass, light up a fire!"</ta>
            <ta e="T40" id="Seg_930" s="T32">The bunch of grass says: "Berry, light up a fire yourself!"</ta>
            <ta e="T49" id="Seg_931" s="T40">Then the bunch of grass stood up nevertheless.</ta>
            <ta e="T53" id="Seg_932" s="T49">It began to light up a fire.</ta>
            <ta e="T56" id="Seg_933" s="T53">It just lit up a match.</ta>
            <ta e="T62" id="Seg_934" s="T56">It burnt down, then the fire ate [it] completely.</ta>
            <ta e="T69" id="Seg_935" s="T62">The berry stood up and was laughing and laughing heavily.</ta>
            <ta e="T72" id="Seg_936" s="T69">Then she burst.</ta>
            <ta e="T75" id="Seg_937" s="T72">With this the tale ends.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_938" s="T0">Es lebten eine Beere und ein Grasbüschel.</ta>
            <ta e="T9" id="Seg_939" s="T5">Jeden Morgen streiten sie miteinander.</ta>
            <ta e="T16" id="Seg_940" s="T9">Der eine sagt: "Mach du Feuer!"</ta>
            <ta e="T22" id="Seg_941" s="T16">"Du", sagt [der andere], "mach du Feuer!"</ta>
            <ta e="T32" id="Seg_942" s="T22">Einmal an einem Morgen sagt die Beere: "Grasbüschel, mach Feuer!"</ta>
            <ta e="T40" id="Seg_943" s="T32">Das Grasbüschel sagt: "Beere, mach selber Feuer!"</ta>
            <ta e="T49" id="Seg_944" s="T40">Dann stand das Grasbüschel dennoch selbst auf.</ta>
            <ta e="T53" id="Seg_945" s="T49">Es fing an Feuer zu machen.</ta>
            <ta e="T56" id="Seg_946" s="T53">Es entzündete nur ein Streichholz.</ta>
            <ta e="T62" id="Seg_947" s="T56">Es brannte ab, dann fraß das Feuer [es] ganz.</ta>
            <ta e="T69" id="Seg_948" s="T62">Die Beere stand auf und lachte und lachte sehr.</ta>
            <ta e="T72" id="Seg_949" s="T69">Dann platzte sie.</ta>
            <ta e="T75" id="Seg_950" s="T72">Damit ist das Märchen zuende.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_951" s="T0">Жили ягодка и пучок сена.</ta>
            <ta e="T9" id="Seg_952" s="T5">Каждое утро спорят.</ta>
            <ta e="T16" id="Seg_953" s="T9">Тот говорит: Ты зажги огонь!</ta>
            <ta e="T22" id="Seg_954" s="T16">(Ты), - так говорит, - ты огонь зажги!</ta>
            <ta e="T32" id="Seg_955" s="T22">Oднажды утром ягодка говорит: Пучок сена, разожги огонь!</ta>
            <ta e="T40" id="Seg_956" s="T32">Пучок сена говорит: Ягодка, сама зажги огонь!</ta>
            <ta e="T49" id="Seg_957" s="T40">Потом пучок сена всё-таки сам встал.</ta>
            <ta e="T53" id="Seg_958" s="T49">Oн огонь зажёг.</ta>
            <ta e="T56" id="Seg_959" s="T53">Спичкой как чиркнул.</ta>
            <ta e="T62" id="Seg_960" s="T56">Cгорел, потом полностью [его] огонь сжёг.</ta>
            <ta e="T69" id="Seg_961" s="T62">Ягодка встала, так смеялась, так смеялась.</ta>
            <ta e="T72" id="Seg_962" s="T69">И лопнула.</ta>
            <ta e="T75" id="Seg_963" s="T72">Сказка на этом кончилась.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T56" id="Seg_964" s="T53">[OSV]: "qättɔ:lqo" - "to strike (a match)".</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
