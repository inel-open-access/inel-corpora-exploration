<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID33088A09-C22A-82D5-CD32-E186B2202C32">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">flk\SAI_1965_Palna_flk\SAI_1965_Palna_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">654</ud-information>
            <ud-information attribute-name="# HIAT:w">517</ud-information>
            <ud-information attribute-name="# e">518</ud-information>
            <ud-information attribute-name="# HIAT:u">110</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SAI">
            <abbreviation>SAI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
         <tli id="T318" />
         <tli id="T319" />
         <tli id="T320" />
         <tli id="T321" />
         <tli id="T322" />
         <tli id="T323" />
         <tli id="T324" />
         <tli id="T325" />
         <tli id="T326" />
         <tli id="T327" />
         <tli id="T328" />
         <tli id="T329" />
         <tli id="T330" />
         <tli id="T331" />
         <tli id="T332" />
         <tli id="T333" />
         <tli id="T334" />
         <tli id="T335" />
         <tli id="T336" />
         <tli id="T337" />
         <tli id="T338" />
         <tli id="T339" />
         <tli id="T340" />
         <tli id="T341" />
         <tli id="T342" />
         <tli id="T343" />
         <tli id="T344" />
         <tli id="T345" />
         <tli id="T346" />
         <tli id="T347" />
         <tli id="T348" />
         <tli id="T349" />
         <tli id="T350" />
         <tli id="T351" />
         <tli id="T352" />
         <tli id="T353" />
         <tli id="T354" />
         <tli id="T355" />
         <tli id="T356" />
         <tli id="T357" />
         <tli id="T358" />
         <tli id="T359" />
         <tli id="T360" />
         <tli id="T361" />
         <tli id="T362" />
         <tli id="T363" />
         <tli id="T364" />
         <tli id="T365" />
         <tli id="T366" />
         <tli id="T367" />
         <tli id="T368" />
         <tli id="T369" />
         <tli id="T370" />
         <tli id="T371" />
         <tli id="T372" />
         <tli id="T373" />
         <tli id="T374" />
         <tli id="T375" />
         <tli id="T376" />
         <tli id="T377" />
         <tli id="T378" />
         <tli id="T379" />
         <tli id="T380" />
         <tli id="T381" />
         <tli id="T382" />
         <tli id="T383" />
         <tli id="T384" />
         <tli id="T385" />
         <tli id="T386" />
         <tli id="T387" />
         <tli id="T388" />
         <tli id="T389" />
         <tli id="T390" />
         <tli id="T391" />
         <tli id="T392" />
         <tli id="T393" />
         <tli id="T394" />
         <tli id="T395" />
         <tli id="T396" />
         <tli id="T397" />
         <tli id="T398" />
         <tli id="T399" />
         <tli id="T400" />
         <tli id="T401" />
         <tli id="T402" />
         <tli id="T403" />
         <tli id="T404" />
         <tli id="T405" />
         <tli id="T406" />
         <tli id="T407" />
         <tli id="T408" />
         <tli id="T409" />
         <tli id="T410" />
         <tli id="T411" />
         <tli id="T412" />
         <tli id="T413" />
         <tli id="T414" />
         <tli id="T415" />
         <tli id="T416" />
         <tli id="T417" />
         <tli id="T418" />
         <tli id="T419" />
         <tli id="T420" />
         <tli id="T421" />
         <tli id="T422" />
         <tli id="T423" />
         <tli id="T424" />
         <tli id="T425" />
         <tli id="T426" />
         <tli id="T427" />
         <tli id="T428" />
         <tli id="T429" />
         <tli id="T430" />
         <tli id="T431" />
         <tli id="T432" />
         <tli id="T433" />
         <tli id="T434" />
         <tli id="T435" />
         <tli id="T436" />
         <tli id="T437" />
         <tli id="T438" />
         <tli id="T439" />
         <tli id="T440" />
         <tli id="T441" />
         <tli id="T442" />
         <tli id="T443" />
         <tli id="T444" />
         <tli id="T445" />
         <tli id="T446" />
         <tli id="T447" />
         <tli id="T448" />
         <tli id="T449" />
         <tli id="T450" />
         <tli id="T451" />
         <tli id="T452" />
         <tli id="T453" />
         <tli id="T454" />
         <tli id="T455" />
         <tli id="T456" />
         <tli id="T457" />
         <tli id="T458" />
         <tli id="T459" />
         <tli id="T460" />
         <tli id="T461" />
         <tli id="T462" />
         <tli id="T463" />
         <tli id="T464" />
         <tli id="T465" />
         <tli id="T466" />
         <tli id="T467" />
         <tli id="T468" />
         <tli id="T469" />
         <tli id="T470" />
         <tli id="T471" />
         <tli id="T472" />
         <tli id="T473" />
         <tli id="T474" />
         <tli id="T475" />
         <tli id="T476" />
         <tli id="T477" />
         <tli id="T478" />
         <tli id="T479" />
         <tli id="T480" />
         <tli id="T481" />
         <tli id="T482" />
         <tli id="T483" />
         <tli id="T484" />
         <tli id="T485" />
         <tli id="T486" />
         <tli id="T487" />
         <tli id="T488" />
         <tli id="T489" />
         <tli id="T490" />
         <tli id="T491" />
         <tli id="T492" />
         <tli id="T493" />
         <tli id="T494" />
         <tli id="T495" />
         <tli id="T496" />
         <tli id="T497" />
         <tli id="T498" />
         <tli id="T499" />
         <tli id="T500" />
         <tli id="T501" />
         <tli id="T502" />
         <tli id="T503" />
         <tli id="T504" />
         <tli id="T505" />
         <tli id="T506" />
         <tli id="T507" />
         <tli id="T508" />
         <tli id="T509" />
         <tli id="T510" />
         <tli id="T511" />
         <tli id="T512" />
         <tli id="T513" />
         <tli id="T514" />
         <tli id="T515" />
         <tli id="T516" />
         <tli id="T517" />
         <tli id="T518" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SAI"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T518" id="Seg_0" n="sc" s="T0">
               <ts e="T1" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Palna</ts>
                  <nts id="Seg_5" n="HIAT:ip">.</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_8" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_10" n="HIAT:w" s="T1">Palna</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">nɔːkɨr</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">timnʼasɨk</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">ilimmɨntɔːtɨt</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_23" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">Šölʼqumɨlʼ</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">mɔːtɨrɨlʼ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">irra</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">ɛppɨmmɨnta</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_38" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">Orsɨmɨlʼ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">irra</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">samɨj</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">märqɨ</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">timnʼätɨt</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_56" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">Čʼontoːqɨlʼ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">timnʼätɨt</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">taŋɨlʼ</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">ɔːtälʼ</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">nʼoːtɨj</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">ɛppɨmmɨnta</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_77" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">Poːs</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">kɨpa</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">timnʼätɨ</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">kərnʼat</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">pɔːralʼ</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">nʼoːtɨj</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">ɛppɨmmɨnta</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_101" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">Nılʼčʼik</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">ilimpɔːtɨt</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_110" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">A</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">na</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">Palnalʼ</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">märqɨ</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">timnʼätɨ</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_127" n="HIAT:w" s="T34">namɨšʼak</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_130" n="HIAT:w" s="T35">qəːtɨsä</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_133" n="HIAT:w" s="T36">ɛːppɨmmɨnta</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_137" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">Kuna</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T38">qaj</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">tünta</ts>
                  <nts id="Seg_146" n="HIAT:ip">,</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_149" n="HIAT:w" s="T40">muntɨk</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">tɛnɨnɨmpatɨ</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_156" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_158" n="HIAT:w" s="T42">Suːmpɨtɨlʼ</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_161" n="HIAT:w" s="T43">təːtɨpɨlʼ</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_164" n="HIAT:w" s="T44">irra</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_167" n="HIAT:w" s="T45">ɛːppɨmmɨnta</ts>
                  <nts id="Seg_168" n="HIAT:ip">.</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_171" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_173" n="HIAT:w" s="T46">Kəːsɨp</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_176" n="HIAT:w" s="T47">meːtɨmpatɨ</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_179" n="HIAT:w" s="T48">ontɨ</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_182" n="HIAT:w" s="T49">iliptäːqɨntɨ</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_186" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_188" n="HIAT:w" s="T50">Čʼɔːtɨrna</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_191" n="HIAT:w" s="T51">kəːsɨp</ts>
                  <nts id="Seg_192" n="HIAT:ip">,</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_195" n="HIAT:w" s="T52">nık</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_198" n="HIAT:w" s="T53">meːtɨmpatɨ</ts>
                  <nts id="Seg_199" n="HIAT:ip">,</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_202" n="HIAT:w" s="T54">čʼaŋak</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_205" n="HIAT:w" s="T55">pünon</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_209" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">A</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_214" n="HIAT:w" s="T57">rušit</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_217" n="HIAT:w" s="T58">meːtɨmpɔːtɨt</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_220" n="HIAT:w" s="T59">aša</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_223" n="HIAT:w" s="T60">tɛnɨmop</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_226" n="HIAT:w" s="T61">qajen</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_229" n="HIAT:w" s="T62">nɔːnɨ</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_231" n="HIAT:ip">…</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_234" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_236" n="HIAT:w" s="T64">Tɨp</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_239" n="HIAT:w" s="T65">meːtɨmpatɨ</ts>
                  <nts id="Seg_240" n="HIAT:ip">,</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_243" n="HIAT:w" s="T66">kəːsɨtqo</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_246" n="HIAT:w" s="T67">püp</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_249" n="HIAT:w" s="T68">meːtɨmpatɨ</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_253" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_255" n="HIAT:w" s="T69">Na</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_258" n="HIAT:w" s="T70">pün</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_261" n="HIAT:w" s="T71">nɔːnɨ</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_264" n="HIAT:w" s="T72">kəːsɨlʼ</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_267" n="HIAT:w" s="T73">porqɨp</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_270" n="HIAT:w" s="T74">meːtɨmpatɨ</ts>
                  <nts id="Seg_271" n="HIAT:ip">.</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_274" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_276" n="HIAT:w" s="T75">Säq</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_279" n="HIAT:w" s="T76">kəːsɨlʼ</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_282" n="HIAT:w" s="T77">ɨntɨ</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_285" n="HIAT:w" s="T78">meːtɨmpat</ts>
                  <nts id="Seg_286" n="HIAT:ip">,</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_289" n="HIAT:w" s="T79">säq</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_292" n="HIAT:w" s="T80">kəːsɨlʼ</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_295" n="HIAT:w" s="T81">komap</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_298" n="HIAT:w" s="T82">meːtɨmmɨntɨtɨ</ts>
                  <nts id="Seg_299" n="HIAT:ip">.</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_302" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_304" n="HIAT:w" s="T83">Nantɨsä</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_307" n="HIAT:w" s="T84">aj</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_310" n="HIAT:w" s="T85">ilɨmpa</ts>
                  <nts id="Seg_311" n="HIAT:ip">.</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_314" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_316" n="HIAT:w" s="T86">A</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_319" n="HIAT:w" s="T87">müttɨlä</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_322" n="HIAT:w" s="T88">aša</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_325" n="HIAT:w" s="T89">ičʼirpa</ts>
                  <nts id="Seg_326" n="HIAT:ip">.</nts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_329" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_331" n="HIAT:w" s="T90">Täpɨpkine</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_334" n="HIAT:w" s="T91">müttɨlä</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_337" n="HIAT:w" s="T92">ičʼirpatɨt</ts>
                  <nts id="Seg_338" n="HIAT:ip">.</nts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_341" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_343" n="HIAT:w" s="T93">Kepkisolʼa</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_346" n="HIAT:w" s="T94">kunɨ</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_349" n="HIAT:w" s="T95">čʼap</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_352" n="HIAT:w" s="T96">tüntɔːtɨt</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_355" n="HIAT:w" s="T97">təp</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_358" n="HIAT:w" s="T98">mun</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_361" n="HIAT:w" s="T99">tɛnimɨt</ts>
                  <nts id="Seg_362" n="HIAT:ip">.</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_365" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_367" n="HIAT:w" s="T100">Qälɨt</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_370" n="HIAT:w" s="T101">ičʼirpɔːtɨt</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_373" n="HIAT:w" s="T102">müttɨqo</ts>
                  <nts id="Seg_374" n="HIAT:ip">.</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_377" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_379" n="HIAT:w" s="T103">Laŋalʼ</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_382" n="HIAT:w" s="T104">qumɨt</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_385" n="HIAT:w" s="T105">pɛlʼaqɨt</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_388" n="HIAT:w" s="T106">aj</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_391" n="HIAT:w" s="T107">nɔːr</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_394" n="HIAT:w" s="T108">timnʼäsɨt</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_397" n="HIAT:w" s="T109">ɛppɨntɔːtɨt</ts>
                  <nts id="Seg_398" n="HIAT:ip">.</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_401" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_403" n="HIAT:w" s="T110">Aj</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_406" n="HIAT:w" s="T111">peːlä</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_409" n="HIAT:w" s="T112">ičʼirpɔːtɨt</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_412" n="HIAT:w" s="T113">tapčʼeːlɨ</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_415" n="HIAT:w" s="T114">qumɨlʼ</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_418" n="HIAT:w" s="T115">nɔːr</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_421" n="HIAT:w" s="T116">tɨmnʼastɨp</ts>
                  <nts id="Seg_422" n="HIAT:ip">.</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_425" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_427" n="HIAT:w" s="T117">Təm</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_430" n="HIAT:w" s="T118">ilɨmpa</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_433" n="HIAT:w" s="T119">okoːt</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_436" n="HIAT:w" s="T120">pulʼčʼos</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_439" n="HIAT:w" s="T121">qoltoːqɨt</ts>
                  <nts id="Seg_440" n="HIAT:ip">,</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_443" n="HIAT:w" s="T122">turuhanska</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_446" n="HIAT:w" s="T123">kərrelʼ</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_449" n="HIAT:w" s="T124">pɛlʼaqɨt</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_452" n="HIAT:w" s="T125">čʼuːroːqɨt</ts>
                  <nts id="Seg_453" n="HIAT:ip">.</nts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_456" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_458" n="HIAT:w" s="T126">Laŋalʼ</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_461" n="HIAT:w" s="T127">qumɨlʼ</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_464" n="HIAT:w" s="T128">nɔːkɨr</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_467" n="HIAT:w" s="T129">timnʼäsɨt</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_470" n="HIAT:w" s="T130">ukkɨr</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_473" n="HIAT:w" s="T131">tät</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_476" n="HIAT:w" s="T132">čʼontoːqɨt</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_479" n="HIAT:w" s="T133">nılʼčʼik</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_482" n="HIAT:w" s="T134">ɛːsɔːtɨt</ts>
                  <nts id="Seg_483" n="HIAT:ip">.</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_486" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_488" n="HIAT:w" s="T135">Palnalʼ</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_491" n="HIAT:w" s="T136">nɔːr</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_494" n="HIAT:w" s="T137">tɨmnʼasɨtɨp</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_497" n="HIAT:w" s="T138">peːlʼa</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_500" n="HIAT:w" s="T139">qənqolamnɔːtɨt</ts>
                  <nts id="Seg_501" n="HIAT:ip">.</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_504" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_506" n="HIAT:w" s="T140">A</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_509" n="HIAT:w" s="T141">Palnalʼ</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_512" n="HIAT:w" s="T142">timnʼäsɨt</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_515" n="HIAT:w" s="T143">qənɔːtɨt</ts>
                  <nts id="Seg_516" n="HIAT:ip">.</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_519" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_521" n="HIAT:w" s="T144">Tɨmnʼäiːqɨntɨ</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_524" n="HIAT:w" s="T145">nılʼčʼik</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_527" n="HIAT:w" s="T146">kətɨŋɨt</ts>
                  <nts id="Seg_528" n="HIAT:ip">.</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_531" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_533" n="HIAT:w" s="T147">Laŋalʼ</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_536" n="HIAT:w" s="T148">qumɨlʼ</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_539" n="HIAT:w" s="T149">nɔːr</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_542" n="HIAT:w" s="T150">tɨmnʼäsɨt</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_545" n="HIAT:w" s="T151">na</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_548" n="HIAT:w" s="T152">tüːqolamtɔːtɨt</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_551" n="HIAT:w" s="T153">müttelä</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_554" n="HIAT:w" s="T154">meːqɨnɨt</ts>
                  <nts id="Seg_555" n="HIAT:ip">.</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_558" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_560" n="HIAT:w" s="T155">Karra</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_563" n="HIAT:w" s="T156">čʼuːrontɨ</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_566" n="HIAT:w" s="T157">mɔːtɨŋlɨmɨt</ts>
                  <nts id="Seg_567" n="HIAT:ip">.</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_570" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_572" n="HIAT:w" s="T158">Čʼuːrontɨ</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_575" n="HIAT:w" s="T159">mɔːtɨŋnʼɛntɨmɨt</ts>
                  <nts id="Seg_576" n="HIAT:ip">.</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_579" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_581" n="HIAT:w" s="T160">Apsɨp</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_584" n="HIAT:w" s="T161">qənnɛntɨmɨt</ts>
                  <nts id="Seg_585" n="HIAT:ip">.</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_588" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_590" n="HIAT:w" s="T162">Qumɨt</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_593" n="HIAT:w" s="T163">kosti</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_595" n="HIAT:ip">(</nts>
                  <ts e="T165" id="Seg_597" n="HIAT:w" s="T164">mɔːttɨla</ts>
                  <nts id="Seg_598" n="HIAT:ip">)</nts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_601" n="HIAT:w" s="T165">tüntɔːtɨt</ts>
                  <nts id="Seg_602" n="HIAT:ip">.</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_605" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_607" n="HIAT:w" s="T166">Amɨrtɛntɔːtɨt</ts>
                  <nts id="Seg_608" n="HIAT:ip">.</nts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_611" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_613" n="HIAT:w" s="T167">Karra</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_616" n="HIAT:w" s="T168">qənnɔːtɨt</ts>
                  <nts id="Seg_617" n="HIAT:ip">,</nts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_620" n="HIAT:w" s="T169">čʼuːrontɨ</ts>
                  <nts id="Seg_621" n="HIAT:ip">.</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_624" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_626" n="HIAT:w" s="T170">Kärtɔːtɨt</ts>
                  <nts id="Seg_627" n="HIAT:ip">,</nts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_630" n="HIAT:w" s="T171">mɔːtɨŋnɔːtɨt</ts>
                  <nts id="Seg_631" n="HIAT:ip">.</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_634" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_636" n="HIAT:w" s="T172">Nʼuːtɨsä</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_639" n="HIAT:w" s="T173">tɔːqqɔːtɨt</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_642" n="HIAT:w" s="T174">mɔːttɨ</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_645" n="HIAT:w" s="T175">tolʼ</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_648" n="HIAT:w" s="T176">pɛlʼaqɨt</ts>
                  <nts id="Seg_649" n="HIAT:ip">.</nts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_652" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_654" n="HIAT:w" s="T177">Tösa</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_657" n="HIAT:w" s="T178">tɔːqqɔːtɨt</ts>
                  <nts id="Seg_658" n="HIAT:ip">.</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_661" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_663" n="HIAT:w" s="T179">ınnän</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_666" n="HIAT:w" s="T180">ɨllä</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_669" n="HIAT:w" s="T181">aj</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_672" n="HIAT:w" s="T182">nʼuːtɨsä</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_675" n="HIAT:w" s="T183">tɔːqqɔːtɨt</ts>
                  <nts id="Seg_676" n="HIAT:ip">.</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_679" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_681" n="HIAT:w" s="T184">Tɨmnʼäqɨntɨkine</ts>
                  <nts id="Seg_682" n="HIAT:ip">:</nts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_685" n="HIAT:w" s="T185">Kaša</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_687" n="HIAT:ip">(</nts>
                  <ts e="T187" id="Seg_689" n="HIAT:w" s="T186">avsa</ts>
                  <nts id="Seg_690" n="HIAT:ip">)</nts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_693" n="HIAT:w" s="T187">mušɨrɨŋɨlɨt</ts>
                  <nts id="Seg_694" n="HIAT:ip">.</nts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_697" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_699" n="HIAT:w" s="T188">Tapčʼeːlɨ</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_702" n="HIAT:w" s="T189">tüntɔːtɨt</ts>
                  <nts id="Seg_703" n="HIAT:ip">.</nts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_706" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_708" n="HIAT:w" s="T190">Ürɨlʼ</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_711" n="HIAT:w" s="T191">pirnʼäp</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_714" n="HIAT:w" s="T192">mɔːttɨ</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_717" n="HIAT:w" s="T193">tultɨŋɨlɨt</ts>
                  <nts id="Seg_718" n="HIAT:ip">.</nts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_721" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_723" n="HIAT:w" s="T194">Kašap</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_725" n="HIAT:ip">(</nts>
                  <ts e="T196" id="Seg_727" n="HIAT:w" s="T195">avsap</ts>
                  <nts id="Seg_728" n="HIAT:ip">)</nts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_731" n="HIAT:w" s="T196">kärrä</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_734" n="HIAT:w" s="T197">mušɨrɔːtɨt</ts>
                  <nts id="Seg_735" n="HIAT:ip">.</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_738" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_740" n="HIAT:w" s="T198">Nanä</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_743" n="HIAT:w" s="T199">na</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_746" n="HIAT:w" s="T200">ɔːmtɔːtɨt</ts>
                  <nts id="Seg_747" n="HIAT:ip">.</nts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_750" n="HIAT:u" s="T201">
                  <nts id="Seg_751" n="HIAT:ip">(</nts>
                  <ts e="T202" id="Seg_753" n="HIAT:w" s="T201">Čʼeːlɨtɨ</ts>
                  <nts id="Seg_754" n="HIAT:ip">)</nts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_757" n="HIAT:w" s="T202">čʼeːlʼ</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_760" n="HIAT:w" s="T203">čʼontɨlʼ</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_763" n="HIAT:w" s="T204">kotäntɨ</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_766" n="HIAT:w" s="T205">korrɨmɔːnnɨ</ts>
                  <nts id="Seg_767" n="HIAT:ip">.</nts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_770" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_772" n="HIAT:w" s="T206">Nʼennäntɨ</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_775" n="HIAT:w" s="T207">antıːrɨ</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_778" n="HIAT:w" s="T208">qontɨšɛlʼčʼa</ts>
                  <nts id="Seg_779" n="HIAT:ip">.</nts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_782" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_784" n="HIAT:w" s="T209">Nɔːkɨr</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_787" n="HIAT:w" s="T210">qumɨt</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_790" n="HIAT:w" s="T211">tıpɨlʼ</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_793" n="HIAT:w" s="T212">antɨ</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_796" n="HIAT:w" s="T213">qontɨššɛːnta</ts>
                  <nts id="Seg_797" n="HIAT:ip">.</nts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_800" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_802" n="HIAT:w" s="T214">Timnʼaiːqɨntɨ</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_805" n="HIAT:w" s="T215">nılʼčʼik</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_808" n="HIAT:w" s="T216">kətɨŋɨt</ts>
                  <nts id="Seg_809" n="HIAT:ip">.</nts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_812" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_814" n="HIAT:w" s="T217">Paŋɨlʼ</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_817" n="HIAT:w" s="T218">toqlʼet</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_820" n="HIAT:w" s="T219">ılla</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_823" n="HIAT:w" s="T220">ättɔːlnɨlɨt</ts>
                  <nts id="Seg_824" n="HIAT:ip">.</nts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_827" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_829" n="HIAT:w" s="T221">Peːmɨt</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_832" n="HIAT:w" s="T222">küːtet</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_835" n="HIAT:w" s="T223">šüː</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_838" n="HIAT:w" s="T224">säqɨtɔːlɛlʼčʼiŋɨlɨt</ts>
                  <nts id="Seg_839" n="HIAT:ip">.</nts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_842" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_844" n="HIAT:w" s="T225">Nɨːnɨ</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_847" n="HIAT:w" s="T226">nılʼčʼik</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_850" n="HIAT:w" s="T227">kətɨŋɨlit</ts>
                  <nts id="Seg_851" n="HIAT:ip">.</nts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_854" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_856" n="HIAT:w" s="T228">Paŋɨmɨt</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_859" n="HIAT:w" s="T229">čʼäːŋka</ts>
                  <nts id="Seg_860" n="HIAT:ip">,</nts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_863" n="HIAT:w" s="T230">paŋɨlɨt</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_866" n="HIAT:w" s="T231">meːqɨnɨt</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_869" n="HIAT:w" s="T232">miŋɨlɨt</ts>
                  <nts id="Seg_870" n="HIAT:ip">,</nts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_873" n="HIAT:w" s="T233">solap</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_876" n="HIAT:w" s="T234">meːqɨllʼčʼilɨmɨt</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_879" n="HIAT:w" s="T235">amɨrqonɨtqo</ts>
                  <nts id="Seg_880" n="HIAT:ip">.</nts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_883" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_885" n="HIAT:w" s="T236">Nık</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_888" n="HIAT:w" s="T237">ətɨrkɨŋɨtɨ</ts>
                  <nts id="Seg_889" n="HIAT:ip">.</nts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_892" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_894" n="HIAT:w" s="T238">Kɨpa</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_897" n="HIAT:w" s="T239">timnʼätɨ</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_900" n="HIAT:w" s="T240">sɨtkɨqɨt</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_903" n="HIAT:w" s="T241">ila</ts>
                  <nts id="Seg_904" n="HIAT:ip">.</nts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_907" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_909" n="HIAT:w" s="T242">Täːqamtɨ</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_912" n="HIAT:w" s="T243">nɨː</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_915" n="HIAT:w" s="T244">ɨnnä</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_918" n="HIAT:w" s="T245">čʼoqolnɨt</ts>
                  <nts id="Seg_919" n="HIAT:ip">.</nts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_922" n="HIAT:u" s="T246">
                  <ts e="T247" id="Seg_924" n="HIAT:w" s="T246">Ɨnnät</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_927" n="HIAT:w" s="T247">tüt</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_930" n="HIAT:w" s="T248">poːrɨlʼ</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_933" n="HIAT:w" s="T249">po</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_936" n="HIAT:w" s="T250">sɔːrɨmmɨntɔːtɨt</ts>
                  <nts id="Seg_937" n="HIAT:ip">.</nts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_940" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_942" n="HIAT:w" s="T251">Nık</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_945" n="HIAT:w" s="T252">qoŋɔːtɨt</ts>
                  <nts id="Seg_946" n="HIAT:ip">:</nts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_949" n="HIAT:w" s="T253">nʼennäntɨ</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_952" n="HIAT:w" s="T254">qontɨššɛıːŋa</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_955" n="HIAT:w" s="T255">nɔːkɨr</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_958" n="HIAT:w" s="T256">qumɨt</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_961" n="HIAT:w" s="T257">tıpɨlʼ</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_964" n="HIAT:w" s="T258">antɨ</ts>
                  <nts id="Seg_965" n="HIAT:ip">.</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T261" id="Seg_968" n="HIAT:u" s="T259">
                  <ts e="T260" id="Seg_970" n="HIAT:w" s="T259">Timnʼäːqımtɨ</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_973" n="HIAT:w" s="T260">əːtɨrkɨqolamnɨtɨ</ts>
                  <nts id="Seg_974" n="HIAT:ip">.</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_977" n="HIAT:u" s="T261">
                  <ts e="T262" id="Seg_979" n="HIAT:w" s="T261">Ponä</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_982" n="HIAT:w" s="T262">na</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_985" n="HIAT:w" s="T263">čʼap</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_988" n="HIAT:w" s="T264">tannɨntɔːtet</ts>
                  <nts id="Seg_989" n="HIAT:ip">.</nts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_992" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_994" n="HIAT:w" s="T265">Montɨ</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_997" n="HIAT:w" s="T266">ɨntɨlʼ</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1000" n="HIAT:w" s="T267">tıšap</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1003" n="HIAT:w" s="T268">ɨnnä</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1006" n="HIAT:w" s="T269">iːqɨlpɔːtet</ts>
                  <nts id="Seg_1007" n="HIAT:ip">.</nts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_1010" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_1012" n="HIAT:w" s="T270">Ütqɨnɨ</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1015" n="HIAT:w" s="T271">konnä</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1018" n="HIAT:w" s="T272">toqalʼ</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1021" n="HIAT:w" s="T273">čʼüntɔːtɨt</ts>
                  <nts id="Seg_1022" n="HIAT:ip">.</nts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_1025" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_1027" n="HIAT:w" s="T274">Ponä</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1030" n="HIAT:w" s="T275">tantɨlä</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1033" n="HIAT:w" s="T276">nık</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1036" n="HIAT:w" s="T277">kətɨŋɨt</ts>
                  <nts id="Seg_1037" n="HIAT:ip">.</nts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_1040" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_1042" n="HIAT:w" s="T278">Konnä</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1045" n="HIAT:w" s="T279">tantɨlä</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1048" n="HIAT:w" s="T280">amɨrŋɨlɨt</ts>
                  <nts id="Seg_1049" n="HIAT:ip">,</nts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1052" n="HIAT:w" s="T281">qɔːtɨnıːsʼa</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1055" n="HIAT:w" s="T282">müttɨlɨmɨt</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1058" n="HIAT:w" s="T283">tentɨlä</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1061" n="HIAT:w" s="T284">poːqɨ</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1064" n="HIAT:w" s="T285">ütqɨnɨ</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1067" n="HIAT:w" s="T286">konnä</ts>
                  <nts id="Seg_1068" n="HIAT:ip">.</nts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1071" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1073" n="HIAT:w" s="T287">Aj</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1076" n="HIAT:w" s="T288">qaj</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1079" n="HIAT:w" s="T289">meːqolamnɨlɨt</ts>
                  <nts id="Seg_1080" n="HIAT:ip">?</nts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1083" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1085" n="HIAT:w" s="T290">Konnä</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1088" n="HIAT:w" s="T291">tantɨlä</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1091" n="HIAT:w" s="T292">amɨrŋɨlɨt</ts>
                  <nts id="Seg_1092" n="HIAT:ip">.</nts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T298" id="Seg_1095" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1097" n="HIAT:w" s="T293">A</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1100" n="HIAT:w" s="T294">nıŋ</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1103" n="HIAT:w" s="T295">ɛːsä</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1106" n="HIAT:w" s="T296">märqɨ</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1109" n="HIAT:w" s="T297">timnʼatɨ</ts>
                  <nts id="Seg_1110" n="HIAT:ip">.</nts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_1113" n="HIAT:u" s="T298">
                  <ts e="T299" id="Seg_1115" n="HIAT:w" s="T298">Konnä</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1118" n="HIAT:w" s="T299">tantɨlä</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1121" n="HIAT:w" s="T300">amɨrlɨmɨt</ts>
                  <nts id="Seg_1122" n="HIAT:ip">.</nts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_1125" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1127" n="HIAT:w" s="T301">Konna</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1130" n="HIAT:w" s="T302">jep</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1133" n="HIAT:w" s="T303">tattɔːtet</ts>
                  <nts id="Seg_1134" n="HIAT:ip">.</nts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T310" id="Seg_1137" n="HIAT:u" s="T304">
                  <ts e="T305" id="Seg_1139" n="HIAT:w" s="T304">Ɨntɨlʼ</ts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1142" n="HIAT:w" s="T305">tıšamtɨt</ts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1145" n="HIAT:w" s="T306">karrät</ts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1148" n="HIAT:w" s="T307">antoːqɨntɨt</ts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1151" n="HIAT:w" s="T308">pillä</ts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1154" n="HIAT:w" s="T309">qəːčʼalnɔːtɨt</ts>
                  <nts id="Seg_1155" n="HIAT:ip">.</nts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_1158" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_1160" n="HIAT:w" s="T310">Mɔːttɨ</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1163" n="HIAT:w" s="T311">na</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1166" n="HIAT:w" s="T312">jep</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1169" n="HIAT:w" s="T313">šeːrtɔːtɨt</ts>
                  <nts id="Seg_1170" n="HIAT:ip">.</nts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T324" id="Seg_1173" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1175" n="HIAT:w" s="T314">Mərqɨ</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1178" n="HIAT:w" s="T315">laŋalʼ</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1181" n="HIAT:w" s="T316">qumɨlʼ</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1184" n="HIAT:w" s="T317">mɔːter</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1187" n="HIAT:w" s="T318">Palnan</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1190" n="HIAT:w" s="T319">optı</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1193" n="HIAT:w" s="T320">ukkɨr</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1196" n="HIAT:w" s="T321">qəntɨ</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1199" n="HIAT:w" s="T322">na</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1202" n="HIAT:w" s="T323">omnɨntɔːtet</ts>
                  <nts id="Seg_1203" n="HIAT:ip">.</nts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_1206" n="HIAT:u" s="T324">
                  <ts e="T325" id="Seg_1208" n="HIAT:w" s="T324">Takɨlʼ</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1211" n="HIAT:w" s="T325">ɔːtälʼ</ts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1214" n="HIAT:w" s="T326">nʼoːtɨlʼ</ts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1217" n="HIAT:w" s="T327">timnʼantɨsä</ts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1220" n="HIAT:w" s="T328">aj</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1223" n="HIAT:w" s="T329">ukkɨr</ts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1226" n="HIAT:w" s="T330">qəntɨ</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1229" n="HIAT:w" s="T331">na</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1232" n="HIAT:w" s="T332">omnɨntɔːtɨt</ts>
                  <nts id="Seg_1233" n="HIAT:ip">.</nts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T339" id="Seg_1236" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_1238" n="HIAT:w" s="T333">Nɔːkɨr</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1241" n="HIAT:w" s="T334">timnʼaset</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1244" n="HIAT:w" s="T335">muntɨk</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1247" n="HIAT:w" s="T336">ukkɨr</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1250" n="HIAT:w" s="T337">qönte</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1253" n="HIAT:w" s="T338">omtelɔːtɨt</ts>
                  <nts id="Seg_1254" n="HIAT:ip">.</nts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T345" id="Seg_1257" n="HIAT:u" s="T339">
                  <ts e="T340" id="Seg_1259" n="HIAT:w" s="T339">Nɨːn</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1262" n="HIAT:w" s="T340">nık</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1265" n="HIAT:w" s="T341">kətɨsɔːtɨt</ts>
                  <nts id="Seg_1266" n="HIAT:ip">:</nts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1269" n="HIAT:w" s="T342">Paŋɨmɨt</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1272" n="HIAT:w" s="T343">qaj</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1275" n="HIAT:w" s="T344">əmɨltɨmnɨmɨt</ts>
                  <nts id="Seg_1276" n="HIAT:ip">.</nts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T347" id="Seg_1279" n="HIAT:u" s="T345">
                  <ts e="T346" id="Seg_1281" n="HIAT:w" s="T345">Paŋɨlɨt</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1284" n="HIAT:w" s="T346">ɛːŋa</ts>
                  <nts id="Seg_1285" n="HIAT:ip">?</nts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T350" id="Seg_1288" n="HIAT:u" s="T347">
                  <ts e="T348" id="Seg_1290" n="HIAT:w" s="T347">Paŋɨsä</ts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1293" n="HIAT:w" s="T348">šınet</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1296" n="HIAT:w" s="T349">miŋɨlɨt</ts>
                  <nts id="Seg_1297" n="HIAT:ip">.</nts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T353" id="Seg_1300" n="HIAT:u" s="T350">
                  <ts e="T351" id="Seg_1302" n="HIAT:w" s="T350">Amɨrqo</ts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1305" n="HIAT:w" s="T351">solap</ts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1308" n="HIAT:w" s="T352">meːqɨllɨmɨt</ts>
                  <nts id="Seg_1309" n="HIAT:ip">.</nts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T360" id="Seg_1312" n="HIAT:u" s="T353">
                  <ts e="T354" id="Seg_1314" n="HIAT:w" s="T353">Paŋɨmtɨt</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1317" n="HIAT:w" s="T354">muntɨk</ts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1320" n="HIAT:w" s="T355">miŋɔːtɨt</ts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1323" n="HIAT:w" s="T356">laŋalʼ</ts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1326" n="HIAT:w" s="T357">qumɨlʼ</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1329" n="HIAT:w" s="T358">nɔːkɨr</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1332" n="HIAT:w" s="T359">timnʼasɨt</ts>
                  <nts id="Seg_1333" n="HIAT:ip">.</nts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T367" id="Seg_1336" n="HIAT:u" s="T360">
                  <ts e="T361" id="Seg_1338" n="HIAT:w" s="T360">A</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1341" n="HIAT:w" s="T361">Palnalʼ</ts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1344" n="HIAT:w" s="T362">nɔːkɨr</ts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1347" n="HIAT:w" s="T363">timnʼasɨt</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1350" n="HIAT:w" s="T364">okoːt</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1353" n="HIAT:w" s="T365">jap</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1356" n="HIAT:w" s="T366">əːtɨrkɨsɔːtɨt</ts>
                  <nts id="Seg_1357" n="HIAT:ip">.</nts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T372" id="Seg_1360" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_1362" n="HIAT:w" s="T367">Märqɨ</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1365" n="HIAT:w" s="T368">timnʼatɨ</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1368" n="HIAT:w" s="T369">kutar</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1371" n="HIAT:w" s="T370">paŋɨsä</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1374" n="HIAT:w" s="T371">qättɨntɨtɨ</ts>
                  <nts id="Seg_1375" n="HIAT:ip">.</nts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1378" n="HIAT:u" s="T372">
                  <ts e="T373" id="Seg_1380" n="HIAT:w" s="T372">Təpɨt</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1383" n="HIAT:w" s="T373">aj</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1386" n="HIAT:w" s="T374">nılʼčʼɨk</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1389" n="HIAT:w" s="T375">qättɨntɔːtɨt</ts>
                  <nts id="Seg_1390" n="HIAT:ip">.</nts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_1393" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1395" n="HIAT:w" s="T376">Märqɨ</ts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1398" n="HIAT:w" s="T377">ira</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1401" n="HIAT:w" s="T378">ukkur</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1404" n="HIAT:w" s="T379">čʼontoːqɨt</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1407" n="HIAT:w" s="T380">nanɨ</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1410" n="HIAT:w" s="T381">matta</ts>
                  <nts id="Seg_1411" n="HIAT:ip">,</nts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1414" n="HIAT:w" s="T382">sajesa</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1417" n="HIAT:w" s="T383">ijap</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1420" n="HIAT:w" s="T384">mɨrɔːtɨt</ts>
                  <nts id="Seg_1421" n="HIAT:ip">.</nts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T393" id="Seg_1424" n="HIAT:u" s="T385">
                  <ts e="T386" id="Seg_1426" n="HIAT:w" s="T385">Paŋɨsä</ts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1429" n="HIAT:w" s="T386">nɨː</ts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1432" n="HIAT:w" s="T387">jep</ts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1435" n="HIAT:w" s="T388">qättɔːtɨt</ts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1438" n="HIAT:w" s="T389">laŋalʼ</ts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1441" n="HIAT:w" s="T390">qumɨlʼ</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1444" n="HIAT:w" s="T391">nɔːr</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1447" n="HIAT:w" s="T392">timnʼästɨp</ts>
                  <nts id="Seg_1448" n="HIAT:ip">.</nts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_1451" n="HIAT:u" s="T393">
                  <ts e="T394" id="Seg_1453" n="HIAT:w" s="T393">Taŋɨlʼ</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1456" n="HIAT:w" s="T394">ɔːtälʼ</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1459" n="HIAT:w" s="T395">nʼoːtɨlʼ</ts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1462" n="HIAT:w" s="T396">timnʼämtɨ</ts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1465" n="HIAT:w" s="T397">čʼap</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1468" n="HIAT:w" s="T398">qättɨtɨ</ts>
                  <nts id="Seg_1469" n="HIAT:ip">.</nts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_1472" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_1474" n="HIAT:w" s="T399">Ola</ts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1477" n="HIAT:w" s="T400">šünʼčʼipɨlʼ</ts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1480" n="HIAT:w" s="T401">təttɨp</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1483" n="HIAT:w" s="T402">qättɨtɨ</ts>
                  <nts id="Seg_1484" n="HIAT:ip">.</nts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T412" id="Seg_1487" n="HIAT:u" s="T403">
                  <ts e="T404" id="Seg_1489" n="HIAT:w" s="T403">Palnat</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1492" n="HIAT:w" s="T404">timnʼätɨ</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1495" n="HIAT:w" s="T405">taŋɨlʼ</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1498" n="HIAT:w" s="T406">ɔːtalʼ</ts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1501" n="HIAT:w" s="T407">nʼoːtɨlʼ</ts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1504" n="HIAT:w" s="T408">tɨmnʼätɨ</ts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1507" n="HIAT:w" s="T409">aj</ts>
                  <nts id="Seg_1508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1510" n="HIAT:w" s="T410">nɨːnɨ</ts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1513" n="HIAT:w" s="T411">pakta</ts>
                  <nts id="Seg_1514" n="HIAT:ip">.</nts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T417" id="Seg_1517" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_1519" n="HIAT:w" s="T412">Nʼoːla</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1522" n="HIAT:w" s="T413">qəntɨtɨ</ts>
                  <nts id="Seg_1523" n="HIAT:ip">,</nts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1526" n="HIAT:w" s="T414">täːqamtɨ</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1529" n="HIAT:w" s="T415">orqɨllä</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1532" n="HIAT:w" s="T416">nʼoːtɨŋɨt</ts>
                  <nts id="Seg_1533" n="HIAT:ip">.</nts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T422" id="Seg_1536" n="HIAT:u" s="T417">
                  <ts e="T418" id="Seg_1538" n="HIAT:w" s="T417">Toːnna</ts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1541" n="HIAT:w" s="T418">šitɨ</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1544" n="HIAT:w" s="T419">tɨmnʼäsɨqä</ts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1547" n="HIAT:w" s="T420">mɔːtqɨt</ts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1550" n="HIAT:w" s="T421">qalɔːtɨt</ts>
                  <nts id="Seg_1551" n="HIAT:ip">.</nts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T426" id="Seg_1554" n="HIAT:u" s="T422">
                  <ts e="T423" id="Seg_1556" n="HIAT:w" s="T422">Qaj</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1559" n="HIAT:w" s="T423">qənnɨtɨ</ts>
                  <nts id="Seg_1560" n="HIAT:ip">,</nts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1563" n="HIAT:w" s="T424">qaj</ts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1566" n="HIAT:w" s="T425">qattɨjɔːtɨt</ts>
                  <nts id="Seg_1567" n="HIAT:ip">.</nts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T428" id="Seg_1570" n="HIAT:u" s="T426">
                  <ts e="T427" id="Seg_1572" n="HIAT:w" s="T426">Nʼoːla</ts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1575" n="HIAT:w" s="T427">qəntɨtɨtɨ</ts>
                  <nts id="Seg_1576" n="HIAT:ip">.</nts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T432" id="Seg_1579" n="HIAT:u" s="T428">
                  <ts e="T429" id="Seg_1581" n="HIAT:w" s="T428">Nʼennät</ts>
                  <nts id="Seg_1582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1584" n="HIAT:w" s="T429">mitallä</ts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1587" n="HIAT:w" s="T430">čʼap</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1590" n="HIAT:w" s="T431">počʼɔːlnɨtɨ</ts>
                  <nts id="Seg_1591" n="HIAT:ip">.</nts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T434" id="Seg_1594" n="HIAT:u" s="T432">
                  <ts e="T433" id="Seg_1596" n="HIAT:w" s="T432">Lʼakčʼitɨ</ts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1599" n="HIAT:w" s="T433">pačʼalnɨt</ts>
                  <nts id="Seg_1600" n="HIAT:ip">.</nts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T438" id="Seg_1603" n="HIAT:u" s="T434">
                  <ts e="T435" id="Seg_1605" n="HIAT:w" s="T434">Qamtä</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1608" n="HIAT:w" s="T435">alʼčʼa</ts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1611" n="HIAT:w" s="T436">laŋalʼ</ts>
                  <nts id="Seg_1612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1614" n="HIAT:w" s="T437">qup</ts>
                  <nts id="Seg_1615" n="HIAT:ip">.</nts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_1618" n="HIAT:u" s="T438">
                  <ts e="T439" id="Seg_1620" n="HIAT:w" s="T438">Šittäl</ts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1623" n="HIAT:w" s="T439">čʼap</ts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1626" n="HIAT:w" s="T440">pačʼalnɨt</ts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1629" n="HIAT:w" s="T441">täːqat</ts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1632" n="HIAT:w" s="T442">kət</ts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1635" n="HIAT:w" s="T443">čʼoːmɨt</ts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1638" n="HIAT:w" s="T444">orqɨlnɨt</ts>
                  <nts id="Seg_1639" n="HIAT:ip">.</nts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_1642" n="HIAT:u" s="T445">
                  <ts e="T446" id="Seg_1644" n="HIAT:w" s="T445">Topɨtɨ</ts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1647" n="HIAT:w" s="T446">toːq</ts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1650" n="HIAT:w" s="T447">säppɛːmpa</ts>
                  <nts id="Seg_1651" n="HIAT:ip">.</nts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T452" id="Seg_1654" n="HIAT:u" s="T448">
                  <ts e="T449" id="Seg_1656" n="HIAT:w" s="T448">Palna</ts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1659" n="HIAT:w" s="T449">toːnna</ts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1662" n="HIAT:w" s="T450">timnʼäqɨmta</ts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1665" n="HIAT:w" s="T451">qätpatɨ</ts>
                  <nts id="Seg_1666" n="HIAT:ip">.</nts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T457" id="Seg_1669" n="HIAT:u" s="T452">
                  <ts e="T453" id="Seg_1671" n="HIAT:w" s="T452">Timnʼäntɨ</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1674" n="HIAT:w" s="T453">mättɨt</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1677" n="HIAT:w" s="T454">šüː</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1680" n="HIAT:w" s="T455">nʼennɨ</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1683" n="HIAT:w" s="T456">paktɨ</ts>
                  <nts id="Seg_1684" n="HIAT:ip">.</nts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T460" id="Seg_1687" n="HIAT:u" s="T457">
                  <ts e="T458" id="Seg_1689" n="HIAT:w" s="T457">Qoŋɨtɨ</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1692" n="HIAT:w" s="T458">mompa</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1695" n="HIAT:w" s="T459">orqɨlʼpetɨ</ts>
                  <nts id="Seg_1696" n="HIAT:ip">.</nts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T462" id="Seg_1699" n="HIAT:u" s="T460">
                  <ts e="T461" id="Seg_1701" n="HIAT:w" s="T460">Mat</ts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1704" n="HIAT:w" s="T461">tülɨlak</ts>
                  <nts id="Seg_1705" n="HIAT:ip">.</nts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T468" id="Seg_1708" n="HIAT:u" s="T462">
                  <ts e="T463" id="Seg_1710" n="HIAT:w" s="T462">Na</ts>
                  <nts id="Seg_1711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1713" n="HIAT:w" s="T463">küšilʼ</ts>
                  <nts id="Seg_1714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1716" n="HIAT:w" s="T464">laka</ts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1719" n="HIAT:w" s="T465">aj</ts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1722" n="HIAT:w" s="T466">qaj</ts>
                  <nts id="Seg_1723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1725" n="HIAT:w" s="T467">meːtɨtɨ</ts>
                  <nts id="Seg_1726" n="HIAT:ip">.</nts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T470" id="Seg_1729" n="HIAT:u" s="T468">
                  <ts e="T469" id="Seg_1731" n="HIAT:w" s="T468">Mat</ts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1734" n="HIAT:w" s="T469">tamɛntak</ts>
                  <nts id="Seg_1735" n="HIAT:ip">.</nts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T475" id="Seg_1738" n="HIAT:u" s="T470">
                  <ts e="T471" id="Seg_1740" n="HIAT:w" s="T470">Nɨːnɨ</ts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1743" n="HIAT:w" s="T471">nɔːkɨr</ts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1746" n="HIAT:w" s="T472">tɨmnʼästɨp</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1749" n="HIAT:w" s="T473">muntɨk</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1752" n="HIAT:w" s="T474">qəttɛːjɔːtɨt</ts>
                  <nts id="Seg_1753" n="HIAT:ip">.</nts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T485" id="Seg_1756" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_1758" n="HIAT:w" s="T475">Okkɨr</ts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1761" n="HIAT:w" s="T476">taːt</ts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1764" n="HIAT:w" s="T477">tättɨt</ts>
                  <nts id="Seg_1765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1767" n="HIAT:w" s="T478">čʼontoːqɨt</ts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1770" n="HIAT:w" s="T479">nanɨ</ts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1773" n="HIAT:w" s="T480">ilɨmpɔːtɨt</ts>
                  <nts id="Seg_1774" n="HIAT:ip">,</nts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1777" n="HIAT:w" s="T481">nʼi</ts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1780" n="HIAT:w" s="T482">qajit</ts>
                  <nts id="Seg_1781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1783" n="HIAT:w" s="T483">sümɨ</ts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1786" n="HIAT:w" s="T484">čʼäːŋka</ts>
                  <nts id="Seg_1787" n="HIAT:ip">.</nts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T488" id="Seg_1790" n="HIAT:u" s="T485">
                  <ts e="T486" id="Seg_1792" n="HIAT:w" s="T485">Qənŋɔːtɨt</ts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1795" n="HIAT:w" s="T486">laŋalʼ</ts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1798" n="HIAT:w" s="T487">qumɨlʼ</ts>
                  <nts id="Seg_1799" n="HIAT:ip">.</nts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T491" id="Seg_1802" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_1804" n="HIAT:w" s="T488">Imaqotalʼ</ts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1807" n="HIAT:w" s="T489">nılʼčʼik</ts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1810" n="HIAT:w" s="T490">tɛnɨrna</ts>
                  <nts id="Seg_1811" n="HIAT:ip">.</nts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T496" id="Seg_1814" n="HIAT:u" s="T491">
                  <ts e="T492" id="Seg_1816" n="HIAT:w" s="T491">Ijaiːmɨ</ts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1819" n="HIAT:w" s="T492">qəssɔːtɨt</ts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1822" n="HIAT:w" s="T493">meːltɨt</ts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1825" n="HIAT:w" s="T494">qoja</ts>
                  <nts id="Seg_1826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1828" n="HIAT:w" s="T495">mɔːtɨŋnɔːtɨt</ts>
                  <nts id="Seg_1829" n="HIAT:ip">.</nts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T504" id="Seg_1832" n="HIAT:u" s="T496">
                  <ts e="T497" id="Seg_1834" n="HIAT:w" s="T496">Meːlle</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1837" n="HIAT:w" s="T497">mɨta</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1840" n="HIAT:w" s="T498">šölʼqup</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1843" n="HIAT:w" s="T499">ijamtɨ</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1846" n="HIAT:w" s="T500">namɨt</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1849" n="HIAT:w" s="T501">mɔːntɨ</ts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1852" n="HIAT:w" s="T502">somak</ts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1855" n="HIAT:w" s="T503">kətɨmpatɨ</ts>
                  <nts id="Seg_1856" n="HIAT:ip">.</nts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T508" id="Seg_1859" n="HIAT:u" s="T504">
                  <ts e="T505" id="Seg_1861" n="HIAT:w" s="T504">Mat</ts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1864" n="HIAT:w" s="T505">kʼe</ts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1867" n="HIAT:w" s="T506">ijamɨ</ts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1870" n="HIAT:w" s="T507">kəːtɨptäkek</ts>
                  <nts id="Seg_1871" n="HIAT:ip">.</nts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T512" id="Seg_1874" n="HIAT:u" s="T508">
                  <ts e="T509" id="Seg_1876" n="HIAT:w" s="T508">Nümanɨ</ts>
                  <nts id="Seg_1877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1879" n="HIAT:w" s="T509">ukɨp</ts>
                  <nts id="Seg_1880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1882" n="HIAT:w" s="T510">aj</ts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1885" n="HIAT:w" s="T511">musɨltɨkap</ts>
                  <nts id="Seg_1886" n="HIAT:ip">.</nts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T518" id="Seg_1889" n="HIAT:u" s="T512">
                  <ts e="T513" id="Seg_1891" n="HIAT:w" s="T512">Šölʼqumɨlʼ</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1894" n="HIAT:w" s="T513">ima</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1897" n="HIAT:w" s="T514">meːlle</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1900" n="HIAT:w" s="T515">qəː</ts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1903" n="HIAT:w" s="T516">ɛːŋa</ts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1906" n="HIAT:w" s="T517">mannoːnɨ</ts>
                  <nts id="Seg_1907" n="HIAT:ip">.</nts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T518" id="Seg_1909" n="sc" s="T0">
               <ts e="T1" id="Seg_1911" n="e" s="T0">Palna. </ts>
               <ts e="T2" id="Seg_1913" n="e" s="T1">Palna </ts>
               <ts e="T3" id="Seg_1915" n="e" s="T2">nɔːkɨr </ts>
               <ts e="T4" id="Seg_1917" n="e" s="T3">timnʼasɨk </ts>
               <ts e="T5" id="Seg_1919" n="e" s="T4">ilimmɨntɔːtɨt. </ts>
               <ts e="T6" id="Seg_1921" n="e" s="T5">Šölʼqumɨlʼ </ts>
               <ts e="T7" id="Seg_1923" n="e" s="T6">mɔːtɨrɨlʼ </ts>
               <ts e="T8" id="Seg_1925" n="e" s="T7">irra </ts>
               <ts e="T9" id="Seg_1927" n="e" s="T8">ɛppɨmmɨnta. </ts>
               <ts e="T10" id="Seg_1929" n="e" s="T9">Orsɨmɨlʼ </ts>
               <ts e="T11" id="Seg_1931" n="e" s="T10">irra </ts>
               <ts e="T12" id="Seg_1933" n="e" s="T11">samɨj </ts>
               <ts e="T13" id="Seg_1935" n="e" s="T12">märqɨ </ts>
               <ts e="T14" id="Seg_1937" n="e" s="T13">timnʼätɨt. </ts>
               <ts e="T15" id="Seg_1939" n="e" s="T14">Čʼontoːqɨlʼ </ts>
               <ts e="T16" id="Seg_1941" n="e" s="T15">timnʼätɨt </ts>
               <ts e="T17" id="Seg_1943" n="e" s="T16">taŋɨlʼ </ts>
               <ts e="T18" id="Seg_1945" n="e" s="T17">ɔːtälʼ </ts>
               <ts e="T19" id="Seg_1947" n="e" s="T18">nʼoːtɨj </ts>
               <ts e="T20" id="Seg_1949" n="e" s="T19">ɛppɨmmɨnta. </ts>
               <ts e="T21" id="Seg_1951" n="e" s="T20">Poːs </ts>
               <ts e="T22" id="Seg_1953" n="e" s="T21">kɨpa </ts>
               <ts e="T23" id="Seg_1955" n="e" s="T22">timnʼätɨ </ts>
               <ts e="T24" id="Seg_1957" n="e" s="T23">kərnʼat </ts>
               <ts e="T25" id="Seg_1959" n="e" s="T24">pɔːralʼ </ts>
               <ts e="T26" id="Seg_1961" n="e" s="T25">nʼoːtɨj </ts>
               <ts e="T27" id="Seg_1963" n="e" s="T26">ɛppɨmmɨnta. </ts>
               <ts e="T28" id="Seg_1965" n="e" s="T27">Nılʼčʼik </ts>
               <ts e="T29" id="Seg_1967" n="e" s="T28">ilimpɔːtɨt. </ts>
               <ts e="T30" id="Seg_1969" n="e" s="T29">A </ts>
               <ts e="T31" id="Seg_1971" n="e" s="T30">na </ts>
               <ts e="T32" id="Seg_1973" n="e" s="T31">Palnalʼ </ts>
               <ts e="T33" id="Seg_1975" n="e" s="T32">märqɨ </ts>
               <ts e="T34" id="Seg_1977" n="e" s="T33">timnʼätɨ </ts>
               <ts e="T35" id="Seg_1979" n="e" s="T34">namɨšʼak </ts>
               <ts e="T36" id="Seg_1981" n="e" s="T35">qəːtɨsä </ts>
               <ts e="T37" id="Seg_1983" n="e" s="T36">ɛːppɨmmɨnta. </ts>
               <ts e="T38" id="Seg_1985" n="e" s="T37">Kuna </ts>
               <ts e="T39" id="Seg_1987" n="e" s="T38">qaj </ts>
               <ts e="T40" id="Seg_1989" n="e" s="T39">tünta, </ts>
               <ts e="T41" id="Seg_1991" n="e" s="T40">muntɨk </ts>
               <ts e="T42" id="Seg_1993" n="e" s="T41">tɛnɨnɨmpatɨ. </ts>
               <ts e="T43" id="Seg_1995" n="e" s="T42">Suːmpɨtɨlʼ </ts>
               <ts e="T44" id="Seg_1997" n="e" s="T43">təːtɨpɨlʼ </ts>
               <ts e="T45" id="Seg_1999" n="e" s="T44">irra </ts>
               <ts e="T46" id="Seg_2001" n="e" s="T45">ɛːppɨmmɨnta. </ts>
               <ts e="T47" id="Seg_2003" n="e" s="T46">Kəːsɨp </ts>
               <ts e="T48" id="Seg_2005" n="e" s="T47">meːtɨmpatɨ </ts>
               <ts e="T49" id="Seg_2007" n="e" s="T48">ontɨ </ts>
               <ts e="T50" id="Seg_2009" n="e" s="T49">iliptäːqɨntɨ. </ts>
               <ts e="T51" id="Seg_2011" n="e" s="T50">Čʼɔːtɨrna </ts>
               <ts e="T52" id="Seg_2013" n="e" s="T51">kəːsɨp, </ts>
               <ts e="T53" id="Seg_2015" n="e" s="T52">nık </ts>
               <ts e="T54" id="Seg_2017" n="e" s="T53">meːtɨmpatɨ, </ts>
               <ts e="T55" id="Seg_2019" n="e" s="T54">čʼaŋak </ts>
               <ts e="T56" id="Seg_2021" n="e" s="T55">pünon. </ts>
               <ts e="T57" id="Seg_2023" n="e" s="T56">A </ts>
               <ts e="T58" id="Seg_2025" n="e" s="T57">rušit </ts>
               <ts e="T59" id="Seg_2027" n="e" s="T58">meːtɨmpɔːtɨt </ts>
               <ts e="T60" id="Seg_2029" n="e" s="T59">aša </ts>
               <ts e="T61" id="Seg_2031" n="e" s="T60">tɛnɨmop </ts>
               <ts e="T62" id="Seg_2033" n="e" s="T61">qajen </ts>
               <ts e="T63" id="Seg_2035" n="e" s="T62">nɔːnɨ </ts>
               <ts e="T64" id="Seg_2037" n="e" s="T63">… </ts>
               <ts e="T65" id="Seg_2039" n="e" s="T64">Tɨp </ts>
               <ts e="T66" id="Seg_2041" n="e" s="T65">meːtɨmpatɨ, </ts>
               <ts e="T67" id="Seg_2043" n="e" s="T66">kəːsɨtqo </ts>
               <ts e="T68" id="Seg_2045" n="e" s="T67">püp </ts>
               <ts e="T69" id="Seg_2047" n="e" s="T68">meːtɨmpatɨ. </ts>
               <ts e="T70" id="Seg_2049" n="e" s="T69">Na </ts>
               <ts e="T71" id="Seg_2051" n="e" s="T70">pün </ts>
               <ts e="T72" id="Seg_2053" n="e" s="T71">nɔːnɨ </ts>
               <ts e="T73" id="Seg_2055" n="e" s="T72">kəːsɨlʼ </ts>
               <ts e="T74" id="Seg_2057" n="e" s="T73">porqɨp </ts>
               <ts e="T75" id="Seg_2059" n="e" s="T74">meːtɨmpatɨ. </ts>
               <ts e="T76" id="Seg_2061" n="e" s="T75">Säq </ts>
               <ts e="T77" id="Seg_2063" n="e" s="T76">kəːsɨlʼ </ts>
               <ts e="T78" id="Seg_2065" n="e" s="T77">ɨntɨ </ts>
               <ts e="T79" id="Seg_2067" n="e" s="T78">meːtɨmpat, </ts>
               <ts e="T80" id="Seg_2069" n="e" s="T79">säq </ts>
               <ts e="T81" id="Seg_2071" n="e" s="T80">kəːsɨlʼ </ts>
               <ts e="T82" id="Seg_2073" n="e" s="T81">komap </ts>
               <ts e="T83" id="Seg_2075" n="e" s="T82">meːtɨmmɨntɨtɨ. </ts>
               <ts e="T84" id="Seg_2077" n="e" s="T83">Nantɨsä </ts>
               <ts e="T85" id="Seg_2079" n="e" s="T84">aj </ts>
               <ts e="T86" id="Seg_2081" n="e" s="T85">ilɨmpa. </ts>
               <ts e="T87" id="Seg_2083" n="e" s="T86">A </ts>
               <ts e="T88" id="Seg_2085" n="e" s="T87">müttɨlä </ts>
               <ts e="T89" id="Seg_2087" n="e" s="T88">aša </ts>
               <ts e="T90" id="Seg_2089" n="e" s="T89">ičʼirpa. </ts>
               <ts e="T91" id="Seg_2091" n="e" s="T90">Täpɨpkine </ts>
               <ts e="T92" id="Seg_2093" n="e" s="T91">müttɨlä </ts>
               <ts e="T93" id="Seg_2095" n="e" s="T92">ičʼirpatɨt. </ts>
               <ts e="T94" id="Seg_2097" n="e" s="T93">Kepkisolʼa </ts>
               <ts e="T95" id="Seg_2099" n="e" s="T94">kunɨ </ts>
               <ts e="T96" id="Seg_2101" n="e" s="T95">čʼap </ts>
               <ts e="T97" id="Seg_2103" n="e" s="T96">tüntɔːtɨt </ts>
               <ts e="T98" id="Seg_2105" n="e" s="T97">təp </ts>
               <ts e="T99" id="Seg_2107" n="e" s="T98">mun </ts>
               <ts e="T100" id="Seg_2109" n="e" s="T99">tɛnimɨt. </ts>
               <ts e="T101" id="Seg_2111" n="e" s="T100">Qälɨt </ts>
               <ts e="T102" id="Seg_2113" n="e" s="T101">ičʼirpɔːtɨt </ts>
               <ts e="T103" id="Seg_2115" n="e" s="T102">müttɨqo. </ts>
               <ts e="T104" id="Seg_2117" n="e" s="T103">Laŋalʼ </ts>
               <ts e="T105" id="Seg_2119" n="e" s="T104">qumɨt </ts>
               <ts e="T106" id="Seg_2121" n="e" s="T105">pɛlʼaqɨt </ts>
               <ts e="T107" id="Seg_2123" n="e" s="T106">aj </ts>
               <ts e="T108" id="Seg_2125" n="e" s="T107">nɔːr </ts>
               <ts e="T109" id="Seg_2127" n="e" s="T108">timnʼäsɨt </ts>
               <ts e="T110" id="Seg_2129" n="e" s="T109">ɛppɨntɔːtɨt. </ts>
               <ts e="T111" id="Seg_2131" n="e" s="T110">Aj </ts>
               <ts e="T112" id="Seg_2133" n="e" s="T111">peːlä </ts>
               <ts e="T113" id="Seg_2135" n="e" s="T112">ičʼirpɔːtɨt </ts>
               <ts e="T114" id="Seg_2137" n="e" s="T113">tapčʼeːlɨ </ts>
               <ts e="T115" id="Seg_2139" n="e" s="T114">qumɨlʼ </ts>
               <ts e="T116" id="Seg_2141" n="e" s="T115">nɔːr </ts>
               <ts e="T117" id="Seg_2143" n="e" s="T116">tɨmnʼastɨp. </ts>
               <ts e="T118" id="Seg_2145" n="e" s="T117">Təm </ts>
               <ts e="T119" id="Seg_2147" n="e" s="T118">ilɨmpa </ts>
               <ts e="T120" id="Seg_2149" n="e" s="T119">okoːt </ts>
               <ts e="T121" id="Seg_2151" n="e" s="T120">pulʼčʼos </ts>
               <ts e="T122" id="Seg_2153" n="e" s="T121">qoltoːqɨt, </ts>
               <ts e="T123" id="Seg_2155" n="e" s="T122">turuhanska </ts>
               <ts e="T124" id="Seg_2157" n="e" s="T123">kərrelʼ </ts>
               <ts e="T125" id="Seg_2159" n="e" s="T124">pɛlʼaqɨt </ts>
               <ts e="T126" id="Seg_2161" n="e" s="T125">čʼuːroːqɨt. </ts>
               <ts e="T127" id="Seg_2163" n="e" s="T126">Laŋalʼ </ts>
               <ts e="T128" id="Seg_2165" n="e" s="T127">qumɨlʼ </ts>
               <ts e="T129" id="Seg_2167" n="e" s="T128">nɔːkɨr </ts>
               <ts e="T130" id="Seg_2169" n="e" s="T129">timnʼäsɨt </ts>
               <ts e="T131" id="Seg_2171" n="e" s="T130">ukkɨr </ts>
               <ts e="T132" id="Seg_2173" n="e" s="T131">tät </ts>
               <ts e="T133" id="Seg_2175" n="e" s="T132">čʼontoːqɨt </ts>
               <ts e="T134" id="Seg_2177" n="e" s="T133">nılʼčʼik </ts>
               <ts e="T135" id="Seg_2179" n="e" s="T134">ɛːsɔːtɨt. </ts>
               <ts e="T136" id="Seg_2181" n="e" s="T135">Palnalʼ </ts>
               <ts e="T137" id="Seg_2183" n="e" s="T136">nɔːr </ts>
               <ts e="T138" id="Seg_2185" n="e" s="T137">tɨmnʼasɨtɨp </ts>
               <ts e="T139" id="Seg_2187" n="e" s="T138">peːlʼa </ts>
               <ts e="T140" id="Seg_2189" n="e" s="T139">qənqolamnɔːtɨt. </ts>
               <ts e="T141" id="Seg_2191" n="e" s="T140">A </ts>
               <ts e="T142" id="Seg_2193" n="e" s="T141">Palnalʼ </ts>
               <ts e="T143" id="Seg_2195" n="e" s="T142">timnʼäsɨt </ts>
               <ts e="T144" id="Seg_2197" n="e" s="T143">qənɔːtɨt. </ts>
               <ts e="T145" id="Seg_2199" n="e" s="T144">Tɨmnʼäiːqɨntɨ </ts>
               <ts e="T146" id="Seg_2201" n="e" s="T145">nılʼčʼik </ts>
               <ts e="T147" id="Seg_2203" n="e" s="T146">kətɨŋɨt. </ts>
               <ts e="T148" id="Seg_2205" n="e" s="T147">Laŋalʼ </ts>
               <ts e="T149" id="Seg_2207" n="e" s="T148">qumɨlʼ </ts>
               <ts e="T150" id="Seg_2209" n="e" s="T149">nɔːr </ts>
               <ts e="T151" id="Seg_2211" n="e" s="T150">tɨmnʼäsɨt </ts>
               <ts e="T152" id="Seg_2213" n="e" s="T151">na </ts>
               <ts e="T153" id="Seg_2215" n="e" s="T152">tüːqolamtɔːtɨt </ts>
               <ts e="T154" id="Seg_2217" n="e" s="T153">müttelä </ts>
               <ts e="T155" id="Seg_2219" n="e" s="T154">meːqɨnɨt. </ts>
               <ts e="T156" id="Seg_2221" n="e" s="T155">Karra </ts>
               <ts e="T157" id="Seg_2223" n="e" s="T156">čʼuːrontɨ </ts>
               <ts e="T158" id="Seg_2225" n="e" s="T157">mɔːtɨŋlɨmɨt. </ts>
               <ts e="T159" id="Seg_2227" n="e" s="T158">Čʼuːrontɨ </ts>
               <ts e="T160" id="Seg_2229" n="e" s="T159">mɔːtɨŋnʼɛntɨmɨt. </ts>
               <ts e="T161" id="Seg_2231" n="e" s="T160">Apsɨp </ts>
               <ts e="T162" id="Seg_2233" n="e" s="T161">qənnɛntɨmɨt. </ts>
               <ts e="T163" id="Seg_2235" n="e" s="T162">Qumɨt </ts>
               <ts e="T164" id="Seg_2237" n="e" s="T163">kosti </ts>
               <ts e="T165" id="Seg_2239" n="e" s="T164">(mɔːttɨla) </ts>
               <ts e="T166" id="Seg_2241" n="e" s="T165">tüntɔːtɨt. </ts>
               <ts e="T167" id="Seg_2243" n="e" s="T166">Amɨrtɛntɔːtɨt. </ts>
               <ts e="T168" id="Seg_2245" n="e" s="T167">Karra </ts>
               <ts e="T169" id="Seg_2247" n="e" s="T168">qənnɔːtɨt, </ts>
               <ts e="T170" id="Seg_2249" n="e" s="T169">čʼuːrontɨ. </ts>
               <ts e="T171" id="Seg_2251" n="e" s="T170">Kärtɔːtɨt, </ts>
               <ts e="T172" id="Seg_2253" n="e" s="T171">mɔːtɨŋnɔːtɨt. </ts>
               <ts e="T173" id="Seg_2255" n="e" s="T172">Nʼuːtɨsä </ts>
               <ts e="T174" id="Seg_2257" n="e" s="T173">tɔːqqɔːtɨt </ts>
               <ts e="T175" id="Seg_2259" n="e" s="T174">mɔːttɨ </ts>
               <ts e="T176" id="Seg_2261" n="e" s="T175">tolʼ </ts>
               <ts e="T177" id="Seg_2263" n="e" s="T176">pɛlʼaqɨt. </ts>
               <ts e="T178" id="Seg_2265" n="e" s="T177">Tösa </ts>
               <ts e="T179" id="Seg_2267" n="e" s="T178">tɔːqqɔːtɨt. </ts>
               <ts e="T180" id="Seg_2269" n="e" s="T179">ınnän </ts>
               <ts e="T181" id="Seg_2271" n="e" s="T180">ɨllä </ts>
               <ts e="T182" id="Seg_2273" n="e" s="T181">aj </ts>
               <ts e="T183" id="Seg_2275" n="e" s="T182">nʼuːtɨsä </ts>
               <ts e="T184" id="Seg_2277" n="e" s="T183">tɔːqqɔːtɨt. </ts>
               <ts e="T185" id="Seg_2279" n="e" s="T184">Tɨmnʼäqɨntɨkine: </ts>
               <ts e="T186" id="Seg_2281" n="e" s="T185">Kaša </ts>
               <ts e="T187" id="Seg_2283" n="e" s="T186">(avsa) </ts>
               <ts e="T188" id="Seg_2285" n="e" s="T187">mušɨrɨŋɨlɨt. </ts>
               <ts e="T189" id="Seg_2287" n="e" s="T188">Tapčʼeːlɨ </ts>
               <ts e="T190" id="Seg_2289" n="e" s="T189">tüntɔːtɨt. </ts>
               <ts e="T191" id="Seg_2291" n="e" s="T190">Ürɨlʼ </ts>
               <ts e="T192" id="Seg_2293" n="e" s="T191">pirnʼäp </ts>
               <ts e="T193" id="Seg_2295" n="e" s="T192">mɔːttɨ </ts>
               <ts e="T194" id="Seg_2297" n="e" s="T193">tultɨŋɨlɨt. </ts>
               <ts e="T195" id="Seg_2299" n="e" s="T194">Kašap </ts>
               <ts e="T196" id="Seg_2301" n="e" s="T195">(avsap) </ts>
               <ts e="T197" id="Seg_2303" n="e" s="T196">kärrä </ts>
               <ts e="T198" id="Seg_2305" n="e" s="T197">mušɨrɔːtɨt. </ts>
               <ts e="T199" id="Seg_2307" n="e" s="T198">Nanä </ts>
               <ts e="T200" id="Seg_2309" n="e" s="T199">na </ts>
               <ts e="T201" id="Seg_2311" n="e" s="T200">ɔːmtɔːtɨt. </ts>
               <ts e="T202" id="Seg_2313" n="e" s="T201">(Čʼeːlɨtɨ) </ts>
               <ts e="T203" id="Seg_2315" n="e" s="T202">čʼeːlʼ </ts>
               <ts e="T204" id="Seg_2317" n="e" s="T203">čʼontɨlʼ </ts>
               <ts e="T205" id="Seg_2319" n="e" s="T204">kotäntɨ </ts>
               <ts e="T206" id="Seg_2321" n="e" s="T205">korrɨmɔːnnɨ. </ts>
               <ts e="T207" id="Seg_2323" n="e" s="T206">Nʼennäntɨ </ts>
               <ts e="T208" id="Seg_2325" n="e" s="T207">antıːrɨ </ts>
               <ts e="T209" id="Seg_2327" n="e" s="T208">qontɨšɛlʼčʼa. </ts>
               <ts e="T210" id="Seg_2329" n="e" s="T209">Nɔːkɨr </ts>
               <ts e="T211" id="Seg_2331" n="e" s="T210">qumɨt </ts>
               <ts e="T212" id="Seg_2333" n="e" s="T211">tıpɨlʼ </ts>
               <ts e="T213" id="Seg_2335" n="e" s="T212">antɨ </ts>
               <ts e="T214" id="Seg_2337" n="e" s="T213">qontɨššɛːnta. </ts>
               <ts e="T215" id="Seg_2339" n="e" s="T214">Timnʼaiːqɨntɨ </ts>
               <ts e="T216" id="Seg_2341" n="e" s="T215">nılʼčʼik </ts>
               <ts e="T217" id="Seg_2343" n="e" s="T216">kətɨŋɨt. </ts>
               <ts e="T218" id="Seg_2345" n="e" s="T217">Paŋɨlʼ </ts>
               <ts e="T219" id="Seg_2347" n="e" s="T218">toqlʼet </ts>
               <ts e="T220" id="Seg_2349" n="e" s="T219">ılla </ts>
               <ts e="T221" id="Seg_2351" n="e" s="T220">ättɔːlnɨlɨt. </ts>
               <ts e="T222" id="Seg_2353" n="e" s="T221">Peːmɨt </ts>
               <ts e="T223" id="Seg_2355" n="e" s="T222">küːtet </ts>
               <ts e="T224" id="Seg_2357" n="e" s="T223">šüː </ts>
               <ts e="T225" id="Seg_2359" n="e" s="T224">säqɨtɔːlɛlʼčʼiŋɨlɨt. </ts>
               <ts e="T226" id="Seg_2361" n="e" s="T225">Nɨːnɨ </ts>
               <ts e="T227" id="Seg_2363" n="e" s="T226">nılʼčʼik </ts>
               <ts e="T228" id="Seg_2365" n="e" s="T227">kətɨŋɨlit. </ts>
               <ts e="T229" id="Seg_2367" n="e" s="T228">Paŋɨmɨt </ts>
               <ts e="T230" id="Seg_2369" n="e" s="T229">čʼäːŋka, </ts>
               <ts e="T231" id="Seg_2371" n="e" s="T230">paŋɨlɨt </ts>
               <ts e="T232" id="Seg_2373" n="e" s="T231">meːqɨnɨt </ts>
               <ts e="T233" id="Seg_2375" n="e" s="T232">miŋɨlɨt, </ts>
               <ts e="T234" id="Seg_2377" n="e" s="T233">solap </ts>
               <ts e="T235" id="Seg_2379" n="e" s="T234">meːqɨllʼčʼilɨmɨt </ts>
               <ts e="T236" id="Seg_2381" n="e" s="T235">amɨrqonɨtqo. </ts>
               <ts e="T237" id="Seg_2383" n="e" s="T236">Nık </ts>
               <ts e="T238" id="Seg_2385" n="e" s="T237">ətɨrkɨŋɨtɨ. </ts>
               <ts e="T239" id="Seg_2387" n="e" s="T238">Kɨpa </ts>
               <ts e="T240" id="Seg_2389" n="e" s="T239">timnʼätɨ </ts>
               <ts e="T241" id="Seg_2391" n="e" s="T240">sɨtkɨqɨt </ts>
               <ts e="T242" id="Seg_2393" n="e" s="T241">ila. </ts>
               <ts e="T243" id="Seg_2395" n="e" s="T242">Täːqamtɨ </ts>
               <ts e="T244" id="Seg_2397" n="e" s="T243">nɨː </ts>
               <ts e="T245" id="Seg_2399" n="e" s="T244">ɨnnä </ts>
               <ts e="T246" id="Seg_2401" n="e" s="T245">čʼoqolnɨt. </ts>
               <ts e="T247" id="Seg_2403" n="e" s="T246">Ɨnnät </ts>
               <ts e="T248" id="Seg_2405" n="e" s="T247">tüt </ts>
               <ts e="T249" id="Seg_2407" n="e" s="T248">poːrɨlʼ </ts>
               <ts e="T250" id="Seg_2409" n="e" s="T249">po </ts>
               <ts e="T251" id="Seg_2411" n="e" s="T250">sɔːrɨmmɨntɔːtɨt. </ts>
               <ts e="T252" id="Seg_2413" n="e" s="T251">Nık </ts>
               <ts e="T253" id="Seg_2415" n="e" s="T252">qoŋɔːtɨt: </ts>
               <ts e="T254" id="Seg_2417" n="e" s="T253">nʼennäntɨ </ts>
               <ts e="T255" id="Seg_2419" n="e" s="T254">qontɨššɛıːŋa </ts>
               <ts e="T256" id="Seg_2421" n="e" s="T255">nɔːkɨr </ts>
               <ts e="T257" id="Seg_2423" n="e" s="T256">qumɨt </ts>
               <ts e="T258" id="Seg_2425" n="e" s="T257">tıpɨlʼ </ts>
               <ts e="T259" id="Seg_2427" n="e" s="T258">antɨ. </ts>
               <ts e="T260" id="Seg_2429" n="e" s="T259">Timnʼäːqımtɨ </ts>
               <ts e="T261" id="Seg_2431" n="e" s="T260">əːtɨrkɨqolamnɨtɨ. </ts>
               <ts e="T262" id="Seg_2433" n="e" s="T261">Ponä </ts>
               <ts e="T263" id="Seg_2435" n="e" s="T262">na </ts>
               <ts e="T264" id="Seg_2437" n="e" s="T263">čʼap </ts>
               <ts e="T265" id="Seg_2439" n="e" s="T264">tannɨntɔːtet. </ts>
               <ts e="T266" id="Seg_2441" n="e" s="T265">Montɨ </ts>
               <ts e="T267" id="Seg_2443" n="e" s="T266">ɨntɨlʼ </ts>
               <ts e="T268" id="Seg_2445" n="e" s="T267">tıšap </ts>
               <ts e="T269" id="Seg_2447" n="e" s="T268">ɨnnä </ts>
               <ts e="T270" id="Seg_2449" n="e" s="T269">iːqɨlpɔːtet. </ts>
               <ts e="T271" id="Seg_2451" n="e" s="T270">Ütqɨnɨ </ts>
               <ts e="T272" id="Seg_2453" n="e" s="T271">konnä </ts>
               <ts e="T273" id="Seg_2455" n="e" s="T272">toqalʼ </ts>
               <ts e="T274" id="Seg_2457" n="e" s="T273">čʼüntɔːtɨt. </ts>
               <ts e="T275" id="Seg_2459" n="e" s="T274">Ponä </ts>
               <ts e="T276" id="Seg_2461" n="e" s="T275">tantɨlä </ts>
               <ts e="T277" id="Seg_2463" n="e" s="T276">nık </ts>
               <ts e="T278" id="Seg_2465" n="e" s="T277">kətɨŋɨt. </ts>
               <ts e="T279" id="Seg_2467" n="e" s="T278">Konnä </ts>
               <ts e="T280" id="Seg_2469" n="e" s="T279">tantɨlä </ts>
               <ts e="T281" id="Seg_2471" n="e" s="T280">amɨrŋɨlɨt, </ts>
               <ts e="T282" id="Seg_2473" n="e" s="T281">qɔːtɨnıːsʼa </ts>
               <ts e="T283" id="Seg_2475" n="e" s="T282">müttɨlɨmɨt </ts>
               <ts e="T284" id="Seg_2477" n="e" s="T283">tentɨlä </ts>
               <ts e="T285" id="Seg_2479" n="e" s="T284">poːqɨ </ts>
               <ts e="T286" id="Seg_2481" n="e" s="T285">ütqɨnɨ </ts>
               <ts e="T287" id="Seg_2483" n="e" s="T286">konnä. </ts>
               <ts e="T288" id="Seg_2485" n="e" s="T287">Aj </ts>
               <ts e="T289" id="Seg_2487" n="e" s="T288">qaj </ts>
               <ts e="T290" id="Seg_2489" n="e" s="T289">meːqolamnɨlɨt? </ts>
               <ts e="T291" id="Seg_2491" n="e" s="T290">Konnä </ts>
               <ts e="T292" id="Seg_2493" n="e" s="T291">tantɨlä </ts>
               <ts e="T293" id="Seg_2495" n="e" s="T292">amɨrŋɨlɨt. </ts>
               <ts e="T294" id="Seg_2497" n="e" s="T293">A </ts>
               <ts e="T295" id="Seg_2499" n="e" s="T294">nıŋ </ts>
               <ts e="T296" id="Seg_2501" n="e" s="T295">ɛːsä </ts>
               <ts e="T297" id="Seg_2503" n="e" s="T296">märqɨ </ts>
               <ts e="T298" id="Seg_2505" n="e" s="T297">timnʼatɨ. </ts>
               <ts e="T299" id="Seg_2507" n="e" s="T298">Konnä </ts>
               <ts e="T300" id="Seg_2509" n="e" s="T299">tantɨlä </ts>
               <ts e="T301" id="Seg_2511" n="e" s="T300">amɨrlɨmɨt. </ts>
               <ts e="T302" id="Seg_2513" n="e" s="T301">Konna </ts>
               <ts e="T303" id="Seg_2515" n="e" s="T302">jep </ts>
               <ts e="T304" id="Seg_2517" n="e" s="T303">tattɔːtet. </ts>
               <ts e="T305" id="Seg_2519" n="e" s="T304">Ɨntɨlʼ </ts>
               <ts e="T306" id="Seg_2521" n="e" s="T305">tıšamtɨt </ts>
               <ts e="T307" id="Seg_2523" n="e" s="T306">karrät </ts>
               <ts e="T308" id="Seg_2525" n="e" s="T307">antoːqɨntɨt </ts>
               <ts e="T309" id="Seg_2527" n="e" s="T308">pillä </ts>
               <ts e="T310" id="Seg_2529" n="e" s="T309">qəːčʼalnɔːtɨt. </ts>
               <ts e="T311" id="Seg_2531" n="e" s="T310">Mɔːttɨ </ts>
               <ts e="T312" id="Seg_2533" n="e" s="T311">na </ts>
               <ts e="T313" id="Seg_2535" n="e" s="T312">jep </ts>
               <ts e="T314" id="Seg_2537" n="e" s="T313">šeːrtɔːtɨt. </ts>
               <ts e="T315" id="Seg_2539" n="e" s="T314">Mərqɨ </ts>
               <ts e="T316" id="Seg_2541" n="e" s="T315">laŋalʼ </ts>
               <ts e="T317" id="Seg_2543" n="e" s="T316">qumɨlʼ </ts>
               <ts e="T318" id="Seg_2545" n="e" s="T317">mɔːter </ts>
               <ts e="T319" id="Seg_2547" n="e" s="T318">Palnan </ts>
               <ts e="T320" id="Seg_2549" n="e" s="T319">optı </ts>
               <ts e="T321" id="Seg_2551" n="e" s="T320">ukkɨr </ts>
               <ts e="T322" id="Seg_2553" n="e" s="T321">qəntɨ </ts>
               <ts e="T323" id="Seg_2555" n="e" s="T322">na </ts>
               <ts e="T324" id="Seg_2557" n="e" s="T323">omnɨntɔːtet. </ts>
               <ts e="T325" id="Seg_2559" n="e" s="T324">Takɨlʼ </ts>
               <ts e="T326" id="Seg_2561" n="e" s="T325">ɔːtälʼ </ts>
               <ts e="T327" id="Seg_2563" n="e" s="T326">nʼoːtɨlʼ </ts>
               <ts e="T328" id="Seg_2565" n="e" s="T327">timnʼantɨsä </ts>
               <ts e="T329" id="Seg_2567" n="e" s="T328">aj </ts>
               <ts e="T330" id="Seg_2569" n="e" s="T329">ukkɨr </ts>
               <ts e="T331" id="Seg_2571" n="e" s="T330">qəntɨ </ts>
               <ts e="T332" id="Seg_2573" n="e" s="T331">na </ts>
               <ts e="T333" id="Seg_2575" n="e" s="T332">omnɨntɔːtɨt. </ts>
               <ts e="T334" id="Seg_2577" n="e" s="T333">Nɔːkɨr </ts>
               <ts e="T335" id="Seg_2579" n="e" s="T334">timnʼaset </ts>
               <ts e="T336" id="Seg_2581" n="e" s="T335">muntɨk </ts>
               <ts e="T337" id="Seg_2583" n="e" s="T336">ukkɨr </ts>
               <ts e="T338" id="Seg_2585" n="e" s="T337">qönte </ts>
               <ts e="T339" id="Seg_2587" n="e" s="T338">omtelɔːtɨt. </ts>
               <ts e="T340" id="Seg_2589" n="e" s="T339">Nɨːn </ts>
               <ts e="T341" id="Seg_2591" n="e" s="T340">nık </ts>
               <ts e="T342" id="Seg_2593" n="e" s="T341">kətɨsɔːtɨt: </ts>
               <ts e="T343" id="Seg_2595" n="e" s="T342">Paŋɨmɨt </ts>
               <ts e="T344" id="Seg_2597" n="e" s="T343">qaj </ts>
               <ts e="T345" id="Seg_2599" n="e" s="T344">əmɨltɨmnɨmɨt. </ts>
               <ts e="T346" id="Seg_2601" n="e" s="T345">Paŋɨlɨt </ts>
               <ts e="T347" id="Seg_2603" n="e" s="T346">ɛːŋa? </ts>
               <ts e="T348" id="Seg_2605" n="e" s="T347">Paŋɨsä </ts>
               <ts e="T349" id="Seg_2607" n="e" s="T348">šınet </ts>
               <ts e="T350" id="Seg_2609" n="e" s="T349">miŋɨlɨt. </ts>
               <ts e="T351" id="Seg_2611" n="e" s="T350">Amɨrqo </ts>
               <ts e="T352" id="Seg_2613" n="e" s="T351">solap </ts>
               <ts e="T353" id="Seg_2615" n="e" s="T352">meːqɨllɨmɨt. </ts>
               <ts e="T354" id="Seg_2617" n="e" s="T353">Paŋɨmtɨt </ts>
               <ts e="T355" id="Seg_2619" n="e" s="T354">muntɨk </ts>
               <ts e="T356" id="Seg_2621" n="e" s="T355">miŋɔːtɨt </ts>
               <ts e="T357" id="Seg_2623" n="e" s="T356">laŋalʼ </ts>
               <ts e="T358" id="Seg_2625" n="e" s="T357">qumɨlʼ </ts>
               <ts e="T359" id="Seg_2627" n="e" s="T358">nɔːkɨr </ts>
               <ts e="T360" id="Seg_2629" n="e" s="T359">timnʼasɨt. </ts>
               <ts e="T361" id="Seg_2631" n="e" s="T360">A </ts>
               <ts e="T362" id="Seg_2633" n="e" s="T361">Palnalʼ </ts>
               <ts e="T363" id="Seg_2635" n="e" s="T362">nɔːkɨr </ts>
               <ts e="T364" id="Seg_2637" n="e" s="T363">timnʼasɨt </ts>
               <ts e="T365" id="Seg_2639" n="e" s="T364">okoːt </ts>
               <ts e="T366" id="Seg_2641" n="e" s="T365">jap </ts>
               <ts e="T367" id="Seg_2643" n="e" s="T366">əːtɨrkɨsɔːtɨt. </ts>
               <ts e="T368" id="Seg_2645" n="e" s="T367">Märqɨ </ts>
               <ts e="T369" id="Seg_2647" n="e" s="T368">timnʼatɨ </ts>
               <ts e="T370" id="Seg_2649" n="e" s="T369">kutar </ts>
               <ts e="T371" id="Seg_2651" n="e" s="T370">paŋɨsä </ts>
               <ts e="T372" id="Seg_2653" n="e" s="T371">qättɨntɨtɨ. </ts>
               <ts e="T373" id="Seg_2655" n="e" s="T372">Təpɨt </ts>
               <ts e="T374" id="Seg_2657" n="e" s="T373">aj </ts>
               <ts e="T375" id="Seg_2659" n="e" s="T374">nılʼčʼɨk </ts>
               <ts e="T376" id="Seg_2661" n="e" s="T375">qättɨntɔːtɨt. </ts>
               <ts e="T377" id="Seg_2663" n="e" s="T376">Märqɨ </ts>
               <ts e="T378" id="Seg_2665" n="e" s="T377">ira </ts>
               <ts e="T379" id="Seg_2667" n="e" s="T378">ukkur </ts>
               <ts e="T380" id="Seg_2669" n="e" s="T379">čʼontoːqɨt </ts>
               <ts e="T381" id="Seg_2671" n="e" s="T380">nanɨ </ts>
               <ts e="T382" id="Seg_2673" n="e" s="T381">matta, </ts>
               <ts e="T383" id="Seg_2675" n="e" s="T382">sajesa </ts>
               <ts e="T384" id="Seg_2677" n="e" s="T383">ijap </ts>
               <ts e="T385" id="Seg_2679" n="e" s="T384">mɨrɔːtɨt. </ts>
               <ts e="T386" id="Seg_2681" n="e" s="T385">Paŋɨsä </ts>
               <ts e="T387" id="Seg_2683" n="e" s="T386">nɨː </ts>
               <ts e="T388" id="Seg_2685" n="e" s="T387">jep </ts>
               <ts e="T389" id="Seg_2687" n="e" s="T388">qättɔːtɨt </ts>
               <ts e="T390" id="Seg_2689" n="e" s="T389">laŋalʼ </ts>
               <ts e="T391" id="Seg_2691" n="e" s="T390">qumɨlʼ </ts>
               <ts e="T392" id="Seg_2693" n="e" s="T391">nɔːr </ts>
               <ts e="T393" id="Seg_2695" n="e" s="T392">timnʼästɨp. </ts>
               <ts e="T394" id="Seg_2697" n="e" s="T393">Taŋɨlʼ </ts>
               <ts e="T395" id="Seg_2699" n="e" s="T394">ɔːtälʼ </ts>
               <ts e="T396" id="Seg_2701" n="e" s="T395">nʼoːtɨlʼ </ts>
               <ts e="T397" id="Seg_2703" n="e" s="T396">timnʼämtɨ </ts>
               <ts e="T398" id="Seg_2705" n="e" s="T397">čʼap </ts>
               <ts e="T399" id="Seg_2707" n="e" s="T398">qättɨtɨ. </ts>
               <ts e="T400" id="Seg_2709" n="e" s="T399">Ola </ts>
               <ts e="T401" id="Seg_2711" n="e" s="T400">šünʼčʼipɨlʼ </ts>
               <ts e="T402" id="Seg_2713" n="e" s="T401">təttɨp </ts>
               <ts e="T403" id="Seg_2715" n="e" s="T402">qättɨtɨ. </ts>
               <ts e="T404" id="Seg_2717" n="e" s="T403">Palnat </ts>
               <ts e="T405" id="Seg_2719" n="e" s="T404">timnʼätɨ </ts>
               <ts e="T406" id="Seg_2721" n="e" s="T405">taŋɨlʼ </ts>
               <ts e="T407" id="Seg_2723" n="e" s="T406">ɔːtalʼ </ts>
               <ts e="T408" id="Seg_2725" n="e" s="T407">nʼoːtɨlʼ </ts>
               <ts e="T409" id="Seg_2727" n="e" s="T408">tɨmnʼätɨ </ts>
               <ts e="T410" id="Seg_2729" n="e" s="T409">aj </ts>
               <ts e="T411" id="Seg_2731" n="e" s="T410">nɨːnɨ </ts>
               <ts e="T412" id="Seg_2733" n="e" s="T411">pakta. </ts>
               <ts e="T413" id="Seg_2735" n="e" s="T412">Nʼoːla </ts>
               <ts e="T414" id="Seg_2737" n="e" s="T413">qəntɨtɨ, </ts>
               <ts e="T415" id="Seg_2739" n="e" s="T414">täːqamtɨ </ts>
               <ts e="T416" id="Seg_2741" n="e" s="T415">orqɨllä </ts>
               <ts e="T417" id="Seg_2743" n="e" s="T416">nʼoːtɨŋɨt. </ts>
               <ts e="T418" id="Seg_2745" n="e" s="T417">Toːnna </ts>
               <ts e="T419" id="Seg_2747" n="e" s="T418">šitɨ </ts>
               <ts e="T420" id="Seg_2749" n="e" s="T419">tɨmnʼäsɨqä </ts>
               <ts e="T421" id="Seg_2751" n="e" s="T420">mɔːtqɨt </ts>
               <ts e="T422" id="Seg_2753" n="e" s="T421">qalɔːtɨt. </ts>
               <ts e="T423" id="Seg_2755" n="e" s="T422">Qaj </ts>
               <ts e="T424" id="Seg_2757" n="e" s="T423">qənnɨtɨ, </ts>
               <ts e="T425" id="Seg_2759" n="e" s="T424">qaj </ts>
               <ts e="T426" id="Seg_2761" n="e" s="T425">qattɨjɔːtɨt. </ts>
               <ts e="T427" id="Seg_2763" n="e" s="T426">Nʼoːla </ts>
               <ts e="T428" id="Seg_2765" n="e" s="T427">qəntɨtɨtɨ. </ts>
               <ts e="T429" id="Seg_2767" n="e" s="T428">Nʼennät </ts>
               <ts e="T430" id="Seg_2769" n="e" s="T429">mitallä </ts>
               <ts e="T431" id="Seg_2771" n="e" s="T430">čʼap </ts>
               <ts e="T432" id="Seg_2773" n="e" s="T431">počʼɔːlnɨtɨ. </ts>
               <ts e="T433" id="Seg_2775" n="e" s="T432">Lʼakčʼitɨ </ts>
               <ts e="T434" id="Seg_2777" n="e" s="T433">pačʼalnɨt. </ts>
               <ts e="T435" id="Seg_2779" n="e" s="T434">Qamtä </ts>
               <ts e="T436" id="Seg_2781" n="e" s="T435">alʼčʼa </ts>
               <ts e="T437" id="Seg_2783" n="e" s="T436">laŋalʼ </ts>
               <ts e="T438" id="Seg_2785" n="e" s="T437">qup. </ts>
               <ts e="T439" id="Seg_2787" n="e" s="T438">Šittäl </ts>
               <ts e="T440" id="Seg_2789" n="e" s="T439">čʼap </ts>
               <ts e="T441" id="Seg_2791" n="e" s="T440">pačʼalnɨt </ts>
               <ts e="T442" id="Seg_2793" n="e" s="T441">täːqat </ts>
               <ts e="T443" id="Seg_2795" n="e" s="T442">kət </ts>
               <ts e="T444" id="Seg_2797" n="e" s="T443">čʼoːmɨt </ts>
               <ts e="T445" id="Seg_2799" n="e" s="T444">orqɨlnɨt. </ts>
               <ts e="T446" id="Seg_2801" n="e" s="T445">Topɨtɨ </ts>
               <ts e="T447" id="Seg_2803" n="e" s="T446">toːq </ts>
               <ts e="T448" id="Seg_2805" n="e" s="T447">säppɛːmpa. </ts>
               <ts e="T449" id="Seg_2807" n="e" s="T448">Palna </ts>
               <ts e="T450" id="Seg_2809" n="e" s="T449">toːnna </ts>
               <ts e="T451" id="Seg_2811" n="e" s="T450">timnʼäqɨmta </ts>
               <ts e="T452" id="Seg_2813" n="e" s="T451">qätpatɨ. </ts>
               <ts e="T453" id="Seg_2815" n="e" s="T452">Timnʼäntɨ </ts>
               <ts e="T454" id="Seg_2817" n="e" s="T453">mättɨt </ts>
               <ts e="T455" id="Seg_2819" n="e" s="T454">šüː </ts>
               <ts e="T456" id="Seg_2821" n="e" s="T455">nʼennɨ </ts>
               <ts e="T457" id="Seg_2823" n="e" s="T456">paktɨ. </ts>
               <ts e="T458" id="Seg_2825" n="e" s="T457">Qoŋɨtɨ </ts>
               <ts e="T459" id="Seg_2827" n="e" s="T458">mompa </ts>
               <ts e="T460" id="Seg_2829" n="e" s="T459">orqɨlʼpetɨ. </ts>
               <ts e="T461" id="Seg_2831" n="e" s="T460">Mat </ts>
               <ts e="T462" id="Seg_2833" n="e" s="T461">tülɨlak. </ts>
               <ts e="T463" id="Seg_2835" n="e" s="T462">Na </ts>
               <ts e="T464" id="Seg_2837" n="e" s="T463">küšilʼ </ts>
               <ts e="T465" id="Seg_2839" n="e" s="T464">laka </ts>
               <ts e="T466" id="Seg_2841" n="e" s="T465">aj </ts>
               <ts e="T467" id="Seg_2843" n="e" s="T466">qaj </ts>
               <ts e="T468" id="Seg_2845" n="e" s="T467">meːtɨtɨ. </ts>
               <ts e="T469" id="Seg_2847" n="e" s="T468">Mat </ts>
               <ts e="T470" id="Seg_2849" n="e" s="T469">tamɛntak. </ts>
               <ts e="T471" id="Seg_2851" n="e" s="T470">Nɨːnɨ </ts>
               <ts e="T472" id="Seg_2853" n="e" s="T471">nɔːkɨr </ts>
               <ts e="T473" id="Seg_2855" n="e" s="T472">tɨmnʼästɨp </ts>
               <ts e="T474" id="Seg_2857" n="e" s="T473">muntɨk </ts>
               <ts e="T475" id="Seg_2859" n="e" s="T474">qəttɛːjɔːtɨt. </ts>
               <ts e="T476" id="Seg_2861" n="e" s="T475">Okkɨr </ts>
               <ts e="T477" id="Seg_2863" n="e" s="T476">taːt </ts>
               <ts e="T478" id="Seg_2865" n="e" s="T477">tättɨt </ts>
               <ts e="T479" id="Seg_2867" n="e" s="T478">čʼontoːqɨt </ts>
               <ts e="T480" id="Seg_2869" n="e" s="T479">nanɨ </ts>
               <ts e="T481" id="Seg_2871" n="e" s="T480">ilɨmpɔːtɨt, </ts>
               <ts e="T482" id="Seg_2873" n="e" s="T481">nʼi </ts>
               <ts e="T483" id="Seg_2875" n="e" s="T482">qajit </ts>
               <ts e="T484" id="Seg_2877" n="e" s="T483">sümɨ </ts>
               <ts e="T485" id="Seg_2879" n="e" s="T484">čʼäːŋka. </ts>
               <ts e="T486" id="Seg_2881" n="e" s="T485">Qənŋɔːtɨt </ts>
               <ts e="T487" id="Seg_2883" n="e" s="T486">laŋalʼ </ts>
               <ts e="T488" id="Seg_2885" n="e" s="T487">qumɨlʼ. </ts>
               <ts e="T489" id="Seg_2887" n="e" s="T488">Imaqotalʼ </ts>
               <ts e="T490" id="Seg_2889" n="e" s="T489">nılʼčʼik </ts>
               <ts e="T491" id="Seg_2891" n="e" s="T490">tɛnɨrna. </ts>
               <ts e="T492" id="Seg_2893" n="e" s="T491">Ijaiːmɨ </ts>
               <ts e="T493" id="Seg_2895" n="e" s="T492">qəssɔːtɨt </ts>
               <ts e="T494" id="Seg_2897" n="e" s="T493">meːltɨt </ts>
               <ts e="T495" id="Seg_2899" n="e" s="T494">qoja </ts>
               <ts e="T496" id="Seg_2901" n="e" s="T495">mɔːtɨŋnɔːtɨt. </ts>
               <ts e="T497" id="Seg_2903" n="e" s="T496">Meːlle </ts>
               <ts e="T498" id="Seg_2905" n="e" s="T497">mɨta </ts>
               <ts e="T499" id="Seg_2907" n="e" s="T498">šölʼqup </ts>
               <ts e="T500" id="Seg_2909" n="e" s="T499">ijamtɨ </ts>
               <ts e="T501" id="Seg_2911" n="e" s="T500">namɨt </ts>
               <ts e="T502" id="Seg_2913" n="e" s="T501">mɔːntɨ </ts>
               <ts e="T503" id="Seg_2915" n="e" s="T502">somak </ts>
               <ts e="T504" id="Seg_2917" n="e" s="T503">kətɨmpatɨ. </ts>
               <ts e="T505" id="Seg_2919" n="e" s="T504">Mat </ts>
               <ts e="T506" id="Seg_2921" n="e" s="T505">kʼe </ts>
               <ts e="T507" id="Seg_2923" n="e" s="T506">ijamɨ </ts>
               <ts e="T508" id="Seg_2925" n="e" s="T507">kəːtɨptäkek. </ts>
               <ts e="T509" id="Seg_2927" n="e" s="T508">Nümanɨ </ts>
               <ts e="T510" id="Seg_2929" n="e" s="T509">ukɨp </ts>
               <ts e="T511" id="Seg_2931" n="e" s="T510">aj </ts>
               <ts e="T512" id="Seg_2933" n="e" s="T511">musɨltɨkap. </ts>
               <ts e="T513" id="Seg_2935" n="e" s="T512">Šölʼqumɨlʼ </ts>
               <ts e="T514" id="Seg_2937" n="e" s="T513">ima </ts>
               <ts e="T515" id="Seg_2939" n="e" s="T514">meːlle </ts>
               <ts e="T516" id="Seg_2941" n="e" s="T515">qəː </ts>
               <ts e="T517" id="Seg_2943" n="e" s="T516">ɛːŋa </ts>
               <ts e="T518" id="Seg_2945" n="e" s="T517">mannoːnɨ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1" id="Seg_2946" s="T0">SAI_1965_Palna_flk.001 (001)</ta>
            <ta e="T5" id="Seg_2947" s="T1">SAI_1965_Palna_flk.002 (002.001)</ta>
            <ta e="T9" id="Seg_2948" s="T5">SAI_1965_Palna_flk.003 (002.002)</ta>
            <ta e="T14" id="Seg_2949" s="T9">SAI_1965_Palna_flk.004 (002.003)</ta>
            <ta e="T20" id="Seg_2950" s="T14">SAI_1965_Palna_flk.005 (002.004)</ta>
            <ta e="T27" id="Seg_2951" s="T20">SAI_1965_Palna_flk.006 (002.005)</ta>
            <ta e="T29" id="Seg_2952" s="T27">SAI_1965_Palna_flk.007 (002.006)</ta>
            <ta e="T37" id="Seg_2953" s="T29">SAI_1965_Palna_flk.008 (002.007)</ta>
            <ta e="T42" id="Seg_2954" s="T37">SAI_1965_Palna_flk.009 (002.008)</ta>
            <ta e="T46" id="Seg_2955" s="T42">SAI_1965_Palna_flk.010 (002.009)</ta>
            <ta e="T50" id="Seg_2956" s="T46">SAI_1965_Palna_flk.011 (002.010)</ta>
            <ta e="T56" id="Seg_2957" s="T50">SAI_1965_Palna_flk.012 (002.011)</ta>
            <ta e="T64" id="Seg_2958" s="T56">SAI_1965_Palna_flk.013 (002.012)</ta>
            <ta e="T69" id="Seg_2959" s="T64">SAI_1965_Palna_flk.014 (002.013)</ta>
            <ta e="T75" id="Seg_2960" s="T69">SAI_1965_Palna_flk.015 (002.014)</ta>
            <ta e="T83" id="Seg_2961" s="T75">SAI_1965_Palna_flk.016 (002.015)</ta>
            <ta e="T86" id="Seg_2962" s="T83">SAI_1965_Palna_flk.017 (002.016)</ta>
            <ta e="T90" id="Seg_2963" s="T86">SAI_1965_Palna_flk.018 (002.017)</ta>
            <ta e="T93" id="Seg_2964" s="T90">SAI_1965_Palna_flk.019 (002.018)</ta>
            <ta e="T100" id="Seg_2965" s="T93">SAI_1965_Palna_flk.020 (002.019)</ta>
            <ta e="T103" id="Seg_2966" s="T100">SAI_1965_Palna_flk.021 (002.020)</ta>
            <ta e="T110" id="Seg_2967" s="T103">SAI_1965_Palna_flk.022 (002.021)</ta>
            <ta e="T117" id="Seg_2968" s="T110">SAI_1965_Palna_flk.023 (002.022)</ta>
            <ta e="T126" id="Seg_2969" s="T117">SAI_1965_Palna_flk.024 (002.023)</ta>
            <ta e="T135" id="Seg_2970" s="T126">SAI_1965_Palna_flk.025 (003.001)</ta>
            <ta e="T140" id="Seg_2971" s="T135">SAI_1965_Palna_flk.026 (003.002)</ta>
            <ta e="T144" id="Seg_2972" s="T140">SAI_1965_Palna_flk.027 (003.003)</ta>
            <ta e="T147" id="Seg_2973" s="T144">SAI_1965_Palna_flk.028 (003.004)</ta>
            <ta e="T155" id="Seg_2974" s="T147">SAI_1965_Palna_flk.029 (003.005)</ta>
            <ta e="T158" id="Seg_2975" s="T155">SAI_1965_Palna_flk.030 (003.006)</ta>
            <ta e="T160" id="Seg_2976" s="T158">SAI_1965_Palna_flk.031 (003.007)</ta>
            <ta e="T162" id="Seg_2977" s="T160">SAI_1965_Palna_flk.032 (003.008)</ta>
            <ta e="T166" id="Seg_2978" s="T162">SAI_1965_Palna_flk.033 (003.009)</ta>
            <ta e="T167" id="Seg_2979" s="T166">SAI_1965_Palna_flk.034 (003.010)</ta>
            <ta e="T170" id="Seg_2980" s="T167">SAI_1965_Palna_flk.035 (003.011)</ta>
            <ta e="T172" id="Seg_2981" s="T170">SAI_1965_Palna_flk.036 (003.012)</ta>
            <ta e="T177" id="Seg_2982" s="T172">SAI_1965_Palna_flk.037 (003.013)</ta>
            <ta e="T179" id="Seg_2983" s="T177">SAI_1965_Palna_flk.038 (003.014)</ta>
            <ta e="T184" id="Seg_2984" s="T179">SAI_1965_Palna_flk.039 (003.015)</ta>
            <ta e="T188" id="Seg_2985" s="T184">SAI_1965_Palna_flk.040 (003.016)</ta>
            <ta e="T190" id="Seg_2986" s="T188">SAI_1965_Palna_flk.041 (003.017)</ta>
            <ta e="T194" id="Seg_2987" s="T190">SAI_1965_Palna_flk.042 (003.018)</ta>
            <ta e="T198" id="Seg_2988" s="T194">SAI_1965_Palna_flk.043 (003.019)</ta>
            <ta e="T201" id="Seg_2989" s="T198">SAI_1965_Palna_flk.044 (003.020)</ta>
            <ta e="T206" id="Seg_2990" s="T201">SAI_1965_Palna_flk.045 (003.021)</ta>
            <ta e="T209" id="Seg_2991" s="T206">SAI_1965_Palna_flk.046 (003.022)</ta>
            <ta e="T214" id="Seg_2992" s="T209">SAI_1965_Palna_flk.047 (003.023)</ta>
            <ta e="T217" id="Seg_2993" s="T214">SAI_1965_Palna_flk.048 (003.024)</ta>
            <ta e="T221" id="Seg_2994" s="T217">SAI_1965_Palna_flk.049 (003.025)</ta>
            <ta e="T225" id="Seg_2995" s="T221">SAI_1965_Palna_flk.050 (003.026)</ta>
            <ta e="T228" id="Seg_2996" s="T225">SAI_1965_Palna_flk.051 (003.027)</ta>
            <ta e="T236" id="Seg_2997" s="T228">SAI_1965_Palna_flk.052 (003.028)</ta>
            <ta e="T238" id="Seg_2998" s="T236">SAI_1965_Palna_flk.053 (003.029)</ta>
            <ta e="T242" id="Seg_2999" s="T238">SAI_1965_Palna_flk.054 (003.030)</ta>
            <ta e="T246" id="Seg_3000" s="T242">SAI_1965_Palna_flk.055 (003.031)</ta>
            <ta e="T251" id="Seg_3001" s="T246">SAI_1965_Palna_flk.056 (003.032)</ta>
            <ta e="T259" id="Seg_3002" s="T251">SAI_1965_Palna_flk.057 (003.033)</ta>
            <ta e="T261" id="Seg_3003" s="T259">SAI_1965_Palna_flk.058 (003.034)</ta>
            <ta e="T265" id="Seg_3004" s="T261">SAI_1965_Palna_flk.059 (003.035)</ta>
            <ta e="T270" id="Seg_3005" s="T265">SAI_1965_Palna_flk.060 (003.036)</ta>
            <ta e="T274" id="Seg_3006" s="T270">SAI_1965_Palna_flk.061 (003.037)</ta>
            <ta e="T278" id="Seg_3007" s="T274">SAI_1965_Palna_flk.062 (003.038)</ta>
            <ta e="T287" id="Seg_3008" s="T278">SAI_1965_Palna_flk.063 (003.039)</ta>
            <ta e="T290" id="Seg_3009" s="T287">SAI_1965_Palna_flk.064 (003.040)</ta>
            <ta e="T293" id="Seg_3010" s="T290">SAI_1965_Palna_flk.065 (003.041)</ta>
            <ta e="T298" id="Seg_3011" s="T293">SAI_1965_Palna_flk.066 (003.042)</ta>
            <ta e="T301" id="Seg_3012" s="T298">SAI_1965_Palna_flk.067 (003.043)</ta>
            <ta e="T304" id="Seg_3013" s="T301">SAI_1965_Palna_flk.068 (003.044)</ta>
            <ta e="T310" id="Seg_3014" s="T304">SAI_1965_Palna_flk.069 (003.045)</ta>
            <ta e="T314" id="Seg_3015" s="T310">SAI_1965_Palna_flk.070 (003.046)</ta>
            <ta e="T324" id="Seg_3016" s="T314">SAI_1965_Palna_flk.071 (003.047)</ta>
            <ta e="T333" id="Seg_3017" s="T324">SAI_1965_Palna_flk.072 (003.048)</ta>
            <ta e="T339" id="Seg_3018" s="T333">SAI_1965_Palna_flk.073 (003.049)</ta>
            <ta e="T345" id="Seg_3019" s="T339">SAI_1965_Palna_flk.074 (003.050)</ta>
            <ta e="T347" id="Seg_3020" s="T345">SAI_1965_Palna_flk.075 (003.051)</ta>
            <ta e="T350" id="Seg_3021" s="T347">SAI_1965_Palna_flk.076 (003.052)</ta>
            <ta e="T353" id="Seg_3022" s="T350">SAI_1965_Palna_flk.077 (003.053)</ta>
            <ta e="T360" id="Seg_3023" s="T353">SAI_1965_Palna_flk.078 (003.054)</ta>
            <ta e="T367" id="Seg_3024" s="T360">SAI_1965_Palna_flk.079 (003.055)</ta>
            <ta e="T372" id="Seg_3025" s="T367">SAI_1965_Palna_flk.080 (003.056)</ta>
            <ta e="T376" id="Seg_3026" s="T372">SAI_1965_Palna_flk.081 (003.057)</ta>
            <ta e="T385" id="Seg_3027" s="T376">SAI_1965_Palna_flk.082 (003.058)</ta>
            <ta e="T393" id="Seg_3028" s="T385">SAI_1965_Palna_flk.083 (003.059)</ta>
            <ta e="T399" id="Seg_3029" s="T393">SAI_1965_Palna_flk.084 (003.060)</ta>
            <ta e="T403" id="Seg_3030" s="T399">SAI_1965_Palna_flk.085 (003.061)</ta>
            <ta e="T412" id="Seg_3031" s="T403">SAI_1965_Palna_flk.086 (003.062)</ta>
            <ta e="T417" id="Seg_3032" s="T412">SAI_1965_Palna_flk.087 (003.063)</ta>
            <ta e="T422" id="Seg_3033" s="T417">SAI_1965_Palna_flk.088 (003.064)</ta>
            <ta e="T426" id="Seg_3034" s="T422">SAI_1965_Palna_flk.089 (003.065)</ta>
            <ta e="T428" id="Seg_3035" s="T426">SAI_1965_Palna_flk.090 (003.066)</ta>
            <ta e="T432" id="Seg_3036" s="T428">SAI_1965_Palna_flk.091 (003.067)</ta>
            <ta e="T434" id="Seg_3037" s="T432">SAI_1965_Palna_flk.092 (003.068)</ta>
            <ta e="T438" id="Seg_3038" s="T434">SAI_1965_Palna_flk.093 (003.069)</ta>
            <ta e="T445" id="Seg_3039" s="T438">SAI_1965_Palna_flk.094 (003.070)</ta>
            <ta e="T448" id="Seg_3040" s="T445">SAI_1965_Palna_flk.095 (003.071)</ta>
            <ta e="T452" id="Seg_3041" s="T448">SAI_1965_Palna_flk.096 (003.072)</ta>
            <ta e="T457" id="Seg_3042" s="T452">SAI_1965_Palna_flk.097 (003.073)</ta>
            <ta e="T460" id="Seg_3043" s="T457">SAI_1965_Palna_flk.098 (003.074)</ta>
            <ta e="T462" id="Seg_3044" s="T460">SAI_1965_Palna_flk.099 (003.075)</ta>
            <ta e="T468" id="Seg_3045" s="T462">SAI_1965_Palna_flk.100 (003.076)</ta>
            <ta e="T470" id="Seg_3046" s="T468">SAI_1965_Palna_flk.101 (003.077)</ta>
            <ta e="T475" id="Seg_3047" s="T470">SAI_1965_Palna_flk.102 (003.078)</ta>
            <ta e="T485" id="Seg_3048" s="T475">SAI_1965_Palna_flk.103 (003.079)</ta>
            <ta e="T488" id="Seg_3049" s="T485">SAI_1965_Palna_flk.104 (003.080)</ta>
            <ta e="T491" id="Seg_3050" s="T488">SAI_1965_Palna_flk.105 (004.001)</ta>
            <ta e="T496" id="Seg_3051" s="T491">SAI_1965_Palna_flk.106 (004.002)</ta>
            <ta e="T504" id="Seg_3052" s="T496">SAI_1965_Palna_flk.107 (004.003)</ta>
            <ta e="T508" id="Seg_3053" s="T504">SAI_1965_Palna_flk.108 (004.004)</ta>
            <ta e="T512" id="Seg_3054" s="T508">SAI_1965_Palna_flk.109 (004.005)</ta>
            <ta e="T518" id="Seg_3055" s="T512">SAI_1965_Palna_flk.110 (004.006)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T1" id="Seg_3056" s="T0">паlна.</ta>
            <ta e="T5" id="Seg_3057" s="T1">′паlна нокыр тым′нʼасʼык илʼимы′нтотыт.</ta>
            <ta e="T9" id="Seg_3058" s="T5">шʼӧ̄l′kумыl мотырыl ′ӣрра ′еппымынта.</ta>
            <ta e="T14" id="Seg_3059" s="T9">′орсымыl ′ирра самый мӓрkы тим′нʼӓтыт.</ta>
            <ta e="T20" id="Seg_3060" s="T14">чон′д̂̂оkыl тым′нʼӓтыт таңыl ′о̨тӓl ′нʼӧ̄тый ′е̨[ɛ]ппымынта.</ta>
            <ta e="T27" id="Seg_3061" s="T20">′по̄с кыпа тымнʼӓты ′кɛ[ə]рнʼат ′по̄раl ′нʼӧ̄тый ′е̨[ɛ]ппымынта.</ta>
            <ta e="T29" id="Seg_3062" s="T27">′ниlчик ′иlимпотыт.</ta>
            <ta e="T37" id="Seg_3063" s="T29">а на ′паlнаl ′мӓ[а̊]рkы ты[и]м′нʼӓты ′намышʼак ′kъ̊̄тысӓ ′ɛ̄ппымынта.</ta>
            <ta e="T42" id="Seg_3064" s="T37">куна kай ′тӱнта, мунтык ′тӓнынымпаты.</ta>
            <ta e="T46" id="Seg_3065" s="T42">′сумпытыl ′тъ̊̄тыпыl ′ир[р]а ɛ̄ппымынта.</ta>
            <ta e="T50" id="Seg_3066" s="T46">′kъ̊̄сып ′ме̄тымпаты онты ‵илʼип′тӓkынты.</ta>
            <ta e="T56" id="Seg_3067" s="T50">′чо̄тырна ′kъ̊̄сып нӣк ′ме̄тымпаты ′чаңак пӱ̄н′нон.</ta>
            <ta e="T64" id="Seg_3068" s="T56">а ′рӯш[ж̂]ит ′ме̄тымпотыт ашʼа ′тӓннымоп ′kаjен‵ноны… </ta>
            <ta e="T69" id="Seg_3069" s="T64">ты[ӓ]п ′ме̄тымпаты ′kъ̊̄сытkо ′пӱ̄п ′ме̄тымпаты.</ta>
            <ta e="T75" id="Seg_3070" s="T69">на ‵пӱн′ноны ′kъ̊сылʼ ′порkып ′ме̄тымпаты.</ta>
            <ta e="T83" id="Seg_3071" s="T75">сӓk ′kъ̊̄сыl ′ынты ′ме̄тымпат, сӓk kъ̊̄сыl ко̄′мап ′ме̄тымынтыты.</ta>
            <ta e="T86" id="Seg_3072" s="T83">′нандысӓ ай ′иlымпа.</ta>
            <ta e="T90" id="Seg_3073" s="T86">а ′мӱттылʼе аша ′ӣчирпа.</ta>
            <ta e="T93" id="Seg_3074" s="T90">′тӓ[а̊]пып′кине ′мӱт[т]ылʼе ′ӣчирпатыт.</ta>
            <ta e="T100" id="Seg_3075" s="T93">′кепки′соlʼа куны чап ′тӱнтотыт тəп ′мун‵тӓнимыт.</ta>
            <ta e="T103" id="Seg_3076" s="T100">ке̄′lыт ′ӣчирпотыт ′мӱттыkо.</ta>
            <ta e="T110" id="Seg_3077" s="T103">′lаңаl kумыт пе′лʼаkыт ай но̄р [ногыр] тим′нʼӓсыт ′е̨п[п]ынтотыт.</ta>
            <ta e="T117" id="Seg_3078" s="T110">ай ′пелʼе ′ӣчирпотыт тапшʼ[ч]елʼи kумыl ′но̨̄р тым′нʼастып.</ta>
            <ta e="T126" id="Seg_3079" s="T117">тӓм[п] ′иlымпа о′кот ′пуlчос kоl′доkыт туру′ханска kъ̊′рреl пе′лʼаkыт чӯ′роkыт.</ta>
            <ta e="T135" id="Seg_3080" s="T126">′lаңаl kумыl но̨̄kыр тим′нʼӓсыт ′уккыр ′тӓт чон′тоkыт ′ниlчик ӓ̄′сотыт.</ta>
            <ta e="T140" id="Seg_3081" s="T135">′паlнаl но̨̄р [но̨гыр] тымнʼасытып ′пе̄лʼа ′kъ̊нкоlам′нотыт.</ta>
            <ta e="T144" id="Seg_3082" s="T140">а ′паlнаl тим′нʼӓсыт kъ̊̄′ңотыт.</ta>
            <ta e="T147" id="Seg_3083" s="T144">тым′нʼӓ′иkынты нилʼчик ′kъ̊̄тыңыт.</ta>
            <ta e="T155" id="Seg_3084" s="T147">lаңаl kумыl но̨̄р тым′нʼӓсыт на ′тӱ̄kо′lам‵тотыт ′мӱттеlе ′ме̄kыныт.</ta>
            <ta e="T158" id="Seg_3085" s="T155">kа′рра ′чуронты мо̄̄тыглымыт.</ta>
            <ta e="T160" id="Seg_3086" s="T158">чӯронты ′мотың′нʼентымыт.</ta>
            <ta e="T162" id="Seg_3087" s="T160">′апсып kъ̊н′нӓнтымыт.</ta>
            <ta e="T166" id="Seg_3088" s="T162">kумыт кости [моттыла] тӱн′то̄тыт.</ta>
            <ta e="T167" id="Seg_3089" s="T166">‵амыртен′тотыт.</ta>
            <ta e="T170" id="Seg_3090" s="T167">kа′рра ′kъ̊ннотыт, ′чӯронты.</ta>
            <ta e="T172" id="Seg_3091" s="T170">‵kӓр′то̄тыт ′мотыңнотыт.</ta>
            <ta e="T177" id="Seg_3092" s="T172">′нʼӱтысӓ ′ток′kотыт ′мотты то̄l пʼе′лʼаkыт.</ta>
            <ta e="T179" id="Seg_3093" s="T177">′тъ̊̄са ‵ток′kотыт.</ta>
            <ta e="T184" id="Seg_3094" s="T179">ин′нӓны‵лʼлʼ[ll]е ай нʼӱтысӓ ток′kотыт.</ta>
            <ta e="T188" id="Seg_3095" s="T184">тым′нʼӓkынты‵кине ′каша [авса] ′мушырыңыlыт.</ta>
            <ta e="T190" id="Seg_3096" s="T188">тап′че̄лы тӱ̄н′тотыт.</ta>
            <ta e="T194" id="Seg_3097" s="T190">′ӱрыl пир′нʼӓп ′мо̄тты ′туlдыңы‵lыт.</ta>
            <ta e="T198" id="Seg_3098" s="T194">кашап [авсап] kӓ̄′рре ′муши′ротыт.</ta>
            <ta e="T201" id="Seg_3099" s="T198">нанӓ на ′о̂м′тотыт.</ta>
            <ta e="T206" id="Seg_3100" s="T201">[′че̄лʼыты] ′че̄лʼ ′чонтыlко′тӓнты, ′kорр[лл]ы[и]‵монны.</ta>
            <ta e="T209" id="Seg_3101" s="T206">нʼ[j]ӓ[а]н′нӓнты ан ′тӣры ′kонты ′шʼеlча.</ta>
            <ta e="T214" id="Seg_3102" s="T209">′ногыр ′kумыт ′типыl ′анты ′kонты′шʼента.</ta>
            <ta e="T217" id="Seg_3103" s="T214">тим′нʼа ′ӣkынты ′ниlчик ′kъ̊̊тыңыт.</ta>
            <ta e="T221" id="Seg_3104" s="T217">′паңыl′тоглʼет ′иllа ӓ′ттоlныlыт.</ta>
            <ta e="T225" id="Seg_3105" s="T221">пе̄мыт кӱ̄тет ′щӯ ′сӓkы‵то′lеlчиңыlит.</ta>
            <ta e="T228" id="Seg_3106" s="T225">′ныны ниlчик ′kъ̊тыңыlит.</ta>
            <ta e="T236" id="Seg_3107" s="T228">′паңымыт ′чӓңка, ′паңылыт ′ме̄кыныт ′миңыlыт, ′соlап ′ме‵kы[лчи]l[l]ымыт ′амырkонытkо̄.</ta>
            <ta e="T238" id="Seg_3108" s="T236">′нӣк ′ъ̊тыркыңыты.</ta>
            <ta e="T242" id="Seg_3109" s="T238">′кыпа тим′нʼӓты сыт′кыкыт ′иllа.</ta>
            <ta e="T246" id="Seg_3110" s="T242">′тӓ̄kамты ны ′ыннӓ чо′kолныт.</ta>
            <ta e="T251" id="Seg_3111" s="T246">′ыннӓт к тӱт ′порыl по ‵сорымын′то̄тыт.</ta>
            <ta e="T259" id="Seg_3112" s="T251">нӣк ′коңотыт нʼӓ′ннӓнты ′конты ′шʼеиңа ′но̨кыр ′kумыт ′типыl ′анты.</ta>
            <ta e="T261" id="Seg_3113" s="T259">тим′нʼӓkи[ы]мты ′ъ̊̄тыркы kо′lамныты.</ta>
            <ta e="T265" id="Seg_3114" s="T261">понӓ на чап ′таннын‵то̄тет.</ta>
            <ta e="T270" id="Seg_3115" s="T265">′монты ынтылʼтищап ынне ‵иkылʼ′по̄тет.</ta>
            <ta e="T274" id="Seg_3116" s="T270">′ӱтkыны коннӓ тоkалʼ чунʼто̄тыт.</ta>
            <ta e="T278" id="Seg_3117" s="T274">понӓ тантылʼлʼе нӣк ′kə[ъ̊]тыңыт.</ta>
            <ta e="T287" id="Seg_3118" s="T278">конӓ танд̂ылʼӓ а′мырныlыт, kо̄тынʼи‵ся мю[ӱ]ттылымыт тентылʼӓпо̄kы ӱтkыны ко′ннӓ.</ta>
            <ta e="T290" id="Seg_3119" s="T287">ай kай ме̄kоlамныlыт. </ta>
            <ta e="T293" id="Seg_3120" s="T290">коннӓ тантыllӓ а′мырнылыт.</ta>
            <ta e="T298" id="Seg_3121" s="T293">а̄ ниң[l] ′е̄сӓ мӓрɣы тим′ня̄ты. </ta>
            <ta e="T301" id="Seg_3122" s="T298">коннӓ тантыllӓ а′мырлымыт.</ta>
            <ta e="T304" id="Seg_3123" s="T301">коннаjеп ‵тат′то̄атет.</ta>
            <ta e="T310" id="Seg_3124" s="T304">ынд̂ылʼ ти′щам[н]те[ы]т kар′ре̄т ан′тоkынтыт пиllӓ kъ̊̄чал‵но̄тыт.</ta>
            <ta e="T314" id="Seg_3125" s="T310">мо̄тты ′на′jеп ′шʼе̄р‵то̄тыт.</ta>
            <ta e="T324" id="Seg_3126" s="T314">мӓрkы lаңалʼ kуммылʼ мо̄тер палʼнанопти уккырkəнты на омнынто̄тет.</ta>
            <ta e="T333" id="Seg_3127" s="T324">такылʼ о̄телʼнʼӧ̄тылʼ тимнʼянтысӓ ай уккыр kə̄нты на омне[ы]нто̄те[ы]т.</ta>
            <ta e="T339" id="Seg_3128" s="T333">ноɣыр тим′нʼясет мунтык уккыр kӧ̄нте омтело̄тыт.</ta>
            <ta e="T345" id="Seg_3129" s="T339">нын нӣк kы[ъ̊̄]ты′сотыт: ′паңымыт kай ′ъ̊мылʼ‵тымнымыт.</ta>
            <ta e="T347" id="Seg_3130" s="T345">па̊ңылыт еңа </ta>
            <ta e="T350" id="Seg_3131" s="T347">па̊ңысӓ шӣнет миңылыт.</ta>
            <ta e="T353" id="Seg_3132" s="T350">′амырkо ′со̄лап ме̄кыллымыт.</ta>
            <ta e="T360" id="Seg_3133" s="T353">паңымтыт мунтык мӣ′ңотыт lаңалʼ kумылʼ ‵но̨̄ɣыр тим′ня̄сыт.</ta>
            <ta e="T367" id="Seg_3134" s="T360">а палʼналʼ но̄ɣыр тим′ня̄сыт о′кот jап ъ̊тыркысо̄тыт.</ta>
            <ta e="T372" id="Seg_3135" s="T367">мы[ӓ]рkы ти′мня̄ты кутар паңысӓ kӓ′ттынтыты. </ta>
            <ta e="T376" id="Seg_3136" s="T372">тӧпыт ай нилʼчик ′kӓ′ттын‵то̄тыт.</ta>
            <ta e="T385" id="Seg_3137" s="T376">мӓрkы ира уккур чонтоkыт на̄нны матта сай[je]са иjап мы[ӓ]′ротыт.</ta>
            <ta e="T393" id="Seg_3138" s="T385">′паңысӓ ′ныjеп kӓ′ттотыт lа′ңаl ′kумыl но̨̄р тим′нʼӓстып. </ta>
            <ta e="T399" id="Seg_3139" s="T393">′таңыl ′о̨тӓlнʼӧ̄тыl тим′нʼӓмты чап ′kӓттыты.</ta>
            <ta e="T403" id="Seg_3140" s="T399"> ′оlа ′щʼунчипыl ′тəттып ′kӓттыты.</ta>
            <ta e="T412" id="Seg_3141" s="T403">′паlнат тим′нʼӓты ′таңыl ′о̨таlнʼӧ̄тыl тым′нʼӓты ай ′ныны ′паkта.</ta>
            <ta e="T417" id="Seg_3142" s="T412">′нʼӧlа ′kъ̊нтыты, ′тӓ̄kамты ′орkылʼе ′нʼӧ̄тыңыт.</ta>
            <ta e="T422" id="Seg_3143" s="T417">′тона ′шʼиты тым′нʼӓсыkӓ мо̄тkыт kа̄′lотыт.</ta>
            <ta e="T426" id="Seg_3144" s="T422">kай ′kъ̊нныты, kай ‵kатты′jотыт. </ta>
            <ta e="T428" id="Seg_3145" s="T426">нʼӧ̄lа ′kъ̊нтытыты. </ta>
            <ta e="T432" id="Seg_3146" s="T428">нʼӓ′ннӓт ′мӣтаllе чап по̄′чоlныты.</ta>
            <ta e="T434" id="Seg_3147" s="T432">lакчиты па′чаlныт.</ta>
            <ta e="T438" id="Seg_3148" s="T434">′kамте ′аlча lа′ңаl kуп.</ta>
            <ta e="T445" id="Seg_3149" s="T438">′шʼиттеl чап ′патчалныт ′тӓ̄kат kъ̊т′чомыт ′орkылныт.</ta>
            <ta e="T448" id="Seg_3150" s="T445">′топыты ′то̄k сӓ′ппемпа.</ta>
            <ta e="T452" id="Seg_3151" s="T448">′паlнта ′тона тим′нʼӓkымта ′kӓтпаты.</ta>
            <ta e="T457" id="Seg_3152" s="T452">тим′нʼӓнты ′мӓттыт щʼу ‵нʼӓнны ′паkты.</ta>
            <ta e="T460" id="Seg_3153" s="T457">′kоңыты ′момба ′орkылʼпеты.</ta>
            <ta e="T462" id="Seg_3154" s="T460">мат ′тӯllылʼлʼак.</ta>
            <ta e="T468" id="Seg_3155" s="T462">на ′кӱшил l[л]ака ай kай ′ме̄тыты.</ta>
            <ta e="T470" id="Seg_3156" s="T468">мат та̄′ментак.</ta>
            <ta e="T475" id="Seg_3157" s="T470">ныны ′ноkыр тым′нʼӓстып ′мунтык ‵kъ̊̄те′jотыт.</ta>
            <ta e="T485" id="Seg_3158" s="T475">оккыр тат ′тӓттыт чо′нтоkыт ′наны ‵иlым′по̄тыт ни ′kаjит сӱ̄мы ′чʼӓңка.</ta>
            <ta e="T488" id="Seg_3159" s="T485">′kъ̊ңно̄тыт lаңалʼ kумылʼ.</ta>
            <ta e="T491" id="Seg_3160" s="T488">има′kотаl ′ниlчик ′тӓнырна.</ta>
            <ta e="T496" id="Seg_3161" s="T491">′иjа ‵имы kъ̊′ссотыт ме̄лʼтыт ko ja‵моты′ңотыт.</ta>
            <ta e="T504" id="Seg_3162" s="T496">′меllе мы′та ′шʼӧlкуп ′иjамты ′намыт ′монты ′сомак ′kъ̊̄тым‵паты.</ta>
            <ta e="T508" id="Seg_3163" s="T504">мат кʼе и′jамы kъ̊тып ′тӓкек.</ta>
            <ta e="T512" id="Seg_3164" s="T508">нӱманы ′укып ай ′мусылты‵кап.</ta>
            <ta e="T518" id="Seg_3165" s="T512">шʼоlkумыl има ′меllе kъ̊̄ еңа ′манно̄ны.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T1" id="Seg_3166" s="T0">palʼna.</ta>
            <ta e="T5" id="Seg_3167" s="T1">palʼna nokɨr tɨmnʼasʼɨk ilʼimɨntotɨt.</ta>
            <ta e="T9" id="Seg_3168" s="T5">šöːlʼqumɨlʼ motɨrɨlʼ iːrra eppɨmɨnta.</ta>
            <ta e="T14" id="Seg_3169" s="T9">orsɨmɨlʼ irra samɨj märqɨ timnʼätɨt.</ta>
            <ta e="T20" id="Seg_3170" s="T14">čond̂̂oqɨlʼ tɨmnʼätɨt taŋɨlʼ otälʼ nʼöːtɨj e[ɛ]ppɨmɨnta.</ta>
            <ta e="T27" id="Seg_3171" s="T20">poːs kɨpa tɨmnʼätɨ kɛ[ə]rnʼat poːralʼ nʼöːtɨj e[ɛ]ppɨmɨnta.</ta>
            <ta e="T29" id="Seg_3172" s="T27">nilʼčik ilʼimpotɨt.</ta>
            <ta e="T37" id="Seg_3173" s="T29">a na palʼnalʼ mä[a]rqɨ tɨ[i]mnʼätɨ namɨšak qəːtɨsä ɛːppɨmɨnta.</ta>
            <ta e="T42" id="Seg_3174" s="T37">kuna qaj tünta, muntɨk tänɨnɨmpatɨ.</ta>
            <ta e="T46" id="Seg_3175" s="T42">sumpɨtɨlʼ təːtɨpɨlʼ ir[r]a ɛːppɨmɨnta.</ta>
            <ta e="T50" id="Seg_3176" s="T46">qəːsɨp meːtɨmpatɨ ontɨ ilʼiptäqɨntɨ.</ta>
            <ta e="T56" id="Seg_3177" s="T50">čoːtɨrna qəːsɨp niːk meːtɨmpatɨ čaŋak püːnnon.</ta>
            <ta e="T64" id="Seg_3178" s="T56">a ruːš[ž̂]it meːtɨmpotɨt aša tännɨmop qajennonɨ… </ta>
            <ta e="T69" id="Seg_3179" s="T64">tɨ[ä]p meːtɨmpatɨ qəːsɨtqo püːp meːtɨmpatɨ.</ta>
            <ta e="T75" id="Seg_3180" s="T69">na pünnonɨ qəsɨlʼ porqɨp meːtɨmpatɨ.</ta>
            <ta e="T83" id="Seg_3181" s="T75">säq qəːsɨlʼ ɨntɨ meːtɨmpat, säq qəːsɨlʼ koːmap meːtɨmɨntɨtɨ.</ta>
            <ta e="T86" id="Seg_3182" s="T83">nandɨsä aj ilʼɨmpa.</ta>
            <ta e="T90" id="Seg_3183" s="T86">a müttɨlʼe aša iːčirpa.</ta>
            <ta e="T93" id="Seg_3184" s="T90">tä[a]pɨpkine müt[t]ɨlʼe iːčirpatɨt.</ta>
            <ta e="T100" id="Seg_3185" s="T93">kepkisolʼa kunɨ čap tüntotɨt təp muntänimɨt.</ta>
            <ta e="T103" id="Seg_3186" s="T100">keːlʼɨt iːčirpotɨt müttɨqo.</ta>
            <ta e="T110" id="Seg_3187" s="T103">lʼaŋalʼ qumɨt pelʼaqɨt aj noːr [nogɨr] timnʼäsɨt ep[p]ɨntotɨt.</ta>
            <ta e="T117" id="Seg_3188" s="T110">aj pelʼe iːčirpotɨt tapš[č]elʼi qumɨlʼ noːr tɨmnʼastɨp.</ta>
            <ta e="T126" id="Seg_3189" s="T117">täm[p] ilʼɨmpa okot pulʼčos qolʼdoqɨt turuhanska qərrelʼ pelʼaqɨt čuːroqɨt.</ta>
            <ta e="T135" id="Seg_3190" s="T126">lʼaŋalʼ qumɨlʼ noːqɨr timnʼäsɨt ukkɨr tät čontoqɨt nilʼčik äːsotɨt.</ta>
            <ta e="T140" id="Seg_3191" s="T135">palʼnalʼ noːr [nogɨr] tɨmnʼasɨtɨp peːlʼa qənkolʼamnotɨt.</ta>
            <ta e="T144" id="Seg_3192" s="T140">a palʼnalʼ timnʼäsɨt qəːŋotɨt.</ta>
            <ta e="T147" id="Seg_3193" s="T144">tɨmnʼäiqɨntɨ nilʼčik qəːtɨŋɨt.</ta>
            <ta e="T155" id="Seg_3194" s="T147">lʼaŋalʼ qumɨlʼ noːr tɨmnʼäsɨt na tüːqolʼamtotɨt müttelʼe meːqɨnɨt.</ta>
            <ta e="T158" id="Seg_3195" s="T155">qarra čurontɨ moːːtɨglɨmɨt.</ta>
            <ta e="T160" id="Seg_3196" s="T158">čuːrontɨ motɨŋnʼentɨmɨt.</ta>
            <ta e="T162" id="Seg_3197" s="T160">apsɨp qənnäntɨmɨt.</ta>
            <ta e="T166" id="Seg_3198" s="T162">qumɨt kosti [mottɨla] tüntoːtɨt.</ta>
            <ta e="T167" id="Seg_3199" s="T166">amɨrtentotɨt.</ta>
            <ta e="T170" id="Seg_3200" s="T167">qarra qənnotɨt, čuːrontɨ.</ta>
            <ta e="T172" id="Seg_3201" s="T170">qärtoːtɨt motɨŋnotɨt.</ta>
            <ta e="T177" id="Seg_3202" s="T172">nʼütɨsä tokqotɨt mottɨ toːlʼ pʼelʼaqɨt.</ta>
            <ta e="T179" id="Seg_3203" s="T177">təːsa tokqotɨt.</ta>
            <ta e="T184" id="Seg_3204" s="T179">innänɨlʼlʼ[lʼlʼ]e aj nʼütɨsä tokqotɨt.</ta>
            <ta e="T188" id="Seg_3205" s="T184">tɨmnʼäqɨntɨkine kaša [avsa] mušɨrɨŋɨlʼɨt.</ta>
            <ta e="T190" id="Seg_3206" s="T188">tapčeːlɨ tüːntotɨt.</ta>
            <ta e="T194" id="Seg_3207" s="T190">ürɨlʼ pirnʼäp moːttɨ tulʼdɨŋɨlʼɨt.</ta>
            <ta e="T198" id="Seg_3208" s="T194">kašap [avsap] qäːrre muširotɨt.</ta>
            <ta e="T201" id="Seg_3209" s="T198">nanä na ômtotɨt.</ta>
            <ta e="T206" id="Seg_3210" s="T201">[čeːlʼɨtɨ] čeːlʼ čontɨlʼkotäntɨ, qorr[ll]ɨ[i]monnɨ.</ta>
            <ta e="T209" id="Seg_3211" s="T206">nʼ[j]ä[a]nnäntɨ an tiːrɨ qontɨ šelʼča.</ta>
            <ta e="T214" id="Seg_3212" s="T209">nogɨr qumɨt tipɨlʼ antɨ qontɨšenta.</ta>
            <ta e="T217" id="Seg_3213" s="T214">timnʼa iːqɨntɨ nilʼčik qətɨŋɨt.</ta>
            <ta e="T221" id="Seg_3214" s="T217">paŋɨlʼtoglʼet ilʼlʼa ättolʼnɨlʼɨt.</ta>
            <ta e="T225" id="Seg_3215" s="T221">peːmɨt küːtet šuː säqɨtolʼelʼčiŋɨlʼit.</ta>
            <ta e="T228" id="Seg_3216" s="T225">nɨnɨ nilʼčik qətɨŋɨlʼit.</ta>
            <ta e="T236" id="Seg_3217" s="T228">paŋɨmɨt čäŋka, paŋɨlɨt meːkɨnɨt miŋɨlʼɨt, solʼap meqɨ[lči]lʼ[lʼ]ɨmɨt amɨrqonɨtqoː.</ta>
            <ta e="T238" id="Seg_3218" s="T236">niːk ətɨrkɨŋɨtɨ.</ta>
            <ta e="T242" id="Seg_3219" s="T238">kɨpa timnʼätɨ sɨtkɨkɨt ilʼlʼa.</ta>
            <ta e="T246" id="Seg_3220" s="T242">täːqamtɨ nɨ ɨnnä čoqolnɨt.</ta>
            <ta e="T251" id="Seg_3221" s="T246">ɨnnät k tüt porɨlʼ po sorɨmɨntoːtɨt.</ta>
            <ta e="T259" id="Seg_3222" s="T251">niːk koŋotɨt nʼännäntɨ kontɨ šeiŋa nokɨr qumɨt tipɨlʼ antɨ.</ta>
            <ta e="T261" id="Seg_3223" s="T259">timnʼäqi[ɨ]mtɨ əːtɨrkɨ qolʼamnɨtɨ.</ta>
            <ta e="T265" id="Seg_3224" s="T261">ponä na čap tannɨntoːtet.</ta>
            <ta e="T270" id="Seg_3225" s="T265">montɨ ɨntɨlʼtišap ɨnne iqɨlʼpoːtet.</ta>
            <ta e="T274" id="Seg_3226" s="T270">ütqɨnɨ konnä toqalʼ čunʼtoːtɨt.</ta>
            <ta e="T278" id="Seg_3227" s="T274">ponä tantɨlʼlʼe niːk qə[ə]tɨŋɨt.</ta>
            <ta e="T287" id="Seg_3228" s="T278">konä tand̂ɨlʼä amɨrnɨlʼɨt, qoːtɨnʼisя mю[ü]ttɨlɨmɨt tentɨlʼäpoːqɨ ütqɨnɨ konnä.</ta>
            <ta e="T290" id="Seg_3229" s="T287">aj qaj meːqolʼamnɨlʼɨt. </ta>
            <ta e="T293" id="Seg_3230" s="T290">konnä tantɨlʼlʼä amɨrnɨlɨt.</ta>
            <ta e="T298" id="Seg_3231" s="T293">aː niŋ[lʼ] eːsä märqɨ timnяːtɨ. </ta>
            <ta e="T301" id="Seg_3232" s="T298">konnä tantɨlʼlʼä amɨrlɨmɨt.</ta>
            <ta e="T304" id="Seg_3233" s="T301">konnajep tattoːatet.</ta>
            <ta e="T310" id="Seg_3234" s="T304">ɨnd̂ɨlʼ tišam[n]te[ɨ]t qarreːt antoqɨntɨt pilʼlʼä qəːčalnoːtɨt.</ta>
            <ta e="T314" id="Seg_3235" s="T310">moːttɨ najep šeːrtoːtɨt.</ta>
            <ta e="T324" id="Seg_3236" s="T314">märqɨ lʼaŋalʼ qummɨlʼ moːter palʼnanopti ukkɨrqəntɨ na omnɨntoːtet.</ta>
            <ta e="T333" id="Seg_3237" s="T324">takɨlʼ oːtelʼnʼöːtɨlʼ timnʼяntɨsä aj ukkɨr qəːntɨ na omne[ɨ]ntoːte[ɨ]t.</ta>
            <ta e="T339" id="Seg_3238" s="T333">noqɨr timnʼяset muntɨk ukkɨr qöːnte omteloːtɨt.</ta>
            <ta e="T345" id="Seg_3239" s="T339">nɨn niːk qɨ[əː]tɨsotɨt: paŋɨmɨt qaj əmɨlʼtɨmnɨmɨt.</ta>
            <ta e="T347" id="Seg_3240" s="T345">paŋɨlɨt eŋa </ta>
            <ta e="T350" id="Seg_3241" s="T347">paŋɨsä šiːnet miŋɨlɨt.</ta>
            <ta e="T353" id="Seg_3242" s="T350">amɨrqo soːlap meːkɨllɨmɨt.</ta>
            <ta e="T360" id="Seg_3243" s="T353">paŋɨmtɨt muntɨk miːŋotɨt lʼaŋalʼ qumɨlʼ noːqɨr timnяːsɨt.</ta>
            <ta e="T367" id="Seg_3244" s="T360">a palʼnalʼ noːqɨr timnяːsɨt okot jap ətɨrkɨsoːtɨt.</ta>
            <ta e="T372" id="Seg_3245" s="T367">mɨ[ä]rqɨ timnяːtɨ kutar paŋɨsä qättɨntɨtɨ. </ta>
            <ta e="T376" id="Seg_3246" s="T372">töpɨt aj nilʼčik qättɨntoːtɨt.</ta>
            <ta e="T385" id="Seg_3247" s="T376">märqɨ ira ukkur čontoqɨt naːnnɨ matta saj[je]sa ijap mɨ[ä]rotɨt.</ta>
            <ta e="T393" id="Seg_3248" s="T385">paŋɨsä nɨjep qättotɨt lʼaŋalʼ qumɨlʼ noːr timnʼästɨp.</ta>
            <ta e="T399" id="Seg_3249" s="T393">Taŋɨlʼ ɔːtälʼ nʼoːtɨlʼ timnʼämtɨ čʼap qättɨtɨ. </ta>
            <ta e="T403" id="Seg_3250" s="T399">olʼa šʼunčipɨlʼ təttɨp qättɨtɨ.</ta>
            <ta e="T412" id="Seg_3251" s="T403">palʼnat timnʼätɨ taŋɨlʼ otalʼnʼöːtɨlʼ tɨmnʼätɨ aj nɨnɨ paqta.</ta>
            <ta e="T417" id="Seg_3252" s="T412">nʼölʼa qəntɨtɨ, täːqamtɨ orqɨlʼe nʼöːtɨŋɨt.</ta>
            <ta e="T422" id="Seg_3253" s="T417">tona šitɨ tɨmnʼäsɨqä moːtqɨt qaːlʼotɨt.</ta>
            <ta e="T426" id="Seg_3254" s="T422">qaj qənnɨtɨ, qaj qattɨjotɨt.</ta>
            <ta e="T428" id="Seg_3255" s="T426">Nʼoːla qəntɨtɨtɨ. </ta>
            <ta e="T432" id="Seg_3256" s="T428">Nʼennät mitallä čʼap počʼɔːlnɨtɨ. </ta>
            <ta e="T434" id="Seg_3257" s="T432">lʼakčitɨ pačalʼnɨt.</ta>
            <ta e="T438" id="Seg_3258" s="T434">qamte alʼča lʼaŋalʼ qup.</ta>
            <ta e="T445" id="Seg_3259" s="T438">šittelʼ čap patčalnɨt täːqat qətčomɨt orqɨlnɨt.</ta>
            <ta e="T448" id="Seg_3260" s="T445">topɨtɨ toːq säppempa.</ta>
            <ta e="T452" id="Seg_3261" s="T448">palʼnta tona timnʼäqɨmta qätpatɨ.</ta>
            <ta e="T457" id="Seg_3262" s="T452">timnʼäntɨ mättɨt šʼu nʼännɨ paqtɨ.</ta>
            <ta e="T460" id="Seg_3263" s="T457">qoŋɨtɨ mompa orqɨlʼpetɨ.</ta>
            <ta e="T462" id="Seg_3264" s="T460">mat tuːlʼlʼɨlʼlʼak.</ta>
            <ta e="T468" id="Seg_3265" s="T462">na küšil lʼ[l]aka aj qaj meːtɨtɨ.</ta>
            <ta e="T470" id="Seg_3266" s="T468">mat taːmentak.</ta>
            <ta e="T475" id="Seg_3267" s="T470">nɨnɨ noqɨr tɨmnʼästɨp muntɨk qəːtejotɨt.</ta>
            <ta e="T485" id="Seg_3268" s="T475">okkɨr tat tättɨt čontoqɨt nanɨ ilʼɨmpoːtɨt ni qajit süːmɨ čʼäŋka.</ta>
            <ta e="T488" id="Seg_3269" s="T485">qəŋnoːtɨt lʼaŋalʼ qumɨlʼ.</ta>
            <ta e="T491" id="Seg_3270" s="T488">imaqotalʼ nilʼčik tänɨrna.</ta>
            <ta e="T496" id="Seg_3271" s="T491">ija imɨ qəssotɨt meːlʼtɨt qo jamotɨŋotɨt.</ta>
            <ta e="T504" id="Seg_3272" s="T496">melʼlʼe mɨta šölʼkup ijamtɨ namɨt montɨ somak qəːtɨmpatɨ.</ta>
            <ta e="T508" id="Seg_3273" s="T504">mat kʼe ijamɨ qətɨp täkek.</ta>
            <ta e="T512" id="Seg_3274" s="T508">nümanɨ ukɨp aj musɨltɨkap.</ta>
            <ta e="T518" id="Seg_3275" s="T512">šolʼqumɨlʼ ima melʼlʼe qəː eŋa mannoːnɨ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1" id="Seg_3276" s="T0">Palna. </ta>
            <ta e="T5" id="Seg_3277" s="T1">Palna nɔːkɨr timnʼasɨk ilimmɨntɔːtɨt. </ta>
            <ta e="T9" id="Seg_3278" s="T5">Šölʼqumɨlʼ mɔːtɨrɨlʼ irra ɛppɨmmɨnta. </ta>
            <ta e="T14" id="Seg_3279" s="T9">Orsɨmɨlʼ irra samɨj märqɨ timnʼätɨt. </ta>
            <ta e="T20" id="Seg_3280" s="T14">Čʼontoːqɨlʼ timnʼätɨt taŋɨlʼ ɔːtälʼ nʼoːtɨj ɛppɨmmɨnta. </ta>
            <ta e="T27" id="Seg_3281" s="T20">Poːs kɨpa timnʼätɨ kərnʼat pɔːralʼ nʼoːtɨj ɛppɨmmɨnta. </ta>
            <ta e="T29" id="Seg_3282" s="T27">Nılʼčʼik ilimpɔːtɨt. </ta>
            <ta e="T37" id="Seg_3283" s="T29">A na Palnalʼ märqɨ timnʼätɨ namɨšʼak qəːtɨsä ɛːppɨmmɨnta. </ta>
            <ta e="T42" id="Seg_3284" s="T37">Kuna qaj tünta, muntɨk tɛnɨnɨmpatɨ. </ta>
            <ta e="T46" id="Seg_3285" s="T42">Suːmpɨtɨlʼ təːtɨpɨlʼ irra ɛːppɨmmɨnta. </ta>
            <ta e="T50" id="Seg_3286" s="T46">Kəːsɨp meːtɨmpatɨ ontɨ iliptäːqɨntɨ. </ta>
            <ta e="T56" id="Seg_3287" s="T50">Čʼɔːtɨrna kəːsɨp, nık meːtɨmpatɨ, čʼaŋak pünon. </ta>
            <ta e="T64" id="Seg_3288" s="T56">A rušit meːtɨmpɔːtɨt aša tɛnɨmop qajen nɔːnɨ… </ta>
            <ta e="T69" id="Seg_3289" s="T64">Tɨp meːtɨmpatɨ, kəːsɨtqo püp meːtɨmpatɨ. </ta>
            <ta e="T75" id="Seg_3290" s="T69">Na pün nɔːnɨ kəːsɨlʼ porqɨp meːtɨmpatɨ. </ta>
            <ta e="T83" id="Seg_3291" s="T75">Säq kəːsɨlʼ ɨntɨ meːtɨmpat, säq kəːsɨlʼ komap meːtɨmmɨntɨtɨ. </ta>
            <ta e="T86" id="Seg_3292" s="T83">Nantɨsä aj ilɨmpa. </ta>
            <ta e="T90" id="Seg_3293" s="T86">A müttɨlä aša ičʼirpa. </ta>
            <ta e="T93" id="Seg_3294" s="T90">Täpɨpkine müttɨlä ičʼirpatɨt. </ta>
            <ta e="T100" id="Seg_3295" s="T93">Kepkisolʼa kunɨ čʼap tüntɔːtɨt təp mun tɛnimɨt. </ta>
            <ta e="T103" id="Seg_3296" s="T100">Qälɨt ičʼirpɔːtɨt müttɨqo. </ta>
            <ta e="T110" id="Seg_3297" s="T103">Laŋalʼ qumɨt pɛlʼaqɨt aj nɔːr timnʼäsɨt ɛppɨntɔːtɨt. </ta>
            <ta e="T117" id="Seg_3298" s="T110">Aj peːlä ičʼirpɔːtɨt tapčʼeːlɨ qumɨlʼ nɔːr tɨmnʼastɨp. </ta>
            <ta e="T126" id="Seg_3299" s="T117">Təm ilɨmpa okoːt pulʼčʼos qoltoːqɨt, turuhanska kərrelʼ pɛlʼaqɨt čʼuːroːqɨt. </ta>
            <ta e="T135" id="Seg_3300" s="T126">Laŋalʼ qumɨlʼ nɔːkɨr timnʼäsɨt ukkɨr tät čʼontoːqɨt nılʼčʼik ɛːsɔːtɨt. </ta>
            <ta e="T140" id="Seg_3301" s="T135">Palnalʼ nɔːr tɨmnʼasɨtɨp peːlʼa qənqolamnɔːtɨt. </ta>
            <ta e="T144" id="Seg_3302" s="T140">A Palnalʼ timnʼäsɨt qənɔːtɨt. </ta>
            <ta e="T147" id="Seg_3303" s="T144">Tɨmnʼäiːqɨntɨ nılʼčʼik kətɨŋɨt. </ta>
            <ta e="T155" id="Seg_3304" s="T147">Laŋalʼ qumɨlʼ nɔːr tɨmnʼäsɨt na tüːqolamtɔːtɨt müttelä meːqɨnɨt. </ta>
            <ta e="T158" id="Seg_3305" s="T155">Karra čʼuːrontɨ mɔːtɨŋlɨmɨt. </ta>
            <ta e="T160" id="Seg_3306" s="T158">Čʼuːrontɨ mɔːtɨŋnʼɛntɨmɨt. </ta>
            <ta e="T162" id="Seg_3307" s="T160">Apsɨp qənnɛntɨmɨt. </ta>
            <ta e="T166" id="Seg_3308" s="T162">Qumɨt kosti (mɔːttɨla) tüntɔːtɨt. </ta>
            <ta e="T167" id="Seg_3309" s="T166">Amɨrtɛntɔːtɨt. </ta>
            <ta e="T170" id="Seg_3310" s="T167">Karra qənnɔːtɨt, čʼuːrontɨ. </ta>
            <ta e="T172" id="Seg_3311" s="T170">Kärtɔːtɨt, mɔːtɨŋnɔːtɨt. </ta>
            <ta e="T177" id="Seg_3312" s="T172">Nʼuːtɨsä tɔːqqɔːtɨt mɔːttɨ tolʼ pɛlʼaqɨt. </ta>
            <ta e="T179" id="Seg_3313" s="T177">Tösa tɔːqqɔːtɨt. </ta>
            <ta e="T184" id="Seg_3314" s="T179">ınnän ɨllä aj nʼuːtɨsä tɔːqqɔːtɨt. </ta>
            <ta e="T188" id="Seg_3315" s="T184">Tɨmnʼäqɨntɨkine: Kaša (avsa) mušɨrɨŋɨlɨt. </ta>
            <ta e="T190" id="Seg_3316" s="T188">Tapčʼeːlɨ tüntɔːtɨt. </ta>
            <ta e="T194" id="Seg_3317" s="T190">Ürɨlʼ pirnʼäp mɔːttɨ tultɨŋɨlɨt. </ta>
            <ta e="T198" id="Seg_3318" s="T194">Kašap (avsap) kärrä mušɨrɔːtɨt. </ta>
            <ta e="T201" id="Seg_3319" s="T198">Nanä na ɔːmtɔːtɨt. </ta>
            <ta e="T206" id="Seg_3320" s="T201">(Čʼeːlɨtɨ) čʼeːlʼ čʼontɨlʼ kotäntɨ korrɨmɔːnnɨ. </ta>
            <ta e="T209" id="Seg_3321" s="T206">Nʼennäntɨ antıːrɨ qontɨšɛlʼčʼa. </ta>
            <ta e="T214" id="Seg_3322" s="T209">Nɔːkɨr qumɨt tıpɨlʼ antɨ qontɨššɛːnta. </ta>
            <ta e="T217" id="Seg_3323" s="T214">Timnʼaiːqɨntɨ nılʼčʼik kətɨŋɨt. </ta>
            <ta e="T221" id="Seg_3324" s="T217">Paŋɨlʼ toqlʼet ılla ättɔːlnɨlɨt. </ta>
            <ta e="T225" id="Seg_3325" s="T221">Peːmɨt küːtet šüː säqɨtɔːlɛlʼčʼiŋɨlɨt. </ta>
            <ta e="T228" id="Seg_3326" s="T225">Nɨːnɨ nılʼčʼik kətɨŋɨlit. </ta>
            <ta e="T236" id="Seg_3327" s="T228">Paŋɨmɨt čʼäːŋka, paŋɨlɨt meːqɨnɨt miŋɨlɨt, solap meːqɨllʼčʼilɨmɨt amɨrqonɨtqo. </ta>
            <ta e="T238" id="Seg_3328" s="T236">Nık ətɨrkɨŋɨtɨ. </ta>
            <ta e="T242" id="Seg_3329" s="T238">Kɨpa timnʼätɨ sɨtkɨqɨt ila. </ta>
            <ta e="T246" id="Seg_3330" s="T242">Täːqamtɨ nɨː ɨnnä čʼoqolnɨt. </ta>
            <ta e="T251" id="Seg_3331" s="T246">Ɨnnät tüt poːrɨlʼ po sɔːrɨmmɨntɔːtɨt. </ta>
            <ta e="T259" id="Seg_3332" s="T251">Nık qoŋɔːtɨt: nʼennäntɨ qontɨššɛıːŋa nɔːkɨr qumɨt tıpɨlʼ antɨ. </ta>
            <ta e="T261" id="Seg_3333" s="T259">Timnʼäːqımtɨ əːtɨrkɨqolamnɨtɨ. </ta>
            <ta e="T265" id="Seg_3334" s="T261">Ponä na čʼap tannɨntɔːtet. </ta>
            <ta e="T270" id="Seg_3335" s="T265">Montɨ ɨntɨlʼ tıšap ɨnnä iːqɨlpɔːtet. </ta>
            <ta e="T274" id="Seg_3336" s="T270">Ütqɨnɨ konnä toqalʼ čʼüntɔːtɨt. </ta>
            <ta e="T278" id="Seg_3337" s="T274">Ponä tantɨlä nık kətɨŋɨt. </ta>
            <ta e="T287" id="Seg_3338" s="T278">Konnä tantɨlä amɨrŋɨlɨt, qɔːtɨnıːsʼa müttɨlɨmɨt tentɨlä poːqɨ ütqɨnɨ konnä. </ta>
            <ta e="T290" id="Seg_3339" s="T287">Aj qaj meːqolamnɨlɨt? </ta>
            <ta e="T293" id="Seg_3340" s="T290">Konnä tantɨlä amɨrŋɨlɨt. </ta>
            <ta e="T298" id="Seg_3341" s="T293">A nıŋ ɛːsä märqɨ timnʼatɨ. </ta>
            <ta e="T301" id="Seg_3342" s="T298">Konnä tantɨlä amɨrlɨmɨt. </ta>
            <ta e="T304" id="Seg_3343" s="T301">Konna jep tattɔːtet. </ta>
            <ta e="T310" id="Seg_3344" s="T304">Ɨntɨlʼ tıšamtɨt karrät antoːqɨntɨt pillä qəːčʼalnɔːtɨt. </ta>
            <ta e="T314" id="Seg_3345" s="T310">Mɔːttɨ na jep šeːrtɔːtɨt. </ta>
            <ta e="T324" id="Seg_3346" s="T314">Mərqɨ laŋalʼ qumɨlʼ mɔːter Palnan optı ukkɨr qəntɨ na omnɨntɔːtet. </ta>
            <ta e="T333" id="Seg_3347" s="T324">Takɨlʼ ɔːtälʼ nʼoːtɨlʼ timnʼantɨsä aj ukkɨr qəntɨ na omnɨntɔːtɨt. </ta>
            <ta e="T339" id="Seg_3348" s="T333">Nɔːkɨr timnʼaset muntɨk ukkɨr qönte omtelɔːtɨt. </ta>
            <ta e="T345" id="Seg_3349" s="T339">Nɨːn nık kətɨsɔːtɨt: Paŋɨmɨt qaj əmɨltɨmnɨmɨt. </ta>
            <ta e="T347" id="Seg_3350" s="T345">Paŋɨlɨt ɛːŋa? </ta>
            <ta e="T350" id="Seg_3351" s="T347">Paŋɨsä šınet miŋɨlɨt. </ta>
            <ta e="T353" id="Seg_3352" s="T350">Amɨrqo solap meːqɨllɨmɨt. </ta>
            <ta e="T360" id="Seg_3353" s="T353">Paŋɨmtɨt muntɨk miŋɔːtɨt laŋalʼ qumɨlʼ nɔːkɨr timnʼasɨt. </ta>
            <ta e="T367" id="Seg_3354" s="T360">A Palnalʼ nɔːkɨr timnʼasɨt okoːt jap əːtɨrkɨsɔːtɨt. </ta>
            <ta e="T372" id="Seg_3355" s="T367">Märqɨ timnʼatɨ kutar paŋɨsä qättɨntɨtɨ. </ta>
            <ta e="T376" id="Seg_3356" s="T372">Təpɨt aj nılʼčʼɨk qättɨntɔːtɨt. </ta>
            <ta e="T385" id="Seg_3357" s="T376">Märqɨ ira ukkur čʼontoːqɨt nanɨ matta, sajesa ijap mɨrɔːtɨt. </ta>
            <ta e="T393" id="Seg_3358" s="T385">Paŋɨsä nɨː jep qättɔːtɨt laŋalʼ qumɨlʼ nɔːr timnʼästɨp. </ta>
            <ta e="T399" id="Seg_3359" s="T393">Taŋɨlʼ ɔːtälʼ nʼoːtɨlʼ timnʼämtɨ čʼap qättɨtɨ. </ta>
            <ta e="T403" id="Seg_3360" s="T399">Ola šünʼčʼipɨlʼ təttɨp qättɨtɨ. </ta>
            <ta e="T412" id="Seg_3361" s="T403">Palnat timnʼätɨ taŋɨlʼ ɔːtalʼ nʼoːtɨlʼ tɨmnʼätɨ aj nɨːnɨ pakta. </ta>
            <ta e="T417" id="Seg_3362" s="T412">Nʼoːla qəntɨtɨ, täːqamtɨ orqɨllä nʼoːtɨŋɨt. </ta>
            <ta e="T422" id="Seg_3363" s="T417">Toːnna šitɨ tɨmnʼäsɨqä mɔːtqɨt qalɔːtɨt. </ta>
            <ta e="T426" id="Seg_3364" s="T422">Qaj qənnɨtɨ, qaj qattɨjɔːtɨt. </ta>
            <ta e="T428" id="Seg_3365" s="T426">Nʼoːla qəntɨtɨtɨ. </ta>
            <ta e="T432" id="Seg_3366" s="T428">Nʼennät mitallä čʼap počʼɔːlnɨtɨ. </ta>
            <ta e="T434" id="Seg_3367" s="T432">Lʼakčʼitɨ pačʼalnɨt. </ta>
            <ta e="T438" id="Seg_3368" s="T434">Qamtä alʼčʼa laŋalʼ qup. </ta>
            <ta e="T445" id="Seg_3369" s="T438">Šittäl čʼap pačʼalnɨt täːqat kət čʼoːmɨt orqɨlnɨt. </ta>
            <ta e="T448" id="Seg_3370" s="T445">Topɨtɨ toːq säppɛːmpa. </ta>
            <ta e="T452" id="Seg_3371" s="T448">Palna toːnna timnʼäqɨmta qätpatɨ. </ta>
            <ta e="T457" id="Seg_3372" s="T452">Timnʼäntɨ mättɨt šüː nʼennɨ paktɨ. </ta>
            <ta e="T460" id="Seg_3373" s="T457">Qoŋɨtɨ mompa orqɨlʼpetɨ. </ta>
            <ta e="T462" id="Seg_3374" s="T460">Mat tülɨlak. </ta>
            <ta e="T468" id="Seg_3375" s="T462">Na küšilʼ laka aj qaj meːtɨtɨ. </ta>
            <ta e="T470" id="Seg_3376" s="T468">Mat tamɛntak. </ta>
            <ta e="T475" id="Seg_3377" s="T470">Nɨːnɨ nɔːkɨr tɨmnʼästɨp muntɨk qəttɛːjɔːtɨt. </ta>
            <ta e="T485" id="Seg_3378" s="T475">Okkɨr taːt tättɨt čʼontoːqɨt nanɨ ilɨmpɔːtɨt, nʼi qajit sümɨ čʼäːŋka. </ta>
            <ta e="T488" id="Seg_3379" s="T485">Qənŋɔːtɨt laŋalʼ qumɨlʼ. </ta>
            <ta e="T491" id="Seg_3380" s="T488">Imaqotalʼ nılʼčʼik tɛnɨrna. </ta>
            <ta e="T496" id="Seg_3381" s="T491">Ijaiːmɨ qəssɔːtɨt meːltɨt qoja mɔːtɨŋnɔːtɨt. </ta>
            <ta e="T504" id="Seg_3382" s="T496">Meːlle mɨta šölʼqup ijamtɨ namɨt mɔːntɨ somak kətɨmpatɨ. </ta>
            <ta e="T508" id="Seg_3383" s="T504">Mat kʼe ijamɨ kəːtɨptäkek. </ta>
            <ta e="T512" id="Seg_3384" s="T508">Nümanɨ ukɨp aj musɨltɨkap. </ta>
            <ta e="T518" id="Seg_3385" s="T512">Šölʼqumɨlʼ ima meːlle qəː ɛːŋa mannoːnɨ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_3386" s="T0">palna</ta>
            <ta e="T2" id="Seg_3387" s="T1">palna</ta>
            <ta e="T3" id="Seg_3388" s="T2">nɔːkɨr</ta>
            <ta e="T4" id="Seg_3389" s="T3">timnʼa-sɨ-k</ta>
            <ta e="T5" id="Seg_3390" s="T4">ili-mmɨ-ntɔː-tɨt</ta>
            <ta e="T6" id="Seg_3391" s="T5">šölʼqum-ɨ-lʼ</ta>
            <ta e="T7" id="Seg_3392" s="T6">mɔːtɨr-ɨ-lʼ</ta>
            <ta e="T8" id="Seg_3393" s="T7">irra</ta>
            <ta e="T9" id="Seg_3394" s="T8">ɛ-ppɨ-mmɨ-nta</ta>
            <ta e="T10" id="Seg_3395" s="T9">or-sɨmɨ-lʼ</ta>
            <ta e="T11" id="Seg_3396" s="T10">irra</ta>
            <ta e="T12" id="Seg_3397" s="T11">samɨj</ta>
            <ta e="T13" id="Seg_3398" s="T12">märqɨ</ta>
            <ta e="T14" id="Seg_3399" s="T13">timnʼä-t-ɨ-t</ta>
            <ta e="T15" id="Seg_3400" s="T14">čʼontoː-qɨ-lʼ</ta>
            <ta e="T16" id="Seg_3401" s="T15">timnʼä-t-ɨ-t</ta>
            <ta e="T17" id="Seg_3402" s="T16">taŋɨ-lʼ</ta>
            <ta e="T18" id="Seg_3403" s="T17">ɔːtä-lʼ</ta>
            <ta e="T19" id="Seg_3404" s="T18">nʼoː-tɨj</ta>
            <ta e="T20" id="Seg_3405" s="T19">ɛ-ppɨ-mmɨ-nta</ta>
            <ta e="T21" id="Seg_3406" s="T20">poːs</ta>
            <ta e="T22" id="Seg_3407" s="T21">kɨpa</ta>
            <ta e="T23" id="Seg_3408" s="T22">timnʼä-tɨ</ta>
            <ta e="T24" id="Seg_3409" s="T23">kərnʼa-t</ta>
            <ta e="T25" id="Seg_3410" s="T24">pɔːra-lʼ</ta>
            <ta e="T26" id="Seg_3411" s="T25">nʼoː-tɨj</ta>
            <ta e="T27" id="Seg_3412" s="T26">ɛ-ppɨ-mmɨ-nta</ta>
            <ta e="T28" id="Seg_3413" s="T27">nılʼčʼi-k</ta>
            <ta e="T29" id="Seg_3414" s="T28">ili-mpɔː-tɨt</ta>
            <ta e="T30" id="Seg_3415" s="T29">a</ta>
            <ta e="T31" id="Seg_3416" s="T30">na</ta>
            <ta e="T32" id="Seg_3417" s="T31">palna-lʼ</ta>
            <ta e="T33" id="Seg_3418" s="T32">märqɨ</ta>
            <ta e="T34" id="Seg_3419" s="T33">timnʼä-tɨ</ta>
            <ta e="T35" id="Seg_3420" s="T34">namɨšʼak</ta>
            <ta e="T36" id="Seg_3421" s="T35">qəːtɨ-sä</ta>
            <ta e="T37" id="Seg_3422" s="T36">ɛː-ppɨ-mmɨ-nta</ta>
            <ta e="T38" id="Seg_3423" s="T37">kuna</ta>
            <ta e="T39" id="Seg_3424" s="T38">qaj</ta>
            <ta e="T40" id="Seg_3425" s="T39">tü-nta</ta>
            <ta e="T41" id="Seg_3426" s="T40">muntɨk</ta>
            <ta e="T42" id="Seg_3427" s="T41">tɛnɨnɨ-mpa-tɨ</ta>
            <ta e="T43" id="Seg_3428" s="T42">suːmpɨ-tɨlʼ</ta>
            <ta e="T44" id="Seg_3429" s="T43">təːtɨpɨ-lʼ</ta>
            <ta e="T45" id="Seg_3430" s="T44">irra</ta>
            <ta e="T46" id="Seg_3431" s="T45">ɛː-ppɨ-mmɨ-nta</ta>
            <ta e="T47" id="Seg_3432" s="T46">kəːsɨ-p</ta>
            <ta e="T48" id="Seg_3433" s="T47">meː-tɨ-mpa-tɨ</ta>
            <ta e="T49" id="Seg_3434" s="T48">ontɨ</ta>
            <ta e="T50" id="Seg_3435" s="T49">ili-ptäː-qɨn-tɨ</ta>
            <ta e="T51" id="Seg_3436" s="T50">čʼɔːtɨ-r-na</ta>
            <ta e="T52" id="Seg_3437" s="T51">kəːsɨ-p</ta>
            <ta e="T53" id="Seg_3438" s="T52">nık</ta>
            <ta e="T54" id="Seg_3439" s="T53">meː-tɨ-mpa-tɨ</ta>
            <ta e="T55" id="Seg_3440" s="T54">čʼaŋak</ta>
            <ta e="T56" id="Seg_3441" s="T55">pü-n-o-n</ta>
            <ta e="T57" id="Seg_3442" s="T56">a</ta>
            <ta e="T58" id="Seg_3443" s="T57">ruš-i-t</ta>
            <ta e="T59" id="Seg_3444" s="T58">meː-tɨ-mpɔː-tɨt</ta>
            <ta e="T60" id="Seg_3445" s="T59">aša</ta>
            <ta e="T61" id="Seg_3446" s="T60">tɛnɨmo-p</ta>
            <ta e="T62" id="Seg_3447" s="T61">qaj-e-n</ta>
            <ta e="T63" id="Seg_3448" s="T62">nɔː-nɨ</ta>
            <ta e="T65" id="Seg_3449" s="T64">tɨp</ta>
            <ta e="T66" id="Seg_3450" s="T65">meː-tɨ-mpa-tɨ</ta>
            <ta e="T67" id="Seg_3451" s="T66">kəːsɨ-tqo</ta>
            <ta e="T68" id="Seg_3452" s="T67">pü-p</ta>
            <ta e="T69" id="Seg_3453" s="T68">meː-tɨ-mpa-tɨ</ta>
            <ta e="T70" id="Seg_3454" s="T69">na</ta>
            <ta e="T71" id="Seg_3455" s="T70">pü-n</ta>
            <ta e="T72" id="Seg_3456" s="T71">nɔː-nɨ</ta>
            <ta e="T73" id="Seg_3457" s="T72">kəːsɨ-lʼ</ta>
            <ta e="T74" id="Seg_3458" s="T73">porqɨ-p</ta>
            <ta e="T75" id="Seg_3459" s="T74">meː-tɨ-mpa-tɨ</ta>
            <ta e="T77" id="Seg_3460" s="T76">kəːsɨ-lʼ</ta>
            <ta e="T78" id="Seg_3461" s="T77">ɨntɨ</ta>
            <ta e="T79" id="Seg_3462" s="T78">meː-tɨ-mpa-t</ta>
            <ta e="T81" id="Seg_3463" s="T80">kəːsɨ-lʼ</ta>
            <ta e="T82" id="Seg_3464" s="T81">koma-p</ta>
            <ta e="T83" id="Seg_3465" s="T82">meː-tɨ-mmɨ-ntɨ-tɨ</ta>
            <ta e="T84" id="Seg_3466" s="T83">na-ntɨ-sä</ta>
            <ta e="T85" id="Seg_3467" s="T84">aj</ta>
            <ta e="T86" id="Seg_3468" s="T85">ilɨ-mpa</ta>
            <ta e="T87" id="Seg_3469" s="T86">a</ta>
            <ta e="T88" id="Seg_3470" s="T87">müt-tɨ-lä</ta>
            <ta e="T89" id="Seg_3471" s="T88">aša</ta>
            <ta e="T90" id="Seg_3472" s="T89">ičʼi-r-pa</ta>
            <ta e="T91" id="Seg_3473" s="T90">täp-ɨ-pkine</ta>
            <ta e="T92" id="Seg_3474" s="T91">müt-tɨ-lä</ta>
            <ta e="T93" id="Seg_3475" s="T92">ičʼi-r-pa-tɨt</ta>
            <ta e="T95" id="Seg_3476" s="T94">kuːnɨ</ta>
            <ta e="T96" id="Seg_3477" s="T95">čʼap</ta>
            <ta e="T97" id="Seg_3478" s="T96">tü-ntɔː-tɨt</ta>
            <ta e="T98" id="Seg_3479" s="T97">təp</ta>
            <ta e="T99" id="Seg_3480" s="T98">mun</ta>
            <ta e="T100" id="Seg_3481" s="T99">tɛnimɨ-t</ta>
            <ta e="T101" id="Seg_3482" s="T100">qälɨ-t</ta>
            <ta e="T102" id="Seg_3483" s="T101">ičʼi-r-pɔː-tɨt</ta>
            <ta e="T103" id="Seg_3484" s="T102">müt-tɨ-qo</ta>
            <ta e="T104" id="Seg_3485" s="T103">laŋa-lʼ</ta>
            <ta e="T105" id="Seg_3486" s="T104">qum-ɨ-t</ta>
            <ta e="T106" id="Seg_3487" s="T105">pɛlʼa-qɨt</ta>
            <ta e="T107" id="Seg_3488" s="T106">aj</ta>
            <ta e="T108" id="Seg_3489" s="T107">nɔːr</ta>
            <ta e="T109" id="Seg_3490" s="T108">timnʼä-sɨ-t</ta>
            <ta e="T110" id="Seg_3491" s="T109">ɛ-ppɨ-ntɔː-tɨt</ta>
            <ta e="T111" id="Seg_3492" s="T110">aj</ta>
            <ta e="T112" id="Seg_3493" s="T111">peː-lä</ta>
            <ta e="T113" id="Seg_3494" s="T112">ičʼi-r-pɔː-tɨt</ta>
            <ta e="T114" id="Seg_3495" s="T113">tapčʼeːlɨ</ta>
            <ta e="T115" id="Seg_3496" s="T114">qum-ɨ-lʼ</ta>
            <ta e="T116" id="Seg_3497" s="T115">nɔːr</ta>
            <ta e="T117" id="Seg_3498" s="T116">tɨmnʼa-s-t-ɨ-p</ta>
            <ta e="T118" id="Seg_3499" s="T117">təm</ta>
            <ta e="T119" id="Seg_3500" s="T118">ilɨ-mpa</ta>
            <ta e="T120" id="Seg_3501" s="T119">okoːt</ta>
            <ta e="T121" id="Seg_3502" s="T120">pu-lʼ-čʼos</ta>
            <ta e="T122" id="Seg_3503" s="T121">qoltoː-qɨt</ta>
            <ta e="T123" id="Seg_3504" s="T122">turuhanska</ta>
            <ta e="T124" id="Seg_3505" s="T123">kərrelʼ</ta>
            <ta e="T125" id="Seg_3506" s="T124">pɛlʼa-qɨt</ta>
            <ta e="T126" id="Seg_3507" s="T125">čʼuːroː-qɨt</ta>
            <ta e="T127" id="Seg_3508" s="T126">laŋa-lʼ</ta>
            <ta e="T128" id="Seg_3509" s="T127">qum-ɨ-lʼ</ta>
            <ta e="T129" id="Seg_3510" s="T128">nɔːkɨr</ta>
            <ta e="T130" id="Seg_3511" s="T129">timnʼä-sɨ-t</ta>
            <ta e="T131" id="Seg_3512" s="T130">ukkɨr</ta>
            <ta e="T132" id="Seg_3513" s="T131">tät</ta>
            <ta e="T133" id="Seg_3514" s="T132">čʼontoː-qɨt</ta>
            <ta e="T134" id="Seg_3515" s="T133">nılʼčʼi-k</ta>
            <ta e="T135" id="Seg_3516" s="T134">ɛː-sɔː-tɨt</ta>
            <ta e="T136" id="Seg_3517" s="T135">palna-lʼ</ta>
            <ta e="T137" id="Seg_3518" s="T136">nɔːr</ta>
            <ta e="T138" id="Seg_3519" s="T137">tɨmnʼa-sɨ-t-ɨ-p</ta>
            <ta e="T139" id="Seg_3520" s="T138">peː-lʼa</ta>
            <ta e="T140" id="Seg_3521" s="T139">qən-q-olam-nɔː-tɨt</ta>
            <ta e="T141" id="Seg_3522" s="T140">a</ta>
            <ta e="T142" id="Seg_3523" s="T141">palna-lʼ</ta>
            <ta e="T143" id="Seg_3524" s="T142">timnʼä-sɨ-t</ta>
            <ta e="T144" id="Seg_3525" s="T143">qənɔː-tɨt</ta>
            <ta e="T145" id="Seg_3526" s="T144">tɨmnʼä-iː-qɨn-tɨ</ta>
            <ta e="T146" id="Seg_3527" s="T145">nılʼčʼi-k</ta>
            <ta e="T147" id="Seg_3528" s="T146">kətɨ-ŋɨ-t</ta>
            <ta e="T148" id="Seg_3529" s="T147">laŋa-lʼ</ta>
            <ta e="T149" id="Seg_3530" s="T148">qum-ɨ-lʼ</ta>
            <ta e="T150" id="Seg_3531" s="T149">nɔːr</ta>
            <ta e="T151" id="Seg_3532" s="T150">tɨmnʼä-sɨ-t</ta>
            <ta e="T152" id="Seg_3533" s="T151">na</ta>
            <ta e="T153" id="Seg_3534" s="T152">tüː-q-olam-tɔː-tɨt</ta>
            <ta e="T154" id="Seg_3535" s="T153">müt-te-lä</ta>
            <ta e="T155" id="Seg_3536" s="T154">meːqɨnɨt</ta>
            <ta e="T156" id="Seg_3537" s="T155">karra</ta>
            <ta e="T157" id="Seg_3538" s="T156">čʼuːro-ntɨ</ta>
            <ta e="T158" id="Seg_3539" s="T157">mɔːt-ɨ-ŋ-lɨ-mɨt</ta>
            <ta e="T159" id="Seg_3540" s="T158">čʼuːro-ntɨ</ta>
            <ta e="T160" id="Seg_3541" s="T159">mɔːt-ɨ-ŋ-nʼɛntɨ-mɨt</ta>
            <ta e="T161" id="Seg_3542" s="T160">apsɨ-p</ta>
            <ta e="T162" id="Seg_3543" s="T161">qən-n-ɛntɨ-mɨt</ta>
            <ta e="T163" id="Seg_3544" s="T162">qum-ɨ-t</ta>
            <ta e="T164" id="Seg_3545" s="T163">kosti</ta>
            <ta e="T165" id="Seg_3546" s="T164">mɔːt-tɨ-la</ta>
            <ta e="T166" id="Seg_3547" s="T165">tü-ntɔː-tɨt</ta>
            <ta e="T167" id="Seg_3548" s="T166">am-ɨ-r-tɛntɔː-tɨt</ta>
            <ta e="T168" id="Seg_3549" s="T167">karra</ta>
            <ta e="T169" id="Seg_3550" s="T168">qən-nɔː-tɨt</ta>
            <ta e="T170" id="Seg_3551" s="T169">čʼuːro-ntɨ</ta>
            <ta e="T171" id="Seg_3552" s="T170">kär-tɔː-tɨt</ta>
            <ta e="T172" id="Seg_3553" s="T171">mɔːt-ɨ-ŋ-nɔː-tɨt</ta>
            <ta e="T173" id="Seg_3554" s="T172">nʼuːtɨ-sä</ta>
            <ta e="T174" id="Seg_3555" s="T173">tɔːqqɔː-tɨt</ta>
            <ta e="T175" id="Seg_3556" s="T174">mɔːt-tɨ</ta>
            <ta e="T176" id="Seg_3557" s="T175">to-lʼ</ta>
            <ta e="T177" id="Seg_3558" s="T176">pɛlʼa-qɨt</ta>
            <ta e="T178" id="Seg_3559" s="T177">tö-sa</ta>
            <ta e="T179" id="Seg_3560" s="T178">tɔːqqɔː-tɨt</ta>
            <ta e="T180" id="Seg_3561" s="T179">ınnä-n</ta>
            <ta e="T181" id="Seg_3562" s="T180">ɨllä</ta>
            <ta e="T182" id="Seg_3563" s="T181">aj</ta>
            <ta e="T183" id="Seg_3564" s="T182">nʼuːtɨ-sä</ta>
            <ta e="T184" id="Seg_3565" s="T183">tɔːqqɔː-tɨt</ta>
            <ta e="T185" id="Seg_3566" s="T184">tɨmnʼä-qɨ-ntɨ-kine</ta>
            <ta e="T186" id="Seg_3567" s="T185">kaša</ta>
            <ta e="T187" id="Seg_3568" s="T186">avsa</ta>
            <ta e="T188" id="Seg_3569" s="T187">mušɨ-rɨ-ŋɨlɨt</ta>
            <ta e="T189" id="Seg_3570" s="T188">tapčʼeːlɨ</ta>
            <ta e="T190" id="Seg_3571" s="T189">tü-ntɔː-tɨt</ta>
            <ta e="T191" id="Seg_3572" s="T190">ür-ɨ-lʼ</ta>
            <ta e="T192" id="Seg_3573" s="T191">pirnʼä-p</ta>
            <ta e="T193" id="Seg_3574" s="T192">mɔːt-tɨ</ta>
            <ta e="T194" id="Seg_3575" s="T193">tul-tɨ-ŋɨlɨt</ta>
            <ta e="T195" id="Seg_3576" s="T194">kaša-p</ta>
            <ta e="T196" id="Seg_3577" s="T195">avsa-p</ta>
            <ta e="T197" id="Seg_3578" s="T196">kärrä</ta>
            <ta e="T198" id="Seg_3579" s="T197">mušɨ-rɔː-tɨt</ta>
            <ta e="T199" id="Seg_3580" s="T198">nanä</ta>
            <ta e="T200" id="Seg_3581" s="T199">na</ta>
            <ta e="T201" id="Seg_3582" s="T200">ɔːmtɔː-tɨt</ta>
            <ta e="T202" id="Seg_3583" s="T201">čʼeːlɨ-tɨ</ta>
            <ta e="T203" id="Seg_3584" s="T202">čʼeː-lʼ</ta>
            <ta e="T204" id="Seg_3585" s="T203">čʼontɨ-lʼ</ta>
            <ta e="T205" id="Seg_3586" s="T204">kotä-ntɨ</ta>
            <ta e="T206" id="Seg_3587" s="T205">korrɨ-mɔːn-nɨ</ta>
            <ta e="T207" id="Seg_3588" s="T206">nʼennä-ntɨ</ta>
            <ta e="T208" id="Seg_3589" s="T207">ant-ıːrɨ</ta>
            <ta e="T209" id="Seg_3590" s="T208">qontɨ-š-ɛlʼčʼa</ta>
            <ta e="T210" id="Seg_3591" s="T209">nɔːkɨr</ta>
            <ta e="T211" id="Seg_3592" s="T210">qum-ɨ-t</ta>
            <ta e="T212" id="Seg_3593" s="T211">tıp-ɨ-lʼ</ta>
            <ta e="T213" id="Seg_3594" s="T212">antɨ</ta>
            <ta e="T214" id="Seg_3595" s="T213">qontɨ-šš-ɛː-nta</ta>
            <ta e="T215" id="Seg_3596" s="T214">timnʼa-iː-qɨn-tɨ</ta>
            <ta e="T216" id="Seg_3597" s="T215">nılʼčʼi-k</ta>
            <ta e="T217" id="Seg_3598" s="T216">kətɨ-ŋɨ-t</ta>
            <ta e="T218" id="Seg_3599" s="T217">paŋɨ-lʼ</ta>
            <ta e="T219" id="Seg_3600" s="T218">toq-lʼe-t</ta>
            <ta e="T220" id="Seg_3601" s="T219">ılla</ta>
            <ta e="T221" id="Seg_3602" s="T220">ätt-ɔːl-nɨlɨt</ta>
            <ta e="T222" id="Seg_3603" s="T221">peːmɨ-t</ta>
            <ta e="T223" id="Seg_3604" s="T222">küːt-e-t</ta>
            <ta e="T224" id="Seg_3605" s="T223">šüː</ta>
            <ta e="T225" id="Seg_3606" s="T224">säqɨ-tɔːl-ɛlʼčʼi-ŋɨlɨt</ta>
            <ta e="T226" id="Seg_3607" s="T225">nɨːnɨ</ta>
            <ta e="T227" id="Seg_3608" s="T226">nılʼčʼi-k</ta>
            <ta e="T228" id="Seg_3609" s="T227">kətɨ-ŋɨlit</ta>
            <ta e="T229" id="Seg_3610" s="T228">paŋɨ-mɨt</ta>
            <ta e="T230" id="Seg_3611" s="T229">čʼäːŋka</ta>
            <ta e="T231" id="Seg_3612" s="T230">paŋɨ-lɨt</ta>
            <ta e="T232" id="Seg_3613" s="T231">meːqɨnɨt</ta>
            <ta e="T233" id="Seg_3614" s="T232">mi-ŋɨlɨt</ta>
            <ta e="T234" id="Seg_3615" s="T233">sola-p</ta>
            <ta e="T235" id="Seg_3616" s="T234">meː-qɨl-lʼčʼi-lɨ-mɨt</ta>
            <ta e="T236" id="Seg_3617" s="T235">am-ɨ-r-qonɨtqo</ta>
            <ta e="T237" id="Seg_3618" s="T236">nık</ta>
            <ta e="T238" id="Seg_3619" s="T237">ətɨ-r-kɨ-ŋɨ-tɨ</ta>
            <ta e="T239" id="Seg_3620" s="T238">kɨpa</ta>
            <ta e="T240" id="Seg_3621" s="T239">timnʼä-tɨ</ta>
            <ta e="T241" id="Seg_3622" s="T240">sɨtkɨ-qɨt</ta>
            <ta e="T242" id="Seg_3623" s="T241">ila</ta>
            <ta e="T243" id="Seg_3624" s="T242">täːqa-m-tɨ</ta>
            <ta e="T244" id="Seg_3625" s="T243">nɨː</ta>
            <ta e="T245" id="Seg_3626" s="T244">ɨnnä</ta>
            <ta e="T246" id="Seg_3627" s="T245">čʼoqo-l-nɨ-t</ta>
            <ta e="T247" id="Seg_3628" s="T246">ɨnnä-t</ta>
            <ta e="T248" id="Seg_3629" s="T247">tü-t</ta>
            <ta e="T249" id="Seg_3630" s="T248">poːrɨ-lʼ</ta>
            <ta e="T250" id="Seg_3631" s="T249">po</ta>
            <ta e="T251" id="Seg_3632" s="T250">sɔːrɨ-mmɨ-ntɔː-tɨt</ta>
            <ta e="T252" id="Seg_3633" s="T251">nık</ta>
            <ta e="T253" id="Seg_3634" s="T252">qo-ŋɔː-tɨt</ta>
            <ta e="T254" id="Seg_3635" s="T253">nʼennä-ntɨ</ta>
            <ta e="T255" id="Seg_3636" s="T254">qontɨ-šš-ɛıː-ŋa</ta>
            <ta e="T256" id="Seg_3637" s="T255">nɔːkɨr</ta>
            <ta e="T257" id="Seg_3638" s="T256">qum-ɨ-t</ta>
            <ta e="T258" id="Seg_3639" s="T257">tıp-ɨ-lʼ</ta>
            <ta e="T259" id="Seg_3640" s="T258">antɨ</ta>
            <ta e="T260" id="Seg_3641" s="T259">timnʼäː-qı-m-tɨ</ta>
            <ta e="T261" id="Seg_3642" s="T260">əːtɨ-r-kɨ-q-olam-nɨ-tɨ</ta>
            <ta e="T262" id="Seg_3643" s="T261">ponä</ta>
            <ta e="T263" id="Seg_3644" s="T262">na</ta>
            <ta e="T264" id="Seg_3645" s="T263">čʼap</ta>
            <ta e="T265" id="Seg_3646" s="T264">tannɨ-ntɔː-tet</ta>
            <ta e="T266" id="Seg_3647" s="T265">montɨ</ta>
            <ta e="T267" id="Seg_3648" s="T266">ɨntɨ-lʼ</ta>
            <ta e="T268" id="Seg_3649" s="T267">tıša-p</ta>
            <ta e="T269" id="Seg_3650" s="T268">ɨnnä</ta>
            <ta e="T270" id="Seg_3651" s="T269">iː-qɨl-pɔː-tet</ta>
            <ta e="T271" id="Seg_3652" s="T270">üt-qɨnɨ</ta>
            <ta e="T272" id="Seg_3653" s="T271">konnä</ta>
            <ta e="T274" id="Seg_3654" s="T273">čʼü-ntɔː-tɨt</ta>
            <ta e="T275" id="Seg_3655" s="T274">ponä</ta>
            <ta e="T276" id="Seg_3656" s="T275">tantɨ-lä</ta>
            <ta e="T277" id="Seg_3657" s="T276">nık</ta>
            <ta e="T278" id="Seg_3658" s="T277">kətɨ-ŋɨ-t</ta>
            <ta e="T279" id="Seg_3659" s="T278">konnä</ta>
            <ta e="T280" id="Seg_3660" s="T279">tantɨ-lä</ta>
            <ta e="T281" id="Seg_3661" s="T280">am-ɨ-r-ŋɨlɨt</ta>
            <ta e="T282" id="Seg_3662" s="T281">qɔːt-ɨ-nıː-sʼa</ta>
            <ta e="T283" id="Seg_3663" s="T282">müt-tɨ-lɨ-mɨt</ta>
            <ta e="T284" id="Seg_3664" s="T283">tentɨ-lä</ta>
            <ta e="T285" id="Seg_3665" s="T284">poː-qɨ</ta>
            <ta e="T286" id="Seg_3666" s="T285">üt-qɨnɨ</ta>
            <ta e="T287" id="Seg_3667" s="T286">konnä</ta>
            <ta e="T288" id="Seg_3668" s="T287">aj</ta>
            <ta e="T289" id="Seg_3669" s="T288">qaj</ta>
            <ta e="T290" id="Seg_3670" s="T289">meː-q-olam-nɨ-lɨt</ta>
            <ta e="T291" id="Seg_3671" s="T290">konnä</ta>
            <ta e="T292" id="Seg_3672" s="T291">tantɨ-lä</ta>
            <ta e="T293" id="Seg_3673" s="T292">am-ɨ-r-ŋɨlɨt</ta>
            <ta e="T294" id="Seg_3674" s="T293">a</ta>
            <ta e="T295" id="Seg_3675" s="T294">nı-ŋ</ta>
            <ta e="T296" id="Seg_3676" s="T295">ɛː-sä</ta>
            <ta e="T297" id="Seg_3677" s="T296">märqɨ</ta>
            <ta e="T298" id="Seg_3678" s="T297">timnʼa-tɨ</ta>
            <ta e="T299" id="Seg_3679" s="T298">konnä</ta>
            <ta e="T300" id="Seg_3680" s="T299">tantɨ-lä</ta>
            <ta e="T301" id="Seg_3681" s="T300">am-ɨ-r-lɨ-mɨt</ta>
            <ta e="T302" id="Seg_3682" s="T301">konna</ta>
            <ta e="T303" id="Seg_3683" s="T302">jep</ta>
            <ta e="T304" id="Seg_3684" s="T303">tat-tɔː-tet</ta>
            <ta e="T305" id="Seg_3685" s="T304">ɨntɨ-lʼ</ta>
            <ta e="T306" id="Seg_3686" s="T305">tıša-m-tɨt</ta>
            <ta e="T307" id="Seg_3687" s="T306">karrä-t</ta>
            <ta e="T308" id="Seg_3688" s="T307">antoː-qɨn-tɨt</ta>
            <ta e="T309" id="Seg_3689" s="T308">pil-lä</ta>
            <ta e="T310" id="Seg_3690" s="T309">qəːčʼ-al-nɔː-tɨt</ta>
            <ta e="T311" id="Seg_3691" s="T310">mɔːt-tɨ</ta>
            <ta e="T312" id="Seg_3692" s="T311">na</ta>
            <ta e="T313" id="Seg_3693" s="T312">jep</ta>
            <ta e="T314" id="Seg_3694" s="T313">šeːr-tɔː-tɨt</ta>
            <ta e="T315" id="Seg_3695" s="T314">mərqɨ</ta>
            <ta e="T316" id="Seg_3696" s="T315">laŋa-lʼ</ta>
            <ta e="T317" id="Seg_3697" s="T316">qum-ɨ-lʼ</ta>
            <ta e="T318" id="Seg_3698" s="T317">mɔːter</ta>
            <ta e="T319" id="Seg_3699" s="T318">palna-n</ta>
            <ta e="T320" id="Seg_3700" s="T319">optı</ta>
            <ta e="T321" id="Seg_3701" s="T320">ukkɨr</ta>
            <ta e="T322" id="Seg_3702" s="T321">qə-ntɨ</ta>
            <ta e="T323" id="Seg_3703" s="T322">na</ta>
            <ta e="T324" id="Seg_3704" s="T323">omnɨ-ntɔː-tet</ta>
            <ta e="T325" id="Seg_3705" s="T324">takɨ-lʼ</ta>
            <ta e="T326" id="Seg_3706" s="T325">ɔːtä-lʼ</ta>
            <ta e="T327" id="Seg_3707" s="T326">nʼoː-tɨlʼ</ta>
            <ta e="T328" id="Seg_3708" s="T327">timnʼa-ntɨ-sä</ta>
            <ta e="T329" id="Seg_3709" s="T328">aj</ta>
            <ta e="T330" id="Seg_3710" s="T329">ukkɨr</ta>
            <ta e="T331" id="Seg_3711" s="T330">qə-ntɨ</ta>
            <ta e="T332" id="Seg_3712" s="T331">na</ta>
            <ta e="T333" id="Seg_3713" s="T332">omnɨ-ntɔː-tɨt</ta>
            <ta e="T334" id="Seg_3714" s="T333">nɔːkɨr</ta>
            <ta e="T335" id="Seg_3715" s="T334">timnʼa-se-t</ta>
            <ta e="T336" id="Seg_3716" s="T335">muntɨk</ta>
            <ta e="T337" id="Seg_3717" s="T336">ukkɨr</ta>
            <ta e="T338" id="Seg_3718" s="T337">qö-nte</ta>
            <ta e="T339" id="Seg_3719" s="T338">omte-lɔː-tɨt</ta>
            <ta e="T340" id="Seg_3720" s="T339">nɨːn</ta>
            <ta e="T341" id="Seg_3721" s="T340">nık</ta>
            <ta e="T342" id="Seg_3722" s="T341">kətɨ-sɔː-tɨt</ta>
            <ta e="T343" id="Seg_3723" s="T342">paŋɨ-mɨt</ta>
            <ta e="T344" id="Seg_3724" s="T343">qaj</ta>
            <ta e="T345" id="Seg_3725" s="T344">əmɨltɨ-mnɨ-mɨt</ta>
            <ta e="T346" id="Seg_3726" s="T345">paŋɨ-lɨt</ta>
            <ta e="T347" id="Seg_3727" s="T346">ɛː-ŋa</ta>
            <ta e="T348" id="Seg_3728" s="T347">paŋɨ-sä</ta>
            <ta e="T349" id="Seg_3729" s="T348">šınet</ta>
            <ta e="T350" id="Seg_3730" s="T349">mi-ŋɨlɨt</ta>
            <ta e="T351" id="Seg_3731" s="T350">am-ɨ-r-qo</ta>
            <ta e="T352" id="Seg_3732" s="T351">sola-p</ta>
            <ta e="T353" id="Seg_3733" s="T352">meː-qɨl-lɨ-mɨt</ta>
            <ta e="T354" id="Seg_3734" s="T353">paŋɨ-m-tɨt</ta>
            <ta e="T355" id="Seg_3735" s="T354">muntɨk</ta>
            <ta e="T356" id="Seg_3736" s="T355">mi-ŋɔː-tɨt</ta>
            <ta e="T357" id="Seg_3737" s="T356">laŋa-lʼ</ta>
            <ta e="T358" id="Seg_3738" s="T357">qum-ɨ-lʼ</ta>
            <ta e="T359" id="Seg_3739" s="T358">nɔːkɨr</ta>
            <ta e="T360" id="Seg_3740" s="T359">timnʼa-sɨ-t</ta>
            <ta e="T361" id="Seg_3741" s="T360">a</ta>
            <ta e="T362" id="Seg_3742" s="T361">palna-lʼ</ta>
            <ta e="T363" id="Seg_3743" s="T362">nɔːkɨr</ta>
            <ta e="T364" id="Seg_3744" s="T363">timnʼa-sɨ-t</ta>
            <ta e="T365" id="Seg_3745" s="T364">okoːt</ta>
            <ta e="T366" id="Seg_3746" s="T365">jap</ta>
            <ta e="T367" id="Seg_3747" s="T366">əːtɨ-r-kɨ-sɔː-tɨt</ta>
            <ta e="T368" id="Seg_3748" s="T367">märqɨ</ta>
            <ta e="T369" id="Seg_3749" s="T368">timnʼa-tɨ</ta>
            <ta e="T370" id="Seg_3750" s="T369">kutar</ta>
            <ta e="T371" id="Seg_3751" s="T370">paŋɨ-sä</ta>
            <ta e="T372" id="Seg_3752" s="T371">qättɨ-ntɨ-tɨ</ta>
            <ta e="T373" id="Seg_3753" s="T372">təp-ɨ-t</ta>
            <ta e="T374" id="Seg_3754" s="T373">aj</ta>
            <ta e="T375" id="Seg_3755" s="T374">nılʼčʼɨ-k</ta>
            <ta e="T376" id="Seg_3756" s="T375">qättɨ-ntɔː-tɨt</ta>
            <ta e="T377" id="Seg_3757" s="T376">märqɨ</ta>
            <ta e="T378" id="Seg_3758" s="T377">ira</ta>
            <ta e="T379" id="Seg_3759" s="T378">ukkur</ta>
            <ta e="T380" id="Seg_3760" s="T379">čʼontoː-qɨt</ta>
            <ta e="T381" id="Seg_3761" s="T380">nanɨ</ta>
            <ta e="T382" id="Seg_3762" s="T381">matta</ta>
            <ta e="T383" id="Seg_3763" s="T382">saje-sa</ta>
            <ta e="T384" id="Seg_3764" s="T383">ija-p</ta>
            <ta e="T385" id="Seg_3765" s="T384">mɨrɔː-tɨ-t</ta>
            <ta e="T386" id="Seg_3766" s="T385">paŋɨ-sä</ta>
            <ta e="T387" id="Seg_3767" s="T386">nɨː</ta>
            <ta e="T388" id="Seg_3768" s="T387">jep</ta>
            <ta e="T389" id="Seg_3769" s="T388">qättɔː-tɨt</ta>
            <ta e="T390" id="Seg_3770" s="T389">laŋa-lʼ</ta>
            <ta e="T391" id="Seg_3771" s="T390">qum-ɨ-lʼ</ta>
            <ta e="T392" id="Seg_3772" s="T391">nɔːr</ta>
            <ta e="T393" id="Seg_3773" s="T392">timnʼä-s-t-ɨ-p</ta>
            <ta e="T394" id="Seg_3774" s="T393">taŋɨ-lʼ</ta>
            <ta e="T395" id="Seg_3775" s="T394">ɔːtä-lʼ</ta>
            <ta e="T396" id="Seg_3776" s="T395">nʼoː-tɨlʼ</ta>
            <ta e="T397" id="Seg_3777" s="T396">timnʼä-m-tɨ</ta>
            <ta e="T398" id="Seg_3778" s="T397">čʼap</ta>
            <ta e="T399" id="Seg_3779" s="T398">qättɨ-tɨ</ta>
            <ta e="T400" id="Seg_3780" s="T399">ola</ta>
            <ta e="T401" id="Seg_3781" s="T400">šünʼčʼi-pɨlʼ</ta>
            <ta e="T402" id="Seg_3782" s="T401">təttɨ-p</ta>
            <ta e="T403" id="Seg_3783" s="T402">qättɨ-tɨ</ta>
            <ta e="T404" id="Seg_3784" s="T403">palna-t</ta>
            <ta e="T405" id="Seg_3785" s="T404">timnʼä-tɨ</ta>
            <ta e="T406" id="Seg_3786" s="T405">taŋɨ-lʼ</ta>
            <ta e="T407" id="Seg_3787" s="T406">ɔːta-lʼ</ta>
            <ta e="T408" id="Seg_3788" s="T407">nʼoː-tɨlʼ</ta>
            <ta e="T409" id="Seg_3789" s="T408">tɨmnʼä-tɨ</ta>
            <ta e="T410" id="Seg_3790" s="T409">aj</ta>
            <ta e="T411" id="Seg_3791" s="T410">nɨːnɨ</ta>
            <ta e="T412" id="Seg_3792" s="T411">pakta</ta>
            <ta e="T413" id="Seg_3793" s="T412">nʼoː-la</ta>
            <ta e="T414" id="Seg_3794" s="T413">qən-tɨ-tɨ</ta>
            <ta e="T415" id="Seg_3795" s="T414">täːqa-m-tɨ</ta>
            <ta e="T416" id="Seg_3796" s="T415">orqɨl-lä</ta>
            <ta e="T417" id="Seg_3797" s="T416">nʼoː-tɨ-ŋɨ-t</ta>
            <ta e="T418" id="Seg_3798" s="T417">toːnna</ta>
            <ta e="T419" id="Seg_3799" s="T418">šitɨ</ta>
            <ta e="T420" id="Seg_3800" s="T419">tɨmnʼä-sɨ-qä</ta>
            <ta e="T421" id="Seg_3801" s="T420">mɔːt-qɨt</ta>
            <ta e="T422" id="Seg_3802" s="T421">qalɔː-tɨt</ta>
            <ta e="T423" id="Seg_3803" s="T422">qaj</ta>
            <ta e="T424" id="Seg_3804" s="T423">qən-nɨ-tɨ</ta>
            <ta e="T425" id="Seg_3805" s="T424">qaj</ta>
            <ta e="T426" id="Seg_3806" s="T425">qattɨ-jɔː-tɨt</ta>
            <ta e="T427" id="Seg_3807" s="T426">nʼoː-la</ta>
            <ta e="T428" id="Seg_3808" s="T427">qən-tɨ-tɨ-tɨ</ta>
            <ta e="T429" id="Seg_3809" s="T428">nʼennä-t</ta>
            <ta e="T430" id="Seg_3810" s="T429">mit-al-lä</ta>
            <ta e="T431" id="Seg_3811" s="T430">čʼap</ta>
            <ta e="T432" id="Seg_3812" s="T431">počʼ-ɔːl-nɨ-tɨ</ta>
            <ta e="T433" id="Seg_3813" s="T432">lʼakčʼi-tɨ</ta>
            <ta e="T434" id="Seg_3814" s="T433">pačʼ-al-nɨ-t</ta>
            <ta e="T435" id="Seg_3815" s="T434">qamtä</ta>
            <ta e="T436" id="Seg_3816" s="T435">alʼčʼa</ta>
            <ta e="T437" id="Seg_3817" s="T436">laŋa-lʼ</ta>
            <ta e="T438" id="Seg_3818" s="T437">qup</ta>
            <ta e="T439" id="Seg_3819" s="T438">šit-täl</ta>
            <ta e="T440" id="Seg_3820" s="T439">čʼap</ta>
            <ta e="T441" id="Seg_3821" s="T440">pačʼ-al-nɨ-t</ta>
            <ta e="T442" id="Seg_3822" s="T441">täːqa-t</ta>
            <ta e="T443" id="Seg_3823" s="T442">kə-t</ta>
            <ta e="T444" id="Seg_3824" s="T443">čʼoːmɨ-t</ta>
            <ta e="T445" id="Seg_3825" s="T444">orqɨl-nɨ-t</ta>
            <ta e="T446" id="Seg_3826" s="T445">topɨ-tɨ</ta>
            <ta e="T448" id="Seg_3827" s="T447">säpp-ɛː-mpa</ta>
            <ta e="T449" id="Seg_3828" s="T448">palna</ta>
            <ta e="T450" id="Seg_3829" s="T449">toːnna</ta>
            <ta e="T451" id="Seg_3830" s="T450">timnʼä-qɨ-m-ta</ta>
            <ta e="T452" id="Seg_3831" s="T451">qät-pa-tɨ</ta>
            <ta e="T453" id="Seg_3832" s="T452">timnʼä-ntɨ</ta>
            <ta e="T454" id="Seg_3833" s="T453">mättɨ-t</ta>
            <ta e="T455" id="Seg_3834" s="T454">šüː</ta>
            <ta e="T456" id="Seg_3835" s="T455">nʼennɨ</ta>
            <ta e="T457" id="Seg_3836" s="T456">paktɨ</ta>
            <ta e="T458" id="Seg_3837" s="T457">qo-ŋɨ-tɨ</ta>
            <ta e="T459" id="Seg_3838" s="T458">mompa</ta>
            <ta e="T460" id="Seg_3839" s="T459">orqɨlʼ-pe-tɨ</ta>
            <ta e="T461" id="Seg_3840" s="T460">mat</ta>
            <ta e="T462" id="Seg_3841" s="T461">tü-lɨ-la-k</ta>
            <ta e="T463" id="Seg_3842" s="T462">na</ta>
            <ta e="T464" id="Seg_3843" s="T463">küši-lʼ</ta>
            <ta e="T465" id="Seg_3844" s="T464">laka</ta>
            <ta e="T466" id="Seg_3845" s="T465">aj</ta>
            <ta e="T467" id="Seg_3846" s="T466">qaj</ta>
            <ta e="T468" id="Seg_3847" s="T467">meː-tɨ-tɨ</ta>
            <ta e="T469" id="Seg_3848" s="T468">mat</ta>
            <ta e="T470" id="Seg_3849" s="T469">tam-ɛnta-k</ta>
            <ta e="T471" id="Seg_3850" s="T470">nɨːnɨ</ta>
            <ta e="T472" id="Seg_3851" s="T471">nɔːkɨr</ta>
            <ta e="T473" id="Seg_3852" s="T472">tɨmnʼä-s-t-ɨ-p</ta>
            <ta e="T474" id="Seg_3853" s="T473">muntɨk</ta>
            <ta e="T475" id="Seg_3854" s="T474">qətt-ɛː-jɔː-tɨt</ta>
            <ta e="T476" id="Seg_3855" s="T475">okkɨr</ta>
            <ta e="T477" id="Seg_3856" s="T476">taː-t</ta>
            <ta e="T478" id="Seg_3857" s="T477">tättɨ-t</ta>
            <ta e="T479" id="Seg_3858" s="T478">čʼontoː-qɨt</ta>
            <ta e="T480" id="Seg_3859" s="T479">nanɨ</ta>
            <ta e="T481" id="Seg_3860" s="T480">ilɨ-mpɔː-tɨt</ta>
            <ta e="T482" id="Seg_3861" s="T481">nʼi</ta>
            <ta e="T483" id="Seg_3862" s="T482">qaj-i-t</ta>
            <ta e="T484" id="Seg_3863" s="T483">sümɨ</ta>
            <ta e="T485" id="Seg_3864" s="T484">čʼäːŋka</ta>
            <ta e="T486" id="Seg_3865" s="T485">qən-ŋɔː-tɨt</ta>
            <ta e="T487" id="Seg_3866" s="T486">laŋa-lʼ</ta>
            <ta e="T488" id="Seg_3867" s="T487">qum-ɨ-lʼ</ta>
            <ta e="T489" id="Seg_3868" s="T488">imaqota-lʼ</ta>
            <ta e="T490" id="Seg_3869" s="T489">nılʼčʼi-k</ta>
            <ta e="T491" id="Seg_3870" s="T490">tɛnɨ-r-na</ta>
            <ta e="T492" id="Seg_3871" s="T491">ija-iː-mɨ</ta>
            <ta e="T493" id="Seg_3872" s="T492">qəs-sɔː-tɨt</ta>
            <ta e="T494" id="Seg_3873" s="T493">meːltɨt</ta>
            <ta e="T495" id="Seg_3874" s="T494">qoja</ta>
            <ta e="T496" id="Seg_3875" s="T495">mɔːt-ɨ-ŋ-nɔː-tɨt</ta>
            <ta e="T497" id="Seg_3876" s="T496">meːlle</ta>
            <ta e="T498" id="Seg_3877" s="T497">mɨta</ta>
            <ta e="T499" id="Seg_3878" s="T498">šölʼqup</ta>
            <ta e="T500" id="Seg_3879" s="T499">ija-m-tɨ</ta>
            <ta e="T501" id="Seg_3880" s="T500">namɨ-t</ta>
            <ta e="T502" id="Seg_3881" s="T501">mɔːntɨ</ta>
            <ta e="T503" id="Seg_3882" s="T502">soma-k</ta>
            <ta e="T504" id="Seg_3883" s="T503">kətɨ-mpa-tɨ</ta>
            <ta e="T505" id="Seg_3884" s="T504">mat</ta>
            <ta e="T507" id="Seg_3885" s="T506">ija-mɨ</ta>
            <ta e="T508" id="Seg_3886" s="T507">kəːtɨ-ptä-ke-k</ta>
            <ta e="T509" id="Seg_3887" s="T508">nüma-nɨ</ta>
            <ta e="T510" id="Seg_3888" s="T509">ukɨ-p</ta>
            <ta e="T511" id="Seg_3889" s="T510">aj</ta>
            <ta e="T512" id="Seg_3890" s="T511">musɨltɨ-ka-p</ta>
            <ta e="T513" id="Seg_3891" s="T512">šölʼqum-ɨ-lʼ</ta>
            <ta e="T514" id="Seg_3892" s="T513">ima</ta>
            <ta e="T515" id="Seg_3893" s="T514">meːlle</ta>
            <ta e="T516" id="Seg_3894" s="T515">qəː</ta>
            <ta e="T517" id="Seg_3895" s="T516">ɛː-ŋa</ta>
            <ta e="T518" id="Seg_3896" s="T517">mannoː-nɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_3897" s="T0">palna</ta>
            <ta e="T2" id="Seg_3898" s="T1">palna</ta>
            <ta e="T3" id="Seg_3899" s="T2">nɔːkɨr</ta>
            <ta e="T4" id="Seg_3900" s="T3">timnʼa-sɨ-t</ta>
            <ta e="T5" id="Seg_3901" s="T4">ilɨ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T6" id="Seg_3902" s="T5">šölʼqum-ɨ-lʼ</ta>
            <ta e="T7" id="Seg_3903" s="T6">mɔːtɨr-ɨ-lʼ</ta>
            <ta e="T8" id="Seg_3904" s="T7">ira</ta>
            <ta e="T9" id="Seg_3905" s="T8">ɛː-mpɨ-mpɨ-ntɨ</ta>
            <ta e="T10" id="Seg_3906" s="T9">orɨ-sɨma-lʼ</ta>
            <ta e="T11" id="Seg_3907" s="T10">ira</ta>
            <ta e="T12" id="Seg_3908" s="T11">samɨj</ta>
            <ta e="T13" id="Seg_3909" s="T12">wərqɨ</ta>
            <ta e="T14" id="Seg_3910" s="T13">timnʼa-t-ɨ-n</ta>
            <ta e="T15" id="Seg_3911" s="T14">čʼontɨ-qɨn-lʼ</ta>
            <ta e="T16" id="Seg_3912" s="T15">timnʼa-t-ɨ-n</ta>
            <ta e="T17" id="Seg_3913" s="T16">taŋɨ-lʼ</ta>
            <ta e="T18" id="Seg_3914" s="T17">ɔːtä-lʼ</ta>
            <ta e="T19" id="Seg_3915" s="T18">nʼoː-ntɨlʼ</ta>
            <ta e="T20" id="Seg_3916" s="T19">ɛː-mpɨ-mpɨ-ntɨ</ta>
            <ta e="T21" id="Seg_3917" s="T20">poːsɨ</ta>
            <ta e="T22" id="Seg_3918" s="T21">kɨpa</ta>
            <ta e="T23" id="Seg_3919" s="T22">timnʼa-tɨ</ta>
            <ta e="T24" id="Seg_3920" s="T23">kərnʼa-n</ta>
            <ta e="T25" id="Seg_3921" s="T24">pɔːrɨ-lʼ</ta>
            <ta e="T26" id="Seg_3922" s="T25">nʼoː-ntɨlʼ</ta>
            <ta e="T27" id="Seg_3923" s="T26">ɛː-mpɨ-mpɨ-ntɨ</ta>
            <ta e="T28" id="Seg_3924" s="T27">nılʼčʼɨ-k</ta>
            <ta e="T29" id="Seg_3925" s="T28">ilɨ-mpɨ-tɨt</ta>
            <ta e="T30" id="Seg_3926" s="T29">a</ta>
            <ta e="T31" id="Seg_3927" s="T30">na</ta>
            <ta e="T32" id="Seg_3928" s="T31">palna-lʼ</ta>
            <ta e="T33" id="Seg_3929" s="T32">wərqɨ</ta>
            <ta e="T34" id="Seg_3930" s="T33">timnʼa-tɨ</ta>
            <ta e="T35" id="Seg_3931" s="T34">namɨššak</ta>
            <ta e="T36" id="Seg_3932" s="T35">qəːtɨ-sä</ta>
            <ta e="T37" id="Seg_3933" s="T36">ɛː-mpɨ-mpɨ-ntɨ</ta>
            <ta e="T38" id="Seg_3934" s="T37">kunɨ</ta>
            <ta e="T39" id="Seg_3935" s="T38">qaj</ta>
            <ta e="T40" id="Seg_3936" s="T39">tü-ɛntɨ</ta>
            <ta e="T41" id="Seg_3937" s="T40">muntɨk</ta>
            <ta e="T42" id="Seg_3938" s="T41">tɛnɨmɨ-mpɨ-tɨ</ta>
            <ta e="T43" id="Seg_3939" s="T42">suːmpɨ-ntɨlʼ</ta>
            <ta e="T44" id="Seg_3940" s="T43">təːtɨpɨ-lʼ</ta>
            <ta e="T45" id="Seg_3941" s="T44">ira</ta>
            <ta e="T46" id="Seg_3942" s="T45">ɛː-mpɨ-mpɨ-ntɨ</ta>
            <ta e="T47" id="Seg_3943" s="T46">kəːsɨ-m</ta>
            <ta e="T48" id="Seg_3944" s="T47">meː-ntɨ-mpɨ-tɨ</ta>
            <ta e="T49" id="Seg_3945" s="T48">ontɨ</ta>
            <ta e="T50" id="Seg_3946" s="T49">ilɨ-ptäː-qɨn-ntɨ</ta>
            <ta e="T51" id="Seg_3947" s="T50">čʼɔːtɨ-r-ŋɨ</ta>
            <ta e="T52" id="Seg_3948" s="T51">kəːsɨ-m</ta>
            <ta e="T53" id="Seg_3949" s="T52">nık</ta>
            <ta e="T54" id="Seg_3950" s="T53">meː-ntɨ-mpɨ-tɨ</ta>
            <ta e="T55" id="Seg_3951" s="T54">čʼaŋak</ta>
            <ta e="T56" id="Seg_3952" s="T55">pü-t-ɨ-n</ta>
            <ta e="T57" id="Seg_3953" s="T56">a</ta>
            <ta e="T58" id="Seg_3954" s="T57">ruš-ɨ-t</ta>
            <ta e="T59" id="Seg_3955" s="T58">meː-ntɨ-mpɨ-tɨt</ta>
            <ta e="T60" id="Seg_3956" s="T59">aššă</ta>
            <ta e="T61" id="Seg_3957" s="T60">tɛnɨmɨ-m</ta>
            <ta e="T62" id="Seg_3958" s="T61">qaj-ɨ-n</ta>
            <ta e="T63" id="Seg_3959" s="T62">*nɔː-nɨ</ta>
            <ta e="T65" id="Seg_3960" s="T64">təp</ta>
            <ta e="T66" id="Seg_3961" s="T65">meː-ntɨ-mpɨ-tɨ</ta>
            <ta e="T67" id="Seg_3962" s="T66">kəːsɨ-tqo</ta>
            <ta e="T68" id="Seg_3963" s="T67">pü-m</ta>
            <ta e="T69" id="Seg_3964" s="T68">meː-ntɨ-mpɨ-tɨ</ta>
            <ta e="T70" id="Seg_3965" s="T69">na</ta>
            <ta e="T71" id="Seg_3966" s="T70">pü-n</ta>
            <ta e="T72" id="Seg_3967" s="T71">*nɔː-nɨ</ta>
            <ta e="T73" id="Seg_3968" s="T72">kəːsɨ-lʼ</ta>
            <ta e="T74" id="Seg_3969" s="T73">porqɨ-m</ta>
            <ta e="T75" id="Seg_3970" s="T74">meː-ntɨ-mpɨ-tɨ</ta>
            <ta e="T77" id="Seg_3971" s="T76">kəːsɨ-lʼ</ta>
            <ta e="T78" id="Seg_3972" s="T77">ɨntɨ</ta>
            <ta e="T79" id="Seg_3973" s="T78">meː-ntɨ-mpɨ-tɨ</ta>
            <ta e="T81" id="Seg_3974" s="T80">kəːsɨ-lʼ</ta>
            <ta e="T82" id="Seg_3975" s="T81">koma-m</ta>
            <ta e="T83" id="Seg_3976" s="T82">meː-ntɨ-mpɨ-ntɨ-tɨ</ta>
            <ta e="T84" id="Seg_3977" s="T83">na-ntɨ-sä</ta>
            <ta e="T85" id="Seg_3978" s="T84">aj</ta>
            <ta e="T86" id="Seg_3979" s="T85">ilɨ-mpɨ</ta>
            <ta e="T87" id="Seg_3980" s="T86">a</ta>
            <ta e="T88" id="Seg_3981" s="T87">mütɨ-tɨ-lä</ta>
            <ta e="T89" id="Seg_3982" s="T88">ašša</ta>
            <ta e="T90" id="Seg_3983" s="T89">ičʼčʼɨ-r-mpɨ</ta>
            <ta e="T91" id="Seg_3984" s="T90">təp-ɨ-nkinı</ta>
            <ta e="T92" id="Seg_3985" s="T91">mütɨ-tɨ-lä</ta>
            <ta e="T93" id="Seg_3986" s="T92">ičʼčʼɨ-r-mpɨ-tɨt</ta>
            <ta e="T95" id="Seg_3987" s="T94">kunɨ</ta>
            <ta e="T96" id="Seg_3988" s="T95">čʼam</ta>
            <ta e="T97" id="Seg_3989" s="T96">tü-ɛntɨ-tɨt</ta>
            <ta e="T98" id="Seg_3990" s="T97">təp</ta>
            <ta e="T99" id="Seg_3991" s="T98">muntɨk</ta>
            <ta e="T100" id="Seg_3992" s="T99">tɛnɨmɨ-tɨ</ta>
            <ta e="T101" id="Seg_3993" s="T100">qälɨk-t</ta>
            <ta e="T102" id="Seg_3994" s="T101">ičʼčʼɨ-r-mpɨ-tɨt</ta>
            <ta e="T103" id="Seg_3995" s="T102">mütɨ-tɨ-qo</ta>
            <ta e="T104" id="Seg_3996" s="T103">laŋa-lʼ</ta>
            <ta e="T105" id="Seg_3997" s="T104">qum-ɨ-n</ta>
            <ta e="T106" id="Seg_3998" s="T105">pɛläk-qɨn</ta>
            <ta e="T107" id="Seg_3999" s="T106">aj</ta>
            <ta e="T108" id="Seg_4000" s="T107">nɔːkɨr</ta>
            <ta e="T109" id="Seg_4001" s="T108">timnʼa-sɨ-t</ta>
            <ta e="T110" id="Seg_4002" s="T109">ɛː-mpɨ-ntɨ-tɨt</ta>
            <ta e="T111" id="Seg_4003" s="T110">aj</ta>
            <ta e="T112" id="Seg_4004" s="T111">peː-lä</ta>
            <ta e="T113" id="Seg_4005" s="T112">ičʼčʼɨ-r-mpɨ-tɨt</ta>
            <ta e="T114" id="Seg_4006" s="T113">tamčʼeːlɨ</ta>
            <ta e="T115" id="Seg_4007" s="T114">qum-ɨ-lʼ</ta>
            <ta e="T116" id="Seg_4008" s="T115">nɔːkɨr</ta>
            <ta e="T117" id="Seg_4009" s="T116">timnʼa-sɨ-t-ɨ-m</ta>
            <ta e="T118" id="Seg_4010" s="T117">təp</ta>
            <ta e="T119" id="Seg_4011" s="T118">ilɨ-mpɨ</ta>
            <ta e="T120" id="Seg_4012" s="T119">ukoːn</ta>
            <ta e="T121" id="Seg_4013" s="T120">pü-lʼ-čʼɔːntɨs</ta>
            <ta e="T122" id="Seg_4014" s="T121">qoltɨ-qɨn</ta>
            <ta e="T123" id="Seg_4015" s="T122">turuhanska</ta>
            <ta e="T124" id="Seg_4016" s="T123">kəralʼ</ta>
            <ta e="T125" id="Seg_4017" s="T124">pɛläk-qɨn</ta>
            <ta e="T126" id="Seg_4018" s="T125">čʼuːrɨ-qɨn</ta>
            <ta e="T127" id="Seg_4019" s="T126">laŋa-lʼ</ta>
            <ta e="T128" id="Seg_4020" s="T127">qum-ɨ-lʼ</ta>
            <ta e="T129" id="Seg_4021" s="T128">nɔːkɨr</ta>
            <ta e="T130" id="Seg_4022" s="T129">timnʼa-sɨ-t</ta>
            <ta e="T131" id="Seg_4023" s="T130">ukkɨr</ta>
            <ta e="T132" id="Seg_4024" s="T131">tɔːt</ta>
            <ta e="T133" id="Seg_4025" s="T132">čʼontɨ-qɨn</ta>
            <ta e="T134" id="Seg_4026" s="T133">nılʼčʼɨ-k</ta>
            <ta e="T135" id="Seg_4027" s="T134">ɛː-sɨ-tɨt</ta>
            <ta e="T136" id="Seg_4028" s="T135">palna-lʼ</ta>
            <ta e="T137" id="Seg_4029" s="T136">nɔːkɨr</ta>
            <ta e="T138" id="Seg_4030" s="T137">timnʼa-sɨ-t-ɨ-m</ta>
            <ta e="T139" id="Seg_4031" s="T138">peː-lä</ta>
            <ta e="T140" id="Seg_4032" s="T139">qən-qo-olam-ŋɨ-tɨt</ta>
            <ta e="T141" id="Seg_4033" s="T140">a</ta>
            <ta e="T142" id="Seg_4034" s="T141">palna-lʼ</ta>
            <ta e="T143" id="Seg_4035" s="T142">timnʼa-sɨ-t</ta>
            <ta e="T144" id="Seg_4036" s="T143">qən-tɨt</ta>
            <ta e="T145" id="Seg_4037" s="T144">timnʼa-iː-qɨn-ntɨ</ta>
            <ta e="T146" id="Seg_4038" s="T145">nılʼčʼɨ-k</ta>
            <ta e="T147" id="Seg_4039" s="T146">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T148" id="Seg_4040" s="T147">laŋa-lʼ</ta>
            <ta e="T149" id="Seg_4041" s="T148">qum-ɨ-lʼ</ta>
            <ta e="T150" id="Seg_4042" s="T149">nɔːkɨr</ta>
            <ta e="T151" id="Seg_4043" s="T150">timnʼa-sɨ-t</ta>
            <ta e="T152" id="Seg_4044" s="T151">na</ta>
            <ta e="T153" id="Seg_4045" s="T152">tü-qo-olam-ntɨ-tɨt</ta>
            <ta e="T154" id="Seg_4046" s="T153">mütɨ-tɨ-lä</ta>
            <ta e="T155" id="Seg_4047" s="T154">meːqɨnʼɨn</ta>
            <ta e="T156" id="Seg_4048" s="T155">karrä</ta>
            <ta e="T157" id="Seg_4049" s="T156">čʼuːrɨ-ntɨ</ta>
            <ta e="T158" id="Seg_4050" s="T157">mɔːt-ɨ-k-lä-mɨt</ta>
            <ta e="T159" id="Seg_4051" s="T158">čʼuːrɨ-ntɨ</ta>
            <ta e="T160" id="Seg_4052" s="T159">mɔːt-ɨ-k-ɛntɨ-mɨt</ta>
            <ta e="T161" id="Seg_4053" s="T160">apsɨ-m</ta>
            <ta e="T162" id="Seg_4054" s="T161">qən-tɨ-ɛntɨ-mɨt</ta>
            <ta e="T163" id="Seg_4055" s="T162">qum-ɨ-t</ta>
            <ta e="T164" id="Seg_4056" s="T163">kosti</ta>
            <ta e="T165" id="Seg_4057" s="T164">mɔːt-tɨ-lä</ta>
            <ta e="T166" id="Seg_4058" s="T165">tü-ɛntɨ-tɨt</ta>
            <ta e="T167" id="Seg_4059" s="T166">am-ɨ-r-ɛntɨ-tɨt</ta>
            <ta e="T168" id="Seg_4060" s="T167">karrä</ta>
            <ta e="T169" id="Seg_4061" s="T168">qən-ŋɨ-tɨt</ta>
            <ta e="T170" id="Seg_4062" s="T169">čʼuːrɨ-ntɨ</ta>
            <ta e="T171" id="Seg_4063" s="T170">kərɨ-tɨ-tɨt</ta>
            <ta e="T172" id="Seg_4064" s="T171">mɔːt-ɨ-k-ŋɨ-tɨt</ta>
            <ta e="T173" id="Seg_4065" s="T172">nʼuːtɨ-sä</ta>
            <ta e="T174" id="Seg_4066" s="T173">tɔːqqɨ-tɨt</ta>
            <ta e="T175" id="Seg_4067" s="T174">mɔːt-ntɨ</ta>
            <ta e="T176" id="Seg_4068" s="T175">to-lʼ</ta>
            <ta e="T177" id="Seg_4069" s="T176">pɛläk-qɨn</ta>
            <ta e="T178" id="Seg_4070" s="T177">tö-sä</ta>
            <ta e="T179" id="Seg_4071" s="T178">tɔːqqɨ-tɨt</ta>
            <ta e="T180" id="Seg_4072" s="T179">ınnä-nɨ</ta>
            <ta e="T181" id="Seg_4073" s="T180">ıllä</ta>
            <ta e="T182" id="Seg_4074" s="T181">aj</ta>
            <ta e="T183" id="Seg_4075" s="T182">nʼuːtɨ-sä</ta>
            <ta e="T184" id="Seg_4076" s="T183">tɔːqqɨ-tɨt</ta>
            <ta e="T185" id="Seg_4077" s="T184">timnʼa-qı-ntɨ-nkinı</ta>
            <ta e="T186" id="Seg_4078" s="T185">kaša</ta>
            <ta e="T187" id="Seg_4079" s="T186">avsa</ta>
            <ta e="T188" id="Seg_4080" s="T187">mušɨ-rɨ-ŋɨlɨt</ta>
            <ta e="T189" id="Seg_4081" s="T188">tamčʼeːlɨ</ta>
            <ta e="T190" id="Seg_4082" s="T189">tü-ɛntɨ-tɨt</ta>
            <ta e="T191" id="Seg_4083" s="T190">ür-ɨ-lʼ</ta>
            <ta e="T192" id="Seg_4084" s="T191">pirnʼa-m</ta>
            <ta e="T193" id="Seg_4085" s="T192">mɔːt-ntɨ</ta>
            <ta e="T194" id="Seg_4086" s="T193">tul-tɨ-ŋɨlɨt</ta>
            <ta e="T195" id="Seg_4087" s="T194">kaša-m</ta>
            <ta e="T196" id="Seg_4088" s="T195">avsa-m</ta>
            <ta e="T197" id="Seg_4089" s="T196">karrä</ta>
            <ta e="T198" id="Seg_4090" s="T197">mušɨ-rɨ-tɨt</ta>
            <ta e="T199" id="Seg_4091" s="T198">nanɨ</ta>
            <ta e="T200" id="Seg_4092" s="T199">na</ta>
            <ta e="T201" id="Seg_4093" s="T200">ɔːmtɨ-tɨt</ta>
            <ta e="T202" id="Seg_4094" s="T201">čʼeːlɨ-tɨ</ta>
            <ta e="T203" id="Seg_4095" s="T202">čʼeːlɨ-lʼ</ta>
            <ta e="T204" id="Seg_4096" s="T203">čʼontɨ-lʼ</ta>
            <ta e="T205" id="Seg_4097" s="T204">kotä-ntɨ</ta>
            <ta e="T206" id="Seg_4098" s="T205">*korɨ-mɔːt-ŋɨ</ta>
            <ta e="T207" id="Seg_4099" s="T206">nʼennä-ntɨ</ta>
            <ta e="T208" id="Seg_4100" s="T207">antɨ-ıːra</ta>
            <ta e="T209" id="Seg_4101" s="T208">qontɨ-š-ɛː</ta>
            <ta e="T210" id="Seg_4102" s="T209">nɔːkɨr</ta>
            <ta e="T211" id="Seg_4103" s="T210">qum-ɨ-t</ta>
            <ta e="T212" id="Seg_4104" s="T211">tıpɨnʼa-ɨ-lʼ</ta>
            <ta e="T213" id="Seg_4105" s="T212">antɨ</ta>
            <ta e="T214" id="Seg_4106" s="T213">qontɨ-š-ɛː-ntɨ</ta>
            <ta e="T215" id="Seg_4107" s="T214">timnʼa-iː-qɨn-ntɨ</ta>
            <ta e="T216" id="Seg_4108" s="T215">nılʼčʼɨ-k</ta>
            <ta e="T217" id="Seg_4109" s="T216">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T218" id="Seg_4110" s="T217">paŋɨ-lʼ</ta>
            <ta e="T219" id="Seg_4111" s="T218">täːqa-lʼa-t</ta>
            <ta e="T220" id="Seg_4112" s="T219">ıllä</ta>
            <ta e="T221" id="Seg_4113" s="T220">əttɨ-qɨl-ŋɨlɨt</ta>
            <ta e="T222" id="Seg_4114" s="T221">peːmɨ-n</ta>
            <ta e="T223" id="Seg_4115" s="T222">küːtɨ-ɨ-n</ta>
            <ta e="T224" id="Seg_4116" s="T223">*šüː</ta>
            <ta e="T225" id="Seg_4117" s="T224">saqɨ-ätɔːl-ɛː-ŋɨlɨt</ta>
            <ta e="T226" id="Seg_4118" s="T225">nɨːnɨ</ta>
            <ta e="T227" id="Seg_4119" s="T226">nılʼčʼɨ-k</ta>
            <ta e="T228" id="Seg_4120" s="T227">kətɨ-ŋɨlɨt</ta>
            <ta e="T229" id="Seg_4121" s="T228">paŋɨ-mɨt</ta>
            <ta e="T230" id="Seg_4122" s="T229">čʼäːŋkɨ</ta>
            <ta e="T231" id="Seg_4123" s="T230">paŋɨ-lɨt</ta>
            <ta e="T232" id="Seg_4124" s="T231">meːqɨnʼɨn</ta>
            <ta e="T233" id="Seg_4125" s="T232">mi-ŋɨlɨt</ta>
            <ta e="T234" id="Seg_4126" s="T233">solaŋ-m</ta>
            <ta e="T235" id="Seg_4127" s="T234">meː-qɨl-lʼčʼɨ-lä-mɨt</ta>
            <ta e="T236" id="Seg_4128" s="T235">am-ɨ-r-qɨnɨtqo</ta>
            <ta e="T237" id="Seg_4129" s="T236">nık</ta>
            <ta e="T238" id="Seg_4130" s="T237">əttɨ-r-kkɨ-ŋɨ-tɨ</ta>
            <ta e="T239" id="Seg_4131" s="T238">kɨpa</ta>
            <ta e="T240" id="Seg_4132" s="T239">timnʼa-tɨ</ta>
            <ta e="T241" id="Seg_4133" s="T240">sɨtkɨ-qɨn</ta>
            <ta e="T242" id="Seg_4134" s="T241">ilɨ</ta>
            <ta e="T243" id="Seg_4135" s="T242">täːqa-m-tɨ</ta>
            <ta e="T244" id="Seg_4136" s="T243">nɨː</ta>
            <ta e="T245" id="Seg_4137" s="T244">ınnä</ta>
            <ta e="T246" id="Seg_4138" s="T245">čʼoqqo-lɨ-ŋɨ-tɨ</ta>
            <ta e="T247" id="Seg_4139" s="T246">ınnä-n</ta>
            <ta e="T248" id="Seg_4140" s="T247">tü-n</ta>
            <ta e="T249" id="Seg_4141" s="T248">poːrɨ-lʼ</ta>
            <ta e="T250" id="Seg_4142" s="T249">poː</ta>
            <ta e="T251" id="Seg_4143" s="T250">sɔːrɨ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T252" id="Seg_4144" s="T251">nık</ta>
            <ta e="T253" id="Seg_4145" s="T252">qo-ŋɨ-tɨt</ta>
            <ta e="T254" id="Seg_4146" s="T253">nʼennä-ntɨ</ta>
            <ta e="T255" id="Seg_4147" s="T254">qontɨ-š-ɛː-ŋɨ</ta>
            <ta e="T256" id="Seg_4148" s="T255">nɔːkɨr</ta>
            <ta e="T257" id="Seg_4149" s="T256">qum-ɨ-t</ta>
            <ta e="T258" id="Seg_4150" s="T257">tıpɨnʼa-ɨ-lʼ</ta>
            <ta e="T259" id="Seg_4151" s="T258">antɨ</ta>
            <ta e="T260" id="Seg_4152" s="T259">timnʼa-qı-m-tɨ</ta>
            <ta e="T261" id="Seg_4153" s="T260">əːtɨ-rɨ-kkɨ-qo-olam-ŋɨ-tɨ</ta>
            <ta e="T262" id="Seg_4154" s="T261">ponä</ta>
            <ta e="T263" id="Seg_4155" s="T262">na</ta>
            <ta e="T264" id="Seg_4156" s="T263">čʼam</ta>
            <ta e="T265" id="Seg_4157" s="T264">tantɨ-ntɨ-tɨt</ta>
            <ta e="T266" id="Seg_4158" s="T265">montɨ</ta>
            <ta e="T267" id="Seg_4159" s="T266">ɨntɨ-lʼ</ta>
            <ta e="T268" id="Seg_4160" s="T267">tıšša-m</ta>
            <ta e="T269" id="Seg_4161" s="T268">ınnä</ta>
            <ta e="T270" id="Seg_4162" s="T269">iː-qɨl-mpɨ-tɨt</ta>
            <ta e="T271" id="Seg_4163" s="T270">üt-qɨnɨ</ta>
            <ta e="T272" id="Seg_4164" s="T271">konnä</ta>
            <ta e="T274" id="Seg_4165" s="T273">čʼü-ntɨ-tɨt</ta>
            <ta e="T275" id="Seg_4166" s="T274">ponä</ta>
            <ta e="T276" id="Seg_4167" s="T275">tantɨ-lä</ta>
            <ta e="T277" id="Seg_4168" s="T276">nık</ta>
            <ta e="T278" id="Seg_4169" s="T277">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T279" id="Seg_4170" s="T278">konnä</ta>
            <ta e="T280" id="Seg_4171" s="T279">tantɨ-lä</ta>
            <ta e="T281" id="Seg_4172" s="T280">am-ɨ-r-ŋɨlɨt</ta>
            <ta e="T282" id="Seg_4173" s="T281">qɔːt-ɨ-nıː-sä</ta>
            <ta e="T283" id="Seg_4174" s="T282">mütɨ-tɨ-lä-mɨt</ta>
            <ta e="T284" id="Seg_4175" s="T283">tantɨ-lä</ta>
            <ta e="T285" id="Seg_4176" s="T284">poː-qɨn</ta>
            <ta e="T286" id="Seg_4177" s="T285">üt-qɨnɨ</ta>
            <ta e="T287" id="Seg_4178" s="T286">konnä</ta>
            <ta e="T288" id="Seg_4179" s="T287">aj</ta>
            <ta e="T289" id="Seg_4180" s="T288">qaj</ta>
            <ta e="T290" id="Seg_4181" s="T289">meː-qo-olam-ŋɨ-lɨt</ta>
            <ta e="T291" id="Seg_4182" s="T290">konnä</ta>
            <ta e="T292" id="Seg_4183" s="T291">tantɨ-lä</ta>
            <ta e="T293" id="Seg_4184" s="T292">am-ɨ-r-ŋɨlɨt</ta>
            <ta e="T294" id="Seg_4185" s="T293">a</ta>
            <ta e="T295" id="Seg_4186" s="T294">nılʼčʼɨ-k</ta>
            <ta e="T296" id="Seg_4187" s="T295">ɛː-sɨ</ta>
            <ta e="T297" id="Seg_4188" s="T296">wərqɨ</ta>
            <ta e="T298" id="Seg_4189" s="T297">timnʼa-tɨ</ta>
            <ta e="T299" id="Seg_4190" s="T298">konnä</ta>
            <ta e="T300" id="Seg_4191" s="T299">tantɨ-lä</ta>
            <ta e="T301" id="Seg_4192" s="T300">am-ɨ-r-lä-mɨt</ta>
            <ta e="T302" id="Seg_4193" s="T301">konnä</ta>
            <ta e="T303" id="Seg_4194" s="T302">jam</ta>
            <ta e="T304" id="Seg_4195" s="T303">taːtɨ-tɨ-tɨt</ta>
            <ta e="T305" id="Seg_4196" s="T304">ɨntɨ-lʼ</ta>
            <ta e="T306" id="Seg_4197" s="T305">tıšša-m-tɨt</ta>
            <ta e="T307" id="Seg_4198" s="T306">karrä-n</ta>
            <ta e="T308" id="Seg_4199" s="T307">antɨ-qɨn-ntɨt</ta>
            <ta e="T309" id="Seg_4200" s="T308">pin-lä</ta>
            <ta e="T310" id="Seg_4201" s="T309">qəːčʼɨ-ätɔːl-ŋɨ-tɨt</ta>
            <ta e="T311" id="Seg_4202" s="T310">mɔːt-ntɨ</ta>
            <ta e="T312" id="Seg_4203" s="T311">na</ta>
            <ta e="T313" id="Seg_4204" s="T312">jam</ta>
            <ta e="T314" id="Seg_4205" s="T313">šeːr-ntɨ-tɨt</ta>
            <ta e="T315" id="Seg_4206" s="T314">wərqɨ</ta>
            <ta e="T316" id="Seg_4207" s="T315">laŋa-lʼ</ta>
            <ta e="T317" id="Seg_4208" s="T316">qum-ɨ-lʼ</ta>
            <ta e="T318" id="Seg_4209" s="T317">mɔːtɨr</ta>
            <ta e="T319" id="Seg_4210" s="T318">palna-n</ta>
            <ta e="T320" id="Seg_4211" s="T319">əptı</ta>
            <ta e="T321" id="Seg_4212" s="T320">ukkɨr</ta>
            <ta e="T322" id="Seg_4213" s="T321">qö-ntɨ</ta>
            <ta e="T323" id="Seg_4214" s="T322">na</ta>
            <ta e="T324" id="Seg_4215" s="T323">omtɨ-ntɨ-tɨt</ta>
            <ta e="T325" id="Seg_4216" s="T324">takkɨ-lʼ</ta>
            <ta e="T326" id="Seg_4217" s="T325">ɔːtä-lʼ</ta>
            <ta e="T327" id="Seg_4218" s="T326">nʼoː-ntɨlʼ</ta>
            <ta e="T328" id="Seg_4219" s="T327">timnʼa-ntɨ-sä</ta>
            <ta e="T329" id="Seg_4220" s="T328">aj</ta>
            <ta e="T330" id="Seg_4221" s="T329">ukkɨr</ta>
            <ta e="T331" id="Seg_4222" s="T330">qö-ntɨ</ta>
            <ta e="T332" id="Seg_4223" s="T331">na</ta>
            <ta e="T333" id="Seg_4224" s="T332">omtɨ-ntɨ-tɨt</ta>
            <ta e="T334" id="Seg_4225" s="T333">nɔːkɨr</ta>
            <ta e="T335" id="Seg_4226" s="T334">timnʼa-sɨ-t</ta>
            <ta e="T336" id="Seg_4227" s="T335">muntɨk</ta>
            <ta e="T337" id="Seg_4228" s="T336">ukkɨr</ta>
            <ta e="T338" id="Seg_4229" s="T337">qö-ntɨ</ta>
            <ta e="T339" id="Seg_4230" s="T338">omtɨ-lä-tɨt</ta>
            <ta e="T340" id="Seg_4231" s="T339">nɨːnɨ</ta>
            <ta e="T341" id="Seg_4232" s="T340">nık</ta>
            <ta e="T342" id="Seg_4233" s="T341">kətɨ-sɨ-tɨt</ta>
            <ta e="T343" id="Seg_4234" s="T342">paŋɨ-mɨt</ta>
            <ta e="T344" id="Seg_4235" s="T343">qaj</ta>
            <ta e="T345" id="Seg_4236" s="T344">əmɨltɨ-mpɨ-mɨt</ta>
            <ta e="T346" id="Seg_4237" s="T345">paŋɨ-lɨt</ta>
            <ta e="T347" id="Seg_4238" s="T346">ɛː-ŋɨ</ta>
            <ta e="T348" id="Seg_4239" s="T347">paŋɨ-sä</ta>
            <ta e="T349" id="Seg_4240" s="T348">meːšımɨt</ta>
            <ta e="T350" id="Seg_4241" s="T349">mi-ŋɨlɨt</ta>
            <ta e="T351" id="Seg_4242" s="T350">am-ɨ-r-qo</ta>
            <ta e="T352" id="Seg_4243" s="T351">solaŋ-m</ta>
            <ta e="T353" id="Seg_4244" s="T352">meː-qɨl-lä-mɨt</ta>
            <ta e="T354" id="Seg_4245" s="T353">paŋɨ-m-tɨt</ta>
            <ta e="T355" id="Seg_4246" s="T354">muntɨk</ta>
            <ta e="T356" id="Seg_4247" s="T355">mi-ŋɨ-tɨt</ta>
            <ta e="T357" id="Seg_4248" s="T356">laŋa-lʼ</ta>
            <ta e="T358" id="Seg_4249" s="T357">qum-ɨ-lʼ</ta>
            <ta e="T359" id="Seg_4250" s="T358">nɔːkɨr</ta>
            <ta e="T360" id="Seg_4251" s="T359">timnʼa-sɨ-t</ta>
            <ta e="T361" id="Seg_4252" s="T360">a</ta>
            <ta e="T362" id="Seg_4253" s="T361">palna-lʼ</ta>
            <ta e="T363" id="Seg_4254" s="T362">nɔːkɨr</ta>
            <ta e="T364" id="Seg_4255" s="T363">timnʼa-sɨ-t</ta>
            <ta e="T365" id="Seg_4256" s="T364">ukoːn</ta>
            <ta e="T366" id="Seg_4257" s="T365">jam</ta>
            <ta e="T367" id="Seg_4258" s="T366">əːtɨ-rɨ-kkɨ-sɨ-tɨt</ta>
            <ta e="T368" id="Seg_4259" s="T367">wərqɨ</ta>
            <ta e="T369" id="Seg_4260" s="T368">timnʼa-tɨ</ta>
            <ta e="T370" id="Seg_4261" s="T369">kuttar</ta>
            <ta e="T371" id="Seg_4262" s="T370">paŋɨ-sä</ta>
            <ta e="T372" id="Seg_4263" s="T371">qättɨ-ntɨ-tɨ</ta>
            <ta e="T373" id="Seg_4264" s="T372">təp-ɨ-t</ta>
            <ta e="T374" id="Seg_4265" s="T373">aj</ta>
            <ta e="T375" id="Seg_4266" s="T374">nılʼčʼɨ-k</ta>
            <ta e="T376" id="Seg_4267" s="T375">qättɨ-ntɨ-tɨt</ta>
            <ta e="T377" id="Seg_4268" s="T376">wərqɨ</ta>
            <ta e="T378" id="Seg_4269" s="T377">ira</ta>
            <ta e="T379" id="Seg_4270" s="T378">ukkɨr</ta>
            <ta e="T380" id="Seg_4271" s="T379">čʼontɨ-qɨn</ta>
            <ta e="T381" id="Seg_4272" s="T380">nanɨ</ta>
            <ta e="T382" id="Seg_4273" s="T381">mattɨ</ta>
            <ta e="T383" id="Seg_4274" s="T382">sajɨ-sä</ta>
            <ta e="T384" id="Seg_4275" s="T383">iːja-m</ta>
            <ta e="T385" id="Seg_4276" s="T384">mɨrɨ-tɨ-tɨ</ta>
            <ta e="T386" id="Seg_4277" s="T385">paŋɨ-sä</ta>
            <ta e="T387" id="Seg_4278" s="T386">nɨː</ta>
            <ta e="T388" id="Seg_4279" s="T387">jam</ta>
            <ta e="T389" id="Seg_4280" s="T388">qättɨ-tɨt</ta>
            <ta e="T390" id="Seg_4281" s="T389">laŋa-lʼ</ta>
            <ta e="T391" id="Seg_4282" s="T390">qum-ɨ-lʼ</ta>
            <ta e="T392" id="Seg_4283" s="T391">nɔːkɨr</ta>
            <ta e="T393" id="Seg_4284" s="T392">timnʼa-sɨ-t-ɨ-m</ta>
            <ta e="T394" id="Seg_4285" s="T393">taŋɨ-lʼ</ta>
            <ta e="T395" id="Seg_4286" s="T394">ɔːtä-lʼ</ta>
            <ta e="T396" id="Seg_4287" s="T395">nʼoː-ntɨlʼ</ta>
            <ta e="T397" id="Seg_4288" s="T396">timnʼa-m-tɨ</ta>
            <ta e="T398" id="Seg_4289" s="T397">čʼam</ta>
            <ta e="T399" id="Seg_4290" s="T398">qättɨ-tɨ</ta>
            <ta e="T400" id="Seg_4291" s="T399">olä</ta>
            <ta e="T401" id="Seg_4292" s="T400">šünʼčʼɨ-mpɨlʼ</ta>
            <ta e="T402" id="Seg_4293" s="T401">təttɨ-m</ta>
            <ta e="T403" id="Seg_4294" s="T402">qättɨ-tɨ</ta>
            <ta e="T404" id="Seg_4295" s="T403">palna-n</ta>
            <ta e="T405" id="Seg_4296" s="T404">timnʼa-tɨ</ta>
            <ta e="T406" id="Seg_4297" s="T405">taŋɨ-lʼ</ta>
            <ta e="T407" id="Seg_4298" s="T406">ɔːtä-lʼ</ta>
            <ta e="T408" id="Seg_4299" s="T407">nʼoː-ntɨlʼ</ta>
            <ta e="T409" id="Seg_4300" s="T408">timnʼa-tɨ</ta>
            <ta e="T410" id="Seg_4301" s="T409">aj</ta>
            <ta e="T411" id="Seg_4302" s="T410">nɨːnɨ</ta>
            <ta e="T412" id="Seg_4303" s="T411">paktɨ</ta>
            <ta e="T413" id="Seg_4304" s="T412">nʼoː-lä</ta>
            <ta e="T414" id="Seg_4305" s="T413">qən-tɨ-tɨ</ta>
            <ta e="T415" id="Seg_4306" s="T414">täːqa-m-tɨ</ta>
            <ta e="T416" id="Seg_4307" s="T415">orqɨl-lä</ta>
            <ta e="T417" id="Seg_4308" s="T416">nʼoː-tɨ-ŋɨ-tɨ</ta>
            <ta e="T418" id="Seg_4309" s="T417">toːnna</ta>
            <ta e="T419" id="Seg_4310" s="T418">šittɨ</ta>
            <ta e="T420" id="Seg_4311" s="T419">timnʼa-sɨ-qı</ta>
            <ta e="T421" id="Seg_4312" s="T420">mɔːt-qɨn</ta>
            <ta e="T422" id="Seg_4313" s="T421">qalɨ-tɨt</ta>
            <ta e="T423" id="Seg_4314" s="T422">qaj</ta>
            <ta e="T424" id="Seg_4315" s="T423">qən-ŋɨ-tɨ</ta>
            <ta e="T425" id="Seg_4316" s="T424">qaj</ta>
            <ta e="T426" id="Seg_4317" s="T425">qättɨ-ŋɨ-tɨt</ta>
            <ta e="T427" id="Seg_4318" s="T426">nʼoː-lä</ta>
            <ta e="T428" id="Seg_4319" s="T427">qən-tɨ-ntɨ-tɨ</ta>
            <ta e="T429" id="Seg_4320" s="T428">nʼennä-n</ta>
            <ta e="T430" id="Seg_4321" s="T429">mitɨ-ätɔːl-lä</ta>
            <ta e="T431" id="Seg_4322" s="T430">čʼam</ta>
            <ta e="T432" id="Seg_4323" s="T431">pačʼčʼɨ-qɨl-ŋɨ-tɨ</ta>
            <ta e="T433" id="Seg_4324" s="T432">lʼakčʼɨn-tɨ</ta>
            <ta e="T434" id="Seg_4325" s="T433">pačʼčʼɨ-ätɔːl-ŋɨ-tɨ</ta>
            <ta e="T435" id="Seg_4326" s="T434">qamtä</ta>
            <ta e="T436" id="Seg_4327" s="T435">alʼčʼɨ</ta>
            <ta e="T437" id="Seg_4328" s="T436">laŋa-lʼ</ta>
            <ta e="T438" id="Seg_4329" s="T437">qum</ta>
            <ta e="T439" id="Seg_4330" s="T438">šittɨ-täl</ta>
            <ta e="T440" id="Seg_4331" s="T439">čʼam</ta>
            <ta e="T441" id="Seg_4332" s="T440">pačʼčʼɨ-ätɔːl-ŋɨ-tɨ</ta>
            <ta e="T442" id="Seg_4333" s="T441">täːqa-n</ta>
            <ta e="T443" id="Seg_4334" s="T442">kɨ-n</ta>
            <ta e="T444" id="Seg_4335" s="T443">*čʼoːmɨ-k</ta>
            <ta e="T445" id="Seg_4336" s="T444">orqɨl-ŋɨ-tɨ</ta>
            <ta e="T446" id="Seg_4337" s="T445">topɨ-tɨ</ta>
            <ta e="T448" id="Seg_4338" s="T447">səpɨ-ɛː-mpɨ</ta>
            <ta e="T449" id="Seg_4339" s="T448">palna</ta>
            <ta e="T450" id="Seg_4340" s="T449">toːnna</ta>
            <ta e="T451" id="Seg_4341" s="T450">timnʼa-qı-m-tɨ</ta>
            <ta e="T452" id="Seg_4342" s="T451">qättɨ-mpɨ-tɨ</ta>
            <ta e="T453" id="Seg_4343" s="T452">timnʼa-ntɨ</ta>
            <ta e="T454" id="Seg_4344" s="T453">wəttɨ-n</ta>
            <ta e="T455" id="Seg_4345" s="T454">*šüː</ta>
            <ta e="T456" id="Seg_4346" s="T455">nʼennä</ta>
            <ta e="T457" id="Seg_4347" s="T456">paktɨ</ta>
            <ta e="T458" id="Seg_4348" s="T457">qo-ŋɨ-tɨ</ta>
            <ta e="T459" id="Seg_4349" s="T458">mompa</ta>
            <ta e="T460" id="Seg_4350" s="T459">orqɨl-mpɨ-tɨ</ta>
            <ta e="T461" id="Seg_4351" s="T460">man</ta>
            <ta e="T462" id="Seg_4352" s="T461">tü-lɨ-lä-k</ta>
            <ta e="T463" id="Seg_4353" s="T462">na</ta>
            <ta e="T464" id="Seg_4354" s="T463">küšɨ-lʼ</ta>
            <ta e="T465" id="Seg_4355" s="T464">laka</ta>
            <ta e="T466" id="Seg_4356" s="T465">aj</ta>
            <ta e="T467" id="Seg_4357" s="T466">qaj</ta>
            <ta e="T468" id="Seg_4358" s="T467">meː-ntɨ-tɨ</ta>
            <ta e="T469" id="Seg_4359" s="T468">man</ta>
            <ta e="T470" id="Seg_4360" s="T469">tom-ɛntɨ-k</ta>
            <ta e="T471" id="Seg_4361" s="T470">nɨːnɨ</ta>
            <ta e="T472" id="Seg_4362" s="T471">nɔːkɨr</ta>
            <ta e="T473" id="Seg_4363" s="T472">timnʼa-sɨ-t-ɨ-m</ta>
            <ta e="T474" id="Seg_4364" s="T473">muntɨk</ta>
            <ta e="T475" id="Seg_4365" s="T474">qət-ɛː-ŋɨ-tɨt</ta>
            <ta e="T476" id="Seg_4366" s="T475">ukkɨr</ta>
            <ta e="T477" id="Seg_4367" s="T476">toː-n</ta>
            <ta e="T478" id="Seg_4368" s="T477">təttɨ-n</ta>
            <ta e="T479" id="Seg_4369" s="T478">čʼontɨ-qɨn</ta>
            <ta e="T480" id="Seg_4370" s="T479">nanɨ</ta>
            <ta e="T481" id="Seg_4371" s="T480">ilɨ-mpɨ-tɨt</ta>
            <ta e="T482" id="Seg_4372" s="T481">nʼi</ta>
            <ta e="T483" id="Seg_4373" s="T482">qaj-ɨ-n</ta>
            <ta e="T484" id="Seg_4374" s="T483">sümɨ</ta>
            <ta e="T485" id="Seg_4375" s="T484">čʼäːŋkɨ</ta>
            <ta e="T486" id="Seg_4376" s="T485">qən-ŋɨ-tɨt</ta>
            <ta e="T487" id="Seg_4377" s="T486">laŋa-lʼ</ta>
            <ta e="T488" id="Seg_4378" s="T487">qum-ɨ-lʼ</ta>
            <ta e="T489" id="Seg_4379" s="T488">imaqota-lʼ</ta>
            <ta e="T490" id="Seg_4380" s="T489">nılʼčʼɨ-k</ta>
            <ta e="T491" id="Seg_4381" s="T490">tɛnɨ-rɨ-ŋɨ</ta>
            <ta e="T492" id="Seg_4382" s="T491">iːja-iː-mɨ</ta>
            <ta e="T493" id="Seg_4383" s="T492">qən-sɨ-tɨt</ta>
            <ta e="T494" id="Seg_4384" s="T493">meːltɨ</ta>
            <ta e="T495" id="Seg_4385" s="T494">qaj</ta>
            <ta e="T496" id="Seg_4386" s="T495">mɔːt-ɨ-k-ŋɨ-tɨt</ta>
            <ta e="T497" id="Seg_4387" s="T496">meːlle</ta>
            <ta e="T498" id="Seg_4388" s="T497">mɨta</ta>
            <ta e="T499" id="Seg_4389" s="T498">šölʼqum</ta>
            <ta e="T500" id="Seg_4390" s="T499">iːja-m-tɨ</ta>
            <ta e="T501" id="Seg_4391" s="T500">na-n</ta>
            <ta e="T502" id="Seg_4392" s="T501">mɔːntɨ</ta>
            <ta e="T503" id="Seg_4393" s="T502">soma-k</ta>
            <ta e="T504" id="Seg_4394" s="T503">kətɨ-mpɨ-tɨ</ta>
            <ta e="T505" id="Seg_4395" s="T504">man</ta>
            <ta e="T507" id="Seg_4396" s="T506">iːja-mɨ</ta>
            <ta e="T508" id="Seg_4397" s="T507">kəːtɨ-äptɨ-kkɨ-k</ta>
            <ta e="T509" id="Seg_4398" s="T508">%%-nɨ</ta>
            <ta e="T510" id="Seg_4399" s="T509">uːkɨ-m</ta>
            <ta e="T511" id="Seg_4400" s="T510">aj</ta>
            <ta e="T512" id="Seg_4401" s="T511">musɨltɨ-kkɨ-m</ta>
            <ta e="T513" id="Seg_4402" s="T512">šölʼqum-ɨ-lʼ</ta>
            <ta e="T514" id="Seg_4403" s="T513">ima</ta>
            <ta e="T515" id="Seg_4404" s="T514">meːlle</ta>
            <ta e="T516" id="Seg_4405" s="T515">%%</ta>
            <ta e="T517" id="Seg_4406" s="T516">ɛː-ŋɨ</ta>
            <ta e="T518" id="Seg_4407" s="T517">mantɨ-ŋɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_4408" s="T0">Palna.[NOM]</ta>
            <ta e="T2" id="Seg_4409" s="T1">Palna.[NOM]</ta>
            <ta e="T3" id="Seg_4410" s="T2">three</ta>
            <ta e="T4" id="Seg_4411" s="T3">brother-CRC-PL.[NOM]</ta>
            <ta e="T5" id="Seg_4412" s="T4">live-PST.NAR-INFER-3PL</ta>
            <ta e="T6" id="Seg_4413" s="T5">Selkup-EP-ADJZ</ta>
            <ta e="T7" id="Seg_4414" s="T6">strongman-EP-ADJZ</ta>
            <ta e="T8" id="Seg_4415" s="T7">old.man.[NOM]</ta>
            <ta e="T9" id="Seg_4416" s="T8">be-HAB-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T10" id="Seg_4417" s="T9">force-PROPR-ADJZ</ta>
            <ta e="T11" id="Seg_4418" s="T10">old.man.[NOM]</ta>
            <ta e="T12" id="Seg_4419" s="T11">the.most</ta>
            <ta e="T13" id="Seg_4420" s="T12">elder</ta>
            <ta e="T14" id="Seg_4421" s="T13">brother-PL-EP-GEN</ta>
            <ta e="T15" id="Seg_4422" s="T14">middle-LOC-ADJZ</ta>
            <ta e="T16" id="Seg_4423" s="T15">brother-PL-EP-GEN</ta>
            <ta e="T17" id="Seg_4424" s="T16">summer-ADJZ</ta>
            <ta e="T18" id="Seg_4425" s="T17">reindeer-ADJZ</ta>
            <ta e="T19" id="Seg_4426" s="T18">catch.up-PTCP.PRS</ta>
            <ta e="T20" id="Seg_4427" s="T19">be-HAB-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T21" id="Seg_4428" s="T20">most</ta>
            <ta e="T22" id="Seg_4429" s="T21">small</ta>
            <ta e="T23" id="Seg_4430" s="T22">brother.[NOM]-3SG</ta>
            <ta e="T24" id="Seg_4431" s="T23">wild.rosemary-GEN</ta>
            <ta e="T25" id="Seg_4432" s="T24">top-ADJZ</ta>
            <ta e="T26" id="Seg_4433" s="T25">catch.up-PTCP.PRS</ta>
            <ta e="T27" id="Seg_4434" s="T26">be-HAB-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T28" id="Seg_4435" s="T27">such-ADVZ</ta>
            <ta e="T29" id="Seg_4436" s="T28">live-PST.NAR-3PL</ta>
            <ta e="T30" id="Seg_4437" s="T29">but</ta>
            <ta e="T31" id="Seg_4438" s="T30">this</ta>
            <ta e="T32" id="Seg_4439" s="T31">Palna-ADJZ</ta>
            <ta e="T33" id="Seg_4440" s="T32">elder</ta>
            <ta e="T34" id="Seg_4441" s="T33">brother.[NOM]-3SG</ta>
            <ta e="T35" id="Seg_4442" s="T34">thus.much</ta>
            <ta e="T36" id="Seg_4443" s="T35">shaman.wisdom-INSTR</ta>
            <ta e="T37" id="Seg_4444" s="T36">be-HAB-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T38" id="Seg_4445" s="T37">where.from</ta>
            <ta e="T39" id="Seg_4446" s="T38">what.[NOM]</ta>
            <ta e="T40" id="Seg_4447" s="T39">come-FUT.[3SG.S]</ta>
            <ta e="T41" id="Seg_4448" s="T40">all</ta>
            <ta e="T42" id="Seg_4449" s="T41">know-PST.NAR-3SG.O</ta>
            <ta e="T43" id="Seg_4450" s="T42">sing-PTCP.PRS</ta>
            <ta e="T44" id="Seg_4451" s="T43">shaman-ADJZ</ta>
            <ta e="T45" id="Seg_4452" s="T44">old.man.[NOM]</ta>
            <ta e="T46" id="Seg_4453" s="T45">be-HAB-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T47" id="Seg_4454" s="T46">iron-ACC</ta>
            <ta e="T48" id="Seg_4455" s="T47">make-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T49" id="Seg_4456" s="T48">himself.[NOM]</ta>
            <ta e="T50" id="Seg_4457" s="T49">live-ACTN-LOC-OBL.3SG</ta>
            <ta e="T51" id="Seg_4458" s="T50">set.fire-FRQ-CO.[3SG.S]</ta>
            <ta e="T52" id="Seg_4459" s="T51">iron-ACC</ta>
            <ta e="T53" id="Seg_4460" s="T52">so</ta>
            <ta e="T54" id="Seg_4461" s="T53">make-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T55" id="Seg_4462" s="T54">to.no.purpose</ta>
            <ta e="T56" id="Seg_4463" s="T55">stone-PL-EP-GEN</ta>
            <ta e="T57" id="Seg_4464" s="T56">but</ta>
            <ta e="T58" id="Seg_4465" s="T57">Russian-EP-PL.[NOM]</ta>
            <ta e="T59" id="Seg_4466" s="T58">make-IPFV-PST.NAR-3PL</ta>
            <ta e="T60" id="Seg_4467" s="T59">NEG</ta>
            <ta e="T61" id="Seg_4468" s="T60">know-1SG.O</ta>
            <ta e="T62" id="Seg_4469" s="T61">what-EP-GEN</ta>
            <ta e="T63" id="Seg_4470" s="T62">out-ADV.EL</ta>
            <ta e="T65" id="Seg_4471" s="T64">(s)he.[NOM]</ta>
            <ta e="T66" id="Seg_4472" s="T65">make-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T67" id="Seg_4473" s="T66">iron-TRL</ta>
            <ta e="T68" id="Seg_4474" s="T67">stone-ACC</ta>
            <ta e="T69" id="Seg_4475" s="T68">make-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T70" id="Seg_4476" s="T69">this</ta>
            <ta e="T71" id="Seg_4477" s="T70">stone-GEN</ta>
            <ta e="T72" id="Seg_4478" s="T71">out-ADV.EL</ta>
            <ta e="T73" id="Seg_4479" s="T72">iron-ADJZ</ta>
            <ta e="T74" id="Seg_4480" s="T73">clothing-ACC</ta>
            <ta e="T75" id="Seg_4481" s="T74">make-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T77" id="Seg_4482" s="T76">iron-ADJZ</ta>
            <ta e="T78" id="Seg_4483" s="T77">bow.[NOM]</ta>
            <ta e="T79" id="Seg_4484" s="T78">make-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T81" id="Seg_4485" s="T80">iron-ADJZ</ta>
            <ta e="T82" id="Seg_4486" s="T81">arrow-ACC</ta>
            <ta e="T83" id="Seg_4487" s="T82">make-IPFV-PST.NAR-INFER-3SG.O</ta>
            <ta e="T84" id="Seg_4488" s="T83">this-OBL.3SG-INSTR</ta>
            <ta e="T85" id="Seg_4489" s="T84">and</ta>
            <ta e="T86" id="Seg_4490" s="T85">live-PST.NAR.[3SG.S]</ta>
            <ta e="T87" id="Seg_4491" s="T86">but</ta>
            <ta e="T88" id="Seg_4492" s="T87">war-TR-CVB</ta>
            <ta e="T89" id="Seg_4493" s="T88">NEG</ta>
            <ta e="T90" id="Seg_4494" s="T89">go-FRQ-PST.NAR.[3SG.S]</ta>
            <ta e="T91" id="Seg_4495" s="T90">(s)he-EP-ALL</ta>
            <ta e="T92" id="Seg_4496" s="T91">war-TR-CVB</ta>
            <ta e="T93" id="Seg_4497" s="T92">go-FRQ-PST.NAR-3PL</ta>
            <ta e="T95" id="Seg_4498" s="T94">where.from</ta>
            <ta e="T96" id="Seg_4499" s="T95">only</ta>
            <ta e="T97" id="Seg_4500" s="T96">come-FUT-3PL</ta>
            <ta e="T98" id="Seg_4501" s="T97">(s)he.[NOM]</ta>
            <ta e="T99" id="Seg_4502" s="T98">all</ta>
            <ta e="T100" id="Seg_4503" s="T99">know-3SG.O</ta>
            <ta e="T101" id="Seg_4504" s="T100">Nenets-PL.[NOM]</ta>
            <ta e="T102" id="Seg_4505" s="T101">go-FRQ-PST.NAR-3PL</ta>
            <ta e="T103" id="Seg_4506" s="T102">war-TR-INF</ta>
            <ta e="T104" id="Seg_4507" s="T103">ide-ADJZ</ta>
            <ta e="T105" id="Seg_4508" s="T104">human.being-EP-GEN</ta>
            <ta e="T106" id="Seg_4509" s="T105">side-LOC</ta>
            <ta e="T107" id="Seg_4510" s="T106">also</ta>
            <ta e="T108" id="Seg_4511" s="T107">three</ta>
            <ta e="T109" id="Seg_4512" s="T108">brother-CRC-PL.[NOM]</ta>
            <ta e="T110" id="Seg_4513" s="T109">be-PST.NAR-INFER-3PL</ta>
            <ta e="T111" id="Seg_4514" s="T110">again</ta>
            <ta e="T112" id="Seg_4515" s="T111">look.for-CVB</ta>
            <ta e="T113" id="Seg_4516" s="T112">go-FRQ-PST.NAR-3PL</ta>
            <ta e="T114" id="Seg_4517" s="T113">this_day.[NOM]</ta>
            <ta e="T115" id="Seg_4518" s="T114">human.being-EP-ADJZ</ta>
            <ta e="T116" id="Seg_4519" s="T115">three</ta>
            <ta e="T117" id="Seg_4520" s="T116">brother-CRC-PL-EP-ACC</ta>
            <ta e="T118" id="Seg_4521" s="T117">(s)he.[NOM]</ta>
            <ta e="T119" id="Seg_4522" s="T118">live-PST.NAR.[3SG.S]</ta>
            <ta e="T120" id="Seg_4523" s="T119">earlier</ta>
            <ta e="T121" id="Seg_4524" s="T120">stone-ADJZ-sea.[NOM]</ta>
            <ta e="T122" id="Seg_4525" s="T121">big.river-LOC</ta>
            <ta e="T123" id="Seg_4526" s="T122">Turukhansk</ta>
            <ta e="T124" id="Seg_4527" s="T123">open</ta>
            <ta e="T125" id="Seg_4528" s="T124">side-LOC</ta>
            <ta e="T126" id="Seg_4529" s="T125">sand-LOC</ta>
            <ta e="T127" id="Seg_4530" s="T126">ide-ADJZ</ta>
            <ta e="T128" id="Seg_4531" s="T127">human.being-EP-ADJZ</ta>
            <ta e="T129" id="Seg_4532" s="T128">three</ta>
            <ta e="T130" id="Seg_4533" s="T129">brother-CRC-PL.[NOM]</ta>
            <ta e="T131" id="Seg_4534" s="T130">one</ta>
            <ta e="T132" id="Seg_4535" s="T131">whole</ta>
            <ta e="T133" id="Seg_4536" s="T132">middle-LOC</ta>
            <ta e="T134" id="Seg_4537" s="T133">such-ADVZ</ta>
            <ta e="T135" id="Seg_4538" s="T134">be-PST-3PL</ta>
            <ta e="T136" id="Seg_4539" s="T135">Palna-ADJZ</ta>
            <ta e="T137" id="Seg_4540" s="T136">three</ta>
            <ta e="T138" id="Seg_4541" s="T137">brother-CRC-PL-EP-ACC</ta>
            <ta e="T139" id="Seg_4542" s="T138">look.for-CVB</ta>
            <ta e="T140" id="Seg_4543" s="T139">go.away-INF-be.going.to-CO-3PL</ta>
            <ta e="T141" id="Seg_4544" s="T140">but</ta>
            <ta e="T142" id="Seg_4545" s="T141">Palna-ADJZ</ta>
            <ta e="T143" id="Seg_4546" s="T142">brother-CRC-PL.[NOM]</ta>
            <ta e="T144" id="Seg_4547" s="T143">go.away-3PL</ta>
            <ta e="T145" id="Seg_4548" s="T144">brother-PL-ILL-OBL.3SG</ta>
            <ta e="T146" id="Seg_4549" s="T145">such-ADVZ</ta>
            <ta e="T147" id="Seg_4550" s="T146">say-CO-3SG.O</ta>
            <ta e="T148" id="Seg_4551" s="T147">ide-ADJZ</ta>
            <ta e="T149" id="Seg_4552" s="T148">human.being-EP-ADJZ</ta>
            <ta e="T150" id="Seg_4553" s="T149">three</ta>
            <ta e="T151" id="Seg_4554" s="T150">brother-CRC-PL.[NOM]</ta>
            <ta e="T152" id="Seg_4555" s="T151">INFER</ta>
            <ta e="T153" id="Seg_4556" s="T152">come-INF-be.going.to-INFER-3PL</ta>
            <ta e="T154" id="Seg_4557" s="T153">war-TR-CVB</ta>
            <ta e="T155" id="Seg_4558" s="T154">we.PL.DAT</ta>
            <ta e="T156" id="Seg_4559" s="T155">down</ta>
            <ta e="T157" id="Seg_4560" s="T156">sand-ILL</ta>
            <ta e="T158" id="Seg_4561" s="T157">tent-EP-VBLZ-OPT-1PL</ta>
            <ta e="T159" id="Seg_4562" s="T158">sand-ILL</ta>
            <ta e="T160" id="Seg_4563" s="T159">tent-EP-VBLZ-FUT-1PL</ta>
            <ta e="T161" id="Seg_4564" s="T160">food-ACC</ta>
            <ta e="T162" id="Seg_4565" s="T161">leave-TR-FUT-1PL</ta>
            <ta e="T163" id="Seg_4566" s="T162">human.being-EP-PL.[NOM]</ta>
            <ta e="T164" id="Seg_4567" s="T163">visiting</ta>
            <ta e="T165" id="Seg_4568" s="T164">tent-TR-CVB</ta>
            <ta e="T166" id="Seg_4569" s="T165">come-FUT-3PL</ta>
            <ta e="T167" id="Seg_4570" s="T166">eat-EP-FRQ-FUT-3PL</ta>
            <ta e="T168" id="Seg_4571" s="T167">down</ta>
            <ta e="T169" id="Seg_4572" s="T168">go.away-CO-3PL</ta>
            <ta e="T170" id="Seg_4573" s="T169">sand-ILL</ta>
            <ta e="T171" id="Seg_4574" s="T170">deer.train-TR-3PL</ta>
            <ta e="T172" id="Seg_4575" s="T171">tent-EP-VBLZ-CO-3PL</ta>
            <ta e="T173" id="Seg_4576" s="T172">hay-INSTR</ta>
            <ta e="T174" id="Seg_4577" s="T173">lay-3PL</ta>
            <ta e="T175" id="Seg_4578" s="T174">tent-ILL</ta>
            <ta e="T176" id="Seg_4579" s="T175">that-ADJZ</ta>
            <ta e="T177" id="Seg_4580" s="T176">side-LOC</ta>
            <ta e="T178" id="Seg_4581" s="T177">birchbark-INSTR</ta>
            <ta e="T179" id="Seg_4582" s="T178">lay-3PL</ta>
            <ta e="T180" id="Seg_4583" s="T179">up-ADV.EL</ta>
            <ta e="T181" id="Seg_4584" s="T180">down</ta>
            <ta e="T182" id="Seg_4585" s="T181">again</ta>
            <ta e="T183" id="Seg_4586" s="T182">hay-INSTR</ta>
            <ta e="T184" id="Seg_4587" s="T183">lay-3PL</ta>
            <ta e="T185" id="Seg_4588" s="T184">brother-DU-OBL.3SG-ALL</ta>
            <ta e="T186" id="Seg_4589" s="T185">porridge.[NOM]</ta>
            <ta e="T187" id="Seg_4590" s="T186">oats.[NOM]</ta>
            <ta e="T188" id="Seg_4591" s="T187">be.cooking-CAUS-IMP.2PL</ta>
            <ta e="T189" id="Seg_4592" s="T188">this_day.[NOM]</ta>
            <ta e="T190" id="Seg_4593" s="T189">come-FUT-3PL</ta>
            <ta e="T191" id="Seg_4594" s="T190">fat-EP-ADJZ</ta>
            <ta e="T192" id="Seg_4595" s="T191">dried.fish-ACC</ta>
            <ta e="T193" id="Seg_4596" s="T192">tent-ILL</ta>
            <ta e="T194" id="Seg_4597" s="T193">bring-TR-IMP.2PL</ta>
            <ta e="T195" id="Seg_4598" s="T194">porridge-ACC</ta>
            <ta e="T196" id="Seg_4599" s="T195">oats-ACC</ta>
            <ta e="T197" id="Seg_4600" s="T196">onto.the.fire</ta>
            <ta e="T198" id="Seg_4601" s="T197">be.cooking-CAUS-3PL</ta>
            <ta e="T199" id="Seg_4602" s="T198">sometimes</ta>
            <ta e="T200" id="Seg_4603" s="T199">here</ta>
            <ta e="T201" id="Seg_4604" s="T200">sit-3PL</ta>
            <ta e="T202" id="Seg_4605" s="T201">sun.[NOM]-3SG</ta>
            <ta e="T203" id="Seg_4606" s="T202">day-ADJZ</ta>
            <ta e="T204" id="Seg_4607" s="T203">middle-ADJZ</ta>
            <ta e="T205" id="Seg_4608" s="T204">interval-ILL</ta>
            <ta e="T206" id="Seg_4609" s="T205">turn-DECAUS-CO.[3SG.S]</ta>
            <ta e="T207" id="Seg_4610" s="T206">forward-ADV.ILL</ta>
            <ta e="T208" id="Seg_4611" s="T207">boat-AUGM.[NOM]</ta>
            <ta e="T209" id="Seg_4612" s="T208">appear-US-PFV.[3SG.S]</ta>
            <ta e="T210" id="Seg_4613" s="T209">three</ta>
            <ta e="T211" id="Seg_4614" s="T210">human.being-EP-PL.[NOM]</ta>
            <ta e="T212" id="Seg_4615" s="T211">brother-EP-ADJZ</ta>
            <ta e="T213" id="Seg_4616" s="T212">boat.[NOM]</ta>
            <ta e="T214" id="Seg_4617" s="T213">appear-US-PFV-INFER.[3SG.S]</ta>
            <ta e="T215" id="Seg_4618" s="T214">brother-PL-ILL-OBL.3SG</ta>
            <ta e="T216" id="Seg_4619" s="T215">such-ADVZ</ta>
            <ta e="T217" id="Seg_4620" s="T216">say-CO-3SG.O</ta>
            <ta e="T218" id="Seg_4621" s="T217">knife-ADJZ</ta>
            <ta e="T219" id="Seg_4622" s="T218">pike-DIM-PL.[NOM]</ta>
            <ta e="T220" id="Seg_4623" s="T219">down</ta>
            <ta e="T221" id="Seg_4624" s="T220">hide-MULO-IMP.2PL</ta>
            <ta e="T222" id="Seg_4625" s="T221">footwear-GEN</ta>
            <ta e="T223" id="Seg_4626" s="T222">bootleg-EP-GEN</ta>
            <ta e="T224" id="Seg_4627" s="T223">into</ta>
            <ta e="T225" id="Seg_4628" s="T224">put.into-MOM-PFV-IMP.2PL</ta>
            <ta e="T226" id="Seg_4629" s="T225">then</ta>
            <ta e="T227" id="Seg_4630" s="T226">such-ADVZ</ta>
            <ta e="T228" id="Seg_4631" s="T227">say-IMP.2PL</ta>
            <ta e="T229" id="Seg_4632" s="T228">knife.[NOM]-1PL</ta>
            <ta e="T230" id="Seg_4633" s="T229">NEG.EX.[3SG.S]</ta>
            <ta e="T231" id="Seg_4634" s="T230">knife.[NOM]-2PL</ta>
            <ta e="T232" id="Seg_4635" s="T231">we.PL.DAT</ta>
            <ta e="T233" id="Seg_4636" s="T232">give-IMP.2PL</ta>
            <ta e="T234" id="Seg_4637" s="T233">spoon-ACC</ta>
            <ta e="T235" id="Seg_4638" s="T234">make-MULO-PFV-OPT-1PL</ta>
            <ta e="T236" id="Seg_4639" s="T235">eat-EP-FRQ-SUP.1PL</ta>
            <ta e="T237" id="Seg_4640" s="T236">so</ta>
            <ta e="T238" id="Seg_4641" s="T237">hide-FRQ-DUR-CO-3SG.O</ta>
            <ta e="T239" id="Seg_4642" s="T238">young</ta>
            <ta e="T240" id="Seg_4643" s="T239">brother.[NOM]-3SG</ta>
            <ta e="T241" id="Seg_4644" s="T240">part.of.the.tent.opposite.to.the.entry-LOC</ta>
            <ta e="T242" id="Seg_4645" s="T241">live.[3SG.S]</ta>
            <ta e="T243" id="Seg_4646" s="T242">pike-ACC-3SG</ta>
            <ta e="T244" id="Seg_4647" s="T243">there</ta>
            <ta e="T245" id="Seg_4648" s="T244">up</ta>
            <ta e="T246" id="Seg_4649" s="T245">poke-RES-CO-3SG.O</ta>
            <ta e="T247" id="Seg_4650" s="T246">up-ADV.LOC</ta>
            <ta e="T248" id="Seg_4651" s="T247">fire-GEN</ta>
            <ta e="T249" id="Seg_4652" s="T248">granary-ADJZ</ta>
            <ta e="T250" id="Seg_4653" s="T249">tree.[NOM]</ta>
            <ta e="T251" id="Seg_4654" s="T250">bind-HAB-IPFV-3PL</ta>
            <ta e="T252" id="Seg_4655" s="T251">so</ta>
            <ta e="T253" id="Seg_4656" s="T252">sight-CO-3PL</ta>
            <ta e="T254" id="Seg_4657" s="T253">forward-ADV.ILL</ta>
            <ta e="T255" id="Seg_4658" s="T254">appear-US-PFV-CO.[3SG.S]</ta>
            <ta e="T256" id="Seg_4659" s="T255">three</ta>
            <ta e="T257" id="Seg_4660" s="T256">human.being-EP-PL.[NOM]</ta>
            <ta e="T258" id="Seg_4661" s="T257">brother-EP-ADJZ</ta>
            <ta e="T259" id="Seg_4662" s="T258">boat.[NOM]</ta>
            <ta e="T260" id="Seg_4663" s="T259">brother-DU-ACC-3SG</ta>
            <ta e="T261" id="Seg_4664" s="T260">speech-VBLZ-DUR-INF-begin-CO-3SG.O</ta>
            <ta e="T262" id="Seg_4665" s="T261">outwards</ta>
            <ta e="T263" id="Seg_4666" s="T262">INFER</ta>
            <ta e="T264" id="Seg_4667" s="T263">only</ta>
            <ta e="T265" id="Seg_4668" s="T264">go.out-INFER-3PL</ta>
            <ta e="T266" id="Seg_4669" s="T265">apparently</ta>
            <ta e="T267" id="Seg_4670" s="T266">bow-ADJZ</ta>
            <ta e="T268" id="Seg_4671" s="T267">arrow-ACC</ta>
            <ta e="T269" id="Seg_4672" s="T268">up</ta>
            <ta e="T270" id="Seg_4673" s="T269">take-MULO-PST.NAR-3PL</ta>
            <ta e="T271" id="Seg_4674" s="T270">water-EL</ta>
            <ta e="T272" id="Seg_4675" s="T271">upwards</ta>
            <ta e="T274" id="Seg_4676" s="T273">shoot-INFER-3PL</ta>
            <ta e="T275" id="Seg_4677" s="T274">outwards</ta>
            <ta e="T276" id="Seg_4678" s="T275">go.out-CVB</ta>
            <ta e="T277" id="Seg_4679" s="T276">so</ta>
            <ta e="T278" id="Seg_4680" s="T277">say-CO-3SG.O</ta>
            <ta e="T279" id="Seg_4681" s="T278">upwards</ta>
            <ta e="T280" id="Seg_4682" s="T279">go.out-CVB</ta>
            <ta e="T281" id="Seg_4683" s="T280">eat-EP-FRQ-IMP.2PL</ta>
            <ta e="T282" id="Seg_4684" s="T281">forehead-EP-OBL.1DU-INSTR</ta>
            <ta e="T283" id="Seg_4685" s="T282">war-TR-OPT-1PL</ta>
            <ta e="T284" id="Seg_4686" s="T283">go.out-CVB</ta>
            <ta e="T285" id="Seg_4687" s="T284">space.outside-LOC</ta>
            <ta e="T286" id="Seg_4688" s="T285">water-EL</ta>
            <ta e="T287" id="Seg_4689" s="T286">upwards</ta>
            <ta e="T288" id="Seg_4690" s="T287">and</ta>
            <ta e="T289" id="Seg_4691" s="T288">what.[NOM]</ta>
            <ta e="T290" id="Seg_4692" s="T289">make-INF-be.going.to-CO-2PL</ta>
            <ta e="T291" id="Seg_4693" s="T290">upwards</ta>
            <ta e="T292" id="Seg_4694" s="T291">go.out-CVB</ta>
            <ta e="T293" id="Seg_4695" s="T292">eat-EP-FRQ-IMP.2PL</ta>
            <ta e="T294" id="Seg_4696" s="T293">and</ta>
            <ta e="T295" id="Seg_4697" s="T294">such-ADVZ</ta>
            <ta e="T296" id="Seg_4698" s="T295">be-PST.[3SG.S]</ta>
            <ta e="T297" id="Seg_4699" s="T296">elder</ta>
            <ta e="T298" id="Seg_4700" s="T297">brother.[NOM]-3SG</ta>
            <ta e="T299" id="Seg_4701" s="T298">upwards</ta>
            <ta e="T300" id="Seg_4702" s="T299">go.out-CVB</ta>
            <ta e="T301" id="Seg_4703" s="T300">eat-EP-FRQ-OPT-1PL</ta>
            <ta e="T302" id="Seg_4704" s="T301">upwards</ta>
            <ta e="T303" id="Seg_4705" s="T302">RFL</ta>
            <ta e="T304" id="Seg_4706" s="T303">bring-TR-3PL</ta>
            <ta e="T305" id="Seg_4707" s="T304">bow-ADJZ</ta>
            <ta e="T306" id="Seg_4708" s="T305">arrow-ACC-3PL</ta>
            <ta e="T307" id="Seg_4709" s="T306">down-ADV.LOC</ta>
            <ta e="T308" id="Seg_4710" s="T307">boat-LOC-OBL.3PL</ta>
            <ta e="T309" id="Seg_4711" s="T308">put-CVB</ta>
            <ta e="T310" id="Seg_4712" s="T309">leave-MOM-CO-3PL</ta>
            <ta e="T311" id="Seg_4713" s="T310">tent-ILL</ta>
            <ta e="T312" id="Seg_4714" s="T311">INFER</ta>
            <ta e="T313" id="Seg_4715" s="T312">RFL</ta>
            <ta e="T314" id="Seg_4716" s="T313">come.in-INFER-3PL</ta>
            <ta e="T315" id="Seg_4717" s="T314">big</ta>
            <ta e="T316" id="Seg_4718" s="T315">ide-ADJZ</ta>
            <ta e="T317" id="Seg_4719" s="T316">human.being-EP-ADJZ</ta>
            <ta e="T318" id="Seg_4720" s="T317">strongman.[NOM]</ta>
            <ta e="T319" id="Seg_4721" s="T318">Palna-GEN</ta>
            <ta e="T320" id="Seg_4722" s="T319">with</ta>
            <ta e="T321" id="Seg_4723" s="T320">one</ta>
            <ta e="T322" id="Seg_4724" s="T321">side-ILL</ta>
            <ta e="T323" id="Seg_4725" s="T322">INFER</ta>
            <ta e="T324" id="Seg_4726" s="T323">sit.down-INFER-3PL</ta>
            <ta e="T325" id="Seg_4727" s="T324">down.the.river-ADJZ</ta>
            <ta e="T326" id="Seg_4728" s="T325">reindeer-ADJZ</ta>
            <ta e="T327" id="Seg_4729" s="T326">catch.up-PTCP.PRS</ta>
            <ta e="T328" id="Seg_4730" s="T327">brother-OBL.3SG-COM</ta>
            <ta e="T329" id="Seg_4731" s="T328">also</ta>
            <ta e="T330" id="Seg_4732" s="T329">one</ta>
            <ta e="T331" id="Seg_4733" s="T330">side-ILL</ta>
            <ta e="T332" id="Seg_4734" s="T331">INFER</ta>
            <ta e="T333" id="Seg_4735" s="T332">sit.down-INFER-3PL</ta>
            <ta e="T334" id="Seg_4736" s="T333">three</ta>
            <ta e="T335" id="Seg_4737" s="T334">brother-CRC-PL.[NOM]</ta>
            <ta e="T336" id="Seg_4738" s="T335">all</ta>
            <ta e="T337" id="Seg_4739" s="T336">one</ta>
            <ta e="T338" id="Seg_4740" s="T337">side-ILL</ta>
            <ta e="T339" id="Seg_4741" s="T338">sit.down-OPT-3PL</ta>
            <ta e="T340" id="Seg_4742" s="T339">then</ta>
            <ta e="T341" id="Seg_4743" s="T340">so</ta>
            <ta e="T342" id="Seg_4744" s="T341">say-PST-3PL</ta>
            <ta e="T343" id="Seg_4745" s="T342">knife.[NOM]-1PL</ta>
            <ta e="T344" id="Seg_4746" s="T343">whether</ta>
            <ta e="T345" id="Seg_4747" s="T344">forget-PST.NAR-1PL</ta>
            <ta e="T346" id="Seg_4748" s="T345">knife.[NOM]-2PL</ta>
            <ta e="T347" id="Seg_4749" s="T346">be-CO.[3SG.S]</ta>
            <ta e="T348" id="Seg_4750" s="T347">knife-INSTR</ta>
            <ta e="T349" id="Seg_4751" s="T348">we.PL.ACC</ta>
            <ta e="T350" id="Seg_4752" s="T349">give-IMP.2PL</ta>
            <ta e="T351" id="Seg_4753" s="T350">eat-EP-FRQ-INF</ta>
            <ta e="T352" id="Seg_4754" s="T351">spoon-ACC</ta>
            <ta e="T353" id="Seg_4755" s="T352">make-MULO-OPT-1PL</ta>
            <ta e="T354" id="Seg_4756" s="T353">knife-ACC-3PL</ta>
            <ta e="T355" id="Seg_4757" s="T354">all</ta>
            <ta e="T356" id="Seg_4758" s="T355">give-CO-3PL</ta>
            <ta e="T357" id="Seg_4759" s="T356">ide-ADJZ</ta>
            <ta e="T358" id="Seg_4760" s="T357">human.being-EP-ADJZ</ta>
            <ta e="T359" id="Seg_4761" s="T358">three</ta>
            <ta e="T360" id="Seg_4762" s="T359">brother-CRC-PL.[NOM]</ta>
            <ta e="T361" id="Seg_4763" s="T360">but</ta>
            <ta e="T362" id="Seg_4764" s="T361">Palna-ADJZ</ta>
            <ta e="T363" id="Seg_4765" s="T362">three</ta>
            <ta e="T364" id="Seg_4766" s="T363">brother-CRC-PL.[NOM]</ta>
            <ta e="T365" id="Seg_4767" s="T364">earlier</ta>
            <ta e="T366" id="Seg_4768" s="T365">RFL</ta>
            <ta e="T367" id="Seg_4769" s="T366">word-VBLZ-DUR-PST-3PL</ta>
            <ta e="T368" id="Seg_4770" s="T367">elder</ta>
            <ta e="T369" id="Seg_4771" s="T368">brother.[NOM]-3SG</ta>
            <ta e="T370" id="Seg_4772" s="T369">how</ta>
            <ta e="T371" id="Seg_4773" s="T370">knife-INSTR</ta>
            <ta e="T372" id="Seg_4774" s="T371">hit-INFER-3SG.O</ta>
            <ta e="T373" id="Seg_4775" s="T372">(s)he-EP-PL</ta>
            <ta e="T374" id="Seg_4776" s="T373">also</ta>
            <ta e="T375" id="Seg_4777" s="T374">such-ADVZ</ta>
            <ta e="T376" id="Seg_4778" s="T375">hit-INFER-3PL</ta>
            <ta e="T377" id="Seg_4779" s="T376">elder</ta>
            <ta e="T378" id="Seg_4780" s="T377">old.man.[NOM]</ta>
            <ta e="T379" id="Seg_4781" s="T378">one</ta>
            <ta e="T380" id="Seg_4782" s="T379">middle-LOC</ta>
            <ta e="T381" id="Seg_4783" s="T380">sometimes</ta>
            <ta e="T382" id="Seg_4784" s="T381">cut.[3SG.S]</ta>
            <ta e="T383" id="Seg_4785" s="T382">eye-INSTR</ta>
            <ta e="T384" id="Seg_4786" s="T383">child-ACC</ta>
            <ta e="T385" id="Seg_4787" s="T384">gnaw-HAB-3SG.O</ta>
            <ta e="T386" id="Seg_4788" s="T385">knife-INSTR</ta>
            <ta e="T387" id="Seg_4789" s="T386">there</ta>
            <ta e="T388" id="Seg_4790" s="T387">RFL</ta>
            <ta e="T389" id="Seg_4791" s="T388">hit-3PL</ta>
            <ta e="T390" id="Seg_4792" s="T389">ide-ADJZ</ta>
            <ta e="T391" id="Seg_4793" s="T390">human.being-EP-ADJZ</ta>
            <ta e="T392" id="Seg_4794" s="T391">three</ta>
            <ta e="T393" id="Seg_4795" s="T392">brother-CRC-PL-EP-ACC</ta>
            <ta e="T394" id="Seg_4796" s="T393">summer-ADJZ</ta>
            <ta e="T395" id="Seg_4797" s="T394">reindeer-ADJZ</ta>
            <ta e="T396" id="Seg_4798" s="T395">catch.up-PTCP.PRS</ta>
            <ta e="T397" id="Seg_4799" s="T396">brother-ACC-3SG</ta>
            <ta e="T398" id="Seg_4800" s="T397">only</ta>
            <ta e="T399" id="Seg_4801" s="T398">hit-3SG.O</ta>
            <ta e="T400" id="Seg_4802" s="T399">only</ta>
            <ta e="T401" id="Seg_4803" s="T400">run.out-PTCP.PST</ta>
            <ta e="T402" id="Seg_4804" s="T401">earth-ACC</ta>
            <ta e="T403" id="Seg_4805" s="T402">hit-3SG.O</ta>
            <ta e="T404" id="Seg_4806" s="T403">Palna-GEN</ta>
            <ta e="T405" id="Seg_4807" s="T404">brother.[NOM]-3SG</ta>
            <ta e="T406" id="Seg_4808" s="T405">summer-ADJZ</ta>
            <ta e="T407" id="Seg_4809" s="T406">reindeer-ADJZ</ta>
            <ta e="T408" id="Seg_4810" s="T407">catch.up-PTCP.PRS</ta>
            <ta e="T409" id="Seg_4811" s="T408">brother.[NOM]-3SG</ta>
            <ta e="T410" id="Seg_4812" s="T409">also</ta>
            <ta e="T411" id="Seg_4813" s="T410">then</ta>
            <ta e="T412" id="Seg_4814" s="T411">run.[3SG.S]</ta>
            <ta e="T413" id="Seg_4815" s="T412">catch.up-CVB</ta>
            <ta e="T414" id="Seg_4816" s="T413">leave-TR-3SG.O</ta>
            <ta e="T415" id="Seg_4817" s="T414">pike-ACC-3SG</ta>
            <ta e="T416" id="Seg_4818" s="T415">catch-CVB</ta>
            <ta e="T417" id="Seg_4819" s="T416">catch.up-TR-CO-3SG.O</ta>
            <ta e="T418" id="Seg_4820" s="T417">that</ta>
            <ta e="T419" id="Seg_4821" s="T418">two</ta>
            <ta e="T420" id="Seg_4822" s="T419">brother-CRC-DU.[NOM]</ta>
            <ta e="T421" id="Seg_4823" s="T420">tent-LOC</ta>
            <ta e="T422" id="Seg_4824" s="T421">stay-3PL</ta>
            <ta e="T423" id="Seg_4825" s="T422">either.or</ta>
            <ta e="T424" id="Seg_4826" s="T423">leave-CO-3SG.O</ta>
            <ta e="T425" id="Seg_4827" s="T424">either.or</ta>
            <ta e="T426" id="Seg_4828" s="T425">hit-CO-3PL</ta>
            <ta e="T427" id="Seg_4829" s="T426">catch.up-CVB</ta>
            <ta e="T428" id="Seg_4830" s="T427">leave-TR-IPFV-3SG.O</ta>
            <ta e="T429" id="Seg_4831" s="T428">forward-ADV.LOC</ta>
            <ta e="T430" id="Seg_4832" s="T429">catch.up-MOM-CVB</ta>
            <ta e="T431" id="Seg_4833" s="T430">only</ta>
            <ta e="T432" id="Seg_4834" s="T431">chop-MULO-CO-3SG.O</ta>
            <ta e="T433" id="Seg_4835" s="T432">heel.[NOM]-3SG</ta>
            <ta e="T434" id="Seg_4836" s="T433">chop-MOM-CO-3SG.O</ta>
            <ta e="T435" id="Seg_4837" s="T434">facedown</ta>
            <ta e="T436" id="Seg_4838" s="T435">fall.[3SG.S]</ta>
            <ta e="T437" id="Seg_4839" s="T436">ide-ADJZ</ta>
            <ta e="T438" id="Seg_4840" s="T437">human.being.[NOM]</ta>
            <ta e="T439" id="Seg_4841" s="T438">two-ITER.NUM</ta>
            <ta e="T440" id="Seg_4842" s="T439">hardly</ta>
            <ta e="T441" id="Seg_4843" s="T440">chop-MOM-CO-3SG.O</ta>
            <ta e="T442" id="Seg_4844" s="T441">pike-GEN</ta>
            <ta e="T443" id="Seg_4845" s="T442">middle-GEN</ta>
            <ta e="T444" id="Seg_4846" s="T443">calm-ADVZ</ta>
            <ta e="T445" id="Seg_4847" s="T444">catch-CO-3SG.O</ta>
            <ta e="T446" id="Seg_4848" s="T445">leg.[NOM]-3SG</ta>
            <ta e="T448" id="Seg_4849" s="T447">break-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T449" id="Seg_4850" s="T448">Palna.[NOM]</ta>
            <ta e="T450" id="Seg_4851" s="T449">that</ta>
            <ta e="T451" id="Seg_4852" s="T450">brother-DU-ACC-3SG</ta>
            <ta e="T452" id="Seg_4853" s="T451">hit-PST.NAR-3SG.O</ta>
            <ta e="T453" id="Seg_4854" s="T452">brother.[NOM]-OBL.3SG</ta>
            <ta e="T454" id="Seg_4855" s="T453">road-GEN</ta>
            <ta e="T455" id="Seg_4856" s="T454">through</ta>
            <ta e="T456" id="Seg_4857" s="T455">forward</ta>
            <ta e="T457" id="Seg_4858" s="T456">run.[3SG.S]</ta>
            <ta e="T458" id="Seg_4859" s="T457">find-CO-3SG.O</ta>
            <ta e="T459" id="Seg_4860" s="T458">it.is.said</ta>
            <ta e="T460" id="Seg_4861" s="T459">catch-PST.NAR-3SG.O</ta>
            <ta e="T461" id="Seg_4862" s="T460">I.NOM</ta>
            <ta e="T462" id="Seg_4863" s="T461">come-RES-OPT-1SG.S</ta>
            <ta e="T463" id="Seg_4864" s="T462">this</ta>
            <ta e="T464" id="Seg_4865" s="T463">urine-ADJZ</ta>
            <ta e="T465" id="Seg_4866" s="T464">piece.[NOM]</ta>
            <ta e="T466" id="Seg_4867" s="T465">again</ta>
            <ta e="T467" id="Seg_4868" s="T466">what.[NOM]</ta>
            <ta e="T468" id="Seg_4869" s="T467">make-IPFV-3SG.O</ta>
            <ta e="T469" id="Seg_4870" s="T468">I.NOM</ta>
            <ta e="T470" id="Seg_4871" s="T469">speak-FUT-1SG.S</ta>
            <ta e="T471" id="Seg_4872" s="T470">then</ta>
            <ta e="T472" id="Seg_4873" s="T471">three</ta>
            <ta e="T473" id="Seg_4874" s="T472">brother-CRC-PL-EP-ACC</ta>
            <ta e="T474" id="Seg_4875" s="T473">all</ta>
            <ta e="T475" id="Seg_4876" s="T474">kill-PFV-CO-3PL</ta>
            <ta e="T476" id="Seg_4877" s="T475">one</ta>
            <ta e="T477" id="Seg_4878" s="T476">to-GEN</ta>
            <ta e="T478" id="Seg_4879" s="T477">up.to-GEN</ta>
            <ta e="T479" id="Seg_4880" s="T478">middle-LOC</ta>
            <ta e="T480" id="Seg_4881" s="T479">sometimes</ta>
            <ta e="T481" id="Seg_4882" s="T480">live-PST.NAR-3PL</ta>
            <ta e="T482" id="Seg_4883" s="T481">NEG</ta>
            <ta e="T483" id="Seg_4884" s="T482">what-EP-GEN</ta>
            <ta e="T484" id="Seg_4885" s="T483">noise.[NOM]</ta>
            <ta e="T485" id="Seg_4886" s="T484">NEG.EX.[3SG.S]</ta>
            <ta e="T486" id="Seg_4887" s="T485">leave-CO-3PL</ta>
            <ta e="T487" id="Seg_4888" s="T486">ide-ADJZ</ta>
            <ta e="T488" id="Seg_4889" s="T487">human.being-EP-ADJZ</ta>
            <ta e="T489" id="Seg_4890" s="T488">wife-ADJZ</ta>
            <ta e="T490" id="Seg_4891" s="T489">such-ADVZ</ta>
            <ta e="T491" id="Seg_4892" s="T490">mind-VBLZ-CO.[3SG.S]</ta>
            <ta e="T492" id="Seg_4893" s="T491">child-PL.[NOM]-1SG</ta>
            <ta e="T493" id="Seg_4894" s="T492">go.away-PST-3PL</ta>
            <ta e="T494" id="Seg_4895" s="T493">always</ta>
            <ta e="T495" id="Seg_4896" s="T494">whether</ta>
            <ta e="T496" id="Seg_4897" s="T495">tent-EP-VBLZ-CO-3PL</ta>
            <ta e="T497" id="Seg_4898" s="T496">%%</ta>
            <ta e="T498" id="Seg_4899" s="T497">as.if</ta>
            <ta e="T499" id="Seg_4900" s="T498">Selkup</ta>
            <ta e="T500" id="Seg_4901" s="T499">child-ACC-3SG</ta>
            <ta e="T501" id="Seg_4902" s="T500">this-GEN</ta>
            <ta e="T502" id="Seg_4903" s="T501">to.the.extent.of</ta>
            <ta e="T503" id="Seg_4904" s="T502">good-ADVZ</ta>
            <ta e="T504" id="Seg_4905" s="T503">say-PST.NAR-3SG.O</ta>
            <ta e="T505" id="Seg_4906" s="T504">I.NOM</ta>
            <ta e="T507" id="Seg_4907" s="T506">child.[NOM]-1SG</ta>
            <ta e="T508" id="Seg_4908" s="T507">bring.up-ATTEN-DUR-1SG.S</ta>
            <ta e="T509" id="Seg_4909" s="T508">%%.[NOM]-OBL.1SG</ta>
            <ta e="T510" id="Seg_4910" s="T509">front.part-ACC</ta>
            <ta e="T511" id="Seg_4911" s="T510">also</ta>
            <ta e="T512" id="Seg_4912" s="T511">wash-DUR-1SG.O</ta>
            <ta e="T513" id="Seg_4913" s="T512">Selkup-EP-ADJZ</ta>
            <ta e="T514" id="Seg_4914" s="T513">woman.[NOM]</ta>
            <ta e="T515" id="Seg_4915" s="T514">%%</ta>
            <ta e="T516" id="Seg_4916" s="T515">%%</ta>
            <ta e="T517" id="Seg_4917" s="T516">be-CO.[3SG.S]</ta>
            <ta e="T518" id="Seg_4918" s="T517">give.a.look-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_4919" s="T0">Пална.[NOM]</ta>
            <ta e="T2" id="Seg_4920" s="T1">Пална.[NOM]</ta>
            <ta e="T3" id="Seg_4921" s="T2">три</ta>
            <ta e="T4" id="Seg_4922" s="T3">брат-CRС-PL.[NOM]</ta>
            <ta e="T5" id="Seg_4923" s="T4">жить-PST.NAR-INFER-3PL</ta>
            <ta e="T6" id="Seg_4924" s="T5">селькуп-EP-ADJZ</ta>
            <ta e="T7" id="Seg_4925" s="T6">богатырь-EP-ADJZ</ta>
            <ta e="T8" id="Seg_4926" s="T7">старик.[NOM]</ta>
            <ta e="T9" id="Seg_4927" s="T8">быть-HAB-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T10" id="Seg_4928" s="T9">сила-PROPR-ADJZ</ta>
            <ta e="T11" id="Seg_4929" s="T10">старик.[NOM]</ta>
            <ta e="T12" id="Seg_4930" s="T11">самый</ta>
            <ta e="T13" id="Seg_4931" s="T12">старший</ta>
            <ta e="T14" id="Seg_4932" s="T13">брат-PL-EP-GEN</ta>
            <ta e="T15" id="Seg_4933" s="T14">середина-LOC-ADJZ</ta>
            <ta e="T16" id="Seg_4934" s="T15">брат-PL-EP-GEN</ta>
            <ta e="T17" id="Seg_4935" s="T16">лето-ADJZ</ta>
            <ta e="T18" id="Seg_4936" s="T17">олень-ADJZ</ta>
            <ta e="T19" id="Seg_4937" s="T18">догонять-PTCP.PRS</ta>
            <ta e="T20" id="Seg_4938" s="T19">быть-HAB-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T21" id="Seg_4939" s="T20">самый</ta>
            <ta e="T22" id="Seg_4940" s="T21">маленький</ta>
            <ta e="T23" id="Seg_4941" s="T22">брат.[NOM]-3SG</ta>
            <ta e="T24" id="Seg_4942" s="T23">багульник-GEN</ta>
            <ta e="T25" id="Seg_4943" s="T24">верхняя.часть-ADJZ</ta>
            <ta e="T26" id="Seg_4944" s="T25">догонять-PTCP.PRS</ta>
            <ta e="T27" id="Seg_4945" s="T26">быть-HAB-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T28" id="Seg_4946" s="T27">такой-ADVZ</ta>
            <ta e="T29" id="Seg_4947" s="T28">жить-PST.NAR-3PL</ta>
            <ta e="T30" id="Seg_4948" s="T29">а</ta>
            <ta e="T31" id="Seg_4949" s="T30">этот</ta>
            <ta e="T32" id="Seg_4950" s="T31">Пална-ADJZ</ta>
            <ta e="T33" id="Seg_4951" s="T32">старший</ta>
            <ta e="T34" id="Seg_4952" s="T33">брат.[NOM]-3SG</ta>
            <ta e="T35" id="Seg_4953" s="T34">настолько</ta>
            <ta e="T36" id="Seg_4954" s="T35">шаманская.мудрость-INSTR</ta>
            <ta e="T37" id="Seg_4955" s="T36">быть-HAB-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T38" id="Seg_4956" s="T37">откуда</ta>
            <ta e="T39" id="Seg_4957" s="T38">что.[NOM]</ta>
            <ta e="T40" id="Seg_4958" s="T39">прийти-FUT.[3SG.S]</ta>
            <ta e="T41" id="Seg_4959" s="T40">всё</ta>
            <ta e="T42" id="Seg_4960" s="T41">знать-PST.NAR-3SG.O</ta>
            <ta e="T43" id="Seg_4961" s="T42">петь-PTCP.PRS</ta>
            <ta e="T44" id="Seg_4962" s="T43">шаман-ADJZ</ta>
            <ta e="T45" id="Seg_4963" s="T44">старик.[NOM]</ta>
            <ta e="T46" id="Seg_4964" s="T45">быть-HAB-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T47" id="Seg_4965" s="T46">железо-ACC</ta>
            <ta e="T48" id="Seg_4966" s="T47">сделать-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T49" id="Seg_4967" s="T48">он.сам.[NOM]</ta>
            <ta e="T50" id="Seg_4968" s="T49">жить-ACTN-LOC-OBL.3SG</ta>
            <ta e="T51" id="Seg_4969" s="T50">зажечь-FRQ-CO.[3SG.S]</ta>
            <ta e="T52" id="Seg_4970" s="T51">железо-ACC</ta>
            <ta e="T53" id="Seg_4971" s="T52">так</ta>
            <ta e="T54" id="Seg_4972" s="T53">сделать-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T55" id="Seg_4973" s="T54">просто.так</ta>
            <ta e="T56" id="Seg_4974" s="T55">камень-PL-EP-GEN</ta>
            <ta e="T57" id="Seg_4975" s="T56">а</ta>
            <ta e="T58" id="Seg_4976" s="T57">русский-EP-PL.[NOM]</ta>
            <ta e="T59" id="Seg_4977" s="T58">сделать-IPFV-PST.NAR-3PL</ta>
            <ta e="T60" id="Seg_4978" s="T59">NEG</ta>
            <ta e="T61" id="Seg_4979" s="T60">знать-1SG.O</ta>
            <ta e="T62" id="Seg_4980" s="T61">что-EP-GEN</ta>
            <ta e="T63" id="Seg_4981" s="T62">из-ADV.EL</ta>
            <ta e="T65" id="Seg_4982" s="T64">он(а).[NOM]</ta>
            <ta e="T66" id="Seg_4983" s="T65">сделать-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T67" id="Seg_4984" s="T66">железо-TRL</ta>
            <ta e="T68" id="Seg_4985" s="T67">камень-ACC</ta>
            <ta e="T69" id="Seg_4986" s="T68">сделать-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T70" id="Seg_4987" s="T69">этот</ta>
            <ta e="T71" id="Seg_4988" s="T70">камень-GEN</ta>
            <ta e="T72" id="Seg_4989" s="T71">из-ADV.EL</ta>
            <ta e="T73" id="Seg_4990" s="T72">железо-ADJZ</ta>
            <ta e="T74" id="Seg_4991" s="T73">одежда-ACC</ta>
            <ta e="T75" id="Seg_4992" s="T74">сделать-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T77" id="Seg_4993" s="T76">железо-ADJZ</ta>
            <ta e="T78" id="Seg_4994" s="T77">лук.[NOM]</ta>
            <ta e="T79" id="Seg_4995" s="T78">сделать-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T81" id="Seg_4996" s="T80">железо-ADJZ</ta>
            <ta e="T82" id="Seg_4997" s="T81">стрела-ACC</ta>
            <ta e="T83" id="Seg_4998" s="T82">сделать-IPFV-PST.NAR-INFER-3SG.O</ta>
            <ta e="T84" id="Seg_4999" s="T83">это-OBL.3SG-INSTR</ta>
            <ta e="T85" id="Seg_5000" s="T84">и</ta>
            <ta e="T86" id="Seg_5001" s="T85">жить-PST.NAR.[3SG.S]</ta>
            <ta e="T87" id="Seg_5002" s="T86">а</ta>
            <ta e="T88" id="Seg_5003" s="T87">война-TR-CVB</ta>
            <ta e="T89" id="Seg_5004" s="T88">NEG</ta>
            <ta e="T90" id="Seg_5005" s="T89">ходить-FRQ-PST.NAR.[3SG.S]</ta>
            <ta e="T91" id="Seg_5006" s="T90">он(а)-EP-ALL</ta>
            <ta e="T92" id="Seg_5007" s="T91">война-TR-CVB</ta>
            <ta e="T93" id="Seg_5008" s="T92">ходить-FRQ-PST.NAR-3PL</ta>
            <ta e="T95" id="Seg_5009" s="T94">откуда</ta>
            <ta e="T96" id="Seg_5010" s="T95">только</ta>
            <ta e="T97" id="Seg_5011" s="T96">прийти-FUT-3PL</ta>
            <ta e="T98" id="Seg_5012" s="T97">он(а).[NOM]</ta>
            <ta e="T99" id="Seg_5013" s="T98">всё</ta>
            <ta e="T100" id="Seg_5014" s="T99">знать-3SG.O</ta>
            <ta e="T101" id="Seg_5015" s="T100">ненец-PL.[NOM]</ta>
            <ta e="T102" id="Seg_5016" s="T101">ходить-FRQ-PST.NAR-3PL</ta>
            <ta e="T103" id="Seg_5017" s="T102">война-TR-INF</ta>
            <ta e="T104" id="Seg_5018" s="T103">язь-ADJZ</ta>
            <ta e="T105" id="Seg_5019" s="T104">человек-EP-GEN</ta>
            <ta e="T106" id="Seg_5020" s="T105">сторона-LOC</ta>
            <ta e="T107" id="Seg_5021" s="T106">тоже</ta>
            <ta e="T108" id="Seg_5022" s="T107">три</ta>
            <ta e="T109" id="Seg_5023" s="T108">брат-CRС-PL.[NOM]</ta>
            <ta e="T110" id="Seg_5024" s="T109">быть-PST.NAR-INFER-3PL</ta>
            <ta e="T111" id="Seg_5025" s="T110">опять</ta>
            <ta e="T112" id="Seg_5026" s="T111">искать-CVB</ta>
            <ta e="T113" id="Seg_5027" s="T112">ходить-FRQ-PST.NAR-3PL</ta>
            <ta e="T114" id="Seg_5028" s="T113">этот_день.[NOM]</ta>
            <ta e="T115" id="Seg_5029" s="T114">человек-EP-ADJZ</ta>
            <ta e="T116" id="Seg_5030" s="T115">три</ta>
            <ta e="T117" id="Seg_5031" s="T116">брат-CRС-PL-EP-ACC</ta>
            <ta e="T118" id="Seg_5032" s="T117">он(а).[NOM]</ta>
            <ta e="T119" id="Seg_5033" s="T118">жить-PST.NAR.[3SG.S]</ta>
            <ta e="T120" id="Seg_5034" s="T119">раньше</ta>
            <ta e="T121" id="Seg_5035" s="T120">камень-ADJZ-море.[NOM]</ta>
            <ta e="T122" id="Seg_5036" s="T121">большая.река-LOC</ta>
            <ta e="T123" id="Seg_5037" s="T122">туруханский</ta>
            <ta e="T124" id="Seg_5038" s="T123">незаросший</ta>
            <ta e="T125" id="Seg_5039" s="T124">сторона-LOC</ta>
            <ta e="T126" id="Seg_5040" s="T125">песок-LOC</ta>
            <ta e="T127" id="Seg_5041" s="T126">язь-ADJZ</ta>
            <ta e="T128" id="Seg_5042" s="T127">человек-EP-ADJZ</ta>
            <ta e="T129" id="Seg_5043" s="T128">три</ta>
            <ta e="T130" id="Seg_5044" s="T129">брат-CRС-PL.[NOM]</ta>
            <ta e="T131" id="Seg_5045" s="T130">один</ta>
            <ta e="T132" id="Seg_5046" s="T131">целый</ta>
            <ta e="T133" id="Seg_5047" s="T132">середина-LOC</ta>
            <ta e="T134" id="Seg_5048" s="T133">такой-ADVZ</ta>
            <ta e="T135" id="Seg_5049" s="T134">быть-PST-3PL</ta>
            <ta e="T136" id="Seg_5050" s="T135">Пална-ADJZ</ta>
            <ta e="T137" id="Seg_5051" s="T136">три</ta>
            <ta e="T138" id="Seg_5052" s="T137">брат-CRС-PL-EP-ACC</ta>
            <ta e="T139" id="Seg_5053" s="T138">искать-CVB</ta>
            <ta e="T140" id="Seg_5054" s="T139">уйти-INF-собраться-CO-3PL</ta>
            <ta e="T141" id="Seg_5055" s="T140">а</ta>
            <ta e="T142" id="Seg_5056" s="T141">Пална-ADJZ</ta>
            <ta e="T143" id="Seg_5057" s="T142">брат-CRС-PL.[NOM]</ta>
            <ta e="T144" id="Seg_5058" s="T143">уйти-3PL</ta>
            <ta e="T145" id="Seg_5059" s="T144">брат-PL-ILL-OBL.3SG</ta>
            <ta e="T146" id="Seg_5060" s="T145">такой-ADVZ</ta>
            <ta e="T147" id="Seg_5061" s="T146">сказать-CO-3SG.O</ta>
            <ta e="T148" id="Seg_5062" s="T147">язь-ADJZ</ta>
            <ta e="T149" id="Seg_5063" s="T148">человек-EP-ADJZ</ta>
            <ta e="T150" id="Seg_5064" s="T149">три</ta>
            <ta e="T151" id="Seg_5065" s="T150">брат-CRС-PL.[NOM]</ta>
            <ta e="T152" id="Seg_5066" s="T151">INFER</ta>
            <ta e="T153" id="Seg_5067" s="T152">прийти-INF-собраться-INFER-3PL</ta>
            <ta e="T154" id="Seg_5068" s="T153">война-TR-CVB</ta>
            <ta e="T155" id="Seg_5069" s="T154">мы.PL.DAT</ta>
            <ta e="T156" id="Seg_5070" s="T155">вниз</ta>
            <ta e="T157" id="Seg_5071" s="T156">песок-ILL</ta>
            <ta e="T158" id="Seg_5072" s="T157">чум-EP-VBLZ-OPT-1PL</ta>
            <ta e="T159" id="Seg_5073" s="T158">песок-ILL</ta>
            <ta e="T160" id="Seg_5074" s="T159">чум-EP-VBLZ-FUT-1PL</ta>
            <ta e="T161" id="Seg_5075" s="T160">еда-ACC</ta>
            <ta e="T162" id="Seg_5076" s="T161">отправиться-TR-FUT-1PL</ta>
            <ta e="T163" id="Seg_5077" s="T162">человек-EP-PL.[NOM]</ta>
            <ta e="T164" id="Seg_5078" s="T163">(в).гости</ta>
            <ta e="T165" id="Seg_5079" s="T164">чум-TR-CVB</ta>
            <ta e="T166" id="Seg_5080" s="T165">прийти-FUT-3PL</ta>
            <ta e="T167" id="Seg_5081" s="T166">съесть-EP-FRQ-FUT-3PL</ta>
            <ta e="T168" id="Seg_5082" s="T167">вниз</ta>
            <ta e="T169" id="Seg_5083" s="T168">уйти-CO-3PL</ta>
            <ta e="T170" id="Seg_5084" s="T169">песок-ILL</ta>
            <ta e="T171" id="Seg_5085" s="T170">олений.обоз-TR-3PL</ta>
            <ta e="T172" id="Seg_5086" s="T171">чум-EP-VBLZ-CO-3PL</ta>
            <ta e="T173" id="Seg_5087" s="T172">сено-INSTR</ta>
            <ta e="T174" id="Seg_5088" s="T173">постелить-3PL</ta>
            <ta e="T175" id="Seg_5089" s="T174">чум-ILL</ta>
            <ta e="T176" id="Seg_5090" s="T175">тот-ADJZ</ta>
            <ta e="T177" id="Seg_5091" s="T176">сторона-LOC</ta>
            <ta e="T178" id="Seg_5092" s="T177">береста-INSTR</ta>
            <ta e="T179" id="Seg_5093" s="T178">постелить-3PL</ta>
            <ta e="T180" id="Seg_5094" s="T179">вверх-ADV.EL</ta>
            <ta e="T181" id="Seg_5095" s="T180">вниз</ta>
            <ta e="T182" id="Seg_5096" s="T181">опять</ta>
            <ta e="T183" id="Seg_5097" s="T182">сено-INSTR</ta>
            <ta e="T184" id="Seg_5098" s="T183">постелить-3PL</ta>
            <ta e="T185" id="Seg_5099" s="T184">брат-DU-OBL.3SG-ALL</ta>
            <ta e="T186" id="Seg_5100" s="T185">каша.[NOM]</ta>
            <ta e="T187" id="Seg_5101" s="T186">овёс.[NOM]</ta>
            <ta e="T188" id="Seg_5102" s="T187">свариться-CAUS-IMP.2PL</ta>
            <ta e="T189" id="Seg_5103" s="T188">этот_день.[NOM]</ta>
            <ta e="T190" id="Seg_5104" s="T189">прийти-FUT-3PL</ta>
            <ta e="T191" id="Seg_5105" s="T190">жир-EP-ADJZ</ta>
            <ta e="T192" id="Seg_5106" s="T191">юкола-ACC</ta>
            <ta e="T193" id="Seg_5107" s="T192">чум-ILL</ta>
            <ta e="T194" id="Seg_5108" s="T193">занести-TR-IMP.2PL</ta>
            <ta e="T195" id="Seg_5109" s="T194">каша-ACC</ta>
            <ta e="T196" id="Seg_5110" s="T195">овёс-ACC</ta>
            <ta e="T197" id="Seg_5111" s="T196">на.огонь</ta>
            <ta e="T198" id="Seg_5112" s="T197">свариться-CAUS-3PL</ta>
            <ta e="T199" id="Seg_5113" s="T198">некоторое.время</ta>
            <ta e="T200" id="Seg_5114" s="T199">вот</ta>
            <ta e="T201" id="Seg_5115" s="T200">сидеть-3PL</ta>
            <ta e="T202" id="Seg_5116" s="T201">солнце.[NOM]-3SG</ta>
            <ta e="T203" id="Seg_5117" s="T202">день-ADJZ</ta>
            <ta e="T204" id="Seg_5118" s="T203">середина-ADJZ</ta>
            <ta e="T205" id="Seg_5119" s="T204">промежуток-ILL</ta>
            <ta e="T206" id="Seg_5120" s="T205">повернуть-DECAUS-CO.[3SG.S]</ta>
            <ta e="T207" id="Seg_5121" s="T206">вперёд-ADV.ILL</ta>
            <ta e="T208" id="Seg_5122" s="T207">ветка-AUGM.[NOM]</ta>
            <ta e="T209" id="Seg_5123" s="T208">показаться-US-PFV.[3SG.S]</ta>
            <ta e="T210" id="Seg_5124" s="T209">три</ta>
            <ta e="T211" id="Seg_5125" s="T210">человек-EP-PL.[NOM]</ta>
            <ta e="T212" id="Seg_5126" s="T211">брат-EP-ADJZ</ta>
            <ta e="T213" id="Seg_5127" s="T212">ветка.[NOM]</ta>
            <ta e="T214" id="Seg_5128" s="T213">показаться-US-PFV-INFER.[3SG.S]</ta>
            <ta e="T215" id="Seg_5129" s="T214">брат-PL-ILL-OBL.3SG</ta>
            <ta e="T216" id="Seg_5130" s="T215">такой-ADVZ</ta>
            <ta e="T217" id="Seg_5131" s="T216">сказать-CO-3SG.O</ta>
            <ta e="T218" id="Seg_5132" s="T217">нож-ADJZ</ta>
            <ta e="T219" id="Seg_5133" s="T218">отказ-DIM-PL.[NOM]</ta>
            <ta e="T220" id="Seg_5134" s="T219">вниз</ta>
            <ta e="T221" id="Seg_5135" s="T220">спрятать(ся)-MULO-IMP.2PL</ta>
            <ta e="T222" id="Seg_5136" s="T221">обувь-GEN</ta>
            <ta e="T223" id="Seg_5137" s="T222">голенище-EP-GEN</ta>
            <ta e="T224" id="Seg_5138" s="T223">в</ta>
            <ta e="T225" id="Seg_5139" s="T224">засунуть-MOM-PFV-IMP.2PL</ta>
            <ta e="T226" id="Seg_5140" s="T225">потом</ta>
            <ta e="T227" id="Seg_5141" s="T226">такой-ADVZ</ta>
            <ta e="T228" id="Seg_5142" s="T227">сказать-IMP.2PL</ta>
            <ta e="T229" id="Seg_5143" s="T228">нож.[NOM]-1PL</ta>
            <ta e="T230" id="Seg_5144" s="T229">NEG.EX.[3SG.S]</ta>
            <ta e="T231" id="Seg_5145" s="T230">нож.[NOM]-2PL</ta>
            <ta e="T232" id="Seg_5146" s="T231">мы.PL.DAT</ta>
            <ta e="T233" id="Seg_5147" s="T232">дать-IMP.2PL</ta>
            <ta e="T234" id="Seg_5148" s="T233">ложка-ACC</ta>
            <ta e="T235" id="Seg_5149" s="T234">сделать-MULO-PFV-OPT-1PL</ta>
            <ta e="T236" id="Seg_5150" s="T235">съесть-EP-FRQ-SUP.1PL</ta>
            <ta e="T237" id="Seg_5151" s="T236">так</ta>
            <ta e="T238" id="Seg_5152" s="T237">спрятать(ся)-FRQ-DUR-CO-3SG.O</ta>
            <ta e="T239" id="Seg_5153" s="T238">младший</ta>
            <ta e="T240" id="Seg_5154" s="T239">брат.[NOM]-3SG</ta>
            <ta e="T241" id="Seg_5155" s="T240">сторона.чума.напротив.входа-LOC</ta>
            <ta e="T242" id="Seg_5156" s="T241">жить.[3SG.S]</ta>
            <ta e="T243" id="Seg_5157" s="T242">отказ-ACC-3SG</ta>
            <ta e="T244" id="Seg_5158" s="T243">туда</ta>
            <ta e="T245" id="Seg_5159" s="T244">вверх</ta>
            <ta e="T246" id="Seg_5160" s="T245">тыкать-RES-CO-3SG.O</ta>
            <ta e="T247" id="Seg_5161" s="T246">вверх-ADV.LOC</ta>
            <ta e="T248" id="Seg_5162" s="T247">огонь-GEN</ta>
            <ta e="T249" id="Seg_5163" s="T248">лабаз-ADJZ</ta>
            <ta e="T250" id="Seg_5164" s="T249">дерево.[NOM]</ta>
            <ta e="T251" id="Seg_5165" s="T250">привязать-HAB-IPFV-3PL</ta>
            <ta e="T252" id="Seg_5166" s="T251">так</ta>
            <ta e="T253" id="Seg_5167" s="T252">увидеть-CO-3PL</ta>
            <ta e="T254" id="Seg_5168" s="T253">вперёд-ADV.ILL</ta>
            <ta e="T255" id="Seg_5169" s="T254">показаться-US-PFV-CO.[3SG.S]</ta>
            <ta e="T256" id="Seg_5170" s="T255">три</ta>
            <ta e="T257" id="Seg_5171" s="T256">человек-EP-PL.[NOM]</ta>
            <ta e="T258" id="Seg_5172" s="T257">брат-EP-ADJZ</ta>
            <ta e="T259" id="Seg_5173" s="T258">ветка.[NOM]</ta>
            <ta e="T260" id="Seg_5174" s="T259">брат-DU-ACC-3SG</ta>
            <ta e="T261" id="Seg_5175" s="T260">речь-VBLZ-DUR-INF-начать-CO-3SG.O</ta>
            <ta e="T262" id="Seg_5176" s="T261">наружу</ta>
            <ta e="T263" id="Seg_5177" s="T262">INFER</ta>
            <ta e="T264" id="Seg_5178" s="T263">только</ta>
            <ta e="T265" id="Seg_5179" s="T264">выйти-INFER-3PL</ta>
            <ta e="T266" id="Seg_5180" s="T265">видать</ta>
            <ta e="T267" id="Seg_5181" s="T266">лук-ADJZ</ta>
            <ta e="T268" id="Seg_5182" s="T267">стрела-ACC</ta>
            <ta e="T269" id="Seg_5183" s="T268">вверх</ta>
            <ta e="T270" id="Seg_5184" s="T269">взять-MULO-PST.NAR-3PL</ta>
            <ta e="T271" id="Seg_5185" s="T270">вода-EL</ta>
            <ta e="T272" id="Seg_5186" s="T271">вверх</ta>
            <ta e="T274" id="Seg_5187" s="T273">стрелять-INFER-3PL</ta>
            <ta e="T275" id="Seg_5188" s="T274">наружу</ta>
            <ta e="T276" id="Seg_5189" s="T275">выйти-CVB</ta>
            <ta e="T277" id="Seg_5190" s="T276">так</ta>
            <ta e="T278" id="Seg_5191" s="T277">сказать-CO-3SG.O</ta>
            <ta e="T279" id="Seg_5192" s="T278">вверх</ta>
            <ta e="T280" id="Seg_5193" s="T279">выйти-CVB</ta>
            <ta e="T281" id="Seg_5194" s="T280">съесть-EP-FRQ-IMP.2PL</ta>
            <ta e="T282" id="Seg_5195" s="T281">лоб-EP-OBL.1DU-INSTR</ta>
            <ta e="T283" id="Seg_5196" s="T282">война-TR-OPT-1PL</ta>
            <ta e="T284" id="Seg_5197" s="T283">выйти-CVB</ta>
            <ta e="T285" id="Seg_5198" s="T284">пространство.снаружи-LOC</ta>
            <ta e="T286" id="Seg_5199" s="T285">вода-EL</ta>
            <ta e="T287" id="Seg_5200" s="T286">вверх</ta>
            <ta e="T288" id="Seg_5201" s="T287">и</ta>
            <ta e="T289" id="Seg_5202" s="T288">что.[NOM]</ta>
            <ta e="T290" id="Seg_5203" s="T289">сделать-INF-собраться-CO-2PL</ta>
            <ta e="T291" id="Seg_5204" s="T290">вверх</ta>
            <ta e="T292" id="Seg_5205" s="T291">выйти-CVB</ta>
            <ta e="T293" id="Seg_5206" s="T292">съесть-EP-FRQ-IMP.2PL</ta>
            <ta e="T294" id="Seg_5207" s="T293">а</ta>
            <ta e="T295" id="Seg_5208" s="T294">такой-ADVZ</ta>
            <ta e="T296" id="Seg_5209" s="T295">быть-PST.[3SG.S]</ta>
            <ta e="T297" id="Seg_5210" s="T296">старший</ta>
            <ta e="T298" id="Seg_5211" s="T297">брат.[NOM]-3SG</ta>
            <ta e="T299" id="Seg_5212" s="T298">вверх</ta>
            <ta e="T300" id="Seg_5213" s="T299">выйти-CVB</ta>
            <ta e="T301" id="Seg_5214" s="T300">съесть-EP-FRQ-OPT-1PL</ta>
            <ta e="T302" id="Seg_5215" s="T301">вверх</ta>
            <ta e="T303" id="Seg_5216" s="T302">RFL</ta>
            <ta e="T304" id="Seg_5217" s="T303">принести-TR-3PL</ta>
            <ta e="T305" id="Seg_5218" s="T304">лук-ADJZ</ta>
            <ta e="T306" id="Seg_5219" s="T305">стрела-ACC-3PL</ta>
            <ta e="T307" id="Seg_5220" s="T306">вниз-ADV.LOC</ta>
            <ta e="T308" id="Seg_5221" s="T307">ветка-LOC-OBL.3PL</ta>
            <ta e="T309" id="Seg_5222" s="T308">положить-CVB</ta>
            <ta e="T310" id="Seg_5223" s="T309">оставить-MOM-CO-3PL</ta>
            <ta e="T311" id="Seg_5224" s="T310">чум-ILL</ta>
            <ta e="T312" id="Seg_5225" s="T311">INFER</ta>
            <ta e="T313" id="Seg_5226" s="T312">RFL</ta>
            <ta e="T314" id="Seg_5227" s="T313">войти-INFER-3PL</ta>
            <ta e="T315" id="Seg_5228" s="T314">большой</ta>
            <ta e="T316" id="Seg_5229" s="T315">язь-ADJZ</ta>
            <ta e="T317" id="Seg_5230" s="T316">человек-EP-ADJZ</ta>
            <ta e="T318" id="Seg_5231" s="T317">богатырь.[NOM]</ta>
            <ta e="T319" id="Seg_5232" s="T318">Пална-GEN</ta>
            <ta e="T320" id="Seg_5233" s="T319">с</ta>
            <ta e="T321" id="Seg_5234" s="T320">один</ta>
            <ta e="T322" id="Seg_5235" s="T321">сторона-ILL</ta>
            <ta e="T323" id="Seg_5236" s="T322">INFER</ta>
            <ta e="T324" id="Seg_5237" s="T323">сесть-INFER-3PL</ta>
            <ta e="T325" id="Seg_5238" s="T324">вниз.по.течению.реки-ADJZ</ta>
            <ta e="T326" id="Seg_5239" s="T325">олень-ADJZ</ta>
            <ta e="T327" id="Seg_5240" s="T326">догонять-PTCP.PRS</ta>
            <ta e="T328" id="Seg_5241" s="T327">брат-OBL.3SG-COM</ta>
            <ta e="T329" id="Seg_5242" s="T328">тоже</ta>
            <ta e="T330" id="Seg_5243" s="T329">один</ta>
            <ta e="T331" id="Seg_5244" s="T330">сторона-ILL</ta>
            <ta e="T332" id="Seg_5245" s="T331">INFER</ta>
            <ta e="T333" id="Seg_5246" s="T332">сесть-INFER-3PL</ta>
            <ta e="T334" id="Seg_5247" s="T333">три</ta>
            <ta e="T335" id="Seg_5248" s="T334">брат-CRС-PL.[NOM]</ta>
            <ta e="T336" id="Seg_5249" s="T335">всё</ta>
            <ta e="T337" id="Seg_5250" s="T336">один</ta>
            <ta e="T338" id="Seg_5251" s="T337">сторона-ILL</ta>
            <ta e="T339" id="Seg_5252" s="T338">сесть-OPT-3PL</ta>
            <ta e="T340" id="Seg_5253" s="T339">потом</ta>
            <ta e="T341" id="Seg_5254" s="T340">так</ta>
            <ta e="T342" id="Seg_5255" s="T341">сказать-PST-3PL</ta>
            <ta e="T343" id="Seg_5256" s="T342">нож.[NOM]-1PL</ta>
            <ta e="T344" id="Seg_5257" s="T343">ли</ta>
            <ta e="T345" id="Seg_5258" s="T344">забыть-PST.NAR-1PL</ta>
            <ta e="T346" id="Seg_5259" s="T345">нож.[NOM]-2PL</ta>
            <ta e="T347" id="Seg_5260" s="T346">быть-CO.[3SG.S]</ta>
            <ta e="T348" id="Seg_5261" s="T347">нож-INSTR</ta>
            <ta e="T349" id="Seg_5262" s="T348">мы.PL.ACC</ta>
            <ta e="T350" id="Seg_5263" s="T349">дать-IMP.2PL</ta>
            <ta e="T351" id="Seg_5264" s="T350">съесть-EP-FRQ-INF</ta>
            <ta e="T352" id="Seg_5265" s="T351">ложка-ACC</ta>
            <ta e="T353" id="Seg_5266" s="T352">сделать-MULO-OPT-1PL</ta>
            <ta e="T354" id="Seg_5267" s="T353">нож-ACC-3PL</ta>
            <ta e="T355" id="Seg_5268" s="T354">всё</ta>
            <ta e="T356" id="Seg_5269" s="T355">дать-CO-3PL</ta>
            <ta e="T357" id="Seg_5270" s="T356">язь-ADJZ</ta>
            <ta e="T358" id="Seg_5271" s="T357">человек-EP-ADJZ</ta>
            <ta e="T359" id="Seg_5272" s="T358">три</ta>
            <ta e="T360" id="Seg_5273" s="T359">брат-CRС-PL.[NOM]</ta>
            <ta e="T361" id="Seg_5274" s="T360">а</ta>
            <ta e="T362" id="Seg_5275" s="T361">Пална-ADJZ</ta>
            <ta e="T363" id="Seg_5276" s="T362">три</ta>
            <ta e="T364" id="Seg_5277" s="T363">брат-CRС-PL.[NOM]</ta>
            <ta e="T365" id="Seg_5278" s="T364">раньше</ta>
            <ta e="T366" id="Seg_5279" s="T365">RFL</ta>
            <ta e="T367" id="Seg_5280" s="T366">слово-VBLZ-DUR-PST-3PL</ta>
            <ta e="T368" id="Seg_5281" s="T367">старший</ta>
            <ta e="T369" id="Seg_5282" s="T368">брат.[NOM]-3SG</ta>
            <ta e="T370" id="Seg_5283" s="T369">как</ta>
            <ta e="T371" id="Seg_5284" s="T370">нож-INSTR</ta>
            <ta e="T372" id="Seg_5285" s="T371">ударить-INFER-3SG.O</ta>
            <ta e="T373" id="Seg_5286" s="T372">он(а)-EP-PL</ta>
            <ta e="T374" id="Seg_5287" s="T373">тоже</ta>
            <ta e="T375" id="Seg_5288" s="T374">такой-ADVZ</ta>
            <ta e="T376" id="Seg_5289" s="T375">ударить-INFER-3PL</ta>
            <ta e="T377" id="Seg_5290" s="T376">старший</ta>
            <ta e="T378" id="Seg_5291" s="T377">старик.[NOM]</ta>
            <ta e="T379" id="Seg_5292" s="T378">один</ta>
            <ta e="T380" id="Seg_5293" s="T379">середина-LOC</ta>
            <ta e="T381" id="Seg_5294" s="T380">некоторое.время</ta>
            <ta e="T382" id="Seg_5295" s="T381">резать.[3SG.S]</ta>
            <ta e="T383" id="Seg_5296" s="T382">глаз-INSTR</ta>
            <ta e="T384" id="Seg_5297" s="T383">ребенок-ACC</ta>
            <ta e="T385" id="Seg_5298" s="T384">грызть-HAB-3SG.O</ta>
            <ta e="T386" id="Seg_5299" s="T385">нож-INSTR</ta>
            <ta e="T387" id="Seg_5300" s="T386">туда</ta>
            <ta e="T388" id="Seg_5301" s="T387">RFL</ta>
            <ta e="T389" id="Seg_5302" s="T388">ударить-3PL</ta>
            <ta e="T390" id="Seg_5303" s="T389">язь-ADJZ</ta>
            <ta e="T391" id="Seg_5304" s="T390">человек-EP-ADJZ</ta>
            <ta e="T392" id="Seg_5305" s="T391">три</ta>
            <ta e="T393" id="Seg_5306" s="T392">брат-CRС-PL-EP-ACC</ta>
            <ta e="T394" id="Seg_5307" s="T393">лето-ADJZ</ta>
            <ta e="T395" id="Seg_5308" s="T394">олень-ADJZ</ta>
            <ta e="T396" id="Seg_5309" s="T395">догонять-PTCP.PRS</ta>
            <ta e="T397" id="Seg_5310" s="T396">брат-ACC-3SG</ta>
            <ta e="T398" id="Seg_5311" s="T397">только</ta>
            <ta e="T399" id="Seg_5312" s="T398">ударить-3SG.O</ta>
            <ta e="T400" id="Seg_5313" s="T399">только</ta>
            <ta e="T401" id="Seg_5314" s="T400">кончиться-PTCP.PST</ta>
            <ta e="T402" id="Seg_5315" s="T401">земля-ACC</ta>
            <ta e="T403" id="Seg_5316" s="T402">ударить-3SG.O</ta>
            <ta e="T404" id="Seg_5317" s="T403">Пална-GEN</ta>
            <ta e="T405" id="Seg_5318" s="T404">брат.[NOM]-3SG</ta>
            <ta e="T406" id="Seg_5319" s="T405">лето-ADJZ</ta>
            <ta e="T407" id="Seg_5320" s="T406">олень-ADJZ</ta>
            <ta e="T408" id="Seg_5321" s="T407">догонять-PTCP.PRS</ta>
            <ta e="T409" id="Seg_5322" s="T408">брат.[NOM]-3SG</ta>
            <ta e="T410" id="Seg_5323" s="T409">тоже</ta>
            <ta e="T411" id="Seg_5324" s="T410">потом</ta>
            <ta e="T412" id="Seg_5325" s="T411">побежать.[3SG.S]</ta>
            <ta e="T413" id="Seg_5326" s="T412">догонять-CVB</ta>
            <ta e="T414" id="Seg_5327" s="T413">отправиться-TR-3SG.O</ta>
            <ta e="T415" id="Seg_5328" s="T414">отказ-ACC-3SG</ta>
            <ta e="T416" id="Seg_5329" s="T415">схватить-CVB</ta>
            <ta e="T417" id="Seg_5330" s="T416">догонять-TR-CO-3SG.O</ta>
            <ta e="T418" id="Seg_5331" s="T417">тот</ta>
            <ta e="T419" id="Seg_5332" s="T418">два</ta>
            <ta e="T420" id="Seg_5333" s="T419">брат-CRС-DU.[NOM]</ta>
            <ta e="T421" id="Seg_5334" s="T420">чум-LOC</ta>
            <ta e="T422" id="Seg_5335" s="T421">остаться-3PL</ta>
            <ta e="T423" id="Seg_5336" s="T422">то.ли</ta>
            <ta e="T424" id="Seg_5337" s="T423">отправиться-CO-3SG.O</ta>
            <ta e="T425" id="Seg_5338" s="T424">то.ли</ta>
            <ta e="T426" id="Seg_5339" s="T425">ударить-CO-3PL</ta>
            <ta e="T427" id="Seg_5340" s="T426">догонять-CVB</ta>
            <ta e="T428" id="Seg_5341" s="T427">отправиться-TR-IPFV-3SG.O</ta>
            <ta e="T429" id="Seg_5342" s="T428">вперёд-ADV.LOC</ta>
            <ta e="T430" id="Seg_5343" s="T429">догнать-MOM-CVB</ta>
            <ta e="T431" id="Seg_5344" s="T430">только</ta>
            <ta e="T432" id="Seg_5345" s="T431">рубить-MULO-CO-3SG.O</ta>
            <ta e="T433" id="Seg_5346" s="T432">пятка.[NOM]-3SG</ta>
            <ta e="T434" id="Seg_5347" s="T433">рубить-MOM-CO-3SG.O</ta>
            <ta e="T435" id="Seg_5348" s="T434">лицом.вниз</ta>
            <ta e="T436" id="Seg_5349" s="T435">упасть.[3SG.S]</ta>
            <ta e="T437" id="Seg_5350" s="T436">язь-ADJZ</ta>
            <ta e="T438" id="Seg_5351" s="T437">человек.[NOM]</ta>
            <ta e="T439" id="Seg_5352" s="T438">два-ITER.NUM</ta>
            <ta e="T440" id="Seg_5353" s="T439">едва</ta>
            <ta e="T441" id="Seg_5354" s="T440">рубить-MOM-CO-3SG.O</ta>
            <ta e="T442" id="Seg_5355" s="T441">отказ-GEN</ta>
            <ta e="T443" id="Seg_5356" s="T442">середина-GEN</ta>
            <ta e="T444" id="Seg_5357" s="T443">спокойный-ADVZ</ta>
            <ta e="T445" id="Seg_5358" s="T444">схватить-CO-3SG.O</ta>
            <ta e="T446" id="Seg_5359" s="T445">нога.[NOM]-3SG</ta>
            <ta e="T448" id="Seg_5360" s="T447">ломать-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T449" id="Seg_5361" s="T448">Пална.[NOM]</ta>
            <ta e="T450" id="Seg_5362" s="T449">тот</ta>
            <ta e="T451" id="Seg_5363" s="T450">брат-DU-ACC-3SG</ta>
            <ta e="T452" id="Seg_5364" s="T451">ударить-PST.NAR-3SG.O</ta>
            <ta e="T453" id="Seg_5365" s="T452">брат.[NOM]-OBL.3SG</ta>
            <ta e="T454" id="Seg_5366" s="T453">дорога-GEN</ta>
            <ta e="T455" id="Seg_5367" s="T454">сквозь</ta>
            <ta e="T456" id="Seg_5368" s="T455">вперёд</ta>
            <ta e="T457" id="Seg_5369" s="T456">побежать.[3SG.S]</ta>
            <ta e="T458" id="Seg_5370" s="T457">найти-CO-3SG.O</ta>
            <ta e="T459" id="Seg_5371" s="T458">мол</ta>
            <ta e="T460" id="Seg_5372" s="T459">схватить-PST.NAR-3SG.O</ta>
            <ta e="T461" id="Seg_5373" s="T460">я.NOM</ta>
            <ta e="T462" id="Seg_5374" s="T461">прийти-RES-OPT-1SG.S</ta>
            <ta e="T463" id="Seg_5375" s="T462">этот</ta>
            <ta e="T464" id="Seg_5376" s="T463">моча-ADJZ</ta>
            <ta e="T465" id="Seg_5377" s="T464">кусок.[NOM]</ta>
            <ta e="T466" id="Seg_5378" s="T465">опять</ta>
            <ta e="T467" id="Seg_5379" s="T466">что.[NOM]</ta>
            <ta e="T468" id="Seg_5380" s="T467">сделать-IPFV-3SG.O</ta>
            <ta e="T469" id="Seg_5381" s="T468">я.NOM</ta>
            <ta e="T470" id="Seg_5382" s="T469">сказать-FUT-1SG.S</ta>
            <ta e="T471" id="Seg_5383" s="T470">потом</ta>
            <ta e="T472" id="Seg_5384" s="T471">три</ta>
            <ta e="T473" id="Seg_5385" s="T472">брат-CRС-PL-EP-ACC</ta>
            <ta e="T474" id="Seg_5386" s="T473">всё</ta>
            <ta e="T475" id="Seg_5387" s="T474">убить-PFV-CO-3PL</ta>
            <ta e="T476" id="Seg_5388" s="T475">один</ta>
            <ta e="T477" id="Seg_5389" s="T476">по-GEN</ta>
            <ta e="T478" id="Seg_5390" s="T477">до-GEN</ta>
            <ta e="T479" id="Seg_5391" s="T478">середина-LOC</ta>
            <ta e="T480" id="Seg_5392" s="T479">некоторое.время</ta>
            <ta e="T481" id="Seg_5393" s="T480">жить-PST.NAR-3PL</ta>
            <ta e="T482" id="Seg_5394" s="T481">NEG</ta>
            <ta e="T483" id="Seg_5395" s="T482">что-EP-GEN</ta>
            <ta e="T484" id="Seg_5396" s="T483">шум.[NOM]</ta>
            <ta e="T485" id="Seg_5397" s="T484">NEG.EX.[3SG.S]</ta>
            <ta e="T486" id="Seg_5398" s="T485">отправиться-CO-3PL</ta>
            <ta e="T487" id="Seg_5399" s="T486">язь-ADJZ</ta>
            <ta e="T488" id="Seg_5400" s="T487">человек-EP-ADJZ</ta>
            <ta e="T489" id="Seg_5401" s="T488">жена-ADJZ</ta>
            <ta e="T490" id="Seg_5402" s="T489">такой-ADVZ</ta>
            <ta e="T491" id="Seg_5403" s="T490">ум-VBLZ-CO.[3SG.S]</ta>
            <ta e="T492" id="Seg_5404" s="T491">ребенок-PL.[NOM]-1SG</ta>
            <ta e="T493" id="Seg_5405" s="T492">уйти-PST-3PL</ta>
            <ta e="T494" id="Seg_5406" s="T493">всегда</ta>
            <ta e="T495" id="Seg_5407" s="T494">ли</ta>
            <ta e="T496" id="Seg_5408" s="T495">чум-EP-VBLZ-CO-3PL</ta>
            <ta e="T497" id="Seg_5409" s="T496">%%</ta>
            <ta e="T498" id="Seg_5410" s="T497">будто</ta>
            <ta e="T499" id="Seg_5411" s="T498">селькуп</ta>
            <ta e="T500" id="Seg_5412" s="T499">ребенок-ACC-3SG</ta>
            <ta e="T501" id="Seg_5413" s="T500">этот-GEN</ta>
            <ta e="T502" id="Seg_5414" s="T501">в.меру</ta>
            <ta e="T503" id="Seg_5415" s="T502">хороший-ADVZ</ta>
            <ta e="T504" id="Seg_5416" s="T503">сказать-PST.NAR-3SG.O</ta>
            <ta e="T505" id="Seg_5417" s="T504">я.NOM</ta>
            <ta e="T507" id="Seg_5418" s="T506">ребенок.[NOM]-1SG</ta>
            <ta e="T508" id="Seg_5419" s="T507">вырастить-ATTEN-DUR-1SG.S</ta>
            <ta e="T509" id="Seg_5420" s="T508">%%.[NOM]-OBL.1SG</ta>
            <ta e="T510" id="Seg_5421" s="T509">перед-ACC</ta>
            <ta e="T511" id="Seg_5422" s="T510">тоже</ta>
            <ta e="T512" id="Seg_5423" s="T511">умыться-DUR-1SG.O</ta>
            <ta e="T513" id="Seg_5424" s="T512">селькуп-EP-ADJZ</ta>
            <ta e="T514" id="Seg_5425" s="T513">женщина.[NOM]</ta>
            <ta e="T515" id="Seg_5426" s="T514">%%</ta>
            <ta e="T516" id="Seg_5427" s="T515">%%</ta>
            <ta e="T517" id="Seg_5428" s="T516">быть-CO.[3SG.S]</ta>
            <ta e="T518" id="Seg_5429" s="T517">взглянуть-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_5430" s="T0">nprop-n:case3</ta>
            <ta e="T2" id="Seg_5431" s="T1">nprop-n:case3</ta>
            <ta e="T3" id="Seg_5432" s="T2">num</ta>
            <ta e="T4" id="Seg_5433" s="T3">n-n&gt;n-n:num-n:case3</ta>
            <ta e="T5" id="Seg_5434" s="T4">v-v:tense-v:mood2-v:pn</ta>
            <ta e="T6" id="Seg_5435" s="T5">n-n:(ins)-n&gt;adj</ta>
            <ta e="T7" id="Seg_5436" s="T6">n-n:(ins)-n&gt;adj</ta>
            <ta e="T8" id="Seg_5437" s="T7">n-n:case3</ta>
            <ta e="T9" id="Seg_5438" s="T8">v-v&gt;v-v:tense-v:mood2-v:pn</ta>
            <ta e="T10" id="Seg_5439" s="T9">n-n&gt;n-n&gt;adj</ta>
            <ta e="T11" id="Seg_5440" s="T10">n-n:case3</ta>
            <ta e="T12" id="Seg_5441" s="T11">adj</ta>
            <ta e="T13" id="Seg_5442" s="T12">adj</ta>
            <ta e="T14" id="Seg_5443" s="T13">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T15" id="Seg_5444" s="T14">n-n:case3-n&gt;adj</ta>
            <ta e="T16" id="Seg_5445" s="T15">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T17" id="Seg_5446" s="T16">n-n&gt;adj</ta>
            <ta e="T18" id="Seg_5447" s="T17">n-n&gt;adj</ta>
            <ta e="T19" id="Seg_5448" s="T18">v-v&gt;ptcp</ta>
            <ta e="T20" id="Seg_5449" s="T19">v-v&gt;v-v:tense-v:mood2-v:pn</ta>
            <ta e="T21" id="Seg_5450" s="T20">adv</ta>
            <ta e="T22" id="Seg_5451" s="T21">adj</ta>
            <ta e="T23" id="Seg_5452" s="T22">n-n:case1-n:poss</ta>
            <ta e="T24" id="Seg_5453" s="T23">n-n:case3</ta>
            <ta e="T25" id="Seg_5454" s="T24">n-n&gt;adj</ta>
            <ta e="T26" id="Seg_5455" s="T25">v-v&gt;ptcp</ta>
            <ta e="T27" id="Seg_5456" s="T26">v-v&gt;v-v:tense-v:mood2-v:pn</ta>
            <ta e="T28" id="Seg_5457" s="T27">dem-adj&gt;adv</ta>
            <ta e="T29" id="Seg_5458" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_5459" s="T29">conj</ta>
            <ta e="T31" id="Seg_5460" s="T30">dem</ta>
            <ta e="T32" id="Seg_5461" s="T31">nprop-n&gt;adj</ta>
            <ta e="T33" id="Seg_5462" s="T32">adj</ta>
            <ta e="T34" id="Seg_5463" s="T33">n-n:case1-n:poss</ta>
            <ta e="T35" id="Seg_5464" s="T34">adv</ta>
            <ta e="T36" id="Seg_5465" s="T35">n-n:case3</ta>
            <ta e="T37" id="Seg_5466" s="T36">v-v&gt;v-v:tense-v:mood2-v:pn</ta>
            <ta e="T38" id="Seg_5467" s="T37">interrog</ta>
            <ta e="T39" id="Seg_5468" s="T38">interrog-n:case3</ta>
            <ta e="T40" id="Seg_5469" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_5470" s="T40">quant</ta>
            <ta e="T42" id="Seg_5471" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_5472" s="T42">v-v&gt;ptcp</ta>
            <ta e="T44" id="Seg_5473" s="T43">n-n&gt;adj</ta>
            <ta e="T45" id="Seg_5474" s="T44">n-n:case3</ta>
            <ta e="T46" id="Seg_5475" s="T45">v-v&gt;v-v:tense-v:mood2-v:pn</ta>
            <ta e="T47" id="Seg_5476" s="T46">n-n:case3</ta>
            <ta e="T48" id="Seg_5477" s="T47">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_5478" s="T48">pro-n:case3</ta>
            <ta e="T50" id="Seg_5479" s="T49">v-v&gt;n-n:case2-n:obl.poss</ta>
            <ta e="T51" id="Seg_5480" s="T50">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T52" id="Seg_5481" s="T51">n-n:case3</ta>
            <ta e="T53" id="Seg_5482" s="T52">adv</ta>
            <ta e="T54" id="Seg_5483" s="T53">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_5484" s="T54">adv</ta>
            <ta e="T56" id="Seg_5485" s="T55">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T57" id="Seg_5486" s="T56">conj</ta>
            <ta e="T58" id="Seg_5487" s="T57">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T59" id="Seg_5488" s="T58">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_5489" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_5490" s="T60">v-v:pn</ta>
            <ta e="T62" id="Seg_5491" s="T61">interrog-n:(ins)-n:case3</ta>
            <ta e="T63" id="Seg_5492" s="T62">pp-adv:advcase</ta>
            <ta e="T65" id="Seg_5493" s="T64">pers-n:case3</ta>
            <ta e="T66" id="Seg_5494" s="T65">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_5495" s="T66">n-n:case3</ta>
            <ta e="T68" id="Seg_5496" s="T67">n-n:case3</ta>
            <ta e="T69" id="Seg_5497" s="T68">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_5498" s="T69">dem</ta>
            <ta e="T71" id="Seg_5499" s="T70">n-n:case3</ta>
            <ta e="T72" id="Seg_5500" s="T71">pp-adv:advcase</ta>
            <ta e="T73" id="Seg_5501" s="T72">n-n&gt;adj</ta>
            <ta e="T74" id="Seg_5502" s="T73">n-n:case3</ta>
            <ta e="T75" id="Seg_5503" s="T74">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_5504" s="T76">n-n&gt;adj</ta>
            <ta e="T78" id="Seg_5505" s="T77">n-n:case3</ta>
            <ta e="T79" id="Seg_5506" s="T78">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_5507" s="T80">n-n&gt;adj</ta>
            <ta e="T82" id="Seg_5508" s="T81">n-n:case3</ta>
            <ta e="T83" id="Seg_5509" s="T82">v-v&gt;v-v:tense-v:mood2-v:pn</ta>
            <ta e="T84" id="Seg_5510" s="T83">pro-n:obl.poss-n:case2</ta>
            <ta e="T85" id="Seg_5511" s="T84">conj</ta>
            <ta e="T86" id="Seg_5512" s="T85">v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_5513" s="T86">conj</ta>
            <ta e="T88" id="Seg_5514" s="T87">n-n&gt;v-v&gt;adv</ta>
            <ta e="T89" id="Seg_5515" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_5516" s="T89">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_5517" s="T90">pers-n:(ins)-n:case3</ta>
            <ta e="T92" id="Seg_5518" s="T91">n-n&gt;v-v&gt;adv</ta>
            <ta e="T93" id="Seg_5519" s="T92">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T95" id="Seg_5520" s="T94">interrog</ta>
            <ta e="T96" id="Seg_5521" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_5522" s="T96">v-v:tense-v:pn</ta>
            <ta e="T98" id="Seg_5523" s="T97">pers-n:case3</ta>
            <ta e="T99" id="Seg_5524" s="T98">quant</ta>
            <ta e="T100" id="Seg_5525" s="T99">v-v:pn</ta>
            <ta e="T101" id="Seg_5526" s="T100">n-n:num-n:case3</ta>
            <ta e="T102" id="Seg_5527" s="T101">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_5528" s="T102">n-n&gt;v-v:inf</ta>
            <ta e="T104" id="Seg_5529" s="T103">n-n&gt;adj</ta>
            <ta e="T105" id="Seg_5530" s="T104">n-n:(ins)-n:case3</ta>
            <ta e="T106" id="Seg_5531" s="T105">n-n:case3</ta>
            <ta e="T107" id="Seg_5532" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_5533" s="T107">num</ta>
            <ta e="T109" id="Seg_5534" s="T108">n-n&gt;n-n:num-n:case3</ta>
            <ta e="T110" id="Seg_5535" s="T109">v-v:tense-v:mood2-v:pn</ta>
            <ta e="T111" id="Seg_5536" s="T110">adv</ta>
            <ta e="T112" id="Seg_5537" s="T111">v-v&gt;adv</ta>
            <ta e="T113" id="Seg_5538" s="T112">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_5539" s="T113">dem-n-n:case3</ta>
            <ta e="T115" id="Seg_5540" s="T114">n-n:(ins)-n&gt;adj</ta>
            <ta e="T116" id="Seg_5541" s="T115">num</ta>
            <ta e="T117" id="Seg_5542" s="T116">n-n&gt;n-n:num-n:(ins)-n:case3</ta>
            <ta e="T118" id="Seg_5543" s="T117">pers-n:case3</ta>
            <ta e="T119" id="Seg_5544" s="T118">v-v:tense-v:pn</ta>
            <ta e="T120" id="Seg_5545" s="T119">adv</ta>
            <ta e="T121" id="Seg_5546" s="T120">n-n&gt;adj-n-n:case3</ta>
            <ta e="T122" id="Seg_5547" s="T121">n-n:case3</ta>
            <ta e="T123" id="Seg_5548" s="T122">%%</ta>
            <ta e="T124" id="Seg_5549" s="T123">adj</ta>
            <ta e="T125" id="Seg_5550" s="T124">n-n:case3</ta>
            <ta e="T126" id="Seg_5551" s="T125">n-n:case3</ta>
            <ta e="T127" id="Seg_5552" s="T126">n-n&gt;adj</ta>
            <ta e="T128" id="Seg_5553" s="T127">n-n:(ins)-n&gt;adj</ta>
            <ta e="T129" id="Seg_5554" s="T128">num</ta>
            <ta e="T130" id="Seg_5555" s="T129">n-n&gt;n-n:num-n:case3</ta>
            <ta e="T131" id="Seg_5556" s="T130">num</ta>
            <ta e="T132" id="Seg_5557" s="T131">adj</ta>
            <ta e="T133" id="Seg_5558" s="T132">n-n:case3</ta>
            <ta e="T134" id="Seg_5559" s="T133">dem-adj&gt;adv</ta>
            <ta e="T135" id="Seg_5560" s="T134">v-v:tense-v:pn</ta>
            <ta e="T136" id="Seg_5561" s="T135">nprop-n&gt;adj</ta>
            <ta e="T137" id="Seg_5562" s="T136">num</ta>
            <ta e="T138" id="Seg_5563" s="T137">n-n&gt;n-n:num-n:(ins)-n:case3</ta>
            <ta e="T139" id="Seg_5564" s="T138">v-v&gt;adv</ta>
            <ta e="T140" id="Seg_5565" s="T139">v-v:inf-v-v:ins-v:pn</ta>
            <ta e="T141" id="Seg_5566" s="T140">conj</ta>
            <ta e="T142" id="Seg_5567" s="T141">nprop-n&gt;adj</ta>
            <ta e="T143" id="Seg_5568" s="T142">n-n&gt;n-n:num-n:case3</ta>
            <ta e="T144" id="Seg_5569" s="T143">v-v:pn</ta>
            <ta e="T145" id="Seg_5570" s="T144">n-n:num-n:case2-n:obl.poss</ta>
            <ta e="T146" id="Seg_5571" s="T145">dem-adj&gt;adv</ta>
            <ta e="T147" id="Seg_5572" s="T146">v-v:ins-v:pn</ta>
            <ta e="T148" id="Seg_5573" s="T147">n-n&gt;adj</ta>
            <ta e="T149" id="Seg_5574" s="T148">n-n:(ins)-n&gt;adj</ta>
            <ta e="T150" id="Seg_5575" s="T149">num</ta>
            <ta e="T151" id="Seg_5576" s="T150">n-n&gt;n-n:num-n:case3</ta>
            <ta e="T152" id="Seg_5577" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_5578" s="T152">v-v:inf-v-v:tense-mood-v:pn</ta>
            <ta e="T154" id="Seg_5579" s="T153">n-n&gt;v-v&gt;adv</ta>
            <ta e="T155" id="Seg_5580" s="T154">pers</ta>
            <ta e="T156" id="Seg_5581" s="T155">adv</ta>
            <ta e="T157" id="Seg_5582" s="T156">n-n:case3</ta>
            <ta e="T158" id="Seg_5583" s="T157">n-n:(ins)-n&gt;v-v:mood-v:pn</ta>
            <ta e="T159" id="Seg_5584" s="T158">n-n:case3</ta>
            <ta e="T160" id="Seg_5585" s="T159">n-n:(ins)-n&gt;v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_5586" s="T160">n-n:case3</ta>
            <ta e="T162" id="Seg_5587" s="T161">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T163" id="Seg_5588" s="T162">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T164" id="Seg_5589" s="T163">adv</ta>
            <ta e="T165" id="Seg_5590" s="T164">n-n&gt;v-v&gt;adv</ta>
            <ta e="T166" id="Seg_5591" s="T165">v-v:tense-v:pn</ta>
            <ta e="T167" id="Seg_5592" s="T166">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T168" id="Seg_5593" s="T167">adv</ta>
            <ta e="T169" id="Seg_5594" s="T168">v-v:ins-v:pn</ta>
            <ta e="T170" id="Seg_5595" s="T169">n-n:case3</ta>
            <ta e="T171" id="Seg_5596" s="T170">n-n&gt;v-v:pn</ta>
            <ta e="T172" id="Seg_5597" s="T171">n-n:(ins)-n&gt;v-v:ins-v:pn</ta>
            <ta e="T173" id="Seg_5598" s="T172">n-n:case3</ta>
            <ta e="T174" id="Seg_5599" s="T173">v-v:pn</ta>
            <ta e="T175" id="Seg_5600" s="T174">n-n:case3</ta>
            <ta e="T176" id="Seg_5601" s="T175">dem-n&gt;adj</ta>
            <ta e="T177" id="Seg_5602" s="T176">n-n:case3</ta>
            <ta e="T178" id="Seg_5603" s="T177">n-n:case3</ta>
            <ta e="T179" id="Seg_5604" s="T178">v-v:pn</ta>
            <ta e="T180" id="Seg_5605" s="T179">adv-adv:advcase</ta>
            <ta e="T181" id="Seg_5606" s="T180">adv</ta>
            <ta e="T182" id="Seg_5607" s="T181">adv</ta>
            <ta e="T183" id="Seg_5608" s="T182">n-n:case3</ta>
            <ta e="T184" id="Seg_5609" s="T183">v-v:pn</ta>
            <ta e="T185" id="Seg_5610" s="T184">n-n:num-n:obl.poss-n:case2</ta>
            <ta e="T186" id="Seg_5611" s="T185">n-n:case3</ta>
            <ta e="T187" id="Seg_5612" s="T186">n-n:case3</ta>
            <ta e="T188" id="Seg_5613" s="T187">v-v&gt;v-v:mood-pn</ta>
            <ta e="T189" id="Seg_5614" s="T188">dem-n-n:case3</ta>
            <ta e="T190" id="Seg_5615" s="T189">v-v:tense-v:pn</ta>
            <ta e="T191" id="Seg_5616" s="T190">n-n:(ins)-n&gt;adj</ta>
            <ta e="T192" id="Seg_5617" s="T191">n-n:case3</ta>
            <ta e="T193" id="Seg_5618" s="T192">n-n:case3</ta>
            <ta e="T194" id="Seg_5619" s="T193">v-v&gt;v-v:mood-pn</ta>
            <ta e="T195" id="Seg_5620" s="T194">n-n:case3</ta>
            <ta e="T196" id="Seg_5621" s="T195">n-n:case3</ta>
            <ta e="T197" id="Seg_5622" s="T196">adv</ta>
            <ta e="T198" id="Seg_5623" s="T197">v-v&gt;v-v:pn</ta>
            <ta e="T199" id="Seg_5624" s="T198">adv</ta>
            <ta e="T200" id="Seg_5625" s="T199">ptcl</ta>
            <ta e="T201" id="Seg_5626" s="T200">v-v:pn</ta>
            <ta e="T202" id="Seg_5627" s="T201">n-n:case1-n:poss</ta>
            <ta e="T203" id="Seg_5628" s="T202">n-n&gt;adj</ta>
            <ta e="T204" id="Seg_5629" s="T203">n-n&gt;adj</ta>
            <ta e="T205" id="Seg_5630" s="T204">n-n:case3</ta>
            <ta e="T206" id="Seg_5631" s="T205">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T207" id="Seg_5632" s="T206">adv-adv:advcase</ta>
            <ta e="T208" id="Seg_5633" s="T207">n-n&gt;n-n:case3</ta>
            <ta e="T209" id="Seg_5634" s="T208">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T210" id="Seg_5635" s="T209">num</ta>
            <ta e="T211" id="Seg_5636" s="T210">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T212" id="Seg_5637" s="T211">n-n:(ins)-n&gt;adj</ta>
            <ta e="T213" id="Seg_5638" s="T212">n-n:case3</ta>
            <ta e="T214" id="Seg_5639" s="T213">v-v&gt;v-v&gt;v-v:tense-mood-v:pn</ta>
            <ta e="T215" id="Seg_5640" s="T214">n-n:num-n:case2-n:obl.poss</ta>
            <ta e="T216" id="Seg_5641" s="T215">dem-adj&gt;adv</ta>
            <ta e="T217" id="Seg_5642" s="T216">v-v:ins-v:pn</ta>
            <ta e="T218" id="Seg_5643" s="T217">n-n&gt;adj</ta>
            <ta e="T219" id="Seg_5644" s="T218">n-n&gt;n-n:num-n:case3</ta>
            <ta e="T220" id="Seg_5645" s="T219">adv</ta>
            <ta e="T221" id="Seg_5646" s="T220">v-v&gt;v-v:mood-pn</ta>
            <ta e="T222" id="Seg_5647" s="T221">n-n:case3</ta>
            <ta e="T223" id="Seg_5648" s="T222">n-n:(ins)-n:case3</ta>
            <ta e="T224" id="Seg_5649" s="T223">pp</ta>
            <ta e="T225" id="Seg_5650" s="T224">v-v&gt;v-v&gt;v-v:mood-pn</ta>
            <ta e="T226" id="Seg_5651" s="T225">adv</ta>
            <ta e="T227" id="Seg_5652" s="T226">dem-adj&gt;adv</ta>
            <ta e="T228" id="Seg_5653" s="T227">v-v:mood-pn</ta>
            <ta e="T229" id="Seg_5654" s="T228">n-n:case1-v:pn</ta>
            <ta e="T230" id="Seg_5655" s="T229">v-v:pn</ta>
            <ta e="T231" id="Seg_5656" s="T230">n-n:case1-v:pn</ta>
            <ta e="T232" id="Seg_5657" s="T231">pers</ta>
            <ta e="T233" id="Seg_5658" s="T232">v-v:mood-pn</ta>
            <ta e="T234" id="Seg_5659" s="T233">n-n:case3</ta>
            <ta e="T235" id="Seg_5660" s="T234">v-v&gt;v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T236" id="Seg_5661" s="T235">v-v:ins-v&gt;v-v:inf-poss</ta>
            <ta e="T237" id="Seg_5662" s="T236">adv</ta>
            <ta e="T238" id="Seg_5663" s="T237">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T239" id="Seg_5664" s="T238">adj</ta>
            <ta e="T240" id="Seg_5665" s="T239">n-n:case1-n:poss</ta>
            <ta e="T241" id="Seg_5666" s="T240">n-n:case3</ta>
            <ta e="T242" id="Seg_5667" s="T241">v-v:pn</ta>
            <ta e="T243" id="Seg_5668" s="T242">n-n:case1-n:poss</ta>
            <ta e="T244" id="Seg_5669" s="T243">adv</ta>
            <ta e="T245" id="Seg_5670" s="T244">adv</ta>
            <ta e="T246" id="Seg_5671" s="T245">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T247" id="Seg_5672" s="T246">adv-adv:advcase</ta>
            <ta e="T248" id="Seg_5673" s="T247">n-n:case3</ta>
            <ta e="T249" id="Seg_5674" s="T248">n-n&gt;adj</ta>
            <ta e="T250" id="Seg_5675" s="T249">n-n:case3</ta>
            <ta e="T251" id="Seg_5676" s="T250">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T252" id="Seg_5677" s="T251">adv</ta>
            <ta e="T253" id="Seg_5678" s="T252">v-v:ins-v:pn</ta>
            <ta e="T254" id="Seg_5679" s="T253">adv-adv:advcase</ta>
            <ta e="T255" id="Seg_5680" s="T254">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T256" id="Seg_5681" s="T255">num</ta>
            <ta e="T257" id="Seg_5682" s="T256">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T258" id="Seg_5683" s="T257">n-n:(ins)-n&gt;adj</ta>
            <ta e="T259" id="Seg_5684" s="T258">n-n:case3</ta>
            <ta e="T260" id="Seg_5685" s="T259">n-n:num-n:case1-n:poss</ta>
            <ta e="T261" id="Seg_5686" s="T260">n-n&gt;v-v&gt;v-v:inf-v-v:ins-v:pn</ta>
            <ta e="T262" id="Seg_5687" s="T261">adv</ta>
            <ta e="T263" id="Seg_5688" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_5689" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_5690" s="T264">v-v:tense-mood-v:pn</ta>
            <ta e="T266" id="Seg_5691" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_5692" s="T266">n-n&gt;adj</ta>
            <ta e="T268" id="Seg_5693" s="T267">n-n:case3</ta>
            <ta e="T269" id="Seg_5694" s="T268">adv</ta>
            <ta e="T270" id="Seg_5695" s="T269">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T271" id="Seg_5696" s="T270">n-n:case2</ta>
            <ta e="T272" id="Seg_5697" s="T271">adv</ta>
            <ta e="T274" id="Seg_5698" s="T273">v-v:tense-mood-v:pn</ta>
            <ta e="T275" id="Seg_5699" s="T274">adv</ta>
            <ta e="T276" id="Seg_5700" s="T275">v-v&gt;adv</ta>
            <ta e="T277" id="Seg_5701" s="T276">adv</ta>
            <ta e="T278" id="Seg_5702" s="T277">v-v:ins-v:pn</ta>
            <ta e="T279" id="Seg_5703" s="T278">adv</ta>
            <ta e="T280" id="Seg_5704" s="T279">v-v&gt;adv</ta>
            <ta e="T281" id="Seg_5705" s="T280">v-v:ins-v&gt;v-v:mood-pn</ta>
            <ta e="T282" id="Seg_5706" s="T281">n-n:(ins)-n:obl.poss-n:case2</ta>
            <ta e="T283" id="Seg_5707" s="T282">n-n&gt;v-v:mood-v:pn</ta>
            <ta e="T284" id="Seg_5708" s="T283">v-v&gt;adv</ta>
            <ta e="T285" id="Seg_5709" s="T284">n-n:case3</ta>
            <ta e="T286" id="Seg_5710" s="T285">n-n:case3</ta>
            <ta e="T287" id="Seg_5711" s="T286">adv</ta>
            <ta e="T288" id="Seg_5712" s="T287">conj</ta>
            <ta e="T289" id="Seg_5713" s="T288">interrog-n:case3</ta>
            <ta e="T290" id="Seg_5714" s="T289">v-v:inf-v-v:ins-v:pn</ta>
            <ta e="T291" id="Seg_5715" s="T290">adv</ta>
            <ta e="T292" id="Seg_5716" s="T291">v-v&gt;adv</ta>
            <ta e="T293" id="Seg_5717" s="T292">v-v:ins-v&gt;v-v:mood-pn</ta>
            <ta e="T294" id="Seg_5718" s="T293">conj</ta>
            <ta e="T295" id="Seg_5719" s="T294">dem-adj&gt;adv</ta>
            <ta e="T296" id="Seg_5720" s="T295">v-v:tense-v:pn</ta>
            <ta e="T297" id="Seg_5721" s="T296">adj</ta>
            <ta e="T298" id="Seg_5722" s="T297">n-n:case1-n:poss</ta>
            <ta e="T299" id="Seg_5723" s="T298">adv</ta>
            <ta e="T300" id="Seg_5724" s="T299">v-v&gt;adv</ta>
            <ta e="T301" id="Seg_5725" s="T300">v-v:ins-v&gt;v-v:mood-v:pn</ta>
            <ta e="T302" id="Seg_5726" s="T301">adv</ta>
            <ta e="T303" id="Seg_5727" s="T302">preverb</ta>
            <ta e="T304" id="Seg_5728" s="T303">v-v&gt;v-v:pn</ta>
            <ta e="T305" id="Seg_5729" s="T304">n-n&gt;adj</ta>
            <ta e="T306" id="Seg_5730" s="T305">n-n:case1-n:poss</ta>
            <ta e="T307" id="Seg_5731" s="T306">adv-adv:advcase</ta>
            <ta e="T308" id="Seg_5732" s="T307">n-n:case2-n:obl.poss</ta>
            <ta e="T309" id="Seg_5733" s="T308">v-v&gt;adv</ta>
            <ta e="T310" id="Seg_5734" s="T309">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T311" id="Seg_5735" s="T310">n-n:case3</ta>
            <ta e="T312" id="Seg_5736" s="T311">ptcl</ta>
            <ta e="T313" id="Seg_5737" s="T312">preverb</ta>
            <ta e="T314" id="Seg_5738" s="T313">v-v:tense-mood-v:pn</ta>
            <ta e="T315" id="Seg_5739" s="T314">adj</ta>
            <ta e="T316" id="Seg_5740" s="T315">n-n&gt;adj</ta>
            <ta e="T317" id="Seg_5741" s="T316">n-n:(ins)-n&gt;adj</ta>
            <ta e="T318" id="Seg_5742" s="T317">n-n:case3</ta>
            <ta e="T319" id="Seg_5743" s="T318">nprop-n:case3</ta>
            <ta e="T320" id="Seg_5744" s="T319">pp</ta>
            <ta e="T321" id="Seg_5745" s="T320">num</ta>
            <ta e="T322" id="Seg_5746" s="T321">n-n:case3</ta>
            <ta e="T323" id="Seg_5747" s="T322">ptcl</ta>
            <ta e="T324" id="Seg_5748" s="T323">v-v:tense-mood-v:pn</ta>
            <ta e="T325" id="Seg_5749" s="T324">adv-adv&gt;adj</ta>
            <ta e="T326" id="Seg_5750" s="T325">n-n&gt;adj</ta>
            <ta e="T327" id="Seg_5751" s="T326">v-v&gt;ptcp</ta>
            <ta e="T328" id="Seg_5752" s="T327">n-n:obl.poss-n:case2</ta>
            <ta e="T329" id="Seg_5753" s="T328">ptcl</ta>
            <ta e="T330" id="Seg_5754" s="T329">num</ta>
            <ta e="T331" id="Seg_5755" s="T330">n-n:case3</ta>
            <ta e="T332" id="Seg_5756" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_5757" s="T332">v-v:tense-mood-v:pn</ta>
            <ta e="T334" id="Seg_5758" s="T333">num</ta>
            <ta e="T335" id="Seg_5759" s="T334">n-n&gt;n-n:num-n:case3</ta>
            <ta e="T336" id="Seg_5760" s="T335">quant</ta>
            <ta e="T337" id="Seg_5761" s="T336">num</ta>
            <ta e="T338" id="Seg_5762" s="T337">n-n:case3</ta>
            <ta e="T339" id="Seg_5763" s="T338">v-v:mood-v:pn</ta>
            <ta e="T340" id="Seg_5764" s="T339">adv</ta>
            <ta e="T341" id="Seg_5765" s="T340">adv</ta>
            <ta e="T342" id="Seg_5766" s="T341">v-v:tense-v:pn</ta>
            <ta e="T343" id="Seg_5767" s="T342">n-n:case1-v:pn</ta>
            <ta e="T344" id="Seg_5768" s="T343">ptcl</ta>
            <ta e="T345" id="Seg_5769" s="T344">v-v:tense-v:pn</ta>
            <ta e="T346" id="Seg_5770" s="T345">n-n:case1-v:pn</ta>
            <ta e="T347" id="Seg_5771" s="T346">v-v:ins-v:pn</ta>
            <ta e="T348" id="Seg_5772" s="T347">n-n:case3</ta>
            <ta e="T349" id="Seg_5773" s="T348">pers</ta>
            <ta e="T350" id="Seg_5774" s="T349">v-v:mood-pn</ta>
            <ta e="T351" id="Seg_5775" s="T350">v-v:ins-v&gt;v-v:inf</ta>
            <ta e="T352" id="Seg_5776" s="T351">n-n:case3</ta>
            <ta e="T353" id="Seg_5777" s="T352">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T354" id="Seg_5778" s="T353">n-n:case1-n:poss</ta>
            <ta e="T355" id="Seg_5779" s="T354">quant</ta>
            <ta e="T356" id="Seg_5780" s="T355">v-v:ins-v:pn</ta>
            <ta e="T357" id="Seg_5781" s="T356">n-n&gt;adj</ta>
            <ta e="T358" id="Seg_5782" s="T357">n-n:(ins)-n&gt;adj</ta>
            <ta e="T359" id="Seg_5783" s="T358">num</ta>
            <ta e="T360" id="Seg_5784" s="T359">n-n&gt;n-n:num-n:case3</ta>
            <ta e="T361" id="Seg_5785" s="T360">conj</ta>
            <ta e="T362" id="Seg_5786" s="T361">nprop-n&gt;adj</ta>
            <ta e="T363" id="Seg_5787" s="T362">num</ta>
            <ta e="T364" id="Seg_5788" s="T363">n-n&gt;n-n:num-n:case3</ta>
            <ta e="T365" id="Seg_5789" s="T364">adv</ta>
            <ta e="T366" id="Seg_5790" s="T365">preverb</ta>
            <ta e="T367" id="Seg_5791" s="T366">n-n&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T368" id="Seg_5792" s="T367">adj</ta>
            <ta e="T369" id="Seg_5793" s="T368">n-n:case1-n:poss</ta>
            <ta e="T370" id="Seg_5794" s="T369">conj</ta>
            <ta e="T371" id="Seg_5795" s="T370">n-n:case3</ta>
            <ta e="T372" id="Seg_5796" s="T371">v-v:tense-mood-v:pn</ta>
            <ta e="T373" id="Seg_5797" s="T372">pers-n:(ins)-n:num</ta>
            <ta e="T374" id="Seg_5798" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_5799" s="T374">dem-adj&gt;adv</ta>
            <ta e="T376" id="Seg_5800" s="T375">v-v:tense-mood-v:pn</ta>
            <ta e="T377" id="Seg_5801" s="T376">adj</ta>
            <ta e="T378" id="Seg_5802" s="T377">n-n:case3</ta>
            <ta e="T379" id="Seg_5803" s="T378">num</ta>
            <ta e="T380" id="Seg_5804" s="T379">n-n:case3</ta>
            <ta e="T381" id="Seg_5805" s="T380">adv</ta>
            <ta e="T382" id="Seg_5806" s="T381">v-v:pn</ta>
            <ta e="T383" id="Seg_5807" s="T382">n-n:case3</ta>
            <ta e="T384" id="Seg_5808" s="T383">n-n:case3</ta>
            <ta e="T385" id="Seg_5809" s="T384">v-v&gt;v-v:pn</ta>
            <ta e="T386" id="Seg_5810" s="T385">n-n:case3</ta>
            <ta e="T387" id="Seg_5811" s="T386">adv</ta>
            <ta e="T388" id="Seg_5812" s="T387">preverb</ta>
            <ta e="T389" id="Seg_5813" s="T388">v-v:pn</ta>
            <ta e="T390" id="Seg_5814" s="T389">n-n&gt;adj</ta>
            <ta e="T391" id="Seg_5815" s="T390">n-n:(ins)-n&gt;adj</ta>
            <ta e="T392" id="Seg_5816" s="T391">num</ta>
            <ta e="T393" id="Seg_5817" s="T392">n-n&gt;n-n:num-n:(ins)-n:case3</ta>
            <ta e="T394" id="Seg_5818" s="T393">n-n&gt;adj</ta>
            <ta e="T395" id="Seg_5819" s="T394">n-n&gt;adj</ta>
            <ta e="T396" id="Seg_5820" s="T395">v-v&gt;ptcp</ta>
            <ta e="T397" id="Seg_5821" s="T396">n-n:case1-n:poss</ta>
            <ta e="T398" id="Seg_5822" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_5823" s="T398">v-v:pn</ta>
            <ta e="T400" id="Seg_5824" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_5825" s="T400">v-v&gt;ptcp</ta>
            <ta e="T402" id="Seg_5826" s="T401">n-n:case3</ta>
            <ta e="T403" id="Seg_5827" s="T402">v-v:pn</ta>
            <ta e="T404" id="Seg_5828" s="T403">nprop-n:case3</ta>
            <ta e="T405" id="Seg_5829" s="T404">n-n:case1-n:poss</ta>
            <ta e="T406" id="Seg_5830" s="T405">n-n&gt;adj</ta>
            <ta e="T407" id="Seg_5831" s="T406">n-n&gt;adj</ta>
            <ta e="T408" id="Seg_5832" s="T407">v-v&gt;ptcp</ta>
            <ta e="T409" id="Seg_5833" s="T408">n-n:case1-n:poss</ta>
            <ta e="T410" id="Seg_5834" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_5835" s="T410">adv</ta>
            <ta e="T412" id="Seg_5836" s="T411">v-v:pn</ta>
            <ta e="T413" id="Seg_5837" s="T412">v-v&gt;adv</ta>
            <ta e="T414" id="Seg_5838" s="T413">v-v&gt;v-v:pn</ta>
            <ta e="T415" id="Seg_5839" s="T414">n-n:case1-n:poss</ta>
            <ta e="T416" id="Seg_5840" s="T415">v-v&gt;adv</ta>
            <ta e="T417" id="Seg_5841" s="T416">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T418" id="Seg_5842" s="T417">dem</ta>
            <ta e="T419" id="Seg_5843" s="T418">num</ta>
            <ta e="T420" id="Seg_5844" s="T419">n-n&gt;n-n:num-n:case3</ta>
            <ta e="T421" id="Seg_5845" s="T420">n-n:case3</ta>
            <ta e="T422" id="Seg_5846" s="T421">v-v:pn</ta>
            <ta e="T423" id="Seg_5847" s="T422">conj</ta>
            <ta e="T424" id="Seg_5848" s="T423">v-v:ins-v:pn</ta>
            <ta e="T425" id="Seg_5849" s="T424">conj</ta>
            <ta e="T426" id="Seg_5850" s="T425">v-v:ins-v:pn</ta>
            <ta e="T427" id="Seg_5851" s="T426">v-v&gt;adv</ta>
            <ta e="T428" id="Seg_5852" s="T427">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T429" id="Seg_5853" s="T428">adv-adv:advcase</ta>
            <ta e="T430" id="Seg_5854" s="T429">v-v&gt;v-v&gt;adv</ta>
            <ta e="T431" id="Seg_5855" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_5856" s="T431">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T433" id="Seg_5857" s="T432">n-n:case1-n:poss</ta>
            <ta e="T434" id="Seg_5858" s="T433">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T435" id="Seg_5859" s="T434">adv</ta>
            <ta e="T436" id="Seg_5860" s="T435">v-v:pn</ta>
            <ta e="T437" id="Seg_5861" s="T436">n-n&gt;adj</ta>
            <ta e="T438" id="Seg_5862" s="T437">n-n:case3</ta>
            <ta e="T439" id="Seg_5863" s="T438">num-num&gt;adj</ta>
            <ta e="T440" id="Seg_5864" s="T439">conj</ta>
            <ta e="T441" id="Seg_5865" s="T440">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T442" id="Seg_5866" s="T441">n-n:case3</ta>
            <ta e="T443" id="Seg_5867" s="T442">n-n:case3</ta>
            <ta e="T444" id="Seg_5868" s="T443">adj-adj&gt;adv</ta>
            <ta e="T445" id="Seg_5869" s="T444">v-v:ins-v:pn</ta>
            <ta e="T446" id="Seg_5870" s="T445">n-n:case1-n:poss</ta>
            <ta e="T448" id="Seg_5871" s="T447">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T449" id="Seg_5872" s="T448">nprop-n:case3</ta>
            <ta e="T450" id="Seg_5873" s="T449">dem</ta>
            <ta e="T451" id="Seg_5874" s="T450">n-n:num-n:case1-n:poss</ta>
            <ta e="T452" id="Seg_5875" s="T451">v-v:tense-v:pn</ta>
            <ta e="T453" id="Seg_5876" s="T452">n-n:case1-n:obl.poss</ta>
            <ta e="T454" id="Seg_5877" s="T453">n-n:case3</ta>
            <ta e="T455" id="Seg_5878" s="T454">pp</ta>
            <ta e="T456" id="Seg_5879" s="T455">adv</ta>
            <ta e="T457" id="Seg_5880" s="T456">v-v:pn</ta>
            <ta e="T458" id="Seg_5881" s="T457">v-v:ins-v:pn</ta>
            <ta e="T459" id="Seg_5882" s="T458">ptcl</ta>
            <ta e="T460" id="Seg_5883" s="T459">v-v:tense-v:pn</ta>
            <ta e="T461" id="Seg_5884" s="T460">pers</ta>
            <ta e="T462" id="Seg_5885" s="T461">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T463" id="Seg_5886" s="T462">dem</ta>
            <ta e="T464" id="Seg_5887" s="T463">n-n&gt;adj</ta>
            <ta e="T465" id="Seg_5888" s="T464">n-n:case3</ta>
            <ta e="T466" id="Seg_5889" s="T465">adv</ta>
            <ta e="T467" id="Seg_5890" s="T466">interrog-n:case3</ta>
            <ta e="T468" id="Seg_5891" s="T467">v-v&gt;v-v:pn</ta>
            <ta e="T469" id="Seg_5892" s="T468">pers</ta>
            <ta e="T470" id="Seg_5893" s="T469">v-v:tense-v:pn</ta>
            <ta e="T471" id="Seg_5894" s="T470">adv</ta>
            <ta e="T472" id="Seg_5895" s="T471">num</ta>
            <ta e="T473" id="Seg_5896" s="T472">n-n&gt;n-n:num-n:(ins)-n:case3</ta>
            <ta e="T474" id="Seg_5897" s="T473">quant</ta>
            <ta e="T475" id="Seg_5898" s="T474">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T476" id="Seg_5899" s="T475">num</ta>
            <ta e="T477" id="Seg_5900" s="T476">pp-n:case3</ta>
            <ta e="T478" id="Seg_5901" s="T477">pp-n:case3</ta>
            <ta e="T479" id="Seg_5902" s="T478">n-n:case3</ta>
            <ta e="T480" id="Seg_5903" s="T479">adv</ta>
            <ta e="T481" id="Seg_5904" s="T480">v-v:tense-v:pn</ta>
            <ta e="T482" id="Seg_5905" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_5906" s="T482">interrog-n:(ins)-n:case3</ta>
            <ta e="T484" id="Seg_5907" s="T483">n-n:case3</ta>
            <ta e="T485" id="Seg_5908" s="T484">v-v:pn</ta>
            <ta e="T486" id="Seg_5909" s="T485">v-v:ins-v:pn</ta>
            <ta e="T487" id="Seg_5910" s="T486">n-n&gt;adj</ta>
            <ta e="T488" id="Seg_5911" s="T487">n-n:(ins)-n&gt;adj</ta>
            <ta e="T489" id="Seg_5912" s="T488">n-n&gt;adj</ta>
            <ta e="T490" id="Seg_5913" s="T489">dem-adj&gt;adv</ta>
            <ta e="T491" id="Seg_5914" s="T490">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T492" id="Seg_5915" s="T491">n-n:num-n:case1-n:poss</ta>
            <ta e="T493" id="Seg_5916" s="T492">v-v:tense-v:pn</ta>
            <ta e="T494" id="Seg_5917" s="T493">adv</ta>
            <ta e="T495" id="Seg_5918" s="T494">ptcl</ta>
            <ta e="T496" id="Seg_5919" s="T495">n-n:(ins)-n&gt;v-v:ins-v:pn</ta>
            <ta e="T497" id="Seg_5920" s="T496">adv</ta>
            <ta e="T498" id="Seg_5921" s="T497">ptcl</ta>
            <ta e="T499" id="Seg_5922" s="T498">n</ta>
            <ta e="T500" id="Seg_5923" s="T499">n-n:case1-n:poss</ta>
            <ta e="T501" id="Seg_5924" s="T500">dem-n:case3</ta>
            <ta e="T502" id="Seg_5925" s="T501">pp</ta>
            <ta e="T503" id="Seg_5926" s="T502">adj-adj&gt;adv</ta>
            <ta e="T504" id="Seg_5927" s="T503">v-v:tense-v:pn</ta>
            <ta e="T505" id="Seg_5928" s="T504">pers</ta>
            <ta e="T507" id="Seg_5929" s="T506">n-n:case1-n:poss</ta>
            <ta e="T508" id="Seg_5930" s="T507">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T509" id="Seg_5931" s="T508">n-n:case1-n:obl.poss</ta>
            <ta e="T510" id="Seg_5932" s="T509">n-n:case3</ta>
            <ta e="T511" id="Seg_5933" s="T510">ptcl</ta>
            <ta e="T512" id="Seg_5934" s="T511">v-v&gt;v-v:pn</ta>
            <ta e="T513" id="Seg_5935" s="T512">n-n:(ins)-n&gt;adj</ta>
            <ta e="T514" id="Seg_5936" s="T513">n-n:case3</ta>
            <ta e="T515" id="Seg_5937" s="T514">adv</ta>
            <ta e="T516" id="Seg_5938" s="T515">v</ta>
            <ta e="T517" id="Seg_5939" s="T516">v-v:ins-v:pn</ta>
            <ta e="T518" id="Seg_5940" s="T517">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_5941" s="T0">nprop</ta>
            <ta e="T2" id="Seg_5942" s="T1">nprop</ta>
            <ta e="T3" id="Seg_5943" s="T2">num</ta>
            <ta e="T4" id="Seg_5944" s="T3">n</ta>
            <ta e="T5" id="Seg_5945" s="T4">v</ta>
            <ta e="T6" id="Seg_5946" s="T5">adj</ta>
            <ta e="T7" id="Seg_5947" s="T6">adj</ta>
            <ta e="T8" id="Seg_5948" s="T7">n</ta>
            <ta e="T9" id="Seg_5949" s="T8">v</ta>
            <ta e="T10" id="Seg_5950" s="T9">adj</ta>
            <ta e="T11" id="Seg_5951" s="T10">n</ta>
            <ta e="T12" id="Seg_5952" s="T11">adj</ta>
            <ta e="T13" id="Seg_5953" s="T12">adj</ta>
            <ta e="T14" id="Seg_5954" s="T13">n</ta>
            <ta e="T15" id="Seg_5955" s="T14">adj</ta>
            <ta e="T16" id="Seg_5956" s="T15">n</ta>
            <ta e="T17" id="Seg_5957" s="T16">adj</ta>
            <ta e="T18" id="Seg_5958" s="T17">adj</ta>
            <ta e="T19" id="Seg_5959" s="T18">ptcp</ta>
            <ta e="T20" id="Seg_5960" s="T19">v</ta>
            <ta e="T21" id="Seg_5961" s="T20">adv</ta>
            <ta e="T22" id="Seg_5962" s="T21">adj</ta>
            <ta e="T23" id="Seg_5963" s="T22">n</ta>
            <ta e="T24" id="Seg_5964" s="T23">n</ta>
            <ta e="T25" id="Seg_5965" s="T24">adj</ta>
            <ta e="T26" id="Seg_5966" s="T25">ptcp</ta>
            <ta e="T27" id="Seg_5967" s="T26">v</ta>
            <ta e="T28" id="Seg_5968" s="T27">adv</ta>
            <ta e="T29" id="Seg_5969" s="T28">v</ta>
            <ta e="T30" id="Seg_5970" s="T29">conj</ta>
            <ta e="T31" id="Seg_5971" s="T30">dem</ta>
            <ta e="T32" id="Seg_5972" s="T31">adj</ta>
            <ta e="T33" id="Seg_5973" s="T32">adj</ta>
            <ta e="T34" id="Seg_5974" s="T33">n</ta>
            <ta e="T35" id="Seg_5975" s="T34">adv</ta>
            <ta e="T36" id="Seg_5976" s="T35">n</ta>
            <ta e="T37" id="Seg_5977" s="T36">v</ta>
            <ta e="T38" id="Seg_5978" s="T37">interrog</ta>
            <ta e="T39" id="Seg_5979" s="T38">interrog</ta>
            <ta e="T40" id="Seg_5980" s="T39">v</ta>
            <ta e="T41" id="Seg_5981" s="T40">quant</ta>
            <ta e="T42" id="Seg_5982" s="T41">v</ta>
            <ta e="T43" id="Seg_5983" s="T42">v</ta>
            <ta e="T44" id="Seg_5984" s="T43">v</ta>
            <ta e="T45" id="Seg_5985" s="T44">n</ta>
            <ta e="T46" id="Seg_5986" s="T45">v</ta>
            <ta e="T47" id="Seg_5987" s="T46">n</ta>
            <ta e="T48" id="Seg_5988" s="T47">v</ta>
            <ta e="T49" id="Seg_5989" s="T48">emphpers</ta>
            <ta e="T50" id="Seg_5990" s="T49">n</ta>
            <ta e="T51" id="Seg_5991" s="T50">v</ta>
            <ta e="T52" id="Seg_5992" s="T51">n</ta>
            <ta e="T53" id="Seg_5993" s="T52">adv</ta>
            <ta e="T54" id="Seg_5994" s="T53">v</ta>
            <ta e="T55" id="Seg_5995" s="T54">adv</ta>
            <ta e="T56" id="Seg_5996" s="T55">n</ta>
            <ta e="T57" id="Seg_5997" s="T56">conj</ta>
            <ta e="T58" id="Seg_5998" s="T57">n</ta>
            <ta e="T59" id="Seg_5999" s="T58">v</ta>
            <ta e="T60" id="Seg_6000" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_6001" s="T60">v</ta>
            <ta e="T62" id="Seg_6002" s="T61">interrog</ta>
            <ta e="T63" id="Seg_6003" s="T62">pp</ta>
            <ta e="T65" id="Seg_6004" s="T64">pers</ta>
            <ta e="T66" id="Seg_6005" s="T65">v</ta>
            <ta e="T67" id="Seg_6006" s="T66">n</ta>
            <ta e="T68" id="Seg_6007" s="T67">n</ta>
            <ta e="T69" id="Seg_6008" s="T68">v</ta>
            <ta e="T70" id="Seg_6009" s="T69">dem</ta>
            <ta e="T71" id="Seg_6010" s="T70">n</ta>
            <ta e="T72" id="Seg_6011" s="T71">pp</ta>
            <ta e="T73" id="Seg_6012" s="T72">adj</ta>
            <ta e="T74" id="Seg_6013" s="T73">n</ta>
            <ta e="T75" id="Seg_6014" s="T74">v</ta>
            <ta e="T77" id="Seg_6015" s="T76">adj</ta>
            <ta e="T78" id="Seg_6016" s="T77">n</ta>
            <ta e="T79" id="Seg_6017" s="T78">v</ta>
            <ta e="T81" id="Seg_6018" s="T80">adj</ta>
            <ta e="T82" id="Seg_6019" s="T81">n</ta>
            <ta e="T83" id="Seg_6020" s="T82">v</ta>
            <ta e="T84" id="Seg_6021" s="T83">pro</ta>
            <ta e="T85" id="Seg_6022" s="T84">conj</ta>
            <ta e="T86" id="Seg_6023" s="T85">v</ta>
            <ta e="T87" id="Seg_6024" s="T86">conj</ta>
            <ta e="T88" id="Seg_6025" s="T87">adv</ta>
            <ta e="T89" id="Seg_6026" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_6027" s="T89">v</ta>
            <ta e="T91" id="Seg_6028" s="T90">pers</ta>
            <ta e="T92" id="Seg_6029" s="T91">adv</ta>
            <ta e="T93" id="Seg_6030" s="T92">v</ta>
            <ta e="T95" id="Seg_6031" s="T94">interrog</ta>
            <ta e="T96" id="Seg_6032" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_6033" s="T96">v</ta>
            <ta e="T98" id="Seg_6034" s="T97">pers</ta>
            <ta e="T99" id="Seg_6035" s="T98">quant</ta>
            <ta e="T100" id="Seg_6036" s="T99">v</ta>
            <ta e="T101" id="Seg_6037" s="T100">n</ta>
            <ta e="T102" id="Seg_6038" s="T101">v</ta>
            <ta e="T103" id="Seg_6039" s="T102">v</ta>
            <ta e="T104" id="Seg_6040" s="T103">adj</ta>
            <ta e="T105" id="Seg_6041" s="T104">n</ta>
            <ta e="T106" id="Seg_6042" s="T105">n</ta>
            <ta e="T107" id="Seg_6043" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_6044" s="T107">num</ta>
            <ta e="T109" id="Seg_6045" s="T108">n</ta>
            <ta e="T110" id="Seg_6046" s="T109">v</ta>
            <ta e="T111" id="Seg_6047" s="T110">adv</ta>
            <ta e="T112" id="Seg_6048" s="T111">adv</ta>
            <ta e="T113" id="Seg_6049" s="T112">v</ta>
            <ta e="T114" id="Seg_6050" s="T113">n</ta>
            <ta e="T115" id="Seg_6051" s="T114">adj</ta>
            <ta e="T116" id="Seg_6052" s="T115">num</ta>
            <ta e="T117" id="Seg_6053" s="T116">n</ta>
            <ta e="T118" id="Seg_6054" s="T117">pers</ta>
            <ta e="T119" id="Seg_6055" s="T118">v</ta>
            <ta e="T120" id="Seg_6056" s="T119">adv</ta>
            <ta e="T121" id="Seg_6057" s="T120">adj</ta>
            <ta e="T122" id="Seg_6058" s="T121">n</ta>
            <ta e="T123" id="Seg_6059" s="T122">%%</ta>
            <ta e="T124" id="Seg_6060" s="T123">adj</ta>
            <ta e="T125" id="Seg_6061" s="T124">n</ta>
            <ta e="T126" id="Seg_6062" s="T125">n</ta>
            <ta e="T127" id="Seg_6063" s="T126">adj</ta>
            <ta e="T128" id="Seg_6064" s="T127">adj</ta>
            <ta e="T129" id="Seg_6065" s="T128">num</ta>
            <ta e="T130" id="Seg_6066" s="T129">n</ta>
            <ta e="T131" id="Seg_6067" s="T130">num</ta>
            <ta e="T132" id="Seg_6068" s="T131">adj</ta>
            <ta e="T133" id="Seg_6069" s="T132">n</ta>
            <ta e="T134" id="Seg_6070" s="T133">adv</ta>
            <ta e="T135" id="Seg_6071" s="T134">v</ta>
            <ta e="T136" id="Seg_6072" s="T135">adj</ta>
            <ta e="T137" id="Seg_6073" s="T136">num</ta>
            <ta e="T138" id="Seg_6074" s="T137">n</ta>
            <ta e="T139" id="Seg_6075" s="T138">adv</ta>
            <ta e="T140" id="Seg_6076" s="T139">v</ta>
            <ta e="T141" id="Seg_6077" s="T140">conj</ta>
            <ta e="T142" id="Seg_6078" s="T141">adj</ta>
            <ta e="T143" id="Seg_6079" s="T142">n</ta>
            <ta e="T144" id="Seg_6080" s="T143">v</ta>
            <ta e="T145" id="Seg_6081" s="T144">n</ta>
            <ta e="T146" id="Seg_6082" s="T145">adv</ta>
            <ta e="T147" id="Seg_6083" s="T146">v</ta>
            <ta e="T148" id="Seg_6084" s="T147">adj</ta>
            <ta e="T149" id="Seg_6085" s="T148">adj</ta>
            <ta e="T150" id="Seg_6086" s="T149">num</ta>
            <ta e="T151" id="Seg_6087" s="T150">n</ta>
            <ta e="T152" id="Seg_6088" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_6089" s="T152">v</ta>
            <ta e="T154" id="Seg_6090" s="T153">adv</ta>
            <ta e="T155" id="Seg_6091" s="T154">pers</ta>
            <ta e="T156" id="Seg_6092" s="T155">adv</ta>
            <ta e="T157" id="Seg_6093" s="T156">n</ta>
            <ta e="T158" id="Seg_6094" s="T157">adv</ta>
            <ta e="T159" id="Seg_6095" s="T158">n</ta>
            <ta e="T160" id="Seg_6096" s="T159">v</ta>
            <ta e="T161" id="Seg_6097" s="T160">n</ta>
            <ta e="T162" id="Seg_6098" s="T161">v</ta>
            <ta e="T163" id="Seg_6099" s="T162">n</ta>
            <ta e="T164" id="Seg_6100" s="T163">adv</ta>
            <ta e="T165" id="Seg_6101" s="T164">adv</ta>
            <ta e="T166" id="Seg_6102" s="T165">v</ta>
            <ta e="T167" id="Seg_6103" s="T166">v</ta>
            <ta e="T168" id="Seg_6104" s="T167">adv</ta>
            <ta e="T169" id="Seg_6105" s="T168">v</ta>
            <ta e="T170" id="Seg_6106" s="T169">n</ta>
            <ta e="T171" id="Seg_6107" s="T170">v</ta>
            <ta e="T172" id="Seg_6108" s="T171">v</ta>
            <ta e="T173" id="Seg_6109" s="T172">n</ta>
            <ta e="T174" id="Seg_6110" s="T173">v</ta>
            <ta e="T175" id="Seg_6111" s="T174">n</ta>
            <ta e="T176" id="Seg_6112" s="T175">adj</ta>
            <ta e="T177" id="Seg_6113" s="T176">n</ta>
            <ta e="T178" id="Seg_6114" s="T177">n</ta>
            <ta e="T179" id="Seg_6115" s="T178">v</ta>
            <ta e="T180" id="Seg_6116" s="T179">adv</ta>
            <ta e="T181" id="Seg_6117" s="T180">preverb</ta>
            <ta e="T182" id="Seg_6118" s="T181">adv</ta>
            <ta e="T183" id="Seg_6119" s="T182">n</ta>
            <ta e="T184" id="Seg_6120" s="T183">v</ta>
            <ta e="T185" id="Seg_6121" s="T184">n</ta>
            <ta e="T186" id="Seg_6122" s="T185">n</ta>
            <ta e="T187" id="Seg_6123" s="T186">n</ta>
            <ta e="T188" id="Seg_6124" s="T187">v</ta>
            <ta e="T189" id="Seg_6125" s="T188">n</ta>
            <ta e="T190" id="Seg_6126" s="T189">v</ta>
            <ta e="T191" id="Seg_6127" s="T190">adj</ta>
            <ta e="T192" id="Seg_6128" s="T191">n</ta>
            <ta e="T193" id="Seg_6129" s="T192">n</ta>
            <ta e="T194" id="Seg_6130" s="T193">v</ta>
            <ta e="T195" id="Seg_6131" s="T194">n</ta>
            <ta e="T196" id="Seg_6132" s="T195">n</ta>
            <ta e="T197" id="Seg_6133" s="T196">adv</ta>
            <ta e="T198" id="Seg_6134" s="T197">v</ta>
            <ta e="T199" id="Seg_6135" s="T198">adv</ta>
            <ta e="T200" id="Seg_6136" s="T199">ptcl</ta>
            <ta e="T201" id="Seg_6137" s="T200">v</ta>
            <ta e="T202" id="Seg_6138" s="T201">n</ta>
            <ta e="T203" id="Seg_6139" s="T202">n</ta>
            <ta e="T204" id="Seg_6140" s="T203">adj</ta>
            <ta e="T205" id="Seg_6141" s="T204">n</ta>
            <ta e="T206" id="Seg_6142" s="T205">v</ta>
            <ta e="T207" id="Seg_6143" s="T206">adv</ta>
            <ta e="T208" id="Seg_6144" s="T207">n</ta>
            <ta e="T209" id="Seg_6145" s="T208">v</ta>
            <ta e="T210" id="Seg_6146" s="T209">num</ta>
            <ta e="T211" id="Seg_6147" s="T210">n</ta>
            <ta e="T212" id="Seg_6148" s="T211">adj</ta>
            <ta e="T213" id="Seg_6149" s="T212">n</ta>
            <ta e="T214" id="Seg_6150" s="T213">v</ta>
            <ta e="T215" id="Seg_6151" s="T214">n</ta>
            <ta e="T216" id="Seg_6152" s="T215">adv</ta>
            <ta e="T217" id="Seg_6153" s="T216">v</ta>
            <ta e="T218" id="Seg_6154" s="T217">adj</ta>
            <ta e="T219" id="Seg_6155" s="T218">n</ta>
            <ta e="T220" id="Seg_6156" s="T219">adv</ta>
            <ta e="T221" id="Seg_6157" s="T220">v</ta>
            <ta e="T222" id="Seg_6158" s="T221">n</ta>
            <ta e="T223" id="Seg_6159" s="T222">n</ta>
            <ta e="T224" id="Seg_6160" s="T223">pp</ta>
            <ta e="T225" id="Seg_6161" s="T224">v</ta>
            <ta e="T226" id="Seg_6162" s="T225">adv</ta>
            <ta e="T227" id="Seg_6163" s="T226">adv</ta>
            <ta e="T228" id="Seg_6164" s="T227">v</ta>
            <ta e="T229" id="Seg_6165" s="T228">n</ta>
            <ta e="T230" id="Seg_6166" s="T229">v</ta>
            <ta e="T231" id="Seg_6167" s="T230">n</ta>
            <ta e="T232" id="Seg_6168" s="T231">pers</ta>
            <ta e="T233" id="Seg_6169" s="T232">v</ta>
            <ta e="T234" id="Seg_6170" s="T233">n</ta>
            <ta e="T235" id="Seg_6171" s="T234">v</ta>
            <ta e="T236" id="Seg_6172" s="T235">v</ta>
            <ta e="T237" id="Seg_6173" s="T236">adv</ta>
            <ta e="T238" id="Seg_6174" s="T237">v</ta>
            <ta e="T239" id="Seg_6175" s="T238">adj</ta>
            <ta e="T240" id="Seg_6176" s="T239">n</ta>
            <ta e="T241" id="Seg_6177" s="T240">n</ta>
            <ta e="T242" id="Seg_6178" s="T241">v</ta>
            <ta e="T243" id="Seg_6179" s="T242">n</ta>
            <ta e="T244" id="Seg_6180" s="T243">adv</ta>
            <ta e="T245" id="Seg_6181" s="T244">adv</ta>
            <ta e="T246" id="Seg_6182" s="T245">v</ta>
            <ta e="T247" id="Seg_6183" s="T246">adv</ta>
            <ta e="T248" id="Seg_6184" s="T247">n</ta>
            <ta e="T249" id="Seg_6185" s="T248">adj</ta>
            <ta e="T250" id="Seg_6186" s="T249">n</ta>
            <ta e="T251" id="Seg_6187" s="T250">v</ta>
            <ta e="T252" id="Seg_6188" s="T251">adv</ta>
            <ta e="T253" id="Seg_6189" s="T252">v</ta>
            <ta e="T254" id="Seg_6190" s="T253">adv</ta>
            <ta e="T255" id="Seg_6191" s="T254">v</ta>
            <ta e="T256" id="Seg_6192" s="T255">num</ta>
            <ta e="T257" id="Seg_6193" s="T256">n</ta>
            <ta e="T258" id="Seg_6194" s="T257">adj</ta>
            <ta e="T259" id="Seg_6195" s="T258">n</ta>
            <ta e="T260" id="Seg_6196" s="T259">v</ta>
            <ta e="T261" id="Seg_6197" s="T260">v</ta>
            <ta e="T262" id="Seg_6198" s="T261">adv</ta>
            <ta e="T263" id="Seg_6199" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_6200" s="T263">conj</ta>
            <ta e="T265" id="Seg_6201" s="T264">v</ta>
            <ta e="T266" id="Seg_6202" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_6203" s="T266">adj</ta>
            <ta e="T268" id="Seg_6204" s="T267">n</ta>
            <ta e="T269" id="Seg_6205" s="T268">adv</ta>
            <ta e="T270" id="Seg_6206" s="T269">v</ta>
            <ta e="T271" id="Seg_6207" s="T270">n</ta>
            <ta e="T272" id="Seg_6208" s="T271">adv</ta>
            <ta e="T274" id="Seg_6209" s="T273">v</ta>
            <ta e="T275" id="Seg_6210" s="T274">adv</ta>
            <ta e="T276" id="Seg_6211" s="T275">adv</ta>
            <ta e="T277" id="Seg_6212" s="T276">adv</ta>
            <ta e="T278" id="Seg_6213" s="T277">v</ta>
            <ta e="T279" id="Seg_6214" s="T278">adv</ta>
            <ta e="T280" id="Seg_6215" s="T279">adv</ta>
            <ta e="T281" id="Seg_6216" s="T280">v</ta>
            <ta e="T282" id="Seg_6217" s="T281">n</ta>
            <ta e="T283" id="Seg_6218" s="T282">v</ta>
            <ta e="T284" id="Seg_6219" s="T283">adv</ta>
            <ta e="T285" id="Seg_6220" s="T284">v</ta>
            <ta e="T286" id="Seg_6221" s="T285">n</ta>
            <ta e="T287" id="Seg_6222" s="T286">adv</ta>
            <ta e="T288" id="Seg_6223" s="T287">conj</ta>
            <ta e="T289" id="Seg_6224" s="T288">interrog</ta>
            <ta e="T290" id="Seg_6225" s="T289">v</ta>
            <ta e="T291" id="Seg_6226" s="T290">adv</ta>
            <ta e="T292" id="Seg_6227" s="T291">adv</ta>
            <ta e="T293" id="Seg_6228" s="T292">v</ta>
            <ta e="T294" id="Seg_6229" s="T293">conj</ta>
            <ta e="T295" id="Seg_6230" s="T294">adv</ta>
            <ta e="T296" id="Seg_6231" s="T295">v</ta>
            <ta e="T297" id="Seg_6232" s="T296">adj</ta>
            <ta e="T298" id="Seg_6233" s="T297">n</ta>
            <ta e="T299" id="Seg_6234" s="T298">adv</ta>
            <ta e="T300" id="Seg_6235" s="T299">adv</ta>
            <ta e="T301" id="Seg_6236" s="T300">v</ta>
            <ta e="T302" id="Seg_6237" s="T301">adv</ta>
            <ta e="T303" id="Seg_6238" s="T302">preverb</ta>
            <ta e="T304" id="Seg_6239" s="T303">v</ta>
            <ta e="T305" id="Seg_6240" s="T304">adj</ta>
            <ta e="T306" id="Seg_6241" s="T305">n</ta>
            <ta e="T307" id="Seg_6242" s="T306">adv</ta>
            <ta e="T308" id="Seg_6243" s="T307">n</ta>
            <ta e="T309" id="Seg_6244" s="T308">adv</ta>
            <ta e="T310" id="Seg_6245" s="T309">v</ta>
            <ta e="T311" id="Seg_6246" s="T310">n</ta>
            <ta e="T312" id="Seg_6247" s="T311">ptcl</ta>
            <ta e="T313" id="Seg_6248" s="T312">preverb</ta>
            <ta e="T314" id="Seg_6249" s="T313">v</ta>
            <ta e="T315" id="Seg_6250" s="T314">adj</ta>
            <ta e="T316" id="Seg_6251" s="T315">adj</ta>
            <ta e="T317" id="Seg_6252" s="T316">adj</ta>
            <ta e="T318" id="Seg_6253" s="T317">n</ta>
            <ta e="T319" id="Seg_6254" s="T318">nprop</ta>
            <ta e="T320" id="Seg_6255" s="T319">pp</ta>
            <ta e="T321" id="Seg_6256" s="T320">num</ta>
            <ta e="T322" id="Seg_6257" s="T321">n</ta>
            <ta e="T323" id="Seg_6258" s="T322">ptcl</ta>
            <ta e="T324" id="Seg_6259" s="T323">v</ta>
            <ta e="T325" id="Seg_6260" s="T324">adj</ta>
            <ta e="T326" id="Seg_6261" s="T325">adj</ta>
            <ta e="T327" id="Seg_6262" s="T326">ptcp</ta>
            <ta e="T328" id="Seg_6263" s="T327">n</ta>
            <ta e="T329" id="Seg_6264" s="T328">adv</ta>
            <ta e="T330" id="Seg_6265" s="T329">num</ta>
            <ta e="T331" id="Seg_6266" s="T330">n</ta>
            <ta e="T332" id="Seg_6267" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_6268" s="T332">v</ta>
            <ta e="T334" id="Seg_6269" s="T333">num</ta>
            <ta e="T335" id="Seg_6270" s="T334">n</ta>
            <ta e="T336" id="Seg_6271" s="T335">quant</ta>
            <ta e="T337" id="Seg_6272" s="T336">num</ta>
            <ta e="T338" id="Seg_6273" s="T337">n</ta>
            <ta e="T339" id="Seg_6274" s="T338">v</ta>
            <ta e="T340" id="Seg_6275" s="T339">adv</ta>
            <ta e="T341" id="Seg_6276" s="T340">adv</ta>
            <ta e="T342" id="Seg_6277" s="T341">v</ta>
            <ta e="T343" id="Seg_6278" s="T342">n</ta>
            <ta e="T344" id="Seg_6279" s="T343">interrog</ta>
            <ta e="T345" id="Seg_6280" s="T344">v</ta>
            <ta e="T346" id="Seg_6281" s="T345">n</ta>
            <ta e="T347" id="Seg_6282" s="T346">n</ta>
            <ta e="T348" id="Seg_6283" s="T347">n</ta>
            <ta e="T349" id="Seg_6284" s="T348">pers</ta>
            <ta e="T350" id="Seg_6285" s="T349">v</ta>
            <ta e="T351" id="Seg_6286" s="T350">v</ta>
            <ta e="T352" id="Seg_6287" s="T351">n</ta>
            <ta e="T353" id="Seg_6288" s="T352">v</ta>
            <ta e="T354" id="Seg_6289" s="T353">n</ta>
            <ta e="T355" id="Seg_6290" s="T354">quant</ta>
            <ta e="T356" id="Seg_6291" s="T355">v</ta>
            <ta e="T357" id="Seg_6292" s="T356">adj</ta>
            <ta e="T358" id="Seg_6293" s="T357">adj</ta>
            <ta e="T359" id="Seg_6294" s="T358">num</ta>
            <ta e="T360" id="Seg_6295" s="T359">n</ta>
            <ta e="T361" id="Seg_6296" s="T360">conj</ta>
            <ta e="T362" id="Seg_6297" s="T361">adj</ta>
            <ta e="T363" id="Seg_6298" s="T362">num</ta>
            <ta e="T364" id="Seg_6299" s="T363">n</ta>
            <ta e="T365" id="Seg_6300" s="T364">adv</ta>
            <ta e="T366" id="Seg_6301" s="T365">preverb</ta>
            <ta e="T367" id="Seg_6302" s="T366">v</ta>
            <ta e="T368" id="Seg_6303" s="T367">adj</ta>
            <ta e="T369" id="Seg_6304" s="T368">n</ta>
            <ta e="T370" id="Seg_6305" s="T369">interrog</ta>
            <ta e="T371" id="Seg_6306" s="T370">n</ta>
            <ta e="T372" id="Seg_6307" s="T371">v</ta>
            <ta e="T373" id="Seg_6308" s="T372">pers</ta>
            <ta e="T374" id="Seg_6309" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_6310" s="T374">dem</ta>
            <ta e="T376" id="Seg_6311" s="T375">v</ta>
            <ta e="T377" id="Seg_6312" s="T376">adj</ta>
            <ta e="T378" id="Seg_6313" s="T377">n</ta>
            <ta e="T379" id="Seg_6314" s="T378">num</ta>
            <ta e="T380" id="Seg_6315" s="T379">n</ta>
            <ta e="T381" id="Seg_6316" s="T380">adv</ta>
            <ta e="T382" id="Seg_6317" s="T381">v</ta>
            <ta e="T383" id="Seg_6318" s="T382">n</ta>
            <ta e="T384" id="Seg_6319" s="T383">n</ta>
            <ta e="T385" id="Seg_6320" s="T384">v</ta>
            <ta e="T386" id="Seg_6321" s="T385">n</ta>
            <ta e="T387" id="Seg_6322" s="T386">adv</ta>
            <ta e="T388" id="Seg_6323" s="T387">preverb</ta>
            <ta e="T389" id="Seg_6324" s="T388">v</ta>
            <ta e="T390" id="Seg_6325" s="T389">adj</ta>
            <ta e="T391" id="Seg_6326" s="T390">adj</ta>
            <ta e="T392" id="Seg_6327" s="T391">num</ta>
            <ta e="T393" id="Seg_6328" s="T392">n</ta>
            <ta e="T394" id="Seg_6329" s="T393">adj</ta>
            <ta e="T395" id="Seg_6330" s="T394">adj</ta>
            <ta e="T396" id="Seg_6331" s="T395">ptcp</ta>
            <ta e="T397" id="Seg_6332" s="T396">n</ta>
            <ta e="T398" id="Seg_6333" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_6334" s="T398">v</ta>
            <ta e="T400" id="Seg_6335" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_6336" s="T400">ptcp</ta>
            <ta e="T402" id="Seg_6337" s="T401">n</ta>
            <ta e="T403" id="Seg_6338" s="T402">v</ta>
            <ta e="T404" id="Seg_6339" s="T403">nprop</ta>
            <ta e="T405" id="Seg_6340" s="T404">n</ta>
            <ta e="T406" id="Seg_6341" s="T405">adj</ta>
            <ta e="T407" id="Seg_6342" s="T406">adj</ta>
            <ta e="T408" id="Seg_6343" s="T407">ptcp</ta>
            <ta e="T409" id="Seg_6344" s="T408">n</ta>
            <ta e="T410" id="Seg_6345" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_6346" s="T410">adv</ta>
            <ta e="T412" id="Seg_6347" s="T411">v</ta>
            <ta e="T413" id="Seg_6348" s="T412">adv</ta>
            <ta e="T414" id="Seg_6349" s="T413">v</ta>
            <ta e="T415" id="Seg_6350" s="T414">n</ta>
            <ta e="T416" id="Seg_6351" s="T415">adv</ta>
            <ta e="T417" id="Seg_6352" s="T416">v</ta>
            <ta e="T418" id="Seg_6353" s="T417">dem</ta>
            <ta e="T419" id="Seg_6354" s="T418">num</ta>
            <ta e="T420" id="Seg_6355" s="T419">n</ta>
            <ta e="T421" id="Seg_6356" s="T420">n</ta>
            <ta e="T422" id="Seg_6357" s="T421">v</ta>
            <ta e="T423" id="Seg_6358" s="T422">conj</ta>
            <ta e="T424" id="Seg_6359" s="T423">v</ta>
            <ta e="T425" id="Seg_6360" s="T424">interrog</ta>
            <ta e="T426" id="Seg_6361" s="T425">v</ta>
            <ta e="T427" id="Seg_6362" s="T426">adv</ta>
            <ta e="T428" id="Seg_6363" s="T427">v</ta>
            <ta e="T429" id="Seg_6364" s="T428">adv</ta>
            <ta e="T430" id="Seg_6365" s="T429">adv</ta>
            <ta e="T431" id="Seg_6366" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_6367" s="T431">v</ta>
            <ta e="T433" id="Seg_6368" s="T432">n</ta>
            <ta e="T434" id="Seg_6369" s="T433">v</ta>
            <ta e="T435" id="Seg_6370" s="T434">adv</ta>
            <ta e="T436" id="Seg_6371" s="T435">v</ta>
            <ta e="T437" id="Seg_6372" s="T436">adj</ta>
            <ta e="T438" id="Seg_6373" s="T437">n</ta>
            <ta e="T439" id="Seg_6374" s="T438">num</ta>
            <ta e="T440" id="Seg_6375" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_6376" s="T440">v</ta>
            <ta e="T442" id="Seg_6377" s="T441">n</ta>
            <ta e="T443" id="Seg_6378" s="T442">n</ta>
            <ta e="T444" id="Seg_6379" s="T443">adj</ta>
            <ta e="T445" id="Seg_6380" s="T444">v</ta>
            <ta e="T446" id="Seg_6381" s="T445">n</ta>
            <ta e="T448" id="Seg_6382" s="T447">v</ta>
            <ta e="T449" id="Seg_6383" s="T448">nprop</ta>
            <ta e="T450" id="Seg_6384" s="T449">dem</ta>
            <ta e="T451" id="Seg_6385" s="T450">n</ta>
            <ta e="T452" id="Seg_6386" s="T451">v</ta>
            <ta e="T453" id="Seg_6387" s="T452">n</ta>
            <ta e="T454" id="Seg_6388" s="T453">n</ta>
            <ta e="T455" id="Seg_6389" s="T454">pp</ta>
            <ta e="T456" id="Seg_6390" s="T455">adv</ta>
            <ta e="T457" id="Seg_6391" s="T456">v</ta>
            <ta e="T458" id="Seg_6392" s="T457">v</ta>
            <ta e="T459" id="Seg_6393" s="T458">ptcl</ta>
            <ta e="T460" id="Seg_6394" s="T459">v</ta>
            <ta e="T461" id="Seg_6395" s="T460">pers</ta>
            <ta e="T462" id="Seg_6396" s="T461">v</ta>
            <ta e="T463" id="Seg_6397" s="T462">pro</ta>
            <ta e="T464" id="Seg_6398" s="T463">adj</ta>
            <ta e="T465" id="Seg_6399" s="T464">n</ta>
            <ta e="T466" id="Seg_6400" s="T465">adv</ta>
            <ta e="T467" id="Seg_6401" s="T466">interrog</ta>
            <ta e="T468" id="Seg_6402" s="T467">v</ta>
            <ta e="T469" id="Seg_6403" s="T468">pers</ta>
            <ta e="T470" id="Seg_6404" s="T469">v</ta>
            <ta e="T471" id="Seg_6405" s="T470">adv</ta>
            <ta e="T472" id="Seg_6406" s="T471">num</ta>
            <ta e="T473" id="Seg_6407" s="T472">n</ta>
            <ta e="T474" id="Seg_6408" s="T473">quant</ta>
            <ta e="T475" id="Seg_6409" s="T474">v</ta>
            <ta e="T476" id="Seg_6410" s="T475">num</ta>
            <ta e="T477" id="Seg_6411" s="T476">pp</ta>
            <ta e="T478" id="Seg_6412" s="T477">pp</ta>
            <ta e="T479" id="Seg_6413" s="T478">n</ta>
            <ta e="T480" id="Seg_6414" s="T479">adv</ta>
            <ta e="T481" id="Seg_6415" s="T480">v</ta>
            <ta e="T482" id="Seg_6416" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_6417" s="T482">interrog</ta>
            <ta e="T484" id="Seg_6418" s="T483">n</ta>
            <ta e="T485" id="Seg_6419" s="T484">v</ta>
            <ta e="T486" id="Seg_6420" s="T485">v</ta>
            <ta e="T487" id="Seg_6421" s="T486">adj</ta>
            <ta e="T488" id="Seg_6422" s="T487">adj</ta>
            <ta e="T489" id="Seg_6423" s="T488">adj</ta>
            <ta e="T490" id="Seg_6424" s="T489">adv</ta>
            <ta e="T491" id="Seg_6425" s="T490">v</ta>
            <ta e="T492" id="Seg_6426" s="T491">n</ta>
            <ta e="T493" id="Seg_6427" s="T492">v</ta>
            <ta e="T494" id="Seg_6428" s="T493">adv</ta>
            <ta e="T495" id="Seg_6429" s="T494">ptcl</ta>
            <ta e="T496" id="Seg_6430" s="T495">v</ta>
            <ta e="T497" id="Seg_6431" s="T496">adv</ta>
            <ta e="T498" id="Seg_6432" s="T497">conj</ta>
            <ta e="T499" id="Seg_6433" s="T498">n</ta>
            <ta e="T500" id="Seg_6434" s="T499">n</ta>
            <ta e="T501" id="Seg_6435" s="T500">dem</ta>
            <ta e="T502" id="Seg_6436" s="T501">pp</ta>
            <ta e="T503" id="Seg_6437" s="T502">adv</ta>
            <ta e="T504" id="Seg_6438" s="T503">v</ta>
            <ta e="T505" id="Seg_6439" s="T504">pers</ta>
            <ta e="T507" id="Seg_6440" s="T506">n</ta>
            <ta e="T508" id="Seg_6441" s="T507">v</ta>
            <ta e="T509" id="Seg_6442" s="T508">n</ta>
            <ta e="T510" id="Seg_6443" s="T509">n</ta>
            <ta e="T511" id="Seg_6444" s="T510">ptcl</ta>
            <ta e="T512" id="Seg_6445" s="T511">n</ta>
            <ta e="T513" id="Seg_6446" s="T512">adj</ta>
            <ta e="T514" id="Seg_6447" s="T513">n</ta>
            <ta e="T515" id="Seg_6448" s="T514">adv</ta>
            <ta e="T516" id="Seg_6449" s="T515">v</ta>
            <ta e="T517" id="Seg_6450" s="T516">n</ta>
            <ta e="T518" id="Seg_6451" s="T517">pers</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_6452" s="T1">np.h:E</ta>
            <ta e="T4" id="Seg_6453" s="T3">np.h:E</ta>
            <ta e="T8" id="Seg_6454" s="T7">np.h:Th</ta>
            <ta e="T14" id="Seg_6455" s="T13">np.h:Th</ta>
            <ta e="T16" id="Seg_6456" s="T15">np.h:Th</ta>
            <ta e="T23" id="Seg_6457" s="T22">np.h:Th</ta>
            <ta e="T29" id="Seg_6458" s="T28">0.3.h:E</ta>
            <ta e="T34" id="Seg_6459" s="T33">np.h:Th</ta>
            <ta e="T36" id="Seg_6460" s="T35">np:Ins</ta>
            <ta e="T40" id="Seg_6461" s="T37">np:Th</ta>
            <ta e="T42" id="Seg_6462" s="T41">0.3.h:E</ta>
            <ta e="T46" id="Seg_6463" s="T45">0.3.h:Th</ta>
            <ta e="T47" id="Seg_6464" s="T46">np:P</ta>
            <ta e="T48" id="Seg_6465" s="T47">0.3.h:A</ta>
            <ta e="T51" id="Seg_6466" s="T50">0.3.h:A</ta>
            <ta e="T52" id="Seg_6467" s="T51">np:P</ta>
            <ta e="T54" id="Seg_6468" s="T53">0.3.h:A</ta>
            <ta e="T56" id="Seg_6469" s="T55">np:So</ta>
            <ta e="T61" id="Seg_6470" s="T60">0.1.h:E</ta>
            <ta e="T62" id="Seg_6471" s="T61">np:P</ta>
            <ta e="T63" id="Seg_6472" s="T62">pp:So</ta>
            <ta e="T65" id="Seg_6473" s="T64">pro.h:A</ta>
            <ta e="T68" id="Seg_6474" s="T67">np:P</ta>
            <ta e="T72" id="Seg_6475" s="T71">pp:So</ta>
            <ta e="T74" id="Seg_6476" s="T73">np:Th</ta>
            <ta e="T75" id="Seg_6477" s="T74">0.3.h:A</ta>
            <ta e="T78" id="Seg_6478" s="T77">np:Th</ta>
            <ta e="T79" id="Seg_6479" s="T78">0.3.h:A</ta>
            <ta e="T82" id="Seg_6480" s="T81">np:Th</ta>
            <ta e="T83" id="Seg_6481" s="T82">0.3.h:A</ta>
            <ta e="T86" id="Seg_6482" s="T85">0.3.h:E</ta>
            <ta e="T90" id="Seg_6483" s="T89">0.3.h:A</ta>
            <ta e="T91" id="Seg_6484" s="T90">pro.h:A</ta>
            <ta e="T95" id="Seg_6485" s="T94">pro:So</ta>
            <ta e="T97" id="Seg_6486" s="T96">0.3.h:A</ta>
            <ta e="T98" id="Seg_6487" s="T97">pro.h:E</ta>
            <ta e="T101" id="Seg_6488" s="T100">np.h:A</ta>
            <ta e="T105" id="Seg_6489" s="T104">np.h:Poss</ta>
            <ta e="T106" id="Seg_6490" s="T105">np:L</ta>
            <ta e="T109" id="Seg_6491" s="T108">np.h:Th</ta>
            <ta e="T113" id="Seg_6492" s="T112">0.3.h:A</ta>
            <ta e="T114" id="Seg_6493" s="T113">np:Time</ta>
            <ta e="T117" id="Seg_6494" s="T116">np.h:Th</ta>
            <ta e="T118" id="Seg_6495" s="T117">pro.h:E</ta>
            <ta e="T122" id="Seg_6496" s="T121">np:L</ta>
            <ta e="T125" id="Seg_6497" s="T124">np:L</ta>
            <ta e="T126" id="Seg_6498" s="T125">np:L</ta>
            <ta e="T130" id="Seg_6499" s="T129">np.h:Th</ta>
            <ta e="T133" id="Seg_6500" s="T132">np:L</ta>
            <ta e="T138" id="Seg_6501" s="T137">np.h:A</ta>
            <ta e="T143" id="Seg_6502" s="T142">np.h:A</ta>
            <ta e="T145" id="Seg_6503" s="T144">np.h:R</ta>
            <ta e="T147" id="Seg_6504" s="T146">0.3.h:A</ta>
            <ta e="T151" id="Seg_6505" s="T150">np.h:A</ta>
            <ta e="T155" id="Seg_6506" s="T154">pro.h:P</ta>
            <ta e="T157" id="Seg_6507" s="T156">np:G</ta>
            <ta e="T158" id="Seg_6508" s="T157">0.1.h:A</ta>
            <ta e="T159" id="Seg_6509" s="T158">np:G</ta>
            <ta e="T160" id="Seg_6510" s="T159">0.1.h:A</ta>
            <ta e="T161" id="Seg_6511" s="T160">np:Th</ta>
            <ta e="T162" id="Seg_6512" s="T161">0.1.h:A</ta>
            <ta e="T163" id="Seg_6513" s="T162">np.h:A</ta>
            <ta e="T167" id="Seg_6514" s="T166">0.3.h:A</ta>
            <ta e="T168" id="Seg_6515" s="T167">adv:Path</ta>
            <ta e="T169" id="Seg_6516" s="T168">0.3.h:A</ta>
            <ta e="T170" id="Seg_6517" s="T169">np:G</ta>
            <ta e="T171" id="Seg_6518" s="T170">0.3.h:A</ta>
            <ta e="T172" id="Seg_6519" s="T171">0.3.h:A</ta>
            <ta e="T173" id="Seg_6520" s="T172">np:Th</ta>
            <ta e="T174" id="Seg_6521" s="T173">0.3.h:A</ta>
            <ta e="T175" id="Seg_6522" s="T174">np:G</ta>
            <ta e="T177" id="Seg_6523" s="T176">np:L</ta>
            <ta e="T178" id="Seg_6524" s="T177">np:Th</ta>
            <ta e="T179" id="Seg_6525" s="T178">0.3.h:A</ta>
            <ta e="T180" id="Seg_6526" s="T179">adv:So</ta>
            <ta e="T183" id="Seg_6527" s="T182">np:Th</ta>
            <ta e="T184" id="Seg_6528" s="T183">0.3.h:A</ta>
            <ta e="T185" id="Seg_6529" s="T184">np.h:R</ta>
            <ta e="T186" id="Seg_6530" s="T185">np:Th</ta>
            <ta e="T187" id="Seg_6531" s="T186">np:Th</ta>
            <ta e="T188" id="Seg_6532" s="T187">0.2.h:A</ta>
            <ta e="T189" id="Seg_6533" s="T188">np:Time</ta>
            <ta e="T190" id="Seg_6534" s="T189">0.3.h:A</ta>
            <ta e="T192" id="Seg_6535" s="T191">np:Th</ta>
            <ta e="T193" id="Seg_6536" s="T192">np:G</ta>
            <ta e="T194" id="Seg_6537" s="T193">0.2.h:A</ta>
            <ta e="T195" id="Seg_6538" s="T194">np:Th</ta>
            <ta e="T196" id="Seg_6539" s="T195">np:Th</ta>
            <ta e="T197" id="Seg_6540" s="T196">adv:L</ta>
            <ta e="T198" id="Seg_6541" s="T197">0.3.h:A</ta>
            <ta e="T199" id="Seg_6542" s="T198">adv:Time</ta>
            <ta e="T200" id="Seg_6543" s="T199">adv:L</ta>
            <ta e="T201" id="Seg_6544" s="T200">0.3.h:A</ta>
            <ta e="T208" id="Seg_6545" s="T207">np:Th</ta>
            <ta e="T213" id="Seg_6546" s="T212">np:Th</ta>
            <ta e="T215" id="Seg_6547" s="T214">np.h:R</ta>
            <ta e="T217" id="Seg_6548" s="T216">0.3.h:A</ta>
            <ta e="T219" id="Seg_6549" s="T218">np:Th</ta>
            <ta e="T221" id="Seg_6550" s="T220">0.2.h:A</ta>
            <ta e="T222" id="Seg_6551" s="T221">np:Poss</ta>
            <ta e="T224" id="Seg_6552" s="T223">pp:Path</ta>
            <ta e="T225" id="Seg_6553" s="T224">0.2.h:A</ta>
            <ta e="T228" id="Seg_6554" s="T227">0.2.h:A</ta>
            <ta e="T229" id="Seg_6555" s="T228">np:Th</ta>
            <ta e="T231" id="Seg_6556" s="T230">np:Th</ta>
            <ta e="T232" id="Seg_6557" s="T231">pro.h:B</ta>
            <ta e="T233" id="Seg_6558" s="T232">0.2.h:A</ta>
            <ta e="T234" id="Seg_6559" s="T233">np:P</ta>
            <ta e="T235" id="Seg_6560" s="T234">0.1.h:A</ta>
            <ta e="T240" id="Seg_6561" s="T239">np.h:E</ta>
            <ta e="T241" id="Seg_6562" s="T240">np:L</ta>
            <ta e="T243" id="Seg_6563" s="T242">np:Th</ta>
            <ta e="T246" id="Seg_6564" s="T245">0.3.h:A</ta>
            <ta e="T247" id="Seg_6565" s="T246">adv:L</ta>
            <ta e="T250" id="Seg_6566" s="T249">np:Th</ta>
            <ta e="T251" id="Seg_6567" s="T250">0.3.h:A</ta>
            <ta e="T253" id="Seg_6568" s="T252">0.3.h:E</ta>
            <ta e="T259" id="Seg_6569" s="T258">np:Th</ta>
            <ta e="T260" id="Seg_6570" s="T259">np.h:R</ta>
            <ta e="T261" id="Seg_6571" s="T260">0.3.h:A</ta>
            <ta e="T262" id="Seg_6572" s="T261">adv:Path</ta>
            <ta e="T265" id="Seg_6573" s="T264">0.3.h:A</ta>
            <ta e="T268" id="Seg_6574" s="T267">np:Th</ta>
            <ta e="T269" id="Seg_6575" s="T268">adv:Path</ta>
            <ta e="T270" id="Seg_6576" s="T269">0.3.h:A</ta>
            <ta e="T271" id="Seg_6577" s="T270">np:So</ta>
            <ta e="T272" id="Seg_6578" s="T271">adv:Path</ta>
            <ta e="T274" id="Seg_6579" s="T273">0.3.h:A</ta>
            <ta e="T275" id="Seg_6580" s="T274">adv:Path</ta>
            <ta e="T278" id="Seg_6581" s="T277">0.3.h:A</ta>
            <ta e="T279" id="Seg_6582" s="T278">adv:Path</ta>
            <ta e="T281" id="Seg_6583" s="T280">0.3.h:A</ta>
            <ta e="T282" id="Seg_6584" s="T281">np:Ins</ta>
            <ta e="T283" id="Seg_6585" s="T282">adv:Path</ta>
            <ta e="T285" id="Seg_6586" s="T284">np:L</ta>
            <ta e="T286" id="Seg_6587" s="T285">np:So</ta>
            <ta e="T287" id="Seg_6588" s="T286">adv:Path</ta>
            <ta e="T289" id="Seg_6589" s="T288">np:Th</ta>
            <ta e="T290" id="Seg_6590" s="T289">0.2.h:A</ta>
            <ta e="T291" id="Seg_6591" s="T290">adv:Path</ta>
            <ta e="T293" id="Seg_6592" s="T292">0.2.h:A</ta>
            <ta e="T298" id="Seg_6593" s="T297">np.h:A</ta>
            <ta e="T299" id="Seg_6594" s="T298">adv:Path</ta>
            <ta e="T301" id="Seg_6595" s="T300">0.2.h:A</ta>
            <ta e="T302" id="Seg_6596" s="T301">adv:Path</ta>
            <ta e="T304" id="Seg_6597" s="T303">0.3.h:A</ta>
            <ta e="T306" id="Seg_6598" s="T305">np:Th</ta>
            <ta e="T307" id="Seg_6599" s="T306">adv:L</ta>
            <ta e="T308" id="Seg_6600" s="T307">np:L</ta>
            <ta e="T310" id="Seg_6601" s="T309">0.3.h:A</ta>
            <ta e="T311" id="Seg_6602" s="T310">np:G</ta>
            <ta e="T314" id="Seg_6603" s="T313">0.3.h:A</ta>
            <ta e="T318" id="Seg_6604" s="T317">np.h:A</ta>
            <ta e="T319" id="Seg_6605" s="T318">np.h:Th</ta>
            <ta e="T322" id="Seg_6606" s="T321">np:L</ta>
            <ta e="T328" id="Seg_6607" s="T327">np:Com</ta>
            <ta e="T331" id="Seg_6608" s="T330">np:L</ta>
            <ta e="T333" id="Seg_6609" s="T332">0.3.h:A</ta>
            <ta e="T335" id="Seg_6610" s="T334">np.h:A</ta>
            <ta e="T338" id="Seg_6611" s="T337">np:L</ta>
            <ta e="T342" id="Seg_6612" s="T341">0.3.h:A</ta>
            <ta e="T343" id="Seg_6613" s="T342">np:Th</ta>
            <ta e="T345" id="Seg_6614" s="T344">0.1.h:E</ta>
            <ta e="T346" id="Seg_6615" s="T345">np:Th</ta>
            <ta e="T348" id="Seg_6616" s="T347">np:Th</ta>
            <ta e="T349" id="Seg_6617" s="T348">pro.h:B</ta>
            <ta e="T350" id="Seg_6618" s="T349">0.2.h:A</ta>
            <ta e="T352" id="Seg_6619" s="T351">np:P</ta>
            <ta e="T353" id="Seg_6620" s="T352">0.1.h:A</ta>
            <ta e="T354" id="Seg_6621" s="T353">np:Th</ta>
            <ta e="T360" id="Seg_6622" s="T359">np.h:A</ta>
            <ta e="T364" id="Seg_6623" s="T363">np.h:A</ta>
            <ta e="T365" id="Seg_6624" s="T364">adv:Time</ta>
            <ta e="T369" id="Seg_6625" s="T368">np.h:A</ta>
            <ta e="T371" id="Seg_6626" s="T370">np:Ins</ta>
            <ta e="T373" id="Seg_6627" s="T372">pro.h:A</ta>
            <ta e="T378" id="Seg_6628" s="T377">np.h:A</ta>
            <ta e="T380" id="Seg_6629" s="T379">np.h:R</ta>
            <ta e="T381" id="Seg_6630" s="T380">adv:Time</ta>
            <ta e="T383" id="Seg_6631" s="T382">np:Ins</ta>
            <ta e="T384" id="Seg_6632" s="T383">np.h:R</ta>
            <ta e="T386" id="Seg_6633" s="T385">np:Ins</ta>
            <ta e="T389" id="Seg_6634" s="T388">0.3.h:A</ta>
            <ta e="T393" id="Seg_6635" s="T392">np.h:R</ta>
            <ta e="T397" id="Seg_6636" s="T396">np.h:R</ta>
            <ta e="T399" id="Seg_6637" s="T398">0.3.h:A</ta>
            <ta e="T402" id="Seg_6638" s="T401">np:P</ta>
            <ta e="T403" id="Seg_6639" s="T402">0.3.h:A</ta>
            <ta e="T404" id="Seg_6640" s="T403">np.h:Poss</ta>
            <ta e="T405" id="Seg_6641" s="T404">np.h:A</ta>
            <ta e="T409" id="Seg_6642" s="T408">np.h:A</ta>
            <ta e="T414" id="Seg_6643" s="T413">0.3.h:A</ta>
            <ta e="T415" id="Seg_6644" s="T414">np:Th</ta>
            <ta e="T417" id="Seg_6645" s="T416">0.3.h:A</ta>
            <ta e="T420" id="Seg_6646" s="T419">np.h:A</ta>
            <ta e="T421" id="Seg_6647" s="T420">np:L</ta>
            <ta e="T424" id="Seg_6648" s="T423">0.3.h:A</ta>
            <ta e="T426" id="Seg_6649" s="T425">0.3.h:A</ta>
            <ta e="T428" id="Seg_6650" s="T427">0.3.h:A</ta>
            <ta e="T429" id="Seg_6651" s="T428">adv:G</ta>
            <ta e="T432" id="Seg_6652" s="T431">0.3.h:A</ta>
            <ta e="T433" id="Seg_6653" s="T432">np:P</ta>
            <ta e="T434" id="Seg_6654" s="T433">0.3.h:A</ta>
            <ta e="T438" id="Seg_6655" s="T437">np.h:E</ta>
            <ta e="T441" id="Seg_6656" s="T440">0.3.h:A</ta>
            <ta e="T445" id="Seg_6657" s="T444">0.3.h:A</ta>
            <ta e="T446" id="Seg_6658" s="T445">np:P</ta>
            <ta e="T448" id="Seg_6659" s="T447">0.3.h:E</ta>
            <ta e="T449" id="Seg_6660" s="T448">np.h:A</ta>
            <ta e="T451" id="Seg_6661" s="T450">np.h:P</ta>
            <ta e="T453" id="Seg_6662" s="T452">np.h:A</ta>
            <ta e="T456" id="Seg_6663" s="T455">adv:Path</ta>
            <ta e="T458" id="Seg_6664" s="T457">0.3.h:A</ta>
            <ta e="T460" id="Seg_6665" s="T459">0.3.h:A</ta>
            <ta e="T461" id="Seg_6666" s="T460">pro.h:A</ta>
            <ta e="T465" id="Seg_6667" s="T464">np.h:A</ta>
            <ta e="T467" id="Seg_6668" s="T466">np:Th</ta>
            <ta e="T469" id="Seg_6669" s="T468">pro.h:A</ta>
            <ta e="T473" id="Seg_6670" s="T472">np.h:P</ta>
            <ta e="T475" id="Seg_6671" s="T474">0.3.h:A</ta>
            <ta e="T480" id="Seg_6672" s="T479">adv:Time</ta>
            <ta e="T481" id="Seg_6673" s="T480">0.3.h:E</ta>
            <ta e="T484" id="Seg_6674" s="T483">np:Th</ta>
            <ta e="T488" id="Seg_6675" s="T487">np.h:A</ta>
            <ta e="T489" id="Seg_6676" s="T488">np.h:E</ta>
            <ta e="T492" id="Seg_6677" s="T491">np.h:A</ta>
            <ta e="T494" id="Seg_6678" s="T493">adv:Time</ta>
            <ta e="T504" id="Seg_6679" s="T503">0.3.h:E</ta>
            <ta e="T505" id="Seg_6680" s="T504">pro.h:A</ta>
            <ta e="T507" id="Seg_6681" s="T506">np.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_6682" s="T1">np.h:S</ta>
            <ta e="T5" id="Seg_6683" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_6684" s="T5">n:pred</ta>
            <ta e="T8" id="Seg_6685" s="T7">np.h:S</ta>
            <ta e="T9" id="Seg_6686" s="T8">cop</ta>
            <ta e="T14" id="Seg_6687" s="T13">np.h:S</ta>
            <ta e="T16" id="Seg_6688" s="T15">np.h:S</ta>
            <ta e="T19" id="Seg_6689" s="T18">ptcl:pred</ta>
            <ta e="T20" id="Seg_6690" s="T19">cop</ta>
            <ta e="T23" id="Seg_6691" s="T22">np.h:S</ta>
            <ta e="T26" id="Seg_6692" s="T25">ptcl:pred</ta>
            <ta e="T27" id="Seg_6693" s="T26">cop</ta>
            <ta e="T29" id="Seg_6694" s="T28">0.3.h:S</ta>
            <ta e="T34" id="Seg_6695" s="T33">np.h:S</ta>
            <ta e="T36" id="Seg_6696" s="T35">n:pred</ta>
            <ta e="T37" id="Seg_6697" s="T36">cop</ta>
            <ta e="T39" id="Seg_6698" s="T38">np:S</ta>
            <ta e="T40" id="Seg_6699" s="T39">s:compl</ta>
            <ta e="T42" id="Seg_6700" s="T41">0.3.h:S v:pred</ta>
            <ta e="T45" id="Seg_6701" s="T44">n:pred</ta>
            <ta e="T46" id="Seg_6702" s="T45">0.3.h:S cop</ta>
            <ta e="T47" id="Seg_6703" s="T46">np:O</ta>
            <ta e="T48" id="Seg_6704" s="T47">0.3.h:S v:pred</ta>
            <ta e="T51" id="Seg_6705" s="T50">0.3.h:S v:pred</ta>
            <ta e="T52" id="Seg_6706" s="T51">np:O</ta>
            <ta e="T54" id="Seg_6707" s="T53">0.3.h:S v:pred</ta>
            <ta e="T61" id="Seg_6708" s="T60">0.1.h:S v:pred</ta>
            <ta e="T65" id="Seg_6709" s="T64">pro.h:S</ta>
            <ta e="T66" id="Seg_6710" s="T65">v:pred</ta>
            <ta e="T68" id="Seg_6711" s="T67">np:O</ta>
            <ta e="T69" id="Seg_6712" s="T68">0.3.h:S v:pred</ta>
            <ta e="T74" id="Seg_6713" s="T73">np:O</ta>
            <ta e="T75" id="Seg_6714" s="T74">0.3.h:S v:pred</ta>
            <ta e="T78" id="Seg_6715" s="T77">np:O</ta>
            <ta e="T79" id="Seg_6716" s="T78">0.3.h:S v:pred</ta>
            <ta e="T82" id="Seg_6717" s="T81">np:O</ta>
            <ta e="T83" id="Seg_6718" s="T82">0.3.h:S v:pred</ta>
            <ta e="T86" id="Seg_6719" s="T85">0.3.h:S v:pred</ta>
            <ta e="T88" id="Seg_6720" s="T87">s:compl</ta>
            <ta e="T90" id="Seg_6721" s="T89">0.3.h:S v:pred</ta>
            <ta e="T91" id="Seg_6722" s="T90">pro.h:S</ta>
            <ta e="T92" id="Seg_6723" s="T91">s:compl</ta>
            <ta e="T93" id="Seg_6724" s="T92">v:pred</ta>
            <ta e="T97" id="Seg_6725" s="T96">0.3.h:S v:pred</ta>
            <ta e="T98" id="Seg_6726" s="T97">pro.h:S</ta>
            <ta e="T100" id="Seg_6727" s="T99">v:pred</ta>
            <ta e="T101" id="Seg_6728" s="T100">np.h:S</ta>
            <ta e="T102" id="Seg_6729" s="T101">v:pred</ta>
            <ta e="T103" id="Seg_6730" s="T102">s:compl</ta>
            <ta e="T109" id="Seg_6731" s="T108">np.h:S</ta>
            <ta e="T110" id="Seg_6732" s="T109">cop</ta>
            <ta e="T112" id="Seg_6733" s="T111">s:purp</ta>
            <ta e="T113" id="Seg_6734" s="T112">0.3.h:S v:pred</ta>
            <ta e="T117" id="Seg_6735" s="T116">np.h:O</ta>
            <ta e="T118" id="Seg_6736" s="T117">pro.h:S</ta>
            <ta e="T119" id="Seg_6737" s="T118">v:pred</ta>
            <ta e="T130" id="Seg_6738" s="T129">np.h:S</ta>
            <ta e="T135" id="Seg_6739" s="T134">cop</ta>
            <ta e="T138" id="Seg_6740" s="T137">np.h:S</ta>
            <ta e="T139" id="Seg_6741" s="T138">s:purp</ta>
            <ta e="T140" id="Seg_6742" s="T139">v:pred</ta>
            <ta e="T143" id="Seg_6743" s="T142">np.h:S</ta>
            <ta e="T144" id="Seg_6744" s="T143">v:pred</ta>
            <ta e="T147" id="Seg_6745" s="T146">0.3.h:S v:pred</ta>
            <ta e="T151" id="Seg_6746" s="T150">np.h:S</ta>
            <ta e="T153" id="Seg_6747" s="T152">v:pred</ta>
            <ta e="T154" id="Seg_6748" s="T153">s:purp</ta>
            <ta e="T158" id="Seg_6749" s="T157">0.1.h:S v:pred</ta>
            <ta e="T160" id="Seg_6750" s="T159">0.1.h:S v:pred</ta>
            <ta e="T161" id="Seg_6751" s="T160">np:O</ta>
            <ta e="T162" id="Seg_6752" s="T161">0.1.h:S v:pred</ta>
            <ta e="T163" id="Seg_6753" s="T162">np.h:S</ta>
            <ta e="T165" id="Seg_6754" s="T164">s:purp</ta>
            <ta e="T166" id="Seg_6755" s="T165">v:pred</ta>
            <ta e="T167" id="Seg_6756" s="T166">0.3.h:S v:pred</ta>
            <ta e="T169" id="Seg_6757" s="T168">0.3.h:S v:pred</ta>
            <ta e="T171" id="Seg_6758" s="T170">0.3.h:S v:pred</ta>
            <ta e="T172" id="Seg_6759" s="T171">0.3.h:S v:pred</ta>
            <ta e="T173" id="Seg_6760" s="T172">np:O</ta>
            <ta e="T174" id="Seg_6761" s="T173">0.3.h:S v:pred</ta>
            <ta e="T178" id="Seg_6762" s="T177">np:O</ta>
            <ta e="T179" id="Seg_6763" s="T178">0.3.h:S v:pred</ta>
            <ta e="T183" id="Seg_6764" s="T182">np:O</ta>
            <ta e="T184" id="Seg_6765" s="T183">0.3.h:S v:pred</ta>
            <ta e="T186" id="Seg_6766" s="T185">np:O</ta>
            <ta e="T187" id="Seg_6767" s="T186">np:O</ta>
            <ta e="T188" id="Seg_6768" s="T187">0.2.h:S v:pred</ta>
            <ta e="T190" id="Seg_6769" s="T189">0.3.h:S v:pred</ta>
            <ta e="T192" id="Seg_6770" s="T191">np:O</ta>
            <ta e="T194" id="Seg_6771" s="T193">0.2.h:S v:pred</ta>
            <ta e="T195" id="Seg_6772" s="T194">np:O</ta>
            <ta e="T196" id="Seg_6773" s="T195">np:O</ta>
            <ta e="T198" id="Seg_6774" s="T197">0.3.h:S v:pred</ta>
            <ta e="T201" id="Seg_6775" s="T200">0.3.h:S v:pred</ta>
            <ta e="T206" id="Seg_6776" s="T205">v:pred</ta>
            <ta e="T208" id="Seg_6777" s="T207">np:S</ta>
            <ta e="T209" id="Seg_6778" s="T208">v:pred</ta>
            <ta e="T213" id="Seg_6779" s="T212">np:S</ta>
            <ta e="T214" id="Seg_6780" s="T213">v:pred</ta>
            <ta e="T217" id="Seg_6781" s="T216">0.3.h:S v:pred</ta>
            <ta e="T219" id="Seg_6782" s="T218">np:O</ta>
            <ta e="T221" id="Seg_6783" s="T220">0.2.h:S v:pred</ta>
            <ta e="T225" id="Seg_6784" s="T224">0.2.h:S 0.3:O v:pred</ta>
            <ta e="T228" id="Seg_6785" s="T227">0.2.h:S v:pred</ta>
            <ta e="T229" id="Seg_6786" s="T228">np:S</ta>
            <ta e="T230" id="Seg_6787" s="T229">v:pred</ta>
            <ta e="T231" id="Seg_6788" s="T230">np:O</ta>
            <ta e="T233" id="Seg_6789" s="T232">0.2.h:S v:pred</ta>
            <ta e="T234" id="Seg_6790" s="T233">np:O</ta>
            <ta e="T235" id="Seg_6791" s="T234">0.1.h:S v:pred</ta>
            <ta e="T236" id="Seg_6792" s="T235">s:purp</ta>
            <ta e="T238" id="Seg_6793" s="T237">0.3.h:S 0.3:O v:pred</ta>
            <ta e="T240" id="Seg_6794" s="T239">np.h:S</ta>
            <ta e="T242" id="Seg_6795" s="T241">v:pred</ta>
            <ta e="T243" id="Seg_6796" s="T242">np:O</ta>
            <ta e="T246" id="Seg_6797" s="T245">0.3.h:S v:pred</ta>
            <ta e="T250" id="Seg_6798" s="T249">np:O</ta>
            <ta e="T251" id="Seg_6799" s="T250">0.3.h:S v:pred</ta>
            <ta e="T253" id="Seg_6800" s="T252">0.3.h:S v:pred</ta>
            <ta e="T255" id="Seg_6801" s="T254">v:pred</ta>
            <ta e="T259" id="Seg_6802" s="T258">np:S</ta>
            <ta e="T261" id="Seg_6803" s="T260">0.3.h:S v:pred</ta>
            <ta e="T265" id="Seg_6804" s="T264">0.3.h:S v:pred</ta>
            <ta e="T268" id="Seg_6805" s="T267">np:O</ta>
            <ta e="T270" id="Seg_6806" s="T269">0.3.h:S v:pred</ta>
            <ta e="T274" id="Seg_6807" s="T273">0.3.h:S v:pred</ta>
            <ta e="T276" id="Seg_6808" s="T275">s:adv</ta>
            <ta e="T278" id="Seg_6809" s="T277">0.3.h:S v:pred</ta>
            <ta e="T280" id="Seg_6810" s="T279">s:adv</ta>
            <ta e="T281" id="Seg_6811" s="T280">0.3.h:S v:pred</ta>
            <ta e="T283" id="Seg_6812" s="T282">0.1.h:S v:pred</ta>
            <ta e="T284" id="Seg_6813" s="T283">s:adv</ta>
            <ta e="T289" id="Seg_6814" s="T288">np:O</ta>
            <ta e="T290" id="Seg_6815" s="T289">0.2.h:S v:pred</ta>
            <ta e="T292" id="Seg_6816" s="T291">s:adv</ta>
            <ta e="T293" id="Seg_6817" s="T292">0.2.h:S v:pred</ta>
            <ta e="T296" id="Seg_6818" s="T295">cop</ta>
            <ta e="T298" id="Seg_6819" s="T297">np.h:S</ta>
            <ta e="T300" id="Seg_6820" s="T299">s:adv</ta>
            <ta e="T301" id="Seg_6821" s="T300">0.1.h:S v:pred</ta>
            <ta e="T304" id="Seg_6822" s="T303">0.3.h:S v:pred</ta>
            <ta e="T306" id="Seg_6823" s="T305">np:O</ta>
            <ta e="T309" id="Seg_6824" s="T308">s:adv</ta>
            <ta e="T310" id="Seg_6825" s="T309">0.3.h:S v:pred</ta>
            <ta e="T314" id="Seg_6826" s="T313">0.3.h:S v:pred</ta>
            <ta e="T318" id="Seg_6827" s="T317">np.h:S</ta>
            <ta e="T324" id="Seg_6828" s="T323">v:pred</ta>
            <ta e="T333" id="Seg_6829" s="T332">0.3.h:S v:pred</ta>
            <ta e="T335" id="Seg_6830" s="T334">np.h:S</ta>
            <ta e="T339" id="Seg_6831" s="T338">v:pred</ta>
            <ta e="T342" id="Seg_6832" s="T341">0.3.h:S v:pred</ta>
            <ta e="T343" id="Seg_6833" s="T342">np:O</ta>
            <ta e="T345" id="Seg_6834" s="T344">0.1.h:S v:pred</ta>
            <ta e="T346" id="Seg_6835" s="T345">np:S</ta>
            <ta e="T347" id="Seg_6836" s="T346">cop</ta>
            <ta e="T348" id="Seg_6837" s="T347">np:O</ta>
            <ta e="T350" id="Seg_6838" s="T349">0.2.h:S v:pred</ta>
            <ta e="T351" id="Seg_6839" s="T350">s:purp</ta>
            <ta e="T352" id="Seg_6840" s="T351">np:O</ta>
            <ta e="T353" id="Seg_6841" s="T352">0.1.h:S v:pred</ta>
            <ta e="T354" id="Seg_6842" s="T353">np:O</ta>
            <ta e="T356" id="Seg_6843" s="T355">v:pred</ta>
            <ta e="T360" id="Seg_6844" s="T359">np.h:S</ta>
            <ta e="T364" id="Seg_6845" s="T363">np.h:S</ta>
            <ta e="T367" id="Seg_6846" s="T366">v:pred</ta>
            <ta e="T369" id="Seg_6847" s="T368">np.h:S</ta>
            <ta e="T372" id="Seg_6848" s="T371">v:pred</ta>
            <ta e="T373" id="Seg_6849" s="T372">pro.h:S</ta>
            <ta e="T376" id="Seg_6850" s="T375">v:pred</ta>
            <ta e="T378" id="Seg_6851" s="T377">np.h:S</ta>
            <ta e="T380" id="Seg_6852" s="T379">np:O</ta>
            <ta e="T382" id="Seg_6853" s="T381">v:pred</ta>
            <ta e="T384" id="Seg_6854" s="T383">np:O</ta>
            <ta e="T385" id="Seg_6855" s="T384">0.3.h:S v:pred</ta>
            <ta e="T389" id="Seg_6856" s="T388">0.3.h:S v:pred</ta>
            <ta e="T393" id="Seg_6857" s="T392">np.h:O</ta>
            <ta e="T397" id="Seg_6858" s="T396">np.h:O</ta>
            <ta e="T399" id="Seg_6859" s="T398">0.3.h:S v:pred</ta>
            <ta e="T402" id="Seg_6860" s="T401">np:O</ta>
            <ta e="T403" id="Seg_6861" s="T402">0.3.h:S v:pred</ta>
            <ta e="T405" id="Seg_6862" s="T404">np.h:S</ta>
            <ta e="T409" id="Seg_6863" s="T408">np.h:S</ta>
            <ta e="T412" id="Seg_6864" s="T411">v:pred</ta>
            <ta e="T413" id="Seg_6865" s="T412">s:adv</ta>
            <ta e="T414" id="Seg_6866" s="T413">0.3.h:S v:pred</ta>
            <ta e="T415" id="Seg_6867" s="T414">np:O</ta>
            <ta e="T416" id="Seg_6868" s="T415">s:adv</ta>
            <ta e="T417" id="Seg_6869" s="T416">0.3.h:S v:pred</ta>
            <ta e="T420" id="Seg_6870" s="T419">np.h:S</ta>
            <ta e="T422" id="Seg_6871" s="T421">v:pred</ta>
            <ta e="T424" id="Seg_6872" s="T423">0.3.h:S v:pred</ta>
            <ta e="T426" id="Seg_6873" s="T425">0.3.h:S v:pred</ta>
            <ta e="T427" id="Seg_6874" s="T426">s:adv</ta>
            <ta e="T428" id="Seg_6875" s="T427">0.3.h:S v:pred</ta>
            <ta e="T430" id="Seg_6876" s="T429">s:adv</ta>
            <ta e="T432" id="Seg_6877" s="T431">0.3.h:S v:pred</ta>
            <ta e="T433" id="Seg_6878" s="T432">np:O</ta>
            <ta e="T434" id="Seg_6879" s="T433">0.3.h:S v:pred</ta>
            <ta e="T436" id="Seg_6880" s="T435">v:pred</ta>
            <ta e="T438" id="Seg_6881" s="T437">np.h:S</ta>
            <ta e="T441" id="Seg_6882" s="T440">0.3.h:S v:pred</ta>
            <ta e="T445" id="Seg_6883" s="T444">0.3.h:S v:pred</ta>
            <ta e="T446" id="Seg_6884" s="T445">np:O</ta>
            <ta e="T448" id="Seg_6885" s="T447">0.3.h:S v:pred</ta>
            <ta e="T449" id="Seg_6886" s="T448">np.h:S</ta>
            <ta e="T451" id="Seg_6887" s="T450">np:O</ta>
            <ta e="T452" id="Seg_6888" s="T451">v:pred</ta>
            <ta e="T453" id="Seg_6889" s="T452">np.h:S</ta>
            <ta e="T457" id="Seg_6890" s="T456">v:pred</ta>
            <ta e="T458" id="Seg_6891" s="T457">0.3.h:S v:pred</ta>
            <ta e="T460" id="Seg_6892" s="T459">0.3.h:S v:pred</ta>
            <ta e="T461" id="Seg_6893" s="T460">pro.h:S</ta>
            <ta e="T462" id="Seg_6894" s="T461">v:pred</ta>
            <ta e="T465" id="Seg_6895" s="T464">np.h:S</ta>
            <ta e="T467" id="Seg_6896" s="T466">np:O</ta>
            <ta e="T468" id="Seg_6897" s="T467">v:pred</ta>
            <ta e="T469" id="Seg_6898" s="T468">pro.h:S</ta>
            <ta e="T470" id="Seg_6899" s="T469">v:pred</ta>
            <ta e="T473" id="Seg_6900" s="T472">np.h:O</ta>
            <ta e="T475" id="Seg_6901" s="T474">0.3.h:S v:pred</ta>
            <ta e="T481" id="Seg_6902" s="T480">0.3.h:S v:pred</ta>
            <ta e="T484" id="Seg_6903" s="T483">np:S</ta>
            <ta e="T485" id="Seg_6904" s="T484">v:pred</ta>
            <ta e="T486" id="Seg_6905" s="T485">np.h:S</ta>
            <ta e="T488" id="Seg_6906" s="T487">np.h:S</ta>
            <ta e="T489" id="Seg_6907" s="T488">np.h:S</ta>
            <ta e="T491" id="Seg_6908" s="T490">v:pred</ta>
            <ta e="T492" id="Seg_6909" s="T491">np.h:S</ta>
            <ta e="T493" id="Seg_6910" s="T492">v:pred</ta>
            <ta e="T496" id="Seg_6911" s="T495">0.3.h:S v:pred</ta>
            <ta e="T504" id="Seg_6912" s="T503">0.3.h:S v:pred</ta>
            <ta e="T505" id="Seg_6913" s="T504">pro.h:S</ta>
            <ta e="T507" id="Seg_6914" s="T506">np.h:O</ta>
            <ta e="T508" id="Seg_6915" s="T507">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T2" id="Seg_6916" s="T1">new</ta>
            <ta e="T4" id="Seg_6917" s="T3">new</ta>
            <ta e="T8" id="Seg_6918" s="T7">new</ta>
            <ta e="T14" id="Seg_6919" s="T13">accs-sit</ta>
            <ta e="T16" id="Seg_6920" s="T15">accs-sit</ta>
            <ta e="T18" id="Seg_6921" s="T17">(accs-gen)</ta>
            <ta e="T23" id="Seg_6922" s="T22">accs-sit</ta>
            <ta e="T25" id="Seg_6923" s="T24">(accs-gen)</ta>
            <ta e="T29" id="Seg_6924" s="T28">accs-aggr</ta>
            <ta e="T34" id="Seg_6925" s="T33">giv-inactive</ta>
            <ta e="T42" id="Seg_6926" s="T41">0.giv-active</ta>
            <ta e="T45" id="Seg_6927" s="T44">accs-sit</ta>
            <ta e="T47" id="Seg_6928" s="T46">new</ta>
            <ta e="T50" id="Seg_6929" s="T49">0.giv-active</ta>
            <ta e="T51" id="Seg_6930" s="T50">0.giv-active</ta>
            <ta e="T52" id="Seg_6931" s="T51">giv-active</ta>
            <ta e="T56" id="Seg_6932" s="T55">accs-gen</ta>
            <ta e="T58" id="Seg_6933" s="T57">accs-gen</ta>
            <ta e="T61" id="Seg_6934" s="T60">0.accs-sit</ta>
            <ta e="T65" id="Seg_6935" s="T64">giv-inactive</ta>
            <ta e="T67" id="Seg_6936" s="T66">giv-inactive</ta>
            <ta e="T68" id="Seg_6937" s="T67">giv-inactive</ta>
            <ta e="T71" id="Seg_6938" s="T70">giv-active</ta>
            <ta e="T74" id="Seg_6939" s="T73">new</ta>
            <ta e="T75" id="Seg_6940" s="T74">0.giv-active</ta>
            <ta e="T78" id="Seg_6941" s="T77">new</ta>
            <ta e="T79" id="Seg_6942" s="T78">0.giv-active</ta>
            <ta e="T82" id="Seg_6943" s="T81">new</ta>
            <ta e="T86" id="Seg_6944" s="T85">0.giv-active</ta>
            <ta e="T90" id="Seg_6945" s="T89">0.giv-active</ta>
            <ta e="T91" id="Seg_6946" s="T90">new</ta>
            <ta e="T97" id="Seg_6947" s="T96">0.giv-active</ta>
            <ta e="T98" id="Seg_6948" s="T97">giv-inactive</ta>
            <ta e="T101" id="Seg_6949" s="T100">accs-gen</ta>
            <ta e="T106" id="Seg_6950" s="T105">accs-sit</ta>
            <ta e="T109" id="Seg_6951" s="T108">new/giv-inactive</ta>
            <ta e="T113" id="Seg_6952" s="T112">0.giv-inactive</ta>
            <ta e="T117" id="Seg_6953" s="T116">giv-active</ta>
            <ta e="T118" id="Seg_6954" s="T117">giv-inactive</ta>
            <ta e="T122" id="Seg_6955" s="T121">accs-gen</ta>
            <ta e="T125" id="Seg_6956" s="T124">accs-gen</ta>
            <ta e="T126" id="Seg_6957" s="T125">accs-gen</ta>
            <ta e="T130" id="Seg_6958" s="T129">giv-active</ta>
            <ta e="T133" id="Seg_6959" s="T132">accs-gen</ta>
            <ta e="T138" id="Seg_6960" s="T137">giv-inactive</ta>
            <ta e="T140" id="Seg_6961" s="T139">0.giv-active</ta>
            <ta e="T143" id="Seg_6962" s="T142">giv-active</ta>
            <ta e="T145" id="Seg_6963" s="T144">giv-active</ta>
            <ta e="T147" id="Seg_6964" s="T146">0.giv-active/quot-sp</ta>
            <ta e="T151" id="Seg_6965" s="T150">giv-inactive-Q</ta>
            <ta e="T155" id="Seg_6966" s="T154">accs-aggr-Q</ta>
            <ta e="T157" id="Seg_6967" s="T156">accs-gen</ta>
            <ta e="T158" id="Seg_6968" s="T157">0.giv-active-Q</ta>
            <ta e="T159" id="Seg_6969" s="T158">giv-active-Q</ta>
            <ta e="T160" id="Seg_6970" s="T159">0.giv-active-Q</ta>
            <ta e="T161" id="Seg_6971" s="T160">accs-gen</ta>
            <ta e="T162" id="Seg_6972" s="T161">0.giv-active-Q</ta>
            <ta e="T163" id="Seg_6973" s="T162">giv-inactive-Q</ta>
            <ta e="T167" id="Seg_6974" s="T166">0.giv-active-Q</ta>
            <ta e="T169" id="Seg_6975" s="T168">0.giv-active</ta>
            <ta e="T170" id="Seg_6976" s="T169">giv-inactive</ta>
            <ta e="T171" id="Seg_6977" s="T170">0.giv-active</ta>
            <ta e="T172" id="Seg_6978" s="T171">0.giv-active</ta>
            <ta e="T173" id="Seg_6979" s="T172">accs-gen</ta>
            <ta e="T174" id="Seg_6980" s="T173">0.giv-active</ta>
            <ta e="T175" id="Seg_6981" s="T174">giv-active</ta>
            <ta e="T177" id="Seg_6982" s="T176">accs-sit</ta>
            <ta e="T178" id="Seg_6983" s="T177">accs-gen</ta>
            <ta e="T179" id="Seg_6984" s="T178">0.giv-active</ta>
            <ta e="T183" id="Seg_6985" s="T182">giv-inactive</ta>
            <ta e="T184" id="Seg_6986" s="T183">0.giv-active</ta>
            <ta e="T185" id="Seg_6987" s="T184">giv-active</ta>
            <ta e="T186" id="Seg_6988" s="T185">new-Q</ta>
            <ta e="T187" id="Seg_6989" s="T186">new-Q</ta>
            <ta e="T188" id="Seg_6990" s="T187">0.giv-active-Q</ta>
            <ta e="T190" id="Seg_6991" s="T189">0.giv-inactive-Q</ta>
            <ta e="T192" id="Seg_6992" s="T191">new-Q</ta>
            <ta e="T193" id="Seg_6993" s="T192">giv-inactive-Q</ta>
            <ta e="T194" id="Seg_6994" s="T193">0.giv-active-Q</ta>
            <ta e="T195" id="Seg_6995" s="T194">giv-inactive</ta>
            <ta e="T196" id="Seg_6996" s="T195">giv-inactive</ta>
            <ta e="T198" id="Seg_6997" s="T197">0.giv-active</ta>
            <ta e="T201" id="Seg_6998" s="T200">0.giv-active</ta>
            <ta e="T202" id="Seg_6999" s="T201">accs-gen</ta>
            <ta e="T208" id="Seg_7000" s="T207">new</ta>
            <ta e="T213" id="Seg_7001" s="T212">accs-sit</ta>
            <ta e="T215" id="Seg_7002" s="T214">giv-inactive</ta>
            <ta e="T217" id="Seg_7003" s="T216">0.giv-inactive/quot-sp</ta>
            <ta e="T219" id="Seg_7004" s="T218">new-Q</ta>
            <ta e="T221" id="Seg_7005" s="T220">0.giv-active-Q</ta>
            <ta e="T225" id="Seg_7006" s="T224">0.giv-active-Q</ta>
            <ta e="T228" id="Seg_7007" s="T227">0.giv-active-Q/quot-sp</ta>
            <ta e="T229" id="Seg_7008" s="T228">accs-gen-Q</ta>
            <ta e="T231" id="Seg_7009" s="T230">accs-sit-Q</ta>
            <ta e="T232" id="Seg_7010" s="T231">giv-active-Q</ta>
            <ta e="T233" id="Seg_7011" s="T232">0.giv-inactive-Q</ta>
            <ta e="T234" id="Seg_7012" s="T233">new-Q</ta>
            <ta e="T238" id="Seg_7013" s="T237">0.giv-active</ta>
            <ta e="T240" id="Seg_7014" s="T239">giv-inactive</ta>
            <ta e="T241" id="Seg_7015" s="T240">new</ta>
            <ta e="T243" id="Seg_7016" s="T242">new</ta>
            <ta e="T246" id="Seg_7017" s="T245">0.giv-active</ta>
            <ta e="T250" id="Seg_7018" s="T249">new</ta>
            <ta e="T251" id="Seg_7019" s="T250">0.giv-inactive</ta>
            <ta e="T259" id="Seg_7020" s="T258">giv-inactive</ta>
            <ta e="T260" id="Seg_7021" s="T259">giv-inactive</ta>
            <ta e="T261" id="Seg_7022" s="T260">0.giv-inactive</ta>
            <ta e="T265" id="Seg_7023" s="T264">0.giv-active</ta>
            <ta e="T268" id="Seg_7024" s="T267">accs-sit</ta>
            <ta e="T270" id="Seg_7025" s="T269">0.giv-inactive</ta>
            <ta e="T271" id="Seg_7026" s="T270">accs-gen</ta>
            <ta e="T274" id="Seg_7027" s="T273">0.giv-active</ta>
            <ta e="T278" id="Seg_7028" s="T277">0.giv-inactive/quot-sp</ta>
            <ta e="T281" id="Seg_7029" s="T280">0.giv-inactive-Q</ta>
            <ta e="T283" id="Seg_7030" s="T282">0.accs-aggr-Q</ta>
            <ta e="T290" id="Seg_7031" s="T289">0.giv-active-Q</ta>
            <ta e="T293" id="Seg_7032" s="T292">0.giv-active-Q</ta>
            <ta e="T298" id="Seg_7033" s="T297">giv-inactive</ta>
            <ta e="T301" id="Seg_7034" s="T300">0.giv-inactive-Q</ta>
            <ta e="T304" id="Seg_7035" s="T303">0.giv-active</ta>
            <ta e="T306" id="Seg_7036" s="T305">giv-inactive</ta>
            <ta e="T308" id="Seg_7037" s="T307">giv-inactive</ta>
            <ta e="T310" id="Seg_7038" s="T309">0.giv-active</ta>
            <ta e="T311" id="Seg_7039" s="T310">giv-inactive</ta>
            <ta e="T314" id="Seg_7040" s="T313">0.giv-active</ta>
            <ta e="T318" id="Seg_7041" s="T317">accs-sit</ta>
            <ta e="T322" id="Seg_7042" s="T321">accs-sit</ta>
            <ta e="T331" id="Seg_7043" s="T330">accs-sit</ta>
            <ta e="T333" id="Seg_7044" s="T332">0.giv-active</ta>
            <ta e="T335" id="Seg_7045" s="T334">giv-active</ta>
            <ta e="T338" id="Seg_7046" s="T337">accs-sit</ta>
            <ta e="T342" id="Seg_7047" s="T341">0.giv-active/quot-sp</ta>
            <ta e="T343" id="Seg_7048" s="T342">giv-inactive-Q</ta>
            <ta e="T345" id="Seg_7049" s="T344">0.giv-active-Q</ta>
            <ta e="T346" id="Seg_7050" s="T345">accs-gen-Q</ta>
            <ta e="T348" id="Seg_7051" s="T347">giv-inactive-Q</ta>
            <ta e="T349" id="Seg_7052" s="T348">giv-active-Q</ta>
            <ta e="T350" id="Seg_7053" s="T349">0.giv-active-Q</ta>
            <ta e="T352" id="Seg_7054" s="T351">accs-gen-Q</ta>
            <ta e="T353" id="Seg_7055" s="T352">0.giv-active-Q</ta>
            <ta e="T354" id="Seg_7056" s="T353">giv-inactive</ta>
            <ta e="T360" id="Seg_7057" s="T359">giv-active</ta>
            <ta e="T364" id="Seg_7058" s="T363">giv-inactive</ta>
            <ta e="T369" id="Seg_7059" s="T368">giv-active</ta>
            <ta e="T371" id="Seg_7060" s="T370">giv-inactive</ta>
            <ta e="T373" id="Seg_7061" s="T372">giv-inactive</ta>
            <ta e="T378" id="Seg_7062" s="T377">giv-inactive</ta>
            <ta e="T380" id="Seg_7063" s="T379">giv-inactive</ta>
            <ta e="T384" id="Seg_7064" s="T383">giv-inactive</ta>
            <ta e="T386" id="Seg_7065" s="T385">giv-inactive</ta>
            <ta e="T389" id="Seg_7066" s="T388">0.giv-inactive</ta>
            <ta e="T393" id="Seg_7067" s="T392">giv-inactive</ta>
            <ta e="T397" id="Seg_7068" s="T396">giv-inactive</ta>
            <ta e="T402" id="Seg_7069" s="T401">accs-gen</ta>
            <ta e="T403" id="Seg_7070" s="T402">0.accs-gen</ta>
            <ta e="T405" id="Seg_7071" s="T404">giv-active</ta>
            <ta e="T409" id="Seg_7072" s="T408">giv-active</ta>
            <ta e="T414" id="Seg_7073" s="T413">0.giv-inactive</ta>
            <ta e="T415" id="Seg_7074" s="T414">giv-inactive</ta>
            <ta e="T420" id="Seg_7075" s="T419">giv-inactive</ta>
            <ta e="T421" id="Seg_7076" s="T420">giv-inactive</ta>
            <ta e="T424" id="Seg_7077" s="T423">0.giv-inactive</ta>
            <ta e="T426" id="Seg_7078" s="T425">0.giv-inactive</ta>
            <ta e="T428" id="Seg_7079" s="T427">0.giv-active</ta>
            <ta e="T432" id="Seg_7080" s="T431">0.giv-active</ta>
            <ta e="T433" id="Seg_7081" s="T432">accs-inf</ta>
            <ta e="T434" id="Seg_7082" s="T433">0.giv-active</ta>
            <ta e="T438" id="Seg_7083" s="T437">giv-active</ta>
            <ta e="T441" id="Seg_7084" s="T440">0.giv-active</ta>
            <ta e="T443" id="Seg_7085" s="T442">accs-inf</ta>
            <ta e="T445" id="Seg_7086" s="T444">0.giv-active</ta>
            <ta e="T446" id="Seg_7087" s="T445">accs-sit</ta>
            <ta e="T448" id="Seg_7088" s="T447">0.giv-active</ta>
            <ta e="T449" id="Seg_7089" s="T448">giv-inactive</ta>
            <ta e="T451" id="Seg_7090" s="T450">giv-inactive</ta>
            <ta e="T453" id="Seg_7091" s="T452">giv-inactive</ta>
            <ta e="T454" id="Seg_7092" s="T453">accs-gen</ta>
            <ta e="T458" id="Seg_7093" s="T457">0.giv-active.0</ta>
            <ta e="T460" id="Seg_7094" s="T459">0.giv-active.0</ta>
            <ta e="T461" id="Seg_7095" s="T460">giv-active-Q</ta>
            <ta e="T465" id="Seg_7096" s="T464">giv-inactive-Q</ta>
            <ta e="T468" id="Seg_7097" s="T467">0.giv-active-Q</ta>
            <ta e="T469" id="Seg_7098" s="T468">giv-active-Q</ta>
            <ta e="T473" id="Seg_7099" s="T472">giv-inactive</ta>
            <ta e="T475" id="Seg_7100" s="T474">0.giv-inactive</ta>
            <ta e="T481" id="Seg_7101" s="T480">0.giv-active</ta>
            <ta e="T488" id="Seg_7102" s="T487">giv-inactive</ta>
            <ta e="T489" id="Seg_7103" s="T488">new</ta>
            <ta e="T491" id="Seg_7104" s="T490">quot-th</ta>
            <ta e="T492" id="Seg_7105" s="T491">giv-inactive-Q</ta>
            <ta e="T500" id="Seg_7106" s="T499">accs-gen</ta>
            <ta e="T504" id="Seg_7107" s="T503">0.giv-active</ta>
            <ta e="T505" id="Seg_7108" s="T504">giv-active-Q</ta>
            <ta e="T507" id="Seg_7109" s="T506">accs-sit-Q</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T123" id="Seg_7110" s="T122">RUS:cult</ta>
            <ta e="T164" id="Seg_7111" s="T163">RUS:cult</ta>
            <ta e="T186" id="Seg_7112" s="T185">RUS:cult</ta>
            <ta e="T187" id="Seg_7113" s="T186">RUS:cult</ta>
            <ta e="T195" id="Seg_7114" s="T194">RUS:cult</ta>
            <ta e="T196" id="Seg_7115" s="T195">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T1" id="Seg_7116" s="T0">Пална.</ta>
            <ta e="T5" id="Seg_7117" s="T1">Пална с тремя братьями жил.</ta>
            <ta e="T9" id="Seg_7118" s="T5">Старик был богатырем-селькупом.</ta>
            <ta e="T14" id="Seg_7119" s="T9">Самый старший из братьев – сильный старик.</ta>
            <ta e="T20" id="Seg_7120" s="T14">Средний брат гонял летом оленей.</ta>
            <ta e="T27" id="Seg_7121" s="T20">Самый младший брат бегал по багульнику.</ta>
            <ta e="T29" id="Seg_7122" s="T27">Так [и] жили.</ta>
            <ta e="T37" id="Seg_7123" s="T29">А этот старший брат Палны такой колдун был.</ta>
            <ta e="T42" id="Seg_7124" s="T37">Откуда что придёт, всё знал.</ta>
            <ta e="T46" id="Seg_7125" s="T42">Он был поющим стариком-шаманом.</ta>
            <ta e="T50" id="Seg_7126" s="T46">Он сам делал железо, когда жил.</ta>
            <ta e="T56" id="Seg_7127" s="T50">Ковал железо, так делал, просто так из камней.</ta>
            <ta e="T64" id="Seg_7128" s="T56">Из чего русские поделывали, не знаю… </ta>
            <ta e="T69" id="Seg_7129" s="T64">Он делал, в железо камень превращал.</ta>
            <ta e="T75" id="Seg_7130" s="T69">Из этого камня железную одежду делал.</ta>
            <ta e="T83" id="Seg_7131" s="T75">Стальной лук делал, стальную стрелу делал.</ta>
            <ta e="T86" id="Seg_7132" s="T83">Этим и жил.</ta>
            <ta e="T90" id="Seg_7133" s="T86">А воевать не ходил.</ta>
            <ta e="T93" id="Seg_7134" s="T90">К нему воевать ходили.</ta>
            <ta e="T100" id="Seg_7135" s="T93">Откуда ни придут, он всё знал.</ta>
            <ta e="T103" id="Seg_7136" s="T100">Ненцы ходили воевать.</ta>
            <ta e="T110" id="Seg_7137" s="T103">В ненецкой стороне тоже бывали три брата.</ta>
            <ta e="T117" id="Seg_7138" s="T110">Опять сегодня приходили искать трёх братьев.</ta>
            <ta e="T126" id="Seg_7139" s="T117">Он [Пална] жил раньше на Енисее, в Туруханской безлесной стороне, в песках.</ta>
            <ta e="T135" id="Seg_7140" s="T126">Три брата-ненца однажды так [там] были.</ta>
            <ta e="T140" id="Seg_7141" s="T135">Едут искать трех братьев Палны.</ta>
            <ta e="T144" id="Seg_7142" s="T140">А братья Палны уезжают.</ta>
            <ta e="T147" id="Seg_7143" s="T144">Братьям так [Пална] говорит.</ta>
            <ta e="T155" id="Seg_7144" s="T147">Три брата-ненца собираются придти воевать к нам.</ta>
            <ta e="T158" id="Seg_7145" s="T155">Вниз на песок чум давайте поставим.</ta>
            <ta e="T160" id="Seg_7146" s="T158">На песок поставим чум.</ta>
            <ta e="T162" id="Seg_7147" s="T160">Еду унесем.</ta>
            <ta e="T166" id="Seg_7148" s="T162">Люди в гости придут.</ta>
            <ta e="T167" id="Seg_7149" s="T166">Кушать будут".</ta>
            <ta e="T170" id="Seg_7150" s="T167">Вниз идут, на песок.</ta>
            <ta e="T172" id="Seg_7151" s="T170">Кочуют, чум ставят.</ta>
            <ta e="T177" id="Seg_7152" s="T172">Сено стелют в чум на той стороне.</ta>
            <ta e="T179" id="Seg_7153" s="T177">Бересту стелют.</ta>
            <ta e="T184" id="Seg_7154" s="T179">Сверху вниз опять сено стелют.</ta>
            <ta e="T188" id="Seg_7155" s="T184">Братьям: "Кашу (овсяную) сварите.</ta>
            <ta e="T190" id="Seg_7156" s="T188">Сегодня придут.</ta>
            <ta e="T194" id="Seg_7157" s="T190">Жирную юколу в чум занесите".</ta>
            <ta e="T198" id="Seg_7158" s="T194">Кашу (овсяную) на огне варят.</ta>
            <ta e="T201" id="Seg_7159" s="T198">Некоторое время так сидят.</ta>
            <ta e="T206" id="Seg_7160" s="T201">Полдня прошло.</ta>
            <ta e="T209" id="Seg_7161" s="T206">Впереди ветка показалась.</ta>
            <ta e="T214" id="Seg_7162" s="T209">Ветка с тремя мужчинами показалась.</ta>
            <ta e="T217" id="Seg_7163" s="T214">Братьям [Пална] так говорит.</ta>
            <ta e="T221" id="Seg_7164" s="T217">"Ножи и отказы вниз спрячьте.</ta>
            <ta e="T225" id="Seg_7165" s="T221">В голенище пим засуньте.</ta>
            <ta e="T228" id="Seg_7166" s="T225">Потом так скажите.</ta>
            <ta e="T236" id="Seg_7167" s="T228">"Ножа [у нас] нет, дайте нам [ваш] нож, мы сделаем ложку, чтобы кушать".</ta>
            <ta e="T238" id="Seg_7168" s="T236">Так прячет.</ta>
            <ta e="T242" id="Seg_7169" s="T238">Младший брат живет в чуме напротив входа. </ta>
            <ta e="T246" id="Seg_7170" s="T242">Отказ там [острием] вверх втыкает.</ta>
            <ta e="T251" id="Seg_7171" s="T246">Сверху от очага палку от лабаза привязывают.</ta>
            <ta e="T259" id="Seg_7172" s="T251">Так видят: впереди показалась ветка с тремя мужчинами.</ta>
            <ta e="T261" id="Seg_7173" s="T259">С братьями [Пална] начинает говорить.</ta>
            <ta e="T265" id="Seg_7174" s="T261">На улицу только выходят.</ta>
            <ta e="T270" id="Seg_7175" s="T265">Видно, как стрелы от лука вверх натянули.</ta>
            <ta e="T274" id="Seg_7176" s="T270">Из воды на берег стреляют.</ta>
            <ta e="T278" id="Seg_7177" s="T274">Выйдя на улицу, [Пална] так говорит.</ta>
            <ta e="T287" id="Seg_7178" s="T278">"Выйдя на берег, поешьте, будем воевать лбами (?), выйдя наружу из воды на берег.</ta>
            <ta e="T290" id="Seg_7179" s="T287">Что собираетесь делать? </ta>
            <ta e="T293" id="Seg_7180" s="T290">Выйдя на берег, поешьте".</ta>
            <ta e="T298" id="Seg_7181" s="T293">Старший брат так [сказал].</ta>
            <ta e="T301" id="Seg_7182" s="T298">"Выйдя на берег, поедим."</ta>
            <ta e="T304" id="Seg_7183" s="T301">На берегу собираются.</ta>
            <ta e="T310" id="Seg_7184" s="T304">Луки и стрелы внизу на ветку кладут и оставляют.</ta>
            <ta e="T314" id="Seg_7185" s="T310">В чум заходят.</ta>
            <ta e="T324" id="Seg_7186" s="T314">Большой ненец-богатырь и Пална с одной стороны садятся.</ta>
            <ta e="T333" id="Seg_7187" s="T324">Также с братом, "догоняющим оленя с низовья (?)", с одной стороны садятся.</ta>
            <ta e="T339" id="Seg_7188" s="T333">Три брата все с одной стороны хотят сесть.</ta>
            <ta e="T345" id="Seg_7189" s="T339">Потом так сказали: "Мы [наш] нож что ли забыли.</ta>
            <ta e="T347" id="Seg_7190" s="T345">[У вас] нож есть? </ta>
            <ta e="T350" id="Seg_7191" s="T347">Дайте нам нож.</ta>
            <ta e="T353" id="Seg_7192" s="T350">Сделаем ложку, чтобы кушать".</ta>
            <ta e="T360" id="Seg_7193" s="T353">Все [свои] ножи отдают три брата-ненца.</ta>
            <ta e="T367" id="Seg_7194" s="T360">А три брата Палны раньше друг с другом разговаривали.</ta>
            <ta e="T372" id="Seg_7195" s="T367">Старший брат как ножом ударит! </ta>
            <ta e="T376" id="Seg_7196" s="T372">Они тоже так ударяют.</ta>
            <ta e="T385" id="Seg_7197" s="T376">Старший брат некоторое время режет, глазами парня гложет (?).</ta>
            <ta e="T393" id="Seg_7198" s="T385">Ножами ударяют трех братьев-ненцев.</ta>
            <ta e="T399" id="Seg_7199" s="T393">"Летнего оленя догоняющего" брата [кто-то] едва ударил.</ta>
            <ta e="T403" id="Seg_7200" s="T399">‎‎Только в пустое место попал.</ta>
            <ta e="T412" id="Seg_7201" s="T403">Брат Палны, "летнего оленя догонящий" брат потом тоже бежит.</ta>
            <ta e="T417" id="Seg_7202" s="T412">Догоняя, уводит, отказ схватив, догоняет.</ta>
            <ta e="T422" id="Seg_7203" s="T417">Те два брата дома остаются.</ta>
            <ta e="T426" id="Seg_7204" s="T422">То он уходит, то они ударяют.</ta>
            <ta e="T428" id="Seg_7205" s="T426">Догнав, уводит.</ta>
            <ta e="T432" id="Seg_7206" s="T428">Впереди догнав, рубит.</ta>
            <ta e="T434" id="Seg_7207" s="T432">Пятки отрубает.</ta>
            <ta e="T438" id="Seg_7208" s="T434">Ничком падает ненец.</ta>
            <ta e="T445" id="Seg_7209" s="T438">Во второй раз только ударил, [но] [тот] за середину отказа спокойно (?) поймал.</ta>
            <ta e="T448" id="Seg_7210" s="T445">Ногу сломал.</ta>
            <ta e="T452" id="Seg_7211" s="T448">Пална тех двух братьев убил.</ta>
            <ta e="T457" id="Seg_7212" s="T452">[Его] брат по дороге вперед бежит.</ta>
            <ta e="T460" id="Seg_7213" s="T457">Нашел, мол, поймал.</ta>
            <ta e="T462" id="Seg_7214" s="T460">"Я приду.</ta>
            <ta e="T468" id="Seg_7215" s="T462">Этот [abusive expression], что он опять делает?</ta>
            <ta e="T470" id="Seg_7216" s="T468">Я скажу".</ta>
            <ta e="T475" id="Seg_7217" s="T470">Потом трех братьев всех убили.</ta>
            <ta e="T485" id="Seg_7218" s="T475">С тех пор некоторое время жили, никакого шума не было.</ta>
            <ta e="T488" id="Seg_7219" s="T485">Уезжают ненцы.</ta>
            <ta e="T491" id="Seg_7220" s="T488">Жена так думает.</ta>
            <ta e="T496" id="Seg_7221" s="T491">"Мои дети ушли, всё чум что ли ставят".</ta>
            <ta e="T504" id="Seg_7222" s="T496">Про селькупского ребенка всегда будто так хорошо говорила.</ta>
            <ta e="T508" id="Seg_7223" s="T504">"Я [моего] ребенка вырастила.</ta>
            <ta e="T512" id="Seg_7224" s="T508">Грудь я тоже мою" (?).</ta>
            <ta e="T518" id="Seg_7225" s="T512">Селькупская женщина всегда смотрит (?).</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1" id="Seg_7226" s="T0">Palna.</ta>
            <ta e="T5" id="Seg_7227" s="T1">Palna lived with his three brothers.</ta>
            <ta e="T9" id="Seg_7228" s="T5">The old one was a selkup strongman.</ta>
            <ta e="T14" id="Seg_7229" s="T9">The eldest brother is a strong man.</ta>
            <ta e="T20" id="Seg_7230" s="T14">The middle brother catches reindeer in the summer.</ta>
            <ta e="T27" id="Seg_7231" s="T20">The youngest brother rushes on the tops of wild rosemary.</ta>
            <ta e="T29" id="Seg_7232" s="T27">They lived so.</ta>
            <ta e="T37" id="Seg_7233" s="T29">Palna's eldest brother had shamanic wisdom.</ta>
            <ta e="T42" id="Seg_7234" s="T37">He knew where everything would come from.</ta>
            <ta e="T46" id="Seg_7235" s="T42">He was a singing old shaman.</ta>
            <ta e="T50" id="Seg_7236" s="T46">He lived and forged iron himself.</ta>
            <ta e="T56" id="Seg_7237" s="T50">He forged iron just so, just out of stones. </ta>
            <ta e="T64" id="Seg_7238" s="T56">I don't know what Russians forged it out of…</ta>
            <ta e="T69" id="Seg_7239" s="T64">He forged iron out of stone.</ta>
            <ta e="T75" id="Seg_7240" s="T69">He made iron clothing out of that stone.</ta>
            <ta e="T83" id="Seg_7241" s="T75">He made iron bows and iron arrows.</ta>
            <ta e="T86" id="Seg_7242" s="T83">This made his living.</ta>
            <ta e="T90" id="Seg_7243" s="T86">He didn't go to war.</ta>
            <ta e="T93" id="Seg_7244" s="T90">They came to fight.</ta>
            <ta e="T100" id="Seg_7245" s="T93">He knew from where they would come.</ta>
            <ta e="T103" id="Seg_7246" s="T100">Nenets went to war.</ta>
            <ta e="T110" id="Seg_7247" s="T103">In the Nenets' land there lived also three brothers.</ta>
            <ta e="T117" id="Seg_7248" s="T110">That day they went to search for the three brothers. ?</ta>
            <ta e="T126" id="Seg_7249" s="T117">He [Palna] used to live on the Yenisei in the Turukhansk woodless region, in the sand.</ta>
            <ta e="T135" id="Seg_7250" s="T126">The three Nenets brothers were there once.</ta>
            <ta e="T140" id="Seg_7251" s="T135">They are searching for Palna's three brothers. </ta>
            <ta e="T144" id="Seg_7252" s="T140">But Palna's brothers are about to go away.</ta>
            <ta e="T147" id="Seg_7253" s="T144">Palna says to his brothers:</ta>
            <ta e="T155" id="Seg_7254" s="T147">"Three nenets brothers are going to attack us.</ta>
            <ta e="T158" id="Seg_7255" s="T155">Let us put our tent down on the sand.</ta>
            <ta e="T160" id="Seg_7256" s="T158">We'll put it on the sand.</ta>
            <ta e="T162" id="Seg_7257" s="T160">We'll put away the food.</ta>
            <ta e="T166" id="Seg_7258" s="T162">People come to us.</ta>
            <ta e="T167" id="Seg_7259" s="T166">They will eat".</ta>
            <ta e="T170" id="Seg_7260" s="T167">They go down to the sand.</ta>
            <ta e="T172" id="Seg_7261" s="T170">They bring their deer and the tent there.</ta>
            <ta e="T177" id="Seg_7262" s="T172">On that side they lay hay into the tent.</ta>
            <ta e="T179" id="Seg_7263" s="T177">They put birchbark on the tent.</ta>
            <ta e="T184" id="Seg_7264" s="T179">They put hay on it again, from the top to the bottom.</ta>
            <ta e="T188" id="Seg_7265" s="T184">He says to his brothers: "Cook porridge.</ta>
            <ta e="T190" id="Seg_7266" s="T188">They are coming today.</ta>
            <ta e="T194" id="Seg_7267" s="T190">Bring fatty dried fish into the tent".</ta>
            <ta e="T198" id="Seg_7268" s="T194">They cook porridge on the fire.</ta>
            <ta e="T201" id="Seg_7269" s="T198">They sat so for a while.</ta>
            <ta e="T206" id="Seg_7270" s="T201">Half of the day passed.</ta>
            <ta e="T209" id="Seg_7271" s="T206">A boat appeared in front.</ta>
            <ta e="T214" id="Seg_7272" s="T209">A boat with three brothers appeared. [Three brothers sat in the boat]</ta>
            <ta e="T217" id="Seg_7273" s="T214">He says to his brothers:</ta>
            <ta e="T221" id="Seg_7274" s="T217">"Hide the knives and the pikes.</ta>
            <ta e="T225" id="Seg_7275" s="T221">Put them into the bootlegs.</ta>
            <ta e="T228" id="Seg_7276" s="T225">Then say the following:</ta>
            <ta e="T236" id="Seg_7277" s="T228">"We don't have any knives, give us yours, we'll make spoons for you to eat with".</ta>
            <ta e="T238" id="Seg_7278" s="T236">He is hiding them so.</ta>
            <ta e="T242" id="Seg_7279" s="T238">The youngest brother lives on the opposite side of the tent entrance.</ta>
            <ta e="T246" id="Seg_7280" s="T242">He there sticks pikes with the points upwards.</ta>
            <ta e="T251" id="Seg_7281" s="T246">They bound a granary tree(?)[a beam] above the fire.</ta>
            <ta e="T259" id="Seg_7282" s="T251">They see a canoe with three brothers.</ta>
            <ta e="T261" id="Seg_7283" s="T259">Palna starts to talk to his brothers.</ta>
            <ta e="T265" id="Seg_7284" s="T261">They go outside.</ta>
            <ta e="T270" id="Seg_7285" s="T265">They see/saw them nocked their arrows.</ta>
            <ta e="T274" id="Seg_7286" s="T270">They are shooting from water to the shore.</ta>
            <ta e="T278" id="Seg_7287" s="T274">Palna went outside and said:</ta>
            <ta e="T287" id="Seg_7288" s="T278">"Come up on the shore, eat, we'll have a frontal fight.</ta>
            <ta e="T290" id="Seg_7289" s="T287">Well, what are your going to do?</ta>
            <ta e="T293" id="Seg_7290" s="T290">Come up on the shore, eat".</ta>
            <ta e="T298" id="Seg_7291" s="T293">- said the eldest brother.</ta>
            <ta e="T301" id="Seg_7292" s="T298">"We'll come up to the shore and eat".</ta>
            <ta e="T304" id="Seg_7293" s="T301">They came up to the shore.</ta>
            <ta e="T310" id="Seg_7294" s="T304">They left their bows and arrows down in the canoe.</ta>
            <ta e="T314" id="Seg_7295" s="T310">They entered the tent.</ta>
            <ta e="T324" id="Seg_7296" s="T314">The big Nenets strongman sat down with Palna.</ta>
            <ta e="T333" id="Seg_7297" s="T324">They sat down also on the other side with the brother who can catch a reindeer down the river.</ta>
            <ta e="T339" id="Seg_7298" s="T333">Three brothers sat down together.</ta>
            <ta e="T345" id="Seg_7299" s="T339">Then they said: "We left our knives somewhere.</ta>
            <ta e="T347" id="Seg_7300" s="T345">Do you have knives?</ta>
            <ta e="T350" id="Seg_7301" s="T347">Give them to us.</ta>
            <ta e="T353" id="Seg_7302" s="T350">We will make spoons to eat with out of them".</ta>
            <ta e="T360" id="Seg_7303" s="T353">Each brother gave up his knife.</ta>
            <ta e="T367" id="Seg_7304" s="T360">Palna's three brothers have discussed this earlier though.</ta>
            <ta e="T372" id="Seg_7305" s="T367">The eldest brother stabbed a Nenets with a knife.</ta>
            <ta e="T376" id="Seg_7306" s="T372">The others also stabbed like him.</ta>
            <ta e="T385" id="Seg_7307" s="T376">The eldest brother observed the middle and the youngest brother for a while.(?)</ta>
            <ta e="T393" id="Seg_7308" s="T385">They stabbed the three Nenets brothers with knives.</ta>
            <ta e="T399" id="Seg_7309" s="T393">The brother who catches a reindeer in summer almost got stabbed.</ta>
            <ta e="T403" id="Seg_7310" s="T399">But the attacke only stabbed into the earth.</ta>
            <ta e="T412" id="Seg_7311" s="T403">Palna's brother, the one who catches a reindeer in summer, also ran.</ta>
            <ta e="T417" id="Seg_7312" s="T412">He chases him, that one runs away, he seizes the pike and chases after him.</ta>
            <ta e="T422" id="Seg_7313" s="T417">The other two brothers stayed in the tent.</ta>
            <ta e="T426" id="Seg_7314" s="T422">Either he runs away, or they fight.</ta>
            <ta e="T428" id="Seg_7315" s="T426">He catches up with him, he runs away again.</ta>
            <ta e="T432" id="Seg_7316" s="T428">He runs forward, catches him and chops him.</ta>
            <ta e="T434" id="Seg_7317" s="T432">He chops his heels off.</ta>
            <ta e="T438" id="Seg_7318" s="T434">The Nenets fell on the ground facedown.</ta>
            <ta e="T445" id="Seg_7319" s="T438">He has hardly chopped him for the second time as the other one calmly caught his pike in the middle. (?)</ta>
            <ta e="T448" id="Seg_7320" s="T445">He broke his leg.</ta>
            <ta e="T452" id="Seg_7321" s="T448">Palna killed the other two Nenets brothers.</ta>
            <ta e="T457" id="Seg_7322" s="T452">His middle brother ran along the road.</ta>
            <ta e="T460" id="Seg_7323" s="T457">He told him that he had found and caught the other one.</ta>
            <ta e="T462" id="Seg_7324" s="T460">"I will go.</ta>
            <ta e="T468" id="Seg_7325" s="T462">This [abusive expression], what does he do again?</ta>
            <ta e="T470" id="Seg_7326" s="T468">I will say to him".</ta>
            <ta e="T475" id="Seg_7327" s="T470">Then they killed all the three brothers.</ta>
            <ta e="T485" id="Seg_7328" s="T475">Since then they lived for a while without any noise.</ta>
            <ta e="T488" id="Seg_7329" s="T485">The Nenets leave.</ta>
            <ta e="T491" id="Seg_7330" s="T488">The woman thinks:</ta>
            <ta e="T496" id="Seg_7331" s="T491">"My children went away from home forever". </ta>
            <ta e="T504" id="Seg_7332" s="T496">She always thought good of Selkup children.</ta>
            <ta e="T508" id="Seg_7333" s="T504">"I brought up my child.</ta>
            <ta e="T512" id="Seg_7334" s="T508">I am also washing my breast (?)."</ta>
            <ta e="T518" id="Seg_7335" s="T512">The Selkup woman always gives a look (?).</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1" id="Seg_7336" s="T0">Palna.</ta>
            <ta e="T5" id="Seg_7337" s="T1">Palna lebte mit seinen drei Brüdern.</ta>
            <ta e="T9" id="Seg_7338" s="T5">Der Alte war ein selkupischer Recke.</ta>
            <ta e="T14" id="Seg_7339" s="T9">Der älteste Bruder ist ein starker Mann. </ta>
            <ta e="T20" id="Seg_7340" s="T14">Der mittlere Bruder trieb im Sommer die Rentiere.</ta>
            <ta e="T27" id="Seg_7341" s="T20">Der jüngste Bruder raste durch die Spitzen von wildem Rosmarin.</ta>
            <ta e="T29" id="Seg_7342" s="T27">So lebten sie.</ta>
            <ta e="T37" id="Seg_7343" s="T29">Palnas ältester Bruder hatte schamanisches Wissen.</ta>
            <ta e="T42" id="Seg_7344" s="T37">Er wusste, woher was kommen würde.</ta>
            <ta e="T46" id="Seg_7345" s="T42">Er war ein singender alter Schamane.</ta>
            <ta e="T50" id="Seg_7346" s="T46">Er lebte und schmiedete selbst Eisen. </ta>
            <ta e="T56" id="Seg_7347" s="T50">Er schmiedete das Eisen einfach so, aus Steinen.</ta>
            <ta e="T64" id="Seg_7348" s="T56">Woraus die Russen es schmiedeten, weiß ich aber nicht…</ta>
            <ta e="T69" id="Seg_7349" s="T64">Er machte es, er machte Steine zu Eisen.</ta>
            <ta e="T75" id="Seg_7350" s="T69">Aus diesem Stein machte er eiserne Kleidung.</ta>
            <ta e="T83" id="Seg_7351" s="T75">Er machte einen eisernen Bogen, er machte einen eisernen Pfeil.</ta>
            <ta e="T86" id="Seg_7352" s="T83">Davon lebte er auch.</ta>
            <ta e="T90" id="Seg_7353" s="T86">Aber in den Krieg zog er nicht.</ta>
            <ta e="T93" id="Seg_7354" s="T90">Es kamen [welche] zu ihm, um Krieg zu führen. </ta>
            <ta e="T100" id="Seg_7355" s="T93">Er wusste aber, woher sie kommen würden.</ta>
            <ta e="T103" id="Seg_7356" s="T100">Nenzen gingen, um zu kämpfen.</ta>
            <ta e="T110" id="Seg_7357" s="T103">Auf der nenzischen Seite gab es auch drei Brüder.</ta>
            <ta e="T117" id="Seg_7358" s="T110">An dem Tag gingen sie die drei Brüder suchen.</ta>
            <ta e="T126" id="Seg_7359" s="T117">Er [Palna] lebte früher am Jenissei, auf der unbewaldeten Turuchansker Seite, im Sand.</ta>
            <ta e="T135" id="Seg_7360" s="T126">Die drei nenzischen Brüder waren einmal dort.</ta>
            <ta e="T140" id="Seg_7361" s="T135">Sie gehen, um Palnas Brüder zu suchen.</ta>
            <ta e="T144" id="Seg_7362" s="T140">Palnas Brüder wollen gerade losgehen.</ta>
            <ta e="T147" id="Seg_7363" s="T144">Da sagt Palna zu seinen Brüdern:</ta>
            <ta e="T155" id="Seg_7364" s="T147">"Die drei nenzischen Brüder sind unterwegs zu uns, um uns anzugreifen.</ta>
            <ta e="T158" id="Seg_7365" s="T155">Lasst uns das Zelt unten auf dem Sand aufstellen.</ta>
            <ta e="T160" id="Seg_7366" s="T158">Wir werden es auf den Sand stellen.</ta>
            <ta e="T162" id="Seg_7367" s="T160">Das Essen werden wir wegbringen.</ta>
            <ta e="T166" id="Seg_7368" s="T162">Die Leute werden zu uns kommen.</ta>
            <ta e="T167" id="Seg_7369" s="T166">Sie werden essen."</ta>
            <ta e="T170" id="Seg_7370" s="T167">Sie gehen hinunter, auf den Sand.</ta>
            <ta e="T172" id="Seg_7371" s="T170">Sie ziehen [hinunter], sie stellen das Zelt auf.</ta>
            <ta e="T177" id="Seg_7372" s="T172">Sie legen Heu ins Zelt auf jene Seite.</ta>
            <ta e="T179" id="Seg_7373" s="T177">Sie legen Birkenrinde aus.</ta>
            <ta e="T184" id="Seg_7374" s="T179">Von oben bis unten legen sie wieder Heu hin.</ta>
            <ta e="T188" id="Seg_7375" s="T184">Zu seinen Brüdern: "Kocht Haferbrei.</ta>
            <ta e="T190" id="Seg_7376" s="T188">Sie kommen heute.</ta>
            <ta e="T194" id="Seg_7377" s="T190">Bringt den fettigen getrockneten Fisch ins Zelt."</ta>
            <ta e="T198" id="Seg_7378" s="T194">Sie kochen Haferbrei auf dem Feuer.</ta>
            <ta e="T201" id="Seg_7379" s="T198">So sitzen sie eine Weile.</ta>
            <ta e="T206" id="Seg_7380" s="T201">Die Hälfte des Tages verging.</ta>
            <ta e="T209" id="Seg_7381" s="T206">Vorne tauchte ein Boot auf.</ta>
            <ta e="T214" id="Seg_7382" s="T209">Ein Boot mit drei Brüdern tauchte auf.</ta>
            <ta e="T217" id="Seg_7383" s="T214">Er sagt zu seinen Brüdern:</ta>
            <ta e="T221" id="Seg_7384" s="T217">"Versteckt die Messer und die Lanzen.</ta>
            <ta e="T225" id="Seg_7385" s="T221">Steckt sie in die Stiefelschäfte.</ta>
            <ta e="T228" id="Seg_7386" s="T225">Dann sagt Folgendes:</ta>
            <ta e="T236" id="Seg_7387" s="T228">"Wie haben kein Messer, gebt uns euer Messer, daraus machen wir einen Löffel, um zu essen."</ta>
            <ta e="T238" id="Seg_7388" s="T236">So versteckt er [die Sachen].</ta>
            <ta e="T242" id="Seg_7389" s="T238">Der jüngste Bruder lebt gegenüber des Eingangs im Zelt.</ta>
            <ta e="T246" id="Seg_7390" s="T242">Seine Lanze steckt er [mit der Spitze] nach oben [in den Boden].</ta>
            <ta e="T251" id="Seg_7391" s="T246">Über dem Feuer hingen sie einen Balken vom Speicher auf.</ta>
            <ta e="T259" id="Seg_7392" s="T251">Da sehen sie: Vorne taucht das Boot mit drei Brüdern auf. </ta>
            <ta e="T261" id="Seg_7393" s="T259">Palna fängt an, mit seinen Brüdern zu reden. </ta>
            <ta e="T265" id="Seg_7394" s="T261">Sie gehen nach draußen.</ta>
            <ta e="T270" id="Seg_7395" s="T265">Sie sehen, dass sie [= die Nenzen] offenbar ihre Bögen und Pfeile erhoben haben.</ta>
            <ta e="T274" id="Seg_7396" s="T270">Aus dem Wasser schießen sie aufs Ufer.</ta>
            <ta e="T278" id="Seg_7397" s="T274">Palna geht nach draußen und sagt:</ta>
            <ta e="T287" id="Seg_7398" s="T278">"Kommt aufs Ufer, esst, auf dem Ufer werden wir mit der Stirn(?) kämpfen, nachdem [ihr] aus dem Wasser gekommen seid.</ta>
            <ta e="T290" id="Seg_7399" s="T287">Also, was werdet ihr tun?</ta>
            <ta e="T293" id="Seg_7400" s="T290">Kommt hoch, esst."</ta>
            <ta e="T298" id="Seg_7401" s="T293">Und so war der ältere Bruder.</ta>
            <ta e="T301" id="Seg_7402" s="T298">"Wir kommen ans Ufer und essen."</ta>
            <ta e="T304" id="Seg_7403" s="T301">Sie sammeln sich auf dem Ufer.</ta>
            <ta e="T310" id="Seg_7404" s="T304">Ihre Bögen und Pfeile legen sie unten ins Boot und lassen sie [dort]. </ta>
            <ta e="T314" id="Seg_7405" s="T310">Sie betreten das Zelt.</ta>
            <ta e="T324" id="Seg_7406" s="T314">Eine großer nenzische Recke und Palna sitzen nebeneinander.</ta>
            <ta e="T333" id="Seg_7407" s="T324">Auch neben den mittleren Bruder, der flussabwärts Rentiere treibt, setzen sie sich auf einer Seite.</ta>
            <ta e="T339" id="Seg_7408" s="T333">Die drei Brüder möchten sich zusammen setzen.</ta>
            <ta e="T345" id="Seg_7409" s="T339">Dann sagten sie: "Wir haben unser Messer vergessen.</ta>
            <ta e="T347" id="Seg_7410" s="T345">Habt ihr ein Messer?</ta>
            <ta e="T350" id="Seg_7411" s="T347">Gebt uns ein Messer.</ta>
            <ta e="T353" id="Seg_7412" s="T350">Wir machen daraus einen Löffel, um zu essen."</ta>
            <ta e="T360" id="Seg_7413" s="T353">Die drei nenzischen Brüder geben alle ihre Messer.</ta>
            <ta e="T367" id="Seg_7414" s="T360">Palnas drei Brüder haben sich abgesprochen.</ta>
            <ta e="T372" id="Seg_7415" s="T367">Wie der älteste Bruder mit dem Messer zusticht!</ta>
            <ta e="T376" id="Seg_7416" s="T372">Die anderen stechen auch zu.</ta>
            <ta e="T385" id="Seg_7417" s="T376">Der älteste Bruder schneidet eine Weile, er knabbert mit seinen Augen das Kind (?). </ta>
            <ta e="T393" id="Seg_7418" s="T385">Sie stechen die drei nenzischen Brüder mit den Messern.</ta>
            <ta e="T399" id="Seg_7419" s="T393">Der Bruder, der Sommerrentiere treibt, wird beinahe gestochen.</ta>
            <ta e="T403" id="Seg_7420" s="T399">[Er] trifft nur den blanken Boden.</ta>
            <ta e="T412" id="Seg_7421" s="T403">Palnas Bruder, der die Rentiere im Sommer treibt, rennt ebenfalls.</ta>
            <ta e="T417" id="Seg_7422" s="T412">Er verfolgt ihn und treibt ihn fort, dann schnappt er sich die Lanze und verfolgt ihn.</ta>
            <ta e="T422" id="Seg_7423" s="T417">Die beiden anderen Brüder bleiben im Zelt.</ta>
            <ta e="T426" id="Seg_7424" s="T422">Entweder flieht er oder sie stechen zu.</ta>
            <ta e="T428" id="Seg_7425" s="T426">Er holt ihn ein, bringt ihn fort.</ta>
            <ta e="T432" id="Seg_7426" s="T428">Weiter vorne fängt er ihn und (zer)hackt ihn.</ta>
            <ta e="T434" id="Seg_7427" s="T432">Er hackt seine Fersen ab.</ta>
            <ta e="T438" id="Seg_7428" s="T434">Der Nenze fällt mit dem Gesicht auf den Boden.</ta>
            <ta e="T445" id="Seg_7429" s="T438">Kaum hat er ihn zum zweiten Mal getroffen, greifte der [andere] ruhig seine Lanze in der Mitte.</ta>
            <ta e="T448" id="Seg_7430" s="T445">Er brach sein Bein.</ta>
            <ta e="T452" id="Seg_7431" s="T448">Palna brachte die anderen beiden Brüder um.</ta>
            <ta e="T457" id="Seg_7432" s="T452">Er rennt weiter entlang des Weges seines Bruders.</ta>
            <ta e="T460" id="Seg_7433" s="T457">Er findet ihn, man sagt, er hat ihn gefangen.</ta>
            <ta e="T462" id="Seg_7434" s="T460">"Ich gehe.</ta>
            <ta e="T468" id="Seg_7435" s="T462">Dieses Stück Pisse, was macht er denn wieder?</ta>
            <ta e="T470" id="Seg_7436" s="T468">Werde ich sagen."</ta>
            <ta e="T475" id="Seg_7437" s="T470">Dann töteten sie alle drei Brüder.</ta>
            <ta e="T485" id="Seg_7438" s="T475">Seitdem lebten sie eine Weile ohne Lärm.</ta>
            <ta e="T488" id="Seg_7439" s="T485">Die Nenzen gehen weg.</ta>
            <ta e="T491" id="Seg_7440" s="T488">Die Frau denkt:</ta>
            <ta e="T496" id="Seg_7441" s="T491">"Meine Kinder sind weggegangen, sie ziehen immer mit dem Zelt umher (?)."</ta>
            <ta e="T504" id="Seg_7442" s="T496">Sie sagte immer nur Gutes über die selkupischen Kinder.</ta>
            <ta e="T508" id="Seg_7443" s="T504">"Ich zog mein Kind groß.</ta>
            <ta e="T512" id="Seg_7444" s="T508">Meine Brust wasche ich auch (?)."</ta>
            <ta e="T518" id="Seg_7445" s="T512">Die selkupische Frau schaut immer (?).</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T1" id="Seg_7446" s="T0">Пальна.</ta>
            <ta e="T5" id="Seg_7447" s="T1">П. трое братья жили</ta>
            <ta e="T9" id="Seg_7448" s="T5">богатырский старик был.</ta>
            <ta e="T14" id="Seg_7449" s="T9">сильный старик с старший брат.</ta>
            <ta e="T20" id="Seg_7450" s="T14">летнего оленя гоняющий [догоняющий] был</ta>
            <ta e="T27" id="Seg_7451" s="T20">самый младший брат багульник гоняющий был [по багульнику бегал]</ta>
            <ta e="T29" id="Seg_7452" s="T27">так жили.</ta>
            <ta e="T37" id="Seg_7453" s="T29">этот пальны старший брат такой был</ta>
            <ta e="T42" id="Seg_7454" s="T37">откуда что придёт, всё знал.</ta>
            <ta e="T46" id="Seg_7455" s="T42">песни поющий шаманящий старик был</ta>
            <ta e="T50" id="Seg_7456" s="T46">железо делал сам когда жил (раньше)</ta>
            <ta e="T56" id="Seg_7457" s="T50">куёт железо так делал только от камня.</ta>
            <ta e="T64" id="Seg_7458" s="T56">русские поделывали не знаю откуда </ta>
            <ta e="T69" id="Seg_7459" s="T64">он поделывал (из камня) в железо камень поделывал</ta>
            <ta e="T75" id="Seg_7460" s="T69">от этого камня железный парку делал</ta>
            <ta e="T83" id="Seg_7461" s="T75">стальной лук делал стальной стрелу делывал</ta>
            <ta e="T86" id="Seg_7462" s="T83">этим и жил</ta>
            <ta e="T90" id="Seg_7463" s="T86">а воевать не ходил</ta>
            <ta e="T93" id="Seg_7464" s="T90">к нему (с) войной ходили</ta>
            <ta e="T100" id="Seg_7465" s="T93">только что откуда ни придут он все знал</ta>
            <ta e="T103" id="Seg_7466" s="T100">ненцы ходили воевать</ta>
            <ta e="T110" id="Seg_7467" s="T103">с язёвые[ой] люди[ей] стороне трое братьев бывали [были].</ta>
            <ta e="T117" id="Seg_7468" s="T110">опять искать приходили сегодня 3х братьев</ta>
            <ta e="T126" id="Seg_7469" s="T117">он жил раньше на Енисее … в тундр. стороне песке</ta>
            <ta e="T135" id="Seg_7470" s="T126">3 брат однажды так стали</ta>
            <ta e="T140" id="Seg_7471" s="T135">3 бр. искать поезжают</ta>
            <ta e="T144" id="Seg_7472" s="T140">брат пошел</ta>
            <ta e="T147" id="Seg_7473" s="T144">к братьям так сказал</ta>
            <ta e="T155" id="Seg_7474" s="T147">3 бр. тут едут воевать к нам</ta>
            <ta e="T158" id="Seg_7475" s="T155">подальше на песок чум поставили</ta>
            <ta e="T160" id="Seg_7476" s="T158">песок поставим чум</ta>
            <ta e="T162" id="Seg_7477" s="T160">еду унесем</ta>
            <ta e="T166" id="Seg_7478" s="T162">люди (в гости) придут</ta>
            <ta e="T167" id="Seg_7479" s="T166">кушать будут</ta>
            <ta e="T170" id="Seg_7480" s="T167">подальше пошли, в пески</ta>
            <ta e="T172" id="Seg_7481" s="T170">пристанут к берегу и чум ставят</ta>
            <ta e="T177" id="Seg_7482" s="T172">сено настелили в чум на той стороне</ta>
            <ta e="T179" id="Seg_7483" s="T177">берестой постелили накрыли</ta>
            <ta e="T184" id="Seg_7484" s="T179">сверху опять сеном накрыли</ta>
            <ta e="T188" id="Seg_7485" s="T184">братьям (еда) сварите</ta>
            <ta e="T190" id="Seg_7486" s="T188">сегодня придут</ta>
            <ta e="T194" id="Seg_7487" s="T190">жирную юколу в чум занесите</ta>
            <ta e="T198" id="Seg_7488" s="T194">кашу поставили сварили</ta>
            <ta e="T201" id="Seg_7489" s="T198">так и “сидят”</ta>
            <ta e="T206" id="Seg_7490" s="T201">полдня повернулся</ta>
            <ta e="T209" id="Seg_7491" s="T206">сверху на ветке едет</ta>
            <ta e="T214" id="Seg_7492" s="T209">3-х чел. ветка показался</ta>
            <ta e="T217" id="Seg_7493" s="T214">брату так говорит</ta>
            <ta e="T221" id="Seg_7494" s="T217">ножики вниз спрятать</ta>
            <ta e="T225" id="Seg_7495" s="T221">в кисы запихайте</ta>
            <ta e="T228" id="Seg_7496" s="T225">потом так скажете</ta>
            <ta e="T236" id="Seg_7497" s="T228">ножика нет, ножик нам дайте, ложку сделаем кушать</ta>
            <ta e="T238" id="Seg_7498" s="T236">[чтобы] так говоть</ta>
            <ta e="T242" id="Seg_7499" s="T238">младший брат место в чуме за огнем [от дверей дальше…] живет</ta>
            <ta e="T246" id="Seg_7500" s="T242">кинжал вверх поставил</ta>
            <ta e="T251" id="Seg_7501" s="T246">вверху палку поперёк привязали</ta>
            <ta e="T259" id="Seg_7502" s="T251">так видали сверху показалась 3 местн ветка</ta>
            <ta e="T261" id="Seg_7503" s="T259">братья предупреждать</ta>
            <ta e="T265" id="Seg_7504" s="T261">на улицу хотели выйти</ta>
            <ta e="T270" id="Seg_7505" s="T265">как стрелы (лук) вверх взяли приготовились</ta>
            <ta e="T274" id="Seg_7506" s="T270">из воды на берег стреляют</ta>
            <ta e="T278" id="Seg_7507" s="T274">на улицу вышел так говорит</ta>
            <ta e="T287" id="Seg_7508" s="T278">на берег выйдя кушайте успеете воевать плохо из воды на берег</ta>
            <ta e="T290" id="Seg_7509" s="T287">что делать хотите </ta>
            <ta e="T293" id="Seg_7510" s="T290">на берег выйдя покушайте</ta>
            <ta e="T298" id="Seg_7511" s="T293">так стал старший брат </ta>
            <ta e="T301" id="Seg_7512" s="T298">на берег выйдя поедим</ta>
            <ta e="T304" id="Seg_7513" s="T301">на берег вышли</ta>
            <ta e="T310" id="Seg_7514" s="T304">луки и стрелы оставили на ветке положили и оставили</ta>
            <ta e="T314" id="Seg_7515" s="T310">в чум они заходят</ta>
            <ta e="T324" id="Seg_7516" s="T314">ст. л. л. богатырь пална в одно место (рядом) сели</ta>
            <ta e="T333" id="Seg_7517" s="T324">низовья назв. богатырь брат опять рядом сели</ta>
            <ta e="T339" id="Seg_7518" s="T333">3 бр. все рядом сели</ta>
            <ta e="T345" id="Seg_7519" s="T339">потом [дальше] так сказали нож забыли</ta>
            <ta e="T347" id="Seg_7520" s="T345">ножик есть? </ta>
            <ta e="T350" id="Seg_7521" s="T347">ножики нам дайте</ta>
            <ta e="T353" id="Seg_7522" s="T350">кушать ложки сделаем</ta>
            <ta e="T360" id="Seg_7523" s="T353">ножиков всех дайте л. ч. 3 бр.</ta>
            <ta e="T367" id="Seg_7524" s="T360">а п. 3 бр. раньше др. др. говорили</ta>
            <ta e="T372" id="Seg_7525" s="T367">ст. бр. как ножом ударит </ta>
            <ta e="T376" id="Seg_7526" s="T372">они тоже так ударили</ta>
            <ta e="T385" id="Seg_7527" s="T376">однажды смотря парни</ta>
            <ta e="T393" id="Seg_7528" s="T385">ножиком друг друга ударили </ta>
            <ta e="T399" id="Seg_7529" s="T393">бр. хотел ударить</ta>
            <ta e="T403" id="Seg_7530" s="T399">в пустое место попал (ударил)</ta>
            <ta e="T412" id="Seg_7531" s="T403">бр. тоже убежали</ta>
            <ta e="T417" id="Seg_7532" s="T412">секиру схватив преследовал</ta>
            <ta e="T422" id="Seg_7533" s="T417">те два брата дома остались</ta>
            <ta e="T426" id="Seg_7534" s="T422">преследовали</ta>
            <ta e="T432" id="Seg_7535" s="T428">‎‎затем догнав зарубили</ta>
            <ta e="T434" id="Seg_7536" s="T432">пятки отрубил</ta>
            <ta e="T438" id="Seg_7537" s="T434">на живот упал</ta>
            <ta e="T445" id="Seg_7538" s="T438">второй раз хотел отрубить за середину кинжала поймал</ta>
            <ta e="T448" id="Seg_7539" s="T445">нога сломалась</ta>
            <ta e="T452" id="Seg_7540" s="T448">те братьев убил</ta>
            <ta e="T457" id="Seg_7541" s="T452">бр. по дороге дальше убежали</ta>
            <ta e="T460" id="Seg_7542" s="T457">нашел говорит держи</ta>
            <ta e="T462" id="Seg_7543" s="T460">я приду</ta>
            <ta e="T468" id="Seg_7544" s="T462">этот чел. что он делает</ta>
            <ta e="T470" id="Seg_7545" s="T468">я ногу срубил</ta>
            <ta e="T475" id="Seg_7546" s="T470">потом 3 бр. все убили</ta>
            <ta e="T485" id="Seg_7547" s="T475">однажды жили никого не было слыхать</ta>
            <ta e="T488" id="Seg_7548" s="T485">поехали</ta>
            <ta e="T491" id="Seg_7549" s="T488">женам так думает</ta>
            <ta e="T496" id="Seg_7550" s="T491">парень женщин пошли все совсем</ta>
            <ta e="T504" id="Seg_7551" s="T496">мы думали селькуп детей так хорош сказал</ta>
            <ta e="T508" id="Seg_7552" s="T504">я детей воспитывал</ta>
            <ta e="T512" id="Seg_7553" s="T508">сиську тоже мою</ta>
            <ta e="T518" id="Seg_7554" s="T512">с. женщ. вроде все знает</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T27" id="Seg_7555" s="T20">[OSV]: "kərnʼa po" - "a wild rosemary".</ta>
            <ta e="T37" id="Seg_7556" s="T29">[OSV]: "kət" - "a wisard".</ta>
            <ta e="T56" id="Seg_7557" s="T50">[OSV]: "kəːsɨp čʼɔːtɨrqo" - "to work iron". </ta>
            <ta e="T100" id="Seg_7558" s="T93"> ‎‎</ta>
            <ta e="T110" id="Seg_7559" s="T103"> ‎‎[OSV]: "laŋalʼ qum" - "a Nenets".</ta>
            <ta e="T126" id="Seg_7560" s="T117">[OSV]: pülʼčʼas qoltɨ - "Enisej (river)".</ta>
            <ta e="T135" id="Seg_7561" s="T126">[OSV]: "ukkɨr toːt čʼontoːqɨt" - "once".</ta>
            <ta e="T162" id="Seg_7562" s="T160"> ‎‎</ta>
            <ta e="T166" id="Seg_7563" s="T162">[OSV]: "mɔttɨqo" - "to visit somebody's house" (?). </ta>
            <ta e="T184" id="Seg_7564" s="T179">[OSV]: "ınnänɨ ıllä" - "from top downward". </ta>
            <ta e="T206" id="Seg_7565" s="T201">[OSV]: "čʼeːlʼ čʼontɨlʼ" - adj. "midday".</ta>
            <ta e="T214" id="Seg_7566" s="T209">[OSV]: "tıpɨlʼ qum" - "a man".</ta>
            <ta e="T259" id="Seg_7567" s="T251">[OSV]: "tıpɨlʼ qum" - "a man".</ta>
            <ta e="T274" id="Seg_7568" s="T270">[OSV]: 1) The adjective "toqalʼ" is probably related to the adverb "tɔːqɨn" - "on the other side". 2) "konnä" - "upwards, to the shore".</ta>
            <ta e="T304" id="Seg_7569" s="T301">[OSV]: "jam tattɨqo" - "to gather together".</ta>
            <ta e="T333" id="Seg_7570" s="T324">[OSV]: Earlier the middle brother was called "taŋɨlʼ ɔːtälʼ nʼoːtɨlʼ" - "catching up the summer deer".</ta>
            <ta e="T385" id="Seg_7571" s="T376">[OSV]: "sajɨsä mɨrɨqo" - "to observe" (?).</ta>
            <ta e="T403" id="Seg_7572" s="T399">[OSV]: "‎‎šünʼčʼɨpɨlʼ" - adj. "bare".</ta>
            <ta e="T448" id="Seg_7573" s="T445">[OSV]: "′то̄k" is probably a phonological variant of "täːqa" - "a pike".</ta>
            <ta e="T457" id="Seg_7574" s="T452">[OSV]: "wəttɨt šüː" - "down the road".</ta>
            <ta e="T485" id="Seg_7575" s="T475">[OSV]: "toːt təttɨt" - "from then on"; "ukkɨr tot čʼontoːqɨt" - "once".</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T1" id="Seg_7576" s="T0">Сказка Саргаева Андрея Ивановича, переведена его женой Марфой Васил. Сарг.</ta>
            <ta e="T20" id="Seg_7577" s="T14">летом олени быстро бегают, а он его догонял</ta>
            <ta e="T27" id="Seg_7578" s="T20">так быстро, что на землю не наступал, а по вершинам</ta>
            <ta e="T37" id="Seg_7579" s="T29">kъ̊т - вроде колдуна, он чувствовал чутьём всё что делается; 2) ɛпымпа - бывал</ta>
            <ta e="T46" id="Seg_7580" s="T42">′тъ̊тыпак - шаман</ta>
            <ta e="T56" id="Seg_7581" s="T50">′чо̄тырkо - ковать</ta>
            <ta e="T64" id="Seg_7582" s="T56">(3 р)</ta>
            <ta e="T399" id="Seg_7583" s="T393">=промахнулся</ta>
            <ta e="T470" id="Seg_7584" s="T468">опять повторил</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
