<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID738D8B12-AA42-F847-E262-8F0705D26051">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">flk\NAI_1965_HareParka_flk\NAI_1965_HareParka_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">73</ud-information>
            <ud-information attribute-name="# HIAT:w">51</ud-information>
            <ud-information attribute-name="# e">52</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">11</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="NAI">
            <abbreviation>NAI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="NAI"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T52" id="Seg_0" n="sc" s="T0">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Nʼomälʼ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">porqära</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_11" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">Seːlʼčʼ</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">ija</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_18" n="HIAT:ip">(</nts>
                  <nts id="Seg_19" n="HIAT:ip">/</nts>
                  <ts e="T5" id="Seg_21" n="HIAT:w" s="T4">iːjatɨ</ts>
                  <nts id="Seg_22" n="HIAT:ip">)</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">i</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">seːlʼčʼ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">amnatə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">ilimpɔːtɨn</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">nan</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">Ilatɨn</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">mačʼä</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">qɨnnɔːtɨn</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">arat</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_56" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">Äsɨtɨt</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">pɛlkɔːla</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">qala</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_68" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">Ukur</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_73" n="HIAT:w" s="T18">čʼontoːqɨt</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">qalet</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">tüntɔːtɨt</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_83" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_85" n="HIAT:w" s="T21">Qalet</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_88" n="HIAT:w" s="T22">türnɔːtɨt</ts>
                  <nts id="Seg_89" n="HIAT:ip">,</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">na</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_95" n="HIAT:w" s="T24">čʼeːlʼe</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">moqonɨ</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">qənnɔːt</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_105" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_107" n="HIAT:w" s="T27">Qalʼe</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">nı</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">kotat</ts>
                  <nts id="Seg_114" n="HIAT:ip">:</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_117" n="HIAT:w" s="T30">Täːl</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_120" n="HIAT:w" s="T31">tüntɔːmɨt</ts>
                  <nts id="Seg_121" n="HIAT:ip">.</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_124" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_126" n="HIAT:w" s="T32">Nʼoolʼ</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_129" n="HIAT:w" s="T33">porqɨrä</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_132" n="HIAT:w" s="T34">amnäqɨntə</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_135" n="HIAT:w" s="T35">nı</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_138" n="HIAT:w" s="T36">kətoŋɨt</ts>
                  <nts id="Seg_139" n="HIAT:ip">:</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">Apsɨkɔːlɨt</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">qontatot</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_149" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_151" n="HIAT:w" s="T39">Pančʼerla</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_154" n="HIAT:w" s="T40">apsɨqɨlʼ</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_157" n="HIAT:w" s="T41">totɔːtɨt</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_161" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_163" n="HIAT:w" s="T42">Ira</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_166" n="HIAT:w" s="T43">nı</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_169" n="HIAT:w" s="T44">kuroltɨkɨtɨ</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_172" n="HIAT:w" s="T45">ämnämtɨ</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_175" n="HIAT:w" s="T46">mɔːttə</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_178" n="HIAT:w" s="T47">šünčʼe</ts>
                  <nts id="Seg_179" n="HIAT:ip">.</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_182" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_184" n="HIAT:w" s="T48">Nʼuːtäsä</ts>
                  <nts id="Seg_185" n="HIAT:ip">,</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_188" n="HIAT:w" s="T49">senti</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_191" n="HIAT:w" s="T50">nʼuːtsa</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_193" n="HIAT:ip">(</nts>
                  <nts id="Seg_194" n="HIAT:ip">(</nts>
                  <ats e="T52" id="Seg_195" n="HIAT:non-pho" s="T51">BREAK</ats>
                  <nts id="Seg_196" n="HIAT:ip">)</nts>
                  <nts id="Seg_197" n="HIAT:ip">)</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T52" id="Seg_199" n="sc" s="T0">
               <ts e="T1" id="Seg_201" n="e" s="T0">Nʼomälʼ </ts>
               <ts e="T2" id="Seg_203" n="e" s="T1">porqära. </ts>
               <ts e="T3" id="Seg_205" n="e" s="T2">Seːlʼčʼ </ts>
               <ts e="T4" id="Seg_207" n="e" s="T3">ija </ts>
               <ts e="T5" id="Seg_209" n="e" s="T4">(/iːjatɨ) </ts>
               <ts e="T6" id="Seg_211" n="e" s="T5">i </ts>
               <ts e="T7" id="Seg_213" n="e" s="T6">seːlʼčʼ </ts>
               <ts e="T8" id="Seg_215" n="e" s="T7">amnatə </ts>
               <ts e="T9" id="Seg_217" n="e" s="T8">ilimpɔːtɨn </ts>
               <ts e="T10" id="Seg_219" n="e" s="T9">nan. </ts>
               <ts e="T11" id="Seg_221" n="e" s="T10">Ilatɨn </ts>
               <ts e="T12" id="Seg_223" n="e" s="T11">mačʼä </ts>
               <ts e="T13" id="Seg_225" n="e" s="T12">qɨnnɔːtɨn </ts>
               <ts e="T14" id="Seg_227" n="e" s="T13">arat. </ts>
               <ts e="T15" id="Seg_229" n="e" s="T14">Äsɨtɨt </ts>
               <ts e="T16" id="Seg_231" n="e" s="T15">pɛlkɔːla </ts>
               <ts e="T17" id="Seg_233" n="e" s="T16">qala. </ts>
               <ts e="T18" id="Seg_235" n="e" s="T17">Ukur </ts>
               <ts e="T19" id="Seg_237" n="e" s="T18">čʼontoːqɨt </ts>
               <ts e="T20" id="Seg_239" n="e" s="T19">qalet </ts>
               <ts e="T21" id="Seg_241" n="e" s="T20">tüntɔːtɨt. </ts>
               <ts e="T22" id="Seg_243" n="e" s="T21">Qalet </ts>
               <ts e="T23" id="Seg_245" n="e" s="T22">türnɔːtɨt, </ts>
               <ts e="T24" id="Seg_247" n="e" s="T23">na </ts>
               <ts e="T25" id="Seg_249" n="e" s="T24">čʼeːlʼe </ts>
               <ts e="T26" id="Seg_251" n="e" s="T25">moqonɨ </ts>
               <ts e="T27" id="Seg_253" n="e" s="T26">qənnɔːt. </ts>
               <ts e="T28" id="Seg_255" n="e" s="T27">Qalʼe </ts>
               <ts e="T29" id="Seg_257" n="e" s="T28">nı </ts>
               <ts e="T30" id="Seg_259" n="e" s="T29">kotat: </ts>
               <ts e="T31" id="Seg_261" n="e" s="T30">Täːl </ts>
               <ts e="T32" id="Seg_263" n="e" s="T31">tüntɔːmɨt. </ts>
               <ts e="T33" id="Seg_265" n="e" s="T32">Nʼoolʼ </ts>
               <ts e="T34" id="Seg_267" n="e" s="T33">porqɨrä </ts>
               <ts e="T35" id="Seg_269" n="e" s="T34">amnäqɨntə </ts>
               <ts e="T36" id="Seg_271" n="e" s="T35">nı </ts>
               <ts e="T37" id="Seg_273" n="e" s="T36">kətoŋɨt: </ts>
               <ts e="T38" id="Seg_275" n="e" s="T37">Apsɨkɔːlɨt </ts>
               <ts e="T39" id="Seg_277" n="e" s="T38">qontatot. </ts>
               <ts e="T40" id="Seg_279" n="e" s="T39">Pančʼerla </ts>
               <ts e="T41" id="Seg_281" n="e" s="T40">apsɨqɨlʼ </ts>
               <ts e="T42" id="Seg_283" n="e" s="T41">totɔːtɨt. </ts>
               <ts e="T43" id="Seg_285" n="e" s="T42">Ira </ts>
               <ts e="T44" id="Seg_287" n="e" s="T43">nı </ts>
               <ts e="T45" id="Seg_289" n="e" s="T44">kuroltɨkɨtɨ </ts>
               <ts e="T46" id="Seg_291" n="e" s="T45">ämnämtɨ </ts>
               <ts e="T47" id="Seg_293" n="e" s="T46">mɔːttə </ts>
               <ts e="T48" id="Seg_295" n="e" s="T47">šünčʼe. </ts>
               <ts e="T49" id="Seg_297" n="e" s="T48">Nʼuːtäsä, </ts>
               <ts e="T50" id="Seg_299" n="e" s="T49">senti </ts>
               <ts e="T51" id="Seg_301" n="e" s="T50">nʼuːtsa </ts>
               <ts e="T52" id="Seg_303" n="e" s="T51">((BREAK)) </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2" id="Seg_304" s="T0">NAI_1965_HareParka_flk.001 (001)</ta>
            <ta e="T10" id="Seg_305" s="T2">NAI_1965_HareParka_flk.002 (002.001)</ta>
            <ta e="T14" id="Seg_306" s="T10">NAI_1965_HareParka_flk.003 (002.002)</ta>
            <ta e="T17" id="Seg_307" s="T14">NAI_1965_HareParka_flk.004 (002.003)</ta>
            <ta e="T21" id="Seg_308" s="T17">NAI_1965_HareParka_flk.005 (002.004)</ta>
            <ta e="T27" id="Seg_309" s="T21">NAI_1965_HareParka_flk.006 (002.005)</ta>
            <ta e="T32" id="Seg_310" s="T27">NAI_1965_HareParka_flk.007 (002.006)</ta>
            <ta e="T39" id="Seg_311" s="T32">NAI_1965_HareParka_flk.008 (002.007)</ta>
            <ta e="T42" id="Seg_312" s="T39">NAI_1965_HareParka_flk.009 (002.008)</ta>
            <ta e="T48" id="Seg_313" s="T42">NAI_1965_HareParka_flk.010 (002.009)</ta>
            <ta e="T52" id="Seg_314" s="T48">NAI_1965_HareParka_flk.011 (002.010)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T2" id="Seg_315" s="T0">′нʼомӓl порkӓра.</ta>
            <ta e="T10" id="Seg_316" s="T2">′сеlчиjа иjаты и ′сеl′чамнатъ ′иlимпотынан.</ta>
            <ta e="T14" id="Seg_317" s="T10">и′lатын ′ма̄чеkынатын ′арат.</ta>
            <ta e="T17" id="Seg_318" s="T14">′ӓсытыт ′пеlкоlа ′каlа.</ta>
            <ta e="T21" id="Seg_319" s="T17">у′курчон′тотыт kалет ′тӱнтотыт.</ta>
            <ta e="T27" id="Seg_320" s="T21">kалет ′тӱ̄рнотыт на̄′челʼе ′моkоныkъ̊ннот.</ta>
            <ta e="T32" id="Seg_321" s="T27">′kалʼе ′никотат ′тӓlтӱнтоɣыт.</ta>
            <ta e="T39" id="Seg_322" s="T32">′нӧ′оlпоркырӓ амнӓkындъ ни′k′ъ̊тоɣыт. ′апсыкоlыт ‵конта′тот.</ta>
            <ta e="T42" id="Seg_323" s="T39">′панчерlа ′апсыkыl ′тототыт.</ta>
            <ta e="T48" id="Seg_324" s="T42"> ира ни′кӯроlдыɣыты ′ӓмнӓмты ′мо̄тъ ′шʼунче.</ta>
            <ta e="T52" id="Seg_325" s="T48">′нӱ̄тӓсӓ сӓнди ′нӱ̄тса…</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T2" id="Seg_326" s="T0">nʼomälʼ porqära.</ta>
            <ta e="T10" id="Seg_327" s="T2">selʼčʼija ijatɨ i selʼčʼamnatə ilʼimpotɨnan.</ta>
            <ta e="T14" id="Seg_328" s="T10">ilʼatɨn maːčʼeqɨnatɨn arat.</ta>
            <ta e="T17" id="Seg_329" s="T14">äsɨtɨt pelʼkolʼa kalʼa.</ta>
            <ta e="T21" id="Seg_330" s="T17">ukurčʼontotɨt qalet tüntotɨt.</ta>
            <ta e="T27" id="Seg_331" s="T21">qalet tüːrnotɨt naːčʼelʼe moqonɨqənnot.</ta>
            <ta e="T32" id="Seg_332" s="T27">qalʼe nikotat tälʼtüntoqɨt.</ta>
            <ta e="T39" id="Seg_333" s="T32">nöolʼporkɨrä amnäqɨndə niqətoqɨt. apsɨkolʼɨt kontatot.</ta>
            <ta e="T42" id="Seg_334" s="T39">pančʼerlʼa apsɨqɨlʼ tototɨt.</ta>
            <ta e="T48" id="Seg_335" s="T42"> ira nikuːrolʼdɨqɨtɨ ämnämtɨ moːtə šʼunčʼe.</ta>
            <ta e="T52" id="Seg_336" s="T48">nüːtäsä sändi nüːtsa…</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2" id="Seg_337" s="T0">Nʼomälʼ porqära. </ta>
            <ta e="T10" id="Seg_338" s="T2">Seːlʼčʼ ija (/iːjatɨ) i seːlʼčʼ amnatə ilimpɔːtɨn nan. </ta>
            <ta e="T14" id="Seg_339" s="T10">Ilatɨn mačʼä qɨnnɔːtɨn arat. </ta>
            <ta e="T17" id="Seg_340" s="T14">Äsɨtɨt pɛlkɔːla qala. </ta>
            <ta e="T21" id="Seg_341" s="T17">Ukur čʼontoːqɨt qalet tüntɔːtɨt. </ta>
            <ta e="T27" id="Seg_342" s="T21">Qalet türnɔːtɨt, na čʼeːlʼe moqonɨ qənnɔːt. </ta>
            <ta e="T32" id="Seg_343" s="T27">Qalʼe nı kotat: Täːl tüntɔːmɨt. </ta>
            <ta e="T39" id="Seg_344" s="T32">Nʼoolʼ porqɨrä amnäqɨntə nı kətoŋɨt: Apsɨkɔːlɨt qontatot. </ta>
            <ta e="T42" id="Seg_345" s="T39">Pančʼerla apsɨqɨlʼ totɔːtɨt. </ta>
            <ta e="T48" id="Seg_346" s="T42">Ira nı kuroltɨkɨtɨ ämnämtɨ mɔːttə šünčʼe. </ta>
            <ta e="T52" id="Seg_347" s="T48">Nʼuːtäsä, senti nʼuːtsa…</ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_348" s="T0">nʼomä-lʼ</ta>
            <ta e="T2" id="Seg_349" s="T1">porq-ära</ta>
            <ta e="T3" id="Seg_350" s="T2">seːlʼčʼ</ta>
            <ta e="T4" id="Seg_351" s="T3">ija</ta>
            <ta e="T5" id="Seg_352" s="T4">iːja-tɨ</ta>
            <ta e="T6" id="Seg_353" s="T5">i</ta>
            <ta e="T7" id="Seg_354" s="T6">seːlʼčʼ</ta>
            <ta e="T8" id="Seg_355" s="T7">amna-tə</ta>
            <ta e="T9" id="Seg_356" s="T8">ili-mpɔː-tɨn</ta>
            <ta e="T10" id="Seg_357" s="T9">nan</ta>
            <ta e="T11" id="Seg_358" s="T10">ila-tɨn</ta>
            <ta e="T12" id="Seg_359" s="T11">mačʼä</ta>
            <ta e="T13" id="Seg_360" s="T12">qɨn-nɔː-tɨn</ta>
            <ta e="T14" id="Seg_361" s="T13">ara-t</ta>
            <ta e="T15" id="Seg_362" s="T14">äsɨ-tɨt</ta>
            <ta e="T16" id="Seg_363" s="T15">pɛl-kɔːla</ta>
            <ta e="T17" id="Seg_364" s="T16">qala</ta>
            <ta e="T18" id="Seg_365" s="T17">ukur</ta>
            <ta e="T19" id="Seg_366" s="T18">čʼontoː-qɨt</ta>
            <ta e="T20" id="Seg_367" s="T19">qale-t</ta>
            <ta e="T21" id="Seg_368" s="T20">tü-ntɔː-tɨt</ta>
            <ta e="T22" id="Seg_369" s="T21">qale-t</ta>
            <ta e="T23" id="Seg_370" s="T22">tü-r-nɔː-tɨt</ta>
            <ta e="T24" id="Seg_371" s="T23">na</ta>
            <ta e="T25" id="Seg_372" s="T24">čʼeːlʼe</ta>
            <ta e="T26" id="Seg_373" s="T25">moqonɨ</ta>
            <ta e="T27" id="Seg_374" s="T26">qən-nɔː-t</ta>
            <ta e="T28" id="Seg_375" s="T27">qalʼe</ta>
            <ta e="T29" id="Seg_376" s="T28">nı</ta>
            <ta e="T30" id="Seg_377" s="T29">kota-t</ta>
            <ta e="T31" id="Seg_378" s="T30">täːl</ta>
            <ta e="T32" id="Seg_379" s="T31">tü-ntɔː-mɨt</ta>
            <ta e="T33" id="Seg_380" s="T32">nʼoo-lʼ</ta>
            <ta e="T34" id="Seg_381" s="T33">porq-ɨrä</ta>
            <ta e="T35" id="Seg_382" s="T34">amnä-qɨn-tə</ta>
            <ta e="T36" id="Seg_383" s="T35">nı</ta>
            <ta e="T37" id="Seg_384" s="T36">kəto-ŋɨ-t</ta>
            <ta e="T38" id="Seg_385" s="T37">Apsɨ-kɔːlɨ-t</ta>
            <ta e="T39" id="Seg_386" s="T38">qon-ta-tot</ta>
            <ta e="T40" id="Seg_387" s="T39">pan-čʼer-la</ta>
            <ta e="T41" id="Seg_388" s="T40">apsɨ-qɨ-lʼ</ta>
            <ta e="T42" id="Seg_389" s="T41">totɔː-tɨt</ta>
            <ta e="T43" id="Seg_390" s="T42">ira</ta>
            <ta e="T44" id="Seg_391" s="T43">nı</ta>
            <ta e="T45" id="Seg_392" s="T44">kur-oltɨ-kɨ-tɨ</ta>
            <ta e="T46" id="Seg_393" s="T45">ämnä-m-tɨ</ta>
            <ta e="T47" id="Seg_394" s="T46">mɔːt-tə</ta>
            <ta e="T48" id="Seg_395" s="T47">šünčʼe</ta>
            <ta e="T49" id="Seg_396" s="T48">nʼuːtä-sä</ta>
            <ta e="T50" id="Seg_397" s="T49">senti</ta>
            <ta e="T51" id="Seg_398" s="T50">nʼuːt-sa</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_399" s="T0">nʼoma-lʼ</ta>
            <ta e="T2" id="Seg_400" s="T1">porqɨ-ira</ta>
            <ta e="T3" id="Seg_401" s="T2">seːlʼčʼɨ</ta>
            <ta e="T4" id="Seg_402" s="T3">iːja</ta>
            <ta e="T5" id="Seg_403" s="T4">iːja-tɨ</ta>
            <ta e="T6" id="Seg_404" s="T5">i</ta>
            <ta e="T7" id="Seg_405" s="T6">seːlʼčʼɨ</ta>
            <ta e="T8" id="Seg_406" s="T7">ämnä-tɨ</ta>
            <ta e="T9" id="Seg_407" s="T8">ilɨ-mpɨ-tɨt</ta>
            <ta e="T10" id="Seg_408" s="T9">nanɨ</ta>
            <ta e="T11" id="Seg_409" s="T10">iːja-tɨt</ta>
            <ta e="T12" id="Seg_410" s="T11">mačʼä</ta>
            <ta e="T13" id="Seg_411" s="T12">qən-ŋɨ-tɨt</ta>
            <ta e="T14" id="Seg_412" s="T13">ara-n</ta>
            <ta e="T15" id="Seg_413" s="T14">əsɨ-tɨt</ta>
            <ta e="T16" id="Seg_414" s="T15">pɛlɨ-kɔːlɨ</ta>
            <ta e="T17" id="Seg_415" s="T16">qalɨ</ta>
            <ta e="T18" id="Seg_416" s="T17">ukkɨr</ta>
            <ta e="T19" id="Seg_417" s="T18">čʼontɨ-qɨn</ta>
            <ta e="T20" id="Seg_418" s="T19">qälɨk-t</ta>
            <ta e="T21" id="Seg_419" s="T20">tü-ntɨ-tɨt</ta>
            <ta e="T22" id="Seg_420" s="T21">qälɨk-t</ta>
            <ta e="T23" id="Seg_421" s="T22">tü-r-ŋɨ-tɨt</ta>
            <ta e="T24" id="Seg_422" s="T23">na</ta>
            <ta e="T25" id="Seg_423" s="T24">čʼeːlɨ</ta>
            <ta e="T26" id="Seg_424" s="T25">moqɨnä</ta>
            <ta e="T27" id="Seg_425" s="T26">qən-ŋɨ-tɨt</ta>
            <ta e="T28" id="Seg_426" s="T27">qälɨk</ta>
            <ta e="T29" id="Seg_427" s="T28">nık</ta>
            <ta e="T30" id="Seg_428" s="T29">kətɨ-tɨ</ta>
            <ta e="T31" id="Seg_429" s="T30">täːlɨ</ta>
            <ta e="T32" id="Seg_430" s="T31">tü-ɛntɨ-mɨt</ta>
            <ta e="T33" id="Seg_431" s="T32">nʼoma-lʼ</ta>
            <ta e="T34" id="Seg_432" s="T33">porqɨ-ira</ta>
            <ta e="T35" id="Seg_433" s="T34">ämnä-qɨn-ntɨ</ta>
            <ta e="T36" id="Seg_434" s="T35">nık</ta>
            <ta e="T37" id="Seg_435" s="T36">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T38" id="Seg_436" s="T37">apsɨ-kɔːlɨ-k</ta>
            <ta e="T39" id="Seg_437" s="T38">qən-ɛntɨ-tɨt</ta>
            <ta e="T40" id="Seg_438" s="T39">pat-ntɨr-lä</ta>
            <ta e="T41" id="Seg_439" s="T40">apsɨ-qɨn-lʼ</ta>
            <ta e="T42" id="Seg_440" s="T41">tottɨ-tɨt</ta>
            <ta e="T43" id="Seg_441" s="T42">ira</ta>
            <ta e="T44" id="Seg_442" s="T43">nık</ta>
            <ta e="T45" id="Seg_443" s="T44">*kurɨ-altɨ-kkɨ-tɨ</ta>
            <ta e="T46" id="Seg_444" s="T45">ämnä-m-tɨ</ta>
            <ta e="T47" id="Seg_445" s="T46">mɔːt-tɨ</ta>
            <ta e="T48" id="Seg_446" s="T47">šünʼčʼɨ</ta>
            <ta e="T49" id="Seg_447" s="T48">nʼuːtɨ-sä</ta>
            <ta e="T50" id="Seg_448" s="T49">šentɨ</ta>
            <ta e="T51" id="Seg_449" s="T50">nʼuːtɨ-sä</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_450" s="T0">hare-ADJZ</ta>
            <ta e="T2" id="Seg_451" s="T1">clothing-old.man.[NOM]</ta>
            <ta e="T3" id="Seg_452" s="T2">seven</ta>
            <ta e="T4" id="Seg_453" s="T3">son.[NOM]</ta>
            <ta e="T5" id="Seg_454" s="T4">son.[NOM]-3SG</ta>
            <ta e="T6" id="Seg_455" s="T5">and</ta>
            <ta e="T7" id="Seg_456" s="T6">seven</ta>
            <ta e="T8" id="Seg_457" s="T7">daughter.in.law.[NOM]-3SG</ta>
            <ta e="T9" id="Seg_458" s="T8">live-PST.NAR-3PL</ta>
            <ta e="T10" id="Seg_459" s="T9">sometimes</ta>
            <ta e="T11" id="Seg_460" s="T10">son.[NOM]-3PL</ta>
            <ta e="T12" id="Seg_461" s="T11">to.the.forest</ta>
            <ta e="T13" id="Seg_462" s="T12">leave-CO-3PL</ta>
            <ta e="T14" id="Seg_463" s="T13">autumn-ADV.LOC</ta>
            <ta e="T15" id="Seg_464" s="T14">father.[NOM]-3PL</ta>
            <ta e="T16" id="Seg_465" s="T15">friend-CAR</ta>
            <ta e="T17" id="Seg_466" s="T16">stay.[3SG.S]</ta>
            <ta e="T18" id="Seg_467" s="T17">one</ta>
            <ta e="T19" id="Seg_468" s="T18">middle-LOC</ta>
            <ta e="T20" id="Seg_469" s="T19">Nenets-PL.[NOM]</ta>
            <ta e="T21" id="Seg_470" s="T20">come-INFER-3PL</ta>
            <ta e="T22" id="Seg_471" s="T21">Nenets-PL.[NOM]</ta>
            <ta e="T23" id="Seg_472" s="T22">come-FRQ-CO-3PL</ta>
            <ta e="T24" id="Seg_473" s="T23">this</ta>
            <ta e="T25" id="Seg_474" s="T24">day.[NOM]</ta>
            <ta e="T26" id="Seg_475" s="T25">home</ta>
            <ta e="T27" id="Seg_476" s="T26">go.away-CO-3PL</ta>
            <ta e="T28" id="Seg_477" s="T27">Nenets.[NOM]</ta>
            <ta e="T29" id="Seg_478" s="T28">so</ta>
            <ta e="T30" id="Seg_479" s="T29">say-3SG.O</ta>
            <ta e="T31" id="Seg_480" s="T30">tomorrow</ta>
            <ta e="T32" id="Seg_481" s="T31">come-FUT-1PL</ta>
            <ta e="T33" id="Seg_482" s="T32">hare-ADJZ</ta>
            <ta e="T34" id="Seg_483" s="T33">clothing-old.man.[NOM]</ta>
            <ta e="T35" id="Seg_484" s="T34">daughter.in.law-ILL-OBL.3SG</ta>
            <ta e="T36" id="Seg_485" s="T35">so</ta>
            <ta e="T37" id="Seg_486" s="T36">say-CO-3SG.O</ta>
            <ta e="T38" id="Seg_487" s="T37">food-CAR-ADVZ</ta>
            <ta e="T39" id="Seg_488" s="T38">leave-FUT-3PL</ta>
            <ta e="T40" id="Seg_489" s="T39">go.down-DRV-CVB</ta>
            <ta e="T41" id="Seg_490" s="T40">food-LOC-ADJZ</ta>
            <ta e="T42" id="Seg_491" s="T41">put-3PL</ta>
            <ta e="T43" id="Seg_492" s="T42">old.man.[NOM]</ta>
            <ta e="T44" id="Seg_493" s="T43">so</ta>
            <ta e="T45" id="Seg_494" s="T44">go-CAUS-DUR-3SG.O</ta>
            <ta e="T46" id="Seg_495" s="T45">daughter.in.law-ACC-3SG</ta>
            <ta e="T47" id="Seg_496" s="T46">tent.[NOM]-3SG</ta>
            <ta e="T48" id="Seg_497" s="T47">inside.[NOM]</ta>
            <ta e="T49" id="Seg_498" s="T48">hay-INSTR</ta>
            <ta e="T50" id="Seg_499" s="T49">fresh</ta>
            <ta e="T51" id="Seg_500" s="T50">hay-INSTR</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_501" s="T0">заяц-ADJZ</ta>
            <ta e="T2" id="Seg_502" s="T1">одежда-старик.[NOM]</ta>
            <ta e="T3" id="Seg_503" s="T2">семь</ta>
            <ta e="T4" id="Seg_504" s="T3">сын.[NOM]</ta>
            <ta e="T5" id="Seg_505" s="T4">сын.[NOM]-3SG</ta>
            <ta e="T6" id="Seg_506" s="T5">и</ta>
            <ta e="T7" id="Seg_507" s="T6">семь</ta>
            <ta e="T8" id="Seg_508" s="T7">жена.сына.[NOM]-3SG</ta>
            <ta e="T9" id="Seg_509" s="T8">жить-PST.NAR-3PL</ta>
            <ta e="T10" id="Seg_510" s="T9">некоторое.время</ta>
            <ta e="T11" id="Seg_511" s="T10">сын.[NOM]-3PL</ta>
            <ta e="T12" id="Seg_512" s="T11">в.лес</ta>
            <ta e="T13" id="Seg_513" s="T12">отправиться-CO-3PL</ta>
            <ta e="T14" id="Seg_514" s="T13">осень-ADV.LOC</ta>
            <ta e="T15" id="Seg_515" s="T14">отец.[NOM]-3PL</ta>
            <ta e="T16" id="Seg_516" s="T15">друг-CAR</ta>
            <ta e="T17" id="Seg_517" s="T16">остаться.[3SG.S]</ta>
            <ta e="T18" id="Seg_518" s="T17">один</ta>
            <ta e="T19" id="Seg_519" s="T18">середина-LOC</ta>
            <ta e="T20" id="Seg_520" s="T19">ненец-PL.[NOM]</ta>
            <ta e="T21" id="Seg_521" s="T20">прийти-INFER-3PL</ta>
            <ta e="T22" id="Seg_522" s="T21">ненец-PL.[NOM]</ta>
            <ta e="T23" id="Seg_523" s="T22">прийти-FRQ-CO-3PL</ta>
            <ta e="T24" id="Seg_524" s="T23">этот</ta>
            <ta e="T25" id="Seg_525" s="T24">день.[NOM]</ta>
            <ta e="T26" id="Seg_526" s="T25">домой</ta>
            <ta e="T27" id="Seg_527" s="T26">уйти-CO-3PL</ta>
            <ta e="T28" id="Seg_528" s="T27">ненец.[NOM]</ta>
            <ta e="T29" id="Seg_529" s="T28">так</ta>
            <ta e="T30" id="Seg_530" s="T29">сказать-3SG.O</ta>
            <ta e="T31" id="Seg_531" s="T30">завтра</ta>
            <ta e="T32" id="Seg_532" s="T31">прийти-FUT-1PL</ta>
            <ta e="T33" id="Seg_533" s="T32">заяц-ADJZ</ta>
            <ta e="T34" id="Seg_534" s="T33">одежда-старик.[NOM]</ta>
            <ta e="T35" id="Seg_535" s="T34">жена.сына-ILL-OBL.3SG</ta>
            <ta e="T36" id="Seg_536" s="T35">так</ta>
            <ta e="T37" id="Seg_537" s="T36">сказать-CO-3SG.O</ta>
            <ta e="T38" id="Seg_538" s="T37">еда-CAR-ADVZ</ta>
            <ta e="T39" id="Seg_539" s="T38">отправиться-FUT-3PL</ta>
            <ta e="T40" id="Seg_540" s="T39">залезть-DRV-CVB</ta>
            <ta e="T41" id="Seg_541" s="T40">еда-LOC-ADJZ</ta>
            <ta e="T42" id="Seg_542" s="T41">положить-3PL</ta>
            <ta e="T43" id="Seg_543" s="T42">старик.[NOM]</ta>
            <ta e="T44" id="Seg_544" s="T43">так</ta>
            <ta e="T45" id="Seg_545" s="T44">идти-CAUS-DUR-3SG.O</ta>
            <ta e="T46" id="Seg_546" s="T45">жена.сына-ACC-3SG</ta>
            <ta e="T47" id="Seg_547" s="T46">чум.[NOM]-3SG</ta>
            <ta e="T48" id="Seg_548" s="T47">внутри.[NOM]</ta>
            <ta e="T49" id="Seg_549" s="T48">сено-INSTR</ta>
            <ta e="T50" id="Seg_550" s="T49">свежий</ta>
            <ta e="T51" id="Seg_551" s="T50">сено-INSTR</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_552" s="T0">n-n&gt;adj</ta>
            <ta e="T2" id="Seg_553" s="T1">n-n-n:case3</ta>
            <ta e="T3" id="Seg_554" s="T2">num</ta>
            <ta e="T4" id="Seg_555" s="T3">n-n:case3</ta>
            <ta e="T5" id="Seg_556" s="T4">n-n:case1-n:poss</ta>
            <ta e="T6" id="Seg_557" s="T5">conj</ta>
            <ta e="T7" id="Seg_558" s="T6">num</ta>
            <ta e="T8" id="Seg_559" s="T7">n-n:case1-n:poss</ta>
            <ta e="T9" id="Seg_560" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_561" s="T9">adv</ta>
            <ta e="T11" id="Seg_562" s="T10">n-n:case1-n:poss</ta>
            <ta e="T12" id="Seg_563" s="T11">adv</ta>
            <ta e="T13" id="Seg_564" s="T12">v-v:ins-v:pn</ta>
            <ta e="T14" id="Seg_565" s="T13">n-n&gt;adv</ta>
            <ta e="T15" id="Seg_566" s="T14">n-n:case1-n:poss</ta>
            <ta e="T16" id="Seg_567" s="T15">n-n&gt;adj</ta>
            <ta e="T17" id="Seg_568" s="T16">v-v:pn</ta>
            <ta e="T18" id="Seg_569" s="T17">num</ta>
            <ta e="T19" id="Seg_570" s="T18">n-n:case3</ta>
            <ta e="T20" id="Seg_571" s="T19">n-n:num-n:case3</ta>
            <ta e="T21" id="Seg_572" s="T20">v-v:tense-mood-v:pn</ta>
            <ta e="T22" id="Seg_573" s="T21">n-n:num-n:case3</ta>
            <ta e="T23" id="Seg_574" s="T22">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T24" id="Seg_575" s="T23">dem</ta>
            <ta e="T25" id="Seg_576" s="T24">n-n:case3</ta>
            <ta e="T26" id="Seg_577" s="T25">adv</ta>
            <ta e="T27" id="Seg_578" s="T26">v-v:ins-v:pn</ta>
            <ta e="T28" id="Seg_579" s="T27">n-n:case3</ta>
            <ta e="T29" id="Seg_580" s="T28">adv</ta>
            <ta e="T30" id="Seg_581" s="T29">v-v:pn</ta>
            <ta e="T31" id="Seg_582" s="T30">adv</ta>
            <ta e="T32" id="Seg_583" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_584" s="T32">n-n&gt;adj</ta>
            <ta e="T34" id="Seg_585" s="T33">n-n-n:case3</ta>
            <ta e="T35" id="Seg_586" s="T34">n-n:case2-n:obl.poss</ta>
            <ta e="T36" id="Seg_587" s="T35">adv</ta>
            <ta e="T37" id="Seg_588" s="T36">v-v:ins-v:pn</ta>
            <ta e="T38" id="Seg_589" s="T37">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T39" id="Seg_590" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_591" s="T39">v-v&gt;v-v&gt;adv</ta>
            <ta e="T41" id="Seg_592" s="T40">n-n:case2-n&gt;adj</ta>
            <ta e="T42" id="Seg_593" s="T41">v-v:pn</ta>
            <ta e="T43" id="Seg_594" s="T42">n-n:case3</ta>
            <ta e="T44" id="Seg_595" s="T43">adv</ta>
            <ta e="T45" id="Seg_596" s="T44">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T46" id="Seg_597" s="T45">n-n:case1-n:poss</ta>
            <ta e="T47" id="Seg_598" s="T46">n-n:case1-n:poss</ta>
            <ta e="T48" id="Seg_599" s="T47">pp-n:case3</ta>
            <ta e="T49" id="Seg_600" s="T48">n-n:case3</ta>
            <ta e="T50" id="Seg_601" s="T49">adj</ta>
            <ta e="T51" id="Seg_602" s="T50">n-n:case3</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_603" s="T0">adj</ta>
            <ta e="T2" id="Seg_604" s="T1">n</ta>
            <ta e="T3" id="Seg_605" s="T2">num</ta>
            <ta e="T4" id="Seg_606" s="T3">n</ta>
            <ta e="T5" id="Seg_607" s="T4">n</ta>
            <ta e="T6" id="Seg_608" s="T5">conj</ta>
            <ta e="T7" id="Seg_609" s="T6">num</ta>
            <ta e="T8" id="Seg_610" s="T7">n</ta>
            <ta e="T9" id="Seg_611" s="T8">v</ta>
            <ta e="T10" id="Seg_612" s="T9">adv</ta>
            <ta e="T11" id="Seg_613" s="T10">n</ta>
            <ta e="T12" id="Seg_614" s="T11">adv</ta>
            <ta e="T13" id="Seg_615" s="T12">v</ta>
            <ta e="T14" id="Seg_616" s="T13">adv</ta>
            <ta e="T15" id="Seg_617" s="T14">n</ta>
            <ta e="T16" id="Seg_618" s="T15">adj</ta>
            <ta e="T17" id="Seg_619" s="T16">v</ta>
            <ta e="T18" id="Seg_620" s="T17">num</ta>
            <ta e="T19" id="Seg_621" s="T18">n</ta>
            <ta e="T20" id="Seg_622" s="T19">n</ta>
            <ta e="T21" id="Seg_623" s="T20">v</ta>
            <ta e="T22" id="Seg_624" s="T21">n</ta>
            <ta e="T23" id="Seg_625" s="T22">v</ta>
            <ta e="T24" id="Seg_626" s="T23">dem</ta>
            <ta e="T25" id="Seg_627" s="T24">n</ta>
            <ta e="T26" id="Seg_628" s="T25">adv</ta>
            <ta e="T27" id="Seg_629" s="T26">v</ta>
            <ta e="T28" id="Seg_630" s="T27">n</ta>
            <ta e="T29" id="Seg_631" s="T28">adv</ta>
            <ta e="T30" id="Seg_632" s="T29">v</ta>
            <ta e="T31" id="Seg_633" s="T30">adv</ta>
            <ta e="T32" id="Seg_634" s="T31">v</ta>
            <ta e="T33" id="Seg_635" s="T32">adj</ta>
            <ta e="T34" id="Seg_636" s="T33">n</ta>
            <ta e="T35" id="Seg_637" s="T34">n</ta>
            <ta e="T36" id="Seg_638" s="T35">adv</ta>
            <ta e="T37" id="Seg_639" s="T36">v</ta>
            <ta e="T38" id="Seg_640" s="T37">adv</ta>
            <ta e="T39" id="Seg_641" s="T38">v</ta>
            <ta e="T40" id="Seg_642" s="T39">v</ta>
            <ta e="T41" id="Seg_643" s="T40">adj</ta>
            <ta e="T42" id="Seg_644" s="T41">v</ta>
            <ta e="T43" id="Seg_645" s="T42">n</ta>
            <ta e="T44" id="Seg_646" s="T43">adv</ta>
            <ta e="T45" id="Seg_647" s="T44">v</ta>
            <ta e="T46" id="Seg_648" s="T45">n</ta>
            <ta e="T47" id="Seg_649" s="T46">n</ta>
            <ta e="T48" id="Seg_650" s="T47">pp</ta>
            <ta e="T49" id="Seg_651" s="T48">n</ta>
            <ta e="T50" id="Seg_652" s="T49">adj</ta>
            <ta e="T51" id="Seg_653" s="T50">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T2" id="Seg_654" s="T0">Старик "заячья шуба".</ta>
            <ta e="T10" id="Seg_655" s="T2">[С ним] семь сыновей и семь невесток жили какое-то время.</ta>
            <ta e="T14" id="Seg_656" s="T10">Сыновья осенью ушли в лес.</ta>
            <ta e="T17" id="Seg_657" s="T14">Отец один остался.</ta>
            <ta e="T21" id="Seg_658" s="T17">Однажды ненцы пришли.</ta>
            <ta e="T27" id="Seg_659" s="T21">Ненцы пришли, в этот же день домой ушли.</ta>
            <ta e="T32" id="Seg_660" s="T27">Ненцы сказали: "Мы завтра придём."</ta>
            <ta e="T39" id="Seg_661" s="T32">Старик "заячья шуба" говорит невесткам: "[Ненцы] без еды [голодные] поедут."</ta>
            <ta e="T42" id="Seg_662" s="T39">[Невестки] спустились и, вернувшись, разложили еду. </ta>
            <ta e="T48" id="Seg_663" s="T42">Старик сказал невесткам пойти в чум.</ta>
            <ta e="T52" id="Seg_664" s="T48">Сеном, свежим сеном…</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2" id="Seg_665" s="T0">The old man "Hare parka".</ta>
            <ta e="T10" id="Seg_666" s="T2">At some time his seven sons and seven daughters-in-law lived [with him].</ta>
            <ta e="T14" id="Seg_667" s="T10">The sons go to the forest in autumn.</ta>
            <ta e="T17" id="Seg_668" s="T14">The father stayed alone.</ta>
            <ta e="T21" id="Seg_669" s="T17">Once Nenets people came.</ta>
            <ta e="T27" id="Seg_670" s="T21">Nenets people came, the same day they went home.</ta>
            <ta e="T32" id="Seg_671" s="T27">The Nenets said: "We'll come tomorrow."</ta>
            <ta e="T39" id="Seg_672" s="T32">The old man "hare parka" says to his daughters-in-law: "[The Nenets] will go without food [hungry]."</ta>
            <ta e="T42" id="Seg_673" s="T39">[The daughters-in-law] went down and put out food [there].</ta>
            <ta e="T48" id="Seg_674" s="T42">The old man made his daughters-in-law to go into the tent.</ta>
            <ta e="T52" id="Seg_675" s="T48">With hay, with fresh hay…</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2" id="Seg_676" s="T0">Der alte Mann "Hasenmantel".</ta>
            <ta e="T10" id="Seg_677" s="T2">Irgendwann lebten [mit ihm] seine sieben Söhne und sieben Schwiegertöchter.</ta>
            <ta e="T14" id="Seg_678" s="T10">Die Söhne gingen im Herbst in den Wald.</ta>
            <ta e="T17" id="Seg_679" s="T14">Der Vater blieb alleine.</ta>
            <ta e="T21" id="Seg_680" s="T17">Einmal kamen Nenzen.</ta>
            <ta e="T27" id="Seg_681" s="T21">Nenzen kamen, an diesem Tag gingen sie nach Hause.</ta>
            <ta e="T32" id="Seg_682" s="T27">Die Nenzen sagten: "Wir kommen morgen."</ta>
            <ta e="T39" id="Seg_683" s="T32">Der Alte "Hasenmantel" sagt seinen Schwiegertöchtern: "Sie gehen ohne Essen."</ta>
            <ta e="T42" id="Seg_684" s="T39">[Die Schwiegertöchter] gingen hinunter und legten Essen hin.</ta>
            <ta e="T48" id="Seg_685" s="T42">Der Alte ließ seine Schwiegertöchter ins Zelt gehen.</ta>
            <ta e="T52" id="Seg_686" s="T48">Mit Heu, mit frischem Heu…</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr" />
         <annotation name="nt" tierref="nt">
            <ta e="T2" id="Seg_687" s="T0">[OSV]: "Nomalʼ porqɨ" - the nickname of an old man in Selkup fairytales.</ta>
            <ta e="T14" id="Seg_688" s="T10">[OSV]: The speaker alternates between Past and Present tenses.</ta>
            <ta e="T21" id="Seg_689" s="T17">[OSV]: "ukur čʼontotɨt" has been edited into "ukur čʼontoːqɨt".</ta>
            <ta e="T32" id="Seg_690" s="T27">[OSV]: The verbal form "tüntoqɨt" has been edited into "tüntɔːmɨt".</ta>
            <ta e="T52" id="Seg_691" s="T48">[OSV]: The text is not finished.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
