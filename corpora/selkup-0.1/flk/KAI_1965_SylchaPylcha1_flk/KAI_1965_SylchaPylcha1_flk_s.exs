<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription>
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">E:\Anne\SelkupCorpus\flk\KAI_1965_SylchaPylcha1_flk\KAI_1965_SylchaPylcha1_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1186</ud-information>
            <ud-information attribute-name="# HIAT:w">904</ud-information>
            <ud-information attribute-name="# e">900</ud-information>
            <ud-information attribute-name="# HIAT:u">176</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KAI">
            <abbreviation>KAI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
         <tli id="T318" />
         <tli id="T319" />
         <tli id="T320" />
         <tli id="T321" />
         <tli id="T322" />
         <tli id="T323" />
         <tli id="T324" />
         <tli id="T325" />
         <tli id="T326" />
         <tli id="T327" />
         <tli id="T328" />
         <tli id="T329" />
         <tli id="T330" />
         <tli id="T331" />
         <tli id="T332" />
         <tli id="T333" />
         <tli id="T334" />
         <tli id="T335" />
         <tli id="T336" />
         <tli id="T337" />
         <tli id="T338" />
         <tli id="T339" />
         <tli id="T340" />
         <tli id="T341" />
         <tli id="T342" />
         <tli id="T343" />
         <tli id="T344" />
         <tli id="T345" />
         <tli id="T346" />
         <tli id="T347" />
         <tli id="T348" />
         <tli id="T349" />
         <tli id="T350" />
         <tli id="T351" />
         <tli id="T352" />
         <tli id="T353" />
         <tli id="T354" />
         <tli id="T355" />
         <tli id="T356" />
         <tli id="T357" />
         <tli id="T358" />
         <tli id="T359" />
         <tli id="T360" />
         <tli id="T361" />
         <tli id="T362" />
         <tli id="T363" />
         <tli id="T364" />
         <tli id="T365" />
         <tli id="T366" />
         <tli id="T367" />
         <tli id="T368" />
         <tli id="T369" />
         <tli id="T370" />
         <tli id="T371" />
         <tli id="T372" />
         <tli id="T373" />
         <tli id="T374" />
         <tli id="T375" />
         <tli id="T376" />
         <tli id="T377" />
         <tli id="T378" />
         <tli id="T379" />
         <tli id="T380" />
         <tli id="T381" />
         <tli id="T382" />
         <tli id="T383" />
         <tli id="T384" />
         <tli id="T385" />
         <tli id="T386" />
         <tli id="T387" />
         <tli id="T388" />
         <tli id="T389" />
         <tli id="T390" />
         <tli id="T391" />
         <tli id="T392" />
         <tli id="T393" />
         <tli id="T394" />
         <tli id="T395" />
         <tli id="T396" />
         <tli id="T397" />
         <tli id="T398" />
         <tli id="T399" />
         <tli id="T400" />
         <tli id="T401" />
         <tli id="T402" />
         <tli id="T403" />
         <tli id="T404" />
         <tli id="T405" />
         <tli id="T406" />
         <tli id="T407" />
         <tli id="T408" />
         <tli id="T409" />
         <tli id="T410" />
         <tli id="T411" />
         <tli id="T412" />
         <tli id="T413" />
         <tli id="T414" />
         <tli id="T415" />
         <tli id="T416" />
         <tli id="T417" />
         <tli id="T418" />
         <tli id="T419" />
         <tli id="T420" />
         <tli id="T421" />
         <tli id="T422" />
         <tli id="T423" />
         <tli id="T424" />
         <tli id="T425" />
         <tli id="T426" />
         <tli id="T427" />
         <tli id="T428" />
         <tli id="T429" />
         <tli id="T430" />
         <tli id="T431" />
         <tli id="T432" />
         <tli id="T433" />
         <tli id="T434" />
         <tli id="T435" />
         <tli id="T436" />
         <tli id="T437" />
         <tli id="T438" />
         <tli id="T439" />
         <tli id="T440" />
         <tli id="T441" />
         <tli id="T442" />
         <tli id="T443" />
         <tli id="T444" />
         <tli id="T445" />
         <tli id="T446" />
         <tli id="T447" />
         <tli id="T448" />
         <tli id="T449" />
         <tli id="T450" />
         <tli id="T451" />
         <tli id="T452" />
         <tli id="T453" />
         <tli id="T454" />
         <tli id="T455" />
         <tli id="T456" />
         <tli id="T457" />
         <tli id="T458" />
         <tli id="T459" />
         <tli id="T460" />
         <tli id="T461" />
         <tli id="T462" />
         <tli id="T463" />
         <tli id="T464" />
         <tli id="T465" />
         <tli id="T466" />
         <tli id="T467" />
         <tli id="T468" />
         <tli id="T469" />
         <tli id="T470" />
         <tli id="T471" />
         <tli id="T472" />
         <tli id="T473" />
         <tli id="T474" />
         <tli id="T475" />
         <tli id="T476" />
         <tli id="T477" />
         <tli id="T478" />
         <tli id="T479" />
         <tli id="T480" />
         <tli id="T481" />
         <tli id="T482" />
         <tli id="T483" />
         <tli id="T484" />
         <tli id="T485" />
         <tli id="T486" />
         <tli id="T487" />
         <tli id="T488" />
         <tli id="T489" />
         <tli id="T490" />
         <tli id="T491" />
         <tli id="T492" />
         <tli id="T493" />
         <tli id="T494" />
         <tli id="T495" />
         <tli id="T496" />
         <tli id="T497" />
         <tli id="T498" />
         <tli id="T499" />
         <tli id="T500" />
         <tli id="T501" />
         <tli id="T502" />
         <tli id="T503" />
         <tli id="T504" />
         <tli id="T505" />
         <tli id="T506" />
         <tli id="T507" />
         <tli id="T508" />
         <tli id="T509" />
         <tli id="T510" />
         <tli id="T511" />
         <tli id="T512" />
         <tli id="T513" />
         <tli id="T514" />
         <tli id="T515" />
         <tli id="T516" />
         <tli id="T517" />
         <tli id="T518" />
         <tli id="T519" />
         <tli id="T520" />
         <tli id="T521" />
         <tli id="T522" />
         <tli id="T523" />
         <tli id="T524" />
         <tli id="T525" />
         <tli id="T526" />
         <tli id="T527" />
         <tli id="T528" />
         <tli id="T529" />
         <tli id="T530" />
         <tli id="T531" />
         <tli id="T532" />
         <tli id="T533" />
         <tli id="T534" />
         <tli id="T535" />
         <tli id="T536" />
         <tli id="T537" />
         <tli id="T538" />
         <tli id="T539" />
         <tli id="T540" />
         <tli id="T541" />
         <tli id="T542" />
         <tli id="T543" />
         <tli id="T544" />
         <tli id="T545" />
         <tli id="T546" />
         <tli id="T547" />
         <tli id="T548" />
         <tli id="T549" />
         <tli id="T550" />
         <tli id="T551" />
         <tli id="T552" />
         <tli id="T553" />
         <tli id="T554" />
         <tli id="T555" />
         <tli id="T556" />
         <tli id="T557" />
         <tli id="T558" />
         <tli id="T559" />
         <tli id="T560" />
         <tli id="T561" />
         <tli id="T562" />
         <tli id="T563" />
         <tli id="T564" />
         <tli id="T565" />
         <tli id="T566" />
         <tli id="T567" />
         <tli id="T568" />
         <tli id="T569" />
         <tli id="T570" />
         <tli id="T571" />
         <tli id="T572" />
         <tli id="T573" />
         <tli id="T574" />
         <tli id="T575" />
         <tli id="T576" />
         <tli id="T577" />
         <tli id="T578" />
         <tli id="T579" />
         <tli id="T580" />
         <tli id="T581" />
         <tli id="T582" />
         <tli id="T583" />
         <tli id="T584" />
         <tli id="T585" />
         <tli id="T586" />
         <tli id="T587" />
         <tli id="T588" />
         <tli id="T589" />
         <tli id="T590" />
         <tli id="T591" />
         <tli id="T592" />
         <tli id="T593" />
         <tli id="T594" />
         <tli id="T595" />
         <tli id="T596" />
         <tli id="T597" />
         <tli id="T598" />
         <tli id="T599" />
         <tli id="T600" />
         <tli id="T601" />
         <tli id="T602" />
         <tli id="T603" />
         <tli id="T604" />
         <tli id="T605" />
         <tli id="T606" />
         <tli id="T607" />
         <tli id="T608" />
         <tli id="T609" />
         <tli id="T610" />
         <tli id="T611" />
         <tli id="T612" />
         <tli id="T613" />
         <tli id="T614" />
         <tli id="T615" />
         <tli id="T616" />
         <tli id="T617" />
         <tli id="T618" />
         <tli id="T619" />
         <tli id="T620" />
         <tli id="T621" />
         <tli id="T622" />
         <tli id="T623" />
         <tli id="T624" />
         <tli id="T625" />
         <tli id="T626" />
         <tli id="T627" />
         <tli id="T628" />
         <tli id="T629" />
         <tli id="T630" />
         <tli id="T631" />
         <tli id="T632" />
         <tli id="T633" />
         <tli id="T634" />
         <tli id="T635" />
         <tli id="T636" />
         <tli id="T637" />
         <tli id="T638" />
         <tli id="T639" />
         <tli id="T640" />
         <tli id="T641" />
         <tli id="T642" />
         <tli id="T643" />
         <tli id="T644" />
         <tli id="T645" />
         <tli id="T646" />
         <tli id="T647" />
         <tli id="T648" />
         <tli id="T649" />
         <tli id="T650" />
         <tli id="T651" />
         <tli id="T652" />
         <tli id="T653" />
         <tli id="T654" />
         <tli id="T655" />
         <tli id="T656" />
         <tli id="T657" />
         <tli id="T658" />
         <tli id="T659" />
         <tli id="T660" />
         <tli id="T661" />
         <tli id="T662" />
         <tli id="T663" />
         <tli id="T664" />
         <tli id="T665" />
         <tli id="T666" />
         <tli id="T667" />
         <tli id="T668" />
         <tli id="T669" />
         <tli id="T670" />
         <tli id="T671" />
         <tli id="T672" />
         <tli id="T673" />
         <tli id="T674" />
         <tli id="T675" />
         <tli id="T676" />
         <tli id="T677" />
         <tli id="T678" />
         <tli id="T679" />
         <tli id="T680" />
         <tli id="T681" />
         <tli id="T682" />
         <tli id="T683" />
         <tli id="T684" />
         <tli id="T685" />
         <tli id="T686" />
         <tli id="T687" />
         <tli id="T688" />
         <tli id="T689" />
         <tli id="T690" />
         <tli id="T691" />
         <tli id="T692" />
         <tli id="T693" />
         <tli id="T694" />
         <tli id="T695" />
         <tli id="T696" />
         <tli id="T697" />
         <tli id="T698" />
         <tli id="T699" />
         <tli id="T700" />
         <tli id="T701" />
         <tli id="T702" />
         <tli id="T703" />
         <tli id="T704" />
         <tli id="T705" />
         <tli id="T706" />
         <tli id="T707" />
         <tli id="T708" />
         <tli id="T709" />
         <tli id="T710" />
         <tli id="T711" />
         <tli id="T712" />
         <tli id="T713" />
         <tli id="T714" />
         <tli id="T715" />
         <tli id="T716" />
         <tli id="T717" />
         <tli id="T718" />
         <tli id="T719" />
         <tli id="T720" />
         <tli id="T721" />
         <tli id="T722" />
         <tli id="T723" />
         <tli id="T724" />
         <tli id="T725" />
         <tli id="T726" />
         <tli id="T727" />
         <tli id="T728" />
         <tli id="T729" />
         <tli id="T730" />
         <tli id="T731" />
         <tli id="T732" />
         <tli id="T733" />
         <tli id="T734" />
         <tli id="T735" />
         <tli id="T736" />
         <tli id="T737" />
         <tli id="T738" />
         <tli id="T739" />
         <tli id="T740" />
         <tli id="T741" />
         <tli id="T742" />
         <tli id="T743" />
         <tli id="T744" />
         <tli id="T745" />
         <tli id="T746" />
         <tli id="T747" />
         <tli id="T748" />
         <tli id="T749" />
         <tli id="T750" />
         <tli id="T751" />
         <tli id="T752" />
         <tli id="T753" />
         <tli id="T754" />
         <tli id="T755" />
         <tli id="T756" />
         <tli id="T757" />
         <tli id="T758" />
         <tli id="T759" />
         <tli id="T760" />
         <tli id="T761" />
         <tli id="T762" />
         <tli id="T763" />
         <tli id="T764" />
         <tli id="T765" />
         <tli id="T766" />
         <tli id="T767" />
         <tli id="T768" />
         <tli id="T769" />
         <tli id="T770" />
         <tli id="T771" />
         <tli id="T772" />
         <tli id="T773" />
         <tli id="T774" />
         <tli id="T775" />
         <tli id="T776" />
         <tli id="T777" />
         <tli id="T778" />
         <tli id="T779" />
         <tli id="T780" />
         <tli id="T781" />
         <tli id="T782" />
         <tli id="T783" />
         <tli id="T784" />
         <tli id="T785" />
         <tli id="T786" />
         <tli id="T787" />
         <tli id="T788" />
         <tli id="T789" />
         <tli id="T790" />
         <tli id="T791" />
         <tli id="T792" />
         <tli id="T793" />
         <tli id="T794" />
         <tli id="T795" />
         <tli id="T796" />
         <tli id="T797" />
         <tli id="T798" />
         <tli id="T799" />
         <tli id="T800" />
         <tli id="T801" />
         <tli id="T802" />
         <tli id="T803" />
         <tli id="T804" />
         <tli id="T805" />
         <tli id="T806" />
         <tli id="T807" />
         <tli id="T808" />
         <tli id="T809" />
         <tli id="T810" />
         <tli id="T811" />
         <tli id="T812" />
         <tli id="T813" />
         <tli id="T814" />
         <tli id="T815" />
         <tli id="T816" />
         <tli id="T817" />
         <tli id="T818" />
         <tli id="T819" />
         <tli id="T820" />
         <tli id="T821" />
         <tli id="T822" />
         <tli id="T823" />
         <tli id="T824" />
         <tli id="T825" />
         <tli id="T826" />
         <tli id="T827" />
         <tli id="T828" />
         <tli id="T829" />
         <tli id="T830" />
         <tli id="T831" />
         <tli id="T832" />
         <tli id="T833" />
         <tli id="T834" />
         <tli id="T835" />
         <tli id="T836" />
         <tli id="T837" />
         <tli id="T838" />
         <tli id="T839" />
         <tli id="T840" />
         <tli id="T841" />
         <tli id="T842" />
         <tli id="T843" />
         <tli id="T844" />
         <tli id="T845" />
         <tli id="T846" />
         <tli id="T847" />
         <tli id="T848" />
         <tli id="T849" />
         <tli id="T850" />
         <tli id="T851" />
         <tli id="T852" />
         <tli id="T853" />
         <tli id="T854" />
         <tli id="T855" />
         <tli id="T856" />
         <tli id="T857" />
         <tli id="T858" />
         <tli id="T859" />
         <tli id="T860" />
         <tli id="T861" />
         <tli id="T862" />
         <tli id="T863" />
         <tli id="T864" />
         <tli id="T865" />
         <tli id="T866" />
         <tli id="T867" />
         <tli id="T868" />
         <tli id="T869" />
         <tli id="T870" />
         <tli id="T871" />
         <tli id="T872" />
         <tli id="T873" />
         <tli id="T874" />
         <tli id="T875" />
         <tli id="T876" />
         <tli id="T877" />
         <tli id="T878" />
         <tli id="T879" />
         <tli id="T880" />
         <tli id="T881" />
         <tli id="T882" />
         <tli id="T883" />
         <tli id="T884" />
         <tli id="T885" />
         <tli id="T886" />
         <tli id="T887" />
         <tli id="T888" />
         <tli id="T889" />
         <tli id="T890" />
         <tli id="T891" />
         <tli id="T892" />
         <tli id="T893" />
         <tli id="T894" />
         <tli id="T895" />
         <tli id="T896" />
         <tli id="T897" />
         <tli id="T898" />
         <tli id="T899" />
         <tli id="T900" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KAI"
                      type="t">
         <timeline-fork end="T288" start="T287">
            <tli id="T287.tx.1" />
         </timeline-fork>
         <timeline-fork end="T345" start="T344">
            <tli id="T344.tx.1" />
         </timeline-fork>
         <timeline-fork end="T507" start="T506">
            <tli id="T506.tx.1" />
         </timeline-fork>
         <timeline-fork end="T806" start="T805">
            <tli id="T805.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T900" id="Seg_0" n="sc" s="T0">
               <ts e="T1" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Sɨlʼčʼa-Pɨlʼčʼa</ts>
                  <nts id="Seg_5" n="HIAT:ip">.</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T4" id="Seg_8" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_10" n="HIAT:w" s="T1">Sɨlʼčʼa-Pɨlʼčʼa</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">Qəš</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">ilɨmpa</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_20" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_22" n="HIAT:w" s="T4">Əmɨtɨj</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">ɛːppɨntɨ</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_29" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_31" n="HIAT:w" s="T6">Äsɨtɨ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_34" n="HIAT:w" s="T7">čʼaŋkɨmpa</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_38" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_40" n="HIAT:w" s="T8">Ila</ts>
                  <nts id="Seg_41" n="HIAT:ip">,</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_44" n="HIAT:w" s="T9">ila</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_48" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_50" n="HIAT:w" s="T10">Okkɨr</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_53" n="HIAT:w" s="T11">čʼontot</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_56" n="HIAT:w" s="T12">əmɨntɨ</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_59" n="HIAT:w" s="T13">nı</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_62" n="HIAT:w" s="T14">kotot</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_64" n="HIAT:ip">(</nts>
                  <nts id="Seg_65" n="HIAT:ip">/</nts>
                  <ts e="T16" id="Seg_67" n="HIAT:w" s="T15">kətɨt</ts>
                  <nts id="Seg_68" n="HIAT:ip">)</nts>
                  <nts id="Seg_69" n="HIAT:ip">,</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_72" n="HIAT:w" s="T16">əmtɨ</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_75" n="HIAT:w" s="T17">tɨ</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_78" n="HIAT:w" s="T18">nılʼčʼik</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_81" n="HIAT:w" s="T19">ɛsa</ts>
                  <nts id="Seg_82" n="HIAT:ip">:</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_85" n="HIAT:w" s="T20">Mat</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_88" n="HIAT:w" s="T21">qumɨlʼ</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_91" n="HIAT:w" s="T22">peːrla</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_94" n="HIAT:w" s="T23">qənnak</ts>
                  <nts id="Seg_95" n="HIAT:ip">.</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_98" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_100" n="HIAT:w" s="T24">Tılʼčʼa</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_103" n="HIAT:w" s="T25">kuttar</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_106" n="HIAT:w" s="T26">ilantɨmɨt</ts>
                  <nts id="Seg_107" n="HIAT:ip">?</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_110" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_112" n="HIAT:w" s="T27">Ama</ts>
                  <nts id="Seg_113" n="HIAT:ip">,</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_116" n="HIAT:w" s="T28">tan</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_119" n="HIAT:w" s="T29">montɨ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_122" n="HIAT:w" s="T30">qaj</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_125" n="HIAT:w" s="T31">tap</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_128" n="HIAT:w" s="T32">təttɨt</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_131" n="HIAT:w" s="T33">pontarqɨt</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_134" n="HIAT:w" s="T34">qumɨp</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_137" n="HIAT:w" s="T35">aš</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_140" n="HIAT:w" s="T36">tɛnima</ts>
                  <nts id="Seg_141" n="HIAT:ip">?</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_144" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_146" n="HIAT:w" s="T37">Əmɨt</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_149" n="HIAT:w" s="T38">tomnɨtɨ</ts>
                  <nts id="Seg_150" n="HIAT:ip">:</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_153" n="HIAT:w" s="T39">Man</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_156" n="HIAT:w" s="T40">nʼi</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_159" n="HIAT:w" s="T41">qajlʼ</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_162" n="HIAT:w" s="T42">qum</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_165" n="HIAT:w" s="T43">aš</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_168" n="HIAT:w" s="T44">tɛnɨma</ts>
                  <nts id="Seg_169" n="HIAT:ip">.</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_172" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_174" n="HIAT:w" s="T45">Tap</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_177" n="HIAT:w" s="T46">təttɨt</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_180" n="HIAT:w" s="T47">pontarqɨt</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_183" n="HIAT:w" s="T48">tätčʼaqɨt</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_186" n="HIAT:w" s="T49">qup</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_189" n="HIAT:w" s="T50">čʼäŋka</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_193" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_195" n="HIAT:w" s="T51">Ijatɨ</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_198" n="HIAT:w" s="T52">əmɨntɨn</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_201" n="HIAT:w" s="T53">nılʼ</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_204" n="HIAT:w" s="T54">kətɨtä</ts>
                  <nts id="Seg_205" n="HIAT:ip">:</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_208" n="HIAT:w" s="T55">Tan</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">mompa</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_214" n="HIAT:w" s="T57">onät</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_217" n="HIAT:w" s="T58">ilašik</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_221" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_223" n="HIAT:w" s="T59">Man</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_226" n="HIAT:w" s="T60">ompa</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_229" n="HIAT:w" s="T61">qumɨlʼ</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_232" n="HIAT:w" s="T62">peːrila</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_235" n="HIAT:w" s="T63">qəlʼlʼak</ts>
                  <nts id="Seg_236" n="HIAT:ip">.</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_239" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_241" n="HIAT:w" s="T64">Tap</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_244" n="HIAT:w" s="T65">tätɨt</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_247" n="HIAT:w" s="T66">pontarqɨt</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_250" n="HIAT:w" s="T67">Sɨlša-Pɨlša</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_253" n="HIAT:w" s="T68">Qəš</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_256" n="HIAT:w" s="T69">nɨːnɨ</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_259" n="HIAT:w" s="T70">qənna</ts>
                  <nts id="Seg_260" n="HIAT:ip">.</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_263" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_265" n="HIAT:w" s="T71">Sɨlša-Pɨlša</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_268" n="HIAT:w" s="T72">Qəš</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_271" n="HIAT:w" s="T73">našak</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_274" n="HIAT:w" s="T74">qəntɨna</ts>
                  <nts id="Seg_275" n="HIAT:ip">.</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_278" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_280" n="HIAT:w" s="T75">Kəŋka</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_283" n="HIAT:w" s="T76">aj</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_286" n="HIAT:w" s="T77">taŋɨmka</ts>
                  <nts id="Seg_287" n="HIAT:ip">.</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_290" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_292" n="HIAT:w" s="T78">Qoltɨp</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_295" n="HIAT:w" s="T79">ka</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_298" n="HIAT:w" s="T80">tulʼtɨmpɨka</ts>
                  <nts id="Seg_299" n="HIAT:ip">,</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_302" n="HIAT:w" s="T81">kɨp</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_305" n="HIAT:w" s="T82">kä</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_308" n="HIAT:w" s="T83">tulʼtɨmpɨka</ts>
                  <nts id="Seg_309" n="HIAT:ip">.</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_312" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_314" n="HIAT:w" s="T84">Nʼikakoj</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_317" n="HIAT:w" s="T85">ni</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_320" n="HIAT:w" s="T86">qailʼ</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_323" n="HIAT:w" s="T87">qup</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_326" n="HIAT:w" s="T88">čʼäŋka</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_330" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_332" n="HIAT:w" s="T89">To</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_335" n="HIAT:w" s="T90">toːlʼ</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_338" n="HIAT:w" s="T91">mɨqɨt</ts>
                  <nts id="Seg_339" n="HIAT:ip">,</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_342" n="HIAT:w" s="T92">ket</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_345" n="HIAT:w" s="T93">qənka</ts>
                  <nts id="Seg_346" n="HIAT:ip">.</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_349" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_351" n="HIAT:w" s="T94">Ükontuqo</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_354" n="HIAT:w" s="T95">qɔːtqet</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_357" n="HIAT:w" s="T96">sɨrɨčʼčʼika</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_360" n="HIAT:w" s="T97">aj</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_363" n="HIAT:w" s="T98">čʼüːqo</ts>
                  <nts id="Seg_364" n="HIAT:ip">.</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_367" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_369" n="HIAT:w" s="T99">Ukkur</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_372" n="HIAT:w" s="T100">čʼontoːqɨn</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_375" n="HIAT:w" s="T101">tulunʼnʼa</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_378" n="HIAT:w" s="T102">mačʼonta</ts>
                  <nts id="Seg_379" n="HIAT:ip">,</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_382" n="HIAT:w" s="T103">marqɨl</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_385" n="HIAT:w" s="T104">mačʼe</ts>
                  <nts id="Seg_386" n="HIAT:ip">.</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_389" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_391" n="HIAT:w" s="T105">Na</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_394" n="HIAT:w" s="T106">mačʼoːmɨt</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_397" n="HIAT:w" s="T107">kɨ</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_400" n="HIAT:w" s="T108">tannɨmmɨnta</ts>
                  <nts id="Seg_401" n="HIAT:ip">.</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_404" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_406" n="HIAT:w" s="T109">Na</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_409" n="HIAT:w" s="T110">kɨt</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_412" n="HIAT:w" s="T111">qanɨŋmɨt</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_415" n="HIAT:w" s="T112">qälʼimpak</ts>
                  <nts id="Seg_416" n="HIAT:ip">.</nts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_419" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_421" n="HIAT:w" s="T113">Ukkur</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_424" n="HIAT:w" s="T114">čʼontoːqɨt</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_427" n="HIAT:w" s="T115">qos</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_430" n="HIAT:w" s="T116">qaj</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_433" n="HIAT:w" s="T117">ünnänta</ts>
                  <nts id="Seg_434" n="HIAT:ip">:</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_437" n="HIAT:w" s="T118">A</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_440" n="HIAT:w" s="T119">buh</ts>
                  <nts id="Seg_441" n="HIAT:ip">,</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_444" n="HIAT:w" s="T120">a</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_447" n="HIAT:w" s="T121">buh</ts>
                  <nts id="Seg_448" n="HIAT:ip">!</nts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_451" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_453" n="HIAT:w" s="T122">Qälʼimpa</ts>
                  <nts id="Seg_454" n="HIAT:ip">,</nts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_457" n="HIAT:w" s="T123">nılʼtij</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_460" n="HIAT:w" s="T124">čʼarɨ</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_463" n="HIAT:w" s="T125">üntɨnit</ts>
                  <nts id="Seg_464" n="HIAT:ip">.</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_467" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_469" n="HIAT:w" s="T126">Nʼi</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_472" n="HIAT:w" s="T127">qaj</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_475" n="HIAT:w" s="T128">suːrɨm</ts>
                  <nts id="Seg_476" n="HIAT:ip">,</nts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_479" n="HIAT:w" s="T129">nʼi</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_482" n="HIAT:w" s="T130">qaj</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_485" n="HIAT:w" s="T131">čʼäŋka</ts>
                  <nts id="Seg_486" n="HIAT:ip">.</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_489" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_491" n="HIAT:w" s="T132">Na</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_494" n="HIAT:w" s="T133">tättɨp</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_497" n="HIAT:w" s="T134">pontar</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_500" n="HIAT:w" s="T135">kolʼaltɨptäːqak</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_503" n="HIAT:w" s="T136">kusa</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_506" n="HIAT:w" s="T137">qaj</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_509" n="HIAT:w" s="T138">orɨčʼčʼe</ts>
                  <nts id="Seg_510" n="HIAT:ip">?</nts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_513" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_515" n="HIAT:w" s="T139">Mat</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_518" n="HIAT:w" s="T140">hot</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_521" n="HIAT:w" s="T141">mannɨmpɨsan</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_524" n="HIAT:w" s="T142">ɛna</ts>
                  <nts id="Seg_525" n="HIAT:ip">.</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_528" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_530" n="HIAT:w" s="T143">Na</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_533" n="HIAT:w" s="T144">tünnent</ts>
                  <nts id="Seg_534" n="HIAT:ip">,</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_537" n="HIAT:w" s="T145">na</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_540" n="HIAT:w" s="T146">tünnenta</ts>
                  <nts id="Seg_541" n="HIAT:ip">.</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_544" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_546" n="HIAT:w" s="T147">Ɨːrɨk</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_549" n="HIAT:w" s="T148">mit</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_552" n="HIAT:w" s="T149">olʼa</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_555" n="HIAT:w" s="T150">kɨ</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_558" n="HIAT:w" s="T151">šinčʼoːqɨt</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_561" n="HIAT:w" s="T152">takkɨt</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_564" n="HIAT:w" s="T153">ünta</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_567" n="HIAT:w" s="T154">aj</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_570" n="HIAT:w" s="T155">kɛn</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_573" n="HIAT:w" s="T156">na</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_576" n="HIAT:w" s="T157">laŋkɨčʼčʼenta</ts>
                  <nts id="Seg_577" n="HIAT:ip">:</nts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_580" n="HIAT:w" s="T158">A</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_583" n="HIAT:w" s="T159">buh</ts>
                  <nts id="Seg_584" n="HIAT:ip">,</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_587" n="HIAT:w" s="T160">a</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_590" n="HIAT:w" s="T161">buh</ts>
                  <nts id="Seg_591" n="HIAT:ip">!</nts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_594" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_596" n="HIAT:w" s="T162">Nılʼti</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_599" n="HIAT:w" s="T163">čʼarɨtɨ</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_602" n="HIAT:w" s="T164">qalʼ</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_605" n="HIAT:w" s="T165">montɨ</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_608" n="HIAT:w" s="T166">tɛːŋŋɨrna</ts>
                  <nts id="Seg_609" n="HIAT:ip">.</nts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_612" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_614" n="HIAT:w" s="T167">Na</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_617" n="HIAT:w" s="T168">tüntanɨɨɨ</ts>
                  <nts id="Seg_618" n="HIAT:ip">.</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_621" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_623" n="HIAT:w" s="T169">Takkɨt</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_626" n="HIAT:w" s="T170">kɨt</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_629" n="HIAT:w" s="T171">kɨːqat</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_632" n="HIAT:w" s="T172">qos</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_635" n="HIAT:w" s="T173">qaj</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_638" n="HIAT:w" s="T174">säːqiɨmɨt</ts>
                  <nts id="Seg_639" n="HIAT:ip">.</nts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_642" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_644" n="HIAT:w" s="T175">Na</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_647" n="HIAT:w" s="T176">majnilʼ</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_650" n="HIAT:w" s="T177">laŋkɨnʼä</ts>
                  <nts id="Seg_651" n="HIAT:ip">:</nts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_654" n="HIAT:w" s="T178">Aaa</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_657" n="HIAT:w" s="T179">buh</ts>
                  <nts id="Seg_658" n="HIAT:ip">,</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_661" n="HIAT:w" s="T180">aaa</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_664" n="HIAT:w" s="T181">buh</ts>
                  <nts id="Seg_665" n="HIAT:ip">!</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_668" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_670" n="HIAT:w" s="T182">Aša</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_673" n="HIAT:w" s="T183">ni</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_676" n="HIAT:w" s="T184">qumɨt</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_679" n="HIAT:w" s="T185">čʼarɨ</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_682" n="HIAT:w" s="T186">ola</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_685" n="HIAT:w" s="T187">nı</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_688" n="HIAT:w" s="T188">laŋkɨnʼnʼä</ts>
                  <nts id="Seg_689" n="HIAT:ip">.</nts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_692" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_694" n="HIAT:w" s="T189">Nɨː</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_697" n="HIAT:w" s="T190">na</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_700" n="HIAT:w" s="T191">tünta</ts>
                  <nts id="Seg_701" n="HIAT:ip">.</nts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_704" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_706" n="HIAT:w" s="T192">Montə</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_709" n="HIAT:w" s="T193">nılʼčʼik</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_712" n="HIAT:w" s="T194">qum</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_715" n="HIAT:w" s="T195">laŋkɨs</ts>
                  <nts id="Seg_716" n="HIAT:ip">.</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_719" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_721" n="HIAT:w" s="T196">Nɨː</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_724" n="HIAT:w" s="T197">čʼap</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_727" n="HIAT:w" s="T198">tünta</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_730" n="HIAT:w" s="T199">montɨla</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_733" n="HIAT:w" s="T200">qup</ts>
                  <nts id="Seg_734" n="HIAT:ip">,</nts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_737" n="HIAT:w" s="T201">montɨ</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_740" n="HIAT:w" s="T202">qup</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_743" n="HIAT:w" s="T203">tam</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_746" n="HIAT:w" s="T204">äsantɨ</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_749" n="HIAT:w" s="T205">ämantɨ</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_752" n="HIAT:w" s="T206">mäčʼisä</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_755" n="HIAT:w" s="T207">nʼäŋɨčʼa</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_758" n="HIAT:w" s="T208">qup</ts>
                  <nts id="Seg_759" n="HIAT:ip">.</nts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_762" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_764" n="HIAT:w" s="T209">Tap</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_767" n="HIAT:w" s="T210">əsɨntɨ</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_770" n="HIAT:w" s="T211">əmɨntɨ</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_773" n="HIAT:w" s="T212">wäčʼisä</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_776" n="HIAT:w" s="T213">nʼäŋɨčʼa</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_779" n="HIAT:w" s="T214">ɔːmta</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_782" n="HIAT:w" s="T215">na</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_785" n="HIAT:w" s="T216">qäqɨn</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_788" n="HIAT:w" s="T217">ɔːqqɨt</ts>
                  <nts id="Seg_789" n="HIAT:ip">.</nts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_792" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_794" n="HIAT:w" s="T218">Na</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_797" n="HIAT:w" s="T219">qaj</ts>
                  <nts id="Seg_798" n="HIAT:ip">,</nts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_801" n="HIAT:w" s="T220">na</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_804" n="HIAT:w" s="T221">qup</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_807" n="HIAT:w" s="T222">laŋkɨšmɨnta</ts>
                  <nts id="Seg_808" n="HIAT:ip">?</nts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_811" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_813" n="HIAT:w" s="T223">Tat</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_816" n="HIAT:w" s="T224">qaː</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_819" n="HIAT:w" s="T225">nıj</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_822" n="HIAT:w" s="T226">ɔːmnant</ts>
                  <nts id="Seg_823" n="HIAT:ip">?</nts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_826" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_828" n="HIAT:w" s="T227">–Aša</ts>
                  <nts id="Seg_829" n="HIAT:ip">,</nts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_832" n="HIAT:w" s="T228">mumpa</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_835" n="HIAT:w" s="T229">mat</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_838" n="HIAT:w" s="T230">loːsa</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_841" n="HIAT:w" s="T231">šım</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_844" n="HIAT:w" s="T232">amqo</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_847" n="HIAT:w" s="T233">ɔːmtak</ts>
                  <nts id="Seg_848" n="HIAT:ip">.</nts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_851" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_853" n="HIAT:w" s="T234">Konnäqɨt</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_856" n="HIAT:w" s="T235">qumiːmɨ</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_859" n="HIAT:w" s="T236">ɛːŋɔːtɨt</ts>
                  <nts id="Seg_860" n="HIAT:ip">.</nts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_863" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_865" n="HIAT:w" s="T237">Nammɨt</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_868" n="HIAT:w" s="T238">šıp</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_871" n="HIAT:w" s="T239">ontalimpɔːtät</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_874" n="HIAT:w" s="T240">loːš</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_877" n="HIAT:w" s="T241">šim</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_880" n="HIAT:w" s="T242">amqo</ts>
                  <nts id="Seg_881" n="HIAT:ip">.</nts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T255" id="Seg_884" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_886" n="HIAT:w" s="T243">Olqa</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_889" n="HIAT:w" s="T244">mat</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_892" n="HIAT:w" s="T245">konna</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_895" n="HIAT:w" s="T246">tannɛntak</ts>
                  <nts id="Seg_896" n="HIAT:ip">,</nts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_899" n="HIAT:w" s="T247">loːs</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_902" n="HIAT:w" s="T248">muntɨk</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_905" n="HIAT:w" s="T249">šımɨt</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_908" n="HIAT:w" s="T250">amta</ts>
                  <nts id="Seg_909" n="HIAT:ip">,</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_912" n="HIAT:w" s="T251">ne</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_915" n="HIAT:w" s="T252">qaim</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_918" n="HIAT:w" s="T253">naš</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_921" n="HIAT:w" s="T254">qaläta</ts>
                  <nts id="Seg_922" n="HIAT:ip">.</nts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_925" n="HIAT:u" s="T255">
                  <ts e="T256" id="Seg_927" n="HIAT:w" s="T255">Na</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_930" n="HIAT:w" s="T256">qaː</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_933" n="HIAT:w" s="T257">šım</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_936" n="HIAT:w" s="T258">omtɨlʼimpɔːtet</ts>
                  <nts id="Seg_937" n="HIAT:ip">?</nts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_940" n="HIAT:u" s="T259">
                  <ts e="T260" id="Seg_942" n="HIAT:w" s="T259">–Tat</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_945" n="HIAT:w" s="T260">konnä</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_948" n="HIAT:w" s="T261">tantɨš</ts>
                  <nts id="Seg_949" n="HIAT:ip">.</nts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_952" n="HIAT:u" s="T262">
                  <ts e="T263" id="Seg_954" n="HIAT:w" s="T262">–Tan</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_957" n="HIAT:w" s="T263">nej</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_960" n="HIAT:w" s="T264">konna</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_963" n="HIAT:w" s="T265">tantaš</ts>
                  <nts id="Seg_964" n="HIAT:ip">.</nts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T269" id="Seg_967" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_969" n="HIAT:w" s="T266">Mat</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_972" n="HIAT:w" s="T267">kutar</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_975" n="HIAT:w" s="T268">tannɛntak</ts>
                  <nts id="Seg_976" n="HIAT:ip">?</nts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_979" n="HIAT:u" s="T269">
                  <ts e="T270" id="Seg_981" n="HIAT:w" s="T269">Qumɨt</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_984" n="HIAT:w" s="T270">šıp</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_987" n="HIAT:w" s="T271">qolʼčʼantɔːtɨt</ts>
                  <nts id="Seg_988" n="HIAT:ip">.</nts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_991" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_993" n="HIAT:w" s="T272">–Mat</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_996" n="HIAT:w" s="T273">tomnap</ts>
                  <nts id="Seg_997" n="HIAT:ip">,</nts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1000" n="HIAT:w" s="T274">tat</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1003" n="HIAT:w" s="T275">kon</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1006" n="HIAT:w" s="T276">tantaš</ts>
                  <nts id="Seg_1007" n="HIAT:ip">.</nts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_1010" n="HIAT:u" s="T277">
                  <ts e="T278" id="Seg_1012" n="HIAT:w" s="T277">Konna</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1015" n="HIAT:w" s="T278">na</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1018" n="HIAT:w" s="T279">tannɨntɔːqaj</ts>
                  <nts id="Seg_1019" n="HIAT:ip">.</nts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1022" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_1024" n="HIAT:w" s="T280">Sɨlʼše-Pɨlʼšet</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1027" n="HIAT:w" s="T281">Qəš</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1030" n="HIAT:w" s="T282">pɔːrkä</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1033" n="HIAT:w" s="T283">na</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1036" n="HIAT:w" s="T284">meːlʼčʼintɨtɨ</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1039" n="HIAT:w" s="T285">täːqantɨsa</ts>
                  <nts id="Seg_1040" n="HIAT:ip">.</nts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1043" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_1045" n="HIAT:w" s="T286">Tan</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287.tx.1" id="Seg_1048" n="HIAT:w" s="T287">na</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1051" n="HIAT:w" s="T287.tx.1">pa</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1054" n="HIAT:w" s="T288">mašıp</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1057" n="HIAT:w" s="T289">kušan</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1060" n="HIAT:w" s="T290">ɨk</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1063" n="HIAT:w" s="T291">šıp</ts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1066" n="HIAT:w" s="T292">kətašik</ts>
                  <nts id="Seg_1067" n="HIAT:ip">!</nts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_1070" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1072" n="HIAT:w" s="T293">Loːs</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1075" n="HIAT:w" s="T294">taštɨ</ts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1078" n="HIAT:w" s="T295">na</ts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1081" n="HIAT:w" s="T296">soqɨčʼɛnnɨnta</ts>
                  <nts id="Seg_1082" n="HIAT:ip">,</nts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1085" n="HIAT:w" s="T297">Sɨlʼčʼa-Pɨlʼčʼat</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1088" n="HIAT:w" s="T298">Qəš</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1091" n="HIAT:w" s="T299">kučʼa</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1094" n="HIAT:w" s="T300">qatɨsä</ts>
                  <nts id="Seg_1095" n="HIAT:ip">.</nts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_1098" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1100" n="HIAT:w" s="T301">Mašıp</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1103" n="HIAT:w" s="T302">kušenna</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1106" n="HIAT:w" s="T303">šıp</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1109" n="HIAT:w" s="T304">kətašik</ts>
                  <nts id="Seg_1110" n="HIAT:ip">.</nts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_1113" n="HIAT:u" s="T305">
                  <ts e="T306" id="Seg_1115" n="HIAT:w" s="T305">Nılʼ</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1118" n="HIAT:w" s="T306">äːkɨlʼčʼimpata</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1121" n="HIAT:w" s="T307">täːqasä</ts>
                  <nts id="Seg_1122" n="HIAT:ip">.</nts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T315" id="Seg_1125" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_1127" n="HIAT:w" s="T308">Konna</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1130" n="HIAT:w" s="T309">na</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1133" n="HIAT:w" s="T310">qənta</ts>
                  <nts id="Seg_1134" n="HIAT:ip">,</nts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1137" n="HIAT:w" s="T311">nɨː</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1140" n="HIAT:w" s="T312">mont</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1143" n="HIAT:w" s="T313">čʼulʼ</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1146" n="HIAT:w" s="T314">mɔːt</ts>
                  <nts id="Seg_1147" n="HIAT:ip">.</nts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T322" id="Seg_1150" n="HIAT:u" s="T315">
                  <ts e="T316" id="Seg_1152" n="HIAT:w" s="T315">Naqät</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1155" n="HIAT:w" s="T316">mɔːtan</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1158" n="HIAT:w" s="T317">ɔːqqɨt</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1161" n="HIAT:w" s="T318">aj</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1164" n="HIAT:w" s="T319">pɔːrkä</ts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1167" n="HIAT:w" s="T320">na</ts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1170" n="HIAT:w" s="T321">meːntɨtɨ</ts>
                  <nts id="Seg_1171" n="HIAT:ip">.</nts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T324" id="Seg_1174" n="HIAT:u" s="T322">
                  <ts e="T323" id="Seg_1176" n="HIAT:w" s="T322">Mɔːt</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1179" n="HIAT:w" s="T323">šeːräšik</ts>
                  <nts id="Seg_1180" n="HIAT:ip">.</nts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1183" n="HIAT:u" s="T324">
                  <ts e="T325" id="Seg_1185" n="HIAT:w" s="T324">Qaː</ts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1188" n="HIAT:w" s="T325">nɨŋantɨ</ts>
                  <nts id="Seg_1189" n="HIAT:ip">,</nts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1192" n="HIAT:w" s="T326">qantenint</ts>
                  <nts id="Seg_1193" n="HIAT:ip">?</nts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_1196" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1198" n="HIAT:w" s="T327">–Mat</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1201" n="HIAT:w" s="T328">qäntɨk</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1204" n="HIAT:w" s="T329">šeːrtak</ts>
                  <nts id="Seg_1205" n="HIAT:ip">,</nts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1208" n="HIAT:w" s="T330">qumiːmɨ</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1211" n="HIAT:w" s="T331">šıp</ts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1214" n="HIAT:w" s="T332">qolʼčʼantɔːtɨt</ts>
                  <nts id="Seg_1215" n="HIAT:ip">.</nts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T337" id="Seg_1218" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_1220" n="HIAT:w" s="T333">Tan</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1223" n="HIAT:w" s="T334">ola</ts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1226" n="HIAT:w" s="T335">mɔːt</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1229" n="HIAT:w" s="T336">šeːräšek</ts>
                  <nts id="Seg_1230" n="HIAT:ip">.</nts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T343" id="Seg_1233" n="HIAT:u" s="T337">
                  <ts e="T338" id="Seg_1235" n="HIAT:w" s="T337">Na</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1238" n="HIAT:w" s="T338">mɔːtan</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1241" n="HIAT:w" s="T339">ɔːqqɨt</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1244" n="HIAT:w" s="T340">aj</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1247" n="HIAT:w" s="T341">pɔːrkä</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1250" n="HIAT:w" s="T342">meːntɨt</ts>
                  <nts id="Seg_1251" n="HIAT:ip">.</nts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T349" id="Seg_1254" n="HIAT:u" s="T343">
                  <ts e="T344" id="Seg_1256" n="HIAT:w" s="T343">Tan</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344.tx.1" id="Seg_1259" n="HIAT:w" s="T344">na</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1262" n="HIAT:w" s="T344.tx.1">pa</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1265" n="HIAT:w" s="T345">mašıp</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1268" n="HIAT:w" s="T346">ɨkɨ</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1271" n="HIAT:w" s="T347">šıp</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1274" n="HIAT:w" s="T348">kətäšik</ts>
                  <nts id="Seg_1275" n="HIAT:ip">!</nts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1278" n="HIAT:u" s="T349">
                  <ts e="T350" id="Seg_1280" n="HIAT:w" s="T349">Taːqasä</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1283" n="HIAT:w" s="T350">äːkɨlʼčʼiŋɨtɨ</ts>
                  <nts id="Seg_1284" n="HIAT:ip">.</nts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1287" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1289" n="HIAT:w" s="T351">Mɔːt</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1292" n="HIAT:w" s="T352">na</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1295" n="HIAT:w" s="T353">šeːra</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1298" n="HIAT:w" s="T354">tɨna</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1301" n="HIAT:w" s="T355">qup</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1304" n="HIAT:w" s="T356">əːtɨmɨntɨ</ts>
                  <nts id="Seg_1305" n="HIAT:ip">.</nts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1308" n="HIAT:u" s="T357">
                  <ts e="T358" id="Seg_1310" n="HIAT:w" s="T357">Munta</ts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1313" n="HIAT:w" s="T358">na</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1316" n="HIAT:w" s="T359">qolʼčʼintɔːtɨt</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1319" n="HIAT:w" s="T360">na</ts>
                  <nts id="Seg_1320" n="HIAT:ip">.</nts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T364" id="Seg_1323" n="HIAT:u" s="T361">
                  <ts e="T362" id="Seg_1325" n="HIAT:w" s="T361">Mumpa</ts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1328" n="HIAT:w" s="T362">qaː</ts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1331" n="HIAT:w" s="T363">šeːrna</ts>
                  <nts id="Seg_1332" n="HIAT:ip">?</nts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T370" id="Seg_1335" n="HIAT:u" s="T364">
                  <ts e="T365" id="Seg_1337" n="HIAT:w" s="T364">–Aša</ts>
                  <nts id="Seg_1338" n="HIAT:ip">,</nts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1341" n="HIAT:w" s="T365">mumpa</ts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1344" n="HIAT:w" s="T366">qupti</ts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1347" n="HIAT:w" s="T367">mɔːttɨ</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1350" n="HIAT:w" s="T368">šıp</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1353" n="HIAT:w" s="T369">üːtɨsa</ts>
                  <nts id="Seg_1354" n="HIAT:ip">.</nts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T378" id="Seg_1357" n="HIAT:u" s="T370">
                  <ts e="T371" id="Seg_1359" n="HIAT:w" s="T370">Qos</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1362" n="HIAT:w" s="T371">qajelʼ</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1365" n="HIAT:w" s="T372">qup</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1368" n="HIAT:w" s="T373">tüsa</ts>
                  <nts id="Seg_1369" n="HIAT:ip">,</nts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1372" n="HIAT:w" s="T374">mašıp</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1375" n="HIAT:w" s="T375">mɔːt</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1378" n="HIAT:w" s="T376">šım</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1381" n="HIAT:w" s="T377">üːtäsa</ts>
                  <nts id="Seg_1382" n="HIAT:ip">.</nts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1385" n="HIAT:u" s="T378">
                  <ts e="T379" id="Seg_1387" n="HIAT:w" s="T378">Konnɨ</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1390" n="HIAT:w" s="T379">šep</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1393" n="HIAT:w" s="T380">tattɨraltes</ts>
                  <nts id="Seg_1394" n="HIAT:ip">.</nts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1397" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1399" n="HIAT:w" s="T381">Täp</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1402" n="HIAT:w" s="T382">čʼap</ts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1405" n="HIAT:w" s="T383">qoŋɨtɨ</ts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1408" n="HIAT:w" s="T384">mɔːtat</ts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1411" n="HIAT:w" s="T385">nʼennalʼ</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1414" n="HIAT:w" s="T386">pɛläqqɨt</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1417" n="HIAT:w" s="T387">aj</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1420" n="HIAT:w" s="T388">ukkɨr</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1423" n="HIAT:w" s="T389">nʼäŋɨčʼa</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1426" n="HIAT:w" s="T390">qup</ts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1429" n="HIAT:w" s="T391">qomta</ts>
                  <nts id="Seg_1430" n="HIAT:ip">.</nts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_1433" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1435" n="HIAT:w" s="T392">Nɨːno</ts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1438" n="HIAT:w" s="T393">qa</ts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1441" n="HIAT:w" s="T394">nılʼčʼik</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1444" n="HIAT:w" s="T395">ɛsa</ts>
                  <nts id="Seg_1445" n="HIAT:ip">:</nts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1448" n="HIAT:w" s="T396">Qumɨp</ts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1451" n="HIAT:w" s="T397">ınnä</ts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1454" n="HIAT:w" s="T398">taqtaltɔːtet</ts>
                  <nts id="Seg_1455" n="HIAT:ip">.</nts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T407" id="Seg_1458" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_1460" n="HIAT:w" s="T399">Nɨːnɨ</ts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1463" n="HIAT:w" s="T400">pona</ts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1466" n="HIAT:w" s="T401">tantɨlʼä</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1469" n="HIAT:w" s="T402">üŋkɨltɨkkolʼčʼimpatɨ</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1472" n="HIAT:w" s="T403">loːsɨ</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1475" n="HIAT:w" s="T404">kutɨlʼ</ts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1478" n="HIAT:w" s="T405">mɔːntoːqɨt</ts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1481" n="HIAT:w" s="T406">qompɨška</ts>
                  <nts id="Seg_1482" n="HIAT:ip">.</nts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T412" id="Seg_1485" n="HIAT:u" s="T407">
                  <ts e="T408" id="Seg_1487" n="HIAT:w" s="T407">Loːsa</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1490" n="HIAT:w" s="T408">qata</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1493" n="HIAT:w" s="T409">qompɨšnä</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1496" n="HIAT:w" s="T410">mɔːt</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1499" n="HIAT:w" s="T411">šeːrnɨlɨt</ts>
                  <nts id="Seg_1500" n="HIAT:ip">.</nts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T415" id="Seg_1503" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_1505" n="HIAT:w" s="T412">Ukkɨr</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1508" n="HIAT:w" s="T413">qup</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1511" n="HIAT:w" s="T414">üŋkɨltɨmpɨŋɨjä</ts>
                  <nts id="Seg_1512" n="HIAT:ip">.</nts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T422" id="Seg_1515" n="HIAT:u" s="T415">
                  <ts e="T416" id="Seg_1517" n="HIAT:w" s="T415">Mompa</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1520" n="HIAT:w" s="T416">üːtɨlʼ</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1523" n="HIAT:w" s="T417">kəntɨtɨ</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1526" n="HIAT:w" s="T418">tokkɨčʼčʼenta</ts>
                  <nts id="Seg_1527" n="HIAT:ip">,</nts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1530" n="HIAT:w" s="T419">našat</ts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1533" n="HIAT:w" s="T420">tantɨkkolʼčʼimpa</ts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1535" n="HIAT:ip">(</nts>
                  <nts id="Seg_1536" n="HIAT:ip">/</nts>
                  <ts e="T422" id="Seg_1538" n="HIAT:w" s="T421">qompačʼčʼentɨ</ts>
                  <nts id="Seg_1539" n="HIAT:ip">)</nts>
                  <nts id="Seg_1540" n="HIAT:ip">.</nts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T430" id="Seg_1543" n="HIAT:u" s="T422">
                  <ts e="T423" id="Seg_1545" n="HIAT:w" s="T422">Seːpɨlak</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1548" n="HIAT:w" s="T423">ɛːnta</ts>
                  <nts id="Seg_1549" n="HIAT:ip">,</nts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1552" n="HIAT:w" s="T424">ukkɨr</ts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1555" n="HIAT:w" s="T425">tät</ts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1558" n="HIAT:w" s="T426">čʼontoːqɨt</ts>
                  <nts id="Seg_1559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1561" n="HIAT:w" s="T427">qup</ts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1564" n="HIAT:w" s="T428">na</ts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1567" n="HIAT:w" s="T429">tannɨnta</ts>
                  <nts id="Seg_1568" n="HIAT:ip">.</nts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T434" id="Seg_1571" n="HIAT:u" s="T430">
                  <ts e="T431" id="Seg_1573" n="HIAT:w" s="T430">Mompa</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1576" n="HIAT:w" s="T431">qaret</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1579" n="HIAT:w" s="T432">na</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1582" n="HIAT:w" s="T433">qompɨčʼčʼa</ts>
                  <nts id="Seg_1583" n="HIAT:ip">.</nts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1586" n="HIAT:u" s="T434">
                  <ts e="T435" id="Seg_1588" n="HIAT:w" s="T434">Mɔːttɨ</ts>
                  <nts id="Seg_1589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1591" n="HIAT:w" s="T435">patqɨlna</ts>
                  <nts id="Seg_1592" n="HIAT:ip">.</nts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T439" id="Seg_1595" n="HIAT:u" s="T436">
                  <ts e="T437" id="Seg_1597" n="HIAT:w" s="T436">Loːsɨ</ts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1600" n="HIAT:w" s="T437">na</ts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1603" n="HIAT:w" s="T438">qompɨčʼčʼa</ts>
                  <nts id="Seg_1604" n="HIAT:ip">!</nts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_1607" n="HIAT:u" s="T439">
                  <ts e="T440" id="Seg_1609" n="HIAT:w" s="T439">A</ts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1612" n="HIAT:w" s="T440">mompa</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1615" n="HIAT:w" s="T441">nɔːtə</ts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1618" n="HIAT:w" s="T442">taŋɨŋ</ts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1621" n="HIAT:w" s="T443">ɔːmtɨŋɨlɨt</ts>
                  <nts id="Seg_1622" n="HIAT:ip">,</nts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1625" n="HIAT:w" s="T444">nʼi</ts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1628" n="HIAT:w" s="T445">kušat</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1631" n="HIAT:w" s="T446">ɨkɨ</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1634" n="HIAT:w" s="T447">tantɨŋɨlit</ts>
                  <nts id="Seg_1635" n="HIAT:ip">.</nts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T452" id="Seg_1638" n="HIAT:u" s="T448">
                  <ts e="T449" id="Seg_1640" n="HIAT:w" s="T448">Sɨlʼčʼi-Pɨlʼčʼit</ts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1643" n="HIAT:w" s="T449">Qəš</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1646" n="HIAT:w" s="T450">üŋkultimpetɨ</ts>
                  <nts id="Seg_1647" n="HIAT:ip">,</nts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1650" n="HIAT:w" s="T451">qaj</ts>
                  <nts id="Seg_1651" n="HIAT:ip">?</nts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T456" id="Seg_1654" n="HIAT:u" s="T452">
                  <ts e="T453" id="Seg_1656" n="HIAT:w" s="T452">Konnä</ts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1659" n="HIAT:w" s="T453">na</ts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1662" n="HIAT:w" s="T454">tannɨntɨŋa</ts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1664" n="HIAT:ip">(</nts>
                  <nts id="Seg_1665" n="HIAT:ip">/</nts>
                  <ts e="T456" id="Seg_1667" n="HIAT:w" s="T455">tannɨnta</ts>
                  <nts id="Seg_1668" n="HIAT:ip">)</nts>
                  <nts id="Seg_1669" n="HIAT:ip">.</nts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T458" id="Seg_1672" n="HIAT:u" s="T456">
                  <ts e="T457" id="Seg_1674" n="HIAT:w" s="T456">Peːkap</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1677" n="HIAT:w" s="T457">qolʼčʼitɨ</ts>
                  <nts id="Seg_1678" n="HIAT:ip">.</nts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T461" id="Seg_1681" n="HIAT:u" s="T458">
                  <ts e="T459" id="Seg_1683" n="HIAT:w" s="T458">Peːkä</ts>
                  <nts id="Seg_1684" n="HIAT:ip">:</nts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1687" n="HIAT:w" s="T459">čʼuk</ts>
                  <nts id="Seg_1688" n="HIAT:ip">,</nts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1691" n="HIAT:w" s="T460">čʼuk</ts>
                  <nts id="Seg_1692" n="HIAT:ip">.</nts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T465" id="Seg_1695" n="HIAT:u" s="T461">
                  <ts e="T462" id="Seg_1697" n="HIAT:w" s="T461">Qaj</ts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1700" n="HIAT:w" s="T462">Sɨlša-Pɨlʼša</ts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1703" n="HIAT:w" s="T463">Qəš</ts>
                  <nts id="Seg_1704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1706" n="HIAT:w" s="T464">qattüsa</ts>
                  <nts id="Seg_1707" n="HIAT:ip">?</nts>
                  <nts id="Seg_1708" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T469" id="Seg_1710" n="HIAT:u" s="T465">
                  <ts e="T466" id="Seg_1712" n="HIAT:w" s="T465">–Sɨlša-Pɨlʼša</ts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1715" n="HIAT:w" s="T466">Qəš</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1718" n="HIAT:w" s="T467">tɨntä</ts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1721" n="HIAT:w" s="T468">qəssa</ts>
                  <nts id="Seg_1722" n="HIAT:ip">.</nts>
                  <nts id="Seg_1723" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T474" id="Seg_1725" n="HIAT:u" s="T469">
                  <ts e="T470" id="Seg_1727" n="HIAT:w" s="T469">Konna</ts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1730" n="HIAT:w" s="T470">na</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1733" n="HIAT:w" s="T471">tünta</ts>
                  <nts id="Seg_1734" n="HIAT:ip">,</nts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1737" n="HIAT:w" s="T472">pɔːrkäp</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1740" n="HIAT:w" s="T473">qoŋot</ts>
                  <nts id="Seg_1741" n="HIAT:ip">.</nts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T480" id="Seg_1744" n="HIAT:u" s="T474">
                  <ts e="T475" id="Seg_1746" n="HIAT:w" s="T474">Pɔːrkä</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1749" n="HIAT:w" s="T475">kətätɨ</ts>
                  <nts id="Seg_1750" n="HIAT:ip">:</nts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1753" n="HIAT:w" s="T476">Sɨlʼša-Pɨlʼša</ts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1756" n="HIAT:w" s="T477">Qəš</ts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1759" n="HIAT:w" s="T478">mɔːtqɨn</ts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1762" n="HIAT:w" s="T479">ɔːmta</ts>
                  <nts id="Seg_1763" n="HIAT:ip">.</nts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T487" id="Seg_1766" n="HIAT:u" s="T480">
                  <ts e="T481" id="Seg_1768" n="HIAT:w" s="T480">Loːsɨ</ts>
                  <nts id="Seg_1769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1771" n="HIAT:w" s="T481">karra</ts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1774" n="HIAT:w" s="T482">kolʼimɔːlʼlʼä</ts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1777" n="HIAT:w" s="T483">kurolʼna</ts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1779" n="HIAT:ip">(</nts>
                  <nts id="Seg_1780" n="HIAT:ip">/</nts>
                  <ts e="T485" id="Seg_1782" n="HIAT:w" s="T484">qənna</ts>
                  <nts id="Seg_1783" n="HIAT:ip">)</nts>
                  <nts id="Seg_1784" n="HIAT:ip">,</nts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1787" n="HIAT:w" s="T485">üttɨ</ts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1790" n="HIAT:w" s="T486">alʼčʼa</ts>
                  <nts id="Seg_1791" n="HIAT:ip">.</nts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T490" id="Seg_1794" n="HIAT:u" s="T487">
                  <ts e="T488" id="Seg_1796" n="HIAT:w" s="T487">Mɨta</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1799" n="HIAT:w" s="T488">qur</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1802" n="HIAT:w" s="T489">qäːš</ts>
                  <nts id="Seg_1803" n="HIAT:ip">.</nts>
                  <nts id="Seg_1804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T500" id="Seg_1806" n="HIAT:u" s="T490">
                  <ts e="T491" id="Seg_1808" n="HIAT:w" s="T490">Pona</ts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1811" n="HIAT:w" s="T491">paktɨlʼä</ts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1814" n="HIAT:w" s="T492">tıntena</ts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1817" n="HIAT:w" s="T493">Sɨlʼčʼa-Pɨlʼčʼa</ts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1820" n="HIAT:w" s="T494">Qəš</ts>
                  <nts id="Seg_1821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1823" n="HIAT:w" s="T495">pɔːrkämtɨ</ts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1826" n="HIAT:w" s="T496">pačʼallä</ts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1829" n="HIAT:w" s="T497">sılʼlʼaltätɨ</ts>
                  <nts id="Seg_1830" n="HIAT:ip">,</nts>
                  <nts id="Seg_1831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1833" n="HIAT:w" s="T498">mačʼa</ts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1836" n="HIAT:w" s="T499">qattɔːlnit</ts>
                  <nts id="Seg_1837" n="HIAT:ip">.</nts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T503" id="Seg_1840" n="HIAT:u" s="T500">
                  <ts e="T501" id="Seg_1842" n="HIAT:w" s="T500">Jarɨk</ts>
                  <nts id="Seg_1843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1845" n="HIAT:w" s="T501">pɔːrkäp</ts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1848" n="HIAT:w" s="T502">meːŋetɨ</ts>
                  <nts id="Seg_1849" n="HIAT:ip">.</nts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T513" id="Seg_1852" n="HIAT:u" s="T503">
                  <ts e="T504" id="Seg_1854" n="HIAT:w" s="T503">Čʼarrä</ts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1857" n="HIAT:w" s="T504">äkulʼčʼimpɨŋɨtɨ</ts>
                  <nts id="Seg_1858" n="HIAT:ip">:</nts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1861" n="HIAT:w" s="T505">Tat</ts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506.tx.1" id="Seg_1864" n="HIAT:w" s="T506">na</ts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1867" n="HIAT:w" s="T506.tx.1">pa</ts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1870" n="HIAT:w" s="T507">mašıp</ts>
                  <nts id="Seg_1871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1873" n="HIAT:w" s="T508">nʼi</ts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1876" n="HIAT:w" s="T509">kušat</ts>
                  <nts id="Seg_1877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1878" n="HIAT:ip">(</nts>
                  <nts id="Seg_1879" n="HIAT:ip">/</nts>
                  <ts e="T511" id="Seg_1881" n="HIAT:w" s="T510">kušannɨ</ts>
                  <nts id="Seg_1882" n="HIAT:ip">)</nts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1885" n="HIAT:w" s="T511">ɨkɨ</ts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1888" n="HIAT:w" s="T512">kətašik</ts>
                  <nts id="Seg_1889" n="HIAT:ip">!</nts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T517" id="Seg_1892" n="HIAT:u" s="T513">
                  <ts e="T514" id="Seg_1894" n="HIAT:w" s="T513">Tiː</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1897" n="HIAT:w" s="T514">tantɨkkolʼčʼimpa</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1900" n="HIAT:w" s="T515">qarɨnɨlʼ</ts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1903" n="HIAT:w" s="T516">pit</ts>
                  <nts id="Seg_1904" n="HIAT:ip">.</nts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T522" id="Seg_1907" n="HIAT:u" s="T517">
                  <ts e="T518" id="Seg_1909" n="HIAT:w" s="T517">Nɨːnɨ</ts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1912" n="HIAT:w" s="T518">pintɨ</ts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1915" n="HIAT:w" s="T519">kuntɨ</ts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1918" n="HIAT:w" s="T520">na</ts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1921" n="HIAT:w" s="T521">ɔːmnentɔːtɨt</ts>
                  <nts id="Seg_1922" n="HIAT:ip">.</nts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T531" id="Seg_1925" n="HIAT:u" s="T522">
                  <ts e="T523" id="Seg_1927" n="HIAT:w" s="T522">Qarnɨlʼ</ts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1930" n="HIAT:w" s="T523">pit</ts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1933" n="HIAT:w" s="T524">qumɨp</ts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1936" n="HIAT:w" s="T525">kuralʼtɔːtɨt</ts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1939" n="HIAT:w" s="T526">ponä</ts>
                  <nts id="Seg_1940" n="HIAT:ip">,</nts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1943" n="HIAT:w" s="T527">mannɨmpɔːtät</ts>
                  <nts id="Seg_1944" n="HIAT:ip">,</nts>
                  <nts id="Seg_1945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1947" n="HIAT:w" s="T528">čʼɔːlsä</ts>
                  <nts id="Seg_1948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1950" n="HIAT:w" s="T529">aša</ts>
                  <nts id="Seg_1951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1953" n="HIAT:w" s="T530">tanta</ts>
                  <nts id="Seg_1954" n="HIAT:ip">.</nts>
                  <nts id="Seg_1955" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T535" id="Seg_1957" n="HIAT:u" s="T531">
                  <ts e="T532" id="Seg_1959" n="HIAT:w" s="T531">Sɨlʼčʼa-Pɨlʼčʼa</ts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1962" n="HIAT:w" s="T532">Qəš</ts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1965" n="HIAT:w" s="T533">kuralʼtɔːtɨt</ts>
                  <nts id="Seg_1966" n="HIAT:ip">:</nts>
                  <nts id="Seg_1967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1969" n="HIAT:w" s="T534">Üŋkɨltɨmpätɨ</ts>
                  <nts id="Seg_1970" n="HIAT:ip">.</nts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T543" id="Seg_1973" n="HIAT:u" s="T535">
                  <ts e="T536" id="Seg_1975" n="HIAT:w" s="T535">Seːpɨlak</ts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1978" n="HIAT:w" s="T536">qup</ts>
                  <nts id="Seg_1979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1981" n="HIAT:w" s="T537">na</ts>
                  <nts id="Seg_1982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1984" n="HIAT:w" s="T538">lʼämɨk</ts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1987" n="HIAT:w" s="T539">ɛinta</ts>
                  <nts id="Seg_1988" n="HIAT:ip">,</nts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1991" n="HIAT:w" s="T540">mɔːttə</ts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1994" n="HIAT:w" s="T541">na</ts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1997" n="HIAT:w" s="T542">alʼčʼinta</ts>
                  <nts id="Seg_1998" n="HIAT:ip">.</nts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T547" id="Seg_2001" n="HIAT:u" s="T543">
                  <ts e="T544" id="Seg_2003" n="HIAT:w" s="T543">Mɨtta</ts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2006" n="HIAT:w" s="T544">ukkɨr</ts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2009" n="HIAT:w" s="T545">loːsɨ</ts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2012" n="HIAT:w" s="T546">qompɨšpa</ts>
                  <nts id="Seg_2013" n="HIAT:ip">.</nts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T550" id="Seg_2016" n="HIAT:u" s="T547">
                  <ts e="T548" id="Seg_2018" n="HIAT:w" s="T547">Loːsɨ</ts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2021" n="HIAT:w" s="T548">na</ts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2024" n="HIAT:w" s="T549">qompɨšpa</ts>
                  <nts id="Seg_2025" n="HIAT:ip">.</nts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T554" id="Seg_2028" n="HIAT:u" s="T550">
                  <ts e="T551" id="Seg_2030" n="HIAT:w" s="T550">Qapija</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2033" n="HIAT:w" s="T551">Šilʼša-Palʼša</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2036" n="HIAT:w" s="T552">ontə</ts>
                  <nts id="Seg_2037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2039" n="HIAT:w" s="T553">üŋkɨltimpatɨ</ts>
                  <nts id="Seg_2040" n="HIAT:ip">.</nts>
                  <nts id="Seg_2041" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T557" id="Seg_2043" n="HIAT:u" s="T554">
                  <ts e="T555" id="Seg_2045" n="HIAT:w" s="T554">Na</ts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2048" n="HIAT:w" s="T555">tannɨnta</ts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2051" n="HIAT:w" s="T556">konnä</ts>
                  <nts id="Seg_2052" n="HIAT:ip">.</nts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T563" id="Seg_2055" n="HIAT:u" s="T557">
                  <ts e="T558" id="Seg_2057" n="HIAT:w" s="T557">Kurɨlʼä</ts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2060" n="HIAT:w" s="T558">tap</ts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2063" n="HIAT:w" s="T559">tükkɨnä</ts>
                  <nts id="Seg_2064" n="HIAT:ip">,</nts>
                  <nts id="Seg_2065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2067" n="HIAT:w" s="T560">kurɨlʼä</ts>
                  <nts id="Seg_2068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2070" n="HIAT:w" s="T561">tap</ts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2073" n="HIAT:w" s="T562">tükkɨnä</ts>
                  <nts id="Seg_2074" n="HIAT:ip">.</nts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T568" id="Seg_2077" n="HIAT:u" s="T563">
                  <ts e="T564" id="Seg_2079" n="HIAT:w" s="T563">Na</ts>
                  <nts id="Seg_2080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2082" n="HIAT:w" s="T564">qət</ts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2085" n="HIAT:w" s="T565">pɔːrɨntɨ</ts>
                  <nts id="Seg_2086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2088" n="HIAT:w" s="T566">na</ts>
                  <nts id="Seg_2089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2091" n="HIAT:w" s="T567">tannɨnta</ts>
                  <nts id="Seg_2092" n="HIAT:ip">.</nts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T573" id="Seg_2095" n="HIAT:u" s="T568">
                  <ts e="T569" id="Seg_2097" n="HIAT:w" s="T568">Tɨntäna</ts>
                  <nts id="Seg_2098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2100" n="HIAT:w" s="T569">pɔːrkäntɨ</ts>
                  <nts id="Seg_2101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2103" n="HIAT:w" s="T570">kurɨlʼä</ts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2106" n="HIAT:w" s="T571">na</ts>
                  <nts id="Seg_2107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2109" n="HIAT:w" s="T572">tünta</ts>
                  <nts id="Seg_2110" n="HIAT:ip">.</nts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T577" id="Seg_2113" n="HIAT:u" s="T573">
                  <ts e="T574" id="Seg_2115" n="HIAT:w" s="T573">Mɨta</ts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2118" n="HIAT:w" s="T574">čʼuk</ts>
                  <nts id="Seg_2119" n="HIAT:ip">,</nts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2122" n="HIAT:w" s="T575">čʼuk</ts>
                  <nts id="Seg_2123" n="HIAT:ip">,</nts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2126" n="HIAT:w" s="T576">čʼuk</ts>
                  <nts id="Seg_2127" n="HIAT:ip">.</nts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T584" id="Seg_2130" n="HIAT:u" s="T577">
                  <ts e="T578" id="Seg_2132" n="HIAT:w" s="T577">Qaj</ts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2135" n="HIAT:w" s="T578">Sɨlʼčʼa-Pɨlʼčʼa</ts>
                  <nts id="Seg_2136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2138" n="HIAT:w" s="T579">Qəš</ts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2141" n="HIAT:w" s="T580">qattüje</ts>
                  <nts id="Seg_2142" n="HIAT:ip">,</nts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2145" n="HIAT:w" s="T581">qaj</ts>
                  <nts id="Seg_2146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2148" n="HIAT:w" s="T582">mɔːtqɨn</ts>
                  <nts id="Seg_2149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2151" n="HIAT:w" s="T583">ɛːŋa</ts>
                  <nts id="Seg_2152" n="HIAT:ip">?</nts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T588" id="Seg_2155" n="HIAT:u" s="T584">
                  <ts e="T585" id="Seg_2157" n="HIAT:w" s="T584">–Sɨlʼčʼa-Pɨlʼčʼa</ts>
                  <nts id="Seg_2158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2160" n="HIAT:w" s="T585">Qəš</ts>
                  <nts id="Seg_2161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2163" n="HIAT:w" s="T586">našat</ts>
                  <nts id="Seg_2164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2166" n="HIAT:w" s="T587">qəssa</ts>
                  <nts id="Seg_2167" n="HIAT:ip">.</nts>
                  <nts id="Seg_2168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T593" id="Seg_2170" n="HIAT:u" s="T588">
                  <ts e="T589" id="Seg_2172" n="HIAT:w" s="T588">Tat</ts>
                  <nts id="Seg_2173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2175" n="HIAT:w" s="T589">konna</ts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2178" n="HIAT:w" s="T590">qəlla</ts>
                  <nts id="Seg_2179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2181" n="HIAT:w" s="T591">apsolʼ</ts>
                  <nts id="Seg_2182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2184" n="HIAT:w" s="T592">amtɨ</ts>
                  <nts id="Seg_2185" n="HIAT:ip">.</nts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T602" id="Seg_2188" n="HIAT:u" s="T593">
                  <ts e="T594" id="Seg_2190" n="HIAT:w" s="T593">Toːnna</ts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2193" n="HIAT:w" s="T594">kurɨlʼä</ts>
                  <nts id="Seg_2194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2196" n="HIAT:w" s="T595">na</ts>
                  <nts id="Seg_2197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2199" n="HIAT:w" s="T596">qəntanəəə</ts>
                  <nts id="Seg_2200" n="HIAT:ip">,</nts>
                  <nts id="Seg_2201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2203" n="HIAT:w" s="T597">mɔːtan</ts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2206" n="HIAT:w" s="T598">ɔːkɨlʼ</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2209" n="HIAT:w" s="T599">tüntanəəə</ts>
                  <nts id="Seg_2210" n="HIAT:ip">,</nts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2213" n="HIAT:w" s="T600">pɔːrkäntɨ</ts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2216" n="HIAT:w" s="T601">tüːŋa</ts>
                  <nts id="Seg_2217" n="HIAT:ip">.</nts>
                  <nts id="Seg_2218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T603" id="Seg_2220" n="HIAT:u" s="T602">
                  <ts e="T603" id="Seg_2222" n="HIAT:w" s="T602">Qattüsa</ts>
                  <nts id="Seg_2223" n="HIAT:ip">?</nts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T611" id="Seg_2226" n="HIAT:u" s="T603">
                  <ts e="T604" id="Seg_2228" n="HIAT:w" s="T603">–Sɨlʼčʼa-Pɨlʼčʼa</ts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2231" n="HIAT:w" s="T604">Qəš</ts>
                  <nts id="Seg_2232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2234" n="HIAT:w" s="T605">tep</ts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2237" n="HIAT:w" s="T606">aša</ts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2240" n="HIAT:w" s="T607">täːlʼa</ts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2243" n="HIAT:w" s="T608">našak</ts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2246" n="HIAT:w" s="T609">qəssa</ts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2248" n="HIAT:ip">(</nts>
                  <nts id="Seg_2249" n="HIAT:ip">/</nts>
                  <ts e="T611" id="Seg_2251" n="HIAT:w" s="T610">qəssɨŋa</ts>
                  <nts id="Seg_2252" n="HIAT:ip">)</nts>
                  <nts id="Seg_2253" n="HIAT:ip">.</nts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T615" id="Seg_2256" n="HIAT:u" s="T611">
                  <ts e="T612" id="Seg_2258" n="HIAT:w" s="T611">Mɔːttɨ</ts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2261" n="HIAT:w" s="T612">šeːrlʼa</ts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2264" n="HIAT:w" s="T613">apsolʼ</ts>
                  <nts id="Seg_2265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2267" n="HIAT:w" s="T614">amtɨ</ts>
                  <nts id="Seg_2268" n="HIAT:ip">.</nts>
                  <nts id="Seg_2269" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T624" id="Seg_2271" n="HIAT:u" s="T615">
                  <ts e="T616" id="Seg_2273" n="HIAT:w" s="T615">Ukkɨr</ts>
                  <nts id="Seg_2274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2276" n="HIAT:w" s="T616">čʼontoːqɨt</ts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2279" n="HIAT:w" s="T617">loːsɨ</ts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2282" n="HIAT:w" s="T618">mɔːttɨ</ts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2285" n="HIAT:w" s="T619">na</ts>
                  <nts id="Seg_2286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2288" n="HIAT:w" s="T620">noqɔːlta</ts>
                  <nts id="Seg_2289" n="HIAT:ip">,</nts>
                  <nts id="Seg_2290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2292" n="HIAT:w" s="T621">mɔːttɨ</ts>
                  <nts id="Seg_2293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2295" n="HIAT:w" s="T622">na</ts>
                  <nts id="Seg_2296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2298" n="HIAT:w" s="T623">noqqɔːlʼta</ts>
                  <nts id="Seg_2299" n="HIAT:ip">.</nts>
                  <nts id="Seg_2300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T633" id="Seg_2302" n="HIAT:u" s="T624">
                  <ts e="T625" id="Seg_2304" n="HIAT:w" s="T624">Mɔːttɨ</ts>
                  <nts id="Seg_2305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2307" n="HIAT:w" s="T625">čʼep</ts>
                  <nts id="Seg_2308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2310" n="HIAT:w" s="T626">šeːrna</ts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2313" n="HIAT:w" s="T627">loːsɨ</ts>
                  <nts id="Seg_2314" n="HIAT:ip">,</nts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2317" n="HIAT:w" s="T628">Sɨlʼčʼa-Pɨlʼčʼa</ts>
                  <nts id="Seg_2318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2320" n="HIAT:w" s="T629">Qəš</ts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2323" n="HIAT:w" s="T630">počʼčʼalʼnɨtɨ</ts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2326" n="HIAT:w" s="T631">olantɨ</ts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2329" n="HIAT:w" s="T632">laka</ts>
                  <nts id="Seg_2330" n="HIAT:ip">.</nts>
                  <nts id="Seg_2331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T637" id="Seg_2333" n="HIAT:u" s="T633">
                  <ts e="T634" id="Seg_2335" n="HIAT:w" s="T633">Mɔːttɨ</ts>
                  <nts id="Seg_2336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2338" n="HIAT:w" s="T634">šuː</ts>
                  <nts id="Seg_2339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2341" n="HIAT:w" s="T635">nʼanna</ts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2344" n="HIAT:w" s="T636">püŋkolʼna</ts>
                  <nts id="Seg_2345" n="HIAT:ip">.</nts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T642" id="Seg_2348" n="HIAT:u" s="T637">
                  <ts e="T638" id="Seg_2350" n="HIAT:w" s="T637">Kəpɨntɨ</ts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2353" n="HIAT:w" s="T638">laka</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2356" n="HIAT:w" s="T639">qottä</ts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2359" n="HIAT:w" s="T640">ponä</ts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2362" n="HIAT:w" s="T641">alʼčʼa</ts>
                  <nts id="Seg_2363" n="HIAT:ip">.</nts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T643" id="Seg_2366" n="HIAT:u" s="T642">
                  <ts e="T643" id="Seg_2368" n="HIAT:w" s="T642">Qəttentit</ts>
                  <nts id="Seg_2369" n="HIAT:ip">.</nts>
                  <nts id="Seg_2370" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T650" id="Seg_2372" n="HIAT:u" s="T643">
                  <ts e="T644" id="Seg_2374" n="HIAT:w" s="T643">Ponä</ts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2377" n="HIAT:w" s="T644">tattɨŋɨt</ts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2380" n="HIAT:w" s="T645">tä</ts>
                  <nts id="Seg_2381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2383" n="HIAT:w" s="T646">Sɨlʼčʼa-Pɨlʼčʼa</ts>
                  <nts id="Seg_2384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2386" n="HIAT:w" s="T647">Qəš</ts>
                  <nts id="Seg_2387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2389" n="HIAT:w" s="T648">kəpɨntɨ</ts>
                  <nts id="Seg_2390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2392" n="HIAT:w" s="T649">laka</ts>
                  <nts id="Seg_2393" n="HIAT:ip">.</nts>
                  <nts id="Seg_2394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T654" id="Seg_2396" n="HIAT:u" s="T650">
                  <ts e="T651" id="Seg_2398" n="HIAT:w" s="T650">Ponä</ts>
                  <nts id="Seg_2399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2401" n="HIAT:w" s="T651">tanta</ts>
                  <nts id="Seg_2402" n="HIAT:ip">,</nts>
                  <nts id="Seg_2403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2405" n="HIAT:w" s="T652">monte</ts>
                  <nts id="Seg_2406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2408" n="HIAT:w" s="T653">čʼeːlɨŋɛlʼčʼa</ts>
                  <nts id="Seg_2409" n="HIAT:ip">.</nts>
                  <nts id="Seg_2410" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T663" id="Seg_2412" n="HIAT:u" s="T654">
                  <ts e="T655" id="Seg_2414" n="HIAT:w" s="T654">Karrə</ts>
                  <nts id="Seg_2415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2417" n="HIAT:w" s="T655">tulʼtɨŋɨt</ts>
                  <nts id="Seg_2418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2420" n="HIAT:w" s="T656">Sɨlʼčʼa-Pɨlʼčʼa</ts>
                  <nts id="Seg_2421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2423" n="HIAT:w" s="T657">Qəš</ts>
                  <nts id="Seg_2424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2426" n="HIAT:w" s="T658">kəpɨntɨ</ts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2429" n="HIAT:w" s="T659">lakap</ts>
                  <nts id="Seg_2430" n="HIAT:ip">,</nts>
                  <nts id="Seg_2431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2433" n="HIAT:w" s="T660">ütɨlʼ</ts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2436" n="HIAT:w" s="T661">qəqontə</ts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2439" n="HIAT:w" s="T662">pinnete</ts>
                  <nts id="Seg_2440" n="HIAT:ip">.</nts>
                  <nts id="Seg_2441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T668" id="Seg_2443" n="HIAT:u" s="T663">
                  <ts e="T664" id="Seg_2445" n="HIAT:w" s="T663">Ulqosä</ts>
                  <nts id="Seg_2446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2448" n="HIAT:w" s="T664">nʼentɨ</ts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2451" n="HIAT:w" s="T665">qantäptitɨ</ts>
                  <nts id="Seg_2452" n="HIAT:ip">,</nts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2455" n="HIAT:w" s="T666">nılʼčʼik</ts>
                  <nts id="Seg_2456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2458" n="HIAT:w" s="T667">pinnete</ts>
                  <nts id="Seg_2459" n="HIAT:ip">.</nts>
                  <nts id="Seg_2460" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T673" id="Seg_2462" n="HIAT:u" s="T668">
                  <ts e="T669" id="Seg_2464" n="HIAT:w" s="T668">Ütɨlʼ</ts>
                  <nts id="Seg_2465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2467" n="HIAT:w" s="T669">qəqɨntɨ</ts>
                  <nts id="Seg_2468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2470" n="HIAT:w" s="T670">qanɨktɨ</ts>
                  <nts id="Seg_2471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2473" n="HIAT:w" s="T671">nılʼčʼik</ts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2476" n="HIAT:w" s="T672">pinnɨt</ts>
                  <nts id="Seg_2477" n="HIAT:ip">.</nts>
                  <nts id="Seg_2478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T677" id="Seg_2480" n="HIAT:u" s="T673">
                  <ts e="T674" id="Seg_2482" n="HIAT:w" s="T673">Mit</ts>
                  <nts id="Seg_2483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2485" n="HIAT:w" s="T674">ola</ts>
                  <nts id="Seg_2486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2488" n="HIAT:w" s="T675">ilɨlʼä</ts>
                  <nts id="Seg_2489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2491" n="HIAT:w" s="T676">ippa</ts>
                  <nts id="Seg_2492" n="HIAT:ip">.</nts>
                  <nts id="Seg_2493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T680" id="Seg_2495" n="HIAT:u" s="T677">
                  <ts e="T678" id="Seg_2497" n="HIAT:w" s="T677">Olɨntäsä</ts>
                  <nts id="Seg_2498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2500" n="HIAT:w" s="T678">nʼentɨ</ts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2503" n="HIAT:w" s="T679">loːqaltɨŋɨtɨ</ts>
                  <nts id="Seg_2504" n="HIAT:ip">.</nts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_2507" n="HIAT:u" s="T680">
                  <ts e="T681" id="Seg_2509" n="HIAT:w" s="T680">Na</ts>
                  <nts id="Seg_2510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2512" n="HIAT:w" s="T681">čʼeːlatɨŋɔːmɨt</ts>
                  <nts id="Seg_2513" n="HIAT:ip">.</nts>
                  <nts id="Seg_2514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T683" id="Seg_2516" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_2518" n="HIAT:w" s="T682">Lɨpkomɔːta</ts>
                  <nts id="Seg_2519" n="HIAT:ip">.</nts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T691" id="Seg_2522" n="HIAT:u" s="T683">
                  <ts e="T684" id="Seg_2524" n="HIAT:w" s="T683">Mompa</ts>
                  <nts id="Seg_2525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2527" n="HIAT:w" s="T684">kekkɨsä</ts>
                  <nts id="Seg_2528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2530" n="HIAT:w" s="T685">šittäqıj</ts>
                  <nts id="Seg_2531" n="HIAT:ip">,</nts>
                  <nts id="Seg_2532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2534" n="HIAT:w" s="T686">jarɨk</ts>
                  <nts id="Seg_2535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2537" n="HIAT:w" s="T687">mɨ</ts>
                  <nts id="Seg_2538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2540" n="HIAT:w" s="T688">ičʼeqa</ts>
                  <nts id="Seg_2541" n="HIAT:ip">,</nts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2544" n="HIAT:w" s="T689">aše</ts>
                  <nts id="Seg_2545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2547" n="HIAT:w" s="T690">tɛnɨmɔːmɨt</ts>
                  <nts id="Seg_2548" n="HIAT:ip">.</nts>
                  <nts id="Seg_2549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T694" id="Seg_2551" n="HIAT:u" s="T691">
                  <ts e="T692" id="Seg_2553" n="HIAT:w" s="T691">Üːtɨt</ts>
                  <nts id="Seg_2554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2556" n="HIAT:w" s="T692">na</ts>
                  <nts id="Seg_2557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2559" n="HIAT:w" s="T693">lʼipkɨmɔːnna</ts>
                  <nts id="Seg_2560" n="HIAT:ip">.</nts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T697" id="Seg_2563" n="HIAT:u" s="T694">
                  <ts e="T695" id="Seg_2565" n="HIAT:w" s="T694">Üːtɨt</ts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2568" n="HIAT:w" s="T695">seːpɨlak</ts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2571" n="HIAT:w" s="T696">ɔːmta</ts>
                  <nts id="Seg_2572" n="HIAT:ip">.</nts>
                  <nts id="Seg_2573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T699" id="Seg_2575" n="HIAT:u" s="T697">
                  <ts e="T698" id="Seg_2577" n="HIAT:w" s="T697">Na</ts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2580" n="HIAT:w" s="T698">rɛmkɨmɔːtqolamna</ts>
                  <nts id="Seg_2581" n="HIAT:ip">.</nts>
                  <nts id="Seg_2582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T703" id="Seg_2584" n="HIAT:u" s="T699">
                  <ts e="T700" id="Seg_2586" n="HIAT:w" s="T699">Qumɨp</ts>
                  <nts id="Seg_2587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2589" n="HIAT:w" s="T700">ponä</ts>
                  <nts id="Seg_2590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2592" n="HIAT:w" s="T701">kuralʼtɨtä</ts>
                  <nts id="Seg_2593" n="HIAT:ip">:</nts>
                  <nts id="Seg_2594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2596" n="HIAT:w" s="T702">Üŋkɨlʼtɨmpätɨ</ts>
                  <nts id="Seg_2597" n="HIAT:ip">.</nts>
                  <nts id="Seg_2598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T706" id="Seg_2600" n="HIAT:u" s="T703">
                  <ts e="T704" id="Seg_2602" n="HIAT:w" s="T703">Qup</ts>
                  <nts id="Seg_2603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2605" n="HIAT:w" s="T704">poːqɨt</ts>
                  <nts id="Seg_2606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2608" n="HIAT:w" s="T705">nɨŋa</ts>
                  <nts id="Seg_2609" n="HIAT:ip">.</nts>
                  <nts id="Seg_2610" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T707" id="Seg_2612" n="HIAT:u" s="T706">
                  <ts e="T707" id="Seg_2614" n="HIAT:w" s="T706">Qəː</ts>
                  <nts id="Seg_2615" n="HIAT:ip">!</nts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T710" id="Seg_2618" n="HIAT:u" s="T707">
                  <ts e="T708" id="Seg_2620" n="HIAT:w" s="T707">Qum</ts>
                  <nts id="Seg_2621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2623" n="HIAT:w" s="T708">na</ts>
                  <nts id="Seg_2624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2626" n="HIAT:w" s="T709">qompɨšqolamna</ts>
                  <nts id="Seg_2627" n="HIAT:ip">!</nts>
                  <nts id="Seg_2628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T712" id="Seg_2630" n="HIAT:u" s="T710">
                  <ts e="T711" id="Seg_2632" n="HIAT:w" s="T710">Mɔːttɨ</ts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2635" n="HIAT:w" s="T711">patqɨlna</ts>
                  <nts id="Seg_2636" n="HIAT:ip">.</nts>
                  <nts id="Seg_2637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T719" id="Seg_2639" n="HIAT:u" s="T712">
                  <ts e="T713" id="Seg_2641" n="HIAT:w" s="T712">Mɔːttɨ</ts>
                  <nts id="Seg_2642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2644" n="HIAT:w" s="T713">šeːrlʼä</ts>
                  <nts id="Seg_2645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2647" n="HIAT:w" s="T714">nılʼčʼik</ts>
                  <nts id="Seg_2648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2650" n="HIAT:w" s="T715">kätɨmpat</ts>
                  <nts id="Seg_2651" n="HIAT:ip">:</nts>
                  <nts id="Seg_2652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2654" n="HIAT:w" s="T716">Loːsɨ</ts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2657" n="HIAT:w" s="T717">karrät</ts>
                  <nts id="Seg_2658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2660" n="HIAT:w" s="T718">qompɨnʼnʼä</ts>
                  <nts id="Seg_2661" n="HIAT:ip">.</nts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T723" id="Seg_2664" n="HIAT:u" s="T719">
                  <ts e="T720" id="Seg_2666" n="HIAT:w" s="T719">Konna</ts>
                  <nts id="Seg_2667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2669" n="HIAT:w" s="T720">kurɨlʼlʼä</ts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2672" n="HIAT:w" s="T721">na</ts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2675" n="HIAT:w" s="T722">tannɨnta</ts>
                  <nts id="Seg_2676" n="HIAT:ip">.</nts>
                  <nts id="Seg_2677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T726" id="Seg_2679" n="HIAT:u" s="T723">
                  <ts e="T724" id="Seg_2681" n="HIAT:w" s="T723">–Konnä</ts>
                  <nts id="Seg_2682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2684" n="HIAT:w" s="T724">qaj</ts>
                  <nts id="Seg_2685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2687" n="HIAT:w" s="T725">qompɨčʼčʼe</ts>
                  <nts id="Seg_2688" n="HIAT:ip">?</nts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T727" id="Seg_2691" n="HIAT:u" s="T726">
                  <ts e="T727" id="Seg_2693" n="HIAT:w" s="T726">Aaa</ts>
                  <nts id="Seg_2694" n="HIAT:ip">!</nts>
                  <nts id="Seg_2695" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T732" id="Seg_2697" n="HIAT:u" s="T727">
                  <ts e="T728" id="Seg_2699" n="HIAT:w" s="T727">Tat</ts>
                  <nts id="Seg_2700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2702" n="HIAT:w" s="T728">qaj</ts>
                  <nts id="Seg_2703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2705" n="HIAT:w" s="T729">kočʼkɨmɔːnantɨ</ts>
                  <nts id="Seg_2706" n="HIAT:ip">,</nts>
                  <nts id="Seg_2707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2709" n="HIAT:w" s="T730">amɨrɛlʼčʼinantɨ</ts>
                  <nts id="Seg_2710" n="HIAT:ip">,</nts>
                  <nts id="Seg_2711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2713" n="HIAT:w" s="T731">narkɨmɔːtijantɨ</ts>
                  <nts id="Seg_2714" n="HIAT:ip">?</nts>
                  <nts id="Seg_2715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T736" id="Seg_2717" n="HIAT:u" s="T732">
                  <ts e="T733" id="Seg_2719" n="HIAT:w" s="T732">Ukkur</ts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2722" n="HIAT:w" s="T733">nʼeːmtɨ</ts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2725" n="HIAT:w" s="T734">nɨlʼčʼik</ts>
                  <nts id="Seg_2726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2728" n="HIAT:w" s="T735">qoŋɨtɨ</ts>
                  <nts id="Seg_2729" n="HIAT:ip">.</nts>
                  <nts id="Seg_2730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T741" id="Seg_2732" n="HIAT:u" s="T736">
                  <ts e="T737" id="Seg_2734" n="HIAT:w" s="T736">Man</ts>
                  <nts id="Seg_2735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2737" n="HIAT:w" s="T737">okoːt</ts>
                  <nts id="Seg_2738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2740" n="HIAT:w" s="T738">amɨrrɛːlʼä</ts>
                  <nts id="Seg_2741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2743" n="HIAT:w" s="T739">nılʼčʼik</ts>
                  <nts id="Seg_2744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2746" n="HIAT:w" s="T740">ippɨkkolʼčʼimpɨkkak</ts>
                  <nts id="Seg_2747" n="HIAT:ip">.</nts>
                  <nts id="Seg_2748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T746" id="Seg_2750" n="HIAT:u" s="T741">
                  <ts e="T742" id="Seg_2752" n="HIAT:w" s="T741">Nɨːnɨ</ts>
                  <nts id="Seg_2753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2755" n="HIAT:w" s="T742">konna</ts>
                  <nts id="Seg_2756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2758" n="HIAT:w" s="T743">kurɨlʼä</ts>
                  <nts id="Seg_2759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2761" n="HIAT:w" s="T744">na</ts>
                  <nts id="Seg_2762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2764" n="HIAT:w" s="T745">tünta</ts>
                  <nts id="Seg_2765" n="HIAT:ip">.</nts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T750" id="Seg_2768" n="HIAT:u" s="T746">
                  <ts e="T747" id="Seg_2770" n="HIAT:w" s="T746">Konna</ts>
                  <nts id="Seg_2771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2773" n="HIAT:w" s="T747">kürɨlʼä</ts>
                  <nts id="Seg_2774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2776" n="HIAT:w" s="T748">na</ts>
                  <nts id="Seg_2777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2779" n="HIAT:w" s="T749">tünta</ts>
                  <nts id="Seg_2780" n="HIAT:ip">.</nts>
                  <nts id="Seg_2781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T752" id="Seg_2783" n="HIAT:u" s="T750">
                  <ts e="T751" id="Seg_2785" n="HIAT:w" s="T750">Čʼuk</ts>
                  <nts id="Seg_2786" n="HIAT:ip">,</nts>
                  <nts id="Seg_2787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2789" n="HIAT:w" s="T751">čʼuk</ts>
                  <nts id="Seg_2790" n="HIAT:ip">.</nts>
                  <nts id="Seg_2791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T757" id="Seg_2793" n="HIAT:u" s="T752">
                  <ts e="T753" id="Seg_2795" n="HIAT:w" s="T752">Qaj</ts>
                  <nts id="Seg_2796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2798" n="HIAT:w" s="T753">Sɨlʼčʼa-Pɨlʼčʼa</ts>
                  <nts id="Seg_2799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2801" n="HIAT:w" s="T754">Qəš</ts>
                  <nts id="Seg_2802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2804" n="HIAT:w" s="T755">qattülʼčʼa</ts>
                  <nts id="Seg_2805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2806" n="HIAT:ip">(</nts>
                  <nts id="Seg_2807" n="HIAT:ip">/</nts>
                  <ts e="T757" id="Seg_2809" n="HIAT:w" s="T756">tüŋa</ts>
                  <nts id="Seg_2810" n="HIAT:ip">)</nts>
                  <nts id="Seg_2811" n="HIAT:ip">?</nts>
                  <nts id="Seg_2812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T761" id="Seg_2814" n="HIAT:u" s="T757">
                  <ts e="T758" id="Seg_2816" n="HIAT:w" s="T757">Qaj</ts>
                  <nts id="Seg_2817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2819" n="HIAT:w" s="T758">qəssa</ts>
                  <nts id="Seg_2820" n="HIAT:ip">,</nts>
                  <nts id="Seg_2821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2823" n="HIAT:w" s="T759">qaj</ts>
                  <nts id="Seg_2824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2826" n="HIAT:w" s="T760">qattüssa</ts>
                  <nts id="Seg_2827" n="HIAT:ip">?</nts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T769" id="Seg_2830" n="HIAT:u" s="T761">
                  <ts e="T762" id="Seg_2832" n="HIAT:w" s="T761">–Sɨlʼčʼa-Pɨlʼčʼa</ts>
                  <nts id="Seg_2833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2835" n="HIAT:w" s="T762">Qəš</ts>
                  <nts id="Seg_2836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2838" n="HIAT:w" s="T763">okoːn</ts>
                  <nts id="Seg_2839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2841" n="HIAT:w" s="T764">naša</ts>
                  <nts id="Seg_2842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2844" n="HIAT:w" s="T765">qəssa</ts>
                  <nts id="Seg_2845" n="HIAT:ip">,</nts>
                  <nts id="Seg_2846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2848" n="HIAT:w" s="T766">tıntɨ</ts>
                  <nts id="Seg_2849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2851" n="HIAT:w" s="T767">naša</ts>
                  <nts id="Seg_2852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2854" n="HIAT:w" s="T768">qəssa</ts>
                  <nts id="Seg_2855" n="HIAT:ip">.</nts>
                  <nts id="Seg_2856" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T774" id="Seg_2858" n="HIAT:u" s="T769">
                  <ts e="T770" id="Seg_2860" n="HIAT:w" s="T769">Tat</ts>
                  <nts id="Seg_2861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_2863" n="HIAT:w" s="T770">konna</ts>
                  <nts id="Seg_2864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2866" n="HIAT:w" s="T771">qəlla</ts>
                  <nts id="Seg_2867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2869" n="HIAT:w" s="T772">apsolʼ</ts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_2872" n="HIAT:w" s="T773">amtɨ</ts>
                  <nts id="Seg_2873" n="HIAT:ip">.</nts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T781" id="Seg_2876" n="HIAT:u" s="T774">
                  <ts e="T775" id="Seg_2878" n="HIAT:w" s="T774">Nɨːna</ts>
                  <nts id="Seg_2879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2881" n="HIAT:w" s="T775">konna</ts>
                  <nts id="Seg_2882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2884" n="HIAT:w" s="T776">qəllä</ts>
                  <nts id="Seg_2885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2886" n="HIAT:ip">(</nts>
                  <nts id="Seg_2887" n="HIAT:ip">/</nts>
                  <ts e="T778" id="Seg_2889" n="HIAT:w" s="T777">kurlʼä</ts>
                  <nts id="Seg_2890" n="HIAT:ip">)</nts>
                  <nts id="Seg_2891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_2893" n="HIAT:w" s="T778">tıntena</ts>
                  <nts id="Seg_2894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2896" n="HIAT:w" s="T779">pɔːrkəntɨ</ts>
                  <nts id="Seg_2897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2899" n="HIAT:w" s="T780">tüːŋa</ts>
                  <nts id="Seg_2900" n="HIAT:ip">.</nts>
                  <nts id="Seg_2901" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T787" id="Seg_2903" n="HIAT:u" s="T781">
                  <ts e="T782" id="Seg_2905" n="HIAT:w" s="T781">Kättɨtɨ</ts>
                  <nts id="Seg_2906" n="HIAT:ip">:</nts>
                  <nts id="Seg_2907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_2909" n="HIAT:w" s="T782">Qaj</ts>
                  <nts id="Seg_2910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2912" n="HIAT:w" s="T783">Sɨlʼčʼa-Pɨlʼčʼa</ts>
                  <nts id="Seg_2913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2915" n="HIAT:w" s="T784">Qəš</ts>
                  <nts id="Seg_2916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_2918" n="HIAT:w" s="T785">mɔːtqɨn</ts>
                  <nts id="Seg_2919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_2921" n="HIAT:w" s="T786">ɛːiŋa</ts>
                  <nts id="Seg_2922" n="HIAT:ip">?</nts>
                  <nts id="Seg_2923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T792" id="Seg_2925" n="HIAT:u" s="T787">
                  <ts e="T788" id="Seg_2927" n="HIAT:w" s="T787">–Täm</ts>
                  <nts id="Seg_2928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_2930" n="HIAT:w" s="T788">aša</ts>
                  <nts id="Seg_2931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_2933" n="HIAT:w" s="T789">našat</ts>
                  <nts id="Seg_2934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_2936" n="HIAT:w" s="T790">tɨntä</ts>
                  <nts id="Seg_2937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_2939" n="HIAT:w" s="T791">qəssa</ts>
                  <nts id="Seg_2940" n="HIAT:ip">.</nts>
                  <nts id="Seg_2941" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T796" id="Seg_2943" n="HIAT:u" s="T792">
                  <ts e="T793" id="Seg_2945" n="HIAT:w" s="T792">Mɔːt</ts>
                  <nts id="Seg_2946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_2948" n="HIAT:w" s="T793">šeːrlʼa</ts>
                  <nts id="Seg_2949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_2951" n="HIAT:w" s="T794">apsolʼ</ts>
                  <nts id="Seg_2952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_2954" n="HIAT:w" s="T795">amtɨ</ts>
                  <nts id="Seg_2955" n="HIAT:ip">.</nts>
                  <nts id="Seg_2956" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T809" id="Seg_2958" n="HIAT:u" s="T796">
                  <ts e="T797" id="Seg_2960" n="HIAT:w" s="T796">Ɔːm</ts>
                  <nts id="Seg_2961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_2963" n="HIAT:w" s="T797">aj</ts>
                  <nts id="Seg_2964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_2966" n="HIAT:w" s="T798">nɨlʼčʼik</ts>
                  <nts id="Seg_2967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_2969" n="HIAT:w" s="T799">ɛssa</ts>
                  <nts id="Seg_2970" n="HIAT:ip">:</nts>
                  <nts id="Seg_2971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_2973" n="HIAT:w" s="T800">Mɔːttɨ</ts>
                  <nts id="Seg_2974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_2976" n="HIAT:w" s="T801">qaj</ts>
                  <nts id="Seg_2977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_2979" n="HIAT:w" s="T802">šeːrtak</ts>
                  <nts id="Seg_2980" n="HIAT:ip">,</nts>
                  <nts id="Seg_2981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_2983" n="HIAT:w" s="T803">qaj</ts>
                  <nts id="Seg_2984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_2986" n="HIAT:w" s="T804">aša</ts>
                  <nts id="Seg_2987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805.tx.1" id="Seg_2989" n="HIAT:w" s="T805">šeːrtak</ts>
                  <nts id="Seg_2990" n="HIAT:ip">,</nts>
                  <ts e="T806" id="Seg_2992" n="HIAT:w" s="T805.tx.1">–</ts>
                  <nts id="Seg_2993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_2995" n="HIAT:w" s="T806">ɔːm</ts>
                  <nts id="Seg_2996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_2998" n="HIAT:w" s="T807">aj</ts>
                  <nts id="Seg_2999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3001" n="HIAT:w" s="T808">mɨ</ts>
                  <nts id="Seg_3002" n="HIAT:ip">.</nts>
                  <nts id="Seg_3003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T814" id="Seg_3005" n="HIAT:u" s="T809">
                  <ts e="T810" id="Seg_3007" n="HIAT:w" s="T809">Pɔːrkä</ts>
                  <nts id="Seg_3008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3010" n="HIAT:w" s="T810">qaj</ts>
                  <nts id="Seg_3011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3013" n="HIAT:w" s="T811">mompa</ts>
                  <nts id="Seg_3014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_3016" n="HIAT:w" s="T812">mašıp</ts>
                  <nts id="Seg_3017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_3019" n="HIAT:w" s="T813">kurɨmmanta</ts>
                  <nts id="Seg_3020" n="HIAT:ip">?</nts>
                  <nts id="Seg_3021" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T823" id="Seg_3023" n="HIAT:u" s="T814">
                  <ts e="T815" id="Seg_3025" n="HIAT:w" s="T814">Qapı</ts>
                  <nts id="Seg_3026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3028" n="HIAT:w" s="T815">kulʼtɨmpa</ts>
                  <nts id="Seg_3029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3030" n="HIAT:ip">(</nts>
                  <nts id="Seg_3031" n="HIAT:ip">/</nts>
                  <ts e="T817" id="Seg_3033" n="HIAT:w" s="T816">kulʼtɨmpɨŋa</ts>
                  <nts id="Seg_3034" n="HIAT:ip">)</nts>
                  <nts id="Seg_3035" n="HIAT:ip">:</nts>
                  <nts id="Seg_3036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3038" n="HIAT:w" s="T817">Ɔːmij</ts>
                  <nts id="Seg_3039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3041" n="HIAT:w" s="T818">qaj</ts>
                  <nts id="Seg_3042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_3044" n="HIAT:w" s="T819">šeːrta</ts>
                  <nts id="Seg_3045" n="HIAT:ip">,</nts>
                  <nts id="Seg_3046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3048" n="HIAT:w" s="T820">qaj</ts>
                  <nts id="Seg_3049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3051" n="HIAT:w" s="T821">aša</ts>
                  <nts id="Seg_3052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_3054" n="HIAT:w" s="T822">šeːrta</ts>
                  <nts id="Seg_3055" n="HIAT:ip">.</nts>
                  <nts id="Seg_3056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T827" id="Seg_3058" n="HIAT:u" s="T823">
                  <ts e="T824" id="Seg_3060" n="HIAT:w" s="T823">Okkɨr</ts>
                  <nts id="Seg_3061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3063" n="HIAT:w" s="T824">čʼontoːqɨt</ts>
                  <nts id="Seg_3064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3066" n="HIAT:w" s="T825">na</ts>
                  <nts id="Seg_3067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3069" n="HIAT:w" s="T826">šeːrqolamta</ts>
                  <nts id="Seg_3070" n="HIAT:ip">.</nts>
                  <nts id="Seg_3071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T836" id="Seg_3073" n="HIAT:u" s="T827">
                  <ts e="T828" id="Seg_3075" n="HIAT:w" s="T827">Olɨmtɨ</ts>
                  <nts id="Seg_3076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3078" n="HIAT:w" s="T828">mɔːttɨ</ts>
                  <nts id="Seg_3079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3081" n="HIAT:w" s="T829">čʼap</ts>
                  <nts id="Seg_3082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_3084" n="HIAT:w" s="T830">noqqɔːlnɨt</ts>
                  <nts id="Seg_3085" n="HIAT:ip">,</nts>
                  <nts id="Seg_3086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3088" n="HIAT:w" s="T831">Sɨlʼčʼa-Pɨlʼčʼa</ts>
                  <nts id="Seg_3089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3091" n="HIAT:w" s="T832">Qəš</ts>
                  <nts id="Seg_3092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3094" n="HIAT:w" s="T833">pačʼalnɨt</ts>
                  <nts id="Seg_3095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3097" n="HIAT:w" s="T834">olantɨ</ts>
                  <nts id="Seg_3098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3100" n="HIAT:w" s="T835">lako</ts>
                  <nts id="Seg_3101" n="HIAT:ip">.</nts>
                  <nts id="Seg_3102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T840" id="Seg_3104" n="HIAT:u" s="T836">
                  <ts e="T837" id="Seg_3106" n="HIAT:w" s="T836">Mɔːttɨ</ts>
                  <nts id="Seg_3107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3109" n="HIAT:w" s="T837">nʼannä</ts>
                  <nts id="Seg_3110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3112" n="HIAT:w" s="T838">alʼčʼa</ts>
                  <nts id="Seg_3113" n="HIAT:ip">,</nts>
                  <nts id="Seg_3114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3116" n="HIAT:w" s="T839">püŋkolʼna</ts>
                  <nts id="Seg_3117" n="HIAT:ip">.</nts>
                  <nts id="Seg_3118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T845" id="Seg_3120" n="HIAT:u" s="T840">
                  <ts e="T841" id="Seg_3122" n="HIAT:w" s="T840">Kopɨntɨ</ts>
                  <nts id="Seg_3123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3125" n="HIAT:w" s="T841">laka</ts>
                  <nts id="Seg_3126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3128" n="HIAT:w" s="T842">qottä</ts>
                  <nts id="Seg_3129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3131" n="HIAT:w" s="T843">ponä</ts>
                  <nts id="Seg_3132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3134" n="HIAT:w" s="T844">alʼčʼa</ts>
                  <nts id="Seg_3135" n="HIAT:ip">.</nts>
                  <nts id="Seg_3136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T846" id="Seg_3138" n="HIAT:u" s="T845">
                  <ts e="T846" id="Seg_3140" n="HIAT:w" s="T845">Qəttɛːŋɨtɨ</ts>
                  <nts id="Seg_3141" n="HIAT:ip">.</nts>
                  <nts id="Seg_3142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T854" id="Seg_3144" n="HIAT:u" s="T846">
                  <ts e="T847" id="Seg_3146" n="HIAT:w" s="T846">Nɨːnɨ</ts>
                  <nts id="Seg_3147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3149" n="HIAT:w" s="T847">nɔːtɨ</ts>
                  <nts id="Seg_3150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_3152" n="HIAT:w" s="T848">čʼap</ts>
                  <nts id="Seg_3153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3155" n="HIAT:w" s="T849">ɔːmtɔːtɨt</ts>
                  <nts id="Seg_3156" n="HIAT:ip">,</nts>
                  <nts id="Seg_3157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3159" n="HIAT:w" s="T850">ɔːmtɔːtɨt</ts>
                  <nts id="Seg_3160" n="HIAT:ip">,</nts>
                  <nts id="Seg_3161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3163" n="HIAT:w" s="T851">nʼi</ts>
                  <nts id="Seg_3164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3166" n="HIAT:w" s="T852">qaj</ts>
                  <nts id="Seg_3167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3169" n="HIAT:w" s="T853">čʼäŋka</ts>
                  <nts id="Seg_3170" n="HIAT:ip">.</nts>
                  <nts id="Seg_3171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T858" id="Seg_3173" n="HIAT:u" s="T854">
                  <ts e="T855" id="Seg_3175" n="HIAT:w" s="T854">Mumpa</ts>
                  <nts id="Seg_3176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3178" n="HIAT:w" s="T855">qaj</ts>
                  <nts id="Seg_3179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_3181" n="HIAT:w" s="T856">šite</ts>
                  <nts id="Seg_3182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3184" n="HIAT:w" s="T857">ičʼa</ts>
                  <nts id="Seg_3185" n="HIAT:ip">.</nts>
                  <nts id="Seg_3186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T862" id="Seg_3188" n="HIAT:u" s="T858">
                  <ts e="T859" id="Seg_3190" n="HIAT:w" s="T858">Tɔːptɨlʼ</ts>
                  <nts id="Seg_3191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3193" n="HIAT:w" s="T859">qarɨt</ts>
                  <nts id="Seg_3194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3196" n="HIAT:w" s="T860">ınna</ts>
                  <nts id="Seg_3197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_3199" n="HIAT:w" s="T861">čʼeːlɨŋna</ts>
                  <nts id="Seg_3200" n="HIAT:ip">.</nts>
                  <nts id="Seg_3201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T865" id="Seg_3203" n="HIAT:u" s="T862">
                  <ts e="T863" id="Seg_3205" n="HIAT:w" s="T862">Mompa</ts>
                  <nts id="Seg_3206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3208" n="HIAT:w" s="T863">poː</ts>
                  <nts id="Seg_3209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3211" n="HIAT:w" s="T864">pačʼčʼalʼnɨlɨt</ts>
                  <nts id="Seg_3212" n="HIAT:ip">.</nts>
                  <nts id="Seg_3213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T867" id="Seg_3215" n="HIAT:u" s="T865">
                  <ts e="T866" id="Seg_3217" n="HIAT:w" s="T865">Poː</ts>
                  <nts id="Seg_3218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3220" n="HIAT:w" s="T866">pačʼčʼalnɔːtɨt</ts>
                  <nts id="Seg_3221" n="HIAT:ip">.</nts>
                  <nts id="Seg_3222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T869" id="Seg_3224" n="HIAT:u" s="T867">
                  <ts e="T868" id="Seg_3226" n="HIAT:w" s="T867">Tü</ts>
                  <nts id="Seg_3227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_3229" n="HIAT:w" s="T868">čʼɔːtɨŋɨlɨt</ts>
                  <nts id="Seg_3230" n="HIAT:ip">.</nts>
                  <nts id="Seg_3231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T872" id="Seg_3233" n="HIAT:u" s="T869">
                  <ts e="T870" id="Seg_3235" n="HIAT:w" s="T869">Əːtɨmɨntɨ</ts>
                  <nts id="Seg_3236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_3238" n="HIAT:w" s="T870">poːp</ts>
                  <nts id="Seg_3239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3241" n="HIAT:w" s="T871">pačʼčʼalʼnɔːtɨt</ts>
                  <nts id="Seg_3242" n="HIAT:ip">.</nts>
                  <nts id="Seg_3243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T880" id="Seg_3245" n="HIAT:u" s="T872">
                  <ts e="T873" id="Seg_3247" n="HIAT:w" s="T872">Šittäqıp</ts>
                  <nts id="Seg_3248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3250" n="HIAT:w" s="T873">karrä</ts>
                  <nts id="Seg_3251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_3253" n="HIAT:w" s="T874">tultoktät</ts>
                  <nts id="Seg_3254" n="HIAT:ip">,</nts>
                  <nts id="Seg_3255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3257" n="HIAT:w" s="T875">tüsä</ts>
                  <nts id="Seg_3258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_3260" n="HIAT:w" s="T876">čʼɔːtɔːtɨt</ts>
                  <nts id="Seg_3261" n="HIAT:ip">,</nts>
                  <nts id="Seg_3262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3264" n="HIAT:w" s="T877">koptɨkɔːlɨk</ts>
                  <nts id="Seg_3265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3267" n="HIAT:w" s="T878">tüsä</ts>
                  <nts id="Seg_3268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_3270" n="HIAT:w" s="T879">čʼɔːtɔːtɨt</ts>
                  <nts id="Seg_3271" n="HIAT:ip">.</nts>
                  <nts id="Seg_3272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T886" id="Seg_3274" n="HIAT:u" s="T880">
                  <ts e="T881" id="Seg_3276" n="HIAT:w" s="T880">Iralʼ</ts>
                  <nts id="Seg_3277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3279" n="HIAT:w" s="T881">äsäsɨt</ts>
                  <nts id="Seg_3280" n="HIAT:ip">,</nts>
                  <nts id="Seg_3281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3283" n="HIAT:w" s="T882">na</ts>
                  <nts id="Seg_3284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_3286" n="HIAT:w" s="T883">ira</ts>
                  <nts id="Seg_3287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_3289" n="HIAT:w" s="T884">nälʼätɨ</ts>
                  <nts id="Seg_3290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3292" n="HIAT:w" s="T885">ɛːppa</ts>
                  <nts id="Seg_3293" n="HIAT:ip">.</nts>
                  <nts id="Seg_3294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T891" id="Seg_3296" n="HIAT:u" s="T886">
                  <ts e="T887" id="Seg_3298" n="HIAT:w" s="T886">Na</ts>
                  <nts id="Seg_3299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_3301" n="HIAT:w" s="T887">nälʼamtɨ</ts>
                  <nts id="Seg_3302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_3304" n="HIAT:w" s="T888">Sɨlʼčʼa-Pɨlʼčʼa</ts>
                  <nts id="Seg_3305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3307" n="HIAT:w" s="T889">Qəštɨ</ts>
                  <nts id="Seg_3308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3310" n="HIAT:w" s="T890">miŋatɨ</ts>
                  <nts id="Seg_3311" n="HIAT:ip">.</nts>
                  <nts id="Seg_3312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T897" id="Seg_3314" n="HIAT:u" s="T891">
                  <ts e="T892" id="Seg_3316" n="HIAT:w" s="T891">Na</ts>
                  <nts id="Seg_3317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_3319" n="HIAT:w" s="T892">təttɨt</ts>
                  <nts id="Seg_3320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_3322" n="HIAT:w" s="T893">moːrɨ</ts>
                  <nts id="Seg_3323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_3325" n="HIAT:w" s="T894">kolʼalʼtɨntäkkɨt</ts>
                  <nts id="Seg_3326" n="HIAT:ip">,</nts>
                  <nts id="Seg_3327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3329" n="HIAT:w" s="T895">šölʼqumɨtɨtkine</ts>
                  <nts id="Seg_3330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3332" n="HIAT:w" s="T896">tüŋa</ts>
                  <nts id="Seg_3333" n="HIAT:ip">.</nts>
                  <nts id="Seg_3334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T900" id="Seg_3336" n="HIAT:u" s="T897">
                  <ts e="T898" id="Seg_3338" n="HIAT:w" s="T897">Moːrɨt</ts>
                  <nts id="Seg_3339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3341" n="HIAT:w" s="T898">na</ts>
                  <nts id="Seg_3342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_3344" n="HIAT:w" s="T899">ɛːnta</ts>
                  <nts id="Seg_3345" n="HIAT:ip">.</nts>
                  <nts id="Seg_3346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T900" id="Seg_3347" n="sc" s="T0">
               <ts e="T1" id="Seg_3349" n="e" s="T0">Sɨlʼčʼa-Pɨlʼčʼa. </ts>
               <ts e="T2" id="Seg_3351" n="e" s="T1">Sɨlʼčʼa-Pɨlʼčʼa </ts>
               <ts e="T3" id="Seg_3353" n="e" s="T2">Qəš </ts>
               <ts e="T4" id="Seg_3355" n="e" s="T3">ilɨmpa. </ts>
               <ts e="T5" id="Seg_3357" n="e" s="T4">Əmɨtɨj </ts>
               <ts e="T6" id="Seg_3359" n="e" s="T5">ɛːppɨntɨ. </ts>
               <ts e="T7" id="Seg_3361" n="e" s="T6">Äsɨtɨ </ts>
               <ts e="T8" id="Seg_3363" n="e" s="T7">čʼaŋkɨmpa. </ts>
               <ts e="T9" id="Seg_3365" n="e" s="T8">Ila, </ts>
               <ts e="T10" id="Seg_3367" n="e" s="T9">ila. </ts>
               <ts e="T11" id="Seg_3369" n="e" s="T10">Okkɨr </ts>
               <ts e="T12" id="Seg_3371" n="e" s="T11">čʼontot </ts>
               <ts e="T13" id="Seg_3373" n="e" s="T12">əmɨntɨ </ts>
               <ts e="T14" id="Seg_3375" n="e" s="T13">nı </ts>
               <ts e="T15" id="Seg_3377" n="e" s="T14">kotot </ts>
               <ts e="T16" id="Seg_3379" n="e" s="T15">(/kətɨt), </ts>
               <ts e="T17" id="Seg_3381" n="e" s="T16">əmtɨ </ts>
               <ts e="T18" id="Seg_3383" n="e" s="T17">tɨ </ts>
               <ts e="T19" id="Seg_3385" n="e" s="T18">nılʼčʼik </ts>
               <ts e="T20" id="Seg_3387" n="e" s="T19">ɛsa: </ts>
               <ts e="T21" id="Seg_3389" n="e" s="T20">Mat </ts>
               <ts e="T22" id="Seg_3391" n="e" s="T21">qumɨlʼ </ts>
               <ts e="T23" id="Seg_3393" n="e" s="T22">peːrla </ts>
               <ts e="T24" id="Seg_3395" n="e" s="T23">qənnak. </ts>
               <ts e="T25" id="Seg_3397" n="e" s="T24">Tılʼčʼa </ts>
               <ts e="T26" id="Seg_3399" n="e" s="T25">kuttar </ts>
               <ts e="T27" id="Seg_3401" n="e" s="T26">ilantɨmɨt? </ts>
               <ts e="T28" id="Seg_3403" n="e" s="T27">Ama, </ts>
               <ts e="T29" id="Seg_3405" n="e" s="T28">tan </ts>
               <ts e="T30" id="Seg_3407" n="e" s="T29">montɨ </ts>
               <ts e="T31" id="Seg_3409" n="e" s="T30">qaj </ts>
               <ts e="T32" id="Seg_3411" n="e" s="T31">tap </ts>
               <ts e="T33" id="Seg_3413" n="e" s="T32">təttɨt </ts>
               <ts e="T34" id="Seg_3415" n="e" s="T33">pontarqɨt </ts>
               <ts e="T35" id="Seg_3417" n="e" s="T34">qumɨp </ts>
               <ts e="T36" id="Seg_3419" n="e" s="T35">aš </ts>
               <ts e="T37" id="Seg_3421" n="e" s="T36">tɛnima? </ts>
               <ts e="T38" id="Seg_3423" n="e" s="T37">Əmɨt </ts>
               <ts e="T39" id="Seg_3425" n="e" s="T38">tomnɨtɨ: </ts>
               <ts e="T40" id="Seg_3427" n="e" s="T39">Man </ts>
               <ts e="T41" id="Seg_3429" n="e" s="T40">nʼi </ts>
               <ts e="T42" id="Seg_3431" n="e" s="T41">qajlʼ </ts>
               <ts e="T43" id="Seg_3433" n="e" s="T42">qum </ts>
               <ts e="T44" id="Seg_3435" n="e" s="T43">aš </ts>
               <ts e="T45" id="Seg_3437" n="e" s="T44">tɛnɨma. </ts>
               <ts e="T46" id="Seg_3439" n="e" s="T45">Tap </ts>
               <ts e="T47" id="Seg_3441" n="e" s="T46">təttɨt </ts>
               <ts e="T48" id="Seg_3443" n="e" s="T47">pontarqɨt </ts>
               <ts e="T49" id="Seg_3445" n="e" s="T48">tätčʼaqɨt </ts>
               <ts e="T50" id="Seg_3447" n="e" s="T49">qup </ts>
               <ts e="T51" id="Seg_3449" n="e" s="T50">čʼäŋka. </ts>
               <ts e="T52" id="Seg_3451" n="e" s="T51">Ijatɨ </ts>
               <ts e="T53" id="Seg_3453" n="e" s="T52">əmɨntɨn </ts>
               <ts e="T54" id="Seg_3455" n="e" s="T53">nılʼ </ts>
               <ts e="T55" id="Seg_3457" n="e" s="T54">kətɨtä: </ts>
               <ts e="T56" id="Seg_3459" n="e" s="T55">Tan </ts>
               <ts e="T57" id="Seg_3461" n="e" s="T56">mompa </ts>
               <ts e="T58" id="Seg_3463" n="e" s="T57">onät </ts>
               <ts e="T59" id="Seg_3465" n="e" s="T58">ilašik. </ts>
               <ts e="T60" id="Seg_3467" n="e" s="T59">Man </ts>
               <ts e="T61" id="Seg_3469" n="e" s="T60">ompa </ts>
               <ts e="T62" id="Seg_3471" n="e" s="T61">qumɨlʼ </ts>
               <ts e="T63" id="Seg_3473" n="e" s="T62">peːrila </ts>
               <ts e="T64" id="Seg_3475" n="e" s="T63">qəlʼlʼak. </ts>
               <ts e="T65" id="Seg_3477" n="e" s="T64">Tap </ts>
               <ts e="T66" id="Seg_3479" n="e" s="T65">tätɨt </ts>
               <ts e="T67" id="Seg_3481" n="e" s="T66">pontarqɨt </ts>
               <ts e="T68" id="Seg_3483" n="e" s="T67">Sɨlša-Pɨlša </ts>
               <ts e="T69" id="Seg_3485" n="e" s="T68">Qəš </ts>
               <ts e="T70" id="Seg_3487" n="e" s="T69">nɨːnɨ </ts>
               <ts e="T71" id="Seg_3489" n="e" s="T70">qənna. </ts>
               <ts e="T72" id="Seg_3491" n="e" s="T71">Sɨlša-Pɨlša </ts>
               <ts e="T73" id="Seg_3493" n="e" s="T72">Qəš </ts>
               <ts e="T74" id="Seg_3495" n="e" s="T73">našak </ts>
               <ts e="T75" id="Seg_3497" n="e" s="T74">qəntɨna. </ts>
               <ts e="T76" id="Seg_3499" n="e" s="T75">Kəŋka </ts>
               <ts e="T77" id="Seg_3501" n="e" s="T76">aj </ts>
               <ts e="T78" id="Seg_3503" n="e" s="T77">taŋɨmka. </ts>
               <ts e="T79" id="Seg_3505" n="e" s="T78">Qoltɨp </ts>
               <ts e="T80" id="Seg_3507" n="e" s="T79">ka </ts>
               <ts e="T81" id="Seg_3509" n="e" s="T80">tulʼtɨmpɨka, </ts>
               <ts e="T82" id="Seg_3511" n="e" s="T81">kɨp </ts>
               <ts e="T83" id="Seg_3513" n="e" s="T82">kä </ts>
               <ts e="T84" id="Seg_3515" n="e" s="T83">tulʼtɨmpɨka. </ts>
               <ts e="T85" id="Seg_3517" n="e" s="T84">Nʼikakoj </ts>
               <ts e="T86" id="Seg_3519" n="e" s="T85">ni </ts>
               <ts e="T87" id="Seg_3521" n="e" s="T86">qailʼ </ts>
               <ts e="T88" id="Seg_3523" n="e" s="T87">qup </ts>
               <ts e="T89" id="Seg_3525" n="e" s="T88">čʼäŋka. </ts>
               <ts e="T90" id="Seg_3527" n="e" s="T89">To </ts>
               <ts e="T91" id="Seg_3529" n="e" s="T90">toːlʼ </ts>
               <ts e="T92" id="Seg_3531" n="e" s="T91">mɨqɨt, </ts>
               <ts e="T93" id="Seg_3533" n="e" s="T92">ket </ts>
               <ts e="T94" id="Seg_3535" n="e" s="T93">qənka. </ts>
               <ts e="T95" id="Seg_3537" n="e" s="T94">Ükontuqo </ts>
               <ts e="T96" id="Seg_3539" n="e" s="T95">qɔːtqet </ts>
               <ts e="T97" id="Seg_3541" n="e" s="T96">sɨrɨčʼčʼika </ts>
               <ts e="T98" id="Seg_3543" n="e" s="T97">aj </ts>
               <ts e="T99" id="Seg_3545" n="e" s="T98">čʼüːqo. </ts>
               <ts e="T100" id="Seg_3547" n="e" s="T99">Ukkur </ts>
               <ts e="T101" id="Seg_3549" n="e" s="T100">čʼontoːqɨn </ts>
               <ts e="T102" id="Seg_3551" n="e" s="T101">tulunʼnʼa </ts>
               <ts e="T103" id="Seg_3553" n="e" s="T102">mačʼonta, </ts>
               <ts e="T104" id="Seg_3555" n="e" s="T103">marqɨl </ts>
               <ts e="T105" id="Seg_3557" n="e" s="T104">mačʼe. </ts>
               <ts e="T106" id="Seg_3559" n="e" s="T105">Na </ts>
               <ts e="T107" id="Seg_3561" n="e" s="T106">mačʼoːmɨt </ts>
               <ts e="T108" id="Seg_3563" n="e" s="T107">kɨ </ts>
               <ts e="T109" id="Seg_3565" n="e" s="T108">tannɨmmɨnta. </ts>
               <ts e="T110" id="Seg_3567" n="e" s="T109">Na </ts>
               <ts e="T111" id="Seg_3569" n="e" s="T110">kɨt </ts>
               <ts e="T112" id="Seg_3571" n="e" s="T111">qanɨŋmɨt </ts>
               <ts e="T113" id="Seg_3573" n="e" s="T112">qälʼimpak. </ts>
               <ts e="T114" id="Seg_3575" n="e" s="T113">Ukkur </ts>
               <ts e="T115" id="Seg_3577" n="e" s="T114">čʼontoːqɨt </ts>
               <ts e="T116" id="Seg_3579" n="e" s="T115">qos </ts>
               <ts e="T117" id="Seg_3581" n="e" s="T116">qaj </ts>
               <ts e="T118" id="Seg_3583" n="e" s="T117">ünnänta: </ts>
               <ts e="T119" id="Seg_3585" n="e" s="T118">A </ts>
               <ts e="T120" id="Seg_3587" n="e" s="T119">buh, </ts>
               <ts e="T121" id="Seg_3589" n="e" s="T120">a </ts>
               <ts e="T122" id="Seg_3591" n="e" s="T121">buh! </ts>
               <ts e="T123" id="Seg_3593" n="e" s="T122">Qälʼimpa, </ts>
               <ts e="T124" id="Seg_3595" n="e" s="T123">nılʼtij </ts>
               <ts e="T125" id="Seg_3597" n="e" s="T124">čʼarɨ </ts>
               <ts e="T126" id="Seg_3599" n="e" s="T125">üntɨnit. </ts>
               <ts e="T127" id="Seg_3601" n="e" s="T126">Nʼi </ts>
               <ts e="T128" id="Seg_3603" n="e" s="T127">qaj </ts>
               <ts e="T129" id="Seg_3605" n="e" s="T128">suːrɨm, </ts>
               <ts e="T130" id="Seg_3607" n="e" s="T129">nʼi </ts>
               <ts e="T131" id="Seg_3609" n="e" s="T130">qaj </ts>
               <ts e="T132" id="Seg_3611" n="e" s="T131">čʼäŋka. </ts>
               <ts e="T133" id="Seg_3613" n="e" s="T132">Na </ts>
               <ts e="T134" id="Seg_3615" n="e" s="T133">tättɨp </ts>
               <ts e="T135" id="Seg_3617" n="e" s="T134">pontar </ts>
               <ts e="T136" id="Seg_3619" n="e" s="T135">kolʼaltɨptäːqak </ts>
               <ts e="T137" id="Seg_3621" n="e" s="T136">kusa </ts>
               <ts e="T138" id="Seg_3623" n="e" s="T137">qaj </ts>
               <ts e="T139" id="Seg_3625" n="e" s="T138">orɨčʼčʼe? </ts>
               <ts e="T140" id="Seg_3627" n="e" s="T139">Mat </ts>
               <ts e="T141" id="Seg_3629" n="e" s="T140">hot </ts>
               <ts e="T142" id="Seg_3631" n="e" s="T141">mannɨmpɨsan </ts>
               <ts e="T143" id="Seg_3633" n="e" s="T142">ɛna. </ts>
               <ts e="T144" id="Seg_3635" n="e" s="T143">Na </ts>
               <ts e="T145" id="Seg_3637" n="e" s="T144">tünnent, </ts>
               <ts e="T146" id="Seg_3639" n="e" s="T145">na </ts>
               <ts e="T147" id="Seg_3641" n="e" s="T146">tünnenta. </ts>
               <ts e="T148" id="Seg_3643" n="e" s="T147">Ɨːrɨk </ts>
               <ts e="T149" id="Seg_3645" n="e" s="T148">mit </ts>
               <ts e="T150" id="Seg_3647" n="e" s="T149">olʼa </ts>
               <ts e="T151" id="Seg_3649" n="e" s="T150">kɨ </ts>
               <ts e="T152" id="Seg_3651" n="e" s="T151">šinčʼoːqɨt </ts>
               <ts e="T153" id="Seg_3653" n="e" s="T152">takkɨt </ts>
               <ts e="T154" id="Seg_3655" n="e" s="T153">ünta </ts>
               <ts e="T155" id="Seg_3657" n="e" s="T154">aj </ts>
               <ts e="T156" id="Seg_3659" n="e" s="T155">kɛn </ts>
               <ts e="T157" id="Seg_3661" n="e" s="T156">na </ts>
               <ts e="T158" id="Seg_3663" n="e" s="T157">laŋkɨčʼčʼenta: </ts>
               <ts e="T159" id="Seg_3665" n="e" s="T158">A </ts>
               <ts e="T160" id="Seg_3667" n="e" s="T159">buh, </ts>
               <ts e="T161" id="Seg_3669" n="e" s="T160">a </ts>
               <ts e="T162" id="Seg_3671" n="e" s="T161">buh! </ts>
               <ts e="T163" id="Seg_3673" n="e" s="T162">Nılʼti </ts>
               <ts e="T164" id="Seg_3675" n="e" s="T163">čʼarɨtɨ </ts>
               <ts e="T165" id="Seg_3677" n="e" s="T164">qalʼ </ts>
               <ts e="T166" id="Seg_3679" n="e" s="T165">montɨ </ts>
               <ts e="T167" id="Seg_3681" n="e" s="T166">tɛːŋŋɨrna. </ts>
               <ts e="T168" id="Seg_3683" n="e" s="T167">Na </ts>
               <ts e="T169" id="Seg_3685" n="e" s="T168">tüntanɨɨɨ. </ts>
               <ts e="T170" id="Seg_3687" n="e" s="T169">Takkɨt </ts>
               <ts e="T171" id="Seg_3689" n="e" s="T170">kɨt </ts>
               <ts e="T172" id="Seg_3691" n="e" s="T171">kɨːqat </ts>
               <ts e="T173" id="Seg_3693" n="e" s="T172">qos </ts>
               <ts e="T174" id="Seg_3695" n="e" s="T173">qaj </ts>
               <ts e="T175" id="Seg_3697" n="e" s="T174">säːqiɨmɨt. </ts>
               <ts e="T176" id="Seg_3699" n="e" s="T175">Na </ts>
               <ts e="T177" id="Seg_3701" n="e" s="T176">majnilʼ </ts>
               <ts e="T178" id="Seg_3703" n="e" s="T177">laŋkɨnʼä: </ts>
               <ts e="T179" id="Seg_3705" n="e" s="T178">Aaa </ts>
               <ts e="T180" id="Seg_3707" n="e" s="T179">buh, </ts>
               <ts e="T181" id="Seg_3709" n="e" s="T180">aaa </ts>
               <ts e="T182" id="Seg_3711" n="e" s="T181">buh! </ts>
               <ts e="T183" id="Seg_3713" n="e" s="T182">Aša </ts>
               <ts e="T184" id="Seg_3715" n="e" s="T183">ni </ts>
               <ts e="T185" id="Seg_3717" n="e" s="T184">qumɨt </ts>
               <ts e="T186" id="Seg_3719" n="e" s="T185">čʼarɨ </ts>
               <ts e="T187" id="Seg_3721" n="e" s="T186">ola </ts>
               <ts e="T188" id="Seg_3723" n="e" s="T187">nı </ts>
               <ts e="T189" id="Seg_3725" n="e" s="T188">laŋkɨnʼnʼä. </ts>
               <ts e="T190" id="Seg_3727" n="e" s="T189">Nɨː </ts>
               <ts e="T191" id="Seg_3729" n="e" s="T190">na </ts>
               <ts e="T192" id="Seg_3731" n="e" s="T191">tünta. </ts>
               <ts e="T193" id="Seg_3733" n="e" s="T192">Montə </ts>
               <ts e="T194" id="Seg_3735" n="e" s="T193">nılʼčʼik </ts>
               <ts e="T195" id="Seg_3737" n="e" s="T194">qum </ts>
               <ts e="T196" id="Seg_3739" n="e" s="T195">laŋkɨs. </ts>
               <ts e="T197" id="Seg_3741" n="e" s="T196">Nɨː </ts>
               <ts e="T198" id="Seg_3743" n="e" s="T197">čʼap </ts>
               <ts e="T199" id="Seg_3745" n="e" s="T198">tünta </ts>
               <ts e="T200" id="Seg_3747" n="e" s="T199">montɨla </ts>
               <ts e="T201" id="Seg_3749" n="e" s="T200">qup, </ts>
               <ts e="T202" id="Seg_3751" n="e" s="T201">montɨ </ts>
               <ts e="T203" id="Seg_3753" n="e" s="T202">qup </ts>
               <ts e="T204" id="Seg_3755" n="e" s="T203">tam </ts>
               <ts e="T205" id="Seg_3757" n="e" s="T204">äsantɨ </ts>
               <ts e="T206" id="Seg_3759" n="e" s="T205">ämantɨ </ts>
               <ts e="T207" id="Seg_3761" n="e" s="T206">mäčʼisä </ts>
               <ts e="T208" id="Seg_3763" n="e" s="T207">nʼäŋɨčʼa </ts>
               <ts e="T209" id="Seg_3765" n="e" s="T208">qup. </ts>
               <ts e="T210" id="Seg_3767" n="e" s="T209">Tap </ts>
               <ts e="T211" id="Seg_3769" n="e" s="T210">əsɨntɨ </ts>
               <ts e="T212" id="Seg_3771" n="e" s="T211">əmɨntɨ </ts>
               <ts e="T213" id="Seg_3773" n="e" s="T212">wäčʼisä </ts>
               <ts e="T214" id="Seg_3775" n="e" s="T213">nʼäŋɨčʼa </ts>
               <ts e="T215" id="Seg_3777" n="e" s="T214">ɔːmta </ts>
               <ts e="T216" id="Seg_3779" n="e" s="T215">na </ts>
               <ts e="T217" id="Seg_3781" n="e" s="T216">qäqɨn </ts>
               <ts e="T218" id="Seg_3783" n="e" s="T217">ɔːqqɨt. </ts>
               <ts e="T219" id="Seg_3785" n="e" s="T218">Na </ts>
               <ts e="T220" id="Seg_3787" n="e" s="T219">qaj, </ts>
               <ts e="T221" id="Seg_3789" n="e" s="T220">na </ts>
               <ts e="T222" id="Seg_3791" n="e" s="T221">qup </ts>
               <ts e="T223" id="Seg_3793" n="e" s="T222">laŋkɨšmɨnta? </ts>
               <ts e="T224" id="Seg_3795" n="e" s="T223">Tat </ts>
               <ts e="T225" id="Seg_3797" n="e" s="T224">qaː </ts>
               <ts e="T226" id="Seg_3799" n="e" s="T225">nıj </ts>
               <ts e="T227" id="Seg_3801" n="e" s="T226">ɔːmnant? </ts>
               <ts e="T228" id="Seg_3803" n="e" s="T227">–Aša, </ts>
               <ts e="T229" id="Seg_3805" n="e" s="T228">mumpa </ts>
               <ts e="T230" id="Seg_3807" n="e" s="T229">mat </ts>
               <ts e="T231" id="Seg_3809" n="e" s="T230">loːsa </ts>
               <ts e="T232" id="Seg_3811" n="e" s="T231">šım </ts>
               <ts e="T233" id="Seg_3813" n="e" s="T232">amqo </ts>
               <ts e="T234" id="Seg_3815" n="e" s="T233">ɔːmtak. </ts>
               <ts e="T235" id="Seg_3817" n="e" s="T234">Konnäqɨt </ts>
               <ts e="T236" id="Seg_3819" n="e" s="T235">qumiːmɨ </ts>
               <ts e="T237" id="Seg_3821" n="e" s="T236">ɛːŋɔːtɨt. </ts>
               <ts e="T238" id="Seg_3823" n="e" s="T237">Nammɨt </ts>
               <ts e="T239" id="Seg_3825" n="e" s="T238">šıp </ts>
               <ts e="T240" id="Seg_3827" n="e" s="T239">ontalimpɔːtät </ts>
               <ts e="T241" id="Seg_3829" n="e" s="T240">loːš </ts>
               <ts e="T242" id="Seg_3831" n="e" s="T241">šim </ts>
               <ts e="T243" id="Seg_3833" n="e" s="T242">amqo. </ts>
               <ts e="T244" id="Seg_3835" n="e" s="T243">Olqa </ts>
               <ts e="T245" id="Seg_3837" n="e" s="T244">mat </ts>
               <ts e="T246" id="Seg_3839" n="e" s="T245">konna </ts>
               <ts e="T247" id="Seg_3841" n="e" s="T246">tannɛntak, </ts>
               <ts e="T248" id="Seg_3843" n="e" s="T247">loːs </ts>
               <ts e="T249" id="Seg_3845" n="e" s="T248">muntɨk </ts>
               <ts e="T250" id="Seg_3847" n="e" s="T249">šımɨt </ts>
               <ts e="T251" id="Seg_3849" n="e" s="T250">amta, </ts>
               <ts e="T252" id="Seg_3851" n="e" s="T251">ne </ts>
               <ts e="T253" id="Seg_3853" n="e" s="T252">qaim </ts>
               <ts e="T254" id="Seg_3855" n="e" s="T253">naš </ts>
               <ts e="T255" id="Seg_3857" n="e" s="T254">qaläta. </ts>
               <ts e="T256" id="Seg_3859" n="e" s="T255">Na </ts>
               <ts e="T257" id="Seg_3861" n="e" s="T256">qaː </ts>
               <ts e="T258" id="Seg_3863" n="e" s="T257">šım </ts>
               <ts e="T259" id="Seg_3865" n="e" s="T258">omtɨlʼimpɔːtet? </ts>
               <ts e="T260" id="Seg_3867" n="e" s="T259">–Tat </ts>
               <ts e="T261" id="Seg_3869" n="e" s="T260">konnä </ts>
               <ts e="T262" id="Seg_3871" n="e" s="T261">tantɨš. </ts>
               <ts e="T263" id="Seg_3873" n="e" s="T262">–Tan </ts>
               <ts e="T264" id="Seg_3875" n="e" s="T263">nej </ts>
               <ts e="T265" id="Seg_3877" n="e" s="T264">konna </ts>
               <ts e="T266" id="Seg_3879" n="e" s="T265">tantaš. </ts>
               <ts e="T267" id="Seg_3881" n="e" s="T266">Mat </ts>
               <ts e="T268" id="Seg_3883" n="e" s="T267">kutar </ts>
               <ts e="T269" id="Seg_3885" n="e" s="T268">tannɛntak? </ts>
               <ts e="T270" id="Seg_3887" n="e" s="T269">Qumɨt </ts>
               <ts e="T271" id="Seg_3889" n="e" s="T270">šıp </ts>
               <ts e="T272" id="Seg_3891" n="e" s="T271">qolʼčʼantɔːtɨt. </ts>
               <ts e="T273" id="Seg_3893" n="e" s="T272">–Mat </ts>
               <ts e="T274" id="Seg_3895" n="e" s="T273">tomnap, </ts>
               <ts e="T275" id="Seg_3897" n="e" s="T274">tat </ts>
               <ts e="T276" id="Seg_3899" n="e" s="T275">kon </ts>
               <ts e="T277" id="Seg_3901" n="e" s="T276">tantaš. </ts>
               <ts e="T278" id="Seg_3903" n="e" s="T277">Konna </ts>
               <ts e="T279" id="Seg_3905" n="e" s="T278">na </ts>
               <ts e="T280" id="Seg_3907" n="e" s="T279">tannɨntɔːqaj. </ts>
               <ts e="T281" id="Seg_3909" n="e" s="T280">Sɨlʼše-Pɨlʼšet </ts>
               <ts e="T282" id="Seg_3911" n="e" s="T281">Qəš </ts>
               <ts e="T283" id="Seg_3913" n="e" s="T282">pɔːrkä </ts>
               <ts e="T284" id="Seg_3915" n="e" s="T283">na </ts>
               <ts e="T285" id="Seg_3917" n="e" s="T284">meːlʼčʼintɨtɨ </ts>
               <ts e="T286" id="Seg_3919" n="e" s="T285">täːqantɨsa. </ts>
               <ts e="T287" id="Seg_3921" n="e" s="T286">Tan </ts>
               <ts e="T288" id="Seg_3923" n="e" s="T287">na pa </ts>
               <ts e="T289" id="Seg_3925" n="e" s="T288">mašıp </ts>
               <ts e="T290" id="Seg_3927" n="e" s="T289">kušan </ts>
               <ts e="T291" id="Seg_3929" n="e" s="T290">ɨk </ts>
               <ts e="T292" id="Seg_3931" n="e" s="T291">šıp </ts>
               <ts e="T293" id="Seg_3933" n="e" s="T292">kətašik! </ts>
               <ts e="T294" id="Seg_3935" n="e" s="T293">Loːs </ts>
               <ts e="T295" id="Seg_3937" n="e" s="T294">taštɨ </ts>
               <ts e="T296" id="Seg_3939" n="e" s="T295">na </ts>
               <ts e="T297" id="Seg_3941" n="e" s="T296">soqɨčʼɛnnɨnta, </ts>
               <ts e="T298" id="Seg_3943" n="e" s="T297">Sɨlʼčʼa-Pɨlʼčʼat </ts>
               <ts e="T299" id="Seg_3945" n="e" s="T298">Qəš </ts>
               <ts e="T300" id="Seg_3947" n="e" s="T299">kučʼa </ts>
               <ts e="T301" id="Seg_3949" n="e" s="T300">qatɨsä. </ts>
               <ts e="T302" id="Seg_3951" n="e" s="T301">Mašıp </ts>
               <ts e="T303" id="Seg_3953" n="e" s="T302">kušenna </ts>
               <ts e="T304" id="Seg_3955" n="e" s="T303">šıp </ts>
               <ts e="T305" id="Seg_3957" n="e" s="T304">kətašik. </ts>
               <ts e="T306" id="Seg_3959" n="e" s="T305">Nılʼ </ts>
               <ts e="T307" id="Seg_3961" n="e" s="T306">äːkɨlʼčʼimpata </ts>
               <ts e="T308" id="Seg_3963" n="e" s="T307">täːqasä. </ts>
               <ts e="T309" id="Seg_3965" n="e" s="T308">Konna </ts>
               <ts e="T310" id="Seg_3967" n="e" s="T309">na </ts>
               <ts e="T311" id="Seg_3969" n="e" s="T310">qənta, </ts>
               <ts e="T312" id="Seg_3971" n="e" s="T311">nɨː </ts>
               <ts e="T313" id="Seg_3973" n="e" s="T312">mont </ts>
               <ts e="T314" id="Seg_3975" n="e" s="T313">čʼulʼ </ts>
               <ts e="T315" id="Seg_3977" n="e" s="T314">mɔːt. </ts>
               <ts e="T316" id="Seg_3979" n="e" s="T315">Naqät </ts>
               <ts e="T317" id="Seg_3981" n="e" s="T316">mɔːtan </ts>
               <ts e="T318" id="Seg_3983" n="e" s="T317">ɔːqqɨt </ts>
               <ts e="T319" id="Seg_3985" n="e" s="T318">aj </ts>
               <ts e="T320" id="Seg_3987" n="e" s="T319">pɔːrkä </ts>
               <ts e="T321" id="Seg_3989" n="e" s="T320">na </ts>
               <ts e="T322" id="Seg_3991" n="e" s="T321">meːntɨtɨ. </ts>
               <ts e="T323" id="Seg_3993" n="e" s="T322">Mɔːt </ts>
               <ts e="T324" id="Seg_3995" n="e" s="T323">šeːräšik. </ts>
               <ts e="T325" id="Seg_3997" n="e" s="T324">Qaː </ts>
               <ts e="T326" id="Seg_3999" n="e" s="T325">nɨŋantɨ, </ts>
               <ts e="T327" id="Seg_4001" n="e" s="T326">qantenint? </ts>
               <ts e="T328" id="Seg_4003" n="e" s="T327">–Mat </ts>
               <ts e="T329" id="Seg_4005" n="e" s="T328">qäntɨk </ts>
               <ts e="T330" id="Seg_4007" n="e" s="T329">šeːrtak, </ts>
               <ts e="T331" id="Seg_4009" n="e" s="T330">qumiːmɨ </ts>
               <ts e="T332" id="Seg_4011" n="e" s="T331">šıp </ts>
               <ts e="T333" id="Seg_4013" n="e" s="T332">qolʼčʼantɔːtɨt. </ts>
               <ts e="T334" id="Seg_4015" n="e" s="T333">Tan </ts>
               <ts e="T335" id="Seg_4017" n="e" s="T334">ola </ts>
               <ts e="T336" id="Seg_4019" n="e" s="T335">mɔːt </ts>
               <ts e="T337" id="Seg_4021" n="e" s="T336">šeːräšek. </ts>
               <ts e="T338" id="Seg_4023" n="e" s="T337">Na </ts>
               <ts e="T339" id="Seg_4025" n="e" s="T338">mɔːtan </ts>
               <ts e="T340" id="Seg_4027" n="e" s="T339">ɔːqqɨt </ts>
               <ts e="T341" id="Seg_4029" n="e" s="T340">aj </ts>
               <ts e="T342" id="Seg_4031" n="e" s="T341">pɔːrkä </ts>
               <ts e="T343" id="Seg_4033" n="e" s="T342">meːntɨt. </ts>
               <ts e="T344" id="Seg_4035" n="e" s="T343">Tan </ts>
               <ts e="T345" id="Seg_4037" n="e" s="T344">na pa </ts>
               <ts e="T346" id="Seg_4039" n="e" s="T345">mašıp </ts>
               <ts e="T347" id="Seg_4041" n="e" s="T346">ɨkɨ </ts>
               <ts e="T348" id="Seg_4043" n="e" s="T347">šıp </ts>
               <ts e="T349" id="Seg_4045" n="e" s="T348">kətäšik! </ts>
               <ts e="T350" id="Seg_4047" n="e" s="T349">Taːqasä </ts>
               <ts e="T351" id="Seg_4049" n="e" s="T350">äːkɨlʼčʼiŋɨtɨ. </ts>
               <ts e="T352" id="Seg_4051" n="e" s="T351">Mɔːt </ts>
               <ts e="T353" id="Seg_4053" n="e" s="T352">na </ts>
               <ts e="T354" id="Seg_4055" n="e" s="T353">šeːra </ts>
               <ts e="T355" id="Seg_4057" n="e" s="T354">tɨna </ts>
               <ts e="T356" id="Seg_4059" n="e" s="T355">qup </ts>
               <ts e="T357" id="Seg_4061" n="e" s="T356">əːtɨmɨntɨ. </ts>
               <ts e="T358" id="Seg_4063" n="e" s="T357">Munta </ts>
               <ts e="T359" id="Seg_4065" n="e" s="T358">na </ts>
               <ts e="T360" id="Seg_4067" n="e" s="T359">qolʼčʼintɔːtɨt </ts>
               <ts e="T361" id="Seg_4069" n="e" s="T360">na. </ts>
               <ts e="T362" id="Seg_4071" n="e" s="T361">Mumpa </ts>
               <ts e="T363" id="Seg_4073" n="e" s="T362">qaː </ts>
               <ts e="T364" id="Seg_4075" n="e" s="T363">šeːrna? </ts>
               <ts e="T365" id="Seg_4077" n="e" s="T364">–Aša, </ts>
               <ts e="T366" id="Seg_4079" n="e" s="T365">mumpa </ts>
               <ts e="T367" id="Seg_4081" n="e" s="T366">qupti </ts>
               <ts e="T368" id="Seg_4083" n="e" s="T367">mɔːttɨ </ts>
               <ts e="T369" id="Seg_4085" n="e" s="T368">šıp </ts>
               <ts e="T370" id="Seg_4087" n="e" s="T369">üːtɨsa. </ts>
               <ts e="T371" id="Seg_4089" n="e" s="T370">Qos </ts>
               <ts e="T372" id="Seg_4091" n="e" s="T371">qajelʼ </ts>
               <ts e="T373" id="Seg_4093" n="e" s="T372">qup </ts>
               <ts e="T374" id="Seg_4095" n="e" s="T373">tüsa, </ts>
               <ts e="T375" id="Seg_4097" n="e" s="T374">mašıp </ts>
               <ts e="T376" id="Seg_4099" n="e" s="T375">mɔːt </ts>
               <ts e="T377" id="Seg_4101" n="e" s="T376">šım </ts>
               <ts e="T378" id="Seg_4103" n="e" s="T377">üːtäsa. </ts>
               <ts e="T379" id="Seg_4105" n="e" s="T378">Konnɨ </ts>
               <ts e="T380" id="Seg_4107" n="e" s="T379">šep </ts>
               <ts e="T381" id="Seg_4109" n="e" s="T380">tattɨraltes. </ts>
               <ts e="T382" id="Seg_4111" n="e" s="T381">Täp </ts>
               <ts e="T383" id="Seg_4113" n="e" s="T382">čʼap </ts>
               <ts e="T384" id="Seg_4115" n="e" s="T383">qoŋɨtɨ </ts>
               <ts e="T385" id="Seg_4117" n="e" s="T384">mɔːtat </ts>
               <ts e="T386" id="Seg_4119" n="e" s="T385">nʼennalʼ </ts>
               <ts e="T387" id="Seg_4121" n="e" s="T386">pɛläqqɨt </ts>
               <ts e="T388" id="Seg_4123" n="e" s="T387">aj </ts>
               <ts e="T389" id="Seg_4125" n="e" s="T388">ukkɨr </ts>
               <ts e="T390" id="Seg_4127" n="e" s="T389">nʼäŋɨčʼa </ts>
               <ts e="T391" id="Seg_4129" n="e" s="T390">qup </ts>
               <ts e="T392" id="Seg_4131" n="e" s="T391">qomta. </ts>
               <ts e="T393" id="Seg_4133" n="e" s="T392">Nɨːno </ts>
               <ts e="T394" id="Seg_4135" n="e" s="T393">qa </ts>
               <ts e="T395" id="Seg_4137" n="e" s="T394">nılʼčʼik </ts>
               <ts e="T396" id="Seg_4139" n="e" s="T395">ɛsa: </ts>
               <ts e="T397" id="Seg_4141" n="e" s="T396">Qumɨp </ts>
               <ts e="T398" id="Seg_4143" n="e" s="T397">ınnä </ts>
               <ts e="T399" id="Seg_4145" n="e" s="T398">taqtaltɔːtet. </ts>
               <ts e="T400" id="Seg_4147" n="e" s="T399">Nɨːnɨ </ts>
               <ts e="T401" id="Seg_4149" n="e" s="T400">pona </ts>
               <ts e="T402" id="Seg_4151" n="e" s="T401">tantɨlʼä </ts>
               <ts e="T403" id="Seg_4153" n="e" s="T402">üŋkɨltɨkkolʼčʼimpatɨ </ts>
               <ts e="T404" id="Seg_4155" n="e" s="T403">loːsɨ </ts>
               <ts e="T405" id="Seg_4157" n="e" s="T404">kutɨlʼ </ts>
               <ts e="T406" id="Seg_4159" n="e" s="T405">mɔːntoːqɨt </ts>
               <ts e="T407" id="Seg_4161" n="e" s="T406">qompɨška. </ts>
               <ts e="T408" id="Seg_4163" n="e" s="T407">Loːsa </ts>
               <ts e="T409" id="Seg_4165" n="e" s="T408">qata </ts>
               <ts e="T410" id="Seg_4167" n="e" s="T409">qompɨšnä </ts>
               <ts e="T411" id="Seg_4169" n="e" s="T410">mɔːt </ts>
               <ts e="T412" id="Seg_4171" n="e" s="T411">šeːrnɨlɨt. </ts>
               <ts e="T413" id="Seg_4173" n="e" s="T412">Ukkɨr </ts>
               <ts e="T414" id="Seg_4175" n="e" s="T413">qup </ts>
               <ts e="T415" id="Seg_4177" n="e" s="T414">üŋkɨltɨmpɨŋɨjä. </ts>
               <ts e="T416" id="Seg_4179" n="e" s="T415">Mompa </ts>
               <ts e="T417" id="Seg_4181" n="e" s="T416">üːtɨlʼ </ts>
               <ts e="T418" id="Seg_4183" n="e" s="T417">kəntɨtɨ </ts>
               <ts e="T419" id="Seg_4185" n="e" s="T418">tokkɨčʼčʼenta, </ts>
               <ts e="T420" id="Seg_4187" n="e" s="T419">našat </ts>
               <ts e="T421" id="Seg_4189" n="e" s="T420">tantɨkkolʼčʼimpa </ts>
               <ts e="T422" id="Seg_4191" n="e" s="T421">(/qompačʼčʼentɨ). </ts>
               <ts e="T423" id="Seg_4193" n="e" s="T422">Seːpɨlak </ts>
               <ts e="T424" id="Seg_4195" n="e" s="T423">ɛːnta, </ts>
               <ts e="T425" id="Seg_4197" n="e" s="T424">ukkɨr </ts>
               <ts e="T426" id="Seg_4199" n="e" s="T425">tät </ts>
               <ts e="T427" id="Seg_4201" n="e" s="T426">čʼontoːqɨt </ts>
               <ts e="T428" id="Seg_4203" n="e" s="T427">qup </ts>
               <ts e="T429" id="Seg_4205" n="e" s="T428">na </ts>
               <ts e="T430" id="Seg_4207" n="e" s="T429">tannɨnta. </ts>
               <ts e="T431" id="Seg_4209" n="e" s="T430">Mompa </ts>
               <ts e="T432" id="Seg_4211" n="e" s="T431">qaret </ts>
               <ts e="T433" id="Seg_4213" n="e" s="T432">na </ts>
               <ts e="T434" id="Seg_4215" n="e" s="T433">qompɨčʼčʼa. </ts>
               <ts e="T435" id="Seg_4217" n="e" s="T434">Mɔːttɨ </ts>
               <ts e="T436" id="Seg_4219" n="e" s="T435">patqɨlna. </ts>
               <ts e="T437" id="Seg_4221" n="e" s="T436">Loːsɨ </ts>
               <ts e="T438" id="Seg_4223" n="e" s="T437">na </ts>
               <ts e="T439" id="Seg_4225" n="e" s="T438">qompɨčʼčʼa! </ts>
               <ts e="T440" id="Seg_4227" n="e" s="T439">A </ts>
               <ts e="T441" id="Seg_4229" n="e" s="T440">mompa </ts>
               <ts e="T442" id="Seg_4231" n="e" s="T441">nɔːtə </ts>
               <ts e="T443" id="Seg_4233" n="e" s="T442">taŋɨŋ </ts>
               <ts e="T444" id="Seg_4235" n="e" s="T443">ɔːmtɨŋɨlɨt, </ts>
               <ts e="T445" id="Seg_4237" n="e" s="T444">nʼi </ts>
               <ts e="T446" id="Seg_4239" n="e" s="T445">kušat </ts>
               <ts e="T447" id="Seg_4241" n="e" s="T446">ɨkɨ </ts>
               <ts e="T448" id="Seg_4243" n="e" s="T447">tantɨŋɨlit. </ts>
               <ts e="T449" id="Seg_4245" n="e" s="T448">Sɨlʼčʼi-Pɨlʼčʼit </ts>
               <ts e="T450" id="Seg_4247" n="e" s="T449">Qəš </ts>
               <ts e="T451" id="Seg_4249" n="e" s="T450">üŋkultimpetɨ, </ts>
               <ts e="T452" id="Seg_4251" n="e" s="T451">qaj? </ts>
               <ts e="T453" id="Seg_4253" n="e" s="T452">Konnä </ts>
               <ts e="T454" id="Seg_4255" n="e" s="T453">na </ts>
               <ts e="T455" id="Seg_4257" n="e" s="T454">tannɨntɨŋa </ts>
               <ts e="T456" id="Seg_4259" n="e" s="T455">(/tannɨnta). </ts>
               <ts e="T457" id="Seg_4261" n="e" s="T456">Peːkap </ts>
               <ts e="T458" id="Seg_4263" n="e" s="T457">qolʼčʼitɨ. </ts>
               <ts e="T459" id="Seg_4265" n="e" s="T458">Peːkä: </ts>
               <ts e="T460" id="Seg_4267" n="e" s="T459">čʼuk, </ts>
               <ts e="T461" id="Seg_4269" n="e" s="T460">čʼuk. </ts>
               <ts e="T462" id="Seg_4271" n="e" s="T461">Qaj </ts>
               <ts e="T463" id="Seg_4273" n="e" s="T462">Sɨlša-Pɨlʼša </ts>
               <ts e="T464" id="Seg_4275" n="e" s="T463">Qəš </ts>
               <ts e="T465" id="Seg_4277" n="e" s="T464">qattüsa? </ts>
               <ts e="T466" id="Seg_4279" n="e" s="T465">–Sɨlša-Pɨlʼša </ts>
               <ts e="T467" id="Seg_4281" n="e" s="T466">Qəš </ts>
               <ts e="T468" id="Seg_4283" n="e" s="T467">tɨntä </ts>
               <ts e="T469" id="Seg_4285" n="e" s="T468">qəssa. </ts>
               <ts e="T470" id="Seg_4287" n="e" s="T469">Konna </ts>
               <ts e="T471" id="Seg_4289" n="e" s="T470">na </ts>
               <ts e="T472" id="Seg_4291" n="e" s="T471">tünta, </ts>
               <ts e="T473" id="Seg_4293" n="e" s="T472">pɔːrkäp </ts>
               <ts e="T474" id="Seg_4295" n="e" s="T473">qoŋot. </ts>
               <ts e="T475" id="Seg_4297" n="e" s="T474">Pɔːrkä </ts>
               <ts e="T476" id="Seg_4299" n="e" s="T475">kətätɨ: </ts>
               <ts e="T477" id="Seg_4301" n="e" s="T476">Sɨlʼša-Pɨlʼša </ts>
               <ts e="T478" id="Seg_4303" n="e" s="T477">Qəš </ts>
               <ts e="T479" id="Seg_4305" n="e" s="T478">mɔːtqɨn </ts>
               <ts e="T480" id="Seg_4307" n="e" s="T479">ɔːmta. </ts>
               <ts e="T481" id="Seg_4309" n="e" s="T480">Loːsɨ </ts>
               <ts e="T482" id="Seg_4311" n="e" s="T481">karra </ts>
               <ts e="T483" id="Seg_4313" n="e" s="T482">kolʼimɔːlʼlʼä </ts>
               <ts e="T484" id="Seg_4315" n="e" s="T483">kurolʼna </ts>
               <ts e="T485" id="Seg_4317" n="e" s="T484">(/qənna), </ts>
               <ts e="T486" id="Seg_4319" n="e" s="T485">üttɨ </ts>
               <ts e="T487" id="Seg_4321" n="e" s="T486">alʼčʼa. </ts>
               <ts e="T488" id="Seg_4323" n="e" s="T487">Mɨta </ts>
               <ts e="T489" id="Seg_4325" n="e" s="T488">qur </ts>
               <ts e="T490" id="Seg_4327" n="e" s="T489">qäːš. </ts>
               <ts e="T491" id="Seg_4329" n="e" s="T490">Pona </ts>
               <ts e="T492" id="Seg_4331" n="e" s="T491">paktɨlʼä </ts>
               <ts e="T493" id="Seg_4333" n="e" s="T492">tıntena </ts>
               <ts e="T494" id="Seg_4335" n="e" s="T493">Sɨlʼčʼa-Pɨlʼčʼa </ts>
               <ts e="T495" id="Seg_4337" n="e" s="T494">Qəš </ts>
               <ts e="T496" id="Seg_4339" n="e" s="T495">pɔːrkämtɨ </ts>
               <ts e="T497" id="Seg_4341" n="e" s="T496">pačʼallä </ts>
               <ts e="T498" id="Seg_4343" n="e" s="T497">sılʼlʼaltätɨ, </ts>
               <ts e="T499" id="Seg_4345" n="e" s="T498">mačʼa </ts>
               <ts e="T500" id="Seg_4347" n="e" s="T499">qattɔːlnit. </ts>
               <ts e="T501" id="Seg_4349" n="e" s="T500">Jarɨk </ts>
               <ts e="T502" id="Seg_4351" n="e" s="T501">pɔːrkäp </ts>
               <ts e="T503" id="Seg_4353" n="e" s="T502">meːŋetɨ. </ts>
               <ts e="T504" id="Seg_4355" n="e" s="T503">Čʼarrä </ts>
               <ts e="T505" id="Seg_4357" n="e" s="T504">äkulʼčʼimpɨŋɨtɨ: </ts>
               <ts e="T506" id="Seg_4359" n="e" s="T505">Tat </ts>
               <ts e="T507" id="Seg_4361" n="e" s="T506">na pa </ts>
               <ts e="T508" id="Seg_4363" n="e" s="T507">mašıp </ts>
               <ts e="T509" id="Seg_4365" n="e" s="T508">nʼi </ts>
               <ts e="T510" id="Seg_4367" n="e" s="T509">kušat </ts>
               <ts e="T511" id="Seg_4369" n="e" s="T510">(/kušannɨ) </ts>
               <ts e="T512" id="Seg_4371" n="e" s="T511">ɨkɨ </ts>
               <ts e="T513" id="Seg_4373" n="e" s="T512">kətašik! </ts>
               <ts e="T514" id="Seg_4375" n="e" s="T513">Tiː </ts>
               <ts e="T515" id="Seg_4377" n="e" s="T514">tantɨkkolʼčʼimpa </ts>
               <ts e="T516" id="Seg_4379" n="e" s="T515">qarɨnɨlʼ </ts>
               <ts e="T517" id="Seg_4381" n="e" s="T516">pit. </ts>
               <ts e="T518" id="Seg_4383" n="e" s="T517">Nɨːnɨ </ts>
               <ts e="T519" id="Seg_4385" n="e" s="T518">pintɨ </ts>
               <ts e="T520" id="Seg_4387" n="e" s="T519">kuntɨ </ts>
               <ts e="T521" id="Seg_4389" n="e" s="T520">na </ts>
               <ts e="T522" id="Seg_4391" n="e" s="T521">ɔːmnentɔːtɨt. </ts>
               <ts e="T523" id="Seg_4393" n="e" s="T522">Qarnɨlʼ </ts>
               <ts e="T524" id="Seg_4395" n="e" s="T523">pit </ts>
               <ts e="T525" id="Seg_4397" n="e" s="T524">qumɨp </ts>
               <ts e="T526" id="Seg_4399" n="e" s="T525">kuralʼtɔːtɨt </ts>
               <ts e="T527" id="Seg_4401" n="e" s="T526">ponä, </ts>
               <ts e="T528" id="Seg_4403" n="e" s="T527">mannɨmpɔːtät, </ts>
               <ts e="T529" id="Seg_4405" n="e" s="T528">čʼɔːlsä </ts>
               <ts e="T530" id="Seg_4407" n="e" s="T529">aša </ts>
               <ts e="T531" id="Seg_4409" n="e" s="T530">tanta. </ts>
               <ts e="T532" id="Seg_4411" n="e" s="T531">Sɨlʼčʼa-Pɨlʼčʼa </ts>
               <ts e="T533" id="Seg_4413" n="e" s="T532">Qəš </ts>
               <ts e="T534" id="Seg_4415" n="e" s="T533">kuralʼtɔːtɨt: </ts>
               <ts e="T535" id="Seg_4417" n="e" s="T534">Üŋkɨltɨmpätɨ. </ts>
               <ts e="T536" id="Seg_4419" n="e" s="T535">Seːpɨlak </ts>
               <ts e="T537" id="Seg_4421" n="e" s="T536">qup </ts>
               <ts e="T538" id="Seg_4423" n="e" s="T537">na </ts>
               <ts e="T539" id="Seg_4425" n="e" s="T538">lʼämɨk </ts>
               <ts e="T540" id="Seg_4427" n="e" s="T539">ɛinta, </ts>
               <ts e="T541" id="Seg_4429" n="e" s="T540">mɔːttə </ts>
               <ts e="T542" id="Seg_4431" n="e" s="T541">na </ts>
               <ts e="T543" id="Seg_4433" n="e" s="T542">alʼčʼinta. </ts>
               <ts e="T544" id="Seg_4435" n="e" s="T543">Mɨtta </ts>
               <ts e="T545" id="Seg_4437" n="e" s="T544">ukkɨr </ts>
               <ts e="T546" id="Seg_4439" n="e" s="T545">loːsɨ </ts>
               <ts e="T547" id="Seg_4441" n="e" s="T546">qompɨšpa. </ts>
               <ts e="T548" id="Seg_4443" n="e" s="T547">Loːsɨ </ts>
               <ts e="T549" id="Seg_4445" n="e" s="T548">na </ts>
               <ts e="T550" id="Seg_4447" n="e" s="T549">qompɨšpa. </ts>
               <ts e="T551" id="Seg_4449" n="e" s="T550">Qapija </ts>
               <ts e="T552" id="Seg_4451" n="e" s="T551">Šilʼša-Palʼša </ts>
               <ts e="T553" id="Seg_4453" n="e" s="T552">ontə </ts>
               <ts e="T554" id="Seg_4455" n="e" s="T553">üŋkɨltimpatɨ. </ts>
               <ts e="T555" id="Seg_4457" n="e" s="T554">Na </ts>
               <ts e="T556" id="Seg_4459" n="e" s="T555">tannɨnta </ts>
               <ts e="T557" id="Seg_4461" n="e" s="T556">konnä. </ts>
               <ts e="T558" id="Seg_4463" n="e" s="T557">Kurɨlʼä </ts>
               <ts e="T559" id="Seg_4465" n="e" s="T558">tap </ts>
               <ts e="T560" id="Seg_4467" n="e" s="T559">tükkɨnä, </ts>
               <ts e="T561" id="Seg_4469" n="e" s="T560">kurɨlʼä </ts>
               <ts e="T562" id="Seg_4471" n="e" s="T561">tap </ts>
               <ts e="T563" id="Seg_4473" n="e" s="T562">tükkɨnä. </ts>
               <ts e="T564" id="Seg_4475" n="e" s="T563">Na </ts>
               <ts e="T565" id="Seg_4477" n="e" s="T564">qət </ts>
               <ts e="T566" id="Seg_4479" n="e" s="T565">pɔːrɨntɨ </ts>
               <ts e="T567" id="Seg_4481" n="e" s="T566">na </ts>
               <ts e="T568" id="Seg_4483" n="e" s="T567">tannɨnta. </ts>
               <ts e="T569" id="Seg_4485" n="e" s="T568">Tɨntäna </ts>
               <ts e="T570" id="Seg_4487" n="e" s="T569">pɔːrkäntɨ </ts>
               <ts e="T571" id="Seg_4489" n="e" s="T570">kurɨlʼä </ts>
               <ts e="T572" id="Seg_4491" n="e" s="T571">na </ts>
               <ts e="T573" id="Seg_4493" n="e" s="T572">tünta. </ts>
               <ts e="T574" id="Seg_4495" n="e" s="T573">Mɨta </ts>
               <ts e="T575" id="Seg_4497" n="e" s="T574">čʼuk, </ts>
               <ts e="T576" id="Seg_4499" n="e" s="T575">čʼuk, </ts>
               <ts e="T577" id="Seg_4501" n="e" s="T576">čʼuk. </ts>
               <ts e="T578" id="Seg_4503" n="e" s="T577">Qaj </ts>
               <ts e="T579" id="Seg_4505" n="e" s="T578">Sɨlʼčʼa-Pɨlʼčʼa </ts>
               <ts e="T580" id="Seg_4507" n="e" s="T579">Qəš </ts>
               <ts e="T581" id="Seg_4509" n="e" s="T580">qattüje, </ts>
               <ts e="T582" id="Seg_4511" n="e" s="T581">qaj </ts>
               <ts e="T583" id="Seg_4513" n="e" s="T582">mɔːtqɨn </ts>
               <ts e="T584" id="Seg_4515" n="e" s="T583">ɛːŋa? </ts>
               <ts e="T585" id="Seg_4517" n="e" s="T584">–Sɨlʼčʼa-Pɨlʼčʼa </ts>
               <ts e="T586" id="Seg_4519" n="e" s="T585">Qəš </ts>
               <ts e="T587" id="Seg_4521" n="e" s="T586">našat </ts>
               <ts e="T588" id="Seg_4523" n="e" s="T587">qəssa. </ts>
               <ts e="T589" id="Seg_4525" n="e" s="T588">Tat </ts>
               <ts e="T590" id="Seg_4527" n="e" s="T589">konna </ts>
               <ts e="T591" id="Seg_4529" n="e" s="T590">qəlla </ts>
               <ts e="T592" id="Seg_4531" n="e" s="T591">apsolʼ </ts>
               <ts e="T593" id="Seg_4533" n="e" s="T592">amtɨ. </ts>
               <ts e="T594" id="Seg_4535" n="e" s="T593">Toːnna </ts>
               <ts e="T595" id="Seg_4537" n="e" s="T594">kurɨlʼä </ts>
               <ts e="T596" id="Seg_4539" n="e" s="T595">na </ts>
               <ts e="T597" id="Seg_4541" n="e" s="T596">qəntanəəə, </ts>
               <ts e="T598" id="Seg_4543" n="e" s="T597">mɔːtan </ts>
               <ts e="T599" id="Seg_4545" n="e" s="T598">ɔːkɨlʼ </ts>
               <ts e="T600" id="Seg_4547" n="e" s="T599">tüntanəəə, </ts>
               <ts e="T601" id="Seg_4549" n="e" s="T600">pɔːrkäntɨ </ts>
               <ts e="T602" id="Seg_4551" n="e" s="T601">tüːŋa. </ts>
               <ts e="T603" id="Seg_4553" n="e" s="T602">Qattüsa? </ts>
               <ts e="T604" id="Seg_4555" n="e" s="T603">–Sɨlʼčʼa-Pɨlʼčʼa </ts>
               <ts e="T605" id="Seg_4557" n="e" s="T604">Qəš </ts>
               <ts e="T606" id="Seg_4559" n="e" s="T605">tep </ts>
               <ts e="T607" id="Seg_4561" n="e" s="T606">aša </ts>
               <ts e="T608" id="Seg_4563" n="e" s="T607">täːlʼa </ts>
               <ts e="T609" id="Seg_4565" n="e" s="T608">našak </ts>
               <ts e="T610" id="Seg_4567" n="e" s="T609">qəssa </ts>
               <ts e="T611" id="Seg_4569" n="e" s="T610">(/qəssɨŋa). </ts>
               <ts e="T612" id="Seg_4571" n="e" s="T611">Mɔːttɨ </ts>
               <ts e="T613" id="Seg_4573" n="e" s="T612">šeːrlʼa </ts>
               <ts e="T614" id="Seg_4575" n="e" s="T613">apsolʼ </ts>
               <ts e="T615" id="Seg_4577" n="e" s="T614">amtɨ. </ts>
               <ts e="T616" id="Seg_4579" n="e" s="T615">Ukkɨr </ts>
               <ts e="T617" id="Seg_4581" n="e" s="T616">čʼontoːqɨt </ts>
               <ts e="T618" id="Seg_4583" n="e" s="T617">loːsɨ </ts>
               <ts e="T619" id="Seg_4585" n="e" s="T618">mɔːttɨ </ts>
               <ts e="T620" id="Seg_4587" n="e" s="T619">na </ts>
               <ts e="T621" id="Seg_4589" n="e" s="T620">noqɔːlta, </ts>
               <ts e="T622" id="Seg_4591" n="e" s="T621">mɔːttɨ </ts>
               <ts e="T623" id="Seg_4593" n="e" s="T622">na </ts>
               <ts e="T624" id="Seg_4595" n="e" s="T623">noqqɔːlʼta. </ts>
               <ts e="T625" id="Seg_4597" n="e" s="T624">Mɔːttɨ </ts>
               <ts e="T626" id="Seg_4599" n="e" s="T625">čʼep </ts>
               <ts e="T627" id="Seg_4601" n="e" s="T626">šeːrna </ts>
               <ts e="T628" id="Seg_4603" n="e" s="T627">loːsɨ, </ts>
               <ts e="T629" id="Seg_4605" n="e" s="T628">Sɨlʼčʼa-Pɨlʼčʼa </ts>
               <ts e="T630" id="Seg_4607" n="e" s="T629">Qəš </ts>
               <ts e="T631" id="Seg_4609" n="e" s="T630">počʼčʼalʼnɨtɨ </ts>
               <ts e="T632" id="Seg_4611" n="e" s="T631">olantɨ </ts>
               <ts e="T633" id="Seg_4613" n="e" s="T632">laka. </ts>
               <ts e="T634" id="Seg_4615" n="e" s="T633">Mɔːttɨ </ts>
               <ts e="T635" id="Seg_4617" n="e" s="T634">šuː </ts>
               <ts e="T636" id="Seg_4619" n="e" s="T635">nʼanna </ts>
               <ts e="T637" id="Seg_4621" n="e" s="T636">püŋkolʼna. </ts>
               <ts e="T638" id="Seg_4623" n="e" s="T637">Kəpɨntɨ </ts>
               <ts e="T639" id="Seg_4625" n="e" s="T638">laka </ts>
               <ts e="T640" id="Seg_4627" n="e" s="T639">qottä </ts>
               <ts e="T641" id="Seg_4629" n="e" s="T640">ponä </ts>
               <ts e="T642" id="Seg_4631" n="e" s="T641">alʼčʼa. </ts>
               <ts e="T643" id="Seg_4633" n="e" s="T642">Qəttentit. </ts>
               <ts e="T644" id="Seg_4635" n="e" s="T643">Ponä </ts>
               <ts e="T645" id="Seg_4637" n="e" s="T644">tattɨŋɨt </ts>
               <ts e="T646" id="Seg_4639" n="e" s="T645">tä </ts>
               <ts e="T647" id="Seg_4641" n="e" s="T646">Sɨlʼčʼa-Pɨlʼčʼa </ts>
               <ts e="T648" id="Seg_4643" n="e" s="T647">Qəš </ts>
               <ts e="T649" id="Seg_4645" n="e" s="T648">kəpɨntɨ </ts>
               <ts e="T650" id="Seg_4647" n="e" s="T649">laka. </ts>
               <ts e="T651" id="Seg_4649" n="e" s="T650">Ponä </ts>
               <ts e="T652" id="Seg_4651" n="e" s="T651">tanta, </ts>
               <ts e="T653" id="Seg_4653" n="e" s="T652">monte </ts>
               <ts e="T654" id="Seg_4655" n="e" s="T653">čʼeːlɨŋɛlʼčʼa. </ts>
               <ts e="T655" id="Seg_4657" n="e" s="T654">Karrə </ts>
               <ts e="T656" id="Seg_4659" n="e" s="T655">tulʼtɨŋɨt </ts>
               <ts e="T657" id="Seg_4661" n="e" s="T656">Sɨlʼčʼa-Pɨlʼčʼa </ts>
               <ts e="T658" id="Seg_4663" n="e" s="T657">Qəš </ts>
               <ts e="T659" id="Seg_4665" n="e" s="T658">kəpɨntɨ </ts>
               <ts e="T660" id="Seg_4667" n="e" s="T659">lakap, </ts>
               <ts e="T661" id="Seg_4669" n="e" s="T660">ütɨlʼ </ts>
               <ts e="T662" id="Seg_4671" n="e" s="T661">qəqontə </ts>
               <ts e="T663" id="Seg_4673" n="e" s="T662">pinnete. </ts>
               <ts e="T664" id="Seg_4675" n="e" s="T663">Ulqosä </ts>
               <ts e="T665" id="Seg_4677" n="e" s="T664">nʼentɨ </ts>
               <ts e="T666" id="Seg_4679" n="e" s="T665">qantäptitɨ, </ts>
               <ts e="T667" id="Seg_4681" n="e" s="T666">nılʼčʼik </ts>
               <ts e="T668" id="Seg_4683" n="e" s="T667">pinnete. </ts>
               <ts e="T669" id="Seg_4685" n="e" s="T668">Ütɨlʼ </ts>
               <ts e="T670" id="Seg_4687" n="e" s="T669">qəqɨntɨ </ts>
               <ts e="T671" id="Seg_4689" n="e" s="T670">qanɨktɨ </ts>
               <ts e="T672" id="Seg_4691" n="e" s="T671">nılʼčʼik </ts>
               <ts e="T673" id="Seg_4693" n="e" s="T672">pinnɨt. </ts>
               <ts e="T674" id="Seg_4695" n="e" s="T673">Mit </ts>
               <ts e="T675" id="Seg_4697" n="e" s="T674">ola </ts>
               <ts e="T676" id="Seg_4699" n="e" s="T675">ilɨlʼä </ts>
               <ts e="T677" id="Seg_4701" n="e" s="T676">ippa. </ts>
               <ts e="T678" id="Seg_4703" n="e" s="T677">Olɨntäsä </ts>
               <ts e="T679" id="Seg_4705" n="e" s="T678">nʼentɨ </ts>
               <ts e="T680" id="Seg_4707" n="e" s="T679">loːqaltɨŋɨtɨ. </ts>
               <ts e="T681" id="Seg_4709" n="e" s="T680">Na </ts>
               <ts e="T682" id="Seg_4711" n="e" s="T681">čʼeːlatɨŋɔːmɨt. </ts>
               <ts e="T683" id="Seg_4713" n="e" s="T682">Lɨpkomɔːta. </ts>
               <ts e="T684" id="Seg_4715" n="e" s="T683">Mompa </ts>
               <ts e="T685" id="Seg_4717" n="e" s="T684">kekkɨsä </ts>
               <ts e="T686" id="Seg_4719" n="e" s="T685">šittäqıj, </ts>
               <ts e="T687" id="Seg_4721" n="e" s="T686">jarɨk </ts>
               <ts e="T688" id="Seg_4723" n="e" s="T687">mɨ </ts>
               <ts e="T689" id="Seg_4725" n="e" s="T688">ičʼeqa, </ts>
               <ts e="T690" id="Seg_4727" n="e" s="T689">aše </ts>
               <ts e="T691" id="Seg_4729" n="e" s="T690">tɛnɨmɔːmɨt. </ts>
               <ts e="T692" id="Seg_4731" n="e" s="T691">Üːtɨt </ts>
               <ts e="T693" id="Seg_4733" n="e" s="T692">na </ts>
               <ts e="T694" id="Seg_4735" n="e" s="T693">lʼipkɨmɔːnna. </ts>
               <ts e="T695" id="Seg_4737" n="e" s="T694">Üːtɨt </ts>
               <ts e="T696" id="Seg_4739" n="e" s="T695">seːpɨlak </ts>
               <ts e="T697" id="Seg_4741" n="e" s="T696">ɔːmta. </ts>
               <ts e="T698" id="Seg_4743" n="e" s="T697">Na </ts>
               <ts e="T699" id="Seg_4745" n="e" s="T698">rɛmkɨmɔːtqolamna. </ts>
               <ts e="T700" id="Seg_4747" n="e" s="T699">Qumɨp </ts>
               <ts e="T701" id="Seg_4749" n="e" s="T700">ponä </ts>
               <ts e="T702" id="Seg_4751" n="e" s="T701">kuralʼtɨtä: </ts>
               <ts e="T703" id="Seg_4753" n="e" s="T702">Üŋkɨlʼtɨmpätɨ. </ts>
               <ts e="T704" id="Seg_4755" n="e" s="T703">Qup </ts>
               <ts e="T705" id="Seg_4757" n="e" s="T704">poːqɨt </ts>
               <ts e="T706" id="Seg_4759" n="e" s="T705">nɨŋa. </ts>
               <ts e="T707" id="Seg_4761" n="e" s="T706">Qəː! </ts>
               <ts e="T708" id="Seg_4763" n="e" s="T707">Qum </ts>
               <ts e="T709" id="Seg_4765" n="e" s="T708">na </ts>
               <ts e="T710" id="Seg_4767" n="e" s="T709">qompɨšqolamna! </ts>
               <ts e="T711" id="Seg_4769" n="e" s="T710">Mɔːttɨ </ts>
               <ts e="T712" id="Seg_4771" n="e" s="T711">patqɨlna. </ts>
               <ts e="T713" id="Seg_4773" n="e" s="T712">Mɔːttɨ </ts>
               <ts e="T714" id="Seg_4775" n="e" s="T713">šeːrlʼä </ts>
               <ts e="T715" id="Seg_4777" n="e" s="T714">nılʼčʼik </ts>
               <ts e="T716" id="Seg_4779" n="e" s="T715">kätɨmpat: </ts>
               <ts e="T717" id="Seg_4781" n="e" s="T716">Loːsɨ </ts>
               <ts e="T718" id="Seg_4783" n="e" s="T717">karrät </ts>
               <ts e="T719" id="Seg_4785" n="e" s="T718">qompɨnʼnʼä. </ts>
               <ts e="T720" id="Seg_4787" n="e" s="T719">Konna </ts>
               <ts e="T721" id="Seg_4789" n="e" s="T720">kurɨlʼlʼä </ts>
               <ts e="T722" id="Seg_4791" n="e" s="T721">na </ts>
               <ts e="T723" id="Seg_4793" n="e" s="T722">tannɨnta. </ts>
               <ts e="T724" id="Seg_4795" n="e" s="T723">–Konnä </ts>
               <ts e="T725" id="Seg_4797" n="e" s="T724">qaj </ts>
               <ts e="T726" id="Seg_4799" n="e" s="T725">qompɨčʼčʼe? </ts>
               <ts e="T727" id="Seg_4801" n="e" s="T726">Aaa! </ts>
               <ts e="T728" id="Seg_4803" n="e" s="T727">Tat </ts>
               <ts e="T729" id="Seg_4805" n="e" s="T728">qaj </ts>
               <ts e="T730" id="Seg_4807" n="e" s="T729">kočʼkɨmɔːnantɨ, </ts>
               <ts e="T731" id="Seg_4809" n="e" s="T730">amɨrɛlʼčʼinantɨ, </ts>
               <ts e="T732" id="Seg_4811" n="e" s="T731">narkɨmɔːtijantɨ? </ts>
               <ts e="T733" id="Seg_4813" n="e" s="T732">Ukkur </ts>
               <ts e="T734" id="Seg_4815" n="e" s="T733">nʼeːmtɨ </ts>
               <ts e="T735" id="Seg_4817" n="e" s="T734">nɨlʼčʼik </ts>
               <ts e="T736" id="Seg_4819" n="e" s="T735">qoŋɨtɨ. </ts>
               <ts e="T737" id="Seg_4821" n="e" s="T736">Man </ts>
               <ts e="T738" id="Seg_4823" n="e" s="T737">okoːt </ts>
               <ts e="T739" id="Seg_4825" n="e" s="T738">amɨrrɛːlʼä </ts>
               <ts e="T740" id="Seg_4827" n="e" s="T739">nılʼčʼik </ts>
               <ts e="T741" id="Seg_4829" n="e" s="T740">ippɨkkolʼčʼimpɨkkak. </ts>
               <ts e="T742" id="Seg_4831" n="e" s="T741">Nɨːnɨ </ts>
               <ts e="T743" id="Seg_4833" n="e" s="T742">konna </ts>
               <ts e="T744" id="Seg_4835" n="e" s="T743">kurɨlʼä </ts>
               <ts e="T745" id="Seg_4837" n="e" s="T744">na </ts>
               <ts e="T746" id="Seg_4839" n="e" s="T745">tünta. </ts>
               <ts e="T747" id="Seg_4841" n="e" s="T746">Konna </ts>
               <ts e="T748" id="Seg_4843" n="e" s="T747">kürɨlʼä </ts>
               <ts e="T749" id="Seg_4845" n="e" s="T748">na </ts>
               <ts e="T750" id="Seg_4847" n="e" s="T749">tünta. </ts>
               <ts e="T751" id="Seg_4849" n="e" s="T750">Čʼuk, </ts>
               <ts e="T752" id="Seg_4851" n="e" s="T751">čʼuk. </ts>
               <ts e="T753" id="Seg_4853" n="e" s="T752">Qaj </ts>
               <ts e="T754" id="Seg_4855" n="e" s="T753">Sɨlʼčʼa-Pɨlʼčʼa </ts>
               <ts e="T755" id="Seg_4857" n="e" s="T754">Qəš </ts>
               <ts e="T756" id="Seg_4859" n="e" s="T755">qattülʼčʼa </ts>
               <ts e="T757" id="Seg_4861" n="e" s="T756">(/tüŋa)? </ts>
               <ts e="T758" id="Seg_4863" n="e" s="T757">Qaj </ts>
               <ts e="T759" id="Seg_4865" n="e" s="T758">qəssa, </ts>
               <ts e="T760" id="Seg_4867" n="e" s="T759">qaj </ts>
               <ts e="T761" id="Seg_4869" n="e" s="T760">qattüssa? </ts>
               <ts e="T762" id="Seg_4871" n="e" s="T761">–Sɨlʼčʼa-Pɨlʼčʼa </ts>
               <ts e="T763" id="Seg_4873" n="e" s="T762">Qəš </ts>
               <ts e="T764" id="Seg_4875" n="e" s="T763">okoːn </ts>
               <ts e="T765" id="Seg_4877" n="e" s="T764">naša </ts>
               <ts e="T766" id="Seg_4879" n="e" s="T765">qəssa, </ts>
               <ts e="T767" id="Seg_4881" n="e" s="T766">tıntɨ </ts>
               <ts e="T768" id="Seg_4883" n="e" s="T767">naša </ts>
               <ts e="T769" id="Seg_4885" n="e" s="T768">qəssa. </ts>
               <ts e="T770" id="Seg_4887" n="e" s="T769">Tat </ts>
               <ts e="T771" id="Seg_4889" n="e" s="T770">konna </ts>
               <ts e="T772" id="Seg_4891" n="e" s="T771">qəlla </ts>
               <ts e="T773" id="Seg_4893" n="e" s="T772">apsolʼ </ts>
               <ts e="T774" id="Seg_4895" n="e" s="T773">amtɨ. </ts>
               <ts e="T775" id="Seg_4897" n="e" s="T774">Nɨːna </ts>
               <ts e="T776" id="Seg_4899" n="e" s="T775">konna </ts>
               <ts e="T777" id="Seg_4901" n="e" s="T776">qəllä </ts>
               <ts e="T778" id="Seg_4903" n="e" s="T777">(/kurlʼä) </ts>
               <ts e="T779" id="Seg_4905" n="e" s="T778">tıntena </ts>
               <ts e="T780" id="Seg_4907" n="e" s="T779">pɔːrkəntɨ </ts>
               <ts e="T781" id="Seg_4909" n="e" s="T780">tüːŋa. </ts>
               <ts e="T782" id="Seg_4911" n="e" s="T781">Kättɨtɨ: </ts>
               <ts e="T783" id="Seg_4913" n="e" s="T782">Qaj </ts>
               <ts e="T784" id="Seg_4915" n="e" s="T783">Sɨlʼčʼa-Pɨlʼčʼa </ts>
               <ts e="T785" id="Seg_4917" n="e" s="T784">Qəš </ts>
               <ts e="T786" id="Seg_4919" n="e" s="T785">mɔːtqɨn </ts>
               <ts e="T787" id="Seg_4921" n="e" s="T786">ɛːiŋa? </ts>
               <ts e="T788" id="Seg_4923" n="e" s="T787">–Täm </ts>
               <ts e="T789" id="Seg_4925" n="e" s="T788">aša </ts>
               <ts e="T790" id="Seg_4927" n="e" s="T789">našat </ts>
               <ts e="T791" id="Seg_4929" n="e" s="T790">tɨntä </ts>
               <ts e="T792" id="Seg_4931" n="e" s="T791">qəssa. </ts>
               <ts e="T793" id="Seg_4933" n="e" s="T792">Mɔːt </ts>
               <ts e="T794" id="Seg_4935" n="e" s="T793">šeːrlʼa </ts>
               <ts e="T795" id="Seg_4937" n="e" s="T794">apsolʼ </ts>
               <ts e="T796" id="Seg_4939" n="e" s="T795">amtɨ. </ts>
               <ts e="T797" id="Seg_4941" n="e" s="T796">Ɔːm </ts>
               <ts e="T798" id="Seg_4943" n="e" s="T797">aj </ts>
               <ts e="T799" id="Seg_4945" n="e" s="T798">nɨlʼčʼik </ts>
               <ts e="T800" id="Seg_4947" n="e" s="T799">ɛssa: </ts>
               <ts e="T801" id="Seg_4949" n="e" s="T800">Mɔːttɨ </ts>
               <ts e="T802" id="Seg_4951" n="e" s="T801">qaj </ts>
               <ts e="T803" id="Seg_4953" n="e" s="T802">šeːrtak, </ts>
               <ts e="T804" id="Seg_4955" n="e" s="T803">qaj </ts>
               <ts e="T805" id="Seg_4957" n="e" s="T804">aša </ts>
               <ts e="T806" id="Seg_4959" n="e" s="T805">šeːrtak,– </ts>
               <ts e="T807" id="Seg_4961" n="e" s="T806">ɔːm </ts>
               <ts e="T808" id="Seg_4963" n="e" s="T807">aj </ts>
               <ts e="T809" id="Seg_4965" n="e" s="T808">mɨ. </ts>
               <ts e="T810" id="Seg_4967" n="e" s="T809">Pɔːrkä </ts>
               <ts e="T811" id="Seg_4969" n="e" s="T810">qaj </ts>
               <ts e="T812" id="Seg_4971" n="e" s="T811">mompa </ts>
               <ts e="T813" id="Seg_4973" n="e" s="T812">mašıp </ts>
               <ts e="T814" id="Seg_4975" n="e" s="T813">kurɨmmanta? </ts>
               <ts e="T815" id="Seg_4977" n="e" s="T814">Qapı </ts>
               <ts e="T816" id="Seg_4979" n="e" s="T815">kulʼtɨmpa </ts>
               <ts e="T817" id="Seg_4981" n="e" s="T816">(/kulʼtɨmpɨŋa): </ts>
               <ts e="T818" id="Seg_4983" n="e" s="T817">Ɔːmij </ts>
               <ts e="T819" id="Seg_4985" n="e" s="T818">qaj </ts>
               <ts e="T820" id="Seg_4987" n="e" s="T819">šeːrta, </ts>
               <ts e="T821" id="Seg_4989" n="e" s="T820">qaj </ts>
               <ts e="T822" id="Seg_4991" n="e" s="T821">aša </ts>
               <ts e="T823" id="Seg_4993" n="e" s="T822">šeːrta. </ts>
               <ts e="T824" id="Seg_4995" n="e" s="T823">Okkɨr </ts>
               <ts e="T825" id="Seg_4997" n="e" s="T824">čʼontoːqɨt </ts>
               <ts e="T826" id="Seg_4999" n="e" s="T825">na </ts>
               <ts e="T827" id="Seg_5001" n="e" s="T826">šeːrqolamta. </ts>
               <ts e="T828" id="Seg_5003" n="e" s="T827">Olɨmtɨ </ts>
               <ts e="T829" id="Seg_5005" n="e" s="T828">mɔːttɨ </ts>
               <ts e="T830" id="Seg_5007" n="e" s="T829">čʼap </ts>
               <ts e="T831" id="Seg_5009" n="e" s="T830">noqqɔːlnɨt, </ts>
               <ts e="T832" id="Seg_5011" n="e" s="T831">Sɨlʼčʼa-Pɨlʼčʼa </ts>
               <ts e="T833" id="Seg_5013" n="e" s="T832">Qəš </ts>
               <ts e="T834" id="Seg_5015" n="e" s="T833">pačʼalnɨt </ts>
               <ts e="T835" id="Seg_5017" n="e" s="T834">olantɨ </ts>
               <ts e="T836" id="Seg_5019" n="e" s="T835">lako. </ts>
               <ts e="T837" id="Seg_5021" n="e" s="T836">Mɔːttɨ </ts>
               <ts e="T838" id="Seg_5023" n="e" s="T837">nʼannä </ts>
               <ts e="T839" id="Seg_5025" n="e" s="T838">alʼčʼa, </ts>
               <ts e="T840" id="Seg_5027" n="e" s="T839">püŋkolʼna. </ts>
               <ts e="T841" id="Seg_5029" n="e" s="T840">Kopɨntɨ </ts>
               <ts e="T842" id="Seg_5031" n="e" s="T841">laka </ts>
               <ts e="T843" id="Seg_5033" n="e" s="T842">qottä </ts>
               <ts e="T844" id="Seg_5035" n="e" s="T843">ponä </ts>
               <ts e="T845" id="Seg_5037" n="e" s="T844">alʼčʼa. </ts>
               <ts e="T846" id="Seg_5039" n="e" s="T845">Qəttɛːŋɨtɨ. </ts>
               <ts e="T847" id="Seg_5041" n="e" s="T846">Nɨːnɨ </ts>
               <ts e="T848" id="Seg_5043" n="e" s="T847">nɔːtɨ </ts>
               <ts e="T849" id="Seg_5045" n="e" s="T848">čʼap </ts>
               <ts e="T850" id="Seg_5047" n="e" s="T849">ɔːmtɔːtɨt, </ts>
               <ts e="T851" id="Seg_5049" n="e" s="T850">ɔːmtɔːtɨt, </ts>
               <ts e="T852" id="Seg_5051" n="e" s="T851">nʼi </ts>
               <ts e="T853" id="Seg_5053" n="e" s="T852">qaj </ts>
               <ts e="T854" id="Seg_5055" n="e" s="T853">čʼäŋka. </ts>
               <ts e="T855" id="Seg_5057" n="e" s="T854">Mumpa </ts>
               <ts e="T856" id="Seg_5059" n="e" s="T855">qaj </ts>
               <ts e="T857" id="Seg_5061" n="e" s="T856">šite </ts>
               <ts e="T858" id="Seg_5063" n="e" s="T857">ičʼa. </ts>
               <ts e="T859" id="Seg_5065" n="e" s="T858">Tɔːptɨlʼ </ts>
               <ts e="T860" id="Seg_5067" n="e" s="T859">qarɨt </ts>
               <ts e="T861" id="Seg_5069" n="e" s="T860">ınna </ts>
               <ts e="T862" id="Seg_5071" n="e" s="T861">čʼeːlɨŋna. </ts>
               <ts e="T863" id="Seg_5073" n="e" s="T862">Mompa </ts>
               <ts e="T864" id="Seg_5075" n="e" s="T863">poː </ts>
               <ts e="T865" id="Seg_5077" n="e" s="T864">pačʼčʼalʼnɨlɨt. </ts>
               <ts e="T866" id="Seg_5079" n="e" s="T865">Poː </ts>
               <ts e="T867" id="Seg_5081" n="e" s="T866">pačʼčʼalnɔːtɨt. </ts>
               <ts e="T868" id="Seg_5083" n="e" s="T867">Tü </ts>
               <ts e="T869" id="Seg_5085" n="e" s="T868">čʼɔːtɨŋɨlɨt. </ts>
               <ts e="T870" id="Seg_5087" n="e" s="T869">Əːtɨmɨntɨ </ts>
               <ts e="T871" id="Seg_5089" n="e" s="T870">poːp </ts>
               <ts e="T872" id="Seg_5091" n="e" s="T871">pačʼčʼalʼnɔːtɨt. </ts>
               <ts e="T873" id="Seg_5093" n="e" s="T872">Šittäqıp </ts>
               <ts e="T874" id="Seg_5095" n="e" s="T873">karrä </ts>
               <ts e="T875" id="Seg_5097" n="e" s="T874">tultoktät, </ts>
               <ts e="T876" id="Seg_5099" n="e" s="T875">tüsä </ts>
               <ts e="T877" id="Seg_5101" n="e" s="T876">čʼɔːtɔːtɨt, </ts>
               <ts e="T878" id="Seg_5103" n="e" s="T877">koptɨkɔːlɨk </ts>
               <ts e="T879" id="Seg_5105" n="e" s="T878">tüsä </ts>
               <ts e="T880" id="Seg_5107" n="e" s="T879">čʼɔːtɔːtɨt. </ts>
               <ts e="T881" id="Seg_5109" n="e" s="T880">Iralʼ </ts>
               <ts e="T882" id="Seg_5111" n="e" s="T881">äsäsɨt, </ts>
               <ts e="T883" id="Seg_5113" n="e" s="T882">na </ts>
               <ts e="T884" id="Seg_5115" n="e" s="T883">ira </ts>
               <ts e="T885" id="Seg_5117" n="e" s="T884">nälʼätɨ </ts>
               <ts e="T886" id="Seg_5119" n="e" s="T885">ɛːppa. </ts>
               <ts e="T887" id="Seg_5121" n="e" s="T886">Na </ts>
               <ts e="T888" id="Seg_5123" n="e" s="T887">nälʼamtɨ </ts>
               <ts e="T889" id="Seg_5125" n="e" s="T888">Sɨlʼčʼa-Pɨlʼčʼa </ts>
               <ts e="T890" id="Seg_5127" n="e" s="T889">Qəštɨ </ts>
               <ts e="T891" id="Seg_5129" n="e" s="T890">miŋatɨ. </ts>
               <ts e="T892" id="Seg_5131" n="e" s="T891">Na </ts>
               <ts e="T893" id="Seg_5133" n="e" s="T892">təttɨt </ts>
               <ts e="T894" id="Seg_5135" n="e" s="T893">moːrɨ </ts>
               <ts e="T895" id="Seg_5137" n="e" s="T894">kolʼalʼtɨntäkkɨt, </ts>
               <ts e="T896" id="Seg_5139" n="e" s="T895">šölʼqumɨtɨtkine </ts>
               <ts e="T897" id="Seg_5141" n="e" s="T896">tüŋa. </ts>
               <ts e="T898" id="Seg_5143" n="e" s="T897">Moːrɨt </ts>
               <ts e="T899" id="Seg_5145" n="e" s="T898">na </ts>
               <ts e="T900" id="Seg_5147" n="e" s="T899">ɛːnta. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1" id="Seg_5148" s="T0">KAI_1965_SylchaPylcha1_flk.001 (001)</ta>
            <ta e="T4" id="Seg_5149" s="T1">KAI_1965_SylchaPylcha1_flk.002 (002.001)</ta>
            <ta e="T6" id="Seg_5150" s="T4">KAI_1965_SylchaPylcha1_flk.003 (002.002)</ta>
            <ta e="T8" id="Seg_5151" s="T6">KAI_1965_SylchaPylcha1_flk.004 (002.003)</ta>
            <ta e="T10" id="Seg_5152" s="T8">KAI_1965_SylchaPylcha1_flk.005 (002.004)</ta>
            <ta e="T24" id="Seg_5153" s="T10">KAI_1965_SylchaPylcha1_flk.006 (002.005)</ta>
            <ta e="T27" id="Seg_5154" s="T24">KAI_1965_SylchaPylcha1_flk.007 (002.006)</ta>
            <ta e="T37" id="Seg_5155" s="T27">KAI_1965_SylchaPylcha1_flk.008 (002.007)</ta>
            <ta e="T45" id="Seg_5156" s="T37">KAI_1965_SylchaPylcha1_flk.009 (002.008)</ta>
            <ta e="T51" id="Seg_5157" s="T45">KAI_1965_SylchaPylcha1_flk.010 (002.009)</ta>
            <ta e="T59" id="Seg_5158" s="T51">KAI_1965_SylchaPylcha1_flk.011 (002.010)</ta>
            <ta e="T64" id="Seg_5159" s="T59">KAI_1965_SylchaPylcha1_flk.012 (002.011)</ta>
            <ta e="T71" id="Seg_5160" s="T64">KAI_1965_SylchaPylcha1_flk.013 (003.001)</ta>
            <ta e="T75" id="Seg_5161" s="T71">KAI_1965_SylchaPylcha1_flk.014 (003.002)</ta>
            <ta e="T78" id="Seg_5162" s="T75">KAI_1965_SylchaPylcha1_flk.015 (003.003)</ta>
            <ta e="T84" id="Seg_5163" s="T78">KAI_1965_SylchaPylcha1_flk.016 (003.004)</ta>
            <ta e="T89" id="Seg_5164" s="T84">KAI_1965_SylchaPylcha1_flk.017 (003.005)</ta>
            <ta e="T94" id="Seg_5165" s="T89">KAI_1965_SylchaPylcha1_flk.018 (003.006)</ta>
            <ta e="T99" id="Seg_5166" s="T94">KAI_1965_SylchaPylcha1_flk.019 (003.007)</ta>
            <ta e="T105" id="Seg_5167" s="T99">KAI_1965_SylchaPylcha1_flk.020 (003.008)</ta>
            <ta e="T109" id="Seg_5168" s="T105">KAI_1965_SylchaPylcha1_flk.021 (003.009)</ta>
            <ta e="T113" id="Seg_5169" s="T109">KAI_1965_SylchaPylcha1_flk.022 (003.010)</ta>
            <ta e="T122" id="Seg_5170" s="T113">KAI_1965_SylchaPylcha1_flk.023 (003.011)</ta>
            <ta e="T126" id="Seg_5171" s="T122">KAI_1965_SylchaPylcha1_flk.024 (003.012)</ta>
            <ta e="T132" id="Seg_5172" s="T126">KAI_1965_SylchaPylcha1_flk.025 (003.013)</ta>
            <ta e="T139" id="Seg_5173" s="T132">KAI_1965_SylchaPylcha1_flk.026 (003.014)</ta>
            <ta e="T143" id="Seg_5174" s="T139">KAI_1965_SylchaPylcha1_flk.027 (003.015)</ta>
            <ta e="T147" id="Seg_5175" s="T143">KAI_1965_SylchaPylcha1_flk.028 (003.016)</ta>
            <ta e="T162" id="Seg_5176" s="T147">KAI_1965_SylchaPylcha1_flk.029 (003.017)</ta>
            <ta e="T167" id="Seg_5177" s="T162">KAI_1965_SylchaPylcha1_flk.030 (003.018)</ta>
            <ta e="T169" id="Seg_5178" s="T167">KAI_1965_SylchaPylcha1_flk.031 (003.019)</ta>
            <ta e="T175" id="Seg_5179" s="T169">KAI_1965_SylchaPylcha1_flk.032 (003.020)</ta>
            <ta e="T182" id="Seg_5180" s="T175">KAI_1965_SylchaPylcha1_flk.033 (003.021)</ta>
            <ta e="T189" id="Seg_5181" s="T182">KAI_1965_SylchaPylcha1_flk.034 (003.022)</ta>
            <ta e="T192" id="Seg_5182" s="T189">KAI_1965_SylchaPylcha1_flk.035 (003.023)</ta>
            <ta e="T196" id="Seg_5183" s="T192">KAI_1965_SylchaPylcha1_flk.036 (003.024)</ta>
            <ta e="T209" id="Seg_5184" s="T196">KAI_1965_SylchaPylcha1_flk.037 (003.025)</ta>
            <ta e="T218" id="Seg_5185" s="T209">KAI_1965_SylchaPylcha1_flk.038 (003.026)</ta>
            <ta e="T223" id="Seg_5186" s="T218">KAI_1965_SylchaPylcha1_flk.039 (003.027)</ta>
            <ta e="T227" id="Seg_5187" s="T223">KAI_1965_SylchaPylcha1_flk.040 (003.028)</ta>
            <ta e="T234" id="Seg_5188" s="T227">KAI_1965_SylchaPylcha1_flk.041 (003.029)</ta>
            <ta e="T237" id="Seg_5189" s="T234">KAI_1965_SylchaPylcha1_flk.042 (003.030)</ta>
            <ta e="T243" id="Seg_5190" s="T237">KAI_1965_SylchaPylcha1_flk.043 (003.031)</ta>
            <ta e="T255" id="Seg_5191" s="T243">KAI_1965_SylchaPylcha1_flk.044 (003.032)</ta>
            <ta e="T259" id="Seg_5192" s="T255">KAI_1965_SylchaPylcha1_flk.045 (003.033)</ta>
            <ta e="T262" id="Seg_5193" s="T259">KAI_1965_SylchaPylcha1_flk.046 (003.034)</ta>
            <ta e="T266" id="Seg_5194" s="T262">KAI_1965_SylchaPylcha1_flk.047 (003.035)</ta>
            <ta e="T269" id="Seg_5195" s="T266">KAI_1965_SylchaPylcha1_flk.048 (003.036)</ta>
            <ta e="T272" id="Seg_5196" s="T269">KAI_1965_SylchaPylcha1_flk.049 (003.037)</ta>
            <ta e="T277" id="Seg_5197" s="T272">KAI_1965_SylchaPylcha1_flk.050 (003.038)</ta>
            <ta e="T280" id="Seg_5198" s="T277">KAI_1965_SylchaPylcha1_flk.051 (003.039)</ta>
            <ta e="T286" id="Seg_5199" s="T280">KAI_1965_SylchaPylcha1_flk.052 (003.040)</ta>
            <ta e="T293" id="Seg_5200" s="T286">KAI_1965_SylchaPylcha1_flk.053 (003.041)</ta>
            <ta e="T301" id="Seg_5201" s="T293">KAI_1965_SylchaPylcha1_flk.054 (003.042)</ta>
            <ta e="T305" id="Seg_5202" s="T301">KAI_1965_SylchaPylcha1_flk.055 (003.043)</ta>
            <ta e="T308" id="Seg_5203" s="T305">KAI_1965_SylchaPylcha1_flk.056 (003.044)</ta>
            <ta e="T315" id="Seg_5204" s="T308">KAI_1965_SylchaPylcha1_flk.057 (003.045)</ta>
            <ta e="T322" id="Seg_5205" s="T315">KAI_1965_SylchaPylcha1_flk.058 (003.046)</ta>
            <ta e="T324" id="Seg_5206" s="T322">KAI_1965_SylchaPylcha1_flk.059 (003.047)</ta>
            <ta e="T327" id="Seg_5207" s="T324">KAI_1965_SylchaPylcha1_flk.060 (003.048)</ta>
            <ta e="T333" id="Seg_5208" s="T327">KAI_1965_SylchaPylcha1_flk.061 (003.049)</ta>
            <ta e="T337" id="Seg_5209" s="T333">KAI_1965_SylchaPylcha1_flk.062 (003.050)</ta>
            <ta e="T343" id="Seg_5210" s="T337">KAI_1965_SylchaPylcha1_flk.063 (003.051)</ta>
            <ta e="T349" id="Seg_5211" s="T343">KAI_1965_SylchaPylcha1_flk.064 (003.052)</ta>
            <ta e="T351" id="Seg_5212" s="T349">KAI_1965_SylchaPylcha1_flk.065 (003.053)</ta>
            <ta e="T357" id="Seg_5213" s="T351">KAI_1965_SylchaPylcha1_flk.066 (003.054)</ta>
            <ta e="T361" id="Seg_5214" s="T357">KAI_1965_SylchaPylcha1_flk.067 (003.055)</ta>
            <ta e="T364" id="Seg_5215" s="T361">KAI_1965_SylchaPylcha1_flk.068 (003.056)</ta>
            <ta e="T370" id="Seg_5216" s="T364">KAI_1965_SylchaPylcha1_flk.069 (003.057)</ta>
            <ta e="T378" id="Seg_5217" s="T370">KAI_1965_SylchaPylcha1_flk.070 (003.058)</ta>
            <ta e="T381" id="Seg_5218" s="T378">KAI_1965_SylchaPylcha1_flk.071 (003.059)</ta>
            <ta e="T392" id="Seg_5219" s="T381">KAI_1965_SylchaPylcha1_flk.072 (003.060)</ta>
            <ta e="T399" id="Seg_5220" s="T392">KAI_1965_SylchaPylcha1_flk.073 (003.061)</ta>
            <ta e="T407" id="Seg_5221" s="T399">KAI_1965_SylchaPylcha1_flk.074 (003.062)</ta>
            <ta e="T412" id="Seg_5222" s="T407">KAI_1965_SylchaPylcha1_flk.075 (003.063)</ta>
            <ta e="T415" id="Seg_5223" s="T412">KAI_1965_SylchaPylcha1_flk.076 (003.064)</ta>
            <ta e="T422" id="Seg_5224" s="T415">KAI_1965_SylchaPylcha1_flk.077 (003.065)</ta>
            <ta e="T430" id="Seg_5225" s="T422">KAI_1965_SylchaPylcha1_flk.078 (004.001)</ta>
            <ta e="T434" id="Seg_5226" s="T430">KAI_1965_SylchaPylcha1_flk.079 (004.002)</ta>
            <ta e="T436" id="Seg_5227" s="T434">KAI_1965_SylchaPylcha1_flk.080 (004.003)</ta>
            <ta e="T439" id="Seg_5228" s="T436">KAI_1965_SylchaPylcha1_flk.081 (004.004)</ta>
            <ta e="T448" id="Seg_5229" s="T439">KAI_1965_SylchaPylcha1_flk.082 (004.005)</ta>
            <ta e="T452" id="Seg_5230" s="T448">KAI_1965_SylchaPylcha1_flk.083 (004.006)</ta>
            <ta e="T456" id="Seg_5231" s="T452">KAI_1965_SylchaPylcha1_flk.084 (004.007)</ta>
            <ta e="T458" id="Seg_5232" s="T456">KAI_1965_SylchaPylcha1_flk.085 (004.008)</ta>
            <ta e="T461" id="Seg_5233" s="T458">KAI_1965_SylchaPylcha1_flk.086 (004.009)</ta>
            <ta e="T465" id="Seg_5234" s="T461">KAI_1965_SylchaPylcha1_flk.087 (004.010)</ta>
            <ta e="T469" id="Seg_5235" s="T465">KAI_1965_SylchaPylcha1_flk.088 (004.011)</ta>
            <ta e="T474" id="Seg_5236" s="T469">KAI_1965_SylchaPylcha1_flk.089 (004.012)</ta>
            <ta e="T480" id="Seg_5237" s="T474">KAI_1965_SylchaPylcha1_flk.090 (004.013)</ta>
            <ta e="T487" id="Seg_5238" s="T480">KAI_1965_SylchaPylcha1_flk.091 (004.014)</ta>
            <ta e="T490" id="Seg_5239" s="T487">KAI_1965_SylchaPylcha1_flk.092 (004.015)</ta>
            <ta e="T500" id="Seg_5240" s="T490">KAI_1965_SylchaPylcha1_flk.093 (004.016)</ta>
            <ta e="T503" id="Seg_5241" s="T500">KAI_1965_SylchaPylcha1_flk.094 (004.017)</ta>
            <ta e="T513" id="Seg_5242" s="T503">KAI_1965_SylchaPylcha1_flk.095 (004.018)</ta>
            <ta e="T517" id="Seg_5243" s="T513">KAI_1965_SylchaPylcha1_flk.096 (004.019)</ta>
            <ta e="T522" id="Seg_5244" s="T517">KAI_1965_SylchaPylcha1_flk.097 (004.020)</ta>
            <ta e="T531" id="Seg_5245" s="T522">KAI_1965_SylchaPylcha1_flk.098 (004.021)</ta>
            <ta e="T535" id="Seg_5246" s="T531">KAI_1965_SylchaPylcha1_flk.099 (004.022)</ta>
            <ta e="T543" id="Seg_5247" s="T535">KAI_1965_SylchaPylcha1_flk.100 (004.023)</ta>
            <ta e="T547" id="Seg_5248" s="T543">KAI_1965_SylchaPylcha1_flk.101 (004.024)</ta>
            <ta e="T550" id="Seg_5249" s="T547">KAI_1965_SylchaPylcha1_flk.102 (004.025)</ta>
            <ta e="T554" id="Seg_5250" s="T550">KAI_1965_SylchaPylcha1_flk.103 (004.026)</ta>
            <ta e="T557" id="Seg_5251" s="T554">KAI_1965_SylchaPylcha1_flk.104 (004.027)</ta>
            <ta e="T563" id="Seg_5252" s="T557">KAI_1965_SylchaPylcha1_flk.105 (004.028)</ta>
            <ta e="T568" id="Seg_5253" s="T563">KAI_1965_SylchaPylcha1_flk.106 (004.029)</ta>
            <ta e="T573" id="Seg_5254" s="T568">KAI_1965_SylchaPylcha1_flk.107 (004.030)</ta>
            <ta e="T577" id="Seg_5255" s="T573">KAI_1965_SylchaPylcha1_flk.108 (004.031)</ta>
            <ta e="T584" id="Seg_5256" s="T577">KAI_1965_SylchaPylcha1_flk.109 (004.032)</ta>
            <ta e="T588" id="Seg_5257" s="T584">KAI_1965_SylchaPylcha1_flk.110 (004.033)</ta>
            <ta e="T593" id="Seg_5258" s="T588">KAI_1965_SylchaPylcha1_flk.111 (004.034)</ta>
            <ta e="T602" id="Seg_5259" s="T593">KAI_1965_SylchaPylcha1_flk.112 (004.035)</ta>
            <ta e="T603" id="Seg_5260" s="T602">KAI_1965_SylchaPylcha1_flk.113 (004.036)</ta>
            <ta e="T611" id="Seg_5261" s="T603">KAI_1965_SylchaPylcha1_flk.114 (004.037)</ta>
            <ta e="T615" id="Seg_5262" s="T611">KAI_1965_SylchaPylcha1_flk.115 (004.038)</ta>
            <ta e="T624" id="Seg_5263" s="T615">KAI_1965_SylchaPylcha1_flk.116 (004.039)</ta>
            <ta e="T633" id="Seg_5264" s="T624">KAI_1965_SylchaPylcha1_flk.117 (004.040)</ta>
            <ta e="T637" id="Seg_5265" s="T633">KAI_1965_SylchaPylcha1_flk.118 (004.041)</ta>
            <ta e="T642" id="Seg_5266" s="T637">KAI_1965_SylchaPylcha1_flk.119 (004.042)</ta>
            <ta e="T643" id="Seg_5267" s="T642">KAI_1965_SylchaPylcha1_flk.120 (004.043)</ta>
            <ta e="T650" id="Seg_5268" s="T643">KAI_1965_SylchaPylcha1_flk.121 (004.044)</ta>
            <ta e="T654" id="Seg_5269" s="T650">KAI_1965_SylchaPylcha1_flk.122 (004.045)</ta>
            <ta e="T663" id="Seg_5270" s="T654">KAI_1965_SylchaPylcha1_flk.123 (004.046)</ta>
            <ta e="T668" id="Seg_5271" s="T663">KAI_1965_SylchaPylcha1_flk.124 (004.047)</ta>
            <ta e="T673" id="Seg_5272" s="T668">KAI_1965_SylchaPylcha1_flk.125 (004.048)</ta>
            <ta e="T677" id="Seg_5273" s="T673">KAI_1965_SylchaPylcha1_flk.126 (004.049)</ta>
            <ta e="T680" id="Seg_5274" s="T677">KAI_1965_SylchaPylcha1_flk.127 (004.050)</ta>
            <ta e="T682" id="Seg_5275" s="T680">KAI_1965_SylchaPylcha1_flk.128 (005.001)</ta>
            <ta e="T683" id="Seg_5276" s="T682">KAI_1965_SylchaPylcha1_flk.129 (005.002)</ta>
            <ta e="T691" id="Seg_5277" s="T683">KAI_1965_SylchaPylcha1_flk.130 (005.003)</ta>
            <ta e="T694" id="Seg_5278" s="T691">KAI_1965_SylchaPylcha1_flk.131 (005.004)</ta>
            <ta e="T697" id="Seg_5279" s="T694">KAI_1965_SylchaPylcha1_flk.132 (005.005)</ta>
            <ta e="T699" id="Seg_5280" s="T697">KAI_1965_SylchaPylcha1_flk.133 (005.006)</ta>
            <ta e="T703" id="Seg_5281" s="T699">KAI_1965_SylchaPylcha1_flk.134 (005.007)</ta>
            <ta e="T706" id="Seg_5282" s="T703">KAI_1965_SylchaPylcha1_flk.135 (005.008)</ta>
            <ta e="T707" id="Seg_5283" s="T706">KAI_1965_SylchaPylcha1_flk.136 (005.009)</ta>
            <ta e="T710" id="Seg_5284" s="T707">KAI_1965_SylchaPylcha1_flk.137 (005.010)</ta>
            <ta e="T712" id="Seg_5285" s="T710">KAI_1965_SylchaPylcha1_flk.138 (005.011)</ta>
            <ta e="T719" id="Seg_5286" s="T712">KAI_1965_SylchaPylcha1_flk.139 (005.012)</ta>
            <ta e="T723" id="Seg_5287" s="T719">KAI_1965_SylchaPylcha1_flk.140 (005.013)</ta>
            <ta e="T726" id="Seg_5288" s="T723">KAI_1965_SylchaPylcha1_flk.141 (005.014)</ta>
            <ta e="T727" id="Seg_5289" s="T726">KAI_1965_SylchaPylcha1_flk.142 (005.015)</ta>
            <ta e="T732" id="Seg_5290" s="T727">KAI_1965_SylchaPylcha1_flk.143 (005.016)</ta>
            <ta e="T736" id="Seg_5291" s="T732">KAI_1965_SylchaPylcha1_flk.144 (005.017)</ta>
            <ta e="T741" id="Seg_5292" s="T736">KAI_1965_SylchaPylcha1_flk.145 (005.018)</ta>
            <ta e="T746" id="Seg_5293" s="T741">KAI_1965_SylchaPylcha1_flk.146 (005.019)</ta>
            <ta e="T750" id="Seg_5294" s="T746">KAI_1965_SylchaPylcha1_flk.147 (005.020)</ta>
            <ta e="T752" id="Seg_5295" s="T750">KAI_1965_SylchaPylcha1_flk.148 (005.021)</ta>
            <ta e="T757" id="Seg_5296" s="T752">KAI_1965_SylchaPylcha1_flk.149 (005.022)</ta>
            <ta e="T761" id="Seg_5297" s="T757">KAI_1965_SylchaPylcha1_flk.150 (005.023)</ta>
            <ta e="T769" id="Seg_5298" s="T761">KAI_1965_SylchaPylcha1_flk.151 (005.024)</ta>
            <ta e="T774" id="Seg_5299" s="T769">KAI_1965_SylchaPylcha1_flk.152 (005.025)</ta>
            <ta e="T781" id="Seg_5300" s="T774">KAI_1965_SylchaPylcha1_flk.153 (005.026)</ta>
            <ta e="T787" id="Seg_5301" s="T781">KAI_1965_SylchaPylcha1_flk.154 (005.027)</ta>
            <ta e="T792" id="Seg_5302" s="T787">KAI_1965_SylchaPylcha1_flk.155 (005.028)</ta>
            <ta e="T796" id="Seg_5303" s="T792">KAI_1965_SylchaPylcha1_flk.156 (005.029)</ta>
            <ta e="T809" id="Seg_5304" s="T796">KAI_1965_SylchaPylcha1_flk.157 (005.030)</ta>
            <ta e="T814" id="Seg_5305" s="T809">KAI_1965_SylchaPylcha1_flk.158 (005.031)</ta>
            <ta e="T823" id="Seg_5306" s="T814">KAI_1965_SylchaPylcha1_flk.159 (005.032)</ta>
            <ta e="T827" id="Seg_5307" s="T823">KAI_1965_SylchaPylcha1_flk.160 (005.033)</ta>
            <ta e="T836" id="Seg_5308" s="T827">KAI_1965_SylchaPylcha1_flk.161 (005.034)</ta>
            <ta e="T840" id="Seg_5309" s="T836">KAI_1965_SylchaPylcha1_flk.162 (005.035)</ta>
            <ta e="T845" id="Seg_5310" s="T840">KAI_1965_SylchaPylcha1_flk.163 (005.036)</ta>
            <ta e="T846" id="Seg_5311" s="T845">KAI_1965_SylchaPylcha1_flk.164 (005.037)</ta>
            <ta e="T854" id="Seg_5312" s="T846">KAI_1965_SylchaPylcha1_flk.165 (006.001)</ta>
            <ta e="T858" id="Seg_5313" s="T854">KAI_1965_SylchaPylcha1_flk.166 (006.002)</ta>
            <ta e="T862" id="Seg_5314" s="T858">KAI_1965_SylchaPylcha1_flk.167 (006.003)</ta>
            <ta e="T865" id="Seg_5315" s="T862">KAI_1965_SylchaPylcha1_flk.168 (006.004)</ta>
            <ta e="T867" id="Seg_5316" s="T865">KAI_1965_SylchaPylcha1_flk.169 (006.005)</ta>
            <ta e="T869" id="Seg_5317" s="T867">KAI_1965_SylchaPylcha1_flk.170 (006.006)</ta>
            <ta e="T872" id="Seg_5318" s="T869">KAI_1965_SylchaPylcha1_flk.171 (006.007)</ta>
            <ta e="T880" id="Seg_5319" s="T872">KAI_1965_SylchaPylcha1_flk.172 (006.008)</ta>
            <ta e="T886" id="Seg_5320" s="T880">KAI_1965_SylchaPylcha1_flk.173 (007.001)</ta>
            <ta e="T891" id="Seg_5321" s="T886">KAI_1965_SylchaPylcha1_flk.174 (007.002)</ta>
            <ta e="T897" id="Seg_5322" s="T891">KAI_1965_SylchaPylcha1_flk.175 (007.003)</ta>
            <ta e="T900" id="Seg_5323" s="T897">KAI_1965_SylchaPylcha1_flk.176 (007.004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T1" id="Seg_5324" s="T0">сылʼча-пылʼча.</ta>
            <ta e="T4" id="Seg_5325" s="T1">′сылʼча ′пыlча ′kъ̄шʼ ӣlымпа.</ta>
            <ta e="T6" id="Seg_5326" s="T4">′ъ̄мытый ′ӓ̄пынты.</ta>
            <ta e="T8" id="Seg_5327" s="T6">ӓ̄зыты ′чаңымпа.</ta>
            <ta e="T10" id="Seg_5328" s="T8">′ӣlа ӣlа.</ta>
            <ta e="T24" id="Seg_5329" s="T10">′оккырчо̄н′тоты ′ъ̊̄мынты нӣ′kотот [kъ̊тыт]. ъ̊̄мтыты ниlчик ӓ̄са мат kумылʼ перла ′kъ̊ннак.</ta>
            <ta e="T27" id="Seg_5330" s="T24">′тиlджа ′кут[т]ар и′лʼантымыт.</ta>
            <ta e="T37" id="Seg_5331" s="T27">′а̄ма тан ′монтыkай тап′тъ̊̄ттыт ′понтарkыт ′kумып аш тӓннима?</ta>
            <ta e="T45" id="Seg_5332" s="T37">′ъ̊̄мыт ′томныты манʼи kайl kум аш ′тӓнныма.</ta>
            <ta e="T51" id="Seg_5333" s="T45">тап ′тъ̊ттыт ′понтарkыт тӓт′чаkыт kуп ′чӓңка.</ta>
            <ta e="T59" id="Seg_5334" s="T51">иjаты ′ъ̊̄мынтын ниl ′kъ̊̄тытӓ тан ′момпа ′о̨не̨т ′ӣl[лʼ]ашʼик.</ta>
            <ta e="T64" id="Seg_5335" s="T59">ман ′омб̂а ′kумылʼ ′пе̄рила ′kъ̊̄лʼлʼак.</ta>
            <ta e="T71" id="Seg_5336" s="T64">тап′тӓтыт ′понтарkыт ′сылша ′пылша ′kъ̊ш ныны ′kъ̊нна.</ta>
            <ta e="T75" id="Seg_5337" s="T71">с. п. К. ′нашʼак ′kъ̊нтына.</ta>
            <ta e="T78" id="Seg_5338" s="T75">′kъ̊[ӓ]ң[н]ка ай ′таңымка.</ta>
            <ta e="T84" id="Seg_5339" s="T78">′kоlдыпка ′тулʼдымпыка кып ′kӓ ′тулʼдымпыка.</ta>
            <ta e="T89" id="Seg_5340" s="T84">никакой ниkаиl kуп чʼӓңка.</ta>
            <ta e="T94" id="Seg_5341" s="T89">то ′толʼмыkӓ[ы]т ′кʼет ′kъ̊[ы]нка.</ta>
            <ta e="T99" id="Seg_5342" s="T94">′ӱ̄kонду′kо кот ′кʼет ′сы̄рычика ай ′чӱkо.</ta>
            <ta e="T105" id="Seg_5343" s="T99">′уккур чондоkын[т] ′туlунʼа ′ма̄ча[о]нта ′маргыл ′ма̄че.</ta>
            <ta e="T109" id="Seg_5344" s="T105">на ма̄′чомыт ′kы′танны мынта.</ta>
            <ta e="T113" id="Seg_5345" s="T109">на ′kыт ′kаныңмыт ′kӓ̄лʼимпак. </ta>
            <ta e="T122" id="Seg_5346" s="T113">′уккур чондоɣыт kос kай ′ӱ̄ннӓнта, а̄ – бӯх! а̄! – бух!</ta>
            <ta e="T126" id="Seg_5347" s="T122">kӓ̄лʼимпа ′ниlдий чары ′ӱнтынит.</ta>
            <ta e="T132" id="Seg_5348" s="T126">ни kай ′сурым ни kай ′чӓңка.</ta>
            <ta e="T139" id="Seg_5349" s="T132">на ′тӓттып понтар ко′лʼалтыптӓ̄kак ′kуса kай ′орыче.</ta>
            <ta e="T143" id="Seg_5350" s="T139">мат ′kот ′маннымпысан ′ена.</ta>
            <ta e="T147" id="Seg_5351" s="T143">на тӱннент, на тӱннен′та.</ta>
            <ta e="T162" id="Seg_5352" s="T147">′ырык ′митолʼа ′kышин′чо̄kыт ′таkkыт ′ӱнта ай ке′нна ′лаңкычента а̄ бух! а̄ бух!</ta>
            <ta e="T167" id="Seg_5353" s="T162">′ниlди ′чарыты kаl′монты тӓнкыр[н]а.</ta>
            <ta e="T169" id="Seg_5354" s="T167">на ′тӱнтаны̄ыы[ӓӓӓ].</ta>
            <ta e="T175" id="Seg_5355" s="T169">′таkkыт kыт kыɣат kос kай ′сӓ̄гиымыт.</ta>
            <ta e="T182" id="Seg_5356" s="T175">на ′майниl lаңынʼӓ ааа! бух! ааа! бух!</ta>
            <ta e="T189" id="Seg_5357" s="T182">ашʼа ни ′kумыт ′чары ′оlани ′лаңк[г]ынʼӓ.</ta>
            <ta e="T192" id="Seg_5358" s="T189">ны на тӱнта.</ta>
            <ta e="T196" id="Seg_5359" s="T192">′монтъ ниlчик kум ′lаңыс.</ta>
            <ta e="T209" id="Seg_5360" s="T196">′нычап ′тӱнта монтыlа kуп монты kуп там ӓ̄санты ӓ̄манты мӓ̄чи се̄ нʼӓңыча kуп.</ta>
            <ta e="T218" id="Seg_5361" s="T209">тап ′ъ̊̄сынты ′ъ̊̄мынты вӓчи се̨ нʼӓңыча ′омта на ′kӓ̄kын ′оkыт.</ta>
            <ta e="T223" id="Seg_5362" s="T218">на kай на kуп lаңышмынта.</ta>
            <ta e="T227" id="Seg_5363" s="T223">тат ′kаний ′омнат.</ta>
            <ta e="T234" id="Seg_5364" s="T227">′аша ′мумпа мат ′lозашʼим ′амkо ′омтак.</ta>
            <ta e="T237" id="Seg_5365" s="T234">kо̄н′нӓkыт ′kумимы ′е̄ңотыт.</ta>
            <ta e="T243" id="Seg_5366" s="T237">наммыт шʼип ондаl им′потӓт, ′lошʼим ′амkо.</ta>
            <ta e="T255" id="Seg_5367" s="T243">′оlка мат ′kонна ′таннӓнтак ′lос ′мунтык ′шимыт амта, не kаим нашʼ kа̄лӓта.</ta>
            <ta e="T259" id="Seg_5368" s="T255">на kа′шим ′омдылʼимпотет.</ta>
            <ta e="T262" id="Seg_5369" s="T259">тат ′kоне ′тантыш.</ta>
            <ta e="T266" id="Seg_5370" s="T262">тан′ней kона ′танташ.</ta>
            <ta e="T269" id="Seg_5371" s="T266">мат ′кутар ′таннентак.</ta>
            <ta e="T272" id="Seg_5372" s="T269">kумыт шʼип ′kоlчантотыт.</ta>
            <ta e="T277" id="Seg_5373" s="T272">мат томнап, тат ′kонтанташ.</ta>
            <ta e="T280" id="Seg_5374" s="T277">′kонна на ′таннын‵тоkай.</ta>
            <ta e="T286" id="Seg_5375" s="T280">′сыlше ′пылʼшет ′kъ̊ш ′поркʼе на мел′чинтыты ′тӓ̄ɣантыса.</ta>
            <ta e="T293" id="Seg_5376" s="T286">тан ′напа ′машип ′кушанык шип ′kъ̊̄ташʼик.</ta>
            <ta e="T301" id="Seg_5377" s="T293">lос ′ташты[ӓ] на ′со̄ɣыченынта сыlʼча пыlчат kъ̊ш ′куча ′kа̄тысӓ.</ta>
            <ta e="T305" id="Seg_5378" s="T301">′ма̄шип ′кушена шип ′kъ̊̄ташʼик.</ta>
            <ta e="T308" id="Seg_5379" s="T305">ниl ′ӓ̄кылʼчимпата ′тӓ̄kасӓ.</ta>
            <ta e="T315" id="Seg_5380" s="T308">′кона на ′kъ̊нта ны̄ монт ′чулʼмот.</ta>
            <ta e="T322" id="Seg_5381" s="T315">на ′kӓт ′мо̄таноkыт ай ′поркʼенна ′мʼе̄нтыты.</ta>
            <ta e="T324" id="Seg_5382" s="T322">мот шʼерешʼик.</ta>
            <ta e="T327" id="Seg_5383" s="T324">′kаныңанты kа′нтенинт</ta>
            <ta e="T333" id="Seg_5384" s="T327">мат ′kӓнтык ′шʼертак kумимы шʼип kоlчантотыт.</ta>
            <ta e="T337" id="Seg_5385" s="T333">тан оlа мо̄т ′шʼерешек.</ta>
            <ta e="T343" id="Seg_5386" s="T337">на ′мо̄таноkыт ай ′поркʼе ментыт.</ta>
            <ta e="T349" id="Seg_5387" s="T343">тан ′напа ′машип ыкы ′шʼип ′kъ̊тӓшик.</ta>
            <ta e="T351" id="Seg_5388" s="T349">′таɣазӓ ′е̄кылʼчиңыты.</ta>
            <ta e="T357" id="Seg_5389" s="T351">′мотна шʼера тына kуп ′ъ̊̄тымынты.</ta>
            <ta e="T361" id="Seg_5390" s="T357">′мунта на kолʼчинтотытна.</ta>
            <ta e="T364" id="Seg_5391" s="T361">′мумпа kа̄ ′шʼерна.</ta>
            <ta e="T370" id="Seg_5392" s="T364">ашʼа ′мумпа ′kупти мо̄тты шип ′ӱ̄тыса.</ta>
            <ta e="T378" id="Seg_5393" s="T370">kос ′kаjеl kуп ′тӱ̄са, ′машип мо̄т шʼим ′ӱ̄тӓса.</ta>
            <ta e="T381" id="Seg_5394" s="T378">′kонышеп ′таттыралʼдес.</ta>
            <ta e="T392" id="Seg_5395" s="T381">тӓп чап kоңыты ′мо̄тат нʼӓннаl пеlӓkыт ай уккыр ′нʼӓңыча kуп ′kомт[д̂]а.</ta>
            <ta e="T399" id="Seg_5396" s="T392">′ныноkа ′нилʼчик ′е̨са ′kумып ′ыннӓ таkтаlто̄тет.</ta>
            <ta e="T407" id="Seg_5397" s="T399">ныны пона ′тандылʼӓ ӱң[к]ыlты колʼчимбаты lо̄сы ′кутылʼ ′монтоkыт kомпышка.</ta>
            <ta e="T412" id="Seg_5398" s="T407">′lоса ′kата′kомпышнӓ мо̄т ′шʼерныл[l]ыт.</ta>
            <ta e="T415" id="Seg_5399" s="T412">′уккыр kуп ӱңу[ы]lд̂ымпыңыjе.</ta>
            <ta e="T422" id="Seg_5400" s="T415">′момпа ′ӱ̄тыl ′kъ̊̄нтыты тоkычента, на′шат kомпаченты танты ′колʼчим‵па.</ta>
            <ta e="T430" id="Seg_5401" s="T422">′се̨̄пыllак ′е̄нта ′уккыр ′тӓ[ъ̊]тчон‵тоkыт kуп на ′таннынта.</ta>
            <ta e="T434" id="Seg_5402" s="T430">′момпа kа′рет на ′kомпыча.</ta>
            <ta e="T436" id="Seg_5403" s="T434">′мо̄тты ′патkыlна.</ta>
            <ta e="T439" id="Seg_5404" s="T436">′lо̄сы на ′kомпыча.</ta>
            <ta e="T448" id="Seg_5405" s="T439">а ′момпа но̄тъ ′таңкын ′о̄мтыңылыт, ни ′кушат ′ыкы ′тантыңыlит.</ta>
            <ta e="T452" id="Seg_5406" s="T448">сылʼчи пылʼчит ′kъ̊ш ′ӱңуlд̂имбеты kай?</ta>
            <ta e="T456" id="Seg_5407" s="T452">′коннӓ на ′таннын ′тыңа ′таннынта.</ta>
            <ta e="T458" id="Seg_5408" s="T456">′пе̄кап kолʼчиты.</ta>
            <ta e="T461" id="Seg_5409" s="T458">′пе̄кʼе чӯk, чӯk.</ta>
            <ta e="T465" id="Seg_5410" s="T461">kай ′сылша пылʼша kъ̊ш kат′тӱса?</ta>
            <ta e="T469" id="Seg_5411" s="T465">с. п. kъ̊ш ′тындӓ ′kъ̊сса.</ta>
            <ta e="T474" id="Seg_5412" s="T469">конна на тӱ̄нта ′порkӓп ′kоңот.</ta>
            <ta e="T480" id="Seg_5413" s="T474">′порkе ′kъ̊̄тӓты сылʼша пылʼша kъ̊ш ′мо̄тkын ′омта.</ta>
            <ta e="T487" id="Seg_5414" s="T480">′lосы ′kарра kолʼлʼимо′лʼлʼӓ ку′рролʼна (kъ̊нна) ӱтты ′а̄lча.</ta>
            <ta e="T490" id="Seg_5415" s="T487">мыта kурр kӓ̄ш.</ta>
            <ta e="T500" id="Seg_5416" s="T490">по̄на ′паkтылʼӓ ′тинтена с. п. k. ′порkʼемты ′пачаllӓ ш[с]и′лʼалтӓты ′мача kа′ттоlнит.</ta>
            <ta e="T503" id="Seg_5417" s="T500">′jарыk ′порkеп ′ме̄ңеты.</ta>
            <ta e="T513" id="Seg_5418" s="T503">′чарӓ ӓ̄ко[у]лʼчимпыңыты тат на па ′ма̄шʼип никушʼат[ны] ′ыкы kъ̊′ташʼик.</ta>
            <ta e="T517" id="Seg_5419" s="T513">′ти ′танты колʼчимпа ′kарынылʼ пит.</ta>
            <ta e="T522" id="Seg_5420" s="T517">′ныны пинты кунты на омнентотыт.</ta>
            <ta e="T531" id="Seg_5421" s="T522">kарнылʼ пит ′kумып kуралʼ′то̄тыт ′понӓ маннымпотет, ′чолсӓ ′аша танта?</ta>
            <ta e="T535" id="Seg_5422" s="T531">с.п. kъ̊ш ′kуралʼто̄тыт ′ӱңа[ы]лтымб̂ӓты.</ta>
            <ta e="T543" id="Seg_5423" s="T535">′себ̂ылак kуп на лʼӓмык ′еинта ′мо̄ттъ на ′аlчинта.</ta>
            <ta e="T547" id="Seg_5424" s="T543">′мытта ′уккыр ′лосы kомпышпа̄.</ta>
            <ta e="T550" id="Seg_5425" s="T547">лозы на kомпышпа.</ta>
            <ta e="T554" id="Seg_5426" s="T550">′kапиjа шʼилʼша ′палʼша ′ондъ ′ӱңылʼдимпаты.</ta>
            <ta e="T557" id="Seg_5427" s="T554">на ′таннын та kоннӓ.</ta>
            <ta e="T563" id="Seg_5428" s="T557">kурылʼлʼе тап ′тӱ̄кынӓ kурылʼлʼе тап ′тӱ̄кынӓ.</ta>
            <ta e="T568" id="Seg_5429" s="T563">на ′kъ̊т по̄рынты на ′таннынта.</ta>
            <ta e="T573" id="Seg_5430" s="T568">тын′тӓна ′порkенты курылʼлʼе на тӱ̄нта.</ta>
            <ta e="T577" id="Seg_5431" s="T573">′мыта чӯk, чӯk, чӯk.</ta>
            <ta e="T584" id="Seg_5432" s="T577">kай сылʼча пылʼча kъ̊ш ′kат‵тӱjе ′kай ′мотkын ′еңа.</ta>
            <ta e="T588" id="Seg_5433" s="T584">с. п. k. на′шат ′kъ̊сса.</ta>
            <ta e="T593" id="Seg_5434" s="T588">тат ′kонна ′kъ̊llа ап′солʼ ′амты.</ta>
            <ta e="T602" id="Seg_5435" s="T593">то̄на кур[ы]′лʼе на ′kъ̊нта нъ̊ъ̊ъ̊, ′мо̄та′ноkылʼ тӱнта′нъ̊ъ̊ъ̊ ′порk[кʼ]енты ′тӱ̄ңа.</ta>
            <ta e="T603" id="Seg_5436" s="T602">kат тӱ̄са?</ta>
            <ta e="T611" id="Seg_5437" s="T603">с. п. kъ̊ш теп аша ′те̨̄лʼа на′шак ′kъ̊сса [kъ̊ссыңа].</ta>
            <ta e="T615" id="Seg_5438" s="T611">′мо̄т[т]ы ′шʼерлʼа ‵апсо′лʼамты.</ta>
            <ta e="T624" id="Seg_5439" s="T615">уккыр чо′нтоkыт ′lосы мо̄тына ′но̨̄′kолта мотты на но′kkолʼта.</ta>
            <ta e="T633" id="Seg_5440" s="T624">мо̄тты ′чеп ′с[шʼ]ерна lо̄сы с. п. k. ′потчалʼныты ′оlанты′lака.</ta>
            <ta e="T637" id="Seg_5441" s="T633">мо̄ты шʼу ′нʼанна пӱнколʼна.</ta>
            <ta e="T642" id="Seg_5442" s="T637">′kъ̊̄пынты lака ′kоттӓ ′понӓ ′аlча.</ta>
            <ta e="T643" id="Seg_5443" s="T642">kъ̊̄′тентит.</ta>
            <ta e="T650" id="Seg_5444" s="T643">′понӓ ′таттыңыт тӓ с. п. k. ′kъ̊̄пынты lака.</ta>
            <ta e="T654" id="Seg_5445" s="T650">′понӓ ′танта ′монте ′че̄лыңелʼча.</ta>
            <ta e="T663" id="Seg_5446" s="T654">kа′ррə ′тулʼтыңыт с. п. k. ′kъ̊̄пынты ′lа̄кап ′ӱ̄тыl kъ̊′kонтъ ′пиннете.</ta>
            <ta e="T668" id="Seg_5447" s="T663">′уlгозе ′нʼе̄нты kантептиты ′ниlчик ′пиннете.</ta>
            <ta e="T673" id="Seg_5448" s="T668">ӱдылʼ ′kъ̊kынты ′kаныкты ниlчик пинныт.</ta>
            <ta e="T677" id="Seg_5449" s="T673">мӣтола ′ӣllылʼе ′иппа.</ta>
            <ta e="T680" id="Seg_5450" s="T677">′оlынтӓсӓ нʼенты lо′каlтыңыты.</ta>
            <ta e="T682" id="Seg_5451" s="T680">на че̄лʼи че̄латы‵ңомыт.</ta>
            <ta e="T683" id="Seg_5452" s="T682">′lы[ӓ]пkо ′мота.</ta>
            <ta e="T691" id="Seg_5453" s="T683">′момпа ′кеккысе шʼи′т[т]ӓkий ′jарыгмы ′ӣчека ашʼе ‵тӓны′мо̄мыт.</ta>
            <ta e="T694" id="Seg_5454" s="T691">′ӱ̄тыт на lипkы ′монна.</ta>
            <ta e="T697" id="Seg_5455" s="T694">′ӱ̄тыт ′се̄б̂ыlак омта.</ta>
            <ta e="T699" id="Seg_5456" s="T697"> на ′ремkы мотkоламна.</ta>
            <ta e="T703" id="Seg_5457" s="T699">kумып ′понӓ куралʼтытӓ ′ӱңылʼтым‵пӓты.</ta>
            <ta e="T706" id="Seg_5458" s="T703">′kуп ′поkыт ′ныңка.</ta>
            <ta e="T710" id="Seg_5459" s="T707">′kумна ′kомпышкоlам′на.</ta>
            <ta e="T712" id="Seg_5460" s="T710">′мо̄тты ′патkылна.</ta>
            <ta e="T719" id="Seg_5461" s="T712">мо̄ты ′шʼерлʼӓ ниlчик ′kӓтымпат ′lо̄зы kа′рре̨т ′kомпынʼнʼӓ.</ta>
            <ta e="T723" id="Seg_5462" s="T719">′kонна куры′лʼлʼе на ′таннынта.</ta>
            <ta e="T727" id="Seg_5463" s="T726">ааа! </ta>
            <ta e="T732" id="Seg_5464" s="T727">тат kай ′кочкымонанты, а̄мы′релʼчи‵нанты ′нарkымо̄тиjанты?</ta>
            <ta e="T736" id="Seg_5465" s="T732">′уккур ′нʼе̄мты нылʼчик kоңыты.</ta>
            <ta e="T741" id="Seg_5466" s="T736">ман[т] о′кот а̄мы′ре̄лʼе ′нилʼчик ′иппы′колʼчимпыкак.</ta>
            <ta e="T746" id="Seg_5467" s="T741">ныны ′kонна ′kурылʼе на ′тӱ̄нта.</ta>
            <ta e="T750" id="Seg_5468" s="T746">′kонна ′kӱ̄рылʼе на ′тӱ̄нта.</ta>
            <ta e="T752" id="Seg_5469" s="T750">чӯк чӯк.</ta>
            <ta e="T757" id="Seg_5470" s="T752">kай с. п. k. ′kат ′тӱlча [тӱңа].</ta>
            <ta e="T761" id="Seg_5471" s="T757">kай ′kъ̊̄сса kай kат тӱсса.</ta>
            <ta e="T769" id="Seg_5472" s="T761">с. п. k. о′кон на′ша ′kъ̊̄сса ′тинды на′шʼа ′kъ̊сса.</ta>
            <ta e="T774" id="Seg_5473" s="T769">′тат kонна ′kъ̊llа апсоl амты.</ta>
            <ta e="T781" id="Seg_5474" s="T774">′нына ′kонна ′курлʼе ′kъ̊̄llӓ тин′тена[ы] ′порk[г]ънты тӱ̄ңа.</ta>
            <ta e="T787" id="Seg_5475" s="T781">′kӓттыты: kай с. п. k. мо̄тkын ′е̄иңа?</ta>
            <ta e="T792" id="Seg_5476" s="T787">тӓм аша на′шʼат ты′нтӓ ′kъ̊̄сса.</ta>
            <ta e="T796" id="Seg_5477" s="T792">мо̄т ′с[шʼ]ерлʼа ап′соl ′амты.</ta>
            <ta e="T809" id="Seg_5478" s="T796">′о̨̄май нылʼчик ′ӓ̄сса ′мо̄тты kай сʼ[шʼ]ертак, kай ашʼа ′шʼертак ′о̨̄май мы.</ta>
            <ta e="T814" id="Seg_5479" s="T809">порkе kай ′момпа ′машʼип ′курымманта.</ta>
            <ta e="T823" id="Seg_5480" s="T814">′kапи ′кӯлʼтымпа [кулʼтымпыңа] ′о̨мий, kай шʼерта, kай ′ашʼа шʼерта.</ta>
            <ta e="T827" id="Seg_5481" s="T823">оккыр чонтоkыт на ′шʼерколамта.</ta>
            <ta e="T836" id="Seg_5482" s="T827">′оlымты мо̄тты чап но′kолныт с. п. kъ̊ш ′пачалныт ′оланты ′lако.</ta>
            <ta e="T840" id="Seg_5483" s="T836">мо̄тты ′нʼаннӓ ′аlча пӱнколʼна.</ta>
            <ta e="T845" id="Seg_5484" s="T840">kопынты ′lака ′kоттӓ по̄нӓ ′а̄lча.</ta>
            <ta e="T846" id="Seg_5485" s="T845">kъ̊′тӓңыты.</ta>
            <ta e="T854" id="Seg_5486" s="T846">ныны ′но̄ты чап о̄мтотыт, о̄мтотыт, ни kай чеңка.</ta>
            <ta e="T858" id="Seg_5487" s="T854">мумпа kай шʼи′те̨ ӣ′ча.</ta>
            <ta e="T862" id="Seg_5488" s="T858">′топтылʼ kарыт ′и[ы]нна ′че̄лыңна.</ta>
            <ta e="T865" id="Seg_5489" s="T862">′момпа по̄ пачалʼнылыт.</ta>
            <ta e="T867" id="Seg_5490" s="T865">по ′па̄чаl‵нотыт.</ta>
            <ta e="T869" id="Seg_5491" s="T867">′тӱ чо̄тыңылыт.</ta>
            <ta e="T872" id="Seg_5492" s="T869">ъ̊̄тымынты поп пачалʼ′нотыт.</ta>
            <ta e="T880" id="Seg_5493" s="T872">′шʼи′ттӓkып kа′ррӓ туl′тоkтӓт, ′тӱсе ′чото̄тыт ′kоптыколык тӱсӓ чо̄′тотыт.</ta>
            <ta e="T886" id="Seg_5494" s="T880">′иралʼ ′ӓ̄сӓсыт на ′ӣра нӓ̄′лʼӓты ӓ̄ппа.</ta>
            <ta e="T891" id="Seg_5495" s="T886">на ′нӓ̄лʼамты сылʼча пылʼча kъ̊шты мӣңаты.</ta>
            <ta e="T897" id="Seg_5496" s="T891">на ′тъ̊ттыт мо̄ры ′колʼалʼтынтӓkыт ′шʼӧлʼkумытыткине ′тӱңа.</ta>
            <ta e="T900" id="Seg_5497" s="T897">′морыт на ′ента.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T1" id="Seg_5498" s="T0">sɨlʼča-pɨlʼča.</ta>
            <ta e="T4" id="Seg_5499" s="T1">sɨlʼča pɨlʼča qəːš iːlʼɨmpa.</ta>
            <ta e="T6" id="Seg_5500" s="T4">əːmɨtɨj äːpɨntɨ.</ta>
            <ta e="T8" id="Seg_5501" s="T6">äːzɨtɨ čaŋɨmpa.</ta>
            <ta e="T10" id="Seg_5502" s="T8">iːlʼa iːlʼa.</ta>
            <ta e="T24" id="Seg_5503" s="T10">okkɨrčoːntotɨ əːmɨntɨ niːqotot [qətɨt]. əːmtɨtɨ nilʼčik äːsa mat qumɨlʼ perla qənnak.</ta>
            <ta e="T27" id="Seg_5504" s="T24">tilʼdža kut[t]ar ilʼantɨmɨt.</ta>
            <ta e="T37" id="Seg_5505" s="T27">aːma tan montɨqaj taptəːttɨt pontarqɨt qumɨp aš tännima?</ta>
            <ta e="T45" id="Seg_5506" s="T37">əːmɨt tomnɨtɨ manʼi qajlʼ qum aš tännɨma.</ta>
            <ta e="T51" id="Seg_5507" s="T45">tap təttɨt pontarqɨt tätčaqɨt qup čäŋka.</ta>
            <ta e="T59" id="Seg_5508" s="T51">ijatɨ əːmɨntɨn nilʼ qəːtɨtä tan mompa onet iːlʼ[lʼ]ašik.</ta>
            <ta e="T64" id="Seg_5509" s="T59">man omp̂a qumɨlʼ peːrila qəːlʼlʼak.</ta>
            <ta e="T71" id="Seg_5510" s="T64">taptätɨt pontarqɨt sɨlša pɨlša qəš nɨnɨ qənna.</ta>
            <ta e="T75" id="Seg_5511" s="T71">s. p. К. našak qəntɨna.</ta>
            <ta e="T78" id="Seg_5512" s="T75">qə[ä]ŋ[n]ka aj taŋɨmka.</ta>
            <ta e="T84" id="Seg_5513" s="T78">qolʼdɨpka tulʼdɨmpɨka kɨp qä tulʼdɨmpɨka.</ta>
            <ta e="T89" id="Seg_5514" s="T84">nikakoj niqailʼ qup čʼäŋka.</ta>
            <ta e="T94" id="Seg_5515" s="T89">to tolʼmɨqä[ɨ]t kʼet qə[ɨ]nka.</ta>
            <ta e="T99" id="Seg_5516" s="T94">üːqonduqo kot kʼet sɨːrɨčika aj čüqo.</ta>
            <ta e="T105" id="Seg_5517" s="T99">ukkur čondoqɨn[t] tulʼunʼa maːča[o]nta margɨl maːče.</ta>
            <ta e="T109" id="Seg_5518" s="T105">na maːčomɨt qɨtannɨ mɨnta.</ta>
            <ta e="T113" id="Seg_5519" s="T109">na qɨt qanɨŋmɨt qäːlʼimpak.</ta>
            <ta e="T122" id="Seg_5520" s="T113">ukkur čondoqɨt qos qaj üːnnänta, aː – puːh! aː! – puh!</ta>
            <ta e="T126" id="Seg_5521" s="T122">qäːlʼimpa nilʼdij čarɨ üntɨnit.</ta>
            <ta e="T132" id="Seg_5522" s="T126">ni qaj surɨm ni qaj čäŋka.</ta>
            <ta e="T139" id="Seg_5523" s="T132">na tättɨp pontar kolʼaltɨptäːqak qusa qaj orɨče. </ta>
            <ta e="T143" id="Seg_5524" s="T139">mat qot mannɨmpɨsan ena.</ta>
            <ta e="T147" id="Seg_5525" s="T143">na tünnent, na tünnenta.</ta>
            <ta e="T162" id="Seg_5526" s="T147">ɨrɨk mitolʼa qɨšinčoːqɨt taqqɨt ünta aj kenna laŋkɨčenta aː puh! aː puh!</ta>
            <ta e="T167" id="Seg_5527" s="T162">nilʼdi čarɨtɨ qalʼmontɨ tänkɨr[n]a.</ta>
            <ta e="T169" id="Seg_5528" s="T167">na tüntanɨːɨɨ[äää].</ta>
            <ta e="T175" id="Seg_5529" s="T169">taqqɨt qɨt qɨqat qos qaj säːgiɨmɨt.</ta>
            <ta e="T182" id="Seg_5530" s="T175">na majnilʼ lʼaŋɨnʼä aaa! puh! aaa! puh!</ta>
            <ta e="T189" id="Seg_5531" s="T182">aša ni qumɨt čarɨ olʼani laŋk[g]ɨnʼä.</ta>
            <ta e="T192" id="Seg_5532" s="T189">nɨ na tünta.</ta>
            <ta e="T196" id="Seg_5533" s="T192">montə nilʼčik qum lʼaŋɨs.</ta>
            <ta e="T209" id="Seg_5534" s="T196">nɨčap tünta montɨlʼa qup montɨ qup tam äːsantɨ äːmantɨ mäːči seː nʼäŋɨča qup.</ta>
            <ta e="T218" id="Seg_5535" s="T209">tap əːsɨntɨ əːmɨntɨ väči se nʼäŋɨča omta na qäːqɨn oqɨt.</ta>
            <ta e="T223" id="Seg_5536" s="T218">na qaj na qup lʼaŋɨšmɨnta.</ta>
            <ta e="T227" id="Seg_5537" s="T223">tat qanij omnat.</ta>
            <ta e="T234" id="Seg_5538" s="T227">aša mumpa mat lʼozašim amqo omtak.</ta>
            <ta e="T237" id="Seg_5539" s="T234">qoːnnäqɨt qumimɨ eːŋotɨt.</ta>
            <ta e="T243" id="Seg_5540" s="T237">nammɨt šip ondalʼ impotät, lʼošim amqo.</ta>
            <ta e="T255" id="Seg_5541" s="T243">olʼka mat qonna tannäntak lʼos muntɨk šimɨt amta, ne qaim naš qaːläta.</ta>
            <ta e="T259" id="Seg_5542" s="T255">na qašim omdɨlʼimpotet.</ta>
            <ta e="T262" id="Seg_5543" s="T259">tat qone tantɨš.</ta>
            <ta e="T266" id="Seg_5544" s="T262">tannej qona tantaš.</ta>
            <ta e="T269" id="Seg_5545" s="T266">mat kutar tannentak.</ta>
            <ta e="T272" id="Seg_5546" s="T269">qumɨt šip qolʼčantotɨt.</ta>
            <ta e="T277" id="Seg_5547" s="T272">mat tomnap, tat qontantaš.</ta>
            <ta e="T280" id="Seg_5548" s="T277">qonna na tannɨntoqaj.</ta>
            <ta e="T286" id="Seg_5549" s="T280">sɨlʼše pɨlʼšet qəš porkʼe na melčintɨtɨ täːqantɨsa.</ta>
            <ta e="T293" id="Seg_5550" s="T286">tan napa mašip kušanɨk šip qəːtašik.</ta>
            <ta e="T301" id="Seg_5551" s="T293">lʼos taštɨ[ä] na soːqɨčenɨnta sɨlʼʼča pɨlʼčat qəš kuča qaːtɨsä.</ta>
            <ta e="T305" id="Seg_5552" s="T301">maːšip kušena šip qəːtašik.</ta>
            <ta e="T308" id="Seg_5553" s="T305">nilʼ äːkɨlʼčimpata täːqasä.</ta>
            <ta e="T315" id="Seg_5554" s="T308">kona na qənta nɨː mont čulʼmot.</ta>
            <ta e="T322" id="Seg_5555" s="T315">na qät moːtanoqɨt aj porkʼenna mʼeːntɨtɨ.</ta>
            <ta e="T324" id="Seg_5556" s="T322">mot šerešik.</ta>
            <ta e="T327" id="Seg_5557" s="T324">qanɨŋantɨ qantenint.</ta>
            <ta e="T333" id="Seg_5558" s="T327">mat qäntɨk šertak qumimɨ šip qolʼčantotɨt.</ta>
            <ta e="T337" id="Seg_5559" s="T333">tan olʼa moːt šerešek.</ta>
            <ta e="T343" id="Seg_5560" s="T337">na moːtanoqɨt aj porkʼe mentɨt.</ta>
            <ta e="T349" id="Seg_5561" s="T343">tan napa mašip ɨkɨ šip qətäšik.</ta>
            <ta e="T351" id="Seg_5562" s="T349">taqazä eːkɨlʼčiŋɨtɨ.</ta>
            <ta e="T357" id="Seg_5563" s="T351">motna šera tɨna qup əːtɨmɨntɨ.</ta>
            <ta e="T361" id="Seg_5564" s="T357">munta na qolʼčintotɨtna.</ta>
            <ta e="T364" id="Seg_5565" s="T361">mumpa qaː šerna.</ta>
            <ta e="T370" id="Seg_5566" s="T364">aša mumpa qupti moːttɨ šip üːtɨsa.</ta>
            <ta e="T378" id="Seg_5567" s="T370">qos qajelʼ qup tüːsa, mašip moːt šim üːtäsa.</ta>
            <ta e="T381" id="Seg_5568" s="T378">qonɨšep tattɨralʼdi.</ta>
            <ta e="T392" id="Seg_5569" s="T381">täp čap qoŋɨtɨ moːtat nʼännalʼ pelʼäqɨt aj ukkɨr nʼäŋɨča qup qomt[d̂]a.</ta>
            <ta e="T399" id="Seg_5570" s="T392">nɨnoqa nilʼčik esa qumɨp ɨnnä taqtalʼtoːtet.</ta>
            <ta e="T407" id="Seg_5571" s="T399">nɨnɨ pona tandɨlʼä üŋ[k]ɨlʼtɨ kolʼčimpatɨ lʼoːsɨ kutɨlʼ montoqɨt qompɨška.</ta>
            <ta e="T412" id="Seg_5572" s="T407">lʼosa qataqompɨšnä moːt šernɨl[lʼ]ɨt.</ta>
            <ta e="T415" id="Seg_5573" s="T412">ukkɨr qup üŋu[ɨ]lʼd̂ɨmpɨŋɨje.</ta>
            <ta e="T422" id="Seg_5574" s="T415">mompa üːtɨlʼ qəːntɨtɨ toqɨčenta, našat qompačentɨ tantɨ kolʼčimpa.</ta>
            <ta e="T430" id="Seg_5575" s="T422">seːpɨlʼlʼak eːnta ukkɨr tä[ə]tčontoqɨt qup na tannɨnta.</ta>
            <ta e="T434" id="Seg_5576" s="T430">mompa qaret na qompɨča.</ta>
            <ta e="T436" id="Seg_5577" s="T434"> moːttɨ patqɨlʼna.</ta>
            <ta e="T439" id="Seg_5578" s="T436">lʼoːsɨ na qompɨča.</ta>
            <ta e="T448" id="Seg_5579" s="T439">a mompa noːtə taŋkɨn oːmtɨŋɨlɨt, ni kušat ɨkɨ tantɨŋɨlʼit.</ta>
            <ta e="T452" id="Seg_5580" s="T448">sɨlʼči pɨlʼčit qəš üŋulʼd̂impetɨ qaj?</ta>
            <ta e="T456" id="Seg_5581" s="T452">konnä na tannɨn tɨŋa tannɨnta.</ta>
            <ta e="T458" id="Seg_5582" s="T456">peːkap qolʼčitɨ.</ta>
            <ta e="T461" id="Seg_5583" s="T458">peːkʼe čuːq, čuːq.</ta>
            <ta e="T465" id="Seg_5584" s="T461">qaj sɨlša pɨlʼša qəš qattüsa?</ta>
            <ta e="T469" id="Seg_5585" s="T465">s. p. qəš tɨndä qəssa.</ta>
            <ta e="T474" id="Seg_5586" s="T469">konna na tüːnta porqäp qoŋot.</ta>
            <ta e="T480" id="Seg_5587" s="T474">porqe qəːtätɨ sɨlʼša pɨlʼša qəš moːtqɨn omta.</ta>
            <ta e="T487" id="Seg_5588" s="T480">lʼosɨ qarra qolʼlʼimolʼlʼä kurrolʼna (qənna) üttɨ aːlʼča.</ta>
            <ta e="T490" id="Seg_5589" s="T487">mɨta qurr qäːš.</ta>
            <ta e="T500" id="Seg_5590" s="T490">poːna paqtɨlʼä tintena s. p. q. porqʼemtɨ pačalʼlʼä š[s]ilʼaltätɨ mača qattolʼnit.</ta>
            <ta e="T503" id="Seg_5591" s="T500">jarɨq porqep meːŋetɨ.</ta>
            <ta e="T513" id="Seg_5592" s="T503">čarä äːko[u]lʼčimpɨŋɨtɨ tat na pa maːšip nikušat[nɨ] ɨkɨ qətašik.</ta>
            <ta e="T517" id="Seg_5593" s="T513">ti tantɨ kolʼčimpa qarɨnɨlʼ pit.</ta>
            <ta e="T522" id="Seg_5594" s="T517">nɨnɨ pintɨ kuntɨ na omnentotɨt.</ta>
            <ta e="T531" id="Seg_5595" s="T522">qarnɨlʼ pit qumɨp quralʼtoːtɨt ponä mannɨmpotet, čolsä aša tanta?</ta>
            <ta e="T535" id="Seg_5596" s="T531">s.p. qəš quralʼtoːtɨt üŋa[ɨ]ltɨmp̂ätɨ.</ta>
            <ta e="T543" id="Seg_5597" s="T535">sep̂ɨlak qup na lʼämɨk einta moːttə na alʼčinta.</ta>
            <ta e="T547" id="Seg_5598" s="T543">mɨtta ukkɨr losɨ qompɨšpa.</ta>
            <ta e="T550" id="Seg_5599" s="T547">lozɨ na qompɨšpa.</ta>
            <ta e="T554" id="Seg_5600" s="T550">qapija šilʼša palʼša ondə üŋɨlʼdimpatɨ.</ta>
            <ta e="T557" id="Seg_5601" s="T554">na tannɨn ta qonnä.</ta>
            <ta e="T563" id="Seg_5602" s="T557">qurɨlʼlʼe tap tüːkɨnä qurɨlʼlʼe tap tüːkɨnä.</ta>
            <ta e="T568" id="Seg_5603" s="T563">na qət poːrɨntɨ na tannɨnta.</ta>
            <ta e="T573" id="Seg_5604" s="T568">tɨntäna porqentɨ kurɨlʼlʼe na tüːnta.</ta>
            <ta e="T577" id="Seg_5605" s="T573">mɨta čuːq, čuːq, čuːq.</ta>
            <ta e="T584" id="Seg_5606" s="T577">qaj sɨlʼča pɨlʼča qəš qattüje qaj motqɨn eŋa.</ta>
            <ta e="T588" id="Seg_5607" s="T584">s. p. q. našat qəssa.</ta>
            <ta e="T593" id="Seg_5608" s="T588">tat qonna qəlʼlʼa apsolʼ amtɨ.</ta>
            <ta e="T602" id="Seg_5609" s="T593">toːna kur[ɨ]lʼe na qənta nəəə, moːtanoqɨlʼ tüntanəəə porq[kʼ]entɨ tüːŋa.</ta>
            <ta e="T603" id="Seg_5610" s="T602">qat tüːsa?</ta>
            <ta e="T611" id="Seg_5611" s="T603">s. p. qəš tep aša teːlʼa našak qəssa [qəssɨŋa].</ta>
            <ta e="T615" id="Seg_5612" s="T611">moːt[t]ɨ šerlʼa apsolʼamtɨ.</ta>
            <ta e="T624" id="Seg_5613" s="T615">ukkɨr čontoqɨt lʼosɨ moːtɨna noːqolta mottɨ na noqqolʼta.</ta>
            <ta e="T633" id="Seg_5614" s="T624">moːttɨ čep s[š]erna lʼoːsɨ s. p. q. potčalʼnɨtɨ olʼantɨlʼaka.</ta>
            <ta e="T637" id="Seg_5615" s="T633">moːtɨ šu nʼanna pünkolʼna.</ta>
            <ta e="T642" id="Seg_5616" s="T637">qəːpɨntɨ lʼaka qottä ponä alʼča.</ta>
            <ta e="T643" id="Seg_5617" s="T642">qəːtentit.</ta>
            <ta e="T650" id="Seg_5618" s="T643">ponä tattɨŋɨt tä s. p. q. qəːpɨntɨ lʼaka.</ta>
            <ta e="T654" id="Seg_5619" s="T650">ponä tanta monte čeːlɨŋelʼča.</ta>
            <ta e="T663" id="Seg_5620" s="T654">qarrə tulʼtɨŋɨt s. p. q. qəːpɨntɨ lʼaːkap üːtɨlʼ qəqontə pinnete.</ta>
            <ta e="T668" id="Seg_5621" s="T663">ulʼgoze nʼeːntɨ qanteptitɨ nilʼčik pinnete.</ta>
            <ta e="T673" id="Seg_5622" s="T668">üdɨlʼ qəqɨntɨ qanɨktɨ nilʼčik pinnɨt.</ta>
            <ta e="T677" id="Seg_5623" s="T673">miːtola iːlʼlʼɨlʼe ippa.</ta>
            <ta e="T680" id="Seg_5624" s="T677">olʼɨntäsä nʼentɨ lʼokalʼtɨŋɨtɨ.</ta>
            <ta e="T682" id="Seg_5625" s="T680">na čeːlʼi čeːlatɨŋomɨt.</ta>
            <ta e="T683" id="Seg_5626" s="T682">lʼɨ[ä]pqo mota.</ta>
            <ta e="T691" id="Seg_5627" s="T683">mompa kekkɨse šit[t]äqij jarɨgmɨ iːčeka aše tänɨmoːmɨt.</ta>
            <ta e="T694" id="Seg_5628" s="T691">üːtɨt na lʼipqɨ monna.</ta>
            <ta e="T697" id="Seg_5629" s="T694">üːtɨt seːp̂ɨlʼak omta.</ta>
            <ta e="T699" id="Seg_5630" s="T697">na remqɨ motqolamna.</ta>
            <ta e="T703" id="Seg_5631" s="T699">qumɨp ponä kuralʼtɨtä üŋɨlʼtɨmpätɨ.</ta>
            <ta e="T706" id="Seg_5632" s="T703">qup poqɨt nɨŋka.</ta>
            <ta e="T707" id="Seg_5633" s="T706">qəː! </ta>
            <ta e="T710" id="Seg_5634" s="T707">qumna qompɨškolʼamna.</ta>
            <ta e="T712" id="Seg_5635" s="T710">moːttɨ patqɨlna.</ta>
            <ta e="T719" id="Seg_5636" s="T712">moːtɨ šerlʼä nilʼčik qätɨmpat lʼoːzɨ qarret qompɨnʼnʼä.</ta>
            <ta e="T723" id="Seg_5637" s="T719">qonna kurɨlʼlʼe na tannɨnta.</ta>
            <ta e="T727" id="Seg_5638" s="T726">aaa! </ta>
            <ta e="T732" id="Seg_5639" s="T727">tat qaj kočkɨmonantɨ, aːmɨrelʼčinantɨ narqɨmoːtijantɨ?</ta>
            <ta e="T736" id="Seg_5640" s="T732">ukkur nʼeːmtɨ nɨlʼčik qoŋɨtɨ.</ta>
            <ta e="T741" id="Seg_5641" s="T736">man[t] okot aːmɨreːlʼe nilʼčik ippɨkolʼčimpɨkak.</ta>
            <ta e="T746" id="Seg_5642" s="T741">nɨnɨ qonna qurɨlʼe na tüːnta.</ta>
            <ta e="T750" id="Seg_5643" s="T746">qonna qüːrɨlʼe na tüːnta.</ta>
            <ta e="T752" id="Seg_5644" s="T750">čuːk čuːk.</ta>
            <ta e="T757" id="Seg_5645" s="T752">qaj s. p. q. qat tülʼča [tüŋa].</ta>
            <ta e="T761" id="Seg_5646" s="T757">qaj qəːssa qaj qat tüssa.</ta>
            <ta e="T769" id="Seg_5647" s="T761">s. p. q. okon naša qəːssa tindɨ naša qəssa.</ta>
            <ta e="T774" id="Seg_5648" s="T769">tat qonna qəlʼlʼa apsolʼ amtɨ.</ta>
            <ta e="T781" id="Seg_5649" s="T774">nɨna qonna kurlʼe qəːlʼlʼä tintena[ɨ] porq[g]əntɨ tüːŋa.</ta>
            <ta e="T787" id="Seg_5650" s="T781">qättɨtɨ: qaj s. p. q. moːtqɨn eːiŋa?</ta>
            <ta e="T792" id="Seg_5651" s="T787">täm aša našat tɨntä qəːssa.</ta>
            <ta e="T796" id="Seg_5652" s="T792">moːt s[š]erlʼa apsolʼ amtɨ.</ta>
            <ta e="T809" id="Seg_5653" s="T796">oːmaj nɨlʼčik äːssa moːttɨ qaj sʼ[š]ertak, qaj aša šertak oːmaj mɨ.</ta>
            <ta e="T814" id="Seg_5654" s="T809">porqe qaj mompa mašip kurɨmmanta.</ta>
            <ta e="T823" id="Seg_5655" s="T814">qapi kuːlʼtɨmpa [kulʼtɨmpɨŋa] omij, qaj šerta, qaj aša šerta.</ta>
            <ta e="T827" id="Seg_5656" s="T823">okkɨr čontoqɨt na šerkolamta.</ta>
            <ta e="T836" id="Seg_5657" s="T827">olʼɨmtɨ moːttɨ čap noqolnɨt s. p. qəš pačalnɨt olantɨ lʼako.</ta>
            <ta e="T840" id="Seg_5658" s="T836">moːttɨ nʼannä alʼča pünkolʼna.</ta>
            <ta e="T845" id="Seg_5659" s="T840">qopɨntɨ lʼaka qottä poːnä aːlʼča.</ta>
            <ta e="T846" id="Seg_5660" s="T845">qətäŋɨtɨ.</ta>
            <ta e="T854" id="Seg_5661" s="T846">nɨnɨ noːtɨ čap oːmtotɨt, oːmtotɨt, ni qaj čeŋka.</ta>
            <ta e="T858" id="Seg_5662" s="T854">mumpa qaj šite iːča.</ta>
            <ta e="T862" id="Seg_5663" s="T858">toptɨlʼ qarɨt i[ɨ]nna čeːlɨŋna.</ta>
            <ta e="T865" id="Seg_5664" s="T862">mompa poː pačalʼnɨlɨt.</ta>
            <ta e="T867" id="Seg_5665" s="T865">po paːčalʼnotɨt.</ta>
            <ta e="T869" id="Seg_5666" s="T867">tü čoːtɨŋɨlɨt.</ta>
            <ta e="T872" id="Seg_5667" s="T869">əːtɨmɨntɨ pop pačalʼnotɨt.</ta>
            <ta e="T880" id="Seg_5668" s="T872">šittäqɨp qarrä tulʼtoqtät, tüse čotoːtɨt qoptɨkolɨk tüsä čoːtotɨt.</ta>
            <ta e="T886" id="Seg_5669" s="T880">iralʼ äːsäsɨt na iːra näːlʼätɨ äːppa.</ta>
            <ta e="T891" id="Seg_5670" s="T886">na näːlʼamtɨ sɨlʼča pɨlʼča qəštɨ miːŋatɨ.</ta>
            <ta e="T897" id="Seg_5671" s="T891">na təttɨt moːrɨ kolʼalʼtɨntäqɨt šölʼqumɨtɨtkine tüŋa.</ta>
            <ta e="T900" id="Seg_5672" s="T897">morɨt na enta.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1" id="Seg_5673" s="T0">Sɨlʼčʼa-Pɨlʼčʼa. </ta>
            <ta e="T4" id="Seg_5674" s="T1">Sɨlʼčʼa-Pɨlʼčʼa Qəš ilɨmpa. </ta>
            <ta e="T6" id="Seg_5675" s="T4">Əmɨtɨj ɛːppɨntɨ. </ta>
            <ta e="T8" id="Seg_5676" s="T6">Äsɨtɨ čʼaŋkɨmpa. </ta>
            <ta e="T10" id="Seg_5677" s="T8">Ila, ila. </ta>
            <ta e="T24" id="Seg_5678" s="T10">Okkɨr čʼontot əmɨntɨ nı kotot (/kətɨt), əmtɨ tɨ nılʼčʼik ɛsa: “Mat qumɨlʼ peːrla qənnak. </ta>
            <ta e="T27" id="Seg_5679" s="T24">Tılʼčʼa kuttar ilantɨmɨt? </ta>
            <ta e="T37" id="Seg_5680" s="T27">Ama, tan montɨ qaj tap təttɨt pontarqɨt qumɨp aš tɛnima?</ta>
            <ta e="T45" id="Seg_5681" s="T37">Əmɨt tomnɨtɨ: Man nʼi qajlʼ qum aš tɛnɨma. </ta>
            <ta e="T51" id="Seg_5682" s="T45">Tap təttɨt pontarqɨt tätčʼaqɨt qup čʼäŋka. </ta>
            <ta e="T59" id="Seg_5683" s="T51">Ijatɨ əmɨntɨn nılʼ kətɨtä: Tan mompa onät ilašik. </ta>
            <ta e="T64" id="Seg_5684" s="T59">Man ompa qumɨlʼ peːrila qəlʼlʼak. </ta>
            <ta e="T71" id="Seg_5685" s="T64">Tap tätɨt pontarqɨt Sɨlša-Pɨlša Qəš nɨːnɨ qənna. </ta>
            <ta e="T75" id="Seg_5686" s="T71">Sɨlša-Pɨlša Qəš našak qəntɨna. </ta>
            <ta e="T78" id="Seg_5687" s="T75">Kəŋka aj taŋɨmka. </ta>
            <ta e="T84" id="Seg_5688" s="T78">Qoltɨp ka tulʼtɨmpɨka, kɨp kä tulʼtɨmpɨka. </ta>
            <ta e="T89" id="Seg_5689" s="T84">Nʼikakoj ni qailʼ qup čʼäŋka. </ta>
            <ta e="T94" id="Seg_5690" s="T89">To toːlʼ mɨqɨt, ket qənka. </ta>
            <ta e="T99" id="Seg_5691" s="T94">Ükontuqo qɔːtqet sɨrɨčʼčʼika aj čʼüːqo. </ta>
            <ta e="T105" id="Seg_5692" s="T99">Ukkur čʼontoːqɨn tulunʼnʼa mačʼonta, marqɨl mačʼe. </ta>
            <ta e="T109" id="Seg_5693" s="T105">Na mačʼoːmɨt kɨ tannɨmmɨnta. </ta>
            <ta e="T113" id="Seg_5694" s="T109">Na kɨt qanɨŋmɨt qälʼimpak. </ta>
            <ta e="T122" id="Seg_5695" s="T113">Ukkur čʼontoːqɨt qos qaj ünnänta: A buh, a buh! </ta>
            <ta e="T126" id="Seg_5696" s="T122">Qälʼimpa, nılʼtij čʼarɨ üntɨnit. </ta>
            <ta e="T132" id="Seg_5697" s="T126">Nʼi qaj suːrɨm, nʼi qaj čʼäŋka. </ta>
            <ta e="T139" id="Seg_5698" s="T132">Na tättɨp pontar kolʼaltɨptäːqak kusa qaj orɨčʼčʼe? </ta>
            <ta e="T143" id="Seg_5699" s="T139">Mat hot mannɨmpɨsan ɛna. </ta>
            <ta e="T147" id="Seg_5700" s="T143">Na tünnent, na tünnenta. </ta>
            <ta e="T162" id="Seg_5701" s="T147">Ɨːrɨk mit olʼa kɨ šinčʼoːqɨt takkɨt ünta aj kɛn na laŋkɨčʼčʼenta: A buh, a buh! </ta>
            <ta e="T167" id="Seg_5702" s="T162">Nılʼti čʼarɨtɨ qalʼ montɨ tɛːŋŋɨrna. </ta>
            <ta e="T169" id="Seg_5703" s="T167">Na tüntanɨɨɨ. </ta>
            <ta e="T175" id="Seg_5704" s="T169">Takkɨt kɨt kɨːqat qos qaj säːqiɨmɨt. </ta>
            <ta e="T182" id="Seg_5705" s="T175">Na majnilʼ laŋkɨnʼä: Aaa buh, aaa buh! </ta>
            <ta e="T189" id="Seg_5706" s="T182">Aša ni qumɨt čʼarɨ ola nı laŋkɨnʼnʼä. </ta>
            <ta e="T192" id="Seg_5707" s="T189">Nɨː na tünta. </ta>
            <ta e="T196" id="Seg_5708" s="T192">Montə nılʼčʼik qum laŋkɨs. </ta>
            <ta e="T209" id="Seg_5709" s="T196">Nɨː čʼap tünta montɨla qup, montɨ qup tam äsantɨ ämantɨ mäčʼisä nʼäŋɨčʼa qup. </ta>
            <ta e="T218" id="Seg_5710" s="T209">Tap əsɨntɨ əmɨntɨ wäčʼisä nʼäŋɨčʼa ɔːmta na qäqɨn ɔːqqɨt. </ta>
            <ta e="T223" id="Seg_5711" s="T218">Na qaj, na qup laŋkɨšmɨnta? </ta>
            <ta e="T227" id="Seg_5712" s="T223">Tat qaː nıj ɔːmnant? </ta>
            <ta e="T234" id="Seg_5713" s="T227">–Aša, mumpa mat loːsa šım amqo ɔːmtak. </ta>
            <ta e="T237" id="Seg_5714" s="T234">Konnäqɨt qumiːmɨ ɛːŋɔːtɨt. </ta>
            <ta e="T243" id="Seg_5715" s="T237">Nammɨt šıp ontalimpɔːtät loːš šim amqo. </ta>
            <ta e="T255" id="Seg_5716" s="T243">Olqa mat konna tannɛntak, loːs muntɨk šımɨt amta, ne qaim naš qaläta. </ta>
            <ta e="T259" id="Seg_5717" s="T255">Na qaː šım omtɨlʼimpɔːtet? </ta>
            <ta e="T262" id="Seg_5718" s="T259">–Tat konnä tantɨš. </ta>
            <ta e="T266" id="Seg_5719" s="T262">–Tan nej konna tantaš. </ta>
            <ta e="T269" id="Seg_5720" s="T266">Mat kutar tannɛntak? </ta>
            <ta e="T272" id="Seg_5721" s="T269">Qumɨt šıp qolʼčʼantɔːtɨt. </ta>
            <ta e="T277" id="Seg_5722" s="T272">–Mat tomnap, tat kon tantaš. </ta>
            <ta e="T280" id="Seg_5723" s="T277">Konna na tannɨntɔːqaj. </ta>
            <ta e="T286" id="Seg_5724" s="T280">Sɨlʼše-Pɨlʼšet Qəš pɔːrkä na meːlʼčʼintɨtɨ täːqantɨsa. </ta>
            <ta e="T293" id="Seg_5725" s="T286">Tan na pa mašıp kušan ɨk šıp kətašik! </ta>
            <ta e="T301" id="Seg_5726" s="T293">Loːs taštɨ na soqɨčʼɛnnɨnta, Sɨlʼčʼa-Pɨlʼčʼat Qəš kučʼa qatɨsä. </ta>
            <ta e="T305" id="Seg_5727" s="T301">Mašıp kušenna šıp kətašik. </ta>
            <ta e="T308" id="Seg_5728" s="T305">Nılʼ äːkɨlʼčʼimpata täːqasä. </ta>
            <ta e="T315" id="Seg_5729" s="T308">Konna na qənta, nɨː mont čʼulʼ mɔːt. </ta>
            <ta e="T322" id="Seg_5730" s="T315">Naqät mɔːtan ɔːqqɨt aj pɔːrkä na meːntɨtɨ. </ta>
            <ta e="T324" id="Seg_5731" s="T322">Mɔːt šeːräšik. </ta>
            <ta e="T327" id="Seg_5732" s="T324">Qaː nɨŋantɨ, qantenint? </ta>
            <ta e="T333" id="Seg_5733" s="T327">–Mat qäntɨk šeːrtak, qumiːmɨ šıp qolʼčʼantɔːtɨt. </ta>
            <ta e="T337" id="Seg_5734" s="T333">Tan ola mɔːt šeːräšek. </ta>
            <ta e="T343" id="Seg_5735" s="T337">Na mɔːtan ɔːqqɨt aj pɔːrkä meːntɨt. </ta>
            <ta e="T349" id="Seg_5736" s="T343">Tan na pa mašıp ɨkɨ šıp kətäšik! </ta>
            <ta e="T351" id="Seg_5737" s="T349">Taːqasä äːkɨlʼčʼiŋɨtɨ. </ta>
            <ta e="T357" id="Seg_5738" s="T351">Mɔːt na šeːra tɨna qup əːtɨmɨntɨ. </ta>
            <ta e="T361" id="Seg_5739" s="T357">Munta na qolʼčʼintɔːtɨt na. </ta>
            <ta e="T364" id="Seg_5740" s="T361">Mumpa qaː šeːrna? </ta>
            <ta e="T370" id="Seg_5741" s="T364">–Aša, mumpa qupti mɔːttɨ šıp üːtɨsa. </ta>
            <ta e="T378" id="Seg_5742" s="T370">Qos qajelʼ qup tüsa, mašıp mɔːt šım üːtäsa. </ta>
            <ta e="T381" id="Seg_5743" s="T378">Konnɨ šep tattɨraltes. </ta>
            <ta e="T392" id="Seg_5744" s="T381">Täp čʼap qoŋɨtɨ mɔːtat nʼennalʼ pɛläqqɨt aj ukkɨr nʼäŋɨčʼa qup qomta. </ta>
            <ta e="T399" id="Seg_5745" s="T392">Nɨːno qa nılʼčʼik ɛsa: Qumɨp ınnä taqtaltɔːtet. </ta>
            <ta e="T407" id="Seg_5746" s="T399">Nɨːnɨ pona tantɨlʼä üŋkɨltɨkkolʼčʼimpatɨ loːsɨ kutɨlʼ mɔːntoːqɨt qompɨška. </ta>
            <ta e="T412" id="Seg_5747" s="T407">Loːsa qata qompɨšnä mɔːt šeːrnɨlɨt. </ta>
            <ta e="T415" id="Seg_5748" s="T412">Ukkɨr qup üŋkɨltɨmpɨŋɨjä. </ta>
            <ta e="T422" id="Seg_5749" s="T415">Mompa üːtɨlʼ kəntɨtɨ tokkɨčʼčʼenta, našat tantɨkkolʼčʼimpa (/qompačʼčʼentɨ). </ta>
            <ta e="T430" id="Seg_5750" s="T422">Seːpɨlak ɛːnta, ukkɨr tät čʼontoːqɨt qup na tannɨnta. </ta>
            <ta e="T434" id="Seg_5751" s="T430">Mompa qaret na qompɨčʼčʼa. </ta>
            <ta e="T436" id="Seg_5752" s="T434">Mɔːttɨ patqɨlna. </ta>
            <ta e="T439" id="Seg_5753" s="T436">Loːsɨ na qompɨčʼčʼa! </ta>
            <ta e="T448" id="Seg_5754" s="T439">A mompa nɔːtə taŋɨŋ ɔːmtɨŋɨlɨt, nʼi kušat ɨkɨ tantɨŋɨlit. </ta>
            <ta e="T452" id="Seg_5755" s="T448">Sɨlʼčʼi-Pɨlʼčʼit Qəš üŋkultimpetɨ, qaj? </ta>
            <ta e="T456" id="Seg_5756" s="T452">Konnä na tannɨntɨŋa (/tannɨnta). </ta>
            <ta e="T458" id="Seg_5757" s="T456">Peːkap qolʼčʼitɨ. </ta>
            <ta e="T461" id="Seg_5758" s="T458">Peːkä: čʼuk, čʼuk. </ta>
            <ta e="T465" id="Seg_5759" s="T461">Qaj Sɨlša-Pɨlʼša Qəš qattüsa? </ta>
            <ta e="T469" id="Seg_5760" s="T465">–Sɨlša-Pɨlʼša Qəš tɨntä qəssa. </ta>
            <ta e="T474" id="Seg_5761" s="T469">Konna na tünta, pɔːrkäp qoŋot. </ta>
            <ta e="T480" id="Seg_5762" s="T474">Pɔːrkä kətätɨ: Sɨlʼša-Pɨlʼša Qəš mɔːtqɨn ɔːmta. </ta>
            <ta e="T487" id="Seg_5763" s="T480">Loːsɨ karra kolʼimɔːlʼlʼä kurolʼna (/qənna), üttɨ alʼčʼa. </ta>
            <ta e="T490" id="Seg_5764" s="T487">Mɨta qur qäːš. </ta>
            <ta e="T500" id="Seg_5765" s="T490">Pona paktɨlʼä tıntena Sɨlʼčʼa-Pɨlʼčʼa Qəš pɔːrkämtɨ pačʼallä sılʼlʼaltätɨ, mačʼa qattɔːlnit. </ta>
            <ta e="T503" id="Seg_5766" s="T500">Jarɨk pɔːrkäp meːŋetɨ. </ta>
            <ta e="T513" id="Seg_5767" s="T503">Čʼarrä äkulʼčʼimpɨŋɨtɨ: Tat na pa mašıp nʼi kušat (/kušannɨ) ɨkɨ kətašik! </ta>
            <ta e="T517" id="Seg_5768" s="T513">Tiː tantɨkkolʼčʼimpa qarɨnɨlʼ pit. </ta>
            <ta e="T522" id="Seg_5769" s="T517">Nɨːnɨ pintɨ kuntɨ na ɔːmnentɔːtɨt. </ta>
            <ta e="T531" id="Seg_5770" s="T522">Qarnɨlʼ pit qumɨp kuralʼtɔːtɨt ponä, mannɨmpɔːtät, čʼɔːlsä aša tanta. </ta>
            <ta e="T535" id="Seg_5771" s="T531">Sɨlʼčʼa-Pɨlʼčʼa Qəš kuralʼtɔːtɨt: Üŋkɨltɨmpätɨ. </ta>
            <ta e="T543" id="Seg_5772" s="T535">Seːpɨlak qup na lʼämɨk ɛinta, mɔːttə na alʼčʼinta. </ta>
            <ta e="T547" id="Seg_5773" s="T543">Mɨtta ukkɨr loːsɨ qompɨšpa. </ta>
            <ta e="T550" id="Seg_5774" s="T547">Loːsɨ na qompɨšpa. </ta>
            <ta e="T554" id="Seg_5775" s="T550">Qapija Šilʼša-Palʼša ontə üŋkɨltimpatɨ. </ta>
            <ta e="T557" id="Seg_5776" s="T554">Na tannɨnta konnä. </ta>
            <ta e="T563" id="Seg_5777" s="T557">Kurɨlʼä tap tükkɨnä, kurɨlʼä tap tükkɨnä. </ta>
            <ta e="T568" id="Seg_5778" s="T563">Na qət pɔːrɨntɨ na tannɨnta. </ta>
            <ta e="T573" id="Seg_5779" s="T568">Tɨntäna pɔːrkäntɨ kurɨlʼä na tünta. </ta>
            <ta e="T577" id="Seg_5780" s="T573">Mɨta čʼuk, čʼuk, čʼuk. </ta>
            <ta e="T584" id="Seg_5781" s="T577">Qaj Sɨlʼčʼa-Pɨlʼčʼa Qəš qattüje, qaj mɔːtqɨn ɛːŋa? </ta>
            <ta e="T588" id="Seg_5782" s="T584">–Sɨlʼčʼa-Pɨlʼčʼa Qəš našat qəssa. </ta>
            <ta e="T593" id="Seg_5783" s="T588">Tat konna qəlla apsolʼ amtɨ. </ta>
            <ta e="T602" id="Seg_5784" s="T593">Toːnna kurɨlʼä na qəntanəəə, mɔːtan ɔːkɨlʼ tüntanəəə, pɔːrkäntɨ tüːŋa. </ta>
            <ta e="T603" id="Seg_5785" s="T602">Qattüsa? </ta>
            <ta e="T611" id="Seg_5786" s="T603">–Sɨlʼčʼa-Pɨlʼčʼa Qəš tep aša täːlʼa našak qəssa (/qəssɨŋa). </ta>
            <ta e="T615" id="Seg_5787" s="T611">Mɔːttɨ šeːrlʼa apsolʼ amtɨ. </ta>
            <ta e="T624" id="Seg_5788" s="T615">Ukkɨr čʼontoːqɨt loːsɨ mɔːttɨ na noqɔːlta, mɔːttɨ na noqqɔːlʼta. </ta>
            <ta e="T633" id="Seg_5789" s="T624">Mɔːttɨ čʼep šeːrna loːsɨ, Sɨlʼčʼa-Pɨlʼčʼa Qəš počʼčʼalʼnɨtɨ olantɨ laka. </ta>
            <ta e="T637" id="Seg_5790" s="T633">Mɔːttɨ šuː nʼanna püŋkolʼna. </ta>
            <ta e="T642" id="Seg_5791" s="T637">Kəpɨntɨ laka qottä ponä alʼčʼa. </ta>
            <ta e="T643" id="Seg_5792" s="T642">Qəttentit. </ta>
            <ta e="T650" id="Seg_5793" s="T643">Ponä tattɨŋɨt tä Sɨlʼčʼa-Pɨlʼčʼa Qəš kəpɨntɨ laka. </ta>
            <ta e="T654" id="Seg_5794" s="T650">Ponä tanta, monte čʼeːlɨŋɛlʼčʼa. </ta>
            <ta e="T663" id="Seg_5795" s="T654">Karrə tulʼtɨŋɨt Sɨlʼčʼa-Pɨlʼčʼa Qəš kəpɨntɨ lakap, ütɨlʼ qəqontə pinnete. </ta>
            <ta e="T668" id="Seg_5796" s="T663">Ulqosä nʼentɨ qantäptitɨ, nılʼčʼik pinnete. </ta>
            <ta e="T673" id="Seg_5797" s="T668">Ütɨlʼ qəqɨntɨ qanɨktɨ nılʼčʼik pinnɨt. </ta>
            <ta e="T677" id="Seg_5798" s="T673">Mit ola ilɨlʼä ippa. </ta>
            <ta e="T680" id="Seg_5799" s="T677">Olɨntäsä nʼentɨ loːqaltɨŋɨtɨ. </ta>
            <ta e="T682" id="Seg_5800" s="T680">Na čʼeːlatɨŋɔːmɨt. </ta>
            <ta e="T683" id="Seg_5801" s="T682">Lɨpkomɔːta. </ta>
            <ta e="T691" id="Seg_5802" s="T683">Mompa kekkɨsä šittäqıj, jarɨk mɨ ičʼeqa, aše tɛnɨmɔːmɨt. </ta>
            <ta e="T694" id="Seg_5803" s="T691">Üːtɨt na lʼipkɨmɔːnna. </ta>
            <ta e="T697" id="Seg_5804" s="T694">Üːtɨt seːpɨlak ɔːmta. </ta>
            <ta e="T699" id="Seg_5805" s="T697">Na rɛmkɨmɔːtqolamna. </ta>
            <ta e="T703" id="Seg_5806" s="T699">Qumɨp ponä kuralʼtɨtä: Üŋkɨlʼtɨmpätɨ. </ta>
            <ta e="T706" id="Seg_5807" s="T703">Qup poːqɨt nɨŋa. </ta>
            <ta e="T707" id="Seg_5808" s="T706">Qəː! </ta>
            <ta e="T710" id="Seg_5809" s="T707">Qum na qompɨšqolamna! </ta>
            <ta e="T712" id="Seg_5810" s="T710">Mɔːttɨ patqɨlna. </ta>
            <ta e="T719" id="Seg_5811" s="T712">Mɔːttɨ šeːrlʼä nılʼčʼik kätɨmpat: Loːsɨ karrät qompɨnʼnʼä. </ta>
            <ta e="T723" id="Seg_5812" s="T719">Konna kurɨlʼlʼä na tannɨnta. </ta>
            <ta e="T726" id="Seg_5813" s="T723">–Konnä qaj qompɨčʼčʼe? </ta>
            <ta e="T727" id="Seg_5814" s="T726">Aaa! </ta>
            <ta e="T732" id="Seg_5815" s="T727">Tat qaj kočʼkɨmɔːnantɨ, amɨrɛlʼčʼinantɨ, narkɨmɔːtijantɨ? </ta>
            <ta e="T736" id="Seg_5816" s="T732">Ukkur nʼeːmtɨ nɨlʼčʼik qoŋɨtɨ. </ta>
            <ta e="T741" id="Seg_5817" s="T736">Man okoːt amɨrrɛːlʼä nılʼčʼik ippɨkkolʼčʼimpɨkkak. </ta>
            <ta e="T746" id="Seg_5818" s="T741">Nɨːnɨ konna kurɨlʼä na tünta. </ta>
            <ta e="T750" id="Seg_5819" s="T746">Konna kürɨlʼä na tünta. </ta>
            <ta e="T752" id="Seg_5820" s="T750">Čʼuk, čʼuk. </ta>
            <ta e="T757" id="Seg_5821" s="T752">Qaj Sɨlʼčʼa-Pɨlʼčʼa Qəš qattülʼčʼa (/tüŋa)? </ta>
            <ta e="T761" id="Seg_5822" s="T757">Qaj qəssa, qaj qattüssa? </ta>
            <ta e="T769" id="Seg_5823" s="T761">–Sɨlʼčʼa-Pɨlʼčʼa Qəš okoːn naša qəssa, tıntɨ naša qəssa. </ta>
            <ta e="T774" id="Seg_5824" s="T769">Tat konna qəlla apsolʼ amtɨ. </ta>
            <ta e="T781" id="Seg_5825" s="T774">Nɨːna konna qəllä (/kurlʼä) tıntena pɔːrkəntɨ tüːŋa. </ta>
            <ta e="T787" id="Seg_5826" s="T781">Kättɨtɨ: Qaj Sɨlʼčʼa-Pɨlʼčʼa Qəš mɔːtqɨn ɛːiŋa? </ta>
            <ta e="T792" id="Seg_5827" s="T787">–Täm aša našat tɨntä qəssa. </ta>
            <ta e="T796" id="Seg_5828" s="T792">Mɔːt šeːrlʼa apsolʼ amtɨ. </ta>
            <ta e="T809" id="Seg_5829" s="T796">Ɔːm aj nɨlʼčʼik ɛssa: Mɔːttɨ qaj šeːrtak, qaj aša šeːrtak,– ɔːm aj mɨ. </ta>
            <ta e="T814" id="Seg_5830" s="T809">Pɔːrkä qaj mompa mašıp kurɨmmanta? </ta>
            <ta e="T823" id="Seg_5831" s="T814">Qapı kulʼtɨmpa (/kulʼtɨmpɨŋa): Ɔːmij qaj šeːrta, qaj aša šeːrta. </ta>
            <ta e="T827" id="Seg_5832" s="T823">Okkɨr čʼontoːqɨt na šeːrqolamta. </ta>
            <ta e="T836" id="Seg_5833" s="T827">Olɨmtɨ mɔːttɨ čʼap noqqɔːlnɨt, Sɨlʼčʼa-Pɨlʼčʼa Qəš pačʼalnɨt olantɨ lako. </ta>
            <ta e="T840" id="Seg_5834" s="T836">Mɔːttɨ nʼannä alʼčʼa, püŋkolʼna. </ta>
            <ta e="T845" id="Seg_5835" s="T840">Kopɨntɨ laka qottä ponä alʼčʼa. </ta>
            <ta e="T846" id="Seg_5836" s="T845">Qəttɛːŋɨtɨ. </ta>
            <ta e="T854" id="Seg_5837" s="T846">Nɨːnɨ nɔːtɨ čʼap ɔːmtɔːtɨt, ɔːmtɔːtɨt, nʼi qaj čʼäŋka. </ta>
            <ta e="T858" id="Seg_5838" s="T854">Mumpa qaj šite ičʼa. </ta>
            <ta e="T862" id="Seg_5839" s="T858">Tɔːptɨlʼ qarɨt ınna čʼeːlɨŋna. </ta>
            <ta e="T865" id="Seg_5840" s="T862">Mompa poː pačʼčʼalʼnɨlɨt. </ta>
            <ta e="T867" id="Seg_5841" s="T865">Poː pačʼčʼalnɔːtɨt. </ta>
            <ta e="T869" id="Seg_5842" s="T867">Tü čʼɔːtɨŋɨlɨt. </ta>
            <ta e="T872" id="Seg_5843" s="T869">Əːtɨmɨntɨ poːp pačʼčʼalʼnɔːtɨt. </ta>
            <ta e="T880" id="Seg_5844" s="T872">Šittäqıp karrä tultoktät, tüsä čʼɔːtɔːtɨt, koptɨkɔːlɨk tüsä čʼɔːtɔːtɨt. </ta>
            <ta e="T886" id="Seg_5845" s="T880">Iralʼ äsäsɨt, na ira nälʼätɨ ɛːppa. </ta>
            <ta e="T891" id="Seg_5846" s="T886">Na nälʼamtɨ Sɨlʼčʼa-Pɨlʼčʼa Qəštɨ miŋatɨ. </ta>
            <ta e="T897" id="Seg_5847" s="T891">Na təttɨt moːrɨ kolʼalʼtɨntäkkɨt, šölʼqumɨtɨtkine tüŋa. </ta>
            <ta e="T900" id="Seg_5848" s="T897">Moːrɨt na ɛːnta. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_5849" s="T0">sɨlʼčʼapɨlʼčʼa</ta>
            <ta e="T2" id="Seg_5850" s="T1">sɨlʼčʼapɨlʼčʼa</ta>
            <ta e="T3" id="Seg_5851" s="T2">qəš</ta>
            <ta e="T4" id="Seg_5852" s="T3">ilɨ-mpa</ta>
            <ta e="T5" id="Seg_5853" s="T4">əmɨ-tɨ-j</ta>
            <ta e="T6" id="Seg_5854" s="T5">ɛː-ppɨ-ntɨ</ta>
            <ta e="T7" id="Seg_5855" s="T6">äsɨ-tɨ</ta>
            <ta e="T8" id="Seg_5856" s="T7">čʼaŋkɨ-mpa</ta>
            <ta e="T9" id="Seg_5857" s="T8">ila</ta>
            <ta e="T10" id="Seg_5858" s="T9">ila</ta>
            <ta e="T11" id="Seg_5859" s="T10">okkɨr</ta>
            <ta e="T12" id="Seg_5860" s="T11">čʼonto-t</ta>
            <ta e="T13" id="Seg_5861" s="T12">əmɨ-ntɨ</ta>
            <ta e="T14" id="Seg_5862" s="T13">nı</ta>
            <ta e="T15" id="Seg_5863" s="T14">koto-t</ta>
            <ta e="T16" id="Seg_5864" s="T15">kətɨ-t</ta>
            <ta e="T17" id="Seg_5865" s="T16">əm-tɨ</ta>
            <ta e="T18" id="Seg_5866" s="T17">tɨ</ta>
            <ta e="T19" id="Seg_5867" s="T18">nılʼčʼi-k</ta>
            <ta e="T20" id="Seg_5868" s="T19">ɛsa</ta>
            <ta e="T21" id="Seg_5869" s="T20">Mat</ta>
            <ta e="T22" id="Seg_5870" s="T21">qum-ɨ-lʼ</ta>
            <ta e="T23" id="Seg_5871" s="T22">peː-r-la</ta>
            <ta e="T24" id="Seg_5872" s="T23">qən-na-k</ta>
            <ta e="T25" id="Seg_5873" s="T24">tılʼčʼa</ta>
            <ta e="T26" id="Seg_5874" s="T25">kuttar</ta>
            <ta e="T27" id="Seg_5875" s="T26">il-antɨ-mɨt</ta>
            <ta e="T28" id="Seg_5876" s="T27">ama</ta>
            <ta e="T29" id="Seg_5877" s="T28">tat</ta>
            <ta e="T30" id="Seg_5878" s="T29">montɨ</ta>
            <ta e="T31" id="Seg_5879" s="T30">qaj</ta>
            <ta e="T32" id="Seg_5880" s="T31">tap</ta>
            <ta e="T33" id="Seg_5881" s="T32">təttɨ-t</ta>
            <ta e="T34" id="Seg_5882" s="T33">pontar-qɨt</ta>
            <ta e="T35" id="Seg_5883" s="T34">qum-ɨ-p</ta>
            <ta e="T36" id="Seg_5884" s="T35">aš</ta>
            <ta e="T37" id="Seg_5885" s="T36">tɛnima</ta>
            <ta e="T38" id="Seg_5886" s="T37">əmɨ-t</ta>
            <ta e="T39" id="Seg_5887" s="T38">tom-nɨ-tɨ</ta>
            <ta e="T40" id="Seg_5888" s="T39">Man</ta>
            <ta e="T41" id="Seg_5889" s="T40">nʼi</ta>
            <ta e="T42" id="Seg_5890" s="T41">qaj-lʼ</ta>
            <ta e="T43" id="Seg_5891" s="T42">qum</ta>
            <ta e="T44" id="Seg_5892" s="T43">aš</ta>
            <ta e="T45" id="Seg_5893" s="T44">tɛnɨma</ta>
            <ta e="T46" id="Seg_5894" s="T45">tap</ta>
            <ta e="T47" id="Seg_5895" s="T46">təttɨ-t</ta>
            <ta e="T48" id="Seg_5896" s="T47">pontar-qɨt</ta>
            <ta e="T49" id="Seg_5897" s="T48">tätčʼa-qɨt</ta>
            <ta e="T50" id="Seg_5898" s="T49">qup</ta>
            <ta e="T51" id="Seg_5899" s="T50">čʼäŋka</ta>
            <ta e="T52" id="Seg_5900" s="T51">ija-tɨ</ta>
            <ta e="T53" id="Seg_5901" s="T52">əmɨ-ntɨ-n</ta>
            <ta e="T54" id="Seg_5902" s="T53">nı-lʼ</ta>
            <ta e="T55" id="Seg_5903" s="T54">kətɨ-tä</ta>
            <ta e="T56" id="Seg_5904" s="T55">Tan</ta>
            <ta e="T57" id="Seg_5905" s="T56">mompa</ta>
            <ta e="T58" id="Seg_5906" s="T57">onät</ta>
            <ta e="T59" id="Seg_5907" s="T58">il-ašik</ta>
            <ta e="T60" id="Seg_5908" s="T59">man</ta>
            <ta e="T61" id="Seg_5909" s="T60">ompa</ta>
            <ta e="T62" id="Seg_5910" s="T61">qum-ɨ-lʼ</ta>
            <ta e="T63" id="Seg_5911" s="T62">peː-r-i-la</ta>
            <ta e="T64" id="Seg_5912" s="T63">qəlʼ-lʼa-k</ta>
            <ta e="T65" id="Seg_5913" s="T64">tap</ta>
            <ta e="T66" id="Seg_5914" s="T65">tätɨ-t</ta>
            <ta e="T67" id="Seg_5915" s="T66">pontar-qɨt</ta>
            <ta e="T68" id="Seg_5916" s="T67">sɨlšapɨlša</ta>
            <ta e="T69" id="Seg_5917" s="T68">qəš</ta>
            <ta e="T70" id="Seg_5918" s="T69">nɨːnɨ</ta>
            <ta e="T71" id="Seg_5919" s="T70">qən-na</ta>
            <ta e="T72" id="Seg_5920" s="T71">sɨlšapɨlša</ta>
            <ta e="T73" id="Seg_5921" s="T72">qəš</ta>
            <ta e="T74" id="Seg_5922" s="T73">našak</ta>
            <ta e="T75" id="Seg_5923" s="T74">qən-tɨ-na</ta>
            <ta e="T76" id="Seg_5924" s="T75">kə-ŋ-ka</ta>
            <ta e="T77" id="Seg_5925" s="T76">aj</ta>
            <ta e="T78" id="Seg_5926" s="T77">taŋɨ-m-ka</ta>
            <ta e="T79" id="Seg_5927" s="T78">qoltɨ-p</ta>
            <ta e="T80" id="Seg_5928" s="T79">ka</ta>
            <ta e="T81" id="Seg_5929" s="T80">tulʼ-tɨ-mpɨ-ka</ta>
            <ta e="T82" id="Seg_5930" s="T81">kɨp</ta>
            <ta e="T83" id="Seg_5931" s="T82">kä</ta>
            <ta e="T84" id="Seg_5932" s="T83">tulʼ-tɨ-mpɨ-ka</ta>
            <ta e="T85" id="Seg_5933" s="T84">nʼikakoj</ta>
            <ta e="T86" id="Seg_5934" s="T85">ni</ta>
            <ta e="T87" id="Seg_5935" s="T86">qai-lʼ</ta>
            <ta e="T88" id="Seg_5936" s="T87">qup</ta>
            <ta e="T89" id="Seg_5937" s="T88">čʼäŋka</ta>
            <ta e="T90" id="Seg_5938" s="T89">to</ta>
            <ta e="T91" id="Seg_5939" s="T90">toː-lʼ</ta>
            <ta e="T92" id="Seg_5940" s="T91">mɨ-qɨt</ta>
            <ta e="T93" id="Seg_5941" s="T92">ke-t</ta>
            <ta e="T94" id="Seg_5942" s="T93">qən-ka</ta>
            <ta e="T95" id="Seg_5943" s="T94">üko-ntuqo</ta>
            <ta e="T96" id="Seg_5944" s="T95">qɔːt-qet</ta>
            <ta e="T97" id="Seg_5945" s="T96">sɨrɨ-čʼ-čʼi-ka</ta>
            <ta e="T98" id="Seg_5946" s="T97">aj</ta>
            <ta e="T99" id="Seg_5947" s="T98">čʼüː-qo</ta>
            <ta e="T100" id="Seg_5948" s="T99">ukkur</ta>
            <ta e="T101" id="Seg_5949" s="T100">čʼontoː-qɨn</ta>
            <ta e="T102" id="Seg_5950" s="T101">tulu-nʼ-nʼa</ta>
            <ta e="T103" id="Seg_5951" s="T102">mačʼo-nta</ta>
            <ta e="T104" id="Seg_5952" s="T103">marqɨ-l</ta>
            <ta e="T105" id="Seg_5953" s="T104">mačʼe</ta>
            <ta e="T106" id="Seg_5954" s="T105">na</ta>
            <ta e="T107" id="Seg_5955" s="T106">mačʼoː-mɨt</ta>
            <ta e="T108" id="Seg_5956" s="T107">kɨ</ta>
            <ta e="T109" id="Seg_5957" s="T108">tannɨ-mmɨ-nta</ta>
            <ta e="T110" id="Seg_5958" s="T109">na</ta>
            <ta e="T111" id="Seg_5959" s="T110">kɨ-t</ta>
            <ta e="T112" id="Seg_5960" s="T111">qanɨŋ-mɨt</ta>
            <ta e="T113" id="Seg_5961" s="T112">qälʼi-mpa-k</ta>
            <ta e="T114" id="Seg_5962" s="T113">ukkur</ta>
            <ta e="T115" id="Seg_5963" s="T114">čʼontoː-qɨt</ta>
            <ta e="T116" id="Seg_5964" s="T115">qos</ta>
            <ta e="T117" id="Seg_5965" s="T116">qaj</ta>
            <ta e="T118" id="Seg_5966" s="T117">ünnä-nta</ta>
            <ta e="T119" id="Seg_5967" s="T118">A</ta>
            <ta e="T120" id="Seg_5968" s="T119">buh</ta>
            <ta e="T121" id="Seg_5969" s="T120">a</ta>
            <ta e="T122" id="Seg_5970" s="T121">buh</ta>
            <ta e="T123" id="Seg_5971" s="T122">qälʼi-mpa</ta>
            <ta e="T124" id="Seg_5972" s="T123">nılʼti-j</ta>
            <ta e="T125" id="Seg_5973" s="T124">čʼarɨ</ta>
            <ta e="T126" id="Seg_5974" s="T125">üntɨ-ni-t</ta>
            <ta e="T127" id="Seg_5975" s="T126">nʼi</ta>
            <ta e="T128" id="Seg_5976" s="T127">qaj</ta>
            <ta e="T129" id="Seg_5977" s="T128">suːrɨm</ta>
            <ta e="T130" id="Seg_5978" s="T129">nʼi</ta>
            <ta e="T131" id="Seg_5979" s="T130">qaj</ta>
            <ta e="T132" id="Seg_5980" s="T131">čʼäŋka</ta>
            <ta e="T133" id="Seg_5981" s="T132">na</ta>
            <ta e="T134" id="Seg_5982" s="T133">tättɨ-p</ta>
            <ta e="T135" id="Seg_5983" s="T134">pontar</ta>
            <ta e="T136" id="Seg_5984" s="T135">kolʼ-altɨ-ptäː-qak</ta>
            <ta e="T137" id="Seg_5985" s="T136">kusa</ta>
            <ta e="T138" id="Seg_5986" s="T137">qaj</ta>
            <ta e="T139" id="Seg_5987" s="T138">orɨ-čʼ-čʼe</ta>
            <ta e="T140" id="Seg_5988" s="T139">mat</ta>
            <ta e="T141" id="Seg_5989" s="T140">hot</ta>
            <ta e="T142" id="Seg_5990" s="T141">mannɨ-mpɨ-sa-n</ta>
            <ta e="T143" id="Seg_5991" s="T142">ɛna</ta>
            <ta e="T144" id="Seg_5992" s="T143">na</ta>
            <ta e="T145" id="Seg_5993" s="T144">tü-nne-nt</ta>
            <ta e="T146" id="Seg_5994" s="T145">na</ta>
            <ta e="T147" id="Seg_5995" s="T146">tü-nne-nta</ta>
            <ta e="T148" id="Seg_5996" s="T147">ɨːrɨŋ</ta>
            <ta e="T149" id="Seg_5997" s="T148">mit</ta>
            <ta e="T150" id="Seg_5998" s="T149">olʼa</ta>
            <ta e="T151" id="Seg_5999" s="T150">kɨ</ta>
            <ta e="T152" id="Seg_6000" s="T151">šinčʼoː-qɨt</ta>
            <ta e="T153" id="Seg_6001" s="T152">takkɨ-t</ta>
            <ta e="T154" id="Seg_6002" s="T153">ünta</ta>
            <ta e="T155" id="Seg_6003" s="T154">aj</ta>
            <ta e="T156" id="Seg_6004" s="T155">kɛn</ta>
            <ta e="T157" id="Seg_6005" s="T156">na</ta>
            <ta e="T158" id="Seg_6006" s="T157">laŋkɨ-čʼ-čʼe-nta</ta>
            <ta e="T159" id="Seg_6007" s="T158">A</ta>
            <ta e="T160" id="Seg_6008" s="T159">buh</ta>
            <ta e="T161" id="Seg_6009" s="T160">a</ta>
            <ta e="T162" id="Seg_6010" s="T161">buh</ta>
            <ta e="T163" id="Seg_6011" s="T162">nılʼti</ta>
            <ta e="T164" id="Seg_6012" s="T163">čʼarɨ-tɨ</ta>
            <ta e="T165" id="Seg_6013" s="T164">qa-lʼ</ta>
            <ta e="T166" id="Seg_6014" s="T165">montɨ</ta>
            <ta e="T167" id="Seg_6015" s="T166">tɛːŋŋɨ-r-na</ta>
            <ta e="T168" id="Seg_6016" s="T167">na</ta>
            <ta e="T169" id="Seg_6017" s="T168">tü-nta-nɨɨɨ</ta>
            <ta e="T170" id="Seg_6018" s="T169">takkɨ-t</ta>
            <ta e="T171" id="Seg_6019" s="T170">kɨ-t</ta>
            <ta e="T172" id="Seg_6020" s="T171">kɨː-qat</ta>
            <ta e="T173" id="Seg_6021" s="T172">qos</ta>
            <ta e="T174" id="Seg_6022" s="T173">qaj</ta>
            <ta e="T175" id="Seg_6023" s="T174">säːqi-ɨ-mɨt</ta>
            <ta e="T176" id="Seg_6024" s="T175">na</ta>
            <ta e="T177" id="Seg_6025" s="T176">majnilʼ</ta>
            <ta e="T178" id="Seg_6026" s="T177">laŋkɨ-nʼä</ta>
            <ta e="T179" id="Seg_6027" s="T178">aaa</ta>
            <ta e="T180" id="Seg_6028" s="T179">buh</ta>
            <ta e="T181" id="Seg_6029" s="T180">aaa</ta>
            <ta e="T182" id="Seg_6030" s="T181">buh</ta>
            <ta e="T183" id="Seg_6031" s="T182">aša</ta>
            <ta e="T184" id="Seg_6032" s="T183">ni</ta>
            <ta e="T185" id="Seg_6033" s="T184">qum-ɨ-t</ta>
            <ta e="T186" id="Seg_6034" s="T185">čʼarɨ</ta>
            <ta e="T187" id="Seg_6035" s="T186">ola</ta>
            <ta e="T188" id="Seg_6036" s="T187">nı</ta>
            <ta e="T189" id="Seg_6037" s="T188">laŋkɨ-nʼ-nʼä</ta>
            <ta e="T190" id="Seg_6038" s="T189">nɨː</ta>
            <ta e="T191" id="Seg_6039" s="T190">na</ta>
            <ta e="T192" id="Seg_6040" s="T191">tü-nta</ta>
            <ta e="T193" id="Seg_6041" s="T192">montə</ta>
            <ta e="T194" id="Seg_6042" s="T193">nılʼčʼi-k</ta>
            <ta e="T195" id="Seg_6043" s="T194">qum</ta>
            <ta e="T196" id="Seg_6044" s="T195">laŋkɨ-s</ta>
            <ta e="T197" id="Seg_6045" s="T196">nɨː</ta>
            <ta e="T198" id="Seg_6046" s="T197">čʼap</ta>
            <ta e="T199" id="Seg_6047" s="T198">tü-nta</ta>
            <ta e="T200" id="Seg_6048" s="T199">montɨ-la</ta>
            <ta e="T201" id="Seg_6049" s="T200">qup</ta>
            <ta e="T202" id="Seg_6050" s="T201">montɨ</ta>
            <ta e="T203" id="Seg_6051" s="T202">qup</ta>
            <ta e="T204" id="Seg_6052" s="T203">tam</ta>
            <ta e="T205" id="Seg_6053" s="T204">äsa-n-tɨ</ta>
            <ta e="T206" id="Seg_6054" s="T205">äma-n-tɨ</ta>
            <ta e="T207" id="Seg_6055" s="T206">mäčʼi-sä</ta>
            <ta e="T208" id="Seg_6056" s="T207">nʼäŋɨčʼa</ta>
            <ta e="T209" id="Seg_6057" s="T208">qup</ta>
            <ta e="T210" id="Seg_6058" s="T209">tap</ta>
            <ta e="T211" id="Seg_6059" s="T210">əsɨ-n-tɨ</ta>
            <ta e="T212" id="Seg_6060" s="T211">əmɨ-n-tɨ</ta>
            <ta e="T213" id="Seg_6061" s="T212">wäčʼi-sä</ta>
            <ta e="T214" id="Seg_6062" s="T213">nʼäŋɨčʼa</ta>
            <ta e="T215" id="Seg_6063" s="T214">ɔːmta</ta>
            <ta e="T216" id="Seg_6064" s="T215">na</ta>
            <ta e="T217" id="Seg_6065" s="T216">qäqɨ-n</ta>
            <ta e="T218" id="Seg_6066" s="T217">ɔːq-qɨt</ta>
            <ta e="T219" id="Seg_6067" s="T218">na</ta>
            <ta e="T220" id="Seg_6068" s="T219">qaj</ta>
            <ta e="T221" id="Seg_6069" s="T220">na</ta>
            <ta e="T222" id="Seg_6070" s="T221">qup</ta>
            <ta e="T223" id="Seg_6071" s="T222">laŋkɨ-š-mɨ-nta</ta>
            <ta e="T224" id="Seg_6072" s="T223">tan</ta>
            <ta e="T225" id="Seg_6073" s="T224">qaː</ta>
            <ta e="T226" id="Seg_6074" s="T225">nı-j</ta>
            <ta e="T227" id="Seg_6075" s="T226">ɔːmna-nt</ta>
            <ta e="T228" id="Seg_6076" s="T227">aša</ta>
            <ta e="T229" id="Seg_6077" s="T228">mumpa</ta>
            <ta e="T230" id="Seg_6078" s="T229">mat</ta>
            <ta e="T231" id="Seg_6079" s="T230">loːsa</ta>
            <ta e="T232" id="Seg_6080" s="T231">šım</ta>
            <ta e="T233" id="Seg_6081" s="T232">am-qo</ta>
            <ta e="T234" id="Seg_6082" s="T233">ɔːmta-k</ta>
            <ta e="T235" id="Seg_6083" s="T234">konnä-qɨt</ta>
            <ta e="T236" id="Seg_6084" s="T235">qum-iː-mɨ</ta>
            <ta e="T237" id="Seg_6085" s="T236">ɛː-ŋɔː-tɨt</ta>
            <ta e="T238" id="Seg_6086" s="T237">nammɨ-t</ta>
            <ta e="T239" id="Seg_6087" s="T238">šıp</ta>
            <ta e="T240" id="Seg_6088" s="T239">onta-li-mpɔː-tät</ta>
            <ta e="T241" id="Seg_6089" s="T240">loːš</ta>
            <ta e="T242" id="Seg_6090" s="T241">šim</ta>
            <ta e="T243" id="Seg_6091" s="T242">am-qo</ta>
            <ta e="T244" id="Seg_6092" s="T243">olqa</ta>
            <ta e="T245" id="Seg_6093" s="T244">mat</ta>
            <ta e="T246" id="Seg_6094" s="T245">konna</ta>
            <ta e="T247" id="Seg_6095" s="T246">tann-ɛnta-k</ta>
            <ta e="T248" id="Seg_6096" s="T247">loːs</ta>
            <ta e="T249" id="Seg_6097" s="T248">muntɨk</ta>
            <ta e="T250" id="Seg_6098" s="T249">šımɨt</ta>
            <ta e="T251" id="Seg_6099" s="T250">am-ta</ta>
            <ta e="T252" id="Seg_6100" s="T251">ne</ta>
            <ta e="T253" id="Seg_6101" s="T252">qai-m</ta>
            <ta e="T254" id="Seg_6102" s="T253">naš</ta>
            <ta e="T255" id="Seg_6103" s="T254">qalä-ta</ta>
            <ta e="T256" id="Seg_6104" s="T255">na</ta>
            <ta e="T257" id="Seg_6105" s="T256">qaː</ta>
            <ta e="T258" id="Seg_6106" s="T257">šım</ta>
            <ta e="T259" id="Seg_6107" s="T258">omtɨ-lʼi-mpɔː-tet</ta>
            <ta e="T260" id="Seg_6108" s="T259">tan</ta>
            <ta e="T261" id="Seg_6109" s="T260">konnä</ta>
            <ta e="T262" id="Seg_6110" s="T261">tant-ɨš</ta>
            <ta e="T263" id="Seg_6111" s="T262">tat</ta>
            <ta e="T264" id="Seg_6112" s="T263">nej</ta>
            <ta e="T265" id="Seg_6113" s="T264">konna</ta>
            <ta e="T266" id="Seg_6114" s="T265">tant-aš</ta>
            <ta e="T267" id="Seg_6115" s="T266">mat</ta>
            <ta e="T268" id="Seg_6116" s="T267">kutar</ta>
            <ta e="T269" id="Seg_6117" s="T268">tann-ɛnta-k</ta>
            <ta e="T270" id="Seg_6118" s="T269">qum-ɨ-t</ta>
            <ta e="T271" id="Seg_6119" s="T270">šıp</ta>
            <ta e="T272" id="Seg_6120" s="T271">qo-lʼčʼ-antɔː-tɨt</ta>
            <ta e="T273" id="Seg_6121" s="T272">mat</ta>
            <ta e="T274" id="Seg_6122" s="T273">tom-na-p</ta>
            <ta e="T275" id="Seg_6123" s="T274">tan</ta>
            <ta e="T276" id="Seg_6124" s="T275">kon</ta>
            <ta e="T277" id="Seg_6125" s="T276">tant-aš</ta>
            <ta e="T278" id="Seg_6126" s="T277">konna</ta>
            <ta e="T279" id="Seg_6127" s="T278">na</ta>
            <ta e="T280" id="Seg_6128" s="T279">tannɨ-ntɔː-qaj</ta>
            <ta e="T281" id="Seg_6129" s="T280">sɨlʼšepɨlʼše-t</ta>
            <ta e="T282" id="Seg_6130" s="T281">qəš</ta>
            <ta e="T283" id="Seg_6131" s="T282">pɔːrkä</ta>
            <ta e="T284" id="Seg_6132" s="T283">na</ta>
            <ta e="T285" id="Seg_6133" s="T284">meː-lʼčʼi-ntɨ-tɨ</ta>
            <ta e="T286" id="Seg_6134" s="T285">täːqa-ntɨ-sa</ta>
            <ta e="T287" id="Seg_6135" s="T286">tat</ta>
            <ta e="T288" id="Seg_6136" s="T287">na pa</ta>
            <ta e="T289" id="Seg_6137" s="T288">mašıp</ta>
            <ta e="T290" id="Seg_6138" s="T289">kušan</ta>
            <ta e="T291" id="Seg_6139" s="T290">ɨk</ta>
            <ta e="T292" id="Seg_6140" s="T291">šıp</ta>
            <ta e="T293" id="Seg_6141" s="T292">kət-ašik</ta>
            <ta e="T294" id="Seg_6142" s="T293">loːs</ta>
            <ta e="T295" id="Seg_6143" s="T294">taštɨ</ta>
            <ta e="T296" id="Seg_6144" s="T295">na</ta>
            <ta e="T297" id="Seg_6145" s="T296">soqɨčʼ-ɛnnɨ-nta</ta>
            <ta e="T298" id="Seg_6146" s="T297">sɨlʼčʼapɨlʼčʼa-t</ta>
            <ta e="T299" id="Seg_6147" s="T298">qəš</ta>
            <ta e="T300" id="Seg_6148" s="T299">kučʼa</ta>
            <ta e="T301" id="Seg_6149" s="T300">qatɨ-sä</ta>
            <ta e="T302" id="Seg_6150" s="T301">mašıp</ta>
            <ta e="T303" id="Seg_6151" s="T302">kušen-na</ta>
            <ta e="T304" id="Seg_6152" s="T303">šıp</ta>
            <ta e="T305" id="Seg_6153" s="T304">kət-ašik</ta>
            <ta e="T306" id="Seg_6154" s="T305">nı-lʼ</ta>
            <ta e="T307" id="Seg_6155" s="T306">äːkɨlʼčʼi-mpa-ta</ta>
            <ta e="T308" id="Seg_6156" s="T307">täːqa-sä</ta>
            <ta e="T309" id="Seg_6157" s="T308">konna</ta>
            <ta e="T310" id="Seg_6158" s="T309">na</ta>
            <ta e="T311" id="Seg_6159" s="T310">qən-ta</ta>
            <ta e="T312" id="Seg_6160" s="T311">nɨː</ta>
            <ta e="T313" id="Seg_6161" s="T312">mont</ta>
            <ta e="T314" id="Seg_6162" s="T313">čʼu-lʼ</ta>
            <ta e="T315" id="Seg_6163" s="T314">mɔːt</ta>
            <ta e="T316" id="Seg_6164" s="T315">na-qät</ta>
            <ta e="T317" id="Seg_6165" s="T316">mɔːta-n</ta>
            <ta e="T318" id="Seg_6166" s="T317">ɔːq-qɨt</ta>
            <ta e="T319" id="Seg_6167" s="T318">aj</ta>
            <ta e="T320" id="Seg_6168" s="T319">pɔːrkä</ta>
            <ta e="T321" id="Seg_6169" s="T320">na</ta>
            <ta e="T322" id="Seg_6170" s="T321">meː-ntɨ-tɨ</ta>
            <ta e="T323" id="Seg_6171" s="T322">mɔːt</ta>
            <ta e="T324" id="Seg_6172" s="T323">šeːr-äšik</ta>
            <ta e="T325" id="Seg_6173" s="T324">qaː</ta>
            <ta e="T326" id="Seg_6174" s="T325">nɨŋ-a-ntɨ</ta>
            <ta e="T327" id="Seg_6175" s="T326">qante-ni-nt</ta>
            <ta e="T328" id="Seg_6176" s="T327">mat</ta>
            <ta e="T329" id="Seg_6177" s="T328">qäntɨk</ta>
            <ta e="T330" id="Seg_6178" s="T329">šeːr-ta-k</ta>
            <ta e="T331" id="Seg_6179" s="T330">qum-iː-mɨ</ta>
            <ta e="T332" id="Seg_6180" s="T331">šıp</ta>
            <ta e="T333" id="Seg_6181" s="T332">qo-lʼčʼ-antɔː-tɨt</ta>
            <ta e="T334" id="Seg_6182" s="T333">tat</ta>
            <ta e="T335" id="Seg_6183" s="T334">ola</ta>
            <ta e="T336" id="Seg_6184" s="T335">mɔːt</ta>
            <ta e="T337" id="Seg_6185" s="T336">šeːr-äšek</ta>
            <ta e="T338" id="Seg_6186" s="T337">na</ta>
            <ta e="T339" id="Seg_6187" s="T338">mɔːta-n</ta>
            <ta e="T340" id="Seg_6188" s="T339">ɔːq-qɨt</ta>
            <ta e="T341" id="Seg_6189" s="T340">aj</ta>
            <ta e="T342" id="Seg_6190" s="T341">pɔːrkä</ta>
            <ta e="T343" id="Seg_6191" s="T342">meː-ntɨ-t</ta>
            <ta e="T344" id="Seg_6192" s="T343">tat</ta>
            <ta e="T345" id="Seg_6193" s="T344">na pa</ta>
            <ta e="T346" id="Seg_6194" s="T345">mašıp</ta>
            <ta e="T347" id="Seg_6195" s="T346">ɨkɨ</ta>
            <ta e="T348" id="Seg_6196" s="T347">šıp</ta>
            <ta e="T349" id="Seg_6197" s="T348">kət-äšik</ta>
            <ta e="T350" id="Seg_6198" s="T349">taːqa-sä</ta>
            <ta e="T351" id="Seg_6199" s="T350">äːkɨlʼčʼi-ŋɨ-tɨ</ta>
            <ta e="T352" id="Seg_6200" s="T351">mɔːt</ta>
            <ta e="T353" id="Seg_6201" s="T352">na</ta>
            <ta e="T354" id="Seg_6202" s="T353">šeːra</ta>
            <ta e="T355" id="Seg_6203" s="T354">tɨna</ta>
            <ta e="T356" id="Seg_6204" s="T355">qup</ta>
            <ta e="T357" id="Seg_6205" s="T356">əːtɨmɨntɨ</ta>
            <ta e="T358" id="Seg_6206" s="T357">munta</ta>
            <ta e="T359" id="Seg_6207" s="T358">na</ta>
            <ta e="T360" id="Seg_6208" s="T359">qo-lʼčʼi-ntɔː-tɨt</ta>
            <ta e="T361" id="Seg_6209" s="T360">na</ta>
            <ta e="T362" id="Seg_6210" s="T361">mumpa</ta>
            <ta e="T363" id="Seg_6211" s="T362">qaː</ta>
            <ta e="T364" id="Seg_6212" s="T363">šeːr-na</ta>
            <ta e="T365" id="Seg_6213" s="T364">aša</ta>
            <ta e="T366" id="Seg_6214" s="T365">mumpa</ta>
            <ta e="T367" id="Seg_6215" s="T366">qup-ti</ta>
            <ta e="T368" id="Seg_6216" s="T367">mɔːt-tɨ</ta>
            <ta e="T369" id="Seg_6217" s="T368">šıp</ta>
            <ta e="T370" id="Seg_6218" s="T369">üːtɨ-sa</ta>
            <ta e="T371" id="Seg_6219" s="T370">qos</ta>
            <ta e="T372" id="Seg_6220" s="T371">qaj-e-lʼ</ta>
            <ta e="T373" id="Seg_6221" s="T372">qup</ta>
            <ta e="T374" id="Seg_6222" s="T373">tü-sa</ta>
            <ta e="T375" id="Seg_6223" s="T374">mašıp</ta>
            <ta e="T376" id="Seg_6224" s="T375">mɔːt</ta>
            <ta e="T377" id="Seg_6225" s="T376">šım</ta>
            <ta e="T378" id="Seg_6226" s="T377">üːtä-sa</ta>
            <ta e="T379" id="Seg_6227" s="T378">konnɨ</ta>
            <ta e="T380" id="Seg_6228" s="T379">šep</ta>
            <ta e="T381" id="Seg_6229" s="T380">tattɨ-ralte-s</ta>
            <ta e="T382" id="Seg_6230" s="T381">täp</ta>
            <ta e="T383" id="Seg_6231" s="T382">čʼap</ta>
            <ta e="T384" id="Seg_6232" s="T383">qo-ŋɨ-tɨ</ta>
            <ta e="T385" id="Seg_6233" s="T384">mɔːta-t</ta>
            <ta e="T386" id="Seg_6234" s="T385">nʼenna-lʼ</ta>
            <ta e="T387" id="Seg_6235" s="T386">pɛläq-qɨt</ta>
            <ta e="T388" id="Seg_6236" s="T387">aj</ta>
            <ta e="T389" id="Seg_6237" s="T388">ukkɨr</ta>
            <ta e="T390" id="Seg_6238" s="T389">nʼäŋɨčʼa</ta>
            <ta e="T391" id="Seg_6239" s="T390">qup</ta>
            <ta e="T392" id="Seg_6240" s="T391">qom-ta</ta>
            <ta e="T393" id="Seg_6241" s="T392">nɨːno</ta>
            <ta e="T394" id="Seg_6242" s="T393">qa</ta>
            <ta e="T395" id="Seg_6243" s="T394">nılʼčʼi-k</ta>
            <ta e="T396" id="Seg_6244" s="T395">ɛsa</ta>
            <ta e="T397" id="Seg_6245" s="T396">qum-ɨ-p</ta>
            <ta e="T398" id="Seg_6246" s="T397">ınnä</ta>
            <ta e="T399" id="Seg_6247" s="T398">taqt-altɔː-tet</ta>
            <ta e="T400" id="Seg_6248" s="T399">nɨːnɨ</ta>
            <ta e="T401" id="Seg_6249" s="T400">pona</ta>
            <ta e="T402" id="Seg_6250" s="T401">tantɨ-lʼä</ta>
            <ta e="T403" id="Seg_6251" s="T402">üŋkɨl-tɨ-kk-olʼ-čʼi-mpa-tɨ</ta>
            <ta e="T404" id="Seg_6252" s="T403">loːsɨ</ta>
            <ta e="T405" id="Seg_6253" s="T404">kutɨ-lʼ</ta>
            <ta e="T406" id="Seg_6254" s="T405">mɔːntoː-qɨt</ta>
            <ta e="T407" id="Seg_6255" s="T406">qompɨ-š-ka</ta>
            <ta e="T408" id="Seg_6256" s="T407">loːsa</ta>
            <ta e="T409" id="Seg_6257" s="T408">qata</ta>
            <ta e="T410" id="Seg_6258" s="T409">qompɨ-š-nä</ta>
            <ta e="T411" id="Seg_6259" s="T410">mɔːt</ta>
            <ta e="T412" id="Seg_6260" s="T411">šeːr-nɨlɨt</ta>
            <ta e="T413" id="Seg_6261" s="T412">ukkɨr</ta>
            <ta e="T414" id="Seg_6262" s="T413">qup</ta>
            <ta e="T415" id="Seg_6263" s="T414">üŋkɨl-tɨ-mpɨ-ŋɨjä</ta>
            <ta e="T416" id="Seg_6264" s="T415">mompa</ta>
            <ta e="T417" id="Seg_6265" s="T416">üːtɨ-lʼ</ta>
            <ta e="T418" id="Seg_6266" s="T417">kəntɨ-tɨ</ta>
            <ta e="T419" id="Seg_6267" s="T418">tokk-ɨ-čʼ-čʼe-nta</ta>
            <ta e="T420" id="Seg_6268" s="T419">našat</ta>
            <ta e="T421" id="Seg_6269" s="T420">tantɨ-kk-olʼ-čʼi-mpa</ta>
            <ta e="T422" id="Seg_6270" s="T421">qompa-čʼ-čʼe-ntɨ</ta>
            <ta e="T423" id="Seg_6271" s="T422">seːpɨlak</ta>
            <ta e="T424" id="Seg_6272" s="T423">ɛː-nta</ta>
            <ta e="T425" id="Seg_6273" s="T424">ukkɨr</ta>
            <ta e="T426" id="Seg_6274" s="T425">tät</ta>
            <ta e="T427" id="Seg_6275" s="T426">čʼontoː-qɨt</ta>
            <ta e="T428" id="Seg_6276" s="T427">qup</ta>
            <ta e="T429" id="Seg_6277" s="T428">na</ta>
            <ta e="T430" id="Seg_6278" s="T429">tannɨ-nta</ta>
            <ta e="T431" id="Seg_6279" s="T430">mompa</ta>
            <ta e="T432" id="Seg_6280" s="T431">qare-t</ta>
            <ta e="T433" id="Seg_6281" s="T432">na</ta>
            <ta e="T434" id="Seg_6282" s="T433">qompɨ-čʼ-čʼa</ta>
            <ta e="T435" id="Seg_6283" s="T434">mɔːt-tɨ</ta>
            <ta e="T436" id="Seg_6284" s="T435">pat-qɨl-na</ta>
            <ta e="T437" id="Seg_6285" s="T436">loːsɨ</ta>
            <ta e="T438" id="Seg_6286" s="T437">na</ta>
            <ta e="T439" id="Seg_6287" s="T438">qompɨ-čʼ-čʼa</ta>
            <ta e="T440" id="Seg_6288" s="T439">a</ta>
            <ta e="T441" id="Seg_6289" s="T440">mompa</ta>
            <ta e="T442" id="Seg_6290" s="T441">nɔːtə</ta>
            <ta e="T443" id="Seg_6291" s="T442">*taŋɨ-ŋ</ta>
            <ta e="T444" id="Seg_6292" s="T443">ɔːmtɨ-ŋɨlɨt</ta>
            <ta e="T445" id="Seg_6293" s="T444">nʼi</ta>
            <ta e="T446" id="Seg_6294" s="T445">kušat</ta>
            <ta e="T447" id="Seg_6295" s="T446">ɨkɨ</ta>
            <ta e="T448" id="Seg_6296" s="T447">tantɨ-ŋɨlit</ta>
            <ta e="T449" id="Seg_6297" s="T448">sɨlʼčʼipɨlʼčʼi-t</ta>
            <ta e="T450" id="Seg_6298" s="T449">qəš</ta>
            <ta e="T451" id="Seg_6299" s="T450">üŋkul-ti-mpe-tɨ</ta>
            <ta e="T452" id="Seg_6300" s="T451">qaj</ta>
            <ta e="T453" id="Seg_6301" s="T452">konnä</ta>
            <ta e="T454" id="Seg_6302" s="T453">na</ta>
            <ta e="T455" id="Seg_6303" s="T454">tannɨ-ntɨ-ŋa</ta>
            <ta e="T456" id="Seg_6304" s="T455">tannɨ-nta</ta>
            <ta e="T457" id="Seg_6305" s="T456">peːka-p</ta>
            <ta e="T458" id="Seg_6306" s="T457">qo-lʼčʼi-tɨ</ta>
            <ta e="T459" id="Seg_6307" s="T458">peːkä</ta>
            <ta e="T460" id="Seg_6308" s="T459">čʼuk</ta>
            <ta e="T461" id="Seg_6309" s="T460">čʼuk</ta>
            <ta e="T462" id="Seg_6310" s="T461">qaj</ta>
            <ta e="T463" id="Seg_6311" s="T462">sɨlšapɨlʼša</ta>
            <ta e="T464" id="Seg_6312" s="T463">qəš</ta>
            <ta e="T465" id="Seg_6313" s="T464">qattü-sa</ta>
            <ta e="T466" id="Seg_6314" s="T465">sɨlšapɨlʼša</ta>
            <ta e="T467" id="Seg_6315" s="T466">qəš</ta>
            <ta e="T468" id="Seg_6316" s="T467">tɨntä</ta>
            <ta e="T469" id="Seg_6317" s="T468">qəs-sa</ta>
            <ta e="T470" id="Seg_6318" s="T469">konna</ta>
            <ta e="T471" id="Seg_6319" s="T470">na</ta>
            <ta e="T472" id="Seg_6320" s="T471">tü-nta</ta>
            <ta e="T473" id="Seg_6321" s="T472">pɔːrkä-p</ta>
            <ta e="T474" id="Seg_6322" s="T473">qo-ŋo-t</ta>
            <ta e="T475" id="Seg_6323" s="T474">pɔːrkä</ta>
            <ta e="T476" id="Seg_6324" s="T475">kətä-tɨ</ta>
            <ta e="T477" id="Seg_6325" s="T476">sɨlʼšapɨlʼša</ta>
            <ta e="T478" id="Seg_6326" s="T477">qəš</ta>
            <ta e="T479" id="Seg_6327" s="T478">mɔːt-qɨn</ta>
            <ta e="T480" id="Seg_6328" s="T479">ɔːmta</ta>
            <ta e="T481" id="Seg_6329" s="T480">loːsɨ</ta>
            <ta e="T482" id="Seg_6330" s="T481">karra</ta>
            <ta e="T483" id="Seg_6331" s="T482">kolʼi-mɔːlʼ-lʼä </ta>
            <ta e="T484" id="Seg_6332" s="T483">kur-olʼ-na</ta>
            <ta e="T485" id="Seg_6333" s="T484">qən-na</ta>
            <ta e="T486" id="Seg_6334" s="T485">üt-tɨ</ta>
            <ta e="T487" id="Seg_6335" s="T486">alʼčʼa</ta>
            <ta e="T488" id="Seg_6336" s="T487">mɨta</ta>
            <ta e="T489" id="Seg_6337" s="T488">qur</ta>
            <ta e="T490" id="Seg_6338" s="T489">qäːš</ta>
            <ta e="T491" id="Seg_6339" s="T490">pona</ta>
            <ta e="T492" id="Seg_6340" s="T491">paktɨ-lʼä</ta>
            <ta e="T493" id="Seg_6341" s="T492">tıntena</ta>
            <ta e="T494" id="Seg_6342" s="T493">sɨlʼčʼapɨlʼčʼa</ta>
            <ta e="T495" id="Seg_6343" s="T494">qəš</ta>
            <ta e="T496" id="Seg_6344" s="T495">pɔːrkä-m-tɨ</ta>
            <ta e="T497" id="Seg_6345" s="T496">pačʼa-l-lä</ta>
            <ta e="T498" id="Seg_6346" s="T497">sılʼ-lʼaltä-tɨ</ta>
            <ta e="T499" id="Seg_6347" s="T498">mačʼa</ta>
            <ta e="T500" id="Seg_6348" s="T499">qatt-ɔːl-ni-t</ta>
            <ta e="T501" id="Seg_6349" s="T500">jarɨk</ta>
            <ta e="T502" id="Seg_6350" s="T501">pɔːrkä-p</ta>
            <ta e="T503" id="Seg_6351" s="T502">meː-ŋe-tɨ</ta>
            <ta e="T504" id="Seg_6352" s="T503">čʼarrä</ta>
            <ta e="T505" id="Seg_6353" s="T504">äkulʼčʼi-mpɨ-ŋɨ-tɨ</ta>
            <ta e="T506" id="Seg_6354" s="T505">tat</ta>
            <ta e="T507" id="Seg_6355" s="T506">na pa</ta>
            <ta e="T508" id="Seg_6356" s="T507">mašıp</ta>
            <ta e="T509" id="Seg_6357" s="T508">nʼi</ta>
            <ta e="T510" id="Seg_6358" s="T509">kušat</ta>
            <ta e="T511" id="Seg_6359" s="T510">kušan-nɨ</ta>
            <ta e="T512" id="Seg_6360" s="T511">ɨkɨ</ta>
            <ta e="T513" id="Seg_6361" s="T512">kət-ašik</ta>
            <ta e="T514" id="Seg_6362" s="T513">tiː</ta>
            <ta e="T515" id="Seg_6363" s="T514">tantɨ-kk-olʼ-čʼi-mpa</ta>
            <ta e="T516" id="Seg_6364" s="T515">qarɨ-n-ɨ-lʼ</ta>
            <ta e="T517" id="Seg_6365" s="T516">pi-t</ta>
            <ta e="T518" id="Seg_6366" s="T517">nɨːnɨ</ta>
            <ta e="T519" id="Seg_6367" s="T518">pi-n-tɨ</ta>
            <ta e="T520" id="Seg_6368" s="T519">kuntɨ</ta>
            <ta e="T521" id="Seg_6369" s="T520">na</ta>
            <ta e="T522" id="Seg_6370" s="T521">ɔːmne-ntɔː-tɨt</ta>
            <ta e="T523" id="Seg_6371" s="T522">qar-n-ɨ-lʼ</ta>
            <ta e="T524" id="Seg_6372" s="T523">pi-t</ta>
            <ta e="T525" id="Seg_6373" s="T524">qum-ɨ-p</ta>
            <ta e="T526" id="Seg_6374" s="T525">kur-alʼtɔː-tɨt</ta>
            <ta e="T527" id="Seg_6375" s="T526">ponä</ta>
            <ta e="T528" id="Seg_6376" s="T527">mannɨ-mpɔː-tät</ta>
            <ta e="T529" id="Seg_6377" s="T528">čʼɔːlsä</ta>
            <ta e="T530" id="Seg_6378" s="T529">aša</ta>
            <ta e="T531" id="Seg_6379" s="T530">tanta</ta>
            <ta e="T532" id="Seg_6380" s="T531">sɨlʼčʼapɨlʼčʼa</ta>
            <ta e="T533" id="Seg_6381" s="T532">qəš</ta>
            <ta e="T534" id="Seg_6382" s="T533">kur-alʼtɔː-tɨt</ta>
            <ta e="T535" id="Seg_6383" s="T534">Üŋkɨl-tɨ-mp-ätɨ</ta>
            <ta e="T536" id="Seg_6384" s="T535">seːpɨlak</ta>
            <ta e="T537" id="Seg_6385" s="T536">qup</ta>
            <ta e="T538" id="Seg_6386" s="T537">na</ta>
            <ta e="T539" id="Seg_6387" s="T538">lʼämɨ-k</ta>
            <ta e="T540" id="Seg_6388" s="T539">ɛi-nta</ta>
            <ta e="T541" id="Seg_6389" s="T540">mɔːt-tə</ta>
            <ta e="T542" id="Seg_6390" s="T541">na</ta>
            <ta e="T543" id="Seg_6391" s="T542">alʼčʼi-nta</ta>
            <ta e="T544" id="Seg_6392" s="T543">mɨtta</ta>
            <ta e="T545" id="Seg_6393" s="T544">ukkɨr</ta>
            <ta e="T546" id="Seg_6394" s="T545">loːsɨ</ta>
            <ta e="T547" id="Seg_6395" s="T546">qompɨ-š-pa</ta>
            <ta e="T548" id="Seg_6396" s="T547">loːsɨ</ta>
            <ta e="T549" id="Seg_6397" s="T548">na</ta>
            <ta e="T550" id="Seg_6398" s="T549">qompɨ-š-pa</ta>
            <ta e="T551" id="Seg_6399" s="T550">qapija</ta>
            <ta e="T552" id="Seg_6400" s="T551">šilʼšapalʼša</ta>
            <ta e="T553" id="Seg_6401" s="T552">ontə</ta>
            <ta e="T554" id="Seg_6402" s="T553">üŋkɨl-ti-mpa-tɨ</ta>
            <ta e="T555" id="Seg_6403" s="T554">na</ta>
            <ta e="T556" id="Seg_6404" s="T555">tannɨ-nta</ta>
            <ta e="T557" id="Seg_6405" s="T556">konnä</ta>
            <ta e="T558" id="Seg_6406" s="T557">kurɨ-lʼä</ta>
            <ta e="T559" id="Seg_6407" s="T558">tap</ta>
            <ta e="T560" id="Seg_6408" s="T559">tü-kkɨ-nä</ta>
            <ta e="T561" id="Seg_6409" s="T560">kurɨ-lʼä</ta>
            <ta e="T562" id="Seg_6410" s="T561">tap</ta>
            <ta e="T563" id="Seg_6411" s="T562">tü-kkɨ-nä</ta>
            <ta e="T564" id="Seg_6412" s="T563">na</ta>
            <ta e="T565" id="Seg_6413" s="T564">qə-t</ta>
            <ta e="T566" id="Seg_6414" s="T565">pɔːrɨ-ɨ-ntɨ</ta>
            <ta e="T567" id="Seg_6415" s="T566">na</ta>
            <ta e="T568" id="Seg_6416" s="T567">tannɨ-nta</ta>
            <ta e="T569" id="Seg_6417" s="T568">tɨntäna</ta>
            <ta e="T570" id="Seg_6418" s="T569">pɔːrkä-ntɨ</ta>
            <ta e="T571" id="Seg_6419" s="T570">kurɨ-lʼä</ta>
            <ta e="T572" id="Seg_6420" s="T571">na</ta>
            <ta e="T573" id="Seg_6421" s="T572">tü-nta</ta>
            <ta e="T574" id="Seg_6422" s="T573">mɨta</ta>
            <ta e="T575" id="Seg_6423" s="T574">čʼuk</ta>
            <ta e="T576" id="Seg_6424" s="T575">čʼuk</ta>
            <ta e="T577" id="Seg_6425" s="T576">čʼuk</ta>
            <ta e="T578" id="Seg_6426" s="T577">qaj</ta>
            <ta e="T579" id="Seg_6427" s="T578">sɨlʼčʼapɨlʼčʼa</ta>
            <ta e="T580" id="Seg_6428" s="T579">qəš</ta>
            <ta e="T581" id="Seg_6429" s="T580">qattü-je</ta>
            <ta e="T582" id="Seg_6430" s="T581">qaj</ta>
            <ta e="T583" id="Seg_6431" s="T582">mɔːt-qɨn</ta>
            <ta e="T584" id="Seg_6432" s="T583">ɛː-ŋa</ta>
            <ta e="T585" id="Seg_6433" s="T584">sɨlʼčʼapɨlʼčʼa</ta>
            <ta e="T586" id="Seg_6434" s="T585">qəš</ta>
            <ta e="T587" id="Seg_6435" s="T586">našat</ta>
            <ta e="T588" id="Seg_6436" s="T587">qəs-sa</ta>
            <ta e="T589" id="Seg_6437" s="T588">tan</ta>
            <ta e="T590" id="Seg_6438" s="T589">konna</ta>
            <ta e="T591" id="Seg_6439" s="T590">qəl-la</ta>
            <ta e="T592" id="Seg_6440" s="T591">apso-lʼ</ta>
            <ta e="T593" id="Seg_6441" s="T592">am-tɨ</ta>
            <ta e="T594" id="Seg_6442" s="T593">toːnna</ta>
            <ta e="T595" id="Seg_6443" s="T594">kurɨ-lʼä</ta>
            <ta e="T596" id="Seg_6444" s="T595">na</ta>
            <ta e="T597" id="Seg_6445" s="T596">qən-ta-nəəə</ta>
            <ta e="T598" id="Seg_6446" s="T597">mɔːta-n</ta>
            <ta e="T599" id="Seg_6447" s="T598">ɔːk-ɨ-lʼ</ta>
            <ta e="T600" id="Seg_6448" s="T599">tü-nta-nəəə</ta>
            <ta e="T601" id="Seg_6449" s="T600">pɔːrkä-ntɨ</ta>
            <ta e="T602" id="Seg_6450" s="T601">tüː-ŋa</ta>
            <ta e="T603" id="Seg_6451" s="T602">qattü-sa</ta>
            <ta e="T604" id="Seg_6452" s="T603">sɨlʼčʼapɨlʼčʼa</ta>
            <ta e="T605" id="Seg_6453" s="T604">qəš</ta>
            <ta e="T606" id="Seg_6454" s="T605">tep</ta>
            <ta e="T607" id="Seg_6455" s="T606">aša</ta>
            <ta e="T608" id="Seg_6456" s="T607">täːlʼa</ta>
            <ta e="T609" id="Seg_6457" s="T608">našak</ta>
            <ta e="T610" id="Seg_6458" s="T609">qəs-sa</ta>
            <ta e="T611" id="Seg_6459" s="T610">qəs-s-ɨ-ŋa</ta>
            <ta e="T612" id="Seg_6460" s="T611">mɔːt-tɨ</ta>
            <ta e="T613" id="Seg_6461" s="T612">šeːr-lʼa</ta>
            <ta e="T614" id="Seg_6462" s="T613">apso-lʼ</ta>
            <ta e="T615" id="Seg_6463" s="T614">am-tɨ</ta>
            <ta e="T616" id="Seg_6464" s="T615">ukkɨr</ta>
            <ta e="T617" id="Seg_6465" s="T616">čʼontoː-qɨt</ta>
            <ta e="T618" id="Seg_6466" s="T617">loːsɨ</ta>
            <ta e="T619" id="Seg_6467" s="T618">mɔːt-tɨ</ta>
            <ta e="T620" id="Seg_6468" s="T619">na</ta>
            <ta e="T621" id="Seg_6469" s="T620">noqq-ɔːl-ta</ta>
            <ta e="T622" id="Seg_6470" s="T621">mɔːt-tɨ</ta>
            <ta e="T623" id="Seg_6471" s="T622">na</ta>
            <ta e="T624" id="Seg_6472" s="T623">noqq-ɔːlʼ-ta</ta>
            <ta e="T625" id="Seg_6473" s="T624">mɔːt-tɨ</ta>
            <ta e="T626" id="Seg_6474" s="T625">čʼep</ta>
            <ta e="T627" id="Seg_6475" s="T626">šeːr-na</ta>
            <ta e="T628" id="Seg_6476" s="T627">loːsɨ</ta>
            <ta e="T629" id="Seg_6477" s="T628">sɨlʼčʼapɨlʼčʼa</ta>
            <ta e="T630" id="Seg_6478" s="T629">qəš</ta>
            <ta e="T631" id="Seg_6479" s="T630">počʼčʼ-alʼ-nɨ-tɨ</ta>
            <ta e="T632" id="Seg_6480" s="T631">ola-n-tɨ</ta>
            <ta e="T633" id="Seg_6481" s="T632">laka</ta>
            <ta e="T634" id="Seg_6482" s="T633">mɔːt-tɨ</ta>
            <ta e="T635" id="Seg_6483" s="T634">šuː</ta>
            <ta e="T636" id="Seg_6484" s="T635">nʼanna</ta>
            <ta e="T637" id="Seg_6485" s="T636">püŋk-olʼ-na</ta>
            <ta e="T638" id="Seg_6486" s="T637">kəpɨ-n-tɨ</ta>
            <ta e="T639" id="Seg_6487" s="T638">laka</ta>
            <ta e="T640" id="Seg_6488" s="T639">qottä</ta>
            <ta e="T641" id="Seg_6489" s="T640">ponä</ta>
            <ta e="T642" id="Seg_6490" s="T641">alʼčʼa</ta>
            <ta e="T643" id="Seg_6491" s="T642">qət-te-nti-t</ta>
            <ta e="T644" id="Seg_6492" s="T643">ponä</ta>
            <ta e="T645" id="Seg_6493" s="T644">tattɨ-ŋɨ-t</ta>
            <ta e="T646" id="Seg_6494" s="T645">tä</ta>
            <ta e="T647" id="Seg_6495" s="T646">sɨlʼčʼapɨlʼčʼa</ta>
            <ta e="T648" id="Seg_6496" s="T647">qəš</ta>
            <ta e="T649" id="Seg_6497" s="T648">kəpɨ-n-tɨ</ta>
            <ta e="T650" id="Seg_6498" s="T649">laka</ta>
            <ta e="T651" id="Seg_6499" s="T650">ponä</ta>
            <ta e="T652" id="Seg_6500" s="T651">tanta</ta>
            <ta e="T653" id="Seg_6501" s="T652">monte</ta>
            <ta e="T654" id="Seg_6502" s="T653">čʼeːlɨ-ŋ-ɛlʼčʼa</ta>
            <ta e="T655" id="Seg_6503" s="T654">karrə</ta>
            <ta e="T656" id="Seg_6504" s="T655">tulʼ-tɨ-ŋɨ-t</ta>
            <ta e="T657" id="Seg_6505" s="T656">sɨlʼčʼapɨlʼčʼa</ta>
            <ta e="T658" id="Seg_6506" s="T657">Qəš</ta>
            <ta e="T659" id="Seg_6507" s="T658">kəpɨ-n-tɨ</ta>
            <ta e="T660" id="Seg_6508" s="T659">laka-p</ta>
            <ta e="T661" id="Seg_6509" s="T660">üt-ɨ-lʼ</ta>
            <ta e="T662" id="Seg_6510" s="T661">qəqo-ntə</ta>
            <ta e="T663" id="Seg_6511" s="T662">pin-ne-te</ta>
            <ta e="T664" id="Seg_6512" s="T663">ulqo-sä</ta>
            <ta e="T665" id="Seg_6513" s="T664">nʼentɨ</ta>
            <ta e="T666" id="Seg_6514" s="T665">qant-äpti-tɨ</ta>
            <ta e="T667" id="Seg_6515" s="T666">nılʼčʼi-k</ta>
            <ta e="T668" id="Seg_6516" s="T667">pin-ne-te</ta>
            <ta e="T669" id="Seg_6517" s="T668">üt-ɨ-lʼ</ta>
            <ta e="T670" id="Seg_6518" s="T669">qəqɨ-n-tɨ</ta>
            <ta e="T671" id="Seg_6519" s="T670">qanɨk-tɨ</ta>
            <ta e="T672" id="Seg_6520" s="T671">nılʼčʼi-k</ta>
            <ta e="T673" id="Seg_6521" s="T672">pin-nɨ-t</ta>
            <ta e="T674" id="Seg_6522" s="T673">mit</ta>
            <ta e="T675" id="Seg_6523" s="T674">ola</ta>
            <ta e="T676" id="Seg_6524" s="T675">ilɨ-lʼä</ta>
            <ta e="T677" id="Seg_6525" s="T676">ippa</ta>
            <ta e="T678" id="Seg_6526" s="T677">olɨ-ntä-sä</ta>
            <ta e="T679" id="Seg_6527" s="T678">nʼentɨ</ta>
            <ta e="T680" id="Seg_6528" s="T679">loːq-altɨ-ŋɨ-tɨ</ta>
            <ta e="T681" id="Seg_6529" s="T680">na</ta>
            <ta e="T682" id="Seg_6530" s="T681">čʼeːla-tɨ-ŋɔː-mɨt</ta>
            <ta e="T683" id="Seg_6531" s="T682">lɨp-k-o-mɔːta</ta>
            <ta e="T684" id="Seg_6532" s="T683">mompa</ta>
            <ta e="T685" id="Seg_6533" s="T684">kekkɨsä</ta>
            <ta e="T686" id="Seg_6534" s="T685">šittä-qı-j</ta>
            <ta e="T687" id="Seg_6535" s="T686">jarɨk</ta>
            <ta e="T688" id="Seg_6536" s="T687">mɨ</ta>
            <ta e="T689" id="Seg_6537" s="T688">ičʼe-qa</ta>
            <ta e="T690" id="Seg_6538" s="T689">aše</ta>
            <ta e="T691" id="Seg_6539" s="T690">tɛnɨmɔː-mɨt</ta>
            <ta e="T692" id="Seg_6540" s="T691">üːtɨ-t</ta>
            <ta e="T693" id="Seg_6541" s="T692">na</ta>
            <ta e="T694" id="Seg_6542" s="T693">lʼip-k-ɨ-mɔːn-na</ta>
            <ta e="T695" id="Seg_6543" s="T694">üːtɨ-t</ta>
            <ta e="T696" id="Seg_6544" s="T695">seːpɨlak</ta>
            <ta e="T697" id="Seg_6545" s="T696">ɔːmta</ta>
            <ta e="T698" id="Seg_6546" s="T697">na</ta>
            <ta e="T699" id="Seg_6547" s="T698">rɛmkɨ-mɔːt-q-olam-na</ta>
            <ta e="T700" id="Seg_6548" s="T699">qum-ɨ-p</ta>
            <ta e="T701" id="Seg_6549" s="T700">ponä</ta>
            <ta e="T702" id="Seg_6550" s="T701">kura-lʼtɨ-tä</ta>
            <ta e="T703" id="Seg_6551" s="T702">Üŋkɨlʼ-tɨ-mp-ätɨ</ta>
            <ta e="T704" id="Seg_6552" s="T703">qup</ta>
            <ta e="T705" id="Seg_6553" s="T704">poː-qɨt</ta>
            <ta e="T706" id="Seg_6554" s="T705">nɨŋa</ta>
            <ta e="T707" id="Seg_6555" s="T706">qəː</ta>
            <ta e="T708" id="Seg_6556" s="T707">qum</ta>
            <ta e="T709" id="Seg_6557" s="T708">na</ta>
            <ta e="T710" id="Seg_6558" s="T709">qompɨ-š-q-olam-na</ta>
            <ta e="T711" id="Seg_6559" s="T710">mɔːt-tɨ</ta>
            <ta e="T712" id="Seg_6560" s="T711">pat-qɨl-na</ta>
            <ta e="T713" id="Seg_6561" s="T712">mɔːt-tɨ</ta>
            <ta e="T714" id="Seg_6562" s="T713">šeːr-lʼä</ta>
            <ta e="T715" id="Seg_6563" s="T714">nılʼčʼi-k</ta>
            <ta e="T716" id="Seg_6564" s="T715">kätɨ-mpa-t</ta>
            <ta e="T717" id="Seg_6565" s="T716">Loːsɨ</ta>
            <ta e="T718" id="Seg_6566" s="T717">karrä-t</ta>
            <ta e="T719" id="Seg_6567" s="T718">qompɨ-nʼ-nʼä</ta>
            <ta e="T720" id="Seg_6568" s="T719">konna</ta>
            <ta e="T721" id="Seg_6569" s="T720">kurɨ-lʼ-lʼä</ta>
            <ta e="T722" id="Seg_6570" s="T721">na</ta>
            <ta e="T723" id="Seg_6571" s="T722">tannɨ-nta</ta>
            <ta e="T724" id="Seg_6572" s="T723">konnä</ta>
            <ta e="T725" id="Seg_6573" s="T724">qaj</ta>
            <ta e="T726" id="Seg_6574" s="T725">qompɨ-čʼ-čʼe</ta>
            <ta e="T727" id="Seg_6575" s="T726">aaa</ta>
            <ta e="T728" id="Seg_6576" s="T727">tan</ta>
            <ta e="T729" id="Seg_6577" s="T728">qaj</ta>
            <ta e="T730" id="Seg_6578" s="T729">kočʼkɨ-mɔːn-a-ntɨ</ta>
            <ta e="T731" id="Seg_6579" s="T730">am-ɨ-r-ɛlʼčʼi-na-ntɨ</ta>
            <ta e="T732" id="Seg_6580" s="T731">narkɨ-mɔːt-i-ja-ntɨ</ta>
            <ta e="T733" id="Seg_6581" s="T732">ukkur</ta>
            <ta e="T734" id="Seg_6582" s="T733">nʼeː-m-tɨ</ta>
            <ta e="T735" id="Seg_6583" s="T734">nɨlʼčʼi-k</ta>
            <ta e="T736" id="Seg_6584" s="T735">qo-ŋɨ-tɨ</ta>
            <ta e="T737" id="Seg_6585" s="T736">man</ta>
            <ta e="T738" id="Seg_6586" s="T737">okoːt</ta>
            <ta e="T739" id="Seg_6587" s="T738">am-ɨ-rr-ɛː-lʼä</ta>
            <ta e="T740" id="Seg_6588" s="T739">nılʼčʼi-k</ta>
            <ta e="T741" id="Seg_6589" s="T740">ippɨ-kk-olʼ-čʼi-mpɨ-kka-k</ta>
            <ta e="T742" id="Seg_6590" s="T741">nɨːnɨ</ta>
            <ta e="T743" id="Seg_6591" s="T742">konna</ta>
            <ta e="T744" id="Seg_6592" s="T743">kurɨ-lʼä</ta>
            <ta e="T745" id="Seg_6593" s="T744">na</ta>
            <ta e="T746" id="Seg_6594" s="T745">tü-nta</ta>
            <ta e="T747" id="Seg_6595" s="T746">konna</ta>
            <ta e="T748" id="Seg_6596" s="T747">kürɨ-lʼä</ta>
            <ta e="T749" id="Seg_6597" s="T748">na</ta>
            <ta e="T750" id="Seg_6598" s="T749">tü-nta</ta>
            <ta e="T751" id="Seg_6599" s="T750">čʼuk</ta>
            <ta e="T752" id="Seg_6600" s="T751">čʼuk</ta>
            <ta e="T753" id="Seg_6601" s="T752">qaj</ta>
            <ta e="T754" id="Seg_6602" s="T753">sɨlʼčʼapɨlʼčʼa</ta>
            <ta e="T755" id="Seg_6603" s="T754">qəš</ta>
            <ta e="T756" id="Seg_6604" s="T755">qatt-ülʼčʼa</ta>
            <ta e="T757" id="Seg_6605" s="T756">tü-ŋa</ta>
            <ta e="T758" id="Seg_6606" s="T757">qaj</ta>
            <ta e="T759" id="Seg_6607" s="T758">qəs-sa</ta>
            <ta e="T760" id="Seg_6608" s="T759">qaj</ta>
            <ta e="T761" id="Seg_6609" s="T760">qattü-ssa</ta>
            <ta e="T762" id="Seg_6610" s="T761">sɨlʼčʼapɨlʼčʼa</ta>
            <ta e="T763" id="Seg_6611" s="T762">qəš</ta>
            <ta e="T764" id="Seg_6612" s="T763">okoːn</ta>
            <ta e="T765" id="Seg_6613" s="T764">naša</ta>
            <ta e="T766" id="Seg_6614" s="T765">qəs-sa</ta>
            <ta e="T767" id="Seg_6615" s="T766">tıntɨ</ta>
            <ta e="T768" id="Seg_6616" s="T767">naša</ta>
            <ta e="T769" id="Seg_6617" s="T768">qəs-sa</ta>
            <ta e="T770" id="Seg_6618" s="T769">tan</ta>
            <ta e="T771" id="Seg_6619" s="T770">konna</ta>
            <ta e="T772" id="Seg_6620" s="T771">qəl-la</ta>
            <ta e="T773" id="Seg_6621" s="T772">apso-lʼ</ta>
            <ta e="T774" id="Seg_6622" s="T773">am-tɨ</ta>
            <ta e="T775" id="Seg_6623" s="T774">nɨːna</ta>
            <ta e="T776" id="Seg_6624" s="T775">konna</ta>
            <ta e="T777" id="Seg_6625" s="T776">qəl-lä</ta>
            <ta e="T778" id="Seg_6626" s="T777">kur-lʼä</ta>
            <ta e="T779" id="Seg_6627" s="T778">tıntena</ta>
            <ta e="T780" id="Seg_6628" s="T779">pɔːrkə-ntɨ</ta>
            <ta e="T781" id="Seg_6629" s="T780">tüː-ŋa</ta>
            <ta e="T782" id="Seg_6630" s="T781">kättɨ-tɨ</ta>
            <ta e="T783" id="Seg_6631" s="T782">qaj</ta>
            <ta e="T784" id="Seg_6632" s="T783">sɨlʼčʼapɨlʼčʼa</ta>
            <ta e="T785" id="Seg_6633" s="T784">qəš</ta>
            <ta e="T786" id="Seg_6634" s="T785">mɔːt-qɨn</ta>
            <ta e="T787" id="Seg_6635" s="T786">ɛːi-ŋa</ta>
            <ta e="T788" id="Seg_6636" s="T787">täm</ta>
            <ta e="T789" id="Seg_6637" s="T788">aša</ta>
            <ta e="T790" id="Seg_6638" s="T789">našat</ta>
            <ta e="T791" id="Seg_6639" s="T790">tɨntä</ta>
            <ta e="T792" id="Seg_6640" s="T791">qəs-sa</ta>
            <ta e="T793" id="Seg_6641" s="T792">mɔːt</ta>
            <ta e="T794" id="Seg_6642" s="T793">šeːr-lʼa</ta>
            <ta e="T795" id="Seg_6643" s="T794">apso-lʼ</ta>
            <ta e="T796" id="Seg_6644" s="T795">am-tɨ</ta>
            <ta e="T797" id="Seg_6645" s="T796">ɔːm</ta>
            <ta e="T798" id="Seg_6646" s="T797">aj</ta>
            <ta e="T799" id="Seg_6647" s="T798">nɨlʼčʼi-k</ta>
            <ta e="T800" id="Seg_6648" s="T799">ɛssa</ta>
            <ta e="T801" id="Seg_6649" s="T800">Mɔːt-tɨ</ta>
            <ta e="T802" id="Seg_6650" s="T801">qaj</ta>
            <ta e="T803" id="Seg_6651" s="T802">šeːr-ta-k</ta>
            <ta e="T804" id="Seg_6652" s="T803">qaj</ta>
            <ta e="T805" id="Seg_6653" s="T804">aša</ta>
            <ta e="T806" id="Seg_6654" s="T805">šeːr-ta-k</ta>
            <ta e="T807" id="Seg_6655" s="T806">ɔːm</ta>
            <ta e="T808" id="Seg_6656" s="T807">aj</ta>
            <ta e="T809" id="Seg_6657" s="T808">mɨ</ta>
            <ta e="T810" id="Seg_6658" s="T809">pɔːrkä</ta>
            <ta e="T811" id="Seg_6659" s="T810">qaj</ta>
            <ta e="T812" id="Seg_6660" s="T811">mompa</ta>
            <ta e="T813" id="Seg_6661" s="T812">mašıp</ta>
            <ta e="T814" id="Seg_6662" s="T813">kurɨ-mma-nta</ta>
            <ta e="T815" id="Seg_6663" s="T814">qapı</ta>
            <ta e="T816" id="Seg_6664" s="T815">kulʼtɨ-mpa</ta>
            <ta e="T817" id="Seg_6665" s="T816">kulʼtɨ-mpɨ-ŋa</ta>
            <ta e="T818" id="Seg_6666" s="T817">Ɔːmi-j</ta>
            <ta e="T819" id="Seg_6667" s="T818">qaj</ta>
            <ta e="T820" id="Seg_6668" s="T819">šeːr-ta</ta>
            <ta e="T821" id="Seg_6669" s="T820">qaj</ta>
            <ta e="T822" id="Seg_6670" s="T821">aša</ta>
            <ta e="T823" id="Seg_6671" s="T822">šeːr-ta</ta>
            <ta e="T824" id="Seg_6672" s="T823">okkɨr</ta>
            <ta e="T825" id="Seg_6673" s="T824">čʼontoː-qɨt</ta>
            <ta e="T826" id="Seg_6674" s="T825">na</ta>
            <ta e="T827" id="Seg_6675" s="T826">šeːr-q-olam-ta</ta>
            <ta e="T828" id="Seg_6676" s="T827">olɨ-m-tɨ</ta>
            <ta e="T829" id="Seg_6677" s="T828">mɔːt-tɨ</ta>
            <ta e="T830" id="Seg_6678" s="T829">čʼap</ta>
            <ta e="T831" id="Seg_6679" s="T830">noqq-ɔːl-nɨ-t</ta>
            <ta e="T832" id="Seg_6680" s="T831">sɨlʼčʼapɨlʼčʼa</ta>
            <ta e="T833" id="Seg_6681" s="T832">qəš</ta>
            <ta e="T834" id="Seg_6682" s="T833">pačʼ-al-nɨ-t</ta>
            <ta e="T835" id="Seg_6683" s="T834">ola-n-tɨ</ta>
            <ta e="T836" id="Seg_6684" s="T835">lako</ta>
            <ta e="T837" id="Seg_6685" s="T836">mɔːt-tɨ</ta>
            <ta e="T838" id="Seg_6686" s="T837">nʼannä</ta>
            <ta e="T839" id="Seg_6687" s="T838">alʼčʼa</ta>
            <ta e="T840" id="Seg_6688" s="T839">püŋk-olʼ-na</ta>
            <ta e="T841" id="Seg_6689" s="T840">kopɨ-n-tɨ</ta>
            <ta e="T842" id="Seg_6690" s="T841">laka</ta>
            <ta e="T843" id="Seg_6691" s="T842">qottä</ta>
            <ta e="T844" id="Seg_6692" s="T843">ponä</ta>
            <ta e="T845" id="Seg_6693" s="T844">alʼčʼa</ta>
            <ta e="T846" id="Seg_6694" s="T845">qətt-ɛː-ŋɨ-tɨ</ta>
            <ta e="T847" id="Seg_6695" s="T846">nɨːnɨ</ta>
            <ta e="T848" id="Seg_6696" s="T847">nɔːtɨ</ta>
            <ta e="T849" id="Seg_6697" s="T848">čʼap</ta>
            <ta e="T850" id="Seg_6698" s="T849">ɔːmtɔː-tɨt</ta>
            <ta e="T851" id="Seg_6699" s="T850">ɔːmtɔː-tɨt</ta>
            <ta e="T852" id="Seg_6700" s="T851">nʼi</ta>
            <ta e="T853" id="Seg_6701" s="T852">qaj</ta>
            <ta e="T854" id="Seg_6702" s="T853">čʼäŋka</ta>
            <ta e="T855" id="Seg_6703" s="T854">mumpa</ta>
            <ta e="T856" id="Seg_6704" s="T855">qaj</ta>
            <ta e="T857" id="Seg_6705" s="T856">šite</ta>
            <ta e="T858" id="Seg_6706" s="T857">ičʼa</ta>
            <ta e="T859" id="Seg_6707" s="T858">tɔːptɨlʼ</ta>
            <ta e="T860" id="Seg_6708" s="T859">qarɨ-t</ta>
            <ta e="T861" id="Seg_6709" s="T860">ınna</ta>
            <ta e="T862" id="Seg_6710" s="T861">čʼeːlɨ-ŋ-na</ta>
            <ta e="T863" id="Seg_6711" s="T862">mompa</ta>
            <ta e="T864" id="Seg_6712" s="T863">poː</ta>
            <ta e="T865" id="Seg_6713" s="T864">pačʼčʼ-alʼ-nɨlɨt</ta>
            <ta e="T866" id="Seg_6714" s="T865">poː</ta>
            <ta e="T867" id="Seg_6715" s="T866">pačʼčʼ-al-nɔː-tɨt</ta>
            <ta e="T868" id="Seg_6716" s="T867">tü</ta>
            <ta e="T869" id="Seg_6717" s="T868">čʼɔːtɨ-ŋɨlɨt</ta>
            <ta e="T870" id="Seg_6718" s="T869">əːtɨmɨntɨ</ta>
            <ta e="T871" id="Seg_6719" s="T870">poː-p</ta>
            <ta e="T872" id="Seg_6720" s="T871">pačʼčʼ-alʼ-nɔː-tɨt</ta>
            <ta e="T873" id="Seg_6721" s="T872">šittä-qı-p</ta>
            <ta e="T874" id="Seg_6722" s="T873">karrä</ta>
            <ta e="T875" id="Seg_6723" s="T874">tul-to-k-tät</ta>
            <ta e="T876" id="Seg_6724" s="T875">tü-sä</ta>
            <ta e="T877" id="Seg_6725" s="T876">čʼɔːtɔː-tɨt</ta>
            <ta e="T878" id="Seg_6726" s="T877">koptɨ-kɔːlɨ-k</ta>
            <ta e="T879" id="Seg_6727" s="T878">tü-sä</ta>
            <ta e="T880" id="Seg_6728" s="T879">čʼɔːtɔː-tɨt</ta>
            <ta e="T881" id="Seg_6729" s="T880">ira-lʼ</ta>
            <ta e="T882" id="Seg_6730" s="T881">äsä-sɨ-t</ta>
            <ta e="T883" id="Seg_6731" s="T882">na</ta>
            <ta e="T884" id="Seg_6732" s="T883">ira</ta>
            <ta e="T885" id="Seg_6733" s="T884">nälʼä-tɨ</ta>
            <ta e="T886" id="Seg_6734" s="T885">ɛː-ppa</ta>
            <ta e="T887" id="Seg_6735" s="T886">na</ta>
            <ta e="T888" id="Seg_6736" s="T887">nälʼa-m-tɨ</ta>
            <ta e="T889" id="Seg_6737" s="T888">sɨlʼčʼapɨlʼčʼa</ta>
            <ta e="T890" id="Seg_6738" s="T889">qəš-tɨ</ta>
            <ta e="T891" id="Seg_6739" s="T890">mi-ŋa-tɨ</ta>
            <ta e="T892" id="Seg_6740" s="T891">na</ta>
            <ta e="T893" id="Seg_6741" s="T892">təttɨ-t</ta>
            <ta e="T894" id="Seg_6742" s="T893">moːrɨ</ta>
            <ta e="T895" id="Seg_6743" s="T894">kolʼ-alʼtɨ-ntä-kkɨ-t</ta>
            <ta e="T896" id="Seg_6744" s="T895">šölʼqum-ɨ-t-ɨ-tkine</ta>
            <ta e="T897" id="Seg_6745" s="T896">tü-ŋa</ta>
            <ta e="T898" id="Seg_6746" s="T897">moːrɨ-t</ta>
            <ta e="T899" id="Seg_6747" s="T898">na</ta>
            <ta e="T900" id="Seg_6748" s="T899">ɛː-nta</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_6749" s="T0">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T2" id="Seg_6750" s="T1">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T3" id="Seg_6751" s="T2">kəš</ta>
            <ta e="T4" id="Seg_6752" s="T3">ilɨ-mpɨ</ta>
            <ta e="T5" id="Seg_6753" s="T4">ama-tɨ-lʼ</ta>
            <ta e="T6" id="Seg_6754" s="T5">ɛː-mpɨ-ntɨ</ta>
            <ta e="T7" id="Seg_6755" s="T6">əsɨ-tɨ</ta>
            <ta e="T8" id="Seg_6756" s="T7">čʼäːŋkɨ-mpɨ</ta>
            <ta e="T9" id="Seg_6757" s="T8">ilɨ</ta>
            <ta e="T10" id="Seg_6758" s="T9">ilɨ</ta>
            <ta e="T11" id="Seg_6759" s="T10">ukkɨr</ta>
            <ta e="T12" id="Seg_6760" s="T11">čʼontɨ-n</ta>
            <ta e="T13" id="Seg_6761" s="T12">ama-ntɨ</ta>
            <ta e="T14" id="Seg_6762" s="T13">nık</ta>
            <ta e="T15" id="Seg_6763" s="T14">kətɨ-tɨ</ta>
            <ta e="T16" id="Seg_6764" s="T15">kətɨ-tɨ</ta>
            <ta e="T17" id="Seg_6765" s="T16">ama-ntɨ</ta>
            <ta e="T18" id="Seg_6766" s="T17">tıː</ta>
            <ta e="T19" id="Seg_6767" s="T18">nılʼčʼɨ-k</ta>
            <ta e="T20" id="Seg_6768" s="T19">ɛsɨ</ta>
            <ta e="T21" id="Seg_6769" s="T20">man</ta>
            <ta e="T22" id="Seg_6770" s="T21">qum-ɨ-lʼ</ta>
            <ta e="T23" id="Seg_6771" s="T22">peː-r-lä</ta>
            <ta e="T24" id="Seg_6772" s="T23">qən-ŋɨ-k</ta>
            <ta e="T25" id="Seg_6773" s="T24">tılʼčʼɨ</ta>
            <ta e="T26" id="Seg_6774" s="T25">kuttar</ta>
            <ta e="T27" id="Seg_6775" s="T26">ilɨ-ɛntɨ-mɨt</ta>
            <ta e="T28" id="Seg_6776" s="T27">ama</ta>
            <ta e="T29" id="Seg_6777" s="T28">tan</ta>
            <ta e="T30" id="Seg_6778" s="T29">montɨ</ta>
            <ta e="T31" id="Seg_6779" s="T30">qaj</ta>
            <ta e="T32" id="Seg_6780" s="T31">tam</ta>
            <ta e="T33" id="Seg_6781" s="T32">təttɨ-n</ta>
            <ta e="T34" id="Seg_6782" s="T33">pontar-qɨn</ta>
            <ta e="T35" id="Seg_6783" s="T34">qum-ɨ-m</ta>
            <ta e="T36" id="Seg_6784" s="T35">ašša</ta>
            <ta e="T37" id="Seg_6785" s="T36">tɛnɨmɨ</ta>
            <ta e="T38" id="Seg_6786" s="T37">ama-tɨ</ta>
            <ta e="T39" id="Seg_6787" s="T38">tom-ŋɨ-tɨ</ta>
            <ta e="T40" id="Seg_6788" s="T39">man</ta>
            <ta e="T41" id="Seg_6789" s="T40">nʼi</ta>
            <ta e="T42" id="Seg_6790" s="T41">qaj-lʼ</ta>
            <ta e="T43" id="Seg_6791" s="T42">qum</ta>
            <ta e="T44" id="Seg_6792" s="T43">ašša</ta>
            <ta e="T45" id="Seg_6793" s="T44">tɛnɨmɨ</ta>
            <ta e="T46" id="Seg_6794" s="T45">tam</ta>
            <ta e="T47" id="Seg_6795" s="T46">təttɨ-n</ta>
            <ta e="T48" id="Seg_6796" s="T47">pontar-qɨn</ta>
            <ta e="T49" id="Seg_6797" s="T48">təttɨčʼa-qɨn</ta>
            <ta e="T50" id="Seg_6798" s="T49">qum</ta>
            <ta e="T51" id="Seg_6799" s="T50">čʼäːŋkɨ</ta>
            <ta e="T52" id="Seg_6800" s="T51">iːja-tɨ</ta>
            <ta e="T53" id="Seg_6801" s="T52">ama-ntɨ-naj</ta>
            <ta e="T54" id="Seg_6802" s="T53">nılʼčʼɨ-lʼ</ta>
            <ta e="T55" id="Seg_6803" s="T54">kətɨ-tɨ</ta>
            <ta e="T56" id="Seg_6804" s="T55">tan</ta>
            <ta e="T57" id="Seg_6805" s="T56">mompa</ta>
            <ta e="T58" id="Seg_6806" s="T57">onäntɨ</ta>
            <ta e="T59" id="Seg_6807" s="T58">ilɨ-äšɨk</ta>
            <ta e="T60" id="Seg_6808" s="T59">man</ta>
            <ta e="T61" id="Seg_6809" s="T60">ompä</ta>
            <ta e="T62" id="Seg_6810" s="T61">qum-ɨ-lʼ</ta>
            <ta e="T63" id="Seg_6811" s="T62">peː-r-ɨ-lä</ta>
            <ta e="T64" id="Seg_6812" s="T63">qən-lä-k</ta>
            <ta e="T65" id="Seg_6813" s="T64">tam</ta>
            <ta e="T66" id="Seg_6814" s="T65">təttɨ-n</ta>
            <ta e="T67" id="Seg_6815" s="T66">pontar-qɨn</ta>
            <ta e="T68" id="Seg_6816" s="T67">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T69" id="Seg_6817" s="T68">kəš</ta>
            <ta e="T70" id="Seg_6818" s="T69">nɨːnɨ</ta>
            <ta e="T71" id="Seg_6819" s="T70">qən-ŋɨ</ta>
            <ta e="T72" id="Seg_6820" s="T71">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T73" id="Seg_6821" s="T72">kəš</ta>
            <ta e="T74" id="Seg_6822" s="T73">naššak</ta>
            <ta e="T75" id="Seg_6823" s="T74">qən-ntɨ-ŋɨ</ta>
            <ta e="T76" id="Seg_6824" s="T75">kə-k-kkɨ</ta>
            <ta e="T77" id="Seg_6825" s="T76">aj</ta>
            <ta e="T78" id="Seg_6826" s="T77">taŋɨ-m-kkɨ</ta>
            <ta e="T79" id="Seg_6827" s="T78">qoltɨ-m</ta>
            <ta e="T80" id="Seg_6828" s="T79">kɨ</ta>
            <ta e="T81" id="Seg_6829" s="T80">tulɨ-ntɨ-mpɨ-kkɨ</ta>
            <ta e="T82" id="Seg_6830" s="T81">kɨpa</ta>
            <ta e="T83" id="Seg_6831" s="T82">kɨ</ta>
            <ta e="T84" id="Seg_6832" s="T83">tulɨ-ntɨ-mpɨ-kkɨ</ta>
            <ta e="T85" id="Seg_6833" s="T84">nʼikakoj</ta>
            <ta e="T86" id="Seg_6834" s="T85">nʼi</ta>
            <ta e="T87" id="Seg_6835" s="T86">qaj-lʼ</ta>
            <ta e="T88" id="Seg_6836" s="T87">qum</ta>
            <ta e="T89" id="Seg_6837" s="T88">čʼäːŋkɨ</ta>
            <ta e="T90" id="Seg_6838" s="T89">to</ta>
            <ta e="T91" id="Seg_6839" s="T90">toː-lʼ</ta>
            <ta e="T92" id="Seg_6840" s="T91">mɨ-qɨn</ta>
            <ta e="T93" id="Seg_6841" s="T92">kɨ-k</ta>
            <ta e="T94" id="Seg_6842" s="T93">qən-kkɨ</ta>
            <ta e="T95" id="Seg_6843" s="T94">ükɨ-ntoːqo</ta>
            <ta e="T96" id="Seg_6844" s="T95">qɔːt-qɨn</ta>
            <ta e="T97" id="Seg_6845" s="T96">sɨrɨ-š-ntɨ-kkɨ</ta>
            <ta e="T98" id="Seg_6846" s="T97">aj</ta>
            <ta e="T99" id="Seg_6847" s="T98">čʼüː-tqo</ta>
            <ta e="T100" id="Seg_6848" s="T99">ukkɨr</ta>
            <ta e="T101" id="Seg_6849" s="T100">čʼontɨ-qɨn</ta>
            <ta e="T102" id="Seg_6850" s="T101">tulɨ-š-ŋɨ</ta>
            <ta e="T103" id="Seg_6851" s="T102">mačʼɨ-ntɨ</ta>
            <ta e="T104" id="Seg_6852" s="T103">wərqɨ-lʼ</ta>
            <ta e="T105" id="Seg_6853" s="T104">mačʼɨ</ta>
            <ta e="T106" id="Seg_6854" s="T105">na</ta>
            <ta e="T107" id="Seg_6855" s="T106">mačʼɨ-mɨn</ta>
            <ta e="T108" id="Seg_6856" s="T107">kɨ</ta>
            <ta e="T109" id="Seg_6857" s="T108">tantɨ-mpɨ-ntɨ</ta>
            <ta e="T110" id="Seg_6858" s="T109">na</ta>
            <ta e="T111" id="Seg_6859" s="T110">kɨ-n</ta>
            <ta e="T112" id="Seg_6860" s="T111">qanɨŋ-mɨn</ta>
            <ta e="T113" id="Seg_6861" s="T112">qäl-mpɨ-k</ta>
            <ta e="T114" id="Seg_6862" s="T113">ukkɨr</ta>
            <ta e="T115" id="Seg_6863" s="T114">čʼontɨ-qɨn</ta>
            <ta e="T116" id="Seg_6864" s="T115">kos</ta>
            <ta e="T117" id="Seg_6865" s="T116">qaj</ta>
            <ta e="T118" id="Seg_6866" s="T117">üntɨ-ntɨ</ta>
            <ta e="T119" id="Seg_6867" s="T118">a</ta>
            <ta e="T120" id="Seg_6868" s="T119">buh</ta>
            <ta e="T121" id="Seg_6869" s="T120">a</ta>
            <ta e="T122" id="Seg_6870" s="T121">buh</ta>
            <ta e="T123" id="Seg_6871" s="T122">qäl-mpɨ</ta>
            <ta e="T124" id="Seg_6872" s="T123">nılʼčʼɨ-lʼ</ta>
            <ta e="T125" id="Seg_6873" s="T124">čʼarɨ</ta>
            <ta e="T126" id="Seg_6874" s="T125">üntɨ-ŋɨ-tɨ</ta>
            <ta e="T127" id="Seg_6875" s="T126">nʼi</ta>
            <ta e="T128" id="Seg_6876" s="T127">qaj</ta>
            <ta e="T129" id="Seg_6877" s="T128">suːrɨm</ta>
            <ta e="T130" id="Seg_6878" s="T129">nʼi</ta>
            <ta e="T131" id="Seg_6879" s="T130">qaj</ta>
            <ta e="T132" id="Seg_6880" s="T131">čʼäːŋkɨ</ta>
            <ta e="T133" id="Seg_6881" s="T132">na</ta>
            <ta e="T134" id="Seg_6882" s="T133">təttɨ-m</ta>
            <ta e="T135" id="Seg_6883" s="T134">pontar</ta>
            <ta e="T136" id="Seg_6884" s="T135">kolʼɨ-altɨ-ptäː-qäk</ta>
            <ta e="T137" id="Seg_6885" s="T136">kɨssa</ta>
            <ta e="T138" id="Seg_6886" s="T137">qaj</ta>
            <ta e="T139" id="Seg_6887" s="T138">orɨ-š-ntɨ</ta>
            <ta e="T140" id="Seg_6888" s="T139">man</ta>
            <ta e="T141" id="Seg_6889" s="T140">hotʼ</ta>
            <ta e="T142" id="Seg_6890" s="T141">mantɨ-mpɨ-sɨ-k</ta>
            <ta e="T143" id="Seg_6891" s="T142">ɛnä</ta>
            <ta e="T144" id="Seg_6892" s="T143">na</ta>
            <ta e="T145" id="Seg_6893" s="T144">tü-ntɨ-ntɨ</ta>
            <ta e="T146" id="Seg_6894" s="T145">na</ta>
            <ta e="T147" id="Seg_6895" s="T146">tü-ntɨ-ntɨ</ta>
            <ta e="T148" id="Seg_6896" s="T147">ɨːrɨŋ</ta>
            <ta e="T149" id="Seg_6897" s="T148">mɨta</ta>
            <ta e="T150" id="Seg_6898" s="T149">olä</ta>
            <ta e="T151" id="Seg_6899" s="T150">kɨ</ta>
            <ta e="T152" id="Seg_6900" s="T151">šünʼčʼɨ-qɨn</ta>
            <ta e="T153" id="Seg_6901" s="T152">takkɨ-n</ta>
            <ta e="T154" id="Seg_6902" s="T153">üntɨ</ta>
            <ta e="T155" id="Seg_6903" s="T154">aj</ta>
            <ta e="T156" id="Seg_6904" s="T155">ket</ta>
            <ta e="T157" id="Seg_6905" s="T156">na</ta>
            <ta e="T158" id="Seg_6906" s="T157">laŋkɨ-š-ntɨ-ntɨ</ta>
            <ta e="T159" id="Seg_6907" s="T158">a</ta>
            <ta e="T160" id="Seg_6908" s="T159">buh</ta>
            <ta e="T161" id="Seg_6909" s="T160">a</ta>
            <ta e="T162" id="Seg_6910" s="T161">buh</ta>
            <ta e="T163" id="Seg_6911" s="T162">nılʼčʼɨ</ta>
            <ta e="T164" id="Seg_6912" s="T163">čʼarɨ-tɨ</ta>
            <ta e="T165" id="Seg_6913" s="T164">qaj-lʼ</ta>
            <ta e="T166" id="Seg_6914" s="T165">montɨ</ta>
            <ta e="T167" id="Seg_6915" s="T166">tɛːŋŋɨ-r-ŋɨ</ta>
            <ta e="T168" id="Seg_6916" s="T167">na</ta>
            <ta e="T169" id="Seg_6917" s="T168">tü-ntɨ-ŋɨ</ta>
            <ta e="T170" id="Seg_6918" s="T169">takkɨ-n</ta>
            <ta e="T171" id="Seg_6919" s="T170">kɨ-n</ta>
            <ta e="T172" id="Seg_6920" s="T171">kɨ-qɨn</ta>
            <ta e="T173" id="Seg_6921" s="T172">kos</ta>
            <ta e="T174" id="Seg_6922" s="T173">qaj</ta>
            <ta e="T175" id="Seg_6923" s="T174">säːqɨ-ɨ-mɔːt</ta>
            <ta e="T176" id="Seg_6924" s="T175">na</ta>
            <ta e="T177" id="Seg_6925" s="T176">mənɨlʼ</ta>
            <ta e="T178" id="Seg_6926" s="T177">laŋkɨ-ŋɨ</ta>
            <ta e="T179" id="Seg_6927" s="T178">a</ta>
            <ta e="T180" id="Seg_6928" s="T179">buh</ta>
            <ta e="T181" id="Seg_6929" s="T180">a</ta>
            <ta e="T182" id="Seg_6930" s="T181">buh</ta>
            <ta e="T183" id="Seg_6931" s="T182">ašša</ta>
            <ta e="T184" id="Seg_6932" s="T183">nʼi</ta>
            <ta e="T185" id="Seg_6933" s="T184">qum-ɨ-n</ta>
            <ta e="T186" id="Seg_6934" s="T185">čʼarɨ</ta>
            <ta e="T187" id="Seg_6935" s="T186">olä</ta>
            <ta e="T188" id="Seg_6936" s="T187">nık</ta>
            <ta e="T189" id="Seg_6937" s="T188">laŋkɨ-š-ŋɨ</ta>
            <ta e="T190" id="Seg_6938" s="T189">nɨː</ta>
            <ta e="T191" id="Seg_6939" s="T190">na</ta>
            <ta e="T192" id="Seg_6940" s="T191">tü-ntɨ</ta>
            <ta e="T193" id="Seg_6941" s="T192">montɨ</ta>
            <ta e="T194" id="Seg_6942" s="T193">nılʼčʼɨ-k</ta>
            <ta e="T195" id="Seg_6943" s="T194">qum</ta>
            <ta e="T196" id="Seg_6944" s="T195">laŋkɨ-sɨ</ta>
            <ta e="T197" id="Seg_6945" s="T196">nɨː</ta>
            <ta e="T198" id="Seg_6946" s="T197">čʼam</ta>
            <ta e="T199" id="Seg_6947" s="T198">tü-ntɨ</ta>
            <ta e="T200" id="Seg_6948" s="T199">mantɨ-lä</ta>
            <ta e="T201" id="Seg_6949" s="T200">qum</ta>
            <ta e="T202" id="Seg_6950" s="T201">montɨ</ta>
            <ta e="T203" id="Seg_6951" s="T202">qum</ta>
            <ta e="T204" id="Seg_6952" s="T203">tam</ta>
            <ta e="T205" id="Seg_6953" s="T204">əsɨ-n-tɨ</ta>
            <ta e="T206" id="Seg_6954" s="T205">ama-n-tɨ</ta>
            <ta e="T207" id="Seg_6955" s="T206">wəčʼɨ-sä</ta>
            <ta e="T208" id="Seg_6956" s="T207">nʼaŋɨčʼa</ta>
            <ta e="T209" id="Seg_6957" s="T208">qum</ta>
            <ta e="T210" id="Seg_6958" s="T209">təp</ta>
            <ta e="T211" id="Seg_6959" s="T210">əsɨ-n-tɨ</ta>
            <ta e="T212" id="Seg_6960" s="T211">ama-n-tɨ</ta>
            <ta e="T213" id="Seg_6961" s="T212">wəčʼɨ-sä</ta>
            <ta e="T214" id="Seg_6962" s="T213">nʼaŋɨčʼa</ta>
            <ta e="T215" id="Seg_6963" s="T214">ɔːmtɨ</ta>
            <ta e="T216" id="Seg_6964" s="T215">na</ta>
            <ta e="T217" id="Seg_6965" s="T216">qəqqɨ-n</ta>
            <ta e="T218" id="Seg_6966" s="T217">ɔːŋ-qɨn</ta>
            <ta e="T219" id="Seg_6967" s="T218">na</ta>
            <ta e="T220" id="Seg_6968" s="T219">qaj</ta>
            <ta e="T221" id="Seg_6969" s="T220">na</ta>
            <ta e="T222" id="Seg_6970" s="T221">qum</ta>
            <ta e="T223" id="Seg_6971" s="T222">laŋkɨ-š-mpɨ-ntɨ</ta>
            <ta e="T224" id="Seg_6972" s="T223">tan</ta>
            <ta e="T225" id="Seg_6973" s="T224">qaː</ta>
            <ta e="T226" id="Seg_6974" s="T225">nılʼčʼɨ-lʼ</ta>
            <ta e="T227" id="Seg_6975" s="T226">ɔːmtɨ-ntɨ</ta>
            <ta e="T228" id="Seg_6976" s="T227">ašša</ta>
            <ta e="T229" id="Seg_6977" s="T228">mompa</ta>
            <ta e="T230" id="Seg_6978" s="T229">man</ta>
            <ta e="T231" id="Seg_6979" s="T230">lоːsɨ</ta>
            <ta e="T232" id="Seg_6980" s="T231">mašım</ta>
            <ta e="T233" id="Seg_6981" s="T232">am-qo</ta>
            <ta e="T234" id="Seg_6982" s="T233">ɔːmtɨ-k</ta>
            <ta e="T235" id="Seg_6983" s="T234">konnä-qɨn</ta>
            <ta e="T236" id="Seg_6984" s="T235">qum-iː-mɨ</ta>
            <ta e="T237" id="Seg_6985" s="T236">ɛː-ŋɨ-tɨt</ta>
            <ta e="T238" id="Seg_6986" s="T237">na-t</ta>
            <ta e="T239" id="Seg_6987" s="T238">mašım</ta>
            <ta e="T240" id="Seg_6988" s="T239">omtɨ-lɨ-mpɨ-tɨt</ta>
            <ta e="T241" id="Seg_6989" s="T240">lоːsɨ</ta>
            <ta e="T242" id="Seg_6990" s="T241">mašım</ta>
            <ta e="T243" id="Seg_6991" s="T242">am-qo</ta>
            <ta e="T244" id="Seg_6992" s="T243">olqa</ta>
            <ta e="T245" id="Seg_6993" s="T244">man</ta>
            <ta e="T246" id="Seg_6994" s="T245">konnä</ta>
            <ta e="T247" id="Seg_6995" s="T246">tantɨ-ɛntɨ-k</ta>
            <ta e="T248" id="Seg_6996" s="T247">lоːsɨ</ta>
            <ta e="T249" id="Seg_6997" s="T248">muntɨk</ta>
            <ta e="T250" id="Seg_6998" s="T249">meːšımɨt</ta>
            <ta e="T251" id="Seg_6999" s="T250">am-ɛntɨ</ta>
            <ta e="T252" id="Seg_7000" s="T251">nʼi</ta>
            <ta e="T253" id="Seg_7001" s="T252">qaj-m</ta>
            <ta e="T254" id="Seg_7002" s="T253">našša</ta>
            <ta e="T255" id="Seg_7003" s="T254">qalɨ-tɨ</ta>
            <ta e="T256" id="Seg_7004" s="T255">na</ta>
            <ta e="T257" id="Seg_7005" s="T256">qaː</ta>
            <ta e="T258" id="Seg_7006" s="T257">mašım</ta>
            <ta e="T259" id="Seg_7007" s="T258">omtɨ-lɨ-mpɨ-tɨt</ta>
            <ta e="T260" id="Seg_7008" s="T259">tan</ta>
            <ta e="T261" id="Seg_7009" s="T260">konnä</ta>
            <ta e="T262" id="Seg_7010" s="T261">tantɨ-äšɨk</ta>
            <ta e="T263" id="Seg_7011" s="T262">tan</ta>
            <ta e="T264" id="Seg_7012" s="T263">naj</ta>
            <ta e="T265" id="Seg_7013" s="T264">konnä</ta>
            <ta e="T266" id="Seg_7014" s="T265">tantɨ-äšɨk</ta>
            <ta e="T267" id="Seg_7015" s="T266">man</ta>
            <ta e="T268" id="Seg_7016" s="T267">kuttar</ta>
            <ta e="T269" id="Seg_7017" s="T268">tantɨ-ɛntɨ-k</ta>
            <ta e="T270" id="Seg_7018" s="T269">qum-ɨ-t</ta>
            <ta e="T271" id="Seg_7019" s="T270">mašım</ta>
            <ta e="T272" id="Seg_7020" s="T271">qo-lʼčʼɨ-ɛntɨ-tɨt</ta>
            <ta e="T273" id="Seg_7021" s="T272">man</ta>
            <ta e="T274" id="Seg_7022" s="T273">tom-ŋɨ-m</ta>
            <ta e="T275" id="Seg_7023" s="T274">tan</ta>
            <ta e="T276" id="Seg_7024" s="T275">konnä</ta>
            <ta e="T277" id="Seg_7025" s="T276">tantɨ-äšɨk</ta>
            <ta e="T278" id="Seg_7026" s="T277">konnä</ta>
            <ta e="T279" id="Seg_7027" s="T278">na</ta>
            <ta e="T280" id="Seg_7028" s="T279">tantɨ-ntɨ-qı</ta>
            <ta e="T281" id="Seg_7029" s="T280">Sɨlʼčʼa_Pɨlʼčʼa-n</ta>
            <ta e="T282" id="Seg_7030" s="T281">kəš</ta>
            <ta e="T283" id="Seg_7031" s="T282">pɔːrkä</ta>
            <ta e="T284" id="Seg_7032" s="T283">na</ta>
            <ta e="T285" id="Seg_7033" s="T284">meː-lʼčʼɨ-ntɨ-tɨ</ta>
            <ta e="T286" id="Seg_7034" s="T285">täːqa-ntɨ-sä</ta>
            <ta e="T287" id="Seg_7035" s="T286">tan</ta>
            <ta e="T288" id="Seg_7036" s="T287">na pa</ta>
            <ta e="T289" id="Seg_7037" s="T288">mašım</ta>
            <ta e="T290" id="Seg_7038" s="T289">kuššak</ta>
            <ta e="T291" id="Seg_7039" s="T290">ɨkɨ</ta>
            <ta e="T292" id="Seg_7040" s="T291">mašım</ta>
            <ta e="T293" id="Seg_7041" s="T292">kətɨ-äšɨk</ta>
            <ta e="T294" id="Seg_7042" s="T293">lоːsɨ</ta>
            <ta e="T295" id="Seg_7043" s="T294">tašıntɨ</ta>
            <ta e="T296" id="Seg_7044" s="T295">na</ta>
            <ta e="T297" id="Seg_7045" s="T296">soqɨš-ɛntɨ-ntɨ</ta>
            <ta e="T298" id="Seg_7046" s="T297">Sɨlʼčʼa_Pɨlʼčʼa-tɨ</ta>
            <ta e="T299" id="Seg_7047" s="T298">kəš</ta>
            <ta e="T300" id="Seg_7048" s="T299">kučʼčʼä</ta>
            <ta e="T301" id="Seg_7049" s="T300">qattɨ-sɨ</ta>
            <ta e="T302" id="Seg_7050" s="T301">mašım</ta>
            <ta e="T303" id="Seg_7051" s="T302">kuššak-naj</ta>
            <ta e="T304" id="Seg_7052" s="T303">mašım</ta>
            <ta e="T305" id="Seg_7053" s="T304">kətɨ-äšɨk</ta>
            <ta e="T306" id="Seg_7054" s="T305">nılʼčʼɨ-lʼ</ta>
            <ta e="T307" id="Seg_7055" s="T306">äːkɨlʼčʼi-mpɨ-tɨ</ta>
            <ta e="T308" id="Seg_7056" s="T307">täːqa-sä</ta>
            <ta e="T309" id="Seg_7057" s="T308">konnä</ta>
            <ta e="T310" id="Seg_7058" s="T309">na</ta>
            <ta e="T311" id="Seg_7059" s="T310">qən-ntɨ</ta>
            <ta e="T312" id="Seg_7060" s="T311">nɨː</ta>
            <ta e="T313" id="Seg_7061" s="T312">montɨ</ta>
            <ta e="T314" id="Seg_7062" s="T313">čʼu-lʼ</ta>
            <ta e="T315" id="Seg_7063" s="T314">mɔːt</ta>
            <ta e="T316" id="Seg_7064" s="T315">na-qɨn</ta>
            <ta e="T317" id="Seg_7065" s="T316">mɔːta-n</ta>
            <ta e="T318" id="Seg_7066" s="T317">ɔːŋ-qɨn</ta>
            <ta e="T319" id="Seg_7067" s="T318">aj</ta>
            <ta e="T320" id="Seg_7068" s="T319">pɔːrkä</ta>
            <ta e="T321" id="Seg_7069" s="T320">na</ta>
            <ta e="T322" id="Seg_7070" s="T321">meː-ntɨ-tɨ</ta>
            <ta e="T323" id="Seg_7071" s="T322">mɔːt</ta>
            <ta e="T324" id="Seg_7072" s="T323">šeːr-äšɨk</ta>
            <ta e="T325" id="Seg_7073" s="T324">qaː</ta>
            <ta e="T326" id="Seg_7074" s="T325">nɨŋ-ɨ-ntɨ</ta>
            <ta e="T327" id="Seg_7075" s="T326">qantɨ-ŋɨ-ntɨ</ta>
            <ta e="T328" id="Seg_7076" s="T327">man</ta>
            <ta e="T329" id="Seg_7077" s="T328">qäntɨk</ta>
            <ta e="T330" id="Seg_7078" s="T329">šeːr-ɛntɨ-k</ta>
            <ta e="T331" id="Seg_7079" s="T330">qum-iː-mɨ</ta>
            <ta e="T332" id="Seg_7080" s="T331">mašım</ta>
            <ta e="T333" id="Seg_7081" s="T332">qo-lʼčʼɨ-ɛntɨ-tɨt</ta>
            <ta e="T334" id="Seg_7082" s="T333">tan</ta>
            <ta e="T335" id="Seg_7083" s="T334">olä</ta>
            <ta e="T336" id="Seg_7084" s="T335">mɔːt</ta>
            <ta e="T337" id="Seg_7085" s="T336">šeːr-äšɨk</ta>
            <ta e="T338" id="Seg_7086" s="T337">na</ta>
            <ta e="T339" id="Seg_7087" s="T338">mɔːta-n</ta>
            <ta e="T340" id="Seg_7088" s="T339">ɔːŋ-qɨn</ta>
            <ta e="T341" id="Seg_7089" s="T340">aj</ta>
            <ta e="T342" id="Seg_7090" s="T341">pɔːrkä</ta>
            <ta e="T343" id="Seg_7091" s="T342">meː-ntɨ-tɨ</ta>
            <ta e="T344" id="Seg_7092" s="T343">tan</ta>
            <ta e="T345" id="Seg_7093" s="T344">na pa</ta>
            <ta e="T346" id="Seg_7094" s="T345">mašım</ta>
            <ta e="T347" id="Seg_7095" s="T346">ɨkɨ</ta>
            <ta e="T348" id="Seg_7096" s="T347">mašım</ta>
            <ta e="T349" id="Seg_7097" s="T348">kətɨ-äšɨk</ta>
            <ta e="T350" id="Seg_7098" s="T349">täːqa-sä</ta>
            <ta e="T351" id="Seg_7099" s="T350">äːkɨlʼčʼɨ-ŋɨ-tɨ</ta>
            <ta e="T352" id="Seg_7100" s="T351">mɔːt</ta>
            <ta e="T353" id="Seg_7101" s="T352">na</ta>
            <ta e="T354" id="Seg_7102" s="T353">šeːr</ta>
            <ta e="T355" id="Seg_7103" s="T354">tına</ta>
            <ta e="T356" id="Seg_7104" s="T355">qum</ta>
            <ta e="T357" id="Seg_7105" s="T356">əːtɨmɨn</ta>
            <ta e="T358" id="Seg_7106" s="T357">muntɨk</ta>
            <ta e="T359" id="Seg_7107" s="T358">na</ta>
            <ta e="T360" id="Seg_7108" s="T359">qo-lʼčʼɨ-ntɨ-tɨt</ta>
            <ta e="T361" id="Seg_7109" s="T360">naj</ta>
            <ta e="T362" id="Seg_7110" s="T361">mompa</ta>
            <ta e="T363" id="Seg_7111" s="T362">qaː</ta>
            <ta e="T364" id="Seg_7112" s="T363">šeːr-ŋɨ</ta>
            <ta e="T365" id="Seg_7113" s="T364">ašša</ta>
            <ta e="T366" id="Seg_7114" s="T365">mompa</ta>
            <ta e="T367" id="Seg_7115" s="T366">qum-tɨ</ta>
            <ta e="T368" id="Seg_7116" s="T367">mɔːt-ntɨ</ta>
            <ta e="T369" id="Seg_7117" s="T368">mašım</ta>
            <ta e="T370" id="Seg_7118" s="T369">üːtɨ-sɨ</ta>
            <ta e="T371" id="Seg_7119" s="T370">kos</ta>
            <ta e="T372" id="Seg_7120" s="T371">qaj-ɨ-lʼ</ta>
            <ta e="T373" id="Seg_7121" s="T372">qum</ta>
            <ta e="T374" id="Seg_7122" s="T373">tü-sɨ</ta>
            <ta e="T375" id="Seg_7123" s="T374">mašım</ta>
            <ta e="T376" id="Seg_7124" s="T375">mɔːt</ta>
            <ta e="T377" id="Seg_7125" s="T376">mašım</ta>
            <ta e="T378" id="Seg_7126" s="T377">üːtɨ-sɨ</ta>
            <ta e="T379" id="Seg_7127" s="T378">konnä</ta>
            <ta e="T380" id="Seg_7128" s="T379">mašım</ta>
            <ta e="T381" id="Seg_7129" s="T380">taːtɨ-altɨ-sɨ</ta>
            <ta e="T382" id="Seg_7130" s="T381">təp</ta>
            <ta e="T383" id="Seg_7131" s="T382">čʼam</ta>
            <ta e="T384" id="Seg_7132" s="T383">qo-ŋɨ-tɨ</ta>
            <ta e="T385" id="Seg_7133" s="T384">mɔːta-n</ta>
            <ta e="T386" id="Seg_7134" s="T385">nʼennä-lʼ</ta>
            <ta e="T387" id="Seg_7135" s="T386">pɛläk-qɨn</ta>
            <ta e="T388" id="Seg_7136" s="T387">aj</ta>
            <ta e="T389" id="Seg_7137" s="T388">ukkɨr</ta>
            <ta e="T390" id="Seg_7138" s="T389">nʼaŋɨčʼa</ta>
            <ta e="T391" id="Seg_7139" s="T390">qum</ta>
            <ta e="T392" id="Seg_7140" s="T391">qompɨ-ntɨ</ta>
            <ta e="T393" id="Seg_7141" s="T392">nɨːnɨ</ta>
            <ta e="T394" id="Seg_7142" s="T393">qaj</ta>
            <ta e="T395" id="Seg_7143" s="T394">nılʼčʼɨ-k</ta>
            <ta e="T396" id="Seg_7144" s="T395">ɛsɨ</ta>
            <ta e="T397" id="Seg_7145" s="T396">qum-ɨ-m</ta>
            <ta e="T398" id="Seg_7146" s="T397">ınnä</ta>
            <ta e="T399" id="Seg_7147" s="T398">*taqtɨ-altɨ-tɨt</ta>
            <ta e="T400" id="Seg_7148" s="T399">nɨːnɨ</ta>
            <ta e="T401" id="Seg_7149" s="T400">ponä</ta>
            <ta e="T402" id="Seg_7150" s="T401">tantɨ-lä</ta>
            <ta e="T403" id="Seg_7151" s="T402">üŋkɨl-tɨ-kkɨ-olʼ-ntɨ-mpɨ-tɨ</ta>
            <ta e="T404" id="Seg_7152" s="T403">lоːsɨ</ta>
            <ta e="T405" id="Seg_7153" s="T404">kutɨ-lʼ</ta>
            <ta e="T406" id="Seg_7154" s="T405">mɔːntɨ-qɨn</ta>
            <ta e="T407" id="Seg_7155" s="T406">qompɨ-š-kkɨ</ta>
            <ta e="T408" id="Seg_7156" s="T407">lоːsɨ</ta>
            <ta e="T409" id="Seg_7157" s="T408">qata</ta>
            <ta e="T410" id="Seg_7158" s="T409">qompɨ-š-ŋɨ</ta>
            <ta e="T411" id="Seg_7159" s="T410">mɔːt</ta>
            <ta e="T412" id="Seg_7160" s="T411">šeːr-ŋɨlɨt</ta>
            <ta e="T413" id="Seg_7161" s="T412">ukkɨr</ta>
            <ta e="T414" id="Seg_7162" s="T413">qum</ta>
            <ta e="T415" id="Seg_7163" s="T414">üŋkɨl-tɨ-mpɨ-ŋɨjä</ta>
            <ta e="T416" id="Seg_7164" s="T415">mompa</ta>
            <ta e="T417" id="Seg_7165" s="T416">üːtɨ-lʼ</ta>
            <ta e="T418" id="Seg_7166" s="T417">kəntɨ-tɨ</ta>
            <ta e="T419" id="Seg_7167" s="T418">*tokk-ɨ-š-tɨ-ntɨ</ta>
            <ta e="T420" id="Seg_7168" s="T419">naššan</ta>
            <ta e="T421" id="Seg_7169" s="T420">tantɨ-kkɨ-olʼ-ntɨ-mpɨ</ta>
            <ta e="T422" id="Seg_7170" s="T421">qompɨ-š-ntɨ-ntɨ</ta>
            <ta e="T423" id="Seg_7171" s="T422">seːpɨlaŋ</ta>
            <ta e="T424" id="Seg_7172" s="T423">ɛː-ntɨ</ta>
            <ta e="T425" id="Seg_7173" s="T424">ukkɨr</ta>
            <ta e="T426" id="Seg_7174" s="T425">tɔːt</ta>
            <ta e="T427" id="Seg_7175" s="T426">čʼontɨ-qɨn</ta>
            <ta e="T428" id="Seg_7176" s="T427">qum</ta>
            <ta e="T429" id="Seg_7177" s="T428">na</ta>
            <ta e="T430" id="Seg_7178" s="T429">tantɨ-ntɨ</ta>
            <ta e="T431" id="Seg_7179" s="T430">mompa</ta>
            <ta e="T432" id="Seg_7180" s="T431">qarɨ-n</ta>
            <ta e="T433" id="Seg_7181" s="T432">na</ta>
            <ta e="T434" id="Seg_7182" s="T433">qompɨ-š-ntɨ</ta>
            <ta e="T435" id="Seg_7183" s="T434">mɔːt-ntɨ</ta>
            <ta e="T436" id="Seg_7184" s="T435">pat-qɨl-ŋɨ</ta>
            <ta e="T437" id="Seg_7185" s="T436">lоːsɨ</ta>
            <ta e="T438" id="Seg_7186" s="T437">na</ta>
            <ta e="T439" id="Seg_7187" s="T438">qompɨ-š-ntɨ</ta>
            <ta e="T440" id="Seg_7188" s="T439">a</ta>
            <ta e="T441" id="Seg_7189" s="T440">mompa</ta>
            <ta e="T442" id="Seg_7190" s="T441">nɔːtɨ</ta>
            <ta e="T443" id="Seg_7191" s="T442">*taŋɨ-k</ta>
            <ta e="T444" id="Seg_7192" s="T443">ɔːmtɨ-ŋɨlɨt</ta>
            <ta e="T445" id="Seg_7193" s="T444">nʼi</ta>
            <ta e="T446" id="Seg_7194" s="T445">kuššat</ta>
            <ta e="T447" id="Seg_7195" s="T446">ɨkɨ</ta>
            <ta e="T448" id="Seg_7196" s="T447">tantɨ-ŋɨlɨt</ta>
            <ta e="T449" id="Seg_7197" s="T448">Sɨlʼčʼa_Pɨlʼčʼa-tɨ</ta>
            <ta e="T450" id="Seg_7198" s="T449">kəš</ta>
            <ta e="T451" id="Seg_7199" s="T450">üŋkɨl-tɨ-mpɨ-tɨ</ta>
            <ta e="T452" id="Seg_7200" s="T451">qaj</ta>
            <ta e="T453" id="Seg_7201" s="T452">konnä</ta>
            <ta e="T454" id="Seg_7202" s="T453">na</ta>
            <ta e="T455" id="Seg_7203" s="T454">tantɨ-ntɨ-ŋɨ</ta>
            <ta e="T456" id="Seg_7204" s="T455">tantɨ-ntɨ</ta>
            <ta e="T457" id="Seg_7205" s="T456">peːkä-m</ta>
            <ta e="T458" id="Seg_7206" s="T457">qo-lʼčʼɨ-tɨ</ta>
            <ta e="T459" id="Seg_7207" s="T458">peːkä</ta>
            <ta e="T460" id="Seg_7208" s="T459">čʼuk</ta>
            <ta e="T461" id="Seg_7209" s="T460">čʼuk</ta>
            <ta e="T462" id="Seg_7210" s="T461">qaj</ta>
            <ta e="T463" id="Seg_7211" s="T462">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T464" id="Seg_7212" s="T463">kəš</ta>
            <ta e="T465" id="Seg_7213" s="T464">qattɨ-sɨ</ta>
            <ta e="T466" id="Seg_7214" s="T465">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T467" id="Seg_7215" s="T466">kəš</ta>
            <ta e="T468" id="Seg_7216" s="T467">tıntä</ta>
            <ta e="T469" id="Seg_7217" s="T468">qən-sɨ</ta>
            <ta e="T470" id="Seg_7218" s="T469">konnä</ta>
            <ta e="T471" id="Seg_7219" s="T470">na</ta>
            <ta e="T472" id="Seg_7220" s="T471">tü-ntɨ</ta>
            <ta e="T473" id="Seg_7221" s="T472">pɔːrkä-m</ta>
            <ta e="T474" id="Seg_7222" s="T473">qo-ŋɨ-tɨ</ta>
            <ta e="T475" id="Seg_7223" s="T474">pɔːrkä</ta>
            <ta e="T476" id="Seg_7224" s="T475">kətɨ-tɨ</ta>
            <ta e="T477" id="Seg_7225" s="T476">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T478" id="Seg_7226" s="T477">kəš</ta>
            <ta e="T479" id="Seg_7227" s="T478">mɔːt-qɨn</ta>
            <ta e="T480" id="Seg_7228" s="T479">ɔːmtɨ</ta>
            <ta e="T481" id="Seg_7229" s="T480">lоːsɨ</ta>
            <ta e="T482" id="Seg_7230" s="T481">karrä</ta>
            <ta e="T483" id="Seg_7231" s="T482">*kolʼɨ-mɔːt-lä</ta>
            <ta e="T484" id="Seg_7232" s="T483">*kurɨ-alʼ-ŋɨ</ta>
            <ta e="T485" id="Seg_7233" s="T484">qən-ŋɨ</ta>
            <ta e="T486" id="Seg_7234" s="T485">üt-ntɨ</ta>
            <ta e="T487" id="Seg_7235" s="T486">alʼčʼɨ</ta>
            <ta e="T488" id="Seg_7236" s="T487">mɨta</ta>
            <ta e="T489" id="Seg_7237" s="T488">qur</ta>
            <ta e="T490" id="Seg_7238" s="T489">qäːš</ta>
            <ta e="T491" id="Seg_7239" s="T490">ponä</ta>
            <ta e="T492" id="Seg_7240" s="T491">paktɨ-lä</ta>
            <ta e="T493" id="Seg_7241" s="T492">tına</ta>
            <ta e="T494" id="Seg_7242" s="T493">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T495" id="Seg_7243" s="T494">kəš</ta>
            <ta e="T496" id="Seg_7244" s="T495">pɔːrkä-m-tɨ</ta>
            <ta e="T497" id="Seg_7245" s="T496">pačʼčʼɨ-š-lä</ta>
            <ta e="T498" id="Seg_7246" s="T497">sılʼɨ-altɨ-tɨ</ta>
            <ta e="T499" id="Seg_7247" s="T498">mačʼä</ta>
            <ta e="T500" id="Seg_7248" s="T499">qättɨ-ɔːl-ŋɨ-tɨ</ta>
            <ta e="T501" id="Seg_7249" s="T500">jarɨk</ta>
            <ta e="T502" id="Seg_7250" s="T501">pɔːrkä-m</ta>
            <ta e="T503" id="Seg_7251" s="T502">meː-ŋɨ-tɨ</ta>
            <ta e="T504" id="Seg_7252" s="T503">čʼarrä</ta>
            <ta e="T505" id="Seg_7253" s="T504">äːkɨlʼčʼɨ-mpɨ-ŋɨ-tɨ</ta>
            <ta e="T506" id="Seg_7254" s="T505">tan</ta>
            <ta e="T507" id="Seg_7255" s="T506">na pa</ta>
            <ta e="T508" id="Seg_7256" s="T507">mašım</ta>
            <ta e="T509" id="Seg_7257" s="T508">nʼi</ta>
            <ta e="T510" id="Seg_7258" s="T509">kuššat</ta>
            <ta e="T511" id="Seg_7259" s="T510">kuššak-nɨ</ta>
            <ta e="T512" id="Seg_7260" s="T511">ɨkɨ</ta>
            <ta e="T513" id="Seg_7261" s="T512">kətɨ-äšɨk</ta>
            <ta e="T514" id="Seg_7262" s="T513">tiː</ta>
            <ta e="T515" id="Seg_7263" s="T514">tantɨ-kkɨ-olʼ-ntɨ-mpɨ</ta>
            <ta e="T516" id="Seg_7264" s="T515">qarɨ-n-ɨ-lʼ</ta>
            <ta e="T517" id="Seg_7265" s="T516">pi-n</ta>
            <ta e="T518" id="Seg_7266" s="T517">nɨːnɨ</ta>
            <ta e="T519" id="Seg_7267" s="T518">pi-n-tɨ</ta>
            <ta e="T520" id="Seg_7268" s="T519">kuntɨ</ta>
            <ta e="T521" id="Seg_7269" s="T520">na</ta>
            <ta e="T522" id="Seg_7270" s="T521">ɔːmtɨ-ntɨ-tɨt</ta>
            <ta e="T523" id="Seg_7271" s="T522">qarɨ-n-ɨ-lʼ</ta>
            <ta e="T524" id="Seg_7272" s="T523">pi-n</ta>
            <ta e="T525" id="Seg_7273" s="T524">qum-ɨ-m</ta>
            <ta e="T526" id="Seg_7274" s="T525">*kurɨ-altɨ-tɨt</ta>
            <ta e="T527" id="Seg_7275" s="T526">ponä</ta>
            <ta e="T528" id="Seg_7276" s="T527">mantɨ-mpɨ-tɨt</ta>
            <ta e="T529" id="Seg_7277" s="T528">čʼɔːlsä</ta>
            <ta e="T530" id="Seg_7278" s="T529">ašša</ta>
            <ta e="T531" id="Seg_7279" s="T530">tantɨ</ta>
            <ta e="T532" id="Seg_7280" s="T531">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T533" id="Seg_7281" s="T532">kəš</ta>
            <ta e="T534" id="Seg_7282" s="T533">*kurɨ-altɨ-tɨt</ta>
            <ta e="T535" id="Seg_7283" s="T534">üŋkɨl-tɨ-mpɨ-ätɨ</ta>
            <ta e="T536" id="Seg_7284" s="T535">seːpɨlaŋ</ta>
            <ta e="T537" id="Seg_7285" s="T536">qum</ta>
            <ta e="T538" id="Seg_7286" s="T537">na</ta>
            <ta e="T539" id="Seg_7287" s="T538">*lʼamɨ-k</ta>
            <ta e="T540" id="Seg_7288" s="T539">ɛː-ntɨ</ta>
            <ta e="T541" id="Seg_7289" s="T540">mɔːt-ntɨ</ta>
            <ta e="T542" id="Seg_7290" s="T541">na</ta>
            <ta e="T543" id="Seg_7291" s="T542">alʼčʼɨ-ntɨ</ta>
            <ta e="T544" id="Seg_7292" s="T543">mɨta</ta>
            <ta e="T545" id="Seg_7293" s="T544">ukkɨr</ta>
            <ta e="T546" id="Seg_7294" s="T545">lоːsɨ</ta>
            <ta e="T547" id="Seg_7295" s="T546">qompɨ-š-mpɨ</ta>
            <ta e="T548" id="Seg_7296" s="T547">lоːsɨ</ta>
            <ta e="T549" id="Seg_7297" s="T548">na</ta>
            <ta e="T550" id="Seg_7298" s="T549">qompɨ-š-mpɨ</ta>
            <ta e="T551" id="Seg_7299" s="T550">qapı</ta>
            <ta e="T552" id="Seg_7300" s="T551">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T553" id="Seg_7301" s="T552">ontɨ</ta>
            <ta e="T554" id="Seg_7302" s="T553">üŋkɨl-tɨ-mpɨ-tɨ</ta>
            <ta e="T555" id="Seg_7303" s="T554">na</ta>
            <ta e="T556" id="Seg_7304" s="T555">tantɨ-ntɨ</ta>
            <ta e="T557" id="Seg_7305" s="T556">konnä</ta>
            <ta e="T558" id="Seg_7306" s="T557">*kurɨ-lä</ta>
            <ta e="T559" id="Seg_7307" s="T558">təp</ta>
            <ta e="T560" id="Seg_7308" s="T559">tü-kkɨ-ŋɨ</ta>
            <ta e="T561" id="Seg_7309" s="T560">*kurɨ-lä</ta>
            <ta e="T562" id="Seg_7310" s="T561">təp</ta>
            <ta e="T563" id="Seg_7311" s="T562">tü-kkɨ-ŋɨ</ta>
            <ta e="T564" id="Seg_7312" s="T563">na</ta>
            <ta e="T565" id="Seg_7313" s="T564">qə-n</ta>
            <ta e="T566" id="Seg_7314" s="T565">pɔːrɨ-ɨ-ntɨ</ta>
            <ta e="T567" id="Seg_7315" s="T566">na</ta>
            <ta e="T568" id="Seg_7316" s="T567">tantɨ-ntɨ</ta>
            <ta e="T569" id="Seg_7317" s="T568">tına</ta>
            <ta e="T570" id="Seg_7318" s="T569">pɔːrkä-ntɨ</ta>
            <ta e="T571" id="Seg_7319" s="T570">*kurɨ-lä</ta>
            <ta e="T572" id="Seg_7320" s="T571">na</ta>
            <ta e="T573" id="Seg_7321" s="T572">tü-ntɨ</ta>
            <ta e="T574" id="Seg_7322" s="T573">mɨta</ta>
            <ta e="T575" id="Seg_7323" s="T574">čʼuk</ta>
            <ta e="T576" id="Seg_7324" s="T575">čʼuk</ta>
            <ta e="T577" id="Seg_7325" s="T576">čʼuk</ta>
            <ta e="T578" id="Seg_7326" s="T577">qaj</ta>
            <ta e="T579" id="Seg_7327" s="T578">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T580" id="Seg_7328" s="T579">kəš</ta>
            <ta e="T581" id="Seg_7329" s="T580">qattɨ-ŋɨ</ta>
            <ta e="T582" id="Seg_7330" s="T581">qaj</ta>
            <ta e="T583" id="Seg_7331" s="T582">mɔːt-qɨn</ta>
            <ta e="T584" id="Seg_7332" s="T583">ɛː-ŋɨ</ta>
            <ta e="T585" id="Seg_7333" s="T584">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T586" id="Seg_7334" s="T585">kəš</ta>
            <ta e="T587" id="Seg_7335" s="T586">naššan</ta>
            <ta e="T588" id="Seg_7336" s="T587">qən-sɨ</ta>
            <ta e="T589" id="Seg_7337" s="T588">tan</ta>
            <ta e="T590" id="Seg_7338" s="T589">konnä</ta>
            <ta e="T591" id="Seg_7339" s="T590">qən-lä</ta>
            <ta e="T592" id="Seg_7340" s="T591">apsɨ-lʼ</ta>
            <ta e="T593" id="Seg_7341" s="T592">am-ätɨ</ta>
            <ta e="T594" id="Seg_7342" s="T593">toːnna</ta>
            <ta e="T595" id="Seg_7343" s="T594">*kurɨ-lä</ta>
            <ta e="T596" id="Seg_7344" s="T595">na</ta>
            <ta e="T597" id="Seg_7345" s="T596">qən-ntɨ-naj</ta>
            <ta e="T598" id="Seg_7346" s="T597">mɔːta-n</ta>
            <ta e="T599" id="Seg_7347" s="T598">ɔːŋ-ɨ-lʼ</ta>
            <ta e="T600" id="Seg_7348" s="T599">tü-ntɨ-naj</ta>
            <ta e="T601" id="Seg_7349" s="T600">pɔːrkä-ntɨ</ta>
            <ta e="T602" id="Seg_7350" s="T601">tü-ŋɨ</ta>
            <ta e="T603" id="Seg_7351" s="T602">qattɨ-sɨ</ta>
            <ta e="T604" id="Seg_7352" s="T603">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T605" id="Seg_7353" s="T604">kəš</ta>
            <ta e="T606" id="Seg_7354" s="T605">təp</ta>
            <ta e="T607" id="Seg_7355" s="T606">aš</ta>
            <ta e="T608" id="Seg_7356" s="T607">täːlɨ</ta>
            <ta e="T609" id="Seg_7357" s="T608">naššan</ta>
            <ta e="T610" id="Seg_7358" s="T609">qən-sɨ</ta>
            <ta e="T611" id="Seg_7359" s="T610">qən-š-ɨ-ŋɨ</ta>
            <ta e="T612" id="Seg_7360" s="T611">mɔːt-ntɨ</ta>
            <ta e="T613" id="Seg_7361" s="T612">šeːr-lä</ta>
            <ta e="T614" id="Seg_7362" s="T613">apsɨ-lʼ</ta>
            <ta e="T615" id="Seg_7363" s="T614">am-ätɨ</ta>
            <ta e="T616" id="Seg_7364" s="T615">ukkɨr</ta>
            <ta e="T617" id="Seg_7365" s="T616">čʼontɨ-qɨn</ta>
            <ta e="T618" id="Seg_7366" s="T617">lоːsɨ</ta>
            <ta e="T619" id="Seg_7367" s="T618">mɔːt-ntɨ</ta>
            <ta e="T620" id="Seg_7368" s="T619">na</ta>
            <ta e="T621" id="Seg_7369" s="T620">noqqo-ɔːl-ntɨ</ta>
            <ta e="T622" id="Seg_7370" s="T621">mɔːt-ntɨ</ta>
            <ta e="T623" id="Seg_7371" s="T622">na</ta>
            <ta e="T624" id="Seg_7372" s="T623">noqqo-ɔːl-ntɨ</ta>
            <ta e="T625" id="Seg_7373" s="T624">mɔːt-ntɨ</ta>
            <ta e="T626" id="Seg_7374" s="T625">čʼam</ta>
            <ta e="T627" id="Seg_7375" s="T626">šeːr-ŋɨ</ta>
            <ta e="T628" id="Seg_7376" s="T627">lоːsɨ</ta>
            <ta e="T629" id="Seg_7377" s="T628">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T630" id="Seg_7378" s="T629">kəš</ta>
            <ta e="T631" id="Seg_7379" s="T630">pačʼčʼɨ-ɔːl-ŋɨ-tɨ</ta>
            <ta e="T632" id="Seg_7380" s="T631">olɨ-n-tɨ</ta>
            <ta e="T633" id="Seg_7381" s="T632">laka</ta>
            <ta e="T634" id="Seg_7382" s="T633">mɔːt-ntɨ</ta>
            <ta e="T635" id="Seg_7383" s="T634">šüː</ta>
            <ta e="T636" id="Seg_7384" s="T635">nʼennä</ta>
            <ta e="T637" id="Seg_7385" s="T636">püŋkɨ-olʼ-ŋɨ</ta>
            <ta e="T638" id="Seg_7386" s="T637">kəpɨ-n-tɨ</ta>
            <ta e="T639" id="Seg_7387" s="T638">laka</ta>
            <ta e="T640" id="Seg_7388" s="T639">qottä</ta>
            <ta e="T641" id="Seg_7389" s="T640">ponä</ta>
            <ta e="T642" id="Seg_7390" s="T641">alʼčʼɨ</ta>
            <ta e="T643" id="Seg_7391" s="T642">qət-tɨ-ntɨ-tɨ</ta>
            <ta e="T644" id="Seg_7392" s="T643">ponä</ta>
            <ta e="T645" id="Seg_7393" s="T644">taːtɨ-ŋɨ-tɨ</ta>
            <ta e="T646" id="Seg_7394" s="T645">to</ta>
            <ta e="T647" id="Seg_7395" s="T646">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T648" id="Seg_7396" s="T647">kəš</ta>
            <ta e="T649" id="Seg_7397" s="T648">kəpɨ-n-tɨ</ta>
            <ta e="T650" id="Seg_7398" s="T649">laka</ta>
            <ta e="T651" id="Seg_7399" s="T650">ponä</ta>
            <ta e="T652" id="Seg_7400" s="T651">tantɨ</ta>
            <ta e="T653" id="Seg_7401" s="T652">montɨ</ta>
            <ta e="T654" id="Seg_7402" s="T653">čʼeːlɨ-k-ɛː</ta>
            <ta e="T655" id="Seg_7403" s="T654">karrä</ta>
            <ta e="T656" id="Seg_7404" s="T655">*tul-tɨ-ŋɨ-tɨ</ta>
            <ta e="T657" id="Seg_7405" s="T656">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T658" id="Seg_7406" s="T657">kəš</ta>
            <ta e="T659" id="Seg_7407" s="T658">kəpɨ-n-tɨ</ta>
            <ta e="T660" id="Seg_7408" s="T659">laka-m</ta>
            <ta e="T661" id="Seg_7409" s="T660">üt-ɨ-lʼ</ta>
            <ta e="T662" id="Seg_7410" s="T661">qəqqɨ-ntɨ</ta>
            <ta e="T663" id="Seg_7411" s="T662">pin-ŋɨ-tɨ</ta>
            <ta e="T664" id="Seg_7412" s="T663">ulqa-sä</ta>
            <ta e="T665" id="Seg_7413" s="T664">nʼentɨ</ta>
            <ta e="T666" id="Seg_7414" s="T665">qantɨ-äptɨ-tɨ</ta>
            <ta e="T667" id="Seg_7415" s="T666">nılʼčʼɨ-k</ta>
            <ta e="T668" id="Seg_7416" s="T667">pin-ŋɨ-tɨ</ta>
            <ta e="T669" id="Seg_7417" s="T668">üt-ɨ-lʼ</ta>
            <ta e="T670" id="Seg_7418" s="T669">qəqqɨ-n-tɨ</ta>
            <ta e="T671" id="Seg_7419" s="T670">qan-ntɨ</ta>
            <ta e="T672" id="Seg_7420" s="T671">nılʼčʼɨ-k</ta>
            <ta e="T673" id="Seg_7421" s="T672">pin-ŋɨ-tɨ</ta>
            <ta e="T674" id="Seg_7422" s="T673">mitɨ</ta>
            <ta e="T675" id="Seg_7423" s="T674">olä</ta>
            <ta e="T676" id="Seg_7424" s="T675">ilɨ-lä</ta>
            <ta e="T677" id="Seg_7425" s="T676">ippɨ</ta>
            <ta e="T678" id="Seg_7426" s="T677">olɨ-ntɨ-sä</ta>
            <ta e="T679" id="Seg_7427" s="T678">nʼentɨ</ta>
            <ta e="T680" id="Seg_7428" s="T679">loːq-altɨ-ŋɨ-tɨ</ta>
            <ta e="T681" id="Seg_7429" s="T680">na</ta>
            <ta e="T682" id="Seg_7430" s="T681">čʼeːlɨ-tɨ-ŋɨ-mɨt</ta>
            <ta e="T683" id="Seg_7431" s="T682">*lɨpɨ-k-ɨ-mɔːt</ta>
            <ta e="T684" id="Seg_7432" s="T683">mompa</ta>
            <ta e="T685" id="Seg_7433" s="T684">kekkɨsä</ta>
            <ta e="T686" id="Seg_7434" s="T685">šittɨ-qı-lʼ</ta>
            <ta e="T687" id="Seg_7435" s="T686">jarɨk</ta>
            <ta e="T688" id="Seg_7436" s="T687">mɨ</ta>
            <ta e="T689" id="Seg_7437" s="T688">ičʼčʼɨ-qo</ta>
            <ta e="T690" id="Seg_7438" s="T689">ašša</ta>
            <ta e="T691" id="Seg_7439" s="T690">tɛnɨmɨ-mɨt</ta>
            <ta e="T692" id="Seg_7440" s="T691">üːtɨ-n</ta>
            <ta e="T693" id="Seg_7441" s="T692">na</ta>
            <ta e="T694" id="Seg_7442" s="T693">*lɨpɨ-k-ɨ-mɔːt-ŋɨ</ta>
            <ta e="T695" id="Seg_7443" s="T694">üːtɨ-n</ta>
            <ta e="T696" id="Seg_7444" s="T695">seːpɨlaŋ</ta>
            <ta e="T697" id="Seg_7445" s="T696">ɔːmtɨ</ta>
            <ta e="T698" id="Seg_7446" s="T697">na</ta>
            <ta e="T699" id="Seg_7447" s="T698">rɛmɨkɨ-mɔːt-qo-olam-ŋɨ</ta>
            <ta e="T700" id="Seg_7448" s="T699">qum-ɨ-m</ta>
            <ta e="T701" id="Seg_7449" s="T700">ponä</ta>
            <ta e="T702" id="Seg_7450" s="T701">*kurɨ-ltɨ-tɨ</ta>
            <ta e="T703" id="Seg_7451" s="T702">üŋkɨl-tɨ-mpɨ-ätɨ</ta>
            <ta e="T704" id="Seg_7452" s="T703">qum</ta>
            <ta e="T705" id="Seg_7453" s="T704">poː-qɨn</ta>
            <ta e="T706" id="Seg_7454" s="T705">nɨŋ</ta>
            <ta e="T707" id="Seg_7455" s="T706">qo</ta>
            <ta e="T708" id="Seg_7456" s="T707">qum</ta>
            <ta e="T709" id="Seg_7457" s="T708">na</ta>
            <ta e="T710" id="Seg_7458" s="T709">qompɨ-š-qo-olam-ŋɨ</ta>
            <ta e="T711" id="Seg_7459" s="T710">mɔːt-ntɨ</ta>
            <ta e="T712" id="Seg_7460" s="T711">pat-qɨl-ŋɨ</ta>
            <ta e="T713" id="Seg_7461" s="T712">mɔːt-ntɨ</ta>
            <ta e="T714" id="Seg_7462" s="T713">šeːr-lä</ta>
            <ta e="T715" id="Seg_7463" s="T714">nılʼčʼɨ-k</ta>
            <ta e="T716" id="Seg_7464" s="T715">kətɨ-mpɨ-tɨ</ta>
            <ta e="T717" id="Seg_7465" s="T716">lоːsɨ</ta>
            <ta e="T718" id="Seg_7466" s="T717">karrä-n</ta>
            <ta e="T719" id="Seg_7467" s="T718">qompɨ-š-ŋɨ</ta>
            <ta e="T720" id="Seg_7468" s="T719">konnä</ta>
            <ta e="T721" id="Seg_7469" s="T720">*kurɨ-š-lä</ta>
            <ta e="T722" id="Seg_7470" s="T721">na</ta>
            <ta e="T723" id="Seg_7471" s="T722">tantɨ-ntɨ</ta>
            <ta e="T724" id="Seg_7472" s="T723">konnä</ta>
            <ta e="T725" id="Seg_7473" s="T724">qaj</ta>
            <ta e="T726" id="Seg_7474" s="T725">qompɨ-š-tɨ</ta>
            <ta e="T727" id="Seg_7475" s="T726">a</ta>
            <ta e="T728" id="Seg_7476" s="T727">tan</ta>
            <ta e="T729" id="Seg_7477" s="T728">qaj</ta>
            <ta e="T730" id="Seg_7478" s="T729">kočʼkɨ-mɔːt-ɨ-ntɨ</ta>
            <ta e="T731" id="Seg_7479" s="T730">am-ɨ-r-ɛː-ŋɨ-ntɨ</ta>
            <ta e="T732" id="Seg_7480" s="T731">nɨrkɨ-mɔːt-ɨ-ŋɨ-ntɨ</ta>
            <ta e="T733" id="Seg_7481" s="T732">ukkɨr</ta>
            <ta e="T734" id="Seg_7482" s="T733">nʼeː-m-tɨ</ta>
            <ta e="T735" id="Seg_7483" s="T734">nılʼčʼɨ-k</ta>
            <ta e="T736" id="Seg_7484" s="T735">qo-ŋɨ-tɨ</ta>
            <ta e="T737" id="Seg_7485" s="T736">man</ta>
            <ta e="T738" id="Seg_7486" s="T737">ukoːn</ta>
            <ta e="T739" id="Seg_7487" s="T738">am-ɨ-r-ɛː-lä</ta>
            <ta e="T740" id="Seg_7488" s="T739">nılʼčʼɨ-k</ta>
            <ta e="T741" id="Seg_7489" s="T740">ippɨ-kkɨ-olʼ-ntɨ-mpɨ-kkɨ-k</ta>
            <ta e="T742" id="Seg_7490" s="T741">nɨːnɨ</ta>
            <ta e="T743" id="Seg_7491" s="T742">konnä</ta>
            <ta e="T744" id="Seg_7492" s="T743">*kurɨ-lä</ta>
            <ta e="T745" id="Seg_7493" s="T744">na</ta>
            <ta e="T746" id="Seg_7494" s="T745">tü-ntɨ</ta>
            <ta e="T747" id="Seg_7495" s="T746">konnä</ta>
            <ta e="T748" id="Seg_7496" s="T747">*kurɨ-lä</ta>
            <ta e="T749" id="Seg_7497" s="T748">na</ta>
            <ta e="T750" id="Seg_7498" s="T749">tü-ntɨ</ta>
            <ta e="T751" id="Seg_7499" s="T750">čʼuk</ta>
            <ta e="T752" id="Seg_7500" s="T751">čʼuk</ta>
            <ta e="T753" id="Seg_7501" s="T752">qaj</ta>
            <ta e="T754" id="Seg_7502" s="T753">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T755" id="Seg_7503" s="T754">kəš</ta>
            <ta e="T756" id="Seg_7504" s="T755">qattɨ-lʼčʼɨ</ta>
            <ta e="T757" id="Seg_7505" s="T756">tü-ŋɨ</ta>
            <ta e="T758" id="Seg_7506" s="T757">qaj</ta>
            <ta e="T759" id="Seg_7507" s="T758">qən-sɨ</ta>
            <ta e="T760" id="Seg_7508" s="T759">qaj</ta>
            <ta e="T761" id="Seg_7509" s="T760">qattɨ-sɨ</ta>
            <ta e="T762" id="Seg_7510" s="T761">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T763" id="Seg_7511" s="T762">kəš</ta>
            <ta e="T764" id="Seg_7512" s="T763">ukoːn</ta>
            <ta e="T765" id="Seg_7513" s="T764">naššan</ta>
            <ta e="T766" id="Seg_7514" s="T765">qən-sɨ</ta>
            <ta e="T767" id="Seg_7515" s="T766">tıntä</ta>
            <ta e="T768" id="Seg_7516" s="T767">naššan</ta>
            <ta e="T769" id="Seg_7517" s="T768">qən-sɨ</ta>
            <ta e="T770" id="Seg_7518" s="T769">tan</ta>
            <ta e="T771" id="Seg_7519" s="T770">konnä</ta>
            <ta e="T772" id="Seg_7520" s="T771">qən-lä</ta>
            <ta e="T773" id="Seg_7521" s="T772">apsɨ-lʼ</ta>
            <ta e="T774" id="Seg_7522" s="T773">am-ätɨ</ta>
            <ta e="T775" id="Seg_7523" s="T774">nɨːnɨ</ta>
            <ta e="T776" id="Seg_7524" s="T775">konnä</ta>
            <ta e="T777" id="Seg_7525" s="T776">qən-lä</ta>
            <ta e="T778" id="Seg_7526" s="T777">*kurɨ-lä</ta>
            <ta e="T779" id="Seg_7527" s="T778">tına</ta>
            <ta e="T780" id="Seg_7528" s="T779">pɔːrkä-ntɨ</ta>
            <ta e="T781" id="Seg_7529" s="T780">tü-ŋɨ</ta>
            <ta e="T782" id="Seg_7530" s="T781">kətɨ-tɨ</ta>
            <ta e="T783" id="Seg_7531" s="T782">qaj</ta>
            <ta e="T784" id="Seg_7532" s="T783">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T785" id="Seg_7533" s="T784">kəš</ta>
            <ta e="T786" id="Seg_7534" s="T785">mɔːt-qɨn</ta>
            <ta e="T787" id="Seg_7535" s="T786">ɛː-ŋɨ</ta>
            <ta e="T788" id="Seg_7536" s="T787">təp</ta>
            <ta e="T789" id="Seg_7537" s="T788">aš</ta>
            <ta e="T790" id="Seg_7538" s="T789">naššan</ta>
            <ta e="T791" id="Seg_7539" s="T790">tıntä</ta>
            <ta e="T792" id="Seg_7540" s="T791">qən-sɨ</ta>
            <ta e="T793" id="Seg_7541" s="T792">mɔːt</ta>
            <ta e="T794" id="Seg_7542" s="T793">šeːr-lä</ta>
            <ta e="T795" id="Seg_7543" s="T794">apsɨ-lʼ</ta>
            <ta e="T796" id="Seg_7544" s="T795">am-ätɨ</ta>
            <ta e="T797" id="Seg_7545" s="T796">ɔːmɨ</ta>
            <ta e="T798" id="Seg_7546" s="T797">aj</ta>
            <ta e="T799" id="Seg_7547" s="T798">nılʼčʼɨ-k</ta>
            <ta e="T800" id="Seg_7548" s="T799">ɛsɨ</ta>
            <ta e="T801" id="Seg_7549" s="T800">mɔːt-ntɨ</ta>
            <ta e="T802" id="Seg_7550" s="T801">qaj</ta>
            <ta e="T803" id="Seg_7551" s="T802">šeːr-ɛntɨ-k</ta>
            <ta e="T804" id="Seg_7552" s="T803">qaj</ta>
            <ta e="T805" id="Seg_7553" s="T804">ašša</ta>
            <ta e="T806" id="Seg_7554" s="T805">šeːr-ɛntɨ-k</ta>
            <ta e="T807" id="Seg_7555" s="T806">ɔːmɨ</ta>
            <ta e="T808" id="Seg_7556" s="T807">aj</ta>
            <ta e="T809" id="Seg_7557" s="T808">mɨ</ta>
            <ta e="T810" id="Seg_7558" s="T809">pɔːrkä</ta>
            <ta e="T811" id="Seg_7559" s="T810">qaj</ta>
            <ta e="T812" id="Seg_7560" s="T811">mompa</ta>
            <ta e="T813" id="Seg_7561" s="T812">mašım</ta>
            <ta e="T814" id="Seg_7562" s="T813">kurɨ-mpɨ-ntɨ</ta>
            <ta e="T815" id="Seg_7563" s="T814">qapı</ta>
            <ta e="T816" id="Seg_7564" s="T815">kulʼtɨ-mpɨ</ta>
            <ta e="T817" id="Seg_7565" s="T816">kulʼtɨ-mpɨ-ŋɨ</ta>
            <ta e="T818" id="Seg_7566" s="T817">ɔːmɨ-lʼ</ta>
            <ta e="T819" id="Seg_7567" s="T818">qaj</ta>
            <ta e="T820" id="Seg_7568" s="T819">šeːr-ɛntɨ</ta>
            <ta e="T821" id="Seg_7569" s="T820">qaj</ta>
            <ta e="T822" id="Seg_7570" s="T821">ašša</ta>
            <ta e="T823" id="Seg_7571" s="T822">šeːr-ɛntɨ</ta>
            <ta e="T824" id="Seg_7572" s="T823">ukkɨr</ta>
            <ta e="T825" id="Seg_7573" s="T824">čʼontɨ-qɨn</ta>
            <ta e="T826" id="Seg_7574" s="T825">na</ta>
            <ta e="T827" id="Seg_7575" s="T826">šeːr-qo-olam-ntɨ</ta>
            <ta e="T828" id="Seg_7576" s="T827">olɨ-m-tɨ</ta>
            <ta e="T829" id="Seg_7577" s="T828">mɔːt-ntɨ</ta>
            <ta e="T830" id="Seg_7578" s="T829">čʼam</ta>
            <ta e="T831" id="Seg_7579" s="T830">noqqo-ɔːl-ŋɨ-tɨ</ta>
            <ta e="T832" id="Seg_7580" s="T831">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T833" id="Seg_7581" s="T832">kəš</ta>
            <ta e="T834" id="Seg_7582" s="T833">pačʼčʼɨ-ätɔːl-ŋɨ-tɨ</ta>
            <ta e="T835" id="Seg_7583" s="T834">olɨ-n-tɨ</ta>
            <ta e="T836" id="Seg_7584" s="T835">laka</ta>
            <ta e="T837" id="Seg_7585" s="T836">mɔːt-ntɨ</ta>
            <ta e="T838" id="Seg_7586" s="T837">nʼennä</ta>
            <ta e="T839" id="Seg_7587" s="T838">alʼčʼɨ</ta>
            <ta e="T840" id="Seg_7588" s="T839">püŋkɨ-olʼ-ŋɨ</ta>
            <ta e="T841" id="Seg_7589" s="T840">kəpɨ-n-tɨ</ta>
            <ta e="T842" id="Seg_7590" s="T841">laka</ta>
            <ta e="T843" id="Seg_7591" s="T842">qottä</ta>
            <ta e="T844" id="Seg_7592" s="T843">ponä</ta>
            <ta e="T845" id="Seg_7593" s="T844">alʼčʼɨ</ta>
            <ta e="T846" id="Seg_7594" s="T845">qət-ɛː-ŋɨ-tɨ</ta>
            <ta e="T847" id="Seg_7595" s="T846">nɨːnɨ</ta>
            <ta e="T848" id="Seg_7596" s="T847">nɔːtɨ</ta>
            <ta e="T849" id="Seg_7597" s="T848">čʼam</ta>
            <ta e="T850" id="Seg_7598" s="T849">ɔːmtɨ-tɨt</ta>
            <ta e="T851" id="Seg_7599" s="T850">ɔːmtɨ-tɨt</ta>
            <ta e="T852" id="Seg_7600" s="T851">nʼi</ta>
            <ta e="T853" id="Seg_7601" s="T852">qaj</ta>
            <ta e="T854" id="Seg_7602" s="T853">čʼäːŋkɨ</ta>
            <ta e="T855" id="Seg_7603" s="T854">mompa</ta>
            <ta e="T856" id="Seg_7604" s="T855">qaj</ta>
            <ta e="T857" id="Seg_7605" s="T856">šittɨ</ta>
            <ta e="T858" id="Seg_7606" s="T857">ičʼčʼɨ</ta>
            <ta e="T859" id="Seg_7607" s="T858">tɔːptɨlʼ</ta>
            <ta e="T860" id="Seg_7608" s="T859">qarɨ-ntɨ</ta>
            <ta e="T861" id="Seg_7609" s="T860">ınnä</ta>
            <ta e="T862" id="Seg_7610" s="T861">čʼeːlɨ-k-ŋɨ</ta>
            <ta e="T863" id="Seg_7611" s="T862">mompa</ta>
            <ta e="T864" id="Seg_7612" s="T863">poː</ta>
            <ta e="T865" id="Seg_7613" s="T864">pačʼčʼɨ-ɔːl-ŋɨlɨt</ta>
            <ta e="T866" id="Seg_7614" s="T865">poː</ta>
            <ta e="T867" id="Seg_7615" s="T866">pačʼčʼɨ-ätɔːl-ŋɨ-tɨt</ta>
            <ta e="T868" id="Seg_7616" s="T867">tü</ta>
            <ta e="T869" id="Seg_7617" s="T868">čʼɔːtɨ-ŋɨlɨt</ta>
            <ta e="T870" id="Seg_7618" s="T869">əːtɨmɨn</ta>
            <ta e="T871" id="Seg_7619" s="T870">poː-m</ta>
            <ta e="T872" id="Seg_7620" s="T871">pačʼčʼɨ-ɔːl-ŋɨ-tɨt</ta>
            <ta e="T873" id="Seg_7621" s="T872">šittɨ-qı-m</ta>
            <ta e="T874" id="Seg_7622" s="T873">karrä</ta>
            <ta e="T875" id="Seg_7623" s="T874">*tul-tɨ-kkɨ-tɨt</ta>
            <ta e="T876" id="Seg_7624" s="T875">tü-sä</ta>
            <ta e="T877" id="Seg_7625" s="T876">čʼɔːtɨ-tɨt</ta>
            <ta e="T878" id="Seg_7626" s="T877">koptɨ-kɔːlɨ-k</ta>
            <ta e="T879" id="Seg_7627" s="T878">tü-sä</ta>
            <ta e="T880" id="Seg_7628" s="T879">čʼɔːtɨ-tɨt</ta>
            <ta e="T881" id="Seg_7629" s="T880">ira-lʼ</ta>
            <ta e="T882" id="Seg_7630" s="T881">əsɨ-sɨ-n</ta>
            <ta e="T883" id="Seg_7631" s="T882">na</ta>
            <ta e="T884" id="Seg_7632" s="T883">ira</ta>
            <ta e="T885" id="Seg_7633" s="T884">nälʼa-tɨ</ta>
            <ta e="T886" id="Seg_7634" s="T885">ɛː-mpɨ</ta>
            <ta e="T887" id="Seg_7635" s="T886">na</ta>
            <ta e="T888" id="Seg_7636" s="T887">nälʼa-m-tɨ</ta>
            <ta e="T889" id="Seg_7637" s="T888">Sɨlʼčʼa_Pɨlʼčʼa</ta>
            <ta e="T890" id="Seg_7638" s="T889">kəš-ntɨ</ta>
            <ta e="T891" id="Seg_7639" s="T890">mi-ŋɨ-tɨ</ta>
            <ta e="T892" id="Seg_7640" s="T891">na</ta>
            <ta e="T893" id="Seg_7641" s="T892">təttɨ-n</ta>
            <ta e="T894" id="Seg_7642" s="T893">moːrɨ</ta>
            <ta e="T895" id="Seg_7643" s="T894">kolʼɨ-altɨ-ntɨ-kkɨ-tɨ</ta>
            <ta e="T896" id="Seg_7644" s="T895">šölʼqum-ɨ-t-ɨ-nɨŋ</ta>
            <ta e="T897" id="Seg_7645" s="T896">tü-ŋɨ</ta>
            <ta e="T898" id="Seg_7646" s="T897">moːrɨ-tɨ</ta>
            <ta e="T899" id="Seg_7647" s="T898">na</ta>
            <ta e="T900" id="Seg_7648" s="T899">ɛː-ntɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_7649" s="T0">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T2" id="Seg_7650" s="T1">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T3" id="Seg_7651" s="T2">half.[NOM]</ta>
            <ta e="T4" id="Seg_7652" s="T3">live-PST.NAR.[3SG.S]</ta>
            <ta e="T5" id="Seg_7653" s="T4">mother-3SG-ADJZ</ta>
            <ta e="T6" id="Seg_7654" s="T5">be-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T7" id="Seg_7655" s="T6">father.[NOM]-3SG</ta>
            <ta e="T8" id="Seg_7656" s="T7">NEG.EX-PST.NAR.[3SG.S]</ta>
            <ta e="T9" id="Seg_7657" s="T8">live.[3SG.S]</ta>
            <ta e="T10" id="Seg_7658" s="T9">live.[3SG.S]</ta>
            <ta e="T11" id="Seg_7659" s="T10">one</ta>
            <ta e="T12" id="Seg_7660" s="T11">middle-ADV.LOC</ta>
            <ta e="T13" id="Seg_7661" s="T12">mother-ILL</ta>
            <ta e="T14" id="Seg_7662" s="T13">so</ta>
            <ta e="T15" id="Seg_7663" s="T14">say-3SG.O</ta>
            <ta e="T16" id="Seg_7664" s="T15">say-3SG.O</ta>
            <ta e="T17" id="Seg_7665" s="T16">mother-ILL</ta>
            <ta e="T18" id="Seg_7666" s="T17">now</ta>
            <ta e="T19" id="Seg_7667" s="T18">such-ADVZ</ta>
            <ta e="T20" id="Seg_7668" s="T19">say.[3SG.S]</ta>
            <ta e="T21" id="Seg_7669" s="T20">I.NOM</ta>
            <ta e="T22" id="Seg_7670" s="T21">human.being-EP-ADJZ</ta>
            <ta e="T23" id="Seg_7671" s="T22">look.for-FRQ-CVB</ta>
            <ta e="T24" id="Seg_7672" s="T23">leave-PRS-1SG.S</ta>
            <ta e="T25" id="Seg_7673" s="T24">so</ta>
            <ta e="T26" id="Seg_7674" s="T25">how</ta>
            <ta e="T27" id="Seg_7675" s="T26">live-FUT-1PL</ta>
            <ta e="T28" id="Seg_7676" s="T27">mother.[NOM]</ta>
            <ta e="T29" id="Seg_7677" s="T28">you.SG.NOM</ta>
            <ta e="T30" id="Seg_7678" s="T29">really</ta>
            <ta e="T31" id="Seg_7679" s="T30">whether</ta>
            <ta e="T32" id="Seg_7680" s="T31">this</ta>
            <ta e="T33" id="Seg_7681" s="T32">earth-GEN</ta>
            <ta e="T34" id="Seg_7682" s="T33">environs-LOC</ta>
            <ta e="T35" id="Seg_7683" s="T34">human.being-EP-ACC</ta>
            <ta e="T36" id="Seg_7684" s="T35">NEG</ta>
            <ta e="T37" id="Seg_7685" s="T36">know.[3SG.S]</ta>
            <ta e="T38" id="Seg_7686" s="T37">mother.[NOM]-3SG</ta>
            <ta e="T39" id="Seg_7687" s="T38">speak-PRS-3SG.O</ta>
            <ta e="T40" id="Seg_7688" s="T39">I.NOM</ta>
            <ta e="T41" id="Seg_7689" s="T40">NEG</ta>
            <ta e="T42" id="Seg_7690" s="T41">what-ADJZ</ta>
            <ta e="T43" id="Seg_7691" s="T42">human.being.[NOM]</ta>
            <ta e="T44" id="Seg_7692" s="T43">NEG</ta>
            <ta e="T45" id="Seg_7693" s="T44">know.[3SG.S]</ta>
            <ta e="T46" id="Seg_7694" s="T45">this</ta>
            <ta e="T47" id="Seg_7695" s="T46">earth-GEN</ta>
            <ta e="T48" id="Seg_7696" s="T47">environs-LOC</ta>
            <ta e="T49" id="Seg_7697" s="T48">relative-ADV.LOC</ta>
            <ta e="T50" id="Seg_7698" s="T49">human.being.[NOM]</ta>
            <ta e="T51" id="Seg_7699" s="T50">NEG.EX.[3SG.S]</ta>
            <ta e="T52" id="Seg_7700" s="T51">son.[NOM]-3SG</ta>
            <ta e="T53" id="Seg_7701" s="T52">mother-ILL-EMPH</ta>
            <ta e="T54" id="Seg_7702" s="T53">such-ADJZ</ta>
            <ta e="T55" id="Seg_7703" s="T54">say-3SG.O</ta>
            <ta e="T56" id="Seg_7704" s="T55">you.SG.NOM</ta>
            <ta e="T57" id="Seg_7705" s="T56">it.is.said</ta>
            <ta e="T58" id="Seg_7706" s="T57">yourself.[NOM]</ta>
            <ta e="T59" id="Seg_7707" s="T58">live-IMP.2SG.S</ta>
            <ta e="T60" id="Seg_7708" s="T59">I.NOM</ta>
            <ta e="T61" id="Seg_7709" s="T60">soon</ta>
            <ta e="T62" id="Seg_7710" s="T61">human.being-EP-ADJZ</ta>
            <ta e="T63" id="Seg_7711" s="T62">look.for-FRQ-EP-CVB</ta>
            <ta e="T64" id="Seg_7712" s="T63">leave-OPT-1SG.S</ta>
            <ta e="T65" id="Seg_7713" s="T64">this</ta>
            <ta e="T66" id="Seg_7714" s="T65">earth-GEN</ta>
            <ta e="T67" id="Seg_7715" s="T66">environs-LOC</ta>
            <ta e="T68" id="Seg_7716" s="T67">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T69" id="Seg_7717" s="T68">half.[NOM]</ta>
            <ta e="T70" id="Seg_7718" s="T69">then</ta>
            <ta e="T71" id="Seg_7719" s="T70">go.away-PRS.[3SG.S]</ta>
            <ta e="T72" id="Seg_7720" s="T71">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T73" id="Seg_7721" s="T72">half.[NOM]</ta>
            <ta e="T74" id="Seg_7722" s="T73">so.much</ta>
            <ta e="T75" id="Seg_7723" s="T74">go.away-IPFV-PRS.[3SG.S]</ta>
            <ta e="T76" id="Seg_7724" s="T75">winter-VBLZ-DUR.[3SG.S]</ta>
            <ta e="T77" id="Seg_7725" s="T76">and</ta>
            <ta e="T78" id="Seg_7726" s="T77">summer-TRL-DUR.[3SG.S]</ta>
            <ta e="T79" id="Seg_7727" s="T78">big.river-ACC</ta>
            <ta e="T80" id="Seg_7728" s="T79">river.[NOM]</ta>
            <ta e="T81" id="Seg_7729" s="T80">come-IPFV-HAB-DUR.[3SG.S]</ta>
            <ta e="T82" id="Seg_7730" s="T81">small</ta>
            <ta e="T83" id="Seg_7731" s="T82">river.[NOM]</ta>
            <ta e="T84" id="Seg_7732" s="T83">come-IPFV-HAB-DUR.[3SG.S]</ta>
            <ta e="T85" id="Seg_7733" s="T84">no</ta>
            <ta e="T86" id="Seg_7734" s="T85">NEG</ta>
            <ta e="T87" id="Seg_7735" s="T86">what-ADJZ</ta>
            <ta e="T88" id="Seg_7736" s="T87">human.being.[NOM]</ta>
            <ta e="T89" id="Seg_7737" s="T88">NEG.EX.[3SG.S]</ta>
            <ta e="T90" id="Seg_7738" s="T89">then</ta>
            <ta e="T91" id="Seg_7739" s="T90">lake-ADJZ</ta>
            <ta e="T92" id="Seg_7740" s="T91">something-LOC</ta>
            <ta e="T93" id="Seg_7741" s="T92">river-ADVZ</ta>
            <ta e="T94" id="Seg_7742" s="T93">leave-DUR.[3SG.S]</ta>
            <ta e="T95" id="Seg_7743" s="T94">cap-3SG.TRL</ta>
            <ta e="T96" id="Seg_7744" s="T95">forehead-LOC</ta>
            <ta e="T97" id="Seg_7745" s="T96">snow-VBLZ-IPFV-DUR.[3SG.S]</ta>
            <ta e="T98" id="Seg_7746" s="T97">and</ta>
            <ta e="T99" id="Seg_7747" s="T98">waist-TRL</ta>
            <ta e="T100" id="Seg_7748" s="T99">one</ta>
            <ta e="T101" id="Seg_7749" s="T100">middle-LOC</ta>
            <ta e="T102" id="Seg_7750" s="T101">come-US-PRS.[3SG.S]</ta>
            <ta e="T103" id="Seg_7751" s="T102">forest-ILL</ta>
            <ta e="T104" id="Seg_7752" s="T103">big-ADJZ</ta>
            <ta e="T105" id="Seg_7753" s="T104">forest.[NOM]</ta>
            <ta e="T106" id="Seg_7754" s="T105">this</ta>
            <ta e="T107" id="Seg_7755" s="T106">forest-PROL</ta>
            <ta e="T108" id="Seg_7756" s="T107">river.[NOM]</ta>
            <ta e="T109" id="Seg_7757" s="T108">go.out-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T110" id="Seg_7758" s="T109">this</ta>
            <ta e="T111" id="Seg_7759" s="T110">river-GEN</ta>
            <ta e="T112" id="Seg_7760" s="T111">bank-PROL</ta>
            <ta e="T113" id="Seg_7761" s="T112">go-PST.NAR-1SG.S</ta>
            <ta e="T114" id="Seg_7762" s="T113">one</ta>
            <ta e="T115" id="Seg_7763" s="T114">middle-LOC</ta>
            <ta e="T116" id="Seg_7764" s="T115">DEF</ta>
            <ta e="T117" id="Seg_7765" s="T116">what.[NOM]</ta>
            <ta e="T118" id="Seg_7766" s="T117">be.heard-INFER.[3SG.S]</ta>
            <ta e="T119" id="Seg_7767" s="T118">ah</ta>
            <ta e="T120" id="Seg_7768" s="T119">boom</ta>
            <ta e="T121" id="Seg_7769" s="T120">ah</ta>
            <ta e="T122" id="Seg_7770" s="T121">boom</ta>
            <ta e="T123" id="Seg_7771" s="T122">go-HAB.[3SG.S]</ta>
            <ta e="T124" id="Seg_7772" s="T123">such-ADJZ</ta>
            <ta e="T125" id="Seg_7773" s="T124">voice.[NOM]</ta>
            <ta e="T126" id="Seg_7774" s="T125">be.heard-PRS-3SG.O</ta>
            <ta e="T127" id="Seg_7775" s="T126">NEG</ta>
            <ta e="T128" id="Seg_7776" s="T127">what.[NOM]</ta>
            <ta e="T129" id="Seg_7777" s="T128">wild.animal.[NOM]</ta>
            <ta e="T130" id="Seg_7778" s="T129">NEG</ta>
            <ta e="T131" id="Seg_7779" s="T130">what.[NOM]</ta>
            <ta e="T132" id="Seg_7780" s="T131">NEG.EX.[3SG.S]</ta>
            <ta e="T133" id="Seg_7781" s="T132">this</ta>
            <ta e="T134" id="Seg_7782" s="T133">earth-ACC</ta>
            <ta e="T135" id="Seg_7783" s="T134">environs.[NOM]</ta>
            <ta e="T136" id="Seg_7784" s="T135">turn-TR-ACTN-1SG.LOC</ta>
            <ta e="T137" id="Seg_7785" s="T136">come.on</ta>
            <ta e="T138" id="Seg_7786" s="T137">what.[NOM]</ta>
            <ta e="T139" id="Seg_7787" s="T138">say-US-INFER.[3SG.S]</ta>
            <ta e="T140" id="Seg_7788" s="T139">I.NOM</ta>
            <ta e="T141" id="Seg_7789" s="T140">if.only</ta>
            <ta e="T142" id="Seg_7790" s="T141">give.a.look-HAB-PST-1SG.S</ta>
            <ta e="T143" id="Seg_7791" s="T142">CONJ</ta>
            <ta e="T144" id="Seg_7792" s="T143">INFER</ta>
            <ta e="T145" id="Seg_7793" s="T144">come-IPFV-INFER.[3SG.S]</ta>
            <ta e="T146" id="Seg_7794" s="T145">INFER</ta>
            <ta e="T147" id="Seg_7795" s="T146">come-IPFV-INFER.[3SG.S]</ta>
            <ta e="T148" id="Seg_7796" s="T147">still</ta>
            <ta e="T149" id="Seg_7797" s="T148">as.if</ta>
            <ta e="T150" id="Seg_7798" s="T149">as.if</ta>
            <ta e="T151" id="Seg_7799" s="T150">river.[NOM]</ta>
            <ta e="T152" id="Seg_7800" s="T151">inside-LOC</ta>
            <ta e="T153" id="Seg_7801" s="T152">down.the.river-ADV.LOC</ta>
            <ta e="T154" id="Seg_7802" s="T153">be.heard.[3SG.S]</ta>
            <ta e="T155" id="Seg_7803" s="T154">again</ta>
            <ta e="T157" id="Seg_7804" s="T156">INFER</ta>
            <ta e="T158" id="Seg_7805" s="T157">cry-US-IPFV-INFER.[3SG.S]</ta>
            <ta e="T159" id="Seg_7806" s="T158">ah</ta>
            <ta e="T160" id="Seg_7807" s="T159">boom</ta>
            <ta e="T161" id="Seg_7808" s="T160">ah</ta>
            <ta e="T162" id="Seg_7809" s="T161">boom</ta>
            <ta e="T163" id="Seg_7810" s="T162">such</ta>
            <ta e="T164" id="Seg_7811" s="T163">sound.[NOM]-3SG</ta>
            <ta e="T165" id="Seg_7812" s="T164">what-ADJZ</ta>
            <ta e="T166" id="Seg_7813" s="T165">apparently</ta>
            <ta e="T167" id="Seg_7814" s="T166">resound-FRQ-PRS.[3SG.S]</ta>
            <ta e="T168" id="Seg_7815" s="T167">INFER</ta>
            <ta e="T169" id="Seg_7816" s="T168">come-IPFV-PRS.[3SG.S]</ta>
            <ta e="T170" id="Seg_7817" s="T169">down.the.river-ADV.LOC</ta>
            <ta e="T171" id="Seg_7818" s="T170">river-GEN</ta>
            <ta e="T172" id="Seg_7819" s="T171">middle-LOC</ta>
            <ta e="T173" id="Seg_7820" s="T172">DEF</ta>
            <ta e="T174" id="Seg_7821" s="T173">what.[NOM]</ta>
            <ta e="T175" id="Seg_7822" s="T174">black-VBLZ-DECAUS.[3SG.S]</ta>
            <ta e="T176" id="Seg_7823" s="T175">this.[NOM]</ta>
            <ta e="T177" id="Seg_7824" s="T176">foreign</ta>
            <ta e="T178" id="Seg_7825" s="T177">cry-PRS.[3SG.S]</ta>
            <ta e="T179" id="Seg_7826" s="T178">ah</ta>
            <ta e="T180" id="Seg_7827" s="T179">boom</ta>
            <ta e="T181" id="Seg_7828" s="T180">ah</ta>
            <ta e="T182" id="Seg_7829" s="T181">boom</ta>
            <ta e="T183" id="Seg_7830" s="T182">NEG</ta>
            <ta e="T184" id="Seg_7831" s="T183">NEG</ta>
            <ta e="T185" id="Seg_7832" s="T184">human.being-EP-GEN</ta>
            <ta e="T186" id="Seg_7833" s="T185">voice.[NOM]</ta>
            <ta e="T187" id="Seg_7834" s="T186">only</ta>
            <ta e="T188" id="Seg_7835" s="T187">so</ta>
            <ta e="T189" id="Seg_7836" s="T188">cry-US-PRS.[3SG.S]</ta>
            <ta e="T190" id="Seg_7837" s="T189">there</ta>
            <ta e="T191" id="Seg_7838" s="T190">INFER</ta>
            <ta e="T192" id="Seg_7839" s="T191">come-INFER.[3SG.S]</ta>
            <ta e="T193" id="Seg_7840" s="T192">apparently</ta>
            <ta e="T194" id="Seg_7841" s="T193">such-ADVZ</ta>
            <ta e="T195" id="Seg_7842" s="T194">human.being.[NOM]</ta>
            <ta e="T196" id="Seg_7843" s="T195">cry-PST.[3SG.S]</ta>
            <ta e="T197" id="Seg_7844" s="T196">there</ta>
            <ta e="T198" id="Seg_7845" s="T197">hardly</ta>
            <ta e="T199" id="Seg_7846" s="T198">come-IPFV.[3SG.S]</ta>
            <ta e="T200" id="Seg_7847" s="T199">give.a.look-CVB</ta>
            <ta e="T201" id="Seg_7848" s="T200">human.being.[NOM]</ta>
            <ta e="T202" id="Seg_7849" s="T201">apparently</ta>
            <ta e="T203" id="Seg_7850" s="T202">human.being.[NOM]</ta>
            <ta e="T204" id="Seg_7851" s="T203">this</ta>
            <ta e="T205" id="Seg_7852" s="T204">father-GEN-3SG</ta>
            <ta e="T206" id="Seg_7853" s="T205">mother-GEN-3SG</ta>
            <ta e="T207" id="Seg_7854" s="T206">meat-INSTR</ta>
            <ta e="T208" id="Seg_7855" s="T207">nude</ta>
            <ta e="T209" id="Seg_7856" s="T208">human.being.[NOM]</ta>
            <ta e="T210" id="Seg_7857" s="T209">(s)he.[NOM]</ta>
            <ta e="T211" id="Seg_7858" s="T210">father-GEN-3SG</ta>
            <ta e="T212" id="Seg_7859" s="T211">mother-GEN-3SG</ta>
            <ta e="T213" id="Seg_7860" s="T212">meat-INSTR</ta>
            <ta e="T214" id="Seg_7861" s="T213">nude</ta>
            <ta e="T215" id="Seg_7862" s="T214">sit.[3SG.S]</ta>
            <ta e="T216" id="Seg_7863" s="T215">this</ta>
            <ta e="T217" id="Seg_7864" s="T216">ice.hole-GEN</ta>
            <ta e="T218" id="Seg_7865" s="T217">opening-LOC</ta>
            <ta e="T219" id="Seg_7866" s="T218">this</ta>
            <ta e="T220" id="Seg_7867" s="T219">whether</ta>
            <ta e="T221" id="Seg_7868" s="T220">this</ta>
            <ta e="T222" id="Seg_7869" s="T221">human.being.[NOM]</ta>
            <ta e="T223" id="Seg_7870" s="T222">cry-US-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T224" id="Seg_7871" s="T223">you.SG.NOM</ta>
            <ta e="T225" id="Seg_7872" s="T224">what.for</ta>
            <ta e="T226" id="Seg_7873" s="T225">such-ADJZ</ta>
            <ta e="T227" id="Seg_7874" s="T226">sit-2SG.S</ta>
            <ta e="T228" id="Seg_7875" s="T227">NEG</ta>
            <ta e="T229" id="Seg_7876" s="T228">it.is.said</ta>
            <ta e="T230" id="Seg_7877" s="T229">I.NOM</ta>
            <ta e="T231" id="Seg_7878" s="T230">devil.[NOM]</ta>
            <ta e="T232" id="Seg_7879" s="T231">I.ACC</ta>
            <ta e="T233" id="Seg_7880" s="T232">eat-INF</ta>
            <ta e="T234" id="Seg_7881" s="T233">sit-1SG.S</ta>
            <ta e="T235" id="Seg_7882" s="T234">upwards-ADV.LOC</ta>
            <ta e="T236" id="Seg_7883" s="T235">human.being-PL.[NOM]-1SG</ta>
            <ta e="T237" id="Seg_7884" s="T236">be-PRS-3PL</ta>
            <ta e="T238" id="Seg_7885" s="T237">this-PL.[NOM]</ta>
            <ta e="T239" id="Seg_7886" s="T238">I.ACC</ta>
            <ta e="T240" id="Seg_7887" s="T239">sit.down-RES-PST.NAR-3PL</ta>
            <ta e="T241" id="Seg_7888" s="T240">devil.[NOM]</ta>
            <ta e="T242" id="Seg_7889" s="T241">I.ACC</ta>
            <ta e="T243" id="Seg_7890" s="T242">eat-INF</ta>
            <ta e="T244" id="Seg_7891" s="T243">simply</ta>
            <ta e="T245" id="Seg_7892" s="T244">I.NOM</ta>
            <ta e="T246" id="Seg_7893" s="T245">upwards</ta>
            <ta e="T247" id="Seg_7894" s="T246">go.out-FUT-1SG.S</ta>
            <ta e="T248" id="Seg_7895" s="T247">devil.[NOM]</ta>
            <ta e="T249" id="Seg_7896" s="T248">all</ta>
            <ta e="T250" id="Seg_7897" s="T249">we.PL.ACC</ta>
            <ta e="T251" id="Seg_7898" s="T250">eat-FUT.[3SG.S]</ta>
            <ta e="T252" id="Seg_7899" s="T251">NEG</ta>
            <ta e="T253" id="Seg_7900" s="T252">what-ACC</ta>
            <ta e="T254" id="Seg_7901" s="T253">numerous</ta>
            <ta e="T255" id="Seg_7902" s="T254">stay-TR.[3SG.S]</ta>
            <ta e="T256" id="Seg_7903" s="T255">here</ta>
            <ta e="T257" id="Seg_7904" s="T256">what.for</ta>
            <ta e="T258" id="Seg_7905" s="T257">I.ACC</ta>
            <ta e="T259" id="Seg_7906" s="T258">sit.down-RES-PST.NAR-3PL</ta>
            <ta e="T260" id="Seg_7907" s="T259">you.SG.NOM</ta>
            <ta e="T261" id="Seg_7908" s="T260">upwards</ta>
            <ta e="T262" id="Seg_7909" s="T261">go.out-IMP.2SG.S</ta>
            <ta e="T263" id="Seg_7910" s="T262">you.SG.NOM</ta>
            <ta e="T264" id="Seg_7911" s="T263">also</ta>
            <ta e="T265" id="Seg_7912" s="T264">upwards</ta>
            <ta e="T266" id="Seg_7913" s="T265">go.out-IMP.2SG.S</ta>
            <ta e="T267" id="Seg_7914" s="T266">I.NOM</ta>
            <ta e="T268" id="Seg_7915" s="T267">how</ta>
            <ta e="T269" id="Seg_7916" s="T268">go.out-FUT-1SG.S</ta>
            <ta e="T270" id="Seg_7917" s="T269">human.being-EP-PL.[NOM]</ta>
            <ta e="T271" id="Seg_7918" s="T270">I.ACC</ta>
            <ta e="T272" id="Seg_7919" s="T271">sight-PFV-FUT-3PL</ta>
            <ta e="T273" id="Seg_7920" s="T272">I.NOM</ta>
            <ta e="T274" id="Seg_7921" s="T273">speak-PRS-1SG.O</ta>
            <ta e="T275" id="Seg_7922" s="T274">you.SG.NOM</ta>
            <ta e="T276" id="Seg_7923" s="T275">upwards</ta>
            <ta e="T277" id="Seg_7924" s="T276">go.out-IMP.2SG.S</ta>
            <ta e="T278" id="Seg_7925" s="T277">upwards</ta>
            <ta e="T279" id="Seg_7926" s="T278">INFER</ta>
            <ta e="T280" id="Seg_7927" s="T279">go.out-INFER-3DU.S</ta>
            <ta e="T281" id="Seg_7928" s="T280">Sylcha_Pylcha-GEN</ta>
            <ta e="T282" id="Seg_7929" s="T281">half.[NOM]</ta>
            <ta e="T283" id="Seg_7930" s="T282">idol.[NOM]</ta>
            <ta e="T284" id="Seg_7931" s="T283">INFER</ta>
            <ta e="T285" id="Seg_7932" s="T284">make-PFV-INFER-3SG.O</ta>
            <ta e="T286" id="Seg_7933" s="T285">pike-OBL.3SG-INSTR</ta>
            <ta e="T287" id="Seg_7934" s="T286">you.SG.NOM</ta>
            <ta e="T288" id="Seg_7935" s="T287">here</ta>
            <ta e="T289" id="Seg_7936" s="T288">I.ACC</ta>
            <ta e="T290" id="Seg_7937" s="T289">how.many</ta>
            <ta e="T291" id="Seg_7938" s="T290">NEG.IMP</ta>
            <ta e="T292" id="Seg_7939" s="T291">I.ACC</ta>
            <ta e="T293" id="Seg_7940" s="T292">say-IMP.2SG.S</ta>
            <ta e="T294" id="Seg_7941" s="T293">devil.[NOM]</ta>
            <ta e="T295" id="Seg_7942" s="T294">you.SG.ACC</ta>
            <ta e="T296" id="Seg_7943" s="T295">INFER</ta>
            <ta e="T297" id="Seg_7944" s="T296">ask-FUT-INFER.[3SG.S]</ta>
            <ta e="T298" id="Seg_7945" s="T297">Sylcha_Pylcha.[NOM]-3SG</ta>
            <ta e="T299" id="Seg_7946" s="T298">half.[NOM]</ta>
            <ta e="T300" id="Seg_7947" s="T299">where</ta>
            <ta e="T301" id="Seg_7948" s="T300">where.to.go-PST.[3SG.S]</ta>
            <ta e="T302" id="Seg_7949" s="T301">I.ACC</ta>
            <ta e="T303" id="Seg_7950" s="T302">how.many-EMPH</ta>
            <ta e="T304" id="Seg_7951" s="T303">I.ACC</ta>
            <ta e="T305" id="Seg_7952" s="T304">say-IMP.2SG.S</ta>
            <ta e="T306" id="Seg_7953" s="T305">such-ADJZ</ta>
            <ta e="T307" id="Seg_7954" s="T306">threaten-PST.NAR-3SG.O</ta>
            <ta e="T308" id="Seg_7955" s="T307">pike-INSTR</ta>
            <ta e="T309" id="Seg_7956" s="T308">upwards</ta>
            <ta e="T310" id="Seg_7957" s="T309">INFER</ta>
            <ta e="T311" id="Seg_7958" s="T310">leave-INFER.[3SG.S]</ta>
            <ta e="T312" id="Seg_7959" s="T311">there</ta>
            <ta e="T313" id="Seg_7960" s="T312">apparently</ta>
            <ta e="T314" id="Seg_7961" s="T313">earth-ADJZ</ta>
            <ta e="T315" id="Seg_7962" s="T314">house.[NOM]</ta>
            <ta e="T316" id="Seg_7963" s="T315">this-LOC</ta>
            <ta e="T317" id="Seg_7964" s="T316">door-GEN</ta>
            <ta e="T318" id="Seg_7965" s="T317">opening-LOC</ta>
            <ta e="T319" id="Seg_7966" s="T318">also</ta>
            <ta e="T320" id="Seg_7967" s="T319">idol.[NOM]</ta>
            <ta e="T321" id="Seg_7968" s="T320">INFER</ta>
            <ta e="T322" id="Seg_7969" s="T321">make-INFER-3SG.O</ta>
            <ta e="T323" id="Seg_7970" s="T322">house.[NOM]</ta>
            <ta e="T324" id="Seg_7971" s="T323">come.in-IMP.2SG.S</ta>
            <ta e="T325" id="Seg_7972" s="T324">what.for</ta>
            <ta e="T326" id="Seg_7973" s="T325">stand-EP-2SG.S</ta>
            <ta e="T327" id="Seg_7974" s="T326">freeze-PRS-2SG.S</ta>
            <ta e="T328" id="Seg_7975" s="T327">I.NOM</ta>
            <ta e="T329" id="Seg_7976" s="T328">when</ta>
            <ta e="T330" id="Seg_7977" s="T329">come.in-FUT-1SG.S</ta>
            <ta e="T331" id="Seg_7978" s="T330">human.being-PL.[NOM]-1SG</ta>
            <ta e="T332" id="Seg_7979" s="T331">I.ACC</ta>
            <ta e="T333" id="Seg_7980" s="T332">sight-PFV-FUT-3PL</ta>
            <ta e="T334" id="Seg_7981" s="T333">you.SG.NOM</ta>
            <ta e="T335" id="Seg_7982" s="T334">only</ta>
            <ta e="T336" id="Seg_7983" s="T335">house.[NOM]</ta>
            <ta e="T337" id="Seg_7984" s="T336">come.in-IMP.2SG.S</ta>
            <ta e="T338" id="Seg_7985" s="T337">this</ta>
            <ta e="T339" id="Seg_7986" s="T338">door-GEN</ta>
            <ta e="T340" id="Seg_7987" s="T339">opening-LOC</ta>
            <ta e="T341" id="Seg_7988" s="T340">also</ta>
            <ta e="T342" id="Seg_7989" s="T341">idol.[NOM]</ta>
            <ta e="T343" id="Seg_7990" s="T342">make-INFER-3SG.O</ta>
            <ta e="T344" id="Seg_7991" s="T343">you.SG.NOM</ta>
            <ta e="T345" id="Seg_7992" s="T344">here</ta>
            <ta e="T346" id="Seg_7993" s="T345">I.ACC</ta>
            <ta e="T347" id="Seg_7994" s="T346">NEG.IMP</ta>
            <ta e="T348" id="Seg_7995" s="T347">I.ACC</ta>
            <ta e="T349" id="Seg_7996" s="T348">say-IMP.2SG.S</ta>
            <ta e="T350" id="Seg_7997" s="T349">pike-INSTR</ta>
            <ta e="T351" id="Seg_7998" s="T350">threaten-CO-3SG.O</ta>
            <ta e="T352" id="Seg_7999" s="T351">house.[NOM]</ta>
            <ta e="T353" id="Seg_8000" s="T352">here</ta>
            <ta e="T354" id="Seg_8001" s="T353">come.in.[3SG.S]</ta>
            <ta e="T355" id="Seg_8002" s="T354">that</ta>
            <ta e="T356" id="Seg_8003" s="T355">human.being.[NOM]</ta>
            <ta e="T357" id="Seg_8004" s="T356">really</ta>
            <ta e="T358" id="Seg_8005" s="T357">all</ta>
            <ta e="T359" id="Seg_8006" s="T358">INFER</ta>
            <ta e="T360" id="Seg_8007" s="T359">sight-PFV-INFER-3PL</ta>
            <ta e="T361" id="Seg_8008" s="T360">EMPH</ta>
            <ta e="T362" id="Seg_8009" s="T361">it.is.said</ta>
            <ta e="T363" id="Seg_8010" s="T362">what.for</ta>
            <ta e="T364" id="Seg_8011" s="T363">come.in-PRS.[3SG.S]</ta>
            <ta e="T365" id="Seg_8012" s="T364">NEG</ta>
            <ta e="T366" id="Seg_8013" s="T365">it.is.said</ta>
            <ta e="T367" id="Seg_8014" s="T366">human.being.[NOM]-3SG</ta>
            <ta e="T368" id="Seg_8015" s="T367">house-ILL</ta>
            <ta e="T369" id="Seg_8016" s="T368">I.ACC</ta>
            <ta e="T370" id="Seg_8017" s="T369">sent-PST.[3SG.S]</ta>
            <ta e="T371" id="Seg_8018" s="T370">DEF</ta>
            <ta e="T372" id="Seg_8019" s="T371">what-EP-ADJZ</ta>
            <ta e="T373" id="Seg_8020" s="T372">human.being.[NOM]</ta>
            <ta e="T374" id="Seg_8021" s="T373">come-PST.[3SG.S]</ta>
            <ta e="T375" id="Seg_8022" s="T374">I.ACC</ta>
            <ta e="T376" id="Seg_8023" s="T375">house.[NOM]</ta>
            <ta e="T377" id="Seg_8024" s="T376">I.ACC</ta>
            <ta e="T378" id="Seg_8025" s="T377">sent-PST.[3SG.S]</ta>
            <ta e="T379" id="Seg_8026" s="T378">upwards</ta>
            <ta e="T380" id="Seg_8027" s="T379">I.ACC</ta>
            <ta e="T381" id="Seg_8028" s="T380">bring-CAUS-PST.[3SG.S]</ta>
            <ta e="T382" id="Seg_8029" s="T381">(s)he.[NOM]</ta>
            <ta e="T383" id="Seg_8030" s="T382">hardly</ta>
            <ta e="T384" id="Seg_8031" s="T383">sight-PRS-3SG.O</ta>
            <ta e="T385" id="Seg_8032" s="T384">door-GEN</ta>
            <ta e="T386" id="Seg_8033" s="T385">forward-ADJZ</ta>
            <ta e="T387" id="Seg_8034" s="T386">side-LOC</ta>
            <ta e="T388" id="Seg_8035" s="T387">also</ta>
            <ta e="T389" id="Seg_8036" s="T388">one</ta>
            <ta e="T390" id="Seg_8037" s="T389">nude</ta>
            <ta e="T391" id="Seg_8038" s="T390">human.being.[NOM]</ta>
            <ta e="T392" id="Seg_8039" s="T391">appear-INFER.[3SG.S]</ta>
            <ta e="T393" id="Seg_8040" s="T392">then</ta>
            <ta e="T394" id="Seg_8041" s="T393">what.[NOM]</ta>
            <ta e="T395" id="Seg_8042" s="T394">such-ADVZ</ta>
            <ta e="T396" id="Seg_8043" s="T395">say.[3SG.S]</ta>
            <ta e="T397" id="Seg_8044" s="T396">human.being-EP-ACC</ta>
            <ta e="T398" id="Seg_8045" s="T397">up</ta>
            <ta e="T399" id="Seg_8046" s="T398">dress-TR-3PL</ta>
            <ta e="T400" id="Seg_8047" s="T399">then</ta>
            <ta e="T401" id="Seg_8048" s="T400">outwards</ta>
            <ta e="T402" id="Seg_8049" s="T401">go.out-CVB</ta>
            <ta e="T403" id="Seg_8050" s="T402">hear-HAB-DUR-FRQ-IPFV-HAB-3SG.O</ta>
            <ta e="T404" id="Seg_8051" s="T403">devil.[NOM]</ta>
            <ta e="T405" id="Seg_8052" s="T404">who-ADJZ</ta>
            <ta e="T406" id="Seg_8053" s="T405">extent-LOC</ta>
            <ta e="T407" id="Seg_8054" s="T406">appear-US-DUR.[3SG.S]</ta>
            <ta e="T408" id="Seg_8055" s="T407">devil.[NOM]</ta>
            <ta e="T409" id="Seg_8056" s="T408">if</ta>
            <ta e="T410" id="Seg_8057" s="T409">appear-US-PRS.[3SG.S]</ta>
            <ta e="T411" id="Seg_8058" s="T410">house.[NOM]</ta>
            <ta e="T412" id="Seg_8059" s="T411">come.in-IMP.2PL</ta>
            <ta e="T413" id="Seg_8060" s="T412">one</ta>
            <ta e="T414" id="Seg_8061" s="T413">human.being.[NOM]</ta>
            <ta e="T415" id="Seg_8062" s="T414">hear-HAB-HAB-IMP.3SG.S</ta>
            <ta e="T416" id="Seg_8063" s="T415">it.is.said</ta>
            <ta e="T417" id="Seg_8064" s="T416">evening-ADJZ</ta>
            <ta e="T418" id="Seg_8065" s="T417">dawn.[NOM]-3SG</ta>
            <ta e="T419" id="Seg_8066" s="T418">put.on-EP-US-HAB-INFER.[3SG.S]</ta>
            <ta e="T420" id="Seg_8067" s="T419">then</ta>
            <ta e="T421" id="Seg_8068" s="T420">go.out-DUR-FRQ-IPFV-HAB.[3SG.S]</ta>
            <ta e="T422" id="Seg_8069" s="T421">appear-US-IPFV-INFER.[3SG.S]</ta>
            <ta e="T423" id="Seg_8070" s="T422">enough</ta>
            <ta e="T424" id="Seg_8071" s="T423">be-INFER.[3SG.S]</ta>
            <ta e="T425" id="Seg_8072" s="T424">one</ta>
            <ta e="T426" id="Seg_8073" s="T425">whole</ta>
            <ta e="T427" id="Seg_8074" s="T426">middle-LOC</ta>
            <ta e="T428" id="Seg_8075" s="T427">human.being.[NOM]</ta>
            <ta e="T429" id="Seg_8076" s="T428">INFER</ta>
            <ta e="T430" id="Seg_8077" s="T429">go.out-INFER.[3SG.S]</ta>
            <ta e="T431" id="Seg_8078" s="T430">it.is.said</ta>
            <ta e="T432" id="Seg_8079" s="T431">morning-ADV.LOC</ta>
            <ta e="T433" id="Seg_8080" s="T432">INFER</ta>
            <ta e="T434" id="Seg_8081" s="T433">appear-US-INFER.[3SG.S]</ta>
            <ta e="T435" id="Seg_8082" s="T434">house-ILL</ta>
            <ta e="T436" id="Seg_8083" s="T435">get.into-MULO-PRS.[3SG.S]</ta>
            <ta e="T437" id="Seg_8084" s="T436">devil.[NOM]</ta>
            <ta e="T438" id="Seg_8085" s="T437">INFER</ta>
            <ta e="T439" id="Seg_8086" s="T438">appear-US-INFER.[3SG.S]</ta>
            <ta e="T440" id="Seg_8087" s="T439">but</ta>
            <ta e="T441" id="Seg_8088" s="T440">it.is.said</ta>
            <ta e="T442" id="Seg_8089" s="T441">further</ta>
            <ta e="T443" id="Seg_8090" s="T442">go.quiet-VBLZ</ta>
            <ta e="T444" id="Seg_8091" s="T443">sit-IMP.2PL</ta>
            <ta e="T445" id="Seg_8092" s="T444">NEG</ta>
            <ta e="T446" id="Seg_8093" s="T445">when</ta>
            <ta e="T447" id="Seg_8094" s="T446">NEG.IMP</ta>
            <ta e="T448" id="Seg_8095" s="T447">go.out-IMP.2PL</ta>
            <ta e="T449" id="Seg_8096" s="T448">Sylcha_Pylcha.[NOM]-3SG</ta>
            <ta e="T450" id="Seg_8097" s="T449">half.[NOM]</ta>
            <ta e="T451" id="Seg_8098" s="T450">hear-HAB-HAB-3SG.O</ta>
            <ta e="T452" id="Seg_8099" s="T451">what.[NOM]</ta>
            <ta e="T453" id="Seg_8100" s="T452">upwards</ta>
            <ta e="T454" id="Seg_8101" s="T453">INFER</ta>
            <ta e="T455" id="Seg_8102" s="T454">go.out-IPFV-PRS.[3SG.S]</ta>
            <ta e="T456" id="Seg_8103" s="T455">go.out-INFER.[3SG.S]</ta>
            <ta e="T457" id="Seg_8104" s="T456">hazelhen-ACC</ta>
            <ta e="T458" id="Seg_8105" s="T457">sight-PFV-3SG.O</ta>
            <ta e="T459" id="Seg_8106" s="T458">hazelhen.[NOM]</ta>
            <ta e="T460" id="Seg_8107" s="T459">weet</ta>
            <ta e="T461" id="Seg_8108" s="T460">weet</ta>
            <ta e="T462" id="Seg_8109" s="T461">whether</ta>
            <ta e="T463" id="Seg_8110" s="T462">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T464" id="Seg_8111" s="T463">half.[NOM]</ta>
            <ta e="T465" id="Seg_8112" s="T464">where.to.go-PST.[3SG.S]</ta>
            <ta e="T466" id="Seg_8113" s="T465">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T467" id="Seg_8114" s="T466">half.[NOM]</ta>
            <ta e="T468" id="Seg_8115" s="T467">lately</ta>
            <ta e="T469" id="Seg_8116" s="T468">go.away-PST.[3SG.S]</ta>
            <ta e="T470" id="Seg_8117" s="T469">upwards</ta>
            <ta e="T471" id="Seg_8118" s="T470">INFER</ta>
            <ta e="T472" id="Seg_8119" s="T471">come-INFER.[3SG.S]</ta>
            <ta e="T473" id="Seg_8120" s="T472">idol-ACC</ta>
            <ta e="T474" id="Seg_8121" s="T473">sight-PRS-3SG.O</ta>
            <ta e="T475" id="Seg_8122" s="T474">idol.[NOM]</ta>
            <ta e="T476" id="Seg_8123" s="T475">say-3SG.O</ta>
            <ta e="T477" id="Seg_8124" s="T476">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T478" id="Seg_8125" s="T477">half.[NOM]</ta>
            <ta e="T479" id="Seg_8126" s="T478">house-LOC</ta>
            <ta e="T480" id="Seg_8127" s="T479">sit.[3SG.S]</ta>
            <ta e="T481" id="Seg_8128" s="T480">devil.[NOM]</ta>
            <ta e="T482" id="Seg_8129" s="T481">down</ta>
            <ta e="T483" id="Seg_8130" s="T482">turn-DECAUS-CVB</ta>
            <ta e="T484" id="Seg_8131" s="T483">go-INCH-PRS.[3SG.S]</ta>
            <ta e="T485" id="Seg_8132" s="T484">go.away-PRS.[3SG.S]</ta>
            <ta e="T486" id="Seg_8133" s="T485">water-ILL</ta>
            <ta e="T487" id="Seg_8134" s="T486">fall.[3SG.S]</ta>
            <ta e="T488" id="Seg_8135" s="T487">as.if</ta>
            <ta e="T489" id="Seg_8136" s="T488">glug</ta>
            <ta e="T490" id="Seg_8137" s="T489">drip.drop</ta>
            <ta e="T491" id="Seg_8138" s="T490">outwards</ta>
            <ta e="T492" id="Seg_8139" s="T491">run-CVB</ta>
            <ta e="T493" id="Seg_8140" s="T492">that</ta>
            <ta e="T494" id="Seg_8141" s="T493">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T495" id="Seg_8142" s="T494">half.[NOM]</ta>
            <ta e="T496" id="Seg_8143" s="T495">idol-ACC-3SG</ta>
            <ta e="T497" id="Seg_8144" s="T496">chop-US-CVB</ta>
            <ta e="T498" id="Seg_8145" s="T497">break-CAUS-3SG.O</ta>
            <ta e="T499" id="Seg_8146" s="T498">away</ta>
            <ta e="T500" id="Seg_8147" s="T499">hit-MOM-PRS-3SG.O</ta>
            <ta e="T501" id="Seg_8148" s="T500">other</ta>
            <ta e="T502" id="Seg_8149" s="T501">idol-ACC</ta>
            <ta e="T503" id="Seg_8150" s="T502">make-PRS-3SG.O</ta>
            <ta e="T504" id="Seg_8151" s="T503">in.the.face</ta>
            <ta e="T505" id="Seg_8152" s="T504">threaten-HAB-SO-3SG.O</ta>
            <ta e="T506" id="Seg_8153" s="T505">you.SG.NOM</ta>
            <ta e="T507" id="Seg_8154" s="T506">here</ta>
            <ta e="T508" id="Seg_8155" s="T507">I.ACC</ta>
            <ta e="T509" id="Seg_8156" s="T508">NEG</ta>
            <ta e="T510" id="Seg_8157" s="T509">when</ta>
            <ta e="T511" id="Seg_8158" s="T510">how.many-ADV.EL</ta>
            <ta e="T512" id="Seg_8159" s="T511">NEG.IMP</ta>
            <ta e="T513" id="Seg_8160" s="T512">say-IMP.2SG.S</ta>
            <ta e="T514" id="Seg_8161" s="T513">now</ta>
            <ta e="T515" id="Seg_8162" s="T514">go.out-DUR-FRQ-IPFV-HAB.[3SG.S]</ta>
            <ta e="T516" id="Seg_8163" s="T515">morning-GEN-EP-ADJZ</ta>
            <ta e="T517" id="Seg_8164" s="T516">night-ADV.LOC</ta>
            <ta e="T518" id="Seg_8165" s="T517">then</ta>
            <ta e="T519" id="Seg_8166" s="T518">night-GEN-3SG</ta>
            <ta e="T520" id="Seg_8167" s="T519">during</ta>
            <ta e="T521" id="Seg_8168" s="T520">INFER</ta>
            <ta e="T522" id="Seg_8169" s="T521">sit-INFER-3PL</ta>
            <ta e="T523" id="Seg_8170" s="T522">morning-GEN-EP-ADJZ</ta>
            <ta e="T524" id="Seg_8171" s="T523">night-ADV.LOC</ta>
            <ta e="T525" id="Seg_8172" s="T524">human.being-EP-ACC</ta>
            <ta e="T526" id="Seg_8173" s="T525">go-CAUS-3PL</ta>
            <ta e="T527" id="Seg_8174" s="T526">outwards</ta>
            <ta e="T528" id="Seg_8175" s="T527">give.a.look-HAB-3PL</ta>
            <ta e="T529" id="Seg_8176" s="T528">if</ta>
            <ta e="T530" id="Seg_8177" s="T529">NEG</ta>
            <ta e="T531" id="Seg_8178" s="T530">go.out.[3SG.S]</ta>
            <ta e="T532" id="Seg_8179" s="T531">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T533" id="Seg_8180" s="T532">half.[NOM]</ta>
            <ta e="T534" id="Seg_8181" s="T533">go-CAUS-3PL</ta>
            <ta e="T535" id="Seg_8182" s="T534">hear-HAB-HAB-IMP.2SG.O</ta>
            <ta e="T536" id="Seg_8183" s="T535">enough</ta>
            <ta e="T537" id="Seg_8184" s="T536">human.being.[NOM]</ta>
            <ta e="T538" id="Seg_8185" s="T537">INFER</ta>
            <ta e="T539" id="Seg_8186" s="T538">quiet-ADVZ</ta>
            <ta e="T540" id="Seg_8187" s="T539">be-INFER.[3SG.S]</ta>
            <ta e="T541" id="Seg_8188" s="T540">house-ILL</ta>
            <ta e="T542" id="Seg_8189" s="T541">this</ta>
            <ta e="T543" id="Seg_8190" s="T542">fall-INFER.[3SG.S]</ta>
            <ta e="T544" id="Seg_8191" s="T543">as.if</ta>
            <ta e="T545" id="Seg_8192" s="T544">one</ta>
            <ta e="T546" id="Seg_8193" s="T545">devil.[NOM]</ta>
            <ta e="T547" id="Seg_8194" s="T546">appear-US-PST.NAR.[3SG.S]</ta>
            <ta e="T548" id="Seg_8195" s="T547">devil.[NOM]</ta>
            <ta e="T549" id="Seg_8196" s="T548">INFER</ta>
            <ta e="T550" id="Seg_8197" s="T549">appear-US-PST.NAR.[3SG.S]</ta>
            <ta e="T551" id="Seg_8198" s="T550">supposedly</ta>
            <ta e="T552" id="Seg_8199" s="T551">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T553" id="Seg_8200" s="T552">himself.[NOM]</ta>
            <ta e="T554" id="Seg_8201" s="T553">hear-HAB-PST.NAR-3SG.O</ta>
            <ta e="T555" id="Seg_8202" s="T554">INFER</ta>
            <ta e="T556" id="Seg_8203" s="T555">go.out-INFER.[3SG.S]</ta>
            <ta e="T557" id="Seg_8204" s="T556">upwards</ta>
            <ta e="T558" id="Seg_8205" s="T557">go-CVB</ta>
            <ta e="T559" id="Seg_8206" s="T558">(s)he.[NOM]</ta>
            <ta e="T560" id="Seg_8207" s="T559">come-DUR-PRS.[3SG.S]</ta>
            <ta e="T561" id="Seg_8208" s="T560">go-CVB</ta>
            <ta e="T562" id="Seg_8209" s="T561">(s)he.[NOM]</ta>
            <ta e="T563" id="Seg_8210" s="T562">come-DUR-PRS.[3SG.S]</ta>
            <ta e="T564" id="Seg_8211" s="T563">this</ta>
            <ta e="T565" id="Seg_8212" s="T564">hill-GEN</ta>
            <ta e="T566" id="Seg_8213" s="T565">top-EP-ILL</ta>
            <ta e="T567" id="Seg_8214" s="T566">INFER</ta>
            <ta e="T568" id="Seg_8215" s="T567">go.out-INFER.[3SG.S]</ta>
            <ta e="T569" id="Seg_8216" s="T568">that.[NOM]</ta>
            <ta e="T570" id="Seg_8217" s="T569">idol-ILL</ta>
            <ta e="T571" id="Seg_8218" s="T570">go-CVB</ta>
            <ta e="T572" id="Seg_8219" s="T571">INFER</ta>
            <ta e="T573" id="Seg_8220" s="T572">come-INFER.[3SG.S]</ta>
            <ta e="T574" id="Seg_8221" s="T573">as.if</ta>
            <ta e="T575" id="Seg_8222" s="T574">weet</ta>
            <ta e="T576" id="Seg_8223" s="T575">weet</ta>
            <ta e="T577" id="Seg_8224" s="T576">weet</ta>
            <ta e="T578" id="Seg_8225" s="T577">whether</ta>
            <ta e="T579" id="Seg_8226" s="T578">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T580" id="Seg_8227" s="T579">half.[NOM]</ta>
            <ta e="T581" id="Seg_8228" s="T580">where.to.go-PRS.[3SG.S]</ta>
            <ta e="T582" id="Seg_8229" s="T581">whether</ta>
            <ta e="T583" id="Seg_8230" s="T582">house-LOC</ta>
            <ta e="T584" id="Seg_8231" s="T583">be-PRS.[3SG.S]</ta>
            <ta e="T585" id="Seg_8232" s="T584">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T586" id="Seg_8233" s="T585">half.[NOM]</ta>
            <ta e="T587" id="Seg_8234" s="T586">then</ta>
            <ta e="T588" id="Seg_8235" s="T587">go.away-PST.[3SG.S]</ta>
            <ta e="T589" id="Seg_8236" s="T588">you.SG.NOM</ta>
            <ta e="T590" id="Seg_8237" s="T589">upwards</ta>
            <ta e="T591" id="Seg_8238" s="T590">go.away-CVB</ta>
            <ta e="T592" id="Seg_8239" s="T591">food-ADJZ</ta>
            <ta e="T593" id="Seg_8240" s="T592">eat-IMP.2SG.O</ta>
            <ta e="T594" id="Seg_8241" s="T593">that.[NOM]</ta>
            <ta e="T595" id="Seg_8242" s="T594">go-CVB</ta>
            <ta e="T596" id="Seg_8243" s="T595">INFER</ta>
            <ta e="T597" id="Seg_8244" s="T596">go.away-INFER.[3SG.S]-EMPH</ta>
            <ta e="T598" id="Seg_8245" s="T597">door-GEN</ta>
            <ta e="T599" id="Seg_8246" s="T598">opening-EP-ADJZ</ta>
            <ta e="T600" id="Seg_8247" s="T599">come-INFER.[3SG.S]-EMPH</ta>
            <ta e="T601" id="Seg_8248" s="T600">idol-ILL</ta>
            <ta e="T602" id="Seg_8249" s="T601">come-PRS.[3SG.S]</ta>
            <ta e="T603" id="Seg_8250" s="T602">where.to.go-PST.[3SG.S]</ta>
            <ta e="T604" id="Seg_8251" s="T603">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T605" id="Seg_8252" s="T604">half.[NOM]</ta>
            <ta e="T606" id="Seg_8253" s="T605">(s)he.[NOM]</ta>
            <ta e="T608" id="Seg_8254" s="T607">yesterday</ta>
            <ta e="T609" id="Seg_8255" s="T608">then</ta>
            <ta e="T610" id="Seg_8256" s="T609">go.away-PST.[3SG.S]</ta>
            <ta e="T611" id="Seg_8257" s="T610">leave-US-EP-PRS.[3SG.S]</ta>
            <ta e="T612" id="Seg_8258" s="T611">house-ILL</ta>
            <ta e="T613" id="Seg_8259" s="T612">come.in-CVB</ta>
            <ta e="T614" id="Seg_8260" s="T613">food-ADJZ</ta>
            <ta e="T615" id="Seg_8261" s="T614">eat-IMP.2SG.O</ta>
            <ta e="T616" id="Seg_8262" s="T615">one</ta>
            <ta e="T617" id="Seg_8263" s="T616">middle-LOC</ta>
            <ta e="T618" id="Seg_8264" s="T617">devil.[NOM]</ta>
            <ta e="T619" id="Seg_8265" s="T618">house-ILL</ta>
            <ta e="T620" id="Seg_8266" s="T619">INFER</ta>
            <ta e="T621" id="Seg_8267" s="T620">push-MOM-INFER.[3SG.S]</ta>
            <ta e="T622" id="Seg_8268" s="T621">house-ILL</ta>
            <ta e="T623" id="Seg_8269" s="T622">INFER</ta>
            <ta e="T624" id="Seg_8270" s="T623">push-MOM-INFER.[3SG.S]</ta>
            <ta e="T625" id="Seg_8271" s="T624">house-ILL</ta>
            <ta e="T626" id="Seg_8272" s="T625">only</ta>
            <ta e="T627" id="Seg_8273" s="T626">come.in-PRS.[3SG.S]</ta>
            <ta e="T628" id="Seg_8274" s="T627">devil.[NOM]</ta>
            <ta e="T629" id="Seg_8275" s="T628">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T630" id="Seg_8276" s="T629">half.[NOM]</ta>
            <ta e="T631" id="Seg_8277" s="T630">chop-MOM-PRS-3SG.O</ta>
            <ta e="T632" id="Seg_8278" s="T631">head-GEN-3SG</ta>
            <ta e="T633" id="Seg_8279" s="T632">piece.[NOM]</ta>
            <ta e="T634" id="Seg_8280" s="T633">house-ILL</ta>
            <ta e="T635" id="Seg_8281" s="T634">through</ta>
            <ta e="T636" id="Seg_8282" s="T635">forward</ta>
            <ta e="T637" id="Seg_8283" s="T636">roll-FRQ-PRS.[3SG.S]</ta>
            <ta e="T638" id="Seg_8284" s="T637">body-GEN-3SG</ta>
            <ta e="T639" id="Seg_8285" s="T638">piece.[NOM]</ta>
            <ta e="T640" id="Seg_8286" s="T639">backwards</ta>
            <ta e="T641" id="Seg_8287" s="T640">outwards</ta>
            <ta e="T642" id="Seg_8288" s="T641">fall.[3SG.S]</ta>
            <ta e="T643" id="Seg_8289" s="T642">kill-TR-INFER-3SG.O</ta>
            <ta e="T644" id="Seg_8290" s="T643">outwards</ta>
            <ta e="T645" id="Seg_8291" s="T644">bring-PRS-3SG.O</ta>
            <ta e="T646" id="Seg_8292" s="T645">that</ta>
            <ta e="T647" id="Seg_8293" s="T646">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T648" id="Seg_8294" s="T647">half.[NOM]</ta>
            <ta e="T649" id="Seg_8295" s="T648">body-GEN-3SG</ta>
            <ta e="T650" id="Seg_8296" s="T649">piece.[NOM]</ta>
            <ta e="T651" id="Seg_8297" s="T650">outwards</ta>
            <ta e="T652" id="Seg_8298" s="T651">go.out.[3SG.S]</ta>
            <ta e="T653" id="Seg_8299" s="T652">apparently</ta>
            <ta e="T654" id="Seg_8300" s="T653">day-VBLZ-PFV.[3SG.S]</ta>
            <ta e="T655" id="Seg_8301" s="T654">down</ta>
            <ta e="T656" id="Seg_8302" s="T655">bring-HAB-PRS-3SG.O</ta>
            <ta e="T657" id="Seg_8303" s="T656">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T658" id="Seg_8304" s="T657">half.[NOM]</ta>
            <ta e="T659" id="Seg_8305" s="T658">body-GEN-3SG</ta>
            <ta e="T660" id="Seg_8306" s="T659">piece-ACC</ta>
            <ta e="T661" id="Seg_8307" s="T660">water-EP-ADJZ</ta>
            <ta e="T662" id="Seg_8308" s="T661">ice.hole-ILL</ta>
            <ta e="T663" id="Seg_8309" s="T662">put-PRS-3SG.O</ta>
            <ta e="T664" id="Seg_8310" s="T663">ice-COM</ta>
            <ta e="T665" id="Seg_8311" s="T664">together</ta>
            <ta e="T666" id="Seg_8312" s="T665">freeze-TR-3SG.O</ta>
            <ta e="T667" id="Seg_8313" s="T666">such-ADVZ</ta>
            <ta e="T668" id="Seg_8314" s="T667">put-PRS-3SG.O</ta>
            <ta e="T669" id="Seg_8315" s="T668">water-EP-ADJZ</ta>
            <ta e="T670" id="Seg_8316" s="T669">ice.hole-GEN-3SG</ta>
            <ta e="T671" id="Seg_8317" s="T670">near-ILL</ta>
            <ta e="T672" id="Seg_8318" s="T671">such-ADVZ</ta>
            <ta e="T673" id="Seg_8319" s="T672">put-PRS-3SG.O</ta>
            <ta e="T674" id="Seg_8320" s="T673">as.if</ta>
            <ta e="T675" id="Seg_8321" s="T674">as.if</ta>
            <ta e="T676" id="Seg_8322" s="T675">live-CVB</ta>
            <ta e="T677" id="Seg_8323" s="T676">lie.[3SG.S]</ta>
            <ta e="T678" id="Seg_8324" s="T677">head-OBL.3SG-COM</ta>
            <ta e="T679" id="Seg_8325" s="T678">together</ta>
            <ta e="T680" id="Seg_8326" s="T679">stick.up-TR-PRS-3SG.O</ta>
            <ta e="T681" id="Seg_8327" s="T680">here</ta>
            <ta e="T682" id="Seg_8328" s="T681">day-TR-PRS-1PL</ta>
            <ta e="T683" id="Seg_8329" s="T682">darkness-VBLZ-EP-DECAUS.[3SG.S]</ta>
            <ta e="T684" id="Seg_8330" s="T683">it.is.said</ta>
            <ta e="T685" id="Seg_8331" s="T684">only</ta>
            <ta e="T686" id="Seg_8332" s="T685">two-DU-ADJZ</ta>
            <ta e="T687" id="Seg_8333" s="T686">other</ta>
            <ta e="T688" id="Seg_8334" s="T687">something.[NOM]</ta>
            <ta e="T689" id="Seg_8335" s="T688">go-INF</ta>
            <ta e="T690" id="Seg_8336" s="T689">NEG</ta>
            <ta e="T691" id="Seg_8337" s="T690">know-1PL</ta>
            <ta e="T692" id="Seg_8338" s="T691">evening-ADV.LOC</ta>
            <ta e="T693" id="Seg_8339" s="T692">here</ta>
            <ta e="T694" id="Seg_8340" s="T693">darkness-VBLZ-EP-DECAUS-PRS.[3SG.S]</ta>
            <ta e="T695" id="Seg_8341" s="T694">evening-ADV.LOC</ta>
            <ta e="T696" id="Seg_8342" s="T695">enough</ta>
            <ta e="T697" id="Seg_8343" s="T696">sit.[3SG.S]</ta>
            <ta e="T698" id="Seg_8344" s="T697">here</ta>
            <ta e="T699" id="Seg_8345" s="T698">gloomy.VBLZ-DECAUS-INF-begin-PRS.[3SG.S]</ta>
            <ta e="T700" id="Seg_8346" s="T699">human.being-EP-ACC</ta>
            <ta e="T701" id="Seg_8347" s="T700">outwards</ta>
            <ta e="T702" id="Seg_8348" s="T701">go-TR-3SG.O</ta>
            <ta e="T703" id="Seg_8349" s="T702">hear-HAB-HAB-IMP.2SG.O</ta>
            <ta e="T704" id="Seg_8350" s="T703">human.being.[NOM]</ta>
            <ta e="T705" id="Seg_8351" s="T704">space.outside-LOC</ta>
            <ta e="T706" id="Seg_8352" s="T705">stand.[3SG.S]</ta>
            <ta e="T707" id="Seg_8353" s="T706">oh</ta>
            <ta e="T708" id="Seg_8354" s="T707">human.being.[NOM]</ta>
            <ta e="T709" id="Seg_8355" s="T708">here</ta>
            <ta e="T710" id="Seg_8356" s="T709">appear-US-INF-begin-PRS.[3SG.S]</ta>
            <ta e="T711" id="Seg_8357" s="T710">house-ILL</ta>
            <ta e="T712" id="Seg_8358" s="T711">get.into-MULO-PRS.[3SG.S]</ta>
            <ta e="T713" id="Seg_8359" s="T712">house-ILL</ta>
            <ta e="T714" id="Seg_8360" s="T713">come.in-CVB</ta>
            <ta e="T715" id="Seg_8361" s="T714">such-ADVZ</ta>
            <ta e="T716" id="Seg_8362" s="T715">say-PST.NAR-3SG.O</ta>
            <ta e="T717" id="Seg_8363" s="T716">devil.[NOM]</ta>
            <ta e="T718" id="Seg_8364" s="T717">down-ADV.LOC</ta>
            <ta e="T719" id="Seg_8365" s="T718">appear-US-PRS.[3SG.S]</ta>
            <ta e="T720" id="Seg_8366" s="T719">upwards</ta>
            <ta e="T721" id="Seg_8367" s="T720">go-US-CVB</ta>
            <ta e="T722" id="Seg_8368" s="T721">INFER</ta>
            <ta e="T723" id="Seg_8369" s="T722">go.out-INFER.[3SG.S]</ta>
            <ta e="T724" id="Seg_8370" s="T723">upwards</ta>
            <ta e="T725" id="Seg_8371" s="T724">what.[NOM]</ta>
            <ta e="T726" id="Seg_8372" s="T725">appear-US-HAB.[3SG.S]</ta>
            <ta e="T727" id="Seg_8373" s="T726">ah</ta>
            <ta e="T728" id="Seg_8374" s="T727">you.SG.NOM</ta>
            <ta e="T729" id="Seg_8375" s="T728">whether</ta>
            <ta e="T730" id="Seg_8376" s="T729">fall.asleep-DECAUS-EP-2SG.S</ta>
            <ta e="T731" id="Seg_8377" s="T730">eat-EP-FRQ-PFV-PRS-2SG.S</ta>
            <ta e="T732" id="Seg_8378" s="T731">be.frightened-DECAUS-EP-PRS-2SG.S</ta>
            <ta e="T733" id="Seg_8379" s="T732">one</ta>
            <ta e="T734" id="Seg_8380" s="T733">somebody-ACC-3SG</ta>
            <ta e="T735" id="Seg_8381" s="T734">such-ADVZ</ta>
            <ta e="T736" id="Seg_8382" s="T735">sight-PRS-3SG.O</ta>
            <ta e="T737" id="Seg_8383" s="T736">I.NOM</ta>
            <ta e="T738" id="Seg_8384" s="T737">earlier</ta>
            <ta e="T739" id="Seg_8385" s="T738">eat-EP-FRQ-PFV-CVB</ta>
            <ta e="T740" id="Seg_8386" s="T739">such-ADVZ</ta>
            <ta e="T741" id="Seg_8387" s="T740">lie-DUR-FRQ-IPFV-HAB-DUR-1SG.S</ta>
            <ta e="T742" id="Seg_8388" s="T741">then</ta>
            <ta e="T743" id="Seg_8389" s="T742">upwards</ta>
            <ta e="T744" id="Seg_8390" s="T743">go-CVB</ta>
            <ta e="T745" id="Seg_8391" s="T744">INFER</ta>
            <ta e="T746" id="Seg_8392" s="T745">come-INFER.[3SG.S]</ta>
            <ta e="T747" id="Seg_8393" s="T746">upwards</ta>
            <ta e="T748" id="Seg_8394" s="T747">go-CVB</ta>
            <ta e="T749" id="Seg_8395" s="T748">INFER</ta>
            <ta e="T750" id="Seg_8396" s="T749">come-INFER.[3SG.S]</ta>
            <ta e="T751" id="Seg_8397" s="T750">weet</ta>
            <ta e="T752" id="Seg_8398" s="T751">weet</ta>
            <ta e="T753" id="Seg_8399" s="T752">whether</ta>
            <ta e="T754" id="Seg_8400" s="T753">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T755" id="Seg_8401" s="T754">half.[NOM]</ta>
            <ta e="T756" id="Seg_8402" s="T755">where.to.go-PFV.[3SG.S]</ta>
            <ta e="T757" id="Seg_8403" s="T756">come-PRS.[3SG.S]</ta>
            <ta e="T758" id="Seg_8404" s="T757">either.or</ta>
            <ta e="T759" id="Seg_8405" s="T758">go.away-PST.[3SG.S]</ta>
            <ta e="T760" id="Seg_8406" s="T759">either.or</ta>
            <ta e="T761" id="Seg_8407" s="T760">where.to.go-PST.[3SG.S]</ta>
            <ta e="T762" id="Seg_8408" s="T761">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T763" id="Seg_8409" s="T762">half.[NOM]</ta>
            <ta e="T764" id="Seg_8410" s="T763">earlier</ta>
            <ta e="T765" id="Seg_8411" s="T764">then</ta>
            <ta e="T766" id="Seg_8412" s="T765">go.away-PST.[3SG.S]</ta>
            <ta e="T767" id="Seg_8413" s="T766">lately</ta>
            <ta e="T768" id="Seg_8414" s="T767">then</ta>
            <ta e="T769" id="Seg_8415" s="T768">go.away-PST.[3SG.S]</ta>
            <ta e="T770" id="Seg_8416" s="T769">you.SG.NOM</ta>
            <ta e="T771" id="Seg_8417" s="T770">upwards</ta>
            <ta e="T772" id="Seg_8418" s="T771">go.away-CVB</ta>
            <ta e="T773" id="Seg_8419" s="T772">food-ADJZ</ta>
            <ta e="T774" id="Seg_8420" s="T773">eat-IMP.2SG.O</ta>
            <ta e="T775" id="Seg_8421" s="T774">then</ta>
            <ta e="T776" id="Seg_8422" s="T775">upwards</ta>
            <ta e="T777" id="Seg_8423" s="T776">go.away-CVB</ta>
            <ta e="T778" id="Seg_8424" s="T777">go-CVB</ta>
            <ta e="T779" id="Seg_8425" s="T778">that</ta>
            <ta e="T780" id="Seg_8426" s="T779">idol-ILL</ta>
            <ta e="T781" id="Seg_8427" s="T780">come-PRS.[3SG.S]</ta>
            <ta e="T782" id="Seg_8428" s="T781">say-3SG.O</ta>
            <ta e="T783" id="Seg_8429" s="T782">whether</ta>
            <ta e="T784" id="Seg_8430" s="T783">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T785" id="Seg_8431" s="T784">half.[NOM]</ta>
            <ta e="T786" id="Seg_8432" s="T785">house-LOC</ta>
            <ta e="T787" id="Seg_8433" s="T786">be-PRS.[3SG.S]</ta>
            <ta e="T788" id="Seg_8434" s="T787">(s)he.[NOM]</ta>
            <ta e="T790" id="Seg_8435" s="T789">then</ta>
            <ta e="T791" id="Seg_8436" s="T790">lately</ta>
            <ta e="T792" id="Seg_8437" s="T791">go.away-PST.[3SG.S]</ta>
            <ta e="T793" id="Seg_8438" s="T792">house.[NOM]</ta>
            <ta e="T794" id="Seg_8439" s="T793">come.in-CVB</ta>
            <ta e="T795" id="Seg_8440" s="T794">food-ADJZ</ta>
            <ta e="T796" id="Seg_8441" s="T795">eat-IMP.2SG.O</ta>
            <ta e="T797" id="Seg_8442" s="T796">other</ta>
            <ta e="T798" id="Seg_8443" s="T797">again</ta>
            <ta e="T799" id="Seg_8444" s="T798">such-ADVZ</ta>
            <ta e="T800" id="Seg_8445" s="T799">say.[3SG.S]</ta>
            <ta e="T801" id="Seg_8446" s="T800">house-ILL</ta>
            <ta e="T802" id="Seg_8447" s="T801">either.or</ta>
            <ta e="T803" id="Seg_8448" s="T802">come.in-FUT-1SG.S</ta>
            <ta e="T804" id="Seg_8449" s="T803">either.or</ta>
            <ta e="T805" id="Seg_8450" s="T804">NEG</ta>
            <ta e="T806" id="Seg_8451" s="T805">come.in-FUT-1SG.S</ta>
            <ta e="T807" id="Seg_8452" s="T806">other</ta>
            <ta e="T808" id="Seg_8453" s="T807">again</ta>
            <ta e="T809" id="Seg_8454" s="T808">something.[NOM]</ta>
            <ta e="T810" id="Seg_8455" s="T809">idol.[NOM]</ta>
            <ta e="T811" id="Seg_8456" s="T810">whether</ta>
            <ta e="T812" id="Seg_8457" s="T811">it.is.said</ta>
            <ta e="T813" id="Seg_8458" s="T812">I.ACC</ta>
            <ta e="T814" id="Seg_8459" s="T813">trip.up-HAB-2SG.S</ta>
            <ta e="T815" id="Seg_8460" s="T814">supposedly</ta>
            <ta e="T816" id="Seg_8461" s="T815">guess-HAB.[3SG.S]</ta>
            <ta e="T817" id="Seg_8462" s="T816">guess-HAB-PRS.[3SG.S]</ta>
            <ta e="T818" id="Seg_8463" s="T817">other-ADJZ</ta>
            <ta e="T819" id="Seg_8464" s="T818">either.or</ta>
            <ta e="T820" id="Seg_8465" s="T819">come.in-FUT.[3SG.S]</ta>
            <ta e="T821" id="Seg_8466" s="T820">either.or</ta>
            <ta e="T822" id="Seg_8467" s="T821">NEG</ta>
            <ta e="T823" id="Seg_8468" s="T822">come.in-FUT.[3SG.S]</ta>
            <ta e="T824" id="Seg_8469" s="T823">one</ta>
            <ta e="T825" id="Seg_8470" s="T824">middle-LOC</ta>
            <ta e="T826" id="Seg_8471" s="T825">INFER</ta>
            <ta e="T827" id="Seg_8472" s="T826">come.in-INF-begin-INFER.[3SG.S]</ta>
            <ta e="T828" id="Seg_8473" s="T827">head-ACC-3SG</ta>
            <ta e="T829" id="Seg_8474" s="T828">house-ILL</ta>
            <ta e="T830" id="Seg_8475" s="T829">hardly</ta>
            <ta e="T831" id="Seg_8476" s="T830">push-MOM-PRS-3SG.O</ta>
            <ta e="T832" id="Seg_8477" s="T831">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T833" id="Seg_8478" s="T832">half.[NOM]</ta>
            <ta e="T834" id="Seg_8479" s="T833">chop-MOM-PRS-3SG.O</ta>
            <ta e="T835" id="Seg_8480" s="T834">head-GEN-3SG</ta>
            <ta e="T836" id="Seg_8481" s="T835">piece.[NOM]</ta>
            <ta e="T837" id="Seg_8482" s="T836">house-ILL</ta>
            <ta e="T838" id="Seg_8483" s="T837">forward</ta>
            <ta e="T839" id="Seg_8484" s="T838">fall.[3SG.S]</ta>
            <ta e="T840" id="Seg_8485" s="T839">roll-FRQ-PRS.[3SG.S]</ta>
            <ta e="T841" id="Seg_8486" s="T840">body-GEN-3SG</ta>
            <ta e="T842" id="Seg_8487" s="T841">piece.[NOM]</ta>
            <ta e="T843" id="Seg_8488" s="T842">backwards</ta>
            <ta e="T844" id="Seg_8489" s="T843">outwards</ta>
            <ta e="T845" id="Seg_8490" s="T844">fall.[3SG.S]</ta>
            <ta e="T846" id="Seg_8491" s="T845">kill-PFV-PRS-3SG.O</ta>
            <ta e="T847" id="Seg_8492" s="T846">then</ta>
            <ta e="T848" id="Seg_8493" s="T847">then</ta>
            <ta e="T849" id="Seg_8494" s="T848">only</ta>
            <ta e="T850" id="Seg_8495" s="T849">sit-3PL</ta>
            <ta e="T851" id="Seg_8496" s="T850">sit-3PL</ta>
            <ta e="T852" id="Seg_8497" s="T851">NEG</ta>
            <ta e="T853" id="Seg_8498" s="T852">what.[NOM]</ta>
            <ta e="T854" id="Seg_8499" s="T853">NEG.EX.[3SG.S]</ta>
            <ta e="T855" id="Seg_8500" s="T854">it.is.said</ta>
            <ta e="T856" id="Seg_8501" s="T855">whether</ta>
            <ta e="T857" id="Seg_8502" s="T856">two</ta>
            <ta e="T858" id="Seg_8503" s="T857">go.[3SG.S]</ta>
            <ta e="T859" id="Seg_8504" s="T858">next</ta>
            <ta e="T860" id="Seg_8505" s="T859">morning-ADV.LOC</ta>
            <ta e="T861" id="Seg_8506" s="T860">up</ta>
            <ta e="T862" id="Seg_8507" s="T861">day-VBLZ-PRS.[3SG.S]</ta>
            <ta e="T863" id="Seg_8508" s="T862">it.is.said</ta>
            <ta e="T864" id="Seg_8509" s="T863">firewood.[NOM]</ta>
            <ta e="T865" id="Seg_8510" s="T864">chop-MOM-IMP.2PL</ta>
            <ta e="T866" id="Seg_8511" s="T865">firewood.[NOM]</ta>
            <ta e="T867" id="Seg_8512" s="T866">chop-MOM-PRS-3PL</ta>
            <ta e="T868" id="Seg_8513" s="T867">fire.[NOM]</ta>
            <ta e="T869" id="Seg_8514" s="T868">set.fire-IMP.2PL</ta>
            <ta e="T870" id="Seg_8515" s="T869">really</ta>
            <ta e="T871" id="Seg_8516" s="T870">firewood-ACC</ta>
            <ta e="T872" id="Seg_8517" s="T871">chop-MOM-PRS-3PL</ta>
            <ta e="T873" id="Seg_8518" s="T872">two-DU-ACC</ta>
            <ta e="T874" id="Seg_8519" s="T873">down</ta>
            <ta e="T875" id="Seg_8520" s="T874">bring-HAB-DUR-3PL</ta>
            <ta e="T876" id="Seg_8521" s="T875">fire-INSTR</ta>
            <ta e="T877" id="Seg_8522" s="T876">set.fire-3PL</ta>
            <ta e="T878" id="Seg_8523" s="T877">place-CAR-ADVZ</ta>
            <ta e="T879" id="Seg_8524" s="T878">fire-INSTR</ta>
            <ta e="T880" id="Seg_8525" s="T879">set.fire-3PL</ta>
            <ta e="T881" id="Seg_8526" s="T880">old.man-ADJZ</ta>
            <ta e="T882" id="Seg_8527" s="T881">father-CRC-GEN</ta>
            <ta e="T883" id="Seg_8528" s="T882">this</ta>
            <ta e="T884" id="Seg_8529" s="T883">old.man.[NOM]</ta>
            <ta e="T885" id="Seg_8530" s="T884">daughter.[NOM]-3SG</ta>
            <ta e="T886" id="Seg_8531" s="T885">be-PST.NAR.[3SG.S]</ta>
            <ta e="T887" id="Seg_8532" s="T886">this</ta>
            <ta e="T888" id="Seg_8533" s="T887">daughter-ACC-3SG</ta>
            <ta e="T889" id="Seg_8534" s="T888">Sylcha_Pylcha.[NOM]</ta>
            <ta e="T890" id="Seg_8535" s="T889">half-ILL</ta>
            <ta e="T891" id="Seg_8536" s="T890">give-PRS-3SG.O</ta>
            <ta e="T892" id="Seg_8537" s="T891">this</ta>
            <ta e="T893" id="Seg_8538" s="T892">earth-GEN</ta>
            <ta e="T894" id="Seg_8539" s="T893">end.[NOM]</ta>
            <ta e="T895" id="Seg_8540" s="T894">turn-TR-IPFV-DUR-3SG.O</ta>
            <ta e="T896" id="Seg_8541" s="T895">Selkup-EP-PL-EP-ALL</ta>
            <ta e="T897" id="Seg_8542" s="T896">come-PRS.[3SG.S]</ta>
            <ta e="T898" id="Seg_8543" s="T897">end.[NOM]-3SG</ta>
            <ta e="T899" id="Seg_8544" s="T898">INFER</ta>
            <ta e="T900" id="Seg_8545" s="T899">be-INFER.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_8546" s="T0">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T2" id="Seg_8547" s="T1">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T3" id="Seg_8548" s="T2">половина.[NOM]</ta>
            <ta e="T4" id="Seg_8549" s="T3">жить-PST.NAR.[3SG.S]</ta>
            <ta e="T5" id="Seg_8550" s="T4">мать-3SG-ADJZ</ta>
            <ta e="T6" id="Seg_8551" s="T5">быть-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T7" id="Seg_8552" s="T6">отец.[NOM]-3SG</ta>
            <ta e="T8" id="Seg_8553" s="T7">NEG.EX-PST.NAR.[3SG.S]</ta>
            <ta e="T9" id="Seg_8554" s="T8">жить.[3SG.S]</ta>
            <ta e="T10" id="Seg_8555" s="T9">жить.[3SG.S]</ta>
            <ta e="T11" id="Seg_8556" s="T10">один</ta>
            <ta e="T12" id="Seg_8557" s="T11">середина-ADV.LOC</ta>
            <ta e="T13" id="Seg_8558" s="T12">мать-ILL</ta>
            <ta e="T14" id="Seg_8559" s="T13">так</ta>
            <ta e="T15" id="Seg_8560" s="T14">сказать-3SG.O</ta>
            <ta e="T16" id="Seg_8561" s="T15">сказать-3SG.O</ta>
            <ta e="T17" id="Seg_8562" s="T16">мать-ILL</ta>
            <ta e="T18" id="Seg_8563" s="T17">сейчас</ta>
            <ta e="T19" id="Seg_8564" s="T18">такой-ADVZ</ta>
            <ta e="T20" id="Seg_8565" s="T19">сказать.[3SG.S]</ta>
            <ta e="T21" id="Seg_8566" s="T20">я.NOM</ta>
            <ta e="T22" id="Seg_8567" s="T21">человек-EP-ADJZ</ta>
            <ta e="T23" id="Seg_8568" s="T22">искать-FRQ-CVB</ta>
            <ta e="T24" id="Seg_8569" s="T23">отправиться-PRS-1SG.S</ta>
            <ta e="T25" id="Seg_8570" s="T24">так</ta>
            <ta e="T26" id="Seg_8571" s="T25">как</ta>
            <ta e="T27" id="Seg_8572" s="T26">жить-FUT-1PL</ta>
            <ta e="T28" id="Seg_8573" s="T27">мать.[NOM]</ta>
            <ta e="T29" id="Seg_8574" s="T28">ты.NOM</ta>
            <ta e="T30" id="Seg_8575" s="T29">неужели</ta>
            <ta e="T31" id="Seg_8576" s="T30">ли</ta>
            <ta e="T32" id="Seg_8577" s="T31">этот</ta>
            <ta e="T33" id="Seg_8578" s="T32">земля-GEN</ta>
            <ta e="T34" id="Seg_8579" s="T33">окрестность-LOC</ta>
            <ta e="T35" id="Seg_8580" s="T34">человек-EP-ACC</ta>
            <ta e="T36" id="Seg_8581" s="T35">NEG</ta>
            <ta e="T37" id="Seg_8582" s="T36">знать.[3SG.S]</ta>
            <ta e="T38" id="Seg_8583" s="T37">мать.[NOM]-3SG</ta>
            <ta e="T39" id="Seg_8584" s="T38">сказать-PRS-3SG.O</ta>
            <ta e="T40" id="Seg_8585" s="T39">я.NOM</ta>
            <ta e="T41" id="Seg_8586" s="T40">NEG</ta>
            <ta e="T42" id="Seg_8587" s="T41">что-ADJZ</ta>
            <ta e="T43" id="Seg_8588" s="T42">человек.[NOM]</ta>
            <ta e="T44" id="Seg_8589" s="T43">NEG</ta>
            <ta e="T45" id="Seg_8590" s="T44">знать.[3SG.S]</ta>
            <ta e="T46" id="Seg_8591" s="T45">этот</ta>
            <ta e="T47" id="Seg_8592" s="T46">земля-GEN</ta>
            <ta e="T48" id="Seg_8593" s="T47">окрестность-LOC</ta>
            <ta e="T49" id="Seg_8594" s="T48">родня-ADV.LOC</ta>
            <ta e="T50" id="Seg_8595" s="T49">человек.[NOM]</ta>
            <ta e="T51" id="Seg_8596" s="T50">NEG.EX.[3SG.S]</ta>
            <ta e="T52" id="Seg_8597" s="T51">сын.[NOM]-3SG</ta>
            <ta e="T53" id="Seg_8598" s="T52">мать-ILL-EMPH</ta>
            <ta e="T54" id="Seg_8599" s="T53">такой-ADJZ</ta>
            <ta e="T55" id="Seg_8600" s="T54">сказать-3SG.O</ta>
            <ta e="T56" id="Seg_8601" s="T55">ты.NOM</ta>
            <ta e="T57" id="Seg_8602" s="T56">мол</ta>
            <ta e="T58" id="Seg_8603" s="T57">ты.сам.[NOM]</ta>
            <ta e="T59" id="Seg_8604" s="T58">жить-IMP.2SG.S</ta>
            <ta e="T60" id="Seg_8605" s="T59">я.NOM</ta>
            <ta e="T61" id="Seg_8606" s="T60">скоро</ta>
            <ta e="T62" id="Seg_8607" s="T61">человек-EP-ADJZ</ta>
            <ta e="T63" id="Seg_8608" s="T62">искать-FRQ-EP-CVB</ta>
            <ta e="T64" id="Seg_8609" s="T63">отправиться-OPT-1SG.S</ta>
            <ta e="T65" id="Seg_8610" s="T64">этот</ta>
            <ta e="T66" id="Seg_8611" s="T65">земля-GEN</ta>
            <ta e="T67" id="Seg_8612" s="T66">окрестность-LOC</ta>
            <ta e="T68" id="Seg_8613" s="T67">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T69" id="Seg_8614" s="T68">половина.[NOM]</ta>
            <ta e="T70" id="Seg_8615" s="T69">потом</ta>
            <ta e="T71" id="Seg_8616" s="T70">уйти-PRS.[3SG.S]</ta>
            <ta e="T72" id="Seg_8617" s="T71">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T73" id="Seg_8618" s="T72">половина.[NOM]</ta>
            <ta e="T74" id="Seg_8619" s="T73">столько</ta>
            <ta e="T75" id="Seg_8620" s="T74">уйти-IPFV-PRS.[3SG.S]</ta>
            <ta e="T76" id="Seg_8621" s="T75">зима-VBLZ-DUR.[3SG.S]</ta>
            <ta e="T77" id="Seg_8622" s="T76">и</ta>
            <ta e="T78" id="Seg_8623" s="T77">лето-TRL-DUR.[3SG.S]</ta>
            <ta e="T79" id="Seg_8624" s="T78">большая.река-ACC</ta>
            <ta e="T80" id="Seg_8625" s="T79">река.[NOM]</ta>
            <ta e="T81" id="Seg_8626" s="T80">прийти-IPFV-HAB-DUR.[3SG.S]</ta>
            <ta e="T82" id="Seg_8627" s="T81">маленький</ta>
            <ta e="T83" id="Seg_8628" s="T82">река.[NOM]</ta>
            <ta e="T84" id="Seg_8629" s="T83">прийти-IPFV-HAB-DUR.[3SG.S]</ta>
            <ta e="T85" id="Seg_8630" s="T84">никакой</ta>
            <ta e="T86" id="Seg_8631" s="T85">NEG</ta>
            <ta e="T87" id="Seg_8632" s="T86">что-ADJZ</ta>
            <ta e="T88" id="Seg_8633" s="T87">человек.[NOM]</ta>
            <ta e="T89" id="Seg_8634" s="T88">NEG.EX.[3SG.S]</ta>
            <ta e="T90" id="Seg_8635" s="T89">то</ta>
            <ta e="T91" id="Seg_8636" s="T90">озеро-ADJZ</ta>
            <ta e="T92" id="Seg_8637" s="T91">нечто-LOC</ta>
            <ta e="T93" id="Seg_8638" s="T92">река-ADVZ</ta>
            <ta e="T94" id="Seg_8639" s="T93">отправиться-DUR.[3SG.S]</ta>
            <ta e="T95" id="Seg_8640" s="T94">шапка-3SG.TRL</ta>
            <ta e="T96" id="Seg_8641" s="T95">лоб-LOC</ta>
            <ta e="T97" id="Seg_8642" s="T96">снег-VBLZ-IPFV-DUR.[3SG.S]</ta>
            <ta e="T98" id="Seg_8643" s="T97">и</ta>
            <ta e="T99" id="Seg_8644" s="T98">пояс-TRL</ta>
            <ta e="T100" id="Seg_8645" s="T99">один</ta>
            <ta e="T101" id="Seg_8646" s="T100">середина-LOC</ta>
            <ta e="T102" id="Seg_8647" s="T101">прийти-US-PRS.[3SG.S]</ta>
            <ta e="T103" id="Seg_8648" s="T102">лес-ILL</ta>
            <ta e="T104" id="Seg_8649" s="T103">большой-ADJZ</ta>
            <ta e="T105" id="Seg_8650" s="T104">лес.[NOM]</ta>
            <ta e="T106" id="Seg_8651" s="T105">этот</ta>
            <ta e="T107" id="Seg_8652" s="T106">лес-PROL</ta>
            <ta e="T108" id="Seg_8653" s="T107">река.[NOM]</ta>
            <ta e="T109" id="Seg_8654" s="T108">выйти-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T110" id="Seg_8655" s="T109">этот</ta>
            <ta e="T111" id="Seg_8656" s="T110">река-GEN</ta>
            <ta e="T112" id="Seg_8657" s="T111">берег-PROL</ta>
            <ta e="T113" id="Seg_8658" s="T112">ходить-PST.NAR-1SG.S</ta>
            <ta e="T114" id="Seg_8659" s="T113">один</ta>
            <ta e="T115" id="Seg_8660" s="T114">середина-LOC</ta>
            <ta e="T116" id="Seg_8661" s="T115">DEF</ta>
            <ta e="T117" id="Seg_8662" s="T116">что.[NOM]</ta>
            <ta e="T118" id="Seg_8663" s="T117">слышаться-INFER.[3SG.S]</ta>
            <ta e="T119" id="Seg_8664" s="T118">а</ta>
            <ta e="T120" id="Seg_8665" s="T119">бух</ta>
            <ta e="T121" id="Seg_8666" s="T120">а</ta>
            <ta e="T122" id="Seg_8667" s="T121">бух</ta>
            <ta e="T123" id="Seg_8668" s="T122">ходить-HAB.[3SG.S]</ta>
            <ta e="T124" id="Seg_8669" s="T123">такой-ADJZ</ta>
            <ta e="T125" id="Seg_8670" s="T124">голос.[NOM]</ta>
            <ta e="T126" id="Seg_8671" s="T125">слышаться-PRS-3SG.O</ta>
            <ta e="T127" id="Seg_8672" s="T126">NEG</ta>
            <ta e="T128" id="Seg_8673" s="T127">что.[NOM]</ta>
            <ta e="T129" id="Seg_8674" s="T128">зверь.[NOM]</ta>
            <ta e="T130" id="Seg_8675" s="T129">NEG</ta>
            <ta e="T131" id="Seg_8676" s="T130">что.[NOM]</ta>
            <ta e="T132" id="Seg_8677" s="T131">NEG.EX.[3SG.S]</ta>
            <ta e="T133" id="Seg_8678" s="T132">этот</ta>
            <ta e="T134" id="Seg_8679" s="T133">земля-ACC</ta>
            <ta e="T135" id="Seg_8680" s="T134">окрестность.[NOM]</ta>
            <ta e="T136" id="Seg_8681" s="T135">повернуться-TR-ACTN-1SG.LOC</ta>
            <ta e="T137" id="Seg_8682" s="T136">ну.ка</ta>
            <ta e="T138" id="Seg_8683" s="T137">что.[NOM]</ta>
            <ta e="T139" id="Seg_8684" s="T138">говорить-US-INFER.[3SG.S]</ta>
            <ta e="T140" id="Seg_8685" s="T139">я.NOM</ta>
            <ta e="T141" id="Seg_8686" s="T140">хоть</ta>
            <ta e="T142" id="Seg_8687" s="T141">взглянуть-HAB-PST-1SG.S</ta>
            <ta e="T143" id="Seg_8688" s="T142">CONJ</ta>
            <ta e="T144" id="Seg_8689" s="T143">INFER</ta>
            <ta e="T145" id="Seg_8690" s="T144">прийти-IPFV-INFER.[3SG.S]</ta>
            <ta e="T146" id="Seg_8691" s="T145">INFER</ta>
            <ta e="T147" id="Seg_8692" s="T146">прийти-IPFV-INFER.[3SG.S]</ta>
            <ta e="T148" id="Seg_8693" s="T147">всё.ёще</ta>
            <ta e="T149" id="Seg_8694" s="T148">будто</ta>
            <ta e="T150" id="Seg_8695" s="T149">будто</ta>
            <ta e="T151" id="Seg_8696" s="T150">река.[NOM]</ta>
            <ta e="T152" id="Seg_8697" s="T151">внутри-LOC</ta>
            <ta e="T153" id="Seg_8698" s="T152">вниз.по.течению.реки-ADV.LOC</ta>
            <ta e="T154" id="Seg_8699" s="T153">слышаться.[3SG.S]</ta>
            <ta e="T155" id="Seg_8700" s="T154">опять</ta>
            <ta e="T156" id="Seg_8701" s="T155">ведь</ta>
            <ta e="T157" id="Seg_8702" s="T156">INFER</ta>
            <ta e="T158" id="Seg_8703" s="T157">кричать-US-IPFV-INFER.[3SG.S]</ta>
            <ta e="T159" id="Seg_8704" s="T158">а</ta>
            <ta e="T160" id="Seg_8705" s="T159">бух</ta>
            <ta e="T161" id="Seg_8706" s="T160">а</ta>
            <ta e="T162" id="Seg_8707" s="T161">бух</ta>
            <ta e="T163" id="Seg_8708" s="T162">такой</ta>
            <ta e="T164" id="Seg_8709" s="T163">звук.[NOM]-3SG</ta>
            <ta e="T165" id="Seg_8710" s="T164">что-ADJZ</ta>
            <ta e="T166" id="Seg_8711" s="T165">видать</ta>
            <ta e="T167" id="Seg_8712" s="T166">раздаваться.эхом-FRQ-PRS.[3SG.S]</ta>
            <ta e="T168" id="Seg_8713" s="T167">INFER</ta>
            <ta e="T169" id="Seg_8714" s="T168">прийти-IPFV-PRS.[3SG.S]</ta>
            <ta e="T170" id="Seg_8715" s="T169">вниз.по.течению.реки-ADV.LOC</ta>
            <ta e="T171" id="Seg_8716" s="T170">река-GEN</ta>
            <ta e="T172" id="Seg_8717" s="T171">середина-LOC</ta>
            <ta e="T173" id="Seg_8718" s="T172">DEF</ta>
            <ta e="T174" id="Seg_8719" s="T173">что.[NOM]</ta>
            <ta e="T175" id="Seg_8720" s="T174">чёрный-VBLZ-DECAUS.[3SG.S]</ta>
            <ta e="T176" id="Seg_8721" s="T175">это.[NOM]</ta>
            <ta e="T177" id="Seg_8722" s="T176">чужой</ta>
            <ta e="T178" id="Seg_8723" s="T177">кричать-PRS.[3SG.S]</ta>
            <ta e="T179" id="Seg_8724" s="T178">а</ta>
            <ta e="T180" id="Seg_8725" s="T179">бух</ta>
            <ta e="T181" id="Seg_8726" s="T180">а</ta>
            <ta e="T182" id="Seg_8727" s="T181">бух</ta>
            <ta e="T183" id="Seg_8728" s="T182">NEG</ta>
            <ta e="T184" id="Seg_8729" s="T183">NEG</ta>
            <ta e="T185" id="Seg_8730" s="T184">человек-EP-GEN</ta>
            <ta e="T186" id="Seg_8731" s="T185">голос.[NOM]</ta>
            <ta e="T187" id="Seg_8732" s="T186">только</ta>
            <ta e="T188" id="Seg_8733" s="T187">так</ta>
            <ta e="T189" id="Seg_8734" s="T188">кричать-US-PRS.[3SG.S]</ta>
            <ta e="T190" id="Seg_8735" s="T189">туда</ta>
            <ta e="T191" id="Seg_8736" s="T190">INFER</ta>
            <ta e="T192" id="Seg_8737" s="T191">прийти-INFER.[3SG.S]</ta>
            <ta e="T193" id="Seg_8738" s="T192">видать</ta>
            <ta e="T194" id="Seg_8739" s="T193">такой-ADVZ</ta>
            <ta e="T195" id="Seg_8740" s="T194">человек.[NOM]</ta>
            <ta e="T196" id="Seg_8741" s="T195">кричать-PST.[3SG.S]</ta>
            <ta e="T197" id="Seg_8742" s="T196">туда</ta>
            <ta e="T198" id="Seg_8743" s="T197">едва</ta>
            <ta e="T199" id="Seg_8744" s="T198">прийти-IPFV.[3SG.S]</ta>
            <ta e="T200" id="Seg_8745" s="T199">взглянуть-CVB</ta>
            <ta e="T201" id="Seg_8746" s="T200">человек.[NOM]</ta>
            <ta e="T202" id="Seg_8747" s="T201">видать</ta>
            <ta e="T203" id="Seg_8748" s="T202">человек.[NOM]</ta>
            <ta e="T204" id="Seg_8749" s="T203">этот</ta>
            <ta e="T205" id="Seg_8750" s="T204">отец-GEN-3SG</ta>
            <ta e="T206" id="Seg_8751" s="T205">мать-GEN-3SG</ta>
            <ta e="T207" id="Seg_8752" s="T206">мясо-INSTR</ta>
            <ta e="T208" id="Seg_8753" s="T207">голый</ta>
            <ta e="T209" id="Seg_8754" s="T208">человек.[NOM]</ta>
            <ta e="T210" id="Seg_8755" s="T209">он(а).[NOM]</ta>
            <ta e="T211" id="Seg_8756" s="T210">отец-GEN-3SG</ta>
            <ta e="T212" id="Seg_8757" s="T211">мать-GEN-3SG</ta>
            <ta e="T213" id="Seg_8758" s="T212">мясо-INSTR</ta>
            <ta e="T214" id="Seg_8759" s="T213">голый</ta>
            <ta e="T215" id="Seg_8760" s="T214">сидеть.[3SG.S]</ta>
            <ta e="T216" id="Seg_8761" s="T215">этот</ta>
            <ta e="T217" id="Seg_8762" s="T216">прорубь-GEN</ta>
            <ta e="T218" id="Seg_8763" s="T217">отверстие-LOC</ta>
            <ta e="T219" id="Seg_8764" s="T218">этот</ta>
            <ta e="T220" id="Seg_8765" s="T219">ли</ta>
            <ta e="T221" id="Seg_8766" s="T220">этот</ta>
            <ta e="T222" id="Seg_8767" s="T221">человек.[NOM]</ta>
            <ta e="T223" id="Seg_8768" s="T222">кричать-US-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T224" id="Seg_8769" s="T223">ты.NOM</ta>
            <ta e="T225" id="Seg_8770" s="T224">зачем</ta>
            <ta e="T226" id="Seg_8771" s="T225">такой-ADJZ</ta>
            <ta e="T227" id="Seg_8772" s="T226">сидеть-2SG.S</ta>
            <ta e="T228" id="Seg_8773" s="T227">NEG</ta>
            <ta e="T229" id="Seg_8774" s="T228">мол</ta>
            <ta e="T230" id="Seg_8775" s="T229">я.NOM</ta>
            <ta e="T231" id="Seg_8776" s="T230">чёрт.[NOM]</ta>
            <ta e="T232" id="Seg_8777" s="T231">я.ACC</ta>
            <ta e="T233" id="Seg_8778" s="T232">съесть-INF</ta>
            <ta e="T234" id="Seg_8779" s="T233">сидеть-1SG.S</ta>
            <ta e="T235" id="Seg_8780" s="T234">вверх-ADV.LOC</ta>
            <ta e="T236" id="Seg_8781" s="T235">человек-PL.[NOM]-1SG</ta>
            <ta e="T237" id="Seg_8782" s="T236">быть-PRS-3PL</ta>
            <ta e="T238" id="Seg_8783" s="T237">это-PL.[NOM]</ta>
            <ta e="T239" id="Seg_8784" s="T238">я.ACC</ta>
            <ta e="T240" id="Seg_8785" s="T239">сесть-RES-PST.NAR-3PL</ta>
            <ta e="T241" id="Seg_8786" s="T240">чёрт.[NOM]</ta>
            <ta e="T242" id="Seg_8787" s="T241">я.ACC</ta>
            <ta e="T243" id="Seg_8788" s="T242">съесть-INF</ta>
            <ta e="T244" id="Seg_8789" s="T243">просто.так</ta>
            <ta e="T245" id="Seg_8790" s="T244">я.NOM</ta>
            <ta e="T246" id="Seg_8791" s="T245">вверх</ta>
            <ta e="T247" id="Seg_8792" s="T246">выйти-FUT-1SG.S</ta>
            <ta e="T248" id="Seg_8793" s="T247">чёрт.[NOM]</ta>
            <ta e="T249" id="Seg_8794" s="T248">всё</ta>
            <ta e="T250" id="Seg_8795" s="T249">мы.PL.ACC</ta>
            <ta e="T251" id="Seg_8796" s="T250">съесть-FUT.[3SG.S]</ta>
            <ta e="T252" id="Seg_8797" s="T251">NEG</ta>
            <ta e="T253" id="Seg_8798" s="T252">что-ACC</ta>
            <ta e="T254" id="Seg_8799" s="T253">многочисленный</ta>
            <ta e="T255" id="Seg_8800" s="T254">остаться-TR.[3SG.S]</ta>
            <ta e="T256" id="Seg_8801" s="T255">вот</ta>
            <ta e="T257" id="Seg_8802" s="T256">зачем</ta>
            <ta e="T258" id="Seg_8803" s="T257">я.ACC</ta>
            <ta e="T259" id="Seg_8804" s="T258">сесть-RES-PST.NAR-3PL</ta>
            <ta e="T260" id="Seg_8805" s="T259">ты.NOM</ta>
            <ta e="T261" id="Seg_8806" s="T260">вверх</ta>
            <ta e="T262" id="Seg_8807" s="T261">выйти-IMP.2SG.S</ta>
            <ta e="T263" id="Seg_8808" s="T262">ты.NOM</ta>
            <ta e="T264" id="Seg_8809" s="T263">тоже</ta>
            <ta e="T265" id="Seg_8810" s="T264">вверх</ta>
            <ta e="T266" id="Seg_8811" s="T265">выйти-IMP.2SG.S</ta>
            <ta e="T267" id="Seg_8812" s="T266">я.NOM</ta>
            <ta e="T268" id="Seg_8813" s="T267">как</ta>
            <ta e="T269" id="Seg_8814" s="T268">выйти-FUT-1SG.S</ta>
            <ta e="T270" id="Seg_8815" s="T269">человек-EP-PL.[NOM]</ta>
            <ta e="T271" id="Seg_8816" s="T270">я.ACC</ta>
            <ta e="T272" id="Seg_8817" s="T271">увидеть-PFV-FUT-3PL</ta>
            <ta e="T273" id="Seg_8818" s="T272">я.NOM</ta>
            <ta e="T274" id="Seg_8819" s="T273">сказать-PRS-1SG.O</ta>
            <ta e="T275" id="Seg_8820" s="T274">ты.NOM</ta>
            <ta e="T276" id="Seg_8821" s="T275">вверх</ta>
            <ta e="T277" id="Seg_8822" s="T276">выйти-IMP.2SG.S</ta>
            <ta e="T278" id="Seg_8823" s="T277">вверх</ta>
            <ta e="T279" id="Seg_8824" s="T278">INFER</ta>
            <ta e="T280" id="Seg_8825" s="T279">выйти-INFER-3DU.S</ta>
            <ta e="T281" id="Seg_8826" s="T280">Сыльча_Пыльча-GEN</ta>
            <ta e="T282" id="Seg_8827" s="T281">половина.[NOM]</ta>
            <ta e="T283" id="Seg_8828" s="T282">идол.[NOM]</ta>
            <ta e="T284" id="Seg_8829" s="T283">INFER</ta>
            <ta e="T285" id="Seg_8830" s="T284">сделать-PFV-INFER-3SG.O</ta>
            <ta e="T286" id="Seg_8831" s="T285">отказ-OBL.3SG-INSTR</ta>
            <ta e="T287" id="Seg_8832" s="T286">ты.NOM</ta>
            <ta e="T288" id="Seg_8833" s="T287">вот</ta>
            <ta e="T289" id="Seg_8834" s="T288">я.ACC</ta>
            <ta e="T290" id="Seg_8835" s="T289">сколько</ta>
            <ta e="T291" id="Seg_8836" s="T290">NEG.IMP</ta>
            <ta e="T292" id="Seg_8837" s="T291">я.ACC</ta>
            <ta e="T293" id="Seg_8838" s="T292">сказать-IMP.2SG.S</ta>
            <ta e="T294" id="Seg_8839" s="T293">чёрт.[NOM]</ta>
            <ta e="T295" id="Seg_8840" s="T294">ты.ACC</ta>
            <ta e="T296" id="Seg_8841" s="T295">INFER</ta>
            <ta e="T297" id="Seg_8842" s="T296">спросить-FUT-INFER.[3SG.S]</ta>
            <ta e="T298" id="Seg_8843" s="T297">Сыльча_Пыльча.[NOM]-3SG</ta>
            <ta e="T299" id="Seg_8844" s="T298">половина.[NOM]</ta>
            <ta e="T300" id="Seg_8845" s="T299">куда</ta>
            <ta e="T301" id="Seg_8846" s="T300">куда.деваться-PST.[3SG.S]</ta>
            <ta e="T302" id="Seg_8847" s="T301">я.ACC</ta>
            <ta e="T303" id="Seg_8848" s="T302">сколько-EMPH</ta>
            <ta e="T304" id="Seg_8849" s="T303">я.ACC</ta>
            <ta e="T305" id="Seg_8850" s="T304">сказать-IMP.2SG.S</ta>
            <ta e="T306" id="Seg_8851" s="T305">такой-ADJZ</ta>
            <ta e="T307" id="Seg_8852" s="T306">пригрозить-PST.NAR-3SG.O</ta>
            <ta e="T308" id="Seg_8853" s="T307">отказ-INSTR</ta>
            <ta e="T309" id="Seg_8854" s="T308">вверх</ta>
            <ta e="T310" id="Seg_8855" s="T309">INFER</ta>
            <ta e="T311" id="Seg_8856" s="T310">отправиться-INFER.[3SG.S]</ta>
            <ta e="T312" id="Seg_8857" s="T311">туда</ta>
            <ta e="T313" id="Seg_8858" s="T312">видать</ta>
            <ta e="T314" id="Seg_8859" s="T313">земля-ADJZ</ta>
            <ta e="T315" id="Seg_8860" s="T314">дом.[NOM]</ta>
            <ta e="T316" id="Seg_8861" s="T315">это-LOC</ta>
            <ta e="T317" id="Seg_8862" s="T316">дверь-GEN</ta>
            <ta e="T318" id="Seg_8863" s="T317">отверстие-LOC</ta>
            <ta e="T319" id="Seg_8864" s="T318">тоже</ta>
            <ta e="T320" id="Seg_8865" s="T319">идол.[NOM]</ta>
            <ta e="T321" id="Seg_8866" s="T320">INFER</ta>
            <ta e="T322" id="Seg_8867" s="T321">сделать-INFER-3SG.O</ta>
            <ta e="T323" id="Seg_8868" s="T322">дом.[NOM]</ta>
            <ta e="T324" id="Seg_8869" s="T323">войти-IMP.2SG.S</ta>
            <ta e="T325" id="Seg_8870" s="T324">зачем</ta>
            <ta e="T326" id="Seg_8871" s="T325">стоять-EP-2SG.S</ta>
            <ta e="T327" id="Seg_8872" s="T326">замерзнуть-PRS-2SG.S</ta>
            <ta e="T328" id="Seg_8873" s="T327">я.NOM</ta>
            <ta e="T329" id="Seg_8874" s="T328">когда</ta>
            <ta e="T330" id="Seg_8875" s="T329">войти-FUT-1SG.S</ta>
            <ta e="T331" id="Seg_8876" s="T330">человек-PL.[NOM]-1SG</ta>
            <ta e="T332" id="Seg_8877" s="T331">я.ACC</ta>
            <ta e="T333" id="Seg_8878" s="T332">увидеть-PFV-FUT-3PL</ta>
            <ta e="T334" id="Seg_8879" s="T333">ты.NOM</ta>
            <ta e="T335" id="Seg_8880" s="T334">только</ta>
            <ta e="T336" id="Seg_8881" s="T335">дом.[NOM]</ta>
            <ta e="T337" id="Seg_8882" s="T336">войти-IMP.2SG.S</ta>
            <ta e="T338" id="Seg_8883" s="T337">этот</ta>
            <ta e="T339" id="Seg_8884" s="T338">дверь-GEN</ta>
            <ta e="T340" id="Seg_8885" s="T339">отверстие-LOC</ta>
            <ta e="T341" id="Seg_8886" s="T340">тоже</ta>
            <ta e="T342" id="Seg_8887" s="T341">идол.[NOM]</ta>
            <ta e="T343" id="Seg_8888" s="T342">сделать-INFER-3SG.O</ta>
            <ta e="T344" id="Seg_8889" s="T343">ты.NOM</ta>
            <ta e="T345" id="Seg_8890" s="T344">вот</ta>
            <ta e="T346" id="Seg_8891" s="T345">я.ACC</ta>
            <ta e="T347" id="Seg_8892" s="T346">NEG.IMP</ta>
            <ta e="T348" id="Seg_8893" s="T347">я.ACC</ta>
            <ta e="T349" id="Seg_8894" s="T348">сказать-IMP.2SG.S</ta>
            <ta e="T350" id="Seg_8895" s="T349">отказ-INSTR</ta>
            <ta e="T351" id="Seg_8896" s="T350">пригрозить-CO-3SG.O</ta>
            <ta e="T352" id="Seg_8897" s="T351">дом.[NOM]</ta>
            <ta e="T353" id="Seg_8898" s="T352">вот</ta>
            <ta e="T354" id="Seg_8899" s="T353">войти.[3SG.S]</ta>
            <ta e="T355" id="Seg_8900" s="T354">тот</ta>
            <ta e="T356" id="Seg_8901" s="T355">человек.[NOM]</ta>
            <ta e="T357" id="Seg_8902" s="T356">действительно</ta>
            <ta e="T358" id="Seg_8903" s="T357">всё</ta>
            <ta e="T359" id="Seg_8904" s="T358">INFER</ta>
            <ta e="T360" id="Seg_8905" s="T359">увидеть-PFV-INFER-3PL</ta>
            <ta e="T361" id="Seg_8906" s="T360">EMPH</ta>
            <ta e="T362" id="Seg_8907" s="T361">мол</ta>
            <ta e="T363" id="Seg_8908" s="T362">зачем</ta>
            <ta e="T364" id="Seg_8909" s="T363">войти-PRS.[3SG.S]</ta>
            <ta e="T365" id="Seg_8910" s="T364">NEG</ta>
            <ta e="T366" id="Seg_8911" s="T365">мол</ta>
            <ta e="T367" id="Seg_8912" s="T366">человек.[NOM]-3SG</ta>
            <ta e="T368" id="Seg_8913" s="T367">дом-ILL</ta>
            <ta e="T369" id="Seg_8914" s="T368">я.ACC</ta>
            <ta e="T370" id="Seg_8915" s="T369">послать-PST.[3SG.S]</ta>
            <ta e="T371" id="Seg_8916" s="T370">DEF</ta>
            <ta e="T372" id="Seg_8917" s="T371">что-EP-ADJZ</ta>
            <ta e="T373" id="Seg_8918" s="T372">человек.[NOM]</ta>
            <ta e="T374" id="Seg_8919" s="T373">прийти-PST.[3SG.S]</ta>
            <ta e="T375" id="Seg_8920" s="T374">я.ACC</ta>
            <ta e="T376" id="Seg_8921" s="T375">дом.[NOM]</ta>
            <ta e="T377" id="Seg_8922" s="T376">я.ACC</ta>
            <ta e="T378" id="Seg_8923" s="T377">послать-PST.[3SG.S]</ta>
            <ta e="T379" id="Seg_8924" s="T378">вверх</ta>
            <ta e="T380" id="Seg_8925" s="T379">я.ACC</ta>
            <ta e="T381" id="Seg_8926" s="T380">принести-CAUS-PST.[3SG.S]</ta>
            <ta e="T382" id="Seg_8927" s="T381">он(а).[NOM]</ta>
            <ta e="T383" id="Seg_8928" s="T382">едва</ta>
            <ta e="T384" id="Seg_8929" s="T383">увидеть-PRS-3SG.O</ta>
            <ta e="T385" id="Seg_8930" s="T384">дверь-GEN</ta>
            <ta e="T386" id="Seg_8931" s="T385">вперёд-ADJZ</ta>
            <ta e="T387" id="Seg_8932" s="T386">сторона-LOC</ta>
            <ta e="T388" id="Seg_8933" s="T387">тоже</ta>
            <ta e="T389" id="Seg_8934" s="T388">один</ta>
            <ta e="T390" id="Seg_8935" s="T389">голый</ta>
            <ta e="T391" id="Seg_8936" s="T390">человек.[NOM]</ta>
            <ta e="T392" id="Seg_8937" s="T391">появиться-INFER.[3SG.S]</ta>
            <ta e="T393" id="Seg_8938" s="T392">потом</ta>
            <ta e="T394" id="Seg_8939" s="T393">что.[NOM]</ta>
            <ta e="T395" id="Seg_8940" s="T394">такой-ADVZ</ta>
            <ta e="T396" id="Seg_8941" s="T395">сказать.[3SG.S]</ta>
            <ta e="T397" id="Seg_8942" s="T396">человек-EP-ACC</ta>
            <ta e="T398" id="Seg_8943" s="T397">вверх</ta>
            <ta e="T399" id="Seg_8944" s="T398">одеть(ся)-TR-3PL</ta>
            <ta e="T400" id="Seg_8945" s="T399">потом</ta>
            <ta e="T401" id="Seg_8946" s="T400">наружу</ta>
            <ta e="T402" id="Seg_8947" s="T401">выйти-CVB</ta>
            <ta e="T403" id="Seg_8948" s="T402">слушать-HAB-DUR-FRQ-IPFV-HAB-3SG.O</ta>
            <ta e="T404" id="Seg_8949" s="T403">чёрт.[NOM]</ta>
            <ta e="T405" id="Seg_8950" s="T404">кто-ADJZ</ta>
            <ta e="T406" id="Seg_8951" s="T405">мера-LOC</ta>
            <ta e="T407" id="Seg_8952" s="T406">появиться-US-DUR.[3SG.S]</ta>
            <ta e="T408" id="Seg_8953" s="T407">чёрт.[NOM]</ta>
            <ta e="T409" id="Seg_8954" s="T408">если</ta>
            <ta e="T410" id="Seg_8955" s="T409">появиться-US-PRS.[3SG.S]</ta>
            <ta e="T411" id="Seg_8956" s="T410">дом.[NOM]</ta>
            <ta e="T412" id="Seg_8957" s="T411">войти-IMP.2PL</ta>
            <ta e="T413" id="Seg_8958" s="T412">один</ta>
            <ta e="T414" id="Seg_8959" s="T413">человек.[NOM]</ta>
            <ta e="T415" id="Seg_8960" s="T414">слушать-HAB-HAB-IMP.3SG.S</ta>
            <ta e="T416" id="Seg_8961" s="T415">мол</ta>
            <ta e="T417" id="Seg_8962" s="T416">вечер-ADJZ</ta>
            <ta e="T418" id="Seg_8963" s="T417">заря.[NOM]-3SG</ta>
            <ta e="T419" id="Seg_8964" s="T418">надеть-EP-US-HAB-INFER.[3SG.S]</ta>
            <ta e="T420" id="Seg_8965" s="T419">тогда</ta>
            <ta e="T421" id="Seg_8966" s="T420">выйти-DUR-FRQ-IPFV-HAB.[3SG.S]</ta>
            <ta e="T422" id="Seg_8967" s="T421">появиться-US-IPFV-INFER.[3SG.S]</ta>
            <ta e="T423" id="Seg_8968" s="T422">достаточно</ta>
            <ta e="T424" id="Seg_8969" s="T423">быть-INFER.[3SG.S]</ta>
            <ta e="T425" id="Seg_8970" s="T424">один</ta>
            <ta e="T426" id="Seg_8971" s="T425">целый</ta>
            <ta e="T427" id="Seg_8972" s="T426">середина-LOC</ta>
            <ta e="T428" id="Seg_8973" s="T427">человек.[NOM]</ta>
            <ta e="T429" id="Seg_8974" s="T428">INFER</ta>
            <ta e="T430" id="Seg_8975" s="T429">выйти-INFER.[3SG.S]</ta>
            <ta e="T431" id="Seg_8976" s="T430">мол</ta>
            <ta e="T432" id="Seg_8977" s="T431">утро-ADV.LOC</ta>
            <ta e="T433" id="Seg_8978" s="T432">INFER</ta>
            <ta e="T434" id="Seg_8979" s="T433">появиться-US-INFER.[3SG.S]</ta>
            <ta e="T435" id="Seg_8980" s="T434">дом-ILL</ta>
            <ta e="T436" id="Seg_8981" s="T435">попасть-MULO-PRS.[3SG.S]</ta>
            <ta e="T437" id="Seg_8982" s="T436">чёрт.[NOM]</ta>
            <ta e="T438" id="Seg_8983" s="T437">INFER</ta>
            <ta e="T439" id="Seg_8984" s="T438">появиться-US-INFER.[3SG.S]</ta>
            <ta e="T440" id="Seg_8985" s="T439">а</ta>
            <ta e="T441" id="Seg_8986" s="T440">мол</ta>
            <ta e="T442" id="Seg_8987" s="T441">дальше</ta>
            <ta e="T443" id="Seg_8988" s="T442">притихнуть-VBLZ</ta>
            <ta e="T444" id="Seg_8989" s="T443">сидеть-IMP.2PL</ta>
            <ta e="T445" id="Seg_8990" s="T444">NEG</ta>
            <ta e="T446" id="Seg_8991" s="T445">когда</ta>
            <ta e="T447" id="Seg_8992" s="T446">NEG.IMP</ta>
            <ta e="T448" id="Seg_8993" s="T447">выйти-IMP.2PL</ta>
            <ta e="T449" id="Seg_8994" s="T448">Сыльча_Пыльча.[NOM]-3SG</ta>
            <ta e="T450" id="Seg_8995" s="T449">половина.[NOM]</ta>
            <ta e="T451" id="Seg_8996" s="T450">слушать-HAB-HAB-3SG.O</ta>
            <ta e="T452" id="Seg_8997" s="T451">что.[NOM]</ta>
            <ta e="T453" id="Seg_8998" s="T452">вверх</ta>
            <ta e="T454" id="Seg_8999" s="T453">INFER</ta>
            <ta e="T455" id="Seg_9000" s="T454">выйти-IPFV-PRS.[3SG.S]</ta>
            <ta e="T456" id="Seg_9001" s="T455">выйти-INFER.[3SG.S]</ta>
            <ta e="T457" id="Seg_9002" s="T456">рябчик-ACC</ta>
            <ta e="T458" id="Seg_9003" s="T457">увидеть-PFV-3SG.O</ta>
            <ta e="T459" id="Seg_9004" s="T458">рябчик.[NOM]</ta>
            <ta e="T460" id="Seg_9005" s="T459">чирик</ta>
            <ta e="T461" id="Seg_9006" s="T460">чирик</ta>
            <ta e="T462" id="Seg_9007" s="T461">ли</ta>
            <ta e="T463" id="Seg_9008" s="T462">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T464" id="Seg_9009" s="T463">половина.[NOM]</ta>
            <ta e="T465" id="Seg_9010" s="T464">куда.деваться-PST.[3SG.S]</ta>
            <ta e="T466" id="Seg_9011" s="T465">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T467" id="Seg_9012" s="T466">половина.[NOM]</ta>
            <ta e="T468" id="Seg_9013" s="T467">недавно</ta>
            <ta e="T469" id="Seg_9014" s="T468">уйти-PST.[3SG.S]</ta>
            <ta e="T470" id="Seg_9015" s="T469">вверх</ta>
            <ta e="T471" id="Seg_9016" s="T470">INFER</ta>
            <ta e="T472" id="Seg_9017" s="T471">прийти-INFER.[3SG.S]</ta>
            <ta e="T473" id="Seg_9018" s="T472">идол-ACC</ta>
            <ta e="T474" id="Seg_9019" s="T473">увидеть-PRS-3SG.O</ta>
            <ta e="T475" id="Seg_9020" s="T474">идол.[NOM]</ta>
            <ta e="T476" id="Seg_9021" s="T475">сказать-3SG.O</ta>
            <ta e="T477" id="Seg_9022" s="T476">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T478" id="Seg_9023" s="T477">половина.[NOM]</ta>
            <ta e="T479" id="Seg_9024" s="T478">дом-LOC</ta>
            <ta e="T480" id="Seg_9025" s="T479">сидеть.[3SG.S]</ta>
            <ta e="T481" id="Seg_9026" s="T480">чёрт.[NOM]</ta>
            <ta e="T482" id="Seg_9027" s="T481">вниз</ta>
            <ta e="T483" id="Seg_9028" s="T482">повернуться-DECAUS-CVB</ta>
            <ta e="T484" id="Seg_9029" s="T483">идти-INCH-PRS.[3SG.S]</ta>
            <ta e="T485" id="Seg_9030" s="T484">уйти-PRS.[3SG.S]</ta>
            <ta e="T486" id="Seg_9031" s="T485">вода-ILL</ta>
            <ta e="T487" id="Seg_9032" s="T486">упасть.[3SG.S]</ta>
            <ta e="T488" id="Seg_9033" s="T487">будто</ta>
            <ta e="T489" id="Seg_9034" s="T488">бульк</ta>
            <ta e="T490" id="Seg_9035" s="T489">кап</ta>
            <ta e="T491" id="Seg_9036" s="T490">наружу</ta>
            <ta e="T492" id="Seg_9037" s="T491">побежать-CVB</ta>
            <ta e="T493" id="Seg_9038" s="T492">тот</ta>
            <ta e="T494" id="Seg_9039" s="T493">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T495" id="Seg_9040" s="T494">половина.[NOM]</ta>
            <ta e="T496" id="Seg_9041" s="T495">идол-ACC-3SG</ta>
            <ta e="T497" id="Seg_9042" s="T496">рубить-US-CVB</ta>
            <ta e="T498" id="Seg_9043" s="T497">сломаться-CAUS-3SG.O</ta>
            <ta e="T499" id="Seg_9044" s="T498">прочь</ta>
            <ta e="T500" id="Seg_9045" s="T499">ударить-MOM-PRS-3SG.O</ta>
            <ta e="T501" id="Seg_9046" s="T500">другой</ta>
            <ta e="T502" id="Seg_9047" s="T501">идол-ACC</ta>
            <ta e="T503" id="Seg_9048" s="T502">сделать-PRS-3SG.O</ta>
            <ta e="T504" id="Seg_9049" s="T503">в.лицо</ta>
            <ta e="T505" id="Seg_9050" s="T504">пригрозить-HAB-SO-3SG.O</ta>
            <ta e="T506" id="Seg_9051" s="T505">ты.NOM</ta>
            <ta e="T507" id="Seg_9052" s="T506">вот</ta>
            <ta e="T508" id="Seg_9053" s="T507">я.ACC</ta>
            <ta e="T509" id="Seg_9054" s="T508">NEG</ta>
            <ta e="T510" id="Seg_9055" s="T509">когда</ta>
            <ta e="T511" id="Seg_9056" s="T510">сколько-ADV.EL</ta>
            <ta e="T512" id="Seg_9057" s="T511">NEG.IMP</ta>
            <ta e="T513" id="Seg_9058" s="T512">сказать-IMP.2SG.S</ta>
            <ta e="T514" id="Seg_9059" s="T513">теперь</ta>
            <ta e="T515" id="Seg_9060" s="T514">выйти-DUR-FRQ-IPFV-HAB.[3SG.S]</ta>
            <ta e="T516" id="Seg_9061" s="T515">утро-GEN-EP-ADJZ</ta>
            <ta e="T517" id="Seg_9062" s="T516">ночь-ADV.LOC</ta>
            <ta e="T518" id="Seg_9063" s="T517">потом</ta>
            <ta e="T519" id="Seg_9064" s="T518">ночь-GEN-3SG</ta>
            <ta e="T520" id="Seg_9065" s="T519">в.течение</ta>
            <ta e="T521" id="Seg_9066" s="T520">INFER</ta>
            <ta e="T522" id="Seg_9067" s="T521">сидеть-INFER-3PL</ta>
            <ta e="T523" id="Seg_9068" s="T522">утро-GEN-EP-ADJZ</ta>
            <ta e="T524" id="Seg_9069" s="T523">ночь-ADV.LOC</ta>
            <ta e="T525" id="Seg_9070" s="T524">человек-EP-ACC</ta>
            <ta e="T526" id="Seg_9071" s="T525">идти-CAUS-3PL</ta>
            <ta e="T527" id="Seg_9072" s="T526">наружу</ta>
            <ta e="T528" id="Seg_9073" s="T527">взглянуть-HAB-3PL</ta>
            <ta e="T529" id="Seg_9074" s="T528">если</ta>
            <ta e="T530" id="Seg_9075" s="T529">NEG</ta>
            <ta e="T531" id="Seg_9076" s="T530">выйти.[3SG.S]</ta>
            <ta e="T532" id="Seg_9077" s="T531">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T533" id="Seg_9078" s="T532">половина.[NOM]</ta>
            <ta e="T534" id="Seg_9079" s="T533">идти-CAUS-3PL</ta>
            <ta e="T535" id="Seg_9080" s="T534">слушать-HAB-HAB-IMP.2SG.O</ta>
            <ta e="T536" id="Seg_9081" s="T535">достаточно</ta>
            <ta e="T537" id="Seg_9082" s="T536">человек.[NOM]</ta>
            <ta e="T538" id="Seg_9083" s="T537">INFER</ta>
            <ta e="T539" id="Seg_9084" s="T538">тихий-ADVZ</ta>
            <ta e="T540" id="Seg_9085" s="T539">быть-INFER.[3SG.S]</ta>
            <ta e="T541" id="Seg_9086" s="T540">дом-ILL</ta>
            <ta e="T542" id="Seg_9087" s="T541">этот</ta>
            <ta e="T543" id="Seg_9088" s="T542">упасть-INFER.[3SG.S]</ta>
            <ta e="T544" id="Seg_9089" s="T543">будто</ta>
            <ta e="T545" id="Seg_9090" s="T544">один</ta>
            <ta e="T546" id="Seg_9091" s="T545">чёрт.[NOM]</ta>
            <ta e="T547" id="Seg_9092" s="T546">появиться-US-PST.NAR.[3SG.S]</ta>
            <ta e="T548" id="Seg_9093" s="T547">чёрт.[NOM]</ta>
            <ta e="T549" id="Seg_9094" s="T548">INFER</ta>
            <ta e="T550" id="Seg_9095" s="T549">появиться-US-PST.NAR.[3SG.S]</ta>
            <ta e="T551" id="Seg_9096" s="T550">вроде</ta>
            <ta e="T552" id="Seg_9097" s="T551">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T553" id="Seg_9098" s="T552">он.сам.[NOM]</ta>
            <ta e="T554" id="Seg_9099" s="T553">слушать-HAB-PST.NAR-3SG.O</ta>
            <ta e="T555" id="Seg_9100" s="T554">INFER</ta>
            <ta e="T556" id="Seg_9101" s="T555">выйти-INFER.[3SG.S]</ta>
            <ta e="T557" id="Seg_9102" s="T556">вверх</ta>
            <ta e="T558" id="Seg_9103" s="T557">идти-CVB</ta>
            <ta e="T559" id="Seg_9104" s="T558">он(а).[NOM]</ta>
            <ta e="T560" id="Seg_9105" s="T559">прийти-DUR-PRS.[3SG.S]</ta>
            <ta e="T561" id="Seg_9106" s="T560">идти-CVB</ta>
            <ta e="T562" id="Seg_9107" s="T561">он(а).[NOM]</ta>
            <ta e="T563" id="Seg_9108" s="T562">прийти-DUR-PRS.[3SG.S]</ta>
            <ta e="T564" id="Seg_9109" s="T563">этот</ta>
            <ta e="T565" id="Seg_9110" s="T564">гора-GEN</ta>
            <ta e="T566" id="Seg_9111" s="T565">верх-EP-ILL</ta>
            <ta e="T567" id="Seg_9112" s="T566">INFER</ta>
            <ta e="T568" id="Seg_9113" s="T567">выйти-INFER.[3SG.S]</ta>
            <ta e="T569" id="Seg_9114" s="T568">тот.[NOM]</ta>
            <ta e="T570" id="Seg_9115" s="T569">идол-ILL</ta>
            <ta e="T571" id="Seg_9116" s="T570">идти-CVB</ta>
            <ta e="T572" id="Seg_9117" s="T571">INFER</ta>
            <ta e="T573" id="Seg_9118" s="T572">прийти-INFER.[3SG.S]</ta>
            <ta e="T574" id="Seg_9119" s="T573">будто</ta>
            <ta e="T575" id="Seg_9120" s="T574">чирик</ta>
            <ta e="T576" id="Seg_9121" s="T575">чирик</ta>
            <ta e="T577" id="Seg_9122" s="T576">чирик</ta>
            <ta e="T578" id="Seg_9123" s="T577">ли</ta>
            <ta e="T579" id="Seg_9124" s="T578">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T580" id="Seg_9125" s="T579">половина.[NOM]</ta>
            <ta e="T581" id="Seg_9126" s="T580">куда.деваться-PRS.[3SG.S]</ta>
            <ta e="T582" id="Seg_9127" s="T581">ли</ta>
            <ta e="T583" id="Seg_9128" s="T582">дом-LOC</ta>
            <ta e="T584" id="Seg_9129" s="T583">быть-PRS.[3SG.S]</ta>
            <ta e="T585" id="Seg_9130" s="T584">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T586" id="Seg_9131" s="T585">половина.[NOM]</ta>
            <ta e="T587" id="Seg_9132" s="T586">тогда</ta>
            <ta e="T588" id="Seg_9133" s="T587">уйти-PST.[3SG.S]</ta>
            <ta e="T589" id="Seg_9134" s="T588">ты.NOM</ta>
            <ta e="T590" id="Seg_9135" s="T589">вверх</ta>
            <ta e="T591" id="Seg_9136" s="T590">уйти-CVB</ta>
            <ta e="T592" id="Seg_9137" s="T591">еда-ADJZ</ta>
            <ta e="T593" id="Seg_9138" s="T592">съесть-IMP.2SG.O</ta>
            <ta e="T594" id="Seg_9139" s="T593">тот.[NOM]</ta>
            <ta e="T595" id="Seg_9140" s="T594">идти-CVB</ta>
            <ta e="T596" id="Seg_9141" s="T595">INFER</ta>
            <ta e="T597" id="Seg_9142" s="T596">уйти-INFER.[3SG.S]-EMPH</ta>
            <ta e="T598" id="Seg_9143" s="T597">дверь-GEN</ta>
            <ta e="T599" id="Seg_9144" s="T598">отверстие-EP-ADJZ</ta>
            <ta e="T600" id="Seg_9145" s="T599">прийти-INFER.[3SG.S]-EMPH</ta>
            <ta e="T601" id="Seg_9146" s="T600">идол-ILL</ta>
            <ta e="T602" id="Seg_9147" s="T601">прийти-PRS.[3SG.S]</ta>
            <ta e="T603" id="Seg_9148" s="T602">куда.деваться-PST.[3SG.S]</ta>
            <ta e="T604" id="Seg_9149" s="T603">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T605" id="Seg_9150" s="T604">половина.[NOM]</ta>
            <ta e="T606" id="Seg_9151" s="T605">он(а).[NOM]</ta>
            <ta e="T607" id="Seg_9152" s="T606">же</ta>
            <ta e="T608" id="Seg_9153" s="T607">вчера</ta>
            <ta e="T609" id="Seg_9154" s="T608">тогда</ta>
            <ta e="T610" id="Seg_9155" s="T609">уйти-PST.[3SG.S]</ta>
            <ta e="T611" id="Seg_9156" s="T610">отправиться-US-EP-PRS.[3SG.S]</ta>
            <ta e="T612" id="Seg_9157" s="T611">дом-ILL</ta>
            <ta e="T613" id="Seg_9158" s="T612">войти-CVB</ta>
            <ta e="T614" id="Seg_9159" s="T613">еда-ADJZ</ta>
            <ta e="T615" id="Seg_9160" s="T614">съесть-IMP.2SG.O</ta>
            <ta e="T616" id="Seg_9161" s="T615">один</ta>
            <ta e="T617" id="Seg_9162" s="T616">середина-LOC</ta>
            <ta e="T618" id="Seg_9163" s="T617">чёрт.[NOM]</ta>
            <ta e="T619" id="Seg_9164" s="T618">дом-ILL</ta>
            <ta e="T620" id="Seg_9165" s="T619">INFER</ta>
            <ta e="T621" id="Seg_9166" s="T620">толкать-MOM-INFER.[3SG.S]</ta>
            <ta e="T622" id="Seg_9167" s="T621">дом-ILL</ta>
            <ta e="T623" id="Seg_9168" s="T622">INFER</ta>
            <ta e="T624" id="Seg_9169" s="T623">толкать-MOM-INFER.[3SG.S]</ta>
            <ta e="T625" id="Seg_9170" s="T624">дом-ILL</ta>
            <ta e="T626" id="Seg_9171" s="T625">только</ta>
            <ta e="T627" id="Seg_9172" s="T626">войти-PRS.[3SG.S]</ta>
            <ta e="T628" id="Seg_9173" s="T627">чёрт.[NOM]</ta>
            <ta e="T629" id="Seg_9174" s="T628">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T630" id="Seg_9175" s="T629">половина.[NOM]</ta>
            <ta e="T631" id="Seg_9176" s="T630">рубить-MOM-PRS-3SG.O</ta>
            <ta e="T632" id="Seg_9177" s="T631">голова-GEN-3SG</ta>
            <ta e="T633" id="Seg_9178" s="T632">кусок.[NOM]</ta>
            <ta e="T634" id="Seg_9179" s="T633">дом-ILL</ta>
            <ta e="T635" id="Seg_9180" s="T634">сквозь</ta>
            <ta e="T636" id="Seg_9181" s="T635">вперёд</ta>
            <ta e="T637" id="Seg_9182" s="T636">кататься-FRQ-PRS.[3SG.S]</ta>
            <ta e="T638" id="Seg_9183" s="T637">тело-GEN-3SG</ta>
            <ta e="T639" id="Seg_9184" s="T638">кусок.[NOM]</ta>
            <ta e="T640" id="Seg_9185" s="T639">на.спину</ta>
            <ta e="T641" id="Seg_9186" s="T640">наружу</ta>
            <ta e="T642" id="Seg_9187" s="T641">упасть.[3SG.S]</ta>
            <ta e="T643" id="Seg_9188" s="T642">убить-TR-INFER-3SG.O</ta>
            <ta e="T644" id="Seg_9189" s="T643">наружу</ta>
            <ta e="T645" id="Seg_9190" s="T644">принести-PRS-3SG.O</ta>
            <ta e="T646" id="Seg_9191" s="T645">тот</ta>
            <ta e="T647" id="Seg_9192" s="T646">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T648" id="Seg_9193" s="T647">половина.[NOM]</ta>
            <ta e="T649" id="Seg_9194" s="T648">тело-GEN-3SG</ta>
            <ta e="T650" id="Seg_9195" s="T649">кусок.[NOM]</ta>
            <ta e="T651" id="Seg_9196" s="T650">наружу</ta>
            <ta e="T652" id="Seg_9197" s="T651">выйти.[3SG.S]</ta>
            <ta e="T653" id="Seg_9198" s="T652">видать</ta>
            <ta e="T654" id="Seg_9199" s="T653">день-VBLZ-PFV.[3SG.S]</ta>
            <ta e="T655" id="Seg_9200" s="T654">вниз</ta>
            <ta e="T656" id="Seg_9201" s="T655">занести-HAB-PRS-3SG.O</ta>
            <ta e="T657" id="Seg_9202" s="T656">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T658" id="Seg_9203" s="T657">половина.[NOM]</ta>
            <ta e="T659" id="Seg_9204" s="T658">тело-GEN-3SG</ta>
            <ta e="T660" id="Seg_9205" s="T659">кусок-ACC</ta>
            <ta e="T661" id="Seg_9206" s="T660">вода-EP-ADJZ</ta>
            <ta e="T662" id="Seg_9207" s="T661">прорубь-ILL</ta>
            <ta e="T663" id="Seg_9208" s="T662">положить-PRS-3SG.O</ta>
            <ta e="T664" id="Seg_9209" s="T663">лёд-COM</ta>
            <ta e="T665" id="Seg_9210" s="T664">вместе</ta>
            <ta e="T666" id="Seg_9211" s="T665">замерзнуть-TR-3SG.O</ta>
            <ta e="T667" id="Seg_9212" s="T666">такой-ADVZ</ta>
            <ta e="T668" id="Seg_9213" s="T667">положить-PRS-3SG.O</ta>
            <ta e="T669" id="Seg_9214" s="T668">вода-EP-ADJZ</ta>
            <ta e="T670" id="Seg_9215" s="T669">прорубь-GEN-3SG</ta>
            <ta e="T671" id="Seg_9216" s="T670">рядом-ILL</ta>
            <ta e="T672" id="Seg_9217" s="T671">такой-ADVZ</ta>
            <ta e="T673" id="Seg_9218" s="T672">положить-PRS-3SG.O</ta>
            <ta e="T674" id="Seg_9219" s="T673">словно</ta>
            <ta e="T675" id="Seg_9220" s="T674">словно</ta>
            <ta e="T676" id="Seg_9221" s="T675">жить-CVB</ta>
            <ta e="T677" id="Seg_9222" s="T676">лежать.[3SG.S]</ta>
            <ta e="T678" id="Seg_9223" s="T677">голова-OBL.3SG-COM</ta>
            <ta e="T679" id="Seg_9224" s="T678">вместе</ta>
            <ta e="T680" id="Seg_9225" s="T679">стоять.торчком-TR-PRS-3SG.O</ta>
            <ta e="T681" id="Seg_9226" s="T680">вот</ta>
            <ta e="T682" id="Seg_9227" s="T681">день-TR-PRS-1PL</ta>
            <ta e="T683" id="Seg_9228" s="T682">темнота-VBLZ-EP-DECAUS.[3SG.S]</ta>
            <ta e="T684" id="Seg_9229" s="T683">мол</ta>
            <ta e="T685" id="Seg_9230" s="T684">только</ta>
            <ta e="T686" id="Seg_9231" s="T685">два-DU-ADJZ</ta>
            <ta e="T687" id="Seg_9232" s="T686">другой</ta>
            <ta e="T688" id="Seg_9233" s="T687">нечто.[NOM]</ta>
            <ta e="T689" id="Seg_9234" s="T688">ходить-INF</ta>
            <ta e="T690" id="Seg_9235" s="T689">NEG</ta>
            <ta e="T691" id="Seg_9236" s="T690">знать-1PL</ta>
            <ta e="T692" id="Seg_9237" s="T691">вечер-ADV.LOC</ta>
            <ta e="T693" id="Seg_9238" s="T692">вот</ta>
            <ta e="T694" id="Seg_9239" s="T693">темнота-VBLZ-EP-DECAUS-PRS.[3SG.S]</ta>
            <ta e="T695" id="Seg_9240" s="T694">вечер-ADV.LOC</ta>
            <ta e="T696" id="Seg_9241" s="T695">достаточно</ta>
            <ta e="T697" id="Seg_9242" s="T696">сидеть.[3SG.S]</ta>
            <ta e="T698" id="Seg_9243" s="T697">вот</ta>
            <ta e="T699" id="Seg_9244" s="T698">сумрачный.VBLZ-DECAUS-INF-начать-PRS.[3SG.S]</ta>
            <ta e="T700" id="Seg_9245" s="T699">человек-EP-ACC</ta>
            <ta e="T701" id="Seg_9246" s="T700">наружу</ta>
            <ta e="T702" id="Seg_9247" s="T701">идти-TR-3SG.O</ta>
            <ta e="T703" id="Seg_9248" s="T702">слушать-HAB-HAB-IMP.2SG.O</ta>
            <ta e="T704" id="Seg_9249" s="T703">человек.[NOM]</ta>
            <ta e="T705" id="Seg_9250" s="T704">пространство.снаружи-LOC</ta>
            <ta e="T706" id="Seg_9251" s="T705">стоять.[3SG.S]</ta>
            <ta e="T707" id="Seg_9252" s="T706">ой</ta>
            <ta e="T708" id="Seg_9253" s="T707">человек.[NOM]</ta>
            <ta e="T709" id="Seg_9254" s="T708">вот</ta>
            <ta e="T710" id="Seg_9255" s="T709">появиться-US-INF-начать-PRS.[3SG.S]</ta>
            <ta e="T711" id="Seg_9256" s="T710">дом-ILL</ta>
            <ta e="T712" id="Seg_9257" s="T711">попасть-MULO-PRS.[3SG.S]</ta>
            <ta e="T713" id="Seg_9258" s="T712">дом-ILL</ta>
            <ta e="T714" id="Seg_9259" s="T713">войти-CVB</ta>
            <ta e="T715" id="Seg_9260" s="T714">такой-ADVZ</ta>
            <ta e="T716" id="Seg_9261" s="T715">сказать-PST.NAR-3SG.O</ta>
            <ta e="T717" id="Seg_9262" s="T716">чёрт.[NOM]</ta>
            <ta e="T718" id="Seg_9263" s="T717">вниз-ADV.LOC</ta>
            <ta e="T719" id="Seg_9264" s="T718">появиться-US-PRS.[3SG.S]</ta>
            <ta e="T720" id="Seg_9265" s="T719">вверх</ta>
            <ta e="T721" id="Seg_9266" s="T720">идти-US-CVB</ta>
            <ta e="T722" id="Seg_9267" s="T721">INFER</ta>
            <ta e="T723" id="Seg_9268" s="T722">выйти-INFER.[3SG.S]</ta>
            <ta e="T724" id="Seg_9269" s="T723">вверх</ta>
            <ta e="T725" id="Seg_9270" s="T724">что.[NOM]</ta>
            <ta e="T726" id="Seg_9271" s="T725">появиться-US-HAB.[3SG.S]</ta>
            <ta e="T727" id="Seg_9272" s="T726">а</ta>
            <ta e="T728" id="Seg_9273" s="T727">ты.NOM</ta>
            <ta e="T729" id="Seg_9274" s="T728">ли</ta>
            <ta e="T730" id="Seg_9275" s="T729">затечь-DECAUS-EP-2SG.S</ta>
            <ta e="T731" id="Seg_9276" s="T730">съесть-EP-FRQ-PFV-PRS-2SG.S</ta>
            <ta e="T732" id="Seg_9277" s="T731">испугаться-DECAUS-EP-PRS-2SG.S</ta>
            <ta e="T733" id="Seg_9278" s="T732">один</ta>
            <ta e="T734" id="Seg_9279" s="T733">некто-ACC-3SG</ta>
            <ta e="T735" id="Seg_9280" s="T734">такой-ADVZ</ta>
            <ta e="T736" id="Seg_9281" s="T735">увидеть-PRS-3SG.O</ta>
            <ta e="T737" id="Seg_9282" s="T736">я.NOM</ta>
            <ta e="T738" id="Seg_9283" s="T737">раньше</ta>
            <ta e="T739" id="Seg_9284" s="T738">съесть-EP-FRQ-PFV-CVB</ta>
            <ta e="T740" id="Seg_9285" s="T739">такой-ADVZ</ta>
            <ta e="T741" id="Seg_9286" s="T740">лежать-DUR-FRQ-IPFV-HAB-DUR-1SG.S</ta>
            <ta e="T742" id="Seg_9287" s="T741">потом</ta>
            <ta e="T743" id="Seg_9288" s="T742">вверх</ta>
            <ta e="T744" id="Seg_9289" s="T743">идти-CVB</ta>
            <ta e="T745" id="Seg_9290" s="T744">INFER</ta>
            <ta e="T746" id="Seg_9291" s="T745">прийти-INFER.[3SG.S]</ta>
            <ta e="T747" id="Seg_9292" s="T746">вверх</ta>
            <ta e="T748" id="Seg_9293" s="T747">идти-CVB</ta>
            <ta e="T749" id="Seg_9294" s="T748">INFER</ta>
            <ta e="T750" id="Seg_9295" s="T749">прийти-INFER.[3SG.S]</ta>
            <ta e="T751" id="Seg_9296" s="T750">чирик</ta>
            <ta e="T752" id="Seg_9297" s="T751">чирик</ta>
            <ta e="T753" id="Seg_9298" s="T752">ли</ta>
            <ta e="T754" id="Seg_9299" s="T753">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T755" id="Seg_9300" s="T754">половина.[NOM]</ta>
            <ta e="T756" id="Seg_9301" s="T755">куда.деваться-PFV.[3SG.S]</ta>
            <ta e="T757" id="Seg_9302" s="T756">прийти-PRS.[3SG.S]</ta>
            <ta e="T758" id="Seg_9303" s="T757">то.ли</ta>
            <ta e="T759" id="Seg_9304" s="T758">уйти-PST.[3SG.S]</ta>
            <ta e="T760" id="Seg_9305" s="T759">то.ли</ta>
            <ta e="T761" id="Seg_9306" s="T760">куда.деваться-PST.[3SG.S]</ta>
            <ta e="T762" id="Seg_9307" s="T761">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T763" id="Seg_9308" s="T762">половина.[NOM]</ta>
            <ta e="T764" id="Seg_9309" s="T763">раньше</ta>
            <ta e="T765" id="Seg_9310" s="T764">тогда</ta>
            <ta e="T766" id="Seg_9311" s="T765">уйти-PST.[3SG.S]</ta>
            <ta e="T767" id="Seg_9312" s="T766">недавно</ta>
            <ta e="T768" id="Seg_9313" s="T767">тогда</ta>
            <ta e="T769" id="Seg_9314" s="T768">уйти-PST.[3SG.S]</ta>
            <ta e="T770" id="Seg_9315" s="T769">ты.NOM</ta>
            <ta e="T771" id="Seg_9316" s="T770">вверх</ta>
            <ta e="T772" id="Seg_9317" s="T771">уйти-CVB</ta>
            <ta e="T773" id="Seg_9318" s="T772">еда-ADJZ</ta>
            <ta e="T774" id="Seg_9319" s="T773">съесть-IMP.2SG.O</ta>
            <ta e="T775" id="Seg_9320" s="T774">потом</ta>
            <ta e="T776" id="Seg_9321" s="T775">вверх</ta>
            <ta e="T777" id="Seg_9322" s="T776">уйти-CVB</ta>
            <ta e="T778" id="Seg_9323" s="T777">идти-CVB</ta>
            <ta e="T779" id="Seg_9324" s="T778">тот</ta>
            <ta e="T780" id="Seg_9325" s="T779">идол-ILL</ta>
            <ta e="T781" id="Seg_9326" s="T780">прийти-PRS.[3SG.S]</ta>
            <ta e="T782" id="Seg_9327" s="T781">сказать-3SG.O</ta>
            <ta e="T783" id="Seg_9328" s="T782">ли</ta>
            <ta e="T784" id="Seg_9329" s="T783">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T785" id="Seg_9330" s="T784">половина.[NOM]</ta>
            <ta e="T786" id="Seg_9331" s="T785">дом-LOC</ta>
            <ta e="T787" id="Seg_9332" s="T786">быть-PRS.[3SG.S]</ta>
            <ta e="T788" id="Seg_9333" s="T787">он(а).[NOM]</ta>
            <ta e="T789" id="Seg_9334" s="T788">же</ta>
            <ta e="T790" id="Seg_9335" s="T789">тогда</ta>
            <ta e="T791" id="Seg_9336" s="T790">недавно</ta>
            <ta e="T792" id="Seg_9337" s="T791">уйти-PST.[3SG.S]</ta>
            <ta e="T793" id="Seg_9338" s="T792">дом.[NOM]</ta>
            <ta e="T794" id="Seg_9339" s="T793">войти-CVB</ta>
            <ta e="T795" id="Seg_9340" s="T794">еда-ADJZ</ta>
            <ta e="T796" id="Seg_9341" s="T795">съесть-IMP.2SG.O</ta>
            <ta e="T797" id="Seg_9342" s="T796">остальной</ta>
            <ta e="T798" id="Seg_9343" s="T797">опять</ta>
            <ta e="T799" id="Seg_9344" s="T798">такой-ADVZ</ta>
            <ta e="T800" id="Seg_9345" s="T799">сказать.[3SG.S]</ta>
            <ta e="T801" id="Seg_9346" s="T800">дом-ILL</ta>
            <ta e="T802" id="Seg_9347" s="T801">то.ли</ta>
            <ta e="T803" id="Seg_9348" s="T802">войти-FUT-1SG.S</ta>
            <ta e="T804" id="Seg_9349" s="T803">то.ли</ta>
            <ta e="T805" id="Seg_9350" s="T804">NEG</ta>
            <ta e="T806" id="Seg_9351" s="T805">войти-FUT-1SG.S</ta>
            <ta e="T807" id="Seg_9352" s="T806">остальной</ta>
            <ta e="T808" id="Seg_9353" s="T807">опять</ta>
            <ta e="T809" id="Seg_9354" s="T808">нечто.[NOM]</ta>
            <ta e="T810" id="Seg_9355" s="T809">идол.[NOM]</ta>
            <ta e="T811" id="Seg_9356" s="T810">ли</ta>
            <ta e="T812" id="Seg_9357" s="T811">мол</ta>
            <ta e="T813" id="Seg_9358" s="T812">я.ACC</ta>
            <ta e="T814" id="Seg_9359" s="T813">запутать-HAB-2SG.S</ta>
            <ta e="T815" id="Seg_9360" s="T814">вроде</ta>
            <ta e="T816" id="Seg_9361" s="T815">догадаться-HAB.[3SG.S]</ta>
            <ta e="T817" id="Seg_9362" s="T816">догадаться-HAB-PRS.[3SG.S]</ta>
            <ta e="T818" id="Seg_9363" s="T817">остальной-ADJZ</ta>
            <ta e="T819" id="Seg_9364" s="T818">то.ли</ta>
            <ta e="T820" id="Seg_9365" s="T819">войти-FUT.[3SG.S]</ta>
            <ta e="T821" id="Seg_9366" s="T820">то.ли</ta>
            <ta e="T822" id="Seg_9367" s="T821">NEG</ta>
            <ta e="T823" id="Seg_9368" s="T822">войти-FUT.[3SG.S]</ta>
            <ta e="T824" id="Seg_9369" s="T823">один</ta>
            <ta e="T825" id="Seg_9370" s="T824">середина-LOC</ta>
            <ta e="T826" id="Seg_9371" s="T825">INFER</ta>
            <ta e="T827" id="Seg_9372" s="T826">войти-INF-начать-INFER.[3SG.S]</ta>
            <ta e="T828" id="Seg_9373" s="T827">голова-ACC-3SG</ta>
            <ta e="T829" id="Seg_9374" s="T828">дом-ILL</ta>
            <ta e="T830" id="Seg_9375" s="T829">едва</ta>
            <ta e="T831" id="Seg_9376" s="T830">толкать-MOM-PRS-3SG.O</ta>
            <ta e="T832" id="Seg_9377" s="T831">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T833" id="Seg_9378" s="T832">половина.[NOM]</ta>
            <ta e="T834" id="Seg_9379" s="T833">рубить-MOM-PRS-3SG.O</ta>
            <ta e="T835" id="Seg_9380" s="T834">голова-GEN-3SG</ta>
            <ta e="T836" id="Seg_9381" s="T835">кусок.[NOM]</ta>
            <ta e="T837" id="Seg_9382" s="T836">дом-ILL</ta>
            <ta e="T838" id="Seg_9383" s="T837">вперёд</ta>
            <ta e="T839" id="Seg_9384" s="T838">упасть.[3SG.S]</ta>
            <ta e="T840" id="Seg_9385" s="T839">кататься-FRQ-PRS.[3SG.S]</ta>
            <ta e="T841" id="Seg_9386" s="T840">тело-GEN-3SG</ta>
            <ta e="T842" id="Seg_9387" s="T841">кусок.[NOM]</ta>
            <ta e="T843" id="Seg_9388" s="T842">на.спину</ta>
            <ta e="T844" id="Seg_9389" s="T843">наружу</ta>
            <ta e="T845" id="Seg_9390" s="T844">упасть.[3SG.S]</ta>
            <ta e="T846" id="Seg_9391" s="T845">убить-PFV-PRS-3SG.O</ta>
            <ta e="T847" id="Seg_9392" s="T846">потом</ta>
            <ta e="T848" id="Seg_9393" s="T847">затем</ta>
            <ta e="T849" id="Seg_9394" s="T848">только</ta>
            <ta e="T850" id="Seg_9395" s="T849">сидеть-3PL</ta>
            <ta e="T851" id="Seg_9396" s="T850">сидеть-3PL</ta>
            <ta e="T852" id="Seg_9397" s="T851">NEG</ta>
            <ta e="T853" id="Seg_9398" s="T852">что.[NOM]</ta>
            <ta e="T854" id="Seg_9399" s="T853">NEG.EX.[3SG.S]</ta>
            <ta e="T855" id="Seg_9400" s="T854">мол</ta>
            <ta e="T856" id="Seg_9401" s="T855">ли</ta>
            <ta e="T857" id="Seg_9402" s="T856">два</ta>
            <ta e="T858" id="Seg_9403" s="T857">ходить.[3SG.S]</ta>
            <ta e="T859" id="Seg_9404" s="T858">следующий</ta>
            <ta e="T860" id="Seg_9405" s="T859">утро-ADV.LOC</ta>
            <ta e="T861" id="Seg_9406" s="T860">вверх</ta>
            <ta e="T862" id="Seg_9407" s="T861">день-VBLZ-PRS.[3SG.S]</ta>
            <ta e="T863" id="Seg_9408" s="T862">мол</ta>
            <ta e="T864" id="Seg_9409" s="T863">дрова.[NOM]</ta>
            <ta e="T865" id="Seg_9410" s="T864">рубить-MOM-IMP.2PL</ta>
            <ta e="T866" id="Seg_9411" s="T865">дрова.[NOM]</ta>
            <ta e="T867" id="Seg_9412" s="T866">рубить-MOM-PRS-3PL</ta>
            <ta e="T868" id="Seg_9413" s="T867">огонь.[NOM]</ta>
            <ta e="T869" id="Seg_9414" s="T868">зажечь-IMP.2PL</ta>
            <ta e="T870" id="Seg_9415" s="T869">действительно</ta>
            <ta e="T871" id="Seg_9416" s="T870">дрова-ACC</ta>
            <ta e="T872" id="Seg_9417" s="T871">рубить-MOM-PRS-3PL</ta>
            <ta e="T873" id="Seg_9418" s="T872">два-DU-ACC</ta>
            <ta e="T874" id="Seg_9419" s="T873">вниз</ta>
            <ta e="T875" id="Seg_9420" s="T874">занести-HAB-DUR-3PL</ta>
            <ta e="T876" id="Seg_9421" s="T875">огонь-INSTR</ta>
            <ta e="T877" id="Seg_9422" s="T876">зажечь-3PL</ta>
            <ta e="T878" id="Seg_9423" s="T877">место-CAR-ADVZ</ta>
            <ta e="T879" id="Seg_9424" s="T878">огонь-INSTR</ta>
            <ta e="T880" id="Seg_9425" s="T879">зажечь-3PL</ta>
            <ta e="T881" id="Seg_9426" s="T880">старик-ADJZ</ta>
            <ta e="T882" id="Seg_9427" s="T881">отец-CRС-GEN</ta>
            <ta e="T883" id="Seg_9428" s="T882">этот</ta>
            <ta e="T884" id="Seg_9429" s="T883">старик.[NOM]</ta>
            <ta e="T885" id="Seg_9430" s="T884">дочь.[NOM]-3SG</ta>
            <ta e="T886" id="Seg_9431" s="T885">быть-PST.NAR.[3SG.S]</ta>
            <ta e="T887" id="Seg_9432" s="T886">этот</ta>
            <ta e="T888" id="Seg_9433" s="T887">дочь-ACC-3SG</ta>
            <ta e="T889" id="Seg_9434" s="T888">Сыльча_Пыльча.[NOM]</ta>
            <ta e="T890" id="Seg_9435" s="T889">половина-ILL</ta>
            <ta e="T891" id="Seg_9436" s="T890">дать-PRS-3SG.O</ta>
            <ta e="T892" id="Seg_9437" s="T891">этот</ta>
            <ta e="T893" id="Seg_9438" s="T892">земля-GEN</ta>
            <ta e="T894" id="Seg_9439" s="T893">конец.[NOM]</ta>
            <ta e="T895" id="Seg_9440" s="T894">повернуться-TR-IPFV-DUR-3SG.O</ta>
            <ta e="T896" id="Seg_9441" s="T895">селькуп-EP-PL-EP-ALL</ta>
            <ta e="T897" id="Seg_9442" s="T896">прийти-PRS.[3SG.S]</ta>
            <ta e="T898" id="Seg_9443" s="T897">конец.[NOM]-3SG</ta>
            <ta e="T899" id="Seg_9444" s="T898">INFER</ta>
            <ta e="T900" id="Seg_9445" s="T899">быть-INFER.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_9446" s="T0">nprop-n:case3</ta>
            <ta e="T2" id="Seg_9447" s="T1">nprop-n:case3</ta>
            <ta e="T3" id="Seg_9448" s="T2">n-n:case3</ta>
            <ta e="T4" id="Seg_9449" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_9450" s="T4">n-n:poss-n&gt;adj</ta>
            <ta e="T6" id="Seg_9451" s="T5">v-v:tense-v:mood2-v:pn</ta>
            <ta e="T7" id="Seg_9452" s="T6">n-n:case1-n:poss</ta>
            <ta e="T8" id="Seg_9453" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_9454" s="T8">v-v:pn</ta>
            <ta e="T10" id="Seg_9455" s="T9">v-v:pn</ta>
            <ta e="T11" id="Seg_9456" s="T10">num</ta>
            <ta e="T12" id="Seg_9457" s="T11">n-n&gt;adv</ta>
            <ta e="T13" id="Seg_9458" s="T12">n-n:case3</ta>
            <ta e="T14" id="Seg_9459" s="T13">adv</ta>
            <ta e="T15" id="Seg_9460" s="T14">v-v:pn</ta>
            <ta e="T16" id="Seg_9461" s="T15">v-v:pn</ta>
            <ta e="T17" id="Seg_9462" s="T16">n-n:case3</ta>
            <ta e="T18" id="Seg_9463" s="T17">adv</ta>
            <ta e="T19" id="Seg_9464" s="T18">dem-adj&gt;adv</ta>
            <ta e="T20" id="Seg_9465" s="T19">v-v:pn</ta>
            <ta e="T21" id="Seg_9466" s="T20">pers</ta>
            <ta e="T22" id="Seg_9467" s="T21">n-n:(ins)-n&gt;adj</ta>
            <ta e="T23" id="Seg_9468" s="T22">v-v&gt;v-v&gt;adv</ta>
            <ta e="T24" id="Seg_9469" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_9470" s="T24">adv</ta>
            <ta e="T26" id="Seg_9471" s="T25">conj</ta>
            <ta e="T27" id="Seg_9472" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_9473" s="T27">n-n:case3</ta>
            <ta e="T29" id="Seg_9474" s="T28">pers</ta>
            <ta e="T30" id="Seg_9475" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_9476" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_9477" s="T31">dem</ta>
            <ta e="T33" id="Seg_9478" s="T32">n-n:case3</ta>
            <ta e="T34" id="Seg_9479" s="T33">n-n:case3</ta>
            <ta e="T35" id="Seg_9480" s="T34">n-n:(ins)-n:case3</ta>
            <ta e="T36" id="Seg_9481" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_9482" s="T36">v-v:pn</ta>
            <ta e="T38" id="Seg_9483" s="T37">n-n:case1-n:poss</ta>
            <ta e="T39" id="Seg_9484" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_9485" s="T39">pers</ta>
            <ta e="T41" id="Seg_9486" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_9487" s="T41">interrog-n&gt;adj</ta>
            <ta e="T43" id="Seg_9488" s="T42">n-n:case3</ta>
            <ta e="T44" id="Seg_9489" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_9490" s="T44">v-v:pn</ta>
            <ta e="T46" id="Seg_9491" s="T45">dem</ta>
            <ta e="T47" id="Seg_9492" s="T46">n-n:case3</ta>
            <ta e="T48" id="Seg_9493" s="T47">n-n:case3</ta>
            <ta e="T49" id="Seg_9494" s="T48">n-n&gt;adv</ta>
            <ta e="T50" id="Seg_9495" s="T49">n-n:case3</ta>
            <ta e="T51" id="Seg_9496" s="T50">v-v:pn</ta>
            <ta e="T52" id="Seg_9497" s="T51">n-n:case1-n:poss</ta>
            <ta e="T53" id="Seg_9498" s="T52">n-n:case3-clit</ta>
            <ta e="T54" id="Seg_9499" s="T53">dem-n&gt;adj</ta>
            <ta e="T55" id="Seg_9500" s="T54">v-v:pn</ta>
            <ta e="T56" id="Seg_9501" s="T55">pers</ta>
            <ta e="T57" id="Seg_9502" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_9503" s="T57">pro-n:case3</ta>
            <ta e="T59" id="Seg_9504" s="T58">v-v:mood-pn</ta>
            <ta e="T60" id="Seg_9505" s="T59">pers</ta>
            <ta e="T61" id="Seg_9506" s="T60">adv</ta>
            <ta e="T62" id="Seg_9507" s="T61">n-n:(ins)-n&gt;adj</ta>
            <ta e="T63" id="Seg_9508" s="T62">v-v&gt;v-v:ins-v&gt;adv</ta>
            <ta e="T64" id="Seg_9509" s="T63">v-v:mood-v:pn</ta>
            <ta e="T65" id="Seg_9510" s="T64">dem</ta>
            <ta e="T66" id="Seg_9511" s="T65">n-n:case3</ta>
            <ta e="T67" id="Seg_9512" s="T66">n-n:case3</ta>
            <ta e="T68" id="Seg_9513" s="T67">nprop-n:case3</ta>
            <ta e="T69" id="Seg_9514" s="T68">n-n:case3</ta>
            <ta e="T70" id="Seg_9515" s="T69">adv</ta>
            <ta e="T71" id="Seg_9516" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_9517" s="T71">nprop-n:case3</ta>
            <ta e="T73" id="Seg_9518" s="T72">n-n:case3</ta>
            <ta e="T74" id="Seg_9519" s="T73">quant</ta>
            <ta e="T75" id="Seg_9520" s="T74">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_9521" s="T75">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T77" id="Seg_9522" s="T76">conj</ta>
            <ta e="T78" id="Seg_9523" s="T77">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T79" id="Seg_9524" s="T78">n-n:case3</ta>
            <ta e="T80" id="Seg_9525" s="T79">n-n:case3</ta>
            <ta e="T81" id="Seg_9526" s="T80">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T82" id="Seg_9527" s="T81">adj</ta>
            <ta e="T83" id="Seg_9528" s="T82">n-n:case3</ta>
            <ta e="T84" id="Seg_9529" s="T83">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T85" id="Seg_9530" s="T84">adj</ta>
            <ta e="T86" id="Seg_9531" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_9532" s="T86">interrog-n&gt;adj</ta>
            <ta e="T88" id="Seg_9533" s="T87">n-n:case3</ta>
            <ta e="T89" id="Seg_9534" s="T88">v-v:pn</ta>
            <ta e="T90" id="Seg_9535" s="T89">conj</ta>
            <ta e="T91" id="Seg_9536" s="T90">n-n&gt;adj</ta>
            <ta e="T92" id="Seg_9537" s="T91">n-n:case3</ta>
            <ta e="T93" id="Seg_9538" s="T92">n-n&gt;adv</ta>
            <ta e="T94" id="Seg_9539" s="T93">v-v&gt;v-v:pn</ta>
            <ta e="T95" id="Seg_9540" s="T94">n-n:poss-case</ta>
            <ta e="T96" id="Seg_9541" s="T95">n-n:case3</ta>
            <ta e="T97" id="Seg_9542" s="T96">n-n&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T98" id="Seg_9543" s="T97">conj</ta>
            <ta e="T99" id="Seg_9544" s="T98">n-n:case3</ta>
            <ta e="T100" id="Seg_9545" s="T99">num</ta>
            <ta e="T101" id="Seg_9546" s="T100">n-n:case3</ta>
            <ta e="T102" id="Seg_9547" s="T101">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_9548" s="T102">n-n:case3</ta>
            <ta e="T104" id="Seg_9549" s="T103">adj-adj&gt;adj</ta>
            <ta e="T105" id="Seg_9550" s="T104">n-n:case3</ta>
            <ta e="T106" id="Seg_9551" s="T105">dem</ta>
            <ta e="T107" id="Seg_9552" s="T106">n-n:case3</ta>
            <ta e="T108" id="Seg_9553" s="T107">n-n:case3</ta>
            <ta e="T109" id="Seg_9554" s="T108">v-v:tense-v:mood2-v:pn</ta>
            <ta e="T110" id="Seg_9555" s="T109">dem</ta>
            <ta e="T111" id="Seg_9556" s="T110">n-n:case3</ta>
            <ta e="T112" id="Seg_9557" s="T111">n-n:case3</ta>
            <ta e="T113" id="Seg_9558" s="T112">v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_9559" s="T113">num</ta>
            <ta e="T115" id="Seg_9560" s="T114">n-n:case3</ta>
            <ta e="T116" id="Seg_9561" s="T115">clit</ta>
            <ta e="T117" id="Seg_9562" s="T116">interrog-n:case3</ta>
            <ta e="T118" id="Seg_9563" s="T117">v-v:tense-mood-v:pn</ta>
            <ta e="T119" id="Seg_9564" s="T118">interj</ta>
            <ta e="T120" id="Seg_9565" s="T119">interj</ta>
            <ta e="T121" id="Seg_9566" s="T120">interj</ta>
            <ta e="T122" id="Seg_9567" s="T121">interj</ta>
            <ta e="T123" id="Seg_9568" s="T122">v-v&gt;v-v:pn</ta>
            <ta e="T124" id="Seg_9569" s="T123">dem-n&gt;adj</ta>
            <ta e="T125" id="Seg_9570" s="T124">n-n:case3</ta>
            <ta e="T126" id="Seg_9571" s="T125">v-v:tense-v:pn</ta>
            <ta e="T127" id="Seg_9572" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_9573" s="T127">interrog-n:case3</ta>
            <ta e="T129" id="Seg_9574" s="T128">n-n:case3</ta>
            <ta e="T130" id="Seg_9575" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_9576" s="T130">interrog-n:case3</ta>
            <ta e="T132" id="Seg_9577" s="T131">v-v:pn</ta>
            <ta e="T133" id="Seg_9578" s="T132">dem</ta>
            <ta e="T134" id="Seg_9579" s="T133">n-n:case3</ta>
            <ta e="T135" id="Seg_9580" s="T134">n-n:case3</ta>
            <ta e="T136" id="Seg_9581" s="T135">v-v&gt;v-v&gt;n-n:poss-case</ta>
            <ta e="T137" id="Seg_9582" s="T136">expl</ta>
            <ta e="T138" id="Seg_9583" s="T137">interrog-n:case3</ta>
            <ta e="T139" id="Seg_9584" s="T138">v-v&gt;v-v:tense-mood-v:pn</ta>
            <ta e="T140" id="Seg_9585" s="T139">pers</ta>
            <ta e="T141" id="Seg_9586" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_9587" s="T141">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T143" id="Seg_9588" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_9589" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_9590" s="T144">v-v&gt;v-v:tense-mood-v:pn</ta>
            <ta e="T146" id="Seg_9591" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_9592" s="T146">v-v&gt;v-v:tense-mood-v:pn</ta>
            <ta e="T148" id="Seg_9593" s="T147">adv</ta>
            <ta e="T149" id="Seg_9594" s="T148">ptcl</ta>
            <ta e="T150" id="Seg_9595" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_9596" s="T150">n-n:case3</ta>
            <ta e="T152" id="Seg_9597" s="T151">pp-n:case3</ta>
            <ta e="T153" id="Seg_9598" s="T152">adv-adv:advcase</ta>
            <ta e="T154" id="Seg_9599" s="T153">v-v:pn</ta>
            <ta e="T155" id="Seg_9600" s="T154">adv</ta>
            <ta e="T156" id="Seg_9601" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_9602" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_9603" s="T157">v-v&gt;v-v&gt;v-v:mood2-v:pn</ta>
            <ta e="T159" id="Seg_9604" s="T158">interj</ta>
            <ta e="T160" id="Seg_9605" s="T159">interj</ta>
            <ta e="T161" id="Seg_9606" s="T160">interj</ta>
            <ta e="T162" id="Seg_9607" s="T161">interj</ta>
            <ta e="T163" id="Seg_9608" s="T162">dem</ta>
            <ta e="T164" id="Seg_9609" s="T163">n-n:case1-n:poss</ta>
            <ta e="T165" id="Seg_9610" s="T164">interrog-n&gt;adj</ta>
            <ta e="T166" id="Seg_9611" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_9612" s="T166">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T168" id="Seg_9613" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_9614" s="T168">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T170" id="Seg_9615" s="T169">adv-adv:advcase</ta>
            <ta e="T171" id="Seg_9616" s="T170">n-n:case3</ta>
            <ta e="T172" id="Seg_9617" s="T171">n-n:case3</ta>
            <ta e="T173" id="Seg_9618" s="T172">clit</ta>
            <ta e="T174" id="Seg_9619" s="T173">interrog-n:case3</ta>
            <ta e="T175" id="Seg_9620" s="T174">adj-adj&gt;v-v&gt;v-v:pn</ta>
            <ta e="T176" id="Seg_9621" s="T175">pro-n:case3</ta>
            <ta e="T177" id="Seg_9622" s="T176">adj</ta>
            <ta e="T178" id="Seg_9623" s="T177">v-v:tense-v:pn</ta>
            <ta e="T179" id="Seg_9624" s="T178">interj</ta>
            <ta e="T180" id="Seg_9625" s="T179">interj</ta>
            <ta e="T181" id="Seg_9626" s="T180">interj</ta>
            <ta e="T182" id="Seg_9627" s="T181">interj</ta>
            <ta e="T183" id="Seg_9628" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_9629" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_9630" s="T184">n-n:(ins)-n:case3</ta>
            <ta e="T186" id="Seg_9631" s="T185">n-n:case3</ta>
            <ta e="T187" id="Seg_9632" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_9633" s="T187">adv</ta>
            <ta e="T189" id="Seg_9634" s="T188">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T190" id="Seg_9635" s="T189">adv</ta>
            <ta e="T191" id="Seg_9636" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_9637" s="T191">v-v:tense-mood-v:pn</ta>
            <ta e="T193" id="Seg_9638" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_9639" s="T193">dem-adj&gt;adv</ta>
            <ta e="T195" id="Seg_9640" s="T194">n-n:case3</ta>
            <ta e="T196" id="Seg_9641" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_9642" s="T196">adv</ta>
            <ta e="T198" id="Seg_9643" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_9644" s="T198">v-v&gt;v-v:pn</ta>
            <ta e="T200" id="Seg_9645" s="T199">v-v&gt;adv</ta>
            <ta e="T201" id="Seg_9646" s="T200">n-n:case3</ta>
            <ta e="T202" id="Seg_9647" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_9648" s="T202">n-n:case3</ta>
            <ta e="T204" id="Seg_9649" s="T203">dem</ta>
            <ta e="T205" id="Seg_9650" s="T204">n-n:case1-n:poss</ta>
            <ta e="T206" id="Seg_9651" s="T205">n-n:case1-n:poss</ta>
            <ta e="T207" id="Seg_9652" s="T206">n-n:case3</ta>
            <ta e="T208" id="Seg_9653" s="T207">adj</ta>
            <ta e="T209" id="Seg_9654" s="T208">n-n:case3</ta>
            <ta e="T210" id="Seg_9655" s="T209">pers-n:case3</ta>
            <ta e="T211" id="Seg_9656" s="T210">n-n:case1-n:poss</ta>
            <ta e="T212" id="Seg_9657" s="T211">n-n:case1-n:poss</ta>
            <ta e="T213" id="Seg_9658" s="T212">n-n:case3</ta>
            <ta e="T214" id="Seg_9659" s="T213">adj</ta>
            <ta e="T215" id="Seg_9660" s="T214">v-v:pn</ta>
            <ta e="T216" id="Seg_9661" s="T215">dem</ta>
            <ta e="T217" id="Seg_9662" s="T216">n-n:case3</ta>
            <ta e="T218" id="Seg_9663" s="T217">n-n:case3</ta>
            <ta e="T219" id="Seg_9664" s="T218">dem</ta>
            <ta e="T220" id="Seg_9665" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_9666" s="T220">dem</ta>
            <ta e="T222" id="Seg_9667" s="T221">n-n:case3</ta>
            <ta e="T223" id="Seg_9668" s="T222">v-v&gt;v-v:tense-v:mood2-v:pn</ta>
            <ta e="T224" id="Seg_9669" s="T223">pers</ta>
            <ta e="T225" id="Seg_9670" s="T224">interrog</ta>
            <ta e="T226" id="Seg_9671" s="T225">dem-n&gt;adj</ta>
            <ta e="T227" id="Seg_9672" s="T226">v-v:pn</ta>
            <ta e="T228" id="Seg_9673" s="T227">ptcl</ta>
            <ta e="T229" id="Seg_9674" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_9675" s="T229">pers</ta>
            <ta e="T231" id="Seg_9676" s="T230">n-n:case3</ta>
            <ta e="T232" id="Seg_9677" s="T231">pers</ta>
            <ta e="T233" id="Seg_9678" s="T232">v-v:inf</ta>
            <ta e="T234" id="Seg_9679" s="T233">v-v:pn</ta>
            <ta e="T235" id="Seg_9680" s="T234">adv-adv&gt;adv</ta>
            <ta e="T236" id="Seg_9681" s="T235">n-n:num-n:case1-n:poss</ta>
            <ta e="T237" id="Seg_9682" s="T236">v-v:tense-v:pn</ta>
            <ta e="T238" id="Seg_9683" s="T237">pro-n:num-n:case3</ta>
            <ta e="T239" id="Seg_9684" s="T238">pers</ta>
            <ta e="T240" id="Seg_9685" s="T239">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_9686" s="T240">n-n:case3</ta>
            <ta e="T242" id="Seg_9687" s="T241">pers</ta>
            <ta e="T243" id="Seg_9688" s="T242">v-v:inf</ta>
            <ta e="T244" id="Seg_9689" s="T243">adv</ta>
            <ta e="T245" id="Seg_9690" s="T244">pers</ta>
            <ta e="T246" id="Seg_9691" s="T245">adv</ta>
            <ta e="T247" id="Seg_9692" s="T246">v-v:tense-v:pn</ta>
            <ta e="T248" id="Seg_9693" s="T247">n-n:case3</ta>
            <ta e="T249" id="Seg_9694" s="T248">quant</ta>
            <ta e="T250" id="Seg_9695" s="T249">pers</ta>
            <ta e="T251" id="Seg_9696" s="T250">v-v:tense-v:pn</ta>
            <ta e="T252" id="Seg_9697" s="T251">ptcl</ta>
            <ta e="T253" id="Seg_9698" s="T252">interrog-n:case3</ta>
            <ta e="T254" id="Seg_9699" s="T253">adj</ta>
            <ta e="T255" id="Seg_9700" s="T254">v-v&gt;v-v:pn</ta>
            <ta e="T256" id="Seg_9701" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_9702" s="T256">interrog</ta>
            <ta e="T258" id="Seg_9703" s="T257">pers</ta>
            <ta e="T259" id="Seg_9704" s="T258">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T260" id="Seg_9705" s="T259">pers</ta>
            <ta e="T261" id="Seg_9706" s="T260">adv</ta>
            <ta e="T262" id="Seg_9707" s="T261">v-v:mood-pn</ta>
            <ta e="T263" id="Seg_9708" s="T262">pers</ta>
            <ta e="T264" id="Seg_9709" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_9710" s="T264">adv</ta>
            <ta e="T266" id="Seg_9711" s="T265">v-v:mood-pn</ta>
            <ta e="T267" id="Seg_9712" s="T266">pers</ta>
            <ta e="T268" id="Seg_9713" s="T267">interrog</ta>
            <ta e="T269" id="Seg_9714" s="T268">v-v:tense-v:pn</ta>
            <ta e="T270" id="Seg_9715" s="T269">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T271" id="Seg_9716" s="T270">pers</ta>
            <ta e="T272" id="Seg_9717" s="T271">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T273" id="Seg_9718" s="T272">pers</ta>
            <ta e="T274" id="Seg_9719" s="T273">v-v:tense-v:pn</ta>
            <ta e="T275" id="Seg_9720" s="T274">pers</ta>
            <ta e="T276" id="Seg_9721" s="T275">adv</ta>
            <ta e="T277" id="Seg_9722" s="T276">v-v:mood-pn</ta>
            <ta e="T278" id="Seg_9723" s="T277">adv</ta>
            <ta e="T279" id="Seg_9724" s="T278">ptcl</ta>
            <ta e="T280" id="Seg_9725" s="T279">v-v:mood2-v:pn</ta>
            <ta e="T281" id="Seg_9726" s="T280">nprop-n:case3</ta>
            <ta e="T282" id="Seg_9727" s="T281">n-n:case3</ta>
            <ta e="T283" id="Seg_9728" s="T282">n-n:case3</ta>
            <ta e="T284" id="Seg_9729" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_9730" s="T284">v-v&gt;v-v:tense-mood-v:pn</ta>
            <ta e="T286" id="Seg_9731" s="T285">n-n:obl.poss-n:case2</ta>
            <ta e="T287" id="Seg_9732" s="T286">pers</ta>
            <ta e="T288" id="Seg_9733" s="T287">ptcl</ta>
            <ta e="T289" id="Seg_9734" s="T288">pers</ta>
            <ta e="T290" id="Seg_9735" s="T289">interrog</ta>
            <ta e="T291" id="Seg_9736" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_9737" s="T291">pers</ta>
            <ta e="T293" id="Seg_9738" s="T292">v-v:mood-pn</ta>
            <ta e="T294" id="Seg_9739" s="T293">n-n:case3</ta>
            <ta e="T295" id="Seg_9740" s="T294">pers</ta>
            <ta e="T296" id="Seg_9741" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_9742" s="T296">v-v:tense-v:mood2-v:pn</ta>
            <ta e="T298" id="Seg_9743" s="T297">nprop-n:case1-n:poss</ta>
            <ta e="T299" id="Seg_9744" s="T298">n-n:case3</ta>
            <ta e="T300" id="Seg_9745" s="T299">interrog</ta>
            <ta e="T301" id="Seg_9746" s="T300">qv-v:tense-v:pn</ta>
            <ta e="T302" id="Seg_9747" s="T301">pers</ta>
            <ta e="T303" id="Seg_9748" s="T302">interrog-clit</ta>
            <ta e="T304" id="Seg_9749" s="T303">pers</ta>
            <ta e="T305" id="Seg_9750" s="T304">v-v:mood-pn</ta>
            <ta e="T306" id="Seg_9751" s="T305">dem-n&gt;adj</ta>
            <ta e="T307" id="Seg_9752" s="T306">v-v:tense-v:pn</ta>
            <ta e="T308" id="Seg_9753" s="T307">n-n:case3</ta>
            <ta e="T309" id="Seg_9754" s="T308">adv</ta>
            <ta e="T310" id="Seg_9755" s="T309">ptcl</ta>
            <ta e="T311" id="Seg_9756" s="T310">v-v:tense-mood-v:pn</ta>
            <ta e="T312" id="Seg_9757" s="T311">adv</ta>
            <ta e="T313" id="Seg_9758" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_9759" s="T313">n-n&gt;adj</ta>
            <ta e="T315" id="Seg_9760" s="T314">n-n:case3</ta>
            <ta e="T316" id="Seg_9761" s="T315">pro-n:case3</ta>
            <ta e="T317" id="Seg_9762" s="T316">n-n:case3</ta>
            <ta e="T318" id="Seg_9763" s="T317">n-n:case3</ta>
            <ta e="T319" id="Seg_9764" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_9765" s="T319">n-n:case3</ta>
            <ta e="T321" id="Seg_9766" s="T320">ptcl</ta>
            <ta e="T322" id="Seg_9767" s="T321">v-v:tense-mood-v:pn</ta>
            <ta e="T323" id="Seg_9768" s="T322">n-n:case3</ta>
            <ta e="T324" id="Seg_9769" s="T323">v-v:mood-pn</ta>
            <ta e="T325" id="Seg_9770" s="T324">interrog</ta>
            <ta e="T326" id="Seg_9771" s="T325">v-v:ins-v:pn</ta>
            <ta e="T327" id="Seg_9772" s="T326">v-v:tense-v:pn</ta>
            <ta e="T328" id="Seg_9773" s="T327">pers</ta>
            <ta e="T329" id="Seg_9774" s="T328">conj</ta>
            <ta e="T330" id="Seg_9775" s="T329">v-v:tense-v:pn</ta>
            <ta e="T331" id="Seg_9776" s="T330">n-n:num-n:case1-n:poss</ta>
            <ta e="T332" id="Seg_9777" s="T331">pers</ta>
            <ta e="T333" id="Seg_9778" s="T332">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T334" id="Seg_9779" s="T333">pers</ta>
            <ta e="T335" id="Seg_9780" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_9781" s="T335">n-n:case3</ta>
            <ta e="T337" id="Seg_9782" s="T336">v-v:mood-pn</ta>
            <ta e="T338" id="Seg_9783" s="T337">dem</ta>
            <ta e="T339" id="Seg_9784" s="T338">n-n:case3</ta>
            <ta e="T340" id="Seg_9785" s="T339">n-n:case3</ta>
            <ta e="T341" id="Seg_9786" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_9787" s="T341">n-n:case3</ta>
            <ta e="T343" id="Seg_9788" s="T342">v-v:tense-mood-v:pn</ta>
            <ta e="T344" id="Seg_9789" s="T343">pers</ta>
            <ta e="T345" id="Seg_9790" s="T344">ptcl</ta>
            <ta e="T346" id="Seg_9791" s="T345">pers</ta>
            <ta e="T347" id="Seg_9792" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_9793" s="T347">pers</ta>
            <ta e="T349" id="Seg_9794" s="T348">v-v:mood-pn</ta>
            <ta e="T350" id="Seg_9795" s="T349">n-n:case3</ta>
            <ta e="T351" id="Seg_9796" s="T350">v-v:ins-v:pn</ta>
            <ta e="T352" id="Seg_9797" s="T351">n-n:case3</ta>
            <ta e="T353" id="Seg_9798" s="T352">ptcl</ta>
            <ta e="T354" id="Seg_9799" s="T353">v-v:pn</ta>
            <ta e="T355" id="Seg_9800" s="T354">dem</ta>
            <ta e="T356" id="Seg_9801" s="T355">n-n:case3</ta>
            <ta e="T357" id="Seg_9802" s="T356">adv</ta>
            <ta e="T358" id="Seg_9803" s="T357">quant</ta>
            <ta e="T359" id="Seg_9804" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_9805" s="T359">v-v&gt;v-v:tense-mood-v:pn</ta>
            <ta e="T361" id="Seg_9806" s="T360">clit</ta>
            <ta e="T362" id="Seg_9807" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_9808" s="T362">interrog</ta>
            <ta e="T364" id="Seg_9809" s="T363">v-v:tense-v:pn</ta>
            <ta e="T365" id="Seg_9810" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_9811" s="T365">ptcl</ta>
            <ta e="T367" id="Seg_9812" s="T366">n-n:case1-n:poss</ta>
            <ta e="T368" id="Seg_9813" s="T367">n-n:case3</ta>
            <ta e="T369" id="Seg_9814" s="T368">pers</ta>
            <ta e="T370" id="Seg_9815" s="T369">v-v:tense-v:pn</ta>
            <ta e="T371" id="Seg_9816" s="T370">clit</ta>
            <ta e="T372" id="Seg_9817" s="T371">interrog-n:(ins)-n&gt;adj</ta>
            <ta e="T373" id="Seg_9818" s="T372">n-n:case3</ta>
            <ta e="T374" id="Seg_9819" s="T373">v-v:tense-v:pn</ta>
            <ta e="T375" id="Seg_9820" s="T374">pers</ta>
            <ta e="T376" id="Seg_9821" s="T375">n-n:case3</ta>
            <ta e="T377" id="Seg_9822" s="T376">pers</ta>
            <ta e="T378" id="Seg_9823" s="T377">v-v:tense-v:pn</ta>
            <ta e="T379" id="Seg_9824" s="T378">adv</ta>
            <ta e="T380" id="Seg_9825" s="T379">pers</ta>
            <ta e="T381" id="Seg_9826" s="T380">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T382" id="Seg_9827" s="T381">pers-n:case3</ta>
            <ta e="T383" id="Seg_9828" s="T382">ptcl</ta>
            <ta e="T384" id="Seg_9829" s="T383">v-v:tense-v:pn</ta>
            <ta e="T385" id="Seg_9830" s="T384">n-n:case3</ta>
            <ta e="T386" id="Seg_9831" s="T385">adv-adv&gt;adj</ta>
            <ta e="T387" id="Seg_9832" s="T386">n-n:case3</ta>
            <ta e="T388" id="Seg_9833" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_9834" s="T388">num</ta>
            <ta e="T390" id="Seg_9835" s="T389">adj</ta>
            <ta e="T391" id="Seg_9836" s="T390">n-n:case3</ta>
            <ta e="T392" id="Seg_9837" s="T391">v-v:tense-mood-v:pn</ta>
            <ta e="T393" id="Seg_9838" s="T392">adv</ta>
            <ta e="T394" id="Seg_9839" s="T393">interrog-n:case3</ta>
            <ta e="T395" id="Seg_9840" s="T394">dem-adj&gt;adv</ta>
            <ta e="T396" id="Seg_9841" s="T395">v-v:pn</ta>
            <ta e="T397" id="Seg_9842" s="T396">n-n:(ins)-n:case3</ta>
            <ta e="T398" id="Seg_9843" s="T397">preverb</ta>
            <ta e="T399" id="Seg_9844" s="T398">v-v&gt;v-v:pn</ta>
            <ta e="T400" id="Seg_9845" s="T399">adv</ta>
            <ta e="T401" id="Seg_9846" s="T400">adv</ta>
            <ta e="T402" id="Seg_9847" s="T401">v-v&gt;adv</ta>
            <ta e="T403" id="Seg_9848" s="T402">v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T404" id="Seg_9849" s="T403">n-n:case3</ta>
            <ta e="T405" id="Seg_9850" s="T404">interrog-n&gt;adj</ta>
            <ta e="T406" id="Seg_9851" s="T405">n-n:case3</ta>
            <ta e="T407" id="Seg_9852" s="T406">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T408" id="Seg_9853" s="T407">n-n:case3</ta>
            <ta e="T409" id="Seg_9854" s="T408">conj</ta>
            <ta e="T410" id="Seg_9855" s="T409">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T411" id="Seg_9856" s="T410">n-n:case3</ta>
            <ta e="T412" id="Seg_9857" s="T411">v-v:mood-pn</ta>
            <ta e="T413" id="Seg_9858" s="T412">num</ta>
            <ta e="T414" id="Seg_9859" s="T413">n-n:case3</ta>
            <ta e="T415" id="Seg_9860" s="T414">v-v&gt;v-v&gt;v-v:mood-pn</ta>
            <ta e="T416" id="Seg_9861" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_9862" s="T416">n-n&gt;adj</ta>
            <ta e="T418" id="Seg_9863" s="T417">n-n:case1-n:poss</ta>
            <ta e="T419" id="Seg_9864" s="T418">v-v:ins-v&gt;v-v&gt;v-v:tense-mood-v:pn</ta>
            <ta e="T420" id="Seg_9865" s="T419">adv</ta>
            <ta e="T421" id="Seg_9866" s="T420">v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T422" id="Seg_9867" s="T421">v-v&gt;v-v&gt;v-v:tense-mood-v:pn</ta>
            <ta e="T423" id="Seg_9868" s="T422">adv</ta>
            <ta e="T424" id="Seg_9869" s="T423">v-v:tense-mood-v:pn</ta>
            <ta e="T425" id="Seg_9870" s="T424">num</ta>
            <ta e="T426" id="Seg_9871" s="T425">adj</ta>
            <ta e="T427" id="Seg_9872" s="T426">n-n:case3</ta>
            <ta e="T428" id="Seg_9873" s="T427">n-n:case3</ta>
            <ta e="T429" id="Seg_9874" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_9875" s="T429">v-v:tense-mood-v:pn</ta>
            <ta e="T431" id="Seg_9876" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_9877" s="T431">n-n&gt;adv</ta>
            <ta e="T433" id="Seg_9878" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_9879" s="T433">v-v&gt;v-v:tense-mood-v:pn</ta>
            <ta e="T435" id="Seg_9880" s="T434">n-n:case3</ta>
            <ta e="T436" id="Seg_9881" s="T435">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T437" id="Seg_9882" s="T436">n-n:case3</ta>
            <ta e="T438" id="Seg_9883" s="T437">ptcl</ta>
            <ta e="T439" id="Seg_9884" s="T438">v-v&gt;v-v:tense-mood-v:pn</ta>
            <ta e="T440" id="Seg_9885" s="T439">conj</ta>
            <ta e="T441" id="Seg_9886" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_9887" s="T441">adv</ta>
            <ta e="T443" id="Seg_9888" s="T442">v-v&gt;adv</ta>
            <ta e="T444" id="Seg_9889" s="T443">v-v:mood-pn</ta>
            <ta e="T445" id="Seg_9890" s="T444">ptcl</ta>
            <ta e="T446" id="Seg_9891" s="T445">interrog</ta>
            <ta e="T447" id="Seg_9892" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_9893" s="T447">v-v:mood-pn</ta>
            <ta e="T449" id="Seg_9894" s="T448">nprop-n:case1-n:poss</ta>
            <ta e="T450" id="Seg_9895" s="T449">n-n:case3</ta>
            <ta e="T451" id="Seg_9896" s="T450">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T452" id="Seg_9897" s="T451">interrog-n:case3</ta>
            <ta e="T453" id="Seg_9898" s="T452">adv</ta>
            <ta e="T454" id="Seg_9899" s="T453">ptcl</ta>
            <ta e="T455" id="Seg_9900" s="T454">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T456" id="Seg_9901" s="T455">v-v:tense-mood-v:pn</ta>
            <ta e="T457" id="Seg_9902" s="T456">n-n:case3</ta>
            <ta e="T458" id="Seg_9903" s="T457">v-v&gt;v-v:pn</ta>
            <ta e="T459" id="Seg_9904" s="T458">n-n:case3</ta>
            <ta e="T460" id="Seg_9905" s="T459">interj</ta>
            <ta e="T461" id="Seg_9906" s="T460">interj</ta>
            <ta e="T462" id="Seg_9907" s="T461">ptcl</ta>
            <ta e="T463" id="Seg_9908" s="T462">nprop-n:case3</ta>
            <ta e="T464" id="Seg_9909" s="T463">n-n:case3</ta>
            <ta e="T465" id="Seg_9910" s="T464">qv-v:tense-v:pn</ta>
            <ta e="T466" id="Seg_9911" s="T465">nprop-n:case3</ta>
            <ta e="T467" id="Seg_9912" s="T466">n-n:case3</ta>
            <ta e="T468" id="Seg_9913" s="T467">adv</ta>
            <ta e="T469" id="Seg_9914" s="T468">v-v:tense-v:pn</ta>
            <ta e="T470" id="Seg_9915" s="T469">adv</ta>
            <ta e="T471" id="Seg_9916" s="T470">ptcl</ta>
            <ta e="T472" id="Seg_9917" s="T471">v-v:tense-mood-v:pn</ta>
            <ta e="T473" id="Seg_9918" s="T472">n-n:case3</ta>
            <ta e="T474" id="Seg_9919" s="T473">v-v:tense-v:pn</ta>
            <ta e="T475" id="Seg_9920" s="T474">n-n:case3</ta>
            <ta e="T476" id="Seg_9921" s="T475">v-v:pn</ta>
            <ta e="T477" id="Seg_9922" s="T476">nprop-n:case3</ta>
            <ta e="T478" id="Seg_9923" s="T477">n-n:case3</ta>
            <ta e="T479" id="Seg_9924" s="T478">n-n:case3</ta>
            <ta e="T480" id="Seg_9925" s="T479">v-v:pn</ta>
            <ta e="T481" id="Seg_9926" s="T480">n-n:case3</ta>
            <ta e="T482" id="Seg_9927" s="T481">adv</ta>
            <ta e="T483" id="Seg_9928" s="T482">v-v&gt;v-v&gt;adv</ta>
            <ta e="T484" id="Seg_9929" s="T483">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T485" id="Seg_9930" s="T484">v-v:tense-v:pn</ta>
            <ta e="T486" id="Seg_9931" s="T485">n-n:case3</ta>
            <ta e="T487" id="Seg_9932" s="T486">v-v:pn</ta>
            <ta e="T488" id="Seg_9933" s="T487">ptcl</ta>
            <ta e="T489" id="Seg_9934" s="T488">interj</ta>
            <ta e="T490" id="Seg_9935" s="T489">interj</ta>
            <ta e="T491" id="Seg_9936" s="T490">adv</ta>
            <ta e="T492" id="Seg_9937" s="T491">v-v&gt;adv</ta>
            <ta e="T493" id="Seg_9938" s="T492">dem</ta>
            <ta e="T494" id="Seg_9939" s="T493">nprop-n:case3</ta>
            <ta e="T495" id="Seg_9940" s="T494">n-n:case3</ta>
            <ta e="T496" id="Seg_9941" s="T495">n-n:case1-n:poss</ta>
            <ta e="T497" id="Seg_9942" s="T496">v-v&gt;v-v&gt;adv</ta>
            <ta e="T498" id="Seg_9943" s="T497">v-v&gt;v-v:pn</ta>
            <ta e="T499" id="Seg_9944" s="T498">preverb</ta>
            <ta e="T500" id="Seg_9945" s="T499">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T501" id="Seg_9946" s="T500">adj</ta>
            <ta e="T502" id="Seg_9947" s="T501">n-n:case3</ta>
            <ta e="T503" id="Seg_9948" s="T502">v-v:tense-v:pn</ta>
            <ta e="T504" id="Seg_9949" s="T503">adv</ta>
            <ta e="T505" id="Seg_9950" s="T504">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T506" id="Seg_9951" s="T505">pers</ta>
            <ta e="T507" id="Seg_9952" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_9953" s="T507">pers</ta>
            <ta e="T509" id="Seg_9954" s="T508">ptcl</ta>
            <ta e="T510" id="Seg_9955" s="T509">interrog</ta>
            <ta e="T511" id="Seg_9956" s="T510">interrog-adv:advcase</ta>
            <ta e="T512" id="Seg_9957" s="T511">ptcl</ta>
            <ta e="T513" id="Seg_9958" s="T512">v-v:mood-pn</ta>
            <ta e="T514" id="Seg_9959" s="T513">adv</ta>
            <ta e="T515" id="Seg_9960" s="T514">v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T516" id="Seg_9961" s="T515">n-n:case1-n:(ins)-n&gt;adj</ta>
            <ta e="T517" id="Seg_9962" s="T516">n-n&gt;adv</ta>
            <ta e="T518" id="Seg_9963" s="T517">adv</ta>
            <ta e="T519" id="Seg_9964" s="T518">n-n:case1-n:poss</ta>
            <ta e="T520" id="Seg_9965" s="T519">pp</ta>
            <ta e="T521" id="Seg_9966" s="T520">ptcl</ta>
            <ta e="T522" id="Seg_9967" s="T521">v-v:tense-mood-v:pn</ta>
            <ta e="T523" id="Seg_9968" s="T522">n-n:case1-n:(ins)-n&gt;adj</ta>
            <ta e="T524" id="Seg_9969" s="T523">n-n&gt;adv</ta>
            <ta e="T525" id="Seg_9970" s="T524">n-n:(ins)-n:case3</ta>
            <ta e="T526" id="Seg_9971" s="T525">v-v&gt;v-v:pn</ta>
            <ta e="T527" id="Seg_9972" s="T526">adv</ta>
            <ta e="T528" id="Seg_9973" s="T527">v-v&gt;v-v:pn</ta>
            <ta e="T529" id="Seg_9974" s="T528">conj</ta>
            <ta e="T530" id="Seg_9975" s="T529">ptcl</ta>
            <ta e="T531" id="Seg_9976" s="T530">v-v:pn</ta>
            <ta e="T532" id="Seg_9977" s="T531">nprop-n:case3</ta>
            <ta e="T533" id="Seg_9978" s="T532">n-n:case3</ta>
            <ta e="T534" id="Seg_9979" s="T533">v-v&gt;v-v:pn</ta>
            <ta e="T535" id="Seg_9980" s="T534">v-v&gt;v-v&gt;v-v:mood-pn</ta>
            <ta e="T536" id="Seg_9981" s="T535">adv</ta>
            <ta e="T537" id="Seg_9982" s="T536">n-n:case3</ta>
            <ta e="T538" id="Seg_9983" s="T537">ptcl</ta>
            <ta e="T539" id="Seg_9984" s="T538">adj-adj&gt;adv</ta>
            <ta e="T540" id="Seg_9985" s="T539">v-v:tense-mood-v:pn</ta>
            <ta e="T541" id="Seg_9986" s="T540">n-n:case3</ta>
            <ta e="T542" id="Seg_9987" s="T541">dem</ta>
            <ta e="T543" id="Seg_9988" s="T542">v-v:tense-mood-v:pn</ta>
            <ta e="T544" id="Seg_9989" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_9990" s="T544">num</ta>
            <ta e="T546" id="Seg_9991" s="T545">n-n:case3</ta>
            <ta e="T547" id="Seg_9992" s="T546">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T548" id="Seg_9993" s="T547">n-n:case3</ta>
            <ta e="T549" id="Seg_9994" s="T548">ptcl</ta>
            <ta e="T550" id="Seg_9995" s="T549">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T551" id="Seg_9996" s="T550">ptcl</ta>
            <ta e="T552" id="Seg_9997" s="T551">nprop-n:case3</ta>
            <ta e="T553" id="Seg_9998" s="T552">pro-n:case3</ta>
            <ta e="T554" id="Seg_9999" s="T553">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T555" id="Seg_10000" s="T554">ptcl</ta>
            <ta e="T556" id="Seg_10001" s="T555">v-v:tense-mood-v:pn</ta>
            <ta e="T557" id="Seg_10002" s="T556">adv</ta>
            <ta e="T558" id="Seg_10003" s="T557">v-v&gt;adv</ta>
            <ta e="T559" id="Seg_10004" s="T558">pers-n:case3</ta>
            <ta e="T560" id="Seg_10005" s="T559">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T561" id="Seg_10006" s="T560">v-v&gt;adv</ta>
            <ta e="T562" id="Seg_10007" s="T561">pers-n:case3</ta>
            <ta e="T563" id="Seg_10008" s="T562">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T564" id="Seg_10009" s="T563">dem</ta>
            <ta e="T565" id="Seg_10010" s="T564">n-n:case3</ta>
            <ta e="T566" id="Seg_10011" s="T565">n-n:(ins)-n:case3</ta>
            <ta e="T567" id="Seg_10012" s="T566">ptcl</ta>
            <ta e="T568" id="Seg_10013" s="T567">v-v:tense-mood-v:pn</ta>
            <ta e="T569" id="Seg_10014" s="T568">pro-n:case3</ta>
            <ta e="T570" id="Seg_10015" s="T569">n-n:case3</ta>
            <ta e="T571" id="Seg_10016" s="T570">v-v&gt;adv</ta>
            <ta e="T572" id="Seg_10017" s="T571">ptcl</ta>
            <ta e="T573" id="Seg_10018" s="T572">v-v:tense-mood-v:pn</ta>
            <ta e="T574" id="Seg_10019" s="T573">ptcl</ta>
            <ta e="T575" id="Seg_10020" s="T574">interj</ta>
            <ta e="T576" id="Seg_10021" s="T575">interj</ta>
            <ta e="T577" id="Seg_10022" s="T576">interj</ta>
            <ta e="T578" id="Seg_10023" s="T577">ptcl</ta>
            <ta e="T579" id="Seg_10024" s="T578">nprop-n:case3</ta>
            <ta e="T580" id="Seg_10025" s="T579">n-n:case3</ta>
            <ta e="T581" id="Seg_10026" s="T580">qv-v:tense-v:pn</ta>
            <ta e="T582" id="Seg_10027" s="T581">ptcl</ta>
            <ta e="T583" id="Seg_10028" s="T582">n-n:case3</ta>
            <ta e="T584" id="Seg_10029" s="T583">v-v:tense-v:pn</ta>
            <ta e="T585" id="Seg_10030" s="T584">nprop-n:case3</ta>
            <ta e="T586" id="Seg_10031" s="T585">n-n:case3</ta>
            <ta e="T587" id="Seg_10032" s="T586">adv</ta>
            <ta e="T588" id="Seg_10033" s="T587">v-v:tense-v:pn</ta>
            <ta e="T589" id="Seg_10034" s="T588">pers</ta>
            <ta e="T590" id="Seg_10035" s="T589">adv</ta>
            <ta e="T591" id="Seg_10036" s="T590">v-v&gt;adv</ta>
            <ta e="T592" id="Seg_10037" s="T591">n-n&gt;adj</ta>
            <ta e="T593" id="Seg_10038" s="T592">v-v:mood-pn</ta>
            <ta e="T594" id="Seg_10039" s="T593">pro-n:case3</ta>
            <ta e="T595" id="Seg_10040" s="T594">v-v&gt;adv</ta>
            <ta e="T596" id="Seg_10041" s="T595">ptcl</ta>
            <ta e="T597" id="Seg_10042" s="T596">v-v:tense-mood-v:pn-clit</ta>
            <ta e="T598" id="Seg_10043" s="T597">n-n:case3</ta>
            <ta e="T599" id="Seg_10044" s="T598">n-n:(ins)-n&gt;adj</ta>
            <ta e="T600" id="Seg_10045" s="T599">v-v:tense-mood-v:pn-clit</ta>
            <ta e="T601" id="Seg_10046" s="T600">n-n:case3</ta>
            <ta e="T602" id="Seg_10047" s="T601">v-v:tense-v:pn</ta>
            <ta e="T603" id="Seg_10048" s="T602">qv-v:tense-v:pn</ta>
            <ta e="T604" id="Seg_10049" s="T603">nprop-n:case3</ta>
            <ta e="T605" id="Seg_10050" s="T604">n-n:case3</ta>
            <ta e="T606" id="Seg_10051" s="T605">pers-n:case3</ta>
            <ta e="T607" id="Seg_10052" s="T606">ptcl</ta>
            <ta e="T608" id="Seg_10053" s="T607">adv</ta>
            <ta e="T609" id="Seg_10054" s="T608">adv</ta>
            <ta e="T610" id="Seg_10055" s="T609">v-v:tense-v:pn</ta>
            <ta e="T611" id="Seg_10056" s="T610">v-v&gt;v-v:ins-v:tense-v:pn</ta>
            <ta e="T612" id="Seg_10057" s="T611">n-n:case3</ta>
            <ta e="T613" id="Seg_10058" s="T612">v-v&gt;adv</ta>
            <ta e="T614" id="Seg_10059" s="T613">n-n&gt;adj</ta>
            <ta e="T615" id="Seg_10060" s="T614">v-v:mood-pn</ta>
            <ta e="T616" id="Seg_10061" s="T615">num</ta>
            <ta e="T617" id="Seg_10062" s="T616">n-n:case3</ta>
            <ta e="T618" id="Seg_10063" s="T617">n-n:case3</ta>
            <ta e="T619" id="Seg_10064" s="T618">n-n:case3</ta>
            <ta e="T620" id="Seg_10065" s="T619">ptcl</ta>
            <ta e="T621" id="Seg_10066" s="T620">v-v&gt;v-v:tense-mood-v:pn</ta>
            <ta e="T622" id="Seg_10067" s="T621">n-n:case3</ta>
            <ta e="T623" id="Seg_10068" s="T622">ptcl</ta>
            <ta e="T624" id="Seg_10069" s="T623">v-v&gt;v-v:tense-mood-v:pn</ta>
            <ta e="T625" id="Seg_10070" s="T624">n-n:case3</ta>
            <ta e="T626" id="Seg_10071" s="T625">ptcl</ta>
            <ta e="T627" id="Seg_10072" s="T626">v-v:tense-v:pn</ta>
            <ta e="T628" id="Seg_10073" s="T627">n-n:case3</ta>
            <ta e="T629" id="Seg_10074" s="T628">nprop-n:case3</ta>
            <ta e="T630" id="Seg_10075" s="T629">n-n:case3</ta>
            <ta e="T631" id="Seg_10076" s="T630">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T632" id="Seg_10077" s="T631">n-n:case1-n:poss</ta>
            <ta e="T633" id="Seg_10078" s="T632">n-n:case3</ta>
            <ta e="T634" id="Seg_10079" s="T633">n-n:case3</ta>
            <ta e="T635" id="Seg_10080" s="T634">pp</ta>
            <ta e="T636" id="Seg_10081" s="T635">adv</ta>
            <ta e="T637" id="Seg_10082" s="T636">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T638" id="Seg_10083" s="T637">n-n:case1-n:poss</ta>
            <ta e="T639" id="Seg_10084" s="T638">n-n:case3</ta>
            <ta e="T640" id="Seg_10085" s="T639">adv</ta>
            <ta e="T641" id="Seg_10086" s="T640">adv</ta>
            <ta e="T642" id="Seg_10087" s="T641">v-v:pn</ta>
            <ta e="T643" id="Seg_10088" s="T642">v-v&gt;v-v:tense-mood-v:pn</ta>
            <ta e="T644" id="Seg_10089" s="T643">adv</ta>
            <ta e="T645" id="Seg_10090" s="T644">v-v:tense-v:pn</ta>
            <ta e="T646" id="Seg_10091" s="T645">dem</ta>
            <ta e="T647" id="Seg_10092" s="T646">nprop-n:case3</ta>
            <ta e="T648" id="Seg_10093" s="T647">n-n:case3</ta>
            <ta e="T649" id="Seg_10094" s="T648">n-n:case1-n:poss</ta>
            <ta e="T650" id="Seg_10095" s="T649">n-n:case3</ta>
            <ta e="T651" id="Seg_10096" s="T650">adv</ta>
            <ta e="T652" id="Seg_10097" s="T651">v-v:pn</ta>
            <ta e="T653" id="Seg_10098" s="T652">ptcl</ta>
            <ta e="T654" id="Seg_10099" s="T653">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T655" id="Seg_10100" s="T654">adv</ta>
            <ta e="T656" id="Seg_10101" s="T655">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T657" id="Seg_10102" s="T656">nprop-n:case3</ta>
            <ta e="T658" id="Seg_10103" s="T657">n-n:case3</ta>
            <ta e="T659" id="Seg_10104" s="T658">n-n:case1-n:poss</ta>
            <ta e="T660" id="Seg_10105" s="T659">n-n:case3</ta>
            <ta e="T661" id="Seg_10106" s="T660">n-n:(ins)-n&gt;adj</ta>
            <ta e="T662" id="Seg_10107" s="T661">n-n:case3</ta>
            <ta e="T663" id="Seg_10108" s="T662">v-v:tense-v:pn</ta>
            <ta e="T664" id="Seg_10109" s="T663">n-n:case3</ta>
            <ta e="T665" id="Seg_10110" s="T664">adv</ta>
            <ta e="T666" id="Seg_10111" s="T665">v-v&gt;v-v:pn</ta>
            <ta e="T667" id="Seg_10112" s="T666">dem-adj&gt;adv</ta>
            <ta e="T668" id="Seg_10113" s="T667">v-v:tense-v:pn</ta>
            <ta e="T669" id="Seg_10114" s="T668">n-n:(ins)-n&gt;adj</ta>
            <ta e="T670" id="Seg_10115" s="T669">n-n:case1-n:poss</ta>
            <ta e="T671" id="Seg_10116" s="T670">pp-n:case3</ta>
            <ta e="T672" id="Seg_10117" s="T671">dem-adj&gt;adv</ta>
            <ta e="T673" id="Seg_10118" s="T672">v-v:tense-v:pn</ta>
            <ta e="T674" id="Seg_10119" s="T673">conj</ta>
            <ta e="T675" id="Seg_10120" s="T674">conj</ta>
            <ta e="T676" id="Seg_10121" s="T675">v-v&gt;adv</ta>
            <ta e="T677" id="Seg_10122" s="T676">v-v:pn</ta>
            <ta e="T678" id="Seg_10123" s="T677">n-n:obl.poss-n:case2</ta>
            <ta e="T679" id="Seg_10124" s="T678">adv</ta>
            <ta e="T680" id="Seg_10125" s="T679">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T681" id="Seg_10126" s="T680">ptcl</ta>
            <ta e="T682" id="Seg_10127" s="T681">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T683" id="Seg_10128" s="T682">n-n&gt;v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T684" id="Seg_10129" s="T683">ptcl</ta>
            <ta e="T685" id="Seg_10130" s="T684">adv</ta>
            <ta e="T686" id="Seg_10131" s="T685">num-n:num-n&gt;adj</ta>
            <ta e="T687" id="Seg_10132" s="T686">adj</ta>
            <ta e="T688" id="Seg_10133" s="T687">n-n:case3</ta>
            <ta e="T689" id="Seg_10134" s="T688">v-v:inf</ta>
            <ta e="T690" id="Seg_10135" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_10136" s="T690">v-v:pn</ta>
            <ta e="T692" id="Seg_10137" s="T691">n-n&gt;adv</ta>
            <ta e="T693" id="Seg_10138" s="T692">ptcl</ta>
            <ta e="T694" id="Seg_10139" s="T693">n-n&gt;v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T695" id="Seg_10140" s="T694">n-n&gt;adv</ta>
            <ta e="T696" id="Seg_10141" s="T695">adv</ta>
            <ta e="T697" id="Seg_10142" s="T696">v-v:pn</ta>
            <ta e="T698" id="Seg_10143" s="T697">ptcl</ta>
            <ta e="T699" id="Seg_10144" s="T698">adj-adj&gt;v-v&gt;v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T700" id="Seg_10145" s="T699">n-n:(ins)-n:case3</ta>
            <ta e="T701" id="Seg_10146" s="T700">adv</ta>
            <ta e="T702" id="Seg_10147" s="T701">v-v&gt;v-v:pn</ta>
            <ta e="T703" id="Seg_10148" s="T702">v-v&gt;v-v&gt;v-v:mood-pn</ta>
            <ta e="T704" id="Seg_10149" s="T703">n-n:case3</ta>
            <ta e="T705" id="Seg_10150" s="T704">n-n:case3</ta>
            <ta e="T706" id="Seg_10151" s="T705">v-v:pn</ta>
            <ta e="T707" id="Seg_10152" s="T706">interj</ta>
            <ta e="T708" id="Seg_10153" s="T707">n-n:case3</ta>
            <ta e="T709" id="Seg_10154" s="T708">ptcl</ta>
            <ta e="T710" id="Seg_10155" s="T709">v-v&gt;v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T711" id="Seg_10156" s="T710">n-n:case3</ta>
            <ta e="T712" id="Seg_10157" s="T711">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T713" id="Seg_10158" s="T712">n-n:case3</ta>
            <ta e="T714" id="Seg_10159" s="T713">v-v&gt;adv</ta>
            <ta e="T715" id="Seg_10160" s="T714">dem-adj&gt;adv</ta>
            <ta e="T716" id="Seg_10161" s="T715">v-v:tense-v:pn</ta>
            <ta e="T717" id="Seg_10162" s="T716">n-n:case3</ta>
            <ta e="T718" id="Seg_10163" s="T717">adv-adv:advcase</ta>
            <ta e="T719" id="Seg_10164" s="T718">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T720" id="Seg_10165" s="T719">adv</ta>
            <ta e="T721" id="Seg_10166" s="T720">v-v&gt;v-v&gt;adv</ta>
            <ta e="T722" id="Seg_10167" s="T721">ptcl</ta>
            <ta e="T723" id="Seg_10168" s="T722">v-v:tense-mood-v:pn</ta>
            <ta e="T724" id="Seg_10169" s="T723">adv</ta>
            <ta e="T725" id="Seg_10170" s="T724">interrog-n:case3</ta>
            <ta e="T726" id="Seg_10171" s="T725">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T727" id="Seg_10172" s="T726">interj</ta>
            <ta e="T728" id="Seg_10173" s="T727">pers</ta>
            <ta e="T729" id="Seg_10174" s="T728">ptcl</ta>
            <ta e="T730" id="Seg_10175" s="T729">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T731" id="Seg_10176" s="T730">v-v:ins-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T732" id="Seg_10177" s="T731">v-v&gt;v-v:ins-v:tense-v:pn</ta>
            <ta e="T733" id="Seg_10178" s="T732">num</ta>
            <ta e="T734" id="Seg_10179" s="T733">n-n:case1-n:poss</ta>
            <ta e="T735" id="Seg_10180" s="T734">dem-adj&gt;adv</ta>
            <ta e="T736" id="Seg_10181" s="T735">v-v:tense-v:pn</ta>
            <ta e="T737" id="Seg_10182" s="T736">pers</ta>
            <ta e="T738" id="Seg_10183" s="T737">adv</ta>
            <ta e="T739" id="Seg_10184" s="T738">v-v:ins-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T740" id="Seg_10185" s="T739">dem-adj&gt;adv</ta>
            <ta e="T741" id="Seg_10186" s="T740">v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T742" id="Seg_10187" s="T741">adv</ta>
            <ta e="T743" id="Seg_10188" s="T742">adv</ta>
            <ta e="T744" id="Seg_10189" s="T743">v-v&gt;adv</ta>
            <ta e="T745" id="Seg_10190" s="T744">ptcl</ta>
            <ta e="T746" id="Seg_10191" s="T745">v-v:tense-mood-v:pn</ta>
            <ta e="T747" id="Seg_10192" s="T746">adv</ta>
            <ta e="T748" id="Seg_10193" s="T747">v-v&gt;adv</ta>
            <ta e="T749" id="Seg_10194" s="T748">ptcl</ta>
            <ta e="T750" id="Seg_10195" s="T749">v-v:tense-mood-v:pn</ta>
            <ta e="T751" id="Seg_10196" s="T750">interj</ta>
            <ta e="T752" id="Seg_10197" s="T751">interj</ta>
            <ta e="T753" id="Seg_10198" s="T752">ptcl</ta>
            <ta e="T754" id="Seg_10199" s="T753">nprop-n:case3</ta>
            <ta e="T755" id="Seg_10200" s="T754">n-n:case3</ta>
            <ta e="T756" id="Seg_10201" s="T755">qv-v&gt;v-v:pn</ta>
            <ta e="T757" id="Seg_10202" s="T756">v-v:tense-v:pn</ta>
            <ta e="T758" id="Seg_10203" s="T757">conj</ta>
            <ta e="T759" id="Seg_10204" s="T758">v-v:tense-v:pn</ta>
            <ta e="T760" id="Seg_10205" s="T759">conj</ta>
            <ta e="T761" id="Seg_10206" s="T760">qv-v:tense-v:pn</ta>
            <ta e="T762" id="Seg_10207" s="T761">nprop-n:case3</ta>
            <ta e="T763" id="Seg_10208" s="T762">n-n:case3</ta>
            <ta e="T764" id="Seg_10209" s="T763">adv</ta>
            <ta e="T765" id="Seg_10210" s="T764">adv</ta>
            <ta e="T766" id="Seg_10211" s="T765">v-v:tense-v:pn</ta>
            <ta e="T767" id="Seg_10212" s="T766">adv</ta>
            <ta e="T768" id="Seg_10213" s="T767">adv</ta>
            <ta e="T769" id="Seg_10214" s="T768">v-v:tense-v:pn</ta>
            <ta e="T770" id="Seg_10215" s="T769">pers</ta>
            <ta e="T771" id="Seg_10216" s="T770">adv</ta>
            <ta e="T772" id="Seg_10217" s="T771">v-v&gt;adv</ta>
            <ta e="T773" id="Seg_10218" s="T772">n-n&gt;adj</ta>
            <ta e="T774" id="Seg_10219" s="T773">v-v:mood-pn</ta>
            <ta e="T775" id="Seg_10220" s="T774">adv</ta>
            <ta e="T776" id="Seg_10221" s="T775">adv</ta>
            <ta e="T777" id="Seg_10222" s="T776">v-v&gt;adv</ta>
            <ta e="T778" id="Seg_10223" s="T777">v-v&gt;adv</ta>
            <ta e="T779" id="Seg_10224" s="T778">dem</ta>
            <ta e="T780" id="Seg_10225" s="T779">n-n:case3</ta>
            <ta e="T781" id="Seg_10226" s="T780">v-v:tense-v:pn</ta>
            <ta e="T782" id="Seg_10227" s="T781">v-v:pn</ta>
            <ta e="T783" id="Seg_10228" s="T782">ptcl</ta>
            <ta e="T784" id="Seg_10229" s="T783">nprop-n:case3</ta>
            <ta e="T785" id="Seg_10230" s="T784">n-n:case3</ta>
            <ta e="T786" id="Seg_10231" s="T785">n-n:case3</ta>
            <ta e="T787" id="Seg_10232" s="T786">v-v:tense-v:pn</ta>
            <ta e="T788" id="Seg_10233" s="T787">pers-n:case3</ta>
            <ta e="T789" id="Seg_10234" s="T788">ptcl</ta>
            <ta e="T790" id="Seg_10235" s="T789">adv</ta>
            <ta e="T791" id="Seg_10236" s="T790">adv</ta>
            <ta e="T792" id="Seg_10237" s="T791">v-v:tense-v:pn</ta>
            <ta e="T793" id="Seg_10238" s="T792">n-n:case3</ta>
            <ta e="T794" id="Seg_10239" s="T793">v-v&gt;adv</ta>
            <ta e="T795" id="Seg_10240" s="T794">n-n&gt;adj</ta>
            <ta e="T796" id="Seg_10241" s="T795">v-v:mood-pn</ta>
            <ta e="T797" id="Seg_10242" s="T796">adj</ta>
            <ta e="T798" id="Seg_10243" s="T797">adv</ta>
            <ta e="T799" id="Seg_10244" s="T798">dem-adj&gt;adv</ta>
            <ta e="T800" id="Seg_10245" s="T799">v-v:pn</ta>
            <ta e="T801" id="Seg_10246" s="T800">n-n:case3</ta>
            <ta e="T802" id="Seg_10247" s="T801">conj</ta>
            <ta e="T803" id="Seg_10248" s="T802">v-v:tense-v:pn</ta>
            <ta e="T804" id="Seg_10249" s="T803">conj</ta>
            <ta e="T805" id="Seg_10250" s="T804">ptcl</ta>
            <ta e="T806" id="Seg_10251" s="T805">v-v:tense-v:pn</ta>
            <ta e="T807" id="Seg_10252" s="T806">adj</ta>
            <ta e="T808" id="Seg_10253" s="T807">adv</ta>
            <ta e="T809" id="Seg_10254" s="T808">n-n:case3</ta>
            <ta e="T810" id="Seg_10255" s="T809">n-n:case3</ta>
            <ta e="T811" id="Seg_10256" s="T810">ptcl</ta>
            <ta e="T812" id="Seg_10257" s="T811">ptcl</ta>
            <ta e="T813" id="Seg_10258" s="T812">pers</ta>
            <ta e="T814" id="Seg_10259" s="T813">v-v&gt;v-v:pn</ta>
            <ta e="T815" id="Seg_10260" s="T814">ptcl</ta>
            <ta e="T816" id="Seg_10261" s="T815">v-v&gt;v-v:pn</ta>
            <ta e="T817" id="Seg_10262" s="T816">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T818" id="Seg_10263" s="T817">adj-adj&gt;adj</ta>
            <ta e="T819" id="Seg_10264" s="T818">conj</ta>
            <ta e="T820" id="Seg_10265" s="T819">v-v:tense-v:pn</ta>
            <ta e="T821" id="Seg_10266" s="T820">conj</ta>
            <ta e="T822" id="Seg_10267" s="T821">ptcl</ta>
            <ta e="T823" id="Seg_10268" s="T822">v-v:tense-v:pn</ta>
            <ta e="T824" id="Seg_10269" s="T823">num</ta>
            <ta e="T825" id="Seg_10270" s="T824">n-n:case3</ta>
            <ta e="T826" id="Seg_10271" s="T825">ptcl</ta>
            <ta e="T827" id="Seg_10272" s="T826">v-v:inf-v-v:tense-mood-v:pn</ta>
            <ta e="T828" id="Seg_10273" s="T827">n-n:case1-n:poss</ta>
            <ta e="T829" id="Seg_10274" s="T828">n-n:case3</ta>
            <ta e="T830" id="Seg_10275" s="T829">ptcl</ta>
            <ta e="T831" id="Seg_10276" s="T830">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T832" id="Seg_10277" s="T831">nprop-n:case3</ta>
            <ta e="T833" id="Seg_10278" s="T832">n-n:case3</ta>
            <ta e="T834" id="Seg_10279" s="T833">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T835" id="Seg_10280" s="T834">n-n:case1-n:poss</ta>
            <ta e="T836" id="Seg_10281" s="T835">n-n:case3</ta>
            <ta e="T837" id="Seg_10282" s="T836">n-n:case3</ta>
            <ta e="T838" id="Seg_10283" s="T837">adv</ta>
            <ta e="T839" id="Seg_10284" s="T838">v-v:pn</ta>
            <ta e="T840" id="Seg_10285" s="T839">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T841" id="Seg_10286" s="T840">n-n:case1-n:poss</ta>
            <ta e="T842" id="Seg_10287" s="T841">n-n:case3</ta>
            <ta e="T843" id="Seg_10288" s="T842">adv</ta>
            <ta e="T844" id="Seg_10289" s="T843">adv</ta>
            <ta e="T845" id="Seg_10290" s="T844">v-v:pn</ta>
            <ta e="T846" id="Seg_10291" s="T845">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T847" id="Seg_10292" s="T846">adv</ta>
            <ta e="T848" id="Seg_10293" s="T847">adv</ta>
            <ta e="T849" id="Seg_10294" s="T848">ptcl</ta>
            <ta e="T850" id="Seg_10295" s="T849">v-v:pn</ta>
            <ta e="T851" id="Seg_10296" s="T850">v-v:pn</ta>
            <ta e="T852" id="Seg_10297" s="T851">ptcl</ta>
            <ta e="T853" id="Seg_10298" s="T852">interrog-n:case3</ta>
            <ta e="T854" id="Seg_10299" s="T853">v-v:pn</ta>
            <ta e="T855" id="Seg_10300" s="T854">ptcl</ta>
            <ta e="T856" id="Seg_10301" s="T855">ptcl</ta>
            <ta e="T857" id="Seg_10302" s="T856">num</ta>
            <ta e="T858" id="Seg_10303" s="T857">v-v:pn</ta>
            <ta e="T859" id="Seg_10304" s="T858">adj</ta>
            <ta e="T860" id="Seg_10305" s="T859">n-adv:advcase</ta>
            <ta e="T861" id="Seg_10306" s="T860">preverb</ta>
            <ta e="T862" id="Seg_10307" s="T861">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T863" id="Seg_10308" s="T862">ptcl</ta>
            <ta e="T864" id="Seg_10309" s="T863">n-n:case3</ta>
            <ta e="T865" id="Seg_10310" s="T864">v-v&gt;v-v:mood-pn</ta>
            <ta e="T866" id="Seg_10311" s="T865">n-n:case3</ta>
            <ta e="T867" id="Seg_10312" s="T866">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T868" id="Seg_10313" s="T867">n-n:case3</ta>
            <ta e="T869" id="Seg_10314" s="T868">v-v:mood-pn</ta>
            <ta e="T870" id="Seg_10315" s="T869">adv</ta>
            <ta e="T871" id="Seg_10316" s="T870">n-n:case3</ta>
            <ta e="T872" id="Seg_10317" s="T871">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T873" id="Seg_10318" s="T872">num-n:num-n:case3</ta>
            <ta e="T874" id="Seg_10319" s="T873">adv</ta>
            <ta e="T875" id="Seg_10320" s="T874">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T876" id="Seg_10321" s="T875">n-n:case3</ta>
            <ta e="T877" id="Seg_10322" s="T876">v-v:pn</ta>
            <ta e="T878" id="Seg_10323" s="T877">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T879" id="Seg_10324" s="T878">n-n:case3</ta>
            <ta e="T880" id="Seg_10325" s="T879">v-v:pn</ta>
            <ta e="T881" id="Seg_10326" s="T880">n-n&gt;adj</ta>
            <ta e="T882" id="Seg_10327" s="T881">n-n&gt;n-n:case3</ta>
            <ta e="T883" id="Seg_10328" s="T882">dem</ta>
            <ta e="T884" id="Seg_10329" s="T883">n-n:case3</ta>
            <ta e="T885" id="Seg_10330" s="T884">n-n:case1-n:poss</ta>
            <ta e="T886" id="Seg_10331" s="T885">v-v:tense-v:pn</ta>
            <ta e="T887" id="Seg_10332" s="T886">dem</ta>
            <ta e="T888" id="Seg_10333" s="T887">n-n:case1-n:poss</ta>
            <ta e="T889" id="Seg_10334" s="T888">nprop-n:case3</ta>
            <ta e="T890" id="Seg_10335" s="T889">n-n:case3</ta>
            <ta e="T891" id="Seg_10336" s="T890">v-v:tense-v:pn</ta>
            <ta e="T892" id="Seg_10337" s="T891">dem</ta>
            <ta e="T893" id="Seg_10338" s="T892">n-n:case3</ta>
            <ta e="T894" id="Seg_10339" s="T893">n-n:case3</ta>
            <ta e="T895" id="Seg_10340" s="T894">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T896" id="Seg_10341" s="T895">n-n:(ins)-n:num-n:(ins)-n:case3</ta>
            <ta e="T897" id="Seg_10342" s="T896">v-v:tense-v:pn</ta>
            <ta e="T898" id="Seg_10343" s="T897">n-n:case1-n:poss</ta>
            <ta e="T899" id="Seg_10344" s="T898">ptcl</ta>
            <ta e="T900" id="Seg_10345" s="T899">v-v:tense-mood-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_10346" s="T0">nprop</ta>
            <ta e="T2" id="Seg_10347" s="T1">nprop</ta>
            <ta e="T3" id="Seg_10348" s="T2">n</ta>
            <ta e="T4" id="Seg_10349" s="T3">v</ta>
            <ta e="T5" id="Seg_10350" s="T4">adj</ta>
            <ta e="T6" id="Seg_10351" s="T5">v</ta>
            <ta e="T7" id="Seg_10352" s="T6">n</ta>
            <ta e="T8" id="Seg_10353" s="T7">v</ta>
            <ta e="T9" id="Seg_10354" s="T8">v</ta>
            <ta e="T10" id="Seg_10355" s="T9">v</ta>
            <ta e="T11" id="Seg_10356" s="T10">num</ta>
            <ta e="T12" id="Seg_10357" s="T11">adv</ta>
            <ta e="T13" id="Seg_10358" s="T12">n</ta>
            <ta e="T14" id="Seg_10359" s="T13">adv</ta>
            <ta e="T15" id="Seg_10360" s="T14">v</ta>
            <ta e="T16" id="Seg_10361" s="T15">v</ta>
            <ta e="T17" id="Seg_10362" s="T16">n</ta>
            <ta e="T18" id="Seg_10363" s="T17">adv</ta>
            <ta e="T19" id="Seg_10364" s="T18">adv</ta>
            <ta e="T20" id="Seg_10365" s="T19">v</ta>
            <ta e="T21" id="Seg_10366" s="T20">pers</ta>
            <ta e="T22" id="Seg_10367" s="T21">adj</ta>
            <ta e="T23" id="Seg_10368" s="T22">adv</ta>
            <ta e="T24" id="Seg_10369" s="T23">v</ta>
            <ta e="T25" id="Seg_10370" s="T24">adv</ta>
            <ta e="T26" id="Seg_10371" s="T25">conj</ta>
            <ta e="T27" id="Seg_10372" s="T26">v</ta>
            <ta e="T28" id="Seg_10373" s="T27">n</ta>
            <ta e="T29" id="Seg_10374" s="T28">pers</ta>
            <ta e="T30" id="Seg_10375" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_10376" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_10377" s="T31">dem</ta>
            <ta e="T33" id="Seg_10378" s="T32">n</ta>
            <ta e="T34" id="Seg_10379" s="T33">n</ta>
            <ta e="T35" id="Seg_10380" s="T34">n</ta>
            <ta e="T36" id="Seg_10381" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_10382" s="T36">v</ta>
            <ta e="T38" id="Seg_10383" s="T37">n</ta>
            <ta e="T39" id="Seg_10384" s="T38">v</ta>
            <ta e="T40" id="Seg_10385" s="T39">pers</ta>
            <ta e="T41" id="Seg_10386" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_10387" s="T41">adj</ta>
            <ta e="T43" id="Seg_10388" s="T42">n</ta>
            <ta e="T44" id="Seg_10389" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_10390" s="T44">v</ta>
            <ta e="T46" id="Seg_10391" s="T45">dem</ta>
            <ta e="T47" id="Seg_10392" s="T46">n</ta>
            <ta e="T48" id="Seg_10393" s="T47">n</ta>
            <ta e="T49" id="Seg_10394" s="T48">adv</ta>
            <ta e="T50" id="Seg_10395" s="T49">n</ta>
            <ta e="T51" id="Seg_10396" s="T50">v</ta>
            <ta e="T52" id="Seg_10397" s="T51">n</ta>
            <ta e="T53" id="Seg_10398" s="T52">n</ta>
            <ta e="T54" id="Seg_10399" s="T53">adj</ta>
            <ta e="T55" id="Seg_10400" s="T54">v</ta>
            <ta e="T56" id="Seg_10401" s="T55">pers</ta>
            <ta e="T57" id="Seg_10402" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_10403" s="T57">pro</ta>
            <ta e="T59" id="Seg_10404" s="T58">v</ta>
            <ta e="T60" id="Seg_10405" s="T59">pers</ta>
            <ta e="T61" id="Seg_10406" s="T60">adv</ta>
            <ta e="T62" id="Seg_10407" s="T61">adj</ta>
            <ta e="T63" id="Seg_10408" s="T62">adv</ta>
            <ta e="T64" id="Seg_10409" s="T63">v</ta>
            <ta e="T65" id="Seg_10410" s="T64">dem</ta>
            <ta e="T66" id="Seg_10411" s="T65">n</ta>
            <ta e="T67" id="Seg_10412" s="T66">n</ta>
            <ta e="T68" id="Seg_10413" s="T67">nprop</ta>
            <ta e="T69" id="Seg_10414" s="T68">n</ta>
            <ta e="T70" id="Seg_10415" s="T69">adv</ta>
            <ta e="T71" id="Seg_10416" s="T70">v</ta>
            <ta e="T72" id="Seg_10417" s="T71">nprop</ta>
            <ta e="T73" id="Seg_10418" s="T72">n</ta>
            <ta e="T74" id="Seg_10419" s="T73">quant</ta>
            <ta e="T75" id="Seg_10420" s="T74">v</ta>
            <ta e="T76" id="Seg_10421" s="T75">v</ta>
            <ta e="T77" id="Seg_10422" s="T76">conj</ta>
            <ta e="T78" id="Seg_10423" s="T77">v</ta>
            <ta e="T79" id="Seg_10424" s="T78">n</ta>
            <ta e="T80" id="Seg_10425" s="T79">n</ta>
            <ta e="T81" id="Seg_10426" s="T80">v</ta>
            <ta e="T82" id="Seg_10427" s="T81">adj</ta>
            <ta e="T83" id="Seg_10428" s="T82">n</ta>
            <ta e="T84" id="Seg_10429" s="T83">v</ta>
            <ta e="T85" id="Seg_10430" s="T84">adj</ta>
            <ta e="T86" id="Seg_10431" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_10432" s="T86">adj</ta>
            <ta e="T88" id="Seg_10433" s="T87">n</ta>
            <ta e="T89" id="Seg_10434" s="T88">v</ta>
            <ta e="T90" id="Seg_10435" s="T89">conj</ta>
            <ta e="T91" id="Seg_10436" s="T90">adj</ta>
            <ta e="T92" id="Seg_10437" s="T91">n</ta>
            <ta e="T93" id="Seg_10438" s="T92">adv</ta>
            <ta e="T94" id="Seg_10439" s="T93">v</ta>
            <ta e="T95" id="Seg_10440" s="T94">n</ta>
            <ta e="T96" id="Seg_10441" s="T95">n</ta>
            <ta e="T97" id="Seg_10442" s="T96">v</ta>
            <ta e="T98" id="Seg_10443" s="T97">conj</ta>
            <ta e="T99" id="Seg_10444" s="T98">n</ta>
            <ta e="T100" id="Seg_10445" s="T99">num</ta>
            <ta e="T101" id="Seg_10446" s="T100">n</ta>
            <ta e="T102" id="Seg_10447" s="T101">v</ta>
            <ta e="T103" id="Seg_10448" s="T102">n</ta>
            <ta e="T104" id="Seg_10449" s="T103">adj</ta>
            <ta e="T105" id="Seg_10450" s="T104">n</ta>
            <ta e="T106" id="Seg_10451" s="T105">dem</ta>
            <ta e="T107" id="Seg_10452" s="T106">n</ta>
            <ta e="T108" id="Seg_10453" s="T107">n</ta>
            <ta e="T109" id="Seg_10454" s="T108">v</ta>
            <ta e="T110" id="Seg_10455" s="T109">dem</ta>
            <ta e="T111" id="Seg_10456" s="T110">n</ta>
            <ta e="T112" id="Seg_10457" s="T111">n</ta>
            <ta e="T113" id="Seg_10458" s="T112">v</ta>
            <ta e="T114" id="Seg_10459" s="T113">num</ta>
            <ta e="T115" id="Seg_10460" s="T114">n</ta>
            <ta e="T116" id="Seg_10461" s="T115">clit</ta>
            <ta e="T117" id="Seg_10462" s="T116">interrog</ta>
            <ta e="T118" id="Seg_10463" s="T117">v</ta>
            <ta e="T119" id="Seg_10464" s="T118">interj</ta>
            <ta e="T120" id="Seg_10465" s="T119">interj</ta>
            <ta e="T121" id="Seg_10466" s="T120">interj</ta>
            <ta e="T122" id="Seg_10467" s="T121">interj</ta>
            <ta e="T123" id="Seg_10468" s="T122">v</ta>
            <ta e="T124" id="Seg_10469" s="T123">adj</ta>
            <ta e="T125" id="Seg_10470" s="T124">n</ta>
            <ta e="T126" id="Seg_10471" s="T125">v</ta>
            <ta e="T127" id="Seg_10472" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_10473" s="T127">interrog</ta>
            <ta e="T129" id="Seg_10474" s="T128">n</ta>
            <ta e="T130" id="Seg_10475" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_10476" s="T130">interrog</ta>
            <ta e="T132" id="Seg_10477" s="T131">v</ta>
            <ta e="T133" id="Seg_10478" s="T132">dem</ta>
            <ta e="T134" id="Seg_10479" s="T133">n</ta>
            <ta e="T135" id="Seg_10480" s="T134">n</ta>
            <ta e="T136" id="Seg_10481" s="T135">v</ta>
            <ta e="T137" id="Seg_10482" s="T136">expl</ta>
            <ta e="T138" id="Seg_10483" s="T137">interrog</ta>
            <ta e="T139" id="Seg_10484" s="T138">v</ta>
            <ta e="T140" id="Seg_10485" s="T139">pers</ta>
            <ta e="T141" id="Seg_10486" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_10487" s="T141">v</ta>
            <ta e="T143" id="Seg_10488" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_10489" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_10490" s="T144">v</ta>
            <ta e="T146" id="Seg_10491" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_10492" s="T146">v</ta>
            <ta e="T148" id="Seg_10493" s="T147">adv</ta>
            <ta e="T149" id="Seg_10494" s="T148">ptcl</ta>
            <ta e="T150" id="Seg_10495" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_10496" s="T150">n</ta>
            <ta e="T152" id="Seg_10497" s="T151">pp</ta>
            <ta e="T153" id="Seg_10498" s="T152">adv</ta>
            <ta e="T154" id="Seg_10499" s="T153">v</ta>
            <ta e="T155" id="Seg_10500" s="T154">adv</ta>
            <ta e="T156" id="Seg_10501" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_10502" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_10503" s="T157">v</ta>
            <ta e="T159" id="Seg_10504" s="T158">interj</ta>
            <ta e="T160" id="Seg_10505" s="T159">interj</ta>
            <ta e="T161" id="Seg_10506" s="T160">interj</ta>
            <ta e="T162" id="Seg_10507" s="T161">interj</ta>
            <ta e="T163" id="Seg_10508" s="T162">dem</ta>
            <ta e="T164" id="Seg_10509" s="T163">n</ta>
            <ta e="T165" id="Seg_10510" s="T164">adj</ta>
            <ta e="T166" id="Seg_10511" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_10512" s="T166">v</ta>
            <ta e="T168" id="Seg_10513" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_10514" s="T168">v</ta>
            <ta e="T170" id="Seg_10515" s="T169">adv</ta>
            <ta e="T171" id="Seg_10516" s="T170">n</ta>
            <ta e="T172" id="Seg_10517" s="T171">n</ta>
            <ta e="T173" id="Seg_10518" s="T172">clit</ta>
            <ta e="T174" id="Seg_10519" s="T173">interrog</ta>
            <ta e="T175" id="Seg_10520" s="T174">v</ta>
            <ta e="T176" id="Seg_10521" s="T175">pro</ta>
            <ta e="T177" id="Seg_10522" s="T176">adj</ta>
            <ta e="T178" id="Seg_10523" s="T177">v</ta>
            <ta e="T179" id="Seg_10524" s="T178">interj</ta>
            <ta e="T180" id="Seg_10525" s="T179">interj</ta>
            <ta e="T181" id="Seg_10526" s="T180">interj</ta>
            <ta e="T182" id="Seg_10527" s="T181">interj</ta>
            <ta e="T183" id="Seg_10528" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_10529" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_10530" s="T184">n</ta>
            <ta e="T186" id="Seg_10531" s="T185">n</ta>
            <ta e="T187" id="Seg_10532" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_10533" s="T187">adv</ta>
            <ta e="T189" id="Seg_10534" s="T188">v</ta>
            <ta e="T190" id="Seg_10535" s="T189">adv</ta>
            <ta e="T191" id="Seg_10536" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_10537" s="T191">v</ta>
            <ta e="T193" id="Seg_10538" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_10539" s="T193">adv</ta>
            <ta e="T195" id="Seg_10540" s="T194">n</ta>
            <ta e="T196" id="Seg_10541" s="T195">v</ta>
            <ta e="T197" id="Seg_10542" s="T196">adv</ta>
            <ta e="T198" id="Seg_10543" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_10544" s="T198">v</ta>
            <ta e="T200" id="Seg_10545" s="T199">adv</ta>
            <ta e="T201" id="Seg_10546" s="T200">n</ta>
            <ta e="T202" id="Seg_10547" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_10548" s="T202">n</ta>
            <ta e="T204" id="Seg_10549" s="T203">dem</ta>
            <ta e="T205" id="Seg_10550" s="T204">n</ta>
            <ta e="T206" id="Seg_10551" s="T205">n</ta>
            <ta e="T207" id="Seg_10552" s="T206">n</ta>
            <ta e="T208" id="Seg_10553" s="T207">adj</ta>
            <ta e="T209" id="Seg_10554" s="T208">n</ta>
            <ta e="T210" id="Seg_10555" s="T209">pers</ta>
            <ta e="T211" id="Seg_10556" s="T210">n</ta>
            <ta e="T212" id="Seg_10557" s="T211">n</ta>
            <ta e="T213" id="Seg_10558" s="T212">n</ta>
            <ta e="T214" id="Seg_10559" s="T213">adj</ta>
            <ta e="T215" id="Seg_10560" s="T214">v</ta>
            <ta e="T216" id="Seg_10561" s="T215">dem</ta>
            <ta e="T217" id="Seg_10562" s="T216">n</ta>
            <ta e="T218" id="Seg_10563" s="T217">n</ta>
            <ta e="T219" id="Seg_10564" s="T218">dem</ta>
            <ta e="T220" id="Seg_10565" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_10566" s="T220">dem</ta>
            <ta e="T222" id="Seg_10567" s="T221">n</ta>
            <ta e="T223" id="Seg_10568" s="T222">v</ta>
            <ta e="T224" id="Seg_10569" s="T223">pers</ta>
            <ta e="T225" id="Seg_10570" s="T224">interrog</ta>
            <ta e="T226" id="Seg_10571" s="T225">adj</ta>
            <ta e="T227" id="Seg_10572" s="T226">v</ta>
            <ta e="T228" id="Seg_10573" s="T227">ptcl</ta>
            <ta e="T229" id="Seg_10574" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_10575" s="T229">pers</ta>
            <ta e="T231" id="Seg_10576" s="T230">n</ta>
            <ta e="T232" id="Seg_10577" s="T231">pers</ta>
            <ta e="T233" id="Seg_10578" s="T232">v</ta>
            <ta e="T234" id="Seg_10579" s="T233">v</ta>
            <ta e="T235" id="Seg_10580" s="T234">adv</ta>
            <ta e="T236" id="Seg_10581" s="T235">n</ta>
            <ta e="T237" id="Seg_10582" s="T236">v</ta>
            <ta e="T238" id="Seg_10583" s="T237">pro</ta>
            <ta e="T239" id="Seg_10584" s="T238">pers</ta>
            <ta e="T240" id="Seg_10585" s="T239">v</ta>
            <ta e="T241" id="Seg_10586" s="T240">n</ta>
            <ta e="T242" id="Seg_10587" s="T241">pers</ta>
            <ta e="T243" id="Seg_10588" s="T242">v</ta>
            <ta e="T244" id="Seg_10589" s="T243">adv</ta>
            <ta e="T245" id="Seg_10590" s="T244">pers</ta>
            <ta e="T246" id="Seg_10591" s="T245">adv</ta>
            <ta e="T247" id="Seg_10592" s="T246">v</ta>
            <ta e="T248" id="Seg_10593" s="T247">n</ta>
            <ta e="T249" id="Seg_10594" s="T248">quant</ta>
            <ta e="T250" id="Seg_10595" s="T249">pers</ta>
            <ta e="T251" id="Seg_10596" s="T250">v</ta>
            <ta e="T252" id="Seg_10597" s="T251">ptcl</ta>
            <ta e="T253" id="Seg_10598" s="T252">interrog</ta>
            <ta e="T254" id="Seg_10599" s="T253">adj</ta>
            <ta e="T255" id="Seg_10600" s="T254">v</ta>
            <ta e="T256" id="Seg_10601" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_10602" s="T256">interrog</ta>
            <ta e="T258" id="Seg_10603" s="T257">pers</ta>
            <ta e="T259" id="Seg_10604" s="T258">v</ta>
            <ta e="T260" id="Seg_10605" s="T259">pers</ta>
            <ta e="T261" id="Seg_10606" s="T260">adv</ta>
            <ta e="T262" id="Seg_10607" s="T261">v</ta>
            <ta e="T263" id="Seg_10608" s="T262">pers</ta>
            <ta e="T264" id="Seg_10609" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_10610" s="T264">adv</ta>
            <ta e="T266" id="Seg_10611" s="T265">v</ta>
            <ta e="T267" id="Seg_10612" s="T266">pers</ta>
            <ta e="T268" id="Seg_10613" s="T267">interrog</ta>
            <ta e="T269" id="Seg_10614" s="T268">v</ta>
            <ta e="T270" id="Seg_10615" s="T269">n</ta>
            <ta e="T271" id="Seg_10616" s="T270">pers</ta>
            <ta e="T272" id="Seg_10617" s="T271">v</ta>
            <ta e="T273" id="Seg_10618" s="T272">pers</ta>
            <ta e="T274" id="Seg_10619" s="T273">v</ta>
            <ta e="T275" id="Seg_10620" s="T274">pers</ta>
            <ta e="T276" id="Seg_10621" s="T275">adv</ta>
            <ta e="T277" id="Seg_10622" s="T276">v</ta>
            <ta e="T278" id="Seg_10623" s="T277">adv</ta>
            <ta e="T279" id="Seg_10624" s="T278">ptcl</ta>
            <ta e="T280" id="Seg_10625" s="T279">v</ta>
            <ta e="T281" id="Seg_10626" s="T280">nprop</ta>
            <ta e="T282" id="Seg_10627" s="T281">n</ta>
            <ta e="T283" id="Seg_10628" s="T282">n</ta>
            <ta e="T284" id="Seg_10629" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_10630" s="T284">v</ta>
            <ta e="T286" id="Seg_10631" s="T285">n</ta>
            <ta e="T287" id="Seg_10632" s="T286">pers</ta>
            <ta e="T288" id="Seg_10633" s="T287">ptcl</ta>
            <ta e="T289" id="Seg_10634" s="T288">pers</ta>
            <ta e="T290" id="Seg_10635" s="T289">interrog</ta>
            <ta e="T291" id="Seg_10636" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_10637" s="T291">pers</ta>
            <ta e="T293" id="Seg_10638" s="T292">v</ta>
            <ta e="T294" id="Seg_10639" s="T293">n</ta>
            <ta e="T295" id="Seg_10640" s="T294">pers</ta>
            <ta e="T296" id="Seg_10641" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_10642" s="T296">v</ta>
            <ta e="T298" id="Seg_10643" s="T297">nprop</ta>
            <ta e="T299" id="Seg_10644" s="T298">n</ta>
            <ta e="T300" id="Seg_10645" s="T299">interrog</ta>
            <ta e="T301" id="Seg_10646" s="T300">qv</ta>
            <ta e="T302" id="Seg_10647" s="T301">pers</ta>
            <ta e="T303" id="Seg_10648" s="T302">interrog</ta>
            <ta e="T304" id="Seg_10649" s="T303">pers</ta>
            <ta e="T305" id="Seg_10650" s="T304">v</ta>
            <ta e="T306" id="Seg_10651" s="T305">adj</ta>
            <ta e="T307" id="Seg_10652" s="T306">v</ta>
            <ta e="T308" id="Seg_10653" s="T307">n</ta>
            <ta e="T309" id="Seg_10654" s="T308">adv</ta>
            <ta e="T310" id="Seg_10655" s="T309">ptcl</ta>
            <ta e="T311" id="Seg_10656" s="T310">v</ta>
            <ta e="T312" id="Seg_10657" s="T311">adv</ta>
            <ta e="T313" id="Seg_10658" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_10659" s="T313">adj</ta>
            <ta e="T315" id="Seg_10660" s="T314">n</ta>
            <ta e="T316" id="Seg_10661" s="T315">pro</ta>
            <ta e="T317" id="Seg_10662" s="T316">n</ta>
            <ta e="T318" id="Seg_10663" s="T317">n</ta>
            <ta e="T319" id="Seg_10664" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_10665" s="T319">n</ta>
            <ta e="T321" id="Seg_10666" s="T320">ptcl</ta>
            <ta e="T322" id="Seg_10667" s="T321">v</ta>
            <ta e="T323" id="Seg_10668" s="T322">n</ta>
            <ta e="T324" id="Seg_10669" s="T323">v</ta>
            <ta e="T325" id="Seg_10670" s="T324">interrog</ta>
            <ta e="T326" id="Seg_10671" s="T325">v</ta>
            <ta e="T327" id="Seg_10672" s="T326">v</ta>
            <ta e="T328" id="Seg_10673" s="T327">pers</ta>
            <ta e="T329" id="Seg_10674" s="T328">conj</ta>
            <ta e="T330" id="Seg_10675" s="T329">v</ta>
            <ta e="T331" id="Seg_10676" s="T330">n</ta>
            <ta e="T332" id="Seg_10677" s="T331">pers</ta>
            <ta e="T333" id="Seg_10678" s="T332">v</ta>
            <ta e="T334" id="Seg_10679" s="T333">pers</ta>
            <ta e="T335" id="Seg_10680" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_10681" s="T335">n</ta>
            <ta e="T337" id="Seg_10682" s="T336">v</ta>
            <ta e="T338" id="Seg_10683" s="T337">dem</ta>
            <ta e="T339" id="Seg_10684" s="T338">n</ta>
            <ta e="T340" id="Seg_10685" s="T339">n</ta>
            <ta e="T341" id="Seg_10686" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_10687" s="T341">n</ta>
            <ta e="T343" id="Seg_10688" s="T342">v</ta>
            <ta e="T344" id="Seg_10689" s="T343">pers</ta>
            <ta e="T345" id="Seg_10690" s="T344">ptcl</ta>
            <ta e="T346" id="Seg_10691" s="T345">pers</ta>
            <ta e="T347" id="Seg_10692" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_10693" s="T347">pers</ta>
            <ta e="T349" id="Seg_10694" s="T348">v</ta>
            <ta e="T350" id="Seg_10695" s="T349">n</ta>
            <ta e="T351" id="Seg_10696" s="T350">v</ta>
            <ta e="T352" id="Seg_10697" s="T351">n</ta>
            <ta e="T353" id="Seg_10698" s="T352">ptcl</ta>
            <ta e="T354" id="Seg_10699" s="T353">v</ta>
            <ta e="T355" id="Seg_10700" s="T354">dem</ta>
            <ta e="T356" id="Seg_10701" s="T355">n</ta>
            <ta e="T357" id="Seg_10702" s="T356">adv</ta>
            <ta e="T358" id="Seg_10703" s="T357">quant</ta>
            <ta e="T359" id="Seg_10704" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_10705" s="T359">v</ta>
            <ta e="T361" id="Seg_10706" s="T360">clit</ta>
            <ta e="T362" id="Seg_10707" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_10708" s="T362">interrog</ta>
            <ta e="T364" id="Seg_10709" s="T363">v</ta>
            <ta e="T365" id="Seg_10710" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_10711" s="T365">ptcl</ta>
            <ta e="T367" id="Seg_10712" s="T366">n</ta>
            <ta e="T368" id="Seg_10713" s="T367">n</ta>
            <ta e="T369" id="Seg_10714" s="T368">pers</ta>
            <ta e="T370" id="Seg_10715" s="T369">v</ta>
            <ta e="T371" id="Seg_10716" s="T370">clit</ta>
            <ta e="T372" id="Seg_10717" s="T371">adj</ta>
            <ta e="T373" id="Seg_10718" s="T372">n</ta>
            <ta e="T374" id="Seg_10719" s="T373">v</ta>
            <ta e="T375" id="Seg_10720" s="T374">pers</ta>
            <ta e="T376" id="Seg_10721" s="T375">n</ta>
            <ta e="T377" id="Seg_10722" s="T376">pers</ta>
            <ta e="T378" id="Seg_10723" s="T377">v</ta>
            <ta e="T379" id="Seg_10724" s="T378">adv</ta>
            <ta e="T380" id="Seg_10725" s="T379">pers</ta>
            <ta e="T381" id="Seg_10726" s="T380">v</ta>
            <ta e="T382" id="Seg_10727" s="T381">pers</ta>
            <ta e="T383" id="Seg_10728" s="T382">ptcl</ta>
            <ta e="T384" id="Seg_10729" s="T383">v</ta>
            <ta e="T385" id="Seg_10730" s="T384">n</ta>
            <ta e="T386" id="Seg_10731" s="T385">adj</ta>
            <ta e="T387" id="Seg_10732" s="T386">n</ta>
            <ta e="T388" id="Seg_10733" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_10734" s="T388">num</ta>
            <ta e="T390" id="Seg_10735" s="T389">adj</ta>
            <ta e="T391" id="Seg_10736" s="T390">n</ta>
            <ta e="T392" id="Seg_10737" s="T391">v</ta>
            <ta e="T393" id="Seg_10738" s="T392">adv</ta>
            <ta e="T394" id="Seg_10739" s="T393">interrog</ta>
            <ta e="T395" id="Seg_10740" s="T394">adv</ta>
            <ta e="T396" id="Seg_10741" s="T395">v</ta>
            <ta e="T397" id="Seg_10742" s="T396">n</ta>
            <ta e="T398" id="Seg_10743" s="T397">preverb</ta>
            <ta e="T399" id="Seg_10744" s="T398">v</ta>
            <ta e="T400" id="Seg_10745" s="T399">adv</ta>
            <ta e="T401" id="Seg_10746" s="T400">adv</ta>
            <ta e="T402" id="Seg_10747" s="T401">adv</ta>
            <ta e="T403" id="Seg_10748" s="T402">v</ta>
            <ta e="T404" id="Seg_10749" s="T403">n</ta>
            <ta e="T405" id="Seg_10750" s="T404">interrog</ta>
            <ta e="T406" id="Seg_10751" s="T405">n</ta>
            <ta e="T407" id="Seg_10752" s="T406">v</ta>
            <ta e="T408" id="Seg_10753" s="T407">n</ta>
            <ta e="T409" id="Seg_10754" s="T408">conj</ta>
            <ta e="T410" id="Seg_10755" s="T409">v</ta>
            <ta e="T411" id="Seg_10756" s="T410">n</ta>
            <ta e="T412" id="Seg_10757" s="T411">v</ta>
            <ta e="T413" id="Seg_10758" s="T412">num</ta>
            <ta e="T414" id="Seg_10759" s="T413">n</ta>
            <ta e="T415" id="Seg_10760" s="T414">v</ta>
            <ta e="T416" id="Seg_10761" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_10762" s="T416">adj</ta>
            <ta e="T418" id="Seg_10763" s="T417">n</ta>
            <ta e="T419" id="Seg_10764" s="T418">v</ta>
            <ta e="T420" id="Seg_10765" s="T419">adv</ta>
            <ta e="T421" id="Seg_10766" s="T420">v</ta>
            <ta e="T422" id="Seg_10767" s="T421">v</ta>
            <ta e="T423" id="Seg_10768" s="T422">adv</ta>
            <ta e="T424" id="Seg_10769" s="T423">v</ta>
            <ta e="T425" id="Seg_10770" s="T424">num</ta>
            <ta e="T426" id="Seg_10771" s="T425">adj</ta>
            <ta e="T427" id="Seg_10772" s="T426">n</ta>
            <ta e="T428" id="Seg_10773" s="T427">n</ta>
            <ta e="T429" id="Seg_10774" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_10775" s="T429">v</ta>
            <ta e="T431" id="Seg_10776" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_10777" s="T431">adv</ta>
            <ta e="T433" id="Seg_10778" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_10779" s="T433">v</ta>
            <ta e="T435" id="Seg_10780" s="T434">n</ta>
            <ta e="T436" id="Seg_10781" s="T435">v</ta>
            <ta e="T437" id="Seg_10782" s="T436">n</ta>
            <ta e="T438" id="Seg_10783" s="T437">ptcl</ta>
            <ta e="T439" id="Seg_10784" s="T438">v</ta>
            <ta e="T440" id="Seg_10785" s="T439">conj</ta>
            <ta e="T441" id="Seg_10786" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_10787" s="T441">adv</ta>
            <ta e="T443" id="Seg_10788" s="T442">adv</ta>
            <ta e="T444" id="Seg_10789" s="T443">v</ta>
            <ta e="T445" id="Seg_10790" s="T444">ptcl</ta>
            <ta e="T446" id="Seg_10791" s="T445">interrog</ta>
            <ta e="T447" id="Seg_10792" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_10793" s="T447">v</ta>
            <ta e="T449" id="Seg_10794" s="T448">nprop</ta>
            <ta e="T450" id="Seg_10795" s="T449">n</ta>
            <ta e="T451" id="Seg_10796" s="T450">v</ta>
            <ta e="T452" id="Seg_10797" s="T451">interrog</ta>
            <ta e="T453" id="Seg_10798" s="T452">adv</ta>
            <ta e="T454" id="Seg_10799" s="T453">ptcl</ta>
            <ta e="T455" id="Seg_10800" s="T454">v</ta>
            <ta e="T456" id="Seg_10801" s="T455">v</ta>
            <ta e="T457" id="Seg_10802" s="T456">n</ta>
            <ta e="T458" id="Seg_10803" s="T457">v</ta>
            <ta e="T459" id="Seg_10804" s="T458">n</ta>
            <ta e="T460" id="Seg_10805" s="T459">interj</ta>
            <ta e="T461" id="Seg_10806" s="T460">interj</ta>
            <ta e="T462" id="Seg_10807" s="T461">ptcl</ta>
            <ta e="T463" id="Seg_10808" s="T462">nprop</ta>
            <ta e="T464" id="Seg_10809" s="T463">n</ta>
            <ta e="T465" id="Seg_10810" s="T464">qv</ta>
            <ta e="T466" id="Seg_10811" s="T465">nprop</ta>
            <ta e="T467" id="Seg_10812" s="T466">n</ta>
            <ta e="T468" id="Seg_10813" s="T467">adv</ta>
            <ta e="T469" id="Seg_10814" s="T468">v</ta>
            <ta e="T470" id="Seg_10815" s="T469">adv</ta>
            <ta e="T471" id="Seg_10816" s="T470">ptcl</ta>
            <ta e="T472" id="Seg_10817" s="T471">v</ta>
            <ta e="T473" id="Seg_10818" s="T472">n</ta>
            <ta e="T474" id="Seg_10819" s="T473">v</ta>
            <ta e="T475" id="Seg_10820" s="T474">n</ta>
            <ta e="T476" id="Seg_10821" s="T475">v</ta>
            <ta e="T477" id="Seg_10822" s="T476">nprop</ta>
            <ta e="T478" id="Seg_10823" s="T477">n</ta>
            <ta e="T479" id="Seg_10824" s="T478">n</ta>
            <ta e="T480" id="Seg_10825" s="T479">v</ta>
            <ta e="T481" id="Seg_10826" s="T480">n</ta>
            <ta e="T482" id="Seg_10827" s="T481">adv</ta>
            <ta e="T483" id="Seg_10828" s="T482">adv</ta>
            <ta e="T484" id="Seg_10829" s="T483">v</ta>
            <ta e="T485" id="Seg_10830" s="T484">v</ta>
            <ta e="T486" id="Seg_10831" s="T485">n</ta>
            <ta e="T487" id="Seg_10832" s="T486">v</ta>
            <ta e="T488" id="Seg_10833" s="T487">ptcl</ta>
            <ta e="T489" id="Seg_10834" s="T488">interj</ta>
            <ta e="T490" id="Seg_10835" s="T489">interj</ta>
            <ta e="T491" id="Seg_10836" s="T490">adv</ta>
            <ta e="T492" id="Seg_10837" s="T491">adv</ta>
            <ta e="T493" id="Seg_10838" s="T492">dem</ta>
            <ta e="T494" id="Seg_10839" s="T493">nprop</ta>
            <ta e="T495" id="Seg_10840" s="T494">n</ta>
            <ta e="T496" id="Seg_10841" s="T495">n</ta>
            <ta e="T497" id="Seg_10842" s="T496">adv</ta>
            <ta e="T498" id="Seg_10843" s="T497">v</ta>
            <ta e="T499" id="Seg_10844" s="T498">preverb</ta>
            <ta e="T500" id="Seg_10845" s="T499">v</ta>
            <ta e="T501" id="Seg_10846" s="T500">adj</ta>
            <ta e="T502" id="Seg_10847" s="T501">n</ta>
            <ta e="T503" id="Seg_10848" s="T502">v</ta>
            <ta e="T504" id="Seg_10849" s="T503">adv</ta>
            <ta e="T505" id="Seg_10850" s="T504">v</ta>
            <ta e="T506" id="Seg_10851" s="T505">pers</ta>
            <ta e="T507" id="Seg_10852" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_10853" s="T507">pers</ta>
            <ta e="T509" id="Seg_10854" s="T508">ptcl</ta>
            <ta e="T510" id="Seg_10855" s="T509">interrog</ta>
            <ta e="T511" id="Seg_10856" s="T510">interrog</ta>
            <ta e="T512" id="Seg_10857" s="T511">ptcl</ta>
            <ta e="T513" id="Seg_10858" s="T512">v</ta>
            <ta e="T514" id="Seg_10859" s="T513">adv</ta>
            <ta e="T515" id="Seg_10860" s="T514">v</ta>
            <ta e="T516" id="Seg_10861" s="T515">adj</ta>
            <ta e="T517" id="Seg_10862" s="T516">adv</ta>
            <ta e="T518" id="Seg_10863" s="T517">adv</ta>
            <ta e="T519" id="Seg_10864" s="T518">n</ta>
            <ta e="T520" id="Seg_10865" s="T519">pp</ta>
            <ta e="T521" id="Seg_10866" s="T520">ptcl</ta>
            <ta e="T522" id="Seg_10867" s="T521">v</ta>
            <ta e="T523" id="Seg_10868" s="T522">adj</ta>
            <ta e="T524" id="Seg_10869" s="T523">adv</ta>
            <ta e="T525" id="Seg_10870" s="T524">n</ta>
            <ta e="T526" id="Seg_10871" s="T525">v</ta>
            <ta e="T527" id="Seg_10872" s="T526">adv</ta>
            <ta e="T528" id="Seg_10873" s="T527">v</ta>
            <ta e="T529" id="Seg_10874" s="T528">conj</ta>
            <ta e="T530" id="Seg_10875" s="T529">ptcl</ta>
            <ta e="T531" id="Seg_10876" s="T530">v</ta>
            <ta e="T532" id="Seg_10877" s="T531">nprop</ta>
            <ta e="T533" id="Seg_10878" s="T532">n</ta>
            <ta e="T534" id="Seg_10879" s="T533">v</ta>
            <ta e="T535" id="Seg_10880" s="T534">v</ta>
            <ta e="T536" id="Seg_10881" s="T535">adv</ta>
            <ta e="T537" id="Seg_10882" s="T536">n</ta>
            <ta e="T538" id="Seg_10883" s="T537">ptcl</ta>
            <ta e="T539" id="Seg_10884" s="T538">adv</ta>
            <ta e="T540" id="Seg_10885" s="T539">v</ta>
            <ta e="T541" id="Seg_10886" s="T540">n</ta>
            <ta e="T542" id="Seg_10887" s="T541">dem</ta>
            <ta e="T543" id="Seg_10888" s="T542">v</ta>
            <ta e="T544" id="Seg_10889" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_10890" s="T544">num</ta>
            <ta e="T546" id="Seg_10891" s="T545">n</ta>
            <ta e="T547" id="Seg_10892" s="T546">v</ta>
            <ta e="T548" id="Seg_10893" s="T547">n</ta>
            <ta e="T549" id="Seg_10894" s="T548">ptcl</ta>
            <ta e="T550" id="Seg_10895" s="T549">v</ta>
            <ta e="T551" id="Seg_10896" s="T550">ptcl</ta>
            <ta e="T552" id="Seg_10897" s="T551">nprop</ta>
            <ta e="T553" id="Seg_10898" s="T552">pro</ta>
            <ta e="T554" id="Seg_10899" s="T553">v</ta>
            <ta e="T555" id="Seg_10900" s="T554">ptcl</ta>
            <ta e="T556" id="Seg_10901" s="T555">v</ta>
            <ta e="T557" id="Seg_10902" s="T556">adv</ta>
            <ta e="T558" id="Seg_10903" s="T557">adv</ta>
            <ta e="T559" id="Seg_10904" s="T558">pers</ta>
            <ta e="T560" id="Seg_10905" s="T559">v</ta>
            <ta e="T561" id="Seg_10906" s="T560">adv</ta>
            <ta e="T562" id="Seg_10907" s="T561">pers</ta>
            <ta e="T563" id="Seg_10908" s="T562">v</ta>
            <ta e="T564" id="Seg_10909" s="T563">dem</ta>
            <ta e="T565" id="Seg_10910" s="T564">n</ta>
            <ta e="T566" id="Seg_10911" s="T565">n</ta>
            <ta e="T567" id="Seg_10912" s="T566">ptcl</ta>
            <ta e="T568" id="Seg_10913" s="T567">v</ta>
            <ta e="T569" id="Seg_10914" s="T568">pro</ta>
            <ta e="T570" id="Seg_10915" s="T569">n</ta>
            <ta e="T571" id="Seg_10916" s="T570">adv</ta>
            <ta e="T572" id="Seg_10917" s="T571">ptcl</ta>
            <ta e="T573" id="Seg_10918" s="T572">v</ta>
            <ta e="T574" id="Seg_10919" s="T573">ptcl</ta>
            <ta e="T575" id="Seg_10920" s="T574">interj</ta>
            <ta e="T576" id="Seg_10921" s="T575">interj</ta>
            <ta e="T577" id="Seg_10922" s="T576">interj</ta>
            <ta e="T578" id="Seg_10923" s="T577">ptcl</ta>
            <ta e="T579" id="Seg_10924" s="T578">nprop</ta>
            <ta e="T580" id="Seg_10925" s="T579">n</ta>
            <ta e="T581" id="Seg_10926" s="T580">v</ta>
            <ta e="T582" id="Seg_10927" s="T581">ptcl</ta>
            <ta e="T583" id="Seg_10928" s="T582">n</ta>
            <ta e="T584" id="Seg_10929" s="T583">n</ta>
            <ta e="T585" id="Seg_10930" s="T584">nprop</ta>
            <ta e="T586" id="Seg_10931" s="T585">n</ta>
            <ta e="T587" id="Seg_10932" s="T586">adv</ta>
            <ta e="T588" id="Seg_10933" s="T587">v</ta>
            <ta e="T589" id="Seg_10934" s="T588">pers</ta>
            <ta e="T590" id="Seg_10935" s="T589">adv</ta>
            <ta e="T591" id="Seg_10936" s="T590">adv</ta>
            <ta e="T592" id="Seg_10937" s="T591">adj</ta>
            <ta e="T593" id="Seg_10938" s="T592">v</ta>
            <ta e="T594" id="Seg_10939" s="T593">pro</ta>
            <ta e="T595" id="Seg_10940" s="T594">adv</ta>
            <ta e="T596" id="Seg_10941" s="T595">ptcl</ta>
            <ta e="T597" id="Seg_10942" s="T596">v</ta>
            <ta e="T598" id="Seg_10943" s="T597">n</ta>
            <ta e="T599" id="Seg_10944" s="T598">adj</ta>
            <ta e="T600" id="Seg_10945" s="T599">v</ta>
            <ta e="T601" id="Seg_10946" s="T600">n</ta>
            <ta e="T602" id="Seg_10947" s="T601">v</ta>
            <ta e="T603" id="Seg_10948" s="T602">qv</ta>
            <ta e="T604" id="Seg_10949" s="T603">nprop</ta>
            <ta e="T605" id="Seg_10950" s="T604">n</ta>
            <ta e="T606" id="Seg_10951" s="T605">pers</ta>
            <ta e="T607" id="Seg_10952" s="T606">ptcl</ta>
            <ta e="T608" id="Seg_10953" s="T607">adv</ta>
            <ta e="T609" id="Seg_10954" s="T608">adv</ta>
            <ta e="T610" id="Seg_10955" s="T609">v</ta>
            <ta e="T611" id="Seg_10956" s="T610">v</ta>
            <ta e="T612" id="Seg_10957" s="T611">n</ta>
            <ta e="T613" id="Seg_10958" s="T612">adv</ta>
            <ta e="T614" id="Seg_10959" s="T613">adj</ta>
            <ta e="T615" id="Seg_10960" s="T614">v</ta>
            <ta e="T616" id="Seg_10961" s="T615">num</ta>
            <ta e="T617" id="Seg_10962" s="T616">n</ta>
            <ta e="T618" id="Seg_10963" s="T617">n</ta>
            <ta e="T619" id="Seg_10964" s="T618">n</ta>
            <ta e="T620" id="Seg_10965" s="T619">ptcl</ta>
            <ta e="T621" id="Seg_10966" s="T620">v</ta>
            <ta e="T622" id="Seg_10967" s="T621">n</ta>
            <ta e="T623" id="Seg_10968" s="T622">ptcl</ta>
            <ta e="T624" id="Seg_10969" s="T623">v</ta>
            <ta e="T625" id="Seg_10970" s="T624">n</ta>
            <ta e="T626" id="Seg_10971" s="T625">ptcl</ta>
            <ta e="T627" id="Seg_10972" s="T626">v</ta>
            <ta e="T628" id="Seg_10973" s="T627">n</ta>
            <ta e="T629" id="Seg_10974" s="T628">nprop</ta>
            <ta e="T630" id="Seg_10975" s="T629">n</ta>
            <ta e="T631" id="Seg_10976" s="T630">v</ta>
            <ta e="T632" id="Seg_10977" s="T631">n</ta>
            <ta e="T633" id="Seg_10978" s="T632">n</ta>
            <ta e="T634" id="Seg_10979" s="T633">n</ta>
            <ta e="T635" id="Seg_10980" s="T634">pp</ta>
            <ta e="T636" id="Seg_10981" s="T635">adv</ta>
            <ta e="T637" id="Seg_10982" s="T636">v</ta>
            <ta e="T638" id="Seg_10983" s="T637">n</ta>
            <ta e="T639" id="Seg_10984" s="T638">n</ta>
            <ta e="T640" id="Seg_10985" s="T639">adv</ta>
            <ta e="T641" id="Seg_10986" s="T640">adv</ta>
            <ta e="T642" id="Seg_10987" s="T641">v</ta>
            <ta e="T643" id="Seg_10988" s="T642">v</ta>
            <ta e="T644" id="Seg_10989" s="T643">adv</ta>
            <ta e="T645" id="Seg_10990" s="T644">v</ta>
            <ta e="T646" id="Seg_10991" s="T645">dem</ta>
            <ta e="T647" id="Seg_10992" s="T646">nprop</ta>
            <ta e="T648" id="Seg_10993" s="T647">n</ta>
            <ta e="T649" id="Seg_10994" s="T648">n</ta>
            <ta e="T650" id="Seg_10995" s="T649">n</ta>
            <ta e="T651" id="Seg_10996" s="T650">adv</ta>
            <ta e="T652" id="Seg_10997" s="T651">v</ta>
            <ta e="T653" id="Seg_10998" s="T652">ptcl</ta>
            <ta e="T654" id="Seg_10999" s="T653">v</ta>
            <ta e="T655" id="Seg_11000" s="T654">adv</ta>
            <ta e="T656" id="Seg_11001" s="T655">v</ta>
            <ta e="T657" id="Seg_11002" s="T656">nprop</ta>
            <ta e="T658" id="Seg_11003" s="T657">n</ta>
            <ta e="T659" id="Seg_11004" s="T658">n</ta>
            <ta e="T660" id="Seg_11005" s="T659">n</ta>
            <ta e="T661" id="Seg_11006" s="T660">adj</ta>
            <ta e="T662" id="Seg_11007" s="T661">n</ta>
            <ta e="T663" id="Seg_11008" s="T662">v</ta>
            <ta e="T664" id="Seg_11009" s="T663">n</ta>
            <ta e="T665" id="Seg_11010" s="T664">adv</ta>
            <ta e="T666" id="Seg_11011" s="T665">v</ta>
            <ta e="T667" id="Seg_11012" s="T666">adv</ta>
            <ta e="T668" id="Seg_11013" s="T667">v</ta>
            <ta e="T669" id="Seg_11014" s="T668">adj</ta>
            <ta e="T670" id="Seg_11015" s="T669">n</ta>
            <ta e="T671" id="Seg_11016" s="T670">pp</ta>
            <ta e="T672" id="Seg_11017" s="T671">adv</ta>
            <ta e="T673" id="Seg_11018" s="T672">v</ta>
            <ta e="T674" id="Seg_11019" s="T673">conj</ta>
            <ta e="T675" id="Seg_11020" s="T674">conj</ta>
            <ta e="T676" id="Seg_11021" s="T675">adv</ta>
            <ta e="T677" id="Seg_11022" s="T676">v</ta>
            <ta e="T678" id="Seg_11023" s="T677">n</ta>
            <ta e="T679" id="Seg_11024" s="T678">adv</ta>
            <ta e="T680" id="Seg_11025" s="T679">v</ta>
            <ta e="T681" id="Seg_11026" s="T680">ptcl</ta>
            <ta e="T682" id="Seg_11027" s="T681">v</ta>
            <ta e="T683" id="Seg_11028" s="T682">v</ta>
            <ta e="T684" id="Seg_11029" s="T683">ptcl</ta>
            <ta e="T685" id="Seg_11030" s="T684">adv</ta>
            <ta e="T686" id="Seg_11031" s="T685">adj</ta>
            <ta e="T687" id="Seg_11032" s="T686">adj</ta>
            <ta e="T688" id="Seg_11033" s="T687">n</ta>
            <ta e="T689" id="Seg_11034" s="T688">v</ta>
            <ta e="T690" id="Seg_11035" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_11036" s="T690">v</ta>
            <ta e="T692" id="Seg_11037" s="T691">adv</ta>
            <ta e="T693" id="Seg_11038" s="T692">ptcl</ta>
            <ta e="T694" id="Seg_11039" s="T693">v</ta>
            <ta e="T695" id="Seg_11040" s="T694">adv</ta>
            <ta e="T696" id="Seg_11041" s="T695">adv</ta>
            <ta e="T697" id="Seg_11042" s="T696">v</ta>
            <ta e="T698" id="Seg_11043" s="T697">ptcl</ta>
            <ta e="T699" id="Seg_11044" s="T698">v</ta>
            <ta e="T700" id="Seg_11045" s="T699">n</ta>
            <ta e="T701" id="Seg_11046" s="T700">adv</ta>
            <ta e="T702" id="Seg_11047" s="T701">v</ta>
            <ta e="T703" id="Seg_11048" s="T702">v</ta>
            <ta e="T704" id="Seg_11049" s="T703">n</ta>
            <ta e="T705" id="Seg_11050" s="T704">n</ta>
            <ta e="T706" id="Seg_11051" s="T705">v</ta>
            <ta e="T707" id="Seg_11052" s="T706">interj</ta>
            <ta e="T708" id="Seg_11053" s="T707">n</ta>
            <ta e="T709" id="Seg_11054" s="T708">ptcl</ta>
            <ta e="T710" id="Seg_11055" s="T709">v</ta>
            <ta e="T711" id="Seg_11056" s="T710">n</ta>
            <ta e="T712" id="Seg_11057" s="T711">v</ta>
            <ta e="T713" id="Seg_11058" s="T712">n</ta>
            <ta e="T714" id="Seg_11059" s="T713">adv</ta>
            <ta e="T715" id="Seg_11060" s="T714">adv</ta>
            <ta e="T716" id="Seg_11061" s="T715">v</ta>
            <ta e="T717" id="Seg_11062" s="T716">n</ta>
            <ta e="T718" id="Seg_11063" s="T717">adv</ta>
            <ta e="T719" id="Seg_11064" s="T718">v</ta>
            <ta e="T720" id="Seg_11065" s="T719">adv</ta>
            <ta e="T721" id="Seg_11066" s="T720">adv</ta>
            <ta e="T722" id="Seg_11067" s="T721">ptcl</ta>
            <ta e="T723" id="Seg_11068" s="T722">v</ta>
            <ta e="T724" id="Seg_11069" s="T723">adv</ta>
            <ta e="T725" id="Seg_11070" s="T724">interrog</ta>
            <ta e="T726" id="Seg_11071" s="T725">v</ta>
            <ta e="T727" id="Seg_11072" s="T726">interj</ta>
            <ta e="T728" id="Seg_11073" s="T727">pers</ta>
            <ta e="T729" id="Seg_11074" s="T728">ptcl</ta>
            <ta e="T730" id="Seg_11075" s="T729">v</ta>
            <ta e="T731" id="Seg_11076" s="T730">v</ta>
            <ta e="T732" id="Seg_11077" s="T731">v</ta>
            <ta e="T733" id="Seg_11078" s="T732">num</ta>
            <ta e="T734" id="Seg_11079" s="T733">n</ta>
            <ta e="T735" id="Seg_11080" s="T734">adv</ta>
            <ta e="T736" id="Seg_11081" s="T735">v</ta>
            <ta e="T737" id="Seg_11082" s="T736">pers</ta>
            <ta e="T738" id="Seg_11083" s="T737">adv</ta>
            <ta e="T739" id="Seg_11084" s="T738">adv</ta>
            <ta e="T740" id="Seg_11085" s="T739">adv</ta>
            <ta e="T741" id="Seg_11086" s="T740">v</ta>
            <ta e="T742" id="Seg_11087" s="T741">adv</ta>
            <ta e="T743" id="Seg_11088" s="T742">adv</ta>
            <ta e="T744" id="Seg_11089" s="T743">adv</ta>
            <ta e="T745" id="Seg_11090" s="T744">ptcl</ta>
            <ta e="T746" id="Seg_11091" s="T745">v</ta>
            <ta e="T747" id="Seg_11092" s="T746">adv</ta>
            <ta e="T748" id="Seg_11093" s="T747">adv</ta>
            <ta e="T749" id="Seg_11094" s="T748">ptcl</ta>
            <ta e="T750" id="Seg_11095" s="T749">v</ta>
            <ta e="T751" id="Seg_11096" s="T750">interj</ta>
            <ta e="T752" id="Seg_11097" s="T751">interj</ta>
            <ta e="T753" id="Seg_11098" s="T752">ptcl</ta>
            <ta e="T754" id="Seg_11099" s="T753">nprop</ta>
            <ta e="T755" id="Seg_11100" s="T754">n</ta>
            <ta e="T756" id="Seg_11101" s="T755">qv</ta>
            <ta e="T757" id="Seg_11102" s="T756">v</ta>
            <ta e="T758" id="Seg_11103" s="T757">conj</ta>
            <ta e="T759" id="Seg_11104" s="T758">v</ta>
            <ta e="T760" id="Seg_11105" s="T759">conj</ta>
            <ta e="T761" id="Seg_11106" s="T760">qv</ta>
            <ta e="T762" id="Seg_11107" s="T761">nprop</ta>
            <ta e="T763" id="Seg_11108" s="T762">n</ta>
            <ta e="T764" id="Seg_11109" s="T763">adv</ta>
            <ta e="T765" id="Seg_11110" s="T764">adv</ta>
            <ta e="T766" id="Seg_11111" s="T765">v</ta>
            <ta e="T767" id="Seg_11112" s="T766">adv</ta>
            <ta e="T768" id="Seg_11113" s="T767">adv</ta>
            <ta e="T769" id="Seg_11114" s="T768">v</ta>
            <ta e="T770" id="Seg_11115" s="T769">pers</ta>
            <ta e="T771" id="Seg_11116" s="T770">adv</ta>
            <ta e="T772" id="Seg_11117" s="T771">adv</ta>
            <ta e="T773" id="Seg_11118" s="T772">adj</ta>
            <ta e="T774" id="Seg_11119" s="T773">v</ta>
            <ta e="T775" id="Seg_11120" s="T774">adv</ta>
            <ta e="T776" id="Seg_11121" s="T775">adv</ta>
            <ta e="T777" id="Seg_11122" s="T776">adv</ta>
            <ta e="T778" id="Seg_11123" s="T777">adv</ta>
            <ta e="T779" id="Seg_11124" s="T778">dem</ta>
            <ta e="T780" id="Seg_11125" s="T779">n</ta>
            <ta e="T781" id="Seg_11126" s="T780">v</ta>
            <ta e="T782" id="Seg_11127" s="T781">v</ta>
            <ta e="T783" id="Seg_11128" s="T782">ptcl</ta>
            <ta e="T784" id="Seg_11129" s="T783">nprop</ta>
            <ta e="T785" id="Seg_11130" s="T784">n</ta>
            <ta e="T786" id="Seg_11131" s="T785">n</ta>
            <ta e="T787" id="Seg_11132" s="T786">v</ta>
            <ta e="T788" id="Seg_11133" s="T787">pers</ta>
            <ta e="T789" id="Seg_11134" s="T788">ptcl</ta>
            <ta e="T790" id="Seg_11135" s="T789">adv</ta>
            <ta e="T791" id="Seg_11136" s="T790">adv</ta>
            <ta e="T792" id="Seg_11137" s="T791">v</ta>
            <ta e="T793" id="Seg_11138" s="T792">n</ta>
            <ta e="T794" id="Seg_11139" s="T793">adv</ta>
            <ta e="T795" id="Seg_11140" s="T794">adj</ta>
            <ta e="T796" id="Seg_11141" s="T795">v</ta>
            <ta e="T797" id="Seg_11142" s="T796">adj</ta>
            <ta e="T798" id="Seg_11143" s="T797">adv</ta>
            <ta e="T799" id="Seg_11144" s="T798">adv</ta>
            <ta e="T800" id="Seg_11145" s="T799">v</ta>
            <ta e="T801" id="Seg_11146" s="T800">n</ta>
            <ta e="T802" id="Seg_11147" s="T801">conj</ta>
            <ta e="T803" id="Seg_11148" s="T802">v</ta>
            <ta e="T804" id="Seg_11149" s="T803">conj</ta>
            <ta e="T805" id="Seg_11150" s="T804">ptcl</ta>
            <ta e="T806" id="Seg_11151" s="T805">v</ta>
            <ta e="T807" id="Seg_11152" s="T806">adj</ta>
            <ta e="T808" id="Seg_11153" s="T807">adv</ta>
            <ta e="T809" id="Seg_11154" s="T808">n</ta>
            <ta e="T810" id="Seg_11155" s="T809">n</ta>
            <ta e="T811" id="Seg_11156" s="T810">ptcl</ta>
            <ta e="T812" id="Seg_11157" s="T811">ptcl</ta>
            <ta e="T813" id="Seg_11158" s="T812">pers</ta>
            <ta e="T814" id="Seg_11159" s="T813">v</ta>
            <ta e="T815" id="Seg_11160" s="T814">ptcl</ta>
            <ta e="T816" id="Seg_11161" s="T815">v</ta>
            <ta e="T817" id="Seg_11162" s="T816">v</ta>
            <ta e="T818" id="Seg_11163" s="T817">adj</ta>
            <ta e="T819" id="Seg_11164" s="T818">conj</ta>
            <ta e="T820" id="Seg_11165" s="T819">v</ta>
            <ta e="T821" id="Seg_11166" s="T820">conj</ta>
            <ta e="T822" id="Seg_11167" s="T821">ptcl</ta>
            <ta e="T823" id="Seg_11168" s="T822">v</ta>
            <ta e="T824" id="Seg_11169" s="T823">num</ta>
            <ta e="T825" id="Seg_11170" s="T824">n</ta>
            <ta e="T826" id="Seg_11171" s="T825">ptcl</ta>
            <ta e="T827" id="Seg_11172" s="T826">v</ta>
            <ta e="T828" id="Seg_11173" s="T827">n</ta>
            <ta e="T829" id="Seg_11174" s="T828">n</ta>
            <ta e="T830" id="Seg_11175" s="T829">ptcl</ta>
            <ta e="T831" id="Seg_11176" s="T830">v</ta>
            <ta e="T832" id="Seg_11177" s="T831">nprop</ta>
            <ta e="T833" id="Seg_11178" s="T832">n</ta>
            <ta e="T834" id="Seg_11179" s="T833">v</ta>
            <ta e="T835" id="Seg_11180" s="T834">n</ta>
            <ta e="T836" id="Seg_11181" s="T835">n</ta>
            <ta e="T837" id="Seg_11182" s="T836">n</ta>
            <ta e="T838" id="Seg_11183" s="T837">adv</ta>
            <ta e="T839" id="Seg_11184" s="T838">v</ta>
            <ta e="T840" id="Seg_11185" s="T839">v</ta>
            <ta e="T841" id="Seg_11186" s="T840">n</ta>
            <ta e="T842" id="Seg_11187" s="T841">n</ta>
            <ta e="T843" id="Seg_11188" s="T842">adv</ta>
            <ta e="T844" id="Seg_11189" s="T843">adv</ta>
            <ta e="T845" id="Seg_11190" s="T844">v</ta>
            <ta e="T846" id="Seg_11191" s="T845">v</ta>
            <ta e="T847" id="Seg_11192" s="T846">adv</ta>
            <ta e="T848" id="Seg_11193" s="T847">adv</ta>
            <ta e="T849" id="Seg_11194" s="T848">ptcl</ta>
            <ta e="T850" id="Seg_11195" s="T849">v</ta>
            <ta e="T851" id="Seg_11196" s="T850">v</ta>
            <ta e="T852" id="Seg_11197" s="T851">ptcl</ta>
            <ta e="T853" id="Seg_11198" s="T852">interrog</ta>
            <ta e="T854" id="Seg_11199" s="T853">v</ta>
            <ta e="T855" id="Seg_11200" s="T854">ptcl</ta>
            <ta e="T856" id="Seg_11201" s="T855">ptcl</ta>
            <ta e="T857" id="Seg_11202" s="T856">num</ta>
            <ta e="T858" id="Seg_11203" s="T857">v</ta>
            <ta e="T859" id="Seg_11204" s="T858">adj</ta>
            <ta e="T860" id="Seg_11205" s="T859">adv</ta>
            <ta e="T861" id="Seg_11206" s="T860">preverb</ta>
            <ta e="T862" id="Seg_11207" s="T861">v</ta>
            <ta e="T863" id="Seg_11208" s="T862">ptcl</ta>
            <ta e="T864" id="Seg_11209" s="T863">n</ta>
            <ta e="T865" id="Seg_11210" s="T864">v</ta>
            <ta e="T866" id="Seg_11211" s="T865">n</ta>
            <ta e="T867" id="Seg_11212" s="T866">v</ta>
            <ta e="T868" id="Seg_11213" s="T867">n</ta>
            <ta e="T869" id="Seg_11214" s="T868">v</ta>
            <ta e="T870" id="Seg_11215" s="T869">adv</ta>
            <ta e="T871" id="Seg_11216" s="T870">n</ta>
            <ta e="T872" id="Seg_11217" s="T871">v</ta>
            <ta e="T873" id="Seg_11218" s="T872">num</ta>
            <ta e="T874" id="Seg_11219" s="T873">adv</ta>
            <ta e="T875" id="Seg_11220" s="T874">v</ta>
            <ta e="T876" id="Seg_11221" s="T875">n</ta>
            <ta e="T877" id="Seg_11222" s="T876">v</ta>
            <ta e="T878" id="Seg_11223" s="T877">adv</ta>
            <ta e="T879" id="Seg_11224" s="T878">n</ta>
            <ta e="T880" id="Seg_11225" s="T879">v</ta>
            <ta e="T881" id="Seg_11226" s="T880">adj</ta>
            <ta e="T882" id="Seg_11227" s="T881">n</ta>
            <ta e="T883" id="Seg_11228" s="T882">dem</ta>
            <ta e="T884" id="Seg_11229" s="T883">n</ta>
            <ta e="T885" id="Seg_11230" s="T884">n</ta>
            <ta e="T886" id="Seg_11231" s="T885">v</ta>
            <ta e="T887" id="Seg_11232" s="T886">dem</ta>
            <ta e="T888" id="Seg_11233" s="T887">n</ta>
            <ta e="T889" id="Seg_11234" s="T888">nprop</ta>
            <ta e="T890" id="Seg_11235" s="T889">n</ta>
            <ta e="T891" id="Seg_11236" s="T890">v</ta>
            <ta e="T892" id="Seg_11237" s="T891">dem</ta>
            <ta e="T893" id="Seg_11238" s="T892">n</ta>
            <ta e="T894" id="Seg_11239" s="T893">n</ta>
            <ta e="T895" id="Seg_11240" s="T894">v</ta>
            <ta e="T896" id="Seg_11241" s="T895">n</ta>
            <ta e="T897" id="Seg_11242" s="T896">v</ta>
            <ta e="T898" id="Seg_11243" s="T897">n</ta>
            <ta e="T899" id="Seg_11244" s="T898">ptcl</ta>
            <ta e="T900" id="Seg_11245" s="T899">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T1" id="Seg_11246" s="T0">Сыльча-Пыльча.</ta>
            <ta e="T4" id="Seg_11247" s="T1">Сыльча-Пыльча Кэш жил.</ta>
            <ta e="T6" id="Seg_11248" s="T4">У него была мать.</ta>
            <ta e="T8" id="Seg_11249" s="T6">Отца не было.</ta>
            <ta e="T10" id="Seg_11250" s="T8">Он живёт, живёт.</ta>
            <ta e="T24" id="Seg_11251" s="T10">Однажды он матери говорит, матери говорит: "Я пойду людей искать.</ta>
            <ta e="T27" id="Seg_11252" s="T24">Как мы так жить будем?</ta>
            <ta e="T37" id="Seg_11253" s="T27">Мама, неужели ты в этих краях не знаешь [какого-нибудь] человека?"</ta>
            <ta e="T45" id="Seg_11254" s="T37">Мать говорит: "Я никаких людей не знаю.</ta>
            <ta e="T51" id="Seg_11255" s="T45">В этих краях близко людей нет".</ta>
            <ta e="T59" id="Seg_11256" s="T51">Сын матери так говорит: “Ты, мол, одна живи. </ta>
            <ta e="T64" id="Seg_11257" s="T59">Я скоро пойду искать людей”.</ta>
            <ta e="T71" id="Seg_11258" s="T64">Потом по этой местности Сыльча-Пыльча Кэш пошёл.</ta>
            <ta e="T75" id="Seg_11259" s="T71">Сыльча-Пыльча Кэш долго шёл.</ta>
            <ta e="T78" id="Seg_11260" s="T75">Зима и лето прошли.</ta>
            <ta e="T84" id="Seg_11261" s="T78">Он большую реку, маленькие реки [переходил?].</ta>
            <ta e="T89" id="Seg_11262" s="T84">Никаких людей нет.</ta>
            <ta e="T94" id="Seg_11263" s="T89">Он через озёра, реки шёл.</ta>
            <ta e="T99" id="Seg_11264" s="T94">Он от шапки на лбу и до пояса заиндевел.</ta>
            <ta e="T105" id="Seg_11265" s="T99">Однажды он пришёл в лес, большой лес.</ta>
            <ta e="T109" id="Seg_11266" s="T105">По этому лесу река проходила.</ta>
            <ta e="T113" id="Seg_11267" s="T109">По берегу этой реки шёл я.</ta>
            <ta e="T122" id="Seg_11268" s="T113">Один раз он что-то слышит: бух, бух!</ta>
            <ta e="T126" id="Seg_11269" s="T122">Идёт, слышит голос.</ta>
            <ta e="T132" id="Seg_11270" s="T126">Никакого зверя, никого нет.</ta>
            <ta e="T139" id="Seg_11271" s="T132">Это место вокруг обошёл: "Ну-ка, кто там говорит? </ta>
            <ta e="T143" id="Seg_11272" s="T139">Я бы хоть взглянул."</ta>
            <ta e="T147" id="Seg_11273" s="T143">Он идёт, идёт.</ta>
            <ta e="T162" id="Seg_11274" s="T147">Всё ещё будто бы в низовье реки слышно, что ведь опять [кто-то] кричит: бух, бух!</ta>
            <ta e="T167" id="Seg_11275" s="T162">Такой звук эхом раздаётся.</ta>
            <ta e="T169" id="Seg_11276" s="T167">Он идёт, идёт. </ta>
            <ta e="T175" id="Seg_11277" s="T169">В низовье посередине реки что-то чернеет.</ta>
            <ta e="T182" id="Seg_11278" s="T175">Тот незнакомый кричит: "Бух, бух!"</ta>
            <ta e="T189" id="Seg_11279" s="T182">Нечеловеческий голос только так кричит.</ta>
            <ta e="T192" id="Seg_11280" s="T189">Он туда пришёл.</ta>
            <ta e="T196" id="Seg_11281" s="T192">Видит, человек так кричал.</ta>
            <ta e="T209" id="Seg_11282" s="T196">Туда только пришёл, видит, человек, этот человек голый, в чём мать родила [букв.: мясом отца и матери голый].</ta>
            <ta e="T218" id="Seg_11283" s="T209">Он голый, в чём мать родила [букв.: мясом отца и матери голый], сидит на краю проруби. </ta>
            <ta e="T223" id="Seg_11284" s="T218">Этот что ли, этот человек кричал?</ta>
            <ta e="T227" id="Seg_11285" s="T223">"Ты зачем [тут] такой сидишь?"</ta>
            <ta e="T234" id="Seg_11286" s="T227">– "Нет, я сижу, чтобы чёрт меня съел.</ta>
            <ta e="T237" id="Seg_11287" s="T234">На берегу мои люди.</ta>
            <ta e="T243" id="Seg_11288" s="T237">Они меня посадили на съедение чёрту.</ta>
            <ta e="T255" id="Seg_11289" s="T243">Просто [если] я выйду на берег, чёрт всех нас съест, никого не оставит.</ta>
            <ta e="T259" id="Seg_11290" s="T255">Вот зачем меня посадили?"</ta>
            <ta e="T262" id="Seg_11291" s="T259">– "Ты иди на берег."</ta>
            <ta e="T266" id="Seg_11292" s="T262">– "Ты тоже иди на берег.</ta>
            <ta e="T269" id="Seg_11293" s="T266">Я как выйду? </ta>
            <ta e="T272" id="Seg_11294" s="T269">Люди меня увидят."</ta>
            <ta e="T277" id="Seg_11295" s="T272">– "Я говорю, выходи."</ta>
            <ta e="T280" id="Seg_11296" s="T277">На берег они вышли.</ta>
            <ta e="T286" id="Seg_11297" s="T280">Сыльча-Пыльча Кэш вырезал [человеческое] лицо на дереве отказом.</ta>
            <ta e="T293" id="Seg_11298" s="T286">"Ты смотри, про меня ничего не говори!</ta>
            <ta e="T301" id="Seg_11299" s="T293">Чёрт тебя будет спрашивать, куда Сыльча-Пыльча подевался.</ta>
            <ta e="T305" id="Seg_11300" s="T301">Про меня [нисколько не?] говори."</ta>
            <ta e="T308" id="Seg_11301" s="T305">Так пригрозил отказом.</ta>
            <ta e="T315" id="Seg_11302" s="T308">На берег пошёл, там видит: землянка.</ta>
            <ta e="T322" id="Seg_11303" s="T315">Там в дверном проёме он тоже [человеческое] лицо вырезал.</ta>
            <ta e="T324" id="Seg_11304" s="T322">"Заходи в дом. </ta>
            <ta e="T327" id="Seg_11305" s="T324">Зачем стоишь, мёрзнешь?"</ta>
            <ta e="T333" id="Seg_11306" s="T327">– "Когда я зайду, мои люди меня увидят.</ta>
            <ta e="T337" id="Seg_11307" s="T333">Ты только в дом заходи."</ta>
            <ta e="T343" id="Seg_11308" s="T337">В этом дверном проёме он тоже [человеческое] лицо вырезал.</ta>
            <ta e="T349" id="Seg_11309" s="T343">"Ты смотри, про меня не говори!"</ta>
            <ta e="T351" id="Seg_11310" s="T349">Отказом пригрозил.</ta>
            <ta e="T357" id="Seg_11311" s="T351">Тот человек всё-таки заходит в дом.</ta>
            <ta e="T361" id="Seg_11312" s="T357">Все его увидели-таки.</ta>
            <ta e="T364" id="Seg_11313" s="T361">Мол, зачем заходишь?</ta>
            <ta e="T370" id="Seg_11314" s="T364">– Нет, меня мол этот человек в дом послал.</ta>
            <ta e="T378" id="Seg_11315" s="T370">Какой-то человек пришёл, меня в дом послал.</ta>
            <ta e="T381" id="Seg_11316" s="T378">Он меня на берег привёл."</ta>
            <ta e="T392" id="Seg_11317" s="T381">Он едва увидел: с передней стороны двери ещё один голый человек появился.</ta>
            <ta e="T399" id="Seg_11318" s="T392">Потом вот что сказал: "Человека оденьте."</ta>
            <ta e="T407" id="Seg_11319" s="T399">Потом вышел и слушает, когда чёрт достаточно [из-под воды] вылезет.</ta>
            <ta e="T412" id="Seg_11320" s="T407">"Если чёрт выйдет, вы в дом заходите.</ta>
            <ta e="T415" id="Seg_11321" s="T412">Один человек пусть слушает."</ta>
            <ta e="T422" id="Seg_11322" s="T415">Говорят, что когда вечерняя заря покрывает [небо], тогда [чёрт] появляется.</ta>
            <ta e="T430" id="Seg_11323" s="T422">Достаточно времени прошло, в одно время человек выходит. </ta>
            <ta e="T434" id="Seg_11324" s="T430">Мол, утром [чёрт] всплывает. </ta>
            <ta e="T436" id="Seg_11325" s="T434">[Человек] в дом забежал.</ta>
            <ta e="T439" id="Seg_11326" s="T436">"Чёрт всплывает!</ta>
            <ta e="T448" id="Seg_11327" s="T439">А вы мол дальше тихо сидите, ни разу не выходите."</ta>
            <ta e="T452" id="Seg_11328" s="T448">Сыльча-Пыльча Кэш слушает, что [там]. </ta>
            <ta e="T456" id="Seg_11329" s="T452">Он на берег выходит.</ta>
            <ta e="T458" id="Seg_11330" s="T456">Видит рябчика.</ta>
            <ta e="T461" id="Seg_11331" s="T458">Рябчик: чирик-чирик.</ta>
            <ta e="T465" id="Seg_11332" s="T461">"Сыльча-Пыльча Кэш куда делся?"</ta>
            <ta e="T469" id="Seg_11333" s="T465">– "Сыльча-Пыльча недавно ушёл."</ta>
            <ta e="T474" id="Seg_11334" s="T469">На берег он выходит, видит [вырезанное человеческое] лицо.</ta>
            <ta e="T480" id="Seg_11335" s="T474">[Вырезанное человеческое] лицо говорит: "Сыльча-Пыльча Кэш дома сидит."</ta>
            <ta e="T487" id="Seg_11336" s="T480">Чёрт, повернув к берегу, убежал (/ушёл), упал в воду.</ta>
            <ta e="T490" id="Seg_11337" s="T487">Будто бульк [в воду].</ta>
            <ta e="T500" id="Seg_11338" s="T490">Выбежав на улицу, тот Сыльча-Пыльча Кэш разрубил и сломал [вырезанное человеческое] лицо, прочь его бросил.</ta>
            <ta e="T503" id="Seg_11339" s="T500">Другое [человеческое] лицо сделал.</ta>
            <ta e="T513" id="Seg_11340" s="T503">Лицу погрозил: "Ты смотри, про меня никогда не говори!"</ta>
            <ta e="T517" id="Seg_11341" s="T513">Теперь он выходит на рассвете.</ta>
            <ta e="T522" id="Seg_11342" s="T517">Дальше они всю ночь сидят.</ta>
            <ta e="T531" id="Seg_11343" s="T522">На рассвете они послали человека на улицу, смотрят, не выходит ли [чёрт].</ta>
            <ta e="T535" id="Seg_11344" s="T531">Сыльчу-Пыльчу Кэш послали: "Послушай."</ta>
            <ta e="T543" id="Seg_11345" s="T535">Какое-то время человек [там] тихо побыл, [потом] в дом забежал. </ta>
            <ta e="T547" id="Seg_11346" s="T543">Будто бы один чёрт появился.</ta>
            <ta e="T550" id="Seg_11347" s="T547">Чёрт появился.</ta>
            <ta e="T554" id="Seg_11348" s="T550">Вроде Сыльча-Пыльча сам слышит.</ta>
            <ta e="T557" id="Seg_11349" s="T554">[Чёрт] выходит на берег.</ta>
            <ta e="T563" id="Seg_11350" s="T557">Шагая, идёт, шагая, идёт.</ta>
            <ta e="T568" id="Seg_11351" s="T563">На этот холм поднимается.</ta>
            <ta e="T573" id="Seg_11352" s="T568">К [вырезанному человеческому] лицу он подходит. </ta>
            <ta e="T577" id="Seg_11353" s="T573">Будто чирик-чирик (?).</ta>
            <ta e="T584" id="Seg_11354" s="T577">"Куда Сыльча-Пыльча Кэш делся, дома ли он?"</ta>
            <ta e="T588" id="Seg_11355" s="T584">– "Сыльча-Пыльча тогда [ещё] ушёл.</ta>
            <ta e="T593" id="Seg_11356" s="T588">Ты иди на берег, поешь еды."</ta>
            <ta e="T602" id="Seg_11357" s="T593">Тот шагая пошёл, к дверному проёму подошёл, к [вырезанному человеческому] лицу пришёл.</ta>
            <ta e="T603" id="Seg_11358" s="T602">"Куда [Сыльча-Пыльча] делся?"</ta>
            <ta e="T611" id="Seg_11359" s="T603">– "Сыльча-Пыльча Кэш, он же вчера тогда ещё ушёл.</ta>
            <ta e="T615" id="Seg_11360" s="T611">Зайди в дом, поешь еды."</ta>
            <ta e="T624" id="Seg_11361" s="T615">В одно время чёрт в дом толкнулся, в дом толкнулся.</ta>
            <ta e="T633" id="Seg_11362" s="T624">Едва чёрт в дом зашёл, как Сыльча-Пыльча Кэш ему голову отрубил. </ta>
            <ta e="T637" id="Seg_11363" s="T633">По дому она покатилась.</ta>
            <ta e="T642" id="Seg_11364" s="T637">Тело назад на улицу выпало.</ta>
            <ta e="T643" id="Seg_11365" s="T642">Он убил [чёрта].</ta>
            <ta e="T650" id="Seg_11366" s="T643">Сыльча-Пыльча Кэш вынес на улицу тело.</ta>
            <ta e="T654" id="Seg_11367" s="T650">[Когда] он на улицу вышел, уже посветлело.</ta>
            <ta e="T663" id="Seg_11368" s="T654">Сыльча-Пыльча унёс вниз [к воде] тело, положил его в прорубь. </ta>
            <ta e="T668" id="Seg_11369" s="T663">Вместе со льдом заморозил, так положил.</ta>
            <ta e="T673" id="Seg_11370" s="T668">Около проруби так положил.</ta>
            <ta e="T677" id="Seg_11371" s="T673">Как будто [чёрт] живой лежит. </ta>
            <ta e="T680" id="Seg_11372" s="T677">С головой вместе торчком поставил.</ta>
            <ta e="T682" id="Seg_11373" s="T680">[День] мы продневали. </ta>
            <ta e="T683" id="Seg_11374" s="T682">Стемнело.</ta>
            <ta e="T691" id="Seg_11375" s="T683">Говорят, [там] только двое, [а] тот другой ходит ли, не знаем.</ta>
            <ta e="T694" id="Seg_11376" s="T691">Вечером стемнело. </ta>
            <ta e="T697" id="Seg_11377" s="T694">Вечером какое-то время сидят.</ta>
            <ta e="T699" id="Seg_11378" s="T697">Начало смеркаться.</ta>
            <ta e="T703" id="Seg_11379" s="T699">Человека на улицу посылают: "Послушай!"</ta>
            <ta e="T706" id="Seg_11380" s="T703">Человек на улице стоит.</ta>
            <ta e="T707" id="Seg_11381" s="T706">"Ой!</ta>
            <ta e="T710" id="Seg_11382" s="T707">Человек всплывает!"</ta>
            <ta e="T712" id="Seg_11383" s="T710">[Тот] в дом забежал. </ta>
            <ta e="T719" id="Seg_11384" s="T712">Зайдя в дом, сказал: "Чёрт внизу всплывает! </ta>
            <ta e="T723" id="Seg_11385" s="T719">На берег, шагая, выходит."</ta>
            <ta e="T726" id="Seg_11386" s="T723">– "Вверх кто всплывает?</ta>
            <ta e="T727" id="Seg_11387" s="T726">А-а-а!</ta>
            <ta e="T732" id="Seg_11388" s="T727">Ты что ли окоченел, объелся, испугался?"</ta>
            <ta e="T736" id="Seg_11389" s="T732">Одного [чёрт] увидел.</ta>
            <ta e="T741" id="Seg_11390" s="T736">"Я раньше, поевши, так полёживал." </ta>
            <ta e="T746" id="Seg_11391" s="T741">Потом, шагая, он пришёл наверх.</ta>
            <ta e="T750" id="Seg_11392" s="T746">Наверх, шагая, пришёл.</ta>
            <ta e="T752" id="Seg_11393" s="T750">Чирик-чирик (?).</ta>
            <ta e="T757" id="Seg_11394" s="T752">"Сыльча-Пыльча Кэш куда делся? </ta>
            <ta e="T761" id="Seg_11395" s="T757">То ли ушёл, то ли куда делся?"</ta>
            <ta e="T769" id="Seg_11396" s="T761">– "Сыльча-Пыльча Кэш раньше тогда ещё ушёл, недавно тогда ещё ушёл. </ta>
            <ta e="T774" id="Seg_11397" s="T769">Ты иди на берег, поешь еды."</ta>
            <ta e="T781" id="Seg_11398" s="T774">Потом на берег пошёл и пришёл к тому [вырезанному человеческому] лицу.</ta>
            <ta e="T787" id="Seg_11399" s="T781">Сказал: "Сыльча-Пыльча Кэш дома ли?"</ta>
            <ta e="T792" id="Seg_11400" s="T787">"Он же тогда ещё недавно ушёл. </ta>
            <ta e="T796" id="Seg_11401" s="T792">Зайди в дом, поешь еды."</ta>
            <ta e="T809" id="Seg_11402" s="T796">Тот другой так говорит: "Зайду в дом или не зайду", – опять другое что-то.</ta>
            <ta e="T814" id="Seg_11403" s="T809">"Лицо, ты меня путаешь что ли?"</ta>
            <ta e="T823" id="Seg_11404" s="T814">Вроде гадает: то ли тот другой зайдёт, то ли не зайдёт.</ta>
            <ta e="T827" id="Seg_11405" s="T823">В одно время начал заходить [в дом].</ta>
            <ta e="T836" id="Seg_11406" s="T827">Едва он в дом толкнулся головой, как Сыльча-Пыльча Кэш ему голову отрубил.</ta>
            <ta e="T840" id="Seg_11407" s="T836">[Голова] в дом вперёд упала, покатилась [по полу].</ta>
            <ta e="T845" id="Seg_11408" s="T840">Тело назад наружу упало.</ta>
            <ta e="T846" id="Seg_11409" s="T845">Он [чёрта] убил.</ta>
            <ta e="T854" id="Seg_11410" s="T846">Потом они сидят, сидят, никого нет. </ta>
            <ta e="T858" id="Seg_11411" s="T854">Мол, [там] двое ходят ли?</ta>
            <ta e="T862" id="Seg_11412" s="T858">На следующее утро рассвело.</ta>
            <ta e="T865" id="Seg_11413" s="T862">[Сыльча-Пыльча говорит:] "Дрова, мол, нарубите."</ta>
            <ta e="T867" id="Seg_11414" s="T865">Они дрова рубят.</ta>
            <ta e="T869" id="Seg_11415" s="T867">"Огонь разожгите."</ta>
            <ta e="T872" id="Seg_11416" s="T869">И правда, дрова рубят.</ta>
            <ta e="T880" id="Seg_11417" s="T872">Обоих вниз унесли, на огне сожгли, на огне совсем сожгли.</ta>
            <ta e="T886" id="Seg_11418" s="T880">Старик с отцом и детьми, у этого старика дочь была.</ta>
            <ta e="T891" id="Seg_11419" s="T886">Эту дочь он Сыльче-Пыльче Кэш отдал.</ta>
            <ta e="T897" id="Seg_11420" s="T891">Вокруг света он обошёл, пришёл к селькупам.</ta>
            <ta e="T900" id="Seg_11421" s="T897">Тут и конец.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1" id="Seg_11422" s="T0">Sylcha-Pylcha.</ta>
            <ta e="T4" id="Seg_11423" s="T1">There lived Sylcha-Pykcha Kash.</ta>
            <ta e="T6" id="Seg_11424" s="T4">He had got a mother.</ta>
            <ta e="T8" id="Seg_11425" s="T6">He didn't have a father.</ta>
            <ta e="T10" id="Seg_11426" s="T8">So he lived and lived.</ta>
            <ta e="T24" id="Seg_11427" s="T10">Once he said so to his mother, to his mother he said so then: “I'll go look for people.</ta>
            <ta e="T27" id="Seg_11428" s="T24">How are we going to live so?</ta>
            <ta e="T37" id="Seg_11429" s="T27">Mother, don't you know [some] people in this area?</ta>
            <ta e="T45" id="Seg_11430" s="T37">His mother says: “I don't know any people.</ta>
            <ta e="T51" id="Seg_11431" s="T45">There are no people in this area near here.”</ta>
            <ta e="T59" id="Seg_11432" s="T51">The son says like this to his mother: “You, he says, live here alone.</ta>
            <ta e="T64" id="Seg_11433" s="T59">I'll go look for [some] people soon.”</ta>
            <ta e="T71" id="Seg_11434" s="T64">Then Sylcha-Pylcha Kash went through this area.</ta>
            <ta e="T75" id="Seg_11435" s="T71">Sylcha-Pylcha Kash went for a long time.</ta>
            <ta e="T78" id="Seg_11436" s="T75">Winter and summer passed by.</ta>
            <ta e="T84" id="Seg_11437" s="T78">He crossed the big river [Taz], he crossed small rivers.</ta>
            <ta e="T89" id="Seg_11438" s="T84">There were no people there.</ta>
            <ta e="T94" id="Seg_11439" s="T89">He went through the lakes, through the rivers.</ta>
            <ta e="T99" id="Seg_11440" s="T94">He was covered with snow from his cap on the forehead to his waist.</ta>
            <ta e="T105" id="Seg_11441" s="T99">Once he came to a forest, to a big forest.</ta>
            <ta e="T109" id="Seg_11442" s="T105">A river was passing through this forest.</ta>
            <ta e="T113" id="Seg_11443" s="T109">I went along the bank of this river.</ta>
            <ta e="T122" id="Seg_11444" s="T113">Once he hears something: ah plump!, ah plump!</ta>
            <ta e="T126" id="Seg_11445" s="T122">He walks and he hears such a voice.</ta>
            <ta e="T132" id="Seg_11446" s="T126">There is no animal, nobody there.</ta>
            <ta e="T139" id="Seg_11447" s="T132">He walked around this place: “Come on, who is speaking there?</ta>
            <ta e="T143" id="Seg_11448" s="T139">I would have a look at him.”</ta>
            <ta e="T147" id="Seg_11449" s="T143">He walks and he walks.</ta>
            <ta e="T162" id="Seg_11450" s="T147">Still as if [something] is heard from down the river, [someone] is crying again: “Ah plump!, ah plump!”</ta>
            <ta e="T167" id="Seg_11451" s="T162">Such a sound echos apparently.</ta>
            <ta e="T169" id="Seg_11452" s="T167">He walks.</ta>
            <ta e="T175" id="Seg_11453" s="T169">Down the river, in the middle of the river something shows black.</ta>
            <ta e="T182" id="Seg_11454" s="T175">That foreigner is crying: “Ah plumps!, ah plumps!”</ta>
            <ta e="T189" id="Seg_11455" s="T182">Only not a human voice is crying like this.</ta>
            <ta e="T192" id="Seg_11456" s="T189">He came there.</ta>
            <ta e="T196" id="Seg_11457" s="T192">Apparently, a man was crying.</ta>
            <ta e="T209" id="Seg_11458" s="T196">He just came there, he gave a look: this is a man, a naked man in his bare skin [lit. with the meat of his father and his mother].</ta>
            <ta e="T218" id="Seg_11459" s="T209">This naked man in his bare skin [lit. with the meat of his father and his mother] is sitting at the ice hole.</ta>
            <ta e="T223" id="Seg_11460" s="T218">Whether it was this man who had cried?</ta>
            <ta e="T227" id="Seg_11461" s="T223">“Why are you sitting here like that?”</ta>
            <ta e="T234" id="Seg_11462" s="T227">“No, he says, I sit here in order for the devil to eat me.</ta>
            <ta e="T237" id="Seg_11463" s="T234">There are my people on the bank.</ta>
            <ta e="T243" id="Seg_11464" s="T237">They made me sit here, so that the devil could eat me.</ta>
            <ta e="T255" id="Seg_11465" s="T243">If I leave simply so, the devil wil eat all of us, nobebody will stay [alive].</ta>
            <ta e="T259" id="Seg_11466" s="T255">That's why they made me seat here.</ta>
            <ta e="T262" id="Seg_11467" s="T259">Go to the bank“.</ta>
            <ta e="T266" id="Seg_11468" s="T262">“You go to the bank too.”</ta>
            <ta e="T269" id="Seg_11469" s="T266">“How can I go away?</ta>
            <ta e="T272" id="Seg_11470" s="T269">The people will see me.”</ta>
            <ta e="T277" id="Seg_11471" s="T272">“I tell you, go out.”</ta>
            <ta e="T280" id="Seg_11472" s="T277">They went out to the bank.</ta>
            <ta e="T286" id="Seg_11473" s="T280">Sylcha-Pylcha Kash made an idol with his pike (he carved a human fase on the tree).</ta>
            <ta e="T293" id="Seg_11474" s="T286">“Don't you say anything about me!</ta>
            <ta e="T301" id="Seg_11475" s="T293">The devil will ask you, where did Sylcha-Pylcha go.</ta>
            <ta e="T305" id="Seg_11476" s="T301">Don't tell him anything about me.”</ta>
            <ta e="T308" id="Seg_11477" s="T305">So he threatened him with his pike.</ta>
            <ta e="T315" id="Seg_11478" s="T308">He went to the bank and he saw there an earth-house.</ta>
            <ta e="T322" id="Seg_11479" s="T315">He made an idol (carved a face) there in the door opening as well.</ta>
            <ta e="T324" id="Seg_11480" s="T322">“Enter the house.</ta>
            <ta e="T327" id="Seg_11481" s="T324">Why do you stand there freezing?”</ta>
            <ta e="T333" id="Seg_11482" s="T327">“If I come in, my people will see me.</ta>
            <ta e="T337" id="Seg_11483" s="T333">You only come in.”</ta>
            <ta e="T343" id="Seg_11484" s="T337">He made an idol (carved a face) in this door opening as well.</ta>
            <ta e="T349" id="Seg_11485" s="T343">“Don't you tell anything about me!”</ta>
            <ta e="T351" id="Seg_11486" s="T349">He threatened him with his pike.</ta>
            <ta e="T357" id="Seg_11487" s="T351">Nevertheless that man did come in.</ta>
            <ta e="T361" id="Seg_11488" s="T357">And everyone still saw him.</ta>
            <ta e="T364" id="Seg_11489" s="T361">Why do you come in, they say.</ta>
            <ta e="T370" id="Seg_11490" s="T364">“No, he says, this man sent me into the house.</ta>
            <ta e="T378" id="Seg_11491" s="T370">Some man came and sent me into the house.</ta>
            <ta e="T381" id="Seg_11492" s="T378">He brought me to the bank.”</ta>
            <ta e="T392" id="Seg_11493" s="T381">He could hardly see: one more nacked man appeared at the front side of the door.</ta>
            <ta e="T399" id="Seg_11494" s="T392">Then he said like this: “Dress this man.”</ta>
            <ta e="T407" id="Seg_11495" s="T399">Then he went out and started listening, when the devil would appear from under the water.</ta>
            <ta e="T412" id="Seg_11496" s="T407">“If the devil appears, come into the house.</ta>
            <ta e="T415" id="Seg_11497" s="T412">Let one man listen.”</ta>
            <ta e="T422" id="Seg_11498" s="T415">It is said, that when the evening dawn covers [the sky], then [the devil] goes out (appears). (probably at midnight)</ta>
            <ta e="T430" id="Seg_11499" s="T422">Enough time passed, once a man went out.</ta>
            <ta e="T434" id="Seg_11500" s="T430">It is said, [the] devil appears in the morning.</ta>
            <ta e="T436" id="Seg_11501" s="T434">[This man] ran into the house.</ta>
            <ta e="T439" id="Seg_11502" s="T436">“The devil is appearing!</ta>
            <ta e="T448" id="Seg_11503" s="T439">And you, he says, sit quietly, don't you dare coming out.”</ta>
            <ta e="T452" id="Seg_11504" s="T448">Sylcha-Pylcha Kash is listening, what is [there].</ta>
            <ta e="T456" id="Seg_11505" s="T452">He goes out onto the bank.</ta>
            <ta e="T458" id="Seg_11506" s="T456">He sees a hazel-hen.</ta>
            <ta e="T461" id="Seg_11507" s="T458">The hazel-hen: tweet-tweet.</ta>
            <ta e="T465" id="Seg_11508" s="T461">“Where did Sylcha-Pylcha go?”</ta>
            <ta e="T469" id="Seg_11509" s="T465">“Sylcha-Pylcha has just left.”</ta>
            <ta e="T474" id="Seg_11510" s="T469">He comes to the bank and he sees a [carved human] face.</ta>
            <ta e="T480" id="Seg_11511" s="T474">A [carved human] face says: “Sylcha-Pylcha sits at home.”</ta>
            <ta e="T487" id="Seg_11512" s="T480">The devil, having turned to the bank, ran (went) [away], and fell into the water.</ta>
            <ta e="T490" id="Seg_11513" s="T487">As if glug [into the water].</ta>
            <ta e="T500" id="Seg_11514" s="T490">Having run out, that Sylcha-Pylcha Kash chopped and broke the [carved human] face, and threw it away.</ta>
            <ta e="T503" id="Seg_11515" s="T500">He made another [carved human] face.</ta>
            <ta e="T513" id="Seg_11516" s="T503">He threatened the face: “You should never say anything about me!”</ta>
            <ta e="T517" id="Seg_11517" s="T513">Then he went out at dawn.</ta>
            <ta e="T522" id="Seg_11518" s="T517">Then they sit the whole night.</ta>
            <ta e="T531" id="Seg_11519" s="T522">At dawn they sent a man to have a look, whether [the devil] was coming out.</ta>
            <ta e="T535" id="Seg_11520" s="T531">They sent Sylcha-Pylcha Kash: “Listen.”</ta>
            <ta e="T543" id="Seg_11521" s="T535">The man spent some time [there] quietly, [then] he ran into the house.</ta>
            <ta e="T547" id="Seg_11522" s="T543">The devil appeared, he said.</ta>
            <ta e="T550" id="Seg_11523" s="T547">The devil appeared.</ta>
            <ta e="T554" id="Seg_11524" s="T550">Sylcha-Pylcha heard it supposedly himself.</ta>
            <ta e="T557" id="Seg_11525" s="T554">[The devil] comes out onto the bank.</ta>
            <ta e="T563" id="Seg_11526" s="T557">He comes walking, he comes walking.</ta>
            <ta e="T568" id="Seg_11527" s="T563">He goes onto the top of that hill.</ta>
            <ta e="T573" id="Seg_11528" s="T568">He approaches that [carved human] face.</ta>
            <ta e="T577" id="Seg_11529" s="T573">Tweet-tweet, tweet-tweet, he says.</ta>
            <ta e="T584" id="Seg_11530" s="T577">“Where did Sylcha-Pylcha go, is he at home?”</ta>
            <ta e="T588" id="Seg_11531" s="T584">“Sylcha-Pylcha has just left.</ta>
            <ta e="T593" id="Seg_11532" s="T588">You go to the bank, eat some food.”</ta>
            <ta e="T602" id="Seg_11533" s="T593">That [devil] went walking, he approached the door opening, he approached the [carved human] face.</ta>
            <ta e="T603" id="Seg_11534" s="T602">“Where did [Sylcha-Pylcha] go?”</ta>
            <ta e="T611" id="Seg_11535" s="T603">“Sylcha-Pylcha Kash, he went away (left) yesterday.</ta>
            <ta e="T615" id="Seg_11536" s="T611">Enter the house, eat some food.”</ta>
            <ta e="T624" id="Seg_11537" s="T615">Once the devil pushed [himself] (started entering) into the house, he pushed [himself] (started entering) into the house.</ta>
            <ta e="T633" id="Seg_11538" s="T624">The devil had just come in, Sylcha-Pylcha Kash cut off his head.</ta>
            <ta e="T637" id="Seg_11539" s="T633">It rolled forward through the house.</ta>
            <ta e="T642" id="Seg_11540" s="T637">The body fell back outwards.</ta>
            <ta e="T643" id="Seg_11541" s="T642">He killed [the devil].</ta>
            <ta e="T650" id="Seg_11542" s="T643">That Sylcha-Pylcha Kash brought the body out of the house.</ta>
            <ta e="T654" id="Seg_11543" s="T650">When he came out, it was already daytime.</ta>
            <ta e="T663" id="Seg_11544" s="T654">Sylcha-Pylcha brought the body [down] to the water, and put it into the ice hole.</ta>
            <ta e="T668" id="Seg_11545" s="T663">He made it freeze with ice, he put it like this.</ta>
            <ta e="T673" id="Seg_11546" s="T668">He put it like this near the ice hole.</ta>
            <ta e="T677" id="Seg_11547" s="T673">[The devil] lies as if he were alive.</ta>
            <ta e="T680" id="Seg_11548" s="T677">He put it sticking up with his head.</ta>
            <ta e="T682" id="Seg_11549" s="T680">We spent a day.</ta>
            <ta e="T683" id="Seg_11550" s="T682">It got dark.</ta>
            <ta e="T691" id="Seg_11551" s="T683">It is said, there are two [of them], we dont' know whether the other one can walk.</ta>
            <ta e="T694" id="Seg_11552" s="T691">In the evening it got dark .</ta>
            <ta e="T697" id="Seg_11553" s="T694">In the evening they are sitting for some time.</ta>
            <ta e="T699" id="Seg_11554" s="T697">It started getting dark.</ta>
            <ta e="T703" id="Seg_11555" s="T699">They send a man outwards: “Listen!”</ta>
            <ta e="T706" id="Seg_11556" s="T703">The man stands outside.</ta>
            <ta e="T707" id="Seg_11557" s="T706">“Oh!</ta>
            <ta e="T710" id="Seg_11558" s="T707">A man is appearing!”</ta>
            <ta e="T712" id="Seg_11559" s="T710">He ran into the house.</ta>
            <ta e="T719" id="Seg_11560" s="T712">Having entered the house, he said like this: “The devil is coming from under the water.</ta>
            <ta e="T723" id="Seg_11561" s="T719">He goes walking onto the bank.”</ta>
            <ta e="T726" id="Seg_11562" s="T723">“Who is coming onto the bank?</ta>
            <ta e="T727" id="Seg_11563" s="T726">Ah!</ta>
            <ta e="T732" id="Seg_11564" s="T727">Did you grow numb, did you overeat, did you get frightened?”</ta>
            <ta e="T736" id="Seg_11565" s="T732">[The devil] saw someone.</ta>
            <ta e="T741" id="Seg_11566" s="T736">“Earlier I used to lie down, having eaten.”</ta>
            <ta e="T746" id="Seg_11567" s="T741">Then he went walking upwards.</ta>
            <ta e="T750" id="Seg_11568" s="T746">He came walking upwards.</ta>
            <ta e="T752" id="Seg_11569" s="T750">“Tweet-tweet.</ta>
            <ta e="T757" id="Seg_11570" s="T752">Where did Sylcha-Pylcha Kash go?</ta>
            <ta e="T761" id="Seg_11571" s="T757">Did he go away or where did he go?”</ta>
            <ta e="T769" id="Seg_11572" s="T761">“Sylcha-Pylcha had left earlier, he went away then.</ta>
            <ta e="T774" id="Seg_11573" s="T769">You go to the bank and eat some food.”</ta>
            <ta e="T781" id="Seg_11574" s="T774">Then he went onto the bank and came to that [carved human] face.</ta>
            <ta e="T787" id="Seg_11575" s="T781">He said: “Is Sylcha-Pylcha at home?”</ta>
            <ta e="T792" id="Seg_11576" s="T787">“He left lately then.</ta>
            <ta e="T796" id="Seg_11577" s="T792">Come in, eat some food.”</ta>
            <ta e="T809" id="Seg_11578" s="T796">That other one again like this: “Should I enter the house or should I not?” – the other one [said] again something.</ta>
            <ta e="T814" id="Seg_11579" s="T809">“Face, are you confusing me?”</ta>
            <ta e="T823" id="Seg_11580" s="T814">He is like hesitating, should he enter the house or should he not.</ta>
            <ta e="T827" id="Seg_11581" s="T823">Once he started entering [the house].</ta>
            <ta e="T836" id="Seg_11582" s="T827">He had just pushed his head into the house, Sylcha-Pylcha Kash cut his head off.</ta>
            <ta e="T840" id="Seg_11583" s="T836">[The head] rolled forward into the house.</ta>
            <ta e="T845" id="Seg_11584" s="T840">The body fell back outwards.</ta>
            <ta e="T846" id="Seg_11585" s="T845">He killed [the devil].</ta>
            <ta e="T854" id="Seg_11586" s="T846">Then they sit, and sit, noone [is coming].</ta>
            <ta e="T858" id="Seg_11587" s="T854">Whether two [of them] are walking [there]?</ta>
            <ta e="T862" id="Seg_11588" s="T858">Next morning it started getting light.</ta>
            <ta e="T865" id="Seg_11589" s="T862">Chop firewood, he says.</ta>
            <ta e="T867" id="Seg_11590" s="T865">They chop firewood.</ta>
            <ta e="T869" id="Seg_11591" s="T867">“Set the fire.”</ta>
            <ta e="T872" id="Seg_11592" s="T869">They really chop firewood.</ta>
            <ta e="T880" id="Seg_11593" s="T872">They brought both of them to the fire and burnt them, they burnt them completely on the fire.</ta>
            <ta e="T886" id="Seg_11594" s="T880">There was an old man with his father and his children, this old man had got a daughter.</ta>
            <ta e="T891" id="Seg_11595" s="T886">He gave this daughter to Sylcha-Pylcha Kash.</ta>
            <ta e="T897" id="Seg_11596" s="T891">He went around the earth and came to the Selkups.</ta>
            <ta e="T900" id="Seg_11597" s="T897">That's the end.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1" id="Seg_11598" s="T0">Syltscha-Paltscha.</ta>
            <ta e="T4" id="Seg_11599" s="T1">Es lebte Sytscha-Paltscha Kesch.</ta>
            <ta e="T6" id="Seg_11600" s="T4">Er hatte eine Mutter.</ta>
            <ta e="T8" id="Seg_11601" s="T6">Er hatte keinen Vater.</ta>
            <ta e="T10" id="Seg_11602" s="T8">So lebt und lebt er.</ta>
            <ta e="T24" id="Seg_11603" s="T10">Einmal sagt er seiner Mutter, er sagt seiner Mutter: "Ich gehe Leute suchen.</ta>
            <ta e="T27" id="Seg_11604" s="T24">Wie werden wir so leben?</ta>
            <ta e="T37" id="Seg_11605" s="T27">Mutter, kennst du in diesem Land nicht vielleicht [irgendeinen] Menschen?"</ta>
            <ta e="T45" id="Seg_11606" s="T37">Seine Mutter sagt: "Ich kenne überhaupt keine Leute.</ta>
            <ta e="T51" id="Seg_11607" s="T45">In der Umgebung hier gibt es keine Leute."</ta>
            <ta e="T59" id="Seg_11608" s="T51">Der Sohn sagt so zu seiner Mutter: "Du, sagt man, lebst hier alleine.</ta>
            <ta e="T64" id="Seg_11609" s="T59">Ich gehe bald, um Leute zu suchen."</ta>
            <ta e="T71" id="Seg_11610" s="T64">Dann ging Syltscha-Paltscha Kesch durch dieses Land.</ta>
            <ta e="T75" id="Seg_11611" s="T71">Syltscha-Paltscha Kesch ging lange.</ta>
            <ta e="T78" id="Seg_11612" s="T75">Winter und Sommer vergingen.</ta>
            <ta e="T84" id="Seg_11613" s="T78">Er überquerte den großen Fluss, er überquerte kleine Flüsse.</ta>
            <ta e="T89" id="Seg_11614" s="T84">Es sind keine Leute dort.</ta>
            <ta e="T94" id="Seg_11615" s="T89">Er ging durch die Seen, durch die Flüsse.</ta>
            <ta e="T99" id="Seg_11616" s="T94">Von der Mütze auf der Stirn bis zum Gürtel war er mit Schnee bedeckt.</ta>
            <ta e="T105" id="Seg_11617" s="T99">Einmal kam er in einen Wald, in einen großen Wald.</ta>
            <ta e="T109" id="Seg_11618" s="T105">Durch diesen Wald floss ein Fluss.</ta>
            <ta e="T113" id="Seg_11619" s="T109">Ich ging entlang des Ufers dieses Flusses.</ta>
            <ta e="T122" id="Seg_11620" s="T113">Einmal hört er etwas: Ach, bumm, ach, bumm!</ta>
            <ta e="T126" id="Seg_11621" s="T122">Er geht und hört so eine Stimme.</ta>
            <ta e="T132" id="Seg_11622" s="T126">Kein Tier, niemand ist da.</ta>
            <ta e="T139" id="Seg_11623" s="T132">Er geht um diesen Ort herum: "Los, wer spricht dort?</ta>
            <ta e="T143" id="Seg_11624" s="T139">Ich würde ihn anschauen."</ta>
            <ta e="T147" id="Seg_11625" s="T143">Er geht und geht.</ta>
            <ta e="T162" id="Seg_11626" s="T147">Als ob immer noch [etwas] von flussabwärts zu hören ist, [jemand] schreit wohl wieder: "Ach, bumm, ach, bumm!"</ta>
            <ta e="T167" id="Seg_11627" s="T162">So ein Geräusch echot offenbar.</ta>
            <ta e="T169" id="Seg_11628" s="T167">Er geht und geht.</ta>
            <ta e="T175" id="Seg_11629" s="T169">Flussabwärts, in der Mitte des Flusses schimmert etwas schwarz.</ta>
            <ta e="T182" id="Seg_11630" s="T175">Jener Unbekannte schreit: "Ach, bumm, ach, bumm!"</ta>
            <ta e="T189" id="Seg_11631" s="T182">Nur eine nicht menschliche Stimme schreit so.</ta>
            <ta e="T192" id="Seg_11632" s="T189">Er kam dorthin.</ta>
            <ta e="T196" id="Seg_11633" s="T192">Offenbar schrie ein Mensch so.</ta>
            <ta e="T209" id="Seg_11634" s="T196">Er kam gerade dorthin und schaute: ein Mensch, dieser Mensch ist nackt, wie ihn Gott geschaffen hat [wörtl. mit dem Fleisch seines Vaters und seiner Mutter].</ta>
            <ta e="T218" id="Seg_11635" s="T209">Dieser Mensch, wie ihn Gott geschaffen hat [wörtl. mit dem Fleisch seines Vaters und seiner Mutter], sitzt an einem Eisloch.</ta>
            <ta e="T223" id="Seg_11636" s="T218">War es ein Mensch, oder was, der geschrieen hat?</ta>
            <ta e="T227" id="Seg_11637" s="T223">"Warum sitzt du so das?"</ta>
            <ta e="T234" id="Seg_11638" s="T227">"Nein, sagt er, ich sitze hier, damit der Teufel mich frisst.</ta>
            <ta e="T237" id="Seg_11639" s="T234">Am Ufer sind meine Leute.</ta>
            <ta e="T243" id="Seg_11640" s="T237">Sie haben mich hier hingesetzt, damit mich der Teufel frisst.</ta>
            <ta e="T255" id="Seg_11641" s="T243">[Wenn] ich einfach so aufs Ufer gehe, wird der Teufel uns alle fressen, keiner wird übrig bleiben.</ta>
            <ta e="T259" id="Seg_11642" s="T255">Deshalb haben sie mich hier hingesetzt."</ta>
            <ta e="T262" id="Seg_11643" s="T259">"Geh auf das Ufer."</ta>
            <ta e="T266" id="Seg_11644" s="T262">"Geh du auch auf das Ufer."</ta>
            <ta e="T269" id="Seg_11645" s="T266">"Wie kann ich weggehen?</ta>
            <ta e="T272" id="Seg_11646" s="T269">Die Leute werden mich sehen."</ta>
            <ta e="T277" id="Seg_11647" s="T272">"Ich sage dir, geh hoch."</ta>
            <ta e="T280" id="Seg_11648" s="T277">Sie gingen aufs Ufer.</ta>
            <ta e="T286" id="Seg_11649" s="T280">Syltscha-Paltscha Kesch machte ein Götzenbild mit seiner Pike. </ta>
            <ta e="T293" id="Seg_11650" s="T286">"Schau, sag nichts über mich!</ta>
            <ta e="T301" id="Seg_11651" s="T293">Der Teufel wird fragen, wohin Syltscha-Paltscha gegangen ist.</ta>
            <ta e="T305" id="Seg_11652" s="T301">Erzähl ihm nichts über mich."</ta>
            <ta e="T308" id="Seg_11653" s="T305">So drohte er ihm mit der Pike.</ta>
            <ta e="T315" id="Seg_11654" s="T308">Er ging aufs Ufer, dort [war] offenbar eine Erdhütte.</ta>
            <ta e="T322" id="Seg_11655" s="T315">Dort an der Türöffnung machte er auch ein Götzenbild.</ta>
            <ta e="T324" id="Seg_11656" s="T322">"Betrete das Haus.</ta>
            <ta e="T327" id="Seg_11657" s="T324">Warum stehst du dort und frierst?"</ta>
            <ta e="T333" id="Seg_11658" s="T327">"Wenn ich hineinkomme, werden mich meine Leute sehen.</ta>
            <ta e="T337" id="Seg_11659" s="T333">Geh nur du hinein."</ta>
            <ta e="T343" id="Seg_11660" s="T337">An der Türöffnung machte er auch ein Götzenbild.</ta>
            <ta e="T349" id="Seg_11661" s="T343">"Erzähl nichts über mich!"</ta>
            <ta e="T351" id="Seg_11662" s="T349">Er drohte ihm mit der Pike.</ta>
            <ta e="T357" id="Seg_11663" s="T351">Jener Mensch geht dennoch ins Haus hinein.</ta>
            <ta e="T361" id="Seg_11664" s="T357">Und alle sahen ihn trotzdem.</ta>
            <ta e="T364" id="Seg_11665" s="T361">"Warum kommt er wohl rein", sagen sie?</ta>
            <ta e="T370" id="Seg_11666" s="T364">"Nein", sagt er, "dieser Mensch schickte mich ins Haus. </ta>
            <ta e="T378" id="Seg_11667" s="T370">Irgendein Mensch kam und schickte mich ins Haus.</ta>
            <ta e="T381" id="Seg_11668" s="T378">Er brachte mich aufs Ufer."</ta>
            <ta e="T392" id="Seg_11669" s="T381">Er sah kaum: an der Vorderseite der Tür erschien noch ein nackter Mensch.</ta>
            <ta e="T399" id="Seg_11670" s="T392">Dann sagte er so: "Zieht den Menschen an."</ta>
            <ta e="T407" id="Seg_11671" s="T399">Dann ging er hinaus und lauscht, wann der Teufel aus dem Wasser auftaucht.</ta>
            <ta e="T412" id="Seg_11672" s="T407">"Wenn der Teufel auftauch, geht ins Haus hinein.</ta>
            <ta e="T415" id="Seg_11673" s="T412">Ein Mensch soll lauschen."</ta>
            <ta e="T422" id="Seg_11674" s="T415">Man sagt, dass [Teufel] herauskommt, wenn die Abenddämmerung [den Himmel] bedeckt.</ta>
            <ta e="T430" id="Seg_11675" s="T422">Genug Zeit verging, einmal kommt ein Mensch heraus.</ta>
            <ta e="T434" id="Seg_11676" s="T430">Man sagt, [der Teufel] erscheint am Morgen.</ta>
            <ta e="T436" id="Seg_11677" s="T434">[Der Mensch] rannte ins Haus.</ta>
            <ta e="T439" id="Seg_11678" s="T436">"Der Teufel taucht auf!</ta>
            <ta e="T448" id="Seg_11679" s="T439">Und ihr", sagt er, "sitzt leise, kommt keinesfalls raus."</ta>
            <ta e="T452" id="Seg_11680" s="T448">Syltscha-Paltscha Kesch lauscht, was [dort ist].</ta>
            <ta e="T456" id="Seg_11681" s="T452">Er geht aufs Ufer.</ta>
            <ta e="T458" id="Seg_11682" s="T456">Er sieht ein Haselhuhn.</ta>
            <ta e="T461" id="Seg_11683" s="T458">Das Haselhuhn: "Zwitscher-zwitscher.</ta>
            <ta e="T465" id="Seg_11684" s="T461">Wo ist Syltscha-Paltscha hingegangen?"</ta>
            <ta e="T469" id="Seg_11685" s="T465">"Syltscha-Paltscha ist gerade weggegangen."</ta>
            <ta e="T474" id="Seg_11686" s="T469">Er kommt aufs Ufer und sieht ein Götzenbild.</ta>
            <ta e="T480" id="Seg_11687" s="T474">Das Götzenbild sagt: "Syltscha-Paltscha sitzt zuhause."</ta>
            <ta e="T487" id="Seg_11688" s="T480">Der Teufel, nachdem er sich zum Ufer gedreht hatte, lief fort und fiel ins Wasser.</ta>
            <ta e="T490" id="Seg_11689" s="T487">So als ob er gluck machte.</ta>
            <ta e="T500" id="Seg_11690" s="T490">Syltscha-Paltscha rannte nach draußen, hackte das Götzenbild ab und warf es weg.</ta>
            <ta e="T503" id="Seg_11691" s="T500">Er machte ein anderes Götzenbild.</ta>
            <ta e="T513" id="Seg_11692" s="T503">Er drohte [ihm] ins Gesicht: "Schau, sage niemals etwas über mich!"</ta>
            <ta e="T517" id="Seg_11693" s="T513">Dann geht er zur [Morgen]dämmerung hinaus.</ta>
            <ta e="T522" id="Seg_11694" s="T517">Dann sitzen sie die ganze Nacht.</ta>
            <ta e="T531" id="Seg_11695" s="T522">In der [Morgen]dämmerung schickten sie einen Menschen nach draußen, sie schauen, ob nicht [der Teufel] herauskommt.</ta>
            <ta e="T535" id="Seg_11696" s="T531">Sie schickten Syltscha-Paltscha Kesch: "Lausche."</ta>
            <ta e="T543" id="Seg_11697" s="T535">Der Mensch war einige Zeit leise [dort], er rannte ins Haus.</ta>
            <ta e="T547" id="Seg_11698" s="T543">Als ob ein Teufel erschienen ist.</ta>
            <ta e="T550" id="Seg_11699" s="T547">Der Teufel erschien.</ta>
            <ta e="T554" id="Seg_11700" s="T550">Syltscha-Paltscha hört es wohl selber.</ta>
            <ta e="T557" id="Seg_11701" s="T554">[Der Teufel] kommt aufs Ufer.</ta>
            <ta e="T563" id="Seg_11702" s="T557">Er kommt gehend, er kommt gehend.</ta>
            <ta e="T568" id="Seg_11703" s="T563">Er geht auf den Gipfel dieses Hügels.</ta>
            <ta e="T573" id="Seg_11704" s="T568">Er kommt zu dem Götzenbild.</ta>
            <ta e="T577" id="Seg_11705" s="T573">Zwitscher-zwitscher macht es.</ta>
            <ta e="T584" id="Seg_11706" s="T577">"Wo ist Syltscha-Paltscha hin, ist er zuhause?"</ta>
            <ta e="T588" id="Seg_11707" s="T584">"Syltscha-Paltscha ist gerade weggegangen.</ta>
            <ta e="T593" id="Seg_11708" s="T588">Geh du aufs Ufer, iss etwas."</ta>
            <ta e="T602" id="Seg_11709" s="T593">Jener ging, er kam zur Türöffnung, er kam zum Götzenbild.</ta>
            <ta e="T603" id="Seg_11710" s="T602">"Wo ist [Syltscha-Paltscha] hingegangen?"</ta>
            <ta e="T611" id="Seg_11711" s="T603">"Syltscha-Paltscha Kesch, er ist gestern weggegangen.</ta>
            <ta e="T615" id="Seg_11712" s="T611">Geh ins Haus, iss etwas."</ta>
            <ta e="T624" id="Seg_11713" s="T615">Auf einmal schob sich der Teufel ins Haus, er schob sich ins Haus.</ta>
            <ta e="T633" id="Seg_11714" s="T624">Der Teufel war kaum hineingekommen, als Syltscha-Paltscha ihm den Kopf abhackte.</ta>
            <ta e="T637" id="Seg_11715" s="T633">Er rollte weiter durch das Haus.</ta>
            <ta e="T642" id="Seg_11716" s="T637">Der Körper fiel zurück nach draußen.</ta>
            <ta e="T643" id="Seg_11717" s="T642">Er hatte ihn getötet.</ta>
            <ta e="T650" id="Seg_11718" s="T643">Syltscha-Paltscha Kesch brachte den Körper aus dem Haus hinaus.</ta>
            <ta e="T654" id="Seg_11719" s="T650">[Als] er hinauskam, war es schon hell geworden.</ta>
            <ta e="T663" id="Seg_11720" s="T654">Syltscha-Paltscha brachte den Körper zum Wasser und legte ihn in das Eisloch.</ta>
            <ta e="T668" id="Seg_11721" s="T663">Er fror ihn mit dem Eis ein, er legte ihn so hin.</ta>
            <ta e="T673" id="Seg_11722" s="T668">Er legte ihn so neben das Eisloch.</ta>
            <ta e="T677" id="Seg_11723" s="T673">Als ob [der Teufel] dort lebendig liegt.</ta>
            <ta e="T680" id="Seg_11724" s="T677">Er ließ in mit dem Kopf herausragen.</ta>
            <ta e="T682" id="Seg_11725" s="T680">Wir verbrachten einen Tag.</ta>
            <ta e="T683" id="Seg_11726" s="T682">Es wurde dunkel.</ta>
            <ta e="T691" id="Seg_11727" s="T683">Man sagt, [dort] sind zwei, ob der eine geht, wissen wir nicht.</ta>
            <ta e="T694" id="Seg_11728" s="T691">Am Abend wurde es dunkel.</ta>
            <ta e="T697" id="Seg_11729" s="T694">Am Abend sitzen sie für einige Zeit.</ta>
            <ta e="T699" id="Seg_11730" s="T697">Es fing an zu dunkeln.</ta>
            <ta e="T703" id="Seg_11731" s="T699">Sie schicken einen Menschen nach draußen: "Lausche!"</ta>
            <ta e="T706" id="Seg_11732" s="T703">Der Mensch steht draußen.</ta>
            <ta e="T707" id="Seg_11733" s="T706">"Oh!</ta>
            <ta e="T710" id="Seg_11734" s="T707">Ein Mensch taucht auf!"</ta>
            <ta e="T712" id="Seg_11735" s="T710">Er rannte ins Haus.</ta>
            <ta e="T719" id="Seg_11736" s="T712">Er kam ins Haus und sagte: "Der Teufel erscheint von unten.</ta>
            <ta e="T723" id="Seg_11737" s="T719">Er geht auf das Ufer."</ta>
            <ta e="T726" id="Seg_11738" s="T723">"Wer kommt aufs Ufer?</ta>
            <ta e="T727" id="Seg_11739" s="T726">Ah!</ta>
            <ta e="T732" id="Seg_11740" s="T727">Bist du betäubt, hast du dich überfressen, hast du Angst?"</ta>
            <ta e="T736" id="Seg_11741" s="T732">Einen sah [der Teufel].</ta>
            <ta e="T741" id="Seg_11742" s="T736">"Früher legte ich mich immer hin, wenn ich gegessen hatte."</ta>
            <ta e="T746" id="Seg_11743" s="T741">Dann kam er gehend hinauf.</ta>
            <ta e="T750" id="Seg_11744" s="T746">Er kam gehend hinauf.</ta>
            <ta e="T752" id="Seg_11745" s="T750">"Zwitscher-zwitscher.</ta>
            <ta e="T757" id="Seg_11746" s="T752">Wo ist Syltscha-Paltscha Kesch hingegangen?</ta>
            <ta e="T761" id="Seg_11747" s="T757">Ist er weggegangen oder wo ist er hin?"</ta>
            <ta e="T769" id="Seg_11748" s="T761">"Syltscha-Paltscha ist früher weggegangen, er ging dann weg.</ta>
            <ta e="T774" id="Seg_11749" s="T769">Geh aufs Ufer und iss etwas."</ta>
            <ta e="T781" id="Seg_11750" s="T774">Dann ging er aufs Ufer und kam zu dem Götzenbild.</ta>
            <ta e="T787" id="Seg_11751" s="T781">Er sagte: "Ist Syltscha-Paltscha zuhause?"</ta>
            <ta e="T792" id="Seg_11752" s="T787">"Er ist gerade weggegangen.</ta>
            <ta e="T796" id="Seg_11753" s="T792">Komm ins Haus, iss etwas."</ta>
            <ta e="T809" id="Seg_11754" s="T796">Jener andere sagt so: "Gehe ich ins Haus hinein oder nicht", der andere [sagte] auch etwas.</ta>
            <ta e="T814" id="Seg_11755" s="T809">"Götzenbild, bringst du mich durcheinander?"</ta>
            <ta e="T823" id="Seg_11756" s="T814">Als ob er zögert, ob er ins Haus hineingehen soll oder nicht.</ta>
            <ta e="T827" id="Seg_11757" s="T823">Auf einmal ging er hinein.</ta>
            <ta e="T836" id="Seg_11758" s="T827">Er hatte gerade seinen Kopf ins Haus gesteckt, da hackte Syltscha-Paltscha ihm den Kopf ab.</ta>
            <ta e="T840" id="Seg_11759" s="T836">[Der Kopf] rollte weiter ins Haus.</ta>
            <ta e="T845" id="Seg_11760" s="T840">Der Körper fiel zurück nach draußen.</ta>
            <ta e="T846" id="Seg_11761" s="T845">Er hatte ihn getötet.</ta>
            <ta e="T854" id="Seg_11762" s="T846">Dann sitzen und sitzen sie, niemand ist dort.</ta>
            <ta e="T858" id="Seg_11763" s="T854">Gehen zwei dort?</ta>
            <ta e="T862" id="Seg_11764" s="T858">Am nächsten Morgen tagt es.</ta>
            <ta e="T865" id="Seg_11765" s="T862">"Hackt Feuerholz", sagt er.</ta>
            <ta e="T867" id="Seg_11766" s="T865">Sie hacken Feuerholz.</ta>
            <ta e="T869" id="Seg_11767" s="T867">"Macht ein Feuer."</ta>
            <ta e="T872" id="Seg_11768" s="T869">Und wirklich, sie hacken Feuerholz.</ta>
            <ta e="T880" id="Seg_11769" s="T872">Sie brachten beide hinunter, sie verbrannten sie im Feuer, sie verbrannten sie völlig im Feuer.</ta>
            <ta e="T886" id="Seg_11770" s="T880">Dort war ein alter Mann mit seinem Vater und seinen Kindern, dieser alte Mann hatte eine Tochter.</ta>
            <ta e="T891" id="Seg_11771" s="T886">Diese Tochter gab er Syltscha-Paltscha Kesch.</ta>
            <ta e="T897" id="Seg_11772" s="T891">Er ging um die Erde herum und kam zu den Selkupen.</ta>
            <ta e="T900" id="Seg_11773" s="T897">Das ist das Ende.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_11774" s="T1">мышь [большой] жил</ta>
            <ta e="T6" id="Seg_11775" s="T4">мать была</ta>
            <ta e="T8" id="Seg_11776" s="T6">отца не было</ta>
            <ta e="T10" id="Seg_11777" s="T8">жил жил</ta>
            <ta e="T24" id="Seg_11778" s="T10">однажды матери так сказал я человека искать пойду</ta>
            <ta e="T27" id="Seg_11779" s="T24">как так жить</ta>
            <ta e="T37" id="Seg_11780" s="T27">мама ты неужели (большая земля) ты не знаешь какого-либо человека</ta>
            <ta e="T45" id="Seg_11781" s="T37">мать говорит что я никого не знаю</ta>
            <ta e="T51" id="Seg_11782" s="T45">на этой стороне (земле) ближе человека нет</ta>
            <ta e="T59" id="Seg_11783" s="T51">сын матери так сказал ты одна живи </ta>
            <ta e="T64" id="Seg_11784" s="T59">я сейчас человека искать пойду</ta>
            <ta e="T71" id="Seg_11785" s="T64">по (в) этой стороне теперь пошел</ta>
            <ta e="T75" id="Seg_11786" s="T71">столько ехал (он)</ta>
            <ta e="T78" id="Seg_11787" s="T75">то зима настанет, то лето настанет</ta>
            <ta e="T84" id="Seg_11788" s="T78">Таз с верхушки до устья пересекал и маленькие речушки пересекал</ta>
            <ta e="T89" id="Seg_11789" s="T84">никакого человека не было</ta>
            <ta e="T94" id="Seg_11790" s="T89">по озерам ехал</ta>
            <ta e="T99" id="Seg_11791" s="T94">шапка на лбу заиндевела то расстанется</ta>
            <ta e="T105" id="Seg_11792" s="T99">однажды дошел до леса большой лес</ta>
            <ta e="T109" id="Seg_11793" s="T105">по этому лесу река текла (проходила)</ta>
            <ta e="T113" id="Seg_11794" s="T109">по этой реке идут</ta>
            <ta e="T122" id="Seg_11795" s="T113">что-то что-то слышно</ta>
            <ta e="T126" id="Seg_11796" s="T122">идет такой голос услышал</ta>
            <ta e="T132" id="Seg_11797" s="T126">никакого зверя ничего нет</ta>
            <ta e="T139" id="Seg_11798" s="T132">это пространство (сторону) обошел ну что такое </ta>
            <ta e="T143" id="Seg_11799" s="T139">я хоть бы посмотрел</ta>
            <ta e="T147" id="Seg_11800" s="T143">идет идет</ta>
            <ta e="T162" id="Seg_11801" s="T147">будто бы по реке внизу слышно опять [второй раз] кричит</ta>
            <ta e="T167" id="Seg_11802" s="T162">такой слух [голос, эхо] отдается</ta>
            <ta e="T169" id="Seg_11803" s="T167">идет (подходит) </ta>
            <ta e="T175" id="Seg_11804" s="T169">внизу посередине реки что-то чернеет</ta>
            <ta e="T189" id="Seg_11805" s="T182">этот опять так кричит не человеческий голос только так кричит</ta>
            <ta e="T209" id="Seg_11806" s="T196">сюда подошел увидел что человек там мать отец голые голый человек</ta>
            <ta e="T218" id="Seg_11807" s="T209">тот отца матери голый сидит на реки проруби </ta>
            <ta e="T223" id="Seg_11808" s="T218">это человек кричал</ta>
            <ta e="T227" id="Seg_11809" s="T223">ты почему так сидишь?</ta>
            <ta e="T234" id="Seg_11810" s="T227">нет я сижу черт меня чтобы скушал сижу</ta>
            <ta e="T237" id="Seg_11811" s="T234">там в лесу люди есть</ta>
            <ta e="T243" id="Seg_11812" s="T237">они меня посадили черту съесть</ta>
            <ta e="T255" id="Seg_11813" s="T243">если я выйду на берег черт всех нас съест никто не останется</ta>
            <ta e="T259" id="Seg_11814" s="T255">зато [поэтому] посадили меня</ta>
            <ta e="T262" id="Seg_11815" s="T259">ты иди туда</ta>
            <ta e="T266" id="Seg_11816" s="T262">ты тоже иди (голому человеку)</ta>
            <ta e="T269" id="Seg_11817" s="T266">я как выйду</ta>
            <ta e="T272" id="Seg_11818" s="T269">люди меня увидят</ta>
            <ta e="T277" id="Seg_11819" s="T272">я говорю ты выходи</ta>
            <ta e="T280" id="Seg_11820" s="T277">на берег вышли</ta>
            <ta e="T286" id="Seg_11821" s="T280">человечье лицо сделал (вырезал) на дереве большим ножом</ta>
            <ta e="T293" id="Seg_11822" s="T286">ты смотри меня нисколько не говори</ta>
            <ta e="T301" id="Seg_11823" s="T293">черт тебя будет спрашивать С.-П. куда девался</ta>
            <ta e="T305" id="Seg_11824" s="T301">меня нисколько не говори</ta>
            <ta e="T308" id="Seg_11825" s="T305">так погрозил секирой [кинжалом]</ta>
            <ta e="T315" id="Seg_11826" s="T308">туда пошел в землянку</ta>
            <ta e="T322" id="Seg_11827" s="T315">там он тоже такое лицо сделал</ta>
            <ta e="T324" id="Seg_11828" s="T322">заходи </ta>
            <ta e="T327" id="Seg_11829" s="T324">зачем стоишь замерзнешь</ta>
            <ta e="T333" id="Seg_11830" s="T327">я как зайду мои люди меня увидят</ta>
            <ta e="T337" id="Seg_11831" s="T333">ты просто домой заходи</ta>
            <ta e="T343" id="Seg_11832" s="T337">на этой двери тоже человеческое лицо сделал</ta>
            <ta e="T349" id="Seg_11833" s="T343">ты смотри (про) меня не [никогда, нисколько] говори</ta>
            <ta e="T351" id="Seg_11834" s="T349">секирой погрозил</ta>
            <ta e="T357" id="Seg_11835" s="T351">в дом вошел этот человек все же</ta>
            <ta e="T361" id="Seg_11836" s="T357">все увидели</ta>
            <ta e="T364" id="Seg_11837" s="T361">зачем зашел</ta>
            <ta e="T370" id="Seg_11838" s="T364">нет меня человек домой отправил</ta>
            <ta e="T378" id="Seg_11839" s="T370">какой-то человек пришел меня домой отправил</ta>
            <ta e="T381" id="Seg_11840" s="T378">он меня привел</ta>
            <ta e="T392" id="Seg_11841" s="T381">он увидел двери с верхней стороны еще один голый человек сидит</ta>
            <ta e="T399" id="Seg_11842" s="T392">потом так стал [стало] (говорить) человека оденьте</ta>
            <ta e="T407" id="Seg_11843" s="T399">потом выходит подслушивает какое время черт из-под воды поднимется (всплывет)</ta>
            <ta e="T412" id="Seg_11844" s="T407">черт если выйдет тогда в дом заходите</ta>
            <ta e="T415" id="Seg_11845" s="T412">один человек пусть подслушивает</ta>
            <ta e="T422" id="Seg_11846" s="T415">говорят что когда вечерняя заря заходит (в полночь, подходит) выйдет тогда выходит</ta>
            <ta e="T430" id="Seg_11847" s="T422">немного погодя в одно время человек вышел будто бы там появляется (поднимается из-под воды) [всплывает] в дом забежал [вбежал]</ta>
            <ta e="T434" id="Seg_11848" s="T430">будто бы там появляется (поднимается из-под воды) [всплывает] </ta>
            <ta e="T436" id="Seg_11849" s="T434">в дом забежал [вбежал]</ta>
            <ta e="T439" id="Seg_11850" s="T436">черт уже выходит</ta>
            <ta e="T448" id="Seg_11851" s="T439">ну ладно сидите спокойно никогда не выходите</ta>
            <ta e="T452" id="Seg_11852" s="T448">подслушивает</ta>
            <ta e="T456" id="Seg_11853" s="T452">на берег вышел он</ta>
            <ta e="T458" id="Seg_11854" s="T456">бурундука увидал</ta>
            <ta e="T461" id="Seg_11855" s="T458">буруна</ta>
            <ta e="T465" id="Seg_11856" s="T461">С. П. К. куда делся</ta>
            <ta e="T469" id="Seg_11857" s="T465">давно ушел</ta>
            <ta e="T474" id="Seg_11858" s="T469">на берег пришел лицо [нарисованное] увидел [нашел]</ta>
            <ta e="T480" id="Seg_11859" s="T474">лицо [нарисованное] сказало С. П. К. дома сидит</ta>
            <ta e="T487" id="Seg_11860" s="T480">на бережную сторону повернулся убежал пошел в воду упал</ta>
            <ta e="T490" id="Seg_11861" s="T487">звук раздался [плеск] послышался</ta>
            <ta e="T500" id="Seg_11862" s="T490">на улицу выбежавши лицо нарисованное срубив все раскрошил на сторону побросал</ta>
            <ta e="T503" id="Seg_11863" s="T500">другое лицо сделал</ta>
            <ta e="T513" id="Seg_11864" s="T503">лицо [его] погрозило: ты смотри про меня никогда не говори</ta>
            <ta e="T517" id="Seg_11865" s="T513">теперь он выходит утренней ночью (на рассвете)</ta>
            <ta e="T522" id="Seg_11866" s="T517">дальше всю ночь сидят</ta>
            <ta e="T531" id="Seg_11867" s="T522">на заре человека попросили (заставили) на улицу посмотрите не вышел ли?</ta>
            <ta e="T535" id="Seg_11868" s="T531">сылшу пылшу заставили послушай</ta>
            <ta e="T543" id="Seg_11869" s="T535">недолго [некоторое время спустя] человек тихо побыл в дом забежал </ta>
            <ta e="T547" id="Seg_11870" s="T543">будто бы один черт выскочил [вылез]</ta>
            <ta e="T554" id="Seg_11871" s="T550">ну он сам слышит</ta>
            <ta e="T557" id="Seg_11872" s="T554">вот он чувствует идет</ta>
            <ta e="T563" id="Seg_11873" s="T557">шагающе идет шагая идет</ta>
            <ta e="T568" id="Seg_11874" s="T563">на это гору (невысокую) [возвышенность] поднялся</ta>
            <ta e="T573" id="Seg_11875" s="T568">к этого [того] черта лицу шагая подошел</ta>
            <ta e="T577" id="Seg_11876" s="T573">целовал: чок, чок, чок</ta>
            <ta e="T584" id="Seg_11877" s="T577">куда делся, дома ли?</ta>
            <ta e="T588" id="Seg_11878" s="T584">вчера вечером [тогда] ушел</ta>
            <ta e="T593" id="Seg_11879" s="T588">ты туда (немного повыше) иди кушанье [еду] ешь</ta>
            <ta e="T602" id="Seg_11880" s="T593">туда шагая подошел у двери лица подошел</ta>
            <ta e="T603" id="Seg_11881" s="T602">куда делся?</ta>
            <ta e="T611" id="Seg_11882" s="T603">он же ведь вчера тогда ушел</ta>
            <ta e="T615" id="Seg_11883" s="T611">в дом зайди кушанье ешь</ta>
            <ta e="T624" id="Seg_11884" s="T615">одно время лозы в дом стал заходить [сам себя толкает в дом] (медленно, плавно) в дом втолкнулся</ta>
            <ta e="T633" id="Seg_11885" s="T624">в дом только что зашел черт срубил (топором) голову шарик [кругляшок] [беспомощную] </ta>
            <ta e="T637" id="Seg_11886" s="T633">по избе туда покатилась [прокатилась]</ta>
            <ta e="T642" id="Seg_11887" s="T637">туловище шара обратно на улицу упало</ta>
            <ta e="T643" id="Seg_11888" s="T642">убил</ta>
            <ta e="T650" id="Seg_11889" s="T643">на улицу вынес С. П. К. туловище шара</ta>
            <ta e="T654" id="Seg_11890" s="T650">на улицу вышел уже посветлело</ta>
            <ta e="T663" id="Seg_11891" s="T654">под угор внес туловище шар в прорубь положил</ta>
            <ta e="T668" id="Seg_11892" s="T663">со льдом вместе заморозил так положил</ta>
            <ta e="T673" id="Seg_11893" s="T668">около проруби так положил</ta>
            <ta e="T677" id="Seg_11894" s="T673">будто бы живой лежит </ta>
            <ta e="T680" id="Seg_11895" s="T677">с головой вместе прилепил (заморозил)</ta>
            <ta e="T682" id="Seg_11896" s="T680">этот день проденевали</ta>
            <ta e="T683" id="Seg_11897" s="T682">стемнело</ta>
            <ta e="T691" id="Seg_11898" s="T683">будто [говорят] только двое другой ходить не знаем</ta>
            <ta e="T694" id="Seg_11899" s="T691">вечером стало темнеть </ta>
            <ta e="T697" id="Seg_11900" s="T694">вечером немного сидит </ta>
            <ta e="T699" id="Seg_11901" s="T697">стало темнеть</ta>
            <ta e="T703" id="Seg_11902" s="T699">человека на улицу послал подслушивай(ся)!</ta>
            <ta e="T706" id="Seg_11903" s="T703">человек на улице стоит</ta>
            <ta e="T707" id="Seg_11904" s="T706">ой [удивление] </ta>
            <ta e="T710" id="Seg_11905" s="T707">человек стал всплывать [подниматься из-под воды]</ta>
            <ta e="T712" id="Seg_11906" s="T710">в дом забежал </ta>
            <ta e="T719" id="Seg_11907" s="T712">в дом зайдя так сказал черт там всплывает </ta>
            <ta e="T723" id="Seg_11908" s="T719">вверх шагая вышел (из-под горы)</ta>
            <ta e="T727" id="Seg_11909" s="T726">а</ta>
            <ta e="T732" id="Seg_11910" s="T727">ты что окоченел что накушался? как палка окоченевший идет</ta>
            <ta e="T736" id="Seg_11911" s="T732">одного из них так нашел</ta>
            <ta e="T741" id="Seg_11912" s="T736">я раньше покушавши так полеживал потом шагающе пришел</ta>
            <ta e="T746" id="Seg_11913" s="T741">потом шагающе пришел</ta>
            <ta e="T750" id="Seg_11914" s="T746">шагающе пришел</ta>
            <ta e="T757" id="Seg_11915" s="T752">что С. П. К. куда делся </ta>
            <ta e="T761" id="Seg_11916" s="T757">что ушел или куда делся</ta>
            <ta e="T769" id="Seg_11917" s="T761">раньше тогда ушел давно тогда ушел </ta>
            <ta e="T774" id="Seg_11918" s="T769">ты на гору иди кушанье кушай</ta>
            <ta e="T781" id="Seg_11919" s="T774">потом вверх идя [шагая] идет того (который раньше был) нарисованного [лица] пришел</ta>
            <ta e="T787" id="Seg_11920" s="T781">сказал что С.П. дома есть?</ta>
            <ta e="T792" id="Seg_11921" s="T787">он же тогда давно ушел в дом зайди кушанье кушай</ta>
            <ta e="T796" id="Seg_11922" s="T792">в дом зайди кушанье кушай</ta>
            <ta e="T809" id="Seg_11923" s="T796">колеблется так было в дом зайти или не зайти колеблясь</ta>
            <ta e="T814" id="Seg_11924" s="T809">лицо что будто бы меня путаешь что ли</ta>
            <ta e="T823" id="Seg_11925" s="T814">догадывается что ли колеблется зайти [зайдет] ли что или нет [не зайдет]</ta>
            <ta e="T827" id="Seg_11926" s="T823">в одно время начал заходить </ta>
            <ta e="T836" id="Seg_11927" s="T827">голову в дом хотел затолкать [просунуть] срубил голову шарик </ta>
            <ta e="T840" id="Seg_11928" s="T836">в дом вперед упала покатилась</ta>
            <ta e="T845" id="Seg_11929" s="T840">туловище обратно на улице упало</ta>
            <ta e="T846" id="Seg_11930" s="T845">убил</ta>
            <ta e="T854" id="Seg_11931" s="T846">потом сидят сидят ничего нет </ta>
            <ta e="T858" id="Seg_11932" s="T854">что двое ходят?</ta>
            <ta e="T862" id="Seg_11933" s="T858">на второе утро рассвело</ta>
            <ta e="T865" id="Seg_11934" s="T862">говорят что дрова нарубите</ta>
            <ta e="T867" id="Seg_11935" s="T865">дрова рубят</ta>
            <ta e="T869" id="Seg_11936" s="T867">огонь [костер] разожгите [зажгите]</ta>
            <ta e="T872" id="Seg_11937" s="T869">правда что дрова нарубили</ta>
            <ta e="T880" id="Seg_11938" s="T872">двоих на реку [под угор] понесли огнем сожгли совсем огнем сожгли</ta>
            <ta e="T886" id="Seg_11939" s="T880">старик с сыном тот старик дочь (его) была</ta>
            <ta e="T891" id="Seg_11940" s="T886">ему отдал</ta>
            <ta e="T897" id="Seg_11941" s="T891">на это пространство [весь свет] обошел [вокруг света обошел] пришел к селькупам</ta>
            <ta e="T900" id="Seg_11942" s="T897">конец сказки</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T1" id="Seg_11943" s="T0">[OSV]: The name Sɨlʼčʼa-Pɨlʼčʼa can be interpreted in different ways. It can be an onomatopoeia which resembles the birds singing: see 'ilʼčʼa-pilʼčʼa' for indicating the ousel's cry in Taz dialect, the verbs 'sılʼɨmpɨqo' - 'to ring', 'sılʼqɨmpɨqo' - 'to squeak', 'pılʼqɨmpɨqo' - 'to ring, to squeak'. According to another interpretation, the name Sɨlʼčʼa-Pɨlʼčʼa goes to verbs 'pɨlʼčʼɨqo' - 'to mature, to ripen' and 'sılʼtɨqo' - 'be noisy', so that it could mean "a naughty grown-up".</ta>
            <ta e="T24" id="Seg_11944" s="T10">[OSV]: The nominal form "čʼontotɨ" has been edited into "čʼontot".</ta>
            <ta e="T243" id="Seg_11945" s="T237"> [OSV]: The verbal form "omtɨ-ltɨ-mpɔː-tet" (sit.down-TR-PST.NAR-3PL) would be more correct here.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
