<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID07C2500A-6CE6-37EC-9176-B89802C2430B">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">flk\KAI_1965_OldManWithLittleMind1_flk\KAI_1965_OldManWithLittleMind1_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">546</ud-information>
            <ud-information attribute-name="# HIAT:w">404</ud-information>
            <ud-information attribute-name="# e">399</ud-information>
            <ud-information attribute-name="# HIAT:u">73</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KAI">
            <abbreviation>KAI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
         <tli id="T318" />
         <tli id="T319" />
         <tli id="T320" />
         <tli id="T321" />
         <tli id="T322" />
         <tli id="T323" />
         <tli id="T324" />
         <tli id="T325" />
         <tli id="T326" />
         <tli id="T327" />
         <tli id="T328" />
         <tli id="T329" />
         <tli id="T330" />
         <tli id="T331" />
         <tli id="T332" />
         <tli id="T333" />
         <tli id="T334" />
         <tli id="T335" />
         <tli id="T336" />
         <tli id="T337" />
         <tli id="T338" />
         <tli id="T339" />
         <tli id="T340" />
         <tli id="T341" />
         <tli id="T342" />
         <tli id="T343" />
         <tli id="T344" />
         <tli id="T345" />
         <tli id="T346" />
         <tli id="T347" />
         <tli id="T348" />
         <tli id="T349" />
         <tli id="T350" />
         <tli id="T351" />
         <tli id="T352" />
         <tli id="T353" />
         <tli id="T354" />
         <tli id="T355" />
         <tli id="T356" />
         <tli id="T357" />
         <tli id="T358" />
         <tli id="T359" />
         <tli id="T360" />
         <tli id="T361" />
         <tli id="T362" />
         <tli id="T363" />
         <tli id="T364" />
         <tli id="T365" />
         <tli id="T366" />
         <tli id="T367" />
         <tli id="T368" />
         <tli id="T369" />
         <tli id="T370" />
         <tli id="T371" />
         <tli id="T372" />
         <tli id="T373" />
         <tli id="T374" />
         <tli id="T375" />
         <tli id="T376" />
         <tli id="T377" />
         <tli id="T378" />
         <tli id="T379" />
         <tli id="T380" />
         <tli id="T381" />
         <tli id="T382" />
         <tli id="T383" />
         <tli id="T384" />
         <tli id="T385" />
         <tli id="T386" />
         <tli id="T387" />
         <tli id="T388" />
         <tli id="T389" />
         <tli id="T390" />
         <tli id="T391" />
         <tli id="T392" />
         <tli id="T393" />
         <tli id="T394" />
         <tli id="T395" />
         <tli id="T396" />
         <tli id="T397" />
         <tli id="T398" />
         <tli id="T399" />
         <tli id="T400" />
         <tli id="T401" />
         <tli id="T402" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KAI"
                      type="t">
         <timeline-fork end="T79" start="T78">
            <tli id="T78.tx.1" />
         </timeline-fork>
         <timeline-fork end="T80" start="T79">
            <tli id="T79.tx.1" />
         </timeline-fork>
         <timeline-fork end="T402" start="T398">
            <tli id="T398.tx.1" />
            <tli id="T398.tx.2" />
            <tli id="T398.tx.3" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T402" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Istarik</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">pervɨj</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">voda</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">zamerznet</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Qatamol</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">ütɨp</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">ammɛːjit</ts>
                  <nts id="Seg_26" n="HIAT:ip">,</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">ira</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">koralʼä</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_34" n="HIAT:ip">(</nts>
                  <nts id="Seg_35" n="HIAT:ip">/</nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">qəːlɨlʼlʼä</ts>
                  <nts id="Seg_38" n="HIAT:ip">)</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">qənpa</ts>
                  <nts id="Seg_42" n="HIAT:ip">.</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_45" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_47" n="HIAT:w" s="T11">Qoltɨp</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">kušat</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">orqälɔːlnɨt</ts>
                  <nts id="Seg_54" n="HIAT:ip">,</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">irra</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">qoltit</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">qanɨŋmɨt</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">nʼanna</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">mantelɔːlpɨlʼä</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">kurɨna</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">suːrɨtqo</ts>
                  <nts id="Seg_76" n="HIAT:ip">,</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">tä</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">toː</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">mantalpelʼa</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">qəəə</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_92" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">Ukkur</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">čʼontoːqɨt</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">täp</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_103" n="HIAT:w" s="T28">qos</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_106" n="HIAT:w" s="T29">qaj</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_109" n="HIAT:w" s="T30">orɨčʼčʼa</ts>
                  <nts id="Seg_110" n="HIAT:ip">.</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_113" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">Qaj</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_118" n="HIAT:w" s="T32">qup</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_121" n="HIAT:w" s="T33">laŋkɨčʼčʼa</ts>
                  <nts id="Seg_122" n="HIAT:ip">,</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">qaj</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">qaj</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">orɨčʼčʼa</ts>
                  <nts id="Seg_132" n="HIAT:ip">.</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_135" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">Täp</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_140" n="HIAT:w" s="T38">qaj</ts>
                  <nts id="Seg_141" n="HIAT:ip">,</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_144" n="HIAT:w" s="T39">qup</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_147" n="HIAT:w" s="T40">lʼi</ts>
                  <nts id="Seg_148" n="HIAT:ip">,</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_151" n="HIAT:w" s="T41">qaj</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_154" n="HIAT:w" s="T42">laŋkɨčʼčʼa</ts>
                  <nts id="Seg_155" n="HIAT:ip">?</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_158" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_160" n="HIAT:w" s="T43">Ira</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_163" n="HIAT:w" s="T44">nık</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_166" n="HIAT:w" s="T45">na</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_169" n="HIAT:w" s="T46">üntɨčʼit</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_173" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_175" n="HIAT:w" s="T47">Nɨːna</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_178" n="HIAT:w" s="T48">tünta</ts>
                  <nts id="Seg_179" n="HIAT:ip">.</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_182" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_184" n="HIAT:w" s="T49">Qəəə</ts>
                  <nts id="Seg_185" n="HIAT:ip">!</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_188" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_190" n="HIAT:w" s="T50">Točʼčʼo</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_193" n="HIAT:w" s="T51">nʼempoːqɨt</ts>
                  <nts id="Seg_194" n="HIAT:ip">,</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_197" n="HIAT:w" s="T52">qajel</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_200" n="HIAT:w" s="T53">laka</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_203" n="HIAT:w" s="T54">ɛːsʼa</ts>
                  <nts id="Seg_204" n="HIAT:ip">?</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_207" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_209" n="HIAT:w" s="T55">Nempoːqɨt</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_212" n="HIAT:w" s="T56">mɨta</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_215" n="HIAT:w" s="T57">qajilʼ</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_218" n="HIAT:w" s="T58">laka</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_221" n="HIAT:w" s="T59">qontɨlʼčʼa</ts>
                  <nts id="Seg_222" n="HIAT:ip">.</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_225" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_227" n="HIAT:w" s="T60">İnnä</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_230" n="HIAT:w" s="T61">čʼap</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_233" n="HIAT:w" s="T62">kɛ</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_236" n="HIAT:w" s="T63">qatɨtɔːlka</ts>
                  <nts id="Seg_237" n="HIAT:ip">,</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_240" n="HIAT:w" s="T64">aj</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_243" n="HIAT:w" s="T65">ılla</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_246" n="HIAT:w" s="T66">qənnɛːja</ts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_250" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_252" n="HIAT:w" s="T67">Aj</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_255" n="HIAT:w" s="T68">ılla</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_258" n="HIAT:w" s="T69">qənnɛːja</ts>
                  <nts id="Seg_259" n="HIAT:ip">.</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_262" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_264" n="HIAT:w" s="T70">Načʼä</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_267" n="HIAT:w" s="T71">čʼap</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_270" n="HIAT:w" s="T72">qənna</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_273" n="HIAT:w" s="T73">montɨ</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_276" n="HIAT:w" s="T74">qup</ts>
                  <nts id="Seg_277" n="HIAT:ip">,</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_280" n="HIAT:w" s="T75">üttɨ</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_283" n="HIAT:w" s="T76">patqɨlpa</ts>
                  <nts id="Seg_284" n="HIAT:ip">,</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_287" n="HIAT:w" s="T77">nʼempontɨ</ts>
                  <nts id="Seg_288" n="HIAT:ip">.</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_291" n="HIAT:u" s="T78">
                  <ts e="T78.tx.1" id="Seg_293" n="HIAT:w" s="T78">Pošalusta</ts>
                  <nts id="Seg_294" n="HIAT:ip">,</nts>
                  <ts e="T79" id="Seg_296" n="HIAT:w" s="T78.tx.1">—</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79.tx.1" id="Seg_299" n="HIAT:w" s="T79">lʼaŋɨnʼnʼa</ts>
                  <nts id="Seg_300" n="HIAT:ip">,</nts>
                  <ts e="T80" id="Seg_302" n="HIAT:w" s="T79.tx.1">—</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_305" n="HIAT:w" s="T80">qɔːtä</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_308" n="HIAT:w" s="T81">ınnä</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_311" n="HIAT:w" s="T82">šık</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_314" n="HIAT:w" s="T83">iːjaš</ts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_318" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_320" n="HIAT:w" s="T84">Timte</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_323" n="HIAT:w" s="T85">na</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_326" n="HIAT:w" s="T86">irra</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_329" n="HIAT:w" s="T87">konna</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_332" n="HIAT:w" s="T88">pakta</ts>
                  <nts id="Seg_333" n="HIAT:ip">.</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_336" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_338" n="HIAT:w" s="T89">Čʼumpɨ</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_341" n="HIAT:w" s="T90">nʼarqɨp</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_344" n="HIAT:w" s="T91">iːŋɨtɨ</ts>
                  <nts id="Seg_345" n="HIAT:ip">.</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_348" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_350" n="HIAT:w" s="T92">Čʼumpɨ</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_353" n="HIAT:w" s="T93">nʼarqɨp</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_356" n="HIAT:w" s="T94">pačʼčʼaltɨt</ts>
                  <nts id="Seg_357" n="HIAT:ip">.</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_360" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_362" n="HIAT:w" s="T95">Karrä</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_365" n="HIAT:w" s="T96">iːttɨtɨ</ts>
                  <nts id="Seg_366" n="HIAT:ip">(</nts>
                  <nts id="Seg_367" n="HIAT:ip">/</nts>
                  <ts e="T98" id="Seg_369" n="HIAT:w" s="T97">iːttɛːtɨ</ts>
                  <nts id="Seg_370" n="HIAT:ip">,</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_373" n="HIAT:w" s="T98">čʼattɛːtɨ</ts>
                  <nts id="Seg_374" n="HIAT:ip">)</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_377" n="HIAT:w" s="T99">irätkine</ts>
                  <nts id="Seg_378" n="HIAT:ip">.</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_381" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_383" n="HIAT:w" s="T100">Nʼa</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_386" n="HIAT:w" s="T101">nʼarqɨsä</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_389" n="HIAT:w" s="T102">konnä</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_392" n="HIAT:w" s="T103">ınna</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_395" n="HIAT:w" s="T104">üːqɨlʼlʼɛːŋɨta</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_399" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_401" n="HIAT:w" s="T105">Konna</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_404" n="HIAT:w" s="T106">čʼap</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_407" n="HIAT:w" s="T107">üːqɨlʼlʼɛːŋat</ts>
                  <nts id="Seg_408" n="HIAT:ip">.</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_411" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_413" n="HIAT:w" s="T108">Qo</ts>
                  <nts id="Seg_414" n="HIAT:ip">,</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_417" n="HIAT:w" s="T109">qo</ts>
                  <nts id="Seg_418" n="HIAT:ip">!</nts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_421" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_423" n="HIAT:w" s="T110">Monte</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_426" n="HIAT:w" s="T111">temqup</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_428" n="HIAT:ip">(</nts>
                  <nts id="Seg_429" n="HIAT:ip">/</nts>
                  <ts e="T113" id="Seg_431" n="HIAT:w" s="T112">kupec</ts>
                  <nts id="Seg_432" n="HIAT:ip">)</nts>
                  <nts id="Seg_433" n="HIAT:ip">,</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_436" n="HIAT:w" s="T113">kupec</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_439" n="HIAT:w" s="T114">na</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_442" n="HIAT:w" s="T115">tətta</ts>
                  <nts id="Seg_443" n="HIAT:ip">.</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_446" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_448" n="HIAT:w" s="T116">Tat</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_451" n="HIAT:w" s="T117">mašıp</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_454" n="HIAT:w" s="T118">ınnä</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_457" n="HIAT:w" s="T119">šık</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_460" n="HIAT:w" s="T120">iːŋantɨ</ts>
                  <nts id="Seg_461" n="HIAT:ip">,</nts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_464" n="HIAT:w" s="T121">qurmon</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_467" n="HIAT:w" s="T122">nɔːnɨ</ts>
                  <nts id="Seg_468" n="HIAT:ip">,</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_471" n="HIAT:w" s="T123">ınnä</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_474" n="HIAT:w" s="T124">šı</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_477" n="HIAT:w" s="T125">iːŋantɨ</ts>
                  <nts id="Seg_478" n="HIAT:ip">.</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_481" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_483" n="HIAT:w" s="T126">İnnä</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_486" n="HIAT:w" s="T127">šı</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_489" n="HIAT:w" s="T128">iːŋantɨ</ts>
                  <nts id="Seg_490" n="HIAT:ip">.</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_493" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_495" n="HIAT:w" s="T129">Solotalʼ</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_498" n="HIAT:w" s="T130">kusoksä</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_501" n="HIAT:w" s="T131">miːŋatɨ</ts>
                  <nts id="Seg_502" n="HIAT:ip">.</nts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_505" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_507" n="HIAT:w" s="T132">Mitɨ</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_510" n="HIAT:w" s="T133">qurmon</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_513" n="HIAT:w" s="T134">nɔːnɨ</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_516" n="HIAT:w" s="T135">ınnä</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_519" n="HIAT:w" s="T136">šıŋ</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_522" n="HIAT:w" s="T137">iːŋäntɨ</ts>
                  <nts id="Seg_523" n="HIAT:ip">.</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_526" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_528" n="HIAT:w" s="T138">Qaj</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_531" n="HIAT:w" s="T139">iːtal</ts>
                  <nts id="Seg_532" n="HIAT:ip">,</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_535" n="HIAT:w" s="T140">qaj</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_538" n="HIAT:w" s="T141">iltäːlʼ</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_541" n="HIAT:w" s="T142">iːtal</ts>
                  <nts id="Seg_542" n="HIAT:ip">?</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_545" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_547" n="HIAT:w" s="T143">Na</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_550" n="HIAT:w" s="T144">qəntanəəə</ts>
                  <nts id="Seg_551" n="HIAT:ip">.</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_554" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_556" n="HIAT:w" s="T145">Lʼa</ts>
                  <nts id="Seg_557" n="HIAT:ip">,</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_560" n="HIAT:w" s="T146">čʼuntɨ</ts>
                  <nts id="Seg_561" n="HIAT:ip">,</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_564" n="HIAT:w" s="T147">ukkur</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_567" n="HIAT:w" s="T148">ruš</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_570" n="HIAT:w" s="T149">na</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_573" n="HIAT:w" s="T150">montɨ</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_576" n="HIAT:w" s="T151">čʼuntɨp</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_579" n="HIAT:w" s="T152">tɔːqqɨmpatə</ts>
                  <nts id="Seg_580" n="HIAT:ip">,</nts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_583" n="HIAT:w" s="T153">moːrətɨ</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_586" n="HIAT:w" s="T154">aša</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_589" n="HIAT:w" s="T155">ata</ts>
                  <nts id="Seg_590" n="HIAT:ip">.</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_593" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_595" n="HIAT:w" s="T156">Nɨːna</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_598" n="HIAT:w" s="T157">tünta</ts>
                  <nts id="Seg_599" n="HIAT:ip">.</nts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_602" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_604" n="HIAT:w" s="T158">Lʼa</ts>
                  <nts id="Seg_605" n="HIAT:ip">,</nts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_608" n="HIAT:w" s="T159">tɛnätɨ</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_611" n="HIAT:w" s="T160">nık</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_614" n="HIAT:w" s="T161">kollimɔːnna</ts>
                  <nts id="Seg_615" n="HIAT:ip">.</nts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_618" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_620" n="HIAT:w" s="T162">Qɔːtɨ</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_623" n="HIAT:w" s="T163">mompa</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_626" n="HIAT:w" s="T164">čʼuntɨp</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_629" n="HIAT:w" s="T165">iːsam</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_632" n="HIAT:w" s="T166">ɛn</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_635" n="HIAT:w" s="T167">uːčʼiqonoːqo</ts>
                  <nts id="Seg_636" n="HIAT:ip">.</nts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_639" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_641" n="HIAT:w" s="T168">Čʼuntɨ</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_644" n="HIAT:w" s="T169">iːntetɨ</ts>
                  <nts id="Seg_645" n="HIAT:ip">,</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_648" n="HIAT:w" s="T170">solotalʼ</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_651" n="HIAT:w" s="T171">kusoktɨ</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_654" n="HIAT:w" s="T172">toː</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_657" n="HIAT:w" s="T173">milʼčʼiŋɨt</ts>
                  <nts id="Seg_658" n="HIAT:ip">.</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_661" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_663" n="HIAT:w" s="T174">Nɨːnɨ</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_666" n="HIAT:w" s="T175">na</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_669" n="HIAT:w" s="T176">čʼunnentɨsa</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_672" n="HIAT:w" s="T177">qənnanəəə</ts>
                  <nts id="Seg_673" n="HIAT:ip">,</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_676" n="HIAT:w" s="T178">qənna</ts>
                  <nts id="Seg_677" n="HIAT:ip">,</nts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_680" n="HIAT:w" s="T179">qənna</ts>
                  <nts id="Seg_681" n="HIAT:ip">.</nts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_684" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_686" n="HIAT:w" s="T180">Ukkur</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_689" n="HIAT:w" s="T181">čʼontoːqɨt</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_692" n="HIAT:w" s="T182">ruš</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_695" n="HIAT:w" s="T183">namɨt</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_698" n="HIAT:w" s="T184">montɨ</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_701" n="HIAT:w" s="T185">sɨːrɨtɨp</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_704" n="HIAT:w" s="T186">tɔːqqɨmpat</ts>
                  <nts id="Seg_705" n="HIAT:ip">,</nts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_708" n="HIAT:w" s="T187">moːrətɨ</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_711" n="HIAT:w" s="T188">aša</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_714" n="HIAT:w" s="T189">ata</ts>
                  <nts id="Seg_715" n="HIAT:ip">.</nts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_718" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_720" n="HIAT:w" s="T190">Lʼa</ts>
                  <nts id="Seg_721" n="HIAT:ip">,</nts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_724" n="HIAT:w" s="T191">tɛnɨtɨ</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_727" n="HIAT:w" s="T192">nık</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_730" n="HIAT:w" s="T193">kollimɔːnna</ts>
                  <nts id="Seg_731" n="HIAT:ip">.</nts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_734" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_736" n="HIAT:w" s="T194">Molokontoːqo</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_738" n="HIAT:ip">(</nts>
                  <nts id="Seg_739" n="HIAT:ip">/</nts>
                  <ts e="T196" id="Seg_741" n="HIAT:w" s="T195">nʼimantoːqo</ts>
                  <nts id="Seg_742" n="HIAT:ip">)</nts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_745" n="HIAT:w" s="T196">sɨːrəp</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_748" n="HIAT:w" s="T197">iːsam</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_751" n="HIAT:w" s="T198">ɛna</ts>
                  <nts id="Seg_752" n="HIAT:ip">.</nts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_755" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_757" n="HIAT:w" s="T199">Čʼuntɨmtɨ</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_760" n="HIAT:w" s="T200">sɨːrɨ</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_763" n="HIAT:w" s="T201">čʼɔːtɨ</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_766" n="HIAT:w" s="T202">toː</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_769" n="HIAT:w" s="T203">milʼčʼit</ts>
                  <nts id="Seg_770" n="HIAT:ip">.</nts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_773" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_775" n="HIAT:w" s="T204">Na</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_778" n="HIAT:w" s="T205">sɨːrɨntɨsä</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_781" n="HIAT:w" s="T206">na</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_784" n="HIAT:w" s="T207">qəntana</ts>
                  <nts id="Seg_785" n="HIAT:ip">,</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_788" n="HIAT:w" s="T208">qəntanəəə</ts>
                  <nts id="Seg_789" n="HIAT:ip">.</nts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_792" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_794" n="HIAT:w" s="T209">Qo</ts>
                  <nts id="Seg_795" n="HIAT:ip">,</nts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_798" n="HIAT:w" s="T210">ukkur</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_801" n="HIAT:w" s="T211">čʼontoːqɨt</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_804" n="HIAT:w" s="T212">sɨpɨnčʼatɨp</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_807" n="HIAT:w" s="T213">na</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_810" n="HIAT:w" s="T214">montɨ</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_813" n="HIAT:w" s="T215">tɔːqqɨmpɔːtɨt</ts>
                  <nts id="Seg_814" n="HIAT:ip">,</nts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_817" n="HIAT:w" s="T216">moːrɨtɨ</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_820" n="HIAT:w" s="T217">aša</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_823" n="HIAT:w" s="T218">ata</ts>
                  <nts id="Seg_824" n="HIAT:ip">.</nts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_827" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_829" n="HIAT:w" s="T219">Nɨːnɨ</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_832" n="HIAT:w" s="T220">nıː</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_835" n="HIAT:w" s="T221">kətɨtɨ</ts>
                  <nts id="Seg_836" n="HIAT:ip">,</nts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_839" n="HIAT:w" s="T222">lʼa</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_842" n="HIAT:w" s="T223">sɨpɨnčʼa</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_845" n="HIAT:w" s="T224">namɨt</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_848" n="HIAT:w" s="T225">monta</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_851" n="HIAT:w" s="T226">ürɨŋ</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_854" n="HIAT:w" s="T227">ɛːŋa</ts>
                  <nts id="Seg_855" n="HIAT:ip">.</nts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_858" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_860" n="HIAT:w" s="T228">Lʼa</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_863" n="HIAT:w" s="T229">mɨta</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_866" n="HIAT:w" s="T230">sɨpɨnčʼa</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_869" n="HIAT:w" s="T231">žirnak</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_872" n="HIAT:w" s="T232">ɛːŋa</ts>
                  <nts id="Seg_873" n="HIAT:ip">,</nts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_876" n="HIAT:w" s="T233">sɨpɨnčʼap</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_879" n="HIAT:w" s="T234">iːsam</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_882" n="HIAT:w" s="T235">ɛna</ts>
                  <nts id="Seg_883" n="HIAT:ip">.</nts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_886" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_888" n="HIAT:w" s="T236">Sɨpɨnčʼat</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_891" n="HIAT:w" s="T237">čʼɔːtɨ</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_894" n="HIAT:w" s="T238">toː</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_897" n="HIAT:w" s="T239">miŋɨtɨ</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_900" n="HIAT:w" s="T240">na</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_903" n="HIAT:w" s="T241">sɨːrɨmtɨ</ts>
                  <nts id="Seg_904" n="HIAT:ip">.</nts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_907" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_909" n="HIAT:w" s="T242">Aj</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_912" n="HIAT:w" s="T243">menʼalsʼa</ts>
                  <nts id="Seg_913" n="HIAT:ip">.</nts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_916" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_918" n="HIAT:w" s="T244">Nɨːnɨ</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_921" n="HIAT:w" s="T245">na</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_924" n="HIAT:w" s="T246">sɨpɨnčʼantɨsa</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_927" n="HIAT:w" s="T247">na</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_930" n="HIAT:w" s="T248">qənta</ts>
                  <nts id="Seg_931" n="HIAT:ip">,</nts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_934" n="HIAT:w" s="T249">na</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_937" n="HIAT:w" s="T250">qəntanəəə</ts>
                  <nts id="Seg_938" n="HIAT:ip">.</nts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_941" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_943" n="HIAT:w" s="T251">Ukkur</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_946" n="HIAT:w" s="T252">tät</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_949" n="HIAT:w" s="T253">čʼontoːqɨt</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_952" n="HIAT:w" s="T254">na</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_955" n="HIAT:w" s="T255">ruš</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_958" n="HIAT:w" s="T256">kəːtɨpɨlʼ</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_961" n="HIAT:w" s="T257">čʼıŋkɨtɨp</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_964" n="HIAT:w" s="T258">tɔːqqɨmpa</ts>
                  <nts id="Seg_965" n="HIAT:ip">.</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_968" n="HIAT:u" s="T259">
                  <ts e="T260" id="Seg_970" n="HIAT:w" s="T259">Čʼıŋkɨtɨm</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_973" n="HIAT:w" s="T260">namɨšek</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_976" n="HIAT:w" s="T261">tɔːqqɨmpat</ts>
                  <nts id="Seg_977" n="HIAT:ip">,</nts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_980" n="HIAT:w" s="T262">moːrətə</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_983" n="HIAT:w" s="T263">čʼäːŋka</ts>
                  <nts id="Seg_984" n="HIAT:ip">.</nts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_987" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_989" n="HIAT:w" s="T264">Qɔːtɨ</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_992" n="HIAT:w" s="T265">jajcatɨ</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_995" n="HIAT:w" s="T266">to</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_998" n="HIAT:w" s="T267">mnogo</ts>
                  <nts id="Seg_999" n="HIAT:ip">,</nts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1002" n="HIAT:w" s="T268">čʼıŋkɨp</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1005" n="HIAT:w" s="T269">iːsam</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1008" n="HIAT:w" s="T270">ɛna</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1010" n="HIAT:ip">(</nts>
                  <ts e="T272" id="Seg_1012" n="HIAT:w" s="T271">ɛŋtoːqo</ts>
                  <nts id="Seg_1013" n="HIAT:ip">)</nts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1015" n="HIAT:ip">(</nts>
                  <nts id="Seg_1016" n="HIAT:ip">/</nts>
                  <ts e="T273" id="Seg_1018" n="HIAT:w" s="T272">ɛŋtɨ</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1021" n="HIAT:w" s="T273">čʼɔːtɨ</ts>
                  <nts id="Seg_1022" n="HIAT:ip">)</nts>
                  <nts id="Seg_1023" n="HIAT:ip">.</nts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_1026" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_1028" n="HIAT:w" s="T274">Čʼıŋkɨt</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1031" n="HIAT:w" s="T275">čʼɔːtɨ</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1034" n="HIAT:w" s="T276">sɨpɨnčʼamtɨ</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1037" n="HIAT:w" s="T277">toː</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1040" n="HIAT:w" s="T278">milʼčʼitɨ</ts>
                  <nts id="Seg_1041" n="HIAT:ip">.</nts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1044" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_1046" n="HIAT:w" s="T279">Nɨːnɨ</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1049" n="HIAT:w" s="T280">na</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1052" n="HIAT:w" s="T281">čʼıŋkɨntɨsa</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1055" n="HIAT:w" s="T282">na</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1058" n="HIAT:w" s="T283">qənna</ts>
                  <nts id="Seg_1059" n="HIAT:ip">,</nts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1062" n="HIAT:w" s="T284">qənna</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1065" n="HIAT:w" s="T285">moqɨnä</ts>
                  <nts id="Seg_1066" n="HIAT:ip">.</nts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1069" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_1071" n="HIAT:w" s="T286">Ukkur</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1074" n="HIAT:w" s="T287">čʼontot</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1076" n="HIAT:ip">(</nts>
                  <nts id="Seg_1077" n="HIAT:ip">/</nts>
                  <ts e="T289" id="Seg_1079" n="HIAT:w" s="T288">čʼontoːqɨt</ts>
                  <nts id="Seg_1080" n="HIAT:ip">)</nts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1083" n="HIAT:w" s="T289">monte</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1086" n="HIAT:w" s="T290">ruš</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1089" n="HIAT:w" s="T291">töːkotɨp</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1092" n="HIAT:w" s="T292">namɨšak</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1095" n="HIAT:w" s="T293">tɔːqqɨmpat</ts>
                  <nts id="Seg_1096" n="HIAT:ip">,</nts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1099" n="HIAT:w" s="T294">moːretə</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1102" n="HIAT:w" s="T295">aša</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1105" n="HIAT:w" s="T296">ata</ts>
                  <nts id="Seg_1106" n="HIAT:ip">.</nts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_1109" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1111" n="HIAT:w" s="T297">Qɔːta</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1114" n="HIAT:w" s="T298">töːkop</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1117" n="HIAT:w" s="T299">iːsam</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1120" n="HIAT:w" s="T300">ɛna</ts>
                  <nts id="Seg_1121" n="HIAT:ip">.</nts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1124" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1126" n="HIAT:w" s="T301">Na</ts>
                  <nts id="Seg_1127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1129" n="HIAT:w" s="T302">čʼıŋkɨmtɨ</ts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1132" n="HIAT:w" s="T303">töːkot</ts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1135" n="HIAT:w" s="T304">čʼɔːtɨ</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1138" n="HIAT:w" s="T305">toː</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1141" n="HIAT:w" s="T306">milʼčʼiŋɨt</ts>
                  <nts id="Seg_1142" n="HIAT:ip">.</nts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T311" id="Seg_1145" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_1147" n="HIAT:w" s="T307">Nɨːnɨ</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1150" n="HIAT:w" s="T308">töːkontosä</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1153" n="HIAT:w" s="T309">qənna</ts>
                  <nts id="Seg_1154" n="HIAT:ip">,</nts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1157" n="HIAT:w" s="T310">qənnanəəə</ts>
                  <nts id="Seg_1158" n="HIAT:ip">.</nts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1161" n="HIAT:u" s="T311">
                  <ts e="T312" id="Seg_1163" n="HIAT:w" s="T311">Ukkɨr</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1166" n="HIAT:w" s="T312">iːralʼ</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1169" n="HIAT:w" s="T313">lıːpɨ</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1172" n="HIAT:w" s="T314">monpa</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1175" n="HIAT:w" s="T315">tintena</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1178" n="HIAT:w" s="T316">namɨt</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1181" n="HIAT:w" s="T317">keːpɨlʼ</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1183" n="HIAT:ip">(</nts>
                  <nts id="Seg_1184" n="HIAT:ip">/</nts>
                  <ts e="T319" id="Seg_1186" n="HIAT:w" s="T318">keːpilʼ</ts>
                  <nts id="Seg_1187" n="HIAT:ip">)</nts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1190" n="HIAT:w" s="T319">mɨkatɨ</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1193" n="HIAT:w" s="T320">türɨŋpatɨ</ts>
                  <nts id="Seg_1194" n="HIAT:ip">.</nts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T324" id="Seg_1197" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_1199" n="HIAT:w" s="T321">Mɨka</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1202" n="HIAT:w" s="T322">aj</ts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1205" n="HIAT:w" s="T323">mɨka</ts>
                  <nts id="Seg_1206" n="HIAT:ip">.</nts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1209" n="HIAT:u" s="T324">
                  <ts e="T325" id="Seg_1211" n="HIAT:w" s="T324">Lʼa</ts>
                  <nts id="Seg_1212" n="HIAT:ip">,</nts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1215" n="HIAT:w" s="T325">tɛnatɨ</ts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1218" n="HIAT:w" s="T326">nı</ts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1221" n="HIAT:w" s="T327">kollimɔːnna</ts>
                  <nts id="Seg_1222" n="HIAT:ip">.</nts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T332" id="Seg_1225" n="HIAT:u" s="T328">
                  <ts e="T329" id="Seg_1227" n="HIAT:w" s="T328">Lʼa</ts>
                  <nts id="Seg_1228" n="HIAT:ip">,</nts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1231" n="HIAT:w" s="T329">sajekɨtɨlʼ</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1234" n="HIAT:w" s="T330">loːsoŋɔːk</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1237" n="HIAT:w" s="T331">lʼaqa</ts>
                  <nts id="Seg_1238" n="HIAT:ip">.</nts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T343" id="Seg_1241" n="HIAT:u" s="T332">
                  <ts e="T333" id="Seg_1243" n="HIAT:w" s="T332">Qajem</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1246" n="HIAT:w" s="T333">map</ts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1249" n="HIAT:w" s="T334">šütqo</ts>
                  <nts id="Seg_1250" n="HIAT:ip">,</nts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1253" n="HIAT:w" s="T335">qaim</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1256" n="HIAT:w" s="T336">šütqo</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1259" n="HIAT:w" s="T337">qɔːtɨ</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1262" n="HIAT:w" s="T338">na</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1265" n="HIAT:w" s="T339">irat</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1268" n="HIAT:w" s="T340">mɨkap</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1271" n="HIAT:w" s="T341">iːsam</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1274" n="HIAT:w" s="T342">ɛŋa</ts>
                  <nts id="Seg_1275" n="HIAT:ip">.</nts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T352" id="Seg_1278" n="HIAT:u" s="T343">
                  <ts e="T344" id="Seg_1280" n="HIAT:w" s="T343">Tintena</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1283" n="HIAT:w" s="T344">mɨka</ts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1286" n="HIAT:w" s="T345">čʼɔːtɨ</ts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1289" n="HIAT:w" s="T346">toː</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1292" n="HIAT:w" s="T347">miŋɨtɨ</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1295" n="HIAT:w" s="T348">töːkotɨ</ts>
                  <nts id="Seg_1296" n="HIAT:ip">,</nts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1299" n="HIAT:w" s="T349">milʼčʼitɨ</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1302" n="HIAT:w" s="T350">mɨkat</ts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1305" n="HIAT:w" s="T351">čʼɔːtɨ</ts>
                  <nts id="Seg_1306" n="HIAT:ip">.</nts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1309" n="HIAT:u" s="T352">
                  <ts e="T353" id="Seg_1311" n="HIAT:w" s="T352">Nɨːnɨ</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1314" n="HIAT:w" s="T353">na</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1317" n="HIAT:w" s="T354">mɨkantɨsä</ts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1320" n="HIAT:w" s="T355">qənna</ts>
                  <nts id="Seg_1321" n="HIAT:ip">,</nts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1324" n="HIAT:w" s="T356">qənna</ts>
                  <nts id="Seg_1325" n="HIAT:ip">.</nts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1328" n="HIAT:u" s="T357">
                  <ts e="T358" id="Seg_1330" n="HIAT:w" s="T357">Mɨka</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1333" n="HIAT:w" s="T358">ürrɛːjsa</ts>
                  <nts id="Seg_1334" n="HIAT:ip">.</nts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366" id="Seg_1337" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_1339" n="HIAT:w" s="T359">Qət</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1342" n="HIAT:w" s="T360">ši</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1345" n="HIAT:w" s="T361">mɔːttɨ</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1348" n="HIAT:w" s="T362">čʼap</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1351" n="HIAT:w" s="T363">šeːrna</ts>
                  <nts id="Seg_1352" n="HIAT:ip">,</nts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1355" n="HIAT:w" s="T364">mɨka</ts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1358" n="HIAT:w" s="T365">urrɛːsʼa</ts>
                  <nts id="Seg_1359" n="HIAT:ip">.</nts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T372" id="Seg_1362" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1364" n="HIAT:w" s="T366">Na</ts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1367" n="HIAT:w" s="T367">qəːt</ts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1370" n="HIAT:w" s="T368">šinčʼoːqɨt</ts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1373" n="HIAT:w" s="T369">na</ts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1376" n="HIAT:w" s="T370">peːŋɨtɨ</ts>
                  <nts id="Seg_1377" n="HIAT:ip">,</nts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1380" n="HIAT:w" s="T371">püntɨrlʼä</ts>
                  <nts id="Seg_1381" n="HIAT:ip">.</nts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1384" n="HIAT:u" s="T372">
                  <ts e="T373" id="Seg_1386" n="HIAT:w" s="T372">Tat</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1389" n="HIAT:w" s="T373">qaj</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1392" n="HIAT:w" s="T374">peːŋal</ts>
                  <nts id="Seg_1393" n="HIAT:ip">,</nts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1396" n="HIAT:w" s="T375">irra</ts>
                  <nts id="Seg_1397" n="HIAT:ip">?</nts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T379" id="Seg_1400" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1402" n="HIAT:w" s="T376">—tɔːlʼ</ts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1405" n="HIAT:w" s="T377">mɔːtalʼ</ts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1408" n="HIAT:w" s="T378">qup</ts>
                  <nts id="Seg_1409" n="HIAT:ip">.</nts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T387" id="Seg_1412" n="HIAT:u" s="T379">
                  <ts e="T380" id="Seg_1414" n="HIAT:w" s="T379">Lʼa</ts>
                  <nts id="Seg_1415" n="HIAT:ip">,</nts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1418" n="HIAT:w" s="T380">mat</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1421" n="HIAT:w" s="T381">mɨkap</ts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1424" n="HIAT:w" s="T382">čʼap</ts>
                  <nts id="Seg_1425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1427" n="HIAT:w" s="T383">tatɨntap</ts>
                  <nts id="Seg_1428" n="HIAT:ip">,</nts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1431" n="HIAT:w" s="T384">tintena</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1434" n="HIAT:w" s="T385">mɨka</ts>
                  <nts id="Seg_1435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1437" n="HIAT:w" s="T386">ürtentap</ts>
                  <nts id="Seg_1438" n="HIAT:ip">.</nts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1441" n="HIAT:u" s="T387">
                  <ts e="T388" id="Seg_1443" n="HIAT:w" s="T387">Mɨkamɨ</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1446" n="HIAT:w" s="T388">tap</ts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1449" n="HIAT:w" s="T389">peːntap</ts>
                  <nts id="Seg_1450" n="HIAT:ip">.</nts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T391" id="Seg_1453" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1455" n="HIAT:w" s="T390">Qo</ts>
                  <nts id="Seg_1456" n="HIAT:ip">!</nts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1459" n="HIAT:u" s="T391">
                  <ts e="T392" id="Seg_1461" n="HIAT:w" s="T391">Imaqotaqantɨ</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1464" n="HIAT:w" s="T392">mɔːt</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1467" n="HIAT:w" s="T393">šeːraš</ts>
                  <nts id="Seg_1468" n="HIAT:ip">,</nts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1471" n="HIAT:w" s="T394">ameraš</ts>
                  <nts id="Seg_1472" n="HIAT:ip">.</nts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T398" id="Seg_1475" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1477" n="HIAT:w" s="T395">Nimto</ts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1480" n="HIAT:w" s="T396">na</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1483" n="HIAT:w" s="T397">jta</ts>
                  <nts id="Seg_1484" n="HIAT:ip">.</nts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T402" id="Seg_1487" n="HIAT:u" s="T398">
                  <ts e="T398.tx.1" id="Seg_1489" n="HIAT:w" s="T398">Malenʼkij</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398.tx.2" id="Seg_1492" n="HIAT:w" s="T398.tx.1">umišʼa</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398.tx.3" id="Seg_1495" n="HIAT:w" s="T398.tx.2">starik</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1498" n="HIAT:w" s="T398.tx.3">to</ts>
                  <nts id="Seg_1499" n="HIAT:ip">.</nts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T402" id="Seg_1501" n="sc" s="T0">
               <ts e="T1" id="Seg_1503" n="e" s="T0">Istarik </ts>
               <ts e="T2" id="Seg_1505" n="e" s="T1">pervɨj </ts>
               <ts e="T3" id="Seg_1507" n="e" s="T2">voda </ts>
               <ts e="T4" id="Seg_1509" n="e" s="T3">zamerznet. </ts>
               <ts e="T5" id="Seg_1511" n="e" s="T4">Qatamol </ts>
               <ts e="T6" id="Seg_1513" n="e" s="T5">ütɨp </ts>
               <ts e="T7" id="Seg_1515" n="e" s="T6">ammɛːjit, </ts>
               <ts e="T8" id="Seg_1517" n="e" s="T7">ira </ts>
               <ts e="T9" id="Seg_1519" n="e" s="T8">koralʼä </ts>
               <ts e="T10" id="Seg_1521" n="e" s="T9">(/qəːlɨlʼlʼä) </ts>
               <ts e="T11" id="Seg_1523" n="e" s="T10">qənpa. </ts>
               <ts e="T12" id="Seg_1525" n="e" s="T11">Qoltɨp </ts>
               <ts e="T13" id="Seg_1527" n="e" s="T12">kušat </ts>
               <ts e="T14" id="Seg_1529" n="e" s="T13">orqälɔːlnɨt, </ts>
               <ts e="T15" id="Seg_1531" n="e" s="T14">irra </ts>
               <ts e="T16" id="Seg_1533" n="e" s="T15">qoltit </ts>
               <ts e="T17" id="Seg_1535" n="e" s="T16">qanɨŋmɨt </ts>
               <ts e="T18" id="Seg_1537" n="e" s="T17">nʼanna </ts>
               <ts e="T19" id="Seg_1539" n="e" s="T18">mantelɔːlpɨlʼä </ts>
               <ts e="T20" id="Seg_1541" n="e" s="T19">kurɨna </ts>
               <ts e="T21" id="Seg_1543" n="e" s="T20">suːrɨtqo, </ts>
               <ts e="T22" id="Seg_1545" n="e" s="T21">tä </ts>
               <ts e="T23" id="Seg_1547" n="e" s="T22">toː </ts>
               <ts e="T24" id="Seg_1549" n="e" s="T23">mantalpelʼa </ts>
               <ts e="T25" id="Seg_1551" n="e" s="T24">qəəə. </ts>
               <ts e="T26" id="Seg_1553" n="e" s="T25">Ukkur </ts>
               <ts e="T27" id="Seg_1555" n="e" s="T26">čʼontoːqɨt </ts>
               <ts e="T28" id="Seg_1557" n="e" s="T27">täp </ts>
               <ts e="T29" id="Seg_1559" n="e" s="T28">qos </ts>
               <ts e="T30" id="Seg_1561" n="e" s="T29">qaj </ts>
               <ts e="T31" id="Seg_1563" n="e" s="T30">orɨčʼčʼa. </ts>
               <ts e="T32" id="Seg_1565" n="e" s="T31">Qaj </ts>
               <ts e="T33" id="Seg_1567" n="e" s="T32">qup </ts>
               <ts e="T34" id="Seg_1569" n="e" s="T33">laŋkɨčʼčʼa, </ts>
               <ts e="T35" id="Seg_1571" n="e" s="T34">qaj </ts>
               <ts e="T36" id="Seg_1573" n="e" s="T35">qaj </ts>
               <ts e="T37" id="Seg_1575" n="e" s="T36">orɨčʼčʼa. </ts>
               <ts e="T38" id="Seg_1577" n="e" s="T37">Täp </ts>
               <ts e="T39" id="Seg_1579" n="e" s="T38">qaj, </ts>
               <ts e="T40" id="Seg_1581" n="e" s="T39">qup </ts>
               <ts e="T41" id="Seg_1583" n="e" s="T40">lʼi, </ts>
               <ts e="T42" id="Seg_1585" n="e" s="T41">qaj </ts>
               <ts e="T43" id="Seg_1587" n="e" s="T42">laŋkɨčʼčʼa? </ts>
               <ts e="T44" id="Seg_1589" n="e" s="T43">Ira </ts>
               <ts e="T45" id="Seg_1591" n="e" s="T44">nık </ts>
               <ts e="T46" id="Seg_1593" n="e" s="T45">na </ts>
               <ts e="T47" id="Seg_1595" n="e" s="T46">üntɨčʼit. </ts>
               <ts e="T48" id="Seg_1597" n="e" s="T47">Nɨːna </ts>
               <ts e="T49" id="Seg_1599" n="e" s="T48">tünta. </ts>
               <ts e="T50" id="Seg_1601" n="e" s="T49">Qəəə! </ts>
               <ts e="T51" id="Seg_1603" n="e" s="T50">Točʼčʼo </ts>
               <ts e="T52" id="Seg_1605" n="e" s="T51">nʼempoːqɨt, </ts>
               <ts e="T53" id="Seg_1607" n="e" s="T52">qajel </ts>
               <ts e="T54" id="Seg_1609" n="e" s="T53">laka </ts>
               <ts e="T55" id="Seg_1611" n="e" s="T54">ɛːsʼa? </ts>
               <ts e="T56" id="Seg_1613" n="e" s="T55">Nempoːqɨt </ts>
               <ts e="T57" id="Seg_1615" n="e" s="T56">mɨta </ts>
               <ts e="T58" id="Seg_1617" n="e" s="T57">qajilʼ </ts>
               <ts e="T59" id="Seg_1619" n="e" s="T58">laka </ts>
               <ts e="T60" id="Seg_1621" n="e" s="T59">qontɨlʼčʼa. </ts>
               <ts e="T61" id="Seg_1623" n="e" s="T60">İnnä </ts>
               <ts e="T62" id="Seg_1625" n="e" s="T61">čʼap </ts>
               <ts e="T63" id="Seg_1627" n="e" s="T62">kɛ </ts>
               <ts e="T64" id="Seg_1629" n="e" s="T63">qatɨtɔːlka, </ts>
               <ts e="T65" id="Seg_1631" n="e" s="T64">aj </ts>
               <ts e="T66" id="Seg_1633" n="e" s="T65">ılla </ts>
               <ts e="T67" id="Seg_1635" n="e" s="T66">qənnɛːja. </ts>
               <ts e="T68" id="Seg_1637" n="e" s="T67">Aj </ts>
               <ts e="T69" id="Seg_1639" n="e" s="T68">ılla </ts>
               <ts e="T70" id="Seg_1641" n="e" s="T69">qənnɛːja. </ts>
               <ts e="T71" id="Seg_1643" n="e" s="T70">Načʼä </ts>
               <ts e="T72" id="Seg_1645" n="e" s="T71">čʼap </ts>
               <ts e="T73" id="Seg_1647" n="e" s="T72">qənna </ts>
               <ts e="T74" id="Seg_1649" n="e" s="T73">montɨ </ts>
               <ts e="T75" id="Seg_1651" n="e" s="T74">qup, </ts>
               <ts e="T76" id="Seg_1653" n="e" s="T75">üttɨ </ts>
               <ts e="T77" id="Seg_1655" n="e" s="T76">patqɨlpa, </ts>
               <ts e="T78" id="Seg_1657" n="e" s="T77">nʼempontɨ. </ts>
               <ts e="T79" id="Seg_1659" n="e" s="T78">Pošalusta,— </ts>
               <ts e="T80" id="Seg_1661" n="e" s="T79">lʼaŋɨnʼnʼa,— </ts>
               <ts e="T81" id="Seg_1663" n="e" s="T80">qɔːtä </ts>
               <ts e="T82" id="Seg_1665" n="e" s="T81">ınnä </ts>
               <ts e="T83" id="Seg_1667" n="e" s="T82">šık </ts>
               <ts e="T84" id="Seg_1669" n="e" s="T83">iːjaš. </ts>
               <ts e="T85" id="Seg_1671" n="e" s="T84">Timte </ts>
               <ts e="T86" id="Seg_1673" n="e" s="T85">na </ts>
               <ts e="T87" id="Seg_1675" n="e" s="T86">irra </ts>
               <ts e="T88" id="Seg_1677" n="e" s="T87">konna </ts>
               <ts e="T89" id="Seg_1679" n="e" s="T88">pakta. </ts>
               <ts e="T90" id="Seg_1681" n="e" s="T89">Čʼumpɨ </ts>
               <ts e="T91" id="Seg_1683" n="e" s="T90">nʼarqɨp </ts>
               <ts e="T92" id="Seg_1685" n="e" s="T91">iːŋɨtɨ. </ts>
               <ts e="T93" id="Seg_1687" n="e" s="T92">Čʼumpɨ </ts>
               <ts e="T94" id="Seg_1689" n="e" s="T93">nʼarqɨp </ts>
               <ts e="T95" id="Seg_1691" n="e" s="T94">pačʼčʼaltɨt. </ts>
               <ts e="T96" id="Seg_1693" n="e" s="T95">Karrä </ts>
               <ts e="T97" id="Seg_1695" n="e" s="T96">iːttɨtɨ</ts>
               <ts e="T98" id="Seg_1697" n="e" s="T97">(/iːttɛːtɨ, </ts>
               <ts e="T99" id="Seg_1699" n="e" s="T98">čʼattɛːtɨ) </ts>
               <ts e="T100" id="Seg_1701" n="e" s="T99">irätkine. </ts>
               <ts e="T101" id="Seg_1703" n="e" s="T100">Nʼa </ts>
               <ts e="T102" id="Seg_1705" n="e" s="T101">nʼarqɨsä </ts>
               <ts e="T103" id="Seg_1707" n="e" s="T102">konnä </ts>
               <ts e="T104" id="Seg_1709" n="e" s="T103">ınna </ts>
               <ts e="T105" id="Seg_1711" n="e" s="T104">üːqɨlʼlʼɛːŋɨta. </ts>
               <ts e="T106" id="Seg_1713" n="e" s="T105">Konna </ts>
               <ts e="T107" id="Seg_1715" n="e" s="T106">čʼap </ts>
               <ts e="T108" id="Seg_1717" n="e" s="T107">üːqɨlʼlʼɛːŋat. </ts>
               <ts e="T109" id="Seg_1719" n="e" s="T108">Qo, </ts>
               <ts e="T110" id="Seg_1721" n="e" s="T109">qo! </ts>
               <ts e="T111" id="Seg_1723" n="e" s="T110">Monte </ts>
               <ts e="T112" id="Seg_1725" n="e" s="T111">temqup </ts>
               <ts e="T113" id="Seg_1727" n="e" s="T112">(/kupec), </ts>
               <ts e="T114" id="Seg_1729" n="e" s="T113">kupec </ts>
               <ts e="T115" id="Seg_1731" n="e" s="T114">na </ts>
               <ts e="T116" id="Seg_1733" n="e" s="T115">tətta. </ts>
               <ts e="T117" id="Seg_1735" n="e" s="T116">Tat </ts>
               <ts e="T118" id="Seg_1737" n="e" s="T117">mašıp </ts>
               <ts e="T119" id="Seg_1739" n="e" s="T118">ınnä </ts>
               <ts e="T120" id="Seg_1741" n="e" s="T119">šık </ts>
               <ts e="T121" id="Seg_1743" n="e" s="T120">iːŋantɨ, </ts>
               <ts e="T122" id="Seg_1745" n="e" s="T121">qurmon </ts>
               <ts e="T123" id="Seg_1747" n="e" s="T122">nɔːnɨ, </ts>
               <ts e="T124" id="Seg_1749" n="e" s="T123">ınnä </ts>
               <ts e="T125" id="Seg_1751" n="e" s="T124">šı </ts>
               <ts e="T126" id="Seg_1753" n="e" s="T125">iːŋantɨ. </ts>
               <ts e="T127" id="Seg_1755" n="e" s="T126">İnnä </ts>
               <ts e="T128" id="Seg_1757" n="e" s="T127">šı </ts>
               <ts e="T129" id="Seg_1759" n="e" s="T128">iːŋantɨ. </ts>
               <ts e="T130" id="Seg_1761" n="e" s="T129">Solotalʼ </ts>
               <ts e="T131" id="Seg_1763" n="e" s="T130">kusoksä </ts>
               <ts e="T132" id="Seg_1765" n="e" s="T131">miːŋatɨ. </ts>
               <ts e="T133" id="Seg_1767" n="e" s="T132">Mitɨ </ts>
               <ts e="T134" id="Seg_1769" n="e" s="T133">qurmon </ts>
               <ts e="T135" id="Seg_1771" n="e" s="T134">nɔːnɨ </ts>
               <ts e="T136" id="Seg_1773" n="e" s="T135">ınnä </ts>
               <ts e="T137" id="Seg_1775" n="e" s="T136">šıŋ </ts>
               <ts e="T138" id="Seg_1777" n="e" s="T137">iːŋäntɨ. </ts>
               <ts e="T139" id="Seg_1779" n="e" s="T138">Qaj </ts>
               <ts e="T140" id="Seg_1781" n="e" s="T139">iːtal, </ts>
               <ts e="T141" id="Seg_1783" n="e" s="T140">qaj </ts>
               <ts e="T142" id="Seg_1785" n="e" s="T141">iltäːlʼ </ts>
               <ts e="T143" id="Seg_1787" n="e" s="T142">iːtal? </ts>
               <ts e="T144" id="Seg_1789" n="e" s="T143">Na </ts>
               <ts e="T145" id="Seg_1791" n="e" s="T144">qəntanəəə. </ts>
               <ts e="T146" id="Seg_1793" n="e" s="T145">Lʼa, </ts>
               <ts e="T147" id="Seg_1795" n="e" s="T146">čʼuntɨ, </ts>
               <ts e="T148" id="Seg_1797" n="e" s="T147">ukkur </ts>
               <ts e="T149" id="Seg_1799" n="e" s="T148">ruš </ts>
               <ts e="T150" id="Seg_1801" n="e" s="T149">na </ts>
               <ts e="T151" id="Seg_1803" n="e" s="T150">montɨ </ts>
               <ts e="T152" id="Seg_1805" n="e" s="T151">čʼuntɨp </ts>
               <ts e="T153" id="Seg_1807" n="e" s="T152">tɔːqqɨmpatə, </ts>
               <ts e="T154" id="Seg_1809" n="e" s="T153">moːrətɨ </ts>
               <ts e="T155" id="Seg_1811" n="e" s="T154">aša </ts>
               <ts e="T156" id="Seg_1813" n="e" s="T155">ata. </ts>
               <ts e="T157" id="Seg_1815" n="e" s="T156">Nɨːna </ts>
               <ts e="T158" id="Seg_1817" n="e" s="T157">tünta. </ts>
               <ts e="T159" id="Seg_1819" n="e" s="T158">Lʼa, </ts>
               <ts e="T160" id="Seg_1821" n="e" s="T159">tɛnätɨ </ts>
               <ts e="T161" id="Seg_1823" n="e" s="T160">nık </ts>
               <ts e="T162" id="Seg_1825" n="e" s="T161">kollimɔːnna. </ts>
               <ts e="T163" id="Seg_1827" n="e" s="T162">Qɔːtɨ </ts>
               <ts e="T164" id="Seg_1829" n="e" s="T163">mompa </ts>
               <ts e="T165" id="Seg_1831" n="e" s="T164">čʼuntɨp </ts>
               <ts e="T166" id="Seg_1833" n="e" s="T165">iːsam </ts>
               <ts e="T167" id="Seg_1835" n="e" s="T166">ɛn </ts>
               <ts e="T168" id="Seg_1837" n="e" s="T167">uːčʼiqonoːqo. </ts>
               <ts e="T169" id="Seg_1839" n="e" s="T168">Čʼuntɨ </ts>
               <ts e="T170" id="Seg_1841" n="e" s="T169">iːntetɨ, </ts>
               <ts e="T171" id="Seg_1843" n="e" s="T170">solotalʼ </ts>
               <ts e="T172" id="Seg_1845" n="e" s="T171">kusoktɨ </ts>
               <ts e="T173" id="Seg_1847" n="e" s="T172">toː </ts>
               <ts e="T174" id="Seg_1849" n="e" s="T173">milʼčʼiŋɨt. </ts>
               <ts e="T175" id="Seg_1851" n="e" s="T174">Nɨːnɨ </ts>
               <ts e="T176" id="Seg_1853" n="e" s="T175">na </ts>
               <ts e="T177" id="Seg_1855" n="e" s="T176">čʼunnentɨsa </ts>
               <ts e="T178" id="Seg_1857" n="e" s="T177">qənnanəəə, </ts>
               <ts e="T179" id="Seg_1859" n="e" s="T178">qənna, </ts>
               <ts e="T180" id="Seg_1861" n="e" s="T179">qənna. </ts>
               <ts e="T181" id="Seg_1863" n="e" s="T180">Ukkur </ts>
               <ts e="T182" id="Seg_1865" n="e" s="T181">čʼontoːqɨt </ts>
               <ts e="T183" id="Seg_1867" n="e" s="T182">ruš </ts>
               <ts e="T184" id="Seg_1869" n="e" s="T183">namɨt </ts>
               <ts e="T185" id="Seg_1871" n="e" s="T184">montɨ </ts>
               <ts e="T186" id="Seg_1873" n="e" s="T185">sɨːrɨtɨp </ts>
               <ts e="T187" id="Seg_1875" n="e" s="T186">tɔːqqɨmpat, </ts>
               <ts e="T188" id="Seg_1877" n="e" s="T187">moːrətɨ </ts>
               <ts e="T189" id="Seg_1879" n="e" s="T188">aša </ts>
               <ts e="T190" id="Seg_1881" n="e" s="T189">ata. </ts>
               <ts e="T191" id="Seg_1883" n="e" s="T190">Lʼa, </ts>
               <ts e="T192" id="Seg_1885" n="e" s="T191">tɛnɨtɨ </ts>
               <ts e="T193" id="Seg_1887" n="e" s="T192">nık </ts>
               <ts e="T194" id="Seg_1889" n="e" s="T193">kollimɔːnna. </ts>
               <ts e="T195" id="Seg_1891" n="e" s="T194">Molokontoːqo </ts>
               <ts e="T196" id="Seg_1893" n="e" s="T195">(/nʼimantoːqo) </ts>
               <ts e="T197" id="Seg_1895" n="e" s="T196">sɨːrəp </ts>
               <ts e="T198" id="Seg_1897" n="e" s="T197">iːsam </ts>
               <ts e="T199" id="Seg_1899" n="e" s="T198">ɛna. </ts>
               <ts e="T200" id="Seg_1901" n="e" s="T199">Čʼuntɨmtɨ </ts>
               <ts e="T201" id="Seg_1903" n="e" s="T200">sɨːrɨ </ts>
               <ts e="T202" id="Seg_1905" n="e" s="T201">čʼɔːtɨ </ts>
               <ts e="T203" id="Seg_1907" n="e" s="T202">toː </ts>
               <ts e="T204" id="Seg_1909" n="e" s="T203">milʼčʼit. </ts>
               <ts e="T205" id="Seg_1911" n="e" s="T204">Na </ts>
               <ts e="T206" id="Seg_1913" n="e" s="T205">sɨːrɨntɨsä </ts>
               <ts e="T207" id="Seg_1915" n="e" s="T206">na </ts>
               <ts e="T208" id="Seg_1917" n="e" s="T207">qəntana, </ts>
               <ts e="T209" id="Seg_1919" n="e" s="T208">qəntanəəə. </ts>
               <ts e="T210" id="Seg_1921" n="e" s="T209">Qo, </ts>
               <ts e="T211" id="Seg_1923" n="e" s="T210">ukkur </ts>
               <ts e="T212" id="Seg_1925" n="e" s="T211">čʼontoːqɨt </ts>
               <ts e="T213" id="Seg_1927" n="e" s="T212">sɨpɨnčʼatɨp </ts>
               <ts e="T214" id="Seg_1929" n="e" s="T213">na </ts>
               <ts e="T215" id="Seg_1931" n="e" s="T214">montɨ </ts>
               <ts e="T216" id="Seg_1933" n="e" s="T215">tɔːqqɨmpɔːtɨt, </ts>
               <ts e="T217" id="Seg_1935" n="e" s="T216">moːrɨtɨ </ts>
               <ts e="T218" id="Seg_1937" n="e" s="T217">aša </ts>
               <ts e="T219" id="Seg_1939" n="e" s="T218">ata. </ts>
               <ts e="T220" id="Seg_1941" n="e" s="T219">Nɨːnɨ </ts>
               <ts e="T221" id="Seg_1943" n="e" s="T220">nıː </ts>
               <ts e="T222" id="Seg_1945" n="e" s="T221">kətɨtɨ, </ts>
               <ts e="T223" id="Seg_1947" n="e" s="T222">lʼa </ts>
               <ts e="T224" id="Seg_1949" n="e" s="T223">sɨpɨnčʼa </ts>
               <ts e="T225" id="Seg_1951" n="e" s="T224">namɨt </ts>
               <ts e="T226" id="Seg_1953" n="e" s="T225">monta </ts>
               <ts e="T227" id="Seg_1955" n="e" s="T226">ürɨŋ </ts>
               <ts e="T228" id="Seg_1957" n="e" s="T227">ɛːŋa. </ts>
               <ts e="T229" id="Seg_1959" n="e" s="T228">Lʼa </ts>
               <ts e="T230" id="Seg_1961" n="e" s="T229">mɨta </ts>
               <ts e="T231" id="Seg_1963" n="e" s="T230">sɨpɨnčʼa </ts>
               <ts e="T232" id="Seg_1965" n="e" s="T231">žirnak </ts>
               <ts e="T233" id="Seg_1967" n="e" s="T232">ɛːŋa, </ts>
               <ts e="T234" id="Seg_1969" n="e" s="T233">sɨpɨnčʼap </ts>
               <ts e="T235" id="Seg_1971" n="e" s="T234">iːsam </ts>
               <ts e="T236" id="Seg_1973" n="e" s="T235">ɛna. </ts>
               <ts e="T237" id="Seg_1975" n="e" s="T236">Sɨpɨnčʼat </ts>
               <ts e="T238" id="Seg_1977" n="e" s="T237">čʼɔːtɨ </ts>
               <ts e="T239" id="Seg_1979" n="e" s="T238">toː </ts>
               <ts e="T240" id="Seg_1981" n="e" s="T239">miŋɨtɨ </ts>
               <ts e="T241" id="Seg_1983" n="e" s="T240">na </ts>
               <ts e="T242" id="Seg_1985" n="e" s="T241">sɨːrɨmtɨ. </ts>
               <ts e="T243" id="Seg_1987" n="e" s="T242">Aj </ts>
               <ts e="T244" id="Seg_1989" n="e" s="T243">menʼalsʼa. </ts>
               <ts e="T245" id="Seg_1991" n="e" s="T244">Nɨːnɨ </ts>
               <ts e="T246" id="Seg_1993" n="e" s="T245">na </ts>
               <ts e="T247" id="Seg_1995" n="e" s="T246">sɨpɨnčʼantɨsa </ts>
               <ts e="T248" id="Seg_1997" n="e" s="T247">na </ts>
               <ts e="T249" id="Seg_1999" n="e" s="T248">qənta, </ts>
               <ts e="T250" id="Seg_2001" n="e" s="T249">na </ts>
               <ts e="T251" id="Seg_2003" n="e" s="T250">qəntanəəə. </ts>
               <ts e="T252" id="Seg_2005" n="e" s="T251">Ukkur </ts>
               <ts e="T253" id="Seg_2007" n="e" s="T252">tät </ts>
               <ts e="T254" id="Seg_2009" n="e" s="T253">čʼontoːqɨt </ts>
               <ts e="T255" id="Seg_2011" n="e" s="T254">na </ts>
               <ts e="T256" id="Seg_2013" n="e" s="T255">ruš </ts>
               <ts e="T257" id="Seg_2015" n="e" s="T256">kəːtɨpɨlʼ </ts>
               <ts e="T258" id="Seg_2017" n="e" s="T257">čʼıŋkɨtɨp </ts>
               <ts e="T259" id="Seg_2019" n="e" s="T258">tɔːqqɨmpa. </ts>
               <ts e="T260" id="Seg_2021" n="e" s="T259">Čʼıŋkɨtɨm </ts>
               <ts e="T261" id="Seg_2023" n="e" s="T260">namɨšek </ts>
               <ts e="T262" id="Seg_2025" n="e" s="T261">tɔːqqɨmpat, </ts>
               <ts e="T263" id="Seg_2027" n="e" s="T262">moːrətə </ts>
               <ts e="T264" id="Seg_2029" n="e" s="T263">čʼäːŋka. </ts>
               <ts e="T265" id="Seg_2031" n="e" s="T264">Qɔːtɨ </ts>
               <ts e="T266" id="Seg_2033" n="e" s="T265">jajcatɨ </ts>
               <ts e="T267" id="Seg_2035" n="e" s="T266">to </ts>
               <ts e="T268" id="Seg_2037" n="e" s="T267">mnogo, </ts>
               <ts e="T269" id="Seg_2039" n="e" s="T268">čʼıŋkɨp </ts>
               <ts e="T270" id="Seg_2041" n="e" s="T269">iːsam </ts>
               <ts e="T271" id="Seg_2043" n="e" s="T270">ɛna </ts>
               <ts e="T272" id="Seg_2045" n="e" s="T271">(ɛŋtoːqo) </ts>
               <ts e="T273" id="Seg_2047" n="e" s="T272">(/ɛŋtɨ </ts>
               <ts e="T274" id="Seg_2049" n="e" s="T273">čʼɔːtɨ). </ts>
               <ts e="T275" id="Seg_2051" n="e" s="T274">Čʼıŋkɨt </ts>
               <ts e="T276" id="Seg_2053" n="e" s="T275">čʼɔːtɨ </ts>
               <ts e="T277" id="Seg_2055" n="e" s="T276">sɨpɨnčʼamtɨ </ts>
               <ts e="T278" id="Seg_2057" n="e" s="T277">toː </ts>
               <ts e="T279" id="Seg_2059" n="e" s="T278">milʼčʼitɨ. </ts>
               <ts e="T280" id="Seg_2061" n="e" s="T279">Nɨːnɨ </ts>
               <ts e="T281" id="Seg_2063" n="e" s="T280">na </ts>
               <ts e="T282" id="Seg_2065" n="e" s="T281">čʼıŋkɨntɨsa </ts>
               <ts e="T283" id="Seg_2067" n="e" s="T282">na </ts>
               <ts e="T284" id="Seg_2069" n="e" s="T283">qənna, </ts>
               <ts e="T285" id="Seg_2071" n="e" s="T284">qənna </ts>
               <ts e="T286" id="Seg_2073" n="e" s="T285">moqɨnä. </ts>
               <ts e="T287" id="Seg_2075" n="e" s="T286">Ukkur </ts>
               <ts e="T288" id="Seg_2077" n="e" s="T287">čʼontot </ts>
               <ts e="T289" id="Seg_2079" n="e" s="T288">(/čʼontoːqɨt) </ts>
               <ts e="T290" id="Seg_2081" n="e" s="T289">monte </ts>
               <ts e="T291" id="Seg_2083" n="e" s="T290">ruš </ts>
               <ts e="T292" id="Seg_2085" n="e" s="T291">töːkotɨp </ts>
               <ts e="T293" id="Seg_2087" n="e" s="T292">namɨšak </ts>
               <ts e="T294" id="Seg_2089" n="e" s="T293">tɔːqqɨmpat, </ts>
               <ts e="T295" id="Seg_2091" n="e" s="T294">moːretə </ts>
               <ts e="T296" id="Seg_2093" n="e" s="T295">aša </ts>
               <ts e="T297" id="Seg_2095" n="e" s="T296">ata. </ts>
               <ts e="T298" id="Seg_2097" n="e" s="T297">Qɔːta </ts>
               <ts e="T299" id="Seg_2099" n="e" s="T298">töːkop </ts>
               <ts e="T300" id="Seg_2101" n="e" s="T299">iːsam </ts>
               <ts e="T301" id="Seg_2103" n="e" s="T300">ɛna. </ts>
               <ts e="T302" id="Seg_2105" n="e" s="T301">Na </ts>
               <ts e="T303" id="Seg_2107" n="e" s="T302">čʼıŋkɨmtɨ </ts>
               <ts e="T304" id="Seg_2109" n="e" s="T303">töːkot </ts>
               <ts e="T305" id="Seg_2111" n="e" s="T304">čʼɔːtɨ </ts>
               <ts e="T306" id="Seg_2113" n="e" s="T305">toː </ts>
               <ts e="T307" id="Seg_2115" n="e" s="T306">milʼčʼiŋɨt. </ts>
               <ts e="T308" id="Seg_2117" n="e" s="T307">Nɨːnɨ </ts>
               <ts e="T309" id="Seg_2119" n="e" s="T308">töːkontosä </ts>
               <ts e="T310" id="Seg_2121" n="e" s="T309">qənna, </ts>
               <ts e="T311" id="Seg_2123" n="e" s="T310">qənnanəəə. </ts>
               <ts e="T312" id="Seg_2125" n="e" s="T311">Ukkɨr </ts>
               <ts e="T313" id="Seg_2127" n="e" s="T312">iːralʼ </ts>
               <ts e="T314" id="Seg_2129" n="e" s="T313">lıːpɨ </ts>
               <ts e="T315" id="Seg_2131" n="e" s="T314">monpa </ts>
               <ts e="T316" id="Seg_2133" n="e" s="T315">tintena </ts>
               <ts e="T317" id="Seg_2135" n="e" s="T316">namɨt </ts>
               <ts e="T318" id="Seg_2137" n="e" s="T317">keːpɨlʼ </ts>
               <ts e="T319" id="Seg_2139" n="e" s="T318">(/keːpilʼ) </ts>
               <ts e="T320" id="Seg_2141" n="e" s="T319">mɨkatɨ </ts>
               <ts e="T321" id="Seg_2143" n="e" s="T320">türɨŋpatɨ. </ts>
               <ts e="T322" id="Seg_2145" n="e" s="T321">Mɨka </ts>
               <ts e="T323" id="Seg_2147" n="e" s="T322">aj </ts>
               <ts e="T324" id="Seg_2149" n="e" s="T323">mɨka. </ts>
               <ts e="T325" id="Seg_2151" n="e" s="T324">Lʼa, </ts>
               <ts e="T326" id="Seg_2153" n="e" s="T325">tɛnatɨ </ts>
               <ts e="T327" id="Seg_2155" n="e" s="T326">nı </ts>
               <ts e="T328" id="Seg_2157" n="e" s="T327">kollimɔːnna. </ts>
               <ts e="T329" id="Seg_2159" n="e" s="T328">Lʼa, </ts>
               <ts e="T330" id="Seg_2161" n="e" s="T329">sajekɨtɨlʼ </ts>
               <ts e="T331" id="Seg_2163" n="e" s="T330">loːsoŋɔːk </ts>
               <ts e="T332" id="Seg_2165" n="e" s="T331">lʼaqa. </ts>
               <ts e="T333" id="Seg_2167" n="e" s="T332">Qajem </ts>
               <ts e="T334" id="Seg_2169" n="e" s="T333">map </ts>
               <ts e="T335" id="Seg_2171" n="e" s="T334">šütqo, </ts>
               <ts e="T336" id="Seg_2173" n="e" s="T335">qaim </ts>
               <ts e="T337" id="Seg_2175" n="e" s="T336">šütqo </ts>
               <ts e="T338" id="Seg_2177" n="e" s="T337">qɔːtɨ </ts>
               <ts e="T339" id="Seg_2179" n="e" s="T338">na </ts>
               <ts e="T340" id="Seg_2181" n="e" s="T339">irat </ts>
               <ts e="T341" id="Seg_2183" n="e" s="T340">mɨkap </ts>
               <ts e="T342" id="Seg_2185" n="e" s="T341">iːsam </ts>
               <ts e="T343" id="Seg_2187" n="e" s="T342">ɛŋa. </ts>
               <ts e="T344" id="Seg_2189" n="e" s="T343">Tintena </ts>
               <ts e="T345" id="Seg_2191" n="e" s="T344">mɨka </ts>
               <ts e="T346" id="Seg_2193" n="e" s="T345">čʼɔːtɨ </ts>
               <ts e="T347" id="Seg_2195" n="e" s="T346">toː </ts>
               <ts e="T348" id="Seg_2197" n="e" s="T347">miŋɨtɨ </ts>
               <ts e="T349" id="Seg_2199" n="e" s="T348">töːkotɨ, </ts>
               <ts e="T350" id="Seg_2201" n="e" s="T349">milʼčʼitɨ </ts>
               <ts e="T351" id="Seg_2203" n="e" s="T350">mɨkat </ts>
               <ts e="T352" id="Seg_2205" n="e" s="T351">čʼɔːtɨ. </ts>
               <ts e="T353" id="Seg_2207" n="e" s="T352">Nɨːnɨ </ts>
               <ts e="T354" id="Seg_2209" n="e" s="T353">na </ts>
               <ts e="T355" id="Seg_2211" n="e" s="T354">mɨkantɨsä </ts>
               <ts e="T356" id="Seg_2213" n="e" s="T355">qənna, </ts>
               <ts e="T357" id="Seg_2215" n="e" s="T356">qənna. </ts>
               <ts e="T358" id="Seg_2217" n="e" s="T357">Mɨka </ts>
               <ts e="T359" id="Seg_2219" n="e" s="T358">ürrɛːjsa. </ts>
               <ts e="T360" id="Seg_2221" n="e" s="T359">Qət </ts>
               <ts e="T361" id="Seg_2223" n="e" s="T360">ši </ts>
               <ts e="T362" id="Seg_2225" n="e" s="T361">mɔːttɨ </ts>
               <ts e="T363" id="Seg_2227" n="e" s="T362">čʼap </ts>
               <ts e="T364" id="Seg_2229" n="e" s="T363">šeːrna, </ts>
               <ts e="T365" id="Seg_2231" n="e" s="T364">mɨka </ts>
               <ts e="T366" id="Seg_2233" n="e" s="T365">urrɛːsʼa. </ts>
               <ts e="T367" id="Seg_2235" n="e" s="T366">Na </ts>
               <ts e="T368" id="Seg_2237" n="e" s="T367">qəːt </ts>
               <ts e="T369" id="Seg_2239" n="e" s="T368">šinčʼoːqɨt </ts>
               <ts e="T370" id="Seg_2241" n="e" s="T369">na </ts>
               <ts e="T371" id="Seg_2243" n="e" s="T370">peːŋɨtɨ, </ts>
               <ts e="T372" id="Seg_2245" n="e" s="T371">püntɨrlʼä. </ts>
               <ts e="T373" id="Seg_2247" n="e" s="T372">Tat </ts>
               <ts e="T374" id="Seg_2249" n="e" s="T373">qaj </ts>
               <ts e="T375" id="Seg_2251" n="e" s="T374">peːŋal, </ts>
               <ts e="T376" id="Seg_2253" n="e" s="T375">irra? </ts>
               <ts e="T377" id="Seg_2255" n="e" s="T376">—tɔːlʼ </ts>
               <ts e="T378" id="Seg_2257" n="e" s="T377">mɔːtalʼ </ts>
               <ts e="T379" id="Seg_2259" n="e" s="T378">qup. </ts>
               <ts e="T380" id="Seg_2261" n="e" s="T379">Lʼa, </ts>
               <ts e="T381" id="Seg_2263" n="e" s="T380">mat </ts>
               <ts e="T382" id="Seg_2265" n="e" s="T381">mɨkap </ts>
               <ts e="T383" id="Seg_2267" n="e" s="T382">čʼap </ts>
               <ts e="T384" id="Seg_2269" n="e" s="T383">tatɨntap, </ts>
               <ts e="T385" id="Seg_2271" n="e" s="T384">tintena </ts>
               <ts e="T386" id="Seg_2273" n="e" s="T385">mɨka </ts>
               <ts e="T387" id="Seg_2275" n="e" s="T386">ürtentap. </ts>
               <ts e="T388" id="Seg_2277" n="e" s="T387">Mɨkamɨ </ts>
               <ts e="T389" id="Seg_2279" n="e" s="T388">tap </ts>
               <ts e="T390" id="Seg_2281" n="e" s="T389">peːntap. </ts>
               <ts e="T391" id="Seg_2283" n="e" s="T390">Qo! </ts>
               <ts e="T392" id="Seg_2285" n="e" s="T391">Imaqotaqantɨ </ts>
               <ts e="T393" id="Seg_2287" n="e" s="T392">mɔːt </ts>
               <ts e="T394" id="Seg_2289" n="e" s="T393">šeːraš, </ts>
               <ts e="T395" id="Seg_2291" n="e" s="T394">ameraš. </ts>
               <ts e="T396" id="Seg_2293" n="e" s="T395">Nimto </ts>
               <ts e="T397" id="Seg_2295" n="e" s="T396">na </ts>
               <ts e="T398" id="Seg_2297" n="e" s="T397">jta. </ts>
               <ts e="T402" id="Seg_2299" n="e" s="T398">Malenʼkij umišʼa starik to. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_2300" s="T0">KAI_1965_OldManWithLittleMind1_flk.001 (001.001)</ta>
            <ta e="T11" id="Seg_2301" s="T4">KAI_1965_OldManWithLittleMind1_flk.002 (001.002)</ta>
            <ta e="T25" id="Seg_2302" s="T11">KAI_1965_OldManWithLittleMind1_flk.003 (001.003)</ta>
            <ta e="T31" id="Seg_2303" s="T25">KAI_1965_OldManWithLittleMind1_flk.004 (001.004)</ta>
            <ta e="T37" id="Seg_2304" s="T31">KAI_1965_OldManWithLittleMind1_flk.005 (001.005)</ta>
            <ta e="T43" id="Seg_2305" s="T37">KAI_1965_OldManWithLittleMind1_flk.006 (001.006)</ta>
            <ta e="T47" id="Seg_2306" s="T43">KAI_1965_OldManWithLittleMind1_flk.007 (001.007)</ta>
            <ta e="T49" id="Seg_2307" s="T47">KAI_1965_OldManWithLittleMind1_flk.008 (001.008)</ta>
            <ta e="T50" id="Seg_2308" s="T49">KAI_1965_OldManWithLittleMind1_flk.009 (001.009)</ta>
            <ta e="T55" id="Seg_2309" s="T50">KAI_1965_OldManWithLittleMind1_flk.010 (001.010)</ta>
            <ta e="T60" id="Seg_2310" s="T55">KAI_1965_OldManWithLittleMind1_flk.011 (001.011)</ta>
            <ta e="T67" id="Seg_2311" s="T60">KAI_1965_OldManWithLittleMind1_flk.012 (001.012)</ta>
            <ta e="T70" id="Seg_2312" s="T67">KAI_1965_OldManWithLittleMind1_flk.013 (001.013)</ta>
            <ta e="T78" id="Seg_2313" s="T70">KAI_1965_OldManWithLittleMind1_flk.014 (001.014)</ta>
            <ta e="T84" id="Seg_2314" s="T78">KAI_1965_OldManWithLittleMind1_flk.015 (001.015)</ta>
            <ta e="T89" id="Seg_2315" s="T84">KAI_1965_OldManWithLittleMind1_flk.016 (001.016)</ta>
            <ta e="T92" id="Seg_2316" s="T89">KAI_1965_OldManWithLittleMind1_flk.017 (001.017)</ta>
            <ta e="T95" id="Seg_2317" s="T92">KAI_1965_OldManWithLittleMind1_flk.018 (001.018)</ta>
            <ta e="T100" id="Seg_2318" s="T95">KAI_1965_OldManWithLittleMind1_flk.019 (001.019)</ta>
            <ta e="T105" id="Seg_2319" s="T100">KAI_1965_OldManWithLittleMind1_flk.020 (001.020)</ta>
            <ta e="T108" id="Seg_2320" s="T105">KAI_1965_OldManWithLittleMind1_flk.021 (001.021)</ta>
            <ta e="T110" id="Seg_2321" s="T108">KAI_1965_OldManWithLittleMind1_flk.022 (001.022)</ta>
            <ta e="T116" id="Seg_2322" s="T110">KAI_1965_OldManWithLittleMind1_flk.023 (001.023)</ta>
            <ta e="T126" id="Seg_2323" s="T116">KAI_1965_OldManWithLittleMind1_flk.024 (001.024)</ta>
            <ta e="T129" id="Seg_2324" s="T126">KAI_1965_OldManWithLittleMind1_flk.025 (001.025)</ta>
            <ta e="T132" id="Seg_2325" s="T129">KAI_1965_OldManWithLittleMind1_flk.026 (001.026)</ta>
            <ta e="T138" id="Seg_2326" s="T132">KAI_1965_OldManWithLittleMind1_flk.027 (001.027)</ta>
            <ta e="T143" id="Seg_2327" s="T138">KAI_1965_OldManWithLittleMind1_flk.028 (001.028)</ta>
            <ta e="T145" id="Seg_2328" s="T143">KAI_1965_OldManWithLittleMind1_flk.029 (002.001)</ta>
            <ta e="T156" id="Seg_2329" s="T145">KAI_1965_OldManWithLittleMind1_flk.030 (002.002)</ta>
            <ta e="T158" id="Seg_2330" s="T156">KAI_1965_OldManWithLittleMind1_flk.031 (002.003)</ta>
            <ta e="T162" id="Seg_2331" s="T158">KAI_1965_OldManWithLittleMind1_flk.032 (002.004)</ta>
            <ta e="T168" id="Seg_2332" s="T162">KAI_1965_OldManWithLittleMind1_flk.033 (002.005)</ta>
            <ta e="T174" id="Seg_2333" s="T168">KAI_1965_OldManWithLittleMind1_flk.034 (002.006)</ta>
            <ta e="T180" id="Seg_2334" s="T174">KAI_1965_OldManWithLittleMind1_flk.035 (002.007)</ta>
            <ta e="T190" id="Seg_2335" s="T180">KAI_1965_OldManWithLittleMind1_flk.036 (002.008)</ta>
            <ta e="T194" id="Seg_2336" s="T190">KAI_1965_OldManWithLittleMind1_flk.037 (002.009)</ta>
            <ta e="T199" id="Seg_2337" s="T194">KAI_1965_OldManWithLittleMind1_flk.038 (002.010)</ta>
            <ta e="T204" id="Seg_2338" s="T199">KAI_1965_OldManWithLittleMind1_flk.039 (002.011)</ta>
            <ta e="T209" id="Seg_2339" s="T204">KAI_1965_OldManWithLittleMind1_flk.040 (002.012)</ta>
            <ta e="T219" id="Seg_2340" s="T209">KAI_1965_OldManWithLittleMind1_flk.041 (002.013)</ta>
            <ta e="T228" id="Seg_2341" s="T219">KAI_1965_OldManWithLittleMind1_flk.042 (002.014)</ta>
            <ta e="T236" id="Seg_2342" s="T228">KAI_1965_OldManWithLittleMind1_flk.043 (002.015)</ta>
            <ta e="T242" id="Seg_2343" s="T236">KAI_1965_OldManWithLittleMind1_flk.044 (002.016)</ta>
            <ta e="T244" id="Seg_2344" s="T242">KAI_1965_OldManWithLittleMind1_flk.045 (002.017)</ta>
            <ta e="T251" id="Seg_2345" s="T244">KAI_1965_OldManWithLittleMind1_flk.046 (002.018)</ta>
            <ta e="T259" id="Seg_2346" s="T251">KAI_1965_OldManWithLittleMind1_flk.047 (002.019)</ta>
            <ta e="T264" id="Seg_2347" s="T259">KAI_1965_OldManWithLittleMind1_flk.048 (002.020)</ta>
            <ta e="T274" id="Seg_2348" s="T264">KAI_1965_OldManWithLittleMind1_flk.049 (002.021)</ta>
            <ta e="T279" id="Seg_2349" s="T274">KAI_1965_OldManWithLittleMind1_flk.050 (002.022)</ta>
            <ta e="T286" id="Seg_2350" s="T279">KAI_1965_OldManWithLittleMind1_flk.051 (002.023)</ta>
            <ta e="T297" id="Seg_2351" s="T286">KAI_1965_OldManWithLittleMind1_flk.052 (002.024)</ta>
            <ta e="T301" id="Seg_2352" s="T297">KAI_1965_OldManWithLittleMind1_flk.053 (002.025)</ta>
            <ta e="T307" id="Seg_2353" s="T301">KAI_1965_OldManWithLittleMind1_flk.054 (002.026)</ta>
            <ta e="T311" id="Seg_2354" s="T307">KAI_1965_OldManWithLittleMind1_flk.055 (002.027)</ta>
            <ta e="T321" id="Seg_2355" s="T311">KAI_1965_OldManWithLittleMind1_flk.056 (002.028)</ta>
            <ta e="T324" id="Seg_2356" s="T321">KAI_1965_OldManWithLittleMind1_flk.057 (002.029)</ta>
            <ta e="T328" id="Seg_2357" s="T324">KAI_1965_OldManWithLittleMind1_flk.058 (002.030)</ta>
            <ta e="T332" id="Seg_2358" s="T328">KAI_1965_OldManWithLittleMind1_flk.059 (002.031)</ta>
            <ta e="T343" id="Seg_2359" s="T332">KAI_1965_OldManWithLittleMind1_flk.060 (002.032)</ta>
            <ta e="T352" id="Seg_2360" s="T343">KAI_1965_OldManWithLittleMind1_flk.061 (002.033)</ta>
            <ta e="T357" id="Seg_2361" s="T352">KAI_1965_OldManWithLittleMind1_flk.062 (002.034)</ta>
            <ta e="T359" id="Seg_2362" s="T357">KAI_1965_OldManWithLittleMind1_flk.063 (002.035)</ta>
            <ta e="T366" id="Seg_2363" s="T359">KAI_1965_OldManWithLittleMind1_flk.064 (002.036)</ta>
            <ta e="T372" id="Seg_2364" s="T366">KAI_1965_OldManWithLittleMind1_flk.065 (002.037)</ta>
            <ta e="T376" id="Seg_2365" s="T372">KAI_1965_OldManWithLittleMind1_flk.066 (002.038)</ta>
            <ta e="T379" id="Seg_2366" s="T376">KAI_1965_OldManWithLittleMind1_flk.067 (002.039)</ta>
            <ta e="T387" id="Seg_2367" s="T379">KAI_1965_OldManWithLittleMind1_flk.068 (002.040)</ta>
            <ta e="T390" id="Seg_2368" s="T387">KAI_1965_OldManWithLittleMind1_flk.069 (002.041)</ta>
            <ta e="T391" id="Seg_2369" s="T390">KAI_1965_OldManWithLittleMind1_flk.070 (002.042)</ta>
            <ta e="T395" id="Seg_2370" s="T391">KAI_1965_OldManWithLittleMind1_flk.071 (002.043)</ta>
            <ta e="T398" id="Seg_2371" s="T395">KAI_1965_OldManWithLittleMind1_flk.072 (002.044)</ta>
            <ta e="T402" id="Seg_2372" s="T398">KAI_1965_OldManWithLittleMind1_flk.073 (002.045)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_2373" s="T0">истарик первый вода з̂амерзнет. </ta>
            <ta e="T11" id="Seg_2374" s="T4">kатамоl ӱ̄тып ам′меjит ′и′ра kоралʼе [kъ̊лылʼе] ′kъ̊̄нпа.</ta>
            <ta e="T25" id="Seg_2375" s="T11">′kоlтып ку′шат ‵орkӓ′lоlныт ′ирра ′kолтит kаныңмыт ′нʼанна манте‵lоl′пылʼӓ. ′курынна ′сӯрытkо ′тӓто ман′таlпелʼа kъ̊ъ̊ъ̊.</ta>
            <ta e="T31" id="Seg_2376" s="T25">уккур чонтоkыт тӓп kос kай ′орыча.</ta>
            <ta e="T37" id="Seg_2377" s="T31">kай kуп lаң[к]ыча, kай, kай ′орыча.</ta>
            <ta e="T43" id="Seg_2378" s="T37">тӓп kай kуп лʼи kай ′lаңкыча. </ta>
            <ta e="T47" id="Seg_2379" s="T43">и′ра ник на ′ӱндычит.</ta>
            <ta e="T49" id="Seg_2380" s="T47">нына ′тӱ̄нта.</ta>
            <ta e="T50" id="Seg_2381" s="T49">kъ̊ъ̊ъ̊. </ta>
            <ta e="T55" id="Seg_2382" s="T50">′тотчо ′нʼем‵поkыт, kаjел ′lака ′е̨̄сʼа?</ta>
            <ta e="T60" id="Seg_2383" s="T55">немпоkыт ′мыта kаjиl lаkа ′kондыlча.</ta>
            <ta e="T67" id="Seg_2384" s="T60">′ыннӓ чап′ке̨ kаты′тоlка. ай ′илла kъ̊ннеjа.</ta>
            <ta e="T78" id="Seg_2385" s="T70">наче чап ′kъ̊̄нна монты kуп ′ӱтты ′патkылпа, ′нʼем‵понты.</ta>
            <ta e="T84" id="Seg_2386" s="T78">пошалуста ′лʼаңынʼа kо̄тӓ ыннӓ шʼик ′иjашʼ.</ta>
            <ta e="T89" id="Seg_2387" s="T84">тимтена ирра ′конна ′пакта.</ta>
            <ta e="T92" id="Seg_2388" s="T89">′чумпы ′нʼарkып ′ӣңыты.</ta>
            <ta e="T95" id="Seg_2389" s="T92">′чумпы ′нʼарkып ′па̊тчаlтыт.</ta>
            <ta e="T100" id="Seg_2390" s="T95">kа′рре̨ ′иттыты (иттӓты, чаттеты) и′рӓткине.</ta>
            <ta e="T105" id="Seg_2391" s="T100">нʼа ′нʼарkысӓ ′коннӓ ′ынна ӱ̄кы′лʼеңыта.</ta>
            <ta e="T108" id="Seg_2392" s="T105">′конна чап ӱ̄кы′лʼеңат.</ta>
            <ta e="T110" id="Seg_2393" s="T108">kо̄, kо̄.</ta>
            <ta e="T116" id="Seg_2394" s="T110">′монте (ку′пец) ‎‎′те̨м kуп купец на ′тӓ[ъ̊]тта.</ta>
            <ta e="T126" id="Seg_2395" s="T116">тат ′ма̄′шип иннӓ шʼик ӣңанты, ′курмоныноны, иннӓ шʼи иңанты.</ta>
            <ta e="T129" id="Seg_2396" s="T126">иннӓ шʼи иңанты.</ta>
            <ta e="T132" id="Seg_2397" s="T129">′соlотаl ку′соксӓ ′мӣңаты.</ta>
            <ta e="T138" id="Seg_2398" s="T132">мӣты ′курмоны ноны ыннӓ ш′иң ′иңӓнты.</ta>
            <ta e="T143" id="Seg_2399" s="T138">kай ′ӣтаl kай ′иlтӓlӓ ′ӣтаl?</ta>
            <ta e="T145" id="Seg_2400" s="T143">на ′kъ̊нтанъ̊ъ̊ъ̊.</ta>
            <ta e="T156" id="Seg_2401" s="T145">lа ′чунты уккур руш на монты чунтып тоkkым′патъ, мо̄ръты ашʼа ′ата.</ta>
            <ta e="T158" id="Seg_2402" s="T156">нына тӱнта.</ta>
            <ta e="T162" id="Seg_2403" s="T158">lа тӓнӓты нӣк ‵коllи′монна.</ta>
            <ta e="T168" id="Seg_2404" s="T162">′kоты ′момпа ′чунтып ′ӣсамен ӯчиконоkо.</ta>
            <ta e="T174" id="Seg_2405" s="T168">чунты ′ӣнтеты, соlотаl ку′сокты то ′милʼчиңыт.</ta>
            <ta e="T180" id="Seg_2406" s="T174">ныны на ′чуннентыса ′kъ̊̄нна нъ̊ъ̊ъ̊, ′kъ̊нна, ′kъ̊нна.</ta>
            <ta e="T190" id="Seg_2407" s="T180">уккур чон′тоkыт руш ′намыт ′монты ′сырытып тоkымпат, моръты ′ашʼа ′а̄та.</ta>
            <ta e="T194" id="Seg_2408" s="T190">lа ′тӓныты нӣк колʼлʼи′монна.</ta>
            <ta e="T199" id="Seg_2409" s="T194">моlоконтоkо [нимантоkо] ′сы̄ръп ′ӣсамена.</ta>
            <ta e="T204" id="Seg_2410" s="T199">′чуд̂нымты ‵сыры′чоты томилʼчит.</ta>
            <ta e="T209" id="Seg_2411" s="T204">на сырынты се′на kънта′на, kъ̊нта′нъ̊ъ̊ъ̊.</ta>
            <ta e="T219" id="Seg_2412" s="T209">kо̄, уккур чонтоkыт ′сыпынчатып на монты тоkkым′потыт, ′морыты ашʼа ′а̄та.</ta>
            <ta e="T228" id="Seg_2413" s="T219">ныны нӣ ′kъ̄тыты, lа̄ сыпынча намыт монта ‵ӱ̄рың′е̄ңа.</ta>
            <ta e="T236" id="Seg_2414" s="T228">lа ′мыта ′сыпынча жирнаk ′еңа, ′сыпынчап ′ӣсамена.</ta>
            <ta e="T242" id="Seg_2415" s="T236">сыпынчат ′чоты то миңыты на ′сырымты.</ta>
            <ta e="T244" id="Seg_2416" s="T242">ай менялся.</ta>
            <ta e="T251" id="Seg_2417" s="T244">ныны на сыпын′чантыса на kъ̊нта, на kъ̊нта нъ̊ъ̊ъ̊.</ta>
            <ta e="T259" id="Seg_2418" s="T251">уккур тӓт чонтоkыт на рӯш kъ̊тыпылʼ ′чиңкытып ′тоkkымпа.</ta>
            <ta e="T264" id="Seg_2419" s="T259">чинкытым намышʼек ′тоkkымпат ′мо̄рътъ ′чеңка.</ta>
            <ta e="T274" id="Seg_2420" s="T264">′kоты яйцаты то много, чинкып ′ӣса‵ме̄на (′е̄ңтоkо) [′еңты чоты].</ta>
            <ta e="T279" id="Seg_2421" s="T274">′чиңкыт чоты ′сыпынчамты ′томилʼчиты.</ta>
            <ta e="T286" id="Seg_2422" s="T279">ныны на ′чиңкынтыса на kъ̊нна, ′kъ̊нна, моkынӓ.</ta>
            <ta e="T297" id="Seg_2423" s="T286">уккур чон′то̄т [тоɣыт] ′монте ′рӯш ′тъ̊̄котып ′намышак ′токымпат ′море[ъ]тъ ашʼа а̄та.</ta>
            <ta e="T301" id="Seg_2424" s="T297">′kота ′тъ̊коп ′ӣса′ме̄на.</ta>
            <ta e="T307" id="Seg_2425" s="T301">на чиңымты тъ̊кот ′чо̄ты ′то ′мӣлʼчиңыт.</ta>
            <ta e="T311" id="Seg_2426" s="T307">ныны ′тъ̊контосӓ ′kъ̊нна нъ̊ъ̊ъ̊.</ta>
            <ta e="T321" id="Seg_2427" s="T311">уккыр ′ӣраl ′lӣпы ′монпа ′тинтена ′намыт ′кʼе̄пыl [′кʼе̄биl] ′мыкаты ′тӱ̄рың′паты.</ta>
            <ta e="T324" id="Seg_2428" s="T321">′мыка ай ′мыка.</ta>
            <ta e="T328" id="Seg_2429" s="T324">lа тӓнаты ни′колʼи‵монна.</ta>
            <ta e="T332" id="Seg_2430" s="T328">лʼа̄ ′саjе‵kытыl ′lосоңок лʼака.</ta>
            <ta e="T343" id="Seg_2431" s="T332">′kаjемап ′щʼутkо, kаим щʼутkо kоты на ират ′мыкап ′ӣса′ме̄ңа.</ta>
            <ta e="T352" id="Seg_2432" s="T343">′тиндена ′мыка чоты то ′мӣңыты тъ̊коты ′мӣlчиты ′мыкат чо̄ты.</ta>
            <ta e="T357" id="Seg_2433" s="T352">ныны на ′мыкантысӓ kъ̊нна kъ̊нна.</ta>
            <ta e="T359" id="Seg_2434" s="T357">′мыка ӱр′рейса.</ta>
            <ta e="T366" id="Seg_2435" s="T359">kъ̊т шʼи ′мо̄тты чап шʼе̄рна, ′мыка у′рресʼа.</ta>
            <ta e="T372" id="Seg_2436" s="T366">на kъ̊̄т шʼин′чоkыт на ′пе̄ңыты, ′пӱнтырлʼе.</ta>
            <ta e="T376" id="Seg_2437" s="T372">тат kай ′пе̄ңаl и′рра?</ta>
            <ta e="T379" id="Seg_2438" s="T376">тоl ′мотаl kуп? </ta>
            <ta e="T387" id="Seg_2439" s="T379">lа̄ мат ′мыкап чап ′татынтап, тинд̂ена мыка ′ӱртентап.</ta>
            <ta e="T390" id="Seg_2440" s="T387">′мыкамы тап ′пентап. </ta>
            <ta e="T391" id="Seg_2441" s="T390">kо.</ta>
            <ta e="T395" id="Seg_2442" s="T391">′ӣма′kота‵kанты мо̄т шʼе̄рашʼ, ′аммерашʼ.</ta>
            <ta e="T398" id="Seg_2443" s="T395">нимто ′найта.</ta>
            <ta e="T402" id="Seg_2444" s="T398">Маленький умища старик-то.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_2445" s="T0">istarik pervɨj voda ẑamerznet. </ta>
            <ta e="T11" id="Seg_2446" s="T4">qatamolʼ üːtɨp ammejit ira qoralʼe [qəlɨlʼe] qəːnpa.</ta>
            <ta e="T25" id="Seg_2447" s="T11">qolʼtɨp kušat orqälʼolʼnɨt irra qoltit qanɨŋmɨt nʼanna mantelʼolʼpɨlʼä. kurɨnna suːrɨtqo täto mantalʼpelʼa qəəə.</ta>
            <ta e="T31" id="Seg_2448" s="T25">ukkur čontoqɨt täp qos qaj orɨča.</ta>
            <ta e="T37" id="Seg_2449" s="T31">qaj qup lʼaŋ[k]ɨča, qaj, qaj orɨča.</ta>
            <ta e="T43" id="Seg_2450" s="T37">täp qaj qup lʼi qaj lʼaŋkɨča. </ta>
            <ta e="T47" id="Seg_2451" s="T43">ira nik na ündɨčit.</ta>
            <ta e="T49" id="Seg_2452" s="T47">nɨna tüːnta.</ta>
            <ta e="T50" id="Seg_2453" s="T49">qəəə. </ta>
            <ta e="T55" id="Seg_2454" s="T50">totčo nʼempoqɨt, qajel lʼaka eːsʼa?</ta>
            <ta e="T60" id="Seg_2455" s="T55">nempoqɨt mɨta qajilʼ lʼaqa qondɨlʼča.</ta>
            <ta e="T67" id="Seg_2456" s="T60">ɨnnä čapke qatɨtolʼka. aj illa qənneja.</ta>
            <ta e="T84" id="Seg_2457" s="T78">pošalusta lʼaŋɨnʼa qoːtä ɨnnä šik ijaš.</ta>
            <ta e="T89" id="Seg_2458" s="T84">timtena irra konna pakta.</ta>
            <ta e="T92" id="Seg_2459" s="T89">čumpɨ nʼarqɨp iːŋɨtɨ.</ta>
            <ta e="T95" id="Seg_2460" s="T92">čumpɨ nʼarqɨp patčalʼtɨt.</ta>
            <ta e="T100" id="Seg_2461" s="T95">qarre ittɨtɨ (ittätɨ, čattetɨ) irätkine.</ta>
            <ta e="T105" id="Seg_2462" s="T100">nʼa nʼarqɨsä konnä ɨnna üːkɨlʼeŋɨta.</ta>
            <ta e="T108" id="Seg_2463" s="T105">konna čap üːkɨlʼeŋat.</ta>
            <ta e="T110" id="Seg_2464" s="T108">qoː, qoː.</ta>
            <ta e="T116" id="Seg_2465" s="T110">monte (kupec) tem qup na tä[ə]tta.</ta>
            <ta e="T126" id="Seg_2466" s="T116">tat maːšip innä šik iːŋantɨ, kurmonɨnonɨ, innä ši iŋantɨ.</ta>
            <ta e="T129" id="Seg_2467" s="T126">innä ši iŋantɨ.</ta>
            <ta e="T132" id="Seg_2468" s="T129">solʼotalʼ kusoksä miːŋatɨ.</ta>
            <ta e="T138" id="Seg_2469" s="T132">miːtɨ kurmonɨ nonɨ ɨnnä šiŋ iŋäntɨ.</ta>
            <ta e="T143" id="Seg_2470" s="T138">qaj iːtalʼ qaj ilʼtälʼä iːtalʼ?</ta>
            <ta e="T145" id="Seg_2471" s="T143">na qəntanəəə.</ta>
            <ta e="T156" id="Seg_2472" s="T145">lʼa čuntɨ ukkur ruš na montɨ čuntɨp toqqɨmpatə, moːrətɨ aša ata.</ta>
            <ta e="T158" id="Seg_2473" s="T156">nɨna tünta.</ta>
            <ta e="T162" id="Seg_2474" s="T158">lʼa tänätɨ niːk kolʼlʼimonna.</ta>
            <ta e="T168" id="Seg_2475" s="T162">qotɨ mompa čuntɨp iːsamen uːčikonoqo.</ta>
            <ta e="T174" id="Seg_2476" s="T168">čuntɨ iːntetɨ, solʼotalʼ kusoktɨ to milʼčiŋɨt.</ta>
            <ta e="T180" id="Seg_2477" s="T174">nɨnɨ na čunnentɨsa qəːnna nəəə, qənna, qənna.</ta>
            <ta e="T190" id="Seg_2478" s="T180">ukkur čontoqɨt ruš namɨt montɨ sɨrɨtɨp toqɨmpat, morətɨ aša aːta.</ta>
            <ta e="T194" id="Seg_2479" s="T190">lʼa tänɨtɨ niːk kolʼlʼimonna.</ta>
            <ta e="T199" id="Seg_2480" s="T194">molʼokontoqo [nimantoqo] sɨːrəp iːsamena.</ta>
            <ta e="T204" id="Seg_2481" s="T199">čud̂nɨmtɨ sɨrɨčotɨ tomilʼčit.</ta>
            <ta e="T209" id="Seg_2482" s="T204">na sɨrɨntɨ sena qəntana, qəntanəəə.</ta>
            <ta e="T219" id="Seg_2483" s="T209">qoː, ukkur čontoqɨt sɨpɨnčatɨp na montɨ toqqɨmpotɨt, morɨtɨ aša aːta.</ta>
            <ta e="T228" id="Seg_2484" s="T219">nɨnɨ niː qəːtɨtɨ, lʼaː sɨpɨnča namɨt monta üːrɨŋeːŋa.</ta>
            <ta e="T236" id="Seg_2485" s="T228">lʼa mɨta sɨpɨnča žirnaq eŋa, sɨpɨnčap iːsamena.</ta>
            <ta e="T242" id="Seg_2486" s="T236">sɨpɨnčat čotɨ to miŋɨ na sɨrɨmtɨ.</ta>
            <ta e="T244" id="Seg_2487" s="T242">aj menяlsя.</ta>
            <ta e="T251" id="Seg_2488" s="T244">nɨnɨ na sɨpɨnčantɨsa na qənta, na qənta nəəə.</ta>
            <ta e="T259" id="Seg_2489" s="T251">ukkur tät čontoqɨt na ruːš qətɨpɨlʼ čiŋkɨtɨp toqqɨmpa.</ta>
            <ta e="T264" id="Seg_2490" s="T259">činkɨtɨm namɨšek toqqɨmpat moːrətə čeŋka.</ta>
            <ta e="T274" id="Seg_2491" s="T264">qotɨ яjcatɨ to mnogo, činkɨp iːsameːna (eːŋtoqo) [eŋtɨ čotɨ].</ta>
            <ta e="T279" id="Seg_2492" s="T274">čiŋkɨt čotɨ sɨpɨnčamtɨ tomilʼčitɨ.</ta>
            <ta e="T286" id="Seg_2493" s="T279">nɨnɨ na čiŋkɨntɨsa na qənna, qənna, moqɨnä.</ta>
            <ta e="T297" id="Seg_2494" s="T286">ukkur čontoːt [toqɨt] monte ruːš təːkotɨp namɨšak tokɨmpat more[ə]tə aša aːta.</ta>
            <ta e="T301" id="Seg_2495" s="T297">qota təkop iːsameːna.</ta>
            <ta e="T307" id="Seg_2496" s="T301">na čiŋɨmtɨ təkot čoːtɨ to miːlʼčiŋɨt.</ta>
            <ta e="T311" id="Seg_2497" s="T307">nɨnɨ təkontosä qənna nəəə.</ta>
            <ta e="T321" id="Seg_2498" s="T311">ukkɨr iːralʼ lʼiːpɨ monpa tintena namɨt kʼeːpɨlʼ [kʼeːpilʼ] mɨkatɨ tüːrɨŋpatɨ.</ta>
            <ta e="T324" id="Seg_2499" s="T321">mɨka aj mɨka.</ta>
            <ta e="T328" id="Seg_2500" s="T324">lʼa tänatɨ nikolʼimonna.</ta>
            <ta e="T332" id="Seg_2501" s="T328">lʼaː sajeqɨtɨlʼ lʼosoŋok lʼaka.</ta>
            <ta e="T343" id="Seg_2502" s="T332">qajemap šʼutqo, qaim šʼutqo qotɨ na irat mɨkap iːsameːŋa.</ta>
            <ta e="T352" id="Seg_2503" s="T343">tindena mɨka čotɨ to miːŋɨtɨ təkotɨ miːlʼčitɨ mɨkat čoːtɨ.</ta>
            <ta e="T357" id="Seg_2504" s="T352">nɨnɨ na mɨkantɨsä qənna qənna.</ta>
            <ta e="T359" id="Seg_2505" s="T357">mɨka ürrejsa.</ta>
            <ta e="T366" id="Seg_2506" s="T359">qət ši moːttɨ čap šeːrna, mɨka urresʼa.</ta>
            <ta e="T372" id="Seg_2507" s="T366">na qəːt šinčoqɨt na peːŋɨtɨ, püntɨrlʼe.</ta>
            <ta e="T376" id="Seg_2508" s="T372">tat qaj peːŋalʼ irra?</ta>
            <ta e="T379" id="Seg_2509" s="T376">tolʼ motalʼ qup? </ta>
            <ta e="T387" id="Seg_2510" s="T379">lʼaː mat mɨkap čap tatɨntap, tind̂ena mɨka ürtentap.</ta>
            <ta e="T390" id="Seg_2511" s="T387">mɨkamɨ tap pentap.</ta>
            <ta e="T391" id="Seg_2512" s="T390">qo. </ta>
            <ta e="T395" id="Seg_2513" s="T391">iːmaqotaqantɨ moːt šeːraš, ammeraš.</ta>
            <ta e="T398" id="Seg_2514" s="T395">nimto najta.</ta>
            <ta e="T402" id="Seg_2515" s="T398">Malenʼkij umišʼa starik to. </ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_2516" s="T0">Istarik pervɨj voda zamerznet. </ta>
            <ta e="T11" id="Seg_2517" s="T4">Qatamol ütɨp ammɛːjit, ira koralʼä (/qəːlɨlʼlʼä) qənpa. </ta>
            <ta e="T25" id="Seg_2518" s="T11">Qoltɨp kušat orqälɔːlnɨt, irra qoltit qanɨŋmɨt nʼanna mantelɔːlpɨlʼä kurɨna suːrɨtqo, tä toː mantalpelʼa qəəə. </ta>
            <ta e="T31" id="Seg_2519" s="T25">Ukkur čʼontoːqɨt täp qos qaj orɨčʼčʼa. </ta>
            <ta e="T37" id="Seg_2520" s="T31">Qaj qup laŋkɨčʼčʼa, qaj qaj orɨčʼčʼa. </ta>
            <ta e="T43" id="Seg_2521" s="T37">Täp qaj, qup lʼi, qaj laŋkɨčʼčʼa? </ta>
            <ta e="T47" id="Seg_2522" s="T43">Ira nık na üntɨčʼit. </ta>
            <ta e="T49" id="Seg_2523" s="T47">Nɨːna tünta. </ta>
            <ta e="T50" id="Seg_2524" s="T49">Qəəə! </ta>
            <ta e="T55" id="Seg_2525" s="T50">Točʼčʼo nʼempoːqɨt, qajel laka ɛːsʼa? </ta>
            <ta e="T60" id="Seg_2526" s="T55">Nempoːqɨt mɨta qajilʼ laka qontɨlʼčʼa. </ta>
            <ta e="T67" id="Seg_2527" s="T60">İnnä čʼap kɛ qatɨtɔːlka, aj ılla qənnɛːja. </ta>
            <ta e="T70" id="Seg_2528" s="T67">Aj ılla qənnɛːja. </ta>
            <ta e="T78" id="Seg_2529" s="T70">Načʼä čʼap qənna montɨ qup, üttɨ patqɨlpa, nʼempontɨ. </ta>
            <ta e="T84" id="Seg_2530" s="T78">Pošalusta,— lʼaŋɨnʼnʼa,— qɔːtä ınnä šık iːjaš. </ta>
            <ta e="T89" id="Seg_2531" s="T84">Timte na irra konna pakta. </ta>
            <ta e="T92" id="Seg_2532" s="T89">Čʼumpɨ nʼarqɨp iːŋɨtɨ. </ta>
            <ta e="T95" id="Seg_2533" s="T92">Čʼumpɨ nʼarqɨp pačʼčʼaltɨt. </ta>
            <ta e="T100" id="Seg_2534" s="T95">Karrä iːttɨtɨ (/iːttɛːtɨ, čʼattɛːtɨ) irätkine. </ta>
            <ta e="T105" id="Seg_2535" s="T100">Nʼa nʼarqɨsä konnä ınna üːqɨlʼlʼɛːŋɨta. </ta>
            <ta e="T108" id="Seg_2536" s="T105">Konna čʼap üːqɨlʼlʼɛːŋat. </ta>
            <ta e="T110" id="Seg_2537" s="T108">Qo, qo! </ta>
            <ta e="T116" id="Seg_2538" s="T110">Monte temqup (/kupec), kupec na tətta. </ta>
            <ta e="T126" id="Seg_2539" s="T116">Tat mašıp ınnä šık iːŋantɨ, qurmon nɔːnɨ, ınnä šı iːŋantɨ. </ta>
            <ta e="T129" id="Seg_2540" s="T126">İnnä šı iːŋantɨ. </ta>
            <ta e="T132" id="Seg_2541" s="T129">Solotalʼ kusoksä miːŋatɨ. </ta>
            <ta e="T138" id="Seg_2542" s="T132">Mitɨ qurmon nɔːnɨ ınnä šıŋ iːŋäntɨ. </ta>
            <ta e="T143" id="Seg_2543" s="T138">Qaj iːtal, qaj iltäːlʼ iːtal? </ta>
            <ta e="T145" id="Seg_2544" s="T143">Na qəntanəəə. </ta>
            <ta e="T156" id="Seg_2545" s="T145">Lʼa, čʼuntɨ, ukkur ruš na montɨ čʼuntɨp tɔːqqɨmpatə, moːrətɨ aša ata. </ta>
            <ta e="T158" id="Seg_2546" s="T156">Nɨːna tünta. </ta>
            <ta e="T162" id="Seg_2547" s="T158">Lʼa, tɛnätɨ nık kollimɔːnna. </ta>
            <ta e="T168" id="Seg_2548" s="T162">Qɔːtɨ mompa čʼuntɨp iːsam ɛn uːčʼiqonoːqo. </ta>
            <ta e="T174" id="Seg_2549" s="T168">Čʼuntɨ iːntetɨ, solotalʼ kusoktɨ toː milʼčʼiŋɨt. </ta>
            <ta e="T180" id="Seg_2550" s="T174">Nɨːnɨ na čʼunnentɨsa qənnanəəə, qənna, qənna. </ta>
            <ta e="T190" id="Seg_2551" s="T180">Ukkur čʼontoːqɨt ruš namɨt montɨ sɨːrɨtɨp tɔːqqɨmpat, moːrətɨ aša ata. </ta>
            <ta e="T194" id="Seg_2552" s="T190">Lʼa, tɛnɨtɨ nık kollimɔːnna. </ta>
            <ta e="T199" id="Seg_2553" s="T194">Molokontoːqo (/nʼimantoːqo) sɨːrəp iːsam ɛna. </ta>
            <ta e="T204" id="Seg_2554" s="T199">Čʼuntɨmtɨ sɨːrɨ čʼɔːtɨ toː milʼčʼit. </ta>
            <ta e="T209" id="Seg_2555" s="T204">Na sɨːrɨntɨsä na qəntana, qəntanəəə. </ta>
            <ta e="T219" id="Seg_2556" s="T209">Qo, ukkur čʼontoːqɨt sɨpɨnčʼatɨp na montɨ tɔːqqɨmpɔːtɨt, moːrɨtɨ aša ata. </ta>
            <ta e="T228" id="Seg_2557" s="T219">Nɨːnɨ nıː kətɨtɨ, lʼa sɨpɨnčʼa namɨt monta ürɨŋ ɛːŋa. </ta>
            <ta e="T236" id="Seg_2558" s="T228">Lʼa mɨta sɨpɨnčʼa žirnak ɛːŋa, sɨpɨnčʼap iːsam ɛna. </ta>
            <ta e="T242" id="Seg_2559" s="T236">Sɨpɨnčʼat čʼɔːtɨ toː miŋɨtɨ na sɨːrɨmtɨ. </ta>
            <ta e="T244" id="Seg_2560" s="T242">Aj menʼalsʼa. </ta>
            <ta e="T251" id="Seg_2561" s="T244">Nɨːnɨ na sɨpɨnčʼantɨsa na qənta, na qəntanəəə. </ta>
            <ta e="T259" id="Seg_2562" s="T251">Ukkur tät čʼontoːqɨt na ruš kəːtɨpɨlʼ čʼıŋkɨtɨp tɔːqqɨmpa. </ta>
            <ta e="T264" id="Seg_2563" s="T259">Čʼıŋkɨtɨm namɨšek tɔːqqɨmpat, moːrətə čʼäːŋka. </ta>
            <ta e="T274" id="Seg_2564" s="T264">Qɔːtɨ jajcatɨ to mnogo, čʼıŋkɨp iːsam ɛna (ɛŋtoːqo) (/ɛŋtɨ čʼɔːtɨ). </ta>
            <ta e="T279" id="Seg_2565" s="T274">Čʼıŋkɨt čʼɔːtɨ sɨpɨnčʼamtɨ toː milʼčʼitɨ. </ta>
            <ta e="T286" id="Seg_2566" s="T279">Nɨːnɨ na čʼıŋkɨntɨsa na qənna, qənna moqɨnä. </ta>
            <ta e="T297" id="Seg_2567" s="T286">Ukkur čʼontot (/čʼontoːqɨt) monte ruš töːkotɨp namɨšak tɔːqqɨmpat, moːretə aša ata. </ta>
            <ta e="T301" id="Seg_2568" s="T297">Qɔːta töːkop iːsam ɛna. </ta>
            <ta e="T307" id="Seg_2569" s="T301">Na čʼıŋkɨmtɨ töːkot čʼɔːtɨ toː milʼčʼiŋɨt. </ta>
            <ta e="T311" id="Seg_2570" s="T307">Nɨːnɨ töːkontosä qənna, qənnanəəə. </ta>
            <ta e="T321" id="Seg_2571" s="T311">Ukkɨr iːralʼ lıːpɨ monpa tintena namɨt keːpɨlʼ (/keːpilʼ) mɨkatɨ türɨŋpatɨ. </ta>
            <ta e="T324" id="Seg_2572" s="T321">Mɨka aj mɨka. </ta>
            <ta e="T328" id="Seg_2573" s="T324">Lʼa, tɛnatɨ nı kollimɔːnna. </ta>
            <ta e="T332" id="Seg_2574" s="T328">Lʼa, sajekɨtɨlʼ loːsoŋɔːk lʼaqa. </ta>
            <ta e="T343" id="Seg_2575" s="T332">Qajem map šütqo, qaim šütqo qɔːtɨ na irat mɨkap iːsam ɛŋa. </ta>
            <ta e="T352" id="Seg_2576" s="T343">Tintena mɨka čʼɔːtɨ toː miŋɨtɨ töːkotɨ, milʼčʼitɨ mɨkat čʼɔːtɨ. </ta>
            <ta e="T357" id="Seg_2577" s="T352">Nɨːnɨ na mɨkantɨsä qənna, qənna. </ta>
            <ta e="T359" id="Seg_2578" s="T357">Mɨka ürrɛːjsa. </ta>
            <ta e="T366" id="Seg_2579" s="T359">Qət ši mɔːttɨ čʼap šeːrna, mɨka urrɛːsʼa. </ta>
            <ta e="T372" id="Seg_2580" s="T366">Na qəːt šinčʼoːqɨt na peːŋɨtɨ, püntɨrlʼä. </ta>
            <ta e="T376" id="Seg_2581" s="T372">Tat qaj peːŋal, irra? </ta>
            <ta e="T379" id="Seg_2582" s="T376">—tɔːlʼ mɔːtalʼ qup. </ta>
            <ta e="T387" id="Seg_2583" s="T379">Lʼa, mat mɨkap čʼap tatɨntap, tintena mɨka ürtentap. </ta>
            <ta e="T390" id="Seg_2584" s="T387">Mɨkamɨ tap peːntap. </ta>
            <ta e="T391" id="Seg_2585" s="T390">Qo! </ta>
            <ta e="T395" id="Seg_2586" s="T391">Imaqotaqantɨ mɔːt šeːraš, ameraš. </ta>
            <ta e="T398" id="Seg_2587" s="T395">Nimto na jta. </ta>
            <ta e="T402" id="Seg_2588" s="T398">Malenʼkij umišʼa starik to. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2589" s="T0">istarik</ta>
            <ta e="T2" id="Seg_2590" s="T1">pervɨj</ta>
            <ta e="T3" id="Seg_2591" s="T2">voda</ta>
            <ta e="T4" id="Seg_2592" s="T3">zamerznet</ta>
            <ta e="T5" id="Seg_2593" s="T4">qatamol</ta>
            <ta e="T6" id="Seg_2594" s="T5">üt-ɨ-p</ta>
            <ta e="T7" id="Seg_2595" s="T6">amm-ɛː-ji-t</ta>
            <ta e="T8" id="Seg_2596" s="T7">ira</ta>
            <ta e="T9" id="Seg_2597" s="T8">kora-lʼä</ta>
            <ta e="T10" id="Seg_2598" s="T9">qəːlɨ-lʼ-lʼä</ta>
            <ta e="T11" id="Seg_2599" s="T10">qən-pa</ta>
            <ta e="T12" id="Seg_2600" s="T11">qoltɨ-p</ta>
            <ta e="T13" id="Seg_2601" s="T12">kušat</ta>
            <ta e="T14" id="Seg_2602" s="T13">orqäl-ɔːl-nɨ-t</ta>
            <ta e="T15" id="Seg_2603" s="T14">irra</ta>
            <ta e="T16" id="Seg_2604" s="T15">qolti-t</ta>
            <ta e="T17" id="Seg_2605" s="T16">qanɨŋ-mɨt</ta>
            <ta e="T18" id="Seg_2606" s="T17">nʼanna</ta>
            <ta e="T19" id="Seg_2607" s="T18">mante-lɔːl-pɨ-lʼä</ta>
            <ta e="T20" id="Seg_2608" s="T19">kurɨ-na</ta>
            <ta e="T21" id="Seg_2609" s="T20">suːrɨ-tqo</ta>
            <ta e="T22" id="Seg_2610" s="T21">tä</ta>
            <ta e="T23" id="Seg_2611" s="T22">toː</ta>
            <ta e="T24" id="Seg_2612" s="T23">mant-al-pe-lʼa</ta>
            <ta e="T25" id="Seg_2613" s="T24">qəəə</ta>
            <ta e="T26" id="Seg_2614" s="T25">ukkur</ta>
            <ta e="T27" id="Seg_2615" s="T26">čʼontoː-qɨt</ta>
            <ta e="T28" id="Seg_2616" s="T27">täp</ta>
            <ta e="T29" id="Seg_2617" s="T28">qos</ta>
            <ta e="T30" id="Seg_2618" s="T29">qaj</ta>
            <ta e="T31" id="Seg_2619" s="T30">orɨ-čʼ-čʼa</ta>
            <ta e="T32" id="Seg_2620" s="T31">qaj</ta>
            <ta e="T33" id="Seg_2621" s="T32">qup</ta>
            <ta e="T34" id="Seg_2622" s="T33">laŋkɨ-čʼ-čʼa</ta>
            <ta e="T35" id="Seg_2623" s="T34">qaj</ta>
            <ta e="T36" id="Seg_2624" s="T35">qaj</ta>
            <ta e="T37" id="Seg_2625" s="T36">orɨ-čʼ-čʼa</ta>
            <ta e="T38" id="Seg_2626" s="T37">täp</ta>
            <ta e="T39" id="Seg_2627" s="T38">qaj</ta>
            <ta e="T40" id="Seg_2628" s="T39">qup</ta>
            <ta e="T41" id="Seg_2629" s="T40">lʼi</ta>
            <ta e="T42" id="Seg_2630" s="T41">qaj</ta>
            <ta e="T43" id="Seg_2631" s="T42">laŋkɨ-čʼ-čʼa</ta>
            <ta e="T44" id="Seg_2632" s="T43">ira</ta>
            <ta e="T45" id="Seg_2633" s="T44">nık</ta>
            <ta e="T46" id="Seg_2634" s="T45">na</ta>
            <ta e="T47" id="Seg_2635" s="T46">üntɨ-čʼi-t</ta>
            <ta e="T48" id="Seg_2636" s="T47">nɨːna</ta>
            <ta e="T49" id="Seg_2637" s="T48">tü-nta</ta>
            <ta e="T50" id="Seg_2638" s="T49">qəəə</ta>
            <ta e="T51" id="Seg_2639" s="T50">točʼčʼo</ta>
            <ta e="T52" id="Seg_2640" s="T51">nʼempoː-qɨt</ta>
            <ta e="T53" id="Seg_2641" s="T52">qaj-e-l</ta>
            <ta e="T54" id="Seg_2642" s="T53">laka</ta>
            <ta e="T55" id="Seg_2643" s="T54">ɛː-sʼa</ta>
            <ta e="T56" id="Seg_2644" s="T55">nempoː-qɨt</ta>
            <ta e="T57" id="Seg_2645" s="T56">mɨta</ta>
            <ta e="T58" id="Seg_2646" s="T57">qaj-i-lʼ</ta>
            <ta e="T59" id="Seg_2647" s="T58">laka</ta>
            <ta e="T60" id="Seg_2648" s="T59">qontɨ-lʼčʼa</ta>
            <ta e="T61" id="Seg_2649" s="T60">i̇nnä</ta>
            <ta e="T62" id="Seg_2650" s="T61">čʼap</ta>
            <ta e="T63" id="Seg_2651" s="T62">kɛ</ta>
            <ta e="T64" id="Seg_2652" s="T63">qatɨt-ɔːl-ka</ta>
            <ta e="T65" id="Seg_2653" s="T64">aj</ta>
            <ta e="T66" id="Seg_2654" s="T65">ılla</ta>
            <ta e="T67" id="Seg_2655" s="T66">qənn-ɛː-ja</ta>
            <ta e="T68" id="Seg_2656" s="T67">aj</ta>
            <ta e="T69" id="Seg_2657" s="T68">ılla</ta>
            <ta e="T70" id="Seg_2658" s="T69">qənn-ɛː-ja</ta>
            <ta e="T71" id="Seg_2659" s="T70">načʼä</ta>
            <ta e="T72" id="Seg_2660" s="T71">čʼap</ta>
            <ta e="T73" id="Seg_2661" s="T72">qən-na</ta>
            <ta e="T74" id="Seg_2662" s="T73">montɨ</ta>
            <ta e="T75" id="Seg_2663" s="T74">qup</ta>
            <ta e="T76" id="Seg_2664" s="T75">üt-tɨ</ta>
            <ta e="T77" id="Seg_2665" s="T76">pat-qɨl-pa</ta>
            <ta e="T78" id="Seg_2666" s="T77">nʼempo-ntɨ</ta>
            <ta e="T79" id="Seg_2667" s="T78">pošalusta</ta>
            <ta e="T80" id="Seg_2668" s="T79">lʼaŋɨ-nʼ-nʼa</ta>
            <ta e="T81" id="Seg_2669" s="T80">qɔːtä</ta>
            <ta e="T82" id="Seg_2670" s="T81">ınnä</ta>
            <ta e="T83" id="Seg_2671" s="T82">šık</ta>
            <ta e="T84" id="Seg_2672" s="T83">iː-j-aš</ta>
            <ta e="T85" id="Seg_2673" s="T84">timte</ta>
            <ta e="T86" id="Seg_2674" s="T85">na</ta>
            <ta e="T87" id="Seg_2675" s="T86">irra</ta>
            <ta e="T88" id="Seg_2676" s="T87">konna</ta>
            <ta e="T89" id="Seg_2677" s="T88">pakta</ta>
            <ta e="T90" id="Seg_2678" s="T89">čʼumpɨ</ta>
            <ta e="T91" id="Seg_2679" s="T90">nʼarqɨ-p</ta>
            <ta e="T92" id="Seg_2680" s="T91">iː-ŋɨ-tɨ</ta>
            <ta e="T93" id="Seg_2681" s="T92">čʼumpɨ</ta>
            <ta e="T94" id="Seg_2682" s="T93">nʼarqɨ-p</ta>
            <ta e="T95" id="Seg_2683" s="T94">pačʼčʼ-al-tɨ-t</ta>
            <ta e="T96" id="Seg_2684" s="T95">karrä</ta>
            <ta e="T97" id="Seg_2685" s="T96">iː-ttɨ-tɨ</ta>
            <ta e="T98" id="Seg_2686" s="T97">iː-tt-ɛː-tɨ</ta>
            <ta e="T99" id="Seg_2687" s="T98">čʼatt-ɛː-tɨ</ta>
            <ta e="T100" id="Seg_2688" s="T99">irä-tkine</ta>
            <ta e="T101" id="Seg_2689" s="T100">nʼa</ta>
            <ta e="T102" id="Seg_2690" s="T101">nʼarqɨ-sä</ta>
            <ta e="T103" id="Seg_2691" s="T102">konnä</ta>
            <ta e="T104" id="Seg_2692" s="T103">ınna</ta>
            <ta e="T105" id="Seg_2693" s="T104">üː-qɨlʼlʼ-ɛː-ŋɨ-ta</ta>
            <ta e="T106" id="Seg_2694" s="T105">konna</ta>
            <ta e="T107" id="Seg_2695" s="T106">čʼap</ta>
            <ta e="T108" id="Seg_2696" s="T107">üː-qɨlʼlʼ-ɛː-ŋa-t</ta>
            <ta e="T109" id="Seg_2697" s="T108">qo</ta>
            <ta e="T110" id="Seg_2698" s="T109">qo</ta>
            <ta e="T111" id="Seg_2699" s="T110">monte</ta>
            <ta e="T112" id="Seg_2700" s="T111">tem-qup</ta>
            <ta e="T113" id="Seg_2701" s="T112">kupec</ta>
            <ta e="T114" id="Seg_2702" s="T113">kupec</ta>
            <ta e="T115" id="Seg_2703" s="T114">na</ta>
            <ta e="T116" id="Seg_2704" s="T115">tətta</ta>
            <ta e="T117" id="Seg_2705" s="T116">tan</ta>
            <ta e="T118" id="Seg_2706" s="T117">mašıp</ta>
            <ta e="T119" id="Seg_2707" s="T118">ınnä</ta>
            <ta e="T120" id="Seg_2708" s="T119">šık</ta>
            <ta e="T121" id="Seg_2709" s="T120">iː-ŋa-ntɨ</ta>
            <ta e="T122" id="Seg_2710" s="T121">qu-r-mo-n</ta>
            <ta e="T123" id="Seg_2711" s="T122">nɔː-nɨ</ta>
            <ta e="T124" id="Seg_2712" s="T123">ınnä</ta>
            <ta e="T125" id="Seg_2713" s="T124">šı</ta>
            <ta e="T126" id="Seg_2714" s="T125">iː-ŋa-ntɨ</ta>
            <ta e="T127" id="Seg_2715" s="T126">i̇nnä</ta>
            <ta e="T128" id="Seg_2716" s="T127">šı</ta>
            <ta e="T129" id="Seg_2717" s="T128">iː-ŋa-ntɨ</ta>
            <ta e="T130" id="Seg_2718" s="T129">solota-lʼ</ta>
            <ta e="T131" id="Seg_2719" s="T130">kusok-sä</ta>
            <ta e="T132" id="Seg_2720" s="T131">miː-ŋa-tɨ</ta>
            <ta e="T133" id="Seg_2721" s="T132">mitɨ</ta>
            <ta e="T134" id="Seg_2722" s="T133">qu-r-mo-n</ta>
            <ta e="T135" id="Seg_2723" s="T134">nɔː-nɨ</ta>
            <ta e="T136" id="Seg_2724" s="T135">ınnä</ta>
            <ta e="T137" id="Seg_2725" s="T136">šıŋ</ta>
            <ta e="T138" id="Seg_2726" s="T137">iː-ŋä-ntɨ</ta>
            <ta e="T139" id="Seg_2727" s="T138">qaj</ta>
            <ta e="T140" id="Seg_2728" s="T139">iː-ta-l</ta>
            <ta e="T141" id="Seg_2729" s="T140">qaj</ta>
            <ta e="T142" id="Seg_2730" s="T141">il-täː-lʼ</ta>
            <ta e="T143" id="Seg_2731" s="T142">iː-ta-l</ta>
            <ta e="T144" id="Seg_2732" s="T143">na</ta>
            <ta e="T145" id="Seg_2733" s="T144">qən-ta-nəəə</ta>
            <ta e="T146" id="Seg_2734" s="T145">lʼa</ta>
            <ta e="T147" id="Seg_2735" s="T146">čʼuntɨ</ta>
            <ta e="T148" id="Seg_2736" s="T147">ukkur</ta>
            <ta e="T149" id="Seg_2737" s="T148">ruš</ta>
            <ta e="T150" id="Seg_2738" s="T149">na</ta>
            <ta e="T151" id="Seg_2739" s="T150">montɨ</ta>
            <ta e="T152" id="Seg_2740" s="T151">čʼuntɨ-p</ta>
            <ta e="T153" id="Seg_2741" s="T152">tɔːqqɨ-mpa-tə</ta>
            <ta e="T154" id="Seg_2742" s="T153">moːrə-tɨ</ta>
            <ta e="T155" id="Seg_2743" s="T154">aša</ta>
            <ta e="T156" id="Seg_2744" s="T155">ata</ta>
            <ta e="T157" id="Seg_2745" s="T156">nɨːna</ta>
            <ta e="T158" id="Seg_2746" s="T157">tü-nta</ta>
            <ta e="T159" id="Seg_2747" s="T158">lʼa</ta>
            <ta e="T160" id="Seg_2748" s="T159">tɛnä-tɨ</ta>
            <ta e="T161" id="Seg_2749" s="T160">nık</ta>
            <ta e="T162" id="Seg_2750" s="T161">kolli-mɔːn-na</ta>
            <ta e="T163" id="Seg_2751" s="T162">qɔːtɨ</ta>
            <ta e="T164" id="Seg_2752" s="T163">mompa</ta>
            <ta e="T165" id="Seg_2753" s="T164">čʼuntɨ-p</ta>
            <ta e="T166" id="Seg_2754" s="T165">iː-sa-m</ta>
            <ta e="T167" id="Seg_2755" s="T166">ɛn</ta>
            <ta e="T168" id="Seg_2756" s="T167">uːčʼi-qonoːqo</ta>
            <ta e="T169" id="Seg_2757" s="T168">čʼuntɨ</ta>
            <ta e="T170" id="Seg_2758" s="T169">iː-nte-tɨ</ta>
            <ta e="T171" id="Seg_2759" s="T170">solota-lʼ</ta>
            <ta e="T172" id="Seg_2760" s="T171">kusok-tɨ</ta>
            <ta e="T173" id="Seg_2761" s="T172">toː</ta>
            <ta e="T174" id="Seg_2762" s="T173">mi-lʼčʼi-ŋɨ-t</ta>
            <ta e="T175" id="Seg_2763" s="T174">nɨːnɨ</ta>
            <ta e="T176" id="Seg_2764" s="T175">na</ta>
            <ta e="T177" id="Seg_2765" s="T176">čʼunne-ntɨ-sa</ta>
            <ta e="T178" id="Seg_2766" s="T177">qən-na-nəəə</ta>
            <ta e="T179" id="Seg_2767" s="T178">qən-na</ta>
            <ta e="T180" id="Seg_2768" s="T179">qən-na</ta>
            <ta e="T181" id="Seg_2769" s="T180">ukkur</ta>
            <ta e="T182" id="Seg_2770" s="T181">čʼontoː-qɨt</ta>
            <ta e="T183" id="Seg_2771" s="T182">ruš</ta>
            <ta e="T184" id="Seg_2772" s="T183">namɨ-t</ta>
            <ta e="T185" id="Seg_2773" s="T184">montɨ</ta>
            <ta e="T186" id="Seg_2774" s="T185">sɨːrɨ-t-ɨ-p</ta>
            <ta e="T187" id="Seg_2775" s="T186">tɔːqqɨ-mpa-t</ta>
            <ta e="T188" id="Seg_2776" s="T187">moːrə-tɨ</ta>
            <ta e="T189" id="Seg_2777" s="T188">aša</ta>
            <ta e="T190" id="Seg_2778" s="T189">ata</ta>
            <ta e="T191" id="Seg_2779" s="T190">lʼa</ta>
            <ta e="T192" id="Seg_2780" s="T191">tɛnɨ-tɨ</ta>
            <ta e="T193" id="Seg_2781" s="T192">nık</ta>
            <ta e="T194" id="Seg_2782" s="T193">kolli-mɔːn-na</ta>
            <ta e="T195" id="Seg_2783" s="T194">moloko-ntoːqo</ta>
            <ta e="T196" id="Seg_2784" s="T195">nʼima-ntoːqo</ta>
            <ta e="T197" id="Seg_2785" s="T196">sɨːrə-p</ta>
            <ta e="T198" id="Seg_2786" s="T197">iː-sa-m</ta>
            <ta e="T199" id="Seg_2787" s="T198">ɛna</ta>
            <ta e="T200" id="Seg_2788" s="T199">čʼuntɨ-m-tɨ</ta>
            <ta e="T201" id="Seg_2789" s="T200">sɨːrɨ</ta>
            <ta e="T202" id="Seg_2790" s="T201">čʼɔːtɨ</ta>
            <ta e="T203" id="Seg_2791" s="T202">toː</ta>
            <ta e="T204" id="Seg_2792" s="T203">mi-lʼčʼi-t</ta>
            <ta e="T205" id="Seg_2793" s="T204">na</ta>
            <ta e="T206" id="Seg_2794" s="T205">sɨːrɨ-ntɨ-sä</ta>
            <ta e="T207" id="Seg_2795" s="T206">na</ta>
            <ta e="T208" id="Seg_2796" s="T207">qən-ta-na</ta>
            <ta e="T209" id="Seg_2797" s="T208">qən-ta-nəəə</ta>
            <ta e="T210" id="Seg_2798" s="T209">qo</ta>
            <ta e="T211" id="Seg_2799" s="T210">ukkur</ta>
            <ta e="T212" id="Seg_2800" s="T211">čʼontoː-qɨt</ta>
            <ta e="T213" id="Seg_2801" s="T212">sɨpɨnčʼa-t-ɨ-p</ta>
            <ta e="T214" id="Seg_2802" s="T213">na</ta>
            <ta e="T215" id="Seg_2803" s="T214">montɨ</ta>
            <ta e="T216" id="Seg_2804" s="T215">tɔːqqɨ-mpɔː-tɨt</ta>
            <ta e="T217" id="Seg_2805" s="T216">moːrɨ-tɨ</ta>
            <ta e="T218" id="Seg_2806" s="T217">aša</ta>
            <ta e="T219" id="Seg_2807" s="T218">ata</ta>
            <ta e="T220" id="Seg_2808" s="T219">nɨːnɨ</ta>
            <ta e="T221" id="Seg_2809" s="T220">nıː</ta>
            <ta e="T222" id="Seg_2810" s="T221">kətɨ-tɨ</ta>
            <ta e="T223" id="Seg_2811" s="T222">lʼa</ta>
            <ta e="T224" id="Seg_2812" s="T223">sɨpɨnčʼa</ta>
            <ta e="T225" id="Seg_2813" s="T224">namɨ-t</ta>
            <ta e="T226" id="Seg_2814" s="T225">monta</ta>
            <ta e="T227" id="Seg_2815" s="T226">ür-ɨ-ŋ</ta>
            <ta e="T228" id="Seg_2816" s="T227">ɛː-ŋa</ta>
            <ta e="T229" id="Seg_2817" s="T228">lʼa</ta>
            <ta e="T230" id="Seg_2818" s="T229">mɨta</ta>
            <ta e="T231" id="Seg_2819" s="T230">sɨpɨnčʼa</ta>
            <ta e="T232" id="Seg_2820" s="T231">žirna-k</ta>
            <ta e="T233" id="Seg_2821" s="T232">ɛː-ŋa</ta>
            <ta e="T234" id="Seg_2822" s="T233">sɨpɨnčʼa-p</ta>
            <ta e="T235" id="Seg_2823" s="T234">iː-sa-m</ta>
            <ta e="T236" id="Seg_2824" s="T235">ɛna</ta>
            <ta e="T237" id="Seg_2825" s="T236">sɨpɨnčʼa-t</ta>
            <ta e="T238" id="Seg_2826" s="T237">čʼɔːtɨ</ta>
            <ta e="T239" id="Seg_2827" s="T238">toː</ta>
            <ta e="T240" id="Seg_2828" s="T239">mi-ŋɨ-tɨ</ta>
            <ta e="T241" id="Seg_2829" s="T240">na</ta>
            <ta e="T242" id="Seg_2830" s="T241">sɨːrɨ-m-tɨ</ta>
            <ta e="T243" id="Seg_2831" s="T242">aj</ta>
            <ta e="T244" id="Seg_2832" s="T243">menʼalsʼa</ta>
            <ta e="T245" id="Seg_2833" s="T244">nɨːnɨ</ta>
            <ta e="T246" id="Seg_2834" s="T245">na</ta>
            <ta e="T247" id="Seg_2835" s="T246">sɨpɨnčʼa-ntɨ-sa</ta>
            <ta e="T248" id="Seg_2836" s="T247">na</ta>
            <ta e="T249" id="Seg_2837" s="T248">qən-ta</ta>
            <ta e="T250" id="Seg_2838" s="T249">na</ta>
            <ta e="T251" id="Seg_2839" s="T250">qən-ta-nəəə</ta>
            <ta e="T252" id="Seg_2840" s="T251">ukkur</ta>
            <ta e="T253" id="Seg_2841" s="T252">tät</ta>
            <ta e="T254" id="Seg_2842" s="T253">čʼontoː-qɨt</ta>
            <ta e="T255" id="Seg_2843" s="T254">na</ta>
            <ta e="T256" id="Seg_2844" s="T255">ruš</ta>
            <ta e="T257" id="Seg_2845" s="T256">kəːtɨ-pɨlʼ</ta>
            <ta e="T258" id="Seg_2846" s="T257">čʼıŋkɨ-t-ɨ-p</ta>
            <ta e="T259" id="Seg_2847" s="T258">tɔːqqɨ-mpa</ta>
            <ta e="T260" id="Seg_2848" s="T259">čʼıŋkɨ-t-ɨ-m</ta>
            <ta e="T261" id="Seg_2849" s="T260">namɨšek</ta>
            <ta e="T262" id="Seg_2850" s="T261">tɔːqqɨ-mpa-t</ta>
            <ta e="T263" id="Seg_2851" s="T262">moːrə-tə</ta>
            <ta e="T264" id="Seg_2852" s="T263">čʼäːŋka</ta>
            <ta e="T265" id="Seg_2853" s="T264">qɔːtɨ</ta>
            <ta e="T266" id="Seg_2854" s="T265">jajca-tɨ</ta>
            <ta e="T267" id="Seg_2855" s="T266">to</ta>
            <ta e="T268" id="Seg_2856" s="T267">mnogo</ta>
            <ta e="T269" id="Seg_2857" s="T268">čʼıŋkɨ-p</ta>
            <ta e="T270" id="Seg_2858" s="T269">iː-sa-m</ta>
            <ta e="T271" id="Seg_2859" s="T270">ɛna</ta>
            <ta e="T272" id="Seg_2860" s="T271">ɛŋ-toːqo</ta>
            <ta e="T273" id="Seg_2861" s="T272">ɛŋ-tɨ</ta>
            <ta e="T274" id="Seg_2862" s="T273">čʼɔːtɨ</ta>
            <ta e="T275" id="Seg_2863" s="T274">čʼıŋkɨ-t</ta>
            <ta e="T276" id="Seg_2864" s="T275">čʼɔːtɨ</ta>
            <ta e="T277" id="Seg_2865" s="T276">sɨpɨnčʼa-m-tɨ</ta>
            <ta e="T278" id="Seg_2866" s="T277">toː</ta>
            <ta e="T279" id="Seg_2867" s="T278">mi-lʼčʼi-tɨ</ta>
            <ta e="T280" id="Seg_2868" s="T279">nɨːnɨ</ta>
            <ta e="T281" id="Seg_2869" s="T280">na</ta>
            <ta e="T282" id="Seg_2870" s="T281">čʼıŋkɨ-ntɨ-sa</ta>
            <ta e="T283" id="Seg_2871" s="T282">na</ta>
            <ta e="T284" id="Seg_2872" s="T283">qən-na</ta>
            <ta e="T285" id="Seg_2873" s="T284">qən-na</ta>
            <ta e="T286" id="Seg_2874" s="T285">moqɨnä</ta>
            <ta e="T287" id="Seg_2875" s="T286">ukkur</ta>
            <ta e="T288" id="Seg_2876" s="T287">čʼonto-t</ta>
            <ta e="T289" id="Seg_2877" s="T288">čʼontoː-qɨt</ta>
            <ta e="T290" id="Seg_2878" s="T289">monte</ta>
            <ta e="T291" id="Seg_2879" s="T290">ruš</ta>
            <ta e="T292" id="Seg_2880" s="T291">töːko-t-ɨ-p</ta>
            <ta e="T293" id="Seg_2881" s="T292">namɨšak</ta>
            <ta e="T294" id="Seg_2882" s="T293">tɔːqqɨ-mpa-t</ta>
            <ta e="T295" id="Seg_2883" s="T294">moːre-tə</ta>
            <ta e="T296" id="Seg_2884" s="T295">aša</ta>
            <ta e="T297" id="Seg_2885" s="T296">ata</ta>
            <ta e="T298" id="Seg_2886" s="T297">qɔːta</ta>
            <ta e="T299" id="Seg_2887" s="T298">töːko-p</ta>
            <ta e="T300" id="Seg_2888" s="T299">iː-sa-m</ta>
            <ta e="T301" id="Seg_2889" s="T300">ɛna</ta>
            <ta e="T302" id="Seg_2890" s="T301">na</ta>
            <ta e="T303" id="Seg_2891" s="T302">čʼıŋkɨ-m-tɨ</ta>
            <ta e="T304" id="Seg_2892" s="T303">töːko-t</ta>
            <ta e="T305" id="Seg_2893" s="T304">čʼɔːtɨ</ta>
            <ta e="T306" id="Seg_2894" s="T305">toː</ta>
            <ta e="T307" id="Seg_2895" s="T306">mi-lʼčʼi-ŋɨ-t</ta>
            <ta e="T308" id="Seg_2896" s="T307">nɨːnɨ</ta>
            <ta e="T309" id="Seg_2897" s="T308">töːko-nto-sä</ta>
            <ta e="T310" id="Seg_2898" s="T309">qən-na</ta>
            <ta e="T311" id="Seg_2899" s="T310">qən-na-nəəə</ta>
            <ta e="T312" id="Seg_2900" s="T311">ukkɨr</ta>
            <ta e="T313" id="Seg_2901" s="T312">iːra-lʼ</ta>
            <ta e="T314" id="Seg_2902" s="T313">lıːpɨ</ta>
            <ta e="T315" id="Seg_2903" s="T314">monpa</ta>
            <ta e="T316" id="Seg_2904" s="T315">tintena</ta>
            <ta e="T317" id="Seg_2905" s="T316">namɨ-t</ta>
            <ta e="T318" id="Seg_2906" s="T317">keːpɨ-lʼ</ta>
            <ta e="T319" id="Seg_2907" s="T318">keːpi-lʼ</ta>
            <ta e="T320" id="Seg_2908" s="T319">mɨka-tɨ</ta>
            <ta e="T321" id="Seg_2909" s="T320">türɨ-ŋ-pa-tɨ</ta>
            <ta e="T322" id="Seg_2910" s="T321">mɨka</ta>
            <ta e="T323" id="Seg_2911" s="T322">aj</ta>
            <ta e="T324" id="Seg_2912" s="T323">mɨka</ta>
            <ta e="T325" id="Seg_2913" s="T324">lʼa</ta>
            <ta e="T326" id="Seg_2914" s="T325">tɛna-tɨ</ta>
            <ta e="T327" id="Seg_2915" s="T326">nı</ta>
            <ta e="T328" id="Seg_2916" s="T327">kolli-mɔːn-na</ta>
            <ta e="T329" id="Seg_2917" s="T328">lʼa</ta>
            <ta e="T330" id="Seg_2918" s="T329">saje-kɨtɨ-lʼ</ta>
            <ta e="T331" id="Seg_2919" s="T330">loːs-o-ŋɔː-k</ta>
            <ta e="T332" id="Seg_2920" s="T331">lʼaqa</ta>
            <ta e="T333" id="Seg_2921" s="T332">qaj-e-m</ta>
            <ta e="T334" id="Seg_2922" s="T333">map</ta>
            <ta e="T335" id="Seg_2923" s="T334">šüt-qo</ta>
            <ta e="T336" id="Seg_2924" s="T335">qai-m</ta>
            <ta e="T337" id="Seg_2925" s="T336">šüt-qo</ta>
            <ta e="T338" id="Seg_2926" s="T337">qɔːtɨ</ta>
            <ta e="T339" id="Seg_2927" s="T338">na</ta>
            <ta e="T340" id="Seg_2928" s="T339">ira-t</ta>
            <ta e="T341" id="Seg_2929" s="T340">mɨka-p</ta>
            <ta e="T342" id="Seg_2930" s="T341">iː-sa-m</ta>
            <ta e="T343" id="Seg_2931" s="T342">ɛŋa</ta>
            <ta e="T344" id="Seg_2932" s="T343">tintena</ta>
            <ta e="T345" id="Seg_2933" s="T344">mɨka</ta>
            <ta e="T346" id="Seg_2934" s="T345">čʼɔːtɨ</ta>
            <ta e="T347" id="Seg_2935" s="T346">toː</ta>
            <ta e="T348" id="Seg_2936" s="T347">mi-ŋɨ-tɨ</ta>
            <ta e="T349" id="Seg_2937" s="T348">töːko-tɨ</ta>
            <ta e="T350" id="Seg_2938" s="T349">mi-lʼčʼi-tɨ</ta>
            <ta e="T351" id="Seg_2939" s="T350">mɨka-t</ta>
            <ta e="T352" id="Seg_2940" s="T351">čʼɔːtɨ</ta>
            <ta e="T353" id="Seg_2941" s="T352">nɨːnɨ</ta>
            <ta e="T354" id="Seg_2942" s="T353">na</ta>
            <ta e="T355" id="Seg_2943" s="T354">mɨka-ntɨ-sä</ta>
            <ta e="T356" id="Seg_2944" s="T355">qən-na</ta>
            <ta e="T357" id="Seg_2945" s="T356">qən-na</ta>
            <ta e="T358" id="Seg_2946" s="T357">mɨka</ta>
            <ta e="T359" id="Seg_2947" s="T358">ürr-ɛːj-sa</ta>
            <ta e="T360" id="Seg_2948" s="T359">qət</ta>
            <ta e="T361" id="Seg_2949" s="T360">ši</ta>
            <ta e="T362" id="Seg_2950" s="T361">mɔːt-tɨ</ta>
            <ta e="T363" id="Seg_2951" s="T362">čʼap</ta>
            <ta e="T364" id="Seg_2952" s="T363">šeːr-na</ta>
            <ta e="T365" id="Seg_2953" s="T364">mɨka</ta>
            <ta e="T366" id="Seg_2954" s="T365">urr-ɛː-sʼa</ta>
            <ta e="T367" id="Seg_2955" s="T366">na</ta>
            <ta e="T368" id="Seg_2956" s="T367">qəːt</ta>
            <ta e="T369" id="Seg_2957" s="T368">šinčʼoː-qɨt</ta>
            <ta e="T370" id="Seg_2958" s="T369">na</ta>
            <ta e="T371" id="Seg_2959" s="T370">peː-ŋɨ-tɨ</ta>
            <ta e="T372" id="Seg_2960" s="T371">pü-ntɨr-lʼä</ta>
            <ta e="T373" id="Seg_2961" s="T372">tan</ta>
            <ta e="T374" id="Seg_2962" s="T373">qaj</ta>
            <ta e="T375" id="Seg_2963" s="T374">peː-ŋa-l</ta>
            <ta e="T376" id="Seg_2964" s="T375">irra</ta>
            <ta e="T377" id="Seg_2965" s="T376">tɔː-lʼ</ta>
            <ta e="T378" id="Seg_2966" s="T377">mɔːt-a-lʼ</ta>
            <ta e="T379" id="Seg_2967" s="T378">qup</ta>
            <ta e="T380" id="Seg_2968" s="T379">lʼa</ta>
            <ta e="T381" id="Seg_2969" s="T380">mat</ta>
            <ta e="T382" id="Seg_2970" s="T381">mɨka-p</ta>
            <ta e="T383" id="Seg_2971" s="T382">čʼap</ta>
            <ta e="T384" id="Seg_2972" s="T383">tatɨ-nta-p</ta>
            <ta e="T385" id="Seg_2973" s="T384">tintena</ta>
            <ta e="T386" id="Seg_2974" s="T385">mɨka</ta>
            <ta e="T387" id="Seg_2975" s="T386">ür-te-nta-p</ta>
            <ta e="T388" id="Seg_2976" s="T387">mɨka-mɨ</ta>
            <ta e="T389" id="Seg_2977" s="T388">tap</ta>
            <ta e="T390" id="Seg_2978" s="T389">peː-nta-p</ta>
            <ta e="T391" id="Seg_2979" s="T390">qo</ta>
            <ta e="T392" id="Seg_2980" s="T391">imaqota-qantɨ</ta>
            <ta e="T393" id="Seg_2981" s="T392">mɔːt</ta>
            <ta e="T394" id="Seg_2982" s="T393">šeːr-aš</ta>
            <ta e="T395" id="Seg_2983" s="T394">am-e-r-aš</ta>
            <ta e="T396" id="Seg_2984" s="T395">nimto</ta>
            <ta e="T397" id="Seg_2985" s="T396">na</ta>
            <ta e="T398" id="Seg_2986" s="T397">j-ta</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2987" s="T0">starik</ta>
            <ta e="T2" id="Seg_2988" s="T1">pervɨj</ta>
            <ta e="T3" id="Seg_2989" s="T2">voda</ta>
            <ta e="T4" id="Seg_2990" s="T3">zamerznet</ta>
            <ta e="T5" id="Seg_2991" s="T4">qatamol</ta>
            <ta e="T6" id="Seg_2992" s="T5">üt-ɨ-m</ta>
            <ta e="T7" id="Seg_2993" s="T6">am-ɛː-ŋɨ-tɨ</ta>
            <ta e="T8" id="Seg_2994" s="T7">ira</ta>
            <ta e="T9" id="Seg_2995" s="T8">kora-lä</ta>
            <ta e="T10" id="Seg_2996" s="T9">qəːlɨ-š-lä</ta>
            <ta e="T11" id="Seg_2997" s="T10">qən-mpɨ</ta>
            <ta e="T12" id="Seg_2998" s="T11">qoltɨ-m</ta>
            <ta e="T13" id="Seg_2999" s="T12">kuššat</ta>
            <ta e="T14" id="Seg_3000" s="T13">orqɨl-qɨl-ŋɨ-tɨ</ta>
            <ta e="T15" id="Seg_3001" s="T14">ira</ta>
            <ta e="T16" id="Seg_3002" s="T15">qoltɨ-n</ta>
            <ta e="T17" id="Seg_3003" s="T16">qanɨŋ-mɨn</ta>
            <ta e="T18" id="Seg_3004" s="T17">nʼennä</ta>
            <ta e="T19" id="Seg_3005" s="T18">mantɨ-qɨl-mpɨ-lä</ta>
            <ta e="T20" id="Seg_3006" s="T19">*kurɨ-ŋɨ</ta>
            <ta e="T21" id="Seg_3007" s="T20">suːrɨm-tqo</ta>
            <ta e="T22" id="Seg_3008" s="T21">to</ta>
            <ta e="T23" id="Seg_3009" s="T22">toː</ta>
            <ta e="T24" id="Seg_3010" s="T23">mantɨ-ätɔːl-mpɨ-lä</ta>
            <ta e="T25" id="Seg_3011" s="T24">qo</ta>
            <ta e="T26" id="Seg_3012" s="T25">ukkɨr</ta>
            <ta e="T27" id="Seg_3013" s="T26">čʼontɨ-qɨn</ta>
            <ta e="T28" id="Seg_3014" s="T27">təp</ta>
            <ta e="T29" id="Seg_3015" s="T28">kos</ta>
            <ta e="T30" id="Seg_3016" s="T29">qaj</ta>
            <ta e="T31" id="Seg_3017" s="T30">orɨ-š-ntɨ</ta>
            <ta e="T32" id="Seg_3018" s="T31">qaj</ta>
            <ta e="T33" id="Seg_3019" s="T32">qum</ta>
            <ta e="T34" id="Seg_3020" s="T33">laŋkɨ-š-ntɨ</ta>
            <ta e="T35" id="Seg_3021" s="T34">qaj</ta>
            <ta e="T36" id="Seg_3022" s="T35">qaj</ta>
            <ta e="T37" id="Seg_3023" s="T36">orɨ-š-ntɨ</ta>
            <ta e="T38" id="Seg_3024" s="T37">təp</ta>
            <ta e="T39" id="Seg_3025" s="T38">qaj</ta>
            <ta e="T40" id="Seg_3026" s="T39">qum</ta>
            <ta e="T41" id="Seg_3027" s="T40">lʼi</ta>
            <ta e="T42" id="Seg_3028" s="T41">qaj</ta>
            <ta e="T43" id="Seg_3029" s="T42">laŋkɨ-š-ntɨ</ta>
            <ta e="T44" id="Seg_3030" s="T43">ira</ta>
            <ta e="T45" id="Seg_3031" s="T44">nık</ta>
            <ta e="T46" id="Seg_3032" s="T45">na</ta>
            <ta e="T47" id="Seg_3033" s="T46">üntɨ-tɨ-tɨ</ta>
            <ta e="T48" id="Seg_3034" s="T47">nɨːnɨ</ta>
            <ta e="T49" id="Seg_3035" s="T48">tü-ntɨ</ta>
            <ta e="T50" id="Seg_3036" s="T49">qo</ta>
            <ta e="T51" id="Seg_3037" s="T50">točʼčʼä</ta>
            <ta e="T52" id="Seg_3038" s="T51">nʼəmpɨ-qɨn</ta>
            <ta e="T53" id="Seg_3039" s="T52">qaj-ɨ-lʼ</ta>
            <ta e="T54" id="Seg_3040" s="T53">laka</ta>
            <ta e="T55" id="Seg_3041" s="T54">ɛː-sɨ</ta>
            <ta e="T56" id="Seg_3042" s="T55">nʼəmpɨ-qɨn</ta>
            <ta e="T57" id="Seg_3043" s="T56">mɨta</ta>
            <ta e="T58" id="Seg_3044" s="T57">qaj-ɨ-lʼ</ta>
            <ta e="T59" id="Seg_3045" s="T58">laka</ta>
            <ta e="T60" id="Seg_3046" s="T59">qontɨ-lʼčʼɨ</ta>
            <ta e="T61" id="Seg_3047" s="T60">ınnä</ta>
            <ta e="T62" id="Seg_3048" s="T61">čʼam</ta>
            <ta e="T63" id="Seg_3049" s="T62">keː</ta>
            <ta e="T64" id="Seg_3050" s="T63">qatɨtɨ-ɔːl-kkɨ</ta>
            <ta e="T65" id="Seg_3051" s="T64">aj</ta>
            <ta e="T66" id="Seg_3052" s="T65">ıllä</ta>
            <ta e="T67" id="Seg_3053" s="T66">qən-ɛː-ŋɨ</ta>
            <ta e="T68" id="Seg_3054" s="T67">aj</ta>
            <ta e="T69" id="Seg_3055" s="T68">ıllä</ta>
            <ta e="T70" id="Seg_3056" s="T69">qən-ɛː-ŋɨ</ta>
            <ta e="T71" id="Seg_3057" s="T70">näčʼčʼä</ta>
            <ta e="T72" id="Seg_3058" s="T71">čʼam</ta>
            <ta e="T73" id="Seg_3059" s="T72">qən-ŋɨ</ta>
            <ta e="T74" id="Seg_3060" s="T73">montɨ</ta>
            <ta e="T75" id="Seg_3061" s="T74">qum</ta>
            <ta e="T76" id="Seg_3062" s="T75">üt-ntɨ</ta>
            <ta e="T77" id="Seg_3063" s="T76">pat-qɨl-mpɨ</ta>
            <ta e="T78" id="Seg_3064" s="T77">nʼəmpɨ-ntɨ</ta>
            <ta e="T79" id="Seg_3065" s="T78">pošalusta</ta>
            <ta e="T80" id="Seg_3066" s="T79">laŋkɨ-š-ŋɨ</ta>
            <ta e="T81" id="Seg_3067" s="T80">qɔːtɨ</ta>
            <ta e="T82" id="Seg_3068" s="T81">ınnä</ta>
            <ta e="T83" id="Seg_3069" s="T82">mašım</ta>
            <ta e="T84" id="Seg_3070" s="T83">iː-ɨ-äšɨk</ta>
            <ta e="T85" id="Seg_3071" s="T84">tɨmtɨ</ta>
            <ta e="T86" id="Seg_3072" s="T85">na</ta>
            <ta e="T87" id="Seg_3073" s="T86">ira</ta>
            <ta e="T88" id="Seg_3074" s="T87">konnä</ta>
            <ta e="T89" id="Seg_3075" s="T88">paktɨ</ta>
            <ta e="T90" id="Seg_3076" s="T89">čʼumpɨ</ta>
            <ta e="T91" id="Seg_3077" s="T90">nʼarqɨ-m</ta>
            <ta e="T92" id="Seg_3078" s="T91">iː-ŋɨ-tɨ</ta>
            <ta e="T93" id="Seg_3079" s="T92">čʼumpɨ</ta>
            <ta e="T94" id="Seg_3080" s="T93">nʼarqɨ-m</ta>
            <ta e="T95" id="Seg_3081" s="T94">pačʼčʼɨ-ätɔːl-tɨ-tɨ</ta>
            <ta e="T96" id="Seg_3082" s="T95">karrä</ta>
            <ta e="T97" id="Seg_3083" s="T96">iː-tɨ-tɨ</ta>
            <ta e="T98" id="Seg_3084" s="T97">iː-tɨ-ɛː-tɨ</ta>
            <ta e="T99" id="Seg_3085" s="T98">čʼattɨ-ɛː-tɨ</ta>
            <ta e="T100" id="Seg_3086" s="T99">ira-nɨŋ</ta>
            <ta e="T101" id="Seg_3087" s="T100">na</ta>
            <ta e="T102" id="Seg_3088" s="T101">nʼarqɨ-sä</ta>
            <ta e="T103" id="Seg_3089" s="T102">konnä</ta>
            <ta e="T104" id="Seg_3090" s="T103">ınnä</ta>
            <ta e="T105" id="Seg_3091" s="T104">üː-qɨl-ɛː-ŋɨ-tɨ</ta>
            <ta e="T106" id="Seg_3092" s="T105">konnä</ta>
            <ta e="T107" id="Seg_3093" s="T106">čʼam</ta>
            <ta e="T108" id="Seg_3094" s="T107">üː-qɨl-ɛː-ŋɨ-tɨ</ta>
            <ta e="T109" id="Seg_3095" s="T108">qo</ta>
            <ta e="T110" id="Seg_3096" s="T109">qo</ta>
            <ta e="T111" id="Seg_3097" s="T110">montɨ</ta>
            <ta e="T112" id="Seg_3098" s="T111">təmɨ-qum</ta>
            <ta e="T113" id="Seg_3099" s="T112">kupec</ta>
            <ta e="T114" id="Seg_3100" s="T113">kupec</ta>
            <ta e="T115" id="Seg_3101" s="T114">na</ta>
            <ta e="T116" id="Seg_3102" s="T115">tətta</ta>
            <ta e="T117" id="Seg_3103" s="T116">tan</ta>
            <ta e="T118" id="Seg_3104" s="T117">mašım</ta>
            <ta e="T119" id="Seg_3105" s="T118">ınnä</ta>
            <ta e="T120" id="Seg_3106" s="T119">mašım</ta>
            <ta e="T121" id="Seg_3107" s="T120">iː-ŋɨ-ntɨ</ta>
            <ta e="T122" id="Seg_3108" s="T121">qu-r-mə-n</ta>
            <ta e="T123" id="Seg_3109" s="T122">*nɔː-nɨ</ta>
            <ta e="T124" id="Seg_3110" s="T123">ınnä</ta>
            <ta e="T125" id="Seg_3111" s="T124">mašım</ta>
            <ta e="T126" id="Seg_3112" s="T125">iː-ŋɨ-ntɨ</ta>
            <ta e="T127" id="Seg_3113" s="T126">ınnä</ta>
            <ta e="T128" id="Seg_3114" s="T127">mašım</ta>
            <ta e="T129" id="Seg_3115" s="T128">iː-ŋɨ-ntɨ</ta>
            <ta e="T130" id="Seg_3116" s="T129">solota-lʼ</ta>
            <ta e="T131" id="Seg_3117" s="T130">kusok-sä</ta>
            <ta e="T132" id="Seg_3118" s="T131">mi-ŋɨ-tɨ</ta>
            <ta e="T133" id="Seg_3119" s="T132">mitɨ</ta>
            <ta e="T134" id="Seg_3120" s="T133">qu-r-mə-n</ta>
            <ta e="T135" id="Seg_3121" s="T134">*nɔː-nɨ</ta>
            <ta e="T136" id="Seg_3122" s="T135">ınnä</ta>
            <ta e="T137" id="Seg_3123" s="T136">mašım</ta>
            <ta e="T138" id="Seg_3124" s="T137">iː-ŋɨ-ntɨ</ta>
            <ta e="T139" id="Seg_3125" s="T138">qaj</ta>
            <ta e="T140" id="Seg_3126" s="T139">iː-ɛntɨ-l</ta>
            <ta e="T141" id="Seg_3127" s="T140">qaj</ta>
            <ta e="T142" id="Seg_3128" s="T141">ilɨ-ptäː-lʼ</ta>
            <ta e="T143" id="Seg_3129" s="T142">iː-ɛntɨ-l</ta>
            <ta e="T144" id="Seg_3130" s="T143">na</ta>
            <ta e="T145" id="Seg_3131" s="T144">qən-ntɨ-naj</ta>
            <ta e="T146" id="Seg_3132" s="T145">lʼa</ta>
            <ta e="T147" id="Seg_3133" s="T146">čʼuntɨ</ta>
            <ta e="T148" id="Seg_3134" s="T147">ukkɨr</ta>
            <ta e="T149" id="Seg_3135" s="T148">ruš</ta>
            <ta e="T150" id="Seg_3136" s="T149">na</ta>
            <ta e="T151" id="Seg_3137" s="T150">montɨ</ta>
            <ta e="T152" id="Seg_3138" s="T151">čʼuntɨ-m</ta>
            <ta e="T153" id="Seg_3139" s="T152">tɔːqqɨ-mpɨ-tɨ</ta>
            <ta e="T154" id="Seg_3140" s="T153">moːrɨ-tɨ</ta>
            <ta e="T155" id="Seg_3141" s="T154">ašša</ta>
            <ta e="T156" id="Seg_3142" s="T155">atɨ</ta>
            <ta e="T157" id="Seg_3143" s="T156">nɨːnɨ</ta>
            <ta e="T158" id="Seg_3144" s="T157">tü-ntɨ</ta>
            <ta e="T159" id="Seg_3145" s="T158">lʼa</ta>
            <ta e="T160" id="Seg_3146" s="T159">tɛnɨ-tɨ</ta>
            <ta e="T161" id="Seg_3147" s="T160">nık</ta>
            <ta e="T162" id="Seg_3148" s="T161">kolʼɨ-mɔːt-ŋɨ</ta>
            <ta e="T163" id="Seg_3149" s="T162">qɔːtɨ</ta>
            <ta e="T164" id="Seg_3150" s="T163">mompa</ta>
            <ta e="T165" id="Seg_3151" s="T164">čʼuntɨ-m</ta>
            <ta e="T166" id="Seg_3152" s="T165">iː-sɨ-m</ta>
            <ta e="T167" id="Seg_3153" s="T166">ɛnä</ta>
            <ta e="T168" id="Seg_3154" s="T167">uːčʼɨ-qɨnoːqo</ta>
            <ta e="T169" id="Seg_3155" s="T168">čʼuntɨ</ta>
            <ta e="T170" id="Seg_3156" s="T169">iː-ntɨ-tɨ</ta>
            <ta e="T171" id="Seg_3157" s="T170">solota-lʼ</ta>
            <ta e="T172" id="Seg_3158" s="T171">kusok-tɨ</ta>
            <ta e="T173" id="Seg_3159" s="T172">toː</ta>
            <ta e="T174" id="Seg_3160" s="T173">mi-lʼčʼɨ-ŋɨ-tɨ</ta>
            <ta e="T175" id="Seg_3161" s="T174">nɨːnɨ</ta>
            <ta e="T176" id="Seg_3162" s="T175">na</ta>
            <ta e="T177" id="Seg_3163" s="T176">čʼuntɨ-ntɨ-sä</ta>
            <ta e="T178" id="Seg_3164" s="T177">qən-ŋɨ-naj</ta>
            <ta e="T179" id="Seg_3165" s="T178">qən-ŋɨ</ta>
            <ta e="T180" id="Seg_3166" s="T179">qən-ŋɨ</ta>
            <ta e="T181" id="Seg_3167" s="T180">ukkɨr</ta>
            <ta e="T182" id="Seg_3168" s="T181">čʼontɨ-qɨn</ta>
            <ta e="T183" id="Seg_3169" s="T182">ruš</ta>
            <ta e="T184" id="Seg_3170" s="T183">na-n</ta>
            <ta e="T185" id="Seg_3171" s="T184">montɨ</ta>
            <ta e="T186" id="Seg_3172" s="T185">sɨːrɨ-t-ɨ-m</ta>
            <ta e="T187" id="Seg_3173" s="T186">tɔːqqɨ-mpɨ-tɨ</ta>
            <ta e="T188" id="Seg_3174" s="T187">moːrɨ-tɨ</ta>
            <ta e="T189" id="Seg_3175" s="T188">ašša</ta>
            <ta e="T190" id="Seg_3176" s="T189">atɨ</ta>
            <ta e="T191" id="Seg_3177" s="T190">lʼa</ta>
            <ta e="T192" id="Seg_3178" s="T191">tɛnɨ-tɨ</ta>
            <ta e="T193" id="Seg_3179" s="T192">nık</ta>
            <ta e="T194" id="Seg_3180" s="T193">kolʼɨ-mɔːt-ŋɨ</ta>
            <ta e="T195" id="Seg_3181" s="T194">moloko-ntoːqo</ta>
            <ta e="T196" id="Seg_3182" s="T195">nʼima-ntoːqo</ta>
            <ta e="T197" id="Seg_3183" s="T196">sɨːrɨ-m</ta>
            <ta e="T198" id="Seg_3184" s="T197">iː-sɨ-m</ta>
            <ta e="T199" id="Seg_3185" s="T198">ɛnä</ta>
            <ta e="T200" id="Seg_3186" s="T199">čʼuntɨ-m-tɨ</ta>
            <ta e="T201" id="Seg_3187" s="T200">sɨːrɨ</ta>
            <ta e="T202" id="Seg_3188" s="T201">čʼɔːtɨ</ta>
            <ta e="T203" id="Seg_3189" s="T202">toː</ta>
            <ta e="T204" id="Seg_3190" s="T203">mi-lʼčʼɨ-tɨ</ta>
            <ta e="T205" id="Seg_3191" s="T204">na</ta>
            <ta e="T206" id="Seg_3192" s="T205">sɨːrɨ-ntɨ-sä</ta>
            <ta e="T207" id="Seg_3193" s="T206">na</ta>
            <ta e="T208" id="Seg_3194" s="T207">qən-ntɨ-naj</ta>
            <ta e="T209" id="Seg_3195" s="T208">qən-ntɨ-naj</ta>
            <ta e="T210" id="Seg_3196" s="T209">qo</ta>
            <ta e="T211" id="Seg_3197" s="T210">ukkɨr</ta>
            <ta e="T212" id="Seg_3198" s="T211">čʼontɨ-qɨn</ta>
            <ta e="T213" id="Seg_3199" s="T212">sɨpɨnʼčʼa-t-ɨ-m</ta>
            <ta e="T214" id="Seg_3200" s="T213">na</ta>
            <ta e="T215" id="Seg_3201" s="T214">montɨ</ta>
            <ta e="T216" id="Seg_3202" s="T215">tɔːqqɨ-mpɨ-tɨt</ta>
            <ta e="T217" id="Seg_3203" s="T216">moːrɨ-tɨ</ta>
            <ta e="T218" id="Seg_3204" s="T217">ašša</ta>
            <ta e="T219" id="Seg_3205" s="T218">atɨ</ta>
            <ta e="T220" id="Seg_3206" s="T219">nɨːnɨ</ta>
            <ta e="T221" id="Seg_3207" s="T220">nık</ta>
            <ta e="T222" id="Seg_3208" s="T221">kətɨ-tɨ</ta>
            <ta e="T223" id="Seg_3209" s="T222">lʼa</ta>
            <ta e="T224" id="Seg_3210" s="T223">sɨpɨnʼčʼa</ta>
            <ta e="T225" id="Seg_3211" s="T224">na-n</ta>
            <ta e="T226" id="Seg_3212" s="T225">montɨ</ta>
            <ta e="T227" id="Seg_3213" s="T226">ür-ɨ-k</ta>
            <ta e="T228" id="Seg_3214" s="T227">ɛː-ŋɨ</ta>
            <ta e="T229" id="Seg_3215" s="T228">lʼa</ta>
            <ta e="T230" id="Seg_3216" s="T229">mɨta</ta>
            <ta e="T231" id="Seg_3217" s="T230">sɨpɨnʼčʼa</ta>
            <ta e="T232" id="Seg_3218" s="T231">žirna-k</ta>
            <ta e="T233" id="Seg_3219" s="T232">ɛː-ŋɨ</ta>
            <ta e="T234" id="Seg_3220" s="T233">sɨpɨnʼčʼa-m</ta>
            <ta e="T235" id="Seg_3221" s="T234">iː-sɨ-m</ta>
            <ta e="T236" id="Seg_3222" s="T235">ɛnä</ta>
            <ta e="T237" id="Seg_3223" s="T236">sɨpɨnʼčʼa-n</ta>
            <ta e="T238" id="Seg_3224" s="T237">čʼɔːtɨ</ta>
            <ta e="T239" id="Seg_3225" s="T238">toː</ta>
            <ta e="T240" id="Seg_3226" s="T239">mi-ŋɨ-tɨ</ta>
            <ta e="T241" id="Seg_3227" s="T240">na</ta>
            <ta e="T242" id="Seg_3228" s="T241">sɨːrɨ-m-tɨ</ta>
            <ta e="T243" id="Seg_3229" s="T242">aj</ta>
            <ta e="T244" id="Seg_3230" s="T243">menʼalsʼa</ta>
            <ta e="T245" id="Seg_3231" s="T244">nɨːnɨ</ta>
            <ta e="T246" id="Seg_3232" s="T245">na</ta>
            <ta e="T247" id="Seg_3233" s="T246">sɨpɨnʼčʼa-ntɨ-sä</ta>
            <ta e="T248" id="Seg_3234" s="T247">na</ta>
            <ta e="T249" id="Seg_3235" s="T248">qən-ntɨ</ta>
            <ta e="T250" id="Seg_3236" s="T249">na</ta>
            <ta e="T251" id="Seg_3237" s="T250">qən-ntɨ-naj</ta>
            <ta e="T252" id="Seg_3238" s="T251">ukkɨr</ta>
            <ta e="T253" id="Seg_3239" s="T252">tɔːt</ta>
            <ta e="T254" id="Seg_3240" s="T253">čʼontɨ-qɨn</ta>
            <ta e="T255" id="Seg_3241" s="T254">na</ta>
            <ta e="T256" id="Seg_3242" s="T255">ruš</ta>
            <ta e="T257" id="Seg_3243" s="T256">kəːtɨ-mpɨlʼ</ta>
            <ta e="T258" id="Seg_3244" s="T257">čʼıŋkɨ-t-ɨ-m</ta>
            <ta e="T259" id="Seg_3245" s="T258">tɔːqqɨ-mpɨ</ta>
            <ta e="T260" id="Seg_3246" s="T259">čʼıŋkɨ-t-ɨ-m</ta>
            <ta e="T261" id="Seg_3247" s="T260">namɨššak</ta>
            <ta e="T262" id="Seg_3248" s="T261">tɔːqqɨ-mpɨ-tɨ</ta>
            <ta e="T263" id="Seg_3249" s="T262">moːrɨ-tɨ</ta>
            <ta e="T264" id="Seg_3250" s="T263">čʼäːŋkɨ</ta>
            <ta e="T265" id="Seg_3251" s="T264">qɔːtɨ</ta>
            <ta e="T266" id="Seg_3252" s="T265">jajca-tɨ</ta>
            <ta e="T267" id="Seg_3253" s="T266">to</ta>
            <ta e="T268" id="Seg_3254" s="T267">mnogo</ta>
            <ta e="T269" id="Seg_3255" s="T268">čʼıŋkɨ-m</ta>
            <ta e="T270" id="Seg_3256" s="T269">iː-sɨ-m</ta>
            <ta e="T271" id="Seg_3257" s="T270">ɛnä</ta>
            <ta e="T272" id="Seg_3258" s="T271">ɛŋ-ntoːqo</ta>
            <ta e="T273" id="Seg_3259" s="T272">ɛŋ-tɨ</ta>
            <ta e="T274" id="Seg_3260" s="T273">čʼɔːtɨ</ta>
            <ta e="T275" id="Seg_3261" s="T274">čʼıŋkɨ-n</ta>
            <ta e="T276" id="Seg_3262" s="T275">čʼɔːtɨ</ta>
            <ta e="T277" id="Seg_3263" s="T276">sɨpɨnʼčʼa-m-tɨ</ta>
            <ta e="T278" id="Seg_3264" s="T277">toː</ta>
            <ta e="T279" id="Seg_3265" s="T278">mi-lʼčʼɨ-tɨ</ta>
            <ta e="T280" id="Seg_3266" s="T279">nɨːnɨ</ta>
            <ta e="T281" id="Seg_3267" s="T280">na</ta>
            <ta e="T282" id="Seg_3268" s="T281">čʼıŋkɨ-ntɨ-sä</ta>
            <ta e="T283" id="Seg_3269" s="T282">na</ta>
            <ta e="T284" id="Seg_3270" s="T283">qən-ŋɨ</ta>
            <ta e="T285" id="Seg_3271" s="T284">qən-ŋɨ</ta>
            <ta e="T286" id="Seg_3272" s="T285">moqɨnä</ta>
            <ta e="T287" id="Seg_3273" s="T286">ukkɨr</ta>
            <ta e="T288" id="Seg_3274" s="T287">čʼontɨ-n</ta>
            <ta e="T289" id="Seg_3275" s="T288">čʼontɨ-qɨn</ta>
            <ta e="T290" id="Seg_3276" s="T289">montɨ</ta>
            <ta e="T291" id="Seg_3277" s="T290">ruš</ta>
            <ta e="T292" id="Seg_3278" s="T291">töːka-t-ɨ-m</ta>
            <ta e="T293" id="Seg_3279" s="T292">namɨššak</ta>
            <ta e="T294" id="Seg_3280" s="T293">tɔːqqɨ-mpɨ-tɨ</ta>
            <ta e="T295" id="Seg_3281" s="T294">moːrɨ-tɨ</ta>
            <ta e="T296" id="Seg_3282" s="T295">ašša</ta>
            <ta e="T297" id="Seg_3283" s="T296">atɨ</ta>
            <ta e="T298" id="Seg_3284" s="T297">qɔːtɨ</ta>
            <ta e="T299" id="Seg_3285" s="T298">töːka-m</ta>
            <ta e="T300" id="Seg_3286" s="T299">iː-sɨ-m</ta>
            <ta e="T301" id="Seg_3287" s="T300">ɛnä</ta>
            <ta e="T302" id="Seg_3288" s="T301">na</ta>
            <ta e="T303" id="Seg_3289" s="T302">čʼıŋkɨ-m-tɨ</ta>
            <ta e="T304" id="Seg_3290" s="T303">töːka-n</ta>
            <ta e="T305" id="Seg_3291" s="T304">čʼɔːtɨ</ta>
            <ta e="T306" id="Seg_3292" s="T305">toː</ta>
            <ta e="T307" id="Seg_3293" s="T306">mi-lʼčʼɨ-ŋɨ-tɨ</ta>
            <ta e="T308" id="Seg_3294" s="T307">nɨːnɨ</ta>
            <ta e="T309" id="Seg_3295" s="T308">töːka-ntɨ-sä</ta>
            <ta e="T310" id="Seg_3296" s="T309">qən-ŋɨ</ta>
            <ta e="T311" id="Seg_3297" s="T310">qən-ŋɨ-naj</ta>
            <ta e="T312" id="Seg_3298" s="T311">ukkɨr</ta>
            <ta e="T313" id="Seg_3299" s="T312">ira-lʼ</ta>
            <ta e="T314" id="Seg_3300" s="T313">lıːpɨ</ta>
            <ta e="T315" id="Seg_3301" s="T314">mompa</ta>
            <ta e="T316" id="Seg_3302" s="T315">tına</ta>
            <ta e="T317" id="Seg_3303" s="T316">na-n</ta>
            <ta e="T318" id="Seg_3304" s="T317">keːpɨ-lʼ</ta>
            <ta e="T319" id="Seg_3305" s="T318">keːpɨ-lʼ</ta>
            <ta e="T320" id="Seg_3306" s="T319">mɨka-tɨ</ta>
            <ta e="T321" id="Seg_3307" s="T320">türɨ-k-mpɨ-tɨ</ta>
            <ta e="T322" id="Seg_3308" s="T321">mɨka</ta>
            <ta e="T323" id="Seg_3309" s="T322">aj</ta>
            <ta e="T324" id="Seg_3310" s="T323">mɨka</ta>
            <ta e="T325" id="Seg_3311" s="T324">lʼa</ta>
            <ta e="T326" id="Seg_3312" s="T325">tɛnɨ-tɨ</ta>
            <ta e="T327" id="Seg_3313" s="T326">nık</ta>
            <ta e="T328" id="Seg_3314" s="T327">kolʼɨ-mɔːt-ŋɨ</ta>
            <ta e="T329" id="Seg_3315" s="T328">lʼa</ta>
            <ta e="T330" id="Seg_3316" s="T329">sajɨ-kɨtɨ-lʼ</ta>
            <ta e="T331" id="Seg_3317" s="T330">lоːsɨ-ɨ-ŋɨ-k</ta>
            <ta e="T332" id="Seg_3318" s="T331">lʼaqa</ta>
            <ta e="T333" id="Seg_3319" s="T332">qaj-ɨ-m</ta>
            <ta e="T334" id="Seg_3320" s="T333">man</ta>
            <ta e="T335" id="Seg_3321" s="T334">šüt-qo</ta>
            <ta e="T336" id="Seg_3322" s="T335">qaj-m</ta>
            <ta e="T337" id="Seg_3323" s="T336">šüt-qo</ta>
            <ta e="T338" id="Seg_3324" s="T337">qɔːtɨ</ta>
            <ta e="T339" id="Seg_3325" s="T338">na</ta>
            <ta e="T340" id="Seg_3326" s="T339">ira-n</ta>
            <ta e="T341" id="Seg_3327" s="T340">mɨka-m</ta>
            <ta e="T342" id="Seg_3328" s="T341">iː-sɨ-m</ta>
            <ta e="T343" id="Seg_3329" s="T342">ɛnä</ta>
            <ta e="T344" id="Seg_3330" s="T343">tına</ta>
            <ta e="T345" id="Seg_3331" s="T344">mɨka</ta>
            <ta e="T346" id="Seg_3332" s="T345">čʼɔːtɨ</ta>
            <ta e="T347" id="Seg_3333" s="T346">toː</ta>
            <ta e="T348" id="Seg_3334" s="T347">mi-ŋɨ-tɨ</ta>
            <ta e="T349" id="Seg_3335" s="T348">töːka-tɨ</ta>
            <ta e="T350" id="Seg_3336" s="T349">mi-lʼčʼɨ-tɨ</ta>
            <ta e="T351" id="Seg_3337" s="T350">mɨka-n</ta>
            <ta e="T352" id="Seg_3338" s="T351">čʼɔːtɨ</ta>
            <ta e="T353" id="Seg_3339" s="T352">nɨːnɨ</ta>
            <ta e="T354" id="Seg_3340" s="T353">na</ta>
            <ta e="T355" id="Seg_3341" s="T354">mɨka-ntɨ-sä</ta>
            <ta e="T356" id="Seg_3342" s="T355">qən-ŋɨ</ta>
            <ta e="T357" id="Seg_3343" s="T356">qən-ŋɨ</ta>
            <ta e="T358" id="Seg_3344" s="T357">mɨka</ta>
            <ta e="T359" id="Seg_3345" s="T358">ürɨ-ɛː-sɨ</ta>
            <ta e="T360" id="Seg_3346" s="T359">qət</ta>
            <ta e="T361" id="Seg_3347" s="T360">*šüː</ta>
            <ta e="T362" id="Seg_3348" s="T361">mɔːt-ntɨ</ta>
            <ta e="T363" id="Seg_3349" s="T362">čʼam</ta>
            <ta e="T364" id="Seg_3350" s="T363">šeːr-ŋɨ</ta>
            <ta e="T365" id="Seg_3351" s="T364">mɨka</ta>
            <ta e="T366" id="Seg_3352" s="T365">ürɨ-ɛː-sɨ</ta>
            <ta e="T367" id="Seg_3353" s="T366">na</ta>
            <ta e="T368" id="Seg_3354" s="T367">qəːt</ta>
            <ta e="T369" id="Seg_3355" s="T368">šünʼčʼɨ-qɨn</ta>
            <ta e="T370" id="Seg_3356" s="T369">na</ta>
            <ta e="T371" id="Seg_3357" s="T370">peː-ŋɨ-tɨ</ta>
            <ta e="T372" id="Seg_3358" s="T371">pü-ntɨr-lä</ta>
            <ta e="T373" id="Seg_3359" s="T372">tan</ta>
            <ta e="T374" id="Seg_3360" s="T373">qaj</ta>
            <ta e="T375" id="Seg_3361" s="T374">peː-ŋɨ-l</ta>
            <ta e="T376" id="Seg_3362" s="T375">ira</ta>
            <ta e="T377" id="Seg_3363" s="T376">tɔː-lʼ</ta>
            <ta e="T378" id="Seg_3364" s="T377">mɔːt-ɨ-lʼ</ta>
            <ta e="T379" id="Seg_3365" s="T378">qum</ta>
            <ta e="T380" id="Seg_3366" s="T379">lʼa</ta>
            <ta e="T381" id="Seg_3367" s="T380">man</ta>
            <ta e="T382" id="Seg_3368" s="T381">mɨka-m</ta>
            <ta e="T383" id="Seg_3369" s="T382">čʼam</ta>
            <ta e="T384" id="Seg_3370" s="T383">taːtɨ-ntɨ-m</ta>
            <ta e="T385" id="Seg_3371" s="T384">tına</ta>
            <ta e="T386" id="Seg_3372" s="T385">mɨka</ta>
            <ta e="T387" id="Seg_3373" s="T386">ürɨ-tɨ-ntɨ-m</ta>
            <ta e="T388" id="Seg_3374" s="T387">mɨka-mɨ</ta>
            <ta e="T389" id="Seg_3375" s="T388">tam</ta>
            <ta e="T390" id="Seg_3376" s="T389">peː-ntɨ-m</ta>
            <ta e="T391" id="Seg_3377" s="T390">qo</ta>
            <ta e="T392" id="Seg_3378" s="T391">imaqota-qäntɨ</ta>
            <ta e="T393" id="Seg_3379" s="T392">mɔːt</ta>
            <ta e="T394" id="Seg_3380" s="T393">šeːr-äšɨk</ta>
            <ta e="T395" id="Seg_3381" s="T394">am-ɨ-r-äšɨk</ta>
            <ta e="T396" id="Seg_3382" s="T395">nɨmtɨ</ta>
            <ta e="T397" id="Seg_3383" s="T396">na</ta>
            <ta e="T398" id="Seg_3384" s="T397">ɛː-ntɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_3385" s="T0">old.man.[NOM]</ta>
            <ta e="T2" id="Seg_3386" s="T1">first</ta>
            <ta e="T3" id="Seg_3387" s="T2">water.[NOM]</ta>
            <ta e="T4" id="Seg_3388" s="T3">will.freeze</ta>
            <ta e="T5" id="Seg_3389" s="T4">when</ta>
            <ta e="T6" id="Seg_3390" s="T5">water-EP-ACC</ta>
            <ta e="T7" id="Seg_3391" s="T6">eat-PFV-CO-3SG.O</ta>
            <ta e="T8" id="Seg_3392" s="T7">old.man.[NOM]</ta>
            <ta e="T9" id="Seg_3393" s="T8">go.hunting-CVB</ta>
            <ta e="T10" id="Seg_3394" s="T9">fish-VBLZ-CVB</ta>
            <ta e="T11" id="Seg_3395" s="T10">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T12" id="Seg_3396" s="T11">big.river-ACC</ta>
            <ta e="T13" id="Seg_3397" s="T12">when</ta>
            <ta e="T14" id="Seg_3398" s="T13">catch-MULO-CO-3SG.O</ta>
            <ta e="T15" id="Seg_3399" s="T14">old.man.[NOM]</ta>
            <ta e="T16" id="Seg_3400" s="T15">big.river-GEN</ta>
            <ta e="T17" id="Seg_3401" s="T16">bank-PROL</ta>
            <ta e="T18" id="Seg_3402" s="T17">forward</ta>
            <ta e="T19" id="Seg_3403" s="T18">give.a.look-MULO-HAB-CVB</ta>
            <ta e="T20" id="Seg_3404" s="T19">go-CO.[3SG.S]</ta>
            <ta e="T21" id="Seg_3405" s="T20">wild.animal-TRL</ta>
            <ta e="T22" id="Seg_3406" s="T21">that</ta>
            <ta e="T23" id="Seg_3407" s="T22">there</ta>
            <ta e="T24" id="Seg_3408" s="T23">give.a.look-MOM-HAB-CVB</ta>
            <ta e="T25" id="Seg_3409" s="T24">oh</ta>
            <ta e="T26" id="Seg_3410" s="T25">one</ta>
            <ta e="T27" id="Seg_3411" s="T26">middle-LOC</ta>
            <ta e="T28" id="Seg_3412" s="T27">(s)he.[NOM]</ta>
            <ta e="T29" id="Seg_3413" s="T28">DEF</ta>
            <ta e="T30" id="Seg_3414" s="T29">what.[NOM]</ta>
            <ta e="T31" id="Seg_3415" s="T30">force-VBLZ-IPFV.[3SG.S]</ta>
            <ta e="T32" id="Seg_3416" s="T31">either.or</ta>
            <ta e="T33" id="Seg_3417" s="T32">human.being.[NOM]</ta>
            <ta e="T34" id="Seg_3418" s="T33">cry-VBLZ-IPFV.[3SG.S]</ta>
            <ta e="T35" id="Seg_3419" s="T34">either.or</ta>
            <ta e="T36" id="Seg_3420" s="T35">what.[NOM]</ta>
            <ta e="T37" id="Seg_3421" s="T36">force-VBLZ-IPFV.[3SG.S]</ta>
            <ta e="T38" id="Seg_3422" s="T37">(s)he.[NOM]</ta>
            <ta e="T39" id="Seg_3423" s="T38">what.[NOM]</ta>
            <ta e="T40" id="Seg_3424" s="T39">human.being.[NOM]</ta>
            <ta e="T41" id="Seg_3425" s="T40">whether</ta>
            <ta e="T42" id="Seg_3426" s="T41">what.[NOM]</ta>
            <ta e="T43" id="Seg_3427" s="T42">cry-VBLZ-IPFV.[3SG.S]</ta>
            <ta e="T44" id="Seg_3428" s="T43">old.man.[NOM]</ta>
            <ta e="T45" id="Seg_3429" s="T44">so</ta>
            <ta e="T46" id="Seg_3430" s="T45">this.[NOM]</ta>
            <ta e="T47" id="Seg_3431" s="T46">be.heard-TR-3SG.O</ta>
            <ta e="T48" id="Seg_3432" s="T47">from.here</ta>
            <ta e="T49" id="Seg_3433" s="T48">come-IPFV.[3SG.S]</ta>
            <ta e="T50" id="Seg_3434" s="T49">oh</ta>
            <ta e="T51" id="Seg_3435" s="T50">there</ta>
            <ta e="T52" id="Seg_3436" s="T51">ice_hole-LOC</ta>
            <ta e="T53" id="Seg_3437" s="T52">what-EP-ADJZ</ta>
            <ta e="T54" id="Seg_3438" s="T53">piece.[NOM]</ta>
            <ta e="T55" id="Seg_3439" s="T54">be-PST.[3SG.S]</ta>
            <ta e="T56" id="Seg_3440" s="T55">ice_hole-LOC</ta>
            <ta e="T57" id="Seg_3441" s="T56">as.if</ta>
            <ta e="T58" id="Seg_3442" s="T57">what-EP-ADJZ</ta>
            <ta e="T59" id="Seg_3443" s="T58">piece.[NOM]</ta>
            <ta e="T60" id="Seg_3444" s="T59">appear-PFV.[3SG.S]</ta>
            <ta e="T61" id="Seg_3445" s="T60">up</ta>
            <ta e="T62" id="Seg_3446" s="T61">hardly</ta>
            <ta e="T63" id="Seg_3447" s="T62">%%</ta>
            <ta e="T64" id="Seg_3448" s="T63">climb-MOM-DUR.[3SG.S]</ta>
            <ta e="T65" id="Seg_3449" s="T64">again</ta>
            <ta e="T66" id="Seg_3450" s="T65">down</ta>
            <ta e="T67" id="Seg_3451" s="T66">leave-PFV-CO.[3SG.S]</ta>
            <ta e="T68" id="Seg_3452" s="T67">again</ta>
            <ta e="T69" id="Seg_3453" s="T68">down</ta>
            <ta e="T70" id="Seg_3454" s="T69">go.away-PFV-CO.[3SG.S]</ta>
            <ta e="T71" id="Seg_3455" s="T70">there</ta>
            <ta e="T72" id="Seg_3456" s="T71">only</ta>
            <ta e="T73" id="Seg_3457" s="T72">leave-CO.[3SG.S]</ta>
            <ta e="T74" id="Seg_3458" s="T73">apparently</ta>
            <ta e="T75" id="Seg_3459" s="T74">human.being.[NOM]</ta>
            <ta e="T76" id="Seg_3460" s="T75">water-ILL</ta>
            <ta e="T77" id="Seg_3461" s="T76">get.into-MULO-PST.NAR.[3SG.S]</ta>
            <ta e="T78" id="Seg_3462" s="T77">ice_hole-ILL</ta>
            <ta e="T79" id="Seg_3463" s="T78">please</ta>
            <ta e="T80" id="Seg_3464" s="T79">cry-VBLZ-CO.[3SG.S]</ta>
            <ta e="T81" id="Seg_3465" s="T80">may.be</ta>
            <ta e="T82" id="Seg_3466" s="T81">up</ta>
            <ta e="T83" id="Seg_3467" s="T82">I.ACC</ta>
            <ta e="T84" id="Seg_3468" s="T83">take-EP-IMP.2SG.S</ta>
            <ta e="T85" id="Seg_3469" s="T84">here</ta>
            <ta e="T86" id="Seg_3470" s="T85">this</ta>
            <ta e="T87" id="Seg_3471" s="T86">old.man.[NOM]</ta>
            <ta e="T88" id="Seg_3472" s="T87">upwards</ta>
            <ta e="T89" id="Seg_3473" s="T88">run.[3SG.S]</ta>
            <ta e="T90" id="Seg_3474" s="T89">long</ta>
            <ta e="T91" id="Seg_3475" s="T90">purple.osier-ACC</ta>
            <ta e="T92" id="Seg_3476" s="T91">take-CO-3SG.O</ta>
            <ta e="T93" id="Seg_3477" s="T92">long</ta>
            <ta e="T94" id="Seg_3478" s="T93">purple.osier-ACC</ta>
            <ta e="T95" id="Seg_3479" s="T94">chop-MOM-TR-3SG.O</ta>
            <ta e="T96" id="Seg_3480" s="T95">down</ta>
            <ta e="T97" id="Seg_3481" s="T96">take-HAB-3SG.O</ta>
            <ta e="T98" id="Seg_3482" s="T97">take-HAB-PFV-3SG.O</ta>
            <ta e="T99" id="Seg_3483" s="T98">throw-PFV-3SG.O</ta>
            <ta e="T100" id="Seg_3484" s="T99">old.man-ALL</ta>
            <ta e="T101" id="Seg_3485" s="T100">this</ta>
            <ta e="T102" id="Seg_3486" s="T101">purple.osier-INSTR</ta>
            <ta e="T103" id="Seg_3487" s="T102">upwards</ta>
            <ta e="T104" id="Seg_3488" s="T103">up</ta>
            <ta e="T105" id="Seg_3489" s="T104">pull-MULO-PFV-CO-3SG.O</ta>
            <ta e="T106" id="Seg_3490" s="T105">upwards</ta>
            <ta e="T107" id="Seg_3491" s="T106">only</ta>
            <ta e="T108" id="Seg_3492" s="T107">pull-MULO-PFV-CO-3SG.O</ta>
            <ta e="T109" id="Seg_3493" s="T108">oh</ta>
            <ta e="T110" id="Seg_3494" s="T109">oh</ta>
            <ta e="T111" id="Seg_3495" s="T110">apparently</ta>
            <ta e="T112" id="Seg_3496" s="T111">buy-human.being.[NOM]</ta>
            <ta e="T113" id="Seg_3497" s="T112">merchant.[NOM]</ta>
            <ta e="T114" id="Seg_3498" s="T113">merchant.[NOM]</ta>
            <ta e="T115" id="Seg_3499" s="T114">this</ta>
            <ta e="T116" id="Seg_3500" s="T115">rich.man.[NOM]</ta>
            <ta e="T117" id="Seg_3501" s="T116">you.SG.NOM</ta>
            <ta e="T118" id="Seg_3502" s="T117">I.ACC</ta>
            <ta e="T119" id="Seg_3503" s="T118">up</ta>
            <ta e="T120" id="Seg_3504" s="T119">I.ACC</ta>
            <ta e="T121" id="Seg_3505" s="T120">take-CO-2SG.S</ta>
            <ta e="T122" id="Seg_3506" s="T121">die-FRQ-ABST-GEN</ta>
            <ta e="T123" id="Seg_3507" s="T122">from-ADV.EL</ta>
            <ta e="T124" id="Seg_3508" s="T123">up</ta>
            <ta e="T125" id="Seg_3509" s="T124">I.ACC</ta>
            <ta e="T126" id="Seg_3510" s="T125">take-CO-2SG.S</ta>
            <ta e="T127" id="Seg_3511" s="T126">up</ta>
            <ta e="T128" id="Seg_3512" s="T127">I.ACC</ta>
            <ta e="T129" id="Seg_3513" s="T128">take-CO-2SG.S</ta>
            <ta e="T130" id="Seg_3514" s="T129">gold-ADJZ</ta>
            <ta e="T131" id="Seg_3515" s="T130">piece-INSTR</ta>
            <ta e="T132" id="Seg_3516" s="T131">give-CO-3SG.O</ta>
            <ta e="T133" id="Seg_3517" s="T132">as.if</ta>
            <ta e="T134" id="Seg_3518" s="T133">die-FRQ-ABST-GEN</ta>
            <ta e="T135" id="Seg_3519" s="T134">out-ADV.EL</ta>
            <ta e="T136" id="Seg_3520" s="T135">up</ta>
            <ta e="T137" id="Seg_3521" s="T136">I.ACC</ta>
            <ta e="T138" id="Seg_3522" s="T137">take-CO-2SG.S</ta>
            <ta e="T139" id="Seg_3523" s="T138">what.[NOM]</ta>
            <ta e="T140" id="Seg_3524" s="T139">take-FUT-2SG.O</ta>
            <ta e="T141" id="Seg_3525" s="T140">what.[NOM]</ta>
            <ta e="T142" id="Seg_3526" s="T141">live-ACTN-ADJZ</ta>
            <ta e="T143" id="Seg_3527" s="T142">take-FUT-2SG.O</ta>
            <ta e="T144" id="Seg_3528" s="T143">INFER</ta>
            <ta e="T145" id="Seg_3529" s="T144">leave-INFER.[3SG.S]-EMPH</ta>
            <ta e="T146" id="Seg_3530" s="T145">hey</ta>
            <ta e="T147" id="Seg_3531" s="T146">horse.[NOM]</ta>
            <ta e="T148" id="Seg_3532" s="T147">one</ta>
            <ta e="T149" id="Seg_3533" s="T148">Russian.[NOM]</ta>
            <ta e="T150" id="Seg_3534" s="T149">here</ta>
            <ta e="T151" id="Seg_3535" s="T150">apparently</ta>
            <ta e="T152" id="Seg_3536" s="T151">horse-ACC</ta>
            <ta e="T153" id="Seg_3537" s="T152">shepherd-HAB-3SG.O</ta>
            <ta e="T154" id="Seg_3538" s="T153">end.[NOM]-3SG</ta>
            <ta e="T155" id="Seg_3539" s="T154">NEG</ta>
            <ta e="T156" id="Seg_3540" s="T155">be.visible.[3SG.S]</ta>
            <ta e="T157" id="Seg_3541" s="T156">then</ta>
            <ta e="T158" id="Seg_3542" s="T157">come-IPFV.[3SG.S]</ta>
            <ta e="T159" id="Seg_3543" s="T158">hey</ta>
            <ta e="T160" id="Seg_3544" s="T159">mind.[NOM]-3SG</ta>
            <ta e="T161" id="Seg_3545" s="T160">so</ta>
            <ta e="T162" id="Seg_3546" s="T161">turn-DECAUS-CO.[3SG.S]</ta>
            <ta e="T163" id="Seg_3547" s="T162">probably</ta>
            <ta e="T164" id="Seg_3548" s="T163">it.is.said</ta>
            <ta e="T165" id="Seg_3549" s="T164">horse-ACC</ta>
            <ta e="T166" id="Seg_3550" s="T165">take-PST-1SG.O</ta>
            <ta e="T167" id="Seg_3551" s="T166">CONJ</ta>
            <ta e="T168" id="Seg_3552" s="T167">work-SUP.1SG</ta>
            <ta e="T169" id="Seg_3553" s="T168">horse.[NOM]</ta>
            <ta e="T170" id="Seg_3554" s="T169">take-IPFV-3SG.O</ta>
            <ta e="T171" id="Seg_3555" s="T170">gold-ADJZ</ta>
            <ta e="T172" id="Seg_3556" s="T171">piece.[NOM]-3SG</ta>
            <ta e="T173" id="Seg_3557" s="T172">away</ta>
            <ta e="T174" id="Seg_3558" s="T173">give-PFV-CO-3SG.O</ta>
            <ta e="T175" id="Seg_3559" s="T174">then</ta>
            <ta e="T176" id="Seg_3560" s="T175">this</ta>
            <ta e="T177" id="Seg_3561" s="T176">horse-OBL.3SG-COM</ta>
            <ta e="T178" id="Seg_3562" s="T177">go.away-CO.[3SG.S]-EMPH</ta>
            <ta e="T179" id="Seg_3563" s="T178">go.away-CO.[3SG.S]</ta>
            <ta e="T180" id="Seg_3564" s="T179">go.away-CO.[3SG.S]</ta>
            <ta e="T181" id="Seg_3565" s="T180">one</ta>
            <ta e="T182" id="Seg_3566" s="T181">middle-LOC</ta>
            <ta e="T183" id="Seg_3567" s="T182">Russian.[NOM]</ta>
            <ta e="T184" id="Seg_3568" s="T183">this-GEN</ta>
            <ta e="T185" id="Seg_3569" s="T184">apparently</ta>
            <ta e="T186" id="Seg_3570" s="T185">cow-PL-EP-ACC</ta>
            <ta e="T187" id="Seg_3571" s="T186">shepherd-HAB-3SG.O</ta>
            <ta e="T188" id="Seg_3572" s="T187">end.[NOM]-3SG</ta>
            <ta e="T189" id="Seg_3573" s="T188">NEG</ta>
            <ta e="T190" id="Seg_3574" s="T189">be.visible.[3SG.S]</ta>
            <ta e="T191" id="Seg_3575" s="T190">hey</ta>
            <ta e="T192" id="Seg_3576" s="T191">mind.[NOM]-3SG</ta>
            <ta e="T193" id="Seg_3577" s="T192">so</ta>
            <ta e="T194" id="Seg_3578" s="T193">turn-DECAUS-CO.[3SG.S]</ta>
            <ta e="T195" id="Seg_3579" s="T194">milk-3SG.TRL</ta>
            <ta e="T196" id="Seg_3580" s="T195">milk-3SG.TRL</ta>
            <ta e="T197" id="Seg_3581" s="T196">cow-ACC</ta>
            <ta e="T198" id="Seg_3582" s="T197">take-PST-1SG.O</ta>
            <ta e="T199" id="Seg_3583" s="T198">CONJ</ta>
            <ta e="T200" id="Seg_3584" s="T199">horse-ACC-3SG</ta>
            <ta e="T201" id="Seg_3585" s="T200">cow.[NOM]</ta>
            <ta e="T202" id="Seg_3586" s="T201">for</ta>
            <ta e="T203" id="Seg_3587" s="T202">away</ta>
            <ta e="T204" id="Seg_3588" s="T203">give-PFV-3SG.O</ta>
            <ta e="T205" id="Seg_3589" s="T204">this</ta>
            <ta e="T206" id="Seg_3590" s="T205">cow-OBL.3SG-COM</ta>
            <ta e="T207" id="Seg_3591" s="T206">INFER</ta>
            <ta e="T208" id="Seg_3592" s="T207">go.away-INFER.[3SG.S]-EMPH</ta>
            <ta e="T209" id="Seg_3593" s="T208">go.away-INFER.[3SG.S]-EMPH</ta>
            <ta e="T210" id="Seg_3594" s="T209">oh</ta>
            <ta e="T211" id="Seg_3595" s="T210">one</ta>
            <ta e="T212" id="Seg_3596" s="T211">middle-LOC</ta>
            <ta e="T213" id="Seg_3597" s="T212">pig-PL-EP-ACC</ta>
            <ta e="T214" id="Seg_3598" s="T213">here</ta>
            <ta e="T215" id="Seg_3599" s="T214">apparently</ta>
            <ta e="T216" id="Seg_3600" s="T215">shepherd-HAB-3PL</ta>
            <ta e="T217" id="Seg_3601" s="T216">end.[NOM]-3SG</ta>
            <ta e="T218" id="Seg_3602" s="T217">NEG</ta>
            <ta e="T219" id="Seg_3603" s="T218">be.visible.[3SG.S]</ta>
            <ta e="T220" id="Seg_3604" s="T219">then</ta>
            <ta e="T221" id="Seg_3605" s="T220">so</ta>
            <ta e="T222" id="Seg_3606" s="T221">say-3SG.O</ta>
            <ta e="T223" id="Seg_3607" s="T222">hey</ta>
            <ta e="T224" id="Seg_3608" s="T223">pig.[NOM]</ta>
            <ta e="T225" id="Seg_3609" s="T224">this-GEN</ta>
            <ta e="T226" id="Seg_3610" s="T225">apparently</ta>
            <ta e="T227" id="Seg_3611" s="T226">fat-EP-ADVZ</ta>
            <ta e="T228" id="Seg_3612" s="T227">be-CO.[3SG.S]</ta>
            <ta e="T229" id="Seg_3613" s="T228">hey</ta>
            <ta e="T230" id="Seg_3614" s="T229">as.if</ta>
            <ta e="T231" id="Seg_3615" s="T230">pig.[NOM]</ta>
            <ta e="T232" id="Seg_3616" s="T231">fat-ADVZ</ta>
            <ta e="T233" id="Seg_3617" s="T232">be-CO.[3SG.S]</ta>
            <ta e="T234" id="Seg_3618" s="T233">pig-ACC</ta>
            <ta e="T235" id="Seg_3619" s="T234">take-PST-1SG.O</ta>
            <ta e="T236" id="Seg_3620" s="T235">CONJ</ta>
            <ta e="T237" id="Seg_3621" s="T236">pig-GEN</ta>
            <ta e="T238" id="Seg_3622" s="T237">for</ta>
            <ta e="T239" id="Seg_3623" s="T238">away</ta>
            <ta e="T240" id="Seg_3624" s="T239">give-CO-3SG.O</ta>
            <ta e="T241" id="Seg_3625" s="T240">this</ta>
            <ta e="T242" id="Seg_3626" s="T241">cow-ACC-3SG</ta>
            <ta e="T243" id="Seg_3627" s="T242">again</ta>
            <ta e="T244" id="Seg_3628" s="T243">exchange</ta>
            <ta e="T245" id="Seg_3629" s="T244">then</ta>
            <ta e="T246" id="Seg_3630" s="T245">this</ta>
            <ta e="T247" id="Seg_3631" s="T246">pig-OBL.3SG-COM</ta>
            <ta e="T248" id="Seg_3632" s="T247">INFER</ta>
            <ta e="T249" id="Seg_3633" s="T248">go.away-INFER.[3SG.S]</ta>
            <ta e="T250" id="Seg_3634" s="T249">INFER</ta>
            <ta e="T251" id="Seg_3635" s="T250">go.away-INFER.[3SG.S]-EMPH</ta>
            <ta e="T252" id="Seg_3636" s="T251">one</ta>
            <ta e="T253" id="Seg_3637" s="T252">whole</ta>
            <ta e="T254" id="Seg_3638" s="T253">middle-LOC</ta>
            <ta e="T255" id="Seg_3639" s="T254">this</ta>
            <ta e="T256" id="Seg_3640" s="T255">Russian.[NOM]</ta>
            <ta e="T257" id="Seg_3641" s="T256">bring.up-PTCP.PST</ta>
            <ta e="T258" id="Seg_3642" s="T257">swan-PL-EP-ACC</ta>
            <ta e="T259" id="Seg_3643" s="T258">shepherd-HAB.[3SG.S]</ta>
            <ta e="T260" id="Seg_3644" s="T259">swan-PL-EP-ACC</ta>
            <ta e="T261" id="Seg_3645" s="T260">thus.much</ta>
            <ta e="T262" id="Seg_3646" s="T261">shepherd-HAB-3SG.O</ta>
            <ta e="T263" id="Seg_3647" s="T262">end.[NOM]-3SG</ta>
            <ta e="T264" id="Seg_3648" s="T263">NEG.EX.[3SG.S]</ta>
            <ta e="T265" id="Seg_3649" s="T264">probably</ta>
            <ta e="T266" id="Seg_3650" s="T265">egg.[NOM]-3SG</ta>
            <ta e="T267" id="Seg_3651" s="T266">INDEF2 </ta>
            <ta e="T268" id="Seg_3652" s="T267">many</ta>
            <ta e="T269" id="Seg_3653" s="T268">swan-ACC</ta>
            <ta e="T270" id="Seg_3654" s="T269">take-PST-1SG.O</ta>
            <ta e="T271" id="Seg_3655" s="T270">CONJ</ta>
            <ta e="T272" id="Seg_3656" s="T271">egg-3SG.TRL</ta>
            <ta e="T273" id="Seg_3657" s="T272">egg.[NOM]-3SG</ta>
            <ta e="T274" id="Seg_3658" s="T273">for</ta>
            <ta e="T275" id="Seg_3659" s="T274">swan-GEN</ta>
            <ta e="T276" id="Seg_3660" s="T275">for</ta>
            <ta e="T277" id="Seg_3661" s="T276">pig-ACC-3SG</ta>
            <ta e="T278" id="Seg_3662" s="T277">away</ta>
            <ta e="T279" id="Seg_3663" s="T278">give-PFV-3SG.O</ta>
            <ta e="T280" id="Seg_3664" s="T279">then</ta>
            <ta e="T281" id="Seg_3665" s="T280">this</ta>
            <ta e="T282" id="Seg_3666" s="T281">swan-OBL.3SG-COM</ta>
            <ta e="T283" id="Seg_3667" s="T282">here</ta>
            <ta e="T284" id="Seg_3668" s="T283">go.away-CO.[3SG.S]</ta>
            <ta e="T285" id="Seg_3669" s="T284">go.away-CO.[3SG.S]</ta>
            <ta e="T286" id="Seg_3670" s="T285">home</ta>
            <ta e="T287" id="Seg_3671" s="T286">one</ta>
            <ta e="T288" id="Seg_3672" s="T287">middle-ADV.LOC</ta>
            <ta e="T289" id="Seg_3673" s="T288">middle-LOC</ta>
            <ta e="T290" id="Seg_3674" s="T289">apparently</ta>
            <ta e="T291" id="Seg_3675" s="T290">Russian.[NOM]</ta>
            <ta e="T292" id="Seg_3676" s="T291">goose-PL-EP-ACC</ta>
            <ta e="T293" id="Seg_3677" s="T292">thus.much</ta>
            <ta e="T294" id="Seg_3678" s="T293">shepherd-HAB-3SG.O</ta>
            <ta e="T295" id="Seg_3679" s="T294">end.[NOM]-3SG</ta>
            <ta e="T296" id="Seg_3680" s="T295">NEG</ta>
            <ta e="T297" id="Seg_3681" s="T296">be.visible.[3SG.S]</ta>
            <ta e="T298" id="Seg_3682" s="T297">probably</ta>
            <ta e="T299" id="Seg_3683" s="T298">goose-ACC</ta>
            <ta e="T300" id="Seg_3684" s="T299">take-PST-1SG.O</ta>
            <ta e="T301" id="Seg_3685" s="T300">CONJ</ta>
            <ta e="T302" id="Seg_3686" s="T301">this</ta>
            <ta e="T303" id="Seg_3687" s="T302">swan-ACC-3SG</ta>
            <ta e="T304" id="Seg_3688" s="T303">goose-GEN</ta>
            <ta e="T305" id="Seg_3689" s="T304">for</ta>
            <ta e="T306" id="Seg_3690" s="T305">away</ta>
            <ta e="T307" id="Seg_3691" s="T306">give-PFV-CO-3SG.O</ta>
            <ta e="T308" id="Seg_3692" s="T307">then</ta>
            <ta e="T309" id="Seg_3693" s="T308">goose-OBL.3SG-COM</ta>
            <ta e="T310" id="Seg_3694" s="T309">go.away-CO.[3SG.S]</ta>
            <ta e="T311" id="Seg_3695" s="T310">go.away-CO.[3SG.S]-EMPH</ta>
            <ta e="T312" id="Seg_3696" s="T311">one</ta>
            <ta e="T313" id="Seg_3697" s="T312">old.man-ADJZ</ta>
            <ta e="T314" id="Seg_3698" s="T313">piece.[NOM]</ta>
            <ta e="T315" id="Seg_3699" s="T314">it.is.said</ta>
            <ta e="T316" id="Seg_3700" s="T315">that</ta>
            <ta e="T317" id="Seg_3701" s="T316">this-GEN</ta>
            <ta e="T318" id="Seg_3702" s="T317">as.large.as-ADJZ</ta>
            <ta e="T319" id="Seg_3703" s="T318">as.large.as-ADJZ</ta>
            <ta e="T320" id="Seg_3704" s="T319">needle.[NOM]-3SG</ta>
            <ta e="T321" id="Seg_3705" s="T320">crook-VBLZ-PST.NAR-3SG.O</ta>
            <ta e="T322" id="Seg_3706" s="T321">needle.[NOM]</ta>
            <ta e="T323" id="Seg_3707" s="T322">and</ta>
            <ta e="T324" id="Seg_3708" s="T323">needle.[NOM]</ta>
            <ta e="T325" id="Seg_3709" s="T324">hey</ta>
            <ta e="T326" id="Seg_3710" s="T325">mind.[NOM]-3SG</ta>
            <ta e="T327" id="Seg_3711" s="T326">so</ta>
            <ta e="T328" id="Seg_3712" s="T327">turn-DECAUS-CO.[3SG.S]</ta>
            <ta e="T329" id="Seg_3713" s="T328">hey</ta>
            <ta e="T330" id="Seg_3714" s="T329">eye-CAR-ADJZ</ta>
            <ta e="T331" id="Seg_3715" s="T330">devil-EP-CO-1SG.S</ta>
            <ta e="T332" id="Seg_3716" s="T331">friend.[NOM]</ta>
            <ta e="T333" id="Seg_3717" s="T332">what-EP-ACC</ta>
            <ta e="T334" id="Seg_3718" s="T333">I.NOM</ta>
            <ta e="T335" id="Seg_3719" s="T334">sew-INF</ta>
            <ta e="T336" id="Seg_3720" s="T335">what-ACC</ta>
            <ta e="T337" id="Seg_3721" s="T336">sew-INF</ta>
            <ta e="T338" id="Seg_3722" s="T337">probably</ta>
            <ta e="T339" id="Seg_3723" s="T338">this</ta>
            <ta e="T340" id="Seg_3724" s="T339">old.man-GEN</ta>
            <ta e="T341" id="Seg_3725" s="T340">needle-ACC</ta>
            <ta e="T342" id="Seg_3726" s="T341">take-PST-1SG.O</ta>
            <ta e="T343" id="Seg_3727" s="T342">CONJ</ta>
            <ta e="T344" id="Seg_3728" s="T343">that</ta>
            <ta e="T345" id="Seg_3729" s="T344">needle.[NOM]</ta>
            <ta e="T346" id="Seg_3730" s="T345">for</ta>
            <ta e="T347" id="Seg_3731" s="T346">away</ta>
            <ta e="T348" id="Seg_3732" s="T347">give-CO-3SG.O</ta>
            <ta e="T349" id="Seg_3733" s="T348">goose.[NOM]-3SG</ta>
            <ta e="T350" id="Seg_3734" s="T349">give-PFV-3SG.O</ta>
            <ta e="T351" id="Seg_3735" s="T350">needle-GEN</ta>
            <ta e="T352" id="Seg_3736" s="T351">for</ta>
            <ta e="T353" id="Seg_3737" s="T352">then</ta>
            <ta e="T354" id="Seg_3738" s="T353">this</ta>
            <ta e="T355" id="Seg_3739" s="T354">needle-OBL.3SG-COM</ta>
            <ta e="T356" id="Seg_3740" s="T355">go.away-CO.[3SG.S]</ta>
            <ta e="T357" id="Seg_3741" s="T356">go.away-CO.[3SG.S]</ta>
            <ta e="T358" id="Seg_3742" s="T357">needle.[NOM]</ta>
            <ta e="T359" id="Seg_3743" s="T358">be.lost-PFV-PST.[3SG.S]</ta>
            <ta e="T360" id="Seg_3744" s="T359">%%.[NOM]</ta>
            <ta e="T361" id="Seg_3745" s="T360">through</ta>
            <ta e="T362" id="Seg_3746" s="T361">house-ILL</ta>
            <ta e="T363" id="Seg_3747" s="T362">hardly</ta>
            <ta e="T364" id="Seg_3748" s="T363">come.in-CO.[3SG.S]</ta>
            <ta e="T365" id="Seg_3749" s="T364">needle.[NOM]</ta>
            <ta e="T366" id="Seg_3750" s="T365">be.lost-PFV-PST.[3SG.S]</ta>
            <ta e="T367" id="Seg_3751" s="T366">this</ta>
            <ta e="T368" id="Seg_3752" s="T367">mudroom.[NOM]</ta>
            <ta e="T369" id="Seg_3753" s="T368">inside-LOC</ta>
            <ta e="T370" id="Seg_3754" s="T369">here</ta>
            <ta e="T371" id="Seg_3755" s="T370">look.for-CO-3SG.O</ta>
            <ta e="T372" id="Seg_3756" s="T371">touch-DRV-CVB</ta>
            <ta e="T373" id="Seg_3757" s="T372">you.SG.NOM</ta>
            <ta e="T374" id="Seg_3758" s="T373">what.[NOM]</ta>
            <ta e="T375" id="Seg_3759" s="T374">look.for-CO-2SG.O</ta>
            <ta e="T376" id="Seg_3760" s="T375">old.man.[NOM]</ta>
            <ta e="T377" id="Seg_3761" s="T376">to.the.other.side-ADJZ</ta>
            <ta e="T378" id="Seg_3762" s="T377">house-EP-ADJZ</ta>
            <ta e="T379" id="Seg_3763" s="T378">human.being.[NOM]</ta>
            <ta e="T380" id="Seg_3764" s="T379">hey</ta>
            <ta e="T381" id="Seg_3765" s="T380">I.NOM</ta>
            <ta e="T382" id="Seg_3766" s="T381">needle-ACC</ta>
            <ta e="T383" id="Seg_3767" s="T382">hardly</ta>
            <ta e="T384" id="Seg_3768" s="T383">bring-INFER-1SG.O</ta>
            <ta e="T385" id="Seg_3769" s="T384">that</ta>
            <ta e="T386" id="Seg_3770" s="T385">needle.[NOM]</ta>
            <ta e="T387" id="Seg_3771" s="T386">be.lost-TR-INFER-1SG.O</ta>
            <ta e="T388" id="Seg_3772" s="T387">needle.[NOM]-1SG</ta>
            <ta e="T389" id="Seg_3773" s="T388">this</ta>
            <ta e="T390" id="Seg_3774" s="T389">look.for-IPFV-1SG.O</ta>
            <ta e="T391" id="Seg_3775" s="T390">oh</ta>
            <ta e="T392" id="Seg_3776" s="T391">old.woman-2SG.ILL</ta>
            <ta e="T393" id="Seg_3777" s="T392">house.[NOM]</ta>
            <ta e="T394" id="Seg_3778" s="T393">come.in-IMP.2SG.S</ta>
            <ta e="T395" id="Seg_3779" s="T394">eat-EP-FRQ-IMP.2SG.S</ta>
            <ta e="T396" id="Seg_3780" s="T395">here</ta>
            <ta e="T397" id="Seg_3781" s="T396">INFER</ta>
            <ta e="T398" id="Seg_3782" s="T397">be-INFER.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3783" s="T0">старик.[NOM]</ta>
            <ta e="T2" id="Seg_3784" s="T1">первый</ta>
            <ta e="T3" id="Seg_3785" s="T2">вода.[NOM]</ta>
            <ta e="T4" id="Seg_3786" s="T3">замерзнет</ta>
            <ta e="T5" id="Seg_3787" s="T4">когда</ta>
            <ta e="T6" id="Seg_3788" s="T5">вода-EP-ACC</ta>
            <ta e="T7" id="Seg_3789" s="T6">съесть-PFV-CO-3SG.O</ta>
            <ta e="T8" id="Seg_3790" s="T7">старик.[NOM]</ta>
            <ta e="T9" id="Seg_3791" s="T8">отправиться.на.охоту-CVB</ta>
            <ta e="T10" id="Seg_3792" s="T9">рыба-VBLZ-CVB</ta>
            <ta e="T11" id="Seg_3793" s="T10">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T12" id="Seg_3794" s="T11">большая.река-ACC</ta>
            <ta e="T13" id="Seg_3795" s="T12">когда</ta>
            <ta e="T14" id="Seg_3796" s="T13">схватить-MULO-CO-3SG.O</ta>
            <ta e="T15" id="Seg_3797" s="T14">старик.[NOM]</ta>
            <ta e="T16" id="Seg_3798" s="T15">большая.река-GEN</ta>
            <ta e="T17" id="Seg_3799" s="T16">берег-PROL</ta>
            <ta e="T18" id="Seg_3800" s="T17">вперёд</ta>
            <ta e="T19" id="Seg_3801" s="T18">взглянуть-MULO-HAB-CVB</ta>
            <ta e="T20" id="Seg_3802" s="T19">идти-CO.[3SG.S]</ta>
            <ta e="T21" id="Seg_3803" s="T20">зверь-TRL</ta>
            <ta e="T22" id="Seg_3804" s="T21">тот</ta>
            <ta e="T23" id="Seg_3805" s="T22">туда</ta>
            <ta e="T24" id="Seg_3806" s="T23">взглянуть-MOM-HAB-CVB</ta>
            <ta e="T25" id="Seg_3807" s="T24">ой</ta>
            <ta e="T26" id="Seg_3808" s="T25">один</ta>
            <ta e="T27" id="Seg_3809" s="T26">середина-LOC</ta>
            <ta e="T28" id="Seg_3810" s="T27">он(а).[NOM]</ta>
            <ta e="T29" id="Seg_3811" s="T28">DEF</ta>
            <ta e="T30" id="Seg_3812" s="T29">что.[NOM]</ta>
            <ta e="T31" id="Seg_3813" s="T30">сила-VBLZ-IPFV.[3SG.S]</ta>
            <ta e="T32" id="Seg_3814" s="T31">то.ли</ta>
            <ta e="T33" id="Seg_3815" s="T32">человек.[NOM]</ta>
            <ta e="T34" id="Seg_3816" s="T33">крик-VBLZ-IPFV.[3SG.S]</ta>
            <ta e="T35" id="Seg_3817" s="T34">то.ли</ta>
            <ta e="T36" id="Seg_3818" s="T35">что.[NOM]</ta>
            <ta e="T37" id="Seg_3819" s="T36">сила-VBLZ-IPFV.[3SG.S]</ta>
            <ta e="T38" id="Seg_3820" s="T37">он(а).[NOM]</ta>
            <ta e="T39" id="Seg_3821" s="T38">что.[NOM]</ta>
            <ta e="T40" id="Seg_3822" s="T39">человек.[NOM]</ta>
            <ta e="T41" id="Seg_3823" s="T40">ли</ta>
            <ta e="T42" id="Seg_3824" s="T41">что.[NOM]</ta>
            <ta e="T43" id="Seg_3825" s="T42">крик-VBLZ-IPFV.[3SG.S]</ta>
            <ta e="T44" id="Seg_3826" s="T43">старик.[NOM]</ta>
            <ta e="T45" id="Seg_3827" s="T44">так</ta>
            <ta e="T46" id="Seg_3828" s="T45">это.[NOM]</ta>
            <ta e="T47" id="Seg_3829" s="T46">слышаться-TR-3SG.O</ta>
            <ta e="T48" id="Seg_3830" s="T47">отсюда</ta>
            <ta e="T49" id="Seg_3831" s="T48">прийти-IPFV.[3SG.S]</ta>
            <ta e="T50" id="Seg_3832" s="T49">ой</ta>
            <ta e="T51" id="Seg_3833" s="T50">туда</ta>
            <ta e="T52" id="Seg_3834" s="T51">полынья-LOC</ta>
            <ta e="T53" id="Seg_3835" s="T52">что-EP-ADJZ</ta>
            <ta e="T54" id="Seg_3836" s="T53">кусок.[NOM]</ta>
            <ta e="T55" id="Seg_3837" s="T54">быть-PST.[3SG.S]</ta>
            <ta e="T56" id="Seg_3838" s="T55">полынья-LOC</ta>
            <ta e="T57" id="Seg_3839" s="T56">будто</ta>
            <ta e="T58" id="Seg_3840" s="T57">что-EP-ADJZ</ta>
            <ta e="T59" id="Seg_3841" s="T58">кусок.[NOM]</ta>
            <ta e="T60" id="Seg_3842" s="T59">показаться-PFV.[3SG.S]</ta>
            <ta e="T61" id="Seg_3843" s="T60">вверх</ta>
            <ta e="T62" id="Seg_3844" s="T61">едва</ta>
            <ta e="T63" id="Seg_3845" s="T62">%%</ta>
            <ta e="T64" id="Seg_3846" s="T63">карабкаться-MOM-DUR.[3SG.S]</ta>
            <ta e="T65" id="Seg_3847" s="T64">опять</ta>
            <ta e="T66" id="Seg_3848" s="T65">вниз</ta>
            <ta e="T67" id="Seg_3849" s="T66">отправиться-PFV-CO.[3SG.S]</ta>
            <ta e="T68" id="Seg_3850" s="T67">опять</ta>
            <ta e="T69" id="Seg_3851" s="T68">вниз</ta>
            <ta e="T70" id="Seg_3852" s="T69">уйти-PFV-CO.[3SG.S]</ta>
            <ta e="T71" id="Seg_3853" s="T70">туда</ta>
            <ta e="T72" id="Seg_3854" s="T71">только</ta>
            <ta e="T73" id="Seg_3855" s="T72">отправиться-CO.[3SG.S]</ta>
            <ta e="T74" id="Seg_3856" s="T73">видать</ta>
            <ta e="T75" id="Seg_3857" s="T74">человек.[NOM]</ta>
            <ta e="T76" id="Seg_3858" s="T75">вода-ILL</ta>
            <ta e="T77" id="Seg_3859" s="T76">попасть-MULO-PST.NAR.[3SG.S]</ta>
            <ta e="T78" id="Seg_3860" s="T77">полынья-ILL</ta>
            <ta e="T79" id="Seg_3861" s="T78">пожалуйста</ta>
            <ta e="T80" id="Seg_3862" s="T79">крик-VBLZ-CO.[3SG.S]</ta>
            <ta e="T81" id="Seg_3863" s="T80">может.быть</ta>
            <ta e="T82" id="Seg_3864" s="T81">вверх</ta>
            <ta e="T83" id="Seg_3865" s="T82">я.ACC</ta>
            <ta e="T84" id="Seg_3866" s="T83">взять-EP-IMP.2SG.S</ta>
            <ta e="T85" id="Seg_3867" s="T84">здесь</ta>
            <ta e="T86" id="Seg_3868" s="T85">этот</ta>
            <ta e="T87" id="Seg_3869" s="T86">старик.[NOM]</ta>
            <ta e="T88" id="Seg_3870" s="T87">вверх</ta>
            <ta e="T89" id="Seg_3871" s="T88">побежать.[3SG.S]</ta>
            <ta e="T90" id="Seg_3872" s="T89">длинный</ta>
            <ta e="T91" id="Seg_3873" s="T90">тальник-ACC</ta>
            <ta e="T92" id="Seg_3874" s="T91">взять-CO-3SG.O</ta>
            <ta e="T93" id="Seg_3875" s="T92">длинный</ta>
            <ta e="T94" id="Seg_3876" s="T93">тальник-ACC</ta>
            <ta e="T95" id="Seg_3877" s="T94">рубить-MOM-TR-3SG.O</ta>
            <ta e="T96" id="Seg_3878" s="T95">вниз</ta>
            <ta e="T97" id="Seg_3879" s="T96">взять-HAB-3SG.O</ta>
            <ta e="T98" id="Seg_3880" s="T97">взять-HAB-PFV-3SG.O</ta>
            <ta e="T99" id="Seg_3881" s="T98">бросать-PFV-3SG.O</ta>
            <ta e="T100" id="Seg_3882" s="T99">старик-ALL</ta>
            <ta e="T101" id="Seg_3883" s="T100">этот</ta>
            <ta e="T102" id="Seg_3884" s="T101">тальник-INSTR</ta>
            <ta e="T103" id="Seg_3885" s="T102">вверх</ta>
            <ta e="T104" id="Seg_3886" s="T103">вверх</ta>
            <ta e="T105" id="Seg_3887" s="T104">тащить-MULO-PFV-CO-3SG.O</ta>
            <ta e="T106" id="Seg_3888" s="T105">вверх</ta>
            <ta e="T107" id="Seg_3889" s="T106">только</ta>
            <ta e="T108" id="Seg_3890" s="T107">тащить-MULO-PFV-CO-3SG.O</ta>
            <ta e="T109" id="Seg_3891" s="T108">ой</ta>
            <ta e="T110" id="Seg_3892" s="T109">ой</ta>
            <ta e="T111" id="Seg_3893" s="T110">видать</ta>
            <ta e="T112" id="Seg_3894" s="T111">купить-человек.[NOM]</ta>
            <ta e="T113" id="Seg_3895" s="T112">купец.[NOM]</ta>
            <ta e="T114" id="Seg_3896" s="T113">купец.[NOM]</ta>
            <ta e="T115" id="Seg_3897" s="T114">этот</ta>
            <ta e="T116" id="Seg_3898" s="T115">богач.[NOM]</ta>
            <ta e="T117" id="Seg_3899" s="T116">ты.NOM</ta>
            <ta e="T118" id="Seg_3900" s="T117">я.ACC</ta>
            <ta e="T119" id="Seg_3901" s="T118">вверх</ta>
            <ta e="T120" id="Seg_3902" s="T119">я.ACC</ta>
            <ta e="T121" id="Seg_3903" s="T120">взять-CO-2SG.S</ta>
            <ta e="T122" id="Seg_3904" s="T121">умереть-FRQ-ABST-GEN</ta>
            <ta e="T123" id="Seg_3905" s="T122">от-ADV.EL</ta>
            <ta e="T124" id="Seg_3906" s="T123">вверх</ta>
            <ta e="T125" id="Seg_3907" s="T124">я.ACC</ta>
            <ta e="T126" id="Seg_3908" s="T125">взять-CO-2SG.S</ta>
            <ta e="T127" id="Seg_3909" s="T126">вверх</ta>
            <ta e="T128" id="Seg_3910" s="T127">я.ACC</ta>
            <ta e="T129" id="Seg_3911" s="T128">взять-CO-2SG.S</ta>
            <ta e="T130" id="Seg_3912" s="T129">золотой-ADJZ</ta>
            <ta e="T131" id="Seg_3913" s="T130">кусок-INSTR</ta>
            <ta e="T132" id="Seg_3914" s="T131">дать-CO-3SG.O</ta>
            <ta e="T133" id="Seg_3915" s="T132">словно</ta>
            <ta e="T134" id="Seg_3916" s="T133">умереть-FRQ-ABST-GEN</ta>
            <ta e="T135" id="Seg_3917" s="T134">из-ADV.EL</ta>
            <ta e="T136" id="Seg_3918" s="T135">вверх</ta>
            <ta e="T137" id="Seg_3919" s="T136">я.ACC</ta>
            <ta e="T138" id="Seg_3920" s="T137">взять-CO-2SG.S</ta>
            <ta e="T139" id="Seg_3921" s="T138">что.[NOM]</ta>
            <ta e="T140" id="Seg_3922" s="T139">взять-FUT-2SG.O</ta>
            <ta e="T141" id="Seg_3923" s="T140">что.[NOM]</ta>
            <ta e="T142" id="Seg_3924" s="T141">жить-ACTN-ADJZ</ta>
            <ta e="T143" id="Seg_3925" s="T142">взять-FUT-2SG.O</ta>
            <ta e="T144" id="Seg_3926" s="T143">INFER</ta>
            <ta e="T145" id="Seg_3927" s="T144">отправиться-INFER.[3SG.S]-EMPH</ta>
            <ta e="T146" id="Seg_3928" s="T145">эй</ta>
            <ta e="T147" id="Seg_3929" s="T146">лошадь.[NOM]</ta>
            <ta e="T148" id="Seg_3930" s="T147">один</ta>
            <ta e="T149" id="Seg_3931" s="T148">русский.[NOM]</ta>
            <ta e="T150" id="Seg_3932" s="T149">вот</ta>
            <ta e="T151" id="Seg_3933" s="T150">видать</ta>
            <ta e="T152" id="Seg_3934" s="T151">лошадь-ACC</ta>
            <ta e="T153" id="Seg_3935" s="T152">погнать-HAB-3SG.O</ta>
            <ta e="T154" id="Seg_3936" s="T153">конец.[NOM]-3SG</ta>
            <ta e="T155" id="Seg_3937" s="T154">NEG</ta>
            <ta e="T156" id="Seg_3938" s="T155">виднеться.[3SG.S]</ta>
            <ta e="T157" id="Seg_3939" s="T156">потом</ta>
            <ta e="T158" id="Seg_3940" s="T157">прийти-IPFV.[3SG.S]</ta>
            <ta e="T159" id="Seg_3941" s="T158">эй</ta>
            <ta e="T160" id="Seg_3942" s="T159">ум.[NOM]-3SG</ta>
            <ta e="T161" id="Seg_3943" s="T160">так</ta>
            <ta e="T162" id="Seg_3944" s="T161">повернуться-DECAUS-CO.[3SG.S]</ta>
            <ta e="T163" id="Seg_3945" s="T162">наверно</ta>
            <ta e="T164" id="Seg_3946" s="T163">мол</ta>
            <ta e="T165" id="Seg_3947" s="T164">лошадь-ACC</ta>
            <ta e="T166" id="Seg_3948" s="T165">взять-PST-1SG.O</ta>
            <ta e="T167" id="Seg_3949" s="T166">CONJ</ta>
            <ta e="T168" id="Seg_3950" s="T167">работать-SUP.1SG</ta>
            <ta e="T169" id="Seg_3951" s="T168">лошадь.[NOM]</ta>
            <ta e="T170" id="Seg_3952" s="T169">взять-IPFV-3SG.O</ta>
            <ta e="T171" id="Seg_3953" s="T170">золотой-ADJZ</ta>
            <ta e="T172" id="Seg_3954" s="T171">кусок.[NOM]-3SG</ta>
            <ta e="T173" id="Seg_3955" s="T172">прочь</ta>
            <ta e="T174" id="Seg_3956" s="T173">дать-PFV-CO-3SG.O</ta>
            <ta e="T175" id="Seg_3957" s="T174">потом</ta>
            <ta e="T176" id="Seg_3958" s="T175">этот</ta>
            <ta e="T177" id="Seg_3959" s="T176">лошадь-OBL.3SG-COM</ta>
            <ta e="T178" id="Seg_3960" s="T177">уйти-CO.[3SG.S]-EMPH</ta>
            <ta e="T179" id="Seg_3961" s="T178">уйти-CO.[3SG.S]</ta>
            <ta e="T180" id="Seg_3962" s="T179">уйти-CO.[3SG.S]</ta>
            <ta e="T181" id="Seg_3963" s="T180">один</ta>
            <ta e="T182" id="Seg_3964" s="T181">середина-LOC</ta>
            <ta e="T183" id="Seg_3965" s="T182">русский.[NOM]</ta>
            <ta e="T184" id="Seg_3966" s="T183">это-GEN</ta>
            <ta e="T185" id="Seg_3967" s="T184">видать</ta>
            <ta e="T186" id="Seg_3968" s="T185">корова-PL-EP-ACC</ta>
            <ta e="T187" id="Seg_3969" s="T186">погнать-HAB-3SG.O</ta>
            <ta e="T188" id="Seg_3970" s="T187">конец.[NOM]-3SG</ta>
            <ta e="T189" id="Seg_3971" s="T188">NEG</ta>
            <ta e="T190" id="Seg_3972" s="T189">виднеться.[3SG.S]</ta>
            <ta e="T191" id="Seg_3973" s="T190">эй</ta>
            <ta e="T192" id="Seg_3974" s="T191">ум.[NOM]-3SG</ta>
            <ta e="T193" id="Seg_3975" s="T192">так</ta>
            <ta e="T194" id="Seg_3976" s="T193">повернуться-DECAUS-CO.[3SG.S]</ta>
            <ta e="T195" id="Seg_3977" s="T194">молоко-3SG.TRL</ta>
            <ta e="T196" id="Seg_3978" s="T195">молоко-3SG.TRL</ta>
            <ta e="T197" id="Seg_3979" s="T196">корова-ACC</ta>
            <ta e="T198" id="Seg_3980" s="T197">взять-PST-1SG.O</ta>
            <ta e="T199" id="Seg_3981" s="T198">CONJ</ta>
            <ta e="T200" id="Seg_3982" s="T199">лошадь-ACC-3SG</ta>
            <ta e="T201" id="Seg_3983" s="T200">корова.[NOM]</ta>
            <ta e="T202" id="Seg_3984" s="T201">для</ta>
            <ta e="T203" id="Seg_3985" s="T202">прочь</ta>
            <ta e="T204" id="Seg_3986" s="T203">дать-PFV-3SG.O</ta>
            <ta e="T205" id="Seg_3987" s="T204">этот</ta>
            <ta e="T206" id="Seg_3988" s="T205">корова-OBL.3SG-COM</ta>
            <ta e="T207" id="Seg_3989" s="T206">INFER</ta>
            <ta e="T208" id="Seg_3990" s="T207">уйти-INFER.[3SG.S]-EMPH</ta>
            <ta e="T209" id="Seg_3991" s="T208">уйти-INFER.[3SG.S]-EMPH</ta>
            <ta e="T210" id="Seg_3992" s="T209">ой</ta>
            <ta e="T211" id="Seg_3993" s="T210">один</ta>
            <ta e="T212" id="Seg_3994" s="T211">середина-LOC</ta>
            <ta e="T213" id="Seg_3995" s="T212">свинья-PL-EP-ACC</ta>
            <ta e="T214" id="Seg_3996" s="T213">вот</ta>
            <ta e="T215" id="Seg_3997" s="T214">видать</ta>
            <ta e="T216" id="Seg_3998" s="T215">погнать-HAB-3PL</ta>
            <ta e="T217" id="Seg_3999" s="T216">конец.[NOM]-3SG</ta>
            <ta e="T218" id="Seg_4000" s="T217">NEG</ta>
            <ta e="T219" id="Seg_4001" s="T218">виднеться.[3SG.S]</ta>
            <ta e="T220" id="Seg_4002" s="T219">потом</ta>
            <ta e="T221" id="Seg_4003" s="T220">так</ta>
            <ta e="T222" id="Seg_4004" s="T221">сказать-3SG.O</ta>
            <ta e="T223" id="Seg_4005" s="T222">эй</ta>
            <ta e="T224" id="Seg_4006" s="T223">свинья.[NOM]</ta>
            <ta e="T225" id="Seg_4007" s="T224">это-GEN</ta>
            <ta e="T226" id="Seg_4008" s="T225">видать</ta>
            <ta e="T227" id="Seg_4009" s="T226">жир-EP-ADVZ</ta>
            <ta e="T228" id="Seg_4010" s="T227">быть-CO.[3SG.S]</ta>
            <ta e="T229" id="Seg_4011" s="T228">эй</ta>
            <ta e="T230" id="Seg_4012" s="T229">будто</ta>
            <ta e="T231" id="Seg_4013" s="T230">свинья.[NOM]</ta>
            <ta e="T232" id="Seg_4014" s="T231">жирный-ADVZ</ta>
            <ta e="T233" id="Seg_4015" s="T232">быть-CO.[3SG.S]</ta>
            <ta e="T234" id="Seg_4016" s="T233">свинья-ACC</ta>
            <ta e="T235" id="Seg_4017" s="T234">взять-PST-1SG.O</ta>
            <ta e="T236" id="Seg_4018" s="T235">CONJ</ta>
            <ta e="T237" id="Seg_4019" s="T236">свинья-GEN</ta>
            <ta e="T238" id="Seg_4020" s="T237">для</ta>
            <ta e="T239" id="Seg_4021" s="T238">прочь</ta>
            <ta e="T240" id="Seg_4022" s="T239">дать-CO-3SG.O</ta>
            <ta e="T241" id="Seg_4023" s="T240">этот</ta>
            <ta e="T242" id="Seg_4024" s="T241">корова-ACC-3SG</ta>
            <ta e="T243" id="Seg_4025" s="T242">опять</ta>
            <ta e="T244" id="Seg_4026" s="T243">меняться</ta>
            <ta e="T245" id="Seg_4027" s="T244">потом</ta>
            <ta e="T246" id="Seg_4028" s="T245">этот</ta>
            <ta e="T247" id="Seg_4029" s="T246">свинья-OBL.3SG-COM</ta>
            <ta e="T248" id="Seg_4030" s="T247">INFER</ta>
            <ta e="T249" id="Seg_4031" s="T248">уйти-INFER.[3SG.S]</ta>
            <ta e="T250" id="Seg_4032" s="T249">INFER</ta>
            <ta e="T251" id="Seg_4033" s="T250">уйти-INFER.[3SG.S]-EMPH</ta>
            <ta e="T252" id="Seg_4034" s="T251">один</ta>
            <ta e="T253" id="Seg_4035" s="T252">целый</ta>
            <ta e="T254" id="Seg_4036" s="T253">середина-LOC</ta>
            <ta e="T255" id="Seg_4037" s="T254">этот</ta>
            <ta e="T256" id="Seg_4038" s="T255">русский.[NOM]</ta>
            <ta e="T257" id="Seg_4039" s="T256">вырастить-PTCP.PST</ta>
            <ta e="T258" id="Seg_4040" s="T257">лебедь-PL-EP-ACC</ta>
            <ta e="T259" id="Seg_4041" s="T258">погнать-HAB.[3SG.S]</ta>
            <ta e="T260" id="Seg_4042" s="T259">лебедь-PL-EP-ACC</ta>
            <ta e="T261" id="Seg_4043" s="T260">настолько</ta>
            <ta e="T262" id="Seg_4044" s="T261">погнать-HAB-3SG.O</ta>
            <ta e="T263" id="Seg_4045" s="T262">конец.[NOM]-3SG</ta>
            <ta e="T264" id="Seg_4046" s="T263">NEG.EX.[3SG.S]</ta>
            <ta e="T265" id="Seg_4047" s="T264">наверно</ta>
            <ta e="T266" id="Seg_4048" s="T265">яйцо.[NOM]-3SG</ta>
            <ta e="T267" id="Seg_4049" s="T266">INDEF2 </ta>
            <ta e="T268" id="Seg_4050" s="T267">много</ta>
            <ta e="T269" id="Seg_4051" s="T268">лебедь-ACC</ta>
            <ta e="T270" id="Seg_4052" s="T269">взять-PST-1SG.O</ta>
            <ta e="T271" id="Seg_4053" s="T270">CONJ</ta>
            <ta e="T272" id="Seg_4054" s="T271">яйцо-3SG.TRL</ta>
            <ta e="T273" id="Seg_4055" s="T272">яйцо.[NOM]-3SG</ta>
            <ta e="T274" id="Seg_4056" s="T273">для</ta>
            <ta e="T275" id="Seg_4057" s="T274">лебедь-GEN</ta>
            <ta e="T276" id="Seg_4058" s="T275">для</ta>
            <ta e="T277" id="Seg_4059" s="T276">свинья-ACC-3SG</ta>
            <ta e="T278" id="Seg_4060" s="T277">прочь</ta>
            <ta e="T279" id="Seg_4061" s="T278">дать-PFV-3SG.O</ta>
            <ta e="T280" id="Seg_4062" s="T279">потом</ta>
            <ta e="T281" id="Seg_4063" s="T280">этот</ta>
            <ta e="T282" id="Seg_4064" s="T281">лебедь-OBL.3SG-COM</ta>
            <ta e="T283" id="Seg_4065" s="T282">вот</ta>
            <ta e="T284" id="Seg_4066" s="T283">уйти-CO.[3SG.S]</ta>
            <ta e="T285" id="Seg_4067" s="T284">уйти-CO.[3SG.S]</ta>
            <ta e="T286" id="Seg_4068" s="T285">домой</ta>
            <ta e="T287" id="Seg_4069" s="T286">один</ta>
            <ta e="T288" id="Seg_4070" s="T287">середина-ADV.LOC</ta>
            <ta e="T289" id="Seg_4071" s="T288">середина-LOC</ta>
            <ta e="T290" id="Seg_4072" s="T289">видать</ta>
            <ta e="T291" id="Seg_4073" s="T290">русский.[NOM]</ta>
            <ta e="T292" id="Seg_4074" s="T291">гусь-PL-EP-ACC</ta>
            <ta e="T293" id="Seg_4075" s="T292">настолько</ta>
            <ta e="T294" id="Seg_4076" s="T293">погнать-HAB-3SG.O</ta>
            <ta e="T295" id="Seg_4077" s="T294">конец.[NOM]-3SG</ta>
            <ta e="T296" id="Seg_4078" s="T295">NEG</ta>
            <ta e="T297" id="Seg_4079" s="T296">виднеться.[3SG.S]</ta>
            <ta e="T298" id="Seg_4080" s="T297">наверно</ta>
            <ta e="T299" id="Seg_4081" s="T298">гусь-ACC</ta>
            <ta e="T300" id="Seg_4082" s="T299">взять-PST-1SG.O</ta>
            <ta e="T301" id="Seg_4083" s="T300">CONJ</ta>
            <ta e="T302" id="Seg_4084" s="T301">этот</ta>
            <ta e="T303" id="Seg_4085" s="T302">лебедь-ACC-3SG</ta>
            <ta e="T304" id="Seg_4086" s="T303">гусь-GEN</ta>
            <ta e="T305" id="Seg_4087" s="T304">для</ta>
            <ta e="T306" id="Seg_4088" s="T305">прочь</ta>
            <ta e="T307" id="Seg_4089" s="T306">дать-PFV-CO-3SG.O</ta>
            <ta e="T308" id="Seg_4090" s="T307">потом</ta>
            <ta e="T309" id="Seg_4091" s="T308">гусь-OBL.3SG-COM</ta>
            <ta e="T310" id="Seg_4092" s="T309">уйти-CO.[3SG.S]</ta>
            <ta e="T311" id="Seg_4093" s="T310">уйти-CO.[3SG.S]-EMPH</ta>
            <ta e="T312" id="Seg_4094" s="T311">один</ta>
            <ta e="T313" id="Seg_4095" s="T312">старик-ADJZ</ta>
            <ta e="T314" id="Seg_4096" s="T313">кусок.[NOM]</ta>
            <ta e="T315" id="Seg_4097" s="T314">мол</ta>
            <ta e="T316" id="Seg_4098" s="T315">тот</ta>
            <ta e="T317" id="Seg_4099" s="T316">это-GEN</ta>
            <ta e="T318" id="Seg_4100" s="T317">величиной.с-ADJZ</ta>
            <ta e="T319" id="Seg_4101" s="T318">величиной.с-ADJZ</ta>
            <ta e="T320" id="Seg_4102" s="T319">игла.[NOM]-3SG</ta>
            <ta e="T321" id="Seg_4103" s="T320">посох-VBLZ-PST.NAR-3SG.O</ta>
            <ta e="T322" id="Seg_4104" s="T321">игла.[NOM]</ta>
            <ta e="T323" id="Seg_4105" s="T322">и</ta>
            <ta e="T324" id="Seg_4106" s="T323">игла.[NOM]</ta>
            <ta e="T325" id="Seg_4107" s="T324">эй</ta>
            <ta e="T326" id="Seg_4108" s="T325">ум.[NOM]-3SG</ta>
            <ta e="T327" id="Seg_4109" s="T326">так</ta>
            <ta e="T328" id="Seg_4110" s="T327">повернуться-DECAUS-CO.[3SG.S]</ta>
            <ta e="T329" id="Seg_4111" s="T328">эй</ta>
            <ta e="T330" id="Seg_4112" s="T329">глаз-CAR-ADJZ</ta>
            <ta e="T331" id="Seg_4113" s="T330">черт-EP-CO-1SG.S</ta>
            <ta e="T332" id="Seg_4114" s="T331">товарищ.[NOM]</ta>
            <ta e="T333" id="Seg_4115" s="T332">что-EP-ACC</ta>
            <ta e="T334" id="Seg_4116" s="T333">я.NOM</ta>
            <ta e="T335" id="Seg_4117" s="T334">сшить-INF</ta>
            <ta e="T336" id="Seg_4118" s="T335">что-ACC</ta>
            <ta e="T337" id="Seg_4119" s="T336">сшить-INF</ta>
            <ta e="T338" id="Seg_4120" s="T337">наверно</ta>
            <ta e="T339" id="Seg_4121" s="T338">этот</ta>
            <ta e="T340" id="Seg_4122" s="T339">старик-GEN</ta>
            <ta e="T341" id="Seg_4123" s="T340">игла-ACC</ta>
            <ta e="T342" id="Seg_4124" s="T341">взять-PST-1SG.O</ta>
            <ta e="T343" id="Seg_4125" s="T342">CONJ</ta>
            <ta e="T344" id="Seg_4126" s="T343">тот</ta>
            <ta e="T345" id="Seg_4127" s="T344">игла.[NOM]</ta>
            <ta e="T346" id="Seg_4128" s="T345">для</ta>
            <ta e="T347" id="Seg_4129" s="T346">прочь</ta>
            <ta e="T348" id="Seg_4130" s="T347">дать-CO-3SG.O</ta>
            <ta e="T349" id="Seg_4131" s="T348">гусь.[NOM]-3SG</ta>
            <ta e="T350" id="Seg_4132" s="T349">дать-PFV-3SG.O</ta>
            <ta e="T351" id="Seg_4133" s="T350">игла-GEN</ta>
            <ta e="T352" id="Seg_4134" s="T351">для</ta>
            <ta e="T353" id="Seg_4135" s="T352">потом</ta>
            <ta e="T354" id="Seg_4136" s="T353">этот</ta>
            <ta e="T355" id="Seg_4137" s="T354">игла-OBL.3SG-COM</ta>
            <ta e="T356" id="Seg_4138" s="T355">уйти-CO.[3SG.S]</ta>
            <ta e="T357" id="Seg_4139" s="T356">уйти-CO.[3SG.S]</ta>
            <ta e="T358" id="Seg_4140" s="T357">игла.[NOM]</ta>
            <ta e="T359" id="Seg_4141" s="T358">потеряться-PFV-PST.[3SG.S]</ta>
            <ta e="T360" id="Seg_4142" s="T359">%%.[NOM]</ta>
            <ta e="T361" id="Seg_4143" s="T360">сквозь</ta>
            <ta e="T362" id="Seg_4144" s="T361">дом-ILL</ta>
            <ta e="T363" id="Seg_4145" s="T362">едва</ta>
            <ta e="T364" id="Seg_4146" s="T363">войти-CO.[3SG.S]</ta>
            <ta e="T365" id="Seg_4147" s="T364">игла.[NOM]</ta>
            <ta e="T366" id="Seg_4148" s="T365">потеряться-PFV-PST.[3SG.S]</ta>
            <ta e="T367" id="Seg_4149" s="T366">этот</ta>
            <ta e="T368" id="Seg_4150" s="T367">сени.[NOM]</ta>
            <ta e="T369" id="Seg_4151" s="T368">внутри-LOC</ta>
            <ta e="T370" id="Seg_4152" s="T369">вот</ta>
            <ta e="T371" id="Seg_4153" s="T370">искать-CO-3SG.O</ta>
            <ta e="T372" id="Seg_4154" s="T371">щупать-DRV-CVB</ta>
            <ta e="T373" id="Seg_4155" s="T372">ты.NOM</ta>
            <ta e="T374" id="Seg_4156" s="T373">что.[NOM]</ta>
            <ta e="T375" id="Seg_4157" s="T374">искать-CO-2SG.O</ta>
            <ta e="T376" id="Seg_4158" s="T375">старик.[NOM]</ta>
            <ta e="T377" id="Seg_4159" s="T376">на.другую.сторону-ADJZ</ta>
            <ta e="T378" id="Seg_4160" s="T377">дом-EP-ADJZ</ta>
            <ta e="T379" id="Seg_4161" s="T378">человек.[NOM]</ta>
            <ta e="T380" id="Seg_4162" s="T379">эй</ta>
            <ta e="T381" id="Seg_4163" s="T380">я.NOM</ta>
            <ta e="T382" id="Seg_4164" s="T381">игла-ACC</ta>
            <ta e="T383" id="Seg_4165" s="T382">едва</ta>
            <ta e="T384" id="Seg_4166" s="T383">принести-INFER-1SG.O</ta>
            <ta e="T385" id="Seg_4167" s="T384">тот</ta>
            <ta e="T386" id="Seg_4168" s="T385">игла.[NOM]</ta>
            <ta e="T387" id="Seg_4169" s="T386">потеряться-TR-INFER-1SG.O</ta>
            <ta e="T388" id="Seg_4170" s="T387">игла.[NOM]-1SG</ta>
            <ta e="T389" id="Seg_4171" s="T388">этот</ta>
            <ta e="T390" id="Seg_4172" s="T389">искать-IPFV-1SG.O</ta>
            <ta e="T391" id="Seg_4173" s="T390">ой</ta>
            <ta e="T392" id="Seg_4174" s="T391">старуха-2SG.ILL</ta>
            <ta e="T393" id="Seg_4175" s="T392">дом.[NOM]</ta>
            <ta e="T394" id="Seg_4176" s="T393">войти-IMP.2SG.S</ta>
            <ta e="T395" id="Seg_4177" s="T394">съесть-EP-FRQ-IMP.2SG.S</ta>
            <ta e="T396" id="Seg_4178" s="T395">здесь</ta>
            <ta e="T397" id="Seg_4179" s="T396">INFER</ta>
            <ta e="T398" id="Seg_4180" s="T397">быть-INFER.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_4181" s="T0">n-n:case3</ta>
            <ta e="T2" id="Seg_4182" s="T1">adj</ta>
            <ta e="T3" id="Seg_4183" s="T2">n-n:case3</ta>
            <ta e="T4" id="Seg_4184" s="T3">v</ta>
            <ta e="T5" id="Seg_4185" s="T4">conj</ta>
            <ta e="T6" id="Seg_4186" s="T5">n-n:(ins)-n:case3</ta>
            <ta e="T7" id="Seg_4187" s="T6">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T8" id="Seg_4188" s="T7">n-n:case3</ta>
            <ta e="T9" id="Seg_4189" s="T8">v-v&gt;adv</ta>
            <ta e="T10" id="Seg_4190" s="T9">n-n&gt;v-v&gt;adv</ta>
            <ta e="T11" id="Seg_4191" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_4192" s="T11">n-n:case3</ta>
            <ta e="T13" id="Seg_4193" s="T12">interrog</ta>
            <ta e="T14" id="Seg_4194" s="T13">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T15" id="Seg_4195" s="T14">n-n:case3</ta>
            <ta e="T16" id="Seg_4196" s="T15">n-n:case3</ta>
            <ta e="T17" id="Seg_4197" s="T16">n-n:case3</ta>
            <ta e="T18" id="Seg_4198" s="T17">adv</ta>
            <ta e="T19" id="Seg_4199" s="T18">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T20" id="Seg_4200" s="T19">v-v:ins-v:pn</ta>
            <ta e="T21" id="Seg_4201" s="T20">n-n:case3</ta>
            <ta e="T22" id="Seg_4202" s="T21">dem</ta>
            <ta e="T23" id="Seg_4203" s="T22">adv</ta>
            <ta e="T24" id="Seg_4204" s="T23">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T25" id="Seg_4205" s="T24">interj</ta>
            <ta e="T26" id="Seg_4206" s="T25">num</ta>
            <ta e="T27" id="Seg_4207" s="T26">n-n:case3</ta>
            <ta e="T28" id="Seg_4208" s="T27">pers-n:case3</ta>
            <ta e="T29" id="Seg_4209" s="T28">clit</ta>
            <ta e="T30" id="Seg_4210" s="T29">interrog-n:case3</ta>
            <ta e="T31" id="Seg_4211" s="T30">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T32" id="Seg_4212" s="T31">conj</ta>
            <ta e="T33" id="Seg_4213" s="T32">n-n:case3</ta>
            <ta e="T34" id="Seg_4214" s="T33">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T35" id="Seg_4215" s="T34">conj</ta>
            <ta e="T36" id="Seg_4216" s="T35">interrog-n:case3</ta>
            <ta e="T37" id="Seg_4217" s="T36">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T38" id="Seg_4218" s="T37">pers-n:case3</ta>
            <ta e="T39" id="Seg_4219" s="T38">interrog-n:case3</ta>
            <ta e="T40" id="Seg_4220" s="T39">n-n:case3</ta>
            <ta e="T41" id="Seg_4221" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_4222" s="T41">interrog-n:case3</ta>
            <ta e="T43" id="Seg_4223" s="T42">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T44" id="Seg_4224" s="T43">n-n:case3</ta>
            <ta e="T45" id="Seg_4225" s="T44">adv</ta>
            <ta e="T46" id="Seg_4226" s="T45">pro-n:case3</ta>
            <ta e="T47" id="Seg_4227" s="T46">v-v&gt;v-v:pn</ta>
            <ta e="T48" id="Seg_4228" s="T47">adv</ta>
            <ta e="T49" id="Seg_4229" s="T48">v-v&gt;v-v:pn</ta>
            <ta e="T50" id="Seg_4230" s="T49">interj</ta>
            <ta e="T51" id="Seg_4231" s="T50">adv</ta>
            <ta e="T52" id="Seg_4232" s="T51">n-n:case3</ta>
            <ta e="T53" id="Seg_4233" s="T52">interrog-n:(ins)-n&gt;adj</ta>
            <ta e="T54" id="Seg_4234" s="T53">n-n:case3</ta>
            <ta e="T55" id="Seg_4235" s="T54">v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_4236" s="T55">n-n:case3</ta>
            <ta e="T57" id="Seg_4237" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_4238" s="T57">interrog-n:(ins)-n&gt;adj</ta>
            <ta e="T59" id="Seg_4239" s="T58">n-n:case3</ta>
            <ta e="T60" id="Seg_4240" s="T59">v-v&gt;v-v:pn</ta>
            <ta e="T61" id="Seg_4241" s="T60">adv</ta>
            <ta e="T62" id="Seg_4242" s="T61">conj</ta>
            <ta e="T63" id="Seg_4243" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_4244" s="T63">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T65" id="Seg_4245" s="T64">adv</ta>
            <ta e="T66" id="Seg_4246" s="T65">adv</ta>
            <ta e="T67" id="Seg_4247" s="T66">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T68" id="Seg_4248" s="T67">adv</ta>
            <ta e="T69" id="Seg_4249" s="T68">adv</ta>
            <ta e="T70" id="Seg_4250" s="T69">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T71" id="Seg_4251" s="T70">adv</ta>
            <ta e="T72" id="Seg_4252" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_4253" s="T72">v-v:ins-v:pn</ta>
            <ta e="T74" id="Seg_4254" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_4255" s="T74">n-n:case3</ta>
            <ta e="T76" id="Seg_4256" s="T75">n-n:case3</ta>
            <ta e="T77" id="Seg_4257" s="T76">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_4258" s="T77">n-n:case3</ta>
            <ta e="T79" id="Seg_4259" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_4260" s="T79">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T81" id="Seg_4261" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_4262" s="T81">adv</ta>
            <ta e="T83" id="Seg_4263" s="T82">pers</ta>
            <ta e="T84" id="Seg_4264" s="T83">v-v:ins-v:mood-pn</ta>
            <ta e="T85" id="Seg_4265" s="T84">adv</ta>
            <ta e="T86" id="Seg_4266" s="T85">dem</ta>
            <ta e="T87" id="Seg_4267" s="T86">n-n:case3</ta>
            <ta e="T88" id="Seg_4268" s="T87">adv</ta>
            <ta e="T89" id="Seg_4269" s="T88">v-v:pn</ta>
            <ta e="T90" id="Seg_4270" s="T89">adj</ta>
            <ta e="T91" id="Seg_4271" s="T90">n-n:case3</ta>
            <ta e="T92" id="Seg_4272" s="T91">v-v:ins-v:pn</ta>
            <ta e="T93" id="Seg_4273" s="T92">adj</ta>
            <ta e="T94" id="Seg_4274" s="T93">n-n:case3</ta>
            <ta e="T95" id="Seg_4275" s="T94">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T96" id="Seg_4276" s="T95">adv</ta>
            <ta e="T97" id="Seg_4277" s="T96">v-v&gt;v-v:pn</ta>
            <ta e="T98" id="Seg_4278" s="T97">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T99" id="Seg_4279" s="T98">v-v&gt;v-v:pn</ta>
            <ta e="T100" id="Seg_4280" s="T99">n-n:case3</ta>
            <ta e="T101" id="Seg_4281" s="T100">dem</ta>
            <ta e="T102" id="Seg_4282" s="T101">n-n:case3</ta>
            <ta e="T103" id="Seg_4283" s="T102">adv</ta>
            <ta e="T104" id="Seg_4284" s="T103">preverb</ta>
            <ta e="T105" id="Seg_4285" s="T104">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T106" id="Seg_4286" s="T105">adv</ta>
            <ta e="T107" id="Seg_4287" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_4288" s="T107">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T109" id="Seg_4289" s="T108">interj</ta>
            <ta e="T110" id="Seg_4290" s="T109">interj</ta>
            <ta e="T111" id="Seg_4291" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_4292" s="T111">v-n-n:case3</ta>
            <ta e="T113" id="Seg_4293" s="T112">n-n:case3</ta>
            <ta e="T114" id="Seg_4294" s="T113">n-n:case3</ta>
            <ta e="T115" id="Seg_4295" s="T114">dem</ta>
            <ta e="T116" id="Seg_4296" s="T115">n-n:case3</ta>
            <ta e="T117" id="Seg_4297" s="T116">pers</ta>
            <ta e="T118" id="Seg_4298" s="T117">pers</ta>
            <ta e="T119" id="Seg_4299" s="T118">adv</ta>
            <ta e="T120" id="Seg_4300" s="T119">pers</ta>
            <ta e="T121" id="Seg_4301" s="T120">v-v:ins-v:pn</ta>
            <ta e="T122" id="Seg_4302" s="T121">v-v&gt;v-v&gt;n-n:case3</ta>
            <ta e="T123" id="Seg_4303" s="T122">pp-adv:advcase</ta>
            <ta e="T124" id="Seg_4304" s="T123">adv</ta>
            <ta e="T125" id="Seg_4305" s="T124">pers</ta>
            <ta e="T126" id="Seg_4306" s="T125">v-v:ins-v:pn</ta>
            <ta e="T127" id="Seg_4307" s="T126">adv</ta>
            <ta e="T128" id="Seg_4308" s="T127">pers</ta>
            <ta e="T129" id="Seg_4309" s="T128">v-v:ins-v:pn</ta>
            <ta e="T130" id="Seg_4310" s="T129">n&gt;adj</ta>
            <ta e="T131" id="Seg_4311" s="T130">n-n:case3</ta>
            <ta e="T132" id="Seg_4312" s="T131">v-v:ins-v:pn</ta>
            <ta e="T133" id="Seg_4313" s="T132">conj</ta>
            <ta e="T134" id="Seg_4314" s="T133">v-v&gt;v-v&gt;n-n:case3</ta>
            <ta e="T135" id="Seg_4315" s="T134">pp-adv:advcase</ta>
            <ta e="T136" id="Seg_4316" s="T135">adv</ta>
            <ta e="T137" id="Seg_4317" s="T136">pers</ta>
            <ta e="T138" id="Seg_4318" s="T137">v-v:ins-v:pn</ta>
            <ta e="T139" id="Seg_4319" s="T138">interrog-n:case3</ta>
            <ta e="T140" id="Seg_4320" s="T139">v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_4321" s="T140">interrog-n:case3</ta>
            <ta e="T142" id="Seg_4322" s="T141">v-v&gt;n-n&gt;adj</ta>
            <ta e="T143" id="Seg_4323" s="T142">v-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_4324" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_4325" s="T144">v-v:tense-mood-v:pn-clit</ta>
            <ta e="T146" id="Seg_4326" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_4327" s="T146">n-n:case3</ta>
            <ta e="T148" id="Seg_4328" s="T147">num</ta>
            <ta e="T149" id="Seg_4329" s="T148">n-n:case3</ta>
            <ta e="T150" id="Seg_4330" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_4331" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_4332" s="T151">n-n:case3</ta>
            <ta e="T153" id="Seg_4333" s="T152">v-v&gt;v-v:pn</ta>
            <ta e="T154" id="Seg_4334" s="T153">n-n:case1-n:poss</ta>
            <ta e="T155" id="Seg_4335" s="T154">ptcl</ta>
            <ta e="T156" id="Seg_4336" s="T155">v-v:pn</ta>
            <ta e="T157" id="Seg_4337" s="T156">adv</ta>
            <ta e="T158" id="Seg_4338" s="T157">v-v&gt;v-v:pn</ta>
            <ta e="T159" id="Seg_4339" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_4340" s="T159">n-n:case1-n:poss</ta>
            <ta e="T161" id="Seg_4341" s="T160">adv</ta>
            <ta e="T162" id="Seg_4342" s="T161">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T163" id="Seg_4343" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_4344" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_4345" s="T164">n-n:case3</ta>
            <ta e="T166" id="Seg_4346" s="T165">v-v:tense-v:pn</ta>
            <ta e="T167" id="Seg_4347" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_4348" s="T167">v-v:inf-poss</ta>
            <ta e="T169" id="Seg_4349" s="T168">n-n:case3</ta>
            <ta e="T170" id="Seg_4350" s="T169">v-v&gt;v-v:pn</ta>
            <ta e="T171" id="Seg_4351" s="T170">n&gt;adj</ta>
            <ta e="T172" id="Seg_4352" s="T171">n-n:case1-n:poss</ta>
            <ta e="T173" id="Seg_4353" s="T172">preverb</ta>
            <ta e="T174" id="Seg_4354" s="T173">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T175" id="Seg_4355" s="T174">adv</ta>
            <ta e="T176" id="Seg_4356" s="T175">dem</ta>
            <ta e="T177" id="Seg_4357" s="T176">n-n:obl.poss-n:case2</ta>
            <ta e="T178" id="Seg_4358" s="T177">v-v:ins-v:pn-clit</ta>
            <ta e="T179" id="Seg_4359" s="T178">v-v:ins-v:pn</ta>
            <ta e="T180" id="Seg_4360" s="T179">v-v:ins-v:pn</ta>
            <ta e="T181" id="Seg_4361" s="T180">num</ta>
            <ta e="T182" id="Seg_4362" s="T181">n-n:case3</ta>
            <ta e="T183" id="Seg_4363" s="T182">n-n:case3</ta>
            <ta e="T184" id="Seg_4364" s="T183">pro-n:case3</ta>
            <ta e="T185" id="Seg_4365" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_4366" s="T185">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T187" id="Seg_4367" s="T186">v-v&gt;v-v:pn</ta>
            <ta e="T188" id="Seg_4368" s="T187">n-n:case1-n:poss</ta>
            <ta e="T189" id="Seg_4369" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_4370" s="T189">v-v:pn</ta>
            <ta e="T191" id="Seg_4371" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_4372" s="T191">n-n:case1-n:poss</ta>
            <ta e="T193" id="Seg_4373" s="T192">adv</ta>
            <ta e="T194" id="Seg_4374" s="T193">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T195" id="Seg_4375" s="T194">n-n:poss-case</ta>
            <ta e="T196" id="Seg_4376" s="T195">n-n:poss-case</ta>
            <ta e="T197" id="Seg_4377" s="T196">n-n:case3</ta>
            <ta e="T198" id="Seg_4378" s="T197">v-v:tense-v:pn</ta>
            <ta e="T199" id="Seg_4379" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_4380" s="T199">n-n:case1-n:poss</ta>
            <ta e="T201" id="Seg_4381" s="T200">n-n:case3</ta>
            <ta e="T202" id="Seg_4382" s="T201">pp</ta>
            <ta e="T203" id="Seg_4383" s="T202">preverb</ta>
            <ta e="T204" id="Seg_4384" s="T203">v-v&gt;v-v:pn</ta>
            <ta e="T205" id="Seg_4385" s="T204">dem</ta>
            <ta e="T206" id="Seg_4386" s="T205">n-n:obl.poss-n:case2</ta>
            <ta e="T207" id="Seg_4387" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_4388" s="T207">v-v:tense-mood-v:pn-clit</ta>
            <ta e="T209" id="Seg_4389" s="T208">v-v:tense-mood-v:pn-clit</ta>
            <ta e="T210" id="Seg_4390" s="T209">interj</ta>
            <ta e="T211" id="Seg_4391" s="T210">num</ta>
            <ta e="T212" id="Seg_4392" s="T211">n-n:case3</ta>
            <ta e="T213" id="Seg_4393" s="T212">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T214" id="Seg_4394" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_4395" s="T214">ptcl</ta>
            <ta e="T216" id="Seg_4396" s="T215">v-v&gt;v-v:pn</ta>
            <ta e="T217" id="Seg_4397" s="T216">n-n:case1-n:poss</ta>
            <ta e="T218" id="Seg_4398" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_4399" s="T218">v-v:pn</ta>
            <ta e="T220" id="Seg_4400" s="T219">adv</ta>
            <ta e="T221" id="Seg_4401" s="T220">adv</ta>
            <ta e="T222" id="Seg_4402" s="T221">v-v:pn</ta>
            <ta e="T223" id="Seg_4403" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_4404" s="T223">n-n:case3</ta>
            <ta e="T225" id="Seg_4405" s="T224">pro-n:case3</ta>
            <ta e="T226" id="Seg_4406" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_4407" s="T226">n-n:(ins)-n&gt;adv</ta>
            <ta e="T228" id="Seg_4408" s="T227">v-v:ins-v:pn</ta>
            <ta e="T229" id="Seg_4409" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_4410" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_4411" s="T230">n-n:case3</ta>
            <ta e="T232" id="Seg_4412" s="T231">adj-adj&gt;adv</ta>
            <ta e="T233" id="Seg_4413" s="T232">v-v:ins-v:pn</ta>
            <ta e="T234" id="Seg_4414" s="T233">n-n:case3</ta>
            <ta e="T235" id="Seg_4415" s="T234">v-v:tense-v:pn</ta>
            <ta e="T236" id="Seg_4416" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_4417" s="T236">n-n:case3</ta>
            <ta e="T238" id="Seg_4418" s="T237">pp</ta>
            <ta e="T239" id="Seg_4419" s="T238">preverb</ta>
            <ta e="T240" id="Seg_4420" s="T239">v-v:ins-v:pn</ta>
            <ta e="T241" id="Seg_4421" s="T240">dem</ta>
            <ta e="T242" id="Seg_4422" s="T241">n-n:case1-n:poss</ta>
            <ta e="T243" id="Seg_4423" s="T242">adv</ta>
            <ta e="T244" id="Seg_4424" s="T243">v</ta>
            <ta e="T245" id="Seg_4425" s="T244">adv</ta>
            <ta e="T246" id="Seg_4426" s="T245">dem</ta>
            <ta e="T247" id="Seg_4427" s="T246">n-n:obl.poss-n:case2</ta>
            <ta e="T248" id="Seg_4428" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_4429" s="T248">v-v:tense-mood-v:pn</ta>
            <ta e="T250" id="Seg_4430" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_4431" s="T250">v-v:tense-mood-v:pn-clit</ta>
            <ta e="T252" id="Seg_4432" s="T251">num</ta>
            <ta e="T253" id="Seg_4433" s="T252">adj</ta>
            <ta e="T254" id="Seg_4434" s="T253">n-n:case3</ta>
            <ta e="T255" id="Seg_4435" s="T254">dem</ta>
            <ta e="T256" id="Seg_4436" s="T255">n-n:case3</ta>
            <ta e="T257" id="Seg_4437" s="T256">v-v&gt;ptcp</ta>
            <ta e="T258" id="Seg_4438" s="T257">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T259" id="Seg_4439" s="T258">v-v&gt;v-v:pn</ta>
            <ta e="T260" id="Seg_4440" s="T259">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T261" id="Seg_4441" s="T260">adv</ta>
            <ta e="T262" id="Seg_4442" s="T261">v-v&gt;v-v:pn</ta>
            <ta e="T263" id="Seg_4443" s="T262">n-n:case1-n:poss</ta>
            <ta e="T264" id="Seg_4444" s="T263">v-v:pn</ta>
            <ta e="T265" id="Seg_4445" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_4446" s="T265">n-n:case1-n:poss</ta>
            <ta e="T267" id="Seg_4447" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_4448" s="T267">quant</ta>
            <ta e="T269" id="Seg_4449" s="T268">n-n:case3</ta>
            <ta e="T270" id="Seg_4450" s="T269">v-v:tense-v:pn</ta>
            <ta e="T271" id="Seg_4451" s="T270">ptcl</ta>
            <ta e="T272" id="Seg_4452" s="T271">n-n:poss-case</ta>
            <ta e="T273" id="Seg_4453" s="T272">n-n:case1-n:poss</ta>
            <ta e="T274" id="Seg_4454" s="T273">pp</ta>
            <ta e="T275" id="Seg_4455" s="T274">n-n:case3</ta>
            <ta e="T276" id="Seg_4456" s="T275">pp</ta>
            <ta e="T277" id="Seg_4457" s="T276">n-n:case1-n:poss</ta>
            <ta e="T278" id="Seg_4458" s="T277">preverb</ta>
            <ta e="T279" id="Seg_4459" s="T278">v-v&gt;v-v:pn</ta>
            <ta e="T280" id="Seg_4460" s="T279">adv</ta>
            <ta e="T281" id="Seg_4461" s="T280">dem</ta>
            <ta e="T282" id="Seg_4462" s="T281">n-n:obl.poss-n:case2</ta>
            <ta e="T283" id="Seg_4463" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_4464" s="T283">v-v:ins-v:pn</ta>
            <ta e="T285" id="Seg_4465" s="T284">v-v:ins-v:pn</ta>
            <ta e="T286" id="Seg_4466" s="T285">adv</ta>
            <ta e="T287" id="Seg_4467" s="T286">num</ta>
            <ta e="T288" id="Seg_4468" s="T287">n-n&gt;adv</ta>
            <ta e="T289" id="Seg_4469" s="T288">n-n:case3</ta>
            <ta e="T290" id="Seg_4470" s="T289">ptcl</ta>
            <ta e="T291" id="Seg_4471" s="T290">n-n:case3</ta>
            <ta e="T292" id="Seg_4472" s="T291">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T293" id="Seg_4473" s="T292">adv</ta>
            <ta e="T294" id="Seg_4474" s="T293">v-v&gt;v-v:pn</ta>
            <ta e="T295" id="Seg_4475" s="T294">n-n:case1-n:poss</ta>
            <ta e="T296" id="Seg_4476" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_4477" s="T296">v-v:pn</ta>
            <ta e="T298" id="Seg_4478" s="T297">ptcl</ta>
            <ta e="T299" id="Seg_4479" s="T298">n-n:case3</ta>
            <ta e="T300" id="Seg_4480" s="T299">v-v:tense-v:pn</ta>
            <ta e="T301" id="Seg_4481" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_4482" s="T301">dem</ta>
            <ta e="T303" id="Seg_4483" s="T302">n-n:case1-n:poss</ta>
            <ta e="T304" id="Seg_4484" s="T303">n-n:case3</ta>
            <ta e="T305" id="Seg_4485" s="T304">pp</ta>
            <ta e="T306" id="Seg_4486" s="T305">preverb</ta>
            <ta e="T307" id="Seg_4487" s="T306">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T308" id="Seg_4488" s="T307">adv</ta>
            <ta e="T309" id="Seg_4489" s="T308">n-n:obl.poss-n:case2</ta>
            <ta e="T310" id="Seg_4490" s="T309">v-v:ins-v:pn</ta>
            <ta e="T311" id="Seg_4491" s="T310">v-v:ins-v:pn-clit</ta>
            <ta e="T312" id="Seg_4492" s="T311">num</ta>
            <ta e="T313" id="Seg_4493" s="T312">n-n&gt;adj</ta>
            <ta e="T314" id="Seg_4494" s="T313">n-n:case3</ta>
            <ta e="T315" id="Seg_4495" s="T314">ptcl</ta>
            <ta e="T316" id="Seg_4496" s="T315">dem</ta>
            <ta e="T317" id="Seg_4497" s="T316">pro-n:case3</ta>
            <ta e="T318" id="Seg_4498" s="T317">pp-n&gt;adj</ta>
            <ta e="T319" id="Seg_4499" s="T318">pp-n&gt;adj</ta>
            <ta e="T320" id="Seg_4500" s="T319">n-n:case1-n:poss</ta>
            <ta e="T321" id="Seg_4501" s="T320">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T322" id="Seg_4502" s="T321">n-n:case3</ta>
            <ta e="T323" id="Seg_4503" s="T322">conj</ta>
            <ta e="T324" id="Seg_4504" s="T323">n-n:case3</ta>
            <ta e="T325" id="Seg_4505" s="T324">ptcl</ta>
            <ta e="T326" id="Seg_4506" s="T325">n-n:case1-n:poss</ta>
            <ta e="T327" id="Seg_4507" s="T326">adv</ta>
            <ta e="T328" id="Seg_4508" s="T327">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T329" id="Seg_4509" s="T328">ptcl</ta>
            <ta e="T330" id="Seg_4510" s="T329">n-n&gt;n-n&gt;adj</ta>
            <ta e="T331" id="Seg_4511" s="T330">n-n:(ins)-v:ins-v:pn</ta>
            <ta e="T332" id="Seg_4512" s="T331">n-n:case3</ta>
            <ta e="T333" id="Seg_4513" s="T332">interrog-n:(ins)-n:case3</ta>
            <ta e="T334" id="Seg_4514" s="T333">pers</ta>
            <ta e="T335" id="Seg_4515" s="T334">v-v:inf</ta>
            <ta e="T336" id="Seg_4516" s="T335">interrog-n:case3</ta>
            <ta e="T337" id="Seg_4517" s="T336">v-v:inf</ta>
            <ta e="T338" id="Seg_4518" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_4519" s="T338">dem</ta>
            <ta e="T340" id="Seg_4520" s="T339">n-n:case3</ta>
            <ta e="T341" id="Seg_4521" s="T340">n-n:case3</ta>
            <ta e="T342" id="Seg_4522" s="T341">v-v:tense-v:pn</ta>
            <ta e="T343" id="Seg_4523" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_4524" s="T343">dem</ta>
            <ta e="T345" id="Seg_4525" s="T344">n-n:case3</ta>
            <ta e="T346" id="Seg_4526" s="T345">pp</ta>
            <ta e="T347" id="Seg_4527" s="T346">preverb</ta>
            <ta e="T348" id="Seg_4528" s="T347">v-v:ins-v:pn</ta>
            <ta e="T349" id="Seg_4529" s="T348">n-n:case1-n:poss</ta>
            <ta e="T350" id="Seg_4530" s="T349">v-v&gt;v-v:pn</ta>
            <ta e="T351" id="Seg_4531" s="T350">n-n:case3</ta>
            <ta e="T352" id="Seg_4532" s="T351">pp</ta>
            <ta e="T353" id="Seg_4533" s="T352">adv</ta>
            <ta e="T354" id="Seg_4534" s="T353">dem</ta>
            <ta e="T355" id="Seg_4535" s="T354">n-n:obl.poss-n:case2</ta>
            <ta e="T356" id="Seg_4536" s="T355">v-v:ins-v:pn</ta>
            <ta e="T357" id="Seg_4537" s="T356">v-v:ins-v:pn</ta>
            <ta e="T358" id="Seg_4538" s="T357">n-n:case3</ta>
            <ta e="T359" id="Seg_4539" s="T358">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T360" id="Seg_4540" s="T359">n-n:case3</ta>
            <ta e="T361" id="Seg_4541" s="T360">pp</ta>
            <ta e="T362" id="Seg_4542" s="T361">n-n:case3</ta>
            <ta e="T363" id="Seg_4543" s="T362">conj</ta>
            <ta e="T364" id="Seg_4544" s="T363">v-v:ins-v:pn</ta>
            <ta e="T365" id="Seg_4545" s="T364">n-n:case3</ta>
            <ta e="T366" id="Seg_4546" s="T365">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T367" id="Seg_4547" s="T366">dem</ta>
            <ta e="T368" id="Seg_4548" s="T367">n-n:case3</ta>
            <ta e="T369" id="Seg_4549" s="T368">pp-n:case3</ta>
            <ta e="T370" id="Seg_4550" s="T369">ptcl</ta>
            <ta e="T371" id="Seg_4551" s="T370">v-v:ins-v:pn</ta>
            <ta e="T372" id="Seg_4552" s="T371">v-v&gt;v-v&gt;adv</ta>
            <ta e="T373" id="Seg_4553" s="T372">pers</ta>
            <ta e="T374" id="Seg_4554" s="T373">interrog-n:case3</ta>
            <ta e="T375" id="Seg_4555" s="T374">v-v:ins-v:pn</ta>
            <ta e="T376" id="Seg_4556" s="T375">n-n:case3</ta>
            <ta e="T377" id="Seg_4557" s="T376">adv-adv&gt;adj</ta>
            <ta e="T378" id="Seg_4558" s="T377">n-n:(ins)-n&gt;adj</ta>
            <ta e="T379" id="Seg_4559" s="T378">n-n:case3</ta>
            <ta e="T380" id="Seg_4560" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_4561" s="T380">pers</ta>
            <ta e="T382" id="Seg_4562" s="T381">n-n:case3</ta>
            <ta e="T383" id="Seg_4563" s="T382">conj</ta>
            <ta e="T384" id="Seg_4564" s="T383">v-v:tense-mood-v:pn</ta>
            <ta e="T385" id="Seg_4565" s="T384">dem</ta>
            <ta e="T386" id="Seg_4566" s="T385">n-n:case3</ta>
            <ta e="T387" id="Seg_4567" s="T386">v-v&gt;v-v:tense-mood-v:pn</ta>
            <ta e="T388" id="Seg_4568" s="T387">n-n:case1-n:poss</ta>
            <ta e="T389" id="Seg_4569" s="T388">dem</ta>
            <ta e="T390" id="Seg_4570" s="T389">v-v&gt;v-v:pn</ta>
            <ta e="T391" id="Seg_4571" s="T390">interj</ta>
            <ta e="T392" id="Seg_4572" s="T391">n-n:poss-case</ta>
            <ta e="T393" id="Seg_4573" s="T392">n-n:case3</ta>
            <ta e="T394" id="Seg_4574" s="T393">v-v:mood-pn</ta>
            <ta e="T395" id="Seg_4575" s="T394">v-v:ins-v&gt;v-v:mood-pn</ta>
            <ta e="T396" id="Seg_4576" s="T395">adv</ta>
            <ta e="T397" id="Seg_4577" s="T396">ptcl</ta>
            <ta e="T398" id="Seg_4578" s="T397">v-v:tense-mood-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_4579" s="T0">n</ta>
            <ta e="T2" id="Seg_4580" s="T1">adj</ta>
            <ta e="T3" id="Seg_4581" s="T2">n</ta>
            <ta e="T4" id="Seg_4582" s="T3">v</ta>
            <ta e="T5" id="Seg_4583" s="T4">conj</ta>
            <ta e="T6" id="Seg_4584" s="T5">n</ta>
            <ta e="T7" id="Seg_4585" s="T6">v</ta>
            <ta e="T8" id="Seg_4586" s="T7">n</ta>
            <ta e="T9" id="Seg_4587" s="T8">adv</ta>
            <ta e="T10" id="Seg_4588" s="T9">adv</ta>
            <ta e="T11" id="Seg_4589" s="T10">v</ta>
            <ta e="T12" id="Seg_4590" s="T11">n</ta>
            <ta e="T13" id="Seg_4591" s="T12">interrog</ta>
            <ta e="T14" id="Seg_4592" s="T13">v</ta>
            <ta e="T15" id="Seg_4593" s="T14">n</ta>
            <ta e="T16" id="Seg_4594" s="T15">n</ta>
            <ta e="T17" id="Seg_4595" s="T16">n</ta>
            <ta e="T18" id="Seg_4596" s="T17">adv</ta>
            <ta e="T19" id="Seg_4597" s="T18">adv</ta>
            <ta e="T20" id="Seg_4598" s="T19">v</ta>
            <ta e="T21" id="Seg_4599" s="T20">n</ta>
            <ta e="T22" id="Seg_4600" s="T21">dem</ta>
            <ta e="T23" id="Seg_4601" s="T22">adv</ta>
            <ta e="T24" id="Seg_4602" s="T23">adv</ta>
            <ta e="T25" id="Seg_4603" s="T24">interj</ta>
            <ta e="T26" id="Seg_4604" s="T25">num</ta>
            <ta e="T27" id="Seg_4605" s="T26">n</ta>
            <ta e="T28" id="Seg_4606" s="T27">pers</ta>
            <ta e="T29" id="Seg_4607" s="T28">clit</ta>
            <ta e="T30" id="Seg_4608" s="T29">interrog</ta>
            <ta e="T31" id="Seg_4609" s="T30">v</ta>
            <ta e="T32" id="Seg_4610" s="T31">conj</ta>
            <ta e="T33" id="Seg_4611" s="T32">n</ta>
            <ta e="T34" id="Seg_4612" s="T33">v</ta>
            <ta e="T35" id="Seg_4613" s="T34">conj</ta>
            <ta e="T36" id="Seg_4614" s="T35">interrog</ta>
            <ta e="T37" id="Seg_4615" s="T36">v</ta>
            <ta e="T38" id="Seg_4616" s="T37">pers</ta>
            <ta e="T39" id="Seg_4617" s="T38">interrog</ta>
            <ta e="T40" id="Seg_4618" s="T39">n</ta>
            <ta e="T41" id="Seg_4619" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_4620" s="T41">interrog</ta>
            <ta e="T43" id="Seg_4621" s="T42">v</ta>
            <ta e="T44" id="Seg_4622" s="T43">n</ta>
            <ta e="T45" id="Seg_4623" s="T44">adv</ta>
            <ta e="T46" id="Seg_4624" s="T45">pro</ta>
            <ta e="T47" id="Seg_4625" s="T46">v</ta>
            <ta e="T48" id="Seg_4626" s="T47">adv</ta>
            <ta e="T49" id="Seg_4627" s="T48">v</ta>
            <ta e="T50" id="Seg_4628" s="T49">interj</ta>
            <ta e="T51" id="Seg_4629" s="T50">adv</ta>
            <ta e="T52" id="Seg_4630" s="T51">n</ta>
            <ta e="T53" id="Seg_4631" s="T52">adj</ta>
            <ta e="T54" id="Seg_4632" s="T53">n</ta>
            <ta e="T55" id="Seg_4633" s="T54">v</ta>
            <ta e="T56" id="Seg_4634" s="T55">n</ta>
            <ta e="T57" id="Seg_4635" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_4636" s="T57">adj</ta>
            <ta e="T59" id="Seg_4637" s="T58">n</ta>
            <ta e="T60" id="Seg_4638" s="T59">v</ta>
            <ta e="T61" id="Seg_4639" s="T60">adv</ta>
            <ta e="T62" id="Seg_4640" s="T61">conj</ta>
            <ta e="T63" id="Seg_4641" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_4642" s="T63">v</ta>
            <ta e="T65" id="Seg_4643" s="T64">adv</ta>
            <ta e="T66" id="Seg_4644" s="T65">adv</ta>
            <ta e="T67" id="Seg_4645" s="T66">v</ta>
            <ta e="T68" id="Seg_4646" s="T67">adv</ta>
            <ta e="T69" id="Seg_4647" s="T68">adv</ta>
            <ta e="T70" id="Seg_4648" s="T69">v</ta>
            <ta e="T71" id="Seg_4649" s="T70">adv</ta>
            <ta e="T72" id="Seg_4650" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_4651" s="T72">v</ta>
            <ta e="T74" id="Seg_4652" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_4653" s="T74">n</ta>
            <ta e="T76" id="Seg_4654" s="T75">n</ta>
            <ta e="T77" id="Seg_4655" s="T76">v</ta>
            <ta e="T78" id="Seg_4656" s="T77">n</ta>
            <ta e="T79" id="Seg_4657" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_4658" s="T79">v</ta>
            <ta e="T81" id="Seg_4659" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_4660" s="T81">adv</ta>
            <ta e="T83" id="Seg_4661" s="T82">pers</ta>
            <ta e="T84" id="Seg_4662" s="T83">v</ta>
            <ta e="T85" id="Seg_4663" s="T84">adv</ta>
            <ta e="T86" id="Seg_4664" s="T85">dem</ta>
            <ta e="T87" id="Seg_4665" s="T86">n</ta>
            <ta e="T88" id="Seg_4666" s="T87">adv</ta>
            <ta e="T89" id="Seg_4667" s="T88">v</ta>
            <ta e="T90" id="Seg_4668" s="T89">adj</ta>
            <ta e="T91" id="Seg_4669" s="T90">n</ta>
            <ta e="T92" id="Seg_4670" s="T91">v</ta>
            <ta e="T93" id="Seg_4671" s="T92">adj</ta>
            <ta e="T94" id="Seg_4672" s="T93">n</ta>
            <ta e="T95" id="Seg_4673" s="T94">v</ta>
            <ta e="T96" id="Seg_4674" s="T95">adv</ta>
            <ta e="T97" id="Seg_4675" s="T96">v</ta>
            <ta e="T98" id="Seg_4676" s="T97">v</ta>
            <ta e="T99" id="Seg_4677" s="T98">v</ta>
            <ta e="T100" id="Seg_4678" s="T99">n</ta>
            <ta e="T101" id="Seg_4679" s="T100">dem</ta>
            <ta e="T102" id="Seg_4680" s="T101">n</ta>
            <ta e="T103" id="Seg_4681" s="T102">adv</ta>
            <ta e="T104" id="Seg_4682" s="T103">preverb</ta>
            <ta e="T105" id="Seg_4683" s="T104">v</ta>
            <ta e="T106" id="Seg_4684" s="T105">adv</ta>
            <ta e="T107" id="Seg_4685" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_4686" s="T107">v</ta>
            <ta e="T109" id="Seg_4687" s="T108">interj</ta>
            <ta e="T110" id="Seg_4688" s="T109">interj</ta>
            <ta e="T111" id="Seg_4689" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_4690" s="T111">n</ta>
            <ta e="T113" id="Seg_4691" s="T112">n</ta>
            <ta e="T114" id="Seg_4692" s="T113">n</ta>
            <ta e="T115" id="Seg_4693" s="T114">dem</ta>
            <ta e="T116" id="Seg_4694" s="T115">n</ta>
            <ta e="T117" id="Seg_4695" s="T116">pers</ta>
            <ta e="T118" id="Seg_4696" s="T117">pers</ta>
            <ta e="T119" id="Seg_4697" s="T118">adv</ta>
            <ta e="T120" id="Seg_4698" s="T119">pers</ta>
            <ta e="T121" id="Seg_4699" s="T120">v</ta>
            <ta e="T122" id="Seg_4700" s="T121">n</ta>
            <ta e="T123" id="Seg_4701" s="T122">pp</ta>
            <ta e="T124" id="Seg_4702" s="T123">adv</ta>
            <ta e="T125" id="Seg_4703" s="T124">pers</ta>
            <ta e="T126" id="Seg_4704" s="T125">v</ta>
            <ta e="T127" id="Seg_4705" s="T126">adv</ta>
            <ta e="T128" id="Seg_4706" s="T127">pers</ta>
            <ta e="T129" id="Seg_4707" s="T128">v</ta>
            <ta e="T130" id="Seg_4708" s="T129">adj</ta>
            <ta e="T131" id="Seg_4709" s="T130">n</ta>
            <ta e="T132" id="Seg_4710" s="T131">v</ta>
            <ta e="T133" id="Seg_4711" s="T132">conj</ta>
            <ta e="T134" id="Seg_4712" s="T133">n</ta>
            <ta e="T135" id="Seg_4713" s="T134">pp</ta>
            <ta e="T136" id="Seg_4714" s="T135">adv</ta>
            <ta e="T137" id="Seg_4715" s="T136">pers</ta>
            <ta e="T138" id="Seg_4716" s="T137">v</ta>
            <ta e="T139" id="Seg_4717" s="T138">interrog</ta>
            <ta e="T140" id="Seg_4718" s="T139">v</ta>
            <ta e="T141" id="Seg_4719" s="T140">interrog</ta>
            <ta e="T142" id="Seg_4720" s="T141">adj</ta>
            <ta e="T143" id="Seg_4721" s="T142">v</ta>
            <ta e="T144" id="Seg_4722" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_4723" s="T144">v</ta>
            <ta e="T146" id="Seg_4724" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_4725" s="T146">n</ta>
            <ta e="T148" id="Seg_4726" s="T147">num</ta>
            <ta e="T149" id="Seg_4727" s="T148">n</ta>
            <ta e="T150" id="Seg_4728" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_4729" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_4730" s="T151">n</ta>
            <ta e="T153" id="Seg_4731" s="T152">v</ta>
            <ta e="T154" id="Seg_4732" s="T153">n</ta>
            <ta e="T155" id="Seg_4733" s="T154">ptcl</ta>
            <ta e="T156" id="Seg_4734" s="T155">v</ta>
            <ta e="T157" id="Seg_4735" s="T156">adv</ta>
            <ta e="T158" id="Seg_4736" s="T157">v</ta>
            <ta e="T159" id="Seg_4737" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_4738" s="T159">n</ta>
            <ta e="T161" id="Seg_4739" s="T160">adv</ta>
            <ta e="T162" id="Seg_4740" s="T161">v</ta>
            <ta e="T163" id="Seg_4741" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_4742" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_4743" s="T164">n</ta>
            <ta e="T166" id="Seg_4744" s="T165">v</ta>
            <ta e="T167" id="Seg_4745" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_4746" s="T167">v</ta>
            <ta e="T169" id="Seg_4747" s="T168">n</ta>
            <ta e="T170" id="Seg_4748" s="T169">v</ta>
            <ta e="T171" id="Seg_4749" s="T170">adj</ta>
            <ta e="T172" id="Seg_4750" s="T171">n</ta>
            <ta e="T173" id="Seg_4751" s="T172">preverb</ta>
            <ta e="T174" id="Seg_4752" s="T173">v</ta>
            <ta e="T175" id="Seg_4753" s="T174">adv</ta>
            <ta e="T176" id="Seg_4754" s="T175">dem</ta>
            <ta e="T177" id="Seg_4755" s="T176">n</ta>
            <ta e="T178" id="Seg_4756" s="T177">v</ta>
            <ta e="T179" id="Seg_4757" s="T178">v</ta>
            <ta e="T180" id="Seg_4758" s="T179">v</ta>
            <ta e="T181" id="Seg_4759" s="T180">num</ta>
            <ta e="T182" id="Seg_4760" s="T181">n</ta>
            <ta e="T183" id="Seg_4761" s="T182">n</ta>
            <ta e="T184" id="Seg_4762" s="T183">pro</ta>
            <ta e="T185" id="Seg_4763" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_4764" s="T185">n</ta>
            <ta e="T187" id="Seg_4765" s="T186">v</ta>
            <ta e="T188" id="Seg_4766" s="T187">n</ta>
            <ta e="T189" id="Seg_4767" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_4768" s="T189">v</ta>
            <ta e="T191" id="Seg_4769" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_4770" s="T191">n</ta>
            <ta e="T193" id="Seg_4771" s="T192">adv</ta>
            <ta e="T194" id="Seg_4772" s="T193">v</ta>
            <ta e="T195" id="Seg_4773" s="T194">n</ta>
            <ta e="T196" id="Seg_4774" s="T195">n</ta>
            <ta e="T197" id="Seg_4775" s="T196">n</ta>
            <ta e="T198" id="Seg_4776" s="T197">v</ta>
            <ta e="T199" id="Seg_4777" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_4778" s="T199">n</ta>
            <ta e="T201" id="Seg_4779" s="T200">n</ta>
            <ta e="T202" id="Seg_4780" s="T201">pp</ta>
            <ta e="T203" id="Seg_4781" s="T202">preverb</ta>
            <ta e="T204" id="Seg_4782" s="T203">v</ta>
            <ta e="T205" id="Seg_4783" s="T204">dem</ta>
            <ta e="T206" id="Seg_4784" s="T205">n</ta>
            <ta e="T207" id="Seg_4785" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_4786" s="T207">v</ta>
            <ta e="T209" id="Seg_4787" s="T208">v</ta>
            <ta e="T210" id="Seg_4788" s="T209">interj</ta>
            <ta e="T211" id="Seg_4789" s="T210">num</ta>
            <ta e="T212" id="Seg_4790" s="T211">n</ta>
            <ta e="T213" id="Seg_4791" s="T212">n</ta>
            <ta e="T214" id="Seg_4792" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_4793" s="T214">ptcl</ta>
            <ta e="T216" id="Seg_4794" s="T215">v</ta>
            <ta e="T217" id="Seg_4795" s="T216">n</ta>
            <ta e="T218" id="Seg_4796" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_4797" s="T218">v</ta>
            <ta e="T220" id="Seg_4798" s="T219">adv</ta>
            <ta e="T221" id="Seg_4799" s="T220">adv</ta>
            <ta e="T222" id="Seg_4800" s="T221">v</ta>
            <ta e="T223" id="Seg_4801" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_4802" s="T223">n</ta>
            <ta e="T225" id="Seg_4803" s="T224">pro</ta>
            <ta e="T226" id="Seg_4804" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_4805" s="T226">adv</ta>
            <ta e="T228" id="Seg_4806" s="T227">v</ta>
            <ta e="T229" id="Seg_4807" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_4808" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_4809" s="T230">n</ta>
            <ta e="T232" id="Seg_4810" s="T231">adv</ta>
            <ta e="T233" id="Seg_4811" s="T232">n</ta>
            <ta e="T234" id="Seg_4812" s="T233">n</ta>
            <ta e="T235" id="Seg_4813" s="T234">v</ta>
            <ta e="T236" id="Seg_4814" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_4815" s="T236">n</ta>
            <ta e="T238" id="Seg_4816" s="T237">pp</ta>
            <ta e="T239" id="Seg_4817" s="T238">preverb</ta>
            <ta e="T240" id="Seg_4818" s="T239">v</ta>
            <ta e="T241" id="Seg_4819" s="T240">dem</ta>
            <ta e="T242" id="Seg_4820" s="T241">n</ta>
            <ta e="T243" id="Seg_4821" s="T242">adv</ta>
            <ta e="T244" id="Seg_4822" s="T243">v</ta>
            <ta e="T245" id="Seg_4823" s="T244">adv</ta>
            <ta e="T246" id="Seg_4824" s="T245">dem</ta>
            <ta e="T247" id="Seg_4825" s="T246">n</ta>
            <ta e="T248" id="Seg_4826" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_4827" s="T248">v</ta>
            <ta e="T250" id="Seg_4828" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_4829" s="T250">v</ta>
            <ta e="T252" id="Seg_4830" s="T251">num</ta>
            <ta e="T253" id="Seg_4831" s="T252">adj</ta>
            <ta e="T254" id="Seg_4832" s="T253">n</ta>
            <ta e="T255" id="Seg_4833" s="T254">dem</ta>
            <ta e="T256" id="Seg_4834" s="T255">n</ta>
            <ta e="T257" id="Seg_4835" s="T256">ptcp</ta>
            <ta e="T258" id="Seg_4836" s="T257">n</ta>
            <ta e="T259" id="Seg_4837" s="T258">v</ta>
            <ta e="T260" id="Seg_4838" s="T259">n</ta>
            <ta e="T261" id="Seg_4839" s="T260">adv</ta>
            <ta e="T262" id="Seg_4840" s="T261">v</ta>
            <ta e="T263" id="Seg_4841" s="T262">n</ta>
            <ta e="T264" id="Seg_4842" s="T263">v</ta>
            <ta e="T265" id="Seg_4843" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_4844" s="T265">n</ta>
            <ta e="T267" id="Seg_4845" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_4846" s="T267">quant</ta>
            <ta e="T269" id="Seg_4847" s="T268">n</ta>
            <ta e="T270" id="Seg_4848" s="T269">v</ta>
            <ta e="T271" id="Seg_4849" s="T270">ptcl</ta>
            <ta e="T272" id="Seg_4850" s="T271">n</ta>
            <ta e="T273" id="Seg_4851" s="T272">n</ta>
            <ta e="T274" id="Seg_4852" s="T273">pp</ta>
            <ta e="T275" id="Seg_4853" s="T274">n</ta>
            <ta e="T276" id="Seg_4854" s="T275">pp</ta>
            <ta e="T277" id="Seg_4855" s="T276">n</ta>
            <ta e="T278" id="Seg_4856" s="T277">preverb</ta>
            <ta e="T279" id="Seg_4857" s="T278">v</ta>
            <ta e="T280" id="Seg_4858" s="T279">adv</ta>
            <ta e="T281" id="Seg_4859" s="T280">dem</ta>
            <ta e="T282" id="Seg_4860" s="T281">n</ta>
            <ta e="T283" id="Seg_4861" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_4862" s="T283">v</ta>
            <ta e="T285" id="Seg_4863" s="T284">v</ta>
            <ta e="T286" id="Seg_4864" s="T285">adv</ta>
            <ta e="T287" id="Seg_4865" s="T286">num</ta>
            <ta e="T288" id="Seg_4866" s="T287">adv</ta>
            <ta e="T289" id="Seg_4867" s="T288">n</ta>
            <ta e="T290" id="Seg_4868" s="T289">ptcl</ta>
            <ta e="T291" id="Seg_4869" s="T290">n</ta>
            <ta e="T292" id="Seg_4870" s="T291">n</ta>
            <ta e="T293" id="Seg_4871" s="T292">adv</ta>
            <ta e="T294" id="Seg_4872" s="T293">v</ta>
            <ta e="T295" id="Seg_4873" s="T294">n</ta>
            <ta e="T296" id="Seg_4874" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_4875" s="T296">v</ta>
            <ta e="T298" id="Seg_4876" s="T297">ptcl</ta>
            <ta e="T299" id="Seg_4877" s="T298">n</ta>
            <ta e="T300" id="Seg_4878" s="T299">v</ta>
            <ta e="T301" id="Seg_4879" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_4880" s="T301">pro</ta>
            <ta e="T303" id="Seg_4881" s="T302">n</ta>
            <ta e="T304" id="Seg_4882" s="T303">n</ta>
            <ta e="T305" id="Seg_4883" s="T304">pp</ta>
            <ta e="T306" id="Seg_4884" s="T305">preverb</ta>
            <ta e="T307" id="Seg_4885" s="T306">v</ta>
            <ta e="T308" id="Seg_4886" s="T307">adv</ta>
            <ta e="T309" id="Seg_4887" s="T308">n</ta>
            <ta e="T310" id="Seg_4888" s="T309">v</ta>
            <ta e="T311" id="Seg_4889" s="T310">v</ta>
            <ta e="T312" id="Seg_4890" s="T311">num</ta>
            <ta e="T313" id="Seg_4891" s="T312">adj</ta>
            <ta e="T314" id="Seg_4892" s="T313">n</ta>
            <ta e="T315" id="Seg_4893" s="T314">ptcl</ta>
            <ta e="T316" id="Seg_4894" s="T315">dem</ta>
            <ta e="T317" id="Seg_4895" s="T316">pro</ta>
            <ta e="T318" id="Seg_4896" s="T317">adj</ta>
            <ta e="T319" id="Seg_4897" s="T318">adj</ta>
            <ta e="T320" id="Seg_4898" s="T319">n</ta>
            <ta e="T321" id="Seg_4899" s="T320">v</ta>
            <ta e="T322" id="Seg_4900" s="T321">n</ta>
            <ta e="T323" id="Seg_4901" s="T322">conj</ta>
            <ta e="T324" id="Seg_4902" s="T323">n</ta>
            <ta e="T325" id="Seg_4903" s="T324">ptcl</ta>
            <ta e="T326" id="Seg_4904" s="T325">n</ta>
            <ta e="T327" id="Seg_4905" s="T326">adv</ta>
            <ta e="T328" id="Seg_4906" s="T327">v</ta>
            <ta e="T329" id="Seg_4907" s="T328">ptcl</ta>
            <ta e="T330" id="Seg_4908" s="T329">adj</ta>
            <ta e="T331" id="Seg_4909" s="T330">v</ta>
            <ta e="T332" id="Seg_4910" s="T331">n</ta>
            <ta e="T333" id="Seg_4911" s="T332">interrog</ta>
            <ta e="T334" id="Seg_4912" s="T333">pers</ta>
            <ta e="T335" id="Seg_4913" s="T334">v</ta>
            <ta e="T336" id="Seg_4914" s="T335">interrog</ta>
            <ta e="T337" id="Seg_4915" s="T336">v</ta>
            <ta e="T338" id="Seg_4916" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_4917" s="T338">dem</ta>
            <ta e="T340" id="Seg_4918" s="T339">n</ta>
            <ta e="T341" id="Seg_4919" s="T340">n</ta>
            <ta e="T342" id="Seg_4920" s="T341">v</ta>
            <ta e="T343" id="Seg_4921" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_4922" s="T343">dem</ta>
            <ta e="T345" id="Seg_4923" s="T344">n</ta>
            <ta e="T346" id="Seg_4924" s="T345">pp</ta>
            <ta e="T347" id="Seg_4925" s="T346">preverb</ta>
            <ta e="T348" id="Seg_4926" s="T347">v</ta>
            <ta e="T349" id="Seg_4927" s="T348">n</ta>
            <ta e="T350" id="Seg_4928" s="T349">v</ta>
            <ta e="T351" id="Seg_4929" s="T350">n</ta>
            <ta e="T352" id="Seg_4930" s="T351">pp</ta>
            <ta e="T353" id="Seg_4931" s="T352">adv</ta>
            <ta e="T354" id="Seg_4932" s="T353">dem</ta>
            <ta e="T355" id="Seg_4933" s="T354">n</ta>
            <ta e="T356" id="Seg_4934" s="T355">v</ta>
            <ta e="T357" id="Seg_4935" s="T356">v</ta>
            <ta e="T358" id="Seg_4936" s="T357">n</ta>
            <ta e="T359" id="Seg_4937" s="T358">v</ta>
            <ta e="T360" id="Seg_4938" s="T359">v</ta>
            <ta e="T361" id="Seg_4939" s="T360">pp</ta>
            <ta e="T362" id="Seg_4940" s="T361">n</ta>
            <ta e="T363" id="Seg_4941" s="T362">conj</ta>
            <ta e="T364" id="Seg_4942" s="T363">v</ta>
            <ta e="T365" id="Seg_4943" s="T364">n</ta>
            <ta e="T366" id="Seg_4944" s="T365">v</ta>
            <ta e="T367" id="Seg_4945" s="T366">dem</ta>
            <ta e="T368" id="Seg_4946" s="T367">n</ta>
            <ta e="T369" id="Seg_4947" s="T368">pp</ta>
            <ta e="T370" id="Seg_4948" s="T369">ptcl</ta>
            <ta e="T371" id="Seg_4949" s="T370">v</ta>
            <ta e="T372" id="Seg_4950" s="T371">adv</ta>
            <ta e="T373" id="Seg_4951" s="T372">pers</ta>
            <ta e="T374" id="Seg_4952" s="T373">interrog</ta>
            <ta e="T375" id="Seg_4953" s="T374">v</ta>
            <ta e="T376" id="Seg_4954" s="T375">n</ta>
            <ta e="T377" id="Seg_4955" s="T376">adj</ta>
            <ta e="T378" id="Seg_4956" s="T377">adj</ta>
            <ta e="T379" id="Seg_4957" s="T378">n</ta>
            <ta e="T380" id="Seg_4958" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_4959" s="T380">pers</ta>
            <ta e="T382" id="Seg_4960" s="T381">n</ta>
            <ta e="T383" id="Seg_4961" s="T382">conj</ta>
            <ta e="T384" id="Seg_4962" s="T383">v</ta>
            <ta e="T385" id="Seg_4963" s="T384">dem</ta>
            <ta e="T386" id="Seg_4964" s="T385">n</ta>
            <ta e="T387" id="Seg_4965" s="T386">v</ta>
            <ta e="T388" id="Seg_4966" s="T387">n</ta>
            <ta e="T389" id="Seg_4967" s="T388">dem</ta>
            <ta e="T390" id="Seg_4968" s="T389">v</ta>
            <ta e="T391" id="Seg_4969" s="T390">interj</ta>
            <ta e="T392" id="Seg_4970" s="T391">n</ta>
            <ta e="T393" id="Seg_4971" s="T392">n</ta>
            <ta e="T394" id="Seg_4972" s="T393">v</ta>
            <ta e="T395" id="Seg_4973" s="T394">v</ta>
            <ta e="T396" id="Seg_4974" s="T395">adv</ta>
            <ta e="T397" id="Seg_4975" s="T396">ptcl</ta>
            <ta e="T398" id="Seg_4976" s="T397">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T8" id="Seg_4977" s="T7">np.h:A</ta>
            <ta e="T15" id="Seg_4978" s="T14">np.h:A</ta>
            <ta e="T16" id="Seg_4979" s="T15">np:Poss</ta>
            <ta e="T17" id="Seg_4980" s="T16">np:Path</ta>
            <ta e="T18" id="Seg_4981" s="T17">adv:G</ta>
            <ta e="T21" id="Seg_4982" s="T20">np:G</ta>
            <ta e="T22" id="Seg_4983" s="T21">pro.h:A</ta>
            <ta e="T23" id="Seg_4984" s="T22">adv:G</ta>
            <ta e="T27" id="Seg_4985" s="T26">np:Time</ta>
            <ta e="T30" id="Seg_4986" s="T29">pro:A</ta>
            <ta e="T33" id="Seg_4987" s="T32">np.h:A</ta>
            <ta e="T36" id="Seg_4988" s="T35">pro:A</ta>
            <ta e="T38" id="Seg_4989" s="T37">pro:Th</ta>
            <ta e="T40" id="Seg_4990" s="T39">np.h:A</ta>
            <ta e="T42" id="Seg_4991" s="T41">pro:A</ta>
            <ta e="T44" id="Seg_4992" s="T43">np.h:E</ta>
            <ta e="T46" id="Seg_4993" s="T45">pro:Th</ta>
            <ta e="T48" id="Seg_4994" s="T47">adv:G</ta>
            <ta e="T49" id="Seg_4995" s="T48">0.3.h:A</ta>
            <ta e="T51" id="Seg_4996" s="T50">adv:L</ta>
            <ta e="T52" id="Seg_4997" s="T51">np:L</ta>
            <ta e="T54" id="Seg_4998" s="T53">np:Th</ta>
            <ta e="T56" id="Seg_4999" s="T55">np:L</ta>
            <ta e="T59" id="Seg_5000" s="T58">np:Th</ta>
            <ta e="T61" id="Seg_5001" s="T60">adv:G</ta>
            <ta e="T64" id="Seg_5002" s="T63">0.3:A</ta>
            <ta e="T66" id="Seg_5003" s="T65">adv:G</ta>
            <ta e="T67" id="Seg_5004" s="T66">0.3:A</ta>
            <ta e="T69" id="Seg_5005" s="T68">adv:G</ta>
            <ta e="T70" id="Seg_5006" s="T69">0.3:A</ta>
            <ta e="T71" id="Seg_5007" s="T70">adv:G</ta>
            <ta e="T73" id="Seg_5008" s="T72">0.3:A</ta>
            <ta e="T75" id="Seg_5009" s="T74">np.h:A</ta>
            <ta e="T76" id="Seg_5010" s="T75">adv:G</ta>
            <ta e="T78" id="Seg_5011" s="T77">adv:G</ta>
            <ta e="T80" id="Seg_5012" s="T79">0.3.h:A</ta>
            <ta e="T82" id="Seg_5013" s="T81">adv:G</ta>
            <ta e="T83" id="Seg_5014" s="T82">pro.h:Th</ta>
            <ta e="T84" id="Seg_5015" s="T83">0.2.h:A</ta>
            <ta e="T87" id="Seg_5016" s="T86">np.h:A</ta>
            <ta e="T88" id="Seg_5017" s="T87">adv:G</ta>
            <ta e="T91" id="Seg_5018" s="T90">np:Th</ta>
            <ta e="T92" id="Seg_5019" s="T91">0.3.h:A</ta>
            <ta e="T94" id="Seg_5020" s="T93">np:P</ta>
            <ta e="T95" id="Seg_5021" s="T94">0.3.h:A</ta>
            <ta e="T96" id="Seg_5022" s="T95">adv:G</ta>
            <ta e="T99" id="Seg_5023" s="T98">0.3.h:A 0.3:Th</ta>
            <ta e="T100" id="Seg_5024" s="T99">np.h:E</ta>
            <ta e="T102" id="Seg_5025" s="T101">np:Com</ta>
            <ta e="T103" id="Seg_5026" s="T102">adv:G</ta>
            <ta e="T105" id="Seg_5027" s="T104">0.3.h:A 0.3.h:Th</ta>
            <ta e="T108" id="Seg_5028" s="T107">0.3.h:A 0.3.h:Th</ta>
            <ta e="T112" id="Seg_5029" s="T111">0.3.h:Th</ta>
            <ta e="T113" id="Seg_5030" s="T112">0.3.h:Th</ta>
            <ta e="T115" id="Seg_5031" s="T114">pro.h:Th</ta>
            <ta e="T117" id="Seg_5032" s="T116">pro.h:A</ta>
            <ta e="T118" id="Seg_5033" s="T117">pro.h:Th</ta>
            <ta e="T119" id="Seg_5034" s="T118">adv:G</ta>
            <ta e="T120" id="Seg_5035" s="T119">pro.h:Th</ta>
            <ta e="T122" id="Seg_5036" s="T121">pp:S</ta>
            <ta e="T124" id="Seg_5037" s="T123">adv:G</ta>
            <ta e="T125" id="Seg_5038" s="T124">pro.h:Th</ta>
            <ta e="T126" id="Seg_5039" s="T125">0.2.h:A</ta>
            <ta e="T127" id="Seg_5040" s="T126">adv:G</ta>
            <ta e="T128" id="Seg_5041" s="T127">pro.h:Th</ta>
            <ta e="T129" id="Seg_5042" s="T128">0.2.h:A</ta>
            <ta e="T131" id="Seg_5043" s="T130">np:Th</ta>
            <ta e="T132" id="Seg_5044" s="T131">0.3.h:A 0.3.h:R</ta>
            <ta e="T134" id="Seg_5045" s="T133">pp:S</ta>
            <ta e="T136" id="Seg_5046" s="T135">adv:G</ta>
            <ta e="T137" id="Seg_5047" s="T136">pro.h:Th</ta>
            <ta e="T138" id="Seg_5048" s="T137">0.2.h:A</ta>
            <ta e="T139" id="Seg_5049" s="T138">pro:Th</ta>
            <ta e="T140" id="Seg_5050" s="T139">0.2.h:A</ta>
            <ta e="T141" id="Seg_5051" s="T140">pro:Th</ta>
            <ta e="T142" id="Seg_5052" s="T141">np:Th</ta>
            <ta e="T143" id="Seg_5053" s="T142">0.2.h:A</ta>
            <ta e="T145" id="Seg_5054" s="T144">0.3.h:A</ta>
            <ta e="T149" id="Seg_5055" s="T148">np.h:A</ta>
            <ta e="T152" id="Seg_5056" s="T151">np:Th</ta>
            <ta e="T154" id="Seg_5057" s="T153">np:Th</ta>
            <ta e="T157" id="Seg_5058" s="T156">adv:Time</ta>
            <ta e="T158" id="Seg_5059" s="T157">0.3.h:A</ta>
            <ta e="T160" id="Seg_5060" s="T159">np:Th</ta>
            <ta e="T165" id="Seg_5061" s="T164">np:Th</ta>
            <ta e="T166" id="Seg_5062" s="T165">0.1.h:A</ta>
            <ta e="T169" id="Seg_5063" s="T168">np:Th</ta>
            <ta e="T170" id="Seg_5064" s="T169">0.3.h:A</ta>
            <ta e="T172" id="Seg_5065" s="T171">np:Th</ta>
            <ta e="T174" id="Seg_5066" s="T173">0.3.h:A</ta>
            <ta e="T175" id="Seg_5067" s="T174">adv:Time</ta>
            <ta e="T177" id="Seg_5068" s="T176">np:Com</ta>
            <ta e="T178" id="Seg_5069" s="T177">0.3.h:A</ta>
            <ta e="T179" id="Seg_5070" s="T178">0.3.h:A</ta>
            <ta e="T180" id="Seg_5071" s="T179">0.3.h:A</ta>
            <ta e="T182" id="Seg_5072" s="T181">np:Time</ta>
            <ta e="T183" id="Seg_5073" s="T182">np.h:A</ta>
            <ta e="T186" id="Seg_5074" s="T185">np:Th</ta>
            <ta e="T188" id="Seg_5075" s="T187">np:Th</ta>
            <ta e="T192" id="Seg_5076" s="T191">np:Th</ta>
            <ta e="T197" id="Seg_5077" s="T196">np:Th</ta>
            <ta e="T198" id="Seg_5078" s="T197">0.1.h:A</ta>
            <ta e="T200" id="Seg_5079" s="T199">np:Th</ta>
            <ta e="T201" id="Seg_5080" s="T200">pp:Th</ta>
            <ta e="T204" id="Seg_5081" s="T203">0.3.h:A</ta>
            <ta e="T206" id="Seg_5082" s="T205">np:Com</ta>
            <ta e="T208" id="Seg_5083" s="T207">0.3.h:A</ta>
            <ta e="T209" id="Seg_5084" s="T208">0.3.h:A</ta>
            <ta e="T212" id="Seg_5085" s="T211">np:Time</ta>
            <ta e="T213" id="Seg_5086" s="T212">np:Th</ta>
            <ta e="T216" id="Seg_5087" s="T215">0.3.h:A</ta>
            <ta e="T217" id="Seg_5088" s="T216">np:Th</ta>
            <ta e="T220" id="Seg_5089" s="T219">adv:Time</ta>
            <ta e="T222" id="Seg_5090" s="T221">0.3.h:A</ta>
            <ta e="T224" id="Seg_5091" s="T223">np:Th</ta>
            <ta e="T231" id="Seg_5092" s="T230">np:Th</ta>
            <ta e="T234" id="Seg_5093" s="T233">np:Th</ta>
            <ta e="T235" id="Seg_5094" s="T234">0.1.h:A</ta>
            <ta e="T237" id="Seg_5095" s="T236">pp:Th</ta>
            <ta e="T240" id="Seg_5096" s="T239">0.3.h:A</ta>
            <ta e="T242" id="Seg_5097" s="T241">np:Th</ta>
            <ta e="T245" id="Seg_5098" s="T244">adv:Time</ta>
            <ta e="T247" id="Seg_5099" s="T246">np:Com</ta>
            <ta e="T249" id="Seg_5100" s="T248">0.3.h:A</ta>
            <ta e="T251" id="Seg_5101" s="T250">0.3.h:A</ta>
            <ta e="T254" id="Seg_5102" s="T253">np:Time</ta>
            <ta e="T256" id="Seg_5103" s="T255">np.h:A</ta>
            <ta e="T258" id="Seg_5104" s="T257">np:Th</ta>
            <ta e="T260" id="Seg_5105" s="T259">np:Th</ta>
            <ta e="T262" id="Seg_5106" s="T261">0.3.h:A</ta>
            <ta e="T263" id="Seg_5107" s="T262">np:Th</ta>
            <ta e="T266" id="Seg_5108" s="T265">np:Th</ta>
            <ta e="T269" id="Seg_5109" s="T268">np:Th</ta>
            <ta e="T270" id="Seg_5110" s="T269">0.3.h:A</ta>
            <ta e="T275" id="Seg_5111" s="T274">pp:Th</ta>
            <ta e="T277" id="Seg_5112" s="T276">np:Th</ta>
            <ta e="T279" id="Seg_5113" s="T278">0.3.h:A</ta>
            <ta e="T280" id="Seg_5114" s="T279">adv:Time</ta>
            <ta e="T282" id="Seg_5115" s="T281">np:Com</ta>
            <ta e="T284" id="Seg_5116" s="T283">0.3.h:A</ta>
            <ta e="T285" id="Seg_5117" s="T284">0.3.h:A</ta>
            <ta e="T286" id="Seg_5118" s="T285">adv:G</ta>
            <ta e="T288" id="Seg_5119" s="T287">adv:Time</ta>
            <ta e="T291" id="Seg_5120" s="T290">np.h:A</ta>
            <ta e="T292" id="Seg_5121" s="T291">np:Th</ta>
            <ta e="T295" id="Seg_5122" s="T294">np:Th</ta>
            <ta e="T299" id="Seg_5123" s="T298">np:Th</ta>
            <ta e="T300" id="Seg_5124" s="T299">0.1.h:A</ta>
            <ta e="T303" id="Seg_5125" s="T302">np:Th</ta>
            <ta e="T304" id="Seg_5126" s="T303">pp:Th</ta>
            <ta e="T307" id="Seg_5127" s="T306">0.3.h:A</ta>
            <ta e="T308" id="Seg_5128" s="T307">adv:Time</ta>
            <ta e="T309" id="Seg_5129" s="T308">np:Com</ta>
            <ta e="T310" id="Seg_5130" s="T309">0.3.h:A</ta>
            <ta e="T311" id="Seg_5131" s="T310">0.3.h:A</ta>
            <ta e="T314" id="Seg_5132" s="T313">np.h:A</ta>
            <ta e="T320" id="Seg_5133" s="T319">np:P</ta>
            <ta e="T326" id="Seg_5134" s="T325">np:Th</ta>
            <ta e="T331" id="Seg_5135" s="T330">0.1.h:Th</ta>
            <ta e="T340" id="Seg_5136" s="T339">np.h:Poss</ta>
            <ta e="T341" id="Seg_5137" s="T340">np:Th</ta>
            <ta e="T342" id="Seg_5138" s="T341">0.1.h:A</ta>
            <ta e="T345" id="Seg_5139" s="T344">pp:Th</ta>
            <ta e="T348" id="Seg_5140" s="T347">0.3.h:A</ta>
            <ta e="T349" id="Seg_5141" s="T348">np:Th</ta>
            <ta e="T350" id="Seg_5142" s="T349">0.3.h:A</ta>
            <ta e="T351" id="Seg_5143" s="T350">pp:Th</ta>
            <ta e="T355" id="Seg_5144" s="T354">np:Com</ta>
            <ta e="T356" id="Seg_5145" s="T355">0.3.h:A</ta>
            <ta e="T357" id="Seg_5146" s="T356">0.3.h:A</ta>
            <ta e="T358" id="Seg_5147" s="T357">np:Th</ta>
            <ta e="T360" id="Seg_5148" s="T359">pp:Path</ta>
            <ta e="T362" id="Seg_5149" s="T361">np:G</ta>
            <ta e="T364" id="Seg_5150" s="T363">0.3.h:A</ta>
            <ta e="T365" id="Seg_5151" s="T364">np:Th</ta>
            <ta e="T368" id="Seg_5152" s="T367">pp:L</ta>
            <ta e="T371" id="Seg_5153" s="T370">0.3.h:A 0.3:Th</ta>
            <ta e="T373" id="Seg_5154" s="T372">pro.h:A</ta>
            <ta e="T374" id="Seg_5155" s="T373">pro:Th</ta>
            <ta e="T381" id="Seg_5156" s="T380">pro.h:A</ta>
            <ta e="T382" id="Seg_5157" s="T381">np:Th</ta>
            <ta e="T386" id="Seg_5158" s="T385">np:Th</ta>
            <ta e="T387" id="Seg_5159" s="T386">0.1.h:A</ta>
            <ta e="T388" id="Seg_5160" s="T387">np:Th</ta>
            <ta e="T390" id="Seg_5161" s="T389">0.1.h:A</ta>
            <ta e="T392" id="Seg_5162" s="T391">np:G</ta>
            <ta e="T393" id="Seg_5163" s="T392">np:G</ta>
            <ta e="T394" id="Seg_5164" s="T393">0.2.h:A</ta>
            <ta e="T395" id="Seg_5165" s="T394">0.2.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T7" id="Seg_5166" s="T4">s:temp</ta>
            <ta e="T8" id="Seg_5167" s="T7">np.h:S</ta>
            <ta e="T9" id="Seg_5168" s="T8">s:adv</ta>
            <ta e="T10" id="Seg_5169" s="T9">s:adv</ta>
            <ta e="T11" id="Seg_5170" s="T10">v:pred</ta>
            <ta e="T14" id="Seg_5171" s="T11">s:temp</ta>
            <ta e="T15" id="Seg_5172" s="T14">np.h:S</ta>
            <ta e="T19" id="Seg_5173" s="T18">s:adv</ta>
            <ta e="T20" id="Seg_5174" s="T19">v:pred</ta>
            <ta e="T22" id="Seg_5175" s="T21">pro.h:S</ta>
            <ta e="T24" id="Seg_5176" s="T23">s:adv</ta>
            <ta e="T25" id="Seg_5177" s="T24">ptcl:pred</ta>
            <ta e="T28" id="Seg_5178" s="T27">pro.h:S</ta>
            <ta e="T30" id="Seg_5179" s="T29">pro.h:S</ta>
            <ta e="T31" id="Seg_5180" s="T30">v:pred</ta>
            <ta e="T33" id="Seg_5181" s="T32">np.h:S</ta>
            <ta e="T34" id="Seg_5182" s="T33">v:pred</ta>
            <ta e="T36" id="Seg_5183" s="T35">pro:S</ta>
            <ta e="T37" id="Seg_5184" s="T36">v:pred</ta>
            <ta e="T38" id="Seg_5185" s="T37">pro:S</ta>
            <ta e="T39" id="Seg_5186" s="T38">pro:pred</ta>
            <ta e="T40" id="Seg_5187" s="T39">np.h:S</ta>
            <ta e="T42" id="Seg_5188" s="T41">pro:S</ta>
            <ta e="T43" id="Seg_5189" s="T42">v:pred</ta>
            <ta e="T44" id="Seg_5190" s="T43">np.h:S</ta>
            <ta e="T46" id="Seg_5191" s="T45">pro:O</ta>
            <ta e="T47" id="Seg_5192" s="T46">v:pred</ta>
            <ta e="T49" id="Seg_5193" s="T48">0.3.h:S v:pred</ta>
            <ta e="T50" id="Seg_5194" s="T49">ptcl:pred</ta>
            <ta e="T54" id="Seg_5195" s="T53">np:S</ta>
            <ta e="T55" id="Seg_5196" s="T54">v:pred</ta>
            <ta e="T59" id="Seg_5197" s="T58">np:S</ta>
            <ta e="T60" id="Seg_5198" s="T59">v:pred</ta>
            <ta e="T64" id="Seg_5199" s="T63">0.3:S v:pred</ta>
            <ta e="T67" id="Seg_5200" s="T66">0.3:S v:pred</ta>
            <ta e="T70" id="Seg_5201" s="T69">0.3:S v:pred</ta>
            <ta e="T73" id="Seg_5202" s="T72">0.3:S v:pred</ta>
            <ta e="T75" id="Seg_5203" s="T74">np.h:S</ta>
            <ta e="T77" id="Seg_5204" s="T76">v:pred</ta>
            <ta e="T80" id="Seg_5205" s="T79">0.3.h:S v:pred</ta>
            <ta e="T83" id="Seg_5206" s="T82">pro.h:O</ta>
            <ta e="T84" id="Seg_5207" s="T83">0.2.h:S v:pred</ta>
            <ta e="T87" id="Seg_5208" s="T86">np.h:S</ta>
            <ta e="T89" id="Seg_5209" s="T88">v:pred</ta>
            <ta e="T91" id="Seg_5210" s="T90">np:O</ta>
            <ta e="T92" id="Seg_5211" s="T91">0.3.h:S v:pred</ta>
            <ta e="T94" id="Seg_5212" s="T93">np:O</ta>
            <ta e="T95" id="Seg_5213" s="T94">0.3.h:S v:pred</ta>
            <ta e="T99" id="Seg_5214" s="T98">0.3.h:S 0.3:O v:pred</ta>
            <ta e="T105" id="Seg_5215" s="T104">0.3.h:S 0.3.h:O v:pred</ta>
            <ta e="T108" id="Seg_5216" s="T107">0.3.h:S 0.3.h:O v:pred</ta>
            <ta e="T109" id="Seg_5217" s="T108">ptcl:pred</ta>
            <ta e="T110" id="Seg_5218" s="T109">ptcl:pred</ta>
            <ta e="T112" id="Seg_5219" s="T111">0.3.h:S n:pred</ta>
            <ta e="T114" id="Seg_5220" s="T113">0.3.h:S n:pred</ta>
            <ta e="T115" id="Seg_5221" s="T114">pro.h:S</ta>
            <ta e="T116" id="Seg_5222" s="T115">n:pred</ta>
            <ta e="T117" id="Seg_5223" s="T116">pro.h:S</ta>
            <ta e="T118" id="Seg_5224" s="T117">pro.h:O</ta>
            <ta e="T120" id="Seg_5225" s="T119">pro.h:O</ta>
            <ta e="T121" id="Seg_5226" s="T120">v:pred</ta>
            <ta e="T125" id="Seg_5227" s="T124">pro.h:O</ta>
            <ta e="T126" id="Seg_5228" s="T125">0.2.h:S v:pred</ta>
            <ta e="T128" id="Seg_5229" s="T127">pro.h:O</ta>
            <ta e="T129" id="Seg_5230" s="T128">0.2.h:S v:pred</ta>
            <ta e="T132" id="Seg_5231" s="T131">0.3.h:S 0.3.h:O v:pred</ta>
            <ta e="T137" id="Seg_5232" s="T136">pro.h:O</ta>
            <ta e="T138" id="Seg_5233" s="T137">0.2.h:S v:pred</ta>
            <ta e="T139" id="Seg_5234" s="T138">pro:O</ta>
            <ta e="T140" id="Seg_5235" s="T139">0.2.h:S v:pred</ta>
            <ta e="T141" id="Seg_5236" s="T140">pro:O</ta>
            <ta e="T142" id="Seg_5237" s="T141">np:O</ta>
            <ta e="T143" id="Seg_5238" s="T142">0.2.h:S v:pred</ta>
            <ta e="T145" id="Seg_5239" s="T144">0.3.h:S v:pred</ta>
            <ta e="T147" id="Seg_5240" s="T146">n:pred</ta>
            <ta e="T149" id="Seg_5241" s="T148">np.h:S</ta>
            <ta e="T152" id="Seg_5242" s="T151">np:O</ta>
            <ta e="T153" id="Seg_5243" s="T152">v:pred</ta>
            <ta e="T154" id="Seg_5244" s="T153">np:S</ta>
            <ta e="T156" id="Seg_5245" s="T155">v:pred</ta>
            <ta e="T158" id="Seg_5246" s="T157">0.3.h:S v:pred</ta>
            <ta e="T159" id="Seg_5247" s="T158">ptcl:pred</ta>
            <ta e="T160" id="Seg_5248" s="T159">np:S</ta>
            <ta e="T162" id="Seg_5249" s="T161">v:pred</ta>
            <ta e="T165" id="Seg_5250" s="T164">np:O</ta>
            <ta e="T166" id="Seg_5251" s="T165">0.1.h:S v:pred</ta>
            <ta e="T168" id="Seg_5252" s="T167">s:purp</ta>
            <ta e="T169" id="Seg_5253" s="T168">np:O</ta>
            <ta e="T170" id="Seg_5254" s="T169">0.3.h:S v:pred</ta>
            <ta e="T172" id="Seg_5255" s="T171">np:O</ta>
            <ta e="T174" id="Seg_5256" s="T173">0.3.h:S v:pred</ta>
            <ta e="T178" id="Seg_5257" s="T177">0.3.h:S v:pred</ta>
            <ta e="T179" id="Seg_5258" s="T178">0.3.h:S v:pred</ta>
            <ta e="T180" id="Seg_5259" s="T179">0.3.h:S v:pred</ta>
            <ta e="T183" id="Seg_5260" s="T182">np.h:S</ta>
            <ta e="T186" id="Seg_5261" s="T185">np:O</ta>
            <ta e="T187" id="Seg_5262" s="T186">v:pred</ta>
            <ta e="T188" id="Seg_5263" s="T187">np:S</ta>
            <ta e="T190" id="Seg_5264" s="T189">v:pred</ta>
            <ta e="T191" id="Seg_5265" s="T190">ptcl:pred</ta>
            <ta e="T192" id="Seg_5266" s="T191">np:S</ta>
            <ta e="T194" id="Seg_5267" s="T193">v:pred</ta>
            <ta e="T197" id="Seg_5268" s="T196">np:O</ta>
            <ta e="T198" id="Seg_5269" s="T197">0.1.h:S v:pred</ta>
            <ta e="T200" id="Seg_5270" s="T199">np:O</ta>
            <ta e="T204" id="Seg_5271" s="T203">0.3.h:S v:pred</ta>
            <ta e="T208" id="Seg_5272" s="T207">0.3.h:S v:pred</ta>
            <ta e="T209" id="Seg_5273" s="T208">0.3.h:S v:pred</ta>
            <ta e="T210" id="Seg_5274" s="T209">ptcl:pred</ta>
            <ta e="T213" id="Seg_5275" s="T212">np:O</ta>
            <ta e="T216" id="Seg_5276" s="T215">0.3.h:S v:pred</ta>
            <ta e="T217" id="Seg_5277" s="T216">np:S</ta>
            <ta e="T219" id="Seg_5278" s="T218">v:pred</ta>
            <ta e="T222" id="Seg_5279" s="T221">0.3.h:S v:pred</ta>
            <ta e="T224" id="Seg_5280" s="T223">np:S</ta>
            <ta e="T227" id="Seg_5281" s="T226">adj:pred</ta>
            <ta e="T228" id="Seg_5282" s="T227">cop</ta>
            <ta e="T231" id="Seg_5283" s="T230">np:S</ta>
            <ta e="T232" id="Seg_5284" s="T231">adj:pred</ta>
            <ta e="T233" id="Seg_5285" s="T232">cop</ta>
            <ta e="T234" id="Seg_5286" s="T233">np:O</ta>
            <ta e="T235" id="Seg_5287" s="T234">0.1.h:S v:pred</ta>
            <ta e="T240" id="Seg_5288" s="T239">0.3.h:S v:pred</ta>
            <ta e="T242" id="Seg_5289" s="T241">np:O</ta>
            <ta e="T244" id="Seg_5290" s="T243">v:pred</ta>
            <ta e="T249" id="Seg_5291" s="T248">0.3.h:S v:pred</ta>
            <ta e="T251" id="Seg_5292" s="T250">0.3.h:S v:pred</ta>
            <ta e="T256" id="Seg_5293" s="T255">np.h:S</ta>
            <ta e="T258" id="Seg_5294" s="T257">np:O</ta>
            <ta e="T259" id="Seg_5295" s="T258">v:pred</ta>
            <ta e="T260" id="Seg_5296" s="T259">np:O</ta>
            <ta e="T262" id="Seg_5297" s="T261">0.3.h:S v:pred</ta>
            <ta e="T263" id="Seg_5298" s="T262">np:S</ta>
            <ta e="T264" id="Seg_5299" s="T263">v:pred</ta>
            <ta e="T266" id="Seg_5300" s="T265">np:S</ta>
            <ta e="T268" id="Seg_5301" s="T267">v:pred</ta>
            <ta e="T269" id="Seg_5302" s="T268">np:O</ta>
            <ta e="T270" id="Seg_5303" s="T269">0.3.h:S v:pred</ta>
            <ta e="T277" id="Seg_5304" s="T276">np:O</ta>
            <ta e="T279" id="Seg_5305" s="T278">0.3.h:S v:pred</ta>
            <ta e="T284" id="Seg_5306" s="T283">0.3.h:S v:pred</ta>
            <ta e="T285" id="Seg_5307" s="T284">0.3.h:S v:pred</ta>
            <ta e="T291" id="Seg_5308" s="T290">np.h:S</ta>
            <ta e="T292" id="Seg_5309" s="T291">np:O</ta>
            <ta e="T294" id="Seg_5310" s="T293">v:pred</ta>
            <ta e="T295" id="Seg_5311" s="T294">np:S</ta>
            <ta e="T297" id="Seg_5312" s="T296">v:pred</ta>
            <ta e="T299" id="Seg_5313" s="T298">np:O</ta>
            <ta e="T300" id="Seg_5314" s="T299">0.1.h:S v:pred</ta>
            <ta e="T303" id="Seg_5315" s="T302">np:O</ta>
            <ta e="T307" id="Seg_5316" s="T306">0.3.h:S v:pred</ta>
            <ta e="T310" id="Seg_5317" s="T309">0.3.h:S v:pred</ta>
            <ta e="T311" id="Seg_5318" s="T310">0.3.h:S v:pred</ta>
            <ta e="T314" id="Seg_5319" s="T313">np.h:S</ta>
            <ta e="T320" id="Seg_5320" s="T319">np:O</ta>
            <ta e="T321" id="Seg_5321" s="T320">v:pred</ta>
            <ta e="T324" id="Seg_5322" s="T321">n:pred</ta>
            <ta e="T325" id="Seg_5323" s="T324">ptcl:pred</ta>
            <ta e="T326" id="Seg_5324" s="T325">np:S</ta>
            <ta e="T328" id="Seg_5325" s="T327">v:pred</ta>
            <ta e="T329" id="Seg_5326" s="T328">ptcl:pred</ta>
            <ta e="T331" id="Seg_5327" s="T330">0.1.h:S n:pred</ta>
            <ta e="T335" id="Seg_5328" s="T332">s:purp</ta>
            <ta e="T337" id="Seg_5329" s="T335">s:purp</ta>
            <ta e="T341" id="Seg_5330" s="T340">np:O</ta>
            <ta e="T342" id="Seg_5331" s="T341">0.1.h:S v:pred</ta>
            <ta e="T348" id="Seg_5332" s="T347">0.3.h:S v:pred</ta>
            <ta e="T349" id="Seg_5333" s="T348">np:O</ta>
            <ta e="T350" id="Seg_5334" s="T349">0.3.h:S v:pred</ta>
            <ta e="T356" id="Seg_5335" s="T355">0.3.h:S v:pred</ta>
            <ta e="T357" id="Seg_5336" s="T356">0.3.h:S v:pred</ta>
            <ta e="T358" id="Seg_5337" s="T357">np:S</ta>
            <ta e="T359" id="Seg_5338" s="T358">v:pred</ta>
            <ta e="T364" id="Seg_5339" s="T363">0.3.h:S v:pred</ta>
            <ta e="T365" id="Seg_5340" s="T364">np:S</ta>
            <ta e="T366" id="Seg_5341" s="T365">v:pred</ta>
            <ta e="T371" id="Seg_5342" s="T370">0.3.h:S 0.3:O v:pred</ta>
            <ta e="T372" id="Seg_5343" s="T371">s:adv</ta>
            <ta e="T373" id="Seg_5344" s="T372">pro.h:S</ta>
            <ta e="T374" id="Seg_5345" s="T373">pro:O</ta>
            <ta e="T375" id="Seg_5346" s="T374">v:pred</ta>
            <ta e="T379" id="Seg_5347" s="T378">np.h:S</ta>
            <ta e="T380" id="Seg_5348" s="T379">ptcl:pred</ta>
            <ta e="T381" id="Seg_5349" s="T380">pro.h:S</ta>
            <ta e="T382" id="Seg_5350" s="T381">np:O</ta>
            <ta e="T384" id="Seg_5351" s="T383">v:pred</ta>
            <ta e="T386" id="Seg_5352" s="T385">np:O</ta>
            <ta e="T387" id="Seg_5353" s="T386">0.1.h:S v:pred</ta>
            <ta e="T388" id="Seg_5354" s="T387">np:O</ta>
            <ta e="T390" id="Seg_5355" s="T389">0.1.h:S v:pred</ta>
            <ta e="T391" id="Seg_5356" s="T390">ptcl:pred</ta>
            <ta e="T394" id="Seg_5357" s="T393">0.2.h:S v:pred</ta>
            <ta e="T395" id="Seg_5358" s="T394">0.2.h:S v:pred</ta>
            <ta e="T398" id="Seg_5359" s="T397">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T266" id="Seg_5360" s="T265">accs-Q</ta>
            <ta e="T269" id="Seg_5361" s="T268">giv-active-Q</ta>
            <ta e="T272" id="Seg_5362" s="T271">giv-active-Q</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T41" id="Seg_5363" s="T40">RUS:disc</ta>
            <ta e="T79" id="Seg_5364" s="T78">RUS:disc</ta>
            <ta e="T113" id="Seg_5365" s="T112">RUS:cult</ta>
            <ta e="T114" id="Seg_5366" s="T113">RUS:cult</ta>
            <ta e="T130" id="Seg_5367" s="T129">RUS:cult</ta>
            <ta e="T131" id="Seg_5368" s="T130">RUS:cult</ta>
            <ta e="T149" id="Seg_5369" s="T148">RUS:cult</ta>
            <ta e="T171" id="Seg_5370" s="T170">RUS:cult</ta>
            <ta e="T172" id="Seg_5371" s="T171">RUS:cult</ta>
            <ta e="T183" id="Seg_5372" s="T182">RUS:cult</ta>
            <ta e="T195" id="Seg_5373" s="T194">RUS:cult</ta>
            <ta e="T232" id="Seg_5374" s="T231">RUS:cult</ta>
            <ta e="T256" id="Seg_5375" s="T255">RUS:cult</ta>
            <ta e="T266" id="Seg_5376" s="T265">RUS:cult</ta>
            <ta e="T268" id="Seg_5377" s="T267">RUS:core</ta>
            <ta e="T291" id="Seg_5378" s="T290">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T266" id="Seg_5379" s="T265">parad:infl</ta>
            <ta e="T268" id="Seg_5380" s="T267">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T4" id="Seg_5381" s="T0">RUS:ext</ta>
            <ta e="T244" id="Seg_5382" s="T243">RUS:int</ta>
            <ta e="T268" id="Seg_5383" s="T266">int:ins</ta>
            <ta e="T402" id="Seg_5384" s="T398">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_5385" s="T0">Старик, когда вода замёрзнет, (…).</ta>
            <ta e="T11" id="Seg_5386" s="T4">Когда вода [замёрзла?], старик ушёл охотиться (/рыбачить).</ta>
            <ta e="T25" id="Seg_5387" s="T11">Когда река [замёрзает?], старик по берегу реки идёт охотиться, идёт и туда[-сюда] смотрит - ой.</ta>
            <ta e="T31" id="Seg_5388" s="T25">Однажды он [видит], кто-то [старается?].</ta>
            <ta e="T37" id="Seg_5389" s="T31">То ли человек кричит, то ли кто-то [старается?].</ta>
            <ta e="T43" id="Seg_5390" s="T37">Что это, человек или кто кричит? </ta>
            <ta e="T47" id="Seg_5391" s="T43">Старик так слышит.</ta>
            <ta e="T49" id="Seg_5392" s="T47">Туда идёт. </ta>
            <ta e="T50" id="Seg_5393" s="T49">Ой! </ta>
            <ta e="T55" id="Seg_5394" s="T50">Там в полынье что-то есть?</ta>
            <ta e="T60" id="Seg_5395" s="T55">В полынье что-то барахтается.</ta>
            <ta e="T67" id="Seg_5396" s="T60">Только вверх вскарабкается, опять вниз уходит.</ta>
            <ta e="T70" id="Seg_5397" s="T67">Опять вниз уходит.</ta>
            <ta e="T78" id="Seg_5398" s="T70"> ‎‎Туда пришел, видать, человек и упал в воду, в полынью.</ta>
            <ta e="T84" id="Seg_5399" s="T78">"Пожалуйста," — кричит, — "вытащи меня наверх!"</ta>
            <ta e="T89" id="Seg_5400" s="T84">Тут этот старик бежит на берег.</ta>
            <ta e="T92" id="Seg_5401" s="T89">Берёт длинный тальник.</ta>
            <ta e="T95" id="Seg_5402" s="T92">Длинный тальник рубит.</ta>
            <ta e="T100" id="Seg_5403" s="T95">С берега берёт, (/берёт, бросает) старику.</ta>
            <ta e="T105" id="Seg_5404" s="T100">С этим тальником [его] на берег [из воды] тащит.</ta>
            <ta e="T108" id="Seg_5405" s="T105">На берег только вытащил.</ta>
            <ta e="T110" id="Seg_5406" s="T108">"Ой! ой! </ta>
            <ta e="T116" id="Seg_5407" s="T110">Это же купец, богатый купец!"</ta>
            <ta e="T126" id="Seg_5408" s="T116">"Ты меня наверх вытащил, от смерти меня наверх вытащил.</ta>
            <ta e="T129" id="Seg_5409" s="T126">Ты меня наверх вытащил".</ta>
            <ta e="T132" id="Seg_5410" s="T129">Он даёт [старику] кусок золота.</ta>
            <ta e="T138" id="Seg_5411" s="T132">"Ты будто от смерти меня наверх вытащил.</ta>
            <ta e="T143" id="Seg_5412" s="T138">Что возьмёшь, что жизненного возьмёшь?"</ta>
            <ta e="T145" id="Seg_5413" s="T143">Вот идёт он.</ta>
            <ta e="T156" id="Seg_5414" s="T145">Ого, лошади, один русский [столько] лошадей гонит, конца не видать!</ta>
            <ta e="T158" id="Seg_5415" s="T156">Идёт [дальше].</ta>
            <ta e="T162" id="Seg_5416" s="T158">"Эй", — думает он.</ta>
            <ta e="T168" id="Seg_5417" s="T162">"Наверное, я бы лошадь взял для работы."</ta>
            <ta e="T174" id="Seg_5418" s="T168">Лошадь берёт, кусок золота отдаёт.</ta>
            <ta e="T180" id="Seg_5419" s="T174">Потом с этой лошадью идёт, идёт.</ta>
            <ta e="T190" id="Seg_5420" s="T180">Однажды видит, русский [столько] коров гонит, конца не видать.</ta>
            <ta e="T194" id="Seg_5421" s="T190">"Эй", — думает он.</ta>
            <ta e="T199" id="Seg_5422" s="T194">"Для молока я бы взял корову".</ta>
            <ta e="T204" id="Seg_5423" s="T199">Лошадь за корову отдаёт.</ta>
            <ta e="T209" id="Seg_5424" s="T204">С этой коровой идёт, идёт.</ta>
            <ta e="T219" id="Seg_5425" s="T209">Ой, видит, однажды свиней гонят, конца не видно.</ta>
            <ta e="T228" id="Seg_5426" s="T219">Потом он говорит: "Эй, свинья такая жирная!"</ta>
            <ta e="T236" id="Seg_5427" s="T228">Эй, свинья [такая] жирная, свинью я бы взял". </ta>
            <ta e="T242" id="Seg_5428" s="T236">За свинью отдаёт корову.</ta>
            <ta e="T244" id="Seg_5429" s="T242">Опять поменялся.</ta>
            <ta e="T251" id="Seg_5430" s="T244">Потом с этой свиньёй идёт, идёт.</ta>
            <ta e="T259" id="Seg_5431" s="T251">Однажды этот русский [домашних?] лебедей гонит.</ta>
            <ta e="T264" id="Seg_5432" s="T259">Лебедей он столько гонит, конца нет.</ta>
            <ta e="T274" id="Seg_5433" s="T264">"Наверное яиц-то много, лебедя я бы взял (ради яиц)".</ta>
            <ta e="T279" id="Seg_5434" s="T274">За лебедя свинью отдаёт. </ta>
            <ta e="T286" id="Seg_5435" s="T279">Потом с этим лебедем идёт, идёт домой.</ta>
            <ta e="T297" id="Seg_5436" s="T286">Однажды видит, русский столько гусей гонит, конца не видно.</ta>
            <ta e="T301" id="Seg_5437" s="T297">"Наверное гуся я бы взял".</ta>
            <ta e="T307" id="Seg_5438" s="T301">Лебедя за гуся отдаёт.</ta>
            <ta e="T311" id="Seg_5439" s="T307">Потом с гусём идёт, идёт.</ta>
            <ta e="T321" id="Seg_5440" s="T311">[Видит] один старичок, который, говорят, иголку величиной с посох сделал.</ta>
            <ta e="T324" id="Seg_5441" s="T321">"[Вот] иголка так иголка!"</ta>
            <ta e="T328" id="Seg_5442" s="T324">"Эй," — думает он.</ta>
            <ta e="T332" id="Seg_5443" s="T328">"Эх, я безглазый чёрт, товарищ!</ta>
            <ta e="T343" id="Seg_5444" s="T332">Что-то шить, [чтобы] что-то шить, наверное я бы у этого старика иголку взял."</ta>
            <ta e="T352" id="Seg_5445" s="T343">За иголку даёт гуся, отдаёт за иголку.</ta>
            <ta e="T357" id="Seg_5446" s="T352">Потом с этой иголкой идёт, идёт.</ta>
            <ta e="T359" id="Seg_5447" s="T357">Иголка потерялась.</ta>
            <ta e="T366" id="Seg_5448" s="T359">Через [сени] в дом только вошёл, как иголка потерялась.</ta>
            <ta e="T372" id="Seg_5449" s="T366">В сенях ищет ощупью.</ta>
            <ta e="T376" id="Seg_5450" s="T372">"Ты что ищешь, старик?"</ta>
            <ta e="T379" id="Seg_5451" s="T376">Из дома напротив человек [спрашивает].</ta>
            <ta e="T387" id="Seg_5452" s="T379">"Эх, я иголку только принёс, [как] эту иголку потерял.</ta>
            <ta e="T390" id="Seg_5453" s="T387">Эту иголку ищу". </ta>
            <ta e="T391" id="Seg_5454" s="T390">"Ой!</ta>
            <ta e="T395" id="Seg_5455" s="T391">К своей старухе в дом заходи, покушай". </ta>
            <ta e="T398" id="Seg_5456" s="T395">Тут и конец.</ta>
            <ta e="T402" id="Seg_5457" s="T398">Небольшого ума старик-то.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_5458" s="T0">Old man, when the water will be frozen up, (…).</ta>
            <ta e="T11" id="Seg_5459" s="T4">When the water was frozen up, the old man went to fish.</ta>
            <ta e="T25" id="Seg_5460" s="T11">When the river [is frozen up], the old man goes hunting along the river bank, he goes and watches here and there - oh.</ta>
            <ta e="T31" id="Seg_5461" s="T25">Once he [sees], somebody [is trying?].</ta>
            <ta e="T37" id="Seg_5462" s="T31">Either a human is shouting or somebody [is trying?].</ta>
            <ta e="T43" id="Seg_5463" s="T37">What is it, is it a human shouting or what?</ta>
            <ta e="T47" id="Seg_5464" s="T43">The old man hears it.</ta>
            <ta e="T49" id="Seg_5465" s="T47">He goes there.</ta>
            <ta e="T50" id="Seg_5466" s="T49">Oh!</ta>
            <ta e="T55" id="Seg_5467" s="T50">Is there something in the ice hole?</ta>
            <ta e="T60" id="Seg_5468" s="T55">Something is moving in the ice hole.</ta>
            <ta e="T67" id="Seg_5469" s="T60">It almost climbs up and then it goes down again.</ta>
            <ta e="T70" id="Seg_5470" s="T67">It goes down again.</ta>
            <ta e="T78" id="Seg_5471" s="T70">Apparently a human walked there and fell into the ice hole.</ta>
            <ta e="T84" id="Seg_5472" s="T78">"Please," — he cries, — "pull me up!" </ta>
            <ta e="T89" id="Seg_5473" s="T84">That old man runs to the shore.</ta>
            <ta e="T92" id="Seg_5474" s="T89">He takes a long purple osier.</ta>
            <ta e="T95" id="Seg_5475" s="T92">He chops the long purple osier.</ta>
            <ta e="T100" id="Seg_5476" s="T95">He takes it from the shore, (/takes, throws it) to the old man.</ta>
            <ta e="T105" id="Seg_5477" s="T100">He pulls [him] with this osier [out of the water].</ta>
            <ta e="T108" id="Seg_5478" s="T105">He just pulled him on the shore.</ta>
            <ta e="T110" id="Seg_5479" s="T108">"Oh! oh!</ta>
            <ta e="T116" id="Seg_5480" s="T110">It is apparentely a merchant, a very rich merchant!"</ta>
            <ta e="T126" id="Seg_5481" s="T116">"You've pulled me up, you've pulled me up from death.</ta>
            <ta e="T129" id="Seg_5482" s="T126">You've pulled me up".</ta>
            <ta e="T132" id="Seg_5483" s="T129">He gives [the old man] a piece of gold.</ta>
            <ta e="T138" id="Seg_5484" s="T132">"As if you pulled me out of the death.</ta>
            <ta e="T143" id="Seg_5485" s="T138">What will you take, something for living?"</ta>
            <ta e="T145" id="Seg_5486" s="T143">Well, he goes.</ta>
            <ta e="T156" id="Seg_5487" s="T145">Hey, horses, one Russian is driving so many horses, the end is not visible!</ta>
            <ta e="T158" id="Seg_5488" s="T156">He goes [on].</ta>
            <ta e="T162" id="Seg_5489" s="T158">"Hey", — he thinks.</ta>
            <ta e="T168" id="Seg_5490" s="T162">"I would probably take a horse for work."</ta>
            <ta e="T174" id="Seg_5491" s="T168">He takes a horse and gives the piece of gold away.</ta>
            <ta e="T180" id="Seg_5492" s="T174">Then he goes away with this horse, he goes on.</ta>
            <ta e="T190" id="Seg_5493" s="T180">Once he sees a Russian, driving so many cows, the end is not visible.</ta>
            <ta e="T194" id="Seg_5494" s="T190">"Hey", — he thinks.</ta>
            <ta e="T199" id="Seg_5495" s="T194">"I would take a cow for the milk".</ta>
            <ta e="T204" id="Seg_5496" s="T199">He gives his horse away for a cow.</ta>
            <ta e="T209" id="Seg_5497" s="T204">With this cow he goes on.</ta>
            <ta e="T219" id="Seg_5498" s="T209">Oh, once he sees they are driving pigs, the end is not visible.</ta>
            <ta e="T228" id="Seg_5499" s="T219">Then he says: "Hey, a pig is so fat!"</ta>
            <ta e="T236" id="Seg_5500" s="T228">"Hey, a pig is so fat, I would take it".</ta>
            <ta e="T242" id="Seg_5501" s="T236">He gives his horse for a pig.</ta>
            <ta e="T244" id="Seg_5502" s="T242">He exchanged again.</ta>
            <ta e="T251" id="Seg_5503" s="T244">Then he goes on with this pig.</ta>
            <ta e="T259" id="Seg_5504" s="T251">Once that Russian is driving swans.</ta>
            <ta e="T264" id="Seg_5505" s="T259">He ist driving so many swans, there is no end.</ta>
            <ta e="T274" id="Seg_5506" s="T264">"I could probably get a lot of eggs, I would take a swan for the eggs".</ta>
            <ta e="T279" id="Seg_5507" s="T274">He gives his pig for a swan.</ta>
            <ta e="T286" id="Seg_5508" s="T279">Then he goes on with the swan, he goes home.</ta>
            <ta e="T297" id="Seg_5509" s="T286">Once he sees the Russian driving so many geese, the end is not visible.</ta>
            <ta e="T301" id="Seg_5510" s="T297">"I would probably take a goose".</ta>
            <ta e="T307" id="Seg_5511" s="T301">He gives his swan away for a goose.</ta>
            <ta e="T311" id="Seg_5512" s="T307">Then he goes on with the goose.</ta>
            <ta e="T321" id="Seg_5513" s="T311">[He sees] an old man who, as they say, made a needle as big as a crook.</ta>
            <ta e="T324" id="Seg_5514" s="T321">"Well, that's a needle!"</ta>
            <ta e="T328" id="Seg_5515" s="T324">"Hey," — he thinks.</ta>
            <ta e="T332" id="Seg_5516" s="T328">"Hey, I'm a blind devil, comrade!</ta>
            <ta e="T343" id="Seg_5517" s="T332">I would probably take the needle of this old man, in order to sew something.</ta>
            <ta e="T352" id="Seg_5518" s="T343">He gives his goose away for the needle.</ta>
            <ta e="T357" id="Seg_5519" s="T352">Then he goes with this needle.</ta>
            <ta e="T359" id="Seg_5520" s="T357">The needle got lost.</ta>
            <ta e="T366" id="Seg_5521" s="T359">He had hardly entered the house, when the needle got lost.</ta>
            <ta e="T372" id="Seg_5522" s="T366">In the mudroom he tries to find it.</ta>
            <ta e="T376" id="Seg_5523" s="T372">"What are you searching for, old man?"</ta>
            <ta e="T379" id="Seg_5524" s="T376">A human [asks] from the house across the road.</ta>
            <ta e="T387" id="Seg_5525" s="T379">"Hey, I've just brought a needle, I lost this needle.</ta>
            <ta e="T390" id="Seg_5526" s="T387">I'm searching for this needle".</ta>
            <ta e="T391" id="Seg_5527" s="T390">"Oh!</ta>
            <ta e="T395" id="Seg_5528" s="T391">Go into the house, to your old woman, eat".</ta>
            <ta e="T398" id="Seg_5529" s="T395">Here is the end.</ta>
            <ta e="T402" id="Seg_5530" s="T398">An old man with little mind, right.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_5531" s="T0">Alter Mann, wenn das Wasser zufriert, (…).</ta>
            <ta e="T11" id="Seg_5532" s="T4">Als das Wasser zugefroren war, ging der alte Mann fischen.</ta>
            <ta e="T25" id="Seg_5533" s="T11">Wenn der Fluss [zufriert], geht der Alte entlang des Ufers jagen, er läuft und schaut hier und da - oh.</ta>
            <ta e="T31" id="Seg_5534" s="T25">Einmal [sieht] er, jemand [versucht?].</ta>
            <ta e="T37" id="Seg_5535" s="T31">Entweder ruft ein Mensch oder jemand [versucht?].</ta>
            <ta e="T43" id="Seg_5536" s="T37">Was ist das, ist es ein Mensch, der schreit, oder was?</ta>
            <ta e="T47" id="Seg_5537" s="T43">Der Alte hört es.</ta>
            <ta e="T49" id="Seg_5538" s="T47">Er geht dorthin.</ta>
            <ta e="T50" id="Seg_5539" s="T49">Oh!</ta>
            <ta e="T55" id="Seg_5540" s="T50">Ist dort etwas im Eisloch?</ta>
            <ta e="T60" id="Seg_5541" s="T55">Etwas bewegt sich im Eisloch.</ta>
            <ta e="T67" id="Seg_5542" s="T60">Es klettert fast hinaus und geht wieder runter. </ta>
            <ta e="T70" id="Seg_5543" s="T67">Es geht wieder runter.</ta>
            <ta e="T78" id="Seg_5544" s="T70">Wahrscheinlich ging dort ein Mensch und fiel in das Eisloch.</ta>
            <ta e="T84" id="Seg_5545" s="T78">"Bitte," — ruft er, — "zieh mich hoch!"</ta>
            <ta e="T89" id="Seg_5546" s="T84">Dieser Alte läuft zum Ufer.</ta>
            <ta e="T92" id="Seg_5547" s="T89">Er nimmt einen langen Weiden[ast].</ta>
            <ta e="T95" id="Seg_5548" s="T92">Er schneidet den langen Weiden[ast] ab.</ta>
            <ta e="T100" id="Seg_5549" s="T95">Vom Ufer nimmt er ihn, (/nimmt, wirft ihn) dem alten Mann zu.</ta>
            <ta e="T105" id="Seg_5550" s="T100">Er zieht [ihn] mit dem Ast [aus dem Wasser].</ta>
            <ta e="T108" id="Seg_5551" s="T105">Er zog ihn auf das Ufer.</ta>
            <ta e="T110" id="Seg_5552" s="T108">"Oh, oh!</ta>
            <ta e="T116" id="Seg_5553" s="T110">Das ist doch ein Kaufmann, ein sehr reicher Kaufmann!"</ta>
            <ta e="T126" id="Seg_5554" s="T116">"Du hast mich herausgezogen, du hast mich vor dem Tod herausgezogen.</ta>
            <ta e="T129" id="Seg_5555" s="T126">Du hast mich herausgezogen".</ta>
            <ta e="T132" id="Seg_5556" s="T129">Er gibt [dem Alten] ein Stück Gold.</ta>
            <ta e="T138" id="Seg_5557" s="T132">"Als ob du mich vom Tod herausggezogen hast.</ta>
            <ta e="T143" id="Seg_5558" s="T138">Was wirst du nehmen, etwas zum Leben?"</ta>
            <ta e="T145" id="Seg_5559" s="T143">Also geht er.</ta>
            <ta e="T156" id="Seg_5560" s="T145">Hey, Pferde, ein Russe treibt so viele Pferde, dass kein Ende zu sehen ist!</ta>
            <ta e="T158" id="Seg_5561" s="T156">Er geht [weiter].</ta>
            <ta e="T162" id="Seg_5562" s="T158">"Hey", — denkt er.</ta>
            <ta e="T168" id="Seg_5563" s="T162">"Ich würde wahrscheinlich ein Pferd zum Arbeiten nehmen."</ta>
            <ta e="T174" id="Seg_5564" s="T168">Er nimmt ein Pferd und gibt das Stück Gold.</ta>
            <ta e="T180" id="Seg_5565" s="T174">Dann geht er mit diesem Pferd, er geht weiter.</ta>
            <ta e="T190" id="Seg_5566" s="T180">Einmal sieht er einen Russen, der Kühe treibt, so viele, dass kein Ende zu sehen ist.</ta>
            <ta e="T194" id="Seg_5567" s="T190">"Hey", — denkt er.</ta>
            <ta e="T199" id="Seg_5568" s="T194">"Ich würde eine Kuh wegen der Milch nehmen."</ta>
            <ta e="T204" id="Seg_5569" s="T199">Er gibt sein Pferd für eine Kuh.</ta>
            <ta e="T209" id="Seg_5570" s="T204">Mit der Kuh geht er weiter.</ta>
            <ta e="T219" id="Seg_5571" s="T209">Oh, einmal sieht er, dass Schweine getrieben werden, so viele, dass kein Ende zu sehen ist.</ta>
            <ta e="T228" id="Seg_5572" s="T219">Dann sagt er: "Hey, ein Schwein ist so fett!"</ta>
            <ta e="T236" id="Seg_5573" s="T228">"Hey, ein Schwein ist so fett, ich würde es nehmen."</ta>
            <ta e="T242" id="Seg_5574" s="T236">Er gibt sein Pferd für ein Schwein.</ta>
            <ta e="T244" id="Seg_5575" s="T242">Er tauschte wieder.</ta>
            <ta e="T251" id="Seg_5576" s="T244">Dann geht er mit dem Schwein weiter.</ta>
            <ta e="T259" id="Seg_5577" s="T251">Einmal treibt dieser Russe Schwäne.</ta>
            <ta e="T264" id="Seg_5578" s="T259">Er treibt so viele Schwäne, es gibt kein Ende.</ta>
            <ta e="T274" id="Seg_5579" s="T264">"Ich könnte wohl viele Eier bekommen, ich würde einen Schwan nehmen wegen der Eier."</ta>
            <ta e="T279" id="Seg_5580" s="T274">Er gibt sein Schwein für einen Schwan.</ta>
            <ta e="T286" id="Seg_5581" s="T279">Dann geht er mit dem Schwan weiter, er geht nach Hause.</ta>
            <ta e="T297" id="Seg_5582" s="T286">Einmal sieht er, wie der Russe so viele Gänse treibt, dass kein Ende zu sehen ist.</ta>
            <ta e="T301" id="Seg_5583" s="T297">"Ich würde wahrscheinlich eine Gans nehmen."</ta>
            <ta e="T307" id="Seg_5584" s="T301">Er gibt seinen Schwan für eine Gans.</ta>
            <ta e="T311" id="Seg_5585" s="T307">Dann geht er mit der Gans weiter.</ta>
            <ta e="T321" id="Seg_5586" s="T311">[Er sieht] einen alten Mann, der, wie man sagt, eine Nadel so groß gemacht hat wie ein Hirtenstab.</ta>
            <ta e="T324" id="Seg_5587" s="T321">"Na, das ist eine Nadel!"</ta>
            <ta e="T328" id="Seg_5588" s="T324">"Hey," — denkt er.</ta>
            <ta e="T332" id="Seg_5589" s="T328">"Hey, ich bin ein blinder Teufel, Kumpel!</ta>
            <ta e="T343" id="Seg_5590" s="T332">Ich würde wahrscheinlich die Nadel von dem Alten nehmen, um etwas zu nähen.</ta>
            <ta e="T352" id="Seg_5591" s="T343">Er gibt seine Gans für die Nadel.</ta>
            <ta e="T357" id="Seg_5592" s="T352">Dann geht er mit der Nadel weiter.</ta>
            <ta e="T359" id="Seg_5593" s="T357">Die Nadel ist verloren gegangen.</ta>
            <ta e="T366" id="Seg_5594" s="T359">Kaum hat er das Haus betreten, da ging die Nadel verloren.</ta>
            <ta e="T372" id="Seg_5595" s="T366">In der Diele versucht er [sie] zu finden.</ta>
            <ta e="T376" id="Seg_5596" s="T372">"Wonach suchst du, Alter?"</ta>
            <ta e="T379" id="Seg_5597" s="T376">[fragt] ein Mensch aus dem Haus von gegenüber.</ta>
            <ta e="T387" id="Seg_5598" s="T379">"Hey, ich habe gerade eine Nadel mitgebracht, die Nadel habe ich verloren.</ta>
            <ta e="T390" id="Seg_5599" s="T387">Diese Nadel suche ich."</ta>
            <ta e="T391" id="Seg_5600" s="T390">"Oh!</ta>
            <ta e="T395" id="Seg_5601" s="T391">Geh ins Haus hinein, zu deiner Alten, iss."</ta>
            <ta e="T398" id="Seg_5602" s="T395">Hier ist das Ende.</ta>
            <ta e="T402" id="Seg_5603" s="T398">Ein alter Mann mit geringem Verstand, oder.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T11" id="Seg_5604" s="T4">тогда когда вода замерзнет старик охотиться ушел.</ta>
            <ta e="T25" id="Seg_5605" s="T11">река (широкая) когда замерзнет (поймается, покроется льдом) старик по реке по берегу вверх смотрит шагает [за пушниной] охотиться туда-сюда смотревши</ta>
            <ta e="T31" id="Seg_5606" s="T25">однажды он что-то он видит</ta>
            <ta e="T37" id="Seg_5607" s="T31">или человек кричит или что-то такое</ta>
            <ta e="T43" id="Seg_5608" s="T37">что это человек или что кричит </ta>
            <ta e="T47" id="Seg_5609" s="T43">старик так услышал</ta>
            <ta e="T49" id="Seg_5610" s="T47">туда пришел</ta>
            <ta e="T50" id="Seg_5611" s="T49">оой </ta>
            <ta e="T55" id="Seg_5612" s="T50">там талое место (прорубь), что-то такое есть</ta>
            <ta e="T60" id="Seg_5613" s="T55">в этой проруби что-то такое барахтается</ta>
            <ta e="T67" id="Seg_5614" s="T60">вверх вскарабкается опять вниз уйдет</ta>
            <ta e="T84" id="Seg_5615" s="T78">пожалуйста кричит: меня вверх вытащи (вверх тащи)</ta>
            <ta e="T89" id="Seg_5616" s="T84">тот старик на берег побежал</ta>
            <ta e="T92" id="Seg_5617" s="T89">длинный тальник взял</ta>
            <ta e="T95" id="Seg_5618" s="T92">длинный тальник срубил</ta>
            <ta e="T100" id="Seg_5619" s="T95">с берега бросил старику</ta>
            <ta e="T105" id="Seg_5620" s="T100">с этим тальником с реки на берег потащил</ta>
            <ta e="T108" id="Seg_5621" s="T105">на берег вытащил</ta>
            <ta e="T110" id="Seg_5622" s="T108">оо! оо! </ta>
            <ta e="T116" id="Seg_5623" s="T110">это же [увидел, что] это богатый</ta>
            <ta e="T126" id="Seg_5624" s="T116">ты меня поднял [взял (спас)] от смерти вверх поднял меня</ta>
            <ta e="T132" id="Seg_5625" s="T129">золота кусок отдал</ta>
            <ta e="T138" id="Seg_5626" s="T132">будто бы из смерти ты вверх меня взял</ta>
            <ta e="T143" id="Seg_5627" s="T138">что возьмешь, что жизненного возьмешь?</ta>
            <ta e="T145" id="Seg_5628" s="T143">но шел</ta>
            <ta e="T156" id="Seg_5629" s="T145">один русский столько лошадей гонит конца не видать</ta>
            <ta e="T158" id="Seg_5630" s="T156">туда пришел</ta>
            <ta e="T162" id="Seg_5631" s="T158">ум так повернулся [он так задумал]</ta>
            <ta e="T168" id="Seg_5632" s="T162">наверное я лошадь бы взял работать</ta>
            <ta e="T174" id="Seg_5633" s="T168">лошадь взял кусок золота тому отдал</ta>
            <ta e="T180" id="Seg_5634" s="T174">потом с этой лошадью шел, шел (=долго шел)</ta>
            <ta e="T190" id="Seg_5635" s="T180">в одно время один русский столько коров гонит, конца не видать</ta>
            <ta e="T194" id="Seg_5636" s="T190">ум так повернулся [так задумал]</ta>
            <ta e="T199" id="Seg_5637" s="T194">для молока бы взял корову</ta>
            <ta e="T204" id="Seg_5638" s="T199">лошадь за корову отдал</ta>
            <ta e="T209" id="Seg_5639" s="T204">с этой коровой шел, шел</ta>
            <ta e="T219" id="Seg_5640" s="T209">ой в одно время свиней гонят конца не видать</ta>
            <ta e="T228" id="Seg_5641" s="T219">ой свинья такая жирная</ta>
            <ta e="T236" id="Seg_5642" s="T228">свинью взял бы</ta>
            <ta e="T242" id="Seg_5643" s="T236">за свинью тому отдал эту корову</ta>
            <ta e="T251" id="Seg_5644" s="T244">потом со свиньей опять пошел</ta>
            <ta e="T259" id="Seg_5645" s="T251">этот русский домашних лебедей гонит</ta>
            <ta e="T264" id="Seg_5646" s="T259">лебедей столько гонит конца нет</ta>
            <ta e="T274" id="Seg_5647" s="T264">наверное лебедя взял бы за яйца</ta>
            <ta e="T279" id="Seg_5648" s="T274">на лебедя свинью переменил тому отдал </ta>
            <ta e="T286" id="Seg_5649" s="T279">потом с этим лебедем опять пошел домой</ta>
            <ta e="T297" id="Seg_5650" s="T286">русский гусей столько гонит конца края не видать</ta>
            <ta e="T301" id="Seg_5651" s="T297">наверное гуся бы взял</ta>
            <ta e="T307" id="Seg_5652" s="T301">этого лебедя за гуся тому отдал</ta>
            <ta e="T311" id="Seg_5653" s="T307">потом с гусем идет идет</ta>
            <ta e="T321" id="Seg_5654" s="T311">один старичок говорят тот которого такой большую иголку палкой (посохом) сделал</ta>
            <ta e="T324" id="Seg_5655" s="T321">иголка да [так] иголка</ta>
            <ta e="T328" id="Seg_5656" s="T324">ум так повернулся</ta>
            <ta e="T332" id="Seg_5657" s="T328">я безглазый черт товарищ!</ta>
            <ta e="T343" id="Seg_5658" s="T332">что-нибудь шить [зашить] что-нибудь зашить наверное у этого старика иголку бы взял</ta>
            <ta e="T352" id="Seg_5659" s="T343">за эту за иголку тому отдал гуся отдал за иголку</ta>
            <ta e="T357" id="Seg_5660" s="T352">теперь с этой иголкой идет идет</ta>
            <ta e="T359" id="Seg_5661" s="T357">потерялась</ta>
            <ta e="T366" id="Seg_5662" s="T359">в коридор домой вошел иголка потерялась</ta>
            <ta e="T372" id="Seg_5663" s="T366">в коридоре [в сенях] ищет ощупью</ta>
            <ta e="T376" id="Seg_5664" s="T372">ты что ищешь старик</ta>
            <ta e="T379" id="Seg_5665" s="T376">с того чума (дома) [с тех дверей] человек </ta>
            <ta e="T387" id="Seg_5666" s="T379">я иголку было принес, эту иголку потерял</ta>
            <ta e="T390" id="Seg_5667" s="T387">иголку ищу </ta>
            <ta e="T395" id="Seg_5668" s="T391">домой заходи покушай </ta>
            <ta e="T398" id="Seg_5669" s="T395">на этом кончил</ta>
            <ta e="T402" id="Seg_5670" s="T398">старик</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T11" id="Seg_5671" s="T4">‎‎[OSV]: Unclear meaning of the collocation "ütɨp ammɛːjit". </ta>
            <ta e="T78" id="Seg_5672" s="T70">[OSV]: The author alternates between Present and Past tenses.</ta>
            <ta e="T126" id="Seg_5673" s="T116">[OSV]: "kurmonɨ" has been edited into "kurmon".</ta>
            <ta e="T138" id="Seg_5674" s="T132"> ‎‎[OSV]: "kurmonɨ" has been edited into "kurmon".</ta>
            <ta e="T204" id="Seg_5675" s="T199">[OSV]: "čʼud̂nɨmtɨ" has been edited into "čʼuntɨmtɨ".</ta>
            <ta e="T398" id="Seg_5676" s="T395">[OSV]: not sure in verbal glosses.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
