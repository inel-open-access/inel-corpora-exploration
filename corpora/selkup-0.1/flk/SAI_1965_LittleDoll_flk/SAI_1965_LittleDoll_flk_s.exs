<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID7DAE5CBB-5628-A01F-181F-CF8BEA96B614">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">flk\SAI_1965_LittleDoll_flk\SAI_1965_LittleDoll_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">439</ud-information>
            <ud-information attribute-name="# HIAT:w">317</ud-information>
            <ud-information attribute-name="# e">317</ud-information>
            <ud-information attribute-name="# HIAT:u">69</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SAI">
            <abbreviation>SAI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SAI"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T317" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Qošikɔːlʼa</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ilʼempa</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">ukkur</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">qup</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">čʼilʼalʼ</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">qup</ts>
                  <nts id="Seg_21" n="HIAT:ip">.</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_24" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">Ni</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">porqɨtɨ</ts>
                  <nts id="Seg_30" n="HIAT:ip">,</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">ni</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">peːmɨtɨ</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">čʼäːŋkasa</ts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_43" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">Nʼaŋɨčʼa</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">ilɨmpa</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_52" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_54" n="HIAT:w" s="T13">Tɛnatɨmpa</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_56" n="HIAT:ip">(</nts>
                  <nts id="Seg_57" n="HIAT:ip">/</nts>
                  <ts e="T15" id="Seg_59" n="HIAT:w" s="T14">tɛnɨrpa</ts>
                  <nts id="Seg_60" n="HIAT:ip">)</nts>
                  <nts id="Seg_61" n="HIAT:ip">:</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_64" n="HIAT:w" s="T15">Kuttar</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_67" n="HIAT:w" s="T16">nɨlʼčʼak</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">ilantak</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_73" n="HIAT:w" s="T18">pɛlʼikɔːlɨk</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_77" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_79" n="HIAT:w" s="T19">Qəssak</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_82" n="HIAT:w" s="T20">ana</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_84" n="HIAT:ip">(</nts>
                  <nts id="Seg_85" n="HIAT:ip">/</nts>
                  <ts e="T22" id="Seg_87" n="HIAT:w" s="T21">qəssaŋ</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_90" n="HIAT:w" s="T22">ɛna</ts>
                  <nts id="Seg_91" n="HIAT:ip">)</nts>
                  <nts id="Seg_92" n="HIAT:ip">,</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_95" n="HIAT:w" s="T23">kun</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_98" n="HIAT:w" s="T24">ɛːma</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_100" n="HIAT:ip">(</nts>
                  <nts id="Seg_101" n="HIAT:ip">/</nts>
                  <ts e="T26" id="Seg_103" n="HIAT:w" s="T25">kučʼät</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_106" n="HIAT:w" s="T26">ɛːmma</ts>
                  <nts id="Seg_107" n="HIAT:ip">)</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_110" n="HIAT:w" s="T27">qumɨk</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_113" n="HIAT:w" s="T28">qontak</ts>
                  <nts id="Seg_114" n="HIAT:ip">,</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_117" n="HIAT:w" s="T29">qosaŋ</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_120" n="HIAT:w" s="T30">ɛna</ts>
                  <nts id="Seg_121" n="HIAT:ip">.</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_124" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_126" n="HIAT:w" s="T31">Pona</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_129" n="HIAT:w" s="T32">tanta</ts>
                  <nts id="Seg_130" n="HIAT:ip">,</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_133" n="HIAT:w" s="T33">qənna</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_136" n="HIAT:w" s="T34">kučʼät</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_139" n="HIAT:w" s="T35">tɛnɨtä</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_142" n="HIAT:w" s="T36">tuːtɨrɨmpa</ts>
                  <nts id="Seg_143" n="HIAT:ip">.</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_146" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_148" n="HIAT:w" s="T37">Ukkɨr</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_151" n="HIAT:w" s="T38">čʼontoːqɨn</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_154" n="HIAT:w" s="T39">mannɨmpat</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_157" n="HIAT:w" s="T40">kɨ</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_160" n="HIAT:w" s="T41">qont</ts>
                  <nts id="Seg_161" n="HIAT:ip">,</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">qolčʼe</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_167" n="HIAT:w" s="T43">ata</ts>
                  <nts id="Seg_168" n="HIAT:ip">.</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_171" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_173" n="HIAT:w" s="T44">Näčʼä</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_176" n="HIAT:w" s="T45">panɨnʼnʼe</ts>
                  <nts id="Seg_177" n="HIAT:ip">,</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_180" n="HIAT:w" s="T46">karrä</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_183" n="HIAT:w" s="T47">čʼa</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_186" n="HIAT:w" s="T48">panɨnʼnʼe</ts>
                  <nts id="Seg_187" n="HIAT:ip">,</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_190" n="HIAT:w" s="T49">montə</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_193" n="HIAT:w" s="T50">qoltə</ts>
                  <nts id="Seg_194" n="HIAT:ip">.</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_197" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_199" n="HIAT:w" s="T51">Takkɨ</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_202" n="HIAT:w" s="T52">mannɨmpa</ts>
                  <nts id="Seg_203" n="HIAT:ip">,</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_206" n="HIAT:w" s="T53">čʼumpɨ</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_209" n="HIAT:w" s="T54">qoltit</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_212" n="HIAT:w" s="T55">kəːlʼ</ts>
                  <nts id="Seg_213" n="HIAT:ip">,</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_216" n="HIAT:w" s="T56">ontə</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_219" n="HIAT:w" s="T57">kınčʼe</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_222" n="HIAT:w" s="T58">atentə</ts>
                  <nts id="Seg_223" n="HIAT:ip">.</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_226" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_228" n="HIAT:w" s="T59">Nɨː</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_231" n="HIAT:w" s="T60">čʼap</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_234" n="HIAT:w" s="T61">tüŋak</ts>
                  <nts id="Seg_235" n="HIAT:ip">,</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_238" n="HIAT:w" s="T62">muqultirilʼ</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_241" n="HIAT:w" s="T63">poːlʼ</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_244" n="HIAT:w" s="T64">maːtɨrtɨ</ts>
                  <nts id="Seg_245" n="HIAT:ip">.</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_248" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_250" n="HIAT:w" s="T65">Üŋɨt</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_253" n="HIAT:w" s="T66">ɔːlak</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_256" n="HIAT:w" s="T67">oračʼareŋɔːtɨt</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_260" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_262" n="HIAT:w" s="T68">Mottɨ</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_265" n="HIAT:w" s="T69">qoŋak</ts>
                  <nts id="Seg_266" n="HIAT:ip">,</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_269" n="HIAT:w" s="T70">muntɨ</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_272" n="HIAT:w" s="T71">šitɨ</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_275" n="HIAT:w" s="T72">topalʼ</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_278" n="HIAT:w" s="T73">mɨrq</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_281" n="HIAT:w" s="T74">ɛːŋa</ts>
                  <nts id="Seg_282" n="HIAT:ip">.</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_285" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_287" n="HIAT:w" s="T75">Mottetə</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_290" n="HIAT:w" s="T76">nɨntə</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_293" n="HIAT:w" s="T77">mortät</ts>
                  <nts id="Seg_294" n="HIAT:ip">,</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_296" n="HIAT:ip">(</nts>
                  <ts e="T79" id="Seg_298" n="HIAT:w" s="T78">qəːla</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_301" n="HIAT:w" s="T79">uːltimpɔːtɨt</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_304" n="HIAT:w" s="T80">mortaːqɨt</ts>
                  <nts id="Seg_305" n="HIAT:ip">)</nts>
                  <nts id="Seg_306" n="HIAT:ip">,</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_309" n="HIAT:w" s="T81">kınčʼin</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_312" n="HIAT:w" s="T82">mortät</ts>
                  <nts id="Seg_313" n="HIAT:ip">.</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_316" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_318" n="HIAT:w" s="T83">İllä</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_321" n="HIAT:w" s="T84">mannɨmpa</ts>
                  <nts id="Seg_322" n="HIAT:ip">,</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_325" n="HIAT:w" s="T85">opčʼin</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_328" n="HIAT:w" s="T86">morqə</ts>
                  <nts id="Seg_329" n="HIAT:ip">,</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_332" n="HIAT:w" s="T87">wärqə</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_335" n="HIAT:w" s="T88">opčʼin</ts>
                  <nts id="Seg_336" n="HIAT:ip">.</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_339" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_341" n="HIAT:w" s="T89">Takkɨ</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_344" n="HIAT:w" s="T90">mannimpak</ts>
                  <nts id="Seg_345" n="HIAT:ip">,</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_348" n="HIAT:w" s="T91">qaj</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_351" n="HIAT:w" s="T92">loːsɨ</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_354" n="HIAT:w" s="T93">tüntɨ</ts>
                  <nts id="Seg_355" n="HIAT:ip">.</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_358" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_360" n="HIAT:w" s="T94">Nɨrkɨmɔːnna</ts>
                  <nts id="Seg_361" n="HIAT:ip">.</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_364" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_366" n="HIAT:w" s="T95">Ukoːt</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_369" n="HIAT:w" s="T96">tüsak</ts>
                  <nts id="Seg_370" n="HIAT:ip">,</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_373" n="HIAT:w" s="T97">märkəlak</ts>
                  <nts id="Seg_374" n="HIAT:ip">.</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_377" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_379" n="HIAT:w" s="T98">Čʼɔːlsä</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_382" n="HIAT:w" s="T99">šıp</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_385" n="HIAT:w" s="T100">aplä</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_388" n="HIAT:w" s="T101">märkak</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_391" n="HIAT:w" s="T102">nɨmtä</ts>
                  <nts id="Seg_392" n="HIAT:ip">.</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_395" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_397" n="HIAT:w" s="T103">Na</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_400" n="HIAT:w" s="T104">tüntə</ts>
                  <nts id="Seg_401" n="HIAT:ip">.</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_404" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_406" n="HIAT:w" s="T105">Nɨlʼčʼik</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_409" n="HIAT:w" s="T106">šıp</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_412" n="HIAT:w" s="T107">qoŋa</ts>
                  <nts id="Seg_413" n="HIAT:ip">.</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_416" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_418" n="HIAT:w" s="T108">Pɨpaja</ts>
                  <nts id="Seg_419" n="HIAT:ip">.</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_422" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_424" n="HIAT:w" s="T109">Nälʼamɨ</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_427" n="HIAT:w" s="T110">sɔːntɨrtɛnta</ts>
                  <nts id="Seg_428" n="HIAT:ip">.</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_431" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_433" n="HIAT:w" s="T111">Qɔːtä</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_436" n="HIAT:w" s="T112">qɨntɨsam</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_439" n="HIAT:w" s="T113">ɛna</ts>
                  <nts id="Seg_440" n="HIAT:ip">.</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_443" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_445" n="HIAT:w" s="T114">Nupoːquntä</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_448" n="HIAT:w" s="T115">omtətäkkɨtä</ts>
                  <nts id="Seg_449" n="HIAT:ip">.</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_452" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_454" n="HIAT:w" s="T116">Moqonä</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_457" n="HIAT:w" s="T117">qɨntokkɨt</ts>
                  <nts id="Seg_458" n="HIAT:ip">.</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_461" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_463" n="HIAT:w" s="T118">A</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_466" n="HIAT:w" s="T119">qup</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_469" n="HIAT:w" s="T120">nopoːqɨt</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_472" n="HIAT:w" s="T121">ɔːmta</ts>
                  <nts id="Seg_473" n="HIAT:ip">.</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_476" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_478" n="HIAT:w" s="T122">Mannɨmpatɨ</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_481" n="HIAT:w" s="T123">konnä</ts>
                  <nts id="Seg_482" n="HIAT:ip">,</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_485" n="HIAT:w" s="T124">warqə</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_488" n="HIAT:w" s="T125">mɔːt</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_491" n="HIAT:w" s="T126">ɔːmnantɨ</ts>
                  <nts id="Seg_492" n="HIAT:ip">.</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_495" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_497" n="HIAT:w" s="T127">Tüŋa</ts>
                  <nts id="Seg_498" n="HIAT:ip">.</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_501" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_503" n="HIAT:w" s="T128">Nɨː</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_506" n="HIAT:w" s="T129">mɔːttɨ</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_509" n="HIAT:w" s="T130">tuːltiŋɨtɨ</ts>
                  <nts id="Seg_510" n="HIAT:ip">.</nts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_513" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_515" n="HIAT:w" s="T131">Näja</ts>
                  <nts id="Seg_516" n="HIAT:ip">,</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_519" n="HIAT:w" s="T132">qoŋak</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_522" n="HIAT:w" s="T133">pɨpajap</ts>
                  <nts id="Seg_523" n="HIAT:ip">,</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_526" n="HIAT:w" s="T134">sɔːntaraš</ts>
                  <nts id="Seg_527" n="HIAT:ip">.</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_530" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_532" n="HIAT:w" s="T135">Ilʼempɔːtɨt</ts>
                  <nts id="Seg_533" n="HIAT:ip">.</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_536" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_538" n="HIAT:w" s="T136">Imaqotatə</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_541" n="HIAT:w" s="T137">nɨlʼčʼik</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_544" n="HIAT:w" s="T138">kətɨŋɨtɨ</ts>
                  <nts id="Seg_545" n="HIAT:ip">:</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_548" n="HIAT:w" s="T139">Irra</ts>
                  <nts id="Seg_549" n="HIAT:ip">,</nts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_552" n="HIAT:w" s="T140">ɨntə</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_555" n="HIAT:w" s="T141">meːŋašip</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_558" n="HIAT:w" s="T142">ukoːt</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_561" n="HIAT:w" s="T143">ontə</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_564" n="HIAT:w" s="T144">ilepilʼ</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_567" n="HIAT:w" s="T145">qup</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_570" n="HIAT:w" s="T146">suːrə</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_573" n="HIAT:w" s="T147">močʼe</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_576" n="HIAT:w" s="T148">amqo</ts>
                  <nts id="Seg_577" n="HIAT:ip">.</nts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_580" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_582" n="HIAT:w" s="T149">Irra</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_585" n="HIAT:w" s="T150">ɨntəp</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_588" n="HIAT:w" s="T151">meːŋatə</ts>
                  <nts id="Seg_589" n="HIAT:ip">.</nts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_592" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_594" n="HIAT:w" s="T152">Kəssa</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_597" n="HIAT:w" s="T153">qəllɔːqən</ts>
                  <nts id="Seg_598" n="HIAT:ip">.</nts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_601" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_603" n="HIAT:w" s="T154">Tolʼčʼep</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_606" n="HIAT:w" s="T155">meːŋatə</ts>
                  <nts id="Seg_607" n="HIAT:ip">.</nts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_610" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_612" n="HIAT:w" s="T156">Na</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_615" n="HIAT:w" s="T157">qup</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_618" n="HIAT:w" s="T158">tolʼčʼisa</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_621" n="HIAT:w" s="T159">qənnä</ts>
                  <nts id="Seg_622" n="HIAT:ip">.</nts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_625" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_627" n="HIAT:w" s="T160">Ɨntatä</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_630" n="HIAT:w" s="T161">iːŋatə</ts>
                  <nts id="Seg_631" n="HIAT:ip">.</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_634" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_636" n="HIAT:w" s="T162">Ont</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_639" n="HIAT:w" s="T163">irra</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_642" n="HIAT:w" s="T164">olqa</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_645" n="HIAT:w" s="T165">qənna</ts>
                  <nts id="Seg_646" n="HIAT:ip">,</nts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_649" n="HIAT:w" s="T166">tolʼčʼikɔːlik</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_652" n="HIAT:w" s="T167">üːta</ts>
                  <nts id="Seg_653" n="HIAT:ip">.</nts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_656" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_658" n="HIAT:w" s="T168">Ukkur</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_661" n="HIAT:w" s="T169">čʼontoːqɨt</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_664" n="HIAT:w" s="T170">qoŋatɨ</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_667" n="HIAT:w" s="T171">ɔːtan</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_670" n="HIAT:w" s="T172">mətɨp</ts>
                  <nts id="Seg_671" n="HIAT:ip">.</nts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_674" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_676" n="HIAT:w" s="T173">Nʼoːtɨŋɨtɨ</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_678" n="HIAT:ip">(</nts>
                  <nts id="Seg_679" n="HIAT:ip">/</nts>
                  <ts e="T175" id="Seg_681" n="HIAT:w" s="T174">nʼuːtɨŋɨtɨ</ts>
                  <nts id="Seg_682" n="HIAT:ip">)</nts>
                  <nts id="Seg_683" n="HIAT:ip">.</nts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_686" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_688" n="HIAT:w" s="T175">Pɨpaja</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_691" n="HIAT:w" s="T176">nʼenna</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_694" n="HIAT:w" s="T177">nešolʼnɨ</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_697" n="HIAT:w" s="T178">kətelʼlʼa</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_700" n="HIAT:w" s="T179">ɔːtap</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_703" n="HIAT:w" s="T180">saisä</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_706" n="HIAT:w" s="T181">qoŋatə</ts>
                  <nts id="Seg_707" n="HIAT:ip">.</nts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_710" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_712" n="HIAT:w" s="T182">Ɨntɨsä</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_715" n="HIAT:w" s="T183">čʼatɨŋɨtɨ</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_718" n="HIAT:w" s="T184">ukkor</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_721" n="HIAT:w" s="T185">qoːrrɨ</ts>
                  <nts id="Seg_722" n="HIAT:ip">.</nts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_725" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_727" n="HIAT:w" s="T186">İllä</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_730" n="HIAT:w" s="T187">omta</ts>
                  <nts id="Seg_731" n="HIAT:ip">.</nts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_734" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_736" n="HIAT:w" s="T188">Moqon</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_739" n="HIAT:w" s="T189">nɨːnä</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_742" n="HIAT:w" s="T190">irra</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_745" n="HIAT:w" s="T191">tüŋa</ts>
                  <nts id="Seg_746" n="HIAT:ip">.</nts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_749" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_751" n="HIAT:w" s="T192">Mannɨmpatɨ</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_754" n="HIAT:w" s="T193">qoːrɨ</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_757" n="HIAT:w" s="T194">ippɨntɨ</ts>
                  <nts id="Seg_758" n="HIAT:ip">,</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_761" n="HIAT:w" s="T195">pɨpaja</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_764" n="HIAT:w" s="T196">ɔːmta</ts>
                  <nts id="Seg_765" n="HIAT:ip">,</nts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_768" n="HIAT:w" s="T197">nuːnɨčʼa</ts>
                  <nts id="Seg_769" n="HIAT:ip">.</nts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_772" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_774" n="HIAT:w" s="T198">Irra</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_777" n="HIAT:w" s="T199">nɨlʼčʼik</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_780" n="HIAT:w" s="T200">kətoŋɨtɨ</ts>
                  <nts id="Seg_781" n="HIAT:ip">:</nts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_784" n="HIAT:w" s="T201">Opsä</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_787" n="HIAT:w" s="T202">qoŋap</ts>
                  <nts id="Seg_788" n="HIAT:ip">,</nts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_791" n="HIAT:w" s="T203">moqonä</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_794" n="HIAT:w" s="T204">qɨntak</ts>
                  <nts id="Seg_795" n="HIAT:ip">.</nts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_798" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_800" n="HIAT:w" s="T205">Toː</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_803" n="HIAT:w" s="T206">qənnä</ts>
                  <nts id="Seg_804" n="HIAT:ip">.</nts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_807" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_809" n="HIAT:w" s="T207">Utäsä</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_812" n="HIAT:w" s="T208">tüma</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_815" n="HIAT:w" s="T209">sopɨrnɨtɨ</ts>
                  <nts id="Seg_816" n="HIAT:ip">.</nts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_819" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_821" n="HIAT:w" s="T210">Qoːrɨmtɨ</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_824" n="HIAT:w" s="T211">puːntɨsä</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_827" n="HIAT:w" s="T212">čʼontoːqɨntä</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_830" n="HIAT:w" s="T213">parqolʼlʼɛːte</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_833" n="HIAT:w" s="T214">kəːqalʼlʼä</ts>
                  <nts id="Seg_834" n="HIAT:ip">.</nts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_837" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_839" n="HIAT:w" s="T215">Moqonä</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_842" n="HIAT:w" s="T216">kusa</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_845" n="HIAT:w" s="T217">qəllaj</ts>
                  <nts id="Seg_846" n="HIAT:ip">.</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_849" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_851" n="HIAT:w" s="T218">Moqona</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_854" n="HIAT:w" s="T219">tüːŋa</ts>
                  <nts id="Seg_855" n="HIAT:ip">.</nts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_858" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_860" n="HIAT:w" s="T220">Imaqota</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_863" n="HIAT:w" s="T221">nɨlʼčʼik</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_866" n="HIAT:w" s="T222">kətɨŋɨtɨ</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_869" n="HIAT:w" s="T223">Opsä</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_872" n="HIAT:w" s="T224">irra</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_875" n="HIAT:w" s="T225">taːtolʼä</ts>
                  <nts id="Seg_876" n="HIAT:ip">:</nts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_879" n="HIAT:w" s="T226">Soma</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_882" n="HIAT:w" s="T227">pɨpaja</ts>
                  <nts id="Seg_883" n="HIAT:ip">.</nts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_886" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_888" n="HIAT:w" s="T228">Ilʼempɔːtɨt</ts>
                  <nts id="Seg_889" n="HIAT:ip">,</nts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_892" n="HIAT:w" s="T229">nälʼantɨn</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_895" n="HIAT:w" s="T230">moqa</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_898" n="HIAT:w" s="T231">sɔːntɨrlʼä</ts>
                  <nts id="Seg_899" n="HIAT:ip">.</nts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_902" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_904" n="HIAT:w" s="T232">Ukkɨr</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_907" n="HIAT:w" s="T233">ija</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_910" n="HIAT:w" s="T234">qoŋatɨ</ts>
                  <nts id="Seg_911" n="HIAT:ip">,</nts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_914" n="HIAT:w" s="T235">ijatɨ</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_917" n="HIAT:w" s="T236">orompa</ts>
                  <nts id="Seg_918" n="HIAT:ip">.</nts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_921" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_923" n="HIAT:w" s="T237">Ukkɨr</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_926" n="HIAT:w" s="T238">čʼontoːqɨt</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_929" n="HIAT:w" s="T239">irra</ts>
                  <nts id="Seg_930" n="HIAT:ip">:</nts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_933" n="HIAT:w" s="T240">Näjannɨ</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_936" n="HIAT:w" s="T241">mɨqı</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_939" n="HIAT:w" s="T242">üːtälʼäj</ts>
                  <nts id="Seg_940" n="HIAT:ip">.</nts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_943" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_945" n="HIAT:w" s="T243">Moqɨnäqat</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_948" n="HIAT:w" s="T244">pɨpaja</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_951" n="HIAT:w" s="T245">qumiːtə</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_954" n="HIAT:w" s="T246">ətältɔːtät</ts>
                  <nts id="Seg_955" n="HIAT:ip">.</nts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_958" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_960" n="HIAT:w" s="T247">Nälʼamtɨ</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_963" n="HIAT:w" s="T248">miŋɔːtäː</ts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_966" n="HIAT:w" s="T249">irra</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_969" n="HIAT:w" s="T250">imaqotaj</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_972" n="HIAT:w" s="T251">mɨqaːqə</ts>
                  <nts id="Seg_973" n="HIAT:ip">.</nts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_976" n="HIAT:u" s="T252">
                  <ts e="T253" id="Seg_978" n="HIAT:w" s="T252">Nälʼantɨlʼ</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_981" n="HIAT:w" s="T253">qənnɔːqə</ts>
                  <nts id="Seg_982" n="HIAT:ip">.</nts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_985" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_987" n="HIAT:w" s="T254">Moqonä</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_990" n="HIAT:w" s="T255">tɨmtä</ts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_993" n="HIAT:w" s="T256">iloqo</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_996" n="HIAT:w" s="T257">qošik</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_999" n="HIAT:w" s="T258">ɛːja</ts>
                  <nts id="Seg_1000" n="HIAT:ip">.</nts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T271" id="Seg_1003" n="HIAT:u" s="T259">
                  <ts e="T260" id="Seg_1005" n="HIAT:w" s="T259">Na</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1008" n="HIAT:w" s="T260">pünakesa</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1011" n="HIAT:w" s="T261">irra</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1014" n="HIAT:w" s="T262">mɨqaːqə</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1017" n="HIAT:w" s="T263">nälʼantɨ</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1020" n="HIAT:w" s="T264">mila</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1023" n="HIAT:w" s="T265">üːtɨŋɨtɨ</ts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1026" n="HIAT:w" s="T266">ijantɨsä</ts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1029" n="HIAT:w" s="T267">šölʼqumɨlʼ</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1032" n="HIAT:w" s="T268">tətontə</ts>
                  <nts id="Seg_1033" n="HIAT:ip">,</nts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1036" n="HIAT:w" s="T269">Tɔːs</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1039" n="HIAT:w" s="T270">qəltontə</ts>
                  <nts id="Seg_1040" n="HIAT:ip">.</nts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_1043" n="HIAT:u" s="T271">
                  <ts e="T272" id="Seg_1045" n="HIAT:w" s="T271">Pünakəsalʼ</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1048" n="HIAT:w" s="T272">nätäk</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1051" n="HIAT:w" s="T273">čʼap</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1054" n="HIAT:w" s="T274">tüːŋa</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1057" n="HIAT:w" s="T275">irantə</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1060" n="HIAT:w" s="T276">tətontə</ts>
                  <nts id="Seg_1061" n="HIAT:ip">.</nts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_1064" n="HIAT:u" s="T277">
                  <ts e="T278" id="Seg_1066" n="HIAT:w" s="T277">Ileqo</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1069" n="HIAT:w" s="T278">qošik</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1072" n="HIAT:w" s="T279">ɛːja</ts>
                  <nts id="Seg_1073" n="HIAT:ip">,</nts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1076" n="HIAT:w" s="T280">säːqasɨk</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1079" n="HIAT:w" s="T281">ɛːŋa</ts>
                  <nts id="Seg_1080" n="HIAT:ip">.</nts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T291" id="Seg_1083" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_1085" n="HIAT:w" s="T282">Ijamtə</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1088" n="HIAT:w" s="T283">äsɨntɨsa</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1091" n="HIAT:w" s="T284">qəːučʼiŋɨtɨ</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1094" n="HIAT:w" s="T285">Tɔːs</ts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1097" n="HIAT:w" s="T286">qoltontə</ts>
                  <nts id="Seg_1098" n="HIAT:ip">,</nts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1101" n="HIAT:w" s="T287">irrantɨ</ts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1104" n="HIAT:w" s="T288">tətontɨ</ts>
                  <nts id="Seg_1105" n="HIAT:ip">,</nts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1108" n="HIAT:w" s="T289">šolʼqumɨn</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1111" n="HIAT:w" s="T290">tətontɨ</ts>
                  <nts id="Seg_1112" n="HIAT:ip">.</nts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T295" id="Seg_1115" n="HIAT:u" s="T291">
                  <ts e="T292" id="Seg_1117" n="HIAT:w" s="T291">A</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1120" n="HIAT:w" s="T292">mat</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1123" n="HIAT:w" s="T293">moqona</ts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1126" n="HIAT:w" s="T294">qəntak</ts>
                  <nts id="Seg_1127" n="HIAT:ip">.</nts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1130" n="HIAT:u" s="T295">
                  <ts e="T296" id="Seg_1132" n="HIAT:w" s="T295">Pünakɨsaj</ts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1135" n="HIAT:w" s="T296">təttaqaːq</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1138" n="HIAT:w" s="T297">iramtɨ</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1141" n="HIAT:w" s="T298">ijantɨsä</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1144" n="HIAT:w" s="T299">qoːčʼiɛːŋɨtɨ</ts>
                  <nts id="Seg_1145" n="HIAT:ip">.</nts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T302" id="Seg_1148" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1150" n="HIAT:w" s="T300">Somak</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1153" n="HIAT:w" s="T301">ilʼeŋɨlʼıː</ts>
                  <nts id="Seg_1154" n="HIAT:ip">.</nts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_1157" n="HIAT:u" s="T302">
                  <ts e="T303" id="Seg_1159" n="HIAT:w" s="T302">Šolʼqup</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1162" n="HIAT:w" s="T303">kɨpa</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1165" n="HIAT:w" s="T304">käːtɨiɛːŋɔːtät</ts>
                  <nts id="Seg_1166" n="HIAT:ip">.</nts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T313" id="Seg_1169" n="HIAT:u" s="T305">
                  <ts e="T306" id="Seg_1171" n="HIAT:w" s="T305">Pünakɨsalʼ</ts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1174" n="HIAT:w" s="T306">nɨlʼčʼik</ts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1177" n="HIAT:w" s="T307">kätɨmpatɨt</ts>
                  <nts id="Seg_1178" n="HIAT:ip">:</nts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1181" n="HIAT:w" s="T308">Šölʼqupsa</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1184" n="HIAT:w" s="T309">ukuršak</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1187" n="HIAT:w" s="T310">kəːtɨpɨlʼ</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1190" n="HIAT:w" s="T311">iːjamɨ</ts>
                  <nts id="Seg_1191" n="HIAT:ip">,</nts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1194" n="HIAT:w" s="T312">ilʼiŋɨijäːtot</ts>
                  <nts id="Seg_1195" n="HIAT:ip">.</nts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_1198" n="HIAT:u" s="T313">
                  <ts e="T314" id="Seg_1200" n="HIAT:w" s="T313">Worqɨ</ts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1203" n="HIAT:w" s="T314">käpɨk</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1206" n="HIAT:w" s="T315">ɛːŋa</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1209" n="HIAT:w" s="T316">ijetat</ts>
                  <nts id="Seg_1210" n="HIAT:ip">.</nts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T317" id="Seg_1212" n="sc" s="T0">
               <ts e="T1" id="Seg_1214" n="e" s="T0">Qošikɔːlʼa </ts>
               <ts e="T2" id="Seg_1216" n="e" s="T1">ilʼempa </ts>
               <ts e="T3" id="Seg_1218" n="e" s="T2">ukkur </ts>
               <ts e="T4" id="Seg_1220" n="e" s="T3">qup, </ts>
               <ts e="T5" id="Seg_1222" n="e" s="T4">čʼilʼalʼ </ts>
               <ts e="T6" id="Seg_1224" n="e" s="T5">qup. </ts>
               <ts e="T7" id="Seg_1226" n="e" s="T6">Ni </ts>
               <ts e="T8" id="Seg_1228" n="e" s="T7">porqɨtɨ, </ts>
               <ts e="T9" id="Seg_1230" n="e" s="T8">ni </ts>
               <ts e="T10" id="Seg_1232" n="e" s="T9">peːmɨtɨ </ts>
               <ts e="T11" id="Seg_1234" n="e" s="T10">čʼäːŋkasa. </ts>
               <ts e="T12" id="Seg_1236" n="e" s="T11">Nʼaŋɨčʼa </ts>
               <ts e="T13" id="Seg_1238" n="e" s="T12">ilɨmpa. </ts>
               <ts e="T14" id="Seg_1240" n="e" s="T13">Tɛnatɨmpa </ts>
               <ts e="T15" id="Seg_1242" n="e" s="T14">(/tɛnɨrpa): </ts>
               <ts e="T16" id="Seg_1244" n="e" s="T15">Kuttar </ts>
               <ts e="T17" id="Seg_1246" n="e" s="T16">nɨlʼčʼak </ts>
               <ts e="T18" id="Seg_1248" n="e" s="T17">ilantak </ts>
               <ts e="T19" id="Seg_1250" n="e" s="T18">pɛlʼikɔːlɨk. </ts>
               <ts e="T20" id="Seg_1252" n="e" s="T19">Qəssak </ts>
               <ts e="T21" id="Seg_1254" n="e" s="T20">ana </ts>
               <ts e="T22" id="Seg_1256" n="e" s="T21">(/qəssaŋ </ts>
               <ts e="T23" id="Seg_1258" n="e" s="T22">ɛna), </ts>
               <ts e="T24" id="Seg_1260" n="e" s="T23">kun </ts>
               <ts e="T25" id="Seg_1262" n="e" s="T24">ɛːma </ts>
               <ts e="T26" id="Seg_1264" n="e" s="T25">(/kučʼät </ts>
               <ts e="T27" id="Seg_1266" n="e" s="T26">ɛːmma) </ts>
               <ts e="T28" id="Seg_1268" n="e" s="T27">qumɨk </ts>
               <ts e="T29" id="Seg_1270" n="e" s="T28">qontak, </ts>
               <ts e="T30" id="Seg_1272" n="e" s="T29">qosaŋ </ts>
               <ts e="T31" id="Seg_1274" n="e" s="T30">ɛna. </ts>
               <ts e="T32" id="Seg_1276" n="e" s="T31">Pona </ts>
               <ts e="T33" id="Seg_1278" n="e" s="T32">tanta, </ts>
               <ts e="T34" id="Seg_1280" n="e" s="T33">qənna </ts>
               <ts e="T35" id="Seg_1282" n="e" s="T34">kučʼät </ts>
               <ts e="T36" id="Seg_1284" n="e" s="T35">tɛnɨtä </ts>
               <ts e="T37" id="Seg_1286" n="e" s="T36">tuːtɨrɨmpa. </ts>
               <ts e="T38" id="Seg_1288" n="e" s="T37">Ukkɨr </ts>
               <ts e="T39" id="Seg_1290" n="e" s="T38">čʼontoːqɨn </ts>
               <ts e="T40" id="Seg_1292" n="e" s="T39">mannɨmpat </ts>
               <ts e="T41" id="Seg_1294" n="e" s="T40">kɨ </ts>
               <ts e="T42" id="Seg_1296" n="e" s="T41">qont, </ts>
               <ts e="T43" id="Seg_1298" n="e" s="T42">qolčʼe </ts>
               <ts e="T44" id="Seg_1300" n="e" s="T43">ata. </ts>
               <ts e="T45" id="Seg_1302" n="e" s="T44">Näčʼä </ts>
               <ts e="T46" id="Seg_1304" n="e" s="T45">panɨnʼnʼe, </ts>
               <ts e="T47" id="Seg_1306" n="e" s="T46">karrä </ts>
               <ts e="T48" id="Seg_1308" n="e" s="T47">čʼa </ts>
               <ts e="T49" id="Seg_1310" n="e" s="T48">panɨnʼnʼe, </ts>
               <ts e="T50" id="Seg_1312" n="e" s="T49">montə </ts>
               <ts e="T51" id="Seg_1314" n="e" s="T50">qoltə. </ts>
               <ts e="T52" id="Seg_1316" n="e" s="T51">Takkɨ </ts>
               <ts e="T53" id="Seg_1318" n="e" s="T52">mannɨmpa, </ts>
               <ts e="T54" id="Seg_1320" n="e" s="T53">čʼumpɨ </ts>
               <ts e="T55" id="Seg_1322" n="e" s="T54">qoltit </ts>
               <ts e="T56" id="Seg_1324" n="e" s="T55">kəːlʼ, </ts>
               <ts e="T57" id="Seg_1326" n="e" s="T56">ontə </ts>
               <ts e="T58" id="Seg_1328" n="e" s="T57">kınčʼe </ts>
               <ts e="T59" id="Seg_1330" n="e" s="T58">atentə. </ts>
               <ts e="T60" id="Seg_1332" n="e" s="T59">Nɨː </ts>
               <ts e="T61" id="Seg_1334" n="e" s="T60">čʼap </ts>
               <ts e="T62" id="Seg_1336" n="e" s="T61">tüŋak, </ts>
               <ts e="T63" id="Seg_1338" n="e" s="T62">muqultirilʼ </ts>
               <ts e="T64" id="Seg_1340" n="e" s="T63">poːlʼ </ts>
               <ts e="T65" id="Seg_1342" n="e" s="T64">maːtɨrtɨ. </ts>
               <ts e="T66" id="Seg_1344" n="e" s="T65">Üŋɨt </ts>
               <ts e="T67" id="Seg_1346" n="e" s="T66">ɔːlak </ts>
               <ts e="T68" id="Seg_1348" n="e" s="T67">oračʼareŋɔːtɨt. </ts>
               <ts e="T69" id="Seg_1350" n="e" s="T68">Mottɨ </ts>
               <ts e="T70" id="Seg_1352" n="e" s="T69">qoŋak, </ts>
               <ts e="T71" id="Seg_1354" n="e" s="T70">muntɨ </ts>
               <ts e="T72" id="Seg_1356" n="e" s="T71">šitɨ </ts>
               <ts e="T73" id="Seg_1358" n="e" s="T72">topalʼ </ts>
               <ts e="T74" id="Seg_1360" n="e" s="T73">mɨrq </ts>
               <ts e="T75" id="Seg_1362" n="e" s="T74">ɛːŋa. </ts>
               <ts e="T76" id="Seg_1364" n="e" s="T75">Mottetə </ts>
               <ts e="T77" id="Seg_1366" n="e" s="T76">nɨntə </ts>
               <ts e="T78" id="Seg_1368" n="e" s="T77">mortät, </ts>
               <ts e="T79" id="Seg_1370" n="e" s="T78">(qəːla </ts>
               <ts e="T80" id="Seg_1372" n="e" s="T79">uːltimpɔːtɨt </ts>
               <ts e="T81" id="Seg_1374" n="e" s="T80">mortaːqɨt), </ts>
               <ts e="T82" id="Seg_1376" n="e" s="T81">kınčʼin </ts>
               <ts e="T83" id="Seg_1378" n="e" s="T82">mortät. </ts>
               <ts e="T84" id="Seg_1380" n="e" s="T83">İllä </ts>
               <ts e="T85" id="Seg_1382" n="e" s="T84">mannɨmpa, </ts>
               <ts e="T86" id="Seg_1384" n="e" s="T85">opčʼin </ts>
               <ts e="T87" id="Seg_1386" n="e" s="T86">morqə, </ts>
               <ts e="T88" id="Seg_1388" n="e" s="T87">wärqə </ts>
               <ts e="T89" id="Seg_1390" n="e" s="T88">opčʼin. </ts>
               <ts e="T90" id="Seg_1392" n="e" s="T89">Takkɨ </ts>
               <ts e="T91" id="Seg_1394" n="e" s="T90">mannimpak, </ts>
               <ts e="T92" id="Seg_1396" n="e" s="T91">qaj </ts>
               <ts e="T93" id="Seg_1398" n="e" s="T92">loːsɨ </ts>
               <ts e="T94" id="Seg_1400" n="e" s="T93">tüntɨ. </ts>
               <ts e="T95" id="Seg_1402" n="e" s="T94">Nɨrkɨmɔːnna. </ts>
               <ts e="T96" id="Seg_1404" n="e" s="T95">Ukoːt </ts>
               <ts e="T97" id="Seg_1406" n="e" s="T96">tüsak, </ts>
               <ts e="T98" id="Seg_1408" n="e" s="T97">märkəlak. </ts>
               <ts e="T99" id="Seg_1410" n="e" s="T98">Čʼɔːlsä </ts>
               <ts e="T100" id="Seg_1412" n="e" s="T99">šıp </ts>
               <ts e="T101" id="Seg_1414" n="e" s="T100">aplä </ts>
               <ts e="T102" id="Seg_1416" n="e" s="T101">märkak </ts>
               <ts e="T103" id="Seg_1418" n="e" s="T102">nɨmtä. </ts>
               <ts e="T104" id="Seg_1420" n="e" s="T103">Na </ts>
               <ts e="T105" id="Seg_1422" n="e" s="T104">tüntə. </ts>
               <ts e="T106" id="Seg_1424" n="e" s="T105">Nɨlʼčʼik </ts>
               <ts e="T107" id="Seg_1426" n="e" s="T106">šıp </ts>
               <ts e="T108" id="Seg_1428" n="e" s="T107">qoŋa. </ts>
               <ts e="T109" id="Seg_1430" n="e" s="T108">Pɨpaja. </ts>
               <ts e="T110" id="Seg_1432" n="e" s="T109">Nälʼamɨ </ts>
               <ts e="T111" id="Seg_1434" n="e" s="T110">sɔːntɨrtɛnta. </ts>
               <ts e="T112" id="Seg_1436" n="e" s="T111">Qɔːtä </ts>
               <ts e="T113" id="Seg_1438" n="e" s="T112">qɨntɨsam </ts>
               <ts e="T114" id="Seg_1440" n="e" s="T113">ɛna. </ts>
               <ts e="T115" id="Seg_1442" n="e" s="T114">Nupoːquntä </ts>
               <ts e="T116" id="Seg_1444" n="e" s="T115">omtətäkkɨtä. </ts>
               <ts e="T117" id="Seg_1446" n="e" s="T116">Moqonä </ts>
               <ts e="T118" id="Seg_1448" n="e" s="T117">qɨntokkɨt. </ts>
               <ts e="T119" id="Seg_1450" n="e" s="T118">A </ts>
               <ts e="T120" id="Seg_1452" n="e" s="T119">qup </ts>
               <ts e="T121" id="Seg_1454" n="e" s="T120">nopoːqɨt </ts>
               <ts e="T122" id="Seg_1456" n="e" s="T121">ɔːmta. </ts>
               <ts e="T123" id="Seg_1458" n="e" s="T122">Mannɨmpatɨ </ts>
               <ts e="T124" id="Seg_1460" n="e" s="T123">konnä, </ts>
               <ts e="T125" id="Seg_1462" n="e" s="T124">warqə </ts>
               <ts e="T126" id="Seg_1464" n="e" s="T125">mɔːt </ts>
               <ts e="T127" id="Seg_1466" n="e" s="T126">ɔːmnantɨ. </ts>
               <ts e="T128" id="Seg_1468" n="e" s="T127">Tüŋa. </ts>
               <ts e="T129" id="Seg_1470" n="e" s="T128">Nɨː </ts>
               <ts e="T130" id="Seg_1472" n="e" s="T129">mɔːttɨ </ts>
               <ts e="T131" id="Seg_1474" n="e" s="T130">tuːltiŋɨtɨ. </ts>
               <ts e="T132" id="Seg_1476" n="e" s="T131">Näja, </ts>
               <ts e="T133" id="Seg_1478" n="e" s="T132">qoŋak </ts>
               <ts e="T134" id="Seg_1480" n="e" s="T133">pɨpajap, </ts>
               <ts e="T135" id="Seg_1482" n="e" s="T134">sɔːntaraš. </ts>
               <ts e="T136" id="Seg_1484" n="e" s="T135">Ilʼempɔːtɨt. </ts>
               <ts e="T137" id="Seg_1486" n="e" s="T136">Imaqotatə </ts>
               <ts e="T138" id="Seg_1488" n="e" s="T137">nɨlʼčʼik </ts>
               <ts e="T139" id="Seg_1490" n="e" s="T138">kətɨŋɨtɨ: </ts>
               <ts e="T140" id="Seg_1492" n="e" s="T139">Irra, </ts>
               <ts e="T141" id="Seg_1494" n="e" s="T140">ɨntə </ts>
               <ts e="T142" id="Seg_1496" n="e" s="T141">meːŋašip </ts>
               <ts e="T143" id="Seg_1498" n="e" s="T142">ukoːt </ts>
               <ts e="T144" id="Seg_1500" n="e" s="T143">ontə </ts>
               <ts e="T145" id="Seg_1502" n="e" s="T144">ilepilʼ </ts>
               <ts e="T146" id="Seg_1504" n="e" s="T145">qup </ts>
               <ts e="T147" id="Seg_1506" n="e" s="T146">suːrə </ts>
               <ts e="T148" id="Seg_1508" n="e" s="T147">močʼe </ts>
               <ts e="T149" id="Seg_1510" n="e" s="T148">amqo. </ts>
               <ts e="T150" id="Seg_1512" n="e" s="T149">Irra </ts>
               <ts e="T151" id="Seg_1514" n="e" s="T150">ɨntəp </ts>
               <ts e="T152" id="Seg_1516" n="e" s="T151">meːŋatə. </ts>
               <ts e="T153" id="Seg_1518" n="e" s="T152">Kəssa </ts>
               <ts e="T154" id="Seg_1520" n="e" s="T153">qəllɔːqən. </ts>
               <ts e="T155" id="Seg_1522" n="e" s="T154">Tolʼčʼep </ts>
               <ts e="T156" id="Seg_1524" n="e" s="T155">meːŋatə. </ts>
               <ts e="T157" id="Seg_1526" n="e" s="T156">Na </ts>
               <ts e="T158" id="Seg_1528" n="e" s="T157">qup </ts>
               <ts e="T159" id="Seg_1530" n="e" s="T158">tolʼčʼisa </ts>
               <ts e="T160" id="Seg_1532" n="e" s="T159">qənnä. </ts>
               <ts e="T161" id="Seg_1534" n="e" s="T160">Ɨntatä </ts>
               <ts e="T162" id="Seg_1536" n="e" s="T161">iːŋatə. </ts>
               <ts e="T163" id="Seg_1538" n="e" s="T162">Ont </ts>
               <ts e="T164" id="Seg_1540" n="e" s="T163">irra </ts>
               <ts e="T165" id="Seg_1542" n="e" s="T164">olqa </ts>
               <ts e="T166" id="Seg_1544" n="e" s="T165">qənna, </ts>
               <ts e="T167" id="Seg_1546" n="e" s="T166">tolʼčʼikɔːlik </ts>
               <ts e="T168" id="Seg_1548" n="e" s="T167">üːta. </ts>
               <ts e="T169" id="Seg_1550" n="e" s="T168">Ukkur </ts>
               <ts e="T170" id="Seg_1552" n="e" s="T169">čʼontoːqɨt </ts>
               <ts e="T171" id="Seg_1554" n="e" s="T170">qoŋatɨ </ts>
               <ts e="T172" id="Seg_1556" n="e" s="T171">ɔːtan </ts>
               <ts e="T173" id="Seg_1558" n="e" s="T172">mətɨp. </ts>
               <ts e="T174" id="Seg_1560" n="e" s="T173">Nʼoːtɨŋɨtɨ </ts>
               <ts e="T175" id="Seg_1562" n="e" s="T174">(/nʼuːtɨŋɨtɨ). </ts>
               <ts e="T176" id="Seg_1564" n="e" s="T175">Pɨpaja </ts>
               <ts e="T177" id="Seg_1566" n="e" s="T176">nʼenna </ts>
               <ts e="T178" id="Seg_1568" n="e" s="T177">nešolʼnɨ </ts>
               <ts e="T179" id="Seg_1570" n="e" s="T178">kətelʼlʼa </ts>
               <ts e="T180" id="Seg_1572" n="e" s="T179">ɔːtap </ts>
               <ts e="T181" id="Seg_1574" n="e" s="T180">saisä </ts>
               <ts e="T182" id="Seg_1576" n="e" s="T181">qoŋatə. </ts>
               <ts e="T183" id="Seg_1578" n="e" s="T182">Ɨntɨsä </ts>
               <ts e="T184" id="Seg_1580" n="e" s="T183">čʼatɨŋɨtɨ </ts>
               <ts e="T185" id="Seg_1582" n="e" s="T184">ukkor </ts>
               <ts e="T186" id="Seg_1584" n="e" s="T185">qoːrrɨ. </ts>
               <ts e="T187" id="Seg_1586" n="e" s="T186">İllä </ts>
               <ts e="T188" id="Seg_1588" n="e" s="T187">omta. </ts>
               <ts e="T189" id="Seg_1590" n="e" s="T188">Moqon </ts>
               <ts e="T190" id="Seg_1592" n="e" s="T189">nɨːnä </ts>
               <ts e="T191" id="Seg_1594" n="e" s="T190">irra </ts>
               <ts e="T192" id="Seg_1596" n="e" s="T191">tüŋa. </ts>
               <ts e="T193" id="Seg_1598" n="e" s="T192">Mannɨmpatɨ </ts>
               <ts e="T194" id="Seg_1600" n="e" s="T193">qoːrɨ </ts>
               <ts e="T195" id="Seg_1602" n="e" s="T194">ippɨntɨ, </ts>
               <ts e="T196" id="Seg_1604" n="e" s="T195">pɨpaja </ts>
               <ts e="T197" id="Seg_1606" n="e" s="T196">ɔːmta, </ts>
               <ts e="T198" id="Seg_1608" n="e" s="T197">nuːnɨčʼa. </ts>
               <ts e="T199" id="Seg_1610" n="e" s="T198">Irra </ts>
               <ts e="T200" id="Seg_1612" n="e" s="T199">nɨlʼčʼik </ts>
               <ts e="T201" id="Seg_1614" n="e" s="T200">kətoŋɨtɨ: </ts>
               <ts e="T202" id="Seg_1616" n="e" s="T201">Opsä </ts>
               <ts e="T203" id="Seg_1618" n="e" s="T202">qoŋap, </ts>
               <ts e="T204" id="Seg_1620" n="e" s="T203">moqonä </ts>
               <ts e="T205" id="Seg_1622" n="e" s="T204">qɨntak. </ts>
               <ts e="T206" id="Seg_1624" n="e" s="T205">Toː </ts>
               <ts e="T207" id="Seg_1626" n="e" s="T206">qənnä. </ts>
               <ts e="T208" id="Seg_1628" n="e" s="T207">Utäsä </ts>
               <ts e="T209" id="Seg_1630" n="e" s="T208">tüma </ts>
               <ts e="T210" id="Seg_1632" n="e" s="T209">sopɨrnɨtɨ. </ts>
               <ts e="T211" id="Seg_1634" n="e" s="T210">Qoːrɨmtɨ </ts>
               <ts e="T212" id="Seg_1636" n="e" s="T211">puːntɨsä </ts>
               <ts e="T213" id="Seg_1638" n="e" s="T212">čʼontoːqɨntä </ts>
               <ts e="T214" id="Seg_1640" n="e" s="T213">parqolʼlʼɛːte </ts>
               <ts e="T215" id="Seg_1642" n="e" s="T214">kəːqalʼlʼä. </ts>
               <ts e="T216" id="Seg_1644" n="e" s="T215">Moqonä </ts>
               <ts e="T217" id="Seg_1646" n="e" s="T216">kusa </ts>
               <ts e="T218" id="Seg_1648" n="e" s="T217">qəllaj. </ts>
               <ts e="T219" id="Seg_1650" n="e" s="T218">Moqona </ts>
               <ts e="T220" id="Seg_1652" n="e" s="T219">tüːŋa. </ts>
               <ts e="T221" id="Seg_1654" n="e" s="T220">Imaqota </ts>
               <ts e="T222" id="Seg_1656" n="e" s="T221">nɨlʼčʼik </ts>
               <ts e="T223" id="Seg_1658" n="e" s="T222">kətɨŋɨtɨ </ts>
               <ts e="T224" id="Seg_1660" n="e" s="T223">Opsä </ts>
               <ts e="T225" id="Seg_1662" n="e" s="T224">irra </ts>
               <ts e="T226" id="Seg_1664" n="e" s="T225">taːtolʼä: </ts>
               <ts e="T227" id="Seg_1666" n="e" s="T226">Soma </ts>
               <ts e="T228" id="Seg_1668" n="e" s="T227">pɨpaja. </ts>
               <ts e="T229" id="Seg_1670" n="e" s="T228">Ilʼempɔːtɨt, </ts>
               <ts e="T230" id="Seg_1672" n="e" s="T229">nälʼantɨn </ts>
               <ts e="T231" id="Seg_1674" n="e" s="T230">moqa </ts>
               <ts e="T232" id="Seg_1676" n="e" s="T231">sɔːntɨrlʼä. </ts>
               <ts e="T233" id="Seg_1678" n="e" s="T232">Ukkɨr </ts>
               <ts e="T234" id="Seg_1680" n="e" s="T233">ija </ts>
               <ts e="T235" id="Seg_1682" n="e" s="T234">qoŋatɨ, </ts>
               <ts e="T236" id="Seg_1684" n="e" s="T235">ijatɨ </ts>
               <ts e="T237" id="Seg_1686" n="e" s="T236">orompa. </ts>
               <ts e="T238" id="Seg_1688" n="e" s="T237">Ukkɨr </ts>
               <ts e="T239" id="Seg_1690" n="e" s="T238">čʼontoːqɨt </ts>
               <ts e="T240" id="Seg_1692" n="e" s="T239">irra: </ts>
               <ts e="T241" id="Seg_1694" n="e" s="T240">Näjannɨ </ts>
               <ts e="T242" id="Seg_1696" n="e" s="T241">mɨqı </ts>
               <ts e="T243" id="Seg_1698" n="e" s="T242">üːtälʼäj. </ts>
               <ts e="T244" id="Seg_1700" n="e" s="T243">Moqɨnäqat </ts>
               <ts e="T245" id="Seg_1702" n="e" s="T244">pɨpaja </ts>
               <ts e="T246" id="Seg_1704" n="e" s="T245">qumiːtə </ts>
               <ts e="T247" id="Seg_1706" n="e" s="T246">ətältɔːtät. </ts>
               <ts e="T248" id="Seg_1708" n="e" s="T247">Nälʼamtɨ </ts>
               <ts e="T249" id="Seg_1710" n="e" s="T248">miŋɔːtäː </ts>
               <ts e="T250" id="Seg_1712" n="e" s="T249">irra </ts>
               <ts e="T251" id="Seg_1714" n="e" s="T250">imaqotaj </ts>
               <ts e="T252" id="Seg_1716" n="e" s="T251">mɨqaːqə. </ts>
               <ts e="T253" id="Seg_1718" n="e" s="T252">Nälʼantɨlʼ </ts>
               <ts e="T254" id="Seg_1720" n="e" s="T253">qənnɔːqə. </ts>
               <ts e="T255" id="Seg_1722" n="e" s="T254">Moqonä </ts>
               <ts e="T256" id="Seg_1724" n="e" s="T255">tɨmtä </ts>
               <ts e="T257" id="Seg_1726" n="e" s="T256">iloqo </ts>
               <ts e="T258" id="Seg_1728" n="e" s="T257">qošik </ts>
               <ts e="T259" id="Seg_1730" n="e" s="T258">ɛːja. </ts>
               <ts e="T260" id="Seg_1732" n="e" s="T259">Na </ts>
               <ts e="T261" id="Seg_1734" n="e" s="T260">pünakesa </ts>
               <ts e="T262" id="Seg_1736" n="e" s="T261">irra </ts>
               <ts e="T263" id="Seg_1738" n="e" s="T262">mɨqaːqə </ts>
               <ts e="T264" id="Seg_1740" n="e" s="T263">nälʼantɨ </ts>
               <ts e="T265" id="Seg_1742" n="e" s="T264">mila </ts>
               <ts e="T266" id="Seg_1744" n="e" s="T265">üːtɨŋɨtɨ </ts>
               <ts e="T267" id="Seg_1746" n="e" s="T266">ijantɨsä </ts>
               <ts e="T268" id="Seg_1748" n="e" s="T267">šölʼqumɨlʼ </ts>
               <ts e="T269" id="Seg_1750" n="e" s="T268">tətontə, </ts>
               <ts e="T270" id="Seg_1752" n="e" s="T269">Tɔːs </ts>
               <ts e="T271" id="Seg_1754" n="e" s="T270">qəltontə. </ts>
               <ts e="T272" id="Seg_1756" n="e" s="T271">Pünakəsalʼ </ts>
               <ts e="T273" id="Seg_1758" n="e" s="T272">nätäk </ts>
               <ts e="T274" id="Seg_1760" n="e" s="T273">čʼap </ts>
               <ts e="T275" id="Seg_1762" n="e" s="T274">tüːŋa </ts>
               <ts e="T276" id="Seg_1764" n="e" s="T275">irantə </ts>
               <ts e="T277" id="Seg_1766" n="e" s="T276">tətontə. </ts>
               <ts e="T278" id="Seg_1768" n="e" s="T277">Ileqo </ts>
               <ts e="T279" id="Seg_1770" n="e" s="T278">qošik </ts>
               <ts e="T280" id="Seg_1772" n="e" s="T279">ɛːja, </ts>
               <ts e="T281" id="Seg_1774" n="e" s="T280">säːqasɨk </ts>
               <ts e="T282" id="Seg_1776" n="e" s="T281">ɛːŋa. </ts>
               <ts e="T283" id="Seg_1778" n="e" s="T282">Ijamtə </ts>
               <ts e="T284" id="Seg_1780" n="e" s="T283">äsɨntɨsa </ts>
               <ts e="T285" id="Seg_1782" n="e" s="T284">qəːučʼiŋɨtɨ </ts>
               <ts e="T286" id="Seg_1784" n="e" s="T285">Tɔːs </ts>
               <ts e="T287" id="Seg_1786" n="e" s="T286">qoltontə, </ts>
               <ts e="T288" id="Seg_1788" n="e" s="T287">irrantɨ </ts>
               <ts e="T289" id="Seg_1790" n="e" s="T288">tətontɨ, </ts>
               <ts e="T290" id="Seg_1792" n="e" s="T289">šolʼqumɨn </ts>
               <ts e="T291" id="Seg_1794" n="e" s="T290">tətontɨ. </ts>
               <ts e="T292" id="Seg_1796" n="e" s="T291">A </ts>
               <ts e="T293" id="Seg_1798" n="e" s="T292">mat </ts>
               <ts e="T294" id="Seg_1800" n="e" s="T293">moqona </ts>
               <ts e="T295" id="Seg_1802" n="e" s="T294">qəntak. </ts>
               <ts e="T296" id="Seg_1804" n="e" s="T295">Pünakɨsaj </ts>
               <ts e="T297" id="Seg_1806" n="e" s="T296">təttaqaːq </ts>
               <ts e="T298" id="Seg_1808" n="e" s="T297">iramtɨ </ts>
               <ts e="T299" id="Seg_1810" n="e" s="T298">ijantɨsä </ts>
               <ts e="T300" id="Seg_1812" n="e" s="T299">qoːčʼiɛːŋɨtɨ. </ts>
               <ts e="T301" id="Seg_1814" n="e" s="T300">Somak </ts>
               <ts e="T302" id="Seg_1816" n="e" s="T301">ilʼeŋɨlʼıː. </ts>
               <ts e="T303" id="Seg_1818" n="e" s="T302">Šolʼqup </ts>
               <ts e="T304" id="Seg_1820" n="e" s="T303">kɨpa </ts>
               <ts e="T305" id="Seg_1822" n="e" s="T304">käːtɨiɛːŋɔːtät. </ts>
               <ts e="T306" id="Seg_1824" n="e" s="T305">Pünakɨsalʼ </ts>
               <ts e="T307" id="Seg_1826" n="e" s="T306">nɨlʼčʼik </ts>
               <ts e="T308" id="Seg_1828" n="e" s="T307">kätɨmpatɨt: </ts>
               <ts e="T309" id="Seg_1830" n="e" s="T308">Šölʼqupsa </ts>
               <ts e="T310" id="Seg_1832" n="e" s="T309">ukuršak </ts>
               <ts e="T311" id="Seg_1834" n="e" s="T310">kəːtɨpɨlʼ </ts>
               <ts e="T312" id="Seg_1836" n="e" s="T311">iːjamɨ, </ts>
               <ts e="T313" id="Seg_1838" n="e" s="T312">ilʼiŋɨijäːtot. </ts>
               <ts e="T314" id="Seg_1840" n="e" s="T313">Worqɨ </ts>
               <ts e="T315" id="Seg_1842" n="e" s="T314">käpɨk </ts>
               <ts e="T316" id="Seg_1844" n="e" s="T315">ɛːŋa </ts>
               <ts e="T317" id="Seg_1846" n="e" s="T316">ijetat. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_1847" s="T0">SAI_1965_LittleDoll_flk.001 (001.001)</ta>
            <ta e="T11" id="Seg_1848" s="T6">SAI_1965_LittleDoll_flk.002 (001.002)</ta>
            <ta e="T13" id="Seg_1849" s="T11">SAI_1965_LittleDoll_flk.003 (001.003)</ta>
            <ta e="T19" id="Seg_1850" s="T13">SAI_1965_LittleDoll_flk.004 (001.004)</ta>
            <ta e="T31" id="Seg_1851" s="T19">SAI_1965_LittleDoll_flk.005 (001.005)</ta>
            <ta e="T37" id="Seg_1852" s="T31">SAI_1965_LittleDoll_flk.006 (001.006)</ta>
            <ta e="T44" id="Seg_1853" s="T37">SAI_1965_LittleDoll_flk.007 (001.007)</ta>
            <ta e="T51" id="Seg_1854" s="T44">SAI_1965_LittleDoll_flk.008 (001.008)</ta>
            <ta e="T59" id="Seg_1855" s="T51">SAI_1965_LittleDoll_flk.009 (001.009)</ta>
            <ta e="T65" id="Seg_1856" s="T59">SAI_1965_LittleDoll_flk.010 (001.010)</ta>
            <ta e="T68" id="Seg_1857" s="T65">SAI_1965_LittleDoll_flk.011 (001.011)</ta>
            <ta e="T75" id="Seg_1858" s="T68">SAI_1965_LittleDoll_flk.012 (001.012)</ta>
            <ta e="T83" id="Seg_1859" s="T75">SAI_1965_LittleDoll_flk.013 (001.013)</ta>
            <ta e="T89" id="Seg_1860" s="T83">SAI_1965_LittleDoll_flk.014 (001.014)</ta>
            <ta e="T94" id="Seg_1861" s="T89">SAI_1965_LittleDoll_flk.015 (001.015)</ta>
            <ta e="T95" id="Seg_1862" s="T94">SAI_1965_LittleDoll_flk.016 (001.016)</ta>
            <ta e="T98" id="Seg_1863" s="T95">SAI_1965_LittleDoll_flk.017 (001.017)</ta>
            <ta e="T103" id="Seg_1864" s="T98">SAI_1965_LittleDoll_flk.018 (001.018)</ta>
            <ta e="T105" id="Seg_1865" s="T103">SAI_1965_LittleDoll_flk.019 (001.019)</ta>
            <ta e="T108" id="Seg_1866" s="T105">SAI_1965_LittleDoll_flk.020 (001.020)</ta>
            <ta e="T109" id="Seg_1867" s="T108">SAI_1965_LittleDoll_flk.021 (001.021)</ta>
            <ta e="T111" id="Seg_1868" s="T109">SAI_1965_LittleDoll_flk.022 (001.022)</ta>
            <ta e="T114" id="Seg_1869" s="T111">SAI_1965_LittleDoll_flk.023 (001.023)</ta>
            <ta e="T116" id="Seg_1870" s="T114">SAI_1965_LittleDoll_flk.024 (001.024)</ta>
            <ta e="T118" id="Seg_1871" s="T116">SAI_1965_LittleDoll_flk.025 (001.025)</ta>
            <ta e="T122" id="Seg_1872" s="T118">SAI_1965_LittleDoll_flk.026 (001.026)</ta>
            <ta e="T127" id="Seg_1873" s="T122">SAI_1965_LittleDoll_flk.027 (001.027)</ta>
            <ta e="T128" id="Seg_1874" s="T127">SAI_1965_LittleDoll_flk.028 (001.028)</ta>
            <ta e="T131" id="Seg_1875" s="T128">SAI_1965_LittleDoll_flk.029 (001.029)</ta>
            <ta e="T135" id="Seg_1876" s="T131">SAI_1965_LittleDoll_flk.030 (001.030)</ta>
            <ta e="T136" id="Seg_1877" s="T135">SAI_1965_LittleDoll_flk.031 (001.031)</ta>
            <ta e="T149" id="Seg_1878" s="T136">SAI_1965_LittleDoll_flk.032 (001.032)</ta>
            <ta e="T152" id="Seg_1879" s="T149">SAI_1965_LittleDoll_flk.033 (001.033)</ta>
            <ta e="T154" id="Seg_1880" s="T152">SAI_1965_LittleDoll_flk.034 (001.034)</ta>
            <ta e="T156" id="Seg_1881" s="T154">SAI_1965_LittleDoll_flk.035 (001.035)</ta>
            <ta e="T160" id="Seg_1882" s="T156">SAI_1965_LittleDoll_flk.036 (001.036)</ta>
            <ta e="T162" id="Seg_1883" s="T160">SAI_1965_LittleDoll_flk.037 (001.037)</ta>
            <ta e="T168" id="Seg_1884" s="T162">SAI_1965_LittleDoll_flk.038 (001.038)</ta>
            <ta e="T173" id="Seg_1885" s="T168">SAI_1965_LittleDoll_flk.039 (001.039)</ta>
            <ta e="T175" id="Seg_1886" s="T173">SAI_1965_LittleDoll_flk.040 (001.040)</ta>
            <ta e="T182" id="Seg_1887" s="T175">SAI_1965_LittleDoll_flk.041 (001.041)</ta>
            <ta e="T186" id="Seg_1888" s="T182">SAI_1965_LittleDoll_flk.042 (001.042)</ta>
            <ta e="T188" id="Seg_1889" s="T186">SAI_1965_LittleDoll_flk.043 (001.043)</ta>
            <ta e="T192" id="Seg_1890" s="T188">SAI_1965_LittleDoll_flk.044 (001.044)</ta>
            <ta e="T198" id="Seg_1891" s="T192">SAI_1965_LittleDoll_flk.045 (001.045)</ta>
            <ta e="T205" id="Seg_1892" s="T198">SAI_1965_LittleDoll_flk.046 (001.046)</ta>
            <ta e="T207" id="Seg_1893" s="T205">SAI_1965_LittleDoll_flk.047 (001.047)</ta>
            <ta e="T210" id="Seg_1894" s="T207">SAI_1965_LittleDoll_flk.048 (001.048)</ta>
            <ta e="T215" id="Seg_1895" s="T210">SAI_1965_LittleDoll_flk.049 (001.049)</ta>
            <ta e="T218" id="Seg_1896" s="T215">SAI_1965_LittleDoll_flk.050 (001.050)</ta>
            <ta e="T220" id="Seg_1897" s="T218">SAI_1965_LittleDoll_flk.051 (001.051)</ta>
            <ta e="T228" id="Seg_1898" s="T220">SAI_1965_LittleDoll_flk.052 (001.052)</ta>
            <ta e="T232" id="Seg_1899" s="T228">SAI_1965_LittleDoll_flk.053 (001.053)</ta>
            <ta e="T237" id="Seg_1900" s="T232">SAI_1965_LittleDoll_flk.054 (001.054)</ta>
            <ta e="T243" id="Seg_1901" s="T237">SAI_1965_LittleDoll_flk.055 (001.055)</ta>
            <ta e="T247" id="Seg_1902" s="T243">SAI_1965_LittleDoll_flk.056 (001.056)</ta>
            <ta e="T252" id="Seg_1903" s="T247">SAI_1965_LittleDoll_flk.057 (001.057)</ta>
            <ta e="T254" id="Seg_1904" s="T252">SAI_1965_LittleDoll_flk.058 (001.058)</ta>
            <ta e="T259" id="Seg_1905" s="T254">SAI_1965_LittleDoll_flk.059 (001.059)</ta>
            <ta e="T271" id="Seg_1906" s="T259">SAI_1965_LittleDoll_flk.060 (001.060)</ta>
            <ta e="T277" id="Seg_1907" s="T271">SAI_1965_LittleDoll_flk.061 (001.061)</ta>
            <ta e="T282" id="Seg_1908" s="T277">SAI_1965_LittleDoll_flk.062 (001.062)</ta>
            <ta e="T291" id="Seg_1909" s="T282">SAI_1965_LittleDoll_flk.063 (001.063)</ta>
            <ta e="T295" id="Seg_1910" s="T291">SAI_1965_LittleDoll_flk.064 (001.064)</ta>
            <ta e="T300" id="Seg_1911" s="T295">SAI_1965_LittleDoll_flk.065 (001.065)</ta>
            <ta e="T302" id="Seg_1912" s="T300">SAI_1965_LittleDoll_flk.066 (001.066)</ta>
            <ta e="T305" id="Seg_1913" s="T302">SAI_1965_LittleDoll_flk.067 (001.067)</ta>
            <ta e="T313" id="Seg_1914" s="T305">SAI_1965_LittleDoll_flk.068 (001.068)</ta>
            <ta e="T317" id="Seg_1915" s="T313">SAI_1965_LittleDoll_flk.069 (001.069)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_1916" s="T0">′кошиколʼа ′илʼемпа ук′кур kуп, тʼ[чʼ]и′лʼалʼ kуп.</ta>
            <ta e="T11" id="Seg_1917" s="T6">ни ′порkыто[ы], ни ′пе̄мыты ′чеңка са̄.</ta>
            <ta e="T13" id="Seg_1918" s="T11">′нʼанkыче[а] ′ӣlымпа.</ta>
            <ta e="T19" id="Seg_1919" s="T13">′тӓ̄натымпа [тӓнырпа] ′куттар ныlджак ′ӣlантак ′пелʼи′kколык.</ta>
            <ta e="T31" id="Seg_1920" s="T19">′kы[ъ̊]саɣана [′kъ̊саңена], ′кӯнӓма [кучет ′эмма] kумык ′kонтак ′косаң ′ена.</ta>
            <ta e="T37" id="Seg_1921" s="T31">′пона ′танта ′kъ̊нна ку′чет ′тӓнытӓ ′тӯтырымпа.</ta>
            <ta e="T44" id="Seg_1922" s="T37">′уккыр ′чонтоkыт[н] ′маннымпат kы? kонд ′коlдже ′а̊̄та.</ta>
            <ta e="T51" id="Seg_1923" s="T44">′нече ′па̊нынʼе[ӓ] kаррӓ ча′па̊нынʼе ′монтъ ′kоlдъ.</ta>
            <ta e="T59" id="Seg_1924" s="T51">′таккы ′маннымпа ′чумпы ′kоlдит кы[ъ̊]l ′онтъ ′кинче ′а̊̄тентъ.</ta>
            <ta e="T65" id="Seg_1925" s="T59">ны ′чап ′тӱңак, ′мукуlтириl поl ′ма̄тырты.</ta>
            <ta e="T68" id="Seg_1926" s="T65">′ӱңы ′тоlак ′о̨рачар еңотыт.</ta>
            <ta e="T75" id="Seg_1927" s="T68">′мотты ′коңак ′мунты ′шʼите[ы] ′топаl ′мырк еңа.</ta>
            <ta e="T83" id="Seg_1928" s="T75">′моттетъ ′нынтъ ′мортӓт ′къ̊lа уlдимботыт ′мортаɣыт ′кинджин ′мо̄ртӓт.</ta>
            <ta e="T89" id="Seg_1929" s="T83">′иllӓ ′маннымпа, ′опчин морɣъ ′вӓрɣъ ′о̨пчин.</ta>
            <ta e="T94" id="Seg_1930" s="T89">′таккы ′маннимпак kай ′лосы ′тӱнты.</ta>
            <ta e="T95" id="Seg_1931" s="T94">′ныркы ′мо̄на.</ta>
            <ta e="T98" id="Seg_1932" s="T95">ӯ′кот ′тӱ̄сак. ′мо[ӓ]ркъlак.</ta>
            <ta e="T103" id="Seg_1933" s="T98">′чоlсӓ шʼип ′абlе. мӓркак ′нымтӓ.</ta>
            <ta e="T105" id="Seg_1934" s="T103">на ′тӱнтъ.</ta>
            <ta e="T108" id="Seg_1935" s="T105">′ныlчик шʼип ′kоңа.</ta>
            <ta e="T109" id="Seg_1936" s="T108">′пы̄‵паjа.</ta>
            <ta e="T111" id="Seg_1937" s="T109">′не̨lамы ′сонтыр ′тӓнта.</ta>
            <ta e="T114" id="Seg_1938" s="T111">′котӓ ′kынтыса‵мена.</ta>
            <ta e="T116" id="Seg_1939" s="T114">ну ′поɣунтӓ ′омтъ′тӓɣытӓ.</ta>
            <ta e="T118" id="Seg_1940" s="T116">′моkонӓ ′kынтоɣыт.</ta>
            <ta e="T122" id="Seg_1941" s="T118">а kуп ′но ′поɣыт ′о̨мта.</ta>
            <ta e="T127" id="Seg_1942" s="T122">′маннымпаты ′коннӓ ′ва̊рkъ ′мот. омнанты.</ta>
            <ta e="T128" id="Seg_1943" s="T127">′тӱңа.</ta>
            <ta e="T131" id="Seg_1944" s="T128">ны ′мо̄ты ′туlд‵иңыты.</ta>
            <ta e="T135" id="Seg_1945" s="T131">′нӓjа ′kоңак ′пы̄паjап ′со̄нтарашʼ.</ta>
            <ta e="T136" id="Seg_1946" s="T135">′илʼем′потыт.</ta>
            <ta e="T149" id="Seg_1947" s="T136">′имакотатъ ′ныlчик ′къ̊тыңыты ′ирра ′ынтъ ′меңащʼип ′ӯкот ′онтъ ′иlеб̂[п]илʼ kуп. ′суръ ′моче ′амkо.</ta>
            <ta e="T152" id="Seg_1948" s="T149">′ирра ′ынтъ[ӓ]п ′ме̄ңатъ.</ta>
            <ta e="T154" id="Seg_1949" s="T152">къ̊сса kъ̊llоɣън.</ta>
            <ta e="T156" id="Seg_1950" s="T154">′тоlчеп ′ме̄ңатъ.</ta>
            <ta e="T160" id="Seg_1951" s="T156">на kуп ′тоlдʼ[ч]иса ′къ̊ннӓ.</ta>
            <ta e="T162" id="Seg_1952" s="T160">′ындатӓ ′ӣңатъ.</ta>
            <ta e="T168" id="Seg_1953" s="T162">онт ′ирра ′оlг̂а къ̊нна ′тоlчикоlик ′ӱ̄та.</ta>
            <ta e="T173" id="Seg_1954" s="T168">ук′кур чон′тоɣыт ′kоңаты. ′о̨тан′мъ̊тып. </ta>
            <ta e="T175" id="Seg_1955" s="T173">′нӱ′[о]тыңыты.</ta>
            <ta e="T182" id="Seg_1956" s="T175">′пыпа‵jа ′нʼӓнна не′щʼолʼны[ъ] ′къ̊теllʼа ′отап ′саисе ′kоңатъ.</ta>
            <ta e="T186" id="Seg_1957" s="T182">′ынтысӓ ′чатыңыты ′уккор ′kорры.</ta>
            <ta e="T188" id="Seg_1958" s="T186">′иllе ′омта.</ta>
            <ta e="T192" id="Seg_1959" s="T188">′моkонынӓ ′ирра ′тӱңа.</ta>
            <ta e="T198" id="Seg_1960" s="T192">′маннымпаты ′kоры ′иппынты ′пыпаjа ′о̨мта ′нуныча.</ta>
            <ta e="T205" id="Seg_1961" s="T198">′ирра ′ныlчик ′къ̊то‵ɣыты ′опсӓ ′kоңап ′моkонӓ ′кынтак.</ta>
            <ta e="T207" id="Seg_1962" s="T205">то̄ ′къ̊ннӓ.</ta>
            <ta e="T210" id="Seg_1963" s="T207">′ӯтӓз̂ӓ ′тӱ̄ма ′сопыр‵ныты.</ta>
            <ta e="T215" id="Seg_1964" s="T210">′корымты ′пунтыз̂ӓ чон′тоɣынтӓ ′парколʼете ′къ̊калʼлʼе.</ta>
            <ta e="T218" id="Seg_1965" s="T215">′моkонӓ ′куса ′къ̊llай.</ta>
            <ta e="T220" id="Seg_1966" s="T218">′моkона ′тӱ̄ңа.</ta>
            <ta e="T228" id="Seg_1967" s="T220">′ӣмаkота ′ныlчик къ̊′тыңыты ′опсӓ ′ирра ′тато̄лʼе. ′сома ′пы̄па‵jа. </ta>
            <ta e="T232" id="Seg_1968" s="T228">′илʼемп′о̄тыт ′нӓlандын ′моkа сонтырлʼе̨.</ta>
            <ta e="T237" id="Seg_1969" s="T232">′укко[ы]р ′иjа ′коңаты ′ӣjаты ′оромпа.</ta>
            <ta e="T243" id="Seg_1970" s="T237">′уккыр чон′тоɣыт ′ирра ′нӓjанны ′мыkы ′ӱ̄тӓлʼей.</ta>
            <ta e="T247" id="Seg_1971" s="T243">′моkо[ы]н ′еɣат ′пы̄па‵jа ′kумитъ ′о[ъ̊]тӓl ′то̄тӓт.</ta>
            <ta e="T252" id="Seg_1972" s="T247">′нӓlамты ′миңотӓ и′рра ′имаkо′тай мы′каɣъ.</ta>
            <ta e="T254" id="Seg_1973" s="T252">′нӓlантыl ′kъ̊нноɣъ.</ta>
            <ta e="T259" id="Seg_1974" s="T254">′мо̄kонӓ ′тымтӓ ′иllого ′кошик ′еjа.</ta>
            <ta e="T271" id="Seg_1975" s="T259">на ′пӱнакеса ′ирра мы′каɣъ ′нӓлʼанты ′миllа ′ӱ̄тыңыты. иjа′нтызӓ ′шʼӧ̄l kӯмыl ′тъ̊тондъ то̄с ′kу[ъ̊]l‵тонтъ.</ta>
            <ta e="T277" id="Seg_1976" s="T271">пӱнакъсаl ′ме̨тӓк ′чап ′тӱ̄ңа ′ирантъ тъ̊̄′тонтъ.</ta>
            <ta e="T282" id="Seg_1977" s="T277">′иlего ′кошик ′е̨jа сӓ̄ касык еңа.</ta>
            <ta e="T291" id="Seg_1978" s="T282">′иjамтъ ′ӓсынтыса ′къ̊у‵чиңыты. то̄с ′kоlтонтъ ′ирранты тъ̊̄′тонты. ′шʼоl kумын тъ̊̄′тонты.</ta>
            <ta e="T295" id="Seg_1979" s="T291">а мат ′мо̄kона ′къ̊нтак.</ta>
            <ta e="T300" id="Seg_1980" s="T295">пӱнакысай ′тъ̊ттаɣак ′ирамты ′иjантысӓ кочи ′еңыты.</ta>
            <ta e="T302" id="Seg_1981" s="T300">′со̄мак ′илʼеңылʼи.</ta>
            <ta e="T305" id="Seg_1982" s="T302">шʼолʼ kуп ′кыпа ′кӓты и еңоты[ӓ]т.</ta>
            <ta e="T313" id="Seg_1983" s="T305">′кӱнакысаl ′ныlчик ′кӓтымпатыт ′шʼӧlкупса ′укуршак ′къ̊тыпыl ′ӣjамы ′иlиңыиjетот.</ta>
            <ta e="T317" id="Seg_1984" s="T313">′ворkы ′кӓпык ′еңа иjетат.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_1985" s="T0">košikolʼa ilʼempa ukkur qup, č[čʼ]ilʼalʼ qup.</ta>
            <ta e="T11" id="Seg_1986" s="T6">ni porqɨto[ɨ], ni peːmɨtɨ čeŋka saː.</ta>
            <ta e="T13" id="Seg_1987" s="T11">nʼanqɨče[a] iːlʼɨmpa.</ta>
            <ta e="T19" id="Seg_1988" s="T13">täːnatɨmpa [tänɨrpa] kuttar nɨlʼdžak iːlʼantak pelʼiqkolɨk.</ta>
            <ta e="T31" id="Seg_1989" s="T19">qɨ[ə]saqana [qəsaŋena], kuːnäma [kučet ɛmma] qumɨk qontak kosaŋ ena.</ta>
            <ta e="T37" id="Seg_1990" s="T31">pona tanta qənna kučet tänɨtä tuːtɨrɨmpa.</ta>
            <ta e="T44" id="Seg_1991" s="T37">ukkɨr čontoqɨt[n] mannɨmpat qɨ? qond kolʼdže aːta.</ta>
            <ta e="T51" id="Seg_1992" s="T44">neče panɨnʼe[ä] qarrä čapanɨnʼe montə qolʼdə.</ta>
            <ta e="T59" id="Seg_1993" s="T51">takkɨ mannɨmpa čumpɨ qolʼdit kɨ[ə]lʼ ontə kinče aːtentə.</ta>
            <ta e="T65" id="Seg_1994" s="T59">nɨ čap tüŋak, mukulʼtirilʼ polʼ maːtɨrtɨ.</ta>
            <ta e="T68" id="Seg_1995" s="T65">üŋɨ tolʼak oračar eŋotɨt.</ta>
            <ta e="T75" id="Seg_1996" s="T68">mottɨ koŋak muntɨ šite[ɨ] topalʼ mɨrk eŋa.</ta>
            <ta e="T83" id="Seg_1997" s="T75">mottetə nɨntə mortät kəlʼa ulʼdimpotɨt mortaqɨt kindžin moːrtät.</ta>
            <ta e="T89" id="Seg_1998" s="T83">ilʼlʼä mannɨmpa, opčin morqə värqə opčin.</ta>
            <ta e="T94" id="Seg_1999" s="T89">takkɨ mannimpak qaj losɨ tüntɨ.</ta>
            <ta e="T95" id="Seg_2000" s="T94">nɨrkɨ moːna.</ta>
            <ta e="T98" id="Seg_2001" s="T95">uːkot tüːsak. mo[ä]rkəlʼak.</ta>
            <ta e="T103" id="Seg_2002" s="T98">čolʼsä šip aplʼe. märkak nɨmtä. </ta>
            <ta e="T105" id="Seg_2003" s="T103">na tüntə.</ta>
            <ta e="T109" id="Seg_2004" s="T108">pɨːpaja.</ta>
            <ta e="T111" id="Seg_2005" s="T109">nelʼamɨ sontɨr tänta.</ta>
            <ta e="T114" id="Seg_2006" s="T111">kotä qɨntɨsamena.</ta>
            <ta e="T116" id="Seg_2007" s="T114">nu poquntä omtətäqɨtä.</ta>
            <ta e="T118" id="Seg_2008" s="T116">moqonä qɨntoqɨt.</ta>
            <ta e="T122" id="Seg_2009" s="T118">a qup no poqɨt omta.</ta>
            <ta e="T127" id="Seg_2010" s="T122">mannɨmpatɨ konnä varqə mot. omnantɨ.</ta>
            <ta e="T128" id="Seg_2011" s="T127">tüŋa</ta>
            <ta e="T131" id="Seg_2012" s="T128">nɨ moːtɨ tulʼdiŋɨtɨ.</ta>
            <ta e="T135" id="Seg_2013" s="T131">näja qoŋak pɨːpajap soːntaraš.</ta>
            <ta e="T136" id="Seg_2014" s="T135">ilʼempotɨt.</ta>
            <ta e="T149" id="Seg_2015" s="T136">imakotatə nɨlʼčik kətɨŋɨtɨ irra ɨntə meŋašʼip uːkot ontə ilʼep̂[p]ilʼ qup. surə moče amqo.</ta>
            <ta e="T152" id="Seg_2016" s="T149">irra ɨntə[ä]p meːŋatə.</ta>
            <ta e="T154" id="Seg_2017" s="T152">kəssa qəlʼlʼoqən.</ta>
            <ta e="T156" id="Seg_2018" s="T154">tolʼčep meːŋatə.</ta>
            <ta e="T160" id="Seg_2019" s="T156">na qup tolʼdʼ[č]isa kənnä.</ta>
            <ta e="T162" id="Seg_2020" s="T160">ɨndatä iːŋatə.</ta>
            <ta e="T168" id="Seg_2021" s="T162">ont irra olʼĝa kənna tolʼčikolʼik üːta.</ta>
            <ta e="T173" id="Seg_2022" s="T168">ukkur čontoqɨt qoŋatɨ. otanmətɨp.</ta>
            <ta e="T175" id="Seg_2023" s="T173">nü[o]tɨŋɨtɨ pɨpaja nʼänna nešʼolʼnɨ[ə] kətelʼlʼʼa otap saise qoŋatə.</ta>
            <ta e="T182" id="Seg_2024" s="T175">nü[o]tɨŋɨtɨ pɨpaja nʼänna nešʼolʼnɨ[ə] kətelʼlʼʼa otap saise qoŋatə.</ta>
            <ta e="T186" id="Seg_2025" s="T182">ɨntɨsä čatɨŋɨtɨ ukkor qorrɨ.</ta>
            <ta e="T188" id="Seg_2026" s="T186">ilʼlʼe omta.</ta>
            <ta e="T192" id="Seg_2027" s="T188">moqonɨnä irra tüŋa.</ta>
            <ta e="T198" id="Seg_2028" s="T192">mannɨmpatɨ qorɨ ippɨntɨ pɨpaja omta nunɨča.</ta>
            <ta e="T205" id="Seg_2029" s="T198">irra nɨlʼčik kətoqɨtɨ opsä qoŋap moqonä kɨntak.</ta>
            <ta e="T207" id="Seg_2030" s="T205">toː kənnä.</ta>
            <ta e="T210" id="Seg_2031" s="T207">uːtäẑä tüːma sopɨrnɨtɨ.</ta>
            <ta e="T215" id="Seg_2032" s="T210">korɨmtɨ puntɨẑä čontoqɨntä parkolʼete kəkalʼlʼe.</ta>
            <ta e="T218" id="Seg_2033" s="T215">moqonä kusa kəlʼlʼaj.</ta>
            <ta e="T220" id="Seg_2034" s="T218">moqona tüːŋa.</ta>
            <ta e="T228" id="Seg_2035" s="T220">iːmaqota nɨlʼčik kətɨŋɨtɨ opsä irra tatoːlʼe. soma pɨːpaja.</ta>
            <ta e="T232" id="Seg_2036" s="T228">ilʼempoːtɨt nälʼandɨn moqa sontɨrlʼe.</ta>
            <ta e="T237" id="Seg_2037" s="T232">ukko[ɨ]r ija koŋatɨ iːjatɨ orompa.</ta>
            <ta e="T243" id="Seg_2038" s="T237">ukkɨr čontoqɨt irra näjannɨ mɨqɨ üːtälʼej.</ta>
            <ta e="T247" id="Seg_2039" s="T243">moqo[ɨ]n eqat pɨːpaja qumitə o[ə]tälʼ toːtät.</ta>
            <ta e="T252" id="Seg_2040" s="T247">nälʼamtɨ miŋotä irra imaqotaj mɨkaqə.</ta>
            <ta e="T254" id="Seg_2041" s="T252">nälʼantɨlʼ qənnoqə.</ta>
            <ta e="T259" id="Seg_2042" s="T254">moːqonä tɨmtä ilʼlʼogo košik eja.</ta>
            <ta e="T271" id="Seg_2043" s="T259">na pünakesa irra mɨkaqə nälʼantɨ milʼlʼa üːtɨŋɨtɨ. ijantɨzä šöːlʼ quːmɨlʼ tətondə toːs qu[ə]lʼtontə.</ta>
            <ta e="T277" id="Seg_2044" s="T271">pünakəsalʼ metäk čap tüːŋa irantə təːtontə.</ta>
            <ta e="T282" id="Seg_2045" s="T277">ilʼego košik eja säː kasɨk eŋa.</ta>
            <ta e="T291" id="Seg_2046" s="T282">ijamtə äsɨntɨsa kəučiŋɨtɨ. toːs qolʼtontə irrantɨ təːtontɨ. šolʼ qumɨn təːtontɨ.</ta>
            <ta e="T295" id="Seg_2047" s="T291">a mat moːqona kəntak.</ta>
            <ta e="T300" id="Seg_2048" s="T295">pünakɨsaj təttaqak iramtɨ ijantɨsä koči eŋɨtɨ.</ta>
            <ta e="T302" id="Seg_2049" s="T300">soːmak ilʼeŋɨlʼi.</ta>
            <ta e="T305" id="Seg_2050" s="T302">šolʼ qup kɨpa kätɨ i eŋotɨ[ä]t.</ta>
            <ta e="T313" id="Seg_2051" s="T305">künakɨsalʼ nɨlʼčik kätɨmpatɨt šölʼkupsa ukuršak kətɨpɨlʼ iːjamɨ ilʼiŋɨijetot.</ta>
            <ta e="T317" id="Seg_2052" s="T313">vorqɨ käpɨk eŋa ijetat.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_2053" s="T0">Qošikɔːlʼa ilʼempa ukkur qup, čʼilʼalʼ qup. </ta>
            <ta e="T11" id="Seg_2054" s="T6">Ni porqɨtɨ, ni peːmɨtɨ čʼäːŋkasa. </ta>
            <ta e="T13" id="Seg_2055" s="T11">Nʼaŋɨčʼa ilɨmpa. </ta>
            <ta e="T19" id="Seg_2056" s="T13">Tɛnatɨmpa (/tɛnɨrpa): Kuttar nɨlʼčʼak ilantak pɛlʼikɔːlɨk. </ta>
            <ta e="T31" id="Seg_2057" s="T19">Qəssak ana (/qəssaŋ ɛna), kun ɛːma (/kučʼät ɛːmma) qumɨk qontak, qosaŋ ɛna. </ta>
            <ta e="T37" id="Seg_2058" s="T31">Pona tanta, qənna kučʼät tɛnɨtä tuːtɨrɨmpa. </ta>
            <ta e="T44" id="Seg_2059" s="T37">Ukkɨr čʼontoːqɨn mannɨmpat kɨ qont, qolčʼe ata. </ta>
            <ta e="T51" id="Seg_2060" s="T44">Näčʼä panɨnʼnʼe, karrä čʼa panɨnʼnʼe, montə qoltə. </ta>
            <ta e="T59" id="Seg_2061" s="T51">Takkɨ mannɨmpa, čʼumpɨ qoltit kəːlʼ, ontə kınčʼe atentə. </ta>
            <ta e="T65" id="Seg_2062" s="T59">Nɨː čʼap tüŋak, muqultirilʼ poːlʼ maːtɨrtɨ. </ta>
            <ta e="T68" id="Seg_2063" s="T65">Üŋɨt ɔːlak oračʼareŋɔːtɨt. </ta>
            <ta e="T75" id="Seg_2064" s="T68">Mottɨ qoŋak, muntɨ šitɨ topalʼ mɨrq ɛːŋa. </ta>
            <ta e="T83" id="Seg_2065" s="T75">Mottetə nɨntə mortät, (qəːla uːltimpɔːtɨt mortaːqɨt), kınčʼin mortät. </ta>
            <ta e="T89" id="Seg_2066" s="T83">İllä mannɨmpa, opčʼin morqə, wärqə opčʼin. </ta>
            <ta e="T94" id="Seg_2067" s="T89">Takkɨ mannimpak, qaj loːsɨ tüntɨ. </ta>
            <ta e="T95" id="Seg_2068" s="T94">Nɨrkɨmɔːnna. </ta>
            <ta e="T98" id="Seg_2069" s="T95">Ukoːt tüsak, märkəlak. </ta>
            <ta e="T103" id="Seg_2070" s="T98">Čʼɔːlsä šıp aplä märkak nɨmtä. </ta>
            <ta e="T105" id="Seg_2071" s="T103">Na tüntə. </ta>
            <ta e="T108" id="Seg_2072" s="T105">Nɨlʼčʼik šıp qoŋa. </ta>
            <ta e="T109" id="Seg_2073" s="T108">Pɨpaja. </ta>
            <ta e="T111" id="Seg_2074" s="T109">Nälʼamɨ sɔːntɨrtɛnta. </ta>
            <ta e="T114" id="Seg_2075" s="T111">Qɔːtä qɨntɨsam ɛna. </ta>
            <ta e="T116" id="Seg_2076" s="T114">Nupoːquntä omtətäkkɨtä. </ta>
            <ta e="T118" id="Seg_2077" s="T116">Moqonä qɨntokkɨt. </ta>
            <ta e="T122" id="Seg_2078" s="T118">A qup nopoːqɨt ɔːmta. </ta>
            <ta e="T127" id="Seg_2079" s="T122">Mannɨmpatɨ konnä, warqə mɔːt ɔːmnantɨ. </ta>
            <ta e="T128" id="Seg_2080" s="T127">Tüŋa. </ta>
            <ta e="T131" id="Seg_2081" s="T128">Nɨː mɔːttɨ tuːltiŋɨtɨ. </ta>
            <ta e="T135" id="Seg_2082" s="T131">Näja, qoŋak pɨpajap, sɔːntaraš. </ta>
            <ta e="T136" id="Seg_2083" s="T135">Ilʼempɔːtɨt. </ta>
            <ta e="T149" id="Seg_2084" s="T136">Imaqotatə nɨlʼčʼik kətɨŋɨtɨ: Irra, ɨntə meːŋašip ukoːt ontə ilepilʼ qup suːrə močʼe amqo. </ta>
            <ta e="T152" id="Seg_2085" s="T149">Irra ɨntəp meːŋatə. </ta>
            <ta e="T154" id="Seg_2086" s="T152">Kəssa qəllɔːqən. </ta>
            <ta e="T156" id="Seg_2087" s="T154">Tolʼčʼep meːŋatə. </ta>
            <ta e="T160" id="Seg_2088" s="T156">Na qup tolʼčʼisa qənnä. </ta>
            <ta e="T162" id="Seg_2089" s="T160">Ɨntatä iːŋatə. </ta>
            <ta e="T168" id="Seg_2090" s="T162">Ont irra olqa qənna, tolʼčʼikɔːlik üːta. </ta>
            <ta e="T173" id="Seg_2091" s="T168">Ukkur čʼontoːqɨt qoŋatɨ ɔːtan mətɨp. </ta>
            <ta e="T175" id="Seg_2092" s="T173">Nʼoːtɨŋɨtɨ (/nʼuːtɨŋɨtɨ). </ta>
            <ta e="T182" id="Seg_2093" s="T175">Pɨpaja nʼenna nešolʼnɨ kətelʼlʼa ɔːtap saisä qoŋatə. </ta>
            <ta e="T186" id="Seg_2094" s="T182">Ɨntɨsä čʼatɨŋɨtɨ ukkor qoːrrɨ. </ta>
            <ta e="T188" id="Seg_2095" s="T186">İllä omta. </ta>
            <ta e="T192" id="Seg_2096" s="T188">Moqon nɨːnä irra tüŋa. </ta>
            <ta e="T198" id="Seg_2097" s="T192">Mannɨmpatɨ qoːrɨ ippɨntɨ, pɨpaja ɔːmta, nuːnɨčʼa. </ta>
            <ta e="T205" id="Seg_2098" s="T198">Irra nɨlʼčʼik kətoŋɨtɨ: Opsä qoŋap, moqonä qɨntak. </ta>
            <ta e="T207" id="Seg_2099" s="T205">Toː qənnä. </ta>
            <ta e="T210" id="Seg_2100" s="T207">Utäsä tüma sopɨrnɨtɨ. </ta>
            <ta e="T215" id="Seg_2101" s="T210">Qoːrɨmtɨ puːntɨsä čʼontoːqɨntä parqolʼlʼɛːte kəːqalʼlʼä. </ta>
            <ta e="T218" id="Seg_2102" s="T215">Moqonä kusa qəllaj. </ta>
            <ta e="T220" id="Seg_2103" s="T218">Moqona tüːŋa. </ta>
            <ta e="T228" id="Seg_2104" s="T220">Imaqota nɨlʼčʼik kətɨŋɨtɨ Opsä irra taːtolʼä: Soma pɨpaja. </ta>
            <ta e="T232" id="Seg_2105" s="T228">Ilʼempɔːtɨt, nälʼantɨn moqa sɔːntɨrlʼä. </ta>
            <ta e="T237" id="Seg_2106" s="T232">Ukkɨr ija qoŋatɨ, ijatɨ orompa. </ta>
            <ta e="T243" id="Seg_2107" s="T237">Ukkɨr čʼontoːqɨt irra: Näjannɨ mɨqı üːtälʼäj. </ta>
            <ta e="T247" id="Seg_2108" s="T243">Moqɨnäqat pɨpaja qumiːtə ətältɔːtät. </ta>
            <ta e="T252" id="Seg_2109" s="T247">Nälʼamtɨ miŋɔːtäː irra imaqotaj mɨqaːqə. </ta>
            <ta e="T254" id="Seg_2110" s="T252">Nälʼantɨlʼ qənnɔːqə. </ta>
            <ta e="T259" id="Seg_2111" s="T254">Moqonä tɨmtä iloqo qošik ɛːja. </ta>
            <ta e="T271" id="Seg_2112" s="T259">Na pünakesa irra mɨqaːqə nälʼantɨ mila üːtɨŋɨtɨ ijantɨsä šölʼqumɨlʼ tətontə, Tɔːs qəltontə. </ta>
            <ta e="T277" id="Seg_2113" s="T271">Pünakəsalʼ nätäk čʼap tüːŋa irantə tətontə. </ta>
            <ta e="T282" id="Seg_2114" s="T277">Ileqo qošik ɛːja, säːqasɨk ɛːŋa. </ta>
            <ta e="T291" id="Seg_2115" s="T282">Ijamtə äsɨntɨsa qəːučʼiŋɨtɨ Tɔːs qoltontə, irrantɨ tətontɨ, šolʼqumɨn tətontɨ. </ta>
            <ta e="T295" id="Seg_2116" s="T291">A mat moqona qəntak. </ta>
            <ta e="T300" id="Seg_2117" s="T295">Pünakɨsaj təttaqaːq iramtɨ ijantɨsä qoːčʼiɛːŋɨtɨ. </ta>
            <ta e="T302" id="Seg_2118" s="T300">Somak ilʼeŋɨlʼıː. </ta>
            <ta e="T305" id="Seg_2119" s="T302">Šolʼqup kɨpa käːtɨiɛːŋɔːtät. </ta>
            <ta e="T313" id="Seg_2120" s="T305">Pünakɨsalʼ nɨlʼčʼik kätɨmpatɨt: Šölʼqupsa ukuršak kəːtɨpɨlʼ iːjamɨ, ilʼiŋɨijäːtot. </ta>
            <ta e="T317" id="Seg_2121" s="T313">Worqɨ käpɨk ɛːŋa ijetat. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2122" s="T0">qošikɔːlʼa</ta>
            <ta e="T2" id="Seg_2123" s="T1">ilʼe-mpa</ta>
            <ta e="T3" id="Seg_2124" s="T2">ukkur</ta>
            <ta e="T4" id="Seg_2125" s="T3">qup</ta>
            <ta e="T5" id="Seg_2126" s="T4">čʼilʼa-lʼ</ta>
            <ta e="T6" id="Seg_2127" s="T5">qup</ta>
            <ta e="T7" id="Seg_2128" s="T6">ni</ta>
            <ta e="T8" id="Seg_2129" s="T7">porqɨ-tɨ</ta>
            <ta e="T9" id="Seg_2130" s="T8">ni</ta>
            <ta e="T10" id="Seg_2131" s="T9">peːmɨ-tɨ</ta>
            <ta e="T11" id="Seg_2132" s="T10">čʼäːŋka-sa</ta>
            <ta e="T12" id="Seg_2133" s="T11">nʼaŋɨčʼa</ta>
            <ta e="T13" id="Seg_2134" s="T12">ilɨ-mpa</ta>
            <ta e="T14" id="Seg_2135" s="T13">tɛna-tɨ-mpa</ta>
            <ta e="T15" id="Seg_2136" s="T14">tɛnɨ-r-pa</ta>
            <ta e="T16" id="Seg_2137" s="T15">kuttar</ta>
            <ta e="T17" id="Seg_2138" s="T16">nɨlʼčʼa-k</ta>
            <ta e="T18" id="Seg_2139" s="T17">il-anta-k</ta>
            <ta e="T19" id="Seg_2140" s="T18">pɛlʼi-kɔːlɨ-k</ta>
            <ta e="T20" id="Seg_2141" s="T19">qəs-sa-k</ta>
            <ta e="T21" id="Seg_2142" s="T20">ana</ta>
            <ta e="T22" id="Seg_2143" s="T21">qəs-sa-ŋ</ta>
            <ta e="T23" id="Seg_2144" s="T22">ɛna</ta>
            <ta e="T24" id="Seg_2145" s="T23">kun</ta>
            <ta e="T25" id="Seg_2146" s="T24">ɛːma</ta>
            <ta e="T26" id="Seg_2147" s="T25">kučʼä-t</ta>
            <ta e="T27" id="Seg_2148" s="T26">ɛːmma</ta>
            <ta e="T28" id="Seg_2149" s="T27">qum-ɨ-k</ta>
            <ta e="T29" id="Seg_2150" s="T28">qo-nta-k</ta>
            <ta e="T30" id="Seg_2151" s="T29">qo-sa-ŋ</ta>
            <ta e="T31" id="Seg_2152" s="T30">ɛna</ta>
            <ta e="T32" id="Seg_2153" s="T31">pona</ta>
            <ta e="T33" id="Seg_2154" s="T32">tanta</ta>
            <ta e="T34" id="Seg_2155" s="T33">qən-na</ta>
            <ta e="T35" id="Seg_2156" s="T34">kučʼä-t</ta>
            <ta e="T36" id="Seg_2157" s="T35">tɛnɨ-tä</ta>
            <ta e="T37" id="Seg_2158" s="T36">tuːtɨrɨ-mpa</ta>
            <ta e="T38" id="Seg_2159" s="T37">ukkɨr</ta>
            <ta e="T39" id="Seg_2160" s="T38">čʼontoː-qɨn</ta>
            <ta e="T40" id="Seg_2161" s="T39">mannɨ-mpa-t</ta>
            <ta e="T41" id="Seg_2162" s="T40">kɨ</ta>
            <ta e="T42" id="Seg_2163" s="T41">qont</ta>
            <ta e="T43" id="Seg_2164" s="T42">qolčʼe</ta>
            <ta e="T44" id="Seg_2165" s="T43">ata</ta>
            <ta e="T45" id="Seg_2166" s="T44">näčʼä</ta>
            <ta e="T46" id="Seg_2167" s="T45">panɨ-nʼ-nʼe</ta>
            <ta e="T47" id="Seg_2168" s="T46">karrä</ta>
            <ta e="T48" id="Seg_2169" s="T47">čʼa</ta>
            <ta e="T49" id="Seg_2170" s="T48">panɨ-nʼ-nʼe</ta>
            <ta e="T50" id="Seg_2171" s="T49">montə</ta>
            <ta e="T51" id="Seg_2172" s="T50">qoltə</ta>
            <ta e="T52" id="Seg_2173" s="T51">takkɨ</ta>
            <ta e="T53" id="Seg_2174" s="T52">mannɨ-mpa</ta>
            <ta e="T54" id="Seg_2175" s="T53">čʼumpɨ</ta>
            <ta e="T55" id="Seg_2176" s="T54">qolti-t</ta>
            <ta e="T56" id="Seg_2177" s="T55">kəːlʼ</ta>
            <ta e="T57" id="Seg_2178" s="T56">ontə</ta>
            <ta e="T58" id="Seg_2179" s="T57">kınčʼe</ta>
            <ta e="T59" id="Seg_2180" s="T58">ate-ntə</ta>
            <ta e="T60" id="Seg_2181" s="T59">nɨː</ta>
            <ta e="T61" id="Seg_2182" s="T60">čʼap</ta>
            <ta e="T62" id="Seg_2183" s="T61">tü-ŋa-k</ta>
            <ta e="T63" id="Seg_2184" s="T62">muqul-tiri-lʼ</ta>
            <ta e="T64" id="Seg_2185" s="T63">poː-lʼ</ta>
            <ta e="T65" id="Seg_2186" s="T64">maːtɨ-r-tɨ</ta>
            <ta e="T66" id="Seg_2187" s="T65">üŋɨ-t</ta>
            <ta e="T67" id="Seg_2188" s="T66">ɔːla-k</ta>
            <ta e="T68" id="Seg_2189" s="T67">ora-čʼa-r-e-ŋɔː-tɨt</ta>
            <ta e="T69" id="Seg_2190" s="T68">mottɨ</ta>
            <ta e="T70" id="Seg_2191" s="T69">qo-ŋa-k</ta>
            <ta e="T71" id="Seg_2192" s="T70">muntɨ</ta>
            <ta e="T72" id="Seg_2193" s="T71">šitɨ</ta>
            <ta e="T73" id="Seg_2194" s="T72">topa-lʼ</ta>
            <ta e="T74" id="Seg_2195" s="T73">mɨrq</ta>
            <ta e="T75" id="Seg_2196" s="T74">ɛː-ŋa</ta>
            <ta e="T76" id="Seg_2197" s="T75">motte-tə</ta>
            <ta e="T77" id="Seg_2198" s="T76">nɨntə</ta>
            <ta e="T78" id="Seg_2199" s="T77">mortä-t</ta>
            <ta e="T79" id="Seg_2200" s="T78">qəːla</ta>
            <ta e="T80" id="Seg_2201" s="T79">uː-lti-mpɔː-tɨt</ta>
            <ta e="T81" id="Seg_2202" s="T80">mortaː-qɨt</ta>
            <ta e="T82" id="Seg_2203" s="T81">kınčʼi-n</ta>
            <ta e="T83" id="Seg_2204" s="T82">mortä-t</ta>
            <ta e="T84" id="Seg_2205" s="T83">i̇llä</ta>
            <ta e="T85" id="Seg_2206" s="T84">mannɨ-mpa</ta>
            <ta e="T86" id="Seg_2207" s="T85">opčʼin</ta>
            <ta e="T87" id="Seg_2208" s="T86">morqə</ta>
            <ta e="T88" id="Seg_2209" s="T87">wärqə</ta>
            <ta e="T89" id="Seg_2210" s="T88">opčʼin</ta>
            <ta e="T90" id="Seg_2211" s="T89">takkɨ</ta>
            <ta e="T91" id="Seg_2212" s="T90">manni-mpa-k</ta>
            <ta e="T92" id="Seg_2213" s="T91">qaj</ta>
            <ta e="T93" id="Seg_2214" s="T92">loːsɨ</ta>
            <ta e="T94" id="Seg_2215" s="T93">tü-ntɨ</ta>
            <ta e="T95" id="Seg_2216" s="T94">nɨrkɨ-mɔːn-na</ta>
            <ta e="T96" id="Seg_2217" s="T95">ukoːt</ta>
            <ta e="T97" id="Seg_2218" s="T96">tü-sa-k</ta>
            <ta e="T98" id="Seg_2219" s="T97">märkə-la-k</ta>
            <ta e="T99" id="Seg_2220" s="T98">čʼɔːlsä</ta>
            <ta e="T100" id="Seg_2221" s="T99">šıp</ta>
            <ta e="T101" id="Seg_2222" s="T100">ap-lä</ta>
            <ta e="T102" id="Seg_2223" s="T101">märka-k</ta>
            <ta e="T103" id="Seg_2224" s="T102">nɨmtä</ta>
            <ta e="T104" id="Seg_2225" s="T103">na</ta>
            <ta e="T105" id="Seg_2226" s="T104">tü-ntə</ta>
            <ta e="T106" id="Seg_2227" s="T105">nɨlʼčʼi-k</ta>
            <ta e="T107" id="Seg_2228" s="T106">šıp</ta>
            <ta e="T108" id="Seg_2229" s="T107">qo-ŋa</ta>
            <ta e="T109" id="Seg_2230" s="T108">pɨpa-ja</ta>
            <ta e="T110" id="Seg_2231" s="T109">nälʼa-mɨ</ta>
            <ta e="T111" id="Seg_2232" s="T110">sɔːntɨr-tɛnta</ta>
            <ta e="T112" id="Seg_2233" s="T111">qɔːtä</ta>
            <ta e="T113" id="Seg_2234" s="T112">qɨn-tɨ-sa-m</ta>
            <ta e="T114" id="Seg_2235" s="T113">ɛna</ta>
            <ta e="T115" id="Seg_2236" s="T114">nupoː-qun-tä</ta>
            <ta e="T116" id="Seg_2237" s="T115">omtə-tä-kkɨ-tä</ta>
            <ta e="T117" id="Seg_2238" s="T116">moqonä</ta>
            <ta e="T118" id="Seg_2239" s="T117">qɨn-to-kkɨ-t</ta>
            <ta e="T119" id="Seg_2240" s="T118">a</ta>
            <ta e="T120" id="Seg_2241" s="T119">qup</ta>
            <ta e="T121" id="Seg_2242" s="T120">nopoː-qɨt</ta>
            <ta e="T122" id="Seg_2243" s="T121">ɔːmta</ta>
            <ta e="T123" id="Seg_2244" s="T122">mannɨ-mpa-tɨ</ta>
            <ta e="T124" id="Seg_2245" s="T123">konnä</ta>
            <ta e="T125" id="Seg_2246" s="T124">warqə</ta>
            <ta e="T126" id="Seg_2247" s="T125">mɔːt</ta>
            <ta e="T127" id="Seg_2248" s="T126">ɔːmna-ntɨ</ta>
            <ta e="T128" id="Seg_2249" s="T127">tü-ŋa</ta>
            <ta e="T129" id="Seg_2250" s="T128">nɨː</ta>
            <ta e="T130" id="Seg_2251" s="T129">mɔːt-tɨ</ta>
            <ta e="T131" id="Seg_2252" s="T130">tuː-lti-ŋɨ-tɨ</ta>
            <ta e="T132" id="Seg_2253" s="T131">näja</ta>
            <ta e="T133" id="Seg_2254" s="T132">qo-ŋa-k</ta>
            <ta e="T134" id="Seg_2255" s="T133">pɨpa-ja-p</ta>
            <ta e="T135" id="Seg_2256" s="T134">sɔːntar-aš</ta>
            <ta e="T136" id="Seg_2257" s="T135">ilʼe-mpɔː-tɨt</ta>
            <ta e="T137" id="Seg_2258" s="T136">imaqota-tə</ta>
            <ta e="T138" id="Seg_2259" s="T137">nɨlʼčʼi-k</ta>
            <ta e="T139" id="Seg_2260" s="T138">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T140" id="Seg_2261" s="T139">irra</ta>
            <ta e="T141" id="Seg_2262" s="T140">ɨntə</ta>
            <ta e="T142" id="Seg_2263" s="T141">meː-ŋašip</ta>
            <ta e="T143" id="Seg_2264" s="T142">ukoːt</ta>
            <ta e="T144" id="Seg_2265" s="T143">ontə</ta>
            <ta e="T145" id="Seg_2266" s="T144">ile-pilʼ</ta>
            <ta e="T146" id="Seg_2267" s="T145">qup</ta>
            <ta e="T147" id="Seg_2268" s="T146">suːrə</ta>
            <ta e="T148" id="Seg_2269" s="T147">močʼe</ta>
            <ta e="T149" id="Seg_2270" s="T148">am-qo</ta>
            <ta e="T150" id="Seg_2271" s="T149">irra</ta>
            <ta e="T151" id="Seg_2272" s="T150">ɨntə-p</ta>
            <ta e="T152" id="Seg_2273" s="T151">meː-ŋa-tə</ta>
            <ta e="T153" id="Seg_2274" s="T152">kəssa</ta>
            <ta e="T154" id="Seg_2275" s="T153">qəl-lɔː-qə-n</ta>
            <ta e="T155" id="Seg_2276" s="T154">tolʼčʼe-p</ta>
            <ta e="T156" id="Seg_2277" s="T155">meː-ŋa-tə</ta>
            <ta e="T157" id="Seg_2278" s="T156">na</ta>
            <ta e="T158" id="Seg_2279" s="T157">qup</ta>
            <ta e="T159" id="Seg_2280" s="T158">tolʼčʼi-sa</ta>
            <ta e="T160" id="Seg_2281" s="T159">qən-nä</ta>
            <ta e="T161" id="Seg_2282" s="T160">ɨnta-tä</ta>
            <ta e="T162" id="Seg_2283" s="T161">iː-ŋa-tə</ta>
            <ta e="T163" id="Seg_2284" s="T162">ont</ta>
            <ta e="T164" id="Seg_2285" s="T163">irra</ta>
            <ta e="T165" id="Seg_2286" s="T164">olqa</ta>
            <ta e="T166" id="Seg_2287" s="T165">qən-na</ta>
            <ta e="T167" id="Seg_2288" s="T166">tolʼčʼi-kɔːli-k</ta>
            <ta e="T168" id="Seg_2289" s="T167">üːta</ta>
            <ta e="T169" id="Seg_2290" s="T168">ukkur</ta>
            <ta e="T170" id="Seg_2291" s="T169">čʼontoː-qɨt</ta>
            <ta e="T171" id="Seg_2292" s="T170">qo-ŋa-tɨ</ta>
            <ta e="T172" id="Seg_2293" s="T171">ɔːta-n</ta>
            <ta e="T173" id="Seg_2294" s="T172">mətɨ-p</ta>
            <ta e="T174" id="Seg_2295" s="T173">nʼoː-tɨ-ŋɨ-tɨ</ta>
            <ta e="T175" id="Seg_2296" s="T174">nʼuː-tɨ-ŋɨ-tɨ</ta>
            <ta e="T176" id="Seg_2297" s="T175">pɨpa-ja</ta>
            <ta e="T177" id="Seg_2298" s="T176">nʼenna</ta>
            <ta e="T178" id="Seg_2299" s="T177">neš-olʼ-nɨ</ta>
            <ta e="T179" id="Seg_2300" s="T178">kəte-lʼ-lʼa</ta>
            <ta e="T180" id="Seg_2301" s="T179">ɔːta-p</ta>
            <ta e="T181" id="Seg_2302" s="T180">sai-sä</ta>
            <ta e="T182" id="Seg_2303" s="T181">qo-ŋa-tə</ta>
            <ta e="T183" id="Seg_2304" s="T182">ɨntɨ-sä</ta>
            <ta e="T184" id="Seg_2305" s="T183">čʼatɨ-ŋɨ-tɨ</ta>
            <ta e="T185" id="Seg_2306" s="T184">ukkor</ta>
            <ta e="T186" id="Seg_2307" s="T185">qoːrrɨ</ta>
            <ta e="T187" id="Seg_2308" s="T186">i̇llä</ta>
            <ta e="T188" id="Seg_2309" s="T187">omta</ta>
            <ta e="T189" id="Seg_2310" s="T188">moqon</ta>
            <ta e="T190" id="Seg_2311" s="T189">nɨːnä</ta>
            <ta e="T191" id="Seg_2312" s="T190">irra</ta>
            <ta e="T192" id="Seg_2313" s="T191">tü-ŋa</ta>
            <ta e="T193" id="Seg_2314" s="T192">mannɨ-mpa-tɨ</ta>
            <ta e="T194" id="Seg_2315" s="T193">qoːrɨ</ta>
            <ta e="T195" id="Seg_2316" s="T194">ippɨ-ntɨ</ta>
            <ta e="T196" id="Seg_2317" s="T195">pɨpa-ja</ta>
            <ta e="T197" id="Seg_2318" s="T196">ɔːmta</ta>
            <ta e="T198" id="Seg_2319" s="T197">nuːnɨčʼa</ta>
            <ta e="T199" id="Seg_2320" s="T198">irra</ta>
            <ta e="T200" id="Seg_2321" s="T199">nɨlʼčʼi-k</ta>
            <ta e="T201" id="Seg_2322" s="T200">kəto-ŋɨ-tɨ</ta>
            <ta e="T202" id="Seg_2323" s="T201">Opsä</ta>
            <ta e="T203" id="Seg_2324" s="T202">qo-ŋa-p</ta>
            <ta e="T204" id="Seg_2325" s="T203">moqonä</ta>
            <ta e="T205" id="Seg_2326" s="T204">qɨn-ta-k</ta>
            <ta e="T206" id="Seg_2327" s="T205">toː</ta>
            <ta e="T207" id="Seg_2328" s="T206">qən-nä</ta>
            <ta e="T208" id="Seg_2329" s="T207">utä-sä</ta>
            <ta e="T209" id="Seg_2330" s="T208">tüma</ta>
            <ta e="T210" id="Seg_2331" s="T209">sopɨ-r-nɨ-tɨ</ta>
            <ta e="T211" id="Seg_2332" s="T210">qoːrɨ-m-tɨ</ta>
            <ta e="T212" id="Seg_2333" s="T211">puːntɨ-sä</ta>
            <ta e="T213" id="Seg_2334" s="T212">čʼontoː-qɨn-tä</ta>
            <ta e="T214" id="Seg_2335" s="T213">parqolʼlʼ-ɛː-te</ta>
            <ta e="T215" id="Seg_2336" s="T214">kəː-qalʼ-lʼä</ta>
            <ta e="T216" id="Seg_2337" s="T215">moqonä</ta>
            <ta e="T217" id="Seg_2338" s="T216">kusa</ta>
            <ta e="T218" id="Seg_2339" s="T217">qəl-la-j</ta>
            <ta e="T219" id="Seg_2340" s="T218">moqona</ta>
            <ta e="T220" id="Seg_2341" s="T219">tüː-ŋa</ta>
            <ta e="T221" id="Seg_2342" s="T220">imaqota</ta>
            <ta e="T222" id="Seg_2343" s="T221">nɨlʼčʼi-k</ta>
            <ta e="T223" id="Seg_2344" s="T222">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T224" id="Seg_2345" s="T223">Opsä</ta>
            <ta e="T225" id="Seg_2346" s="T224">irra</ta>
            <ta e="T226" id="Seg_2347" s="T225">taːto-lʼä</ta>
            <ta e="T227" id="Seg_2348" s="T226">Soma</ta>
            <ta e="T228" id="Seg_2349" s="T227">pɨpa-ja</ta>
            <ta e="T229" id="Seg_2350" s="T228">ilʼe-mpɔː-tɨt</ta>
            <ta e="T230" id="Seg_2351" s="T229">nälʼa-n-tɨn</ta>
            <ta e="T231" id="Seg_2352" s="T230">mo-qa</ta>
            <ta e="T232" id="Seg_2353" s="T231">sɔːntɨr-lʼä</ta>
            <ta e="T233" id="Seg_2354" s="T232">ukkɨr</ta>
            <ta e="T234" id="Seg_2355" s="T233">ija</ta>
            <ta e="T235" id="Seg_2356" s="T234">qo-ŋa-tɨ</ta>
            <ta e="T236" id="Seg_2357" s="T235">ija-tɨ</ta>
            <ta e="T237" id="Seg_2358" s="T236">oro-m-pa</ta>
            <ta e="T238" id="Seg_2359" s="T237">ukkɨr</ta>
            <ta e="T239" id="Seg_2360" s="T238">čʼontoː-qɨt</ta>
            <ta e="T240" id="Seg_2361" s="T239">irra</ta>
            <ta e="T241" id="Seg_2362" s="T240">näja-n-nɨ</ta>
            <ta e="T242" id="Seg_2363" s="T241">mɨ-qı</ta>
            <ta e="T243" id="Seg_2364" s="T242">üːtä-lʼä-j</ta>
            <ta e="T244" id="Seg_2365" s="T243">moqɨnä-qat</ta>
            <ta e="T245" id="Seg_2366" s="T244">pɨpa-ja</ta>
            <ta e="T246" id="Seg_2367" s="T245">qum-iː-tə</ta>
            <ta e="T247" id="Seg_2368" s="T246">ətä-ltɔː-tät</ta>
            <ta e="T248" id="Seg_2369" s="T247">nälʼa-m-tɨ</ta>
            <ta e="T249" id="Seg_2370" s="T248">mi-ŋɔː-täː</ta>
            <ta e="T250" id="Seg_2371" s="T249">irra</ta>
            <ta e="T251" id="Seg_2372" s="T250">imaqota-j</ta>
            <ta e="T252" id="Seg_2373" s="T251">mɨ-qaːqə</ta>
            <ta e="T253" id="Seg_2374" s="T252">nälʼa-ntɨ-lʼ</ta>
            <ta e="T254" id="Seg_2375" s="T253">qən-nɔː-qə</ta>
            <ta e="T255" id="Seg_2376" s="T254">moqonä</ta>
            <ta e="T256" id="Seg_2377" s="T255">tɨmtä</ta>
            <ta e="T257" id="Seg_2378" s="T256">ilo-qo</ta>
            <ta e="T258" id="Seg_2379" s="T257">qoši-k</ta>
            <ta e="T259" id="Seg_2380" s="T258">ɛː-ja</ta>
            <ta e="T260" id="Seg_2381" s="T259">na</ta>
            <ta e="T261" id="Seg_2382" s="T260">pünakesa</ta>
            <ta e="T262" id="Seg_2383" s="T261">irra</ta>
            <ta e="T263" id="Seg_2384" s="T262">mɨ-qaːqə</ta>
            <ta e="T264" id="Seg_2385" s="T263">nälʼa-ntɨ</ta>
            <ta e="T265" id="Seg_2386" s="T264">mi-la</ta>
            <ta e="T266" id="Seg_2387" s="T265">üːtɨ-ŋɨ-tɨ</ta>
            <ta e="T267" id="Seg_2388" s="T266">ija-ntɨ-sä</ta>
            <ta e="T268" id="Seg_2389" s="T267">šölʼqum-ɨ-lʼ</ta>
            <ta e="T269" id="Seg_2390" s="T268">təto-ntə</ta>
            <ta e="T270" id="Seg_2391" s="T269">Tɔːs</ta>
            <ta e="T271" id="Seg_2392" s="T270">qəlto-ntə</ta>
            <ta e="T272" id="Seg_2393" s="T271">pünakəsa-lʼ</ta>
            <ta e="T273" id="Seg_2394" s="T272">nätäk</ta>
            <ta e="T274" id="Seg_2395" s="T273">čʼap</ta>
            <ta e="T275" id="Seg_2396" s="T274">tüː-ŋa</ta>
            <ta e="T276" id="Seg_2397" s="T275">ira-n-tə</ta>
            <ta e="T277" id="Seg_2398" s="T276">təto-ntə</ta>
            <ta e="T278" id="Seg_2399" s="T277">ile-qo</ta>
            <ta e="T279" id="Seg_2400" s="T278">qoši-k</ta>
            <ta e="T280" id="Seg_2401" s="T279">ɛː-ja</ta>
            <ta e="T281" id="Seg_2402" s="T280">säːqa-sɨ-k</ta>
            <ta e="T282" id="Seg_2403" s="T281">ɛː-ŋa</ta>
            <ta e="T283" id="Seg_2404" s="T282">ija-m-tə</ta>
            <ta e="T284" id="Seg_2405" s="T283">äsɨ-ntɨ-sa</ta>
            <ta e="T285" id="Seg_2406" s="T284">qəːučʼi-ŋɨ-tɨ</ta>
            <ta e="T286" id="Seg_2407" s="T285">Tɔːs</ta>
            <ta e="T287" id="Seg_2408" s="T286">qolto-ntə</ta>
            <ta e="T288" id="Seg_2409" s="T287">irra-n-tɨ</ta>
            <ta e="T289" id="Seg_2410" s="T288">təto-ntɨ</ta>
            <ta e="T290" id="Seg_2411" s="T289">šolʼqum-ɨ-n</ta>
            <ta e="T291" id="Seg_2412" s="T290">təto-ntɨ</ta>
            <ta e="T292" id="Seg_2413" s="T291">a</ta>
            <ta e="T293" id="Seg_2414" s="T292">mat</ta>
            <ta e="T294" id="Seg_2415" s="T293">moqona</ta>
            <ta e="T295" id="Seg_2416" s="T294">qən-ta-k</ta>
            <ta e="T296" id="Seg_2417" s="T295">pünakɨsa-j</ta>
            <ta e="T297" id="Seg_2418" s="T296">tətta-qaːq</ta>
            <ta e="T298" id="Seg_2419" s="T297">ira-m-tɨ</ta>
            <ta e="T299" id="Seg_2420" s="T298">ija-ntɨ-sä</ta>
            <ta e="T300" id="Seg_2421" s="T299">qoːčʼi-ɛː-ŋɨ-tɨ</ta>
            <ta e="T301" id="Seg_2422" s="T300">soma-k</ta>
            <ta e="T302" id="Seg_2423" s="T301">ilʼe-ŋɨlʼıː</ta>
            <ta e="T303" id="Seg_2424" s="T302">šolʼqup</ta>
            <ta e="T304" id="Seg_2425" s="T303">kɨpa</ta>
            <ta e="T305" id="Seg_2426" s="T304">käːtɨ-iɛː-ŋɔːtät</ta>
            <ta e="T306" id="Seg_2427" s="T305">pünakɨsa-lʼ</ta>
            <ta e="T307" id="Seg_2428" s="T306">nɨlʼčʼi-k</ta>
            <ta e="T308" id="Seg_2429" s="T307">kätɨ-mpa-tɨt</ta>
            <ta e="T309" id="Seg_2430" s="T308">šölʼqup-sa</ta>
            <ta e="T310" id="Seg_2431" s="T309">ukur-šak</ta>
            <ta e="T311" id="Seg_2432" s="T310">kəːtɨ-pɨlʼ</ta>
            <ta e="T312" id="Seg_2433" s="T311">iːja-mɨ</ta>
            <ta e="T313" id="Seg_2434" s="T312">ilʼi-ŋɨijäːtot</ta>
            <ta e="T314" id="Seg_2435" s="T313">worqɨ</ta>
            <ta e="T315" id="Seg_2436" s="T314">käpɨ-k</ta>
            <ta e="T316" id="Seg_2437" s="T315">ɛː-ŋa</ta>
            <ta e="T317" id="Seg_2438" s="T316">ije-tat</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2439" s="T0">qoškɔːl</ta>
            <ta e="T2" id="Seg_2440" s="T1">ilɨ-mpɨ</ta>
            <ta e="T3" id="Seg_2441" s="T2">ukkɨr</ta>
            <ta e="T4" id="Seg_2442" s="T3">qum</ta>
            <ta e="T5" id="Seg_2443" s="T4">*čʼilʼa-lʼ</ta>
            <ta e="T6" id="Seg_2444" s="T5">qum</ta>
            <ta e="T7" id="Seg_2445" s="T6">nʼi</ta>
            <ta e="T8" id="Seg_2446" s="T7">porqɨ-tɨ</ta>
            <ta e="T9" id="Seg_2447" s="T8">nʼi</ta>
            <ta e="T10" id="Seg_2448" s="T9">peːmɨ-tɨ</ta>
            <ta e="T11" id="Seg_2449" s="T10">čʼäːŋkɨ-sɨ</ta>
            <ta e="T12" id="Seg_2450" s="T11">nʼaŋɨčʼa</ta>
            <ta e="T13" id="Seg_2451" s="T12">ilɨ-mpɨ</ta>
            <ta e="T14" id="Seg_2452" s="T13">tɛnɨ-tɨ-mpɨ</ta>
            <ta e="T15" id="Seg_2453" s="T14">tɛnɨ-r-mpɨ</ta>
            <ta e="T16" id="Seg_2454" s="T15">kuttar</ta>
            <ta e="T17" id="Seg_2455" s="T16">nılʼčʼɨ-k</ta>
            <ta e="T18" id="Seg_2456" s="T17">ilɨ-ɛntɨ-k</ta>
            <ta e="T19" id="Seg_2457" s="T18">pɛlɨ-kɔːlɨ-k</ta>
            <ta e="T20" id="Seg_2458" s="T19">qən-sɨ-k</ta>
            <ta e="T21" id="Seg_2459" s="T20">ɛnä</ta>
            <ta e="T22" id="Seg_2460" s="T21">qən-sɨ-k</ta>
            <ta e="T23" id="Seg_2461" s="T22">ɛnä</ta>
            <ta e="T24" id="Seg_2462" s="T23">kun</ta>
            <ta e="T25" id="Seg_2463" s="T24">ɛːmä</ta>
            <ta e="T26" id="Seg_2464" s="T25">kučʼčʼä-k</ta>
            <ta e="T27" id="Seg_2465" s="T26">ɛːmä</ta>
            <ta e="T28" id="Seg_2466" s="T27">qum-ɨ-t</ta>
            <ta e="T29" id="Seg_2467" s="T28">qo-ɛntɨ-k</ta>
            <ta e="T30" id="Seg_2468" s="T29">qo-sɨ-k</ta>
            <ta e="T31" id="Seg_2469" s="T30">ɛnä</ta>
            <ta e="T32" id="Seg_2470" s="T31">ponä</ta>
            <ta e="T33" id="Seg_2471" s="T32">tantɨ</ta>
            <ta e="T34" id="Seg_2472" s="T33">qən-ŋɨ</ta>
            <ta e="T35" id="Seg_2473" s="T34">kučʼčʼä-k</ta>
            <ta e="T36" id="Seg_2474" s="T35">tɛnɨ-tɨ</ta>
            <ta e="T37" id="Seg_2475" s="T36">tuːtɨrɨ-mpɨ</ta>
            <ta e="T38" id="Seg_2476" s="T37">ukkɨr</ta>
            <ta e="T39" id="Seg_2477" s="T38">čʼontɨ-qɨn</ta>
            <ta e="T40" id="Seg_2478" s="T39">mantɨ-mpɨ-tɨ</ta>
            <ta e="T41" id="Seg_2479" s="T40">kɨ</ta>
            <ta e="T42" id="Seg_2480" s="T41">qontɨ</ta>
            <ta e="T43" id="Seg_2481" s="T42">qoltɨ</ta>
            <ta e="T44" id="Seg_2482" s="T43">atɨ</ta>
            <ta e="T45" id="Seg_2483" s="T44">näčʼčʼä</ta>
            <ta e="T46" id="Seg_2484" s="T45">panı-š-ŋɨ</ta>
            <ta e="T47" id="Seg_2485" s="T46">karrä</ta>
            <ta e="T48" id="Seg_2486" s="T47">čʼam</ta>
            <ta e="T49" id="Seg_2487" s="T48">panı-š-ŋɨ</ta>
            <ta e="T50" id="Seg_2488" s="T49">montɨ</ta>
            <ta e="T51" id="Seg_2489" s="T50">qoltɨ</ta>
            <ta e="T52" id="Seg_2490" s="T51">takkɨ</ta>
            <ta e="T53" id="Seg_2491" s="T52">mantɨ-mpɨ</ta>
            <ta e="T54" id="Seg_2492" s="T53">čʼumpɨ</ta>
            <ta e="T55" id="Seg_2493" s="T54">qoltɨ-n</ta>
            <ta e="T56" id="Seg_2494" s="T55">kəːlʼ</ta>
            <ta e="T57" id="Seg_2495" s="T56">ontɨ</ta>
            <ta e="T58" id="Seg_2496" s="T57">kınʼčʼɨ</ta>
            <ta e="T59" id="Seg_2497" s="T58">atɨ-ntɨ</ta>
            <ta e="T60" id="Seg_2498" s="T59">nɨː</ta>
            <ta e="T61" id="Seg_2499" s="T60">čʼam</ta>
            <ta e="T62" id="Seg_2500" s="T61">tü-ŋɨ-k</ta>
            <ta e="T63" id="Seg_2501" s="T62">muqqɨl-türɨ-lʼ</ta>
            <ta e="T64" id="Seg_2502" s="T63">poː-lʼ</ta>
            <ta e="T65" id="Seg_2503" s="T64">maːtɨ-r-tɨ</ta>
            <ta e="T66" id="Seg_2504" s="T65">üŋkɨ-t</ta>
            <ta e="T67" id="Seg_2505" s="T66">ɔːlɨ-k</ta>
            <ta e="T68" id="Seg_2506" s="T67">ora-čʼɨ-r-ɨ-ŋɨ-tɨt</ta>
            <ta e="T69" id="Seg_2507" s="T68">wəttɨ</ta>
            <ta e="T70" id="Seg_2508" s="T69">qo-ŋɨ-k</ta>
            <ta e="T71" id="Seg_2509" s="T70">montɨ</ta>
            <ta e="T72" id="Seg_2510" s="T71">šittɨ</ta>
            <ta e="T73" id="Seg_2511" s="T72">topɨ-lʼ</ta>
            <ta e="T74" id="Seg_2512" s="T73">wərqɨ</ta>
            <ta e="T75" id="Seg_2513" s="T74">ɛː-ŋɨ</ta>
            <ta e="T76" id="Seg_2514" s="T75">wəttɨ-tɨ</ta>
            <ta e="T77" id="Seg_2515" s="T76">nɨmtɨ</ta>
            <ta e="T78" id="Seg_2516" s="T77">mortä-n</ta>
            <ta e="T79" id="Seg_2517" s="T78">qəːlɨ</ta>
            <ta e="T80" id="Seg_2518" s="T79">uː-ltɨ-mpɨ-tɨt</ta>
            <ta e="T81" id="Seg_2519" s="T80">mortä-qɨn</ta>
            <ta e="T82" id="Seg_2520" s="T81">kınʼčʼɨ-n</ta>
            <ta e="T83" id="Seg_2521" s="T82">mortä-n</ta>
            <ta e="T84" id="Seg_2522" s="T83">ıllä</ta>
            <ta e="T85" id="Seg_2523" s="T84">mantɨ-mpɨ</ta>
            <ta e="T86" id="Seg_2524" s="T85">opčʼin</ta>
            <ta e="T87" id="Seg_2525" s="T86">wərqɨ</ta>
            <ta e="T88" id="Seg_2526" s="T87">wərqɨ</ta>
            <ta e="T89" id="Seg_2527" s="T88">opčʼin</ta>
            <ta e="T90" id="Seg_2528" s="T89">takkɨ</ta>
            <ta e="T91" id="Seg_2529" s="T90">mantɨ-mpɨ-k</ta>
            <ta e="T92" id="Seg_2530" s="T91">qaj</ta>
            <ta e="T93" id="Seg_2531" s="T92">lоːsɨ</ta>
            <ta e="T94" id="Seg_2532" s="T93">tü-ntɨ</ta>
            <ta e="T95" id="Seg_2533" s="T94">nɨrkɨ-mɔːt-ŋɨ</ta>
            <ta e="T96" id="Seg_2534" s="T95">ukoːn</ta>
            <ta e="T97" id="Seg_2535" s="T96">tü-sɨ-k</ta>
            <ta e="T98" id="Seg_2536" s="T97">wərkɨ-lä-k</ta>
            <ta e="T99" id="Seg_2537" s="T98">čʼɔːlsä</ta>
            <ta e="T100" id="Seg_2538" s="T99">mašım</ta>
            <ta e="T101" id="Seg_2539" s="T100">am-lä</ta>
            <ta e="T102" id="Seg_2540" s="T101">wərkɨ-k</ta>
            <ta e="T103" id="Seg_2541" s="T102">nɨmtɨ</ta>
            <ta e="T104" id="Seg_2542" s="T103">na</ta>
            <ta e="T105" id="Seg_2543" s="T104">tü-ntɨ</ta>
            <ta e="T106" id="Seg_2544" s="T105">nılʼčʼɨ-k</ta>
            <ta e="T107" id="Seg_2545" s="T106">mašım</ta>
            <ta e="T108" id="Seg_2546" s="T107">qo-ŋɨ</ta>
            <ta e="T109" id="Seg_2547" s="T108">pɨpa-ja</ta>
            <ta e="T110" id="Seg_2548" s="T109">nälʼa-mɨ</ta>
            <ta e="T111" id="Seg_2549" s="T110">sɔːntɨr-ɛntɨ</ta>
            <ta e="T112" id="Seg_2550" s="T111">qɔːtɨ</ta>
            <ta e="T113" id="Seg_2551" s="T112">qən-tɨ-sɨ-m</ta>
            <ta e="T114" id="Seg_2552" s="T113">ɛnä</ta>
            <ta e="T115" id="Seg_2553" s="T114">nopɨ-qɨn-ntɨ</ta>
            <ta e="T116" id="Seg_2554" s="T115">omtɨ-tɨ-kkɨ-tɨ</ta>
            <ta e="T117" id="Seg_2555" s="T116">moqɨnä</ta>
            <ta e="T118" id="Seg_2556" s="T117">qən-tɨ-kkɨ-tɨ</ta>
            <ta e="T119" id="Seg_2557" s="T118">a</ta>
            <ta e="T120" id="Seg_2558" s="T119">qum</ta>
            <ta e="T121" id="Seg_2559" s="T120">nopɨ-qɨn</ta>
            <ta e="T122" id="Seg_2560" s="T121">ɔːmtɨ</ta>
            <ta e="T123" id="Seg_2561" s="T122">mantɨ-mpɨ-tɨ</ta>
            <ta e="T124" id="Seg_2562" s="T123">konnä</ta>
            <ta e="T125" id="Seg_2563" s="T124">wərqɨ</ta>
            <ta e="T126" id="Seg_2564" s="T125">mɔːt</ta>
            <ta e="T127" id="Seg_2565" s="T126">ɔːmtɨ-ntɨ</ta>
            <ta e="T128" id="Seg_2566" s="T127">tü-ŋɨ</ta>
            <ta e="T129" id="Seg_2567" s="T128">nɨː</ta>
            <ta e="T130" id="Seg_2568" s="T129">mɔːt-ntɨ</ta>
            <ta e="T131" id="Seg_2569" s="T130">tuː-ltɨ-ŋɨ-tɨ</ta>
            <ta e="T132" id="Seg_2570" s="T131">nälʼa</ta>
            <ta e="T133" id="Seg_2571" s="T132">qo-ŋɨ-k</ta>
            <ta e="T134" id="Seg_2572" s="T133">pɨpa-ja-m</ta>
            <ta e="T135" id="Seg_2573" s="T134">sɔːntɨr-äšɨk</ta>
            <ta e="T136" id="Seg_2574" s="T135">ilɨ-mpɨ-tɨt</ta>
            <ta e="T137" id="Seg_2575" s="T136">imaqota-tɨ</ta>
            <ta e="T138" id="Seg_2576" s="T137">nılʼčʼɨ-k</ta>
            <ta e="T139" id="Seg_2577" s="T138">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T140" id="Seg_2578" s="T139">ira</ta>
            <ta e="T141" id="Seg_2579" s="T140">ɨntɨ</ta>
            <ta e="T142" id="Seg_2580" s="T141">meː-äšɨk</ta>
            <ta e="T143" id="Seg_2581" s="T142">ukoːn</ta>
            <ta e="T144" id="Seg_2582" s="T143">ontɨ</ta>
            <ta e="T145" id="Seg_2583" s="T144">ilɨ-mpɨlʼ</ta>
            <ta e="T146" id="Seg_2584" s="T145">qum</ta>
            <ta e="T147" id="Seg_2585" s="T146">suːrɨm</ta>
            <ta e="T148" id="Seg_2586" s="T147">wəčʼɨ</ta>
            <ta e="T149" id="Seg_2587" s="T148">am-qo</ta>
            <ta e="T150" id="Seg_2588" s="T149">ira</ta>
            <ta e="T151" id="Seg_2589" s="T150">ɨntɨ-m</ta>
            <ta e="T152" id="Seg_2590" s="T151">meː-ŋɨ-tɨ</ta>
            <ta e="T153" id="Seg_2591" s="T152">kɨssa</ta>
            <ta e="T154" id="Seg_2592" s="T153">qən-lä-qı-naj</ta>
            <ta e="T155" id="Seg_2593" s="T154">tolʼčʼɨ-m</ta>
            <ta e="T156" id="Seg_2594" s="T155">meː-ŋɨ-tɨ</ta>
            <ta e="T157" id="Seg_2595" s="T156">na</ta>
            <ta e="T158" id="Seg_2596" s="T157">qum</ta>
            <ta e="T159" id="Seg_2597" s="T158">tolʼčʼɨ-sä</ta>
            <ta e="T160" id="Seg_2598" s="T159">qən-ŋɨ</ta>
            <ta e="T161" id="Seg_2599" s="T160">ɨntɨ-tɨ</ta>
            <ta e="T162" id="Seg_2600" s="T161">iː-ŋɨ-tɨ</ta>
            <ta e="T163" id="Seg_2601" s="T162">ontɨ</ta>
            <ta e="T164" id="Seg_2602" s="T163">ira</ta>
            <ta e="T165" id="Seg_2603" s="T164">olqa</ta>
            <ta e="T166" id="Seg_2604" s="T165">qən-ŋɨ</ta>
            <ta e="T167" id="Seg_2605" s="T166">tolʼčʼɨ-kɔːlɨ-k</ta>
            <ta e="T168" id="Seg_2606" s="T167">üːtɨ</ta>
            <ta e="T169" id="Seg_2607" s="T168">ukkɨr</ta>
            <ta e="T170" id="Seg_2608" s="T169">čʼontɨ-qɨn</ta>
            <ta e="T171" id="Seg_2609" s="T170">qo-ŋɨ-tɨ</ta>
            <ta e="T172" id="Seg_2610" s="T171">ɔːtä-n</ta>
            <ta e="T173" id="Seg_2611" s="T172">wəttɨ-m</ta>
            <ta e="T174" id="Seg_2612" s="T173">nʼoː-tɨ-ŋɨ-tɨ</ta>
            <ta e="T175" id="Seg_2613" s="T174">nʼoː-tɨ-ŋɨ-tɨ</ta>
            <ta e="T176" id="Seg_2614" s="T175">pɨpa-ja</ta>
            <ta e="T177" id="Seg_2615" s="T176">nʼennä</ta>
            <ta e="T178" id="Seg_2616" s="T177">*nʼeš-olʼ-ŋɨ</ta>
            <ta e="T179" id="Seg_2617" s="T178">kətɨ-š-lä</ta>
            <ta e="T180" id="Seg_2618" s="T179">ɔːtä-m</ta>
            <ta e="T181" id="Seg_2619" s="T180">sajɨ-sä</ta>
            <ta e="T182" id="Seg_2620" s="T181">qo-ŋɨ-tɨ</ta>
            <ta e="T183" id="Seg_2621" s="T182">ɨntɨ-sä</ta>
            <ta e="T184" id="Seg_2622" s="T183">čʼattɨ-ŋɨ-tɨ</ta>
            <ta e="T185" id="Seg_2623" s="T184">ukkɨr</ta>
            <ta e="T186" id="Seg_2624" s="T185">qoːrɨ</ta>
            <ta e="T187" id="Seg_2625" s="T186">ıllä</ta>
            <ta e="T188" id="Seg_2626" s="T187">omtɨ</ta>
            <ta e="T189" id="Seg_2627" s="T188">moqɨnä</ta>
            <ta e="T190" id="Seg_2628" s="T189">nɨːnɨ</ta>
            <ta e="T191" id="Seg_2629" s="T190">ira</ta>
            <ta e="T192" id="Seg_2630" s="T191">tü-ŋɨ</ta>
            <ta e="T193" id="Seg_2631" s="T192">mantɨ-mpɨ-tɨ</ta>
            <ta e="T194" id="Seg_2632" s="T193">qoːrɨ</ta>
            <ta e="T195" id="Seg_2633" s="T194">ippɨ-ntɨ</ta>
            <ta e="T196" id="Seg_2634" s="T195">pɨpa-ja</ta>
            <ta e="T197" id="Seg_2635" s="T196">ɔːmtɨ</ta>
            <ta e="T198" id="Seg_2636" s="T197">nuːnɨčʼɨ</ta>
            <ta e="T199" id="Seg_2637" s="T198">ira</ta>
            <ta e="T200" id="Seg_2638" s="T199">nılʼčʼɨ-k</ta>
            <ta e="T201" id="Seg_2639" s="T200">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T202" id="Seg_2640" s="T201">Opsä</ta>
            <ta e="T203" id="Seg_2641" s="T202">qo-ŋɨ-m</ta>
            <ta e="T204" id="Seg_2642" s="T203">moqɨnä</ta>
            <ta e="T205" id="Seg_2643" s="T204">qən-tɨ-k</ta>
            <ta e="T206" id="Seg_2644" s="T205">toː</ta>
            <ta e="T207" id="Seg_2645" s="T206">qən-ŋɨ</ta>
            <ta e="T208" id="Seg_2646" s="T207">utɨ-sä</ta>
            <ta e="T209" id="Seg_2647" s="T208">tümɨ</ta>
            <ta e="T210" id="Seg_2648" s="T209">səpɨ-r-ŋɨ-tɨ</ta>
            <ta e="T211" id="Seg_2649" s="T210">qoːrɨ-m-tɨ</ta>
            <ta e="T212" id="Seg_2650" s="T211">poːntɨ-sä</ta>
            <ta e="T213" id="Seg_2651" s="T212">čʼontɨ-qɨn-ntɨ</ta>
            <ta e="T214" id="Seg_2652" s="T213">pärqɨl-ɛː-tɨ</ta>
            <ta e="T215" id="Seg_2653" s="T214">kəː-qɨl-lä</ta>
            <ta e="T216" id="Seg_2654" s="T215">moqɨnä</ta>
            <ta e="T217" id="Seg_2655" s="T216">kɨssa</ta>
            <ta e="T218" id="Seg_2656" s="T217">qən-lä-j</ta>
            <ta e="T219" id="Seg_2657" s="T218">moqɨnä</ta>
            <ta e="T220" id="Seg_2658" s="T219">tü-ŋɨ</ta>
            <ta e="T221" id="Seg_2659" s="T220">imaqota</ta>
            <ta e="T222" id="Seg_2660" s="T221">nılʼčʼɨ-k</ta>
            <ta e="T223" id="Seg_2661" s="T222">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T224" id="Seg_2662" s="T223">Opsä</ta>
            <ta e="T225" id="Seg_2663" s="T224">ira</ta>
            <ta e="T226" id="Seg_2664" s="T225">taːtɨ-lä</ta>
            <ta e="T227" id="Seg_2665" s="T226">soma</ta>
            <ta e="T228" id="Seg_2666" s="T227">pɨpa-ja</ta>
            <ta e="T229" id="Seg_2667" s="T228">ilɨ-mpɨ-tɨt</ta>
            <ta e="T230" id="Seg_2668" s="T229">nälʼa-n-tɨt</ta>
            <ta e="T231" id="Seg_2669" s="T230">mɨ-qı</ta>
            <ta e="T232" id="Seg_2670" s="T231">sɔːntɨr-lä</ta>
            <ta e="T233" id="Seg_2671" s="T232">ukkɨr</ta>
            <ta e="T234" id="Seg_2672" s="T233">iːja</ta>
            <ta e="T235" id="Seg_2673" s="T234">qo-ŋɨ-tɨ</ta>
            <ta e="T236" id="Seg_2674" s="T235">iːja-tɨ</ta>
            <ta e="T237" id="Seg_2675" s="T236">orɨ-m-mpɨ</ta>
            <ta e="T238" id="Seg_2676" s="T237">ukkɨr</ta>
            <ta e="T239" id="Seg_2677" s="T238">čʼontɨ-qɨn</ta>
            <ta e="T240" id="Seg_2678" s="T239">ira</ta>
            <ta e="T241" id="Seg_2679" s="T240">nälʼa-n-nɨ</ta>
            <ta e="T242" id="Seg_2680" s="T241">mɨ-qı</ta>
            <ta e="T243" id="Seg_2681" s="T242">üːtɨ-lä-j</ta>
            <ta e="T244" id="Seg_2682" s="T243">moqɨnä-qɨn</ta>
            <ta e="T245" id="Seg_2683" s="T244">pɨpa-ja</ta>
            <ta e="T246" id="Seg_2684" s="T245">qum-iː-tɨ</ta>
            <ta e="T247" id="Seg_2685" s="T246">ətɨ-ltɨ-tɨt</ta>
            <ta e="T248" id="Seg_2686" s="T247">nälʼa-m-tɨ</ta>
            <ta e="T249" id="Seg_2687" s="T248">mi-ŋɨ-tıː</ta>
            <ta e="T250" id="Seg_2688" s="T249">ira</ta>
            <ta e="T251" id="Seg_2689" s="T250">imaqota-lʼ</ta>
            <ta e="T252" id="Seg_2690" s="T251">mɨ-qı</ta>
            <ta e="T253" id="Seg_2691" s="T252">nälʼa-ntɨ-lʼ</ta>
            <ta e="T254" id="Seg_2692" s="T253">qən-ŋɨ-qı</ta>
            <ta e="T255" id="Seg_2693" s="T254">moqɨnä</ta>
            <ta e="T256" id="Seg_2694" s="T255">tɨmtɨ</ta>
            <ta e="T257" id="Seg_2695" s="T256">ilɨ-qo</ta>
            <ta e="T258" id="Seg_2696" s="T257">*qoš-k</ta>
            <ta e="T259" id="Seg_2697" s="T258">ɛː-ŋɨ</ta>
            <ta e="T260" id="Seg_2698" s="T259">na</ta>
            <ta e="T261" id="Seg_2699" s="T260">pünakɨsa</ta>
            <ta e="T262" id="Seg_2700" s="T261">ira</ta>
            <ta e="T263" id="Seg_2701" s="T262">mɨ-qı</ta>
            <ta e="T264" id="Seg_2702" s="T263">nälʼa-ntɨ</ta>
            <ta e="T265" id="Seg_2703" s="T264">mi-lä</ta>
            <ta e="T266" id="Seg_2704" s="T265">üːtɨ-ŋɨ-tɨ</ta>
            <ta e="T267" id="Seg_2705" s="T266">iːja-ntɨ-sä</ta>
            <ta e="T268" id="Seg_2706" s="T267">šölʼqum-ɨ-lʼ</ta>
            <ta e="T269" id="Seg_2707" s="T268">təttɨ-ntɨ</ta>
            <ta e="T270" id="Seg_2708" s="T269">Tɔːs</ta>
            <ta e="T271" id="Seg_2709" s="T270">qoltɨ-ntɨ</ta>
            <ta e="T272" id="Seg_2710" s="T271">pünakɨsa-lʼ</ta>
            <ta e="T273" id="Seg_2711" s="T272">nätäk</ta>
            <ta e="T274" id="Seg_2712" s="T273">čʼam</ta>
            <ta e="T275" id="Seg_2713" s="T274">tü-ŋɨ</ta>
            <ta e="T276" id="Seg_2714" s="T275">ira-n-tɨ</ta>
            <ta e="T277" id="Seg_2715" s="T276">təttɨ-ntɨ</ta>
            <ta e="T278" id="Seg_2716" s="T277">ilɨ-qo</ta>
            <ta e="T279" id="Seg_2717" s="T278">*qoš-k</ta>
            <ta e="T280" id="Seg_2718" s="T279">ɛː-ŋɨ</ta>
            <ta e="T281" id="Seg_2719" s="T280">säːqɨ-sä-k</ta>
            <ta e="T282" id="Seg_2720" s="T281">ɛː-ŋɨ</ta>
            <ta e="T283" id="Seg_2721" s="T282">iːja-m-tɨ</ta>
            <ta e="T284" id="Seg_2722" s="T283">əsɨ-ntɨ-sä</ta>
            <ta e="T285" id="Seg_2723" s="T284">qəːčʼɨ-ŋɨ-tɨ</ta>
            <ta e="T286" id="Seg_2724" s="T285">Tɔːs</ta>
            <ta e="T287" id="Seg_2725" s="T286">qoltɨ-ntɨ</ta>
            <ta e="T288" id="Seg_2726" s="T287">ira-n-tɨ</ta>
            <ta e="T289" id="Seg_2727" s="T288">təttɨ-ntɨ</ta>
            <ta e="T290" id="Seg_2728" s="T289">šölʼqum-ɨ-n</ta>
            <ta e="T291" id="Seg_2729" s="T290">təttɨ-ntɨ</ta>
            <ta e="T292" id="Seg_2730" s="T291">a</ta>
            <ta e="T293" id="Seg_2731" s="T292">man</ta>
            <ta e="T294" id="Seg_2732" s="T293">moqɨnä</ta>
            <ta e="T295" id="Seg_2733" s="T294">qən-ɛntɨ-k</ta>
            <ta e="T296" id="Seg_2734" s="T295">pünakɨsa-lʼ</ta>
            <ta e="T297" id="Seg_2735" s="T296">tətta-qı</ta>
            <ta e="T298" id="Seg_2736" s="T297">ira-m-tɨ</ta>
            <ta e="T299" id="Seg_2737" s="T298">iːja-ntɨ-sä</ta>
            <ta e="T300" id="Seg_2738" s="T299">qəːčʼɨ-ɛː-ŋɨ-tɨ</ta>
            <ta e="T301" id="Seg_2739" s="T300">soma-k</ta>
            <ta e="T302" id="Seg_2740" s="T301">ilɨ-ŋɨlıː</ta>
            <ta e="T303" id="Seg_2741" s="T302">šölʼqum</ta>
            <ta e="T304" id="Seg_2742" s="T303">kɨpa</ta>
            <ta e="T305" id="Seg_2743" s="T304">kəːtɨ-ɛː-ŋɔːtɨ</ta>
            <ta e="T306" id="Seg_2744" s="T305">pünakɨsa-lʼ</ta>
            <ta e="T307" id="Seg_2745" s="T306">nılʼčʼɨ-k</ta>
            <ta e="T308" id="Seg_2746" s="T307">kətɨ-mpɨ-tɨt</ta>
            <ta e="T309" id="Seg_2747" s="T308">šölʼqum-sä</ta>
            <ta e="T310" id="Seg_2748" s="T309">ukkɨr-ššak</ta>
            <ta e="T311" id="Seg_2749" s="T310">kəːtɨ-mpɨlʼ</ta>
            <ta e="T312" id="Seg_2750" s="T311">iːja-mɨ</ta>
            <ta e="T313" id="Seg_2751" s="T312">ilɨ-ŋɨjäːtɨt</ta>
            <ta e="T314" id="Seg_2752" s="T313">wərqɨ</ta>
            <ta e="T315" id="Seg_2753" s="T314">kəpɨ-ka</ta>
            <ta e="T316" id="Seg_2754" s="T315">ɛː-ŋɨ</ta>
            <ta e="T317" id="Seg_2755" s="T316">iːja-tɨt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2756" s="T0">badly</ta>
            <ta e="T2" id="Seg_2757" s="T1">live-PST.NAR.[3SG.S]</ta>
            <ta e="T3" id="Seg_2758" s="T2">one</ta>
            <ta e="T4" id="Seg_2759" s="T3">human.being.[NOM]</ta>
            <ta e="T5" id="Seg_2760" s="T4">orphan-ADJZ</ta>
            <ta e="T6" id="Seg_2761" s="T5">human.being.[NOM]</ta>
            <ta e="T7" id="Seg_2762" s="T6">neither…nor</ta>
            <ta e="T8" id="Seg_2763" s="T7">clothing.[NOM]-3SG</ta>
            <ta e="T9" id="Seg_2764" s="T8">neither…nor</ta>
            <ta e="T10" id="Seg_2765" s="T9">footwear.[NOM]-3SG</ta>
            <ta e="T11" id="Seg_2766" s="T10">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T12" id="Seg_2767" s="T11">nude</ta>
            <ta e="T13" id="Seg_2768" s="T12">live-PST.NAR.[3SG.S]</ta>
            <ta e="T14" id="Seg_2769" s="T13">think-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T15" id="Seg_2770" s="T14">think-FRQ-PST.NAR.[3SG.S]</ta>
            <ta e="T16" id="Seg_2771" s="T15">how</ta>
            <ta e="T17" id="Seg_2772" s="T16">such-ADVZ</ta>
            <ta e="T18" id="Seg_2773" s="T17">live-FUT-1SG.S</ta>
            <ta e="T19" id="Seg_2774" s="T18">friend-CAR-ADVZ</ta>
            <ta e="T20" id="Seg_2775" s="T19">go.away-PST-1SG.S</ta>
            <ta e="T21" id="Seg_2776" s="T20">CONJ</ta>
            <ta e="T22" id="Seg_2777" s="T21">leave-PST-1SG.S</ta>
            <ta e="T23" id="Seg_2778" s="T22">CONJ</ta>
            <ta e="T24" id="Seg_2779" s="T23">where</ta>
            <ta e="T25" id="Seg_2780" s="T24">INDEF1</ta>
            <ta e="T26" id="Seg_2781" s="T25">where-ADVZ</ta>
            <ta e="T27" id="Seg_2782" s="T26">INDEF1</ta>
            <ta e="T28" id="Seg_2783" s="T27">human.being-EP-PL.[NOM]</ta>
            <ta e="T29" id="Seg_2784" s="T28">find-FUT-1SG.S</ta>
            <ta e="T30" id="Seg_2785" s="T29">sight-PST-1SG.S</ta>
            <ta e="T31" id="Seg_2786" s="T30">CONJ</ta>
            <ta e="T32" id="Seg_2787" s="T31">outwards</ta>
            <ta e="T33" id="Seg_2788" s="T32">go.out.[3SG.S]</ta>
            <ta e="T34" id="Seg_2789" s="T33">leave-CO.[3SG.S]</ta>
            <ta e="T35" id="Seg_2790" s="T34">where-ADVZ</ta>
            <ta e="T36" id="Seg_2791" s="T35">mind.[NOM]-3SG</ta>
            <ta e="T37" id="Seg_2792" s="T36">get-HAB.[3SG.S]</ta>
            <ta e="T38" id="Seg_2793" s="T37">one</ta>
            <ta e="T39" id="Seg_2794" s="T38">middle-LOC</ta>
            <ta e="T40" id="Seg_2795" s="T39">give.a.look-HAB-3SG.O</ta>
            <ta e="T41" id="Seg_2796" s="T40">river.[NOM]</ta>
            <ta e="T42" id="Seg_2797" s="T41">appear.[3SG.S]</ta>
            <ta e="T43" id="Seg_2798" s="T42">big.river.[NOM]</ta>
            <ta e="T44" id="Seg_2799" s="T43">be.visible.[3SG.S]</ta>
            <ta e="T45" id="Seg_2800" s="T44">there</ta>
            <ta e="T46" id="Seg_2801" s="T45">come.down-US-CO.[3SG.S]</ta>
            <ta e="T47" id="Seg_2802" s="T46">down</ta>
            <ta e="T48" id="Seg_2803" s="T47">hardly</ta>
            <ta e="T49" id="Seg_2804" s="T48">come.down-US-CO.[3SG.S]</ta>
            <ta e="T50" id="Seg_2805" s="T49">apparently</ta>
            <ta e="T51" id="Seg_2806" s="T50">big.river.[NOM]</ta>
            <ta e="T52" id="Seg_2807" s="T51">down.the.river</ta>
            <ta e="T53" id="Seg_2808" s="T52">give.a.look-HAB.[3SG.S]</ta>
            <ta e="T54" id="Seg_2809" s="T53">long</ta>
            <ta e="T55" id="Seg_2810" s="T54">big.river-GEN</ta>
            <ta e="T56" id="Seg_2811" s="T55">reach.[NOM]</ta>
            <ta e="T57" id="Seg_2812" s="T56">himself.[NOM]</ta>
            <ta e="T58" id="Seg_2813" s="T57">bolt.[NOM]</ta>
            <ta e="T59" id="Seg_2814" s="T58">be.visible-INFER.[3SG.S]</ta>
            <ta e="T60" id="Seg_2815" s="T59">there</ta>
            <ta e="T61" id="Seg_2816" s="T60">hardly</ta>
            <ta e="T62" id="Seg_2817" s="T61">come-CO-1SG.S</ta>
            <ta e="T63" id="Seg_2818" s="T62">knot-stick-ADJZ</ta>
            <ta e="T64" id="Seg_2819" s="T63">tree-ADJZ</ta>
            <ta e="T65" id="Seg_2820" s="T64">cut-FRQ-3SG.O</ta>
            <ta e="T66" id="Seg_2821" s="T65">sticks.for.making.up.fishing.bolts-PL.[NOM]</ta>
            <ta e="T67" id="Seg_2822" s="T66">weak-ADVZ</ta>
            <ta e="T68" id="Seg_2823" s="T67">catch-RFL-FRQ-EP-CO-3PL</ta>
            <ta e="T69" id="Seg_2824" s="T68">trace.[NOM]</ta>
            <ta e="T70" id="Seg_2825" s="T69">sight-CO-1SG.S</ta>
            <ta e="T71" id="Seg_2826" s="T70">apparently</ta>
            <ta e="T72" id="Seg_2827" s="T71">two</ta>
            <ta e="T73" id="Seg_2828" s="T72">leg-ADJZ</ta>
            <ta e="T74" id="Seg_2829" s="T73">big</ta>
            <ta e="T75" id="Seg_2830" s="T74">be-CO.[3SG.S]</ta>
            <ta e="T76" id="Seg_2831" s="T75">trace.[NOM]-3SG</ta>
            <ta e="T77" id="Seg_2832" s="T76">here</ta>
            <ta e="T78" id="Seg_2833" s="T77">bailer.for.bailing.out.fish-GEN</ta>
            <ta e="T79" id="Seg_2834" s="T78">fish.[NOM]</ta>
            <ta e="T80" id="Seg_2835" s="T79">swim-TR-HAB-3PL</ta>
            <ta e="T81" id="Seg_2836" s="T80">bailer.for.bailing.out.fish-LOC</ta>
            <ta e="T82" id="Seg_2837" s="T81">bolt-GEN</ta>
            <ta e="T83" id="Seg_2838" s="T82">bailer.for.bailing.out.fish-GEN</ta>
            <ta e="T84" id="Seg_2839" s="T83">down</ta>
            <ta e="T85" id="Seg_2840" s="T84">give.a.look-HAB.[3SG.S]</ta>
            <ta e="T86" id="Seg_2841" s="T85">bailer.for.bailing.out.fish.[NOM]</ta>
            <ta e="T87" id="Seg_2842" s="T86">big</ta>
            <ta e="T88" id="Seg_2843" s="T87">big</ta>
            <ta e="T89" id="Seg_2844" s="T88">bailer.for.bailing.out.fish.[NOM]</ta>
            <ta e="T90" id="Seg_2845" s="T89">down.the.river</ta>
            <ta e="T91" id="Seg_2846" s="T90">give.a.look-HAB-1SG.S</ta>
            <ta e="T92" id="Seg_2847" s="T91">whether</ta>
            <ta e="T93" id="Seg_2848" s="T92">devil.[NOM]</ta>
            <ta e="T94" id="Seg_2849" s="T93">come-INFER.[3SG.S]</ta>
            <ta e="T95" id="Seg_2850" s="T94">be.frightened-DECAUS-CO.[3SG.S]</ta>
            <ta e="T96" id="Seg_2851" s="T95">earlier</ta>
            <ta e="T97" id="Seg_2852" s="T96">come-PST-1SG.S</ta>
            <ta e="T98" id="Seg_2853" s="T97">live-OPT-1SG.S</ta>
            <ta e="T99" id="Seg_2854" s="T98">if</ta>
            <ta e="T100" id="Seg_2855" s="T99">I.ACC</ta>
            <ta e="T101" id="Seg_2856" s="T100">eat-CVB</ta>
            <ta e="T102" id="Seg_2857" s="T101">live-1SG.S</ta>
            <ta e="T103" id="Seg_2858" s="T102">here</ta>
            <ta e="T104" id="Seg_2859" s="T103">INFER</ta>
            <ta e="T105" id="Seg_2860" s="T104">come-INFER.[3SG.S]</ta>
            <ta e="T106" id="Seg_2861" s="T105">such-ADVZ</ta>
            <ta e="T107" id="Seg_2862" s="T106">I.ACC</ta>
            <ta e="T108" id="Seg_2863" s="T107">find-CO.[3SG.S]</ta>
            <ta e="T109" id="Seg_2864" s="T108">doll-DIM.[NOM]</ta>
            <ta e="T110" id="Seg_2865" s="T109">daughter.[NOM]-1SG</ta>
            <ta e="T111" id="Seg_2866" s="T110">play-FUT.[3SG.S]</ta>
            <ta e="T112" id="Seg_2867" s="T111">probably</ta>
            <ta e="T113" id="Seg_2868" s="T112">leave-TR-PST-1SG.O</ta>
            <ta e="T114" id="Seg_2869" s="T113">CONJ</ta>
            <ta e="T115" id="Seg_2870" s="T114">mitten-ILL-OBL.3SG</ta>
            <ta e="T116" id="Seg_2871" s="T115">sit.down-TR-DUR-3SG.O</ta>
            <ta e="T117" id="Seg_2872" s="T116">home</ta>
            <ta e="T118" id="Seg_2873" s="T117">leave-TR-DUR-3SG.O</ta>
            <ta e="T119" id="Seg_2874" s="T118">and</ta>
            <ta e="T120" id="Seg_2875" s="T119">human.being.[NOM]</ta>
            <ta e="T121" id="Seg_2876" s="T120">mitten-LOC</ta>
            <ta e="T122" id="Seg_2877" s="T121">sit.[3SG.S]</ta>
            <ta e="T123" id="Seg_2878" s="T122">give.a.look-HAB-3SG.O</ta>
            <ta e="T124" id="Seg_2879" s="T123">upwards</ta>
            <ta e="T125" id="Seg_2880" s="T124">big</ta>
            <ta e="T126" id="Seg_2881" s="T125">tent.[NOM]</ta>
            <ta e="T127" id="Seg_2882" s="T126">stand-INFER.[3SG.S]</ta>
            <ta e="T128" id="Seg_2883" s="T127">come-CO.[3SG.S]</ta>
            <ta e="T129" id="Seg_2884" s="T128">there</ta>
            <ta e="T130" id="Seg_2885" s="T129">tent-ILL</ta>
            <ta e="T131" id="Seg_2886" s="T130">carry-TR-CO-3SG.O</ta>
            <ta e="T132" id="Seg_2887" s="T131">daughter.[NOM]</ta>
            <ta e="T133" id="Seg_2888" s="T132">find-CO-1SG.S</ta>
            <ta e="T134" id="Seg_2889" s="T133">doll-DIM-ACC</ta>
            <ta e="T135" id="Seg_2890" s="T134">play-IMP.2SG.S</ta>
            <ta e="T136" id="Seg_2891" s="T135">live-HAB-3PL</ta>
            <ta e="T137" id="Seg_2892" s="T136">old.woman.[NOM]-3SG</ta>
            <ta e="T138" id="Seg_2893" s="T137">such-ADVZ</ta>
            <ta e="T139" id="Seg_2894" s="T138">say-CO-3SG.O</ta>
            <ta e="T140" id="Seg_2895" s="T139">old.man.[NOM]</ta>
            <ta e="T141" id="Seg_2896" s="T140">bow.[NOM]</ta>
            <ta e="T142" id="Seg_2897" s="T141">make-IMP.2SG.S</ta>
            <ta e="T143" id="Seg_2898" s="T142">earlier</ta>
            <ta e="T144" id="Seg_2899" s="T143">himself.[NOM]</ta>
            <ta e="T145" id="Seg_2900" s="T144">live-PTCP.PST</ta>
            <ta e="T146" id="Seg_2901" s="T145">human.being.[NOM]</ta>
            <ta e="T147" id="Seg_2902" s="T146">wild.animal.[NOM]</ta>
            <ta e="T148" id="Seg_2903" s="T147">meat.[NOM]</ta>
            <ta e="T149" id="Seg_2904" s="T148">eat-INF</ta>
            <ta e="T150" id="Seg_2905" s="T149">old.man.[NOM]</ta>
            <ta e="T151" id="Seg_2906" s="T150">bow-ACC</ta>
            <ta e="T152" id="Seg_2907" s="T151">make-CO-3SG.O</ta>
            <ta e="T153" id="Seg_2908" s="T152">come.on</ta>
            <ta e="T154" id="Seg_2909" s="T153">leave-OPT-3DU.S-EMPH</ta>
            <ta e="T155" id="Seg_2910" s="T154">ski-ACC</ta>
            <ta e="T156" id="Seg_2911" s="T155">make-CO-3SG.O</ta>
            <ta e="T157" id="Seg_2912" s="T156">this</ta>
            <ta e="T158" id="Seg_2913" s="T157">human.being.[NOM]</ta>
            <ta e="T159" id="Seg_2914" s="T158">ski-INSTR</ta>
            <ta e="T160" id="Seg_2915" s="T159">leave-CO.[3SG.S]</ta>
            <ta e="T161" id="Seg_2916" s="T160">bow.[NOM]-3SG</ta>
            <ta e="T162" id="Seg_2917" s="T161">take-CO-3SG.O</ta>
            <ta e="T163" id="Seg_2918" s="T162">himself.[NOM]</ta>
            <ta e="T164" id="Seg_2919" s="T163">old.man.[NOM]</ta>
            <ta e="T165" id="Seg_2920" s="T164">simply</ta>
            <ta e="T166" id="Seg_2921" s="T165">leave-CO.[3SG.S]</ta>
            <ta e="T167" id="Seg_2922" s="T166">ski-CAR-ADVZ</ta>
            <ta e="T168" id="Seg_2923" s="T167">sent.[3SG.S]</ta>
            <ta e="T169" id="Seg_2924" s="T168">one</ta>
            <ta e="T170" id="Seg_2925" s="T169">middle-LOC</ta>
            <ta e="T171" id="Seg_2926" s="T170">sight-CO-3SG.O</ta>
            <ta e="T172" id="Seg_2927" s="T171">reindeer-GEN</ta>
            <ta e="T173" id="Seg_2928" s="T172">trace-ACC</ta>
            <ta e="T174" id="Seg_2929" s="T173">catch.up-TR-CO-3SG.O</ta>
            <ta e="T175" id="Seg_2930" s="T174">catch.up-TR-CO-3SG.O</ta>
            <ta e="T176" id="Seg_2931" s="T175">doll-DIM.[NOM]</ta>
            <ta e="T177" id="Seg_2932" s="T176">forward</ta>
            <ta e="T178" id="Seg_2933" s="T177">roll-FRQ-CO.[3SG.S]</ta>
            <ta e="T179" id="Seg_2934" s="T178">say-US-CVB</ta>
            <ta e="T180" id="Seg_2935" s="T179">reindeer-ACC</ta>
            <ta e="T181" id="Seg_2936" s="T180">eye-INSTR</ta>
            <ta e="T182" id="Seg_2937" s="T181">sight-CO-3SG.O</ta>
            <ta e="T183" id="Seg_2938" s="T182">bow-INSTR</ta>
            <ta e="T184" id="Seg_2939" s="T183">shoot-CO-3SG.O</ta>
            <ta e="T185" id="Seg_2940" s="T184">one</ta>
            <ta e="T186" id="Seg_2941" s="T185">buck.[NOM]</ta>
            <ta e="T187" id="Seg_2942" s="T186">down</ta>
            <ta e="T188" id="Seg_2943" s="T187">sit.down.[3SG.S]</ta>
            <ta e="T189" id="Seg_2944" s="T188">back</ta>
            <ta e="T190" id="Seg_2945" s="T189">then</ta>
            <ta e="T191" id="Seg_2946" s="T190">old.man.[NOM]</ta>
            <ta e="T192" id="Seg_2947" s="T191">come-CO.[3SG.S]</ta>
            <ta e="T193" id="Seg_2948" s="T192">give.a.look-HAB-3SG.O</ta>
            <ta e="T194" id="Seg_2949" s="T193">buck.[NOM]</ta>
            <ta e="T195" id="Seg_2950" s="T194">lie-INFER.[3SG.S]</ta>
            <ta e="T196" id="Seg_2951" s="T195">doll-DIM.[NOM]</ta>
            <ta e="T197" id="Seg_2952" s="T196">sit.[3SG.S]</ta>
            <ta e="T198" id="Seg_2953" s="T197">get.tired.[3SG.S]</ta>
            <ta e="T199" id="Seg_2954" s="T198">old.man.[NOM]</ta>
            <ta e="T200" id="Seg_2955" s="T199">such-ADVZ</ta>
            <ta e="T201" id="Seg_2956" s="T200">say-CO-3SG.O</ta>
            <ta e="T202" id="Seg_2957" s="T201">Opsja.[NOM]</ta>
            <ta e="T203" id="Seg_2958" s="T202">sight-CO-1SG.O</ta>
            <ta e="T204" id="Seg_2959" s="T203">home</ta>
            <ta e="T205" id="Seg_2960" s="T204">leave-TR-1SG.S</ta>
            <ta e="T206" id="Seg_2961" s="T205">away</ta>
            <ta e="T207" id="Seg_2962" s="T206">go.away-CO.[3SG.S]</ta>
            <ta e="T208" id="Seg_2963" s="T207">hand-INSTR</ta>
            <ta e="T209" id="Seg_2964" s="T208">larch.[NOM]</ta>
            <ta e="T210" id="Seg_2965" s="T209">break-FRQ-CO-3SG.O</ta>
            <ta e="T211" id="Seg_2966" s="T210">buck-ACC-3SG</ta>
            <ta e="T212" id="Seg_2967" s="T211">deer's.leg-INSTR</ta>
            <ta e="T213" id="Seg_2968" s="T212">middle-ILL-OBL.3SG</ta>
            <ta e="T214" id="Seg_2969" s="T213">stab-PFV-3SG.O</ta>
            <ta e="T215" id="Seg_2970" s="T214">throw-MULO-CVB</ta>
            <ta e="T216" id="Seg_2971" s="T215">home</ta>
            <ta e="T217" id="Seg_2972" s="T216">come.on</ta>
            <ta e="T218" id="Seg_2973" s="T217">leave-OPT-1DU</ta>
            <ta e="T219" id="Seg_2974" s="T218">home</ta>
            <ta e="T220" id="Seg_2975" s="T219">come-CO.[3SG.S]</ta>
            <ta e="T221" id="Seg_2976" s="T220">old.woman.[NOM]</ta>
            <ta e="T222" id="Seg_2977" s="T221">such-ADVZ</ta>
            <ta e="T223" id="Seg_2978" s="T222">say-CO-3SG.O</ta>
            <ta e="T224" id="Seg_2979" s="T223">Opsja.[NOM]</ta>
            <ta e="T225" id="Seg_2980" s="T224">old.man.[NOM]</ta>
            <ta e="T226" id="Seg_2981" s="T225">bring-CVB</ta>
            <ta e="T227" id="Seg_2982" s="T226">good</ta>
            <ta e="T228" id="Seg_2983" s="T227">doll-DIM.[NOM]</ta>
            <ta e="T229" id="Seg_2984" s="T228">live-HAB-3PL</ta>
            <ta e="T230" id="Seg_2985" s="T229">daughter-GEN-3PL</ta>
            <ta e="T231" id="Seg_2986" s="T230">something-DU.[NOM]</ta>
            <ta e="T232" id="Seg_2987" s="T231">play-CVB</ta>
            <ta e="T233" id="Seg_2988" s="T232">one</ta>
            <ta e="T234" id="Seg_2989" s="T233">guy.[NOM]</ta>
            <ta e="T235" id="Seg_2990" s="T234">find-CO-3SG.O</ta>
            <ta e="T236" id="Seg_2991" s="T235">son.[NOM]-3SG</ta>
            <ta e="T237" id="Seg_2992" s="T236">force-TRL-HAB.[3SG.S]</ta>
            <ta e="T238" id="Seg_2993" s="T237">one</ta>
            <ta e="T239" id="Seg_2994" s="T238">middle-LOC</ta>
            <ta e="T240" id="Seg_2995" s="T239">old.man.[NOM]</ta>
            <ta e="T241" id="Seg_2996" s="T240">daughter-GEN-OBL.1SG</ta>
            <ta e="T242" id="Seg_2997" s="T241">something-DU.[NOM]</ta>
            <ta e="T243" id="Seg_2998" s="T242">let.go-OPT-1DU</ta>
            <ta e="T244" id="Seg_2999" s="T243">home-ADV.LOC</ta>
            <ta e="T245" id="Seg_3000" s="T244">doll-DIM.[NOM]</ta>
            <ta e="T246" id="Seg_3001" s="T245">human.being-PL.[NOM]-3SG</ta>
            <ta e="T247" id="Seg_3002" s="T246">wait-TR-3PL</ta>
            <ta e="T248" id="Seg_3003" s="T247">daughter-ACC-3SG</ta>
            <ta e="T249" id="Seg_3004" s="T248">give-CO-3DU.O</ta>
            <ta e="T250" id="Seg_3005" s="T249">old.man.[NOM]</ta>
            <ta e="T251" id="Seg_3006" s="T250">old.woman-ADJZ</ta>
            <ta e="T252" id="Seg_3007" s="T251">something-DU.[NOM]</ta>
            <ta e="T253" id="Seg_3008" s="T252">daughter-OBL.3SG-ADJZ</ta>
            <ta e="T254" id="Seg_3009" s="T253">leave-CO-3DU.S</ta>
            <ta e="T255" id="Seg_3010" s="T254">home</ta>
            <ta e="T256" id="Seg_3011" s="T255">here</ta>
            <ta e="T257" id="Seg_3012" s="T256">live-INF</ta>
            <ta e="T258" id="Seg_3013" s="T257">bad-ADVZ</ta>
            <ta e="T259" id="Seg_3014" s="T258">be-CO.[3SG.S]</ta>
            <ta e="T260" id="Seg_3015" s="T259">here</ta>
            <ta e="T261" id="Seg_3016" s="T260">giant.[NOM]</ta>
            <ta e="T262" id="Seg_3017" s="T261">old.man.[3SG.S]</ta>
            <ta e="T263" id="Seg_3018" s="T262">something-DU.[NOM]</ta>
            <ta e="T264" id="Seg_3019" s="T263">daughter-ILL</ta>
            <ta e="T265" id="Seg_3020" s="T264">give-CVB</ta>
            <ta e="T266" id="Seg_3021" s="T265">sent-CO-3SG.O</ta>
            <ta e="T267" id="Seg_3022" s="T266">son-OBL.3SG-INSTR</ta>
            <ta e="T268" id="Seg_3023" s="T267">Selkup-EP-ADJZ</ta>
            <ta e="T269" id="Seg_3024" s="T268">earth-ILL</ta>
            <ta e="T270" id="Seg_3025" s="T269">Taz.[NOM]</ta>
            <ta e="T271" id="Seg_3026" s="T270">big.river-ILL</ta>
            <ta e="T272" id="Seg_3027" s="T271">giant-ADJZ</ta>
            <ta e="T273" id="Seg_3028" s="T272">nätäk.[NOM]</ta>
            <ta e="T274" id="Seg_3029" s="T273">hardly</ta>
            <ta e="T275" id="Seg_3030" s="T274">come-CO.[3SG.S]</ta>
            <ta e="T276" id="Seg_3031" s="T275">old.man-GEN-3SG</ta>
            <ta e="T277" id="Seg_3032" s="T276">earth-ILL</ta>
            <ta e="T278" id="Seg_3033" s="T277">live-INF</ta>
            <ta e="T279" id="Seg_3034" s="T278">bad-ADVZ</ta>
            <ta e="T280" id="Seg_3035" s="T279">be-CO.[3SG.S]</ta>
            <ta e="T281" id="Seg_3036" s="T280">black-INSTR-ADVZ</ta>
            <ta e="T282" id="Seg_3037" s="T281">be-CO.[3SG.S]</ta>
            <ta e="T283" id="Seg_3038" s="T282">son-ACC-3SG</ta>
            <ta e="T284" id="Seg_3039" s="T283">father-OBL.3SG-INSTR</ta>
            <ta e="T285" id="Seg_3040" s="T284">leave-CO-3SG.O</ta>
            <ta e="T286" id="Seg_3041" s="T285">Taz.[NOM]</ta>
            <ta e="T287" id="Seg_3042" s="T286">big.river-ILL</ta>
            <ta e="T288" id="Seg_3043" s="T287">husband-GEN-3SG</ta>
            <ta e="T289" id="Seg_3044" s="T288">earth-ILL</ta>
            <ta e="T290" id="Seg_3045" s="T289">Selkup-EP-GEN</ta>
            <ta e="T291" id="Seg_3046" s="T290">earth-ILL</ta>
            <ta e="T292" id="Seg_3047" s="T291">but</ta>
            <ta e="T293" id="Seg_3048" s="T292">I.NOM</ta>
            <ta e="T294" id="Seg_3049" s="T293">home</ta>
            <ta e="T295" id="Seg_3050" s="T294">leave-FUT-1SG.S</ta>
            <ta e="T296" id="Seg_3051" s="T295">giant-ADJZ</ta>
            <ta e="T297" id="Seg_3052" s="T296">rich.man-DU.[NOM]</ta>
            <ta e="T298" id="Seg_3053" s="T297">old.man-ACC-3SG</ta>
            <ta e="T299" id="Seg_3054" s="T298">son-OBL.3SG-INSTR</ta>
            <ta e="T300" id="Seg_3055" s="T299">leave-PFV-CO-3SG.O</ta>
            <ta e="T301" id="Seg_3056" s="T300">good-ADVZ</ta>
            <ta e="T302" id="Seg_3057" s="T301">live-IMP.2DU</ta>
            <ta e="T303" id="Seg_3058" s="T302">Selkup.[NOM]</ta>
            <ta e="T304" id="Seg_3059" s="T303">young</ta>
            <ta e="T305" id="Seg_3060" s="T304">bring.up-PFV-IMP.2PL.O</ta>
            <ta e="T306" id="Seg_3061" s="T305">giant-ADJZ</ta>
            <ta e="T307" id="Seg_3062" s="T306">such-ADVZ</ta>
            <ta e="T308" id="Seg_3063" s="T307">say-PST.NAR-3PL</ta>
            <ta e="T309" id="Seg_3064" s="T308">Selkup-COM</ta>
            <ta e="T310" id="Seg_3065" s="T309">one-COR</ta>
            <ta e="T311" id="Seg_3066" s="T310">bring.up-PTCP.PST</ta>
            <ta e="T312" id="Seg_3067" s="T311">son.[NOM]-1SG</ta>
            <ta e="T313" id="Seg_3068" s="T312">live-IMP.3PL.S</ta>
            <ta e="T314" id="Seg_3069" s="T313">big</ta>
            <ta e="T315" id="Seg_3070" s="T314">body-AUGM.[NOM]</ta>
            <ta e="T316" id="Seg_3071" s="T315">be-CO.[3SG.S]</ta>
            <ta e="T317" id="Seg_3072" s="T316">child.[NOM]-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3073" s="T0">плохо</ta>
            <ta e="T2" id="Seg_3074" s="T1">жить-PST.NAR.[3SG.S]</ta>
            <ta e="T3" id="Seg_3075" s="T2">один</ta>
            <ta e="T4" id="Seg_3076" s="T3">человек.[NOM]</ta>
            <ta e="T5" id="Seg_3077" s="T4">сирота-ADJZ</ta>
            <ta e="T6" id="Seg_3078" s="T5">человек.[NOM]</ta>
            <ta e="T7" id="Seg_3079" s="T6">ни…ни</ta>
            <ta e="T8" id="Seg_3080" s="T7">одежда.[NOM]-3SG</ta>
            <ta e="T9" id="Seg_3081" s="T8">ни…ни</ta>
            <ta e="T10" id="Seg_3082" s="T9">обувь.[NOM]-3SG</ta>
            <ta e="T11" id="Seg_3083" s="T10">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T12" id="Seg_3084" s="T11">голый</ta>
            <ta e="T13" id="Seg_3085" s="T12">жить-PST.NAR.[3SG.S]</ta>
            <ta e="T14" id="Seg_3086" s="T13">думать-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T15" id="Seg_3087" s="T14">думать-FRQ-PST.NAR.[3SG.S]</ta>
            <ta e="T16" id="Seg_3088" s="T15">как</ta>
            <ta e="T17" id="Seg_3089" s="T16">такой-ADVZ</ta>
            <ta e="T18" id="Seg_3090" s="T17">жить-FUT-1SG.S</ta>
            <ta e="T19" id="Seg_3091" s="T18">друг-CAR-ADVZ</ta>
            <ta e="T20" id="Seg_3092" s="T19">уйти-PST-1SG.S</ta>
            <ta e="T21" id="Seg_3093" s="T20">CONJ</ta>
            <ta e="T22" id="Seg_3094" s="T21">отправиться-PST-1SG.S</ta>
            <ta e="T23" id="Seg_3095" s="T22">CONJ</ta>
            <ta e="T24" id="Seg_3096" s="T23">где</ta>
            <ta e="T25" id="Seg_3097" s="T24">INDEF1</ta>
            <ta e="T26" id="Seg_3098" s="T25">куда-ADVZ</ta>
            <ta e="T27" id="Seg_3099" s="T26">INDEF1</ta>
            <ta e="T28" id="Seg_3100" s="T27">человек-EP-PL.[NOM]</ta>
            <ta e="T29" id="Seg_3101" s="T28">найти-FUT-1SG.S</ta>
            <ta e="T30" id="Seg_3102" s="T29">увидеть-PST-1SG.S</ta>
            <ta e="T31" id="Seg_3103" s="T30">CONJ</ta>
            <ta e="T32" id="Seg_3104" s="T31">наружу</ta>
            <ta e="T33" id="Seg_3105" s="T32">выйти.[3SG.S]</ta>
            <ta e="T34" id="Seg_3106" s="T33">отправиться-CO.[3SG.S]</ta>
            <ta e="T35" id="Seg_3107" s="T34">куда-ADVZ</ta>
            <ta e="T36" id="Seg_3108" s="T35">ум.[NOM]-3SG</ta>
            <ta e="T37" id="Seg_3109" s="T36">попасться-HAB.[3SG.S]</ta>
            <ta e="T38" id="Seg_3110" s="T37">один</ta>
            <ta e="T39" id="Seg_3111" s="T38">середина-LOC</ta>
            <ta e="T40" id="Seg_3112" s="T39">взглянуть-HAB-3SG.O</ta>
            <ta e="T41" id="Seg_3113" s="T40">река.[NOM]</ta>
            <ta e="T42" id="Seg_3114" s="T41">показаться.[3SG.S]</ta>
            <ta e="T43" id="Seg_3115" s="T42">большая.река.[NOM]</ta>
            <ta e="T44" id="Seg_3116" s="T43">виднеться.[3SG.S]</ta>
            <ta e="T45" id="Seg_3117" s="T44">туда</ta>
            <ta e="T46" id="Seg_3118" s="T45">спуститься-US-CO.[3SG.S]</ta>
            <ta e="T47" id="Seg_3119" s="T46">вниз</ta>
            <ta e="T48" id="Seg_3120" s="T47">едва</ta>
            <ta e="T49" id="Seg_3121" s="T48">спуститься-US-CO.[3SG.S]</ta>
            <ta e="T50" id="Seg_3122" s="T49">видать</ta>
            <ta e="T51" id="Seg_3123" s="T50">большая.река.[NOM]</ta>
            <ta e="T52" id="Seg_3124" s="T51">вниз.по.течению.реки</ta>
            <ta e="T53" id="Seg_3125" s="T52">взглянуть-HAB.[3SG.S]</ta>
            <ta e="T54" id="Seg_3126" s="T53">длинный</ta>
            <ta e="T55" id="Seg_3127" s="T54">большая.река-GEN</ta>
            <ta e="T56" id="Seg_3128" s="T55">плёс.[NOM]</ta>
            <ta e="T57" id="Seg_3129" s="T56">он.сам.[NOM]</ta>
            <ta e="T58" id="Seg_3130" s="T57">запор.[NOM]</ta>
            <ta e="T59" id="Seg_3131" s="T58">виднеться-INFER.[3SG.S]</ta>
            <ta e="T60" id="Seg_3132" s="T59">туда</ta>
            <ta e="T61" id="Seg_3133" s="T60">едва</ta>
            <ta e="T62" id="Seg_3134" s="T61">прийти-CO-1SG.S</ta>
            <ta e="T63" id="Seg_3135" s="T62">узел-палка-ADJZ</ta>
            <ta e="T64" id="Seg_3136" s="T63">дерево-ADJZ</ta>
            <ta e="T65" id="Seg_3137" s="T64">срубить-FRQ-3SG.O</ta>
            <ta e="T66" id="Seg_3138" s="T65">прутья.для.плетения.запора-PL.[NOM]</ta>
            <ta e="T67" id="Seg_3139" s="T66">слабый-ADVZ</ta>
            <ta e="T68" id="Seg_3140" s="T67">хватать-RFL-FRQ-EP-CO-3PL</ta>
            <ta e="T69" id="Seg_3141" s="T68">след.[NOM]</ta>
            <ta e="T70" id="Seg_3142" s="T69">увидеть-CO-1SG.S</ta>
            <ta e="T71" id="Seg_3143" s="T70">видать</ta>
            <ta e="T72" id="Seg_3144" s="T71">два</ta>
            <ta e="T73" id="Seg_3145" s="T72">нога-ADJZ</ta>
            <ta e="T74" id="Seg_3146" s="T73">большой</ta>
            <ta e="T75" id="Seg_3147" s="T74">быть-CO.[3SG.S]</ta>
            <ta e="T76" id="Seg_3148" s="T75">след.[NOM]-3SG</ta>
            <ta e="T77" id="Seg_3149" s="T76">здесь</ta>
            <ta e="T78" id="Seg_3150" s="T77">морда-GEN</ta>
            <ta e="T79" id="Seg_3151" s="T78">рыба.[NOM]</ta>
            <ta e="T80" id="Seg_3152" s="T79">плыть-TR-HAB-3PL</ta>
            <ta e="T81" id="Seg_3153" s="T80">морда-LOC</ta>
            <ta e="T82" id="Seg_3154" s="T81">запор-GEN</ta>
            <ta e="T83" id="Seg_3155" s="T82">морда-GEN</ta>
            <ta e="T84" id="Seg_3156" s="T83">вниз</ta>
            <ta e="T85" id="Seg_3157" s="T84">взглянуть-HAB.[3SG.S]</ta>
            <ta e="T86" id="Seg_3158" s="T85">морда.[NOM]</ta>
            <ta e="T87" id="Seg_3159" s="T86">большой</ta>
            <ta e="T88" id="Seg_3160" s="T87">большой</ta>
            <ta e="T89" id="Seg_3161" s="T88">морда.[NOM]</ta>
            <ta e="T90" id="Seg_3162" s="T89">вниз.по.течению.реки</ta>
            <ta e="T91" id="Seg_3163" s="T90">взглянуть-HAB-1SG.S</ta>
            <ta e="T92" id="Seg_3164" s="T91">ли</ta>
            <ta e="T93" id="Seg_3165" s="T92">чёрт.[NOM]</ta>
            <ta e="T94" id="Seg_3166" s="T93">прийти-INFER.[3SG.S]</ta>
            <ta e="T95" id="Seg_3167" s="T94">испугаться-DECAUS-CO.[3SG.S]</ta>
            <ta e="T96" id="Seg_3168" s="T95">раньше</ta>
            <ta e="T97" id="Seg_3169" s="T96">прийти-PST-1SG.S</ta>
            <ta e="T98" id="Seg_3170" s="T97">находиться-OPT-1SG.S</ta>
            <ta e="T99" id="Seg_3171" s="T98">если</ta>
            <ta e="T100" id="Seg_3172" s="T99">я.ACC</ta>
            <ta e="T101" id="Seg_3173" s="T100">съесть-CVB</ta>
            <ta e="T102" id="Seg_3174" s="T101">находиться-1SG.S</ta>
            <ta e="T103" id="Seg_3175" s="T102">здесь</ta>
            <ta e="T104" id="Seg_3176" s="T103">INFER</ta>
            <ta e="T105" id="Seg_3177" s="T104">прийти-INFER.[3SG.S]</ta>
            <ta e="T106" id="Seg_3178" s="T105">такой-ADVZ</ta>
            <ta e="T107" id="Seg_3179" s="T106">я.ACC</ta>
            <ta e="T108" id="Seg_3180" s="T107">найти-CO.[3SG.S]</ta>
            <ta e="T109" id="Seg_3181" s="T108">кукла-DIM.[NOM]</ta>
            <ta e="T110" id="Seg_3182" s="T109">дочь.[NOM]-1SG</ta>
            <ta e="T111" id="Seg_3183" s="T110">играть-FUT.[3SG.S]</ta>
            <ta e="T112" id="Seg_3184" s="T111">наверно</ta>
            <ta e="T113" id="Seg_3185" s="T112">отправиться-TR-PST-1SG.O</ta>
            <ta e="T114" id="Seg_3186" s="T113">CONJ</ta>
            <ta e="T115" id="Seg_3187" s="T114">рукавица-ILL-OBL.3SG</ta>
            <ta e="T116" id="Seg_3188" s="T115">сесть-TR-DUR-3SG.O</ta>
            <ta e="T117" id="Seg_3189" s="T116">домой</ta>
            <ta e="T118" id="Seg_3190" s="T117">отправиться-TR-DUR-3SG.O</ta>
            <ta e="T119" id="Seg_3191" s="T118">а</ta>
            <ta e="T120" id="Seg_3192" s="T119">человек.[NOM]</ta>
            <ta e="T121" id="Seg_3193" s="T120">рукавица-LOC</ta>
            <ta e="T122" id="Seg_3194" s="T121">сидеть.[3SG.S]</ta>
            <ta e="T123" id="Seg_3195" s="T122">взглянуть-HAB-3SG.O</ta>
            <ta e="T124" id="Seg_3196" s="T123">вверх</ta>
            <ta e="T125" id="Seg_3197" s="T124">большой</ta>
            <ta e="T126" id="Seg_3198" s="T125">чум.[NOM]</ta>
            <ta e="T127" id="Seg_3199" s="T126">стоять-INFER.[3SG.S]</ta>
            <ta e="T128" id="Seg_3200" s="T127">прийти-CO.[3SG.S]</ta>
            <ta e="T129" id="Seg_3201" s="T128">туда</ta>
            <ta e="T130" id="Seg_3202" s="T129">чум-ILL</ta>
            <ta e="T131" id="Seg_3203" s="T130">таскать-TR-CO-3SG.O</ta>
            <ta e="T132" id="Seg_3204" s="T131">дочь.[NOM]</ta>
            <ta e="T133" id="Seg_3205" s="T132">найти-CO-1SG.S</ta>
            <ta e="T134" id="Seg_3206" s="T133">кукла-DIM-ACC</ta>
            <ta e="T135" id="Seg_3207" s="T134">играть-IMP.2SG.S</ta>
            <ta e="T136" id="Seg_3208" s="T135">жить-HAB-3PL</ta>
            <ta e="T137" id="Seg_3209" s="T136">старуха.[NOM]-3SG</ta>
            <ta e="T138" id="Seg_3210" s="T137">такой-ADVZ</ta>
            <ta e="T139" id="Seg_3211" s="T138">сказать-CO-3SG.O</ta>
            <ta e="T140" id="Seg_3212" s="T139">старик.[NOM]</ta>
            <ta e="T141" id="Seg_3213" s="T140">лук.[NOM]</ta>
            <ta e="T142" id="Seg_3214" s="T141">сделать-IMP.2SG.S</ta>
            <ta e="T143" id="Seg_3215" s="T142">раньше</ta>
            <ta e="T144" id="Seg_3216" s="T143">он.сам.[NOM]</ta>
            <ta e="T145" id="Seg_3217" s="T144">жить-PTCP.PST</ta>
            <ta e="T146" id="Seg_3218" s="T145">человек.[NOM]</ta>
            <ta e="T147" id="Seg_3219" s="T146">зверь.[NOM]</ta>
            <ta e="T148" id="Seg_3220" s="T147">мясо.[NOM]</ta>
            <ta e="T149" id="Seg_3221" s="T148">съесть-INF</ta>
            <ta e="T150" id="Seg_3222" s="T149">старик.[NOM]</ta>
            <ta e="T151" id="Seg_3223" s="T150">лук-ACC</ta>
            <ta e="T152" id="Seg_3224" s="T151">сделать-CO-3SG.O</ta>
            <ta e="T153" id="Seg_3225" s="T152">ну.ка</ta>
            <ta e="T154" id="Seg_3226" s="T153">отправиться-OPT-3DU.S-EMPH</ta>
            <ta e="T155" id="Seg_3227" s="T154">лыжи-ACC</ta>
            <ta e="T156" id="Seg_3228" s="T155">сделать-CO-3SG.O</ta>
            <ta e="T157" id="Seg_3229" s="T156">этот</ta>
            <ta e="T158" id="Seg_3230" s="T157">человек.[NOM]</ta>
            <ta e="T159" id="Seg_3231" s="T158">лыжи-INSTR</ta>
            <ta e="T160" id="Seg_3232" s="T159">отправиться-CO.[3SG.S]</ta>
            <ta e="T161" id="Seg_3233" s="T160">лук.[NOM]-3SG</ta>
            <ta e="T162" id="Seg_3234" s="T161">взять-CO-3SG.O</ta>
            <ta e="T163" id="Seg_3235" s="T162">он.сам.[NOM]</ta>
            <ta e="T164" id="Seg_3236" s="T163">старик.[NOM]</ta>
            <ta e="T165" id="Seg_3237" s="T164">просто.так</ta>
            <ta e="T166" id="Seg_3238" s="T165">отправиться-CO.[3SG.S]</ta>
            <ta e="T167" id="Seg_3239" s="T166">лыжи-CAR-ADVZ</ta>
            <ta e="T168" id="Seg_3240" s="T167">послать.[3SG.S]</ta>
            <ta e="T169" id="Seg_3241" s="T168">один</ta>
            <ta e="T170" id="Seg_3242" s="T169">середина-LOC</ta>
            <ta e="T171" id="Seg_3243" s="T170">увидеть-CO-3SG.O</ta>
            <ta e="T172" id="Seg_3244" s="T171">олень-GEN</ta>
            <ta e="T173" id="Seg_3245" s="T172">след-ACC</ta>
            <ta e="T174" id="Seg_3246" s="T173">догонять-TR-CO-3SG.O</ta>
            <ta e="T175" id="Seg_3247" s="T174">догонять-TR-CO-3SG.O</ta>
            <ta e="T176" id="Seg_3248" s="T175">кукла-DIM.[NOM]</ta>
            <ta e="T177" id="Seg_3249" s="T176">вперёд</ta>
            <ta e="T178" id="Seg_3250" s="T177">катиться-FRQ-CO.[3SG.S]</ta>
            <ta e="T179" id="Seg_3251" s="T178">сказать-US-CVB</ta>
            <ta e="T180" id="Seg_3252" s="T179">олень-ACC</ta>
            <ta e="T181" id="Seg_3253" s="T180">глаз-INSTR</ta>
            <ta e="T182" id="Seg_3254" s="T181">увидеть-CO-3SG.O</ta>
            <ta e="T183" id="Seg_3255" s="T182">лук-INSTR</ta>
            <ta e="T184" id="Seg_3256" s="T183">стрелять-CO-3SG.O</ta>
            <ta e="T185" id="Seg_3257" s="T184">один</ta>
            <ta e="T186" id="Seg_3258" s="T185">хор.[NOM]</ta>
            <ta e="T187" id="Seg_3259" s="T186">вниз</ta>
            <ta e="T188" id="Seg_3260" s="T187">сесть.[3SG.S]</ta>
            <ta e="T189" id="Seg_3261" s="T188">обратно</ta>
            <ta e="T190" id="Seg_3262" s="T189">потом</ta>
            <ta e="T191" id="Seg_3263" s="T190">старик.[NOM]</ta>
            <ta e="T192" id="Seg_3264" s="T191">прийти-CO.[3SG.S]</ta>
            <ta e="T193" id="Seg_3265" s="T192">взглянуть-HAB-3SG.O</ta>
            <ta e="T194" id="Seg_3266" s="T193">хор.[NOM]</ta>
            <ta e="T195" id="Seg_3267" s="T194">лежать-INFER.[3SG.S]</ta>
            <ta e="T196" id="Seg_3268" s="T195">кукла-DIM.[NOM]</ta>
            <ta e="T197" id="Seg_3269" s="T196">сидеть.[3SG.S]</ta>
            <ta e="T198" id="Seg_3270" s="T197">устать.[3SG.S]</ta>
            <ta e="T199" id="Seg_3271" s="T198">старик.[NOM]</ta>
            <ta e="T200" id="Seg_3272" s="T199">такой-ADVZ</ta>
            <ta e="T201" id="Seg_3273" s="T200">сказать-CO-3SG.O</ta>
            <ta e="T202" id="Seg_3274" s="T201">Опся.[NOM]</ta>
            <ta e="T203" id="Seg_3275" s="T202">увидеть-CO-1SG.O</ta>
            <ta e="T204" id="Seg_3276" s="T203">домой</ta>
            <ta e="T205" id="Seg_3277" s="T204">отправиться-TR-1SG.S</ta>
            <ta e="T206" id="Seg_3278" s="T205">прочь</ta>
            <ta e="T207" id="Seg_3279" s="T206">уйти-CO.[3SG.S]</ta>
            <ta e="T208" id="Seg_3280" s="T207">рука-INSTR</ta>
            <ta e="T209" id="Seg_3281" s="T208">лиственница.[NOM]</ta>
            <ta e="T210" id="Seg_3282" s="T209">ломать-FRQ-CO-3SG.O</ta>
            <ta e="T211" id="Seg_3283" s="T210">хор-ACC-3SG</ta>
            <ta e="T212" id="Seg_3284" s="T211">голень.оленя-INSTR</ta>
            <ta e="T213" id="Seg_3285" s="T212">середина-ILL-OBL.3SG</ta>
            <ta e="T214" id="Seg_3286" s="T213">ткнуть-PFV-3SG.O</ta>
            <ta e="T215" id="Seg_3287" s="T214">бросать-MULO-CVB</ta>
            <ta e="T216" id="Seg_3288" s="T215">домой</ta>
            <ta e="T217" id="Seg_3289" s="T216">ну.ка</ta>
            <ta e="T218" id="Seg_3290" s="T217">отправиться-OPT-1DU</ta>
            <ta e="T219" id="Seg_3291" s="T218">домой</ta>
            <ta e="T220" id="Seg_3292" s="T219">прийти-CO.[3SG.S]</ta>
            <ta e="T221" id="Seg_3293" s="T220">старуха.[NOM]</ta>
            <ta e="T222" id="Seg_3294" s="T221">такой-ADVZ</ta>
            <ta e="T223" id="Seg_3295" s="T222">сказать-CO-3SG.O</ta>
            <ta e="T224" id="Seg_3296" s="T223">Опся.[NOM]</ta>
            <ta e="T225" id="Seg_3297" s="T224">старик.[NOM]</ta>
            <ta e="T226" id="Seg_3298" s="T225">принести-CVB</ta>
            <ta e="T227" id="Seg_3299" s="T226">хороший</ta>
            <ta e="T228" id="Seg_3300" s="T227">кукла-DIM.[NOM]</ta>
            <ta e="T229" id="Seg_3301" s="T228">жить-HAB-3PL</ta>
            <ta e="T230" id="Seg_3302" s="T229">дочь-GEN-3PL</ta>
            <ta e="T231" id="Seg_3303" s="T230">нечто-DU.[NOM]</ta>
            <ta e="T232" id="Seg_3304" s="T231">играть-CVB</ta>
            <ta e="T233" id="Seg_3305" s="T232">один</ta>
            <ta e="T234" id="Seg_3306" s="T233">парень.[NOM]</ta>
            <ta e="T235" id="Seg_3307" s="T234">найти-CO-3SG.O</ta>
            <ta e="T236" id="Seg_3308" s="T235">сын.[NOM]-3SG</ta>
            <ta e="T237" id="Seg_3309" s="T236">сила-TRL-HAB.[3SG.S]</ta>
            <ta e="T238" id="Seg_3310" s="T237">один</ta>
            <ta e="T239" id="Seg_3311" s="T238">середина-LOC</ta>
            <ta e="T240" id="Seg_3312" s="T239">старик.[NOM]</ta>
            <ta e="T241" id="Seg_3313" s="T240">дочь-GEN-OBL.1SG</ta>
            <ta e="T242" id="Seg_3314" s="T241">нечто-DU.[NOM]</ta>
            <ta e="T243" id="Seg_3315" s="T242">пустить-OPT-1DU</ta>
            <ta e="T244" id="Seg_3316" s="T243">домой-ADV.LOC</ta>
            <ta e="T245" id="Seg_3317" s="T244">кукла-DIM.[NOM]</ta>
            <ta e="T246" id="Seg_3318" s="T245">человек-PL.[NOM]-3SG</ta>
            <ta e="T247" id="Seg_3319" s="T246">подождать-TR-3PL</ta>
            <ta e="T248" id="Seg_3320" s="T247">дочь-ACC-3SG</ta>
            <ta e="T249" id="Seg_3321" s="T248">дать-CO-3DU.O</ta>
            <ta e="T250" id="Seg_3322" s="T249">старик.[NOM]</ta>
            <ta e="T251" id="Seg_3323" s="T250">старуха-ADJZ</ta>
            <ta e="T252" id="Seg_3324" s="T251">нечто-DU.[NOM]</ta>
            <ta e="T253" id="Seg_3325" s="T252">дочь-OBL.3SG-ADJZ</ta>
            <ta e="T254" id="Seg_3326" s="T253">отправиться-CO-3DU.S</ta>
            <ta e="T255" id="Seg_3327" s="T254">домой</ta>
            <ta e="T256" id="Seg_3328" s="T255">здесь</ta>
            <ta e="T257" id="Seg_3329" s="T256">жить-INF</ta>
            <ta e="T258" id="Seg_3330" s="T257">плохой-ADVZ</ta>
            <ta e="T259" id="Seg_3331" s="T258">быть-CO.[3SG.S]</ta>
            <ta e="T260" id="Seg_3332" s="T259">вот</ta>
            <ta e="T261" id="Seg_3333" s="T260">великан.[NOM]</ta>
            <ta e="T262" id="Seg_3334" s="T261">старик.[3SG.S]</ta>
            <ta e="T263" id="Seg_3335" s="T262">нечто-DU.[NOM]</ta>
            <ta e="T264" id="Seg_3336" s="T263">дочь-ILL</ta>
            <ta e="T265" id="Seg_3337" s="T264">дать-CVB</ta>
            <ta e="T266" id="Seg_3338" s="T265">послать-CO-3SG.O</ta>
            <ta e="T267" id="Seg_3339" s="T266">сын-OBL.3SG-INSTR</ta>
            <ta e="T268" id="Seg_3340" s="T267">селькуп-EP-ADJZ</ta>
            <ta e="T269" id="Seg_3341" s="T268">земля-ILL</ta>
            <ta e="T270" id="Seg_3342" s="T269">Таз.[NOM]</ta>
            <ta e="T271" id="Seg_3343" s="T270">большая.река-ILL</ta>
            <ta e="T272" id="Seg_3344" s="T271">великан-ADJZ</ta>
            <ta e="T273" id="Seg_3345" s="T272">дочь.[NOM]</ta>
            <ta e="T274" id="Seg_3346" s="T273">едва</ta>
            <ta e="T275" id="Seg_3347" s="T274">прийти-CO.[3SG.S]</ta>
            <ta e="T276" id="Seg_3348" s="T275">старик-GEN-3SG</ta>
            <ta e="T277" id="Seg_3349" s="T276">земля-ILL</ta>
            <ta e="T278" id="Seg_3350" s="T277">жить-INF</ta>
            <ta e="T279" id="Seg_3351" s="T278">плохой-ADVZ</ta>
            <ta e="T280" id="Seg_3352" s="T279">быть-CO.[3SG.S]</ta>
            <ta e="T281" id="Seg_3353" s="T280">чёрный-INSTR-ADVZ</ta>
            <ta e="T282" id="Seg_3354" s="T281">быть-CO.[3SG.S]</ta>
            <ta e="T283" id="Seg_3355" s="T282">сын-ACC-3SG</ta>
            <ta e="T284" id="Seg_3356" s="T283">отец-OBL.3SG-INSTR</ta>
            <ta e="T285" id="Seg_3357" s="T284">оставить-CO-3SG.O</ta>
            <ta e="T286" id="Seg_3358" s="T285">Таз.[NOM]</ta>
            <ta e="T287" id="Seg_3359" s="T286">большая.река-ILL</ta>
            <ta e="T288" id="Seg_3360" s="T287">муж-GEN-3SG</ta>
            <ta e="T289" id="Seg_3361" s="T288">земля-ILL</ta>
            <ta e="T290" id="Seg_3362" s="T289">селькуп-EP-GEN</ta>
            <ta e="T291" id="Seg_3363" s="T290">земля-ILL</ta>
            <ta e="T292" id="Seg_3364" s="T291">а</ta>
            <ta e="T293" id="Seg_3365" s="T292">я.NOM</ta>
            <ta e="T294" id="Seg_3366" s="T293">домой</ta>
            <ta e="T295" id="Seg_3367" s="T294">отправиться-FUT-1SG.S</ta>
            <ta e="T296" id="Seg_3368" s="T295">великан-ADJZ</ta>
            <ta e="T297" id="Seg_3369" s="T296">богач-DU.[NOM]</ta>
            <ta e="T298" id="Seg_3370" s="T297">старик-ACC-3SG</ta>
            <ta e="T299" id="Seg_3371" s="T298">сын-OBL.3SG-INSTR</ta>
            <ta e="T300" id="Seg_3372" s="T299">оставить-PFV-CO-3SG.O</ta>
            <ta e="T301" id="Seg_3373" s="T300">хороший-ADVZ</ta>
            <ta e="T302" id="Seg_3374" s="T301">жить-IMP.2DU</ta>
            <ta e="T303" id="Seg_3375" s="T302">селькуп.[NOM]</ta>
            <ta e="T304" id="Seg_3376" s="T303">младший</ta>
            <ta e="T305" id="Seg_3377" s="T304">вырастить-PFV-IMP.2PL.O</ta>
            <ta e="T306" id="Seg_3378" s="T305">великан-ADJZ</ta>
            <ta e="T307" id="Seg_3379" s="T306">такой-ADVZ</ta>
            <ta e="T308" id="Seg_3380" s="T307">сказать-PST.NAR-3PL</ta>
            <ta e="T309" id="Seg_3381" s="T308">селькуп-COM</ta>
            <ta e="T310" id="Seg_3382" s="T309">один-COR</ta>
            <ta e="T311" id="Seg_3383" s="T310">вырастить-PTCP.PST</ta>
            <ta e="T312" id="Seg_3384" s="T311">сын.[NOM]-1SG</ta>
            <ta e="T313" id="Seg_3385" s="T312">жить-IMP.3PL.S</ta>
            <ta e="T314" id="Seg_3386" s="T313">большой</ta>
            <ta e="T315" id="Seg_3387" s="T314">тело-AUGM.[NOM]</ta>
            <ta e="T316" id="Seg_3388" s="T315">быть-CO.[3SG.S]</ta>
            <ta e="T317" id="Seg_3389" s="T316">ребенок.[NOM]-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3390" s="T0">adv</ta>
            <ta e="T2" id="Seg_3391" s="T1">v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_3392" s="T2">num</ta>
            <ta e="T4" id="Seg_3393" s="T3">n-n:case3</ta>
            <ta e="T5" id="Seg_3394" s="T4">n-n&gt;adj</ta>
            <ta e="T6" id="Seg_3395" s="T5">n-n:case3</ta>
            <ta e="T7" id="Seg_3396" s="T6">conj</ta>
            <ta e="T8" id="Seg_3397" s="T7">n-n:case1-n:poss</ta>
            <ta e="T9" id="Seg_3398" s="T8">conj</ta>
            <ta e="T10" id="Seg_3399" s="T9">n-n:case1-n:poss</ta>
            <ta e="T11" id="Seg_3400" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_3401" s="T11">adj</ta>
            <ta e="T13" id="Seg_3402" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_3403" s="T13">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_3404" s="T14">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_3405" s="T15">interrog</ta>
            <ta e="T17" id="Seg_3406" s="T16">dem-adj&gt;adv</ta>
            <ta e="T18" id="Seg_3407" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_3408" s="T18">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T20" id="Seg_3409" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_3410" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_3411" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_3412" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_3413" s="T23">interrog</ta>
            <ta e="T25" id="Seg_3414" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_3415" s="T25">interrog-adj&gt;adv</ta>
            <ta e="T27" id="Seg_3416" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_3417" s="T27">n-n:(ins)-n:num-n:case3</ta>
            <ta e="T29" id="Seg_3418" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_3419" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_3420" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_3421" s="T31">adv</ta>
            <ta e="T33" id="Seg_3422" s="T32">v-v:pn</ta>
            <ta e="T34" id="Seg_3423" s="T33">v-v:ins-v:pn</ta>
            <ta e="T35" id="Seg_3424" s="T34">interrog-adj&gt;adv</ta>
            <ta e="T36" id="Seg_3425" s="T35">n-n:case1-n:poss</ta>
            <ta e="T37" id="Seg_3426" s="T36">v-v&gt;v-v:pn</ta>
            <ta e="T38" id="Seg_3427" s="T37">num</ta>
            <ta e="T39" id="Seg_3428" s="T38">n-n:case3</ta>
            <ta e="T40" id="Seg_3429" s="T39">v-v&gt;v-v:pn</ta>
            <ta e="T41" id="Seg_3430" s="T40">n-n:case3</ta>
            <ta e="T42" id="Seg_3431" s="T41">v-v:pn</ta>
            <ta e="T43" id="Seg_3432" s="T42">n-n:case3</ta>
            <ta e="T44" id="Seg_3433" s="T43">v-v:pn</ta>
            <ta e="T45" id="Seg_3434" s="T44">adv</ta>
            <ta e="T46" id="Seg_3435" s="T45">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T47" id="Seg_3436" s="T46">adv</ta>
            <ta e="T48" id="Seg_3437" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_3438" s="T48">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T50" id="Seg_3439" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_3440" s="T50">n-n:case3</ta>
            <ta e="T52" id="Seg_3441" s="T51">adv</ta>
            <ta e="T53" id="Seg_3442" s="T52">v-v&gt;v-v:pn</ta>
            <ta e="T54" id="Seg_3443" s="T53">adj</ta>
            <ta e="T55" id="Seg_3444" s="T54">n-n:case3</ta>
            <ta e="T56" id="Seg_3445" s="T55">n-n:case3</ta>
            <ta e="T57" id="Seg_3446" s="T56">pro-n:case3</ta>
            <ta e="T58" id="Seg_3447" s="T57">n-n:case3</ta>
            <ta e="T59" id="Seg_3448" s="T58">v-v:tense-mood-v:pn</ta>
            <ta e="T60" id="Seg_3449" s="T59">adv</ta>
            <ta e="T61" id="Seg_3450" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_3451" s="T61">v-v:ins-v:pn</ta>
            <ta e="T63" id="Seg_3452" s="T62">n-n-n&gt;adj</ta>
            <ta e="T64" id="Seg_3453" s="T63">n-n&gt;adj</ta>
            <ta e="T65" id="Seg_3454" s="T64">v-v&gt;v-v:pn</ta>
            <ta e="T66" id="Seg_3455" s="T65">n-n:num-n:case3</ta>
            <ta e="T67" id="Seg_3456" s="T66">adj-adj&gt;adv</ta>
            <ta e="T68" id="Seg_3457" s="T67">v-v&gt;v-v&gt;v-v:ins-v:ins-v:pn</ta>
            <ta e="T69" id="Seg_3458" s="T68">n-n:case3</ta>
            <ta e="T70" id="Seg_3459" s="T69">v-v:ins-v:pn</ta>
            <ta e="T71" id="Seg_3460" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_3461" s="T71">num</ta>
            <ta e="T73" id="Seg_3462" s="T72">n-n&gt;adj</ta>
            <ta e="T74" id="Seg_3463" s="T73">adj</ta>
            <ta e="T75" id="Seg_3464" s="T74">v-v:ins-v:pn</ta>
            <ta e="T76" id="Seg_3465" s="T75">n-n:case1-n:poss</ta>
            <ta e="T77" id="Seg_3466" s="T76">adv</ta>
            <ta e="T78" id="Seg_3467" s="T77">n-n:case3</ta>
            <ta e="T79" id="Seg_3468" s="T78">n-n:case3</ta>
            <ta e="T80" id="Seg_3469" s="T79">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T81" id="Seg_3470" s="T80">n-n:case3</ta>
            <ta e="T82" id="Seg_3471" s="T81">n-n:case3</ta>
            <ta e="T83" id="Seg_3472" s="T82">n-n:case3</ta>
            <ta e="T84" id="Seg_3473" s="T83">adv</ta>
            <ta e="T85" id="Seg_3474" s="T84">v-v&gt;v-v:pn</ta>
            <ta e="T86" id="Seg_3475" s="T85">n-n:case3</ta>
            <ta e="T87" id="Seg_3476" s="T86">adj</ta>
            <ta e="T88" id="Seg_3477" s="T87">adj</ta>
            <ta e="T89" id="Seg_3478" s="T88">n-n:case3</ta>
            <ta e="T90" id="Seg_3479" s="T89">adv</ta>
            <ta e="T91" id="Seg_3480" s="T90">v-v&gt;v-v:pn</ta>
            <ta e="T92" id="Seg_3481" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_3482" s="T92">n-n:case3</ta>
            <ta e="T94" id="Seg_3483" s="T93">v-v:tense-mood-v:pn</ta>
            <ta e="T95" id="Seg_3484" s="T94">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T96" id="Seg_3485" s="T95">adv</ta>
            <ta e="T97" id="Seg_3486" s="T96">v-v:tense-v:pn</ta>
            <ta e="T98" id="Seg_3487" s="T97">v-v:mood-v:pn</ta>
            <ta e="T99" id="Seg_3488" s="T98">conj</ta>
            <ta e="T100" id="Seg_3489" s="T99">pers</ta>
            <ta e="T101" id="Seg_3490" s="T100">v-v&gt;adv</ta>
            <ta e="T102" id="Seg_3491" s="T101">v-v:pn</ta>
            <ta e="T103" id="Seg_3492" s="T102">adv</ta>
            <ta e="T104" id="Seg_3493" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_3494" s="T104">v-v:tense-mood-v:pn</ta>
            <ta e="T106" id="Seg_3495" s="T105">dem-adj&gt;adv</ta>
            <ta e="T107" id="Seg_3496" s="T106">pers</ta>
            <ta e="T108" id="Seg_3497" s="T107">v-v:ins-v:pn</ta>
            <ta e="T109" id="Seg_3498" s="T108">n-n&gt;n-n:case3</ta>
            <ta e="T110" id="Seg_3499" s="T109">n-n:case1-n:poss</ta>
            <ta e="T111" id="Seg_3500" s="T110">v-v:tense-v:pn</ta>
            <ta e="T112" id="Seg_3501" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_3502" s="T112">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_3503" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_3504" s="T114">n-n:case2-n:obl.poss</ta>
            <ta e="T116" id="Seg_3505" s="T115">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T117" id="Seg_3506" s="T116">adv</ta>
            <ta e="T118" id="Seg_3507" s="T117">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T119" id="Seg_3508" s="T118">conj</ta>
            <ta e="T120" id="Seg_3509" s="T119">n-n:case3</ta>
            <ta e="T121" id="Seg_3510" s="T120">n-n:case3</ta>
            <ta e="T122" id="Seg_3511" s="T121">v-v:pn</ta>
            <ta e="T123" id="Seg_3512" s="T122">v-v&gt;v-v:pn</ta>
            <ta e="T124" id="Seg_3513" s="T123">adv</ta>
            <ta e="T125" id="Seg_3514" s="T124">adj</ta>
            <ta e="T126" id="Seg_3515" s="T125">n-n:case3</ta>
            <ta e="T127" id="Seg_3516" s="T126">v-v:tense-mood-v:pn</ta>
            <ta e="T128" id="Seg_3517" s="T127">v-v:ins-v:pn</ta>
            <ta e="T129" id="Seg_3518" s="T128">adv</ta>
            <ta e="T130" id="Seg_3519" s="T129">n-n:case3</ta>
            <ta e="T131" id="Seg_3520" s="T130">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T132" id="Seg_3521" s="T131">n-n:case3</ta>
            <ta e="T133" id="Seg_3522" s="T132">v-v:ins-v:pn</ta>
            <ta e="T134" id="Seg_3523" s="T133">n-n&gt;n-n:case3</ta>
            <ta e="T135" id="Seg_3524" s="T134">v-v:mood-pn</ta>
            <ta e="T136" id="Seg_3525" s="T135">v-v&gt;v-v:pn</ta>
            <ta e="T137" id="Seg_3526" s="T136">n-n:case1-n:poss</ta>
            <ta e="T138" id="Seg_3527" s="T137">dem-adj&gt;adv</ta>
            <ta e="T139" id="Seg_3528" s="T138">v-v:ins-v:pn</ta>
            <ta e="T140" id="Seg_3529" s="T139">n-n:case3</ta>
            <ta e="T141" id="Seg_3530" s="T140">n-n:case3</ta>
            <ta e="T142" id="Seg_3531" s="T141">v-v:mood-pn</ta>
            <ta e="T143" id="Seg_3532" s="T142">adv</ta>
            <ta e="T144" id="Seg_3533" s="T143">pro-n:case3</ta>
            <ta e="T145" id="Seg_3534" s="T144">v-v&gt;ptcp</ta>
            <ta e="T146" id="Seg_3535" s="T145">n-n:case3</ta>
            <ta e="T147" id="Seg_3536" s="T146">n-n:case3</ta>
            <ta e="T148" id="Seg_3537" s="T147">n-n:case3</ta>
            <ta e="T149" id="Seg_3538" s="T148">v-v:inf</ta>
            <ta e="T150" id="Seg_3539" s="T149">n-n:case3</ta>
            <ta e="T151" id="Seg_3540" s="T150">n-n:case3</ta>
            <ta e="T152" id="Seg_3541" s="T151">v-v:ins-v:pn</ta>
            <ta e="T153" id="Seg_3542" s="T152">expl</ta>
            <ta e="T154" id="Seg_3543" s="T153">v-v:mood-v:pn-clit</ta>
            <ta e="T155" id="Seg_3544" s="T154">n-n:case3</ta>
            <ta e="T156" id="Seg_3545" s="T155">v-v:ins-v:pn</ta>
            <ta e="T157" id="Seg_3546" s="T156">dem</ta>
            <ta e="T158" id="Seg_3547" s="T157">n-n:case3</ta>
            <ta e="T159" id="Seg_3548" s="T158">n-n:case3</ta>
            <ta e="T160" id="Seg_3549" s="T159">v-v:ins-v:pn</ta>
            <ta e="T161" id="Seg_3550" s="T160">n-n:case1-n:poss</ta>
            <ta e="T162" id="Seg_3551" s="T161">v-v:ins-v:pn</ta>
            <ta e="T163" id="Seg_3552" s="T162">pro-n:case3</ta>
            <ta e="T164" id="Seg_3553" s="T163">n-n:case3</ta>
            <ta e="T165" id="Seg_3554" s="T164">adv</ta>
            <ta e="T166" id="Seg_3555" s="T165">v-v:ins-v:pn</ta>
            <ta e="T167" id="Seg_3556" s="T166">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T168" id="Seg_3557" s="T167">v-v:pn</ta>
            <ta e="T169" id="Seg_3558" s="T168">num</ta>
            <ta e="T170" id="Seg_3559" s="T169">n-n:case3</ta>
            <ta e="T171" id="Seg_3560" s="T170">v-v:ins-v:pn</ta>
            <ta e="T172" id="Seg_3561" s="T171">n-n:case3</ta>
            <ta e="T173" id="Seg_3562" s="T172">n-n:case3</ta>
            <ta e="T174" id="Seg_3563" s="T173">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T175" id="Seg_3564" s="T174">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T176" id="Seg_3565" s="T175">n-n&gt;n-n:case3</ta>
            <ta e="T177" id="Seg_3566" s="T176">adv</ta>
            <ta e="T178" id="Seg_3567" s="T177">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T179" id="Seg_3568" s="T178">v-v&gt;v-v&gt;adv</ta>
            <ta e="T180" id="Seg_3569" s="T179">n-n:case3</ta>
            <ta e="T181" id="Seg_3570" s="T180">n-n:case3</ta>
            <ta e="T182" id="Seg_3571" s="T181">v-v:ins-v:pn</ta>
            <ta e="T183" id="Seg_3572" s="T182">n-n:case3</ta>
            <ta e="T184" id="Seg_3573" s="T183">v-v:ins-v:pn</ta>
            <ta e="T185" id="Seg_3574" s="T184">num</ta>
            <ta e="T186" id="Seg_3575" s="T185">n-n:case3</ta>
            <ta e="T187" id="Seg_3576" s="T186">preverb</ta>
            <ta e="T188" id="Seg_3577" s="T187">v-v:pn</ta>
            <ta e="T189" id="Seg_3578" s="T188">adv</ta>
            <ta e="T190" id="Seg_3579" s="T189">adv</ta>
            <ta e="T191" id="Seg_3580" s="T190">n-n:case3</ta>
            <ta e="T192" id="Seg_3581" s="T191">v-v:ins-v:pn</ta>
            <ta e="T193" id="Seg_3582" s="T192">v-v&gt;v-v:pn</ta>
            <ta e="T194" id="Seg_3583" s="T193">n-n:case3</ta>
            <ta e="T195" id="Seg_3584" s="T194">v-v:tense-mood-v:pn</ta>
            <ta e="T196" id="Seg_3585" s="T195">n-n&gt;n-n:case3</ta>
            <ta e="T197" id="Seg_3586" s="T196">v-v:pn</ta>
            <ta e="T198" id="Seg_3587" s="T197">v-v:pn</ta>
            <ta e="T199" id="Seg_3588" s="T198">n-n:case3</ta>
            <ta e="T200" id="Seg_3589" s="T199">dem-adj&gt;adv</ta>
            <ta e="T201" id="Seg_3590" s="T200">v-v:ins-v:pn</ta>
            <ta e="T202" id="Seg_3591" s="T201">nprop-n:case3</ta>
            <ta e="T203" id="Seg_3592" s="T202">v-v:ins-v:pn</ta>
            <ta e="T204" id="Seg_3593" s="T203">adv</ta>
            <ta e="T205" id="Seg_3594" s="T204">v-v&gt;v-v:pn</ta>
            <ta e="T206" id="Seg_3595" s="T205">preverb</ta>
            <ta e="T207" id="Seg_3596" s="T206">v-v:ins-v:pn</ta>
            <ta e="T208" id="Seg_3597" s="T207">n-n:case3</ta>
            <ta e="T209" id="Seg_3598" s="T208">n-n:case3</ta>
            <ta e="T210" id="Seg_3599" s="T209">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T211" id="Seg_3600" s="T210">n-n:case1-n:poss</ta>
            <ta e="T212" id="Seg_3601" s="T211">n-n:case3</ta>
            <ta e="T213" id="Seg_3602" s="T212">n-n:case2-n:obl.poss</ta>
            <ta e="T214" id="Seg_3603" s="T213">v-v&gt;v-v:pn</ta>
            <ta e="T215" id="Seg_3604" s="T214">v-v&gt;v-v&gt;adv</ta>
            <ta e="T216" id="Seg_3605" s="T215">adv</ta>
            <ta e="T217" id="Seg_3606" s="T216">expl</ta>
            <ta e="T218" id="Seg_3607" s="T217">v-v:mood-v:pn</ta>
            <ta e="T219" id="Seg_3608" s="T218">adv</ta>
            <ta e="T220" id="Seg_3609" s="T219">v-v:ins-v:pn</ta>
            <ta e="T221" id="Seg_3610" s="T220">n-n:case3</ta>
            <ta e="T222" id="Seg_3611" s="T221">dem-adj&gt;adv</ta>
            <ta e="T223" id="Seg_3612" s="T222">v-v:ins-v:pn</ta>
            <ta e="T224" id="Seg_3613" s="T223">nprop-n:case3</ta>
            <ta e="T225" id="Seg_3614" s="T224">n-n:case3</ta>
            <ta e="T226" id="Seg_3615" s="T225">v-v&gt;adv</ta>
            <ta e="T227" id="Seg_3616" s="T226">adj</ta>
            <ta e="T228" id="Seg_3617" s="T227">n-n&gt;n-n:case3</ta>
            <ta e="T229" id="Seg_3618" s="T228">v-v&gt;v-v:pn</ta>
            <ta e="T230" id="Seg_3619" s="T229">n-n:case1-n:poss</ta>
            <ta e="T231" id="Seg_3620" s="T230">n-n:num-n:case3</ta>
            <ta e="T232" id="Seg_3621" s="T231">v-v&gt;adv</ta>
            <ta e="T233" id="Seg_3622" s="T232">num</ta>
            <ta e="T234" id="Seg_3623" s="T233">n-n:case3</ta>
            <ta e="T235" id="Seg_3624" s="T234">v-v:ins-v:pn</ta>
            <ta e="T236" id="Seg_3625" s="T235">n-n:case1-n:poss</ta>
            <ta e="T237" id="Seg_3626" s="T236">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T238" id="Seg_3627" s="T237">num</ta>
            <ta e="T239" id="Seg_3628" s="T238">n-n:case3</ta>
            <ta e="T240" id="Seg_3629" s="T239">n-n:case3</ta>
            <ta e="T241" id="Seg_3630" s="T240">n-n:case1-n:obl.poss</ta>
            <ta e="T242" id="Seg_3631" s="T241">n-n:num-n:case3</ta>
            <ta e="T243" id="Seg_3632" s="T242">v-v:mood-v:pn</ta>
            <ta e="T244" id="Seg_3633" s="T243">adv-adv&gt;adv</ta>
            <ta e="T245" id="Seg_3634" s="T244">n-n&gt;n-n:case3</ta>
            <ta e="T246" id="Seg_3635" s="T245">n-n:num-n:case1-n:poss</ta>
            <ta e="T247" id="Seg_3636" s="T246">v-v&gt;v-v:pn</ta>
            <ta e="T248" id="Seg_3637" s="T247">n-n:case1-n:poss</ta>
            <ta e="T249" id="Seg_3638" s="T248">v-v:ins-v:pn</ta>
            <ta e="T250" id="Seg_3639" s="T249">n-n:case3</ta>
            <ta e="T251" id="Seg_3640" s="T250">n-n&gt;adj</ta>
            <ta e="T252" id="Seg_3641" s="T251">n-n:num-n:case3</ta>
            <ta e="T253" id="Seg_3642" s="T252">n-n:obl.poss-n&gt;adj</ta>
            <ta e="T254" id="Seg_3643" s="T253">v-v:ins-v:pn</ta>
            <ta e="T255" id="Seg_3644" s="T254">adv</ta>
            <ta e="T256" id="Seg_3645" s="T255">adv</ta>
            <ta e="T257" id="Seg_3646" s="T256">v-v:inf</ta>
            <ta e="T258" id="Seg_3647" s="T257">adj-adj&gt;adv</ta>
            <ta e="T259" id="Seg_3648" s="T258">v-v:ins-v:pn</ta>
            <ta e="T260" id="Seg_3649" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_3650" s="T260">n-n:case3</ta>
            <ta e="T262" id="Seg_3651" s="T261">n-v:pn</ta>
            <ta e="T263" id="Seg_3652" s="T262">n-n:num-n:case3</ta>
            <ta e="T264" id="Seg_3653" s="T263">n-n:case3</ta>
            <ta e="T265" id="Seg_3654" s="T264">v-v&gt;adv</ta>
            <ta e="T266" id="Seg_3655" s="T265">v-v:ins-v:pn</ta>
            <ta e="T267" id="Seg_3656" s="T266">n-n:obl.poss-n:case2</ta>
            <ta e="T268" id="Seg_3657" s="T267">n-n:(ins)-n&gt;adj</ta>
            <ta e="T269" id="Seg_3658" s="T268">n-n:case3</ta>
            <ta e="T270" id="Seg_3659" s="T269">nprop-n:case3</ta>
            <ta e="T271" id="Seg_3660" s="T270">n-n:case3</ta>
            <ta e="T272" id="Seg_3661" s="T271">n-n&gt;adj</ta>
            <ta e="T273" id="Seg_3662" s="T272">n</ta>
            <ta e="T274" id="Seg_3663" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_3664" s="T274">v-v:ins-v:pn</ta>
            <ta e="T276" id="Seg_3665" s="T275">n-n:case1-n:poss</ta>
            <ta e="T277" id="Seg_3666" s="T276">n-n:case3</ta>
            <ta e="T278" id="Seg_3667" s="T277">v-v:inf</ta>
            <ta e="T279" id="Seg_3668" s="T278">adj-adj&gt;adv</ta>
            <ta e="T280" id="Seg_3669" s="T279">v-v:ins-v:pn</ta>
            <ta e="T281" id="Seg_3670" s="T280">adj-n:case2-adj&gt;adv</ta>
            <ta e="T282" id="Seg_3671" s="T281">v-v:ins-v:pn</ta>
            <ta e="T283" id="Seg_3672" s="T282">n-n:case1-n:poss</ta>
            <ta e="T284" id="Seg_3673" s="T283">n-n:obl.poss-n:case2</ta>
            <ta e="T285" id="Seg_3674" s="T284">v-v:ins-v:pn</ta>
            <ta e="T286" id="Seg_3675" s="T285">nprop-n:case3</ta>
            <ta e="T287" id="Seg_3676" s="T286">n-n:case3</ta>
            <ta e="T288" id="Seg_3677" s="T287">n-n:case1-n:poss</ta>
            <ta e="T289" id="Seg_3678" s="T288">n-n:case3</ta>
            <ta e="T290" id="Seg_3679" s="T289">n-n:(ins)-n:case3</ta>
            <ta e="T291" id="Seg_3680" s="T290">n-n:case3</ta>
            <ta e="T292" id="Seg_3681" s="T291">conj</ta>
            <ta e="T293" id="Seg_3682" s="T292">pers</ta>
            <ta e="T294" id="Seg_3683" s="T293">adv</ta>
            <ta e="T295" id="Seg_3684" s="T294">v-v:tense-v:pn</ta>
            <ta e="T296" id="Seg_3685" s="T295">n-n&gt;adj</ta>
            <ta e="T297" id="Seg_3686" s="T296">n-n:num-n:case3</ta>
            <ta e="T298" id="Seg_3687" s="T297">n-n:case1-n:poss</ta>
            <ta e="T299" id="Seg_3688" s="T298">n-n:obl.poss-n:case2</ta>
            <ta e="T300" id="Seg_3689" s="T299">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T301" id="Seg_3690" s="T300">adj-adj&gt;adv</ta>
            <ta e="T302" id="Seg_3691" s="T301">v-v:mood-pn</ta>
            <ta e="T303" id="Seg_3692" s="T302">n-n:case3</ta>
            <ta e="T304" id="Seg_3693" s="T303">adj</ta>
            <ta e="T305" id="Seg_3694" s="T304">v-v&gt;v-v:mood-pn</ta>
            <ta e="T306" id="Seg_3695" s="T305">n-n&gt;adj</ta>
            <ta e="T307" id="Seg_3696" s="T306">dem-adj&gt;adv</ta>
            <ta e="T308" id="Seg_3697" s="T307">v-v:tense-v:pn</ta>
            <ta e="T309" id="Seg_3698" s="T308">n-n:case3</ta>
            <ta e="T310" id="Seg_3699" s="T309">num-n:case3</ta>
            <ta e="T311" id="Seg_3700" s="T310">v-v&gt;ptcp</ta>
            <ta e="T312" id="Seg_3701" s="T311">n-n:case1-n:poss</ta>
            <ta e="T313" id="Seg_3702" s="T312">v-v:mood-pn</ta>
            <ta e="T314" id="Seg_3703" s="T313">adj</ta>
            <ta e="T315" id="Seg_3704" s="T314">n-n&gt;n-n:case3</ta>
            <ta e="T316" id="Seg_3705" s="T315">v-v:ins-v:pn</ta>
            <ta e="T317" id="Seg_3706" s="T316">n-n:case1-n:poss</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_3707" s="T0">adv</ta>
            <ta e="T2" id="Seg_3708" s="T1">v</ta>
            <ta e="T3" id="Seg_3709" s="T2">num</ta>
            <ta e="T4" id="Seg_3710" s="T3">n</ta>
            <ta e="T5" id="Seg_3711" s="T4">adj</ta>
            <ta e="T6" id="Seg_3712" s="T5">n</ta>
            <ta e="T7" id="Seg_3713" s="T6">conj</ta>
            <ta e="T8" id="Seg_3714" s="T7">n</ta>
            <ta e="T9" id="Seg_3715" s="T8">conj</ta>
            <ta e="T10" id="Seg_3716" s="T9">n</ta>
            <ta e="T11" id="Seg_3717" s="T10">v</ta>
            <ta e="T12" id="Seg_3718" s="T11">adj</ta>
            <ta e="T13" id="Seg_3719" s="T12">v</ta>
            <ta e="T14" id="Seg_3720" s="T13">v</ta>
            <ta e="T15" id="Seg_3721" s="T14">v</ta>
            <ta e="T16" id="Seg_3722" s="T15">interrog</ta>
            <ta e="T17" id="Seg_3723" s="T16">adv</ta>
            <ta e="T18" id="Seg_3724" s="T17">v</ta>
            <ta e="T19" id="Seg_3725" s="T18">adv</ta>
            <ta e="T20" id="Seg_3726" s="T19">v</ta>
            <ta e="T21" id="Seg_3727" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_3728" s="T21">v</ta>
            <ta e="T23" id="Seg_3729" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_3730" s="T23">interrog</ta>
            <ta e="T25" id="Seg_3731" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_3732" s="T25">adv</ta>
            <ta e="T27" id="Seg_3733" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_3734" s="T27">n</ta>
            <ta e="T29" id="Seg_3735" s="T28">v</ta>
            <ta e="T30" id="Seg_3736" s="T29">v</ta>
            <ta e="T31" id="Seg_3737" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_3738" s="T31">adv</ta>
            <ta e="T33" id="Seg_3739" s="T32">v</ta>
            <ta e="T34" id="Seg_3740" s="T33">v</ta>
            <ta e="T35" id="Seg_3741" s="T34">adv</ta>
            <ta e="T36" id="Seg_3742" s="T35">n</ta>
            <ta e="T37" id="Seg_3743" s="T36">v</ta>
            <ta e="T38" id="Seg_3744" s="T37">num</ta>
            <ta e="T39" id="Seg_3745" s="T38">n</ta>
            <ta e="T40" id="Seg_3746" s="T39">v</ta>
            <ta e="T41" id="Seg_3747" s="T40">n</ta>
            <ta e="T42" id="Seg_3748" s="T41">v</ta>
            <ta e="T43" id="Seg_3749" s="T42">n</ta>
            <ta e="T44" id="Seg_3750" s="T43">v</ta>
            <ta e="T45" id="Seg_3751" s="T44">adv</ta>
            <ta e="T46" id="Seg_3752" s="T45">v</ta>
            <ta e="T47" id="Seg_3753" s="T46">adv</ta>
            <ta e="T48" id="Seg_3754" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_3755" s="T48">v</ta>
            <ta e="T50" id="Seg_3756" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_3757" s="T50">n</ta>
            <ta e="T52" id="Seg_3758" s="T51">adv</ta>
            <ta e="T53" id="Seg_3759" s="T52">v</ta>
            <ta e="T54" id="Seg_3760" s="T53">adj</ta>
            <ta e="T55" id="Seg_3761" s="T54">n</ta>
            <ta e="T56" id="Seg_3762" s="T55">n</ta>
            <ta e="T57" id="Seg_3763" s="T56">pro</ta>
            <ta e="T58" id="Seg_3764" s="T57">n</ta>
            <ta e="T59" id="Seg_3765" s="T58">v</ta>
            <ta e="T60" id="Seg_3766" s="T59">adv</ta>
            <ta e="T61" id="Seg_3767" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_3768" s="T61">v</ta>
            <ta e="T63" id="Seg_3769" s="T62">adj</ta>
            <ta e="T64" id="Seg_3770" s="T63">adj</ta>
            <ta e="T65" id="Seg_3771" s="T64">v</ta>
            <ta e="T66" id="Seg_3772" s="T65">n</ta>
            <ta e="T67" id="Seg_3773" s="T66">adv</ta>
            <ta e="T68" id="Seg_3774" s="T67">v</ta>
            <ta e="T69" id="Seg_3775" s="T68">n</ta>
            <ta e="T70" id="Seg_3776" s="T69">v</ta>
            <ta e="T71" id="Seg_3777" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_3778" s="T71">num</ta>
            <ta e="T73" id="Seg_3779" s="T72">adj</ta>
            <ta e="T74" id="Seg_3780" s="T73">adj</ta>
            <ta e="T75" id="Seg_3781" s="T74">v</ta>
            <ta e="T76" id="Seg_3782" s="T75">n</ta>
            <ta e="T77" id="Seg_3783" s="T76">adv</ta>
            <ta e="T78" id="Seg_3784" s="T77">n</ta>
            <ta e="T79" id="Seg_3785" s="T78">n</ta>
            <ta e="T80" id="Seg_3786" s="T79">v</ta>
            <ta e="T81" id="Seg_3787" s="T80">n</ta>
            <ta e="T82" id="Seg_3788" s="T81">n</ta>
            <ta e="T83" id="Seg_3789" s="T82">n</ta>
            <ta e="T84" id="Seg_3790" s="T83">adv</ta>
            <ta e="T85" id="Seg_3791" s="T84">v</ta>
            <ta e="T86" id="Seg_3792" s="T85">n</ta>
            <ta e="T87" id="Seg_3793" s="T86">adj</ta>
            <ta e="T88" id="Seg_3794" s="T87">adj</ta>
            <ta e="T89" id="Seg_3795" s="T88">n</ta>
            <ta e="T90" id="Seg_3796" s="T89">adv</ta>
            <ta e="T91" id="Seg_3797" s="T90">v</ta>
            <ta e="T92" id="Seg_3798" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_3799" s="T92">n</ta>
            <ta e="T94" id="Seg_3800" s="T93">v</ta>
            <ta e="T95" id="Seg_3801" s="T94">v</ta>
            <ta e="T96" id="Seg_3802" s="T95">adv</ta>
            <ta e="T97" id="Seg_3803" s="T96">v</ta>
            <ta e="T98" id="Seg_3804" s="T97">v</ta>
            <ta e="T99" id="Seg_3805" s="T98">conj</ta>
            <ta e="T100" id="Seg_3806" s="T99">pers</ta>
            <ta e="T101" id="Seg_3807" s="T100">adv</ta>
            <ta e="T102" id="Seg_3808" s="T101">v</ta>
            <ta e="T103" id="Seg_3809" s="T102">adv</ta>
            <ta e="T104" id="Seg_3810" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_3811" s="T104">v</ta>
            <ta e="T106" id="Seg_3812" s="T105">adv</ta>
            <ta e="T107" id="Seg_3813" s="T106">pers</ta>
            <ta e="T108" id="Seg_3814" s="T107">v</ta>
            <ta e="T109" id="Seg_3815" s="T108">n</ta>
            <ta e="T110" id="Seg_3816" s="T109">n</ta>
            <ta e="T111" id="Seg_3817" s="T110">v</ta>
            <ta e="T112" id="Seg_3818" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_3819" s="T112">v</ta>
            <ta e="T114" id="Seg_3820" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_3821" s="T114">n</ta>
            <ta e="T116" id="Seg_3822" s="T115">v</ta>
            <ta e="T117" id="Seg_3823" s="T116">adv</ta>
            <ta e="T118" id="Seg_3824" s="T117">v</ta>
            <ta e="T119" id="Seg_3825" s="T118">conj</ta>
            <ta e="T120" id="Seg_3826" s="T119">n</ta>
            <ta e="T121" id="Seg_3827" s="T120">n</ta>
            <ta e="T122" id="Seg_3828" s="T121">v</ta>
            <ta e="T123" id="Seg_3829" s="T122">v</ta>
            <ta e="T124" id="Seg_3830" s="T123">adv</ta>
            <ta e="T125" id="Seg_3831" s="T124">adj</ta>
            <ta e="T126" id="Seg_3832" s="T125">n</ta>
            <ta e="T127" id="Seg_3833" s="T126">v</ta>
            <ta e="T128" id="Seg_3834" s="T127">v</ta>
            <ta e="T129" id="Seg_3835" s="T128">adv</ta>
            <ta e="T130" id="Seg_3836" s="T129">n</ta>
            <ta e="T131" id="Seg_3837" s="T130">v</ta>
            <ta e="T132" id="Seg_3838" s="T131">n</ta>
            <ta e="T133" id="Seg_3839" s="T132">v</ta>
            <ta e="T134" id="Seg_3840" s="T133">n</ta>
            <ta e="T135" id="Seg_3841" s="T134">v</ta>
            <ta e="T136" id="Seg_3842" s="T135">v</ta>
            <ta e="T137" id="Seg_3843" s="T136">n</ta>
            <ta e="T138" id="Seg_3844" s="T137">adv</ta>
            <ta e="T139" id="Seg_3845" s="T138">v</ta>
            <ta e="T140" id="Seg_3846" s="T139">n</ta>
            <ta e="T141" id="Seg_3847" s="T140">n</ta>
            <ta e="T142" id="Seg_3848" s="T141">v</ta>
            <ta e="T143" id="Seg_3849" s="T142">adv</ta>
            <ta e="T144" id="Seg_3850" s="T143">pro</ta>
            <ta e="T145" id="Seg_3851" s="T144">ptcp</ta>
            <ta e="T146" id="Seg_3852" s="T145">n</ta>
            <ta e="T147" id="Seg_3853" s="T146">n</ta>
            <ta e="T148" id="Seg_3854" s="T147">n</ta>
            <ta e="T149" id="Seg_3855" s="T148">v</ta>
            <ta e="T150" id="Seg_3856" s="T149">n</ta>
            <ta e="T151" id="Seg_3857" s="T150">n</ta>
            <ta e="T152" id="Seg_3858" s="T151">v</ta>
            <ta e="T153" id="Seg_3859" s="T152">expl</ta>
            <ta e="T154" id="Seg_3860" s="T153">v</ta>
            <ta e="T155" id="Seg_3861" s="T154">n</ta>
            <ta e="T156" id="Seg_3862" s="T155">v</ta>
            <ta e="T157" id="Seg_3863" s="T156">dem</ta>
            <ta e="T158" id="Seg_3864" s="T157">n</ta>
            <ta e="T159" id="Seg_3865" s="T158">n</ta>
            <ta e="T160" id="Seg_3866" s="T159">v</ta>
            <ta e="T161" id="Seg_3867" s="T160">n</ta>
            <ta e="T162" id="Seg_3868" s="T161">v</ta>
            <ta e="T163" id="Seg_3869" s="T162">pro</ta>
            <ta e="T164" id="Seg_3870" s="T163">n</ta>
            <ta e="T165" id="Seg_3871" s="T164">adv</ta>
            <ta e="T166" id="Seg_3872" s="T165">v</ta>
            <ta e="T167" id="Seg_3873" s="T166">adv</ta>
            <ta e="T168" id="Seg_3874" s="T167">v</ta>
            <ta e="T169" id="Seg_3875" s="T168">num</ta>
            <ta e="T170" id="Seg_3876" s="T169">n</ta>
            <ta e="T171" id="Seg_3877" s="T170">v</ta>
            <ta e="T172" id="Seg_3878" s="T171">n</ta>
            <ta e="T173" id="Seg_3879" s="T172">n</ta>
            <ta e="T174" id="Seg_3880" s="T173">v</ta>
            <ta e="T175" id="Seg_3881" s="T174">v</ta>
            <ta e="T176" id="Seg_3882" s="T175">n</ta>
            <ta e="T177" id="Seg_3883" s="T176">adv</ta>
            <ta e="T178" id="Seg_3884" s="T177">v</ta>
            <ta e="T179" id="Seg_3885" s="T178">adv</ta>
            <ta e="T180" id="Seg_3886" s="T179">n</ta>
            <ta e="T181" id="Seg_3887" s="T180">n</ta>
            <ta e="T182" id="Seg_3888" s="T181">v</ta>
            <ta e="T183" id="Seg_3889" s="T182">n</ta>
            <ta e="T184" id="Seg_3890" s="T183">v</ta>
            <ta e="T185" id="Seg_3891" s="T184">num</ta>
            <ta e="T186" id="Seg_3892" s="T185">n</ta>
            <ta e="T187" id="Seg_3893" s="T186">preverb</ta>
            <ta e="T188" id="Seg_3894" s="T187">v</ta>
            <ta e="T189" id="Seg_3895" s="T188">adv</ta>
            <ta e="T190" id="Seg_3896" s="T189">adv</ta>
            <ta e="T191" id="Seg_3897" s="T190">n</ta>
            <ta e="T192" id="Seg_3898" s="T191">v</ta>
            <ta e="T193" id="Seg_3899" s="T192">v</ta>
            <ta e="T194" id="Seg_3900" s="T193">n</ta>
            <ta e="T195" id="Seg_3901" s="T194">v</ta>
            <ta e="T196" id="Seg_3902" s="T195">n</ta>
            <ta e="T197" id="Seg_3903" s="T196">v</ta>
            <ta e="T198" id="Seg_3904" s="T197">v</ta>
            <ta e="T199" id="Seg_3905" s="T198">n</ta>
            <ta e="T200" id="Seg_3906" s="T199">adv</ta>
            <ta e="T201" id="Seg_3907" s="T200">v</ta>
            <ta e="T202" id="Seg_3908" s="T201">nprop</ta>
            <ta e="T203" id="Seg_3909" s="T202">v</ta>
            <ta e="T204" id="Seg_3910" s="T203">adv</ta>
            <ta e="T205" id="Seg_3911" s="T204">v</ta>
            <ta e="T206" id="Seg_3912" s="T205">preverb</ta>
            <ta e="T207" id="Seg_3913" s="T206">v</ta>
            <ta e="T208" id="Seg_3914" s="T207">n</ta>
            <ta e="T209" id="Seg_3915" s="T208">n</ta>
            <ta e="T210" id="Seg_3916" s="T209">v</ta>
            <ta e="T211" id="Seg_3917" s="T210">n</ta>
            <ta e="T212" id="Seg_3918" s="T211">n</ta>
            <ta e="T213" id="Seg_3919" s="T212">n</ta>
            <ta e="T214" id="Seg_3920" s="T213">v</ta>
            <ta e="T215" id="Seg_3921" s="T214">adv</ta>
            <ta e="T216" id="Seg_3922" s="T215">adv</ta>
            <ta e="T217" id="Seg_3923" s="T216">expl</ta>
            <ta e="T218" id="Seg_3924" s="T217">v</ta>
            <ta e="T219" id="Seg_3925" s="T218">adv</ta>
            <ta e="T220" id="Seg_3926" s="T219">v</ta>
            <ta e="T221" id="Seg_3927" s="T220">n</ta>
            <ta e="T222" id="Seg_3928" s="T221">adv</ta>
            <ta e="T223" id="Seg_3929" s="T222">v</ta>
            <ta e="T224" id="Seg_3930" s="T223">nprop</ta>
            <ta e="T225" id="Seg_3931" s="T224">n</ta>
            <ta e="T226" id="Seg_3932" s="T225">adv</ta>
            <ta e="T227" id="Seg_3933" s="T226">adj</ta>
            <ta e="T228" id="Seg_3934" s="T227">n</ta>
            <ta e="T229" id="Seg_3935" s="T228">v</ta>
            <ta e="T230" id="Seg_3936" s="T229">n</ta>
            <ta e="T231" id="Seg_3937" s="T230">n</ta>
            <ta e="T232" id="Seg_3938" s="T231">adv</ta>
            <ta e="T233" id="Seg_3939" s="T232">num</ta>
            <ta e="T234" id="Seg_3940" s="T233">n</ta>
            <ta e="T235" id="Seg_3941" s="T234">v</ta>
            <ta e="T236" id="Seg_3942" s="T235">n</ta>
            <ta e="T237" id="Seg_3943" s="T236">v</ta>
            <ta e="T238" id="Seg_3944" s="T237">num</ta>
            <ta e="T239" id="Seg_3945" s="T238">n</ta>
            <ta e="T240" id="Seg_3946" s="T239">n</ta>
            <ta e="T241" id="Seg_3947" s="T240">n</ta>
            <ta e="T242" id="Seg_3948" s="T241">n</ta>
            <ta e="T243" id="Seg_3949" s="T242">v</ta>
            <ta e="T244" id="Seg_3950" s="T243">adv</ta>
            <ta e="T245" id="Seg_3951" s="T244">n</ta>
            <ta e="T246" id="Seg_3952" s="T245">n</ta>
            <ta e="T247" id="Seg_3953" s="T246">v</ta>
            <ta e="T248" id="Seg_3954" s="T247">n</ta>
            <ta e="T249" id="Seg_3955" s="T248">v</ta>
            <ta e="T250" id="Seg_3956" s="T249">n</ta>
            <ta e="T251" id="Seg_3957" s="T250">adj</ta>
            <ta e="T252" id="Seg_3958" s="T251">n</ta>
            <ta e="T253" id="Seg_3959" s="T252">adj</ta>
            <ta e="T254" id="Seg_3960" s="T253">v</ta>
            <ta e="T255" id="Seg_3961" s="T254">adv</ta>
            <ta e="T256" id="Seg_3962" s="T255">adv</ta>
            <ta e="T257" id="Seg_3963" s="T256">v</ta>
            <ta e="T258" id="Seg_3964" s="T257">adv</ta>
            <ta e="T259" id="Seg_3965" s="T258">v</ta>
            <ta e="T260" id="Seg_3966" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_3967" s="T260">n</ta>
            <ta e="T262" id="Seg_3968" s="T261">n</ta>
            <ta e="T263" id="Seg_3969" s="T262">n</ta>
            <ta e="T264" id="Seg_3970" s="T263">n</ta>
            <ta e="T265" id="Seg_3971" s="T264">adv</ta>
            <ta e="T266" id="Seg_3972" s="T265">v</ta>
            <ta e="T267" id="Seg_3973" s="T266">n</ta>
            <ta e="T268" id="Seg_3974" s="T267">adj</ta>
            <ta e="T269" id="Seg_3975" s="T268">n</ta>
            <ta e="T270" id="Seg_3976" s="T269">nprop</ta>
            <ta e="T271" id="Seg_3977" s="T270">n</ta>
            <ta e="T272" id="Seg_3978" s="T271">adj</ta>
            <ta e="T273" id="Seg_3979" s="T272">n</ta>
            <ta e="T274" id="Seg_3980" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_3981" s="T274">v</ta>
            <ta e="T276" id="Seg_3982" s="T275">n</ta>
            <ta e="T277" id="Seg_3983" s="T276">n</ta>
            <ta e="T278" id="Seg_3984" s="T277">v</ta>
            <ta e="T279" id="Seg_3985" s="T278">adv</ta>
            <ta e="T280" id="Seg_3986" s="T279">v</ta>
            <ta e="T281" id="Seg_3987" s="T280">adv</ta>
            <ta e="T282" id="Seg_3988" s="T281">v</ta>
            <ta e="T283" id="Seg_3989" s="T282">n</ta>
            <ta e="T284" id="Seg_3990" s="T283">n</ta>
            <ta e="T285" id="Seg_3991" s="T284">v</ta>
            <ta e="T286" id="Seg_3992" s="T285">nprop</ta>
            <ta e="T287" id="Seg_3993" s="T286">n</ta>
            <ta e="T288" id="Seg_3994" s="T287">n</ta>
            <ta e="T289" id="Seg_3995" s="T288">n</ta>
            <ta e="T290" id="Seg_3996" s="T289">n</ta>
            <ta e="T291" id="Seg_3997" s="T290">n</ta>
            <ta e="T292" id="Seg_3998" s="T291">conj</ta>
            <ta e="T293" id="Seg_3999" s="T292">pers</ta>
            <ta e="T294" id="Seg_4000" s="T293">adv</ta>
            <ta e="T295" id="Seg_4001" s="T294">v</ta>
            <ta e="T296" id="Seg_4002" s="T295">adj</ta>
            <ta e="T297" id="Seg_4003" s="T296">n</ta>
            <ta e="T298" id="Seg_4004" s="T297">n</ta>
            <ta e="T299" id="Seg_4005" s="T298">n</ta>
            <ta e="T300" id="Seg_4006" s="T299">v</ta>
            <ta e="T301" id="Seg_4007" s="T300">adv</ta>
            <ta e="T302" id="Seg_4008" s="T301">v</ta>
            <ta e="T303" id="Seg_4009" s="T302">n</ta>
            <ta e="T304" id="Seg_4010" s="T303">adj</ta>
            <ta e="T305" id="Seg_4011" s="T304">v</ta>
            <ta e="T306" id="Seg_4012" s="T305">adj</ta>
            <ta e="T307" id="Seg_4013" s="T306">adv</ta>
            <ta e="T308" id="Seg_4014" s="T307">v</ta>
            <ta e="T309" id="Seg_4015" s="T308">n</ta>
            <ta e="T310" id="Seg_4016" s="T309">num</ta>
            <ta e="T311" id="Seg_4017" s="T310">ptcp</ta>
            <ta e="T312" id="Seg_4018" s="T311">n</ta>
            <ta e="T313" id="Seg_4019" s="T312">v</ta>
            <ta e="T314" id="Seg_4020" s="T313">adj</ta>
            <ta e="T315" id="Seg_4021" s="T314">n</ta>
            <ta e="T316" id="Seg_4022" s="T315">v</ta>
            <ta e="T317" id="Seg_4023" s="T316">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_4024" s="T0">Плохо жил один человек, сирота.</ta>
            <ta e="T11" id="Seg_4025" s="T6">Ни одежды, ни обуви у него не было.</ta>
            <ta e="T13" id="Seg_4026" s="T11">Голый жил.</ta>
            <ta e="T19" id="Seg_4027" s="T13">Думал: "Как я буду жить один?</ta>
            <ta e="T31" id="Seg_4028" s="T19">Если я отправлюсь [в путь], то где-нибудь найду, увижу людей".</ta>
            <ta e="T37" id="Seg_4029" s="T31">На улицу он вышел, пошёл, куда глаза глядят.</ta>
            <ta e="T44" id="Seg_4030" s="T37">Однажды он смотрит, река показалась, большая река виднеется.</ta>
            <ta e="T51" id="Seg_4031" s="T44">Он туда спускается, вниз спускается, [там] видна большая река.</ta>
            <ta e="T59" id="Seg_4032" s="T51">Вниз смотрит, [там] длинный плёс, запор виднеется.</ta>
            <ta e="T65" id="Seg_4033" s="T59">"Туда пришёл я, связки шестов нарубил.</ta>
            <ta e="T68" id="Seg_4034" s="T65">Прутья слабо захватываются.</ta>
            <ta e="T75" id="Seg_4035" s="T68">След я вижу, видно, что в две ступни величиной.</ta>
            <ta e="T83" id="Seg_4036" s="T75">След там от морды (рыба плавает в морде), запора морды. </ta>
            <ta e="T89" id="Seg_4037" s="T83">Вниз он смотрит, там морда большая, большая морда.</ta>
            <ta e="T94" id="Seg_4038" s="T89">"Вниз по течению смотрю, [там] чёрт что ли пришёл".</ta>
            <ta e="T95" id="Seg_4039" s="T94">Ему страшно.</ta>
            <ta e="T98" id="Seg_4040" s="T95">"Я раньше пришёл, (пусть) я останусь. </ta>
            <ta e="T103" id="Seg_4041" s="T98">[Даже] если съест меня, буду здесь.</ta>
            <ta e="T105" id="Seg_4042" s="T103">Вот он пришёл.</ta>
            <ta e="T108" id="Seg_4043" s="T105">Меня видит.</ta>
            <ta e="T109" id="Seg_4044" s="T108">"Чертёнок!</ta>
            <ta e="T111" id="Seg_4045" s="T109">Моя дочка будет играть.</ta>
            <ta e="T114" id="Seg_4046" s="T111">Наверное унесу домой".</ta>
            <ta e="T116" id="Seg_4047" s="T114">Он [его] посадил в свою рукавицу.</ta>
            <ta e="T118" id="Seg_4048" s="T116">Домой понёс.</ta>
            <ta e="T122" id="Seg_4049" s="T118">А человек в рукавице сидит.</ta>
            <ta e="T127" id="Seg_4050" s="T122">Смотрит вверх, [там] большой чум стоит.</ta>
            <ta e="T128" id="Seg_4051" s="T127">Пришёл.</ta>
            <ta e="T131" id="Seg_4052" s="T128">Туда в чум заносит [чертёнка].</ta>
            <ta e="T135" id="Seg_4053" s="T131">"Дочка, я нашёл чертёнка, играй."</ta>
            <ta e="T136" id="Seg_4054" s="T135">Живут они.</ta>
            <ta e="T149" id="Seg_4055" s="T136">Его старуха говорит: "Старик, сделай лук, чтобы человек, живший раньше сам по себе, [смог] звериное мясо поесть".</ta>
            <ta e="T152" id="Seg_4056" s="T149">Старик делает лук. </ta>
            <ta e="T154" id="Seg_4057" s="T152">"Пусть они [вдвоём] пойдут".</ta>
            <ta e="T156" id="Seg_4058" s="T154">Он лыжи делает.</ta>
            <ta e="T160" id="Seg_4059" s="T156">Этот человек [чертёнок] на лыжах идёт.</ta>
            <ta e="T162" id="Seg_4060" s="T160">Лук свой берёт.</ta>
            <ta e="T168" id="Seg_4061" s="T162">Сам старик просто так идёт, без лыж [его] посылает.</ta>
            <ta e="T173" id="Seg_4062" s="T168">Однажды он видит олений след. </ta>
            <ta e="T175" id="Seg_4063" s="T173">Догоняет [= идёт по следу]. </ta>
            <ta e="T182" id="Seg_4064" s="T175">Чертёнок впереди катится, говоря, [что] оленя [своими] глазами видит.</ta>
            <ta e="T186" id="Seg_4065" s="T182">Из лука он стреляет в оленя-хора.</ta>
            <ta e="T188" id="Seg_4066" s="T186">Он [старик?] садится.</ta>
            <ta e="T192" id="Seg_4067" s="T188">Потом старик приходит обратно.</ta>
            <ta e="T198" id="Seg_4068" s="T192">Видит, олень-хор лежит, чертёнок сидит уставший.</ta>
            <ta e="T205" id="Seg_4069" s="T198">Старик так говорит: Я Опсю вижу, я [его] домой унесу".</ta>
            <ta e="T207" id="Seg_4070" s="T205">Он уходит.</ta>
            <ta e="T210" id="Seg_4071" s="T207">Руками он ломает лиственницу.</ta>
            <ta e="T215" id="Seg_4072" s="T210">Оленя-хора своего ногами посередине, бросив, пристроил.</ta>
            <ta e="T218" id="Seg_4073" s="T215">"Пойдём домой". </ta>
            <ta e="T220" id="Seg_4074" s="T218">Он пришёл домой.</ta>
            <ta e="T228" id="Seg_4075" s="T220">Старуха так сказала, [когда] старик принёс Опсю: "Хороший чертёнок". </ta>
            <ta e="T232" id="Seg_4076" s="T228">Живут они, их дочь [с чертёнком] вдвоём играют.</ta>
            <ta e="T237" id="Seg_4077" s="T232">Одного сына она [родила?], сын растёт.</ta>
            <ta e="T243" id="Seg_4078" s="T237">Однажды старик [говорит]: "Давай дочь с её парой отпустим.</ta>
            <ta e="T247" id="Seg_4079" s="T243">Дома чертёнка ждут его люди".</ta>
            <ta e="T252" id="Seg_4080" s="T247">Дочь свою отдали старик со старухой.</ta>
            <ta e="T254" id="Seg_4081" s="T252">Дочкина [пара] ушла.</ta>
            <ta e="T259" id="Seg_4082" s="T254">Дома здесь жить плохо.</ta>
            <ta e="T271" id="Seg_4083" s="T259">Вот старик-великан, дочке пару отдав, послал сына на селькупскую землю, на реку Таз.</ta>
            <ta e="T277" id="Seg_4084" s="T271">Великана дочь пришла на старикову землю.</ta>
            <ta e="T282" id="Seg_4085" s="T277">Жить было плохо, мрачно.</ta>
            <ta e="T291" id="Seg_4086" s="T282">Сына с отцом оставил на реке Таз, на стариковой земле, на селькупской земле.</ta>
            <ta e="T295" id="Seg_4087" s="T291">"А я домой пойду".</ta>
            <ta e="T300" id="Seg_4088" s="T295">Пара великана-богача оставила [там] старика с его сыном.</ta>
            <ta e="T302" id="Seg_4089" s="T300">"Хорошо живите.</ta>
            <ta e="T305" id="Seg_4090" s="T302">Селькупских детей воспитывайте.</ta>
            <ta e="T313" id="Seg_4091" s="T305">Великана пара так сказала: "Мой сын, выращенный вместе с селькупами, [и они], пусть живут".</ta>
            <ta e="T317" id="Seg_4092" s="T313">Их сын большого телосложения.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_4093" s="T0">Badly lived one human, an orphan.</ta>
            <ta e="T11" id="Seg_4094" s="T6">He had neither clothing nor footwear.</ta>
            <ta e="T13" id="Seg_4095" s="T11">He lived nude.</ta>
            <ta e="T19" id="Seg_4096" s="T13">He thought: "How will I live alone?"</ta>
            <ta e="T31" id="Seg_4097" s="T19">If I go away, I will find, meet people".</ta>
            <ta e="T37" id="Seg_4098" s="T31">He went outside and went wherever his eyes led him.</ta>
            <ta e="T44" id="Seg_4099" s="T37">Once he saw a river appeared, a big river is seen.</ta>
            <ta e="T51" id="Seg_4100" s="T44">He comes down there, comes down, a big river is there.</ta>
            <ta e="T59" id="Seg_4101" s="T51">He looks down, a long reach and a fish weir are visible.</ta>
            <ta e="T65" id="Seg_4102" s="T59">"I came there, I cut tree pole bunches.</ta>
            <ta e="T68" id="Seg_4103" s="T65">The poles are too big to catch.</ta>
            <ta e="T75" id="Seg_4104" s="T68">I see a trace, apparently, two-feet big.</ta>
            <ta e="T83" id="Seg_4105" s="T75">There is a trace of a a bailer, of it's bolt.</ta>
            <ta e="T89" id="Seg_4106" s="T83">He looks down, it is a big bailer, a big bailer.</ta>
            <ta e="T94" id="Seg_4107" s="T89">"Down the river I see a devil coming".</ta>
            <ta e="T95" id="Seg_4108" s="T94">He got frightened.</ta>
            <ta e="T98" id="Seg_4109" s="T95">"I came earlier, I will stay here.</ta>
            <ta e="T103" id="Seg_4110" s="T98">[Even] if he eats me, I will still be here.</ta>
            <ta e="T105" id="Seg_4111" s="T103">Here he comes.</ta>
            <ta e="T108" id="Seg_4112" s="T105">He sees me.</ta>
            <ta e="T109" id="Seg_4113" s="T108">"A dolly!</ta>
            <ta e="T111" id="Seg_4114" s="T109">My daughter will play [with you].</ta>
            <ta e="T114" id="Seg_4115" s="T111">I will probably take you with me".</ta>
            <ta e="T116" id="Seg_4116" s="T114">He put him into his mitten.</ta>
            <ta e="T118" id="Seg_4117" s="T116">He went home with him.</ta>
            <ta e="T122" id="Seg_4118" s="T118">The human is sitting in the mitten.</ta>
            <ta e="T127" id="Seg_4119" s="T122">He looks upwards, a big tent is standing there.</ta>
            <ta e="T128" id="Seg_4120" s="T127">He came.</ta>
            <ta e="T131" id="Seg_4121" s="T128">He carries [dolly] into the tent.</ta>
            <ta e="T135" id="Seg_4122" s="T131">"Daughter, I found a dolly, play."</ta>
            <ta e="T136" id="Seg_4123" s="T135">They live.</ta>
            <ta e="T149" id="Seg_4124" s="T136">His old woman says to him: "Old man, make a bow, for a human, who used to live by himself, [could] eat meat of a wild animal".</ta>
            <ta e="T152" id="Seg_4125" s="T149">The old man makes a bow.</ta>
            <ta e="T154" id="Seg_4126" s="T152">"Let them [both] go".</ta>
            <ta e="T156" id="Seg_4127" s="T154">He makes ski.</ta>
            <ta e="T160" id="Seg_4128" s="T156">That human [dolly] skis.</ta>
            <ta e="T162" id="Seg_4129" s="T160">He takes his bow.</ta>
            <ta e="T168" id="Seg_4130" s="T162">The old man himself goes just so, without ski.</ta>
            <ta e="T173" id="Seg_4131" s="T168">Once he sees a reindeer trace.</ta>
            <ta e="T175" id="Seg_4132" s="T173">He catches up [= follows the trail].</ta>
            <ta e="T182" id="Seg_4133" s="T175">Dolly is skiing forward, telling him, that he sees a reindeer with his own eyes.</ta>
            <ta e="T186" id="Seg_4134" s="T182">He shoots one buck.</ta>
            <ta e="T188" id="Seg_4135" s="T186">He [the old man?] sits down.</ta>
            <ta e="T192" id="Seg_4136" s="T188">Then the old man comes back.</ta>
            <ta e="T198" id="Seg_4137" s="T192">He sees a buck lying, the dolly is sitting tired.</ta>
            <ta e="T205" id="Seg_4138" s="T198">The old man says: "I see Opsja, I will bring [it] home".</ta>
            <ta e="T207" id="Seg_4139" s="T205">He goes away.</ta>
            <ta e="T210" id="Seg_4140" s="T207">He breakes one larch with his hands.</ta>
            <ta e="T215" id="Seg_4141" s="T210">He stuck the buck with its legs into the middle [of the larch] (?).</ta>
            <ta e="T218" id="Seg_4142" s="T215">"Let's go home".</ta>
            <ta e="T220" id="Seg_4143" s="T218">He came home.</ta>
            <ta e="T228" id="Seg_4144" s="T220">[When] the old man brought Opsja, the old woman said so: "Good dolly".</ta>
            <ta e="T232" id="Seg_4145" s="T228">They live, their daughter and the dolly play together.</ta>
            <ta e="T237" id="Seg_4146" s="T232">She gave birth to a child, her son grows.</ta>
            <ta e="T243" id="Seg_4147" s="T237">Once the old man [says]: "Let's let our daughter and her partner go.</ta>
            <ta e="T247" id="Seg_4148" s="T243">The dolly's people are waiting at home."</ta>
            <ta e="T252" id="Seg_4149" s="T247">The old man and the old woman gave their daughter away.</ta>
            <ta e="T254" id="Seg_4150" s="T252">Their daughter's [partner?] went away.</ta>
            <ta e="T259" id="Seg_4151" s="T254">It is bad to live here at home.</ta>
            <ta e="T271" id="Seg_4152" s="T259">The giant old man, having given away his daughter's husband, sent [his] son to the Selkup land, to the river Taz.</ta>
            <ta e="T277" id="Seg_4153" s="T271">The giant's daughter came to the old man's land.</ta>
            <ta e="T282" id="Seg_4154" s="T277">Life was bad, dark.</ta>
            <ta e="T291" id="Seg_4155" s="T282">They left son and father on the river Taz, on the old man's land, in the Selkup land.</ta>
            <ta e="T295" id="Seg_4156" s="T291">"And I will go home".</ta>
            <ta e="T300" id="Seg_4157" s="T295">The giant and the rich man left the old man with his son there.</ta>
            <ta e="T302" id="Seg_4158" s="T300">"Live well.</ta>
            <ta e="T305" id="Seg_4159" s="T302">Grow up Selkup children.</ta>
            <ta e="T313" id="Seg_4160" s="T305">The giant's partner(?) said: "My son, grown up among the Selkup people, let them live".</ta>
            <ta e="T317" id="Seg_4161" s="T313">Their son has a big body.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_4162" s="T0">Es lebte ein Mensch schlecht, ein Waise.</ta>
            <ta e="T11" id="Seg_4163" s="T6">Er hatte weder Kleidung noch Schuhe.</ta>
            <ta e="T13" id="Seg_4164" s="T11">Er lebte nackt.</ta>
            <ta e="T19" id="Seg_4165" s="T13">Er dachte: "Wie soll ich alleine leben?"</ta>
            <ta e="T31" id="Seg_4166" s="T19">Wenn ich losgehe, werde ich Menschen finden, treffen."</ta>
            <ta e="T37" id="Seg_4167" s="T31">Er ging nach draußen und ging immer der Nase nach.</ta>
            <ta e="T44" id="Seg_4168" s="T37">Einmal schaute er, ein Fluss erschien, ein großer Fluss ist zu sehen.</ta>
            <ta e="T51" id="Seg_4169" s="T44">Er geht hinunter, geht hinunter, [dort] ist ein großer Fluss.</ta>
            <ta e="T59" id="Seg_4170" s="T51">Er schaut flussabwärts, ein langer Flussabschnitt und ein Fischwehr sind zu sehen.</ta>
            <ta e="T65" id="Seg_4171" s="T59">"Ich kam dorthin, ich hackte Rutenbündel.</ta>
            <ta e="T68" id="Seg_4172" s="T65">Die Pflöcke sind zu groß, um sie zu umfassen.</ta>
            <ta e="T75" id="Seg_4173" s="T68">Ich sehe eine Spur, sie ist offenbar zwei Fuß lang.</ta>
            <ta e="T83" id="Seg_4174" s="T75">Dort ist die Spur eines Fischfangkorbes, seines Riegels.</ta>
            <ta e="T89" id="Seg_4175" s="T83">Er schaut nach unten, es ist ein großer Fischfangkorb, ein großer Fischfangkorb.</ta>
            <ta e="T94" id="Seg_4176" s="T89">"Flussabwärts sehe ich wohl den Teufel kommen."</ta>
            <ta e="T95" id="Seg_4177" s="T94">Er erschrak.</ta>
            <ta e="T98" id="Seg_4178" s="T95">"Ich war früher hier, ich bleibe hier.</ta>
            <ta e="T103" id="Seg_4179" s="T98">[Sogar] wenn er mich frisst, bleibe ich hier.</ta>
            <ta e="T105" id="Seg_4180" s="T103">Da kommt er.</ta>
            <ta e="T108" id="Seg_4181" s="T105">Er sieht mich.</ta>
            <ta e="T109" id="Seg_4182" s="T108">"Ein Püppchen!</ta>
            <ta e="T111" id="Seg_4183" s="T109">Meine Tochter wird [mit dir] spielen.</ta>
            <ta e="T114" id="Seg_4184" s="T111">Ich werde dich wohl mitnehmen."</ta>
            <ta e="T116" id="Seg_4185" s="T114">Er setzte ihn in seinen Fäustling.</ta>
            <ta e="T118" id="Seg_4186" s="T116">Er brachte ihn nach Hause.</ta>
            <ta e="T122" id="Seg_4187" s="T118">Der Mensch sitzt in dem Fäustling.</ta>
            <ta e="T127" id="Seg_4188" s="T122">Er schaut nach oben, dort steht ein großes Zelt.</ta>
            <ta e="T128" id="Seg_4189" s="T127">Er kam an.</ta>
            <ta e="T131" id="Seg_4190" s="T128">Er trägt [das Püppchen] ins Zelt hinein.</ta>
            <ta e="T135" id="Seg_4191" s="T131">"Tochter, ich habe ein Püppchen gefunden, spiel."</ta>
            <ta e="T136" id="Seg_4192" s="T135">Sie leben.</ta>
            <ta e="T149" id="Seg_4193" s="T136">Seine Alte sagt: "Alter Mann, mach einen Bogen, damit der Mensch, der früher alleine lebte, Wildfleisch essen kann."</ta>
            <ta e="T152" id="Seg_4194" s="T149">Der Alte macht einen Bogen.</ta>
            <ta e="T154" id="Seg_4195" s="T152">"Sollen die beiden [zusammen] gehen."</ta>
            <ta e="T156" id="Seg_4196" s="T154">Er macht Skier.</ta>
            <ta e="T160" id="Seg_4197" s="T156">Dieser Mensch [das Püppchen] läuft Ski.</ta>
            <ta e="T162" id="Seg_4198" s="T160">Er nimmt seinen Bogen mit.</ta>
            <ta e="T168" id="Seg_4199" s="T162">Der Alte geht einfach so, ohne Skier.</ta>
            <ta e="T173" id="Seg_4200" s="T168">Einmal sieht er eine Rentierspur.</ta>
            <ta e="T175" id="Seg_4201" s="T173">Er holt sie ein [= folgt ihr].</ta>
            <ta e="T182" id="Seg_4202" s="T175">Das Püppchen läuft vorne Ski und sagt ihm, dass es mit [seinen] Augen ein Rentier sieht.</ta>
            <ta e="T186" id="Seg_4203" s="T182">Es schießt mit dem Bogen auf einen Bock.</ta>
            <ta e="T188" id="Seg_4204" s="T186">Er [der Alte?] setzt sich.</ta>
            <ta e="T192" id="Seg_4205" s="T188">Dann kommt der Alte zurück.</ta>
            <ta e="T198" id="Seg_4206" s="T192">Er sieht den Bock liegen, das Püppchen sitzt da, es ist müde.</ta>
            <ta e="T205" id="Seg_4207" s="T198">Der Alte sagt: "Ich sehe Opsja, ich bringe [ihn] nach Hause."</ta>
            <ta e="T207" id="Seg_4208" s="T205">Er geht weg.</ta>
            <ta e="T210" id="Seg_4209" s="T207">Er zerbricht eine Lärche mit seinen Händen.</ta>
            <ta e="T215" id="Seg_4210" s="T210">Er steckte den Bock mit seinen Beinen in die Mitte [der Lärche] (?).</ta>
            <ta e="T218" id="Seg_4211" s="T215">"Lass uns nach Hause gehen."</ta>
            <ta e="T220" id="Seg_4212" s="T218">Er kam nach Hause.</ta>
            <ta e="T228" id="Seg_4213" s="T220">[Als] der Alte Opsja nach Hause gebracht hat, sagte die Alte so: "Gutes Püppchen."</ta>
            <ta e="T232" id="Seg_4214" s="T228">Sie leben, ihre Tochter [und das Püppchen] spielen zusammen.</ta>
            <ta e="T237" id="Seg_4215" s="T232">Sie gebiert ein Kind, ihr Sohn wächst.</ta>
            <ta e="T243" id="Seg_4216" s="T237">Einmal [sagt] der Alte: "Lass uns unsere Tochter und ihren Partner gehen lassen. </ta>
            <ta e="T247" id="Seg_4217" s="T243">Die Leute des Püppchen warten zuhause."</ta>
            <ta e="T252" id="Seg_4218" s="T247">Der Alte und die Alte gaben ihre Tochter weg.</ta>
            <ta e="T254" id="Seg_4219" s="T252">[Der Partner?] ihrer Tochter ging weg.</ta>
            <ta e="T259" id="Seg_4220" s="T254">Es ist schlecht, hier zu Hause zu leben.</ta>
            <ta e="T271" id="Seg_4221" s="T259">Nachdem der riesige Alte seine Tochter weggegeben hatte, schickte er [seinen] Sohn in das selkupische Land zum Fluss Taz.</ta>
            <ta e="T277" id="Seg_4222" s="T271">Die Tochter des Riesen kam im Land des alten Mannes an.</ta>
            <ta e="T282" id="Seg_4223" s="T277">Das Leben war schlecht, dunkel.</ta>
            <ta e="T291" id="Seg_4224" s="T282">Sie ließen den Sohn und mit seinem Vater am Fluss Taz, im Land des alten Mannes, im selkupischen Land.</ta>
            <ta e="T295" id="Seg_4225" s="T291">"Und ich werde nach Hause gehen"</ta>
            <ta e="T300" id="Seg_4226" s="T295">Der Riese und der reiche Mann ließen den Alten mit seinem Sohn dort.</ta>
            <ta e="T302" id="Seg_4227" s="T300">"Leb wohl.</ta>
            <ta e="T305" id="Seg_4228" s="T302">Zieht selkupische Kinder groß.</ta>
            <ta e="T313" id="Seg_4229" s="T305">Der Partner(?) des Riesen sagte: "Mein unter Selkupen aufgewachsener Sohn, sie sollen leben." </ta>
            <ta e="T317" id="Seg_4230" s="T313">Der Körper ihres Sohnes ist groß.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_4231" s="T0">плохо жил один человек беспризорник [сирота]</ta>
            <ta e="T11" id="Seg_4232" s="T6">ни пальто ни валенок не было </ta>
            <ta e="T13" id="Seg_4233" s="T11">голый [разутый] жил</ta>
            <ta e="T19" id="Seg_4234" s="T13">думал как поживу [буду жить] один</ta>
            <ta e="T31" id="Seg_4235" s="T19">пойду [ушел бы] где-нибудь [куда-нибудь]</ta>
            <ta e="T37" id="Seg_4236" s="T31">вышел на улицу и ушел куда глаза глядят</ta>
            <ta e="T44" id="Seg_4237" s="T37">однажды [один раз] смотрит [видит] в Таз вижу </ta>
            <ta e="T51" id="Seg_4238" s="T44">туда пришел вижу река Таз</ta>
            <ta e="T59" id="Seg_4239" s="T51">вниз смотрит длинный плёс запор видать</ta>
            <ta e="T65" id="Seg_4240" s="T59">пришел туда запорные колья (жерди) через Таз наставил</ta>
            <ta e="T68" id="Seg_4241" s="T65">крупные рейка рука не берет (кое-как хватает)</ta>
            <ta e="T75" id="Seg_4242" s="T68">след увижу две ступни не хватает большой</ta>
            <ta e="T83" id="Seg_4243" s="T75">вентель спущен с конца рыба плавает в вентилях вентиля</ta>
            <ta e="T89" id="Seg_4244" s="T83">вниз (под собой) смотрю сак (?) большой огромный</ta>
            <ta e="T94" id="Seg_4245" s="T89">вниз по течению смотрит что (что ли) черт идет</ta>
            <ta e="T95" id="Seg_4246" s="T94">испугался</ta>
            <ta e="T98" id="Seg_4247" s="T95">так и пришел обратно </ta>
            <ta e="T103" id="Seg_4248" s="T98">если чего сделает со мной, или съест, или нет стою тут же</ta>
            <ta e="T105" id="Seg_4249" s="T103">ты пришел</ta>
            <ta e="T108" id="Seg_4250" s="T105"> увидел его </ta>
            <ta e="T109" id="Seg_4251" s="T108">маленький чертенок</ta>
            <ta e="T111" id="Seg_4252" s="T109">дочь будет играть.</ta>
            <ta e="T114" id="Seg_4253" s="T111">унесу домой</ta>
            <ta e="T116" id="Seg_4254" s="T114">в рукавицу посадил</ta>
            <ta e="T118" id="Seg_4255" s="T116">домой пошел</ta>
            <ta e="T122" id="Seg_4256" s="T118">сидит в рукавичке он</ta>
            <ta e="T127" id="Seg_4257" s="T122">видит большой чум сели </ta>
            <ta e="T128" id="Seg_4258" s="T127">вошел в чум</ta>
            <ta e="T131" id="Seg_4259" s="T128">заносит в чум его</ta>
            <ta e="T135" id="Seg_4260" s="T131">дочка с куколкой играет</ta>
            <ta e="T149" id="Seg_4261" s="T136">его старуха так сказала мужик лук</ta>
            <ta e="T152" id="Seg_4262" s="T149">старик лук сделал</ta>
            <ta e="T156" id="Seg_4263" s="T154">лыжи сделал</ta>
            <ta e="T160" id="Seg_4264" s="T156">этот человек на лыжах пошел</ta>
            <ta e="T168" id="Seg_4265" s="T162">сам старик просто пошел без лыж</ta>
            <ta e="T198" id="Seg_4266" s="T192">чертенок (куколка) сидит устал</ta>
            <ta e="T205" id="Seg_4267" s="T198">старик так</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T205" id="Seg_4268" s="T198">[OSV]: not sure in the correct interpretation of the word "opsä", but according to IES it can be the name of the doll.</ta>
            <ta e="T228" id="Seg_4269" s="T220">[OSV]: not sure in the correct interpretation of the word "opsä", but according to IES it can be the name of the doll.</ta>
            <ta e="T237" id="Seg_4270" s="T232">[OSV]: Here the verb "qoqo" has the meaning "to give birth".</ta>
            <ta e="T277" id="Seg_4271" s="T271">[OSV]: The word "mätäk" has been edited into "nätäk". A possible interpretation is "pünakəsa-lʼ mɨ-qaːqı-^0" (giant-ADJZ something-DU-NOM).</ta>
            <ta e="T282" id="Seg_4272" s="T277">[OSV]: unclear adverbial form "säːqasɨk".</ta>
            <ta e="T313" id="Seg_4273" s="T305">[OSV]: the nominal form "künakɨsalʼ" has been edited into "pünakɨsalʼ".</ta>
            <ta e="T317" id="Seg_4274" s="T313">[OSV]: A possible interpretation is "ɛː-ŋaijäːtat" (be-IMP.3PL.S).</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
