<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID6A7F6564-6713-0E1A-8946-E4D720745812">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">flk\SVG_1964_Stars_flk\SVG_1964_Stars_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">60</ud-information>
            <ud-information attribute-name="# HIAT:w">46</ud-information>
            <ud-information attribute-name="# e">33</ud-information>
            <ud-information attribute-name="# HIAT:u">13</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SVG">
            <abbreviation>SVG</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T42" />
         <tli id="T53" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SVG"
                      type="t">
         <timeline-fork end="T20" start="T12">
            <tli id="T12.tx.1" />
            <tli id="T12.tx.2" />
            <tli id="T12.tx.3" />
            <tli id="T12.tx.4" />
            <tli id="T12.tx.5" />
            <tli id="T12.tx.6" />
            <tli id="T12.tx.7" />
         </timeline-fork>
         <timeline-fork end="T27" start="T20">
            <tli id="T20.tx.1" />
            <tli id="T20.tx.2" />
            <tli id="T20.tx.3" />
            <tli id="T20.tx.4" />
            <tli id="T20.tx.5" />
            <tli id="T20.tx.6" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T51" id="Seg_0" n="sc" s="T0">
               <ts e="T1" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">kɨsaŋa</ts>
                  <nts id="Seg_5" n="HIAT:ip">.</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_8" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_10" n="HIAT:w" s="T1">qaraj</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">qwändaːntə</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">paːri</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">kɨsaːŋa</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_23" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">üːtaj</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">qwändaːntə</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">paːri</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">kɨsaːŋa</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_38" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">qwärgaːj</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">kɨsaŋaj</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_46" n="HIAT:w" s="T11">pärat</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_50" n="HIAT:u" s="T42">
                  <ts e="T53" id="Seg_52" n="HIAT:w" s="T42">kɨsaŋaj</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_55" n="HIAT:w" s="T53">pärat</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_59" n="HIAT:u" s="T12">
                  <ts e="T12.tx.1" id="Seg_61" n="HIAT:w" s="T12">ranʼše</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12.tx.2" id="Seg_64" n="HIAT:w" s="T12.tx.1">kto-to</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12.tx.3" id="Seg_67" n="HIAT:w" s="T12.tx.2">gonʼal</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12.tx.4" id="Seg_70" n="HIAT:w" s="T12.tx.3">soxatogo</ts>
                  <nts id="Seg_71" n="HIAT:ip">,</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12.tx.5" id="Seg_74" n="HIAT:w" s="T12.tx.4">on</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12.tx.6" id="Seg_77" n="HIAT:w" s="T12.tx.5">tuda</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12.tx.7" id="Seg_80" n="HIAT:w" s="T12.tx.6">i</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_83" n="HIAT:w" s="T12.tx.7">uletel</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_87" n="HIAT:u" s="T20">
                  <ts e="T20.tx.1" id="Seg_89" n="HIAT:w" s="T20">u</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20.tx.2" id="Seg_92" n="HIAT:w" s="T20.tx.1">poslednej</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20.tx.3" id="Seg_95" n="HIAT:w" s="T20.tx.2">zvezdy</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20.tx.4" id="Seg_98" n="HIAT:w" s="T20.tx.3">kak</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20.tx.5" id="Seg_101" n="HIAT:w" s="T20.tx.4">budto</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20.tx.6" id="Seg_104" n="HIAT:w" s="T20.tx.5">by</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_107" n="HIAT:w" s="T20.tx.6">katomočka</ts>
                  <nts id="Seg_108" n="HIAT:ip">.</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_111" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_113" n="HIAT:w" s="T27">Iːtʼtʼe</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_116" n="HIAT:w" s="T28">peŋgam</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_119" n="HIAT:w" s="T29">nʼöɣat</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_123" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_125" n="HIAT:w" s="T30">Iːtʼen</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_128" n="HIAT:w" s="T31">ɨlʼdʼät</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_132" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_134" n="HIAT:w" s="T32">kazɨkɨn</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_137" n="HIAT:w" s="T33">tɨn</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_140" n="HIAT:w" s="T34">ɨːlaj</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_143" n="HIAT:w" s="T35">tʼepartə</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_147" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_149" n="HIAT:w" s="T43">nʼöj</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_152" n="HIAT:w" s="T44">pärat</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_156" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_158" n="HIAT:w" s="T45">Iːtʼen</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_161" n="HIAT:w" s="T46">naːr</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_164" n="HIAT:w" s="T47">kaːsaj</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_167" n="HIAT:w" s="T48">poŋgə</ts>
                  <nts id="Seg_168" n="HIAT:ip">.</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_171" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_173" n="HIAT:w" s="T49">Iːtʼen</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_176" n="HIAT:w" s="T50">tɨsse</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T51" id="Seg_179" n="sc" s="T0">
               <ts e="T1" id="Seg_181" n="e" s="T0">kɨsaŋa. </ts>
               <ts e="T2" id="Seg_183" n="e" s="T1">qaraj </ts>
               <ts e="T3" id="Seg_185" n="e" s="T2">qwändaːntə </ts>
               <ts e="T4" id="Seg_187" n="e" s="T3">paːri </ts>
               <ts e="T5" id="Seg_189" n="e" s="T4">kɨsaːŋa. </ts>
               <ts e="T6" id="Seg_191" n="e" s="T5">üːtaj </ts>
               <ts e="T7" id="Seg_193" n="e" s="T6">qwändaːntə </ts>
               <ts e="T8" id="Seg_195" n="e" s="T7">paːri </ts>
               <ts e="T9" id="Seg_197" n="e" s="T8">kɨsaːŋa. </ts>
               <ts e="T10" id="Seg_199" n="e" s="T9">qwärgaːj </ts>
               <ts e="T11" id="Seg_201" n="e" s="T10">kɨsaŋaj </ts>
               <ts e="T42" id="Seg_203" n="e" s="T11">pärat. </ts>
               <ts e="T53" id="Seg_205" n="e" s="T42">kɨsaŋaj </ts>
               <ts e="T12" id="Seg_207" n="e" s="T53">pärat. </ts>
               <ts e="T20" id="Seg_209" n="e" s="T12">ranʼše kto-to gonʼal soxatogo, on tuda i uletel. </ts>
               <ts e="T27" id="Seg_211" n="e" s="T20">u poslednej zvezdy kak budto by katomočka. </ts>
               <ts e="T28" id="Seg_213" n="e" s="T27">Iːtʼtʼe </ts>
               <ts e="T29" id="Seg_215" n="e" s="T28">peŋgam </ts>
               <ts e="T30" id="Seg_217" n="e" s="T29">nʼöɣat. </ts>
               <ts e="T31" id="Seg_219" n="e" s="T30">Iːtʼen </ts>
               <ts e="T32" id="Seg_221" n="e" s="T31">ɨlʼdʼät. </ts>
               <ts e="T33" id="Seg_223" n="e" s="T32">kazɨkɨn </ts>
               <ts e="T34" id="Seg_225" n="e" s="T33">tɨn </ts>
               <ts e="T35" id="Seg_227" n="e" s="T34">ɨːlaj </ts>
               <ts e="T43" id="Seg_229" n="e" s="T35">tʼepartə. </ts>
               <ts e="T44" id="Seg_231" n="e" s="T43">nʼöj </ts>
               <ts e="T45" id="Seg_233" n="e" s="T44">pärat. </ts>
               <ts e="T46" id="Seg_235" n="e" s="T45">Iːtʼen </ts>
               <ts e="T47" id="Seg_237" n="e" s="T46">naːr </ts>
               <ts e="T48" id="Seg_239" n="e" s="T47">kaːsaj </ts>
               <ts e="T49" id="Seg_241" n="e" s="T48">poŋgə. </ts>
               <ts e="T50" id="Seg_243" n="e" s="T49">Iːtʼen </ts>
               <ts e="T51" id="Seg_245" n="e" s="T50">tɨsse. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1" id="Seg_246" s="T0">SVG_1964_Stars_flk.001 (001.001)</ta>
            <ta e="T5" id="Seg_247" s="T1">SVG_1964_Stars_flk.002 (001.002)</ta>
            <ta e="T9" id="Seg_248" s="T5">SVG_1964_Stars_flk.003 (001.003)</ta>
            <ta e="T42" id="Seg_249" s="T9">SVG_1964_Stars_flk.004 (001.004)</ta>
            <ta e="T12" id="Seg_250" s="T42">SVG_1964_Stars_flk.005 (001.005)</ta>
            <ta e="T20" id="Seg_251" s="T12">SVG_1964_Stars_flk.006 (001.006)</ta>
            <ta e="T27" id="Seg_252" s="T20">SVG_1964_Stars_flk.007 (001.007)</ta>
            <ta e="T30" id="Seg_253" s="T27">SVG_1964_Stars_flk.008 (001.008)</ta>
            <ta e="T32" id="Seg_254" s="T30">SVG_1964_Stars_flk.009 (001.009)</ta>
            <ta e="T43" id="Seg_255" s="T32">SVG_1964_Stars_flk.010 (001.010)</ta>
            <ta e="T45" id="Seg_256" s="T43">SVG_1964_Stars_flk.011 (001.011)</ta>
            <ta e="T49" id="Seg_257" s="T45">SVG_1964_Stars_flk.012 (001.012)</ta>
            <ta e="T51" id="Seg_258" s="T49">SVG_1964_Stars_flk.013 (001.013)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T1" id="Seg_259" s="T0">кы ′саңа</ta>
            <ta e="T5" id="Seg_260" s="T1">kа′рай kвäн′д̂а̄нтъ ′па̄ри кы′са̄ңа</ta>
            <ta e="T9" id="Seg_261" s="T5">′ӱ̅тай kвäн′д̂а̄нтъ ′па̄ри кы′з̂а̄ңа</ta>
            <ta e="T42" id="Seg_262" s="T9">′kвäрга̄й кы′саңай ′пäрат</ta>
            <ta e="T12" id="Seg_263" s="T42">кы′саңай ′пäрат</ta>
            <ta e="T20" id="Seg_264" s="T12">Раньше кто-то гонял сохатого, он туда и улетел.</ta>
            <ta e="T27" id="Seg_265" s="T20">У последней звезды как будто бы катомочка. </ta>
            <ta e="T30" id="Seg_266" s="T27">ӣтʼтʼе ′пеңгам ′нʼöɣам</ta>
            <ta e="T32" id="Seg_267" s="T30">Ӣтʼен(нылʼ) ′ылʼдʼäт</ta>
            <ta e="T43" id="Seg_268" s="T32">′казыkын ′тын ′ы̄лай тʼе′парты (ъ)</ta>
            <ta e="T45" id="Seg_269" s="T43">′нʼöй ′пäрат</ta>
            <ta e="T49" id="Seg_270" s="T45">ӣт′ен на̄р ка̄′сай ′поңгъ</ta>
            <ta e="T51" id="Seg_271" s="T49">itʼen tɨsse</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T1" id="Seg_272" s="T0">kɨsaŋa</ta>
            <ta e="T5" id="Seg_273" s="T1">qaraj qwändaːntə paːri kɨsaːŋa</ta>
            <ta e="T9" id="Seg_274" s="T5">üːtaj qwändaːntə paːri kɨsaːŋa</ta>
            <ta e="T42" id="Seg_275" s="T9">qwärgaːj kɨsaŋaj pärat</ta>
            <ta e="T12" id="Seg_276" s="T42">kɨsaŋaj pärat</ta>
            <ta e="T20" id="Seg_277" s="T12">ranʼše kto-to gonʼal soxatogo, on tuda i uletel. </ta>
            <ta e="T27" id="Seg_278" s="T20">u poslednej zvezdy kak budto by katomočka. </ta>
            <ta e="T30" id="Seg_279" s="T27">Iːtʼtʼe peŋgam nʼöɣat</ta>
            <ta e="T32" id="Seg_280" s="T30">Iːtʼen ɨlʼdʼät</ta>
            <ta e="T43" id="Seg_281" s="T32">kazɨqɨn tɨn ɨːlaj tʼepartɨ</ta>
            <ta e="T45" id="Seg_282" s="T43">nʼöj pärat.</ta>
            <ta e="T49" id="Seg_283" s="T45">iːtʼen naːr kaːsaj poŋgə</ta>
            <ta e="T51" id="Seg_284" s="T49">′итен ты′ссе</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1" id="Seg_285" s="T0">kɨsaŋa. </ta>
            <ta e="T5" id="Seg_286" s="T1">qaraj qwändaːntə paːri kɨsaːŋa. </ta>
            <ta e="T9" id="Seg_287" s="T5">üːtaj qwändaːntə paːri kɨsaːŋa. </ta>
            <ta e="T42" id="Seg_288" s="T9">qwärgaːj kɨsaŋaj pärat. </ta>
            <ta e="T12" id="Seg_289" s="T42">kɨsaŋaj pärat. </ta>
            <ta e="T20" id="Seg_290" s="T12">ranʼše kto-to gonʼal soxatogo, on tuda i uletel. </ta>
            <ta e="T27" id="Seg_291" s="T20">u poslednej zvezdy kak budto by katomočka. </ta>
            <ta e="T30" id="Seg_292" s="T27">Iːtʼtʼe peŋgam nʼöɣat. </ta>
            <ta e="T32" id="Seg_293" s="T30">Iːtʼen ɨlʼdʼät. </ta>
            <ta e="T43" id="Seg_294" s="T32">kazɨkɨn tɨn ɨːlaj tʼepartə. </ta>
            <ta e="T45" id="Seg_295" s="T43">nʼöj pärat. </ta>
            <ta e="T49" id="Seg_296" s="T45">Iːtʼen naːr kaːsaj poŋgə. </ta>
            <ta e="T51" id="Seg_297" s="T49">Iːtʼen tɨsse. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_298" s="T0">kɨsaŋ-a</ta>
            <ta e="T2" id="Seg_299" s="T1">qara-j</ta>
            <ta e="T3" id="Seg_300" s="T2">qwändaː-n-tə</ta>
            <ta e="T4" id="Seg_301" s="T3">paːri</ta>
            <ta e="T5" id="Seg_302" s="T4">kɨsaːŋ-a</ta>
            <ta e="T6" id="Seg_303" s="T5">üːta-j</ta>
            <ta e="T7" id="Seg_304" s="T6">qwändaː-n-tə</ta>
            <ta e="T8" id="Seg_305" s="T7">paːri</ta>
            <ta e="T9" id="Seg_306" s="T8">kɨsaːŋ-a</ta>
            <ta e="T10" id="Seg_307" s="T9">qwärgaː-j</ta>
            <ta e="T11" id="Seg_308" s="T10">kɨsaŋ-a-j</ta>
            <ta e="T42" id="Seg_309" s="T11">pära-t</ta>
            <ta e="T53" id="Seg_310" s="T42">kɨsaŋ-a-j</ta>
            <ta e="T12" id="Seg_311" s="T53">pära-t</ta>
            <ta e="T28" id="Seg_312" s="T27">Iːtʼtʼe</ta>
            <ta e="T29" id="Seg_313" s="T28">peŋga-m</ta>
            <ta e="T30" id="Seg_314" s="T29">nʼö-ɣa-t</ta>
            <ta e="T31" id="Seg_315" s="T30">iːtʼe-n</ta>
            <ta e="T32" id="Seg_316" s="T31">ɨlʼdʼä-t</ta>
            <ta e="T33" id="Seg_317" s="T32">kazɨ-kɨn</ta>
            <ta e="T34" id="Seg_318" s="T33">tɨ-n</ta>
            <ta e="T35" id="Seg_319" s="T34">ɨːla-j</ta>
            <ta e="T43" id="Seg_320" s="T35">tʼepartə</ta>
            <ta e="T44" id="Seg_321" s="T43">nʼö-j</ta>
            <ta e="T45" id="Seg_322" s="T44">pära-t</ta>
            <ta e="T46" id="Seg_323" s="T45">Iːtʼe-n</ta>
            <ta e="T47" id="Seg_324" s="T46">naːr</ta>
            <ta e="T48" id="Seg_325" s="T47">kaːs-a-j</ta>
            <ta e="T49" id="Seg_326" s="T48">poŋgə</ta>
            <ta e="T50" id="Seg_327" s="T49">Iːtʼe-n</ta>
            <ta e="T51" id="Seg_328" s="T50">tɨsse</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_329" s="T0">kɨsaŋ-la</ta>
            <ta e="T2" id="Seg_330" s="T1">qarɨ-lʼ</ta>
            <ta e="T3" id="Seg_331" s="T2">qwänd-n-t</ta>
            <ta e="T4" id="Seg_332" s="T3">par</ta>
            <ta e="T5" id="Seg_333" s="T4">kɨsaŋ-la</ta>
            <ta e="T6" id="Seg_334" s="T5">üːtǝ-lʼ</ta>
            <ta e="T7" id="Seg_335" s="T6">qwänd-n-t</ta>
            <ta e="T8" id="Seg_336" s="T7">par</ta>
            <ta e="T9" id="Seg_337" s="T8">kɨsaŋ-la</ta>
            <ta e="T10" id="Seg_338" s="T9">qwärqa-lʼ</ta>
            <ta e="T11" id="Seg_339" s="T10">kɨsaŋ-ɨ-lʼ</ta>
            <ta e="T42" id="Seg_340" s="T11">pori-t</ta>
            <ta e="T53" id="Seg_341" s="T42">kɨsaŋ-ɨ-lʼ</ta>
            <ta e="T12" id="Seg_342" s="T53">pori-t</ta>
            <ta e="T28" id="Seg_343" s="T27">Itʼe</ta>
            <ta e="T29" id="Seg_344" s="T28">päŋqa-m</ta>
            <ta e="T30" id="Seg_345" s="T29">nö-ŋɨ-tɨ</ta>
            <ta e="T31" id="Seg_346" s="T30">Itʼe-n</ta>
            <ta e="T32" id="Seg_347" s="T31">ɨlʼdʼä-t</ta>
            <ta e="T33" id="Seg_348" s="T32">kazɨ-qən</ta>
            <ta e="T34" id="Seg_349" s="T33">tɛː-n</ta>
            <ta e="T35" id="Seg_350" s="T34">ilɨ-j</ta>
            <ta e="T43" id="Seg_351" s="T35">tʼepartə</ta>
            <ta e="T44" id="Seg_352" s="T43">nʼo-lʼ</ta>
            <ta e="T45" id="Seg_353" s="T44">pori-t</ta>
            <ta e="T46" id="Seg_354" s="T45">Itʼe-n</ta>
            <ta e="T47" id="Seg_355" s="T46">nakkɨr</ta>
            <ta e="T48" id="Seg_356" s="T47">qaːs-ɨ-lʼ</ta>
            <ta e="T49" id="Seg_357" s="T48">poqqo</ta>
            <ta e="T50" id="Seg_358" s="T49">Itʼe-n</ta>
            <ta e="T51" id="Seg_359" s="T50">täsä</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_360" s="T0">stern-PL.[NOM]</ta>
            <ta e="T2" id="Seg_361" s="T1">morning-ADJZ</ta>
            <ta e="T3" id="Seg_362" s="T2">dawn-GEN-3SG</ta>
            <ta e="T4" id="Seg_363" s="T3">top.[NOM]</ta>
            <ta e="T5" id="Seg_364" s="T4">stern-PL.[NOM]</ta>
            <ta e="T6" id="Seg_365" s="T5">evening-ADJZ</ta>
            <ta e="T7" id="Seg_366" s="T6">dawn-GEN-3SG</ta>
            <ta e="T8" id="Seg_367" s="T7">top.[NOM]</ta>
            <ta e="T9" id="Seg_368" s="T8">stern-PL.[NOM]</ta>
            <ta e="T10" id="Seg_369" s="T9">bear-ADJZ</ta>
            <ta e="T11" id="Seg_370" s="T10">stern-EP-ADJZ</ta>
            <ta e="T42" id="Seg_371" s="T11">storehouse.for.souls-3SG</ta>
            <ta e="T53" id="Seg_372" s="T42">stern-EP-ADJZ</ta>
            <ta e="T12" id="Seg_373" s="T53">storehouse.for.souls-3SG</ta>
            <ta e="T28" id="Seg_374" s="T27">Itja.[NOM]</ta>
            <ta e="T29" id="Seg_375" s="T28">elk-ACC</ta>
            <ta e="T30" id="Seg_376" s="T29">hunt-CO-3SG.O</ta>
            <ta e="T31" id="Seg_377" s="T30">Itja-GEN</ta>
            <ta e="T32" id="Seg_378" s="T31">father.in.law-3SG</ta>
            <ta e="T33" id="Seg_379" s="T32">Tym.river-LOC</ta>
            <ta e="T34" id="Seg_380" s="T33">here-ADV.LOC</ta>
            <ta e="T35" id="Seg_381" s="T34">live-1DU</ta>
            <ta e="T43" id="Seg_382" s="T35">%%</ta>
            <ta e="T44" id="Seg_383" s="T43">hare-ADJZ</ta>
            <ta e="T45" id="Seg_384" s="T44">storehouse.for.souls.[NOM]-3SG</ta>
            <ta e="T46" id="Seg_385" s="T45">Itja-GEN</ta>
            <ta e="T47" id="Seg_386" s="T46">three</ta>
            <ta e="T48" id="Seg_387" s="T47">float-EP-ADJZ</ta>
            <ta e="T49" id="Seg_388" s="T48">net.[NOM]</ta>
            <ta e="T50" id="Seg_389" s="T49">Itja-GEN</ta>
            <ta e="T51" id="Seg_390" s="T50">arrow.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_391" s="T0">звезда-PL.[NOM]</ta>
            <ta e="T2" id="Seg_392" s="T1">утро-ADJZ</ta>
            <ta e="T3" id="Seg_393" s="T2">заря-GEN-3SG</ta>
            <ta e="T4" id="Seg_394" s="T3">верх.[NOM]</ta>
            <ta e="T5" id="Seg_395" s="T4">звезда-PL.[NOM]</ta>
            <ta e="T6" id="Seg_396" s="T5">вечер-ADJZ</ta>
            <ta e="T7" id="Seg_397" s="T6">заря-GEN-3SG</ta>
            <ta e="T8" id="Seg_398" s="T7">верх.[NOM]</ta>
            <ta e="T9" id="Seg_399" s="T8">звезда-PL.[NOM]</ta>
            <ta e="T10" id="Seg_400" s="T9">медведь-ADJZ</ta>
            <ta e="T11" id="Seg_401" s="T10">звезда-EP-ADJZ</ta>
            <ta e="T42" id="Seg_402" s="T11">амбар.для.духов-3SG</ta>
            <ta e="T53" id="Seg_403" s="T42">звезда-EP-ADJZ</ta>
            <ta e="T12" id="Seg_404" s="T53">амбар.для.духов-3SG</ta>
            <ta e="T28" id="Seg_405" s="T27">Ича.[NOM]</ta>
            <ta e="T29" id="Seg_406" s="T28">лось-ACC</ta>
            <ta e="T30" id="Seg_407" s="T29">гнать-CO-3SG.O</ta>
            <ta e="T31" id="Seg_408" s="T30">Ича-GEN</ta>
            <ta e="T32" id="Seg_409" s="T31">свёкор-3SG</ta>
            <ta e="T33" id="Seg_410" s="T32">Tым.река-LOC</ta>
            <ta e="T34" id="Seg_411" s="T33">сюда-ADV.LOC</ta>
            <ta e="T35" id="Seg_412" s="T34">жить-1DU</ta>
            <ta e="T43" id="Seg_413" s="T35">%%</ta>
            <ta e="T44" id="Seg_414" s="T43">заяц-ADJZ</ta>
            <ta e="T45" id="Seg_415" s="T44">амбар.для.духов.[NOM]-3SG</ta>
            <ta e="T46" id="Seg_416" s="T45">Ича-GEN</ta>
            <ta e="T47" id="Seg_417" s="T46">три</ta>
            <ta e="T48" id="Seg_418" s="T47">поплавок-EP-ADJZ</ta>
            <ta e="T49" id="Seg_419" s="T48">сеть.[NOM]</ta>
            <ta e="T50" id="Seg_420" s="T49">Ича-GEN</ta>
            <ta e="T51" id="Seg_421" s="T50">стрела.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_422" s="T0">n-n:num-n:case1</ta>
            <ta e="T2" id="Seg_423" s="T1">n-n&gt;adj</ta>
            <ta e="T3" id="Seg_424" s="T2">n-n:case1-n:poss</ta>
            <ta e="T4" id="Seg_425" s="T3">n-n:case1</ta>
            <ta e="T5" id="Seg_426" s="T4">n-n:num-n:case1</ta>
            <ta e="T6" id="Seg_427" s="T5">n-n&gt;adj</ta>
            <ta e="T7" id="Seg_428" s="T6">n-n:case1-n:poss</ta>
            <ta e="T8" id="Seg_429" s="T7">n-n:case1</ta>
            <ta e="T9" id="Seg_430" s="T8">n-n:num-n:case1</ta>
            <ta e="T10" id="Seg_431" s="T9">n-n&gt;adj</ta>
            <ta e="T11" id="Seg_432" s="T10">n-n:ins-n&gt;adj</ta>
            <ta e="T42" id="Seg_433" s="T11">n-n:poss</ta>
            <ta e="T53" id="Seg_434" s="T42">n-n:ins-n&gt;adj</ta>
            <ta e="T12" id="Seg_435" s="T53">n-n:poss</ta>
            <ta e="T28" id="Seg_436" s="T27">nprop-n:case1</ta>
            <ta e="T29" id="Seg_437" s="T28">n-n:case1</ta>
            <ta e="T30" id="Seg_438" s="T29">v-v:ins-v:pn</ta>
            <ta e="T31" id="Seg_439" s="T30">nprop-n:case1</ta>
            <ta e="T32" id="Seg_440" s="T31">n-n:case1-n:poss</ta>
            <ta e="T33" id="Seg_441" s="T32">nprop-n:case2</ta>
            <ta e="T34" id="Seg_442" s="T33">adv-adv:case</ta>
            <ta e="T35" id="Seg_443" s="T34">v-v:pn</ta>
            <ta e="T43" id="Seg_444" s="T35">%%</ta>
            <ta e="T44" id="Seg_445" s="T43">n-n&gt;adj</ta>
            <ta e="T45" id="Seg_446" s="T44">n-n:case1-n:poss</ta>
            <ta e="T46" id="Seg_447" s="T45">nprop-n:case1</ta>
            <ta e="T47" id="Seg_448" s="T46">num</ta>
            <ta e="T48" id="Seg_449" s="T47">n-n:ins-n&gt;adj</ta>
            <ta e="T49" id="Seg_450" s="T48">n-n:case1</ta>
            <ta e="T50" id="Seg_451" s="T49">nprop-n:case1</ta>
            <ta e="T51" id="Seg_452" s="T50">n-n:case1</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_453" s="T0">n</ta>
            <ta e="T2" id="Seg_454" s="T1">adj</ta>
            <ta e="T3" id="Seg_455" s="T2">n</ta>
            <ta e="T4" id="Seg_456" s="T3">n</ta>
            <ta e="T5" id="Seg_457" s="T4">n</ta>
            <ta e="T6" id="Seg_458" s="T5">adj</ta>
            <ta e="T7" id="Seg_459" s="T6">n</ta>
            <ta e="T8" id="Seg_460" s="T7">n</ta>
            <ta e="T9" id="Seg_461" s="T8">n</ta>
            <ta e="T10" id="Seg_462" s="T9">adj</ta>
            <ta e="T11" id="Seg_463" s="T10">adj</ta>
            <ta e="T42" id="Seg_464" s="T11">n</ta>
            <ta e="T53" id="Seg_465" s="T42">adj</ta>
            <ta e="T12" id="Seg_466" s="T53">n</ta>
            <ta e="T28" id="Seg_467" s="T27">nprop</ta>
            <ta e="T29" id="Seg_468" s="T28">n</ta>
            <ta e="T30" id="Seg_469" s="T29">v</ta>
            <ta e="T31" id="Seg_470" s="T30">n</ta>
            <ta e="T32" id="Seg_471" s="T31">n</ta>
            <ta e="T33" id="Seg_472" s="T32">nprop</ta>
            <ta e="T34" id="Seg_473" s="T33">adv</ta>
            <ta e="T35" id="Seg_474" s="T34">v</ta>
            <ta e="T43" id="Seg_475" s="T35">%%</ta>
            <ta e="T44" id="Seg_476" s="T43">adj</ta>
            <ta e="T45" id="Seg_477" s="T44">n</ta>
            <ta e="T46" id="Seg_478" s="T45">nprop</ta>
            <ta e="T47" id="Seg_479" s="T46">num</ta>
            <ta e="T48" id="Seg_480" s="T47">adj</ta>
            <ta e="T49" id="Seg_481" s="T48">n</ta>
            <ta e="T50" id="Seg_482" s="T49">nprop</ta>
            <ta e="T51" id="Seg_483" s="T50">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T28" id="Seg_484" s="T27">np.h:A</ta>
            <ta e="T29" id="Seg_485" s="T28">np:P</ta>
            <ta e="T31" id="Seg_486" s="T30">np.h:Poss</ta>
            <ta e="T33" id="Seg_487" s="T32">np:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T28" id="Seg_488" s="T27">np.h:S</ta>
            <ta e="T29" id="Seg_489" s="T28">np:O</ta>
            <ta e="T30" id="Seg_490" s="T29">v:pred</ta>
            <ta e="T35" id="Seg_491" s="T34">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T20" id="Seg_492" s="T12">RUS:ext</ta>
            <ta e="T27" id="Seg_493" s="T20">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T1" id="Seg_494" s="T0">Звёзды. </ta>
            <ta e="T5" id="Seg_495" s="T1">Утреннее на заре [бывает] созвездие. </ta>
            <ta e="T9" id="Seg_496" s="T5">Вечернее на заре [бывает] созвездие. </ta>
            <ta e="T42" id="Seg_497" s="T9">Медвежий звездный амбар (Малая медведица?).</ta>
            <ta e="T12" id="Seg_498" s="T42">Звездный амбар (Большая медведица?).</ta>
            <ta e="T20" id="Seg_499" s="T12">Раньше кто-то гонял лося, он туда и улетел. </ta>
            <ta e="T27" id="Seg_500" s="T20">У последней звезды как будто бы котомочка. </ta>
            <ta e="T30" id="Seg_501" s="T27">Итя сохатого гонит.</ta>
            <ta e="T32" id="Seg_502" s="T30">Итин свекор.</ta>
            <ta e="T43" id="Seg_503" s="T32">На реке Тым живущий зять.</ta>
            <ta e="T45" id="Seg_504" s="T43">Заячий амбар.</ta>
            <ta e="T49" id="Seg_505" s="T45">Итина сеть с тремя поплавками.</ta>
            <ta e="T51" id="Seg_506" s="T49">Итина стрела.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1" id="Seg_507" s="T0">Stars.</ta>
            <ta e="T5" id="Seg_508" s="T1">[There is] a morning constellation at dawn.</ta>
            <ta e="T9" id="Seg_509" s="T5">[There is] an evening constellation at dawn.</ta>
            <ta e="T42" id="Seg_510" s="T9">Bear star storehouse (the Great Bear?).</ta>
            <ta e="T12" id="Seg_511" s="T42">Star storehouse (the Little Bear?).</ta>
            <ta e="T20" id="Seg_512" s="T12">Earlier someone had hunted an elk, and he [the elk] flew there [to the sky].</ta>
            <ta e="T27" id="Seg_513" s="T20">The last star is like havind a sack.</ta>
            <ta e="T30" id="Seg_514" s="T27">Itja hunts an elk.</ta>
            <ta e="T32" id="Seg_515" s="T30">Itja's father-in-law.</ta>
            <ta e="T43" id="Seg_516" s="T32">The son-in-law, who lives at the river Tym.</ta>
            <ta e="T45" id="Seg_517" s="T43">Hare storehouse.</ta>
            <ta e="T49" id="Seg_518" s="T45">Itja's fishing net with three floats.</ta>
            <ta e="T51" id="Seg_519" s="T49">Itja's arrow.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1" id="Seg_520" s="T0">Sterne.</ta>
            <ta e="T5" id="Seg_521" s="T1">In der Morgendämmerung [gibt es] ein Sternbild. </ta>
            <ta e="T9" id="Seg_522" s="T5">In der Abenddämmerung [gibt es] ein Sternbild. </ta>
            <ta e="T42" id="Seg_523" s="T9">Bär-Sternenhaus (Großer Wagen?). </ta>
            <ta e="T12" id="Seg_524" s="T42">Sternenhaus (Kleiner Wagen?). </ta>
            <ta e="T20" id="Seg_525" s="T12">Früher trieb jemand den Elch, er ist dorthin [in den Himmel] geflogen. </ta>
            <ta e="T27" id="Seg_526" s="T20">Der letzte Stern hat etwas wie einen Quersack. </ta>
            <ta e="T30" id="Seg_527" s="T27">Itja treibt den Elch. </ta>
            <ta e="T32" id="Seg_528" s="T30">Itjas Schwiegervater.</ta>
            <ta e="T43" id="Seg_529" s="T32">Der am Tym lebende Schwiegersohn. </ta>
            <ta e="T45" id="Seg_530" s="T43">Hasen-Lagerhaus. </ta>
            <ta e="T49" id="Seg_531" s="T45">Itjas Netz hat drei Schwimmer. </ta>
            <ta e="T51" id="Seg_532" s="T49">Itjas Pfeil. </ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T1" id="Seg_533" s="T0">звезды</ta>
            <ta e="T5" id="Seg_534" s="T1">утренная на заре бывает звезда</ta>
            <ta e="T9" id="Seg_535" s="T5">вечерняя на верху зари звезда</ta>
            <ta e="T42" id="Seg_536" s="T9">малая медведица</ta>
            <ta e="T12" id="Seg_537" s="T42">большая медведица</ta>
            <ta e="T20" id="Seg_538" s="T12">Раньше кто-то гонял сохатого, он туда и улетел. </ta>
            <ta e="T27" id="Seg_539" s="T20">У последней звезды как будто бы катомочка. </ta>
            <ta e="T30" id="Seg_540" s="T27">Итя сохатого гонит</ta>
            <ta e="T32" id="Seg_541" s="T30">Итин свекор - в серёдке звёздушка. </ta>
            <ta e="T43" id="Seg_542" s="T32">С реки Казы живущий зять.</ta>
            <ta e="T45" id="Seg_543" s="T43">На небе смотришь и кружок такой: часто-часто звёздочки бывают – это называется какой-то «заячий пäрат» (не переводится). </ta>
            <ta e="T49" id="Seg_544" s="T45">Итина с тремя поплавками (наплавками) сеть</ta>
            <ta e="T51" id="Seg_545" s="T49">Итена стрела</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T30" id="Seg_546" s="T27">[BrM] The first of the three stars of the Great Bear is Itja. The group of four stars (and several small stars nearby) is the elk himself.</ta>
            <ta e="T32" id="Seg_547" s="T30">[BrM] The second of the three stars of the Great Bear.</ta>
            <ta e="T43" id="Seg_548" s="T32">[BrM] The third of the three stars of the Great Bear.</ta>
            <ta e="T45" id="Seg_549" s="T43">[BrM] Another constellation in form of a circle.</ta>
            <ta e="T51" id="Seg_550" s="T49">[There are seven of them in the sky. Itja shot and missed, and the arrows flew to the sky.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T30" id="Seg_551" s="T27">4 звезды Медведицы и еще впереди маленькие - это сам сохатый</ta>
            <ta e="T32" id="Seg_552" s="T30">в серёдке звёздушка. </ta>
            <ta e="T43" id="Seg_553" s="T32">Три звезды – люди, которые гонят сохатого: «Ите», «Дедушка (отец жены)» и «С реки Казы [Тым] живущий зять». </ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T42" />
            <conversion-tli id="T53" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
