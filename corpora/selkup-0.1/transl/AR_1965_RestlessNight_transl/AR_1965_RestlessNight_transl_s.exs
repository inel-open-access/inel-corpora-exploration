<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID57639A77-23B4-312C-DE02-C2E3D353B5B7">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">transl\AR_1965_RestlessNight_transl\AR_1965_RestlessNight_transl.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">276</ud-information>
            <ud-information attribute-name="# HIAT:w">219</ud-information>
            <ud-information attribute-name="# e">220</ud-information>
            <ud-information attribute-name="# HIAT:u">39</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AR">
            <abbreviation>AR</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T220" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AR"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T219" id="Seg_0" n="sc" s="T0">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Lɨpɨqɨn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ɛːsa</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_11" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">Marat</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">meːltə</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">patqɨlsä</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">qontɨtɨlʼ</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">kotaqɨntɨ</ts>
                  <nts id="Seg_26" n="HIAT:ip">,</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">pɔːpɨ</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_31" n="HIAT:ip">(</nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">kutɨpqos</ts>
                  <nts id="Seg_34" n="HIAT:ip">)</nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">üntɨšit</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">qaj</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">kos</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">tukɨmɨn</ts>
                  <nts id="Seg_46" n="HIAT:ip">.</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_49" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">Täp</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">tɛnaptɨsɨtä</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">kutar</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">Аndre</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">mulaltɨmpɨsɨt</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">qumɨj</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T19">qäːlɨj</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">čʼɔːtɨ</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_76" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">Marat</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">kuralʼna</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">qäːt</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">pɔːrantə</ts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_91" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">Täp</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">kuralsɨ</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_99" n="HIAT:w" s="T27">nɨntɨk</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_102" n="HIAT:w" s="T28">lʼɨpɨqɨj</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_105" n="HIAT:w" s="T29">qanɨp</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_108" n="HIAT:w" s="T30">i</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_111" n="HIAT:w" s="T31">sɨːqɨlsɨ</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_114" n="HIAT:w" s="T32">ınna</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_117" n="HIAT:w" s="T33">mättantɨ</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_121" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_123" n="HIAT:w" s="T34">Aj</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_126" n="HIAT:w" s="T35">täp</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_129" n="HIAT:w" s="T36">üntɨšit</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_132" n="HIAT:w" s="T37">kutɨ</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_135" n="HIAT:w" s="T38">koš</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_138" n="HIAT:w" s="T39">tukɨmpa</ts>
                  <nts id="Seg_139" n="HIAT:ip">.</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_142" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_144" n="HIAT:w" s="T40">Ni</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_147" n="HIAT:w" s="T41">qajim</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_150" n="HIAT:w" s="T42">aša</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_153" n="HIAT:w" s="T43">tɛnɨmɨla</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_156" n="HIAT:w" s="T44">Marat</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_159" n="HIAT:w" s="T45">nɨlaıːs</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_163" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_165" n="HIAT:w" s="T46">Pɔːpɨ</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_168" n="HIAT:w" s="T47">atalis</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_171" n="HIAT:w" s="T48">iːralʼıːp</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_175" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_177" n="HIAT:w" s="T49">Utaqɨntɨ</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_180" n="HIAT:w" s="T50">täpɨn</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_183" n="HIAT:w" s="T51">mɨqɨn</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_186" n="HIAT:w" s="T52">ɛːsa</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_189" n="HIAT:w" s="T53">po</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_193" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_195" n="HIAT:w" s="T54">Marat</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_198" n="HIAT:w" s="T55">torawatɨs</ts>
                  <nts id="Seg_199" n="HIAT:ip">,</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_202" n="HIAT:w" s="T56">täp</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_205" n="HIAT:w" s="T57">čʼaptäːn</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_208" n="HIAT:w" s="T58">tɛnɨmɨsɨt</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_211" n="HIAT:w" s="T59">iralʼıːpɨm</ts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_215" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_217" n="HIAT:w" s="T60">Ivan</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_220" n="HIAT:w" s="T61">Pavlovičʼ</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_223" n="HIAT:w" s="T62">Lunʼ</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_226" n="HIAT:w" s="T63">dižurnatɨsɨ</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_229" n="HIAT:w" s="T64">pin</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_232" n="HIAT:w" s="T65">poːqän</ts>
                  <nts id="Seg_233" n="HIAT:ip">,</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_236" n="HIAT:w" s="T66">qäːt</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_239" n="HIAT:w" s="T67">pɔːraqɨn</ts>
                  <nts id="Seg_240" n="HIAT:ip">.</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_243" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_245" n="HIAT:w" s="T68">Täp</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_248" n="HIAT:w" s="T69">ila</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_251" n="HIAT:w" s="T70">tɨmtɨ</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_254" n="HIAT:w" s="T71">meːltɨ</ts>
                  <nts id="Seg_255" n="HIAT:ip">,</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_258" n="HIAT:w" s="T72">tɨmtɨ</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_261" n="HIAT:w" s="T73">täm</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_264" n="HIAT:w" s="T74">ɛj</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_267" n="HIAT:w" s="T75">čʼeːlɨksä</ts>
                  <nts id="Seg_268" n="HIAT:ip">.</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_271" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_273" n="HIAT:w" s="T76">Täm</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_276" n="HIAT:w" s="T77">ɛːsa</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_279" n="HIAT:w" s="T78">somak</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_282" n="HIAT:w" s="T79">qäːlɨčʼčʼilʼ</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_285" n="HIAT:w" s="T80">qum</ts>
                  <nts id="Seg_286" n="HIAT:ip">.</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_289" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_291" n="HIAT:w" s="T81">Täp</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_294" n="HIAT:w" s="T82">tɛnɨmɨsɨt</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_297" n="HIAT:w" s="T83">orsa</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_300" n="HIAT:w" s="T84">kočʼi</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_303" n="HIAT:w" s="T85">čʼattäm</ts>
                  <nts id="Seg_304" n="HIAT:ip">.</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_307" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_309" n="HIAT:w" s="T86">Ütnɨlʼ</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_312" n="HIAT:w" s="T87">čʼeːlɨt</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_315" n="HIAT:w" s="T88">täp</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_318" n="HIAT:w" s="T89">kɨkɨsɨt</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_321" n="HIAT:w" s="T90">ijatɨsa</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_324" n="HIAT:w" s="T91">ɔːmtɨqa</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_327" n="HIAT:w" s="T92">kun</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_330" n="HIAT:w" s="T93">ɛːma</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_333" n="HIAT:w" s="T94">ütɨ</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_336" n="HIAT:w" s="T95">qanɨqqɨn</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_339" n="HIAT:w" s="T96">tüt</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_342" n="HIAT:w" s="T97">qanɨqqɨn</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_346" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_348" n="HIAT:w" s="T98">Marat</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_351" n="HIAT:w" s="T99">qəssɨ</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_354" n="HIAT:w" s="T100">Lunsä</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_357" n="HIAT:w" s="T101">aj</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_360" n="HIAT:w" s="T102">soqɨssɨt</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_363" n="HIAT:w" s="T103">täpɨm</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_366" n="HIAT:w" s="T104">qänqa</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_369" n="HIAT:w" s="T105">qäːlɨjja</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_372" n="HIAT:w" s="T106">ijatɨsa</ts>
                  <nts id="Seg_373" n="HIAT:ip">.</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_376" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_378" n="HIAT:w" s="T107">Ukoːt</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_381" n="HIAT:w" s="T108">kušaqɨn</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_383" n="HIAT:ip">(</nts>
                  <nts id="Seg_384" n="HIAT:ip">/</nts>
                  <ts e="T110" id="Seg_386" n="HIAT:w" s="T109">kuša</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_389" n="HIAT:w" s="T110">kos</ts>
                  <nts id="Seg_390" n="HIAT:ip">)</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_393" n="HIAT:w" s="T111">ɛːsa</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_396" n="HIAT:w" s="T112">čʼeːlam</ts>
                  <nts id="Seg_397" n="HIAT:ip">,</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_400" n="HIAT:w" s="T113">iːnatɨsa</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_402" n="HIAT:ip">(</nts>
                  <ts e="T115" id="Seg_404" n="HIAT:w" s="T114">qaltɨrɨkkak</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_407" n="HIAT:w" s="T115">qarɨn</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_410" n="HIAT:w" s="T116">morʼat</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_413" n="HIAT:w" s="T117">qanɨqqɨn</ts>
                  <nts id="Seg_414" n="HIAT:ip">)</nts>
                  <nts id="Seg_415" n="HIAT:ip">.</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_418" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_420" n="HIAT:w" s="T118">Iːnatɨsa</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_423" n="HIAT:w" s="T119">korasaŋ</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_426" n="HIAT:w" s="T120">morʼaqɨn</ts>
                  <nts id="Seg_427" n="HIAT:ip">.</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_430" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_432" n="HIAT:w" s="T121">Tiː</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_435" n="HIAT:w" s="T122">tɛː</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_438" n="HIAT:w" s="T123">mɨqɨntɨn</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_441" n="HIAT:w" s="T124">mänɨlʼ</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_444" n="HIAT:w" s="T125">üral</ts>
                  <nts id="Seg_445" n="HIAT:ip">.</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_448" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_450" n="HIAT:w" s="T126">Tan</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_453" n="HIAT:w" s="T127">mɛntalʼ</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_456" n="HIAT:w" s="T128">ürɨtqa</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_459" n="HIAT:w" s="T129">soqɨnčʼas</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_462" n="HIAT:w" s="T130">Fillip</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_465" n="HIAT:w" s="T131">mɨqɨn</ts>
                  <nts id="Seg_466" n="HIAT:ip">,</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_469" n="HIAT:w" s="T132">täm</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_472" n="HIAT:w" s="T133">täntɨ</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_475" n="HIAT:w" s="T134">muntɨk</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_478" n="HIAT:w" s="T135">mulaltäntɨŋɨtɨ</ts>
                  <nts id="Seg_479" n="HIAT:ip">.</nts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_482" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_484" n="HIAT:w" s="T136">Qajij</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_487" n="HIAT:w" s="T137">Filʼip</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_490" n="HIAT:w" s="T138">mɨqɨn</ts>
                  <nts id="Seg_491" n="HIAT:ip">?</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_494" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_496" n="HIAT:w" s="T139">Ɛːan</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_499" n="HIAT:w" s="T140">nılʼčʼij</ts>
                  <nts id="Seg_500" n="HIAT:ip">.</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_503" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_505" n="HIAT:w" s="T141">Täm</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_508" n="HIAT:w" s="T142">man</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_511" n="HIAT:w" s="T143">mɨqak</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_514" n="HIAT:w" s="T144">tiː</ts>
                  <nts id="Seg_515" n="HIAT:ip">.</nts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_518" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_520" n="HIAT:w" s="T145">Täm</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_523" n="HIAT:w" s="T146">inžiner</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_526" n="HIAT:w" s="T147">Leningradnɨ</ts>
                  <nts id="Seg_527" n="HIAT:ip">.</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_530" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_532" n="HIAT:w" s="T148">Tan</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_535" n="HIAT:w" s="T149">ukkɨrna</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_538" n="HIAT:w" s="T150">üntɨıːsal</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_541" n="HIAT:w" s="T151">täpɨn</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_544" n="HIAT:w" s="T152">familʼijam</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_547" n="HIAT:w" s="T153">Klʼučʼin</ts>
                  <nts id="Seg_548" n="HIAT:ip">.</nts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_551" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_553" n="HIAT:w" s="T154">Man</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_556" n="HIAT:w" s="T155">üntɨıːsam</ts>
                  <nts id="Seg_557" n="HIAT:ip">.</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_560" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_562" n="HIAT:w" s="T156">Filipp</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_565" n="HIAT:w" s="T157">orsa</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_568" n="HIAT:w" s="T158">soma</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_571" n="HIAT:w" s="T159">qum</ts>
                  <nts id="Seg_572" n="HIAT:ip">.</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_575" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_577" n="HIAT:w" s="T160">Täp</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_580" n="HIAT:w" s="T161">tɨmtɨ</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_583" n="HIAT:w" s="T162">tɛː</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_586" n="HIAT:w" s="T163">karabəlʼip</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_589" n="HIAT:w" s="T164">mantalʼtukkɨŋɨtɨ</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_591" n="HIAT:ip">(</nts>
                  <nts id="Seg_592" n="HIAT:ip">/</nts>
                  <ts e="T166" id="Seg_594" n="HIAT:w" s="T165">mantaltukkɨsa</ts>
                  <nts id="Seg_595" n="HIAT:ip">)</nts>
                  <nts id="Seg_596" n="HIAT:ip">.</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_599" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_601" n="HIAT:w" s="T166">Kätɨsät</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_604" n="HIAT:w" s="T167">soma</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_607" n="HIAT:w" s="T168">ɛːja</ts>
                  <nts id="Seg_608" n="HIAT:ip">.</nts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_611" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_613" n="HIAT:w" s="T169">Pɛltɛntam</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_616" n="HIAT:w" s="T170">man</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_619" n="HIAT:w" s="T171">iːjatɨm</ts>
                  <nts id="Seg_620" n="HIAT:ip">,</nts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_623" n="HIAT:w" s="T172">kätɨsɨtɨ</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_626" n="HIAT:w" s="T173">täm</ts>
                  <nts id="Seg_627" n="HIAT:ip">.</nts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T185" id="Seg_630" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_632" n="HIAT:w" s="T174">Marat</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_635" n="HIAT:w" s="T175">kätəsɨt</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_638" n="HIAT:w" s="T176">iːralʼıːpənɨŋ</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_641" n="HIAT:w" s="T177">spasipo</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_644" n="HIAT:w" s="T178">mɛntalʼ</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_647" n="HIAT:w" s="T179">qäːlɨčʼčʼilʼ</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_650" n="HIAT:w" s="T180">qumɨnɨŋ</ts>
                  <nts id="Seg_651" n="HIAT:ip">,</nts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_654" n="HIAT:w" s="T181">täpaqäj</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_657" n="HIAT:w" s="T182">kuralsɔːqıj</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_660" n="HIAT:w" s="T183">mänɨlʼ</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_663" n="HIAT:w" s="T184">pilaqtɨ</ts>
                  <nts id="Seg_664" n="HIAT:ip">.</nts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_667" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_669" n="HIAT:w" s="T185">Marat</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_672" n="HIAT:w" s="T186">qolʼčʼisɨt</ts>
                  <nts id="Seg_673" n="HIAT:ip">,</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_676" n="HIAT:w" s="T187">majak</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_679" n="HIAT:w" s="T188">asa</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_682" n="HIAT:w" s="T189">čʼɔːpɨmpa</ts>
                  <nts id="Seg_683" n="HIAT:ip">.</nts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_686" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_688" n="HIAT:w" s="T190">Täp</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_691" n="HIAT:w" s="T191">qolčʼisɨt</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_694" n="HIAT:w" s="T192">qaji</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_697" n="HIAT:w" s="T193">qos</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_700" n="HIAT:w" s="T194">qoškeja</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_703" n="HIAT:w" s="T195">aj</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_706" n="HIAT:w" s="T196">čʼaktɨsa</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_709" n="HIAT:w" s="T197">kuralsä</ts>
                  <nts id="Seg_710" n="HIAT:ip">.</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_713" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_715" n="HIAT:w" s="T198">Täp</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_718" n="HIAT:w" s="T199">čʼeːtɨsɨt</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_721" n="HIAT:w" s="T200">Аlʼenkam</ts>
                  <nts id="Seg_722" n="HIAT:ip">.</nts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_725" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_727" n="HIAT:w" s="T201">Paktɨlɨmɨj</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_730" n="HIAT:w" s="T202">točʼa</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_733" n="HIAT:w" s="T203">čʼaktɨsa</ts>
                  <nts id="Seg_734" n="HIAT:ip">.</nts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_737" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_739" n="HIAT:w" s="T204">Täpaqıj</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_742" n="HIAT:w" s="T205">qalʼlʼɛːjisɔːqıj</ts>
                  <nts id="Seg_743" n="HIAT:ip">.</nts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_746" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_748" n="HIAT:w" s="T206">Kutə</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_751" n="HIAT:w" s="T207">qos</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_754" n="HIAT:w" s="T208">paktat</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_757" n="HIAT:w" s="T209">üttə</ts>
                  <nts id="Seg_758" n="HIAT:ip">.</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_761" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_763" n="HIAT:w" s="T210">Me</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_766" n="HIAT:w" s="T211">ɛnalisɔːmɨn</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_769" n="HIAT:w" s="T212">paktɨqa</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_772" n="HIAT:w" s="T213">üttɨ</ts>
                  <nts id="Seg_773" n="HIAT:ip">.</nts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_776" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_778" n="HIAT:w" s="T214">Nɨmtɨ</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_781" n="HIAT:w" s="T215">ɛːsä</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_784" n="HIAT:w" s="T216">kočʼi</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_787" n="HIAT:w" s="T217">ütqɨlʼ</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_790" n="HIAT:w" s="T218">poː</ts>
                  <nts id="Seg_791" n="HIAT:ip">.</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T219" id="Seg_793" n="sc" s="T0">
               <ts e="T1" id="Seg_795" n="e" s="T0">Lɨpɨqɨn </ts>
               <ts e="T2" id="Seg_797" n="e" s="T1">ɛːsa. </ts>
               <ts e="T3" id="Seg_799" n="e" s="T2">Marat </ts>
               <ts e="T4" id="Seg_801" n="e" s="T3">meːltə </ts>
               <ts e="T5" id="Seg_803" n="e" s="T4">patqɨlsä </ts>
               <ts e="T6" id="Seg_805" n="e" s="T5">qontɨtɨlʼ </ts>
               <ts e="T7" id="Seg_807" n="e" s="T6">kotaqɨntɨ, </ts>
               <ts e="T8" id="Seg_809" n="e" s="T7">pɔːpɨ </ts>
               <ts e="T220" id="Seg_811" n="e" s="T8">(kutɨp</ts>
               <ts e="T9" id="Seg_813" n="e" s="T220">qos)</ts>
               <ts e="T10" id="Seg_815" n="e" s="T9">üntɨšit </ts>
               <ts e="T11" id="Seg_817" n="e" s="T10">qaj </ts>
               <ts e="T12" id="Seg_819" n="e" s="T11">kos </ts>
               <ts e="T13" id="Seg_821" n="e" s="T12">tukɨmɨn. </ts>
               <ts e="T14" id="Seg_823" n="e" s="T13">Täp </ts>
               <ts e="T15" id="Seg_825" n="e" s="T14">tɛnaptɨsɨtä </ts>
               <ts e="T16" id="Seg_827" n="e" s="T15">kutar </ts>
               <ts e="T17" id="Seg_829" n="e" s="T16">Аndre </ts>
               <ts e="T18" id="Seg_831" n="e" s="T17">mulaltɨmpɨsɨt </ts>
               <ts e="T19" id="Seg_833" n="e" s="T18">qumɨj </ts>
               <ts e="T20" id="Seg_835" n="e" s="T19">qäːlɨj </ts>
               <ts e="T21" id="Seg_837" n="e" s="T20">čʼɔːtɨ. </ts>
               <ts e="T22" id="Seg_839" n="e" s="T21">Marat </ts>
               <ts e="T23" id="Seg_841" n="e" s="T22">kuralʼna </ts>
               <ts e="T24" id="Seg_843" n="e" s="T23">qäːt </ts>
               <ts e="T25" id="Seg_845" n="e" s="T24">pɔːrantə. </ts>
               <ts e="T26" id="Seg_847" n="e" s="T25">Täp </ts>
               <ts e="T27" id="Seg_849" n="e" s="T26">kuralsɨ </ts>
               <ts e="T28" id="Seg_851" n="e" s="T27">nɨntɨk </ts>
               <ts e="T29" id="Seg_853" n="e" s="T28">lʼɨpɨqɨj </ts>
               <ts e="T30" id="Seg_855" n="e" s="T29">qanɨp </ts>
               <ts e="T31" id="Seg_857" n="e" s="T30">i </ts>
               <ts e="T32" id="Seg_859" n="e" s="T31">sɨːqɨlsɨ </ts>
               <ts e="T33" id="Seg_861" n="e" s="T32">ınna </ts>
               <ts e="T34" id="Seg_863" n="e" s="T33">mättantɨ. </ts>
               <ts e="T35" id="Seg_865" n="e" s="T34">Aj </ts>
               <ts e="T36" id="Seg_867" n="e" s="T35">täp </ts>
               <ts e="T37" id="Seg_869" n="e" s="T36">üntɨšit </ts>
               <ts e="T38" id="Seg_871" n="e" s="T37">kutɨ </ts>
               <ts e="T39" id="Seg_873" n="e" s="T38">koš </ts>
               <ts e="T40" id="Seg_875" n="e" s="T39">tukɨmpa. </ts>
               <ts e="T41" id="Seg_877" n="e" s="T40">Ni </ts>
               <ts e="T42" id="Seg_879" n="e" s="T41">qajim </ts>
               <ts e="T43" id="Seg_881" n="e" s="T42">aša </ts>
               <ts e="T44" id="Seg_883" n="e" s="T43">tɛnɨmɨla </ts>
               <ts e="T45" id="Seg_885" n="e" s="T44">Marat </ts>
               <ts e="T46" id="Seg_887" n="e" s="T45">nɨlaıːs. </ts>
               <ts e="T47" id="Seg_889" n="e" s="T46">Pɔːpɨ </ts>
               <ts e="T48" id="Seg_891" n="e" s="T47">atalis </ts>
               <ts e="T49" id="Seg_893" n="e" s="T48">iːralʼıːp. </ts>
               <ts e="T50" id="Seg_895" n="e" s="T49">Utaqɨntɨ </ts>
               <ts e="T51" id="Seg_897" n="e" s="T50">täpɨn </ts>
               <ts e="T52" id="Seg_899" n="e" s="T51">mɨqɨn </ts>
               <ts e="T53" id="Seg_901" n="e" s="T52">ɛːsa </ts>
               <ts e="T54" id="Seg_903" n="e" s="T53">po. </ts>
               <ts e="T55" id="Seg_905" n="e" s="T54">Marat </ts>
               <ts e="T56" id="Seg_907" n="e" s="T55">torawatɨs, </ts>
               <ts e="T57" id="Seg_909" n="e" s="T56">täp </ts>
               <ts e="T58" id="Seg_911" n="e" s="T57">čʼaptäːn </ts>
               <ts e="T59" id="Seg_913" n="e" s="T58">tɛnɨmɨsɨt </ts>
               <ts e="T60" id="Seg_915" n="e" s="T59">iralʼıːpɨm. </ts>
               <ts e="T61" id="Seg_917" n="e" s="T60">Ivan </ts>
               <ts e="T62" id="Seg_919" n="e" s="T61">Pavlovičʼ </ts>
               <ts e="T63" id="Seg_921" n="e" s="T62">Lunʼ </ts>
               <ts e="T64" id="Seg_923" n="e" s="T63">dižurnatɨsɨ </ts>
               <ts e="T65" id="Seg_925" n="e" s="T64">pin </ts>
               <ts e="T66" id="Seg_927" n="e" s="T65">poːqän, </ts>
               <ts e="T67" id="Seg_929" n="e" s="T66">qäːt </ts>
               <ts e="T68" id="Seg_931" n="e" s="T67">pɔːraqɨn. </ts>
               <ts e="T69" id="Seg_933" n="e" s="T68">Täp </ts>
               <ts e="T70" id="Seg_935" n="e" s="T69">ila </ts>
               <ts e="T71" id="Seg_937" n="e" s="T70">tɨmtɨ </ts>
               <ts e="T72" id="Seg_939" n="e" s="T71">meːltɨ, </ts>
               <ts e="T73" id="Seg_941" n="e" s="T72">tɨmtɨ </ts>
               <ts e="T74" id="Seg_943" n="e" s="T73">täm </ts>
               <ts e="T75" id="Seg_945" n="e" s="T74">ɛj </ts>
               <ts e="T76" id="Seg_947" n="e" s="T75">čʼeːlɨksä. </ts>
               <ts e="T77" id="Seg_949" n="e" s="T76">Täm </ts>
               <ts e="T78" id="Seg_951" n="e" s="T77">ɛːsa </ts>
               <ts e="T79" id="Seg_953" n="e" s="T78">somak </ts>
               <ts e="T80" id="Seg_955" n="e" s="T79">qäːlɨčʼčʼilʼ </ts>
               <ts e="T81" id="Seg_957" n="e" s="T80">qum. </ts>
               <ts e="T82" id="Seg_959" n="e" s="T81">Täp </ts>
               <ts e="T83" id="Seg_961" n="e" s="T82">tɛnɨmɨsɨt </ts>
               <ts e="T84" id="Seg_963" n="e" s="T83">orsa </ts>
               <ts e="T85" id="Seg_965" n="e" s="T84">kočʼi </ts>
               <ts e="T86" id="Seg_967" n="e" s="T85">čʼattäm. </ts>
               <ts e="T87" id="Seg_969" n="e" s="T86">Ütnɨlʼ </ts>
               <ts e="T88" id="Seg_971" n="e" s="T87">čʼeːlɨt </ts>
               <ts e="T89" id="Seg_973" n="e" s="T88">täp </ts>
               <ts e="T90" id="Seg_975" n="e" s="T89">kɨkɨsɨt </ts>
               <ts e="T91" id="Seg_977" n="e" s="T90">ijatɨsa </ts>
               <ts e="T92" id="Seg_979" n="e" s="T91">ɔːmtɨqa </ts>
               <ts e="T93" id="Seg_981" n="e" s="T92">kun </ts>
               <ts e="T94" id="Seg_983" n="e" s="T93">ɛːma </ts>
               <ts e="T95" id="Seg_985" n="e" s="T94">ütɨ </ts>
               <ts e="T96" id="Seg_987" n="e" s="T95">qanɨqqɨn </ts>
               <ts e="T97" id="Seg_989" n="e" s="T96">tüt </ts>
               <ts e="T98" id="Seg_991" n="e" s="T97">qanɨqqɨn. </ts>
               <ts e="T99" id="Seg_993" n="e" s="T98">Marat </ts>
               <ts e="T100" id="Seg_995" n="e" s="T99">qəssɨ </ts>
               <ts e="T101" id="Seg_997" n="e" s="T100">Lunsä </ts>
               <ts e="T102" id="Seg_999" n="e" s="T101">aj </ts>
               <ts e="T103" id="Seg_1001" n="e" s="T102">soqɨssɨt </ts>
               <ts e="T104" id="Seg_1003" n="e" s="T103">täpɨm </ts>
               <ts e="T105" id="Seg_1005" n="e" s="T104">qänqa </ts>
               <ts e="T106" id="Seg_1007" n="e" s="T105">qäːlɨjja </ts>
               <ts e="T107" id="Seg_1009" n="e" s="T106">ijatɨsa. </ts>
               <ts e="T108" id="Seg_1011" n="e" s="T107">Ukoːt </ts>
               <ts e="T109" id="Seg_1013" n="e" s="T108">kušaqɨn </ts>
               <ts e="T110" id="Seg_1015" n="e" s="T109">(/kuša </ts>
               <ts e="T111" id="Seg_1017" n="e" s="T110">kos) </ts>
               <ts e="T112" id="Seg_1019" n="e" s="T111">ɛːsa </ts>
               <ts e="T113" id="Seg_1021" n="e" s="T112">čʼeːlam, </ts>
               <ts e="T114" id="Seg_1023" n="e" s="T113">iːnatɨsa </ts>
               <ts e="T115" id="Seg_1025" n="e" s="T114">(qaltɨrɨkkak </ts>
               <ts e="T116" id="Seg_1027" n="e" s="T115">qarɨn </ts>
               <ts e="T117" id="Seg_1029" n="e" s="T116">morʼat </ts>
               <ts e="T118" id="Seg_1031" n="e" s="T117">qanɨqqɨn). </ts>
               <ts e="T119" id="Seg_1033" n="e" s="T118">Iːnatɨsa </ts>
               <ts e="T120" id="Seg_1035" n="e" s="T119">korasaŋ </ts>
               <ts e="T121" id="Seg_1037" n="e" s="T120">morʼaqɨn. </ts>
               <ts e="T122" id="Seg_1039" n="e" s="T121">Tiː </ts>
               <ts e="T123" id="Seg_1041" n="e" s="T122">tɛː </ts>
               <ts e="T124" id="Seg_1043" n="e" s="T123">mɨqɨntɨn </ts>
               <ts e="T125" id="Seg_1045" n="e" s="T124">mänɨlʼ </ts>
               <ts e="T126" id="Seg_1047" n="e" s="T125">üral. </ts>
               <ts e="T127" id="Seg_1049" n="e" s="T126">Tan </ts>
               <ts e="T128" id="Seg_1051" n="e" s="T127">mɛntalʼ </ts>
               <ts e="T129" id="Seg_1053" n="e" s="T128">ürɨtqa </ts>
               <ts e="T130" id="Seg_1055" n="e" s="T129">soqɨnčʼas </ts>
               <ts e="T131" id="Seg_1057" n="e" s="T130">Fillip </ts>
               <ts e="T132" id="Seg_1059" n="e" s="T131">mɨqɨn, </ts>
               <ts e="T133" id="Seg_1061" n="e" s="T132">täm </ts>
               <ts e="T134" id="Seg_1063" n="e" s="T133">täntɨ </ts>
               <ts e="T135" id="Seg_1065" n="e" s="T134">muntɨk </ts>
               <ts e="T136" id="Seg_1067" n="e" s="T135">mulaltäntɨŋɨtɨ. </ts>
               <ts e="T137" id="Seg_1069" n="e" s="T136">Qajij </ts>
               <ts e="T138" id="Seg_1071" n="e" s="T137">Filʼip </ts>
               <ts e="T139" id="Seg_1073" n="e" s="T138">mɨqɨn? </ts>
               <ts e="T140" id="Seg_1075" n="e" s="T139">Ɛːan </ts>
               <ts e="T141" id="Seg_1077" n="e" s="T140">nılʼčʼij. </ts>
               <ts e="T142" id="Seg_1079" n="e" s="T141">Täm </ts>
               <ts e="T143" id="Seg_1081" n="e" s="T142">man </ts>
               <ts e="T144" id="Seg_1083" n="e" s="T143">mɨqak </ts>
               <ts e="T145" id="Seg_1085" n="e" s="T144">tiː. </ts>
               <ts e="T146" id="Seg_1087" n="e" s="T145">Täm </ts>
               <ts e="T147" id="Seg_1089" n="e" s="T146">inžiner </ts>
               <ts e="T148" id="Seg_1091" n="e" s="T147">Leningradnɨ. </ts>
               <ts e="T149" id="Seg_1093" n="e" s="T148">Tan </ts>
               <ts e="T150" id="Seg_1095" n="e" s="T149">ukkɨrna </ts>
               <ts e="T151" id="Seg_1097" n="e" s="T150">üntɨıːsal </ts>
               <ts e="T152" id="Seg_1099" n="e" s="T151">täpɨn </ts>
               <ts e="T153" id="Seg_1101" n="e" s="T152">familʼijam </ts>
               <ts e="T154" id="Seg_1103" n="e" s="T153">Klʼučʼin. </ts>
               <ts e="T155" id="Seg_1105" n="e" s="T154">Man </ts>
               <ts e="T156" id="Seg_1107" n="e" s="T155">üntɨıːsam. </ts>
               <ts e="T157" id="Seg_1109" n="e" s="T156">Filipp </ts>
               <ts e="T158" id="Seg_1111" n="e" s="T157">orsa </ts>
               <ts e="T159" id="Seg_1113" n="e" s="T158">soma </ts>
               <ts e="T160" id="Seg_1115" n="e" s="T159">qum. </ts>
               <ts e="T161" id="Seg_1117" n="e" s="T160">Täp </ts>
               <ts e="T162" id="Seg_1119" n="e" s="T161">tɨmtɨ </ts>
               <ts e="T163" id="Seg_1121" n="e" s="T162">tɛː </ts>
               <ts e="T164" id="Seg_1123" n="e" s="T163">karabəlʼip </ts>
               <ts e="T165" id="Seg_1125" n="e" s="T164">mantalʼtukkɨŋɨtɨ </ts>
               <ts e="T166" id="Seg_1127" n="e" s="T165">(/mantaltukkɨsa). </ts>
               <ts e="T167" id="Seg_1129" n="e" s="T166">Kätɨsät </ts>
               <ts e="T168" id="Seg_1131" n="e" s="T167">soma </ts>
               <ts e="T169" id="Seg_1133" n="e" s="T168">ɛːja. </ts>
               <ts e="T170" id="Seg_1135" n="e" s="T169">Pɛltɛntam </ts>
               <ts e="T171" id="Seg_1137" n="e" s="T170">man </ts>
               <ts e="T172" id="Seg_1139" n="e" s="T171">iːjatɨm, </ts>
               <ts e="T173" id="Seg_1141" n="e" s="T172">kätɨsɨtɨ </ts>
               <ts e="T174" id="Seg_1143" n="e" s="T173">täm. </ts>
               <ts e="T175" id="Seg_1145" n="e" s="T174">Marat </ts>
               <ts e="T176" id="Seg_1147" n="e" s="T175">kätəsɨt </ts>
               <ts e="T177" id="Seg_1149" n="e" s="T176">iːralʼıːpənɨŋ </ts>
               <ts e="T178" id="Seg_1151" n="e" s="T177">spasipo </ts>
               <ts e="T179" id="Seg_1153" n="e" s="T178">mɛntalʼ </ts>
               <ts e="T180" id="Seg_1155" n="e" s="T179">qäːlɨčʼčʼilʼ </ts>
               <ts e="T181" id="Seg_1157" n="e" s="T180">qumɨnɨŋ, </ts>
               <ts e="T182" id="Seg_1159" n="e" s="T181">täpaqäj </ts>
               <ts e="T183" id="Seg_1161" n="e" s="T182">kuralsɔːqıj </ts>
               <ts e="T184" id="Seg_1163" n="e" s="T183">mänɨlʼ </ts>
               <ts e="T185" id="Seg_1165" n="e" s="T184">pilaqtɨ. </ts>
               <ts e="T186" id="Seg_1167" n="e" s="T185">Marat </ts>
               <ts e="T187" id="Seg_1169" n="e" s="T186">qolʼčʼisɨt, </ts>
               <ts e="T188" id="Seg_1171" n="e" s="T187">majak </ts>
               <ts e="T189" id="Seg_1173" n="e" s="T188">asa </ts>
               <ts e="T190" id="Seg_1175" n="e" s="T189">čʼɔːpɨmpa. </ts>
               <ts e="T191" id="Seg_1177" n="e" s="T190">Täp </ts>
               <ts e="T192" id="Seg_1179" n="e" s="T191">qolčʼisɨt </ts>
               <ts e="T193" id="Seg_1181" n="e" s="T192">qaji </ts>
               <ts e="T194" id="Seg_1183" n="e" s="T193">qos </ts>
               <ts e="T195" id="Seg_1185" n="e" s="T194">qoškeja </ts>
               <ts e="T196" id="Seg_1187" n="e" s="T195">aj </ts>
               <ts e="T197" id="Seg_1189" n="e" s="T196">čʼaktɨsa </ts>
               <ts e="T198" id="Seg_1191" n="e" s="T197">kuralsä. </ts>
               <ts e="T199" id="Seg_1193" n="e" s="T198">Täp </ts>
               <ts e="T200" id="Seg_1195" n="e" s="T199">čʼeːtɨsɨt </ts>
               <ts e="T201" id="Seg_1197" n="e" s="T200">Аlʼenkam. </ts>
               <ts e="T202" id="Seg_1199" n="e" s="T201">Paktɨlɨmɨj </ts>
               <ts e="T203" id="Seg_1201" n="e" s="T202">točʼa </ts>
               <ts e="T204" id="Seg_1203" n="e" s="T203">čʼaktɨsa. </ts>
               <ts e="T205" id="Seg_1205" n="e" s="T204">Täpaqıj </ts>
               <ts e="T206" id="Seg_1207" n="e" s="T205">qalʼlʼɛːjisɔːqıj. </ts>
               <ts e="T207" id="Seg_1209" n="e" s="T206">Kutə </ts>
               <ts e="T208" id="Seg_1211" n="e" s="T207">qos </ts>
               <ts e="T209" id="Seg_1213" n="e" s="T208">paktat </ts>
               <ts e="T210" id="Seg_1215" n="e" s="T209">üttə. </ts>
               <ts e="T211" id="Seg_1217" n="e" s="T210">Me </ts>
               <ts e="T212" id="Seg_1219" n="e" s="T211">ɛnalisɔːmɨn </ts>
               <ts e="T213" id="Seg_1221" n="e" s="T212">paktɨqa </ts>
               <ts e="T214" id="Seg_1223" n="e" s="T213">üttɨ. </ts>
               <ts e="T215" id="Seg_1225" n="e" s="T214">Nɨmtɨ </ts>
               <ts e="T216" id="Seg_1227" n="e" s="T215">ɛːsä </ts>
               <ts e="T217" id="Seg_1229" n="e" s="T216">kočʼi </ts>
               <ts e="T218" id="Seg_1231" n="e" s="T217">ütqɨlʼ </ts>
               <ts e="T219" id="Seg_1233" n="e" s="T218">poː. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2" id="Seg_1234" s="T0">AR_1965_RestlessNight_transl.001 (001.001)</ta>
            <ta e="T13" id="Seg_1235" s="T2">AR_1965_RestlessNight_transl.002 (001.002)</ta>
            <ta e="T21" id="Seg_1236" s="T13">AR_1965_RestlessNight_transl.003 (001.003)</ta>
            <ta e="T25" id="Seg_1237" s="T21">AR_1965_RestlessNight_transl.004 (001.004)</ta>
            <ta e="T34" id="Seg_1238" s="T25">AR_1965_RestlessNight_transl.005 (001.005)</ta>
            <ta e="T40" id="Seg_1239" s="T34">AR_1965_RestlessNight_transl.006 (001.006)</ta>
            <ta e="T46" id="Seg_1240" s="T40">AR_1965_RestlessNight_transl.007 (001.007)</ta>
            <ta e="T49" id="Seg_1241" s="T46">AR_1965_RestlessNight_transl.008 (001.008)</ta>
            <ta e="T54" id="Seg_1242" s="T49">AR_1965_RestlessNight_transl.009 (001.009)</ta>
            <ta e="T60" id="Seg_1243" s="T54">AR_1965_RestlessNight_transl.010 (001.010)</ta>
            <ta e="T68" id="Seg_1244" s="T60">AR_1965_RestlessNight_transl.011 (001.011)</ta>
            <ta e="T76" id="Seg_1245" s="T68">AR_1965_RestlessNight_transl.012 (001.012)</ta>
            <ta e="T81" id="Seg_1246" s="T76">AR_1965_RestlessNight_transl.013 (001.013)</ta>
            <ta e="T86" id="Seg_1247" s="T81">AR_1965_RestlessNight_transl.014 (001.014)</ta>
            <ta e="T98" id="Seg_1248" s="T86">AR_1965_RestlessNight_transl.015 (001.015)</ta>
            <ta e="T107" id="Seg_1249" s="T98">AR_1965_RestlessNight_transl.016 (001.016)</ta>
            <ta e="T118" id="Seg_1250" s="T107">AR_1965_RestlessNight_transl.017 (002.001)</ta>
            <ta e="T121" id="Seg_1251" s="T118">AR_1965_RestlessNight_transl.018 (002.002)</ta>
            <ta e="T126" id="Seg_1252" s="T121">AR_1965_RestlessNight_transl.019 (002.003)</ta>
            <ta e="T136" id="Seg_1253" s="T126">AR_1965_RestlessNight_transl.020 (002.004)</ta>
            <ta e="T139" id="Seg_1254" s="T136">AR_1965_RestlessNight_transl.021 (002.005)</ta>
            <ta e="T141" id="Seg_1255" s="T139">AR_1965_RestlessNight_transl.022 (002.006)</ta>
            <ta e="T145" id="Seg_1256" s="T141">AR_1965_RestlessNight_transl.023 (002.007)</ta>
            <ta e="T148" id="Seg_1257" s="T145">AR_1965_RestlessNight_transl.024 (002.008)</ta>
            <ta e="T154" id="Seg_1258" s="T148">AR_1965_RestlessNight_transl.025 (002.009)</ta>
            <ta e="T156" id="Seg_1259" s="T154">AR_1965_RestlessNight_transl.026 (002.010)</ta>
            <ta e="T160" id="Seg_1260" s="T156">AR_1965_RestlessNight_transl.027 (002.011)</ta>
            <ta e="T166" id="Seg_1261" s="T160">AR_1965_RestlessNight_transl.028 (002.012)</ta>
            <ta e="T169" id="Seg_1262" s="T166">AR_1965_RestlessNight_transl.029 (002.013)</ta>
            <ta e="T174" id="Seg_1263" s="T169">AR_1965_RestlessNight_transl.030 (002.014)</ta>
            <ta e="T185" id="Seg_1264" s="T174">AR_1965_RestlessNight_transl.031 (003.001)</ta>
            <ta e="T190" id="Seg_1265" s="T185">AR_1965_RestlessNight_transl.032 (003.002)</ta>
            <ta e="T198" id="Seg_1266" s="T190">AR_1965_RestlessNight_transl.033 (003.003)</ta>
            <ta e="T201" id="Seg_1267" s="T198">AR_1965_RestlessNight_transl.034 (003.004)</ta>
            <ta e="T204" id="Seg_1268" s="T201">AR_1965_RestlessNight_transl.035 (003.005)</ta>
            <ta e="T206" id="Seg_1269" s="T204">AR_1965_RestlessNight_transl.036 (003.006)</ta>
            <ta e="T210" id="Seg_1270" s="T206">AR_1965_RestlessNight_transl.037 (003.007)</ta>
            <ta e="T214" id="Seg_1271" s="T210">AR_1965_RestlessNight_transl.038 (003.008)</ta>
            <ta e="T219" id="Seg_1272" s="T214">AR_1965_RestlessNight_transl.039 (003.009)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T2" id="Seg_1273" s="T0">′лыбыГың ӓса.</ta>
            <ta e="T13" id="Seg_1274" s="T2">ма′рат ′ме̄lтъ ′патkылсӓ kондытыl ко̄′таkынты, ′по̄пы (′кутырмо̄с) ′ӱ̄ндышʼит kайгос ′тукымын.</ta>
            <ta e="T21" id="Seg_1275" s="T13">′тӓп ′тӓ̄нап′тысытӓ ку′тар Андре ′мӯлалтымпысыт ′kумый ′kӓлый чоты.</ta>
            <ta e="T25" id="Seg_1276" s="T21">Марат ку′ралʼна ′kӓт ′порантъ.</ta>
            <ta e="T34" id="Seg_1277" s="T25">тӓп ку′ралсы′нынтык лʼыпыkый ′kанып и ′сы̄ɣылсы ′инна ′мӓ[ы]ттанты.</ta>
            <ta e="T40" id="Seg_1278" s="T34">ай тӓп ′ӱндышит ′кутыкош ′тукымпа.</ta>
            <ta e="T46" id="Seg_1279" s="T40">ни kайим ′аша ′тӓ̄нымыlа марат ′нылаис.</ta>
            <ta e="T49" id="Seg_1280" s="T46">′по̄п[б̂]ы ′а̄таlис ′ӣралʼип.</ta>
            <ta e="T54" id="Seg_1281" s="T49">′ӯтаkынты ′тӓбын мы′kын ′ӓ̄са по.</ta>
            <ta e="T60" id="Seg_1282" s="T54">Ма′рат то′раватыс, тӓп чап′тӓн ′тӓ̄нымысыт ‵иралʼиб̂ым.</ta>
            <ta e="T68" id="Seg_1283" s="T60">Иван Павлович Лунь д̂и′журнатысы пин по̄ɣӓн, ′kӓт ′пораɣын.</ta>
            <ta e="T76" id="Seg_1284" s="T68">′тӓп ′иlа ′тымды ′меlды, ′тымды тӓ′мей ′челыксӓ.</ta>
            <ta e="T81" id="Seg_1285" s="T76">′тӓм ′ӓ̄са ′сомак ′kӓ̄лычилʼ kум.</ta>
            <ta e="T86" id="Seg_1286" s="T81">тӓп ′тӓнымысыт ′орса кочи чаттӓм.</ta>
            <ta e="T98" id="Seg_1287" s="T86">′ӱтныl челыт тӓп ′кыкысыт ′иjатыса ′омдыkа ′кун ӓма ӱтыканыkын ′тӱ̄тkаныкын.</ta>
            <ta e="T107" id="Seg_1288" s="T98">Ма′рат ′kъ̊ссы ′лунсӓ ай ′соkысыт ′тӓпым kӓнка ′kӓлыjа ′иjатыса.</ta>
            <ta e="T118" id="Seg_1289" s="T107">у′кот ку′шаkын (кушакос) ′ӓса ′челам, ӣнатыса (′kаlтырыкак ′kарын ′морʼат kаныkын).</ta>
            <ta e="T121" id="Seg_1290" s="T118">ӣнатыса ′kорасаң ′морʼаkын.</ta>
            <ta e="T126" id="Seg_1291" s="T121">тӣ ′тӓ̄мыkынтын ′мӓныl ′ӱ̄рал.</ta>
            <ta e="T136" id="Seg_1292" s="T126">тан ′ме̄нтаl ′ӱрытkа ′соkынчас Филлипмыkын, тӓм ′тӓнты ′мунтык мулалтӓнтыңыты.</ta>
            <ta e="T139" id="Seg_1293" s="T136">′kай ий ′Филʼипмыkын?</ta>
            <ta e="T141" id="Seg_1294" s="T139">′еан ′ни[ы]lджий.</ta>
            <ta e="T145" id="Seg_1295" s="T141">′тӓм ′манмыkак тӣ.</ta>
            <ta e="T148" id="Seg_1296" s="T145">тӓм инжи′нер ′Ленинградмы.</ta>
            <ta e="T154" id="Seg_1297" s="T148">тан ′уккырна ′ӱнд̂ыисаl ′тӓпын фамилʼиjам Ключин.</ta>
            <ta e="T156" id="Seg_1298" s="T154">ман ′ӱ̄ндыисам.</ta>
            <ta e="T160" id="Seg_1299" s="T156">филипп ′орса ′сома kум.</ta>
            <ta e="T166" id="Seg_1300" s="T160">тӓп ′тымты тӓ̄ ка′рабълʼип ′мандалʼ‵туkы′ңыты [′мандалтуkыка].</ta>
            <ta e="T169" id="Seg_1301" s="T166">kӓтысӓт ′сома ′еjа.</ta>
            <ta e="T174" id="Seg_1302" s="T169">′пеlдӓн̂там ман ′ӣjатым, ′kӓтысыты тӓм.</ta>
            <ta e="T185" id="Seg_1303" s="T174">марат ′kӓтъсыт ′ӣра‵лʼибъның «спасибо» ′ме̄нтаl ′kӓ̄лычʼиl kумының, ′тӓпаkӓй kу′раlсоɣый ′мӓныl ′пӣлаkты.</ta>
            <ta e="T190" id="Seg_1304" s="T185">ма′рат ′kоlчисыт, ма′jак ′аса чо̄пымпа.</ta>
            <ta e="T198" id="Seg_1305" s="T190">тӓп ′kолчисыт ′kаjиkос ′kошкеjа ай ′чаkтыса kу′ралсӓ.</ta>
            <ta e="T201" id="Seg_1306" s="T198">тӓп ′че̨тысыт ′Алʼенкам.</ta>
            <ta e="T204" id="Seg_1307" s="T201">′Паkтылымый ′точа ′чактыса.</ta>
            <ta e="T206" id="Seg_1308" s="T204">′тӓпаkий kа̄′лʼлʼей и′соkый.</ta>
            <ta e="T210" id="Seg_1309" s="T206">кутъ kос ′паkтат ′ӱттъ[ӓ].</ta>
            <ta e="T214" id="Seg_1310" s="T210">ме ′ӓ̄наlисомын ′паkтыка ′ӱтты.</ta>
            <ta e="T219" id="Seg_1311" s="T214">′нымды ′ӓ̄сӓ ′кочи ′ӱтkылʼ по̄.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T2" id="Seg_1312" s="T0">lɨpɨГɨŋ äsa.</ta>
            <ta e="T13" id="Seg_1313" s="T2">marat meːlʼtə patqɨlsä qondɨtɨlʼ koːtaqɨntɨ, poːpɨ (kutɨrmoːs) üːndɨšit qajgos tukɨmɨn.</ta>
            <ta e="T21" id="Seg_1314" s="T13">täp täːnaptɨsɨtä kutar Аndre muːlaltɨmpɨsɨt qumɨj qälɨj čʼotɨ.</ta>
            <ta e="T25" id="Seg_1315" s="T21">Мarat kuralʼna qät porantə.</ta>
            <ta e="T34" id="Seg_1316" s="T25">täp kuralsɨnɨntɨk lʼɨpɨqɨj qanɨp i sɨːqɨlsɨ inna mä[ɨ]ttantɨ.</ta>
            <ta e="T40" id="Seg_1317" s="T34">aj täp ündɨšit kutɨkoš tukɨmpa.</ta>
            <ta e="T46" id="Seg_1318" s="T40">ni qajim aša täːnɨmɨlʼa marat nɨlais.</ta>
            <ta e="T49" id="Seg_1319" s="T46">poːp[p̂]ɨ aːtalʼis iːralʼip.</ta>
            <ta e="T54" id="Seg_1320" s="T49">uːtaqɨntɨ täpɨn mɨqɨn äːsa po.</ta>
            <ta e="T60" id="Seg_1321" s="T54">Мarat toravatɨs, täp čʼaptän täːnɨmɨsɨt iralʼip̂ɨm.</ta>
            <ta e="T68" id="Seg_1322" s="T60">Иvan Пavlovičʼ Лunь d̂iжurnatɨsɨ pin poːqän, qät poraqɨn.</ta>
            <ta e="T76" id="Seg_1323" s="T68">täp ilʼa tɨmdɨ melʼdɨ, tɨmdɨ tämej čʼelɨksä.</ta>
            <ta e="T81" id="Seg_1324" s="T76">täm äːsa somak qäːlɨčʼilʼ qum.</ta>
            <ta e="T86" id="Seg_1325" s="T81">täp tänɨmɨsɨt orsa kočʼi čʼattäm.</ta>
            <ta e="T98" id="Seg_1326" s="T86">ütnɨlʼ čʼelɨt täp kɨkɨsɨt ijatɨsa omdɨqa kun äma ütɨkanɨqɨn tüːtqanɨkɨn.</ta>
            <ta e="T107" id="Seg_1327" s="T98">Мarat qəssɨ lunsä aj soqɨsɨt täpɨm qänka qälɨja ijatɨsa.</ta>
            <ta e="T118" id="Seg_1328" s="T107">ukot kušaqɨn (kušakos) äsa čʼelam, iːnatɨsa (qalʼtɨrɨkak qarɨn morʼat qanɨqɨn).</ta>
            <ta e="T121" id="Seg_1329" s="T118">iːnatɨsa qorasaŋ morʼaqɨn.</ta>
            <ta e="T126" id="Seg_1330" s="T121">tiː täːmɨqɨntɨn mänɨlʼ üːral.</ta>
            <ta e="T136" id="Seg_1331" s="T126">tan meːntalʼ ürɨtqa soqɨnčʼas Фillipmɨqɨn, täm täntɨ muntɨk mulaltäntɨŋɨtɨ.</ta>
            <ta e="T139" id="Seg_1332" s="T136">qaj ij Фilʼipmɨqɨn?</ta>
            <ta e="T141" id="Seg_1333" s="T139">ean ni[ɨ]lʼdжij.</ta>
            <ta e="T145" id="Seg_1334" s="T141">täm manmɨqak tiː.</ta>
            <ta e="T148" id="Seg_1335" s="T145">täm inжiner leningradmɨ.</ta>
            <ta e="T156" id="Seg_1336" s="T154">man üːndɨisam.</ta>
            <ta e="T160" id="Seg_1337" s="T156">filipp orsa soma qum.</ta>
            <ta e="T166" id="Seg_1338" s="T160">täp tɨmtɨ täː karapəlʼip mandalʼtuqɨŋɨtɨ [mandaltuqɨka].</ta>
            <ta e="T169" id="Seg_1339" s="T166">qätɨsät soma eja.</ta>
            <ta e="T174" id="Seg_1340" s="T169">pelʼdän̂tam man iːjatɨm, qätɨsɨtɨ täm.</ta>
            <ta e="T185" id="Seg_1341" s="T174">marat qätəsɨt iːralʼipənɨŋ «spasipo» meːntalʼ qäːlɨčʼʼilʼ qumɨnɨŋ, täpaqäj quralʼsoqɨj mänɨlʼ piːlaqtɨ.</ta>
            <ta e="T190" id="Seg_1342" s="T185">marat qolʼčʼisɨt, majak asa čʼoːpɨmpa.</ta>
            <ta e="T198" id="Seg_1343" s="T190">täp qolčʼisɨt qajiqos qoškeja aj čʼaqtɨsa quralsä.</ta>
            <ta e="T201" id="Seg_1344" s="T198">täp čʼetɨsɨt Аlʼenkam.</ta>
            <ta e="T204" id="Seg_1345" s="T201">Пaqtɨlɨmɨj točʼa čʼaktɨsa.</ta>
            <ta e="T206" id="Seg_1346" s="T204">täpaqij qaːlʼlʼej isoqɨj.</ta>
            <ta e="T210" id="Seg_1347" s="T206">kutə qos paqtat üttə[ä].</ta>
            <ta e="T214" id="Seg_1348" s="T210">me äːnalʼisomɨn paqtɨka üttɨ.</ta>
            <ta e="T219" id="Seg_1349" s="T214">nɨmdɨ äːsä kočʼi ütqɨlʼ poː.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2" id="Seg_1350" s="T0">Lɨpɨqɨn ɛːsa. </ta>
            <ta e="T13" id="Seg_1351" s="T2">Marat meːltə patqɨlsä qontɨtɨlʼ kotaqɨntɨ, pɔːpɨ (kutɨp qos) üntɨšit qaj kos tukɨmɨn. </ta>
            <ta e="T21" id="Seg_1352" s="T13">Täp tɛnaptɨsɨtä kutar Аndre mulaltɨmpɨsɨt qumɨj qäːlɨj čʼɔːtɨ. </ta>
            <ta e="T25" id="Seg_1353" s="T21">Marat kuralʼna qäːt pɔːrantə. </ta>
            <ta e="T34" id="Seg_1354" s="T25">Täp kuralsɨ nɨntɨk lʼɨpɨqɨj qanɨp i sɨːqɨlsɨ ınna mättantɨ. </ta>
            <ta e="T40" id="Seg_1355" s="T34">Aj täp üntɨšit kutɨ koš tukɨmpa. </ta>
            <ta e="T46" id="Seg_1356" s="T40">Ni qajim aša tɛnɨmɨla Marat nɨlaıːs. </ta>
            <ta e="T49" id="Seg_1357" s="T46">Pɔːpɨ atalis iːralʼıːp. </ta>
            <ta e="T54" id="Seg_1358" s="T49">Utaqɨntɨ täpɨn mɨqɨn ɛːsa po. </ta>
            <ta e="T60" id="Seg_1359" s="T54">Marat torawatɨs, täp čʼaptäːn tɛnɨmɨsɨt iralʼıːpɨm. </ta>
            <ta e="T68" id="Seg_1360" s="T60">Ivan Pavlovičʼ Lunʼ dižurnatɨsɨ pin poːqän, qäːt pɔːraqɨn. </ta>
            <ta e="T76" id="Seg_1361" s="T68">Täp ila tɨmtɨ meːltɨ, tɨmtɨ täm ɛj čʼeːlɨksä. </ta>
            <ta e="T81" id="Seg_1362" s="T76">Täm ɛːsa somak qäːlɨčʼčʼilʼ qum. </ta>
            <ta e="T86" id="Seg_1363" s="T81">Täp tɛnɨmɨsɨt orsa kočʼi čʼattäm. </ta>
            <ta e="T98" id="Seg_1364" s="T86">Ütnɨlʼ čʼeːlɨt täp kɨkɨsɨt ijatɨsa ɔːmtɨqa kun ɛːma ütɨ qanɨqqɨn tüt qanɨqqɨn. </ta>
            <ta e="T107" id="Seg_1365" s="T98">Marat qəssɨ Lunsä aj soqɨssɨt täpɨm qänqa qäːlɨjja ijatɨsa. </ta>
            <ta e="T118" id="Seg_1366" s="T107">Ukoːt kušaqɨn (/kuša kos) ɛːsa čʼeːlam, iːnatɨsa (qaltɨrɨkkak qarɨn morʼat qanɨqqɨn). </ta>
            <ta e="T121" id="Seg_1367" s="T118">Iːnatɨsa korasaŋ morʼaqɨn. </ta>
            <ta e="T126" id="Seg_1368" s="T121">Tiː tɛː mɨqɨntɨn mänɨlʼ üral. </ta>
            <ta e="T136" id="Seg_1369" s="T126">Tan mɛntalʼ ürɨtqa soqɨnčʼas Fillip mɨqɨn, täm täntɨ muntɨk mulaltäntɨŋɨtɨ. </ta>
            <ta e="T139" id="Seg_1370" s="T136">Qajij Filʼip mɨqɨn? </ta>
            <ta e="T141" id="Seg_1371" s="T139">Ɛːan nılʼčʼij. </ta>
            <ta e="T145" id="Seg_1372" s="T141">Täm man mɨqak tiː. </ta>
            <ta e="T148" id="Seg_1373" s="T145">Täm inžiner Leningradnɨ. </ta>
            <ta e="T154" id="Seg_1374" s="T148">Tan ukkɨrna üntɨıːsal täpɨn familʼijam Klʼučʼin. </ta>
            <ta e="T156" id="Seg_1375" s="T154">Man üntɨıːsam. </ta>
            <ta e="T160" id="Seg_1376" s="T156">Filipp orsa soma qum. </ta>
            <ta e="T166" id="Seg_1377" s="T160">Täp tɨmtɨ tɛː karabəlʼip mantalʼtukkɨŋɨtɨ (/mantaltukkɨsa). </ta>
            <ta e="T169" id="Seg_1378" s="T166">Kätɨsät soma ɛːja. </ta>
            <ta e="T174" id="Seg_1379" s="T169">Pɛltɛntam man iːjatɨm, kätɨsɨtɨ täm. </ta>
            <ta e="T185" id="Seg_1380" s="T174">Marat kätəsɨt iːralʼıːpənɨŋ spasipo mɛntalʼ qäːlɨčʼčʼilʼ qumɨnɨŋ, täpaqäj kuralsɔːqıj mänɨlʼ pilaqtɨ. </ta>
            <ta e="T190" id="Seg_1381" s="T185">Marat qolʼčʼisɨt, majak asa čʼɔːpɨmpa. </ta>
            <ta e="T198" id="Seg_1382" s="T190">Täp qolčʼisɨt qaji qos qoškeja aj čʼaktɨsa kuralsä. </ta>
            <ta e="T201" id="Seg_1383" s="T198">Täp čʼeːtɨsɨt Аlʼenkam. </ta>
            <ta e="T204" id="Seg_1384" s="T201">Paktɨlɨmɨj točʼa čʼaktɨsa. </ta>
            <ta e="T206" id="Seg_1385" s="T204">Täpaqıj qalʼlʼɛːjisɔːqıj. </ta>
            <ta e="T210" id="Seg_1386" s="T206">Kutə qos paktat üttə. </ta>
            <ta e="T214" id="Seg_1387" s="T210">Me ɛnalisɔːmɨn paktɨqa üttɨ. </ta>
            <ta e="T219" id="Seg_1388" s="T214">Nɨmtɨ ɛːsä kočʼi ütqɨlʼ poː. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1389" s="T0">lɨpɨ-qɨn</ta>
            <ta e="T2" id="Seg_1390" s="T1">ɛː-sa</ta>
            <ta e="T3" id="Seg_1391" s="T2">marat</ta>
            <ta e="T4" id="Seg_1392" s="T3">meːltə</ta>
            <ta e="T5" id="Seg_1393" s="T4">pat-qɨl-sä</ta>
            <ta e="T6" id="Seg_1394" s="T5">qontɨ-tɨ-lʼ</ta>
            <ta e="T7" id="Seg_1395" s="T6">kota-qɨn-tɨ</ta>
            <ta e="T8" id="Seg_1396" s="T7">pɔːpɨ</ta>
            <ta e="T220" id="Seg_1397" s="T8">kutɨ-p</ta>
            <ta e="T9" id="Seg_1398" s="T220">qos</ta>
            <ta e="T10" id="Seg_1399" s="T9">üntɨ-š-i-t</ta>
            <ta e="T11" id="Seg_1400" s="T10">qaj</ta>
            <ta e="T12" id="Seg_1401" s="T11">kos</ta>
            <ta e="T13" id="Seg_1402" s="T12">tukɨ-mɨ-n</ta>
            <ta e="T14" id="Seg_1403" s="T13">täp</ta>
            <ta e="T15" id="Seg_1404" s="T14">tɛna-ptɨ-sɨ-tä</ta>
            <ta e="T16" id="Seg_1405" s="T15">kutar</ta>
            <ta e="T17" id="Seg_1406" s="T16">Аndre</ta>
            <ta e="T18" id="Seg_1407" s="T17">mul-altɨ-mpɨ-sɨ-t</ta>
            <ta e="T19" id="Seg_1408" s="T18">qum-ɨ-j</ta>
            <ta e="T20" id="Seg_1409" s="T19">qäːlɨ-j</ta>
            <ta e="T21" id="Seg_1410" s="T20">čʼɔːtɨ</ta>
            <ta e="T22" id="Seg_1411" s="T21">marat</ta>
            <ta e="T23" id="Seg_1412" s="T22">kur-alʼ-na</ta>
            <ta e="T24" id="Seg_1413" s="T23">qäː-t</ta>
            <ta e="T25" id="Seg_1414" s="T24">pɔːra-ntə</ta>
            <ta e="T26" id="Seg_1415" s="T25">täp</ta>
            <ta e="T27" id="Seg_1416" s="T26">kur-al-sɨ</ta>
            <ta e="T28" id="Seg_1417" s="T27">nɨntɨk</ta>
            <ta e="T29" id="Seg_1418" s="T28">lʼɨpɨ-qɨ-j</ta>
            <ta e="T30" id="Seg_1419" s="T29">qanɨ-p</ta>
            <ta e="T31" id="Seg_1420" s="T30">i</ta>
            <ta e="T32" id="Seg_1421" s="T31">sɨːqɨl-sɨ</ta>
            <ta e="T33" id="Seg_1422" s="T32">ınna</ta>
            <ta e="T34" id="Seg_1423" s="T33">mätta-ntɨ</ta>
            <ta e="T35" id="Seg_1424" s="T34">aj</ta>
            <ta e="T36" id="Seg_1425" s="T35">täp</ta>
            <ta e="T37" id="Seg_1426" s="T36">üntɨ-š-i-t</ta>
            <ta e="T38" id="Seg_1427" s="T37">kutɨ</ta>
            <ta e="T39" id="Seg_1428" s="T38">koš</ta>
            <ta e="T40" id="Seg_1429" s="T39">tukɨ-mpa</ta>
            <ta e="T41" id="Seg_1430" s="T40">ni</ta>
            <ta e="T42" id="Seg_1431" s="T41">qaj-i-m</ta>
            <ta e="T43" id="Seg_1432" s="T42">aša</ta>
            <ta e="T44" id="Seg_1433" s="T43">tɛnɨmɨ-la</ta>
            <ta e="T45" id="Seg_1434" s="T44">marat</ta>
            <ta e="T46" id="Seg_1435" s="T45">nɨl-aıː-s</ta>
            <ta e="T47" id="Seg_1436" s="T46">pɔːpɨ</ta>
            <ta e="T48" id="Seg_1437" s="T47">at-al-i-s</ta>
            <ta e="T49" id="Seg_1438" s="T48">iːra-lʼıːp</ta>
            <ta e="T50" id="Seg_1439" s="T49">uta-qɨn-tɨ</ta>
            <ta e="T51" id="Seg_1440" s="T50">täp-ɨ-n</ta>
            <ta e="T52" id="Seg_1441" s="T51">mɨ-qɨn</ta>
            <ta e="T53" id="Seg_1442" s="T52">ɛː-sa</ta>
            <ta e="T54" id="Seg_1443" s="T53">po</ta>
            <ta e="T55" id="Seg_1444" s="T54">marat</ta>
            <ta e="T56" id="Seg_1445" s="T55">torawatɨ-s</ta>
            <ta e="T57" id="Seg_1446" s="T56">täp</ta>
            <ta e="T58" id="Seg_1447" s="T57">čʼaptäː-n</ta>
            <ta e="T59" id="Seg_1448" s="T58">tɛnɨmɨ-sɨ-t</ta>
            <ta e="T60" id="Seg_1449" s="T59">ira-lʼıːpɨ-m</ta>
            <ta e="T61" id="Seg_1450" s="T60">ivan</ta>
            <ta e="T62" id="Seg_1451" s="T61">Pavlovičʼ</ta>
            <ta e="T63" id="Seg_1452" s="T62">Lunʼ</ta>
            <ta e="T64" id="Seg_1453" s="T63">dižurnatɨ-sɨ</ta>
            <ta e="T65" id="Seg_1454" s="T64">pi-n</ta>
            <ta e="T66" id="Seg_1455" s="T65">poː-qän</ta>
            <ta e="T67" id="Seg_1456" s="T66">qäː-t</ta>
            <ta e="T68" id="Seg_1457" s="T67">pɔːra-qɨn</ta>
            <ta e="T69" id="Seg_1458" s="T68">täp</ta>
            <ta e="T70" id="Seg_1459" s="T69">ila</ta>
            <ta e="T71" id="Seg_1460" s="T70">tɨmtɨ</ta>
            <ta e="T72" id="Seg_1461" s="T71">meːltɨ</ta>
            <ta e="T73" id="Seg_1462" s="T72">tɨmtɨ</ta>
            <ta e="T74" id="Seg_1463" s="T73">täm</ta>
            <ta e="T75" id="Seg_1464" s="T74">ɛj</ta>
            <ta e="T76" id="Seg_1465" s="T75">čʼeːlɨk-sä</ta>
            <ta e="T77" id="Seg_1466" s="T76">täm</ta>
            <ta e="T78" id="Seg_1467" s="T77">ɛː-sa</ta>
            <ta e="T79" id="Seg_1468" s="T78">soma-k</ta>
            <ta e="T80" id="Seg_1469" s="T79">qäːlɨ-čʼ-čʼilʼ</ta>
            <ta e="T81" id="Seg_1470" s="T80">qum</ta>
            <ta e="T82" id="Seg_1471" s="T81">täp</ta>
            <ta e="T83" id="Seg_1472" s="T82">tɛnɨmɨ-sɨ-t</ta>
            <ta e="T84" id="Seg_1473" s="T83">or-sa</ta>
            <ta e="T85" id="Seg_1474" s="T84">kočʼi</ta>
            <ta e="T86" id="Seg_1475" s="T85">čʼattä-m</ta>
            <ta e="T87" id="Seg_1476" s="T86">üt-n-ɨ-lʼ</ta>
            <ta e="T88" id="Seg_1477" s="T87">čʼeːlɨ-t</ta>
            <ta e="T89" id="Seg_1478" s="T88">täp</ta>
            <ta e="T90" id="Seg_1479" s="T89">kɨkɨ-sɨ-t</ta>
            <ta e="T91" id="Seg_1480" s="T90">ija-t-ɨ-sa</ta>
            <ta e="T92" id="Seg_1481" s="T91">ɔːmtɨ-qa</ta>
            <ta e="T93" id="Seg_1482" s="T92">kun</ta>
            <ta e="T94" id="Seg_1483" s="T93">ɛːma</ta>
            <ta e="T95" id="Seg_1484" s="T94">ütɨ</ta>
            <ta e="T96" id="Seg_1485" s="T95">qanɨq-qɨn</ta>
            <ta e="T97" id="Seg_1486" s="T96">tü-t</ta>
            <ta e="T98" id="Seg_1487" s="T97">qanɨq-qɨn</ta>
            <ta e="T99" id="Seg_1488" s="T98">marat</ta>
            <ta e="T100" id="Seg_1489" s="T99">qəs-sɨ</ta>
            <ta e="T101" id="Seg_1490" s="T100">lun-sä</ta>
            <ta e="T102" id="Seg_1491" s="T101">aj</ta>
            <ta e="T103" id="Seg_1492" s="T102">soqɨs-sɨ-t</ta>
            <ta e="T104" id="Seg_1493" s="T103">täp-ɨ-m</ta>
            <ta e="T105" id="Seg_1494" s="T104">qän-qa</ta>
            <ta e="T106" id="Seg_1495" s="T105">qäːlɨ-j-ja</ta>
            <ta e="T107" id="Seg_1496" s="T106">ija-t-ɨ-sa</ta>
            <ta e="T108" id="Seg_1497" s="T107">ukoːt</ta>
            <ta e="T109" id="Seg_1498" s="T108">kuša-qɨn</ta>
            <ta e="T110" id="Seg_1499" s="T109">kuša</ta>
            <ta e="T111" id="Seg_1500" s="T110">kos</ta>
            <ta e="T112" id="Seg_1501" s="T111">ɛː-sa</ta>
            <ta e="T113" id="Seg_1502" s="T112">čʼeːla-m</ta>
            <ta e="T114" id="Seg_1503" s="T113">iːna-t-ɨ-sa</ta>
            <ta e="T115" id="Seg_1504" s="T114">qal-tɨr-ɨ-kka-k</ta>
            <ta e="T116" id="Seg_1505" s="T115">qarɨ-n</ta>
            <ta e="T117" id="Seg_1506" s="T116">morʼa-t</ta>
            <ta e="T118" id="Seg_1507" s="T117">qanɨq-qɨn</ta>
            <ta e="T119" id="Seg_1508" s="T118">iːna-t-ɨ-sa</ta>
            <ta e="T120" id="Seg_1509" s="T119">kora-sa-ŋ</ta>
            <ta e="T121" id="Seg_1510" s="T120">morʼa-qɨn</ta>
            <ta e="T122" id="Seg_1511" s="T121">tiː</ta>
            <ta e="T123" id="Seg_1512" s="T122">tɛː</ta>
            <ta e="T124" id="Seg_1513" s="T123">mɨ-qɨn-tɨn</ta>
            <ta e="T125" id="Seg_1514" s="T124">mänɨlʼ</ta>
            <ta e="T126" id="Seg_1515" s="T125">üra-l</ta>
            <ta e="T127" id="Seg_1516" s="T126">tat</ta>
            <ta e="T128" id="Seg_1517" s="T127">mɛntalʼ</ta>
            <ta e="T129" id="Seg_1518" s="T128">ürɨ-tqa</ta>
            <ta e="T130" id="Seg_1519" s="T129">soqɨ-nčʼ-as</ta>
            <ta e="T131" id="Seg_1520" s="T130">Fillip</ta>
            <ta e="T132" id="Seg_1521" s="T131">mɨ-qɨn</ta>
            <ta e="T133" id="Seg_1522" s="T132">täm</ta>
            <ta e="T134" id="Seg_1523" s="T133">täntɨ</ta>
            <ta e="T135" id="Seg_1524" s="T134">muntɨk</ta>
            <ta e="T136" id="Seg_1525" s="T135">mul-altä-ntɨ-ŋɨ-tɨ</ta>
            <ta e="T137" id="Seg_1526" s="T136">qaj-i-j</ta>
            <ta e="T138" id="Seg_1527" s="T137">Filʼip</ta>
            <ta e="T139" id="Seg_1528" s="T138">mɨ-qɨn</ta>
            <ta e="T140" id="Seg_1529" s="T139">ɛː-a-n</ta>
            <ta e="T141" id="Seg_1530" s="T140">nılʼčʼi-j</ta>
            <ta e="T142" id="Seg_1531" s="T141">täm</ta>
            <ta e="T143" id="Seg_1532" s="T142">man</ta>
            <ta e="T144" id="Seg_1533" s="T143">mɨ-qak</ta>
            <ta e="T145" id="Seg_1534" s="T144">tiː</ta>
            <ta e="T146" id="Seg_1535" s="T145">täm</ta>
            <ta e="T147" id="Seg_1536" s="T146">inžiner</ta>
            <ta e="T148" id="Seg_1537" s="T147">Leningrad-nɨ</ta>
            <ta e="T149" id="Seg_1538" s="T148">tat</ta>
            <ta e="T150" id="Seg_1539" s="T149">ukkɨrna</ta>
            <ta e="T151" id="Seg_1540" s="T150">üntɨ-ıː-sa-l</ta>
            <ta e="T152" id="Seg_1541" s="T151">täp-ɨ-n</ta>
            <ta e="T153" id="Seg_1542" s="T152">familʼija-m</ta>
            <ta e="T154" id="Seg_1543" s="T153">Klʼučʼin</ta>
            <ta e="T155" id="Seg_1544" s="T154">man</ta>
            <ta e="T156" id="Seg_1545" s="T155">üntɨ-ıː-sa-m</ta>
            <ta e="T157" id="Seg_1546" s="T156">filipp</ta>
            <ta e="T158" id="Seg_1547" s="T157">or-sa</ta>
            <ta e="T159" id="Seg_1548" s="T158">soma</ta>
            <ta e="T160" id="Seg_1549" s="T159">qum</ta>
            <ta e="T161" id="Seg_1550" s="T160">täp</ta>
            <ta e="T162" id="Seg_1551" s="T161">tɨmtɨ</ta>
            <ta e="T163" id="Seg_1552" s="T162">tɛː</ta>
            <ta e="T164" id="Seg_1553" s="T163">karabəlʼ-i-p</ta>
            <ta e="T165" id="Seg_1554" s="T164">mant-alʼtu-kkɨ-ŋɨ-tɨ</ta>
            <ta e="T166" id="Seg_1555" s="T165">mant-altu-kkɨ-sa</ta>
            <ta e="T167" id="Seg_1556" s="T166">kätɨ-sä-t</ta>
            <ta e="T168" id="Seg_1557" s="T167">soma</ta>
            <ta e="T169" id="Seg_1558" s="T168">ɛː-ja</ta>
            <ta e="T170" id="Seg_1559" s="T169">pɛlt-ɛnta-m</ta>
            <ta e="T171" id="Seg_1560" s="T170">man</ta>
            <ta e="T172" id="Seg_1561" s="T171">iːja-t-ɨ-m</ta>
            <ta e="T173" id="Seg_1562" s="T172">kätɨ-sɨ-tɨ</ta>
            <ta e="T174" id="Seg_1563" s="T173">täm</ta>
            <ta e="T175" id="Seg_1564" s="T174">marat</ta>
            <ta e="T176" id="Seg_1565" s="T175">kätə-sɨ-t</ta>
            <ta e="T177" id="Seg_1566" s="T176">iːra-lʼıːpə-nɨŋ</ta>
            <ta e="T178" id="Seg_1567" s="T177">spasipo</ta>
            <ta e="T179" id="Seg_1568" s="T178">mɛntalʼ</ta>
            <ta e="T180" id="Seg_1569" s="T179">qäːlɨ-čʼ-čʼilʼ</ta>
            <ta e="T181" id="Seg_1570" s="T180">qum-ɨ-nɨŋ</ta>
            <ta e="T182" id="Seg_1571" s="T181">täp-aqäj</ta>
            <ta e="T183" id="Seg_1572" s="T182">kur-al-sɔː-qıj</ta>
            <ta e="T184" id="Seg_1573" s="T183">mänɨlʼ</ta>
            <ta e="T185" id="Seg_1574" s="T184">pilaq-tɨ</ta>
            <ta e="T186" id="Seg_1575" s="T185">marat</ta>
            <ta e="T187" id="Seg_1576" s="T186">qo-lʼčʼi-sɨ-t</ta>
            <ta e="T188" id="Seg_1577" s="T187">majak</ta>
            <ta e="T189" id="Seg_1578" s="T188">asa</ta>
            <ta e="T190" id="Seg_1579" s="T189">čʼɔːpɨ-mpa</ta>
            <ta e="T191" id="Seg_1580" s="T190">täp</ta>
            <ta e="T192" id="Seg_1581" s="T191">qo-lčʼi-sɨ-t</ta>
            <ta e="T193" id="Seg_1582" s="T192">qaji</ta>
            <ta e="T194" id="Seg_1583" s="T193">qos</ta>
            <ta e="T195" id="Seg_1584" s="T194">qoške-ja</ta>
            <ta e="T196" id="Seg_1585" s="T195">aj</ta>
            <ta e="T197" id="Seg_1586" s="T196">čʼaktɨ-sa</ta>
            <ta e="T198" id="Seg_1587" s="T197">kur-al-sä</ta>
            <ta e="T199" id="Seg_1588" s="T198">täp</ta>
            <ta e="T200" id="Seg_1589" s="T199">čʼeːtɨ-sɨ-t</ta>
            <ta e="T201" id="Seg_1590" s="T200">Аlʼenka-m</ta>
            <ta e="T202" id="Seg_1591" s="T201">paktɨ-lɨ-mɨj</ta>
            <ta e="T203" id="Seg_1592" s="T202">točʼa</ta>
            <ta e="T204" id="Seg_1593" s="T203">čʼaktɨ-sa</ta>
            <ta e="T205" id="Seg_1594" s="T204">täp-aqıj</ta>
            <ta e="T206" id="Seg_1595" s="T205">qalʼlʼ-ɛː-ji-sɔː-qıj</ta>
            <ta e="T207" id="Seg_1596" s="T206">kutə</ta>
            <ta e="T208" id="Seg_1597" s="T207">qos</ta>
            <ta e="T209" id="Seg_1598" s="T208">pakta-t</ta>
            <ta e="T210" id="Seg_1599" s="T209">üt-tə</ta>
            <ta e="T211" id="Seg_1600" s="T210">me</ta>
            <ta e="T212" id="Seg_1601" s="T211">ɛna-li-sɔː-mɨn</ta>
            <ta e="T213" id="Seg_1602" s="T212">paktɨ-qa</ta>
            <ta e="T214" id="Seg_1603" s="T213">üt-tɨ</ta>
            <ta e="T215" id="Seg_1604" s="T214">nɨmtɨ</ta>
            <ta e="T216" id="Seg_1605" s="T215">ɛː-sä</ta>
            <ta e="T217" id="Seg_1606" s="T216">kočʼi</ta>
            <ta e="T218" id="Seg_1607" s="T217">üt-qɨ-lʼ</ta>
            <ta e="T219" id="Seg_1608" s="T218">poː</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1609" s="T0">*lɨpɨ-qɨn</ta>
            <ta e="T2" id="Seg_1610" s="T1">ɛː-sɨ</ta>
            <ta e="T3" id="Seg_1611" s="T2">marat</ta>
            <ta e="T4" id="Seg_1612" s="T3">meːltɨ</ta>
            <ta e="T5" id="Seg_1613" s="T4">pat-qɨl-sɨ</ta>
            <ta e="T6" id="Seg_1614" s="T5">qontɨ-tɨ-lʼ</ta>
            <ta e="T7" id="Seg_1615" s="T6">kota-qɨn-ntɨ</ta>
            <ta e="T8" id="Seg_1616" s="T7">pɔːpɨ</ta>
            <ta e="T220" id="Seg_1617" s="T8">kutɨ-m</ta>
            <ta e="T9" id="Seg_1618" s="T220">kos</ta>
            <ta e="T10" id="Seg_1619" s="T9">üntɨ-š-ɨ-tɨ</ta>
            <ta e="T11" id="Seg_1620" s="T10">qaj</ta>
            <ta e="T12" id="Seg_1621" s="T11">kos</ta>
            <ta e="T13" id="Seg_1622" s="T12">tukɨ-mpɨ-ŋɨ</ta>
            <ta e="T14" id="Seg_1623" s="T13">təp</ta>
            <ta e="T15" id="Seg_1624" s="T14">tɛnɨ-ptɨ-sɨ-tɨ</ta>
            <ta e="T16" id="Seg_1625" s="T15">kuttar</ta>
            <ta e="T17" id="Seg_1626" s="T16">Andre</ta>
            <ta e="T18" id="Seg_1627" s="T17">mulɨ-altɨ-mpɨ-sɨ-tɨ</ta>
            <ta e="T19" id="Seg_1628" s="T18">qum-ɨ-lʼ</ta>
            <ta e="T20" id="Seg_1629" s="T19">qəːlɨ-lʼ</ta>
            <ta e="T21" id="Seg_1630" s="T20">čʼɔːtɨ</ta>
            <ta e="T22" id="Seg_1631" s="T21">marat</ta>
            <ta e="T23" id="Seg_1632" s="T22">*kurɨ-alʼ-ŋɨ</ta>
            <ta e="T24" id="Seg_1633" s="T23">qəː-n</ta>
            <ta e="T25" id="Seg_1634" s="T24">pɔːrɨ-ntɨ</ta>
            <ta e="T26" id="Seg_1635" s="T25">təp</ta>
            <ta e="T27" id="Seg_1636" s="T26">*kurɨ-ätɔːl-sɨ</ta>
            <ta e="T28" id="Seg_1637" s="T27">nɨntɨk</ta>
            <ta e="T29" id="Seg_1638" s="T28">*lɨpɨ-qɨn-lʼ</ta>
            <ta e="T30" id="Seg_1639" s="T29">qanɨŋ-m</ta>
            <ta e="T31" id="Seg_1640" s="T30">i</ta>
            <ta e="T32" id="Seg_1641" s="T31">sɨːqɨl-sɨ</ta>
            <ta e="T33" id="Seg_1642" s="T32">ınnä</ta>
            <ta e="T34" id="Seg_1643" s="T33">wəttɨ-ntɨ</ta>
            <ta e="T35" id="Seg_1644" s="T34">aj</ta>
            <ta e="T36" id="Seg_1645" s="T35">təp</ta>
            <ta e="T37" id="Seg_1646" s="T36">üntɨ-š-ɨ-tɨ</ta>
            <ta e="T38" id="Seg_1647" s="T37">kutɨ</ta>
            <ta e="T39" id="Seg_1648" s="T38">kos</ta>
            <ta e="T40" id="Seg_1649" s="T39">tukɨ-mpɨ</ta>
            <ta e="T41" id="Seg_1650" s="T40">nʼi</ta>
            <ta e="T42" id="Seg_1651" s="T41">qaj-ɨ-m</ta>
            <ta e="T43" id="Seg_1652" s="T42">ašša</ta>
            <ta e="T44" id="Seg_1653" s="T43">tɛnɨmɨ-lä</ta>
            <ta e="T45" id="Seg_1654" s="T44">marat</ta>
            <ta e="T46" id="Seg_1655" s="T45">nɨl-ɛː-sɨ</ta>
            <ta e="T47" id="Seg_1656" s="T46">pɔːpɨ</ta>
            <ta e="T48" id="Seg_1657" s="T47">atɨ-ätɔːl-ɨ-sɨ</ta>
            <ta e="T49" id="Seg_1658" s="T48">ira-lıːpɨ</ta>
            <ta e="T50" id="Seg_1659" s="T49">utɨ-qɨn-tɨ</ta>
            <ta e="T51" id="Seg_1660" s="T50">təp-ɨ-n</ta>
            <ta e="T52" id="Seg_1661" s="T51">mɨ-qɨn</ta>
            <ta e="T53" id="Seg_1662" s="T52">ɛː-sɨ</ta>
            <ta e="T54" id="Seg_1663" s="T53">poː</ta>
            <ta e="T55" id="Seg_1664" s="T54">marat</ta>
            <ta e="T56" id="Seg_1665" s="T55">torowajtɨ-sɨ</ta>
            <ta e="T57" id="Seg_1666" s="T56">təp</ta>
            <ta e="T58" id="Seg_1667" s="T57">čʼaptä-n</ta>
            <ta e="T59" id="Seg_1668" s="T58">tɛnɨmɨ-sɨ-tɨ</ta>
            <ta e="T60" id="Seg_1669" s="T59">ira-lıːpɨ-m</ta>
            <ta e="T61" id="Seg_1670" s="T60">Ivan</ta>
            <ta e="T62" id="Seg_1671" s="T61">Pavlovičʼ</ta>
            <ta e="T63" id="Seg_1672" s="T62">Lunʼ</ta>
            <ta e="T64" id="Seg_1673" s="T63">dižurnatɨ-sɨ</ta>
            <ta e="T65" id="Seg_1674" s="T64">pi-n</ta>
            <ta e="T66" id="Seg_1675" s="T65">poː-qɨn</ta>
            <ta e="T67" id="Seg_1676" s="T66">qəː-n</ta>
            <ta e="T68" id="Seg_1677" s="T67">pɔːrɨ-qɨn</ta>
            <ta e="T69" id="Seg_1678" s="T68">təp</ta>
            <ta e="T70" id="Seg_1679" s="T69">ilɨ</ta>
            <ta e="T71" id="Seg_1680" s="T70">tɨmtɨ</ta>
            <ta e="T72" id="Seg_1681" s="T71">meːltɨ</ta>
            <ta e="T73" id="Seg_1682" s="T72">tɨmtɨ</ta>
            <ta e="T74" id="Seg_1683" s="T73">təp</ta>
            <ta e="T75" id="Seg_1684" s="T74">aj</ta>
            <ta e="T76" id="Seg_1685" s="T75">čʼeːlɨŋ-sɨ</ta>
            <ta e="T77" id="Seg_1686" s="T76">təp</ta>
            <ta e="T78" id="Seg_1687" s="T77">ɛː-sɨ</ta>
            <ta e="T79" id="Seg_1688" s="T78">soma-k</ta>
            <ta e="T80" id="Seg_1689" s="T79">qəːlɨ-š-ntɨlʼ</ta>
            <ta e="T81" id="Seg_1690" s="T80">qum</ta>
            <ta e="T82" id="Seg_1691" s="T81">təp</ta>
            <ta e="T83" id="Seg_1692" s="T82">tɛnɨmɨ-sɨ-tɨ</ta>
            <ta e="T84" id="Seg_1693" s="T83">orɨ-sä</ta>
            <ta e="T85" id="Seg_1694" s="T84">kočʼčʼɨ</ta>
            <ta e="T86" id="Seg_1695" s="T85">čʼaptä-m</ta>
            <ta e="T87" id="Seg_1696" s="T86">üːtɨ-t-ɨ-lʼ</ta>
            <ta e="T88" id="Seg_1697" s="T87">čʼeːlɨ-n</ta>
            <ta e="T89" id="Seg_1698" s="T88">təp</ta>
            <ta e="T90" id="Seg_1699" s="T89">kɨkɨ-sɨ-tɨ</ta>
            <ta e="T91" id="Seg_1700" s="T90">iːja-t-ɨ-sä</ta>
            <ta e="T92" id="Seg_1701" s="T91">ɔːmtɨ-qo</ta>
            <ta e="T93" id="Seg_1702" s="T92">kun</ta>
            <ta e="T94" id="Seg_1703" s="T93">ɛːmä</ta>
            <ta e="T95" id="Seg_1704" s="T94">üt</ta>
            <ta e="T96" id="Seg_1705" s="T95">qan-qɨn</ta>
            <ta e="T97" id="Seg_1706" s="T96">tü-n</ta>
            <ta e="T98" id="Seg_1707" s="T97">qan-qɨn</ta>
            <ta e="T99" id="Seg_1708" s="T98">marat</ta>
            <ta e="T100" id="Seg_1709" s="T99">qən-sɨ</ta>
            <ta e="T101" id="Seg_1710" s="T100">Lunʼ-sä</ta>
            <ta e="T102" id="Seg_1711" s="T101">aj</ta>
            <ta e="T103" id="Seg_1712" s="T102">soqɨš-sɨ-tɨ</ta>
            <ta e="T104" id="Seg_1713" s="T103">təp-ɨ-m</ta>
            <ta e="T105" id="Seg_1714" s="T104">qən-qo</ta>
            <ta e="T106" id="Seg_1715" s="T105">qəːlɨ-š-lä</ta>
            <ta e="T107" id="Seg_1716" s="T106">iːja-t-ɨ-sä</ta>
            <ta e="T108" id="Seg_1717" s="T107">ukoːn</ta>
            <ta e="T109" id="Seg_1718" s="T108">*kušša-qɨn</ta>
            <ta e="T110" id="Seg_1719" s="T109">*kušša</ta>
            <ta e="T111" id="Seg_1720" s="T110">kos</ta>
            <ta e="T112" id="Seg_1721" s="T111">ɛː-sɨ</ta>
            <ta e="T113" id="Seg_1722" s="T112">čʼeːlɨ-mɨ</ta>
            <ta e="T114" id="Seg_1723" s="T113">iːja-t-ɨ-sä</ta>
            <ta e="T115" id="Seg_1724" s="T114">qälɨ-ntɨr-ɨ-kkɨ-k</ta>
            <ta e="T116" id="Seg_1725" s="T115">qarɨ-n</ta>
            <ta e="T117" id="Seg_1726" s="T116">morä-n</ta>
            <ta e="T118" id="Seg_1727" s="T117">qan-qɨn</ta>
            <ta e="T119" id="Seg_1728" s="T118">iːja-t-ɨ-sä</ta>
            <ta e="T120" id="Seg_1729" s="T119">kora-sɨ-k</ta>
            <ta e="T121" id="Seg_1730" s="T120">morä-qɨn</ta>
            <ta e="T122" id="Seg_1731" s="T121">tiː</ta>
            <ta e="T123" id="Seg_1732" s="T122">tɛː</ta>
            <ta e="T124" id="Seg_1733" s="T123">mɨ-qɨn-tɨt</ta>
            <ta e="T125" id="Seg_1734" s="T124">mənɨlʼ</ta>
            <ta e="T126" id="Seg_1735" s="T125">ürɨ-lʼ</ta>
            <ta e="T127" id="Seg_1736" s="T126">tan</ta>
            <ta e="T128" id="Seg_1737" s="T127">mɛntälʼ</ta>
            <ta e="T129" id="Seg_1738" s="T128">ürɨ-tqo</ta>
            <ta e="T130" id="Seg_1739" s="T129">soqɨš-ntɨ-äšɨk</ta>
            <ta e="T131" id="Seg_1740" s="T130">Fillip</ta>
            <ta e="T132" id="Seg_1741" s="T131">mɨ-qɨn</ta>
            <ta e="T133" id="Seg_1742" s="T132">təp</ta>
            <ta e="T134" id="Seg_1743" s="T133">täntɨ</ta>
            <ta e="T135" id="Seg_1744" s="T134">muntɨk</ta>
            <ta e="T136" id="Seg_1745" s="T135">mulɨ-altɨ-ntɨ-ŋɨ-tɨ</ta>
            <ta e="T137" id="Seg_1746" s="T136">qaj-ɨ-lʼ</ta>
            <ta e="T138" id="Seg_1747" s="T137">Fillip</ta>
            <ta e="T139" id="Seg_1748" s="T138">mɨ-qɨn</ta>
            <ta e="T140" id="Seg_1749" s="T139">ɛː-ŋɨ-n</ta>
            <ta e="T141" id="Seg_1750" s="T140">nılʼčʼɨ-lʼ</ta>
            <ta e="T142" id="Seg_1751" s="T141">təp</ta>
            <ta e="T143" id="Seg_1752" s="T142">man</ta>
            <ta e="T144" id="Seg_1753" s="T143">mɨ-qäk</ta>
            <ta e="T145" id="Seg_1754" s="T144">tiː</ta>
            <ta e="T146" id="Seg_1755" s="T145">təp</ta>
            <ta e="T147" id="Seg_1756" s="T146">inžiner</ta>
            <ta e="T148" id="Seg_1757" s="T147">Leningrad-nɨ</ta>
            <ta e="T149" id="Seg_1758" s="T148">tan</ta>
            <ta e="T150" id="Seg_1759" s="T149">ukkɨrna</ta>
            <ta e="T151" id="Seg_1760" s="T150">üntɨ-ıː-sɨ-l</ta>
            <ta e="T152" id="Seg_1761" s="T151">təp-ɨ-n</ta>
            <ta e="T153" id="Seg_1762" s="T152">familʼija-m</ta>
            <ta e="T154" id="Seg_1763" s="T153">Klʼučʼin</ta>
            <ta e="T155" id="Seg_1764" s="T154">man</ta>
            <ta e="T156" id="Seg_1765" s="T155">üntɨ-ıː-sɨ-m</ta>
            <ta e="T157" id="Seg_1766" s="T156">Fillip</ta>
            <ta e="T158" id="Seg_1767" s="T157">orɨ-sä</ta>
            <ta e="T159" id="Seg_1768" s="T158">soma</ta>
            <ta e="T160" id="Seg_1769" s="T159">qum</ta>
            <ta e="T161" id="Seg_1770" s="T160">təp</ta>
            <ta e="T162" id="Seg_1771" s="T161">tɨmtɨ</ta>
            <ta e="T163" id="Seg_1772" s="T162">tɛː</ta>
            <ta e="T164" id="Seg_1773" s="T163">karabəlʼ-ɨ-m</ta>
            <ta e="T165" id="Seg_1774" s="T164">mantɨ-altɨ-kkɨ-ŋɨ-tɨ</ta>
            <ta e="T166" id="Seg_1775" s="T165">mantɨ-altɨ-kkɨ-sɨ</ta>
            <ta e="T167" id="Seg_1776" s="T166">kətɨ-sɨ-tɨ</ta>
            <ta e="T168" id="Seg_1777" s="T167">soma</ta>
            <ta e="T169" id="Seg_1778" s="T168">ɛː-ŋɨ</ta>
            <ta e="T170" id="Seg_1779" s="T169">pɛltɨ-ɛntɨ-m</ta>
            <ta e="T171" id="Seg_1780" s="T170">man</ta>
            <ta e="T172" id="Seg_1781" s="T171">iːja-t-ɨ-m</ta>
            <ta e="T173" id="Seg_1782" s="T172">kətɨ-sɨ-tɨ</ta>
            <ta e="T174" id="Seg_1783" s="T173">təp</ta>
            <ta e="T175" id="Seg_1784" s="T174">marat</ta>
            <ta e="T176" id="Seg_1785" s="T175">kətɨ-sɨ-tɨ</ta>
            <ta e="T177" id="Seg_1786" s="T176">ira-lıːpɨ-nɨŋ</ta>
            <ta e="T178" id="Seg_1787" s="T177">spasipo</ta>
            <ta e="T179" id="Seg_1788" s="T178">mɛntälʼ</ta>
            <ta e="T180" id="Seg_1789" s="T179">qəːlɨ-š-ntɨlʼ</ta>
            <ta e="T181" id="Seg_1790" s="T180">qum-ɨ-nɨŋ</ta>
            <ta e="T182" id="Seg_1791" s="T181">təp-qı</ta>
            <ta e="T183" id="Seg_1792" s="T182">*kurɨ-ätɔːl-sɨ-qı</ta>
            <ta e="T184" id="Seg_1793" s="T183">mənɨlʼ</ta>
            <ta e="T185" id="Seg_1794" s="T184">pɛläk-ntɨ</ta>
            <ta e="T186" id="Seg_1795" s="T185">marat</ta>
            <ta e="T187" id="Seg_1796" s="T186">qo-lʼčʼɨ-sɨ-tɨ</ta>
            <ta e="T188" id="Seg_1797" s="T187">majak</ta>
            <ta e="T189" id="Seg_1798" s="T188">ašša</ta>
            <ta e="T190" id="Seg_1799" s="T189">čʼɔːpɨ-mpɨ</ta>
            <ta e="T191" id="Seg_1800" s="T190">təp</ta>
            <ta e="T192" id="Seg_1801" s="T191">qo-lʼčʼɨ-sɨ-tɨ</ta>
            <ta e="T193" id="Seg_1802" s="T192">qaj</ta>
            <ta e="T194" id="Seg_1803" s="T193">kos</ta>
            <ta e="T195" id="Seg_1804" s="T194">*qoš-ja</ta>
            <ta e="T196" id="Seg_1805" s="T195">aj</ta>
            <ta e="T197" id="Seg_1806" s="T196">čʼatkɨ-sä</ta>
            <ta e="T198" id="Seg_1807" s="T197">*kurɨ-ätɔːl-sɨ</ta>
            <ta e="T199" id="Seg_1808" s="T198">təp</ta>
            <ta e="T200" id="Seg_1809" s="T199">čʼəːtɨ-sɨ-tɨ</ta>
            <ta e="T201" id="Seg_1810" s="T200">Аlʼenka-m</ta>
            <ta e="T202" id="Seg_1811" s="T201">paktɨ-lä-mɨt</ta>
            <ta e="T203" id="Seg_1812" s="T202">točʼčʼä</ta>
            <ta e="T204" id="Seg_1813" s="T203">čʼatkɨ-sä</ta>
            <ta e="T205" id="Seg_1814" s="T204">təp-qı</ta>
            <ta e="T206" id="Seg_1815" s="T205">qalɨ-ɛː-lɨ-sɨ-qı</ta>
            <ta e="T207" id="Seg_1816" s="T206">kutɨ</ta>
            <ta e="T208" id="Seg_1817" s="T207">kos</ta>
            <ta e="T209" id="Seg_1818" s="T208">paktɨ-tɨ</ta>
            <ta e="T210" id="Seg_1819" s="T209">üt-ntɨ</ta>
            <ta e="T211" id="Seg_1820" s="T210">meː</ta>
            <ta e="T212" id="Seg_1821" s="T211">ɛnɨ-lɨ-sɨ-mɨt</ta>
            <ta e="T213" id="Seg_1822" s="T212">paktɨ-qo</ta>
            <ta e="T214" id="Seg_1823" s="T213">üt-ntɨ</ta>
            <ta e="T215" id="Seg_1824" s="T214">nɨmtɨ</ta>
            <ta e="T216" id="Seg_1825" s="T215">ɛː-sɨ</ta>
            <ta e="T217" id="Seg_1826" s="T216">kočʼčʼɨ</ta>
            <ta e="T218" id="Seg_1827" s="T217">üt-qɨn-lʼ</ta>
            <ta e="T219" id="Seg_1828" s="T218">poː</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1829" s="T0">darkness-ADV.LOC</ta>
            <ta e="T2" id="Seg_1830" s="T1">be-PST.[3SG.S]</ta>
            <ta e="T3" id="Seg_1831" s="T2">Marat.[NOM]</ta>
            <ta e="T4" id="Seg_1832" s="T3">all.the.time</ta>
            <ta e="T5" id="Seg_1833" s="T4">go.down-MULO-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_1834" s="T5">sleep-ABST-ADJZ</ta>
            <ta e="T7" id="Seg_1835" s="T6">sack-ILL-OBL.3SG</ta>
            <ta e="T8" id="Seg_1836" s="T7">suddenly</ta>
            <ta e="T220" id="Seg_1837" s="T8">who-ACC</ta>
            <ta e="T9" id="Seg_1838" s="T220">DEF</ta>
            <ta e="T10" id="Seg_1839" s="T9">be.heard-US-EP-3SG.O</ta>
            <ta e="T11" id="Seg_1840" s="T10">what.[NOM]</ta>
            <ta e="T12" id="Seg_1841" s="T11">DEF</ta>
            <ta e="T13" id="Seg_1842" s="T12">knock-HAB-CO.[3SG.S]</ta>
            <ta e="T14" id="Seg_1843" s="T13">(s)he.[NOM]</ta>
            <ta e="T15" id="Seg_1844" s="T14">think-CAUS-PST-3SG.O</ta>
            <ta e="T16" id="Seg_1845" s="T15">how</ta>
            <ta e="T17" id="Seg_1846" s="T16">Andrey.[NOM]</ta>
            <ta e="T18" id="Seg_1847" s="T17">tell-TR-HAB-PST-3SG.O</ta>
            <ta e="T19" id="Seg_1848" s="T18">human.being-EP-ADJZ</ta>
            <ta e="T20" id="Seg_1849" s="T19">fish-ADJZ</ta>
            <ta e="T21" id="Seg_1850" s="T20">about</ta>
            <ta e="T22" id="Seg_1851" s="T21">Marat.[NOM]</ta>
            <ta e="T23" id="Seg_1852" s="T22">go-INCH-CO.[3SG.S]</ta>
            <ta e="T24" id="Seg_1853" s="T23">coast-GEN</ta>
            <ta e="T25" id="Seg_1854" s="T24">top-ILL</ta>
            <ta e="T26" id="Seg_1855" s="T25">(s)he.[NOM]</ta>
            <ta e="T27" id="Seg_1856" s="T26">go-MOM-PST.[3SG.S]</ta>
            <ta e="T28" id="Seg_1857" s="T27">nearby</ta>
            <ta e="T29" id="Seg_1858" s="T28">darkness-LOC-ADJZ</ta>
            <ta e="T30" id="Seg_1859" s="T29">bank-ACC</ta>
            <ta e="T31" id="Seg_1860" s="T30">and</ta>
            <ta e="T32" id="Seg_1861" s="T31">climb-PST.[3SG.S]</ta>
            <ta e="T33" id="Seg_1862" s="T32">up</ta>
            <ta e="T34" id="Seg_1863" s="T33">road-ILL</ta>
            <ta e="T35" id="Seg_1864" s="T34">again</ta>
            <ta e="T36" id="Seg_1865" s="T35">(s)he.[NOM]</ta>
            <ta e="T37" id="Seg_1866" s="T36">be.heard-US-EP-3SG.O</ta>
            <ta e="T38" id="Seg_1867" s="T37">who.[NOM]</ta>
            <ta e="T39" id="Seg_1868" s="T38">DEF</ta>
            <ta e="T40" id="Seg_1869" s="T39">knock-HAB.[3SG.S]</ta>
            <ta e="T41" id="Seg_1870" s="T40">NEG</ta>
            <ta e="T42" id="Seg_1871" s="T41">what-EP-ACC</ta>
            <ta e="T43" id="Seg_1872" s="T42">NEG</ta>
            <ta e="T44" id="Seg_1873" s="T43">understand-CVB</ta>
            <ta e="T45" id="Seg_1874" s="T44">Marat.[NOM]</ta>
            <ta e="T46" id="Seg_1875" s="T45">stop-PFV-PST.[3SG.S]</ta>
            <ta e="T47" id="Seg_1876" s="T46">suddenly</ta>
            <ta e="T48" id="Seg_1877" s="T47">be.visible-MOM-EP-PST.[3SG.S]</ta>
            <ta e="T49" id="Seg_1878" s="T48">old.man-DIM.[NOM]</ta>
            <ta e="T50" id="Seg_1879" s="T49">hand-LOC-3SG</ta>
            <ta e="T51" id="Seg_1880" s="T50">(s)he-EP-GEN</ta>
            <ta e="T52" id="Seg_1881" s="T51">something-LOC</ta>
            <ta e="T53" id="Seg_1882" s="T52">be-PST.[3SG.S]</ta>
            <ta e="T54" id="Seg_1883" s="T53">tree.[NOM]</ta>
            <ta e="T55" id="Seg_1884" s="T54">Marat.[NOM]</ta>
            <ta e="T56" id="Seg_1885" s="T55">greet-PST.[3SG.S]</ta>
            <ta e="T57" id="Seg_1886" s="T56">(s)he.[NOM]</ta>
            <ta e="T58" id="Seg_1887" s="T57">tale-ADV.LOC</ta>
            <ta e="T59" id="Seg_1888" s="T58">know-PST-3SG.O</ta>
            <ta e="T60" id="Seg_1889" s="T59">old.man-DIM-ACC</ta>
            <ta e="T61" id="Seg_1890" s="T60">Ivan.[NOM]</ta>
            <ta e="T62" id="Seg_1891" s="T61">Pavlovich.[NOM]</ta>
            <ta e="T63" id="Seg_1892" s="T62">Lunʼ.[NOM]</ta>
            <ta e="T64" id="Seg_1893" s="T63">guard-PST.[3SG.S]</ta>
            <ta e="T65" id="Seg_1894" s="T64">night-ADV.LOC</ta>
            <ta e="T66" id="Seg_1895" s="T65">space.outside-LOC</ta>
            <ta e="T67" id="Seg_1896" s="T66">coast-GEN</ta>
            <ta e="T68" id="Seg_1897" s="T67">above-LOC</ta>
            <ta e="T69" id="Seg_1898" s="T68">(s)he.[NOM]</ta>
            <ta e="T70" id="Seg_1899" s="T69">live.[3SG.S]</ta>
            <ta e="T71" id="Seg_1900" s="T70">here</ta>
            <ta e="T72" id="Seg_1901" s="T71">always</ta>
            <ta e="T73" id="Seg_1902" s="T72">here</ta>
            <ta e="T74" id="Seg_1903" s="T73">(s)he.[NOM]</ta>
            <ta e="T75" id="Seg_1904" s="T74">also</ta>
            <ta e="T76" id="Seg_1905" s="T75">be.born-PST.[3SG.S]</ta>
            <ta e="T77" id="Seg_1906" s="T76">(s)he.[NOM]</ta>
            <ta e="T78" id="Seg_1907" s="T77">be-PST.[3SG.S]</ta>
            <ta e="T79" id="Seg_1908" s="T78">good-ADVZ</ta>
            <ta e="T80" id="Seg_1909" s="T79">fish-VBLZ-PTCP.PRS</ta>
            <ta e="T81" id="Seg_1910" s="T80">human.being.[NOM]</ta>
            <ta e="T82" id="Seg_1911" s="T81">(s)he.[NOM]</ta>
            <ta e="T83" id="Seg_1912" s="T82">know-PST-3SG.O</ta>
            <ta e="T84" id="Seg_1913" s="T83">force-INSTR</ta>
            <ta e="T85" id="Seg_1914" s="T84">much</ta>
            <ta e="T86" id="Seg_1915" s="T85">tale-ACC</ta>
            <ta e="T87" id="Seg_1916" s="T86">evening-PL-EP-ADJZ</ta>
            <ta e="T88" id="Seg_1917" s="T87">day-ADV.LOC</ta>
            <ta e="T89" id="Seg_1918" s="T88">(s)he.[NOM]</ta>
            <ta e="T90" id="Seg_1919" s="T89">like-PST-3SG.O</ta>
            <ta e="T91" id="Seg_1920" s="T90">child-PL-EP-COM</ta>
            <ta e="T92" id="Seg_1921" s="T91">sit-INF</ta>
            <ta e="T93" id="Seg_1922" s="T92">where</ta>
            <ta e="T94" id="Seg_1923" s="T93">INDEF1</ta>
            <ta e="T95" id="Seg_1924" s="T94">water.[NOM]</ta>
            <ta e="T96" id="Seg_1925" s="T95">near-LOC</ta>
            <ta e="T97" id="Seg_1926" s="T96">fire-GEN</ta>
            <ta e="T98" id="Seg_1927" s="T97">near-LOC</ta>
            <ta e="T99" id="Seg_1928" s="T98">Marat.[NOM]</ta>
            <ta e="T100" id="Seg_1929" s="T99">leave-PST.[3SG.S]</ta>
            <ta e="T101" id="Seg_1930" s="T100">Lunʼ-COM</ta>
            <ta e="T102" id="Seg_1931" s="T101">and</ta>
            <ta e="T103" id="Seg_1932" s="T102">ask-PST-3SG.O</ta>
            <ta e="T104" id="Seg_1933" s="T103">(s)he-EP-ACC</ta>
            <ta e="T105" id="Seg_1934" s="T104">leave-INF</ta>
            <ta e="T106" id="Seg_1935" s="T105">fish-VBLZ-CVB</ta>
            <ta e="T107" id="Seg_1936" s="T106">child-PL-EP-COM</ta>
            <ta e="T108" id="Seg_1937" s="T107">earlier</ta>
            <ta e="T109" id="Seg_1938" s="T108">interrog.pron.stem-ADV.LOC</ta>
            <ta e="T110" id="Seg_1939" s="T109">interrog.pron.stem</ta>
            <ta e="T111" id="Seg_1940" s="T110">DEF</ta>
            <ta e="T112" id="Seg_1941" s="T111">be-PST.[3SG.S]</ta>
            <ta e="T113" id="Seg_1942" s="T112">day.[NOM]-1SG</ta>
            <ta e="T114" id="Seg_1943" s="T113">child-PL-EP-COM</ta>
            <ta e="T115" id="Seg_1944" s="T114">run-DRV-EP-DUR-1SG.S</ta>
            <ta e="T116" id="Seg_1945" s="T115">morning-ADV.LOC</ta>
            <ta e="T117" id="Seg_1946" s="T116">sea-GEN</ta>
            <ta e="T118" id="Seg_1947" s="T117">near-LOC</ta>
            <ta e="T119" id="Seg_1948" s="T118">child-PL-EP-COM</ta>
            <ta e="T120" id="Seg_1949" s="T119">go.hunting-PST-1SG.S</ta>
            <ta e="T121" id="Seg_1950" s="T120">sea-LOC</ta>
            <ta e="T122" id="Seg_1951" s="T121">now</ta>
            <ta e="T123" id="Seg_1952" s="T122">you.PL.GEN</ta>
            <ta e="T124" id="Seg_1953" s="T123">something-LOC-3PL</ta>
            <ta e="T125" id="Seg_1954" s="T124">new</ta>
            <ta e="T126" id="Seg_1955" s="T125">business-ADJZ</ta>
            <ta e="T127" id="Seg_1956" s="T126">you.SG.NOM</ta>
            <ta e="T128" id="Seg_1957" s="T127">old</ta>
            <ta e="T129" id="Seg_1958" s="T128">business-TRL</ta>
            <ta e="T130" id="Seg_1959" s="T129">ask-IPFV-IMP.2SG.S</ta>
            <ta e="T131" id="Seg_1960" s="T130">Philip.[NOM]</ta>
            <ta e="T132" id="Seg_1961" s="T131">something-LOC</ta>
            <ta e="T133" id="Seg_1962" s="T132">(s)he.[NOM]</ta>
            <ta e="T134" id="Seg_1963" s="T133">you.SG.DAT</ta>
            <ta e="T135" id="Seg_1964" s="T134">all</ta>
            <ta e="T136" id="Seg_1965" s="T135">tell-TR-IPFV-CO-3SG.O</ta>
            <ta e="T137" id="Seg_1966" s="T136">what-EP-ADJZ</ta>
            <ta e="T138" id="Seg_1967" s="T137">Philip.[NOM]</ta>
            <ta e="T139" id="Seg_1968" s="T138">something-LOC</ta>
            <ta e="T140" id="Seg_1969" s="T139">be-CO-3SG.S</ta>
            <ta e="T141" id="Seg_1970" s="T140">such-ADJZ</ta>
            <ta e="T142" id="Seg_1971" s="T141">(s)he.[NOM]</ta>
            <ta e="T143" id="Seg_1972" s="T142">I.NOM</ta>
            <ta e="T144" id="Seg_1973" s="T143">something-1SG.LOC</ta>
            <ta e="T145" id="Seg_1974" s="T144">now</ta>
            <ta e="T146" id="Seg_1975" s="T145">(s)he.[NOM]</ta>
            <ta e="T147" id="Seg_1976" s="T146">engineer.[NOM]</ta>
            <ta e="T148" id="Seg_1977" s="T147">Leningrad-ADV.EL</ta>
            <ta e="T149" id="Seg_1978" s="T148">you.SG.NOM</ta>
            <ta e="T150" id="Seg_1979" s="T149">continously</ta>
            <ta e="T151" id="Seg_1980" s="T150">be.heard-RFL.PFV-PST-2SG.O</ta>
            <ta e="T152" id="Seg_1981" s="T151">(s)he-EP-GEN</ta>
            <ta e="T153" id="Seg_1982" s="T152">surname-ACC</ta>
            <ta e="T154" id="Seg_1983" s="T153">Klyuchin.[NOM]</ta>
            <ta e="T155" id="Seg_1984" s="T154">I.NOM</ta>
            <ta e="T156" id="Seg_1985" s="T155">be.heard-RFL.PFV-PST-1SG.O</ta>
            <ta e="T157" id="Seg_1986" s="T156">Philip</ta>
            <ta e="T158" id="Seg_1987" s="T157">force-INSTR</ta>
            <ta e="T159" id="Seg_1988" s="T158">good</ta>
            <ta e="T160" id="Seg_1989" s="T159">human.being.[NOM]</ta>
            <ta e="T161" id="Seg_1990" s="T160">(s)he.[NOM]</ta>
            <ta e="T162" id="Seg_1991" s="T161">here</ta>
            <ta e="T163" id="Seg_1992" s="T162">you.PL.GEN</ta>
            <ta e="T164" id="Seg_1993" s="T163">ship-EP-ACC</ta>
            <ta e="T165" id="Seg_1994" s="T164">give.a.look-TR-DUR-CO-3SG.O</ta>
            <ta e="T166" id="Seg_1995" s="T165">give.a.look-TR-DUR-PST.[3SG.S]</ta>
            <ta e="T167" id="Seg_1996" s="T166">say-PST-3SG.O</ta>
            <ta e="T168" id="Seg_1997" s="T167">good</ta>
            <ta e="T169" id="Seg_1998" s="T168">be-CO.[3SG.S]</ta>
            <ta e="T170" id="Seg_1999" s="T169">help-FUT-1SG.O</ta>
            <ta e="T171" id="Seg_2000" s="T170">I.NOM</ta>
            <ta e="T172" id="Seg_2001" s="T171">child-PL-EP-ACC</ta>
            <ta e="T173" id="Seg_2002" s="T172">say-PST-3SG.O</ta>
            <ta e="T174" id="Seg_2003" s="T173">(s)he.[NOM]</ta>
            <ta e="T175" id="Seg_2004" s="T174">Marat.[NOM]</ta>
            <ta e="T176" id="Seg_2005" s="T175">say-PST-3SG.O</ta>
            <ta e="T177" id="Seg_2006" s="T176">husband-DIM-ALL</ta>
            <ta e="T178" id="Seg_2007" s="T177">thanks</ta>
            <ta e="T179" id="Seg_2008" s="T178">old</ta>
            <ta e="T180" id="Seg_2009" s="T179">fish-VBLZ-PTCP.PRS</ta>
            <ta e="T181" id="Seg_2010" s="T180">human.being-EP-ALL</ta>
            <ta e="T182" id="Seg_2011" s="T181">(s)he-DU.[NOM]</ta>
            <ta e="T183" id="Seg_2012" s="T182">go-MOM-PST-3DU.S</ta>
            <ta e="T184" id="Seg_2013" s="T183">new</ta>
            <ta e="T185" id="Seg_2014" s="T184">side-ILL</ta>
            <ta e="T186" id="Seg_2015" s="T185">Marat.[NOM]</ta>
            <ta e="T187" id="Seg_2016" s="T186">sight-PFV-PST-3SG.O</ta>
            <ta e="T188" id="Seg_2017" s="T187">sea.light.[NOM]</ta>
            <ta e="T189" id="Seg_2018" s="T188">NEG</ta>
            <ta e="T190" id="Seg_2019" s="T189">burn-HAB.[3SG.S]</ta>
            <ta e="T191" id="Seg_2020" s="T190">(s)he.[NOM]</ta>
            <ta e="T192" id="Seg_2021" s="T191">sight-PFV-PST-3SG.O</ta>
            <ta e="T193" id="Seg_2022" s="T192">what.[NOM]</ta>
            <ta e="T194" id="Seg_2023" s="T193">DEF</ta>
            <ta e="T195" id="Seg_2024" s="T194">bad-DIM</ta>
            <ta e="T196" id="Seg_2025" s="T195">and</ta>
            <ta e="T197" id="Seg_2026" s="T196">speed-INSTR</ta>
            <ta e="T198" id="Seg_2027" s="T197">go-MOM-PST.[3SG.S]</ta>
            <ta e="T199" id="Seg_2028" s="T198">(s)he.[NOM]</ta>
            <ta e="T200" id="Seg_2029" s="T199">meet-PST-3SG.O</ta>
            <ta e="T201" id="Seg_2030" s="T200">Aljonka-ACC</ta>
            <ta e="T202" id="Seg_2031" s="T201">run-OPT-1PL</ta>
            <ta e="T203" id="Seg_2032" s="T202">there</ta>
            <ta e="T204" id="Seg_2033" s="T203">speed-INSTR</ta>
            <ta e="T205" id="Seg_2034" s="T204">(s)he-DU.[NOM]</ta>
            <ta e="T206" id="Seg_2035" s="T205">fall.behind-PFV-RES-PST-3DU.S</ta>
            <ta e="T207" id="Seg_2036" s="T206">who.[NOM]</ta>
            <ta e="T208" id="Seg_2037" s="T207">DEF</ta>
            <ta e="T209" id="Seg_2038" s="T208">jump-3SG.O</ta>
            <ta e="T210" id="Seg_2039" s="T209">water-ILL</ta>
            <ta e="T211" id="Seg_2040" s="T210">we.PL.NOM</ta>
            <ta e="T212" id="Seg_2041" s="T211">be.afraid-RES-PST-1PL</ta>
            <ta e="T213" id="Seg_2042" s="T212">jump-INF</ta>
            <ta e="T214" id="Seg_2043" s="T213">water-ILL</ta>
            <ta e="T215" id="Seg_2044" s="T214">there</ta>
            <ta e="T216" id="Seg_2045" s="T215">be-PST.[3SG.S]</ta>
            <ta e="T217" id="Seg_2046" s="T216">much</ta>
            <ta e="T218" id="Seg_2047" s="T217">water-LOC-ADJZ</ta>
            <ta e="T219" id="Seg_2048" s="T218">tree.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2049" s="T0">темнота-ADV.LOC</ta>
            <ta e="T2" id="Seg_2050" s="T1">быть-PST.[3SG.S]</ta>
            <ta e="T3" id="Seg_2051" s="T2">Марат.[NOM]</ta>
            <ta e="T4" id="Seg_2052" s="T3">всё.время</ta>
            <ta e="T5" id="Seg_2053" s="T4">залезть-MULO-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_2054" s="T5">спать-ABST-ADJZ</ta>
            <ta e="T7" id="Seg_2055" s="T6">мешок-ILL-OBL.3SG</ta>
            <ta e="T8" id="Seg_2056" s="T7">вдруг</ta>
            <ta e="T220" id="Seg_2057" s="T8">кто-ACC</ta>
            <ta e="T9" id="Seg_2058" s="T220">DEF</ta>
            <ta e="T10" id="Seg_2059" s="T9">слышаться-US-EP-3SG.O</ta>
            <ta e="T11" id="Seg_2060" s="T10">что.[NOM]</ta>
            <ta e="T12" id="Seg_2061" s="T11">DEF</ta>
            <ta e="T13" id="Seg_2062" s="T12">стучать-HAB-CO.[3SG.S]</ta>
            <ta e="T14" id="Seg_2063" s="T13">он(а).[NOM]</ta>
            <ta e="T15" id="Seg_2064" s="T14">думать-CAUS-PST-3SG.O</ta>
            <ta e="T16" id="Seg_2065" s="T15">как</ta>
            <ta e="T17" id="Seg_2066" s="T16">Андрей.[NOM]</ta>
            <ta e="T18" id="Seg_2067" s="T17">сказать-TR-HAB-PST-3SG.O</ta>
            <ta e="T19" id="Seg_2068" s="T18">человек-EP-ADJZ</ta>
            <ta e="T20" id="Seg_2069" s="T19">рыба-ADJZ</ta>
            <ta e="T21" id="Seg_2070" s="T20">о</ta>
            <ta e="T22" id="Seg_2071" s="T21">Марат.[NOM]</ta>
            <ta e="T23" id="Seg_2072" s="T22">идти-INCH-CO.[3SG.S]</ta>
            <ta e="T24" id="Seg_2073" s="T23">берег-GEN</ta>
            <ta e="T25" id="Seg_2074" s="T24">верхняя.часть-ILL</ta>
            <ta e="T26" id="Seg_2075" s="T25">он(а).[NOM]</ta>
            <ta e="T27" id="Seg_2076" s="T26">идти-MOM-PST.[3SG.S]</ta>
            <ta e="T28" id="Seg_2077" s="T27">рядом</ta>
            <ta e="T29" id="Seg_2078" s="T28">темнота-LOC-ADJZ</ta>
            <ta e="T30" id="Seg_2079" s="T29">берег-ACC</ta>
            <ta e="T31" id="Seg_2080" s="T30">и</ta>
            <ta e="T32" id="Seg_2081" s="T31">взбираться-PST.[3SG.S]</ta>
            <ta e="T33" id="Seg_2082" s="T32">вверх</ta>
            <ta e="T34" id="Seg_2083" s="T33">дорога-ILL</ta>
            <ta e="T35" id="Seg_2084" s="T34">опять</ta>
            <ta e="T36" id="Seg_2085" s="T35">он(а).[NOM]</ta>
            <ta e="T37" id="Seg_2086" s="T36">слышаться-US-EP-3SG.O</ta>
            <ta e="T38" id="Seg_2087" s="T37">кто.[NOM]</ta>
            <ta e="T39" id="Seg_2088" s="T38">DEF</ta>
            <ta e="T40" id="Seg_2089" s="T39">стучать-HAB.[3SG.S]</ta>
            <ta e="T41" id="Seg_2090" s="T40">NEG</ta>
            <ta e="T42" id="Seg_2091" s="T41">что-EP-ACC</ta>
            <ta e="T43" id="Seg_2092" s="T42">NEG</ta>
            <ta e="T44" id="Seg_2093" s="T43">понимать-CVB</ta>
            <ta e="T45" id="Seg_2094" s="T44">Марат.[NOM]</ta>
            <ta e="T46" id="Seg_2095" s="T45">остановиться-PFV-PST.[3SG.S]</ta>
            <ta e="T47" id="Seg_2096" s="T46">вдруг</ta>
            <ta e="T48" id="Seg_2097" s="T47">виднеться-MOM-EP-PST.[3SG.S]</ta>
            <ta e="T49" id="Seg_2098" s="T48">старик-DIM.[NOM]</ta>
            <ta e="T50" id="Seg_2099" s="T49">рука-LOC-3SG</ta>
            <ta e="T51" id="Seg_2100" s="T50">он(а)-EP-GEN</ta>
            <ta e="T52" id="Seg_2101" s="T51">нечто-LOC</ta>
            <ta e="T53" id="Seg_2102" s="T52">быть-PST.[3SG.S]</ta>
            <ta e="T54" id="Seg_2103" s="T53">дерево.[NOM]</ta>
            <ta e="T55" id="Seg_2104" s="T54">Марат.[NOM]</ta>
            <ta e="T56" id="Seg_2105" s="T55">здороваться-PST.[3SG.S]</ta>
            <ta e="T57" id="Seg_2106" s="T56">он(а).[NOM]</ta>
            <ta e="T58" id="Seg_2107" s="T57">сказка-ADV.LOC</ta>
            <ta e="T59" id="Seg_2108" s="T58">знать-PST-3SG.O</ta>
            <ta e="T60" id="Seg_2109" s="T59">старик-DIM-ACC</ta>
            <ta e="T61" id="Seg_2110" s="T60">Иван.[NOM]</ta>
            <ta e="T62" id="Seg_2111" s="T61">Павлович.[NOM]</ta>
            <ta e="T63" id="Seg_2112" s="T62">Лунь.[NOM]</ta>
            <ta e="T64" id="Seg_2113" s="T63">дежурить-PST.[3SG.S]</ta>
            <ta e="T65" id="Seg_2114" s="T64">ночь-ADV.LOC</ta>
            <ta e="T66" id="Seg_2115" s="T65">пространство.снаружи-LOC</ta>
            <ta e="T67" id="Seg_2116" s="T66">берег-GEN</ta>
            <ta e="T68" id="Seg_2117" s="T67">над-LOC</ta>
            <ta e="T69" id="Seg_2118" s="T68">он(а).[NOM]</ta>
            <ta e="T70" id="Seg_2119" s="T69">жить.[3SG.S]</ta>
            <ta e="T71" id="Seg_2120" s="T70">здесь</ta>
            <ta e="T72" id="Seg_2121" s="T71">всегда</ta>
            <ta e="T73" id="Seg_2122" s="T72">здесь</ta>
            <ta e="T74" id="Seg_2123" s="T73">он(а).[NOM]</ta>
            <ta e="T75" id="Seg_2124" s="T74">тоже</ta>
            <ta e="T76" id="Seg_2125" s="T75">родиться-PST.[3SG.S]</ta>
            <ta e="T77" id="Seg_2126" s="T76">он(а).[NOM]</ta>
            <ta e="T78" id="Seg_2127" s="T77">быть-PST.[3SG.S]</ta>
            <ta e="T79" id="Seg_2128" s="T78">хороший-ADVZ</ta>
            <ta e="T80" id="Seg_2129" s="T79">рыба-VBLZ-PTCP.PRS</ta>
            <ta e="T81" id="Seg_2130" s="T80">человек.[NOM]</ta>
            <ta e="T82" id="Seg_2131" s="T81">он(а).[NOM]</ta>
            <ta e="T83" id="Seg_2132" s="T82">знать-PST-3SG.O</ta>
            <ta e="T84" id="Seg_2133" s="T83">сила-INSTR</ta>
            <ta e="T85" id="Seg_2134" s="T84">много</ta>
            <ta e="T86" id="Seg_2135" s="T85">сказка-ACC</ta>
            <ta e="T87" id="Seg_2136" s="T86">вечер-PL-EP-ADJZ</ta>
            <ta e="T88" id="Seg_2137" s="T87">день-ADV.LOC</ta>
            <ta e="T89" id="Seg_2138" s="T88">он(а).[NOM]</ta>
            <ta e="T90" id="Seg_2139" s="T89">любить-PST-3SG.O</ta>
            <ta e="T91" id="Seg_2140" s="T90">ребенок-PL-EP-COM</ta>
            <ta e="T92" id="Seg_2141" s="T91">сидеть-INF</ta>
            <ta e="T93" id="Seg_2142" s="T92">где</ta>
            <ta e="T94" id="Seg_2143" s="T93">INDEF1</ta>
            <ta e="T95" id="Seg_2144" s="T94">вода.[NOM]</ta>
            <ta e="T96" id="Seg_2145" s="T95">рядом-LOC</ta>
            <ta e="T97" id="Seg_2146" s="T96">огонь-GEN</ta>
            <ta e="T98" id="Seg_2147" s="T97">рядом-LOC</ta>
            <ta e="T99" id="Seg_2148" s="T98">Марат.[NOM]</ta>
            <ta e="T100" id="Seg_2149" s="T99">отправиться-PST.[3SG.S]</ta>
            <ta e="T101" id="Seg_2150" s="T100">Лунь-COM</ta>
            <ta e="T102" id="Seg_2151" s="T101">и</ta>
            <ta e="T103" id="Seg_2152" s="T102">просить-PST-3SG.O</ta>
            <ta e="T104" id="Seg_2153" s="T103">он(а)-EP-ACC</ta>
            <ta e="T105" id="Seg_2154" s="T104">отправиться-INF</ta>
            <ta e="T106" id="Seg_2155" s="T105">рыба-VBLZ-CVB</ta>
            <ta e="T107" id="Seg_2156" s="T106">ребенок-PL-EP-COM</ta>
            <ta e="T108" id="Seg_2157" s="T107">раньше</ta>
            <ta e="T109" id="Seg_2158" s="T108">interrog.pron.stem-ADV.LOC</ta>
            <ta e="T110" id="Seg_2159" s="T109">interrog.pron.stem</ta>
            <ta e="T111" id="Seg_2160" s="T110">DEF</ta>
            <ta e="T112" id="Seg_2161" s="T111">быть-PST.[3SG.S]</ta>
            <ta e="T113" id="Seg_2162" s="T112">день.[NOM]-1SG</ta>
            <ta e="T114" id="Seg_2163" s="T113">ребенок-PL-EP-COM</ta>
            <ta e="T115" id="Seg_2164" s="T114">бегать-DRV-EP-DUR-1SG.S</ta>
            <ta e="T116" id="Seg_2165" s="T115">утро-ADV.LOC</ta>
            <ta e="T117" id="Seg_2166" s="T116">море-GEN</ta>
            <ta e="T118" id="Seg_2167" s="T117">рядом-LOC</ta>
            <ta e="T119" id="Seg_2168" s="T118">ребенок-PL-EP-COM</ta>
            <ta e="T120" id="Seg_2169" s="T119">отправиться.на.охоту-PST-1SG.S</ta>
            <ta e="T121" id="Seg_2170" s="T120">море-LOC</ta>
            <ta e="T122" id="Seg_2171" s="T121">теперь</ta>
            <ta e="T123" id="Seg_2172" s="T122">вы.PL.GEN</ta>
            <ta e="T124" id="Seg_2173" s="T123">нечто-LOC-3PL</ta>
            <ta e="T125" id="Seg_2174" s="T124">новый</ta>
            <ta e="T126" id="Seg_2175" s="T125">дело-ADJZ</ta>
            <ta e="T127" id="Seg_2176" s="T126">ты.NOM</ta>
            <ta e="T128" id="Seg_2177" s="T127">старый</ta>
            <ta e="T129" id="Seg_2178" s="T128">дело-TRL</ta>
            <ta e="T130" id="Seg_2179" s="T129">спрашивать-IPFV-IMP.2SG.S</ta>
            <ta e="T131" id="Seg_2180" s="T130">Филипп.[NOM]</ta>
            <ta e="T132" id="Seg_2181" s="T131">нечто-LOC</ta>
            <ta e="T133" id="Seg_2182" s="T132">он(а).[NOM]</ta>
            <ta e="T134" id="Seg_2183" s="T133">ты.DAT</ta>
            <ta e="T135" id="Seg_2184" s="T134">всё</ta>
            <ta e="T136" id="Seg_2185" s="T135">сказать-TR-IPFV-CO-3SG.O</ta>
            <ta e="T137" id="Seg_2186" s="T136">что-EP-ADJZ</ta>
            <ta e="T138" id="Seg_2187" s="T137">Филипп.[NOM]</ta>
            <ta e="T139" id="Seg_2188" s="T138">нечто-LOC</ta>
            <ta e="T140" id="Seg_2189" s="T139">быть-CO-3SG.S</ta>
            <ta e="T141" id="Seg_2190" s="T140">такой-ADJZ</ta>
            <ta e="T142" id="Seg_2191" s="T141">он(а).[NOM]</ta>
            <ta e="T143" id="Seg_2192" s="T142">я.NOM</ta>
            <ta e="T144" id="Seg_2193" s="T143">нечто-1SG.LOC</ta>
            <ta e="T145" id="Seg_2194" s="T144">теперь</ta>
            <ta e="T146" id="Seg_2195" s="T145">он(а).[NOM]</ta>
            <ta e="T147" id="Seg_2196" s="T146">инженер.[NOM]</ta>
            <ta e="T148" id="Seg_2197" s="T147">Ленинград-ADV.EL</ta>
            <ta e="T149" id="Seg_2198" s="T148">ты.NOM</ta>
            <ta e="T150" id="Seg_2199" s="T149">непрерывно</ta>
            <ta e="T151" id="Seg_2200" s="T150">слышаться-RFL.PFV-PST-2SG.O</ta>
            <ta e="T152" id="Seg_2201" s="T151">он(а)-EP-GEN</ta>
            <ta e="T153" id="Seg_2202" s="T152">фамилия-ACC</ta>
            <ta e="T154" id="Seg_2203" s="T153">Ключин.[NOM]</ta>
            <ta e="T155" id="Seg_2204" s="T154">я.NOM</ta>
            <ta e="T156" id="Seg_2205" s="T155">слышаться-RFL.PFV-PST-1SG.O</ta>
            <ta e="T157" id="Seg_2206" s="T156">Филипп</ta>
            <ta e="T158" id="Seg_2207" s="T157">сила-INSTR</ta>
            <ta e="T159" id="Seg_2208" s="T158">хороший</ta>
            <ta e="T160" id="Seg_2209" s="T159">человек.[NOM]</ta>
            <ta e="T161" id="Seg_2210" s="T160">он(а).[NOM]</ta>
            <ta e="T162" id="Seg_2211" s="T161">здесь</ta>
            <ta e="T163" id="Seg_2212" s="T162">вы.PL.GEN</ta>
            <ta e="T164" id="Seg_2213" s="T163">корабль-EP-ACC</ta>
            <ta e="T165" id="Seg_2214" s="T164">взглянуть-TR-DUR-CO-3SG.O</ta>
            <ta e="T166" id="Seg_2215" s="T165">взглянуть-TR-DUR-PST.[3SG.S]</ta>
            <ta e="T167" id="Seg_2216" s="T166">сказать-PST-3SG.O</ta>
            <ta e="T168" id="Seg_2217" s="T167">хороший</ta>
            <ta e="T169" id="Seg_2218" s="T168">быть-CO.[3SG.S]</ta>
            <ta e="T170" id="Seg_2219" s="T169">помочь-FUT-1SG.O</ta>
            <ta e="T171" id="Seg_2220" s="T170">я.NOM</ta>
            <ta e="T172" id="Seg_2221" s="T171">ребенок-PL-EP-ACC</ta>
            <ta e="T173" id="Seg_2222" s="T172">сказать-PST-3SG.O</ta>
            <ta e="T174" id="Seg_2223" s="T173">он(а).[NOM]</ta>
            <ta e="T175" id="Seg_2224" s="T174">Марат.[NOM]</ta>
            <ta e="T176" id="Seg_2225" s="T175">сказать-PST-3SG.O</ta>
            <ta e="T177" id="Seg_2226" s="T176">муж-DIM-ALL</ta>
            <ta e="T178" id="Seg_2227" s="T177">спасибо</ta>
            <ta e="T179" id="Seg_2228" s="T178">старый</ta>
            <ta e="T180" id="Seg_2229" s="T179">рыба-VBLZ-PTCP.PRS</ta>
            <ta e="T181" id="Seg_2230" s="T180">человек-EP-ALL</ta>
            <ta e="T182" id="Seg_2231" s="T181">он(а)-DU.[NOM]</ta>
            <ta e="T183" id="Seg_2232" s="T182">идти-MOM-PST-3DU.S</ta>
            <ta e="T184" id="Seg_2233" s="T183">новый</ta>
            <ta e="T185" id="Seg_2234" s="T184">сторона-ILL</ta>
            <ta e="T186" id="Seg_2235" s="T185">Марат.[NOM]</ta>
            <ta e="T187" id="Seg_2236" s="T186">видеть-PFV-PST-3SG.O</ta>
            <ta e="T188" id="Seg_2237" s="T187">маяк.[NOM]</ta>
            <ta e="T189" id="Seg_2238" s="T188">NEG</ta>
            <ta e="T190" id="Seg_2239" s="T189">гореть-HAB.[3SG.S]</ta>
            <ta e="T191" id="Seg_2240" s="T190">он(а).[NOM]</ta>
            <ta e="T192" id="Seg_2241" s="T191">видеть-PFV-PST-3SG.O</ta>
            <ta e="T193" id="Seg_2242" s="T192">что.[NOM]</ta>
            <ta e="T194" id="Seg_2243" s="T193">DEF</ta>
            <ta e="T195" id="Seg_2244" s="T194">плохой-DIM</ta>
            <ta e="T196" id="Seg_2245" s="T195">и</ta>
            <ta e="T197" id="Seg_2246" s="T196">скорость-INSTR</ta>
            <ta e="T198" id="Seg_2247" s="T197">идти-MOM-PST.[3SG.S]</ta>
            <ta e="T199" id="Seg_2248" s="T198">он(а).[NOM]</ta>
            <ta e="T200" id="Seg_2249" s="T199">встретить-PST-3SG.O</ta>
            <ta e="T201" id="Seg_2250" s="T200">Алёнка-ACC</ta>
            <ta e="T202" id="Seg_2251" s="T201">побежать-OPT-1PL</ta>
            <ta e="T203" id="Seg_2252" s="T202">туда</ta>
            <ta e="T204" id="Seg_2253" s="T203">скорость-INSTR</ta>
            <ta e="T205" id="Seg_2254" s="T204">он(а)-DU.[NOM]</ta>
            <ta e="T206" id="Seg_2255" s="T205">отстать-PFV-RES-PST-3DU.S</ta>
            <ta e="T207" id="Seg_2256" s="T206">кто.[NOM]</ta>
            <ta e="T208" id="Seg_2257" s="T207">DEF</ta>
            <ta e="T209" id="Seg_2258" s="T208">прыгнуть-3SG.O</ta>
            <ta e="T210" id="Seg_2259" s="T209">вода-ILL</ta>
            <ta e="T211" id="Seg_2260" s="T210">мы.PL.NOM</ta>
            <ta e="T212" id="Seg_2261" s="T211">бояться-RES-PST-1PL</ta>
            <ta e="T213" id="Seg_2262" s="T212">прыгнуть-INF</ta>
            <ta e="T214" id="Seg_2263" s="T213">вода-ILL</ta>
            <ta e="T215" id="Seg_2264" s="T214">там</ta>
            <ta e="T216" id="Seg_2265" s="T215">быть-PST.[3SG.S]</ta>
            <ta e="T217" id="Seg_2266" s="T216">много</ta>
            <ta e="T218" id="Seg_2267" s="T217">вода-LOC-ADJZ</ta>
            <ta e="T219" id="Seg_2268" s="T218">дерево.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2269" s="T0">n-n&gt;adv</ta>
            <ta e="T2" id="Seg_2270" s="T1">v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_2271" s="T2">nprop-n:case3</ta>
            <ta e="T4" id="Seg_2272" s="T3">adv</ta>
            <ta e="T5" id="Seg_2273" s="T4">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_2274" s="T5">v-v&gt;n-n&gt;adj</ta>
            <ta e="T7" id="Seg_2275" s="T6">n-n:case2-n:obl.poss</ta>
            <ta e="T8" id="Seg_2276" s="T7">adv</ta>
            <ta e="T220" id="Seg_2277" s="T8">interrog-n:case3</ta>
            <ta e="T9" id="Seg_2278" s="T220">clit</ta>
            <ta e="T10" id="Seg_2279" s="T9">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T11" id="Seg_2280" s="T10">interrog-n:case3</ta>
            <ta e="T12" id="Seg_2281" s="T11">clit</ta>
            <ta e="T13" id="Seg_2282" s="T12">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T14" id="Seg_2283" s="T13">pers-n:case3</ta>
            <ta e="T15" id="Seg_2284" s="T14">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_2285" s="T15">conj</ta>
            <ta e="T17" id="Seg_2286" s="T16">nprop-n:case3</ta>
            <ta e="T18" id="Seg_2287" s="T17">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_2288" s="T18">n-n:(ins)-n&gt;adj</ta>
            <ta e="T20" id="Seg_2289" s="T19">n-n&gt;adj</ta>
            <ta e="T21" id="Seg_2290" s="T20">pp</ta>
            <ta e="T22" id="Seg_2291" s="T21">nprop-n:case3</ta>
            <ta e="T23" id="Seg_2292" s="T22">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T24" id="Seg_2293" s="T23">n-n:case3</ta>
            <ta e="T25" id="Seg_2294" s="T24">n-n:case3</ta>
            <ta e="T26" id="Seg_2295" s="T25">pers-n:case3</ta>
            <ta e="T27" id="Seg_2296" s="T26">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_2297" s="T27">adv</ta>
            <ta e="T29" id="Seg_2298" s="T28">n-n:case2-n&gt;adj</ta>
            <ta e="T30" id="Seg_2299" s="T29">n-n:case3</ta>
            <ta e="T31" id="Seg_2300" s="T30">conj</ta>
            <ta e="T32" id="Seg_2301" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_2302" s="T32">adv</ta>
            <ta e="T34" id="Seg_2303" s="T33">n-n:case3</ta>
            <ta e="T35" id="Seg_2304" s="T34">adv</ta>
            <ta e="T36" id="Seg_2305" s="T35">pers-n:case3</ta>
            <ta e="T37" id="Seg_2306" s="T36">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T38" id="Seg_2307" s="T37">interrog-n:case3</ta>
            <ta e="T39" id="Seg_2308" s="T38">clit</ta>
            <ta e="T40" id="Seg_2309" s="T39">v-v&gt;v-v:pn</ta>
            <ta e="T41" id="Seg_2310" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_2311" s="T41">interrog-n:(ins)-n:case3</ta>
            <ta e="T43" id="Seg_2312" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_2313" s="T43">v-v&gt;adv</ta>
            <ta e="T45" id="Seg_2314" s="T44">nprop-n:case3</ta>
            <ta e="T46" id="Seg_2315" s="T45">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_2316" s="T46">adv</ta>
            <ta e="T48" id="Seg_2317" s="T47">v-v&gt;v-v:ins-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_2318" s="T48">n-n&gt;n-n:case3</ta>
            <ta e="T50" id="Seg_2319" s="T49">n-n:case2-n:poss</ta>
            <ta e="T51" id="Seg_2320" s="T50">pers-n:(ins)-n:case3</ta>
            <ta e="T52" id="Seg_2321" s="T51">n-n:case3</ta>
            <ta e="T53" id="Seg_2322" s="T52">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_2323" s="T53">n-n:case3</ta>
            <ta e="T55" id="Seg_2324" s="T54">nprop-n:case3</ta>
            <ta e="T56" id="Seg_2325" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_2326" s="T56">pers-n:case3</ta>
            <ta e="T58" id="Seg_2327" s="T57">n-n&gt;adv</ta>
            <ta e="T59" id="Seg_2328" s="T58">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_2329" s="T59">n-n&gt;n-n:case3</ta>
            <ta e="T61" id="Seg_2330" s="T60">nprop-n:case3</ta>
            <ta e="T62" id="Seg_2331" s="T61">nprop-n:case3</ta>
            <ta e="T63" id="Seg_2332" s="T62">nprop-n:case3</ta>
            <ta e="T64" id="Seg_2333" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_2334" s="T64">n-n&gt;adv</ta>
            <ta e="T66" id="Seg_2335" s="T65">n-n:case3</ta>
            <ta e="T67" id="Seg_2336" s="T66">n-n:case3</ta>
            <ta e="T68" id="Seg_2337" s="T67">pp-n:case3</ta>
            <ta e="T69" id="Seg_2338" s="T68">pers-n:case3</ta>
            <ta e="T70" id="Seg_2339" s="T69">v-v:pn</ta>
            <ta e="T71" id="Seg_2340" s="T70">adv</ta>
            <ta e="T72" id="Seg_2341" s="T71">adv</ta>
            <ta e="T73" id="Seg_2342" s="T72">adv</ta>
            <ta e="T74" id="Seg_2343" s="T73">pers-n:case3</ta>
            <ta e="T75" id="Seg_2344" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_2345" s="T75">v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_2346" s="T76">pers-n:case3</ta>
            <ta e="T78" id="Seg_2347" s="T77">v-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_2348" s="T78">adj-adj&gt;adv</ta>
            <ta e="T80" id="Seg_2349" s="T79">n-n&gt;v-v&gt;ptcp</ta>
            <ta e="T81" id="Seg_2350" s="T80">n-n:case3</ta>
            <ta e="T82" id="Seg_2351" s="T81">pers-n:case3</ta>
            <ta e="T83" id="Seg_2352" s="T82">v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_2353" s="T83">n-n:case3</ta>
            <ta e="T85" id="Seg_2354" s="T84">quant</ta>
            <ta e="T86" id="Seg_2355" s="T85">n-n:case3</ta>
            <ta e="T87" id="Seg_2356" s="T86">n-n:num-n:(ins)-n&gt;adj</ta>
            <ta e="T88" id="Seg_2357" s="T87">n-n&gt;adv</ta>
            <ta e="T89" id="Seg_2358" s="T88">pers-n:case3</ta>
            <ta e="T90" id="Seg_2359" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_2360" s="T90">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T92" id="Seg_2361" s="T91">v-v:inf</ta>
            <ta e="T93" id="Seg_2362" s="T92">interrog</ta>
            <ta e="T94" id="Seg_2363" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_2364" s="T94">n-n:case3</ta>
            <ta e="T96" id="Seg_2365" s="T95">pp-n:case3</ta>
            <ta e="T97" id="Seg_2366" s="T96">n-n:case3</ta>
            <ta e="T98" id="Seg_2367" s="T97">pp-n:case3</ta>
            <ta e="T99" id="Seg_2368" s="T98">nprop-n:case3</ta>
            <ta e="T100" id="Seg_2369" s="T99">v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_2370" s="T100">nprop-n:case3</ta>
            <ta e="T102" id="Seg_2371" s="T101">conj</ta>
            <ta e="T103" id="Seg_2372" s="T102">v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_2373" s="T103">pers-n:(ins)-n:case3</ta>
            <ta e="T105" id="Seg_2374" s="T104">v-v:inf</ta>
            <ta e="T106" id="Seg_2375" s="T105">n-n&gt;v-v&gt;adv</ta>
            <ta e="T107" id="Seg_2376" s="T106">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T108" id="Seg_2377" s="T107">adv</ta>
            <ta e="T109" id="Seg_2378" s="T108">interrog-n&gt;adv</ta>
            <ta e="T110" id="Seg_2379" s="T109">interrog</ta>
            <ta e="T111" id="Seg_2380" s="T110">clit</ta>
            <ta e="T112" id="Seg_2381" s="T111">v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_2382" s="T112">n-n:case1-n:poss</ta>
            <ta e="T114" id="Seg_2383" s="T113">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T115" id="Seg_2384" s="T114">v-v&gt;v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T116" id="Seg_2385" s="T115">n-n&gt;adv</ta>
            <ta e="T117" id="Seg_2386" s="T116">n-n:case3</ta>
            <ta e="T118" id="Seg_2387" s="T117">pp-n:case3</ta>
            <ta e="T119" id="Seg_2388" s="T118">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T120" id="Seg_2389" s="T119">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_2390" s="T120">n-n:case3</ta>
            <ta e="T122" id="Seg_2391" s="T121">adv</ta>
            <ta e="T123" id="Seg_2392" s="T122">pers</ta>
            <ta e="T124" id="Seg_2393" s="T123">n-n:case2-n:poss</ta>
            <ta e="T125" id="Seg_2394" s="T124">adj</ta>
            <ta e="T126" id="Seg_2395" s="T125">n-n&gt;adj</ta>
            <ta e="T127" id="Seg_2396" s="T126">pers</ta>
            <ta e="T128" id="Seg_2397" s="T127">adj</ta>
            <ta e="T129" id="Seg_2398" s="T128">n-n:case3</ta>
            <ta e="T130" id="Seg_2399" s="T129">v-v&gt;v-v:mood-pn</ta>
            <ta e="T131" id="Seg_2400" s="T130">nprop-n:case3</ta>
            <ta e="T132" id="Seg_2401" s="T131">n-n:case3</ta>
            <ta e="T133" id="Seg_2402" s="T132">pers-n:case3</ta>
            <ta e="T134" id="Seg_2403" s="T133">pers</ta>
            <ta e="T135" id="Seg_2404" s="T134">quant</ta>
            <ta e="T136" id="Seg_2405" s="T135">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T137" id="Seg_2406" s="T136">interrog-n:(ins)-n&gt;adj</ta>
            <ta e="T138" id="Seg_2407" s="T137">nprop-n:case3</ta>
            <ta e="T139" id="Seg_2408" s="T138">n-n:case3</ta>
            <ta e="T140" id="Seg_2409" s="T139">v-v:ins-v:pn</ta>
            <ta e="T141" id="Seg_2410" s="T140">dem-adj&gt;adj</ta>
            <ta e="T142" id="Seg_2411" s="T141">pers-n:case3</ta>
            <ta e="T143" id="Seg_2412" s="T142">pers</ta>
            <ta e="T144" id="Seg_2413" s="T143">n-n:poss-case</ta>
            <ta e="T145" id="Seg_2414" s="T144">adv</ta>
            <ta e="T146" id="Seg_2415" s="T145">pers-n:case3</ta>
            <ta e="T147" id="Seg_2416" s="T146">n-n:case3</ta>
            <ta e="T148" id="Seg_2417" s="T147">nprop-n&gt;adv</ta>
            <ta e="T149" id="Seg_2418" s="T148">pers</ta>
            <ta e="T150" id="Seg_2419" s="T149">adv</ta>
            <ta e="T151" id="Seg_2420" s="T150">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T152" id="Seg_2421" s="T151">pers-n:(ins)-n:case3</ta>
            <ta e="T153" id="Seg_2422" s="T152">n-n:case3</ta>
            <ta e="T154" id="Seg_2423" s="T153">nprop-n:case3</ta>
            <ta e="T155" id="Seg_2424" s="T154">pers</ta>
            <ta e="T156" id="Seg_2425" s="T155">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T157" id="Seg_2426" s="T156">nprop</ta>
            <ta e="T158" id="Seg_2427" s="T157">n-n:case3</ta>
            <ta e="T159" id="Seg_2428" s="T158">adj</ta>
            <ta e="T160" id="Seg_2429" s="T159">n-n:case3</ta>
            <ta e="T161" id="Seg_2430" s="T160">pers-n:case3</ta>
            <ta e="T162" id="Seg_2431" s="T161">adv</ta>
            <ta e="T163" id="Seg_2432" s="T162">pers</ta>
            <ta e="T164" id="Seg_2433" s="T163">n-n:(ins)-n:case3</ta>
            <ta e="T165" id="Seg_2434" s="T164">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T166" id="Seg_2435" s="T165">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T167" id="Seg_2436" s="T166">v-v:tense-v:pn</ta>
            <ta e="T168" id="Seg_2437" s="T167">adj</ta>
            <ta e="T169" id="Seg_2438" s="T168">v-v:ins-v:pn</ta>
            <ta e="T170" id="Seg_2439" s="T169">v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_2440" s="T170">pers</ta>
            <ta e="T172" id="Seg_2441" s="T171">n-n:num-n:(ins)-n:case3</ta>
            <ta e="T173" id="Seg_2442" s="T172">v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_2443" s="T173">pers-n:case3</ta>
            <ta e="T175" id="Seg_2444" s="T174">nprop-n:case3</ta>
            <ta e="T176" id="Seg_2445" s="T175">v-v:tense-v:pn</ta>
            <ta e="T177" id="Seg_2446" s="T176">n-n&gt;n-n:case3</ta>
            <ta e="T178" id="Seg_2447" s="T177">ptcl</ta>
            <ta e="T179" id="Seg_2448" s="T178">adj</ta>
            <ta e="T180" id="Seg_2449" s="T179">n-n&gt;v-v&gt;ptcp</ta>
            <ta e="T181" id="Seg_2450" s="T180">n-n:(ins)-n:case3</ta>
            <ta e="T182" id="Seg_2451" s="T181">pers-n:num-n:case3</ta>
            <ta e="T183" id="Seg_2452" s="T182">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T184" id="Seg_2453" s="T183">adj</ta>
            <ta e="T185" id="Seg_2454" s="T184">n-n:case3</ta>
            <ta e="T186" id="Seg_2455" s="T185">nprop-n:case3</ta>
            <ta e="T187" id="Seg_2456" s="T186">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T188" id="Seg_2457" s="T187">n-n:case3</ta>
            <ta e="T189" id="Seg_2458" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_2459" s="T189">v-v&gt;v-v:pn</ta>
            <ta e="T191" id="Seg_2460" s="T190">pers-n:case3</ta>
            <ta e="T192" id="Seg_2461" s="T191">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T193" id="Seg_2462" s="T192">interrog-n:case3</ta>
            <ta e="T194" id="Seg_2463" s="T193">clit</ta>
            <ta e="T195" id="Seg_2464" s="T194">adj-adj&gt;adj</ta>
            <ta e="T196" id="Seg_2465" s="T195">conj</ta>
            <ta e="T197" id="Seg_2466" s="T196">n-n:case3</ta>
            <ta e="T198" id="Seg_2467" s="T197">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T199" id="Seg_2468" s="T198">pers-n:case3</ta>
            <ta e="T200" id="Seg_2469" s="T199">v-v:tense-v:pn</ta>
            <ta e="T201" id="Seg_2470" s="T200">nprop-n:case3</ta>
            <ta e="T202" id="Seg_2471" s="T201">v-v:mood-v:pn</ta>
            <ta e="T203" id="Seg_2472" s="T202">adv</ta>
            <ta e="T204" id="Seg_2473" s="T203">n-n:case3</ta>
            <ta e="T205" id="Seg_2474" s="T204">pers-n:num-n:case3</ta>
            <ta e="T206" id="Seg_2475" s="T205">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T207" id="Seg_2476" s="T206">interrog-n:case3</ta>
            <ta e="T208" id="Seg_2477" s="T207">clit</ta>
            <ta e="T209" id="Seg_2478" s="T208">v-v:pn</ta>
            <ta e="T210" id="Seg_2479" s="T209">n-n:case3</ta>
            <ta e="T211" id="Seg_2480" s="T210">pers</ta>
            <ta e="T212" id="Seg_2481" s="T211">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T213" id="Seg_2482" s="T212">v-v:inf</ta>
            <ta e="T214" id="Seg_2483" s="T213">n-n:case3</ta>
            <ta e="T215" id="Seg_2484" s="T214">adv</ta>
            <ta e="T216" id="Seg_2485" s="T215">v-v:tense-v:pn</ta>
            <ta e="T217" id="Seg_2486" s="T216">quant</ta>
            <ta e="T218" id="Seg_2487" s="T217">n-n:case2-n&gt;adj</ta>
            <ta e="T219" id="Seg_2488" s="T218">n-n:case3</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2489" s="T0">adv</ta>
            <ta e="T2" id="Seg_2490" s="T1">n</ta>
            <ta e="T3" id="Seg_2491" s="T2">nprop</ta>
            <ta e="T4" id="Seg_2492" s="T3">adv</ta>
            <ta e="T5" id="Seg_2493" s="T4">v</ta>
            <ta e="T6" id="Seg_2494" s="T5">adj</ta>
            <ta e="T7" id="Seg_2495" s="T6">n</ta>
            <ta e="T8" id="Seg_2496" s="T7">adv</ta>
            <ta e="T220" id="Seg_2497" s="T8">interrog</ta>
            <ta e="T9" id="Seg_2498" s="T220">clit</ta>
            <ta e="T10" id="Seg_2499" s="T9">v</ta>
            <ta e="T11" id="Seg_2500" s="T10">interrog</ta>
            <ta e="T12" id="Seg_2501" s="T11">clit</ta>
            <ta e="T13" id="Seg_2502" s="T12">v</ta>
            <ta e="T14" id="Seg_2503" s="T13">pers</ta>
            <ta e="T15" id="Seg_2504" s="T14">v</ta>
            <ta e="T16" id="Seg_2505" s="T15">conj</ta>
            <ta e="T17" id="Seg_2506" s="T16">nprop</ta>
            <ta e="T18" id="Seg_2507" s="T17">v</ta>
            <ta e="T19" id="Seg_2508" s="T18">adj</ta>
            <ta e="T20" id="Seg_2509" s="T19">adj</ta>
            <ta e="T21" id="Seg_2510" s="T20">pp</ta>
            <ta e="T22" id="Seg_2511" s="T21">nprop</ta>
            <ta e="T23" id="Seg_2512" s="T22">v</ta>
            <ta e="T24" id="Seg_2513" s="T23">n</ta>
            <ta e="T25" id="Seg_2514" s="T24">n</ta>
            <ta e="T26" id="Seg_2515" s="T25">pers</ta>
            <ta e="T27" id="Seg_2516" s="T26">v</ta>
            <ta e="T28" id="Seg_2517" s="T27">adv</ta>
            <ta e="T29" id="Seg_2518" s="T28">adj</ta>
            <ta e="T30" id="Seg_2519" s="T29">n</ta>
            <ta e="T31" id="Seg_2520" s="T30">conj</ta>
            <ta e="T32" id="Seg_2521" s="T31">v</ta>
            <ta e="T33" id="Seg_2522" s="T32">adv</ta>
            <ta e="T34" id="Seg_2523" s="T33">n</ta>
            <ta e="T35" id="Seg_2524" s="T34">adv</ta>
            <ta e="T36" id="Seg_2525" s="T35">pers</ta>
            <ta e="T37" id="Seg_2526" s="T36">v</ta>
            <ta e="T38" id="Seg_2527" s="T37">interrog</ta>
            <ta e="T39" id="Seg_2528" s="T38">clit</ta>
            <ta e="T40" id="Seg_2529" s="T39">v</ta>
            <ta e="T41" id="Seg_2530" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_2531" s="T41">interrog</ta>
            <ta e="T43" id="Seg_2532" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_2533" s="T43">adv</ta>
            <ta e="T45" id="Seg_2534" s="T44">nprop</ta>
            <ta e="T46" id="Seg_2535" s="T45">v</ta>
            <ta e="T47" id="Seg_2536" s="T46">adv</ta>
            <ta e="T48" id="Seg_2537" s="T47">v</ta>
            <ta e="T49" id="Seg_2538" s="T48">n</ta>
            <ta e="T50" id="Seg_2539" s="T49">n</ta>
            <ta e="T51" id="Seg_2540" s="T50">pers</ta>
            <ta e="T52" id="Seg_2541" s="T51">n</ta>
            <ta e="T53" id="Seg_2542" s="T52">n</ta>
            <ta e="T54" id="Seg_2543" s="T53">n</ta>
            <ta e="T55" id="Seg_2544" s="T54">nprop</ta>
            <ta e="T56" id="Seg_2545" s="T55">v</ta>
            <ta e="T57" id="Seg_2546" s="T56">pers</ta>
            <ta e="T58" id="Seg_2547" s="T57">adv</ta>
            <ta e="T59" id="Seg_2548" s="T58">v</ta>
            <ta e="T60" id="Seg_2549" s="T59">n</ta>
            <ta e="T61" id="Seg_2550" s="T60">nprop</ta>
            <ta e="T62" id="Seg_2551" s="T61">nprop</ta>
            <ta e="T63" id="Seg_2552" s="T62">nprop</ta>
            <ta e="T64" id="Seg_2553" s="T63">v</ta>
            <ta e="T65" id="Seg_2554" s="T64">adv</ta>
            <ta e="T66" id="Seg_2555" s="T65">n</ta>
            <ta e="T67" id="Seg_2556" s="T66">n</ta>
            <ta e="T68" id="Seg_2557" s="T67">pp</ta>
            <ta e="T69" id="Seg_2558" s="T68">pers</ta>
            <ta e="T70" id="Seg_2559" s="T69">v</ta>
            <ta e="T71" id="Seg_2560" s="T70">adv</ta>
            <ta e="T72" id="Seg_2561" s="T71">adv</ta>
            <ta e="T73" id="Seg_2562" s="T72">adv</ta>
            <ta e="T74" id="Seg_2563" s="T73">pers</ta>
            <ta e="T75" id="Seg_2564" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_2565" s="T75">v</ta>
            <ta e="T77" id="Seg_2566" s="T76">pers</ta>
            <ta e="T78" id="Seg_2567" s="T77">n</ta>
            <ta e="T79" id="Seg_2568" s="T78">adv</ta>
            <ta e="T80" id="Seg_2569" s="T79">ptcp</ta>
            <ta e="T81" id="Seg_2570" s="T80">n</ta>
            <ta e="T82" id="Seg_2571" s="T81">pers</ta>
            <ta e="T83" id="Seg_2572" s="T82">v</ta>
            <ta e="T84" id="Seg_2573" s="T83">n</ta>
            <ta e="T85" id="Seg_2574" s="T84">quant</ta>
            <ta e="T86" id="Seg_2575" s="T85">n</ta>
            <ta e="T87" id="Seg_2576" s="T86">adj</ta>
            <ta e="T88" id="Seg_2577" s="T87">adv</ta>
            <ta e="T89" id="Seg_2578" s="T88">pers</ta>
            <ta e="T90" id="Seg_2579" s="T89">v</ta>
            <ta e="T91" id="Seg_2580" s="T90">n</ta>
            <ta e="T92" id="Seg_2581" s="T91">v</ta>
            <ta e="T93" id="Seg_2582" s="T92">interrog</ta>
            <ta e="T94" id="Seg_2583" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_2584" s="T94">n</ta>
            <ta e="T96" id="Seg_2585" s="T95">pp</ta>
            <ta e="T97" id="Seg_2586" s="T96">n</ta>
            <ta e="T98" id="Seg_2587" s="T97">pp</ta>
            <ta e="T99" id="Seg_2588" s="T98">nprop</ta>
            <ta e="T100" id="Seg_2589" s="T99">v</ta>
            <ta e="T101" id="Seg_2590" s="T100">nprop</ta>
            <ta e="T102" id="Seg_2591" s="T101">conj</ta>
            <ta e="T103" id="Seg_2592" s="T102">v</ta>
            <ta e="T104" id="Seg_2593" s="T103">pers</ta>
            <ta e="T105" id="Seg_2594" s="T104">v</ta>
            <ta e="T106" id="Seg_2595" s="T105">adv</ta>
            <ta e="T107" id="Seg_2596" s="T106">n</ta>
            <ta e="T108" id="Seg_2597" s="T107">adv</ta>
            <ta e="T109" id="Seg_2598" s="T108">adv</ta>
            <ta e="T110" id="Seg_2599" s="T109">interrog</ta>
            <ta e="T111" id="Seg_2600" s="T110">clit</ta>
            <ta e="T112" id="Seg_2601" s="T111">n</ta>
            <ta e="T113" id="Seg_2602" s="T112">n</ta>
            <ta e="T114" id="Seg_2603" s="T113">n</ta>
            <ta e="T115" id="Seg_2604" s="T114">v</ta>
            <ta e="T116" id="Seg_2605" s="T115">adv</ta>
            <ta e="T117" id="Seg_2606" s="T116">n</ta>
            <ta e="T118" id="Seg_2607" s="T117">pp</ta>
            <ta e="T119" id="Seg_2608" s="T118">n</ta>
            <ta e="T120" id="Seg_2609" s="T119">v</ta>
            <ta e="T121" id="Seg_2610" s="T120">n</ta>
            <ta e="T122" id="Seg_2611" s="T121">adv</ta>
            <ta e="T123" id="Seg_2612" s="T122">pers</ta>
            <ta e="T124" id="Seg_2613" s="T123">n</ta>
            <ta e="T125" id="Seg_2614" s="T124">adj</ta>
            <ta e="T126" id="Seg_2615" s="T125">adj</ta>
            <ta e="T127" id="Seg_2616" s="T126">pers</ta>
            <ta e="T128" id="Seg_2617" s="T127">adj</ta>
            <ta e="T129" id="Seg_2618" s="T128">n</ta>
            <ta e="T130" id="Seg_2619" s="T129">v</ta>
            <ta e="T131" id="Seg_2620" s="T130">nprop</ta>
            <ta e="T132" id="Seg_2621" s="T131">n</ta>
            <ta e="T133" id="Seg_2622" s="T132">pers</ta>
            <ta e="T134" id="Seg_2623" s="T133">pers</ta>
            <ta e="T135" id="Seg_2624" s="T134">quant</ta>
            <ta e="T136" id="Seg_2625" s="T135">v</ta>
            <ta e="T137" id="Seg_2626" s="T136">adj</ta>
            <ta e="T138" id="Seg_2627" s="T137">nprop</ta>
            <ta e="T139" id="Seg_2628" s="T138">n</ta>
            <ta e="T140" id="Seg_2629" s="T139">v</ta>
            <ta e="T141" id="Seg_2630" s="T140">adj</ta>
            <ta e="T142" id="Seg_2631" s="T141">pers</ta>
            <ta e="T143" id="Seg_2632" s="T142">pers</ta>
            <ta e="T144" id="Seg_2633" s="T143">n</ta>
            <ta e="T145" id="Seg_2634" s="T144">adv</ta>
            <ta e="T146" id="Seg_2635" s="T145">pers</ta>
            <ta e="T147" id="Seg_2636" s="T146">n</ta>
            <ta e="T148" id="Seg_2637" s="T147">nprop</ta>
            <ta e="T149" id="Seg_2638" s="T148">pers</ta>
            <ta e="T150" id="Seg_2639" s="T149">adv</ta>
            <ta e="T151" id="Seg_2640" s="T150">v</ta>
            <ta e="T152" id="Seg_2641" s="T151">pers</ta>
            <ta e="T153" id="Seg_2642" s="T152">n</ta>
            <ta e="T154" id="Seg_2643" s="T153">nprop</ta>
            <ta e="T155" id="Seg_2644" s="T154">pers</ta>
            <ta e="T156" id="Seg_2645" s="T155">v</ta>
            <ta e="T157" id="Seg_2646" s="T156">nprop</ta>
            <ta e="T158" id="Seg_2647" s="T157">n</ta>
            <ta e="T159" id="Seg_2648" s="T158">adj</ta>
            <ta e="T160" id="Seg_2649" s="T159">n</ta>
            <ta e="T161" id="Seg_2650" s="T160">pers</ta>
            <ta e="T162" id="Seg_2651" s="T161">adv</ta>
            <ta e="T163" id="Seg_2652" s="T162">pers</ta>
            <ta e="T164" id="Seg_2653" s="T163">n</ta>
            <ta e="T165" id="Seg_2654" s="T164">v</ta>
            <ta e="T166" id="Seg_2655" s="T165">v</ta>
            <ta e="T167" id="Seg_2656" s="T166">v</ta>
            <ta e="T168" id="Seg_2657" s="T167">adj</ta>
            <ta e="T169" id="Seg_2658" s="T168">v</ta>
            <ta e="T170" id="Seg_2659" s="T169">v</ta>
            <ta e="T171" id="Seg_2660" s="T170">pers</ta>
            <ta e="T172" id="Seg_2661" s="T171">n</ta>
            <ta e="T173" id="Seg_2662" s="T172">v</ta>
            <ta e="T174" id="Seg_2663" s="T173">pers</ta>
            <ta e="T175" id="Seg_2664" s="T174">nprop</ta>
            <ta e="T176" id="Seg_2665" s="T175">v</ta>
            <ta e="T177" id="Seg_2666" s="T176">n</ta>
            <ta e="T178" id="Seg_2667" s="T177">ptcl</ta>
            <ta e="T179" id="Seg_2668" s="T178">adj</ta>
            <ta e="T180" id="Seg_2669" s="T179">ptcp</ta>
            <ta e="T181" id="Seg_2670" s="T180">n</ta>
            <ta e="T182" id="Seg_2671" s="T181">pers</ta>
            <ta e="T183" id="Seg_2672" s="T182">v</ta>
            <ta e="T184" id="Seg_2673" s="T183">adj</ta>
            <ta e="T185" id="Seg_2674" s="T184">n</ta>
            <ta e="T186" id="Seg_2675" s="T185">nprop</ta>
            <ta e="T187" id="Seg_2676" s="T186">v</ta>
            <ta e="T188" id="Seg_2677" s="T187">n</ta>
            <ta e="T189" id="Seg_2678" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_2679" s="T189">v</ta>
            <ta e="T191" id="Seg_2680" s="T190">pers</ta>
            <ta e="T192" id="Seg_2681" s="T191">v</ta>
            <ta e="T193" id="Seg_2682" s="T192">interrog</ta>
            <ta e="T194" id="Seg_2683" s="T193">clit</ta>
            <ta e="T195" id="Seg_2684" s="T194">adj</ta>
            <ta e="T196" id="Seg_2685" s="T195">conj</ta>
            <ta e="T197" id="Seg_2686" s="T196">n</ta>
            <ta e="T198" id="Seg_2687" s="T197">v</ta>
            <ta e="T199" id="Seg_2688" s="T198">pers</ta>
            <ta e="T200" id="Seg_2689" s="T199">v</ta>
            <ta e="T201" id="Seg_2690" s="T200">nprop</ta>
            <ta e="T202" id="Seg_2691" s="T201">v</ta>
            <ta e="T203" id="Seg_2692" s="T202">adv</ta>
            <ta e="T204" id="Seg_2693" s="T203">n</ta>
            <ta e="T205" id="Seg_2694" s="T204">pers</ta>
            <ta e="T206" id="Seg_2695" s="T205">v</ta>
            <ta e="T207" id="Seg_2696" s="T206">interrog</ta>
            <ta e="T208" id="Seg_2697" s="T207">clit</ta>
            <ta e="T209" id="Seg_2698" s="T208">v</ta>
            <ta e="T210" id="Seg_2699" s="T209">n</ta>
            <ta e="T211" id="Seg_2700" s="T210">pers</ta>
            <ta e="T212" id="Seg_2701" s="T211">v</ta>
            <ta e="T213" id="Seg_2702" s="T212">v</ta>
            <ta e="T214" id="Seg_2703" s="T213">n</ta>
            <ta e="T215" id="Seg_2704" s="T214">adv</ta>
            <ta e="T216" id="Seg_2705" s="T215">v</ta>
            <ta e="T217" id="Seg_2706" s="T216">quant</ta>
            <ta e="T218" id="Seg_2707" s="T217">adj</ta>
            <ta e="T219" id="Seg_2708" s="T218">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_2709" s="T0">adv:Time</ta>
            <ta e="T2" id="Seg_2710" s="T1">0.3:Th</ta>
            <ta e="T3" id="Seg_2711" s="T2">np.h:A</ta>
            <ta e="T7" id="Seg_2712" s="T6">np:G</ta>
            <ta e="T8" id="Seg_2713" s="T7">adv:Time</ta>
            <ta e="T10" id="Seg_2714" s="T9">0.3.h:E</ta>
            <ta e="T14" id="Seg_2715" s="T13">pro.h:E</ta>
            <ta e="T22" id="Seg_2716" s="T21">pro.h:A</ta>
            <ta e="T24" id="Seg_2717" s="T23">pp:G</ta>
            <ta e="T26" id="Seg_2718" s="T25">pro.h:A</ta>
            <ta e="T30" id="Seg_2719" s="T29">pp:Path</ta>
            <ta e="T32" id="Seg_2720" s="T31">0.3.h:A</ta>
            <ta e="T33" id="Seg_2721" s="T32">adv:G</ta>
            <ta e="T34" id="Seg_2722" s="T33">np:G</ta>
            <ta e="T36" id="Seg_2723" s="T35">pro.h:E</ta>
            <ta e="T45" id="Seg_2724" s="T44">np.h:A</ta>
            <ta e="T49" id="Seg_2725" s="T48">np.h:Th</ta>
            <ta e="T50" id="Seg_2726" s="T49">np:L</ta>
            <ta e="T51" id="Seg_2727" s="T50">pp:Poss</ta>
            <ta e="T54" id="Seg_2728" s="T53">np:Th</ta>
            <ta e="T55" id="Seg_2729" s="T54">np.h:A</ta>
            <ta e="T57" id="Seg_2730" s="T56">pro.h:E</ta>
            <ta e="T60" id="Seg_2731" s="T59">np.h:Th</ta>
            <ta e="T63" id="Seg_2732" s="T62">np.h:A</ta>
            <ta e="T65" id="Seg_2733" s="T64">adv:Time</ta>
            <ta e="T66" id="Seg_2734" s="T65">np:L</ta>
            <ta e="T67" id="Seg_2735" s="T66">pp:L</ta>
            <ta e="T69" id="Seg_2736" s="T68">pro.h:Th</ta>
            <ta e="T71" id="Seg_2737" s="T70">adv:L</ta>
            <ta e="T72" id="Seg_2738" s="T71">adv:Time</ta>
            <ta e="T73" id="Seg_2739" s="T72">adv:L</ta>
            <ta e="T74" id="Seg_2740" s="T73">pro.h:P</ta>
            <ta e="T77" id="Seg_2741" s="T76">pro.h:Th</ta>
            <ta e="T82" id="Seg_2742" s="T81">pro.h:E</ta>
            <ta e="T86" id="Seg_2743" s="T85">np:Th</ta>
            <ta e="T89" id="Seg_2744" s="T88">pro.h:E</ta>
            <ta e="T99" id="Seg_2745" s="T98">pro.h:A</ta>
            <ta e="T101" id="Seg_2746" s="T100">np:Com</ta>
            <ta e="T103" id="Seg_2747" s="T102">0.3.h:A</ta>
            <ta e="T104" id="Seg_2748" s="T103">pro.h:S</ta>
            <ta e="T108" id="Seg_2749" s="T107">adv:Time</ta>
            <ta e="T109" id="Seg_2750" s="T108">adv:Time</ta>
            <ta e="T113" id="Seg_2751" s="T112">0.1.h:Poss np:Th</ta>
            <ta e="T114" id="Seg_2752" s="T113">np:Com</ta>
            <ta e="T115" id="Seg_2753" s="T114">0.1.h:A</ta>
            <ta e="T116" id="Seg_2754" s="T115">adv:Time</ta>
            <ta e="T117" id="Seg_2755" s="T116">pp:L</ta>
            <ta e="T119" id="Seg_2756" s="T118">np:Com</ta>
            <ta e="T120" id="Seg_2757" s="T119">0.1.h:A</ta>
            <ta e="T121" id="Seg_2758" s="T120">np:L</ta>
            <ta e="T122" id="Seg_2759" s="T121">adv:Time</ta>
            <ta e="T123" id="Seg_2760" s="T122">pp:Poss</ta>
            <ta e="T126" id="Seg_2761" s="T125">np:Th</ta>
            <ta e="T127" id="Seg_2762" s="T126">pro.h:A</ta>
            <ta e="T129" id="Seg_2763" s="T128">np:Th</ta>
            <ta e="T131" id="Seg_2764" s="T130">pp:S</ta>
            <ta e="T133" id="Seg_2765" s="T132">pro.h:A</ta>
            <ta e="T134" id="Seg_2766" s="T133">np.h:R</ta>
            <ta e="T135" id="Seg_2767" s="T134">pro:Th</ta>
            <ta e="T138" id="Seg_2768" s="T137">pp:S</ta>
            <ta e="T141" id="Seg_2769" s="T140">pro.h:Th</ta>
            <ta e="T142" id="Seg_2770" s="T141">pro.h:Th</ta>
            <ta e="T145" id="Seg_2771" s="T144">adv:Time</ta>
            <ta e="T146" id="Seg_2772" s="T145">pro.h:Th</ta>
            <ta e="T148" id="Seg_2773" s="T147">np:S</ta>
            <ta e="T149" id="Seg_2774" s="T148">pro.h:E</ta>
            <ta e="T152" id="Seg_2775" s="T151">pro.h:Poss</ta>
            <ta e="T153" id="Seg_2776" s="T152">np:Th</ta>
            <ta e="T155" id="Seg_2777" s="T154">pro.h:E</ta>
            <ta e="T156" id="Seg_2778" s="T155">0.3:Th</ta>
            <ta e="T157" id="Seg_2779" s="T156">np.h:Th</ta>
            <ta e="T161" id="Seg_2780" s="T160">pro.h:A</ta>
            <ta e="T162" id="Seg_2781" s="T161">adv:L</ta>
            <ta e="T164" id="Seg_2782" s="T163">np:Th</ta>
            <ta e="T167" id="Seg_2783" s="T166">0.3.h:A</ta>
            <ta e="T171" id="Seg_2784" s="T170">pro.h:A</ta>
            <ta e="T172" id="Seg_2785" s="T171">np.h:B</ta>
            <ta e="T174" id="Seg_2786" s="T173">pro.h:A</ta>
            <ta e="T175" id="Seg_2787" s="T174">np.h:A</ta>
            <ta e="T177" id="Seg_2788" s="T176">np.h:R</ta>
            <ta e="T181" id="Seg_2789" s="T180">np.h:R</ta>
            <ta e="T182" id="Seg_2790" s="T181">pro.h:A</ta>
            <ta e="T185" id="Seg_2791" s="T184">np:G</ta>
            <ta e="T186" id="Seg_2792" s="T185">np.h:E</ta>
            <ta e="T191" id="Seg_2793" s="T190">pro.h:E</ta>
            <ta e="T195" id="Seg_2794" s="T194">np:Th</ta>
            <ta e="T198" id="Seg_2795" s="T197">0.3.h:A</ta>
            <ta e="T199" id="Seg_2796" s="T198">pro.h:A</ta>
            <ta e="T201" id="Seg_2797" s="T200">np.h:Th</ta>
            <ta e="T202" id="Seg_2798" s="T201">0.1.h:A</ta>
            <ta e="T203" id="Seg_2799" s="T202">adv:G</ta>
            <ta e="T205" id="Seg_2800" s="T204">pro.h:Th</ta>
            <ta e="T207" id="Seg_2801" s="T206">np.h:A</ta>
            <ta e="T210" id="Seg_2802" s="T209">np:G</ta>
            <ta e="T211" id="Seg_2803" s="T210">pro.h:E</ta>
            <ta e="T215" id="Seg_2804" s="T214">adv:L</ta>
            <ta e="T219" id="Seg_2805" s="T218">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_2806" s="T1">0.3:S v:pred</ta>
            <ta e="T3" id="Seg_2807" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_2808" s="T4">v:pred</ta>
            <ta e="T10" id="Seg_2809" s="T9">0.3.h:S v:pred</ta>
            <ta e="T13" id="Seg_2810" s="T10">s:compl</ta>
            <ta e="T14" id="Seg_2811" s="T13">pro.h:S</ta>
            <ta e="T15" id="Seg_2812" s="T14">v:pred</ta>
            <ta e="T21" id="Seg_2813" s="T15">s:compl</ta>
            <ta e="T22" id="Seg_2814" s="T21">np.h:S</ta>
            <ta e="T23" id="Seg_2815" s="T22">v:pred</ta>
            <ta e="T26" id="Seg_2816" s="T25">pro.h:S</ta>
            <ta e="T27" id="Seg_2817" s="T26">v:pred</ta>
            <ta e="T32" id="Seg_2818" s="T31">0.3.h:S v:pred</ta>
            <ta e="T36" id="Seg_2819" s="T35">pro.h:S</ta>
            <ta e="T37" id="Seg_2820" s="T36">v:pred</ta>
            <ta e="T40" id="Seg_2821" s="T37">s:compl</ta>
            <ta e="T44" id="Seg_2822" s="T40">s:adv</ta>
            <ta e="T45" id="Seg_2823" s="T44">np.h:S</ta>
            <ta e="T46" id="Seg_2824" s="T45">v:pred</ta>
            <ta e="T48" id="Seg_2825" s="T47">v:pred</ta>
            <ta e="T49" id="Seg_2826" s="T48">np.h:S</ta>
            <ta e="T53" id="Seg_2827" s="T52">v:pred</ta>
            <ta e="T54" id="Seg_2828" s="T53">np:S</ta>
            <ta e="T55" id="Seg_2829" s="T54">np.h:S</ta>
            <ta e="T56" id="Seg_2830" s="T55">v:pred</ta>
            <ta e="T57" id="Seg_2831" s="T56">pro.h:S</ta>
            <ta e="T59" id="Seg_2832" s="T58">v:pred</ta>
            <ta e="T60" id="Seg_2833" s="T59">np.h:O</ta>
            <ta e="T63" id="Seg_2834" s="T62">np.h:S</ta>
            <ta e="T64" id="Seg_2835" s="T63">v:pred</ta>
            <ta e="T69" id="Seg_2836" s="T68">pro.h:S</ta>
            <ta e="T70" id="Seg_2837" s="T69">v:pred</ta>
            <ta e="T74" id="Seg_2838" s="T73">pro.h:S</ta>
            <ta e="T76" id="Seg_2839" s="T75">v:pred</ta>
            <ta e="T77" id="Seg_2840" s="T76">pro.h:S</ta>
            <ta e="T78" id="Seg_2841" s="T77">cop</ta>
            <ta e="T80" id="Seg_2842" s="T78">s:rel</ta>
            <ta e="T81" id="Seg_2843" s="T80">n:pred</ta>
            <ta e="T82" id="Seg_2844" s="T81">pro.h:S</ta>
            <ta e="T83" id="Seg_2845" s="T82">v:pred</ta>
            <ta e="T86" id="Seg_2846" s="T85">np:O</ta>
            <ta e="T89" id="Seg_2847" s="T88">pro.h:S</ta>
            <ta e="T90" id="Seg_2848" s="T89">v:pred</ta>
            <ta e="T98" id="Seg_2849" s="T90">s:compl</ta>
            <ta e="T99" id="Seg_2850" s="T98">pro.h:S</ta>
            <ta e="T100" id="Seg_2851" s="T99">v:pred</ta>
            <ta e="T103" id="Seg_2852" s="T102">0.3.h:S v:pred</ta>
            <ta e="T104" id="Seg_2853" s="T103">pro.h:O</ta>
            <ta e="T107" id="Seg_2854" s="T104">s:compl</ta>
            <ta e="T112" id="Seg_2855" s="T111">v:pred</ta>
            <ta e="T113" id="Seg_2856" s="T112">np:S</ta>
            <ta e="T115" id="Seg_2857" s="T114">0.1.h:S v:pred</ta>
            <ta e="T120" id="Seg_2858" s="T119">0.1.h:S v:pred</ta>
            <ta e="T126" id="Seg_2859" s="T125">np:S</ta>
            <ta e="T127" id="Seg_2860" s="T126">pro.h:S</ta>
            <ta e="T130" id="Seg_2861" s="T129">v:pred</ta>
            <ta e="T133" id="Seg_2862" s="T132">pro.h:S</ta>
            <ta e="T135" id="Seg_2863" s="T134">pro:O</ta>
            <ta e="T136" id="Seg_2864" s="T135">v:pred</ta>
            <ta e="T140" id="Seg_2865" s="T139">v:pred</ta>
            <ta e="T141" id="Seg_2866" s="T140">pro.h:S</ta>
            <ta e="T142" id="Seg_2867" s="T141">pro.h:S</ta>
            <ta e="T144" id="Seg_2868" s="T143">adj:pred</ta>
            <ta e="T146" id="Seg_2869" s="T145">pro.h:S</ta>
            <ta e="T147" id="Seg_2870" s="T146">n:pred</ta>
            <ta e="T149" id="Seg_2871" s="T148">pro.h:S</ta>
            <ta e="T151" id="Seg_2872" s="T150">v:pred</ta>
            <ta e="T153" id="Seg_2873" s="T152">np:O</ta>
            <ta e="T155" id="Seg_2874" s="T154">pro.h:S</ta>
            <ta e="T156" id="Seg_2875" s="T155">0.3:O v:pred</ta>
            <ta e="T157" id="Seg_2876" s="T156">np.h:S</ta>
            <ta e="T160" id="Seg_2877" s="T159">n:pred</ta>
            <ta e="T161" id="Seg_2878" s="T160">pro.h:S</ta>
            <ta e="T164" id="Seg_2879" s="T163">np:O</ta>
            <ta e="T165" id="Seg_2880" s="T164">v:pred</ta>
            <ta e="T167" id="Seg_2881" s="T166">0.3.h:S v:pred</ta>
            <ta e="T169" id="Seg_2882" s="T167">s:compl</ta>
            <ta e="T170" id="Seg_2883" s="T169">v:pred</ta>
            <ta e="T171" id="Seg_2884" s="T170">pro.h:S</ta>
            <ta e="T173" id="Seg_2885" s="T172">v:pred</ta>
            <ta e="T174" id="Seg_2886" s="T173">pro.h:S</ta>
            <ta e="T175" id="Seg_2887" s="T174">np.h:S</ta>
            <ta e="T176" id="Seg_2888" s="T175">v:pred</ta>
            <ta e="T178" id="Seg_2889" s="T177">s:compl</ta>
            <ta e="T180" id="Seg_2890" s="T179">s:rel</ta>
            <ta e="T182" id="Seg_2891" s="T181">pro.h:S</ta>
            <ta e="T183" id="Seg_2892" s="T182">v:pred</ta>
            <ta e="T186" id="Seg_2893" s="T185">np.h:S</ta>
            <ta e="T187" id="Seg_2894" s="T186">v:pred</ta>
            <ta e="T190" id="Seg_2895" s="T187">s:compl</ta>
            <ta e="T191" id="Seg_2896" s="T190">pro.h:S</ta>
            <ta e="T192" id="Seg_2897" s="T191">v:pred</ta>
            <ta e="T195" id="Seg_2898" s="T194">np:O</ta>
            <ta e="T198" id="Seg_2899" s="T197">0.3.h:S v:pred</ta>
            <ta e="T199" id="Seg_2900" s="T198">pro.h:S</ta>
            <ta e="T200" id="Seg_2901" s="T199">v:pred</ta>
            <ta e="T201" id="Seg_2902" s="T200">np.h:O</ta>
            <ta e="T202" id="Seg_2903" s="T201">0.1.h:S v:pred</ta>
            <ta e="T205" id="Seg_2904" s="T204">pro.h:S</ta>
            <ta e="T206" id="Seg_2905" s="T205">v:pred</ta>
            <ta e="T207" id="Seg_2906" s="T206">pro.h:S</ta>
            <ta e="T209" id="Seg_2907" s="T208">v:pred</ta>
            <ta e="T211" id="Seg_2908" s="T210">pro.h:S</ta>
            <ta e="T212" id="Seg_2909" s="T211">v:pred</ta>
            <ta e="T214" id="Seg_2910" s="T212">s:compl</ta>
            <ta e="T216" id="Seg_2911" s="T215">v:pred</ta>
            <ta e="T219" id="Seg_2912" s="T218">np:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_2913" s="T2">RUS:cult</ta>
            <ta e="T17" id="Seg_2914" s="T16">RUS:cult</ta>
            <ta e="T22" id="Seg_2915" s="T21">RUS:cult</ta>
            <ta e="T31" id="Seg_2916" s="T30">RUS:gram</ta>
            <ta e="T45" id="Seg_2917" s="T44">RUS:cult</ta>
            <ta e="T55" id="Seg_2918" s="T54">RUS:cult</ta>
            <ta e="T56" id="Seg_2919" s="T55">RUS:cult</ta>
            <ta e="T63" id="Seg_2920" s="T60">RUS:cult</ta>
            <ta e="T64" id="Seg_2921" s="T63">RUS:cult</ta>
            <ta e="T99" id="Seg_2922" s="T98">RUS:cult</ta>
            <ta e="T101" id="Seg_2923" s="T100">RUS:cult</ta>
            <ta e="T131" id="Seg_2924" s="T130">RUS:cult</ta>
            <ta e="T138" id="Seg_2925" s="T137">RUS:cult</ta>
            <ta e="T147" id="Seg_2926" s="T146">RUS:cult</ta>
            <ta e="T148" id="Seg_2927" s="T147">RUS:cult</ta>
            <ta e="T153" id="Seg_2928" s="T152">RUS:cult</ta>
            <ta e="T154" id="Seg_2929" s="T153">RUS:cult</ta>
            <ta e="T157" id="Seg_2930" s="T156">RUS:cult</ta>
            <ta e="T164" id="Seg_2931" s="T163">RUS:cult</ta>
            <ta e="T175" id="Seg_2932" s="T174">RUS:cult</ta>
            <ta e="T178" id="Seg_2933" s="T177">RUS:disc</ta>
            <ta e="T186" id="Seg_2934" s="T185">RUS:cult</ta>
            <ta e="T188" id="Seg_2935" s="T187">RUS:cult</ta>
            <ta e="T201" id="Seg_2936" s="T200">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T2" id="Seg_2937" s="T0">Это было ночью.</ta>
            <ta e="T13" id="Seg_2938" s="T2">Марат всё ещё залезал в спальный мешок, как вдруг (кого-то) услышал, что что-то стучит.</ta>
            <ta e="T21" id="Seg_2939" s="T13">Он вспомнил, как Андрей рассказывал о человеке-рыбе.</ta>
            <ta e="T25" id="Seg_2940" s="T21">Марат пошел на берег.</ta>
            <ta e="T34" id="Seg_2941" s="T25">Он прошел мимо темного берега и взобрался вверх на дорогу.</ta>
            <ta e="T40" id="Seg_2942" s="T34">Опять он услышал, что кто-то стучит.</ta>
            <ta e="T46" id="Seg_2943" s="T40">Ничего не понимая, Марат остановился.</ta>
            <ta e="T49" id="Seg_2944" s="T46">Вдруг показался старикашка.</ta>
            <ta e="T54" id="Seg_2945" s="T49">В руке у него была палка.</ta>
            <ta e="T60" id="Seg_2946" s="T54">Марат поздоровался, он давно знал старика.</ta>
            <ta e="T68" id="Seg_2947" s="T60">Иван Павлович Лунь дежурил ночью на улице, на берегу.</ta>
            <ta e="T76" id="Seg_2948" s="T68">Он жил здесь всё время, здесь он и родился.</ta>
            <ta e="T81" id="Seg_2949" s="T76">Он был хорошим рыбаком.</ta>
            <ta e="T86" id="Seg_2950" s="T81">Он знал очень много сказок.</ta>
            <ta e="T98" id="Seg_2951" s="T86">Вечерами он любил с ребятами сидеть где-нибудь у реки у костра.</ta>
            <ta e="T107" id="Seg_2952" s="T98">Марат пошёл с Лунем и попросил его поехать на рыбалку с ребятами.</ta>
            <ta e="T118" id="Seg_2953" s="T107">"Раньше когда-то было время, я бегал с ребятами по утрам около моря.</ta>
            <ta e="T121" id="Seg_2954" s="T118">С ребятами я [охотился?] в море.</ta>
            <ta e="T126" id="Seg_2955" s="T121">Теперь у вас другие дела.</ta>
            <ta e="T136" id="Seg_2956" s="T126">Ты о старых делах спроси у Филиппа, он тебе всё расскажет".</ta>
            <ta e="T139" id="Seg_2957" s="T136">"У какого Филиппа?"</ta>
            <ta e="T141" id="Seg_2958" s="T139">"Есть такой.</ta>
            <ta e="T145" id="Seg_2959" s="T141">Он у меня сейчас.</ta>
            <ta e="T148" id="Seg_2960" s="T145">Он инженер из Ленинграда.</ta>
            <ta e="T154" id="Seg_2961" s="T148">Ты часто слышал его фамилию Ключин".</ta>
            <ta e="T156" id="Seg_2962" s="T154">"Я слышал".</ta>
            <ta e="T160" id="Seg_2963" s="T156">Филипп ― очень хороший человек.</ta>
            <ta e="T166" id="Seg_2964" s="T160">Он здесь ваш корабль осматривает (/осматривал).</ta>
            <ta e="T169" id="Seg_2965" s="T166">Сказал, хороший.</ta>
            <ta e="T174" id="Seg_2966" s="T169">"Помогу я ребятам", ― сказал он".</ta>
            <ta e="T185" id="Seg_2967" s="T174">Марат сказал "спасибо" старику старому рыбаку, они пошли в разные стороны.</ta>
            <ta e="T190" id="Seg_2968" s="T185">Марат увидел, что маяк не горит.</ta>
            <ta e="T198" id="Seg_2969" s="T190">Он увидел что-то плохое и быстро пошёл.</ta>
            <ta e="T201" id="Seg_2970" s="T198">Он встретил Алёнку.</ta>
            <ta e="T204" id="Seg_2971" s="T201">"Побежим туда быстро!"</ta>
            <ta e="T206" id="Seg_2972" s="T204">Они опоздали.</ta>
            <ta e="T210" id="Seg_2973" s="T206">Кто-то прыгнул в воду.</ta>
            <ta e="T214" id="Seg_2974" s="T210">"Мы побоялись прыгнуть в воду. </ta>
            <ta e="T219" id="Seg_2975" s="T214">Там было много коряг".</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2" id="Seg_2976" s="T0">It was at night.</ta>
            <ta e="T13" id="Seg_2977" s="T2">Marat was about to crawl into the sleeping bag, as he suddenly heard someone knocking.</ta>
            <ta e="T21" id="Seg_2978" s="T13">He remembered how Andrey had told him about the human-fish.</ta>
            <ta e="T25" id="Seg_2979" s="T21">Marat went to the shore.</ta>
            <ta e="T34" id="Seg_2980" s="T25">He went along the dark shore and climbed up to the road.</ta>
            <ta e="T40" id="Seg_2981" s="T34">Again he heard someone knocking.</ta>
            <ta e="T46" id="Seg_2982" s="T40">Not understanding anything, Marat stopped.</ta>
            <ta e="T49" id="Seg_2983" s="T46">Suddenly an old man appeared.</ta>
            <ta e="T54" id="Seg_2984" s="T49">He held a stick in his hand.</ta>
            <ta e="T60" id="Seg_2985" s="T54">Marat greeted him, he knew the old man for a long time.</ta>
            <ta e="T68" id="Seg_2986" s="T60">Ivan Pavlovich Lunʼ was on duty on the shore at night.</ta>
            <ta e="T76" id="Seg_2987" s="T68">He lived here his whole life, he was born here, too.</ta>
            <ta e="T81" id="Seg_2988" s="T76">He was a good fisherman.</ta>
            <ta e="T86" id="Seg_2989" s="T81">He knew a lot of fairytales.</ta>
            <ta e="T98" id="Seg_2990" s="T86">In the evening he liked to sit together with children by the fire somewhere by the river.</ta>
            <ta e="T107" id="Seg_2991" s="T98">Marat went with Lunʼ and asked him to go fishing together with the children.</ta>
            <ta e="T118" id="Seg_2992" s="T107">"There was a time earlier, I went jogging together with the children along the sea in the morning.</ta>
            <ta e="T121" id="Seg_2993" s="T118">I went fishing to the sea with the children.</ta>
            <ta e="T126" id="Seg_2994" s="T121">Now you have other things to do.</ta>
            <ta e="T136" id="Seg_2995" s="T126">Ask Philip about the old businesses, he will tell you everything".</ta>
            <ta e="T139" id="Seg_2996" s="T136">"What Philip?"</ta>
            <ta e="T141" id="Seg_2997" s="T139">"There is one.</ta>
            <ta e="T145" id="Seg_2998" s="T141">He is at my place now.</ta>
            <ta e="T148" id="Seg_2999" s="T145">He is an engineer from Leningrad.</ta>
            <ta e="T154" id="Seg_3000" s="T148">You often heard his surname: Klyuchin".</ta>
            <ta e="T156" id="Seg_3001" s="T154">"I've heard".</ta>
            <ta e="T160" id="Seg_3002" s="T156">Philip is a very good person.</ta>
            <ta e="T166" id="Seg_3003" s="T160">He checked your ship here.</ta>
            <ta e="T169" id="Seg_3004" s="T166">He said [that] it is good.</ta>
            <ta e="T174" id="Seg_3005" s="T169">"I will help the children", ― he said".</ta>
            <ta e="T185" id="Seg_3006" s="T174">Marat said "thank you" to the old fisherman and they went in different directions.</ta>
            <ta e="T190" id="Seg_3007" s="T185">Marat saw that the lighthouse didn't shine.</ta>
            <ta e="T198" id="Seg_3008" s="T190">He saw something bad and went on.</ta>
            <ta e="T201" id="Seg_3009" s="T198">He met Alyonka.</ta>
            <ta e="T204" id="Seg_3010" s="T201">"Let's quickly run over there!"</ta>
            <ta e="T206" id="Seg_3011" s="T204">They came too late.</ta>
            <ta e="T210" id="Seg_3012" s="T206">Somebody had jumped into the water.</ta>
            <ta e="T214" id="Seg_3013" s="T210">"We were afraid to jump into the water.</ta>
            <ta e="T219" id="Seg_3014" s="T214">There were a lot of snags there".</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2" id="Seg_3015" s="T0">Es war in der Nacht.</ta>
            <ta e="T13" id="Seg_3016" s="T2">Marat war noch dabei in den Schlafsack hineinzukriechen, als er plötzlich hörte, dass jemand klopfte.</ta>
            <ta e="T21" id="Seg_3017" s="T13">Er erinnerte sich, wie Andrej ihm über den Fischmenschen erzählt hatte.</ta>
            <ta e="T25" id="Seg_3018" s="T21">Marat lief zum Ufer.</ta>
            <ta e="T34" id="Seg_3019" s="T25">Er ging am dunklen Ufer entlang und kletterte hoch auf die Straße.</ta>
            <ta e="T40" id="Seg_3020" s="T34">Er hat wieder jemanden klopfen gehört.</ta>
            <ta e="T46" id="Seg_3021" s="T40">Ohne etwas zu verstehen, blieb Marat stehen.</ta>
            <ta e="T49" id="Seg_3022" s="T46">Plötzlich erschien ein alter Mann.</ta>
            <ta e="T54" id="Seg_3023" s="T49">Er hielt einen Stock in seiner Hand.</ta>
            <ta e="T60" id="Seg_3024" s="T54">Marat begrüßte ihn, er kannte den Alten seit langem.</ta>
            <ta e="T68" id="Seg_3025" s="T60">Ivan Pavlovich Lunʼ hatte Dienst auf dem Ufer in der Nacht.</ta>
            <ta e="T76" id="Seg_3026" s="T68">Er lebte sein ganzes Leben lang hier, er wurde auch hier geboren.</ta>
            <ta e="T81" id="Seg_3027" s="T76">Er war ein guter Fischer.</ta>
            <ta e="T86" id="Seg_3028" s="T81">Er kannte viele Märchen.</ta>
            <ta e="T98" id="Seg_3029" s="T86">Abends mochte er mit Kindern am Feuer irgendwo am Fluss sitzen.</ta>
            <ta e="T107" id="Seg_3030" s="T98">Marat ging mit Lunʼ und bat ihn, zusammen mit den Kindern fischen zu gehen.</ta>
            <ta e="T118" id="Seg_3031" s="T107">"Es gab Zeiten früher, ich bin mit den Kindern morgens entlang des Meeres laufen gegangen.</ta>
            <ta e="T121" id="Seg_3032" s="T118">Ich bin mit den Kindern aufs Meer hinaus zum Fischen gefahren.</ta>
            <ta e="T126" id="Seg_3033" s="T121">Jetzt habt ihr andere Beschäftigungen.</ta>
            <ta e="T136" id="Seg_3034" s="T126">Frag' Fillip über die alten Beschäftigungen, er wird dir alles erzählen".</ta>
            <ta e="T139" id="Seg_3035" s="T136">"Welchen Filip?"</ta>
            <ta e="T141" id="Seg_3036" s="T139">"Es gibt so einen.</ta>
            <ta e="T145" id="Seg_3037" s="T141">Er ist jetzt bei mir.</ta>
            <ta e="T148" id="Seg_3038" s="T145">Er ist Ingenieur aus Leningrad.</ta>
            <ta e="T154" id="Seg_3039" s="T148">Du hast seinen Nachnamen oft gehört: Kljutschin".</ta>
            <ta e="T156" id="Seg_3040" s="T154">"Habe ich gehört".</ta>
            <ta e="T160" id="Seg_3041" s="T156">Filip ist ein sehr guter Mensch.</ta>
            <ta e="T166" id="Seg_3042" s="T160">Er überprüfte euer Schiff hier.</ta>
            <ta e="T169" id="Seg_3043" s="T166">Er sagte, [dass] es gut ist.</ta>
            <ta e="T174" id="Seg_3044" s="T169">"Ich werde den Kindern helfen", ― sagte er".</ta>
            <ta e="T185" id="Seg_3045" s="T174">Marat sagte dem alten Fischer "danke" und sie gingen in verschiedene Richtungen.</ta>
            <ta e="T190" id="Seg_3046" s="T185">Marat sah, dass der Leuchtturm nicht leuchtete.</ta>
            <ta e="T198" id="Seg_3047" s="T190">Er sah etwas Schlechtes und ging schnell fort.</ta>
            <ta e="T201" id="Seg_3048" s="T198">Er traf Aljonka.</ta>
            <ta e="T204" id="Seg_3049" s="T201">"Lass uns schnell dahin laufen!"</ta>
            <ta e="T206" id="Seg_3050" s="T204">Sie kamen zu spät.</ta>
            <ta e="T210" id="Seg_3051" s="T206">Jemand war ins Wasser gesprungen.</ta>
            <ta e="T214" id="Seg_3052" s="T210">"Wir hatten Angst ins Wasser zu springen.</ta>
            <ta e="T219" id="Seg_3053" s="T214">Es gab viele Baumstämme dort."</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T2" id="Seg_3054" s="T0">темно было.</ta>
            <ta e="T13" id="Seg_3055" s="T2">марат все залез в спальный мешок, вдруг (выскочила) услыхал что-то стучит</ta>
            <ta e="T21" id="Seg_3056" s="T13">он вспомнил как рассказывал о человек-рыбе.</ta>
            <ta e="T25" id="Seg_3057" s="T21">Марат пошел на берег.</ta>
            <ta e="T34" id="Seg_3058" s="T25">он прошел мимо темное место залез ввер на дорогу</ta>
            <ta e="T40" id="Seg_3059" s="T34">опять он услыхал кто-то стучит</ta>
            <ta e="T46" id="Seg_3060" s="T40">ничего не понимая марат остановился</ta>
            <ta e="T49" id="Seg_3061" s="T46">вдруг показался старик.</ta>
            <ta e="T54" id="Seg_3062" s="T49">в руке у него была палка.</ta>
            <ta e="T60" id="Seg_3063" s="T54">Марат поздоровался, он давно знал старика.</ta>
            <ta e="T68" id="Seg_3064" s="T60">И. П. дежурил (по ночам) ночью на улице, на берегу.</ta>
            <ta e="T76" id="Seg_3065" s="T68">он жил здесь все время, здесь он и родился.</ta>
            <ta e="T81" id="Seg_3066" s="T76">он был хорошим рыбаком</ta>
            <ta e="T86" id="Seg_3067" s="T81">он знал очень много сказок.</ta>
            <ta e="T98" id="Seg_3068" s="T86">вечерами он любил с ребятами сидеть где-нибудь у реки у костра</ta>
            <ta e="T107" id="Seg_3069" s="T98">Марат пошел с Лунем и просил его ехать рыбачить с ребятами.</ta>
            <ta e="T118" id="Seg_3070" s="T107">раньше когда-то было время, с ребятами (бегал утром около моря).</ta>
            <ta e="T121" id="Seg_3071" s="T118">с ребятами ездил в море.</ta>
            <ta e="T126" id="Seg_3072" s="T121">теперь у вас другие дела.</ta>
            <ta e="T136" id="Seg_3073" s="T126">ты о старых делах спроси у Филиппа, он тебе все расскажет</ta>
            <ta e="T139" id="Seg_3074" s="T136">какого Филиппа?</ta>
            <ta e="T141" id="Seg_3075" s="T139">есть такой</ta>
            <ta e="T145" id="Seg_3076" s="T141">он у меня сейчас.</ta>
            <ta e="T148" id="Seg_3077" s="T145">он инженер из Л.</ta>
            <ta e="T154" id="Seg_3078" s="T148">ты наверное слыхал его фамилию Ключин.</ta>
            <ta e="T156" id="Seg_3079" s="T154">я слыхал.</ta>
            <ta e="T160" id="Seg_3080" s="T156">Филипп очень хороший человек.</ta>
            <ta e="T166" id="Seg_3081" s="T160">он здесь ваш корабль осматривает [осматривал].</ta>
            <ta e="T169" id="Seg_3082" s="T166">сказал, хороший.</ta>
            <ta e="T174" id="Seg_3083" s="T169">помогу я ребятам. сказал он.</ta>
            <ta e="T185" id="Seg_3084" s="T174">Марат сказал старику спасибо старому рыбаку, они пошли в разные стороны.</ta>
            <ta e="T190" id="Seg_3085" s="T185">Марат увидел, что маяк не горит.</ta>
            <ta e="T198" id="Seg_3086" s="T190">он увидел что-то нехорошее и быстро пошел.</ta>
            <ta e="T201" id="Seg_3087" s="T198">он встретил Аленку.</ta>
            <ta e="T204" id="Seg_3088" s="T201">побежим туда быстро.</ta>
            <ta e="T206" id="Seg_3089" s="T204">они опоздали.</ta>
            <ta e="T210" id="Seg_3090" s="T206">кто-то прыгнул в воду.</ta>
            <ta e="T214" id="Seg_3091" s="T210">мы побоялись прыгнуть в воду </ta>
            <ta e="T219" id="Seg_3092" s="T214">там было много коряг (подводных деревьев)</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T13" id="Seg_3093" s="T2">[OSV]: The word "kutɨrmoːs" has been edited into "kutɨp qos".</ta>
            <ta e="T166" id="Seg_3094" s="T160">[OSV]: The verbal form has been edited into "mantaltukkɨsa".</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T220" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
