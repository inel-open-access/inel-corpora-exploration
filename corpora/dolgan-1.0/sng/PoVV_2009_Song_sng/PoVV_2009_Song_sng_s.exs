<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID1FB8BD55-EEC2-281E-939A-E38805D5D736">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoVV_2009_Song_sng</transcription-name>
         <referenced-file url="PoVV_2009_Song_sng.wav" />
         <referenced-file url="PoVV_2009_Song_sng.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\sng\PoVV_2009_Song_sng\PoVV_2009_Song_sng.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">54</ud-information>
            <ud-information attribute-name="# HIAT:w">42</ud-information>
            <ud-information attribute-name="# e">42</ud-information>
            <ud-information attribute-name="# HIAT:u">8</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoVV">
            <abbreviation>PoVV</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="1.183" type="appl" />
         <tli id="T1" time="2.072" type="appl" />
         <tli id="T2" time="2.962" type="appl" />
         <tli id="T3" time="3.851" type="appl" />
         <tli id="T4" time="4.74" type="appl" />
         <tli id="T5" time="5.632" type="appl" />
         <tli id="T6" time="6.515" type="appl" />
         <tli id="T7" time="7.397" type="appl" />
         <tli id="T8" time="8.28" type="appl" />
         <tli id="T9" time="9.276" type="appl" />
         <tli id="T10" time="10.271" type="appl" />
         <tli id="T11" time="11.267" type="appl" />
         <tli id="T12" time="12.263" type="appl" />
         <tli id="T13" time="13.259" type="appl" />
         <tli id="T14" time="14.254" type="appl" />
         <tli id="T15" time="15.25" type="appl" />
         <tli id="T16" time="16.333" type="appl" />
         <tli id="T17" time="17.405" type="appl" />
         <tli id="T18" time="18.478" type="appl" />
         <tli id="T19" time="19.551" type="appl" />
         <tli id="T20" time="20.624" type="appl" />
         <tli id="T21" time="21.696" type="appl" />
         <tli id="T22" time="22.769" type="appl" />
         <tli id="T23" time="23.682" type="appl" />
         <tli id="T24" time="24.585" type="appl" />
         <tli id="T25" time="25.488" type="appl" />
         <tli id="T26" time="26.39" type="appl" />
         <tli id="T27" time="27.242" type="appl" />
         <tli id="T28" time="28.095" type="appl" />
         <tli id="T29" time="28.948" type="appl" />
         <tli id="T30" time="29.8" type="appl" />
         <tli id="T31" time="30.95" type="appl" />
         <tli id="T32" time="32.101" type="appl" />
         <tli id="T33" time="33.251" type="appl" />
         <tli id="T34" time="34.401" type="appl" />
         <tli id="T35" time="35.552" type="appl" />
         <tli id="T36" time="36.702" type="appl" />
         <tli id="T37" time="38.237" type="appl" />
         <tli id="T38" time="39.772" type="appl" />
         <tli id="T39" time="41.307" type="appl" />
         <tli id="T40" time="42.842" type="appl" />
         <tli id="T41" time="44.377" type="appl" />
         <tli id="T42" time="45.912" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoVV"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T42" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Ulakan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kölge</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">olordum</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">tɨːga</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Balɨkčɨt</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">dʼonnorgo</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">ɨːranɨ</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">tardɨm</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_32" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">Ulakan</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">dolgun</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">okson</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">kelen</ts>
                  <nts id="Seg_44" n="HIAT:ip">,</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">dolgun</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">baːlɨn</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">baragattɨːr</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_57" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">Ulakan</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">dolgun</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">okson</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">kelen</ts>
                  <nts id="Seg_69" n="HIAT:ip">,</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">dolgun</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">baːlɨn</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">baragattɨːr</ts>
                  <nts id="Seg_79" n="HIAT:ip">.</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_82" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">Iti</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">dolgun</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">mögör</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">dolgun</ts>
                  <nts id="Seg_94" n="HIAT:ip">.</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_97" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_99" n="HIAT:w" s="T26">Innitin</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_102" n="HIAT:w" s="T27">bi͡erbet</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">mököhör</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_108" n="HIAT:w" s="T29">dolgun</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_112" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_114" n="HIAT:w" s="T30">Tɨːttan</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">tiːjen</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">ɨstaŋalɨː</ts>
                  <nts id="Seg_121" n="HIAT:ip">,</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">dolgun</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_127" n="HIAT:w" s="T34">baːlɨn</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_130" n="HIAT:w" s="T35">baragattɨːr</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_134" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">Tɨːttan</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">tiːjen</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T38">ɨstaŋalɨː</ts>
                  <nts id="Seg_143" n="HIAT:ip">,</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_146" n="HIAT:w" s="T39">dolgun</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_149" n="HIAT:w" s="T40">baːlɨn</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">baragattɨːr</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T42" id="Seg_155" n="sc" s="T0">
               <ts e="T1" id="Seg_157" n="e" s="T0">Ulakan </ts>
               <ts e="T2" id="Seg_159" n="e" s="T1">kölge </ts>
               <ts e="T3" id="Seg_161" n="e" s="T2">olordum </ts>
               <ts e="T4" id="Seg_163" n="e" s="T3">tɨːga. </ts>
               <ts e="T5" id="Seg_165" n="e" s="T4">Balɨkčɨt </ts>
               <ts e="T6" id="Seg_167" n="e" s="T5">dʼonnorgo </ts>
               <ts e="T7" id="Seg_169" n="e" s="T6">ɨːranɨ </ts>
               <ts e="T8" id="Seg_171" n="e" s="T7">tardɨm. </ts>
               <ts e="T9" id="Seg_173" n="e" s="T8">Ulakan </ts>
               <ts e="T10" id="Seg_175" n="e" s="T9">dolgun </ts>
               <ts e="T11" id="Seg_177" n="e" s="T10">okson </ts>
               <ts e="T12" id="Seg_179" n="e" s="T11">kelen, </ts>
               <ts e="T13" id="Seg_181" n="e" s="T12">dolgun </ts>
               <ts e="T14" id="Seg_183" n="e" s="T13">baːlɨn </ts>
               <ts e="T15" id="Seg_185" n="e" s="T14">baragattɨːr. </ts>
               <ts e="T16" id="Seg_187" n="e" s="T15">Ulakan </ts>
               <ts e="T17" id="Seg_189" n="e" s="T16">dolgun </ts>
               <ts e="T18" id="Seg_191" n="e" s="T17">okson </ts>
               <ts e="T19" id="Seg_193" n="e" s="T18">kelen, </ts>
               <ts e="T20" id="Seg_195" n="e" s="T19">dolgun </ts>
               <ts e="T21" id="Seg_197" n="e" s="T20">baːlɨn </ts>
               <ts e="T22" id="Seg_199" n="e" s="T21">baragattɨːr. </ts>
               <ts e="T23" id="Seg_201" n="e" s="T22">Iti </ts>
               <ts e="T24" id="Seg_203" n="e" s="T23">dolgun </ts>
               <ts e="T25" id="Seg_205" n="e" s="T24">mögör </ts>
               <ts e="T26" id="Seg_207" n="e" s="T25">dolgun. </ts>
               <ts e="T27" id="Seg_209" n="e" s="T26">Innitin </ts>
               <ts e="T28" id="Seg_211" n="e" s="T27">bi͡erbet </ts>
               <ts e="T29" id="Seg_213" n="e" s="T28">mököhör </ts>
               <ts e="T30" id="Seg_215" n="e" s="T29">dolgun. </ts>
               <ts e="T31" id="Seg_217" n="e" s="T30">Tɨːttan </ts>
               <ts e="T32" id="Seg_219" n="e" s="T31">tiːjen </ts>
               <ts e="T33" id="Seg_221" n="e" s="T32">ɨstaŋalɨː, </ts>
               <ts e="T34" id="Seg_223" n="e" s="T33">dolgun </ts>
               <ts e="T35" id="Seg_225" n="e" s="T34">baːlɨn </ts>
               <ts e="T36" id="Seg_227" n="e" s="T35">baragattɨːr. </ts>
               <ts e="T37" id="Seg_229" n="e" s="T36">Tɨːttan </ts>
               <ts e="T38" id="Seg_231" n="e" s="T37">tiːjen </ts>
               <ts e="T39" id="Seg_233" n="e" s="T38">ɨstaŋalɨː, </ts>
               <ts e="T40" id="Seg_235" n="e" s="T39">dolgun </ts>
               <ts e="T41" id="Seg_237" n="e" s="T40">baːlɨn </ts>
               <ts e="T42" id="Seg_239" n="e" s="T41">baragattɨːr. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_240" s="T0">PoVV_2009_Song_sng.001 (001)</ta>
            <ta e="T8" id="Seg_241" s="T4">PoVV_2009_Song_sng.002 (002)</ta>
            <ta e="T15" id="Seg_242" s="T8">PoVV_2009_Song_sng.003 (003)</ta>
            <ta e="T22" id="Seg_243" s="T15">PoVV_2009_Song_sng.004 (004)</ta>
            <ta e="T26" id="Seg_244" s="T22">PoVV_2009_Song_sng.005 (005)</ta>
            <ta e="T30" id="Seg_245" s="T26">PoVV_2009_Song_sng.006 (006)</ta>
            <ta e="T36" id="Seg_246" s="T30">PoVV_2009_Song_sng.007 (007)</ta>
            <ta e="T42" id="Seg_247" s="T36">PoVV_2009_Song_sng.008 (008)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_248" s="T0">Улакан күөлгэ олордуӈ тыыга.</ta>
            <ta e="T8" id="Seg_249" s="T4">Балыкчыт дьоннорго ырыаны тардыӈ.</ta>
            <ta e="T15" id="Seg_250" s="T8">Улакан долгун оксон кэллэ. Долгун балыгын барагаттыр (бырагаттыр).</ta>
            <ta e="T22" id="Seg_251" s="T15">Улакан долгун оксон кэллэн. Долгун балыгын барагаттыр.</ta>
            <ta e="T26" id="Seg_252" s="T22">Ити долгун мөгөр (мөӈөр) долгун.</ta>
            <ta e="T30" id="Seg_253" s="T26">Иннитин биэрбэт мөккүһэр долгун.</ta>
            <ta e="T36" id="Seg_254" s="T30">Тыыттан тийэн ыстаӈалы. Долгун балыгын барагаттыр.</ta>
            <ta e="T42" id="Seg_255" s="T36">Тыыттан тийэн ыстаӈалыр. Долгун балыгын барагаттыр.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_256" s="T0">Ulakan kölge olordum tɨːga. </ta>
            <ta e="T8" id="Seg_257" s="T4">Balɨkčɨt dʼonnorgo ɨːranɨ tardɨm. </ta>
            <ta e="T15" id="Seg_258" s="T8">Ulakan dolgun okson kelen, dolgun baːlɨn baragattɨːr. </ta>
            <ta e="T22" id="Seg_259" s="T15">Ulakan dolgun okson kelen, dolgun baːlɨn baragattɨːr. </ta>
            <ta e="T26" id="Seg_260" s="T22">Iti dolgun mögör dolgun. </ta>
            <ta e="T30" id="Seg_261" s="T26">Innitin bi͡erbet mököhör dolgun. </ta>
            <ta e="T36" id="Seg_262" s="T30">Tɨːttan tiːjen ɨstaŋalɨː, dolgun baːlɨn baragattɨːr. </ta>
            <ta e="T42" id="Seg_263" s="T36">Tɨːttan tiːjen ɨstaŋalɨː, dolgun baːlɨn baragattɨːr. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_264" s="T0">ulakan</ta>
            <ta e="T2" id="Seg_265" s="T1">köl-ge</ta>
            <ta e="T3" id="Seg_266" s="T2">olor-du-m</ta>
            <ta e="T4" id="Seg_267" s="T3">tɨː-ga</ta>
            <ta e="T5" id="Seg_268" s="T4">balɨk-čɨt</ta>
            <ta e="T6" id="Seg_269" s="T5">dʼon-nor-go</ta>
            <ta e="T7" id="Seg_270" s="T6">ɨːra-nɨ</ta>
            <ta e="T8" id="Seg_271" s="T7">tar-dɨ-m</ta>
            <ta e="T9" id="Seg_272" s="T8">ulakan</ta>
            <ta e="T10" id="Seg_273" s="T9">dolgun</ta>
            <ta e="T11" id="Seg_274" s="T10">oks-on</ta>
            <ta e="T12" id="Seg_275" s="T11">kel-en</ta>
            <ta e="T13" id="Seg_276" s="T12">dolgun</ta>
            <ta e="T14" id="Seg_277" s="T13">baːlɨ-n</ta>
            <ta e="T15" id="Seg_278" s="T14">barag-attɨː-r</ta>
            <ta e="T16" id="Seg_279" s="T15">ulakan</ta>
            <ta e="T17" id="Seg_280" s="T16">dolgun</ta>
            <ta e="T18" id="Seg_281" s="T17">oks-on</ta>
            <ta e="T19" id="Seg_282" s="T18">kel-en</ta>
            <ta e="T20" id="Seg_283" s="T19">dolgun</ta>
            <ta e="T21" id="Seg_284" s="T20">baːlɨ-n</ta>
            <ta e="T22" id="Seg_285" s="T21">barag-attɨː-r</ta>
            <ta e="T23" id="Seg_286" s="T22">iti</ta>
            <ta e="T24" id="Seg_287" s="T23">dolgun</ta>
            <ta e="T25" id="Seg_288" s="T24">mög-ör</ta>
            <ta e="T26" id="Seg_289" s="T25">dolgun</ta>
            <ta e="T27" id="Seg_290" s="T26">inni-ti-n</ta>
            <ta e="T28" id="Seg_291" s="T27">bi͡er-bet</ta>
            <ta e="T29" id="Seg_292" s="T28">mök-ö-h-ör</ta>
            <ta e="T30" id="Seg_293" s="T29">dolgun</ta>
            <ta e="T31" id="Seg_294" s="T30">tɨː-ttan</ta>
            <ta e="T32" id="Seg_295" s="T31">tiːj-en</ta>
            <ta e="T33" id="Seg_296" s="T32">ɨstaŋal-ɨː</ta>
            <ta e="T34" id="Seg_297" s="T33">dolgun</ta>
            <ta e="T35" id="Seg_298" s="T34">baːlɨ-n</ta>
            <ta e="T36" id="Seg_299" s="T35">barag-attɨː-r</ta>
            <ta e="T37" id="Seg_300" s="T36">tɨː-ttan</ta>
            <ta e="T38" id="Seg_301" s="T37">tiːj-en</ta>
            <ta e="T39" id="Seg_302" s="T38">ɨstaŋal-ɨː</ta>
            <ta e="T40" id="Seg_303" s="T39">dolgun</ta>
            <ta e="T41" id="Seg_304" s="T40">baːlɨ-n</ta>
            <ta e="T42" id="Seg_305" s="T41">barag-attɨː-r</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_306" s="T0">ulakan</ta>
            <ta e="T2" id="Seg_307" s="T1">kü͡öl-GA</ta>
            <ta e="T3" id="Seg_308" s="T2">olor-TI-m</ta>
            <ta e="T4" id="Seg_309" s="T3">tɨː-GA</ta>
            <ta e="T5" id="Seg_310" s="T4">balɨk-ČIt</ta>
            <ta e="T6" id="Seg_311" s="T5">dʼon-LAr-GA</ta>
            <ta e="T7" id="Seg_312" s="T6">ɨrɨ͡a-nI</ta>
            <ta e="T8" id="Seg_313" s="T7">tart-TI-m</ta>
            <ta e="T9" id="Seg_314" s="T8">ulakan</ta>
            <ta e="T10" id="Seg_315" s="T9">dolgun</ta>
            <ta e="T11" id="Seg_316" s="T10">ogus-An</ta>
            <ta e="T12" id="Seg_317" s="T11">kel-An</ta>
            <ta e="T13" id="Seg_318" s="T12">dolgun</ta>
            <ta e="T14" id="Seg_319" s="T13">balɨk-n</ta>
            <ta e="T15" id="Seg_320" s="T14">bɨrak-AttAː-Ar</ta>
            <ta e="T16" id="Seg_321" s="T15">ulakan</ta>
            <ta e="T17" id="Seg_322" s="T16">dolgun</ta>
            <ta e="T18" id="Seg_323" s="T17">ogus-An</ta>
            <ta e="T19" id="Seg_324" s="T18">kel-An</ta>
            <ta e="T20" id="Seg_325" s="T19">dolgun</ta>
            <ta e="T21" id="Seg_326" s="T20">balɨk-n</ta>
            <ta e="T22" id="Seg_327" s="T21">bɨrak-AttAː-Ar</ta>
            <ta e="T23" id="Seg_328" s="T22">iti</ta>
            <ta e="T24" id="Seg_329" s="T23">dolgun</ta>
            <ta e="T25" id="Seg_330" s="T24">mök-Ar</ta>
            <ta e="T26" id="Seg_331" s="T25">dolgun</ta>
            <ta e="T27" id="Seg_332" s="T26">ilin-tI-n</ta>
            <ta e="T28" id="Seg_333" s="T27">bi͡er-BAT</ta>
            <ta e="T29" id="Seg_334" s="T28">mök-A-s-Ar</ta>
            <ta e="T30" id="Seg_335" s="T29">dolgun</ta>
            <ta e="T31" id="Seg_336" s="T30">tɨː-ttAn</ta>
            <ta e="T32" id="Seg_337" s="T31">tij-An</ta>
            <ta e="T33" id="Seg_338" s="T32">ɨstaŋalaː-A</ta>
            <ta e="T34" id="Seg_339" s="T33">dolgun</ta>
            <ta e="T35" id="Seg_340" s="T34">balɨk-n</ta>
            <ta e="T36" id="Seg_341" s="T35">bɨrak-AttAː-Ar</ta>
            <ta e="T37" id="Seg_342" s="T36">tɨː-ttAn</ta>
            <ta e="T38" id="Seg_343" s="T37">tij-An</ta>
            <ta e="T39" id="Seg_344" s="T38">ɨstaŋalaː-A</ta>
            <ta e="T40" id="Seg_345" s="T39">dolgun</ta>
            <ta e="T41" id="Seg_346" s="T40">balɨk-n</ta>
            <ta e="T42" id="Seg_347" s="T41">bɨrak-AttAː-Ar</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_348" s="T0">big</ta>
            <ta e="T2" id="Seg_349" s="T1">lake-DAT/LOC</ta>
            <ta e="T3" id="Seg_350" s="T2">sit.down-PST1-1SG</ta>
            <ta e="T4" id="Seg_351" s="T3">small.boat-DAT/LOC</ta>
            <ta e="T5" id="Seg_352" s="T4">fish-AG.[NOM]</ta>
            <ta e="T6" id="Seg_353" s="T5">people-PL-DAT/LOC</ta>
            <ta e="T7" id="Seg_354" s="T6">song-ACC</ta>
            <ta e="T8" id="Seg_355" s="T7">pull-PST1-1SG</ta>
            <ta e="T9" id="Seg_356" s="T8">big</ta>
            <ta e="T10" id="Seg_357" s="T9">wave.[NOM]</ta>
            <ta e="T11" id="Seg_358" s="T10">beat-CVB.SEQ</ta>
            <ta e="T12" id="Seg_359" s="T11">come-CVB.SEQ</ta>
            <ta e="T13" id="Seg_360" s="T12">wave.[NOM]</ta>
            <ta e="T14" id="Seg_361" s="T13">fish-ACC</ta>
            <ta e="T15" id="Seg_362" s="T14">throw-MULT-PRS.[3SG]</ta>
            <ta e="T16" id="Seg_363" s="T15">big</ta>
            <ta e="T17" id="Seg_364" s="T16">wave.[NOM]</ta>
            <ta e="T18" id="Seg_365" s="T17">beat-CVB.SEQ</ta>
            <ta e="T19" id="Seg_366" s="T18">come-CVB.SEQ</ta>
            <ta e="T20" id="Seg_367" s="T19">wave.[NOM]</ta>
            <ta e="T21" id="Seg_368" s="T20">fish-ACC</ta>
            <ta e="T22" id="Seg_369" s="T21">throw-MULT-PRS.[3SG]</ta>
            <ta e="T23" id="Seg_370" s="T22">that</ta>
            <ta e="T24" id="Seg_371" s="T23">wave.[NOM]</ta>
            <ta e="T25" id="Seg_372" s="T24">throw.oneself-PTCP.PRS</ta>
            <ta e="T26" id="Seg_373" s="T25">wave.[NOM]</ta>
            <ta e="T27" id="Seg_374" s="T26">front-3SG-ACC</ta>
            <ta e="T28" id="Seg_375" s="T27">give-NEG.[3SG]</ta>
            <ta e="T29" id="Seg_376" s="T28">throw.oneself-EP-RECP/COLL-PTCP.PRS</ta>
            <ta e="T30" id="Seg_377" s="T29">wave.[NOM]</ta>
            <ta e="T31" id="Seg_378" s="T30">small.boat-ABL</ta>
            <ta e="T32" id="Seg_379" s="T31">reach-CVB.SEQ</ta>
            <ta e="T33" id="Seg_380" s="T32">jump-CVB.SIM</ta>
            <ta e="T34" id="Seg_381" s="T33">wave.[NOM]</ta>
            <ta e="T35" id="Seg_382" s="T34">fish-ACC</ta>
            <ta e="T36" id="Seg_383" s="T35">throw-MULT-PRS.[3SG]</ta>
            <ta e="T37" id="Seg_384" s="T36">small.boat-ABL</ta>
            <ta e="T38" id="Seg_385" s="T37">reach-CVB.SEQ</ta>
            <ta e="T39" id="Seg_386" s="T38">jump-CVB.SIM</ta>
            <ta e="T40" id="Seg_387" s="T39">wave.[NOM]</ta>
            <ta e="T41" id="Seg_388" s="T40">fish-ACC</ta>
            <ta e="T42" id="Seg_389" s="T41">throw-MULT-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_390" s="T0">groß</ta>
            <ta e="T2" id="Seg_391" s="T1">See-DAT/LOC</ta>
            <ta e="T3" id="Seg_392" s="T2">sich.setzen-PST1-1SG</ta>
            <ta e="T4" id="Seg_393" s="T3">kleines.Boot-DAT/LOC</ta>
            <ta e="T5" id="Seg_394" s="T4">Fisch-AG.[NOM]</ta>
            <ta e="T6" id="Seg_395" s="T5">Volk-PL-DAT/LOC</ta>
            <ta e="T7" id="Seg_396" s="T6">Lied-ACC</ta>
            <ta e="T8" id="Seg_397" s="T7">ziehen-PST1-1SG</ta>
            <ta e="T9" id="Seg_398" s="T8">groß</ta>
            <ta e="T10" id="Seg_399" s="T9">Welle.[NOM]</ta>
            <ta e="T11" id="Seg_400" s="T10">schlagen-CVB.SEQ</ta>
            <ta e="T12" id="Seg_401" s="T11">kommen-CVB.SEQ</ta>
            <ta e="T13" id="Seg_402" s="T12">Welle.[NOM]</ta>
            <ta e="T14" id="Seg_403" s="T13">Fisch-ACC</ta>
            <ta e="T15" id="Seg_404" s="T14">werfen-MULT-PRS.[3SG]</ta>
            <ta e="T16" id="Seg_405" s="T15">groß</ta>
            <ta e="T17" id="Seg_406" s="T16">Welle.[NOM]</ta>
            <ta e="T18" id="Seg_407" s="T17">schlagen-CVB.SEQ</ta>
            <ta e="T19" id="Seg_408" s="T18">kommen-CVB.SEQ</ta>
            <ta e="T20" id="Seg_409" s="T19">Welle.[NOM]</ta>
            <ta e="T21" id="Seg_410" s="T20">Fisch-ACC</ta>
            <ta e="T22" id="Seg_411" s="T21">werfen-MULT-PRS.[3SG]</ta>
            <ta e="T23" id="Seg_412" s="T22">dieses</ta>
            <ta e="T24" id="Seg_413" s="T23">Welle.[NOM]</ta>
            <ta e="T25" id="Seg_414" s="T24">sich.werfen-PTCP.PRS</ta>
            <ta e="T26" id="Seg_415" s="T25">Welle.[NOM]</ta>
            <ta e="T27" id="Seg_416" s="T26">Vorderteil-3SG-ACC</ta>
            <ta e="T28" id="Seg_417" s="T27">geben-NEG.[3SG]</ta>
            <ta e="T29" id="Seg_418" s="T28">sich.werfen-EP-RECP/COLL-PTCP.PRS</ta>
            <ta e="T30" id="Seg_419" s="T29">Welle.[NOM]</ta>
            <ta e="T31" id="Seg_420" s="T30">kleines.Boot-ABL</ta>
            <ta e="T32" id="Seg_421" s="T31">ankommen-CVB.SEQ</ta>
            <ta e="T33" id="Seg_422" s="T32">springen-CVB.SIM</ta>
            <ta e="T34" id="Seg_423" s="T33">Welle.[NOM]</ta>
            <ta e="T35" id="Seg_424" s="T34">Fisch-ACC</ta>
            <ta e="T36" id="Seg_425" s="T35">werfen-MULT-PRS.[3SG]</ta>
            <ta e="T37" id="Seg_426" s="T36">kleines.Boot-ABL</ta>
            <ta e="T38" id="Seg_427" s="T37">ankommen-CVB.SEQ</ta>
            <ta e="T39" id="Seg_428" s="T38">springen-CVB.SIM</ta>
            <ta e="T40" id="Seg_429" s="T39">Welle.[NOM]</ta>
            <ta e="T41" id="Seg_430" s="T40">Fisch-ACC</ta>
            <ta e="T42" id="Seg_431" s="T41">werfen-MULT-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_432" s="T0">большой</ta>
            <ta e="T2" id="Seg_433" s="T1">озеро-DAT/LOC</ta>
            <ta e="T3" id="Seg_434" s="T2">сесть-PST1-1SG</ta>
            <ta e="T4" id="Seg_435" s="T3">лодочка-DAT/LOC</ta>
            <ta e="T5" id="Seg_436" s="T4">рыба-AG.[NOM]</ta>
            <ta e="T6" id="Seg_437" s="T5">народ-PL-DAT/LOC</ta>
            <ta e="T7" id="Seg_438" s="T6">песня-ACC</ta>
            <ta e="T8" id="Seg_439" s="T7">тянуть-PST1-1SG</ta>
            <ta e="T9" id="Seg_440" s="T8">большой</ta>
            <ta e="T10" id="Seg_441" s="T9">волна.[NOM]</ta>
            <ta e="T11" id="Seg_442" s="T10">бить-CVB.SEQ</ta>
            <ta e="T12" id="Seg_443" s="T11">приходить-CVB.SEQ</ta>
            <ta e="T13" id="Seg_444" s="T12">волна.[NOM]</ta>
            <ta e="T14" id="Seg_445" s="T13">рыба-ACC</ta>
            <ta e="T15" id="Seg_446" s="T14">бросать-MULT-PRS.[3SG]</ta>
            <ta e="T16" id="Seg_447" s="T15">большой</ta>
            <ta e="T17" id="Seg_448" s="T16">волна.[NOM]</ta>
            <ta e="T18" id="Seg_449" s="T17">бить-CVB.SEQ</ta>
            <ta e="T19" id="Seg_450" s="T18">приходить-CVB.SEQ</ta>
            <ta e="T20" id="Seg_451" s="T19">волна.[NOM]</ta>
            <ta e="T21" id="Seg_452" s="T20">рыба-ACC</ta>
            <ta e="T22" id="Seg_453" s="T21">бросать-MULT-PRS.[3SG]</ta>
            <ta e="T23" id="Seg_454" s="T22">тот</ta>
            <ta e="T24" id="Seg_455" s="T23">волна.[NOM]</ta>
            <ta e="T25" id="Seg_456" s="T24">кидаться-PTCP.PRS</ta>
            <ta e="T26" id="Seg_457" s="T25">волна.[NOM]</ta>
            <ta e="T27" id="Seg_458" s="T26">передняя.часть-3SG-ACC</ta>
            <ta e="T28" id="Seg_459" s="T27">давать-NEG.[3SG]</ta>
            <ta e="T29" id="Seg_460" s="T28">кидаться-EP-RECP/COLL-PTCP.PRS</ta>
            <ta e="T30" id="Seg_461" s="T29">волна.[NOM]</ta>
            <ta e="T31" id="Seg_462" s="T30">лодочка-ABL</ta>
            <ta e="T32" id="Seg_463" s="T31">доезжать-CVB.SEQ</ta>
            <ta e="T33" id="Seg_464" s="T32">прыгать-CVB.SIM</ta>
            <ta e="T34" id="Seg_465" s="T33">волна.[NOM]</ta>
            <ta e="T35" id="Seg_466" s="T34">рыба-ACC</ta>
            <ta e="T36" id="Seg_467" s="T35">бросать-MULT-PRS.[3SG]</ta>
            <ta e="T37" id="Seg_468" s="T36">лодочка-ABL</ta>
            <ta e="T38" id="Seg_469" s="T37">доезжать-CVB.SEQ</ta>
            <ta e="T39" id="Seg_470" s="T38">прыгать-CVB.SIM</ta>
            <ta e="T40" id="Seg_471" s="T39">волна.[NOM]</ta>
            <ta e="T41" id="Seg_472" s="T40">рыба-ACC</ta>
            <ta e="T42" id="Seg_473" s="T41">бросать-MULT-PRS.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_474" s="T0">adj</ta>
            <ta e="T2" id="Seg_475" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_476" s="T2">v-v:tense-v:poss.pn</ta>
            <ta e="T4" id="Seg_477" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_478" s="T4">n-n&gt;n-n:case</ta>
            <ta e="T6" id="Seg_479" s="T5">n-n:(num)-n:case</ta>
            <ta e="T7" id="Seg_480" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_481" s="T7">v-v:tense-v:poss.pn</ta>
            <ta e="T9" id="Seg_482" s="T8">adj</ta>
            <ta e="T10" id="Seg_483" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_484" s="T10">v-v:cvb</ta>
            <ta e="T12" id="Seg_485" s="T11">v-v:cvb</ta>
            <ta e="T13" id="Seg_486" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_487" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_488" s="T14">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T16" id="Seg_489" s="T15">adj</ta>
            <ta e="T17" id="Seg_490" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_491" s="T17">v-v:cvb</ta>
            <ta e="T19" id="Seg_492" s="T18">v-v:cvb</ta>
            <ta e="T20" id="Seg_493" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_494" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_495" s="T21">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T23" id="Seg_496" s="T22">dempro</ta>
            <ta e="T24" id="Seg_497" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_498" s="T24">v-v:ptcp</ta>
            <ta e="T26" id="Seg_499" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_500" s="T26">n-n:poss-n:case</ta>
            <ta e="T28" id="Seg_501" s="T27">v-v:(neg)-v:pred.pn</ta>
            <ta e="T29" id="Seg_502" s="T28">v-v:(ins)-v&gt;v-v:ptcp</ta>
            <ta e="T30" id="Seg_503" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_504" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_505" s="T31">v-v:cvb</ta>
            <ta e="T33" id="Seg_506" s="T32">v-v:cvb</ta>
            <ta e="T34" id="Seg_507" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_508" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_509" s="T35">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T37" id="Seg_510" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_511" s="T37">v-v:cvb</ta>
            <ta e="T39" id="Seg_512" s="T38">v-v:cvb</ta>
            <ta e="T40" id="Seg_513" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_514" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_515" s="T41">v-v&gt;v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_516" s="T0">adj</ta>
            <ta e="T2" id="Seg_517" s="T1">n</ta>
            <ta e="T3" id="Seg_518" s="T2">v</ta>
            <ta e="T4" id="Seg_519" s="T3">n</ta>
            <ta e="T5" id="Seg_520" s="T4">n</ta>
            <ta e="T6" id="Seg_521" s="T5">n</ta>
            <ta e="T7" id="Seg_522" s="T6">n</ta>
            <ta e="T8" id="Seg_523" s="T7">v</ta>
            <ta e="T9" id="Seg_524" s="T8">adj</ta>
            <ta e="T10" id="Seg_525" s="T9">n</ta>
            <ta e="T11" id="Seg_526" s="T10">v</ta>
            <ta e="T12" id="Seg_527" s="T11">v</ta>
            <ta e="T13" id="Seg_528" s="T12">n</ta>
            <ta e="T14" id="Seg_529" s="T13">n</ta>
            <ta e="T15" id="Seg_530" s="T14">v</ta>
            <ta e="T16" id="Seg_531" s="T15">adj</ta>
            <ta e="T17" id="Seg_532" s="T16">n</ta>
            <ta e="T18" id="Seg_533" s="T17">v</ta>
            <ta e="T19" id="Seg_534" s="T18">v</ta>
            <ta e="T20" id="Seg_535" s="T19">n</ta>
            <ta e="T21" id="Seg_536" s="T20">n</ta>
            <ta e="T22" id="Seg_537" s="T21">v</ta>
            <ta e="T23" id="Seg_538" s="T22">dempro</ta>
            <ta e="T24" id="Seg_539" s="T23">n</ta>
            <ta e="T25" id="Seg_540" s="T24">v</ta>
            <ta e="T26" id="Seg_541" s="T25">n</ta>
            <ta e="T27" id="Seg_542" s="T26">n</ta>
            <ta e="T28" id="Seg_543" s="T27">v</ta>
            <ta e="T29" id="Seg_544" s="T28">v</ta>
            <ta e="T30" id="Seg_545" s="T29">n</ta>
            <ta e="T31" id="Seg_546" s="T30">n</ta>
            <ta e="T32" id="Seg_547" s="T31">v</ta>
            <ta e="T33" id="Seg_548" s="T32">v</ta>
            <ta e="T34" id="Seg_549" s="T33">n</ta>
            <ta e="T35" id="Seg_550" s="T34">n</ta>
            <ta e="T36" id="Seg_551" s="T35">v</ta>
            <ta e="T37" id="Seg_552" s="T36">n</ta>
            <ta e="T38" id="Seg_553" s="T37">v</ta>
            <ta e="T39" id="Seg_554" s="T38">v</ta>
            <ta e="T40" id="Seg_555" s="T39">n</ta>
            <ta e="T41" id="Seg_556" s="T40">n</ta>
            <ta e="T42" id="Seg_557" s="T41">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_558" s="T0">At a big lake I sat down into a small boat. </ta>
            <ta e="T8" id="Seg_559" s="T4">I brought a song to the fishermen. </ta>
            <ta e="T15" id="Seg_560" s="T8">A big wave came up, the wave throws fish. </ta>
            <ta e="T22" id="Seg_561" s="T15">A big wave came up, the wave throws fish. </ta>
            <ta e="T26" id="Seg_562" s="T22">This wave, the surging wave. </ta>
            <ta e="T30" id="Seg_563" s="T26">The wave does not give its front. </ta>
            <ta e="T36" id="Seg_564" s="T30">Jumping into the small boat the wave throws fish. </ta>
            <ta e="T42" id="Seg_565" s="T36">Jumping into the small boat the wave throws fish. </ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_566" s="T0">Auf einem großen See setzte ich mich in ein kleines Boot.</ta>
            <ta e="T8" id="Seg_567" s="T4">Den Fischern brachte ich ein Lied. </ta>
            <ta e="T15" id="Seg_568" s="T8">Es kommt eine große Welle auf, die Welle wirft Fisch. </ta>
            <ta e="T22" id="Seg_569" s="T15">Es kommt eine große Welle auf, die Welle wirft Fisch. </ta>
            <ta e="T26" id="Seg_570" s="T22">Diese Welle, die sich auftürmende Welle.</ta>
            <ta e="T30" id="Seg_571" s="T26">Ihr Vorderteil gibt die sich auftürmende Welle nicht.</ta>
            <ta e="T36" id="Seg_572" s="T30">Ins kleine Boot springend wirft die Welle Fisch. </ta>
            <ta e="T42" id="Seg_573" s="T36">Ins Boot springend wirft die Welle Fisch. </ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_574" s="T0">В большой озере сел на ветку, рыбак людям песню тянуть (с пой).</ta>
            <ta e="T8" id="Seg_575" s="T4">Рыбак людям песню тянуть (с пой).</ta>
            <ta e="T15" id="Seg_576" s="T8">Большая волна поднялась. Рыбу кидает.</ta>
            <ta e="T22" id="Seg_577" s="T15">Большая волна поднялась. Волна рыбу кидает.</ta>
            <ta e="T26" id="Seg_578" s="T22">Эта волна волнуется волна.</ta>
            <ta e="T30" id="Seg_579" s="T26">Не сдается упрямая волна.</ta>
            <ta e="T36" id="Seg_580" s="T30">До ветки достает прыгает(брызгает). Волна рыбу кидает.</ta>
            <ta e="T42" id="Seg_581" s="T36">До ветки достает прыгает (брызгает). Волна рыбу кидает.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T30" id="Seg_582" s="T26">[DCh]: Sense not clear. </ta>
            <ta e="T36" id="Seg_583" s="T30">[DCh]: Not clear why ablative case on "tɨː". </ta>
            <ta e="T42" id="Seg_584" s="T36">[DCh]: Not clear why ablative case on "tɨː". </ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
