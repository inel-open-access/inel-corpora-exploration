<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID49578AAF-5AC6-0785-D7D9-DEEE523D10C4">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>YeSV_1964_SnowOwl_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\ErSV_1964_SnowOwl_flk\ErSV_1964_SnowOwl_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">418</ud-information>
            <ud-information attribute-name="# HIAT:w">300</ud-information>
            <ud-information attribute-name="# e">299</ud-information>
            <ud-information attribute-name="# HIAT:u">62</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="YeSV">
            <abbreviation>ErSV</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="YeSV"
                      type="t">
         <timeline-fork end="T264" start="T263">
            <tli id="T263.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T299" id="Seg_0" n="sc" s="T0">
               <ts e="T11" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Bɨlɨr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kaːhɨŋ</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_11" n="HIAT:w" s="T2">kuhuŋ</ts>
                  <nts id="Seg_12" n="HIAT:ip">,</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_15" n="HIAT:w" s="T3">čiːčaːgɨŋ</ts>
                  <nts id="Seg_16" n="HIAT:ip">,</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">leŋkejiŋ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_21" n="HIAT:ip">—</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_24" n="HIAT:w" s="T5">barɨ</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">kötör</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">bu</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">hirge</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">hirdeːk</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">ete</ts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_43" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">Bu</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">hiriŋ</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">ol</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">kemŋe</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">ičiges</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">ühü</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_64" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">Onton</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">tɨmnɨːlar</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">bu͡ollular</ts>
                  <nts id="Seg_73" n="HIAT:ip">,</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">dʼɨl-kün</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">ularɨjda</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_83" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">Munuga</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">kötörüŋ</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">barɨta</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">hübeleher</ts>
                  <nts id="Seg_95" n="HIAT:ip">.</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_98" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">Barɨlarɨgar</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">biːr</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">hübehit</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">baːr</ts>
                  <nts id="Seg_110" n="HIAT:ip">.</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_113" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">Leŋkejiŋ</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">hoččotton</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">kurpaːskɨnɨ</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">hi͡ečči</ts>
                  <nts id="Seg_125" n="HIAT:ip">,</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_128" n="HIAT:w" s="T34">kuhu</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">bu͡ollun</ts>
                  <nts id="Seg_132" n="HIAT:ip">,</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_135" n="HIAT:w" s="T36">kaːhɨ</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_138" n="HIAT:w" s="T37">bu͡ollun</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_141" n="HIAT:w" s="T38">emi͡e</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_145" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_147" n="HIAT:w" s="T39">Bu</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_150" n="HIAT:w" s="T40">kötördör</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_153" n="HIAT:w" s="T41">hübehittere</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_156" n="HIAT:w" s="T42">gini</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_159" n="HIAT:w" s="T43">kaːstartan</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_162" n="HIAT:w" s="T44">ete</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_165" n="HIAT:w" s="T45">kötörü</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_168" n="HIAT:w" s="T46">barɨtɨn</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_171" n="HIAT:w" s="T47">munnʼakka</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_174" n="HIAT:w" s="T48">ɨgɨrar</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_178" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_180" n="HIAT:w" s="T49">Biːr</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_183" n="HIAT:w" s="T50">alɨːnɨ</ts>
                  <nts id="Seg_184" n="HIAT:ip">,</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_187" n="HIAT:w" s="T51">hihi</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_190" n="HIAT:w" s="T52">toloru</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_193" n="HIAT:w" s="T53">kötör</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_196" n="HIAT:w" s="T54">muhunna</ts>
                  <nts id="Seg_197" n="HIAT:ip">.</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_200" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_202" n="HIAT:w" s="T55">Leŋkejiŋ</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_205" n="HIAT:w" s="T56">emi͡e</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_208" n="HIAT:w" s="T57">kelbit</ts>
                  <nts id="Seg_209" n="HIAT:ip">.</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_212" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_214" n="HIAT:w" s="T58">Hübehit</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_217" n="HIAT:w" s="T59">eter</ts>
                  <nts id="Seg_218" n="HIAT:ip">:</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_221" n="HIAT:u" s="T60">
                  <nts id="Seg_222" n="HIAT:ip">"</nts>
                  <ts e="T61" id="Seg_224" n="HIAT:w" s="T60">Dʼe</ts>
                  <nts id="Seg_225" n="HIAT:ip">,</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_228" n="HIAT:w" s="T61">dʼɨl-kün</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_231" n="HIAT:w" s="T62">ularɨjda</ts>
                  <nts id="Seg_232" n="HIAT:ip">.</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_235" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_237" n="HIAT:w" s="T63">Bihigi</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_240" n="HIAT:w" s="T64">hajɨnɨn</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_243" n="HIAT:w" s="T65">agaj</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_246" n="HIAT:w" s="T66">oloror</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_249" n="HIAT:w" s="T67">bu͡ollubut</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_253" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_255" n="HIAT:w" s="T68">Min</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_258" n="HIAT:w" s="T69">bilebin</ts>
                  <nts id="Seg_259" n="HIAT:ip">,</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_262" n="HIAT:w" s="T70">kannɨk</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_265" n="HIAT:w" s="T71">ere</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_268" n="HIAT:w" s="T72">hirge</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_271" n="HIAT:w" s="T73">kɨhɨn</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_274" n="HIAT:w" s="T74">bu͡olbat</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_277" n="HIAT:w" s="T75">dojduta</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_280" n="HIAT:w" s="T76">baːr</ts>
                  <nts id="Seg_281" n="HIAT:ip">.</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_284" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_286" n="HIAT:w" s="T77">Bihigi</ts>
                  <nts id="Seg_287" n="HIAT:ip">,</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_290" n="HIAT:w" s="T78">kɨnattaːktar</ts>
                  <nts id="Seg_291" n="HIAT:ip">,</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_294" n="HIAT:w" s="T79">togo</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_297" n="HIAT:w" s="T80">bu</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_300" n="HIAT:w" s="T81">hirge</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_303" n="HIAT:w" s="T82">toŋo</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_306" n="HIAT:w" s="T83">hɨtɨ͡akpɨtɨj</ts>
                  <nts id="Seg_307" n="HIAT:ip">?</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_310" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_312" n="HIAT:w" s="T84">Hirinen</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_315" n="HIAT:w" s="T85">bardakka</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_318" n="HIAT:w" s="T86">ɨraːk</ts>
                  <nts id="Seg_319" n="HIAT:ip">.</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_322" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_324" n="HIAT:w" s="T87">Bihigi</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_327" n="HIAT:w" s="T88">kurdarɨ</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_330" n="HIAT:w" s="T89">ol</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_333" n="HIAT:w" s="T90">hirge</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_336" n="HIAT:w" s="T91">kötü͡ögüŋ</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_339" n="HIAT:w" s="T92">kɨhɨn</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_342" n="HIAT:w" s="T93">keliːte</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_346" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_348" n="HIAT:w" s="T94">Bu</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_351" n="HIAT:w" s="T95">tɨmnɨːbɨt</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_354" n="HIAT:w" s="T96">aːhɨ͡ar</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_357" n="HIAT:w" s="T97">di͡eri</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_360" n="HIAT:w" s="T98">ol</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_363" n="HIAT:w" s="T99">hirge</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_366" n="HIAT:w" s="T100">ologuru͡oguŋ</ts>
                  <nts id="Seg_367" n="HIAT:ip">.</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_370" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_372" n="HIAT:w" s="T101">Munna</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_375" n="HIAT:w" s="T102">olordokputuna</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_378" n="HIAT:w" s="T103">bihigi</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_381" n="HIAT:w" s="T104">barɨ</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_384" n="HIAT:w" s="T105">habɨllɨ͡akpɨt</ts>
                  <nts id="Seg_385" n="HIAT:ip">,</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_388" n="HIAT:w" s="T106">toŋon</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_391" n="HIAT:w" s="T107">ölü͡ökpüt</ts>
                  <nts id="Seg_392" n="HIAT:ip">.</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_395" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_397" n="HIAT:w" s="T108">Onno</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_400" n="HIAT:w" s="T109">köttökpütüne</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_403" n="HIAT:w" s="T110">barɨ</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_406" n="HIAT:w" s="T111">kihi</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_409" n="HIAT:w" s="T112">bu͡olu͡okput</ts>
                  <nts id="Seg_410" n="HIAT:ip">.</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_413" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_415" n="HIAT:w" s="T113">Dʼe</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_418" n="HIAT:w" s="T114">hübegitin</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_421" n="HIAT:w" s="T115">bi͡eriŋ</ts>
                  <nts id="Seg_422" n="HIAT:ip">!</nts>
                  <nts id="Seg_423" n="HIAT:ip">"</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_426" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_428" n="HIAT:w" s="T116">Onuga</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_431" n="HIAT:w" s="T117">kötör</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_434" n="HIAT:w" s="T118">barɨta</ts>
                  <nts id="Seg_435" n="HIAT:ip">:</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_438" n="HIAT:u" s="T119">
                  <nts id="Seg_439" n="HIAT:ip">"</nts>
                  <ts e="T120" id="Seg_441" n="HIAT:w" s="T119">Dʼe</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_444" n="HIAT:w" s="T120">kirdik</ts>
                  <nts id="Seg_445" n="HIAT:ip">!</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_448" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_450" n="HIAT:w" s="T121">Bihigi</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_453" n="HIAT:w" s="T122">barɨ</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_456" n="HIAT:w" s="T123">kötü͡ökpüt</ts>
                  <nts id="Seg_457" n="HIAT:ip">.</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_460" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_462" n="HIAT:w" s="T124">Munna</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_465" n="HIAT:w" s="T125">ölüːhübüt</ts>
                  <nts id="Seg_466" n="HIAT:ip">!</nts>
                  <nts id="Seg_467" n="HIAT:ip">"</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_470" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_472" n="HIAT:w" s="T126">Hübehit</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_475" n="HIAT:w" s="T127">ɨjɨtar</ts>
                  <nts id="Seg_476" n="HIAT:ip">:</nts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_479" n="HIAT:u" s="T128">
                  <nts id="Seg_480" n="HIAT:ip">"</nts>
                  <ts e="T129" id="Seg_482" n="HIAT:w" s="T128">Dʼe</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_485" n="HIAT:w" s="T129">kaskɨt</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_488" n="HIAT:w" s="T130">höbüleːte</ts>
                  <nts id="Seg_489" n="HIAT:ip">?</nts>
                  <nts id="Seg_490" n="HIAT:ip">"</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_493" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_495" n="HIAT:w" s="T131">Kötörüŋ</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_498" n="HIAT:w" s="T132">barɨta</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_501" n="HIAT:w" s="T133">höbüleːbit</ts>
                  <nts id="Seg_502" n="HIAT:ip">.</nts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_505" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_507" n="HIAT:w" s="T134">Leŋkej</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_510" n="HIAT:w" s="T135">emi͡e</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_513" n="HIAT:w" s="T136">haŋalaːk</ts>
                  <nts id="Seg_514" n="HIAT:ip">:</nts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_517" n="HIAT:u" s="T137">
                  <nts id="Seg_518" n="HIAT:ip">"</nts>
                  <ts e="T138" id="Seg_520" n="HIAT:w" s="T137">Min</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_523" n="HIAT:w" s="T138">emi͡e</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_526" n="HIAT:w" s="T139">barabɨn</ts>
                  <nts id="Seg_527" n="HIAT:ip">.</nts>
                  <nts id="Seg_528" n="HIAT:ip">"</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_531" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_533" n="HIAT:w" s="T140">Hübehit</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_536" n="HIAT:w" s="T141">ɨjɨtar</ts>
                  <nts id="Seg_537" n="HIAT:ip">:</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_540" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_542" n="HIAT:w" s="T142">Kaja</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_545" n="HIAT:w" s="T143">bu</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_548" n="HIAT:w" s="T144">leŋkej</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_551" n="HIAT:w" s="T145">emi͡e</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_554" n="HIAT:w" s="T146">barsaːrɨ</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_557" n="HIAT:w" s="T147">gɨnar</ts>
                  <nts id="Seg_558" n="HIAT:ip">.</nts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_561" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_563" n="HIAT:w" s="T148">Höbülüːgüt</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_566" n="HIAT:w" s="T149">du͡o</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_569" n="HIAT:w" s="T150">gini</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_572" n="HIAT:w" s="T151">barsarɨn</ts>
                  <nts id="Seg_573" n="HIAT:ip">?</nts>
                  <nts id="Seg_574" n="HIAT:ip">"</nts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_577" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_579" n="HIAT:w" s="T152">Onuga</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_582" n="HIAT:w" s="T153">kötördörüŋ</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_585" n="HIAT:w" s="T154">barɨta</ts>
                  <nts id="Seg_586" n="HIAT:ip">:</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T159" id="Seg_589" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_591" n="HIAT:w" s="T155">Bihigi</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_594" n="HIAT:w" s="T156">leŋkej</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_597" n="HIAT:w" s="T157">barsarɨn</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_600" n="HIAT:w" s="T158">höbüleːbeppit</ts>
                  <nts id="Seg_601" n="HIAT:ip">.</nts>
                  <nts id="Seg_602" n="HIAT:ip">"</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_605" n="HIAT:u" s="T159">
                  <nts id="Seg_606" n="HIAT:ip">"</nts>
                  <ts e="T160" id="Seg_608" n="HIAT:w" s="T159">Togo</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_611" n="HIAT:w" s="T160">höbüleːbekkit</ts>
                  <nts id="Seg_612" n="HIAT:ip">?</nts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_615" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_617" n="HIAT:w" s="T161">Hɨrajdaːk-karagar</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_620" n="HIAT:w" s="T162">haŋarɨŋ</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_623" n="HIAT:w" s="T163">ginini</ts>
                  <nts id="Seg_624" n="HIAT:ip">.</nts>
                  <nts id="Seg_625" n="HIAT:ip">"</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_628" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_630" n="HIAT:w" s="T164">Onuga</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_633" n="HIAT:w" s="T165">kötör</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_636" n="HIAT:w" s="T166">barɨta</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_639" n="HIAT:w" s="T167">di͡ete</ts>
                  <nts id="Seg_640" n="HIAT:ip">:</nts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_643" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_645" n="HIAT:w" s="T168">Munna</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_648" n="HIAT:w" s="T169">bu͡ollagɨn</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_651" n="HIAT:w" s="T170">leŋkej</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_654" n="HIAT:w" s="T171">iːtillen</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_657" n="HIAT:w" s="T172">oloror</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_660" n="HIAT:w" s="T173">bihigi</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_663" n="HIAT:w" s="T174">ere</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_666" n="HIAT:w" s="T175">kergemmitinen</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_669" n="HIAT:w" s="T176">ahɨlɨktanan</ts>
                  <nts id="Seg_670" n="HIAT:ip">.</nts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T185" id="Seg_673" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_675" n="HIAT:w" s="T177">Onno</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_678" n="HIAT:w" s="T178">bardagɨna</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_681" n="HIAT:w" s="T179">da</ts>
                  <nts id="Seg_682" n="HIAT:ip">,</nts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_685" n="HIAT:w" s="T180">ol</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_688" n="HIAT:w" s="T181">mi͡eretin</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_691" n="HIAT:w" s="T182">hin</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_694" n="HIAT:w" s="T183">keːhi͡e</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_697" n="HIAT:w" s="T184">hu͡oga</ts>
                  <nts id="Seg_698" n="HIAT:ip">.</nts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_701" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_703" n="HIAT:w" s="T185">Bihigi</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_706" n="HIAT:w" s="T186">ippeppit</ts>
                  <nts id="Seg_707" n="HIAT:ip">!</nts>
                  <nts id="Seg_708" n="HIAT:ip">"</nts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_711" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_713" n="HIAT:w" s="T187">Hübehit</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_716" n="HIAT:w" s="T188">diːr</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_719" n="HIAT:w" s="T189">leŋkejge</ts>
                  <nts id="Seg_720" n="HIAT:ip">:</nts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_723" n="HIAT:u" s="T190">
                  <nts id="Seg_724" n="HIAT:ip">"</nts>
                  <ts e="T191" id="Seg_726" n="HIAT:w" s="T190">Kör</ts>
                  <nts id="Seg_727" n="HIAT:ip">,</nts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_730" n="HIAT:w" s="T191">dʼon</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_733" n="HIAT:w" s="T192">poru͡oktaːta</ts>
                  <nts id="Seg_734" n="HIAT:ip">.</nts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_737" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_739" n="HIAT:w" s="T193">Kötör</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_742" n="HIAT:w" s="T194">barɨta</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_745" n="HIAT:w" s="T195">haŋarda</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_747" n="HIAT:ip">—</nts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_750" n="HIAT:w" s="T196">ol</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_753" n="HIAT:w" s="T197">hoku͡on</ts>
                  <nts id="Seg_754" n="HIAT:ip">.</nts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_757" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_759" n="HIAT:w" s="T198">Bu</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_762" n="HIAT:w" s="T199">hirge</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_765" n="HIAT:w" s="T200">kaːlagɨn</ts>
                  <nts id="Seg_766" n="HIAT:ip">,</nts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_769" n="HIAT:w" s="T201">bagar</ts>
                  <nts id="Seg_770" n="HIAT:ip">,</nts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_773" n="HIAT:w" s="T202">toŋon</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_776" n="HIAT:w" s="T203">öl</ts>
                  <nts id="Seg_777" n="HIAT:ip">.</nts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_780" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_782" n="HIAT:w" s="T204">Kajdak</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_785" n="HIAT:w" s="T205">hanɨːgɨn</ts>
                  <nts id="Seg_786" n="HIAT:ip">?</nts>
                  <nts id="Seg_787" n="HIAT:ip">"</nts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_790" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_792" n="HIAT:w" s="T206">Leŋkej</ts>
                  <nts id="Seg_793" n="HIAT:ip">:</nts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_796" n="HIAT:u" s="T207">
                  <nts id="Seg_797" n="HIAT:ip">"</nts>
                  <ts e="T208" id="Seg_799" n="HIAT:w" s="T207">Mökküjbeppin</ts>
                  <nts id="Seg_800" n="HIAT:ip">.</nts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_803" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_805" n="HIAT:w" s="T208">Hogotok</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_808" n="HIAT:w" s="T209">duša</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_811" n="HIAT:w" s="T210">kanna</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_814" n="HIAT:w" s="T211">ölböt</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_816" n="HIAT:ip">—</nts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_819" n="HIAT:w" s="T212">bagar</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_822" n="HIAT:w" s="T213">ordu͡om</ts>
                  <nts id="Seg_823" n="HIAT:ip">.</nts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_826" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_828" n="HIAT:w" s="T214">Kaːlabɨn</ts>
                  <nts id="Seg_829" n="HIAT:ip">"</nts>
                  <nts id="Seg_830" n="HIAT:ip">,</nts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_833" n="HIAT:w" s="T215">diːr</ts>
                  <nts id="Seg_834" n="HIAT:ip">.</nts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_837" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_839" n="HIAT:w" s="T216">Kötördör</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_842" n="HIAT:w" s="T217">barɨ</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_845" n="HIAT:w" s="T218">kötüteleːn</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_848" n="HIAT:w" s="T219">kaːlbɨttar</ts>
                  <nts id="Seg_849" n="HIAT:ip">.</nts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_852" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_854" n="HIAT:w" s="T220">Onuga</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_857" n="HIAT:w" s="T221">leŋkej</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_860" n="HIAT:w" s="T222">emeːksiniger</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_863" n="HIAT:w" s="T223">geler</ts>
                  <nts id="Seg_864" n="HIAT:ip">:</nts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_867" n="HIAT:u" s="T224">
                  <nts id="Seg_868" n="HIAT:ip">"</nts>
                  <ts e="T225" id="Seg_870" n="HIAT:w" s="T224">Kaja-keː</ts>
                  <nts id="Seg_871" n="HIAT:ip">,</nts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_874" n="HIAT:w" s="T225">bihigini</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_877" n="HIAT:w" s="T226">ilpetter</ts>
                  <nts id="Seg_878" n="HIAT:ip">.</nts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_881" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_883" n="HIAT:w" s="T227">Mini͡eke</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_886" n="HIAT:w" s="T228">en</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_889" n="HIAT:w" s="T229">köp</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_892" n="HIAT:w" s="T230">mɨjaːn</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_895" n="HIAT:w" s="T231">larga</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_898" n="HIAT:w" s="T232">hokujda</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_901" n="HIAT:w" s="T233">tik</ts>
                  <nts id="Seg_902" n="HIAT:ip">.</nts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T241" id="Seg_905" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_907" n="HIAT:w" s="T234">Purgaːga</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_910" n="HIAT:w" s="T235">da</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_913" n="HIAT:w" s="T236">tibillibet</ts>
                  <nts id="Seg_914" n="HIAT:ip">,</nts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_917" n="HIAT:w" s="T237">tɨmnɨːga</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_920" n="HIAT:w" s="T238">da</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_923" n="HIAT:w" s="T239">toŋmot</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_926" n="HIAT:w" s="T240">gɨna</ts>
                  <nts id="Seg_927" n="HIAT:ip">.</nts>
                  <nts id="Seg_928" n="HIAT:ip">"</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_931" n="HIAT:u" s="T241">
                  <ts e="T242" id="Seg_933" n="HIAT:w" s="T241">Emeːksine</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_935" n="HIAT:ip">"</nts>
                  <ts e="T243" id="Seg_937" n="HIAT:w" s="T242">tigi͡ektibin</ts>
                  <nts id="Seg_938" n="HIAT:ip">"</nts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_941" n="HIAT:w" s="T243">diːr</ts>
                  <nts id="Seg_942" n="HIAT:ip">.</nts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_945" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_947" n="HIAT:w" s="T244">Kaːrɨ</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_950" n="HIAT:w" s="T245">gɨtta</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_953" n="HIAT:w" s="T246">biːr</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_956" n="HIAT:w" s="T247">čeːlkeː</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_959" n="HIAT:w" s="T248">bu͡ollun</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_962" n="HIAT:w" s="T249">ol</ts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_965" n="HIAT:w" s="T250">hokuj</ts>
                  <nts id="Seg_966" n="HIAT:ip">!</nts>
                  <nts id="Seg_967" n="HIAT:ip">"</nts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_970" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_972" n="HIAT:w" s="T251">Emeːksine</ts>
                  <nts id="Seg_973" n="HIAT:ip">,</nts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_976" n="HIAT:w" s="T252">uːs</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_979" n="HIAT:w" s="T253">mujaːn</ts>
                  <nts id="Seg_980" n="HIAT:ip">,</nts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_983" n="HIAT:w" s="T254">tikpit</ts>
                  <nts id="Seg_984" n="HIAT:ip">,</nts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_987" n="HIAT:w" s="T255">kaːs</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_990" n="HIAT:w" s="T256">gi͡enineːger</ts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_993" n="HIAT:w" s="T257">ürüŋ</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_996" n="HIAT:w" s="T258">čeːlkeː</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_999" n="HIAT:w" s="T259">hokuju</ts>
                  <nts id="Seg_1000" n="HIAT:ip">.</nts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_1003" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_1005" n="HIAT:w" s="T260">Leŋkej</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1008" n="HIAT:w" s="T261">bunu</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1011" n="HIAT:w" s="T262">keppite</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1013" n="HIAT:ip">—</nts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263.tx.1" id="Seg_1016" n="HIAT:w" s="T263">hörü</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1019" n="HIAT:w" s="T263.tx.1">höp</ts>
                  <nts id="Seg_1020" n="HIAT:ip">,</nts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1023" n="HIAT:w" s="T264">hokuja</ts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1026" n="HIAT:w" s="T265">etiger</ts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1029" n="HIAT:w" s="T266">erije</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1032" n="HIAT:w" s="T267">kappɨt</ts>
                  <nts id="Seg_1033" n="HIAT:ip">,</nts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1036" n="HIAT:w" s="T268">tɨmnɨːga</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1039" n="HIAT:w" s="T269">da</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1042" n="HIAT:w" s="T270">toŋmot</ts>
                  <nts id="Seg_1043" n="HIAT:ip">,</nts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1046" n="HIAT:w" s="T271">purgaːga</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1049" n="HIAT:w" s="T272">da</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1052" n="HIAT:w" s="T273">hɨstɨbat</ts>
                  <nts id="Seg_1053" n="HIAT:ip">,</nts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1056" n="HIAT:w" s="T274">kaːr</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1059" n="HIAT:w" s="T275">da</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1062" n="HIAT:w" s="T276">hɨstɨbat</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1065" n="HIAT:w" s="T277">gini͡eke</ts>
                  <nts id="Seg_1066" n="HIAT:ip">.</nts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_1069" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_1071" n="HIAT:w" s="T278">Leŋkej</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1074" n="HIAT:w" s="T279">hoččotton</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1077" n="HIAT:w" s="T280">bu</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1080" n="HIAT:w" s="T281">hirge</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1083" n="HIAT:w" s="T282">dojdulammɨta</ts>
                  <nts id="Seg_1084" n="HIAT:ip">.</nts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1087" n="HIAT:u" s="T283">
                  <ts e="T284" id="Seg_1089" n="HIAT:w" s="T283">Ol</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1092" n="HIAT:w" s="T284">kaːlbɨtɨn</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1095" n="HIAT:w" s="T285">öhü͡ömčüleːn</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1098" n="HIAT:w" s="T286">leŋkej</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1101" n="HIAT:w" s="T287">kɨra</ts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1104" n="HIAT:w" s="T288">kötörü</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1107" n="HIAT:w" s="T289">barɨtɨn</ts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1110" n="HIAT:w" s="T290">dössü͡ö</ts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1113" n="HIAT:w" s="T291">hiːr</ts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1116" n="HIAT:w" s="T292">bu͡olbuta</ts>
                  <nts id="Seg_1117" n="HIAT:ip">.</nts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T298" id="Seg_1120" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1122" n="HIAT:w" s="T293">Orduk</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1125" n="HIAT:w" s="T294">ginini</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1128" n="HIAT:w" s="T295">kɨtta</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1131" n="HIAT:w" s="T296">kɨstɨːr</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1134" n="HIAT:w" s="T297">kurpaːskɨnɨ</ts>
                  <nts id="Seg_1135" n="HIAT:ip">.</nts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_1138" n="HIAT:u" s="T298">
                  <ts e="T299" id="Seg_1140" n="HIAT:w" s="T298">Elete</ts>
                  <nts id="Seg_1141" n="HIAT:ip">.</nts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T299" id="Seg_1143" n="sc" s="T0">
               <ts e="T1" id="Seg_1145" n="e" s="T0">Bɨlɨr </ts>
               <ts e="T2" id="Seg_1147" n="e" s="T1">kaːhɨŋ, </ts>
               <ts e="T3" id="Seg_1149" n="e" s="T2">kuhuŋ, </ts>
               <ts e="T4" id="Seg_1151" n="e" s="T3">čiːčaːgɨŋ, </ts>
               <ts e="T5" id="Seg_1153" n="e" s="T4">leŋkejiŋ — </ts>
               <ts e="T6" id="Seg_1155" n="e" s="T5">barɨ </ts>
               <ts e="T7" id="Seg_1157" n="e" s="T6">kötör </ts>
               <ts e="T8" id="Seg_1159" n="e" s="T7">bu </ts>
               <ts e="T9" id="Seg_1161" n="e" s="T8">hirge </ts>
               <ts e="T10" id="Seg_1163" n="e" s="T9">hirdeːk </ts>
               <ts e="T11" id="Seg_1165" n="e" s="T10">ete. </ts>
               <ts e="T12" id="Seg_1167" n="e" s="T11">Bu </ts>
               <ts e="T13" id="Seg_1169" n="e" s="T12">hiriŋ </ts>
               <ts e="T14" id="Seg_1171" n="e" s="T13">ol </ts>
               <ts e="T15" id="Seg_1173" n="e" s="T14">kemŋe </ts>
               <ts e="T16" id="Seg_1175" n="e" s="T15">ičiges </ts>
               <ts e="T17" id="Seg_1177" n="e" s="T16">ühü. </ts>
               <ts e="T18" id="Seg_1179" n="e" s="T17">Onton </ts>
               <ts e="T19" id="Seg_1181" n="e" s="T18">tɨmnɨːlar </ts>
               <ts e="T20" id="Seg_1183" n="e" s="T19">bu͡ollular, </ts>
               <ts e="T21" id="Seg_1185" n="e" s="T20">dʼɨl-kün </ts>
               <ts e="T22" id="Seg_1187" n="e" s="T21">ularɨjda. </ts>
               <ts e="T23" id="Seg_1189" n="e" s="T22">Munuga </ts>
               <ts e="T24" id="Seg_1191" n="e" s="T23">kötörüŋ </ts>
               <ts e="T25" id="Seg_1193" n="e" s="T24">barɨta </ts>
               <ts e="T26" id="Seg_1195" n="e" s="T25">hübeleher. </ts>
               <ts e="T27" id="Seg_1197" n="e" s="T26">Barɨlarɨgar </ts>
               <ts e="T28" id="Seg_1199" n="e" s="T27">biːr </ts>
               <ts e="T29" id="Seg_1201" n="e" s="T28">hübehit </ts>
               <ts e="T30" id="Seg_1203" n="e" s="T29">baːr. </ts>
               <ts e="T31" id="Seg_1205" n="e" s="T30">Leŋkejiŋ </ts>
               <ts e="T32" id="Seg_1207" n="e" s="T31">hoččotton </ts>
               <ts e="T33" id="Seg_1209" n="e" s="T32">kurpaːskɨnɨ </ts>
               <ts e="T34" id="Seg_1211" n="e" s="T33">hi͡ečči, </ts>
               <ts e="T35" id="Seg_1213" n="e" s="T34">kuhu </ts>
               <ts e="T36" id="Seg_1215" n="e" s="T35">bu͡ollun, </ts>
               <ts e="T37" id="Seg_1217" n="e" s="T36">kaːhɨ </ts>
               <ts e="T38" id="Seg_1219" n="e" s="T37">bu͡ollun </ts>
               <ts e="T39" id="Seg_1221" n="e" s="T38">emi͡e. </ts>
               <ts e="T40" id="Seg_1223" n="e" s="T39">Bu </ts>
               <ts e="T41" id="Seg_1225" n="e" s="T40">kötördör </ts>
               <ts e="T42" id="Seg_1227" n="e" s="T41">hübehittere </ts>
               <ts e="T43" id="Seg_1229" n="e" s="T42">gini </ts>
               <ts e="T44" id="Seg_1231" n="e" s="T43">kaːstartan </ts>
               <ts e="T45" id="Seg_1233" n="e" s="T44">ete </ts>
               <ts e="T46" id="Seg_1235" n="e" s="T45">kötörü </ts>
               <ts e="T47" id="Seg_1237" n="e" s="T46">barɨtɨn </ts>
               <ts e="T48" id="Seg_1239" n="e" s="T47">munnʼakka </ts>
               <ts e="T49" id="Seg_1241" n="e" s="T48">ɨgɨrar. </ts>
               <ts e="T50" id="Seg_1243" n="e" s="T49">Biːr </ts>
               <ts e="T51" id="Seg_1245" n="e" s="T50">alɨːnɨ, </ts>
               <ts e="T52" id="Seg_1247" n="e" s="T51">hihi </ts>
               <ts e="T53" id="Seg_1249" n="e" s="T52">toloru </ts>
               <ts e="T54" id="Seg_1251" n="e" s="T53">kötör </ts>
               <ts e="T55" id="Seg_1253" n="e" s="T54">muhunna. </ts>
               <ts e="T56" id="Seg_1255" n="e" s="T55">Leŋkejiŋ </ts>
               <ts e="T57" id="Seg_1257" n="e" s="T56">emi͡e </ts>
               <ts e="T58" id="Seg_1259" n="e" s="T57">kelbit. </ts>
               <ts e="T59" id="Seg_1261" n="e" s="T58">Hübehit </ts>
               <ts e="T60" id="Seg_1263" n="e" s="T59">eter: </ts>
               <ts e="T61" id="Seg_1265" n="e" s="T60">"Dʼe, </ts>
               <ts e="T62" id="Seg_1267" n="e" s="T61">dʼɨl-kün </ts>
               <ts e="T63" id="Seg_1269" n="e" s="T62">ularɨjda. </ts>
               <ts e="T64" id="Seg_1271" n="e" s="T63">Bihigi </ts>
               <ts e="T65" id="Seg_1273" n="e" s="T64">hajɨnɨn </ts>
               <ts e="T66" id="Seg_1275" n="e" s="T65">agaj </ts>
               <ts e="T67" id="Seg_1277" n="e" s="T66">oloror </ts>
               <ts e="T68" id="Seg_1279" n="e" s="T67">bu͡ollubut. </ts>
               <ts e="T69" id="Seg_1281" n="e" s="T68">Min </ts>
               <ts e="T70" id="Seg_1283" n="e" s="T69">bilebin, </ts>
               <ts e="T71" id="Seg_1285" n="e" s="T70">kannɨk </ts>
               <ts e="T72" id="Seg_1287" n="e" s="T71">ere </ts>
               <ts e="T73" id="Seg_1289" n="e" s="T72">hirge </ts>
               <ts e="T74" id="Seg_1291" n="e" s="T73">kɨhɨn </ts>
               <ts e="T75" id="Seg_1293" n="e" s="T74">bu͡olbat </ts>
               <ts e="T76" id="Seg_1295" n="e" s="T75">dojduta </ts>
               <ts e="T77" id="Seg_1297" n="e" s="T76">baːr. </ts>
               <ts e="T78" id="Seg_1299" n="e" s="T77">Bihigi, </ts>
               <ts e="T79" id="Seg_1301" n="e" s="T78">kɨnattaːktar, </ts>
               <ts e="T80" id="Seg_1303" n="e" s="T79">togo </ts>
               <ts e="T81" id="Seg_1305" n="e" s="T80">bu </ts>
               <ts e="T82" id="Seg_1307" n="e" s="T81">hirge </ts>
               <ts e="T83" id="Seg_1309" n="e" s="T82">toŋo </ts>
               <ts e="T84" id="Seg_1311" n="e" s="T83">hɨtɨ͡akpɨtɨj? </ts>
               <ts e="T85" id="Seg_1313" n="e" s="T84">Hirinen </ts>
               <ts e="T86" id="Seg_1315" n="e" s="T85">bardakka </ts>
               <ts e="T87" id="Seg_1317" n="e" s="T86">ɨraːk. </ts>
               <ts e="T88" id="Seg_1319" n="e" s="T87">Bihigi </ts>
               <ts e="T89" id="Seg_1321" n="e" s="T88">kurdarɨ </ts>
               <ts e="T90" id="Seg_1323" n="e" s="T89">ol </ts>
               <ts e="T91" id="Seg_1325" n="e" s="T90">hirge </ts>
               <ts e="T92" id="Seg_1327" n="e" s="T91">kötü͡ögüŋ </ts>
               <ts e="T93" id="Seg_1329" n="e" s="T92">kɨhɨn </ts>
               <ts e="T94" id="Seg_1331" n="e" s="T93">keliːte. </ts>
               <ts e="T95" id="Seg_1333" n="e" s="T94">Bu </ts>
               <ts e="T96" id="Seg_1335" n="e" s="T95">tɨmnɨːbɨt </ts>
               <ts e="T97" id="Seg_1337" n="e" s="T96">aːhɨ͡ar </ts>
               <ts e="T98" id="Seg_1339" n="e" s="T97">di͡eri </ts>
               <ts e="T99" id="Seg_1341" n="e" s="T98">ol </ts>
               <ts e="T100" id="Seg_1343" n="e" s="T99">hirge </ts>
               <ts e="T101" id="Seg_1345" n="e" s="T100">ologuru͡oguŋ. </ts>
               <ts e="T102" id="Seg_1347" n="e" s="T101">Munna </ts>
               <ts e="T103" id="Seg_1349" n="e" s="T102">olordokputuna </ts>
               <ts e="T104" id="Seg_1351" n="e" s="T103">bihigi </ts>
               <ts e="T105" id="Seg_1353" n="e" s="T104">barɨ </ts>
               <ts e="T106" id="Seg_1355" n="e" s="T105">habɨllɨ͡akpɨt, </ts>
               <ts e="T107" id="Seg_1357" n="e" s="T106">toŋon </ts>
               <ts e="T108" id="Seg_1359" n="e" s="T107">ölü͡ökpüt. </ts>
               <ts e="T109" id="Seg_1361" n="e" s="T108">Onno </ts>
               <ts e="T110" id="Seg_1363" n="e" s="T109">köttökpütüne </ts>
               <ts e="T111" id="Seg_1365" n="e" s="T110">barɨ </ts>
               <ts e="T112" id="Seg_1367" n="e" s="T111">kihi </ts>
               <ts e="T113" id="Seg_1369" n="e" s="T112">bu͡olu͡okput. </ts>
               <ts e="T114" id="Seg_1371" n="e" s="T113">Dʼe </ts>
               <ts e="T115" id="Seg_1373" n="e" s="T114">hübegitin </ts>
               <ts e="T116" id="Seg_1375" n="e" s="T115">bi͡eriŋ!" </ts>
               <ts e="T117" id="Seg_1377" n="e" s="T116">Onuga </ts>
               <ts e="T118" id="Seg_1379" n="e" s="T117">kötör </ts>
               <ts e="T119" id="Seg_1381" n="e" s="T118">barɨta: </ts>
               <ts e="T120" id="Seg_1383" n="e" s="T119">"Dʼe </ts>
               <ts e="T121" id="Seg_1385" n="e" s="T120">kirdik! </ts>
               <ts e="T122" id="Seg_1387" n="e" s="T121">Bihigi </ts>
               <ts e="T123" id="Seg_1389" n="e" s="T122">barɨ </ts>
               <ts e="T124" id="Seg_1391" n="e" s="T123">kötü͡ökpüt. </ts>
               <ts e="T125" id="Seg_1393" n="e" s="T124">Munna </ts>
               <ts e="T126" id="Seg_1395" n="e" s="T125">ölüːhübüt!" </ts>
               <ts e="T127" id="Seg_1397" n="e" s="T126">Hübehit </ts>
               <ts e="T128" id="Seg_1399" n="e" s="T127">ɨjɨtar: </ts>
               <ts e="T129" id="Seg_1401" n="e" s="T128">"Dʼe </ts>
               <ts e="T130" id="Seg_1403" n="e" s="T129">kaskɨt </ts>
               <ts e="T131" id="Seg_1405" n="e" s="T130">höbüleːte?" </ts>
               <ts e="T132" id="Seg_1407" n="e" s="T131">Kötörüŋ </ts>
               <ts e="T133" id="Seg_1409" n="e" s="T132">barɨta </ts>
               <ts e="T134" id="Seg_1411" n="e" s="T133">höbüleːbit. </ts>
               <ts e="T135" id="Seg_1413" n="e" s="T134">Leŋkej </ts>
               <ts e="T136" id="Seg_1415" n="e" s="T135">emi͡e </ts>
               <ts e="T137" id="Seg_1417" n="e" s="T136">haŋalaːk: </ts>
               <ts e="T138" id="Seg_1419" n="e" s="T137">"Min </ts>
               <ts e="T139" id="Seg_1421" n="e" s="T138">emi͡e </ts>
               <ts e="T140" id="Seg_1423" n="e" s="T139">barabɨn." </ts>
               <ts e="T141" id="Seg_1425" n="e" s="T140">Hübehit </ts>
               <ts e="T142" id="Seg_1427" n="e" s="T141">ɨjɨtar: </ts>
               <ts e="T143" id="Seg_1429" n="e" s="T142">Kaja </ts>
               <ts e="T144" id="Seg_1431" n="e" s="T143">bu </ts>
               <ts e="T145" id="Seg_1433" n="e" s="T144">leŋkej </ts>
               <ts e="T146" id="Seg_1435" n="e" s="T145">emi͡e </ts>
               <ts e="T147" id="Seg_1437" n="e" s="T146">barsaːrɨ </ts>
               <ts e="T148" id="Seg_1439" n="e" s="T147">gɨnar. </ts>
               <ts e="T149" id="Seg_1441" n="e" s="T148">Höbülüːgüt </ts>
               <ts e="T150" id="Seg_1443" n="e" s="T149">du͡o </ts>
               <ts e="T151" id="Seg_1445" n="e" s="T150">gini </ts>
               <ts e="T152" id="Seg_1447" n="e" s="T151">barsarɨn?" </ts>
               <ts e="T153" id="Seg_1449" n="e" s="T152">Onuga </ts>
               <ts e="T154" id="Seg_1451" n="e" s="T153">kötördörüŋ </ts>
               <ts e="T155" id="Seg_1453" n="e" s="T154">barɨta: </ts>
               <ts e="T156" id="Seg_1455" n="e" s="T155">Bihigi </ts>
               <ts e="T157" id="Seg_1457" n="e" s="T156">leŋkej </ts>
               <ts e="T158" id="Seg_1459" n="e" s="T157">barsarɨn </ts>
               <ts e="T159" id="Seg_1461" n="e" s="T158">höbüleːbeppit." </ts>
               <ts e="T160" id="Seg_1463" n="e" s="T159">"Togo </ts>
               <ts e="T161" id="Seg_1465" n="e" s="T160">höbüleːbekkit? </ts>
               <ts e="T162" id="Seg_1467" n="e" s="T161">Hɨrajdaːk-karagar </ts>
               <ts e="T163" id="Seg_1469" n="e" s="T162">haŋarɨŋ </ts>
               <ts e="T164" id="Seg_1471" n="e" s="T163">ginini." </ts>
               <ts e="T165" id="Seg_1473" n="e" s="T164">Onuga </ts>
               <ts e="T166" id="Seg_1475" n="e" s="T165">kötör </ts>
               <ts e="T167" id="Seg_1477" n="e" s="T166">barɨta </ts>
               <ts e="T168" id="Seg_1479" n="e" s="T167">di͡ete: </ts>
               <ts e="T169" id="Seg_1481" n="e" s="T168">Munna </ts>
               <ts e="T170" id="Seg_1483" n="e" s="T169">bu͡ollagɨn </ts>
               <ts e="T171" id="Seg_1485" n="e" s="T170">leŋkej </ts>
               <ts e="T172" id="Seg_1487" n="e" s="T171">iːtillen </ts>
               <ts e="T173" id="Seg_1489" n="e" s="T172">oloror </ts>
               <ts e="T174" id="Seg_1491" n="e" s="T173">bihigi </ts>
               <ts e="T175" id="Seg_1493" n="e" s="T174">ere </ts>
               <ts e="T176" id="Seg_1495" n="e" s="T175">kergemmitinen </ts>
               <ts e="T177" id="Seg_1497" n="e" s="T176">ahɨlɨktanan. </ts>
               <ts e="T178" id="Seg_1499" n="e" s="T177">Onno </ts>
               <ts e="T179" id="Seg_1501" n="e" s="T178">bardagɨna </ts>
               <ts e="T180" id="Seg_1503" n="e" s="T179">da, </ts>
               <ts e="T181" id="Seg_1505" n="e" s="T180">ol </ts>
               <ts e="T182" id="Seg_1507" n="e" s="T181">mi͡eretin </ts>
               <ts e="T183" id="Seg_1509" n="e" s="T182">hin </ts>
               <ts e="T184" id="Seg_1511" n="e" s="T183">keːhi͡e </ts>
               <ts e="T185" id="Seg_1513" n="e" s="T184">hu͡oga. </ts>
               <ts e="T186" id="Seg_1515" n="e" s="T185">Bihigi </ts>
               <ts e="T187" id="Seg_1517" n="e" s="T186">ippeppit!" </ts>
               <ts e="T188" id="Seg_1519" n="e" s="T187">Hübehit </ts>
               <ts e="T189" id="Seg_1521" n="e" s="T188">diːr </ts>
               <ts e="T190" id="Seg_1523" n="e" s="T189">leŋkejge: </ts>
               <ts e="T191" id="Seg_1525" n="e" s="T190">"Kör, </ts>
               <ts e="T192" id="Seg_1527" n="e" s="T191">dʼon </ts>
               <ts e="T193" id="Seg_1529" n="e" s="T192">poru͡oktaːta. </ts>
               <ts e="T194" id="Seg_1531" n="e" s="T193">Kötör </ts>
               <ts e="T195" id="Seg_1533" n="e" s="T194">barɨta </ts>
               <ts e="T196" id="Seg_1535" n="e" s="T195">haŋarda — </ts>
               <ts e="T197" id="Seg_1537" n="e" s="T196">ol </ts>
               <ts e="T198" id="Seg_1539" n="e" s="T197">hoku͡on. </ts>
               <ts e="T199" id="Seg_1541" n="e" s="T198">Bu </ts>
               <ts e="T200" id="Seg_1543" n="e" s="T199">hirge </ts>
               <ts e="T201" id="Seg_1545" n="e" s="T200">kaːlagɨn, </ts>
               <ts e="T202" id="Seg_1547" n="e" s="T201">bagar, </ts>
               <ts e="T203" id="Seg_1549" n="e" s="T202">toŋon </ts>
               <ts e="T204" id="Seg_1551" n="e" s="T203">öl. </ts>
               <ts e="T205" id="Seg_1553" n="e" s="T204">Kajdak </ts>
               <ts e="T206" id="Seg_1555" n="e" s="T205">hanɨːgɨn?" </ts>
               <ts e="T207" id="Seg_1557" n="e" s="T206">Leŋkej: </ts>
               <ts e="T208" id="Seg_1559" n="e" s="T207">"Mökküjbeppin. </ts>
               <ts e="T209" id="Seg_1561" n="e" s="T208">Hogotok </ts>
               <ts e="T210" id="Seg_1563" n="e" s="T209">duša </ts>
               <ts e="T211" id="Seg_1565" n="e" s="T210">kanna </ts>
               <ts e="T212" id="Seg_1567" n="e" s="T211">ölböt — </ts>
               <ts e="T213" id="Seg_1569" n="e" s="T212">bagar </ts>
               <ts e="T214" id="Seg_1571" n="e" s="T213">ordu͡om. </ts>
               <ts e="T215" id="Seg_1573" n="e" s="T214">Kaːlabɨn", </ts>
               <ts e="T216" id="Seg_1575" n="e" s="T215">diːr. </ts>
               <ts e="T217" id="Seg_1577" n="e" s="T216">Kötördör </ts>
               <ts e="T218" id="Seg_1579" n="e" s="T217">barɨ </ts>
               <ts e="T219" id="Seg_1581" n="e" s="T218">kötüteleːn </ts>
               <ts e="T220" id="Seg_1583" n="e" s="T219">kaːlbɨttar. </ts>
               <ts e="T221" id="Seg_1585" n="e" s="T220">Onuga </ts>
               <ts e="T222" id="Seg_1587" n="e" s="T221">leŋkej </ts>
               <ts e="T223" id="Seg_1589" n="e" s="T222">emeːksiniger </ts>
               <ts e="T224" id="Seg_1591" n="e" s="T223">geler: </ts>
               <ts e="T225" id="Seg_1593" n="e" s="T224">"Kaja-keː, </ts>
               <ts e="T226" id="Seg_1595" n="e" s="T225">bihigini </ts>
               <ts e="T227" id="Seg_1597" n="e" s="T226">ilpetter. </ts>
               <ts e="T228" id="Seg_1599" n="e" s="T227">Mini͡eke </ts>
               <ts e="T229" id="Seg_1601" n="e" s="T228">en </ts>
               <ts e="T230" id="Seg_1603" n="e" s="T229">köp </ts>
               <ts e="T231" id="Seg_1605" n="e" s="T230">mɨjaːn </ts>
               <ts e="T232" id="Seg_1607" n="e" s="T231">larga </ts>
               <ts e="T233" id="Seg_1609" n="e" s="T232">hokujda </ts>
               <ts e="T234" id="Seg_1611" n="e" s="T233">tik. </ts>
               <ts e="T235" id="Seg_1613" n="e" s="T234">Purgaːga </ts>
               <ts e="T236" id="Seg_1615" n="e" s="T235">da </ts>
               <ts e="T237" id="Seg_1617" n="e" s="T236">tibillibet, </ts>
               <ts e="T238" id="Seg_1619" n="e" s="T237">tɨmnɨːga </ts>
               <ts e="T239" id="Seg_1621" n="e" s="T238">da </ts>
               <ts e="T240" id="Seg_1623" n="e" s="T239">toŋmot </ts>
               <ts e="T241" id="Seg_1625" n="e" s="T240">gɨna." </ts>
               <ts e="T242" id="Seg_1627" n="e" s="T241">Emeːksine </ts>
               <ts e="T243" id="Seg_1629" n="e" s="T242">"tigi͡ektibin" </ts>
               <ts e="T244" id="Seg_1631" n="e" s="T243">diːr. </ts>
               <ts e="T245" id="Seg_1633" n="e" s="T244">Kaːrɨ </ts>
               <ts e="T246" id="Seg_1635" n="e" s="T245">gɨtta </ts>
               <ts e="T247" id="Seg_1637" n="e" s="T246">biːr </ts>
               <ts e="T248" id="Seg_1639" n="e" s="T247">čeːlkeː </ts>
               <ts e="T249" id="Seg_1641" n="e" s="T248">bu͡ollun </ts>
               <ts e="T250" id="Seg_1643" n="e" s="T249">ol </ts>
               <ts e="T251" id="Seg_1645" n="e" s="T250">hokuj!" </ts>
               <ts e="T252" id="Seg_1647" n="e" s="T251">Emeːksine, </ts>
               <ts e="T253" id="Seg_1649" n="e" s="T252">uːs </ts>
               <ts e="T254" id="Seg_1651" n="e" s="T253">mujaːn, </ts>
               <ts e="T255" id="Seg_1653" n="e" s="T254">tikpit, </ts>
               <ts e="T256" id="Seg_1655" n="e" s="T255">kaːs </ts>
               <ts e="T257" id="Seg_1657" n="e" s="T256">gi͡enineːger </ts>
               <ts e="T258" id="Seg_1659" n="e" s="T257">ürüŋ </ts>
               <ts e="T259" id="Seg_1661" n="e" s="T258">čeːlkeː </ts>
               <ts e="T260" id="Seg_1663" n="e" s="T259">hokuju. </ts>
               <ts e="T261" id="Seg_1665" n="e" s="T260">Leŋkej </ts>
               <ts e="T262" id="Seg_1667" n="e" s="T261">bunu </ts>
               <ts e="T263" id="Seg_1669" n="e" s="T262">keppite — </ts>
               <ts e="T264" id="Seg_1671" n="e" s="T263">hörü höp, </ts>
               <ts e="T265" id="Seg_1673" n="e" s="T264">hokuja </ts>
               <ts e="T266" id="Seg_1675" n="e" s="T265">etiger </ts>
               <ts e="T267" id="Seg_1677" n="e" s="T266">erije </ts>
               <ts e="T268" id="Seg_1679" n="e" s="T267">kappɨt, </ts>
               <ts e="T269" id="Seg_1681" n="e" s="T268">tɨmnɨːga </ts>
               <ts e="T270" id="Seg_1683" n="e" s="T269">da </ts>
               <ts e="T271" id="Seg_1685" n="e" s="T270">toŋmot, </ts>
               <ts e="T272" id="Seg_1687" n="e" s="T271">purgaːga </ts>
               <ts e="T273" id="Seg_1689" n="e" s="T272">da </ts>
               <ts e="T274" id="Seg_1691" n="e" s="T273">hɨstɨbat, </ts>
               <ts e="T275" id="Seg_1693" n="e" s="T274">kaːr </ts>
               <ts e="T276" id="Seg_1695" n="e" s="T275">da </ts>
               <ts e="T277" id="Seg_1697" n="e" s="T276">hɨstɨbat </ts>
               <ts e="T278" id="Seg_1699" n="e" s="T277">gini͡eke. </ts>
               <ts e="T279" id="Seg_1701" n="e" s="T278">Leŋkej </ts>
               <ts e="T280" id="Seg_1703" n="e" s="T279">hoččotton </ts>
               <ts e="T281" id="Seg_1705" n="e" s="T280">bu </ts>
               <ts e="T282" id="Seg_1707" n="e" s="T281">hirge </ts>
               <ts e="T283" id="Seg_1709" n="e" s="T282">dojdulammɨta. </ts>
               <ts e="T284" id="Seg_1711" n="e" s="T283">Ol </ts>
               <ts e="T285" id="Seg_1713" n="e" s="T284">kaːlbɨtɨn </ts>
               <ts e="T286" id="Seg_1715" n="e" s="T285">öhü͡ömčüleːn </ts>
               <ts e="T287" id="Seg_1717" n="e" s="T286">leŋkej </ts>
               <ts e="T288" id="Seg_1719" n="e" s="T287">kɨra </ts>
               <ts e="T289" id="Seg_1721" n="e" s="T288">kötörü </ts>
               <ts e="T290" id="Seg_1723" n="e" s="T289">barɨtɨn </ts>
               <ts e="T291" id="Seg_1725" n="e" s="T290">dössü͡ö </ts>
               <ts e="T292" id="Seg_1727" n="e" s="T291">hiːr </ts>
               <ts e="T293" id="Seg_1729" n="e" s="T292">bu͡olbuta. </ts>
               <ts e="T294" id="Seg_1731" n="e" s="T293">Orduk </ts>
               <ts e="T295" id="Seg_1733" n="e" s="T294">ginini </ts>
               <ts e="T296" id="Seg_1735" n="e" s="T295">kɨtta </ts>
               <ts e="T297" id="Seg_1737" n="e" s="T296">kɨstɨːr </ts>
               <ts e="T298" id="Seg_1739" n="e" s="T297">kurpaːskɨnɨ. </ts>
               <ts e="T299" id="Seg_1741" n="e" s="T298">Elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T11" id="Seg_1742" s="T0">ErSV_1964_SnowOwl_flk.001</ta>
            <ta e="T17" id="Seg_1743" s="T11">ErSV_1964_SnowOwl_flk.002</ta>
            <ta e="T22" id="Seg_1744" s="T17">ErSV_1964_SnowOwl_flk.003</ta>
            <ta e="T26" id="Seg_1745" s="T22">ErSV_1964_SnowOwl_flk.004</ta>
            <ta e="T30" id="Seg_1746" s="T26">ErSV_1964_SnowOwl_flk.005</ta>
            <ta e="T39" id="Seg_1747" s="T30">ErSV_1964_SnowOwl_flk.006</ta>
            <ta e="T49" id="Seg_1748" s="T39">ErSV_1964_SnowOwl_flk.007</ta>
            <ta e="T55" id="Seg_1749" s="T49">ErSV_1964_SnowOwl_flk.008</ta>
            <ta e="T58" id="Seg_1750" s="T55">ErSV_1964_SnowOwl_flk.009</ta>
            <ta e="T60" id="Seg_1751" s="T58">ErSV_1964_SnowOwl_flk.010</ta>
            <ta e="T63" id="Seg_1752" s="T60">ErSV_1964_SnowOwl_flk.011</ta>
            <ta e="T68" id="Seg_1753" s="T63">ErSV_1964_SnowOwl_flk.012</ta>
            <ta e="T77" id="Seg_1754" s="T68">ErSV_1964_SnowOwl_flk.013</ta>
            <ta e="T84" id="Seg_1755" s="T77">ErSV_1964_SnowOwl_flk.014</ta>
            <ta e="T87" id="Seg_1756" s="T84">ErSV_1964_SnowOwl_flk.015</ta>
            <ta e="T94" id="Seg_1757" s="T87">ErSV_1964_SnowOwl_flk.016</ta>
            <ta e="T101" id="Seg_1758" s="T94">ErSV_1964_SnowOwl_flk.017</ta>
            <ta e="T108" id="Seg_1759" s="T101">ErSV_1964_SnowOwl_flk.018</ta>
            <ta e="T113" id="Seg_1760" s="T108">ErSV_1964_SnowOwl_flk.019</ta>
            <ta e="T116" id="Seg_1761" s="T113">ErSV_1964_SnowOwl_flk.020</ta>
            <ta e="T119" id="Seg_1762" s="T116">ErSV_1964_SnowOwl_flk.021</ta>
            <ta e="T121" id="Seg_1763" s="T119">ErSV_1964_SnowOwl_flk.022</ta>
            <ta e="T124" id="Seg_1764" s="T121">ErSV_1964_SnowOwl_flk.023</ta>
            <ta e="T126" id="Seg_1765" s="T124">ErSV_1964_SnowOwl_flk.024</ta>
            <ta e="T128" id="Seg_1766" s="T126">ErSV_1964_SnowOwl_flk.025</ta>
            <ta e="T131" id="Seg_1767" s="T128">ErSV_1964_SnowOwl_flk.026</ta>
            <ta e="T134" id="Seg_1768" s="T131">ErSV_1964_SnowOwl_flk.027</ta>
            <ta e="T137" id="Seg_1769" s="T134">ErSV_1964_SnowOwl_flk.028</ta>
            <ta e="T140" id="Seg_1770" s="T137">ErSV_1964_SnowOwl_flk.029</ta>
            <ta e="T142" id="Seg_1771" s="T140">ErSV_1964_SnowOwl_flk.030</ta>
            <ta e="T148" id="Seg_1772" s="T142">ErSV_1964_SnowOwl_flk.031</ta>
            <ta e="T152" id="Seg_1773" s="T148">ErSV_1964_SnowOwl_flk.032</ta>
            <ta e="T155" id="Seg_1774" s="T152">ErSV_1964_SnowOwl_flk.033</ta>
            <ta e="T159" id="Seg_1775" s="T155">ErSV_1964_SnowOwl_flk.034</ta>
            <ta e="T161" id="Seg_1776" s="T159">ErSV_1964_SnowOwl_flk.035</ta>
            <ta e="T164" id="Seg_1777" s="T161">ErSV_1964_SnowOwl_flk.036</ta>
            <ta e="T168" id="Seg_1778" s="T164">ErSV_1964_SnowOwl_flk.037</ta>
            <ta e="T177" id="Seg_1779" s="T168">ErSV_1964_SnowOwl_flk.038</ta>
            <ta e="T185" id="Seg_1780" s="T177">ErSV_1964_SnowOwl_flk.039</ta>
            <ta e="T187" id="Seg_1781" s="T185">ErSV_1964_SnowOwl_flk.040</ta>
            <ta e="T190" id="Seg_1782" s="T187">ErSV_1964_SnowOwl_flk.041</ta>
            <ta e="T193" id="Seg_1783" s="T190">ErSV_1964_SnowOwl_flk.042</ta>
            <ta e="T198" id="Seg_1784" s="T193">ErSV_1964_SnowOwl_flk.043</ta>
            <ta e="T204" id="Seg_1785" s="T198">ErSV_1964_SnowOwl_flk.044</ta>
            <ta e="T206" id="Seg_1786" s="T204">ErSV_1964_SnowOwl_flk.045</ta>
            <ta e="T207" id="Seg_1787" s="T206">ErSV_1964_SnowOwl_flk.046</ta>
            <ta e="T208" id="Seg_1788" s="T207">ErSV_1964_SnowOwl_flk.047</ta>
            <ta e="T214" id="Seg_1789" s="T208">ErSV_1964_SnowOwl_flk.048</ta>
            <ta e="T216" id="Seg_1790" s="T214">ErSV_1964_SnowOwl_flk.049</ta>
            <ta e="T220" id="Seg_1791" s="T216">ErSV_1964_SnowOwl_flk.050</ta>
            <ta e="T224" id="Seg_1792" s="T220">ErSV_1964_SnowOwl_flk.051</ta>
            <ta e="T227" id="Seg_1793" s="T224">ErSV_1964_SnowOwl_flk.052</ta>
            <ta e="T234" id="Seg_1794" s="T227">ErSV_1964_SnowOwl_flk.053</ta>
            <ta e="T241" id="Seg_1795" s="T234">ErSV_1964_SnowOwl_flk.054</ta>
            <ta e="T244" id="Seg_1796" s="T241">ErSV_1964_SnowOwl_flk.055</ta>
            <ta e="T251" id="Seg_1797" s="T244">ErSV_1964_SnowOwl_flk.056</ta>
            <ta e="T260" id="Seg_1798" s="T251">ErSV_1964_SnowOwl_flk.057</ta>
            <ta e="T278" id="Seg_1799" s="T260">ErSV_1964_SnowOwl_flk.058</ta>
            <ta e="T283" id="Seg_1800" s="T278">ErSV_1964_SnowOwl_flk.059</ta>
            <ta e="T293" id="Seg_1801" s="T283">ErSV_1964_SnowOwl_flk.060</ta>
            <ta e="T298" id="Seg_1802" s="T293">ErSV_1964_SnowOwl_flk.061</ta>
            <ta e="T299" id="Seg_1803" s="T298">ErSV_1964_SnowOwl_flk.062</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T11" id="Seg_1804" s="T0">Былыр кааһыҥ, куһуҥ, чиичаагыҥ, лэҥкэйиҥ — бары көтөр бу һиргэ һирдээк этэ.</ta>
            <ta e="T17" id="Seg_1805" s="T11">Бу һириҥ ол кэмҥэ ичигэс үһү.</ta>
            <ta e="T22" id="Seg_1806" s="T17">Онтон тымныылар буоллулар, дьыл-күн уларыйда.</ta>
            <ta e="T26" id="Seg_1807" s="T22">Мунуга көтөрүҥ барыта һүбэлэһэр.</ta>
            <ta e="T30" id="Seg_1808" s="T26">Барыларыгар биир һүбэһит баар.</ta>
            <ta e="T39" id="Seg_1809" s="T30">Лэҥкэйиҥ һоччоттон курпааскыны һиэччи, куһу буоллун, кааһы буоллун эмиэ.</ta>
            <ta e="T49" id="Seg_1810" s="T39">Бу көтөрдөр һүбэһиттэрэ (гини каастартан этэ) көтөрү барытын мунньакка ыгырар.</ta>
            <ta e="T55" id="Seg_1811" s="T49">Биир алыыны, һиһи толору көтөр муһунна.</ta>
            <ta e="T58" id="Seg_1812" s="T55">Лэҥкэйиҥ эмиэ кэлбит.</ta>
            <ta e="T60" id="Seg_1813" s="T58">Һүбэһит этэр: </ta>
            <ta e="T63" id="Seg_1814" s="T60">— Дьэ, дьыл-күн уларыйда.</ta>
            <ta e="T68" id="Seg_1815" s="T63">Биһиги һайынын агай олорор буоллубут.</ta>
            <ta e="T77" id="Seg_1816" s="T68">Мин билэбин: каннык эрэ һиргэ кыһын буолбат дойдута баар.</ta>
            <ta e="T84" id="Seg_1817" s="T77">Биһиги, кынаттаактар, того бу һиргэ тоҥо һытыакпытый?!</ta>
            <ta e="T87" id="Seg_1818" s="T84">һиринэн бардакка ыраак.</ta>
            <ta e="T94" id="Seg_1819" s="T87">Биһиги курдары ол һиргэ көтүөгүҥ кыһын кэлиитэ.</ta>
            <ta e="T101" id="Seg_1820" s="T94">Бу тымныыбыт ааһыар диэри ол һиргэ ологуруогуҥ.</ta>
            <ta e="T108" id="Seg_1821" s="T101">Мунна олордокпутуна биһиги бары һабыллыакпыт, тоҥон өлүөкпүт.</ta>
            <ta e="T113" id="Seg_1822" s="T108">Онно көттөкпүтүнэ бары киһи буолуокпут.</ta>
            <ta e="T116" id="Seg_1823" s="T113">Дьэ һүбэгитин биэриҥ!</ta>
            <ta e="T119" id="Seg_1824" s="T116">Онуга көтөр барыта: </ta>
            <ta e="T121" id="Seg_1825" s="T119">— Дьэ кирдик!</ta>
            <ta e="T124" id="Seg_1826" s="T121">Биһиги бары көтүөкпүт.</ta>
            <ta e="T126" id="Seg_1827" s="T124">Мунна өлүүһүбүт!</ta>
            <ta e="T128" id="Seg_1828" s="T126">Һүбэһит ыйытар: </ta>
            <ta e="T131" id="Seg_1829" s="T128">— Дьэ каскыт һөбүлээтэ?</ta>
            <ta e="T134" id="Seg_1830" s="T131">Көтөрүҥ барыта һөбүлээбит.</ta>
            <ta e="T137" id="Seg_1831" s="T134">Лэҥкэй эмиэ һаҥалаак: </ta>
            <ta e="T140" id="Seg_1832" s="T137">— Мин эмиэ барабын.</ta>
            <ta e="T142" id="Seg_1833" s="T140">Һүбэһит ыйытар: </ta>
            <ta e="T148" id="Seg_1834" s="T142">— Кайа бу лэҥкэй эмиэ барсаары гынар.</ta>
            <ta e="T152" id="Seg_1835" s="T148">Һөбүлүүгүт дуо гини барсарын?</ta>
            <ta e="T155" id="Seg_1836" s="T152">Онуга көтөрдөрүҥ барыта: </ta>
            <ta e="T159" id="Seg_1837" s="T155">— Биһиги лэҥкэй барсарын һөбүлээбэппит.</ta>
            <ta e="T161" id="Seg_1838" s="T159">— Того һөбүлээбэккит?</ta>
            <ta e="T164" id="Seg_1839" s="T161">Һырайдаак-карагар һаҥарыҥ гинини.</ta>
            <ta e="T168" id="Seg_1840" s="T164">Онуга көтөр барыта диэтэ: </ta>
            <ta e="T177" id="Seg_1841" s="T168">— Мунна буоллагын лэҥкэй иитиллэн олорор биһиги эрэ кэргэммитинэн аһылыктанан.</ta>
            <ta e="T185" id="Seg_1842" s="T177">Онно бардагына да, ол миэрэтин һин кээһиэ һуога.</ta>
            <ta e="T187" id="Seg_1843" s="T185">Биһиги иппэппит!</ta>
            <ta e="T190" id="Seg_1844" s="T187">Һүбэһит диир лэҥкэйгэ: </ta>
            <ta e="T193" id="Seg_1845" s="T190">— Көр: дьон поруоктаата.</ta>
            <ta e="T198" id="Seg_1846" s="T193">Көтөр барыта һаҥарда — ол һокуон.</ta>
            <ta e="T204" id="Seg_1847" s="T198">Бу һиргэ каалагын, багар, тоҥон өл.</ta>
            <ta e="T206" id="Seg_1848" s="T204">Кайдак һаныыгын?</ta>
            <ta e="T207" id="Seg_1849" s="T206">Лэҥкэй: </ta>
            <ta e="T208" id="Seg_1850" s="T207">— Мөккүйбэппин.</ta>
            <ta e="T214" id="Seg_1851" s="T208">Һоготок душа канна өлбөт — багар ордуом.</ta>
            <ta e="T216" id="Seg_1852" s="T214">Каалабын, — диир.</ta>
            <ta e="T220" id="Seg_1853" s="T216">Көтөрдөр бары көтүтэлээн каалбыттар.</ta>
            <ta e="T224" id="Seg_1854" s="T220">Онуга лэҥкэй эмээксинигэр гэлэр: </ta>
            <ta e="T227" id="Seg_1855" s="T224">— Кайа-кээ, биһигини илпэттэр.</ta>
            <ta e="T234" id="Seg_1856" s="T227">Миниэкэ эн көп мыйаан ларга һокуйда тик.</ta>
            <ta e="T241" id="Seg_1857" s="T234">Пургаага да тибиллибэт, тымныыга да тоҥмот гына.</ta>
            <ta e="T244" id="Seg_1858" s="T241">Эмээксинэ: — Тигиэктибин! — диир.</ta>
            <ta e="T251" id="Seg_1859" s="T244">— Каары гытта биир чээлкээ буоллун ол һокуй!</ta>
            <ta e="T260" id="Seg_1860" s="T251">Эмээксинэ, уус муйаан, тикпит: каас гиэнинээгэр үрүҥ чээлкээ һокуйу.</ta>
            <ta e="T278" id="Seg_1861" s="T260">Лэҥкэй буну кэппитэ — һөрү-һөп, һокуйа этигэр эрийэ каппыт, тымныыга да тоҥмот, пургаага да һыстыбат, каар да һыстыбат гиниэкэ.</ta>
            <ta e="T283" id="Seg_1862" s="T278">Лэҥкэй һоччоттон бу һиргэ дойдуламмыта.</ta>
            <ta e="T293" id="Seg_1863" s="T283">Ол каалбытын өһүөмчүлээн лэҥкэй кыра көтөрү барытын дөссүө һиир буолбута.</ta>
            <ta e="T298" id="Seg_1864" s="T293">Ордук гинини кытта кыстыыр курпааскыны.</ta>
            <ta e="T299" id="Seg_1865" s="T298">Элэтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T11" id="Seg_1866" s="T0">Bɨlɨr kaːhɨŋ, kuhuŋ, čiːčaːgɨŋ, leŋkejiŋ — barɨ kötör bu hirge hirdeːk ete. </ta>
            <ta e="T17" id="Seg_1867" s="T11">Bu hiriŋ ol kemŋe ičiges ühü. </ta>
            <ta e="T22" id="Seg_1868" s="T17">Onton tɨmnɨːlar bu͡ollular, dʼɨl-kün ularɨjda. </ta>
            <ta e="T26" id="Seg_1869" s="T22">Munuga kötörüŋ barɨta hübeleher. </ta>
            <ta e="T30" id="Seg_1870" s="T26">Barɨlarɨgar biːr hübehit baːr. </ta>
            <ta e="T39" id="Seg_1871" s="T30">Leŋkejiŋ hoččotton kurpaːskɨnɨ hi͡ečči, kuhu bu͡ollun, kaːhɨ bu͡ollun emi͡e. </ta>
            <ta e="T49" id="Seg_1872" s="T39">Bu kötördör hübehittere (gini kaːstartan ete) kötörü barɨtɨn munnʼakka ɨgɨrar. </ta>
            <ta e="T55" id="Seg_1873" s="T49">Biːr alɨːnɨ, hihi toloru kötör muhunna. </ta>
            <ta e="T58" id="Seg_1874" s="T55">Leŋkejiŋ emi͡e kelbit. </ta>
            <ta e="T60" id="Seg_1875" s="T58">Hübehit eter: </ta>
            <ta e="T63" id="Seg_1876" s="T60">"Dʼe, dʼɨl-kün ularɨjda. </ta>
            <ta e="T68" id="Seg_1877" s="T63">Bihigi hajɨnɨn agaj oloror bu͡ollubut. </ta>
            <ta e="T77" id="Seg_1878" s="T68">Min bilebin, kannɨk ere hirge kɨhɨn bu͡olbat dojduta baːr. </ta>
            <ta e="T84" id="Seg_1879" s="T77">Bihigi, kɨnattaːktar, togo bu hirge toŋo hɨtɨ͡akpɨtɨj? </ta>
            <ta e="T87" id="Seg_1880" s="T84">Hirinen bardakka ɨraːk. </ta>
            <ta e="T94" id="Seg_1881" s="T87">Bihigi kurdarɨ ol hirge kötü͡ögüŋ kɨhɨn keliːte. </ta>
            <ta e="T101" id="Seg_1882" s="T94">Bu tɨmnɨːbɨt aːhɨ͡ar di͡eri ol hirge ologuru͡oguŋ. </ta>
            <ta e="T108" id="Seg_1883" s="T101">Munna olordokputuna bihigi barɨ habɨllɨ͡akpɨt, toŋon ölü͡ökpüt. </ta>
            <ta e="T113" id="Seg_1884" s="T108">Onno köttökpütüne barɨ kihi bu͡olu͡okput. </ta>
            <ta e="T116" id="Seg_1885" s="T113">Dʼe hübegitin bi͡eriŋ!" </ta>
            <ta e="T119" id="Seg_1886" s="T116">Onuga kötör barɨta: </ta>
            <ta e="T121" id="Seg_1887" s="T119">"Dʼe kirdik! </ta>
            <ta e="T124" id="Seg_1888" s="T121">Bihigi barɨ kötü͡ökpüt. </ta>
            <ta e="T126" id="Seg_1889" s="T124">Munna ölüːhübüt!" </ta>
            <ta e="T128" id="Seg_1890" s="T126">Hübehit ɨjɨtar: </ta>
            <ta e="T131" id="Seg_1891" s="T128">"Dʼe kaskɨt höbüleːte?" </ta>
            <ta e="T134" id="Seg_1892" s="T131">Kötörüŋ barɨta höbüleːbit. </ta>
            <ta e="T137" id="Seg_1893" s="T134">Leŋkej emi͡e haŋalaːk: </ta>
            <ta e="T140" id="Seg_1894" s="T137">"Min emi͡e barabɨn." </ta>
            <ta e="T142" id="Seg_1895" s="T140">Hübehit ɨjɨtar: </ta>
            <ta e="T148" id="Seg_1896" s="T142">"Kaja bu leŋkej emi͡e barsaːrɨ gɨnar. </ta>
            <ta e="T152" id="Seg_1897" s="T148">Höbülüːgüt du͡o gini barsarɨn?" </ta>
            <ta e="T155" id="Seg_1898" s="T152">Onuga kötördörüŋ barɨta: </ta>
            <ta e="T159" id="Seg_1899" s="T155">"Bihigi leŋkej barsarɨn höbüleːbeppit." </ta>
            <ta e="T161" id="Seg_1900" s="T159">"Togo höbüleːbekkit? </ta>
            <ta e="T164" id="Seg_1901" s="T161">Hɨrajdaːk-karagar haŋarɨŋ ginini." </ta>
            <ta e="T168" id="Seg_1902" s="T164">Onuga kötör barɨta di͡ete: </ta>
            <ta e="T177" id="Seg_1903" s="T168">"Munna bu͡ollagɨn leŋkej iːtillen oloror bihigi ere kergemmitinen ahɨlɨktanan. </ta>
            <ta e="T185" id="Seg_1904" s="T177">Onno bardagɨna da, ol mi͡eretin hin keːhi͡e hu͡oga. </ta>
            <ta e="T187" id="Seg_1905" s="T185">Bihigi ippeppit!" </ta>
            <ta e="T190" id="Seg_1906" s="T187">Hübehit diːr leŋkejge: </ta>
            <ta e="T193" id="Seg_1907" s="T190">"Kör, dʼon poru͡oktaːta. </ta>
            <ta e="T198" id="Seg_1908" s="T193">Kötör barɨta haŋarda — ol hoku͡on. </ta>
            <ta e="T204" id="Seg_1909" s="T198">Bu hirge kaːlagɨn, bagar, toŋon öl. </ta>
            <ta e="T206" id="Seg_1910" s="T204">Kajdak hanɨːgɨn?" </ta>
            <ta e="T207" id="Seg_1911" s="T206">Leŋkej: </ta>
            <ta e="T208" id="Seg_1912" s="T207">"Mökküjbeppin. </ta>
            <ta e="T214" id="Seg_1913" s="T208">Hogotok duša kanna ölböt — bagar ordu͡om. </ta>
            <ta e="T216" id="Seg_1914" s="T214">Kaːlabɨn", diːr. </ta>
            <ta e="T220" id="Seg_1915" s="T216">Kötördör barɨ kötüteleːn kaːlbɨttar. </ta>
            <ta e="T224" id="Seg_1916" s="T220">Onuga leŋkej emeːksiniger geler: </ta>
            <ta e="T227" id="Seg_1917" s="T224">"Kaja-keː, bihigini ilpetter. </ta>
            <ta e="T234" id="Seg_1918" s="T227">Mini͡eke en köp mɨjaːn larga hokujda tik. </ta>
            <ta e="T241" id="Seg_1919" s="T234">Purgaːga da tibillibet, tɨmnɨːga da toŋmot gɨna." </ta>
            <ta e="T244" id="Seg_1920" s="T241">Emeːksine "tigi͡ektibin" diːr. </ta>
            <ta e="T251" id="Seg_1921" s="T244">"Kaːrɨ gɨtta biːr čeːlkeː bu͡ollun ol hokuj!" </ta>
            <ta e="T260" id="Seg_1922" s="T251">Emeːksine, uːs mujaːn, tikpit, kaːs gi͡enineːger ürüŋ čeːlkeː hokuju. </ta>
            <ta e="T278" id="Seg_1923" s="T260">Leŋkej bunu keppite — hörü höp, hokuja etiger erije kappɨt, tɨmnɨːga da toŋmot, purgaːga da hɨstɨbat, kaːr da hɨstɨbat gini͡eke. </ta>
            <ta e="T283" id="Seg_1924" s="T278">Leŋkej hoččotton bu hirge dojdulammɨta. </ta>
            <ta e="T293" id="Seg_1925" s="T283">Ol kaːlbɨtɨn öhü͡ömčüleːn leŋkej kɨra kötörü barɨtɨn dössü͡ö hiːr bu͡olbuta. </ta>
            <ta e="T298" id="Seg_1926" s="T293">Orduk ginini kɨtta kɨstɨːr kurpaːskɨnɨ. </ta>
            <ta e="T299" id="Seg_1927" s="T298">Elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1928" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_1929" s="T1">kaːh-ɨ-ŋ</ta>
            <ta e="T3" id="Seg_1930" s="T2">kuh-u-ŋ</ta>
            <ta e="T4" id="Seg_1931" s="T3">čiːčaːg-ɨ-ŋ</ta>
            <ta e="T5" id="Seg_1932" s="T4">leŋkej-i-ŋ</ta>
            <ta e="T6" id="Seg_1933" s="T5">barɨ</ta>
            <ta e="T7" id="Seg_1934" s="T6">kötör</ta>
            <ta e="T8" id="Seg_1935" s="T7">bu</ta>
            <ta e="T9" id="Seg_1936" s="T8">hir-ge</ta>
            <ta e="T10" id="Seg_1937" s="T9">hir-deːk</ta>
            <ta e="T11" id="Seg_1938" s="T10">e-t-e</ta>
            <ta e="T12" id="Seg_1939" s="T11">bu</ta>
            <ta e="T13" id="Seg_1940" s="T12">hir-i-ŋ</ta>
            <ta e="T14" id="Seg_1941" s="T13">ol</ta>
            <ta e="T15" id="Seg_1942" s="T14">kem-ŋe</ta>
            <ta e="T16" id="Seg_1943" s="T15">ičiges</ta>
            <ta e="T17" id="Seg_1944" s="T16">ühü</ta>
            <ta e="T18" id="Seg_1945" s="T17">onton</ta>
            <ta e="T19" id="Seg_1946" s="T18">tɨmnɨː-lar</ta>
            <ta e="T20" id="Seg_1947" s="T19">bu͡ol-lu-lar</ta>
            <ta e="T21" id="Seg_1948" s="T20">dʼɨl-kün</ta>
            <ta e="T22" id="Seg_1949" s="T21">ularɨj-d-a</ta>
            <ta e="T23" id="Seg_1950" s="T22">munu-ga</ta>
            <ta e="T24" id="Seg_1951" s="T23">kötör-ü-ŋ</ta>
            <ta e="T25" id="Seg_1952" s="T24">barɨ-ta</ta>
            <ta e="T26" id="Seg_1953" s="T25">hübel-e-h-er</ta>
            <ta e="T27" id="Seg_1954" s="T26">barɨ-larɨ-gar</ta>
            <ta e="T28" id="Seg_1955" s="T27">biːr</ta>
            <ta e="T29" id="Seg_1956" s="T28">hübehit</ta>
            <ta e="T30" id="Seg_1957" s="T29">baːr</ta>
            <ta e="T31" id="Seg_1958" s="T30">leŋkej-i-ŋ</ta>
            <ta e="T32" id="Seg_1959" s="T31">hoččotton</ta>
            <ta e="T33" id="Seg_1960" s="T32">kurpaːskɨ-nɨ</ta>
            <ta e="T34" id="Seg_1961" s="T33">hi͡e-čči</ta>
            <ta e="T35" id="Seg_1962" s="T34">kuh-u</ta>
            <ta e="T36" id="Seg_1963" s="T35">bu͡ollun</ta>
            <ta e="T37" id="Seg_1964" s="T36">kaːh-ɨ</ta>
            <ta e="T38" id="Seg_1965" s="T37">bu͡ollun</ta>
            <ta e="T39" id="Seg_1966" s="T38">emi͡e</ta>
            <ta e="T40" id="Seg_1967" s="T39">bu</ta>
            <ta e="T41" id="Seg_1968" s="T40">kötör-dör</ta>
            <ta e="T42" id="Seg_1969" s="T41">hübehit-tere</ta>
            <ta e="T43" id="Seg_1970" s="T42">gini</ta>
            <ta e="T44" id="Seg_1971" s="T43">kaːs-tar-tan</ta>
            <ta e="T45" id="Seg_1972" s="T44">e-t-e</ta>
            <ta e="T46" id="Seg_1973" s="T45">kötör-ü</ta>
            <ta e="T47" id="Seg_1974" s="T46">barɨ-tɨ-n</ta>
            <ta e="T48" id="Seg_1975" s="T47">munnʼak-ka</ta>
            <ta e="T49" id="Seg_1976" s="T48">ɨgɨr-ar</ta>
            <ta e="T50" id="Seg_1977" s="T49">biːr</ta>
            <ta e="T51" id="Seg_1978" s="T50">alɨː-nɨ</ta>
            <ta e="T52" id="Seg_1979" s="T51">hih-i</ta>
            <ta e="T53" id="Seg_1980" s="T52">toloru</ta>
            <ta e="T54" id="Seg_1981" s="T53">kötör</ta>
            <ta e="T55" id="Seg_1982" s="T54">muh-u-n-n-a</ta>
            <ta e="T56" id="Seg_1983" s="T55">leŋkej-i-ŋ</ta>
            <ta e="T57" id="Seg_1984" s="T56">emi͡e</ta>
            <ta e="T58" id="Seg_1985" s="T57">kel-bit</ta>
            <ta e="T59" id="Seg_1986" s="T58">hübehit</ta>
            <ta e="T60" id="Seg_1987" s="T59">et-er</ta>
            <ta e="T61" id="Seg_1988" s="T60">dʼe</ta>
            <ta e="T62" id="Seg_1989" s="T61">dʼɨl-kün</ta>
            <ta e="T63" id="Seg_1990" s="T62">ularɨj-d-a</ta>
            <ta e="T64" id="Seg_1991" s="T63">bihigi</ta>
            <ta e="T65" id="Seg_1992" s="T64">hajɨnɨn</ta>
            <ta e="T66" id="Seg_1993" s="T65">agaj</ta>
            <ta e="T67" id="Seg_1994" s="T66">olor-or</ta>
            <ta e="T68" id="Seg_1995" s="T67">bu͡ol-lu-but</ta>
            <ta e="T69" id="Seg_1996" s="T68">min</ta>
            <ta e="T70" id="Seg_1997" s="T69">bil-e-bin</ta>
            <ta e="T71" id="Seg_1998" s="T70">kannɨk</ta>
            <ta e="T72" id="Seg_1999" s="T71">ere</ta>
            <ta e="T73" id="Seg_2000" s="T72">hir-ge</ta>
            <ta e="T74" id="Seg_2001" s="T73">kɨhɨn</ta>
            <ta e="T75" id="Seg_2002" s="T74">bu͡ol-bat</ta>
            <ta e="T76" id="Seg_2003" s="T75">dojdu-ta</ta>
            <ta e="T77" id="Seg_2004" s="T76">baːr</ta>
            <ta e="T78" id="Seg_2005" s="T77">bihigi</ta>
            <ta e="T79" id="Seg_2006" s="T78">kɨnat-taːk-tar</ta>
            <ta e="T80" id="Seg_2007" s="T79">togo</ta>
            <ta e="T81" id="Seg_2008" s="T80">bu</ta>
            <ta e="T82" id="Seg_2009" s="T81">hir-ge</ta>
            <ta e="T83" id="Seg_2010" s="T82">toŋ-o</ta>
            <ta e="T84" id="Seg_2011" s="T83">hɨt-ɨ͡ak-pɨt=ɨj</ta>
            <ta e="T85" id="Seg_2012" s="T84">hir-i-nen</ta>
            <ta e="T86" id="Seg_2013" s="T85">bar-dak-ka</ta>
            <ta e="T87" id="Seg_2014" s="T86">ɨraːk</ta>
            <ta e="T88" id="Seg_2015" s="T87">bihigi</ta>
            <ta e="T89" id="Seg_2016" s="T88">kurdarɨ</ta>
            <ta e="T90" id="Seg_2017" s="T89">ol</ta>
            <ta e="T91" id="Seg_2018" s="T90">hir-ge</ta>
            <ta e="T92" id="Seg_2019" s="T91">köt-ü͡ögüŋ</ta>
            <ta e="T93" id="Seg_2020" s="T92">kɨhɨn</ta>
            <ta e="T94" id="Seg_2021" s="T93">kel-iːt-e</ta>
            <ta e="T95" id="Seg_2022" s="T94">bu</ta>
            <ta e="T96" id="Seg_2023" s="T95">tɨmnɨː-bɨt</ta>
            <ta e="T97" id="Seg_2024" s="T96">aːh-ɨ͡a-r</ta>
            <ta e="T98" id="Seg_2025" s="T97">di͡eri</ta>
            <ta e="T99" id="Seg_2026" s="T98">ol</ta>
            <ta e="T100" id="Seg_2027" s="T99">hir-ge</ta>
            <ta e="T101" id="Seg_2028" s="T100">ologur-u͡oguŋ</ta>
            <ta e="T102" id="Seg_2029" s="T101">munna</ta>
            <ta e="T103" id="Seg_2030" s="T102">olor-dok-putuna</ta>
            <ta e="T104" id="Seg_2031" s="T103">bihigi</ta>
            <ta e="T105" id="Seg_2032" s="T104">barɨ</ta>
            <ta e="T106" id="Seg_2033" s="T105">habɨll-ɨ͡ak-pɨt</ta>
            <ta e="T107" id="Seg_2034" s="T106">toŋ-on</ta>
            <ta e="T108" id="Seg_2035" s="T107">öl-ü͡ök-püt</ta>
            <ta e="T109" id="Seg_2036" s="T108">onno</ta>
            <ta e="T110" id="Seg_2037" s="T109">köt-tök-pütüne</ta>
            <ta e="T111" id="Seg_2038" s="T110">barɨ</ta>
            <ta e="T112" id="Seg_2039" s="T111">kihi</ta>
            <ta e="T113" id="Seg_2040" s="T112">bu͡ol-u͡ok-put</ta>
            <ta e="T114" id="Seg_2041" s="T113">dʼe</ta>
            <ta e="T115" id="Seg_2042" s="T114">hübe-giti-n</ta>
            <ta e="T116" id="Seg_2043" s="T115">bi͡er-i-ŋ</ta>
            <ta e="T117" id="Seg_2044" s="T116">onu-ga</ta>
            <ta e="T118" id="Seg_2045" s="T117">kötör</ta>
            <ta e="T119" id="Seg_2046" s="T118">barɨ-ta</ta>
            <ta e="T120" id="Seg_2047" s="T119">dʼe</ta>
            <ta e="T121" id="Seg_2048" s="T120">kirdik</ta>
            <ta e="T122" id="Seg_2049" s="T121">bihigi</ta>
            <ta e="T123" id="Seg_2050" s="T122">barɨ</ta>
            <ta e="T124" id="Seg_2051" s="T123">köt-ü͡ök-püt</ta>
            <ta e="T125" id="Seg_2052" s="T124">munna</ta>
            <ta e="T126" id="Seg_2053" s="T125">öl-üːhü-büt</ta>
            <ta e="T127" id="Seg_2054" s="T126">hübehit</ta>
            <ta e="T128" id="Seg_2055" s="T127">ɨjɨt-ar</ta>
            <ta e="T129" id="Seg_2056" s="T128">dʼe</ta>
            <ta e="T130" id="Seg_2057" s="T129">kas-kɨt</ta>
            <ta e="T131" id="Seg_2058" s="T130">höbüleː-t-e</ta>
            <ta e="T132" id="Seg_2059" s="T131">kötör-ü-ŋ</ta>
            <ta e="T133" id="Seg_2060" s="T132">barɨ-ta</ta>
            <ta e="T134" id="Seg_2061" s="T133">höbüleː-bit</ta>
            <ta e="T135" id="Seg_2062" s="T134">leŋkej</ta>
            <ta e="T136" id="Seg_2063" s="T135">emi͡e</ta>
            <ta e="T137" id="Seg_2064" s="T136">haŋa-laːk</ta>
            <ta e="T138" id="Seg_2065" s="T137">min</ta>
            <ta e="T139" id="Seg_2066" s="T138">emi͡e</ta>
            <ta e="T140" id="Seg_2067" s="T139">bar-a-bɨn</ta>
            <ta e="T141" id="Seg_2068" s="T140">hübehit</ta>
            <ta e="T142" id="Seg_2069" s="T141">ɨjɨt-ar</ta>
            <ta e="T143" id="Seg_2070" s="T142">kaja</ta>
            <ta e="T144" id="Seg_2071" s="T143">bu</ta>
            <ta e="T145" id="Seg_2072" s="T144">leŋkej</ta>
            <ta e="T146" id="Seg_2073" s="T145">emi͡e</ta>
            <ta e="T147" id="Seg_2074" s="T146">bars-aːrɨ</ta>
            <ta e="T148" id="Seg_2075" s="T147">gɨn-ar</ta>
            <ta e="T149" id="Seg_2076" s="T148">höbül-üː-güt</ta>
            <ta e="T150" id="Seg_2077" s="T149">du͡o</ta>
            <ta e="T151" id="Seg_2078" s="T150">gini</ta>
            <ta e="T152" id="Seg_2079" s="T151">bars-ar-ɨ-n</ta>
            <ta e="T153" id="Seg_2080" s="T152">onu-ga</ta>
            <ta e="T154" id="Seg_2081" s="T153">kötör-dör-ü-ŋ</ta>
            <ta e="T155" id="Seg_2082" s="T154">barɨ-ta</ta>
            <ta e="T156" id="Seg_2083" s="T155">bihigi</ta>
            <ta e="T157" id="Seg_2084" s="T156">leŋkej</ta>
            <ta e="T158" id="Seg_2085" s="T157">bars-ar-ɨ-n</ta>
            <ta e="T159" id="Seg_2086" s="T158">höbüleː-bep-pit</ta>
            <ta e="T160" id="Seg_2087" s="T159">togo</ta>
            <ta e="T161" id="Seg_2088" s="T160">höbüleː-bek-kit</ta>
            <ta e="T162" id="Seg_2089" s="T161">hɨraj-daːk-karag-a-r</ta>
            <ta e="T163" id="Seg_2090" s="T162">haŋar-ɨ-ŋ</ta>
            <ta e="T164" id="Seg_2091" s="T163">gini-ni</ta>
            <ta e="T165" id="Seg_2092" s="T164">onu-ga</ta>
            <ta e="T166" id="Seg_2093" s="T165">kötör</ta>
            <ta e="T167" id="Seg_2094" s="T166">barɨ-ta</ta>
            <ta e="T168" id="Seg_2095" s="T167">di͡e-t-e</ta>
            <ta e="T169" id="Seg_2096" s="T168">munna</ta>
            <ta e="T170" id="Seg_2097" s="T169">bu͡ollagɨn</ta>
            <ta e="T171" id="Seg_2098" s="T170">leŋkej</ta>
            <ta e="T172" id="Seg_2099" s="T171">iːt-i-ll-en</ta>
            <ta e="T173" id="Seg_2100" s="T172">olor-or</ta>
            <ta e="T174" id="Seg_2101" s="T173">bihigi</ta>
            <ta e="T175" id="Seg_2102" s="T174">ere</ta>
            <ta e="T176" id="Seg_2103" s="T175">kergem-miti-nen</ta>
            <ta e="T177" id="Seg_2104" s="T176">ahɨlɨktan-an</ta>
            <ta e="T178" id="Seg_2105" s="T177">onno</ta>
            <ta e="T179" id="Seg_2106" s="T178">bar-dag-ɨna</ta>
            <ta e="T180" id="Seg_2107" s="T179">da</ta>
            <ta e="T181" id="Seg_2108" s="T180">ol</ta>
            <ta e="T182" id="Seg_2109" s="T181">mi͡ere-ti-n</ta>
            <ta e="T183" id="Seg_2110" s="T182">hin</ta>
            <ta e="T184" id="Seg_2111" s="T183">keːh-i͡e</ta>
            <ta e="T185" id="Seg_2112" s="T184">hu͡og-a</ta>
            <ta e="T186" id="Seg_2113" s="T185">bihigi</ta>
            <ta e="T187" id="Seg_2114" s="T186">ip-pep-pit</ta>
            <ta e="T188" id="Seg_2115" s="T187">hübehit</ta>
            <ta e="T189" id="Seg_2116" s="T188">diː-r</ta>
            <ta e="T190" id="Seg_2117" s="T189">leŋkej-ge</ta>
            <ta e="T191" id="Seg_2118" s="T190">kör</ta>
            <ta e="T192" id="Seg_2119" s="T191">dʼon</ta>
            <ta e="T193" id="Seg_2120" s="T192">poru͡oktaː-t-a</ta>
            <ta e="T194" id="Seg_2121" s="T193">kötör</ta>
            <ta e="T195" id="Seg_2122" s="T194">barɨ-ta</ta>
            <ta e="T196" id="Seg_2123" s="T195">haŋar-d-a</ta>
            <ta e="T197" id="Seg_2124" s="T196">ol</ta>
            <ta e="T198" id="Seg_2125" s="T197">hoku͡on</ta>
            <ta e="T199" id="Seg_2126" s="T198">bu</ta>
            <ta e="T200" id="Seg_2127" s="T199">hir-ge</ta>
            <ta e="T201" id="Seg_2128" s="T200">kaːl-a-gɨn</ta>
            <ta e="T202" id="Seg_2129" s="T201">bagar</ta>
            <ta e="T203" id="Seg_2130" s="T202">toŋ-on</ta>
            <ta e="T204" id="Seg_2131" s="T203">öl</ta>
            <ta e="T205" id="Seg_2132" s="T204">kajdak</ta>
            <ta e="T206" id="Seg_2133" s="T205">han-ɨː-gɨn</ta>
            <ta e="T207" id="Seg_2134" s="T206">leŋkej</ta>
            <ta e="T208" id="Seg_2135" s="T207">mökküj-bep-pin</ta>
            <ta e="T209" id="Seg_2136" s="T208">hogotok</ta>
            <ta e="T210" id="Seg_2137" s="T209">duša</ta>
            <ta e="T211" id="Seg_2138" s="T210">kanna</ta>
            <ta e="T212" id="Seg_2139" s="T211">öl-böt</ta>
            <ta e="T213" id="Seg_2140" s="T212">bagar</ta>
            <ta e="T214" id="Seg_2141" s="T213">ord-u͡o-m</ta>
            <ta e="T215" id="Seg_2142" s="T214">kaːl-a-bɨn</ta>
            <ta e="T216" id="Seg_2143" s="T215">diː-r</ta>
            <ta e="T217" id="Seg_2144" s="T216">kötör-dör</ta>
            <ta e="T218" id="Seg_2145" s="T217">barɨ</ta>
            <ta e="T219" id="Seg_2146" s="T218">köt-üteleː-n</ta>
            <ta e="T220" id="Seg_2147" s="T219">kaːl-bɨt-tar</ta>
            <ta e="T221" id="Seg_2148" s="T220">onu-ga</ta>
            <ta e="T222" id="Seg_2149" s="T221">leŋkej</ta>
            <ta e="T223" id="Seg_2150" s="T222">emeːksin-i-ger</ta>
            <ta e="T224" id="Seg_2151" s="T223">gel-er</ta>
            <ta e="T225" id="Seg_2152" s="T224">kaja=keː</ta>
            <ta e="T226" id="Seg_2153" s="T225">bihigi-ni</ta>
            <ta e="T227" id="Seg_2154" s="T226">il-pet-ter</ta>
            <ta e="T228" id="Seg_2155" s="T227">mini͡e-ke</ta>
            <ta e="T229" id="Seg_2156" s="T228">en</ta>
            <ta e="T230" id="Seg_2157" s="T229">köp</ta>
            <ta e="T231" id="Seg_2158" s="T230">mɨjaːn</ta>
            <ta e="T232" id="Seg_2159" s="T231">larga</ta>
            <ta e="T233" id="Seg_2160" s="T232">hokuj-da</ta>
            <ta e="T234" id="Seg_2161" s="T233">tik</ta>
            <ta e="T235" id="Seg_2162" s="T234">purgaː-ga</ta>
            <ta e="T236" id="Seg_2163" s="T235">da</ta>
            <ta e="T237" id="Seg_2164" s="T236">tib-i-ll-i-bet</ta>
            <ta e="T238" id="Seg_2165" s="T237">tɨmnɨː-ga</ta>
            <ta e="T239" id="Seg_2166" s="T238">da</ta>
            <ta e="T240" id="Seg_2167" s="T239">toŋ-mot</ta>
            <ta e="T241" id="Seg_2168" s="T240">gɨn-a</ta>
            <ta e="T242" id="Seg_2169" s="T241">emeːksin-e</ta>
            <ta e="T243" id="Seg_2170" s="T242">tig-i͡ekti-bin</ta>
            <ta e="T244" id="Seg_2171" s="T243">diː-r</ta>
            <ta e="T245" id="Seg_2172" s="T244">kaːr-ɨ</ta>
            <ta e="T246" id="Seg_2173" s="T245">gɨtta</ta>
            <ta e="T247" id="Seg_2174" s="T246">biːr</ta>
            <ta e="T248" id="Seg_2175" s="T247">čeːlkeː</ta>
            <ta e="T249" id="Seg_2176" s="T248">bu͡ol-lun</ta>
            <ta e="T250" id="Seg_2177" s="T249">ol</ta>
            <ta e="T251" id="Seg_2178" s="T250">hokuj</ta>
            <ta e="T252" id="Seg_2179" s="T251">emeːksin-e</ta>
            <ta e="T253" id="Seg_2180" s="T252">uːs</ta>
            <ta e="T254" id="Seg_2181" s="T253">mujaːn</ta>
            <ta e="T255" id="Seg_2182" s="T254">tik-pit</ta>
            <ta e="T256" id="Seg_2183" s="T255">kaːs</ta>
            <ta e="T257" id="Seg_2184" s="T256">gi͡en-i-neːger</ta>
            <ta e="T258" id="Seg_2185" s="T257">ürüŋ</ta>
            <ta e="T259" id="Seg_2186" s="T258">čeːlkeː</ta>
            <ta e="T260" id="Seg_2187" s="T259">hokuj-u</ta>
            <ta e="T261" id="Seg_2188" s="T260">leŋkej</ta>
            <ta e="T262" id="Seg_2189" s="T261">bu-nu</ta>
            <ta e="T263" id="Seg_2190" s="T262">kep-pit-e</ta>
            <ta e="T264" id="Seg_2191" s="T263">hörü höp</ta>
            <ta e="T265" id="Seg_2192" s="T264">hokuj-a</ta>
            <ta e="T266" id="Seg_2193" s="T265">et-i-ger</ta>
            <ta e="T267" id="Seg_2194" s="T266">erij-e</ta>
            <ta e="T268" id="Seg_2195" s="T267">kap-pɨt</ta>
            <ta e="T269" id="Seg_2196" s="T268">tɨmnɨː-ga</ta>
            <ta e="T270" id="Seg_2197" s="T269">da</ta>
            <ta e="T271" id="Seg_2198" s="T270">toŋ-mot</ta>
            <ta e="T272" id="Seg_2199" s="T271">purgaː-ga</ta>
            <ta e="T273" id="Seg_2200" s="T272">da</ta>
            <ta e="T274" id="Seg_2201" s="T273">hɨst-ɨ-bat</ta>
            <ta e="T275" id="Seg_2202" s="T274">kaːr</ta>
            <ta e="T276" id="Seg_2203" s="T275">da</ta>
            <ta e="T277" id="Seg_2204" s="T276">hɨst-ɨ-bat</ta>
            <ta e="T278" id="Seg_2205" s="T277">gini͡e-ke</ta>
            <ta e="T279" id="Seg_2206" s="T278">leŋkej</ta>
            <ta e="T280" id="Seg_2207" s="T279">hoččotton</ta>
            <ta e="T281" id="Seg_2208" s="T280">bu</ta>
            <ta e="T282" id="Seg_2209" s="T281">hir-ge</ta>
            <ta e="T283" id="Seg_2210" s="T282">dojdu-lam-mɨt-a</ta>
            <ta e="T284" id="Seg_2211" s="T283">ol</ta>
            <ta e="T285" id="Seg_2212" s="T284">kaːl-bɨt-ɨ-n</ta>
            <ta e="T286" id="Seg_2213" s="T285">öhü͡ömčüleː-n</ta>
            <ta e="T287" id="Seg_2214" s="T286">leŋkej</ta>
            <ta e="T288" id="Seg_2215" s="T287">kɨra</ta>
            <ta e="T289" id="Seg_2216" s="T288">kötör-ü</ta>
            <ta e="T290" id="Seg_2217" s="T289">barɨ-tɨ-n</ta>
            <ta e="T291" id="Seg_2218" s="T290">dössü͡ö</ta>
            <ta e="T292" id="Seg_2219" s="T291">hiː-r</ta>
            <ta e="T293" id="Seg_2220" s="T292">bu͡ol-but-a</ta>
            <ta e="T294" id="Seg_2221" s="T293">orduk</ta>
            <ta e="T295" id="Seg_2222" s="T294">gini-ni</ta>
            <ta e="T296" id="Seg_2223" s="T295">kɨtta</ta>
            <ta e="T297" id="Seg_2224" s="T296">kɨs-tɨː-r</ta>
            <ta e="T298" id="Seg_2225" s="T297">kurpaːskɨ-nɨ</ta>
            <ta e="T299" id="Seg_2226" s="T298">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2227" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_2228" s="T1">kaːs-I-ŋ</ta>
            <ta e="T3" id="Seg_2229" s="T2">kus-I-ŋ</ta>
            <ta e="T4" id="Seg_2230" s="T3">čɨːčaːk-I-ŋ</ta>
            <ta e="T5" id="Seg_2231" s="T4">leŋkej-I-ŋ</ta>
            <ta e="T6" id="Seg_2232" s="T5">barɨ</ta>
            <ta e="T7" id="Seg_2233" s="T6">kötör</ta>
            <ta e="T8" id="Seg_2234" s="T7">bu</ta>
            <ta e="T9" id="Seg_2235" s="T8">hir-GA</ta>
            <ta e="T10" id="Seg_2236" s="T9">hir-LAːK</ta>
            <ta e="T11" id="Seg_2237" s="T10">e-TI-tA</ta>
            <ta e="T12" id="Seg_2238" s="T11">bu</ta>
            <ta e="T13" id="Seg_2239" s="T12">hir-I-ŋ</ta>
            <ta e="T14" id="Seg_2240" s="T13">ol</ta>
            <ta e="T15" id="Seg_2241" s="T14">kem-GA</ta>
            <ta e="T16" id="Seg_2242" s="T15">ičiges</ta>
            <ta e="T17" id="Seg_2243" s="T16">ühü</ta>
            <ta e="T18" id="Seg_2244" s="T17">onton</ta>
            <ta e="T19" id="Seg_2245" s="T18">tɨmnɨː-LAr</ta>
            <ta e="T20" id="Seg_2246" s="T19">bu͡ol-TI-LAr</ta>
            <ta e="T21" id="Seg_2247" s="T20">dʼɨl-kün</ta>
            <ta e="T22" id="Seg_2248" s="T21">ularɨj-TI-tA</ta>
            <ta e="T23" id="Seg_2249" s="T22">bu-GA</ta>
            <ta e="T24" id="Seg_2250" s="T23">kötör-I-ŋ</ta>
            <ta e="T25" id="Seg_2251" s="T24">barɨ-tA</ta>
            <ta e="T26" id="Seg_2252" s="T25">hübeleː-A-s-Ar</ta>
            <ta e="T27" id="Seg_2253" s="T26">barɨ-LArI-GAr</ta>
            <ta e="T28" id="Seg_2254" s="T27">biːr</ta>
            <ta e="T29" id="Seg_2255" s="T28">hübehit</ta>
            <ta e="T30" id="Seg_2256" s="T29">baːr</ta>
            <ta e="T31" id="Seg_2257" s="T30">leŋkej-I-ŋ</ta>
            <ta e="T32" id="Seg_2258" s="T31">hoččotton</ta>
            <ta e="T33" id="Seg_2259" s="T32">kurpaːskɨ-nI</ta>
            <ta e="T34" id="Seg_2260" s="T33">hi͡e-AːččI</ta>
            <ta e="T35" id="Seg_2261" s="T34">kus-nI</ta>
            <ta e="T36" id="Seg_2262" s="T35">bu͡ollun</ta>
            <ta e="T37" id="Seg_2263" s="T36">kaːs-nI</ta>
            <ta e="T38" id="Seg_2264" s="T37">bu͡ollun</ta>
            <ta e="T39" id="Seg_2265" s="T38">emi͡e</ta>
            <ta e="T40" id="Seg_2266" s="T39">bu</ta>
            <ta e="T41" id="Seg_2267" s="T40">kötör-LAr</ta>
            <ta e="T42" id="Seg_2268" s="T41">hübehit-LArA</ta>
            <ta e="T43" id="Seg_2269" s="T42">gini</ta>
            <ta e="T44" id="Seg_2270" s="T43">kaːs-LAr-ttAn</ta>
            <ta e="T45" id="Seg_2271" s="T44">e-TI-tA</ta>
            <ta e="T46" id="Seg_2272" s="T45">kötör-nI</ta>
            <ta e="T47" id="Seg_2273" s="T46">barɨ-tI-n</ta>
            <ta e="T48" id="Seg_2274" s="T47">munnʼak-GA</ta>
            <ta e="T49" id="Seg_2275" s="T48">ɨgɨr-Ar</ta>
            <ta e="T50" id="Seg_2276" s="T49">biːr</ta>
            <ta e="T51" id="Seg_2277" s="T50">alɨː-nI</ta>
            <ta e="T52" id="Seg_2278" s="T51">his-nI</ta>
            <ta e="T53" id="Seg_2279" s="T52">toloru</ta>
            <ta e="T54" id="Seg_2280" s="T53">kötör</ta>
            <ta e="T55" id="Seg_2281" s="T54">mus-I-n-TI-tA</ta>
            <ta e="T56" id="Seg_2282" s="T55">leŋkej-I-ŋ</ta>
            <ta e="T57" id="Seg_2283" s="T56">emi͡e</ta>
            <ta e="T58" id="Seg_2284" s="T57">kel-BIT</ta>
            <ta e="T59" id="Seg_2285" s="T58">hübehit</ta>
            <ta e="T60" id="Seg_2286" s="T59">et-Ar</ta>
            <ta e="T61" id="Seg_2287" s="T60">dʼe</ta>
            <ta e="T62" id="Seg_2288" s="T61">dʼɨl-kün</ta>
            <ta e="T63" id="Seg_2289" s="T62">ularɨj-TI-tA</ta>
            <ta e="T64" id="Seg_2290" s="T63">bihigi</ta>
            <ta e="T65" id="Seg_2291" s="T64">hajɨnɨn</ta>
            <ta e="T66" id="Seg_2292" s="T65">agaj</ta>
            <ta e="T67" id="Seg_2293" s="T66">olor-Ar</ta>
            <ta e="T68" id="Seg_2294" s="T67">bu͡ol-TI-BIt</ta>
            <ta e="T69" id="Seg_2295" s="T68">min</ta>
            <ta e="T70" id="Seg_2296" s="T69">bil-A-BIn</ta>
            <ta e="T71" id="Seg_2297" s="T70">kannɨk</ta>
            <ta e="T72" id="Seg_2298" s="T71">ere</ta>
            <ta e="T73" id="Seg_2299" s="T72">hir-GA</ta>
            <ta e="T74" id="Seg_2300" s="T73">kɨhɨn</ta>
            <ta e="T75" id="Seg_2301" s="T74">bu͡ol-BAT</ta>
            <ta e="T76" id="Seg_2302" s="T75">dojdu-tA</ta>
            <ta e="T77" id="Seg_2303" s="T76">baːr</ta>
            <ta e="T78" id="Seg_2304" s="T77">bihigi</ta>
            <ta e="T79" id="Seg_2305" s="T78">kɨnat-LAːK-LAr</ta>
            <ta e="T80" id="Seg_2306" s="T79">togo</ta>
            <ta e="T81" id="Seg_2307" s="T80">bu</ta>
            <ta e="T82" id="Seg_2308" s="T81">hir-GA</ta>
            <ta e="T83" id="Seg_2309" s="T82">toŋ-A</ta>
            <ta e="T84" id="Seg_2310" s="T83">hɨt-IAK-BIt=Ij</ta>
            <ta e="T85" id="Seg_2311" s="T84">hir-tI-nAn</ta>
            <ta e="T86" id="Seg_2312" s="T85">bar-TAK-GA</ta>
            <ta e="T87" id="Seg_2313" s="T86">ɨraːk</ta>
            <ta e="T88" id="Seg_2314" s="T87">bihigi</ta>
            <ta e="T89" id="Seg_2315" s="T88">kurdarɨ</ta>
            <ta e="T90" id="Seg_2316" s="T89">ol</ta>
            <ta e="T91" id="Seg_2317" s="T90">hir-GA</ta>
            <ta e="T92" id="Seg_2318" s="T91">köt-IAgIŋ</ta>
            <ta e="T93" id="Seg_2319" s="T92">kɨhɨn</ta>
            <ta e="T94" id="Seg_2320" s="T93">kel-BIT-tA</ta>
            <ta e="T95" id="Seg_2321" s="T94">bu</ta>
            <ta e="T96" id="Seg_2322" s="T95">tɨmnɨː-BIt</ta>
            <ta e="T97" id="Seg_2323" s="T96">aːs-IAK.[tI]-r</ta>
            <ta e="T98" id="Seg_2324" s="T97">di͡eri</ta>
            <ta e="T99" id="Seg_2325" s="T98">ol</ta>
            <ta e="T100" id="Seg_2326" s="T99">hir-GA</ta>
            <ta e="T101" id="Seg_2327" s="T100">ologur-IAgIŋ</ta>
            <ta e="T102" id="Seg_2328" s="T101">manna</ta>
            <ta e="T103" id="Seg_2329" s="T102">olor-TAK-BItInA</ta>
            <ta e="T104" id="Seg_2330" s="T103">bihigi</ta>
            <ta e="T105" id="Seg_2331" s="T104">barɨ</ta>
            <ta e="T106" id="Seg_2332" s="T105">habɨlɨn-IAK-BIt</ta>
            <ta e="T107" id="Seg_2333" s="T106">toŋ-An</ta>
            <ta e="T108" id="Seg_2334" s="T107">öl-IAK-BIt</ta>
            <ta e="T109" id="Seg_2335" s="T108">onno</ta>
            <ta e="T110" id="Seg_2336" s="T109">köt-TAK-BItInA</ta>
            <ta e="T111" id="Seg_2337" s="T110">barɨ</ta>
            <ta e="T112" id="Seg_2338" s="T111">kihi</ta>
            <ta e="T113" id="Seg_2339" s="T112">bu͡ol-IAK-BIt</ta>
            <ta e="T114" id="Seg_2340" s="T113">dʼe</ta>
            <ta e="T115" id="Seg_2341" s="T114">hübe-GItI-n</ta>
            <ta e="T116" id="Seg_2342" s="T115">bi͡er-I-ŋ</ta>
            <ta e="T117" id="Seg_2343" s="T116">ol-GA</ta>
            <ta e="T118" id="Seg_2344" s="T117">kötör</ta>
            <ta e="T119" id="Seg_2345" s="T118">barɨ-tA</ta>
            <ta e="T120" id="Seg_2346" s="T119">dʼe</ta>
            <ta e="T121" id="Seg_2347" s="T120">kirdik</ta>
            <ta e="T122" id="Seg_2348" s="T121">bihigi</ta>
            <ta e="T123" id="Seg_2349" s="T122">barɨ</ta>
            <ta e="T124" id="Seg_2350" s="T123">köt-IAK-BIt</ta>
            <ta e="T125" id="Seg_2351" s="T124">manna</ta>
            <ta e="T126" id="Seg_2352" s="T125">öl-IːhI-BIt</ta>
            <ta e="T127" id="Seg_2353" s="T126">hübehit</ta>
            <ta e="T128" id="Seg_2354" s="T127">ɨjɨt-Ar</ta>
            <ta e="T129" id="Seg_2355" s="T128">dʼe</ta>
            <ta e="T130" id="Seg_2356" s="T129">kas-GIt</ta>
            <ta e="T131" id="Seg_2357" s="T130">höbüleː-TI-tA</ta>
            <ta e="T132" id="Seg_2358" s="T131">kötör-I-ŋ</ta>
            <ta e="T133" id="Seg_2359" s="T132">barɨ-tA</ta>
            <ta e="T134" id="Seg_2360" s="T133">höbüleː-BIT</ta>
            <ta e="T135" id="Seg_2361" s="T134">leŋkej</ta>
            <ta e="T136" id="Seg_2362" s="T135">emi͡e</ta>
            <ta e="T137" id="Seg_2363" s="T136">haŋa-LAːK</ta>
            <ta e="T138" id="Seg_2364" s="T137">min</ta>
            <ta e="T139" id="Seg_2365" s="T138">emi͡e</ta>
            <ta e="T140" id="Seg_2366" s="T139">bar-A-BIn</ta>
            <ta e="T141" id="Seg_2367" s="T140">hübehit</ta>
            <ta e="T142" id="Seg_2368" s="T141">ɨjɨt-Ar</ta>
            <ta e="T143" id="Seg_2369" s="T142">kaja</ta>
            <ta e="T144" id="Seg_2370" s="T143">bu</ta>
            <ta e="T145" id="Seg_2371" s="T144">leŋkej</ta>
            <ta e="T146" id="Seg_2372" s="T145">emi͡e</ta>
            <ta e="T147" id="Seg_2373" s="T146">barɨs-AːrI</ta>
            <ta e="T148" id="Seg_2374" s="T147">gɨn-Ar</ta>
            <ta e="T149" id="Seg_2375" s="T148">höbüleː-A-GIt</ta>
            <ta e="T150" id="Seg_2376" s="T149">du͡o</ta>
            <ta e="T151" id="Seg_2377" s="T150">gini</ta>
            <ta e="T152" id="Seg_2378" s="T151">barɨs-Ar-tI-n</ta>
            <ta e="T153" id="Seg_2379" s="T152">ol-GA</ta>
            <ta e="T154" id="Seg_2380" s="T153">kötör-LAr-I-ŋ</ta>
            <ta e="T155" id="Seg_2381" s="T154">barɨ-tA</ta>
            <ta e="T156" id="Seg_2382" s="T155">bihigi</ta>
            <ta e="T157" id="Seg_2383" s="T156">leŋkej</ta>
            <ta e="T158" id="Seg_2384" s="T157">barɨs-Ar-tI-n</ta>
            <ta e="T159" id="Seg_2385" s="T158">höbüleː-BAT-BIt</ta>
            <ta e="T160" id="Seg_2386" s="T159">togo</ta>
            <ta e="T161" id="Seg_2387" s="T160">höbüleː-BAT-GIt</ta>
            <ta e="T162" id="Seg_2388" s="T161">hɨraj-LAːK-karak-tA-r</ta>
            <ta e="T163" id="Seg_2389" s="T162">haŋar-I-ŋ</ta>
            <ta e="T164" id="Seg_2390" s="T163">gini-nI</ta>
            <ta e="T165" id="Seg_2391" s="T164">ol-GA</ta>
            <ta e="T166" id="Seg_2392" s="T165">kötör</ta>
            <ta e="T167" id="Seg_2393" s="T166">barɨ-tA</ta>
            <ta e="T168" id="Seg_2394" s="T167">di͡e-TI-tA</ta>
            <ta e="T169" id="Seg_2395" s="T168">manna</ta>
            <ta e="T170" id="Seg_2396" s="T169">bu͡ollagɨna</ta>
            <ta e="T171" id="Seg_2397" s="T170">leŋkej</ta>
            <ta e="T172" id="Seg_2398" s="T171">iːt-I-LIN-An</ta>
            <ta e="T173" id="Seg_2399" s="T172">olor-Ar</ta>
            <ta e="T174" id="Seg_2400" s="T173">bihigi</ta>
            <ta e="T175" id="Seg_2401" s="T174">ere</ta>
            <ta e="T176" id="Seg_2402" s="T175">kergen-BItI-nAn</ta>
            <ta e="T177" id="Seg_2403" s="T176">ahɨlɨktan-An</ta>
            <ta e="T178" id="Seg_2404" s="T177">onno</ta>
            <ta e="T179" id="Seg_2405" s="T178">bar-TAK-InA</ta>
            <ta e="T180" id="Seg_2406" s="T179">da</ta>
            <ta e="T181" id="Seg_2407" s="T180">ol</ta>
            <ta e="T182" id="Seg_2408" s="T181">mi͡ere-tI-n</ta>
            <ta e="T183" id="Seg_2409" s="T182">hin</ta>
            <ta e="T184" id="Seg_2410" s="T183">keːs-IAK.[tA]</ta>
            <ta e="T185" id="Seg_2411" s="T184">hu͡ok-tA</ta>
            <ta e="T186" id="Seg_2412" s="T185">bihigi</ta>
            <ta e="T187" id="Seg_2413" s="T186">it-BAT-BIt</ta>
            <ta e="T188" id="Seg_2414" s="T187">hübehit</ta>
            <ta e="T189" id="Seg_2415" s="T188">di͡e-Ar</ta>
            <ta e="T190" id="Seg_2416" s="T189">leŋkej-GA</ta>
            <ta e="T191" id="Seg_2417" s="T190">kör</ta>
            <ta e="T192" id="Seg_2418" s="T191">dʼon</ta>
            <ta e="T193" id="Seg_2419" s="T192">poru͡oktaː-TI-tA</ta>
            <ta e="T194" id="Seg_2420" s="T193">kötör</ta>
            <ta e="T195" id="Seg_2421" s="T194">barɨ-tA</ta>
            <ta e="T196" id="Seg_2422" s="T195">haŋar-TI-tA</ta>
            <ta e="T197" id="Seg_2423" s="T196">ol</ta>
            <ta e="T198" id="Seg_2424" s="T197">hoku͡on</ta>
            <ta e="T199" id="Seg_2425" s="T198">bu</ta>
            <ta e="T200" id="Seg_2426" s="T199">hir-GA</ta>
            <ta e="T201" id="Seg_2427" s="T200">kaːl-A-GIn</ta>
            <ta e="T202" id="Seg_2428" s="T201">bagar</ta>
            <ta e="T203" id="Seg_2429" s="T202">toŋ-An</ta>
            <ta e="T204" id="Seg_2430" s="T203">öl</ta>
            <ta e="T205" id="Seg_2431" s="T204">kajdak</ta>
            <ta e="T206" id="Seg_2432" s="T205">hanaː-A-GIn</ta>
            <ta e="T207" id="Seg_2433" s="T206">leŋkej</ta>
            <ta e="T208" id="Seg_2434" s="T207">mökküj-BAT-BIn</ta>
            <ta e="T209" id="Seg_2435" s="T208">čogotok</ta>
            <ta e="T210" id="Seg_2436" s="T209">duša</ta>
            <ta e="T211" id="Seg_2437" s="T210">kanna</ta>
            <ta e="T212" id="Seg_2438" s="T211">öl-BAT</ta>
            <ta e="T213" id="Seg_2439" s="T212">bagar</ta>
            <ta e="T214" id="Seg_2440" s="T213">ort-IAK-m</ta>
            <ta e="T215" id="Seg_2441" s="T214">kaːl-A-BIn</ta>
            <ta e="T216" id="Seg_2442" s="T215">di͡e-Ar</ta>
            <ta e="T217" id="Seg_2443" s="T216">kötör-LAr</ta>
            <ta e="T218" id="Seg_2444" s="T217">barɨ</ta>
            <ta e="T219" id="Seg_2445" s="T218">köt-ItAlAː-An</ta>
            <ta e="T220" id="Seg_2446" s="T219">kaːl-BIT-LAr</ta>
            <ta e="T221" id="Seg_2447" s="T220">ol-GA</ta>
            <ta e="T222" id="Seg_2448" s="T221">leŋkej</ta>
            <ta e="T223" id="Seg_2449" s="T222">emeːksin-tI-GAr</ta>
            <ta e="T224" id="Seg_2450" s="T223">kel-Ar</ta>
            <ta e="T225" id="Seg_2451" s="T224">kaja=keː</ta>
            <ta e="T226" id="Seg_2452" s="T225">bihigi-nI</ta>
            <ta e="T227" id="Seg_2453" s="T226">ilt-BAT-LAr</ta>
            <ta e="T228" id="Seg_2454" s="T227">min-GA</ta>
            <ta e="T229" id="Seg_2455" s="T228">en</ta>
            <ta e="T230" id="Seg_2456" s="T229">köp</ta>
            <ta e="T231" id="Seg_2457" s="T230">mɨjaːn</ta>
            <ta e="T232" id="Seg_2458" s="T231">larga</ta>
            <ta e="T233" id="Seg_2459" s="T232">hokuj-TA</ta>
            <ta e="T234" id="Seg_2460" s="T233">tik</ta>
            <ta e="T235" id="Seg_2461" s="T234">purgaː-GA</ta>
            <ta e="T236" id="Seg_2462" s="T235">da</ta>
            <ta e="T237" id="Seg_2463" s="T236">tip-I-LIN-I-BAT</ta>
            <ta e="T238" id="Seg_2464" s="T237">tɨmnɨː-GA</ta>
            <ta e="T239" id="Seg_2465" s="T238">da</ta>
            <ta e="T240" id="Seg_2466" s="T239">toŋ-BAT</ta>
            <ta e="T241" id="Seg_2467" s="T240">gɨn-A</ta>
            <ta e="T242" id="Seg_2468" s="T241">emeːksin-tA</ta>
            <ta e="T243" id="Seg_2469" s="T242">tik-IAktI-BIn</ta>
            <ta e="T244" id="Seg_2470" s="T243">di͡e-Ar</ta>
            <ta e="T245" id="Seg_2471" s="T244">kaːr-nI</ta>
            <ta e="T246" id="Seg_2472" s="T245">kɨtta</ta>
            <ta e="T247" id="Seg_2473" s="T246">biːr</ta>
            <ta e="T248" id="Seg_2474" s="T247">čeːlkeː</ta>
            <ta e="T249" id="Seg_2475" s="T248">bu͡ol-TIn</ta>
            <ta e="T250" id="Seg_2476" s="T249">ol</ta>
            <ta e="T251" id="Seg_2477" s="T250">hokuj</ta>
            <ta e="T252" id="Seg_2478" s="T251">emeːksin-tA</ta>
            <ta e="T253" id="Seg_2479" s="T252">uːs</ta>
            <ta e="T254" id="Seg_2480" s="T253">mɨjaːn</ta>
            <ta e="T255" id="Seg_2481" s="T254">tik-BIT</ta>
            <ta e="T256" id="Seg_2482" s="T255">kaːs</ta>
            <ta e="T257" id="Seg_2483" s="T256">gi͡en-tI-TAːgAr</ta>
            <ta e="T258" id="Seg_2484" s="T257">ürüŋ</ta>
            <ta e="T259" id="Seg_2485" s="T258">čeːlkeː</ta>
            <ta e="T260" id="Seg_2486" s="T259">hokuj-nI</ta>
            <ta e="T261" id="Seg_2487" s="T260">leŋkej</ta>
            <ta e="T262" id="Seg_2488" s="T261">bu-nI</ta>
            <ta e="T263" id="Seg_2489" s="T262">ket-BIT-tA</ta>
            <ta e="T264" id="Seg_2490" s="T263">hörü höp</ta>
            <ta e="T265" id="Seg_2491" s="T264">hokuj-tA</ta>
            <ta e="T266" id="Seg_2492" s="T265">et-tI-GAr</ta>
            <ta e="T267" id="Seg_2493" s="T266">erij-A</ta>
            <ta e="T268" id="Seg_2494" s="T267">kap-BIT</ta>
            <ta e="T269" id="Seg_2495" s="T268">tɨmnɨː-GA</ta>
            <ta e="T270" id="Seg_2496" s="T269">da</ta>
            <ta e="T271" id="Seg_2497" s="T270">toŋ-BAT</ta>
            <ta e="T272" id="Seg_2498" s="T271">purgaː-GA</ta>
            <ta e="T273" id="Seg_2499" s="T272">da</ta>
            <ta e="T274" id="Seg_2500" s="T273">hɨhɨn-I-BAT</ta>
            <ta e="T275" id="Seg_2501" s="T274">kaːr</ta>
            <ta e="T276" id="Seg_2502" s="T275">da</ta>
            <ta e="T277" id="Seg_2503" s="T276">hɨhɨn-I-BAT</ta>
            <ta e="T278" id="Seg_2504" s="T277">gini-GA</ta>
            <ta e="T279" id="Seg_2505" s="T278">leŋkej</ta>
            <ta e="T280" id="Seg_2506" s="T279">hoččotton</ta>
            <ta e="T281" id="Seg_2507" s="T280">bu</ta>
            <ta e="T282" id="Seg_2508" s="T281">hir-GA</ta>
            <ta e="T283" id="Seg_2509" s="T282">dojdu-LAN-BIT-tA</ta>
            <ta e="T284" id="Seg_2510" s="T283">ol</ta>
            <ta e="T285" id="Seg_2511" s="T284">kaːl-BIT-tI-n</ta>
            <ta e="T286" id="Seg_2512" s="T285">öhü͡ömčüleː-An</ta>
            <ta e="T287" id="Seg_2513" s="T286">leŋkej</ta>
            <ta e="T288" id="Seg_2514" s="T287">kɨra</ta>
            <ta e="T289" id="Seg_2515" s="T288">kötör-nI</ta>
            <ta e="T290" id="Seg_2516" s="T289">barɨ-tI-n</ta>
            <ta e="T291" id="Seg_2517" s="T290">össü͡ö</ta>
            <ta e="T292" id="Seg_2518" s="T291">hi͡e-Ar</ta>
            <ta e="T293" id="Seg_2519" s="T292">bu͡ol-BIT-tA</ta>
            <ta e="T294" id="Seg_2520" s="T293">orduk</ta>
            <ta e="T295" id="Seg_2521" s="T294">gini-nI</ta>
            <ta e="T296" id="Seg_2522" s="T295">kɨtta</ta>
            <ta e="T297" id="Seg_2523" s="T296">kɨs-LAː-Ar</ta>
            <ta e="T298" id="Seg_2524" s="T297">kurpaːskɨ-nI</ta>
            <ta e="T299" id="Seg_2525" s="T298">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2526" s="T0">long.ago</ta>
            <ta e="T2" id="Seg_2527" s="T1">goose-EP-2SG.[NOM]</ta>
            <ta e="T3" id="Seg_2528" s="T2">duck-EP-2SG.[NOM]</ta>
            <ta e="T4" id="Seg_2529" s="T3">small.bird-EP-2SG.[NOM]</ta>
            <ta e="T5" id="Seg_2530" s="T4">snow.owl-EP-2SG.[NOM]</ta>
            <ta e="T6" id="Seg_2531" s="T5">every</ta>
            <ta e="T7" id="Seg_2532" s="T6">bird.[NOM]</ta>
            <ta e="T8" id="Seg_2533" s="T7">this</ta>
            <ta e="T9" id="Seg_2534" s="T8">earth-DAT/LOC</ta>
            <ta e="T10" id="Seg_2535" s="T9">earth-PROPR.[NOM]</ta>
            <ta e="T11" id="Seg_2536" s="T10">be-PST1-3SG</ta>
            <ta e="T12" id="Seg_2537" s="T11">this</ta>
            <ta e="T13" id="Seg_2538" s="T12">earth-EP-2SG.[NOM]</ta>
            <ta e="T14" id="Seg_2539" s="T13">that</ta>
            <ta e="T15" id="Seg_2540" s="T14">time-DAT/LOC</ta>
            <ta e="T16" id="Seg_2541" s="T15">warm.[NOM]</ta>
            <ta e="T17" id="Seg_2542" s="T16">it.is.said</ta>
            <ta e="T18" id="Seg_2543" s="T17">then</ta>
            <ta e="T19" id="Seg_2544" s="T18">cold-PL.[NOM]</ta>
            <ta e="T20" id="Seg_2545" s="T19">be-PST1-3PL</ta>
            <ta e="T21" id="Seg_2546" s="T20">year.[NOM]-day.[NOM]</ta>
            <ta e="T22" id="Seg_2547" s="T21">change-PST1-3SG</ta>
            <ta e="T23" id="Seg_2548" s="T22">this-DAT/LOC</ta>
            <ta e="T24" id="Seg_2549" s="T23">bird-EP-2SG.[NOM]</ta>
            <ta e="T25" id="Seg_2550" s="T24">every-3SG.[NOM]</ta>
            <ta e="T26" id="Seg_2551" s="T25">advise-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T27" id="Seg_2552" s="T26">every-3PL-DAT/LOC</ta>
            <ta e="T28" id="Seg_2553" s="T27">one</ta>
            <ta e="T29" id="Seg_2554" s="T28">leader.[NOM]</ta>
            <ta e="T30" id="Seg_2555" s="T29">there.is</ta>
            <ta e="T31" id="Seg_2556" s="T30">snow.owl-EP-2SG.[NOM]</ta>
            <ta e="T32" id="Seg_2557" s="T31">from.then.on</ta>
            <ta e="T33" id="Seg_2558" s="T32">partridge-ACC</ta>
            <ta e="T34" id="Seg_2559" s="T33">eat-HAB.[3SG]</ta>
            <ta e="T35" id="Seg_2560" s="T34">duck-ACC</ta>
            <ta e="T36" id="Seg_2561" s="T35">EMPH</ta>
            <ta e="T37" id="Seg_2562" s="T36">goose-ACC</ta>
            <ta e="T38" id="Seg_2563" s="T37">EMPH</ta>
            <ta e="T39" id="Seg_2564" s="T38">also</ta>
            <ta e="T40" id="Seg_2565" s="T39">this</ta>
            <ta e="T41" id="Seg_2566" s="T40">bird-PL.[NOM]</ta>
            <ta e="T42" id="Seg_2567" s="T41">leader-3PL.[NOM]</ta>
            <ta e="T43" id="Seg_2568" s="T42">3SG.[NOM]</ta>
            <ta e="T44" id="Seg_2569" s="T43">goose-PL-ABL</ta>
            <ta e="T45" id="Seg_2570" s="T44">be-PST1-3SG</ta>
            <ta e="T46" id="Seg_2571" s="T45">bird-ACC</ta>
            <ta e="T47" id="Seg_2572" s="T46">every-3SG-ACC</ta>
            <ta e="T48" id="Seg_2573" s="T47">gathering-DAT/LOC</ta>
            <ta e="T49" id="Seg_2574" s="T48">call-PRS.[3SG]</ta>
            <ta e="T50" id="Seg_2575" s="T49">one</ta>
            <ta e="T51" id="Seg_2576" s="T50">valley-ACC</ta>
            <ta e="T52" id="Seg_2577" s="T51">forested.mountain-ACC</ta>
            <ta e="T53" id="Seg_2578" s="T52">full</ta>
            <ta e="T54" id="Seg_2579" s="T53">bird.[NOM]</ta>
            <ta e="T55" id="Seg_2580" s="T54">gather-EP-MED-PST1-3SG</ta>
            <ta e="T56" id="Seg_2581" s="T55">snow.owl-EP-2SG.[NOM]</ta>
            <ta e="T57" id="Seg_2582" s="T56">also</ta>
            <ta e="T58" id="Seg_2583" s="T57">come-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_2584" s="T58">leader.[NOM]</ta>
            <ta e="T60" id="Seg_2585" s="T59">speak-PRS.[3SG]</ta>
            <ta e="T61" id="Seg_2586" s="T60">well</ta>
            <ta e="T62" id="Seg_2587" s="T61">year.[NOM]-day.[NOM]</ta>
            <ta e="T63" id="Seg_2588" s="T62">change-PST1-3SG</ta>
            <ta e="T64" id="Seg_2589" s="T63">1PL.[NOM]</ta>
            <ta e="T65" id="Seg_2590" s="T64">in.summer</ta>
            <ta e="T66" id="Seg_2591" s="T65">only</ta>
            <ta e="T67" id="Seg_2592" s="T66">live-PTCP.PRS</ta>
            <ta e="T68" id="Seg_2593" s="T67">be-PST1-1PL</ta>
            <ta e="T69" id="Seg_2594" s="T68">1SG.[NOM]</ta>
            <ta e="T70" id="Seg_2595" s="T69">know-PRS-1SG</ta>
            <ta e="T71" id="Seg_2596" s="T70">what.kind.of</ta>
            <ta e="T72" id="Seg_2597" s="T71">INDEF</ta>
            <ta e="T73" id="Seg_2598" s="T72">earth-DAT/LOC</ta>
            <ta e="T74" id="Seg_2599" s="T73">winter.[NOM]</ta>
            <ta e="T75" id="Seg_2600" s="T74">be-NEG.PTCP</ta>
            <ta e="T76" id="Seg_2601" s="T75">country-3SG.[NOM]</ta>
            <ta e="T77" id="Seg_2602" s="T76">there.is</ta>
            <ta e="T78" id="Seg_2603" s="T77">1PL.[NOM]</ta>
            <ta e="T79" id="Seg_2604" s="T78">wing-PROPR-PL.[NOM]</ta>
            <ta e="T80" id="Seg_2605" s="T79">why</ta>
            <ta e="T81" id="Seg_2606" s="T80">this</ta>
            <ta e="T82" id="Seg_2607" s="T81">earth-DAT/LOC</ta>
            <ta e="T83" id="Seg_2608" s="T82">freeze-CVB.SIM</ta>
            <ta e="T84" id="Seg_2609" s="T83">lie-FUT-1PL=Q</ta>
            <ta e="T85" id="Seg_2610" s="T84">earth-3SG-INSTR</ta>
            <ta e="T86" id="Seg_2611" s="T85">go-PTCP.COND-DAT/LOC</ta>
            <ta e="T87" id="Seg_2612" s="T86">distant.[NOM]</ta>
            <ta e="T88" id="Seg_2613" s="T87">1PL.[NOM]</ta>
            <ta e="T89" id="Seg_2614" s="T88">straightforward</ta>
            <ta e="T90" id="Seg_2615" s="T89">that</ta>
            <ta e="T91" id="Seg_2616" s="T90">place-DAT/LOC</ta>
            <ta e="T92" id="Seg_2617" s="T91">fly-IMP.1PL</ta>
            <ta e="T93" id="Seg_2618" s="T92">winter.[NOM]</ta>
            <ta e="T94" id="Seg_2619" s="T93">come-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T95" id="Seg_2620" s="T94">this</ta>
            <ta e="T96" id="Seg_2621" s="T95">cold-1PL.[NOM]</ta>
            <ta e="T97" id="Seg_2622" s="T96">pass.by-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T98" id="Seg_2623" s="T97">until</ta>
            <ta e="T99" id="Seg_2624" s="T98">that</ta>
            <ta e="T100" id="Seg_2625" s="T99">place-DAT/LOC</ta>
            <ta e="T101" id="Seg_2626" s="T100">live.permanently-IMP.1PL</ta>
            <ta e="T102" id="Seg_2627" s="T101">here</ta>
            <ta e="T103" id="Seg_2628" s="T102">live-TEMP-1PL</ta>
            <ta e="T104" id="Seg_2629" s="T103">1PL.[NOM]</ta>
            <ta e="T105" id="Seg_2630" s="T104">every</ta>
            <ta e="T106" id="Seg_2631" s="T105">disappear-FUT-1PL</ta>
            <ta e="T107" id="Seg_2632" s="T106">freeze-CVB.SEQ</ta>
            <ta e="T108" id="Seg_2633" s="T107">die-FUT-1PL</ta>
            <ta e="T109" id="Seg_2634" s="T108">there</ta>
            <ta e="T110" id="Seg_2635" s="T109">fly-TEMP-1PL</ta>
            <ta e="T111" id="Seg_2636" s="T110">every.[NOM]</ta>
            <ta e="T112" id="Seg_2637" s="T111">human.being.[NOM]</ta>
            <ta e="T113" id="Seg_2638" s="T112">be-FUT-1PL</ta>
            <ta e="T114" id="Seg_2639" s="T113">well</ta>
            <ta e="T115" id="Seg_2640" s="T114">advise-2PL-ACC</ta>
            <ta e="T116" id="Seg_2641" s="T115">give-EP-IMP.2PL</ta>
            <ta e="T117" id="Seg_2642" s="T116">that-DAT/LOC</ta>
            <ta e="T118" id="Seg_2643" s="T117">bird.[NOM]</ta>
            <ta e="T119" id="Seg_2644" s="T118">every-3SG.[NOM]</ta>
            <ta e="T120" id="Seg_2645" s="T119">well</ta>
            <ta e="T121" id="Seg_2646" s="T120">truth.[NOM]</ta>
            <ta e="T122" id="Seg_2647" s="T121">1PL.[NOM]</ta>
            <ta e="T123" id="Seg_2648" s="T122">every</ta>
            <ta e="T124" id="Seg_2649" s="T123">fly-FUT-1PL</ta>
            <ta e="T125" id="Seg_2650" s="T124">here</ta>
            <ta e="T126" id="Seg_2651" s="T125">die-CAP-1PL</ta>
            <ta e="T127" id="Seg_2652" s="T126">leader.[NOM]</ta>
            <ta e="T128" id="Seg_2653" s="T127">ask-PRS.[3SG]</ta>
            <ta e="T129" id="Seg_2654" s="T128">well</ta>
            <ta e="T130" id="Seg_2655" s="T129">how.much-2PL.[NOM]</ta>
            <ta e="T131" id="Seg_2656" s="T130">agree-PST1-3SG</ta>
            <ta e="T132" id="Seg_2657" s="T131">bird-EP-2SG.[NOM]</ta>
            <ta e="T133" id="Seg_2658" s="T132">every-3SG.[NOM]</ta>
            <ta e="T134" id="Seg_2659" s="T133">agree-PST2.[3SG]</ta>
            <ta e="T135" id="Seg_2660" s="T134">snow.owl.[NOM]</ta>
            <ta e="T136" id="Seg_2661" s="T135">also</ta>
            <ta e="T137" id="Seg_2662" s="T136">word-PROPR.[NOM]</ta>
            <ta e="T138" id="Seg_2663" s="T137">1SG.[NOM]</ta>
            <ta e="T139" id="Seg_2664" s="T138">also</ta>
            <ta e="T140" id="Seg_2665" s="T139">go-PRS-1SG</ta>
            <ta e="T141" id="Seg_2666" s="T140">leader.[NOM]</ta>
            <ta e="T142" id="Seg_2667" s="T141">ask-PRS.[3SG]</ta>
            <ta e="T143" id="Seg_2668" s="T142">well</ta>
            <ta e="T144" id="Seg_2669" s="T143">this</ta>
            <ta e="T145" id="Seg_2670" s="T144">snow.owl.[NOM]</ta>
            <ta e="T146" id="Seg_2671" s="T145">also</ta>
            <ta e="T147" id="Seg_2672" s="T146">come.along-CVB.PURP</ta>
            <ta e="T148" id="Seg_2673" s="T147">want-PRS.[3SG]</ta>
            <ta e="T149" id="Seg_2674" s="T148">agree-PRS-2PL</ta>
            <ta e="T150" id="Seg_2675" s="T149">Q</ta>
            <ta e="T151" id="Seg_2676" s="T150">3SG.[NOM]</ta>
            <ta e="T152" id="Seg_2677" s="T151">come.along-PTCP.PRS-3SG-ACC</ta>
            <ta e="T153" id="Seg_2678" s="T152">that-DAT/LOC</ta>
            <ta e="T154" id="Seg_2679" s="T153">bird-PL-EP-2SG.[NOM]</ta>
            <ta e="T155" id="Seg_2680" s="T154">every-3SG.[NOM]</ta>
            <ta e="T156" id="Seg_2681" s="T155">1PL.[NOM]</ta>
            <ta e="T157" id="Seg_2682" s="T156">snow.owl.[NOM]</ta>
            <ta e="T158" id="Seg_2683" s="T157">come.along-PTCP.PRS-3SG-ACC</ta>
            <ta e="T159" id="Seg_2684" s="T158">agree-NEG-1PL</ta>
            <ta e="T160" id="Seg_2685" s="T159">why</ta>
            <ta e="T161" id="Seg_2686" s="T160">agree-NEG-2PL</ta>
            <ta e="T162" id="Seg_2687" s="T161">face-PROPR-eye-3SG-DAT/LOC</ta>
            <ta e="T163" id="Seg_2688" s="T162">say-EP-IMP.2PL</ta>
            <ta e="T164" id="Seg_2689" s="T163">3SG-ACC</ta>
            <ta e="T165" id="Seg_2690" s="T164">that-DAT/LOC</ta>
            <ta e="T166" id="Seg_2691" s="T165">bird.[NOM]</ta>
            <ta e="T167" id="Seg_2692" s="T166">every-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_2693" s="T167">say-PST1-3SG</ta>
            <ta e="T169" id="Seg_2694" s="T168">here</ta>
            <ta e="T170" id="Seg_2695" s="T169">though</ta>
            <ta e="T171" id="Seg_2696" s="T170">snow.owl.[NOM]</ta>
            <ta e="T172" id="Seg_2697" s="T171">feed-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T173" id="Seg_2698" s="T172">sit-PRS.[3SG]</ta>
            <ta e="T174" id="Seg_2699" s="T173">1PL.[NOM]</ta>
            <ta e="T175" id="Seg_2700" s="T174">EMPH</ta>
            <ta e="T176" id="Seg_2701" s="T175">family-1PL-INSTR</ta>
            <ta e="T177" id="Seg_2702" s="T176">feed.oneself-CVB.SEQ</ta>
            <ta e="T178" id="Seg_2703" s="T177">thither</ta>
            <ta e="T179" id="Seg_2704" s="T178">go-TEMP-3SG</ta>
            <ta e="T180" id="Seg_2705" s="T179">and</ta>
            <ta e="T181" id="Seg_2706" s="T180">that</ta>
            <ta e="T182" id="Seg_2707" s="T181">custom-3SG-ACC</ta>
            <ta e="T183" id="Seg_2708" s="T182">however</ta>
            <ta e="T184" id="Seg_2709" s="T183">throw-FUT.[3SG]</ta>
            <ta e="T185" id="Seg_2710" s="T184">NEG-3SG</ta>
            <ta e="T186" id="Seg_2711" s="T185">1PL.[NOM]</ta>
            <ta e="T187" id="Seg_2712" s="T186">move-NEG-1PL</ta>
            <ta e="T188" id="Seg_2713" s="T187">leader.[NOM]</ta>
            <ta e="T189" id="Seg_2714" s="T188">say-PRS.[3SG]</ta>
            <ta e="T190" id="Seg_2715" s="T189">snow.owl-DAT/LOC</ta>
            <ta e="T191" id="Seg_2716" s="T190">see.[IMP.2SG]</ta>
            <ta e="T192" id="Seg_2717" s="T191">people.[NOM]</ta>
            <ta e="T193" id="Seg_2718" s="T192">disagree-PST1-3SG</ta>
            <ta e="T194" id="Seg_2719" s="T193">bird.[NOM]</ta>
            <ta e="T195" id="Seg_2720" s="T194">every-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_2721" s="T195">speak-PST1-3SG</ta>
            <ta e="T197" id="Seg_2722" s="T196">that.[NOM]</ta>
            <ta e="T198" id="Seg_2723" s="T197">law.[NOM]</ta>
            <ta e="T199" id="Seg_2724" s="T198">this</ta>
            <ta e="T200" id="Seg_2725" s="T199">place-DAT/LOC</ta>
            <ta e="T201" id="Seg_2726" s="T200">stay-PRS-2SG</ta>
            <ta e="T202" id="Seg_2727" s="T201">maybe</ta>
            <ta e="T203" id="Seg_2728" s="T202">freeze-CVB.SEQ</ta>
            <ta e="T204" id="Seg_2729" s="T203">die.[IMP.2SG]</ta>
            <ta e="T205" id="Seg_2730" s="T204">how</ta>
            <ta e="T206" id="Seg_2731" s="T205">think-PRS-2SG</ta>
            <ta e="T207" id="Seg_2732" s="T206">snow.owl.[NOM]</ta>
            <ta e="T208" id="Seg_2733" s="T207">struggle-NEG-1SG</ta>
            <ta e="T209" id="Seg_2734" s="T208">lonely</ta>
            <ta e="T210" id="Seg_2735" s="T209">soul.[NOM]</ta>
            <ta e="T211" id="Seg_2736" s="T210">where</ta>
            <ta e="T212" id="Seg_2737" s="T211">die-NEG.[3SG]</ta>
            <ta e="T213" id="Seg_2738" s="T212">maybe</ta>
            <ta e="T214" id="Seg_2739" s="T213">survive-FUT-1SG</ta>
            <ta e="T215" id="Seg_2740" s="T214">stay-PRS-1SG</ta>
            <ta e="T216" id="Seg_2741" s="T215">say-PRS.[3SG]</ta>
            <ta e="T217" id="Seg_2742" s="T216">bird-PL.[NOM]</ta>
            <ta e="T218" id="Seg_2743" s="T217">every</ta>
            <ta e="T219" id="Seg_2744" s="T218">fly-FREQ-CVB.SEQ</ta>
            <ta e="T220" id="Seg_2745" s="T219">stay-PST2-3PL</ta>
            <ta e="T221" id="Seg_2746" s="T220">that-DAT/LOC</ta>
            <ta e="T222" id="Seg_2747" s="T221">snow.owl.[NOM]</ta>
            <ta e="T223" id="Seg_2748" s="T222">old.woman-3SG-DAT/LOC</ta>
            <ta e="T224" id="Seg_2749" s="T223">come-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_2750" s="T224">well=EMPH</ta>
            <ta e="T226" id="Seg_2751" s="T225">1PL-ACC</ta>
            <ta e="T227" id="Seg_2752" s="T226">bring-NEG-3PL</ta>
            <ta e="T228" id="Seg_2753" s="T227">1SG-DAT/LOC</ta>
            <ta e="T229" id="Seg_2754" s="T228">2SG.[NOM]</ta>
            <ta e="T230" id="Seg_2755" s="T229">furry</ta>
            <ta e="T231" id="Seg_2756" s="T230">very</ta>
            <ta e="T232" id="Seg_2757" s="T231">spotted.seal.[NOM]</ta>
            <ta e="T233" id="Seg_2758" s="T232">long.coat-PART</ta>
            <ta e="T234" id="Seg_2759" s="T233">sew.[IMP.2SG]</ta>
            <ta e="T235" id="Seg_2760" s="T234">snowstorm-DAT/LOC</ta>
            <ta e="T236" id="Seg_2761" s="T235">NEG</ta>
            <ta e="T237" id="Seg_2762" s="T236">flurry-EP-PASS/REFL-EP-NEG.PTCP.[NOM]</ta>
            <ta e="T238" id="Seg_2763" s="T237">cold-DAT/LOC</ta>
            <ta e="T239" id="Seg_2764" s="T238">NEG</ta>
            <ta e="T240" id="Seg_2765" s="T239">freeze-NEG.PTCP.[NOM]</ta>
            <ta e="T241" id="Seg_2766" s="T240">make-CVB.SIM</ta>
            <ta e="T242" id="Seg_2767" s="T241">old.woman-3SG.[NOM]</ta>
            <ta e="T243" id="Seg_2768" s="T242">sew-FUT2-1SG</ta>
            <ta e="T244" id="Seg_2769" s="T243">say-PRS.[3SG]</ta>
            <ta e="T245" id="Seg_2770" s="T244">snow-ACC</ta>
            <ta e="T246" id="Seg_2771" s="T245">with</ta>
            <ta e="T247" id="Seg_2772" s="T246">one</ta>
            <ta e="T248" id="Seg_2773" s="T247">white.[NOM]</ta>
            <ta e="T249" id="Seg_2774" s="T248">be-IMP.3SG</ta>
            <ta e="T250" id="Seg_2775" s="T249">that</ta>
            <ta e="T251" id="Seg_2776" s="T250">long.coat.[NOM]</ta>
            <ta e="T252" id="Seg_2777" s="T251">old.woman-3SG.[NOM]</ta>
            <ta e="T253" id="Seg_2778" s="T252">master.[NOM]</ta>
            <ta e="T254" id="Seg_2779" s="T253">very</ta>
            <ta e="T255" id="Seg_2780" s="T254">sew-PST2.[3SG]</ta>
            <ta e="T256" id="Seg_2781" s="T255">goose.[NOM]</ta>
            <ta e="T257" id="Seg_2782" s="T256">own-3SG-COMP</ta>
            <ta e="T258" id="Seg_2783" s="T257">white</ta>
            <ta e="T259" id="Seg_2784" s="T258">white</ta>
            <ta e="T260" id="Seg_2785" s="T259">long.coat-ACC</ta>
            <ta e="T261" id="Seg_2786" s="T260">snow.owl.[NOM]</ta>
            <ta e="T262" id="Seg_2787" s="T261">this-ACC</ta>
            <ta e="T263" id="Seg_2788" s="T262">dress-PST2-3SG</ta>
            <ta e="T264" id="Seg_2789" s="T263">like.wax</ta>
            <ta e="T265" id="Seg_2790" s="T264">long.coat-3SG.[NOM]</ta>
            <ta e="T266" id="Seg_2791" s="T265">body-3SG-DAT/LOC</ta>
            <ta e="T267" id="Seg_2792" s="T266">turn.around-CVB.SIM</ta>
            <ta e="T268" id="Seg_2793" s="T267">grab-PST2.[3SG]</ta>
            <ta e="T269" id="Seg_2794" s="T268">cold-DAT/LOC</ta>
            <ta e="T270" id="Seg_2795" s="T269">NEG</ta>
            <ta e="T271" id="Seg_2796" s="T270">freeze-NEG.[3SG]</ta>
            <ta e="T272" id="Seg_2797" s="T271">snowstorm-DAT/LOC</ta>
            <ta e="T273" id="Seg_2798" s="T272">NEG</ta>
            <ta e="T274" id="Seg_2799" s="T273">stick-EP-NEG.[3SG]</ta>
            <ta e="T275" id="Seg_2800" s="T274">snow.[NOM]</ta>
            <ta e="T276" id="Seg_2801" s="T275">NEG</ta>
            <ta e="T277" id="Seg_2802" s="T276">stick-EP-NEG.[3SG]</ta>
            <ta e="T278" id="Seg_2803" s="T277">3SG-DAT/LOC</ta>
            <ta e="T279" id="Seg_2804" s="T278">snow.owl.[NOM]</ta>
            <ta e="T280" id="Seg_2805" s="T279">from.then.on</ta>
            <ta e="T281" id="Seg_2806" s="T280">this</ta>
            <ta e="T282" id="Seg_2807" s="T281">place-DAT/LOC</ta>
            <ta e="T283" id="Seg_2808" s="T282">country-VBZ-PST2-3SG</ta>
            <ta e="T284" id="Seg_2809" s="T283">that</ta>
            <ta e="T285" id="Seg_2810" s="T284">stay-PTCP.PST-3SG-ACC</ta>
            <ta e="T286" id="Seg_2811" s="T285">take.revenge-CVB.SEQ</ta>
            <ta e="T287" id="Seg_2812" s="T286">snow.owl.[NOM]</ta>
            <ta e="T288" id="Seg_2813" s="T287">small</ta>
            <ta e="T289" id="Seg_2814" s="T288">bird-ACC</ta>
            <ta e="T290" id="Seg_2815" s="T289">whole-3SG-ACC</ta>
            <ta e="T291" id="Seg_2816" s="T290">still</ta>
            <ta e="T292" id="Seg_2817" s="T291">eat-PTCP.PRS</ta>
            <ta e="T293" id="Seg_2818" s="T292">become-PST2-3SG</ta>
            <ta e="T294" id="Seg_2819" s="T293">better</ta>
            <ta e="T295" id="Seg_2820" s="T294">3SG-ACC</ta>
            <ta e="T296" id="Seg_2821" s="T295">with</ta>
            <ta e="T297" id="Seg_2822" s="T296">winter-VBZ-PTCP.PRS</ta>
            <ta e="T298" id="Seg_2823" s="T297">partridge-ACC</ta>
            <ta e="T299" id="Seg_2824" s="T298">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gd">
            <ta e="T1" id="Seg_2825" s="T0">vor.langer.Zeit</ta>
            <ta e="T2" id="Seg_2826" s="T1">Gans-EP-2SG.[NOM]</ta>
            <ta e="T3" id="Seg_2827" s="T2">Ente-EP-2SG.[NOM]</ta>
            <ta e="T4" id="Seg_2828" s="T3">Vögelchen-EP-2SG.[NOM]</ta>
            <ta e="T5" id="Seg_2829" s="T4">Schneeeule-EP-2SG.[NOM]</ta>
            <ta e="T6" id="Seg_2830" s="T5">jeder</ta>
            <ta e="T7" id="Seg_2831" s="T6">Vogel.[NOM]</ta>
            <ta e="T8" id="Seg_2832" s="T7">dieses</ta>
            <ta e="T9" id="Seg_2833" s="T8">Erde-DAT/LOC</ta>
            <ta e="T10" id="Seg_2834" s="T9">Erde-PROPR.[NOM]</ta>
            <ta e="T11" id="Seg_2835" s="T10">sein-PST1-3SG</ta>
            <ta e="T12" id="Seg_2836" s="T11">dieses</ta>
            <ta e="T13" id="Seg_2837" s="T12">Erde-EP-2SG.[NOM]</ta>
            <ta e="T14" id="Seg_2838" s="T13">jenes</ta>
            <ta e="T15" id="Seg_2839" s="T14">Zeit-DAT/LOC</ta>
            <ta e="T16" id="Seg_2840" s="T15">warm.[NOM]</ta>
            <ta e="T17" id="Seg_2841" s="T16">man.sagt</ta>
            <ta e="T18" id="Seg_2842" s="T17">dann</ta>
            <ta e="T19" id="Seg_2843" s="T18">Kälte-PL.[NOM]</ta>
            <ta e="T20" id="Seg_2844" s="T19">sein-PST1-3PL</ta>
            <ta e="T21" id="Seg_2845" s="T20">Jahr.[NOM]-Tag.[NOM]</ta>
            <ta e="T22" id="Seg_2846" s="T21">sich.ändern-PST1-3SG</ta>
            <ta e="T23" id="Seg_2847" s="T22">dieses-DAT/LOC</ta>
            <ta e="T24" id="Seg_2848" s="T23">Vogel-EP-2SG.[NOM]</ta>
            <ta e="T25" id="Seg_2849" s="T24">jeder-3SG.[NOM]</ta>
            <ta e="T26" id="Seg_2850" s="T25">raten-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T27" id="Seg_2851" s="T26">jeder-3PL-DAT/LOC</ta>
            <ta e="T28" id="Seg_2852" s="T27">eins</ta>
            <ta e="T29" id="Seg_2853" s="T28">Anführer.[NOM]</ta>
            <ta e="T30" id="Seg_2854" s="T29">es.gibt</ta>
            <ta e="T31" id="Seg_2855" s="T30">Schneeeule-EP-2SG.[NOM]</ta>
            <ta e="T32" id="Seg_2856" s="T31">von.da.an</ta>
            <ta e="T33" id="Seg_2857" s="T32">Rebhuhn-ACC</ta>
            <ta e="T34" id="Seg_2858" s="T33">essen-HAB.[3SG]</ta>
            <ta e="T35" id="Seg_2859" s="T34">Ente-ACC</ta>
            <ta e="T36" id="Seg_2860" s="T35">EMPH</ta>
            <ta e="T37" id="Seg_2861" s="T36">Gans-ACC</ta>
            <ta e="T38" id="Seg_2862" s="T37">EMPH</ta>
            <ta e="T39" id="Seg_2863" s="T38">auch</ta>
            <ta e="T40" id="Seg_2864" s="T39">dieses</ta>
            <ta e="T41" id="Seg_2865" s="T40">Vogel-PL.[NOM]</ta>
            <ta e="T42" id="Seg_2866" s="T41">Anführer-3PL.[NOM]</ta>
            <ta e="T43" id="Seg_2867" s="T42">3SG.[NOM]</ta>
            <ta e="T44" id="Seg_2868" s="T43">Gans-PL-ABL</ta>
            <ta e="T45" id="Seg_2869" s="T44">sein-PST1-3SG</ta>
            <ta e="T46" id="Seg_2870" s="T45">Vogel-ACC</ta>
            <ta e="T47" id="Seg_2871" s="T46">jeder-3SG-ACC</ta>
            <ta e="T48" id="Seg_2872" s="T47">Versammlung-DAT/LOC</ta>
            <ta e="T49" id="Seg_2873" s="T48">rufen-PRS.[3SG]</ta>
            <ta e="T50" id="Seg_2874" s="T49">eins</ta>
            <ta e="T51" id="Seg_2875" s="T50">Tal-ACC</ta>
            <ta e="T52" id="Seg_2876" s="T51">bewaldeter.Berg-ACC</ta>
            <ta e="T53" id="Seg_2877" s="T52">voll</ta>
            <ta e="T54" id="Seg_2878" s="T53">Vogel.[NOM]</ta>
            <ta e="T55" id="Seg_2879" s="T54">sammeln-EP-MED-PST1-3SG</ta>
            <ta e="T56" id="Seg_2880" s="T55">Schneeeule-EP-2SG.[NOM]</ta>
            <ta e="T57" id="Seg_2881" s="T56">auch</ta>
            <ta e="T58" id="Seg_2882" s="T57">kommen-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_2883" s="T58">Anführer.[NOM]</ta>
            <ta e="T60" id="Seg_2884" s="T59">sprechen-PRS.[3SG]</ta>
            <ta e="T61" id="Seg_2885" s="T60">doch</ta>
            <ta e="T62" id="Seg_2886" s="T61">Jahr.[NOM]-Tag.[NOM]</ta>
            <ta e="T63" id="Seg_2887" s="T62">sich.ändern-PST1-3SG</ta>
            <ta e="T64" id="Seg_2888" s="T63">1PL.[NOM]</ta>
            <ta e="T65" id="Seg_2889" s="T64">im.Sommer</ta>
            <ta e="T66" id="Seg_2890" s="T65">nur</ta>
            <ta e="T67" id="Seg_2891" s="T66">leben-PTCP.PRS</ta>
            <ta e="T68" id="Seg_2892" s="T67">sein-PST1-1PL</ta>
            <ta e="T69" id="Seg_2893" s="T68">1SG.[NOM]</ta>
            <ta e="T70" id="Seg_2894" s="T69">wissen-PRS-1SG</ta>
            <ta e="T71" id="Seg_2895" s="T70">was.für.ein</ta>
            <ta e="T72" id="Seg_2896" s="T71">INDEF</ta>
            <ta e="T73" id="Seg_2897" s="T72">Erde-DAT/LOC</ta>
            <ta e="T74" id="Seg_2898" s="T73">Winter.[NOM]</ta>
            <ta e="T75" id="Seg_2899" s="T74">sein-NEG.PTCP</ta>
            <ta e="T76" id="Seg_2900" s="T75">Land-3SG.[NOM]</ta>
            <ta e="T77" id="Seg_2901" s="T76">es.gibt</ta>
            <ta e="T78" id="Seg_2902" s="T77">1PL.[NOM]</ta>
            <ta e="T79" id="Seg_2903" s="T78">Flügel-PROPR-PL.[NOM]</ta>
            <ta e="T80" id="Seg_2904" s="T79">warum</ta>
            <ta e="T81" id="Seg_2905" s="T80">dieses</ta>
            <ta e="T82" id="Seg_2906" s="T81">Erde-DAT/LOC</ta>
            <ta e="T83" id="Seg_2907" s="T82">frieren-CVB.SIM</ta>
            <ta e="T84" id="Seg_2908" s="T83">liegen-FUT-1PL=Q</ta>
            <ta e="T85" id="Seg_2909" s="T84">Erde-3SG-INSTR</ta>
            <ta e="T86" id="Seg_2910" s="T85">gehen-PTCP.COND-DAT/LOC</ta>
            <ta e="T87" id="Seg_2911" s="T86">fern.[NOM]</ta>
            <ta e="T88" id="Seg_2912" s="T87">1PL.[NOM]</ta>
            <ta e="T89" id="Seg_2913" s="T88">geradeaus</ta>
            <ta e="T90" id="Seg_2914" s="T89">jenes</ta>
            <ta e="T91" id="Seg_2915" s="T90">Ort-DAT/LOC</ta>
            <ta e="T92" id="Seg_2916" s="T91">fliegen-IMP.1PL</ta>
            <ta e="T93" id="Seg_2917" s="T92">Winter.[NOM]</ta>
            <ta e="T94" id="Seg_2918" s="T93">kommen-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T95" id="Seg_2919" s="T94">dieses</ta>
            <ta e="T96" id="Seg_2920" s="T95">Kälte-1PL.[NOM]</ta>
            <ta e="T97" id="Seg_2921" s="T96">vorbeigehen-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T98" id="Seg_2922" s="T97">bis.zu</ta>
            <ta e="T99" id="Seg_2923" s="T98">jenes</ta>
            <ta e="T100" id="Seg_2924" s="T99">Ort-DAT/LOC</ta>
            <ta e="T101" id="Seg_2925" s="T100">dauerhaft.wohnen-IMP.1PL</ta>
            <ta e="T102" id="Seg_2926" s="T101">hier</ta>
            <ta e="T103" id="Seg_2927" s="T102">leben-TEMP-1PL</ta>
            <ta e="T104" id="Seg_2928" s="T103">1PL.[NOM]</ta>
            <ta e="T105" id="Seg_2929" s="T104">jeder</ta>
            <ta e="T106" id="Seg_2930" s="T105">untergehen-FUT-1PL</ta>
            <ta e="T107" id="Seg_2931" s="T106">frieren-CVB.SEQ</ta>
            <ta e="T108" id="Seg_2932" s="T107">sterben-FUT-1PL</ta>
            <ta e="T109" id="Seg_2933" s="T108">dorthin</ta>
            <ta e="T110" id="Seg_2934" s="T109">fliegen-TEMP-1PL</ta>
            <ta e="T111" id="Seg_2935" s="T110">jeder.[NOM]</ta>
            <ta e="T112" id="Seg_2936" s="T111">Mensch.[NOM]</ta>
            <ta e="T113" id="Seg_2937" s="T112">sein-FUT-1PL</ta>
            <ta e="T114" id="Seg_2938" s="T113">doch</ta>
            <ta e="T115" id="Seg_2939" s="T114">Rat-2PL-ACC</ta>
            <ta e="T116" id="Seg_2940" s="T115">geben-EP-IMP.2PL</ta>
            <ta e="T117" id="Seg_2941" s="T116">jenes-DAT/LOC</ta>
            <ta e="T118" id="Seg_2942" s="T117">Vogel.[NOM]</ta>
            <ta e="T119" id="Seg_2943" s="T118">jeder-3SG.[NOM]</ta>
            <ta e="T120" id="Seg_2944" s="T119">doch</ta>
            <ta e="T121" id="Seg_2945" s="T120">Wahrheit.[NOM]</ta>
            <ta e="T122" id="Seg_2946" s="T121">1PL.[NOM]</ta>
            <ta e="T123" id="Seg_2947" s="T122">jeder</ta>
            <ta e="T124" id="Seg_2948" s="T123">fliegen-FUT-1PL</ta>
            <ta e="T125" id="Seg_2949" s="T124">hier</ta>
            <ta e="T126" id="Seg_2950" s="T125">sterben-CAP-1PL</ta>
            <ta e="T127" id="Seg_2951" s="T126">Anführer.[NOM]</ta>
            <ta e="T128" id="Seg_2952" s="T127">fragen-PRS.[3SG]</ta>
            <ta e="T129" id="Seg_2953" s="T128">doch</ta>
            <ta e="T130" id="Seg_2954" s="T129">wie.viel-2PL.[NOM]</ta>
            <ta e="T131" id="Seg_2955" s="T130">einverstanden.sein-PST1-3SG</ta>
            <ta e="T132" id="Seg_2956" s="T131">Vogel-EP-2SG.[NOM]</ta>
            <ta e="T133" id="Seg_2957" s="T132">jeder-3SG.[NOM]</ta>
            <ta e="T134" id="Seg_2958" s="T133">einverstanden.sein-PST2.[3SG]</ta>
            <ta e="T135" id="Seg_2959" s="T134">Schneeeule.[NOM]</ta>
            <ta e="T136" id="Seg_2960" s="T135">auch</ta>
            <ta e="T137" id="Seg_2961" s="T136">Wort-PROPR.[NOM]</ta>
            <ta e="T138" id="Seg_2962" s="T137">1SG.[NOM]</ta>
            <ta e="T139" id="Seg_2963" s="T138">auch</ta>
            <ta e="T140" id="Seg_2964" s="T139">gehen-PRS-1SG</ta>
            <ta e="T141" id="Seg_2965" s="T140">Anführer.[NOM]</ta>
            <ta e="T142" id="Seg_2966" s="T141">fragen-PRS.[3SG]</ta>
            <ta e="T143" id="Seg_2967" s="T142">na</ta>
            <ta e="T144" id="Seg_2968" s="T143">dieses</ta>
            <ta e="T145" id="Seg_2969" s="T144">Schneeeule.[NOM]</ta>
            <ta e="T146" id="Seg_2970" s="T145">auch</ta>
            <ta e="T147" id="Seg_2971" s="T146">mitkommen-CVB.PURP</ta>
            <ta e="T148" id="Seg_2972" s="T147">wollen-PRS.[3SG]</ta>
            <ta e="T149" id="Seg_2973" s="T148">einverstanden.sein-PRS-2PL</ta>
            <ta e="T150" id="Seg_2974" s="T149">Q</ta>
            <ta e="T151" id="Seg_2975" s="T150">3SG.[NOM]</ta>
            <ta e="T152" id="Seg_2976" s="T151">mitkommen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T153" id="Seg_2977" s="T152">jenes-DAT/LOC</ta>
            <ta e="T154" id="Seg_2978" s="T153">Vogel-PL-EP-2SG.[NOM]</ta>
            <ta e="T155" id="Seg_2979" s="T154">jeder-3SG.[NOM]</ta>
            <ta e="T156" id="Seg_2980" s="T155">1PL.[NOM]</ta>
            <ta e="T157" id="Seg_2981" s="T156">Schneeeule.[NOM]</ta>
            <ta e="T158" id="Seg_2982" s="T157">mitkommen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T159" id="Seg_2983" s="T158">einverstanden.sein-NEG-1PL</ta>
            <ta e="T160" id="Seg_2984" s="T159">warum</ta>
            <ta e="T161" id="Seg_2985" s="T160">einverstanden.sein-NEG-2PL</ta>
            <ta e="T162" id="Seg_2986" s="T161">Gesicht-PROPR-Auge-3SG-DAT/LOC</ta>
            <ta e="T163" id="Seg_2987" s="T162">sprechen-EP-IMP.2PL</ta>
            <ta e="T164" id="Seg_2988" s="T163">3SG-ACC</ta>
            <ta e="T165" id="Seg_2989" s="T164">jenes-DAT/LOC</ta>
            <ta e="T166" id="Seg_2990" s="T165">Vogel.[NOM]</ta>
            <ta e="T167" id="Seg_2991" s="T166">jeder-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_2992" s="T167">sagen-PST1-3SG</ta>
            <ta e="T169" id="Seg_2993" s="T168">hier</ta>
            <ta e="T170" id="Seg_2994" s="T169">aber</ta>
            <ta e="T171" id="Seg_2995" s="T170">Schneeeule.[NOM]</ta>
            <ta e="T172" id="Seg_2996" s="T171">füttern-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T173" id="Seg_2997" s="T172">sitzen-PRS.[3SG]</ta>
            <ta e="T174" id="Seg_2998" s="T173">1PL.[NOM]</ta>
            <ta e="T175" id="Seg_2999" s="T174">EMPH</ta>
            <ta e="T176" id="Seg_3000" s="T175">Familie-1PL-INSTR</ta>
            <ta e="T177" id="Seg_3001" s="T176">sich.ernähren-CVB.SEQ</ta>
            <ta e="T178" id="Seg_3002" s="T177">dorthin</ta>
            <ta e="T179" id="Seg_3003" s="T178">gehen-TEMP-3SG</ta>
            <ta e="T180" id="Seg_3004" s="T179">und</ta>
            <ta e="T181" id="Seg_3005" s="T180">jenes</ta>
            <ta e="T182" id="Seg_3006" s="T181">Brauch-3SG-ACC</ta>
            <ta e="T183" id="Seg_3007" s="T182">doch</ta>
            <ta e="T184" id="Seg_3008" s="T183">werfen-FUT.[3SG]</ta>
            <ta e="T185" id="Seg_3009" s="T184">NEG-3SG</ta>
            <ta e="T186" id="Seg_3010" s="T185">1PL.[NOM]</ta>
            <ta e="T187" id="Seg_3011" s="T186">bewegen-NEG-1PL</ta>
            <ta e="T188" id="Seg_3012" s="T187">Anführer.[NOM]</ta>
            <ta e="T189" id="Seg_3013" s="T188">sagen-PRS.[3SG]</ta>
            <ta e="T190" id="Seg_3014" s="T189">Schneeeule-DAT/LOC</ta>
            <ta e="T191" id="Seg_3015" s="T190">sehen.[IMP.2SG]</ta>
            <ta e="T192" id="Seg_3016" s="T191">Leute.[NOM]</ta>
            <ta e="T193" id="Seg_3017" s="T192">nicht.einverstanden.sein-PST1-3SG</ta>
            <ta e="T194" id="Seg_3018" s="T193">Vogel.[NOM]</ta>
            <ta e="T195" id="Seg_3019" s="T194">jeder-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_3020" s="T195">sprechen-PST1-3SG</ta>
            <ta e="T197" id="Seg_3021" s="T196">jenes.[NOM]</ta>
            <ta e="T198" id="Seg_3022" s="T197">Gesetz.[NOM]</ta>
            <ta e="T199" id="Seg_3023" s="T198">dieses</ta>
            <ta e="T200" id="Seg_3024" s="T199">Ort-DAT/LOC</ta>
            <ta e="T201" id="Seg_3025" s="T200">bleiben-PRS-2SG</ta>
            <ta e="T202" id="Seg_3026" s="T201">vielleicht</ta>
            <ta e="T203" id="Seg_3027" s="T202">frieren-CVB.SEQ</ta>
            <ta e="T204" id="Seg_3028" s="T203">sterben.[IMP.2SG]</ta>
            <ta e="T205" id="Seg_3029" s="T204">wie</ta>
            <ta e="T206" id="Seg_3030" s="T205">denken-PRS-2SG</ta>
            <ta e="T207" id="Seg_3031" s="T206">Schneeeule.[NOM]</ta>
            <ta e="T208" id="Seg_3032" s="T207">streiten-NEG-1SG</ta>
            <ta e="T209" id="Seg_3033" s="T208">einsam</ta>
            <ta e="T210" id="Seg_3034" s="T209">Seele.[NOM]</ta>
            <ta e="T211" id="Seg_3035" s="T210">wo</ta>
            <ta e="T212" id="Seg_3036" s="T211">sterben-NEG.[3SG]</ta>
            <ta e="T213" id="Seg_3037" s="T212">vielleicht</ta>
            <ta e="T214" id="Seg_3038" s="T213">überleben-FUT-1SG</ta>
            <ta e="T215" id="Seg_3039" s="T214">bleiben-PRS-1SG</ta>
            <ta e="T216" id="Seg_3040" s="T215">sagen-PRS.[3SG]</ta>
            <ta e="T217" id="Seg_3041" s="T216">Vogel-PL.[NOM]</ta>
            <ta e="T218" id="Seg_3042" s="T217">jeder</ta>
            <ta e="T219" id="Seg_3043" s="T218">fliegen-FREQ-CVB.SEQ</ta>
            <ta e="T220" id="Seg_3044" s="T219">bleiben-PST2-3PL</ta>
            <ta e="T221" id="Seg_3045" s="T220">jenes-DAT/LOC</ta>
            <ta e="T222" id="Seg_3046" s="T221">Schneeeule.[NOM]</ta>
            <ta e="T223" id="Seg_3047" s="T222">Alte-3SG-DAT/LOC</ta>
            <ta e="T224" id="Seg_3048" s="T223">kommen-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_3049" s="T224">na=EMPH</ta>
            <ta e="T226" id="Seg_3050" s="T225">1PL-ACC</ta>
            <ta e="T227" id="Seg_3051" s="T226">bringen-NEG-3PL</ta>
            <ta e="T228" id="Seg_3052" s="T227">1SG-DAT/LOC</ta>
            <ta e="T229" id="Seg_3053" s="T228">2SG.[NOM]</ta>
            <ta e="T230" id="Seg_3054" s="T229">pelzig</ta>
            <ta e="T231" id="Seg_3055" s="T230">sehr</ta>
            <ta e="T232" id="Seg_3056" s="T231">Largarobbe.[NOM]</ta>
            <ta e="T233" id="Seg_3057" s="T232">langer.Mantel-PART</ta>
            <ta e="T234" id="Seg_3058" s="T233">nähen.[IMP.2SG]</ta>
            <ta e="T235" id="Seg_3059" s="T234">Schneesturm-DAT/LOC</ta>
            <ta e="T236" id="Seg_3060" s="T235">NEG</ta>
            <ta e="T237" id="Seg_3061" s="T236">stöbern-EP-PASS/REFL-EP-NEG.PTCP.[NOM]</ta>
            <ta e="T238" id="Seg_3062" s="T237">Kälte-DAT/LOC</ta>
            <ta e="T239" id="Seg_3063" s="T238">NEG</ta>
            <ta e="T240" id="Seg_3064" s="T239">frieren-NEG.PTCP.[NOM]</ta>
            <ta e="T241" id="Seg_3065" s="T240">machen-CVB.SIM</ta>
            <ta e="T242" id="Seg_3066" s="T241">Alte-3SG.[NOM]</ta>
            <ta e="T243" id="Seg_3067" s="T242">nähen-FUT2-1SG</ta>
            <ta e="T244" id="Seg_3068" s="T243">sagen-PRS.[3SG]</ta>
            <ta e="T245" id="Seg_3069" s="T244">Schnee-ACC</ta>
            <ta e="T246" id="Seg_3070" s="T245">mit</ta>
            <ta e="T247" id="Seg_3071" s="T246">eins</ta>
            <ta e="T248" id="Seg_3072" s="T247">weiß.[NOM]</ta>
            <ta e="T249" id="Seg_3073" s="T248">sein-IMP.3SG</ta>
            <ta e="T250" id="Seg_3074" s="T249">jenes</ta>
            <ta e="T251" id="Seg_3075" s="T250">langer.Mantel.[NOM]</ta>
            <ta e="T252" id="Seg_3076" s="T251">Alte-3SG.[NOM]</ta>
            <ta e="T253" id="Seg_3077" s="T252">Meister.[NOM]</ta>
            <ta e="T254" id="Seg_3078" s="T253">sehr</ta>
            <ta e="T255" id="Seg_3079" s="T254">nähen-PST2.[3SG]</ta>
            <ta e="T256" id="Seg_3080" s="T255">Gans.[NOM]</ta>
            <ta e="T257" id="Seg_3081" s="T256">eigen-3SG-COMP</ta>
            <ta e="T258" id="Seg_3082" s="T257">weiß</ta>
            <ta e="T259" id="Seg_3083" s="T258">weiß</ta>
            <ta e="T260" id="Seg_3084" s="T259">langer.Mantel-ACC</ta>
            <ta e="T261" id="Seg_3085" s="T260">Schneeeule.[NOM]</ta>
            <ta e="T262" id="Seg_3086" s="T261">dieses-ACC</ta>
            <ta e="T263" id="Seg_3087" s="T262">anziehen-PST2-3SG</ta>
            <ta e="T264" id="Seg_3088" s="T263">wie.angegossen</ta>
            <ta e="T265" id="Seg_3089" s="T264">langer.Mantel-3SG.[NOM]</ta>
            <ta e="T266" id="Seg_3090" s="T265">Körper-3SG-DAT/LOC</ta>
            <ta e="T267" id="Seg_3091" s="T266">umdrehen-CVB.SIM</ta>
            <ta e="T268" id="Seg_3092" s="T267">greifen-PST2.[3SG]</ta>
            <ta e="T269" id="Seg_3093" s="T268">Kälte-DAT/LOC</ta>
            <ta e="T270" id="Seg_3094" s="T269">NEG</ta>
            <ta e="T271" id="Seg_3095" s="T270">frieren-NEG.[3SG]</ta>
            <ta e="T272" id="Seg_3096" s="T271">Schneesturm-DAT/LOC</ta>
            <ta e="T273" id="Seg_3097" s="T272">NEG</ta>
            <ta e="T274" id="Seg_3098" s="T273">haften-EP-NEG.[3SG]</ta>
            <ta e="T275" id="Seg_3099" s="T274">Schnee.[NOM]</ta>
            <ta e="T276" id="Seg_3100" s="T275">NEG</ta>
            <ta e="T277" id="Seg_3101" s="T276">haften-EP-NEG.[3SG]</ta>
            <ta e="T278" id="Seg_3102" s="T277">3SG-DAT/LOC</ta>
            <ta e="T279" id="Seg_3103" s="T278">Schneeeule.[NOM]</ta>
            <ta e="T280" id="Seg_3104" s="T279">von.da.an</ta>
            <ta e="T281" id="Seg_3105" s="T280">dieses</ta>
            <ta e="T282" id="Seg_3106" s="T281">Ort-DAT/LOC</ta>
            <ta e="T283" id="Seg_3107" s="T282">Land-VBZ-PST2-3SG</ta>
            <ta e="T284" id="Seg_3108" s="T283">jenes</ta>
            <ta e="T285" id="Seg_3109" s="T284">bleiben-PTCP.PST-3SG-ACC</ta>
            <ta e="T286" id="Seg_3110" s="T285">sich.rächen-CVB.SEQ</ta>
            <ta e="T287" id="Seg_3111" s="T286">Schneeeule.[NOM]</ta>
            <ta e="T288" id="Seg_3112" s="T287">klein</ta>
            <ta e="T289" id="Seg_3113" s="T288">Vogel-ACC</ta>
            <ta e="T290" id="Seg_3114" s="T289">ganz-3SG-ACC</ta>
            <ta e="T291" id="Seg_3115" s="T290">noch</ta>
            <ta e="T292" id="Seg_3116" s="T291">essen-PTCP.PRS</ta>
            <ta e="T293" id="Seg_3117" s="T292">werden-PST2-3SG</ta>
            <ta e="T294" id="Seg_3118" s="T293">besser</ta>
            <ta e="T295" id="Seg_3119" s="T294">3SG-ACC</ta>
            <ta e="T296" id="Seg_3120" s="T295">mit</ta>
            <ta e="T297" id="Seg_3121" s="T296">Winter-VBZ-PTCP.PRS</ta>
            <ta e="T298" id="Seg_3122" s="T297">Rebhuhn-ACC</ta>
            <ta e="T299" id="Seg_3123" s="T298">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3124" s="T0">давно</ta>
            <ta e="T2" id="Seg_3125" s="T1">гусь-EP-2SG.[NOM]</ta>
            <ta e="T3" id="Seg_3126" s="T2">утка-EP-2SG.[NOM]</ta>
            <ta e="T4" id="Seg_3127" s="T3">птичка-EP-2SG.[NOM]</ta>
            <ta e="T5" id="Seg_3128" s="T4">полярная.сова-EP-2SG.[NOM]</ta>
            <ta e="T6" id="Seg_3129" s="T5">каждый</ta>
            <ta e="T7" id="Seg_3130" s="T6">птица.[NOM]</ta>
            <ta e="T8" id="Seg_3131" s="T7">этот</ta>
            <ta e="T9" id="Seg_3132" s="T8">земля-DAT/LOC</ta>
            <ta e="T10" id="Seg_3133" s="T9">земля-PROPR.[NOM]</ta>
            <ta e="T11" id="Seg_3134" s="T10">быть-PST1-3SG</ta>
            <ta e="T12" id="Seg_3135" s="T11">этот</ta>
            <ta e="T13" id="Seg_3136" s="T12">земля-EP-2SG.[NOM]</ta>
            <ta e="T14" id="Seg_3137" s="T13">тот</ta>
            <ta e="T15" id="Seg_3138" s="T14">час-DAT/LOC</ta>
            <ta e="T16" id="Seg_3139" s="T15">теплый.[NOM]</ta>
            <ta e="T17" id="Seg_3140" s="T16">говорят</ta>
            <ta e="T18" id="Seg_3141" s="T17">потом</ta>
            <ta e="T19" id="Seg_3142" s="T18">холод-PL.[NOM]</ta>
            <ta e="T20" id="Seg_3143" s="T19">быть-PST1-3PL</ta>
            <ta e="T21" id="Seg_3144" s="T20">год.[NOM]-день.[NOM]</ta>
            <ta e="T22" id="Seg_3145" s="T21">измениться-PST1-3SG</ta>
            <ta e="T23" id="Seg_3146" s="T22">этот-DAT/LOC</ta>
            <ta e="T24" id="Seg_3147" s="T23">птица-EP-2SG.[NOM]</ta>
            <ta e="T25" id="Seg_3148" s="T24">каждый-3SG.[NOM]</ta>
            <ta e="T26" id="Seg_3149" s="T25">советовать-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T27" id="Seg_3150" s="T26">каждый-3PL-DAT/LOC</ta>
            <ta e="T28" id="Seg_3151" s="T27">один</ta>
            <ta e="T29" id="Seg_3152" s="T28">руководитель.[NOM]</ta>
            <ta e="T30" id="Seg_3153" s="T29">есть</ta>
            <ta e="T31" id="Seg_3154" s="T30">полярная.сова-EP-2SG.[NOM]</ta>
            <ta e="T32" id="Seg_3155" s="T31">с.тех.пор</ta>
            <ta e="T33" id="Seg_3156" s="T32">куропатка-ACC</ta>
            <ta e="T34" id="Seg_3157" s="T33">есть-HAB.[3SG]</ta>
            <ta e="T35" id="Seg_3158" s="T34">утка-ACC</ta>
            <ta e="T36" id="Seg_3159" s="T35">EMPH</ta>
            <ta e="T37" id="Seg_3160" s="T36">гусь-ACC</ta>
            <ta e="T38" id="Seg_3161" s="T37">EMPH</ta>
            <ta e="T39" id="Seg_3162" s="T38">тоже</ta>
            <ta e="T40" id="Seg_3163" s="T39">этот</ta>
            <ta e="T41" id="Seg_3164" s="T40">птица-PL.[NOM]</ta>
            <ta e="T42" id="Seg_3165" s="T41">руководитель-3PL.[NOM]</ta>
            <ta e="T43" id="Seg_3166" s="T42">3SG.[NOM]</ta>
            <ta e="T44" id="Seg_3167" s="T43">гусь-PL-ABL</ta>
            <ta e="T45" id="Seg_3168" s="T44">быть-PST1-3SG</ta>
            <ta e="T46" id="Seg_3169" s="T45">птица-ACC</ta>
            <ta e="T47" id="Seg_3170" s="T46">каждый-3SG-ACC</ta>
            <ta e="T48" id="Seg_3171" s="T47">сход-DAT/LOC</ta>
            <ta e="T49" id="Seg_3172" s="T48">звать-PRS.[3SG]</ta>
            <ta e="T50" id="Seg_3173" s="T49">один</ta>
            <ta e="T51" id="Seg_3174" s="T50">долина-ACC</ta>
            <ta e="T52" id="Seg_3175" s="T51">лесистая.гора-ACC</ta>
            <ta e="T53" id="Seg_3176" s="T52">полный</ta>
            <ta e="T54" id="Seg_3177" s="T53">птица.[NOM]</ta>
            <ta e="T55" id="Seg_3178" s="T54">собирать-EP-MED-PST1-3SG</ta>
            <ta e="T56" id="Seg_3179" s="T55">полярная.сова-EP-2SG.[NOM]</ta>
            <ta e="T57" id="Seg_3180" s="T56">тоже</ta>
            <ta e="T58" id="Seg_3181" s="T57">приходить-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_3182" s="T58">руководитель.[NOM]</ta>
            <ta e="T60" id="Seg_3183" s="T59">говорить-PRS.[3SG]</ta>
            <ta e="T61" id="Seg_3184" s="T60">вот</ta>
            <ta e="T62" id="Seg_3185" s="T61">год.[NOM]-день.[NOM]</ta>
            <ta e="T63" id="Seg_3186" s="T62">измениться-PST1-3SG</ta>
            <ta e="T64" id="Seg_3187" s="T63">1PL.[NOM]</ta>
            <ta e="T65" id="Seg_3188" s="T64">летом</ta>
            <ta e="T66" id="Seg_3189" s="T65">только</ta>
            <ta e="T67" id="Seg_3190" s="T66">жить-PTCP.PRS</ta>
            <ta e="T68" id="Seg_3191" s="T67">быть-PST1-1PL</ta>
            <ta e="T69" id="Seg_3192" s="T68">1SG.[NOM]</ta>
            <ta e="T70" id="Seg_3193" s="T69">знать-PRS-1SG</ta>
            <ta e="T71" id="Seg_3194" s="T70">какой</ta>
            <ta e="T72" id="Seg_3195" s="T71">INDEF</ta>
            <ta e="T73" id="Seg_3196" s="T72">земля-DAT/LOC</ta>
            <ta e="T74" id="Seg_3197" s="T73">зима.[NOM]</ta>
            <ta e="T75" id="Seg_3198" s="T74">быть-NEG.PTCP</ta>
            <ta e="T76" id="Seg_3199" s="T75">страна-3SG.[NOM]</ta>
            <ta e="T77" id="Seg_3200" s="T76">есть</ta>
            <ta e="T78" id="Seg_3201" s="T77">1PL.[NOM]</ta>
            <ta e="T79" id="Seg_3202" s="T78">крыло-PROPR-PL.[NOM]</ta>
            <ta e="T80" id="Seg_3203" s="T79">почему</ta>
            <ta e="T81" id="Seg_3204" s="T80">этот</ta>
            <ta e="T82" id="Seg_3205" s="T81">земля-DAT/LOC</ta>
            <ta e="T83" id="Seg_3206" s="T82">мерзнуть-CVB.SIM</ta>
            <ta e="T84" id="Seg_3207" s="T83">лежать-FUT-1PL=Q</ta>
            <ta e="T85" id="Seg_3208" s="T84">земля-3SG-INSTR</ta>
            <ta e="T86" id="Seg_3209" s="T85">идти-PTCP.COND-DAT/LOC</ta>
            <ta e="T87" id="Seg_3210" s="T86">далекий.[NOM]</ta>
            <ta e="T88" id="Seg_3211" s="T87">1PL.[NOM]</ta>
            <ta e="T89" id="Seg_3212" s="T88">напрямик</ta>
            <ta e="T90" id="Seg_3213" s="T89">тот</ta>
            <ta e="T91" id="Seg_3214" s="T90">место-DAT/LOC</ta>
            <ta e="T92" id="Seg_3215" s="T91">летать-IMP.1PL</ta>
            <ta e="T93" id="Seg_3216" s="T92">зима.[NOM]</ta>
            <ta e="T94" id="Seg_3217" s="T93">приходить-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T95" id="Seg_3218" s="T94">этот</ta>
            <ta e="T96" id="Seg_3219" s="T95">холод-1PL.[NOM]</ta>
            <ta e="T97" id="Seg_3220" s="T96">проехать-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T98" id="Seg_3221" s="T97">пока</ta>
            <ta e="T99" id="Seg_3222" s="T98">тот</ta>
            <ta e="T100" id="Seg_3223" s="T99">место-DAT/LOC</ta>
            <ta e="T101" id="Seg_3224" s="T100">жить.постоянно-IMP.1PL</ta>
            <ta e="T102" id="Seg_3225" s="T101">здесь</ta>
            <ta e="T103" id="Seg_3226" s="T102">жить-TEMP-1PL</ta>
            <ta e="T104" id="Seg_3227" s="T103">1PL.[NOM]</ta>
            <ta e="T105" id="Seg_3228" s="T104">каждый</ta>
            <ta e="T106" id="Seg_3229" s="T105">пропадать-FUT-1PL</ta>
            <ta e="T107" id="Seg_3230" s="T106">мерзнуть-CVB.SEQ</ta>
            <ta e="T108" id="Seg_3231" s="T107">умирать-FUT-1PL</ta>
            <ta e="T109" id="Seg_3232" s="T108">там</ta>
            <ta e="T110" id="Seg_3233" s="T109">летать-TEMP-1PL</ta>
            <ta e="T111" id="Seg_3234" s="T110">каждый.[NOM]</ta>
            <ta e="T112" id="Seg_3235" s="T111">человек.[NOM]</ta>
            <ta e="T113" id="Seg_3236" s="T112">быть-FUT-1PL</ta>
            <ta e="T114" id="Seg_3237" s="T113">вот</ta>
            <ta e="T115" id="Seg_3238" s="T114">совет-2PL-ACC</ta>
            <ta e="T116" id="Seg_3239" s="T115">давать-EP-IMP.2PL</ta>
            <ta e="T117" id="Seg_3240" s="T116">тот-DAT/LOC</ta>
            <ta e="T118" id="Seg_3241" s="T117">птица.[NOM]</ta>
            <ta e="T119" id="Seg_3242" s="T118">каждый-3SG.[NOM]</ta>
            <ta e="T120" id="Seg_3243" s="T119">вот</ta>
            <ta e="T121" id="Seg_3244" s="T120">правда.[NOM]</ta>
            <ta e="T122" id="Seg_3245" s="T121">1PL.[NOM]</ta>
            <ta e="T123" id="Seg_3246" s="T122">каждый</ta>
            <ta e="T124" id="Seg_3247" s="T123">летать-FUT-1PL</ta>
            <ta e="T125" id="Seg_3248" s="T124">здесь</ta>
            <ta e="T126" id="Seg_3249" s="T125">умирать-CAP-1PL</ta>
            <ta e="T127" id="Seg_3250" s="T126">руководитель.[NOM]</ta>
            <ta e="T128" id="Seg_3251" s="T127">спрашивать-PRS.[3SG]</ta>
            <ta e="T129" id="Seg_3252" s="T128">вот</ta>
            <ta e="T130" id="Seg_3253" s="T129">сколько-2PL.[NOM]</ta>
            <ta e="T131" id="Seg_3254" s="T130">согласить-PST1-3SG</ta>
            <ta e="T132" id="Seg_3255" s="T131">птица-EP-2SG.[NOM]</ta>
            <ta e="T133" id="Seg_3256" s="T132">каждый-3SG.[NOM]</ta>
            <ta e="T134" id="Seg_3257" s="T133">согласить-PST2.[3SG]</ta>
            <ta e="T135" id="Seg_3258" s="T134">полярная.сова.[NOM]</ta>
            <ta e="T136" id="Seg_3259" s="T135">тоже</ta>
            <ta e="T137" id="Seg_3260" s="T136">слово-PROPR.[NOM]</ta>
            <ta e="T138" id="Seg_3261" s="T137">1SG.[NOM]</ta>
            <ta e="T139" id="Seg_3262" s="T138">тоже</ta>
            <ta e="T140" id="Seg_3263" s="T139">идти-PRS-1SG</ta>
            <ta e="T141" id="Seg_3264" s="T140">руководитель.[NOM]</ta>
            <ta e="T142" id="Seg_3265" s="T141">спрашивать-PRS.[3SG]</ta>
            <ta e="T143" id="Seg_3266" s="T142">эй</ta>
            <ta e="T144" id="Seg_3267" s="T143">этот</ta>
            <ta e="T145" id="Seg_3268" s="T144">полярная.сова.[NOM]</ta>
            <ta e="T146" id="Seg_3269" s="T145">тоже</ta>
            <ta e="T147" id="Seg_3270" s="T146">сопровождать-CVB.PURP</ta>
            <ta e="T148" id="Seg_3271" s="T147">хотеть-PRS.[3SG]</ta>
            <ta e="T149" id="Seg_3272" s="T148">согласить-PRS-2PL</ta>
            <ta e="T150" id="Seg_3273" s="T149">Q</ta>
            <ta e="T151" id="Seg_3274" s="T150">3SG.[NOM]</ta>
            <ta e="T152" id="Seg_3275" s="T151">сопровождать-PTCP.PRS-3SG-ACC</ta>
            <ta e="T153" id="Seg_3276" s="T152">тот-DAT/LOC</ta>
            <ta e="T154" id="Seg_3277" s="T153">птица-PL-EP-2SG.[NOM]</ta>
            <ta e="T155" id="Seg_3278" s="T154">каждый-3SG.[NOM]</ta>
            <ta e="T156" id="Seg_3279" s="T155">1PL.[NOM]</ta>
            <ta e="T157" id="Seg_3280" s="T156">полярная.сова.[NOM]</ta>
            <ta e="T158" id="Seg_3281" s="T157">сопровождать-PTCP.PRS-3SG-ACC</ta>
            <ta e="T159" id="Seg_3282" s="T158">согласить-NEG-1PL</ta>
            <ta e="T160" id="Seg_3283" s="T159">почему</ta>
            <ta e="T161" id="Seg_3284" s="T160">согласить-NEG-2PL</ta>
            <ta e="T162" id="Seg_3285" s="T161">лицо-PROPR-глаз-3SG-DAT/LOC</ta>
            <ta e="T163" id="Seg_3286" s="T162">говорить-EP-IMP.2PL</ta>
            <ta e="T164" id="Seg_3287" s="T163">3SG-ACC</ta>
            <ta e="T165" id="Seg_3288" s="T164">тот-DAT/LOC</ta>
            <ta e="T166" id="Seg_3289" s="T165">птица.[NOM]</ta>
            <ta e="T167" id="Seg_3290" s="T166">каждый-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_3291" s="T167">говорить-PST1-3SG</ta>
            <ta e="T169" id="Seg_3292" s="T168">здесь</ta>
            <ta e="T170" id="Seg_3293" s="T169">однако</ta>
            <ta e="T171" id="Seg_3294" s="T170">полярная.сова.[NOM]</ta>
            <ta e="T172" id="Seg_3295" s="T171">кормить-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T173" id="Seg_3296" s="T172">сидеть-PRS.[3SG]</ta>
            <ta e="T174" id="Seg_3297" s="T173">1PL.[NOM]</ta>
            <ta e="T175" id="Seg_3298" s="T174">EMPH</ta>
            <ta e="T176" id="Seg_3299" s="T175">семья-1PL-INSTR</ta>
            <ta e="T177" id="Seg_3300" s="T176">кормиться-CVB.SEQ</ta>
            <ta e="T178" id="Seg_3301" s="T177">туда</ta>
            <ta e="T179" id="Seg_3302" s="T178">идти-TEMP-3SG</ta>
            <ta e="T180" id="Seg_3303" s="T179">да</ta>
            <ta e="T181" id="Seg_3304" s="T180">тот</ta>
            <ta e="T182" id="Seg_3305" s="T181">обычай-3SG-ACC</ta>
            <ta e="T183" id="Seg_3306" s="T182">ведь</ta>
            <ta e="T184" id="Seg_3307" s="T183">бросать-FUT.[3SG]</ta>
            <ta e="T185" id="Seg_3308" s="T184">NEG-3SG</ta>
            <ta e="T186" id="Seg_3309" s="T185">1PL.[NOM]</ta>
            <ta e="T187" id="Seg_3310" s="T186">двигать-NEG-1PL</ta>
            <ta e="T188" id="Seg_3311" s="T187">руководитель.[NOM]</ta>
            <ta e="T189" id="Seg_3312" s="T188">говорить-PRS.[3SG]</ta>
            <ta e="T190" id="Seg_3313" s="T189">полярная.сова-DAT/LOC</ta>
            <ta e="T191" id="Seg_3314" s="T190">видеть.[IMP.2SG]</ta>
            <ta e="T192" id="Seg_3315" s="T191">люди.[NOM]</ta>
            <ta e="T193" id="Seg_3316" s="T192">не.согласить-PST1-3SG</ta>
            <ta e="T194" id="Seg_3317" s="T193">птица.[NOM]</ta>
            <ta e="T195" id="Seg_3318" s="T194">каждый-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_3319" s="T195">говорить-PST1-3SG</ta>
            <ta e="T197" id="Seg_3320" s="T196">тот.[NOM]</ta>
            <ta e="T198" id="Seg_3321" s="T197">закон.[NOM]</ta>
            <ta e="T199" id="Seg_3322" s="T198">этот</ta>
            <ta e="T200" id="Seg_3323" s="T199">место-DAT/LOC</ta>
            <ta e="T201" id="Seg_3324" s="T200">оставаться-PRS-2SG</ta>
            <ta e="T202" id="Seg_3325" s="T201">можно</ta>
            <ta e="T203" id="Seg_3326" s="T202">мерзнуть-CVB.SEQ</ta>
            <ta e="T204" id="Seg_3327" s="T203">умирать.[IMP.2SG]</ta>
            <ta e="T205" id="Seg_3328" s="T204">как</ta>
            <ta e="T206" id="Seg_3329" s="T205">думать-PRS-2SG</ta>
            <ta e="T207" id="Seg_3330" s="T206">полярная.сова.[NOM]</ta>
            <ta e="T208" id="Seg_3331" s="T207">спорить-NEG-1SG</ta>
            <ta e="T209" id="Seg_3332" s="T208">одинаковый</ta>
            <ta e="T210" id="Seg_3333" s="T209">душа.[NOM]</ta>
            <ta e="T211" id="Seg_3334" s="T210">где</ta>
            <ta e="T212" id="Seg_3335" s="T211">умирать-NEG.[3SG]</ta>
            <ta e="T213" id="Seg_3336" s="T212">можно</ta>
            <ta e="T214" id="Seg_3337" s="T213">выжить-FUT-1SG</ta>
            <ta e="T215" id="Seg_3338" s="T214">оставаться-PRS-1SG</ta>
            <ta e="T216" id="Seg_3339" s="T215">говорить-PRS.[3SG]</ta>
            <ta e="T217" id="Seg_3340" s="T216">птица-PL.[NOM]</ta>
            <ta e="T218" id="Seg_3341" s="T217">каждый</ta>
            <ta e="T219" id="Seg_3342" s="T218">летать-FREQ-CVB.SEQ</ta>
            <ta e="T220" id="Seg_3343" s="T219">оставаться-PST2-3PL</ta>
            <ta e="T221" id="Seg_3344" s="T220">тот-DAT/LOC</ta>
            <ta e="T222" id="Seg_3345" s="T221">полярная.сова.[NOM]</ta>
            <ta e="T223" id="Seg_3346" s="T222">старуха-3SG-DAT/LOC</ta>
            <ta e="T224" id="Seg_3347" s="T223">приходить-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_3348" s="T224">эй=EMPH</ta>
            <ta e="T226" id="Seg_3349" s="T225">1PL-ACC</ta>
            <ta e="T227" id="Seg_3350" s="T226">приносить-NEG-3PL</ta>
            <ta e="T228" id="Seg_3351" s="T227">1SG-DAT/LOC</ta>
            <ta e="T229" id="Seg_3352" s="T228">2SG.[NOM]</ta>
            <ta e="T230" id="Seg_3353" s="T229">мохнатый</ta>
            <ta e="T231" id="Seg_3354" s="T230">очень</ta>
            <ta e="T232" id="Seg_3355" s="T231">ларга.[NOM]</ta>
            <ta e="T233" id="Seg_3356" s="T232">сокуй-PART</ta>
            <ta e="T234" id="Seg_3357" s="T233">шить.[IMP.2SG]</ta>
            <ta e="T235" id="Seg_3358" s="T234">пурга-DAT/LOC</ta>
            <ta e="T236" id="Seg_3359" s="T235">NEG</ta>
            <ta e="T237" id="Seg_3360" s="T236">замести-EP-PASS/REFL-EP-NEG.PTCP.[NOM]</ta>
            <ta e="T238" id="Seg_3361" s="T237">холод-DAT/LOC</ta>
            <ta e="T239" id="Seg_3362" s="T238">NEG</ta>
            <ta e="T240" id="Seg_3363" s="T239">мерзнуть-NEG.PTCP.[NOM]</ta>
            <ta e="T241" id="Seg_3364" s="T240">делать-CVB.SIM</ta>
            <ta e="T242" id="Seg_3365" s="T241">старуха-3SG.[NOM]</ta>
            <ta e="T243" id="Seg_3366" s="T242">шить-FUT2-1SG</ta>
            <ta e="T244" id="Seg_3367" s="T243">говорить-PRS.[3SG]</ta>
            <ta e="T245" id="Seg_3368" s="T244">снег-ACC</ta>
            <ta e="T246" id="Seg_3369" s="T245">с</ta>
            <ta e="T247" id="Seg_3370" s="T246">один</ta>
            <ta e="T248" id="Seg_3371" s="T247">белый.[NOM]</ta>
            <ta e="T249" id="Seg_3372" s="T248">быть-IMP.3SG</ta>
            <ta e="T250" id="Seg_3373" s="T249">тот</ta>
            <ta e="T251" id="Seg_3374" s="T250">сокуй.[NOM]</ta>
            <ta e="T252" id="Seg_3375" s="T251">старуха-3SG.[NOM]</ta>
            <ta e="T253" id="Seg_3376" s="T252">мастер.[NOM]</ta>
            <ta e="T254" id="Seg_3377" s="T253">очень</ta>
            <ta e="T255" id="Seg_3378" s="T254">шить-PST2.[3SG]</ta>
            <ta e="T256" id="Seg_3379" s="T255">гусь.[NOM]</ta>
            <ta e="T257" id="Seg_3380" s="T256">собственный-3SG-COMP</ta>
            <ta e="T258" id="Seg_3381" s="T257">белый</ta>
            <ta e="T259" id="Seg_3382" s="T258">белый</ta>
            <ta e="T260" id="Seg_3383" s="T259">сокуй-ACC</ta>
            <ta e="T261" id="Seg_3384" s="T260">полярная.сова.[NOM]</ta>
            <ta e="T262" id="Seg_3385" s="T261">этот-ACC</ta>
            <ta e="T263" id="Seg_3386" s="T262">одеть-PST2-3SG</ta>
            <ta e="T264" id="Seg_3387" s="T263">впору</ta>
            <ta e="T265" id="Seg_3388" s="T264">сокуй-3SG.[NOM]</ta>
            <ta e="T266" id="Seg_3389" s="T265">тело-3SG-DAT/LOC</ta>
            <ta e="T267" id="Seg_3390" s="T266">повернуть-CVB.SIM</ta>
            <ta e="T268" id="Seg_3391" s="T267">хватать-PST2.[3SG]</ta>
            <ta e="T269" id="Seg_3392" s="T268">холод-DAT/LOC</ta>
            <ta e="T270" id="Seg_3393" s="T269">NEG</ta>
            <ta e="T271" id="Seg_3394" s="T270">мерзнуть-NEG.[3SG]</ta>
            <ta e="T272" id="Seg_3395" s="T271">пурга-DAT/LOC</ta>
            <ta e="T273" id="Seg_3396" s="T272">NEG</ta>
            <ta e="T274" id="Seg_3397" s="T273">прилипать-EP-NEG.[3SG]</ta>
            <ta e="T275" id="Seg_3398" s="T274">снег.[NOM]</ta>
            <ta e="T276" id="Seg_3399" s="T275">NEG</ta>
            <ta e="T277" id="Seg_3400" s="T276">прилипать-EP-NEG.[3SG]</ta>
            <ta e="T278" id="Seg_3401" s="T277">3SG-DAT/LOC</ta>
            <ta e="T279" id="Seg_3402" s="T278">полярная.сова.[NOM]</ta>
            <ta e="T280" id="Seg_3403" s="T279">с.тех.пор</ta>
            <ta e="T281" id="Seg_3404" s="T280">этот</ta>
            <ta e="T282" id="Seg_3405" s="T281">место-DAT/LOC</ta>
            <ta e="T283" id="Seg_3406" s="T282">страна-VBZ-PST2-3SG</ta>
            <ta e="T284" id="Seg_3407" s="T283">тот</ta>
            <ta e="T285" id="Seg_3408" s="T284">оставаться-PTCP.PST-3SG-ACC</ta>
            <ta e="T286" id="Seg_3409" s="T285">мстить-CVB.SEQ</ta>
            <ta e="T287" id="Seg_3410" s="T286">полярная.сова.[NOM]</ta>
            <ta e="T288" id="Seg_3411" s="T287">маленький</ta>
            <ta e="T289" id="Seg_3412" s="T288">птица-ACC</ta>
            <ta e="T290" id="Seg_3413" s="T289">целый-3SG-ACC</ta>
            <ta e="T291" id="Seg_3414" s="T290">еще</ta>
            <ta e="T292" id="Seg_3415" s="T291">есть-PTCP.PRS</ta>
            <ta e="T293" id="Seg_3416" s="T292">становиться-PST2-3SG</ta>
            <ta e="T294" id="Seg_3417" s="T293">лучше</ta>
            <ta e="T295" id="Seg_3418" s="T294">3SG-ACC</ta>
            <ta e="T296" id="Seg_3419" s="T295">с</ta>
            <ta e="T297" id="Seg_3420" s="T296">зима-VBZ-PTCP.PRS</ta>
            <ta e="T298" id="Seg_3421" s="T297">куропатка-ACC</ta>
            <ta e="T299" id="Seg_3422" s="T298">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3423" s="T0">adv</ta>
            <ta e="T2" id="Seg_3424" s="T1">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T3" id="Seg_3425" s="T2">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T4" id="Seg_3426" s="T3">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T5" id="Seg_3427" s="T4">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T6" id="Seg_3428" s="T5">adj</ta>
            <ta e="T7" id="Seg_3429" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_3430" s="T7">dempro</ta>
            <ta e="T9" id="Seg_3431" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_3432" s="T9">n-n&gt;adj-n:case</ta>
            <ta e="T11" id="Seg_3433" s="T10">v-v:tense-v:poss.pn</ta>
            <ta e="T12" id="Seg_3434" s="T11">dempro</ta>
            <ta e="T13" id="Seg_3435" s="T12">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T14" id="Seg_3436" s="T13">dempro</ta>
            <ta e="T15" id="Seg_3437" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_3438" s="T15">adj-n:case</ta>
            <ta e="T17" id="Seg_3439" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_3440" s="T17">adv</ta>
            <ta e="T19" id="Seg_3441" s="T18">n-n:(num)-n:case</ta>
            <ta e="T20" id="Seg_3442" s="T19">v-v:tense-v:pred.pn</ta>
            <ta e="T21" id="Seg_3443" s="T20">n-n:case-n-n:case</ta>
            <ta e="T22" id="Seg_3444" s="T21">v-v:tense-v:poss.pn</ta>
            <ta e="T23" id="Seg_3445" s="T22">dempro-pro:case</ta>
            <ta e="T24" id="Seg_3446" s="T23">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T25" id="Seg_3447" s="T24">adj-n:(poss)-n:case</ta>
            <ta e="T26" id="Seg_3448" s="T25">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T27" id="Seg_3449" s="T26">adj-n:poss-n:case</ta>
            <ta e="T28" id="Seg_3450" s="T27">cardnum</ta>
            <ta e="T29" id="Seg_3451" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_3452" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_3453" s="T30">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T32" id="Seg_3454" s="T31">adv</ta>
            <ta e="T33" id="Seg_3455" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_3456" s="T33">v-v:mood-v:pred.pn</ta>
            <ta e="T35" id="Seg_3457" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_3458" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_3459" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_3460" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_3461" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_3462" s="T39">dempro</ta>
            <ta e="T41" id="Seg_3463" s="T40">n-n:(num)-n:case</ta>
            <ta e="T42" id="Seg_3464" s="T41">n-n:(poss)-n:case</ta>
            <ta e="T43" id="Seg_3465" s="T42">pers-pro:case</ta>
            <ta e="T44" id="Seg_3466" s="T43">n-n:(num)-n:case</ta>
            <ta e="T45" id="Seg_3467" s="T44">v-v:tense-v:poss.pn</ta>
            <ta e="T46" id="Seg_3468" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_3469" s="T46">adj-n:poss-n:case</ta>
            <ta e="T48" id="Seg_3470" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_3471" s="T48">v-v:tense-v:pred.pn</ta>
            <ta e="T50" id="Seg_3472" s="T49">cardnum</ta>
            <ta e="T51" id="Seg_3473" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_3474" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_3475" s="T52">adj</ta>
            <ta e="T54" id="Seg_3476" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_3477" s="T54">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T56" id="Seg_3478" s="T55">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T57" id="Seg_3479" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_3480" s="T57">v-v:tense-v:pred.pn</ta>
            <ta e="T59" id="Seg_3481" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_3482" s="T59">v-v:tense-v:pred.pn</ta>
            <ta e="T61" id="Seg_3483" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_3484" s="T61">n-n:case-n-n:case</ta>
            <ta e="T63" id="Seg_3485" s="T62">v-v:tense-v:poss.pn</ta>
            <ta e="T64" id="Seg_3486" s="T63">pers-pro:case</ta>
            <ta e="T65" id="Seg_3487" s="T64">adv</ta>
            <ta e="T66" id="Seg_3488" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_3489" s="T66">v-v:ptcp</ta>
            <ta e="T68" id="Seg_3490" s="T67">v-v:tense-v:poss.pn</ta>
            <ta e="T69" id="Seg_3491" s="T68">pers-pro:case</ta>
            <ta e="T70" id="Seg_3492" s="T69">v-v:tense-v:pred.pn</ta>
            <ta e="T71" id="Seg_3493" s="T70">que</ta>
            <ta e="T72" id="Seg_3494" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_3495" s="T72">n-n:case</ta>
            <ta e="T74" id="Seg_3496" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_3497" s="T74">v-v:ptcp</ta>
            <ta e="T76" id="Seg_3498" s="T75">n-n:(poss)-n:case</ta>
            <ta e="T77" id="Seg_3499" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_3500" s="T77">pers-pro:case</ta>
            <ta e="T79" id="Seg_3501" s="T78">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T80" id="Seg_3502" s="T79">que</ta>
            <ta e="T81" id="Seg_3503" s="T80">dempro</ta>
            <ta e="T82" id="Seg_3504" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_3505" s="T82">v-v:cvb</ta>
            <ta e="T84" id="Seg_3506" s="T83">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T85" id="Seg_3507" s="T84">n-n:poss-n:case</ta>
            <ta e="T86" id="Seg_3508" s="T85">v-v:ptcp-v:(case)</ta>
            <ta e="T87" id="Seg_3509" s="T86">adj-n:case</ta>
            <ta e="T88" id="Seg_3510" s="T87">pers-pro:case</ta>
            <ta e="T89" id="Seg_3511" s="T88">adv</ta>
            <ta e="T90" id="Seg_3512" s="T89">dempro</ta>
            <ta e="T91" id="Seg_3513" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_3514" s="T91">v-v:mood.pn</ta>
            <ta e="T93" id="Seg_3515" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_3516" s="T93">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T95" id="Seg_3517" s="T94">dempro</ta>
            <ta e="T96" id="Seg_3518" s="T95">n-n:(poss)-n:case</ta>
            <ta e="T97" id="Seg_3519" s="T96">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T98" id="Seg_3520" s="T97">post</ta>
            <ta e="T99" id="Seg_3521" s="T98">dempro</ta>
            <ta e="T100" id="Seg_3522" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_3523" s="T100">v-v:mood.pn</ta>
            <ta e="T102" id="Seg_3524" s="T101">adv</ta>
            <ta e="T103" id="Seg_3525" s="T102">v-v:mood-v:temp.pn</ta>
            <ta e="T104" id="Seg_3526" s="T103">pers-pro:case</ta>
            <ta e="T105" id="Seg_3527" s="T104">adj</ta>
            <ta e="T106" id="Seg_3528" s="T105">v-v:tense-v:poss.pn</ta>
            <ta e="T107" id="Seg_3529" s="T106">v-v:cvb</ta>
            <ta e="T108" id="Seg_3530" s="T107">v-v:tense-v:poss.pn</ta>
            <ta e="T109" id="Seg_3531" s="T108">adv</ta>
            <ta e="T110" id="Seg_3532" s="T109">v-v:mood-v:temp.pn</ta>
            <ta e="T111" id="Seg_3533" s="T110">adj-n:case</ta>
            <ta e="T112" id="Seg_3534" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_3535" s="T112">v-v:tense-v:poss.pn</ta>
            <ta e="T114" id="Seg_3536" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_3537" s="T114">n-n:poss-n:case</ta>
            <ta e="T116" id="Seg_3538" s="T115">v-v:(ins)-v:mood.pn</ta>
            <ta e="T117" id="Seg_3539" s="T116">dempro-pro:case</ta>
            <ta e="T118" id="Seg_3540" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_3541" s="T118">adj-n:(poss)-n:case</ta>
            <ta e="T120" id="Seg_3542" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_3543" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_3544" s="T121">pers-pro:case</ta>
            <ta e="T123" id="Seg_3545" s="T122">adj</ta>
            <ta e="T124" id="Seg_3546" s="T123">v-v:tense-v:poss.pn</ta>
            <ta e="T125" id="Seg_3547" s="T124">adv</ta>
            <ta e="T126" id="Seg_3548" s="T125">v-v:mood-v:pred.pn</ta>
            <ta e="T127" id="Seg_3549" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_3550" s="T127">v-v:tense-v:pred.pn</ta>
            <ta e="T129" id="Seg_3551" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_3552" s="T129">que-pro:(poss)-pro:case</ta>
            <ta e="T131" id="Seg_3553" s="T130">v-v:tense-v:poss.pn</ta>
            <ta e="T132" id="Seg_3554" s="T131">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T133" id="Seg_3555" s="T132">adj-n:(poss)-n:case</ta>
            <ta e="T134" id="Seg_3556" s="T133">v-v:tense-v:pred.pn</ta>
            <ta e="T135" id="Seg_3557" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_3558" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_3559" s="T136">n-n&gt;adj-n:case</ta>
            <ta e="T138" id="Seg_3560" s="T137">pers-pro:case</ta>
            <ta e="T139" id="Seg_3561" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_3562" s="T139">v-v:tense-v:pred.pn</ta>
            <ta e="T141" id="Seg_3563" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_3564" s="T141">v-v:tense-v:pred.pn</ta>
            <ta e="T143" id="Seg_3565" s="T142">interj</ta>
            <ta e="T144" id="Seg_3566" s="T143">dempro</ta>
            <ta e="T145" id="Seg_3567" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_3568" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_3569" s="T146">v-v:cvb</ta>
            <ta e="T148" id="Seg_3570" s="T147">v-v:tense-v:pred.pn</ta>
            <ta e="T149" id="Seg_3571" s="T148">v-v:tense-v:pred.pn</ta>
            <ta e="T150" id="Seg_3572" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_3573" s="T150">pers-pro:case</ta>
            <ta e="T152" id="Seg_3574" s="T151">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T153" id="Seg_3575" s="T152">dempro-pro:case</ta>
            <ta e="T154" id="Seg_3576" s="T153">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T155" id="Seg_3577" s="T154">adj-n:(poss)-n:case</ta>
            <ta e="T156" id="Seg_3578" s="T155">pers-pro:case</ta>
            <ta e="T157" id="Seg_3579" s="T156">n-n:case</ta>
            <ta e="T158" id="Seg_3580" s="T157">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T159" id="Seg_3581" s="T158">v-v:(neg)-v:pred.pn</ta>
            <ta e="T160" id="Seg_3582" s="T159">que</ta>
            <ta e="T161" id="Seg_3583" s="T160">v-v:(neg)-v:pred.pn</ta>
            <ta e="T162" id="Seg_3584" s="T161">n-n&gt;adj-n-n:(poss)-n:case</ta>
            <ta e="T163" id="Seg_3585" s="T162">v-v:(ins)-v:mood.pn</ta>
            <ta e="T164" id="Seg_3586" s="T163">pers-pro:case</ta>
            <ta e="T165" id="Seg_3587" s="T164">dempro-pro:case</ta>
            <ta e="T166" id="Seg_3588" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_3589" s="T166">adj-n:(poss)-n:case</ta>
            <ta e="T168" id="Seg_3590" s="T167">v-v:tense-v:poss.pn</ta>
            <ta e="T169" id="Seg_3591" s="T168">adv</ta>
            <ta e="T170" id="Seg_3592" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_3593" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_3594" s="T171">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T173" id="Seg_3595" s="T172">v-v:tense-v:pred.pn</ta>
            <ta e="T174" id="Seg_3596" s="T173">pers-pro:case</ta>
            <ta e="T175" id="Seg_3597" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_3598" s="T175">n-n:poss-n:case</ta>
            <ta e="T177" id="Seg_3599" s="T176">v-v:cvb</ta>
            <ta e="T178" id="Seg_3600" s="T177">adv</ta>
            <ta e="T179" id="Seg_3601" s="T178">v-v:mood-v:temp.pn</ta>
            <ta e="T180" id="Seg_3602" s="T179">conj</ta>
            <ta e="T181" id="Seg_3603" s="T180">dempro</ta>
            <ta e="T182" id="Seg_3604" s="T181">n-n:poss-n:case</ta>
            <ta e="T183" id="Seg_3605" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_3606" s="T183">v-v:tense-v:poss.pn</ta>
            <ta e="T185" id="Seg_3607" s="T184">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T186" id="Seg_3608" s="T185">pers-pro:case</ta>
            <ta e="T187" id="Seg_3609" s="T186">v-v:(neg)-v:pred.pn</ta>
            <ta e="T188" id="Seg_3610" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_3611" s="T188">v-v:tense-v:pred.pn</ta>
            <ta e="T190" id="Seg_3612" s="T189">n-n:case</ta>
            <ta e="T191" id="Seg_3613" s="T190">v-v:mood.pn</ta>
            <ta e="T192" id="Seg_3614" s="T191">n-n:case</ta>
            <ta e="T193" id="Seg_3615" s="T192">v-v:tense-v:poss.pn</ta>
            <ta e="T194" id="Seg_3616" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_3617" s="T194">adj-n:(poss)-n:case</ta>
            <ta e="T196" id="Seg_3618" s="T195">v-v:tense-v:poss.pn</ta>
            <ta e="T197" id="Seg_3619" s="T196">dempro-pro:case</ta>
            <ta e="T198" id="Seg_3620" s="T197">n-n:case</ta>
            <ta e="T199" id="Seg_3621" s="T198">dempro</ta>
            <ta e="T200" id="Seg_3622" s="T199">n-n:case</ta>
            <ta e="T201" id="Seg_3623" s="T200">v-v:tense-v:pred.pn</ta>
            <ta e="T202" id="Seg_3624" s="T201">adv</ta>
            <ta e="T203" id="Seg_3625" s="T202">v-v:cvb</ta>
            <ta e="T204" id="Seg_3626" s="T203">v-v:mood.pn</ta>
            <ta e="T205" id="Seg_3627" s="T204">que</ta>
            <ta e="T206" id="Seg_3628" s="T205">v-v:tense-v:pred.pn</ta>
            <ta e="T207" id="Seg_3629" s="T206">n-n:case</ta>
            <ta e="T208" id="Seg_3630" s="T207">v-v:(neg)-v:pred.pn</ta>
            <ta e="T209" id="Seg_3631" s="T208">adj</ta>
            <ta e="T210" id="Seg_3632" s="T209">n-n:case</ta>
            <ta e="T211" id="Seg_3633" s="T210">que</ta>
            <ta e="T212" id="Seg_3634" s="T211">v-v:(neg)-v:pred.pn</ta>
            <ta e="T213" id="Seg_3635" s="T212">adv</ta>
            <ta e="T214" id="Seg_3636" s="T213">v-v:tense-v:poss.pn</ta>
            <ta e="T215" id="Seg_3637" s="T214">v-v:tense-v:pred.pn</ta>
            <ta e="T216" id="Seg_3638" s="T215">v-v:tense-v:pred.pn</ta>
            <ta e="T217" id="Seg_3639" s="T216">n-n:(num)-n:case</ta>
            <ta e="T218" id="Seg_3640" s="T217">adj</ta>
            <ta e="T219" id="Seg_3641" s="T218">v-v&gt;v-v:cvb</ta>
            <ta e="T220" id="Seg_3642" s="T219">v-v:tense-v:pred.pn</ta>
            <ta e="T221" id="Seg_3643" s="T220">dempro-pro:case</ta>
            <ta e="T222" id="Seg_3644" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_3645" s="T222">n-n:poss-n:case</ta>
            <ta e="T224" id="Seg_3646" s="T223">v-v:tense-v:pred.pn</ta>
            <ta e="T225" id="Seg_3647" s="T224">interj-ptcl</ta>
            <ta e="T226" id="Seg_3648" s="T225">pers-pro:case</ta>
            <ta e="T227" id="Seg_3649" s="T226">v-v:(neg)-v:pred.pn</ta>
            <ta e="T228" id="Seg_3650" s="T227">pers-pro:case</ta>
            <ta e="T229" id="Seg_3651" s="T228">pers-pro:case</ta>
            <ta e="T230" id="Seg_3652" s="T229">adj</ta>
            <ta e="T231" id="Seg_3653" s="T230">post</ta>
            <ta e="T232" id="Seg_3654" s="T231">n-n:case</ta>
            <ta e="T233" id="Seg_3655" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_3656" s="T233">v-v:mood.pn</ta>
            <ta e="T235" id="Seg_3657" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_3658" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_3659" s="T236">v-v:(ins)-v&gt;v-v:(ins)-v:ptcp-v:(case)</ta>
            <ta e="T238" id="Seg_3660" s="T237">n-n:case</ta>
            <ta e="T239" id="Seg_3661" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_3662" s="T239">v-v:ptcp-v:(case)</ta>
            <ta e="T241" id="Seg_3663" s="T240">v-v:cvb</ta>
            <ta e="T242" id="Seg_3664" s="T241">n-n:(poss)-n:case</ta>
            <ta e="T243" id="Seg_3665" s="T242">v-v:tense-v:pred.pn</ta>
            <ta e="T244" id="Seg_3666" s="T243">v-v:tense-v:pred.pn</ta>
            <ta e="T245" id="Seg_3667" s="T244">n-n:case</ta>
            <ta e="T246" id="Seg_3668" s="T245">post</ta>
            <ta e="T247" id="Seg_3669" s="T246">cardnum</ta>
            <ta e="T248" id="Seg_3670" s="T247">adj-n:case</ta>
            <ta e="T249" id="Seg_3671" s="T248">v-v:mood.pn</ta>
            <ta e="T250" id="Seg_3672" s="T249">dempro</ta>
            <ta e="T251" id="Seg_3673" s="T250">n-n:case</ta>
            <ta e="T252" id="Seg_3674" s="T251">n-n:(poss)-n:case</ta>
            <ta e="T253" id="Seg_3675" s="T252">n-n:case</ta>
            <ta e="T254" id="Seg_3676" s="T253">post</ta>
            <ta e="T255" id="Seg_3677" s="T254">v-v:tense-v:pred.pn</ta>
            <ta e="T256" id="Seg_3678" s="T255">n-n:case</ta>
            <ta e="T257" id="Seg_3679" s="T256">adj-n:poss-n:case</ta>
            <ta e="T258" id="Seg_3680" s="T257">adj</ta>
            <ta e="T259" id="Seg_3681" s="T258">adj</ta>
            <ta e="T260" id="Seg_3682" s="T259">n-n:case</ta>
            <ta e="T261" id="Seg_3683" s="T260">n-n:case</ta>
            <ta e="T262" id="Seg_3684" s="T261">dempro-pro:case</ta>
            <ta e="T263" id="Seg_3685" s="T262">v-v:tense-v:poss.pn</ta>
            <ta e="T264" id="Seg_3686" s="T263">adv</ta>
            <ta e="T265" id="Seg_3687" s="T264">n-n:(poss)-n:case</ta>
            <ta e="T266" id="Seg_3688" s="T265">n-n:poss-n:case</ta>
            <ta e="T267" id="Seg_3689" s="T266">v-v:cvb</ta>
            <ta e="T268" id="Seg_3690" s="T267">v-v:tense-v:pred.pn</ta>
            <ta e="T269" id="Seg_3691" s="T268">n-n:case</ta>
            <ta e="T270" id="Seg_3692" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_3693" s="T270">v-v:(neg)-v:pred.pn</ta>
            <ta e="T272" id="Seg_3694" s="T271">n-n:case</ta>
            <ta e="T273" id="Seg_3695" s="T272">ptcl</ta>
            <ta e="T274" id="Seg_3696" s="T273">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T275" id="Seg_3697" s="T274">n-n:case</ta>
            <ta e="T276" id="Seg_3698" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_3699" s="T276">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T278" id="Seg_3700" s="T277">pers-pro:case</ta>
            <ta e="T279" id="Seg_3701" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_3702" s="T279">adv</ta>
            <ta e="T281" id="Seg_3703" s="T280">dempro</ta>
            <ta e="T282" id="Seg_3704" s="T281">n-n:case</ta>
            <ta e="T283" id="Seg_3705" s="T282">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T284" id="Seg_3706" s="T283">dempro</ta>
            <ta e="T285" id="Seg_3707" s="T284">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T286" id="Seg_3708" s="T285">v-v:cvb</ta>
            <ta e="T287" id="Seg_3709" s="T286">n-n:case</ta>
            <ta e="T288" id="Seg_3710" s="T287">adj</ta>
            <ta e="T289" id="Seg_3711" s="T288">n-n:case</ta>
            <ta e="T290" id="Seg_3712" s="T289">adj-n:poss-n:case</ta>
            <ta e="T291" id="Seg_3713" s="T290">adv</ta>
            <ta e="T292" id="Seg_3714" s="T291">v-v:ptcp</ta>
            <ta e="T293" id="Seg_3715" s="T292">v-v:tense-v:poss.pn</ta>
            <ta e="T294" id="Seg_3716" s="T293">adv</ta>
            <ta e="T295" id="Seg_3717" s="T294">pers-pro:case</ta>
            <ta e="T296" id="Seg_3718" s="T295">post</ta>
            <ta e="T297" id="Seg_3719" s="T296">n-n&gt;v-v:ptcp</ta>
            <ta e="T298" id="Seg_3720" s="T297">n-n:case</ta>
            <ta e="T299" id="Seg_3721" s="T298">adj-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_3722" s="T0">adv</ta>
            <ta e="T2" id="Seg_3723" s="T1">n</ta>
            <ta e="T3" id="Seg_3724" s="T2">n</ta>
            <ta e="T4" id="Seg_3725" s="T3">n</ta>
            <ta e="T5" id="Seg_3726" s="T4">n</ta>
            <ta e="T6" id="Seg_3727" s="T5">adj</ta>
            <ta e="T7" id="Seg_3728" s="T6">n</ta>
            <ta e="T8" id="Seg_3729" s="T7">dempro</ta>
            <ta e="T9" id="Seg_3730" s="T8">n</ta>
            <ta e="T10" id="Seg_3731" s="T9">adj</ta>
            <ta e="T11" id="Seg_3732" s="T10">cop</ta>
            <ta e="T12" id="Seg_3733" s="T11">dempro</ta>
            <ta e="T13" id="Seg_3734" s="T12">n</ta>
            <ta e="T14" id="Seg_3735" s="T13">dempro</ta>
            <ta e="T15" id="Seg_3736" s="T14">n</ta>
            <ta e="T16" id="Seg_3737" s="T15">adj</ta>
            <ta e="T17" id="Seg_3738" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_3739" s="T17">adv</ta>
            <ta e="T19" id="Seg_3740" s="T18">n</ta>
            <ta e="T20" id="Seg_3741" s="T19">cop</ta>
            <ta e="T21" id="Seg_3742" s="T20">n</ta>
            <ta e="T22" id="Seg_3743" s="T21">v</ta>
            <ta e="T23" id="Seg_3744" s="T22">dempro</ta>
            <ta e="T24" id="Seg_3745" s="T23">n</ta>
            <ta e="T25" id="Seg_3746" s="T24">adj</ta>
            <ta e="T26" id="Seg_3747" s="T25">v</ta>
            <ta e="T27" id="Seg_3748" s="T26">adj</ta>
            <ta e="T28" id="Seg_3749" s="T27">cardnum</ta>
            <ta e="T29" id="Seg_3750" s="T28">n</ta>
            <ta e="T30" id="Seg_3751" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_3752" s="T30">n</ta>
            <ta e="T32" id="Seg_3753" s="T31">adv</ta>
            <ta e="T33" id="Seg_3754" s="T32">n</ta>
            <ta e="T34" id="Seg_3755" s="T33">v</ta>
            <ta e="T35" id="Seg_3756" s="T34">n</ta>
            <ta e="T36" id="Seg_3757" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_3758" s="T36">n</ta>
            <ta e="T38" id="Seg_3759" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_3760" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_3761" s="T39">dempro</ta>
            <ta e="T41" id="Seg_3762" s="T40">n</ta>
            <ta e="T42" id="Seg_3763" s="T41">n</ta>
            <ta e="T43" id="Seg_3764" s="T42">pers</ta>
            <ta e="T44" id="Seg_3765" s="T43">n</ta>
            <ta e="T45" id="Seg_3766" s="T44">cop</ta>
            <ta e="T46" id="Seg_3767" s="T45">n</ta>
            <ta e="T47" id="Seg_3768" s="T46">adj</ta>
            <ta e="T48" id="Seg_3769" s="T47">n</ta>
            <ta e="T49" id="Seg_3770" s="T48">v</ta>
            <ta e="T50" id="Seg_3771" s="T49">cardnum</ta>
            <ta e="T51" id="Seg_3772" s="T50">n</ta>
            <ta e="T52" id="Seg_3773" s="T51">n</ta>
            <ta e="T53" id="Seg_3774" s="T52">adj</ta>
            <ta e="T54" id="Seg_3775" s="T53">n</ta>
            <ta e="T55" id="Seg_3776" s="T54">v</ta>
            <ta e="T56" id="Seg_3777" s="T55">n</ta>
            <ta e="T57" id="Seg_3778" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_3779" s="T57">v</ta>
            <ta e="T59" id="Seg_3780" s="T58">n</ta>
            <ta e="T60" id="Seg_3781" s="T59">v</ta>
            <ta e="T61" id="Seg_3782" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_3783" s="T61">n</ta>
            <ta e="T63" id="Seg_3784" s="T62">v</ta>
            <ta e="T64" id="Seg_3785" s="T63">pers</ta>
            <ta e="T65" id="Seg_3786" s="T64">adv</ta>
            <ta e="T66" id="Seg_3787" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_3788" s="T66">v</ta>
            <ta e="T68" id="Seg_3789" s="T67">aux</ta>
            <ta e="T69" id="Seg_3790" s="T68">pers</ta>
            <ta e="T70" id="Seg_3791" s="T69">v</ta>
            <ta e="T71" id="Seg_3792" s="T70">que</ta>
            <ta e="T72" id="Seg_3793" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_3794" s="T72">n</ta>
            <ta e="T74" id="Seg_3795" s="T73">n</ta>
            <ta e="T75" id="Seg_3796" s="T74">cop</ta>
            <ta e="T76" id="Seg_3797" s="T75">n</ta>
            <ta e="T77" id="Seg_3798" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_3799" s="T77">pers</ta>
            <ta e="T79" id="Seg_3800" s="T78">n</ta>
            <ta e="T80" id="Seg_3801" s="T79">que</ta>
            <ta e="T81" id="Seg_3802" s="T80">dempro</ta>
            <ta e="T82" id="Seg_3803" s="T81">n</ta>
            <ta e="T83" id="Seg_3804" s="T82">v</ta>
            <ta e="T84" id="Seg_3805" s="T83">aux</ta>
            <ta e="T85" id="Seg_3806" s="T84">n</ta>
            <ta e="T86" id="Seg_3807" s="T85">v</ta>
            <ta e="T87" id="Seg_3808" s="T86">adj</ta>
            <ta e="T88" id="Seg_3809" s="T87">pers</ta>
            <ta e="T89" id="Seg_3810" s="T88">adv</ta>
            <ta e="T90" id="Seg_3811" s="T89">dempro</ta>
            <ta e="T91" id="Seg_3812" s="T90">n</ta>
            <ta e="T92" id="Seg_3813" s="T91">v</ta>
            <ta e="T93" id="Seg_3814" s="T92">n</ta>
            <ta e="T94" id="Seg_3815" s="T93">v</ta>
            <ta e="T95" id="Seg_3816" s="T94">dempro</ta>
            <ta e="T96" id="Seg_3817" s="T95">n</ta>
            <ta e="T97" id="Seg_3818" s="T96">v</ta>
            <ta e="T98" id="Seg_3819" s="T97">post</ta>
            <ta e="T99" id="Seg_3820" s="T98">dempro</ta>
            <ta e="T100" id="Seg_3821" s="T99">n</ta>
            <ta e="T101" id="Seg_3822" s="T100">v</ta>
            <ta e="T102" id="Seg_3823" s="T101">adv</ta>
            <ta e="T103" id="Seg_3824" s="T102">v</ta>
            <ta e="T104" id="Seg_3825" s="T103">pers</ta>
            <ta e="T105" id="Seg_3826" s="T104">adj</ta>
            <ta e="T106" id="Seg_3827" s="T105">v</ta>
            <ta e="T107" id="Seg_3828" s="T106">v</ta>
            <ta e="T108" id="Seg_3829" s="T107">v</ta>
            <ta e="T109" id="Seg_3830" s="T108">adv</ta>
            <ta e="T110" id="Seg_3831" s="T109">v</ta>
            <ta e="T111" id="Seg_3832" s="T110">adj</ta>
            <ta e="T112" id="Seg_3833" s="T111">n</ta>
            <ta e="T113" id="Seg_3834" s="T112">cop</ta>
            <ta e="T114" id="Seg_3835" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_3836" s="T114">n</ta>
            <ta e="T116" id="Seg_3837" s="T115">v</ta>
            <ta e="T117" id="Seg_3838" s="T116">dempro</ta>
            <ta e="T118" id="Seg_3839" s="T117">n</ta>
            <ta e="T119" id="Seg_3840" s="T118">adj</ta>
            <ta e="T120" id="Seg_3841" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_3842" s="T120">n</ta>
            <ta e="T122" id="Seg_3843" s="T121">pers</ta>
            <ta e="T123" id="Seg_3844" s="T122">adj</ta>
            <ta e="T124" id="Seg_3845" s="T123">v</ta>
            <ta e="T125" id="Seg_3846" s="T124">adv</ta>
            <ta e="T126" id="Seg_3847" s="T125">v</ta>
            <ta e="T127" id="Seg_3848" s="T126">n</ta>
            <ta e="T128" id="Seg_3849" s="T127">v</ta>
            <ta e="T129" id="Seg_3850" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_3851" s="T129">que</ta>
            <ta e="T131" id="Seg_3852" s="T130">v</ta>
            <ta e="T132" id="Seg_3853" s="T131">n</ta>
            <ta e="T133" id="Seg_3854" s="T132">adj</ta>
            <ta e="T134" id="Seg_3855" s="T133">v</ta>
            <ta e="T135" id="Seg_3856" s="T134">n</ta>
            <ta e="T136" id="Seg_3857" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_3858" s="T136">adj</ta>
            <ta e="T138" id="Seg_3859" s="T137">pers</ta>
            <ta e="T139" id="Seg_3860" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_3861" s="T139">v</ta>
            <ta e="T141" id="Seg_3862" s="T140">n</ta>
            <ta e="T142" id="Seg_3863" s="T141">v</ta>
            <ta e="T143" id="Seg_3864" s="T142">interj</ta>
            <ta e="T144" id="Seg_3865" s="T143">dempro</ta>
            <ta e="T145" id="Seg_3866" s="T144">n</ta>
            <ta e="T146" id="Seg_3867" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_3868" s="T146">v</ta>
            <ta e="T148" id="Seg_3869" s="T147">v</ta>
            <ta e="T149" id="Seg_3870" s="T148">v</ta>
            <ta e="T150" id="Seg_3871" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_3872" s="T150">pers</ta>
            <ta e="T152" id="Seg_3873" s="T151">v</ta>
            <ta e="T153" id="Seg_3874" s="T152">dempro</ta>
            <ta e="T154" id="Seg_3875" s="T153">n</ta>
            <ta e="T155" id="Seg_3876" s="T154">adj</ta>
            <ta e="T156" id="Seg_3877" s="T155">pers</ta>
            <ta e="T157" id="Seg_3878" s="T156">n</ta>
            <ta e="T158" id="Seg_3879" s="T157">v</ta>
            <ta e="T159" id="Seg_3880" s="T158">v</ta>
            <ta e="T160" id="Seg_3881" s="T159">que</ta>
            <ta e="T161" id="Seg_3882" s="T160">v</ta>
            <ta e="T162" id="Seg_3883" s="T161">n</ta>
            <ta e="T163" id="Seg_3884" s="T162">v</ta>
            <ta e="T164" id="Seg_3885" s="T163">pers</ta>
            <ta e="T165" id="Seg_3886" s="T164">dempro</ta>
            <ta e="T166" id="Seg_3887" s="T165">n</ta>
            <ta e="T167" id="Seg_3888" s="T166">adj</ta>
            <ta e="T168" id="Seg_3889" s="T167">v</ta>
            <ta e="T169" id="Seg_3890" s="T168">adv</ta>
            <ta e="T170" id="Seg_3891" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_3892" s="T170">n</ta>
            <ta e="T172" id="Seg_3893" s="T171">v</ta>
            <ta e="T173" id="Seg_3894" s="T172">aux</ta>
            <ta e="T174" id="Seg_3895" s="T173">pers</ta>
            <ta e="T175" id="Seg_3896" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_3897" s="T175">n</ta>
            <ta e="T177" id="Seg_3898" s="T176">v</ta>
            <ta e="T178" id="Seg_3899" s="T177">adv</ta>
            <ta e="T179" id="Seg_3900" s="T178">v</ta>
            <ta e="T180" id="Seg_3901" s="T179">conj</ta>
            <ta e="T181" id="Seg_3902" s="T180">dempro</ta>
            <ta e="T182" id="Seg_3903" s="T181">n</ta>
            <ta e="T183" id="Seg_3904" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_3905" s="T183">v</ta>
            <ta e="T185" id="Seg_3906" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_3907" s="T185">pers</ta>
            <ta e="T187" id="Seg_3908" s="T186">v</ta>
            <ta e="T188" id="Seg_3909" s="T187">n</ta>
            <ta e="T189" id="Seg_3910" s="T188">v</ta>
            <ta e="T190" id="Seg_3911" s="T189">n</ta>
            <ta e="T191" id="Seg_3912" s="T190">v</ta>
            <ta e="T192" id="Seg_3913" s="T191">n</ta>
            <ta e="T193" id="Seg_3914" s="T192">v</ta>
            <ta e="T194" id="Seg_3915" s="T193">n</ta>
            <ta e="T195" id="Seg_3916" s="T194">adj</ta>
            <ta e="T196" id="Seg_3917" s="T195">v</ta>
            <ta e="T197" id="Seg_3918" s="T196">dempro</ta>
            <ta e="T198" id="Seg_3919" s="T197">n</ta>
            <ta e="T199" id="Seg_3920" s="T198">dempro</ta>
            <ta e="T200" id="Seg_3921" s="T199">n</ta>
            <ta e="T201" id="Seg_3922" s="T200">v</ta>
            <ta e="T202" id="Seg_3923" s="T201">adv</ta>
            <ta e="T203" id="Seg_3924" s="T202">v</ta>
            <ta e="T204" id="Seg_3925" s="T203">v</ta>
            <ta e="T205" id="Seg_3926" s="T204">que</ta>
            <ta e="T206" id="Seg_3927" s="T205">v</ta>
            <ta e="T207" id="Seg_3928" s="T206">n</ta>
            <ta e="T208" id="Seg_3929" s="T207">v</ta>
            <ta e="T209" id="Seg_3930" s="T208">adj</ta>
            <ta e="T210" id="Seg_3931" s="T209">n</ta>
            <ta e="T211" id="Seg_3932" s="T210">que</ta>
            <ta e="T212" id="Seg_3933" s="T211">v</ta>
            <ta e="T213" id="Seg_3934" s="T212">adv</ta>
            <ta e="T214" id="Seg_3935" s="T213">v</ta>
            <ta e="T215" id="Seg_3936" s="T214">v</ta>
            <ta e="T216" id="Seg_3937" s="T215">v</ta>
            <ta e="T217" id="Seg_3938" s="T216">n</ta>
            <ta e="T218" id="Seg_3939" s="T217">adj</ta>
            <ta e="T219" id="Seg_3940" s="T218">v</ta>
            <ta e="T220" id="Seg_3941" s="T219">aux</ta>
            <ta e="T221" id="Seg_3942" s="T220">dempro</ta>
            <ta e="T222" id="Seg_3943" s="T221">n</ta>
            <ta e="T223" id="Seg_3944" s="T222">n</ta>
            <ta e="T224" id="Seg_3945" s="T223">v</ta>
            <ta e="T225" id="Seg_3946" s="T224">interj</ta>
            <ta e="T226" id="Seg_3947" s="T225">pers</ta>
            <ta e="T227" id="Seg_3948" s="T226">v</ta>
            <ta e="T228" id="Seg_3949" s="T227">pers</ta>
            <ta e="T229" id="Seg_3950" s="T228">pers</ta>
            <ta e="T230" id="Seg_3951" s="T229">adj</ta>
            <ta e="T231" id="Seg_3952" s="T230">post</ta>
            <ta e="T232" id="Seg_3953" s="T231">n</ta>
            <ta e="T233" id="Seg_3954" s="T232">n</ta>
            <ta e="T234" id="Seg_3955" s="T233">v</ta>
            <ta e="T235" id="Seg_3956" s="T234">n</ta>
            <ta e="T236" id="Seg_3957" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_3958" s="T236">v</ta>
            <ta e="T238" id="Seg_3959" s="T237">n</ta>
            <ta e="T239" id="Seg_3960" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_3961" s="T239">v</ta>
            <ta e="T241" id="Seg_3962" s="T240">v</ta>
            <ta e="T242" id="Seg_3963" s="T241">n</ta>
            <ta e="T243" id="Seg_3964" s="T242">v</ta>
            <ta e="T244" id="Seg_3965" s="T243">v</ta>
            <ta e="T245" id="Seg_3966" s="T244">n</ta>
            <ta e="T246" id="Seg_3967" s="T245">post</ta>
            <ta e="T247" id="Seg_3968" s="T246">cardnum</ta>
            <ta e="T248" id="Seg_3969" s="T247">adj</ta>
            <ta e="T249" id="Seg_3970" s="T248">cop</ta>
            <ta e="T250" id="Seg_3971" s="T249">dempro</ta>
            <ta e="T251" id="Seg_3972" s="T250">n</ta>
            <ta e="T252" id="Seg_3973" s="T251">n</ta>
            <ta e="T253" id="Seg_3974" s="T252">n</ta>
            <ta e="T254" id="Seg_3975" s="T253">post</ta>
            <ta e="T255" id="Seg_3976" s="T254">v</ta>
            <ta e="T256" id="Seg_3977" s="T255">n</ta>
            <ta e="T257" id="Seg_3978" s="T256">adj</ta>
            <ta e="T258" id="Seg_3979" s="T257">adj</ta>
            <ta e="T259" id="Seg_3980" s="T258">adj</ta>
            <ta e="T260" id="Seg_3981" s="T259">n</ta>
            <ta e="T261" id="Seg_3982" s="T260">n</ta>
            <ta e="T262" id="Seg_3983" s="T261">dempro</ta>
            <ta e="T263" id="Seg_3984" s="T262">v</ta>
            <ta e="T264" id="Seg_3985" s="T263">adv</ta>
            <ta e="T265" id="Seg_3986" s="T264">n</ta>
            <ta e="T266" id="Seg_3987" s="T265">n</ta>
            <ta e="T267" id="Seg_3988" s="T266">v</ta>
            <ta e="T268" id="Seg_3989" s="T267">v</ta>
            <ta e="T269" id="Seg_3990" s="T268">n</ta>
            <ta e="T270" id="Seg_3991" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_3992" s="T270">v</ta>
            <ta e="T272" id="Seg_3993" s="T271">n</ta>
            <ta e="T273" id="Seg_3994" s="T272">ptcl</ta>
            <ta e="T274" id="Seg_3995" s="T273">v</ta>
            <ta e="T275" id="Seg_3996" s="T274">n</ta>
            <ta e="T276" id="Seg_3997" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_3998" s="T276">v</ta>
            <ta e="T278" id="Seg_3999" s="T277">pers</ta>
            <ta e="T279" id="Seg_4000" s="T278">n</ta>
            <ta e="T280" id="Seg_4001" s="T279">adv</ta>
            <ta e="T281" id="Seg_4002" s="T280">dempro</ta>
            <ta e="T282" id="Seg_4003" s="T281">n</ta>
            <ta e="T283" id="Seg_4004" s="T282">v</ta>
            <ta e="T284" id="Seg_4005" s="T283">dempro</ta>
            <ta e="T285" id="Seg_4006" s="T284">v</ta>
            <ta e="T286" id="Seg_4007" s="T285">v</ta>
            <ta e="T287" id="Seg_4008" s="T286">n</ta>
            <ta e="T288" id="Seg_4009" s="T287">adj</ta>
            <ta e="T289" id="Seg_4010" s="T288">n</ta>
            <ta e="T290" id="Seg_4011" s="T289">adj</ta>
            <ta e="T291" id="Seg_4012" s="T290">adv</ta>
            <ta e="T292" id="Seg_4013" s="T291">v</ta>
            <ta e="T293" id="Seg_4014" s="T292">aux</ta>
            <ta e="T294" id="Seg_4015" s="T293">adv</ta>
            <ta e="T295" id="Seg_4016" s="T294">pers</ta>
            <ta e="T296" id="Seg_4017" s="T295">post</ta>
            <ta e="T297" id="Seg_4018" s="T296">v</ta>
            <ta e="T298" id="Seg_4019" s="T297">n</ta>
            <ta e="T299" id="Seg_4020" s="T298">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_4021" s="T0">adv:Time</ta>
            <ta e="T2" id="Seg_4022" s="T1">np.h:Th</ta>
            <ta e="T3" id="Seg_4023" s="T2">np.h:Th</ta>
            <ta e="T4" id="Seg_4024" s="T3">np.h:Th</ta>
            <ta e="T5" id="Seg_4025" s="T4">np.h:Th</ta>
            <ta e="T7" id="Seg_4026" s="T6">np.h:Th</ta>
            <ta e="T9" id="Seg_4027" s="T8">np:L</ta>
            <ta e="T13" id="Seg_4028" s="T12">np:Th</ta>
            <ta e="T15" id="Seg_4029" s="T14">n:Time</ta>
            <ta e="T18" id="Seg_4030" s="T17">pro:Time</ta>
            <ta e="T19" id="Seg_4031" s="T18">np:Th</ta>
            <ta e="T21" id="Seg_4032" s="T20">np:P</ta>
            <ta e="T23" id="Seg_4033" s="T22">pro:Time</ta>
            <ta e="T24" id="Seg_4034" s="T23">np.h:A</ta>
            <ta e="T27" id="Seg_4035" s="T26">np.h:Poss</ta>
            <ta e="T29" id="Seg_4036" s="T28">np.h:Th</ta>
            <ta e="T31" id="Seg_4037" s="T30">np.h:A</ta>
            <ta e="T32" id="Seg_4038" s="T31">adv:Time</ta>
            <ta e="T33" id="Seg_4039" s="T32">np.h:P</ta>
            <ta e="T35" id="Seg_4040" s="T34">np.h:P</ta>
            <ta e="T37" id="Seg_4041" s="T36">np.h:P</ta>
            <ta e="T41" id="Seg_4042" s="T40">np.h:Poss</ta>
            <ta e="T42" id="Seg_4043" s="T41">np.h:A</ta>
            <ta e="T43" id="Seg_4044" s="T42">pro.h:Th</ta>
            <ta e="T44" id="Seg_4045" s="T43">np:So</ta>
            <ta e="T46" id="Seg_4046" s="T45">np.h:Th</ta>
            <ta e="T48" id="Seg_4047" s="T47">np:G</ta>
            <ta e="T51" id="Seg_4048" s="T50">np:P</ta>
            <ta e="T52" id="Seg_4049" s="T51">np:P</ta>
            <ta e="T54" id="Seg_4050" s="T53">np.h:A</ta>
            <ta e="T56" id="Seg_4051" s="T55">np.h:A</ta>
            <ta e="T59" id="Seg_4052" s="T58">np.h:A</ta>
            <ta e="T62" id="Seg_4053" s="T61">np:Th</ta>
            <ta e="T64" id="Seg_4054" s="T63">pro.h:Th</ta>
            <ta e="T65" id="Seg_4055" s="T64">adv:Time</ta>
            <ta e="T69" id="Seg_4056" s="T68">np.h:E</ta>
            <ta e="T73" id="Seg_4057" s="T72">np:L</ta>
            <ta e="T76" id="Seg_4058" s="T75">np:Th</ta>
            <ta e="T78" id="Seg_4059" s="T77">pro.h:P</ta>
            <ta e="T79" id="Seg_4060" s="T78">np.h:P</ta>
            <ta e="T82" id="Seg_4061" s="T81">np:L</ta>
            <ta e="T85" id="Seg_4062" s="T84">np:Path</ta>
            <ta e="T88" id="Seg_4063" s="T87">pro.h:A</ta>
            <ta e="T91" id="Seg_4064" s="T90">np:G</ta>
            <ta e="T93" id="Seg_4065" s="T92">np:Th</ta>
            <ta e="T96" id="Seg_4066" s="T95">np:Th</ta>
            <ta e="T100" id="Seg_4067" s="T99">np:L</ta>
            <ta e="T101" id="Seg_4068" s="T100">0.1.h:A</ta>
            <ta e="T102" id="Seg_4069" s="T101">adv:L</ta>
            <ta e="T103" id="Seg_4070" s="T102">0.1.h:Th</ta>
            <ta e="T104" id="Seg_4071" s="T103">pro.h:P</ta>
            <ta e="T108" id="Seg_4072" s="T106">0.1.h:P</ta>
            <ta e="T109" id="Seg_4073" s="T108">adv:G</ta>
            <ta e="T110" id="Seg_4074" s="T109">0.1.h:A</ta>
            <ta e="T111" id="Seg_4075" s="T110">np.h:Th</ta>
            <ta e="T115" id="Seg_4076" s="T114">np:P</ta>
            <ta e="T116" id="Seg_4077" s="T115">0.2.h:A</ta>
            <ta e="T117" id="Seg_4078" s="T116">pro:Time</ta>
            <ta e="T118" id="Seg_4079" s="T117">np.h:A</ta>
            <ta e="T121" id="Seg_4080" s="T120">0.3:Th</ta>
            <ta e="T122" id="Seg_4081" s="T121">pro.h:A</ta>
            <ta e="T125" id="Seg_4082" s="T124">adv:L</ta>
            <ta e="T126" id="Seg_4083" s="T125">0.1.h:P</ta>
            <ta e="T127" id="Seg_4084" s="T126">np.h:A</ta>
            <ta e="T130" id="Seg_4085" s="T129">pro.h:E</ta>
            <ta e="T132" id="Seg_4086" s="T131">np.h:E</ta>
            <ta e="T135" id="Seg_4087" s="T134">np.h:Th</ta>
            <ta e="T138" id="Seg_4088" s="T137">pro.h:A</ta>
            <ta e="T141" id="Seg_4089" s="T140">np.h:A</ta>
            <ta e="T145" id="Seg_4090" s="T144">np.h:E</ta>
            <ta e="T149" id="Seg_4091" s="T148">0.2.h:E</ta>
            <ta e="T151" id="Seg_4092" s="T150">pro.h:A</ta>
            <ta e="T153" id="Seg_4093" s="T152">pro:Time</ta>
            <ta e="T154" id="Seg_4094" s="T153">np.h:A</ta>
            <ta e="T156" id="Seg_4095" s="T155">pro.h:E</ta>
            <ta e="T157" id="Seg_4096" s="T156">np.h:A</ta>
            <ta e="T161" id="Seg_4097" s="T160">0.2.h:E</ta>
            <ta e="T162" id="Seg_4098" s="T161">0.3.h:Poss np:G</ta>
            <ta e="T163" id="Seg_4099" s="T162">0.2.h:A</ta>
            <ta e="T164" id="Seg_4100" s="T163">pro.h:R</ta>
            <ta e="T165" id="Seg_4101" s="T164">pro:Time</ta>
            <ta e="T166" id="Seg_4102" s="T165">np.h:A</ta>
            <ta e="T169" id="Seg_4103" s="T168">adv:L</ta>
            <ta e="T171" id="Seg_4104" s="T170">np.h:A</ta>
            <ta e="T174" id="Seg_4105" s="T173">pro.h:Poss</ta>
            <ta e="T176" id="Seg_4106" s="T175">np:Ins</ta>
            <ta e="T178" id="Seg_4107" s="T177">adv:G</ta>
            <ta e="T179" id="Seg_4108" s="T178">0.3.h:A</ta>
            <ta e="T182" id="Seg_4109" s="T181">np:P</ta>
            <ta e="T185" id="Seg_4110" s="T183">0.3.h:A</ta>
            <ta e="T186" id="Seg_4111" s="T185">pro.h:A</ta>
            <ta e="T188" id="Seg_4112" s="T187">np.h:A</ta>
            <ta e="T190" id="Seg_4113" s="T189">np.h:R</ta>
            <ta e="T191" id="Seg_4114" s="T190">0.2.h:E</ta>
            <ta e="T192" id="Seg_4115" s="T191">np.h:E</ta>
            <ta e="T194" id="Seg_4116" s="T193">np.h:A</ta>
            <ta e="T197" id="Seg_4117" s="T196">pro:Th</ta>
            <ta e="T200" id="Seg_4118" s="T199">np:L</ta>
            <ta e="T201" id="Seg_4119" s="T200">0.2.h:A</ta>
            <ta e="T204" id="Seg_4120" s="T202">0.2.h:P</ta>
            <ta e="T206" id="Seg_4121" s="T205">0.2.h:E</ta>
            <ta e="T207" id="Seg_4122" s="T206">np.h:A</ta>
            <ta e="T208" id="Seg_4123" s="T207">0.1.h:A</ta>
            <ta e="T210" id="Seg_4124" s="T209">np:P</ta>
            <ta e="T211" id="Seg_4125" s="T210">adv:L</ta>
            <ta e="T214" id="Seg_4126" s="T213">0.1.h:P</ta>
            <ta e="T215" id="Seg_4127" s="T214">0.1.h:Th</ta>
            <ta e="T216" id="Seg_4128" s="T215">0.3.h:A</ta>
            <ta e="T217" id="Seg_4129" s="T216">np.h:A</ta>
            <ta e="T221" id="Seg_4130" s="T220">pro:Time</ta>
            <ta e="T222" id="Seg_4131" s="T221">np.h:A</ta>
            <ta e="T223" id="Seg_4132" s="T222">0.3.h:Poss np:G</ta>
            <ta e="T226" id="Seg_4133" s="T225">pro.h:Th</ta>
            <ta e="T227" id="Seg_4134" s="T226">0.3.h:A</ta>
            <ta e="T228" id="Seg_4135" s="T227">pro.h:B</ta>
            <ta e="T229" id="Seg_4136" s="T228">pro.h:A</ta>
            <ta e="T233" id="Seg_4137" s="T232">np:P</ta>
            <ta e="T235" id="Seg_4138" s="T234">np:L</ta>
            <ta e="T238" id="Seg_4139" s="T237">np:L</ta>
            <ta e="T242" id="Seg_4140" s="T241">np.h:A</ta>
            <ta e="T243" id="Seg_4141" s="T242">0.1.h:A</ta>
            <ta e="T244" id="Seg_4142" s="T243">0.3.h:A</ta>
            <ta e="T251" id="Seg_4143" s="T250">np:Th</ta>
            <ta e="T252" id="Seg_4144" s="T251">np.h:A</ta>
            <ta e="T256" id="Seg_4145" s="T255">np:Poss</ta>
            <ta e="T260" id="Seg_4146" s="T259">np:Th</ta>
            <ta e="T261" id="Seg_4147" s="T260">np.h:A</ta>
            <ta e="T262" id="Seg_4148" s="T261">pro:Th</ta>
            <ta e="T265" id="Seg_4149" s="T264">np:Th</ta>
            <ta e="T266" id="Seg_4150" s="T265">0.3.h:Poss np:G</ta>
            <ta e="T269" id="Seg_4151" s="T268">np:L</ta>
            <ta e="T272" id="Seg_4152" s="T271">np:L</ta>
            <ta e="T275" id="Seg_4153" s="T274">np:P</ta>
            <ta e="T278" id="Seg_4154" s="T277">pro:G</ta>
            <ta e="T279" id="Seg_4155" s="T278">np.h:Th</ta>
            <ta e="T280" id="Seg_4156" s="T279">adv:Time</ta>
            <ta e="T282" id="Seg_4157" s="T281">np:L</ta>
            <ta e="T284" id="Seg_4158" s="T283">pro.h:A</ta>
            <ta e="T287" id="Seg_4159" s="T286">np.h:Th</ta>
            <ta e="T289" id="Seg_4160" s="T288">np.h:P</ta>
            <ta e="T296" id="Seg_4161" s="T294">pp:Com</ta>
            <ta e="T298" id="Seg_4162" s="T297">np.h:P</ta>
            <ta e="T299" id="Seg_4163" s="T298">0.3:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_4164" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_4165" s="T2">np.h:S</ta>
            <ta e="T4" id="Seg_4166" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_4167" s="T4">np.h:S</ta>
            <ta e="T7" id="Seg_4168" s="T6">np.h:S</ta>
            <ta e="T10" id="Seg_4169" s="T9">adj:pred</ta>
            <ta e="T11" id="Seg_4170" s="T10">cop</ta>
            <ta e="T13" id="Seg_4171" s="T12">np:S</ta>
            <ta e="T16" id="Seg_4172" s="T15">adj:pred</ta>
            <ta e="T19" id="Seg_4173" s="T18">np:S</ta>
            <ta e="T20" id="Seg_4174" s="T19">cop</ta>
            <ta e="T21" id="Seg_4175" s="T20">np:S</ta>
            <ta e="T22" id="Seg_4176" s="T21">v:pred</ta>
            <ta e="T24" id="Seg_4177" s="T23">np.h:S</ta>
            <ta e="T26" id="Seg_4178" s="T25">v:pred</ta>
            <ta e="T29" id="Seg_4179" s="T28">np.h:S</ta>
            <ta e="T30" id="Seg_4180" s="T29">ptcl:pred</ta>
            <ta e="T31" id="Seg_4181" s="T30">np.h:S</ta>
            <ta e="T36" id="Seg_4182" s="T35">cop</ta>
            <ta e="T38" id="Seg_4183" s="T37">cop</ta>
            <ta e="T42" id="Seg_4184" s="T41">np.h:S</ta>
            <ta e="T43" id="Seg_4185" s="T42">pro.h:S</ta>
            <ta e="T44" id="Seg_4186" s="T43">n:pred</ta>
            <ta e="T45" id="Seg_4187" s="T44">cop</ta>
            <ta e="T46" id="Seg_4188" s="T45">np.h:O</ta>
            <ta e="T49" id="Seg_4189" s="T48">v:pred</ta>
            <ta e="T51" id="Seg_4190" s="T50">np:O</ta>
            <ta e="T52" id="Seg_4191" s="T51">np:O</ta>
            <ta e="T54" id="Seg_4192" s="T53">np.h:S</ta>
            <ta e="T55" id="Seg_4193" s="T54">v:pred</ta>
            <ta e="T56" id="Seg_4194" s="T55">np.h:S</ta>
            <ta e="T58" id="Seg_4195" s="T57">v:pred</ta>
            <ta e="T59" id="Seg_4196" s="T58">np.h:S</ta>
            <ta e="T60" id="Seg_4197" s="T59">v:pred</ta>
            <ta e="T62" id="Seg_4198" s="T61">np:S</ta>
            <ta e="T63" id="Seg_4199" s="T62">v:pred</ta>
            <ta e="T64" id="Seg_4200" s="T63">pro.h:S</ta>
            <ta e="T68" id="Seg_4201" s="T67">cop</ta>
            <ta e="T69" id="Seg_4202" s="T68">pro.h:S</ta>
            <ta e="T70" id="Seg_4203" s="T69">v:pred</ta>
            <ta e="T76" id="Seg_4204" s="T75">np:S</ta>
            <ta e="T77" id="Seg_4205" s="T76">ptcl:pred</ta>
            <ta e="T78" id="Seg_4206" s="T77">pro.h:S</ta>
            <ta e="T79" id="Seg_4207" s="T78">s:rel</ta>
            <ta e="T84" id="Seg_4208" s="T82">v:pred</ta>
            <ta e="T86" id="Seg_4209" s="T84">s:adv</ta>
            <ta e="T87" id="Seg_4210" s="T86">adj:pred</ta>
            <ta e="T88" id="Seg_4211" s="T87">pro.h:S</ta>
            <ta e="T92" id="Seg_4212" s="T91">v:pred</ta>
            <ta e="T94" id="Seg_4213" s="T92">s:temp</ta>
            <ta e="T98" id="Seg_4214" s="T94">s:temp</ta>
            <ta e="T101" id="Seg_4215" s="T100">0.1.h:S v:pred</ta>
            <ta e="T103" id="Seg_4216" s="T101">s:cond</ta>
            <ta e="T104" id="Seg_4217" s="T103">pro.h:S</ta>
            <ta e="T106" id="Seg_4218" s="T105">v:pred</ta>
            <ta e="T108" id="Seg_4219" s="T106">0.1.h:S v:pred</ta>
            <ta e="T110" id="Seg_4220" s="T108">s:cond</ta>
            <ta e="T113" id="Seg_4221" s="T112">0.1.h:S v:pred</ta>
            <ta e="T115" id="Seg_4222" s="T114">np:O</ta>
            <ta e="T116" id="Seg_4223" s="T115">0.2.h:S v:pred</ta>
            <ta e="T118" id="Seg_4224" s="T117">np.h:S</ta>
            <ta e="T121" id="Seg_4225" s="T120">0.3:S n:pred</ta>
            <ta e="T122" id="Seg_4226" s="T121">pro.h:S</ta>
            <ta e="T124" id="Seg_4227" s="T123">v:pred</ta>
            <ta e="T126" id="Seg_4228" s="T125">0.1.h:S v:pred</ta>
            <ta e="T127" id="Seg_4229" s="T126">np.h:S</ta>
            <ta e="T128" id="Seg_4230" s="T127">v:pred</ta>
            <ta e="T130" id="Seg_4231" s="T129">pro.h:S</ta>
            <ta e="T131" id="Seg_4232" s="T130">v:pred</ta>
            <ta e="T132" id="Seg_4233" s="T131">np.h:S</ta>
            <ta e="T134" id="Seg_4234" s="T133">v:pred</ta>
            <ta e="T135" id="Seg_4235" s="T134">np.h:S</ta>
            <ta e="T137" id="Seg_4236" s="T136">adj:pred</ta>
            <ta e="T138" id="Seg_4237" s="T137">pro.h:S</ta>
            <ta e="T140" id="Seg_4238" s="T139">v:pred</ta>
            <ta e="T141" id="Seg_4239" s="T140">np.h:S</ta>
            <ta e="T142" id="Seg_4240" s="T141">v:pred</ta>
            <ta e="T145" id="Seg_4241" s="T144">np.h:S</ta>
            <ta e="T147" id="Seg_4242" s="T146">s:comp</ta>
            <ta e="T148" id="Seg_4243" s="T147">v:pred</ta>
            <ta e="T149" id="Seg_4244" s="T148">0.2.h:S v:pred</ta>
            <ta e="T152" id="Seg_4245" s="T150">s:comp</ta>
            <ta e="T154" id="Seg_4246" s="T153">np.h:S</ta>
            <ta e="T156" id="Seg_4247" s="T155">pro.h:S</ta>
            <ta e="T158" id="Seg_4248" s="T156">s:comp</ta>
            <ta e="T159" id="Seg_4249" s="T158">v:pred</ta>
            <ta e="T161" id="Seg_4250" s="T160">0.2.h:S v:pred</ta>
            <ta e="T163" id="Seg_4251" s="T162">0.2.h:S v:pred</ta>
            <ta e="T164" id="Seg_4252" s="T163">np.h:O</ta>
            <ta e="T166" id="Seg_4253" s="T165">np.h:S</ta>
            <ta e="T168" id="Seg_4254" s="T167">v:pred</ta>
            <ta e="T170" id="Seg_4255" s="T168">s:temp</ta>
            <ta e="T171" id="Seg_4256" s="T170">np.h:S</ta>
            <ta e="T172" id="Seg_4257" s="T171">s:adv</ta>
            <ta e="T173" id="Seg_4258" s="T172">v:pred</ta>
            <ta e="T180" id="Seg_4259" s="T177">s:cond</ta>
            <ta e="T182" id="Seg_4260" s="T181">np:O</ta>
            <ta e="T185" id="Seg_4261" s="T183">0.3.h:S v:pred</ta>
            <ta e="T186" id="Seg_4262" s="T185">pro.h:S</ta>
            <ta e="T187" id="Seg_4263" s="T186">v:pred</ta>
            <ta e="T188" id="Seg_4264" s="T187">np.h:S</ta>
            <ta e="T189" id="Seg_4265" s="T188">v:pred</ta>
            <ta e="T191" id="Seg_4266" s="T190">0.2.h:S v:pred</ta>
            <ta e="T192" id="Seg_4267" s="T191">np.h:S</ta>
            <ta e="T193" id="Seg_4268" s="T192">v:pred</ta>
            <ta e="T194" id="Seg_4269" s="T193">np.h:S</ta>
            <ta e="T196" id="Seg_4270" s="T195">v:pred</ta>
            <ta e="T197" id="Seg_4271" s="T196">pro:S</ta>
            <ta e="T198" id="Seg_4272" s="T197">n:pred</ta>
            <ta e="T201" id="Seg_4273" s="T200">0.2.h:S v:pred</ta>
            <ta e="T204" id="Seg_4274" s="T202">0.2.h:S v:pred</ta>
            <ta e="T206" id="Seg_4275" s="T205">0.2.h:S v:pred</ta>
            <ta e="T207" id="Seg_4276" s="T206">np.h:S</ta>
            <ta e="T208" id="Seg_4277" s="T207">0.1.h:S v:pred</ta>
            <ta e="T210" id="Seg_4278" s="T209">np:S</ta>
            <ta e="T212" id="Seg_4279" s="T211">v:pred</ta>
            <ta e="T214" id="Seg_4280" s="T213">0.1.h:S v:pred</ta>
            <ta e="T215" id="Seg_4281" s="T214">0.1.h:S v:pred</ta>
            <ta e="T216" id="Seg_4282" s="T215">0.3.h:S v:pred</ta>
            <ta e="T217" id="Seg_4283" s="T216">np.h:S</ta>
            <ta e="T220" id="Seg_4284" s="T218">v:pred</ta>
            <ta e="T222" id="Seg_4285" s="T221">np.h:S</ta>
            <ta e="T224" id="Seg_4286" s="T223">v:pred</ta>
            <ta e="T226" id="Seg_4287" s="T225">np.h:O</ta>
            <ta e="T227" id="Seg_4288" s="T226">0.3.h:S v:pred</ta>
            <ta e="T229" id="Seg_4289" s="T228">pro.h:S</ta>
            <ta e="T233" id="Seg_4290" s="T232">np:O</ta>
            <ta e="T234" id="Seg_4291" s="T233">v:pred</ta>
            <ta e="T241" id="Seg_4292" s="T234">s:adv</ta>
            <ta e="T242" id="Seg_4293" s="T241">np.h:S</ta>
            <ta e="T243" id="Seg_4294" s="T242">0.1.h:S v:pred</ta>
            <ta e="T244" id="Seg_4295" s="T243">0.3.h:S v:pred</ta>
            <ta e="T248" id="Seg_4296" s="T247">adj:pred</ta>
            <ta e="T249" id="Seg_4297" s="T248">cop</ta>
            <ta e="T251" id="Seg_4298" s="T250">np:S</ta>
            <ta e="T252" id="Seg_4299" s="T251">np.h:S</ta>
            <ta e="T254" id="Seg_4300" s="T252">s:rel</ta>
            <ta e="T255" id="Seg_4301" s="T254">v:pred</ta>
            <ta e="T260" id="Seg_4302" s="T259">np:O</ta>
            <ta e="T261" id="Seg_4303" s="T260">np.h:S</ta>
            <ta e="T262" id="Seg_4304" s="T261">pro:O</ta>
            <ta e="T263" id="Seg_4305" s="T262">v:pred</ta>
            <ta e="T265" id="Seg_4306" s="T264">np:S</ta>
            <ta e="T268" id="Seg_4307" s="T266">v:pred</ta>
            <ta e="T271" id="Seg_4308" s="T268">s:rel</ta>
            <ta e="T274" id="Seg_4309" s="T271">s:rel</ta>
            <ta e="T278" id="Seg_4310" s="T274">s:rel</ta>
            <ta e="T279" id="Seg_4311" s="T278">np.h:S</ta>
            <ta e="T283" id="Seg_4312" s="T282">v:pred</ta>
            <ta e="T286" id="Seg_4313" s="T283">s:purp</ta>
            <ta e="T287" id="Seg_4314" s="T286">np.h:S</ta>
            <ta e="T289" id="Seg_4315" s="T288">np.h:O</ta>
            <ta e="T293" id="Seg_4316" s="T291">v:pred</ta>
            <ta e="T297" id="Seg_4317" s="T294">s:rel</ta>
            <ta e="T298" id="Seg_4318" s="T297">np.h:O</ta>
            <ta e="T299" id="Seg_4319" s="T298">0.3:S adj:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T2" id="Seg_4320" s="T1">accs-gen</ta>
            <ta e="T3" id="Seg_4321" s="T2">accs-gen</ta>
            <ta e="T4" id="Seg_4322" s="T3">accs-gen</ta>
            <ta e="T5" id="Seg_4323" s="T4">accs-gen</ta>
            <ta e="T7" id="Seg_4324" s="T6">accs-gen</ta>
            <ta e="T9" id="Seg_4325" s="T8">accs-gen</ta>
            <ta e="T13" id="Seg_4326" s="T12">giv-active</ta>
            <ta e="T15" id="Seg_4327" s="T14">giv-active</ta>
            <ta e="T19" id="Seg_4328" s="T18">new</ta>
            <ta e="T21" id="Seg_4329" s="T20">giv-active</ta>
            <ta e="T24" id="Seg_4330" s="T23">giv-inactive</ta>
            <ta e="T27" id="Seg_4331" s="T26">giv-active</ta>
            <ta e="T29" id="Seg_4332" s="T28">new</ta>
            <ta e="T31" id="Seg_4333" s="T30">giv-inactive</ta>
            <ta e="T33" id="Seg_4334" s="T32">accs-gen</ta>
            <ta e="T35" id="Seg_4335" s="T34">giv-inactive</ta>
            <ta e="T37" id="Seg_4336" s="T36">giv-inactive</ta>
            <ta e="T41" id="Seg_4337" s="T40">giv-inactive</ta>
            <ta e="T42" id="Seg_4338" s="T41">new</ta>
            <ta e="T43" id="Seg_4339" s="T42">giv-active</ta>
            <ta e="T44" id="Seg_4340" s="T43">giv-inactive</ta>
            <ta e="T46" id="Seg_4341" s="T45">giv-active</ta>
            <ta e="T48" id="Seg_4342" s="T47">new</ta>
            <ta e="T51" id="Seg_4343" s="T50">new</ta>
            <ta e="T52" id="Seg_4344" s="T51">new</ta>
            <ta e="T54" id="Seg_4345" s="T53">giv-active</ta>
            <ta e="T56" id="Seg_4346" s="T55">giv-inactive</ta>
            <ta e="T59" id="Seg_4347" s="T58">giv-inactive</ta>
            <ta e="T60" id="Seg_4348" s="T59">quot-sp</ta>
            <ta e="T62" id="Seg_4349" s="T61">giv-inactive-Q</ta>
            <ta e="T64" id="Seg_4350" s="T63">giv-inactive-Q</ta>
            <ta e="T69" id="Seg_4351" s="T68">giv-inactive-Q</ta>
            <ta e="T73" id="Seg_4352" s="T72">new-Q</ta>
            <ta e="T76" id="Seg_4353" s="T75">new-Q</ta>
            <ta e="T78" id="Seg_4354" s="T77">giv-inactive-Q</ta>
            <ta e="T82" id="Seg_4355" s="T81">giv-inactive-Q</ta>
            <ta e="T85" id="Seg_4356" s="T84">accs-gen-Q</ta>
            <ta e="T88" id="Seg_4357" s="T87">giv-inactive-Q</ta>
            <ta e="T91" id="Seg_4358" s="T90">giv-inactive-Q</ta>
            <ta e="T93" id="Seg_4359" s="T92">accs-gen</ta>
            <ta e="T96" id="Seg_4360" s="T95">giv-inactive-Q</ta>
            <ta e="T100" id="Seg_4361" s="T99">giv-active-Q</ta>
            <ta e="T101" id="Seg_4362" s="T100">0.giv-active-Q</ta>
            <ta e="T103" id="Seg_4363" s="T102">0.giv-active-Q</ta>
            <ta e="T104" id="Seg_4364" s="T103">giv-active-Q</ta>
            <ta e="T108" id="Seg_4365" s="T106">0.giv-active-Q</ta>
            <ta e="T110" id="Seg_4366" s="T109">0.giv-active-Q</ta>
            <ta e="T111" id="Seg_4367" s="T110">accs-aggr-Q</ta>
            <ta e="T115" id="Seg_4368" s="T114">new-Q</ta>
            <ta e="T116" id="Seg_4369" s="T115">0.giv-inactive-Q</ta>
            <ta e="T118" id="Seg_4370" s="T117">giv-active</ta>
            <ta e="T122" id="Seg_4371" s="T121">giv-active-Q</ta>
            <ta e="T126" id="Seg_4372" s="T125">0.giv-active-Q</ta>
            <ta e="T127" id="Seg_4373" s="T126">giv-inactive</ta>
            <ta e="T128" id="Seg_4374" s="T127">quot-sp</ta>
            <ta e="T130" id="Seg_4375" s="T129">giv-inactive-Q</ta>
            <ta e="T132" id="Seg_4376" s="T131">giv-inactive</ta>
            <ta e="T135" id="Seg_4377" s="T134">giv-inactive</ta>
            <ta e="T138" id="Seg_4378" s="T137">giv-active-Q</ta>
            <ta e="T141" id="Seg_4379" s="T140">giv-inactive</ta>
            <ta e="T142" id="Seg_4380" s="T141">quot-sp</ta>
            <ta e="T145" id="Seg_4381" s="T144">giv-active-Q</ta>
            <ta e="T149" id="Seg_4382" s="T148">0.giv-inactive-Q</ta>
            <ta e="T151" id="Seg_4383" s="T150">giv-active-Q</ta>
            <ta e="T154" id="Seg_4384" s="T153">giv-active</ta>
            <ta e="T156" id="Seg_4385" s="T155">giv-active-Q</ta>
            <ta e="T157" id="Seg_4386" s="T156">giv-inactive-Q</ta>
            <ta e="T161" id="Seg_4387" s="T160">0.giv-active-Q</ta>
            <ta e="T162" id="Seg_4388" s="T161">accs-inf-Q</ta>
            <ta e="T163" id="Seg_4389" s="T162">0.giv-active-Q</ta>
            <ta e="T164" id="Seg_4390" s="T163">giv-inactive-Q</ta>
            <ta e="T166" id="Seg_4391" s="T165">giv-active</ta>
            <ta e="T168" id="Seg_4392" s="T167">quot-sp</ta>
            <ta e="T171" id="Seg_4393" s="T170">giv-inactive-Q</ta>
            <ta e="T174" id="Seg_4394" s="T173">giv-active-Q</ta>
            <ta e="T176" id="Seg_4395" s="T175">accs-inf-Q</ta>
            <ta e="T179" id="Seg_4396" s="T178">0.giv-active-Q</ta>
            <ta e="T182" id="Seg_4397" s="T181">accs-inf-Q</ta>
            <ta e="T185" id="Seg_4398" s="T183">0.giv-active-Q</ta>
            <ta e="T186" id="Seg_4399" s="T185">giv-inactive-Q</ta>
            <ta e="T188" id="Seg_4400" s="T187">giv-inactive</ta>
            <ta e="T189" id="Seg_4401" s="T188">quot-sp</ta>
            <ta e="T190" id="Seg_4402" s="T189">giv-inactive</ta>
            <ta e="T191" id="Seg_4403" s="T190">0.giv-active-Q</ta>
            <ta e="T192" id="Seg_4404" s="T191">accs-aggr-Q</ta>
            <ta e="T194" id="Seg_4405" s="T193">giv-inactive-Q</ta>
            <ta e="T197" id="Seg_4406" s="T196">accs-sit-Q</ta>
            <ta e="T198" id="Seg_4407" s="T197">new-Q</ta>
            <ta e="T200" id="Seg_4408" s="T199">giv-inactive-Q</ta>
            <ta e="T201" id="Seg_4409" s="T200">0.giv-inactive-Q</ta>
            <ta e="T204" id="Seg_4410" s="T202">0.giv-active-Q</ta>
            <ta e="T206" id="Seg_4411" s="T205">0.giv-active-Q</ta>
            <ta e="T207" id="Seg_4412" s="T206">giv-active</ta>
            <ta e="T208" id="Seg_4413" s="T207">0.giv-active-Q</ta>
            <ta e="T210" id="Seg_4414" s="T209">accs-gen-Q</ta>
            <ta e="T214" id="Seg_4415" s="T213">0.giv-active-Q</ta>
            <ta e="T215" id="Seg_4416" s="T214">0.giv-active-Q</ta>
            <ta e="T216" id="Seg_4417" s="T215">0.quot-sp</ta>
            <ta e="T217" id="Seg_4418" s="T216">giv-inactive</ta>
            <ta e="T222" id="Seg_4419" s="T221">giv-inactive</ta>
            <ta e="T223" id="Seg_4420" s="T222">accs-inf</ta>
            <ta e="T226" id="Seg_4421" s="T225">accs-aggr-Q</ta>
            <ta e="T227" id="Seg_4422" s="T226">0.giv-inactive-Q</ta>
            <ta e="T228" id="Seg_4423" s="T227">giv-active-Q</ta>
            <ta e="T229" id="Seg_4424" s="T228">giv-active-Q</ta>
            <ta e="T233" id="Seg_4425" s="T232">new-Q</ta>
            <ta e="T235" id="Seg_4426" s="T234">accs-gen-Q</ta>
            <ta e="T238" id="Seg_4427" s="T237">accs-gen-Q</ta>
            <ta e="T242" id="Seg_4428" s="T241">giv-inactive</ta>
            <ta e="T243" id="Seg_4429" s="T242">0.giv-active-Q</ta>
            <ta e="T244" id="Seg_4430" s="T243">0.quot-sp</ta>
            <ta e="T245" id="Seg_4431" s="T244">accs-gen-Q</ta>
            <ta e="T249" id="Seg_4432" s="T248">0.giv-inactive-Q</ta>
            <ta e="T251" id="Seg_4433" s="T250">giv-active-Q</ta>
            <ta e="T252" id="Seg_4434" s="T251">giv-inactive</ta>
            <ta e="T256" id="Seg_4435" s="T255">giv-inactive</ta>
            <ta e="T260" id="Seg_4436" s="T259">giv-inactive</ta>
            <ta e="T261" id="Seg_4437" s="T260">giv-inactive</ta>
            <ta e="T262" id="Seg_4438" s="T261">giv-active</ta>
            <ta e="T265" id="Seg_4439" s="T264">giv-active</ta>
            <ta e="T266" id="Seg_4440" s="T265">accs-inf</ta>
            <ta e="T269" id="Seg_4441" s="T268">giv-inactive</ta>
            <ta e="T272" id="Seg_4442" s="T271">giv-inactive</ta>
            <ta e="T275" id="Seg_4443" s="T274">giv-inactive</ta>
            <ta e="T278" id="Seg_4444" s="T277">giv-active</ta>
            <ta e="T279" id="Seg_4445" s="T278">giv-active</ta>
            <ta e="T282" id="Seg_4446" s="T281">giv-inactive</ta>
            <ta e="T287" id="Seg_4447" s="T286">giv-active</ta>
            <ta e="T289" id="Seg_4448" s="T288">giv-inactive</ta>
            <ta e="T295" id="Seg_4449" s="T294">giv-active</ta>
            <ta e="T298" id="Seg_4450" s="T297">giv-inactive</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T1" id="Seg_4451" s="T0">top.int.concr.</ta>
            <ta e="T13" id="Seg_4452" s="T11">top.int.concr</ta>
            <ta e="T18" id="Seg_4453" s="T17">top.int.concr.</ta>
            <ta e="T21" id="Seg_4454" s="T20">top.int.concr</ta>
            <ta e="T23" id="Seg_4455" s="T22">top.int.concr.</ta>
            <ta e="T27" id="Seg_4456" s="T26">top.int.concr</ta>
            <ta e="T31" id="Seg_4457" s="T30">top.int.concr</ta>
            <ta e="T42" id="Seg_4458" s="T39">top.int.concr</ta>
            <ta e="T43" id="Seg_4459" s="T42">top.int.concr</ta>
            <ta e="T51" id="Seg_4460" s="T49">top.int.concr</ta>
            <ta e="T52" id="Seg_4461" s="T51">top.int.concr</ta>
            <ta e="T56" id="Seg_4462" s="T55">top.int.concr</ta>
            <ta e="T59" id="Seg_4463" s="T58">top.int.concr</ta>
            <ta e="T62" id="Seg_4464" s="T61">top.int.concr</ta>
            <ta e="T64" id="Seg_4465" s="T63">top.int.concr</ta>
            <ta e="T69" id="Seg_4466" s="T68">top.int.concr</ta>
            <ta e="T73" id="Seg_4467" s="T70">top.int.concr</ta>
            <ta e="T78" id="Seg_4468" s="T77">top.int.concr.</ta>
            <ta e="T86" id="Seg_4469" s="T84">top.int.concr</ta>
            <ta e="T88" id="Seg_4470" s="T87">top.int.concr</ta>
            <ta e="T98" id="Seg_4471" s="T94">top.int.concr.</ta>
            <ta e="T103" id="Seg_4472" s="T102">0.top.int.concr</ta>
            <ta e="T104" id="Seg_4473" s="T103">top.int.concr</ta>
            <ta e="T108" id="Seg_4474" s="T106">0.top.int.concr</ta>
            <ta e="T110" id="Seg_4475" s="T109">0.top.int.concr</ta>
            <ta e="T111" id="Seg_4476" s="T110">top.int.concr</ta>
            <ta e="T116" id="Seg_4477" s="T115">0.top.int.concr</ta>
            <ta e="T117" id="Seg_4478" s="T116">top.int.concr.</ta>
            <ta e="T122" id="Seg_4479" s="T121">top.int.concr</ta>
            <ta e="T125" id="Seg_4480" s="T124">top.int.concr</ta>
            <ta e="T127" id="Seg_4481" s="T126">top.int.concr</ta>
            <ta e="T130" id="Seg_4482" s="T129">top.int.concr</ta>
            <ta e="T132" id="Seg_4483" s="T131">top.int.concr</ta>
            <ta e="T135" id="Seg_4484" s="T134">top.int.concr</ta>
            <ta e="T138" id="Seg_4485" s="T137">top.int.concr</ta>
            <ta e="T141" id="Seg_4486" s="T140">top.int.concr</ta>
            <ta e="T145" id="Seg_4487" s="T144">top.int.concr</ta>
            <ta e="T149" id="Seg_4488" s="T148">0.top.int.concr</ta>
            <ta e="T156" id="Seg_4489" s="T155">top.int.concr</ta>
            <ta e="T161" id="Seg_4490" s="T160">0.top.int.concr</ta>
            <ta e="T163" id="Seg_4491" s="T162">0.top.int.concr</ta>
            <ta e="T165" id="Seg_4492" s="T164">top.int.concr.</ta>
            <ta e="T171" id="Seg_4493" s="T170">top.int.concr</ta>
            <ta e="T179" id="Seg_4494" s="T178">0.top.int.concr</ta>
            <ta e="T185" id="Seg_4495" s="T183">0.top.int.concr</ta>
            <ta e="T186" id="Seg_4496" s="T185">top.int.concr</ta>
            <ta e="T188" id="Seg_4497" s="T187">top.int.concr</ta>
            <ta e="T191" id="Seg_4498" s="T190">0.top.int.concr</ta>
            <ta e="T192" id="Seg_4499" s="T191">top.int.concr</ta>
            <ta e="T194" id="Seg_4500" s="T193">top.int.concr</ta>
            <ta e="T197" id="Seg_4501" s="T196">top.int.concr</ta>
            <ta e="T201" id="Seg_4502" s="T200">0.top.int.concr</ta>
            <ta e="T204" id="Seg_4503" s="T202">0.top.int.concr</ta>
            <ta e="T206" id="Seg_4504" s="T205">0.top.int.concr</ta>
            <ta e="T208" id="Seg_4505" s="T207">0.top.int.concr</ta>
            <ta e="T210" id="Seg_4506" s="T208">top.int.concr</ta>
            <ta e="T214" id="Seg_4507" s="T213">0.top.int.concr</ta>
            <ta e="T215" id="Seg_4508" s="T214">0.top.int.concr</ta>
            <ta e="T217" id="Seg_4509" s="T216">top.int.concr</ta>
            <ta e="T221" id="Seg_4510" s="T220">top.int.concr.</ta>
            <ta e="T227" id="Seg_4511" s="T226">0.top.int.concr</ta>
            <ta e="T228" id="Seg_4512" s="T227">top.int.concr</ta>
            <ta e="T243" id="Seg_4513" s="T242">0.top.int.concr</ta>
            <ta e="T251" id="Seg_4514" s="T250">top.ext</ta>
            <ta e="T252" id="Seg_4515" s="T251">top.int.concr</ta>
            <ta e="T261" id="Seg_4516" s="T260">top.int.concr</ta>
            <ta e="T265" id="Seg_4517" s="T264">top.int.concr</ta>
            <ta e="T279" id="Seg_4518" s="T278">top.int.concr</ta>
            <ta e="T287" id="Seg_4519" s="T286">top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T11" id="Seg_4520" s="T0">foc.wid</ta>
            <ta e="T16" id="Seg_4521" s="T15">foc.nar</ta>
            <ta e="T20" id="Seg_4522" s="T17">foc.wid</ta>
            <ta e="T22" id="Seg_4523" s="T21">foc.int</ta>
            <ta e="T26" id="Seg_4524" s="T22">foc.wid</ta>
            <ta e="T29" id="Seg_4525" s="T27">foc.nar</ta>
            <ta e="T33" id="Seg_4526" s="T32">foc.nar</ta>
            <ta e="T35" id="Seg_4527" s="T34">foc.nar</ta>
            <ta e="T37" id="Seg_4528" s="T36">foc.nar</ta>
            <ta e="T44" id="Seg_4529" s="T43">foc.nar</ta>
            <ta e="T49" id="Seg_4530" s="T45">foc.int</ta>
            <ta e="T55" id="Seg_4531" s="T49">foc.wid</ta>
            <ta e="T56" id="Seg_4532" s="T55">foc.nar</ta>
            <ta e="T60" id="Seg_4533" s="T58">foc.wid</ta>
            <ta e="T63" id="Seg_4534" s="T61">foc.wid</ta>
            <ta e="T66" id="Seg_4535" s="T64">foc.nar</ta>
            <ta e="T70" id="Seg_4536" s="T69">foc.int</ta>
            <ta e="T76" id="Seg_4537" s="T73">foc.nar</ta>
            <ta e="T80" id="Seg_4538" s="T79">foc.nar</ta>
            <ta e="T87" id="Seg_4539" s="T86">foc.nar</ta>
            <ta e="T92" id="Seg_4540" s="T89">foc.int</ta>
            <ta e="T101" id="Seg_4541" s="T98">foc.wid</ta>
            <ta e="T102" id="Seg_4542" s="T101">foc.contr</ta>
            <ta e="T106" id="Seg_4543" s="T105">foc.int</ta>
            <ta e="T108" id="Seg_4544" s="T106">foc.int</ta>
            <ta e="T109" id="Seg_4545" s="T108">foc.contr</ta>
            <ta e="T113" id="Seg_4546" s="T111">foc.int</ta>
            <ta e="T115" id="Seg_4547" s="T114">foc.nar</ta>
            <ta e="T119" id="Seg_4548" s="T116">foc.wid</ta>
            <ta e="T124" id="Seg_4549" s="T123">foc.int</ta>
            <ta e="T125" id="Seg_4550" s="T124">foc.nar</ta>
            <ta e="T128" id="Seg_4551" s="T126">foc.wid</ta>
            <ta e="T130" id="Seg_4552" s="T129">foc.nar</ta>
            <ta e="T133" id="Seg_4553" s="T132">foc.nar</ta>
            <ta e="T135" id="Seg_4554" s="T134">foc.nar</ta>
            <ta e="T138" id="Seg_4555" s="T137">foc.nar</ta>
            <ta e="T142" id="Seg_4556" s="T140">foc.wid</ta>
            <ta e="T145" id="Seg_4557" s="T144">foc.nar</ta>
            <ta e="T152" id="Seg_4558" s="T148">foc.wid</ta>
            <ta e="T159" id="Seg_4559" s="T155">foc.ver</ta>
            <ta e="T160" id="Seg_4560" s="T159">foc.nar</ta>
            <ta e="T162" id="Seg_4561" s="T161">foc.nar</ta>
            <ta e="T168" id="Seg_4562" s="T164">foc.wid</ta>
            <ta e="T176" id="Seg_4563" s="T173">foc.nar</ta>
            <ta e="T179" id="Seg_4564" s="T177">foc.ver</ta>
            <ta e="T185" id="Seg_4565" s="T180">foc.int</ta>
            <ta e="T187" id="Seg_4566" s="T186">foc.int</ta>
            <ta e="T190" id="Seg_4567" s="T187">foc.wid</ta>
            <ta e="T191" id="Seg_4568" s="T190">foc.int</ta>
            <ta e="T193" id="Seg_4569" s="T192">foc.int</ta>
            <ta e="T195" id="Seg_4570" s="T193">foc.nar</ta>
            <ta e="T198" id="Seg_4571" s="T197">foc.nar</ta>
            <ta e="T201" id="Seg_4572" s="T198">foc.int</ta>
            <ta e="T204" id="Seg_4573" s="T202">foc.int</ta>
            <ta e="T205" id="Seg_4574" s="T204">foc.nar</ta>
            <ta e="T208" id="Seg_4575" s="T207">foc.int</ta>
            <ta e="T211" id="Seg_4576" s="T210">foc.nar</ta>
            <ta e="T214" id="Seg_4577" s="T213">foc.int</ta>
            <ta e="T215" id="Seg_4578" s="T214">foc.int</ta>
            <ta e="T217" id="Seg_4579" s="T216">foc.nar</ta>
            <ta e="T224" id="Seg_4580" s="T220">foc.wid</ta>
            <ta e="T226" id="Seg_4581" s="T225">foc.nar</ta>
            <ta e="T234" id="Seg_4582" s="T229">foc.int</ta>
            <ta e="T243" id="Seg_4583" s="T242">foc.int</ta>
            <ta e="T248" id="Seg_4584" s="T244">foc.nar</ta>
            <ta e="T260" id="Seg_4585" s="T254">foc.int</ta>
            <ta e="T263" id="Seg_4586" s="T261">foc.int</ta>
            <ta e="T268" id="Seg_4587" s="T265">foc.int</ta>
            <ta e="T283" id="Seg_4588" s="T279">foc.int</ta>
            <ta e="T291" id="Seg_4589" s="T287">foc.nar</ta>
         </annotation>
         <annotation name="BOR" tierref="TIE0" />
         <annotation name="BOR-Phon" tierref="TIE2" />
         <annotation name="BOR-Morph" tierref="TIE3" />
         <annotation name="CS" tierref="TIE4" />
         <annotation name="fe" tierref="fe">
            <ta e="T11" id="Seg_4590" s="T0">A long time ago geese, ducks, little birds, snow owls — all the birds lived in this land.</ta>
            <ta e="T17" id="Seg_4591" s="T11">They say, at that time this land used to be warm.</ta>
            <ta e="T22" id="Seg_4592" s="T17">Then the cold came, the times have changed.</ta>
            <ta e="T26" id="Seg_4593" s="T22">All the birds started to discuss it.</ta>
            <ta e="T30" id="Seg_4594" s="T26">They all had a leader.</ta>
            <ta e="T39" id="Seg_4595" s="T30">The snow owl already at that time ate partridges, ducks, and geese either.</ta>
            <ta e="T49" id="Seg_4596" s="T39">The leader of the birds (he came from the geese) gathered everyone.</ta>
            <ta e="T55" id="Seg_4597" s="T49">A valley and a forested mountain were filled with birds.</ta>
            <ta e="T58" id="Seg_4598" s="T55">The snow owl also came.</ta>
            <ta e="T60" id="Seg_4599" s="T58">The leader says: </ta>
            <ta e="T63" id="Seg_4600" s="T60">"Well, the times have changed.</ta>
            <ta e="T68" id="Seg_4601" s="T63">Only in summer we can live here.</ta>
            <ta e="T77" id="Seg_4602" s="T68">I know: At some place there is a land with no winter.</ta>
            <ta e="T84" id="Seg_4603" s="T77">Why should we, the feathered tribe, freeze in this land?!</ta>
            <ta e="T87" id="Seg_4604" s="T84">It is too far to go there on foot.</ta>
            <ta e="T94" id="Seg_4605" s="T87">Let us fly to this land when the winter comes.</ta>
            <ta e="T101" id="Seg_4606" s="T94">Until this cold is gone, let us live at that place.</ta>
            <ta e="T108" id="Seg_4607" s="T101">If we stay here, we will freeze and die.</ta>
            <ta e="T113" id="Seg_4608" s="T108">If we fly there, then we will all stay alive.</ta>
            <ta e="T116" id="Seg_4609" s="T113">Well, give your advise!"</ta>
            <ta e="T119" id="Seg_4610" s="T116">On this all the birds [said]: </ta>
            <ta e="T121" id="Seg_4611" s="T119">"Yes, that's right!</ta>
            <ta e="T124" id="Seg_4612" s="T121">We all will fly.</ta>
            <ta e="T126" id="Seg_4613" s="T124">Here we would die."</ta>
            <ta e="T128" id="Seg_4614" s="T126">The leader asks: </ta>
            <ta e="T131" id="Seg_4615" s="T128">"So, how many of you agree [to fly]?"</ta>
            <ta e="T134" id="Seg_4616" s="T131">All the birds agreed.</ta>
            <ta e="T137" id="Seg_4617" s="T134">The snow owl also said something: </ta>
            <ta e="T140" id="Seg_4618" s="T137">"I will also go".</ta>
            <ta e="T142" id="Seg_4619" s="T140">The leader asks: </ta>
            <ta e="T148" id="Seg_4620" s="T142">"Well, now the snow owl wants to come along.</ta>
            <ta e="T152" id="Seg_4621" s="T148">Do you agree that she comes along?"</ta>
            <ta e="T155" id="Seg_4622" s="T152">On this the birds: </ta>
            <ta e="T159" id="Seg_4623" s="T155">"We do not want the owl to fly with us".</ta>
            <ta e="T161" id="Seg_4624" s="T159">"Why don't you agree?</ta>
            <ta e="T164" id="Seg_4625" s="T161">Say it to her face."</ta>
            <ta e="T168" id="Seg_4626" s="T164">The birds said then: </ta>
            <ta e="T177" id="Seg_4627" s="T168">"Here the owl lives on our kins.</ta>
            <ta e="T185" id="Seg_4628" s="T177">And even if she goes there, she will not break that habit.</ta>
            <ta e="T187" id="Seg_4629" s="T185">We will not take her with us!"</ta>
            <ta e="T190" id="Seg_4630" s="T187">The leader says to the owl: </ta>
            <ta e="T193" id="Seg_4631" s="T190">"Look, everybody is against.</ta>
            <ta e="T198" id="Seg_4632" s="T193">All the birds said so — that is the rule.</ta>
            <ta e="T204" id="Seg_4633" s="T198">You will stay on this land, die frozen!</ta>
            <ta e="T206" id="Seg_4634" s="T204">What do you think?"</ta>
            <ta e="T207" id="Seg_4635" s="T206">The snow owl: </ta>
            <ta e="T208" id="Seg_4636" s="T207">"I will not argue.</ta>
            <ta e="T214" id="Seg_4637" s="T208">A lonesome soul does not die — maybe I will survive.</ta>
            <ta e="T216" id="Seg_4638" s="T214">I will stay", she said.</ta>
            <ta e="T220" id="Seg_4639" s="T216">All the birds flew away.</ta>
            <ta e="T224" id="Seg_4640" s="T220">The snow owl comes to its old owl: </ta>
            <ta e="T227" id="Seg_4641" s="T224">"Well, they don't take us with them.</ta>
            <ta e="T234" id="Seg_4642" s="T227">Sew me a warm fur coat out of the fur of a spotted seal!</ta>
            <ta e="T241" id="Seg_4643" s="T234">Such [a coat] that keeps you warm by snowstorm and cold weather." </ta>
            <ta e="T244" id="Seg_4644" s="T241">The old owl: "I will sew it!", she says.</ta>
            <ta e="T251" id="Seg_4645" s="T244">"The coat that shall be as white as snow!"</ta>
            <ta e="T260" id="Seg_4646" s="T251">The old owl, such a mistress, sewed a coat which was whiter than that of a goose. </ta>
            <ta e="T278" id="Seg_4647" s="T260">The snow owl put it on — it suited just right, sticked right to her body: when it is cold, she stays warm, the snowstorm doesn't blow through, and the snow doesn't stick on it. </ta>
            <ta e="T283" id="Seg_4648" s="T278">From there on the snow owl inhabited that land.</ta>
            <ta e="T293" id="Seg_4649" s="T283">In revenge for having been left, the snow owl started to eat even more small birds. </ta>
            <ta e="T298" id="Seg_4650" s="T293">Especially the partridges wintering with her. </ta>
            <ta e="T299" id="Seg_4651" s="T298">The end.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T11" id="Seg_4652" s="T0">Die Gänse, die Enten, die Vögelchen, die Schneeeulen — alle Vögel lebten vor langer Zeit hier in diesem Land.</ta>
            <ta e="T17" id="Seg_4653" s="T11">Man sagt, dass dieses Land damals warm war.</ta>
            <ta e="T22" id="Seg_4654" s="T17">Dann kam die Kälte, die Zeiten änderten sich.</ta>
            <ta e="T26" id="Seg_4655" s="T22">Da berieten sich alle Vögel.</ta>
            <ta e="T30" id="Seg_4656" s="T26">Jeder von ihnen hatte einen Anführer.</ta>
            <ta e="T39" id="Seg_4657" s="T30">Die Schneeeule fraß damals schon Rebhühner, Ente und auch Gänse.</ta>
            <ta e="T49" id="Seg_4658" s="T39">Der Anführer der Vögel (er kam von den Gänsen) rief alle Vögel zu einer Versammlung.</ta>
            <ta e="T55" id="Seg_4659" s="T49">Ein Tal und einen bewaldeten Berg füllten die Vögel.</ta>
            <ta e="T58" id="Seg_4660" s="T55">Auch die Schneeeule kam.</ta>
            <ta e="T60" id="Seg_4661" s="T58">Der Anführer sagt: </ta>
            <ta e="T63" id="Seg_4662" s="T60">"Nun, die Zeiten haben sich geändert.</ta>
            <ta e="T68" id="Seg_4663" s="T63">Nur im Sommer können wir hier leben.</ta>
            <ta e="T77" id="Seg_4664" s="T68">Ich weiß: An einem Ort gibt es ein Land, wo es keinen Winter gibt.</ta>
            <ta e="T84" id="Seg_4665" s="T77">Warum sollen wir, die Flügel haben, in diesem Land erfrieren?!</ta>
            <ta e="T87" id="Seg_4666" s="T84">Die Erde entlang zu gehen ist weit.</ta>
            <ta e="T94" id="Seg_4667" s="T87">Lasst uns, wenn der Winter kommt, direkt in dieses Land fliegen.</ta>
            <ta e="T101" id="Seg_4668" s="T94">Bis die Kälte vorbeigeht, lasst uns an jenem Ort wohnen.</ta>
            <ta e="T108" id="Seg_4669" s="T101">Wenn wir weiter hier leben, dann werden wir alle erfrieren und sterben.</ta>
            <ta e="T113" id="Seg_4670" s="T108">Wenn wir dorthin fliegen, dann bleiben wir alle am Leben.</ta>
            <ta e="T116" id="Seg_4671" s="T113">Nun, gebt euren Rat!"</ta>
            <ta e="T119" id="Seg_4672" s="T116">Darauf alle Vögel: </ta>
            <ta e="T121" id="Seg_4673" s="T119">"Ja, das stimmt!</ta>
            <ta e="T124" id="Seg_4674" s="T121">Wir werden alle fliegen.</ta>
            <ta e="T126" id="Seg_4675" s="T124">Hier würden wir sterben!"</ta>
            <ta e="T128" id="Seg_4676" s="T126">Der Anführer fragt: </ta>
            <ta e="T131" id="Seg_4677" s="T128">"Nun, wie viele von euch sind einverstanden?"</ta>
            <ta e="T134" id="Seg_4678" s="T131">Die Vögel waren alle einverstanden.</ta>
            <ta e="T137" id="Seg_4679" s="T134">Die Schneeeule sagte auch etwas: </ta>
            <ta e="T140" id="Seg_4680" s="T137">"Ich komme auch mit."</ta>
            <ta e="T142" id="Seg_4681" s="T140">Der Anführer fragt: </ta>
            <ta e="T148" id="Seg_4682" s="T142">"Nun möchte diese Schneeeule auch mitkommen.</ta>
            <ta e="T152" id="Seg_4683" s="T148">Seid ihr einverstanden, dass sie mitkommt?"</ta>
            <ta e="T155" id="Seg_4684" s="T152">Darauf alle Vögel: </ta>
            <ta e="T159" id="Seg_4685" s="T155">"Wir sind nicht einverstanden, dass die Schneeeule mitkommt."</ta>
            <ta e="T161" id="Seg_4686" s="T159">"Warum seid ihr nicht einverstanden?</ta>
            <ta e="T164" id="Seg_4687" s="T161">Sagt es ihr ins Gesicht."</ta>
            <ta e="T168" id="Seg_4688" s="T164">Da sagten alle Vögel: </ta>
            <ta e="T177" id="Seg_4689" s="T168">"Hier nämlich ernährt sich die Schneeeule von unseren Familien.</ta>
            <ta e="T185" id="Seg_4690" s="T177">Und auch, wenn sie dorthin geht, wird sie das nicht sein lassen.</ta>
            <ta e="T187" id="Seg_4691" s="T185">Wir nehmen sie nicht mit!"</ta>
            <ta e="T190" id="Seg_4692" s="T187">Der Anführer sagt zur Schneeeule: </ta>
            <ta e="T193" id="Seg_4693" s="T190">"Schau, die Leute sind nicht einverstanden.</ta>
            <ta e="T198" id="Seg_4694" s="T193">Alle Vögel haben es gesagt — das ist das Gesetz.</ta>
            <ta e="T204" id="Seg_4695" s="T198">Du bleibst an diesem Ort, erfriere hier!</ta>
            <ta e="T206" id="Seg_4696" s="T204">Was meinst du?"</ta>
            <ta e="T207" id="Seg_4697" s="T206">Die Schneeeule: </ta>
            <ta e="T208" id="Seg_4698" s="T207">"Ich streite nicht.</ta>
            <ta e="T214" id="Seg_4699" s="T208">Eine einsame Seele stirbt nirgendwo — vielleicht überlebe ich.</ta>
            <ta e="T216" id="Seg_4700" s="T214">Ich bleibe", sagt sie.</ta>
            <ta e="T220" id="Seg_4701" s="T216">Alle Vögel flogen weg.</ta>
            <ta e="T224" id="Seg_4702" s="T220">Darauf kommt die Schneeeule zu ihrer Alten: </ta>
            <ta e="T227" id="Seg_4703" s="T224">"Nun, sie nehmen uns nicht mit.</ta>
            <ta e="T234" id="Seg_4704" s="T227">Nähe mir aus dem Pelz einer Largarobbe einen gut schließenden Mantel.</ta>
            <ta e="T241" id="Seg_4705" s="T234">So einen, der im Schneesturm nicht zugeweht wird und den die Kälte nicht durchdringt."</ta>
            <ta e="T244" id="Seg_4706" s="T241">Seine Alte: "Ich nähe ihn!", sagt sie.</ta>
            <ta e="T251" id="Seg_4707" s="T244">"Er soll so weiß sein wie Schnee, dieser Mantel!"</ta>
            <ta e="T260" id="Seg_4708" s="T251">Seine Alte, so eine Meisterin, nähte einen Mantel, weißer als der einer Gans.</ta>
            <ta e="T278" id="Seg_4709" s="T260">Die Schneeeule zog ihn an — wie angegossen umschloss der Mantel ihren Körper, in der Kälte friert sie nicht, im Schneesturm bleibt nichts hängen, kein Schnee bleibt an ihr hängen.</ta>
            <ta e="T283" id="Seg_4710" s="T278">Von da an bewohnte die Schneeeule dieses Land.</ta>
            <ta e="T293" id="Seg_4711" s="T283">Um zu rächen, dass sie blieb, fing die Schneeeule an, noch mehr kleine Vögel zu fressen.</ta>
            <ta e="T298" id="Seg_4712" s="T293">Besonders die mit ihr überwinternden Rebhühner.</ta>
            <ta e="T299" id="Seg_4713" s="T298">Ende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T11" id="Seg_4714" s="T0">В старину гуси, утки, птички, полярная сова — все птицы на этой земле обитали.</ta>
            <ta e="T17" id="Seg_4715" s="T11">Эта земля в ту пору теплой была, говорят.</ta>
            <ta e="T22" id="Seg_4716" s="T17">Потом холода наступили, времена переменились.</ta>
            <ta e="T26" id="Seg_4717" s="T22">Стали пернатые совет держать.</ta>
            <ta e="T30" id="Seg_4718" s="T26">У всех у них был один старший.</ta>
            <ta e="T39" id="Seg_4719" s="T30">Полярная сова-то уже тогда куропатками питалась, утками и гусями тоже.</ta>
            <ta e="T49" id="Seg_4720" s="T39">Старший из пернатых (он был из гусей) всех на сход созвал.</ta>
            <ta e="T55" id="Seg_4721" s="T49">Всю мшистую низину и весь нагорный лес птицы заполнили.</ta>
            <ta e="T58" id="Seg_4722" s="T55">Полярная сова тоже пришла.</ta>
            <ta e="T60" id="Seg_4723" s="T58">Старший говорит: </ta>
            <ta e="T63" id="Seg_4724" s="T60">"Ну вот, времена переменились.</ta>
            <ta e="T68" id="Seg_4725" s="T63">Только летом можно [здесь] жить.</ta>
            <ta e="T77" id="Seg_4726" s="T68">Я знаю: в каких-то краях есть земля, где не бывает зимы.</ta>
            <ta e="T84" id="Seg_4727" s="T77">Зачем нам, пернатым, мерзнуть на этой земле?!</ta>
            <ta e="T87" id="Seg_4728" s="T84">Пешему туда далеко.</ta>
            <ta e="T94" id="Seg_4729" s="T87">Давайте на зиму в ту землю напрямик полетим.</ta>
            <ta e="T101" id="Seg_4730" s="T94">Пока морозы здесь не кончатся, обживем ту землю.</ta>
            <ta e="T108" id="Seg_4731" s="T101">Если здесь останемся жить, все пропадем, замерзнем.</ta>
            <ta e="T113" id="Seg_4732" s="T108">Если туда полетим, все в живых останемся.</ta>
            <ta e="T116" id="Seg_4733" s="T113">Ну, скажите, что вы думаете!"</ta>
            <ta e="T119" id="Seg_4734" s="T116">На это все пернатые: </ta>
            <ta e="T121" id="Seg_4735" s="T119">"Да, правда!</ta>
            <ta e="T124" id="Seg_4736" s="T121">Мы все полетим.</ta>
            <ta e="T126" id="Seg_4737" s="T124">Здесь погибнем!"</ta>
            <ta e="T128" id="Seg_4738" s="T126">Старший спрашивает: </ta>
            <ta e="T131" id="Seg_4739" s="T128">"Ну, все ли согласны?"</ta>
            <ta e="T134" id="Seg_4740" s="T131">Все пернатые согласны.</ta>
            <ta e="T137" id="Seg_4741" s="T134">Полярная сова тоже слово свое сказала: </ta>
            <ta e="T140" id="Seg_4742" s="T137">"Я тоже полечу."</ta>
            <ta e="T142" id="Seg_4743" s="T140">Старший спрашивает: </ta>
            <ta e="T148" id="Seg_4744" s="T142">"Вот полярная сова тоже хочет с нами.</ta>
            <ta e="T152" id="Seg_4745" s="T148">Согласны ли, чтобы она полетела?"</ta>
            <ta e="T155" id="Seg_4746" s="T152">На это все пернатые: </ta>
            <ta e="T159" id="Seg_4747" s="T155">"Мы не хотим, чтобы полярная сова с нами летела."</ta>
            <ta e="T161" id="Seg_4748" s="T159">"А почему не хотите?</ta>
            <ta e="T164" id="Seg_4749" s="T161">[В глаза] скажите ей."</ta>
            <ta e="T168" id="Seg_4750" s="T164">Тут все пернатые говорят: </ta>
            <ta e="T177" id="Seg_4751" s="T168">"Здесь полярная сова питается нашими родами.</ta>
            <ta e="T185" id="Seg_4752" s="T177">И там своих замашек тоже не бросит.</ta>
            <ta e="T187" id="Seg_4753" s="T185">Мы ее с собой не возьмем!"</ta>
            <ta e="T190" id="Seg_4754" s="T187">Старший говорит полярной сове: </ta>
            <ta e="T193" id="Seg_4755" s="T190">"Видишь: все против.</ta>
            <ta e="T198" id="Seg_4756" s="T193">Раз все пернатые сказали — это закон.</ta>
            <ta e="T204" id="Seg_4757" s="T198">На этой земле оставайся, хоть замерзай.</ta>
            <ta e="T206" id="Seg_4758" s="T204">Что скажешь?"</ta>
            <ta e="T207" id="Seg_4759" s="T206">Полярная сова: </ta>
            <ta e="T208" id="Seg_4760" s="T207">"Не спорю.</ta>
            <ta e="T214" id="Seg_4761" s="T208">Одинокой душе где ни пропадать — может, выживу.</ta>
            <ta e="T216" id="Seg_4762" s="T214">Остаюсь", говорит.</ta>
            <ta e="T220" id="Seg_4763" s="T216">Все пернатые поулетали.</ta>
            <ta e="T224" id="Seg_4764" s="T220">Тут полярная сова к своей старухе приходит: </ta>
            <ta e="T227" id="Seg_4765" s="T224">"Ну вот, не берут они нас с собой.</ta>
            <ta e="T234" id="Seg_4766" s="T227">Ты сшей мне из меха ларгы закрытый сокуй.</ta>
            <ta e="T241" id="Seg_4767" s="T234">Такой, который пурга не продувает и мороз не пробирает."</ta>
            <ta e="T244" id="Seg_4768" s="T241">Старуха: "Сошью!", говорит.</ta>
            <ta e="T251" id="Seg_4769" s="T244">"Пусть белым как снег будет тот сокуй!"</ta>
            <ta e="T260" id="Seg_4770" s="T251">Старуха, такая мастерица, сшила белее гуся белый сокуй.</ta>
            <ta e="T278" id="Seg_4771" s="T260">Полярная сова его надела — впору, сокуй к телу накрепко пристал: и в мороз не холодно, и в пургу не продувает, и снег на него не налипает.</ta>
            <ta e="T283" id="Seg_4772" s="T278">Полярная сова с тех пор в этом краю обжилась.</ta>
            <ta e="T293" id="Seg_4773" s="T283">В отместку за то, что ее оставили, полярная сова мелких птиц еще пуще стала есть.</ta>
            <ta e="T298" id="Seg_4774" s="T293">Особенно зимующую вместе с ней куропатку.</ta>
            <ta e="T299" id="Seg_4775" s="T298">Конец.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T11" id="Seg_4776" s="T0">В старину гуси, утки, птички, полярная сова — все птицы на этой земле обитали.</ta>
            <ta e="T17" id="Seg_4777" s="T11">Эта земля в ту пору теплой была, говорят.</ta>
            <ta e="T22" id="Seg_4778" s="T17">Потом холода наступили, времена переменились.</ta>
            <ta e="T26" id="Seg_4779" s="T22">Стали пернатые совет держать.</ta>
            <ta e="T30" id="Seg_4780" s="T26">У всех у них был один старший,</ta>
            <ta e="T39" id="Seg_4781" s="T30">Полярная сова-то уже тогда куропатками питалась, утками и гусями тоже.</ta>
            <ta e="T49" id="Seg_4782" s="T39">Старший из пернатых (он был из гусей) всех на сход созвал.</ta>
            <ta e="T55" id="Seg_4783" s="T49">Всю мшистую низину и весь нагорный лес птицы заполнили.</ta>
            <ta e="T58" id="Seg_4784" s="T55">Полярная сова тоже пришла.</ta>
            <ta e="T60" id="Seg_4785" s="T58">Старший говорит: </ta>
            <ta e="T63" id="Seg_4786" s="T60">— Ну вот, времена переменились.</ta>
            <ta e="T68" id="Seg_4787" s="T63">Только летом можно [здесь] жить.</ta>
            <ta e="T77" id="Seg_4788" s="T68">Я знаю: в каких-то краях есть земля, где не бывает зимы.</ta>
            <ta e="T84" id="Seg_4789" s="T77">Зачем нам, пернатым, мерзнуть на этой земле?!</ta>
            <ta e="T87" id="Seg_4790" s="T84">Пешему туда далеко.</ta>
            <ta e="T94" id="Seg_4791" s="T87">Давайте на зиму в ту землю напрямик полетим.</ta>
            <ta e="T101" id="Seg_4792" s="T94">Пока морозы здесь не кончатся, обживем ту землю.</ta>
            <ta e="T108" id="Seg_4793" s="T101">Если здесь останемся жить, все пропадем, замерзнем.</ta>
            <ta e="T113" id="Seg_4794" s="T108">Если туда полетим, все в живых останемся.</ta>
            <ta e="T116" id="Seg_4795" s="T113">Ну, скажите, что вы думаете!</ta>
            <ta e="T119" id="Seg_4796" s="T116">На это все пернатые: </ta>
            <ta e="T121" id="Seg_4797" s="T119">— Да, правда!</ta>
            <ta e="T124" id="Seg_4798" s="T121">Мы все полетим.</ta>
            <ta e="T126" id="Seg_4799" s="T124">Здесь погибнем!</ta>
            <ta e="T128" id="Seg_4800" s="T126">Старший спрашивает: </ta>
            <ta e="T131" id="Seg_4801" s="T128">— Ну, все ли согласны?</ta>
            <ta e="T134" id="Seg_4802" s="T131">Все пернатые согласны.</ta>
            <ta e="T137" id="Seg_4803" s="T134">Полярная сова тоже слово свое сказала: </ta>
            <ta e="T140" id="Seg_4804" s="T137">— Я тоже полечу.</ta>
            <ta e="T142" id="Seg_4805" s="T140">Старший спрашивает: </ta>
            <ta e="T148" id="Seg_4806" s="T142">— Вот полярная сова тоже хочет с нами.</ta>
            <ta e="T152" id="Seg_4807" s="T148">Согласны ли, чтобы она полетела?</ta>
            <ta e="T155" id="Seg_4808" s="T152">На это все пернатые: </ta>
            <ta e="T159" id="Seg_4809" s="T155">— Мы не хотим, чтобы полярная сова с нами летела.</ta>
            <ta e="T161" id="Seg_4810" s="T159">— А почему не хотите?</ta>
            <ta e="T164" id="Seg_4811" s="T161">[В глаза] скажите ей.</ta>
            <ta e="T168" id="Seg_4812" s="T164">Тут все пернатые говорят: </ta>
            <ta e="T177" id="Seg_4813" s="T168">— Здесь полярная сова питается нами, пернатыми.</ta>
            <ta e="T185" id="Seg_4814" s="T177">И там своих замашек тоже не бросит.</ta>
            <ta e="T187" id="Seg_4815" s="T185">Мы [ее] с собой не возьмем!</ta>
            <ta e="T190" id="Seg_4816" s="T187">Старший говорит полярной сове: </ta>
            <ta e="T193" id="Seg_4817" s="T190">— Видишь: все против.</ta>
            <ta e="T198" id="Seg_4818" s="T193">Раз всё пернатые сказали — это закон.</ta>
            <ta e="T204" id="Seg_4819" s="T198">На этой земле оставайся, хоть замерзай.</ta>
            <ta e="T206" id="Seg_4820" s="T204">Что скажешь?</ta>
            <ta e="T207" id="Seg_4821" s="T206">Полярная сова: </ta>
            <ta e="T208" id="Seg_4822" s="T207">— Не спорю.</ta>
            <ta e="T214" id="Seg_4823" s="T208">Одинокой душе где ни пропадать — может, выживу.</ta>
            <ta e="T216" id="Seg_4824" s="T214">Остаюсь, — говорит.</ta>
            <ta e="T220" id="Seg_4825" s="T216">Все пернатые поулетали.</ta>
            <ta e="T224" id="Seg_4826" s="T220">Тут полярная сова к своей старухе приходит: </ta>
            <ta e="T227" id="Seg_4827" s="T224">— Ну вот, нас с собой не берут.</ta>
            <ta e="T234" id="Seg_4828" s="T227">Ты сшей мне из меха такой закрытый сокуй,</ta>
            <ta e="T241" id="Seg_4829" s="T234">чтоб и пурга не продувала, и мороз не пробирал.</ta>
            <ta e="T244" id="Seg_4830" s="T241">Старуха: — Сошью! — говорит.</ta>
            <ta e="T251" id="Seg_4831" s="T244">— Пусть белым как снег будет тот сокуй!</ta>
            <ta e="T260" id="Seg_4832" s="T251">Старуха, такая мастерица, сшила белее гуся белый сокуй.</ta>
            <ta e="T278" id="Seg_4833" s="T260">Полярная сова его надела — впору, сокуй к телу накрепко пристал: и в мороз не холодно, и в пургу не продувает, и снег на него не налипает.</ta>
            <ta e="T283" id="Seg_4834" s="T278">Полярная сова с тех пор в этом краю обжилась.</ta>
            <ta e="T293" id="Seg_4835" s="T283">В отместку за то, что оставили, полярная сова мелких птиц еще пуще стала есть.</ta>
            <ta e="T298" id="Seg_4836" s="T293">Особенно зимующую вместе с ней куропатку.</ta>
            <ta e="T299" id="Seg_4837" s="T298">Конец.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T113" id="Seg_4838" s="T108">[DCh]: "kihi buol-", literally 'become a human', means 'stay alive'.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
