<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDFD26FC8C-7992-DDBF-BA69-16DF60276310">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>BoND_1964_ThreeBrothers_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\BoND_1964_ThreeBrothers_flk\BoND_1964_ThreeBrothers_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1482</ud-information>
            <ud-information attribute-name="# HIAT:w">1029</ud-information>
            <ud-information attribute-name="# e">1029</ud-information>
            <ud-information attribute-name="# HIAT:u">223</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BoND">
            <abbreviation>BoND</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
         <tli id="T349" time="178.5" type="appl" />
         <tli id="T350" time="179.0" type="appl" />
         <tli id="T351" time="179.5" type="appl" />
         <tli id="T352" time="180.0" type="appl" />
         <tli id="T353" time="180.5" type="appl" />
         <tli id="T354" time="181.0" type="appl" />
         <tli id="T355" time="181.5" type="appl" />
         <tli id="T356" time="182.0" type="appl" />
         <tli id="T357" time="182.5" type="appl" />
         <tli id="T358" time="183.0" type="appl" />
         <tli id="T359" time="183.5" type="appl" />
         <tli id="T360" time="184.0" type="appl" />
         <tli id="T361" time="184.5" type="appl" />
         <tli id="T362" time="185.0" type="appl" />
         <tli id="T363" time="185.5" type="appl" />
         <tli id="T364" time="186.0" type="appl" />
         <tli id="T365" time="186.5" type="appl" />
         <tli id="T366" time="187.0" type="appl" />
         <tli id="T367" time="187.5" type="appl" />
         <tli id="T368" time="188.0" type="appl" />
         <tli id="T369" time="188.5" type="appl" />
         <tli id="T370" time="189.0" type="appl" />
         <tli id="T371" time="189.5" type="appl" />
         <tli id="T372" time="190.0" type="appl" />
         <tli id="T373" time="190.5" type="appl" />
         <tli id="T374" time="191.0" type="appl" />
         <tli id="T375" time="191.5" type="appl" />
         <tli id="T376" time="192.0" type="appl" />
         <tli id="T377" time="192.5" type="appl" />
         <tli id="T378" time="193.0" type="appl" />
         <tli id="T379" time="193.5" type="appl" />
         <tli id="T380" time="194.0" type="appl" />
         <tli id="T381" time="194.5" type="appl" />
         <tli id="T382" time="195.0" type="appl" />
         <tli id="T383" time="195.5" type="appl" />
         <tli id="T384" time="196.0" type="appl" />
         <tli id="T385" time="196.5" type="appl" />
         <tli id="T386" time="197.0" type="appl" />
         <tli id="T387" time="197.5" type="appl" />
         <tli id="T388" time="198.0" type="appl" />
         <tli id="T389" time="198.5" type="appl" />
         <tli id="T390" time="199.0" type="appl" />
         <tli id="T391" time="199.5" type="appl" />
         <tli id="T392" time="200.0" type="appl" />
         <tli id="T393" time="200.5" type="appl" />
         <tli id="T394" time="201.0" type="appl" />
         <tli id="T395" time="201.5" type="appl" />
         <tli id="T396" time="202.0" type="appl" />
         <tli id="T397" time="202.5" type="appl" />
         <tli id="T398" time="203.0" type="appl" />
         <tli id="T399" time="203.5" type="appl" />
         <tli id="T400" time="204.0" type="appl" />
         <tli id="T401" time="204.5" type="appl" />
         <tli id="T402" time="205.0" type="appl" />
         <tli id="T403" time="205.5" type="appl" />
         <tli id="T404" time="206.0" type="appl" />
         <tli id="T405" time="206.5" type="appl" />
         <tli id="T406" time="207.0" type="appl" />
         <tli id="T407" time="207.5" type="appl" />
         <tli id="T408" time="208.0" type="appl" />
         <tli id="T409" time="208.5" type="appl" />
         <tli id="T410" time="209.0" type="appl" />
         <tli id="T411" time="209.5" type="appl" />
         <tli id="T412" time="210.0" type="appl" />
         <tli id="T413" time="210.5" type="appl" />
         <tli id="T414" time="211.0" type="appl" />
         <tli id="T415" time="211.5" type="appl" />
         <tli id="T416" time="212.0" type="appl" />
         <tli id="T417" time="212.5" type="appl" />
         <tli id="T418" time="213.0" type="appl" />
         <tli id="T419" time="213.5" type="appl" />
         <tli id="T420" time="214.0" type="appl" />
         <tli id="T421" time="214.5" type="appl" />
         <tli id="T422" time="215.0" type="appl" />
         <tli id="T423" time="215.5" type="appl" />
         <tli id="T424" time="216.0" type="appl" />
         <tli id="T425" time="216.5" type="appl" />
         <tli id="T426" time="217.0" type="appl" />
         <tli id="T427" time="217.5" type="appl" />
         <tli id="T428" time="218.0" type="appl" />
         <tli id="T429" time="218.5" type="appl" />
         <tli id="T430" time="219.0" type="appl" />
         <tli id="T431" time="219.5" type="appl" />
         <tli id="T432" time="220.0" type="appl" />
         <tli id="T433" time="220.5" type="appl" />
         <tli id="T434" time="221.0" type="appl" />
         <tli id="T435" time="221.5" type="appl" />
         <tli id="T436" time="222.0" type="appl" />
         <tli id="T437" time="222.5" type="appl" />
         <tli id="T438" time="223.0" type="appl" />
         <tli id="T439" time="223.5" type="appl" />
         <tli id="T440" time="224.0" type="appl" />
         <tli id="T441" time="224.5" type="appl" />
         <tli id="T442" time="225.0" type="appl" />
         <tli id="T443" time="225.5" type="appl" />
         <tli id="T444" time="226.0" type="appl" />
         <tli id="T445" time="226.5" type="appl" />
         <tli id="T446" time="227.0" type="appl" />
         <tli id="T447" time="227.5" type="appl" />
         <tli id="T448" time="228.0" type="appl" />
         <tli id="T449" time="228.5" type="appl" />
         <tli id="T450" time="229.0" type="appl" />
         <tli id="T451" time="229.5" type="appl" />
         <tli id="T452" time="230.0" type="appl" />
         <tli id="T453" time="230.5" type="appl" />
         <tli id="T454" time="231.0" type="appl" />
         <tli id="T455" time="231.5" type="appl" />
         <tli id="T456" time="232.0" type="appl" />
         <tli id="T457" time="232.5" type="appl" />
         <tli id="T458" time="233.0" type="appl" />
         <tli id="T459" time="233.5" type="appl" />
         <tli id="T460" time="234.0" type="appl" />
         <tli id="T461" time="234.5" type="appl" />
         <tli id="T462" time="235.0" type="appl" />
         <tli id="T463" time="235.5" type="appl" />
         <tli id="T464" time="236.0" type="appl" />
         <tli id="T465" time="236.5" type="appl" />
         <tli id="T466" time="237.0" type="appl" />
         <tli id="T467" time="237.5" type="appl" />
         <tli id="T468" time="238.0" type="appl" />
         <tli id="T469" time="238.5" type="appl" />
         <tli id="T470" time="239.0" type="appl" />
         <tli id="T471" time="239.5" type="appl" />
         <tli id="T472" time="240.0" type="appl" />
         <tli id="T473" time="240.5" type="appl" />
         <tli id="T474" time="241.0" type="appl" />
         <tli id="T475" time="241.5" type="appl" />
         <tli id="T476" time="242.0" type="appl" />
         <tli id="T477" time="242.5" type="appl" />
         <tli id="T478" time="243.0" type="appl" />
         <tli id="T479" time="243.5" type="appl" />
         <tli id="T480" time="244.0" type="appl" />
         <tli id="T481" time="244.5" type="appl" />
         <tli id="T482" time="245.0" type="appl" />
         <tli id="T483" time="245.5" type="appl" />
         <tli id="T484" time="246.0" type="appl" />
         <tli id="T485" time="246.5" type="appl" />
         <tli id="T486" time="247.0" type="appl" />
         <tli id="T487" time="247.5" type="appl" />
         <tli id="T488" time="248.0" type="appl" />
         <tli id="T489" time="248.5" type="appl" />
         <tli id="T490" time="249.0" type="appl" />
         <tli id="T491" time="249.5" type="appl" />
         <tli id="T492" time="250.0" type="appl" />
         <tli id="T493" time="250.5" type="appl" />
         <tli id="T494" time="251.0" type="appl" />
         <tli id="T495" time="251.5" type="appl" />
         <tli id="T496" time="252.0" type="appl" />
         <tli id="T497" time="252.5" type="appl" />
         <tli id="T498" time="253.0" type="appl" />
         <tli id="T499" time="253.5" type="appl" />
         <tli id="T500" time="254.0" type="appl" />
         <tli id="T501" time="254.5" type="appl" />
         <tli id="T502" time="255.0" type="appl" />
         <tli id="T503" time="255.5" type="appl" />
         <tli id="T504" time="256.0" type="appl" />
         <tli id="T505" time="256.5" type="appl" />
         <tli id="T506" time="257.0" type="appl" />
         <tli id="T507" time="257.5" type="appl" />
         <tli id="T508" time="258.0" type="appl" />
         <tli id="T509" time="258.5" type="appl" />
         <tli id="T510" time="259.0" type="appl" />
         <tli id="T511" time="259.5" type="appl" />
         <tli id="T512" time="260.0" type="appl" />
         <tli id="T513" time="260.5" type="appl" />
         <tli id="T514" time="261.0" type="appl" />
         <tli id="T515" time="261.5" type="appl" />
         <tli id="T516" time="262.0" type="appl" />
         <tli id="T517" time="262.5" type="appl" />
         <tli id="T518" time="263.0" type="appl" />
         <tli id="T519" time="263.5" type="appl" />
         <tli id="T520" time="264.0" type="appl" />
         <tli id="T521" time="264.5" type="appl" />
         <tli id="T522" time="265.0" type="appl" />
         <tli id="T523" time="265.5" type="appl" />
         <tli id="T524" time="266.0" type="appl" />
         <tli id="T525" time="266.5" type="appl" />
         <tli id="T526" time="267.0" type="appl" />
         <tli id="T527" time="267.5" type="appl" />
         <tli id="T528" time="268.0" type="appl" />
         <tli id="T529" time="268.5" type="appl" />
         <tli id="T530" time="269.0" type="appl" />
         <tli id="T531" time="269.5" type="appl" />
         <tli id="T532" time="270.0" type="appl" />
         <tli id="T533" time="270.5" type="appl" />
         <tli id="T534" time="271.0" type="appl" />
         <tli id="T535" time="271.5" type="appl" />
         <tli id="T536" time="272.0" type="appl" />
         <tli id="T537" time="272.5" type="appl" />
         <tli id="T538" time="273.0" type="appl" />
         <tli id="T539" time="273.5" type="appl" />
         <tli id="T540" time="274.0" type="appl" />
         <tli id="T541" time="274.5" type="appl" />
         <tli id="T542" time="275.0" type="appl" />
         <tli id="T543" time="275.5" type="appl" />
         <tli id="T544" time="276.0" type="appl" />
         <tli id="T545" time="276.5" type="appl" />
         <tli id="T546" time="277.0" type="appl" />
         <tli id="T547" time="277.5" type="appl" />
         <tli id="T548" time="278.0" type="appl" />
         <tli id="T549" time="278.5" type="appl" />
         <tli id="T550" time="279.0" type="appl" />
         <tli id="T551" time="279.5" type="appl" />
         <tli id="T552" time="280.0" type="appl" />
         <tli id="T553" time="280.5" type="appl" />
         <tli id="T554" time="281.0" type="appl" />
         <tli id="T555" time="281.5" type="appl" />
         <tli id="T556" time="282.0" type="appl" />
         <tli id="T557" time="282.5" type="appl" />
         <tli id="T558" time="283.0" type="appl" />
         <tli id="T559" time="283.5" type="appl" />
         <tli id="T560" time="284.0" type="appl" />
         <tli id="T561" time="284.5" type="appl" />
         <tli id="T562" time="285.0" type="appl" />
         <tli id="T563" time="285.5" type="appl" />
         <tli id="T564" time="286.0" type="appl" />
         <tli id="T565" time="286.5" type="appl" />
         <tli id="T566" time="287.0" type="appl" />
         <tli id="T567" time="287.5" type="appl" />
         <tli id="T568" time="288.0" type="appl" />
         <tli id="T569" time="288.5" type="appl" />
         <tli id="T570" time="289.0" type="appl" />
         <tli id="T571" time="289.5" type="appl" />
         <tli id="T572" time="290.0" type="appl" />
         <tli id="T573" time="290.5" type="appl" />
         <tli id="T574" time="291.0" type="appl" />
         <tli id="T575" time="291.5" type="appl" />
         <tli id="T576" time="292.0" type="appl" />
         <tli id="T577" time="292.5" type="appl" />
         <tli id="T578" time="293.0" type="appl" />
         <tli id="T579" time="293.5" type="appl" />
         <tli id="T580" time="294.0" type="appl" />
         <tli id="T581" time="294.5" type="appl" />
         <tli id="T582" time="295.0" type="appl" />
         <tli id="T583" time="295.5" type="appl" />
         <tli id="T584" time="296.0" type="appl" />
         <tli id="T585" time="296.5" type="appl" />
         <tli id="T586" time="297.0" type="appl" />
         <tli id="T587" time="297.5" type="appl" />
         <tli id="T588" time="298.0" type="appl" />
         <tli id="T589" time="298.5" type="appl" />
         <tli id="T590" time="299.0" type="appl" />
         <tli id="T591" time="299.5" type="appl" />
         <tli id="T592" time="300.0" type="appl" />
         <tli id="T593" time="300.5" type="appl" />
         <tli id="T594" time="301.0" type="appl" />
         <tli id="T595" time="301.5" type="appl" />
         <tli id="T596" time="302.0" type="appl" />
         <tli id="T597" time="302.5" type="appl" />
         <tli id="T598" time="303.0" type="appl" />
         <tli id="T599" time="303.5" type="appl" />
         <tli id="T600" time="304.0" type="appl" />
         <tli id="T601" time="304.5" type="appl" />
         <tli id="T602" time="305.0" type="appl" />
         <tli id="T603" time="305.5" type="appl" />
         <tli id="T604" time="306.0" type="appl" />
         <tli id="T605" time="306.5" type="appl" />
         <tli id="T606" time="307.0" type="appl" />
         <tli id="T607" time="307.5" type="appl" />
         <tli id="T608" time="308.0" type="appl" />
         <tli id="T609" time="308.5" type="appl" />
         <tli id="T610" time="309.0" type="appl" />
         <tli id="T611" time="309.5" type="appl" />
         <tli id="T612" time="310.0" type="appl" />
         <tli id="T613" time="310.5" type="appl" />
         <tli id="T614" time="311.0" type="appl" />
         <tli id="T615" time="311.5" type="appl" />
         <tli id="T616" time="312.0" type="appl" />
         <tli id="T617" time="312.5" type="appl" />
         <tli id="T618" time="313.0" type="appl" />
         <tli id="T619" time="313.5" type="appl" />
         <tli id="T620" time="314.0" type="appl" />
         <tli id="T621" time="314.5" type="appl" />
         <tli id="T622" time="315.0" type="appl" />
         <tli id="T623" time="315.5" type="appl" />
         <tli id="T624" time="316.0" type="appl" />
         <tli id="T625" time="316.5" type="appl" />
         <tli id="T626" time="317.0" type="appl" />
         <tli id="T627" time="317.5" type="appl" />
         <tli id="T628" time="318.0" type="appl" />
         <tli id="T629" time="318.5" type="appl" />
         <tli id="T630" time="319.0" type="appl" />
         <tli id="T631" time="319.5" type="appl" />
         <tli id="T632" time="320.0" type="appl" />
         <tli id="T633" time="320.5" type="appl" />
         <tli id="T634" time="321.0" type="appl" />
         <tli id="T635" time="321.5" type="appl" />
         <tli id="T636" time="322.0" type="appl" />
         <tli id="T637" time="322.5" type="appl" />
         <tli id="T638" time="323.0" type="appl" />
         <tli id="T639" time="323.5" type="appl" />
         <tli id="T640" time="324.0" type="appl" />
         <tli id="T641" time="324.5" type="appl" />
         <tli id="T642" time="325.0" type="appl" />
         <tli id="T643" time="325.5" type="appl" />
         <tli id="T644" time="326.0" type="appl" />
         <tli id="T645" time="326.5" type="appl" />
         <tli id="T646" time="327.0" type="appl" />
         <tli id="T647" time="327.5" type="appl" />
         <tli id="T648" time="328.0" type="appl" />
         <tli id="T649" time="328.5" type="appl" />
         <tli id="T650" time="329.0" type="appl" />
         <tli id="T651" time="329.5" type="appl" />
         <tli id="T652" time="330.0" type="appl" />
         <tli id="T653" time="330.5" type="appl" />
         <tli id="T654" time="331.0" type="appl" />
         <tli id="T655" time="331.5" type="appl" />
         <tli id="T656" time="332.0" type="appl" />
         <tli id="T657" time="332.5" type="appl" />
         <tli id="T658" time="333.0" type="appl" />
         <tli id="T659" time="333.5" type="appl" />
         <tli id="T660" time="334.0" type="appl" />
         <tli id="T661" time="334.5" type="appl" />
         <tli id="T662" time="335.0" type="appl" />
         <tli id="T663" time="335.5" type="appl" />
         <tli id="T664" time="336.0" type="appl" />
         <tli id="T665" time="336.5" type="appl" />
         <tli id="T666" time="337.0" type="appl" />
         <tli id="T667" time="337.5" type="appl" />
         <tli id="T668" time="338.0" type="appl" />
         <tli id="T669" time="338.5" type="appl" />
         <tli id="T670" time="339.0" type="appl" />
         <tli id="T671" time="339.5" type="appl" />
         <tli id="T672" time="340.0" type="appl" />
         <tli id="T673" time="340.5" type="appl" />
         <tli id="T674" time="341.0" type="appl" />
         <tli id="T675" time="341.5" type="appl" />
         <tli id="T676" time="342.0" type="appl" />
         <tli id="T677" time="342.5" type="appl" />
         <tli id="T678" time="343.0" type="appl" />
         <tli id="T679" time="343.5" type="appl" />
         <tli id="T680" time="344.0" type="appl" />
         <tli id="T681" time="344.5" type="appl" />
         <tli id="T682" time="345.0" type="appl" />
         <tli id="T683" time="345.5" type="appl" />
         <tli id="T684" time="346.0" type="appl" />
         <tli id="T685" time="346.5" type="appl" />
         <tli id="T686" time="347.0" type="appl" />
         <tli id="T687" time="347.5" type="appl" />
         <tli id="T688" time="348.0" type="appl" />
         <tli id="T689" time="348.5" type="appl" />
         <tli id="T690" time="349.0" type="appl" />
         <tli id="T691" time="349.5" type="appl" />
         <tli id="T692" time="350.0" type="appl" />
         <tli id="T693" time="350.5" type="appl" />
         <tli id="T694" time="351.0" type="appl" />
         <tli id="T695" time="351.5" type="appl" />
         <tli id="T696" time="352.0" type="appl" />
         <tli id="T697" time="352.5" type="appl" />
         <tli id="T698" time="353.0" type="appl" />
         <tli id="T699" time="353.5" type="appl" />
         <tli id="T700" time="354.0" type="appl" />
         <tli id="T701" time="354.5" type="appl" />
         <tli id="T702" time="355.0" type="appl" />
         <tli id="T703" time="355.5" type="appl" />
         <tli id="T704" time="356.0" type="appl" />
         <tli id="T705" time="356.5" type="appl" />
         <tli id="T706" time="357.0" type="appl" />
         <tli id="T707" time="357.5" type="appl" />
         <tli id="T708" time="358.0" type="appl" />
         <tli id="T709" time="358.5" type="appl" />
         <tli id="T710" time="359.0" type="appl" />
         <tli id="T711" time="359.5" type="appl" />
         <tli id="T712" time="360.0" type="appl" />
         <tli id="T713" time="360.5" type="appl" />
         <tli id="T714" time="361.0" type="appl" />
         <tli id="T715" time="361.5" type="appl" />
         <tli id="T716" time="362.0" type="appl" />
         <tli id="T717" time="362.5" type="appl" />
         <tli id="T718" time="363.0" type="appl" />
         <tli id="T719" time="363.5" type="appl" />
         <tli id="T720" time="364.0" type="appl" />
         <tli id="T721" time="364.5" type="appl" />
         <tli id="T722" time="365.0" type="appl" />
         <tli id="T723" time="365.5" type="appl" />
         <tli id="T724" time="366.0" type="appl" />
         <tli id="T725" time="366.5" type="appl" />
         <tli id="T726" time="367.0" type="appl" />
         <tli id="T727" time="367.5" type="appl" />
         <tli id="T728" time="368.0" type="appl" />
         <tli id="T729" time="368.5" type="appl" />
         <tli id="T730" time="369.0" type="appl" />
         <tli id="T731" time="369.5" type="appl" />
         <tli id="T732" time="370.0" type="appl" />
         <tli id="T733" time="370.5" type="appl" />
         <tli id="T734" time="371.0" type="appl" />
         <tli id="T735" time="371.5" type="appl" />
         <tli id="T736" time="372.0" type="appl" />
         <tli id="T737" time="372.5" type="appl" />
         <tli id="T738" time="373.0" type="appl" />
         <tli id="T739" time="373.5" type="appl" />
         <tli id="T740" time="374.0" type="appl" />
         <tli id="T741" time="374.5" type="appl" />
         <tli id="T742" time="375.0" type="appl" />
         <tli id="T743" time="375.5" type="appl" />
         <tli id="T744" time="376.0" type="appl" />
         <tli id="T745" time="376.5" type="appl" />
         <tli id="T746" time="377.0" type="appl" />
         <tli id="T747" time="377.5" type="appl" />
         <tli id="T748" time="378.0" type="appl" />
         <tli id="T749" time="378.5" type="appl" />
         <tli id="T750" time="379.0" type="appl" />
         <tli id="T751" time="379.5" type="appl" />
         <tli id="T752" time="380.0" type="appl" />
         <tli id="T753" time="380.5" type="appl" />
         <tli id="T754" time="381.0" type="appl" />
         <tli id="T755" time="381.5" type="appl" />
         <tli id="T756" time="382.0" type="appl" />
         <tli id="T757" time="382.5" type="appl" />
         <tli id="T758" time="383.0" type="appl" />
         <tli id="T759" time="383.5" type="appl" />
         <tli id="T760" time="384.0" type="appl" />
         <tli id="T761" time="384.5" type="appl" />
         <tli id="T762" time="385.0" type="appl" />
         <tli id="T763" time="385.5" type="appl" />
         <tli id="T764" time="386.0" type="appl" />
         <tli id="T765" time="386.5" type="appl" />
         <tli id="T766" time="387.0" type="appl" />
         <tli id="T767" time="387.5" type="appl" />
         <tli id="T768" time="388.0" type="appl" />
         <tli id="T769" time="388.5" type="appl" />
         <tli id="T770" time="389.0" type="appl" />
         <tli id="T771" time="389.5" type="appl" />
         <tli id="T772" time="390.0" type="appl" />
         <tli id="T773" time="390.5" type="appl" />
         <tli id="T774" time="391.0" type="appl" />
         <tli id="T775" time="391.5" type="appl" />
         <tli id="T776" time="392.0" type="appl" />
         <tli id="T777" time="392.5" type="appl" />
         <tli id="T778" time="393.0" type="appl" />
         <tli id="T779" time="393.5" type="appl" />
         <tli id="T780" time="394.0" type="appl" />
         <tli id="T781" time="394.5" type="appl" />
         <tli id="T782" time="395.0" type="appl" />
         <tli id="T783" time="395.5" type="appl" />
         <tli id="T784" time="396.0" type="appl" />
         <tli id="T785" time="396.5" type="appl" />
         <tli id="T786" time="397.0" type="appl" />
         <tli id="T787" time="397.5" type="appl" />
         <tli id="T788" time="398.0" type="appl" />
         <tli id="T789" time="398.5" type="appl" />
         <tli id="T790" time="399.0" type="appl" />
         <tli id="T791" time="399.5" type="appl" />
         <tli id="T792" time="400.0" type="appl" />
         <tli id="T793" time="400.5" type="appl" />
         <tli id="T794" time="401.0" type="appl" />
         <tli id="T795" time="401.5" type="appl" />
         <tli id="T796" time="402.0" type="appl" />
         <tli id="T797" time="402.5" type="appl" />
         <tli id="T798" time="403.0" type="appl" />
         <tli id="T799" time="403.5" type="appl" />
         <tli id="T800" time="404.0" type="appl" />
         <tli id="T801" time="404.5" type="appl" />
         <tli id="T802" time="405.0" type="appl" />
         <tli id="T803" time="405.5" type="appl" />
         <tli id="T804" time="406.0" type="appl" />
         <tli id="T805" time="406.5" type="appl" />
         <tli id="T806" time="407.0" type="appl" />
         <tli id="T807" time="407.5" type="appl" />
         <tli id="T808" time="408.0" type="appl" />
         <tli id="T809" time="408.5" type="appl" />
         <tli id="T810" time="409.0" type="appl" />
         <tli id="T811" time="409.5" type="appl" />
         <tli id="T812" time="410.0" type="appl" />
         <tli id="T813" time="410.5" type="appl" />
         <tli id="T814" time="411.0" type="appl" />
         <tli id="T815" time="411.5" type="appl" />
         <tli id="T816" time="412.0" type="appl" />
         <tli id="T817" time="412.5" type="appl" />
         <tli id="T818" time="413.0" type="appl" />
         <tli id="T819" time="413.5" type="appl" />
         <tli id="T820" time="414.0" type="appl" />
         <tli id="T821" time="414.5" type="appl" />
         <tli id="T822" time="415.0" type="appl" />
         <tli id="T823" time="415.5" type="appl" />
         <tli id="T824" time="416.0" type="appl" />
         <tli id="T825" time="416.5" type="appl" />
         <tli id="T826" time="417.0" type="appl" />
         <tli id="T827" time="417.5" type="appl" />
         <tli id="T828" time="418.0" type="appl" />
         <tli id="T829" time="418.5" type="appl" />
         <tli id="T830" time="419.0" type="appl" />
         <tli id="T831" time="419.5" type="appl" />
         <tli id="T832" time="420.0" type="appl" />
         <tli id="T833" time="420.5" type="appl" />
         <tli id="T834" time="421.0" type="appl" />
         <tli id="T835" time="421.5" type="appl" />
         <tli id="T836" time="422.0" type="appl" />
         <tli id="T837" time="422.5" type="appl" />
         <tli id="T838" time="423.0" type="appl" />
         <tli id="T839" time="423.5" type="appl" />
         <tli id="T840" time="424.0" type="appl" />
         <tli id="T841" time="424.5" type="appl" />
         <tli id="T842" time="425.0" type="appl" />
         <tli id="T843" time="425.5" type="appl" />
         <tli id="T844" time="426.0" type="appl" />
         <tli id="T845" time="426.5" type="appl" />
         <tli id="T846" time="427.0" type="appl" />
         <tli id="T847" time="427.5" type="appl" />
         <tli id="T848" time="428.0" type="appl" />
         <tli id="T849" time="428.5" type="appl" />
         <tli id="T850" time="429.0" type="appl" />
         <tli id="T851" time="429.5" type="appl" />
         <tli id="T852" time="430.0" type="appl" />
         <tli id="T853" time="430.5" type="appl" />
         <tli id="T854" time="431.0" type="appl" />
         <tli id="T855" time="431.5" type="appl" />
         <tli id="T856" time="432.0" type="appl" />
         <tli id="T857" time="432.5" type="appl" />
         <tli id="T858" time="433.0" type="appl" />
         <tli id="T859" time="433.5" type="appl" />
         <tli id="T860" time="434.0" type="appl" />
         <tli id="T861" time="434.5" type="appl" />
         <tli id="T862" time="435.0" type="appl" />
         <tli id="T863" time="435.5" type="appl" />
         <tli id="T864" time="436.0" type="appl" />
         <tli id="T865" time="436.5" type="appl" />
         <tli id="T866" time="437.0" type="appl" />
         <tli id="T867" time="437.5" type="appl" />
         <tli id="T868" time="438.0" type="appl" />
         <tli id="T869" time="438.5" type="appl" />
         <tli id="T870" time="439.0" type="appl" />
         <tli id="T871" time="439.5" type="appl" />
         <tli id="T872" time="440.0" type="appl" />
         <tli id="T873" time="440.5" type="appl" />
         <tli id="T874" time="441.0" type="appl" />
         <tli id="T875" time="441.5" type="appl" />
         <tli id="T876" time="442.0" type="appl" />
         <tli id="T877" time="442.5" type="appl" />
         <tli id="T878" time="443.0" type="appl" />
         <tli id="T879" time="443.5" type="appl" />
         <tli id="T880" time="444.0" type="appl" />
         <tli id="T881" time="444.5" type="appl" />
         <tli id="T882" time="445.0" type="appl" />
         <tli id="T883" time="445.5" type="appl" />
         <tli id="T884" time="446.0" type="appl" />
         <tli id="T885" time="446.5" type="appl" />
         <tli id="T886" time="447.0" type="appl" />
         <tli id="T887" time="447.5" type="appl" />
         <tli id="T888" time="448.0" type="appl" />
         <tli id="T889" time="448.5" type="appl" />
         <tli id="T890" time="449.0" type="appl" />
         <tli id="T891" time="449.5" type="appl" />
         <tli id="T892" time="450.0" type="appl" />
         <tli id="T893" time="450.5" type="appl" />
         <tli id="T894" time="451.0" type="appl" />
         <tli id="T895" time="451.5" type="appl" />
         <tli id="T896" time="452.0" type="appl" />
         <tli id="T897" time="452.5" type="appl" />
         <tli id="T898" time="453.0" type="appl" />
         <tli id="T899" time="453.5" type="appl" />
         <tli id="T900" time="454.0" type="appl" />
         <tli id="T901" time="454.5" type="appl" />
         <tli id="T902" time="455.0" type="appl" />
         <tli id="T903" time="455.5" type="appl" />
         <tli id="T904" time="456.0" type="appl" />
         <tli id="T905" time="456.5" type="appl" />
         <tli id="T906" time="457.0" type="appl" />
         <tli id="T907" time="457.5" type="appl" />
         <tli id="T908" time="458.0" type="appl" />
         <tli id="T909" time="458.5" type="appl" />
         <tli id="T910" time="459.0" type="appl" />
         <tli id="T911" time="459.5" type="appl" />
         <tli id="T912" time="460.0" type="appl" />
         <tli id="T913" time="460.5" type="appl" />
         <tli id="T914" time="461.0" type="appl" />
         <tli id="T915" time="461.5" type="appl" />
         <tli id="T916" time="462.0" type="appl" />
         <tli id="T917" time="462.5" type="appl" />
         <tli id="T918" time="463.0" type="appl" />
         <tli id="T919" time="463.5" type="appl" />
         <tli id="T920" time="464.0" type="appl" />
         <tli id="T921" time="464.5" type="appl" />
         <tli id="T922" time="465.0" type="appl" />
         <tli id="T923" time="465.5" type="appl" />
         <tli id="T924" time="466.0" type="appl" />
         <tli id="T925" time="466.5" type="appl" />
         <tli id="T926" time="467.0" type="appl" />
         <tli id="T927" time="467.5" type="appl" />
         <tli id="T928" time="468.0" type="appl" />
         <tli id="T929" time="468.5" type="appl" />
         <tli id="T930" time="469.0" type="appl" />
         <tli id="T931" time="469.5" type="appl" />
         <tli id="T932" time="470.0" type="appl" />
         <tli id="T933" time="470.5" type="appl" />
         <tli id="T934" time="471.0" type="appl" />
         <tli id="T935" time="471.5" type="appl" />
         <tli id="T936" time="472.0" type="appl" />
         <tli id="T937" time="472.5" type="appl" />
         <tli id="T938" time="473.0" type="appl" />
         <tli id="T939" time="473.5" type="appl" />
         <tli id="T940" time="474.0" type="appl" />
         <tli id="T941" time="474.5" type="appl" />
         <tli id="T942" time="475.0" type="appl" />
         <tli id="T943" time="475.5" type="appl" />
         <tli id="T944" time="476.0" type="appl" />
         <tli id="T945" time="476.5" type="appl" />
         <tli id="T946" time="477.0" type="appl" />
         <tli id="T947" time="477.5" type="appl" />
         <tli id="T948" time="478.0" type="appl" />
         <tli id="T949" time="478.5" type="appl" />
         <tli id="T950" time="479.0" type="appl" />
         <tli id="T951" time="479.5" type="appl" />
         <tli id="T952" time="480.0" type="appl" />
         <tli id="T953" time="480.5" type="appl" />
         <tli id="T954" time="481.0" type="appl" />
         <tli id="T955" time="481.5" type="appl" />
         <tli id="T956" time="482.0" type="appl" />
         <tli id="T957" time="482.5" type="appl" />
         <tli id="T958" time="483.0" type="appl" />
         <tli id="T959" time="483.5" type="appl" />
         <tli id="T960" time="484.0" type="appl" />
         <tli id="T961" time="484.5" type="appl" />
         <tli id="T962" time="485.0" type="appl" />
         <tli id="T963" time="485.5" type="appl" />
         <tli id="T964" time="486.0" type="appl" />
         <tli id="T965" time="486.5" type="appl" />
         <tli id="T966" time="487.0" type="appl" />
         <tli id="T967" time="487.5" type="appl" />
         <tli id="T968" time="488.0" type="appl" />
         <tli id="T969" time="488.5" type="appl" />
         <tli id="T970" time="489.0" type="appl" />
         <tli id="T971" time="489.5" type="appl" />
         <tli id="T972" time="490.0" type="appl" />
         <tli id="T973" time="490.5" type="appl" />
         <tli id="T974" time="491.0" type="appl" />
         <tli id="T975" time="491.5" type="appl" />
         <tli id="T976" time="492.0" type="appl" />
         <tli id="T977" time="492.5" type="appl" />
         <tli id="T978" time="493.0" type="appl" />
         <tli id="T979" time="493.5" type="appl" />
         <tli id="T980" time="494.0" type="appl" />
         <tli id="T981" time="494.5" type="appl" />
         <tli id="T982" time="495.0" type="appl" />
         <tli id="T983" time="495.5" type="appl" />
         <tli id="T984" time="496.0" type="appl" />
         <tli id="T985" time="496.5" type="appl" />
         <tli id="T986" time="497.0" type="appl" />
         <tli id="T987" time="497.5" type="appl" />
         <tli id="T988" time="498.0" type="appl" />
         <tli id="T989" time="498.5" type="appl" />
         <tli id="T990" time="499.0" type="appl" />
         <tli id="T991" time="499.5" type="appl" />
         <tli id="T992" time="500.0" type="appl" />
         <tli id="T993" time="500.5" type="appl" />
         <tli id="T994" time="501.0" type="appl" />
         <tli id="T995" time="501.5" type="appl" />
         <tli id="T996" time="502.0" type="appl" />
         <tli id="T997" time="502.5" type="appl" />
         <tli id="T998" time="503.0" type="appl" />
         <tli id="T999" time="503.5" type="appl" />
         <tli id="T1000" time="504.0" type="appl" />
         <tli id="T1001" time="504.5" type="appl" />
         <tli id="T1002" time="505.0" type="appl" />
         <tli id="T1003" time="505.5" type="appl" />
         <tli id="T1004" time="506.0" type="appl" />
         <tli id="T1005" time="506.5" type="appl" />
         <tli id="T1006" time="507.0" type="appl" />
         <tli id="T1007" time="507.5" type="appl" />
         <tli id="T1008" time="508.0" type="appl" />
         <tli id="T1009" time="508.5" type="appl" />
         <tli id="T1010" time="509.0" type="appl" />
         <tli id="T1011" time="509.5" type="appl" />
         <tli id="T1012" time="510.0" type="appl" />
         <tli id="T1013" time="510.5" type="appl" />
         <tli id="T1014" time="511.0" type="appl" />
         <tli id="T1015" time="511.5" type="appl" />
         <tli id="T1016" time="512.0" type="appl" />
         <tli id="T1017" time="512.5" type="appl" />
         <tli id="T1018" time="513.0" type="appl" />
         <tli id="T1019" time="513.5" type="appl" />
         <tli id="T1020" time="514.0" type="appl" />
         <tli id="T1021" time="514.5" type="appl" />
         <tli id="T1022" time="515.0" type="appl" />
         <tli id="T1023" time="515.5" type="appl" />
         <tli id="T1024" time="516.0" type="appl" />
         <tli id="T1025" time="516.5" type="appl" />
         <tli id="T1026" time="517.0" type="appl" />
         <tli id="T1027" time="517.5" type="appl" />
         <tli id="T1028" time="518.0" type="appl" />
         <tli id="T1029" time="518.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BoND"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1029" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Üs</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">inibiːler</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">olorbuttar</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Biːrdere</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">kɨra</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">kuččukkaːn</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">kihi</ts>
                  <nts id="Seg_26" n="HIAT:ip">,</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">ortoloro</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">kaːmpat</ts>
                  <nts id="Seg_33" n="HIAT:ip">,</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">tobugunan</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">agaj</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">hɨːlar</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">Kuŋadʼaj</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">di͡en</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_52" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">Ubajdara</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">aːta</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">hu͡ok</ts>
                  <nts id="Seg_61" n="HIAT:ip">,</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">bulčut</ts>
                  <nts id="Seg_65" n="HIAT:ip">,</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">dʼaktardaːk</ts>
                  <nts id="Seg_69" n="HIAT:ip">,</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">biːr</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">hahɨl</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">ogoloːk</ts>
                  <nts id="Seg_79" n="HIAT:ip">.</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_82" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">Tabalara</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">hu͡oč-hogotok</ts>
                  <nts id="Seg_88" n="HIAT:ip">,</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">at</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">kördük</ts>
                  <nts id="Seg_95" n="HIAT:ip">.</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_98" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">Üjege</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">kölümmetter</ts>
                  <nts id="Seg_104" n="HIAT:ip">,</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">köstöktörüne</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">ere</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">dʼi͡elerin</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">tartarallar</ts>
                  <nts id="Seg_117" n="HIAT:ip">.</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_120" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_122" n="HIAT:w" s="T32">Ubajdara</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_125" n="HIAT:w" s="T33">aːttaːk</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_128" n="HIAT:w" s="T34">bulčut</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_132" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_134" n="HIAT:w" s="T35">Kɨːlɨ</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_137" n="HIAT:w" s="T36">bultuːr</ts>
                  <nts id="Seg_138" n="HIAT:ip">,</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_141" n="HIAT:w" s="T37">balɨgɨ</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_144" n="HIAT:w" s="T38">bu͡ollun</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_148" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_150" n="HIAT:w" s="T39">Dʼɨl</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_153" n="HIAT:w" s="T40">haːstɨjarɨn</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_156" n="HIAT:w" s="T41">hagana</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_159" n="HIAT:w" s="T42">ubajdara</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_162" n="HIAT:w" s="T43">ɨ͡aldʼar</ts>
                  <nts id="Seg_163" n="HIAT:ip">,</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_166" n="HIAT:w" s="T44">bergeːbit</ts>
                  <nts id="Seg_167" n="HIAT:ip">,</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_170" n="HIAT:w" s="T45">ühüs</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_173" n="HIAT:w" s="T46">künüger</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_176" n="HIAT:w" s="T47">bu͡opsa</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_179" n="HIAT:w" s="T48">möltöːbüt</ts>
                  <nts id="Seg_180" n="HIAT:ip">,</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_183" n="HIAT:w" s="T49">keri͡es</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_186" n="HIAT:w" s="T50">eppit</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_189" n="HIAT:w" s="T51">dʼaktarɨgar</ts>
                  <nts id="Seg_190" n="HIAT:ip">:</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_193" n="HIAT:u" s="T52">
                  <nts id="Seg_194" n="HIAT:ip">"</nts>
                  <ts e="T53" id="Seg_196" n="HIAT:w" s="T52">Dʼe</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_199" n="HIAT:w" s="T53">min</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_202" n="HIAT:w" s="T54">öllüm</ts>
                  <nts id="Seg_203" n="HIAT:ip">.</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_206" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_208" n="HIAT:w" s="T55">Kimiŋ</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">karajɨ͡agaj</ts>
                  <nts id="Seg_212" n="HIAT:ip">?</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_215" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_217" n="HIAT:w" s="T57">Bultaːbɨt</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_220" n="HIAT:w" s="T58">bulpun</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_223" n="HIAT:w" s="T59">hajɨnɨ</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_226" n="HIAT:w" s="T60">bɨha</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_229" n="HIAT:w" s="T61">ahɨ͡akkɨt</ts>
                  <nts id="Seg_230" n="HIAT:ip">.</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_233" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_235" n="HIAT:w" s="T62">Askɨn</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_238" n="HIAT:w" s="T63">baraːtakkɨna</ts>
                  <nts id="Seg_239" n="HIAT:ip">,</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_242" n="HIAT:w" s="T64">ehiːl</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_245" n="HIAT:w" s="T65">tiriːtin</ts>
                  <nts id="Seg_246" n="HIAT:ip">,</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_249" n="HIAT:w" s="T66">tɨhɨn</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_252" n="HIAT:w" s="T67">harɨːlaːn</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_255" n="HIAT:w" s="T68">minneːn</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_258" n="HIAT:w" s="T69">iheːr</ts>
                  <nts id="Seg_259" n="HIAT:ip">.</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_262" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_264" n="HIAT:w" s="T70">Iti</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_267" n="HIAT:w" s="T71">dʼonuŋ</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_270" n="HIAT:w" s="T72">tuhata</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_273" n="HIAT:w" s="T73">hu͡ok</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_276" n="HIAT:w" s="T74">dʼon</ts>
                  <nts id="Seg_277" n="HIAT:ip">.</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_280" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_282" n="HIAT:w" s="T75">Ölbükkünen</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_285" n="HIAT:w" s="T76">ölögün</ts>
                  <nts id="Seg_286" n="HIAT:ip">.</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_289" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_291" n="HIAT:w" s="T77">Ogom</ts>
                  <nts id="Seg_292" n="HIAT:ip">,</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_295" n="HIAT:w" s="T78">bagar</ts>
                  <nts id="Seg_296" n="HIAT:ip">,</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_299" n="HIAT:w" s="T79">kihi</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_302" n="HIAT:w" s="T80">bu͡olu͡o</ts>
                  <nts id="Seg_303" n="HIAT:ip">,</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_306" n="HIAT:w" s="T81">ölbötögüne</ts>
                  <nts id="Seg_307" n="HIAT:ip">,</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_310" n="HIAT:w" s="T82">kini</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_313" n="HIAT:w" s="T83">iːti͡e</ts>
                  <nts id="Seg_314" n="HIAT:ip">.</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_317" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_319" n="HIAT:w" s="T84">Elete</ts>
                  <nts id="Seg_320" n="HIAT:ip">.</nts>
                  <nts id="Seg_321" n="HIAT:ip">"</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_324" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_326" n="HIAT:w" s="T85">Ölön</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_329" n="HIAT:w" s="T86">kaːlar</ts>
                  <nts id="Seg_330" n="HIAT:ip">.</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_333" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_335" n="HIAT:w" s="T87">ɨtana-ɨtana</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_338" n="HIAT:w" s="T88">dʼaktar</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_341" n="HIAT:w" s="T89">uːran</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_344" n="HIAT:w" s="T90">keːher</ts>
                  <nts id="Seg_345" n="HIAT:ip">.</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_348" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_350" n="HIAT:w" s="T91">Bu</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_353" n="HIAT:w" s="T92">hajɨna</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_356" n="HIAT:w" s="T93">bu͡olar</ts>
                  <nts id="Seg_357" n="HIAT:ip">.</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_360" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_362" n="HIAT:w" s="T94">Astarɨn</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_365" n="HIAT:w" s="T95">katarɨnallar</ts>
                  <nts id="Seg_366" n="HIAT:ip">.</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_369" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_371" n="HIAT:w" s="T96">Hajɨnnarɨn</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_374" n="HIAT:w" s="T97">bɨhallar</ts>
                  <nts id="Seg_375" n="HIAT:ip">,</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_378" n="HIAT:w" s="T98">kühünneriger</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_381" n="HIAT:w" s="T99">tɨstarɨn</ts>
                  <nts id="Seg_382" n="HIAT:ip">,</nts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_385" n="HIAT:w" s="T100">tiriːlerin</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_388" n="HIAT:w" s="T101">hiːller</ts>
                  <nts id="Seg_389" n="HIAT:ip">.</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_392" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_394" n="HIAT:w" s="T102">Barɨta</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_397" n="HIAT:w" s="T103">baranar</ts>
                  <nts id="Seg_398" n="HIAT:ip">.</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_401" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_403" n="HIAT:w" s="T104">Kɨhɨn</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_406" n="HIAT:w" s="T105">bu͡olar</ts>
                  <nts id="Seg_407" n="HIAT:ip">.</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_410" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_412" n="HIAT:w" s="T106">Hi͡ek</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_415" n="HIAT:w" s="T107">hirdere</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_418" n="HIAT:w" s="T108">hu͡ok</ts>
                  <nts id="Seg_419" n="HIAT:ip">.</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_422" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_424" n="HIAT:w" s="T109">Tɨ͡ala</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_427" n="HIAT:w" s="T110">hu͡ok</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_430" n="HIAT:w" s="T111">kün</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_433" n="HIAT:w" s="T112">araj</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_436" n="HIAT:w" s="T113">hɨ͡argalaːk</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_439" n="HIAT:w" s="T114">tɨ͡ahɨn</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_442" n="HIAT:w" s="T115">ister</ts>
                  <nts id="Seg_443" n="HIAT:ip">.</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_446" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_448" n="HIAT:w" s="T116">Dʼaktar</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_451" n="HIAT:w" s="T117">taksa</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_454" n="HIAT:w" s="T118">kötör</ts>
                  <nts id="Seg_455" n="HIAT:ip">,</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_458" n="HIAT:w" s="T119">haŋa</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_461" n="HIAT:w" s="T120">ere</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_464" n="HIAT:w" s="T121">ihiller</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_467" n="HIAT:w" s="T122">hiriger</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_470" n="HIAT:w" s="T123">toktoːbut</ts>
                  <nts id="Seg_471" n="HIAT:ip">.</nts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_474" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_476" n="HIAT:w" s="T124">Araj</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_479" n="HIAT:w" s="T125">dʼaktar</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_482" n="HIAT:w" s="T126">agata</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_484" n="HIAT:ip">—</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_487" n="HIAT:w" s="T127">Agɨs</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_490" n="HIAT:w" s="T128">konnomu͡oj</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_493" n="HIAT:w" s="T129">iččite</ts>
                  <nts id="Seg_494" n="HIAT:ip">,</nts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_497" n="HIAT:w" s="T130">küseːjine</ts>
                  <nts id="Seg_498" n="HIAT:ip">,</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_501" n="HIAT:w" s="T131">kini͡es</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_504" n="HIAT:w" s="T132">ebit</ts>
                  <nts id="Seg_505" n="HIAT:ip">.</nts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_508" n="HIAT:u" s="T133">
                  <nts id="Seg_509" n="HIAT:ip">"</nts>
                  <ts e="T134" id="Seg_511" n="HIAT:w" s="T133">Kajɨː</ts>
                  <nts id="Seg_512" n="HIAT:ip">,</nts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_515" n="HIAT:w" s="T134">kajdak</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_518" n="HIAT:w" s="T135">olorogut</ts>
                  <nts id="Seg_519" n="HIAT:ip">?</nts>
                  <nts id="Seg_520" n="HIAT:ip">"</nts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_523" n="HIAT:u" s="T136">
                  <nts id="Seg_524" n="HIAT:ip">"</nts>
                  <ts e="T137" id="Seg_526" n="HIAT:w" s="T136">Erim</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_529" n="HIAT:w" s="T137">ölbüte</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_532" n="HIAT:w" s="T138">bɨlɨrɨːn</ts>
                  <nts id="Seg_533" n="HIAT:ip">.</nts>
                  <nts id="Seg_534" n="HIAT:ip">"</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_537" n="HIAT:u" s="T139">
                  <nts id="Seg_538" n="HIAT:ip">"</nts>
                  <ts e="T140" id="Seg_540" n="HIAT:w" s="T139">Kajɨː</ts>
                  <nts id="Seg_541" n="HIAT:ip">,</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_544" n="HIAT:w" s="T140">slaːbu͡ok</ts>
                  <nts id="Seg_545" n="HIAT:ip">!</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_548" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_550" n="HIAT:w" s="T141">Olus</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_553" n="HIAT:w" s="T142">ü͡ördüm</ts>
                  <nts id="Seg_554" n="HIAT:ip">!</nts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_557" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_559" n="HIAT:w" s="T143">Taŋɨn</ts>
                  <nts id="Seg_560" n="HIAT:ip">,</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_563" n="HIAT:w" s="T144">barɨ͡ak</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_566" n="HIAT:w" s="T145">min</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_569" n="HIAT:w" s="T146">dʼi͡eber</ts>
                  <nts id="Seg_570" n="HIAT:ip">!</nts>
                  <nts id="Seg_571" n="HIAT:ip">"</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_574" n="HIAT:u" s="T147">
                  <nts id="Seg_575" n="HIAT:ip">"</nts>
                  <ts e="T148" id="Seg_577" n="HIAT:w" s="T147">Kaja</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_580" n="HIAT:w" s="T148">ogobun</ts>
                  <nts id="Seg_581" n="HIAT:ip">,</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_584" n="HIAT:w" s="T149">dʼommun</ts>
                  <nts id="Seg_585" n="HIAT:ip">?</nts>
                  <nts id="Seg_586" n="HIAT:ip">"</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_589" n="HIAT:u" s="T150">
                  <nts id="Seg_590" n="HIAT:ip">"</nts>
                  <ts e="T151" id="Seg_592" n="HIAT:w" s="T150">Okton</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_595" n="HIAT:w" s="T151">öllünner</ts>
                  <nts id="Seg_596" n="HIAT:ip">,</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_599" n="HIAT:w" s="T152">barɨ͡ak</ts>
                  <nts id="Seg_600" n="HIAT:ip">!</nts>
                  <nts id="Seg_601" n="HIAT:ip">"</nts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_604" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_606" n="HIAT:w" s="T153">Dʼaktar</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_609" n="HIAT:w" s="T154">taŋnan</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_612" n="HIAT:w" s="T155">tijen</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_615" n="HIAT:w" s="T156">meŋester</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_618" n="HIAT:w" s="T157">da</ts>
                  <nts id="Seg_619" n="HIAT:ip">,</nts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_622" n="HIAT:w" s="T158">kötüten</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_625" n="HIAT:w" s="T159">kaːlallar</ts>
                  <nts id="Seg_626" n="HIAT:ip">.</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_629" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_631" n="HIAT:w" s="T160">Kuŋadʼajdaːk</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_634" n="HIAT:w" s="T161">kam</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_637" n="HIAT:w" s="T162">aččɨk</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_640" n="HIAT:w" s="T163">hɨttaktara</ts>
                  <nts id="Seg_641" n="HIAT:ip">.</nts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_644" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_646" n="HIAT:w" s="T164">Karaŋa</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_649" n="HIAT:w" s="T165">aːhan</ts>
                  <nts id="Seg_650" n="HIAT:ip">,</nts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_653" n="HIAT:w" s="T166">hɨrdɨːrga</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_656" n="HIAT:w" s="T167">barar</ts>
                  <nts id="Seg_657" n="HIAT:ip">.</nts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_660" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_662" n="HIAT:w" s="T168">Kuŋadʼaj</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_665" n="HIAT:w" s="T169">baltɨtɨgar</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_668" n="HIAT:w" s="T170">eter</ts>
                  <nts id="Seg_669" n="HIAT:ip">:</nts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_672" n="HIAT:u" s="T171">
                  <nts id="Seg_673" n="HIAT:ip">"</nts>
                  <ts e="T172" id="Seg_675" n="HIAT:w" s="T171">Tabagɨn</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_678" n="HIAT:w" s="T172">kölüj</ts>
                  <nts id="Seg_679" n="HIAT:ip">.</nts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_682" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_684" n="HIAT:w" s="T173">Hɨppɨtɨnan</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_687" n="HIAT:w" s="T174">ölü͡ökpüt</ts>
                  <nts id="Seg_688" n="HIAT:ip">.</nts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_691" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_693" n="HIAT:w" s="T175">Agɨs</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_696" n="HIAT:w" s="T176">koŋnomu͡oju</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_699" n="HIAT:w" s="T177">batɨ͡ak</ts>
                  <nts id="Seg_700" n="HIAT:ip">!</nts>
                  <nts id="Seg_701" n="HIAT:ip">"</nts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T185" id="Seg_704" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_706" n="HIAT:w" s="T178">Tabalarɨn</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_709" n="HIAT:w" s="T179">kölüjen</ts>
                  <nts id="Seg_710" n="HIAT:ip">,</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_713" n="HIAT:w" s="T180">dʼi͡elerin</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_716" n="HIAT:w" s="T181">kötüren</ts>
                  <nts id="Seg_717" n="HIAT:ip">,</nts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_720" n="HIAT:w" s="T182">meŋesten</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_723" n="HIAT:w" s="T183">etteten</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_726" n="HIAT:w" s="T184">istektere</ts>
                  <nts id="Seg_727" n="HIAT:ip">.</nts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_730" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_732" n="HIAT:w" s="T185">Kah</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_735" n="HIAT:w" s="T186">u͡onna</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_738" n="HIAT:w" s="T187">köspütter</ts>
                  <nts id="Seg_739" n="HIAT:ip">.</nts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_742" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_744" n="HIAT:w" s="T188">Biːrde</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_747" n="HIAT:w" s="T189">kütürü͡ök</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_750" n="HIAT:w" s="T190">hiske</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_753" n="HIAT:w" s="T191">ɨttɨbɨttar</ts>
                  <nts id="Seg_754" n="HIAT:ip">.</nts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_757" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_759" n="HIAT:w" s="T192">Körüleːbittere</ts>
                  <nts id="Seg_760" n="HIAT:ip">,</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_763" n="HIAT:w" s="T193">kütürdeːk</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_766" n="HIAT:w" s="T194">teːn</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_769" n="HIAT:w" s="T195">teːn</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_772" n="HIAT:w" s="T196">ortotugar</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_775" n="HIAT:w" s="T197">ü͡ör</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_778" n="HIAT:w" s="T198">bögö</ts>
                  <nts id="Seg_779" n="HIAT:ip">,</nts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_782" n="HIAT:w" s="T199">teːn</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_785" n="HIAT:w" s="T200">uŋu͡or</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_788" n="HIAT:w" s="T201">hette</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_791" n="HIAT:w" s="T202">u͡on</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_794" n="HIAT:w" s="T203">dʼi͡e</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_797" n="HIAT:w" s="T204">kihi</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_800" n="HIAT:w" s="T205">köstör</ts>
                  <nts id="Seg_801" n="HIAT:ip">.</nts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_804" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_806" n="HIAT:w" s="T206">Barbɨttar</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_809" n="HIAT:w" s="T207">ü͡ör</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_812" n="HIAT:w" s="T208">ihinen</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_815" n="HIAT:w" s="T209">ü͡ör</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_818" n="HIAT:w" s="T210">anaraː</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_821" n="HIAT:w" s="T211">öttütüger</ts>
                  <nts id="Seg_822" n="HIAT:ip">.</nts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_825" n="HIAT:u" s="T212">
                  <nts id="Seg_826" n="HIAT:ip">"</nts>
                  <ts e="T213" id="Seg_828" n="HIAT:w" s="T212">Tur</ts>
                  <nts id="Seg_829" n="HIAT:ip">"</nts>
                  <nts id="Seg_830" n="HIAT:ip">,</nts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_833" n="HIAT:w" s="T213">di͡ebit</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_836" n="HIAT:w" s="T214">Kuŋadʼaj</ts>
                  <nts id="Seg_837" n="HIAT:ip">.</nts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_840" n="HIAT:u" s="T215">
                  <nts id="Seg_841" n="HIAT:ip">"</nts>
                  <ts e="T216" id="Seg_843" n="HIAT:w" s="T215">Iti</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_846" n="HIAT:w" s="T216">dʼi͡elerten</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_849" n="HIAT:w" s="T217">biːr</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_852" n="HIAT:w" s="T218">orduktarɨgar</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_855" n="HIAT:w" s="T219">toktoːr</ts>
                  <nts id="Seg_856" n="HIAT:ip">,</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_859" n="HIAT:w" s="T220">niptelerin</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_862" n="HIAT:w" s="T221">betereː</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_865" n="HIAT:w" s="T222">öttükeːniger</ts>
                  <nts id="Seg_866" n="HIAT:ip">.</nts>
                  <nts id="Seg_867" n="HIAT:ip">"</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_870" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_872" n="HIAT:w" s="T223">Körbüte</ts>
                  <nts id="Seg_873" n="HIAT:ip">,</nts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_876" n="HIAT:w" s="T224">biːr</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_879" n="HIAT:w" s="T225">dʼi͡e</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_882" n="HIAT:w" s="T226">barɨlarɨttan</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_885" n="HIAT:w" s="T227">ulakan</ts>
                  <nts id="Seg_886" n="HIAT:ip">.</nts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_889" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_891" n="HIAT:w" s="T228">Aːn</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_894" n="HIAT:w" s="T229">tuhugar</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_897" n="HIAT:w" s="T230">tijbitter</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_900" n="HIAT:w" s="T231">da</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_903" n="HIAT:w" s="T232">tabalarɨn</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_906" n="HIAT:w" s="T233">ɨːtan</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_909" n="HIAT:w" s="T234">keːheller</ts>
                  <nts id="Seg_910" n="HIAT:ip">.</nts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_913" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_915" n="HIAT:w" s="T235">Dʼi͡e</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_918" n="HIAT:w" s="T236">tuttan</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_921" n="HIAT:w" s="T237">keːspitter</ts>
                  <nts id="Seg_922" n="HIAT:ip">,</nts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_925" n="HIAT:w" s="T238">ottubuttar</ts>
                  <nts id="Seg_926" n="HIAT:ip">.</nts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_929" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_931" n="HIAT:w" s="T239">Baltɨtɨn</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_933" n="HIAT:ip">"</nts>
                  <ts e="T241" id="Seg_935" n="HIAT:w" s="T240">bar</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_938" n="HIAT:w" s="T241">haŋaskar</ts>
                  <nts id="Seg_939" n="HIAT:ip">.</nts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_942" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_944" n="HIAT:w" s="T242">Ogotun</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_947" n="HIAT:w" s="T243">emnere</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_950" n="HIAT:w" s="T244">kellin</ts>
                  <nts id="Seg_951" n="HIAT:ip">.</nts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T258" id="Seg_954" n="HIAT:u" s="T245">
                  <ts e="T246" id="Seg_956" n="HIAT:w" s="T245">Agɨs</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_959" n="HIAT:w" s="T246">Koŋnomu͡ojga</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_962" n="HIAT:w" s="T247">et</ts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_965" n="HIAT:w" s="T248">'öllübüt</ts>
                  <nts id="Seg_966" n="HIAT:ip">,</nts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_969" n="HIAT:w" s="T249">biːr</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_972" n="HIAT:w" s="T250">eme</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_975" n="HIAT:w" s="T251">hürek</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_978" n="HIAT:w" s="T252">aŋaːrkaːnɨn</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_981" n="HIAT:w" s="T253">kɨtta</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_984" n="HIAT:w" s="T254">bɨ͡ar</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_987" n="HIAT:w" s="T255">tulaːjagɨn</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_990" n="HIAT:w" s="T256">beristin'</ts>
                  <nts id="Seg_991" n="HIAT:ip">"</nts>
                  <nts id="Seg_992" n="HIAT:ip">,</nts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_995" n="HIAT:w" s="T257">diːr</ts>
                  <nts id="Seg_996" n="HIAT:ip">.</nts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_999" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_1001" n="HIAT:w" s="T258">U͡ol</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1004" n="HIAT:w" s="T259">hüːren</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1007" n="HIAT:w" s="T260">kiːren</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1010" n="HIAT:w" s="T261">eter</ts>
                  <nts id="Seg_1011" n="HIAT:ip">.</nts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_1014" n="HIAT:u" s="T262">
                  <nts id="Seg_1015" n="HIAT:ip">"</nts>
                  <ts e="T263" id="Seg_1017" n="HIAT:w" s="T262">Kör</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1020" n="HIAT:w" s="T263">ere</ts>
                  <nts id="Seg_1021" n="HIAT:ip">,</nts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1024" n="HIAT:w" s="T264">emterer</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1027" n="HIAT:w" s="T265">hokuču͡ok</ts>
                  <nts id="Seg_1028" n="HIAT:ip">!</nts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_1031" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_1033" n="HIAT:w" s="T266">Okton</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1036" n="HIAT:w" s="T267">öllün</ts>
                  <nts id="Seg_1037" n="HIAT:ip">"</nts>
                  <nts id="Seg_1038" n="HIAT:ip">,</nts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1041" n="HIAT:w" s="T268">diːr</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1044" n="HIAT:w" s="T269">dʼaktar</ts>
                  <nts id="Seg_1045" n="HIAT:ip">.</nts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_1048" n="HIAT:u" s="T270">
                  <nts id="Seg_1049" n="HIAT:ip">"</nts>
                  <ts e="T271" id="Seg_1051" n="HIAT:w" s="T270">Kaja</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1054" n="HIAT:w" s="T271">eheː</ts>
                  <nts id="Seg_1055" n="HIAT:ip">,</nts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1058" n="HIAT:w" s="T272">ildʼitteːte</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1061" n="HIAT:w" s="T273">ubajɨm</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1064" n="HIAT:w" s="T274">'öllübüt</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1067" n="HIAT:w" s="T275">biːr</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1070" n="HIAT:w" s="T276">eme</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1073" n="HIAT:w" s="T277">hürek</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1076" n="HIAT:w" s="T278">aŋaːrkaːnɨn</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1079" n="HIAT:w" s="T279">kɨtta</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1082" n="HIAT:w" s="T280">bɨ͡ar</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1085" n="HIAT:w" s="T281">tulaːjagɨn</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1088" n="HIAT:w" s="T282">beristin'</ts>
                  <nts id="Seg_1089" n="HIAT:ip">.</nts>
                  <nts id="Seg_1090" n="HIAT:ip">"</nts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1093" n="HIAT:u" s="T283">
                  <nts id="Seg_1094" n="HIAT:ip">"</nts>
                  <ts e="T284" id="Seg_1096" n="HIAT:w" s="T283">Kaːk</ts>
                  <nts id="Seg_1097" n="HIAT:ip">,</nts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1100" n="HIAT:w" s="T284">okton</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1103" n="HIAT:w" s="T285">ölüŋ</ts>
                  <nts id="Seg_1104" n="HIAT:ip">.</nts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T291" id="Seg_1107" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_1109" n="HIAT:w" s="T286">ɨːppappɨn</ts>
                  <nts id="Seg_1110" n="HIAT:ip">"</nts>
                  <nts id="Seg_1111" n="HIAT:ip">,</nts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1114" n="HIAT:w" s="T287">diːr</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1117" n="HIAT:w" s="T288">Agɨs</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1120" n="HIAT:w" s="T289">koŋnomu͡oj</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1123" n="HIAT:w" s="T290">iččite</ts>
                  <nts id="Seg_1124" n="HIAT:ip">.</nts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_1127" n="HIAT:u" s="T291">
                  <ts e="T292" id="Seg_1129" n="HIAT:w" s="T291">Utujaːktɨːllar</ts>
                  <nts id="Seg_1130" n="HIAT:ip">.</nts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_1133" n="HIAT:u" s="T292">
                  <ts e="T293" id="Seg_1135" n="HIAT:w" s="T292">Tugu</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1138" n="HIAT:w" s="T293">hi͡ekterej</ts>
                  <nts id="Seg_1139" n="HIAT:ip">?</nts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T298" id="Seg_1142" n="HIAT:u" s="T294">
                  <ts e="T295" id="Seg_1144" n="HIAT:w" s="T294">Araj</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1147" n="HIAT:w" s="T295">tabalɨːr</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1150" n="HIAT:w" s="T296">haŋa</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1153" n="HIAT:w" s="T297">ihiller</ts>
                  <nts id="Seg_1154" n="HIAT:ip">.</nts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1157" n="HIAT:u" s="T298">
                  <ts e="T299" id="Seg_1159" n="HIAT:w" s="T298">Kuŋadʼaj</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1162" n="HIAT:w" s="T299">taksan</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1165" n="HIAT:w" s="T300">iːktiː</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1168" n="HIAT:w" s="T301">turbut</ts>
                  <nts id="Seg_1169" n="HIAT:ip">,</nts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1172" n="HIAT:w" s="T302">tobugar</ts>
                  <nts id="Seg_1173" n="HIAT:ip">.</nts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T306" id="Seg_1176" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1178" n="HIAT:w" s="T303">Taba</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1181" n="HIAT:w" s="T304">bögö</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1184" n="HIAT:w" s="T305">keler</ts>
                  <nts id="Seg_1185" n="HIAT:ip">.</nts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T311" id="Seg_1188" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_1190" n="HIAT:w" s="T306">Aktamiː</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1193" n="HIAT:w" s="T307">buːr</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1196" n="HIAT:w" s="T308">emis</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1199" n="HIAT:w" s="T309">bagajɨ</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1202" n="HIAT:w" s="T310">keler</ts>
                  <nts id="Seg_1203" n="HIAT:ip">.</nts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_1206" n="HIAT:u" s="T311">
                  <ts e="T312" id="Seg_1208" n="HIAT:w" s="T311">U͡ol</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1211" n="HIAT:w" s="T312">kaban</ts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1214" n="HIAT:w" s="T313">ɨlar</ts>
                  <nts id="Seg_1215" n="HIAT:ip">.</nts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_1218" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1220" n="HIAT:w" s="T314">Mögön</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1223" n="HIAT:w" s="T315">bögö</ts>
                  <nts id="Seg_1224" n="HIAT:ip">.</nts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T318" id="Seg_1227" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_1229" n="HIAT:w" s="T316">Bahagɨn</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1232" n="HIAT:w" s="T317">ɨllarar</ts>
                  <nts id="Seg_1233" n="HIAT:ip">.</nts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1236" n="HIAT:u" s="T318">
                  <ts e="T319" id="Seg_1238" n="HIAT:w" s="T318">Kabargatɨn</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1241" n="HIAT:w" s="T319">bɨha</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1244" n="HIAT:w" s="T320">hotor</ts>
                  <nts id="Seg_1245" n="HIAT:ip">.</nts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T331" id="Seg_1248" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_1250" n="HIAT:w" s="T321">Hülbekke</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1253" n="HIAT:w" s="T322">da</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1256" n="HIAT:w" s="T323">killereller</ts>
                  <nts id="Seg_1257" n="HIAT:ip">,</nts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1260" n="HIAT:w" s="T324">kü͡östenen</ts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1263" n="HIAT:w" s="T325">dʼe</ts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1266" n="HIAT:w" s="T326">ahɨːllar</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1269" n="HIAT:w" s="T327">künü</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1272" n="HIAT:w" s="T328">meldʼi</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1275" n="HIAT:w" s="T329">ki͡ehege</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1278" n="HIAT:w" s="T330">di͡eri</ts>
                  <nts id="Seg_1279" n="HIAT:ip">.</nts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T337" id="Seg_1282" n="HIAT:u" s="T331">
                  <ts e="T332" id="Seg_1284" n="HIAT:w" s="T331">Bu</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1287" n="HIAT:w" s="T332">dʼi͡e</ts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1290" n="HIAT:w" s="T333">tahɨgar</ts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1293" n="HIAT:w" s="T334">kuččuguj</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1296" n="HIAT:w" s="T335">dʼi͡e</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1299" n="HIAT:w" s="T336">turar</ts>
                  <nts id="Seg_1300" n="HIAT:ip">.</nts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1303" n="HIAT:u" s="T337">
                  <nts id="Seg_1304" n="HIAT:ip">"</nts>
                  <ts e="T338" id="Seg_1306" n="HIAT:w" s="T337">Čeː</ts>
                  <nts id="Seg_1307" n="HIAT:ip">,</nts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1310" n="HIAT:w" s="T338">hette</ts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1313" n="HIAT:w" s="T339">timek</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1316" n="HIAT:w" s="T340">iččitiger</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1319" n="HIAT:w" s="T341">bar</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1322" n="HIAT:w" s="T342">iti</ts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1325" n="HIAT:w" s="T343">dʼi͡ege</ts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1327" n="HIAT:ip">(</nts>
                  <ts e="T345" id="Seg_1329" n="HIAT:w" s="T344">staršina</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1332" n="HIAT:w" s="T345">bu͡olu͡o</ts>
                  <nts id="Seg_1333" n="HIAT:ip">)</nts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1336" n="HIAT:w" s="T346">kü͡öste</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1339" n="HIAT:w" s="T347">kördöː</ts>
                  <nts id="Seg_1340" n="HIAT:ip">.</nts>
                  <nts id="Seg_1341" n="HIAT:ip">"</nts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T355" id="Seg_1344" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_1346" n="HIAT:w" s="T348">U͡ol</ts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1349" n="HIAT:w" s="T349">barar</ts>
                  <nts id="Seg_1350" n="HIAT:ip">,</nts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1353" n="HIAT:w" s="T350">hette</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1356" n="HIAT:w" s="T351">timek</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1359" n="HIAT:w" s="T352">iččitiger</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1362" n="HIAT:w" s="T353">eter</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1365" n="HIAT:w" s="T354">ildʼiti</ts>
                  <nts id="Seg_1366" n="HIAT:ip">.</nts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T360" id="Seg_1369" n="HIAT:u" s="T355">
                  <nts id="Seg_1370" n="HIAT:ip">"</nts>
                  <ts e="T356" id="Seg_1372" n="HIAT:w" s="T355">Agɨs</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1375" n="HIAT:w" s="T356">koŋnomu͡oj</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1378" n="HIAT:w" s="T357">iččite</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1381" n="HIAT:w" s="T358">bi͡erde</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1384" n="HIAT:w" s="T359">eni</ts>
                  <nts id="Seg_1385" n="HIAT:ip">?</nts>
                  <nts id="Seg_1386" n="HIAT:ip">"</nts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1389" n="HIAT:u" s="T360">
                  <nts id="Seg_1390" n="HIAT:ip">"</nts>
                  <ts e="T361" id="Seg_1392" n="HIAT:w" s="T360">Hu͡ok</ts>
                  <nts id="Seg_1393" n="HIAT:ip">.</nts>
                  <nts id="Seg_1394" n="HIAT:ip">"</nts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366" id="Seg_1397" n="HIAT:u" s="T361">
                  <nts id="Seg_1398" n="HIAT:ip">"</nts>
                  <ts e="T362" id="Seg_1400" n="HIAT:w" s="T361">Eː</ts>
                  <nts id="Seg_1401" n="HIAT:ip">,</nts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1404" n="HIAT:w" s="T362">oččogo</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1407" n="HIAT:w" s="T363">min</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1410" n="HIAT:w" s="T364">emi͡e</ts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1413" n="HIAT:w" s="T365">bi͡erbeppin</ts>
                  <nts id="Seg_1414" n="HIAT:ip">.</nts>
                  <nts id="Seg_1415" n="HIAT:ip">"</nts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T367" id="Seg_1418" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1420" n="HIAT:w" s="T366">Ahɨːllar</ts>
                  <nts id="Seg_1421" n="HIAT:ip">.</nts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1424" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_1426" n="HIAT:w" s="T367">Harsi͡erda</ts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1429" n="HIAT:w" s="T368">ü͡ör</ts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1432" n="HIAT:w" s="T369">iher</ts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1435" n="HIAT:w" s="T370">emi͡e</ts>
                  <nts id="Seg_1436" n="HIAT:ip">.</nts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_1439" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1441" n="HIAT:w" s="T371">Tobugunan</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1444" n="HIAT:w" s="T372">ünen</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1447" n="HIAT:w" s="T373">taksar</ts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1450" n="HIAT:w" s="T374">Kuŋadʼaj</ts>
                  <nts id="Seg_1451" n="HIAT:ip">.</nts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1454" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1456" n="HIAT:w" s="T375">Kuŋadʼaj</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1459" n="HIAT:w" s="T376">töröːböt</ts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1462" n="HIAT:w" s="T377">maːŋkaːjɨ</ts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1465" n="HIAT:w" s="T378">tutan</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1468" n="HIAT:w" s="T379">ɨlar</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1471" n="HIAT:w" s="T380">kaldaːtɨttan</ts>
                  <nts id="Seg_1472" n="HIAT:ip">.</nts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T384" id="Seg_1475" n="HIAT:u" s="T381">
                  <nts id="Seg_1476" n="HIAT:ip">"</nts>
                  <ts e="T382" id="Seg_1478" n="HIAT:w" s="T381">Kaja-kuː</ts>
                  <nts id="Seg_1479" n="HIAT:ip">,</nts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1482" n="HIAT:w" s="T382">bahagɨ</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1485" n="HIAT:w" s="T383">tahaːr</ts>
                  <nts id="Seg_1486" n="HIAT:ip">!</nts>
                  <nts id="Seg_1487" n="HIAT:ip">"</nts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T386" id="Seg_1490" n="HIAT:u" s="T384">
                  <ts e="T385" id="Seg_1492" n="HIAT:w" s="T384">Ölörön</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1495" n="HIAT:w" s="T385">keːher</ts>
                  <nts id="Seg_1496" n="HIAT:ip">.</nts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1499" n="HIAT:u" s="T386">
                  <ts e="T387" id="Seg_1501" n="HIAT:w" s="T386">Emi͡e</ts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1504" n="HIAT:w" s="T387">hülbekke</ts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1507" n="HIAT:w" s="T388">hiːller</ts>
                  <nts id="Seg_1508" n="HIAT:ip">.</nts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T396" id="Seg_1511" n="HIAT:u" s="T389">
                  <ts e="T390" id="Seg_1513" n="HIAT:w" s="T389">Kaja</ts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1516" n="HIAT:w" s="T390">bu</ts>
                  <nts id="Seg_1517" n="HIAT:ip">,</nts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1520" n="HIAT:w" s="T391">bili</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1523" n="HIAT:w" s="T392">tojottor</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1526" n="HIAT:w" s="T393">gi͡ennerin</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1529" n="HIAT:w" s="T394">ölörbüt</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1532" n="HIAT:w" s="T395">ebit</ts>
                  <nts id="Seg_1533" n="HIAT:ip">.</nts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T397" id="Seg_1536" n="HIAT:u" s="T396">
                  <ts e="T397" id="Seg_1538" n="HIAT:w" s="T396">Utujallar</ts>
                  <nts id="Seg_1539" n="HIAT:ip">.</nts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1542" n="HIAT:u" s="T397">
                  <ts e="T398" id="Seg_1544" n="HIAT:w" s="T397">Harsi͡erda</ts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1547" n="HIAT:w" s="T398">Kuŋadʼaj</ts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1550" n="HIAT:w" s="T399">taksɨbɨta</ts>
                  <nts id="Seg_1551" n="HIAT:ip">,</nts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1554" n="HIAT:w" s="T400">Agɨs</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1557" n="HIAT:w" s="T401">koŋnomu͡oj</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1560" n="HIAT:w" s="T402">küseːjine</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1563" n="HIAT:w" s="T403">kürejin</ts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1566" n="HIAT:w" s="T404">ku͡ohana</ts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1569" n="HIAT:w" s="T405">oloror</ts>
                  <nts id="Seg_1570" n="HIAT:ip">.</nts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T411" id="Seg_1573" n="HIAT:u" s="T406">
                  <nts id="Seg_1574" n="HIAT:ip">"</nts>
                  <ts e="T407" id="Seg_1576" n="HIAT:w" s="T406">Kajaː</ts>
                  <nts id="Seg_1577" n="HIAT:ip">,</nts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1580" n="HIAT:w" s="T407">Agɨs</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1583" n="HIAT:w" s="T408">koŋnomu͡oj</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1586" n="HIAT:w" s="T409">iččite</ts>
                  <nts id="Seg_1587" n="HIAT:ip">,</nts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1590" n="HIAT:w" s="T410">doroːbo</ts>
                  <nts id="Seg_1591" n="HIAT:ip">!</nts>
                  <nts id="Seg_1592" n="HIAT:ip">"</nts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T413" id="Seg_1595" n="HIAT:u" s="T411">
                  <nts id="Seg_1596" n="HIAT:ip">"</nts>
                  <ts e="T412" id="Seg_1598" n="HIAT:w" s="T411">Össü͡ö</ts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1601" n="HIAT:w" s="T412">doroːbo</ts>
                  <nts id="Seg_1602" n="HIAT:ip">!</nts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T415" id="Seg_1605" n="HIAT:u" s="T413">
                  <ts e="T414" id="Seg_1607" n="HIAT:w" s="T413">Bügün</ts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1610" n="HIAT:w" s="T414">ölörtüːbüt</ts>
                  <nts id="Seg_1611" n="HIAT:ip">.</nts>
                  <nts id="Seg_1612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_1614" n="HIAT:u" s="T415">
                  <ts e="T416" id="Seg_1616" n="HIAT:w" s="T415">Ele</ts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1619" n="HIAT:w" s="T416">tabalarbɨttan</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1622" n="HIAT:w" s="T417">hiː</ts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1625" n="HIAT:w" s="T418">hɨtagɨt</ts>
                  <nts id="Seg_1626" n="HIAT:ip">!</nts>
                  <nts id="Seg_1627" n="HIAT:ip">"</nts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T421" id="Seg_1630" n="HIAT:u" s="T419">
                  <nts id="Seg_1631" n="HIAT:ip">"</nts>
                  <ts e="T420" id="Seg_1633" n="HIAT:w" s="T419">Bejegit</ts>
                  <nts id="Seg_1634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1636" n="HIAT:w" s="T420">burujgut</ts>
                  <nts id="Seg_1637" n="HIAT:ip">.</nts>
                  <nts id="Seg_1638" n="HIAT:ip">"</nts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T434" id="Seg_1641" n="HIAT:u" s="T421">
                  <ts e="T422" id="Seg_1643" n="HIAT:w" s="T421">Agɨs</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1646" n="HIAT:w" s="T422">koŋnomu͡oj</ts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1649" n="HIAT:w" s="T423">iččite</ts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1652" n="HIAT:w" s="T424">tobuktaːn</ts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1655" n="HIAT:w" s="T425">turar</ts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1658" n="HIAT:w" s="T426">kihini</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1661" n="HIAT:w" s="T427">kürej</ts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1664" n="HIAT:w" s="T428">ünüːtünen</ts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1667" n="HIAT:w" s="T429">öttükke</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1670" n="HIAT:w" s="T430">haːjar</ts>
                  <nts id="Seg_1671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1673" n="HIAT:w" s="T431">da</ts>
                  <nts id="Seg_1674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1676" n="HIAT:w" s="T432">hɨːhan</ts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1679" n="HIAT:w" s="T433">keːher</ts>
                  <nts id="Seg_1680" n="HIAT:ip">.</nts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T438" id="Seg_1683" n="HIAT:u" s="T434">
                  <ts e="T435" id="Seg_1685" n="HIAT:w" s="T434">Kuŋadʼaja</ts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1688" n="HIAT:w" s="T435">kaːr</ts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1691" n="HIAT:w" s="T436">annɨnan</ts>
                  <nts id="Seg_1692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1694" n="HIAT:w" s="T437">barbɨt</ts>
                  <nts id="Seg_1695" n="HIAT:ip">.</nts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_1698" n="HIAT:u" s="T438">
                  <ts e="T439" id="Seg_1700" n="HIAT:w" s="T438">Hette</ts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1703" n="HIAT:w" s="T439">timek</ts>
                  <nts id="Seg_1704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1706" n="HIAT:w" s="T440">iččite</ts>
                  <nts id="Seg_1707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1709" n="HIAT:w" s="T441">kürej</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1712" n="HIAT:w" s="T442">ku͡ohana</ts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1715" n="HIAT:w" s="T443">olordoguna</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1718" n="HIAT:w" s="T444">taksar</ts>
                  <nts id="Seg_1719" n="HIAT:ip">.</nts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T450" id="Seg_1722" n="HIAT:u" s="T445">
                  <nts id="Seg_1723" n="HIAT:ip">"</nts>
                  <ts e="T446" id="Seg_1725" n="HIAT:w" s="T445">Kajaː</ts>
                  <nts id="Seg_1726" n="HIAT:ip">,</nts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1729" n="HIAT:w" s="T446">hette</ts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1732" n="HIAT:w" s="T447">timek</ts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1735" n="HIAT:w" s="T448">iččite</ts>
                  <nts id="Seg_1736" n="HIAT:ip">,</nts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1739" n="HIAT:w" s="T449">doroːbo</ts>
                  <nts id="Seg_1740" n="HIAT:ip">!</nts>
                  <nts id="Seg_1741" n="HIAT:ip">"</nts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T452" id="Seg_1744" n="HIAT:u" s="T450">
                  <nts id="Seg_1745" n="HIAT:ip">"</nts>
                  <ts e="T451" id="Seg_1747" n="HIAT:w" s="T450">Össü͡ö</ts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1750" n="HIAT:w" s="T451">doroːbo</ts>
                  <nts id="Seg_1751" n="HIAT:ip">!</nts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T454" id="Seg_1754" n="HIAT:u" s="T452">
                  <ts e="T453" id="Seg_1756" n="HIAT:w" s="T452">Bügün</ts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1759" n="HIAT:w" s="T453">ölörtüːbüt</ts>
                  <nts id="Seg_1760" n="HIAT:ip">.</nts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T458" id="Seg_1763" n="HIAT:u" s="T454">
                  <ts e="T455" id="Seg_1765" n="HIAT:w" s="T454">Ele</ts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1768" n="HIAT:w" s="T455">tabalarbɨtɨn</ts>
                  <nts id="Seg_1769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1771" n="HIAT:w" s="T456">hiː</ts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1774" n="HIAT:w" s="T457">hɨtagɨt</ts>
                  <nts id="Seg_1775" n="HIAT:ip">!</nts>
                  <nts id="Seg_1776" n="HIAT:ip">"</nts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T460" id="Seg_1779" n="HIAT:u" s="T458">
                  <nts id="Seg_1780" n="HIAT:ip">"</nts>
                  <ts e="T459" id="Seg_1782" n="HIAT:w" s="T458">Bejegit</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1785" n="HIAT:w" s="T459">burujgut</ts>
                  <nts id="Seg_1786" n="HIAT:ip">.</nts>
                  <nts id="Seg_1787" n="HIAT:ip">"</nts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T472" id="Seg_1790" n="HIAT:u" s="T460">
                  <ts e="T461" id="Seg_1792" n="HIAT:w" s="T460">Hette</ts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1795" n="HIAT:w" s="T461">timek</ts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1798" n="HIAT:w" s="T462">iččite</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1801" n="HIAT:w" s="T463">tobuktaːn</ts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1804" n="HIAT:w" s="T464">turar</ts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1807" n="HIAT:w" s="T465">kihini</ts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1810" n="HIAT:w" s="T466">kürej</ts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1813" n="HIAT:w" s="T467">üŋüːtünen</ts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1816" n="HIAT:w" s="T468">öttükke</ts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1819" n="HIAT:w" s="T469">annʼaːrɨ</ts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1822" n="HIAT:w" s="T470">hiri</ts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1825" n="HIAT:w" s="T471">haːjbɨt</ts>
                  <nts id="Seg_1826" n="HIAT:ip">.</nts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T484" id="Seg_1829" n="HIAT:u" s="T472">
                  <ts e="T473" id="Seg_1831" n="HIAT:w" s="T472">Kaččaga</ts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1834" n="HIAT:w" s="T473">ere</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1837" n="HIAT:w" s="T474">Kuŋadʼaj</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1840" n="HIAT:w" s="T475">nöŋü͡ö</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1843" n="HIAT:w" s="T476">hirinen</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1846" n="HIAT:w" s="T477">taksa</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1849" n="HIAT:w" s="T478">kötör</ts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1852" n="HIAT:w" s="T479">da</ts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1855" n="HIAT:w" s="T480">ikki</ts>
                  <nts id="Seg_1856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1858" n="HIAT:w" s="T481">atagar</ts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1861" n="HIAT:w" s="T482">tura</ts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1864" n="HIAT:w" s="T483">ekkiriːr</ts>
                  <nts id="Seg_1865" n="HIAT:ip">.</nts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T488" id="Seg_1868" n="HIAT:u" s="T484">
                  <ts e="T485" id="Seg_1870" n="HIAT:w" s="T484">Körüleːbite</ts>
                  <nts id="Seg_1871" n="HIAT:ip">,</nts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1874" n="HIAT:w" s="T485">kös</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1877" n="HIAT:w" s="T486">baran</ts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1880" n="HIAT:w" s="T487">erer</ts>
                  <nts id="Seg_1881" n="HIAT:ip">.</nts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T494" id="Seg_1884" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_1886" n="HIAT:w" s="T488">Haŋaha</ts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1889" n="HIAT:w" s="T489">köhön</ts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1892" n="HIAT:w" s="T490">erer</ts>
                  <nts id="Seg_1893" n="HIAT:ip">,</nts>
                  <nts id="Seg_1894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1896" n="HIAT:w" s="T491">agata</ts>
                  <nts id="Seg_1897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1899" n="HIAT:w" s="T492">kergen</ts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1902" n="HIAT:w" s="T493">bi͡erbit</ts>
                  <nts id="Seg_1903" n="HIAT:ip">.</nts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T499" id="Seg_1906" n="HIAT:u" s="T494">
                  <ts e="T495" id="Seg_1908" n="HIAT:w" s="T494">Kuŋadʼaj</ts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1911" n="HIAT:w" s="T495">haŋahɨn</ts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1914" n="HIAT:w" s="T496">nʼu͡ogutun</ts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1917" n="HIAT:w" s="T497">kaban</ts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1920" n="HIAT:w" s="T498">ɨlar</ts>
                  <nts id="Seg_1921" n="HIAT:ip">:</nts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T500" id="Seg_1924" n="HIAT:u" s="T499">
                  <nts id="Seg_1925" n="HIAT:ip">"</nts>
                  <ts e="T500" id="Seg_1927" n="HIAT:w" s="T499">ɨːppappɨn</ts>
                  <nts id="Seg_1928" n="HIAT:ip">.</nts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T503" id="Seg_1931" n="HIAT:u" s="T500">
                  <ts e="T501" id="Seg_1933" n="HIAT:w" s="T500">Kaja</ts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1936" n="HIAT:w" s="T501">di͡eki</ts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1939" n="HIAT:w" s="T502">baragɨn</ts>
                  <nts id="Seg_1940" n="HIAT:ip">?</nts>
                  <nts id="Seg_1941" n="HIAT:ip">"</nts>
                  <nts id="Seg_1942" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T506" id="Seg_1944" n="HIAT:u" s="T503">
                  <ts e="T504" id="Seg_1946" n="HIAT:w" s="T503">Dʼaktar</ts>
                  <nts id="Seg_1947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1949" n="HIAT:w" s="T504">kötüten</ts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1952" n="HIAT:w" s="T505">kaːlar</ts>
                  <nts id="Seg_1953" n="HIAT:ip">.</nts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T512" id="Seg_1956" n="HIAT:u" s="T506">
                  <ts e="T507" id="Seg_1958" n="HIAT:w" s="T506">Kuŋadʼaj</ts>
                  <nts id="Seg_1959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1961" n="HIAT:w" s="T507">hite</ts>
                  <nts id="Seg_1962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1964" n="HIAT:w" s="T508">battaːn</ts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1967" n="HIAT:w" s="T509">ölgöbüːnün</ts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1970" n="HIAT:w" s="T510">ildʼe</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1973" n="HIAT:w" s="T511">kaːlar</ts>
                  <nts id="Seg_1974" n="HIAT:ip">.</nts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T517" id="Seg_1977" n="HIAT:u" s="T512">
                  <ts e="T513" id="Seg_1979" n="HIAT:w" s="T512">Kuŋadʼaj</ts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1982" n="HIAT:w" s="T513">Agɨs</ts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1985" n="HIAT:w" s="T514">koŋnomu͡oj</ts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1988" n="HIAT:w" s="T515">iččitiger</ts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1991" n="HIAT:w" s="T516">kiːrer</ts>
                  <nts id="Seg_1992" n="HIAT:ip">:</nts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T526" id="Seg_1995" n="HIAT:u" s="T517">
                  <nts id="Seg_1996" n="HIAT:ip">"</nts>
                  <ts e="T518" id="Seg_1998" n="HIAT:w" s="T517">Dʼe</ts>
                  <nts id="Seg_1999" n="HIAT:ip">,</nts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_2002" n="HIAT:w" s="T518">Agɨs</ts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_2005" n="HIAT:w" s="T519">koŋnomu͡oj</ts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2008" n="HIAT:w" s="T520">iččite</ts>
                  <nts id="Seg_2009" n="HIAT:ip">,</nts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2012" n="HIAT:w" s="T521">ejeleːk</ts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2015" n="HIAT:w" s="T522">erdekke</ts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_2018" n="HIAT:w" s="T523">balɨstarbɨn</ts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2021" n="HIAT:w" s="T524">karajan</ts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_2024" n="HIAT:w" s="T525">olor</ts>
                  <nts id="Seg_2025" n="HIAT:ip">.</nts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T532" id="Seg_2028" n="HIAT:u" s="T526">
                  <ts e="T527" id="Seg_2030" n="HIAT:w" s="T526">Kuhagannɨk</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2033" n="HIAT:w" s="T527">körü͡öŋ</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2035" n="HIAT:ip">—</nts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2038" n="HIAT:w" s="T528">biːr</ts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2041" n="HIAT:w" s="T529">kününen</ts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2044" n="HIAT:w" s="T530">hu͡ok</ts>
                  <nts id="Seg_2045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2047" n="HIAT:w" s="T531">oŋortu͡om</ts>
                  <nts id="Seg_2048" n="HIAT:ip">.</nts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T535" id="Seg_2051" n="HIAT:u" s="T532">
                  <ts e="T533" id="Seg_2053" n="HIAT:w" s="T532">Haŋaspɨn</ts>
                  <nts id="Seg_2054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2056" n="HIAT:w" s="T533">bata</ts>
                  <nts id="Seg_2057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2059" n="HIAT:w" s="T534">bardɨm</ts>
                  <nts id="Seg_2060" n="HIAT:ip">.</nts>
                  <nts id="Seg_2061" n="HIAT:ip">"</nts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T539" id="Seg_2064" n="HIAT:u" s="T535">
                  <ts e="T536" id="Seg_2066" n="HIAT:w" s="T535">Haŋahɨn</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2069" n="HIAT:w" s="T536">batan</ts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2072" n="HIAT:w" s="T537">hüteren</ts>
                  <nts id="Seg_2073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2075" n="HIAT:w" s="T538">keːher</ts>
                  <nts id="Seg_2076" n="HIAT:ip">.</nts>
                  <nts id="Seg_2077" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T545" id="Seg_2079" n="HIAT:u" s="T539">
                  <ts e="T540" id="Seg_2081" n="HIAT:w" s="T539">Kannɨk</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2084" n="HIAT:w" s="T540">dojdu</ts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2087" n="HIAT:w" s="T541">uhugar</ts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2090" n="HIAT:w" s="T542">körör</ts>
                  <nts id="Seg_2091" n="HIAT:ip">,</nts>
                  <nts id="Seg_2092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2094" n="HIAT:w" s="T543">hiti͡ek</ts>
                  <nts id="Seg_2095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2097" n="HIAT:w" s="T544">hippet</ts>
                  <nts id="Seg_2098" n="HIAT:ip">.</nts>
                  <nts id="Seg_2099" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T550" id="Seg_2101" n="HIAT:u" s="T545">
                  <ts e="T546" id="Seg_2103" n="HIAT:w" s="T545">Ergiten</ts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2106" n="HIAT:w" s="T546">egelen</ts>
                  <nts id="Seg_2107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2109" n="HIAT:w" s="T547">dʼi͡etin</ts>
                  <nts id="Seg_2110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2112" n="HIAT:w" s="T548">di͡ek</ts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2115" n="HIAT:w" s="T549">kü͡öjer</ts>
                  <nts id="Seg_2116" n="HIAT:ip">.</nts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T557" id="Seg_2119" n="HIAT:u" s="T550">
                  <ts e="T551" id="Seg_2121" n="HIAT:w" s="T550">Tutu͡ok</ts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2124" n="HIAT:w" s="T551">da</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2127" n="HIAT:w" s="T552">ereːri</ts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2130" n="HIAT:w" s="T553">tuppakka</ts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2133" n="HIAT:w" s="T554">haŋahɨn</ts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2136" n="HIAT:w" s="T555">kennitten</ts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2139" n="HIAT:w" s="T556">kiːrer</ts>
                  <nts id="Seg_2140" n="HIAT:ip">.</nts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T561" id="Seg_2143" n="HIAT:u" s="T557">
                  <ts e="T558" id="Seg_2145" n="HIAT:w" s="T557">Hanaha</ts>
                  <nts id="Seg_2146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2148" n="HIAT:w" s="T558">ogotun</ts>
                  <nts id="Seg_2149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2151" n="HIAT:w" s="T559">emijdiː</ts>
                  <nts id="Seg_2152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2154" n="HIAT:w" s="T560">oloror</ts>
                  <nts id="Seg_2155" n="HIAT:ip">.</nts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T564" id="Seg_2158" n="HIAT:u" s="T561">
                  <ts e="T562" id="Seg_2160" n="HIAT:w" s="T561">Balɨstara</ts>
                  <nts id="Seg_2161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2163" n="HIAT:w" s="T562">ahɨː</ts>
                  <nts id="Seg_2164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2166" n="HIAT:w" s="T563">oloror</ts>
                  <nts id="Seg_2167" n="HIAT:ip">.</nts>
                  <nts id="Seg_2168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T568" id="Seg_2170" n="HIAT:u" s="T564">
                  <nts id="Seg_2171" n="HIAT:ip">"</nts>
                  <ts e="T565" id="Seg_2173" n="HIAT:w" s="T564">Dʼe</ts>
                  <nts id="Seg_2174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2176" n="HIAT:w" s="T565">haŋaːs</ts>
                  <nts id="Seg_2177" n="HIAT:ip">,</nts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2180" n="HIAT:w" s="T566">burujgun</ts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2183" n="HIAT:w" s="T567">ahardɨŋ</ts>
                  <nts id="Seg_2184" n="HIAT:ip">.</nts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T573" id="Seg_2187" n="HIAT:u" s="T568">
                  <ts e="T569" id="Seg_2189" n="HIAT:w" s="T568">Ogogun</ts>
                  <nts id="Seg_2190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2192" n="HIAT:w" s="T569">ɨlbatagɨŋ</ts>
                  <nts id="Seg_2193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2195" n="HIAT:w" s="T570">bu͡ollar</ts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2198" n="HIAT:w" s="T571">ölörü͡ök</ts>
                  <nts id="Seg_2199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2201" n="HIAT:w" s="T572">etim</ts>
                  <nts id="Seg_2202" n="HIAT:ip">!</nts>
                  <nts id="Seg_2203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T578" id="Seg_2205" n="HIAT:u" s="T573">
                  <ts e="T574" id="Seg_2207" n="HIAT:w" s="T573">Agɨs</ts>
                  <nts id="Seg_2208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2210" n="HIAT:w" s="T574">koŋnomu͡oj</ts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2213" n="HIAT:w" s="T575">iččite</ts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2216" n="HIAT:w" s="T576">kuttanaːrɨ</ts>
                  <nts id="Seg_2217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2219" n="HIAT:w" s="T577">kuttanar</ts>
                  <nts id="Seg_2220" n="HIAT:ip">.</nts>
                  <nts id="Seg_2221" n="HIAT:ip">"</nts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T579" id="Seg_2224" n="HIAT:u" s="T578">
                  <ts e="T579" id="Seg_2226" n="HIAT:w" s="T578">Olorollor</ts>
                  <nts id="Seg_2227" n="HIAT:ip">.</nts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T583" id="Seg_2230" n="HIAT:u" s="T579">
                  <ts e="T580" id="Seg_2232" n="HIAT:w" s="T579">Kahan</ts>
                  <nts id="Seg_2233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2235" n="HIAT:w" s="T580">ere</ts>
                  <nts id="Seg_2236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2238" n="HIAT:w" s="T581">ühüs</ts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2241" n="HIAT:w" s="T582">künüger</ts>
                  <nts id="Seg_2242" n="HIAT:ip">:</nts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T589" id="Seg_2245" n="HIAT:u" s="T583">
                  <nts id="Seg_2246" n="HIAT:ip">"</nts>
                  <ts e="T584" id="Seg_2248" n="HIAT:w" s="T583">Dʼe</ts>
                  <nts id="Seg_2249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2251" n="HIAT:w" s="T584">Agɨs</ts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2254" n="HIAT:w" s="T585">koŋnomu͡oj</ts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2257" n="HIAT:w" s="T586">iččite</ts>
                  <nts id="Seg_2258" n="HIAT:ip">,</nts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2261" n="HIAT:w" s="T587">dʼoŋŋun</ts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2264" n="HIAT:w" s="T588">mus</ts>
                  <nts id="Seg_2265" n="HIAT:ip">.</nts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T592" id="Seg_2268" n="HIAT:u" s="T589">
                  <ts e="T590" id="Seg_2270" n="HIAT:w" s="T589">Munnʼaktɨ͡akpɨt</ts>
                  <nts id="Seg_2271" n="HIAT:ip">"</nts>
                  <nts id="Seg_2272" n="HIAT:ip">,</nts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2275" n="HIAT:w" s="T590">diːr</ts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2278" n="HIAT:w" s="T591">Kuŋadʼaj</ts>
                  <nts id="Seg_2279" n="HIAT:ip">.</nts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T598" id="Seg_2282" n="HIAT:u" s="T592">
                  <ts e="T593" id="Seg_2284" n="HIAT:w" s="T592">Dʼe</ts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2287" n="HIAT:w" s="T593">munnʼallar</ts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2290" n="HIAT:w" s="T594">hette</ts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2293" n="HIAT:w" s="T595">u͡on</ts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2296" n="HIAT:w" s="T596">dʼi͡e</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2299" n="HIAT:w" s="T597">kihitin</ts>
                  <nts id="Seg_2300" n="HIAT:ip">.</nts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T600" id="Seg_2303" n="HIAT:u" s="T598">
                  <ts e="T599" id="Seg_2305" n="HIAT:w" s="T598">Kuŋadʼaj</ts>
                  <nts id="Seg_2306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2308" n="HIAT:w" s="T599">eter</ts>
                  <nts id="Seg_2309" n="HIAT:ip">:</nts>
                  <nts id="Seg_2310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T611" id="Seg_2312" n="HIAT:u" s="T600">
                  <nts id="Seg_2313" n="HIAT:ip">"</nts>
                  <ts e="T601" id="Seg_2315" n="HIAT:w" s="T600">Ehigi</ts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2318" n="HIAT:w" s="T601">ikki</ts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2321" n="HIAT:w" s="T602">küseːjin</ts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2324" n="HIAT:w" s="T603">baːrgɨt</ts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2326" n="HIAT:ip">—</nts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2329" n="HIAT:w" s="T604">Agɨs</ts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2332" n="HIAT:w" s="T605">koŋnomu͡oj</ts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2335" n="HIAT:w" s="T606">küseːjine</ts>
                  <nts id="Seg_2336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2338" n="HIAT:w" s="T607">u͡onna</ts>
                  <nts id="Seg_2339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2341" n="HIAT:w" s="T608">hette</ts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2344" n="HIAT:w" s="T609">timek</ts>
                  <nts id="Seg_2345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2347" n="HIAT:w" s="T610">iččite</ts>
                  <nts id="Seg_2348" n="HIAT:ip">.</nts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T615" id="Seg_2351" n="HIAT:u" s="T611">
                  <ts e="T612" id="Seg_2353" n="HIAT:w" s="T611">Anɨ</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2356" n="HIAT:w" s="T612">küseːjin</ts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2359" n="HIAT:w" s="T613">bu͡olan</ts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2362" n="HIAT:w" s="T614">büttügüt</ts>
                  <nts id="Seg_2363" n="HIAT:ip">.</nts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T621" id="Seg_2366" n="HIAT:u" s="T615">
                  <ts e="T616" id="Seg_2368" n="HIAT:w" s="T615">Küseːjiŋŋit</ts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2370" n="HIAT:ip">—</nts>
                  <nts id="Seg_2371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2373" n="HIAT:w" s="T616">iti</ts>
                  <nts id="Seg_2374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2376" n="HIAT:w" s="T617">u͡ol</ts>
                  <nts id="Seg_2377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2379" n="HIAT:w" s="T618">ogo</ts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2381" n="HIAT:ip">—</nts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2384" n="HIAT:w" s="T619">ubajɨm</ts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2387" n="HIAT:w" s="T620">ogoto</ts>
                  <nts id="Seg_2388" n="HIAT:ip">.</nts>
                  <nts id="Seg_2389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T625" id="Seg_2391" n="HIAT:u" s="T621">
                  <ts e="T622" id="Seg_2393" n="HIAT:w" s="T621">Komunuŋ</ts>
                  <nts id="Seg_2394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2396" n="HIAT:w" s="T622">barɨgɨt</ts>
                  <nts id="Seg_2397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2399" n="HIAT:w" s="T623">ikki</ts>
                  <nts id="Seg_2400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2402" n="HIAT:w" s="T624">küŋŋe</ts>
                  <nts id="Seg_2403" n="HIAT:ip">.</nts>
                  <nts id="Seg_2404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T628" id="Seg_2406" n="HIAT:u" s="T625">
                  <ts e="T626" id="Seg_2408" n="HIAT:w" s="T625">Ühüs</ts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2411" n="HIAT:w" s="T626">küŋŋütüger</ts>
                  <nts id="Seg_2412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2414" n="HIAT:w" s="T627">köhü͡ökküt</ts>
                  <nts id="Seg_2415" n="HIAT:ip">.</nts>
                  <nts id="Seg_2416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T631" id="Seg_2418" n="HIAT:u" s="T628">
                  <ts e="T629" id="Seg_2420" n="HIAT:w" s="T628">Bastɨŋŋɨt</ts>
                  <nts id="Seg_2421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2423" n="HIAT:w" s="T629">kini</ts>
                  <nts id="Seg_2424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2426" n="HIAT:w" s="T630">bu͡olu͡o</ts>
                  <nts id="Seg_2427" n="HIAT:ip">.</nts>
                  <nts id="Seg_2428" n="HIAT:ip">"</nts>
                  <nts id="Seg_2429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T642" id="Seg_2431" n="HIAT:u" s="T631">
                  <nts id="Seg_2432" n="HIAT:ip">"</nts>
                  <ts e="T632" id="Seg_2434" n="HIAT:w" s="T631">Atɨn</ts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2437" n="HIAT:w" s="T632">tabanɨ</ts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2440" n="HIAT:w" s="T633">kölünüme</ts>
                  <nts id="Seg_2441" n="HIAT:ip">,</nts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2444" n="HIAT:w" s="T634">agaŋ</ts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2447" n="HIAT:w" s="T635">tabatɨn</ts>
                  <nts id="Seg_2448" n="HIAT:ip">,</nts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2451" n="HIAT:w" s="T636">keri͡ehin</ts>
                  <nts id="Seg_2452" n="HIAT:ip">,</nts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2455" n="HIAT:w" s="T637">kölüneːr</ts>
                  <nts id="Seg_2456" n="HIAT:ip">"</nts>
                  <nts id="Seg_2457" n="HIAT:ip">,</nts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2460" n="HIAT:w" s="T638">diːr</ts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2463" n="HIAT:w" s="T639">u͡olga</ts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2465" n="HIAT:ip">[</nts>
                  <ts e="T641" id="Seg_2467" n="HIAT:w" s="T640">u͡ol</ts>
                  <nts id="Seg_2468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2470" n="HIAT:w" s="T641">ulaːppɨt</ts>
                  <nts id="Seg_2471" n="HIAT:ip">]</nts>
                  <nts id="Seg_2472" n="HIAT:ip">.</nts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T644" id="Seg_2475" n="HIAT:u" s="T642">
                  <nts id="Seg_2476" n="HIAT:ip">"</nts>
                  <ts e="T643" id="Seg_2478" n="HIAT:w" s="T642">Bastataŋŋɨn</ts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2481" n="HIAT:w" s="T643">köhü͡ökküt</ts>
                  <nts id="Seg_2482" n="HIAT:ip">.</nts>
                  <nts id="Seg_2483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T658" id="Seg_2485" n="HIAT:u" s="T644">
                  <ts e="T645" id="Seg_2487" n="HIAT:w" s="T644">Mas</ts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2490" n="HIAT:w" s="T645">hagatɨnan</ts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2493" n="HIAT:w" s="T646">dʼirbiː</ts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2496" n="HIAT:w" s="T647">baːr</ts>
                  <nts id="Seg_2497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2499" n="HIAT:w" s="T648">bu͡olu͡o</ts>
                  <nts id="Seg_2500" n="HIAT:ip">,</nts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2503" n="HIAT:w" s="T649">bu</ts>
                  <nts id="Seg_2504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2506" n="HIAT:w" s="T650">dʼirbiː</ts>
                  <nts id="Seg_2507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2509" n="HIAT:w" s="T651">mu͡ora</ts>
                  <nts id="Seg_2510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2512" n="HIAT:w" s="T652">di͡eki</ts>
                  <nts id="Seg_2513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2515" n="HIAT:w" s="T653">öttünen</ts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2518" n="HIAT:w" s="T654">köhü͡öŋ</ts>
                  <nts id="Seg_2519" n="HIAT:ip">,</nts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2522" n="HIAT:w" s="T655">ikki</ts>
                  <nts id="Seg_2523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2525" n="HIAT:w" s="T656">ɨjɨ</ts>
                  <nts id="Seg_2526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2528" n="HIAT:w" s="T657">bɨha</ts>
                  <nts id="Seg_2529" n="HIAT:ip">.</nts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T668" id="Seg_2532" n="HIAT:u" s="T658">
                  <ts e="T659" id="Seg_2534" n="HIAT:w" s="T658">Elete</ts>
                  <nts id="Seg_2535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2537" n="HIAT:w" s="T659">bu͡ollagɨna</ts>
                  <nts id="Seg_2538" n="HIAT:ip">,</nts>
                  <nts id="Seg_2539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2541" n="HIAT:w" s="T660">dʼirbiːte</ts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2544" n="HIAT:w" s="T661">büttegine</ts>
                  <nts id="Seg_2545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2547" n="HIAT:w" s="T662">hir</ts>
                  <nts id="Seg_2548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2550" n="HIAT:w" s="T663">bu͡olu͡o</ts>
                  <nts id="Seg_2551" n="HIAT:ip">,</nts>
                  <nts id="Seg_2552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2554" n="HIAT:w" s="T664">kaːrɨn</ts>
                  <nts id="Seg_2555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2557" n="HIAT:w" s="T665">annɨttan</ts>
                  <nts id="Seg_2558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2560" n="HIAT:w" s="T666">buru͡olaːk</ts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2563" n="HIAT:w" s="T667">bu͡olu͡o</ts>
                  <nts id="Seg_2564" n="HIAT:ip">.</nts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T672" id="Seg_2567" n="HIAT:u" s="T668">
                  <ts e="T669" id="Seg_2569" n="HIAT:w" s="T668">Ol</ts>
                  <nts id="Seg_2570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2572" n="HIAT:w" s="T669">bihigi</ts>
                  <nts id="Seg_2573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2575" n="HIAT:w" s="T670">törüt</ts>
                  <nts id="Seg_2576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2578" n="HIAT:w" s="T671">hirbit</ts>
                  <nts id="Seg_2579" n="HIAT:ip">.</nts>
                  <nts id="Seg_2580" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T677" id="Seg_2582" n="HIAT:u" s="T672">
                  <ts e="T673" id="Seg_2584" n="HIAT:w" s="T672">Ulakan</ts>
                  <nts id="Seg_2585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2587" n="HIAT:w" s="T673">hu͡olga</ts>
                  <nts id="Seg_2588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2590" n="HIAT:w" s="T674">kiːri͡eŋ</ts>
                  <nts id="Seg_2591" n="HIAT:ip">,</nts>
                  <nts id="Seg_2592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2594" n="HIAT:w" s="T675">ajmaktargar</ts>
                  <nts id="Seg_2595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2597" n="HIAT:w" s="T676">tiji͡eŋ</ts>
                  <nts id="Seg_2598" n="HIAT:ip">.</nts>
                  <nts id="Seg_2599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_2601" n="HIAT:u" s="T677">
                  <ts e="T678" id="Seg_2603" n="HIAT:w" s="T677">Dʼonnor</ts>
                  <nts id="Seg_2604" n="HIAT:ip">,</nts>
                  <nts id="Seg_2605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2607" n="HIAT:w" s="T678">tabagatɨn</ts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2610" n="HIAT:w" s="T679">tüːnü-künü</ts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2613" n="HIAT:w" s="T680">bɨha</ts>
                  <nts id="Seg_2614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2616" n="HIAT:w" s="T681">körü͡ökküt</ts>
                  <nts id="Seg_2617" n="HIAT:ip">.</nts>
                  <nts id="Seg_2618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T690" id="Seg_2620" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_2622" n="HIAT:w" s="T682">Agɨs</ts>
                  <nts id="Seg_2623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2625" n="HIAT:w" s="T683">koŋnomu͡oj</ts>
                  <nts id="Seg_2626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2628" n="HIAT:w" s="T684">küseːjine</ts>
                  <nts id="Seg_2629" n="HIAT:ip">,</nts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2632" n="HIAT:w" s="T685">hette</ts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2635" n="HIAT:w" s="T686">timek</ts>
                  <nts id="Seg_2636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2638" n="HIAT:w" s="T687">iččite</ts>
                  <nts id="Seg_2639" n="HIAT:ip">,</nts>
                  <nts id="Seg_2640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2642" n="HIAT:w" s="T688">emi͡e</ts>
                  <nts id="Seg_2643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2645" n="HIAT:w" s="T689">körsü͡ökküt</ts>
                  <nts id="Seg_2646" n="HIAT:ip">.</nts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T703" id="Seg_2649" n="HIAT:u" s="T690">
                  <ts e="T691" id="Seg_2651" n="HIAT:w" s="T690">Min</ts>
                  <nts id="Seg_2652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2654" n="HIAT:w" s="T691">bu͡ollagɨna</ts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2657" n="HIAT:w" s="T692">bu</ts>
                  <nts id="Seg_2658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2660" n="HIAT:w" s="T693">üčügejdik</ts>
                  <nts id="Seg_2661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2663" n="HIAT:w" s="T694">ajannɨːrgɨt</ts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2666" n="HIAT:w" s="T695">tuhuttan</ts>
                  <nts id="Seg_2667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2669" n="HIAT:w" s="T696">kuhagan</ts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2672" n="HIAT:w" s="T697">dʼonu</ts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2674" n="HIAT:ip">—</nts>
                  <nts id="Seg_2675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2677" n="HIAT:w" s="T698">čaŋɨttarɨ</ts>
                  <nts id="Seg_2678" n="HIAT:ip">,</nts>
                  <nts id="Seg_2679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2681" n="HIAT:w" s="T699">kɨːllarɨ</ts>
                  <nts id="Seg_2682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2684" n="HIAT:w" s="T700">hu͡ok</ts>
                  <nts id="Seg_2685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2687" n="HIAT:w" s="T701">gɨna</ts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2690" n="HIAT:w" s="T702">bardɨm</ts>
                  <nts id="Seg_2691" n="HIAT:ip">.</nts>
                  <nts id="Seg_2692" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T714" id="Seg_2694" n="HIAT:u" s="T703">
                  <ts e="T704" id="Seg_2696" n="HIAT:w" s="T703">Ol</ts>
                  <nts id="Seg_2697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2699" n="HIAT:w" s="T704">hirge</ts>
                  <nts id="Seg_2700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2702" n="HIAT:w" s="T705">tijer</ts>
                  <nts id="Seg_2703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2705" n="HIAT:w" s="T706">küŋŋer</ts>
                  <nts id="Seg_2706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2708" n="HIAT:w" s="T707">tiji͡em</ts>
                  <nts id="Seg_2709" n="HIAT:ip">,</nts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2712" n="HIAT:w" s="T708">ulakannɨk</ts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2715" n="HIAT:w" s="T709">haːraːtakpɨna</ts>
                  <nts id="Seg_2716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2718" n="HIAT:w" s="T710">ühüs</ts>
                  <nts id="Seg_2719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2721" n="HIAT:w" s="T711">künüger</ts>
                  <nts id="Seg_2722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2724" n="HIAT:w" s="T712">tiji͡em</ts>
                  <nts id="Seg_2725" n="HIAT:ip">"</nts>
                  <nts id="Seg_2726" n="HIAT:ip">,</nts>
                  <nts id="Seg_2727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2729" n="HIAT:w" s="T713">diːr</ts>
                  <nts id="Seg_2730" n="HIAT:ip">.</nts>
                  <nts id="Seg_2731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T717" id="Seg_2733" n="HIAT:u" s="T714">
                  <ts e="T715" id="Seg_2735" n="HIAT:w" s="T714">Ühüs</ts>
                  <nts id="Seg_2736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2738" n="HIAT:w" s="T715">künüger</ts>
                  <nts id="Seg_2739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2741" n="HIAT:w" s="T716">köhöllör</ts>
                  <nts id="Seg_2742" n="HIAT:ip">.</nts>
                  <nts id="Seg_2743" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T722" id="Seg_2745" n="HIAT:u" s="T717">
                  <ts e="T718" id="Seg_2747" n="HIAT:w" s="T717">Kuŋadʼaj</ts>
                  <nts id="Seg_2748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2750" n="HIAT:w" s="T718">aragan</ts>
                  <nts id="Seg_2751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2753" n="HIAT:w" s="T719">barar</ts>
                  <nts id="Seg_2754" n="HIAT:ip">,</nts>
                  <nts id="Seg_2755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2757" n="HIAT:w" s="T720">hu͡os</ts>
                  <nts id="Seg_2758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2760" n="HIAT:w" s="T721">hatɨː</ts>
                  <nts id="Seg_2761" n="HIAT:ip">.</nts>
                  <nts id="Seg_2762" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T728" id="Seg_2764" n="HIAT:u" s="T722">
                  <ts e="T723" id="Seg_2766" n="HIAT:w" s="T722">Baː</ts>
                  <nts id="Seg_2767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2769" n="HIAT:w" s="T723">ogo</ts>
                  <nts id="Seg_2770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2772" n="HIAT:w" s="T724">bastɨŋ</ts>
                  <nts id="Seg_2773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2775" n="HIAT:w" s="T725">bu͡olan</ts>
                  <nts id="Seg_2776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2778" n="HIAT:w" s="T726">köhön</ts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2781" n="HIAT:w" s="T727">istiler</ts>
                  <nts id="Seg_2782" n="HIAT:ip">.</nts>
                  <nts id="Seg_2783" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T735" id="Seg_2785" n="HIAT:u" s="T728">
                  <ts e="T729" id="Seg_2787" n="HIAT:w" s="T728">Ketenen</ts>
                  <nts id="Seg_2788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2790" n="HIAT:w" s="T729">ajannɨːllar</ts>
                  <nts id="Seg_2791" n="HIAT:ip">,</nts>
                  <nts id="Seg_2792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2794" n="HIAT:w" s="T730">tojonu-kotunu</ts>
                  <nts id="Seg_2795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2797" n="HIAT:w" s="T731">barɨtɨn</ts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2800" n="HIAT:w" s="T732">keteter</ts>
                  <nts id="Seg_2801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2803" n="HIAT:w" s="T733">bu</ts>
                  <nts id="Seg_2804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2806" n="HIAT:w" s="T734">u͡ol</ts>
                  <nts id="Seg_2807" n="HIAT:ip">.</nts>
                  <nts id="Seg_2808" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T738" id="Seg_2810" n="HIAT:u" s="T735">
                  <ts e="T736" id="Seg_2812" n="HIAT:w" s="T735">ɨjɨn</ts>
                  <nts id="Seg_2813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2815" n="HIAT:w" s="T736">ortoto</ts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2818" n="HIAT:w" s="T737">bu͡olbut</ts>
                  <nts id="Seg_2819" n="HIAT:ip">.</nts>
                  <nts id="Seg_2820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T741" id="Seg_2822" n="HIAT:u" s="T738">
                  <ts e="T739" id="Seg_2824" n="HIAT:w" s="T738">Dʼirbiːtin</ts>
                  <nts id="Seg_2825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2827" n="HIAT:w" s="T739">irdeːn</ts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2830" n="HIAT:w" s="T740">istege</ts>
                  <nts id="Seg_2831" n="HIAT:ip">.</nts>
                  <nts id="Seg_2832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T749" id="Seg_2834" n="HIAT:u" s="T741">
                  <ts e="T742" id="Seg_2836" n="HIAT:w" s="T741">Araj</ts>
                  <nts id="Seg_2837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2839" n="HIAT:w" s="T742">mu͡ora</ts>
                  <nts id="Seg_2840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2842" n="HIAT:w" s="T743">di͡eki</ts>
                  <nts id="Seg_2843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2845" n="HIAT:w" s="T744">dʼirbiː</ts>
                  <nts id="Seg_2846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2848" n="HIAT:w" s="T745">aragar</ts>
                  <nts id="Seg_2849" n="HIAT:ip">,</nts>
                  <nts id="Seg_2850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2852" n="HIAT:w" s="T746">ürdüger</ts>
                  <nts id="Seg_2853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2855" n="HIAT:w" s="T747">kihi</ts>
                  <nts id="Seg_2856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2858" n="HIAT:w" s="T748">hɨldʼar</ts>
                  <nts id="Seg_2859" n="HIAT:ip">.</nts>
                  <nts id="Seg_2860" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T752" id="Seg_2862" n="HIAT:u" s="T749">
                  <nts id="Seg_2863" n="HIAT:ip">"</nts>
                  <ts e="T750" id="Seg_2865" n="HIAT:w" s="T749">Kaja</ts>
                  <nts id="Seg_2866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2868" n="HIAT:w" s="T750">di͡ekki</ts>
                  <nts id="Seg_2869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2871" n="HIAT:w" s="T751">kihiginij</ts>
                  <nts id="Seg_2872" n="HIAT:ip">?</nts>
                  <nts id="Seg_2873" n="HIAT:ip">"</nts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T754" id="Seg_2876" n="HIAT:u" s="T752">
                  <nts id="Seg_2877" n="HIAT:ip">"</nts>
                  <ts e="T753" id="Seg_2879" n="HIAT:w" s="T752">Tu͡ok</ts>
                  <nts id="Seg_2880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2882" n="HIAT:w" s="T753">keli͡ej</ts>
                  <nts id="Seg_2883" n="HIAT:ip">?</nts>
                  <nts id="Seg_2884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T756" id="Seg_2886" n="HIAT:u" s="T754">
                  <ts e="T755" id="Seg_2888" n="HIAT:w" s="T754">Dʼi͡eni-u͡otu</ts>
                  <nts id="Seg_2889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2891" n="HIAT:w" s="T755">bilbeppin</ts>
                  <nts id="Seg_2892" n="HIAT:ip">.</nts>
                  <nts id="Seg_2893" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T760" id="Seg_2895" n="HIAT:u" s="T756">
                  <ts e="T757" id="Seg_2897" n="HIAT:w" s="T756">Töröːbüt</ts>
                  <nts id="Seg_2898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2900" n="HIAT:w" s="T757">ubajbɨn</ts>
                  <nts id="Seg_2901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2903" n="HIAT:w" s="T758">hütere</ts>
                  <nts id="Seg_2904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2906" n="HIAT:w" s="T759">hɨldʼabɨn</ts>
                  <nts id="Seg_2907" n="HIAT:ip">.</nts>
                  <nts id="Seg_2908" n="HIAT:ip">"</nts>
                  <nts id="Seg_2909" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T762" id="Seg_2911" n="HIAT:u" s="T760">
                  <nts id="Seg_2912" n="HIAT:ip">"</nts>
                  <ts e="T761" id="Seg_2914" n="HIAT:w" s="T760">Kajalaraj</ts>
                  <nts id="Seg_2915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2917" n="HIAT:w" s="T761">ol</ts>
                  <nts id="Seg_2918" n="HIAT:ip">?</nts>
                  <nts id="Seg_2919" n="HIAT:ip">"</nts>
                  <nts id="Seg_2920" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T765" id="Seg_2922" n="HIAT:u" s="T762">
                  <nts id="Seg_2923" n="HIAT:ip">"</nts>
                  <ts e="T763" id="Seg_2925" n="HIAT:w" s="T762">Agɨs</ts>
                  <nts id="Seg_2926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2928" n="HIAT:w" s="T763">koŋnomu͡oj</ts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2931" n="HIAT:w" s="T764">küseːjine</ts>
                  <nts id="Seg_2932" n="HIAT:ip">!</nts>
                  <nts id="Seg_2933" n="HIAT:ip">"</nts>
                  <nts id="Seg_2934" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T766" id="Seg_2936" n="HIAT:u" s="T765">
                  <nts id="Seg_2937" n="HIAT:ip">"</nts>
                  <ts e="T766" id="Seg_2939" n="HIAT:w" s="T765">Heːj</ts>
                  <nts id="Seg_2940" n="HIAT:ip">!</nts>
                  <nts id="Seg_2941" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T769" id="Seg_2943" n="HIAT:u" s="T766">
                  <ts e="T767" id="Seg_2945" n="HIAT:w" s="T766">Ol</ts>
                  <nts id="Seg_2946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2948" n="HIAT:w" s="T767">min</ts>
                  <nts id="Seg_2949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2951" n="HIAT:w" s="T768">kihim</ts>
                  <nts id="Seg_2952" n="HIAT:ip">.</nts>
                  <nts id="Seg_2953" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T773" id="Seg_2955" n="HIAT:u" s="T769">
                  <ts e="T770" id="Seg_2957" n="HIAT:w" s="T769">Ol</ts>
                  <nts id="Seg_2958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_2960" n="HIAT:w" s="T770">delemičeː</ts>
                  <nts id="Seg_2961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2963" n="HIAT:w" s="T771">üːren</ts>
                  <nts id="Seg_2964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2966" n="HIAT:w" s="T772">iher</ts>
                  <nts id="Seg_2967" n="HIAT:ip">.</nts>
                  <nts id="Seg_2968" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T778" id="Seg_2970" n="HIAT:u" s="T773">
                  <ts e="T774" id="Seg_2972" n="HIAT:w" s="T773">Dʼe</ts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2975" n="HIAT:w" s="T774">oččogo</ts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2978" n="HIAT:w" s="T775">mi͡eke</ts>
                  <nts id="Seg_2979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2981" n="HIAT:w" s="T776">kihi</ts>
                  <nts id="Seg_2982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_2984" n="HIAT:w" s="T777">bu͡ol</ts>
                  <nts id="Seg_2985" n="HIAT:ip">.</nts>
                  <nts id="Seg_2986" n="HIAT:ip">"</nts>
                  <nts id="Seg_2987" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T780" id="Seg_2989" n="HIAT:u" s="T778">
                  <ts e="T779" id="Seg_2991" n="HIAT:w" s="T778">Haŋara</ts>
                  <nts id="Seg_2992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2994" n="HIAT:w" s="T779">tarpat</ts>
                  <nts id="Seg_2995" n="HIAT:ip">.</nts>
                  <nts id="Seg_2996" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T796" id="Seg_2998" n="HIAT:u" s="T780">
                  <ts e="T781" id="Seg_3000" n="HIAT:w" s="T780">I͡ehili</ts>
                  <nts id="Seg_3001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_3003" n="HIAT:w" s="T781">kihi</ts>
                  <nts id="Seg_3004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3006" n="HIAT:w" s="T782">bu͡olu͡oŋ</ts>
                  <nts id="Seg_3007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3009" n="HIAT:w" s="T783">hu͡oga</ts>
                  <nts id="Seg_3010" n="HIAT:ip">,</nts>
                  <nts id="Seg_3011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_3013" n="HIAT:w" s="T784">bu</ts>
                  <nts id="Seg_3014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_3016" n="HIAT:w" s="T785">üs</ts>
                  <nts id="Seg_3017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3019" n="HIAT:w" s="T786">halaːlaːk</ts>
                  <nts id="Seg_3020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_3022" n="HIAT:w" s="T787">kürejim</ts>
                  <nts id="Seg_3023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_3025" n="HIAT:w" s="T788">aŋar</ts>
                  <nts id="Seg_3026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_3028" n="HIAT:w" s="T789">halaːta</ts>
                  <nts id="Seg_3029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_3031" n="HIAT:w" s="T790">kaŋɨrgaha</ts>
                  <nts id="Seg_3032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3034" n="HIAT:w" s="T791">bu͡olu͡oŋ</ts>
                  <nts id="Seg_3035" n="HIAT:ip">,</nts>
                  <nts id="Seg_3036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_3038" n="HIAT:w" s="T792">ikkite</ts>
                  <nts id="Seg_3039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3041" n="HIAT:w" s="T793">bukatɨːr</ts>
                  <nts id="Seg_3042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3044" n="HIAT:w" s="T794">eːldete</ts>
                  <nts id="Seg_3045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3047" n="HIAT:w" s="T795">kaŋɨrgastaːk</ts>
                  <nts id="Seg_3048" n="HIAT:ip">.</nts>
                  <nts id="Seg_3049" n="HIAT:ip">"</nts>
                  <nts id="Seg_3050" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T799" id="Seg_3052" n="HIAT:u" s="T796">
                  <ts e="T797" id="Seg_3054" n="HIAT:w" s="T796">Kihi</ts>
                  <nts id="Seg_3055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_3057" n="HIAT:w" s="T797">höbülener</ts>
                  <nts id="Seg_3058" n="HIAT:ip">,</nts>
                  <nts id="Seg_3059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3061" n="HIAT:w" s="T798">barsar</ts>
                  <nts id="Seg_3062" n="HIAT:ip">.</nts>
                  <nts id="Seg_3063" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T801" id="Seg_3065" n="HIAT:u" s="T799">
                  <ts e="T800" id="Seg_3067" n="HIAT:w" s="T799">Köhön</ts>
                  <nts id="Seg_3068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3070" n="HIAT:w" s="T800">istiler</ts>
                  <nts id="Seg_3071" n="HIAT:ip">.</nts>
                  <nts id="Seg_3072" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T803" id="Seg_3074" n="HIAT:u" s="T801">
                  <ts e="T802" id="Seg_3076" n="HIAT:w" s="T801">ɨjdara</ts>
                  <nts id="Seg_3077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_3079" n="HIAT:w" s="T802">baranna</ts>
                  <nts id="Seg_3080" n="HIAT:ip">.</nts>
                  <nts id="Seg_3081" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T807" id="Seg_3083" n="HIAT:u" s="T803">
                  <ts e="T804" id="Seg_3085" n="HIAT:w" s="T803">Nöŋü͡ö</ts>
                  <nts id="Seg_3086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3088" n="HIAT:w" s="T804">ɨj</ts>
                  <nts id="Seg_3089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_3091" n="HIAT:w" s="T805">ortoto</ts>
                  <nts id="Seg_3092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3094" n="HIAT:w" s="T806">bu͡olla</ts>
                  <nts id="Seg_3095" n="HIAT:ip">.</nts>
                  <nts id="Seg_3096" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T812" id="Seg_3098" n="HIAT:u" s="T807">
                  <ts e="T808" id="Seg_3100" n="HIAT:w" s="T807">Emi͡e</ts>
                  <nts id="Seg_3101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3103" n="HIAT:w" s="T808">kihini</ts>
                  <nts id="Seg_3104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3106" n="HIAT:w" s="T809">kördüler</ts>
                  <nts id="Seg_3107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3109" n="HIAT:w" s="T810">biːr</ts>
                  <nts id="Seg_3110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3112" n="HIAT:w" s="T811">dʼirbiːkeːŋŋe</ts>
                  <nts id="Seg_3113" n="HIAT:ip">.</nts>
                  <nts id="Seg_3114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T814" id="Seg_3116" n="HIAT:u" s="T812">
                  <ts e="T813" id="Seg_3118" n="HIAT:w" s="T812">Emi͡e</ts>
                  <nts id="Seg_3119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_3121" n="HIAT:w" s="T813">doroːbolohor</ts>
                  <nts id="Seg_3122" n="HIAT:ip">.</nts>
                  <nts id="Seg_3123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T818" id="Seg_3125" n="HIAT:u" s="T814">
                  <nts id="Seg_3126" n="HIAT:ip">"</nts>
                  <ts e="T815" id="Seg_3128" n="HIAT:w" s="T814">Kaja</ts>
                  <nts id="Seg_3129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3131" n="HIAT:w" s="T815">di͡eki</ts>
                  <nts id="Seg_3132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_3134" n="HIAT:w" s="T816">dʼi͡eleːk-u͡ottaːk</ts>
                  <nts id="Seg_3135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3137" n="HIAT:w" s="T817">kihiginij</ts>
                  <nts id="Seg_3138" n="HIAT:ip">?</nts>
                  <nts id="Seg_3139" n="HIAT:ip">"</nts>
                  <nts id="Seg_3140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T820" id="Seg_3142" n="HIAT:u" s="T818">
                  <nts id="Seg_3143" n="HIAT:ip">"</nts>
                  <ts e="T819" id="Seg_3145" n="HIAT:w" s="T818">Dʼi͡e-u͡ot</ts>
                  <nts id="Seg_3146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_3148" n="HIAT:w" s="T819">hu͡ok</ts>
                  <nts id="Seg_3149" n="HIAT:ip">.</nts>
                  <nts id="Seg_3150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T822" id="Seg_3152" n="HIAT:u" s="T820">
                  <ts e="T821" id="Seg_3154" n="HIAT:w" s="T820">Hɨldʼabɨn</ts>
                  <nts id="Seg_3155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3157" n="HIAT:w" s="T821">agaj</ts>
                  <nts id="Seg_3158" n="HIAT:ip">.</nts>
                  <nts id="Seg_3159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T824" id="Seg_3161" n="HIAT:u" s="T822">
                  <ts e="T823" id="Seg_3163" n="HIAT:w" s="T822">Ubajbɨn</ts>
                  <nts id="Seg_3164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_3166" n="HIAT:w" s="T823">kördüːbün</ts>
                  <nts id="Seg_3167" n="HIAT:ip">.</nts>
                  <nts id="Seg_3168" n="HIAT:ip">"</nts>
                  <nts id="Seg_3169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T827" id="Seg_3171" n="HIAT:u" s="T824">
                  <nts id="Seg_3172" n="HIAT:ip">"</nts>
                  <ts e="T825" id="Seg_3174" n="HIAT:w" s="T824">Tu͡ok</ts>
                  <nts id="Seg_3175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3177" n="HIAT:w" s="T825">etej</ts>
                  <nts id="Seg_3178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3180" n="HIAT:w" s="T826">ubajɨn</ts>
                  <nts id="Seg_3181" n="HIAT:ip">?</nts>
                  <nts id="Seg_3182" n="HIAT:ip">"</nts>
                  <nts id="Seg_3183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T831" id="Seg_3185" n="HIAT:u" s="T827">
                  <nts id="Seg_3186" n="HIAT:ip">"</nts>
                  <ts e="T828" id="Seg_3188" n="HIAT:w" s="T827">Eː</ts>
                  <nts id="Seg_3189" n="HIAT:ip">,</nts>
                  <nts id="Seg_3190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3192" n="HIAT:w" s="T828">hette</ts>
                  <nts id="Seg_3193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3195" n="HIAT:w" s="T829">timek</ts>
                  <nts id="Seg_3196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_3198" n="HIAT:w" s="T830">iččite</ts>
                  <nts id="Seg_3199" n="HIAT:ip">!</nts>
                  <nts id="Seg_3200" n="HIAT:ip">"</nts>
                  <nts id="Seg_3201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T837" id="Seg_3203" n="HIAT:u" s="T831">
                  <nts id="Seg_3204" n="HIAT:ip">"</nts>
                  <ts e="T832" id="Seg_3206" n="HIAT:w" s="T831">Eː</ts>
                  <nts id="Seg_3207" n="HIAT:ip">,</nts>
                  <nts id="Seg_3208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3210" n="HIAT:w" s="T832">ol</ts>
                  <nts id="Seg_3211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3213" n="HIAT:w" s="T833">delemičeː</ts>
                  <nts id="Seg_3214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3216" n="HIAT:w" s="T834">üːren</ts>
                  <nts id="Seg_3217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3219" n="HIAT:w" s="T835">iher</ts>
                  <nts id="Seg_3220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_3222" n="HIAT:w" s="T836">ubajɨŋ</ts>
                  <nts id="Seg_3223" n="HIAT:ip">.</nts>
                  <nts id="Seg_3224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T841" id="Seg_3226" n="HIAT:u" s="T837">
                  <ts e="T838" id="Seg_3228" n="HIAT:w" s="T837">Kaja</ts>
                  <nts id="Seg_3229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3231" n="HIAT:w" s="T838">mi͡eke</ts>
                  <nts id="Seg_3232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3234" n="HIAT:w" s="T839">kihi</ts>
                  <nts id="Seg_3235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_3237" n="HIAT:w" s="T840">bu͡olu͡oŋ</ts>
                  <nts id="Seg_3238" n="HIAT:ip">?</nts>
                  <nts id="Seg_3239" n="HIAT:ip">"</nts>
                  <nts id="Seg_3240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T842" id="Seg_3242" n="HIAT:u" s="T841">
                  <ts e="T842" id="Seg_3244" n="HIAT:w" s="T841">Haŋarbat</ts>
                  <nts id="Seg_3245" n="HIAT:ip">.</nts>
                  <nts id="Seg_3246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T849" id="Seg_3248" n="HIAT:u" s="T842">
                  <nts id="Seg_3249" n="HIAT:ip">"</nts>
                  <ts e="T843" id="Seg_3251" n="HIAT:w" s="T842">Bu͡olu͡oŋ</ts>
                  <nts id="Seg_3252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3254" n="HIAT:w" s="T843">hu͡oga</ts>
                  <nts id="Seg_3255" n="HIAT:ip">,</nts>
                  <nts id="Seg_3256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3258" n="HIAT:w" s="T844">bu</ts>
                  <nts id="Seg_3259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3261" n="HIAT:w" s="T845">kürejim</ts>
                  <nts id="Seg_3262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3264" n="HIAT:w" s="T846">halaːtɨgar</ts>
                  <nts id="Seg_3265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3267" n="HIAT:w" s="T847">kaŋɨrgas</ts>
                  <nts id="Seg_3268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_3270" n="HIAT:w" s="T848">gɨnɨ͡am</ts>
                  <nts id="Seg_3271" n="HIAT:ip">!</nts>
                  <nts id="Seg_3272" n="HIAT:ip">"</nts>
                  <nts id="Seg_3273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T852" id="Seg_3275" n="HIAT:u" s="T849">
                  <ts e="T850" id="Seg_3277" n="HIAT:w" s="T849">Höbüleher</ts>
                  <nts id="Seg_3278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3280" n="HIAT:w" s="T850">bili</ts>
                  <nts id="Seg_3281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3283" n="HIAT:w" s="T851">kihi</ts>
                  <nts id="Seg_3284" n="HIAT:ip">.</nts>
                  <nts id="Seg_3285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T854" id="Seg_3287" n="HIAT:u" s="T852">
                  <ts e="T853" id="Seg_3289" n="HIAT:w" s="T852">Ubajɨgar</ts>
                  <nts id="Seg_3290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3292" n="HIAT:w" s="T853">tijer</ts>
                  <nts id="Seg_3293" n="HIAT:ip">:</nts>
                  <nts id="Seg_3294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T856" id="Seg_3296" n="HIAT:u" s="T854">
                  <nts id="Seg_3297" n="HIAT:ip">"</nts>
                  <ts e="T855" id="Seg_3299" n="HIAT:w" s="T854">Togo</ts>
                  <nts id="Seg_3300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3302" n="HIAT:w" s="T855">ölörbökküt</ts>
                  <nts id="Seg_3303" n="HIAT:ip">?</nts>
                  <nts id="Seg_3304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T860" id="Seg_3306" n="HIAT:u" s="T856">
                  <ts e="T857" id="Seg_3308" n="HIAT:w" s="T856">Ogogo</ts>
                  <nts id="Seg_3309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3311" n="HIAT:w" s="T857">bas</ts>
                  <nts id="Seg_3312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_3314" n="HIAT:w" s="T858">berinegit</ts>
                  <nts id="Seg_3315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3317" n="HIAT:w" s="T859">du͡o</ts>
                  <nts id="Seg_3318" n="HIAT:ip">?</nts>
                  <nts id="Seg_3319" n="HIAT:ip">"</nts>
                  <nts id="Seg_3320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T862" id="Seg_3322" n="HIAT:u" s="T860">
                  <nts id="Seg_3323" n="HIAT:ip">"</nts>
                  <ts e="T861" id="Seg_3325" n="HIAT:w" s="T860">Hu͡ok</ts>
                  <nts id="Seg_3326" n="HIAT:ip">,</nts>
                  <nts id="Seg_3327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_3329" n="HIAT:w" s="T861">kebis</ts>
                  <nts id="Seg_3330" n="HIAT:ip">.</nts>
                  <nts id="Seg_3331" n="HIAT:ip">"</nts>
                  <nts id="Seg_3332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T864" id="Seg_3334" n="HIAT:u" s="T862">
                  <nts id="Seg_3335" n="HIAT:ip">"</nts>
                  <ts e="T863" id="Seg_3337" n="HIAT:w" s="T862">Bultu͡okka</ts>
                  <nts id="Seg_3338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3340" n="HIAT:w" s="T863">naːda</ts>
                  <nts id="Seg_3341" n="HIAT:ip">.</nts>
                  <nts id="Seg_3342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T867" id="Seg_3344" n="HIAT:u" s="T864">
                  <ts e="T865" id="Seg_3346" n="HIAT:w" s="T864">Kohuta</ts>
                  <nts id="Seg_3347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3349" n="HIAT:w" s="T865">da</ts>
                  <nts id="Seg_3350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3352" n="HIAT:w" s="T866">bert</ts>
                  <nts id="Seg_3353" n="HIAT:ip">.</nts>
                  <nts id="Seg_3354" n="HIAT:ip">"</nts>
                  <nts id="Seg_3355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T869" id="Seg_3357" n="HIAT:u" s="T867">
                  <nts id="Seg_3358" n="HIAT:ip">"</nts>
                  <ts e="T868" id="Seg_3360" n="HIAT:w" s="T867">Če</ts>
                  <nts id="Seg_3361" n="HIAT:ip">,</nts>
                  <nts id="Seg_3362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_3364" n="HIAT:w" s="T868">bagalɨ͡akpɨt</ts>
                  <nts id="Seg_3365" n="HIAT:ip">!</nts>
                  <nts id="Seg_3366" n="HIAT:ip">"</nts>
                  <nts id="Seg_3367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T873" id="Seg_3369" n="HIAT:u" s="T869">
                  <ts e="T870" id="Seg_3371" n="HIAT:w" s="T869">Ki͡ehe</ts>
                  <nts id="Seg_3372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_3374" n="HIAT:w" s="T870">tüheller</ts>
                  <nts id="Seg_3375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3377" n="HIAT:w" s="T871">biːr</ts>
                  <nts id="Seg_3378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3380" n="HIAT:w" s="T872">hirge</ts>
                  <nts id="Seg_3381" n="HIAT:ip">.</nts>
                  <nts id="Seg_3382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T880" id="Seg_3384" n="HIAT:u" s="T873">
                  <ts e="T874" id="Seg_3386" n="HIAT:w" s="T873">Ahɨːllar</ts>
                  <nts id="Seg_3387" n="HIAT:ip">,</nts>
                  <nts id="Seg_3388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_3390" n="HIAT:w" s="T874">u͡ol</ts>
                  <nts id="Seg_3391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3393" n="HIAT:w" s="T875">dʼonun</ts>
                  <nts id="Seg_3394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_3396" n="HIAT:w" s="T876">batan</ts>
                  <nts id="Seg_3397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3399" n="HIAT:w" s="T877">tahaːrar</ts>
                  <nts id="Seg_3400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3402" n="HIAT:w" s="T878">ü͡ör</ts>
                  <nts id="Seg_3403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_3405" n="HIAT:w" s="T879">ketete</ts>
                  <nts id="Seg_3406" n="HIAT:ip">.</nts>
                  <nts id="Seg_3407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T883" id="Seg_3409" n="HIAT:u" s="T880">
                  <ts e="T881" id="Seg_3411" n="HIAT:w" s="T880">Ogo</ts>
                  <nts id="Seg_3412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3414" n="HIAT:w" s="T881">utujan</ts>
                  <nts id="Seg_3415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3417" n="HIAT:w" s="T882">kaːlar</ts>
                  <nts id="Seg_3418" n="HIAT:ip">.</nts>
                  <nts id="Seg_3419" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T887" id="Seg_3421" n="HIAT:u" s="T883">
                  <ts e="T884" id="Seg_3423" n="HIAT:w" s="T883">Kihiler</ts>
                  <nts id="Seg_3424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_3426" n="HIAT:w" s="T884">kalɨjar</ts>
                  <nts id="Seg_3427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3429" n="HIAT:w" s="T885">haŋalarɨn</ts>
                  <nts id="Seg_3430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_3432" n="HIAT:w" s="T886">ister</ts>
                  <nts id="Seg_3433" n="HIAT:ip">.</nts>
                  <nts id="Seg_3434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T891" id="Seg_3436" n="HIAT:u" s="T887">
                  <ts e="T888" id="Seg_3438" n="HIAT:w" s="T887">Kihi</ts>
                  <nts id="Seg_3439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_3441" n="HIAT:w" s="T888">batɨjannan</ts>
                  <nts id="Seg_3442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3444" n="HIAT:w" s="T889">aːnɨ</ts>
                  <nts id="Seg_3445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3447" n="HIAT:w" s="T890">hegeter</ts>
                  <nts id="Seg_3448" n="HIAT:ip">:</nts>
                  <nts id="Seg_3449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T897" id="Seg_3451" n="HIAT:u" s="T891">
                  <nts id="Seg_3452" n="HIAT:ip">"</nts>
                  <ts e="T892" id="Seg_3454" n="HIAT:w" s="T891">Kaja</ts>
                  <nts id="Seg_3455" n="HIAT:ip">,</nts>
                  <nts id="Seg_3456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_3458" n="HIAT:w" s="T892">togo</ts>
                  <nts id="Seg_3459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_3461" n="HIAT:w" s="T893">ketiːgit</ts>
                  <nts id="Seg_3462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_3464" n="HIAT:w" s="T894">miːgin</ts>
                  <nts id="Seg_3465" n="HIAT:ip">,</nts>
                  <nts id="Seg_3466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3468" n="HIAT:w" s="T895">ü͡örü</ts>
                  <nts id="Seg_3469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3471" n="HIAT:w" s="T896">keteːŋ</ts>
                  <nts id="Seg_3472" n="HIAT:ip">!</nts>
                  <nts id="Seg_3473" n="HIAT:ip">"</nts>
                  <nts id="Seg_3474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T900" id="Seg_3476" n="HIAT:u" s="T897">
                  <ts e="T898" id="Seg_3478" n="HIAT:w" s="T897">Dʼon</ts>
                  <nts id="Seg_3479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3481" n="HIAT:w" s="T898">töttörü</ts>
                  <nts id="Seg_3482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_3484" n="HIAT:w" s="T899">hahallar</ts>
                  <nts id="Seg_3485" n="HIAT:ip">.</nts>
                  <nts id="Seg_3486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T904" id="Seg_3488" n="HIAT:u" s="T900">
                  <nts id="Seg_3489" n="HIAT:ip">"</nts>
                  <ts e="T901" id="Seg_3491" n="HIAT:w" s="T900">Harsi͡erdaːŋŋɨ</ts>
                  <nts id="Seg_3492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3494" n="HIAT:w" s="T901">uːtugar</ts>
                  <nts id="Seg_3495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3497" n="HIAT:w" s="T902">bagalɨ͡akpɨt</ts>
                  <nts id="Seg_3498" n="HIAT:ip">"</nts>
                  <nts id="Seg_3499" n="HIAT:ip">,</nts>
                  <nts id="Seg_3500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_3502" n="HIAT:w" s="T903">deheller</ts>
                  <nts id="Seg_3503" n="HIAT:ip">.</nts>
                  <nts id="Seg_3504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T906" id="Seg_3506" n="HIAT:u" s="T904">
                  <ts e="T905" id="Seg_3508" n="HIAT:w" s="T904">Emi͡e</ts>
                  <nts id="Seg_3509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3511" n="HIAT:w" s="T905">bürüːrdüːller</ts>
                  <nts id="Seg_3512" n="HIAT:ip">.</nts>
                  <nts id="Seg_3513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T909" id="Seg_3515" n="HIAT:u" s="T906">
                  <ts e="T907" id="Seg_3517" n="HIAT:w" s="T906">Emi͡e</ts>
                  <nts id="Seg_3518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3519" n="HIAT:ip">"</nts>
                  <ts e="T908" id="Seg_3521" n="HIAT:w" s="T907">tugu</ts>
                  <nts id="Seg_3522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_3524" n="HIAT:w" s="T908">ketiːgit</ts>
                  <nts id="Seg_3525" n="HIAT:ip">?</nts>
                  <nts id="Seg_3526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T912" id="Seg_3528" n="HIAT:u" s="T909">
                  <ts e="T910" id="Seg_3530" n="HIAT:w" s="T909">Barɨŋ</ts>
                  <nts id="Seg_3531" n="HIAT:ip">"</nts>
                  <nts id="Seg_3532" n="HIAT:ip">,</nts>
                  <nts id="Seg_3533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_3535" n="HIAT:w" s="T910">batan</ts>
                  <nts id="Seg_3536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_3538" n="HIAT:w" s="T911">ɨːtar</ts>
                  <nts id="Seg_3539" n="HIAT:ip">.</nts>
                  <nts id="Seg_3540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T914" id="Seg_3542" n="HIAT:u" s="T912">
                  <ts e="T913" id="Seg_3544" n="HIAT:w" s="T912">Honton</ts>
                  <nts id="Seg_3545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_3547" n="HIAT:w" s="T913">tɨːppataktara</ts>
                  <nts id="Seg_3548" n="HIAT:ip">.</nts>
                  <nts id="Seg_3549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T920" id="Seg_3551" n="HIAT:u" s="T914">
                  <ts e="T915" id="Seg_3553" n="HIAT:w" s="T914">Ikki</ts>
                  <nts id="Seg_3554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3556" n="HIAT:w" s="T915">ɨjdara</ts>
                  <nts id="Seg_3557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_3559" n="HIAT:w" s="T916">bɨstarɨgar</ts>
                  <nts id="Seg_3560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3562" n="HIAT:w" s="T917">dʼirbiːlere</ts>
                  <nts id="Seg_3563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_3565" n="HIAT:w" s="T918">elete</ts>
                  <nts id="Seg_3566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3568" n="HIAT:w" s="T919">bu͡olar</ts>
                  <nts id="Seg_3569" n="HIAT:ip">.</nts>
                  <nts id="Seg_3570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T923" id="Seg_3572" n="HIAT:u" s="T920">
                  <ts e="T921" id="Seg_3574" n="HIAT:w" s="T920">Körbüte</ts>
                  <nts id="Seg_3575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3576" n="HIAT:ip">—</nts>
                  <nts id="Seg_3577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3579" n="HIAT:w" s="T921">hir</ts>
                  <nts id="Seg_3580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_3582" n="HIAT:w" s="T922">bu͡olbut</ts>
                  <nts id="Seg_3583" n="HIAT:ip">.</nts>
                  <nts id="Seg_3584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T926" id="Seg_3586" n="HIAT:u" s="T923">
                  <ts e="T924" id="Seg_3588" n="HIAT:w" s="T923">Kaːr</ts>
                  <nts id="Seg_3589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_3591" n="HIAT:w" s="T924">annɨttan</ts>
                  <nts id="Seg_3592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_3594" n="HIAT:w" s="T925">buru͡okaːkɨna</ts>
                  <nts id="Seg_3595" n="HIAT:ip">.</nts>
                  <nts id="Seg_3596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T932" id="Seg_3598" n="HIAT:u" s="T926">
                  <ts e="T927" id="Seg_3600" n="HIAT:w" s="T926">Mu͡ora</ts>
                  <nts id="Seg_3601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_3603" n="HIAT:w" s="T927">di͡ekkitten</ts>
                  <nts id="Seg_3604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_3606" n="HIAT:w" s="T928">kütür</ts>
                  <nts id="Seg_3607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_3609" n="HIAT:w" s="T929">bagajɨ</ts>
                  <nts id="Seg_3610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_3612" n="HIAT:w" s="T930">orok</ts>
                  <nts id="Seg_3613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_3615" n="HIAT:w" s="T931">keler</ts>
                  <nts id="Seg_3616" n="HIAT:ip">.</nts>
                  <nts id="Seg_3617" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T937" id="Seg_3619" n="HIAT:u" s="T932">
                  <ts e="T933" id="Seg_3621" n="HIAT:w" s="T932">Hir</ts>
                  <nts id="Seg_3622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_3624" n="HIAT:w" s="T933">ihin</ts>
                  <nts id="Seg_3625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3627" n="HIAT:w" s="T934">di͡eki</ts>
                  <nts id="Seg_3628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_3630" n="HIAT:w" s="T935">barar</ts>
                  <nts id="Seg_3631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_3633" n="HIAT:w" s="T936">orok</ts>
                  <nts id="Seg_3634" n="HIAT:ip">.</nts>
                  <nts id="Seg_3635" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T939" id="Seg_3637" n="HIAT:u" s="T937">
                  <ts e="T938" id="Seg_3639" n="HIAT:w" s="T937">Baran</ts>
                  <nts id="Seg_3640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T939" id="Seg_3642" n="HIAT:w" s="T938">iheller</ts>
                  <nts id="Seg_3643" n="HIAT:ip">.</nts>
                  <nts id="Seg_3644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T946" id="Seg_3646" n="HIAT:u" s="T939">
                  <ts e="T940" id="Seg_3648" n="HIAT:w" s="T939">Pireːme</ts>
                  <nts id="Seg_3649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_3651" n="HIAT:w" s="T940">noru͡ottan</ts>
                  <nts id="Seg_3652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_3654" n="HIAT:w" s="T941">noru͡ot</ts>
                  <nts id="Seg_3655" n="HIAT:ip">,</nts>
                  <nts id="Seg_3656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_3658" n="HIAT:w" s="T942">dʼi͡etten</ts>
                  <nts id="Seg_3659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_3661" n="HIAT:w" s="T943">dʼi͡e</ts>
                  <nts id="Seg_3662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T945" id="Seg_3664" n="HIAT:w" s="T944">hirge</ts>
                  <nts id="Seg_3665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_3667" n="HIAT:w" s="T945">keleller</ts>
                  <nts id="Seg_3668" n="HIAT:ip">.</nts>
                  <nts id="Seg_3669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T950" id="Seg_3671" n="HIAT:u" s="T946">
                  <ts e="T947" id="Seg_3673" n="HIAT:w" s="T946">Biːr</ts>
                  <nts id="Seg_3674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_3676" n="HIAT:w" s="T947">dʼi͡e</ts>
                  <nts id="Seg_3677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_3679" n="HIAT:w" s="T948">tahɨgar</ts>
                  <nts id="Seg_3680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_3682" n="HIAT:w" s="T949">toktuːr</ts>
                  <nts id="Seg_3683" n="HIAT:ip">.</nts>
                  <nts id="Seg_3684" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T955" id="Seg_3686" n="HIAT:u" s="T950">
                  <ts e="T951" id="Seg_3688" n="HIAT:w" s="T950">Biːr</ts>
                  <nts id="Seg_3689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T952" id="Seg_3691" n="HIAT:w" s="T951">kɨrdʼagas</ts>
                  <nts id="Seg_3692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_3694" n="HIAT:w" s="T952">hogus</ts>
                  <nts id="Seg_3695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_3697" n="HIAT:w" s="T953">dʼaktar</ts>
                  <nts id="Seg_3698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_3700" n="HIAT:w" s="T954">taksar</ts>
                  <nts id="Seg_3701" n="HIAT:ip">:</nts>
                  <nts id="Seg_3702" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T959" id="Seg_3704" n="HIAT:u" s="T955">
                  <nts id="Seg_3705" n="HIAT:ip">"</nts>
                  <ts e="T956" id="Seg_3707" n="HIAT:w" s="T955">Baj</ts>
                  <nts id="Seg_3708" n="HIAT:ip">,</nts>
                  <nts id="Seg_3709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_3711" n="HIAT:w" s="T956">tabata</ts>
                  <nts id="Seg_3712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_3714" n="HIAT:w" s="T957">oduː</ts>
                  <nts id="Seg_3715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_3717" n="HIAT:w" s="T958">taba</ts>
                  <nts id="Seg_3718" n="HIAT:ip">!</nts>
                  <nts id="Seg_3719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T963" id="Seg_3721" n="HIAT:u" s="T959">
                  <ts e="T960" id="Seg_3723" n="HIAT:w" s="T959">Bihigi</ts>
                  <nts id="Seg_3724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_3726" n="HIAT:w" s="T960">törüt</ts>
                  <nts id="Seg_3727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_3729" n="HIAT:w" s="T961">tababɨt</ts>
                  <nts id="Seg_3730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_3732" n="HIAT:w" s="T962">kurduguj</ts>
                  <nts id="Seg_3733" n="HIAT:ip">?</nts>
                  <nts id="Seg_3734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T970" id="Seg_3736" n="HIAT:u" s="T963">
                  <ts e="T964" id="Seg_3738" n="HIAT:w" s="T963">Bɨlɨr</ts>
                  <nts id="Seg_3739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3741" n="HIAT:w" s="T964">ikki</ts>
                  <nts id="Seg_3742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_3744" n="HIAT:w" s="T965">kihi</ts>
                  <nts id="Seg_3745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_3747" n="HIAT:w" s="T966">barbɨta</ts>
                  <nts id="Seg_3748" n="HIAT:ip">,</nts>
                  <nts id="Seg_3749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_3751" n="HIAT:w" s="T967">ol</ts>
                  <nts id="Seg_3752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_3754" n="HIAT:w" s="T968">törüttere</ts>
                  <nts id="Seg_3755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T970" id="Seg_3757" n="HIAT:w" s="T969">bu͡olu͡o</ts>
                  <nts id="Seg_3758" n="HIAT:ip">!</nts>
                  <nts id="Seg_3759" n="HIAT:ip">"</nts>
                  <nts id="Seg_3760" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T981" id="Seg_3762" n="HIAT:u" s="T970">
                  <ts e="T971" id="Seg_3764" n="HIAT:w" s="T970">Üs</ts>
                  <nts id="Seg_3765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_3767" n="HIAT:w" s="T971">dʼi͡etten</ts>
                  <nts id="Seg_3768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_3770" n="HIAT:w" s="T972">üs</ts>
                  <nts id="Seg_3771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_3773" n="HIAT:w" s="T973">dʼaktar</ts>
                  <nts id="Seg_3774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_3776" n="HIAT:w" s="T974">taksan</ts>
                  <nts id="Seg_3777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_3779" n="HIAT:w" s="T975">emi͡e</ts>
                  <nts id="Seg_3780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_3782" n="HIAT:w" s="T976">taːjbɨttar</ts>
                  <nts id="Seg_3783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_3785" n="HIAT:w" s="T977">törütterin</ts>
                  <nts id="Seg_3786" n="HIAT:ip">,</nts>
                  <nts id="Seg_3787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_3789" n="HIAT:w" s="T978">kepseten</ts>
                  <nts id="Seg_3790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_3792" n="HIAT:w" s="T979">dʼe</ts>
                  <nts id="Seg_3793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T981" id="Seg_3795" n="HIAT:w" s="T980">bilseller</ts>
                  <nts id="Seg_3796" n="HIAT:ip">.</nts>
                  <nts id="Seg_3797" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T983" id="Seg_3799" n="HIAT:u" s="T981">
                  <ts e="T982" id="Seg_3801" n="HIAT:w" s="T981">Uruːlarɨn</ts>
                  <nts id="Seg_3802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_3804" n="HIAT:w" s="T982">bulsallar</ts>
                  <nts id="Seg_3805" n="HIAT:ip">.</nts>
                  <nts id="Seg_3806" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T985" id="Seg_3808" n="HIAT:u" s="T983">
                  <ts e="T984" id="Seg_3810" n="HIAT:w" s="T983">Kuŋadʼajɨ</ts>
                  <nts id="Seg_3811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T985" id="Seg_3813" n="HIAT:w" s="T984">küːteller</ts>
                  <nts id="Seg_3814" n="HIAT:ip">.</nts>
                  <nts id="Seg_3815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T990" id="Seg_3817" n="HIAT:u" s="T985">
                  <ts e="T986" id="Seg_3819" n="HIAT:w" s="T985">Ühüs</ts>
                  <nts id="Seg_3820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T987" id="Seg_3822" n="HIAT:w" s="T986">künün</ts>
                  <nts id="Seg_3823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T988" id="Seg_3825" n="HIAT:w" s="T987">ortoto</ts>
                  <nts id="Seg_3826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T989" id="Seg_3828" n="HIAT:w" s="T988">Kuŋadʼaj</ts>
                  <nts id="Seg_3829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_3831" n="HIAT:w" s="T989">keler</ts>
                  <nts id="Seg_3832" n="HIAT:ip">.</nts>
                  <nts id="Seg_3833" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T997" id="Seg_3835" n="HIAT:u" s="T990">
                  <ts e="T991" id="Seg_3837" n="HIAT:w" s="T990">Et</ts>
                  <nts id="Seg_3838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_3840" n="HIAT:w" s="T991">aːta</ts>
                  <nts id="Seg_3841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T993" id="Seg_3843" n="HIAT:w" s="T992">bütüne</ts>
                  <nts id="Seg_3844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T994" id="Seg_3846" n="HIAT:w" s="T993">hu͡ok</ts>
                  <nts id="Seg_3847" n="HIAT:ip">,</nts>
                  <nts id="Seg_3848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_3850" n="HIAT:w" s="T994">tiriːtin</ts>
                  <nts id="Seg_3851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_3853" n="HIAT:w" s="T995">aŋara</ts>
                  <nts id="Seg_3854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T997" id="Seg_3856" n="HIAT:w" s="T996">kaːlbɨt</ts>
                  <nts id="Seg_3857" n="HIAT:ip">.</nts>
                  <nts id="Seg_3858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1001" id="Seg_3860" n="HIAT:u" s="T997">
                  <nts id="Seg_3861" n="HIAT:ip">"</nts>
                  <ts e="T998" id="Seg_3863" n="HIAT:w" s="T997">Ogom</ts>
                  <nts id="Seg_3864" n="HIAT:ip">,</nts>
                  <nts id="Seg_3865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_3867" n="HIAT:w" s="T998">kelbikkin</ts>
                  <nts id="Seg_3868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3869" n="HIAT:ip">—</nts>
                  <nts id="Seg_3870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1000" id="Seg_3872" n="HIAT:w" s="T999">dʼe</ts>
                  <nts id="Seg_3873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1001" id="Seg_3875" n="HIAT:w" s="T1000">üčügej</ts>
                  <nts id="Seg_3876" n="HIAT:ip">!</nts>
                  <nts id="Seg_3877" n="HIAT:ip">"</nts>
                  <nts id="Seg_3878" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1008" id="Seg_3880" n="HIAT:u" s="T1001">
                  <ts e="T1002" id="Seg_3882" n="HIAT:w" s="T1001">Kuhagan</ts>
                  <nts id="Seg_3883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_3885" n="HIAT:w" s="T1002">dʼonu</ts>
                  <nts id="Seg_3886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1004" id="Seg_3888" n="HIAT:w" s="T1003">baraːn</ts>
                  <nts id="Seg_3889" n="HIAT:ip">,</nts>
                  <nts id="Seg_3890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1005" id="Seg_3892" n="HIAT:w" s="T1004">dojdu</ts>
                  <nts id="Seg_3893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1006" id="Seg_3895" n="HIAT:w" s="T1005">buldun</ts>
                  <nts id="Seg_3896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1007" id="Seg_3898" n="HIAT:w" s="T1006">arɨčča</ts>
                  <nts id="Seg_3899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1008" id="Seg_3901" n="HIAT:w" s="T1007">kɨ͡ajdɨm</ts>
                  <nts id="Seg_3902" n="HIAT:ip">.</nts>
                  <nts id="Seg_3903" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1013" id="Seg_3905" n="HIAT:u" s="T1008">
                  <ts e="T1009" id="Seg_3907" n="HIAT:w" s="T1008">Tɨːnɨm</ts>
                  <nts id="Seg_3908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1010" id="Seg_3910" n="HIAT:w" s="T1009">ere</ts>
                  <nts id="Seg_3911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1011" id="Seg_3913" n="HIAT:w" s="T1010">kellim</ts>
                  <nts id="Seg_3914" n="HIAT:ip">,</nts>
                  <nts id="Seg_3915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1012" id="Seg_3917" n="HIAT:w" s="T1011">gojobuːn</ts>
                  <nts id="Seg_3918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1013" id="Seg_3920" n="HIAT:w" s="T1012">oŋoronnor</ts>
                  <nts id="Seg_3921" n="HIAT:ip">.</nts>
                  <nts id="Seg_3922" n="HIAT:ip">"</nts>
                  <nts id="Seg_3923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1018" id="Seg_3925" n="HIAT:u" s="T1013">
                  <nts id="Seg_3926" n="HIAT:ip">"</nts>
                  <ts e="T1014" id="Seg_3928" n="HIAT:w" s="T1013">Dʼe</ts>
                  <nts id="Seg_3929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1015" id="Seg_3931" n="HIAT:w" s="T1014">olor</ts>
                  <nts id="Seg_3932" n="HIAT:ip">,</nts>
                  <nts id="Seg_3933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1016" id="Seg_3935" n="HIAT:w" s="T1015">tu͡oktan</ts>
                  <nts id="Seg_3936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1017" id="Seg_3938" n="HIAT:w" s="T1016">da</ts>
                  <nts id="Seg_3939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1018" id="Seg_3941" n="HIAT:w" s="T1017">kuttanɨma</ts>
                  <nts id="Seg_3942" n="HIAT:ip">!</nts>
                  <nts id="Seg_3943" n="HIAT:ip">"</nts>
                  <nts id="Seg_3944" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1022" id="Seg_3946" n="HIAT:u" s="T1018">
                  <ts e="T1019" id="Seg_3948" n="HIAT:w" s="T1018">Kuŋadʼaj</ts>
                  <nts id="Seg_3949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1020" id="Seg_3951" n="HIAT:w" s="T1019">dʼe</ts>
                  <nts id="Seg_3952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1021" id="Seg_3954" n="HIAT:w" s="T1020">kojut</ts>
                  <nts id="Seg_3955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1022" id="Seg_3957" n="HIAT:w" s="T1021">tiller</ts>
                  <nts id="Seg_3958" n="HIAT:ip">.</nts>
                  <nts id="Seg_3959" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1028" id="Seg_3961" n="HIAT:u" s="T1022">
                  <ts e="T1023" id="Seg_3963" n="HIAT:w" s="T1022">U͡ol</ts>
                  <nts id="Seg_3964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1024" id="Seg_3966" n="HIAT:w" s="T1023">küseːjin</ts>
                  <nts id="Seg_3967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1025" id="Seg_3969" n="HIAT:w" s="T1024">bu͡olan</ts>
                  <nts id="Seg_3970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1026" id="Seg_3972" n="HIAT:w" s="T1025">olorbuta</ts>
                  <nts id="Seg_3973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1027" id="Seg_3975" n="HIAT:w" s="T1026">bu</ts>
                  <nts id="Seg_3976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1028" id="Seg_3978" n="HIAT:w" s="T1027">omukka</ts>
                  <nts id="Seg_3979" n="HIAT:ip">.</nts>
                  <nts id="Seg_3980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1029" id="Seg_3982" n="HIAT:u" s="T1028">
                  <ts e="T1029" id="Seg_3984" n="HIAT:w" s="T1028">Elete</ts>
                  <nts id="Seg_3985" n="HIAT:ip">.</nts>
                  <nts id="Seg_3986" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1029" id="Seg_3987" n="sc" s="T0">
               <ts e="T1" id="Seg_3989" n="e" s="T0">Üs </ts>
               <ts e="T2" id="Seg_3991" n="e" s="T1">inibiːler </ts>
               <ts e="T3" id="Seg_3993" n="e" s="T2">olorbuttar. </ts>
               <ts e="T4" id="Seg_3995" n="e" s="T3">Biːrdere </ts>
               <ts e="T5" id="Seg_3997" n="e" s="T4">kɨra </ts>
               <ts e="T6" id="Seg_3999" n="e" s="T5">kuččukkaːn </ts>
               <ts e="T7" id="Seg_4001" n="e" s="T6">kihi, </ts>
               <ts e="T8" id="Seg_4003" n="e" s="T7">ortoloro </ts>
               <ts e="T9" id="Seg_4005" n="e" s="T8">kaːmpat, </ts>
               <ts e="T10" id="Seg_4007" n="e" s="T9">tobugunan </ts>
               <ts e="T11" id="Seg_4009" n="e" s="T10">agaj </ts>
               <ts e="T12" id="Seg_4011" n="e" s="T11">hɨːlar </ts>
               <ts e="T13" id="Seg_4013" n="e" s="T12">Kuŋadʼaj </ts>
               <ts e="T14" id="Seg_4015" n="e" s="T13">di͡en. </ts>
               <ts e="T15" id="Seg_4017" n="e" s="T14">Ubajdara </ts>
               <ts e="T16" id="Seg_4019" n="e" s="T15">aːta </ts>
               <ts e="T17" id="Seg_4021" n="e" s="T16">hu͡ok, </ts>
               <ts e="T18" id="Seg_4023" n="e" s="T17">bulčut, </ts>
               <ts e="T19" id="Seg_4025" n="e" s="T18">dʼaktardaːk, </ts>
               <ts e="T20" id="Seg_4027" n="e" s="T19">biːr </ts>
               <ts e="T21" id="Seg_4029" n="e" s="T20">hahɨl </ts>
               <ts e="T22" id="Seg_4031" n="e" s="T21">ogoloːk. </ts>
               <ts e="T23" id="Seg_4033" n="e" s="T22">Tabalara </ts>
               <ts e="T24" id="Seg_4035" n="e" s="T23">hu͡oč-hogotok, </ts>
               <ts e="T25" id="Seg_4037" n="e" s="T24">at </ts>
               <ts e="T26" id="Seg_4039" n="e" s="T25">kördük. </ts>
               <ts e="T27" id="Seg_4041" n="e" s="T26">Üjege </ts>
               <ts e="T28" id="Seg_4043" n="e" s="T27">kölümmetter, </ts>
               <ts e="T29" id="Seg_4045" n="e" s="T28">köstöktörüne </ts>
               <ts e="T30" id="Seg_4047" n="e" s="T29">ere </ts>
               <ts e="T31" id="Seg_4049" n="e" s="T30">dʼi͡elerin </ts>
               <ts e="T32" id="Seg_4051" n="e" s="T31">tartarallar. </ts>
               <ts e="T33" id="Seg_4053" n="e" s="T32">Ubajdara </ts>
               <ts e="T34" id="Seg_4055" n="e" s="T33">aːttaːk </ts>
               <ts e="T35" id="Seg_4057" n="e" s="T34">bulčut. </ts>
               <ts e="T36" id="Seg_4059" n="e" s="T35">Kɨːlɨ </ts>
               <ts e="T37" id="Seg_4061" n="e" s="T36">bultuːr, </ts>
               <ts e="T38" id="Seg_4063" n="e" s="T37">balɨgɨ </ts>
               <ts e="T39" id="Seg_4065" n="e" s="T38">bu͡ollun. </ts>
               <ts e="T40" id="Seg_4067" n="e" s="T39">Dʼɨl </ts>
               <ts e="T41" id="Seg_4069" n="e" s="T40">haːstɨjarɨn </ts>
               <ts e="T42" id="Seg_4071" n="e" s="T41">hagana </ts>
               <ts e="T43" id="Seg_4073" n="e" s="T42">ubajdara </ts>
               <ts e="T44" id="Seg_4075" n="e" s="T43">ɨ͡aldʼar, </ts>
               <ts e="T45" id="Seg_4077" n="e" s="T44">bergeːbit, </ts>
               <ts e="T46" id="Seg_4079" n="e" s="T45">ühüs </ts>
               <ts e="T47" id="Seg_4081" n="e" s="T46">künüger </ts>
               <ts e="T48" id="Seg_4083" n="e" s="T47">bu͡opsa </ts>
               <ts e="T49" id="Seg_4085" n="e" s="T48">möltöːbüt, </ts>
               <ts e="T50" id="Seg_4087" n="e" s="T49">keri͡es </ts>
               <ts e="T51" id="Seg_4089" n="e" s="T50">eppit </ts>
               <ts e="T52" id="Seg_4091" n="e" s="T51">dʼaktarɨgar: </ts>
               <ts e="T53" id="Seg_4093" n="e" s="T52">"Dʼe </ts>
               <ts e="T54" id="Seg_4095" n="e" s="T53">min </ts>
               <ts e="T55" id="Seg_4097" n="e" s="T54">öllüm. </ts>
               <ts e="T56" id="Seg_4099" n="e" s="T55">Kimiŋ </ts>
               <ts e="T57" id="Seg_4101" n="e" s="T56">karajɨ͡agaj? </ts>
               <ts e="T58" id="Seg_4103" n="e" s="T57">Bultaːbɨt </ts>
               <ts e="T59" id="Seg_4105" n="e" s="T58">bulpun </ts>
               <ts e="T60" id="Seg_4107" n="e" s="T59">hajɨnɨ </ts>
               <ts e="T61" id="Seg_4109" n="e" s="T60">bɨha </ts>
               <ts e="T62" id="Seg_4111" n="e" s="T61">ahɨ͡akkɨt. </ts>
               <ts e="T63" id="Seg_4113" n="e" s="T62">Askɨn </ts>
               <ts e="T64" id="Seg_4115" n="e" s="T63">baraːtakkɨna, </ts>
               <ts e="T65" id="Seg_4117" n="e" s="T64">ehiːl </ts>
               <ts e="T66" id="Seg_4119" n="e" s="T65">tiriːtin, </ts>
               <ts e="T67" id="Seg_4121" n="e" s="T66">tɨhɨn </ts>
               <ts e="T68" id="Seg_4123" n="e" s="T67">harɨːlaːn </ts>
               <ts e="T69" id="Seg_4125" n="e" s="T68">minneːn </ts>
               <ts e="T70" id="Seg_4127" n="e" s="T69">iheːr. </ts>
               <ts e="T71" id="Seg_4129" n="e" s="T70">Iti </ts>
               <ts e="T72" id="Seg_4131" n="e" s="T71">dʼonuŋ </ts>
               <ts e="T73" id="Seg_4133" n="e" s="T72">tuhata </ts>
               <ts e="T74" id="Seg_4135" n="e" s="T73">hu͡ok </ts>
               <ts e="T75" id="Seg_4137" n="e" s="T74">dʼon. </ts>
               <ts e="T76" id="Seg_4139" n="e" s="T75">Ölbükkünen </ts>
               <ts e="T77" id="Seg_4141" n="e" s="T76">ölögün. </ts>
               <ts e="T78" id="Seg_4143" n="e" s="T77">Ogom, </ts>
               <ts e="T79" id="Seg_4145" n="e" s="T78">bagar, </ts>
               <ts e="T80" id="Seg_4147" n="e" s="T79">kihi </ts>
               <ts e="T81" id="Seg_4149" n="e" s="T80">bu͡olu͡o, </ts>
               <ts e="T82" id="Seg_4151" n="e" s="T81">ölbötögüne, </ts>
               <ts e="T83" id="Seg_4153" n="e" s="T82">kini </ts>
               <ts e="T84" id="Seg_4155" n="e" s="T83">iːti͡e. </ts>
               <ts e="T85" id="Seg_4157" n="e" s="T84">Elete." </ts>
               <ts e="T86" id="Seg_4159" n="e" s="T85">Ölön </ts>
               <ts e="T87" id="Seg_4161" n="e" s="T86">kaːlar. </ts>
               <ts e="T88" id="Seg_4163" n="e" s="T87">ɨtana-ɨtana </ts>
               <ts e="T89" id="Seg_4165" n="e" s="T88">dʼaktar </ts>
               <ts e="T90" id="Seg_4167" n="e" s="T89">uːran </ts>
               <ts e="T91" id="Seg_4169" n="e" s="T90">keːher. </ts>
               <ts e="T92" id="Seg_4171" n="e" s="T91">Bu </ts>
               <ts e="T93" id="Seg_4173" n="e" s="T92">hajɨna </ts>
               <ts e="T94" id="Seg_4175" n="e" s="T93">bu͡olar. </ts>
               <ts e="T95" id="Seg_4177" n="e" s="T94">Astarɨn </ts>
               <ts e="T96" id="Seg_4179" n="e" s="T95">katarɨnallar. </ts>
               <ts e="T97" id="Seg_4181" n="e" s="T96">Hajɨnnarɨn </ts>
               <ts e="T98" id="Seg_4183" n="e" s="T97">bɨhallar, </ts>
               <ts e="T99" id="Seg_4185" n="e" s="T98">kühünneriger </ts>
               <ts e="T100" id="Seg_4187" n="e" s="T99">tɨstarɨn, </ts>
               <ts e="T101" id="Seg_4189" n="e" s="T100">tiriːlerin </ts>
               <ts e="T102" id="Seg_4191" n="e" s="T101">hiːller. </ts>
               <ts e="T103" id="Seg_4193" n="e" s="T102">Barɨta </ts>
               <ts e="T104" id="Seg_4195" n="e" s="T103">baranar. </ts>
               <ts e="T105" id="Seg_4197" n="e" s="T104">Kɨhɨn </ts>
               <ts e="T106" id="Seg_4199" n="e" s="T105">bu͡olar. </ts>
               <ts e="T107" id="Seg_4201" n="e" s="T106">Hi͡ek </ts>
               <ts e="T108" id="Seg_4203" n="e" s="T107">hirdere </ts>
               <ts e="T109" id="Seg_4205" n="e" s="T108">hu͡ok. </ts>
               <ts e="T110" id="Seg_4207" n="e" s="T109">Tɨ͡ala </ts>
               <ts e="T111" id="Seg_4209" n="e" s="T110">hu͡ok </ts>
               <ts e="T112" id="Seg_4211" n="e" s="T111">kün </ts>
               <ts e="T113" id="Seg_4213" n="e" s="T112">araj </ts>
               <ts e="T114" id="Seg_4215" n="e" s="T113">hɨ͡argalaːk </ts>
               <ts e="T115" id="Seg_4217" n="e" s="T114">tɨ͡ahɨn </ts>
               <ts e="T116" id="Seg_4219" n="e" s="T115">ister. </ts>
               <ts e="T117" id="Seg_4221" n="e" s="T116">Dʼaktar </ts>
               <ts e="T118" id="Seg_4223" n="e" s="T117">taksa </ts>
               <ts e="T119" id="Seg_4225" n="e" s="T118">kötör, </ts>
               <ts e="T120" id="Seg_4227" n="e" s="T119">haŋa </ts>
               <ts e="T121" id="Seg_4229" n="e" s="T120">ere </ts>
               <ts e="T122" id="Seg_4231" n="e" s="T121">ihiller </ts>
               <ts e="T123" id="Seg_4233" n="e" s="T122">hiriger </ts>
               <ts e="T124" id="Seg_4235" n="e" s="T123">toktoːbut. </ts>
               <ts e="T125" id="Seg_4237" n="e" s="T124">Araj </ts>
               <ts e="T126" id="Seg_4239" n="e" s="T125">dʼaktar </ts>
               <ts e="T127" id="Seg_4241" n="e" s="T126">agata — </ts>
               <ts e="T128" id="Seg_4243" n="e" s="T127">Agɨs </ts>
               <ts e="T129" id="Seg_4245" n="e" s="T128">konnomu͡oj </ts>
               <ts e="T130" id="Seg_4247" n="e" s="T129">iččite, </ts>
               <ts e="T131" id="Seg_4249" n="e" s="T130">küseːjine, </ts>
               <ts e="T132" id="Seg_4251" n="e" s="T131">kini͡es </ts>
               <ts e="T133" id="Seg_4253" n="e" s="T132">ebit. </ts>
               <ts e="T134" id="Seg_4255" n="e" s="T133">"Kajɨː, </ts>
               <ts e="T135" id="Seg_4257" n="e" s="T134">kajdak </ts>
               <ts e="T136" id="Seg_4259" n="e" s="T135">olorogut?" </ts>
               <ts e="T137" id="Seg_4261" n="e" s="T136">"Erim </ts>
               <ts e="T138" id="Seg_4263" n="e" s="T137">ölbüte </ts>
               <ts e="T139" id="Seg_4265" n="e" s="T138">bɨlɨrɨːn." </ts>
               <ts e="T140" id="Seg_4267" n="e" s="T139">"Kajɨː, </ts>
               <ts e="T141" id="Seg_4269" n="e" s="T140">slaːbu͡ok! </ts>
               <ts e="T142" id="Seg_4271" n="e" s="T141">Olus </ts>
               <ts e="T143" id="Seg_4273" n="e" s="T142">ü͡ördüm! </ts>
               <ts e="T144" id="Seg_4275" n="e" s="T143">Taŋɨn, </ts>
               <ts e="T145" id="Seg_4277" n="e" s="T144">barɨ͡ak </ts>
               <ts e="T146" id="Seg_4279" n="e" s="T145">min </ts>
               <ts e="T147" id="Seg_4281" n="e" s="T146">dʼi͡eber!" </ts>
               <ts e="T148" id="Seg_4283" n="e" s="T147">"Kaja </ts>
               <ts e="T149" id="Seg_4285" n="e" s="T148">ogobun, </ts>
               <ts e="T150" id="Seg_4287" n="e" s="T149">dʼommun?" </ts>
               <ts e="T151" id="Seg_4289" n="e" s="T150">"Okton </ts>
               <ts e="T152" id="Seg_4291" n="e" s="T151">öllünner, </ts>
               <ts e="T153" id="Seg_4293" n="e" s="T152">barɨ͡ak!" </ts>
               <ts e="T154" id="Seg_4295" n="e" s="T153">Dʼaktar </ts>
               <ts e="T155" id="Seg_4297" n="e" s="T154">taŋnan </ts>
               <ts e="T156" id="Seg_4299" n="e" s="T155">tijen </ts>
               <ts e="T157" id="Seg_4301" n="e" s="T156">meŋester </ts>
               <ts e="T158" id="Seg_4303" n="e" s="T157">da, </ts>
               <ts e="T159" id="Seg_4305" n="e" s="T158">kötüten </ts>
               <ts e="T160" id="Seg_4307" n="e" s="T159">kaːlallar. </ts>
               <ts e="T161" id="Seg_4309" n="e" s="T160">Kuŋadʼajdaːk </ts>
               <ts e="T162" id="Seg_4311" n="e" s="T161">kam </ts>
               <ts e="T163" id="Seg_4313" n="e" s="T162">aččɨk </ts>
               <ts e="T164" id="Seg_4315" n="e" s="T163">hɨttaktara. </ts>
               <ts e="T165" id="Seg_4317" n="e" s="T164">Karaŋa </ts>
               <ts e="T166" id="Seg_4319" n="e" s="T165">aːhan, </ts>
               <ts e="T167" id="Seg_4321" n="e" s="T166">hɨrdɨːrga </ts>
               <ts e="T168" id="Seg_4323" n="e" s="T167">barar. </ts>
               <ts e="T169" id="Seg_4325" n="e" s="T168">Kuŋadʼaj </ts>
               <ts e="T170" id="Seg_4327" n="e" s="T169">baltɨtɨgar </ts>
               <ts e="T171" id="Seg_4329" n="e" s="T170">eter: </ts>
               <ts e="T172" id="Seg_4331" n="e" s="T171">"Tabagɨn </ts>
               <ts e="T173" id="Seg_4333" n="e" s="T172">kölüj. </ts>
               <ts e="T174" id="Seg_4335" n="e" s="T173">Hɨppɨtɨnan </ts>
               <ts e="T175" id="Seg_4337" n="e" s="T174">ölü͡ökpüt. </ts>
               <ts e="T176" id="Seg_4339" n="e" s="T175">Agɨs </ts>
               <ts e="T177" id="Seg_4341" n="e" s="T176">koŋnomu͡oju </ts>
               <ts e="T178" id="Seg_4343" n="e" s="T177">batɨ͡ak!" </ts>
               <ts e="T179" id="Seg_4345" n="e" s="T178">Tabalarɨn </ts>
               <ts e="T180" id="Seg_4347" n="e" s="T179">kölüjen, </ts>
               <ts e="T181" id="Seg_4349" n="e" s="T180">dʼi͡elerin </ts>
               <ts e="T182" id="Seg_4351" n="e" s="T181">kötüren, </ts>
               <ts e="T183" id="Seg_4353" n="e" s="T182">meŋesten </ts>
               <ts e="T184" id="Seg_4355" n="e" s="T183">etteten </ts>
               <ts e="T185" id="Seg_4357" n="e" s="T184">istektere. </ts>
               <ts e="T186" id="Seg_4359" n="e" s="T185">Kah </ts>
               <ts e="T187" id="Seg_4361" n="e" s="T186">u͡onna </ts>
               <ts e="T188" id="Seg_4363" n="e" s="T187">köspütter. </ts>
               <ts e="T189" id="Seg_4365" n="e" s="T188">Biːrde </ts>
               <ts e="T190" id="Seg_4367" n="e" s="T189">kütürü͡ök </ts>
               <ts e="T191" id="Seg_4369" n="e" s="T190">hiske </ts>
               <ts e="T192" id="Seg_4371" n="e" s="T191">ɨttɨbɨttar. </ts>
               <ts e="T193" id="Seg_4373" n="e" s="T192">Körüleːbittere, </ts>
               <ts e="T194" id="Seg_4375" n="e" s="T193">kütürdeːk </ts>
               <ts e="T195" id="Seg_4377" n="e" s="T194">teːn </ts>
               <ts e="T196" id="Seg_4379" n="e" s="T195">teːn </ts>
               <ts e="T197" id="Seg_4381" n="e" s="T196">ortotugar </ts>
               <ts e="T198" id="Seg_4383" n="e" s="T197">ü͡ör </ts>
               <ts e="T199" id="Seg_4385" n="e" s="T198">bögö, </ts>
               <ts e="T200" id="Seg_4387" n="e" s="T199">teːn </ts>
               <ts e="T201" id="Seg_4389" n="e" s="T200">uŋu͡or </ts>
               <ts e="T202" id="Seg_4391" n="e" s="T201">hette </ts>
               <ts e="T203" id="Seg_4393" n="e" s="T202">u͡on </ts>
               <ts e="T204" id="Seg_4395" n="e" s="T203">dʼi͡e </ts>
               <ts e="T205" id="Seg_4397" n="e" s="T204">kihi </ts>
               <ts e="T206" id="Seg_4399" n="e" s="T205">köstör. </ts>
               <ts e="T207" id="Seg_4401" n="e" s="T206">Barbɨttar </ts>
               <ts e="T208" id="Seg_4403" n="e" s="T207">ü͡ör </ts>
               <ts e="T209" id="Seg_4405" n="e" s="T208">ihinen </ts>
               <ts e="T210" id="Seg_4407" n="e" s="T209">ü͡ör </ts>
               <ts e="T211" id="Seg_4409" n="e" s="T210">anaraː </ts>
               <ts e="T212" id="Seg_4411" n="e" s="T211">öttütüger. </ts>
               <ts e="T213" id="Seg_4413" n="e" s="T212">"Tur", </ts>
               <ts e="T214" id="Seg_4415" n="e" s="T213">di͡ebit </ts>
               <ts e="T215" id="Seg_4417" n="e" s="T214">Kuŋadʼaj. </ts>
               <ts e="T216" id="Seg_4419" n="e" s="T215">"Iti </ts>
               <ts e="T217" id="Seg_4421" n="e" s="T216">dʼi͡elerten </ts>
               <ts e="T218" id="Seg_4423" n="e" s="T217">biːr </ts>
               <ts e="T219" id="Seg_4425" n="e" s="T218">orduktarɨgar </ts>
               <ts e="T220" id="Seg_4427" n="e" s="T219">toktoːr, </ts>
               <ts e="T221" id="Seg_4429" n="e" s="T220">niptelerin </ts>
               <ts e="T222" id="Seg_4431" n="e" s="T221">betereː </ts>
               <ts e="T223" id="Seg_4433" n="e" s="T222">öttükeːniger." </ts>
               <ts e="T224" id="Seg_4435" n="e" s="T223">Körbüte, </ts>
               <ts e="T225" id="Seg_4437" n="e" s="T224">biːr </ts>
               <ts e="T226" id="Seg_4439" n="e" s="T225">dʼi͡e </ts>
               <ts e="T227" id="Seg_4441" n="e" s="T226">barɨlarɨttan </ts>
               <ts e="T228" id="Seg_4443" n="e" s="T227">ulakan. </ts>
               <ts e="T229" id="Seg_4445" n="e" s="T228">Aːn </ts>
               <ts e="T230" id="Seg_4447" n="e" s="T229">tuhugar </ts>
               <ts e="T231" id="Seg_4449" n="e" s="T230">tijbitter </ts>
               <ts e="T232" id="Seg_4451" n="e" s="T231">da </ts>
               <ts e="T233" id="Seg_4453" n="e" s="T232">tabalarɨn </ts>
               <ts e="T234" id="Seg_4455" n="e" s="T233">ɨːtan </ts>
               <ts e="T235" id="Seg_4457" n="e" s="T234">keːheller. </ts>
               <ts e="T236" id="Seg_4459" n="e" s="T235">Dʼi͡e </ts>
               <ts e="T237" id="Seg_4461" n="e" s="T236">tuttan </ts>
               <ts e="T238" id="Seg_4463" n="e" s="T237">keːspitter, </ts>
               <ts e="T239" id="Seg_4465" n="e" s="T238">ottubuttar. </ts>
               <ts e="T240" id="Seg_4467" n="e" s="T239">Baltɨtɨn </ts>
               <ts e="T241" id="Seg_4469" n="e" s="T240">"bar </ts>
               <ts e="T242" id="Seg_4471" n="e" s="T241">haŋaskar. </ts>
               <ts e="T243" id="Seg_4473" n="e" s="T242">Ogotun </ts>
               <ts e="T244" id="Seg_4475" n="e" s="T243">emnere </ts>
               <ts e="T245" id="Seg_4477" n="e" s="T244">kellin. </ts>
               <ts e="T246" id="Seg_4479" n="e" s="T245">Agɨs </ts>
               <ts e="T247" id="Seg_4481" n="e" s="T246">Koŋnomu͡ojga </ts>
               <ts e="T248" id="Seg_4483" n="e" s="T247">et </ts>
               <ts e="T249" id="Seg_4485" n="e" s="T248">'öllübüt, </ts>
               <ts e="T250" id="Seg_4487" n="e" s="T249">biːr </ts>
               <ts e="T251" id="Seg_4489" n="e" s="T250">eme </ts>
               <ts e="T252" id="Seg_4491" n="e" s="T251">hürek </ts>
               <ts e="T253" id="Seg_4493" n="e" s="T252">aŋaːrkaːnɨn </ts>
               <ts e="T254" id="Seg_4495" n="e" s="T253">kɨtta </ts>
               <ts e="T255" id="Seg_4497" n="e" s="T254">bɨ͡ar </ts>
               <ts e="T256" id="Seg_4499" n="e" s="T255">tulaːjagɨn </ts>
               <ts e="T257" id="Seg_4501" n="e" s="T256">beristin'", </ts>
               <ts e="T258" id="Seg_4503" n="e" s="T257">diːr. </ts>
               <ts e="T259" id="Seg_4505" n="e" s="T258">U͡ol </ts>
               <ts e="T260" id="Seg_4507" n="e" s="T259">hüːren </ts>
               <ts e="T261" id="Seg_4509" n="e" s="T260">kiːren </ts>
               <ts e="T262" id="Seg_4511" n="e" s="T261">eter. </ts>
               <ts e="T263" id="Seg_4513" n="e" s="T262">"Kör </ts>
               <ts e="T264" id="Seg_4515" n="e" s="T263">ere, </ts>
               <ts e="T265" id="Seg_4517" n="e" s="T264">emterer </ts>
               <ts e="T266" id="Seg_4519" n="e" s="T265">hokuču͡ok! </ts>
               <ts e="T267" id="Seg_4521" n="e" s="T266">Okton </ts>
               <ts e="T268" id="Seg_4523" n="e" s="T267">öllün", </ts>
               <ts e="T269" id="Seg_4525" n="e" s="T268">diːr </ts>
               <ts e="T270" id="Seg_4527" n="e" s="T269">dʼaktar. </ts>
               <ts e="T271" id="Seg_4529" n="e" s="T270">"Kaja </ts>
               <ts e="T272" id="Seg_4531" n="e" s="T271">eheː, </ts>
               <ts e="T273" id="Seg_4533" n="e" s="T272">ildʼitteːte </ts>
               <ts e="T274" id="Seg_4535" n="e" s="T273">ubajɨm </ts>
               <ts e="T275" id="Seg_4537" n="e" s="T274">'öllübüt </ts>
               <ts e="T276" id="Seg_4539" n="e" s="T275">biːr </ts>
               <ts e="T277" id="Seg_4541" n="e" s="T276">eme </ts>
               <ts e="T278" id="Seg_4543" n="e" s="T277">hürek </ts>
               <ts e="T279" id="Seg_4545" n="e" s="T278">aŋaːrkaːnɨn </ts>
               <ts e="T280" id="Seg_4547" n="e" s="T279">kɨtta </ts>
               <ts e="T281" id="Seg_4549" n="e" s="T280">bɨ͡ar </ts>
               <ts e="T282" id="Seg_4551" n="e" s="T281">tulaːjagɨn </ts>
               <ts e="T283" id="Seg_4553" n="e" s="T282">beristin'." </ts>
               <ts e="T284" id="Seg_4555" n="e" s="T283">"Kaːk, </ts>
               <ts e="T285" id="Seg_4557" n="e" s="T284">okton </ts>
               <ts e="T286" id="Seg_4559" n="e" s="T285">ölüŋ. </ts>
               <ts e="T287" id="Seg_4561" n="e" s="T286">ɨːppappɨn", </ts>
               <ts e="T288" id="Seg_4563" n="e" s="T287">diːr </ts>
               <ts e="T289" id="Seg_4565" n="e" s="T288">Agɨs </ts>
               <ts e="T290" id="Seg_4567" n="e" s="T289">koŋnomu͡oj </ts>
               <ts e="T291" id="Seg_4569" n="e" s="T290">iččite. </ts>
               <ts e="T292" id="Seg_4571" n="e" s="T291">Utujaːktɨːllar. </ts>
               <ts e="T293" id="Seg_4573" n="e" s="T292">Tugu </ts>
               <ts e="T294" id="Seg_4575" n="e" s="T293">hi͡ekterej? </ts>
               <ts e="T295" id="Seg_4577" n="e" s="T294">Araj </ts>
               <ts e="T296" id="Seg_4579" n="e" s="T295">tabalɨːr </ts>
               <ts e="T297" id="Seg_4581" n="e" s="T296">haŋa </ts>
               <ts e="T298" id="Seg_4583" n="e" s="T297">ihiller. </ts>
               <ts e="T299" id="Seg_4585" n="e" s="T298">Kuŋadʼaj </ts>
               <ts e="T300" id="Seg_4587" n="e" s="T299">taksan </ts>
               <ts e="T301" id="Seg_4589" n="e" s="T300">iːktiː </ts>
               <ts e="T302" id="Seg_4591" n="e" s="T301">turbut, </ts>
               <ts e="T303" id="Seg_4593" n="e" s="T302">tobugar. </ts>
               <ts e="T304" id="Seg_4595" n="e" s="T303">Taba </ts>
               <ts e="T305" id="Seg_4597" n="e" s="T304">bögö </ts>
               <ts e="T306" id="Seg_4599" n="e" s="T305">keler. </ts>
               <ts e="T307" id="Seg_4601" n="e" s="T306">Aktamiː </ts>
               <ts e="T308" id="Seg_4603" n="e" s="T307">buːr </ts>
               <ts e="T309" id="Seg_4605" n="e" s="T308">emis </ts>
               <ts e="T310" id="Seg_4607" n="e" s="T309">bagajɨ </ts>
               <ts e="T311" id="Seg_4609" n="e" s="T310">keler. </ts>
               <ts e="T312" id="Seg_4611" n="e" s="T311">U͡ol </ts>
               <ts e="T313" id="Seg_4613" n="e" s="T312">kaban </ts>
               <ts e="T314" id="Seg_4615" n="e" s="T313">ɨlar. </ts>
               <ts e="T315" id="Seg_4617" n="e" s="T314">Mögön </ts>
               <ts e="T316" id="Seg_4619" n="e" s="T315">bögö. </ts>
               <ts e="T317" id="Seg_4621" n="e" s="T316">Bahagɨn </ts>
               <ts e="T318" id="Seg_4623" n="e" s="T317">ɨllarar. </ts>
               <ts e="T319" id="Seg_4625" n="e" s="T318">Kabargatɨn </ts>
               <ts e="T320" id="Seg_4627" n="e" s="T319">bɨha </ts>
               <ts e="T321" id="Seg_4629" n="e" s="T320">hotor. </ts>
               <ts e="T322" id="Seg_4631" n="e" s="T321">Hülbekke </ts>
               <ts e="T323" id="Seg_4633" n="e" s="T322">da </ts>
               <ts e="T324" id="Seg_4635" n="e" s="T323">killereller, </ts>
               <ts e="T325" id="Seg_4637" n="e" s="T324">kü͡östenen </ts>
               <ts e="T326" id="Seg_4639" n="e" s="T325">dʼe </ts>
               <ts e="T327" id="Seg_4641" n="e" s="T326">ahɨːllar </ts>
               <ts e="T328" id="Seg_4643" n="e" s="T327">künü </ts>
               <ts e="T329" id="Seg_4645" n="e" s="T328">meldʼi </ts>
               <ts e="T330" id="Seg_4647" n="e" s="T329">ki͡ehege </ts>
               <ts e="T331" id="Seg_4649" n="e" s="T330">di͡eri. </ts>
               <ts e="T332" id="Seg_4651" n="e" s="T331">Bu </ts>
               <ts e="T333" id="Seg_4653" n="e" s="T332">dʼi͡e </ts>
               <ts e="T334" id="Seg_4655" n="e" s="T333">tahɨgar </ts>
               <ts e="T335" id="Seg_4657" n="e" s="T334">kuččuguj </ts>
               <ts e="T336" id="Seg_4659" n="e" s="T335">dʼi͡e </ts>
               <ts e="T337" id="Seg_4661" n="e" s="T336">turar. </ts>
               <ts e="T338" id="Seg_4663" n="e" s="T337">"Čeː, </ts>
               <ts e="T339" id="Seg_4665" n="e" s="T338">hette </ts>
               <ts e="T340" id="Seg_4667" n="e" s="T339">timek </ts>
               <ts e="T341" id="Seg_4669" n="e" s="T340">iččitiger </ts>
               <ts e="T342" id="Seg_4671" n="e" s="T341">bar </ts>
               <ts e="T343" id="Seg_4673" n="e" s="T342">iti </ts>
               <ts e="T344" id="Seg_4675" n="e" s="T343">dʼi͡ege </ts>
               <ts e="T345" id="Seg_4677" n="e" s="T344">(staršina </ts>
               <ts e="T346" id="Seg_4679" n="e" s="T345">bu͡olu͡o) </ts>
               <ts e="T347" id="Seg_4681" n="e" s="T346">kü͡öste </ts>
               <ts e="T348" id="Seg_4683" n="e" s="T347">kördöː." </ts>
               <ts e="T349" id="Seg_4685" n="e" s="T348">U͡ol </ts>
               <ts e="T350" id="Seg_4687" n="e" s="T349">barar, </ts>
               <ts e="T351" id="Seg_4689" n="e" s="T350">hette </ts>
               <ts e="T352" id="Seg_4691" n="e" s="T351">timek </ts>
               <ts e="T353" id="Seg_4693" n="e" s="T352">iččitiger </ts>
               <ts e="T354" id="Seg_4695" n="e" s="T353">eter </ts>
               <ts e="T355" id="Seg_4697" n="e" s="T354">ildʼiti. </ts>
               <ts e="T356" id="Seg_4699" n="e" s="T355">"Agɨs </ts>
               <ts e="T357" id="Seg_4701" n="e" s="T356">koŋnomu͡oj </ts>
               <ts e="T358" id="Seg_4703" n="e" s="T357">iččite </ts>
               <ts e="T359" id="Seg_4705" n="e" s="T358">bi͡erde </ts>
               <ts e="T360" id="Seg_4707" n="e" s="T359">eni?" </ts>
               <ts e="T361" id="Seg_4709" n="e" s="T360">"Hu͡ok." </ts>
               <ts e="T362" id="Seg_4711" n="e" s="T361">"Eː, </ts>
               <ts e="T363" id="Seg_4713" n="e" s="T362">oččogo </ts>
               <ts e="T364" id="Seg_4715" n="e" s="T363">min </ts>
               <ts e="T365" id="Seg_4717" n="e" s="T364">emi͡e </ts>
               <ts e="T366" id="Seg_4719" n="e" s="T365">bi͡erbeppin." </ts>
               <ts e="T367" id="Seg_4721" n="e" s="T366">Ahɨːllar. </ts>
               <ts e="T368" id="Seg_4723" n="e" s="T367">Harsi͡erda </ts>
               <ts e="T369" id="Seg_4725" n="e" s="T368">ü͡ör </ts>
               <ts e="T370" id="Seg_4727" n="e" s="T369">iher </ts>
               <ts e="T371" id="Seg_4729" n="e" s="T370">emi͡e. </ts>
               <ts e="T372" id="Seg_4731" n="e" s="T371">Tobugunan </ts>
               <ts e="T373" id="Seg_4733" n="e" s="T372">ünen </ts>
               <ts e="T374" id="Seg_4735" n="e" s="T373">taksar </ts>
               <ts e="T375" id="Seg_4737" n="e" s="T374">Kuŋadʼaj. </ts>
               <ts e="T376" id="Seg_4739" n="e" s="T375">Kuŋadʼaj </ts>
               <ts e="T377" id="Seg_4741" n="e" s="T376">töröːböt </ts>
               <ts e="T378" id="Seg_4743" n="e" s="T377">maːŋkaːjɨ </ts>
               <ts e="T379" id="Seg_4745" n="e" s="T378">tutan </ts>
               <ts e="T380" id="Seg_4747" n="e" s="T379">ɨlar </ts>
               <ts e="T381" id="Seg_4749" n="e" s="T380">kaldaːtɨttan. </ts>
               <ts e="T382" id="Seg_4751" n="e" s="T381">"Kaja-kuː, </ts>
               <ts e="T383" id="Seg_4753" n="e" s="T382">bahagɨ </ts>
               <ts e="T384" id="Seg_4755" n="e" s="T383">tahaːr!" </ts>
               <ts e="T385" id="Seg_4757" n="e" s="T384">Ölörön </ts>
               <ts e="T386" id="Seg_4759" n="e" s="T385">keːher. </ts>
               <ts e="T387" id="Seg_4761" n="e" s="T386">Emi͡e </ts>
               <ts e="T388" id="Seg_4763" n="e" s="T387">hülbekke </ts>
               <ts e="T389" id="Seg_4765" n="e" s="T388">hiːller. </ts>
               <ts e="T390" id="Seg_4767" n="e" s="T389">Kaja </ts>
               <ts e="T391" id="Seg_4769" n="e" s="T390">bu, </ts>
               <ts e="T392" id="Seg_4771" n="e" s="T391">bili </ts>
               <ts e="T393" id="Seg_4773" n="e" s="T392">tojottor </ts>
               <ts e="T394" id="Seg_4775" n="e" s="T393">gi͡ennerin </ts>
               <ts e="T395" id="Seg_4777" n="e" s="T394">ölörbüt </ts>
               <ts e="T396" id="Seg_4779" n="e" s="T395">ebit. </ts>
               <ts e="T397" id="Seg_4781" n="e" s="T396">Utujallar. </ts>
               <ts e="T398" id="Seg_4783" n="e" s="T397">Harsi͡erda </ts>
               <ts e="T399" id="Seg_4785" n="e" s="T398">Kuŋadʼaj </ts>
               <ts e="T400" id="Seg_4787" n="e" s="T399">taksɨbɨta, </ts>
               <ts e="T401" id="Seg_4789" n="e" s="T400">Agɨs </ts>
               <ts e="T402" id="Seg_4791" n="e" s="T401">koŋnomu͡oj </ts>
               <ts e="T403" id="Seg_4793" n="e" s="T402">küseːjine </ts>
               <ts e="T404" id="Seg_4795" n="e" s="T403">kürejin </ts>
               <ts e="T405" id="Seg_4797" n="e" s="T404">ku͡ohana </ts>
               <ts e="T406" id="Seg_4799" n="e" s="T405">oloror. </ts>
               <ts e="T407" id="Seg_4801" n="e" s="T406">"Kajaː, </ts>
               <ts e="T408" id="Seg_4803" n="e" s="T407">Agɨs </ts>
               <ts e="T409" id="Seg_4805" n="e" s="T408">koŋnomu͡oj </ts>
               <ts e="T410" id="Seg_4807" n="e" s="T409">iččite, </ts>
               <ts e="T411" id="Seg_4809" n="e" s="T410">doroːbo!" </ts>
               <ts e="T412" id="Seg_4811" n="e" s="T411">"Össü͡ö </ts>
               <ts e="T413" id="Seg_4813" n="e" s="T412">doroːbo! </ts>
               <ts e="T414" id="Seg_4815" n="e" s="T413">Bügün </ts>
               <ts e="T415" id="Seg_4817" n="e" s="T414">ölörtüːbüt. </ts>
               <ts e="T416" id="Seg_4819" n="e" s="T415">Ele </ts>
               <ts e="T417" id="Seg_4821" n="e" s="T416">tabalarbɨttan </ts>
               <ts e="T418" id="Seg_4823" n="e" s="T417">hiː </ts>
               <ts e="T419" id="Seg_4825" n="e" s="T418">hɨtagɨt!" </ts>
               <ts e="T420" id="Seg_4827" n="e" s="T419">"Bejegit </ts>
               <ts e="T421" id="Seg_4829" n="e" s="T420">burujgut." </ts>
               <ts e="T422" id="Seg_4831" n="e" s="T421">Agɨs </ts>
               <ts e="T423" id="Seg_4833" n="e" s="T422">koŋnomu͡oj </ts>
               <ts e="T424" id="Seg_4835" n="e" s="T423">iččite </ts>
               <ts e="T425" id="Seg_4837" n="e" s="T424">tobuktaːn </ts>
               <ts e="T426" id="Seg_4839" n="e" s="T425">turar </ts>
               <ts e="T427" id="Seg_4841" n="e" s="T426">kihini </ts>
               <ts e="T428" id="Seg_4843" n="e" s="T427">kürej </ts>
               <ts e="T429" id="Seg_4845" n="e" s="T428">ünüːtünen </ts>
               <ts e="T430" id="Seg_4847" n="e" s="T429">öttükke </ts>
               <ts e="T431" id="Seg_4849" n="e" s="T430">haːjar </ts>
               <ts e="T432" id="Seg_4851" n="e" s="T431">da </ts>
               <ts e="T433" id="Seg_4853" n="e" s="T432">hɨːhan </ts>
               <ts e="T434" id="Seg_4855" n="e" s="T433">keːher. </ts>
               <ts e="T435" id="Seg_4857" n="e" s="T434">Kuŋadʼaja </ts>
               <ts e="T436" id="Seg_4859" n="e" s="T435">kaːr </ts>
               <ts e="T437" id="Seg_4861" n="e" s="T436">annɨnan </ts>
               <ts e="T438" id="Seg_4863" n="e" s="T437">barbɨt. </ts>
               <ts e="T439" id="Seg_4865" n="e" s="T438">Hette </ts>
               <ts e="T440" id="Seg_4867" n="e" s="T439">timek </ts>
               <ts e="T441" id="Seg_4869" n="e" s="T440">iččite </ts>
               <ts e="T442" id="Seg_4871" n="e" s="T441">kürej </ts>
               <ts e="T443" id="Seg_4873" n="e" s="T442">ku͡ohana </ts>
               <ts e="T444" id="Seg_4875" n="e" s="T443">olordoguna </ts>
               <ts e="T445" id="Seg_4877" n="e" s="T444">taksar. </ts>
               <ts e="T446" id="Seg_4879" n="e" s="T445">"Kajaː, </ts>
               <ts e="T447" id="Seg_4881" n="e" s="T446">hette </ts>
               <ts e="T448" id="Seg_4883" n="e" s="T447">timek </ts>
               <ts e="T449" id="Seg_4885" n="e" s="T448">iččite, </ts>
               <ts e="T450" id="Seg_4887" n="e" s="T449">doroːbo!" </ts>
               <ts e="T451" id="Seg_4889" n="e" s="T450">"Össü͡ö </ts>
               <ts e="T452" id="Seg_4891" n="e" s="T451">doroːbo! </ts>
               <ts e="T453" id="Seg_4893" n="e" s="T452">Bügün </ts>
               <ts e="T454" id="Seg_4895" n="e" s="T453">ölörtüːbüt. </ts>
               <ts e="T455" id="Seg_4897" n="e" s="T454">Ele </ts>
               <ts e="T456" id="Seg_4899" n="e" s="T455">tabalarbɨtɨn </ts>
               <ts e="T457" id="Seg_4901" n="e" s="T456">hiː </ts>
               <ts e="T458" id="Seg_4903" n="e" s="T457">hɨtagɨt!" </ts>
               <ts e="T459" id="Seg_4905" n="e" s="T458">"Bejegit </ts>
               <ts e="T460" id="Seg_4907" n="e" s="T459">burujgut." </ts>
               <ts e="T461" id="Seg_4909" n="e" s="T460">Hette </ts>
               <ts e="T462" id="Seg_4911" n="e" s="T461">timek </ts>
               <ts e="T463" id="Seg_4913" n="e" s="T462">iččite </ts>
               <ts e="T464" id="Seg_4915" n="e" s="T463">tobuktaːn </ts>
               <ts e="T465" id="Seg_4917" n="e" s="T464">turar </ts>
               <ts e="T466" id="Seg_4919" n="e" s="T465">kihini </ts>
               <ts e="T467" id="Seg_4921" n="e" s="T466">kürej </ts>
               <ts e="T468" id="Seg_4923" n="e" s="T467">üŋüːtünen </ts>
               <ts e="T469" id="Seg_4925" n="e" s="T468">öttükke </ts>
               <ts e="T470" id="Seg_4927" n="e" s="T469">annʼaːrɨ </ts>
               <ts e="T471" id="Seg_4929" n="e" s="T470">hiri </ts>
               <ts e="T472" id="Seg_4931" n="e" s="T471">haːjbɨt. </ts>
               <ts e="T473" id="Seg_4933" n="e" s="T472">Kaččaga </ts>
               <ts e="T474" id="Seg_4935" n="e" s="T473">ere </ts>
               <ts e="T475" id="Seg_4937" n="e" s="T474">Kuŋadʼaj </ts>
               <ts e="T476" id="Seg_4939" n="e" s="T475">nöŋü͡ö </ts>
               <ts e="T477" id="Seg_4941" n="e" s="T476">hirinen </ts>
               <ts e="T478" id="Seg_4943" n="e" s="T477">taksa </ts>
               <ts e="T479" id="Seg_4945" n="e" s="T478">kötör </ts>
               <ts e="T480" id="Seg_4947" n="e" s="T479">da </ts>
               <ts e="T481" id="Seg_4949" n="e" s="T480">ikki </ts>
               <ts e="T482" id="Seg_4951" n="e" s="T481">atagar </ts>
               <ts e="T483" id="Seg_4953" n="e" s="T482">tura </ts>
               <ts e="T484" id="Seg_4955" n="e" s="T483">ekkiriːr. </ts>
               <ts e="T485" id="Seg_4957" n="e" s="T484">Körüleːbite, </ts>
               <ts e="T486" id="Seg_4959" n="e" s="T485">kös </ts>
               <ts e="T487" id="Seg_4961" n="e" s="T486">baran </ts>
               <ts e="T488" id="Seg_4963" n="e" s="T487">erer. </ts>
               <ts e="T489" id="Seg_4965" n="e" s="T488">Haŋaha </ts>
               <ts e="T490" id="Seg_4967" n="e" s="T489">köhön </ts>
               <ts e="T491" id="Seg_4969" n="e" s="T490">erer, </ts>
               <ts e="T492" id="Seg_4971" n="e" s="T491">agata </ts>
               <ts e="T493" id="Seg_4973" n="e" s="T492">kergen </ts>
               <ts e="T494" id="Seg_4975" n="e" s="T493">bi͡erbit. </ts>
               <ts e="T495" id="Seg_4977" n="e" s="T494">Kuŋadʼaj </ts>
               <ts e="T496" id="Seg_4979" n="e" s="T495">haŋahɨn </ts>
               <ts e="T497" id="Seg_4981" n="e" s="T496">nʼu͡ogutun </ts>
               <ts e="T498" id="Seg_4983" n="e" s="T497">kaban </ts>
               <ts e="T499" id="Seg_4985" n="e" s="T498">ɨlar: </ts>
               <ts e="T500" id="Seg_4987" n="e" s="T499">"ɨːppappɨn. </ts>
               <ts e="T501" id="Seg_4989" n="e" s="T500">Kaja </ts>
               <ts e="T502" id="Seg_4991" n="e" s="T501">di͡eki </ts>
               <ts e="T503" id="Seg_4993" n="e" s="T502">baragɨn?" </ts>
               <ts e="T504" id="Seg_4995" n="e" s="T503">Dʼaktar </ts>
               <ts e="T505" id="Seg_4997" n="e" s="T504">kötüten </ts>
               <ts e="T506" id="Seg_4999" n="e" s="T505">kaːlar. </ts>
               <ts e="T507" id="Seg_5001" n="e" s="T506">Kuŋadʼaj </ts>
               <ts e="T508" id="Seg_5003" n="e" s="T507">hite </ts>
               <ts e="T509" id="Seg_5005" n="e" s="T508">battaːn </ts>
               <ts e="T510" id="Seg_5007" n="e" s="T509">ölgöbüːnün </ts>
               <ts e="T511" id="Seg_5009" n="e" s="T510">ildʼe </ts>
               <ts e="T512" id="Seg_5011" n="e" s="T511">kaːlar. </ts>
               <ts e="T513" id="Seg_5013" n="e" s="T512">Kuŋadʼaj </ts>
               <ts e="T514" id="Seg_5015" n="e" s="T513">Agɨs </ts>
               <ts e="T515" id="Seg_5017" n="e" s="T514">koŋnomu͡oj </ts>
               <ts e="T516" id="Seg_5019" n="e" s="T515">iččitiger </ts>
               <ts e="T517" id="Seg_5021" n="e" s="T516">kiːrer: </ts>
               <ts e="T518" id="Seg_5023" n="e" s="T517">"Dʼe, </ts>
               <ts e="T519" id="Seg_5025" n="e" s="T518">Agɨs </ts>
               <ts e="T520" id="Seg_5027" n="e" s="T519">koŋnomu͡oj </ts>
               <ts e="T521" id="Seg_5029" n="e" s="T520">iččite, </ts>
               <ts e="T522" id="Seg_5031" n="e" s="T521">ejeleːk </ts>
               <ts e="T523" id="Seg_5033" n="e" s="T522">erdekke </ts>
               <ts e="T524" id="Seg_5035" n="e" s="T523">balɨstarbɨn </ts>
               <ts e="T525" id="Seg_5037" n="e" s="T524">karajan </ts>
               <ts e="T526" id="Seg_5039" n="e" s="T525">olor. </ts>
               <ts e="T527" id="Seg_5041" n="e" s="T526">Kuhagannɨk </ts>
               <ts e="T528" id="Seg_5043" n="e" s="T527">körü͡öŋ — </ts>
               <ts e="T529" id="Seg_5045" n="e" s="T528">biːr </ts>
               <ts e="T530" id="Seg_5047" n="e" s="T529">kününen </ts>
               <ts e="T531" id="Seg_5049" n="e" s="T530">hu͡ok </ts>
               <ts e="T532" id="Seg_5051" n="e" s="T531">oŋortu͡om. </ts>
               <ts e="T533" id="Seg_5053" n="e" s="T532">Haŋaspɨn </ts>
               <ts e="T534" id="Seg_5055" n="e" s="T533">bata </ts>
               <ts e="T535" id="Seg_5057" n="e" s="T534">bardɨm." </ts>
               <ts e="T536" id="Seg_5059" n="e" s="T535">Haŋahɨn </ts>
               <ts e="T537" id="Seg_5061" n="e" s="T536">batan </ts>
               <ts e="T538" id="Seg_5063" n="e" s="T537">hüteren </ts>
               <ts e="T539" id="Seg_5065" n="e" s="T538">keːher. </ts>
               <ts e="T540" id="Seg_5067" n="e" s="T539">Kannɨk </ts>
               <ts e="T541" id="Seg_5069" n="e" s="T540">dojdu </ts>
               <ts e="T542" id="Seg_5071" n="e" s="T541">uhugar </ts>
               <ts e="T543" id="Seg_5073" n="e" s="T542">körör, </ts>
               <ts e="T544" id="Seg_5075" n="e" s="T543">hiti͡ek </ts>
               <ts e="T545" id="Seg_5077" n="e" s="T544">hippet. </ts>
               <ts e="T546" id="Seg_5079" n="e" s="T545">Ergiten </ts>
               <ts e="T547" id="Seg_5081" n="e" s="T546">egelen </ts>
               <ts e="T548" id="Seg_5083" n="e" s="T547">dʼi͡etin </ts>
               <ts e="T549" id="Seg_5085" n="e" s="T548">di͡ek </ts>
               <ts e="T550" id="Seg_5087" n="e" s="T549">kü͡öjer. </ts>
               <ts e="T551" id="Seg_5089" n="e" s="T550">Tutu͡ok </ts>
               <ts e="T552" id="Seg_5091" n="e" s="T551">da </ts>
               <ts e="T553" id="Seg_5093" n="e" s="T552">ereːri </ts>
               <ts e="T554" id="Seg_5095" n="e" s="T553">tuppakka </ts>
               <ts e="T555" id="Seg_5097" n="e" s="T554">haŋahɨn </ts>
               <ts e="T556" id="Seg_5099" n="e" s="T555">kennitten </ts>
               <ts e="T557" id="Seg_5101" n="e" s="T556">kiːrer. </ts>
               <ts e="T558" id="Seg_5103" n="e" s="T557">Hanaha </ts>
               <ts e="T559" id="Seg_5105" n="e" s="T558">ogotun </ts>
               <ts e="T560" id="Seg_5107" n="e" s="T559">emijdiː </ts>
               <ts e="T561" id="Seg_5109" n="e" s="T560">oloror. </ts>
               <ts e="T562" id="Seg_5111" n="e" s="T561">Balɨstara </ts>
               <ts e="T563" id="Seg_5113" n="e" s="T562">ahɨː </ts>
               <ts e="T564" id="Seg_5115" n="e" s="T563">oloror. </ts>
               <ts e="T565" id="Seg_5117" n="e" s="T564">"Dʼe </ts>
               <ts e="T566" id="Seg_5119" n="e" s="T565">haŋaːs, </ts>
               <ts e="T567" id="Seg_5121" n="e" s="T566">burujgun </ts>
               <ts e="T568" id="Seg_5123" n="e" s="T567">ahardɨŋ. </ts>
               <ts e="T569" id="Seg_5125" n="e" s="T568">Ogogun </ts>
               <ts e="T570" id="Seg_5127" n="e" s="T569">ɨlbatagɨŋ </ts>
               <ts e="T571" id="Seg_5129" n="e" s="T570">bu͡ollar </ts>
               <ts e="T572" id="Seg_5131" n="e" s="T571">ölörü͡ök </ts>
               <ts e="T573" id="Seg_5133" n="e" s="T572">etim! </ts>
               <ts e="T574" id="Seg_5135" n="e" s="T573">Agɨs </ts>
               <ts e="T575" id="Seg_5137" n="e" s="T574">koŋnomu͡oj </ts>
               <ts e="T576" id="Seg_5139" n="e" s="T575">iččite </ts>
               <ts e="T577" id="Seg_5141" n="e" s="T576">kuttanaːrɨ </ts>
               <ts e="T578" id="Seg_5143" n="e" s="T577">kuttanar." </ts>
               <ts e="T579" id="Seg_5145" n="e" s="T578">Olorollor. </ts>
               <ts e="T580" id="Seg_5147" n="e" s="T579">Kahan </ts>
               <ts e="T581" id="Seg_5149" n="e" s="T580">ere </ts>
               <ts e="T582" id="Seg_5151" n="e" s="T581">ühüs </ts>
               <ts e="T583" id="Seg_5153" n="e" s="T582">künüger: </ts>
               <ts e="T584" id="Seg_5155" n="e" s="T583">"Dʼe </ts>
               <ts e="T585" id="Seg_5157" n="e" s="T584">Agɨs </ts>
               <ts e="T586" id="Seg_5159" n="e" s="T585">koŋnomu͡oj </ts>
               <ts e="T587" id="Seg_5161" n="e" s="T586">iččite, </ts>
               <ts e="T588" id="Seg_5163" n="e" s="T587">dʼoŋŋun </ts>
               <ts e="T589" id="Seg_5165" n="e" s="T588">mus. </ts>
               <ts e="T590" id="Seg_5167" n="e" s="T589">Munnʼaktɨ͡akpɨt", </ts>
               <ts e="T591" id="Seg_5169" n="e" s="T590">diːr </ts>
               <ts e="T592" id="Seg_5171" n="e" s="T591">Kuŋadʼaj. </ts>
               <ts e="T593" id="Seg_5173" n="e" s="T592">Dʼe </ts>
               <ts e="T594" id="Seg_5175" n="e" s="T593">munnʼallar </ts>
               <ts e="T595" id="Seg_5177" n="e" s="T594">hette </ts>
               <ts e="T596" id="Seg_5179" n="e" s="T595">u͡on </ts>
               <ts e="T597" id="Seg_5181" n="e" s="T596">dʼi͡e </ts>
               <ts e="T598" id="Seg_5183" n="e" s="T597">kihitin. </ts>
               <ts e="T599" id="Seg_5185" n="e" s="T598">Kuŋadʼaj </ts>
               <ts e="T600" id="Seg_5187" n="e" s="T599">eter: </ts>
               <ts e="T601" id="Seg_5189" n="e" s="T600">"Ehigi </ts>
               <ts e="T602" id="Seg_5191" n="e" s="T601">ikki </ts>
               <ts e="T603" id="Seg_5193" n="e" s="T602">küseːjin </ts>
               <ts e="T604" id="Seg_5195" n="e" s="T603">baːrgɨt — </ts>
               <ts e="T605" id="Seg_5197" n="e" s="T604">Agɨs </ts>
               <ts e="T606" id="Seg_5199" n="e" s="T605">koŋnomu͡oj </ts>
               <ts e="T607" id="Seg_5201" n="e" s="T606">küseːjine </ts>
               <ts e="T608" id="Seg_5203" n="e" s="T607">u͡onna </ts>
               <ts e="T609" id="Seg_5205" n="e" s="T608">hette </ts>
               <ts e="T610" id="Seg_5207" n="e" s="T609">timek </ts>
               <ts e="T611" id="Seg_5209" n="e" s="T610">iččite. </ts>
               <ts e="T612" id="Seg_5211" n="e" s="T611">Anɨ </ts>
               <ts e="T613" id="Seg_5213" n="e" s="T612">küseːjin </ts>
               <ts e="T614" id="Seg_5215" n="e" s="T613">bu͡olan </ts>
               <ts e="T615" id="Seg_5217" n="e" s="T614">büttügüt. </ts>
               <ts e="T616" id="Seg_5219" n="e" s="T615">Küseːjiŋŋit — </ts>
               <ts e="T617" id="Seg_5221" n="e" s="T616">iti </ts>
               <ts e="T618" id="Seg_5223" n="e" s="T617">u͡ol </ts>
               <ts e="T619" id="Seg_5225" n="e" s="T618">ogo — </ts>
               <ts e="T620" id="Seg_5227" n="e" s="T619">ubajɨm </ts>
               <ts e="T621" id="Seg_5229" n="e" s="T620">ogoto. </ts>
               <ts e="T622" id="Seg_5231" n="e" s="T621">Komunuŋ </ts>
               <ts e="T623" id="Seg_5233" n="e" s="T622">barɨgɨt </ts>
               <ts e="T624" id="Seg_5235" n="e" s="T623">ikki </ts>
               <ts e="T625" id="Seg_5237" n="e" s="T624">küŋŋe. </ts>
               <ts e="T626" id="Seg_5239" n="e" s="T625">Ühüs </ts>
               <ts e="T627" id="Seg_5241" n="e" s="T626">küŋŋütüger </ts>
               <ts e="T628" id="Seg_5243" n="e" s="T627">köhü͡ökküt. </ts>
               <ts e="T629" id="Seg_5245" n="e" s="T628">Bastɨŋŋɨt </ts>
               <ts e="T630" id="Seg_5247" n="e" s="T629">kini </ts>
               <ts e="T631" id="Seg_5249" n="e" s="T630">bu͡olu͡o." </ts>
               <ts e="T632" id="Seg_5251" n="e" s="T631">"Atɨn </ts>
               <ts e="T633" id="Seg_5253" n="e" s="T632">tabanɨ </ts>
               <ts e="T634" id="Seg_5255" n="e" s="T633">kölünüme, </ts>
               <ts e="T635" id="Seg_5257" n="e" s="T634">agaŋ </ts>
               <ts e="T636" id="Seg_5259" n="e" s="T635">tabatɨn, </ts>
               <ts e="T637" id="Seg_5261" n="e" s="T636">keri͡ehin, </ts>
               <ts e="T638" id="Seg_5263" n="e" s="T637">kölüneːr", </ts>
               <ts e="T639" id="Seg_5265" n="e" s="T638">diːr </ts>
               <ts e="T640" id="Seg_5267" n="e" s="T639">u͡olga </ts>
               <ts e="T641" id="Seg_5269" n="e" s="T640">[u͡ol </ts>
               <ts e="T642" id="Seg_5271" n="e" s="T641">ulaːppɨt]. </ts>
               <ts e="T643" id="Seg_5273" n="e" s="T642">"Bastataŋŋɨn </ts>
               <ts e="T644" id="Seg_5275" n="e" s="T643">köhü͡ökküt. </ts>
               <ts e="T645" id="Seg_5277" n="e" s="T644">Mas </ts>
               <ts e="T646" id="Seg_5279" n="e" s="T645">hagatɨnan </ts>
               <ts e="T647" id="Seg_5281" n="e" s="T646">dʼirbiː </ts>
               <ts e="T648" id="Seg_5283" n="e" s="T647">baːr </ts>
               <ts e="T649" id="Seg_5285" n="e" s="T648">bu͡olu͡o, </ts>
               <ts e="T650" id="Seg_5287" n="e" s="T649">bu </ts>
               <ts e="T651" id="Seg_5289" n="e" s="T650">dʼirbiː </ts>
               <ts e="T652" id="Seg_5291" n="e" s="T651">mu͡ora </ts>
               <ts e="T653" id="Seg_5293" n="e" s="T652">di͡eki </ts>
               <ts e="T654" id="Seg_5295" n="e" s="T653">öttünen </ts>
               <ts e="T655" id="Seg_5297" n="e" s="T654">köhü͡öŋ, </ts>
               <ts e="T656" id="Seg_5299" n="e" s="T655">ikki </ts>
               <ts e="T657" id="Seg_5301" n="e" s="T656">ɨjɨ </ts>
               <ts e="T658" id="Seg_5303" n="e" s="T657">bɨha. </ts>
               <ts e="T659" id="Seg_5305" n="e" s="T658">Elete </ts>
               <ts e="T660" id="Seg_5307" n="e" s="T659">bu͡ollagɨna, </ts>
               <ts e="T661" id="Seg_5309" n="e" s="T660">dʼirbiːte </ts>
               <ts e="T662" id="Seg_5311" n="e" s="T661">büttegine </ts>
               <ts e="T663" id="Seg_5313" n="e" s="T662">hir </ts>
               <ts e="T664" id="Seg_5315" n="e" s="T663">bu͡olu͡o, </ts>
               <ts e="T665" id="Seg_5317" n="e" s="T664">kaːrɨn </ts>
               <ts e="T666" id="Seg_5319" n="e" s="T665">annɨttan </ts>
               <ts e="T667" id="Seg_5321" n="e" s="T666">buru͡olaːk </ts>
               <ts e="T668" id="Seg_5323" n="e" s="T667">bu͡olu͡o. </ts>
               <ts e="T669" id="Seg_5325" n="e" s="T668">Ol </ts>
               <ts e="T670" id="Seg_5327" n="e" s="T669">bihigi </ts>
               <ts e="T671" id="Seg_5329" n="e" s="T670">törüt </ts>
               <ts e="T672" id="Seg_5331" n="e" s="T671">hirbit. </ts>
               <ts e="T673" id="Seg_5333" n="e" s="T672">Ulakan </ts>
               <ts e="T674" id="Seg_5335" n="e" s="T673">hu͡olga </ts>
               <ts e="T675" id="Seg_5337" n="e" s="T674">kiːri͡eŋ, </ts>
               <ts e="T676" id="Seg_5339" n="e" s="T675">ajmaktargar </ts>
               <ts e="T677" id="Seg_5341" n="e" s="T676">tiji͡eŋ. </ts>
               <ts e="T678" id="Seg_5343" n="e" s="T677">Dʼonnor, </ts>
               <ts e="T679" id="Seg_5345" n="e" s="T678">tabagatɨn </ts>
               <ts e="T680" id="Seg_5347" n="e" s="T679">tüːnü-künü </ts>
               <ts e="T681" id="Seg_5349" n="e" s="T680">bɨha </ts>
               <ts e="T682" id="Seg_5351" n="e" s="T681">körü͡ökküt. </ts>
               <ts e="T683" id="Seg_5353" n="e" s="T682">Agɨs </ts>
               <ts e="T684" id="Seg_5355" n="e" s="T683">koŋnomu͡oj </ts>
               <ts e="T685" id="Seg_5357" n="e" s="T684">küseːjine, </ts>
               <ts e="T686" id="Seg_5359" n="e" s="T685">hette </ts>
               <ts e="T687" id="Seg_5361" n="e" s="T686">timek </ts>
               <ts e="T688" id="Seg_5363" n="e" s="T687">iččite, </ts>
               <ts e="T689" id="Seg_5365" n="e" s="T688">emi͡e </ts>
               <ts e="T690" id="Seg_5367" n="e" s="T689">körsü͡ökküt. </ts>
               <ts e="T691" id="Seg_5369" n="e" s="T690">Min </ts>
               <ts e="T692" id="Seg_5371" n="e" s="T691">bu͡ollagɨna </ts>
               <ts e="T693" id="Seg_5373" n="e" s="T692">bu </ts>
               <ts e="T694" id="Seg_5375" n="e" s="T693">üčügejdik </ts>
               <ts e="T695" id="Seg_5377" n="e" s="T694">ajannɨːrgɨt </ts>
               <ts e="T696" id="Seg_5379" n="e" s="T695">tuhuttan </ts>
               <ts e="T697" id="Seg_5381" n="e" s="T696">kuhagan </ts>
               <ts e="T698" id="Seg_5383" n="e" s="T697">dʼonu — </ts>
               <ts e="T699" id="Seg_5385" n="e" s="T698">čaŋɨttarɨ, </ts>
               <ts e="T700" id="Seg_5387" n="e" s="T699">kɨːllarɨ </ts>
               <ts e="T701" id="Seg_5389" n="e" s="T700">hu͡ok </ts>
               <ts e="T702" id="Seg_5391" n="e" s="T701">gɨna </ts>
               <ts e="T703" id="Seg_5393" n="e" s="T702">bardɨm. </ts>
               <ts e="T704" id="Seg_5395" n="e" s="T703">Ol </ts>
               <ts e="T705" id="Seg_5397" n="e" s="T704">hirge </ts>
               <ts e="T706" id="Seg_5399" n="e" s="T705">tijer </ts>
               <ts e="T707" id="Seg_5401" n="e" s="T706">küŋŋer </ts>
               <ts e="T708" id="Seg_5403" n="e" s="T707">tiji͡em, </ts>
               <ts e="T709" id="Seg_5405" n="e" s="T708">ulakannɨk </ts>
               <ts e="T710" id="Seg_5407" n="e" s="T709">haːraːtakpɨna </ts>
               <ts e="T711" id="Seg_5409" n="e" s="T710">ühüs </ts>
               <ts e="T712" id="Seg_5411" n="e" s="T711">künüger </ts>
               <ts e="T713" id="Seg_5413" n="e" s="T712">tiji͡em", </ts>
               <ts e="T714" id="Seg_5415" n="e" s="T713">diːr. </ts>
               <ts e="T715" id="Seg_5417" n="e" s="T714">Ühüs </ts>
               <ts e="T716" id="Seg_5419" n="e" s="T715">künüger </ts>
               <ts e="T717" id="Seg_5421" n="e" s="T716">köhöllör. </ts>
               <ts e="T718" id="Seg_5423" n="e" s="T717">Kuŋadʼaj </ts>
               <ts e="T719" id="Seg_5425" n="e" s="T718">aragan </ts>
               <ts e="T720" id="Seg_5427" n="e" s="T719">barar, </ts>
               <ts e="T721" id="Seg_5429" n="e" s="T720">hu͡os </ts>
               <ts e="T722" id="Seg_5431" n="e" s="T721">hatɨː. </ts>
               <ts e="T723" id="Seg_5433" n="e" s="T722">Baː </ts>
               <ts e="T724" id="Seg_5435" n="e" s="T723">ogo </ts>
               <ts e="T725" id="Seg_5437" n="e" s="T724">bastɨŋ </ts>
               <ts e="T726" id="Seg_5439" n="e" s="T725">bu͡olan </ts>
               <ts e="T727" id="Seg_5441" n="e" s="T726">köhön </ts>
               <ts e="T728" id="Seg_5443" n="e" s="T727">istiler. </ts>
               <ts e="T729" id="Seg_5445" n="e" s="T728">Ketenen </ts>
               <ts e="T730" id="Seg_5447" n="e" s="T729">ajannɨːllar, </ts>
               <ts e="T731" id="Seg_5449" n="e" s="T730">tojonu-kotunu </ts>
               <ts e="T732" id="Seg_5451" n="e" s="T731">barɨtɨn </ts>
               <ts e="T733" id="Seg_5453" n="e" s="T732">keteter </ts>
               <ts e="T734" id="Seg_5455" n="e" s="T733">bu </ts>
               <ts e="T735" id="Seg_5457" n="e" s="T734">u͡ol. </ts>
               <ts e="T736" id="Seg_5459" n="e" s="T735">ɨjɨn </ts>
               <ts e="T737" id="Seg_5461" n="e" s="T736">ortoto </ts>
               <ts e="T738" id="Seg_5463" n="e" s="T737">bu͡olbut. </ts>
               <ts e="T739" id="Seg_5465" n="e" s="T738">Dʼirbiːtin </ts>
               <ts e="T740" id="Seg_5467" n="e" s="T739">irdeːn </ts>
               <ts e="T741" id="Seg_5469" n="e" s="T740">istege. </ts>
               <ts e="T742" id="Seg_5471" n="e" s="T741">Araj </ts>
               <ts e="T743" id="Seg_5473" n="e" s="T742">mu͡ora </ts>
               <ts e="T744" id="Seg_5475" n="e" s="T743">di͡eki </ts>
               <ts e="T745" id="Seg_5477" n="e" s="T744">dʼirbiː </ts>
               <ts e="T746" id="Seg_5479" n="e" s="T745">aragar, </ts>
               <ts e="T747" id="Seg_5481" n="e" s="T746">ürdüger </ts>
               <ts e="T748" id="Seg_5483" n="e" s="T747">kihi </ts>
               <ts e="T749" id="Seg_5485" n="e" s="T748">hɨldʼar. </ts>
               <ts e="T750" id="Seg_5487" n="e" s="T749">"Kaja </ts>
               <ts e="T751" id="Seg_5489" n="e" s="T750">di͡ekki </ts>
               <ts e="T752" id="Seg_5491" n="e" s="T751">kihiginij?" </ts>
               <ts e="T753" id="Seg_5493" n="e" s="T752">"Tu͡ok </ts>
               <ts e="T754" id="Seg_5495" n="e" s="T753">keli͡ej? </ts>
               <ts e="T755" id="Seg_5497" n="e" s="T754">Dʼi͡eni-u͡otu </ts>
               <ts e="T756" id="Seg_5499" n="e" s="T755">bilbeppin. </ts>
               <ts e="T757" id="Seg_5501" n="e" s="T756">Töröːbüt </ts>
               <ts e="T758" id="Seg_5503" n="e" s="T757">ubajbɨn </ts>
               <ts e="T759" id="Seg_5505" n="e" s="T758">hütere </ts>
               <ts e="T760" id="Seg_5507" n="e" s="T759">hɨldʼabɨn." </ts>
               <ts e="T761" id="Seg_5509" n="e" s="T760">"Kajalaraj </ts>
               <ts e="T762" id="Seg_5511" n="e" s="T761">ol?" </ts>
               <ts e="T763" id="Seg_5513" n="e" s="T762">"Agɨs </ts>
               <ts e="T764" id="Seg_5515" n="e" s="T763">koŋnomu͡oj </ts>
               <ts e="T765" id="Seg_5517" n="e" s="T764">küseːjine!" </ts>
               <ts e="T766" id="Seg_5519" n="e" s="T765">"Heːj! </ts>
               <ts e="T767" id="Seg_5521" n="e" s="T766">Ol </ts>
               <ts e="T768" id="Seg_5523" n="e" s="T767">min </ts>
               <ts e="T769" id="Seg_5525" n="e" s="T768">kihim. </ts>
               <ts e="T770" id="Seg_5527" n="e" s="T769">Ol </ts>
               <ts e="T771" id="Seg_5529" n="e" s="T770">delemičeː </ts>
               <ts e="T772" id="Seg_5531" n="e" s="T771">üːren </ts>
               <ts e="T773" id="Seg_5533" n="e" s="T772">iher. </ts>
               <ts e="T774" id="Seg_5535" n="e" s="T773">Dʼe </ts>
               <ts e="T775" id="Seg_5537" n="e" s="T774">oččogo </ts>
               <ts e="T776" id="Seg_5539" n="e" s="T775">mi͡eke </ts>
               <ts e="T777" id="Seg_5541" n="e" s="T776">kihi </ts>
               <ts e="T778" id="Seg_5543" n="e" s="T777">bu͡ol." </ts>
               <ts e="T779" id="Seg_5545" n="e" s="T778">Haŋara </ts>
               <ts e="T780" id="Seg_5547" n="e" s="T779">tarpat. </ts>
               <ts e="T781" id="Seg_5549" n="e" s="T780">I͡ehili </ts>
               <ts e="T782" id="Seg_5551" n="e" s="T781">kihi </ts>
               <ts e="T783" id="Seg_5553" n="e" s="T782">bu͡olu͡oŋ </ts>
               <ts e="T784" id="Seg_5555" n="e" s="T783">hu͡oga, </ts>
               <ts e="T785" id="Seg_5557" n="e" s="T784">bu </ts>
               <ts e="T786" id="Seg_5559" n="e" s="T785">üs </ts>
               <ts e="T787" id="Seg_5561" n="e" s="T786">halaːlaːk </ts>
               <ts e="T788" id="Seg_5563" n="e" s="T787">kürejim </ts>
               <ts e="T789" id="Seg_5565" n="e" s="T788">aŋar </ts>
               <ts e="T790" id="Seg_5567" n="e" s="T789">halaːta </ts>
               <ts e="T791" id="Seg_5569" n="e" s="T790">kaŋɨrgaha </ts>
               <ts e="T792" id="Seg_5571" n="e" s="T791">bu͡olu͡oŋ, </ts>
               <ts e="T793" id="Seg_5573" n="e" s="T792">ikkite </ts>
               <ts e="T794" id="Seg_5575" n="e" s="T793">bukatɨːr </ts>
               <ts e="T795" id="Seg_5577" n="e" s="T794">eːldete </ts>
               <ts e="T796" id="Seg_5579" n="e" s="T795">kaŋɨrgastaːk." </ts>
               <ts e="T797" id="Seg_5581" n="e" s="T796">Kihi </ts>
               <ts e="T798" id="Seg_5583" n="e" s="T797">höbülener, </ts>
               <ts e="T799" id="Seg_5585" n="e" s="T798">barsar. </ts>
               <ts e="T800" id="Seg_5587" n="e" s="T799">Köhön </ts>
               <ts e="T801" id="Seg_5589" n="e" s="T800">istiler. </ts>
               <ts e="T802" id="Seg_5591" n="e" s="T801">ɨjdara </ts>
               <ts e="T803" id="Seg_5593" n="e" s="T802">baranna. </ts>
               <ts e="T804" id="Seg_5595" n="e" s="T803">Nöŋü͡ö </ts>
               <ts e="T805" id="Seg_5597" n="e" s="T804">ɨj </ts>
               <ts e="T806" id="Seg_5599" n="e" s="T805">ortoto </ts>
               <ts e="T807" id="Seg_5601" n="e" s="T806">bu͡olla. </ts>
               <ts e="T808" id="Seg_5603" n="e" s="T807">Emi͡e </ts>
               <ts e="T809" id="Seg_5605" n="e" s="T808">kihini </ts>
               <ts e="T810" id="Seg_5607" n="e" s="T809">kördüler </ts>
               <ts e="T811" id="Seg_5609" n="e" s="T810">biːr </ts>
               <ts e="T812" id="Seg_5611" n="e" s="T811">dʼirbiːkeːŋŋe. </ts>
               <ts e="T813" id="Seg_5613" n="e" s="T812">Emi͡e </ts>
               <ts e="T814" id="Seg_5615" n="e" s="T813">doroːbolohor. </ts>
               <ts e="T815" id="Seg_5617" n="e" s="T814">"Kaja </ts>
               <ts e="T816" id="Seg_5619" n="e" s="T815">di͡eki </ts>
               <ts e="T817" id="Seg_5621" n="e" s="T816">dʼi͡eleːk-u͡ottaːk </ts>
               <ts e="T818" id="Seg_5623" n="e" s="T817">kihiginij?" </ts>
               <ts e="T819" id="Seg_5625" n="e" s="T818">"Dʼi͡e-u͡ot </ts>
               <ts e="T820" id="Seg_5627" n="e" s="T819">hu͡ok. </ts>
               <ts e="T821" id="Seg_5629" n="e" s="T820">Hɨldʼabɨn </ts>
               <ts e="T822" id="Seg_5631" n="e" s="T821">agaj. </ts>
               <ts e="T823" id="Seg_5633" n="e" s="T822">Ubajbɨn </ts>
               <ts e="T824" id="Seg_5635" n="e" s="T823">kördüːbün." </ts>
               <ts e="T825" id="Seg_5637" n="e" s="T824">"Tu͡ok </ts>
               <ts e="T826" id="Seg_5639" n="e" s="T825">etej </ts>
               <ts e="T827" id="Seg_5641" n="e" s="T826">ubajɨn?" </ts>
               <ts e="T828" id="Seg_5643" n="e" s="T827">"Eː, </ts>
               <ts e="T829" id="Seg_5645" n="e" s="T828">hette </ts>
               <ts e="T830" id="Seg_5647" n="e" s="T829">timek </ts>
               <ts e="T831" id="Seg_5649" n="e" s="T830">iččite!" </ts>
               <ts e="T832" id="Seg_5651" n="e" s="T831">"Eː, </ts>
               <ts e="T833" id="Seg_5653" n="e" s="T832">ol </ts>
               <ts e="T834" id="Seg_5655" n="e" s="T833">delemičeː </ts>
               <ts e="T835" id="Seg_5657" n="e" s="T834">üːren </ts>
               <ts e="T836" id="Seg_5659" n="e" s="T835">iher </ts>
               <ts e="T837" id="Seg_5661" n="e" s="T836">ubajɨŋ. </ts>
               <ts e="T838" id="Seg_5663" n="e" s="T837">Kaja </ts>
               <ts e="T839" id="Seg_5665" n="e" s="T838">mi͡eke </ts>
               <ts e="T840" id="Seg_5667" n="e" s="T839">kihi </ts>
               <ts e="T841" id="Seg_5669" n="e" s="T840">bu͡olu͡oŋ?" </ts>
               <ts e="T842" id="Seg_5671" n="e" s="T841">Haŋarbat. </ts>
               <ts e="T843" id="Seg_5673" n="e" s="T842">"Bu͡olu͡oŋ </ts>
               <ts e="T844" id="Seg_5675" n="e" s="T843">hu͡oga, </ts>
               <ts e="T845" id="Seg_5677" n="e" s="T844">bu </ts>
               <ts e="T846" id="Seg_5679" n="e" s="T845">kürejim </ts>
               <ts e="T847" id="Seg_5681" n="e" s="T846">halaːtɨgar </ts>
               <ts e="T848" id="Seg_5683" n="e" s="T847">kaŋɨrgas </ts>
               <ts e="T849" id="Seg_5685" n="e" s="T848">gɨnɨ͡am!" </ts>
               <ts e="T850" id="Seg_5687" n="e" s="T849">Höbüleher </ts>
               <ts e="T851" id="Seg_5689" n="e" s="T850">bili </ts>
               <ts e="T852" id="Seg_5691" n="e" s="T851">kihi. </ts>
               <ts e="T853" id="Seg_5693" n="e" s="T852">Ubajɨgar </ts>
               <ts e="T854" id="Seg_5695" n="e" s="T853">tijer: </ts>
               <ts e="T855" id="Seg_5697" n="e" s="T854">"Togo </ts>
               <ts e="T856" id="Seg_5699" n="e" s="T855">ölörbökküt? </ts>
               <ts e="T857" id="Seg_5701" n="e" s="T856">Ogogo </ts>
               <ts e="T858" id="Seg_5703" n="e" s="T857">bas </ts>
               <ts e="T859" id="Seg_5705" n="e" s="T858">berinegit </ts>
               <ts e="T860" id="Seg_5707" n="e" s="T859">du͡o?" </ts>
               <ts e="T861" id="Seg_5709" n="e" s="T860">"Hu͡ok, </ts>
               <ts e="T862" id="Seg_5711" n="e" s="T861">kebis." </ts>
               <ts e="T863" id="Seg_5713" n="e" s="T862">"Bultu͡okka </ts>
               <ts e="T864" id="Seg_5715" n="e" s="T863">naːda. </ts>
               <ts e="T865" id="Seg_5717" n="e" s="T864">Kohuta </ts>
               <ts e="T866" id="Seg_5719" n="e" s="T865">da </ts>
               <ts e="T867" id="Seg_5721" n="e" s="T866">bert." </ts>
               <ts e="T868" id="Seg_5723" n="e" s="T867">"Če, </ts>
               <ts e="T869" id="Seg_5725" n="e" s="T868">bagalɨ͡akpɨt!" </ts>
               <ts e="T870" id="Seg_5727" n="e" s="T869">Ki͡ehe </ts>
               <ts e="T871" id="Seg_5729" n="e" s="T870">tüheller </ts>
               <ts e="T872" id="Seg_5731" n="e" s="T871">biːr </ts>
               <ts e="T873" id="Seg_5733" n="e" s="T872">hirge. </ts>
               <ts e="T874" id="Seg_5735" n="e" s="T873">Ahɨːllar, </ts>
               <ts e="T875" id="Seg_5737" n="e" s="T874">u͡ol </ts>
               <ts e="T876" id="Seg_5739" n="e" s="T875">dʼonun </ts>
               <ts e="T877" id="Seg_5741" n="e" s="T876">batan </ts>
               <ts e="T878" id="Seg_5743" n="e" s="T877">tahaːrar </ts>
               <ts e="T879" id="Seg_5745" n="e" s="T878">ü͡ör </ts>
               <ts e="T880" id="Seg_5747" n="e" s="T879">ketete. </ts>
               <ts e="T881" id="Seg_5749" n="e" s="T880">Ogo </ts>
               <ts e="T882" id="Seg_5751" n="e" s="T881">utujan </ts>
               <ts e="T883" id="Seg_5753" n="e" s="T882">kaːlar. </ts>
               <ts e="T884" id="Seg_5755" n="e" s="T883">Kihiler </ts>
               <ts e="T885" id="Seg_5757" n="e" s="T884">kalɨjar </ts>
               <ts e="T886" id="Seg_5759" n="e" s="T885">haŋalarɨn </ts>
               <ts e="T887" id="Seg_5761" n="e" s="T886">ister. </ts>
               <ts e="T888" id="Seg_5763" n="e" s="T887">Kihi </ts>
               <ts e="T889" id="Seg_5765" n="e" s="T888">batɨjannan </ts>
               <ts e="T890" id="Seg_5767" n="e" s="T889">aːnɨ </ts>
               <ts e="T891" id="Seg_5769" n="e" s="T890">hegeter: </ts>
               <ts e="T892" id="Seg_5771" n="e" s="T891">"Kaja, </ts>
               <ts e="T893" id="Seg_5773" n="e" s="T892">togo </ts>
               <ts e="T894" id="Seg_5775" n="e" s="T893">ketiːgit </ts>
               <ts e="T895" id="Seg_5777" n="e" s="T894">miːgin, </ts>
               <ts e="T896" id="Seg_5779" n="e" s="T895">ü͡örü </ts>
               <ts e="T897" id="Seg_5781" n="e" s="T896">keteːŋ!" </ts>
               <ts e="T898" id="Seg_5783" n="e" s="T897">Dʼon </ts>
               <ts e="T899" id="Seg_5785" n="e" s="T898">töttörü </ts>
               <ts e="T900" id="Seg_5787" n="e" s="T899">hahallar. </ts>
               <ts e="T901" id="Seg_5789" n="e" s="T900">"Harsi͡erdaːŋŋɨ </ts>
               <ts e="T902" id="Seg_5791" n="e" s="T901">uːtugar </ts>
               <ts e="T903" id="Seg_5793" n="e" s="T902">bagalɨ͡akpɨt", </ts>
               <ts e="T904" id="Seg_5795" n="e" s="T903">deheller. </ts>
               <ts e="T905" id="Seg_5797" n="e" s="T904">Emi͡e </ts>
               <ts e="T906" id="Seg_5799" n="e" s="T905">bürüːrdüːller. </ts>
               <ts e="T907" id="Seg_5801" n="e" s="T906">Emi͡e </ts>
               <ts e="T908" id="Seg_5803" n="e" s="T907">"tugu </ts>
               <ts e="T909" id="Seg_5805" n="e" s="T908">ketiːgit? </ts>
               <ts e="T910" id="Seg_5807" n="e" s="T909">Barɨŋ", </ts>
               <ts e="T911" id="Seg_5809" n="e" s="T910">batan </ts>
               <ts e="T912" id="Seg_5811" n="e" s="T911">ɨːtar. </ts>
               <ts e="T913" id="Seg_5813" n="e" s="T912">Honton </ts>
               <ts e="T914" id="Seg_5815" n="e" s="T913">tɨːppataktara. </ts>
               <ts e="T915" id="Seg_5817" n="e" s="T914">Ikki </ts>
               <ts e="T916" id="Seg_5819" n="e" s="T915">ɨjdara </ts>
               <ts e="T917" id="Seg_5821" n="e" s="T916">bɨstarɨgar </ts>
               <ts e="T918" id="Seg_5823" n="e" s="T917">dʼirbiːlere </ts>
               <ts e="T919" id="Seg_5825" n="e" s="T918">elete </ts>
               <ts e="T920" id="Seg_5827" n="e" s="T919">bu͡olar. </ts>
               <ts e="T921" id="Seg_5829" n="e" s="T920">Körbüte — </ts>
               <ts e="T922" id="Seg_5831" n="e" s="T921">hir </ts>
               <ts e="T923" id="Seg_5833" n="e" s="T922">bu͡olbut. </ts>
               <ts e="T924" id="Seg_5835" n="e" s="T923">Kaːr </ts>
               <ts e="T925" id="Seg_5837" n="e" s="T924">annɨttan </ts>
               <ts e="T926" id="Seg_5839" n="e" s="T925">buru͡okaːkɨna. </ts>
               <ts e="T927" id="Seg_5841" n="e" s="T926">Mu͡ora </ts>
               <ts e="T928" id="Seg_5843" n="e" s="T927">di͡ekkitten </ts>
               <ts e="T929" id="Seg_5845" n="e" s="T928">kütür </ts>
               <ts e="T930" id="Seg_5847" n="e" s="T929">bagajɨ </ts>
               <ts e="T931" id="Seg_5849" n="e" s="T930">orok </ts>
               <ts e="T932" id="Seg_5851" n="e" s="T931">keler. </ts>
               <ts e="T933" id="Seg_5853" n="e" s="T932">Hir </ts>
               <ts e="T934" id="Seg_5855" n="e" s="T933">ihin </ts>
               <ts e="T935" id="Seg_5857" n="e" s="T934">di͡eki </ts>
               <ts e="T936" id="Seg_5859" n="e" s="T935">barar </ts>
               <ts e="T937" id="Seg_5861" n="e" s="T936">orok. </ts>
               <ts e="T938" id="Seg_5863" n="e" s="T937">Baran </ts>
               <ts e="T939" id="Seg_5865" n="e" s="T938">iheller. </ts>
               <ts e="T940" id="Seg_5867" n="e" s="T939">Pireːme </ts>
               <ts e="T941" id="Seg_5869" n="e" s="T940">noru͡ottan </ts>
               <ts e="T942" id="Seg_5871" n="e" s="T941">noru͡ot, </ts>
               <ts e="T943" id="Seg_5873" n="e" s="T942">dʼi͡etten </ts>
               <ts e="T944" id="Seg_5875" n="e" s="T943">dʼi͡e </ts>
               <ts e="T945" id="Seg_5877" n="e" s="T944">hirge </ts>
               <ts e="T946" id="Seg_5879" n="e" s="T945">keleller. </ts>
               <ts e="T947" id="Seg_5881" n="e" s="T946">Biːr </ts>
               <ts e="T948" id="Seg_5883" n="e" s="T947">dʼi͡e </ts>
               <ts e="T949" id="Seg_5885" n="e" s="T948">tahɨgar </ts>
               <ts e="T950" id="Seg_5887" n="e" s="T949">toktuːr. </ts>
               <ts e="T951" id="Seg_5889" n="e" s="T950">Biːr </ts>
               <ts e="T952" id="Seg_5891" n="e" s="T951">kɨrdʼagas </ts>
               <ts e="T953" id="Seg_5893" n="e" s="T952">hogus </ts>
               <ts e="T954" id="Seg_5895" n="e" s="T953">dʼaktar </ts>
               <ts e="T955" id="Seg_5897" n="e" s="T954">taksar: </ts>
               <ts e="T956" id="Seg_5899" n="e" s="T955">"Baj, </ts>
               <ts e="T957" id="Seg_5901" n="e" s="T956">tabata </ts>
               <ts e="T958" id="Seg_5903" n="e" s="T957">oduː </ts>
               <ts e="T959" id="Seg_5905" n="e" s="T958">taba! </ts>
               <ts e="T960" id="Seg_5907" n="e" s="T959">Bihigi </ts>
               <ts e="T961" id="Seg_5909" n="e" s="T960">törüt </ts>
               <ts e="T962" id="Seg_5911" n="e" s="T961">tababɨt </ts>
               <ts e="T963" id="Seg_5913" n="e" s="T962">kurduguj? </ts>
               <ts e="T964" id="Seg_5915" n="e" s="T963">Bɨlɨr </ts>
               <ts e="T965" id="Seg_5917" n="e" s="T964">ikki </ts>
               <ts e="T966" id="Seg_5919" n="e" s="T965">kihi </ts>
               <ts e="T967" id="Seg_5921" n="e" s="T966">barbɨta, </ts>
               <ts e="T968" id="Seg_5923" n="e" s="T967">ol </ts>
               <ts e="T969" id="Seg_5925" n="e" s="T968">törüttere </ts>
               <ts e="T970" id="Seg_5927" n="e" s="T969">bu͡olu͡o!" </ts>
               <ts e="T971" id="Seg_5929" n="e" s="T970">Üs </ts>
               <ts e="T972" id="Seg_5931" n="e" s="T971">dʼi͡etten </ts>
               <ts e="T973" id="Seg_5933" n="e" s="T972">üs </ts>
               <ts e="T974" id="Seg_5935" n="e" s="T973">dʼaktar </ts>
               <ts e="T975" id="Seg_5937" n="e" s="T974">taksan </ts>
               <ts e="T976" id="Seg_5939" n="e" s="T975">emi͡e </ts>
               <ts e="T977" id="Seg_5941" n="e" s="T976">taːjbɨttar </ts>
               <ts e="T978" id="Seg_5943" n="e" s="T977">törütterin, </ts>
               <ts e="T979" id="Seg_5945" n="e" s="T978">kepseten </ts>
               <ts e="T980" id="Seg_5947" n="e" s="T979">dʼe </ts>
               <ts e="T981" id="Seg_5949" n="e" s="T980">bilseller. </ts>
               <ts e="T982" id="Seg_5951" n="e" s="T981">Uruːlarɨn </ts>
               <ts e="T983" id="Seg_5953" n="e" s="T982">bulsallar. </ts>
               <ts e="T984" id="Seg_5955" n="e" s="T983">Kuŋadʼajɨ </ts>
               <ts e="T985" id="Seg_5957" n="e" s="T984">küːteller. </ts>
               <ts e="T986" id="Seg_5959" n="e" s="T985">Ühüs </ts>
               <ts e="T987" id="Seg_5961" n="e" s="T986">künün </ts>
               <ts e="T988" id="Seg_5963" n="e" s="T987">ortoto </ts>
               <ts e="T989" id="Seg_5965" n="e" s="T988">Kuŋadʼaj </ts>
               <ts e="T990" id="Seg_5967" n="e" s="T989">keler. </ts>
               <ts e="T991" id="Seg_5969" n="e" s="T990">Et </ts>
               <ts e="T992" id="Seg_5971" n="e" s="T991">aːta </ts>
               <ts e="T993" id="Seg_5973" n="e" s="T992">bütüne </ts>
               <ts e="T994" id="Seg_5975" n="e" s="T993">hu͡ok, </ts>
               <ts e="T995" id="Seg_5977" n="e" s="T994">tiriːtin </ts>
               <ts e="T996" id="Seg_5979" n="e" s="T995">aŋara </ts>
               <ts e="T997" id="Seg_5981" n="e" s="T996">kaːlbɨt. </ts>
               <ts e="T998" id="Seg_5983" n="e" s="T997">"Ogom, </ts>
               <ts e="T999" id="Seg_5985" n="e" s="T998">kelbikkin — </ts>
               <ts e="T1000" id="Seg_5987" n="e" s="T999">dʼe </ts>
               <ts e="T1001" id="Seg_5989" n="e" s="T1000">üčügej!" </ts>
               <ts e="T1002" id="Seg_5991" n="e" s="T1001">Kuhagan </ts>
               <ts e="T1003" id="Seg_5993" n="e" s="T1002">dʼonu </ts>
               <ts e="T1004" id="Seg_5995" n="e" s="T1003">baraːn, </ts>
               <ts e="T1005" id="Seg_5997" n="e" s="T1004">dojdu </ts>
               <ts e="T1006" id="Seg_5999" n="e" s="T1005">buldun </ts>
               <ts e="T1007" id="Seg_6001" n="e" s="T1006">arɨčča </ts>
               <ts e="T1008" id="Seg_6003" n="e" s="T1007">kɨ͡ajdɨm. </ts>
               <ts e="T1009" id="Seg_6005" n="e" s="T1008">Tɨːnɨm </ts>
               <ts e="T1010" id="Seg_6007" n="e" s="T1009">ere </ts>
               <ts e="T1011" id="Seg_6009" n="e" s="T1010">kellim, </ts>
               <ts e="T1012" id="Seg_6011" n="e" s="T1011">gojobuːn </ts>
               <ts e="T1013" id="Seg_6013" n="e" s="T1012">oŋoronnor." </ts>
               <ts e="T1014" id="Seg_6015" n="e" s="T1013">"Dʼe </ts>
               <ts e="T1015" id="Seg_6017" n="e" s="T1014">olor, </ts>
               <ts e="T1016" id="Seg_6019" n="e" s="T1015">tu͡oktan </ts>
               <ts e="T1017" id="Seg_6021" n="e" s="T1016">da </ts>
               <ts e="T1018" id="Seg_6023" n="e" s="T1017">kuttanɨma!" </ts>
               <ts e="T1019" id="Seg_6025" n="e" s="T1018">Kuŋadʼaj </ts>
               <ts e="T1020" id="Seg_6027" n="e" s="T1019">dʼe </ts>
               <ts e="T1021" id="Seg_6029" n="e" s="T1020">kojut </ts>
               <ts e="T1022" id="Seg_6031" n="e" s="T1021">tiller. </ts>
               <ts e="T1023" id="Seg_6033" n="e" s="T1022">U͡ol </ts>
               <ts e="T1024" id="Seg_6035" n="e" s="T1023">küseːjin </ts>
               <ts e="T1025" id="Seg_6037" n="e" s="T1024">bu͡olan </ts>
               <ts e="T1026" id="Seg_6039" n="e" s="T1025">olorbuta </ts>
               <ts e="T1027" id="Seg_6041" n="e" s="T1026">bu </ts>
               <ts e="T1028" id="Seg_6043" n="e" s="T1027">omukka. </ts>
               <ts e="T1029" id="Seg_6045" n="e" s="T1028">Elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_6046" s="T0">BoND_1964_ThreeBrothers_flk.001 (001.001)</ta>
            <ta e="T14" id="Seg_6047" s="T3">BoND_1964_ThreeBrothers_flk.002 (001.002)</ta>
            <ta e="T22" id="Seg_6048" s="T14">BoND_1964_ThreeBrothers_flk.003 (001.003)</ta>
            <ta e="T26" id="Seg_6049" s="T22">BoND_1964_ThreeBrothers_flk.004 (001.004)</ta>
            <ta e="T32" id="Seg_6050" s="T26">BoND_1964_ThreeBrothers_flk.005 (001.005)</ta>
            <ta e="T35" id="Seg_6051" s="T32">BoND_1964_ThreeBrothers_flk.006 (001.006)</ta>
            <ta e="T39" id="Seg_6052" s="T35">BoND_1964_ThreeBrothers_flk.007 (001.007)</ta>
            <ta e="T52" id="Seg_6053" s="T39">BoND_1964_ThreeBrothers_flk.008 (001.008)</ta>
            <ta e="T55" id="Seg_6054" s="T52">BoND_1964_ThreeBrothers_flk.009 (001.008)</ta>
            <ta e="T57" id="Seg_6055" s="T55">BoND_1964_ThreeBrothers_flk.010 (001.009)</ta>
            <ta e="T62" id="Seg_6056" s="T57">BoND_1964_ThreeBrothers_flk.011 (001.010)</ta>
            <ta e="T70" id="Seg_6057" s="T62">BoND_1964_ThreeBrothers_flk.012 (001.011)</ta>
            <ta e="T75" id="Seg_6058" s="T70">BoND_1964_ThreeBrothers_flk.013 (001.012)</ta>
            <ta e="T77" id="Seg_6059" s="T75">BoND_1964_ThreeBrothers_flk.014 (001.013)</ta>
            <ta e="T84" id="Seg_6060" s="T77">BoND_1964_ThreeBrothers_flk.015 (001.014)</ta>
            <ta e="T85" id="Seg_6061" s="T84">BoND_1964_ThreeBrothers_flk.016 (001.015)</ta>
            <ta e="T87" id="Seg_6062" s="T85">BoND_1964_ThreeBrothers_flk.017 (001.016)</ta>
            <ta e="T91" id="Seg_6063" s="T87">BoND_1964_ThreeBrothers_flk.018 (001.017)</ta>
            <ta e="T94" id="Seg_6064" s="T91">BoND_1964_ThreeBrothers_flk.019 (001.018)</ta>
            <ta e="T96" id="Seg_6065" s="T94">BoND_1964_ThreeBrothers_flk.020 (001.019)</ta>
            <ta e="T102" id="Seg_6066" s="T96">BoND_1964_ThreeBrothers_flk.021 (001.020)</ta>
            <ta e="T104" id="Seg_6067" s="T102">BoND_1964_ThreeBrothers_flk.022 (001.021)</ta>
            <ta e="T106" id="Seg_6068" s="T104">BoND_1964_ThreeBrothers_flk.023 (001.022)</ta>
            <ta e="T109" id="Seg_6069" s="T106">BoND_1964_ThreeBrothers_flk.024 (001.023)</ta>
            <ta e="T116" id="Seg_6070" s="T109">BoND_1964_ThreeBrothers_flk.025 (001.024)</ta>
            <ta e="T124" id="Seg_6071" s="T116">BoND_1964_ThreeBrothers_flk.026 (001.025)</ta>
            <ta e="T133" id="Seg_6072" s="T124">BoND_1964_ThreeBrothers_flk.027 (001.026)</ta>
            <ta e="T136" id="Seg_6073" s="T133">BoND_1964_ThreeBrothers_flk.028 (001.027)</ta>
            <ta e="T139" id="Seg_6074" s="T136">BoND_1964_ThreeBrothers_flk.029 (001.028)</ta>
            <ta e="T141" id="Seg_6075" s="T139">BoND_1964_ThreeBrothers_flk.030 (001.029)</ta>
            <ta e="T143" id="Seg_6076" s="T141">BoND_1964_ThreeBrothers_flk.031 (001.030)</ta>
            <ta e="T147" id="Seg_6077" s="T143">BoND_1964_ThreeBrothers_flk.032 (001.031)</ta>
            <ta e="T150" id="Seg_6078" s="T147">BoND_1964_ThreeBrothers_flk.033 (001.032)</ta>
            <ta e="T153" id="Seg_6079" s="T150">BoND_1964_ThreeBrothers_flk.034 (001.033)</ta>
            <ta e="T160" id="Seg_6080" s="T153">BoND_1964_ThreeBrothers_flk.035 (001.034)</ta>
            <ta e="T164" id="Seg_6081" s="T160">BoND_1964_ThreeBrothers_flk.036 (001.035)</ta>
            <ta e="T168" id="Seg_6082" s="T164">BoND_1964_ThreeBrothers_flk.037 (001.036)</ta>
            <ta e="T171" id="Seg_6083" s="T168">BoND_1964_ThreeBrothers_flk.038 (001.037)</ta>
            <ta e="T173" id="Seg_6084" s="T171">BoND_1964_ThreeBrothers_flk.039 (001.037)</ta>
            <ta e="T175" id="Seg_6085" s="T173">BoND_1964_ThreeBrothers_flk.040 (001.038)</ta>
            <ta e="T178" id="Seg_6086" s="T175">BoND_1964_ThreeBrothers_flk.041 (001.039)</ta>
            <ta e="T185" id="Seg_6087" s="T178">BoND_1964_ThreeBrothers_flk.042 (001.040)</ta>
            <ta e="T188" id="Seg_6088" s="T185">BoND_1964_ThreeBrothers_flk.043 (001.041)</ta>
            <ta e="T192" id="Seg_6089" s="T188">BoND_1964_ThreeBrothers_flk.044 (001.042)</ta>
            <ta e="T206" id="Seg_6090" s="T192">BoND_1964_ThreeBrothers_flk.045 (001.043)</ta>
            <ta e="T212" id="Seg_6091" s="T206">BoND_1964_ThreeBrothers_flk.046 (001.044)</ta>
            <ta e="T215" id="Seg_6092" s="T212">BoND_1964_ThreeBrothers_flk.047 (001.045)</ta>
            <ta e="T223" id="Seg_6093" s="T215">BoND_1964_ThreeBrothers_flk.048 (001.047)</ta>
            <ta e="T228" id="Seg_6094" s="T223">BoND_1964_ThreeBrothers_flk.049 (001.048)</ta>
            <ta e="T235" id="Seg_6095" s="T228">BoND_1964_ThreeBrothers_flk.050 (001.049)</ta>
            <ta e="T239" id="Seg_6096" s="T235">BoND_1964_ThreeBrothers_flk.051 (001.050)</ta>
            <ta e="T242" id="Seg_6097" s="T239">BoND_1964_ThreeBrothers_flk.052 (001.051)</ta>
            <ta e="T245" id="Seg_6098" s="T242">BoND_1964_ThreeBrothers_flk.053 (001.052)</ta>
            <ta e="T258" id="Seg_6099" s="T245">BoND_1964_ThreeBrothers_flk.054 (001.053)</ta>
            <ta e="T262" id="Seg_6100" s="T258">BoND_1964_ThreeBrothers_flk.055 (001.054)</ta>
            <ta e="T266" id="Seg_6101" s="T262">BoND_1964_ThreeBrothers_flk.056 (001.055)</ta>
            <ta e="T270" id="Seg_6102" s="T266">BoND_1964_ThreeBrothers_flk.057 (001.056)</ta>
            <ta e="T283" id="Seg_6103" s="T270">BoND_1964_ThreeBrothers_flk.058 (001.058)</ta>
            <ta e="T286" id="Seg_6104" s="T283">BoND_1964_ThreeBrothers_flk.059 (001.059)</ta>
            <ta e="T291" id="Seg_6105" s="T286">BoND_1964_ThreeBrothers_flk.060 (001.060)</ta>
            <ta e="T292" id="Seg_6106" s="T291">BoND_1964_ThreeBrothers_flk.061 (001.062)</ta>
            <ta e="T294" id="Seg_6107" s="T292">BoND_1964_ThreeBrothers_flk.062 (001.063)</ta>
            <ta e="T298" id="Seg_6108" s="T294">BoND_1964_ThreeBrothers_flk.063 (001.064)</ta>
            <ta e="T303" id="Seg_6109" s="T298">BoND_1964_ThreeBrothers_flk.064 (001.065)</ta>
            <ta e="T306" id="Seg_6110" s="T303">BoND_1964_ThreeBrothers_flk.065 (001.066)</ta>
            <ta e="T311" id="Seg_6111" s="T306">BoND_1964_ThreeBrothers_flk.066 (001.067)</ta>
            <ta e="T314" id="Seg_6112" s="T311">BoND_1964_ThreeBrothers_flk.067 (001.068)</ta>
            <ta e="T316" id="Seg_6113" s="T314">BoND_1964_ThreeBrothers_flk.068 (001.069)</ta>
            <ta e="T318" id="Seg_6114" s="T316">BoND_1964_ThreeBrothers_flk.069 (001.070)</ta>
            <ta e="T321" id="Seg_6115" s="T318">BoND_1964_ThreeBrothers_flk.070 (001.071)</ta>
            <ta e="T331" id="Seg_6116" s="T321">BoND_1964_ThreeBrothers_flk.071 (001.072)</ta>
            <ta e="T337" id="Seg_6117" s="T331">BoND_1964_ThreeBrothers_flk.072 (001.073)</ta>
            <ta e="T348" id="Seg_6118" s="T337">BoND_1964_ThreeBrothers_flk.073 (001.074)</ta>
            <ta e="T355" id="Seg_6119" s="T348">BoND_1964_ThreeBrothers_flk.074 (001.075)</ta>
            <ta e="T360" id="Seg_6120" s="T355">BoND_1964_ThreeBrothers_flk.075 (001.076)</ta>
            <ta e="T361" id="Seg_6121" s="T360">BoND_1964_ThreeBrothers_flk.076 (001.077)</ta>
            <ta e="T366" id="Seg_6122" s="T361">BoND_1964_ThreeBrothers_flk.077 (001.078)</ta>
            <ta e="T367" id="Seg_6123" s="T366">BoND_1964_ThreeBrothers_flk.078 (001.079)</ta>
            <ta e="T371" id="Seg_6124" s="T367">BoND_1964_ThreeBrothers_flk.079 (001.080)</ta>
            <ta e="T375" id="Seg_6125" s="T371">BoND_1964_ThreeBrothers_flk.080 (001.081)</ta>
            <ta e="T381" id="Seg_6126" s="T375">BoND_1964_ThreeBrothers_flk.081 (001.082)</ta>
            <ta e="T384" id="Seg_6127" s="T381">BoND_1964_ThreeBrothers_flk.082 (001.083)</ta>
            <ta e="T386" id="Seg_6128" s="T384">BoND_1964_ThreeBrothers_flk.083 (001.084)</ta>
            <ta e="T389" id="Seg_6129" s="T386">BoND_1964_ThreeBrothers_flk.084 (001.085)</ta>
            <ta e="T396" id="Seg_6130" s="T389">BoND_1964_ThreeBrothers_flk.085 (001.086)</ta>
            <ta e="T397" id="Seg_6131" s="T396">BoND_1964_ThreeBrothers_flk.086 (001.087)</ta>
            <ta e="T406" id="Seg_6132" s="T397">BoND_1964_ThreeBrothers_flk.087 (001.088)</ta>
            <ta e="T411" id="Seg_6133" s="T406">BoND_1964_ThreeBrothers_flk.088 (001.089)</ta>
            <ta e="T413" id="Seg_6134" s="T411">BoND_1964_ThreeBrothers_flk.089 (001.090)</ta>
            <ta e="T415" id="Seg_6135" s="T413">BoND_1964_ThreeBrothers_flk.090 (001.091)</ta>
            <ta e="T419" id="Seg_6136" s="T415">BoND_1964_ThreeBrothers_flk.091 (001.092)</ta>
            <ta e="T421" id="Seg_6137" s="T419">BoND_1964_ThreeBrothers_flk.092 (001.093)</ta>
            <ta e="T434" id="Seg_6138" s="T421">BoND_1964_ThreeBrothers_flk.093 (001.094)</ta>
            <ta e="T438" id="Seg_6139" s="T434">BoND_1964_ThreeBrothers_flk.094 (001.095)</ta>
            <ta e="T445" id="Seg_6140" s="T438">BoND_1964_ThreeBrothers_flk.095 (001.096)</ta>
            <ta e="T450" id="Seg_6141" s="T445">BoND_1964_ThreeBrothers_flk.096 (001.097)</ta>
            <ta e="T452" id="Seg_6142" s="T450">BoND_1964_ThreeBrothers_flk.097 (001.098)</ta>
            <ta e="T454" id="Seg_6143" s="T452">BoND_1964_ThreeBrothers_flk.098 (001.099)</ta>
            <ta e="T458" id="Seg_6144" s="T454">BoND_1964_ThreeBrothers_flk.099 (001.100)</ta>
            <ta e="T460" id="Seg_6145" s="T458">BoND_1964_ThreeBrothers_flk.100 (001.101)</ta>
            <ta e="T472" id="Seg_6146" s="T460">BoND_1964_ThreeBrothers_flk.101 (001.102)</ta>
            <ta e="T484" id="Seg_6147" s="T472">BoND_1964_ThreeBrothers_flk.102 (001.103)</ta>
            <ta e="T488" id="Seg_6148" s="T484">BoND_1964_ThreeBrothers_flk.103 (001.104)</ta>
            <ta e="T494" id="Seg_6149" s="T488">BoND_1964_ThreeBrothers_flk.104 (001.105)</ta>
            <ta e="T499" id="Seg_6150" s="T494">BoND_1964_ThreeBrothers_flk.105 (001.106)</ta>
            <ta e="T500" id="Seg_6151" s="T499">BoND_1964_ThreeBrothers_flk.106 (001.106)</ta>
            <ta e="T503" id="Seg_6152" s="T500">BoND_1964_ThreeBrothers_flk.107 (001.107)</ta>
            <ta e="T506" id="Seg_6153" s="T503">BoND_1964_ThreeBrothers_flk.108 (001.108)</ta>
            <ta e="T512" id="Seg_6154" s="T506">BoND_1964_ThreeBrothers_flk.109 (001.109)</ta>
            <ta e="T517" id="Seg_6155" s="T512">BoND_1964_ThreeBrothers_flk.110 (001.110)</ta>
            <ta e="T526" id="Seg_6156" s="T517">BoND_1964_ThreeBrothers_flk.111 (001.110)</ta>
            <ta e="T532" id="Seg_6157" s="T526">BoND_1964_ThreeBrothers_flk.112 (001.111)</ta>
            <ta e="T535" id="Seg_6158" s="T532">BoND_1964_ThreeBrothers_flk.113 (001.112)</ta>
            <ta e="T539" id="Seg_6159" s="T535">BoND_1964_ThreeBrothers_flk.114 (001.113)</ta>
            <ta e="T545" id="Seg_6160" s="T539">BoND_1964_ThreeBrothers_flk.115 (001.114)</ta>
            <ta e="T550" id="Seg_6161" s="T545">BoND_1964_ThreeBrothers_flk.116 (001.115)</ta>
            <ta e="T557" id="Seg_6162" s="T550">BoND_1964_ThreeBrothers_flk.117 (001.116)</ta>
            <ta e="T561" id="Seg_6163" s="T557">BoND_1964_ThreeBrothers_flk.118 (001.117)</ta>
            <ta e="T564" id="Seg_6164" s="T561">BoND_1964_ThreeBrothers_flk.119 (001.118)</ta>
            <ta e="T568" id="Seg_6165" s="T564">BoND_1964_ThreeBrothers_flk.120 (001.119)</ta>
            <ta e="T573" id="Seg_6166" s="T568">BoND_1964_ThreeBrothers_flk.121 (001.120)</ta>
            <ta e="T578" id="Seg_6167" s="T573">BoND_1964_ThreeBrothers_flk.122 (001.121)</ta>
            <ta e="T579" id="Seg_6168" s="T578">BoND_1964_ThreeBrothers_flk.123 (001.122)</ta>
            <ta e="T583" id="Seg_6169" s="T579">BoND_1964_ThreeBrothers_flk.124 (001.123)</ta>
            <ta e="T589" id="Seg_6170" s="T583">BoND_1964_ThreeBrothers_flk.125 (001.123)</ta>
            <ta e="T592" id="Seg_6171" s="T589">BoND_1964_ThreeBrothers_flk.126 (001.124)</ta>
            <ta e="T598" id="Seg_6172" s="T592">BoND_1964_ThreeBrothers_flk.127 (001.126)</ta>
            <ta e="T600" id="Seg_6173" s="T598">BoND_1964_ThreeBrothers_flk.128 (001.127)</ta>
            <ta e="T611" id="Seg_6174" s="T600">BoND_1964_ThreeBrothers_flk.129 (001.127)</ta>
            <ta e="T615" id="Seg_6175" s="T611">BoND_1964_ThreeBrothers_flk.130 (001.128)</ta>
            <ta e="T621" id="Seg_6176" s="T615">BoND_1964_ThreeBrothers_flk.131 (001.129)</ta>
            <ta e="T625" id="Seg_6177" s="T621">BoND_1964_ThreeBrothers_flk.132 (001.130)</ta>
            <ta e="T628" id="Seg_6178" s="T625">BoND_1964_ThreeBrothers_flk.133 (001.131)</ta>
            <ta e="T631" id="Seg_6179" s="T628">BoND_1964_ThreeBrothers_flk.134 (001.132)</ta>
            <ta e="T642" id="Seg_6180" s="T631">BoND_1964_ThreeBrothers_flk.135 (001.133)</ta>
            <ta e="T644" id="Seg_6181" s="T642">BoND_1964_ThreeBrothers_flk.136 (001.135)</ta>
            <ta e="T658" id="Seg_6182" s="T644">BoND_1964_ThreeBrothers_flk.137 (001.136)</ta>
            <ta e="T668" id="Seg_6183" s="T658">BoND_1964_ThreeBrothers_flk.138 (001.137)</ta>
            <ta e="T672" id="Seg_6184" s="T668">BoND_1964_ThreeBrothers_flk.139 (001.138)</ta>
            <ta e="T677" id="Seg_6185" s="T672">BoND_1964_ThreeBrothers_flk.140 (001.139)</ta>
            <ta e="T682" id="Seg_6186" s="T677">BoND_1964_ThreeBrothers_flk.141 (001.140)</ta>
            <ta e="T690" id="Seg_6187" s="T682">BoND_1964_ThreeBrothers_flk.142 (001.141)</ta>
            <ta e="T703" id="Seg_6188" s="T690">BoND_1964_ThreeBrothers_flk.143 (001.142)</ta>
            <ta e="T714" id="Seg_6189" s="T703">BoND_1964_ThreeBrothers_flk.144 (001.143)</ta>
            <ta e="T717" id="Seg_6190" s="T714">BoND_1964_ThreeBrothers_flk.145 (001.145)</ta>
            <ta e="T722" id="Seg_6191" s="T717">BoND_1964_ThreeBrothers_flk.146 (001.146)</ta>
            <ta e="T728" id="Seg_6192" s="T722">BoND_1964_ThreeBrothers_flk.147 (001.147)</ta>
            <ta e="T735" id="Seg_6193" s="T728">BoND_1964_ThreeBrothers_flk.148 (001.148)</ta>
            <ta e="T738" id="Seg_6194" s="T735">BoND_1964_ThreeBrothers_flk.149 (001.149)</ta>
            <ta e="T741" id="Seg_6195" s="T738">BoND_1964_ThreeBrothers_flk.150 (001.150)</ta>
            <ta e="T749" id="Seg_6196" s="T741">BoND_1964_ThreeBrothers_flk.151 (001.151)</ta>
            <ta e="T752" id="Seg_6197" s="T749">BoND_1964_ThreeBrothers_flk.152 (001.152)</ta>
            <ta e="T754" id="Seg_6198" s="T752">BoND_1964_ThreeBrothers_flk.153 (001.153)</ta>
            <ta e="T756" id="Seg_6199" s="T754">BoND_1964_ThreeBrothers_flk.154 (001.154)</ta>
            <ta e="T760" id="Seg_6200" s="T756">BoND_1964_ThreeBrothers_flk.155 (001.155)</ta>
            <ta e="T762" id="Seg_6201" s="T760">BoND_1964_ThreeBrothers_flk.156 (001.156)</ta>
            <ta e="T765" id="Seg_6202" s="T762">BoND_1964_ThreeBrothers_flk.157 (001.157)</ta>
            <ta e="T766" id="Seg_6203" s="T765">BoND_1964_ThreeBrothers_flk.158 (001.158)</ta>
            <ta e="T769" id="Seg_6204" s="T766">BoND_1964_ThreeBrothers_flk.159 (001.159)</ta>
            <ta e="T773" id="Seg_6205" s="T769">BoND_1964_ThreeBrothers_flk.160 (001.160)</ta>
            <ta e="T778" id="Seg_6206" s="T773">BoND_1964_ThreeBrothers_flk.161 (001.161)</ta>
            <ta e="T780" id="Seg_6207" s="T778">BoND_1964_ThreeBrothers_flk.162 (001.162)</ta>
            <ta e="T796" id="Seg_6208" s="T780">BoND_1964_ThreeBrothers_flk.163 (001.163)</ta>
            <ta e="T799" id="Seg_6209" s="T796">BoND_1964_ThreeBrothers_flk.164 (001.164)</ta>
            <ta e="T801" id="Seg_6210" s="T799">BoND_1964_ThreeBrothers_flk.165 (001.165)</ta>
            <ta e="T803" id="Seg_6211" s="T801">BoND_1964_ThreeBrothers_flk.166 (001.166)</ta>
            <ta e="T807" id="Seg_6212" s="T803">BoND_1964_ThreeBrothers_flk.167 (001.167)</ta>
            <ta e="T812" id="Seg_6213" s="T807">BoND_1964_ThreeBrothers_flk.168 (001.168)</ta>
            <ta e="T814" id="Seg_6214" s="T812">BoND_1964_ThreeBrothers_flk.169 (001.169)</ta>
            <ta e="T818" id="Seg_6215" s="T814">BoND_1964_ThreeBrothers_flk.170 (001.170)</ta>
            <ta e="T820" id="Seg_6216" s="T818">BoND_1964_ThreeBrothers_flk.171 (001.171)</ta>
            <ta e="T822" id="Seg_6217" s="T820">BoND_1964_ThreeBrothers_flk.172 (001.172)</ta>
            <ta e="T824" id="Seg_6218" s="T822">BoND_1964_ThreeBrothers_flk.173 (001.173)</ta>
            <ta e="T827" id="Seg_6219" s="T824">BoND_1964_ThreeBrothers_flk.174 (001.174)</ta>
            <ta e="T831" id="Seg_6220" s="T827">BoND_1964_ThreeBrothers_flk.175 (001.175)</ta>
            <ta e="T837" id="Seg_6221" s="T831">BoND_1964_ThreeBrothers_flk.176 (001.176)</ta>
            <ta e="T841" id="Seg_6222" s="T837">BoND_1964_ThreeBrothers_flk.177 (001.177)</ta>
            <ta e="T842" id="Seg_6223" s="T841">BoND_1964_ThreeBrothers_flk.178 (001.178)</ta>
            <ta e="T849" id="Seg_6224" s="T842">BoND_1964_ThreeBrothers_flk.179 (001.179)</ta>
            <ta e="T852" id="Seg_6225" s="T849">BoND_1964_ThreeBrothers_flk.180 (001.180)</ta>
            <ta e="T854" id="Seg_6226" s="T852">BoND_1964_ThreeBrothers_flk.181 (001.181)</ta>
            <ta e="T856" id="Seg_6227" s="T854">BoND_1964_ThreeBrothers_flk.182 (001.181)</ta>
            <ta e="T860" id="Seg_6228" s="T856">BoND_1964_ThreeBrothers_flk.183 (001.182)</ta>
            <ta e="T862" id="Seg_6229" s="T860">BoND_1964_ThreeBrothers_flk.184 (001.183)</ta>
            <ta e="T864" id="Seg_6230" s="T862">BoND_1964_ThreeBrothers_flk.185 (001.184)</ta>
            <ta e="T867" id="Seg_6231" s="T864">BoND_1964_ThreeBrothers_flk.186 (001.185)</ta>
            <ta e="T869" id="Seg_6232" s="T867">BoND_1964_ThreeBrothers_flk.187 (001.186)</ta>
            <ta e="T873" id="Seg_6233" s="T869">BoND_1964_ThreeBrothers_flk.188 (001.187)</ta>
            <ta e="T880" id="Seg_6234" s="T873">BoND_1964_ThreeBrothers_flk.189 (001.188)</ta>
            <ta e="T883" id="Seg_6235" s="T880">BoND_1964_ThreeBrothers_flk.190 (001.189)</ta>
            <ta e="T887" id="Seg_6236" s="T883">BoND_1964_ThreeBrothers_flk.191 (001.190)</ta>
            <ta e="T891" id="Seg_6237" s="T887">BoND_1964_ThreeBrothers_flk.192 (001.191)</ta>
            <ta e="T897" id="Seg_6238" s="T891">BoND_1964_ThreeBrothers_flk.193 (001.191)</ta>
            <ta e="T900" id="Seg_6239" s="T897">BoND_1964_ThreeBrothers_flk.194 (001.192)</ta>
            <ta e="T904" id="Seg_6240" s="T900">BoND_1964_ThreeBrothers_flk.195 (001.193)</ta>
            <ta e="T906" id="Seg_6241" s="T904">BoND_1964_ThreeBrothers_flk.196 (001.195)</ta>
            <ta e="T909" id="Seg_6242" s="T906">BoND_1964_ThreeBrothers_flk.197 (001.196)</ta>
            <ta e="T912" id="Seg_6243" s="T909">BoND_1964_ThreeBrothers_flk.198 (001.197)</ta>
            <ta e="T914" id="Seg_6244" s="T912">BoND_1964_ThreeBrothers_flk.199 (001.199)</ta>
            <ta e="T920" id="Seg_6245" s="T914">BoND_1964_ThreeBrothers_flk.200 (001.200)</ta>
            <ta e="T923" id="Seg_6246" s="T920">BoND_1964_ThreeBrothers_flk.201 (001.201)</ta>
            <ta e="T926" id="Seg_6247" s="T923">BoND_1964_ThreeBrothers_flk.202 (001.202)</ta>
            <ta e="T932" id="Seg_6248" s="T926">BoND_1964_ThreeBrothers_flk.203 (001.203)</ta>
            <ta e="T937" id="Seg_6249" s="T932">BoND_1964_ThreeBrothers_flk.204 (001.204)</ta>
            <ta e="T939" id="Seg_6250" s="T937">BoND_1964_ThreeBrothers_flk.205 (001.205)</ta>
            <ta e="T946" id="Seg_6251" s="T939">BoND_1964_ThreeBrothers_flk.206 (001.206)</ta>
            <ta e="T950" id="Seg_6252" s="T946">BoND_1964_ThreeBrothers_flk.207 (001.207)</ta>
            <ta e="T955" id="Seg_6253" s="T950">BoND_1964_ThreeBrothers_flk.208 (001.208)</ta>
            <ta e="T959" id="Seg_6254" s="T955">BoND_1964_ThreeBrothers_flk.209 (001.208)</ta>
            <ta e="T963" id="Seg_6255" s="T959">BoND_1964_ThreeBrothers_flk.210 (001.209)</ta>
            <ta e="T970" id="Seg_6256" s="T963">BoND_1964_ThreeBrothers_flk.211 (001.210)</ta>
            <ta e="T981" id="Seg_6257" s="T970">BoND_1964_ThreeBrothers_flk.212 (001.211)</ta>
            <ta e="T983" id="Seg_6258" s="T981">BoND_1964_ThreeBrothers_flk.213 (001.212)</ta>
            <ta e="T985" id="Seg_6259" s="T983">BoND_1964_ThreeBrothers_flk.214 (001.213)</ta>
            <ta e="T990" id="Seg_6260" s="T985">BoND_1964_ThreeBrothers_flk.215 (001.214)</ta>
            <ta e="T997" id="Seg_6261" s="T990">BoND_1964_ThreeBrothers_flk.216 (001.215)</ta>
            <ta e="T1001" id="Seg_6262" s="T997">BoND_1964_ThreeBrothers_flk.217 (001.216)</ta>
            <ta e="T1008" id="Seg_6263" s="T1001">BoND_1964_ThreeBrothers_flk.218 (001.217)</ta>
            <ta e="T1013" id="Seg_6264" s="T1008">BoND_1964_ThreeBrothers_flk.219 (001.218)</ta>
            <ta e="T1018" id="Seg_6265" s="T1013">BoND_1964_ThreeBrothers_flk.220 (001.219)</ta>
            <ta e="T1022" id="Seg_6266" s="T1018">BoND_1964_ThreeBrothers_flk.221 (001.220)</ta>
            <ta e="T1028" id="Seg_6267" s="T1022">BoND_1964_ThreeBrothers_flk.222 (001.221)</ta>
            <ta e="T1029" id="Seg_6268" s="T1028">BoND_1964_ThreeBrothers_flk.223 (001.222)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_6269" s="T0">Үс ини-биилэр олорбуттар.</ta>
            <ta e="T14" id="Seg_6270" s="T3">Биирдэрэ кыра куччуккаан киһи, ортолоро каампат, тобугунан агай һыылар Куҥадьай диэн.</ta>
            <ta e="T22" id="Seg_6271" s="T14">Убайдара аата һуок, булчут, дьактардаак, биир һаһыл оголоок.</ta>
            <ta e="T26" id="Seg_6272" s="T22">Табалара һуоч-һоготок, ат көрдүк.</ta>
            <ta e="T32" id="Seg_6273" s="T26">Үйэгэ көлүммэттэр, көстөктөрүнэ эрэ дьиэлэрин тартараллар.</ta>
            <ta e="T35" id="Seg_6274" s="T32">Убайдара ааттаак булчут.</ta>
            <ta e="T39" id="Seg_6275" s="T35">Кыылы бултуур, балыгы буоллун.</ta>
            <ta e="T52" id="Seg_6276" s="T39">Дьыл һаастыйарын һагана убайдара ыалдьар, бэргээбит, үһүс күнүгэр буопса мөлтөөбүт, кэриэс эппит дьактарыгар: </ta>
            <ta e="T55" id="Seg_6277" s="T52">— Дьэ мин өллүм.</ta>
            <ta e="T57" id="Seg_6278" s="T55">Кимиҥ карайыагай?!</ta>
            <ta e="T62" id="Seg_6279" s="T57">Бултаабыт булпун һайыны быһа аһыаккыт.</ta>
            <ta e="T70" id="Seg_6280" s="T62">Аскын бараатаккына, эһиил тириитин, тыһын һарыылаан миннээн иһээр.</ta>
            <ta e="T75" id="Seg_6281" s="T70">Ити дьонуҥ туһата һуок дьон.</ta>
            <ta e="T77" id="Seg_6282" s="T75">Өлбүккүнэн өлөгүн.</ta>
            <ta e="T84" id="Seg_6283" s="T77">Огом, багар, киһи буолуо, өлбөтөгүнэ, кини иитиэ.</ta>
            <ta e="T85" id="Seg_6284" s="T84">Элэтэ.</ta>
            <ta e="T87" id="Seg_6285" s="T85">Өлөн каалар.</ta>
            <ta e="T91" id="Seg_6286" s="T87">Ытана-ытана дьактар ууран кээһэр.</ta>
            <ta e="T94" id="Seg_6287" s="T91">Бу һайына буолар.</ta>
            <ta e="T96" id="Seg_6288" s="T94">Астарын катарыналлар.</ta>
            <ta e="T102" id="Seg_6289" s="T96">һайыннарын быһаллар, күһүннэригэр тыстарын, тириилэрин һииллэр.</ta>
            <ta e="T104" id="Seg_6290" s="T102">Барыта баранар.</ta>
            <ta e="T106" id="Seg_6291" s="T104">Кыһын буолар.</ta>
            <ta e="T109" id="Seg_6292" s="T106">һиэк һирдэрэ һуок.</ta>
            <ta e="T116" id="Seg_6293" s="T109">Тыала һуок күн арай һыаргалаак тыаһын истэр.</ta>
            <ta e="T124" id="Seg_6294" s="T116">Дьактар такса көтөр, һаҥа эрэ иһиллэр һиригэр токтообут.</ta>
            <ta e="T133" id="Seg_6295" s="T124">Арай дьактар агата — Агыс конномуой иччитэ, күсээйинэ, киниэс эбит.</ta>
            <ta e="T136" id="Seg_6296" s="T133">— Кайыы, кайдак олорогут?</ta>
            <ta e="T139" id="Seg_6297" s="T136">— Эрим өлбүтэ былырыын.</ta>
            <ta e="T141" id="Seg_6298" s="T139">— Кайыы, слаабуок!</ta>
            <ta e="T143" id="Seg_6299" s="T141">Олус үөрдүм!</ta>
            <ta e="T147" id="Seg_6300" s="T143">Таҥын, барыак мин дьиэбэр!</ta>
            <ta e="T150" id="Seg_6301" s="T147">— Кайа огобун, дьоммун?</ta>
            <ta e="T153" id="Seg_6302" s="T150">— Октон өллүннэр, барыак!</ta>
            <ta e="T160" id="Seg_6303" s="T153">Дьактар таҥнан тийэн мэҥэстэр да, көтүтэн каалаллар.</ta>
            <ta e="T164" id="Seg_6304" s="T160">Куҥадьайдаак кам аччык һыттактара.</ta>
            <ta e="T168" id="Seg_6305" s="T164">Караҥа ааһан, һырдыырга барар.</ta>
            <ta e="T171" id="Seg_6306" s="T168">Куҥадьай балтытыгар этэр: </ta>
            <ta e="T173" id="Seg_6307" s="T171">— Табагын көлүй.</ta>
            <ta e="T175" id="Seg_6308" s="T173">Һыппытынан өлүөкпүт.</ta>
            <ta e="T178" id="Seg_6309" s="T175">Агыс коҥномуойу батыак!</ta>
            <ta e="T185" id="Seg_6310" s="T178">Табаларын көлүйэн, дьиэлэрин көтүрэн, мэҥэстэн эттэтэн истэктэрэ.</ta>
            <ta e="T188" id="Seg_6311" s="T185">Хаһ уонна көспүттэр.</ta>
            <ta e="T192" id="Seg_6312" s="T188">Биирдэ күтүрүөк һискэ ыттыбыттар.</ta>
            <ta e="T206" id="Seg_6313" s="T192">Көрүлээбиттэрэ: күтүрдээк тээн* тээн ортотугар үөр бөгө, тээн уҥуор һэттэ уон дьиэ киһи көстөр.</ta>
            <ta e="T212" id="Seg_6314" s="T206">Барбыттар үөр иһинэн үөр анараа өттүтүгэр.</ta>
            <ta e="T215" id="Seg_6315" s="T212">— Тур! — диэбит Куҥадьай.</ta>
            <ta e="T223" id="Seg_6316" s="T215">— Ити дьиэлэртэн биир ордук-тарыгар токтоор, ниптэлэрин бэтэрээ өттүкээнигэр.</ta>
            <ta e="T228" id="Seg_6317" s="T223">Көрбүтэ, биир дьиэ барыларыттан улакан.</ta>
            <ta e="T235" id="Seg_6318" s="T228">Аан туһугар тийбиттэр да табаларын ыытан кээһэллэр.</ta>
            <ta e="T239" id="Seg_6319" s="T235">Дьиэ туттан кээспиттэр, оттубуттар.</ta>
            <ta e="T242" id="Seg_6320" s="T239">Балтытын: — Бар һаҥаскар.</ta>
            <ta e="T245" id="Seg_6321" s="T242">Оготун эмнэрэ кэллин.</ta>
            <ta e="T258" id="Seg_6322" s="T245">Агыс Коҥномуойга эт: өллүбүт, биир эмэ һүрэк аҥааркаанын кытта быар тулаайагын бэристин, — диир.</ta>
            <ta e="T262" id="Seg_6323" s="T258">Уол һүүрэн киирэн этэр.</ta>
            <ta e="T266" id="Seg_6324" s="T262">— Көр эрэ, эмтэрэр һокучуок!</ta>
            <ta e="T270" id="Seg_6325" s="T266">Октон өллүн! — диир дьактар.</ta>
            <ta e="T283" id="Seg_6326" s="T270">— Кайа эһээ, илдьиттээтэ убайым: өллүбүт биир эмэ һүрэк аҥааркаанын кытта быар тулаайагын бэристин.</ta>
            <ta e="T286" id="Seg_6327" s="T283">— Каак, октон өлүҥ.</ta>
            <ta e="T291" id="Seg_6328" s="T286">Ыыппаппын! — диир Агыс коҥномуой иччитэ.</ta>
            <ta e="T292" id="Seg_6329" s="T291">Утуйаактыыллар.</ta>
            <ta e="T294" id="Seg_6330" s="T292">Тугу һиэктэрэй?!</ta>
            <ta e="T298" id="Seg_6331" s="T294">Арай табалыыр һаҥа иһиллэр.</ta>
            <ta e="T303" id="Seg_6332" s="T298">Куҥадьай таксан ииктии турбут, тобугар.</ta>
            <ta e="T306" id="Seg_6333" s="T303">Таба бөгө кэлэр.</ta>
            <ta e="T311" id="Seg_6334" s="T306">Актамии буур эмис багайы кэлэр.</ta>
            <ta e="T314" id="Seg_6335" s="T311">Уол кабан ылар.</ta>
            <ta e="T316" id="Seg_6336" s="T314">Мөгөн бөгө.</ta>
            <ta e="T318" id="Seg_6337" s="T316">Баһагын ылларар.</ta>
            <ta e="T321" id="Seg_6338" s="T318">Кабаргатын быһа һотор.</ta>
            <ta e="T331" id="Seg_6339" s="T321">һүлбэккэ да киллэрэллэр, күөстэнэн дьэ аһыыллар күнү мэлдьи киэһэгэ диэри.</ta>
            <ta e="T337" id="Seg_6340" s="T331">Бу дьиэ таһыгар куччугуй дьиэ турар.</ta>
            <ta e="T348" id="Seg_6341" s="T337">— Чээ, һэттэ тимэк иччитигэр бар ити дьиэгэ (старшина буолуо?!) күөстэ көрдөө.</ta>
            <ta e="T355" id="Seg_6342" s="T348">Уол барар, һэттэ тимэк иччитигэр этэр илдьити.</ta>
            <ta e="T360" id="Seg_6343" s="T355">— Агыс коҥномуой иччитэ биэрдэ эни?</ta>
            <ta e="T361" id="Seg_6344" s="T360">— Һуок.</ta>
            <ta e="T366" id="Seg_6345" s="T361">— Ээ, оччого мин эмиэ биэрбэппин.</ta>
            <ta e="T367" id="Seg_6346" s="T366">Аһыыллар.</ta>
            <ta e="T371" id="Seg_6347" s="T367">һарсиэрда үөр иһэр эмиэ.</ta>
            <ta e="T375" id="Seg_6348" s="T371">Тобугунан үнэн таксар Куҥадьай.</ta>
            <ta e="T381" id="Seg_6349" s="T375">Куҥадьай төрөөбөт мааҥкаайы тутан ылар калдаатыттан.</ta>
            <ta e="T384" id="Seg_6350" s="T381">— Кайа-куу, баһагы таһаар!</ta>
            <ta e="T386" id="Seg_6351" s="T384">Өлөрөн кээһэр.</ta>
            <ta e="T389" id="Seg_6352" s="T386">Эмиэ һүлбэккэ һииллэр.</ta>
            <ta e="T396" id="Seg_6353" s="T389">Кайа бу, били тойоттор гиэннэрин өлөрбүт эбит.</ta>
            <ta e="T397" id="Seg_6354" s="T396">Утуйаллар.</ta>
            <ta e="T406" id="Seg_6355" s="T397">һарсиэрда Куҥадьай таксыбыта, Агыс коҥномуой күсээйинэ күрэйин куоһана олорор.</ta>
            <ta e="T411" id="Seg_6356" s="T406">— Кайаа, Агыс коҥномуой иччитэ, дорообо!</ta>
            <ta e="T413" id="Seg_6357" s="T411">— Өссүө дорообо!</ta>
            <ta e="T415" id="Seg_6358" s="T413">Бүгүн өлөртүүбүт.</ta>
            <ta e="T419" id="Seg_6359" s="T415">Элэ табаларбыттан һии һытагыт!</ta>
            <ta e="T421" id="Seg_6360" s="T419">— Бэйэгит буруйгут.</ta>
            <ta e="T434" id="Seg_6361" s="T421">Агыс коҥномуой иччитэ тобуктаан турар киһини күрэй үнүүтүнэн өттүккэ һаайар да һыыһан кээһэр.</ta>
            <ta e="T438" id="Seg_6362" s="T434">Куҥадьайа каар аннынан барбыт.</ta>
            <ta e="T445" id="Seg_6363" s="T438">һэттэ тимэк иччитэ күрэй куоһана олордогуна таксар.</ta>
            <ta e="T450" id="Seg_6364" s="T445">— Кайаа, һэттэ тимэк иччитэ, дорообо!</ta>
            <ta e="T452" id="Seg_6365" s="T450">— Өссүө дорообо!</ta>
            <ta e="T454" id="Seg_6366" s="T452">Бүгүн өлөртүүбүт.</ta>
            <ta e="T458" id="Seg_6367" s="T454">Элэ табаларбытын һии һытагыт!</ta>
            <ta e="T460" id="Seg_6368" s="T458">— Бэйэгит буруйгут.</ta>
            <ta e="T472" id="Seg_6369" s="T460">һэттэ тимэк иччитэ тобуктаан турар киһини күрэй үҥүүтүнэн өттүккэ анньаары һири һаайбыт.</ta>
            <ta e="T484" id="Seg_6370" s="T472">Каччага эрэ Куҥадьай нөҥүө һиринэн такса көтөр да икки атагар тура эккириир.</ta>
            <ta e="T488" id="Seg_6371" s="T484">Көрүлээбитэ: көс баран эрэр.</ta>
            <ta e="T494" id="Seg_6372" s="T488">Һаҥаһа көһөн эрэр, агата кэргэн биэрбит.</ta>
            <ta e="T499" id="Seg_6373" s="T494">Куҥадьай һаҥаһын ньуогутун кабан ылар: </ta>
            <ta e="T500" id="Seg_6374" s="T499">— Ыыппаппын.</ta>
            <ta e="T503" id="Seg_6375" s="T500">Кайа диэки барагын?</ta>
            <ta e="T506" id="Seg_6376" s="T503">Дьактар көтүтэн каалар.</ta>
            <ta e="T512" id="Seg_6377" s="T506">Куҥадьай һитэ баттаан өлгөбүүнүн илдьэ каалар.</ta>
            <ta e="T517" id="Seg_6378" s="T512">Куҥадьай Агыс коҥномуой иччитигэр киирэр: </ta>
            <ta e="T526" id="Seg_6379" s="T517">— Дьэ, Агыс коҥномуой иччитэ, эйэлээк эрдэккэ балыстарбын карайан олор.</ta>
            <ta e="T532" id="Seg_6380" s="T526">Куһаганнык көрүөҥ — биир күнүнэн һуок оҥортуом.</ta>
            <ta e="T535" id="Seg_6381" s="T532">һаҥаспын бата бардым.</ta>
            <ta e="T539" id="Seg_6382" s="T535">һаҥаһын батан һүтэрэн кээһэр.</ta>
            <ta e="T545" id="Seg_6383" s="T539">Каннык дойду уһугар көрөр: һитиэк һиппэт.</ta>
            <ta e="T550" id="Seg_6384" s="T545">Эргитэн эгэлэн дьиэтин диэк күөйэр.</ta>
            <ta e="T557" id="Seg_6385" s="T550">Тутуок да эрээри туппакка һаҥаһын кэнниттэн киирэр.</ta>
            <ta e="T561" id="Seg_6386" s="T557">һанаһа оготун эмийдии олорор.</ta>
            <ta e="T564" id="Seg_6387" s="T561">Балыстара аһыы олорор.</ta>
            <ta e="T568" id="Seg_6388" s="T564">— Дьэ һаҥаас, буруйгун аһардыҥ.</ta>
            <ta e="T573" id="Seg_6389" s="T568">Огогун ылбатагыҥ буоллар өлөрүөк этим!</ta>
            <ta e="T578" id="Seg_6390" s="T573">Агыс коҥномуой иччитэ куттанаары куттанар.</ta>
            <ta e="T579" id="Seg_6391" s="T578">Олороллор.</ta>
            <ta e="T583" id="Seg_6392" s="T579">Каһан эрэ үһүс күнүгэр: </ta>
            <ta e="T589" id="Seg_6393" s="T583">— Дьэ Агыс коҥномуой иччитэ, дьоҥҥун мус.</ta>
            <ta e="T592" id="Seg_6394" s="T589">Мунньактыакпыт! — диир Куҥадьай.</ta>
            <ta e="T598" id="Seg_6395" s="T592">Дьэ мунньаллар һэттэ уон дьиэ киһитин,</ta>
            <ta e="T600" id="Seg_6396" s="T598">Куҥадьай этэр: </ta>
            <ta e="T611" id="Seg_6397" s="T600">— Эһиги икки күсээйин бааргыт — Агыс коҥномуой күсээйинэ уонна һэттэ тимэк иччитэ.</ta>
            <ta e="T615" id="Seg_6398" s="T611">Аны күсээйин буолан бүттүгүт.</ta>
            <ta e="T621" id="Seg_6399" s="T615">Күсээйиҥҥит — ити уол ого — убайым огото.</ta>
            <ta e="T625" id="Seg_6400" s="T621">Комунуҥ барыгыт икки күҥҥэ.</ta>
            <ta e="T628" id="Seg_6401" s="T625">Үһүс күҥҥүтүгэр көһүөккүт.</ta>
            <ta e="T631" id="Seg_6402" s="T628">Бастыҥҥыт кини буолуо.</ta>
            <ta e="T642" id="Seg_6403" s="T631">— Атын табаны көлүнүмэ, агаҥ табатын, кэриэһин, көлүнээр! — диир уолга [уол улааппыт].</ta>
            <ta e="T644" id="Seg_6404" s="T642">— Бастатаҥҥын көһүөккүт.</ta>
            <ta e="T658" id="Seg_6405" s="T644">Мас һага-тынан дьирбии баар буолуо, бу дьирбии муора диэки өттүнэн көһүөҥ, икки ыйы быһа.</ta>
            <ta e="T668" id="Seg_6406" s="T658">Элэтэ буоллагына, дьирбиитэ бүттэгинэ һир буолуо, каарын анныттан буруолаак буолуо.</ta>
            <ta e="T672" id="Seg_6407" s="T668">Ол биһиги төрүт һирбит.</ta>
            <ta e="T677" id="Seg_6408" s="T672">Улакан һуолга киириэҥ, аймактаргар тийиэҥ.</ta>
            <ta e="T682" id="Seg_6409" s="T677">Дьоннор, табагатын түүнү-күнү быһа көрүөккүт.</ta>
            <ta e="T690" id="Seg_6410" s="T682">Агыс коҥномуой күсээйинэ, һэттэ тимэк иччитэ, эмиэ көрсүөккүт.</ta>
            <ta e="T703" id="Seg_6411" s="T690">Мин буоллагына бу үчүгэйдик айанныыргыт туһуттан куһаган дьону — чаҥыттары, кыыллары һуок гына бардым.</ta>
            <ta e="T714" id="Seg_6412" s="T703">Ол һиргэ тийэр күҥҥэр тийиэм, улаканнык һаараатакпына үһүс күнүгэр тийиэм! — диир.</ta>
            <ta e="T717" id="Seg_6413" s="T714">Үһүс күнүгэр көһөллөр.</ta>
            <ta e="T722" id="Seg_6414" s="T717">Куҥадьай араган барар, һуос һатыы.</ta>
            <ta e="T728" id="Seg_6415" s="T722">Баа ого бастыҥ буолан көһөн истилэр.</ta>
            <ta e="T735" id="Seg_6416" s="T728">Кэтэнэн айанныыллар, тойону-котуну барытын кэтэтэр бу уол.</ta>
            <ta e="T738" id="Seg_6417" s="T735">Ыйын ортото буолбут.</ta>
            <ta e="T741" id="Seg_6418" s="T738">Дьирбиитин ирдээн истэгэ.</ta>
            <ta e="T749" id="Seg_6419" s="T741">Арай муора диэки дьирбии арагар, үрдүгэр киһи һылдьар.</ta>
            <ta e="T752" id="Seg_6420" s="T749">— Кайа диэкки киһигиний?</ta>
            <ta e="T754" id="Seg_6421" s="T752">— Туок кэлиэй?</ta>
            <ta e="T756" id="Seg_6422" s="T754">Дьиэни-уоту билбэппин.</ta>
            <ta e="T760" id="Seg_6423" s="T756">Төрөөбүт убайбын һүтэрэ һылдьабын.</ta>
            <ta e="T762" id="Seg_6424" s="T760">— Кайаларай ол?</ta>
            <ta e="T765" id="Seg_6425" s="T762">— Агыс коҥномуой күсээйинэ!</ta>
            <ta e="T766" id="Seg_6426" s="T765">— Һээй!</ta>
            <ta e="T769" id="Seg_6427" s="T766">Ол мин киһим.</ta>
            <ta e="T773" id="Seg_6428" s="T769">Ол дэлэмичээ үүрэн иһэр.</ta>
            <ta e="T778" id="Seg_6429" s="T773">Дьэ оччого миэкэ киһи буол.</ta>
            <ta e="T780" id="Seg_6430" s="T778">һаҥара тарпат.</ta>
            <ta e="T796" id="Seg_6431" s="T780">— Иэһили киһи буолуоҥ һуога, бу үс һалаалаак күрэйим аҥар һалаата каҥыргаһа буолуоҥ, иккитэ букатыыр ээлдэтэ каҥыргастаак.</ta>
            <ta e="T799" id="Seg_6432" s="T796">Киһи һөбүлэнэр, барсар.</ta>
            <ta e="T801" id="Seg_6433" s="T799">Көһөн истилэр.</ta>
            <ta e="T803" id="Seg_6434" s="T801">Ыйдара баранна.</ta>
            <ta e="T807" id="Seg_6435" s="T803">Нөҥүө ый ортото буолла.</ta>
            <ta e="T812" id="Seg_6436" s="T807">Эмиэ киһини көрдүлэр биир дьирбиикээҥҥэ.</ta>
            <ta e="T814" id="Seg_6437" s="T812">Эмиэ дорооболоһор.</ta>
            <ta e="T818" id="Seg_6438" s="T814">— Кайа диэки дьиэлээк-уоттаак киһигиний?</ta>
            <ta e="T820" id="Seg_6439" s="T818">— Дьиэ-уот һуок.</ta>
            <ta e="T822" id="Seg_6440" s="T820">һылдьабын агай.</ta>
            <ta e="T824" id="Seg_6441" s="T822">Убайбын көрдүүбүн.</ta>
            <ta e="T827" id="Seg_6442" s="T824">— Туок этэй убайын?</ta>
            <ta e="T831" id="Seg_6443" s="T827">— Ээ, һэттэ тимэк иччитэ!</ta>
            <ta e="T837" id="Seg_6444" s="T831">— Ээ, ол дэлэмичээ үүрэн иһэр убайыҥ.</ta>
            <ta e="T841" id="Seg_6445" s="T837">Кайа миэкэ киһи буолуоҥ?</ta>
            <ta e="T842" id="Seg_6446" s="T841">һаҥарбат.</ta>
            <ta e="T849" id="Seg_6447" s="T842">— Буолуон-һуога, бу күрэйим һалаатыгар каҥыргас гыныам!</ta>
            <ta e="T852" id="Seg_6448" s="T849">һөбүлэһэр били киһи.</ta>
            <ta e="T854" id="Seg_6449" s="T852">Убайыгар тийэр: </ta>
            <ta e="T856" id="Seg_6450" s="T854">— Того өлөрбөккүт?</ta>
            <ta e="T860" id="Seg_6451" s="T856">Огого бас бэринэгит дуо?</ta>
            <ta e="T862" id="Seg_6452" s="T860">— Һуок, кэбис.</ta>
            <ta e="T864" id="Seg_6453" s="T862">— Бултуокка наада.</ta>
            <ta e="T867" id="Seg_6454" s="T864">Коһута да бэрт.</ta>
            <ta e="T869" id="Seg_6455" s="T867">— Чэ, багалыакпыт!</ta>
            <ta e="T873" id="Seg_6456" s="T869">Киэһэ түһэллэр биир һиргэ.</ta>
            <ta e="T880" id="Seg_6457" s="T873">Аһыыллар, уол дьонун батан таһаарар үөр кэтэтэ.</ta>
            <ta e="T883" id="Seg_6458" s="T880">Ого утуйан каалар.</ta>
            <ta e="T887" id="Seg_6459" s="T883">Киһилэр калыйар һаҥаларын истэр.</ta>
            <ta e="T891" id="Seg_6460" s="T887">Киһи батыйаннан ааны һэгэтэр: </ta>
            <ta e="T897" id="Seg_6461" s="T891">— Кайа, того кэтиигит миигин, үөрү кэтээҥ!</ta>
            <ta e="T900" id="Seg_6462" s="T897">Дьон төттөрү һаһаллар.</ta>
            <ta e="T904" id="Seg_6463" s="T900">— һарсиэрдааҥҥы уутугар багалыакпыт! — дэһэллэр.</ta>
            <ta e="T906" id="Seg_6464" s="T904">Эмиэ бүрүүрдүүллэр.</ta>
            <ta e="T909" id="Seg_6465" s="T906">Эмиэ: — Тугу кэтиигит?</ta>
            <ta e="T912" id="Seg_6466" s="T909">Барыҥ! — батан ыытар.</ta>
            <ta e="T914" id="Seg_6467" s="T912">Һонтон тыыппатактара.</ta>
            <ta e="T920" id="Seg_6468" s="T914">Икки ыйдара быстарыгар дьирбиилэрэ элэтэ буолар.</ta>
            <ta e="T923" id="Seg_6469" s="T920">Көрбүтэ — һир буолбут.</ta>
            <ta e="T926" id="Seg_6470" s="T923">Каар анныттан буруокаакына.</ta>
            <ta e="T932" id="Seg_6471" s="T926">Муора диэккиттэн күтүр багайы орок кэлэр.</ta>
            <ta e="T937" id="Seg_6472" s="T932">һир иһин диэки барар орок.</ta>
            <ta e="T939" id="Seg_6473" s="T937">Баран иһэллэр.</ta>
            <ta e="T946" id="Seg_6474" s="T939">Пирээмэ норуоттан норуот, дьиэттэн дьиэ һиргэ кэлэллэр.</ta>
            <ta e="T950" id="Seg_6475" s="T946">Биир дьиэ таһыгар токтуур.</ta>
            <ta e="T955" id="Seg_6476" s="T950">Биир кырдьагас һогус дьактар таксар: </ta>
            <ta e="T959" id="Seg_6477" s="T955">— Бай, табата одуу таба!</ta>
            <ta e="T963" id="Seg_6478" s="T959">Биһиги төрүт табабыт курдугуй?!</ta>
            <ta e="T970" id="Seg_6479" s="T963">Былыр икки киһи барбыта, ол төрүттэрэ буолуо!</ta>
            <ta e="T981" id="Seg_6480" s="T970">Үс дьиэттэн үс дьактар таксан эмиэ таайбыттар төрүттэрин, кэпсэтэн дьэ билсэллэр.</ta>
            <ta e="T983" id="Seg_6481" s="T981">Урууларын булсаллар.</ta>
            <ta e="T985" id="Seg_6482" s="T983">Куҥадьайы күүтэллэр.</ta>
            <ta e="T990" id="Seg_6483" s="T985">Үһүс күнүн ортото Куҥадьай кэлэр.</ta>
            <ta e="T997" id="Seg_6484" s="T990">Эт аата бүтүнэ һуок, тириитин аҥара каалбыт.</ta>
            <ta e="T1001" id="Seg_6485" s="T997">— Огом, кэлбиккин — дьэ үчүгэй!</ta>
            <ta e="T1008" id="Seg_6486" s="T1001">Куһаган дьону бараан, дойду булдун арычча кыайдым.</ta>
            <ta e="T1013" id="Seg_6487" s="T1008">Тыыным эрэ кэллим, гойобуун оҥороннор.</ta>
            <ta e="T1018" id="Seg_6488" s="T1013">Дьэ олор, туоктан да куттаныма!</ta>
            <ta e="T1022" id="Seg_6489" s="T1018">Куҥадьай дьэ койут тиллэр.</ta>
            <ta e="T1028" id="Seg_6490" s="T1022">Уол күсээйин буолан олорбута бу омукка.</ta>
            <ta e="T1029" id="Seg_6491" s="T1028">Элэтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_6492" s="T0">Üs inibiːler olorbuttar. </ta>
            <ta e="T14" id="Seg_6493" s="T3">Biːrdere kɨra kuččukkaːn kihi, ortoloro kaːmpat, tobugunan agaj hɨːlar Kuŋadʼaj di͡en. </ta>
            <ta e="T22" id="Seg_6494" s="T14">Ubajdara aːta hu͡ok, bulčut, dʼaktardaːk, biːr hahɨl ogoloːk. </ta>
            <ta e="T26" id="Seg_6495" s="T22">Tabalara hu͡oč-hogotok, at kördük. </ta>
            <ta e="T32" id="Seg_6496" s="T26">Üjege kölümmetter, köstöktörüne ere dʼi͡elerin tartarallar. </ta>
            <ta e="T35" id="Seg_6497" s="T32">Ubajdara aːttaːk bulčut. </ta>
            <ta e="T39" id="Seg_6498" s="T35">Kɨːlɨ bultuːr, balɨgɨ bu͡ollun. </ta>
            <ta e="T52" id="Seg_6499" s="T39">Dʼɨl haːstɨjarɨn hagana ubajdara ɨ͡aldʼar, bergeːbit, ühüs künüger bu͡opsa möltöːbüt, keri͡es eppit dʼaktarɨgar: </ta>
            <ta e="T55" id="Seg_6500" s="T52">"Dʼe min öllüm. </ta>
            <ta e="T57" id="Seg_6501" s="T55">Kimiŋ karajɨ͡agaj? </ta>
            <ta e="T62" id="Seg_6502" s="T57">Bultaːbɨt bulpun hajɨnɨ bɨha ahɨ͡akkɨt. </ta>
            <ta e="T70" id="Seg_6503" s="T62">Askɨn baraːtakkɨna, ehiːl tiriːtin, tɨhɨn harɨːlaːn minneːn iheːr. </ta>
            <ta e="T75" id="Seg_6504" s="T70">Iti dʼonuŋ tuhata hu͡ok dʼon. </ta>
            <ta e="T77" id="Seg_6505" s="T75">Ölbükkünen ölögün. </ta>
            <ta e="T84" id="Seg_6506" s="T77">Ogom, bagar, kihi bu͡olu͡o, ölbötögüne, kini iːti͡e. </ta>
            <ta e="T85" id="Seg_6507" s="T84">Elete." </ta>
            <ta e="T87" id="Seg_6508" s="T85">Ölön kaːlar. </ta>
            <ta e="T91" id="Seg_6509" s="T87">ɨtana-ɨtana dʼaktar uːran keːher. </ta>
            <ta e="T94" id="Seg_6510" s="T91">Bu hajɨna bu͡olar. </ta>
            <ta e="T96" id="Seg_6511" s="T94">Astarɨn katarɨnallar. </ta>
            <ta e="T102" id="Seg_6512" s="T96">Hajɨnnarɨn bɨhallar, kühünneriger tɨstarɨn, tiriːlerin hiːller. </ta>
            <ta e="T104" id="Seg_6513" s="T102">Barɨta baranar. </ta>
            <ta e="T106" id="Seg_6514" s="T104">Kɨhɨn bu͡olar. </ta>
            <ta e="T109" id="Seg_6515" s="T106">Hi͡ek hirdere hu͡ok. </ta>
            <ta e="T116" id="Seg_6516" s="T109">Tɨ͡ala hu͡ok kün araj hɨ͡argalaːk tɨ͡ahɨn ister. </ta>
            <ta e="T124" id="Seg_6517" s="T116">Dʼaktar taksa kötör, haŋa ere ihiller hiriger toktoːbut. </ta>
            <ta e="T133" id="Seg_6518" s="T124">Araj dʼaktar agata — Agɨs konnomu͡oj iččite, küseːjine, kini͡es ebit. </ta>
            <ta e="T136" id="Seg_6519" s="T133">"Kajɨː, kajdak olorogut?" </ta>
            <ta e="T139" id="Seg_6520" s="T136">"Erim ölbüte bɨlɨrɨːn." </ta>
            <ta e="T141" id="Seg_6521" s="T139">"Kajɨː, slaːbu͡ok! </ta>
            <ta e="T143" id="Seg_6522" s="T141">Olus ü͡ördüm! </ta>
            <ta e="T147" id="Seg_6523" s="T143">Taŋɨn, barɨ͡ak min dʼi͡eber!" </ta>
            <ta e="T150" id="Seg_6524" s="T147">"Kaja ogobun, dʼommun?" </ta>
            <ta e="T153" id="Seg_6525" s="T150">"Okton öllünner, barɨ͡ak!" </ta>
            <ta e="T160" id="Seg_6526" s="T153">Dʼaktar taŋnan tijen meŋester da, kötüten kaːlallar. </ta>
            <ta e="T164" id="Seg_6527" s="T160">Kuŋadʼajdaːk kam aččɨk hɨttaktara. </ta>
            <ta e="T168" id="Seg_6528" s="T164">Karaŋa aːhan, hɨrdɨːrga barar. </ta>
            <ta e="T171" id="Seg_6529" s="T168">Kuŋadʼaj baltɨtɨgar eter: </ta>
            <ta e="T173" id="Seg_6530" s="T171">"Tabagɨn kölüj. </ta>
            <ta e="T175" id="Seg_6531" s="T173">Hɨppɨtɨnan ölü͡ökpüt. </ta>
            <ta e="T178" id="Seg_6532" s="T175">Agɨs koŋnomu͡oju batɨ͡ak!" </ta>
            <ta e="T185" id="Seg_6533" s="T178">Tabalarɨn kölüjen, dʼi͡elerin kötüren, meŋesten etteten istektere. </ta>
            <ta e="T188" id="Seg_6534" s="T185">Kah u͡onna köspütter. </ta>
            <ta e="T192" id="Seg_6535" s="T188">Biːrde kütürü͡ök hiske ɨttɨbɨttar. </ta>
            <ta e="T206" id="Seg_6536" s="T192">Körüleːbittere, kütürdeːk teːn teːn ortotugar ü͡ör bögö, teːn uŋu͡or hette u͡on dʼi͡e kihi köstör. </ta>
            <ta e="T212" id="Seg_6537" s="T206">Barbɨttar ü͡ör ihinen ü͡ör anaraː öttütüger. </ta>
            <ta e="T215" id="Seg_6538" s="T212">"Tur", di͡ebit Kuŋadʼaj. </ta>
            <ta e="T223" id="Seg_6539" s="T215">"Iti dʼi͡elerten biːr orduktarɨgar toktoːr, niptelerin betereː öttükeːniger." </ta>
            <ta e="T228" id="Seg_6540" s="T223">Körbüte, biːr dʼi͡e barɨlarɨttan ulakan. </ta>
            <ta e="T235" id="Seg_6541" s="T228">Aːn tuhugar tijbitter da tabalarɨn ɨːtan keːheller. </ta>
            <ta e="T239" id="Seg_6542" s="T235">Dʼi͡e tuttan keːspitter, ottubuttar. </ta>
            <ta e="T242" id="Seg_6543" s="T239">Baltɨtɨn "bar haŋaskar. </ta>
            <ta e="T245" id="Seg_6544" s="T242">Ogotun emnere kellin. </ta>
            <ta e="T258" id="Seg_6545" s="T245">Agɨs Koŋnomu͡ojga et 'öllübüt, biːr eme hürek aŋaːrkaːnɨn kɨtta bɨ͡ar tulaːjagɨn beristin'", diːr. </ta>
            <ta e="T262" id="Seg_6546" s="T258">U͡ol hüːren kiːren eter. </ta>
            <ta e="T266" id="Seg_6547" s="T262">"Kör ere, emterer hokuču͡ok! </ta>
            <ta e="T270" id="Seg_6548" s="T266">Okton öllün", diːr dʼaktar. </ta>
            <ta e="T283" id="Seg_6549" s="T270">"Kaja eheː, ildʼitteːte ubajɨm 'öllübüt biːr eme hürek aŋaːrkaːnɨn kɨtta bɨ͡ar tulaːjagɨn beristin'." </ta>
            <ta e="T286" id="Seg_6550" s="T283">"Kaːk, okton ölüŋ. </ta>
            <ta e="T291" id="Seg_6551" s="T286">ɨːppappɨn", diːr Agɨs koŋnomu͡oj iččite. </ta>
            <ta e="T292" id="Seg_6552" s="T291">Utujaːktɨːllar. </ta>
            <ta e="T294" id="Seg_6553" s="T292">Tugu hi͡ekterej? </ta>
            <ta e="T298" id="Seg_6554" s="T294">Araj tabalɨːr haŋa ihiller. </ta>
            <ta e="T303" id="Seg_6555" s="T298">Kuŋadʼaj taksan iːktiː turbut, tobugar. </ta>
            <ta e="T306" id="Seg_6556" s="T303">Taba bögö keler. </ta>
            <ta e="T311" id="Seg_6557" s="T306">Aktamiː buːr emis bagajɨ keler. </ta>
            <ta e="T314" id="Seg_6558" s="T311">U͡ol kaban ɨlar. </ta>
            <ta e="T316" id="Seg_6559" s="T314">Mögön bögö. </ta>
            <ta e="T318" id="Seg_6560" s="T316">Bahagɨn ɨllarar. </ta>
            <ta e="T321" id="Seg_6561" s="T318">Kabargatɨn bɨha hotor. </ta>
            <ta e="T331" id="Seg_6562" s="T321">Hülbekke da killereller, kü͡östenen dʼe ahɨːllar künü meldʼi ki͡ehege di͡eri. </ta>
            <ta e="T337" id="Seg_6563" s="T331">Bu dʼi͡e tahɨgar kuččuguj dʼi͡e turar. </ta>
            <ta e="T348" id="Seg_6564" s="T337">"Čeː, hette timek iččitiger bar iti dʼi͡ege (staršina bu͡olu͡o) kü͡öste kördöː." </ta>
            <ta e="T355" id="Seg_6565" s="T348">U͡ol barar, hette timek iččitiger eter ildʼiti. </ta>
            <ta e="T360" id="Seg_6566" s="T355">"Agɨs koŋnomu͡oj iččite bi͡erde eni?" </ta>
            <ta e="T361" id="Seg_6567" s="T360">"Hu͡ok." </ta>
            <ta e="T366" id="Seg_6568" s="T361">"Eː, oččogo min emi͡e bi͡erbeppin." </ta>
            <ta e="T367" id="Seg_6569" s="T366">Ahɨːllar. </ta>
            <ta e="T371" id="Seg_6570" s="T367">Harsi͡erda ü͡ör iher emi͡e. </ta>
            <ta e="T375" id="Seg_6571" s="T371">Tobugunan ünen taksar Kuŋadʼaj. </ta>
            <ta e="T381" id="Seg_6572" s="T375">Kuŋadʼaj töröːböt maːŋkaːjɨ tutan ɨlar kaldaːtɨttan. </ta>
            <ta e="T384" id="Seg_6573" s="T381">"Kaja-kuː, bahagɨ tahaːr!" </ta>
            <ta e="T386" id="Seg_6574" s="T384">Ölörön keːher. </ta>
            <ta e="T389" id="Seg_6575" s="T386">Emi͡e hülbekke hiːller. </ta>
            <ta e="T396" id="Seg_6576" s="T389">Kaja bu, bili tojottor gi͡ennerin ölörbüt ebit. </ta>
            <ta e="T397" id="Seg_6577" s="T396">Utujallar. </ta>
            <ta e="T406" id="Seg_6578" s="T397">Harsi͡erda Kuŋadʼaj taksɨbɨta, Agɨs koŋnomu͡oj küseːjine kürejin ku͡ohana oloror. </ta>
            <ta e="T411" id="Seg_6579" s="T406">"Kajaː, Agɨs koŋnomu͡oj iččite, doroːbo!" </ta>
            <ta e="T413" id="Seg_6580" s="T411">"Össü͡ö doroːbo! </ta>
            <ta e="T415" id="Seg_6581" s="T413">Bügün ölörtüːbüt. </ta>
            <ta e="T419" id="Seg_6582" s="T415">Ele tabalarbɨttan hiː hɨtagɨt!" </ta>
            <ta e="T421" id="Seg_6583" s="T419">"Bejegit burujgut." </ta>
            <ta e="T434" id="Seg_6584" s="T421">Agɨs koŋnomu͡oj iččite tobuktaːn turar kihini kürej ünüːtünen öttükke haːjar da hɨːhan keːher. </ta>
            <ta e="T438" id="Seg_6585" s="T434">Kuŋadʼaja kaːr annɨnan barbɨt. </ta>
            <ta e="T445" id="Seg_6586" s="T438">Hette timek iččite kürej ku͡ohana olordoguna taksar. </ta>
            <ta e="T450" id="Seg_6587" s="T445">"Kajaː, hette timek iččite, doroːbo!" </ta>
            <ta e="T452" id="Seg_6588" s="T450">"Össü͡ö doroːbo! </ta>
            <ta e="T454" id="Seg_6589" s="T452">Bügün ölörtüːbüt. </ta>
            <ta e="T458" id="Seg_6590" s="T454">Ele tabalarbɨtɨn hiː hɨtagɨt!" </ta>
            <ta e="T460" id="Seg_6591" s="T458">"Bejegit burujgut." </ta>
            <ta e="T472" id="Seg_6592" s="T460">Hette timek iččite tobuktaːn turar kihini kürej üŋüːtünen öttükke annʼaːrɨ hiri haːjbɨt. </ta>
            <ta e="T484" id="Seg_6593" s="T472">Kaččaga ere Kuŋadʼaj nöŋü͡ö hirinen taksa kötör da ikki atagar tura ekkiriːr. </ta>
            <ta e="T488" id="Seg_6594" s="T484">Körüleːbite, kös baran erer. </ta>
            <ta e="T494" id="Seg_6595" s="T488">Haŋaha köhön erer, agata kergen bi͡erbit. </ta>
            <ta e="T499" id="Seg_6596" s="T494">Kuŋadʼaj haŋahɨn nʼu͡ogutun kaban ɨlar: </ta>
            <ta e="T500" id="Seg_6597" s="T499">"ɨːppappɨn. </ta>
            <ta e="T503" id="Seg_6598" s="T500">Kaja di͡eki baragɨn?" </ta>
            <ta e="T506" id="Seg_6599" s="T503">Dʼaktar kötüten kaːlar. </ta>
            <ta e="T512" id="Seg_6600" s="T506">Kuŋadʼaj hite battaːn ölgöbüːnün ildʼe kaːlar. </ta>
            <ta e="T517" id="Seg_6601" s="T512">Kuŋadʼaj Agɨs koŋnomu͡oj iččitiger kiːrer: </ta>
            <ta e="T526" id="Seg_6602" s="T517">"Dʼe, Agɨs koŋnomu͡oj iččite, ejeleːk erdekke balɨstarbɨn karajan olor. </ta>
            <ta e="T532" id="Seg_6603" s="T526">Kuhagannɨk körü͡öŋ — biːr kününen hu͡ok oŋortu͡om. </ta>
            <ta e="T535" id="Seg_6604" s="T532">Haŋaspɨn bata bardɨm." </ta>
            <ta e="T539" id="Seg_6605" s="T535">Haŋahɨn batan hüteren keːher. </ta>
            <ta e="T545" id="Seg_6606" s="T539">Kannɨk dojdu uhugar körör, hiti͡ek hippet. </ta>
            <ta e="T550" id="Seg_6607" s="T545">Ergiten egelen dʼi͡etin di͡ek kü͡öjer. </ta>
            <ta e="T557" id="Seg_6608" s="T550">Tutu͡ok da ereːri tuppakka haŋahɨn kennitten kiːrer. </ta>
            <ta e="T561" id="Seg_6609" s="T557">Hanaha ogotun emijdiː oloror. </ta>
            <ta e="T564" id="Seg_6610" s="T561">Balɨstara ahɨː oloror. </ta>
            <ta e="T568" id="Seg_6611" s="T564">"Dʼe haŋaːs, burujgun ahardɨŋ. </ta>
            <ta e="T573" id="Seg_6612" s="T568">Ogogun ɨlbatagɨŋ bu͡ollar ölörü͡ök etim! </ta>
            <ta e="T578" id="Seg_6613" s="T573">Agɨs koŋnomu͡oj iččite kuttanaːrɨ kuttanar." </ta>
            <ta e="T579" id="Seg_6614" s="T578">Olorollor. </ta>
            <ta e="T583" id="Seg_6615" s="T579">Kahan ere ühüs künüger: </ta>
            <ta e="T589" id="Seg_6616" s="T583">"Dʼe Agɨs koŋnomu͡oj iččite, dʼoŋŋun mus. </ta>
            <ta e="T592" id="Seg_6617" s="T589">Munnʼaktɨ͡akpɨt", diːr Kuŋadʼaj. </ta>
            <ta e="T598" id="Seg_6618" s="T592">Dʼe munnʼallar hette u͡on dʼi͡e kihitin. </ta>
            <ta e="T600" id="Seg_6619" s="T598">Kuŋadʼaj eter: </ta>
            <ta e="T611" id="Seg_6620" s="T600">"Ehigi ikki küseːjin baːrgɨt — Agɨs koŋnomu͡oj küseːjine u͡onna hette timek iččite. </ta>
            <ta e="T615" id="Seg_6621" s="T611">Anɨ küseːjin bu͡olan büttügüt. </ta>
            <ta e="T621" id="Seg_6622" s="T615">Küseːjiŋŋit — iti u͡ol ogo — ubajɨm ogoto. </ta>
            <ta e="T625" id="Seg_6623" s="T621">Komunuŋ barɨgɨt ikki küŋŋe. </ta>
            <ta e="T628" id="Seg_6624" s="T625">Ühüs küŋŋütüger köhü͡ökküt. </ta>
            <ta e="T631" id="Seg_6625" s="T628">Bastɨŋŋɨt kini bu͡olu͡o." </ta>
            <ta e="T642" id="Seg_6626" s="T631">"Atɨn tabanɨ kölünüme, agaŋ tabatɨn, keri͡ehin, kölüneːr", diːr u͡olga [u͡ol ulaːppɨt]. </ta>
            <ta e="T644" id="Seg_6627" s="T642">"Bastataŋŋɨn köhü͡ökküt. </ta>
            <ta e="T658" id="Seg_6628" s="T644">Mas hagatɨnan dʼirbiː baːr bu͡olu͡o, bu dʼirbiː mu͡ora di͡eki öttünen köhü͡öŋ, ikki ɨjɨ bɨha. </ta>
            <ta e="T668" id="Seg_6629" s="T658">Elete bu͡ollagɨna, dʼirbiːte büttegine hir bu͡olu͡o, kaːrɨn annɨttan buru͡olaːk bu͡olu͡o. </ta>
            <ta e="T672" id="Seg_6630" s="T668">Ol bihigi törüt hirbit. </ta>
            <ta e="T677" id="Seg_6631" s="T672">Ulakan hu͡olga kiːri͡eŋ, ajmaktargar tiji͡eŋ. </ta>
            <ta e="T682" id="Seg_6632" s="T677">Dʼonnor, tabagatɨn tüːnü-künü bɨha körü͡ökküt. </ta>
            <ta e="T690" id="Seg_6633" s="T682">Agɨs koŋnomu͡oj küseːjine, hette timek iččite, emi͡e körsü͡ökküt. </ta>
            <ta e="T703" id="Seg_6634" s="T690">Min bu͡ollagɨna bu üčügejdik ajannɨːrgɨt tuhuttan kuhagan dʼonu — čaŋɨttarɨ, kɨːllarɨ hu͡ok gɨna bardɨm. </ta>
            <ta e="T714" id="Seg_6635" s="T703">Ol hirge tijer küŋŋer tiji͡em, ulakannɨk haːraːtakpɨna ühüs künüger tiji͡em", diːr. </ta>
            <ta e="T717" id="Seg_6636" s="T714">Ühüs künüger köhöllör. </ta>
            <ta e="T722" id="Seg_6637" s="T717">Kuŋadʼaj aragan barar, hu͡os hatɨː. </ta>
            <ta e="T728" id="Seg_6638" s="T722">Baː ogo bastɨŋ bu͡olan köhön istiler. </ta>
            <ta e="T735" id="Seg_6639" s="T728">Ketenen ajannɨːllar, tojonu-kotunu barɨtɨn keteter bu u͡ol. </ta>
            <ta e="T738" id="Seg_6640" s="T735">ɨjɨn ortoto bu͡olbut. </ta>
            <ta e="T741" id="Seg_6641" s="T738">Dʼirbiːtin irdeːn istege. </ta>
            <ta e="T749" id="Seg_6642" s="T741">Araj mu͡ora di͡eki dʼirbiː aragar, ürdüger kihi hɨldʼar. </ta>
            <ta e="T752" id="Seg_6643" s="T749">"Kaja di͡ekki kihiginij?" </ta>
            <ta e="T754" id="Seg_6644" s="T752">"Tu͡ok keli͡ej? </ta>
            <ta e="T756" id="Seg_6645" s="T754">Dʼi͡eni-u͡otu bilbeppin. </ta>
            <ta e="T760" id="Seg_6646" s="T756">Töröːbüt ubajbɨn hütere hɨldʼabɨn." </ta>
            <ta e="T762" id="Seg_6647" s="T760">"Kajalaraj ol?" </ta>
            <ta e="T765" id="Seg_6648" s="T762">"Agɨs koŋnomu͡oj küseːjine!" </ta>
            <ta e="T766" id="Seg_6649" s="T765">"Heːj! </ta>
            <ta e="T769" id="Seg_6650" s="T766">Ol min kihim. </ta>
            <ta e="T773" id="Seg_6651" s="T769">Ol delemičeː üːren iher. </ta>
            <ta e="T778" id="Seg_6652" s="T773">Dʼe oččogo mi͡eke kihi bu͡ol." </ta>
            <ta e="T780" id="Seg_6653" s="T778">Haŋara tarpat. </ta>
            <ta e="T796" id="Seg_6654" s="T780">"I͡ehili kihi bu͡olu͡oŋ hu͡oga, bu üs halaːlaːk kürejim aŋar halaːta kaŋɨrgaha bu͡olu͡oŋ, ikkite bukatɨːr eːldete kaŋɨrgastaːk." </ta>
            <ta e="T799" id="Seg_6655" s="T796">Kihi höbülener, barsar. </ta>
            <ta e="T801" id="Seg_6656" s="T799">Köhön istiler. </ta>
            <ta e="T803" id="Seg_6657" s="T801">ɨjdara baranna. </ta>
            <ta e="T807" id="Seg_6658" s="T803">Nöŋü͡ö ɨj ortoto bu͡olla. </ta>
            <ta e="T812" id="Seg_6659" s="T807">Emi͡e kihini kördüler biːr dʼirbiːkeːŋŋe. </ta>
            <ta e="T814" id="Seg_6660" s="T812">Emi͡e doroːbolohor. </ta>
            <ta e="T818" id="Seg_6661" s="T814">"Kaja di͡eki dʼi͡eleːk-u͡ottaːk kihiginij?" </ta>
            <ta e="T820" id="Seg_6662" s="T818">"Dʼi͡e-u͡ot hu͡ok. </ta>
            <ta e="T822" id="Seg_6663" s="T820">Hɨldʼabɨn agaj. </ta>
            <ta e="T824" id="Seg_6664" s="T822">Ubajbɨn kördüːbün." </ta>
            <ta e="T827" id="Seg_6665" s="T824">"Tu͡ok etej ubajɨn?" </ta>
            <ta e="T831" id="Seg_6666" s="T827">"Eː, hette timek iččite!" </ta>
            <ta e="T837" id="Seg_6667" s="T831">"Eː, ol delemičeː üːren iher ubajɨŋ. </ta>
            <ta e="T841" id="Seg_6668" s="T837">Kaja mi͡eke kihi bu͡olu͡oŋ?" </ta>
            <ta e="T842" id="Seg_6669" s="T841">Haŋarbat. </ta>
            <ta e="T849" id="Seg_6670" s="T842">"Bu͡olu͡oŋ hu͡oga, bu kürejim halaːtɨgar kaŋɨrgas gɨnɨ͡am!" </ta>
            <ta e="T852" id="Seg_6671" s="T849">Höbüleher bili kihi. </ta>
            <ta e="T854" id="Seg_6672" s="T852">Ubajɨgar tijer: </ta>
            <ta e="T856" id="Seg_6673" s="T854">"Togo ölörbökküt? </ta>
            <ta e="T860" id="Seg_6674" s="T856">Ogogo bas berinegit du͡o?" </ta>
            <ta e="T862" id="Seg_6675" s="T860">"Hu͡ok, kebis." </ta>
            <ta e="T864" id="Seg_6676" s="T862">"Bultu͡okka naːda. </ta>
            <ta e="T867" id="Seg_6677" s="T864">Kohuta da bert." </ta>
            <ta e="T869" id="Seg_6678" s="T867">"Če, bagalɨ͡akpɨt!" </ta>
            <ta e="T873" id="Seg_6679" s="T869">Ki͡ehe tüheller biːr hirge. </ta>
            <ta e="T880" id="Seg_6680" s="T873">Ahɨːllar, u͡ol dʼonun batan tahaːrar ü͡ör ketete. </ta>
            <ta e="T883" id="Seg_6681" s="T880">Ogo utujan kaːlar. </ta>
            <ta e="T887" id="Seg_6682" s="T883">Kihiler kalɨjar haŋalarɨn ister. </ta>
            <ta e="T891" id="Seg_6683" s="T887">Kihi batɨjannan aːnɨ hegeter: </ta>
            <ta e="T897" id="Seg_6684" s="T891">"Kaja, togo ketiːgit miːgin, ü͡örü keteːŋ!" </ta>
            <ta e="T900" id="Seg_6685" s="T897">Dʼon töttörü hahallar. </ta>
            <ta e="T904" id="Seg_6686" s="T900">"Harsi͡erdaːŋŋɨ uːtugar bagalɨ͡akpɨt", deheller. </ta>
            <ta e="T906" id="Seg_6687" s="T904">Emi͡e bürüːrdüːller. </ta>
            <ta e="T909" id="Seg_6688" s="T906">Emi͡e "tugu ketiːgit? </ta>
            <ta e="T912" id="Seg_6689" s="T909">Barɨŋ", batan ɨːtar. </ta>
            <ta e="T914" id="Seg_6690" s="T912">Honton tɨːppataktara. </ta>
            <ta e="T920" id="Seg_6691" s="T914">Ikki ɨjdara bɨstarɨgar dʼirbiːlere elete bu͡olar. </ta>
            <ta e="T923" id="Seg_6692" s="T920">Körbüte — hir bu͡olbut. </ta>
            <ta e="T926" id="Seg_6693" s="T923">Kaːr annɨttan buru͡okaːkɨna. </ta>
            <ta e="T932" id="Seg_6694" s="T926">Mu͡ora di͡ekkitten kütür bagajɨ orok keler. </ta>
            <ta e="T937" id="Seg_6695" s="T932">Hir ihin di͡eki barar orok. </ta>
            <ta e="T939" id="Seg_6696" s="T937">Baran iheller. </ta>
            <ta e="T946" id="Seg_6697" s="T939">Pireːme noru͡ottan noru͡ot, dʼi͡etten dʼi͡e hirge keleller. </ta>
            <ta e="T950" id="Seg_6698" s="T946">Biːr dʼi͡e tahɨgar toktuːr. </ta>
            <ta e="T955" id="Seg_6699" s="T950">Biːr kɨrdʼagas hogus dʼaktar taksar: </ta>
            <ta e="T959" id="Seg_6700" s="T955">"Baj, tabata oduː taba! </ta>
            <ta e="T963" id="Seg_6701" s="T959">Bihigi törüt tababɨt kurduguj? </ta>
            <ta e="T970" id="Seg_6702" s="T963">Bɨlɨr ikki kihi barbɨta, ol törüttere bu͡olu͡o!" </ta>
            <ta e="T981" id="Seg_6703" s="T970">Üs dʼi͡etten üs dʼaktar taksan emi͡e taːjbɨttar törütterin, kepseten dʼe bilseller. </ta>
            <ta e="T983" id="Seg_6704" s="T981">Uruːlarɨn bulsallar. </ta>
            <ta e="T985" id="Seg_6705" s="T983">Kuŋadʼajɨ küːteller. </ta>
            <ta e="T990" id="Seg_6706" s="T985">Ühüs künün ortoto Kuŋadʼaj keler. </ta>
            <ta e="T997" id="Seg_6707" s="T990">Et aːta bütüne hu͡ok, tiriːtin aŋara kaːlbɨt. </ta>
            <ta e="T1001" id="Seg_6708" s="T997">"Ogom, kelbikkin — dʼe üčügej!" </ta>
            <ta e="T1008" id="Seg_6709" s="T1001">Kuhagan dʼonu baraːn, dojdu buldun arɨčča kɨ͡ajdɨm. </ta>
            <ta e="T1013" id="Seg_6710" s="T1008">Tɨːnɨm ere kellim, gojobuːn oŋoronnor." </ta>
            <ta e="T1018" id="Seg_6711" s="T1013">"Dʼe olor, tu͡oktan da kuttanɨma!" </ta>
            <ta e="T1022" id="Seg_6712" s="T1018">Kuŋadʼaj dʼe kojut tiller. </ta>
            <ta e="T1028" id="Seg_6713" s="T1022">U͡ol küseːjin bu͡olan olorbuta bu omukka. </ta>
            <ta e="T1029" id="Seg_6714" s="T1028">Elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_6715" s="T0">üs</ta>
            <ta e="T2" id="Seg_6716" s="T1">inibiː-ler</ta>
            <ta e="T3" id="Seg_6717" s="T2">olor-but-tar</ta>
            <ta e="T4" id="Seg_6718" s="T3">biːr-dere</ta>
            <ta e="T5" id="Seg_6719" s="T4">kɨra</ta>
            <ta e="T6" id="Seg_6720" s="T5">kuččukkaːn</ta>
            <ta e="T7" id="Seg_6721" s="T6">kihi</ta>
            <ta e="T8" id="Seg_6722" s="T7">orto-loro</ta>
            <ta e="T9" id="Seg_6723" s="T8">kaːm-pat</ta>
            <ta e="T10" id="Seg_6724" s="T9">tobug-u-nan</ta>
            <ta e="T11" id="Seg_6725" s="T10">agaj</ta>
            <ta e="T12" id="Seg_6726" s="T11">hɨːl-ar</ta>
            <ta e="T13" id="Seg_6727" s="T12">Kuŋadʼaj</ta>
            <ta e="T14" id="Seg_6728" s="T13">di͡e-n</ta>
            <ta e="T15" id="Seg_6729" s="T14">ubaj-dara</ta>
            <ta e="T16" id="Seg_6730" s="T15">aːt-a</ta>
            <ta e="T17" id="Seg_6731" s="T16">hu͡ok</ta>
            <ta e="T18" id="Seg_6732" s="T17">bulčut</ta>
            <ta e="T19" id="Seg_6733" s="T18">dʼaktar-daːk</ta>
            <ta e="T20" id="Seg_6734" s="T19">biːr</ta>
            <ta e="T21" id="Seg_6735" s="T20">hahɨl</ta>
            <ta e="T22" id="Seg_6736" s="T21">ogo-loːk</ta>
            <ta e="T23" id="Seg_6737" s="T22">taba-lara</ta>
            <ta e="T24" id="Seg_6738" s="T23">hu͡oč-hogotok</ta>
            <ta e="T25" id="Seg_6739" s="T24">at</ta>
            <ta e="T26" id="Seg_6740" s="T25">kördük</ta>
            <ta e="T27" id="Seg_6741" s="T26">üje-ge</ta>
            <ta e="T28" id="Seg_6742" s="T27">kölüm-met-ter</ta>
            <ta e="T29" id="Seg_6743" s="T28">kös-tök-törüne</ta>
            <ta e="T30" id="Seg_6744" s="T29">ere</ta>
            <ta e="T31" id="Seg_6745" s="T30">dʼi͡e-leri-n</ta>
            <ta e="T32" id="Seg_6746" s="T31">tar-tar-al-lar</ta>
            <ta e="T33" id="Seg_6747" s="T32">ubaj-dara</ta>
            <ta e="T34" id="Seg_6748" s="T33">aːt-taːk</ta>
            <ta e="T35" id="Seg_6749" s="T34">bulčut</ta>
            <ta e="T36" id="Seg_6750" s="T35">kɨːl-ɨ</ta>
            <ta e="T37" id="Seg_6751" s="T36">bultuː-r</ta>
            <ta e="T38" id="Seg_6752" s="T37">balɨg-ɨ</ta>
            <ta e="T39" id="Seg_6753" s="T38">bu͡ollun</ta>
            <ta e="T40" id="Seg_6754" s="T39">dʼɨl</ta>
            <ta e="T41" id="Seg_6755" s="T40">haːs-tɨj-ar-ɨ-n</ta>
            <ta e="T42" id="Seg_6756" s="T41">hagana</ta>
            <ta e="T43" id="Seg_6757" s="T42">ubaj-dara</ta>
            <ta e="T44" id="Seg_6758" s="T43">ɨ͡aldʼ-ar</ta>
            <ta e="T45" id="Seg_6759" s="T44">bergeː-bit</ta>
            <ta e="T46" id="Seg_6760" s="T45">üh-üs</ta>
            <ta e="T47" id="Seg_6761" s="T46">kün-ü-ger</ta>
            <ta e="T48" id="Seg_6762" s="T47">bu͡opsa</ta>
            <ta e="T49" id="Seg_6763" s="T48">möltöː-büt</ta>
            <ta e="T50" id="Seg_6764" s="T49">keri͡es</ta>
            <ta e="T51" id="Seg_6765" s="T50">ep-pit</ta>
            <ta e="T52" id="Seg_6766" s="T51">dʼaktar-ɨ-gar</ta>
            <ta e="T53" id="Seg_6767" s="T52">dʼe</ta>
            <ta e="T54" id="Seg_6768" s="T53">min</ta>
            <ta e="T55" id="Seg_6769" s="T54">öl-lü-m</ta>
            <ta e="T56" id="Seg_6770" s="T55">kim-i-ŋ</ta>
            <ta e="T57" id="Seg_6771" s="T56">karaj-ɨ͡ag-a=j</ta>
            <ta e="T58" id="Seg_6772" s="T57">bultaː-bɨt</ta>
            <ta e="T59" id="Seg_6773" s="T58">bul-pu-n</ta>
            <ta e="T60" id="Seg_6774" s="T59">hajɨn-ɨ</ta>
            <ta e="T61" id="Seg_6775" s="T60">bɨha</ta>
            <ta e="T62" id="Seg_6776" s="T61">ah-ɨ͡ak-kɨt</ta>
            <ta e="T63" id="Seg_6777" s="T62">as-kɨ-n</ta>
            <ta e="T64" id="Seg_6778" s="T63">baraː-tak-kɨna</ta>
            <ta e="T65" id="Seg_6779" s="T64">ehiːl</ta>
            <ta e="T66" id="Seg_6780" s="T65">tiriː-ti-n</ta>
            <ta e="T67" id="Seg_6781" s="T66">tɨh-ɨ-n</ta>
            <ta e="T68" id="Seg_6782" s="T67">harɨː-laː-n</ta>
            <ta e="T69" id="Seg_6783" s="T68">min-neː-n</ta>
            <ta e="T70" id="Seg_6784" s="T69">ih-eːr</ta>
            <ta e="T71" id="Seg_6785" s="T70">iti</ta>
            <ta e="T72" id="Seg_6786" s="T71">dʼon-u-ŋ</ta>
            <ta e="T73" id="Seg_6787" s="T72">tuha-ta</ta>
            <ta e="T74" id="Seg_6788" s="T73">hu͡ok</ta>
            <ta e="T75" id="Seg_6789" s="T74">dʼon</ta>
            <ta e="T76" id="Seg_6790" s="T75">öl-bük-kü-nen</ta>
            <ta e="T77" id="Seg_6791" s="T76">öl-ö-gün</ta>
            <ta e="T78" id="Seg_6792" s="T77">ogo-m</ta>
            <ta e="T79" id="Seg_6793" s="T78">bagar</ta>
            <ta e="T80" id="Seg_6794" s="T79">kihi</ta>
            <ta e="T81" id="Seg_6795" s="T80">bu͡ol-u͡o</ta>
            <ta e="T82" id="Seg_6796" s="T81">öl-bö-tög-üne</ta>
            <ta e="T83" id="Seg_6797" s="T82">kini</ta>
            <ta e="T84" id="Seg_6798" s="T83">iːt-i͡e</ta>
            <ta e="T85" id="Seg_6799" s="T84">ele-te</ta>
            <ta e="T86" id="Seg_6800" s="T85">öl-ön</ta>
            <ta e="T87" id="Seg_6801" s="T86">kaːl-ar</ta>
            <ta e="T88" id="Seg_6802" s="T87">ɨt-a-n-a-ɨt-a-n-a</ta>
            <ta e="T89" id="Seg_6803" s="T88">dʼaktar</ta>
            <ta e="T90" id="Seg_6804" s="T89">uːr-an</ta>
            <ta e="T91" id="Seg_6805" s="T90">keːh-er</ta>
            <ta e="T92" id="Seg_6806" s="T91">bu</ta>
            <ta e="T93" id="Seg_6807" s="T92">hajɨn-a</ta>
            <ta e="T94" id="Seg_6808" s="T93">bu͡ol-ar</ta>
            <ta e="T95" id="Seg_6809" s="T94">as-tarɨ-n</ta>
            <ta e="T96" id="Seg_6810" s="T95">kat-a-r-ɨ-n-al-lar</ta>
            <ta e="T97" id="Seg_6811" s="T96">hajɨn-narɨ-n</ta>
            <ta e="T98" id="Seg_6812" s="T97">bɨh-al-lar</ta>
            <ta e="T99" id="Seg_6813" s="T98">kühün-neri-ger</ta>
            <ta e="T100" id="Seg_6814" s="T99">tɨs-tarɨ-n</ta>
            <ta e="T101" id="Seg_6815" s="T100">tiriː-leri-n</ta>
            <ta e="T102" id="Seg_6816" s="T101">hiː-l-ler</ta>
            <ta e="T103" id="Seg_6817" s="T102">barɨ-ta</ta>
            <ta e="T104" id="Seg_6818" s="T103">baran-ar</ta>
            <ta e="T105" id="Seg_6819" s="T104">kɨhɨn</ta>
            <ta e="T106" id="Seg_6820" s="T105">bu͡ol-ar</ta>
            <ta e="T107" id="Seg_6821" s="T106">h-i͡ek</ta>
            <ta e="T108" id="Seg_6822" s="T107">hir-dere</ta>
            <ta e="T109" id="Seg_6823" s="T108">hu͡ok</ta>
            <ta e="T110" id="Seg_6824" s="T109">tɨ͡al-a</ta>
            <ta e="T111" id="Seg_6825" s="T110">hu͡ok</ta>
            <ta e="T112" id="Seg_6826" s="T111">kün</ta>
            <ta e="T113" id="Seg_6827" s="T112">araj</ta>
            <ta e="T114" id="Seg_6828" s="T113">hɨ͡arga-laːk</ta>
            <ta e="T115" id="Seg_6829" s="T114">tɨ͡ah-ɨ-n</ta>
            <ta e="T116" id="Seg_6830" s="T115">ist-er</ta>
            <ta e="T117" id="Seg_6831" s="T116">dʼaktar</ta>
            <ta e="T118" id="Seg_6832" s="T117">taks-a</ta>
            <ta e="T119" id="Seg_6833" s="T118">köt-ör</ta>
            <ta e="T120" id="Seg_6834" s="T119">haŋa</ta>
            <ta e="T121" id="Seg_6835" s="T120">ere</ta>
            <ta e="T122" id="Seg_6836" s="T121">ihill-er</ta>
            <ta e="T123" id="Seg_6837" s="T122">hir-i-ger</ta>
            <ta e="T124" id="Seg_6838" s="T123">toktoː-but</ta>
            <ta e="T125" id="Seg_6839" s="T124">araj</ta>
            <ta e="T126" id="Seg_6840" s="T125">dʼaktar</ta>
            <ta e="T127" id="Seg_6841" s="T126">aga-ta</ta>
            <ta e="T128" id="Seg_6842" s="T127">agɨs</ta>
            <ta e="T129" id="Seg_6843" s="T128">konnomu͡oj</ta>
            <ta e="T130" id="Seg_6844" s="T129">ičči-te</ta>
            <ta e="T131" id="Seg_6845" s="T130">küseːjin-e</ta>
            <ta e="T132" id="Seg_6846" s="T131">kini͡es</ta>
            <ta e="T133" id="Seg_6847" s="T132">e-bit</ta>
            <ta e="T134" id="Seg_6848" s="T133">kajɨː</ta>
            <ta e="T135" id="Seg_6849" s="T134">kajdak</ta>
            <ta e="T136" id="Seg_6850" s="T135">olor-o-gut</ta>
            <ta e="T137" id="Seg_6851" s="T136">er-i-m</ta>
            <ta e="T138" id="Seg_6852" s="T137">öl-büt-e</ta>
            <ta e="T139" id="Seg_6853" s="T138">bɨlɨrɨːn</ta>
            <ta e="T140" id="Seg_6854" s="T139">kajɨː</ta>
            <ta e="T141" id="Seg_6855" s="T140">slaːbu͡ok</ta>
            <ta e="T142" id="Seg_6856" s="T141">olus</ta>
            <ta e="T143" id="Seg_6857" s="T142">ü͡ör-dü-m</ta>
            <ta e="T144" id="Seg_6858" s="T143">taŋɨn</ta>
            <ta e="T145" id="Seg_6859" s="T144">bar-ɨ͡ak</ta>
            <ta e="T146" id="Seg_6860" s="T145">min</ta>
            <ta e="T147" id="Seg_6861" s="T146">dʼi͡e-be-r</ta>
            <ta e="T148" id="Seg_6862" s="T147">kaja</ta>
            <ta e="T149" id="Seg_6863" s="T148">ogo-bu-n</ta>
            <ta e="T150" id="Seg_6864" s="T149">dʼom-mu-n</ta>
            <ta e="T151" id="Seg_6865" s="T150">okt-on</ta>
            <ta e="T152" id="Seg_6866" s="T151">öl-lünner</ta>
            <ta e="T153" id="Seg_6867" s="T152">bar-ɨ͡ak</ta>
            <ta e="T154" id="Seg_6868" s="T153">dʼaktar</ta>
            <ta e="T155" id="Seg_6869" s="T154">taŋn-an</ta>
            <ta e="T156" id="Seg_6870" s="T155">tij-en</ta>
            <ta e="T157" id="Seg_6871" s="T156">meŋest-er</ta>
            <ta e="T158" id="Seg_6872" s="T157">da</ta>
            <ta e="T159" id="Seg_6873" s="T158">kötüt-en</ta>
            <ta e="T160" id="Seg_6874" s="T159">kaːl-al-lar</ta>
            <ta e="T161" id="Seg_6875" s="T160">Kuŋadʼaj-daːk</ta>
            <ta e="T162" id="Seg_6876" s="T161">kam</ta>
            <ta e="T163" id="Seg_6877" s="T162">aččɨk</ta>
            <ta e="T164" id="Seg_6878" s="T163">hɨt-tak-tara</ta>
            <ta e="T165" id="Seg_6879" s="T164">karaŋa</ta>
            <ta e="T166" id="Seg_6880" s="T165">aːh-an</ta>
            <ta e="T167" id="Seg_6881" s="T166">hɨrdɨː-r-ga</ta>
            <ta e="T168" id="Seg_6882" s="T167">bar-ar</ta>
            <ta e="T169" id="Seg_6883" s="T168">Kuŋadʼaj</ta>
            <ta e="T170" id="Seg_6884" s="T169">balt-ɨ-tɨ-gar</ta>
            <ta e="T171" id="Seg_6885" s="T170">et-er</ta>
            <ta e="T172" id="Seg_6886" s="T171">taba-gɨ-n</ta>
            <ta e="T173" id="Seg_6887" s="T172">kölüj</ta>
            <ta e="T174" id="Seg_6888" s="T173">hɨp-pɨt-ɨ-nan</ta>
            <ta e="T175" id="Seg_6889" s="T174">öl-ü͡ök-püt</ta>
            <ta e="T176" id="Seg_6890" s="T175">agɨs</ta>
            <ta e="T177" id="Seg_6891" s="T176">koŋnomu͡oj-u</ta>
            <ta e="T178" id="Seg_6892" s="T177">bat-ɨ͡ak</ta>
            <ta e="T179" id="Seg_6893" s="T178">taba-larɨ-n</ta>
            <ta e="T180" id="Seg_6894" s="T179">kölüj-en</ta>
            <ta e="T181" id="Seg_6895" s="T180">dʼi͡e-leri-n</ta>
            <ta e="T182" id="Seg_6896" s="T181">kötür-en</ta>
            <ta e="T183" id="Seg_6897" s="T182">meŋest-en</ta>
            <ta e="T184" id="Seg_6898" s="T183">ett-e-t-en</ta>
            <ta e="T185" id="Seg_6899" s="T184">is-tek-tere</ta>
            <ta e="T186" id="Seg_6900" s="T185">kah</ta>
            <ta e="T187" id="Seg_6901" s="T186">u͡on-na</ta>
            <ta e="T188" id="Seg_6902" s="T187">kös-püt-ter</ta>
            <ta e="T189" id="Seg_6903" s="T188">biːrde</ta>
            <ta e="T190" id="Seg_6904" s="T189">kütürü͡ök</ta>
            <ta e="T191" id="Seg_6905" s="T190">his-ke</ta>
            <ta e="T192" id="Seg_6906" s="T191">ɨtt-ɨ-bɨt-tar</ta>
            <ta e="T193" id="Seg_6907" s="T192">körüleː-bit-tere</ta>
            <ta e="T194" id="Seg_6908" s="T193">kütür-deːk</ta>
            <ta e="T195" id="Seg_6909" s="T194">teːn</ta>
            <ta e="T196" id="Seg_6910" s="T195">teːn</ta>
            <ta e="T197" id="Seg_6911" s="T196">orto-tu-gar</ta>
            <ta e="T198" id="Seg_6912" s="T197">ü͡ör</ta>
            <ta e="T199" id="Seg_6913" s="T198">bögö</ta>
            <ta e="T200" id="Seg_6914" s="T199">teːn</ta>
            <ta e="T201" id="Seg_6915" s="T200">uŋu͡or</ta>
            <ta e="T202" id="Seg_6916" s="T201">hette</ta>
            <ta e="T203" id="Seg_6917" s="T202">u͡on</ta>
            <ta e="T204" id="Seg_6918" s="T203">dʼi͡e</ta>
            <ta e="T205" id="Seg_6919" s="T204">kihi</ta>
            <ta e="T206" id="Seg_6920" s="T205">köst-ör</ta>
            <ta e="T207" id="Seg_6921" s="T206">bar-bɨt-tar</ta>
            <ta e="T208" id="Seg_6922" s="T207">ü͡ör</ta>
            <ta e="T209" id="Seg_6923" s="T208">ih-i-nen</ta>
            <ta e="T210" id="Seg_6924" s="T209">ü͡ör</ta>
            <ta e="T211" id="Seg_6925" s="T210">anaraː</ta>
            <ta e="T212" id="Seg_6926" s="T211">öttü-tü-ger</ta>
            <ta e="T213" id="Seg_6927" s="T212">tur</ta>
            <ta e="T214" id="Seg_6928" s="T213">di͡e-bit</ta>
            <ta e="T215" id="Seg_6929" s="T214">Kuŋadʼaj</ta>
            <ta e="T216" id="Seg_6930" s="T215">iti</ta>
            <ta e="T217" id="Seg_6931" s="T216">dʼi͡e-ler-ten</ta>
            <ta e="T218" id="Seg_6932" s="T217">biːr</ta>
            <ta e="T219" id="Seg_6933" s="T218">orduk-tarɨ-gar</ta>
            <ta e="T220" id="Seg_6934" s="T219">tokt-oːr</ta>
            <ta e="T221" id="Seg_6935" s="T220">nipte-leri-n</ta>
            <ta e="T222" id="Seg_6936" s="T221">betereː</ta>
            <ta e="T223" id="Seg_6937" s="T222">öttü-keːn-i-ger</ta>
            <ta e="T224" id="Seg_6938" s="T223">kör-büt-e</ta>
            <ta e="T225" id="Seg_6939" s="T224">biːr</ta>
            <ta e="T226" id="Seg_6940" s="T225">dʼi͡e</ta>
            <ta e="T227" id="Seg_6941" s="T226">barɨ-larɨ-ttan</ta>
            <ta e="T228" id="Seg_6942" s="T227">ulakan</ta>
            <ta e="T229" id="Seg_6943" s="T228">aːn</ta>
            <ta e="T230" id="Seg_6944" s="T229">tuh-u-gar</ta>
            <ta e="T231" id="Seg_6945" s="T230">tij-bit-ter</ta>
            <ta e="T232" id="Seg_6946" s="T231">da</ta>
            <ta e="T233" id="Seg_6947" s="T232">taba-larɨ-n</ta>
            <ta e="T234" id="Seg_6948" s="T233">ɨːt-an</ta>
            <ta e="T235" id="Seg_6949" s="T234">keːh-el-ler</ta>
            <ta e="T236" id="Seg_6950" s="T235">dʼi͡e</ta>
            <ta e="T237" id="Seg_6951" s="T236">tutt-an</ta>
            <ta e="T238" id="Seg_6952" s="T237">keːs-pit-ter</ta>
            <ta e="T239" id="Seg_6953" s="T238">ott-u-but-tar</ta>
            <ta e="T240" id="Seg_6954" s="T239">balt-ɨ-tɨ-n</ta>
            <ta e="T241" id="Seg_6955" s="T240">bar</ta>
            <ta e="T242" id="Seg_6956" s="T241">haŋas-ka-r</ta>
            <ta e="T243" id="Seg_6957" s="T242">ogo-tu-n</ta>
            <ta e="T244" id="Seg_6958" s="T243">em-ner-e</ta>
            <ta e="T245" id="Seg_6959" s="T244">kel-lin</ta>
            <ta e="T246" id="Seg_6960" s="T245">agɨs</ta>
            <ta e="T247" id="Seg_6961" s="T246">Koŋnomu͡oj-ga</ta>
            <ta e="T248" id="Seg_6962" s="T247">et</ta>
            <ta e="T249" id="Seg_6963" s="T248">öl-lü-büt</ta>
            <ta e="T250" id="Seg_6964" s="T249">biːr</ta>
            <ta e="T251" id="Seg_6965" s="T250">eme</ta>
            <ta e="T252" id="Seg_6966" s="T251">hürek</ta>
            <ta e="T253" id="Seg_6967" s="T252">aŋaːr-kaːn-ɨ-n</ta>
            <ta e="T254" id="Seg_6968" s="T253">kɨtta</ta>
            <ta e="T255" id="Seg_6969" s="T254">bɨ͡ar</ta>
            <ta e="T256" id="Seg_6970" s="T255">tulaːjag-ɨ-n</ta>
            <ta e="T257" id="Seg_6971" s="T256">beris-tin</ta>
            <ta e="T258" id="Seg_6972" s="T257">diː-r</ta>
            <ta e="T259" id="Seg_6973" s="T258">u͡ol</ta>
            <ta e="T260" id="Seg_6974" s="T259">hüːr-en</ta>
            <ta e="T261" id="Seg_6975" s="T260">kiːr-en</ta>
            <ta e="T262" id="Seg_6976" s="T261">et-er</ta>
            <ta e="T263" id="Seg_6977" s="T262">kör</ta>
            <ta e="T264" id="Seg_6978" s="T263">ere</ta>
            <ta e="T265" id="Seg_6979" s="T264">em-ter-er</ta>
            <ta e="T266" id="Seg_6980" s="T265">hokuču͡ok</ta>
            <ta e="T267" id="Seg_6981" s="T266">okt-on</ta>
            <ta e="T268" id="Seg_6982" s="T267">öl-lün</ta>
            <ta e="T269" id="Seg_6983" s="T268">diː-r</ta>
            <ta e="T270" id="Seg_6984" s="T269">dʼaktar</ta>
            <ta e="T271" id="Seg_6985" s="T270">kaja</ta>
            <ta e="T272" id="Seg_6986" s="T271">eheː</ta>
            <ta e="T273" id="Seg_6987" s="T272">ildʼit-teː-t-e</ta>
            <ta e="T274" id="Seg_6988" s="T273">ubaj-ɨ-m</ta>
            <ta e="T275" id="Seg_6989" s="T274">öl-lü-büt</ta>
            <ta e="T276" id="Seg_6990" s="T275">biːr</ta>
            <ta e="T277" id="Seg_6991" s="T276">eme</ta>
            <ta e="T278" id="Seg_6992" s="T277">hürek</ta>
            <ta e="T279" id="Seg_6993" s="T278">aŋaːr-kaːn-ɨ-n</ta>
            <ta e="T280" id="Seg_6994" s="T279">kɨtta</ta>
            <ta e="T281" id="Seg_6995" s="T280">bɨ͡ar</ta>
            <ta e="T282" id="Seg_6996" s="T281">tulaːjag-ɨ-n</ta>
            <ta e="T283" id="Seg_6997" s="T282">beris-tin</ta>
            <ta e="T284" id="Seg_6998" s="T283">kaːk</ta>
            <ta e="T285" id="Seg_6999" s="T284">okt-on</ta>
            <ta e="T286" id="Seg_7000" s="T285">öl-ü-ŋ</ta>
            <ta e="T287" id="Seg_7001" s="T286">ɨːp-pap-pɨn</ta>
            <ta e="T288" id="Seg_7002" s="T287">diː-r</ta>
            <ta e="T289" id="Seg_7003" s="T288">agɨs</ta>
            <ta e="T290" id="Seg_7004" s="T289">koŋnomu͡oj</ta>
            <ta e="T291" id="Seg_7005" s="T290">ičči-te</ta>
            <ta e="T292" id="Seg_7006" s="T291">utuj-aːktɨː-l-lar</ta>
            <ta e="T293" id="Seg_7007" s="T292">tug-u</ta>
            <ta e="T294" id="Seg_7008" s="T293">h-i͡ek-tere=j</ta>
            <ta e="T295" id="Seg_7009" s="T294">araj</ta>
            <ta e="T296" id="Seg_7010" s="T295">taba-lɨː-r</ta>
            <ta e="T297" id="Seg_7011" s="T296">haŋa</ta>
            <ta e="T298" id="Seg_7012" s="T297">ihill-er</ta>
            <ta e="T299" id="Seg_7013" s="T298">Kuŋadʼaj</ta>
            <ta e="T300" id="Seg_7014" s="T299">taks-an</ta>
            <ta e="T301" id="Seg_7015" s="T300">iːkt-iː</ta>
            <ta e="T302" id="Seg_7016" s="T301">tur-but</ta>
            <ta e="T303" id="Seg_7017" s="T302">tobug-a-r</ta>
            <ta e="T304" id="Seg_7018" s="T303">taba</ta>
            <ta e="T305" id="Seg_7019" s="T304">bögö</ta>
            <ta e="T306" id="Seg_7020" s="T305">kel-er</ta>
            <ta e="T307" id="Seg_7021" s="T306">aktamiː</ta>
            <ta e="T308" id="Seg_7022" s="T307">buːr</ta>
            <ta e="T309" id="Seg_7023" s="T308">emis</ta>
            <ta e="T310" id="Seg_7024" s="T309">bagajɨ</ta>
            <ta e="T311" id="Seg_7025" s="T310">kel-er</ta>
            <ta e="T312" id="Seg_7026" s="T311">u͡ol</ta>
            <ta e="T313" id="Seg_7027" s="T312">kab-an</ta>
            <ta e="T314" id="Seg_7028" s="T313">ɨl-ar</ta>
            <ta e="T315" id="Seg_7029" s="T314">mög-ön</ta>
            <ta e="T316" id="Seg_7030" s="T315">bögö</ta>
            <ta e="T317" id="Seg_7031" s="T316">bahag-ɨ-n</ta>
            <ta e="T318" id="Seg_7032" s="T317">ɨl-lar-ar</ta>
            <ta e="T319" id="Seg_7033" s="T318">kabarga-tɨ-n</ta>
            <ta e="T320" id="Seg_7034" s="T319">bɨh-a</ta>
            <ta e="T321" id="Seg_7035" s="T320">hot-or</ta>
            <ta e="T322" id="Seg_7036" s="T321">hül-bekke</ta>
            <ta e="T323" id="Seg_7037" s="T322">da</ta>
            <ta e="T324" id="Seg_7038" s="T323">killer-el-ler</ta>
            <ta e="T325" id="Seg_7039" s="T324">kü͡ös-ten-en</ta>
            <ta e="T326" id="Seg_7040" s="T325">dʼe</ta>
            <ta e="T327" id="Seg_7041" s="T326">ahɨː-l-lar</ta>
            <ta e="T328" id="Seg_7042" s="T327">kün-ü</ta>
            <ta e="T329" id="Seg_7043" s="T328">meldʼi</ta>
            <ta e="T330" id="Seg_7044" s="T329">ki͡ehe-ge</ta>
            <ta e="T331" id="Seg_7045" s="T330">di͡eri</ta>
            <ta e="T332" id="Seg_7046" s="T331">bu</ta>
            <ta e="T333" id="Seg_7047" s="T332">dʼi͡e</ta>
            <ta e="T334" id="Seg_7048" s="T333">tah-ɨ-gar</ta>
            <ta e="T335" id="Seg_7049" s="T334">kuččuguj</ta>
            <ta e="T336" id="Seg_7050" s="T335">dʼi͡e</ta>
            <ta e="T337" id="Seg_7051" s="T336">tur-ar</ta>
            <ta e="T338" id="Seg_7052" s="T337">čeː</ta>
            <ta e="T339" id="Seg_7053" s="T338">hette</ta>
            <ta e="T340" id="Seg_7054" s="T339">timek</ta>
            <ta e="T341" id="Seg_7055" s="T340">ičči-ti-ger</ta>
            <ta e="T342" id="Seg_7056" s="T341">bar</ta>
            <ta e="T343" id="Seg_7057" s="T342">iti</ta>
            <ta e="T344" id="Seg_7058" s="T343">dʼi͡e-ge</ta>
            <ta e="T345" id="Seg_7059" s="T344">staršina</ta>
            <ta e="T346" id="Seg_7060" s="T345">bu͡olu͡o</ta>
            <ta e="T347" id="Seg_7061" s="T346">kü͡ös-te</ta>
            <ta e="T348" id="Seg_7062" s="T347">kördöː</ta>
            <ta e="T349" id="Seg_7063" s="T348">u͡ol</ta>
            <ta e="T350" id="Seg_7064" s="T349">bar-ar</ta>
            <ta e="T351" id="Seg_7065" s="T350">hette</ta>
            <ta e="T352" id="Seg_7066" s="T351">timek</ta>
            <ta e="T353" id="Seg_7067" s="T352">ičči-ti-ger</ta>
            <ta e="T354" id="Seg_7068" s="T353">et-er</ta>
            <ta e="T355" id="Seg_7069" s="T354">ildʼit-i</ta>
            <ta e="T356" id="Seg_7070" s="T355">agɨs</ta>
            <ta e="T357" id="Seg_7071" s="T356">koŋnomu͡oj</ta>
            <ta e="T358" id="Seg_7072" s="T357">ičči-te</ta>
            <ta e="T359" id="Seg_7073" s="T358">bi͡er-d-e</ta>
            <ta e="T360" id="Seg_7074" s="T359">eni</ta>
            <ta e="T361" id="Seg_7075" s="T360">hu͡ok</ta>
            <ta e="T362" id="Seg_7076" s="T361">eː</ta>
            <ta e="T363" id="Seg_7077" s="T362">oččogo</ta>
            <ta e="T364" id="Seg_7078" s="T363">min</ta>
            <ta e="T365" id="Seg_7079" s="T364">emi͡e</ta>
            <ta e="T366" id="Seg_7080" s="T365">bi͡er-bep-pin</ta>
            <ta e="T367" id="Seg_7081" s="T366">ahɨː-l-lar</ta>
            <ta e="T368" id="Seg_7082" s="T367">harsi͡erda</ta>
            <ta e="T369" id="Seg_7083" s="T368">ü͡ör</ta>
            <ta e="T370" id="Seg_7084" s="T369">ih-er</ta>
            <ta e="T371" id="Seg_7085" s="T370">emi͡e</ta>
            <ta e="T372" id="Seg_7086" s="T371">tobug-u-nan</ta>
            <ta e="T373" id="Seg_7087" s="T372">ün-en</ta>
            <ta e="T374" id="Seg_7088" s="T373">taks-ar</ta>
            <ta e="T375" id="Seg_7089" s="T374">Kuŋadʼaj</ta>
            <ta e="T376" id="Seg_7090" s="T375">Kuŋadʼaj</ta>
            <ta e="T377" id="Seg_7091" s="T376">töröː-böt</ta>
            <ta e="T378" id="Seg_7092" s="T377">maːŋkaːj-ɨ</ta>
            <ta e="T379" id="Seg_7093" s="T378">tut-an</ta>
            <ta e="T380" id="Seg_7094" s="T379">ɨl-ar</ta>
            <ta e="T381" id="Seg_7095" s="T380">kaldaː-tɨ-ttan</ta>
            <ta e="T382" id="Seg_7096" s="T381">kaja=kuː</ta>
            <ta e="T383" id="Seg_7097" s="T382">bahag-ɨ</ta>
            <ta e="T384" id="Seg_7098" s="T383">tahaːr</ta>
            <ta e="T385" id="Seg_7099" s="T384">ölör-ön</ta>
            <ta e="T386" id="Seg_7100" s="T385">keːh-er</ta>
            <ta e="T387" id="Seg_7101" s="T386">emi͡e</ta>
            <ta e="T388" id="Seg_7102" s="T387">hül-bekke</ta>
            <ta e="T389" id="Seg_7103" s="T388">hiː-l-ler</ta>
            <ta e="T390" id="Seg_7104" s="T389">kaja</ta>
            <ta e="T391" id="Seg_7105" s="T390">bu</ta>
            <ta e="T392" id="Seg_7106" s="T391">bili</ta>
            <ta e="T393" id="Seg_7107" s="T392">tojot-tor</ta>
            <ta e="T394" id="Seg_7108" s="T393">gi͡en-neri-n</ta>
            <ta e="T395" id="Seg_7109" s="T394">ölör-büt</ta>
            <ta e="T396" id="Seg_7110" s="T395">e-bit</ta>
            <ta e="T397" id="Seg_7111" s="T396">utuj-al-lar</ta>
            <ta e="T398" id="Seg_7112" s="T397">harsi͡erda</ta>
            <ta e="T399" id="Seg_7113" s="T398">Kuŋadʼaj</ta>
            <ta e="T400" id="Seg_7114" s="T399">taks-ɨ-bɨt-a</ta>
            <ta e="T401" id="Seg_7115" s="T400">agɨs</ta>
            <ta e="T402" id="Seg_7116" s="T401">koŋnomu͡oj</ta>
            <ta e="T403" id="Seg_7117" s="T402">küseːjin-e</ta>
            <ta e="T404" id="Seg_7118" s="T403">kürej-i-n</ta>
            <ta e="T405" id="Seg_7119" s="T404">ku͡ohan-a</ta>
            <ta e="T406" id="Seg_7120" s="T405">olor-or</ta>
            <ta e="T407" id="Seg_7121" s="T406">kajaː</ta>
            <ta e="T408" id="Seg_7122" s="T407">agɨs</ta>
            <ta e="T409" id="Seg_7123" s="T408">koŋnomu͡oj</ta>
            <ta e="T410" id="Seg_7124" s="T409">ičči-te</ta>
            <ta e="T411" id="Seg_7125" s="T410">doroːbo</ta>
            <ta e="T412" id="Seg_7126" s="T411">össü͡ö</ta>
            <ta e="T413" id="Seg_7127" s="T412">doroːbo</ta>
            <ta e="T414" id="Seg_7128" s="T413">bügün</ta>
            <ta e="T415" id="Seg_7129" s="T414">ölör-t-üː-büt</ta>
            <ta e="T416" id="Seg_7130" s="T415">ele</ta>
            <ta e="T417" id="Seg_7131" s="T416">taba-lar-bɨt-tan</ta>
            <ta e="T418" id="Seg_7132" s="T417">h-iː</ta>
            <ta e="T419" id="Seg_7133" s="T418">hɨt-a-gɨt</ta>
            <ta e="T420" id="Seg_7134" s="T419">beje-git</ta>
            <ta e="T421" id="Seg_7135" s="T420">buruj-gut</ta>
            <ta e="T422" id="Seg_7136" s="T421">agɨs</ta>
            <ta e="T423" id="Seg_7137" s="T422">koŋnomu͡oj</ta>
            <ta e="T424" id="Seg_7138" s="T423">ičči-te</ta>
            <ta e="T425" id="Seg_7139" s="T424">tobuk-taː-n</ta>
            <ta e="T426" id="Seg_7140" s="T425">tur-ar</ta>
            <ta e="T427" id="Seg_7141" s="T426">kihi-ni</ta>
            <ta e="T428" id="Seg_7142" s="T427">kürej</ta>
            <ta e="T429" id="Seg_7143" s="T428">ünüː-tü-nen</ta>
            <ta e="T430" id="Seg_7144" s="T429">öttük-ke</ta>
            <ta e="T431" id="Seg_7145" s="T430">haːj-ar</ta>
            <ta e="T432" id="Seg_7146" s="T431">da</ta>
            <ta e="T433" id="Seg_7147" s="T432">hɨːh-an</ta>
            <ta e="T434" id="Seg_7148" s="T433">keːh-er</ta>
            <ta e="T435" id="Seg_7149" s="T434">Kuŋadʼaj-a</ta>
            <ta e="T436" id="Seg_7150" s="T435">kaːr</ta>
            <ta e="T437" id="Seg_7151" s="T436">ann-ɨ-nan</ta>
            <ta e="T438" id="Seg_7152" s="T437">bar-bɨt</ta>
            <ta e="T439" id="Seg_7153" s="T438">hette</ta>
            <ta e="T440" id="Seg_7154" s="T439">timek</ta>
            <ta e="T441" id="Seg_7155" s="T440">ičči-te</ta>
            <ta e="T442" id="Seg_7156" s="T441">kürej</ta>
            <ta e="T443" id="Seg_7157" s="T442">ku͡ohan-a</ta>
            <ta e="T444" id="Seg_7158" s="T443">olor-dog-una</ta>
            <ta e="T445" id="Seg_7159" s="T444">taks-ar</ta>
            <ta e="T446" id="Seg_7160" s="T445">kajaː</ta>
            <ta e="T447" id="Seg_7161" s="T446">hette</ta>
            <ta e="T448" id="Seg_7162" s="T447">timek</ta>
            <ta e="T449" id="Seg_7163" s="T448">ičči-te</ta>
            <ta e="T450" id="Seg_7164" s="T449">doroːbo</ta>
            <ta e="T451" id="Seg_7165" s="T450">össü͡ö</ta>
            <ta e="T452" id="Seg_7166" s="T451">doroːbo</ta>
            <ta e="T453" id="Seg_7167" s="T452">bügün</ta>
            <ta e="T454" id="Seg_7168" s="T453">ölör-t-üː-büt</ta>
            <ta e="T455" id="Seg_7169" s="T454">ele</ta>
            <ta e="T456" id="Seg_7170" s="T455">taba-lar-bɨtɨ-n</ta>
            <ta e="T457" id="Seg_7171" s="T456">h-iː</ta>
            <ta e="T458" id="Seg_7172" s="T457">hɨt-a-gɨt</ta>
            <ta e="T459" id="Seg_7173" s="T458">beje-git</ta>
            <ta e="T460" id="Seg_7174" s="T459">buruj-gut</ta>
            <ta e="T461" id="Seg_7175" s="T460">hette</ta>
            <ta e="T462" id="Seg_7176" s="T461">timek</ta>
            <ta e="T463" id="Seg_7177" s="T462">ičči-te</ta>
            <ta e="T464" id="Seg_7178" s="T463">tobuk-taː-n</ta>
            <ta e="T465" id="Seg_7179" s="T464">tur-ar</ta>
            <ta e="T466" id="Seg_7180" s="T465">kihi-ni</ta>
            <ta e="T467" id="Seg_7181" s="T466">kürej</ta>
            <ta e="T468" id="Seg_7182" s="T467">üŋüː-tü-nen</ta>
            <ta e="T469" id="Seg_7183" s="T468">öttük-ke</ta>
            <ta e="T470" id="Seg_7184" s="T469">annʼ-aːrɨ</ta>
            <ta e="T471" id="Seg_7185" s="T470">hir-i</ta>
            <ta e="T472" id="Seg_7186" s="T471">haːj-bɨt</ta>
            <ta e="T473" id="Seg_7187" s="T472">kaččaga</ta>
            <ta e="T474" id="Seg_7188" s="T473">ere</ta>
            <ta e="T475" id="Seg_7189" s="T474">Kuŋadʼaj</ta>
            <ta e="T476" id="Seg_7190" s="T475">nöŋü͡ö</ta>
            <ta e="T477" id="Seg_7191" s="T476">hir-i-nen</ta>
            <ta e="T478" id="Seg_7192" s="T477">taks-a</ta>
            <ta e="T479" id="Seg_7193" s="T478">köt-ör</ta>
            <ta e="T480" id="Seg_7194" s="T479">da</ta>
            <ta e="T481" id="Seg_7195" s="T480">ikki</ta>
            <ta e="T482" id="Seg_7196" s="T481">atag-a-r</ta>
            <ta e="T483" id="Seg_7197" s="T482">tur-a</ta>
            <ta e="T484" id="Seg_7198" s="T483">ekkiriː-r</ta>
            <ta e="T485" id="Seg_7199" s="T484">körüleː-bit-e</ta>
            <ta e="T486" id="Seg_7200" s="T485">kös</ta>
            <ta e="T487" id="Seg_7201" s="T486">bar-an</ta>
            <ta e="T488" id="Seg_7202" s="T487">er-er</ta>
            <ta e="T489" id="Seg_7203" s="T488">haŋah-a</ta>
            <ta e="T490" id="Seg_7204" s="T489">köh-ön</ta>
            <ta e="T491" id="Seg_7205" s="T490">er-er</ta>
            <ta e="T492" id="Seg_7206" s="T491">aga-ta</ta>
            <ta e="T493" id="Seg_7207" s="T492">kergen</ta>
            <ta e="T494" id="Seg_7208" s="T493">bi͡er-bit</ta>
            <ta e="T495" id="Seg_7209" s="T494">Kuŋadʼaj</ta>
            <ta e="T496" id="Seg_7210" s="T495">haŋah-ɨ-n</ta>
            <ta e="T497" id="Seg_7211" s="T496">nʼu͡ogu-tu-n</ta>
            <ta e="T498" id="Seg_7212" s="T497">kab-an</ta>
            <ta e="T499" id="Seg_7213" s="T498">ɨl-ar</ta>
            <ta e="T500" id="Seg_7214" s="T499">ɨːp-pap-pɨn</ta>
            <ta e="T501" id="Seg_7215" s="T500">kaja</ta>
            <ta e="T502" id="Seg_7216" s="T501">di͡eki</ta>
            <ta e="T503" id="Seg_7217" s="T502">bar-a-gɨn</ta>
            <ta e="T504" id="Seg_7218" s="T503">dʼaktar</ta>
            <ta e="T505" id="Seg_7219" s="T504">kötüt-en</ta>
            <ta e="T506" id="Seg_7220" s="T505">kaːl-ar</ta>
            <ta e="T507" id="Seg_7221" s="T506">Kuŋadʼaj</ta>
            <ta e="T508" id="Seg_7222" s="T507">hit-e</ta>
            <ta e="T509" id="Seg_7223" s="T508">bat-taː-n</ta>
            <ta e="T510" id="Seg_7224" s="T509">ölgöbüːn-ü-n</ta>
            <ta e="T511" id="Seg_7225" s="T510">ildʼ-e</ta>
            <ta e="T512" id="Seg_7226" s="T511">kaːl-ar</ta>
            <ta e="T513" id="Seg_7227" s="T512">Kuŋadʼaj</ta>
            <ta e="T514" id="Seg_7228" s="T513">agɨs</ta>
            <ta e="T515" id="Seg_7229" s="T514">koŋnomu͡oj</ta>
            <ta e="T516" id="Seg_7230" s="T515">ičči-ti-ger</ta>
            <ta e="T517" id="Seg_7231" s="T516">kiːr-er</ta>
            <ta e="T518" id="Seg_7232" s="T517">dʼe</ta>
            <ta e="T519" id="Seg_7233" s="T518">agɨs</ta>
            <ta e="T520" id="Seg_7234" s="T519">koŋnomu͡oj</ta>
            <ta e="T521" id="Seg_7235" s="T520">ičči-te</ta>
            <ta e="T522" id="Seg_7236" s="T521">eje-leːk</ta>
            <ta e="T523" id="Seg_7237" s="T522">er-dek-ke</ta>
            <ta e="T524" id="Seg_7238" s="T523">balɨs-tar-bɨ-n</ta>
            <ta e="T525" id="Seg_7239" s="T524">karaj-an</ta>
            <ta e="T526" id="Seg_7240" s="T525">olor</ta>
            <ta e="T527" id="Seg_7241" s="T526">kuhagan-nɨk</ta>
            <ta e="T528" id="Seg_7242" s="T527">kör-ü͡ö-ŋ</ta>
            <ta e="T529" id="Seg_7243" s="T528">biːr</ta>
            <ta e="T530" id="Seg_7244" s="T529">kün-ü-nen</ta>
            <ta e="T531" id="Seg_7245" s="T530">hu͡ok</ta>
            <ta e="T532" id="Seg_7246" s="T531">oŋor-t-u͡o-m</ta>
            <ta e="T533" id="Seg_7247" s="T532">haŋas-pɨ-n</ta>
            <ta e="T534" id="Seg_7248" s="T533">bat-a</ta>
            <ta e="T535" id="Seg_7249" s="T534">bar-dɨ-m</ta>
            <ta e="T536" id="Seg_7250" s="T535">haŋah-ɨ-n</ta>
            <ta e="T537" id="Seg_7251" s="T536">bat-an</ta>
            <ta e="T538" id="Seg_7252" s="T537">hüter-en</ta>
            <ta e="T539" id="Seg_7253" s="T538">keːh-er</ta>
            <ta e="T540" id="Seg_7254" s="T539">kannɨk</ta>
            <ta e="T541" id="Seg_7255" s="T540">dojdu</ta>
            <ta e="T542" id="Seg_7256" s="T541">uhug-a-r</ta>
            <ta e="T543" id="Seg_7257" s="T542">kör-ör</ta>
            <ta e="T544" id="Seg_7258" s="T543">hit-i͡ek</ta>
            <ta e="T545" id="Seg_7259" s="T544">hip-pet</ta>
            <ta e="T546" id="Seg_7260" s="T545">ergi-t-en</ta>
            <ta e="T547" id="Seg_7261" s="T546">egel-en</ta>
            <ta e="T548" id="Seg_7262" s="T547">dʼi͡e-ti-n</ta>
            <ta e="T549" id="Seg_7263" s="T548">di͡ek</ta>
            <ta e="T550" id="Seg_7264" s="T549">kü͡öj-er</ta>
            <ta e="T551" id="Seg_7265" s="T550">tut-u͡ok</ta>
            <ta e="T552" id="Seg_7266" s="T551">da</ta>
            <ta e="T553" id="Seg_7267" s="T552">ereːri</ta>
            <ta e="T554" id="Seg_7268" s="T553">tup-pakka</ta>
            <ta e="T555" id="Seg_7269" s="T554">haŋah-ɨ-n</ta>
            <ta e="T556" id="Seg_7270" s="T555">kenn-i-tten</ta>
            <ta e="T557" id="Seg_7271" s="T556">kiːr-er</ta>
            <ta e="T558" id="Seg_7272" s="T557">hanah-a</ta>
            <ta e="T559" id="Seg_7273" s="T558">ogo-tu-n</ta>
            <ta e="T560" id="Seg_7274" s="T559">emij-d-iː</ta>
            <ta e="T561" id="Seg_7275" s="T560">olor-or</ta>
            <ta e="T562" id="Seg_7276" s="T561">balɨs-tara</ta>
            <ta e="T563" id="Seg_7277" s="T562">ah-ɨː</ta>
            <ta e="T564" id="Seg_7278" s="T563">olor-or</ta>
            <ta e="T565" id="Seg_7279" s="T564">dʼe</ta>
            <ta e="T566" id="Seg_7280" s="T565">haŋaːs</ta>
            <ta e="T567" id="Seg_7281" s="T566">buruj-gu-n</ta>
            <ta e="T568" id="Seg_7282" s="T567">ah-a-r-dɨ-ŋ</ta>
            <ta e="T569" id="Seg_7283" s="T568">ogo-gu-n</ta>
            <ta e="T570" id="Seg_7284" s="T569">ɨl-batag-ɨ-ŋ</ta>
            <ta e="T571" id="Seg_7285" s="T570">bu͡ol-lar</ta>
            <ta e="T572" id="Seg_7286" s="T571">ölör-ü͡ök</ta>
            <ta e="T573" id="Seg_7287" s="T572">e-ti-m</ta>
            <ta e="T574" id="Seg_7288" s="T573">agɨs</ta>
            <ta e="T575" id="Seg_7289" s="T574">koŋnomu͡oj</ta>
            <ta e="T576" id="Seg_7290" s="T575">ičči-te</ta>
            <ta e="T577" id="Seg_7291" s="T576">kuttan-aːrɨ</ta>
            <ta e="T578" id="Seg_7292" s="T577">kuttan-ar</ta>
            <ta e="T579" id="Seg_7293" s="T578">olor-ol-lor</ta>
            <ta e="T580" id="Seg_7294" s="T579">kahan</ta>
            <ta e="T581" id="Seg_7295" s="T580">ere</ta>
            <ta e="T582" id="Seg_7296" s="T581">üh-üs</ta>
            <ta e="T583" id="Seg_7297" s="T582">kün-ü-ger</ta>
            <ta e="T584" id="Seg_7298" s="T583">dʼe</ta>
            <ta e="T585" id="Seg_7299" s="T584">agɨs</ta>
            <ta e="T586" id="Seg_7300" s="T585">koŋnomu͡oj</ta>
            <ta e="T587" id="Seg_7301" s="T586">ičči-te</ta>
            <ta e="T588" id="Seg_7302" s="T587">dʼoŋ-ŋu-n</ta>
            <ta e="T589" id="Seg_7303" s="T588">mus</ta>
            <ta e="T590" id="Seg_7304" s="T589">munnʼak-t-ɨ͡ak-pɨt</ta>
            <ta e="T591" id="Seg_7305" s="T590">diː-r</ta>
            <ta e="T592" id="Seg_7306" s="T591">Kuŋadʼaj</ta>
            <ta e="T593" id="Seg_7307" s="T592">dʼe</ta>
            <ta e="T594" id="Seg_7308" s="T593">munnʼ-al-lar</ta>
            <ta e="T595" id="Seg_7309" s="T594">hette</ta>
            <ta e="T596" id="Seg_7310" s="T595">u͡on</ta>
            <ta e="T597" id="Seg_7311" s="T596">dʼi͡e</ta>
            <ta e="T598" id="Seg_7312" s="T597">kihi-ti-n</ta>
            <ta e="T599" id="Seg_7313" s="T598">Kuŋadʼaj</ta>
            <ta e="T600" id="Seg_7314" s="T599">et-er</ta>
            <ta e="T601" id="Seg_7315" s="T600">ehigi</ta>
            <ta e="T602" id="Seg_7316" s="T601">ikki</ta>
            <ta e="T603" id="Seg_7317" s="T602">küseːjin</ta>
            <ta e="T604" id="Seg_7318" s="T603">baːr-gɨt</ta>
            <ta e="T605" id="Seg_7319" s="T604">agɨs</ta>
            <ta e="T606" id="Seg_7320" s="T605">koŋnomu͡oj</ta>
            <ta e="T607" id="Seg_7321" s="T606">küseːjin-e</ta>
            <ta e="T608" id="Seg_7322" s="T607">u͡onna</ta>
            <ta e="T609" id="Seg_7323" s="T608">hette</ta>
            <ta e="T610" id="Seg_7324" s="T609">timek</ta>
            <ta e="T611" id="Seg_7325" s="T610">ičči-te</ta>
            <ta e="T612" id="Seg_7326" s="T611">anɨ</ta>
            <ta e="T613" id="Seg_7327" s="T612">küseːjin</ta>
            <ta e="T614" id="Seg_7328" s="T613">bu͡ol-an</ta>
            <ta e="T615" id="Seg_7329" s="T614">büt-tü-güt</ta>
            <ta e="T616" id="Seg_7330" s="T615">küseːjiŋ-ŋit</ta>
            <ta e="T617" id="Seg_7331" s="T616">iti</ta>
            <ta e="T618" id="Seg_7332" s="T617">u͡ol</ta>
            <ta e="T619" id="Seg_7333" s="T618">ogo</ta>
            <ta e="T620" id="Seg_7334" s="T619">ubaj-ɨ-m</ta>
            <ta e="T621" id="Seg_7335" s="T620">ogo-to</ta>
            <ta e="T622" id="Seg_7336" s="T621">komu-n-u-ŋ</ta>
            <ta e="T623" id="Seg_7337" s="T622">barɨ-gɨt</ta>
            <ta e="T624" id="Seg_7338" s="T623">ikki</ta>
            <ta e="T625" id="Seg_7339" s="T624">küŋ-ŋe</ta>
            <ta e="T626" id="Seg_7340" s="T625">üh-üs</ta>
            <ta e="T627" id="Seg_7341" s="T626">küŋ-ŋütü-ger</ta>
            <ta e="T628" id="Seg_7342" s="T627">köh-ü͡ök-küt</ta>
            <ta e="T629" id="Seg_7343" s="T628">bastɨŋ-ŋɨt</ta>
            <ta e="T630" id="Seg_7344" s="T629">kini</ta>
            <ta e="T631" id="Seg_7345" s="T630">bu͡ol-u͡o</ta>
            <ta e="T632" id="Seg_7346" s="T631">atɨn</ta>
            <ta e="T633" id="Seg_7347" s="T632">taba-nɨ</ta>
            <ta e="T634" id="Seg_7348" s="T633">kölün-ü-me</ta>
            <ta e="T635" id="Seg_7349" s="T634">aga-ŋ</ta>
            <ta e="T636" id="Seg_7350" s="T635">taba-tɨ-n</ta>
            <ta e="T637" id="Seg_7351" s="T636">keri͡eh-i-n</ta>
            <ta e="T638" id="Seg_7352" s="T637">kölün-eːr</ta>
            <ta e="T639" id="Seg_7353" s="T638">diː-r</ta>
            <ta e="T640" id="Seg_7354" s="T639">u͡ol-ga</ta>
            <ta e="T641" id="Seg_7355" s="T640">u͡ol</ta>
            <ta e="T642" id="Seg_7356" s="T641">ulaːp-pɨt</ta>
            <ta e="T643" id="Seg_7357" s="T642">bast-a-t-aŋ-ŋɨn</ta>
            <ta e="T644" id="Seg_7358" s="T643">köh-ü͡ök-küt</ta>
            <ta e="T645" id="Seg_7359" s="T644">mas</ta>
            <ta e="T646" id="Seg_7360" s="T645">haga-tɨ-nan</ta>
            <ta e="T647" id="Seg_7361" s="T646">dʼirbiː</ta>
            <ta e="T648" id="Seg_7362" s="T647">baːr</ta>
            <ta e="T649" id="Seg_7363" s="T648">bu͡ol-u͡o</ta>
            <ta e="T650" id="Seg_7364" s="T649">bu</ta>
            <ta e="T651" id="Seg_7365" s="T650">dʼirbiː</ta>
            <ta e="T652" id="Seg_7366" s="T651">mu͡ora</ta>
            <ta e="T653" id="Seg_7367" s="T652">di͡eki</ta>
            <ta e="T654" id="Seg_7368" s="T653">ött-ü-nen</ta>
            <ta e="T655" id="Seg_7369" s="T654">köh-ü͡ö-ŋ</ta>
            <ta e="T656" id="Seg_7370" s="T655">ikki</ta>
            <ta e="T657" id="Seg_7371" s="T656">ɨj-ɨ</ta>
            <ta e="T658" id="Seg_7372" s="T657">bɨha</ta>
            <ta e="T659" id="Seg_7373" s="T658">ele-te</ta>
            <ta e="T660" id="Seg_7374" s="T659">bu͡ollagɨna</ta>
            <ta e="T661" id="Seg_7375" s="T660">dʼirbiː-te</ta>
            <ta e="T662" id="Seg_7376" s="T661">büt-teg-ine</ta>
            <ta e="T663" id="Seg_7377" s="T662">hir</ta>
            <ta e="T664" id="Seg_7378" s="T663">bu͡ol-u͡o</ta>
            <ta e="T665" id="Seg_7379" s="T664">kaːr-ɨ-n</ta>
            <ta e="T666" id="Seg_7380" s="T665">ann-ɨ-ttan</ta>
            <ta e="T667" id="Seg_7381" s="T666">buru͡o-laːk</ta>
            <ta e="T668" id="Seg_7382" s="T667">bu͡ol-u͡o</ta>
            <ta e="T669" id="Seg_7383" s="T668">ol</ta>
            <ta e="T670" id="Seg_7384" s="T669">bihigi</ta>
            <ta e="T671" id="Seg_7385" s="T670">törüt</ta>
            <ta e="T672" id="Seg_7386" s="T671">hir-bit</ta>
            <ta e="T673" id="Seg_7387" s="T672">ulakan</ta>
            <ta e="T674" id="Seg_7388" s="T673">hu͡ol-ga</ta>
            <ta e="T675" id="Seg_7389" s="T674">kiːr-i͡e-ŋ</ta>
            <ta e="T676" id="Seg_7390" s="T675">ajmak-tar-ga-r</ta>
            <ta e="T677" id="Seg_7391" s="T676">tij-i͡e-ŋ</ta>
            <ta e="T678" id="Seg_7392" s="T677">dʼon-nor</ta>
            <ta e="T679" id="Seg_7393" s="T678">taba-gatɨ-n</ta>
            <ta e="T680" id="Seg_7394" s="T679">tüːn-ü-kün-ü</ta>
            <ta e="T681" id="Seg_7395" s="T680">bɨha</ta>
            <ta e="T682" id="Seg_7396" s="T681">kör-ü͡ök-küt</ta>
            <ta e="T683" id="Seg_7397" s="T682">agɨs</ta>
            <ta e="T684" id="Seg_7398" s="T683">koŋnomu͡oj</ta>
            <ta e="T685" id="Seg_7399" s="T684">küseːjin-e</ta>
            <ta e="T686" id="Seg_7400" s="T685">hette</ta>
            <ta e="T687" id="Seg_7401" s="T686">timek</ta>
            <ta e="T688" id="Seg_7402" s="T687">ičči-te</ta>
            <ta e="T689" id="Seg_7403" s="T688">emi͡e</ta>
            <ta e="T690" id="Seg_7404" s="T689">körs-ü͡ök-küt</ta>
            <ta e="T691" id="Seg_7405" s="T690">min</ta>
            <ta e="T692" id="Seg_7406" s="T691">bu͡ollagɨna</ta>
            <ta e="T693" id="Seg_7407" s="T692">bu</ta>
            <ta e="T694" id="Seg_7408" s="T693">üčügej-dik</ta>
            <ta e="T695" id="Seg_7409" s="T694">ajannɨː-r-gɨt</ta>
            <ta e="T696" id="Seg_7410" s="T695">tuhuttan</ta>
            <ta e="T697" id="Seg_7411" s="T696">kuhagan</ta>
            <ta e="T698" id="Seg_7412" s="T697">dʼon-u</ta>
            <ta e="T699" id="Seg_7413" s="T698">čaŋɨt-tar-ɨ</ta>
            <ta e="T700" id="Seg_7414" s="T699">kɨːl-lar-ɨ</ta>
            <ta e="T701" id="Seg_7415" s="T700">hu͡ok</ta>
            <ta e="T702" id="Seg_7416" s="T701">gɨn-a</ta>
            <ta e="T703" id="Seg_7417" s="T702">bar-dɨ-m</ta>
            <ta e="T704" id="Seg_7418" s="T703">ol</ta>
            <ta e="T705" id="Seg_7419" s="T704">hir-ge</ta>
            <ta e="T706" id="Seg_7420" s="T705">tij-er</ta>
            <ta e="T707" id="Seg_7421" s="T706">küŋ-ŋe-r</ta>
            <ta e="T708" id="Seg_7422" s="T707">tij-i͡e-m</ta>
            <ta e="T709" id="Seg_7423" s="T708">ulakan-nɨk</ta>
            <ta e="T710" id="Seg_7424" s="T709">haːraː-tak-pɨna</ta>
            <ta e="T711" id="Seg_7425" s="T710">üh-üs</ta>
            <ta e="T712" id="Seg_7426" s="T711">kün-ü-ger</ta>
            <ta e="T713" id="Seg_7427" s="T712">tij-i͡e-m</ta>
            <ta e="T714" id="Seg_7428" s="T713">diː-r</ta>
            <ta e="T715" id="Seg_7429" s="T714">üh-üs</ta>
            <ta e="T716" id="Seg_7430" s="T715">kün-ü-ger</ta>
            <ta e="T717" id="Seg_7431" s="T716">köh-öl-lör</ta>
            <ta e="T718" id="Seg_7432" s="T717">Kuŋadʼaj</ta>
            <ta e="T719" id="Seg_7433" s="T718">arag-an</ta>
            <ta e="T720" id="Seg_7434" s="T719">bar-ar</ta>
            <ta e="T721" id="Seg_7435" s="T720">hu͡os</ta>
            <ta e="T722" id="Seg_7436" s="T721">hatɨː</ta>
            <ta e="T723" id="Seg_7437" s="T722">baː</ta>
            <ta e="T724" id="Seg_7438" s="T723">ogo</ta>
            <ta e="T725" id="Seg_7439" s="T724">bastɨŋ</ta>
            <ta e="T726" id="Seg_7440" s="T725">bu͡ol-an</ta>
            <ta e="T727" id="Seg_7441" s="T726">köh-ön</ta>
            <ta e="T728" id="Seg_7442" s="T727">is-ti-ler</ta>
            <ta e="T729" id="Seg_7443" s="T728">ket-e-n-en</ta>
            <ta e="T730" id="Seg_7444" s="T729">ajannɨː-l-lar</ta>
            <ta e="T731" id="Seg_7445" s="T730">tojon-u-kotun-u</ta>
            <ta e="T732" id="Seg_7446" s="T731">barɨ-tɨ-n</ta>
            <ta e="T733" id="Seg_7447" s="T732">ket-e-t-er</ta>
            <ta e="T734" id="Seg_7448" s="T733">bu</ta>
            <ta e="T735" id="Seg_7449" s="T734">u͡ol</ta>
            <ta e="T736" id="Seg_7450" s="T735">ɨj-ɨ-n</ta>
            <ta e="T737" id="Seg_7451" s="T736">orto-to</ta>
            <ta e="T738" id="Seg_7452" s="T737">bu͡ol-but</ta>
            <ta e="T739" id="Seg_7453" s="T738">dʼirbiː-ti-n</ta>
            <ta e="T740" id="Seg_7454" s="T739">irdeː-n</ta>
            <ta e="T741" id="Seg_7455" s="T740">is-teg-e</ta>
            <ta e="T742" id="Seg_7456" s="T741">araj</ta>
            <ta e="T743" id="Seg_7457" s="T742">mu͡ora</ta>
            <ta e="T744" id="Seg_7458" s="T743">di͡eki</ta>
            <ta e="T745" id="Seg_7459" s="T744">dʼirbiː</ta>
            <ta e="T746" id="Seg_7460" s="T745">arag-ar</ta>
            <ta e="T747" id="Seg_7461" s="T746">ürd-ü-ger</ta>
            <ta e="T748" id="Seg_7462" s="T747">kihi</ta>
            <ta e="T749" id="Seg_7463" s="T748">hɨldʼ-ar</ta>
            <ta e="T750" id="Seg_7464" s="T749">kaja</ta>
            <ta e="T751" id="Seg_7465" s="T750">di͡ekki</ta>
            <ta e="T752" id="Seg_7466" s="T751">kihi-gin=ij</ta>
            <ta e="T753" id="Seg_7467" s="T752">tu͡ok</ta>
            <ta e="T754" id="Seg_7468" s="T753">kel-i͡e=j</ta>
            <ta e="T755" id="Seg_7469" s="T754">dʼi͡e-ni-u͡ot-u</ta>
            <ta e="T756" id="Seg_7470" s="T755">bil-bep-pin</ta>
            <ta e="T757" id="Seg_7471" s="T756">töröː-büt</ta>
            <ta e="T758" id="Seg_7472" s="T757">ubaj-bɨ-n</ta>
            <ta e="T759" id="Seg_7473" s="T758">hüter-e</ta>
            <ta e="T760" id="Seg_7474" s="T759">hɨldʼ-a-bɨn</ta>
            <ta e="T761" id="Seg_7475" s="T760">kaja-lara=j</ta>
            <ta e="T762" id="Seg_7476" s="T761">ol</ta>
            <ta e="T763" id="Seg_7477" s="T762">agɨs</ta>
            <ta e="T764" id="Seg_7478" s="T763">koŋnomu͡oj</ta>
            <ta e="T765" id="Seg_7479" s="T764">küseːjin-e</ta>
            <ta e="T766" id="Seg_7480" s="T765">heːj</ta>
            <ta e="T767" id="Seg_7481" s="T766">ol</ta>
            <ta e="T768" id="Seg_7482" s="T767">min</ta>
            <ta e="T769" id="Seg_7483" s="T768">kihi-m</ta>
            <ta e="T770" id="Seg_7484" s="T769">ol</ta>
            <ta e="T771" id="Seg_7485" s="T770">delemičeː</ta>
            <ta e="T772" id="Seg_7486" s="T771">üːr-en</ta>
            <ta e="T773" id="Seg_7487" s="T772">ih-er</ta>
            <ta e="T774" id="Seg_7488" s="T773">dʼe</ta>
            <ta e="T775" id="Seg_7489" s="T774">oččogo</ta>
            <ta e="T776" id="Seg_7490" s="T775">mi͡e-ke</ta>
            <ta e="T777" id="Seg_7491" s="T776">kihi</ta>
            <ta e="T778" id="Seg_7492" s="T777">bu͡ol</ta>
            <ta e="T779" id="Seg_7493" s="T778">haŋar-a</ta>
            <ta e="T780" id="Seg_7494" s="T779">tar-pat</ta>
            <ta e="T781" id="Seg_7495" s="T780">i͡ehili</ta>
            <ta e="T782" id="Seg_7496" s="T781">kihi</ta>
            <ta e="T783" id="Seg_7497" s="T782">bu͡ol-u͡o-ŋ</ta>
            <ta e="T784" id="Seg_7498" s="T783">hu͡og-a</ta>
            <ta e="T785" id="Seg_7499" s="T784">bu</ta>
            <ta e="T786" id="Seg_7500" s="T785">üs</ta>
            <ta e="T787" id="Seg_7501" s="T786">halaː-laːk</ta>
            <ta e="T788" id="Seg_7502" s="T787">kürej-i-m</ta>
            <ta e="T789" id="Seg_7503" s="T788">aŋar</ta>
            <ta e="T790" id="Seg_7504" s="T789">halaː-ta</ta>
            <ta e="T791" id="Seg_7505" s="T790">kaŋɨrgah-a</ta>
            <ta e="T792" id="Seg_7506" s="T791">bu͡ol-u͡o-ŋ</ta>
            <ta e="T793" id="Seg_7507" s="T792">ikki-te</ta>
            <ta e="T794" id="Seg_7508" s="T793">bukatɨːr</ta>
            <ta e="T795" id="Seg_7509" s="T794">eːlde-te</ta>
            <ta e="T796" id="Seg_7510" s="T795">kaŋɨrgas-taːk</ta>
            <ta e="T797" id="Seg_7511" s="T796">kihi</ta>
            <ta e="T798" id="Seg_7512" s="T797">höbül-e-n-er</ta>
            <ta e="T799" id="Seg_7513" s="T798">bars-ar</ta>
            <ta e="T800" id="Seg_7514" s="T799">köh-ön</ta>
            <ta e="T801" id="Seg_7515" s="T800">is-ti-ler</ta>
            <ta e="T802" id="Seg_7516" s="T801">ɨj-dara</ta>
            <ta e="T803" id="Seg_7517" s="T802">baran-n-a</ta>
            <ta e="T804" id="Seg_7518" s="T803">nöŋü͡ö</ta>
            <ta e="T805" id="Seg_7519" s="T804">ɨj</ta>
            <ta e="T806" id="Seg_7520" s="T805">orto-to</ta>
            <ta e="T807" id="Seg_7521" s="T806">bu͡ol-l-a</ta>
            <ta e="T808" id="Seg_7522" s="T807">emi͡e</ta>
            <ta e="T809" id="Seg_7523" s="T808">kihi-ni</ta>
            <ta e="T810" id="Seg_7524" s="T809">kör-dü-ler</ta>
            <ta e="T811" id="Seg_7525" s="T810">biːr</ta>
            <ta e="T812" id="Seg_7526" s="T811">dʼirbiː-keːŋ-ŋe</ta>
            <ta e="T813" id="Seg_7527" s="T812">emi͡e</ta>
            <ta e="T814" id="Seg_7528" s="T813">doroːbo-l-o-h-or</ta>
            <ta e="T815" id="Seg_7529" s="T814">kaja</ta>
            <ta e="T816" id="Seg_7530" s="T815">di͡eki</ta>
            <ta e="T817" id="Seg_7531" s="T816">dʼi͡e-leːk-u͡ot-taːk</ta>
            <ta e="T818" id="Seg_7532" s="T817">kihi-gin=ij</ta>
            <ta e="T819" id="Seg_7533" s="T818">dʼi͡e-u͡ot</ta>
            <ta e="T820" id="Seg_7534" s="T819">hu͡ok</ta>
            <ta e="T821" id="Seg_7535" s="T820">hɨldʼ-a-bɨn</ta>
            <ta e="T822" id="Seg_7536" s="T821">agaj</ta>
            <ta e="T823" id="Seg_7537" s="T822">ubaj-bɨ-n</ta>
            <ta e="T824" id="Seg_7538" s="T823">körd-üː-bün</ta>
            <ta e="T825" id="Seg_7539" s="T824">tu͡ok</ta>
            <ta e="T826" id="Seg_7540" s="T825">e-t-e=j</ta>
            <ta e="T827" id="Seg_7541" s="T826">ubaj-ɨ-n</ta>
            <ta e="T828" id="Seg_7542" s="T827">eː</ta>
            <ta e="T829" id="Seg_7543" s="T828">hette</ta>
            <ta e="T830" id="Seg_7544" s="T829">timek</ta>
            <ta e="T831" id="Seg_7545" s="T830">ičči-te</ta>
            <ta e="T832" id="Seg_7546" s="T831">eː</ta>
            <ta e="T833" id="Seg_7547" s="T832">ol</ta>
            <ta e="T834" id="Seg_7548" s="T833">delemičeː</ta>
            <ta e="T835" id="Seg_7549" s="T834">üːr-en</ta>
            <ta e="T836" id="Seg_7550" s="T835">ih-er</ta>
            <ta e="T837" id="Seg_7551" s="T836">ubaj-ɨ-ŋ</ta>
            <ta e="T838" id="Seg_7552" s="T837">kaja</ta>
            <ta e="T839" id="Seg_7553" s="T838">mi͡e-ke</ta>
            <ta e="T840" id="Seg_7554" s="T839">kihi</ta>
            <ta e="T841" id="Seg_7555" s="T840">bu͡ol-u͡o-ŋ</ta>
            <ta e="T842" id="Seg_7556" s="T841">haŋar-bat</ta>
            <ta e="T843" id="Seg_7557" s="T842">bu͡ol-u͡o-ŋ</ta>
            <ta e="T844" id="Seg_7558" s="T843">hu͡og-a</ta>
            <ta e="T845" id="Seg_7559" s="T844">bu</ta>
            <ta e="T846" id="Seg_7560" s="T845">kürej-i-m</ta>
            <ta e="T847" id="Seg_7561" s="T846">halaː-tɨ-gar</ta>
            <ta e="T848" id="Seg_7562" s="T847">kaŋɨrgas</ta>
            <ta e="T849" id="Seg_7563" s="T848">gɨn-ɨ͡a-m</ta>
            <ta e="T850" id="Seg_7564" s="T849">höbül-e-h-er</ta>
            <ta e="T851" id="Seg_7565" s="T850">bili</ta>
            <ta e="T852" id="Seg_7566" s="T851">kihi</ta>
            <ta e="T853" id="Seg_7567" s="T852">ubaj-ɨ-gar</ta>
            <ta e="T854" id="Seg_7568" s="T853">tij-er</ta>
            <ta e="T855" id="Seg_7569" s="T854">togo</ta>
            <ta e="T856" id="Seg_7570" s="T855">ölör-bök-küt</ta>
            <ta e="T857" id="Seg_7571" s="T856">ogo-go</ta>
            <ta e="T858" id="Seg_7572" s="T857">bas</ta>
            <ta e="T859" id="Seg_7573" s="T858">berin-e-git</ta>
            <ta e="T860" id="Seg_7574" s="T859">du͡o</ta>
            <ta e="T861" id="Seg_7575" s="T860">hu͡ok</ta>
            <ta e="T862" id="Seg_7576" s="T861">kebis</ta>
            <ta e="T863" id="Seg_7577" s="T862">bult-u͡ok-ka</ta>
            <ta e="T864" id="Seg_7578" s="T863">naːda</ta>
            <ta e="T865" id="Seg_7579" s="T864">kohut-a</ta>
            <ta e="T866" id="Seg_7580" s="T865">da</ta>
            <ta e="T867" id="Seg_7581" s="T866">bert</ta>
            <ta e="T868" id="Seg_7582" s="T867">če</ta>
            <ta e="T869" id="Seg_7583" s="T868">bagal-ɨ͡ak-pɨt</ta>
            <ta e="T870" id="Seg_7584" s="T869">ki͡ehe</ta>
            <ta e="T871" id="Seg_7585" s="T870">tüh-el-ler</ta>
            <ta e="T872" id="Seg_7586" s="T871">biːr</ta>
            <ta e="T873" id="Seg_7587" s="T872">hir-ge</ta>
            <ta e="T874" id="Seg_7588" s="T873">ahɨː-l-lar</ta>
            <ta e="T875" id="Seg_7589" s="T874">u͡ol</ta>
            <ta e="T876" id="Seg_7590" s="T875">dʼon-u-n</ta>
            <ta e="T877" id="Seg_7591" s="T876">bat-an</ta>
            <ta e="T878" id="Seg_7592" s="T877">tahaːr-ar</ta>
            <ta e="T879" id="Seg_7593" s="T878">ü͡ör</ta>
            <ta e="T880" id="Seg_7594" s="T879">ket-e-t-e</ta>
            <ta e="T881" id="Seg_7595" s="T880">ogo</ta>
            <ta e="T882" id="Seg_7596" s="T881">utuj-an</ta>
            <ta e="T883" id="Seg_7597" s="T882">kaːl-ar</ta>
            <ta e="T884" id="Seg_7598" s="T883">kihi-ler</ta>
            <ta e="T885" id="Seg_7599" s="T884">kalɨj-ar</ta>
            <ta e="T886" id="Seg_7600" s="T885">haŋa-larɨ-n</ta>
            <ta e="T887" id="Seg_7601" s="T886">ist-er</ta>
            <ta e="T888" id="Seg_7602" s="T887">kihi</ta>
            <ta e="T889" id="Seg_7603" s="T888">batɨja-nnan</ta>
            <ta e="T890" id="Seg_7604" s="T889">aːn-ɨ</ta>
            <ta e="T891" id="Seg_7605" s="T890">heget-er</ta>
            <ta e="T892" id="Seg_7606" s="T891">kaja</ta>
            <ta e="T893" id="Seg_7607" s="T892">togo</ta>
            <ta e="T894" id="Seg_7608" s="T893">ket-iː-git</ta>
            <ta e="T895" id="Seg_7609" s="T894">miːgi-n</ta>
            <ta e="T896" id="Seg_7610" s="T895">ü͡ör-ü</ta>
            <ta e="T897" id="Seg_7611" s="T896">keteː-ŋ</ta>
            <ta e="T898" id="Seg_7612" s="T897">dʼon</ta>
            <ta e="T899" id="Seg_7613" s="T898">töttörü</ta>
            <ta e="T900" id="Seg_7614" s="T899">hah-al-lar</ta>
            <ta e="T901" id="Seg_7615" s="T900">harsi͡erdaːŋŋɨ</ta>
            <ta e="T902" id="Seg_7616" s="T901">uː-tu-gar</ta>
            <ta e="T903" id="Seg_7617" s="T902">bagal-ɨ͡ak-pɨt</ta>
            <ta e="T904" id="Seg_7618" s="T903">d-e-h-el-ler</ta>
            <ta e="T905" id="Seg_7619" s="T904">emi͡e</ta>
            <ta e="T906" id="Seg_7620" s="T905">bürüːrdüː-l-ler</ta>
            <ta e="T907" id="Seg_7621" s="T906">emi͡e</ta>
            <ta e="T908" id="Seg_7622" s="T907">tug-u</ta>
            <ta e="T909" id="Seg_7623" s="T908">ket-iː-git</ta>
            <ta e="T910" id="Seg_7624" s="T909">bar-ɨ-ŋ</ta>
            <ta e="T911" id="Seg_7625" s="T910">bat-an</ta>
            <ta e="T912" id="Seg_7626" s="T911">ɨːt-ar</ta>
            <ta e="T913" id="Seg_7627" s="T912">hon-ton</ta>
            <ta e="T914" id="Seg_7628" s="T913">tɨːp-patak-tara</ta>
            <ta e="T915" id="Seg_7629" s="T914">ikki</ta>
            <ta e="T916" id="Seg_7630" s="T915">ɨj-dara</ta>
            <ta e="T917" id="Seg_7631" s="T916">bɨs-t-ar-ɨ-gar</ta>
            <ta e="T918" id="Seg_7632" s="T917">dʼirbiː-lere</ta>
            <ta e="T919" id="Seg_7633" s="T918">ele-te</ta>
            <ta e="T920" id="Seg_7634" s="T919">bu͡ol-ar</ta>
            <ta e="T921" id="Seg_7635" s="T920">kör-büt-e</ta>
            <ta e="T922" id="Seg_7636" s="T921">hir</ta>
            <ta e="T923" id="Seg_7637" s="T922">bu͡ol-but</ta>
            <ta e="T924" id="Seg_7638" s="T923">kaːr</ta>
            <ta e="T925" id="Seg_7639" s="T924">ann-ɨ-ttan</ta>
            <ta e="T926" id="Seg_7640" s="T925">buru͡o-kaː-kɨn-a</ta>
            <ta e="T927" id="Seg_7641" s="T926">mu͡ora</ta>
            <ta e="T928" id="Seg_7642" s="T927">di͡ekki-tten</ta>
            <ta e="T929" id="Seg_7643" s="T928">kütür</ta>
            <ta e="T930" id="Seg_7644" s="T929">bagajɨ</ta>
            <ta e="T931" id="Seg_7645" s="T930">orok</ta>
            <ta e="T932" id="Seg_7646" s="T931">kel-er</ta>
            <ta e="T933" id="Seg_7647" s="T932">hir</ta>
            <ta e="T934" id="Seg_7648" s="T933">ih-i-n</ta>
            <ta e="T935" id="Seg_7649" s="T934">di͡eki</ta>
            <ta e="T936" id="Seg_7650" s="T935">bar-ar</ta>
            <ta e="T937" id="Seg_7651" s="T936">orok</ta>
            <ta e="T938" id="Seg_7652" s="T937">bar-an</ta>
            <ta e="T939" id="Seg_7653" s="T938">ih-el-ler</ta>
            <ta e="T940" id="Seg_7654" s="T939">pireːme</ta>
            <ta e="T941" id="Seg_7655" s="T940">noru͡ot-tan</ta>
            <ta e="T942" id="Seg_7656" s="T941">noru͡ot</ta>
            <ta e="T943" id="Seg_7657" s="T942">dʼi͡e-tten</ta>
            <ta e="T944" id="Seg_7658" s="T943">dʼi͡e</ta>
            <ta e="T945" id="Seg_7659" s="T944">hir-ge</ta>
            <ta e="T946" id="Seg_7660" s="T945">kel-el-ler</ta>
            <ta e="T947" id="Seg_7661" s="T946">biːr</ta>
            <ta e="T948" id="Seg_7662" s="T947">dʼi͡e</ta>
            <ta e="T949" id="Seg_7663" s="T948">tah-ɨ-gar</ta>
            <ta e="T950" id="Seg_7664" s="T949">toktuː-r</ta>
            <ta e="T951" id="Seg_7665" s="T950">biːr</ta>
            <ta e="T952" id="Seg_7666" s="T951">kɨrdʼagas</ta>
            <ta e="T953" id="Seg_7667" s="T952">hogus</ta>
            <ta e="T954" id="Seg_7668" s="T953">dʼaktar</ta>
            <ta e="T955" id="Seg_7669" s="T954">taks-ar</ta>
            <ta e="T956" id="Seg_7670" s="T955">baj</ta>
            <ta e="T957" id="Seg_7671" s="T956">taba-ta</ta>
            <ta e="T958" id="Seg_7672" s="T957">oduː</ta>
            <ta e="T959" id="Seg_7673" s="T958">taba</ta>
            <ta e="T960" id="Seg_7674" s="T959">bihigi</ta>
            <ta e="T961" id="Seg_7675" s="T960">törüt</ta>
            <ta e="T962" id="Seg_7676" s="T961">taba-bɨt</ta>
            <ta e="T963" id="Seg_7677" s="T962">kurdug=uj</ta>
            <ta e="T964" id="Seg_7678" s="T963">bɨlɨr</ta>
            <ta e="T965" id="Seg_7679" s="T964">ikki</ta>
            <ta e="T966" id="Seg_7680" s="T965">kihi</ta>
            <ta e="T967" id="Seg_7681" s="T966">bar-bɨt-a</ta>
            <ta e="T968" id="Seg_7682" s="T967">ol</ta>
            <ta e="T969" id="Seg_7683" s="T968">törüt-tere</ta>
            <ta e="T970" id="Seg_7684" s="T969">bu͡olu͡o</ta>
            <ta e="T971" id="Seg_7685" s="T970">üs</ta>
            <ta e="T972" id="Seg_7686" s="T971">dʼi͡e-tten</ta>
            <ta e="T973" id="Seg_7687" s="T972">üs</ta>
            <ta e="T974" id="Seg_7688" s="T973">dʼaktar</ta>
            <ta e="T975" id="Seg_7689" s="T974">taks-an</ta>
            <ta e="T976" id="Seg_7690" s="T975">emi͡e</ta>
            <ta e="T977" id="Seg_7691" s="T976">taːj-bɨt-tar</ta>
            <ta e="T978" id="Seg_7692" s="T977">törüt-teri-n</ta>
            <ta e="T979" id="Seg_7693" s="T978">kepset-en</ta>
            <ta e="T980" id="Seg_7694" s="T979">dʼe</ta>
            <ta e="T981" id="Seg_7695" s="T980">bil-s-el-ler</ta>
            <ta e="T982" id="Seg_7696" s="T981">uruː-larɨ-n</ta>
            <ta e="T983" id="Seg_7697" s="T982">bul-s-al-lar</ta>
            <ta e="T984" id="Seg_7698" s="T983">Kuŋadʼaj-ɨ</ta>
            <ta e="T985" id="Seg_7699" s="T984">küːt-el-ler</ta>
            <ta e="T986" id="Seg_7700" s="T985">üh-üs</ta>
            <ta e="T987" id="Seg_7701" s="T986">kün-ü-n</ta>
            <ta e="T988" id="Seg_7702" s="T987">orto-to</ta>
            <ta e="T989" id="Seg_7703" s="T988">Kuŋadʼaj</ta>
            <ta e="T990" id="Seg_7704" s="T989">kel-er</ta>
            <ta e="T991" id="Seg_7705" s="T990">et</ta>
            <ta e="T992" id="Seg_7706" s="T991">aːt-a</ta>
            <ta e="T993" id="Seg_7707" s="T992">bütün-e</ta>
            <ta e="T994" id="Seg_7708" s="T993">hu͡ok</ta>
            <ta e="T995" id="Seg_7709" s="T994">tiriː-ti-n</ta>
            <ta e="T996" id="Seg_7710" s="T995">aŋar-a</ta>
            <ta e="T997" id="Seg_7711" s="T996">kaːl-bɨt</ta>
            <ta e="T998" id="Seg_7712" s="T997">ogo-m</ta>
            <ta e="T999" id="Seg_7713" s="T998">kel-bik-kin</ta>
            <ta e="T1000" id="Seg_7714" s="T999">dʼe</ta>
            <ta e="T1001" id="Seg_7715" s="T1000">üčügej</ta>
            <ta e="T1002" id="Seg_7716" s="T1001">kuhagan</ta>
            <ta e="T1003" id="Seg_7717" s="T1002">dʼon-u</ta>
            <ta e="T1004" id="Seg_7718" s="T1003">baraː-n</ta>
            <ta e="T1005" id="Seg_7719" s="T1004">dojdu</ta>
            <ta e="T1006" id="Seg_7720" s="T1005">buld-u-n</ta>
            <ta e="T1007" id="Seg_7721" s="T1006">arɨčča</ta>
            <ta e="T1008" id="Seg_7722" s="T1007">kɨ͡aj-dɨ-m</ta>
            <ta e="T1009" id="Seg_7723" s="T1008">tɨːn-ɨ-m</ta>
            <ta e="T1010" id="Seg_7724" s="T1009">ere</ta>
            <ta e="T1011" id="Seg_7725" s="T1010">kel-li-m</ta>
            <ta e="T1012" id="Seg_7726" s="T1011">gojobuːn</ta>
            <ta e="T1013" id="Seg_7727" s="T1012">oŋor-on-nor</ta>
            <ta e="T1014" id="Seg_7728" s="T1013">dʼe</ta>
            <ta e="T1015" id="Seg_7729" s="T1014">olor</ta>
            <ta e="T1016" id="Seg_7730" s="T1015">tu͡ok-tan</ta>
            <ta e="T1017" id="Seg_7731" s="T1016">da</ta>
            <ta e="T1018" id="Seg_7732" s="T1017">kuttan-ɨ-ma</ta>
            <ta e="T1019" id="Seg_7733" s="T1018">Kuŋadʼaj</ta>
            <ta e="T1020" id="Seg_7734" s="T1019">dʼe</ta>
            <ta e="T1021" id="Seg_7735" s="T1020">kojut</ta>
            <ta e="T1022" id="Seg_7736" s="T1021">till-er</ta>
            <ta e="T1023" id="Seg_7737" s="T1022">u͡ol</ta>
            <ta e="T1024" id="Seg_7738" s="T1023">küseːjin</ta>
            <ta e="T1025" id="Seg_7739" s="T1024">bu͡ol-an</ta>
            <ta e="T1026" id="Seg_7740" s="T1025">olor-but-a</ta>
            <ta e="T1027" id="Seg_7741" s="T1026">bu</ta>
            <ta e="T1028" id="Seg_7742" s="T1027">omuk-ka</ta>
            <ta e="T1029" id="Seg_7743" s="T1028">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_7744" s="T0">üs</ta>
            <ta e="T2" id="Seg_7745" s="T1">inibiː-LAr</ta>
            <ta e="T3" id="Seg_7746" s="T2">olor-BIT-LAr</ta>
            <ta e="T4" id="Seg_7747" s="T3">biːr-LArA</ta>
            <ta e="T5" id="Seg_7748" s="T4">kɨra</ta>
            <ta e="T6" id="Seg_7749" s="T5">küččükkeːn</ta>
            <ta e="T7" id="Seg_7750" s="T6">kihi</ta>
            <ta e="T8" id="Seg_7751" s="T7">orto-LArA</ta>
            <ta e="T9" id="Seg_7752" s="T8">kaːm-BAT</ta>
            <ta e="T10" id="Seg_7753" s="T9">tobuk-tI-nAn</ta>
            <ta e="T11" id="Seg_7754" s="T10">agaj</ta>
            <ta e="T12" id="Seg_7755" s="T11">hɨːl-Ar</ta>
            <ta e="T13" id="Seg_7756" s="T12">Kuŋadʼaj</ta>
            <ta e="T14" id="Seg_7757" s="T13">di͡e-An</ta>
            <ta e="T15" id="Seg_7758" s="T14">ubaj-LArA</ta>
            <ta e="T16" id="Seg_7759" s="T15">aːt-tA</ta>
            <ta e="T17" id="Seg_7760" s="T16">hu͡ok</ta>
            <ta e="T18" id="Seg_7761" s="T17">bulčut</ta>
            <ta e="T19" id="Seg_7762" s="T18">dʼaktar-LAːK</ta>
            <ta e="T20" id="Seg_7763" s="T19">biːr</ta>
            <ta e="T21" id="Seg_7764" s="T20">hahɨl</ta>
            <ta e="T22" id="Seg_7765" s="T21">ogo-LAːK</ta>
            <ta e="T23" id="Seg_7766" s="T22">taba-LArA</ta>
            <ta e="T24" id="Seg_7767" s="T23">[C^1][V^1][C^2]-čogotok</ta>
            <ta e="T25" id="Seg_7768" s="T24">at</ta>
            <ta e="T26" id="Seg_7769" s="T25">kördük</ta>
            <ta e="T27" id="Seg_7770" s="T26">üje-GA</ta>
            <ta e="T28" id="Seg_7771" s="T27">kölün-BAT-LAr</ta>
            <ta e="T29" id="Seg_7772" s="T28">kös-TAK-TArInA</ta>
            <ta e="T30" id="Seg_7773" s="T29">ere</ta>
            <ta e="T31" id="Seg_7774" s="T30">dʼi͡e-LArI-n</ta>
            <ta e="T32" id="Seg_7775" s="T31">tart-TAr-Ar-LAr</ta>
            <ta e="T33" id="Seg_7776" s="T32">ubaj-LArA</ta>
            <ta e="T34" id="Seg_7777" s="T33">aːt-LAːK</ta>
            <ta e="T35" id="Seg_7778" s="T34">bulčut</ta>
            <ta e="T36" id="Seg_7779" s="T35">kɨːl-nI</ta>
            <ta e="T37" id="Seg_7780" s="T36">bultaː-Ar</ta>
            <ta e="T38" id="Seg_7781" s="T37">balɨk-nI</ta>
            <ta e="T39" id="Seg_7782" s="T38">bu͡ollun</ta>
            <ta e="T40" id="Seg_7783" s="T39">dʼɨl</ta>
            <ta e="T41" id="Seg_7784" s="T40">haːs-TIj-Ar-tI-n</ta>
            <ta e="T42" id="Seg_7785" s="T41">hagana</ta>
            <ta e="T43" id="Seg_7786" s="T42">ubaj-LArA</ta>
            <ta e="T44" id="Seg_7787" s="T43">ɨ͡arɨj-Ar</ta>
            <ta e="T45" id="Seg_7788" s="T44">bergeː-BIT</ta>
            <ta e="T46" id="Seg_7789" s="T45">üs-Is</ta>
            <ta e="T47" id="Seg_7790" s="T46">kün-tI-GAr</ta>
            <ta e="T48" id="Seg_7791" s="T47">bu͡opsa</ta>
            <ta e="T49" id="Seg_7792" s="T48">möltöː-BIT</ta>
            <ta e="T50" id="Seg_7793" s="T49">keri͡es</ta>
            <ta e="T51" id="Seg_7794" s="T50">et-BIT</ta>
            <ta e="T52" id="Seg_7795" s="T51">dʼaktar-tI-GAr</ta>
            <ta e="T53" id="Seg_7796" s="T52">dʼe</ta>
            <ta e="T54" id="Seg_7797" s="T53">min</ta>
            <ta e="T55" id="Seg_7798" s="T54">öl-TI-m</ta>
            <ta e="T56" id="Seg_7799" s="T55">kim-I-ŋ</ta>
            <ta e="T57" id="Seg_7800" s="T56">karaj-IAK-tA=Ij</ta>
            <ta e="T58" id="Seg_7801" s="T57">bultaː-BIT</ta>
            <ta e="T59" id="Seg_7802" s="T58">bult-BI-n</ta>
            <ta e="T60" id="Seg_7803" s="T59">hajɨn-nI</ta>
            <ta e="T61" id="Seg_7804" s="T60">bɨha</ta>
            <ta e="T62" id="Seg_7805" s="T61">ahaː-IAK-GIt</ta>
            <ta e="T63" id="Seg_7806" s="T62">as-GI-n</ta>
            <ta e="T64" id="Seg_7807" s="T63">baraː-TAK-GInA</ta>
            <ta e="T65" id="Seg_7808" s="T64">ehiːl</ta>
            <ta e="T66" id="Seg_7809" s="T65">tiriː-tI-n</ta>
            <ta e="T67" id="Seg_7810" s="T66">tɨs-tI-n</ta>
            <ta e="T68" id="Seg_7811" s="T67">harɨː-LAː-An</ta>
            <ta e="T69" id="Seg_7812" s="T68">min-LAː-An</ta>
            <ta e="T70" id="Seg_7813" s="T69">is-Aːr</ta>
            <ta e="T71" id="Seg_7814" s="T70">iti</ta>
            <ta e="T72" id="Seg_7815" s="T71">dʼon-I-ŋ</ta>
            <ta e="T73" id="Seg_7816" s="T72">tuha-tA</ta>
            <ta e="T74" id="Seg_7817" s="T73">hu͡ok</ta>
            <ta e="T75" id="Seg_7818" s="T74">dʼon</ta>
            <ta e="T76" id="Seg_7819" s="T75">öl-BIT-GI-nAn</ta>
            <ta e="T77" id="Seg_7820" s="T76">öl-A-GIn</ta>
            <ta e="T78" id="Seg_7821" s="T77">ogo-m</ta>
            <ta e="T79" id="Seg_7822" s="T78">bagar</ta>
            <ta e="T80" id="Seg_7823" s="T79">kihi</ta>
            <ta e="T81" id="Seg_7824" s="T80">bu͡ol-IAK.[tA]</ta>
            <ta e="T82" id="Seg_7825" s="T81">öl-BA-TAK-InA</ta>
            <ta e="T83" id="Seg_7826" s="T82">gini</ta>
            <ta e="T84" id="Seg_7827" s="T83">iːt-IAK.[tA]</ta>
            <ta e="T85" id="Seg_7828" s="T84">ele-tA</ta>
            <ta e="T86" id="Seg_7829" s="T85">öl-An</ta>
            <ta e="T87" id="Seg_7830" s="T86">kaːl-Ar</ta>
            <ta e="T88" id="Seg_7831" s="T87">ɨtaː-A-n-A-ɨtaː-A-n-A</ta>
            <ta e="T89" id="Seg_7832" s="T88">dʼaktar</ta>
            <ta e="T90" id="Seg_7833" s="T89">uːr-An</ta>
            <ta e="T91" id="Seg_7834" s="T90">keːs-Ar</ta>
            <ta e="T92" id="Seg_7835" s="T91">bu</ta>
            <ta e="T93" id="Seg_7836" s="T92">hajɨn-tA</ta>
            <ta e="T94" id="Seg_7837" s="T93">bu͡ol-Ar</ta>
            <ta e="T95" id="Seg_7838" s="T94">as-LArI-n</ta>
            <ta e="T96" id="Seg_7839" s="T95">kat-A-r-I-n-Ar-LAr</ta>
            <ta e="T97" id="Seg_7840" s="T96">hajɨn-LArI-n</ta>
            <ta e="T98" id="Seg_7841" s="T97">bɨs-Ar-LAr</ta>
            <ta e="T99" id="Seg_7842" s="T98">kühün-LArI-GAr</ta>
            <ta e="T100" id="Seg_7843" s="T99">tɨs-LArI-n</ta>
            <ta e="T101" id="Seg_7844" s="T100">tiriː-LArI-n</ta>
            <ta e="T102" id="Seg_7845" s="T101">hi͡e-Ar-LAr</ta>
            <ta e="T103" id="Seg_7846" s="T102">barɨ-tA</ta>
            <ta e="T104" id="Seg_7847" s="T103">baran-Ar</ta>
            <ta e="T105" id="Seg_7848" s="T104">kɨhɨn</ta>
            <ta e="T106" id="Seg_7849" s="T105">bu͡ol-Ar</ta>
            <ta e="T107" id="Seg_7850" s="T106">hi͡e-IAK</ta>
            <ta e="T108" id="Seg_7851" s="T107">hir-LArA</ta>
            <ta e="T109" id="Seg_7852" s="T108">hu͡ok</ta>
            <ta e="T110" id="Seg_7853" s="T109">tɨ͡al-tA</ta>
            <ta e="T111" id="Seg_7854" s="T110">hu͡ok</ta>
            <ta e="T112" id="Seg_7855" s="T111">kün</ta>
            <ta e="T113" id="Seg_7856" s="T112">agaj</ta>
            <ta e="T114" id="Seg_7857" s="T113">hɨrga-LAːK</ta>
            <ta e="T115" id="Seg_7858" s="T114">tɨ͡as-tI-n</ta>
            <ta e="T116" id="Seg_7859" s="T115">ihit-Ar</ta>
            <ta e="T117" id="Seg_7860" s="T116">dʼaktar</ta>
            <ta e="T118" id="Seg_7861" s="T117">tagɨs-A</ta>
            <ta e="T119" id="Seg_7862" s="T118">köt-Ar</ta>
            <ta e="T120" id="Seg_7863" s="T119">haŋa</ta>
            <ta e="T121" id="Seg_7864" s="T120">ere</ta>
            <ta e="T122" id="Seg_7865" s="T121">ihilin-Ar</ta>
            <ta e="T123" id="Seg_7866" s="T122">hir-tI-GAr</ta>
            <ta e="T124" id="Seg_7867" s="T123">toktoː-BIT</ta>
            <ta e="T125" id="Seg_7868" s="T124">agaj</ta>
            <ta e="T126" id="Seg_7869" s="T125">dʼaktar</ta>
            <ta e="T127" id="Seg_7870" s="T126">aga-tA</ta>
            <ta e="T128" id="Seg_7871" s="T127">agɨs</ta>
            <ta e="T129" id="Seg_7872" s="T128">koŋnomu͡oj</ta>
            <ta e="T130" id="Seg_7873" s="T129">ičči-tA</ta>
            <ta e="T131" id="Seg_7874" s="T130">küheːjin-tA</ta>
            <ta e="T132" id="Seg_7875" s="T131">kineːs</ta>
            <ta e="T133" id="Seg_7876" s="T132">e-BIT</ta>
            <ta e="T134" id="Seg_7877" s="T133">kajaː</ta>
            <ta e="T135" id="Seg_7878" s="T134">kajdak</ta>
            <ta e="T136" id="Seg_7879" s="T135">olor-A-GIt</ta>
            <ta e="T137" id="Seg_7880" s="T136">er-I-m</ta>
            <ta e="T138" id="Seg_7881" s="T137">öl-BIT-tA</ta>
            <ta e="T139" id="Seg_7882" s="T138">bɨlɨrɨːn</ta>
            <ta e="T140" id="Seg_7883" s="T139">kajaː</ta>
            <ta e="T141" id="Seg_7884" s="T140">slaːbu͡ok</ta>
            <ta e="T142" id="Seg_7885" s="T141">olus</ta>
            <ta e="T143" id="Seg_7886" s="T142">ü͡ör-TI-m</ta>
            <ta e="T144" id="Seg_7887" s="T143">taŋɨn</ta>
            <ta e="T145" id="Seg_7888" s="T144">bar-IAk</ta>
            <ta e="T146" id="Seg_7889" s="T145">min</ta>
            <ta e="T147" id="Seg_7890" s="T146">dʼi͡e-BA-r</ta>
            <ta e="T148" id="Seg_7891" s="T147">kaja</ta>
            <ta e="T149" id="Seg_7892" s="T148">ogo-BI-n</ta>
            <ta e="T150" id="Seg_7893" s="T149">dʼon-BI-n</ta>
            <ta e="T151" id="Seg_7894" s="T150">ogut-An</ta>
            <ta e="T152" id="Seg_7895" s="T151">öl-TInnAr</ta>
            <ta e="T153" id="Seg_7896" s="T152">bar-IAk</ta>
            <ta e="T154" id="Seg_7897" s="T153">dʼaktar</ta>
            <ta e="T155" id="Seg_7898" s="T154">taŋɨn-An</ta>
            <ta e="T156" id="Seg_7899" s="T155">tij-An</ta>
            <ta e="T157" id="Seg_7900" s="T156">meŋehin-Ar</ta>
            <ta e="T158" id="Seg_7901" s="T157">da</ta>
            <ta e="T159" id="Seg_7902" s="T158">kötüt-An</ta>
            <ta e="T160" id="Seg_7903" s="T159">kaːl-Ar-LAr</ta>
            <ta e="T161" id="Seg_7904" s="T160">Kuŋadʼaj-LAːK</ta>
            <ta e="T162" id="Seg_7905" s="T161">kam</ta>
            <ta e="T163" id="Seg_7906" s="T162">aččɨk</ta>
            <ta e="T164" id="Seg_7907" s="T163">hɨt-TAK-LArA</ta>
            <ta e="T165" id="Seg_7908" s="T164">karaŋa</ta>
            <ta e="T166" id="Seg_7909" s="T165">aːs-An</ta>
            <ta e="T167" id="Seg_7910" s="T166">hɨrdaː-Ar-GA</ta>
            <ta e="T168" id="Seg_7911" s="T167">bar-Ar</ta>
            <ta e="T169" id="Seg_7912" s="T168">Kuŋadʼaj</ta>
            <ta e="T170" id="Seg_7913" s="T169">balɨs-I-tI-GAr</ta>
            <ta e="T171" id="Seg_7914" s="T170">et-Ar</ta>
            <ta e="T172" id="Seg_7915" s="T171">taba-GI-n</ta>
            <ta e="T173" id="Seg_7916" s="T172">kölüj</ta>
            <ta e="T174" id="Seg_7917" s="T173">hɨt-BIT-I-nAn</ta>
            <ta e="T175" id="Seg_7918" s="T174">öl-IAK-BIt</ta>
            <ta e="T176" id="Seg_7919" s="T175">agɨs</ta>
            <ta e="T177" id="Seg_7920" s="T176">koŋnomu͡oj-nI</ta>
            <ta e="T178" id="Seg_7921" s="T177">bat-IAk</ta>
            <ta e="T179" id="Seg_7922" s="T178">taba-LArI-n</ta>
            <ta e="T180" id="Seg_7923" s="T179">kölüj-An</ta>
            <ta e="T181" id="Seg_7924" s="T180">dʼi͡e-LArI-n</ta>
            <ta e="T182" id="Seg_7925" s="T181">kötür-An</ta>
            <ta e="T183" id="Seg_7926" s="T182">meŋehin-An</ta>
            <ta e="T184" id="Seg_7927" s="T183">etteː-A-t-An</ta>
            <ta e="T185" id="Seg_7928" s="T184">is-TAK-LArA</ta>
            <ta e="T186" id="Seg_7929" s="T185">kas</ta>
            <ta e="T187" id="Seg_7930" s="T186">u͡on-TA</ta>
            <ta e="T188" id="Seg_7931" s="T187">kös-BIT-LAr</ta>
            <ta e="T189" id="Seg_7932" s="T188">biːrde</ta>
            <ta e="T190" id="Seg_7933" s="T189">kütürü͡ök</ta>
            <ta e="T191" id="Seg_7934" s="T190">his-GA</ta>
            <ta e="T192" id="Seg_7935" s="T191">ɨtɨn-I-BIT-LAr</ta>
            <ta e="T193" id="Seg_7936" s="T192">körüleː-BIT-LArA</ta>
            <ta e="T194" id="Seg_7937" s="T193">kütür-LAːK</ta>
            <ta e="T195" id="Seg_7938" s="T194">teːn</ta>
            <ta e="T196" id="Seg_7939" s="T195">teːn</ta>
            <ta e="T197" id="Seg_7940" s="T196">orto-tI-GAr</ta>
            <ta e="T198" id="Seg_7941" s="T197">ü͡ör</ta>
            <ta e="T199" id="Seg_7942" s="T198">bögö</ta>
            <ta e="T200" id="Seg_7943" s="T199">teːn</ta>
            <ta e="T201" id="Seg_7944" s="T200">onu͡or</ta>
            <ta e="T202" id="Seg_7945" s="T201">hette</ta>
            <ta e="T203" id="Seg_7946" s="T202">u͡on</ta>
            <ta e="T204" id="Seg_7947" s="T203">dʼi͡e</ta>
            <ta e="T205" id="Seg_7948" s="T204">kihi</ta>
            <ta e="T206" id="Seg_7949" s="T205">köhün-Ar</ta>
            <ta e="T207" id="Seg_7950" s="T206">bar-BIT-LAr</ta>
            <ta e="T208" id="Seg_7951" s="T207">ü͡ör</ta>
            <ta e="T209" id="Seg_7952" s="T208">is-tI-nAn</ta>
            <ta e="T210" id="Seg_7953" s="T209">ü͡ör</ta>
            <ta e="T211" id="Seg_7954" s="T210">aŋar</ta>
            <ta e="T212" id="Seg_7955" s="T211">öttü-tI-GAr</ta>
            <ta e="T213" id="Seg_7956" s="T212">tur</ta>
            <ta e="T214" id="Seg_7957" s="T213">di͡e-BIT</ta>
            <ta e="T215" id="Seg_7958" s="T214">Kuŋadʼaj</ta>
            <ta e="T216" id="Seg_7959" s="T215">iti</ta>
            <ta e="T217" id="Seg_7960" s="T216">dʼi͡e-LAr-ttAn</ta>
            <ta e="T218" id="Seg_7961" s="T217">biːr</ta>
            <ta e="T219" id="Seg_7962" s="T218">orduk-LArI-GAr</ta>
            <ta e="T220" id="Seg_7963" s="T219">toktoː-Aːr</ta>
            <ta e="T221" id="Seg_7964" s="T220">nipte-LArI-n</ta>
            <ta e="T222" id="Seg_7965" s="T221">betereː</ta>
            <ta e="T223" id="Seg_7966" s="T222">öttü-kAːN-tI-GAr</ta>
            <ta e="T224" id="Seg_7967" s="T223">kör-BIT-tA</ta>
            <ta e="T225" id="Seg_7968" s="T224">biːr</ta>
            <ta e="T226" id="Seg_7969" s="T225">dʼi͡e</ta>
            <ta e="T227" id="Seg_7970" s="T226">barɨ-LArI-ttAn</ta>
            <ta e="T228" id="Seg_7971" s="T227">ulakan</ta>
            <ta e="T229" id="Seg_7972" s="T228">aːn</ta>
            <ta e="T230" id="Seg_7973" s="T229">tus-tI-GAr</ta>
            <ta e="T231" id="Seg_7974" s="T230">tij-BIT-LAr</ta>
            <ta e="T232" id="Seg_7975" s="T231">da</ta>
            <ta e="T233" id="Seg_7976" s="T232">taba-LArI-n</ta>
            <ta e="T234" id="Seg_7977" s="T233">ɨːt-An</ta>
            <ta e="T235" id="Seg_7978" s="T234">keːs-Ar-LAr</ta>
            <ta e="T236" id="Seg_7979" s="T235">dʼi͡e</ta>
            <ta e="T237" id="Seg_7980" s="T236">tutun-An</ta>
            <ta e="T238" id="Seg_7981" s="T237">keːs-BIT-LAr</ta>
            <ta e="T239" id="Seg_7982" s="T238">otut-I-BIT-LAr</ta>
            <ta e="T240" id="Seg_7983" s="T239">balɨs-I-tI-n</ta>
            <ta e="T241" id="Seg_7984" s="T240">bar</ta>
            <ta e="T242" id="Seg_7985" s="T241">haŋas-GA-r</ta>
            <ta e="T243" id="Seg_7986" s="T242">ogo-tI-n</ta>
            <ta e="T244" id="Seg_7987" s="T243">em-TAr-A</ta>
            <ta e="T245" id="Seg_7988" s="T244">kel-TIn</ta>
            <ta e="T246" id="Seg_7989" s="T245">agɨs</ta>
            <ta e="T247" id="Seg_7990" s="T246">koŋnomu͡oj-GA</ta>
            <ta e="T248" id="Seg_7991" s="T247">et</ta>
            <ta e="T249" id="Seg_7992" s="T248">öl-TI-BIt</ta>
            <ta e="T250" id="Seg_7993" s="T249">biːr</ta>
            <ta e="T251" id="Seg_7994" s="T250">eme</ta>
            <ta e="T252" id="Seg_7995" s="T251">hürek</ta>
            <ta e="T253" id="Seg_7996" s="T252">aŋar-kAːN-tI-n</ta>
            <ta e="T254" id="Seg_7997" s="T253">kɨtta</ta>
            <ta e="T255" id="Seg_7998" s="T254">bɨ͡ar</ta>
            <ta e="T256" id="Seg_7999" s="T255">tulaːjak-tI-n</ta>
            <ta e="T257" id="Seg_8000" s="T256">beris-TIn</ta>
            <ta e="T258" id="Seg_8001" s="T257">di͡e-Ar</ta>
            <ta e="T259" id="Seg_8002" s="T258">u͡ol</ta>
            <ta e="T260" id="Seg_8003" s="T259">hüːr-An</ta>
            <ta e="T261" id="Seg_8004" s="T260">kiːr-An</ta>
            <ta e="T262" id="Seg_8005" s="T261">et-Ar</ta>
            <ta e="T263" id="Seg_8006" s="T262">kör</ta>
            <ta e="T264" id="Seg_8007" s="T263">ere</ta>
            <ta e="T265" id="Seg_8008" s="T264">em-TAr-Ar</ta>
            <ta e="T266" id="Seg_8009" s="T265">hokuču͡ok</ta>
            <ta e="T267" id="Seg_8010" s="T266">ogut-An</ta>
            <ta e="T268" id="Seg_8011" s="T267">öl-TIn</ta>
            <ta e="T269" id="Seg_8012" s="T268">di͡e-Ar</ta>
            <ta e="T270" id="Seg_8013" s="T269">dʼaktar</ta>
            <ta e="T271" id="Seg_8014" s="T270">kaja</ta>
            <ta e="T272" id="Seg_8015" s="T271">ehe</ta>
            <ta e="T273" id="Seg_8016" s="T272">ildʼit-LAː-TI-tA</ta>
            <ta e="T274" id="Seg_8017" s="T273">ubaj-I-m</ta>
            <ta e="T275" id="Seg_8018" s="T274">öl-TI-BIt</ta>
            <ta e="T276" id="Seg_8019" s="T275">biːr</ta>
            <ta e="T277" id="Seg_8020" s="T276">eme</ta>
            <ta e="T278" id="Seg_8021" s="T277">hürek</ta>
            <ta e="T279" id="Seg_8022" s="T278">aŋar-kAːN-tI-n</ta>
            <ta e="T280" id="Seg_8023" s="T279">kɨtta</ta>
            <ta e="T281" id="Seg_8024" s="T280">bɨ͡ar</ta>
            <ta e="T282" id="Seg_8025" s="T281">tulaːjak-tI-n</ta>
            <ta e="T283" id="Seg_8026" s="T282">beris-TIn</ta>
            <ta e="T284" id="Seg_8027" s="T283">kaːk</ta>
            <ta e="T285" id="Seg_8028" s="T284">ogut-An</ta>
            <ta e="T286" id="Seg_8029" s="T285">öl-I-ŋ</ta>
            <ta e="T287" id="Seg_8030" s="T286">ɨːt-BAT-BIn</ta>
            <ta e="T288" id="Seg_8031" s="T287">di͡e-Ar</ta>
            <ta e="T289" id="Seg_8032" s="T288">agɨs</ta>
            <ta e="T290" id="Seg_8033" s="T289">koŋnomu͡oj</ta>
            <ta e="T291" id="Seg_8034" s="T290">ičči-tA</ta>
            <ta e="T292" id="Seg_8035" s="T291">utuj-AːktAː-Ar-LAr</ta>
            <ta e="T293" id="Seg_8036" s="T292">tu͡ok-nI</ta>
            <ta e="T294" id="Seg_8037" s="T293">hi͡e-IAK-LArA=Ij</ta>
            <ta e="T295" id="Seg_8038" s="T294">agaj</ta>
            <ta e="T296" id="Seg_8039" s="T295">taba-LAː-Ar</ta>
            <ta e="T297" id="Seg_8040" s="T296">haŋa</ta>
            <ta e="T298" id="Seg_8041" s="T297">ihilin-Ar</ta>
            <ta e="T299" id="Seg_8042" s="T298">Kuŋadʼaj</ta>
            <ta e="T300" id="Seg_8043" s="T299">tagɨs-An</ta>
            <ta e="T301" id="Seg_8044" s="T300">iːkteː-A</ta>
            <ta e="T302" id="Seg_8045" s="T301">tur-BIT</ta>
            <ta e="T303" id="Seg_8046" s="T302">tobuk-tA-r</ta>
            <ta e="T304" id="Seg_8047" s="T303">taba</ta>
            <ta e="T305" id="Seg_8048" s="T304">bögö</ta>
            <ta e="T306" id="Seg_8049" s="T305">kel-Ar</ta>
            <ta e="T307" id="Seg_8050" s="T306">aktamiː</ta>
            <ta e="T308" id="Seg_8051" s="T307">buːr</ta>
            <ta e="T309" id="Seg_8052" s="T308">emis</ta>
            <ta e="T310" id="Seg_8053" s="T309">bagajɨ</ta>
            <ta e="T311" id="Seg_8054" s="T310">kel-Ar</ta>
            <ta e="T312" id="Seg_8055" s="T311">u͡ol</ta>
            <ta e="T313" id="Seg_8056" s="T312">kap-An</ta>
            <ta e="T314" id="Seg_8057" s="T313">ɨl-Ar</ta>
            <ta e="T315" id="Seg_8058" s="T314">mök-An</ta>
            <ta e="T316" id="Seg_8059" s="T315">bögö</ta>
            <ta e="T317" id="Seg_8060" s="T316">bahak-tI-n</ta>
            <ta e="T318" id="Seg_8061" s="T317">ɨl-TAr-Ar</ta>
            <ta e="T319" id="Seg_8062" s="T318">kabɨrga-tI-n</ta>
            <ta e="T320" id="Seg_8063" s="T319">bɨs-A</ta>
            <ta e="T321" id="Seg_8064" s="T320">hot-Ar</ta>
            <ta e="T322" id="Seg_8065" s="T321">hül-BAkkA</ta>
            <ta e="T323" id="Seg_8066" s="T322">da</ta>
            <ta e="T324" id="Seg_8067" s="T323">killer-Ar-LAr</ta>
            <ta e="T325" id="Seg_8068" s="T324">kü͡ös-LAN-An</ta>
            <ta e="T326" id="Seg_8069" s="T325">dʼe</ta>
            <ta e="T327" id="Seg_8070" s="T326">ahaː-Ar-LAr</ta>
            <ta e="T328" id="Seg_8071" s="T327">kün-nI</ta>
            <ta e="T329" id="Seg_8072" s="T328">meldʼi</ta>
            <ta e="T330" id="Seg_8073" s="T329">ki͡ehe-GA</ta>
            <ta e="T331" id="Seg_8074" s="T330">di͡eri</ta>
            <ta e="T332" id="Seg_8075" s="T331">bu</ta>
            <ta e="T333" id="Seg_8076" s="T332">dʼi͡e</ta>
            <ta e="T334" id="Seg_8077" s="T333">tas-tI-GAr</ta>
            <ta e="T335" id="Seg_8078" s="T334">küččügüj</ta>
            <ta e="T336" id="Seg_8079" s="T335">dʼi͡e</ta>
            <ta e="T337" id="Seg_8080" s="T336">tur-Ar</ta>
            <ta e="T338" id="Seg_8081" s="T337">dʼe</ta>
            <ta e="T339" id="Seg_8082" s="T338">hette</ta>
            <ta e="T340" id="Seg_8083" s="T339">timek</ta>
            <ta e="T341" id="Seg_8084" s="T340">ičči-tI-GAr</ta>
            <ta e="T342" id="Seg_8085" s="T341">bar</ta>
            <ta e="T343" id="Seg_8086" s="T342">iti</ta>
            <ta e="T344" id="Seg_8087" s="T343">dʼi͡e-GA</ta>
            <ta e="T345" id="Seg_8088" s="T344">staršina</ta>
            <ta e="T346" id="Seg_8089" s="T345">bu͡olu͡o</ta>
            <ta e="T347" id="Seg_8090" s="T346">kü͡ös-TA</ta>
            <ta e="T348" id="Seg_8091" s="T347">kördöː</ta>
            <ta e="T349" id="Seg_8092" s="T348">u͡ol</ta>
            <ta e="T350" id="Seg_8093" s="T349">bar-Ar</ta>
            <ta e="T351" id="Seg_8094" s="T350">hette</ta>
            <ta e="T352" id="Seg_8095" s="T351">timek</ta>
            <ta e="T353" id="Seg_8096" s="T352">ičči-tI-GAr</ta>
            <ta e="T354" id="Seg_8097" s="T353">et-Ar</ta>
            <ta e="T355" id="Seg_8098" s="T354">ildʼit-nI</ta>
            <ta e="T356" id="Seg_8099" s="T355">agɨs</ta>
            <ta e="T357" id="Seg_8100" s="T356">koŋnomu͡oj</ta>
            <ta e="T358" id="Seg_8101" s="T357">ičči-tA</ta>
            <ta e="T359" id="Seg_8102" s="T358">bi͡er-TI-tA</ta>
            <ta e="T360" id="Seg_8103" s="T359">eni</ta>
            <ta e="T361" id="Seg_8104" s="T360">hu͡ok</ta>
            <ta e="T362" id="Seg_8105" s="T361">eː</ta>
            <ta e="T363" id="Seg_8106" s="T362">oččogo</ta>
            <ta e="T364" id="Seg_8107" s="T363">min</ta>
            <ta e="T365" id="Seg_8108" s="T364">emi͡e</ta>
            <ta e="T366" id="Seg_8109" s="T365">bi͡er-BAT-BIn</ta>
            <ta e="T367" id="Seg_8110" s="T366">ahaː-Ar-LAr</ta>
            <ta e="T368" id="Seg_8111" s="T367">harsi͡erda</ta>
            <ta e="T369" id="Seg_8112" s="T368">ü͡ör</ta>
            <ta e="T370" id="Seg_8113" s="T369">is-Ar</ta>
            <ta e="T371" id="Seg_8114" s="T370">emi͡e</ta>
            <ta e="T372" id="Seg_8115" s="T371">tobuk-tI-nAn</ta>
            <ta e="T373" id="Seg_8116" s="T372">ün-An</ta>
            <ta e="T374" id="Seg_8117" s="T373">tagɨs-Ar</ta>
            <ta e="T375" id="Seg_8118" s="T374">Kuŋadʼaj</ta>
            <ta e="T376" id="Seg_8119" s="T375">Kuŋadʼaj</ta>
            <ta e="T377" id="Seg_8120" s="T376">töröː-BAT</ta>
            <ta e="T378" id="Seg_8121" s="T377">maːŋkaːj-nI</ta>
            <ta e="T379" id="Seg_8122" s="T378">tut-An</ta>
            <ta e="T380" id="Seg_8123" s="T379">ɨl-Ar</ta>
            <ta e="T381" id="Seg_8124" s="T380">kaldaː-tI-ttAn</ta>
            <ta e="T382" id="Seg_8125" s="T381">kaja=kuː</ta>
            <ta e="T383" id="Seg_8126" s="T382">bahak-nI</ta>
            <ta e="T384" id="Seg_8127" s="T383">tahaːr</ta>
            <ta e="T385" id="Seg_8128" s="T384">ölör-An</ta>
            <ta e="T386" id="Seg_8129" s="T385">keːs-Ar</ta>
            <ta e="T387" id="Seg_8130" s="T386">emi͡e</ta>
            <ta e="T388" id="Seg_8131" s="T387">hül-BAkkA</ta>
            <ta e="T389" id="Seg_8132" s="T388">hi͡e-Ar-LAr</ta>
            <ta e="T390" id="Seg_8133" s="T389">kaja</ta>
            <ta e="T391" id="Seg_8134" s="T390">bu</ta>
            <ta e="T392" id="Seg_8135" s="T391">bili</ta>
            <ta e="T393" id="Seg_8136" s="T392">tojon-LAr</ta>
            <ta e="T394" id="Seg_8137" s="T393">gi͡en-LArI-n</ta>
            <ta e="T395" id="Seg_8138" s="T394">ölör-BIT</ta>
            <ta e="T396" id="Seg_8139" s="T395">e-BIT</ta>
            <ta e="T397" id="Seg_8140" s="T396">utuj-Ar-LAr</ta>
            <ta e="T398" id="Seg_8141" s="T397">harsi͡erda</ta>
            <ta e="T399" id="Seg_8142" s="T398">Kuŋadʼaj</ta>
            <ta e="T400" id="Seg_8143" s="T399">tagɨs-I-BIT-tA</ta>
            <ta e="T401" id="Seg_8144" s="T400">agɨs</ta>
            <ta e="T402" id="Seg_8145" s="T401">koŋnomu͡oj</ta>
            <ta e="T403" id="Seg_8146" s="T402">küheːjin-tA</ta>
            <ta e="T404" id="Seg_8147" s="T403">kürej-tI-n</ta>
            <ta e="T405" id="Seg_8148" s="T404">ku͡ohan-A</ta>
            <ta e="T406" id="Seg_8149" s="T405">olor-Ar</ta>
            <ta e="T407" id="Seg_8150" s="T406">kajaː</ta>
            <ta e="T408" id="Seg_8151" s="T407">agɨs</ta>
            <ta e="T409" id="Seg_8152" s="T408">koŋnomu͡oj</ta>
            <ta e="T410" id="Seg_8153" s="T409">ičči-tA</ta>
            <ta e="T411" id="Seg_8154" s="T410">doroːbo</ta>
            <ta e="T412" id="Seg_8155" s="T411">össü͡ö</ta>
            <ta e="T413" id="Seg_8156" s="T412">doroːbo</ta>
            <ta e="T414" id="Seg_8157" s="T413">bügün</ta>
            <ta e="T415" id="Seg_8158" s="T414">ölör-TAː-A-BIt</ta>
            <ta e="T416" id="Seg_8159" s="T415">ele</ta>
            <ta e="T417" id="Seg_8160" s="T416">taba-LAr-BIt-ttAn</ta>
            <ta e="T418" id="Seg_8161" s="T417">hi͡e-A</ta>
            <ta e="T419" id="Seg_8162" s="T418">hɨt-A-GIt</ta>
            <ta e="T420" id="Seg_8163" s="T419">beje-GIt</ta>
            <ta e="T421" id="Seg_8164" s="T420">buruj-GIt</ta>
            <ta e="T422" id="Seg_8165" s="T421">agɨs</ta>
            <ta e="T423" id="Seg_8166" s="T422">koŋnomu͡oj</ta>
            <ta e="T424" id="Seg_8167" s="T423">ičči-tA</ta>
            <ta e="T425" id="Seg_8168" s="T424">tobuk-LAː-An</ta>
            <ta e="T426" id="Seg_8169" s="T425">tur-Ar</ta>
            <ta e="T427" id="Seg_8170" s="T426">kihi-nI</ta>
            <ta e="T428" id="Seg_8171" s="T427">kürej</ta>
            <ta e="T429" id="Seg_8172" s="T428">üŋüː-tI-nAn</ta>
            <ta e="T430" id="Seg_8173" s="T429">öttük-GA</ta>
            <ta e="T431" id="Seg_8174" s="T430">haːj-Ar</ta>
            <ta e="T432" id="Seg_8175" s="T431">da</ta>
            <ta e="T433" id="Seg_8176" s="T432">hɨːs-An</ta>
            <ta e="T434" id="Seg_8177" s="T433">keːs-Ar</ta>
            <ta e="T435" id="Seg_8178" s="T434">Kuŋadʼaj-tA</ta>
            <ta e="T436" id="Seg_8179" s="T435">kaːr</ta>
            <ta e="T437" id="Seg_8180" s="T436">alɨn-tI-nAn</ta>
            <ta e="T438" id="Seg_8181" s="T437">bar-BIT</ta>
            <ta e="T439" id="Seg_8182" s="T438">hette</ta>
            <ta e="T440" id="Seg_8183" s="T439">timek</ta>
            <ta e="T441" id="Seg_8184" s="T440">ičči-tA</ta>
            <ta e="T442" id="Seg_8185" s="T441">kürej</ta>
            <ta e="T443" id="Seg_8186" s="T442">ku͡ohan-A</ta>
            <ta e="T444" id="Seg_8187" s="T443">olor-TAK-InA</ta>
            <ta e="T445" id="Seg_8188" s="T444">tagɨs-Ar</ta>
            <ta e="T446" id="Seg_8189" s="T445">kajaː</ta>
            <ta e="T447" id="Seg_8190" s="T446">hette</ta>
            <ta e="T448" id="Seg_8191" s="T447">timek</ta>
            <ta e="T449" id="Seg_8192" s="T448">ičči-tA</ta>
            <ta e="T450" id="Seg_8193" s="T449">doroːbo</ta>
            <ta e="T451" id="Seg_8194" s="T450">össü͡ö</ta>
            <ta e="T452" id="Seg_8195" s="T451">doroːbo</ta>
            <ta e="T453" id="Seg_8196" s="T452">bügün</ta>
            <ta e="T454" id="Seg_8197" s="T453">ölör-TAː-A-BIt</ta>
            <ta e="T455" id="Seg_8198" s="T454">ele</ta>
            <ta e="T456" id="Seg_8199" s="T455">taba-LAr-BItI-n</ta>
            <ta e="T457" id="Seg_8200" s="T456">hi͡e-A</ta>
            <ta e="T458" id="Seg_8201" s="T457">hɨt-A-GIt</ta>
            <ta e="T459" id="Seg_8202" s="T458">beje-GIt</ta>
            <ta e="T460" id="Seg_8203" s="T459">buruj-GIt</ta>
            <ta e="T461" id="Seg_8204" s="T460">hette</ta>
            <ta e="T462" id="Seg_8205" s="T461">timek</ta>
            <ta e="T463" id="Seg_8206" s="T462">ičči-tA</ta>
            <ta e="T464" id="Seg_8207" s="T463">tobuk-LAː-An</ta>
            <ta e="T465" id="Seg_8208" s="T464">tur-Ar</ta>
            <ta e="T466" id="Seg_8209" s="T465">kihi-nI</ta>
            <ta e="T467" id="Seg_8210" s="T466">kürej</ta>
            <ta e="T468" id="Seg_8211" s="T467">üŋüː-tI-nAn</ta>
            <ta e="T469" id="Seg_8212" s="T468">öttük-GA</ta>
            <ta e="T470" id="Seg_8213" s="T469">as-AːrI</ta>
            <ta e="T471" id="Seg_8214" s="T470">hir-nI</ta>
            <ta e="T472" id="Seg_8215" s="T471">haːj-BIT</ta>
            <ta e="T473" id="Seg_8216" s="T472">kaččaga</ta>
            <ta e="T474" id="Seg_8217" s="T473">ere</ta>
            <ta e="T475" id="Seg_8218" s="T474">Kuŋadʼaj</ta>
            <ta e="T476" id="Seg_8219" s="T475">nöŋü͡ö</ta>
            <ta e="T477" id="Seg_8220" s="T476">hir-I-nAn</ta>
            <ta e="T478" id="Seg_8221" s="T477">tagɨs-A</ta>
            <ta e="T479" id="Seg_8222" s="T478">köt-Ar</ta>
            <ta e="T480" id="Seg_8223" s="T479">da</ta>
            <ta e="T481" id="Seg_8224" s="T480">ikki</ta>
            <ta e="T482" id="Seg_8225" s="T481">atak-tA-r</ta>
            <ta e="T483" id="Seg_8226" s="T482">tur-A</ta>
            <ta e="T484" id="Seg_8227" s="T483">ekkireː-Ar</ta>
            <ta e="T485" id="Seg_8228" s="T484">körüleː-BIT-tA</ta>
            <ta e="T486" id="Seg_8229" s="T485">kös</ta>
            <ta e="T487" id="Seg_8230" s="T486">bar-An</ta>
            <ta e="T488" id="Seg_8231" s="T487">er-Ar</ta>
            <ta e="T489" id="Seg_8232" s="T488">haŋas-tA</ta>
            <ta e="T490" id="Seg_8233" s="T489">kös-An</ta>
            <ta e="T491" id="Seg_8234" s="T490">er-Ar</ta>
            <ta e="T492" id="Seg_8235" s="T491">aga-tA</ta>
            <ta e="T493" id="Seg_8236" s="T492">kergen</ta>
            <ta e="T494" id="Seg_8237" s="T493">bi͡er-BIT</ta>
            <ta e="T495" id="Seg_8238" s="T494">Kuŋadʼaj</ta>
            <ta e="T496" id="Seg_8239" s="T495">haŋas-tI-n</ta>
            <ta e="T497" id="Seg_8240" s="T496">nʼu͡oguː-tI-n</ta>
            <ta e="T498" id="Seg_8241" s="T497">kap-An</ta>
            <ta e="T499" id="Seg_8242" s="T498">ɨl-Ar</ta>
            <ta e="T500" id="Seg_8243" s="T499">ɨːt-BAT-BIn</ta>
            <ta e="T501" id="Seg_8244" s="T500">kaja</ta>
            <ta e="T502" id="Seg_8245" s="T501">di͡ekki</ta>
            <ta e="T503" id="Seg_8246" s="T502">bar-A-GIn</ta>
            <ta e="T504" id="Seg_8247" s="T503">dʼaktar</ta>
            <ta e="T505" id="Seg_8248" s="T504">kötüt-An</ta>
            <ta e="T506" id="Seg_8249" s="T505">kaːl-Ar</ta>
            <ta e="T507" id="Seg_8250" s="T506">Kuŋadʼaj</ta>
            <ta e="T508" id="Seg_8251" s="T507">hit-A</ta>
            <ta e="T509" id="Seg_8252" s="T508">bat-TAː-An</ta>
            <ta e="T510" id="Seg_8253" s="T509">ölgöbüːn-tI-n</ta>
            <ta e="T511" id="Seg_8254" s="T510">ilt-A</ta>
            <ta e="T512" id="Seg_8255" s="T511">kaːl-Ar</ta>
            <ta e="T513" id="Seg_8256" s="T512">Kuŋadʼaj</ta>
            <ta e="T514" id="Seg_8257" s="T513">agɨs</ta>
            <ta e="T515" id="Seg_8258" s="T514">koŋnomu͡oj</ta>
            <ta e="T516" id="Seg_8259" s="T515">ičči-tI-GAr</ta>
            <ta e="T517" id="Seg_8260" s="T516">kiːr-Ar</ta>
            <ta e="T518" id="Seg_8261" s="T517">dʼe</ta>
            <ta e="T519" id="Seg_8262" s="T518">agɨs</ta>
            <ta e="T520" id="Seg_8263" s="T519">koŋnomu͡oj</ta>
            <ta e="T521" id="Seg_8264" s="T520">ičči-tA</ta>
            <ta e="T522" id="Seg_8265" s="T521">eje-LAːK</ta>
            <ta e="T523" id="Seg_8266" s="T522">er-TAK-GA</ta>
            <ta e="T524" id="Seg_8267" s="T523">balɨs-LAr-BI-n</ta>
            <ta e="T525" id="Seg_8268" s="T524">karaj-An</ta>
            <ta e="T526" id="Seg_8269" s="T525">olor</ta>
            <ta e="T527" id="Seg_8270" s="T526">kuhagan-LIk</ta>
            <ta e="T528" id="Seg_8271" s="T527">kör-IAK-ŋ</ta>
            <ta e="T529" id="Seg_8272" s="T528">biːr</ta>
            <ta e="T530" id="Seg_8273" s="T529">kün-I-nAn</ta>
            <ta e="T531" id="Seg_8274" s="T530">hu͡ok</ta>
            <ta e="T532" id="Seg_8275" s="T531">oŋor-t-IAK-m</ta>
            <ta e="T533" id="Seg_8276" s="T532">haŋas-BI-n</ta>
            <ta e="T534" id="Seg_8277" s="T533">bat-A</ta>
            <ta e="T535" id="Seg_8278" s="T534">bar-TI-m</ta>
            <ta e="T536" id="Seg_8279" s="T535">haŋas-tI-n</ta>
            <ta e="T537" id="Seg_8280" s="T536">bat-An</ta>
            <ta e="T538" id="Seg_8281" s="T537">hüter-An</ta>
            <ta e="T539" id="Seg_8282" s="T538">keːs-Ar</ta>
            <ta e="T540" id="Seg_8283" s="T539">kannɨk</ta>
            <ta e="T541" id="Seg_8284" s="T540">dojdu</ta>
            <ta e="T542" id="Seg_8285" s="T541">uhuk-tA-r</ta>
            <ta e="T543" id="Seg_8286" s="T542">kör-Ar</ta>
            <ta e="T544" id="Seg_8287" s="T543">hit-IAK</ta>
            <ta e="T545" id="Seg_8288" s="T544">hit-BAT</ta>
            <ta e="T546" id="Seg_8289" s="T545">ergij-t-An</ta>
            <ta e="T547" id="Seg_8290" s="T546">egel-An</ta>
            <ta e="T548" id="Seg_8291" s="T547">dʼi͡e-tI-n</ta>
            <ta e="T549" id="Seg_8292" s="T548">dek</ta>
            <ta e="T550" id="Seg_8293" s="T549">kü͡öj-Ar</ta>
            <ta e="T551" id="Seg_8294" s="T550">tut-IAK</ta>
            <ta e="T552" id="Seg_8295" s="T551">da</ta>
            <ta e="T553" id="Seg_8296" s="T552">ereːri</ta>
            <ta e="T554" id="Seg_8297" s="T553">tut-BAkkA</ta>
            <ta e="T555" id="Seg_8298" s="T554">haŋas-tI-n</ta>
            <ta e="T556" id="Seg_8299" s="T555">kelin-tI-ttAn</ta>
            <ta e="T557" id="Seg_8300" s="T556">kiːr-Ar</ta>
            <ta e="T558" id="Seg_8301" s="T557">haŋas-tA</ta>
            <ta e="T559" id="Seg_8302" s="T558">ogo-tI-n</ta>
            <ta e="T560" id="Seg_8303" s="T559">emij-LAː-A</ta>
            <ta e="T561" id="Seg_8304" s="T560">olor-Ar</ta>
            <ta e="T562" id="Seg_8305" s="T561">balɨs-LArA</ta>
            <ta e="T563" id="Seg_8306" s="T562">ahaː-A</ta>
            <ta e="T564" id="Seg_8307" s="T563">olor-Ar</ta>
            <ta e="T565" id="Seg_8308" s="T564">dʼe</ta>
            <ta e="T566" id="Seg_8309" s="T565">haŋas</ta>
            <ta e="T567" id="Seg_8310" s="T566">buruj-GI-n</ta>
            <ta e="T568" id="Seg_8311" s="T567">aːs-A-r-TI-ŋ</ta>
            <ta e="T569" id="Seg_8312" s="T568">ogo-GI-n</ta>
            <ta e="T570" id="Seg_8313" s="T569">ɨl-BAtAK-I-ŋ</ta>
            <ta e="T571" id="Seg_8314" s="T570">bu͡ol-TAR</ta>
            <ta e="T572" id="Seg_8315" s="T571">ölör-IAK</ta>
            <ta e="T573" id="Seg_8316" s="T572">e-TI-m</ta>
            <ta e="T574" id="Seg_8317" s="T573">agɨs</ta>
            <ta e="T575" id="Seg_8318" s="T574">koŋnomu͡oj</ta>
            <ta e="T576" id="Seg_8319" s="T575">ičči-tA</ta>
            <ta e="T577" id="Seg_8320" s="T576">kuttan-AːrI</ta>
            <ta e="T578" id="Seg_8321" s="T577">kuttan-Ar</ta>
            <ta e="T579" id="Seg_8322" s="T578">olor-Ar-LAr</ta>
            <ta e="T580" id="Seg_8323" s="T579">kahan</ta>
            <ta e="T581" id="Seg_8324" s="T580">ere</ta>
            <ta e="T582" id="Seg_8325" s="T581">üs-Is</ta>
            <ta e="T583" id="Seg_8326" s="T582">kün-tI-GAr</ta>
            <ta e="T584" id="Seg_8327" s="T583">dʼe</ta>
            <ta e="T585" id="Seg_8328" s="T584">agɨs</ta>
            <ta e="T586" id="Seg_8329" s="T585">koŋnomu͡oj</ta>
            <ta e="T587" id="Seg_8330" s="T586">ičči-tA</ta>
            <ta e="T588" id="Seg_8331" s="T587">dʼon-GI-n</ta>
            <ta e="T589" id="Seg_8332" s="T588">mus</ta>
            <ta e="T590" id="Seg_8333" s="T589">munnʼak-LAː-IAK-BIt</ta>
            <ta e="T591" id="Seg_8334" s="T590">di͡e-Ar</ta>
            <ta e="T592" id="Seg_8335" s="T591">Kuŋadʼaj</ta>
            <ta e="T593" id="Seg_8336" s="T592">dʼe</ta>
            <ta e="T594" id="Seg_8337" s="T593">mus-Ar-LAr</ta>
            <ta e="T595" id="Seg_8338" s="T594">hette</ta>
            <ta e="T596" id="Seg_8339" s="T595">u͡on</ta>
            <ta e="T597" id="Seg_8340" s="T596">dʼi͡e</ta>
            <ta e="T598" id="Seg_8341" s="T597">kihi-tI-n</ta>
            <ta e="T599" id="Seg_8342" s="T598">Kuŋadʼaj</ta>
            <ta e="T600" id="Seg_8343" s="T599">et-Ar</ta>
            <ta e="T601" id="Seg_8344" s="T600">ehigi</ta>
            <ta e="T602" id="Seg_8345" s="T601">ikki</ta>
            <ta e="T603" id="Seg_8346" s="T602">küheːjin</ta>
            <ta e="T604" id="Seg_8347" s="T603">baːr-GIt</ta>
            <ta e="T605" id="Seg_8348" s="T604">agɨs</ta>
            <ta e="T606" id="Seg_8349" s="T605">koŋnomu͡oj</ta>
            <ta e="T607" id="Seg_8350" s="T606">küheːjin-tA</ta>
            <ta e="T608" id="Seg_8351" s="T607">u͡onna</ta>
            <ta e="T609" id="Seg_8352" s="T608">hette</ta>
            <ta e="T610" id="Seg_8353" s="T609">timek</ta>
            <ta e="T611" id="Seg_8354" s="T610">ičči-tA</ta>
            <ta e="T612" id="Seg_8355" s="T611">anɨ</ta>
            <ta e="T613" id="Seg_8356" s="T612">küheːjin</ta>
            <ta e="T614" id="Seg_8357" s="T613">bu͡ol-An</ta>
            <ta e="T615" id="Seg_8358" s="T614">büt-TI-GIt</ta>
            <ta e="T616" id="Seg_8359" s="T615">küheːjin-GIt</ta>
            <ta e="T617" id="Seg_8360" s="T616">iti</ta>
            <ta e="T618" id="Seg_8361" s="T617">u͡ol</ta>
            <ta e="T619" id="Seg_8362" s="T618">ogo</ta>
            <ta e="T620" id="Seg_8363" s="T619">ubaj-I-m</ta>
            <ta e="T621" id="Seg_8364" s="T620">ogo-tA</ta>
            <ta e="T622" id="Seg_8365" s="T621">komuj-n-I-ŋ</ta>
            <ta e="T623" id="Seg_8366" s="T622">barɨ-GIt</ta>
            <ta e="T624" id="Seg_8367" s="T623">ikki</ta>
            <ta e="T625" id="Seg_8368" s="T624">kün-GA</ta>
            <ta e="T626" id="Seg_8369" s="T625">üs-Is</ta>
            <ta e="T627" id="Seg_8370" s="T626">kün-GItI-GAr</ta>
            <ta e="T628" id="Seg_8371" s="T627">kös-IAK-GIt</ta>
            <ta e="T629" id="Seg_8372" s="T628">bastɨŋ-GIt</ta>
            <ta e="T630" id="Seg_8373" s="T629">gini</ta>
            <ta e="T631" id="Seg_8374" s="T630">bu͡ol-IAK.[tA]</ta>
            <ta e="T632" id="Seg_8375" s="T631">atɨn</ta>
            <ta e="T633" id="Seg_8376" s="T632">taba-nI</ta>
            <ta e="T634" id="Seg_8377" s="T633">kölün-I-m</ta>
            <ta e="T635" id="Seg_8378" s="T634">aga-ŋ</ta>
            <ta e="T636" id="Seg_8379" s="T635">taba-tI-n</ta>
            <ta e="T637" id="Seg_8380" s="T636">keri͡es-tI-n</ta>
            <ta e="T638" id="Seg_8381" s="T637">kölün-Aːr</ta>
            <ta e="T639" id="Seg_8382" s="T638">di͡e-Ar</ta>
            <ta e="T640" id="Seg_8383" s="T639">u͡ol-GA</ta>
            <ta e="T641" id="Seg_8384" s="T640">u͡ol</ta>
            <ta e="T642" id="Seg_8385" s="T641">ulaːt-BIT</ta>
            <ta e="T643" id="Seg_8386" s="T642">bastaː-A-t-An-GIn</ta>
            <ta e="T644" id="Seg_8387" s="T643">kös-IAK-GIt</ta>
            <ta e="T645" id="Seg_8388" s="T644">mas</ta>
            <ta e="T646" id="Seg_8389" s="T645">haga-tI-nAn</ta>
            <ta e="T647" id="Seg_8390" s="T646">dʼirbiː</ta>
            <ta e="T648" id="Seg_8391" s="T647">baːr</ta>
            <ta e="T649" id="Seg_8392" s="T648">bu͡ol-IAK.[tA]</ta>
            <ta e="T650" id="Seg_8393" s="T649">bu</ta>
            <ta e="T651" id="Seg_8394" s="T650">dʼirbiː</ta>
            <ta e="T652" id="Seg_8395" s="T651">mu͡ora</ta>
            <ta e="T653" id="Seg_8396" s="T652">di͡ekki</ta>
            <ta e="T654" id="Seg_8397" s="T653">örüt-tI-nAn</ta>
            <ta e="T655" id="Seg_8398" s="T654">kös-IAK-ŋ</ta>
            <ta e="T656" id="Seg_8399" s="T655">ikki</ta>
            <ta e="T657" id="Seg_8400" s="T656">ɨj-nI</ta>
            <ta e="T658" id="Seg_8401" s="T657">bɨha</ta>
            <ta e="T659" id="Seg_8402" s="T658">ele-tA</ta>
            <ta e="T660" id="Seg_8403" s="T659">bu͡ollagɨna</ta>
            <ta e="T661" id="Seg_8404" s="T660">dʼirbiː-tA</ta>
            <ta e="T662" id="Seg_8405" s="T661">büt-TAK-InA</ta>
            <ta e="T663" id="Seg_8406" s="T662">hir</ta>
            <ta e="T664" id="Seg_8407" s="T663">bu͡ol-IAK.[tA]</ta>
            <ta e="T665" id="Seg_8408" s="T664">kaːr-tI-n</ta>
            <ta e="T666" id="Seg_8409" s="T665">alɨn-tI-ttAn</ta>
            <ta e="T667" id="Seg_8410" s="T666">buru͡o-LAːK</ta>
            <ta e="T668" id="Seg_8411" s="T667">bu͡ol-IAK.[tA]</ta>
            <ta e="T669" id="Seg_8412" s="T668">ol</ta>
            <ta e="T670" id="Seg_8413" s="T669">bihigi</ta>
            <ta e="T671" id="Seg_8414" s="T670">törüt</ta>
            <ta e="T672" id="Seg_8415" s="T671">hir-BIt</ta>
            <ta e="T673" id="Seg_8416" s="T672">ulakan</ta>
            <ta e="T674" id="Seg_8417" s="T673">hu͡ol-GA</ta>
            <ta e="T675" id="Seg_8418" s="T674">kiːr-IAK-ŋ</ta>
            <ta e="T676" id="Seg_8419" s="T675">ajmak-LAr-GA-r</ta>
            <ta e="T677" id="Seg_8420" s="T676">tij-IAK-ŋ</ta>
            <ta e="T678" id="Seg_8421" s="T677">dʼon-LAr</ta>
            <ta e="T679" id="Seg_8422" s="T678">taba-GItI-n</ta>
            <ta e="T680" id="Seg_8423" s="T679">tüːn-nI-kün-nI</ta>
            <ta e="T681" id="Seg_8424" s="T680">bɨha</ta>
            <ta e="T682" id="Seg_8425" s="T681">kör-IAK-GIt</ta>
            <ta e="T683" id="Seg_8426" s="T682">agɨs</ta>
            <ta e="T684" id="Seg_8427" s="T683">koŋnomu͡oj</ta>
            <ta e="T685" id="Seg_8428" s="T684">küheːjin-tA</ta>
            <ta e="T686" id="Seg_8429" s="T685">hette</ta>
            <ta e="T687" id="Seg_8430" s="T686">timek</ta>
            <ta e="T688" id="Seg_8431" s="T687">ičči-tA</ta>
            <ta e="T689" id="Seg_8432" s="T688">emi͡e</ta>
            <ta e="T690" id="Seg_8433" s="T689">körüs-IAK-GIt</ta>
            <ta e="T691" id="Seg_8434" s="T690">min</ta>
            <ta e="T692" id="Seg_8435" s="T691">bu͡ollagɨna</ta>
            <ta e="T693" id="Seg_8436" s="T692">bu</ta>
            <ta e="T694" id="Seg_8437" s="T693">üčügej-LIk</ta>
            <ta e="T695" id="Seg_8438" s="T694">ajannaː-Ar-GIt</ta>
            <ta e="T696" id="Seg_8439" s="T695">tuhuttan</ta>
            <ta e="T697" id="Seg_8440" s="T696">kuhagan</ta>
            <ta e="T698" id="Seg_8441" s="T697">dʼon-nI</ta>
            <ta e="T699" id="Seg_8442" s="T698">čaŋɨt-LAr-nI</ta>
            <ta e="T700" id="Seg_8443" s="T699">kɨːl-LAr-nI</ta>
            <ta e="T701" id="Seg_8444" s="T700">hu͡ok</ta>
            <ta e="T702" id="Seg_8445" s="T701">gɨn-A</ta>
            <ta e="T703" id="Seg_8446" s="T702">bar-TI-m</ta>
            <ta e="T704" id="Seg_8447" s="T703">ol</ta>
            <ta e="T705" id="Seg_8448" s="T704">hir-GA</ta>
            <ta e="T706" id="Seg_8449" s="T705">tij-Ar</ta>
            <ta e="T707" id="Seg_8450" s="T706">kün-GA-r</ta>
            <ta e="T708" id="Seg_8451" s="T707">tij-IAK-m</ta>
            <ta e="T709" id="Seg_8452" s="T708">ulakan-LIk</ta>
            <ta e="T710" id="Seg_8453" s="T709">haːraː-TAK-BInA</ta>
            <ta e="T711" id="Seg_8454" s="T710">üs-Is</ta>
            <ta e="T712" id="Seg_8455" s="T711">kün-tI-GAr</ta>
            <ta e="T713" id="Seg_8456" s="T712">tij-IAK-m</ta>
            <ta e="T714" id="Seg_8457" s="T713">di͡e-Ar</ta>
            <ta e="T715" id="Seg_8458" s="T714">üs-Is</ta>
            <ta e="T716" id="Seg_8459" s="T715">kün-tI-GAr</ta>
            <ta e="T717" id="Seg_8460" s="T716">kös-Ar-LAr</ta>
            <ta e="T718" id="Seg_8461" s="T717">Kuŋadʼaj</ta>
            <ta e="T719" id="Seg_8462" s="T718">araːk-An</ta>
            <ta e="T720" id="Seg_8463" s="T719">bar-Ar</ta>
            <ta e="T721" id="Seg_8464" s="T720">bu͡opsa</ta>
            <ta e="T722" id="Seg_8465" s="T721">hatɨː</ta>
            <ta e="T723" id="Seg_8466" s="T722">bu</ta>
            <ta e="T724" id="Seg_8467" s="T723">ogo</ta>
            <ta e="T725" id="Seg_8468" s="T724">bastɨŋ</ta>
            <ta e="T726" id="Seg_8469" s="T725">bu͡ol-An</ta>
            <ta e="T727" id="Seg_8470" s="T726">kös-An</ta>
            <ta e="T728" id="Seg_8471" s="T727">is-TI-LAr</ta>
            <ta e="T729" id="Seg_8472" s="T728">keteː-A-n-An</ta>
            <ta e="T730" id="Seg_8473" s="T729">ajannaː-Ar-LAr</ta>
            <ta e="T731" id="Seg_8474" s="T730">tojon-nI-kotun-nI</ta>
            <ta e="T732" id="Seg_8475" s="T731">barɨ-tI-n</ta>
            <ta e="T733" id="Seg_8476" s="T732">keteː-A-t-Ar</ta>
            <ta e="T734" id="Seg_8477" s="T733">bu</ta>
            <ta e="T735" id="Seg_8478" s="T734">u͡ol</ta>
            <ta e="T736" id="Seg_8479" s="T735">ɨj-tI-n</ta>
            <ta e="T737" id="Seg_8480" s="T736">orto-tA</ta>
            <ta e="T738" id="Seg_8481" s="T737">bu͡ol-BIT</ta>
            <ta e="T739" id="Seg_8482" s="T738">dʼirbiː-tI-n</ta>
            <ta e="T740" id="Seg_8483" s="T739">irdeː-An</ta>
            <ta e="T741" id="Seg_8484" s="T740">is-TAK-tA</ta>
            <ta e="T742" id="Seg_8485" s="T741">agaj</ta>
            <ta e="T743" id="Seg_8486" s="T742">mu͡ora</ta>
            <ta e="T744" id="Seg_8487" s="T743">di͡ekki</ta>
            <ta e="T745" id="Seg_8488" s="T744">dʼirbiː</ta>
            <ta e="T746" id="Seg_8489" s="T745">araːk-Ar</ta>
            <ta e="T747" id="Seg_8490" s="T746">ürüt-tI-GAr</ta>
            <ta e="T748" id="Seg_8491" s="T747">kihi</ta>
            <ta e="T749" id="Seg_8492" s="T748">hɨrɨt-Ar</ta>
            <ta e="T750" id="Seg_8493" s="T749">kaja</ta>
            <ta e="T751" id="Seg_8494" s="T750">di͡ekki</ta>
            <ta e="T752" id="Seg_8495" s="T751">kihi-GIn=Ij</ta>
            <ta e="T753" id="Seg_8496" s="T752">tu͡ok</ta>
            <ta e="T754" id="Seg_8497" s="T753">kel-IAK.[tA]=Ij</ta>
            <ta e="T755" id="Seg_8498" s="T754">dʼi͡e-nI-u͡ot-nI</ta>
            <ta e="T756" id="Seg_8499" s="T755">bil-BAT-BIn</ta>
            <ta e="T757" id="Seg_8500" s="T756">töröː-BIT</ta>
            <ta e="T758" id="Seg_8501" s="T757">ubaj-BI-n</ta>
            <ta e="T759" id="Seg_8502" s="T758">hüter-A</ta>
            <ta e="T760" id="Seg_8503" s="T759">hɨrɨt-A-BIn</ta>
            <ta e="T761" id="Seg_8504" s="T760">kaja-LArA=Ij</ta>
            <ta e="T762" id="Seg_8505" s="T761">ol</ta>
            <ta e="T763" id="Seg_8506" s="T762">agɨs</ta>
            <ta e="T764" id="Seg_8507" s="T763">koŋnomu͡oj</ta>
            <ta e="T765" id="Seg_8508" s="T764">küheːjin-tA</ta>
            <ta e="T766" id="Seg_8509" s="T765">heːj</ta>
            <ta e="T767" id="Seg_8510" s="T766">ol</ta>
            <ta e="T768" id="Seg_8511" s="T767">min</ta>
            <ta e="T769" id="Seg_8512" s="T768">kihi-m</ta>
            <ta e="T770" id="Seg_8513" s="T769">ol</ta>
            <ta e="T771" id="Seg_8514" s="T770">delemičeː</ta>
            <ta e="T772" id="Seg_8515" s="T771">üːr-An</ta>
            <ta e="T773" id="Seg_8516" s="T772">is-Ar</ta>
            <ta e="T774" id="Seg_8517" s="T773">dʼe</ta>
            <ta e="T775" id="Seg_8518" s="T774">oččogo</ta>
            <ta e="T776" id="Seg_8519" s="T775">min-GA</ta>
            <ta e="T777" id="Seg_8520" s="T776">kihi</ta>
            <ta e="T778" id="Seg_8521" s="T777">bu͡ol</ta>
            <ta e="T779" id="Seg_8522" s="T778">haŋar-A</ta>
            <ta e="T780" id="Seg_8523" s="T779">tart-BAT</ta>
            <ta e="T781" id="Seg_8524" s="T780">jeslʼi</ta>
            <ta e="T782" id="Seg_8525" s="T781">kihi</ta>
            <ta e="T783" id="Seg_8526" s="T782">bu͡ol-IAK-ŋ</ta>
            <ta e="T784" id="Seg_8527" s="T783">hu͡ok-tA</ta>
            <ta e="T785" id="Seg_8528" s="T784">bu</ta>
            <ta e="T786" id="Seg_8529" s="T785">üs</ta>
            <ta e="T787" id="Seg_8530" s="T786">halaː-LAːK</ta>
            <ta e="T788" id="Seg_8531" s="T787">kürej-I-m</ta>
            <ta e="T789" id="Seg_8532" s="T788">aŋar</ta>
            <ta e="T790" id="Seg_8533" s="T789">halaː-tA</ta>
            <ta e="T791" id="Seg_8534" s="T790">kaŋɨrgas-tA</ta>
            <ta e="T792" id="Seg_8535" s="T791">bu͡ol-IAK-ŋ</ta>
            <ta e="T793" id="Seg_8536" s="T792">ikki-TA</ta>
            <ta e="T794" id="Seg_8537" s="T793">bukatɨːr</ta>
            <ta e="T795" id="Seg_8538" s="T794">eːlde-tA</ta>
            <ta e="T796" id="Seg_8539" s="T795">kaŋɨrgas-LAːK</ta>
            <ta e="T797" id="Seg_8540" s="T796">kihi</ta>
            <ta e="T798" id="Seg_8541" s="T797">höbüleː-A-n-Ar</ta>
            <ta e="T799" id="Seg_8542" s="T798">barɨs-Ar</ta>
            <ta e="T800" id="Seg_8543" s="T799">kös-An</ta>
            <ta e="T801" id="Seg_8544" s="T800">is-TI-LAr</ta>
            <ta e="T802" id="Seg_8545" s="T801">ɨj-LArA</ta>
            <ta e="T803" id="Seg_8546" s="T802">baran-TI-tA</ta>
            <ta e="T804" id="Seg_8547" s="T803">nöŋü͡ö</ta>
            <ta e="T805" id="Seg_8548" s="T804">ɨj</ta>
            <ta e="T806" id="Seg_8549" s="T805">orto-tA</ta>
            <ta e="T807" id="Seg_8550" s="T806">bu͡ol-TI-tA</ta>
            <ta e="T808" id="Seg_8551" s="T807">emi͡e</ta>
            <ta e="T809" id="Seg_8552" s="T808">kihi-nI</ta>
            <ta e="T810" id="Seg_8553" s="T809">kör-TI-LAr</ta>
            <ta e="T811" id="Seg_8554" s="T810">biːr</ta>
            <ta e="T812" id="Seg_8555" s="T811">dʼirbiː-kAːN-GA</ta>
            <ta e="T813" id="Seg_8556" s="T812">emi͡e</ta>
            <ta e="T814" id="Seg_8557" s="T813">doroːbo-LAː-A-s-Ar</ta>
            <ta e="T815" id="Seg_8558" s="T814">kaja</ta>
            <ta e="T816" id="Seg_8559" s="T815">di͡ekki</ta>
            <ta e="T817" id="Seg_8560" s="T816">dʼi͡e-LAːK-u͡ot-LAːK</ta>
            <ta e="T818" id="Seg_8561" s="T817">kihi-GIn=Ij</ta>
            <ta e="T819" id="Seg_8562" s="T818">dʼi͡e-u͡ot</ta>
            <ta e="T820" id="Seg_8563" s="T819">hu͡ok</ta>
            <ta e="T821" id="Seg_8564" s="T820">hɨrɨt-A-BIn</ta>
            <ta e="T822" id="Seg_8565" s="T821">agaj</ta>
            <ta e="T823" id="Seg_8566" s="T822">ubaj-BI-n</ta>
            <ta e="T824" id="Seg_8567" s="T823">kördöː-A-BIn</ta>
            <ta e="T825" id="Seg_8568" s="T824">tu͡ok</ta>
            <ta e="T826" id="Seg_8569" s="T825">e-TI-tA=Ij</ta>
            <ta e="T827" id="Seg_8570" s="T826">ubaj-I-ŋ</ta>
            <ta e="T828" id="Seg_8571" s="T827">eː</ta>
            <ta e="T829" id="Seg_8572" s="T828">hette</ta>
            <ta e="T830" id="Seg_8573" s="T829">timek</ta>
            <ta e="T831" id="Seg_8574" s="T830">ičči-tA</ta>
            <ta e="T832" id="Seg_8575" s="T831">eː</ta>
            <ta e="T833" id="Seg_8576" s="T832">ol</ta>
            <ta e="T834" id="Seg_8577" s="T833">delemičeː</ta>
            <ta e="T835" id="Seg_8578" s="T834">üːr-An</ta>
            <ta e="T836" id="Seg_8579" s="T835">is-Ar</ta>
            <ta e="T837" id="Seg_8580" s="T836">ubaj-I-ŋ</ta>
            <ta e="T838" id="Seg_8581" s="T837">kaja</ta>
            <ta e="T839" id="Seg_8582" s="T838">min-GA</ta>
            <ta e="T840" id="Seg_8583" s="T839">kihi</ta>
            <ta e="T841" id="Seg_8584" s="T840">bu͡ol-IAK-ŋ</ta>
            <ta e="T842" id="Seg_8585" s="T841">haŋar-BAT</ta>
            <ta e="T843" id="Seg_8586" s="T842">bu͡ol-IAK-ŋ</ta>
            <ta e="T844" id="Seg_8587" s="T843">hu͡ok-tA</ta>
            <ta e="T845" id="Seg_8588" s="T844">bu</ta>
            <ta e="T846" id="Seg_8589" s="T845">kürej-I-m</ta>
            <ta e="T847" id="Seg_8590" s="T846">halaː-tI-GAr</ta>
            <ta e="T848" id="Seg_8591" s="T847">kaŋɨrgas</ta>
            <ta e="T849" id="Seg_8592" s="T848">gɨn-IAK-m</ta>
            <ta e="T850" id="Seg_8593" s="T849">höbüleː-A-s-Ar</ta>
            <ta e="T851" id="Seg_8594" s="T850">bili</ta>
            <ta e="T852" id="Seg_8595" s="T851">kihi</ta>
            <ta e="T853" id="Seg_8596" s="T852">ubaj-tI-GAr</ta>
            <ta e="T854" id="Seg_8597" s="T853">tij-Ar</ta>
            <ta e="T855" id="Seg_8598" s="T854">togo</ta>
            <ta e="T856" id="Seg_8599" s="T855">ölör-BAT-GIt</ta>
            <ta e="T857" id="Seg_8600" s="T856">ogo-GA</ta>
            <ta e="T858" id="Seg_8601" s="T857">bas</ta>
            <ta e="T859" id="Seg_8602" s="T858">berin-A-GIt</ta>
            <ta e="T860" id="Seg_8603" s="T859">du͡o</ta>
            <ta e="T861" id="Seg_8604" s="T860">hu͡ok</ta>
            <ta e="T862" id="Seg_8605" s="T861">keːs</ta>
            <ta e="T863" id="Seg_8606" s="T862">bultaː-IAK-GA</ta>
            <ta e="T864" id="Seg_8607" s="T863">naːda</ta>
            <ta e="T865" id="Seg_8608" s="T864">kosuːn-tA</ta>
            <ta e="T866" id="Seg_8609" s="T865">da</ta>
            <ta e="T867" id="Seg_8610" s="T866">bert</ta>
            <ta e="T868" id="Seg_8611" s="T867">dʼe</ta>
            <ta e="T869" id="Seg_8612" s="T868">bagalaː-IAK-BIt</ta>
            <ta e="T870" id="Seg_8613" s="T869">ki͡ehe</ta>
            <ta e="T871" id="Seg_8614" s="T870">tüs-Ar-LAr</ta>
            <ta e="T872" id="Seg_8615" s="T871">biːr</ta>
            <ta e="T873" id="Seg_8616" s="T872">hir-GA</ta>
            <ta e="T874" id="Seg_8617" s="T873">ahaː-Ar-LAr</ta>
            <ta e="T875" id="Seg_8618" s="T874">u͡ol</ta>
            <ta e="T876" id="Seg_8619" s="T875">dʼon-tI-n</ta>
            <ta e="T877" id="Seg_8620" s="T876">bat-An</ta>
            <ta e="T878" id="Seg_8621" s="T877">tahaːr-Ar</ta>
            <ta e="T879" id="Seg_8622" s="T878">ü͡ör</ta>
            <ta e="T880" id="Seg_8623" s="T879">keteː-A-t-A</ta>
            <ta e="T881" id="Seg_8624" s="T880">ogo</ta>
            <ta e="T882" id="Seg_8625" s="T881">utuj-An</ta>
            <ta e="T883" id="Seg_8626" s="T882">kaːl-Ar</ta>
            <ta e="T884" id="Seg_8627" s="T883">kihi-LAr</ta>
            <ta e="T885" id="Seg_8628" s="T884">kalɨj-Ar</ta>
            <ta e="T886" id="Seg_8629" s="T885">haŋa-LArI-n</ta>
            <ta e="T887" id="Seg_8630" s="T886">ihit-Ar</ta>
            <ta e="T888" id="Seg_8631" s="T887">kihi</ta>
            <ta e="T889" id="Seg_8632" s="T888">batɨja-nAn</ta>
            <ta e="T890" id="Seg_8633" s="T889">aːn-nI</ta>
            <ta e="T891" id="Seg_8634" s="T890">heget-Ar</ta>
            <ta e="T892" id="Seg_8635" s="T891">kaja</ta>
            <ta e="T893" id="Seg_8636" s="T892">togo</ta>
            <ta e="T894" id="Seg_8637" s="T893">keteː-A-GIt</ta>
            <ta e="T895" id="Seg_8638" s="T894">min-n</ta>
            <ta e="T896" id="Seg_8639" s="T895">ü͡ör-nI</ta>
            <ta e="T897" id="Seg_8640" s="T896">keteː-ŋ</ta>
            <ta e="T898" id="Seg_8641" s="T897">dʼon</ta>
            <ta e="T899" id="Seg_8642" s="T898">töttörü</ta>
            <ta e="T900" id="Seg_8643" s="T899">has-Ar-LAr</ta>
            <ta e="T901" id="Seg_8644" s="T900">harsi͡erdaːŋŋɨ</ta>
            <ta e="T902" id="Seg_8645" s="T901">uː-tI-GAr</ta>
            <ta e="T903" id="Seg_8646" s="T902">bagalaː-IAK-BIt</ta>
            <ta e="T904" id="Seg_8647" s="T903">di͡e-A-s-Ar-LAr</ta>
            <ta e="T905" id="Seg_8648" s="T904">emi͡e</ta>
            <ta e="T906" id="Seg_8649" s="T905">bürüːrdeː-Ar-LAr</ta>
            <ta e="T907" id="Seg_8650" s="T906">emi͡e</ta>
            <ta e="T908" id="Seg_8651" s="T907">tu͡ok-nI</ta>
            <ta e="T909" id="Seg_8652" s="T908">keteː-A-GIt</ta>
            <ta e="T910" id="Seg_8653" s="T909">bar-I-ŋ</ta>
            <ta e="T911" id="Seg_8654" s="T910">bat-An</ta>
            <ta e="T912" id="Seg_8655" s="T911">ɨːt-Ar</ta>
            <ta e="T913" id="Seg_8656" s="T912">hol-ttAn</ta>
            <ta e="T914" id="Seg_8657" s="T913">tɨːt-BAtAK-LArA</ta>
            <ta e="T915" id="Seg_8658" s="T914">ikki</ta>
            <ta e="T916" id="Seg_8659" s="T915">ɨj-LArA</ta>
            <ta e="T917" id="Seg_8660" s="T916">bɨs-t-Ar-tI-GAr</ta>
            <ta e="T918" id="Seg_8661" s="T917">dʼirbiː-LArA</ta>
            <ta e="T919" id="Seg_8662" s="T918">ele-tA</ta>
            <ta e="T920" id="Seg_8663" s="T919">bu͡ol-Ar</ta>
            <ta e="T921" id="Seg_8664" s="T920">kör-BIT-tA</ta>
            <ta e="T922" id="Seg_8665" s="T921">hir</ta>
            <ta e="T923" id="Seg_8666" s="T922">bu͡ol-BIT</ta>
            <ta e="T924" id="Seg_8667" s="T923">kaːr</ta>
            <ta e="T925" id="Seg_8668" s="T924">alɨn-tI-ttAn</ta>
            <ta e="T926" id="Seg_8669" s="T925">buru͡o-kAːN-kɨn-tA</ta>
            <ta e="T927" id="Seg_8670" s="T926">mu͡ora</ta>
            <ta e="T928" id="Seg_8671" s="T927">di͡ekki-ttAn</ta>
            <ta e="T929" id="Seg_8672" s="T928">kütür</ta>
            <ta e="T930" id="Seg_8673" s="T929">bagajɨ</ta>
            <ta e="T931" id="Seg_8674" s="T930">orok</ta>
            <ta e="T932" id="Seg_8675" s="T931">kel-Ar</ta>
            <ta e="T933" id="Seg_8676" s="T932">hir</ta>
            <ta e="T934" id="Seg_8677" s="T933">is-tI-n</ta>
            <ta e="T935" id="Seg_8678" s="T934">di͡ekki</ta>
            <ta e="T936" id="Seg_8679" s="T935">bar-Ar</ta>
            <ta e="T937" id="Seg_8680" s="T936">orok</ta>
            <ta e="T938" id="Seg_8681" s="T937">bar-An</ta>
            <ta e="T939" id="Seg_8682" s="T938">is-Ar-LAr</ta>
            <ta e="T940" id="Seg_8683" s="T939">pireːme</ta>
            <ta e="T941" id="Seg_8684" s="T940">noru͡ot-ttAn</ta>
            <ta e="T942" id="Seg_8685" s="T941">noru͡ot</ta>
            <ta e="T943" id="Seg_8686" s="T942">dʼi͡e-ttAn</ta>
            <ta e="T944" id="Seg_8687" s="T943">dʼi͡e</ta>
            <ta e="T945" id="Seg_8688" s="T944">hir-GA</ta>
            <ta e="T946" id="Seg_8689" s="T945">kel-Ar-LAr</ta>
            <ta e="T947" id="Seg_8690" s="T946">biːr</ta>
            <ta e="T948" id="Seg_8691" s="T947">dʼi͡e</ta>
            <ta e="T949" id="Seg_8692" s="T948">tas-tI-GAr</ta>
            <ta e="T950" id="Seg_8693" s="T949">toktoː-Ar</ta>
            <ta e="T951" id="Seg_8694" s="T950">biːr</ta>
            <ta e="T952" id="Seg_8695" s="T951">kɨrdʼagas</ta>
            <ta e="T953" id="Seg_8696" s="T952">hogus</ta>
            <ta e="T954" id="Seg_8697" s="T953">dʼaktar</ta>
            <ta e="T955" id="Seg_8698" s="T954">tagɨs-Ar</ta>
            <ta e="T956" id="Seg_8699" s="T955">baj</ta>
            <ta e="T957" id="Seg_8700" s="T956">taba-tA</ta>
            <ta e="T958" id="Seg_8701" s="T957">oduː</ta>
            <ta e="T959" id="Seg_8702" s="T958">taba</ta>
            <ta e="T960" id="Seg_8703" s="T959">bihigi</ta>
            <ta e="T961" id="Seg_8704" s="T960">törüt</ta>
            <ta e="T962" id="Seg_8705" s="T961">taba-BIt</ta>
            <ta e="T963" id="Seg_8706" s="T962">kurduk=Ij</ta>
            <ta e="T964" id="Seg_8707" s="T963">bɨlɨr</ta>
            <ta e="T965" id="Seg_8708" s="T964">ikki</ta>
            <ta e="T966" id="Seg_8709" s="T965">kihi</ta>
            <ta e="T967" id="Seg_8710" s="T966">bar-BIT-tA</ta>
            <ta e="T968" id="Seg_8711" s="T967">ol</ta>
            <ta e="T969" id="Seg_8712" s="T968">törüt-LArA</ta>
            <ta e="T970" id="Seg_8713" s="T969">bu͡olu͡o</ta>
            <ta e="T971" id="Seg_8714" s="T970">üs</ta>
            <ta e="T972" id="Seg_8715" s="T971">dʼi͡e-ttAn</ta>
            <ta e="T973" id="Seg_8716" s="T972">üs</ta>
            <ta e="T974" id="Seg_8717" s="T973">dʼaktar</ta>
            <ta e="T975" id="Seg_8718" s="T974">tagɨs-An</ta>
            <ta e="T976" id="Seg_8719" s="T975">emi͡e</ta>
            <ta e="T977" id="Seg_8720" s="T976">taːj-BIT-LAr</ta>
            <ta e="T978" id="Seg_8721" s="T977">törüt-LArI-n</ta>
            <ta e="T979" id="Seg_8722" s="T978">kepset-An</ta>
            <ta e="T980" id="Seg_8723" s="T979">dʼe</ta>
            <ta e="T981" id="Seg_8724" s="T980">bil-s-Ar-LAr</ta>
            <ta e="T982" id="Seg_8725" s="T981">uruː-LArI-n</ta>
            <ta e="T983" id="Seg_8726" s="T982">bul-s-Ar-LAr</ta>
            <ta e="T984" id="Seg_8727" s="T983">Kuŋadʼaj-nI</ta>
            <ta e="T985" id="Seg_8728" s="T984">köhüt-Ar-LAr</ta>
            <ta e="T986" id="Seg_8729" s="T985">üs-Is</ta>
            <ta e="T987" id="Seg_8730" s="T986">kün-tI-n</ta>
            <ta e="T988" id="Seg_8731" s="T987">orto-tA</ta>
            <ta e="T989" id="Seg_8732" s="T988">Kuŋadʼaj</ta>
            <ta e="T990" id="Seg_8733" s="T989">kel-Ar</ta>
            <ta e="T991" id="Seg_8734" s="T990">et</ta>
            <ta e="T992" id="Seg_8735" s="T991">aːt-tA</ta>
            <ta e="T993" id="Seg_8736" s="T992">bütün-tA</ta>
            <ta e="T994" id="Seg_8737" s="T993">hu͡ok</ta>
            <ta e="T995" id="Seg_8738" s="T994">tiriː-tI-n</ta>
            <ta e="T996" id="Seg_8739" s="T995">aŋar-tA</ta>
            <ta e="T997" id="Seg_8740" s="T996">kaːl-BIT</ta>
            <ta e="T998" id="Seg_8741" s="T997">ogo-m</ta>
            <ta e="T999" id="Seg_8742" s="T998">kel-BIT-GIn</ta>
            <ta e="T1000" id="Seg_8743" s="T999">dʼe</ta>
            <ta e="T1001" id="Seg_8744" s="T1000">üčügej</ta>
            <ta e="T1002" id="Seg_8745" s="T1001">kuhagan</ta>
            <ta e="T1003" id="Seg_8746" s="T1002">dʼon-nI</ta>
            <ta e="T1004" id="Seg_8747" s="T1003">baraː-An</ta>
            <ta e="T1005" id="Seg_8748" s="T1004">dojdu</ta>
            <ta e="T1006" id="Seg_8749" s="T1005">bult-tI-n</ta>
            <ta e="T1007" id="Seg_8750" s="T1006">arɨːččɨ</ta>
            <ta e="T1008" id="Seg_8751" s="T1007">kɨ͡aj-TI-m</ta>
            <ta e="T1009" id="Seg_8752" s="T1008">tɨːn-I-m</ta>
            <ta e="T1010" id="Seg_8753" s="T1009">ere</ta>
            <ta e="T1011" id="Seg_8754" s="T1010">kel-TI-m</ta>
            <ta e="T1012" id="Seg_8755" s="T1011">gojobuːn</ta>
            <ta e="T1013" id="Seg_8756" s="T1012">oŋor-An-LAr</ta>
            <ta e="T1014" id="Seg_8757" s="T1013">dʼe</ta>
            <ta e="T1015" id="Seg_8758" s="T1014">olor</ta>
            <ta e="T1016" id="Seg_8759" s="T1015">tu͡ok-ttAn</ta>
            <ta e="T1017" id="Seg_8760" s="T1016">da</ta>
            <ta e="T1018" id="Seg_8761" s="T1017">kuttan-I-m</ta>
            <ta e="T1019" id="Seg_8762" s="T1018">Kuŋadʼaj</ta>
            <ta e="T1020" id="Seg_8763" s="T1019">dʼe</ta>
            <ta e="T1021" id="Seg_8764" s="T1020">kojut</ta>
            <ta e="T1022" id="Seg_8765" s="T1021">tilin-Ar</ta>
            <ta e="T1023" id="Seg_8766" s="T1022">u͡ol</ta>
            <ta e="T1024" id="Seg_8767" s="T1023">küheːjin</ta>
            <ta e="T1025" id="Seg_8768" s="T1024">bu͡ol-An</ta>
            <ta e="T1026" id="Seg_8769" s="T1025">olor-BIT-tA</ta>
            <ta e="T1027" id="Seg_8770" s="T1026">bu</ta>
            <ta e="T1028" id="Seg_8771" s="T1027">omuk-GA</ta>
            <ta e="T1029" id="Seg_8772" s="T1028">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_8773" s="T0">three</ta>
            <ta e="T2" id="Seg_8774" s="T1">brother-PL.[NOM]</ta>
            <ta e="T3" id="Seg_8775" s="T2">live-PST2-3PL</ta>
            <ta e="T4" id="Seg_8776" s="T3">one-3PL.[NOM]</ta>
            <ta e="T5" id="Seg_8777" s="T4">small</ta>
            <ta e="T6" id="Seg_8778" s="T5">tiny</ta>
            <ta e="T7" id="Seg_8779" s="T6">human.being.[NOM]</ta>
            <ta e="T8" id="Seg_8780" s="T7">middle-3PL.[NOM]</ta>
            <ta e="T9" id="Seg_8781" s="T8">walk-NEG.[3SG]</ta>
            <ta e="T10" id="Seg_8782" s="T9">knee-3SG-INSTR</ta>
            <ta e="T11" id="Seg_8783" s="T10">only</ta>
            <ta e="T12" id="Seg_8784" s="T11">crawl-PRS.[3SG]</ta>
            <ta e="T13" id="Seg_8785" s="T12">Kungadyay.[NOM]</ta>
            <ta e="T14" id="Seg_8786" s="T13">say-CVB.SEQ</ta>
            <ta e="T15" id="Seg_8787" s="T14">elder.brother-3PL.[NOM]</ta>
            <ta e="T16" id="Seg_8788" s="T15">name-POSS</ta>
            <ta e="T17" id="Seg_8789" s="T16">NEG</ta>
            <ta e="T18" id="Seg_8790" s="T17">hunter.[NOM]</ta>
            <ta e="T19" id="Seg_8791" s="T18">woman-PROPR.[NOM]</ta>
            <ta e="T20" id="Seg_8792" s="T19">one</ta>
            <ta e="T21" id="Seg_8793" s="T20">fox.[NOM]</ta>
            <ta e="T22" id="Seg_8794" s="T21">child-PROPR.[NOM]</ta>
            <ta e="T23" id="Seg_8795" s="T22">reindeer-3PL.[NOM]</ta>
            <ta e="T24" id="Seg_8796" s="T23">EMPH-lonely.[NOM]</ta>
            <ta e="T25" id="Seg_8797" s="T24">horse.[NOM]</ta>
            <ta e="T26" id="Seg_8798" s="T25">similar</ta>
            <ta e="T27" id="Seg_8799" s="T26">time-DAT/LOC</ta>
            <ta e="T28" id="Seg_8800" s="T27">harness-NEG-3PL</ta>
            <ta e="T29" id="Seg_8801" s="T28">nomadize-TEMP-3PL</ta>
            <ta e="T30" id="Seg_8802" s="T29">just</ta>
            <ta e="T31" id="Seg_8803" s="T30">tent-3PL-ACC</ta>
            <ta e="T32" id="Seg_8804" s="T31">pull-CAUS-PRS-3PL</ta>
            <ta e="T33" id="Seg_8805" s="T32">elder.brother-3PL.[NOM]</ta>
            <ta e="T34" id="Seg_8806" s="T33">name-PROPR</ta>
            <ta e="T35" id="Seg_8807" s="T34">hunter.[NOM]</ta>
            <ta e="T36" id="Seg_8808" s="T35">wild.reindeer-ACC</ta>
            <ta e="T37" id="Seg_8809" s="T36">hunt-PRS.[3SG]</ta>
            <ta e="T38" id="Seg_8810" s="T37">fish-ACC</ta>
            <ta e="T39" id="Seg_8811" s="T38">EMPH</ta>
            <ta e="T40" id="Seg_8812" s="T39">year.[NOM]</ta>
            <ta e="T41" id="Seg_8813" s="T40">spring-INCH-PTCP.PRS-3SG-ACC</ta>
            <ta e="T42" id="Seg_8814" s="T41">during</ta>
            <ta e="T43" id="Seg_8815" s="T42">elder.brother-3PL.[NOM]</ta>
            <ta e="T44" id="Seg_8816" s="T43">be.sick-PRS.[3SG]</ta>
            <ta e="T45" id="Seg_8817" s="T44">intensify-PST2.[3SG]</ta>
            <ta e="T46" id="Seg_8818" s="T45">three-ORD</ta>
            <ta e="T47" id="Seg_8819" s="T46">day-3SG-DAT/LOC</ta>
            <ta e="T48" id="Seg_8820" s="T47">completely</ta>
            <ta e="T49" id="Seg_8821" s="T48">weaken-PST2.[3SG]</ta>
            <ta e="T50" id="Seg_8822" s="T49">legacy.[NOM]</ta>
            <ta e="T51" id="Seg_8823" s="T50">speak-PST2.[3SG]</ta>
            <ta e="T52" id="Seg_8824" s="T51">woman-3SG-DAT/LOC</ta>
            <ta e="T53" id="Seg_8825" s="T52">well</ta>
            <ta e="T54" id="Seg_8826" s="T53">1SG.[NOM]</ta>
            <ta e="T55" id="Seg_8827" s="T54">die-PST1-1SG</ta>
            <ta e="T56" id="Seg_8828" s="T55">who-EP-2SG.[NOM]</ta>
            <ta e="T57" id="Seg_8829" s="T56">care.about-FUT-3SG=Q</ta>
            <ta e="T58" id="Seg_8830" s="T57">hunt-PTCP.PST</ta>
            <ta e="T59" id="Seg_8831" s="T58">haul-1SG-ACC</ta>
            <ta e="T60" id="Seg_8832" s="T59">summer-ACC</ta>
            <ta e="T61" id="Seg_8833" s="T60">during</ta>
            <ta e="T62" id="Seg_8834" s="T61">eat-FUT-2PL</ta>
            <ta e="T63" id="Seg_8835" s="T62">food-2SG-ACC</ta>
            <ta e="T64" id="Seg_8836" s="T63">spend-TEMP-2SG</ta>
            <ta e="T65" id="Seg_8837" s="T64">coming.year.[NOM]</ta>
            <ta e="T66" id="Seg_8838" s="T65">skin-3SG-ACC</ta>
            <ta e="T67" id="Seg_8839" s="T66">leather.from.reindeer.leg-3SG-ACC</ta>
            <ta e="T68" id="Seg_8840" s="T67">thin.layer.of.leather-VBZ-CVB.SEQ</ta>
            <ta e="T69" id="Seg_8841" s="T68">soup-VBZ-CVB.SEQ</ta>
            <ta e="T70" id="Seg_8842" s="T69">go-FUT.[IMP.2SG]</ta>
            <ta e="T71" id="Seg_8843" s="T70">that.[NOM]</ta>
            <ta e="T72" id="Seg_8844" s="T71">family-EP-2SG.[NOM]</ta>
            <ta e="T73" id="Seg_8845" s="T72">use-POSS</ta>
            <ta e="T74" id="Seg_8846" s="T73">NEG</ta>
            <ta e="T75" id="Seg_8847" s="T74">family.[NOM]</ta>
            <ta e="T76" id="Seg_8848" s="T75">die-PTCP.PST-2SG-INSTR</ta>
            <ta e="T77" id="Seg_8849" s="T76">die-PRS-2SG</ta>
            <ta e="T78" id="Seg_8850" s="T77">child-1SG.[NOM]</ta>
            <ta e="T79" id="Seg_8851" s="T78">maybe</ta>
            <ta e="T80" id="Seg_8852" s="T79">human.being.[NOM]</ta>
            <ta e="T81" id="Seg_8853" s="T80">be-FUT.[3SG]</ta>
            <ta e="T82" id="Seg_8854" s="T81">die-NEG-TEMP-3SG</ta>
            <ta e="T83" id="Seg_8855" s="T82">3SG.[NOM]</ta>
            <ta e="T84" id="Seg_8856" s="T83">feed-FUT.[3SG]</ta>
            <ta e="T85" id="Seg_8857" s="T84">last-3SG.[NOM]</ta>
            <ta e="T86" id="Seg_8858" s="T85">die-CVB.SEQ</ta>
            <ta e="T87" id="Seg_8859" s="T86">stay-PRS.[3SG]</ta>
            <ta e="T88" id="Seg_8860" s="T87">cry-EP-MED-CVB.SIM-cry-EP-MED-CVB.SIM</ta>
            <ta e="T89" id="Seg_8861" s="T88">woman.[NOM]</ta>
            <ta e="T90" id="Seg_8862" s="T89">lay-CVB.SEQ</ta>
            <ta e="T91" id="Seg_8863" s="T90">throw-PRS.[3SG]</ta>
            <ta e="T92" id="Seg_8864" s="T91">this</ta>
            <ta e="T93" id="Seg_8865" s="T92">summer-3SG.[NOM]</ta>
            <ta e="T94" id="Seg_8866" s="T93">become-PRS.[3SG]</ta>
            <ta e="T95" id="Seg_8867" s="T94">food-3PL-ACC</ta>
            <ta e="T96" id="Seg_8868" s="T95">get.dry-EP-CAUS-EP-MED-PRS-3PL</ta>
            <ta e="T97" id="Seg_8869" s="T96">summer-3PL-ACC</ta>
            <ta e="T98" id="Seg_8870" s="T97">come.through-PRS-3PL</ta>
            <ta e="T99" id="Seg_8871" s="T98">autumn-3PL-DAT/LOC</ta>
            <ta e="T100" id="Seg_8872" s="T99">leather.from.reindeer.leg-3PL-ACC</ta>
            <ta e="T101" id="Seg_8873" s="T100">skin-3PL-ACC</ta>
            <ta e="T102" id="Seg_8874" s="T101">eat-PRS-3PL</ta>
            <ta e="T103" id="Seg_8875" s="T102">every-3SG.[NOM]</ta>
            <ta e="T104" id="Seg_8876" s="T103">run.out-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_8877" s="T104">winter.[NOM]</ta>
            <ta e="T106" id="Seg_8878" s="T105">become-PRS.[3SG]</ta>
            <ta e="T107" id="Seg_8879" s="T106">eat-PTCP.FUT</ta>
            <ta e="T108" id="Seg_8880" s="T107">matter-3PL.[NOM]</ta>
            <ta e="T109" id="Seg_8881" s="T108">NEG.EX</ta>
            <ta e="T110" id="Seg_8882" s="T109">wind-POSS</ta>
            <ta e="T111" id="Seg_8883" s="T110">NEG</ta>
            <ta e="T112" id="Seg_8884" s="T111">day.[NOM]</ta>
            <ta e="T113" id="Seg_8885" s="T112">only</ta>
            <ta e="T114" id="Seg_8886" s="T113">sledge-PROPR</ta>
            <ta e="T115" id="Seg_8887" s="T114">sound-3SG-ACC</ta>
            <ta e="T116" id="Seg_8888" s="T115">hear-PRS.[3SG]</ta>
            <ta e="T117" id="Seg_8889" s="T116">woman.[NOM]</ta>
            <ta e="T118" id="Seg_8890" s="T117">go.out-CVB.SIM</ta>
            <ta e="T119" id="Seg_8891" s="T118">run-PRS.[3SG]</ta>
            <ta e="T120" id="Seg_8892" s="T119">voice.[NOM]</ta>
            <ta e="T121" id="Seg_8893" s="T120">just</ta>
            <ta e="T122" id="Seg_8894" s="T121">be.heard-PTCP.PRS</ta>
            <ta e="T123" id="Seg_8895" s="T122">place-3SG-DAT/LOC</ta>
            <ta e="T124" id="Seg_8896" s="T123">stop-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_8897" s="T124">only</ta>
            <ta e="T126" id="Seg_8898" s="T125">woman.[NOM]</ta>
            <ta e="T127" id="Seg_8899" s="T126">father-3SG.[NOM]</ta>
            <ta e="T128" id="Seg_8900" s="T127">eight</ta>
            <ta e="T129" id="Seg_8901" s="T128">black.reindeer.[NOM]</ta>
            <ta e="T130" id="Seg_8902" s="T129">master-3SG.[NOM]</ta>
            <ta e="T131" id="Seg_8903" s="T130">man.of.the.house-3SG.[NOM]</ta>
            <ta e="T132" id="Seg_8904" s="T131">prince.[NOM]</ta>
            <ta e="T133" id="Seg_8905" s="T132">be-PST2.[3SG]</ta>
            <ta e="T134" id="Seg_8906" s="T133">hey</ta>
            <ta e="T135" id="Seg_8907" s="T134">how</ta>
            <ta e="T136" id="Seg_8908" s="T135">live-PRS-2PL</ta>
            <ta e="T137" id="Seg_8909" s="T136">man-EP-1SG.[NOM]</ta>
            <ta e="T138" id="Seg_8910" s="T137">die-PST2-3SG</ta>
            <ta e="T139" id="Seg_8911" s="T138">last.year.[NOM]</ta>
            <ta e="T140" id="Seg_8912" s="T139">hey</ta>
            <ta e="T141" id="Seg_8913" s="T140">thank.god</ta>
            <ta e="T142" id="Seg_8914" s="T141">very</ta>
            <ta e="T143" id="Seg_8915" s="T142">be.happy-PST1-1SG</ta>
            <ta e="T144" id="Seg_8916" s="T143">dress.[IMP.2SG]</ta>
            <ta e="T145" id="Seg_8917" s="T144">go-IMP.1DU</ta>
            <ta e="T146" id="Seg_8918" s="T145">1SG.[NOM]</ta>
            <ta e="T147" id="Seg_8919" s="T146">house-1SG-DAT/LOC</ta>
            <ta e="T148" id="Seg_8920" s="T147">well</ta>
            <ta e="T149" id="Seg_8921" s="T148">child-1SG-ACC</ta>
            <ta e="T150" id="Seg_8922" s="T149">family-1SG-ACC</ta>
            <ta e="T151" id="Seg_8923" s="T150">hunger-CVB.SEQ</ta>
            <ta e="T152" id="Seg_8924" s="T151">die-IMP.3PL</ta>
            <ta e="T153" id="Seg_8925" s="T152">go-IMP.1DU</ta>
            <ta e="T154" id="Seg_8926" s="T153">woman.[NOM]</ta>
            <ta e="T155" id="Seg_8927" s="T154">dress-CVB.SEQ</ta>
            <ta e="T156" id="Seg_8928" s="T155">reach-CVB.SEQ</ta>
            <ta e="T157" id="Seg_8929" s="T156">ride-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_8930" s="T157">and</ta>
            <ta e="T159" id="Seg_8931" s="T158">run-CVB.SEQ</ta>
            <ta e="T160" id="Seg_8932" s="T159">stay-PRS-3PL</ta>
            <ta e="T161" id="Seg_8933" s="T160">Kungadyay-PROPR</ta>
            <ta e="T162" id="Seg_8934" s="T161">strongly</ta>
            <ta e="T163" id="Seg_8935" s="T162">hungry.[NOM]</ta>
            <ta e="T164" id="Seg_8936" s="T163">lie-INFER-3PL</ta>
            <ta e="T165" id="Seg_8937" s="T164">polar.night.[NOM]</ta>
            <ta e="T166" id="Seg_8938" s="T165">pass.by-CVB.SEQ</ta>
            <ta e="T167" id="Seg_8939" s="T166">get.light-PTCP.PRS-DAT/LOC</ta>
            <ta e="T168" id="Seg_8940" s="T167">go-PRS.[3SG]</ta>
            <ta e="T169" id="Seg_8941" s="T168">Kungadyay.[NOM]</ta>
            <ta e="T170" id="Seg_8942" s="T169">younger.brother-EP-3SG-DAT/LOC</ta>
            <ta e="T171" id="Seg_8943" s="T170">speak-PRS.[3SG]</ta>
            <ta e="T172" id="Seg_8944" s="T171">reindeer-2SG-ACC</ta>
            <ta e="T173" id="Seg_8945" s="T172">harness.[IMP.2SG]</ta>
            <ta e="T174" id="Seg_8946" s="T173">lie-PTCP.PST-EP-INSTR</ta>
            <ta e="T175" id="Seg_8947" s="T174">die-FUT-1PL</ta>
            <ta e="T176" id="Seg_8948" s="T175">eight</ta>
            <ta e="T177" id="Seg_8949" s="T176">black.reindeer-ACC</ta>
            <ta e="T178" id="Seg_8950" s="T177">follow-IMP.1DU</ta>
            <ta e="T179" id="Seg_8951" s="T178">reindeer-3PL-ACC</ta>
            <ta e="T180" id="Seg_8952" s="T179">harness-CVB.SEQ</ta>
            <ta e="T181" id="Seg_8953" s="T180">tent-3PL-ACC</ta>
            <ta e="T182" id="Seg_8954" s="T181">demount-CVB.SEQ</ta>
            <ta e="T183" id="Seg_8955" s="T182">ride-CVB.SEQ</ta>
            <ta e="T184" id="Seg_8956" s="T183">hit-EP-CAUS-CVB.SEQ</ta>
            <ta e="T185" id="Seg_8957" s="T184">go-INFER-3PL</ta>
            <ta e="T186" id="Seg_8958" s="T185">how.much</ta>
            <ta e="T187" id="Seg_8959" s="T186">ten-MLTP</ta>
            <ta e="T188" id="Seg_8960" s="T187">nomadize-PST2-3PL</ta>
            <ta e="T189" id="Seg_8961" s="T188">once</ta>
            <ta e="T190" id="Seg_8962" s="T189">high</ta>
            <ta e="T191" id="Seg_8963" s="T190">forested.mountain-DAT/LOC</ta>
            <ta e="T192" id="Seg_8964" s="T191">climb-EP-PST2-3PL</ta>
            <ta e="T193" id="Seg_8965" s="T192">look.at-PST2-3PL</ta>
            <ta e="T194" id="Seg_8966" s="T193">very.big-PROPR</ta>
            <ta e="T195" id="Seg_8967" s="T194">broad.plain.[NOM]</ta>
            <ta e="T196" id="Seg_8968" s="T195">broad.plain.[NOM]</ta>
            <ta e="T197" id="Seg_8969" s="T196">middle-3SG-DAT/LOC</ta>
            <ta e="T198" id="Seg_8970" s="T197">herd.[NOM]</ta>
            <ta e="T199" id="Seg_8971" s="T198">a.lot</ta>
            <ta e="T200" id="Seg_8972" s="T199">broad.plain.[NOM]</ta>
            <ta e="T201" id="Seg_8973" s="T200">on.the.other.side</ta>
            <ta e="T202" id="Seg_8974" s="T201">seven</ta>
            <ta e="T203" id="Seg_8975" s="T202">ten</ta>
            <ta e="T204" id="Seg_8976" s="T203">tent.[NOM]</ta>
            <ta e="T205" id="Seg_8977" s="T204">MOD</ta>
            <ta e="T206" id="Seg_8978" s="T205">to.be.on.view-PRS.[3SG]</ta>
            <ta e="T207" id="Seg_8979" s="T206">go-PST2-3PL</ta>
            <ta e="T208" id="Seg_8980" s="T207">herd.[NOM]</ta>
            <ta e="T209" id="Seg_8981" s="T208">inside-3SG-INSTR</ta>
            <ta e="T210" id="Seg_8982" s="T209">herd.[NOM]</ta>
            <ta e="T211" id="Seg_8983" s="T210">other.of.two</ta>
            <ta e="T212" id="Seg_8984" s="T211">side-3SG-DAT/LOC</ta>
            <ta e="T213" id="Seg_8985" s="T212">stand.[IMP.2SG]</ta>
            <ta e="T214" id="Seg_8986" s="T213">say-PST2.[3SG]</ta>
            <ta e="T215" id="Seg_8987" s="T214">Kungadyay.[NOM]</ta>
            <ta e="T216" id="Seg_8988" s="T215">that.[NOM]</ta>
            <ta e="T217" id="Seg_8989" s="T216">tent-PL-ABL</ta>
            <ta e="T218" id="Seg_8990" s="T217">one</ta>
            <ta e="T219" id="Seg_8991" s="T218">better-3PL-DAT/LOC</ta>
            <ta e="T220" id="Seg_8992" s="T219">stop-FUT.[IMP.2SG]</ta>
            <ta e="T221" id="Seg_8993" s="T220">place.for.chopping.wood-3PL-ACC</ta>
            <ta e="T222" id="Seg_8994" s="T221">that.side.[NOM]</ta>
            <ta e="T223" id="Seg_8995" s="T222">side-DIM-3SG-DAT/LOC</ta>
            <ta e="T224" id="Seg_8996" s="T223">see-PST2-3SG</ta>
            <ta e="T225" id="Seg_8997" s="T224">one</ta>
            <ta e="T226" id="Seg_8998" s="T225">house.[NOM]</ta>
            <ta e="T227" id="Seg_8999" s="T226">every-3PL-ABL</ta>
            <ta e="T228" id="Seg_9000" s="T227">big.[NOM]</ta>
            <ta e="T229" id="Seg_9001" s="T228">entrance.[NOM]</ta>
            <ta e="T230" id="Seg_9002" s="T229">side-3SG-DAT/LOC</ta>
            <ta e="T231" id="Seg_9003" s="T230">reach-PST2-3PL</ta>
            <ta e="T232" id="Seg_9004" s="T231">and</ta>
            <ta e="T233" id="Seg_9005" s="T232">reindeer-3PL-ACC</ta>
            <ta e="T234" id="Seg_9006" s="T233">release-CVB.SEQ</ta>
            <ta e="T235" id="Seg_9007" s="T234">throw-PRS-3PL</ta>
            <ta e="T236" id="Seg_9008" s="T235">tent.[NOM]</ta>
            <ta e="T237" id="Seg_9009" s="T236">build.up-CVB.SEQ</ta>
            <ta e="T238" id="Seg_9010" s="T237">throw-PST2-3PL</ta>
            <ta e="T239" id="Seg_9011" s="T238">light-EP-PST2-3PL</ta>
            <ta e="T240" id="Seg_9012" s="T239">younger.brother-EP-3SG-ACC</ta>
            <ta e="T241" id="Seg_9013" s="T240">go.[IMP.2SG]</ta>
            <ta e="T242" id="Seg_9014" s="T241">sister_in_law-2SG-DAT/LOC</ta>
            <ta e="T243" id="Seg_9015" s="T242">child-3SG-ACC</ta>
            <ta e="T244" id="Seg_9016" s="T243">suck-CAUS-CVB.SIM</ta>
            <ta e="T245" id="Seg_9017" s="T244">come-IMP.3SG</ta>
            <ta e="T246" id="Seg_9018" s="T245">eight</ta>
            <ta e="T247" id="Seg_9019" s="T246">black.reindeer-DAT/LOC</ta>
            <ta e="T248" id="Seg_9020" s="T247">speak.[IMP.2SG]</ta>
            <ta e="T249" id="Seg_9021" s="T248">die-PST1-1PL</ta>
            <ta e="T250" id="Seg_9022" s="T249">one</ta>
            <ta e="T251" id="Seg_9023" s="T250">INDEF</ta>
            <ta e="T252" id="Seg_9024" s="T251">heart.[NOM]</ta>
            <ta e="T253" id="Seg_9025" s="T252">half-DIM-3SG-ACC</ta>
            <ta e="T254" id="Seg_9026" s="T253">with</ta>
            <ta e="T255" id="Seg_9027" s="T254">liver.[NOM]</ta>
            <ta e="T256" id="Seg_9028" s="T255">widower-3SG-ACC</ta>
            <ta e="T257" id="Seg_9029" s="T256">share-IMP.3SG</ta>
            <ta e="T258" id="Seg_9030" s="T257">say-PRS.[3SG]</ta>
            <ta e="T259" id="Seg_9031" s="T258">boy.[NOM]</ta>
            <ta e="T260" id="Seg_9032" s="T259">run-CVB.SEQ</ta>
            <ta e="T261" id="Seg_9033" s="T260">go.in-CVB.SEQ</ta>
            <ta e="T262" id="Seg_9034" s="T261">speak-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_9035" s="T262">see.[IMP.2SG]</ta>
            <ta e="T264" id="Seg_9036" s="T263">just</ta>
            <ta e="T265" id="Seg_9037" s="T264">suck-CAUS-PTCP.PRS</ta>
            <ta e="T266" id="Seg_9038" s="T265">mess.[NOM]</ta>
            <ta e="T267" id="Seg_9039" s="T266">hunger-CVB.SEQ</ta>
            <ta e="T268" id="Seg_9040" s="T267">die-IMP.3SG</ta>
            <ta e="T269" id="Seg_9041" s="T268">say-PRS.[3SG]</ta>
            <ta e="T270" id="Seg_9042" s="T269">woman.[NOM]</ta>
            <ta e="T271" id="Seg_9043" s="T270">well</ta>
            <ta e="T272" id="Seg_9044" s="T271">grandfather</ta>
            <ta e="T273" id="Seg_9045" s="T272">message-VBZ-PST1-3SG</ta>
            <ta e="T274" id="Seg_9046" s="T273">elder.brother-EP-1SG.[NOM]</ta>
            <ta e="T275" id="Seg_9047" s="T274">die-PST1-1PL</ta>
            <ta e="T276" id="Seg_9048" s="T275">one</ta>
            <ta e="T277" id="Seg_9049" s="T276">INDEF</ta>
            <ta e="T278" id="Seg_9050" s="T277">heart.[NOM]</ta>
            <ta e="T279" id="Seg_9051" s="T278">half-DIM-3SG-ACC</ta>
            <ta e="T280" id="Seg_9052" s="T279">with</ta>
            <ta e="T281" id="Seg_9053" s="T280">liver.[NOM]</ta>
            <ta e="T282" id="Seg_9054" s="T281">widower-3SG-ACC</ta>
            <ta e="T283" id="Seg_9055" s="T282">share-IMP.3SG</ta>
            <ta e="T284" id="Seg_9056" s="T283">how</ta>
            <ta e="T285" id="Seg_9057" s="T284">hunger-CVB.SEQ</ta>
            <ta e="T286" id="Seg_9058" s="T285">die-EP-IMP.2PL</ta>
            <ta e="T287" id="Seg_9059" s="T286">send-NEG-1SG</ta>
            <ta e="T288" id="Seg_9060" s="T287">say-PRS.[3SG]</ta>
            <ta e="T289" id="Seg_9061" s="T288">eight</ta>
            <ta e="T290" id="Seg_9062" s="T289">black.reindeer.[NOM]</ta>
            <ta e="T291" id="Seg_9063" s="T290">master-3SG.[NOM]</ta>
            <ta e="T292" id="Seg_9064" s="T291">sleep-EMOT-PRS-3PL</ta>
            <ta e="T293" id="Seg_9065" s="T292">what-ACC</ta>
            <ta e="T294" id="Seg_9066" s="T293">eat-FUT-3PL=Q</ta>
            <ta e="T295" id="Seg_9067" s="T294">suddenly</ta>
            <ta e="T296" id="Seg_9068" s="T295">reindeer-VBZ-PTCP.PRS</ta>
            <ta e="T297" id="Seg_9069" s="T296">voice.[NOM]</ta>
            <ta e="T298" id="Seg_9070" s="T297">be.heard-PRS.[3SG]</ta>
            <ta e="T299" id="Seg_9071" s="T298">Kungadyay.[NOM]</ta>
            <ta e="T300" id="Seg_9072" s="T299">go.out-CVB.SEQ</ta>
            <ta e="T301" id="Seg_9073" s="T300">pee-CVB.SIM</ta>
            <ta e="T302" id="Seg_9074" s="T301">stand.up-PST2.[3SG]</ta>
            <ta e="T303" id="Seg_9075" s="T302">knee-3SG-DAT/LOC</ta>
            <ta e="T304" id="Seg_9076" s="T303">reindeer.[NOM]</ta>
            <ta e="T305" id="Seg_9077" s="T304">a.lot</ta>
            <ta e="T306" id="Seg_9078" s="T305">come-PRS.[3SG]</ta>
            <ta e="T307" id="Seg_9079" s="T306">castrated</ta>
            <ta e="T308" id="Seg_9080" s="T307">male.reindeer.[NOM]</ta>
            <ta e="T309" id="Seg_9081" s="T308">fat.[NOM]</ta>
            <ta e="T310" id="Seg_9082" s="T309">very</ta>
            <ta e="T311" id="Seg_9083" s="T310">come-PRS.[3SG]</ta>
            <ta e="T312" id="Seg_9084" s="T311">boy.[NOM]</ta>
            <ta e="T313" id="Seg_9085" s="T312">catch-CVB.SEQ</ta>
            <ta e="T314" id="Seg_9086" s="T313">take-PRS.[3SG]</ta>
            <ta e="T315" id="Seg_9087" s="T314">throw.oneself-CVB.SEQ</ta>
            <ta e="T316" id="Seg_9088" s="T315">a.lot</ta>
            <ta e="T317" id="Seg_9089" s="T316">knife-3SG-ACC</ta>
            <ta e="T318" id="Seg_9090" s="T317">get-CAUS-PRS.[3SG]</ta>
            <ta e="T319" id="Seg_9091" s="T318">throat-3SG-ACC</ta>
            <ta e="T320" id="Seg_9092" s="T319">cut-CVB.SIM</ta>
            <ta e="T321" id="Seg_9093" s="T320">wipe-PRS.[3SG]</ta>
            <ta e="T322" id="Seg_9094" s="T321">skin-NEG.CVB.SIM</ta>
            <ta e="T323" id="Seg_9095" s="T322">NEG</ta>
            <ta e="T324" id="Seg_9096" s="T323">bring.in-PRS-3PL</ta>
            <ta e="T325" id="Seg_9097" s="T324">kettle-VBZ-CVB.SEQ</ta>
            <ta e="T326" id="Seg_9098" s="T325">well</ta>
            <ta e="T327" id="Seg_9099" s="T326">eat-PRS-3PL</ta>
            <ta e="T328" id="Seg_9100" s="T327">day-ACC</ta>
            <ta e="T329" id="Seg_9101" s="T328">whole</ta>
            <ta e="T330" id="Seg_9102" s="T329">evening-DAT/LOC</ta>
            <ta e="T331" id="Seg_9103" s="T330">until</ta>
            <ta e="T332" id="Seg_9104" s="T331">this</ta>
            <ta e="T333" id="Seg_9105" s="T332">tent.[NOM]</ta>
            <ta e="T334" id="Seg_9106" s="T333">edge-3SG-DAT/LOC</ta>
            <ta e="T335" id="Seg_9107" s="T334">small.[NOM]</ta>
            <ta e="T336" id="Seg_9108" s="T335">tent.[NOM]</ta>
            <ta e="T337" id="Seg_9109" s="T336">stand-PRS.[3SG]</ta>
            <ta e="T338" id="Seg_9110" s="T337">well</ta>
            <ta e="T339" id="Seg_9111" s="T338">seven</ta>
            <ta e="T340" id="Seg_9112" s="T339">button.[NOM]</ta>
            <ta e="T341" id="Seg_9113" s="T340">master-3SG-DAT/LOC</ta>
            <ta e="T342" id="Seg_9114" s="T341">go.[IMP.2SG]</ta>
            <ta e="T343" id="Seg_9115" s="T342">that.[NOM]</ta>
            <ta e="T344" id="Seg_9116" s="T343">tent-DAT/LOC</ta>
            <ta e="T345" id="Seg_9117" s="T344">village.elder.[NOM]</ta>
            <ta e="T346" id="Seg_9118" s="T345">probably</ta>
            <ta e="T347" id="Seg_9119" s="T346">meal-PART</ta>
            <ta e="T348" id="Seg_9120" s="T347">beg.[IMP.2SG]</ta>
            <ta e="T349" id="Seg_9121" s="T348">boy.[NOM]</ta>
            <ta e="T350" id="Seg_9122" s="T349">go-PRS.[3SG]</ta>
            <ta e="T351" id="Seg_9123" s="T350">seven</ta>
            <ta e="T352" id="Seg_9124" s="T351">button.[NOM]</ta>
            <ta e="T353" id="Seg_9125" s="T352">master-3SG-DAT/LOC</ta>
            <ta e="T354" id="Seg_9126" s="T353">speak-PRS.[3SG]</ta>
            <ta e="T355" id="Seg_9127" s="T354">message-ACC</ta>
            <ta e="T356" id="Seg_9128" s="T355">eight</ta>
            <ta e="T357" id="Seg_9129" s="T356">black.reindeer.[NOM]</ta>
            <ta e="T358" id="Seg_9130" s="T357">master-3SG.[NOM]</ta>
            <ta e="T359" id="Seg_9131" s="T358">give-PST1-3SG</ta>
            <ta e="T360" id="Seg_9132" s="T359">apparently</ta>
            <ta e="T361" id="Seg_9133" s="T360">no</ta>
            <ta e="T362" id="Seg_9134" s="T361">hey</ta>
            <ta e="T363" id="Seg_9135" s="T362">then</ta>
            <ta e="T364" id="Seg_9136" s="T363">1SG.[NOM]</ta>
            <ta e="T365" id="Seg_9137" s="T364">also</ta>
            <ta e="T366" id="Seg_9138" s="T365">give-NEG-1SG</ta>
            <ta e="T367" id="Seg_9139" s="T366">eat-PRS-3PL</ta>
            <ta e="T368" id="Seg_9140" s="T367">in.the.morning</ta>
            <ta e="T369" id="Seg_9141" s="T368">herd.[NOM]</ta>
            <ta e="T370" id="Seg_9142" s="T369">go-PRS.[3SG]</ta>
            <ta e="T371" id="Seg_9143" s="T370">again</ta>
            <ta e="T372" id="Seg_9144" s="T371">knee-3SG-INSTR</ta>
            <ta e="T373" id="Seg_9145" s="T372">crawl-CVB.SEQ</ta>
            <ta e="T374" id="Seg_9146" s="T373">go.out-PRS.[3SG]</ta>
            <ta e="T375" id="Seg_9147" s="T374">Kungadyay.[NOM]</ta>
            <ta e="T376" id="Seg_9148" s="T375">Kungadyay.[NOM]</ta>
            <ta e="T377" id="Seg_9149" s="T376">give.birth-NEG.PTCP</ta>
            <ta e="T378" id="Seg_9150" s="T377">infertile.female.reindeer-ACC</ta>
            <ta e="T379" id="Seg_9151" s="T378">grab-CVB.SEQ</ta>
            <ta e="T380" id="Seg_9152" s="T379">take-PRS.[3SG]</ta>
            <ta e="T381" id="Seg_9153" s="T380">thick.end.of.antler-3SG-ABL</ta>
            <ta e="T382" id="Seg_9154" s="T381">well=EMPH</ta>
            <ta e="T383" id="Seg_9155" s="T382">knife-ACC</ta>
            <ta e="T384" id="Seg_9156" s="T383">take.out.[IMP.2SG]</ta>
            <ta e="T385" id="Seg_9157" s="T384">kill-CVB.SEQ</ta>
            <ta e="T386" id="Seg_9158" s="T385">throw-PRS.[3SG]</ta>
            <ta e="T387" id="Seg_9159" s="T386">again</ta>
            <ta e="T388" id="Seg_9160" s="T387">skin-NEG.CVB.SIM</ta>
            <ta e="T389" id="Seg_9161" s="T388">eat-PRS-3PL</ta>
            <ta e="T390" id="Seg_9162" s="T389">what.kind.of</ta>
            <ta e="T391" id="Seg_9163" s="T390">this</ta>
            <ta e="T392" id="Seg_9164" s="T391">exactly.this</ta>
            <ta e="T393" id="Seg_9165" s="T392">lord-PL.[NOM]</ta>
            <ta e="T394" id="Seg_9166" s="T393">own-3PL-ACC</ta>
            <ta e="T395" id="Seg_9167" s="T394">kill-PTCP.PST</ta>
            <ta e="T396" id="Seg_9168" s="T395">be-PST2.[3SG]</ta>
            <ta e="T397" id="Seg_9169" s="T396">fall.asleep-PRS-3PL</ta>
            <ta e="T398" id="Seg_9170" s="T397">in.the.morning</ta>
            <ta e="T399" id="Seg_9171" s="T398">Kungadyay.[NOM]</ta>
            <ta e="T400" id="Seg_9172" s="T399">go.out-EP-PST2-3SG</ta>
            <ta e="T401" id="Seg_9173" s="T400">eight</ta>
            <ta e="T402" id="Seg_9174" s="T401">black.reindeer.[NOM]</ta>
            <ta e="T403" id="Seg_9175" s="T402">man.of.the.house-3SG.[NOM]</ta>
            <ta e="T404" id="Seg_9176" s="T403">driving.pole-3SG-ACC</ta>
            <ta e="T405" id="Seg_9177" s="T404">carve-CVB.SIM</ta>
            <ta e="T406" id="Seg_9178" s="T405">sit-PRS.[3SG]</ta>
            <ta e="T407" id="Seg_9179" s="T406">hey</ta>
            <ta e="T408" id="Seg_9180" s="T407">eight</ta>
            <ta e="T409" id="Seg_9181" s="T408">black.reindeer.[NOM]</ta>
            <ta e="T410" id="Seg_9182" s="T409">master-3SG.[NOM]</ta>
            <ta e="T411" id="Seg_9183" s="T410">hello</ta>
            <ta e="T412" id="Seg_9184" s="T411">still</ta>
            <ta e="T413" id="Seg_9185" s="T412">hello</ta>
            <ta e="T414" id="Seg_9186" s="T413">today</ta>
            <ta e="T415" id="Seg_9187" s="T414">kill-ITER-PRS-1PL</ta>
            <ta e="T416" id="Seg_9188" s="T415">last</ta>
            <ta e="T417" id="Seg_9189" s="T416">reindeer-PL-1PL-ABL</ta>
            <ta e="T418" id="Seg_9190" s="T417">eat-CVB.SIM</ta>
            <ta e="T419" id="Seg_9191" s="T418">lie.down-PRS-2PL</ta>
            <ta e="T420" id="Seg_9192" s="T419">self-2PL.[NOM]</ta>
            <ta e="T421" id="Seg_9193" s="T420">guilt-2PL.[NOM]</ta>
            <ta e="T422" id="Seg_9194" s="T421">eight</ta>
            <ta e="T423" id="Seg_9195" s="T422">black.reindeer.[NOM]</ta>
            <ta e="T424" id="Seg_9196" s="T423">master-3SG.[NOM]</ta>
            <ta e="T425" id="Seg_9197" s="T424">knee-VBZ-CVB.SEQ</ta>
            <ta e="T426" id="Seg_9198" s="T425">stand-PTCP.PRS</ta>
            <ta e="T427" id="Seg_9199" s="T426">human.being-ACC</ta>
            <ta e="T428" id="Seg_9200" s="T427">driving.pole.[NOM]</ta>
            <ta e="T429" id="Seg_9201" s="T428">spear-3SG-INSTR</ta>
            <ta e="T430" id="Seg_9202" s="T429">hip-DAT/LOC</ta>
            <ta e="T431" id="Seg_9203" s="T430">beat-PRS.[3SG]</ta>
            <ta e="T432" id="Seg_9204" s="T431">and</ta>
            <ta e="T433" id="Seg_9205" s="T432">can.not-CVB.SEQ</ta>
            <ta e="T434" id="Seg_9206" s="T433">throw-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_9207" s="T434">Kungadyay-3SG.[NOM]</ta>
            <ta e="T436" id="Seg_9208" s="T435">snow.[NOM]</ta>
            <ta e="T437" id="Seg_9209" s="T436">lower.part-3SG-INSTR</ta>
            <ta e="T438" id="Seg_9210" s="T437">go-PST2.[3SG]</ta>
            <ta e="T439" id="Seg_9211" s="T438">seven</ta>
            <ta e="T440" id="Seg_9212" s="T439">button.[NOM]</ta>
            <ta e="T441" id="Seg_9213" s="T440">master-3SG.[NOM]</ta>
            <ta e="T442" id="Seg_9214" s="T441">driving.pole.[NOM]</ta>
            <ta e="T443" id="Seg_9215" s="T442">carve-CVB.SIM</ta>
            <ta e="T444" id="Seg_9216" s="T443">sit-TEMP-3SG</ta>
            <ta e="T445" id="Seg_9217" s="T444">go.out-PRS.[3SG]</ta>
            <ta e="T446" id="Seg_9218" s="T445">hey</ta>
            <ta e="T447" id="Seg_9219" s="T446">seven</ta>
            <ta e="T448" id="Seg_9220" s="T447">button.[NOM]</ta>
            <ta e="T449" id="Seg_9221" s="T448">master-3SG.[NOM]</ta>
            <ta e="T450" id="Seg_9222" s="T449">hello</ta>
            <ta e="T451" id="Seg_9223" s="T450">still</ta>
            <ta e="T452" id="Seg_9224" s="T451">hello</ta>
            <ta e="T453" id="Seg_9225" s="T452">today</ta>
            <ta e="T454" id="Seg_9226" s="T453">kill-ITER-PRS-1PL</ta>
            <ta e="T455" id="Seg_9227" s="T454">last</ta>
            <ta e="T456" id="Seg_9228" s="T455">reindeer-PL-1PL-ACC</ta>
            <ta e="T457" id="Seg_9229" s="T456">eat-CVB.SIM</ta>
            <ta e="T458" id="Seg_9230" s="T457">lie.down-PRS-2PL</ta>
            <ta e="T459" id="Seg_9231" s="T458">self-2PL.[NOM]</ta>
            <ta e="T460" id="Seg_9232" s="T459">guilt-2PL.[NOM]</ta>
            <ta e="T461" id="Seg_9233" s="T460">seven</ta>
            <ta e="T462" id="Seg_9234" s="T461">button.[NOM]</ta>
            <ta e="T463" id="Seg_9235" s="T462">master-3SG.[NOM]</ta>
            <ta e="T464" id="Seg_9236" s="T463">knee-VBZ-CVB.SEQ</ta>
            <ta e="T465" id="Seg_9237" s="T464">stand-PTCP.PRS</ta>
            <ta e="T466" id="Seg_9238" s="T465">human.being-ACC</ta>
            <ta e="T467" id="Seg_9239" s="T466">driving.pole.[NOM]</ta>
            <ta e="T468" id="Seg_9240" s="T467">spear-3SG-INSTR</ta>
            <ta e="T469" id="Seg_9241" s="T468">hip-DAT/LOC</ta>
            <ta e="T470" id="Seg_9242" s="T469">push-CVB.PURP</ta>
            <ta e="T471" id="Seg_9243" s="T470">earth-ACC</ta>
            <ta e="T472" id="Seg_9244" s="T471">beat-PST2.[3SG]</ta>
            <ta e="T473" id="Seg_9245" s="T472">when</ta>
            <ta e="T474" id="Seg_9246" s="T473">INDEF</ta>
            <ta e="T475" id="Seg_9247" s="T474">Kungadyay.[NOM]</ta>
            <ta e="T476" id="Seg_9248" s="T475">next</ta>
            <ta e="T477" id="Seg_9249" s="T476">place-EP-INSTR</ta>
            <ta e="T478" id="Seg_9250" s="T477">go.out-CVB.SIM</ta>
            <ta e="T479" id="Seg_9251" s="T478">run-PRS.[3SG]</ta>
            <ta e="T480" id="Seg_9252" s="T479">and</ta>
            <ta e="T481" id="Seg_9253" s="T480">two</ta>
            <ta e="T482" id="Seg_9254" s="T481">leg-3SG-DAT/LOC</ta>
            <ta e="T483" id="Seg_9255" s="T482">stand.up-CVB.SIM</ta>
            <ta e="T484" id="Seg_9256" s="T483">jump-PRS.[3SG]</ta>
            <ta e="T485" id="Seg_9257" s="T484">look.at-PST2-3SG</ta>
            <ta e="T486" id="Seg_9258" s="T485">reindeer.sledge.[NOM]</ta>
            <ta e="T487" id="Seg_9259" s="T486">go-CVB.SEQ</ta>
            <ta e="T488" id="Seg_9260" s="T487">be-PRS.[3SG]</ta>
            <ta e="T489" id="Seg_9261" s="T488">sister_in_law-3SG.[NOM]</ta>
            <ta e="T490" id="Seg_9262" s="T489">nomadize-CVB.SEQ</ta>
            <ta e="T491" id="Seg_9263" s="T490">be-PRS.[3SG]</ta>
            <ta e="T492" id="Seg_9264" s="T491">father-3SG.[NOM]</ta>
            <ta e="T493" id="Seg_9265" s="T492">family.[NOM]</ta>
            <ta e="T494" id="Seg_9266" s="T493">give-PST2.[3SG]</ta>
            <ta e="T495" id="Seg_9267" s="T494">Kungadyay.[NOM]</ta>
            <ta e="T496" id="Seg_9268" s="T495">sister_in_law-3SG-GEN</ta>
            <ta e="T497" id="Seg_9269" s="T496">rein-3SG-ACC</ta>
            <ta e="T498" id="Seg_9270" s="T497">catch-CVB.SEQ</ta>
            <ta e="T499" id="Seg_9271" s="T498">take-PRS.[3SG]</ta>
            <ta e="T500" id="Seg_9272" s="T499">release-NEG-1SG</ta>
            <ta e="T501" id="Seg_9273" s="T500">what.kind.of</ta>
            <ta e="T502" id="Seg_9274" s="T501">in.the.direction</ta>
            <ta e="T503" id="Seg_9275" s="T502">go-PRS-2SG</ta>
            <ta e="T504" id="Seg_9276" s="T503">woman.[NOM]</ta>
            <ta e="T505" id="Seg_9277" s="T504">run-CVB.SEQ</ta>
            <ta e="T506" id="Seg_9278" s="T505">stay-PRS.[3SG]</ta>
            <ta e="T507" id="Seg_9279" s="T506">Kungadyay.[NOM]</ta>
            <ta e="T508" id="Seg_9280" s="T507">chase-CVB.SIM</ta>
            <ta e="T509" id="Seg_9281" s="T508">follow-ITER-CVB.SEQ</ta>
            <ta e="T510" id="Seg_9282" s="T509">caravan-3SG-ACC</ta>
            <ta e="T511" id="Seg_9283" s="T510">carry-CVB.SIM</ta>
            <ta e="T512" id="Seg_9284" s="T511">stay-PRS.[3SG]</ta>
            <ta e="T513" id="Seg_9285" s="T512">Kungadyay.[NOM]</ta>
            <ta e="T514" id="Seg_9286" s="T513">eight</ta>
            <ta e="T515" id="Seg_9287" s="T514">black.reindeer.[NOM]</ta>
            <ta e="T516" id="Seg_9288" s="T515">master-3SG-DAT/LOC</ta>
            <ta e="T517" id="Seg_9289" s="T516">go.in-PRS.[3SG]</ta>
            <ta e="T518" id="Seg_9290" s="T517">well</ta>
            <ta e="T519" id="Seg_9291" s="T518">eight</ta>
            <ta e="T520" id="Seg_9292" s="T519">black.reindeer.[NOM]</ta>
            <ta e="T521" id="Seg_9293" s="T520">master-3SG.[NOM]</ta>
            <ta e="T522" id="Seg_9294" s="T521">peace-PROPR.[NOM]</ta>
            <ta e="T523" id="Seg_9295" s="T522">be-PTCP.COND-DAT/LOC</ta>
            <ta e="T524" id="Seg_9296" s="T523">younger.brother-PL-1SG-ACC</ta>
            <ta e="T525" id="Seg_9297" s="T524">care.about-CVB.SEQ</ta>
            <ta e="T526" id="Seg_9298" s="T525">sit.[IMP.2SG]</ta>
            <ta e="T527" id="Seg_9299" s="T526">bad-ADVZ</ta>
            <ta e="T528" id="Seg_9300" s="T527">try-FUT-2SG</ta>
            <ta e="T529" id="Seg_9301" s="T528">one</ta>
            <ta e="T530" id="Seg_9302" s="T529">day-EP-INSTR</ta>
            <ta e="T531" id="Seg_9303" s="T530">NEG.EX</ta>
            <ta e="T532" id="Seg_9304" s="T531">make-CAUS-FUT-1SG</ta>
            <ta e="T533" id="Seg_9305" s="T532">sister_in_law-1SG-ACC</ta>
            <ta e="T534" id="Seg_9306" s="T533">follow-CVB.SIM</ta>
            <ta e="T535" id="Seg_9307" s="T534">go-PST1-1SG</ta>
            <ta e="T536" id="Seg_9308" s="T535">sister_in_law-3SG-ACC</ta>
            <ta e="T537" id="Seg_9309" s="T536">follow-CVB.SEQ</ta>
            <ta e="T538" id="Seg_9310" s="T537">lose-CVB.SEQ</ta>
            <ta e="T539" id="Seg_9311" s="T538">throw-PRS.[3SG]</ta>
            <ta e="T540" id="Seg_9312" s="T539">what.kind.of</ta>
            <ta e="T541" id="Seg_9313" s="T540">country.[NOM]</ta>
            <ta e="T542" id="Seg_9314" s="T541">end-3SG-DAT/LOC</ta>
            <ta e="T543" id="Seg_9315" s="T542">see-PRS.[3SG]</ta>
            <ta e="T544" id="Seg_9316" s="T543">reach-PTCP.FUT</ta>
            <ta e="T545" id="Seg_9317" s="T544">chase-NEG.[3SG]</ta>
            <ta e="T546" id="Seg_9318" s="T545">turn-CAUS-CVB.SEQ</ta>
            <ta e="T547" id="Seg_9319" s="T546">get-CVB.SEQ</ta>
            <ta e="T548" id="Seg_9320" s="T547">house-3SG-ACC</ta>
            <ta e="T549" id="Seg_9321" s="T548">to</ta>
            <ta e="T550" id="Seg_9322" s="T549">block-PRS.[3SG]</ta>
            <ta e="T551" id="Seg_9323" s="T550">grab-PTCP.FUT.[NOM]</ta>
            <ta e="T552" id="Seg_9324" s="T551">and</ta>
            <ta e="T553" id="Seg_9325" s="T552">despite</ta>
            <ta e="T554" id="Seg_9326" s="T553">grab-NEG.CVB.SIM</ta>
            <ta e="T555" id="Seg_9327" s="T554">sister_in_law-3SG-GEN</ta>
            <ta e="T556" id="Seg_9328" s="T555">back-3SG-ABL</ta>
            <ta e="T557" id="Seg_9329" s="T556">go.in-PRS.[3SG]</ta>
            <ta e="T558" id="Seg_9330" s="T557">sister_in_law-3SG.[NOM]</ta>
            <ta e="T559" id="Seg_9331" s="T558">child-3SG-ACC</ta>
            <ta e="T560" id="Seg_9332" s="T559">bosoms-VBZ-CVB.SIM</ta>
            <ta e="T561" id="Seg_9333" s="T560">sit-PRS.[3SG]</ta>
            <ta e="T562" id="Seg_9334" s="T561">younger.brother-3PL.[NOM]</ta>
            <ta e="T563" id="Seg_9335" s="T562">eat-CVB.SIM</ta>
            <ta e="T564" id="Seg_9336" s="T563">sit-PRS.[3SG]</ta>
            <ta e="T565" id="Seg_9337" s="T564">well</ta>
            <ta e="T566" id="Seg_9338" s="T565">sister_in_law.[NOM]</ta>
            <ta e="T567" id="Seg_9339" s="T566">guilt-2SG-ACC</ta>
            <ta e="T568" id="Seg_9340" s="T567">pass.by-EP-CAUS-PST1-2SG</ta>
            <ta e="T569" id="Seg_9341" s="T568">child-2SG-ACC</ta>
            <ta e="T570" id="Seg_9342" s="T569">take-NEG.PTCP.PST-EP-2SG.[NOM]</ta>
            <ta e="T571" id="Seg_9343" s="T570">be-COND.[3SG]</ta>
            <ta e="T572" id="Seg_9344" s="T571">kill-PTCP.FUT</ta>
            <ta e="T573" id="Seg_9345" s="T572">be-PST1-1SG</ta>
            <ta e="T574" id="Seg_9346" s="T573">eight</ta>
            <ta e="T575" id="Seg_9347" s="T574">black.reindeer.[NOM]</ta>
            <ta e="T576" id="Seg_9348" s="T575">master-3SG.[NOM]</ta>
            <ta e="T577" id="Seg_9349" s="T576">be.afraid-CVB.PURP</ta>
            <ta e="T578" id="Seg_9350" s="T577">be.afraid-PRS.[3SG]</ta>
            <ta e="T579" id="Seg_9351" s="T578">live-PRS-3PL</ta>
            <ta e="T580" id="Seg_9352" s="T579">when</ta>
            <ta e="T581" id="Seg_9353" s="T580">INDEF</ta>
            <ta e="T582" id="Seg_9354" s="T581">three-ORD</ta>
            <ta e="T583" id="Seg_9355" s="T582">day-3SG-DAT/LOC</ta>
            <ta e="T584" id="Seg_9356" s="T583">well</ta>
            <ta e="T585" id="Seg_9357" s="T584">eight</ta>
            <ta e="T586" id="Seg_9358" s="T585">black.reindeer.[NOM]</ta>
            <ta e="T587" id="Seg_9359" s="T586">master-3SG.[NOM]</ta>
            <ta e="T588" id="Seg_9360" s="T587">people-2SG-ACC</ta>
            <ta e="T589" id="Seg_9361" s="T588">gather.[IMP.2SG]</ta>
            <ta e="T590" id="Seg_9362" s="T589">gathering-VBZ-FUT-1PL</ta>
            <ta e="T591" id="Seg_9363" s="T590">say-PRS.[3SG]</ta>
            <ta e="T592" id="Seg_9364" s="T591">Kungadyay.[NOM]</ta>
            <ta e="T593" id="Seg_9365" s="T592">well</ta>
            <ta e="T594" id="Seg_9366" s="T593">gather-PRS-3PL</ta>
            <ta e="T595" id="Seg_9367" s="T594">seven</ta>
            <ta e="T596" id="Seg_9368" s="T595">ten</ta>
            <ta e="T597" id="Seg_9369" s="T596">tent.[NOM]</ta>
            <ta e="T598" id="Seg_9370" s="T597">human.being-3SG-ACC</ta>
            <ta e="T599" id="Seg_9371" s="T598">Kungadyay.[NOM]</ta>
            <ta e="T600" id="Seg_9372" s="T599">speak-PRS.[3SG]</ta>
            <ta e="T601" id="Seg_9373" s="T600">2PL.[NOM]</ta>
            <ta e="T602" id="Seg_9374" s="T601">two</ta>
            <ta e="T603" id="Seg_9375" s="T602">man.of.the.house.[NOM]</ta>
            <ta e="T604" id="Seg_9376" s="T603">there.is-2PL</ta>
            <ta e="T605" id="Seg_9377" s="T604">eight</ta>
            <ta e="T606" id="Seg_9378" s="T605">black.reindeer.[NOM]</ta>
            <ta e="T607" id="Seg_9379" s="T606">man.of.the.house-3SG.[NOM]</ta>
            <ta e="T608" id="Seg_9380" s="T607">and</ta>
            <ta e="T609" id="Seg_9381" s="T608">seven</ta>
            <ta e="T610" id="Seg_9382" s="T609">button.[NOM]</ta>
            <ta e="T611" id="Seg_9383" s="T610">master-3SG.[NOM]</ta>
            <ta e="T612" id="Seg_9384" s="T611">now</ta>
            <ta e="T613" id="Seg_9385" s="T612">man.of.the.house.[NOM]</ta>
            <ta e="T614" id="Seg_9386" s="T613">be-CVB.SEQ</ta>
            <ta e="T615" id="Seg_9387" s="T614">stop-PST1-2PL</ta>
            <ta e="T616" id="Seg_9388" s="T615">man.of.the.house-2PL.[NOM]</ta>
            <ta e="T617" id="Seg_9389" s="T616">that.[NOM]</ta>
            <ta e="T618" id="Seg_9390" s="T617">boy.[NOM]</ta>
            <ta e="T619" id="Seg_9391" s="T618">child.[NOM]</ta>
            <ta e="T620" id="Seg_9392" s="T619">elder.brother-EP-1SG.[NOM]</ta>
            <ta e="T621" id="Seg_9393" s="T620">child-3SG.[NOM]</ta>
            <ta e="T622" id="Seg_9394" s="T621">gather-REFL-EP-IMP.2PL</ta>
            <ta e="T623" id="Seg_9395" s="T622">every-2PL.[NOM]</ta>
            <ta e="T624" id="Seg_9396" s="T623">two</ta>
            <ta e="T625" id="Seg_9397" s="T624">day-DAT/LOC</ta>
            <ta e="T626" id="Seg_9398" s="T625">three-ORD</ta>
            <ta e="T627" id="Seg_9399" s="T626">day-2PL-DAT/LOC</ta>
            <ta e="T628" id="Seg_9400" s="T627">start-FUT-2PL</ta>
            <ta e="T629" id="Seg_9401" s="T628">leading-2PL.[NOM]</ta>
            <ta e="T630" id="Seg_9402" s="T629">3SG.[NOM]</ta>
            <ta e="T631" id="Seg_9403" s="T630">be-FUT.[3SG]</ta>
            <ta e="T632" id="Seg_9404" s="T631">foreign</ta>
            <ta e="T633" id="Seg_9405" s="T632">reindeer-ACC</ta>
            <ta e="T634" id="Seg_9406" s="T633">harness-EP-NEG.[IMP.2SG]</ta>
            <ta e="T635" id="Seg_9407" s="T634">father-2SG.[NOM]</ta>
            <ta e="T636" id="Seg_9408" s="T635">reindeer-3SG-ACC</ta>
            <ta e="T637" id="Seg_9409" s="T636">legacy-3SG-ACC</ta>
            <ta e="T638" id="Seg_9410" s="T637">harness-FUT.[IMP.2SG]</ta>
            <ta e="T639" id="Seg_9411" s="T638">say-PRS.[3SG]</ta>
            <ta e="T640" id="Seg_9412" s="T639">boy-DAT/LOC</ta>
            <ta e="T641" id="Seg_9413" s="T640">boy.[NOM]</ta>
            <ta e="T642" id="Seg_9414" s="T641">grow-PST2.[3SG]</ta>
            <ta e="T643" id="Seg_9415" s="T642">lead-EP-CAUS-CVB.SEQ-2SG</ta>
            <ta e="T644" id="Seg_9416" s="T643">start-FUT-2PL</ta>
            <ta e="T645" id="Seg_9417" s="T644">forest.[NOM]</ta>
            <ta e="T646" id="Seg_9418" s="T645">big.as-3SG-INSTR</ta>
            <ta e="T647" id="Seg_9419" s="T646">crest.[NOM]</ta>
            <ta e="T648" id="Seg_9420" s="T647">there.is</ta>
            <ta e="T649" id="Seg_9421" s="T648">be-FUT.[3SG]</ta>
            <ta e="T650" id="Seg_9422" s="T649">this</ta>
            <ta e="T651" id="Seg_9423" s="T650">crest.[NOM]</ta>
            <ta e="T652" id="Seg_9424" s="T651">tundra.[NOM]</ta>
            <ta e="T653" id="Seg_9425" s="T652">in.the.direction</ta>
            <ta e="T654" id="Seg_9426" s="T653">side-3SG-INSTR</ta>
            <ta e="T655" id="Seg_9427" s="T654">nomadize-FUT-2SG</ta>
            <ta e="T656" id="Seg_9428" s="T655">two</ta>
            <ta e="T657" id="Seg_9429" s="T656">month-ACC</ta>
            <ta e="T658" id="Seg_9430" s="T657">during</ta>
            <ta e="T659" id="Seg_9431" s="T658">last-3SG.[NOM]</ta>
            <ta e="T660" id="Seg_9432" s="T659">though</ta>
            <ta e="T661" id="Seg_9433" s="T660">crest-3SG.[NOM]</ta>
            <ta e="T662" id="Seg_9434" s="T661">stop-TEMP-3SG</ta>
            <ta e="T663" id="Seg_9435" s="T662">tundra.[NOM]</ta>
            <ta e="T664" id="Seg_9436" s="T663">be-FUT.[3SG]</ta>
            <ta e="T665" id="Seg_9437" s="T664">snow-3SG-GEN</ta>
            <ta e="T666" id="Seg_9438" s="T665">lower.part-3SG-ABL</ta>
            <ta e="T667" id="Seg_9439" s="T666">smoke-PROPR.[NOM]</ta>
            <ta e="T668" id="Seg_9440" s="T667">be-FUT.[3SG]</ta>
            <ta e="T669" id="Seg_9441" s="T668">that.[NOM]</ta>
            <ta e="T670" id="Seg_9442" s="T669">1PL.[NOM]</ta>
            <ta e="T671" id="Seg_9443" s="T670">ancestor.[NOM]</ta>
            <ta e="T672" id="Seg_9444" s="T671">earth-1PL.[NOM]</ta>
            <ta e="T673" id="Seg_9445" s="T672">big</ta>
            <ta e="T674" id="Seg_9446" s="T673">way-DAT/LOC</ta>
            <ta e="T675" id="Seg_9447" s="T674">go.in-FUT-2SG</ta>
            <ta e="T676" id="Seg_9448" s="T675">relative-PL-2SG-DAT/LOC</ta>
            <ta e="T677" id="Seg_9449" s="T676">reach-FUT-2SG</ta>
            <ta e="T678" id="Seg_9450" s="T677">people-PL.[NOM]</ta>
            <ta e="T679" id="Seg_9451" s="T678">reindeer-2PL-ACC</ta>
            <ta e="T680" id="Seg_9452" s="T679">night-ACC-day-ACC</ta>
            <ta e="T681" id="Seg_9453" s="T680">during</ta>
            <ta e="T682" id="Seg_9454" s="T681">see-FUT-2PL</ta>
            <ta e="T683" id="Seg_9455" s="T682">eight</ta>
            <ta e="T684" id="Seg_9456" s="T683">black.reindeer.[NOM]</ta>
            <ta e="T685" id="Seg_9457" s="T684">man.of.the.house-3SG.[NOM]</ta>
            <ta e="T686" id="Seg_9458" s="T685">seven</ta>
            <ta e="T687" id="Seg_9459" s="T686">button.[NOM]</ta>
            <ta e="T688" id="Seg_9460" s="T687">master-3SG.[NOM]</ta>
            <ta e="T689" id="Seg_9461" s="T688">again</ta>
            <ta e="T690" id="Seg_9462" s="T689">meet-FUT-2PL</ta>
            <ta e="T691" id="Seg_9463" s="T690">1SG.[NOM]</ta>
            <ta e="T692" id="Seg_9464" s="T691">though</ta>
            <ta e="T693" id="Seg_9465" s="T692">this</ta>
            <ta e="T694" id="Seg_9466" s="T693">good-ADVZ</ta>
            <ta e="T695" id="Seg_9467" s="T694">travel-PTCP.PRS-2PL.[NOM]</ta>
            <ta e="T696" id="Seg_9468" s="T695">because.of</ta>
            <ta e="T697" id="Seg_9469" s="T696">bad</ta>
            <ta e="T698" id="Seg_9470" s="T697">people-ACC</ta>
            <ta e="T699" id="Seg_9471" s="T698">Changit-PL-ACC</ta>
            <ta e="T700" id="Seg_9472" s="T699">animal-PL-ACC</ta>
            <ta e="T701" id="Seg_9473" s="T700">NEG.EX</ta>
            <ta e="T702" id="Seg_9474" s="T701">make-CVB.SIM</ta>
            <ta e="T703" id="Seg_9475" s="T702">go-PST1-1SG</ta>
            <ta e="T704" id="Seg_9476" s="T703">that</ta>
            <ta e="T705" id="Seg_9477" s="T704">place-DAT/LOC</ta>
            <ta e="T706" id="Seg_9478" s="T705">reach-PTCP.PRS</ta>
            <ta e="T707" id="Seg_9479" s="T706">day-2SG-DAT/LOC</ta>
            <ta e="T708" id="Seg_9480" s="T707">reach-FUT-1SG</ta>
            <ta e="T709" id="Seg_9481" s="T708">big-ADVZ</ta>
            <ta e="T710" id="Seg_9482" s="T709">be.late-TEMP-1SG</ta>
            <ta e="T711" id="Seg_9483" s="T710">three-ORD</ta>
            <ta e="T712" id="Seg_9484" s="T711">day-3SG-DAT/LOC</ta>
            <ta e="T713" id="Seg_9485" s="T712">reach-FUT-1SG</ta>
            <ta e="T714" id="Seg_9486" s="T713">say-PRS.[3SG]</ta>
            <ta e="T715" id="Seg_9487" s="T714">three-ORD</ta>
            <ta e="T716" id="Seg_9488" s="T715">day-3SG-DAT/LOC</ta>
            <ta e="T717" id="Seg_9489" s="T716">start-PRS-3PL</ta>
            <ta e="T718" id="Seg_9490" s="T717">Kungadyay.[NOM]</ta>
            <ta e="T719" id="Seg_9491" s="T718">go.away-CVB.SEQ</ta>
            <ta e="T720" id="Seg_9492" s="T719">go-PRS.[3SG]</ta>
            <ta e="T721" id="Seg_9493" s="T720">completely</ta>
            <ta e="T722" id="Seg_9494" s="T721">by.foot</ta>
            <ta e="T723" id="Seg_9495" s="T722">this</ta>
            <ta e="T724" id="Seg_9496" s="T723">child.[NOM]</ta>
            <ta e="T725" id="Seg_9497" s="T724">leading.[NOM]</ta>
            <ta e="T726" id="Seg_9498" s="T725">be-CVB.SEQ</ta>
            <ta e="T727" id="Seg_9499" s="T726">nomadize-CVB.SEQ</ta>
            <ta e="T728" id="Seg_9500" s="T727">drive-PST1-3PL</ta>
            <ta e="T729" id="Seg_9501" s="T728">guard-EP-MED-CVB.SEQ</ta>
            <ta e="T730" id="Seg_9502" s="T729">travel-PRS-3PL</ta>
            <ta e="T731" id="Seg_9503" s="T730">lord-ACC-mistress-ACC</ta>
            <ta e="T732" id="Seg_9504" s="T731">whole-3SG-ACC</ta>
            <ta e="T733" id="Seg_9505" s="T732">guard-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T734" id="Seg_9506" s="T733">this</ta>
            <ta e="T735" id="Seg_9507" s="T734">boy.[NOM]</ta>
            <ta e="T736" id="Seg_9508" s="T735">month-3SG-GEN</ta>
            <ta e="T737" id="Seg_9509" s="T736">middle-3SG.[NOM]</ta>
            <ta e="T738" id="Seg_9510" s="T737">become-PST2.[3SG]</ta>
            <ta e="T739" id="Seg_9511" s="T738">crest-3SG-ACC</ta>
            <ta e="T740" id="Seg_9512" s="T739">follow-CVB.SEQ</ta>
            <ta e="T741" id="Seg_9513" s="T740">go-INFER-3SG</ta>
            <ta e="T742" id="Seg_9514" s="T741">suddenly</ta>
            <ta e="T743" id="Seg_9515" s="T742">tundra.[NOM]</ta>
            <ta e="T744" id="Seg_9516" s="T743">in.the.direction</ta>
            <ta e="T745" id="Seg_9517" s="T744">crest.[NOM]</ta>
            <ta e="T746" id="Seg_9518" s="T745">go.away-PRS.[3SG]</ta>
            <ta e="T747" id="Seg_9519" s="T746">upper.part-3SG-DAT/LOC</ta>
            <ta e="T748" id="Seg_9520" s="T747">human.being.[NOM]</ta>
            <ta e="T749" id="Seg_9521" s="T748">go-PRS.[3SG]</ta>
            <ta e="T750" id="Seg_9522" s="T749">what.kind.of</ta>
            <ta e="T751" id="Seg_9523" s="T750">in.the.direction</ta>
            <ta e="T752" id="Seg_9524" s="T751">human.being-2SG=Q</ta>
            <ta e="T753" id="Seg_9525" s="T752">what</ta>
            <ta e="T754" id="Seg_9526" s="T753">come-FUT.[3SG]=Q</ta>
            <ta e="T755" id="Seg_9527" s="T754">tent-ACC-fire-ACC</ta>
            <ta e="T756" id="Seg_9528" s="T755">know-NEG-1SG</ta>
            <ta e="T757" id="Seg_9529" s="T756">be.born-PTCP.PST</ta>
            <ta e="T758" id="Seg_9530" s="T757">elder.brother-1SG-ACC</ta>
            <ta e="T759" id="Seg_9531" s="T758">lose-CVB.SIM</ta>
            <ta e="T760" id="Seg_9532" s="T759">go-PRS-1SG</ta>
            <ta e="T761" id="Seg_9533" s="T760">what.kind.of-3PL.[NOM]=Q</ta>
            <ta e="T762" id="Seg_9534" s="T761">that.[NOM]</ta>
            <ta e="T763" id="Seg_9535" s="T762">eight</ta>
            <ta e="T764" id="Seg_9536" s="T763">black.reindeer.[NOM]</ta>
            <ta e="T765" id="Seg_9537" s="T764">man.of.the.house-3SG.[NOM]</ta>
            <ta e="T766" id="Seg_9538" s="T765">yeah.right</ta>
            <ta e="T767" id="Seg_9539" s="T766">that.[NOM]</ta>
            <ta e="T768" id="Seg_9540" s="T767">1SG.[NOM]</ta>
            <ta e="T769" id="Seg_9541" s="T768">human.being-1SG.[NOM]</ta>
            <ta e="T770" id="Seg_9542" s="T769">that.[NOM]</ta>
            <ta e="T771" id="Seg_9543" s="T770">free.running.domestic.reindeer.[NOM]</ta>
            <ta e="T772" id="Seg_9544" s="T771">hunt-CVB.SEQ</ta>
            <ta e="T773" id="Seg_9545" s="T772">go-PRS.[3SG]</ta>
            <ta e="T774" id="Seg_9546" s="T773">well</ta>
            <ta e="T775" id="Seg_9547" s="T774">then</ta>
            <ta e="T776" id="Seg_9548" s="T775">1SG-DAT/LOC</ta>
            <ta e="T777" id="Seg_9549" s="T776">human.being.[NOM]</ta>
            <ta e="T778" id="Seg_9550" s="T777">be.[IMP.2SG]</ta>
            <ta e="T779" id="Seg_9551" s="T778">speak-CVB.SIM</ta>
            <ta e="T780" id="Seg_9552" s="T779">fetch-NEG.[3SG]</ta>
            <ta e="T781" id="Seg_9553" s="T780">if</ta>
            <ta e="T782" id="Seg_9554" s="T781">human.being.[NOM]</ta>
            <ta e="T783" id="Seg_9555" s="T782">be-FUT-2SG</ta>
            <ta e="T784" id="Seg_9556" s="T783">NEG-3SG</ta>
            <ta e="T785" id="Seg_9557" s="T784">this</ta>
            <ta e="T786" id="Seg_9558" s="T785">three</ta>
            <ta e="T787" id="Seg_9559" s="T786">branch-PROPR</ta>
            <ta e="T788" id="Seg_9560" s="T787">driving.pole-EP-1SG.[NOM]</ta>
            <ta e="T789" id="Seg_9561" s="T788">other.of.two</ta>
            <ta e="T790" id="Seg_9562" s="T789">branch-3SG.[NOM]</ta>
            <ta e="T791" id="Seg_9563" s="T790">ornament-3SG.[NOM]</ta>
            <ta e="T792" id="Seg_9564" s="T791">be-FUT-2SG</ta>
            <ta e="T793" id="Seg_9565" s="T792">two-MLTP</ta>
            <ta e="T794" id="Seg_9566" s="T793">hero.[NOM]</ta>
            <ta e="T795" id="Seg_9567" s="T794">skullcap-3SG.[NOM]</ta>
            <ta e="T796" id="Seg_9568" s="T795">ornament-PROPR.[NOM]</ta>
            <ta e="T797" id="Seg_9569" s="T796">human.being.[NOM]</ta>
            <ta e="T798" id="Seg_9570" s="T797">agree-EP-MED-PRS.[3SG]</ta>
            <ta e="T799" id="Seg_9571" s="T798">come.along-PRS.[3SG]</ta>
            <ta e="T800" id="Seg_9572" s="T799">nomadize-CVB.SEQ</ta>
            <ta e="T801" id="Seg_9573" s="T800">drive-PST1-3PL</ta>
            <ta e="T802" id="Seg_9574" s="T801">month-3PL.[NOM]</ta>
            <ta e="T803" id="Seg_9575" s="T802">end-PST1-3SG</ta>
            <ta e="T804" id="Seg_9576" s="T803">next</ta>
            <ta e="T805" id="Seg_9577" s="T804">month.[NOM]</ta>
            <ta e="T806" id="Seg_9578" s="T805">middle-3SG.[NOM]</ta>
            <ta e="T807" id="Seg_9579" s="T806">become-PST1-3SG</ta>
            <ta e="T808" id="Seg_9580" s="T807">again</ta>
            <ta e="T809" id="Seg_9581" s="T808">human.being-ACC</ta>
            <ta e="T810" id="Seg_9582" s="T809">see-PST1-3PL</ta>
            <ta e="T811" id="Seg_9583" s="T810">one</ta>
            <ta e="T812" id="Seg_9584" s="T811">crest-DIM-DAT/LOC</ta>
            <ta e="T813" id="Seg_9585" s="T812">also</ta>
            <ta e="T814" id="Seg_9586" s="T813">hello-VBZ-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T815" id="Seg_9587" s="T814">what.kind.of</ta>
            <ta e="T816" id="Seg_9588" s="T815">in.the.direction</ta>
            <ta e="T817" id="Seg_9589" s="T816">tent-PROPR-fire-PROPR</ta>
            <ta e="T818" id="Seg_9590" s="T817">human.being-2SG=Q</ta>
            <ta e="T819" id="Seg_9591" s="T818">tent.[NOM]-fire.[NOM]</ta>
            <ta e="T820" id="Seg_9592" s="T819">NEG.EX</ta>
            <ta e="T821" id="Seg_9593" s="T820">go-PRS-1SG</ta>
            <ta e="T822" id="Seg_9594" s="T821">only</ta>
            <ta e="T823" id="Seg_9595" s="T822">elder.brother-1SG-ACC</ta>
            <ta e="T824" id="Seg_9596" s="T823">search-PRS-1SG</ta>
            <ta e="T825" id="Seg_9597" s="T824">what.[NOM]</ta>
            <ta e="T826" id="Seg_9598" s="T825">be-PST1-3SG=Q</ta>
            <ta e="T827" id="Seg_9599" s="T826">elder.brother-EP-2SG.[NOM]</ta>
            <ta e="T828" id="Seg_9600" s="T827">hey</ta>
            <ta e="T829" id="Seg_9601" s="T828">seven</ta>
            <ta e="T830" id="Seg_9602" s="T829">button.[NOM]</ta>
            <ta e="T831" id="Seg_9603" s="T830">master-3SG.[NOM]</ta>
            <ta e="T832" id="Seg_9604" s="T831">hey</ta>
            <ta e="T833" id="Seg_9605" s="T832">that</ta>
            <ta e="T834" id="Seg_9606" s="T833">free.running.domestic.reindeer.[NOM]</ta>
            <ta e="T835" id="Seg_9607" s="T834">hunt-CVB.SEQ</ta>
            <ta e="T836" id="Seg_9608" s="T835">go-PRS.[3SG]</ta>
            <ta e="T837" id="Seg_9609" s="T836">elder.brother-EP-2SG.[NOM]</ta>
            <ta e="T838" id="Seg_9610" s="T837">well</ta>
            <ta e="T839" id="Seg_9611" s="T838">1SG-DAT/LOC</ta>
            <ta e="T840" id="Seg_9612" s="T839">human.being.[NOM]</ta>
            <ta e="T841" id="Seg_9613" s="T840">be-FUT-2SG</ta>
            <ta e="T842" id="Seg_9614" s="T841">say-NEG.[3SG]</ta>
            <ta e="T843" id="Seg_9615" s="T842">be-FUT-2SG</ta>
            <ta e="T844" id="Seg_9616" s="T843">NEG-3SG</ta>
            <ta e="T845" id="Seg_9617" s="T844">this</ta>
            <ta e="T846" id="Seg_9618" s="T845">driving.pole-EP-1SG.[NOM]</ta>
            <ta e="T847" id="Seg_9619" s="T846">branch-3SG-DAT/LOC</ta>
            <ta e="T848" id="Seg_9620" s="T847">ornament.[NOM]</ta>
            <ta e="T849" id="Seg_9621" s="T848">make-FUT-1SG</ta>
            <ta e="T850" id="Seg_9622" s="T849">agree-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T851" id="Seg_9623" s="T850">exactly.this</ta>
            <ta e="T852" id="Seg_9624" s="T851">human.being.[NOM]</ta>
            <ta e="T853" id="Seg_9625" s="T852">elder.brother-3SG-DAT/LOC</ta>
            <ta e="T854" id="Seg_9626" s="T853">reach-PRS.[3SG]</ta>
            <ta e="T855" id="Seg_9627" s="T854">why</ta>
            <ta e="T856" id="Seg_9628" s="T855">kill-NEG-2PL</ta>
            <ta e="T857" id="Seg_9629" s="T856">child-DAT/LOC</ta>
            <ta e="T858" id="Seg_9630" s="T857">head.[NOM]</ta>
            <ta e="T859" id="Seg_9631" s="T858">obey-PRS-2PL</ta>
            <ta e="T860" id="Seg_9632" s="T859">Q</ta>
            <ta e="T861" id="Seg_9633" s="T860">no</ta>
            <ta e="T862" id="Seg_9634" s="T861">let.[IMP.2SG]</ta>
            <ta e="T863" id="Seg_9635" s="T862">hunt-PTCP.FUT-DAT/LOC</ta>
            <ta e="T864" id="Seg_9636" s="T863">need.to</ta>
            <ta e="T865" id="Seg_9637" s="T864">brave-3SG.[NOM]</ta>
            <ta e="T866" id="Seg_9638" s="T865">and</ta>
            <ta e="T867" id="Seg_9639" s="T866">very</ta>
            <ta e="T868" id="Seg_9640" s="T867">well</ta>
            <ta e="T869" id="Seg_9641" s="T868">try-FUT-1PL</ta>
            <ta e="T870" id="Seg_9642" s="T869">in.the.evening</ta>
            <ta e="T871" id="Seg_9643" s="T870">go.down-PRS-3PL</ta>
            <ta e="T872" id="Seg_9644" s="T871">one</ta>
            <ta e="T873" id="Seg_9645" s="T872">place-DAT/LOC</ta>
            <ta e="T874" id="Seg_9646" s="T873">eat-PRS-3PL</ta>
            <ta e="T875" id="Seg_9647" s="T874">boy.[NOM]</ta>
            <ta e="T876" id="Seg_9648" s="T875">people-3SG-ACC</ta>
            <ta e="T877" id="Seg_9649" s="T876">follow-CVB.SEQ</ta>
            <ta e="T878" id="Seg_9650" s="T877">take.out-PRS.[3SG]</ta>
            <ta e="T879" id="Seg_9651" s="T878">herd.[NOM]</ta>
            <ta e="T880" id="Seg_9652" s="T879">guard-EP-CAUS-CVB.SIM</ta>
            <ta e="T881" id="Seg_9653" s="T880">child.[NOM]</ta>
            <ta e="T882" id="Seg_9654" s="T881">sleep-CVB.SEQ</ta>
            <ta e="T883" id="Seg_9655" s="T882">stay-PRS.[3SG]</ta>
            <ta e="T884" id="Seg_9656" s="T883">human.being-PL.[NOM]</ta>
            <ta e="T885" id="Seg_9657" s="T884">stalk-PTCP.PRS</ta>
            <ta e="T886" id="Seg_9658" s="T885">voice-3PL-ACC</ta>
            <ta e="T887" id="Seg_9659" s="T886">hear-PRS.[3SG]</ta>
            <ta e="T888" id="Seg_9660" s="T887">human.being.[NOM]</ta>
            <ta e="T889" id="Seg_9661" s="T888">glaive-INSTR</ta>
            <ta e="T890" id="Seg_9662" s="T889">door-ACC</ta>
            <ta e="T891" id="Seg_9663" s="T890">open.slightly-PRS.[3SG]</ta>
            <ta e="T892" id="Seg_9664" s="T891">well</ta>
            <ta e="T893" id="Seg_9665" s="T892">why</ta>
            <ta e="T894" id="Seg_9666" s="T893">guard-PRS-2PL</ta>
            <ta e="T895" id="Seg_9667" s="T894">1SG-ACC</ta>
            <ta e="T896" id="Seg_9668" s="T895">herd-ACC</ta>
            <ta e="T897" id="Seg_9669" s="T896">guard-IMP.2PL</ta>
            <ta e="T898" id="Seg_9670" s="T897">people.[NOM]</ta>
            <ta e="T899" id="Seg_9671" s="T898">back</ta>
            <ta e="T900" id="Seg_9672" s="T899">hide-PRS-3PL</ta>
            <ta e="T901" id="Seg_9673" s="T900">morning</ta>
            <ta e="T902" id="Seg_9674" s="T901">sleep-3SG-DAT/LOC</ta>
            <ta e="T903" id="Seg_9675" s="T902">try-FUT-1PL</ta>
            <ta e="T904" id="Seg_9676" s="T903">say-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T905" id="Seg_9677" s="T904">again</ta>
            <ta e="T906" id="Seg_9678" s="T905">try-PRS-3PL</ta>
            <ta e="T907" id="Seg_9679" s="T906">again</ta>
            <ta e="T908" id="Seg_9680" s="T907">what-ACC</ta>
            <ta e="T909" id="Seg_9681" s="T908">guard-PRS-2PL</ta>
            <ta e="T910" id="Seg_9682" s="T909">go-EP-IMP.2PL</ta>
            <ta e="T911" id="Seg_9683" s="T910">follow-CVB.SEQ</ta>
            <ta e="T912" id="Seg_9684" s="T911">send-PRS.[3SG]</ta>
            <ta e="T913" id="Seg_9685" s="T912">that.EMPH-ABL</ta>
            <ta e="T914" id="Seg_9686" s="T913">touch-PST2.NEG-3PL</ta>
            <ta e="T915" id="Seg_9687" s="T914">two</ta>
            <ta e="T916" id="Seg_9688" s="T915">month-3PL.[NOM]</ta>
            <ta e="T917" id="Seg_9689" s="T916">come.through-MED-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T918" id="Seg_9690" s="T917">crest-3PL.[NOM]</ta>
            <ta e="T919" id="Seg_9691" s="T918">last-3SG.[NOM]</ta>
            <ta e="T920" id="Seg_9692" s="T919">be-PRS.[3SG]</ta>
            <ta e="T921" id="Seg_9693" s="T920">see-PST2-3SG</ta>
            <ta e="T922" id="Seg_9694" s="T921">tundra.[NOM]</ta>
            <ta e="T923" id="Seg_9695" s="T922">become-PST2.[3SG]</ta>
            <ta e="T924" id="Seg_9696" s="T923">snow.[NOM]</ta>
            <ta e="T925" id="Seg_9697" s="T924">lower.part-3SG-ABL</ta>
            <ta e="T926" id="Seg_9698" s="T925">smoke-DIM-INTNS-3SG.[NOM]</ta>
            <ta e="T927" id="Seg_9699" s="T926">tundra.[NOM]</ta>
            <ta e="T928" id="Seg_9700" s="T927">in.the.direction-ABL</ta>
            <ta e="T929" id="Seg_9701" s="T928">very.big</ta>
            <ta e="T930" id="Seg_9702" s="T929">very</ta>
            <ta e="T931" id="Seg_9703" s="T930">path.[NOM]</ta>
            <ta e="T932" id="Seg_9704" s="T931">come-PRS.[3SG]</ta>
            <ta e="T933" id="Seg_9705" s="T932">earth.[NOM]</ta>
            <ta e="T934" id="Seg_9706" s="T933">inside-3SG-ACC</ta>
            <ta e="T935" id="Seg_9707" s="T934">in.the.direction</ta>
            <ta e="T936" id="Seg_9708" s="T935">go-PRS.[3SG]</ta>
            <ta e="T937" id="Seg_9709" s="T936">path.[NOM]</ta>
            <ta e="T938" id="Seg_9710" s="T937">go-CVB.SEQ</ta>
            <ta e="T939" id="Seg_9711" s="T938">go-PRS-3PL</ta>
            <ta e="T940" id="Seg_9712" s="T939">directly</ta>
            <ta e="T941" id="Seg_9713" s="T940">people-ABL</ta>
            <ta e="T942" id="Seg_9714" s="T941">people.[NOM]</ta>
            <ta e="T943" id="Seg_9715" s="T942">tent-ABL</ta>
            <ta e="T944" id="Seg_9716" s="T943">tent.[NOM]</ta>
            <ta e="T945" id="Seg_9717" s="T944">earth-DAT/LOC</ta>
            <ta e="T946" id="Seg_9718" s="T945">come-PRS-3PL</ta>
            <ta e="T947" id="Seg_9719" s="T946">one</ta>
            <ta e="T948" id="Seg_9720" s="T947">tent.[NOM]</ta>
            <ta e="T949" id="Seg_9721" s="T948">edge-3SG-DAT/LOC</ta>
            <ta e="T950" id="Seg_9722" s="T949">stop-PRS.[3SG]</ta>
            <ta e="T951" id="Seg_9723" s="T950">one</ta>
            <ta e="T952" id="Seg_9724" s="T951">old</ta>
            <ta e="T953" id="Seg_9725" s="T952">EMPH</ta>
            <ta e="T954" id="Seg_9726" s="T953">woman.[NOM]</ta>
            <ta e="T955" id="Seg_9727" s="T954">go.out-PRS.[3SG]</ta>
            <ta e="T956" id="Seg_9728" s="T955">jeez</ta>
            <ta e="T957" id="Seg_9729" s="T956">reindeer-3SG.[NOM]</ta>
            <ta e="T958" id="Seg_9730" s="T957">miracle.[NOM]</ta>
            <ta e="T959" id="Seg_9731" s="T958">reindeer.[NOM]</ta>
            <ta e="T960" id="Seg_9732" s="T959">1PL.[NOM]</ta>
            <ta e="T961" id="Seg_9733" s="T960">ancestor.[NOM]</ta>
            <ta e="T962" id="Seg_9734" s="T961">reindeer-1PL.[NOM]</ta>
            <ta e="T963" id="Seg_9735" s="T962">like=Q</ta>
            <ta e="T964" id="Seg_9736" s="T963">long.ago</ta>
            <ta e="T965" id="Seg_9737" s="T964">two</ta>
            <ta e="T966" id="Seg_9738" s="T965">human.being.[NOM]</ta>
            <ta e="T967" id="Seg_9739" s="T966">go-PST2-3SG</ta>
            <ta e="T968" id="Seg_9740" s="T967">that</ta>
            <ta e="T969" id="Seg_9741" s="T968">lineage-3PL.[NOM]</ta>
            <ta e="T970" id="Seg_9742" s="T969">probably</ta>
            <ta e="T971" id="Seg_9743" s="T970">three</ta>
            <ta e="T972" id="Seg_9744" s="T971">tent-ABL</ta>
            <ta e="T973" id="Seg_9745" s="T972">three</ta>
            <ta e="T974" id="Seg_9746" s="T973">woman.[NOM]</ta>
            <ta e="T975" id="Seg_9747" s="T974">go.out-CVB.SEQ</ta>
            <ta e="T976" id="Seg_9748" s="T975">again</ta>
            <ta e="T977" id="Seg_9749" s="T976">realize-PST2-3PL</ta>
            <ta e="T978" id="Seg_9750" s="T977">lineage-3PL-ACC</ta>
            <ta e="T979" id="Seg_9751" s="T978">chat-CVB.SEQ</ta>
            <ta e="T980" id="Seg_9752" s="T979">well</ta>
            <ta e="T981" id="Seg_9753" s="T980">get.to.know-RECP/COLL-PRS-3PL</ta>
            <ta e="T982" id="Seg_9754" s="T981">sibling-3PL-ACC</ta>
            <ta e="T983" id="Seg_9755" s="T982">find-RECP/COLL-PRS-3PL</ta>
            <ta e="T984" id="Seg_9756" s="T983">Kungadyay-ACC</ta>
            <ta e="T985" id="Seg_9757" s="T984">wait-PRS-3PL</ta>
            <ta e="T986" id="Seg_9758" s="T985">three-ORD</ta>
            <ta e="T987" id="Seg_9759" s="T986">day-3SG-GEN</ta>
            <ta e="T988" id="Seg_9760" s="T987">middle-3SG.[NOM]</ta>
            <ta e="T989" id="Seg_9761" s="T988">Kungadyay.[NOM]</ta>
            <ta e="T990" id="Seg_9762" s="T989">come-PRS.[3SG]</ta>
            <ta e="T991" id="Seg_9763" s="T990">meat.[NOM]</ta>
            <ta e="T992" id="Seg_9764" s="T991">name-3SG.[NOM]</ta>
            <ta e="T993" id="Seg_9765" s="T992">intact-POSS</ta>
            <ta e="T994" id="Seg_9766" s="T993">NEG</ta>
            <ta e="T995" id="Seg_9767" s="T994">skin-3SG-GEN</ta>
            <ta e="T996" id="Seg_9768" s="T995">half-3SG.[NOM]</ta>
            <ta e="T997" id="Seg_9769" s="T996">stay-PST2.[3SG]</ta>
            <ta e="T998" id="Seg_9770" s="T997">child-1SG.[NOM]</ta>
            <ta e="T999" id="Seg_9771" s="T998">come-PST2-2SG</ta>
            <ta e="T1000" id="Seg_9772" s="T999">well</ta>
            <ta e="T1001" id="Seg_9773" s="T1000">good.[NOM]</ta>
            <ta e="T1002" id="Seg_9774" s="T1001">bad</ta>
            <ta e="T1003" id="Seg_9775" s="T1002">people-ACC</ta>
            <ta e="T1004" id="Seg_9776" s="T1003">defeat-CVB.SEQ</ta>
            <ta e="T1005" id="Seg_9777" s="T1004">place.[NOM]</ta>
            <ta e="T1006" id="Seg_9778" s="T1005">animal-3SG-ACC</ta>
            <ta e="T1007" id="Seg_9779" s="T1006">hardly</ta>
            <ta e="T1008" id="Seg_9780" s="T1007">win-PST1-1SG</ta>
            <ta e="T1009" id="Seg_9781" s="T1008">soul-EP-1SG.[NOM]</ta>
            <ta e="T1010" id="Seg_9782" s="T1009">just</ta>
            <ta e="T1011" id="Seg_9783" s="T1010">come-PST1-1SG</ta>
            <ta e="T1012" id="Seg_9784" s="T1011">wound.[NOM]</ta>
            <ta e="T1013" id="Seg_9785" s="T1012">make-CVB.SEQ-3PL</ta>
            <ta e="T1014" id="Seg_9786" s="T1013">well</ta>
            <ta e="T1015" id="Seg_9787" s="T1014">live.[IMP.2SG]</ta>
            <ta e="T1016" id="Seg_9788" s="T1015">what-ABL</ta>
            <ta e="T1017" id="Seg_9789" s="T1016">NEG</ta>
            <ta e="T1018" id="Seg_9790" s="T1017">be.afraid-EP-NEG.[IMP.2SG]</ta>
            <ta e="T1019" id="Seg_9791" s="T1018">Kungadyay.[NOM]</ta>
            <ta e="T1020" id="Seg_9792" s="T1019">well</ta>
            <ta e="T1021" id="Seg_9793" s="T1020">later</ta>
            <ta e="T1022" id="Seg_9794" s="T1021">revive-PRS.[3SG]</ta>
            <ta e="T1023" id="Seg_9795" s="T1022">boy.[NOM]</ta>
            <ta e="T1024" id="Seg_9796" s="T1023">man.of.the.house.[NOM]</ta>
            <ta e="T1025" id="Seg_9797" s="T1024">become-CVB.SEQ</ta>
            <ta e="T1026" id="Seg_9798" s="T1025">live-PST2-3SG</ta>
            <ta e="T1027" id="Seg_9799" s="T1026">this</ta>
            <ta e="T1028" id="Seg_9800" s="T1027">people-DAT/LOC</ta>
            <ta e="T1029" id="Seg_9801" s="T1028">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_9802" s="T0">drei</ta>
            <ta e="T2" id="Seg_9803" s="T1">Bruder-PL.[NOM]</ta>
            <ta e="T3" id="Seg_9804" s="T2">leben-PST2-3PL</ta>
            <ta e="T4" id="Seg_9805" s="T3">eins-3PL.[NOM]</ta>
            <ta e="T5" id="Seg_9806" s="T4">klein</ta>
            <ta e="T6" id="Seg_9807" s="T5">winzig</ta>
            <ta e="T7" id="Seg_9808" s="T6">Mensch.[NOM]</ta>
            <ta e="T8" id="Seg_9809" s="T7">mittlerer-3PL.[NOM]</ta>
            <ta e="T9" id="Seg_9810" s="T8">go-NEG.[3SG]</ta>
            <ta e="T10" id="Seg_9811" s="T9">Knie-3SG-INSTR</ta>
            <ta e="T11" id="Seg_9812" s="T10">nur</ta>
            <ta e="T12" id="Seg_9813" s="T11">kriechen-PRS.[3SG]</ta>
            <ta e="T13" id="Seg_9814" s="T12">Kungadjaj.[NOM]</ta>
            <ta e="T14" id="Seg_9815" s="T13">sagen-CVB.SEQ</ta>
            <ta e="T15" id="Seg_9816" s="T14">älterer.Bruder-3PL.[NOM]</ta>
            <ta e="T16" id="Seg_9817" s="T15">Name-POSS</ta>
            <ta e="T17" id="Seg_9818" s="T16">NEG</ta>
            <ta e="T18" id="Seg_9819" s="T17">Jäger.[NOM]</ta>
            <ta e="T19" id="Seg_9820" s="T18">Frau-PROPR.[NOM]</ta>
            <ta e="T20" id="Seg_9821" s="T19">eins</ta>
            <ta e="T21" id="Seg_9822" s="T20">Fuchs.[NOM]</ta>
            <ta e="T22" id="Seg_9823" s="T21">Kind-PROPR.[NOM]</ta>
            <ta e="T23" id="Seg_9824" s="T22">Rentier-3PL.[NOM]</ta>
            <ta e="T24" id="Seg_9825" s="T23">EMPH-einsam.[NOM]</ta>
            <ta e="T25" id="Seg_9826" s="T24">Pferd.[NOM]</ta>
            <ta e="T26" id="Seg_9827" s="T25">ähnlich</ta>
            <ta e="T27" id="Seg_9828" s="T26">Zeit-DAT/LOC</ta>
            <ta e="T28" id="Seg_9829" s="T27">einspannen-NEG-3PL</ta>
            <ta e="T29" id="Seg_9830" s="T28">nomadisieren-TEMP-3PL</ta>
            <ta e="T30" id="Seg_9831" s="T29">nur</ta>
            <ta e="T31" id="Seg_9832" s="T30">Zelt-3PL-ACC</ta>
            <ta e="T32" id="Seg_9833" s="T31">ziehen-CAUS-PRS-3PL</ta>
            <ta e="T33" id="Seg_9834" s="T32">älterer.Bruder-3PL.[NOM]</ta>
            <ta e="T34" id="Seg_9835" s="T33">Name-PROPR</ta>
            <ta e="T35" id="Seg_9836" s="T34">Jäger.[NOM]</ta>
            <ta e="T36" id="Seg_9837" s="T35">wildes.Rentier-ACC</ta>
            <ta e="T37" id="Seg_9838" s="T36">jagen-PRS.[3SG]</ta>
            <ta e="T38" id="Seg_9839" s="T37">Fisch-ACC</ta>
            <ta e="T39" id="Seg_9840" s="T38">EMPH</ta>
            <ta e="T40" id="Seg_9841" s="T39">Jahr.[NOM]</ta>
            <ta e="T41" id="Seg_9842" s="T40">Frühling-INCH-PTCP.PRS-3SG-ACC</ta>
            <ta e="T42" id="Seg_9843" s="T41">während</ta>
            <ta e="T43" id="Seg_9844" s="T42">älterer.Bruder-3PL.[NOM]</ta>
            <ta e="T44" id="Seg_9845" s="T43">krank.sein-PRS.[3SG]</ta>
            <ta e="T45" id="Seg_9846" s="T44">stärker.werden-PST2.[3SG]</ta>
            <ta e="T46" id="Seg_9847" s="T45">drei-ORD</ta>
            <ta e="T47" id="Seg_9848" s="T46">Tag-3SG-DAT/LOC</ta>
            <ta e="T48" id="Seg_9849" s="T47">ganz</ta>
            <ta e="T49" id="Seg_9850" s="T48">schwach.werden-PST2.[3SG]</ta>
            <ta e="T50" id="Seg_9851" s="T49">Vermächtnis.[NOM]</ta>
            <ta e="T51" id="Seg_9852" s="T50">sprechen-PST2.[3SG]</ta>
            <ta e="T52" id="Seg_9853" s="T51">Frau-3SG-DAT/LOC</ta>
            <ta e="T53" id="Seg_9854" s="T52">doch</ta>
            <ta e="T54" id="Seg_9855" s="T53">1SG.[NOM]</ta>
            <ta e="T55" id="Seg_9856" s="T54">sterben-PST1-1SG</ta>
            <ta e="T56" id="Seg_9857" s="T55">wer-EP-2SG.[NOM]</ta>
            <ta e="T57" id="Seg_9858" s="T56">sich.kümmern-FUT-3SG=Q</ta>
            <ta e="T58" id="Seg_9859" s="T57">jagen-PTCP.PST</ta>
            <ta e="T59" id="Seg_9860" s="T58">Fang-1SG-ACC</ta>
            <ta e="T60" id="Seg_9861" s="T59">Sommer-ACC</ta>
            <ta e="T61" id="Seg_9862" s="T60">während</ta>
            <ta e="T62" id="Seg_9863" s="T61">essen-FUT-2PL</ta>
            <ta e="T63" id="Seg_9864" s="T62">Nahrung-2SG-ACC</ta>
            <ta e="T64" id="Seg_9865" s="T63">verbrauchen-TEMP-2SG</ta>
            <ta e="T65" id="Seg_9866" s="T64">kommendes.Jahr.[NOM]</ta>
            <ta e="T66" id="Seg_9867" s="T65">Haut-3SG-ACC</ta>
            <ta e="T67" id="Seg_9868" s="T66">Leder.vom.Rentierbein-3SG-ACC</ta>
            <ta e="T68" id="Seg_9869" s="T67">dünne.Lederschicht-VBZ-CVB.SEQ</ta>
            <ta e="T69" id="Seg_9870" s="T68">Suppe-VBZ-CVB.SEQ</ta>
            <ta e="T70" id="Seg_9871" s="T69">gehen-FUT.[IMP.2SG]</ta>
            <ta e="T71" id="Seg_9872" s="T70">dieses.[NOM]</ta>
            <ta e="T72" id="Seg_9873" s="T71">Familie-EP-2SG.[NOM]</ta>
            <ta e="T73" id="Seg_9874" s="T72">Nutzen-POSS</ta>
            <ta e="T74" id="Seg_9875" s="T73">NEG</ta>
            <ta e="T75" id="Seg_9876" s="T74">Familie.[NOM]</ta>
            <ta e="T76" id="Seg_9877" s="T75">sterben-PTCP.PST-2SG-INSTR</ta>
            <ta e="T77" id="Seg_9878" s="T76">sterben-PRS-2SG</ta>
            <ta e="T78" id="Seg_9879" s="T77">Kind-1SG.[NOM]</ta>
            <ta e="T79" id="Seg_9880" s="T78">vielleicht</ta>
            <ta e="T80" id="Seg_9881" s="T79">Mensch.[NOM]</ta>
            <ta e="T81" id="Seg_9882" s="T80">sein-FUT.[3SG]</ta>
            <ta e="T82" id="Seg_9883" s="T81">sterben-NEG-TEMP-3SG</ta>
            <ta e="T83" id="Seg_9884" s="T82">3SG.[NOM]</ta>
            <ta e="T84" id="Seg_9885" s="T83">füttern-FUT.[3SG]</ta>
            <ta e="T85" id="Seg_9886" s="T84">letzter-3SG.[NOM]</ta>
            <ta e="T86" id="Seg_9887" s="T85">sterben-CVB.SEQ</ta>
            <ta e="T87" id="Seg_9888" s="T86">bleiben-PRS.[3SG]</ta>
            <ta e="T88" id="Seg_9889" s="T87">weinen-EP-MED-CVB.SIM-weinen-EP-MED-CVB.SIM</ta>
            <ta e="T89" id="Seg_9890" s="T88">Frau.[NOM]</ta>
            <ta e="T90" id="Seg_9891" s="T89">legen-CVB.SEQ</ta>
            <ta e="T91" id="Seg_9892" s="T90">werfen-PRS.[3SG]</ta>
            <ta e="T92" id="Seg_9893" s="T91">dieses</ta>
            <ta e="T93" id="Seg_9894" s="T92">Sommer-3SG.[NOM]</ta>
            <ta e="T94" id="Seg_9895" s="T93">werden-PRS.[3SG]</ta>
            <ta e="T95" id="Seg_9896" s="T94">Nahrung-3PL-ACC</ta>
            <ta e="T96" id="Seg_9897" s="T95">trocken.werden-EP-CAUS-EP-MED-PRS-3PL</ta>
            <ta e="T97" id="Seg_9898" s="T96">Sommer-3PL-ACC</ta>
            <ta e="T98" id="Seg_9899" s="T97">überstehen-PRS-3PL</ta>
            <ta e="T99" id="Seg_9900" s="T98">Herbst-3PL-DAT/LOC</ta>
            <ta e="T100" id="Seg_9901" s="T99">Leder.vom.Rentierbein-3PL-ACC</ta>
            <ta e="T101" id="Seg_9902" s="T100">Haut-3PL-ACC</ta>
            <ta e="T102" id="Seg_9903" s="T101">essen-PRS-3PL</ta>
            <ta e="T103" id="Seg_9904" s="T102">jeder-3SG.[NOM]</ta>
            <ta e="T104" id="Seg_9905" s="T103">zu.Ende.gehen-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_9906" s="T104">Winter.[NOM]</ta>
            <ta e="T106" id="Seg_9907" s="T105">werden-PRS.[3SG]</ta>
            <ta e="T107" id="Seg_9908" s="T106">essen-PTCP.FUT</ta>
            <ta e="T108" id="Seg_9909" s="T107">Sache-3PL.[NOM]</ta>
            <ta e="T109" id="Seg_9910" s="T108">NEG.EX</ta>
            <ta e="T110" id="Seg_9911" s="T109">Wind-POSS</ta>
            <ta e="T111" id="Seg_9912" s="T110">NEG</ta>
            <ta e="T112" id="Seg_9913" s="T111">Tag.[NOM]</ta>
            <ta e="T113" id="Seg_9914" s="T112">nur</ta>
            <ta e="T114" id="Seg_9915" s="T113">Schlitten-PROPR</ta>
            <ta e="T115" id="Seg_9916" s="T114">Geräusch-3SG-ACC</ta>
            <ta e="T116" id="Seg_9917" s="T115">hören-PRS.[3SG]</ta>
            <ta e="T117" id="Seg_9918" s="T116">Frau.[NOM]</ta>
            <ta e="T118" id="Seg_9919" s="T117">hinausgehen-CVB.SIM</ta>
            <ta e="T119" id="Seg_9920" s="T118">rennen-PRS.[3SG]</ta>
            <ta e="T120" id="Seg_9921" s="T119">Stimme.[NOM]</ta>
            <ta e="T121" id="Seg_9922" s="T120">nur</ta>
            <ta e="T122" id="Seg_9923" s="T121">gehört.werden-PTCP.PRS</ta>
            <ta e="T123" id="Seg_9924" s="T122">Ort-3SG-DAT/LOC</ta>
            <ta e="T124" id="Seg_9925" s="T123">halten-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_9926" s="T124">nur</ta>
            <ta e="T126" id="Seg_9927" s="T125">Frau.[NOM]</ta>
            <ta e="T127" id="Seg_9928" s="T126">Vater-3SG.[NOM]</ta>
            <ta e="T128" id="Seg_9929" s="T127">acht</ta>
            <ta e="T129" id="Seg_9930" s="T128">schwarzes.Rentier.[NOM]</ta>
            <ta e="T130" id="Seg_9931" s="T129">Herr-3SG.[NOM]</ta>
            <ta e="T131" id="Seg_9932" s="T130">Hausherr-3SG.[NOM]</ta>
            <ta e="T132" id="Seg_9933" s="T131">Fürst.[NOM]</ta>
            <ta e="T133" id="Seg_9934" s="T132">sein-PST2.[3SG]</ta>
            <ta e="T134" id="Seg_9935" s="T133">hey</ta>
            <ta e="T135" id="Seg_9936" s="T134">wie</ta>
            <ta e="T136" id="Seg_9937" s="T135">leben-PRS-2PL</ta>
            <ta e="T137" id="Seg_9938" s="T136">Mann-EP-1SG.[NOM]</ta>
            <ta e="T138" id="Seg_9939" s="T137">sterben-PST2-3SG</ta>
            <ta e="T139" id="Seg_9940" s="T138">letztes.Jahr.[NOM]</ta>
            <ta e="T140" id="Seg_9941" s="T139">hey</ta>
            <ta e="T141" id="Seg_9942" s="T140">Gott.sei.Dank</ta>
            <ta e="T142" id="Seg_9943" s="T141">sehr</ta>
            <ta e="T143" id="Seg_9944" s="T142">sich.freuen-PST1-1SG</ta>
            <ta e="T144" id="Seg_9945" s="T143">sich.anziehen.[IMP.2SG]</ta>
            <ta e="T145" id="Seg_9946" s="T144">gehen-IMP.1DU</ta>
            <ta e="T146" id="Seg_9947" s="T145">1SG.[NOM]</ta>
            <ta e="T147" id="Seg_9948" s="T146">Haus-1SG-DAT/LOC</ta>
            <ta e="T148" id="Seg_9949" s="T147">na</ta>
            <ta e="T149" id="Seg_9950" s="T148">Kind-1SG-ACC</ta>
            <ta e="T150" id="Seg_9951" s="T149">Familie-1SG-ACC</ta>
            <ta e="T151" id="Seg_9952" s="T150">hungern-CVB.SEQ</ta>
            <ta e="T152" id="Seg_9953" s="T151">sterben-IMP.3PL</ta>
            <ta e="T153" id="Seg_9954" s="T152">gehen-IMP.1DU</ta>
            <ta e="T154" id="Seg_9955" s="T153">Frau.[NOM]</ta>
            <ta e="T155" id="Seg_9956" s="T154">sich.anziehen-CVB.SEQ</ta>
            <ta e="T156" id="Seg_9957" s="T155">ankommen-CVB.SEQ</ta>
            <ta e="T157" id="Seg_9958" s="T156">reiten-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_9959" s="T157">und</ta>
            <ta e="T159" id="Seg_9960" s="T158">rennen-CVB.SEQ</ta>
            <ta e="T160" id="Seg_9961" s="T159">bleiben-PRS-3PL</ta>
            <ta e="T161" id="Seg_9962" s="T160">Kungadjaj-PROPR</ta>
            <ta e="T162" id="Seg_9963" s="T161">heftig</ta>
            <ta e="T163" id="Seg_9964" s="T162">hungrig.[NOM]</ta>
            <ta e="T164" id="Seg_9965" s="T163">liegen-INFER-3PL</ta>
            <ta e="T165" id="Seg_9966" s="T164">Polarnacht.[NOM]</ta>
            <ta e="T166" id="Seg_9967" s="T165">vorbeigehen-CVB.SEQ</ta>
            <ta e="T167" id="Seg_9968" s="T166">hell.werden-PTCP.PRS-DAT/LOC</ta>
            <ta e="T168" id="Seg_9969" s="T167">gehen-PRS.[3SG]</ta>
            <ta e="T169" id="Seg_9970" s="T168">Kungadjaj.[NOM]</ta>
            <ta e="T170" id="Seg_9971" s="T169">jüngerer.Bruder-EP-3SG-DAT/LOC</ta>
            <ta e="T171" id="Seg_9972" s="T170">sprechen-PRS.[3SG]</ta>
            <ta e="T172" id="Seg_9973" s="T171">Rentier-2SG-ACC</ta>
            <ta e="T173" id="Seg_9974" s="T172">einspannen.[IMP.2SG]</ta>
            <ta e="T174" id="Seg_9975" s="T173">liegen-PTCP.PST-EP-INSTR</ta>
            <ta e="T175" id="Seg_9976" s="T174">sterben-FUT-1PL</ta>
            <ta e="T176" id="Seg_9977" s="T175">acht</ta>
            <ta e="T177" id="Seg_9978" s="T176">schwarzes.Rentier-ACC</ta>
            <ta e="T178" id="Seg_9979" s="T177">folgen-IMP.1DU</ta>
            <ta e="T179" id="Seg_9980" s="T178">Rentier-3PL-ACC</ta>
            <ta e="T180" id="Seg_9981" s="T179">einspannen-CVB.SEQ</ta>
            <ta e="T181" id="Seg_9982" s="T180">Zelt-3PL-ACC</ta>
            <ta e="T182" id="Seg_9983" s="T181">zerlegen-CVB.SEQ</ta>
            <ta e="T183" id="Seg_9984" s="T182">reiten-CVB.SEQ</ta>
            <ta e="T184" id="Seg_9985" s="T183">schlagen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T185" id="Seg_9986" s="T184">gehen-INFER-3PL</ta>
            <ta e="T186" id="Seg_9987" s="T185">wie.viel</ta>
            <ta e="T187" id="Seg_9988" s="T186">zehn-MLTP</ta>
            <ta e="T188" id="Seg_9989" s="T187">nomadisieren-PST2-3PL</ta>
            <ta e="T189" id="Seg_9990" s="T188">einmal</ta>
            <ta e="T190" id="Seg_9991" s="T189">hoch</ta>
            <ta e="T191" id="Seg_9992" s="T190">bewaldeter.Berg-DAT/LOC</ta>
            <ta e="T192" id="Seg_9993" s="T191">klettern-EP-PST2-3PL</ta>
            <ta e="T193" id="Seg_9994" s="T192">schauen.auf-PST2-3PL</ta>
            <ta e="T194" id="Seg_9995" s="T193">riesig-PROPR</ta>
            <ta e="T195" id="Seg_9996" s="T194">weite.Ebene.[NOM]</ta>
            <ta e="T196" id="Seg_9997" s="T195">weite.Ebene.[NOM]</ta>
            <ta e="T197" id="Seg_9998" s="T196">Mitte-3SG-DAT/LOC</ta>
            <ta e="T198" id="Seg_9999" s="T197">Herde.[NOM]</ta>
            <ta e="T199" id="Seg_10000" s="T198">sehr.viel</ta>
            <ta e="T200" id="Seg_10001" s="T199">weite.Ebene.[NOM]</ta>
            <ta e="T201" id="Seg_10002" s="T200">auf.der.anderen.Seite</ta>
            <ta e="T202" id="Seg_10003" s="T201">sieben</ta>
            <ta e="T203" id="Seg_10004" s="T202">zehn</ta>
            <ta e="T204" id="Seg_10005" s="T203">Zelt.[NOM]</ta>
            <ta e="T205" id="Seg_10006" s="T204">MOD</ta>
            <ta e="T206" id="Seg_10007" s="T205">zu.sehen.sein-PRS.[3SG]</ta>
            <ta e="T207" id="Seg_10008" s="T206">gehen-PST2-3PL</ta>
            <ta e="T208" id="Seg_10009" s="T207">Herde.[NOM]</ta>
            <ta e="T209" id="Seg_10010" s="T208">Inneres-3SG-INSTR</ta>
            <ta e="T210" id="Seg_10011" s="T209">Herde.[NOM]</ta>
            <ta e="T211" id="Seg_10012" s="T210">anderer.von.zwei</ta>
            <ta e="T212" id="Seg_10013" s="T211">Seite-3SG-DAT/LOC</ta>
            <ta e="T213" id="Seg_10014" s="T212">stehen.[IMP.2SG]</ta>
            <ta e="T214" id="Seg_10015" s="T213">sagen-PST2.[3SG]</ta>
            <ta e="T215" id="Seg_10016" s="T214">Kungadjaj.[NOM]</ta>
            <ta e="T216" id="Seg_10017" s="T215">dieses.[NOM]</ta>
            <ta e="T217" id="Seg_10018" s="T216">Zelt-PL-ABL</ta>
            <ta e="T218" id="Seg_10019" s="T217">eins</ta>
            <ta e="T219" id="Seg_10020" s="T218">besser-3PL-DAT/LOC</ta>
            <ta e="T220" id="Seg_10021" s="T219">halten-FUT.[IMP.2SG]</ta>
            <ta e="T221" id="Seg_10022" s="T220">Platz.zum.Holzhacken-3PL-ACC</ta>
            <ta e="T222" id="Seg_10023" s="T221">jene.Seite.[NOM]</ta>
            <ta e="T223" id="Seg_10024" s="T222">Seite-DIM-3SG-DAT/LOC</ta>
            <ta e="T224" id="Seg_10025" s="T223">sehen-PST2-3SG</ta>
            <ta e="T225" id="Seg_10026" s="T224">eins</ta>
            <ta e="T226" id="Seg_10027" s="T225">Haus.[NOM]</ta>
            <ta e="T227" id="Seg_10028" s="T226">jeder-3PL-ABL</ta>
            <ta e="T228" id="Seg_10029" s="T227">groß.[NOM]</ta>
            <ta e="T229" id="Seg_10030" s="T228">Eingang.[NOM]</ta>
            <ta e="T230" id="Seg_10031" s="T229">Seite-3SG-DAT/LOC</ta>
            <ta e="T231" id="Seg_10032" s="T230">ankommen-PST2-3PL</ta>
            <ta e="T232" id="Seg_10033" s="T231">und</ta>
            <ta e="T233" id="Seg_10034" s="T232">Rentier-3PL-ACC</ta>
            <ta e="T234" id="Seg_10035" s="T233">lassen-CVB.SEQ</ta>
            <ta e="T235" id="Seg_10036" s="T234">werfen-PRS-3PL</ta>
            <ta e="T236" id="Seg_10037" s="T235">Zelt.[NOM]</ta>
            <ta e="T237" id="Seg_10038" s="T236">aufbauen-CVB.SEQ</ta>
            <ta e="T238" id="Seg_10039" s="T237">werfen-PST2-3PL</ta>
            <ta e="T239" id="Seg_10040" s="T238">anzünden-EP-PST2-3PL</ta>
            <ta e="T240" id="Seg_10041" s="T239">jüngerer.Bruder-EP-3SG-ACC</ta>
            <ta e="T241" id="Seg_10042" s="T240">gehen.[IMP.2SG]</ta>
            <ta e="T242" id="Seg_10043" s="T241">Schwägerin-2SG-DAT/LOC</ta>
            <ta e="T243" id="Seg_10044" s="T242">Kind-3SG-ACC</ta>
            <ta e="T244" id="Seg_10045" s="T243">saugen-CAUS-CVB.SIM</ta>
            <ta e="T245" id="Seg_10046" s="T244">kommen-IMP.3SG</ta>
            <ta e="T246" id="Seg_10047" s="T245">acht</ta>
            <ta e="T247" id="Seg_10048" s="T246">schwarzes.Rentier-DAT/LOC</ta>
            <ta e="T248" id="Seg_10049" s="T247">sprechen.[IMP.2SG]</ta>
            <ta e="T249" id="Seg_10050" s="T248">sterben-PST1-1PL</ta>
            <ta e="T250" id="Seg_10051" s="T249">eins</ta>
            <ta e="T251" id="Seg_10052" s="T250">INDEF</ta>
            <ta e="T252" id="Seg_10053" s="T251">Herz.[NOM]</ta>
            <ta e="T253" id="Seg_10054" s="T252">Hälfte-DIM-3SG-ACC</ta>
            <ta e="T254" id="Seg_10055" s="T253">mit</ta>
            <ta e="T255" id="Seg_10056" s="T254">Leber.[NOM]</ta>
            <ta e="T256" id="Seg_10057" s="T255">Witwer-3SG-ACC</ta>
            <ta e="T257" id="Seg_10058" s="T256">teilen-IMP.3SG</ta>
            <ta e="T258" id="Seg_10059" s="T257">sagen-PRS.[3SG]</ta>
            <ta e="T259" id="Seg_10060" s="T258">Junge.[NOM]</ta>
            <ta e="T260" id="Seg_10061" s="T259">laufen-CVB.SEQ</ta>
            <ta e="T261" id="Seg_10062" s="T260">hineingehen-CVB.SEQ</ta>
            <ta e="T262" id="Seg_10063" s="T261">sprechen-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_10064" s="T262">sehen.[IMP.2SG]</ta>
            <ta e="T264" id="Seg_10065" s="T263">nur</ta>
            <ta e="T265" id="Seg_10066" s="T264">saugen-CAUS-PTCP.PRS</ta>
            <ta e="T266" id="Seg_10067" s="T265">Unordnung.[NOM]</ta>
            <ta e="T267" id="Seg_10068" s="T266">hungern-CVB.SEQ</ta>
            <ta e="T268" id="Seg_10069" s="T267">sterben-IMP.3SG</ta>
            <ta e="T269" id="Seg_10070" s="T268">sagen-PRS.[3SG]</ta>
            <ta e="T270" id="Seg_10071" s="T269">Frau.[NOM]</ta>
            <ta e="T271" id="Seg_10072" s="T270">na</ta>
            <ta e="T272" id="Seg_10073" s="T271">Großvater</ta>
            <ta e="T273" id="Seg_10074" s="T272">Mitteilung-VBZ-PST1-3SG</ta>
            <ta e="T274" id="Seg_10075" s="T273">älterer.Bruder-EP-1SG.[NOM]</ta>
            <ta e="T275" id="Seg_10076" s="T274">sterben-PST1-1PL</ta>
            <ta e="T276" id="Seg_10077" s="T275">eins</ta>
            <ta e="T277" id="Seg_10078" s="T276">INDEF</ta>
            <ta e="T278" id="Seg_10079" s="T277">Herz.[NOM]</ta>
            <ta e="T279" id="Seg_10080" s="T278">Hälfte-DIM-3SG-ACC</ta>
            <ta e="T280" id="Seg_10081" s="T279">mit</ta>
            <ta e="T281" id="Seg_10082" s="T280">Leber.[NOM]</ta>
            <ta e="T282" id="Seg_10083" s="T281">Witwer-3SG-ACC</ta>
            <ta e="T283" id="Seg_10084" s="T282">teilen-IMP.3SG</ta>
            <ta e="T284" id="Seg_10085" s="T283">wie</ta>
            <ta e="T285" id="Seg_10086" s="T284">hungern-CVB.SEQ</ta>
            <ta e="T286" id="Seg_10087" s="T285">sterben-EP-IMP.2PL</ta>
            <ta e="T287" id="Seg_10088" s="T286">schicken-NEG-1SG</ta>
            <ta e="T288" id="Seg_10089" s="T287">sagen-PRS.[3SG]</ta>
            <ta e="T289" id="Seg_10090" s="T288">acht</ta>
            <ta e="T290" id="Seg_10091" s="T289">schwarzes.Rentier.[NOM]</ta>
            <ta e="T291" id="Seg_10092" s="T290">Herr-3SG.[NOM]</ta>
            <ta e="T292" id="Seg_10093" s="T291">schlafen-EMOT-PRS-3PL</ta>
            <ta e="T293" id="Seg_10094" s="T292">was-ACC</ta>
            <ta e="T294" id="Seg_10095" s="T293">essen-FUT-3PL=Q</ta>
            <ta e="T295" id="Seg_10096" s="T294">plötzlich</ta>
            <ta e="T296" id="Seg_10097" s="T295">Rentier-VBZ-PTCP.PRS</ta>
            <ta e="T297" id="Seg_10098" s="T296">Stimme.[NOM]</ta>
            <ta e="T298" id="Seg_10099" s="T297">gehört.werden-PRS.[3SG]</ta>
            <ta e="T299" id="Seg_10100" s="T298">Kungadjaj.[NOM]</ta>
            <ta e="T300" id="Seg_10101" s="T299">hinausgehen-CVB.SEQ</ta>
            <ta e="T301" id="Seg_10102" s="T300">pinkeln-CVB.SIM</ta>
            <ta e="T302" id="Seg_10103" s="T301">aufstehen-PST2.[3SG]</ta>
            <ta e="T303" id="Seg_10104" s="T302">Knie-3SG-DAT/LOC</ta>
            <ta e="T304" id="Seg_10105" s="T303">Rentier.[NOM]</ta>
            <ta e="T305" id="Seg_10106" s="T304">sehr.viel</ta>
            <ta e="T306" id="Seg_10107" s="T305">kommen-PRS.[3SG]</ta>
            <ta e="T307" id="Seg_10108" s="T306">kastriert</ta>
            <ta e="T308" id="Seg_10109" s="T307">männliches.Rentier.[NOM]</ta>
            <ta e="T309" id="Seg_10110" s="T308">fett.[NOM]</ta>
            <ta e="T310" id="Seg_10111" s="T309">sehr</ta>
            <ta e="T311" id="Seg_10112" s="T310">kommen-PRS.[3SG]</ta>
            <ta e="T312" id="Seg_10113" s="T311">Junge.[NOM]</ta>
            <ta e="T313" id="Seg_10114" s="T312">fangen-CVB.SEQ</ta>
            <ta e="T314" id="Seg_10115" s="T313">nehmen-PRS.[3SG]</ta>
            <ta e="T315" id="Seg_10116" s="T314">sich.werfen-CVB.SEQ</ta>
            <ta e="T316" id="Seg_10117" s="T315">sehr.viel</ta>
            <ta e="T317" id="Seg_10118" s="T316">Messer-3SG-ACC</ta>
            <ta e="T318" id="Seg_10119" s="T317">bekommen-CAUS-PRS.[3SG]</ta>
            <ta e="T319" id="Seg_10120" s="T318">Hals-3SG-ACC</ta>
            <ta e="T320" id="Seg_10121" s="T319">schneiden-CVB.SIM</ta>
            <ta e="T321" id="Seg_10122" s="T320">wischen-PRS.[3SG]</ta>
            <ta e="T322" id="Seg_10123" s="T321">Haut.abziehen-NEG.CVB.SIM</ta>
            <ta e="T323" id="Seg_10124" s="T322">NEG</ta>
            <ta e="T324" id="Seg_10125" s="T323">hereinbringen-PRS-3PL</ta>
            <ta e="T325" id="Seg_10126" s="T324">Kessel-VBZ-CVB.SEQ</ta>
            <ta e="T326" id="Seg_10127" s="T325">doch</ta>
            <ta e="T327" id="Seg_10128" s="T326">essen-PRS-3PL</ta>
            <ta e="T328" id="Seg_10129" s="T327">Tag-ACC</ta>
            <ta e="T329" id="Seg_10130" s="T328">ganz</ta>
            <ta e="T330" id="Seg_10131" s="T329">Abend-DAT/LOC</ta>
            <ta e="T331" id="Seg_10132" s="T330">bis.zu</ta>
            <ta e="T332" id="Seg_10133" s="T331">dieses</ta>
            <ta e="T333" id="Seg_10134" s="T332">Zelt.[NOM]</ta>
            <ta e="T334" id="Seg_10135" s="T333">Rand-3SG-DAT/LOC</ta>
            <ta e="T335" id="Seg_10136" s="T334">klein.[NOM]</ta>
            <ta e="T336" id="Seg_10137" s="T335">Zelt.[NOM]</ta>
            <ta e="T337" id="Seg_10138" s="T336">stehen-PRS.[3SG]</ta>
            <ta e="T338" id="Seg_10139" s="T337">doch</ta>
            <ta e="T339" id="Seg_10140" s="T338">sieben</ta>
            <ta e="T340" id="Seg_10141" s="T339">Knopf.[NOM]</ta>
            <ta e="T341" id="Seg_10142" s="T340">Herr-3SG-DAT/LOC</ta>
            <ta e="T342" id="Seg_10143" s="T341">gehen.[IMP.2SG]</ta>
            <ta e="T343" id="Seg_10144" s="T342">dieses.[NOM]</ta>
            <ta e="T344" id="Seg_10145" s="T343">Zelt-DAT/LOC</ta>
            <ta e="T345" id="Seg_10146" s="T344">Dorfältester.[NOM]</ta>
            <ta e="T346" id="Seg_10147" s="T345">wahrscheinlich</ta>
            <ta e="T347" id="Seg_10148" s="T346">Speise-PART</ta>
            <ta e="T348" id="Seg_10149" s="T347">bitten.[IMP.2SG]</ta>
            <ta e="T349" id="Seg_10150" s="T348">Junge.[NOM]</ta>
            <ta e="T350" id="Seg_10151" s="T349">gehen-PRS.[3SG]</ta>
            <ta e="T351" id="Seg_10152" s="T350">sieben</ta>
            <ta e="T352" id="Seg_10153" s="T351">Knopf.[NOM]</ta>
            <ta e="T353" id="Seg_10154" s="T352">Herr-3SG-DAT/LOC</ta>
            <ta e="T354" id="Seg_10155" s="T353">sprechen-PRS.[3SG]</ta>
            <ta e="T355" id="Seg_10156" s="T354">Mitteilung-ACC</ta>
            <ta e="T356" id="Seg_10157" s="T355">acht</ta>
            <ta e="T357" id="Seg_10158" s="T356">schwarzes.Rentier.[NOM]</ta>
            <ta e="T358" id="Seg_10159" s="T357">Herr-3SG.[NOM]</ta>
            <ta e="T359" id="Seg_10160" s="T358">geben-PST1-3SG</ta>
            <ta e="T360" id="Seg_10161" s="T359">offenbar</ta>
            <ta e="T361" id="Seg_10162" s="T360">nein</ta>
            <ta e="T362" id="Seg_10163" s="T361">hey</ta>
            <ta e="T363" id="Seg_10164" s="T362">dann</ta>
            <ta e="T364" id="Seg_10165" s="T363">1SG.[NOM]</ta>
            <ta e="T365" id="Seg_10166" s="T364">auch</ta>
            <ta e="T366" id="Seg_10167" s="T365">geben-NEG-1SG</ta>
            <ta e="T367" id="Seg_10168" s="T366">essen-PRS-3PL</ta>
            <ta e="T368" id="Seg_10169" s="T367">am.Morgen</ta>
            <ta e="T369" id="Seg_10170" s="T368">Herde.[NOM]</ta>
            <ta e="T370" id="Seg_10171" s="T369">gehen-PRS.[3SG]</ta>
            <ta e="T371" id="Seg_10172" s="T370">wieder</ta>
            <ta e="T372" id="Seg_10173" s="T371">Knie-3SG-INSTR</ta>
            <ta e="T373" id="Seg_10174" s="T372">kriechen-CVB.SEQ</ta>
            <ta e="T374" id="Seg_10175" s="T373">hinausgehen-PRS.[3SG]</ta>
            <ta e="T375" id="Seg_10176" s="T374">Kungadjaj.[NOM]</ta>
            <ta e="T376" id="Seg_10177" s="T375">Kungadjaj.[NOM]</ta>
            <ta e="T377" id="Seg_10178" s="T376">gebären-NEG.PTCP</ta>
            <ta e="T378" id="Seg_10179" s="T377">unfruchtbare.Rentierkuh-ACC</ta>
            <ta e="T379" id="Seg_10180" s="T378">greifen-CVB.SEQ</ta>
            <ta e="T380" id="Seg_10181" s="T379">nehmen-PRS.[3SG]</ta>
            <ta e="T381" id="Seg_10182" s="T380">dickes.Ende.des.Geweihs-3SG-ABL</ta>
            <ta e="T382" id="Seg_10183" s="T381">na=EMPH</ta>
            <ta e="T383" id="Seg_10184" s="T382">Messer-ACC</ta>
            <ta e="T384" id="Seg_10185" s="T383">herausnehmen.[IMP.2SG]</ta>
            <ta e="T385" id="Seg_10186" s="T384">töten-CVB.SEQ</ta>
            <ta e="T386" id="Seg_10187" s="T385">werfen-PRS.[3SG]</ta>
            <ta e="T387" id="Seg_10188" s="T386">wieder</ta>
            <ta e="T388" id="Seg_10189" s="T387">Haut.abziehen-NEG.CVB.SIM</ta>
            <ta e="T389" id="Seg_10190" s="T388">essen-PRS-3PL</ta>
            <ta e="T390" id="Seg_10191" s="T389">was.für.ein</ta>
            <ta e="T391" id="Seg_10192" s="T390">dieses</ta>
            <ta e="T392" id="Seg_10193" s="T391">eben.der</ta>
            <ta e="T393" id="Seg_10194" s="T392">Herr-PL.[NOM]</ta>
            <ta e="T394" id="Seg_10195" s="T393">eigen-3PL-ACC</ta>
            <ta e="T395" id="Seg_10196" s="T394">töten-PTCP.PST</ta>
            <ta e="T396" id="Seg_10197" s="T395">sein-PST2.[3SG]</ta>
            <ta e="T397" id="Seg_10198" s="T396">einschlafen-PRS-3PL</ta>
            <ta e="T398" id="Seg_10199" s="T397">am.Morgen</ta>
            <ta e="T399" id="Seg_10200" s="T398">Kungadjaj.[NOM]</ta>
            <ta e="T400" id="Seg_10201" s="T399">hinausgehen-EP-PST2-3SG</ta>
            <ta e="T401" id="Seg_10202" s="T400">acht</ta>
            <ta e="T402" id="Seg_10203" s="T401">schwarzes.Rentier.[NOM]</ta>
            <ta e="T403" id="Seg_10204" s="T402">Hausherr-3SG.[NOM]</ta>
            <ta e="T404" id="Seg_10205" s="T403">Lenkstange-3SG-ACC</ta>
            <ta e="T405" id="Seg_10206" s="T404">schnitzen-CVB.SIM</ta>
            <ta e="T406" id="Seg_10207" s="T405">sitzen-PRS.[3SG]</ta>
            <ta e="T407" id="Seg_10208" s="T406">hey</ta>
            <ta e="T408" id="Seg_10209" s="T407">acht</ta>
            <ta e="T409" id="Seg_10210" s="T408">schwarzes.Rentier.[NOM]</ta>
            <ta e="T410" id="Seg_10211" s="T409">Herr-3SG.[NOM]</ta>
            <ta e="T411" id="Seg_10212" s="T410">hallo</ta>
            <ta e="T412" id="Seg_10213" s="T411">noch</ta>
            <ta e="T413" id="Seg_10214" s="T412">hallo</ta>
            <ta e="T414" id="Seg_10215" s="T413">heute</ta>
            <ta e="T415" id="Seg_10216" s="T414">töten-ITER-PRS-1PL</ta>
            <ta e="T416" id="Seg_10217" s="T415">letzter</ta>
            <ta e="T417" id="Seg_10218" s="T416">Rentier-PL-1PL-ABL</ta>
            <ta e="T418" id="Seg_10219" s="T417">essen-CVB.SIM</ta>
            <ta e="T419" id="Seg_10220" s="T418">sich.hinlegen-PRS-2PL</ta>
            <ta e="T420" id="Seg_10221" s="T419">selbst-2PL.[NOM]</ta>
            <ta e="T421" id="Seg_10222" s="T420">Schuld-2PL.[NOM]</ta>
            <ta e="T422" id="Seg_10223" s="T421">acht</ta>
            <ta e="T423" id="Seg_10224" s="T422">schwarzes.Rentier.[NOM]</ta>
            <ta e="T424" id="Seg_10225" s="T423">Herr-3SG.[NOM]</ta>
            <ta e="T425" id="Seg_10226" s="T424">Knie-VBZ-CVB.SEQ</ta>
            <ta e="T426" id="Seg_10227" s="T425">stehen-PTCP.PRS</ta>
            <ta e="T427" id="Seg_10228" s="T426">Mensch-ACC</ta>
            <ta e="T428" id="Seg_10229" s="T427">Lenkstange.[NOM]</ta>
            <ta e="T429" id="Seg_10230" s="T428">Speer-3SG-INSTR</ta>
            <ta e="T430" id="Seg_10231" s="T429">Hüfte-DAT/LOC</ta>
            <ta e="T431" id="Seg_10232" s="T430">schlagen-PRS.[3SG]</ta>
            <ta e="T432" id="Seg_10233" s="T431">und</ta>
            <ta e="T433" id="Seg_10234" s="T432">nicht.können-CVB.SEQ</ta>
            <ta e="T434" id="Seg_10235" s="T433">werfen-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_10236" s="T434">Kungadjaj-3SG.[NOM]</ta>
            <ta e="T436" id="Seg_10237" s="T435">Schnee.[NOM]</ta>
            <ta e="T437" id="Seg_10238" s="T436">Unterteil-3SG-INSTR</ta>
            <ta e="T438" id="Seg_10239" s="T437">gehen-PST2.[3SG]</ta>
            <ta e="T439" id="Seg_10240" s="T438">sieben</ta>
            <ta e="T440" id="Seg_10241" s="T439">Knopf.[NOM]</ta>
            <ta e="T441" id="Seg_10242" s="T440">Herr-3SG.[NOM]</ta>
            <ta e="T442" id="Seg_10243" s="T441">Lenkstange.[NOM]</ta>
            <ta e="T443" id="Seg_10244" s="T442">schnitzen-CVB.SIM</ta>
            <ta e="T444" id="Seg_10245" s="T443">sitzen-TEMP-3SG</ta>
            <ta e="T445" id="Seg_10246" s="T444">hinausgehen-PRS.[3SG]</ta>
            <ta e="T446" id="Seg_10247" s="T445">hey</ta>
            <ta e="T447" id="Seg_10248" s="T446">sieben</ta>
            <ta e="T448" id="Seg_10249" s="T447">Knopf.[NOM]</ta>
            <ta e="T449" id="Seg_10250" s="T448">Herr-3SG.[NOM]</ta>
            <ta e="T450" id="Seg_10251" s="T449">hallo</ta>
            <ta e="T451" id="Seg_10252" s="T450">noch</ta>
            <ta e="T452" id="Seg_10253" s="T451">hallo</ta>
            <ta e="T453" id="Seg_10254" s="T452">heute</ta>
            <ta e="T454" id="Seg_10255" s="T453">töten-ITER-PRS-1PL</ta>
            <ta e="T455" id="Seg_10256" s="T454">letzter</ta>
            <ta e="T456" id="Seg_10257" s="T455">Rentier-PL-1PL-ACC</ta>
            <ta e="T457" id="Seg_10258" s="T456">essen-CVB.SIM</ta>
            <ta e="T458" id="Seg_10259" s="T457">sich.hinlegen-PRS-2PL</ta>
            <ta e="T459" id="Seg_10260" s="T458">selbst-2PL.[NOM]</ta>
            <ta e="T460" id="Seg_10261" s="T459">Schuld-2PL.[NOM]</ta>
            <ta e="T461" id="Seg_10262" s="T460">sieben</ta>
            <ta e="T462" id="Seg_10263" s="T461">Knopf.[NOM]</ta>
            <ta e="T463" id="Seg_10264" s="T462">Herr-3SG.[NOM]</ta>
            <ta e="T464" id="Seg_10265" s="T463">Knie-VBZ-CVB.SEQ</ta>
            <ta e="T465" id="Seg_10266" s="T464">stehen-PTCP.PRS</ta>
            <ta e="T466" id="Seg_10267" s="T465">Mensch-ACC</ta>
            <ta e="T467" id="Seg_10268" s="T466">Lenkstange.[NOM]</ta>
            <ta e="T468" id="Seg_10269" s="T467">Speer-3SG-INSTR</ta>
            <ta e="T469" id="Seg_10270" s="T468">Hüfte-DAT/LOC</ta>
            <ta e="T470" id="Seg_10271" s="T469">stoßen-CVB.PURP</ta>
            <ta e="T471" id="Seg_10272" s="T470">Erde-ACC</ta>
            <ta e="T472" id="Seg_10273" s="T471">schlagen-PST2.[3SG]</ta>
            <ta e="T473" id="Seg_10274" s="T472">wann</ta>
            <ta e="T474" id="Seg_10275" s="T473">INDEF</ta>
            <ta e="T475" id="Seg_10276" s="T474">Kungadjaj.[NOM]</ta>
            <ta e="T476" id="Seg_10277" s="T475">nächster</ta>
            <ta e="T477" id="Seg_10278" s="T476">Ort-EP-INSTR</ta>
            <ta e="T478" id="Seg_10279" s="T477">hinausgehen-CVB.SIM</ta>
            <ta e="T479" id="Seg_10280" s="T478">rennen-PRS.[3SG]</ta>
            <ta e="T480" id="Seg_10281" s="T479">und</ta>
            <ta e="T481" id="Seg_10282" s="T480">zwei</ta>
            <ta e="T482" id="Seg_10283" s="T481">Bein-3SG-DAT/LOC</ta>
            <ta e="T483" id="Seg_10284" s="T482">aufstehen-CVB.SIM</ta>
            <ta e="T484" id="Seg_10285" s="T483">springen-PRS.[3SG]</ta>
            <ta e="T485" id="Seg_10286" s="T484">schauen.auf-PST2-3SG</ta>
            <ta e="T486" id="Seg_10287" s="T485">Rentierschlitten.[NOM]</ta>
            <ta e="T487" id="Seg_10288" s="T486">gehen-CVB.SEQ</ta>
            <ta e="T488" id="Seg_10289" s="T487">sein-PRS.[3SG]</ta>
            <ta e="T489" id="Seg_10290" s="T488">Schwägerin-3SG.[NOM]</ta>
            <ta e="T490" id="Seg_10291" s="T489">nomadisieren-CVB.SEQ</ta>
            <ta e="T491" id="Seg_10292" s="T490">sein-PRS.[3SG]</ta>
            <ta e="T492" id="Seg_10293" s="T491">Vater-3SG.[NOM]</ta>
            <ta e="T493" id="Seg_10294" s="T492">Familie.[NOM]</ta>
            <ta e="T494" id="Seg_10295" s="T493">geben-PST2.[3SG]</ta>
            <ta e="T495" id="Seg_10296" s="T494">Kungadjaj.[NOM]</ta>
            <ta e="T496" id="Seg_10297" s="T495">Schwägerin-3SG-GEN</ta>
            <ta e="T497" id="Seg_10298" s="T496">Zügel-3SG-ACC</ta>
            <ta e="T498" id="Seg_10299" s="T497">fangen-CVB.SEQ</ta>
            <ta e="T499" id="Seg_10300" s="T498">nehmen-PRS.[3SG]</ta>
            <ta e="T500" id="Seg_10301" s="T499">lassen-NEG-1SG</ta>
            <ta e="T501" id="Seg_10302" s="T500">was.für.ein</ta>
            <ta e="T502" id="Seg_10303" s="T501">in.Richtung</ta>
            <ta e="T503" id="Seg_10304" s="T502">gehen-PRS-2SG</ta>
            <ta e="T504" id="Seg_10305" s="T503">Frau.[NOM]</ta>
            <ta e="T505" id="Seg_10306" s="T504">rennen-CVB.SEQ</ta>
            <ta e="T506" id="Seg_10307" s="T505">bleiben-PRS.[3SG]</ta>
            <ta e="T507" id="Seg_10308" s="T506">Kungadjaj.[NOM]</ta>
            <ta e="T508" id="Seg_10309" s="T507">verfolgen-CVB.SIM</ta>
            <ta e="T509" id="Seg_10310" s="T508">folgen-ITER-CVB.SEQ</ta>
            <ta e="T510" id="Seg_10311" s="T509">Karawane-3SG-ACC</ta>
            <ta e="T511" id="Seg_10312" s="T510">tragen-CVB.SIM</ta>
            <ta e="T512" id="Seg_10313" s="T511">bleiben-PRS.[3SG]</ta>
            <ta e="T513" id="Seg_10314" s="T512">Kungadjaj.[NOM]</ta>
            <ta e="T514" id="Seg_10315" s="T513">acht</ta>
            <ta e="T515" id="Seg_10316" s="T514">schwarzes.Rentier.[NOM]</ta>
            <ta e="T516" id="Seg_10317" s="T515">Herr-3SG-DAT/LOC</ta>
            <ta e="T517" id="Seg_10318" s="T516">hineingehen-PRS.[3SG]</ta>
            <ta e="T518" id="Seg_10319" s="T517">doch</ta>
            <ta e="T519" id="Seg_10320" s="T518">acht</ta>
            <ta e="T520" id="Seg_10321" s="T519">schwarzes.Rentier.[NOM]</ta>
            <ta e="T521" id="Seg_10322" s="T520">Herr-3SG.[NOM]</ta>
            <ta e="T522" id="Seg_10323" s="T521">Frieden-PROPR.[NOM]</ta>
            <ta e="T523" id="Seg_10324" s="T522">sein-PTCP.COND-DAT/LOC</ta>
            <ta e="T524" id="Seg_10325" s="T523">jüngerer.Bruder-PL-1SG-ACC</ta>
            <ta e="T525" id="Seg_10326" s="T524">sich.kümmern-CVB.SEQ</ta>
            <ta e="T526" id="Seg_10327" s="T525">sitzen.[IMP.2SG]</ta>
            <ta e="T527" id="Seg_10328" s="T526">schlecht-ADVZ</ta>
            <ta e="T528" id="Seg_10329" s="T527">versuchen-FUT-2SG</ta>
            <ta e="T529" id="Seg_10330" s="T528">eins</ta>
            <ta e="T530" id="Seg_10331" s="T529">Tag-EP-INSTR</ta>
            <ta e="T531" id="Seg_10332" s="T530">NEG.EX</ta>
            <ta e="T532" id="Seg_10333" s="T531">machen-CAUS-FUT-1SG</ta>
            <ta e="T533" id="Seg_10334" s="T532">Schwägerin-1SG-ACC</ta>
            <ta e="T534" id="Seg_10335" s="T533">folgen-CVB.SIM</ta>
            <ta e="T535" id="Seg_10336" s="T534">gehen-PST1-1SG</ta>
            <ta e="T536" id="Seg_10337" s="T535">Schwägerin-3SG-ACC</ta>
            <ta e="T537" id="Seg_10338" s="T536">folgen-CVB.SEQ</ta>
            <ta e="T538" id="Seg_10339" s="T537">verlieren-CVB.SEQ</ta>
            <ta e="T539" id="Seg_10340" s="T538">werfen-PRS.[3SG]</ta>
            <ta e="T540" id="Seg_10341" s="T539">was.für.ein</ta>
            <ta e="T541" id="Seg_10342" s="T540">Land.[NOM]</ta>
            <ta e="T542" id="Seg_10343" s="T541">Ende-3SG-DAT/LOC</ta>
            <ta e="T543" id="Seg_10344" s="T542">sehen-PRS.[3SG]</ta>
            <ta e="T544" id="Seg_10345" s="T543">erreichen-PTCP.FUT</ta>
            <ta e="T545" id="Seg_10346" s="T544">verfolgen-NEG.[3SG]</ta>
            <ta e="T546" id="Seg_10347" s="T545">sich.drehen-CAUS-CVB.SEQ</ta>
            <ta e="T547" id="Seg_10348" s="T546">holen-CVB.SEQ</ta>
            <ta e="T548" id="Seg_10349" s="T547">Haus-3SG-ACC</ta>
            <ta e="T549" id="Seg_10350" s="T548">zu</ta>
            <ta e="T550" id="Seg_10351" s="T549">versperren-PRS.[3SG]</ta>
            <ta e="T551" id="Seg_10352" s="T550">greifen-PTCP.FUT.[NOM]</ta>
            <ta e="T552" id="Seg_10353" s="T551">und</ta>
            <ta e="T553" id="Seg_10354" s="T552">trotz</ta>
            <ta e="T554" id="Seg_10355" s="T553">greifen-NEG.CVB.SIM</ta>
            <ta e="T555" id="Seg_10356" s="T554">Schwägerin-3SG-GEN</ta>
            <ta e="T556" id="Seg_10357" s="T555">Hinterteil-3SG-ABL</ta>
            <ta e="T557" id="Seg_10358" s="T556">hineingehen-PRS.[3SG]</ta>
            <ta e="T558" id="Seg_10359" s="T557">Schwägerin-3SG.[NOM]</ta>
            <ta e="T559" id="Seg_10360" s="T558">Kind-3SG-ACC</ta>
            <ta e="T560" id="Seg_10361" s="T559">Busen-VBZ-CVB.SIM</ta>
            <ta e="T561" id="Seg_10362" s="T560">sitzen-PRS.[3SG]</ta>
            <ta e="T562" id="Seg_10363" s="T561">jüngerer.Bruder-3PL.[NOM]</ta>
            <ta e="T563" id="Seg_10364" s="T562">essen-CVB.SIM</ta>
            <ta e="T564" id="Seg_10365" s="T563">sitzen-PRS.[3SG]</ta>
            <ta e="T565" id="Seg_10366" s="T564">doch</ta>
            <ta e="T566" id="Seg_10367" s="T565">Schwägerin.[NOM]</ta>
            <ta e="T567" id="Seg_10368" s="T566">Schuld-2SG-ACC</ta>
            <ta e="T568" id="Seg_10369" s="T567">vorbeigehen-EP-CAUS-PST1-2SG</ta>
            <ta e="T569" id="Seg_10370" s="T568">Kind-2SG-ACC</ta>
            <ta e="T570" id="Seg_10371" s="T569">nehmen-NEG.PTCP.PST-EP-2SG.[NOM]</ta>
            <ta e="T571" id="Seg_10372" s="T570">sein-COND.[3SG]</ta>
            <ta e="T572" id="Seg_10373" s="T571">töten-PTCP.FUT</ta>
            <ta e="T573" id="Seg_10374" s="T572">sein-PST1-1SG</ta>
            <ta e="T574" id="Seg_10375" s="T573">acht</ta>
            <ta e="T575" id="Seg_10376" s="T574">schwarzes.Rentier.[NOM]</ta>
            <ta e="T576" id="Seg_10377" s="T575">Herr-3SG.[NOM]</ta>
            <ta e="T577" id="Seg_10378" s="T576">Angst.haben-CVB.PURP</ta>
            <ta e="T578" id="Seg_10379" s="T577">Angst.haben-PRS.[3SG]</ta>
            <ta e="T579" id="Seg_10380" s="T578">leben-PRS-3PL</ta>
            <ta e="T580" id="Seg_10381" s="T579">wann</ta>
            <ta e="T581" id="Seg_10382" s="T580">INDEF</ta>
            <ta e="T582" id="Seg_10383" s="T581">drei-ORD</ta>
            <ta e="T583" id="Seg_10384" s="T582">Tag-3SG-DAT/LOC</ta>
            <ta e="T584" id="Seg_10385" s="T583">doch</ta>
            <ta e="T585" id="Seg_10386" s="T584">acht</ta>
            <ta e="T586" id="Seg_10387" s="T585">schwarzes.Rentier.[NOM]</ta>
            <ta e="T587" id="Seg_10388" s="T586">Herr-3SG.[NOM]</ta>
            <ta e="T588" id="Seg_10389" s="T587">Volk-2SG-ACC</ta>
            <ta e="T589" id="Seg_10390" s="T588">sammeln.[IMP.2SG]</ta>
            <ta e="T590" id="Seg_10391" s="T589">Versammlung-VBZ-FUT-1PL</ta>
            <ta e="T591" id="Seg_10392" s="T590">sagen-PRS.[3SG]</ta>
            <ta e="T592" id="Seg_10393" s="T591">Kungadjaj.[NOM]</ta>
            <ta e="T593" id="Seg_10394" s="T592">doch</ta>
            <ta e="T594" id="Seg_10395" s="T593">sammeln-PRS-3PL</ta>
            <ta e="T595" id="Seg_10396" s="T594">sieben</ta>
            <ta e="T596" id="Seg_10397" s="T595">zehn</ta>
            <ta e="T597" id="Seg_10398" s="T596">Zelt.[NOM]</ta>
            <ta e="T598" id="Seg_10399" s="T597">Mensch-3SG-ACC</ta>
            <ta e="T599" id="Seg_10400" s="T598">Kungadjaj.[NOM]</ta>
            <ta e="T600" id="Seg_10401" s="T599">sprechen-PRS.[3SG]</ta>
            <ta e="T601" id="Seg_10402" s="T600">2PL.[NOM]</ta>
            <ta e="T602" id="Seg_10403" s="T601">zwei</ta>
            <ta e="T603" id="Seg_10404" s="T602">Hausherr.[NOM]</ta>
            <ta e="T604" id="Seg_10405" s="T603">es.gibt-2PL</ta>
            <ta e="T605" id="Seg_10406" s="T604">acht</ta>
            <ta e="T606" id="Seg_10407" s="T605">schwarzes.Rentier.[NOM]</ta>
            <ta e="T607" id="Seg_10408" s="T606">Hausherr-3SG.[NOM]</ta>
            <ta e="T608" id="Seg_10409" s="T607">und</ta>
            <ta e="T609" id="Seg_10410" s="T608">sieben</ta>
            <ta e="T610" id="Seg_10411" s="T609">Knopf.[NOM]</ta>
            <ta e="T611" id="Seg_10412" s="T610">Herr-3SG.[NOM]</ta>
            <ta e="T612" id="Seg_10413" s="T611">jetzt</ta>
            <ta e="T613" id="Seg_10414" s="T612">Hausherr.[NOM]</ta>
            <ta e="T614" id="Seg_10415" s="T613">sein-CVB.SEQ</ta>
            <ta e="T615" id="Seg_10416" s="T614">aufhören-PST1-2PL</ta>
            <ta e="T616" id="Seg_10417" s="T615">Hausherr-2PL.[NOM]</ta>
            <ta e="T617" id="Seg_10418" s="T616">dieses.[NOM]</ta>
            <ta e="T618" id="Seg_10419" s="T617">Junge.[NOM]</ta>
            <ta e="T619" id="Seg_10420" s="T618">Kind.[NOM]</ta>
            <ta e="T620" id="Seg_10421" s="T619">älterer.Bruder-EP-1SG.[NOM]</ta>
            <ta e="T621" id="Seg_10422" s="T620">Kind-3SG.[NOM]</ta>
            <ta e="T622" id="Seg_10423" s="T621">sammeln-REFL-EP-IMP.2PL</ta>
            <ta e="T623" id="Seg_10424" s="T622">jeder-2PL.[NOM]</ta>
            <ta e="T624" id="Seg_10425" s="T623">zwei</ta>
            <ta e="T625" id="Seg_10426" s="T624">Tag-DAT/LOC</ta>
            <ta e="T626" id="Seg_10427" s="T625">drei-ORD</ta>
            <ta e="T627" id="Seg_10428" s="T626">Tag-2PL-DAT/LOC</ta>
            <ta e="T628" id="Seg_10429" s="T627">wegfahren-FUT-2PL</ta>
            <ta e="T629" id="Seg_10430" s="T628">führend-2PL.[NOM]</ta>
            <ta e="T630" id="Seg_10431" s="T629">3SG.[NOM]</ta>
            <ta e="T631" id="Seg_10432" s="T630">sein-FUT.[3SG]</ta>
            <ta e="T632" id="Seg_10433" s="T631">fremd</ta>
            <ta e="T633" id="Seg_10434" s="T632">Rentier-ACC</ta>
            <ta e="T634" id="Seg_10435" s="T633">einspannen-EP-NEG.[IMP.2SG]</ta>
            <ta e="T635" id="Seg_10436" s="T634">Vater-2SG.[NOM]</ta>
            <ta e="T636" id="Seg_10437" s="T635">Rentier-3SG-ACC</ta>
            <ta e="T637" id="Seg_10438" s="T636">Vermächtnis-3SG-ACC</ta>
            <ta e="T638" id="Seg_10439" s="T637">einspannen-FUT.[IMP.2SG]</ta>
            <ta e="T639" id="Seg_10440" s="T638">sagen-PRS.[3SG]</ta>
            <ta e="T640" id="Seg_10441" s="T639">Junge-DAT/LOC</ta>
            <ta e="T641" id="Seg_10442" s="T640">Junge.[NOM]</ta>
            <ta e="T642" id="Seg_10443" s="T641">wachsen-PST2.[3SG]</ta>
            <ta e="T643" id="Seg_10444" s="T642">leiten-EP-CAUS-CVB.SEQ-2SG</ta>
            <ta e="T644" id="Seg_10445" s="T643">wegfahren-FUT-2PL</ta>
            <ta e="T645" id="Seg_10446" s="T644">Wald.[NOM]</ta>
            <ta e="T646" id="Seg_10447" s="T645">von.der.Größe.von-3SG-INSTR</ta>
            <ta e="T647" id="Seg_10448" s="T646">Bergrücken.[NOM]</ta>
            <ta e="T648" id="Seg_10449" s="T647">es.gibt</ta>
            <ta e="T649" id="Seg_10450" s="T648">sein-FUT.[3SG]</ta>
            <ta e="T650" id="Seg_10451" s="T649">dieses</ta>
            <ta e="T651" id="Seg_10452" s="T650">Bergrücken.[NOM]</ta>
            <ta e="T652" id="Seg_10453" s="T651">Tundra.[NOM]</ta>
            <ta e="T653" id="Seg_10454" s="T652">in.Richtung</ta>
            <ta e="T654" id="Seg_10455" s="T653">Seite-3SG-INSTR</ta>
            <ta e="T655" id="Seg_10456" s="T654">nomadisieren-FUT-2SG</ta>
            <ta e="T656" id="Seg_10457" s="T655">zwei</ta>
            <ta e="T657" id="Seg_10458" s="T656">Monat-ACC</ta>
            <ta e="T658" id="Seg_10459" s="T657">während</ta>
            <ta e="T659" id="Seg_10460" s="T658">letzter-3SG.[NOM]</ta>
            <ta e="T660" id="Seg_10461" s="T659">aber</ta>
            <ta e="T661" id="Seg_10462" s="T660">Bergrücken-3SG.[NOM]</ta>
            <ta e="T662" id="Seg_10463" s="T661">aufhören-TEMP-3SG</ta>
            <ta e="T663" id="Seg_10464" s="T662">Tundra.[NOM]</ta>
            <ta e="T664" id="Seg_10465" s="T663">sein-FUT.[3SG]</ta>
            <ta e="T665" id="Seg_10466" s="T664">Schnee-3SG-GEN</ta>
            <ta e="T666" id="Seg_10467" s="T665">Unterteil-3SG-ABL</ta>
            <ta e="T667" id="Seg_10468" s="T666">Rauch-PROPR.[NOM]</ta>
            <ta e="T668" id="Seg_10469" s="T667">sein-FUT.[3SG]</ta>
            <ta e="T669" id="Seg_10470" s="T668">jenes.[NOM]</ta>
            <ta e="T670" id="Seg_10471" s="T669">1PL.[NOM]</ta>
            <ta e="T671" id="Seg_10472" s="T670">Vorfahr.[NOM]</ta>
            <ta e="T672" id="Seg_10473" s="T671">Erde-1PL.[NOM]</ta>
            <ta e="T673" id="Seg_10474" s="T672">groß</ta>
            <ta e="T674" id="Seg_10475" s="T673">Weg-DAT/LOC</ta>
            <ta e="T675" id="Seg_10476" s="T674">hineingehen-FUT-2SG</ta>
            <ta e="T676" id="Seg_10477" s="T675">Verwandter-PL-2SG-DAT/LOC</ta>
            <ta e="T677" id="Seg_10478" s="T676">ankommen-FUT-2SG</ta>
            <ta e="T678" id="Seg_10479" s="T677">Leute-PL.[NOM]</ta>
            <ta e="T679" id="Seg_10480" s="T678">Rentier-2PL-ACC</ta>
            <ta e="T680" id="Seg_10481" s="T679">Nacht-ACC-Tag-ACC</ta>
            <ta e="T681" id="Seg_10482" s="T680">während</ta>
            <ta e="T682" id="Seg_10483" s="T681">sehen-FUT-2PL</ta>
            <ta e="T683" id="Seg_10484" s="T682">acht</ta>
            <ta e="T684" id="Seg_10485" s="T683">schwarzes.Rentier.[NOM]</ta>
            <ta e="T685" id="Seg_10486" s="T684">Hausherr-3SG.[NOM]</ta>
            <ta e="T686" id="Seg_10487" s="T685">sieben</ta>
            <ta e="T687" id="Seg_10488" s="T686">Knopf.[NOM]</ta>
            <ta e="T688" id="Seg_10489" s="T687">Herr-3SG.[NOM]</ta>
            <ta e="T689" id="Seg_10490" s="T688">wieder</ta>
            <ta e="T690" id="Seg_10491" s="T689">treffen-FUT-2PL</ta>
            <ta e="T691" id="Seg_10492" s="T690">1SG.[NOM]</ta>
            <ta e="T692" id="Seg_10493" s="T691">aber</ta>
            <ta e="T693" id="Seg_10494" s="T692">dieses</ta>
            <ta e="T694" id="Seg_10495" s="T693">gut-ADVZ</ta>
            <ta e="T695" id="Seg_10496" s="T694">reisen-PTCP.PRS-2PL.[NOM]</ta>
            <ta e="T696" id="Seg_10497" s="T695">wegen</ta>
            <ta e="T697" id="Seg_10498" s="T696">schlecht</ta>
            <ta e="T698" id="Seg_10499" s="T697">Leute-ACC</ta>
            <ta e="T699" id="Seg_10500" s="T698">Tschangit-PL-ACC</ta>
            <ta e="T700" id="Seg_10501" s="T699">Tier-PL-ACC</ta>
            <ta e="T701" id="Seg_10502" s="T700">NEG.EX</ta>
            <ta e="T702" id="Seg_10503" s="T701">machen-CVB.SIM</ta>
            <ta e="T703" id="Seg_10504" s="T702">gehen-PST1-1SG</ta>
            <ta e="T704" id="Seg_10505" s="T703">jenes</ta>
            <ta e="T705" id="Seg_10506" s="T704">Ort-DAT/LOC</ta>
            <ta e="T706" id="Seg_10507" s="T705">ankommen-PTCP.PRS</ta>
            <ta e="T707" id="Seg_10508" s="T706">Tag-2SG-DAT/LOC</ta>
            <ta e="T708" id="Seg_10509" s="T707">ankommen-FUT-1SG</ta>
            <ta e="T709" id="Seg_10510" s="T708">groß-ADVZ</ta>
            <ta e="T710" id="Seg_10511" s="T709">sich.verspäten-TEMP-1SG</ta>
            <ta e="T711" id="Seg_10512" s="T710">drei-ORD</ta>
            <ta e="T712" id="Seg_10513" s="T711">Tag-3SG-DAT/LOC</ta>
            <ta e="T713" id="Seg_10514" s="T712">ankommen-FUT-1SG</ta>
            <ta e="T714" id="Seg_10515" s="T713">sagen-PRS.[3SG]</ta>
            <ta e="T715" id="Seg_10516" s="T714">drei-ORD</ta>
            <ta e="T716" id="Seg_10517" s="T715">Tag-3SG-DAT/LOC</ta>
            <ta e="T717" id="Seg_10518" s="T716">wegfahren-PRS-3PL</ta>
            <ta e="T718" id="Seg_10519" s="T717">Kungadjaj.[NOM]</ta>
            <ta e="T719" id="Seg_10520" s="T718">weggehen-CVB.SEQ</ta>
            <ta e="T720" id="Seg_10521" s="T719">gehen-PRS.[3SG]</ta>
            <ta e="T721" id="Seg_10522" s="T720">ganz</ta>
            <ta e="T722" id="Seg_10523" s="T721">zu.Fuß</ta>
            <ta e="T723" id="Seg_10524" s="T722">dieses</ta>
            <ta e="T724" id="Seg_10525" s="T723">Kind.[NOM]</ta>
            <ta e="T725" id="Seg_10526" s="T724">führend.[NOM]</ta>
            <ta e="T726" id="Seg_10527" s="T725">sein-CVB.SEQ</ta>
            <ta e="T727" id="Seg_10528" s="T726">nomadisieren-CVB.SEQ</ta>
            <ta e="T728" id="Seg_10529" s="T727">fahren-PST1-3PL</ta>
            <ta e="T729" id="Seg_10530" s="T728">hüten-EP-MED-CVB.SEQ</ta>
            <ta e="T730" id="Seg_10531" s="T729">reisen-PRS-3PL</ta>
            <ta e="T731" id="Seg_10532" s="T730">Herr-ACC-Herrin-ACC</ta>
            <ta e="T732" id="Seg_10533" s="T731">ganz-3SG-ACC</ta>
            <ta e="T733" id="Seg_10534" s="T732">hüten-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T734" id="Seg_10535" s="T733">dieses</ta>
            <ta e="T735" id="Seg_10536" s="T734">Junge.[NOM]</ta>
            <ta e="T736" id="Seg_10537" s="T735">Monat-3SG-GEN</ta>
            <ta e="T737" id="Seg_10538" s="T736">Mitte-3SG.[NOM]</ta>
            <ta e="T738" id="Seg_10539" s="T737">werden-PST2.[3SG]</ta>
            <ta e="T739" id="Seg_10540" s="T738">Bergrücken-3SG-ACC</ta>
            <ta e="T740" id="Seg_10541" s="T739">folgen-CVB.SEQ</ta>
            <ta e="T741" id="Seg_10542" s="T740">gehen-INFER-3SG</ta>
            <ta e="T742" id="Seg_10543" s="T741">plötzlich</ta>
            <ta e="T743" id="Seg_10544" s="T742">Tundra.[NOM]</ta>
            <ta e="T744" id="Seg_10545" s="T743">in.Richtung</ta>
            <ta e="T745" id="Seg_10546" s="T744">Bergrücken.[NOM]</ta>
            <ta e="T746" id="Seg_10547" s="T745">weggehen-PRS.[3SG]</ta>
            <ta e="T747" id="Seg_10548" s="T746">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T748" id="Seg_10549" s="T747">Mensch.[NOM]</ta>
            <ta e="T749" id="Seg_10550" s="T748">gehen-PRS.[3SG]</ta>
            <ta e="T750" id="Seg_10551" s="T749">was.für.ein</ta>
            <ta e="T751" id="Seg_10552" s="T750">in.Richtung</ta>
            <ta e="T752" id="Seg_10553" s="T751">Mensch-2SG=Q</ta>
            <ta e="T753" id="Seg_10554" s="T752">was</ta>
            <ta e="T754" id="Seg_10555" s="T753">kommen-FUT.[3SG]=Q</ta>
            <ta e="T755" id="Seg_10556" s="T754">Zelt-ACC-Feuer-ACC</ta>
            <ta e="T756" id="Seg_10557" s="T755">wissen-NEG-1SG</ta>
            <ta e="T757" id="Seg_10558" s="T756">geboren.werden-PTCP.PST</ta>
            <ta e="T758" id="Seg_10559" s="T757">älterer.Bruder-1SG-ACC</ta>
            <ta e="T759" id="Seg_10560" s="T758">verlieren-CVB.SIM</ta>
            <ta e="T760" id="Seg_10561" s="T759">gehen-PRS-1SG</ta>
            <ta e="T761" id="Seg_10562" s="T760">was.für.ein-3PL.[NOM]=Q</ta>
            <ta e="T762" id="Seg_10563" s="T761">jenes.[NOM]</ta>
            <ta e="T763" id="Seg_10564" s="T762">acht</ta>
            <ta e="T764" id="Seg_10565" s="T763">schwarzes.Rentier.[NOM]</ta>
            <ta e="T765" id="Seg_10566" s="T764">Hausherr-3SG.[NOM]</ta>
            <ta e="T766" id="Seg_10567" s="T765">ach.ja</ta>
            <ta e="T767" id="Seg_10568" s="T766">jenes.[NOM]</ta>
            <ta e="T768" id="Seg_10569" s="T767">1SG.[NOM]</ta>
            <ta e="T769" id="Seg_10570" s="T768">Mensch-1SG.[NOM]</ta>
            <ta e="T770" id="Seg_10571" s="T769">jenes.[NOM]</ta>
            <ta e="T771" id="Seg_10572" s="T770">freilaufendes.Hausrentier.[NOM]</ta>
            <ta e="T772" id="Seg_10573" s="T771">jagen-CVB.SEQ</ta>
            <ta e="T773" id="Seg_10574" s="T772">gehen-PRS.[3SG]</ta>
            <ta e="T774" id="Seg_10575" s="T773">doch</ta>
            <ta e="T775" id="Seg_10576" s="T774">dann</ta>
            <ta e="T776" id="Seg_10577" s="T775">1SG-DAT/LOC</ta>
            <ta e="T777" id="Seg_10578" s="T776">Mensch.[NOM]</ta>
            <ta e="T778" id="Seg_10579" s="T777">sein.[IMP.2SG]</ta>
            <ta e="T779" id="Seg_10580" s="T778">sprechen-CVB.SIM</ta>
            <ta e="T780" id="Seg_10581" s="T779">herbeiholen-NEG.[3SG]</ta>
            <ta e="T781" id="Seg_10582" s="T780">wenn</ta>
            <ta e="T782" id="Seg_10583" s="T781">Mensch.[NOM]</ta>
            <ta e="T783" id="Seg_10584" s="T782">sein-FUT-2SG</ta>
            <ta e="T784" id="Seg_10585" s="T783">NEG-3SG</ta>
            <ta e="T785" id="Seg_10586" s="T784">dieses</ta>
            <ta e="T786" id="Seg_10587" s="T785">drei</ta>
            <ta e="T787" id="Seg_10588" s="T786">Zweig-PROPR</ta>
            <ta e="T788" id="Seg_10589" s="T787">Lenkstange-EP-1SG.[NOM]</ta>
            <ta e="T789" id="Seg_10590" s="T788">anderer.von.zwei</ta>
            <ta e="T790" id="Seg_10591" s="T789">Zweig-3SG.[NOM]</ta>
            <ta e="T791" id="Seg_10592" s="T790">Verzierung-3SG.[NOM]</ta>
            <ta e="T792" id="Seg_10593" s="T791">sein-FUT-2SG</ta>
            <ta e="T793" id="Seg_10594" s="T792">zwei-MLTP</ta>
            <ta e="T794" id="Seg_10595" s="T793">Held.[NOM]</ta>
            <ta e="T795" id="Seg_10596" s="T794">Schädeldecke-3SG.[NOM]</ta>
            <ta e="T796" id="Seg_10597" s="T795">Verzierung-PROPR.[NOM]</ta>
            <ta e="T797" id="Seg_10598" s="T796">Mensch.[NOM]</ta>
            <ta e="T798" id="Seg_10599" s="T797">einverstanden.sein-EP-MED-PRS.[3SG]</ta>
            <ta e="T799" id="Seg_10600" s="T798">mitkommen-PRS.[3SG]</ta>
            <ta e="T800" id="Seg_10601" s="T799">nomadisieren-CVB.SEQ</ta>
            <ta e="T801" id="Seg_10602" s="T800">fahren-PST1-3PL</ta>
            <ta e="T802" id="Seg_10603" s="T801">Monat-3PL.[NOM]</ta>
            <ta e="T803" id="Seg_10604" s="T802">enden-PST1-3SG</ta>
            <ta e="T804" id="Seg_10605" s="T803">nächster</ta>
            <ta e="T805" id="Seg_10606" s="T804">Monat.[NOM]</ta>
            <ta e="T806" id="Seg_10607" s="T805">Mitte-3SG.[NOM]</ta>
            <ta e="T807" id="Seg_10608" s="T806">werden-PST1-3SG</ta>
            <ta e="T808" id="Seg_10609" s="T807">wieder</ta>
            <ta e="T809" id="Seg_10610" s="T808">Mensch-ACC</ta>
            <ta e="T810" id="Seg_10611" s="T809">sehen-PST1-3PL</ta>
            <ta e="T811" id="Seg_10612" s="T810">eins</ta>
            <ta e="T812" id="Seg_10613" s="T811">Bergrücken-DIM-DAT/LOC</ta>
            <ta e="T813" id="Seg_10614" s="T812">auch</ta>
            <ta e="T814" id="Seg_10615" s="T813">hallo-VBZ-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T815" id="Seg_10616" s="T814">was.für.ein</ta>
            <ta e="T816" id="Seg_10617" s="T815">in.Richtung</ta>
            <ta e="T817" id="Seg_10618" s="T816">Zelt-PROPR-Feuer-PROPR</ta>
            <ta e="T818" id="Seg_10619" s="T817">Mensch-2SG=Q</ta>
            <ta e="T819" id="Seg_10620" s="T818">Zelt.[NOM]-Feuer.[NOM]</ta>
            <ta e="T820" id="Seg_10621" s="T819">NEG.EX</ta>
            <ta e="T821" id="Seg_10622" s="T820">gehen-PRS-1SG</ta>
            <ta e="T822" id="Seg_10623" s="T821">nur</ta>
            <ta e="T823" id="Seg_10624" s="T822">älterer.Bruder-1SG-ACC</ta>
            <ta e="T824" id="Seg_10625" s="T823">suchen-PRS-1SG</ta>
            <ta e="T825" id="Seg_10626" s="T824">was.[NOM]</ta>
            <ta e="T826" id="Seg_10627" s="T825">sein-PST1-3SG=Q</ta>
            <ta e="T827" id="Seg_10628" s="T826">älterer.Bruder-EP-2SG.[NOM]</ta>
            <ta e="T828" id="Seg_10629" s="T827">hey</ta>
            <ta e="T829" id="Seg_10630" s="T828">sieben</ta>
            <ta e="T830" id="Seg_10631" s="T829">Knopf.[NOM]</ta>
            <ta e="T831" id="Seg_10632" s="T830">Herr-3SG.[NOM]</ta>
            <ta e="T832" id="Seg_10633" s="T831">hey</ta>
            <ta e="T833" id="Seg_10634" s="T832">jenes</ta>
            <ta e="T834" id="Seg_10635" s="T833">freilaufendes.Hausrentier.[NOM]</ta>
            <ta e="T835" id="Seg_10636" s="T834">jagen-CVB.SEQ</ta>
            <ta e="T836" id="Seg_10637" s="T835">gehen-PRS.[3SG]</ta>
            <ta e="T837" id="Seg_10638" s="T836">älterer.Bruder-EP-2SG.[NOM]</ta>
            <ta e="T838" id="Seg_10639" s="T837">na</ta>
            <ta e="T839" id="Seg_10640" s="T838">1SG-DAT/LOC</ta>
            <ta e="T840" id="Seg_10641" s="T839">Mensch.[NOM]</ta>
            <ta e="T841" id="Seg_10642" s="T840">sein-FUT-2SG</ta>
            <ta e="T842" id="Seg_10643" s="T841">sagen-NEG.[3SG]</ta>
            <ta e="T843" id="Seg_10644" s="T842">sein-FUT-2SG</ta>
            <ta e="T844" id="Seg_10645" s="T843">NEG-3SG</ta>
            <ta e="T845" id="Seg_10646" s="T844">dieses</ta>
            <ta e="T846" id="Seg_10647" s="T845">Lenkstange-EP-1SG.[NOM]</ta>
            <ta e="T847" id="Seg_10648" s="T846">Zweig-3SG-DAT/LOC</ta>
            <ta e="T848" id="Seg_10649" s="T847">Verzierung.[NOM]</ta>
            <ta e="T849" id="Seg_10650" s="T848">machen-FUT-1SG</ta>
            <ta e="T850" id="Seg_10651" s="T849">einverstanden.sein-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T851" id="Seg_10652" s="T850">eben.der</ta>
            <ta e="T852" id="Seg_10653" s="T851">Mensch.[NOM]</ta>
            <ta e="T853" id="Seg_10654" s="T852">älterer.Bruder-3SG-DAT/LOC</ta>
            <ta e="T854" id="Seg_10655" s="T853">ankommen-PRS.[3SG]</ta>
            <ta e="T855" id="Seg_10656" s="T854">warum</ta>
            <ta e="T856" id="Seg_10657" s="T855">töten-NEG-2PL</ta>
            <ta e="T857" id="Seg_10658" s="T856">Kind-DAT/LOC</ta>
            <ta e="T858" id="Seg_10659" s="T857">Kopf.[NOM]</ta>
            <ta e="T859" id="Seg_10660" s="T858">gehorchen-PRS-2PL</ta>
            <ta e="T860" id="Seg_10661" s="T859">Q</ta>
            <ta e="T861" id="Seg_10662" s="T860">nein</ta>
            <ta e="T862" id="Seg_10663" s="T861">lassen.[IMP.2SG]</ta>
            <ta e="T863" id="Seg_10664" s="T862">jagen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T864" id="Seg_10665" s="T863">man.muss</ta>
            <ta e="T865" id="Seg_10666" s="T864">mutig-3SG.[NOM]</ta>
            <ta e="T866" id="Seg_10667" s="T865">und</ta>
            <ta e="T867" id="Seg_10668" s="T866">sehr</ta>
            <ta e="T868" id="Seg_10669" s="T867">doch</ta>
            <ta e="T869" id="Seg_10670" s="T868">versuchen-FUT-1PL</ta>
            <ta e="T870" id="Seg_10671" s="T869">am.Abend</ta>
            <ta e="T871" id="Seg_10672" s="T870">hinuntergehen-PRS-3PL</ta>
            <ta e="T872" id="Seg_10673" s="T871">eins</ta>
            <ta e="T873" id="Seg_10674" s="T872">Ort-DAT/LOC</ta>
            <ta e="T874" id="Seg_10675" s="T873">essen-PRS-3PL</ta>
            <ta e="T875" id="Seg_10676" s="T874">Junge.[NOM]</ta>
            <ta e="T876" id="Seg_10677" s="T875">Volk-3SG-ACC</ta>
            <ta e="T877" id="Seg_10678" s="T876">folgen-CVB.SEQ</ta>
            <ta e="T878" id="Seg_10679" s="T877">herausnehmen-PRS.[3SG]</ta>
            <ta e="T879" id="Seg_10680" s="T878">Herde.[NOM]</ta>
            <ta e="T880" id="Seg_10681" s="T879">hüten-EP-CAUS-CVB.SIM</ta>
            <ta e="T881" id="Seg_10682" s="T880">Kind.[NOM]</ta>
            <ta e="T882" id="Seg_10683" s="T881">schlafen-CVB.SEQ</ta>
            <ta e="T883" id="Seg_10684" s="T882">bleiben-PRS.[3SG]</ta>
            <ta e="T884" id="Seg_10685" s="T883">Mensch-PL.[NOM]</ta>
            <ta e="T885" id="Seg_10686" s="T884">sich.anschleichen-PTCP.PRS</ta>
            <ta e="T886" id="Seg_10687" s="T885">Stimme-3PL-ACC</ta>
            <ta e="T887" id="Seg_10688" s="T886">hören-PRS.[3SG]</ta>
            <ta e="T888" id="Seg_10689" s="T887">Mensch.[NOM]</ta>
            <ta e="T889" id="Seg_10690" s="T888">Glefe-INSTR</ta>
            <ta e="T890" id="Seg_10691" s="T889">Tür-ACC</ta>
            <ta e="T891" id="Seg_10692" s="T890">etwas.öffnen-PRS.[3SG]</ta>
            <ta e="T892" id="Seg_10693" s="T891">na</ta>
            <ta e="T893" id="Seg_10694" s="T892">warum</ta>
            <ta e="T894" id="Seg_10695" s="T893">hüten-PRS-2PL</ta>
            <ta e="T895" id="Seg_10696" s="T894">1SG-ACC</ta>
            <ta e="T896" id="Seg_10697" s="T895">Herde-ACC</ta>
            <ta e="T897" id="Seg_10698" s="T896">hüten-IMP.2PL</ta>
            <ta e="T898" id="Seg_10699" s="T897">Leute.[NOM]</ta>
            <ta e="T899" id="Seg_10700" s="T898">zurück</ta>
            <ta e="T900" id="Seg_10701" s="T899">sich.verstecken-PRS-3PL</ta>
            <ta e="T901" id="Seg_10702" s="T900">morgendlich</ta>
            <ta e="T902" id="Seg_10703" s="T901">Schlaf-3SG-DAT/LOC</ta>
            <ta e="T903" id="Seg_10704" s="T902">versuchen-FUT-1PL</ta>
            <ta e="T904" id="Seg_10705" s="T903">sagen-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T905" id="Seg_10706" s="T904">wieder</ta>
            <ta e="T906" id="Seg_10707" s="T905">probieren-PRS-3PL</ta>
            <ta e="T907" id="Seg_10708" s="T906">wieder</ta>
            <ta e="T908" id="Seg_10709" s="T907">was-ACC</ta>
            <ta e="T909" id="Seg_10710" s="T908">hüten-PRS-2PL</ta>
            <ta e="T910" id="Seg_10711" s="T909">gehen-EP-IMP.2PL</ta>
            <ta e="T911" id="Seg_10712" s="T910">folgen-CVB.SEQ</ta>
            <ta e="T912" id="Seg_10713" s="T911">schicken-PRS.[3SG]</ta>
            <ta e="T913" id="Seg_10714" s="T912">jenes.EMPH-ABL</ta>
            <ta e="T914" id="Seg_10715" s="T913">berühren-PST2.NEG-3PL</ta>
            <ta e="T915" id="Seg_10716" s="T914">zwei</ta>
            <ta e="T916" id="Seg_10717" s="T915">Monat-3PL.[NOM]</ta>
            <ta e="T917" id="Seg_10718" s="T916">überstehen-MED-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T918" id="Seg_10719" s="T917">Bergrücken-3PL.[NOM]</ta>
            <ta e="T919" id="Seg_10720" s="T918">letzter-3SG.[NOM]</ta>
            <ta e="T920" id="Seg_10721" s="T919">sein-PRS.[3SG]</ta>
            <ta e="T921" id="Seg_10722" s="T920">sehen-PST2-3SG</ta>
            <ta e="T922" id="Seg_10723" s="T921">Tundra.[NOM]</ta>
            <ta e="T923" id="Seg_10724" s="T922">werden-PST2.[3SG]</ta>
            <ta e="T924" id="Seg_10725" s="T923">Schnee.[NOM]</ta>
            <ta e="T925" id="Seg_10726" s="T924">Unterteil-3SG-ABL</ta>
            <ta e="T926" id="Seg_10727" s="T925">Rauch-DIM-INTNS-3SG.[NOM]</ta>
            <ta e="T927" id="Seg_10728" s="T926">Tundra.[NOM]</ta>
            <ta e="T928" id="Seg_10729" s="T927">in.Richtung-ABL</ta>
            <ta e="T929" id="Seg_10730" s="T928">riesig</ta>
            <ta e="T930" id="Seg_10731" s="T929">sehr</ta>
            <ta e="T931" id="Seg_10732" s="T930">Pfad.[NOM]</ta>
            <ta e="T932" id="Seg_10733" s="T931">kommen-PRS.[3SG]</ta>
            <ta e="T933" id="Seg_10734" s="T932">Erde.[NOM]</ta>
            <ta e="T934" id="Seg_10735" s="T933">Inneres-3SG-ACC</ta>
            <ta e="T935" id="Seg_10736" s="T934">in.Richtung</ta>
            <ta e="T936" id="Seg_10737" s="T935">gehen-PRS.[3SG]</ta>
            <ta e="T937" id="Seg_10738" s="T936">Pfad.[NOM]</ta>
            <ta e="T938" id="Seg_10739" s="T937">gehen-CVB.SEQ</ta>
            <ta e="T939" id="Seg_10740" s="T938">gehen-PRS-3PL</ta>
            <ta e="T940" id="Seg_10741" s="T939">direkt</ta>
            <ta e="T941" id="Seg_10742" s="T940">Volk-ABL</ta>
            <ta e="T942" id="Seg_10743" s="T941">Volk.[NOM]</ta>
            <ta e="T943" id="Seg_10744" s="T942">Zelt-ABL</ta>
            <ta e="T944" id="Seg_10745" s="T943">Zelt.[NOM]</ta>
            <ta e="T945" id="Seg_10746" s="T944">Erde-DAT/LOC</ta>
            <ta e="T946" id="Seg_10747" s="T945">kommen-PRS-3PL</ta>
            <ta e="T947" id="Seg_10748" s="T946">eins</ta>
            <ta e="T948" id="Seg_10749" s="T947">Zelt.[NOM]</ta>
            <ta e="T949" id="Seg_10750" s="T948">Rand-3SG-DAT/LOC</ta>
            <ta e="T950" id="Seg_10751" s="T949">halten-PRS.[3SG]</ta>
            <ta e="T951" id="Seg_10752" s="T950">eins</ta>
            <ta e="T952" id="Seg_10753" s="T951">alt</ta>
            <ta e="T953" id="Seg_10754" s="T952">EMPH</ta>
            <ta e="T954" id="Seg_10755" s="T953">Frau.[NOM]</ta>
            <ta e="T955" id="Seg_10756" s="T954">hinausgehen-PRS.[3SG]</ta>
            <ta e="T956" id="Seg_10757" s="T955">ui</ta>
            <ta e="T957" id="Seg_10758" s="T956">Rentier-3SG.[NOM]</ta>
            <ta e="T958" id="Seg_10759" s="T957">Wunder.[NOM]</ta>
            <ta e="T959" id="Seg_10760" s="T958">Rentier.[NOM]</ta>
            <ta e="T960" id="Seg_10761" s="T959">1PL.[NOM]</ta>
            <ta e="T961" id="Seg_10762" s="T960">Vorfahr.[NOM]</ta>
            <ta e="T962" id="Seg_10763" s="T961">Rentier-1PL.[NOM]</ta>
            <ta e="T963" id="Seg_10764" s="T962">wie=Q</ta>
            <ta e="T964" id="Seg_10765" s="T963">vor.langer.Zeit</ta>
            <ta e="T965" id="Seg_10766" s="T964">zwei</ta>
            <ta e="T966" id="Seg_10767" s="T965">Mensch.[NOM]</ta>
            <ta e="T967" id="Seg_10768" s="T966">gehen-PST2-3SG</ta>
            <ta e="T968" id="Seg_10769" s="T967">jenes</ta>
            <ta e="T969" id="Seg_10770" s="T968">Geschlecht-3PL.[NOM]</ta>
            <ta e="T970" id="Seg_10771" s="T969">wahrscheinlich</ta>
            <ta e="T971" id="Seg_10772" s="T970">drei</ta>
            <ta e="T972" id="Seg_10773" s="T971">Zelt-ABL</ta>
            <ta e="T973" id="Seg_10774" s="T972">drei</ta>
            <ta e="T974" id="Seg_10775" s="T973">Frau.[NOM]</ta>
            <ta e="T975" id="Seg_10776" s="T974">hinausgehen-CVB.SEQ</ta>
            <ta e="T976" id="Seg_10777" s="T975">wieder</ta>
            <ta e="T977" id="Seg_10778" s="T976">erkennen-PST2-3PL</ta>
            <ta e="T978" id="Seg_10779" s="T977">Geschlecht-3PL-ACC</ta>
            <ta e="T979" id="Seg_10780" s="T978">sich.unterhalten-CVB.SEQ</ta>
            <ta e="T980" id="Seg_10781" s="T979">doch</ta>
            <ta e="T981" id="Seg_10782" s="T980">erfahren-RECP/COLL-PRS-3PL</ta>
            <ta e="T982" id="Seg_10783" s="T981">Verwandter-3PL-ACC</ta>
            <ta e="T983" id="Seg_10784" s="T982">finden-RECP/COLL-PRS-3PL</ta>
            <ta e="T984" id="Seg_10785" s="T983">Kungadjaj-ACC</ta>
            <ta e="T985" id="Seg_10786" s="T984">warten-PRS-3PL</ta>
            <ta e="T986" id="Seg_10787" s="T985">drei-ORD</ta>
            <ta e="T987" id="Seg_10788" s="T986">Tag-3SG-GEN</ta>
            <ta e="T988" id="Seg_10789" s="T987">Mitte-3SG.[NOM]</ta>
            <ta e="T989" id="Seg_10790" s="T988">Kungadjaj.[NOM]</ta>
            <ta e="T990" id="Seg_10791" s="T989">kommen-PRS.[3SG]</ta>
            <ta e="T991" id="Seg_10792" s="T990">Fleisch.[NOM]</ta>
            <ta e="T992" id="Seg_10793" s="T991">Name-3SG.[NOM]</ta>
            <ta e="T993" id="Seg_10794" s="T992">ganz-POSS</ta>
            <ta e="T994" id="Seg_10795" s="T993">NEG</ta>
            <ta e="T995" id="Seg_10796" s="T994">Haut-3SG-GEN</ta>
            <ta e="T996" id="Seg_10797" s="T995">Hälfte-3SG.[NOM]</ta>
            <ta e="T997" id="Seg_10798" s="T996">bleiben-PST2.[3SG]</ta>
            <ta e="T998" id="Seg_10799" s="T997">Kind-1SG.[NOM]</ta>
            <ta e="T999" id="Seg_10800" s="T998">kommen-PST2-2SG</ta>
            <ta e="T1000" id="Seg_10801" s="T999">doch</ta>
            <ta e="T1001" id="Seg_10802" s="T1000">gut.[NOM]</ta>
            <ta e="T1002" id="Seg_10803" s="T1001">schlecht</ta>
            <ta e="T1003" id="Seg_10804" s="T1002">Leute-ACC</ta>
            <ta e="T1004" id="Seg_10805" s="T1003">vernichten-CVB.SEQ</ta>
            <ta e="T1005" id="Seg_10806" s="T1004">Ort.[NOM]</ta>
            <ta e="T1006" id="Seg_10807" s="T1005">Tier-3SG-ACC</ta>
            <ta e="T1007" id="Seg_10808" s="T1006">kaum</ta>
            <ta e="T1008" id="Seg_10809" s="T1007">siegen-PST1-1SG</ta>
            <ta e="T1009" id="Seg_10810" s="T1008">Seele-EP-1SG.[NOM]</ta>
            <ta e="T1010" id="Seg_10811" s="T1009">nur</ta>
            <ta e="T1011" id="Seg_10812" s="T1010">kommen-PST1-1SG</ta>
            <ta e="T1012" id="Seg_10813" s="T1011">Wunde.[NOM]</ta>
            <ta e="T1013" id="Seg_10814" s="T1012">machen-CVB.SEQ-3PL</ta>
            <ta e="T1014" id="Seg_10815" s="T1013">doch</ta>
            <ta e="T1015" id="Seg_10816" s="T1014">leben.[IMP.2SG]</ta>
            <ta e="T1016" id="Seg_10817" s="T1015">was-ABL</ta>
            <ta e="T1017" id="Seg_10818" s="T1016">NEG</ta>
            <ta e="T1018" id="Seg_10819" s="T1017">Angst.haben-EP-NEG.[IMP.2SG]</ta>
            <ta e="T1019" id="Seg_10820" s="T1018">Kungadjaj.[NOM]</ta>
            <ta e="T1020" id="Seg_10821" s="T1019">doch</ta>
            <ta e="T1021" id="Seg_10822" s="T1020">später</ta>
            <ta e="T1022" id="Seg_10823" s="T1021">wieder.aufleben-PRS.[3SG]</ta>
            <ta e="T1023" id="Seg_10824" s="T1022">Junge.[NOM]</ta>
            <ta e="T1024" id="Seg_10825" s="T1023">Hausherr.[NOM]</ta>
            <ta e="T1025" id="Seg_10826" s="T1024">werden-CVB.SEQ</ta>
            <ta e="T1026" id="Seg_10827" s="T1025">leben-PST2-3SG</ta>
            <ta e="T1027" id="Seg_10828" s="T1026">dieses</ta>
            <ta e="T1028" id="Seg_10829" s="T1027">Volk-DAT/LOC</ta>
            <ta e="T1029" id="Seg_10830" s="T1028">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_10831" s="T0">три</ta>
            <ta e="T2" id="Seg_10832" s="T1">брат-PL.[NOM]</ta>
            <ta e="T3" id="Seg_10833" s="T2">жить-PST2-3PL</ta>
            <ta e="T4" id="Seg_10834" s="T3">один-3PL.[NOM]</ta>
            <ta e="T5" id="Seg_10835" s="T4">маленький</ta>
            <ta e="T6" id="Seg_10836" s="T5">крошечный</ta>
            <ta e="T7" id="Seg_10837" s="T6">человек.[NOM]</ta>
            <ta e="T8" id="Seg_10838" s="T7">средний-3PL.[NOM]</ta>
            <ta e="T9" id="Seg_10839" s="T8">идти-NEG.[3SG]</ta>
            <ta e="T10" id="Seg_10840" s="T9">колено-3SG-INSTR</ta>
            <ta e="T11" id="Seg_10841" s="T10">только</ta>
            <ta e="T12" id="Seg_10842" s="T11">ползать-PRS.[3SG]</ta>
            <ta e="T13" id="Seg_10843" s="T12">Кунгаджай.[NOM]</ta>
            <ta e="T14" id="Seg_10844" s="T13">говорить-CVB.SEQ</ta>
            <ta e="T15" id="Seg_10845" s="T14">старший.брат-3PL.[NOM]</ta>
            <ta e="T16" id="Seg_10846" s="T15">имя-POSS</ta>
            <ta e="T17" id="Seg_10847" s="T16">NEG</ta>
            <ta e="T18" id="Seg_10848" s="T17">охотник.[NOM]</ta>
            <ta e="T19" id="Seg_10849" s="T18">жена-PROPR.[NOM]</ta>
            <ta e="T20" id="Seg_10850" s="T19">один</ta>
            <ta e="T21" id="Seg_10851" s="T20">лиса.[NOM]</ta>
            <ta e="T22" id="Seg_10852" s="T21">ребенок-PROPR.[NOM]</ta>
            <ta e="T23" id="Seg_10853" s="T22">олень-3PL.[NOM]</ta>
            <ta e="T24" id="Seg_10854" s="T23">EMPH-одинокий.[NOM]</ta>
            <ta e="T25" id="Seg_10855" s="T24">лошадь.[NOM]</ta>
            <ta e="T26" id="Seg_10856" s="T25">подобно</ta>
            <ta e="T27" id="Seg_10857" s="T26">время-DAT/LOC</ta>
            <ta e="T28" id="Seg_10858" s="T27">запрячь-NEG-3PL</ta>
            <ta e="T29" id="Seg_10859" s="T28">кочевать-TEMP-3PL</ta>
            <ta e="T30" id="Seg_10860" s="T29">только</ta>
            <ta e="T31" id="Seg_10861" s="T30">чум-3PL-ACC</ta>
            <ta e="T32" id="Seg_10862" s="T31">тянуть-CAUS-PRS-3PL</ta>
            <ta e="T33" id="Seg_10863" s="T32">старший.брат-3PL.[NOM]</ta>
            <ta e="T34" id="Seg_10864" s="T33">имя-PROPR</ta>
            <ta e="T35" id="Seg_10865" s="T34">охотник.[NOM]</ta>
            <ta e="T36" id="Seg_10866" s="T35">дикий.олень-ACC</ta>
            <ta e="T37" id="Seg_10867" s="T36">охотить-PRS.[3SG]</ta>
            <ta e="T38" id="Seg_10868" s="T37">рыба-ACC</ta>
            <ta e="T39" id="Seg_10869" s="T38">EMPH</ta>
            <ta e="T40" id="Seg_10870" s="T39">год.[NOM]</ta>
            <ta e="T41" id="Seg_10871" s="T40">весна-INCH-PTCP.PRS-3SG-ACC</ta>
            <ta e="T42" id="Seg_10872" s="T41">во.время</ta>
            <ta e="T43" id="Seg_10873" s="T42">старший.брат-3PL.[NOM]</ta>
            <ta e="T44" id="Seg_10874" s="T43">быть.больным-PRS.[3SG]</ta>
            <ta e="T45" id="Seg_10875" s="T44">усиливаться-PST2.[3SG]</ta>
            <ta e="T46" id="Seg_10876" s="T45">три-ORD</ta>
            <ta e="T47" id="Seg_10877" s="T46">день-3SG-DAT/LOC</ta>
            <ta e="T48" id="Seg_10878" s="T47">вовсе</ta>
            <ta e="T49" id="Seg_10879" s="T48">слабеть-PST2.[3SG]</ta>
            <ta e="T50" id="Seg_10880" s="T49">завет.[NOM]</ta>
            <ta e="T51" id="Seg_10881" s="T50">говорить-PST2.[3SG]</ta>
            <ta e="T52" id="Seg_10882" s="T51">жена-3SG-DAT/LOC</ta>
            <ta e="T53" id="Seg_10883" s="T52">вот</ta>
            <ta e="T54" id="Seg_10884" s="T53">1SG.[NOM]</ta>
            <ta e="T55" id="Seg_10885" s="T54">умирать-PST1-1SG</ta>
            <ta e="T56" id="Seg_10886" s="T55">кто-EP-2SG.[NOM]</ta>
            <ta e="T57" id="Seg_10887" s="T56">заботиться-FUT-3SG=Q</ta>
            <ta e="T58" id="Seg_10888" s="T57">охотить-PTCP.PST</ta>
            <ta e="T59" id="Seg_10889" s="T58">добыча-1SG-ACC</ta>
            <ta e="T60" id="Seg_10890" s="T59">лето-ACC</ta>
            <ta e="T61" id="Seg_10891" s="T60">в.течении</ta>
            <ta e="T62" id="Seg_10892" s="T61">есть-FUT-2PL</ta>
            <ta e="T63" id="Seg_10893" s="T62">пища-2SG-ACC</ta>
            <ta e="T64" id="Seg_10894" s="T63">истратить-TEMP-2SG</ta>
            <ta e="T65" id="Seg_10895" s="T64">будущий.год.[NOM]</ta>
            <ta e="T66" id="Seg_10896" s="T65">кожа-3SG-ACC</ta>
            <ta e="T67" id="Seg_10897" s="T66">камыс-3SG-ACC</ta>
            <ta e="T68" id="Seg_10898" s="T67">тонкий.слой.кожы-VBZ-CVB.SEQ</ta>
            <ta e="T69" id="Seg_10899" s="T68">суп-VBZ-CVB.SEQ</ta>
            <ta e="T70" id="Seg_10900" s="T69">идти-FUT.[IMP.2SG]</ta>
            <ta e="T71" id="Seg_10901" s="T70">тот.[NOM]</ta>
            <ta e="T72" id="Seg_10902" s="T71">семья-EP-2SG.[NOM]</ta>
            <ta e="T73" id="Seg_10903" s="T72">польза-POSS</ta>
            <ta e="T74" id="Seg_10904" s="T73">NEG</ta>
            <ta e="T75" id="Seg_10905" s="T74">семья.[NOM]</ta>
            <ta e="T76" id="Seg_10906" s="T75">умирать-PTCP.PST-2SG-INSTR</ta>
            <ta e="T77" id="Seg_10907" s="T76">умирать-PRS-2SG</ta>
            <ta e="T78" id="Seg_10908" s="T77">ребенок-1SG.[NOM]</ta>
            <ta e="T79" id="Seg_10909" s="T78">можно</ta>
            <ta e="T80" id="Seg_10910" s="T79">человек.[NOM]</ta>
            <ta e="T81" id="Seg_10911" s="T80">быть-FUT.[3SG]</ta>
            <ta e="T82" id="Seg_10912" s="T81">умирать-NEG-TEMP-3SG</ta>
            <ta e="T83" id="Seg_10913" s="T82">3SG.[NOM]</ta>
            <ta e="T84" id="Seg_10914" s="T83">кормить-FUT.[3SG]</ta>
            <ta e="T85" id="Seg_10915" s="T84">последний-3SG.[NOM]</ta>
            <ta e="T86" id="Seg_10916" s="T85">умирать-CVB.SEQ</ta>
            <ta e="T87" id="Seg_10917" s="T86">оставаться-PRS.[3SG]</ta>
            <ta e="T88" id="Seg_10918" s="T87">плакать-EP-MED-CVB.SIM-плакать-EP-MED-CVB.SIM</ta>
            <ta e="T89" id="Seg_10919" s="T88">жена.[NOM]</ta>
            <ta e="T90" id="Seg_10920" s="T89">класть-CVB.SEQ</ta>
            <ta e="T91" id="Seg_10921" s="T90">бросать-PRS.[3SG]</ta>
            <ta e="T92" id="Seg_10922" s="T91">этот</ta>
            <ta e="T93" id="Seg_10923" s="T92">лето-3SG.[NOM]</ta>
            <ta e="T94" id="Seg_10924" s="T93">становиться-PRS.[3SG]</ta>
            <ta e="T95" id="Seg_10925" s="T94">пища-3PL-ACC</ta>
            <ta e="T96" id="Seg_10926" s="T95">высыхать-EP-CAUS-EP-MED-PRS-3PL</ta>
            <ta e="T97" id="Seg_10927" s="T96">лето-3PL-ACC</ta>
            <ta e="T98" id="Seg_10928" s="T97">пережить-PRS-3PL</ta>
            <ta e="T99" id="Seg_10929" s="T98">осень-3PL-DAT/LOC</ta>
            <ta e="T100" id="Seg_10930" s="T99">камыс-3PL-ACC</ta>
            <ta e="T101" id="Seg_10931" s="T100">кожа-3PL-ACC</ta>
            <ta e="T102" id="Seg_10932" s="T101">есть-PRS-3PL</ta>
            <ta e="T103" id="Seg_10933" s="T102">каждый-3SG.[NOM]</ta>
            <ta e="T104" id="Seg_10934" s="T103">кончаться-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_10935" s="T104">зима.[NOM]</ta>
            <ta e="T106" id="Seg_10936" s="T105">становиться-PRS.[3SG]</ta>
            <ta e="T107" id="Seg_10937" s="T106">есть-PTCP.FUT</ta>
            <ta e="T108" id="Seg_10938" s="T107">дело-3PL.[NOM]</ta>
            <ta e="T109" id="Seg_10939" s="T108">NEG.EX</ta>
            <ta e="T110" id="Seg_10940" s="T109">ветер-POSS</ta>
            <ta e="T111" id="Seg_10941" s="T110">NEG</ta>
            <ta e="T112" id="Seg_10942" s="T111">день.[NOM]</ta>
            <ta e="T113" id="Seg_10943" s="T112">только</ta>
            <ta e="T114" id="Seg_10944" s="T113">сани-PROPR</ta>
            <ta e="T115" id="Seg_10945" s="T114">звук-3SG-ACC</ta>
            <ta e="T116" id="Seg_10946" s="T115">слышать-PRS.[3SG]</ta>
            <ta e="T117" id="Seg_10947" s="T116">жена.[NOM]</ta>
            <ta e="T118" id="Seg_10948" s="T117">выйти-CVB.SIM</ta>
            <ta e="T119" id="Seg_10949" s="T118">бежать-PRS.[3SG]</ta>
            <ta e="T120" id="Seg_10950" s="T119">голос.[NOM]</ta>
            <ta e="T121" id="Seg_10951" s="T120">только</ta>
            <ta e="T122" id="Seg_10952" s="T121">слышаться-PTCP.PRS</ta>
            <ta e="T123" id="Seg_10953" s="T122">место-3SG-DAT/LOC</ta>
            <ta e="T124" id="Seg_10954" s="T123">останавливаться-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_10955" s="T124">только</ta>
            <ta e="T126" id="Seg_10956" s="T125">жена.[NOM]</ta>
            <ta e="T127" id="Seg_10957" s="T126">отец-3SG.[NOM]</ta>
            <ta e="T128" id="Seg_10958" s="T127">восемь</ta>
            <ta e="T129" id="Seg_10959" s="T128">черный.олень.[NOM]</ta>
            <ta e="T130" id="Seg_10960" s="T129">господин-3SG.[NOM]</ta>
            <ta e="T131" id="Seg_10961" s="T130">хозяин-3SG.[NOM]</ta>
            <ta e="T132" id="Seg_10962" s="T131">князь.[NOM]</ta>
            <ta e="T133" id="Seg_10963" s="T132">быть-PST2.[3SG]</ta>
            <ta e="T134" id="Seg_10964" s="T133">ой</ta>
            <ta e="T135" id="Seg_10965" s="T134">как</ta>
            <ta e="T136" id="Seg_10966" s="T135">жить-PRS-2PL</ta>
            <ta e="T137" id="Seg_10967" s="T136">мужчина-EP-1SG.[NOM]</ta>
            <ta e="T138" id="Seg_10968" s="T137">умирать-PST2-3SG</ta>
            <ta e="T139" id="Seg_10969" s="T138">прошлый.год.[NOM]</ta>
            <ta e="T140" id="Seg_10970" s="T139">ой</ta>
            <ta e="T141" id="Seg_10971" s="T140">слава.богу</ta>
            <ta e="T142" id="Seg_10972" s="T141">очень</ta>
            <ta e="T143" id="Seg_10973" s="T142">радоваться-PST1-1SG</ta>
            <ta e="T144" id="Seg_10974" s="T143">одеваться.[IMP.2SG]</ta>
            <ta e="T145" id="Seg_10975" s="T144">идти-IMP.1DU</ta>
            <ta e="T146" id="Seg_10976" s="T145">1SG.[NOM]</ta>
            <ta e="T147" id="Seg_10977" s="T146">дом-1SG-DAT/LOC</ta>
            <ta e="T148" id="Seg_10978" s="T147">эй</ta>
            <ta e="T149" id="Seg_10979" s="T148">ребенок-1SG-ACC</ta>
            <ta e="T150" id="Seg_10980" s="T149">семья-1SG-ACC</ta>
            <ta e="T151" id="Seg_10981" s="T150">голодать-CVB.SEQ</ta>
            <ta e="T152" id="Seg_10982" s="T151">умирать-IMP.3PL</ta>
            <ta e="T153" id="Seg_10983" s="T152">идти-IMP.1DU</ta>
            <ta e="T154" id="Seg_10984" s="T153">жена.[NOM]</ta>
            <ta e="T155" id="Seg_10985" s="T154">одеваться-CVB.SEQ</ta>
            <ta e="T156" id="Seg_10986" s="T155">доезжать-CVB.SEQ</ta>
            <ta e="T157" id="Seg_10987" s="T156">скакать-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_10988" s="T157">да</ta>
            <ta e="T159" id="Seg_10989" s="T158">мчаться-CVB.SEQ</ta>
            <ta e="T160" id="Seg_10990" s="T159">оставаться-PRS-3PL</ta>
            <ta e="T161" id="Seg_10991" s="T160">Кунгаджай-PROPR</ta>
            <ta e="T162" id="Seg_10992" s="T161">крепко</ta>
            <ta e="T163" id="Seg_10993" s="T162">голодный.[NOM]</ta>
            <ta e="T164" id="Seg_10994" s="T163">лежать-INFER-3PL</ta>
            <ta e="T165" id="Seg_10995" s="T164">полярная.ночь.[NOM]</ta>
            <ta e="T166" id="Seg_10996" s="T165">проехать-CVB.SEQ</ta>
            <ta e="T167" id="Seg_10997" s="T166">посветлеть-PTCP.PRS-DAT/LOC</ta>
            <ta e="T168" id="Seg_10998" s="T167">идти-PRS.[3SG]</ta>
            <ta e="T169" id="Seg_10999" s="T168">Кунгаджай.[NOM]</ta>
            <ta e="T170" id="Seg_11000" s="T169">младший.брат-EP-3SG-DAT/LOC</ta>
            <ta e="T171" id="Seg_11001" s="T170">говорить-PRS.[3SG]</ta>
            <ta e="T172" id="Seg_11002" s="T171">олень-2SG-ACC</ta>
            <ta e="T173" id="Seg_11003" s="T172">запрячь.[IMP.2SG]</ta>
            <ta e="T174" id="Seg_11004" s="T173">лежать-PTCP.PST-EP-INSTR</ta>
            <ta e="T175" id="Seg_11005" s="T174">умирать-FUT-1PL</ta>
            <ta e="T176" id="Seg_11006" s="T175">восемь</ta>
            <ta e="T177" id="Seg_11007" s="T176">черный.олень-ACC</ta>
            <ta e="T178" id="Seg_11008" s="T177">следовать-IMP.1DU</ta>
            <ta e="T179" id="Seg_11009" s="T178">олень-3PL-ACC</ta>
            <ta e="T180" id="Seg_11010" s="T179">запрячь-CVB.SEQ</ta>
            <ta e="T181" id="Seg_11011" s="T180">чум-3PL-ACC</ta>
            <ta e="T182" id="Seg_11012" s="T181">разобрать-CVB.SEQ</ta>
            <ta e="T183" id="Seg_11013" s="T182">скакать-CVB.SEQ</ta>
            <ta e="T184" id="Seg_11014" s="T183">бить-EP-CAUS-CVB.SEQ</ta>
            <ta e="T185" id="Seg_11015" s="T184">идти-INFER-3PL</ta>
            <ta e="T186" id="Seg_11016" s="T185">сколько</ta>
            <ta e="T187" id="Seg_11017" s="T186">десять-MLTP</ta>
            <ta e="T188" id="Seg_11018" s="T187">кочевать-PST2-3PL</ta>
            <ta e="T189" id="Seg_11019" s="T188">однажды</ta>
            <ta e="T190" id="Seg_11020" s="T189">высокий</ta>
            <ta e="T191" id="Seg_11021" s="T190">лесистая.гора-DAT/LOC</ta>
            <ta e="T192" id="Seg_11022" s="T191">лезть-EP-PST2-3PL</ta>
            <ta e="T193" id="Seg_11023" s="T192">заглянуть-PST2-3PL</ta>
            <ta e="T194" id="Seg_11024" s="T193">огромный-PROPR</ta>
            <ta e="T195" id="Seg_11025" s="T194">широкая.равнина.[NOM]</ta>
            <ta e="T196" id="Seg_11026" s="T195">широкая.равнина.[NOM]</ta>
            <ta e="T197" id="Seg_11027" s="T196">середина-3SG-DAT/LOC</ta>
            <ta e="T198" id="Seg_11028" s="T197">стадо.[NOM]</ta>
            <ta e="T199" id="Seg_11029" s="T198">очень.много</ta>
            <ta e="T200" id="Seg_11030" s="T199">широкая.равнина.[NOM]</ta>
            <ta e="T201" id="Seg_11031" s="T200">на.той.стороне</ta>
            <ta e="T202" id="Seg_11032" s="T201">семь</ta>
            <ta e="T203" id="Seg_11033" s="T202">десять</ta>
            <ta e="T204" id="Seg_11034" s="T203">чум.[NOM]</ta>
            <ta e="T205" id="Seg_11035" s="T204">MOD</ta>
            <ta e="T206" id="Seg_11036" s="T205">быть.видно-PRS.[3SG]</ta>
            <ta e="T207" id="Seg_11037" s="T206">идти-PST2-3PL</ta>
            <ta e="T208" id="Seg_11038" s="T207">стадо.[NOM]</ta>
            <ta e="T209" id="Seg_11039" s="T208">нутро-3SG-INSTR</ta>
            <ta e="T210" id="Seg_11040" s="T209">стадо.[NOM]</ta>
            <ta e="T211" id="Seg_11041" s="T210">другой.из.двух</ta>
            <ta e="T212" id="Seg_11042" s="T211">сторона-3SG-DAT/LOC</ta>
            <ta e="T213" id="Seg_11043" s="T212">стоять.[IMP.2SG]</ta>
            <ta e="T214" id="Seg_11044" s="T213">говорить-PST2.[3SG]</ta>
            <ta e="T215" id="Seg_11045" s="T214">Кунгаджай.[NOM]</ta>
            <ta e="T216" id="Seg_11046" s="T215">тот.[NOM]</ta>
            <ta e="T217" id="Seg_11047" s="T216">чум-PL-ABL</ta>
            <ta e="T218" id="Seg_11048" s="T217">один</ta>
            <ta e="T219" id="Seg_11049" s="T218">лучший-3PL-DAT/LOC</ta>
            <ta e="T220" id="Seg_11050" s="T219">останавливаться-FUT.[IMP.2SG]</ta>
            <ta e="T221" id="Seg_11051" s="T220">место.где.рубят.дрова-3PL-ACC</ta>
            <ta e="T222" id="Seg_11052" s="T221">та.сторона.[NOM]</ta>
            <ta e="T223" id="Seg_11053" s="T222">сторона-DIM-3SG-DAT/LOC</ta>
            <ta e="T224" id="Seg_11054" s="T223">видеть-PST2-3SG</ta>
            <ta e="T225" id="Seg_11055" s="T224">один</ta>
            <ta e="T226" id="Seg_11056" s="T225">дом.[NOM]</ta>
            <ta e="T227" id="Seg_11057" s="T226">каждый-3PL-ABL</ta>
            <ta e="T228" id="Seg_11058" s="T227">большой.[NOM]</ta>
            <ta e="T229" id="Seg_11059" s="T228">вход.[NOM]</ta>
            <ta e="T230" id="Seg_11060" s="T229">сторона-3SG-DAT/LOC</ta>
            <ta e="T231" id="Seg_11061" s="T230">доезжать-PST2-3PL</ta>
            <ta e="T232" id="Seg_11062" s="T231">да</ta>
            <ta e="T233" id="Seg_11063" s="T232">олень-3PL-ACC</ta>
            <ta e="T234" id="Seg_11064" s="T233">пустить-CVB.SEQ</ta>
            <ta e="T235" id="Seg_11065" s="T234">бросать-PRS-3PL</ta>
            <ta e="T236" id="Seg_11066" s="T235">чум.[NOM]</ta>
            <ta e="T237" id="Seg_11067" s="T236">построить-CVB.SEQ</ta>
            <ta e="T238" id="Seg_11068" s="T237">бросать-PST2-3PL</ta>
            <ta e="T239" id="Seg_11069" s="T238">зажигать-EP-PST2-3PL</ta>
            <ta e="T240" id="Seg_11070" s="T239">младший.брат-EP-3SG-ACC</ta>
            <ta e="T241" id="Seg_11071" s="T240">идти.[IMP.2SG]</ta>
            <ta e="T242" id="Seg_11072" s="T241">невестка-2SG-DAT/LOC</ta>
            <ta e="T243" id="Seg_11073" s="T242">ребенок-3SG-ACC</ta>
            <ta e="T244" id="Seg_11074" s="T243">сосать-CAUS-CVB.SIM</ta>
            <ta e="T245" id="Seg_11075" s="T244">приходить-IMP.3SG</ta>
            <ta e="T246" id="Seg_11076" s="T245">восемь</ta>
            <ta e="T247" id="Seg_11077" s="T246">черный.олень-DAT/LOC</ta>
            <ta e="T248" id="Seg_11078" s="T247">говорить.[IMP.2SG]</ta>
            <ta e="T249" id="Seg_11079" s="T248">умирать-PST1-1PL</ta>
            <ta e="T250" id="Seg_11080" s="T249">один</ta>
            <ta e="T251" id="Seg_11081" s="T250">INDEF</ta>
            <ta e="T252" id="Seg_11082" s="T251">сердце.[NOM]</ta>
            <ta e="T253" id="Seg_11083" s="T252">половина-DIM-3SG-ACC</ta>
            <ta e="T254" id="Seg_11084" s="T253">с</ta>
            <ta e="T255" id="Seg_11085" s="T254">печень.[NOM]</ta>
            <ta e="T256" id="Seg_11086" s="T255">вдовец-3SG-ACC</ta>
            <ta e="T257" id="Seg_11087" s="T256">делить-IMP.3SG</ta>
            <ta e="T258" id="Seg_11088" s="T257">говорить-PRS.[3SG]</ta>
            <ta e="T259" id="Seg_11089" s="T258">мальчик.[NOM]</ta>
            <ta e="T260" id="Seg_11090" s="T259">бегать-CVB.SEQ</ta>
            <ta e="T261" id="Seg_11091" s="T260">входить-CVB.SEQ</ta>
            <ta e="T262" id="Seg_11092" s="T261">говорить-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_11093" s="T262">видеть.[IMP.2SG]</ta>
            <ta e="T264" id="Seg_11094" s="T263">только</ta>
            <ta e="T265" id="Seg_11095" s="T264">сосать-CAUS-PTCP.PRS</ta>
            <ta e="T266" id="Seg_11096" s="T265">веспорядок.[NOM]</ta>
            <ta e="T267" id="Seg_11097" s="T266">голодать-CVB.SEQ</ta>
            <ta e="T268" id="Seg_11098" s="T267">умирать-IMP.3SG</ta>
            <ta e="T269" id="Seg_11099" s="T268">говорить-PRS.[3SG]</ta>
            <ta e="T270" id="Seg_11100" s="T269">жена.[NOM]</ta>
            <ta e="T271" id="Seg_11101" s="T270">эй</ta>
            <ta e="T272" id="Seg_11102" s="T271">дедушка</ta>
            <ta e="T273" id="Seg_11103" s="T272">сообщение-VBZ-PST1-3SG</ta>
            <ta e="T274" id="Seg_11104" s="T273">старший.брат-EP-1SG.[NOM]</ta>
            <ta e="T275" id="Seg_11105" s="T274">умирать-PST1-1PL</ta>
            <ta e="T276" id="Seg_11106" s="T275">один</ta>
            <ta e="T277" id="Seg_11107" s="T276">INDEF</ta>
            <ta e="T278" id="Seg_11108" s="T277">сердце.[NOM]</ta>
            <ta e="T279" id="Seg_11109" s="T278">половина-DIM-3SG-ACC</ta>
            <ta e="T280" id="Seg_11110" s="T279">с</ta>
            <ta e="T281" id="Seg_11111" s="T280">печень.[NOM]</ta>
            <ta e="T282" id="Seg_11112" s="T281">вдовец-3SG-ACC</ta>
            <ta e="T283" id="Seg_11113" s="T282">делить-IMP.3SG</ta>
            <ta e="T284" id="Seg_11114" s="T283">как</ta>
            <ta e="T285" id="Seg_11115" s="T284">голодать-CVB.SEQ</ta>
            <ta e="T286" id="Seg_11116" s="T285">умирать-EP-IMP.2PL</ta>
            <ta e="T287" id="Seg_11117" s="T286">послать-NEG-1SG</ta>
            <ta e="T288" id="Seg_11118" s="T287">говорить-PRS.[3SG]</ta>
            <ta e="T289" id="Seg_11119" s="T288">восемь</ta>
            <ta e="T290" id="Seg_11120" s="T289">черный.олень.[NOM]</ta>
            <ta e="T291" id="Seg_11121" s="T290">господин-3SG.[NOM]</ta>
            <ta e="T292" id="Seg_11122" s="T291">спать-EMOT-PRS-3PL</ta>
            <ta e="T293" id="Seg_11123" s="T292">что-ACC</ta>
            <ta e="T294" id="Seg_11124" s="T293">есть-FUT-3PL=Q</ta>
            <ta e="T295" id="Seg_11125" s="T294">вдруг</ta>
            <ta e="T296" id="Seg_11126" s="T295">олень-VBZ-PTCP.PRS</ta>
            <ta e="T297" id="Seg_11127" s="T296">голос.[NOM]</ta>
            <ta e="T298" id="Seg_11128" s="T297">слышаться-PRS.[3SG]</ta>
            <ta e="T299" id="Seg_11129" s="T298">Кунгаджай.[NOM]</ta>
            <ta e="T300" id="Seg_11130" s="T299">выйти-CVB.SEQ</ta>
            <ta e="T301" id="Seg_11131" s="T300">мочиться-CVB.SIM</ta>
            <ta e="T302" id="Seg_11132" s="T301">вставать-PST2.[3SG]</ta>
            <ta e="T303" id="Seg_11133" s="T302">колено-3SG-DAT/LOC</ta>
            <ta e="T304" id="Seg_11134" s="T303">олень.[NOM]</ta>
            <ta e="T305" id="Seg_11135" s="T304">очень.много</ta>
            <ta e="T306" id="Seg_11136" s="T305">приходить-PRS.[3SG]</ta>
            <ta e="T307" id="Seg_11137" s="T306">холощеный</ta>
            <ta e="T308" id="Seg_11138" s="T307">олений.самец.[NOM]</ta>
            <ta e="T309" id="Seg_11139" s="T308">жирный.[NOM]</ta>
            <ta e="T310" id="Seg_11140" s="T309">очень</ta>
            <ta e="T311" id="Seg_11141" s="T310">приходить-PRS.[3SG]</ta>
            <ta e="T312" id="Seg_11142" s="T311">мальчик.[NOM]</ta>
            <ta e="T313" id="Seg_11143" s="T312">поймать-CVB.SEQ</ta>
            <ta e="T314" id="Seg_11144" s="T313">взять-PRS.[3SG]</ta>
            <ta e="T315" id="Seg_11145" s="T314">кидаться-CVB.SEQ</ta>
            <ta e="T316" id="Seg_11146" s="T315">очень.много</ta>
            <ta e="T317" id="Seg_11147" s="T316">нож-3SG-ACC</ta>
            <ta e="T318" id="Seg_11148" s="T317">получить-CAUS-PRS.[3SG]</ta>
            <ta e="T319" id="Seg_11149" s="T318">горло-3SG-ACC</ta>
            <ta e="T320" id="Seg_11150" s="T319">резать-CVB.SIM</ta>
            <ta e="T321" id="Seg_11151" s="T320">вытирать-PRS.[3SG]</ta>
            <ta e="T322" id="Seg_11152" s="T321">сдирать.кожу-NEG.CVB.SIM</ta>
            <ta e="T323" id="Seg_11153" s="T322">NEG</ta>
            <ta e="T324" id="Seg_11154" s="T323">внести-PRS-3PL</ta>
            <ta e="T325" id="Seg_11155" s="T324">котел-VBZ-CVB.SEQ</ta>
            <ta e="T326" id="Seg_11156" s="T325">вот</ta>
            <ta e="T327" id="Seg_11157" s="T326">есть-PRS-3PL</ta>
            <ta e="T328" id="Seg_11158" s="T327">день-ACC</ta>
            <ta e="T329" id="Seg_11159" s="T328">целый</ta>
            <ta e="T330" id="Seg_11160" s="T329">вечер-DAT/LOC</ta>
            <ta e="T331" id="Seg_11161" s="T330">пока</ta>
            <ta e="T332" id="Seg_11162" s="T331">этот</ta>
            <ta e="T333" id="Seg_11163" s="T332">чум.[NOM]</ta>
            <ta e="T334" id="Seg_11164" s="T333">край-3SG-DAT/LOC</ta>
            <ta e="T335" id="Seg_11165" s="T334">маленький.[NOM]</ta>
            <ta e="T336" id="Seg_11166" s="T335">чум.[NOM]</ta>
            <ta e="T337" id="Seg_11167" s="T336">стоять-PRS.[3SG]</ta>
            <ta e="T338" id="Seg_11168" s="T337">вот</ta>
            <ta e="T339" id="Seg_11169" s="T338">семь</ta>
            <ta e="T340" id="Seg_11170" s="T339">пуговица.[NOM]</ta>
            <ta e="T341" id="Seg_11171" s="T340">господин-3SG-DAT/LOC</ta>
            <ta e="T342" id="Seg_11172" s="T341">идти.[IMP.2SG]</ta>
            <ta e="T343" id="Seg_11173" s="T342">тот.[NOM]</ta>
            <ta e="T344" id="Seg_11174" s="T343">чум-DAT/LOC</ta>
            <ta e="T345" id="Seg_11175" s="T344">старшина.[NOM]</ta>
            <ta e="T346" id="Seg_11176" s="T345">наверное</ta>
            <ta e="T347" id="Seg_11177" s="T346">пища-PART</ta>
            <ta e="T348" id="Seg_11178" s="T347">попросить.[IMP.2SG]</ta>
            <ta e="T349" id="Seg_11179" s="T348">мальчик.[NOM]</ta>
            <ta e="T350" id="Seg_11180" s="T349">идти-PRS.[3SG]</ta>
            <ta e="T351" id="Seg_11181" s="T350">семь</ta>
            <ta e="T352" id="Seg_11182" s="T351">пуговица.[NOM]</ta>
            <ta e="T353" id="Seg_11183" s="T352">господин-3SG-DAT/LOC</ta>
            <ta e="T354" id="Seg_11184" s="T353">говорить-PRS.[3SG]</ta>
            <ta e="T355" id="Seg_11185" s="T354">сообщение-ACC</ta>
            <ta e="T356" id="Seg_11186" s="T355">восемь</ta>
            <ta e="T357" id="Seg_11187" s="T356">черный.олень.[NOM]</ta>
            <ta e="T358" id="Seg_11188" s="T357">господин-3SG.[NOM]</ta>
            <ta e="T359" id="Seg_11189" s="T358">давать-PST1-3SG</ta>
            <ta e="T360" id="Seg_11190" s="T359">очевидно</ta>
            <ta e="T361" id="Seg_11191" s="T360">нет</ta>
            <ta e="T362" id="Seg_11192" s="T361">ээ</ta>
            <ta e="T363" id="Seg_11193" s="T362">тогда</ta>
            <ta e="T364" id="Seg_11194" s="T363">1SG.[NOM]</ta>
            <ta e="T365" id="Seg_11195" s="T364">тоже</ta>
            <ta e="T366" id="Seg_11196" s="T365">давать-NEG-1SG</ta>
            <ta e="T367" id="Seg_11197" s="T366">есть-PRS-3PL</ta>
            <ta e="T368" id="Seg_11198" s="T367">утром</ta>
            <ta e="T369" id="Seg_11199" s="T368">стадо.[NOM]</ta>
            <ta e="T370" id="Seg_11200" s="T369">идти-PRS.[3SG]</ta>
            <ta e="T371" id="Seg_11201" s="T370">опять</ta>
            <ta e="T372" id="Seg_11202" s="T371">колено-3SG-INSTR</ta>
            <ta e="T373" id="Seg_11203" s="T372">ползать-CVB.SEQ</ta>
            <ta e="T374" id="Seg_11204" s="T373">выйти-PRS.[3SG]</ta>
            <ta e="T375" id="Seg_11205" s="T374">Кунгаджай.[NOM]</ta>
            <ta e="T376" id="Seg_11206" s="T375">Кунгаджай.[NOM]</ta>
            <ta e="T377" id="Seg_11207" s="T376">родить-NEG.PTCP</ta>
            <ta e="T378" id="Seg_11208" s="T377">яловая.важенка-ACC</ta>
            <ta e="T379" id="Seg_11209" s="T378">хватать-CVB.SEQ</ta>
            <ta e="T380" id="Seg_11210" s="T379">взять-PRS.[3SG]</ta>
            <ta e="T381" id="Seg_11211" s="T380">широкая.часть.рога-3SG-ABL</ta>
            <ta e="T382" id="Seg_11212" s="T381">эй=EMPH</ta>
            <ta e="T383" id="Seg_11213" s="T382">нож-ACC</ta>
            <ta e="T384" id="Seg_11214" s="T383">вынимать.[IMP.2SG]</ta>
            <ta e="T385" id="Seg_11215" s="T384">убить-CVB.SEQ</ta>
            <ta e="T386" id="Seg_11216" s="T385">бросать-PRS.[3SG]</ta>
            <ta e="T387" id="Seg_11217" s="T386">опять</ta>
            <ta e="T388" id="Seg_11218" s="T387">сдирать.кожу-NEG.CVB.SIM</ta>
            <ta e="T389" id="Seg_11219" s="T388">есть-PRS-3PL</ta>
            <ta e="T390" id="Seg_11220" s="T389">какой</ta>
            <ta e="T391" id="Seg_11221" s="T390">этот</ta>
            <ta e="T392" id="Seg_11222" s="T391">тот.самый</ta>
            <ta e="T393" id="Seg_11223" s="T392">господин-PL.[NOM]</ta>
            <ta e="T394" id="Seg_11224" s="T393">собственный-3PL-ACC</ta>
            <ta e="T395" id="Seg_11225" s="T394">убить-PTCP.PST</ta>
            <ta e="T396" id="Seg_11226" s="T395">быть-PST2.[3SG]</ta>
            <ta e="T397" id="Seg_11227" s="T396">уснуть-PRS-3PL</ta>
            <ta e="T398" id="Seg_11228" s="T397">утром</ta>
            <ta e="T399" id="Seg_11229" s="T398">Кунгаджай.[NOM]</ta>
            <ta e="T400" id="Seg_11230" s="T399">выйти-EP-PST2-3SG</ta>
            <ta e="T401" id="Seg_11231" s="T400">восемь</ta>
            <ta e="T402" id="Seg_11232" s="T401">черный.олень.[NOM]</ta>
            <ta e="T403" id="Seg_11233" s="T402">хозяин-3SG.[NOM]</ta>
            <ta e="T404" id="Seg_11234" s="T403">хорей-3SG-ACC</ta>
            <ta e="T405" id="Seg_11235" s="T404">подстругивать-CVB.SIM</ta>
            <ta e="T406" id="Seg_11236" s="T405">сидеть-PRS.[3SG]</ta>
            <ta e="T407" id="Seg_11237" s="T406">ой</ta>
            <ta e="T408" id="Seg_11238" s="T407">восемь</ta>
            <ta e="T409" id="Seg_11239" s="T408">черный.олень.[NOM]</ta>
            <ta e="T410" id="Seg_11240" s="T409">господин-3SG.[NOM]</ta>
            <ta e="T411" id="Seg_11241" s="T410">здравствуй</ta>
            <ta e="T412" id="Seg_11242" s="T411">еще</ta>
            <ta e="T413" id="Seg_11243" s="T412">здравствуй</ta>
            <ta e="T414" id="Seg_11244" s="T413">сегодня</ta>
            <ta e="T415" id="Seg_11245" s="T414">убить-ITER-PRS-1PL</ta>
            <ta e="T416" id="Seg_11246" s="T415">последний</ta>
            <ta e="T417" id="Seg_11247" s="T416">олень-PL-1PL-ABL</ta>
            <ta e="T418" id="Seg_11248" s="T417">есть-CVB.SIM</ta>
            <ta e="T419" id="Seg_11249" s="T418">ложиться-PRS-2PL</ta>
            <ta e="T420" id="Seg_11250" s="T419">сам-2PL.[NOM]</ta>
            <ta e="T421" id="Seg_11251" s="T420">вина-2PL.[NOM]</ta>
            <ta e="T422" id="Seg_11252" s="T421">восемь</ta>
            <ta e="T423" id="Seg_11253" s="T422">черный.олень.[NOM]</ta>
            <ta e="T424" id="Seg_11254" s="T423">господин-3SG.[NOM]</ta>
            <ta e="T425" id="Seg_11255" s="T424">колено-VBZ-CVB.SEQ</ta>
            <ta e="T426" id="Seg_11256" s="T425">стоять-PTCP.PRS</ta>
            <ta e="T427" id="Seg_11257" s="T426">человек-ACC</ta>
            <ta e="T428" id="Seg_11258" s="T427">хорей.[NOM]</ta>
            <ta e="T429" id="Seg_11259" s="T428">копье-3SG-INSTR</ta>
            <ta e="T430" id="Seg_11260" s="T429">бедро-DAT/LOC</ta>
            <ta e="T431" id="Seg_11261" s="T430">ударить-PRS.[3SG]</ta>
            <ta e="T432" id="Seg_11262" s="T431">да</ta>
            <ta e="T433" id="Seg_11263" s="T432">не.мочь-CVB.SEQ</ta>
            <ta e="T434" id="Seg_11264" s="T433">бросать-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_11265" s="T434">Кунгаджай-3SG.[NOM]</ta>
            <ta e="T436" id="Seg_11266" s="T435">снег.[NOM]</ta>
            <ta e="T437" id="Seg_11267" s="T436">нижняя.часть-3SG-INSTR</ta>
            <ta e="T438" id="Seg_11268" s="T437">идти-PST2.[3SG]</ta>
            <ta e="T439" id="Seg_11269" s="T438">семь</ta>
            <ta e="T440" id="Seg_11270" s="T439">пуговица.[NOM]</ta>
            <ta e="T441" id="Seg_11271" s="T440">господин-3SG.[NOM]</ta>
            <ta e="T442" id="Seg_11272" s="T441">хорей.[NOM]</ta>
            <ta e="T443" id="Seg_11273" s="T442">подстругивать-CVB.SIM</ta>
            <ta e="T444" id="Seg_11274" s="T443">сидеть-TEMP-3SG</ta>
            <ta e="T445" id="Seg_11275" s="T444">выйти-PRS.[3SG]</ta>
            <ta e="T446" id="Seg_11276" s="T445">ой</ta>
            <ta e="T447" id="Seg_11277" s="T446">семь</ta>
            <ta e="T448" id="Seg_11278" s="T447">пуговица.[NOM]</ta>
            <ta e="T449" id="Seg_11279" s="T448">господин-3SG.[NOM]</ta>
            <ta e="T450" id="Seg_11280" s="T449">здравствуй</ta>
            <ta e="T451" id="Seg_11281" s="T450">еще</ta>
            <ta e="T452" id="Seg_11282" s="T451">здравствуй</ta>
            <ta e="T453" id="Seg_11283" s="T452">сегодня</ta>
            <ta e="T454" id="Seg_11284" s="T453">убить-ITER-PRS-1PL</ta>
            <ta e="T455" id="Seg_11285" s="T454">последний</ta>
            <ta e="T456" id="Seg_11286" s="T455">олень-PL-1PL-ACC</ta>
            <ta e="T457" id="Seg_11287" s="T456">есть-CVB.SIM</ta>
            <ta e="T458" id="Seg_11288" s="T457">ложиться-PRS-2PL</ta>
            <ta e="T459" id="Seg_11289" s="T458">сам-2PL.[NOM]</ta>
            <ta e="T460" id="Seg_11290" s="T459">вина-2PL.[NOM]</ta>
            <ta e="T461" id="Seg_11291" s="T460">семь</ta>
            <ta e="T462" id="Seg_11292" s="T461">пуговица.[NOM]</ta>
            <ta e="T463" id="Seg_11293" s="T462">господин-3SG.[NOM]</ta>
            <ta e="T464" id="Seg_11294" s="T463">колено-VBZ-CVB.SEQ</ta>
            <ta e="T465" id="Seg_11295" s="T464">стоять-PTCP.PRS</ta>
            <ta e="T466" id="Seg_11296" s="T465">человек-ACC</ta>
            <ta e="T467" id="Seg_11297" s="T466">хорей.[NOM]</ta>
            <ta e="T468" id="Seg_11298" s="T467">копье-3SG-INSTR</ta>
            <ta e="T469" id="Seg_11299" s="T468">бедро-DAT/LOC</ta>
            <ta e="T470" id="Seg_11300" s="T469">толкать-CVB.PURP</ta>
            <ta e="T471" id="Seg_11301" s="T470">земля-ACC</ta>
            <ta e="T472" id="Seg_11302" s="T471">ударить-PST2.[3SG]</ta>
            <ta e="T473" id="Seg_11303" s="T472">когда</ta>
            <ta e="T474" id="Seg_11304" s="T473">INDEF</ta>
            <ta e="T475" id="Seg_11305" s="T474">Кунгаджай.[NOM]</ta>
            <ta e="T476" id="Seg_11306" s="T475">следующий</ta>
            <ta e="T477" id="Seg_11307" s="T476">место-EP-INSTR</ta>
            <ta e="T478" id="Seg_11308" s="T477">выйти-CVB.SIM</ta>
            <ta e="T479" id="Seg_11309" s="T478">бежать-PRS.[3SG]</ta>
            <ta e="T480" id="Seg_11310" s="T479">да</ta>
            <ta e="T481" id="Seg_11311" s="T480">два</ta>
            <ta e="T482" id="Seg_11312" s="T481">нога-3SG-DAT/LOC</ta>
            <ta e="T483" id="Seg_11313" s="T482">вставать-CVB.SIM</ta>
            <ta e="T484" id="Seg_11314" s="T483">прыгать-PRS.[3SG]</ta>
            <ta e="T485" id="Seg_11315" s="T484">заглянуть-PST2-3SG</ta>
            <ta e="T486" id="Seg_11316" s="T485">оление.нарты.[NOM]</ta>
            <ta e="T487" id="Seg_11317" s="T486">идти-CVB.SEQ</ta>
            <ta e="T488" id="Seg_11318" s="T487">быть-PRS.[3SG]</ta>
            <ta e="T489" id="Seg_11319" s="T488">невестка-3SG.[NOM]</ta>
            <ta e="T490" id="Seg_11320" s="T489">кочевать-CVB.SEQ</ta>
            <ta e="T491" id="Seg_11321" s="T490">быть-PRS.[3SG]</ta>
            <ta e="T492" id="Seg_11322" s="T491">отец-3SG.[NOM]</ta>
            <ta e="T493" id="Seg_11323" s="T492">семья.[NOM]</ta>
            <ta e="T494" id="Seg_11324" s="T493">давать-PST2.[3SG]</ta>
            <ta e="T495" id="Seg_11325" s="T494">Кунгаджай.[NOM]</ta>
            <ta e="T496" id="Seg_11326" s="T495">невестка-3SG-GEN</ta>
            <ta e="T497" id="Seg_11327" s="T496">повод-3SG-ACC</ta>
            <ta e="T498" id="Seg_11328" s="T497">поймать-CVB.SEQ</ta>
            <ta e="T499" id="Seg_11329" s="T498">взять-PRS.[3SG]</ta>
            <ta e="T500" id="Seg_11330" s="T499">пустить-NEG-1SG</ta>
            <ta e="T501" id="Seg_11331" s="T500">какой</ta>
            <ta e="T502" id="Seg_11332" s="T501">в.сторону</ta>
            <ta e="T503" id="Seg_11333" s="T502">идти-PRS-2SG</ta>
            <ta e="T504" id="Seg_11334" s="T503">жена.[NOM]</ta>
            <ta e="T505" id="Seg_11335" s="T504">мчаться-CVB.SEQ</ta>
            <ta e="T506" id="Seg_11336" s="T505">оставаться-PRS.[3SG]</ta>
            <ta e="T507" id="Seg_11337" s="T506">Кунгаджай.[NOM]</ta>
            <ta e="T508" id="Seg_11338" s="T507">догонять-CVB.SIM</ta>
            <ta e="T509" id="Seg_11339" s="T508">следовать-ITER-CVB.SEQ</ta>
            <ta e="T510" id="Seg_11340" s="T509">караван-3SG-ACC</ta>
            <ta e="T511" id="Seg_11341" s="T510">носить-CVB.SIM</ta>
            <ta e="T512" id="Seg_11342" s="T511">оставаться-PRS.[3SG]</ta>
            <ta e="T513" id="Seg_11343" s="T512">Кунгаджай.[NOM]</ta>
            <ta e="T514" id="Seg_11344" s="T513">восемь</ta>
            <ta e="T515" id="Seg_11345" s="T514">черный.олень.[NOM]</ta>
            <ta e="T516" id="Seg_11346" s="T515">господин-3SG-DAT/LOC</ta>
            <ta e="T517" id="Seg_11347" s="T516">входить-PRS.[3SG]</ta>
            <ta e="T518" id="Seg_11348" s="T517">вот</ta>
            <ta e="T519" id="Seg_11349" s="T518">восемь</ta>
            <ta e="T520" id="Seg_11350" s="T519">черный.олень.[NOM]</ta>
            <ta e="T521" id="Seg_11351" s="T520">господин-3SG.[NOM]</ta>
            <ta e="T522" id="Seg_11352" s="T521">мир-PROPR.[NOM]</ta>
            <ta e="T523" id="Seg_11353" s="T522">быть-PTCP.COND-DAT/LOC</ta>
            <ta e="T524" id="Seg_11354" s="T523">младший.брат-PL-1SG-ACC</ta>
            <ta e="T525" id="Seg_11355" s="T524">заботиться-CVB.SEQ</ta>
            <ta e="T526" id="Seg_11356" s="T525">сидеть.[IMP.2SG]</ta>
            <ta e="T527" id="Seg_11357" s="T526">плохой-ADVZ</ta>
            <ta e="T528" id="Seg_11358" s="T527">попробовать-FUT-2SG</ta>
            <ta e="T529" id="Seg_11359" s="T528">один</ta>
            <ta e="T530" id="Seg_11360" s="T529">день-EP-INSTR</ta>
            <ta e="T531" id="Seg_11361" s="T530">NEG.EX</ta>
            <ta e="T532" id="Seg_11362" s="T531">делать-CAUS-FUT-1SG</ta>
            <ta e="T533" id="Seg_11363" s="T532">невестка-1SG-ACC</ta>
            <ta e="T534" id="Seg_11364" s="T533">следовать-CVB.SIM</ta>
            <ta e="T535" id="Seg_11365" s="T534">идти-PST1-1SG</ta>
            <ta e="T536" id="Seg_11366" s="T535">невестка-3SG-ACC</ta>
            <ta e="T537" id="Seg_11367" s="T536">следовать-CVB.SEQ</ta>
            <ta e="T538" id="Seg_11368" s="T537">потерять-CVB.SEQ</ta>
            <ta e="T539" id="Seg_11369" s="T538">бросать-PRS.[3SG]</ta>
            <ta e="T540" id="Seg_11370" s="T539">какой</ta>
            <ta e="T541" id="Seg_11371" s="T540">страна.[NOM]</ta>
            <ta e="T542" id="Seg_11372" s="T541">конец-3SG-DAT/LOC</ta>
            <ta e="T543" id="Seg_11373" s="T542">видеть-PRS.[3SG]</ta>
            <ta e="T544" id="Seg_11374" s="T543">достигать-PTCP.FUT</ta>
            <ta e="T545" id="Seg_11375" s="T544">догонять-NEG.[3SG]</ta>
            <ta e="T546" id="Seg_11376" s="T545">вертеться-CAUS-CVB.SEQ</ta>
            <ta e="T547" id="Seg_11377" s="T546">приносить-CVB.SEQ</ta>
            <ta e="T548" id="Seg_11378" s="T547">дом-3SG-ACC</ta>
            <ta e="T549" id="Seg_11379" s="T548">к</ta>
            <ta e="T550" id="Seg_11380" s="T549">заграждать-PRS.[3SG]</ta>
            <ta e="T551" id="Seg_11381" s="T550">хватать-PTCP.FUT.[NOM]</ta>
            <ta e="T552" id="Seg_11382" s="T551">да</ta>
            <ta e="T553" id="Seg_11383" s="T552">вопреки</ta>
            <ta e="T554" id="Seg_11384" s="T553">хватать-NEG.CVB.SIM</ta>
            <ta e="T555" id="Seg_11385" s="T554">невестка-3SG-GEN</ta>
            <ta e="T556" id="Seg_11386" s="T555">задняя.часть-3SG-ABL</ta>
            <ta e="T557" id="Seg_11387" s="T556">входить-PRS.[3SG]</ta>
            <ta e="T558" id="Seg_11388" s="T557">невестка-3SG.[NOM]</ta>
            <ta e="T559" id="Seg_11389" s="T558">ребенок-3SG-ACC</ta>
            <ta e="T560" id="Seg_11390" s="T559">женская.грудь-VBZ-CVB.SIM</ta>
            <ta e="T561" id="Seg_11391" s="T560">сидеть-PRS.[3SG]</ta>
            <ta e="T562" id="Seg_11392" s="T561">младший.брат-3PL.[NOM]</ta>
            <ta e="T563" id="Seg_11393" s="T562">есть-CVB.SIM</ta>
            <ta e="T564" id="Seg_11394" s="T563">сидеть-PRS.[3SG]</ta>
            <ta e="T565" id="Seg_11395" s="T564">вот</ta>
            <ta e="T566" id="Seg_11396" s="T565">невестка.[NOM]</ta>
            <ta e="T567" id="Seg_11397" s="T566">вина-2SG-ACC</ta>
            <ta e="T568" id="Seg_11398" s="T567">проехать-EP-CAUS-PST1-2SG</ta>
            <ta e="T569" id="Seg_11399" s="T568">ребенок-2SG-ACC</ta>
            <ta e="T570" id="Seg_11400" s="T569">взять-NEG.PTCP.PST-EP-2SG.[NOM]</ta>
            <ta e="T571" id="Seg_11401" s="T570">быть-COND.[3SG]</ta>
            <ta e="T572" id="Seg_11402" s="T571">убить-PTCP.FUT</ta>
            <ta e="T573" id="Seg_11403" s="T572">быть-PST1-1SG</ta>
            <ta e="T574" id="Seg_11404" s="T573">восемь</ta>
            <ta e="T575" id="Seg_11405" s="T574">черный.олень.[NOM]</ta>
            <ta e="T576" id="Seg_11406" s="T575">господин-3SG.[NOM]</ta>
            <ta e="T577" id="Seg_11407" s="T576">бояться-CVB.PURP</ta>
            <ta e="T578" id="Seg_11408" s="T577">бояться-PRS.[3SG]</ta>
            <ta e="T579" id="Seg_11409" s="T578">жить-PRS-3PL</ta>
            <ta e="T580" id="Seg_11410" s="T579">когда</ta>
            <ta e="T581" id="Seg_11411" s="T580">INDEF</ta>
            <ta e="T582" id="Seg_11412" s="T581">три-ORD</ta>
            <ta e="T583" id="Seg_11413" s="T582">день-3SG-DAT/LOC</ta>
            <ta e="T584" id="Seg_11414" s="T583">вот</ta>
            <ta e="T585" id="Seg_11415" s="T584">восемь</ta>
            <ta e="T586" id="Seg_11416" s="T585">черный.олень.[NOM]</ta>
            <ta e="T587" id="Seg_11417" s="T586">господин-3SG.[NOM]</ta>
            <ta e="T588" id="Seg_11418" s="T587">народ-2SG-ACC</ta>
            <ta e="T589" id="Seg_11419" s="T588">собирать.[IMP.2SG]</ta>
            <ta e="T590" id="Seg_11420" s="T589">сход-VBZ-FUT-1PL</ta>
            <ta e="T591" id="Seg_11421" s="T590">говорить-PRS.[3SG]</ta>
            <ta e="T592" id="Seg_11422" s="T591">Кунгаджай.[NOM]</ta>
            <ta e="T593" id="Seg_11423" s="T592">вот</ta>
            <ta e="T594" id="Seg_11424" s="T593">собирать-PRS-3PL</ta>
            <ta e="T595" id="Seg_11425" s="T594">семь</ta>
            <ta e="T596" id="Seg_11426" s="T595">десять</ta>
            <ta e="T597" id="Seg_11427" s="T596">чум.[NOM]</ta>
            <ta e="T598" id="Seg_11428" s="T597">человек-3SG-ACC</ta>
            <ta e="T599" id="Seg_11429" s="T598">Кунгаджай.[NOM]</ta>
            <ta e="T600" id="Seg_11430" s="T599">говорить-PRS.[3SG]</ta>
            <ta e="T601" id="Seg_11431" s="T600">2PL.[NOM]</ta>
            <ta e="T602" id="Seg_11432" s="T601">два</ta>
            <ta e="T603" id="Seg_11433" s="T602">хозяин.[NOM]</ta>
            <ta e="T604" id="Seg_11434" s="T603">есть-2PL</ta>
            <ta e="T605" id="Seg_11435" s="T604">восемь</ta>
            <ta e="T606" id="Seg_11436" s="T605">черный.олень.[NOM]</ta>
            <ta e="T607" id="Seg_11437" s="T606">хозяин-3SG.[NOM]</ta>
            <ta e="T608" id="Seg_11438" s="T607">и</ta>
            <ta e="T609" id="Seg_11439" s="T608">семь</ta>
            <ta e="T610" id="Seg_11440" s="T609">пуговица.[NOM]</ta>
            <ta e="T611" id="Seg_11441" s="T610">господин-3SG.[NOM]</ta>
            <ta e="T612" id="Seg_11442" s="T611">теперь</ta>
            <ta e="T613" id="Seg_11443" s="T612">хозяин.[NOM]</ta>
            <ta e="T614" id="Seg_11444" s="T613">быть-CVB.SEQ</ta>
            <ta e="T615" id="Seg_11445" s="T614">кончать-PST1-2PL</ta>
            <ta e="T616" id="Seg_11446" s="T615">хозяин-2PL.[NOM]</ta>
            <ta e="T617" id="Seg_11447" s="T616">тот.[NOM]</ta>
            <ta e="T618" id="Seg_11448" s="T617">мальчик.[NOM]</ta>
            <ta e="T619" id="Seg_11449" s="T618">ребенок.[NOM]</ta>
            <ta e="T620" id="Seg_11450" s="T619">старший.брат-EP-1SG.[NOM]</ta>
            <ta e="T621" id="Seg_11451" s="T620">ребенок-3SG.[NOM]</ta>
            <ta e="T622" id="Seg_11452" s="T621">собирать-REFL-EP-IMP.2PL</ta>
            <ta e="T623" id="Seg_11453" s="T622">каждый-2PL.[NOM]</ta>
            <ta e="T624" id="Seg_11454" s="T623">два</ta>
            <ta e="T625" id="Seg_11455" s="T624">день-DAT/LOC</ta>
            <ta e="T626" id="Seg_11456" s="T625">три-ORD</ta>
            <ta e="T627" id="Seg_11457" s="T626">день-2PL-DAT/LOC</ta>
            <ta e="T628" id="Seg_11458" s="T627">уезжать-FUT-2PL</ta>
            <ta e="T629" id="Seg_11459" s="T628">передовой-2PL.[NOM]</ta>
            <ta e="T630" id="Seg_11460" s="T629">3SG.[NOM]</ta>
            <ta e="T631" id="Seg_11461" s="T630">быть-FUT.[3SG]</ta>
            <ta e="T632" id="Seg_11462" s="T631">чужой</ta>
            <ta e="T633" id="Seg_11463" s="T632">олень-ACC</ta>
            <ta e="T634" id="Seg_11464" s="T633">запрячь-EP-NEG.[IMP.2SG]</ta>
            <ta e="T635" id="Seg_11465" s="T634">отец-2SG.[NOM]</ta>
            <ta e="T636" id="Seg_11466" s="T635">олень-3SG-ACC</ta>
            <ta e="T637" id="Seg_11467" s="T636">завет-3SG-ACC</ta>
            <ta e="T638" id="Seg_11468" s="T637">запрячь-FUT.[IMP.2SG]</ta>
            <ta e="T639" id="Seg_11469" s="T638">говорить-PRS.[3SG]</ta>
            <ta e="T640" id="Seg_11470" s="T639">мальчик-DAT/LOC</ta>
            <ta e="T641" id="Seg_11471" s="T640">мальчик.[NOM]</ta>
            <ta e="T642" id="Seg_11472" s="T641">расти-PST2.[3SG]</ta>
            <ta e="T643" id="Seg_11473" s="T642">руководить-EP-CAUS-CVB.SEQ-2SG</ta>
            <ta e="T644" id="Seg_11474" s="T643">уезжать-FUT-2PL</ta>
            <ta e="T645" id="Seg_11475" s="T644">лес.[NOM]</ta>
            <ta e="T646" id="Seg_11476" s="T645">ростом.с-3SG-INSTR</ta>
            <ta e="T647" id="Seg_11477" s="T646">хребет.[NOM]</ta>
            <ta e="T648" id="Seg_11478" s="T647">есть</ta>
            <ta e="T649" id="Seg_11479" s="T648">быть-FUT.[3SG]</ta>
            <ta e="T650" id="Seg_11480" s="T649">этот</ta>
            <ta e="T651" id="Seg_11481" s="T650">хребет.[NOM]</ta>
            <ta e="T652" id="Seg_11482" s="T651">тундра.[NOM]</ta>
            <ta e="T653" id="Seg_11483" s="T652">в.сторону</ta>
            <ta e="T654" id="Seg_11484" s="T653">сторона-3SG-INSTR</ta>
            <ta e="T655" id="Seg_11485" s="T654">кочевать-FUT-2SG</ta>
            <ta e="T656" id="Seg_11486" s="T655">два</ta>
            <ta e="T657" id="Seg_11487" s="T656">месяц-ACC</ta>
            <ta e="T658" id="Seg_11488" s="T657">в.течении</ta>
            <ta e="T659" id="Seg_11489" s="T658">последний-3SG.[NOM]</ta>
            <ta e="T660" id="Seg_11490" s="T659">однако</ta>
            <ta e="T661" id="Seg_11491" s="T660">хребет-3SG.[NOM]</ta>
            <ta e="T662" id="Seg_11492" s="T661">кончать-TEMP-3SG</ta>
            <ta e="T663" id="Seg_11493" s="T662">тундра.[NOM]</ta>
            <ta e="T664" id="Seg_11494" s="T663">быть-FUT.[3SG]</ta>
            <ta e="T665" id="Seg_11495" s="T664">снег-3SG-GEN</ta>
            <ta e="T666" id="Seg_11496" s="T665">нижняя.часть-3SG-ABL</ta>
            <ta e="T667" id="Seg_11497" s="T666">дым-PROPR.[NOM]</ta>
            <ta e="T668" id="Seg_11498" s="T667">быть-FUT.[3SG]</ta>
            <ta e="T669" id="Seg_11499" s="T668">тот.[NOM]</ta>
            <ta e="T670" id="Seg_11500" s="T669">1PL.[NOM]</ta>
            <ta e="T671" id="Seg_11501" s="T670">предок.[NOM]</ta>
            <ta e="T672" id="Seg_11502" s="T671">земля-1PL.[NOM]</ta>
            <ta e="T673" id="Seg_11503" s="T672">большой</ta>
            <ta e="T674" id="Seg_11504" s="T673">дорога-DAT/LOC</ta>
            <ta e="T675" id="Seg_11505" s="T674">входить-FUT-2SG</ta>
            <ta e="T676" id="Seg_11506" s="T675">родственник-PL-2SG-DAT/LOC</ta>
            <ta e="T677" id="Seg_11507" s="T676">доезжать-FUT-2SG</ta>
            <ta e="T678" id="Seg_11508" s="T677">люди-PL.[NOM]</ta>
            <ta e="T679" id="Seg_11509" s="T678">олень-2PL-ACC</ta>
            <ta e="T680" id="Seg_11510" s="T679">ночь-ACC-день-ACC</ta>
            <ta e="T681" id="Seg_11511" s="T680">в.течении</ta>
            <ta e="T682" id="Seg_11512" s="T681">видеть-FUT-2PL</ta>
            <ta e="T683" id="Seg_11513" s="T682">восемь</ta>
            <ta e="T684" id="Seg_11514" s="T683">черный.олень.[NOM]</ta>
            <ta e="T685" id="Seg_11515" s="T684">хозяин-3SG.[NOM]</ta>
            <ta e="T686" id="Seg_11516" s="T685">семь</ta>
            <ta e="T687" id="Seg_11517" s="T686">пуговица.[NOM]</ta>
            <ta e="T688" id="Seg_11518" s="T687">господин-3SG.[NOM]</ta>
            <ta e="T689" id="Seg_11519" s="T688">опять</ta>
            <ta e="T690" id="Seg_11520" s="T689">встречать-FUT-2PL</ta>
            <ta e="T691" id="Seg_11521" s="T690">1SG.[NOM]</ta>
            <ta e="T692" id="Seg_11522" s="T691">однако</ta>
            <ta e="T693" id="Seg_11523" s="T692">этот</ta>
            <ta e="T694" id="Seg_11524" s="T693">хороший-ADVZ</ta>
            <ta e="T695" id="Seg_11525" s="T694">путешествовать-PTCP.PRS-2PL.[NOM]</ta>
            <ta e="T696" id="Seg_11526" s="T695">из.за</ta>
            <ta e="T697" id="Seg_11527" s="T696">плохой</ta>
            <ta e="T698" id="Seg_11528" s="T697">люди-ACC</ta>
            <ta e="T699" id="Seg_11529" s="T698">Чангит-PL-ACC</ta>
            <ta e="T700" id="Seg_11530" s="T699">зверь-PL-ACC</ta>
            <ta e="T701" id="Seg_11531" s="T700">NEG.EX</ta>
            <ta e="T702" id="Seg_11532" s="T701">делать-CVB.SIM</ta>
            <ta e="T703" id="Seg_11533" s="T702">идти-PST1-1SG</ta>
            <ta e="T704" id="Seg_11534" s="T703">тот</ta>
            <ta e="T705" id="Seg_11535" s="T704">место-DAT/LOC</ta>
            <ta e="T706" id="Seg_11536" s="T705">доезжать-PTCP.PRS</ta>
            <ta e="T707" id="Seg_11537" s="T706">день-2SG-DAT/LOC</ta>
            <ta e="T708" id="Seg_11538" s="T707">доезжать-FUT-1SG</ta>
            <ta e="T709" id="Seg_11539" s="T708">большой-ADVZ</ta>
            <ta e="T710" id="Seg_11540" s="T709">задержиться-TEMP-1SG</ta>
            <ta e="T711" id="Seg_11541" s="T710">три-ORD</ta>
            <ta e="T712" id="Seg_11542" s="T711">день-3SG-DAT/LOC</ta>
            <ta e="T713" id="Seg_11543" s="T712">доезжать-FUT-1SG</ta>
            <ta e="T714" id="Seg_11544" s="T713">говорить-PRS.[3SG]</ta>
            <ta e="T715" id="Seg_11545" s="T714">три-ORD</ta>
            <ta e="T716" id="Seg_11546" s="T715">день-3SG-DAT/LOC</ta>
            <ta e="T717" id="Seg_11547" s="T716">уезжать-PRS-3PL</ta>
            <ta e="T718" id="Seg_11548" s="T717">Кунгаджай.[NOM]</ta>
            <ta e="T719" id="Seg_11549" s="T718">уйти-CVB.SEQ</ta>
            <ta e="T720" id="Seg_11550" s="T719">идти-PRS.[3SG]</ta>
            <ta e="T721" id="Seg_11551" s="T720">вовсе</ta>
            <ta e="T722" id="Seg_11552" s="T721">пешком</ta>
            <ta e="T723" id="Seg_11553" s="T722">этот</ta>
            <ta e="T724" id="Seg_11554" s="T723">ребенок.[NOM]</ta>
            <ta e="T725" id="Seg_11555" s="T724">передовой.[NOM]</ta>
            <ta e="T726" id="Seg_11556" s="T725">быть-CVB.SEQ</ta>
            <ta e="T727" id="Seg_11557" s="T726">кочевать-CVB.SEQ</ta>
            <ta e="T728" id="Seg_11558" s="T727">ехать-PST1-3PL</ta>
            <ta e="T729" id="Seg_11559" s="T728">охранять-EP-MED-CVB.SEQ</ta>
            <ta e="T730" id="Seg_11560" s="T729">путешествовать-PRS-3PL</ta>
            <ta e="T731" id="Seg_11561" s="T730">господин-ACC-госпожа-ACC</ta>
            <ta e="T732" id="Seg_11562" s="T731">целый-3SG-ACC</ta>
            <ta e="T733" id="Seg_11563" s="T732">охранять-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T734" id="Seg_11564" s="T733">этот</ta>
            <ta e="T735" id="Seg_11565" s="T734">мальчик.[NOM]</ta>
            <ta e="T736" id="Seg_11566" s="T735">месяц-3SG-GEN</ta>
            <ta e="T737" id="Seg_11567" s="T736">середина-3SG.[NOM]</ta>
            <ta e="T738" id="Seg_11568" s="T737">становиться-PST2.[3SG]</ta>
            <ta e="T739" id="Seg_11569" s="T738">хребет-3SG-ACC</ta>
            <ta e="T740" id="Seg_11570" s="T739">следовать-CVB.SEQ</ta>
            <ta e="T741" id="Seg_11571" s="T740">идти-INFER-3SG</ta>
            <ta e="T742" id="Seg_11572" s="T741">вдруг</ta>
            <ta e="T743" id="Seg_11573" s="T742">тундра.[NOM]</ta>
            <ta e="T744" id="Seg_11574" s="T743">в.сторону</ta>
            <ta e="T745" id="Seg_11575" s="T744">хребет.[NOM]</ta>
            <ta e="T746" id="Seg_11576" s="T745">уйти-PRS.[3SG]</ta>
            <ta e="T747" id="Seg_11577" s="T746">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T748" id="Seg_11578" s="T747">человек.[NOM]</ta>
            <ta e="T749" id="Seg_11579" s="T748">идти-PRS.[3SG]</ta>
            <ta e="T750" id="Seg_11580" s="T749">какой</ta>
            <ta e="T751" id="Seg_11581" s="T750">в.сторону</ta>
            <ta e="T752" id="Seg_11582" s="T751">человек-2SG=Q</ta>
            <ta e="T753" id="Seg_11583" s="T752">что</ta>
            <ta e="T754" id="Seg_11584" s="T753">приходить-FUT.[3SG]=Q</ta>
            <ta e="T755" id="Seg_11585" s="T754">чум-ACC-огонь-ACC</ta>
            <ta e="T756" id="Seg_11586" s="T755">знать-NEG-1SG</ta>
            <ta e="T757" id="Seg_11587" s="T756">родиться-PTCP.PST</ta>
            <ta e="T758" id="Seg_11588" s="T757">старший.брат-1SG-ACC</ta>
            <ta e="T759" id="Seg_11589" s="T758">потерять-CVB.SIM</ta>
            <ta e="T760" id="Seg_11590" s="T759">идти-PRS-1SG</ta>
            <ta e="T761" id="Seg_11591" s="T760">какой-3PL.[NOM]=Q</ta>
            <ta e="T762" id="Seg_11592" s="T761">тот.[NOM]</ta>
            <ta e="T763" id="Seg_11593" s="T762">восемь</ta>
            <ta e="T764" id="Seg_11594" s="T763">черный.олень.[NOM]</ta>
            <ta e="T765" id="Seg_11595" s="T764">хозяин-3SG.[NOM]</ta>
            <ta e="T766" id="Seg_11596" s="T765">да.ну</ta>
            <ta e="T767" id="Seg_11597" s="T766">тот.[NOM]</ta>
            <ta e="T768" id="Seg_11598" s="T767">1SG.[NOM]</ta>
            <ta e="T769" id="Seg_11599" s="T768">человек-1SG.[NOM]</ta>
            <ta e="T770" id="Seg_11600" s="T769">тот.[NOM]</ta>
            <ta e="T771" id="Seg_11601" s="T770">свободно.бегающий.домашний.олень.[NOM]</ta>
            <ta e="T772" id="Seg_11602" s="T771">гнать-CVB.SEQ</ta>
            <ta e="T773" id="Seg_11603" s="T772">идти-PRS.[3SG]</ta>
            <ta e="T774" id="Seg_11604" s="T773">вот</ta>
            <ta e="T775" id="Seg_11605" s="T774">тогда</ta>
            <ta e="T776" id="Seg_11606" s="T775">1SG-DAT/LOC</ta>
            <ta e="T777" id="Seg_11607" s="T776">человек.[NOM]</ta>
            <ta e="T778" id="Seg_11608" s="T777">быть.[IMP.2SG]</ta>
            <ta e="T779" id="Seg_11609" s="T778">говорить-CVB.SIM</ta>
            <ta e="T780" id="Seg_11610" s="T779">привести-NEG.[3SG]</ta>
            <ta e="T781" id="Seg_11611" s="T780">если</ta>
            <ta e="T782" id="Seg_11612" s="T781">человек.[NOM]</ta>
            <ta e="T783" id="Seg_11613" s="T782">быть-FUT-2SG</ta>
            <ta e="T784" id="Seg_11614" s="T783">NEG-3SG</ta>
            <ta e="T785" id="Seg_11615" s="T784">этот</ta>
            <ta e="T786" id="Seg_11616" s="T785">три</ta>
            <ta e="T787" id="Seg_11617" s="T786">ветвь-PROPR</ta>
            <ta e="T788" id="Seg_11618" s="T787">хорей-EP-1SG.[NOM]</ta>
            <ta e="T789" id="Seg_11619" s="T788">другой.из.двух</ta>
            <ta e="T790" id="Seg_11620" s="T789">ветвь-3SG.[NOM]</ta>
            <ta e="T791" id="Seg_11621" s="T790">побрякушки-3SG.[NOM]</ta>
            <ta e="T792" id="Seg_11622" s="T791">быть-FUT-2SG</ta>
            <ta e="T793" id="Seg_11623" s="T792">два-MLTP</ta>
            <ta e="T794" id="Seg_11624" s="T793">богатырь.[NOM]</ta>
            <ta e="T795" id="Seg_11625" s="T794">свод.черепа-3SG.[NOM]</ta>
            <ta e="T796" id="Seg_11626" s="T795">побрякушки-PROPR.[NOM]</ta>
            <ta e="T797" id="Seg_11627" s="T796">человек.[NOM]</ta>
            <ta e="T798" id="Seg_11628" s="T797">согласить-EP-MED-PRS.[3SG]</ta>
            <ta e="T799" id="Seg_11629" s="T798">сопровождать-PRS.[3SG]</ta>
            <ta e="T800" id="Seg_11630" s="T799">кочевать-CVB.SEQ</ta>
            <ta e="T801" id="Seg_11631" s="T800">ехать-PST1-3PL</ta>
            <ta e="T802" id="Seg_11632" s="T801">месяц-3PL.[NOM]</ta>
            <ta e="T803" id="Seg_11633" s="T802">кончаться-PST1-3SG</ta>
            <ta e="T804" id="Seg_11634" s="T803">следующий</ta>
            <ta e="T805" id="Seg_11635" s="T804">месяц.[NOM]</ta>
            <ta e="T806" id="Seg_11636" s="T805">середина-3SG.[NOM]</ta>
            <ta e="T807" id="Seg_11637" s="T806">становиться-PST1-3SG</ta>
            <ta e="T808" id="Seg_11638" s="T807">опять</ta>
            <ta e="T809" id="Seg_11639" s="T808">человек-ACC</ta>
            <ta e="T810" id="Seg_11640" s="T809">видеть-PST1-3PL</ta>
            <ta e="T811" id="Seg_11641" s="T810">один</ta>
            <ta e="T812" id="Seg_11642" s="T811">хребет-DIM-DAT/LOC</ta>
            <ta e="T813" id="Seg_11643" s="T812">тоже</ta>
            <ta e="T814" id="Seg_11644" s="T813">здравствуй-VBZ-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T815" id="Seg_11645" s="T814">какой</ta>
            <ta e="T816" id="Seg_11646" s="T815">в.сторону</ta>
            <ta e="T817" id="Seg_11647" s="T816">чум-PROPR-огонь-PROPR</ta>
            <ta e="T818" id="Seg_11648" s="T817">человек-2SG=Q</ta>
            <ta e="T819" id="Seg_11649" s="T818">чум.[NOM]-огонь.[NOM]</ta>
            <ta e="T820" id="Seg_11650" s="T819">NEG.EX</ta>
            <ta e="T821" id="Seg_11651" s="T820">идти-PRS-1SG</ta>
            <ta e="T822" id="Seg_11652" s="T821">только</ta>
            <ta e="T823" id="Seg_11653" s="T822">старший.брат-1SG-ACC</ta>
            <ta e="T824" id="Seg_11654" s="T823">искать-PRS-1SG</ta>
            <ta e="T825" id="Seg_11655" s="T824">что.[NOM]</ta>
            <ta e="T826" id="Seg_11656" s="T825">быть-PST1-3SG=Q</ta>
            <ta e="T827" id="Seg_11657" s="T826">старший.брат-EP-2SG.[NOM]</ta>
            <ta e="T828" id="Seg_11658" s="T827">ээ</ta>
            <ta e="T829" id="Seg_11659" s="T828">семь</ta>
            <ta e="T830" id="Seg_11660" s="T829">пуговица.[NOM]</ta>
            <ta e="T831" id="Seg_11661" s="T830">господин-3SG.[NOM]</ta>
            <ta e="T832" id="Seg_11662" s="T831">ээ</ta>
            <ta e="T833" id="Seg_11663" s="T832">тот</ta>
            <ta e="T834" id="Seg_11664" s="T833">свободно.бегающий.домашний.олень.[NOM]</ta>
            <ta e="T835" id="Seg_11665" s="T834">гнать-CVB.SEQ</ta>
            <ta e="T836" id="Seg_11666" s="T835">идти-PRS.[3SG]</ta>
            <ta e="T837" id="Seg_11667" s="T836">старший.брат-EP-2SG.[NOM]</ta>
            <ta e="T838" id="Seg_11668" s="T837">эй</ta>
            <ta e="T839" id="Seg_11669" s="T838">1SG-DAT/LOC</ta>
            <ta e="T840" id="Seg_11670" s="T839">человек.[NOM]</ta>
            <ta e="T841" id="Seg_11671" s="T840">быть-FUT-2SG</ta>
            <ta e="T842" id="Seg_11672" s="T841">говорить-NEG.[3SG]</ta>
            <ta e="T843" id="Seg_11673" s="T842">быть-FUT-2SG</ta>
            <ta e="T844" id="Seg_11674" s="T843">NEG-3SG</ta>
            <ta e="T845" id="Seg_11675" s="T844">этот</ta>
            <ta e="T846" id="Seg_11676" s="T845">хорей-EP-1SG.[NOM]</ta>
            <ta e="T847" id="Seg_11677" s="T846">ветвь-3SG-DAT/LOC</ta>
            <ta e="T848" id="Seg_11678" s="T847">побрякушки.[NOM]</ta>
            <ta e="T849" id="Seg_11679" s="T848">делать-FUT-1SG</ta>
            <ta e="T850" id="Seg_11680" s="T849">согласить-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T851" id="Seg_11681" s="T850">тот.самый</ta>
            <ta e="T852" id="Seg_11682" s="T851">человек.[NOM]</ta>
            <ta e="T853" id="Seg_11683" s="T852">старший.брат-3SG-DAT/LOC</ta>
            <ta e="T854" id="Seg_11684" s="T853">доезжать-PRS.[3SG]</ta>
            <ta e="T855" id="Seg_11685" s="T854">почему</ta>
            <ta e="T856" id="Seg_11686" s="T855">убить-NEG-2PL</ta>
            <ta e="T857" id="Seg_11687" s="T856">ребенок-DAT/LOC</ta>
            <ta e="T858" id="Seg_11688" s="T857">голова.[NOM]</ta>
            <ta e="T859" id="Seg_11689" s="T858">слушаться-PRS-2PL</ta>
            <ta e="T860" id="Seg_11690" s="T859">Q</ta>
            <ta e="T861" id="Seg_11691" s="T860">нет</ta>
            <ta e="T862" id="Seg_11692" s="T861">оставлять.[IMP.2SG]</ta>
            <ta e="T863" id="Seg_11693" s="T862">охотить-PTCP.FUT-DAT/LOC</ta>
            <ta e="T864" id="Seg_11694" s="T863">надо</ta>
            <ta e="T865" id="Seg_11695" s="T864">смелый-3SG.[NOM]</ta>
            <ta e="T866" id="Seg_11696" s="T865">да</ta>
            <ta e="T867" id="Seg_11697" s="T866">очень</ta>
            <ta e="T868" id="Seg_11698" s="T867">вот</ta>
            <ta e="T869" id="Seg_11699" s="T868">попробовать-FUT-1PL</ta>
            <ta e="T870" id="Seg_11700" s="T869">вечером</ta>
            <ta e="T871" id="Seg_11701" s="T870">сходить-PRS-3PL</ta>
            <ta e="T872" id="Seg_11702" s="T871">один</ta>
            <ta e="T873" id="Seg_11703" s="T872">место-DAT/LOC</ta>
            <ta e="T874" id="Seg_11704" s="T873">есть-PRS-3PL</ta>
            <ta e="T875" id="Seg_11705" s="T874">мальчик.[NOM]</ta>
            <ta e="T876" id="Seg_11706" s="T875">народ-3SG-ACC</ta>
            <ta e="T877" id="Seg_11707" s="T876">следовать-CVB.SEQ</ta>
            <ta e="T878" id="Seg_11708" s="T877">вынимать-PRS.[3SG]</ta>
            <ta e="T879" id="Seg_11709" s="T878">стадо.[NOM]</ta>
            <ta e="T880" id="Seg_11710" s="T879">охранять-EP-CAUS-CVB.SIM</ta>
            <ta e="T881" id="Seg_11711" s="T880">ребенок.[NOM]</ta>
            <ta e="T882" id="Seg_11712" s="T881">спать-CVB.SEQ</ta>
            <ta e="T883" id="Seg_11713" s="T882">оставаться-PRS.[3SG]</ta>
            <ta e="T884" id="Seg_11714" s="T883">человек-PL.[NOM]</ta>
            <ta e="T885" id="Seg_11715" s="T884">подкрадываться-PTCP.PRS</ta>
            <ta e="T886" id="Seg_11716" s="T885">голос-3PL-ACC</ta>
            <ta e="T887" id="Seg_11717" s="T886">слышать-PRS.[3SG]</ta>
            <ta e="T888" id="Seg_11718" s="T887">человек.[NOM]</ta>
            <ta e="T889" id="Seg_11719" s="T888">пальма-INSTR</ta>
            <ta e="T890" id="Seg_11720" s="T889">дверь-ACC</ta>
            <ta e="T891" id="Seg_11721" s="T890">приоткрывать-PRS.[3SG]</ta>
            <ta e="T892" id="Seg_11722" s="T891">эй</ta>
            <ta e="T893" id="Seg_11723" s="T892">почему</ta>
            <ta e="T894" id="Seg_11724" s="T893">охранять-PRS-2PL</ta>
            <ta e="T895" id="Seg_11725" s="T894">1SG-ACC</ta>
            <ta e="T896" id="Seg_11726" s="T895">стадо-ACC</ta>
            <ta e="T897" id="Seg_11727" s="T896">охранять-IMP.2PL</ta>
            <ta e="T898" id="Seg_11728" s="T897">люди.[NOM]</ta>
            <ta e="T899" id="Seg_11729" s="T898">назад</ta>
            <ta e="T900" id="Seg_11730" s="T899">прятаться-PRS-3PL</ta>
            <ta e="T901" id="Seg_11731" s="T900">утренний</ta>
            <ta e="T902" id="Seg_11732" s="T901">сон-3SG-DAT/LOC</ta>
            <ta e="T903" id="Seg_11733" s="T902">попробовать-FUT-1PL</ta>
            <ta e="T904" id="Seg_11734" s="T903">говорить-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T905" id="Seg_11735" s="T904">опять</ta>
            <ta e="T906" id="Seg_11736" s="T905">попробовать-PRS-3PL</ta>
            <ta e="T907" id="Seg_11737" s="T906">опять</ta>
            <ta e="T908" id="Seg_11738" s="T907">что-ACC</ta>
            <ta e="T909" id="Seg_11739" s="T908">охранять-PRS-2PL</ta>
            <ta e="T910" id="Seg_11740" s="T909">идти-EP-IMP.2PL</ta>
            <ta e="T911" id="Seg_11741" s="T910">следовать-CVB.SEQ</ta>
            <ta e="T912" id="Seg_11742" s="T911">послать-PRS.[3SG]</ta>
            <ta e="T913" id="Seg_11743" s="T912">тот.EMPH-ABL</ta>
            <ta e="T914" id="Seg_11744" s="T913">трогать-PST2.NEG-3PL</ta>
            <ta e="T915" id="Seg_11745" s="T914">два</ta>
            <ta e="T916" id="Seg_11746" s="T915">месяц-3PL.[NOM]</ta>
            <ta e="T917" id="Seg_11747" s="T916">пережить-MED-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T918" id="Seg_11748" s="T917">хребет-3PL.[NOM]</ta>
            <ta e="T919" id="Seg_11749" s="T918">последний-3SG.[NOM]</ta>
            <ta e="T920" id="Seg_11750" s="T919">быть-PRS.[3SG]</ta>
            <ta e="T921" id="Seg_11751" s="T920">видеть-PST2-3SG</ta>
            <ta e="T922" id="Seg_11752" s="T921">тундра.[NOM]</ta>
            <ta e="T923" id="Seg_11753" s="T922">становиться-PST2.[3SG]</ta>
            <ta e="T924" id="Seg_11754" s="T923">снег.[NOM]</ta>
            <ta e="T925" id="Seg_11755" s="T924">нижняя.часть-3SG-ABL</ta>
            <ta e="T926" id="Seg_11756" s="T925">дым-DIM-INTNS-3SG.[NOM]</ta>
            <ta e="T927" id="Seg_11757" s="T926">тундра.[NOM]</ta>
            <ta e="T928" id="Seg_11758" s="T927">в.сторону-ABL</ta>
            <ta e="T929" id="Seg_11759" s="T928">огромный</ta>
            <ta e="T930" id="Seg_11760" s="T929">очень</ta>
            <ta e="T931" id="Seg_11761" s="T930">тропка.[NOM]</ta>
            <ta e="T932" id="Seg_11762" s="T931">приходить-PRS.[3SG]</ta>
            <ta e="T933" id="Seg_11763" s="T932">земля.[NOM]</ta>
            <ta e="T934" id="Seg_11764" s="T933">нутро-3SG-ACC</ta>
            <ta e="T935" id="Seg_11765" s="T934">в.сторону</ta>
            <ta e="T936" id="Seg_11766" s="T935">идти-PRS.[3SG]</ta>
            <ta e="T937" id="Seg_11767" s="T936">тропка.[NOM]</ta>
            <ta e="T938" id="Seg_11768" s="T937">идти-CVB.SEQ</ta>
            <ta e="T939" id="Seg_11769" s="T938">идти-PRS-3PL</ta>
            <ta e="T940" id="Seg_11770" s="T939">прямо</ta>
            <ta e="T941" id="Seg_11771" s="T940">народ-ABL</ta>
            <ta e="T942" id="Seg_11772" s="T941">народ.[NOM]</ta>
            <ta e="T943" id="Seg_11773" s="T942">чум-ABL</ta>
            <ta e="T944" id="Seg_11774" s="T943">чум.[NOM]</ta>
            <ta e="T945" id="Seg_11775" s="T944">земля-DAT/LOC</ta>
            <ta e="T946" id="Seg_11776" s="T945">приходить-PRS-3PL</ta>
            <ta e="T947" id="Seg_11777" s="T946">один</ta>
            <ta e="T948" id="Seg_11778" s="T947">чум.[NOM]</ta>
            <ta e="T949" id="Seg_11779" s="T948">край-3SG-DAT/LOC</ta>
            <ta e="T950" id="Seg_11780" s="T949">останавливаться-PRS.[3SG]</ta>
            <ta e="T951" id="Seg_11781" s="T950">один</ta>
            <ta e="T952" id="Seg_11782" s="T951">старый</ta>
            <ta e="T953" id="Seg_11783" s="T952">EMPH</ta>
            <ta e="T954" id="Seg_11784" s="T953">жена.[NOM]</ta>
            <ta e="T955" id="Seg_11785" s="T954">выйти-PRS.[3SG]</ta>
            <ta e="T956" id="Seg_11786" s="T955">ба</ta>
            <ta e="T957" id="Seg_11787" s="T956">олень-3SG.[NOM]</ta>
            <ta e="T958" id="Seg_11788" s="T957">чудо.[NOM]</ta>
            <ta e="T959" id="Seg_11789" s="T958">олень.[NOM]</ta>
            <ta e="T960" id="Seg_11790" s="T959">1PL.[NOM]</ta>
            <ta e="T961" id="Seg_11791" s="T960">предок.[NOM]</ta>
            <ta e="T962" id="Seg_11792" s="T961">олень-1PL.[NOM]</ta>
            <ta e="T963" id="Seg_11793" s="T962">как=Q</ta>
            <ta e="T964" id="Seg_11794" s="T963">давно</ta>
            <ta e="T965" id="Seg_11795" s="T964">два</ta>
            <ta e="T966" id="Seg_11796" s="T965">человек.[NOM]</ta>
            <ta e="T967" id="Seg_11797" s="T966">идти-PST2-3SG</ta>
            <ta e="T968" id="Seg_11798" s="T967">тот</ta>
            <ta e="T969" id="Seg_11799" s="T968">род-3PL.[NOM]</ta>
            <ta e="T970" id="Seg_11800" s="T969">наверное</ta>
            <ta e="T971" id="Seg_11801" s="T970">три</ta>
            <ta e="T972" id="Seg_11802" s="T971">чум-ABL</ta>
            <ta e="T973" id="Seg_11803" s="T972">три</ta>
            <ta e="T974" id="Seg_11804" s="T973">жена.[NOM]</ta>
            <ta e="T975" id="Seg_11805" s="T974">выйти-CVB.SEQ</ta>
            <ta e="T976" id="Seg_11806" s="T975">опять</ta>
            <ta e="T977" id="Seg_11807" s="T976">узнавать-PST2-3PL</ta>
            <ta e="T978" id="Seg_11808" s="T977">род-3PL-ACC</ta>
            <ta e="T979" id="Seg_11809" s="T978">разговаривать-CVB.SEQ</ta>
            <ta e="T980" id="Seg_11810" s="T979">вот</ta>
            <ta e="T981" id="Seg_11811" s="T980">узнавать-RECP/COLL-PRS-3PL</ta>
            <ta e="T982" id="Seg_11812" s="T981">родственник-3PL-ACC</ta>
            <ta e="T983" id="Seg_11813" s="T982">найти-RECP/COLL-PRS-3PL</ta>
            <ta e="T984" id="Seg_11814" s="T983">Кунгаджай-ACC</ta>
            <ta e="T985" id="Seg_11815" s="T984">ждать-PRS-3PL</ta>
            <ta e="T986" id="Seg_11816" s="T985">три-ORD</ta>
            <ta e="T987" id="Seg_11817" s="T986">день-3SG-GEN</ta>
            <ta e="T988" id="Seg_11818" s="T987">середина-3SG.[NOM]</ta>
            <ta e="T989" id="Seg_11819" s="T988">Кунгаджай.[NOM]</ta>
            <ta e="T990" id="Seg_11820" s="T989">приходить-PRS.[3SG]</ta>
            <ta e="T991" id="Seg_11821" s="T990">мясо.[NOM]</ta>
            <ta e="T992" id="Seg_11822" s="T991">имя-3SG.[NOM]</ta>
            <ta e="T993" id="Seg_11823" s="T992">целый-POSS</ta>
            <ta e="T994" id="Seg_11824" s="T993">NEG</ta>
            <ta e="T995" id="Seg_11825" s="T994">кожа-3SG-GEN</ta>
            <ta e="T996" id="Seg_11826" s="T995">половина-3SG.[NOM]</ta>
            <ta e="T997" id="Seg_11827" s="T996">оставаться-PST2.[3SG]</ta>
            <ta e="T998" id="Seg_11828" s="T997">ребенок-1SG.[NOM]</ta>
            <ta e="T999" id="Seg_11829" s="T998">приходить-PST2-2SG</ta>
            <ta e="T1000" id="Seg_11830" s="T999">вот</ta>
            <ta e="T1001" id="Seg_11831" s="T1000">хороший.[NOM]</ta>
            <ta e="T1002" id="Seg_11832" s="T1001">плохой</ta>
            <ta e="T1003" id="Seg_11833" s="T1002">люди-ACC</ta>
            <ta e="T1004" id="Seg_11834" s="T1003">истребить-CVB.SEQ</ta>
            <ta e="T1005" id="Seg_11835" s="T1004">место.[NOM]</ta>
            <ta e="T1006" id="Seg_11836" s="T1005">животное-3SG-ACC</ta>
            <ta e="T1007" id="Seg_11837" s="T1006">еле</ta>
            <ta e="T1008" id="Seg_11838" s="T1007">побеждать-PST1-1SG</ta>
            <ta e="T1009" id="Seg_11839" s="T1008">душа-EP-1SG.[NOM]</ta>
            <ta e="T1010" id="Seg_11840" s="T1009">только</ta>
            <ta e="T1011" id="Seg_11841" s="T1010">приходить-PST1-1SG</ta>
            <ta e="T1012" id="Seg_11842" s="T1011">рана.[NOM]</ta>
            <ta e="T1013" id="Seg_11843" s="T1012">делать-CVB.SEQ-3PL</ta>
            <ta e="T1014" id="Seg_11844" s="T1013">вот</ta>
            <ta e="T1015" id="Seg_11845" s="T1014">жить.[IMP.2SG]</ta>
            <ta e="T1016" id="Seg_11846" s="T1015">что-ABL</ta>
            <ta e="T1017" id="Seg_11847" s="T1016">NEG</ta>
            <ta e="T1018" id="Seg_11848" s="T1017">бояться-EP-NEG.[IMP.2SG]</ta>
            <ta e="T1019" id="Seg_11849" s="T1018">Кунгаджай.[NOM]</ta>
            <ta e="T1020" id="Seg_11850" s="T1019">вот</ta>
            <ta e="T1021" id="Seg_11851" s="T1020">позже</ta>
            <ta e="T1022" id="Seg_11852" s="T1021">оживать-PRS.[3SG]</ta>
            <ta e="T1023" id="Seg_11853" s="T1022">мальчик.[NOM]</ta>
            <ta e="T1024" id="Seg_11854" s="T1023">хозяин.[NOM]</ta>
            <ta e="T1025" id="Seg_11855" s="T1024">становиться-CVB.SEQ</ta>
            <ta e="T1026" id="Seg_11856" s="T1025">жить-PST2-3SG</ta>
            <ta e="T1027" id="Seg_11857" s="T1026">этот</ta>
            <ta e="T1028" id="Seg_11858" s="T1027">народ-DAT/LOC</ta>
            <ta e="T1029" id="Seg_11859" s="T1028">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_11860" s="T0">cardnum</ta>
            <ta e="T2" id="Seg_11861" s="T1">n-n:(num)-n:case</ta>
            <ta e="T3" id="Seg_11862" s="T2">v-v:tense-v:pred.pn</ta>
            <ta e="T4" id="Seg_11863" s="T3">cardnum-n:(poss)-n:case</ta>
            <ta e="T5" id="Seg_11864" s="T4">adj</ta>
            <ta e="T6" id="Seg_11865" s="T5">adj</ta>
            <ta e="T7" id="Seg_11866" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_11867" s="T7">adj-n:(poss)-n:case</ta>
            <ta e="T9" id="Seg_11868" s="T8">v-v:(neg)-v:pred.pn</ta>
            <ta e="T10" id="Seg_11869" s="T9">n-n:poss-n:case</ta>
            <ta e="T11" id="Seg_11870" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_11871" s="T11">v-v:tense-v:pred.pn</ta>
            <ta e="T13" id="Seg_11872" s="T12">propr-n:case</ta>
            <ta e="T14" id="Seg_11873" s="T13">v-v:cvb</ta>
            <ta e="T15" id="Seg_11874" s="T14">n-n:(poss)-n:case</ta>
            <ta e="T16" id="Seg_11875" s="T15">n-n:(poss)</ta>
            <ta e="T17" id="Seg_11876" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_11877" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_11878" s="T18">n-n&gt;adj-n:case</ta>
            <ta e="T20" id="Seg_11879" s="T19">cardnum</ta>
            <ta e="T21" id="Seg_11880" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_11881" s="T21">n-n&gt;adj-n:case</ta>
            <ta e="T23" id="Seg_11882" s="T22">n-n:(poss)-n:case</ta>
            <ta e="T24" id="Seg_11883" s="T23">adj&gt;adj-adj-n:case</ta>
            <ta e="T25" id="Seg_11884" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_11885" s="T25">post</ta>
            <ta e="T27" id="Seg_11886" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_11887" s="T27">v-v:(neg)-v:pred.pn</ta>
            <ta e="T29" id="Seg_11888" s="T28">v-v:mood-v:temp.pn</ta>
            <ta e="T30" id="Seg_11889" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_11890" s="T30">n-n:poss-n:case</ta>
            <ta e="T32" id="Seg_11891" s="T31">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T33" id="Seg_11892" s="T32">n-n:(poss)-n:case</ta>
            <ta e="T34" id="Seg_11893" s="T33">n-n&gt;adj</ta>
            <ta e="T35" id="Seg_11894" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_11895" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_11896" s="T36">v-v:tense-v:pred.pn</ta>
            <ta e="T38" id="Seg_11897" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_11898" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_11899" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_11900" s="T40">n-n&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T42" id="Seg_11901" s="T41">post</ta>
            <ta e="T43" id="Seg_11902" s="T42">n-n:(poss)-n:case</ta>
            <ta e="T44" id="Seg_11903" s="T43">v-v:tense-v:pred.pn</ta>
            <ta e="T45" id="Seg_11904" s="T44">v-v:tense-v:pred.pn</ta>
            <ta e="T46" id="Seg_11905" s="T45">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T47" id="Seg_11906" s="T46">n-n:poss-n:case</ta>
            <ta e="T48" id="Seg_11907" s="T47">adv</ta>
            <ta e="T49" id="Seg_11908" s="T48">v-v:tense-v:pred.pn</ta>
            <ta e="T50" id="Seg_11909" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_11910" s="T50">v-v:tense-v:pred.pn</ta>
            <ta e="T52" id="Seg_11911" s="T51">n-n:poss-n:case</ta>
            <ta e="T53" id="Seg_11912" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_11913" s="T53">pers-pro:case</ta>
            <ta e="T55" id="Seg_11914" s="T54">v-v:tense-v:poss.pn</ta>
            <ta e="T56" id="Seg_11915" s="T55">que-pro:(ins)-pro:(poss)-pro:case</ta>
            <ta e="T57" id="Seg_11916" s="T56">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T58" id="Seg_11917" s="T57">v-v:ptcp</ta>
            <ta e="T59" id="Seg_11918" s="T58">n-n:poss-n:case</ta>
            <ta e="T60" id="Seg_11919" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_11920" s="T60">post</ta>
            <ta e="T62" id="Seg_11921" s="T61">v-v:tense-v:poss.pn</ta>
            <ta e="T63" id="Seg_11922" s="T62">n-n:poss-n:case</ta>
            <ta e="T64" id="Seg_11923" s="T63">v-v:mood-v:temp.pn</ta>
            <ta e="T65" id="Seg_11924" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_11925" s="T65">n-n:poss-n:case</ta>
            <ta e="T67" id="Seg_11926" s="T66">n-n:poss-n:case</ta>
            <ta e="T68" id="Seg_11927" s="T67">n-n&gt;v-v:cvb</ta>
            <ta e="T69" id="Seg_11928" s="T68">n-n&gt;v-v:cvb</ta>
            <ta e="T70" id="Seg_11929" s="T69">v-v:(tense)-v:mood.pn</ta>
            <ta e="T71" id="Seg_11930" s="T70">dempro-pro:case</ta>
            <ta e="T72" id="Seg_11931" s="T71">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T73" id="Seg_11932" s="T72">n-n:(poss)</ta>
            <ta e="T74" id="Seg_11933" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_11934" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_11935" s="T75">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T77" id="Seg_11936" s="T76">v-v:tense-v:pred.pn</ta>
            <ta e="T78" id="Seg_11937" s="T77">n-n:(poss)-n:case</ta>
            <ta e="T79" id="Seg_11938" s="T78">adv</ta>
            <ta e="T80" id="Seg_11939" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_11940" s="T80">v-v:tense-v:poss.pn</ta>
            <ta e="T82" id="Seg_11941" s="T81">v-v:(neg)-v:mood-v:temp.pn</ta>
            <ta e="T83" id="Seg_11942" s="T82">pers-pro:case</ta>
            <ta e="T84" id="Seg_11943" s="T83">v-v:tense-v:poss.pn</ta>
            <ta e="T85" id="Seg_11944" s="T84">adj-n:(poss)-n:case</ta>
            <ta e="T86" id="Seg_11945" s="T85">v-v:cvb</ta>
            <ta e="T87" id="Seg_11946" s="T86">v-v:tense-v:pred.pn</ta>
            <ta e="T88" id="Seg_11947" s="T87">v-v:(ins)-v&gt;v-v:cvb-v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T89" id="Seg_11948" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_11949" s="T89">v-v:cvb</ta>
            <ta e="T91" id="Seg_11950" s="T90">v-v:tense-v:pred.pn</ta>
            <ta e="T92" id="Seg_11951" s="T91">dempro</ta>
            <ta e="T93" id="Seg_11952" s="T92">n-n:(poss)-n:case</ta>
            <ta e="T94" id="Seg_11953" s="T93">v-v:tense-v:pred.pn</ta>
            <ta e="T95" id="Seg_11954" s="T94">n-n:poss-n:case</ta>
            <ta e="T96" id="Seg_11955" s="T95">v-v:(ins)-v&gt;v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T97" id="Seg_11956" s="T96">n-n:poss-n:case</ta>
            <ta e="T98" id="Seg_11957" s="T97">v-v:tense-v:pred.pn</ta>
            <ta e="T99" id="Seg_11958" s="T98">n-n:poss-n:case</ta>
            <ta e="T100" id="Seg_11959" s="T99">n-n:poss-n:case</ta>
            <ta e="T101" id="Seg_11960" s="T100">n-n:poss-n:case</ta>
            <ta e="T102" id="Seg_11961" s="T101">v-v:tense-v:pred.pn</ta>
            <ta e="T103" id="Seg_11962" s="T102">adj-n:(poss)-n:case</ta>
            <ta e="T104" id="Seg_11963" s="T103">v-v:tense-v:pred.pn</ta>
            <ta e="T105" id="Seg_11964" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_11965" s="T105">v-v:tense-v:pred.pn</ta>
            <ta e="T107" id="Seg_11966" s="T106">v-v:ptcp</ta>
            <ta e="T108" id="Seg_11967" s="T107">n-n:(poss)-n:case</ta>
            <ta e="T109" id="Seg_11968" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_11969" s="T109">n-n:(poss)</ta>
            <ta e="T111" id="Seg_11970" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_11971" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_11972" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_11973" s="T113">n-n&gt;adj</ta>
            <ta e="T115" id="Seg_11974" s="T114">n-n:poss-n:case</ta>
            <ta e="T116" id="Seg_11975" s="T115">v-v:tense-v:pred.pn</ta>
            <ta e="T117" id="Seg_11976" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_11977" s="T117">v-v:cvb</ta>
            <ta e="T119" id="Seg_11978" s="T118">v-v:tense-v:pred.pn</ta>
            <ta e="T120" id="Seg_11979" s="T119">n-n:case</ta>
            <ta e="T121" id="Seg_11980" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_11981" s="T121">v-v:ptcp</ta>
            <ta e="T123" id="Seg_11982" s="T122">n-n:poss-n:case</ta>
            <ta e="T124" id="Seg_11983" s="T123">v-v:tense-v:pred.pn</ta>
            <ta e="T125" id="Seg_11984" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_11985" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_11986" s="T126">n-n:(poss)-n:case</ta>
            <ta e="T128" id="Seg_11987" s="T127">cardnum</ta>
            <ta e="T129" id="Seg_11988" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_11989" s="T129">n-n:(poss)-n:case</ta>
            <ta e="T131" id="Seg_11990" s="T130">n-n:(poss)-n:case</ta>
            <ta e="T132" id="Seg_11991" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_11992" s="T132">v-v:tense-v:pred.pn</ta>
            <ta e="T134" id="Seg_11993" s="T133">interj</ta>
            <ta e="T135" id="Seg_11994" s="T134">que</ta>
            <ta e="T136" id="Seg_11995" s="T135">v-v:tense-v:pred.pn</ta>
            <ta e="T137" id="Seg_11996" s="T136">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T138" id="Seg_11997" s="T137">v-v:tense-v:poss.pn</ta>
            <ta e="T139" id="Seg_11998" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_11999" s="T139">interj</ta>
            <ta e="T141" id="Seg_12000" s="T140">interj</ta>
            <ta e="T142" id="Seg_12001" s="T141">adv</ta>
            <ta e="T143" id="Seg_12002" s="T142">v-v:tense-v:poss.pn</ta>
            <ta e="T144" id="Seg_12003" s="T143">v-v:mood.pn</ta>
            <ta e="T145" id="Seg_12004" s="T144">v-v:mood.pn</ta>
            <ta e="T146" id="Seg_12005" s="T145">pers-pro:case</ta>
            <ta e="T147" id="Seg_12006" s="T146">n-n:poss-n:case</ta>
            <ta e="T148" id="Seg_12007" s="T147">interj</ta>
            <ta e="T149" id="Seg_12008" s="T148">n-n:poss-n:case</ta>
            <ta e="T150" id="Seg_12009" s="T149">n-n:poss-n:case</ta>
            <ta e="T151" id="Seg_12010" s="T150">v-v:cvb</ta>
            <ta e="T152" id="Seg_12011" s="T151">v-v:mood.pn</ta>
            <ta e="T153" id="Seg_12012" s="T152">v-v:mood.pn</ta>
            <ta e="T154" id="Seg_12013" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_12014" s="T154">v-v:cvb</ta>
            <ta e="T156" id="Seg_12015" s="T155">v-v:cvb</ta>
            <ta e="T157" id="Seg_12016" s="T156">v-v:tense-v:pred.pn</ta>
            <ta e="T158" id="Seg_12017" s="T157">conj</ta>
            <ta e="T159" id="Seg_12018" s="T158">v-v:cvb</ta>
            <ta e="T160" id="Seg_12019" s="T159">v-v:tense-v:pred.pn</ta>
            <ta e="T161" id="Seg_12020" s="T160">propr-propr&gt;adj</ta>
            <ta e="T162" id="Seg_12021" s="T161">adv</ta>
            <ta e="T163" id="Seg_12022" s="T162">adj-n:case</ta>
            <ta e="T164" id="Seg_12023" s="T163">v-v:mood-v:poss.pn</ta>
            <ta e="T165" id="Seg_12024" s="T164">n-n:case</ta>
            <ta e="T166" id="Seg_12025" s="T165">v-v:cvb</ta>
            <ta e="T167" id="Seg_12026" s="T166">v-v:ptcp-v:(case)</ta>
            <ta e="T168" id="Seg_12027" s="T167">v-v:tense-v:pred.pn</ta>
            <ta e="T169" id="Seg_12028" s="T168">propr-n:case</ta>
            <ta e="T170" id="Seg_12029" s="T169">n-n:(ins)-n:poss-n:case</ta>
            <ta e="T171" id="Seg_12030" s="T170">v-v:tense-v:pred.pn</ta>
            <ta e="T172" id="Seg_12031" s="T171">n-n:poss-n:case</ta>
            <ta e="T173" id="Seg_12032" s="T172">v-v:mood.pn</ta>
            <ta e="T174" id="Seg_12033" s="T173">v-v:ptcp-v:(ins)-v:(case)</ta>
            <ta e="T175" id="Seg_12034" s="T174">v-v:tense-v:poss.pn</ta>
            <ta e="T176" id="Seg_12035" s="T175">cardnum</ta>
            <ta e="T177" id="Seg_12036" s="T176">n-n:case</ta>
            <ta e="T178" id="Seg_12037" s="T177">v-v:mood.pn</ta>
            <ta e="T179" id="Seg_12038" s="T178">n-n:poss-n:case</ta>
            <ta e="T180" id="Seg_12039" s="T179">v-v:cvb</ta>
            <ta e="T181" id="Seg_12040" s="T180">n-n:poss-n:case</ta>
            <ta e="T182" id="Seg_12041" s="T181">v-v:cvb</ta>
            <ta e="T183" id="Seg_12042" s="T182">v-v:cvb</ta>
            <ta e="T184" id="Seg_12043" s="T183">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T185" id="Seg_12044" s="T184">v-v:mood-v:poss.pn</ta>
            <ta e="T186" id="Seg_12045" s="T185">que</ta>
            <ta e="T187" id="Seg_12046" s="T186">cardnum-cardnum&gt;adv</ta>
            <ta e="T188" id="Seg_12047" s="T187">v-v:tense-v:pred.pn</ta>
            <ta e="T189" id="Seg_12048" s="T188">adv</ta>
            <ta e="T190" id="Seg_12049" s="T189">adj</ta>
            <ta e="T191" id="Seg_12050" s="T190">n-n:case</ta>
            <ta e="T192" id="Seg_12051" s="T191">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T193" id="Seg_12052" s="T192">v-v:tense-v:poss.pn</ta>
            <ta e="T194" id="Seg_12053" s="T193">adj-adj&gt;adj</ta>
            <ta e="T195" id="Seg_12054" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_12055" s="T195">n-n:case</ta>
            <ta e="T197" id="Seg_12056" s="T196">n-n:poss-n:case</ta>
            <ta e="T198" id="Seg_12057" s="T197">n-n:case</ta>
            <ta e="T199" id="Seg_12058" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_12059" s="T199">n-n:case</ta>
            <ta e="T201" id="Seg_12060" s="T200">adv</ta>
            <ta e="T202" id="Seg_12061" s="T201">cardnum</ta>
            <ta e="T203" id="Seg_12062" s="T202">cardnum</ta>
            <ta e="T204" id="Seg_12063" s="T203">n-n:case</ta>
            <ta e="T205" id="Seg_12064" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_12065" s="T205">v-v:tense-v:pred.pn</ta>
            <ta e="T207" id="Seg_12066" s="T206">v-v:tense-v:pred.pn</ta>
            <ta e="T208" id="Seg_12067" s="T207">n-n:case</ta>
            <ta e="T209" id="Seg_12068" s="T208">n-n:poss-n:case</ta>
            <ta e="T210" id="Seg_12069" s="T209">n-n:case</ta>
            <ta e="T211" id="Seg_12070" s="T210">adj</ta>
            <ta e="T212" id="Seg_12071" s="T211">n-n:poss-n:case</ta>
            <ta e="T213" id="Seg_12072" s="T212">v-v:mood.pn</ta>
            <ta e="T214" id="Seg_12073" s="T213">v-v:tense-v:pred.pn</ta>
            <ta e="T215" id="Seg_12074" s="T214">propr-n:case</ta>
            <ta e="T216" id="Seg_12075" s="T215">dempro-pro:case</ta>
            <ta e="T217" id="Seg_12076" s="T216">n-n:(num)-n:case</ta>
            <ta e="T218" id="Seg_12077" s="T217">cardnum</ta>
            <ta e="T219" id="Seg_12078" s="T218">adj-n:poss-n:case</ta>
            <ta e="T220" id="Seg_12079" s="T219">v-v:(tense)-v:mood.pn</ta>
            <ta e="T221" id="Seg_12080" s="T220">n-n:poss-n:case</ta>
            <ta e="T222" id="Seg_12081" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_12082" s="T222">n-n&gt;n-n:poss-n:case</ta>
            <ta e="T224" id="Seg_12083" s="T223">v-v:tense-v:poss.pn</ta>
            <ta e="T225" id="Seg_12084" s="T224">cardnum</ta>
            <ta e="T226" id="Seg_12085" s="T225">n-n:case</ta>
            <ta e="T227" id="Seg_12086" s="T226">adj-n:poss-n:case</ta>
            <ta e="T228" id="Seg_12087" s="T227">adj-n:case</ta>
            <ta e="T229" id="Seg_12088" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_12089" s="T229">n-n:poss-n:case</ta>
            <ta e="T231" id="Seg_12090" s="T230">v-v:tense-v:pred.pn</ta>
            <ta e="T232" id="Seg_12091" s="T231">conj</ta>
            <ta e="T233" id="Seg_12092" s="T232">n-n:poss-n:case</ta>
            <ta e="T234" id="Seg_12093" s="T233">v-v:cvb</ta>
            <ta e="T235" id="Seg_12094" s="T234">v-v:tense-v:pred.pn</ta>
            <ta e="T236" id="Seg_12095" s="T235">n-n:case</ta>
            <ta e="T237" id="Seg_12096" s="T236">v-v:cvb</ta>
            <ta e="T238" id="Seg_12097" s="T237">v-v:tense-v:pred.pn</ta>
            <ta e="T239" id="Seg_12098" s="T238">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T240" id="Seg_12099" s="T239">n-n:(ins)-n:poss-n:case</ta>
            <ta e="T241" id="Seg_12100" s="T240">v-v:mood.pn</ta>
            <ta e="T242" id="Seg_12101" s="T241">n-n:poss-n:case</ta>
            <ta e="T243" id="Seg_12102" s="T242">n-n:poss-n:case</ta>
            <ta e="T244" id="Seg_12103" s="T243">v-v&gt;v-v:cvb</ta>
            <ta e="T245" id="Seg_12104" s="T244">v-v:mood.pn</ta>
            <ta e="T246" id="Seg_12105" s="T245">cardnum</ta>
            <ta e="T247" id="Seg_12106" s="T246">n-n:case</ta>
            <ta e="T248" id="Seg_12107" s="T247">v-v:mood.pn</ta>
            <ta e="T249" id="Seg_12108" s="T248">v-v:tense-v:poss.pn</ta>
            <ta e="T250" id="Seg_12109" s="T249">cardnum</ta>
            <ta e="T251" id="Seg_12110" s="T250">ptcl</ta>
            <ta e="T252" id="Seg_12111" s="T251">n-n:case</ta>
            <ta e="T253" id="Seg_12112" s="T252">n-n&gt;n-n:poss-n:case</ta>
            <ta e="T254" id="Seg_12113" s="T253">post</ta>
            <ta e="T255" id="Seg_12114" s="T254">n-n:case</ta>
            <ta e="T256" id="Seg_12115" s="T255">n-n:poss-n:case</ta>
            <ta e="T257" id="Seg_12116" s="T256">v-v:mood.pn</ta>
            <ta e="T258" id="Seg_12117" s="T257">v-v:tense-v:pred.pn</ta>
            <ta e="T259" id="Seg_12118" s="T258">n-n:case</ta>
            <ta e="T260" id="Seg_12119" s="T259">v-v:cvb</ta>
            <ta e="T261" id="Seg_12120" s="T260">v-v:cvb</ta>
            <ta e="T262" id="Seg_12121" s="T261">v-v:tense-v:pred.pn</ta>
            <ta e="T263" id="Seg_12122" s="T262">v-v:mood.pn</ta>
            <ta e="T264" id="Seg_12123" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_12124" s="T264">v-v&gt;v-v:ptcp</ta>
            <ta e="T266" id="Seg_12125" s="T265">n-n:case</ta>
            <ta e="T267" id="Seg_12126" s="T266">v-v:cvb</ta>
            <ta e="T268" id="Seg_12127" s="T267">v-v:mood.pn</ta>
            <ta e="T269" id="Seg_12128" s="T268">v-v:tense-v:pred.pn</ta>
            <ta e="T270" id="Seg_12129" s="T269">n-n:case</ta>
            <ta e="T271" id="Seg_12130" s="T270">interj</ta>
            <ta e="T272" id="Seg_12131" s="T271">n</ta>
            <ta e="T273" id="Seg_12132" s="T272">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T274" id="Seg_12133" s="T273">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T275" id="Seg_12134" s="T274">v-v:tense-v:poss.pn</ta>
            <ta e="T276" id="Seg_12135" s="T275">cardnum</ta>
            <ta e="T277" id="Seg_12136" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_12137" s="T277">n-n:case</ta>
            <ta e="T279" id="Seg_12138" s="T278">n-n&gt;n-n:poss-n:case</ta>
            <ta e="T280" id="Seg_12139" s="T279">post</ta>
            <ta e="T281" id="Seg_12140" s="T280">n-n:case</ta>
            <ta e="T282" id="Seg_12141" s="T281">n-n:poss-n:case</ta>
            <ta e="T283" id="Seg_12142" s="T282">v-v:mood.pn</ta>
            <ta e="T284" id="Seg_12143" s="T283">interj</ta>
            <ta e="T285" id="Seg_12144" s="T284">v-v:cvb</ta>
            <ta e="T286" id="Seg_12145" s="T285">v-v:(ins)-v:mood.pn</ta>
            <ta e="T287" id="Seg_12146" s="T286">v-v:(neg)-v:pred.pn</ta>
            <ta e="T288" id="Seg_12147" s="T287">v-v:tense-v:pred.pn</ta>
            <ta e="T289" id="Seg_12148" s="T288">cardnum</ta>
            <ta e="T290" id="Seg_12149" s="T289">n-n:case</ta>
            <ta e="T291" id="Seg_12150" s="T290">n-n:(poss)-n:case</ta>
            <ta e="T292" id="Seg_12151" s="T291">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T293" id="Seg_12152" s="T292">que-pro:case</ta>
            <ta e="T294" id="Seg_12153" s="T293">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T295" id="Seg_12154" s="T294">adv</ta>
            <ta e="T296" id="Seg_12155" s="T295">n-n&gt;v-v:ptcp</ta>
            <ta e="T297" id="Seg_12156" s="T296">n-n:case</ta>
            <ta e="T298" id="Seg_12157" s="T297">v-v:tense-v:pred.pn</ta>
            <ta e="T299" id="Seg_12158" s="T298">propr-n:case</ta>
            <ta e="T300" id="Seg_12159" s="T299">v-v:cvb</ta>
            <ta e="T301" id="Seg_12160" s="T300">v-v:cvb</ta>
            <ta e="T302" id="Seg_12161" s="T301">v-v:tense-v:pred.pn</ta>
            <ta e="T303" id="Seg_12162" s="T302">n-n:(poss)-n:case</ta>
            <ta e="T304" id="Seg_12163" s="T303">n-n:case</ta>
            <ta e="T305" id="Seg_12164" s="T304">ptcl</ta>
            <ta e="T306" id="Seg_12165" s="T305">v-v:tense-v:pred.pn</ta>
            <ta e="T307" id="Seg_12166" s="T306">adj</ta>
            <ta e="T308" id="Seg_12167" s="T307">n-n:case</ta>
            <ta e="T309" id="Seg_12168" s="T308">adj-n:case</ta>
            <ta e="T310" id="Seg_12169" s="T309">ptcl</ta>
            <ta e="T311" id="Seg_12170" s="T310">v-v:tense-v:pred.pn</ta>
            <ta e="T312" id="Seg_12171" s="T311">n-n:case</ta>
            <ta e="T313" id="Seg_12172" s="T312">v-v:cvb</ta>
            <ta e="T314" id="Seg_12173" s="T313">v-v:tense-v:pred.pn</ta>
            <ta e="T315" id="Seg_12174" s="T314">v-v:cvb</ta>
            <ta e="T316" id="Seg_12175" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_12176" s="T316">n-n:poss-n:case</ta>
            <ta e="T318" id="Seg_12177" s="T317">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T319" id="Seg_12178" s="T318">n-n:poss-n:case</ta>
            <ta e="T320" id="Seg_12179" s="T319">v-v:cvb</ta>
            <ta e="T321" id="Seg_12180" s="T320">v-v:tense-v:pred.pn</ta>
            <ta e="T322" id="Seg_12181" s="T321">v-v:cvb</ta>
            <ta e="T323" id="Seg_12182" s="T322">ptcl</ta>
            <ta e="T324" id="Seg_12183" s="T323">v-v:tense-v:pred.pn</ta>
            <ta e="T325" id="Seg_12184" s="T324">n-n&gt;v-v:cvb</ta>
            <ta e="T326" id="Seg_12185" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_12186" s="T326">v-v:tense-v:pred.pn</ta>
            <ta e="T328" id="Seg_12187" s="T327">n-n:case</ta>
            <ta e="T329" id="Seg_12188" s="T328">post</ta>
            <ta e="T330" id="Seg_12189" s="T329">n-n:case</ta>
            <ta e="T331" id="Seg_12190" s="T330">post</ta>
            <ta e="T332" id="Seg_12191" s="T331">dempro</ta>
            <ta e="T333" id="Seg_12192" s="T332">n-n:case</ta>
            <ta e="T334" id="Seg_12193" s="T333">n-n:poss-n:case</ta>
            <ta e="T335" id="Seg_12194" s="T334">adj-n:case</ta>
            <ta e="T336" id="Seg_12195" s="T335">n-n:case</ta>
            <ta e="T337" id="Seg_12196" s="T336">v-v:tense-v:pred.pn</ta>
            <ta e="T338" id="Seg_12197" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_12198" s="T338">cardnum</ta>
            <ta e="T340" id="Seg_12199" s="T339">n-n:case</ta>
            <ta e="T341" id="Seg_12200" s="T340">n-n:poss-n:case</ta>
            <ta e="T342" id="Seg_12201" s="T341">v-v:mood.pn</ta>
            <ta e="T343" id="Seg_12202" s="T342">dempro-pro:case</ta>
            <ta e="T344" id="Seg_12203" s="T343">n-n:case</ta>
            <ta e="T345" id="Seg_12204" s="T344">n-n:case</ta>
            <ta e="T346" id="Seg_12205" s="T345">adv</ta>
            <ta e="T347" id="Seg_12206" s="T346">n-n:case</ta>
            <ta e="T348" id="Seg_12207" s="T347">v-v:mood.pn</ta>
            <ta e="T349" id="Seg_12208" s="T348">n-n:case</ta>
            <ta e="T350" id="Seg_12209" s="T349">v-v:tense-v:pred.pn</ta>
            <ta e="T351" id="Seg_12210" s="T350">cardnum</ta>
            <ta e="T352" id="Seg_12211" s="T351">n-n:case</ta>
            <ta e="T353" id="Seg_12212" s="T352">n-n:poss-n:case</ta>
            <ta e="T354" id="Seg_12213" s="T353">v-v:tense-v:pred.pn</ta>
            <ta e="T355" id="Seg_12214" s="T354">n-n:case</ta>
            <ta e="T356" id="Seg_12215" s="T355">cardnum</ta>
            <ta e="T357" id="Seg_12216" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_12217" s="T357">n-n:(poss)-n:case</ta>
            <ta e="T359" id="Seg_12218" s="T358">v-v:tense-v:poss.pn</ta>
            <ta e="T360" id="Seg_12219" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_12220" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_12221" s="T361">interj</ta>
            <ta e="T363" id="Seg_12222" s="T362">adv</ta>
            <ta e="T364" id="Seg_12223" s="T363">pers-pro:case</ta>
            <ta e="T365" id="Seg_12224" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_12225" s="T365">v-v:(neg)-v:pred.pn</ta>
            <ta e="T367" id="Seg_12226" s="T366">v-v:tense-v:pred.pn</ta>
            <ta e="T368" id="Seg_12227" s="T367">adv</ta>
            <ta e="T369" id="Seg_12228" s="T368">n-n:case</ta>
            <ta e="T370" id="Seg_12229" s="T369">v-v:tense-v:pred.pn</ta>
            <ta e="T371" id="Seg_12230" s="T370">ptcl</ta>
            <ta e="T372" id="Seg_12231" s="T371">n-n:poss-n:case</ta>
            <ta e="T373" id="Seg_12232" s="T372">v-v:cvb</ta>
            <ta e="T374" id="Seg_12233" s="T373">v-v:tense-v:pred.pn</ta>
            <ta e="T375" id="Seg_12234" s="T374">propr-n:case</ta>
            <ta e="T376" id="Seg_12235" s="T375">propr-n:case</ta>
            <ta e="T377" id="Seg_12236" s="T376">v-v:ptcp</ta>
            <ta e="T378" id="Seg_12237" s="T377">n-n:case</ta>
            <ta e="T379" id="Seg_12238" s="T378">v-v:cvb</ta>
            <ta e="T380" id="Seg_12239" s="T379">v-v:tense-v:pred.pn</ta>
            <ta e="T381" id="Seg_12240" s="T380">n-n:poss-n:case</ta>
            <ta e="T382" id="Seg_12241" s="T381">interj-ptcl</ta>
            <ta e="T383" id="Seg_12242" s="T382">n-n:case</ta>
            <ta e="T384" id="Seg_12243" s="T383">v-v:mood.pn</ta>
            <ta e="T385" id="Seg_12244" s="T384">v-v:cvb</ta>
            <ta e="T386" id="Seg_12245" s="T385">v-v:tense-v:pred.pn</ta>
            <ta e="T387" id="Seg_12246" s="T386">ptcl</ta>
            <ta e="T388" id="Seg_12247" s="T387">v-v:cvb</ta>
            <ta e="T389" id="Seg_12248" s="T388">v-v:tense-v:pred.pn</ta>
            <ta e="T390" id="Seg_12249" s="T389">que</ta>
            <ta e="T391" id="Seg_12250" s="T390">dempro</ta>
            <ta e="T392" id="Seg_12251" s="T391">dempro</ta>
            <ta e="T393" id="Seg_12252" s="T392">n-n:(num)-n:case</ta>
            <ta e="T394" id="Seg_12253" s="T393">adj-n:poss-n:case</ta>
            <ta e="T395" id="Seg_12254" s="T394">v-v:ptcp</ta>
            <ta e="T396" id="Seg_12255" s="T395">v-v:tense-v:pred.pn</ta>
            <ta e="T397" id="Seg_12256" s="T396">v-v:tense-v:pred.pn</ta>
            <ta e="T398" id="Seg_12257" s="T397">adv</ta>
            <ta e="T399" id="Seg_12258" s="T398">propr-n:case</ta>
            <ta e="T400" id="Seg_12259" s="T399">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T401" id="Seg_12260" s="T400">cardnum</ta>
            <ta e="T402" id="Seg_12261" s="T401">n-n:case</ta>
            <ta e="T403" id="Seg_12262" s="T402">n-n:(poss)-n:case</ta>
            <ta e="T404" id="Seg_12263" s="T403">n-n:poss-n:case</ta>
            <ta e="T405" id="Seg_12264" s="T404">v-v:cvb</ta>
            <ta e="T406" id="Seg_12265" s="T405">v-v:tense-v:pred.pn</ta>
            <ta e="T407" id="Seg_12266" s="T406">interj</ta>
            <ta e="T408" id="Seg_12267" s="T407">cardnum</ta>
            <ta e="T409" id="Seg_12268" s="T408">n-n:case</ta>
            <ta e="T410" id="Seg_12269" s="T409">n-n:(poss)-n:case</ta>
            <ta e="T411" id="Seg_12270" s="T410">interj</ta>
            <ta e="T412" id="Seg_12271" s="T411">adv</ta>
            <ta e="T413" id="Seg_12272" s="T412">interj</ta>
            <ta e="T414" id="Seg_12273" s="T413">adv</ta>
            <ta e="T415" id="Seg_12274" s="T414">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T416" id="Seg_12275" s="T415">adj</ta>
            <ta e="T417" id="Seg_12276" s="T416">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T418" id="Seg_12277" s="T417">v-v:cvb</ta>
            <ta e="T419" id="Seg_12278" s="T418">v-v:tense-v:pred.pn</ta>
            <ta e="T420" id="Seg_12279" s="T419">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T421" id="Seg_12280" s="T420">n-n:(poss)-n:case</ta>
            <ta e="T422" id="Seg_12281" s="T421">cardnum</ta>
            <ta e="T423" id="Seg_12282" s="T422">n-n:case</ta>
            <ta e="T424" id="Seg_12283" s="T423">n-n:(poss)-n:case</ta>
            <ta e="T425" id="Seg_12284" s="T424">n-n&gt;v-v:cvb</ta>
            <ta e="T426" id="Seg_12285" s="T425">v-v:ptcp</ta>
            <ta e="T427" id="Seg_12286" s="T426">n-n:case</ta>
            <ta e="T428" id="Seg_12287" s="T427">n-n:case</ta>
            <ta e="T429" id="Seg_12288" s="T428">n-n:poss-n:case</ta>
            <ta e="T430" id="Seg_12289" s="T429">n-n:case</ta>
            <ta e="T431" id="Seg_12290" s="T430">v-v:tense-v:pred.pn</ta>
            <ta e="T432" id="Seg_12291" s="T431">conj</ta>
            <ta e="T433" id="Seg_12292" s="T432">v-v:cvb</ta>
            <ta e="T434" id="Seg_12293" s="T433">v-v:tense-v:pred.pn</ta>
            <ta e="T435" id="Seg_12294" s="T434">propr-n:(poss)-n:case</ta>
            <ta e="T436" id="Seg_12295" s="T435">n-n:case</ta>
            <ta e="T437" id="Seg_12296" s="T436">n-n:poss-n:case</ta>
            <ta e="T438" id="Seg_12297" s="T437">v-v:tense-v:pred.pn</ta>
            <ta e="T439" id="Seg_12298" s="T438">cardnum</ta>
            <ta e="T440" id="Seg_12299" s="T439">n-n:case</ta>
            <ta e="T441" id="Seg_12300" s="T440">n-n:(poss)-n:case</ta>
            <ta e="T442" id="Seg_12301" s="T441">n-n:case</ta>
            <ta e="T443" id="Seg_12302" s="T442">v-v:cvb</ta>
            <ta e="T444" id="Seg_12303" s="T443">v-v:mood-v:temp.pn</ta>
            <ta e="T445" id="Seg_12304" s="T444">v-v:tense-v:pred.pn</ta>
            <ta e="T446" id="Seg_12305" s="T445">interj</ta>
            <ta e="T447" id="Seg_12306" s="T446">cardnum</ta>
            <ta e="T448" id="Seg_12307" s="T447">n-n:case</ta>
            <ta e="T449" id="Seg_12308" s="T448">n-n:(poss)-n:case</ta>
            <ta e="T450" id="Seg_12309" s="T449">interj</ta>
            <ta e="T451" id="Seg_12310" s="T450">adv</ta>
            <ta e="T452" id="Seg_12311" s="T451">interj</ta>
            <ta e="T453" id="Seg_12312" s="T452">adv</ta>
            <ta e="T454" id="Seg_12313" s="T453">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T455" id="Seg_12314" s="T454">adj</ta>
            <ta e="T456" id="Seg_12315" s="T455">n-n:(num)-n:poss-n:case</ta>
            <ta e="T457" id="Seg_12316" s="T456">v-v:cvb</ta>
            <ta e="T458" id="Seg_12317" s="T457">v-v:tense-v:pred.pn</ta>
            <ta e="T459" id="Seg_12318" s="T458">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T460" id="Seg_12319" s="T459">n-n:(poss)-n:case</ta>
            <ta e="T461" id="Seg_12320" s="T460">cardnum</ta>
            <ta e="T462" id="Seg_12321" s="T461">n-n:case</ta>
            <ta e="T463" id="Seg_12322" s="T462">n-n:(poss)-n:case</ta>
            <ta e="T464" id="Seg_12323" s="T463">n-n&gt;v-v:cvb</ta>
            <ta e="T465" id="Seg_12324" s="T464">v-v:ptcp</ta>
            <ta e="T466" id="Seg_12325" s="T465">n-n:case</ta>
            <ta e="T467" id="Seg_12326" s="T466">n-n:case</ta>
            <ta e="T468" id="Seg_12327" s="T467">n-n:poss-n:case</ta>
            <ta e="T469" id="Seg_12328" s="T468">n-n:case</ta>
            <ta e="T470" id="Seg_12329" s="T469">v-v:cvb</ta>
            <ta e="T471" id="Seg_12330" s="T470">n-n:case</ta>
            <ta e="T472" id="Seg_12331" s="T471">v-v:tense-v:pred.pn</ta>
            <ta e="T473" id="Seg_12332" s="T472">que</ta>
            <ta e="T474" id="Seg_12333" s="T473">ptcl</ta>
            <ta e="T475" id="Seg_12334" s="T474">propr-n:case</ta>
            <ta e="T476" id="Seg_12335" s="T475">adj</ta>
            <ta e="T477" id="Seg_12336" s="T476">n-n:(ins)-n:case</ta>
            <ta e="T478" id="Seg_12337" s="T477">v-v:cvb</ta>
            <ta e="T479" id="Seg_12338" s="T478">v-v:tense-v:pred.pn</ta>
            <ta e="T480" id="Seg_12339" s="T479">conj</ta>
            <ta e="T481" id="Seg_12340" s="T480">cardnum</ta>
            <ta e="T482" id="Seg_12341" s="T481">n-n:(poss)-n:case</ta>
            <ta e="T483" id="Seg_12342" s="T482">v-v:cvb</ta>
            <ta e="T484" id="Seg_12343" s="T483">v-v:tense-v:pred.pn</ta>
            <ta e="T485" id="Seg_12344" s="T484">v-v:tense-v:poss.pn</ta>
            <ta e="T486" id="Seg_12345" s="T485">n-n:case</ta>
            <ta e="T487" id="Seg_12346" s="T486">v-v:cvb</ta>
            <ta e="T488" id="Seg_12347" s="T487">v-v:tense-v:pred.pn</ta>
            <ta e="T489" id="Seg_12348" s="T488">n-n:(poss)-n:case</ta>
            <ta e="T490" id="Seg_12349" s="T489">v-v:cvb</ta>
            <ta e="T491" id="Seg_12350" s="T490">v-v:tense-v:pred.pn</ta>
            <ta e="T492" id="Seg_12351" s="T491">n-n:(poss)-n:case</ta>
            <ta e="T493" id="Seg_12352" s="T492">n-n:case</ta>
            <ta e="T494" id="Seg_12353" s="T493">v-v:tense-v:pred.pn</ta>
            <ta e="T495" id="Seg_12354" s="T494">propr-n:case</ta>
            <ta e="T496" id="Seg_12355" s="T495">n-n:poss-n:case</ta>
            <ta e="T497" id="Seg_12356" s="T496">n-n:poss-n:case</ta>
            <ta e="T498" id="Seg_12357" s="T497">v-v:cvb</ta>
            <ta e="T499" id="Seg_12358" s="T498">v-v:tense-v:pred.pn</ta>
            <ta e="T500" id="Seg_12359" s="T499">v-v:(neg)-v:pred.pn</ta>
            <ta e="T501" id="Seg_12360" s="T500">que</ta>
            <ta e="T502" id="Seg_12361" s="T501">post</ta>
            <ta e="T503" id="Seg_12362" s="T502">v-v:tense-v:pred.pn</ta>
            <ta e="T504" id="Seg_12363" s="T503">n-n:case</ta>
            <ta e="T505" id="Seg_12364" s="T504">v-v:cvb</ta>
            <ta e="T506" id="Seg_12365" s="T505">v-v:tense-v:pred.pn</ta>
            <ta e="T507" id="Seg_12366" s="T506">propr-n:case</ta>
            <ta e="T508" id="Seg_12367" s="T507">v-v:cvb</ta>
            <ta e="T509" id="Seg_12368" s="T508">v-v&gt;v-v:cvb</ta>
            <ta e="T510" id="Seg_12369" s="T509">n-n:poss-n:case</ta>
            <ta e="T511" id="Seg_12370" s="T510">v-v:cvb</ta>
            <ta e="T512" id="Seg_12371" s="T511">v-v:tense-v:pred.pn</ta>
            <ta e="T513" id="Seg_12372" s="T512">propr-n:case</ta>
            <ta e="T514" id="Seg_12373" s="T513">cardnum</ta>
            <ta e="T515" id="Seg_12374" s="T514">n-n:case</ta>
            <ta e="T516" id="Seg_12375" s="T515">n-n:poss-n:case</ta>
            <ta e="T517" id="Seg_12376" s="T516">v-v:tense-v:pred.pn</ta>
            <ta e="T518" id="Seg_12377" s="T517">ptcl</ta>
            <ta e="T519" id="Seg_12378" s="T518">cardnum</ta>
            <ta e="T520" id="Seg_12379" s="T519">n-n:case</ta>
            <ta e="T521" id="Seg_12380" s="T520">n-n:(poss)-n:case</ta>
            <ta e="T522" id="Seg_12381" s="T521">n-n&gt;adj-n:case</ta>
            <ta e="T523" id="Seg_12382" s="T522">v-v:ptcp-v:(case)</ta>
            <ta e="T524" id="Seg_12383" s="T523">n-n:(num)-n:poss-n:case</ta>
            <ta e="T525" id="Seg_12384" s="T524">v-v:cvb</ta>
            <ta e="T526" id="Seg_12385" s="T525">v-v:mood.pn</ta>
            <ta e="T527" id="Seg_12386" s="T526">adj-adj&gt;adv</ta>
            <ta e="T528" id="Seg_12387" s="T527">v-v:tense-v:poss.pn</ta>
            <ta e="T529" id="Seg_12388" s="T528">cardnum</ta>
            <ta e="T530" id="Seg_12389" s="T529">n-n:(ins)-n:case</ta>
            <ta e="T531" id="Seg_12390" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_12391" s="T531">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T533" id="Seg_12392" s="T532">n-n:poss-n:case</ta>
            <ta e="T534" id="Seg_12393" s="T533">v-v:cvb</ta>
            <ta e="T535" id="Seg_12394" s="T534">v-v:tense-v:poss.pn</ta>
            <ta e="T536" id="Seg_12395" s="T535">n-n:poss-n:case</ta>
            <ta e="T537" id="Seg_12396" s="T536">v-v:cvb</ta>
            <ta e="T538" id="Seg_12397" s="T537">v-v:cvb</ta>
            <ta e="T539" id="Seg_12398" s="T538">v-v:tense-v:pred.pn</ta>
            <ta e="T540" id="Seg_12399" s="T539">que</ta>
            <ta e="T541" id="Seg_12400" s="T540">n-n:case</ta>
            <ta e="T542" id="Seg_12401" s="T541">n-n:(poss)-n:case</ta>
            <ta e="T543" id="Seg_12402" s="T542">v-v:tense-v:pred.pn</ta>
            <ta e="T544" id="Seg_12403" s="T543">v-v:ptcp</ta>
            <ta e="T545" id="Seg_12404" s="T544">v-v:(neg)-v:pred.pn</ta>
            <ta e="T546" id="Seg_12405" s="T545">v-v&gt;v-v:cvb</ta>
            <ta e="T547" id="Seg_12406" s="T546">v-v:cvb</ta>
            <ta e="T548" id="Seg_12407" s="T547">n-n:poss-n:case</ta>
            <ta e="T549" id="Seg_12408" s="T548">post</ta>
            <ta e="T550" id="Seg_12409" s="T549">v-v:tense-v:pred.pn</ta>
            <ta e="T551" id="Seg_12410" s="T550">v-v:ptcp-v:(case)</ta>
            <ta e="T552" id="Seg_12411" s="T551">conj</ta>
            <ta e="T553" id="Seg_12412" s="T552">post</ta>
            <ta e="T554" id="Seg_12413" s="T553">v-v:cvb</ta>
            <ta e="T555" id="Seg_12414" s="T554">n-n:poss-n:case</ta>
            <ta e="T556" id="Seg_12415" s="T555">n-n:poss-n:case</ta>
            <ta e="T557" id="Seg_12416" s="T556">v-v:tense-v:pred.pn</ta>
            <ta e="T558" id="Seg_12417" s="T557">n-n:(poss)-n:case</ta>
            <ta e="T559" id="Seg_12418" s="T558">n-n:poss-n:case</ta>
            <ta e="T560" id="Seg_12419" s="T559">n-n&gt;v-v:cvb</ta>
            <ta e="T561" id="Seg_12420" s="T560">v-v:tense-v:pred.pn</ta>
            <ta e="T562" id="Seg_12421" s="T561">n-n:(poss)-n:case</ta>
            <ta e="T563" id="Seg_12422" s="T562">v-v:cvb</ta>
            <ta e="T564" id="Seg_12423" s="T563">v-v:tense-v:pred.pn</ta>
            <ta e="T565" id="Seg_12424" s="T564">ptcl</ta>
            <ta e="T566" id="Seg_12425" s="T565">n-n:case</ta>
            <ta e="T567" id="Seg_12426" s="T566">n-n:poss-n:case</ta>
            <ta e="T568" id="Seg_12427" s="T567">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T569" id="Seg_12428" s="T568">n-n:poss-n:case</ta>
            <ta e="T570" id="Seg_12429" s="T569">v-v:ptcp-v:(ins)-v:(poss)-v:(case)</ta>
            <ta e="T571" id="Seg_12430" s="T570">v-v:mood-v:pred.pn</ta>
            <ta e="T572" id="Seg_12431" s="T571">v-v:ptcp</ta>
            <ta e="T573" id="Seg_12432" s="T572">v-v:tense-v:poss.pn</ta>
            <ta e="T574" id="Seg_12433" s="T573">cardnum</ta>
            <ta e="T575" id="Seg_12434" s="T574">n-n:case</ta>
            <ta e="T576" id="Seg_12435" s="T575">n-n:(poss)-n:case</ta>
            <ta e="T577" id="Seg_12436" s="T576">v-v:cvb</ta>
            <ta e="T578" id="Seg_12437" s="T577">v-v:tense-v:pred.pn</ta>
            <ta e="T579" id="Seg_12438" s="T578">v-v:tense-v:pred.pn</ta>
            <ta e="T580" id="Seg_12439" s="T579">que</ta>
            <ta e="T581" id="Seg_12440" s="T580">ptcl</ta>
            <ta e="T582" id="Seg_12441" s="T581">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T583" id="Seg_12442" s="T582">n-n:poss-n:case</ta>
            <ta e="T584" id="Seg_12443" s="T583">ptcl</ta>
            <ta e="T585" id="Seg_12444" s="T584">cardnum</ta>
            <ta e="T586" id="Seg_12445" s="T585">n-n:case</ta>
            <ta e="T587" id="Seg_12446" s="T586">n-n:(poss)-n:case</ta>
            <ta e="T588" id="Seg_12447" s="T587">n-n:poss-n:case</ta>
            <ta e="T589" id="Seg_12448" s="T588">v-v:mood.pn</ta>
            <ta e="T590" id="Seg_12449" s="T589">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T591" id="Seg_12450" s="T590">v-v:tense-v:pred.pn</ta>
            <ta e="T592" id="Seg_12451" s="T591">propr-n:case</ta>
            <ta e="T593" id="Seg_12452" s="T592">ptcl</ta>
            <ta e="T594" id="Seg_12453" s="T593">v-v:tense-v:pred.pn</ta>
            <ta e="T595" id="Seg_12454" s="T594">cardnum</ta>
            <ta e="T596" id="Seg_12455" s="T595">cardnum</ta>
            <ta e="T597" id="Seg_12456" s="T596">n-n:case</ta>
            <ta e="T598" id="Seg_12457" s="T597">n-n:poss-n:case</ta>
            <ta e="T599" id="Seg_12458" s="T598">propr-n:case</ta>
            <ta e="T600" id="Seg_12459" s="T599">v-v:tense-v:pred.pn</ta>
            <ta e="T601" id="Seg_12460" s="T600">pers-pro:case</ta>
            <ta e="T602" id="Seg_12461" s="T601">cardnum</ta>
            <ta e="T603" id="Seg_12462" s="T602">n-n:case</ta>
            <ta e="T604" id="Seg_12463" s="T603">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T605" id="Seg_12464" s="T604">cardnum</ta>
            <ta e="T606" id="Seg_12465" s="T605">n-n:case</ta>
            <ta e="T607" id="Seg_12466" s="T606">n-n:(poss)-n:case</ta>
            <ta e="T608" id="Seg_12467" s="T607">conj</ta>
            <ta e="T609" id="Seg_12468" s="T608">cardnum</ta>
            <ta e="T610" id="Seg_12469" s="T609">n-n:case</ta>
            <ta e="T611" id="Seg_12470" s="T610">n-n:(poss)-n:case</ta>
            <ta e="T612" id="Seg_12471" s="T611">adv</ta>
            <ta e="T613" id="Seg_12472" s="T612">n-n:case</ta>
            <ta e="T614" id="Seg_12473" s="T613">v-v:cvb</ta>
            <ta e="T615" id="Seg_12474" s="T614">v-v:tense-v:poss.pn</ta>
            <ta e="T616" id="Seg_12475" s="T615">n-n:(poss)-n:case</ta>
            <ta e="T617" id="Seg_12476" s="T616">dempro-pro:case</ta>
            <ta e="T618" id="Seg_12477" s="T617">n-n:case</ta>
            <ta e="T619" id="Seg_12478" s="T618">n-n:case</ta>
            <ta e="T620" id="Seg_12479" s="T619">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T621" id="Seg_12480" s="T620">n-n:(poss)-n:case</ta>
            <ta e="T622" id="Seg_12481" s="T621">v-v&gt;v-v:(ins)-v:mood.pn</ta>
            <ta e="T623" id="Seg_12482" s="T622">adj-n:(poss)-n:case</ta>
            <ta e="T624" id="Seg_12483" s="T623">cardnum</ta>
            <ta e="T625" id="Seg_12484" s="T624">n-n:case</ta>
            <ta e="T626" id="Seg_12485" s="T625">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T627" id="Seg_12486" s="T626">n-n:poss-n:case</ta>
            <ta e="T628" id="Seg_12487" s="T627">v-v:tense-v:poss.pn</ta>
            <ta e="T629" id="Seg_12488" s="T628">adj-n:(poss)-n:case</ta>
            <ta e="T630" id="Seg_12489" s="T629">pers-pro:case</ta>
            <ta e="T631" id="Seg_12490" s="T630">v-v:tense-v:poss.pn</ta>
            <ta e="T632" id="Seg_12491" s="T631">adj</ta>
            <ta e="T633" id="Seg_12492" s="T632">n-n:case</ta>
            <ta e="T634" id="Seg_12493" s="T633">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T635" id="Seg_12494" s="T634">n-n:(poss)-n:case</ta>
            <ta e="T636" id="Seg_12495" s="T635">n-n:poss-n:case</ta>
            <ta e="T637" id="Seg_12496" s="T636">n-n:poss-n:case</ta>
            <ta e="T638" id="Seg_12497" s="T637">v-v:(tense)-v:mood.pn</ta>
            <ta e="T639" id="Seg_12498" s="T638">v-v:tense-v:pred.pn</ta>
            <ta e="T640" id="Seg_12499" s="T639">n-n:case</ta>
            <ta e="T641" id="Seg_12500" s="T640">n-n:case</ta>
            <ta e="T642" id="Seg_12501" s="T641">v-v:tense-v:pred.pn</ta>
            <ta e="T643" id="Seg_12502" s="T642">v-v:(ins)-v&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T644" id="Seg_12503" s="T643">v-v:tense-v:poss.pn</ta>
            <ta e="T645" id="Seg_12504" s="T644">n-n:case</ta>
            <ta e="T646" id="Seg_12505" s="T645">post-n:poss-n:case</ta>
            <ta e="T647" id="Seg_12506" s="T646">n-n:case</ta>
            <ta e="T648" id="Seg_12507" s="T647">ptcl</ta>
            <ta e="T649" id="Seg_12508" s="T648">v-v:tense-v:poss.pn</ta>
            <ta e="T650" id="Seg_12509" s="T649">dempro</ta>
            <ta e="T651" id="Seg_12510" s="T650">n-n:case</ta>
            <ta e="T652" id="Seg_12511" s="T651">n-n:case</ta>
            <ta e="T653" id="Seg_12512" s="T652">post</ta>
            <ta e="T654" id="Seg_12513" s="T653">n-n:poss-n:case</ta>
            <ta e="T655" id="Seg_12514" s="T654">v-v:tense-v:poss.pn</ta>
            <ta e="T656" id="Seg_12515" s="T655">cardnum</ta>
            <ta e="T657" id="Seg_12516" s="T656">n-n:case</ta>
            <ta e="T658" id="Seg_12517" s="T657">post</ta>
            <ta e="T659" id="Seg_12518" s="T658">adj-n:(poss)-n:case</ta>
            <ta e="T660" id="Seg_12519" s="T659">ptcl</ta>
            <ta e="T661" id="Seg_12520" s="T660">n-n:(poss)-n:case</ta>
            <ta e="T662" id="Seg_12521" s="T661">v-v:mood-v:temp.pn</ta>
            <ta e="T663" id="Seg_12522" s="T662">n-n:case</ta>
            <ta e="T664" id="Seg_12523" s="T663">v-v:tense-v:poss.pn</ta>
            <ta e="T665" id="Seg_12524" s="T664">n-n:poss-n:case</ta>
            <ta e="T666" id="Seg_12525" s="T665">n-n:poss-n:case</ta>
            <ta e="T667" id="Seg_12526" s="T666">n-n&gt;adj-n:case</ta>
            <ta e="T668" id="Seg_12527" s="T667">v-v:tense-v:poss.pn</ta>
            <ta e="T669" id="Seg_12528" s="T668">dempro-pro:case</ta>
            <ta e="T670" id="Seg_12529" s="T669">pers-pro:case</ta>
            <ta e="T671" id="Seg_12530" s="T670">n-n:case</ta>
            <ta e="T672" id="Seg_12531" s="T671">n-n:(poss)-n:case</ta>
            <ta e="T673" id="Seg_12532" s="T672">adj</ta>
            <ta e="T674" id="Seg_12533" s="T673">n-n:case</ta>
            <ta e="T675" id="Seg_12534" s="T674">v-v:tense-v:poss.pn</ta>
            <ta e="T676" id="Seg_12535" s="T675">n-n:(num)-n:poss-n:case</ta>
            <ta e="T677" id="Seg_12536" s="T676">v-v:tense-v:poss.pn</ta>
            <ta e="T678" id="Seg_12537" s="T677">n-n:(num)-n:case</ta>
            <ta e="T679" id="Seg_12538" s="T678">n-n:poss-n:case</ta>
            <ta e="T680" id="Seg_12539" s="T679">n-n:case-n-n:case</ta>
            <ta e="T681" id="Seg_12540" s="T680">post</ta>
            <ta e="T682" id="Seg_12541" s="T681">v-v:tense-v:poss.pn</ta>
            <ta e="T683" id="Seg_12542" s="T682">cardnum</ta>
            <ta e="T684" id="Seg_12543" s="T683">n-n:case</ta>
            <ta e="T685" id="Seg_12544" s="T684">n-n:(poss)-n:case</ta>
            <ta e="T686" id="Seg_12545" s="T685">cardnum</ta>
            <ta e="T687" id="Seg_12546" s="T686">n-n:case</ta>
            <ta e="T688" id="Seg_12547" s="T687">n-n:(poss)-n:case</ta>
            <ta e="T689" id="Seg_12548" s="T688">ptcl</ta>
            <ta e="T690" id="Seg_12549" s="T689">v-v:tense-v:poss.pn</ta>
            <ta e="T691" id="Seg_12550" s="T690">pers-pro:case</ta>
            <ta e="T692" id="Seg_12551" s="T691">ptcl</ta>
            <ta e="T693" id="Seg_12552" s="T692">dempro</ta>
            <ta e="T694" id="Seg_12553" s="T693">adj-adj&gt;adv</ta>
            <ta e="T695" id="Seg_12554" s="T694">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T696" id="Seg_12555" s="T695">post</ta>
            <ta e="T697" id="Seg_12556" s="T696">adj</ta>
            <ta e="T698" id="Seg_12557" s="T697">n-n:case</ta>
            <ta e="T699" id="Seg_12558" s="T698">n-n:(num)-n:case</ta>
            <ta e="T700" id="Seg_12559" s="T699">n-n:(num)-n:case</ta>
            <ta e="T701" id="Seg_12560" s="T700">ptcl</ta>
            <ta e="T702" id="Seg_12561" s="T701">v-v:cvb</ta>
            <ta e="T703" id="Seg_12562" s="T702">v-v:tense-v:poss.pn</ta>
            <ta e="T704" id="Seg_12563" s="T703">dempro</ta>
            <ta e="T705" id="Seg_12564" s="T704">n-n:case</ta>
            <ta e="T706" id="Seg_12565" s="T705">v-v:ptcp</ta>
            <ta e="T707" id="Seg_12566" s="T706">n-n:poss-n:case</ta>
            <ta e="T708" id="Seg_12567" s="T707">v-v:tense-v:poss.pn</ta>
            <ta e="T709" id="Seg_12568" s="T708">adj-adj&gt;adv</ta>
            <ta e="T710" id="Seg_12569" s="T709">v-v:mood-v:temp.pn</ta>
            <ta e="T711" id="Seg_12570" s="T710">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T712" id="Seg_12571" s="T711">n-n:poss-n:case</ta>
            <ta e="T713" id="Seg_12572" s="T712">v-v:tense-v:poss.pn</ta>
            <ta e="T714" id="Seg_12573" s="T713">v-v:tense-v:pred.pn</ta>
            <ta e="T715" id="Seg_12574" s="T714">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T716" id="Seg_12575" s="T715">n-n:poss-n:case</ta>
            <ta e="T717" id="Seg_12576" s="T716">v-v:tense-v:pred.pn</ta>
            <ta e="T718" id="Seg_12577" s="T717">propr-n:case</ta>
            <ta e="T719" id="Seg_12578" s="T718">v-v:cvb</ta>
            <ta e="T720" id="Seg_12579" s="T719">v-v:tense-v:pred.pn</ta>
            <ta e="T721" id="Seg_12580" s="T720">adv</ta>
            <ta e="T722" id="Seg_12581" s="T721">adv</ta>
            <ta e="T723" id="Seg_12582" s="T722">dempro</ta>
            <ta e="T724" id="Seg_12583" s="T723">n-n:case</ta>
            <ta e="T725" id="Seg_12584" s="T724">adj-n:case</ta>
            <ta e="T726" id="Seg_12585" s="T725">v-v:cvb</ta>
            <ta e="T727" id="Seg_12586" s="T726">v-v:cvb</ta>
            <ta e="T728" id="Seg_12587" s="T727">v-v:tense-v:pred.pn</ta>
            <ta e="T729" id="Seg_12588" s="T728">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T730" id="Seg_12589" s="T729">v-v:tense-v:pred.pn</ta>
            <ta e="T731" id="Seg_12590" s="T730">n-n:case-n-n:case</ta>
            <ta e="T732" id="Seg_12591" s="T731">adj-n:poss-n:case</ta>
            <ta e="T733" id="Seg_12592" s="T732">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T734" id="Seg_12593" s="T733">dempro</ta>
            <ta e="T735" id="Seg_12594" s="T734">n-n:case</ta>
            <ta e="T736" id="Seg_12595" s="T735">n-n:poss-n:case</ta>
            <ta e="T737" id="Seg_12596" s="T736">n-n:(poss)-n:case</ta>
            <ta e="T738" id="Seg_12597" s="T737">v-v:tense-v:pred.pn</ta>
            <ta e="T739" id="Seg_12598" s="T738">n-n:poss-n:case</ta>
            <ta e="T740" id="Seg_12599" s="T739">v-v:cvb</ta>
            <ta e="T741" id="Seg_12600" s="T740">v-v:mood-v:poss.pn</ta>
            <ta e="T742" id="Seg_12601" s="T741">adv</ta>
            <ta e="T743" id="Seg_12602" s="T742">n-n:case</ta>
            <ta e="T744" id="Seg_12603" s="T743">post</ta>
            <ta e="T745" id="Seg_12604" s="T744">n-n:case</ta>
            <ta e="T746" id="Seg_12605" s="T745">v-v:tense-v:pred.pn</ta>
            <ta e="T747" id="Seg_12606" s="T746">n-n:poss-n:case</ta>
            <ta e="T748" id="Seg_12607" s="T747">n-n:case</ta>
            <ta e="T749" id="Seg_12608" s="T748">v-v:tense-v:pred.pn</ta>
            <ta e="T750" id="Seg_12609" s="T749">que</ta>
            <ta e="T751" id="Seg_12610" s="T750">post</ta>
            <ta e="T752" id="Seg_12611" s="T751">n-n:(pred.pn)-ptcl</ta>
            <ta e="T753" id="Seg_12612" s="T752">que</ta>
            <ta e="T754" id="Seg_12613" s="T753">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T755" id="Seg_12614" s="T754">n-n:case-n-n:case</ta>
            <ta e="T756" id="Seg_12615" s="T755">v-v:(neg)-v:pred.pn</ta>
            <ta e="T757" id="Seg_12616" s="T756">v-v:ptcp</ta>
            <ta e="T758" id="Seg_12617" s="T757">n-n:poss-n:case</ta>
            <ta e="T759" id="Seg_12618" s="T758">v-v:cvb</ta>
            <ta e="T760" id="Seg_12619" s="T759">v-v:tense-v:pred.pn</ta>
            <ta e="T761" id="Seg_12620" s="T760">que-pro:(poss)-pro:case-ptcl</ta>
            <ta e="T762" id="Seg_12621" s="T761">dempro-pro:case</ta>
            <ta e="T763" id="Seg_12622" s="T762">cardnum</ta>
            <ta e="T764" id="Seg_12623" s="T763">n-n:case</ta>
            <ta e="T765" id="Seg_12624" s="T764">n-n:(poss)-n:case</ta>
            <ta e="T766" id="Seg_12625" s="T765">interj</ta>
            <ta e="T767" id="Seg_12626" s="T766">dempro-pro:case</ta>
            <ta e="T768" id="Seg_12627" s="T767">pers-pro:case</ta>
            <ta e="T769" id="Seg_12628" s="T768">n-n:(poss)-n:case</ta>
            <ta e="T770" id="Seg_12629" s="T769">dempro-pro:case</ta>
            <ta e="T771" id="Seg_12630" s="T770">n-n:case</ta>
            <ta e="T772" id="Seg_12631" s="T771">v-v:cvb</ta>
            <ta e="T773" id="Seg_12632" s="T772">v-v:tense-v:pred.pn</ta>
            <ta e="T774" id="Seg_12633" s="T773">ptcl</ta>
            <ta e="T775" id="Seg_12634" s="T774">adv</ta>
            <ta e="T776" id="Seg_12635" s="T775">pers-pro:case</ta>
            <ta e="T777" id="Seg_12636" s="T776">n-n:case</ta>
            <ta e="T778" id="Seg_12637" s="T777">v-v:mood.pn</ta>
            <ta e="T779" id="Seg_12638" s="T778">v-v:cvb</ta>
            <ta e="T780" id="Seg_12639" s="T779">v-v:(neg)-v:pred.pn</ta>
            <ta e="T781" id="Seg_12640" s="T780">conj</ta>
            <ta e="T782" id="Seg_12641" s="T781">n-n:case</ta>
            <ta e="T783" id="Seg_12642" s="T782">v-v:tense-v:poss.pn</ta>
            <ta e="T784" id="Seg_12643" s="T783">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T785" id="Seg_12644" s="T784">dempro</ta>
            <ta e="T786" id="Seg_12645" s="T785">cardnum</ta>
            <ta e="T787" id="Seg_12646" s="T786">n-n&gt;adj</ta>
            <ta e="T788" id="Seg_12647" s="T787">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T789" id="Seg_12648" s="T788">adj</ta>
            <ta e="T790" id="Seg_12649" s="T789">n-n:(poss)-n:case</ta>
            <ta e="T791" id="Seg_12650" s="T790">n-n:(poss)-n:case</ta>
            <ta e="T792" id="Seg_12651" s="T791">v-v:tense-v:poss.pn</ta>
            <ta e="T793" id="Seg_12652" s="T792">cardnum-cardnum&gt;adv</ta>
            <ta e="T794" id="Seg_12653" s="T793">n-n:case</ta>
            <ta e="T795" id="Seg_12654" s="T794">n-n:(poss)-n:case</ta>
            <ta e="T796" id="Seg_12655" s="T795">n-n&gt;adj-n:case</ta>
            <ta e="T797" id="Seg_12656" s="T796">n-n:case</ta>
            <ta e="T798" id="Seg_12657" s="T797">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T799" id="Seg_12658" s="T798">v-v:tense-v:pred.pn</ta>
            <ta e="T800" id="Seg_12659" s="T799">v-v:cvb</ta>
            <ta e="T801" id="Seg_12660" s="T800">v-v:tense-v:pred.pn</ta>
            <ta e="T802" id="Seg_12661" s="T801">n-n:(poss)-n:case</ta>
            <ta e="T803" id="Seg_12662" s="T802">v-v:tense-v:poss.pn</ta>
            <ta e="T804" id="Seg_12663" s="T803">adj</ta>
            <ta e="T805" id="Seg_12664" s="T804">n-n:case</ta>
            <ta e="T806" id="Seg_12665" s="T805">n-n:(poss)-n:case</ta>
            <ta e="T807" id="Seg_12666" s="T806">v-v:tense-v:poss.pn</ta>
            <ta e="T808" id="Seg_12667" s="T807">ptcl</ta>
            <ta e="T809" id="Seg_12668" s="T808">n-n:case</ta>
            <ta e="T810" id="Seg_12669" s="T809">v-v:tense-v:pred.pn</ta>
            <ta e="T811" id="Seg_12670" s="T810">cardnum</ta>
            <ta e="T812" id="Seg_12671" s="T811">n-n&gt;n-n:case</ta>
            <ta e="T813" id="Seg_12672" s="T812">ptcl</ta>
            <ta e="T814" id="Seg_12673" s="T813">interj-n&gt;v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T815" id="Seg_12674" s="T814">que</ta>
            <ta e="T816" id="Seg_12675" s="T815">post</ta>
            <ta e="T817" id="Seg_12676" s="T816">n-n&gt;adj-n-n&gt;adj</ta>
            <ta e="T818" id="Seg_12677" s="T817">n-n:(pred.pn)-ptcl</ta>
            <ta e="T819" id="Seg_12678" s="T818">n-n:case-n-n:case</ta>
            <ta e="T820" id="Seg_12679" s="T819">ptcl</ta>
            <ta e="T821" id="Seg_12680" s="T820">v-v:tense-v:pred.pn</ta>
            <ta e="T822" id="Seg_12681" s="T821">ptcl</ta>
            <ta e="T823" id="Seg_12682" s="T822">n-n:poss-n:case</ta>
            <ta e="T824" id="Seg_12683" s="T823">v-v:tense-v:pred.pn</ta>
            <ta e="T825" id="Seg_12684" s="T824">que-pro:case</ta>
            <ta e="T826" id="Seg_12685" s="T825">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T827" id="Seg_12686" s="T826">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T828" id="Seg_12687" s="T827">interj</ta>
            <ta e="T829" id="Seg_12688" s="T828">cardnum</ta>
            <ta e="T830" id="Seg_12689" s="T829">n-n:case</ta>
            <ta e="T831" id="Seg_12690" s="T830">n-n:(poss)-n:case</ta>
            <ta e="T832" id="Seg_12691" s="T831">interj</ta>
            <ta e="T833" id="Seg_12692" s="T832">dempro</ta>
            <ta e="T834" id="Seg_12693" s="T833">n-n:case</ta>
            <ta e="T835" id="Seg_12694" s="T834">v-v:cvb</ta>
            <ta e="T836" id="Seg_12695" s="T835">v-v:tense-v:pred.pn</ta>
            <ta e="T837" id="Seg_12696" s="T836">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T838" id="Seg_12697" s="T837">interj</ta>
            <ta e="T839" id="Seg_12698" s="T838">pers-pro:case</ta>
            <ta e="T840" id="Seg_12699" s="T839">n-n:case</ta>
            <ta e="T841" id="Seg_12700" s="T840">v-v:tense-v:poss.pn</ta>
            <ta e="T842" id="Seg_12701" s="T841">v-v:(neg)-v:pred.pn</ta>
            <ta e="T843" id="Seg_12702" s="T842">v-v:tense-v:poss.pn</ta>
            <ta e="T844" id="Seg_12703" s="T843">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T845" id="Seg_12704" s="T844">dempro</ta>
            <ta e="T846" id="Seg_12705" s="T845">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T847" id="Seg_12706" s="T846">n-n:poss-n:case</ta>
            <ta e="T848" id="Seg_12707" s="T847">n-n:case</ta>
            <ta e="T849" id="Seg_12708" s="T848">v-v:tense-v:poss.pn</ta>
            <ta e="T850" id="Seg_12709" s="T849">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T851" id="Seg_12710" s="T850">dempro</ta>
            <ta e="T852" id="Seg_12711" s="T851">n-n:case</ta>
            <ta e="T853" id="Seg_12712" s="T852">n-n:poss-n:case</ta>
            <ta e="T854" id="Seg_12713" s="T853">v-v:tense-v:pred.pn</ta>
            <ta e="T855" id="Seg_12714" s="T854">que</ta>
            <ta e="T856" id="Seg_12715" s="T855">v-v:(neg)-v:pred.pn</ta>
            <ta e="T857" id="Seg_12716" s="T856">n-n:case</ta>
            <ta e="T858" id="Seg_12717" s="T857">n-n:case</ta>
            <ta e="T859" id="Seg_12718" s="T858">v-v:tense-v:pred.pn</ta>
            <ta e="T860" id="Seg_12719" s="T859">ptcl</ta>
            <ta e="T861" id="Seg_12720" s="T860">ptcl</ta>
            <ta e="T862" id="Seg_12721" s="T861">v-v:mood.pn</ta>
            <ta e="T863" id="Seg_12722" s="T862">v-v:ptcp-v:(case)</ta>
            <ta e="T864" id="Seg_12723" s="T863">ptcl</ta>
            <ta e="T865" id="Seg_12724" s="T864">adj-n:(poss)-n:case</ta>
            <ta e="T866" id="Seg_12725" s="T865">conj</ta>
            <ta e="T867" id="Seg_12726" s="T866">adv</ta>
            <ta e="T868" id="Seg_12727" s="T867">ptcl</ta>
            <ta e="T869" id="Seg_12728" s="T868">v-v:tense-v:poss.pn</ta>
            <ta e="T870" id="Seg_12729" s="T869">adv</ta>
            <ta e="T871" id="Seg_12730" s="T870">v-v:tense-v:pred.pn</ta>
            <ta e="T872" id="Seg_12731" s="T871">cardnum</ta>
            <ta e="T873" id="Seg_12732" s="T872">n-n:case</ta>
            <ta e="T874" id="Seg_12733" s="T873">v-v:tense-v:pred.pn</ta>
            <ta e="T875" id="Seg_12734" s="T874">n-n:case</ta>
            <ta e="T876" id="Seg_12735" s="T875">n-n:poss-n:case</ta>
            <ta e="T877" id="Seg_12736" s="T876">v-v:cvb</ta>
            <ta e="T878" id="Seg_12737" s="T877">v-v:tense-v:pred.pn</ta>
            <ta e="T879" id="Seg_12738" s="T878">n-n:case</ta>
            <ta e="T880" id="Seg_12739" s="T879">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T881" id="Seg_12740" s="T880">n-n:case</ta>
            <ta e="T882" id="Seg_12741" s="T881">v-v:cvb</ta>
            <ta e="T883" id="Seg_12742" s="T882">v-v:tense-v:pred.pn</ta>
            <ta e="T884" id="Seg_12743" s="T883">n-n:(num)-n:case</ta>
            <ta e="T885" id="Seg_12744" s="T884">v-v:ptcp</ta>
            <ta e="T886" id="Seg_12745" s="T885">n-n:poss-n:case</ta>
            <ta e="T887" id="Seg_12746" s="T886">v-v:tense-v:pred.pn</ta>
            <ta e="T888" id="Seg_12747" s="T887">n-n:case</ta>
            <ta e="T889" id="Seg_12748" s="T888">n-n:case</ta>
            <ta e="T890" id="Seg_12749" s="T889">n-n:case</ta>
            <ta e="T891" id="Seg_12750" s="T890">v-v:tense-v:pred.pn</ta>
            <ta e="T892" id="Seg_12751" s="T891">interj</ta>
            <ta e="T893" id="Seg_12752" s="T892">que</ta>
            <ta e="T894" id="Seg_12753" s="T893">v-v:tense-v:pred.pn</ta>
            <ta e="T895" id="Seg_12754" s="T894">pers-pro:case</ta>
            <ta e="T896" id="Seg_12755" s="T895">n-n:case</ta>
            <ta e="T897" id="Seg_12756" s="T896">v-v:mood.pn</ta>
            <ta e="T898" id="Seg_12757" s="T897">n-n:case</ta>
            <ta e="T899" id="Seg_12758" s="T898">adv</ta>
            <ta e="T900" id="Seg_12759" s="T899">v-v:tense-v:pred.pn</ta>
            <ta e="T901" id="Seg_12760" s="T900">adj</ta>
            <ta e="T902" id="Seg_12761" s="T901">n-n:poss-n:case</ta>
            <ta e="T903" id="Seg_12762" s="T902">v-v:tense-v:poss.pn</ta>
            <ta e="T904" id="Seg_12763" s="T903">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T905" id="Seg_12764" s="T904">ptcl</ta>
            <ta e="T906" id="Seg_12765" s="T905">v-v:tense-v:pred.pn</ta>
            <ta e="T907" id="Seg_12766" s="T906">ptcl</ta>
            <ta e="T908" id="Seg_12767" s="T907">que-pro:case</ta>
            <ta e="T909" id="Seg_12768" s="T908">v-v:tense-v:pred.pn</ta>
            <ta e="T910" id="Seg_12769" s="T909">v-v:(ins)-v:mood.pn</ta>
            <ta e="T911" id="Seg_12770" s="T910">v-v:cvb</ta>
            <ta e="T912" id="Seg_12771" s="T911">v-v:tense-v:pred.pn</ta>
            <ta e="T913" id="Seg_12772" s="T912">dempro-pro:case</ta>
            <ta e="T914" id="Seg_12773" s="T913">v-v:neg-v:poss.pn</ta>
            <ta e="T915" id="Seg_12774" s="T914">cardnum</ta>
            <ta e="T916" id="Seg_12775" s="T915">n-n:(poss)-n:case</ta>
            <ta e="T917" id="Seg_12776" s="T916">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T918" id="Seg_12777" s="T917">n-n:(poss)-n:case</ta>
            <ta e="T919" id="Seg_12778" s="T918">adj-n:(poss)-n:case</ta>
            <ta e="T920" id="Seg_12779" s="T919">v-v:tense-v:pred.pn</ta>
            <ta e="T921" id="Seg_12780" s="T920">v-v:tense-v:poss.pn</ta>
            <ta e="T922" id="Seg_12781" s="T921">n-n:case</ta>
            <ta e="T923" id="Seg_12782" s="T922">v-v:tense-v:pred.pn</ta>
            <ta e="T924" id="Seg_12783" s="T923">n-n:case</ta>
            <ta e="T925" id="Seg_12784" s="T924">n-n:poss-n:case</ta>
            <ta e="T926" id="Seg_12785" s="T925">n-n&gt;n-n&gt;n-n:(poss)-n:case</ta>
            <ta e="T927" id="Seg_12786" s="T926">n-n:case</ta>
            <ta e="T928" id="Seg_12787" s="T927">post-n:case</ta>
            <ta e="T929" id="Seg_12788" s="T928">adj</ta>
            <ta e="T930" id="Seg_12789" s="T929">ptcl</ta>
            <ta e="T931" id="Seg_12790" s="T930">n-n:case</ta>
            <ta e="T932" id="Seg_12791" s="T931">v-v:tense-v:pred.pn</ta>
            <ta e="T933" id="Seg_12792" s="T932">n-n:case</ta>
            <ta e="T934" id="Seg_12793" s="T933">n-n:poss-n:case</ta>
            <ta e="T935" id="Seg_12794" s="T934">post</ta>
            <ta e="T936" id="Seg_12795" s="T935">v-v:tense-v:pred.pn</ta>
            <ta e="T937" id="Seg_12796" s="T936">n-n:case</ta>
            <ta e="T938" id="Seg_12797" s="T937">v-v:cvb</ta>
            <ta e="T939" id="Seg_12798" s="T938">v-v:tense-v:pred.pn</ta>
            <ta e="T940" id="Seg_12799" s="T939">adv</ta>
            <ta e="T941" id="Seg_12800" s="T940">n-n:case</ta>
            <ta e="T942" id="Seg_12801" s="T941">n-n:case</ta>
            <ta e="T943" id="Seg_12802" s="T942">n-n:case</ta>
            <ta e="T944" id="Seg_12803" s="T943">n-n:case</ta>
            <ta e="T945" id="Seg_12804" s="T944">n-n:case</ta>
            <ta e="T946" id="Seg_12805" s="T945">v-v:tense-v:pred.pn</ta>
            <ta e="T947" id="Seg_12806" s="T946">cardnum</ta>
            <ta e="T948" id="Seg_12807" s="T947">n-n:case</ta>
            <ta e="T949" id="Seg_12808" s="T948">n-n:poss-n:case</ta>
            <ta e="T950" id="Seg_12809" s="T949">v-v:tense-v:pred.pn</ta>
            <ta e="T951" id="Seg_12810" s="T950">cardnum</ta>
            <ta e="T952" id="Seg_12811" s="T951">adj</ta>
            <ta e="T953" id="Seg_12812" s="T952">ptcl</ta>
            <ta e="T954" id="Seg_12813" s="T953">n-n:case</ta>
            <ta e="T955" id="Seg_12814" s="T954">v-v:tense-v:pred.pn</ta>
            <ta e="T956" id="Seg_12815" s="T955">interj</ta>
            <ta e="T957" id="Seg_12816" s="T956">n-n:(poss)-n:case</ta>
            <ta e="T958" id="Seg_12817" s="T957">n-n:case</ta>
            <ta e="T959" id="Seg_12818" s="T958">n-n:case</ta>
            <ta e="T960" id="Seg_12819" s="T959">pers-pro:case</ta>
            <ta e="T961" id="Seg_12820" s="T960">n-n:case</ta>
            <ta e="T962" id="Seg_12821" s="T961">n-n:(poss)-n:case</ta>
            <ta e="T963" id="Seg_12822" s="T962">post-ptcl</ta>
            <ta e="T964" id="Seg_12823" s="T963">adv</ta>
            <ta e="T965" id="Seg_12824" s="T964">cardnum</ta>
            <ta e="T966" id="Seg_12825" s="T965">n-n:case</ta>
            <ta e="T967" id="Seg_12826" s="T966">v-v:tense-v:poss.pn</ta>
            <ta e="T968" id="Seg_12827" s="T967">dempro</ta>
            <ta e="T969" id="Seg_12828" s="T968">n-n:(poss)-n:case</ta>
            <ta e="T970" id="Seg_12829" s="T969">adv</ta>
            <ta e="T971" id="Seg_12830" s="T970">cardnum</ta>
            <ta e="T972" id="Seg_12831" s="T971">n-n:case</ta>
            <ta e="T973" id="Seg_12832" s="T972">cardnum</ta>
            <ta e="T974" id="Seg_12833" s="T973">n-n:case</ta>
            <ta e="T975" id="Seg_12834" s="T974">v-v:cvb</ta>
            <ta e="T976" id="Seg_12835" s="T975">ptcl</ta>
            <ta e="T977" id="Seg_12836" s="T976">v-v:tense-v:pred.pn</ta>
            <ta e="T978" id="Seg_12837" s="T977">n-n:poss-n:case</ta>
            <ta e="T979" id="Seg_12838" s="T978">v-v:cvb</ta>
            <ta e="T980" id="Seg_12839" s="T979">ptcl</ta>
            <ta e="T981" id="Seg_12840" s="T980">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T982" id="Seg_12841" s="T981">n-n:poss-n:case</ta>
            <ta e="T983" id="Seg_12842" s="T982">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T984" id="Seg_12843" s="T983">propr-n:case</ta>
            <ta e="T985" id="Seg_12844" s="T984">v-v:tense-v:pred.pn</ta>
            <ta e="T986" id="Seg_12845" s="T985">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T987" id="Seg_12846" s="T986">n-n:poss-n:case</ta>
            <ta e="T988" id="Seg_12847" s="T987">n-n:(poss)-n:case</ta>
            <ta e="T989" id="Seg_12848" s="T988">propr-n:case</ta>
            <ta e="T990" id="Seg_12849" s="T989">v-v:tense-v:pred.pn</ta>
            <ta e="T991" id="Seg_12850" s="T990">n-n:case</ta>
            <ta e="T992" id="Seg_12851" s="T991">n-n:(poss)-n:case</ta>
            <ta e="T993" id="Seg_12852" s="T992">adj-n:(poss)</ta>
            <ta e="T994" id="Seg_12853" s="T993">ptcl</ta>
            <ta e="T995" id="Seg_12854" s="T994">n-n:poss-n:case</ta>
            <ta e="T996" id="Seg_12855" s="T995">n-n:(poss)-n:case</ta>
            <ta e="T997" id="Seg_12856" s="T996">v-v:tense-v:pred.pn</ta>
            <ta e="T998" id="Seg_12857" s="T997">n-n:(poss)-n:case</ta>
            <ta e="T999" id="Seg_12858" s="T998">v-v:tense-v:pred.pn</ta>
            <ta e="T1000" id="Seg_12859" s="T999">ptcl</ta>
            <ta e="T1001" id="Seg_12860" s="T1000">adj-n:case</ta>
            <ta e="T1002" id="Seg_12861" s="T1001">adj</ta>
            <ta e="T1003" id="Seg_12862" s="T1002">n-n:case</ta>
            <ta e="T1004" id="Seg_12863" s="T1003">v-v:cvb</ta>
            <ta e="T1005" id="Seg_12864" s="T1004">n-n:case</ta>
            <ta e="T1006" id="Seg_12865" s="T1005">n-n:poss-n:case</ta>
            <ta e="T1007" id="Seg_12866" s="T1006">adv</ta>
            <ta e="T1008" id="Seg_12867" s="T1007">v-v:tense-v:poss.pn</ta>
            <ta e="T1009" id="Seg_12868" s="T1008">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T1010" id="Seg_12869" s="T1009">ptcl</ta>
            <ta e="T1011" id="Seg_12870" s="T1010">v-v:tense-v:poss.pn</ta>
            <ta e="T1012" id="Seg_12871" s="T1011">n-n:case</ta>
            <ta e="T1013" id="Seg_12872" s="T1012">v-v:cvb-v:pred.pn</ta>
            <ta e="T1014" id="Seg_12873" s="T1013">ptcl</ta>
            <ta e="T1015" id="Seg_12874" s="T1014">v-v:mood.pn</ta>
            <ta e="T1016" id="Seg_12875" s="T1015">que-pro:case</ta>
            <ta e="T1017" id="Seg_12876" s="T1016">ptcl</ta>
            <ta e="T1018" id="Seg_12877" s="T1017">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T1019" id="Seg_12878" s="T1018">propr-n:case</ta>
            <ta e="T1020" id="Seg_12879" s="T1019">ptcl</ta>
            <ta e="T1021" id="Seg_12880" s="T1020">adv</ta>
            <ta e="T1022" id="Seg_12881" s="T1021">v-v:tense-v:pred.pn</ta>
            <ta e="T1023" id="Seg_12882" s="T1022">n-n:case</ta>
            <ta e="T1024" id="Seg_12883" s="T1023">n-n:case</ta>
            <ta e="T1025" id="Seg_12884" s="T1024">v-v:cvb</ta>
            <ta e="T1026" id="Seg_12885" s="T1025">v-v:tense-v:poss.pn</ta>
            <ta e="T1027" id="Seg_12886" s="T1026">dempro</ta>
            <ta e="T1028" id="Seg_12887" s="T1027">n-n:case</ta>
            <ta e="T1029" id="Seg_12888" s="T1028">adj-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_12889" s="T0">cardnum</ta>
            <ta e="T2" id="Seg_12890" s="T1">n</ta>
            <ta e="T3" id="Seg_12891" s="T2">v</ta>
            <ta e="T4" id="Seg_12892" s="T3">cardnum</ta>
            <ta e="T5" id="Seg_12893" s="T4">adj</ta>
            <ta e="T6" id="Seg_12894" s="T5">adj</ta>
            <ta e="T7" id="Seg_12895" s="T6">n</ta>
            <ta e="T8" id="Seg_12896" s="T7">n</ta>
            <ta e="T9" id="Seg_12897" s="T8">v</ta>
            <ta e="T10" id="Seg_12898" s="T9">n</ta>
            <ta e="T11" id="Seg_12899" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_12900" s="T11">v</ta>
            <ta e="T13" id="Seg_12901" s="T12">propr</ta>
            <ta e="T14" id="Seg_12902" s="T13">v</ta>
            <ta e="T15" id="Seg_12903" s="T14">n</ta>
            <ta e="T16" id="Seg_12904" s="T15">n</ta>
            <ta e="T17" id="Seg_12905" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_12906" s="T17">n</ta>
            <ta e="T19" id="Seg_12907" s="T18">adj</ta>
            <ta e="T20" id="Seg_12908" s="T19">cardnum</ta>
            <ta e="T21" id="Seg_12909" s="T20">n</ta>
            <ta e="T22" id="Seg_12910" s="T21">adj</ta>
            <ta e="T23" id="Seg_12911" s="T22">n</ta>
            <ta e="T24" id="Seg_12912" s="T23">adj</ta>
            <ta e="T25" id="Seg_12913" s="T24">n</ta>
            <ta e="T26" id="Seg_12914" s="T25">post</ta>
            <ta e="T27" id="Seg_12915" s="T26">n</ta>
            <ta e="T28" id="Seg_12916" s="T27">v</ta>
            <ta e="T29" id="Seg_12917" s="T28">v</ta>
            <ta e="T30" id="Seg_12918" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_12919" s="T30">n</ta>
            <ta e="T32" id="Seg_12920" s="T31">v</ta>
            <ta e="T33" id="Seg_12921" s="T32">n</ta>
            <ta e="T34" id="Seg_12922" s="T33">adj</ta>
            <ta e="T35" id="Seg_12923" s="T34">n</ta>
            <ta e="T36" id="Seg_12924" s="T35">n</ta>
            <ta e="T37" id="Seg_12925" s="T36">v</ta>
            <ta e="T38" id="Seg_12926" s="T37">n</ta>
            <ta e="T39" id="Seg_12927" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_12928" s="T39">n</ta>
            <ta e="T41" id="Seg_12929" s="T40">v</ta>
            <ta e="T42" id="Seg_12930" s="T41">post</ta>
            <ta e="T43" id="Seg_12931" s="T42">n</ta>
            <ta e="T44" id="Seg_12932" s="T43">v</ta>
            <ta e="T45" id="Seg_12933" s="T44">v</ta>
            <ta e="T46" id="Seg_12934" s="T45">ordnum</ta>
            <ta e="T47" id="Seg_12935" s="T46">n</ta>
            <ta e="T48" id="Seg_12936" s="T47">adv</ta>
            <ta e="T49" id="Seg_12937" s="T48">v</ta>
            <ta e="T50" id="Seg_12938" s="T49">n</ta>
            <ta e="T51" id="Seg_12939" s="T50">v</ta>
            <ta e="T52" id="Seg_12940" s="T51">n</ta>
            <ta e="T53" id="Seg_12941" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_12942" s="T53">pers</ta>
            <ta e="T55" id="Seg_12943" s="T54">v</ta>
            <ta e="T56" id="Seg_12944" s="T55">que</ta>
            <ta e="T57" id="Seg_12945" s="T56">v</ta>
            <ta e="T58" id="Seg_12946" s="T57">v</ta>
            <ta e="T59" id="Seg_12947" s="T58">n</ta>
            <ta e="T60" id="Seg_12948" s="T59">n</ta>
            <ta e="T61" id="Seg_12949" s="T60">post</ta>
            <ta e="T62" id="Seg_12950" s="T61">v</ta>
            <ta e="T63" id="Seg_12951" s="T62">n</ta>
            <ta e="T64" id="Seg_12952" s="T63">v</ta>
            <ta e="T65" id="Seg_12953" s="T64">n</ta>
            <ta e="T66" id="Seg_12954" s="T65">n</ta>
            <ta e="T67" id="Seg_12955" s="T66">n</ta>
            <ta e="T68" id="Seg_12956" s="T67">v</ta>
            <ta e="T69" id="Seg_12957" s="T68">v</ta>
            <ta e="T70" id="Seg_12958" s="T69">aux</ta>
            <ta e="T71" id="Seg_12959" s="T70">dempro</ta>
            <ta e="T72" id="Seg_12960" s="T71">n</ta>
            <ta e="T73" id="Seg_12961" s="T72">n</ta>
            <ta e="T74" id="Seg_12962" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_12963" s="T74">n</ta>
            <ta e="T76" id="Seg_12964" s="T75">v</ta>
            <ta e="T77" id="Seg_12965" s="T76">v</ta>
            <ta e="T78" id="Seg_12966" s="T77">n</ta>
            <ta e="T79" id="Seg_12967" s="T78">adv</ta>
            <ta e="T80" id="Seg_12968" s="T79">n</ta>
            <ta e="T81" id="Seg_12969" s="T80">cop</ta>
            <ta e="T82" id="Seg_12970" s="T81">v</ta>
            <ta e="T83" id="Seg_12971" s="T82">pers</ta>
            <ta e="T84" id="Seg_12972" s="T83">v</ta>
            <ta e="T85" id="Seg_12973" s="T84">adj</ta>
            <ta e="T86" id="Seg_12974" s="T85">v</ta>
            <ta e="T87" id="Seg_12975" s="T86">aux</ta>
            <ta e="T88" id="Seg_12976" s="T87">v</ta>
            <ta e="T89" id="Seg_12977" s="T88">n</ta>
            <ta e="T90" id="Seg_12978" s="T89">v</ta>
            <ta e="T91" id="Seg_12979" s="T90">aux</ta>
            <ta e="T92" id="Seg_12980" s="T91">dempro</ta>
            <ta e="T93" id="Seg_12981" s="T92">n</ta>
            <ta e="T94" id="Seg_12982" s="T93">cop</ta>
            <ta e="T95" id="Seg_12983" s="T94">n</ta>
            <ta e="T96" id="Seg_12984" s="T95">v</ta>
            <ta e="T97" id="Seg_12985" s="T96">n</ta>
            <ta e="T98" id="Seg_12986" s="T97">v</ta>
            <ta e="T99" id="Seg_12987" s="T98">n</ta>
            <ta e="T100" id="Seg_12988" s="T99">n</ta>
            <ta e="T101" id="Seg_12989" s="T100">n</ta>
            <ta e="T102" id="Seg_12990" s="T101">v</ta>
            <ta e="T103" id="Seg_12991" s="T102">adj</ta>
            <ta e="T104" id="Seg_12992" s="T103">v</ta>
            <ta e="T105" id="Seg_12993" s="T104">n</ta>
            <ta e="T106" id="Seg_12994" s="T105">cop</ta>
            <ta e="T107" id="Seg_12995" s="T106">v</ta>
            <ta e="T108" id="Seg_12996" s="T107">n</ta>
            <ta e="T109" id="Seg_12997" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_12998" s="T109">n</ta>
            <ta e="T111" id="Seg_12999" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_13000" s="T111">n</ta>
            <ta e="T113" id="Seg_13001" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_13002" s="T113">adj</ta>
            <ta e="T115" id="Seg_13003" s="T114">n</ta>
            <ta e="T116" id="Seg_13004" s="T115">v</ta>
            <ta e="T117" id="Seg_13005" s="T116">n</ta>
            <ta e="T118" id="Seg_13006" s="T117">v</ta>
            <ta e="T119" id="Seg_13007" s="T118">v</ta>
            <ta e="T120" id="Seg_13008" s="T119">n</ta>
            <ta e="T121" id="Seg_13009" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_13010" s="T121">v</ta>
            <ta e="T123" id="Seg_13011" s="T122">n</ta>
            <ta e="T124" id="Seg_13012" s="T123">v</ta>
            <ta e="T125" id="Seg_13013" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_13014" s="T125">n</ta>
            <ta e="T127" id="Seg_13015" s="T126">n</ta>
            <ta e="T128" id="Seg_13016" s="T127">cardnum</ta>
            <ta e="T129" id="Seg_13017" s="T128">n</ta>
            <ta e="T130" id="Seg_13018" s="T129">n</ta>
            <ta e="T131" id="Seg_13019" s="T130">n</ta>
            <ta e="T132" id="Seg_13020" s="T131">n</ta>
            <ta e="T133" id="Seg_13021" s="T132">cop</ta>
            <ta e="T134" id="Seg_13022" s="T133">interj</ta>
            <ta e="T135" id="Seg_13023" s="T134">que</ta>
            <ta e="T136" id="Seg_13024" s="T135">v</ta>
            <ta e="T137" id="Seg_13025" s="T136">n</ta>
            <ta e="T138" id="Seg_13026" s="T137">v</ta>
            <ta e="T139" id="Seg_13027" s="T138">n</ta>
            <ta e="T140" id="Seg_13028" s="T139">interj</ta>
            <ta e="T141" id="Seg_13029" s="T140">interj</ta>
            <ta e="T142" id="Seg_13030" s="T141">adv</ta>
            <ta e="T143" id="Seg_13031" s="T142">v</ta>
            <ta e="T144" id="Seg_13032" s="T143">v</ta>
            <ta e="T145" id="Seg_13033" s="T144">v</ta>
            <ta e="T146" id="Seg_13034" s="T145">pers</ta>
            <ta e="T147" id="Seg_13035" s="T146">n</ta>
            <ta e="T148" id="Seg_13036" s="T147">interj</ta>
            <ta e="T149" id="Seg_13037" s="T148">n</ta>
            <ta e="T150" id="Seg_13038" s="T149">n</ta>
            <ta e="T151" id="Seg_13039" s="T150">v</ta>
            <ta e="T152" id="Seg_13040" s="T151">v</ta>
            <ta e="T153" id="Seg_13041" s="T152">v</ta>
            <ta e="T154" id="Seg_13042" s="T153">n</ta>
            <ta e="T155" id="Seg_13043" s="T154">v</ta>
            <ta e="T156" id="Seg_13044" s="T155">v</ta>
            <ta e="T157" id="Seg_13045" s="T156">v</ta>
            <ta e="T158" id="Seg_13046" s="T157">conj</ta>
            <ta e="T159" id="Seg_13047" s="T158">v</ta>
            <ta e="T160" id="Seg_13048" s="T159">aux</ta>
            <ta e="T161" id="Seg_13049" s="T160">adj</ta>
            <ta e="T162" id="Seg_13050" s="T161">adv</ta>
            <ta e="T163" id="Seg_13051" s="T162">adj</ta>
            <ta e="T164" id="Seg_13052" s="T163">v</ta>
            <ta e="T165" id="Seg_13053" s="T164">n</ta>
            <ta e="T166" id="Seg_13054" s="T165">v</ta>
            <ta e="T167" id="Seg_13055" s="T166">v</ta>
            <ta e="T168" id="Seg_13056" s="T167">v</ta>
            <ta e="T169" id="Seg_13057" s="T168">propr</ta>
            <ta e="T170" id="Seg_13058" s="T169">n</ta>
            <ta e="T171" id="Seg_13059" s="T170">v</ta>
            <ta e="T172" id="Seg_13060" s="T171">n</ta>
            <ta e="T173" id="Seg_13061" s="T172">v</ta>
            <ta e="T174" id="Seg_13062" s="T173">v</ta>
            <ta e="T175" id="Seg_13063" s="T174">v</ta>
            <ta e="T176" id="Seg_13064" s="T175">cardnum</ta>
            <ta e="T177" id="Seg_13065" s="T176">n</ta>
            <ta e="T178" id="Seg_13066" s="T177">v</ta>
            <ta e="T179" id="Seg_13067" s="T178">n</ta>
            <ta e="T180" id="Seg_13068" s="T179">v</ta>
            <ta e="T181" id="Seg_13069" s="T180">n</ta>
            <ta e="T182" id="Seg_13070" s="T181">v</ta>
            <ta e="T183" id="Seg_13071" s="T182">v</ta>
            <ta e="T184" id="Seg_13072" s="T183">aux</ta>
            <ta e="T185" id="Seg_13073" s="T184">v</ta>
            <ta e="T186" id="Seg_13074" s="T185">que</ta>
            <ta e="T187" id="Seg_13075" s="T186">adv</ta>
            <ta e="T188" id="Seg_13076" s="T187">v</ta>
            <ta e="T189" id="Seg_13077" s="T188">adv</ta>
            <ta e="T190" id="Seg_13078" s="T189">adj</ta>
            <ta e="T191" id="Seg_13079" s="T190">n</ta>
            <ta e="T192" id="Seg_13080" s="T191">v</ta>
            <ta e="T193" id="Seg_13081" s="T192">v</ta>
            <ta e="T194" id="Seg_13082" s="T193">adj</ta>
            <ta e="T195" id="Seg_13083" s="T194">n</ta>
            <ta e="T196" id="Seg_13084" s="T195">n</ta>
            <ta e="T197" id="Seg_13085" s="T196">n</ta>
            <ta e="T198" id="Seg_13086" s="T197">n</ta>
            <ta e="T199" id="Seg_13087" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_13088" s="T199">n</ta>
            <ta e="T201" id="Seg_13089" s="T200">adv</ta>
            <ta e="T202" id="Seg_13090" s="T201">cardnum</ta>
            <ta e="T203" id="Seg_13091" s="T202">cardnum</ta>
            <ta e="T204" id="Seg_13092" s="T203">n</ta>
            <ta e="T205" id="Seg_13093" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_13094" s="T205">v</ta>
            <ta e="T207" id="Seg_13095" s="T206">v</ta>
            <ta e="T208" id="Seg_13096" s="T207">n</ta>
            <ta e="T209" id="Seg_13097" s="T208">n</ta>
            <ta e="T210" id="Seg_13098" s="T209">n</ta>
            <ta e="T211" id="Seg_13099" s="T210">adj</ta>
            <ta e="T212" id="Seg_13100" s="T211">n</ta>
            <ta e="T213" id="Seg_13101" s="T212">v</ta>
            <ta e="T214" id="Seg_13102" s="T213">v</ta>
            <ta e="T215" id="Seg_13103" s="T214">propr</ta>
            <ta e="T216" id="Seg_13104" s="T215">dempro</ta>
            <ta e="T217" id="Seg_13105" s="T216">n</ta>
            <ta e="T218" id="Seg_13106" s="T217">cardnum</ta>
            <ta e="T219" id="Seg_13107" s="T218">n</ta>
            <ta e="T220" id="Seg_13108" s="T219">v</ta>
            <ta e="T221" id="Seg_13109" s="T220">n</ta>
            <ta e="T222" id="Seg_13110" s="T221">n</ta>
            <ta e="T223" id="Seg_13111" s="T222">n</ta>
            <ta e="T224" id="Seg_13112" s="T223">v</ta>
            <ta e="T225" id="Seg_13113" s="T224">cardnum</ta>
            <ta e="T226" id="Seg_13114" s="T225">n</ta>
            <ta e="T227" id="Seg_13115" s="T226">adj</ta>
            <ta e="T228" id="Seg_13116" s="T227">adj</ta>
            <ta e="T229" id="Seg_13117" s="T228">n</ta>
            <ta e="T230" id="Seg_13118" s="T229">n</ta>
            <ta e="T231" id="Seg_13119" s="T230">v</ta>
            <ta e="T232" id="Seg_13120" s="T231">conj</ta>
            <ta e="T233" id="Seg_13121" s="T232">n</ta>
            <ta e="T234" id="Seg_13122" s="T233">v</ta>
            <ta e="T235" id="Seg_13123" s="T234">aux</ta>
            <ta e="T236" id="Seg_13124" s="T235">n</ta>
            <ta e="T237" id="Seg_13125" s="T236">v</ta>
            <ta e="T238" id="Seg_13126" s="T237">aux</ta>
            <ta e="T239" id="Seg_13127" s="T238">v</ta>
            <ta e="T240" id="Seg_13128" s="T239">n</ta>
            <ta e="T241" id="Seg_13129" s="T240">v</ta>
            <ta e="T242" id="Seg_13130" s="T241">n</ta>
            <ta e="T243" id="Seg_13131" s="T242">n</ta>
            <ta e="T244" id="Seg_13132" s="T243">v</ta>
            <ta e="T245" id="Seg_13133" s="T244">v</ta>
            <ta e="T246" id="Seg_13134" s="T245">cardnum</ta>
            <ta e="T247" id="Seg_13135" s="T246">n</ta>
            <ta e="T248" id="Seg_13136" s="T247">v</ta>
            <ta e="T249" id="Seg_13137" s="T248">v</ta>
            <ta e="T250" id="Seg_13138" s="T249">cardnum</ta>
            <ta e="T251" id="Seg_13139" s="T250">ptcl</ta>
            <ta e="T252" id="Seg_13140" s="T251">n</ta>
            <ta e="T253" id="Seg_13141" s="T252">n</ta>
            <ta e="T254" id="Seg_13142" s="T253">post</ta>
            <ta e="T255" id="Seg_13143" s="T254">n</ta>
            <ta e="T256" id="Seg_13144" s="T255">n</ta>
            <ta e="T257" id="Seg_13145" s="T256">v</ta>
            <ta e="T258" id="Seg_13146" s="T257">v</ta>
            <ta e="T259" id="Seg_13147" s="T258">n</ta>
            <ta e="T260" id="Seg_13148" s="T259">v</ta>
            <ta e="T261" id="Seg_13149" s="T260">v</ta>
            <ta e="T262" id="Seg_13150" s="T261">v</ta>
            <ta e="T263" id="Seg_13151" s="T262">v</ta>
            <ta e="T264" id="Seg_13152" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_13153" s="T264">v</ta>
            <ta e="T266" id="Seg_13154" s="T265">n</ta>
            <ta e="T267" id="Seg_13155" s="T266">v</ta>
            <ta e="T268" id="Seg_13156" s="T267">v</ta>
            <ta e="T269" id="Seg_13157" s="T268">v</ta>
            <ta e="T270" id="Seg_13158" s="T269">n</ta>
            <ta e="T271" id="Seg_13159" s="T270">interj</ta>
            <ta e="T272" id="Seg_13160" s="T271">n</ta>
            <ta e="T273" id="Seg_13161" s="T272">v</ta>
            <ta e="T274" id="Seg_13162" s="T273">n</ta>
            <ta e="T275" id="Seg_13163" s="T274">v</ta>
            <ta e="T276" id="Seg_13164" s="T275">cardnum</ta>
            <ta e="T277" id="Seg_13165" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_13166" s="T277">n</ta>
            <ta e="T279" id="Seg_13167" s="T278">n</ta>
            <ta e="T280" id="Seg_13168" s="T279">post</ta>
            <ta e="T281" id="Seg_13169" s="T280">n</ta>
            <ta e="T282" id="Seg_13170" s="T281">n</ta>
            <ta e="T283" id="Seg_13171" s="T282">v</ta>
            <ta e="T284" id="Seg_13172" s="T283">interj</ta>
            <ta e="T285" id="Seg_13173" s="T284">v</ta>
            <ta e="T286" id="Seg_13174" s="T285">v</ta>
            <ta e="T287" id="Seg_13175" s="T286">v</ta>
            <ta e="T288" id="Seg_13176" s="T287">v</ta>
            <ta e="T289" id="Seg_13177" s="T288">cardnum</ta>
            <ta e="T290" id="Seg_13178" s="T289">n</ta>
            <ta e="T291" id="Seg_13179" s="T290">n</ta>
            <ta e="T292" id="Seg_13180" s="T291">v</ta>
            <ta e="T293" id="Seg_13181" s="T292">que</ta>
            <ta e="T294" id="Seg_13182" s="T293">v</ta>
            <ta e="T295" id="Seg_13183" s="T294">adv</ta>
            <ta e="T296" id="Seg_13184" s="T295">v</ta>
            <ta e="T297" id="Seg_13185" s="T296">n</ta>
            <ta e="T298" id="Seg_13186" s="T297">v</ta>
            <ta e="T299" id="Seg_13187" s="T298">propr</ta>
            <ta e="T300" id="Seg_13188" s="T299">v</ta>
            <ta e="T301" id="Seg_13189" s="T300">v</ta>
            <ta e="T302" id="Seg_13190" s="T301">aux</ta>
            <ta e="T303" id="Seg_13191" s="T302">n</ta>
            <ta e="T304" id="Seg_13192" s="T303">n</ta>
            <ta e="T305" id="Seg_13193" s="T304">ptcl</ta>
            <ta e="T306" id="Seg_13194" s="T305">v</ta>
            <ta e="T307" id="Seg_13195" s="T306">adj</ta>
            <ta e="T308" id="Seg_13196" s="T307">n</ta>
            <ta e="T309" id="Seg_13197" s="T308">adj</ta>
            <ta e="T310" id="Seg_13198" s="T309">ptcl</ta>
            <ta e="T311" id="Seg_13199" s="T310">v</ta>
            <ta e="T312" id="Seg_13200" s="T311">n</ta>
            <ta e="T313" id="Seg_13201" s="T312">v</ta>
            <ta e="T314" id="Seg_13202" s="T313">aux</ta>
            <ta e="T315" id="Seg_13203" s="T314">v</ta>
            <ta e="T316" id="Seg_13204" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_13205" s="T316">n</ta>
            <ta e="T318" id="Seg_13206" s="T317">v</ta>
            <ta e="T319" id="Seg_13207" s="T318">n</ta>
            <ta e="T320" id="Seg_13208" s="T319">v</ta>
            <ta e="T321" id="Seg_13209" s="T320">aux</ta>
            <ta e="T322" id="Seg_13210" s="T321">v</ta>
            <ta e="T323" id="Seg_13211" s="T322">ptcl</ta>
            <ta e="T324" id="Seg_13212" s="T323">v</ta>
            <ta e="T325" id="Seg_13213" s="T324">v</ta>
            <ta e="T326" id="Seg_13214" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_13215" s="T326">v</ta>
            <ta e="T328" id="Seg_13216" s="T327">n</ta>
            <ta e="T329" id="Seg_13217" s="T328">post</ta>
            <ta e="T330" id="Seg_13218" s="T329">n</ta>
            <ta e="T331" id="Seg_13219" s="T330">post</ta>
            <ta e="T332" id="Seg_13220" s="T331">dempro</ta>
            <ta e="T333" id="Seg_13221" s="T332">n</ta>
            <ta e="T334" id="Seg_13222" s="T333">n</ta>
            <ta e="T335" id="Seg_13223" s="T334">adj</ta>
            <ta e="T336" id="Seg_13224" s="T335">n</ta>
            <ta e="T337" id="Seg_13225" s="T336">v</ta>
            <ta e="T338" id="Seg_13226" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_13227" s="T338">cardnum</ta>
            <ta e="T340" id="Seg_13228" s="T339">n</ta>
            <ta e="T341" id="Seg_13229" s="T340">n</ta>
            <ta e="T342" id="Seg_13230" s="T341">v</ta>
            <ta e="T343" id="Seg_13231" s="T342">dempro</ta>
            <ta e="T344" id="Seg_13232" s="T343">n</ta>
            <ta e="T345" id="Seg_13233" s="T344">n</ta>
            <ta e="T346" id="Seg_13234" s="T345">adv</ta>
            <ta e="T347" id="Seg_13235" s="T346">n</ta>
            <ta e="T348" id="Seg_13236" s="T347">v</ta>
            <ta e="T349" id="Seg_13237" s="T348">n</ta>
            <ta e="T350" id="Seg_13238" s="T349">v</ta>
            <ta e="T351" id="Seg_13239" s="T350">cardnum</ta>
            <ta e="T352" id="Seg_13240" s="T351">n</ta>
            <ta e="T353" id="Seg_13241" s="T352">n</ta>
            <ta e="T354" id="Seg_13242" s="T353">v</ta>
            <ta e="T355" id="Seg_13243" s="T354">n</ta>
            <ta e="T356" id="Seg_13244" s="T355">cardnum</ta>
            <ta e="T357" id="Seg_13245" s="T356">n</ta>
            <ta e="T358" id="Seg_13246" s="T357">n</ta>
            <ta e="T359" id="Seg_13247" s="T358">v</ta>
            <ta e="T360" id="Seg_13248" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_13249" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_13250" s="T361">interj</ta>
            <ta e="T363" id="Seg_13251" s="T362">adv</ta>
            <ta e="T364" id="Seg_13252" s="T363">pers</ta>
            <ta e="T365" id="Seg_13253" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_13254" s="T365">v</ta>
            <ta e="T367" id="Seg_13255" s="T366">v</ta>
            <ta e="T368" id="Seg_13256" s="T367">adv</ta>
            <ta e="T369" id="Seg_13257" s="T368">n</ta>
            <ta e="T370" id="Seg_13258" s="T369">v</ta>
            <ta e="T371" id="Seg_13259" s="T370">ptcl</ta>
            <ta e="T372" id="Seg_13260" s="T371">n</ta>
            <ta e="T373" id="Seg_13261" s="T372">v</ta>
            <ta e="T374" id="Seg_13262" s="T373">v</ta>
            <ta e="T375" id="Seg_13263" s="T374">propr</ta>
            <ta e="T376" id="Seg_13264" s="T375">propr</ta>
            <ta e="T377" id="Seg_13265" s="T376">v</ta>
            <ta e="T378" id="Seg_13266" s="T377">n</ta>
            <ta e="T379" id="Seg_13267" s="T378">v</ta>
            <ta e="T380" id="Seg_13268" s="T379">aux</ta>
            <ta e="T381" id="Seg_13269" s="T380">n</ta>
            <ta e="T382" id="Seg_13270" s="T381">interj</ta>
            <ta e="T383" id="Seg_13271" s="T382">n</ta>
            <ta e="T384" id="Seg_13272" s="T383">v</ta>
            <ta e="T385" id="Seg_13273" s="T384">v</ta>
            <ta e="T386" id="Seg_13274" s="T385">aux</ta>
            <ta e="T387" id="Seg_13275" s="T386">ptcl</ta>
            <ta e="T388" id="Seg_13276" s="T387">v</ta>
            <ta e="T389" id="Seg_13277" s="T388">v</ta>
            <ta e="T390" id="Seg_13278" s="T389">que</ta>
            <ta e="T391" id="Seg_13279" s="T390">dempro</ta>
            <ta e="T392" id="Seg_13280" s="T391">dempro</ta>
            <ta e="T393" id="Seg_13281" s="T392">n</ta>
            <ta e="T394" id="Seg_13282" s="T393">adj</ta>
            <ta e="T395" id="Seg_13283" s="T394">v</ta>
            <ta e="T396" id="Seg_13284" s="T395">aux</ta>
            <ta e="T397" id="Seg_13285" s="T396">v</ta>
            <ta e="T398" id="Seg_13286" s="T397">adv</ta>
            <ta e="T399" id="Seg_13287" s="T398">propr</ta>
            <ta e="T400" id="Seg_13288" s="T399">v</ta>
            <ta e="T401" id="Seg_13289" s="T400">cardnum</ta>
            <ta e="T402" id="Seg_13290" s="T401">n</ta>
            <ta e="T403" id="Seg_13291" s="T402">n</ta>
            <ta e="T404" id="Seg_13292" s="T403">n</ta>
            <ta e="T405" id="Seg_13293" s="T404">v</ta>
            <ta e="T406" id="Seg_13294" s="T405">v</ta>
            <ta e="T407" id="Seg_13295" s="T406">interj</ta>
            <ta e="T408" id="Seg_13296" s="T407">cardnum</ta>
            <ta e="T409" id="Seg_13297" s="T408">n</ta>
            <ta e="T410" id="Seg_13298" s="T409">n</ta>
            <ta e="T411" id="Seg_13299" s="T410">interj</ta>
            <ta e="T412" id="Seg_13300" s="T411">adv</ta>
            <ta e="T413" id="Seg_13301" s="T412">interj</ta>
            <ta e="T414" id="Seg_13302" s="T413">adv</ta>
            <ta e="T415" id="Seg_13303" s="T414">v</ta>
            <ta e="T416" id="Seg_13304" s="T415">adj</ta>
            <ta e="T417" id="Seg_13305" s="T416">n</ta>
            <ta e="T418" id="Seg_13306" s="T417">v</ta>
            <ta e="T419" id="Seg_13307" s="T418">aux</ta>
            <ta e="T420" id="Seg_13308" s="T419">emphpro</ta>
            <ta e="T421" id="Seg_13309" s="T420">n</ta>
            <ta e="T422" id="Seg_13310" s="T421">cardnum</ta>
            <ta e="T423" id="Seg_13311" s="T422">n</ta>
            <ta e="T424" id="Seg_13312" s="T423">n</ta>
            <ta e="T425" id="Seg_13313" s="T424">v</ta>
            <ta e="T426" id="Seg_13314" s="T425">v</ta>
            <ta e="T427" id="Seg_13315" s="T426">n</ta>
            <ta e="T428" id="Seg_13316" s="T427">n</ta>
            <ta e="T429" id="Seg_13317" s="T428">n</ta>
            <ta e="T430" id="Seg_13318" s="T429">n</ta>
            <ta e="T431" id="Seg_13319" s="T430">v</ta>
            <ta e="T432" id="Seg_13320" s="T431">conj</ta>
            <ta e="T433" id="Seg_13321" s="T432">v</ta>
            <ta e="T434" id="Seg_13322" s="T433">aux</ta>
            <ta e="T435" id="Seg_13323" s="T434">propr</ta>
            <ta e="T436" id="Seg_13324" s="T435">n</ta>
            <ta e="T437" id="Seg_13325" s="T436">n</ta>
            <ta e="T438" id="Seg_13326" s="T437">v</ta>
            <ta e="T439" id="Seg_13327" s="T438">cardnum</ta>
            <ta e="T440" id="Seg_13328" s="T439">n</ta>
            <ta e="T441" id="Seg_13329" s="T440">n</ta>
            <ta e="T442" id="Seg_13330" s="T441">n</ta>
            <ta e="T443" id="Seg_13331" s="T442">v</ta>
            <ta e="T444" id="Seg_13332" s="T443">v</ta>
            <ta e="T445" id="Seg_13333" s="T444">v</ta>
            <ta e="T446" id="Seg_13334" s="T445">interj</ta>
            <ta e="T447" id="Seg_13335" s="T446">cardnum</ta>
            <ta e="T448" id="Seg_13336" s="T447">n</ta>
            <ta e="T449" id="Seg_13337" s="T448">n</ta>
            <ta e="T450" id="Seg_13338" s="T449">interj</ta>
            <ta e="T451" id="Seg_13339" s="T450">adv</ta>
            <ta e="T452" id="Seg_13340" s="T451">interj</ta>
            <ta e="T453" id="Seg_13341" s="T452">adv</ta>
            <ta e="T454" id="Seg_13342" s="T453">v</ta>
            <ta e="T455" id="Seg_13343" s="T454">adj</ta>
            <ta e="T456" id="Seg_13344" s="T455">n</ta>
            <ta e="T457" id="Seg_13345" s="T456">v</ta>
            <ta e="T458" id="Seg_13346" s="T457">aux</ta>
            <ta e="T459" id="Seg_13347" s="T458">emphpro</ta>
            <ta e="T460" id="Seg_13348" s="T459">n</ta>
            <ta e="T461" id="Seg_13349" s="T460">cardnum</ta>
            <ta e="T462" id="Seg_13350" s="T461">n</ta>
            <ta e="T463" id="Seg_13351" s="T462">n</ta>
            <ta e="T464" id="Seg_13352" s="T463">v</ta>
            <ta e="T465" id="Seg_13353" s="T464">v</ta>
            <ta e="T466" id="Seg_13354" s="T465">n</ta>
            <ta e="T467" id="Seg_13355" s="T466">n</ta>
            <ta e="T468" id="Seg_13356" s="T467">n</ta>
            <ta e="T469" id="Seg_13357" s="T468">n</ta>
            <ta e="T470" id="Seg_13358" s="T469">v</ta>
            <ta e="T471" id="Seg_13359" s="T470">n</ta>
            <ta e="T472" id="Seg_13360" s="T471">v</ta>
            <ta e="T473" id="Seg_13361" s="T472">que</ta>
            <ta e="T474" id="Seg_13362" s="T473">ptcl</ta>
            <ta e="T475" id="Seg_13363" s="T474">propr</ta>
            <ta e="T476" id="Seg_13364" s="T475">adj</ta>
            <ta e="T477" id="Seg_13365" s="T476">n</ta>
            <ta e="T478" id="Seg_13366" s="T477">v</ta>
            <ta e="T479" id="Seg_13367" s="T478">v</ta>
            <ta e="T480" id="Seg_13368" s="T479">conj</ta>
            <ta e="T481" id="Seg_13369" s="T480">cardnum</ta>
            <ta e="T482" id="Seg_13370" s="T481">n</ta>
            <ta e="T483" id="Seg_13371" s="T482">v</ta>
            <ta e="T484" id="Seg_13372" s="T483">v</ta>
            <ta e="T485" id="Seg_13373" s="T484">v</ta>
            <ta e="T486" id="Seg_13374" s="T485">n</ta>
            <ta e="T487" id="Seg_13375" s="T486">v</ta>
            <ta e="T488" id="Seg_13376" s="T487">aux</ta>
            <ta e="T489" id="Seg_13377" s="T488">n</ta>
            <ta e="T490" id="Seg_13378" s="T489">v</ta>
            <ta e="T491" id="Seg_13379" s="T490">aux</ta>
            <ta e="T492" id="Seg_13380" s="T491">n</ta>
            <ta e="T493" id="Seg_13381" s="T492">n</ta>
            <ta e="T494" id="Seg_13382" s="T493">v</ta>
            <ta e="T495" id="Seg_13383" s="T494">propr</ta>
            <ta e="T496" id="Seg_13384" s="T495">n</ta>
            <ta e="T497" id="Seg_13385" s="T496">n</ta>
            <ta e="T498" id="Seg_13386" s="T497">v</ta>
            <ta e="T499" id="Seg_13387" s="T498">aux</ta>
            <ta e="T500" id="Seg_13388" s="T499">v</ta>
            <ta e="T501" id="Seg_13389" s="T500">que</ta>
            <ta e="T502" id="Seg_13390" s="T501">post</ta>
            <ta e="T503" id="Seg_13391" s="T502">v</ta>
            <ta e="T504" id="Seg_13392" s="T503">n</ta>
            <ta e="T505" id="Seg_13393" s="T504">v</ta>
            <ta e="T506" id="Seg_13394" s="T505">aux</ta>
            <ta e="T507" id="Seg_13395" s="T506">propr</ta>
            <ta e="T508" id="Seg_13396" s="T507">v</ta>
            <ta e="T509" id="Seg_13397" s="T508">v</ta>
            <ta e="T510" id="Seg_13398" s="T509">n</ta>
            <ta e="T511" id="Seg_13399" s="T510">v</ta>
            <ta e="T512" id="Seg_13400" s="T511">aux</ta>
            <ta e="T513" id="Seg_13401" s="T512">propr</ta>
            <ta e="T514" id="Seg_13402" s="T513">cardnum</ta>
            <ta e="T515" id="Seg_13403" s="T514">n</ta>
            <ta e="T516" id="Seg_13404" s="T515">n</ta>
            <ta e="T517" id="Seg_13405" s="T516">v</ta>
            <ta e="T518" id="Seg_13406" s="T517">ptcl</ta>
            <ta e="T519" id="Seg_13407" s="T518">cardnum</ta>
            <ta e="T520" id="Seg_13408" s="T519">n</ta>
            <ta e="T521" id="Seg_13409" s="T520">n</ta>
            <ta e="T522" id="Seg_13410" s="T521">adj</ta>
            <ta e="T523" id="Seg_13411" s="T522">cop</ta>
            <ta e="T524" id="Seg_13412" s="T523">n</ta>
            <ta e="T525" id="Seg_13413" s="T524">v</ta>
            <ta e="T526" id="Seg_13414" s="T525">aux</ta>
            <ta e="T527" id="Seg_13415" s="T526">adv</ta>
            <ta e="T528" id="Seg_13416" s="T527">v</ta>
            <ta e="T529" id="Seg_13417" s="T528">cardnum</ta>
            <ta e="T530" id="Seg_13418" s="T529">n</ta>
            <ta e="T531" id="Seg_13419" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_13420" s="T531">v</ta>
            <ta e="T533" id="Seg_13421" s="T532">n</ta>
            <ta e="T534" id="Seg_13422" s="T533">v</ta>
            <ta e="T535" id="Seg_13423" s="T534">v</ta>
            <ta e="T536" id="Seg_13424" s="T535">n</ta>
            <ta e="T537" id="Seg_13425" s="T536">v</ta>
            <ta e="T538" id="Seg_13426" s="T537">v</ta>
            <ta e="T539" id="Seg_13427" s="T538">aux</ta>
            <ta e="T540" id="Seg_13428" s="T539">que</ta>
            <ta e="T541" id="Seg_13429" s="T540">n</ta>
            <ta e="T542" id="Seg_13430" s="T541">n</ta>
            <ta e="T543" id="Seg_13431" s="T542">v</ta>
            <ta e="T544" id="Seg_13432" s="T543">v</ta>
            <ta e="T545" id="Seg_13433" s="T544">v</ta>
            <ta e="T546" id="Seg_13434" s="T545">v</ta>
            <ta e="T547" id="Seg_13435" s="T546">v</ta>
            <ta e="T548" id="Seg_13436" s="T547">n</ta>
            <ta e="T549" id="Seg_13437" s="T548">post</ta>
            <ta e="T550" id="Seg_13438" s="T549">v</ta>
            <ta e="T551" id="Seg_13439" s="T550">v</ta>
            <ta e="T552" id="Seg_13440" s="T551">conj</ta>
            <ta e="T553" id="Seg_13441" s="T552">post</ta>
            <ta e="T554" id="Seg_13442" s="T553">v</ta>
            <ta e="T555" id="Seg_13443" s="T554">n</ta>
            <ta e="T556" id="Seg_13444" s="T555">n</ta>
            <ta e="T557" id="Seg_13445" s="T556">v</ta>
            <ta e="T558" id="Seg_13446" s="T557">n</ta>
            <ta e="T559" id="Seg_13447" s="T558">n</ta>
            <ta e="T560" id="Seg_13448" s="T559">v</ta>
            <ta e="T561" id="Seg_13449" s="T560">v</ta>
            <ta e="T562" id="Seg_13450" s="T561">n</ta>
            <ta e="T563" id="Seg_13451" s="T562">v</ta>
            <ta e="T564" id="Seg_13452" s="T563">v</ta>
            <ta e="T565" id="Seg_13453" s="T564">ptcl</ta>
            <ta e="T566" id="Seg_13454" s="T565">n</ta>
            <ta e="T567" id="Seg_13455" s="T566">n</ta>
            <ta e="T568" id="Seg_13456" s="T567">v</ta>
            <ta e="T569" id="Seg_13457" s="T568">n</ta>
            <ta e="T570" id="Seg_13458" s="T569">v</ta>
            <ta e="T571" id="Seg_13459" s="T570">aux</ta>
            <ta e="T572" id="Seg_13460" s="T571">v</ta>
            <ta e="T573" id="Seg_13461" s="T572">aux</ta>
            <ta e="T574" id="Seg_13462" s="T573">cardnum</ta>
            <ta e="T575" id="Seg_13463" s="T574">n</ta>
            <ta e="T576" id="Seg_13464" s="T575">n</ta>
            <ta e="T577" id="Seg_13465" s="T576">v</ta>
            <ta e="T578" id="Seg_13466" s="T577">v</ta>
            <ta e="T579" id="Seg_13467" s="T578">v</ta>
            <ta e="T580" id="Seg_13468" s="T579">que</ta>
            <ta e="T581" id="Seg_13469" s="T580">ptcl</ta>
            <ta e="T582" id="Seg_13470" s="T581">ordnum</ta>
            <ta e="T583" id="Seg_13471" s="T582">n</ta>
            <ta e="T584" id="Seg_13472" s="T583">ptcl</ta>
            <ta e="T585" id="Seg_13473" s="T584">cardnum</ta>
            <ta e="T586" id="Seg_13474" s="T585">n</ta>
            <ta e="T587" id="Seg_13475" s="T586">n</ta>
            <ta e="T588" id="Seg_13476" s="T587">n</ta>
            <ta e="T589" id="Seg_13477" s="T588">v</ta>
            <ta e="T590" id="Seg_13478" s="T589">v</ta>
            <ta e="T591" id="Seg_13479" s="T590">v</ta>
            <ta e="T592" id="Seg_13480" s="T591">propr</ta>
            <ta e="T593" id="Seg_13481" s="T592">ptcl</ta>
            <ta e="T594" id="Seg_13482" s="T593">v</ta>
            <ta e="T595" id="Seg_13483" s="T594">cardnum</ta>
            <ta e="T596" id="Seg_13484" s="T595">cardnum</ta>
            <ta e="T597" id="Seg_13485" s="T596">n</ta>
            <ta e="T598" id="Seg_13486" s="T597">n</ta>
            <ta e="T599" id="Seg_13487" s="T598">propr</ta>
            <ta e="T600" id="Seg_13488" s="T599">v</ta>
            <ta e="T601" id="Seg_13489" s="T600">pers</ta>
            <ta e="T602" id="Seg_13490" s="T601">cardnum</ta>
            <ta e="T603" id="Seg_13491" s="T602">n</ta>
            <ta e="T604" id="Seg_13492" s="T603">ptcl</ta>
            <ta e="T605" id="Seg_13493" s="T604">cardnum</ta>
            <ta e="T606" id="Seg_13494" s="T605">n</ta>
            <ta e="T607" id="Seg_13495" s="T606">n</ta>
            <ta e="T608" id="Seg_13496" s="T607">conj</ta>
            <ta e="T609" id="Seg_13497" s="T608">cardnum</ta>
            <ta e="T610" id="Seg_13498" s="T609">n</ta>
            <ta e="T611" id="Seg_13499" s="T610">n</ta>
            <ta e="T612" id="Seg_13500" s="T611">adv</ta>
            <ta e="T613" id="Seg_13501" s="T612">n</ta>
            <ta e="T614" id="Seg_13502" s="T613">cop</ta>
            <ta e="T615" id="Seg_13503" s="T614">v</ta>
            <ta e="T616" id="Seg_13504" s="T615">n</ta>
            <ta e="T617" id="Seg_13505" s="T616">dempro</ta>
            <ta e="T618" id="Seg_13506" s="T617">n</ta>
            <ta e="T619" id="Seg_13507" s="T618">n</ta>
            <ta e="T620" id="Seg_13508" s="T619">n</ta>
            <ta e="T621" id="Seg_13509" s="T620">n</ta>
            <ta e="T622" id="Seg_13510" s="T621">v</ta>
            <ta e="T623" id="Seg_13511" s="T622">adj</ta>
            <ta e="T624" id="Seg_13512" s="T623">cardnum</ta>
            <ta e="T625" id="Seg_13513" s="T624">n</ta>
            <ta e="T626" id="Seg_13514" s="T625">ordnum</ta>
            <ta e="T627" id="Seg_13515" s="T626">n</ta>
            <ta e="T628" id="Seg_13516" s="T627">v</ta>
            <ta e="T629" id="Seg_13517" s="T628">n</ta>
            <ta e="T630" id="Seg_13518" s="T629">pers</ta>
            <ta e="T631" id="Seg_13519" s="T630">cop</ta>
            <ta e="T632" id="Seg_13520" s="T631">adj</ta>
            <ta e="T633" id="Seg_13521" s="T632">n</ta>
            <ta e="T634" id="Seg_13522" s="T633">v</ta>
            <ta e="T635" id="Seg_13523" s="T634">n</ta>
            <ta e="T636" id="Seg_13524" s="T635">n</ta>
            <ta e="T637" id="Seg_13525" s="T636">n</ta>
            <ta e="T638" id="Seg_13526" s="T637">v</ta>
            <ta e="T639" id="Seg_13527" s="T638">v</ta>
            <ta e="T640" id="Seg_13528" s="T639">n</ta>
            <ta e="T641" id="Seg_13529" s="T640">n</ta>
            <ta e="T642" id="Seg_13530" s="T641">v</ta>
            <ta e="T643" id="Seg_13531" s="T642">v</ta>
            <ta e="T644" id="Seg_13532" s="T643">v</ta>
            <ta e="T645" id="Seg_13533" s="T644">n</ta>
            <ta e="T646" id="Seg_13534" s="T645">post</ta>
            <ta e="T647" id="Seg_13535" s="T646">n</ta>
            <ta e="T648" id="Seg_13536" s="T647">ptcl</ta>
            <ta e="T649" id="Seg_13537" s="T648">cop</ta>
            <ta e="T650" id="Seg_13538" s="T649">dempro</ta>
            <ta e="T651" id="Seg_13539" s="T650">n</ta>
            <ta e="T652" id="Seg_13540" s="T651">n</ta>
            <ta e="T653" id="Seg_13541" s="T652">post</ta>
            <ta e="T654" id="Seg_13542" s="T653">n</ta>
            <ta e="T655" id="Seg_13543" s="T654">v</ta>
            <ta e="T656" id="Seg_13544" s="T655">cardnum</ta>
            <ta e="T657" id="Seg_13545" s="T656">n</ta>
            <ta e="T658" id="Seg_13546" s="T657">post</ta>
            <ta e="T659" id="Seg_13547" s="T658">adj</ta>
            <ta e="T660" id="Seg_13548" s="T659">ptcl</ta>
            <ta e="T661" id="Seg_13549" s="T660">n</ta>
            <ta e="T662" id="Seg_13550" s="T661">v</ta>
            <ta e="T663" id="Seg_13551" s="T662">n</ta>
            <ta e="T664" id="Seg_13552" s="T663">cop</ta>
            <ta e="T665" id="Seg_13553" s="T664">n</ta>
            <ta e="T666" id="Seg_13554" s="T665">n</ta>
            <ta e="T667" id="Seg_13555" s="T666">adj</ta>
            <ta e="T668" id="Seg_13556" s="T667">cop</ta>
            <ta e="T669" id="Seg_13557" s="T668">dempro</ta>
            <ta e="T670" id="Seg_13558" s="T669">pers</ta>
            <ta e="T671" id="Seg_13559" s="T670">n</ta>
            <ta e="T672" id="Seg_13560" s="T671">n</ta>
            <ta e="T673" id="Seg_13561" s="T672">adj</ta>
            <ta e="T674" id="Seg_13562" s="T673">n</ta>
            <ta e="T675" id="Seg_13563" s="T674">v</ta>
            <ta e="T676" id="Seg_13564" s="T675">n</ta>
            <ta e="T677" id="Seg_13565" s="T676">v</ta>
            <ta e="T678" id="Seg_13566" s="T677">n</ta>
            <ta e="T679" id="Seg_13567" s="T678">n</ta>
            <ta e="T680" id="Seg_13568" s="T679">n</ta>
            <ta e="T681" id="Seg_13569" s="T680">post</ta>
            <ta e="T682" id="Seg_13570" s="T681">v</ta>
            <ta e="T683" id="Seg_13571" s="T682">cardnum</ta>
            <ta e="T684" id="Seg_13572" s="T683">n</ta>
            <ta e="T685" id="Seg_13573" s="T684">n</ta>
            <ta e="T686" id="Seg_13574" s="T685">cardnum</ta>
            <ta e="T687" id="Seg_13575" s="T686">n</ta>
            <ta e="T688" id="Seg_13576" s="T687">n</ta>
            <ta e="T689" id="Seg_13577" s="T688">ptcl</ta>
            <ta e="T690" id="Seg_13578" s="T689">v</ta>
            <ta e="T691" id="Seg_13579" s="T690">pers</ta>
            <ta e="T692" id="Seg_13580" s="T691">ptcl</ta>
            <ta e="T693" id="Seg_13581" s="T692">dempro</ta>
            <ta e="T694" id="Seg_13582" s="T693">adv</ta>
            <ta e="T695" id="Seg_13583" s="T694">v</ta>
            <ta e="T696" id="Seg_13584" s="T695">post</ta>
            <ta e="T697" id="Seg_13585" s="T696">adj</ta>
            <ta e="T698" id="Seg_13586" s="T697">n</ta>
            <ta e="T699" id="Seg_13587" s="T698">n</ta>
            <ta e="T700" id="Seg_13588" s="T699">n</ta>
            <ta e="T701" id="Seg_13589" s="T700">ptcl</ta>
            <ta e="T702" id="Seg_13590" s="T701">v</ta>
            <ta e="T703" id="Seg_13591" s="T702">aux</ta>
            <ta e="T704" id="Seg_13592" s="T703">dempro</ta>
            <ta e="T705" id="Seg_13593" s="T704">n</ta>
            <ta e="T706" id="Seg_13594" s="T705">v</ta>
            <ta e="T707" id="Seg_13595" s="T706">n</ta>
            <ta e="T708" id="Seg_13596" s="T707">v</ta>
            <ta e="T709" id="Seg_13597" s="T708">adv</ta>
            <ta e="T710" id="Seg_13598" s="T709">v</ta>
            <ta e="T711" id="Seg_13599" s="T710">ordnum</ta>
            <ta e="T712" id="Seg_13600" s="T711">n</ta>
            <ta e="T713" id="Seg_13601" s="T712">v</ta>
            <ta e="T714" id="Seg_13602" s="T713">v</ta>
            <ta e="T715" id="Seg_13603" s="T714">ordnum</ta>
            <ta e="T716" id="Seg_13604" s="T715">n</ta>
            <ta e="T717" id="Seg_13605" s="T716">v</ta>
            <ta e="T718" id="Seg_13606" s="T717">propr</ta>
            <ta e="T719" id="Seg_13607" s="T718">v</ta>
            <ta e="T720" id="Seg_13608" s="T719">v</ta>
            <ta e="T721" id="Seg_13609" s="T720">adv</ta>
            <ta e="T722" id="Seg_13610" s="T721">adv</ta>
            <ta e="T723" id="Seg_13611" s="T722">dempro</ta>
            <ta e="T724" id="Seg_13612" s="T723">n</ta>
            <ta e="T725" id="Seg_13613" s="T724">n</ta>
            <ta e="T726" id="Seg_13614" s="T725">cop</ta>
            <ta e="T727" id="Seg_13615" s="T726">v</ta>
            <ta e="T728" id="Seg_13616" s="T727">v</ta>
            <ta e="T729" id="Seg_13617" s="T728">v</ta>
            <ta e="T730" id="Seg_13618" s="T729">v</ta>
            <ta e="T731" id="Seg_13619" s="T730">n</ta>
            <ta e="T732" id="Seg_13620" s="T731">adj</ta>
            <ta e="T733" id="Seg_13621" s="T732">v</ta>
            <ta e="T734" id="Seg_13622" s="T733">dempro</ta>
            <ta e="T735" id="Seg_13623" s="T734">n</ta>
            <ta e="T736" id="Seg_13624" s="T735">n</ta>
            <ta e="T737" id="Seg_13625" s="T736">n</ta>
            <ta e="T738" id="Seg_13626" s="T737">cop</ta>
            <ta e="T739" id="Seg_13627" s="T738">n</ta>
            <ta e="T740" id="Seg_13628" s="T739">v</ta>
            <ta e="T741" id="Seg_13629" s="T740">v</ta>
            <ta e="T742" id="Seg_13630" s="T741">adv</ta>
            <ta e="T743" id="Seg_13631" s="T742">n</ta>
            <ta e="T744" id="Seg_13632" s="T743">post</ta>
            <ta e="T745" id="Seg_13633" s="T744">n</ta>
            <ta e="T746" id="Seg_13634" s="T745">v</ta>
            <ta e="T747" id="Seg_13635" s="T746">n</ta>
            <ta e="T748" id="Seg_13636" s="T747">n</ta>
            <ta e="T749" id="Seg_13637" s="T748">v</ta>
            <ta e="T750" id="Seg_13638" s="T749">que</ta>
            <ta e="T751" id="Seg_13639" s="T750">post</ta>
            <ta e="T752" id="Seg_13640" s="T751">n</ta>
            <ta e="T753" id="Seg_13641" s="T752">que</ta>
            <ta e="T754" id="Seg_13642" s="T753">v</ta>
            <ta e="T755" id="Seg_13643" s="T754">n</ta>
            <ta e="T756" id="Seg_13644" s="T755">v</ta>
            <ta e="T757" id="Seg_13645" s="T756">v</ta>
            <ta e="T758" id="Seg_13646" s="T757">n</ta>
            <ta e="T759" id="Seg_13647" s="T758">v</ta>
            <ta e="T760" id="Seg_13648" s="T759">v</ta>
            <ta e="T761" id="Seg_13649" s="T760">que</ta>
            <ta e="T762" id="Seg_13650" s="T761">dempro</ta>
            <ta e="T763" id="Seg_13651" s="T762">cardnum</ta>
            <ta e="T764" id="Seg_13652" s="T763">n</ta>
            <ta e="T765" id="Seg_13653" s="T764">n</ta>
            <ta e="T766" id="Seg_13654" s="T765">interj</ta>
            <ta e="T767" id="Seg_13655" s="T766">dempro</ta>
            <ta e="T768" id="Seg_13656" s="T767">pers</ta>
            <ta e="T769" id="Seg_13657" s="T768">n</ta>
            <ta e="T770" id="Seg_13658" s="T769">dempro</ta>
            <ta e="T771" id="Seg_13659" s="T770">n</ta>
            <ta e="T772" id="Seg_13660" s="T771">v</ta>
            <ta e="T773" id="Seg_13661" s="T772">aux</ta>
            <ta e="T774" id="Seg_13662" s="T773">ptcl</ta>
            <ta e="T775" id="Seg_13663" s="T774">adv</ta>
            <ta e="T776" id="Seg_13664" s="T775">pers</ta>
            <ta e="T777" id="Seg_13665" s="T776">n</ta>
            <ta e="T778" id="Seg_13666" s="T777">cop</ta>
            <ta e="T779" id="Seg_13667" s="T778">v</ta>
            <ta e="T780" id="Seg_13668" s="T779">v</ta>
            <ta e="T781" id="Seg_13669" s="T780">conj</ta>
            <ta e="T782" id="Seg_13670" s="T781">n</ta>
            <ta e="T783" id="Seg_13671" s="T782">cop</ta>
            <ta e="T784" id="Seg_13672" s="T783">ptcl</ta>
            <ta e="T785" id="Seg_13673" s="T784">dempro</ta>
            <ta e="T786" id="Seg_13674" s="T785">cardnum</ta>
            <ta e="T787" id="Seg_13675" s="T786">adj</ta>
            <ta e="T788" id="Seg_13676" s="T787">n</ta>
            <ta e="T789" id="Seg_13677" s="T788">adj</ta>
            <ta e="T790" id="Seg_13678" s="T789">n</ta>
            <ta e="T791" id="Seg_13679" s="T790">n</ta>
            <ta e="T792" id="Seg_13680" s="T791">cop</ta>
            <ta e="T793" id="Seg_13681" s="T792">adv</ta>
            <ta e="T794" id="Seg_13682" s="T793">n</ta>
            <ta e="T795" id="Seg_13683" s="T794">n</ta>
            <ta e="T796" id="Seg_13684" s="T795">adj</ta>
            <ta e="T797" id="Seg_13685" s="T796">n</ta>
            <ta e="T798" id="Seg_13686" s="T797">v</ta>
            <ta e="T799" id="Seg_13687" s="T798">v</ta>
            <ta e="T800" id="Seg_13688" s="T799">v</ta>
            <ta e="T801" id="Seg_13689" s="T800">aux</ta>
            <ta e="T802" id="Seg_13690" s="T801">n</ta>
            <ta e="T803" id="Seg_13691" s="T802">v</ta>
            <ta e="T804" id="Seg_13692" s="T803">adj</ta>
            <ta e="T805" id="Seg_13693" s="T804">n</ta>
            <ta e="T806" id="Seg_13694" s="T805">n</ta>
            <ta e="T807" id="Seg_13695" s="T806">cop</ta>
            <ta e="T808" id="Seg_13696" s="T807">ptcl</ta>
            <ta e="T809" id="Seg_13697" s="T808">n</ta>
            <ta e="T810" id="Seg_13698" s="T809">v</ta>
            <ta e="T811" id="Seg_13699" s="T810">cardnum</ta>
            <ta e="T812" id="Seg_13700" s="T811">n</ta>
            <ta e="T813" id="Seg_13701" s="T812">ptcl</ta>
            <ta e="T814" id="Seg_13702" s="T813">v</ta>
            <ta e="T815" id="Seg_13703" s="T814">que</ta>
            <ta e="T816" id="Seg_13704" s="T815">post</ta>
            <ta e="T817" id="Seg_13705" s="T816">adj</ta>
            <ta e="T818" id="Seg_13706" s="T817">n</ta>
            <ta e="T819" id="Seg_13707" s="T818">n</ta>
            <ta e="T820" id="Seg_13708" s="T819">ptcl</ta>
            <ta e="T821" id="Seg_13709" s="T820">v</ta>
            <ta e="T822" id="Seg_13710" s="T821">ptcl</ta>
            <ta e="T823" id="Seg_13711" s="T822">n</ta>
            <ta e="T824" id="Seg_13712" s="T823">v</ta>
            <ta e="T825" id="Seg_13713" s="T824">que</ta>
            <ta e="T826" id="Seg_13714" s="T825">cop</ta>
            <ta e="T827" id="Seg_13715" s="T826">n</ta>
            <ta e="T828" id="Seg_13716" s="T827">interj</ta>
            <ta e="T829" id="Seg_13717" s="T828">cardnum</ta>
            <ta e="T830" id="Seg_13718" s="T829">n</ta>
            <ta e="T831" id="Seg_13719" s="T830">n</ta>
            <ta e="T832" id="Seg_13720" s="T831">interj</ta>
            <ta e="T833" id="Seg_13721" s="T832">dempro</ta>
            <ta e="T834" id="Seg_13722" s="T833">n</ta>
            <ta e="T835" id="Seg_13723" s="T834">v</ta>
            <ta e="T836" id="Seg_13724" s="T835">aux</ta>
            <ta e="T837" id="Seg_13725" s="T836">n</ta>
            <ta e="T838" id="Seg_13726" s="T837">interj</ta>
            <ta e="T839" id="Seg_13727" s="T838">pers</ta>
            <ta e="T840" id="Seg_13728" s="T839">n</ta>
            <ta e="T841" id="Seg_13729" s="T840">cop</ta>
            <ta e="T842" id="Seg_13730" s="T841">v</ta>
            <ta e="T843" id="Seg_13731" s="T842">cop</ta>
            <ta e="T844" id="Seg_13732" s="T843">ptcl</ta>
            <ta e="T845" id="Seg_13733" s="T844">dempro</ta>
            <ta e="T846" id="Seg_13734" s="T845">n</ta>
            <ta e="T847" id="Seg_13735" s="T846">n</ta>
            <ta e="T848" id="Seg_13736" s="T847">n</ta>
            <ta e="T849" id="Seg_13737" s="T848">v</ta>
            <ta e="T850" id="Seg_13738" s="T849">v</ta>
            <ta e="T851" id="Seg_13739" s="T850">dempro</ta>
            <ta e="T852" id="Seg_13740" s="T851">n</ta>
            <ta e="T853" id="Seg_13741" s="T852">n</ta>
            <ta e="T854" id="Seg_13742" s="T853">v</ta>
            <ta e="T855" id="Seg_13743" s="T854">que</ta>
            <ta e="T856" id="Seg_13744" s="T855">v</ta>
            <ta e="T857" id="Seg_13745" s="T856">n</ta>
            <ta e="T858" id="Seg_13746" s="T857">n</ta>
            <ta e="T859" id="Seg_13747" s="T858">v</ta>
            <ta e="T860" id="Seg_13748" s="T859">ptcl</ta>
            <ta e="T861" id="Seg_13749" s="T860">ptcl</ta>
            <ta e="T862" id="Seg_13750" s="T861">v</ta>
            <ta e="T863" id="Seg_13751" s="T862">v</ta>
            <ta e="T864" id="Seg_13752" s="T863">ptcl</ta>
            <ta e="T865" id="Seg_13753" s="T864">adj</ta>
            <ta e="T866" id="Seg_13754" s="T865">conj</ta>
            <ta e="T867" id="Seg_13755" s="T866">adv</ta>
            <ta e="T868" id="Seg_13756" s="T867">ptcl</ta>
            <ta e="T869" id="Seg_13757" s="T868">v</ta>
            <ta e="T870" id="Seg_13758" s="T869">adv</ta>
            <ta e="T871" id="Seg_13759" s="T870">v</ta>
            <ta e="T872" id="Seg_13760" s="T871">cardnum</ta>
            <ta e="T873" id="Seg_13761" s="T872">n</ta>
            <ta e="T874" id="Seg_13762" s="T873">v</ta>
            <ta e="T875" id="Seg_13763" s="T874">n</ta>
            <ta e="T876" id="Seg_13764" s="T875">n</ta>
            <ta e="T877" id="Seg_13765" s="T876">v</ta>
            <ta e="T878" id="Seg_13766" s="T877">v</ta>
            <ta e="T879" id="Seg_13767" s="T878">n</ta>
            <ta e="T880" id="Seg_13768" s="T879">v</ta>
            <ta e="T881" id="Seg_13769" s="T880">n</ta>
            <ta e="T882" id="Seg_13770" s="T881">v</ta>
            <ta e="T883" id="Seg_13771" s="T882">aux</ta>
            <ta e="T884" id="Seg_13772" s="T883">n</ta>
            <ta e="T885" id="Seg_13773" s="T884">v</ta>
            <ta e="T886" id="Seg_13774" s="T885">n</ta>
            <ta e="T887" id="Seg_13775" s="T886">v</ta>
            <ta e="T888" id="Seg_13776" s="T887">n</ta>
            <ta e="T889" id="Seg_13777" s="T888">n</ta>
            <ta e="T890" id="Seg_13778" s="T889">n</ta>
            <ta e="T891" id="Seg_13779" s="T890">v</ta>
            <ta e="T892" id="Seg_13780" s="T891">interj</ta>
            <ta e="T893" id="Seg_13781" s="T892">que</ta>
            <ta e="T894" id="Seg_13782" s="T893">v</ta>
            <ta e="T895" id="Seg_13783" s="T894">pers</ta>
            <ta e="T896" id="Seg_13784" s="T895">n</ta>
            <ta e="T897" id="Seg_13785" s="T896">v</ta>
            <ta e="T898" id="Seg_13786" s="T897">n</ta>
            <ta e="T899" id="Seg_13787" s="T898">adv</ta>
            <ta e="T900" id="Seg_13788" s="T899">v</ta>
            <ta e="T901" id="Seg_13789" s="T900">adj</ta>
            <ta e="T902" id="Seg_13790" s="T901">n</ta>
            <ta e="T903" id="Seg_13791" s="T902">v</ta>
            <ta e="T904" id="Seg_13792" s="T903">v</ta>
            <ta e="T905" id="Seg_13793" s="T904">ptcl</ta>
            <ta e="T906" id="Seg_13794" s="T905">v</ta>
            <ta e="T907" id="Seg_13795" s="T906">ptcl</ta>
            <ta e="T908" id="Seg_13796" s="T907">que</ta>
            <ta e="T909" id="Seg_13797" s="T908">v</ta>
            <ta e="T910" id="Seg_13798" s="T909">v</ta>
            <ta e="T911" id="Seg_13799" s="T910">v</ta>
            <ta e="T912" id="Seg_13800" s="T911">v</ta>
            <ta e="T913" id="Seg_13801" s="T912">dempro</ta>
            <ta e="T914" id="Seg_13802" s="T913">v</ta>
            <ta e="T915" id="Seg_13803" s="T914">cardnum</ta>
            <ta e="T916" id="Seg_13804" s="T915">n</ta>
            <ta e="T917" id="Seg_13805" s="T916">v</ta>
            <ta e="T918" id="Seg_13806" s="T917">n</ta>
            <ta e="T919" id="Seg_13807" s="T918">adj</ta>
            <ta e="T920" id="Seg_13808" s="T919">cop</ta>
            <ta e="T921" id="Seg_13809" s="T920">v</ta>
            <ta e="T922" id="Seg_13810" s="T921">n</ta>
            <ta e="T923" id="Seg_13811" s="T922">cop</ta>
            <ta e="T924" id="Seg_13812" s="T923">n</ta>
            <ta e="T925" id="Seg_13813" s="T924">n</ta>
            <ta e="T926" id="Seg_13814" s="T925">n</ta>
            <ta e="T927" id="Seg_13815" s="T926">n</ta>
            <ta e="T928" id="Seg_13816" s="T927">post</ta>
            <ta e="T929" id="Seg_13817" s="T928">adj</ta>
            <ta e="T930" id="Seg_13818" s="T929">ptcl</ta>
            <ta e="T931" id="Seg_13819" s="T930">n</ta>
            <ta e="T932" id="Seg_13820" s="T931">v</ta>
            <ta e="T933" id="Seg_13821" s="T932">n</ta>
            <ta e="T934" id="Seg_13822" s="T933">n</ta>
            <ta e="T935" id="Seg_13823" s="T934">post</ta>
            <ta e="T936" id="Seg_13824" s="T935">v</ta>
            <ta e="T937" id="Seg_13825" s="T936">n</ta>
            <ta e="T938" id="Seg_13826" s="T937">v</ta>
            <ta e="T939" id="Seg_13827" s="T938">aux</ta>
            <ta e="T940" id="Seg_13828" s="T939">adv</ta>
            <ta e="T941" id="Seg_13829" s="T940">n</ta>
            <ta e="T942" id="Seg_13830" s="T941">n</ta>
            <ta e="T943" id="Seg_13831" s="T942">n</ta>
            <ta e="T944" id="Seg_13832" s="T943">n</ta>
            <ta e="T945" id="Seg_13833" s="T944">n</ta>
            <ta e="T946" id="Seg_13834" s="T945">v</ta>
            <ta e="T947" id="Seg_13835" s="T946">cardnum</ta>
            <ta e="T948" id="Seg_13836" s="T947">n</ta>
            <ta e="T949" id="Seg_13837" s="T948">n</ta>
            <ta e="T950" id="Seg_13838" s="T949">v</ta>
            <ta e="T951" id="Seg_13839" s="T950">cardnum</ta>
            <ta e="T952" id="Seg_13840" s="T951">adj</ta>
            <ta e="T953" id="Seg_13841" s="T952">ptcl</ta>
            <ta e="T954" id="Seg_13842" s="T953">n</ta>
            <ta e="T955" id="Seg_13843" s="T954">v</ta>
            <ta e="T956" id="Seg_13844" s="T955">interj</ta>
            <ta e="T957" id="Seg_13845" s="T956">n</ta>
            <ta e="T958" id="Seg_13846" s="T957">n</ta>
            <ta e="T959" id="Seg_13847" s="T958">n</ta>
            <ta e="T960" id="Seg_13848" s="T959">pers</ta>
            <ta e="T961" id="Seg_13849" s="T960">n</ta>
            <ta e="T962" id="Seg_13850" s="T961">n</ta>
            <ta e="T963" id="Seg_13851" s="T962">post</ta>
            <ta e="T964" id="Seg_13852" s="T963">adv</ta>
            <ta e="T965" id="Seg_13853" s="T964">cardnum</ta>
            <ta e="T966" id="Seg_13854" s="T965">n</ta>
            <ta e="T967" id="Seg_13855" s="T966">v</ta>
            <ta e="T968" id="Seg_13856" s="T967">dempro</ta>
            <ta e="T969" id="Seg_13857" s="T968">n</ta>
            <ta e="T970" id="Seg_13858" s="T969">adv</ta>
            <ta e="T971" id="Seg_13859" s="T970">cardnum</ta>
            <ta e="T972" id="Seg_13860" s="T971">n</ta>
            <ta e="T973" id="Seg_13861" s="T972">cardnum</ta>
            <ta e="T974" id="Seg_13862" s="T973">n</ta>
            <ta e="T975" id="Seg_13863" s="T974">v</ta>
            <ta e="T976" id="Seg_13864" s="T975">ptcl</ta>
            <ta e="T977" id="Seg_13865" s="T976">v</ta>
            <ta e="T978" id="Seg_13866" s="T977">n</ta>
            <ta e="T979" id="Seg_13867" s="T978">v</ta>
            <ta e="T980" id="Seg_13868" s="T979">ptcl</ta>
            <ta e="T981" id="Seg_13869" s="T980">v</ta>
            <ta e="T982" id="Seg_13870" s="T981">n</ta>
            <ta e="T983" id="Seg_13871" s="T982">v</ta>
            <ta e="T984" id="Seg_13872" s="T983">propr</ta>
            <ta e="T985" id="Seg_13873" s="T984">v</ta>
            <ta e="T986" id="Seg_13874" s="T985">ordnum</ta>
            <ta e="T987" id="Seg_13875" s="T986">n</ta>
            <ta e="T988" id="Seg_13876" s="T987">n</ta>
            <ta e="T989" id="Seg_13877" s="T988">propr</ta>
            <ta e="T990" id="Seg_13878" s="T989">v</ta>
            <ta e="T991" id="Seg_13879" s="T990">n</ta>
            <ta e="T992" id="Seg_13880" s="T991">n</ta>
            <ta e="T993" id="Seg_13881" s="T992">adj</ta>
            <ta e="T994" id="Seg_13882" s="T993">ptcl</ta>
            <ta e="T995" id="Seg_13883" s="T994">n</ta>
            <ta e="T996" id="Seg_13884" s="T995">n</ta>
            <ta e="T997" id="Seg_13885" s="T996">v</ta>
            <ta e="T998" id="Seg_13886" s="T997">n</ta>
            <ta e="T999" id="Seg_13887" s="T998">v</ta>
            <ta e="T1000" id="Seg_13888" s="T999">ptcl</ta>
            <ta e="T1001" id="Seg_13889" s="T1000">adj</ta>
            <ta e="T1002" id="Seg_13890" s="T1001">adj</ta>
            <ta e="T1003" id="Seg_13891" s="T1002">n</ta>
            <ta e="T1004" id="Seg_13892" s="T1003">v</ta>
            <ta e="T1005" id="Seg_13893" s="T1004">n</ta>
            <ta e="T1006" id="Seg_13894" s="T1005">n</ta>
            <ta e="T1007" id="Seg_13895" s="T1006">adv</ta>
            <ta e="T1008" id="Seg_13896" s="T1007">v</ta>
            <ta e="T1009" id="Seg_13897" s="T1008">n</ta>
            <ta e="T1010" id="Seg_13898" s="T1009">ptcl</ta>
            <ta e="T1011" id="Seg_13899" s="T1010">v</ta>
            <ta e="T1012" id="Seg_13900" s="T1011">n</ta>
            <ta e="T1013" id="Seg_13901" s="T1012">v</ta>
            <ta e="T1014" id="Seg_13902" s="T1013">ptcl</ta>
            <ta e="T1015" id="Seg_13903" s="T1014">v</ta>
            <ta e="T1016" id="Seg_13904" s="T1015">que</ta>
            <ta e="T1017" id="Seg_13905" s="T1016">ptcl</ta>
            <ta e="T1018" id="Seg_13906" s="T1017">v</ta>
            <ta e="T1019" id="Seg_13907" s="T1018">propr</ta>
            <ta e="T1020" id="Seg_13908" s="T1019">ptcl</ta>
            <ta e="T1021" id="Seg_13909" s="T1020">adv</ta>
            <ta e="T1022" id="Seg_13910" s="T1021">v</ta>
            <ta e="T1023" id="Seg_13911" s="T1022">n</ta>
            <ta e="T1024" id="Seg_13912" s="T1023">n</ta>
            <ta e="T1025" id="Seg_13913" s="T1024">cop</ta>
            <ta e="T1026" id="Seg_13914" s="T1025">v</ta>
            <ta e="T1027" id="Seg_13915" s="T1026">dempro</ta>
            <ta e="T1028" id="Seg_13916" s="T1027">n</ta>
            <ta e="T1029" id="Seg_13917" s="T1028">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_13918" s="T0">There lived three brothers.</ta>
            <ta e="T14" id="Seg_13919" s="T3">The youngest one is still very small, the middle one is just crawling on his knees and is called Kungadyay.</ta>
            <ta e="T22" id="Seg_13920" s="T14">The eldest brother does not have a name, he is a hunter, he has got a wife and small child.</ta>
            <ta e="T26" id="Seg_13921" s="T22">Their reindeer is very lonely, [it is] like a horse.</ta>
            <ta e="T32" id="Seg_13922" s="T26">They never harness it, only while nomadizing they let it pull their tent.</ta>
            <ta e="T35" id="Seg_13923" s="T32">The eldest brother is a famous hunter.</ta>
            <ta e="T39" id="Seg_13924" s="T35">He hunts wild reindeers and goes fishing, too.</ta>
            <ta e="T52" id="Seg_13925" s="T39">As spring is coming the eldest brother gets sick, he got worse and on the third day he had completely weakened, he spoke to his wife: </ta>
            <ta e="T55" id="Seg_13926" s="T52">"Well, I am dying.</ta>
            <ta e="T57" id="Seg_13927" s="T55">Who will take care [of you]?!</ta>
            <ta e="T62" id="Seg_13928" s="T57">You will eat the haul, that I have hunted, during the summer.</ta>
            <ta e="T70" id="Seg_13929" s="T62">When you have spent the food, then in the next year scratch the remaining meat from the leather and cook it.</ta>
            <ta e="T75" id="Seg_13930" s="T70">There is no use of this family.</ta>
            <ta e="T77" id="Seg_13931" s="T75">You will also die.</ta>
            <ta e="T84" id="Seg_13932" s="T77">Maybe my child survives, if it doesn't die, then it will feed you.</ta>
            <ta e="T85" id="Seg_13933" s="T84">That's all."</ta>
            <ta e="T87" id="Seg_13934" s="T85">He dies.</ta>
            <ta e="T91" id="Seg_13935" s="T87">Crying his wife buries him.</ta>
            <ta e="T94" id="Seg_13936" s="T91">Summer comes.</ta>
            <ta e="T96" id="Seg_13937" s="T94">They dry their food.</ta>
            <ta e="T102" id="Seg_13938" s="T96">They come through the summer, in autumn they eat reindeer leather and skin.</ta>
            <ta e="T104" id="Seg_13939" s="T102">Everything runs out.</ta>
            <ta e="T106" id="Seg_13940" s="T104">Winter comes.</ta>
            <ta e="T109" id="Seg_13941" s="T106">There is nothing to eat.</ta>
            <ta e="T116" id="Seg_13942" s="T109">On a windless day she hears the sound of a sledge.</ta>
            <ta e="T124" id="Seg_13943" s="T116">The woman runs outside, [somebody] stopped at a place, from where only his voice can be heard.</ta>
            <ta e="T133" id="Seg_13944" s="T124">It is apparently this woman's father, the master of eight black reindeers, the prince.</ta>
            <ta e="T136" id="Seg_13945" s="T133">"Hey, how do you live?"</ta>
            <ta e="T139" id="Seg_13946" s="T136">"My husband died last year."</ta>
            <ta e="T141" id="Seg_13947" s="T139">"Well, thank God!</ta>
            <ta e="T143" id="Seg_13948" s="T141">I am very happy!</ta>
            <ta e="T147" id="Seg_13949" s="T143">Get dressed, we will go to my home!"</ta>
            <ta e="T150" id="Seg_13950" s="T147">"What about my child, my family?"</ta>
            <ta e="T153" id="Seg_13951" s="T150">"They shall die of hunger, let's go!"</ta>
            <ta e="T160" id="Seg_13952" s="T153">The woman gets dressed, sits down onto the sledge and they drive off.</ta>
            <ta e="T164" id="Seg_13953" s="T160">Kungadyay [and his brother] are lying there very hungry.</ta>
            <ta e="T168" id="Seg_13954" s="T164">The polar night comes to an end, it starts to get light.</ta>
            <ta e="T171" id="Seg_13955" s="T168">Kungadyay says to his younger brother: </ta>
            <ta e="T173" id="Seg_13956" s="T171">"Harness your reindeer.</ta>
            <ta e="T175" id="Seg_13957" s="T173">Lying so here, we will die.</ta>
            <ta e="T178" id="Seg_13958" s="T175">Let us follow the eight black reindeers!"</ta>
            <ta e="T185" id="Seg_13959" s="T178">They harness their reindeers, demount their tent and drive off, apparently.</ta>
            <ta e="T188" id="Seg_13960" s="T185">They overnighted several times.</ta>
            <ta e="T192" id="Seg_13961" s="T188">Once they climbed a high forested mountain.</ta>
            <ta e="T206" id="Seg_13962" s="T192">They watched: In the middle of a broad plain a lot of herds, on the other side of the plain seventy tents are seen. </ta>
            <ta e="T212" id="Seg_13963" s="T206">They drove through the herd to the other side.</ta>
            <ta e="T215" id="Seg_13964" s="T212">"Stop!", Kungadyay said.</ta>
            <ta e="T223" id="Seg_13965" s="T215">"Stop at the best tent of them, [don't go] to the place for chopping wood."</ta>
            <ta e="T228" id="Seg_13966" s="T223">He saw, that one house was bigger then the others.</ta>
            <ta e="T235" id="Seg_13967" s="T228">They came to the entrance and released their reindeers.</ta>
            <ta e="T239" id="Seg_13968" s="T235">They built up the tent and lit up a fire.</ta>
            <ta e="T242" id="Seg_13969" s="T239">To the younger brother: "Go to our sister-in-law.</ta>
            <ta e="T245" id="Seg_13970" s="T242">She shall come and suckle her child.</ta>
            <ta e="T258" id="Seg_13971" s="T245">Say to the master of eight black reindeers: 'We are dying, he shall share the half of a heart and the liver with the widower'", he says.</ta>
            <ta e="T262" id="Seg_13972" s="T258">The boy runs inside and says it.</ta>
            <ta e="T266" id="Seg_13973" s="T262">"Look, what a mess, to suckle [it]!</ta>
            <ta e="T270" id="Seg_13974" s="T266">Shall it die of hunger!", says the woman.</ta>
            <ta e="T283" id="Seg_13975" s="T270">"Grandfather, my elder brother sand you a message: 'We are dying, you should share at least the half of the heart and the liver with the widower'."</ta>
            <ta e="T286" id="Seg_13976" s="T283">"What, die of hunger!</ta>
            <ta e="T291" id="Seg_13977" s="T286">I don't give anything!", the master of eight black reindeers says.</ta>
            <ta e="T292" id="Seg_13978" s="T291">They go to sleep.</ta>
            <ta e="T294" id="Seg_13979" s="T292">What will they eat?!</ta>
            <ta e="T298" id="Seg_13980" s="T294">Suddenly a voice driving reindeers is heard.</ta>
            <ta e="T303" id="Seg_13981" s="T298">Kungadyay went outside and began to pee, on his knees.</ta>
            <ta e="T306" id="Seg_13982" s="T303">Many reindeers come.</ta>
            <ta e="T311" id="Seg_13983" s="T306">A very fat castrated reindeer bull comes.</ta>
            <ta e="T314" id="Seg_13984" s="T311">The boy catches it.</ta>
            <ta e="T316" id="Seg_13985" s="T314">That one is struggling.</ta>
            <ta e="T318" id="Seg_13986" s="T316">He lets bring his knife.</ta>
            <ta e="T321" id="Seg_13987" s="T318">He cuts its throat.</ta>
            <ta e="T331" id="Seg_13988" s="T321">Without skinning it they bring it inside, cook it and eat the whole day until the evening.</ta>
            <ta e="T337" id="Seg_13989" s="T331">Beneath this tent a small tent is standing.</ta>
            <ta e="T348" id="Seg_13990" s="T337">"Go into the tent of the master of seven buttons (probably the village elder), ask him for food."</ta>
            <ta e="T355" id="Seg_13991" s="T348">The boy goes and passes on the message to the master of seven buttons.</ta>
            <ta e="T360" id="Seg_13992" s="T355">"Did the master of eight black reindeers give something?"</ta>
            <ta e="T361" id="Seg_13993" s="T360">"No."</ta>
            <ta e="T366" id="Seg_13994" s="T361">"Well, then I don't give anything, too."</ta>
            <ta e="T367" id="Seg_13995" s="T366">They eat.</ta>
            <ta e="T371" id="Seg_13996" s="T367">In the morning the herd goes on again.</ta>
            <ta e="T375" id="Seg_13997" s="T371">Kungadjaj crawls out on kis knees.</ta>
            <ta e="T381" id="Seg_13998" s="T375">Kungadjaj catches an infertile reindeer doe by its antlers.</ta>
            <ta e="T384" id="Seg_13999" s="T381">"Hey, take out the knife!"</ta>
            <ta e="T386" id="Seg_14000" s="T384">He kills it.</ta>
            <ta e="T389" id="Seg_14001" s="T386">Again they eat it without skinning it.</ta>
            <ta e="T396" id="Seg_14002" s="T389">It turned out that he had killed the [reindeers] of those masters.</ta>
            <ta e="T397" id="Seg_14003" s="T396">They go to sleep.</ta>
            <ta e="T406" id="Seg_14004" s="T397">In the morning Kungadyay went out, the master of eight black reindeers is sitting and carving his pole for driving reindeers.</ta>
            <ta e="T411" id="Seg_14005" s="T406">"Oh, master of eight black reindeers, hello!"</ta>
            <ta e="T413" id="Seg_14006" s="T411">"You still dare to greet me!</ta>
            <ta e="T415" id="Seg_14007" s="T413">We will kill you today.</ta>
            <ta e="T419" id="Seg_14008" s="T415">You eat our last reindeers!"</ta>
            <ta e="T421" id="Seg_14009" s="T419">"It's your own fault."</ta>
            <ta e="T434" id="Seg_14010" s="T421">The master of eight black reindeers targets with his pole-spear the hip of the human, standing on his knees, but he misses it.</ta>
            <ta e="T438" id="Seg_14011" s="T434">Kungadyay crawled under the snow.</ta>
            <ta e="T445" id="Seg_14012" s="T438">He comes out there, where the master of seven buttons is sitting and carving his driving pole.</ta>
            <ta e="T450" id="Seg_14013" s="T445">"Oh, master of seven buttons, hello!"</ta>
            <ta e="T452" id="Seg_14014" s="T450">"You still dare to greet me!</ta>
            <ta e="T454" id="Seg_14015" s="T452">We will kill you today.</ta>
            <ta e="T458" id="Seg_14016" s="T454">You eat our last reindeers!"</ta>
            <ta e="T460" id="Seg_14017" s="T458">"It's your own fault!"</ta>
            <ta e="T472" id="Seg_14018" s="T460">The master of seven buttons wanted to push his pole-spear into the hip of the kneeing human, but he pushed it into the ground.</ta>
            <ta e="T484" id="Seg_14019" s="T472">Soon Kungadyay goes out in another place and jumpes up on his feet.</ta>
            <ta e="T488" id="Seg_14020" s="T484">He looked: a reindeer sledge is moving.</ta>
            <ta e="T494" id="Seg_14021" s="T488">Their sister-in-law goes off, her father married her.</ta>
            <ta e="T499" id="Seg_14022" s="T494">Kungadyay catches the reins of the sister-in-law: </ta>
            <ta e="T500" id="Seg_14023" s="T499">"I won't let you go.</ta>
            <ta e="T503" id="Seg_14024" s="T500">Where are you going?"</ta>
            <ta e="T506" id="Seg_14025" s="T503">The woman drives away.</ta>
            <ta e="T512" id="Seg_14026" s="T506">Kungadyay chases her and takes away her caravan.</ta>
            <ta e="T517" id="Seg_14027" s="T512">Kungadyay goes to the master of eight black reindeers: </ta>
            <ta e="T526" id="Seg_14028" s="T517">"Well, master of eight black reindeers, as long as we live in peace, take care of my younger brothers.</ta>
            <ta e="T532" id="Seg_14029" s="T526">If you treat them badly, I will defeat everybody one day. </ta>
            <ta e="T535" id="Seg_14030" s="T532">I follow my sister-in-law."</ta>
            <ta e="T539" id="Seg_14031" s="T535">He follows his sister-in-law, but he looses her [trace].</ta>
            <ta e="T545" id="Seg_14032" s="T539">He finds her at some distant location, but he doesn't chase her, until he reaches her.</ta>
            <ta e="T550" id="Seg_14033" s="T545">He blocks her way and lets her turn in the direction of the settlement.</ta>
            <ta e="T557" id="Seg_14034" s="T550">Although he can grab her, he doesn't grab her, but follows her into the tent.</ta>
            <ta e="T561" id="Seg_14035" s="T557">The sister-in-law is sitting and suckling the child.</ta>
            <ta e="T564" id="Seg_14036" s="T561">The younger brother is sitting and eating.</ta>
            <ta e="T568" id="Seg_14037" s="T564">"Well, sister-in-law, i forgive you.</ta>
            <ta e="T573" id="Seg_14038" s="T568">If you hadn't taken the child, I would have killed [you]!</ta>
            <ta e="T578" id="Seg_14039" s="T573">The master of eight black reindeers is afraid [of him]."</ta>
            <ta e="T579" id="Seg_14040" s="T578">They live.</ta>
            <ta e="T583" id="Seg_14041" s="T579">Sometime on the third day: </ta>
            <ta e="T589" id="Seg_14042" s="T583">"Well, master of eight black reindeers, gather your people.</ta>
            <ta e="T592" id="Seg_14043" s="T589">We will have gathering!", Kungadyay says.</ta>
            <ta e="T598" id="Seg_14044" s="T592">Well, they gather people from seven tents.</ta>
            <ta e="T600" id="Seg_14045" s="T598">Kungadyay says: </ta>
            <ta e="T611" id="Seg_14046" s="T600">"You are two masters, the master of eight black reindeers and the master of seven buttons.</ta>
            <ta e="T615" id="Seg_14047" s="T611">Now your lordship is over.</ta>
            <ta e="T621" id="Seg_14048" s="T615">Your master is this boy, the son of my elder brother.</ta>
            <ta e="T625" id="Seg_14049" s="T621">Gather yourselves within two days.</ta>
            <ta e="T628" id="Seg_14050" s="T625">On the third day you will go off.</ta>
            <ta e="T631" id="Seg_14051" s="T628">He will lead you."</ta>
            <ta e="T642" id="Seg_14052" s="T631">"Don't harness any other reindeer, harness only the reindeers your father made over to you!", he said to boy. (The boy had grown up.) </ta>
            <ta e="T644" id="Seg_14053" s="T642">"When you go, you will lead them.</ta>
            <ta e="T658" id="Seg_14054" s="T644">There is a crest along the forest, go along the tundra side of the crest, it will take two months.</ta>
            <ta e="T668" id="Seg_14055" s="T658">At the end, when the crest turns into tundra, there will be smoke, going up from underneath the snow.</ta>
            <ta e="T672" id="Seg_14056" s="T668">This is the land of our ancestors.</ta>
            <ta e="T677" id="Seg_14057" s="T672">You will go out on a big way, you will reach your relatives.</ta>
            <ta e="T682" id="Seg_14058" s="T677">People, you will look after your reindeers by day and by night.</ta>
            <ta e="T690" id="Seg_14059" s="T682">Master of eight black reindeers, master of seven buttons, you will see each other again.</ta>
            <ta e="T703" id="Seg_14060" s="T690">I, though, will kill bad people, Changits and animals, for your travel to be safe.</ta>
            <ta e="T714" id="Seg_14061" s="T703">I will be there the day you will reach it, if I am late, I will come on the third day!", he says.</ta>
            <ta e="T717" id="Seg_14062" s="T714">On the third day they start off.</ta>
            <ta e="T722" id="Seg_14063" s="T717">Kungadyay goes away from them, just by foot.</ta>
            <ta e="T728" id="Seg_14064" s="T722">They nomadized with this child as their leader.</ta>
            <ta e="T735" id="Seg_14065" s="T728">They travel and guard [their animals], that boy makes everybody guard, even lord and mistress.</ta>
            <ta e="T738" id="Seg_14066" s="T735">The middle of the month came.</ta>
            <ta e="T741" id="Seg_14067" s="T738">They went along the crest.</ta>
            <ta e="T749" id="Seg_14068" s="T741">Suddenly a crest branches off to the Tundra, a human is walking on it.</ta>
            <ta e="T752" id="Seg_14069" s="T749">"Where are you from?"</ta>
            <ta e="T754" id="Seg_14070" s="T752">"What comes?</ta>
            <ta e="T756" id="Seg_14071" s="T754">I don't know fire and tent.</ta>
            <ta e="T760" id="Seg_14072" s="T756">I walk and search for my elder brother."</ta>
            <ta e="T762" id="Seg_14073" s="T760">"What is he like?"</ta>
            <ta e="T765" id="Seg_14074" s="T762">"He is the master of eight black reindeers!"</ta>
            <ta e="T766" id="Seg_14075" s="T765">"Oh, really?</ta>
            <ta e="T769" id="Seg_14076" s="T766">This is my human.</ta>
            <ta e="T773" id="Seg_14077" s="T769">He drives free running domestic reindeers.</ta>
            <ta e="T778" id="Seg_14078" s="T773">Well then be my human."</ta>
            <ta e="T780" id="Seg_14079" s="T778">That one doesn't answer.</ta>
            <ta e="T796" id="Seg_14080" s="T780">"If you don't serve me, you will become an ornament on one of the three branches of my pole, two of them already have ornaments of two heroes." </ta>
            <ta e="T799" id="Seg_14081" s="T796">The human agrees and comes along.</ta>
            <ta e="T801" id="Seg_14082" s="T799">They kept driving.</ta>
            <ta e="T803" id="Seg_14083" s="T801">The month ended.</ta>
            <ta e="T807" id="Seg_14084" s="T803">The middle of the next month came.</ta>
            <ta e="T812" id="Seg_14085" s="T807">They saw again a human on a crest.</ta>
            <ta e="T814" id="Seg_14086" s="T812">He greeted them, too.</ta>
            <ta e="T818" id="Seg_14087" s="T814">"In which direction is your tent and your fireplace?"</ta>
            <ta e="T820" id="Seg_14088" s="T818">"I don't have a tent or a fireplace.</ta>
            <ta e="T822" id="Seg_14089" s="T820">I just walk.</ta>
            <ta e="T824" id="Seg_14090" s="T822">I search for my elder brother."</ta>
            <ta e="T827" id="Seg_14091" s="T824">"Who was your elder brother?"</ta>
            <ta e="T831" id="Seg_14092" s="T827">"Hey, he is the master of seven buttons!"</ta>
            <ta e="T837" id="Seg_14093" s="T831">"Hey, your elder brother driving free running domestic reindeers there.</ta>
            <ta e="T841" id="Seg_14094" s="T837">Well, will you be my human?"</ta>
            <ta e="T842" id="Seg_14095" s="T841">He doesn't answer.</ta>
            <ta e="T849" id="Seg_14096" s="T842">"If you won't be, I will make an ornament for one of the branches of my pole out of you!"</ta>
            <ta e="T852" id="Seg_14097" s="T849">There that human agrees.</ta>
            <ta e="T854" id="Seg_14098" s="T852">He comes to his elder brother: </ta>
            <ta e="T856" id="Seg_14099" s="T854">"Why dont' you kill [him]?</ta>
            <ta e="T860" id="Seg_14100" s="T856">Do you obey a child?"</ta>
            <ta e="T862" id="Seg_14101" s="T860">"No, let it be."</ta>
            <ta e="T864" id="Seg_14102" s="T862">"He should be killed.</ta>
            <ta e="T867" id="Seg_14103" s="T864">It's embarassing."</ta>
            <ta e="T869" id="Seg_14104" s="T867">"Well, we will try!"</ta>
            <ta e="T873" id="Seg_14105" s="T869">In the evening they stop at one place.</ta>
            <ta e="T880" id="Seg_14106" s="T873">They eat, the boy sends all people out to guard the herd.</ta>
            <ta e="T883" id="Seg_14107" s="T880">The child falls asleep.</ta>
            <ta e="T887" id="Seg_14108" s="T883">He hears the voices of people who are stalking.</ta>
            <ta e="T891" id="Seg_14109" s="T887">The human slightly opens the door with a glaive: </ta>
            <ta e="T897" id="Seg_14110" s="T891">"Why are you guarding me, guard the herd!"</ta>
            <ta e="T900" id="Seg_14111" s="T897">The people hid back.</ta>
            <ta e="T904" id="Seg_14112" s="T900">"We will try again during the morning sleep!", they agree.</ta>
            <ta e="T906" id="Seg_14113" s="T904">They try again.</ta>
            <ta e="T909" id="Seg_14114" s="T906">Again: "Why are you guarding [me]?</ta>
            <ta e="T912" id="Seg_14115" s="T909">Go!", he sends them away.</ta>
            <ta e="T914" id="Seg_14116" s="T912">They didn't try anymore.</ta>
            <ta e="T920" id="Seg_14117" s="T914">By the end of the second month the crest ended.</ta>
            <ta e="T923" id="Seg_14118" s="T920">He saw the tundra beginning there.</ta>
            <ta e="T926" id="Seg_14119" s="T923">It was smoking from under the snow.</ta>
            <ta e="T932" id="Seg_14120" s="T926">A broad path goes from the Tundra.</ta>
            <ta e="T937" id="Seg_14121" s="T932">That path goes further inside of the land.</ta>
            <ta e="T939" id="Seg_14122" s="T937">They go.</ta>
            <ta e="T946" id="Seg_14123" s="T939">They come to the land, where people settle tightly, where tents stand tightly.</ta>
            <ta e="T950" id="Seg_14124" s="T946">He stops at one tent.</ta>
            <ta e="T955" id="Seg_14125" s="T950">One very old woman goes out: </ta>
            <ta e="T959" id="Seg_14126" s="T955">"Jeez, what a wonderful reindeer he has!</ta>
            <ta e="T963" id="Seg_14127" s="T959">Like the reindeers of our ancestors, isn't it?!</ta>
            <ta e="T970" id="Seg_14128" s="T963">Long ago two humans went off, these must be their descendants!"</ta>
            <ta e="T981" id="Seg_14129" s="T970">Three women came out of three tents and recognized their relatives, they talk and get to know each other. </ta>
            <ta e="T983" id="Seg_14130" s="T981">They find out their kinship.</ta>
            <ta e="T985" id="Seg_14131" s="T983">They wait for Kungadyay.</ta>
            <ta e="T990" id="Seg_14132" s="T985">In the middle of the third day Kungadyay comes.</ta>
            <ta e="T997" id="Seg_14133" s="T990">There was no intact spot on his body, only the half of the skin stayed.</ta>
            <ta e="T1001" id="Seg_14134" s="T997">"My child, you came, fine then!"</ta>
            <ta e="T1008" id="Seg_14135" s="T1001">"I killed bad people, but I could hardly win over the (wild?) animals.</ta>
            <ta e="T1013" id="Seg_14136" s="T1008">They wounded me, but I come alive".</ta>
            <ta e="T1018" id="Seg_14137" s="T1013">"Well, now live and don't be afraid of anybody!"</ta>
            <ta e="T1022" id="Seg_14138" s="T1018">Kungadyay revives later on.</ta>
            <ta e="T1028" id="Seg_14139" s="T1022">The boy became the leader of this people.</ta>
            <ta e="T1029" id="Seg_14140" s="T1028">The end.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_14141" s="T0">Es lebten einmal drei Brüder.</ta>
            <ta e="T14" id="Seg_14142" s="T3">Der Jüngste von ihnen ist noch ganz klein, der Mittlere kriecht nur auf seinen Knieen und heißt Kungadjaj.</ta>
            <ta e="T22" id="Seg_14143" s="T14">Der älteste Bruder hat keinen Namen, er ist Jäger, hat eine Frau und ein kleines Kind.</ta>
            <ta e="T26" id="Seg_14144" s="T22">Ihr Rentier ist sehr einsam, [es ist] wie ein Pferd.</ta>
            <ta e="T32" id="Seg_14145" s="T26">Sie spannen es nie an, nur wenn sie nomadisieren, lassen sie es ihr Zelt ziehen.</ta>
            <ta e="T35" id="Seg_14146" s="T32">Der älteste Bruder ist ein bekannter Jäger.</ta>
            <ta e="T39" id="Seg_14147" s="T35">Er jagt wilde Rentiere und fischt auch.</ta>
            <ta e="T52" id="Seg_14148" s="T39">Als es Frühling wird, wird der älteste Bruder krank, er wurde kränker, am dritten Tag war er ganz schwach geworden, er sagte seiner Frau sein Vermächtnis: </ta>
            <ta e="T55" id="Seg_14149" s="T52">"Nun, ich sterbe.</ta>
            <ta e="T57" id="Seg_14150" s="T55">Wer wird sich [um dich] kümmern?!</ta>
            <ta e="T62" id="Seg_14151" s="T57">Meine Jagdbeute esst ihr über den Sommer.</ta>
            <ta e="T70" id="Seg_14152" s="T62">Wenn du das Essen aufgebraucht hast, dann schabe im kommenden Jahr den Rest Fleisch vom Leder ab und koche es.</ta>
            <ta e="T75" id="Seg_14153" s="T70">Die Familie hier ist nutzlos.</ta>
            <ta e="T77" id="Seg_14154" s="T75">Du stirbst dann auch.</ta>
            <ta e="T84" id="Seg_14155" s="T77">Mein Kind bleibt vielleicht am Leben, wenn es nicht stirbt, dann wird es dich ernähren.</ta>
            <ta e="T85" id="Seg_14156" s="T84">Das ist alles."</ta>
            <ta e="T87" id="Seg_14157" s="T85">Er stirbt.</ta>
            <ta e="T91" id="Seg_14158" s="T87">Weinend begräbt die Frau ihn.</ta>
            <ta e="T94" id="Seg_14159" s="T91">Es wird Sommer.</ta>
            <ta e="T96" id="Seg_14160" s="T94">Sie trocknen sich ihre Nahrung.</ta>
            <ta e="T102" id="Seg_14161" s="T96">Sie überstehen den Sommer, im Herbst essen sie Rentierleder und Haut.</ta>
            <ta e="T104" id="Seg_14162" s="T102">Alles geht zu Ende.</ta>
            <ta e="T106" id="Seg_14163" s="T104">Es wird Winter.</ta>
            <ta e="T109" id="Seg_14164" s="T106">Es gibt nichts zu essen.</ta>
            <ta e="T116" id="Seg_14165" s="T109">An einem windstillen Tag hört sie das Geräusch eines Schlittens.</ta>
            <ta e="T124" id="Seg_14166" s="T116">Die Frau rennt hinaus, [jemand] hat an einem Ort angehalten, von dem nur die Stimme zu hören ist.</ta>
            <ta e="T133" id="Seg_14167" s="T124">Es ist offenbar der Vater der Frau, Herr von acht schwarzen Rentieren, Fürst.</ta>
            <ta e="T136" id="Seg_14168" s="T133">"Hey, wie lebt ihr?"</ta>
            <ta e="T139" id="Seg_14169" s="T136">"Mein Mann starb im letzten Jahr."</ta>
            <ta e="T141" id="Seg_14170" s="T139">"Na, Gott sei Dank!</ta>
            <ta e="T143" id="Seg_14171" s="T141">Ich bin sehr froh!</ta>
            <ta e="T147" id="Seg_14172" s="T143">Zieh dich an, fahren wir zu mir!"</ta>
            <ta e="T150" id="Seg_14173" s="T147">"Und mein Kind, meine Familie?"</ta>
            <ta e="T153" id="Seg_14174" s="T150">"Die sollen verhungern, gehen wir!"</ta>
            <ta e="T160" id="Seg_14175" s="T153">Die Frau zieht sich an, steigt auf den Schlitten und sie fahren davon.</ta>
            <ta e="T164" id="Seg_14176" s="T160">Kungadjaj [und sein Bruder] liegen wohl sehr hungrig da.</ta>
            <ta e="T168" id="Seg_14177" s="T164">Die Polarnacht geht vorüber, es wird wieder hell.</ta>
            <ta e="T171" id="Seg_14178" s="T168">Kungadjaj sagt zu seinem jüngeren Bruder: </ta>
            <ta e="T173" id="Seg_14179" s="T171">"Spann dein Rentier an.</ta>
            <ta e="T175" id="Seg_14180" s="T173">Wenn wir so hier liegen, sterben wir.</ta>
            <ta e="T178" id="Seg_14181" s="T175">Lass uns den acht schwarzen Rentieren folgen!"</ta>
            <ta e="T185" id="Seg_14182" s="T178">Sie spannen die Rentiere an, zerlegen ihr Zelt und fahren wohl gemeinsam los.</ta>
            <ta e="T188" id="Seg_14183" s="T185">Sie übernachteten einige Male.</ta>
            <ta e="T192" id="Seg_14184" s="T188">Einmal kletterten sie auf einen hohen bewaldeten Berg.</ta>
            <ta e="T206" id="Seg_14185" s="T192">Sie schauten: In der Mitte einer weiten Ebene sind viele Herden, auf der anderen Seite der Ebene sind siebzig Zelte zu sehen.</ta>
            <ta e="T212" id="Seg_14186" s="T206">Sie fuhren durch die Herde auf die andere Seite.</ta>
            <ta e="T215" id="Seg_14187" s="T212">"Halt!", sagte Kungadjaj.</ta>
            <ta e="T223" id="Seg_14188" s="T215">"Halte am besten von diesen Zelten an, [geh nicht] zum Platz auf jener Seite, wo Holz gehackt wird."</ta>
            <ta e="T228" id="Seg_14189" s="T223">Er sah, dass ein Haus größer als alle anderen war.</ta>
            <ta e="T235" id="Seg_14190" s="T228">Sie kamen am Eingang an und ließen ihre Rentiere los.</ta>
            <ta e="T239" id="Seg_14191" s="T235">Sie bauten das Zelt auf und zündeten ein Feuer an.</ta>
            <ta e="T242" id="Seg_14192" s="T239">Zum jüngeren Bruder: "Geh zur Schwägerin.</ta>
            <ta e="T245" id="Seg_14193" s="T242">Sie soll kommen und ihr Kind stillen.</ta>
            <ta e="T258" id="Seg_14194" s="T245">Sage zum [Herren] der acht schwarzen Rentiere: 'Wir sterben. Er soll die Hälfte eines Herzes und eine Leber mit dem Witwer teilen'", sagt er.</ta>
            <ta e="T262" id="Seg_14195" s="T258">Der Junge geht hinein und sagt es.</ta>
            <ta e="T266" id="Seg_14196" s="T262">"Schau, was für eine Unordnung, [es] noch zu stillen!</ta>
            <ta e="T270" id="Seg_14197" s="T266">Soll es verhungern!", sagt die Frau.</ta>
            <ta e="T283" id="Seg_14198" s="T270">"Großvater, mein älterer Bruder lässt mitteilen: 'Wir sterben und ihr sollt die Hälfte eines Herzes und eine Leber mit dem Witwer teilen'."</ta>
            <ta e="T286" id="Seg_14199" s="T283">"Was, verhungert!</ta>
            <ta e="T291" id="Seg_14200" s="T286">Ich gebe nichts!", sagt der Herr der acht schwarzen Rentiere.</ta>
            <ta e="T292" id="Seg_14201" s="T291">Sie legen sich schlafen.</ta>
            <ta e="T294" id="Seg_14202" s="T292">Was werden sie essen?!</ta>
            <ta e="T298" id="Seg_14203" s="T294">Plötzlich ist eine Stimme zu hören, die Rentiere treibt.</ta>
            <ta e="T303" id="Seg_14204" s="T298">Kungadjaj ging hinaus und fing an zu pinkeln, auf den Knien.</ta>
            <ta e="T306" id="Seg_14205" s="T303">Viele Rentiere kommen.</ta>
            <ta e="T311" id="Seg_14206" s="T306">Ein sehr fetter kastrierter Rentierbulle kommt.</ta>
            <ta e="T314" id="Seg_14207" s="T311">Der Junge fängt ihn.</ta>
            <ta e="T316" id="Seg_14208" s="T314">Jener wehrt sich sehr.</ta>
            <ta e="T318" id="Seg_14209" s="T316">Er lässt sein Messer holen.</ta>
            <ta e="T321" id="Seg_14210" s="T318">Er schneidet die Kehle durch.</ta>
            <ta e="T331" id="Seg_14211" s="T321">Ohne die Haut abzuziehen bringen sie es herein und kochen es, sie essen den ganzen Tag, bis zum Abend.</ta>
            <ta e="T337" id="Seg_14212" s="T331">Neben diesem Zelt steht ein kleineres Zelt.</ta>
            <ta e="T348" id="Seg_14213" s="T337">"Geh in das Zelt des Herrn der sieben Knöpfe (der Dorfälteste wahrscheinlich), bitte um etwas zu essen."</ta>
            <ta e="T355" id="Seg_14214" s="T348">Der Junge geht und überbringt dem Herrn der sieben Knöpfe die Mitteilung.</ta>
            <ta e="T360" id="Seg_14215" s="T355">"Gab denn der Herr der acht schwarzen Rentiere etwas?"</ta>
            <ta e="T361" id="Seg_14216" s="T360">"Nein."</ta>
            <ta e="T366" id="Seg_14217" s="T361">"Na, dann gebe ich auch nichts."</ta>
            <ta e="T367" id="Seg_14218" s="T366">Sie essen.</ta>
            <ta e="T371" id="Seg_14219" s="T367">Am Morgen geht die Herde wieder.</ta>
            <ta e="T375" id="Seg_14220" s="T371">Kungadjaj kriecht auf seinen Knien hinaus.</ta>
            <ta e="T381" id="Seg_14221" s="T375">Kungadjaj greift eine unfruchtbare Rentierkuh am Geweih.</ta>
            <ta e="T384" id="Seg_14222" s="T381">"Hey, nimm das Messer heraus!"</ta>
            <ta e="T386" id="Seg_14223" s="T384">Er tötet sie.</ta>
            <ta e="T389" id="Seg_14224" s="T386">Wieder essen sie, ohne das Fell abzuziehen.</ta>
            <ta e="T396" id="Seg_14225" s="T389">Er zeigte sich, dass er die [Rentiere] dieser Herren getötet hat.</ta>
            <ta e="T397" id="Seg_14226" s="T396">Sie legen sich schlafen.</ta>
            <ta e="T406" id="Seg_14227" s="T397">Am Morgen ging Kungadjaj hinaus, der Herr der acht schwarzen Rentiere saß da und schnitzte an seiner Lenkstange.</ta>
            <ta e="T411" id="Seg_14228" s="T406">"Ach, Herr der acht schwarzen Rentiere, hallo!"</ta>
            <ta e="T413" id="Seg_14229" s="T411">"Du sagst noch hallo!</ta>
            <ta e="T415" id="Seg_14230" s="T413">Heute töten wir euch.</ta>
            <ta e="T419" id="Seg_14231" s="T415">Ihr esst die letzten unserer Rentiere!"</ta>
            <ta e="T421" id="Seg_14232" s="T419">"Es ist eure eigene Schuld."</ta>
            <ta e="T434" id="Seg_14233" s="T421">Der Herr der acht schwarzen Rentiere zielt mit seinem Lenkstangenspeer auf die Hüfte des knieenden Menschen, aber er trifft nicht.</ta>
            <ta e="T438" id="Seg_14234" s="T434">Kungadjaj kroch unter den Schnee.</ta>
            <ta e="T445" id="Seg_14235" s="T438">Er kommt dort hinaus, wo der Herr der sieben Knöpfe sitzt und an seiner Lenkstange schnitzt.</ta>
            <ta e="T450" id="Seg_14236" s="T445">"Ach, Herr der sieben Knöpfe, hallo!"</ta>
            <ta e="T452" id="Seg_14237" s="T450">"Du sagst noch hallo!</ta>
            <ta e="T454" id="Seg_14238" s="T452">Heute töten wir euch.</ta>
            <ta e="T458" id="Seg_14239" s="T454">Ihr esst unsere letzten Rentiere!"</ta>
            <ta e="T460" id="Seg_14240" s="T458">"Es ist eure eigene Schuld."</ta>
            <ta e="T472" id="Seg_14241" s="T460">Der Herr der sieben Knöpfe wollte mit seinem Lenkstangenspeer in die Hüfte des knieenden Menschen stoßen, aber stieß in die Erde.</ta>
            <ta e="T484" id="Seg_14242" s="T472">Bald kommt Kungadjaj an der nächsten Stelle heraus und plötzlich springt er auf seine beiden Beine.</ta>
            <ta e="T488" id="Seg_14243" s="T484">Er schaute: Ein Schlitten bewegt sich.</ta>
            <ta e="T494" id="Seg_14244" s="T488">Die Schwägerin fährt davon, der Vater hatte sie verheiratet.</ta>
            <ta e="T499" id="Seg_14245" s="T494">Kungadjaj greift die Zügel der Schwägerin: </ta>
            <ta e="T500" id="Seg_14246" s="T499">"Ich lasse dich nicht.</ta>
            <ta e="T503" id="Seg_14247" s="T500">Wohin fährst du?"</ta>
            <ta e="T506" id="Seg_14248" s="T503">Die Frau fährt davon.</ta>
            <ta e="T512" id="Seg_14249" s="T506">Kungadjaj verfolgt sie und nimmt ihre Karawane weg.</ta>
            <ta e="T517" id="Seg_14250" s="T512">Kungadjaj geht zum Herren der acht schwarzen Rentiere hinein: </ta>
            <ta e="T526" id="Seg_14251" s="T517">"Nun, Herr der acht schwarzen Rentiere, solange wir in Frieden leben, kümmere dich um meine jüngeren Brüder.</ta>
            <ta e="T532" id="Seg_14252" s="T526">Wenn du sie schlecht behandelst, dann werde ich eines Tages alle vernichten.</ta>
            <ta e="T535" id="Seg_14253" s="T532">Ich gehe nach meiner Schwägerin."</ta>
            <ta e="T539" id="Seg_14254" s="T535">Er verfolgt die Schwägerin, aber er verliert sie.</ta>
            <ta e="T545" id="Seg_14255" s="T539">An irgendeinem entlegenen Ort sieht er sie, aber er verfolgt sie nicht, bis er sie erreicht.</ta>
            <ta e="T550" id="Seg_14256" s="T545">Er versperrt ihr den Weg und lässt sie sich umdrehen in Richtung der Siedlung.</ta>
            <ta e="T557" id="Seg_14257" s="T550">Obwohl er sie greifen kann, greift er sie nicht, sondern geht hinter der Schwägerin hinein.</ta>
            <ta e="T561" id="Seg_14258" s="T557">Die Schwägerin sitzt und stillt das Kind.</ta>
            <ta e="T564" id="Seg_14259" s="T561">Der jüngere Bruder sitzt und isst.</ta>
            <ta e="T568" id="Seg_14260" s="T564">"Nun, Schwägerin, du hast deine Schuldigkeit getan.</ta>
            <ta e="T573" id="Seg_14261" s="T568">Wenn du das Kind nicht genommen hättest, hätte ich [dich] getötet!</ta>
            <ta e="T578" id="Seg_14262" s="T573">Der Herr der acht schwarzen Rentiere hat Angst [vor ihm]."</ta>
            <ta e="T579" id="Seg_14263" s="T578">Sie leben.</ta>
            <ta e="T583" id="Seg_14264" s="T579">Irgendwann am dritten Tag: </ta>
            <ta e="T589" id="Seg_14265" s="T583">"Nun, Herr der acht schwarzen Rentiere, sammele deine Leute.</ta>
            <ta e="T592" id="Seg_14266" s="T589">Wir halten eine Versammlung ab!", sagt Kungadjaj.</ta>
            <ta e="T598" id="Seg_14267" s="T592">So sammeln sie die Leute aus siebzig Zelten.</ta>
            <ta e="T600" id="Seg_14268" s="T598">Kungadjaj sagt: </ta>
            <ta e="T611" id="Seg_14269" s="T600">"Ihr seid zwei Hausherren, der Herr der acht schwarzen Rentiere und der Herr der sieben Knöpfe.</ta>
            <ta e="T615" id="Seg_14270" s="T611">Jetzt hört eure Herrschaft auf.</ta>
            <ta e="T621" id="Seg_14271" s="T615">Euer Herr ist dieser Junge, der Sohn meines älteren Bruders.</ta>
            <ta e="T625" id="Seg_14272" s="T621">Sammelt euch alle in zwei Tagen.</ta>
            <ta e="T628" id="Seg_14273" s="T625">Am dritten Tag fahrt ihr weg.</ta>
            <ta e="T631" id="Seg_14274" s="T628">Er wird euch führen."</ta>
            <ta e="T642" id="Seg_14275" s="T631">"Spanne keine fremden Rentiere an, spanne die Rentiere an, die der Vater dir vermacht hat!", sagt er dem Jungen. (Der Junge war gewachsen.)</ta>
            <ta e="T644" id="Seg_14276" s="T642">"Du führst sie an, wenn ihr fahrt.</ta>
            <ta e="T658" id="Seg_14277" s="T644">Entlang des Waldes wird es einen Bergrücken geben, geht an der Tundraseite des Bergrückens entlang, zwei Monate wird das dauern.</ta>
            <ta e="T668" id="Seg_14278" s="T658">Am Ende, wenn der Bergrücken aufhört und in Tundra übergeht, dort wird es von unter dem Schnee rauchen.</ta>
            <ta e="T672" id="Seg_14279" s="T668">Das ist die Erde unserer Vorfahren.</ta>
            <ta e="T677" id="Seg_14280" s="T672">Du gehst auf einen großen Weg, du kommst zu deinen Verwandten.</ta>
            <ta e="T682" id="Seg_14281" s="T677">Leute, ihr werdet Tag und Nacht nach euren Rentieren sehen.</ta>
            <ta e="T690" id="Seg_14282" s="T682">Herr der acht schwarzen Rentiere, Herr der sieben Knöpfe, ihr werdet euch wieder treffen.</ta>
            <ta e="T703" id="Seg_14283" s="T690">Ich aber werde, damit ihr gut reisen könnt, die schlechten Leute, Tschangiten und Tiere vernichten.</ta>
            <ta e="T714" id="Seg_14284" s="T703">Ich werde an jenem Ort sein an dem Tag, an dem ihr ankommt, wenn ich mich sehr verspäte, komme ich am dritten Tag an!", sagt er.</ta>
            <ta e="T717" id="Seg_14285" s="T714">Am dritten Tag fahren sie los.</ta>
            <ta e="T722" id="Seg_14286" s="T717">Kungadjaj entfernt sich von ihnen, nur zu Fuß.</ta>
            <ta e="T728" id="Seg_14287" s="T722">Sie gingen mit diesem Kind als Anführer.</ta>
            <ta e="T735" id="Seg_14288" s="T728">Sie reisen und hüten [ihre Tiere], dieser Junge lässt alle, auch Herr und Herrin, hüten.</ta>
            <ta e="T738" id="Seg_14289" s="T735">Es kam die Mitte des Monats.</ta>
            <ta e="T741" id="Seg_14290" s="T738">Sie gingen entlang des Bergrückens.</ta>
            <ta e="T749" id="Seg_14291" s="T741">Plötzlich geht ein Bergrücken in Richtung der Tundra weg, darauf geht ein Mensch.</ta>
            <ta e="T752" id="Seg_14292" s="T749">"Woher stammst du?"</ta>
            <ta e="T754" id="Seg_14293" s="T752">"Was kommt?</ta>
            <ta e="T756" id="Seg_14294" s="T754">Ich kenne kein Zelt und kein Feuer.</ta>
            <ta e="T760" id="Seg_14295" s="T756">Ich gehe und suche meinen älteren Bruder."</ta>
            <ta e="T762" id="Seg_14296" s="T760">"Was für einer ist er?"</ta>
            <ta e="T765" id="Seg_14297" s="T762">"Er ist der Herr der acht schwarzen Rentiere!"</ta>
            <ta e="T766" id="Seg_14298" s="T765">"Ach ja?</ta>
            <ta e="T769" id="Seg_14299" s="T766">Das ist mein Mensch.</ta>
            <ta e="T773" id="Seg_14300" s="T769">Er treibt freilaufende Hausrentiere.</ta>
            <ta e="T778" id="Seg_14301" s="T773">Aber dann werde doch mein Mensch."</ta>
            <ta e="T780" id="Seg_14302" s="T778">Jener antwortet nicht.</ta>
            <ta e="T796" id="Seg_14303" s="T780">"Wenn du nicht mein Diener wirst, dann wirst du zur Verzierung einer der drei Spitzen meiner Lenkstange, zwei Stück haben schon Verzierungen von Helden."</ta>
            <ta e="T799" id="Seg_14304" s="T796">Der Mensch ist einverstanden und kommt mit.</ta>
            <ta e="T801" id="Seg_14305" s="T799">Sie fuhren weiter.</ta>
            <ta e="T803" id="Seg_14306" s="T801">Der Monat ging zuende.</ta>
            <ta e="T807" id="Seg_14307" s="T803">Es kam die Mitte des nächsten Monats.</ta>
            <ta e="T812" id="Seg_14308" s="T807">Sie sahen wieder einen Menschen auf einem Bergrücken.</ta>
            <ta e="T814" id="Seg_14309" s="T812">Er begrüßt sie auch.</ta>
            <ta e="T818" id="Seg_14310" s="T814">"In welche Richtung ist dein Zelt und dein Feuer?"</ta>
            <ta e="T820" id="Seg_14311" s="T818">"Ich habe kein Zelt und kein Feuer.</ta>
            <ta e="T822" id="Seg_14312" s="T820">Ich gehe nur.</ta>
            <ta e="T824" id="Seg_14313" s="T822">Ich suche meinen älteren Bruder."</ta>
            <ta e="T827" id="Seg_14314" s="T824">"Was war dein Bruder?"</ta>
            <ta e="T831" id="Seg_14315" s="T827">"Ach, der Herr der sieben Knöpfe!"</ta>
            <ta e="T837" id="Seg_14316" s="T831">"Ach, dein Bruder treibt freilaufende Rentiere da.</ta>
            <ta e="T841" id="Seg_14317" s="T837">Vielleicht wirst du mein Mensch?"</ta>
            <ta e="T842" id="Seg_14318" s="T841">Er sagt nichts.</ta>
            <ta e="T849" id="Seg_14319" s="T842">"Wenn du es nicht wirst, dann mache ich dich zu Verzierung auf einer Spitze meiner Lenkstange!"</ta>
            <ta e="T852" id="Seg_14320" s="T849">Da willigt dieser Mensch ein.</ta>
            <ta e="T854" id="Seg_14321" s="T852">Er kommt zu seinem älteren Bruder: </ta>
            <ta e="T856" id="Seg_14322" s="T854">"Warum bringt ihr [ihn] nicht um?</ta>
            <ta e="T860" id="Seg_14323" s="T856">Gehorcht ihr etwa einem Kind?"</ta>
            <ta e="T862" id="Seg_14324" s="T860">"Nein, lass es."</ta>
            <ta e="T864" id="Seg_14325" s="T862">"Man muss ihn umbringen.</ta>
            <ta e="T867" id="Seg_14326" s="T864">Das ist doch peinlich."</ta>
            <ta e="T869" id="Seg_14327" s="T867">"Gut, wir versuchen es!"</ta>
            <ta e="T873" id="Seg_14328" s="T869">Am Abend halten sie an einem Platz an.</ta>
            <ta e="T880" id="Seg_14329" s="T873">Sie essen, der Junge schickt alle Leute hinaus, um die Herde zu hüten.</ta>
            <ta e="T883" id="Seg_14330" s="T880">Das Kind schläft ein.</ta>
            <ta e="T887" id="Seg_14331" s="T883">Er hört die Stimmen von Menschen, die sich anschleichen.</ta>
            <ta e="T891" id="Seg_14332" s="T887">Der Mensch öffnet mit einer Glefe den Eingang etwas: </ta>
            <ta e="T897" id="Seg_14333" s="T891">"Warum schaut ihr nach mir, hütet die Herde!"</ta>
            <ta e="T900" id="Seg_14334" s="T897">Die Leute verstecken sich wieder.</ta>
            <ta e="T904" id="Seg_14335" s="T900">"Im morgendlichen Schlaf probieren wir es!", sprechen sie sich ab.</ta>
            <ta e="T906" id="Seg_14336" s="T904">Sie machen wieder einen Versuch.</ta>
            <ta e="T909" id="Seg_14337" s="T906">Wieder: "Warum schaut ihr [nach mir]?</ta>
            <ta e="T912" id="Seg_14338" s="T909">Geht!", schickt er sie weg.</ta>
            <ta e="T914" id="Seg_14339" s="T912">Da probierten sie es nicht mehr.</ta>
            <ta e="T920" id="Seg_14340" s="T914">Als die zwei Monate überstanden waren, war der Bergrücken zuende.</ta>
            <ta e="T923" id="Seg_14341" s="T920">Er sah, dass dort flache Tundra anfing.</ta>
            <ta e="T926" id="Seg_14342" s="T923">Von unter dem Schnee raucht es.</ta>
            <ta e="T932" id="Seg_14343" s="T926">Aus der Tundra kommt ein riesiger Pfad.</ta>
            <ta e="T937" id="Seg_14344" s="T932">In das Land hinein geht dieser Pfad.</ta>
            <ta e="T939" id="Seg_14345" s="T937">Sie gehen.</ta>
            <ta e="T946" id="Seg_14346" s="T939">Sie kommen in ein Land, wo Volk an Volk wohnt, wo Zelt an Zelt steht.</ta>
            <ta e="T950" id="Seg_14347" s="T946">Er hält an einem Zelt an.</ta>
            <ta e="T955" id="Seg_14348" s="T950">Es kommt eine sehr alte Frau heraus: </ta>
            <ta e="T959" id="Seg_14349" s="T955">"Ui, was für ein wundersames Rentier er hat!</ta>
            <ta e="T963" id="Seg_14350" s="T959">Doch wie die Rentiere der Vorfahren?!</ta>
            <ta e="T970" id="Seg_14351" s="T963">Vor langer Zeit gingen zwei Menschen fort, das sind wohl ihre Nachkommen!"</ta>
            <ta e="T981" id="Seg_14352" s="T970">Aus drei Zelten kamen drei Frauen heraus und erkannten ihre Verwandten, sie unterhalten sich und machen sich bekannt.</ta>
            <ta e="T983" id="Seg_14353" s="T981">Sie klären ihre Verwandtschaft.</ta>
            <ta e="T985" id="Seg_14354" s="T983">Sie warten auf Kungadjaj.</ta>
            <ta e="T990" id="Seg_14355" s="T985">In der Mitte des dritten Tages kommt Kungadjaj.</ta>
            <ta e="T997" id="Seg_14356" s="T990">Kein Fleck seines Körper war unversehrt, nur die Hälfte der Haut war übrig geblieben.</ta>
            <ta e="T1001" id="Seg_14357" s="T997">"Mein Kind, du bist gekommen, wie schön!"</ta>
            <ta e="T1008" id="Seg_14358" s="T1001">"Die schlechten Leute habe ich vernichtet, die (wilden?) Tiere konnte ich kaum besiegen.</ta>
            <ta e="T1013" id="Seg_14359" s="T1008">Sie haben mich verwundet, aber ich komme lebendig."</ta>
            <ta e="T1018" id="Seg_14360" s="T1013">"Nun lebe und habe vor nichts Angst!"</ta>
            <ta e="T1022" id="Seg_14361" s="T1018">Kungadjaj wird mit der Zeit wieder gesund.</ta>
            <ta e="T1028" id="Seg_14362" s="T1022">Der Junge wurde der Herr dieses Volkes.</ta>
            <ta e="T1029" id="Seg_14363" s="T1028">Ende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_14364" s="T0">Жили-были три брата.</ta>
            <ta e="T14" id="Seg_14365" s="T3">Младший совсем еще ребенок, средний, по имени Кунгаджай, на коленях только ползает.</ta>
            <ta e="T22" id="Seg_14366" s="T14">У старшего брата имени нет, он охотник, у него есть жена и грудный ребенок.</ta>
            <ta e="T26" id="Seg_14367" s="T22">Их олень одинокий, как конь.</ta>
            <ta e="T32" id="Seg_14368" s="T26">Совсем на нем не ездят, только когда кочуют, возят на нем покрышки чума.</ta>
            <ta e="T35" id="Seg_14369" s="T32">Старший — замечательный охотник.</ta>
            <ta e="T39" id="Seg_14370" s="T35">Промышляет диких оленей, также рыбачит.</ta>
            <ta e="T52" id="Seg_14371" s="T39">Когда год повернул к весне, старший брат занемог, становилось ему все хуже, на третий день совсем ослабел, жене завещал: </ta>
            <ta e="T55" id="Seg_14372" s="T52">"Вот я помираю.</ta>
            <ta e="T57" id="Seg_14373" s="T55">Кто будет заботиться [о тебе]?!</ta>
            <ta e="T62" id="Seg_14374" s="T57">Добытой мной дичи будете есть в течении лета.</ta>
            <ta e="T70" id="Seg_14375" s="T62">Когда продукты кончатся, зимой соскабливай мездру со шкур, камусов и отваривай в воде.</ta>
            <ta e="T75" id="Seg_14376" s="T70">Oт моих братьев пользы нет.</ta>
            <ta e="T77" id="Seg_14377" s="T75">Так и пропадешь.</ta>
            <ta e="T84" id="Seg_14378" s="T77">Может, сын человеком станет, если выживет, тогда он будет тебя содержать.</ta>
            <ta e="T85" id="Seg_14379" s="T84">Все."</ta>
            <ta e="T87" id="Seg_14380" s="T85">Вот умирает.</ta>
            <ta e="T91" id="Seg_14381" s="T87">Плача-рыдая, женщина хоронит его.</ta>
            <ta e="T94" id="Seg_14382" s="T91">Наступает лето.</ta>
            <ta e="T96" id="Seg_14383" s="T94">Сушат, вялят впрок пищу.</ta>
            <ta e="T102" id="Seg_14384" s="T96">Так проживают лето, осенью едят камусы и кожу оленья. </ta>
            <ta e="T104" id="Seg_14385" s="T102">Все кончается.</ta>
            <ta e="T106" id="Seg_14386" s="T104">Наступает зима.</ta>
            <ta e="T109" id="Seg_14387" s="T106">Есть стало нечего.</ta>
            <ta e="T116" id="Seg_14388" s="T109">Однажды в безветренный день слышит скрип саней.</ta>
            <ta e="T124" id="Seg_14389" s="T116">Женщина выскакивает, [кто-то] остановился вдали, едва голос его слышен.</ta>
            <ta e="T133" id="Seg_14390" s="T124">Оказывается, это отец женщины — Хозяин восьми черных оленей, князь.</ta>
            <ta e="T136" id="Seg_14391" s="T133">"Ну, как живете?"</ta>
            <ta e="T139" id="Seg_14392" s="T136">"Муж мой умер в прошлом году."</ta>
            <ta e="T141" id="Seg_14393" s="T139">"Да ну, слава богу!</ta>
            <ta e="T143" id="Seg_14394" s="T141">Очень я рад!</ta>
            <ta e="T147" id="Seg_14395" s="T143">Одевайся, поедем к вам!"</ta>
            <ta e="T150" id="Seg_14396" s="T147">"А ребенок, родственники мои?"</ta>
            <ta e="T153" id="Seg_14397" s="T150">"Пусть помирают от голода, поехали!"</ta>
            <ta e="T160" id="Seg_14398" s="T153">Женщина, одевшись, садится на его нарты — они мчаются.</ta>
            <ta e="T164" id="Seg_14399" s="T160">Кунгаджай с братом совсем голодные лежат.</ta>
            <ta e="T168" id="Seg_14400" s="T164">[Полярная] ночь кончается, к свету идет.</ta>
            <ta e="T171" id="Seg_14401" s="T168">Кунгаджай говорит брату: </ta>
            <ta e="T173" id="Seg_14402" s="T171">"Запряги оленя.</ta>
            <ta e="T175" id="Seg_14403" s="T173">Так лежа, помрем.</ta>
            <ta e="T178" id="Seg_14404" s="T175">Поедем по следу восьми черных оленей!"</ta>
            <ta e="T185" id="Seg_14405" s="T178">Запрягают оленя, разбирают чум, садятся вместе и пойдут.</ta>
            <ta e="T188" id="Seg_14406" s="T185">Перекочевывали много раз.</ta>
            <ta e="T192" id="Seg_14407" s="T188">Однажды поднялись на высокий перевал.</ta>
            <ta e="T206" id="Seg_14408" s="T192">Осмотрелись: внизу видна широкая равнина, посреди равнины пасутся во множестве стада, за равниной стойбище видно из семидесяти чумов.</ta>
            <ta e="T212" id="Seg_14409" s="T206">Поехали через стада на ту сторону.</ta>
            <ta e="T215" id="Seg_14410" s="T212">"Стой!", сказал Кунгаджай.</ta>
            <ta e="T223" id="Seg_14411" s="T215">"Остановись у самого лучшего жилища, не доезжая до места, где рубят дрова."</ta>
            <ta e="T228" id="Seg_14412" s="T223">Увидел, что один чум больше других.</ta>
            <ta e="T235" id="Seg_14413" s="T228">Подъехали, остановились напротив полога да и отпустили своего оленя.</ta>
            <ta e="T239" id="Seg_14414" s="T235">Поставили чум, разожгли огонь [в очаге].</ta>
            <ta e="T242" id="Seg_14415" s="T239">Младшему брату: "Иди к жене брата.</ta>
            <ta e="T245" id="Seg_14416" s="T242">Пусть придет и покормит своего ребенка.</ta>
            <ta e="T258" id="Seg_14417" s="T245">Хозяину восьми черных оленей передай: 'пропадаем, пусть поделится хотя бы половиной [оленьего] сердца и придатком печени'", говорит.</ta>
            <ta e="T262" id="Seg_14418" s="T258">Мальчик вбежал [в тот чум] и все передал.</ta>
            <ta e="T266" id="Seg_14419" s="T262">"Смотри-ка, еще дать [ребенку] грудь пососать!</ta>
            <ta e="T270" id="Seg_14420" s="T266">Пусть подыхает!", говорит женщина.</ta>
            <ta e="T283" id="Seg_14421" s="T270">"Дедушка, брат просил сказать: 'помираем, поделитесь хотя бы половиной оленьего сердца и придатком печени'."</ta>
            <ta e="T286" id="Seg_14422" s="T283">"Еще что, подыхайте.</ta>
            <ta e="T291" id="Seg_14423" s="T286">Не дам!", говорит Хозяин восьми черных оленей.</ta>
            <ta e="T292" id="Seg_14424" s="T291">Ложатся спать.</ta>
            <ta e="T294" id="Seg_14425" s="T292">Есть то им что же?!</ta>
            <ta e="T298" id="Seg_14426" s="T294">Вдруг слышится голос, погоняющий оленей.</ta>
            <ta e="T303" id="Seg_14427" s="T298">Кунгаджай выполз и стал мочиться, на коленях.</ta>
            <ta e="T306" id="Seg_14428" s="T303">Много оленей приходит.</ta>
            <ta e="T311" id="Seg_14429" s="T306">Приходит и очень жирный холощеный самец.</ta>
            <ta e="T314" id="Seg_14430" s="T311">Парень хватает его.</ta>
            <ta e="T316" id="Seg_14431" s="T314">Тот рвется.</ta>
            <ta e="T318" id="Seg_14432" s="T316">Велит вынести ему нож.</ta>
            <ta e="T321" id="Seg_14433" s="T318">Одним взмахом перерезал глотку [оленя].</ta>
            <ta e="T331" id="Seg_14434" s="T321">Не снимая шкуры, втаскивают, варят и едят целый день, до самого вечера.</ta>
            <ta e="T337" id="Seg_14435" s="T331">Около того чума стоял чум поменьше.</ta>
            <ta e="T348" id="Seg_14436" s="T337">"Давай-ка иди в тот чум к Хозяину семи пуговиц (старшина, наверное?!), попроси чего-нибудь на варево."</ta>
            <ta e="T355" id="Seg_14437" s="T348">Мальчик идет и передает Хозяину семи пуговиц сообщение.</ta>
            <ta e="T360" id="Seg_14438" s="T355">"Хозяин восьми черных оленей, наверное, дал что-нибудь?"</ta>
            <ta e="T361" id="Seg_14439" s="T360">"Нет."</ta>
            <ta e="T366" id="Seg_14440" s="T361">"Ну, тогда я тоже не дам."</ta>
            <ta e="T367" id="Seg_14441" s="T366">Едят.</ta>
            <ta e="T371" id="Seg_14442" s="T367">Утром опять идет стадо.</ta>
            <ta e="T375" id="Seg_14443" s="T371">Кунгаджай выползает на коленях.</ta>
            <ta e="T381" id="Seg_14444" s="T375">Кунгаджай хватает за рога яловую важенку.</ta>
            <ta e="T384" id="Seg_14445" s="T381">"А ну, вынеси нож!"</ta>
            <ta e="T386" id="Seg_14446" s="T384">Забивает.</ta>
            <ta e="T389" id="Seg_14447" s="T386">Опять, не снимая шкуры, едят.</ta>
            <ta e="T396" id="Seg_14448" s="T389">Оказывается, он убил оленей тех хозяев.</ta>
            <ta e="T397" id="Seg_14449" s="T396">Ложатся спать.</ta>
            <ta e="T406" id="Seg_14450" s="T397">Когда утром Кунгаджай вышел, Хозяин восьми черных [оленей] сидел и подстругивал свой хорей.</ta>
            <ta e="T411" id="Seg_14451" s="T406">"О-о, Хозяин восьми черных [оленей], здравствуй!"</ta>
            <ta e="T413" id="Seg_14452" s="T411">"Еще здоровается!</ta>
            <ta e="T415" id="Seg_14453" s="T413">Сегодня мы убьем вас.</ta>
            <ta e="T419" id="Seg_14454" s="T415">Последних наших оленей поедаете!"</ta>
            <ta e="T421" id="Seg_14455" s="T419">"Сами виноваты."</ta>
            <ta e="T434" id="Seg_14456" s="T421">Хозяин восьми черных [оленей] острым наконечникам хорея нацеливается в бедро человека, стоящего на коленях, но промахивается.</ta>
            <ta e="T438" id="Seg_14457" s="T434">Кунгаджай скользнул под сугроб.</ta>
            <ta e="T445" id="Seg_14458" s="T438">И выскакивает там, где Хозяин семи пуговиц сидит и подстругивает хорей.</ta>
            <ta e="T450" id="Seg_14459" s="T445">"О-о, Хозяин семи пуговиц, здравствуй!"</ta>
            <ta e="T452" id="Seg_14460" s="T450">"Еще здоровается!</ta>
            <ta e="T454" id="Seg_14461" s="T452">Сегодня мы убьем вас.</ta>
            <ta e="T458" id="Seg_14462" s="T454">Последних наших оленей поедаете!"</ta>
            <ta e="T460" id="Seg_14463" s="T458">"Сами виноваты."</ta>
            <ta e="T472" id="Seg_14464" s="T460">Хозяин семи пуговиц попытался ударить острым концом хорея в бедро человека, стоящего на коленях, но вонзил его в землю.</ta>
            <ta e="T484" id="Seg_14465" s="T472">Вскоре Кунгаджай выскакивает в другом месте и вдруг вскакивает на обе ноги.</ta>
            <ta e="T488" id="Seg_14466" s="T484">Огляделся: нарты трогаются.</ta>
            <ta e="T494" id="Seg_14467" s="T488">Откочевывает их невестка, отец ее замуж выдал.</ta>
            <ta e="T499" id="Seg_14468" s="T494">Кунгаджай выхвативает поводья невестки: </ta>
            <ta e="T500" id="Seg_14469" s="T499">"Не пущу.</ta>
            <ta e="T503" id="Seg_14470" s="T500">Куда едешь?"</ta>
            <ta e="T506" id="Seg_14471" s="T503">Женщина мчается.</ta>
            <ta e="T512" id="Seg_14472" s="T506">Кунгаджай догоняет и отнимает ее запасных оленей.</ta>
            <ta e="T517" id="Seg_14473" s="T512">Входит Кунгаджай к Хозяину восьми черных [оленей]: </ta>
            <ta e="T526" id="Seg_14474" s="T517">"Ну, Хозяин восьми черных [оленей], пока мы в мире, ухаживай за моими младшими братьями.</ta>
            <ta e="T532" id="Seg_14475" s="T526">Если будешь плохо заботиться, за один день всех уничтожу.</ta>
            <ta e="T535" id="Seg_14476" s="T532">Я же иду догонять жену старшего брата."</ta>
            <ta e="T539" id="Seg_14477" s="T535">Гонится за женой брата, ‎‎но теряет ее след.</ta>
            <ta e="T545" id="Seg_14478" s="T539">Наконец обнаружил ее в конце какой-то местности, но не спешит догнать.</ta>
            <ta e="T550" id="Seg_14479" s="T545">Обегает и заставляет ее завернуть упряжку в сторону стойбища.</ta>
            <ta e="T557" id="Seg_14480" s="T550">Хотя может схватить ее, не хватает, а входит в чум вслед за ней.</ta>
            <ta e="T561" id="Seg_14481" s="T557">Жена брата сидит и кормит грудью ребенка.</ta>
            <ta e="T564" id="Seg_14482" s="T561">Младший сидит кушает.</ta>
            <ta e="T568" id="Seg_14483" s="T564">"Ну, жена брата, вину твою прощаю.</ta>
            <ta e="T573" id="Seg_14484" s="T568">Если б не взяла ребенка, убил бы!</ta>
            <ta e="T578" id="Seg_14485" s="T573">Хозяин восьми черных [оленей] боится его."</ta>
            <ta e="T579" id="Seg_14486" s="T578">Они живут.</ta>
            <ta e="T583" id="Seg_14487" s="T579">Как-то на третий день: </ta>
            <ta e="T589" id="Seg_14488" s="T583">"Ну, Хозяин восьми черных [оленей], собери свой народ.</ta>
            <ta e="T592" id="Seg_14489" s="T589">Устроим собрание!", говорит Кунгаджай.</ta>
            <ta e="T598" id="Seg_14490" s="T592">Вот собирают людей из семидесяти чумов.</ta>
            <ta e="T600" id="Seg_14491" s="T598">Кунгаджай говорит: </ta>
            <ta e="T611" id="Seg_14492" s="T600">"Вот вы тут два хосяина — Хозяин восьми черных [оленей] и Хозяин семи пуговиц.</ta>
            <ta e="T615" id="Seg_14493" s="T611">Пришел конец вашему хозяйничанию.</ta>
            <ta e="T621" id="Seg_14494" s="T615">Теперь хозяин — этот мальчик, сын моего старшего брата.</ta>
            <ta e="T625" id="Seg_14495" s="T621">Все соберитесь за два дня.</ta>
            <ta e="T628" id="Seg_14496" s="T625">На третий день откочуете.</ta>
            <ta e="T631" id="Seg_14497" s="T628">Он поведет вас."</ta>
            <ta e="T642" id="Seg_14498" s="T631">— Не езди на чужих оленях, езди только на олене, завещанном отцом!", говорит парню. (Мальчик уже вырос.)</ta>
            <ta e="T644" id="Seg_14499" s="T642">"Возглавишь кочевку.</ta>
            <ta e="T658" id="Seg_14500" s="T644">По краю леса тянется хребет, кочуй вдоль хребта со стороны тундры, в пути пробудешь два месяца.</ta>
            <ta e="T668" id="Seg_14501" s="T658">К концу кочевки хребет перейдет в тундру, там будет дым, поднимающийся из-под снега.</ta>
            <ta e="T672" id="Seg_14502" s="T668">Это земля наших предков.</ta>
            <ta e="T677" id="Seg_14503" s="T672">Выедешь на большую дорогу, доедешь до своих родственников.</ta>
            <ta e="T682" id="Seg_14504" s="T677">Люди, вы должны охранять стада и днем и ночью.</ta>
            <ta e="T690" id="Seg_14505" s="T682">Хозяин восьми черных [оленей], Хозяин семи пуговиц, будете опять встречаться.</ta>
            <ta e="T703" id="Seg_14506" s="T690">А я пойду уничтожать худых людей — чангитов и хищных зверей, чтобы вы кочевали благополучно.</ta>
            <ta e="T714" id="Seg_14507" s="T703">В ту землю прибуду в день твоего приезда, если очень задержусь, приду на третий день!", говорит.</ta>
            <ta e="T717" id="Seg_14508" s="T714">На третий день откочевывают.</ta>
            <ta e="T722" id="Seg_14509" s="T717">Кунгаджай уходит от них один, вовсе пешком.</ta>
            <ta e="T728" id="Seg_14510" s="T722">Вот во главе с этим мальчиком кочевали.</ta>
            <ta e="T735" id="Seg_14511" s="T728">В пути держаются сторожко, этот мальчик заставляет всех сторожить стада, даже господ.</ta>
            <ta e="T738" id="Seg_14512" s="T735">Прошло полмесяца.</ta>
            <ta e="T741" id="Seg_14513" s="T738">Едут вдоль хребта.</ta>
            <ta e="T749" id="Seg_14514" s="T741">Вдруг заметили хребет, ответвляющийся в сторону моря, на нем человек ходит.</ta>
            <ta e="T752" id="Seg_14515" s="T749">"Откуда ты родом?"</ta>
            <ta e="T754" id="Seg_14516" s="T752">"Не знаю.</ta>
            <ta e="T756" id="Seg_14517" s="T754">Нет у меня родного очага.</ta>
            <ta e="T760" id="Seg_14518" s="T756">Хожу и ищу родного старшего брата."</ta>
            <ta e="T762" id="Seg_14519" s="T760">"Кто он такой?"</ta>
            <ta e="T765" id="Seg_14520" s="T762">"Хозяин восьми черных [оленей]!"</ta>
            <ta e="T766" id="Seg_14521" s="T765">"Да ну!</ta>
            <ta e="T769" id="Seg_14522" s="T766">Он мой человек.</ta>
            <ta e="T773" id="Seg_14523" s="T769">Вон гонит свободное стадо.</ta>
            <ta e="T778" id="Seg_14524" s="T773">Раз такое дело, то иди ко мне работником."</ta>
            <ta e="T780" id="Seg_14525" s="T778">Тот медлит с ответом.</ta>
            <ta e="T796" id="Seg_14526" s="T780">"Если не пойдешь ко мне работником, станешь побрякушками на одной из трех развилок моего хорея, на двух уже висят побрякушки из черепов богатырей."</ta>
            <ta e="T799" id="Seg_14527" s="T796">Человек покорился, пошел с ними.</ta>
            <ta e="T801" id="Seg_14528" s="T799">Кочевали дальше.</ta>
            <ta e="T803" id="Seg_14529" s="T801">Месяц прошел.</ta>
            <ta e="T807" id="Seg_14530" s="T803">Наступила середина следующего месяца.</ta>
            <ta e="T812" id="Seg_14531" s="T807">На одном хребте опять увидели человека.</ta>
            <ta e="T814" id="Seg_14532" s="T812">Он тоже здоровается.</ta>
            <ta e="T818" id="Seg_14533" s="T814">"В какой стороне твой чум и очаг?"</ta>
            <ta e="T820" id="Seg_14534" s="T818">"Очага-чума не имею.</ta>
            <ta e="T822" id="Seg_14535" s="T820">Хожу вот.</ta>
            <ta e="T824" id="Seg_14536" s="T822">Старшего брата ищу."</ta>
            <ta e="T827" id="Seg_14537" s="T824">"Кем был твой брат?"</ta>
            <ta e="T831" id="Seg_14538" s="T827">"Э-э, он Хозяин семи пуговиц!"</ta>
            <ta e="T837" id="Seg_14539" s="T831">"Да ну, вон твой брат гонит свободное стадо.</ta>
            <ta e="T841" id="Seg_14540" s="T837">Может, ко мне пойдешь работником?"</ta>
            <ta e="T842" id="Seg_14541" s="T841">Молчит.</ta>
            <ta e="T849" id="Seg_14542" s="T842">"Если не будешь, сделаю из тебя побрякушки на одной из развилок хорея!"</ta>
            <ta e="T852" id="Seg_14543" s="T849">Тот человек соглашается.</ta>
            <ta e="T854" id="Seg_14544" s="T852">Подходит к брату: </ta>
            <ta e="T856" id="Seg_14545" s="T854">"Почему не убьете?</ta>
            <ta e="T860" id="Seg_14546" s="T856">Неужели ребенку подчинились?"</ta>
            <ta e="T862" id="Seg_14547" s="T860">"Нет, не надо."</ta>
            <ta e="T864" id="Seg_14548" s="T862">"Надо убить.</ta>
            <ta e="T867" id="Seg_14549" s="T864">Стыдно ведь."</ta>
            <ta e="T869" id="Seg_14550" s="T867">"Ну попробуем!"</ta>
            <ta e="T873" id="Seg_14551" s="T869">Вечером останавливаются в одном месте.</ta>
            <ta e="T880" id="Seg_14552" s="T873">Едят, мальчик всех людей гонит сторожить стада.</ta>
            <ta e="T883" id="Seg_14553" s="T880">Мальчик усыпает тут же.</ta>
            <ta e="T887" id="Seg_14554" s="T883">Слышит шум подкрадывающихся людей.</ta>
            <ta e="T891" id="Seg_14555" s="T887">Он приоткрывает пальмой полог: </ta>
            <ta e="T897" id="Seg_14556" s="T891">"Зачем меня стережете, охраняйте стадо!"</ta>
            <ta e="T900" id="Seg_14557" s="T897">Люди обратно скрылись.</ta>
            <ta e="T904" id="Seg_14558" s="T900">"Под утренний сон попробуем!", сговариваются.</ta>
            <ta e="T906" id="Seg_14559" s="T904">Снова делают попытку.</ta>
            <ta e="T909" id="Seg_14560" s="T906">Опять: "Чего стережете?</ta>
            <ta e="T912" id="Seg_14561" s="T909">Уходитe!", прогоняет.</ta>
            <ta e="T914" id="Seg_14562" s="T912">Больше не стали пытаться.</ta>
            <ta e="T920" id="Seg_14563" s="T914">К концу второго месяца хребет кончился.</ta>
            <ta e="T923" id="Seg_14564" s="T920">Увидел: начинается тундра.</ta>
            <ta e="T926" id="Seg_14565" s="T923">Из-под снега клубится дым.</ta>
            <ta e="T932" id="Seg_14566" s="T926">От тундры тянется широкая дорога.</ta>
            <ta e="T937" id="Seg_14567" s="T932">Уходит дорога дальше в глубь этого края.</ta>
            <ta e="T939" id="Seg_14568" s="T937">Едут.</ta>
            <ta e="T946" id="Seg_14569" s="T939">Прибыли в страну, где народ к народу селится, чум к чуму теснится.</ta>
            <ta e="T950" id="Seg_14570" s="T946">Останавливается у одного чума.</ta>
            <ta e="T955" id="Seg_14571" s="T950">Выходит пожилая женщина: </ta>
            <ta e="T959" id="Seg_14572" s="T955">— Да ну, какой у него удивительный олень!</ta>
            <ta e="T963" id="Seg_14573" s="T959">Ведь похож на оленей наших предков?!</ta>
            <ta e="T970" id="Seg_14574" s="T963">В старину уехали от нас два человека, это, наверное, их потомки."</ta>
            <ta e="T981" id="Seg_14575" s="T970">Из трех чумов вышли три женщины и тоже признали, что это их сородичи, разговаривают, знакомятся.</ta>
            <ta e="T983" id="Seg_14576" s="T981">Выясняют свое родство.</ta>
            <ta e="T985" id="Seg_14577" s="T983">Ждут Кунгаджая.</ta>
            <ta e="T990" id="Seg_14578" s="T985">К полудню третьего дня приходит Кунгаджай.</ta>
            <ta e="T997" id="Seg_14579" s="T990">Живого места не осталось, изрублен так, что половина кожи осталась.</ta>
            <ta e="T1001" id="Seg_14580" s="T997">"Дитя мое, добрался ты — вот хорошо!"</ta>
            <ta e="T1008" id="Seg_14581" s="T1001">Одолел худых людей, а хищных зверей с трудом осилил.</ta>
            <ta e="T1013" id="Seg_14582" s="T1008">Изранили меня, чуть живой пришел."</ta>
            <ta e="T1018" id="Seg_14583" s="T1013">"Теперь живи и никого не бойся!"</ta>
            <ta e="T1022" id="Seg_14584" s="T1018">Кунгаджай со временем выздоровел.</ta>
            <ta e="T1028" id="Seg_14585" s="T1022">Парень стал вождем этого народа.</ta>
            <ta e="T1029" id="Seg_14586" s="T1028">Конец.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_14587" s="T0">Жили-были три брата.</ta>
            <ta e="T14" id="Seg_14588" s="T3">Младший совсем еще ребенок, средний, по имени Кунгаджай, на коленях только ползает.</ta>
            <ta e="T22" id="Seg_14589" s="T14">Имя старшего неизвестно, он был охотник, имел жену и грудного ребенка.</ta>
            <ta e="T26" id="Seg_14590" s="T22">У них был один-единственный олень, [большой,] как конь.</ta>
            <ta e="T32" id="Seg_14591" s="T26">Совсем на нем не ездят, только когда кочуют, возят на нем покрышки чума.</ta>
            <ta e="T35" id="Seg_14592" s="T32">Старший — замечательный охотник.</ta>
            <ta e="T39" id="Seg_14593" s="T35">Промышляет диких оленей, также рыбачит.</ta>
            <ta e="T52" id="Seg_14594" s="T39">Когда год повернул к весне, старший брат занемог, становилось ему все хуже, на третий день совсем ослабел, жене завещал: </ta>
            <ta e="T55" id="Seg_14595" s="T52">— Вот я помираю.</ta>
            <ta e="T57" id="Seg_14596" s="T55">Кто будет заботиться о тебе?!</ta>
            <ta e="T62" id="Seg_14597" s="T57">Добытой мной дичи хватит на все лето.</ta>
            <ta e="T70" id="Seg_14598" s="T62">Когда продукты кончатся, зимой соскабливай мездру со шкур, камусов и отваривай в воде.</ta>
            <ta e="T75" id="Seg_14599" s="T70">Oт моих братьев пользы нет.</ta>
            <ta e="T77" id="Seg_14600" s="T75">Так и пропадешь.</ta>
            <ta e="T84" id="Seg_14601" s="T77">Может, сын человеком станет, если выживет, тогда он будет тебя содержать.</ta>
            <ta e="T85" id="Seg_14602" s="T84">Все.</ta>
            <ta e="T87" id="Seg_14603" s="T85">Вот умирает.</ta>
            <ta e="T91" id="Seg_14604" s="T87">Плача-рыдая, женщина хоронит его.</ta>
            <ta e="T94" id="Seg_14605" s="T91">Наступает лето.</ta>
            <ta e="T96" id="Seg_14606" s="T94">Сушат, вялят впрок пищу.</ta>
            <ta e="T102" id="Seg_14607" s="T96">Так прожили лето, осенью стали питаться, отваривая кусочки кожи и камусов.</ta>
            <ta e="T104" id="Seg_14608" s="T102">Все кончается.</ta>
            <ta e="T106" id="Seg_14609" s="T104">Наступила зима.</ta>
            <ta e="T109" id="Seg_14610" s="T106">Есть стало нечего.</ta>
            <ta e="T116" id="Seg_14611" s="T109">Однажды в безветренный день послышался скрип саней.</ta>
            <ta e="T124" id="Seg_14612" s="T116">Женщина выскакивает, [кто-то] остановился вдали, едва голос его слышен.</ta>
            <ta e="T133" id="Seg_14613" s="T124">Оказывается, это отец женщины — Хозяин восьми черных оленей, князь.</ta>
            <ta e="T136" id="Seg_14614" s="T133">— Ну, как живете?</ta>
            <ta e="T139" id="Seg_14615" s="T136">— Муж мой умер в прошлом году.</ta>
            <ta e="T141" id="Seg_14616" s="T139">— Да ну, слава богу!</ta>
            <ta e="T143" id="Seg_14617" s="T141">Очень я рад!</ta>
            <ta e="T147" id="Seg_14618" s="T143">Одевайся, поедем к вам!</ta>
            <ta e="T150" id="Seg_14619" s="T147">— А ребенок, родственники мои?</ta>
            <ta e="T153" id="Seg_14620" s="T150">— Пусть помирают от голода, поехали!</ta>
            <ta e="T160" id="Seg_14621" s="T153">Женщина, одевшись, села на его нарты — они умчались.</ta>
            <ta e="T164" id="Seg_14622" s="T160">Кунгаджай с братом совсем голодные лежат.</ta>
            <ta e="T168" id="Seg_14623" s="T164">[Полярная] ночь кончается, к свету идет.</ta>
            <ta e="T171" id="Seg_14624" s="T168">Кунгаджай говорит брату: </ta>
            <ta e="T173" id="Seg_14625" s="T171">— Запряги оленя.</ta>
            <ta e="T175" id="Seg_14626" s="T173">Так лежа, помрем.</ta>
            <ta e="T178" id="Seg_14627" s="T175">Поедем по следу Хозяина восьми черных оленей!</ta>
            <ta e="T185" id="Seg_14628" s="T178">Запрягли оленя, разобрали чум, сели вместе и потащились.</ta>
            <ta e="T188" id="Seg_14629" s="T185">Перекочевывали много раз.</ta>
            <ta e="T192" id="Seg_14630" s="T188">Однажды поднялись на высокий перевал.</ta>
            <ta e="T206" id="Seg_14631" s="T192">Осмотрелись: внизу видна широкая равнина, посреди равнины пасутся во множестве стада, за равниной стойбище видно из семидесяти чумов.</ta>
            <ta e="T212" id="Seg_14632" s="T206">Поехали через стада на ту сторону.</ta>
            <ta e="T215" id="Seg_14633" s="T212">— Стой! — сказал Кунгаджай.</ta>
            <ta e="T223" id="Seg_14634" s="T215">— Остановись у самого лучшего жилища, не доезжая до места, где рубят дрова.</ta>
            <ta e="T228" id="Seg_14635" s="T223">Видит, что один чум больше других.</ta>
            <ta e="T235" id="Seg_14636" s="T228">Подъехали, остановились напротив полога да и отпустили своего оленя.</ta>
            <ta e="T239" id="Seg_14637" s="T235">Поставили чум, разожгли огонь [в очаге].</ta>
            <ta e="T242" id="Seg_14638" s="T239">Младшему брату [средний] говорит: — Иди к жене брата.</ta>
            <ta e="T245" id="Seg_14639" s="T242">Пусть придет и покормит своего ребенка.</ta>
            <ta e="T258" id="Seg_14640" s="T245">Хозяину восьми черных оленей передай: пропадаем, пусть поделится хотя бы половиной [оленьего] сердца и придатком печени, — сказал.</ta>
            <ta e="T262" id="Seg_14641" s="T258">Мальчик вбежал [в тот чум] и все передал.</ta>
            <ta e="T266" id="Seg_14642" s="T262">— Смотри-ка, еще дать [ребенку] грудь пососать!</ta>
            <ta e="T270" id="Seg_14643" s="T266">Пусть подыхает! — говорит женщина.</ta>
            <ta e="T283" id="Seg_14644" s="T270">— Дедушка, брат просил сказать: помираем, поделитесь хотя бы половиной оленьего сердца и придатком печени.</ta>
            <ta e="T286" id="Seg_14645" s="T283">Еще что, подыхайте.</ta>
            <ta e="T291" id="Seg_14646" s="T286">Не дам! — говорит Хозяин восьми черных оленей.</ta>
            <ta e="T292" id="Seg_14647" s="T291">Бедняги легли спать.</ta>
            <ta e="T294" id="Seg_14648" s="T292">Есть то им что же?!</ta>
            <ta e="T298" id="Seg_14649" s="T294">Вдруг послышался голос, погоняющий оленей.</ta>
            <ta e="T303" id="Seg_14650" s="T298">Кунгаджай выполз, на коленях стоит и мочится.</ta>
            <ta e="T306" id="Seg_14651" s="T303">Тут к нему сбежалось много оленей.</ta>
            <ta e="T311" id="Seg_14652" s="T306">Пришел и очень жирный холощеный самец.</ta>
            <ta e="T314" id="Seg_14653" s="T311">Парень схватил его.</ta>
            <ta e="T316" id="Seg_14654" s="T314">Тот рвется.</ta>
            <ta e="T318" id="Seg_14655" s="T316">Велит вынести ему нож.</ta>
            <ta e="T321" id="Seg_14656" s="T318">Одним взмахом перерезал глотку [оленя].</ta>
            <ta e="T331" id="Seg_14657" s="T321">Не снимая шкуры, втаскивают, варят и едят целый день, до самого вечера.</ta>
            <ta e="T337" id="Seg_14658" s="T331">Около того чума стоял чум поменьше.</ta>
            <ta e="T348" id="Seg_14659" s="T337">— Давай-ка иди в тот чум к Хозяину семи пуговиц (старшина, наверное?!), попроси чего-нибудь на варево.</ta>
            <ta e="T355" id="Seg_14660" s="T348">Мальчик пошел и стал просить у Хозяина семи пуговиц.</ta>
            <ta e="T360" id="Seg_14661" s="T355">— Хозяин восьми черных оленей, наверное, дал что-нибудь?</ta>
            <ta e="T361" id="Seg_14662" s="T360">— Нет.</ta>
            <ta e="T366" id="Seg_14663" s="T361">— Ну, тогда я тоже не дам.</ta>
            <ta e="T367" id="Seg_14664" s="T366">Поели.</ta>
            <ta e="T371" id="Seg_14665" s="T367">Утром опять идет стадо.</ta>
            <ta e="T375" id="Seg_14666" s="T371">Кунгаджай выползает на коленях.</ta>
            <ta e="T381" id="Seg_14667" s="T375">Кунгаджай схватил за рога яловую важенку.</ta>
            <ta e="T384" id="Seg_14668" s="T381">— А ну, братец мой, вынеси нож!</ta>
            <ta e="T386" id="Seg_14669" s="T384">Забивает.</ta>
            <ta e="T389" id="Seg_14670" s="T386">Опять, не снимая шкуры, [втаскивают] и едят.</ta>
            <ta e="T396" id="Seg_14671" s="T389">Оказывается, он убил оленей тех хозяев.</ta>
            <ta e="T397" id="Seg_14672" s="T396">Улеглись спать.</ta>
            <ta e="T406" id="Seg_14673" s="T397">Когда утром Кунгаджай вышел, Хозяин восьми черных [оленей] сидел и подстругивал свой хорей.</ta>
            <ta e="T411" id="Seg_14674" s="T406">— О-о, Хозяин восьми черных [оленей], здравствуй!</ta>
            <ta e="T413" id="Seg_14675" s="T411">— Еще здоровается!</ta>
            <ta e="T415" id="Seg_14676" s="T413">Сегодня мы убьем вас.</ta>
            <ta e="T419" id="Seg_14677" s="T415">Лучших наших оленей поедаете!</ta>
            <ta e="T421" id="Seg_14678" s="T419">— Сами виноваты.</ta>
            <ta e="T434" id="Seg_14679" s="T421">Хозяин восьми черных [оленей] острым наконечникам хорея нацелился в бедро человека, стоящего на коленях, но промахнулся.</ta>
            <ta e="T438" id="Seg_14680" s="T434">Кунгаджай скользнул под сугроб.</ta>
            <ta e="T445" id="Seg_14681" s="T438">И выскочил там, где Хозяин семи пуговиц сидел и подстругивал хорей.</ta>
            <ta e="T450" id="Seg_14682" s="T445">— О-о, Хозяин семи пуговиц, здравствуй!</ta>
            <ta e="T452" id="Seg_14683" s="T450">— Еще здоровается!</ta>
            <ta e="T454" id="Seg_14684" s="T452">Сегодня мы убьем вас.</ta>
            <ta e="T458" id="Seg_14685" s="T454">Лучших наших оленей поедаете!</ta>
            <ta e="T460" id="Seg_14686" s="T458">— Сами виноваты.</ta>
            <ta e="T472" id="Seg_14687" s="T460">Хозяин семи пуговиц попытался ударить острым концом хорея в бедро человека, стоящего на коленях, но вонзил его в землю.</ta>
            <ta e="T484" id="Seg_14688" s="T472">Вскоре Кунгаджай выскочил в другом месте и вдруг вскочил на обе ноги.</ta>
            <ta e="T488" id="Seg_14689" s="T484">Огляделся: нарты трогаются.</ta>
            <ta e="T494" id="Seg_14690" s="T488">Откочевывает их невестка, отец ее замуж выдал.</ta>
            <ta e="T499" id="Seg_14691" s="T494">Кунгаджай выхватил у нее поводья: </ta>
            <ta e="T500" id="Seg_14692" s="T499">— Не пущу.</ta>
            <ta e="T503" id="Seg_14693" s="T500">Куда едешь?</ta>
            <ta e="T506" id="Seg_14694" s="T503">Женщина [не послушалась и] умчалась.</ta>
            <ta e="T512" id="Seg_14695" s="T506">Кунгаджай догнал и отнял ее запасных оленей.</ta>
            <ta e="T517" id="Seg_14696" s="T512">Входит Кунгаджай к Хозяину восьми черных [оленей]: </ta>
            <ta e="T526" id="Seg_14697" s="T517">— Ну, Хозяин восьми черных [оленей], пока мы в мире, ухаживай за моими младшими братьями.</ta>
            <ta e="T532" id="Seg_14698" s="T526">Если будешь плохо заботиться, за один день всех уничтожу.</ta>
            <ta e="T535" id="Seg_14699" s="T532">Я же иду догонять жену старшего брата.</ta>
            <ta e="T539" id="Seg_14700" s="T535">Гонится за женой брата, ‎‎но теряет ее след.</ta>
            <ta e="T545" id="Seg_14701" s="T539">Наконец обнаружил ее в конце какой-то местности, но не спешит догнать.</ta>
            <ta e="T550" id="Seg_14702" s="T545">Обежал и заставил ее завернуть упряжку в сторону стойбища.</ta>
            <ta e="T557" id="Seg_14703" s="T550">Хотя мог схватить ее, но не стал этого делать, а вошел в чум вслед за ней.</ta>
            <ta e="T561" id="Seg_14704" s="T557">Жена брата сидит и кормит грудью ребенка.</ta>
            <ta e="T564" id="Seg_14705" s="T561">Младший сидит кушает.</ta>
            <ta e="T568" id="Seg_14706" s="T564">— Ну, жена брата, вину твою прощаю.</ta>
            <ta e="T573" id="Seg_14707" s="T568">Если б не взяла ребенка, убил бы!</ta>
            <ta e="T578" id="Seg_14708" s="T573">Хозяин восьми черных [оленей] боится его,</ta>
            <ta e="T579" id="Seg_14709" s="T578">и есть за что.</ta>
            <ta e="T583" id="Seg_14710" s="T579">Как-то на третий день: </ta>
            <ta e="T589" id="Seg_14711" s="T583">— Ну, Хозяин восьми черных [оленей], собери свой народ.</ta>
            <ta e="T592" id="Seg_14712" s="T589">Устроим собрание! — говорит Кунгаджай.</ta>
            <ta e="T598" id="Seg_14713" s="T592">Вот собирают людей из семидесяти чумов.</ta>
            <ta e="T600" id="Seg_14714" s="T598">Кунгаджай говорит: </ta>
            <ta e="T611" id="Seg_14715" s="T600">— Вот тут вдвоем были хозяевами Хозяин восьми черных [оленей] и Хозяин семи пуговиц.</ta>
            <ta e="T615" id="Seg_14716" s="T611">Пришел конец вашему хозяйничанию.</ta>
            <ta e="T621" id="Seg_14717" s="T615">Теперь хозяин — этот мальчик, сын моего старшего брата.</ta>
            <ta e="T625" id="Seg_14718" s="T621">Все соберитесь за два дня.</ta>
            <ta e="T628" id="Seg_14719" s="T625">На третий день откочуете.</ta>
            <ta e="T631" id="Seg_14720" s="T628">Он поведет вас.</ta>
            <ta e="T642" id="Seg_14721" s="T631">— Не езди на чужих оленях, езди только на олене, завещанном отцом! — говорит парню.</ta>
            <ta e="T644" id="Seg_14722" s="T642">— Возглавишь кочевку.</ta>
            <ta e="T658" id="Seg_14723" s="T644">По краю леса тянется хребет, кочуй вдоль хребта со стороны моря, в пути пробудешь два месяца.</ta>
            <ta e="T668" id="Seg_14724" s="T658">К концу кочевки хребет перейдет в равнину, там [увидишь] дым, поднимающийся из-под снега.</ta>
            <ta e="T672" id="Seg_14725" s="T668">Это земля наших предков.</ta>
            <ta e="T677" id="Seg_14726" s="T672">Выедешь на большую дорогу, доедешь до своих родственников.</ta>
            <ta e="T682" id="Seg_14727" s="T677">Люди, вы должны охранять стада и днем и ночью.</ta>
            <ta e="T690" id="Seg_14728" s="T682">Хозяин восьми черных [оленей], Хозяин семи пуговиц, со всеми наравне будете [оленей] караулить.</ta>
            <ta e="T703" id="Seg_14729" s="T690">А я пойду уничтожать худых людей — чангитов и хищных зверей, чтобы вы кочевали благополучно.</ta>
            <ta e="T714" id="Seg_14730" s="T703">В ту землю прибуду в день твоего приезда, если очень задержусь, приду на третий день! — говорит.</ta>
            <ta e="T717" id="Seg_14731" s="T714">На третий день откочевывают.</ta>
            <ta e="T722" id="Seg_14732" s="T717">Кунгаджай уходит от них один, вовсе пешком.</ta>
            <ta e="T728" id="Seg_14733" s="T722">Вот во главе с этим мальчиком кочевали.</ta>
            <ta e="T735" id="Seg_14734" s="T728">В пути держались сторожко, этот мальчик заставлял всех сторожить стада, даже господ.</ta>
            <ta e="T738" id="Seg_14735" s="T735">Прошло полмесяца.</ta>
            <ta e="T741" id="Seg_14736" s="T738">Едут вдоль хребта.</ta>
            <ta e="T749" id="Seg_14737" s="T741">Вдруг заметили хребет, ответвляющийся в сторону моря, на нем человек ходит.</ta>
            <ta e="T752" id="Seg_14738" s="T749">— Откуда ты родом?</ta>
            <ta e="T754" id="Seg_14739" s="T752">— Не знаю.</ta>
            <ta e="T756" id="Seg_14740" s="T754">Нет у меня родного очага.</ta>
            <ta e="T760" id="Seg_14741" s="T756">Хожу и ищу родного старшего брата.</ta>
            <ta e="T762" id="Seg_14742" s="T760">— Кто он такой?</ta>
            <ta e="T765" id="Seg_14743" s="T762">— Хозяин восьми черных [оленей]!</ta>
            <ta e="T766" id="Seg_14744" s="T765">— Да ну!</ta>
            <ta e="T769" id="Seg_14745" s="T766">Он мой человек.</ta>
            <ta e="T773" id="Seg_14746" s="T769">Вон гонит свободное стадо.</ta>
            <ta e="T778" id="Seg_14747" s="T773">Раз такое дело, то иди ко мне работником.</ta>
            <ta e="T780" id="Seg_14748" s="T778">Тот медлит с ответом.</ta>
            <ta e="T796" id="Seg_14749" s="T780">— Если не пойдешь ко мне работником, станут [твои кости] побрякушками на одной из трех развилок моего хорея, на двух уже висят побрякушки из черепов двух богатырей.</ta>
            <ta e="T799" id="Seg_14750" s="T796">Человек покорился, пошел с ними.</ta>
            <ta e="T801" id="Seg_14751" s="T799">Кочуют дальше.</ta>
            <ta e="T803" id="Seg_14752" s="T801">Месяц прошел.</ta>
            <ta e="T807" id="Seg_14753" s="T803">Наступила середина следующего месяца.</ta>
            <ta e="T812" id="Seg_14754" s="T807">На одном хребте опять увидели человека.</ta>
            <ta e="T814" id="Seg_14755" s="T812">Он тоже здоровается.</ta>
            <ta e="T818" id="Seg_14756" s="T814">— В какой стороне твой чум и очаг?</ta>
            <ta e="T820" id="Seg_14757" s="T818">— Очага-чума не имею.</ta>
            <ta e="T822" id="Seg_14758" s="T820">Хожу вот.</ta>
            <ta e="T824" id="Seg_14759" s="T822">Старшего брата ищу.</ta>
            <ta e="T827" id="Seg_14760" s="T824">— Кем был твой брат?</ta>
            <ta e="T831" id="Seg_14761" s="T827">— Э-э, он Хозяин семи пуговиц!</ta>
            <ta e="T837" id="Seg_14762" s="T831">— Да ну, вон твой брат гонит свободное стадо.</ta>
            <ta e="T841" id="Seg_14763" s="T837">Может, ко мне пойдешь работником?</ta>
            <ta e="T842" id="Seg_14764" s="T841">Молчит.</ta>
            <ta e="T849" id="Seg_14765" s="T842">— Если не подчинишься, сделаю [из костей твоих] побрякушки на одной из развилок хорея!</ta>
            <ta e="T852" id="Seg_14766" s="T849">Тот человек соглашается.</ta>
            <ta e="T854" id="Seg_14767" s="T852">Подходит к брату: </ta>
            <ta e="T856" id="Seg_14768" s="T854">— Почему не убьете?</ta>
            <ta e="T860" id="Seg_14769" s="T856">Неужели ребенку подчинились?</ta>
            <ta e="T862" id="Seg_14770" s="T860">— Нет, не надо.</ta>
            <ta e="T864" id="Seg_14771" s="T862">— Надо убить.</ta>
            <ta e="T867" id="Seg_14772" s="T864">Стыдно ведь.</ta>
            <ta e="T869" id="Seg_14773" s="T867">— Ну попробуем!</ta>
            <ta e="T873" id="Seg_14774" s="T869">Вечером остановились в одном месте.</ta>
            <ta e="T880" id="Seg_14775" s="T873">Едят, мальчик всех людей гонит сторожить стада.</ta>
            <ta e="T883" id="Seg_14776" s="T880">Мальчик уснул тут же.</ta>
            <ta e="T887" id="Seg_14777" s="T883">Слышит шум подкрадывающихся людей.</ta>
            <ta e="T891" id="Seg_14778" s="T887">Он приоткрывает пальмой полог: </ta>
            <ta e="T897" id="Seg_14779" s="T891">— Зачем меня стережете, охраняйте стадо!</ta>
            <ta e="T900" id="Seg_14780" s="T897">Люди обратно скрылись.</ta>
            <ta e="T904" id="Seg_14781" s="T900">— Под утренний сон попробуем! — сговариваются.</ta>
            <ta e="T906" id="Seg_14782" s="T904">Снова делают попытку.</ta>
            <ta e="T909" id="Seg_14783" s="T906">Опять [говорит]: — Чего стережете?</ta>
            <ta e="T912" id="Seg_14784" s="T909">Уходите! — прогоняет.</ta>
            <ta e="T914" id="Seg_14785" s="T912">Больше не стали пытаться.</ta>
            <ta e="T920" id="Seg_14786" s="T914">К концу второго месяца хребет кончился.</ta>
            <ta e="T923" id="Seg_14787" s="T920">Видит: начинается равнина.</ta>
            <ta e="T926" id="Seg_14788" s="T923">Из-под снега клубится дым.</ta>
            <ta e="T932" id="Seg_14789" s="T926">От моря тянется широкая дорога.</ta>
            <ta e="T937" id="Seg_14790" s="T932">Уходит дорога дальше в глубь этого края.</ta>
            <ta e="T939" id="Seg_14791" s="T937">Едут.</ta>
            <ta e="T946" id="Seg_14792" s="T939">Прибыли в страну, где народ к народу селится, чум к чуму теснится,</ta>
            <ta e="T950" id="Seg_14793" s="T946">Останавливается у одного чума.</ta>
            <ta e="T955" id="Seg_14794" s="T950">Выходит пожилая женщина: </ta>
            <ta e="T959" id="Seg_14795" s="T955">— Да ну, какой у него удивительный олень!</ta>
            <ta e="T963" id="Seg_14796" s="T959">Ведь похож на оленей наших предков ?!</ta>
            <ta e="T970" id="Seg_14797" s="T963">В старину уехали от нас два человека, это, наверное, их потомки.</ta>
            <ta e="T981" id="Seg_14798" s="T970">Из трех чумов вышли три женщины и тоже признали, что это их сородичи, разговаривают, знакомятся.</ta>
            <ta e="T983" id="Seg_14799" s="T981">Выясняют свое родство.</ta>
            <ta e="T985" id="Seg_14800" s="T983">Ждут Кунгаджая.</ta>
            <ta e="T990" id="Seg_14801" s="T985">К полудню третьего дня приходит Кунгаджай.</ta>
            <ta e="T997" id="Seg_14802" s="T990">Живого места не осталось, изрублен так, что половина кожи осталась.</ta>
            <ta e="T1001" id="Seg_14803" s="T997">— Дитя мое, добрался ты — вот хорошо! [— говорит.—] </ta>
            <ta e="T1008" id="Seg_14804" s="T1001">Одолел худых людей, а хищных зверей с трудом осилил.</ta>
            <ta e="T1013" id="Seg_14805" s="T1008">Изранили меня, чуть живой пришел.</ta>
            <ta e="T1018" id="Seg_14806" s="T1013">Теперь живи и никого не бойся!</ta>
            <ta e="T1022" id="Seg_14807" s="T1018">Кунгаджай со временем выздоровел.</ta>
            <ta e="T1028" id="Seg_14808" s="T1022">Парень стал вождем этого народа.</ta>
            <ta e="T1029" id="Seg_14809" s="T1028">Конец.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T22" id="Seg_14810" s="T14">[DCh]: "hahɨl ogo", lit. 'fox child' means a very small child.</ta>
            <ta e="T754" id="Seg_14811" s="T752">[DCh]: The sense of the sentence is not clear in this context, even for native speakers.</ta>
            <ta e="T1008" id="Seg_14812" s="T1001">[DCh]: Not really clear what kind of animals in the second clause.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
