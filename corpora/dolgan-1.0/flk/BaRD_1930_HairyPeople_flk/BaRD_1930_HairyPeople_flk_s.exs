<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID23C173CC-91F2-269B-8BF7-C89DC40560B9">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>BaRD_1930_HairyPeople_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\BaRD_1930_HairyPeople_flk\BaRD_1930_HairyPeople_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">294</ud-information>
            <ud-information attribute-name="# HIAT:w">241</ud-information>
            <ud-information attribute-name="# e">241</ud-information>
            <ud-information attribute-name="# HIAT:u">27</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BaRD">
            <abbreviation>BaRD</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BaRD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T241" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Bɨlɨr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">biːr</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">ɨttaːk</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">kihi</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">hirge</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">barbɨt</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_23" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">Bu</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">baran</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">ihen</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">ulakan</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">bagajɨ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">kihi</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">hu͡olun</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">kaːrga</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_49" n="HIAT:w" s="T14">körbüt</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_53" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">Hu͡ola</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">ulakana</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">türge</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">hu͡olun</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_67" n="HIAT:w" s="T19">kurduk</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_70" n="HIAT:w" s="T20">ebit</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_74" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">Baː</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">köröːččü</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">kihi</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_85" n="HIAT:w" s="T24">hiŋil</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_88" n="HIAT:w" s="T25">ebit</ts>
                  <nts id="Seg_89" n="HIAT:ip">,</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_92" n="HIAT:w" s="T26">tugu</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_95" n="HIAT:w" s="T27">daː</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_98" n="HIAT:w" s="T28">hoččo</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_101" n="HIAT:w" s="T29">kuttammat</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_105" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_107" n="HIAT:w" s="T30">Hol</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_110" n="HIAT:w" s="T31">ereːri</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_113" n="HIAT:w" s="T32">kini</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_116" n="HIAT:w" s="T33">dʼulajbɨt</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_119" n="HIAT:w" s="T34">künüs</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_122" n="HIAT:w" s="T35">barɨ͡agɨn</ts>
                  <nts id="Seg_123" n="HIAT:ip">.</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_126" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_128" n="HIAT:w" s="T36">Ki͡ehe</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_131" n="HIAT:w" s="T37">bu͡olarɨn</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_134" n="HIAT:w" s="T38">ketespit</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_137" n="HIAT:w" s="T39">oloroːron</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_141" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_143" n="HIAT:w" s="T40">Bu</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_146" n="HIAT:w" s="T41">ki͡ehe</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_149" n="HIAT:w" s="T42">bu͡olbutugar</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_152" n="HIAT:w" s="T43">ɨtɨgar</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_155" n="HIAT:w" s="T44">oloron</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_158" n="HIAT:w" s="T45">hu͡olu</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_161" n="HIAT:w" s="T46">batan</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_164" n="HIAT:w" s="T47">barar</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_168" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_170" n="HIAT:w" s="T48">Baː</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_173" n="HIAT:w" s="T49">ulakan</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_176" n="HIAT:w" s="T50">dʼon</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_179" n="HIAT:w" s="T51">hu͡ollara</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_182" n="HIAT:w" s="T52">elbeːn</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_185" n="HIAT:w" s="T53">barbɨt</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_189" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_191" n="HIAT:w" s="T54">Ulakan</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_194" n="HIAT:w" s="T55">bagajɨ</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_197" n="HIAT:w" s="T56">balagaŋŋa</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_200" n="HIAT:w" s="T57">tijbit</ts>
                  <nts id="Seg_201" n="HIAT:ip">.</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_204" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_206" n="HIAT:w" s="T58">Ulakan</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_209" n="HIAT:w" s="T59">mijeː</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_212" n="HIAT:w" s="T60">hetiːler</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_215" n="HIAT:w" s="T61">turallarɨn</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_218" n="HIAT:w" s="T62">körbüt</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_222" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_224" n="HIAT:w" s="T63">ɨttarɨn</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_227" n="HIAT:w" s="T64">tönnör</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_230" n="HIAT:w" s="T65">hu͡olun</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_233" n="HIAT:w" s="T66">di͡eg</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_236" n="HIAT:w" s="T67">turu͡oran</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_239" n="HIAT:w" s="T68">baran</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_242" n="HIAT:w" s="T69">keleːčči</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_245" n="HIAT:w" s="T70">kihi</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_248" n="HIAT:w" s="T71">balagan</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_251" n="HIAT:w" s="T72">ürdüger</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_254" n="HIAT:w" s="T73">taksɨbɨt</ts>
                  <nts id="Seg_255" n="HIAT:ip">,</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_258" n="HIAT:w" s="T74">taksan</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_261" n="HIAT:w" s="T75">ü͡öleske</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_264" n="HIAT:w" s="T76">öŋöjön</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_267" n="HIAT:w" s="T77">körbüte</ts>
                  <nts id="Seg_268" n="HIAT:ip">,</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_271" n="HIAT:w" s="T78">u͡ottara</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_274" n="HIAT:w" s="T79">umullubut</ts>
                  <nts id="Seg_275" n="HIAT:ip">,</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_278" n="HIAT:w" s="T80">čogo</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_281" n="HIAT:w" s="T81">ere</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_284" n="HIAT:w" s="T82">kɨtara</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_287" n="HIAT:w" s="T83">hɨtar</ts>
                  <nts id="Seg_288" n="HIAT:ip">.</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_291" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_293" n="HIAT:w" s="T84">Araj</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_296" n="HIAT:w" s="T85">istibite</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_299" n="HIAT:w" s="T86">biːr</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_302" n="HIAT:w" s="T87">kihi</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_305" n="HIAT:w" s="T88">oloŋkoluːr</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_308" n="HIAT:w" s="T89">bɨhɨːlaːk</ts>
                  <nts id="Seg_309" n="HIAT:ip">.</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_312" n="HIAT:u" s="T90">
                  <nts id="Seg_313" n="HIAT:ip">"</nts>
                  <ts e="T91" id="Seg_315" n="HIAT:w" s="T90">Magan</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_318" n="HIAT:w" s="T91">ɨraːktaːgɨ</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_321" n="HIAT:w" s="T92">dʼonnoro</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_324" n="HIAT:w" s="T93">baːllar</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_327" n="HIAT:w" s="T94">ühü</ts>
                  <nts id="Seg_328" n="HIAT:ip">"</nts>
                  <nts id="Seg_329" n="HIAT:ip">,</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_332" n="HIAT:w" s="T95">di͡en</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_335" n="HIAT:w" s="T96">kepsiːr</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_338" n="HIAT:w" s="T97">ebit</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_341" n="HIAT:w" s="T98">bu</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_344" n="HIAT:w" s="T99">oloŋkohut</ts>
                  <nts id="Seg_345" n="HIAT:ip">.</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_348" n="HIAT:u" s="T100">
                  <nts id="Seg_349" n="HIAT:ip">"</nts>
                  <ts e="T101" id="Seg_351" n="HIAT:w" s="T100">Ol</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_354" n="HIAT:w" s="T101">magan</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_357" n="HIAT:w" s="T102">ɨraːktaːgɨ</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_360" n="HIAT:w" s="T103">dʼonnoro</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_363" n="HIAT:w" s="T104">biːr</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_366" n="HIAT:w" s="T105">tugut</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_369" n="HIAT:w" s="T106">tiriːtitten</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_372" n="HIAT:w" s="T107">taŋnallar</ts>
                  <nts id="Seg_373" n="HIAT:ip">,</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_376" n="HIAT:w" s="T108">biːr</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_379" n="HIAT:w" s="T109">tugut</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_382" n="HIAT:w" s="T110">buːtun</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_385" n="HIAT:w" s="T111">ikkite</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_388" n="HIAT:w" s="T112">kü͡östeneller</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_391" n="HIAT:w" s="T113">ühü</ts>
                  <nts id="Seg_392" n="HIAT:ip">"</nts>
                  <nts id="Seg_393" n="HIAT:ip">,</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_396" n="HIAT:w" s="T114">diːr</ts>
                  <nts id="Seg_397" n="HIAT:ip">.</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_400" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_402" n="HIAT:w" s="T115">Manɨ͡aga</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_405" n="HIAT:w" s="T116">isteːččilere</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_408" n="HIAT:w" s="T117">küleller</ts>
                  <nts id="Seg_409" n="HIAT:ip">:</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_412" n="HIAT:u" s="T118">
                  <nts id="Seg_413" n="HIAT:ip">"</nts>
                  <ts e="T119" id="Seg_415" n="HIAT:w" s="T118">Dʼe</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_418" n="HIAT:w" s="T119">eri͡ekkes</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_421" n="HIAT:w" s="T120">deː</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_424" n="HIAT:w" s="T121">dʼon</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_427" n="HIAT:w" s="T122">ebitter</ts>
                  <nts id="Seg_428" n="HIAT:ip">"</nts>
                  <nts id="Seg_429" n="HIAT:ip">,</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_432" n="HIAT:w" s="T123">di͡en</ts>
                  <nts id="Seg_433" n="HIAT:ip">.</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_436" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_438" n="HIAT:w" s="T124">Manɨ</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_441" n="HIAT:w" s="T125">isten</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_444" n="HIAT:w" s="T126">baː</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_447" n="HIAT:w" s="T127">kihi</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_450" n="HIAT:w" s="T128">hürdeːktik</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_453" n="HIAT:w" s="T129">körü͡ön</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_456" n="HIAT:w" s="T130">bagarbɨt</ts>
                  <nts id="Seg_457" n="HIAT:ip">.</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_460" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_462" n="HIAT:w" s="T131">Bɨlɨrgɨ</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_465" n="HIAT:w" s="T132">kihi͡eke</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_468" n="HIAT:w" s="T133">katatɨn</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_471" n="HIAT:w" s="T134">gɨtta</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_474" n="HIAT:w" s="T135">biːrge</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_477" n="HIAT:w" s="T136">baːr</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_480" n="HIAT:w" s="T137">bu͡olaːččɨ</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_483" n="HIAT:w" s="T138">emek</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_486" n="HIAT:w" s="T139">mas</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_489" n="HIAT:w" s="T140">duː</ts>
                  <nts id="Seg_490" n="HIAT:ip">,</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_493" n="HIAT:w" s="T141">higiriː</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_496" n="HIAT:w" s="T142">duː</ts>
                  <nts id="Seg_497" n="HIAT:ip">.</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_500" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_502" n="HIAT:w" s="T143">Manan</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_505" n="HIAT:w" s="T144">kɨːmɨ</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_508" n="HIAT:w" s="T145">kü͡ödʼüten</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_511" n="HIAT:w" s="T146">umatallar</ts>
                  <nts id="Seg_512" n="HIAT:ip">.</nts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_515" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_517" n="HIAT:w" s="T147">Baː</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_520" n="HIAT:w" s="T148">kihi͡eke</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_523" n="HIAT:w" s="T149">higiriː</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_526" n="HIAT:w" s="T150">baːr</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_529" n="HIAT:w" s="T151">ebit</ts>
                  <nts id="Seg_530" n="HIAT:ip">.</nts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_533" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_535" n="HIAT:w" s="T152">Ontun</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_538" n="HIAT:w" s="T153">ubatan</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_541" n="HIAT:w" s="T154">ü͡ölehinen</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_544" n="HIAT:w" s="T155">bɨrakpɨt</ts>
                  <nts id="Seg_545" n="HIAT:ip">,</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_548" n="HIAT:w" s="T156">higiriːte</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_551" n="HIAT:w" s="T157">čokko</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_554" n="HIAT:w" s="T158">tühen</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_557" n="HIAT:w" s="T159">emiske</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_560" n="HIAT:w" s="T160">külübür</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_563" n="HIAT:w" s="T161">gɨmmɨt</ts>
                  <nts id="Seg_564" n="HIAT:ip">,</nts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_567" n="HIAT:w" s="T162">onno</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_570" n="HIAT:w" s="T163">körbüte</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_573" n="HIAT:w" s="T164">u͡ot</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_576" n="HIAT:w" s="T165">innitiger</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_579" n="HIAT:w" s="T166">biːr</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_582" n="HIAT:w" s="T167">kihi</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_585" n="HIAT:w" s="T168">oloŋkoluː</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_588" n="HIAT:w" s="T169">oloror</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_591" n="HIAT:w" s="T170">ebit</ts>
                  <nts id="Seg_592" n="HIAT:ip">.</nts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_595" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_597" n="HIAT:w" s="T171">Bu</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_600" n="HIAT:w" s="T172">kihi</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_603" n="HIAT:w" s="T173">hɨgɨnnʼak</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_606" n="HIAT:w" s="T174">bu͡olan</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_609" n="HIAT:w" s="T175">baran</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_612" n="HIAT:w" s="T176">barɨta</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_615" n="HIAT:w" s="T177">tüː</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_618" n="HIAT:w" s="T178">ebit</ts>
                  <nts id="Seg_619" n="HIAT:ip">.</nts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_622" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_624" n="HIAT:w" s="T179">Manɨ</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_627" n="HIAT:w" s="T180">körör</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_630" n="HIAT:w" s="T181">dʼe</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_633" n="HIAT:w" s="T182">baː</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_636" n="HIAT:w" s="T183">kihi</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_639" n="HIAT:w" s="T184">ojo</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_642" n="HIAT:w" s="T185">ɨstanar</ts>
                  <nts id="Seg_643" n="HIAT:ip">,</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_646" n="HIAT:w" s="T186">ɨttarɨn</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_649" n="HIAT:w" s="T187">hɨrgatɨgar</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_652" n="HIAT:w" s="T188">oloror</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_655" n="HIAT:w" s="T189">daː</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_658" n="HIAT:w" s="T190">barar</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_661" n="HIAT:w" s="T191">di͡ekki</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_664" n="HIAT:w" s="T192">bu͡olar</ts>
                  <nts id="Seg_665" n="HIAT:ip">.</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_668" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_670" n="HIAT:w" s="T193">Kennin</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_673" n="HIAT:w" s="T194">di͡eg</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_676" n="HIAT:w" s="T195">kajɨspɨta</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_679" n="HIAT:w" s="T196">biːr</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_682" n="HIAT:w" s="T197">tüːleːk</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_685" n="HIAT:w" s="T198">kihi</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_688" n="HIAT:w" s="T199">hiten</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_691" n="HIAT:w" s="T200">iher</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_694" n="HIAT:w" s="T201">ebit</ts>
                  <nts id="Seg_695" n="HIAT:ip">,</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_698" n="HIAT:w" s="T202">hiten</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_701" n="HIAT:w" s="T203">hɨrgatɨn</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_704" n="HIAT:w" s="T204">kenniki</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_707" n="HIAT:w" s="T205">atagɨttan</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_710" n="HIAT:w" s="T206">ɨlbɨta</ts>
                  <nts id="Seg_711" n="HIAT:ip">,</nts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_714" n="HIAT:w" s="T207">ol</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_717" n="HIAT:w" s="T208">kennigi</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_720" n="HIAT:w" s="T209">ataga</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_723" n="HIAT:w" s="T210">baː</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_726" n="HIAT:w" s="T211">tüːleːk</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_729" n="HIAT:w" s="T212">kihi</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_732" n="HIAT:w" s="T213">iliːtiger</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_735" n="HIAT:w" s="T214">kaːlbɨt</ts>
                  <nts id="Seg_736" n="HIAT:ip">.</nts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_739" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_741" n="HIAT:w" s="T215">Ol</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_744" n="HIAT:w" s="T216">ɨlaːččɨ</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_747" n="HIAT:w" s="T217">kihi</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_750" n="HIAT:w" s="T218">ü͡ögüleːbit</ts>
                  <nts id="Seg_751" n="HIAT:ip">:</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_754" n="HIAT:u" s="T219">
                  <nts id="Seg_755" n="HIAT:ip">"</nts>
                  <ts e="T220" id="Seg_757" n="HIAT:w" s="T219">Magan</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_760" n="HIAT:w" s="T220">ɨraːktaːgɨ</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_763" n="HIAT:w" s="T221">dʼonnoro</ts>
                  <nts id="Seg_764" n="HIAT:ip">,</nts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_767" n="HIAT:w" s="T222">tönnör</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_770" n="HIAT:w" s="T223">bu͡olaːjagɨt</ts>
                  <nts id="Seg_771" n="HIAT:ip">,</nts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_774" n="HIAT:w" s="T224">bihigi</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_777" n="HIAT:w" s="T225">bejebitin</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_780" n="HIAT:w" s="T226">hurak</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_783" n="HIAT:w" s="T227">ɨːtarɨ</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_786" n="HIAT:w" s="T228">höbüleːbet</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_789" n="HIAT:w" s="T229">bu͡olabɨt</ts>
                  <nts id="Seg_790" n="HIAT:ip">.</nts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T241" id="Seg_793" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_795" n="HIAT:w" s="T230">Anɨ</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_798" n="HIAT:w" s="T231">kim</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_801" n="HIAT:w" s="T232">eme</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_804" n="HIAT:w" s="T233">bihigi</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_807" n="HIAT:w" s="T234">di͡eg</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_810" n="HIAT:w" s="T235">keli͡e</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_813" n="HIAT:w" s="T236">daː</ts>
                  <nts id="Seg_814" n="HIAT:ip">,</nts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_817" n="HIAT:w" s="T237">bihigi</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_820" n="HIAT:w" s="T238">tönnörü͡ökpüt</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_823" n="HIAT:w" s="T239">hu͡oga</ts>
                  <nts id="Seg_824" n="HIAT:ip">"</nts>
                  <nts id="Seg_825" n="HIAT:ip">,</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_828" n="HIAT:w" s="T240">di͡ebit</ts>
                  <nts id="Seg_829" n="HIAT:ip">.</nts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T241" id="Seg_831" n="sc" s="T0">
               <ts e="T1" id="Seg_833" n="e" s="T0">Bɨlɨr </ts>
               <ts e="T2" id="Seg_835" n="e" s="T1">biːr </ts>
               <ts e="T3" id="Seg_837" n="e" s="T2">ɨttaːk </ts>
               <ts e="T4" id="Seg_839" n="e" s="T3">kihi </ts>
               <ts e="T5" id="Seg_841" n="e" s="T4">hirge </ts>
               <ts e="T6" id="Seg_843" n="e" s="T5">barbɨt. </ts>
               <ts e="T7" id="Seg_845" n="e" s="T6">Bu </ts>
               <ts e="T8" id="Seg_847" n="e" s="T7">baran </ts>
               <ts e="T9" id="Seg_849" n="e" s="T8">ihen </ts>
               <ts e="T10" id="Seg_851" n="e" s="T9">ulakan </ts>
               <ts e="T11" id="Seg_853" n="e" s="T10">bagajɨ </ts>
               <ts e="T12" id="Seg_855" n="e" s="T11">kihi </ts>
               <ts e="T13" id="Seg_857" n="e" s="T12">hu͡olun </ts>
               <ts e="T14" id="Seg_859" n="e" s="T13">kaːrga </ts>
               <ts e="T15" id="Seg_861" n="e" s="T14">körbüt. </ts>
               <ts e="T16" id="Seg_863" n="e" s="T15">Hu͡ola </ts>
               <ts e="T17" id="Seg_865" n="e" s="T16">ulakana </ts>
               <ts e="T18" id="Seg_867" n="e" s="T17">türge </ts>
               <ts e="T19" id="Seg_869" n="e" s="T18">hu͡olun </ts>
               <ts e="T20" id="Seg_871" n="e" s="T19">kurduk </ts>
               <ts e="T21" id="Seg_873" n="e" s="T20">ebit. </ts>
               <ts e="T22" id="Seg_875" n="e" s="T21">Baː </ts>
               <ts e="T23" id="Seg_877" n="e" s="T22">köröːččü </ts>
               <ts e="T24" id="Seg_879" n="e" s="T23">kihi </ts>
               <ts e="T25" id="Seg_881" n="e" s="T24">hiŋil </ts>
               <ts e="T26" id="Seg_883" n="e" s="T25">ebit, </ts>
               <ts e="T27" id="Seg_885" n="e" s="T26">tugu </ts>
               <ts e="T28" id="Seg_887" n="e" s="T27">daː </ts>
               <ts e="T29" id="Seg_889" n="e" s="T28">hoččo </ts>
               <ts e="T30" id="Seg_891" n="e" s="T29">kuttammat. </ts>
               <ts e="T31" id="Seg_893" n="e" s="T30">Hol </ts>
               <ts e="T32" id="Seg_895" n="e" s="T31">ereːri </ts>
               <ts e="T33" id="Seg_897" n="e" s="T32">kini </ts>
               <ts e="T34" id="Seg_899" n="e" s="T33">dʼulajbɨt </ts>
               <ts e="T35" id="Seg_901" n="e" s="T34">künüs </ts>
               <ts e="T36" id="Seg_903" n="e" s="T35">barɨ͡agɨn. </ts>
               <ts e="T37" id="Seg_905" n="e" s="T36">Ki͡ehe </ts>
               <ts e="T38" id="Seg_907" n="e" s="T37">bu͡olarɨn </ts>
               <ts e="T39" id="Seg_909" n="e" s="T38">ketespit </ts>
               <ts e="T40" id="Seg_911" n="e" s="T39">oloroːron. </ts>
               <ts e="T41" id="Seg_913" n="e" s="T40">Bu </ts>
               <ts e="T42" id="Seg_915" n="e" s="T41">ki͡ehe </ts>
               <ts e="T43" id="Seg_917" n="e" s="T42">bu͡olbutugar </ts>
               <ts e="T44" id="Seg_919" n="e" s="T43">ɨtɨgar </ts>
               <ts e="T45" id="Seg_921" n="e" s="T44">oloron </ts>
               <ts e="T46" id="Seg_923" n="e" s="T45">hu͡olu </ts>
               <ts e="T47" id="Seg_925" n="e" s="T46">batan </ts>
               <ts e="T48" id="Seg_927" n="e" s="T47">barar. </ts>
               <ts e="T49" id="Seg_929" n="e" s="T48">Baː </ts>
               <ts e="T50" id="Seg_931" n="e" s="T49">ulakan </ts>
               <ts e="T51" id="Seg_933" n="e" s="T50">dʼon </ts>
               <ts e="T52" id="Seg_935" n="e" s="T51">hu͡ollara </ts>
               <ts e="T53" id="Seg_937" n="e" s="T52">elbeːn </ts>
               <ts e="T54" id="Seg_939" n="e" s="T53">barbɨt. </ts>
               <ts e="T55" id="Seg_941" n="e" s="T54">Ulakan </ts>
               <ts e="T56" id="Seg_943" n="e" s="T55">bagajɨ </ts>
               <ts e="T57" id="Seg_945" n="e" s="T56">balagaŋŋa </ts>
               <ts e="T58" id="Seg_947" n="e" s="T57">tijbit. </ts>
               <ts e="T59" id="Seg_949" n="e" s="T58">Ulakan </ts>
               <ts e="T60" id="Seg_951" n="e" s="T59">mijeː </ts>
               <ts e="T61" id="Seg_953" n="e" s="T60">hetiːler </ts>
               <ts e="T62" id="Seg_955" n="e" s="T61">turallarɨn </ts>
               <ts e="T63" id="Seg_957" n="e" s="T62">körbüt. </ts>
               <ts e="T64" id="Seg_959" n="e" s="T63">ɨttarɨn </ts>
               <ts e="T65" id="Seg_961" n="e" s="T64">tönnör </ts>
               <ts e="T66" id="Seg_963" n="e" s="T65">hu͡olun </ts>
               <ts e="T67" id="Seg_965" n="e" s="T66">di͡eg </ts>
               <ts e="T68" id="Seg_967" n="e" s="T67">turu͡oran </ts>
               <ts e="T69" id="Seg_969" n="e" s="T68">baran </ts>
               <ts e="T70" id="Seg_971" n="e" s="T69">keleːčči </ts>
               <ts e="T71" id="Seg_973" n="e" s="T70">kihi </ts>
               <ts e="T72" id="Seg_975" n="e" s="T71">balagan </ts>
               <ts e="T73" id="Seg_977" n="e" s="T72">ürdüger </ts>
               <ts e="T74" id="Seg_979" n="e" s="T73">taksɨbɨt, </ts>
               <ts e="T75" id="Seg_981" n="e" s="T74">taksan </ts>
               <ts e="T76" id="Seg_983" n="e" s="T75">ü͡öleske </ts>
               <ts e="T77" id="Seg_985" n="e" s="T76">öŋöjön </ts>
               <ts e="T78" id="Seg_987" n="e" s="T77">körbüte, </ts>
               <ts e="T79" id="Seg_989" n="e" s="T78">u͡ottara </ts>
               <ts e="T80" id="Seg_991" n="e" s="T79">umullubut, </ts>
               <ts e="T81" id="Seg_993" n="e" s="T80">čogo </ts>
               <ts e="T82" id="Seg_995" n="e" s="T81">ere </ts>
               <ts e="T83" id="Seg_997" n="e" s="T82">kɨtara </ts>
               <ts e="T84" id="Seg_999" n="e" s="T83">hɨtar. </ts>
               <ts e="T85" id="Seg_1001" n="e" s="T84">Araj </ts>
               <ts e="T86" id="Seg_1003" n="e" s="T85">istibite </ts>
               <ts e="T87" id="Seg_1005" n="e" s="T86">biːr </ts>
               <ts e="T88" id="Seg_1007" n="e" s="T87">kihi </ts>
               <ts e="T89" id="Seg_1009" n="e" s="T88">oloŋkoluːr </ts>
               <ts e="T90" id="Seg_1011" n="e" s="T89">bɨhɨːlaːk. </ts>
               <ts e="T91" id="Seg_1013" n="e" s="T90">"Magan </ts>
               <ts e="T92" id="Seg_1015" n="e" s="T91">ɨraːktaːgɨ </ts>
               <ts e="T93" id="Seg_1017" n="e" s="T92">dʼonnoro </ts>
               <ts e="T94" id="Seg_1019" n="e" s="T93">baːllar </ts>
               <ts e="T95" id="Seg_1021" n="e" s="T94">ühü", </ts>
               <ts e="T96" id="Seg_1023" n="e" s="T95">di͡en </ts>
               <ts e="T97" id="Seg_1025" n="e" s="T96">kepsiːr </ts>
               <ts e="T98" id="Seg_1027" n="e" s="T97">ebit </ts>
               <ts e="T99" id="Seg_1029" n="e" s="T98">bu </ts>
               <ts e="T100" id="Seg_1031" n="e" s="T99">oloŋkohut. </ts>
               <ts e="T101" id="Seg_1033" n="e" s="T100">"Ol </ts>
               <ts e="T102" id="Seg_1035" n="e" s="T101">magan </ts>
               <ts e="T103" id="Seg_1037" n="e" s="T102">ɨraːktaːgɨ </ts>
               <ts e="T104" id="Seg_1039" n="e" s="T103">dʼonnoro </ts>
               <ts e="T105" id="Seg_1041" n="e" s="T104">biːr </ts>
               <ts e="T106" id="Seg_1043" n="e" s="T105">tugut </ts>
               <ts e="T107" id="Seg_1045" n="e" s="T106">tiriːtitten </ts>
               <ts e="T108" id="Seg_1047" n="e" s="T107">taŋnallar, </ts>
               <ts e="T109" id="Seg_1049" n="e" s="T108">biːr </ts>
               <ts e="T110" id="Seg_1051" n="e" s="T109">tugut </ts>
               <ts e="T111" id="Seg_1053" n="e" s="T110">buːtun </ts>
               <ts e="T112" id="Seg_1055" n="e" s="T111">ikkite </ts>
               <ts e="T113" id="Seg_1057" n="e" s="T112">kü͡östeneller </ts>
               <ts e="T114" id="Seg_1059" n="e" s="T113">ühü", </ts>
               <ts e="T115" id="Seg_1061" n="e" s="T114">diːr. </ts>
               <ts e="T116" id="Seg_1063" n="e" s="T115">Manɨ͡aga </ts>
               <ts e="T117" id="Seg_1065" n="e" s="T116">isteːččilere </ts>
               <ts e="T118" id="Seg_1067" n="e" s="T117">küleller: </ts>
               <ts e="T119" id="Seg_1069" n="e" s="T118">"Dʼe </ts>
               <ts e="T120" id="Seg_1071" n="e" s="T119">eri͡ekkes </ts>
               <ts e="T121" id="Seg_1073" n="e" s="T120">deː </ts>
               <ts e="T122" id="Seg_1075" n="e" s="T121">dʼon </ts>
               <ts e="T123" id="Seg_1077" n="e" s="T122">ebitter", </ts>
               <ts e="T124" id="Seg_1079" n="e" s="T123">di͡en. </ts>
               <ts e="T125" id="Seg_1081" n="e" s="T124">Manɨ </ts>
               <ts e="T126" id="Seg_1083" n="e" s="T125">isten </ts>
               <ts e="T127" id="Seg_1085" n="e" s="T126">baː </ts>
               <ts e="T128" id="Seg_1087" n="e" s="T127">kihi </ts>
               <ts e="T129" id="Seg_1089" n="e" s="T128">hürdeːktik </ts>
               <ts e="T130" id="Seg_1091" n="e" s="T129">körü͡ön </ts>
               <ts e="T131" id="Seg_1093" n="e" s="T130">bagarbɨt. </ts>
               <ts e="T132" id="Seg_1095" n="e" s="T131">Bɨlɨrgɨ </ts>
               <ts e="T133" id="Seg_1097" n="e" s="T132">kihi͡eke </ts>
               <ts e="T134" id="Seg_1099" n="e" s="T133">katatɨn </ts>
               <ts e="T135" id="Seg_1101" n="e" s="T134">gɨtta </ts>
               <ts e="T136" id="Seg_1103" n="e" s="T135">biːrge </ts>
               <ts e="T137" id="Seg_1105" n="e" s="T136">baːr </ts>
               <ts e="T138" id="Seg_1107" n="e" s="T137">bu͡olaːččɨ </ts>
               <ts e="T139" id="Seg_1109" n="e" s="T138">emek </ts>
               <ts e="T140" id="Seg_1111" n="e" s="T139">mas </ts>
               <ts e="T141" id="Seg_1113" n="e" s="T140">duː, </ts>
               <ts e="T142" id="Seg_1115" n="e" s="T141">higiriː </ts>
               <ts e="T143" id="Seg_1117" n="e" s="T142">duː. </ts>
               <ts e="T144" id="Seg_1119" n="e" s="T143">Manan </ts>
               <ts e="T145" id="Seg_1121" n="e" s="T144">kɨːmɨ </ts>
               <ts e="T146" id="Seg_1123" n="e" s="T145">kü͡ödʼüten </ts>
               <ts e="T147" id="Seg_1125" n="e" s="T146">umatallar. </ts>
               <ts e="T148" id="Seg_1127" n="e" s="T147">Baː </ts>
               <ts e="T149" id="Seg_1129" n="e" s="T148">kihi͡eke </ts>
               <ts e="T150" id="Seg_1131" n="e" s="T149">higiriː </ts>
               <ts e="T151" id="Seg_1133" n="e" s="T150">baːr </ts>
               <ts e="T152" id="Seg_1135" n="e" s="T151">ebit. </ts>
               <ts e="T153" id="Seg_1137" n="e" s="T152">Ontun </ts>
               <ts e="T154" id="Seg_1139" n="e" s="T153">ubatan </ts>
               <ts e="T155" id="Seg_1141" n="e" s="T154">ü͡ölehinen </ts>
               <ts e="T156" id="Seg_1143" n="e" s="T155">bɨrakpɨt, </ts>
               <ts e="T157" id="Seg_1145" n="e" s="T156">higiriːte </ts>
               <ts e="T158" id="Seg_1147" n="e" s="T157">čokko </ts>
               <ts e="T159" id="Seg_1149" n="e" s="T158">tühen </ts>
               <ts e="T160" id="Seg_1151" n="e" s="T159">emiske </ts>
               <ts e="T161" id="Seg_1153" n="e" s="T160">külübür </ts>
               <ts e="T162" id="Seg_1155" n="e" s="T161">gɨmmɨt, </ts>
               <ts e="T163" id="Seg_1157" n="e" s="T162">onno </ts>
               <ts e="T164" id="Seg_1159" n="e" s="T163">körbüte </ts>
               <ts e="T165" id="Seg_1161" n="e" s="T164">u͡ot </ts>
               <ts e="T166" id="Seg_1163" n="e" s="T165">innitiger </ts>
               <ts e="T167" id="Seg_1165" n="e" s="T166">biːr </ts>
               <ts e="T168" id="Seg_1167" n="e" s="T167">kihi </ts>
               <ts e="T169" id="Seg_1169" n="e" s="T168">oloŋkoluː </ts>
               <ts e="T170" id="Seg_1171" n="e" s="T169">oloror </ts>
               <ts e="T171" id="Seg_1173" n="e" s="T170">ebit. </ts>
               <ts e="T172" id="Seg_1175" n="e" s="T171">Bu </ts>
               <ts e="T173" id="Seg_1177" n="e" s="T172">kihi </ts>
               <ts e="T174" id="Seg_1179" n="e" s="T173">hɨgɨnnʼak </ts>
               <ts e="T175" id="Seg_1181" n="e" s="T174">bu͡olan </ts>
               <ts e="T176" id="Seg_1183" n="e" s="T175">baran </ts>
               <ts e="T177" id="Seg_1185" n="e" s="T176">barɨta </ts>
               <ts e="T178" id="Seg_1187" n="e" s="T177">tüː </ts>
               <ts e="T179" id="Seg_1189" n="e" s="T178">ebit. </ts>
               <ts e="T180" id="Seg_1191" n="e" s="T179">Manɨ </ts>
               <ts e="T181" id="Seg_1193" n="e" s="T180">körör </ts>
               <ts e="T182" id="Seg_1195" n="e" s="T181">dʼe </ts>
               <ts e="T183" id="Seg_1197" n="e" s="T182">baː </ts>
               <ts e="T184" id="Seg_1199" n="e" s="T183">kihi </ts>
               <ts e="T185" id="Seg_1201" n="e" s="T184">ojo </ts>
               <ts e="T186" id="Seg_1203" n="e" s="T185">ɨstanar, </ts>
               <ts e="T187" id="Seg_1205" n="e" s="T186">ɨttarɨn </ts>
               <ts e="T188" id="Seg_1207" n="e" s="T187">hɨrgatɨgar </ts>
               <ts e="T189" id="Seg_1209" n="e" s="T188">oloror </ts>
               <ts e="T190" id="Seg_1211" n="e" s="T189">daː </ts>
               <ts e="T191" id="Seg_1213" n="e" s="T190">barar </ts>
               <ts e="T192" id="Seg_1215" n="e" s="T191">di͡ekki </ts>
               <ts e="T193" id="Seg_1217" n="e" s="T192">bu͡olar. </ts>
               <ts e="T194" id="Seg_1219" n="e" s="T193">Kennin </ts>
               <ts e="T195" id="Seg_1221" n="e" s="T194">di͡eg </ts>
               <ts e="T196" id="Seg_1223" n="e" s="T195">kajɨspɨta </ts>
               <ts e="T197" id="Seg_1225" n="e" s="T196">biːr </ts>
               <ts e="T198" id="Seg_1227" n="e" s="T197">tüːleːk </ts>
               <ts e="T199" id="Seg_1229" n="e" s="T198">kihi </ts>
               <ts e="T200" id="Seg_1231" n="e" s="T199">hiten </ts>
               <ts e="T201" id="Seg_1233" n="e" s="T200">iher </ts>
               <ts e="T202" id="Seg_1235" n="e" s="T201">ebit, </ts>
               <ts e="T203" id="Seg_1237" n="e" s="T202">hiten </ts>
               <ts e="T204" id="Seg_1239" n="e" s="T203">hɨrgatɨn </ts>
               <ts e="T205" id="Seg_1241" n="e" s="T204">kenniki </ts>
               <ts e="T206" id="Seg_1243" n="e" s="T205">atagɨttan </ts>
               <ts e="T207" id="Seg_1245" n="e" s="T206">ɨlbɨta, </ts>
               <ts e="T208" id="Seg_1247" n="e" s="T207">ol </ts>
               <ts e="T209" id="Seg_1249" n="e" s="T208">kennigi </ts>
               <ts e="T210" id="Seg_1251" n="e" s="T209">ataga </ts>
               <ts e="T211" id="Seg_1253" n="e" s="T210">baː </ts>
               <ts e="T212" id="Seg_1255" n="e" s="T211">tüːleːk </ts>
               <ts e="T213" id="Seg_1257" n="e" s="T212">kihi </ts>
               <ts e="T214" id="Seg_1259" n="e" s="T213">iliːtiger </ts>
               <ts e="T215" id="Seg_1261" n="e" s="T214">kaːlbɨt. </ts>
               <ts e="T216" id="Seg_1263" n="e" s="T215">Ol </ts>
               <ts e="T217" id="Seg_1265" n="e" s="T216">ɨlaːččɨ </ts>
               <ts e="T218" id="Seg_1267" n="e" s="T217">kihi </ts>
               <ts e="T219" id="Seg_1269" n="e" s="T218">ü͡ögüleːbit: </ts>
               <ts e="T220" id="Seg_1271" n="e" s="T219">"Magan </ts>
               <ts e="T221" id="Seg_1273" n="e" s="T220">ɨraːktaːgɨ </ts>
               <ts e="T222" id="Seg_1275" n="e" s="T221">dʼonnoro, </ts>
               <ts e="T223" id="Seg_1277" n="e" s="T222">tönnör </ts>
               <ts e="T224" id="Seg_1279" n="e" s="T223">bu͡olaːjagɨt, </ts>
               <ts e="T225" id="Seg_1281" n="e" s="T224">bihigi </ts>
               <ts e="T226" id="Seg_1283" n="e" s="T225">bejebitin </ts>
               <ts e="T227" id="Seg_1285" n="e" s="T226">hurak </ts>
               <ts e="T228" id="Seg_1287" n="e" s="T227">ɨːtarɨ </ts>
               <ts e="T229" id="Seg_1289" n="e" s="T228">höbüleːbet </ts>
               <ts e="T230" id="Seg_1291" n="e" s="T229">bu͡olabɨt. </ts>
               <ts e="T231" id="Seg_1293" n="e" s="T230">Anɨ </ts>
               <ts e="T232" id="Seg_1295" n="e" s="T231">kim </ts>
               <ts e="T233" id="Seg_1297" n="e" s="T232">eme </ts>
               <ts e="T234" id="Seg_1299" n="e" s="T233">bihigi </ts>
               <ts e="T235" id="Seg_1301" n="e" s="T234">di͡eg </ts>
               <ts e="T236" id="Seg_1303" n="e" s="T235">keli͡e </ts>
               <ts e="T237" id="Seg_1305" n="e" s="T236">daː, </ts>
               <ts e="T238" id="Seg_1307" n="e" s="T237">bihigi </ts>
               <ts e="T239" id="Seg_1309" n="e" s="T238">tönnörü͡ökpüt </ts>
               <ts e="T240" id="Seg_1311" n="e" s="T239">hu͡oga", </ts>
               <ts e="T241" id="Seg_1313" n="e" s="T240">di͡ebit. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_1314" s="T0">BaRD_1930_HairyPeople_flk.001 (001.001)</ta>
            <ta e="T15" id="Seg_1315" s="T6">BaRD_1930_HairyPeople_flk.002 (001.002)</ta>
            <ta e="T21" id="Seg_1316" s="T15">BaRD_1930_HairyPeople_flk.003 (001.003)</ta>
            <ta e="T30" id="Seg_1317" s="T21">BaRD_1930_HairyPeople_flk.004 (001.004)</ta>
            <ta e="T36" id="Seg_1318" s="T30">BaRD_1930_HairyPeople_flk.005 (001.005)</ta>
            <ta e="T40" id="Seg_1319" s="T36">BaRD_1930_HairyPeople_flk.006 (001.006)</ta>
            <ta e="T48" id="Seg_1320" s="T40">BaRD_1930_HairyPeople_flk.007 (001.007)</ta>
            <ta e="T54" id="Seg_1321" s="T48">BaRD_1930_HairyPeople_flk.008 (001.008)</ta>
            <ta e="T58" id="Seg_1322" s="T54">BaRD_1930_HairyPeople_flk.009 (001.009)</ta>
            <ta e="T63" id="Seg_1323" s="T58">BaRD_1930_HairyPeople_flk.010 (001.010)</ta>
            <ta e="T84" id="Seg_1324" s="T63">BaRD_1930_HairyPeople_flk.011 (001.011)</ta>
            <ta e="T90" id="Seg_1325" s="T84">BaRD_1930_HairyPeople_flk.012 (001.012)</ta>
            <ta e="T100" id="Seg_1326" s="T90">BaRD_1930_HairyPeople_flk.013 (001.013)</ta>
            <ta e="T115" id="Seg_1327" s="T100">BaRD_1930_HairyPeople_flk.014 (001.014)</ta>
            <ta e="T118" id="Seg_1328" s="T115">BaRD_1930_HairyPeople_flk.015 (001.015)</ta>
            <ta e="T124" id="Seg_1329" s="T118">BaRD_1930_HairyPeople_flk.016 (001.016)</ta>
            <ta e="T131" id="Seg_1330" s="T124">BaRD_1930_HairyPeople_flk.017 (001.017)</ta>
            <ta e="T143" id="Seg_1331" s="T131">BaRD_1930_HairyPeople_flk.018 (001.018)</ta>
            <ta e="T147" id="Seg_1332" s="T143">BaRD_1930_HairyPeople_flk.019 (001.019)</ta>
            <ta e="T152" id="Seg_1333" s="T147">BaRD_1930_HairyPeople_flk.020 (001.020)</ta>
            <ta e="T171" id="Seg_1334" s="T152">BaRD_1930_HairyPeople_flk.021 (001.021)</ta>
            <ta e="T179" id="Seg_1335" s="T171">BaRD_1930_HairyPeople_flk.022 (001.022)</ta>
            <ta e="T193" id="Seg_1336" s="T179">BaRD_1930_HairyPeople_flk.023 (001.023)</ta>
            <ta e="T215" id="Seg_1337" s="T193">BaRD_1930_HairyPeople_flk.024 (001.024)</ta>
            <ta e="T219" id="Seg_1338" s="T215">BaRD_1930_HairyPeople_flk.025 (001.025)</ta>
            <ta e="T230" id="Seg_1339" s="T219">BaRD_1930_HairyPeople_flk.026 (001.026)</ta>
            <ta e="T241" id="Seg_1340" s="T230">BaRD_1930_HairyPeople_flk.027 (001.027)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_1341" s="T0">Былыр биир ыттаак киһи һиргэ барбыт.</ta>
            <ta e="T15" id="Seg_1342" s="T6">Бу баран иһэн улакан багайы киһи һуолун каарга көрбүт.</ta>
            <ta e="T21" id="Seg_1343" s="T15">һуола улакана түргэ һуолун курдук эбит.</ta>
            <ta e="T30" id="Seg_1344" s="T21">Баа көрөөччү киһи һиҥил эбит, тугу даа һоччо куттаммат.</ta>
            <ta e="T36" id="Seg_1345" s="T30">Һол эрээри кини дьулайбыт күнүс барыагын.</ta>
            <ta e="T40" id="Seg_1346" s="T36">Киэһэ буоларын кэтэспит олороорон.</ta>
            <ta e="T48" id="Seg_1347" s="T40">Бу киэһэ буолбутугар ытыгар олорон һуолу батан барар.</ta>
            <ta e="T54" id="Seg_1348" s="T48">Баа улакан дьон һуоллара элбээн барбыт.</ta>
            <ta e="T58" id="Seg_1349" s="T54">Улакан багайы балагаҥҥа тийбит.</ta>
            <ta e="T63" id="Seg_1350" s="T58">Улакан мийээ һэтиилэр туралларын көрбүт.</ta>
            <ta e="T84" id="Seg_1351" s="T63">Ыттарын төннөр һуолун диэг туруоран баран кэлээччи киһи балаган үрдүгэр таксыбыт, таксан үөлэскэ өҥөйөн көрбүтэ, уоттара умуллубут, чого эрэ кытара һытар.</ta>
            <ta e="T90" id="Seg_1352" s="T84">Арай истибитэ биир киһи олоҥколуур быһыылаак.</ta>
            <ta e="T100" id="Seg_1353" s="T90">— Маган ыраактаагы дьонноро бааллар үһү, — диэн кэпсиир эбит бу олоҥкоһут.</ta>
            <ta e="T115" id="Seg_1354" s="T100">Ол маган ыраактаагы дьонноро биир тугут тириититтэн таҥналлар, биир тугут буутун иккитэ күөстэнэллэр үһү, — диир.</ta>
            <ta e="T118" id="Seg_1355" s="T115">Маныага истээччилэрэ күлэллэр:</ta>
            <ta e="T124" id="Seg_1356" s="T118">— Дьэ эриэккэс дээ дьон эбиттэр, — диэн.</ta>
            <ta e="T131" id="Seg_1357" s="T124">Маны истэн баа киһи һүрдээктик көрүөн багарбыт.</ta>
            <ta e="T143" id="Seg_1358" s="T131">Былыргы киһиэкэ кататын гытта бииргэ баар буолааччы эмэк мас дуу, һигирии дуу.</ta>
            <ta e="T147" id="Seg_1359" s="T143">Манан кыымы күөдьүтэн уматаллар.</ta>
            <ta e="T152" id="Seg_1360" s="T147">Баа киһиэхэ һигирии баар эбит.</ta>
            <ta e="T171" id="Seg_1361" s="T152">Онтун убатан үөлэһинэн быракпыт, һигириитэ чокко түһэн эмискэ күлүбүр гыммыт, онно көрбүтэ уот иннитигэр биир киһи олоҥколуу олорор эбит.</ta>
            <ta e="T179" id="Seg_1362" s="T171">Бу киһи һыгынньак буолан баран барыта түү эбит.</ta>
            <ta e="T193" id="Seg_1363" s="T179">Маны көрөр дьэ баа киһи ойо ыстанар, ыттарын һыргатыгар олорор даа барар диэкки буолар.</ta>
            <ta e="T215" id="Seg_1364" s="T193">Кэннин диэг кайыспыта биир түүлээк киһи һитэн иһэр эбит, һитэн һыргатын кэнники атагыттан ылбыта, ол кэнниги атага баа түүлээк киһи илиитигэр каалбыт.</ta>
            <ta e="T219" id="Seg_1365" s="T215">Ол ылааччы киһи үөгүлээбит:</ta>
            <ta e="T230" id="Seg_1366" s="T219">— Маган ыраактаагы дьонноро, төннөр буолаайагыт, биһиги бэйэбитин һурак ыытары һөбүлээбэт буолабыт.</ta>
            <ta e="T241" id="Seg_1367" s="T230">Аны ким эмэ биһиги диэг кэлиэ даа, биһиги төннөрүөкпүт һуога! — диэбит.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_1368" s="T0">Bɨlɨr biːr ɨttaːk kihi hirge barbɨt. </ta>
            <ta e="T15" id="Seg_1369" s="T6">Bu baran ihen ulakan bagajɨ kihi hu͡olun kaːrga körbüt. </ta>
            <ta e="T21" id="Seg_1370" s="T15">Hu͡ola ulakana türge hu͡olun kurduk ebit. </ta>
            <ta e="T30" id="Seg_1371" s="T21">Baː köröːččü kihi hiŋil ebit, tugu daː hoččo kuttammat. </ta>
            <ta e="T36" id="Seg_1372" s="T30">Hol ereːri kini dʼulajbɨt künüs barɨ͡agɨn. </ta>
            <ta e="T40" id="Seg_1373" s="T36">Ki͡ehe bu͡olarɨn ketespit oloroːron. </ta>
            <ta e="T48" id="Seg_1374" s="T40">Bu ki͡ehe bu͡olbutugar ɨtɨgar oloron hu͡olu batan barar. </ta>
            <ta e="T54" id="Seg_1375" s="T48">Baː ulakan dʼon hu͡ollara elbeːn barbɨt. </ta>
            <ta e="T58" id="Seg_1376" s="T54">Ulakan bagajɨ balagaŋŋa tijbit. </ta>
            <ta e="T63" id="Seg_1377" s="T58">Ulakan mijeː hetiːler turallarɨn körbüt. </ta>
            <ta e="T84" id="Seg_1378" s="T63">ɨttarɨn tönnör hu͡olun di͡eg turu͡oran baran keleːčči kihi balagan ürdüger taksɨbɨt, taksan ü͡öleske öŋöjön körbüte, u͡ottara umullubut, čogo ere kɨtara hɨtar. </ta>
            <ta e="T90" id="Seg_1379" s="T84">Araj istibite biːr kihi oloŋkoluːr bɨhɨːlaːk. </ta>
            <ta e="T100" id="Seg_1380" s="T90">"Magan ɨraːktaːgɨ dʼonnoro baːllar ühü", di͡en kepsiːr ebit bu oloŋkohut. </ta>
            <ta e="T115" id="Seg_1381" s="T100">"Ol magan ɨraːktaːgɨ dʼonnoro biːr tugut tiriːtitten taŋnallar, biːr tugut buːtun ikkite kü͡östeneller ühü", diːr. </ta>
            <ta e="T118" id="Seg_1382" s="T115">Manɨ͡aga isteːččilere küleller: </ta>
            <ta e="T124" id="Seg_1383" s="T118">"Dʼe eri͡ekkes deː dʼon ebitter", di͡en. </ta>
            <ta e="T131" id="Seg_1384" s="T124">Manɨ isten baː kihi hürdeːktik körü͡ön bagarbɨt. </ta>
            <ta e="T143" id="Seg_1385" s="T131">Bɨlɨrgɨ kihi͡eke katatɨn gɨtta biːrge baːr bu͡olaːččɨ emek mas duː, higiriː duː. </ta>
            <ta e="T147" id="Seg_1386" s="T143">Manan kɨːmɨ kü͡ödʼüten umatallar. </ta>
            <ta e="T152" id="Seg_1387" s="T147">Baː kihi͡eke higiriː baːr ebit. </ta>
            <ta e="T171" id="Seg_1388" s="T152">Ontun ubatan ü͡ölehinen bɨrakpɨt, higiriːte čokko tühen emiske külübür gɨmmɨt, onno körbüte u͡ot innitiger biːr kihi oloŋkoluː oloror ebit. </ta>
            <ta e="T179" id="Seg_1389" s="T171">Bu kihi hɨgɨnnʼak bu͡olan baran barɨta tüː ebit. </ta>
            <ta e="T193" id="Seg_1390" s="T179">Manɨ körör dʼe baː kihi ojo ɨstanar, ɨttarɨn hɨrgatɨgar oloror daː barar di͡ekki bu͡olar. </ta>
            <ta e="T215" id="Seg_1391" s="T193">Kennin di͡eg kajɨspɨta biːr tüːleːk kihi hiten iher ebit, hiten hɨrgatɨn kenniki atagɨttan ɨlbɨta, ol kennigi ataga baː tüːleːk kihi iliːtiger kaːlbɨt. </ta>
            <ta e="T219" id="Seg_1392" s="T215">Ol ɨlaːččɨ kihi ü͡ögüleːbit:</ta>
            <ta e="T230" id="Seg_1393" s="T219">"Magan ɨraːktaːgɨ dʼonnoro, tönnör bu͡olaːjagɨt, bihigi bejebitin hurak ɨːtarɨ höbüleːbet bu͡olabɨt. </ta>
            <ta e="T241" id="Seg_1394" s="T230">Anɨ kim eme bihigi di͡eg keli͡e daː, bihigi tönnörü͡ökpüt hu͡oga", di͡ebit. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1395" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_1396" s="T1">biːr</ta>
            <ta e="T3" id="Seg_1397" s="T2">ɨt-taːk</ta>
            <ta e="T4" id="Seg_1398" s="T3">kihi</ta>
            <ta e="T5" id="Seg_1399" s="T4">hir-ge</ta>
            <ta e="T6" id="Seg_1400" s="T5">bar-bɨt</ta>
            <ta e="T7" id="Seg_1401" s="T6">bu</ta>
            <ta e="T8" id="Seg_1402" s="T7">bar-an</ta>
            <ta e="T9" id="Seg_1403" s="T8">ih-en</ta>
            <ta e="T10" id="Seg_1404" s="T9">ulakan</ta>
            <ta e="T11" id="Seg_1405" s="T10">bagajɨ</ta>
            <ta e="T12" id="Seg_1406" s="T11">kihi</ta>
            <ta e="T13" id="Seg_1407" s="T12">hu͡ol-u-n</ta>
            <ta e="T14" id="Seg_1408" s="T13">kaːr-ga</ta>
            <ta e="T15" id="Seg_1409" s="T14">kör-büt</ta>
            <ta e="T16" id="Seg_1410" s="T15">hu͡ol-a</ta>
            <ta e="T17" id="Seg_1411" s="T16">ulakan-a</ta>
            <ta e="T18" id="Seg_1412" s="T17">türge</ta>
            <ta e="T19" id="Seg_1413" s="T18">hu͡ol-u-n</ta>
            <ta e="T20" id="Seg_1414" s="T19">kurduk</ta>
            <ta e="T21" id="Seg_1415" s="T20">e-bit</ta>
            <ta e="T22" id="Seg_1416" s="T21">baː</ta>
            <ta e="T23" id="Seg_1417" s="T22">kör-öːččü</ta>
            <ta e="T24" id="Seg_1418" s="T23">kihi</ta>
            <ta e="T25" id="Seg_1419" s="T24">hiŋil</ta>
            <ta e="T26" id="Seg_1420" s="T25">e-bit</ta>
            <ta e="T27" id="Seg_1421" s="T26">tug-u</ta>
            <ta e="T28" id="Seg_1422" s="T27">daː</ta>
            <ta e="T29" id="Seg_1423" s="T28">h-oččo</ta>
            <ta e="T30" id="Seg_1424" s="T29">kuttam-mat</ta>
            <ta e="T31" id="Seg_1425" s="T30">hol</ta>
            <ta e="T32" id="Seg_1426" s="T31">ereːri</ta>
            <ta e="T33" id="Seg_1427" s="T32">kini</ta>
            <ta e="T34" id="Seg_1428" s="T33">dʼulaj-bɨt</ta>
            <ta e="T35" id="Seg_1429" s="T34">künüs</ta>
            <ta e="T36" id="Seg_1430" s="T35">bar-ɨ͡ag-ɨ-n</ta>
            <ta e="T37" id="Seg_1431" s="T36">ki͡ehe</ta>
            <ta e="T38" id="Seg_1432" s="T37">bu͡ol-ar-ɨ-n</ta>
            <ta e="T39" id="Seg_1433" s="T38">ketes-pit</ta>
            <ta e="T40" id="Seg_1434" s="T39">olor-oːr-on</ta>
            <ta e="T41" id="Seg_1435" s="T40">bu</ta>
            <ta e="T42" id="Seg_1436" s="T41">ki͡ehe</ta>
            <ta e="T43" id="Seg_1437" s="T42">bu͡ol-but-u-gar</ta>
            <ta e="T44" id="Seg_1438" s="T43">ɨt-ɨ-gar</ta>
            <ta e="T45" id="Seg_1439" s="T44">olor-on</ta>
            <ta e="T46" id="Seg_1440" s="T45">hu͡ol-u</ta>
            <ta e="T47" id="Seg_1441" s="T46">bat-an</ta>
            <ta e="T48" id="Seg_1442" s="T47">bar-ar</ta>
            <ta e="T49" id="Seg_1443" s="T48">baː</ta>
            <ta e="T50" id="Seg_1444" s="T49">ulakan</ta>
            <ta e="T51" id="Seg_1445" s="T50">dʼon</ta>
            <ta e="T52" id="Seg_1446" s="T51">hu͡ol-lara</ta>
            <ta e="T53" id="Seg_1447" s="T52">elbeː-n</ta>
            <ta e="T54" id="Seg_1448" s="T53">bar-bɨt</ta>
            <ta e="T55" id="Seg_1449" s="T54">ulakan</ta>
            <ta e="T56" id="Seg_1450" s="T55">bagajɨ</ta>
            <ta e="T57" id="Seg_1451" s="T56">balagaŋ-ŋa</ta>
            <ta e="T58" id="Seg_1452" s="T57">tij-bit</ta>
            <ta e="T59" id="Seg_1453" s="T58">ulakan</ta>
            <ta e="T60" id="Seg_1454" s="T59">mijeː</ta>
            <ta e="T61" id="Seg_1455" s="T60">hetiː-ler</ta>
            <ta e="T62" id="Seg_1456" s="T61">tur-al-larɨ-n</ta>
            <ta e="T63" id="Seg_1457" s="T62">kör-büt</ta>
            <ta e="T64" id="Seg_1458" s="T63">ɨt-tar-ɨ-n</ta>
            <ta e="T65" id="Seg_1459" s="T64">tönn-ör</ta>
            <ta e="T66" id="Seg_1460" s="T65">hu͡ol-u-n</ta>
            <ta e="T67" id="Seg_1461" s="T66">di͡eg</ta>
            <ta e="T68" id="Seg_1462" s="T67">turu͡or-an</ta>
            <ta e="T69" id="Seg_1463" s="T68">baran</ta>
            <ta e="T70" id="Seg_1464" s="T69">kel-eːčči</ta>
            <ta e="T71" id="Seg_1465" s="T70">kihi</ta>
            <ta e="T72" id="Seg_1466" s="T71">balagan</ta>
            <ta e="T73" id="Seg_1467" s="T72">ürd-ü-ger</ta>
            <ta e="T74" id="Seg_1468" s="T73">taks-ɨ-bɨt</ta>
            <ta e="T75" id="Seg_1469" s="T74">taks-an</ta>
            <ta e="T76" id="Seg_1470" s="T75">ü͡öles-ke</ta>
            <ta e="T77" id="Seg_1471" s="T76">öŋöj-ön</ta>
            <ta e="T78" id="Seg_1472" s="T77">kör-büt-e</ta>
            <ta e="T79" id="Seg_1473" s="T78">u͡ot-tara</ta>
            <ta e="T80" id="Seg_1474" s="T79">umull-u-but</ta>
            <ta e="T81" id="Seg_1475" s="T80">čog-o</ta>
            <ta e="T82" id="Seg_1476" s="T81">ere</ta>
            <ta e="T83" id="Seg_1477" s="T82">kɨtar-a</ta>
            <ta e="T84" id="Seg_1478" s="T83">hɨt-ar</ta>
            <ta e="T85" id="Seg_1479" s="T84">araj</ta>
            <ta e="T86" id="Seg_1480" s="T85">ist-i-bit-e</ta>
            <ta e="T87" id="Seg_1481" s="T86">biːr</ta>
            <ta e="T88" id="Seg_1482" s="T87">kihi</ta>
            <ta e="T89" id="Seg_1483" s="T88">oloŋko-luː-r</ta>
            <ta e="T90" id="Seg_1484" s="T89">bɨhɨːlaːk</ta>
            <ta e="T91" id="Seg_1485" s="T90">magan</ta>
            <ta e="T92" id="Seg_1486" s="T91">ɨraːktaːgɨ</ta>
            <ta e="T93" id="Seg_1487" s="T92">dʼon-noro</ta>
            <ta e="T94" id="Seg_1488" s="T93">baːl-lar</ta>
            <ta e="T95" id="Seg_1489" s="T94">ühü</ta>
            <ta e="T96" id="Seg_1490" s="T95">di͡e-n</ta>
            <ta e="T97" id="Seg_1491" s="T96">kepsiː-r</ta>
            <ta e="T98" id="Seg_1492" s="T97">e-bit</ta>
            <ta e="T99" id="Seg_1493" s="T98">bu</ta>
            <ta e="T100" id="Seg_1494" s="T99">oloŋko-hut</ta>
            <ta e="T101" id="Seg_1495" s="T100">ol</ta>
            <ta e="T102" id="Seg_1496" s="T101">magan</ta>
            <ta e="T103" id="Seg_1497" s="T102">ɨraːktaːgɨ</ta>
            <ta e="T104" id="Seg_1498" s="T103">dʼon-noro</ta>
            <ta e="T105" id="Seg_1499" s="T104">biːr</ta>
            <ta e="T106" id="Seg_1500" s="T105">tugut</ta>
            <ta e="T107" id="Seg_1501" s="T106">tiriː-ti-tten</ta>
            <ta e="T108" id="Seg_1502" s="T107">taŋn-al-lar</ta>
            <ta e="T109" id="Seg_1503" s="T108">biːr</ta>
            <ta e="T110" id="Seg_1504" s="T109">tugut</ta>
            <ta e="T111" id="Seg_1505" s="T110">buːt-u-n</ta>
            <ta e="T112" id="Seg_1506" s="T111">ikki-te</ta>
            <ta e="T113" id="Seg_1507" s="T112">kü͡ös-ten-el-ler</ta>
            <ta e="T114" id="Seg_1508" s="T113">ühü</ta>
            <ta e="T115" id="Seg_1509" s="T114">diː-r</ta>
            <ta e="T116" id="Seg_1510" s="T115">manɨ͡a-ga</ta>
            <ta e="T117" id="Seg_1511" s="T116">ist-eːčči-ler-e</ta>
            <ta e="T118" id="Seg_1512" s="T117">kül-el-ler</ta>
            <ta e="T119" id="Seg_1513" s="T118">dʼe</ta>
            <ta e="T120" id="Seg_1514" s="T119">eri͡ekkes</ta>
            <ta e="T121" id="Seg_1515" s="T120">deː</ta>
            <ta e="T122" id="Seg_1516" s="T121">dʼon</ta>
            <ta e="T123" id="Seg_1517" s="T122">e-bit-ter</ta>
            <ta e="T124" id="Seg_1518" s="T123">di͡e-n</ta>
            <ta e="T125" id="Seg_1519" s="T124">ma-nɨ</ta>
            <ta e="T126" id="Seg_1520" s="T125">ist-en</ta>
            <ta e="T127" id="Seg_1521" s="T126">baː</ta>
            <ta e="T128" id="Seg_1522" s="T127">kihi</ta>
            <ta e="T129" id="Seg_1523" s="T128">hürdeːk-tik</ta>
            <ta e="T130" id="Seg_1524" s="T129">kör-ü͡ö-n</ta>
            <ta e="T131" id="Seg_1525" s="T130">bagar-bɨt</ta>
            <ta e="T132" id="Seg_1526" s="T131">bɨlɨr-gɨ</ta>
            <ta e="T133" id="Seg_1527" s="T132">kihi͡e-ke</ta>
            <ta e="T134" id="Seg_1528" s="T133">katat-ɨ-n</ta>
            <ta e="T135" id="Seg_1529" s="T134">gɨtta</ta>
            <ta e="T136" id="Seg_1530" s="T135">biːrge</ta>
            <ta e="T137" id="Seg_1531" s="T136">baːr</ta>
            <ta e="T138" id="Seg_1532" s="T137">bu͡ol-aːččɨ</ta>
            <ta e="T139" id="Seg_1533" s="T138">emek</ta>
            <ta e="T140" id="Seg_1534" s="T139">mas</ta>
            <ta e="T141" id="Seg_1535" s="T140">duː</ta>
            <ta e="T142" id="Seg_1536" s="T141">higiriː</ta>
            <ta e="T143" id="Seg_1537" s="T142">duː</ta>
            <ta e="T144" id="Seg_1538" s="T143">ma-nan</ta>
            <ta e="T145" id="Seg_1539" s="T144">kɨːm-ɨ</ta>
            <ta e="T146" id="Seg_1540" s="T145">kü͡ödʼüt-en</ta>
            <ta e="T147" id="Seg_1541" s="T146">umat-al-lar</ta>
            <ta e="T148" id="Seg_1542" s="T147">baː</ta>
            <ta e="T149" id="Seg_1543" s="T148">kihi͡e-ke</ta>
            <ta e="T150" id="Seg_1544" s="T149">higiriː</ta>
            <ta e="T151" id="Seg_1545" s="T150">baːr</ta>
            <ta e="T152" id="Seg_1546" s="T151">e-bit</ta>
            <ta e="T153" id="Seg_1547" s="T152">on-tu-n</ta>
            <ta e="T154" id="Seg_1548" s="T153">ubat-an</ta>
            <ta e="T155" id="Seg_1549" s="T154">ü͡öleh-i-nen</ta>
            <ta e="T156" id="Seg_1550" s="T155">bɨrak-pɨt</ta>
            <ta e="T157" id="Seg_1551" s="T156">higiriː-te</ta>
            <ta e="T158" id="Seg_1552" s="T157">čok-ko</ta>
            <ta e="T159" id="Seg_1553" s="T158">tüh-en</ta>
            <ta e="T160" id="Seg_1554" s="T159">emiske</ta>
            <ta e="T161" id="Seg_1555" s="T160">külübür</ta>
            <ta e="T162" id="Seg_1556" s="T161">gɨm-mɨt</ta>
            <ta e="T163" id="Seg_1557" s="T162">onno</ta>
            <ta e="T164" id="Seg_1558" s="T163">kör-büt-e</ta>
            <ta e="T165" id="Seg_1559" s="T164">u͡ot</ta>
            <ta e="T166" id="Seg_1560" s="T165">inni-ti-ger</ta>
            <ta e="T167" id="Seg_1561" s="T166">biːr</ta>
            <ta e="T168" id="Seg_1562" s="T167">kihi</ta>
            <ta e="T169" id="Seg_1563" s="T168">oloŋko-l-uː</ta>
            <ta e="T170" id="Seg_1564" s="T169">olor-or</ta>
            <ta e="T171" id="Seg_1565" s="T170">e-bit</ta>
            <ta e="T172" id="Seg_1566" s="T171">bu</ta>
            <ta e="T173" id="Seg_1567" s="T172">kihi</ta>
            <ta e="T174" id="Seg_1568" s="T173">hɨgɨnnʼak</ta>
            <ta e="T175" id="Seg_1569" s="T174">bu͡ol-an</ta>
            <ta e="T176" id="Seg_1570" s="T175">baran</ta>
            <ta e="T177" id="Seg_1571" s="T176">barɨta</ta>
            <ta e="T178" id="Seg_1572" s="T177">tüː</ta>
            <ta e="T179" id="Seg_1573" s="T178">e-bit</ta>
            <ta e="T180" id="Seg_1574" s="T179">ma-nɨ</ta>
            <ta e="T181" id="Seg_1575" s="T180">kör-ör</ta>
            <ta e="T182" id="Seg_1576" s="T181">dʼe</ta>
            <ta e="T183" id="Seg_1577" s="T182">baː</ta>
            <ta e="T184" id="Seg_1578" s="T183">kihi</ta>
            <ta e="T185" id="Seg_1579" s="T184">oj-o</ta>
            <ta e="T186" id="Seg_1580" s="T185">ɨstan-ar</ta>
            <ta e="T187" id="Seg_1581" s="T186">ɨt-tarɨ-n</ta>
            <ta e="T188" id="Seg_1582" s="T187">hɨrga-tɨ-gar</ta>
            <ta e="T189" id="Seg_1583" s="T188">olor-or</ta>
            <ta e="T190" id="Seg_1584" s="T189">daː</ta>
            <ta e="T191" id="Seg_1585" s="T190">bar-ar</ta>
            <ta e="T192" id="Seg_1586" s="T191">di͡ekki</ta>
            <ta e="T193" id="Seg_1587" s="T192">bu͡ol-ar</ta>
            <ta e="T194" id="Seg_1588" s="T193">kenn-i-n</ta>
            <ta e="T195" id="Seg_1589" s="T194">di͡eg</ta>
            <ta e="T196" id="Seg_1590" s="T195">kaj-ɨ-s-pɨt-a</ta>
            <ta e="T197" id="Seg_1591" s="T196">biːr</ta>
            <ta e="T198" id="Seg_1592" s="T197">tüː-leːk</ta>
            <ta e="T199" id="Seg_1593" s="T198">kihi</ta>
            <ta e="T200" id="Seg_1594" s="T199">hit-en</ta>
            <ta e="T201" id="Seg_1595" s="T200">ih-er</ta>
            <ta e="T202" id="Seg_1596" s="T201">e-bit</ta>
            <ta e="T203" id="Seg_1597" s="T202">hit-en</ta>
            <ta e="T204" id="Seg_1598" s="T203">hɨrga-tɨ-n</ta>
            <ta e="T205" id="Seg_1599" s="T204">kenni-ki</ta>
            <ta e="T206" id="Seg_1600" s="T205">atag-ɨ-ttan</ta>
            <ta e="T207" id="Seg_1601" s="T206">ɨl-bɨt-a</ta>
            <ta e="T208" id="Seg_1602" s="T207">ol</ta>
            <ta e="T209" id="Seg_1603" s="T208">kenni-gi</ta>
            <ta e="T210" id="Seg_1604" s="T209">atag-a</ta>
            <ta e="T211" id="Seg_1605" s="T210">baː</ta>
            <ta e="T212" id="Seg_1606" s="T211">tüː-leːk</ta>
            <ta e="T213" id="Seg_1607" s="T212">kihi</ta>
            <ta e="T214" id="Seg_1608" s="T213">iliː-ti-ger</ta>
            <ta e="T215" id="Seg_1609" s="T214">kaːl-bɨt</ta>
            <ta e="T216" id="Seg_1610" s="T215">ol</ta>
            <ta e="T217" id="Seg_1611" s="T216">ɨl-aːččɨ</ta>
            <ta e="T218" id="Seg_1612" s="T217">kihi</ta>
            <ta e="T219" id="Seg_1613" s="T218">ü͡ögüleː-bit</ta>
            <ta e="T220" id="Seg_1614" s="T219">magan</ta>
            <ta e="T221" id="Seg_1615" s="T220">ɨraːktaːgɨ</ta>
            <ta e="T222" id="Seg_1616" s="T221">dʼon-nor-o</ta>
            <ta e="T223" id="Seg_1617" s="T222">tönn-ör</ta>
            <ta e="T224" id="Seg_1618" s="T223">bu͡ol-aːja-gɨt</ta>
            <ta e="T225" id="Seg_1619" s="T224">bihigi</ta>
            <ta e="T226" id="Seg_1620" s="T225">beje-biti-n</ta>
            <ta e="T227" id="Seg_1621" s="T226">hurak</ta>
            <ta e="T228" id="Seg_1622" s="T227">ɨːt-ar-ɨ</ta>
            <ta e="T229" id="Seg_1623" s="T228">höbüleː-bet</ta>
            <ta e="T230" id="Seg_1624" s="T229">bu͡ol-a-bɨt</ta>
            <ta e="T231" id="Seg_1625" s="T230">anɨ</ta>
            <ta e="T232" id="Seg_1626" s="T231">kim</ta>
            <ta e="T233" id="Seg_1627" s="T232">eme</ta>
            <ta e="T234" id="Seg_1628" s="T233">bihigi</ta>
            <ta e="T235" id="Seg_1629" s="T234">di͡eg</ta>
            <ta e="T236" id="Seg_1630" s="T235">kel-i͡e</ta>
            <ta e="T237" id="Seg_1631" s="T236">daː</ta>
            <ta e="T238" id="Seg_1632" s="T237">bihigi</ta>
            <ta e="T239" id="Seg_1633" s="T238">tönn-ö-r-ü͡ök-püt</ta>
            <ta e="T240" id="Seg_1634" s="T239">hu͡og-a</ta>
            <ta e="T241" id="Seg_1635" s="T240">di͡e-bit</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1636" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_1637" s="T1">biːr</ta>
            <ta e="T3" id="Seg_1638" s="T2">ɨt-LAːK</ta>
            <ta e="T4" id="Seg_1639" s="T3">kihi</ta>
            <ta e="T5" id="Seg_1640" s="T4">hir-GA</ta>
            <ta e="T6" id="Seg_1641" s="T5">bar-BIT</ta>
            <ta e="T7" id="Seg_1642" s="T6">bu</ta>
            <ta e="T8" id="Seg_1643" s="T7">bar-An</ta>
            <ta e="T9" id="Seg_1644" s="T8">is-An</ta>
            <ta e="T10" id="Seg_1645" s="T9">ulakan</ta>
            <ta e="T11" id="Seg_1646" s="T10">bagajɨ</ta>
            <ta e="T12" id="Seg_1647" s="T11">kihi</ta>
            <ta e="T13" id="Seg_1648" s="T12">hu͡ol-tI-n</ta>
            <ta e="T14" id="Seg_1649" s="T13">kaːr-GA</ta>
            <ta e="T15" id="Seg_1650" s="T14">kör-BIT</ta>
            <ta e="T16" id="Seg_1651" s="T15">hu͡ol-tA</ta>
            <ta e="T17" id="Seg_1652" s="T16">ulakan-tA</ta>
            <ta e="T18" id="Seg_1653" s="T17">türge</ta>
            <ta e="T19" id="Seg_1654" s="T18">hu͡ol-tI-n</ta>
            <ta e="T20" id="Seg_1655" s="T19">kurduk</ta>
            <ta e="T21" id="Seg_1656" s="T20">e-BIT</ta>
            <ta e="T22" id="Seg_1657" s="T21">bu</ta>
            <ta e="T23" id="Seg_1658" s="T22">kör-AːččI</ta>
            <ta e="T24" id="Seg_1659" s="T23">kihi</ta>
            <ta e="T25" id="Seg_1660" s="T24">hiŋil</ta>
            <ta e="T26" id="Seg_1661" s="T25">e-BIT</ta>
            <ta e="T27" id="Seg_1662" s="T26">tu͡ok-nI</ta>
            <ta e="T28" id="Seg_1663" s="T27">da</ta>
            <ta e="T29" id="Seg_1664" s="T28">h-oččo</ta>
            <ta e="T30" id="Seg_1665" s="T29">kuttan-BAT</ta>
            <ta e="T31" id="Seg_1666" s="T30">hol</ta>
            <ta e="T32" id="Seg_1667" s="T31">ereːri</ta>
            <ta e="T33" id="Seg_1668" s="T32">gini</ta>
            <ta e="T34" id="Seg_1669" s="T33">dʼulaj-BIT</ta>
            <ta e="T35" id="Seg_1670" s="T34">künüs</ta>
            <ta e="T36" id="Seg_1671" s="T35">bar-IAK-tI-n</ta>
            <ta e="T37" id="Seg_1672" s="T36">ki͡ehe</ta>
            <ta e="T38" id="Seg_1673" s="T37">bu͡ol-Ar-tI-n</ta>
            <ta e="T39" id="Seg_1674" s="T38">ketes-BIT</ta>
            <ta e="T40" id="Seg_1675" s="T39">olor-IAr-An</ta>
            <ta e="T41" id="Seg_1676" s="T40">bu</ta>
            <ta e="T42" id="Seg_1677" s="T41">ki͡ehe</ta>
            <ta e="T43" id="Seg_1678" s="T42">bu͡ol-BIT-tI-GAr</ta>
            <ta e="T44" id="Seg_1679" s="T43">ɨt-tI-GAr</ta>
            <ta e="T45" id="Seg_1680" s="T44">olor-An</ta>
            <ta e="T46" id="Seg_1681" s="T45">hu͡ol-nI</ta>
            <ta e="T47" id="Seg_1682" s="T46">bat-An</ta>
            <ta e="T48" id="Seg_1683" s="T47">bar-Ar</ta>
            <ta e="T49" id="Seg_1684" s="T48">bu</ta>
            <ta e="T50" id="Seg_1685" s="T49">ulakan</ta>
            <ta e="T51" id="Seg_1686" s="T50">dʼon</ta>
            <ta e="T52" id="Seg_1687" s="T51">hu͡ol-LArA</ta>
            <ta e="T53" id="Seg_1688" s="T52">elbeː-An</ta>
            <ta e="T54" id="Seg_1689" s="T53">bar-BIT</ta>
            <ta e="T55" id="Seg_1690" s="T54">ulakan</ta>
            <ta e="T56" id="Seg_1691" s="T55">bagajɨ</ta>
            <ta e="T57" id="Seg_1692" s="T56">balagan-GA</ta>
            <ta e="T58" id="Seg_1693" s="T57">tij-BIT</ta>
            <ta e="T59" id="Seg_1694" s="T58">ulakan</ta>
            <ta e="T60" id="Seg_1695" s="T59">mɨjaːn</ta>
            <ta e="T61" id="Seg_1696" s="T60">hetiː-LAr</ta>
            <ta e="T62" id="Seg_1697" s="T61">tur-Ar-LArI-n</ta>
            <ta e="T63" id="Seg_1698" s="T62">kör-BIT</ta>
            <ta e="T64" id="Seg_1699" s="T63">ɨt-LAr-tI-n</ta>
            <ta e="T65" id="Seg_1700" s="T64">tönün-Ar</ta>
            <ta e="T66" id="Seg_1701" s="T65">hu͡ol-tI-n</ta>
            <ta e="T67" id="Seg_1702" s="T66">dek</ta>
            <ta e="T68" id="Seg_1703" s="T67">turu͡or-An</ta>
            <ta e="T69" id="Seg_1704" s="T68">baran</ta>
            <ta e="T70" id="Seg_1705" s="T69">kel-AːččI</ta>
            <ta e="T71" id="Seg_1706" s="T70">kihi</ta>
            <ta e="T72" id="Seg_1707" s="T71">balagan</ta>
            <ta e="T73" id="Seg_1708" s="T72">ürüt-tI-GAr</ta>
            <ta e="T74" id="Seg_1709" s="T73">tagɨs-I-BIT</ta>
            <ta e="T75" id="Seg_1710" s="T74">tagɨs-An</ta>
            <ta e="T76" id="Seg_1711" s="T75">ü͡öles-GA</ta>
            <ta e="T77" id="Seg_1712" s="T76">öŋöj-An</ta>
            <ta e="T78" id="Seg_1713" s="T77">kör-BIT-tA</ta>
            <ta e="T79" id="Seg_1714" s="T78">u͡ot-LArA</ta>
            <ta e="T80" id="Seg_1715" s="T79">umulun-I-BIT</ta>
            <ta e="T81" id="Seg_1716" s="T80">čok-tA</ta>
            <ta e="T82" id="Seg_1717" s="T81">ere</ta>
            <ta e="T83" id="Seg_1718" s="T82">kɨtar-A</ta>
            <ta e="T84" id="Seg_1719" s="T83">hɨt-Ar</ta>
            <ta e="T85" id="Seg_1720" s="T84">agaj</ta>
            <ta e="T86" id="Seg_1721" s="T85">ihit-I-BIT-tA</ta>
            <ta e="T87" id="Seg_1722" s="T86">biːr</ta>
            <ta e="T88" id="Seg_1723" s="T87">kihi</ta>
            <ta e="T89" id="Seg_1724" s="T88">oloŋko-LAː-Ar</ta>
            <ta e="T90" id="Seg_1725" s="T89">bɨhɨːlaːk</ta>
            <ta e="T91" id="Seg_1726" s="T90">magan</ta>
            <ta e="T92" id="Seg_1727" s="T91">ɨraːktaːgɨ</ta>
            <ta e="T93" id="Seg_1728" s="T92">dʼon-LArA</ta>
            <ta e="T94" id="Seg_1729" s="T93">baːr-LAr</ta>
            <ta e="T95" id="Seg_1730" s="T94">ühü</ta>
            <ta e="T96" id="Seg_1731" s="T95">di͡e-An</ta>
            <ta e="T97" id="Seg_1732" s="T96">kepseː-Ar</ta>
            <ta e="T98" id="Seg_1733" s="T97">e-BIT</ta>
            <ta e="T99" id="Seg_1734" s="T98">bu</ta>
            <ta e="T100" id="Seg_1735" s="T99">oloŋko-ČIt</ta>
            <ta e="T101" id="Seg_1736" s="T100">ol</ta>
            <ta e="T102" id="Seg_1737" s="T101">magan</ta>
            <ta e="T103" id="Seg_1738" s="T102">ɨraːktaːgɨ</ta>
            <ta e="T104" id="Seg_1739" s="T103">dʼon-LArA</ta>
            <ta e="T105" id="Seg_1740" s="T104">biːr</ta>
            <ta e="T106" id="Seg_1741" s="T105">tugut</ta>
            <ta e="T107" id="Seg_1742" s="T106">tiriː-tI-ttAn</ta>
            <ta e="T108" id="Seg_1743" s="T107">taŋɨn-Ar-LAr</ta>
            <ta e="T109" id="Seg_1744" s="T108">biːr</ta>
            <ta e="T110" id="Seg_1745" s="T109">tugut</ta>
            <ta e="T111" id="Seg_1746" s="T110">buːt-tI-n</ta>
            <ta e="T112" id="Seg_1747" s="T111">ikki-TA</ta>
            <ta e="T113" id="Seg_1748" s="T112">kü͡ös-LAN-Ar-LAr</ta>
            <ta e="T114" id="Seg_1749" s="T113">ühü</ta>
            <ta e="T115" id="Seg_1750" s="T114">di͡e-Ar</ta>
            <ta e="T116" id="Seg_1751" s="T115">bu-GA</ta>
            <ta e="T117" id="Seg_1752" s="T116">ihit-AːččI-LAr-tA</ta>
            <ta e="T118" id="Seg_1753" s="T117">kül-Ar-LAr</ta>
            <ta e="T119" id="Seg_1754" s="T118">dʼe</ta>
            <ta e="T120" id="Seg_1755" s="T119">eri͡ekkes</ta>
            <ta e="T121" id="Seg_1756" s="T120">deː</ta>
            <ta e="T122" id="Seg_1757" s="T121">dʼon</ta>
            <ta e="T123" id="Seg_1758" s="T122">e-BIT-LAr</ta>
            <ta e="T124" id="Seg_1759" s="T123">di͡e-An</ta>
            <ta e="T125" id="Seg_1760" s="T124">bu-nI</ta>
            <ta e="T126" id="Seg_1761" s="T125">ihit-An</ta>
            <ta e="T127" id="Seg_1762" s="T126">bu</ta>
            <ta e="T128" id="Seg_1763" s="T127">kihi</ta>
            <ta e="T129" id="Seg_1764" s="T128">hürdeːk-LIk</ta>
            <ta e="T130" id="Seg_1765" s="T129">kör-IAK.[tI]-n</ta>
            <ta e="T131" id="Seg_1766" s="T130">bagar-BIT</ta>
            <ta e="T132" id="Seg_1767" s="T131">bɨlɨr-GI</ta>
            <ta e="T133" id="Seg_1768" s="T132">kihi-GA</ta>
            <ta e="T134" id="Seg_1769" s="T133">katat-tI-n</ta>
            <ta e="T135" id="Seg_1770" s="T134">kɨtta</ta>
            <ta e="T136" id="Seg_1771" s="T135">biːrge</ta>
            <ta e="T137" id="Seg_1772" s="T136">baːr</ta>
            <ta e="T138" id="Seg_1773" s="T137">bu͡ol-AːččI</ta>
            <ta e="T139" id="Seg_1774" s="T138">emek</ta>
            <ta e="T140" id="Seg_1775" s="T139">mas</ta>
            <ta e="T141" id="Seg_1776" s="T140">du͡o</ta>
            <ta e="T142" id="Seg_1777" s="T141">higiriː</ta>
            <ta e="T143" id="Seg_1778" s="T142">du͡o</ta>
            <ta e="T144" id="Seg_1779" s="T143">bu-nAn</ta>
            <ta e="T145" id="Seg_1780" s="T144">kɨːm-nI</ta>
            <ta e="T146" id="Seg_1781" s="T145">kü͡ödʼüt-An</ta>
            <ta e="T147" id="Seg_1782" s="T146">ubat-Ar-LAr</ta>
            <ta e="T148" id="Seg_1783" s="T147">bu</ta>
            <ta e="T149" id="Seg_1784" s="T148">kihi-GA</ta>
            <ta e="T150" id="Seg_1785" s="T149">higiriː</ta>
            <ta e="T151" id="Seg_1786" s="T150">baːr</ta>
            <ta e="T152" id="Seg_1787" s="T151">e-BIT</ta>
            <ta e="T153" id="Seg_1788" s="T152">ol-tI-n</ta>
            <ta e="T154" id="Seg_1789" s="T153">ubat-An</ta>
            <ta e="T155" id="Seg_1790" s="T154">ü͡öles-tI-nAn</ta>
            <ta e="T156" id="Seg_1791" s="T155">bɨrak-BIT</ta>
            <ta e="T157" id="Seg_1792" s="T156">higiriː-tA</ta>
            <ta e="T158" id="Seg_1793" s="T157">čok-GA</ta>
            <ta e="T159" id="Seg_1794" s="T158">tüs-An</ta>
            <ta e="T160" id="Seg_1795" s="T159">emiske</ta>
            <ta e="T161" id="Seg_1796" s="T160">külübür</ta>
            <ta e="T162" id="Seg_1797" s="T161">gɨn-BIT</ta>
            <ta e="T163" id="Seg_1798" s="T162">onno</ta>
            <ta e="T164" id="Seg_1799" s="T163">kör-BIT-tA</ta>
            <ta e="T165" id="Seg_1800" s="T164">u͡ot</ta>
            <ta e="T166" id="Seg_1801" s="T165">ilin-tI-GAr</ta>
            <ta e="T167" id="Seg_1802" s="T166">biːr</ta>
            <ta e="T168" id="Seg_1803" s="T167">kihi</ta>
            <ta e="T169" id="Seg_1804" s="T168">oloŋko-LAː-A</ta>
            <ta e="T170" id="Seg_1805" s="T169">olor-Ar</ta>
            <ta e="T171" id="Seg_1806" s="T170">e-BIT</ta>
            <ta e="T172" id="Seg_1807" s="T171">bu</ta>
            <ta e="T173" id="Seg_1808" s="T172">kihi</ta>
            <ta e="T174" id="Seg_1809" s="T173">hɨgɨnʼak</ta>
            <ta e="T175" id="Seg_1810" s="T174">bu͡ol-An</ta>
            <ta e="T176" id="Seg_1811" s="T175">baran</ta>
            <ta e="T177" id="Seg_1812" s="T176">barɨta</ta>
            <ta e="T178" id="Seg_1813" s="T177">tüː</ta>
            <ta e="T179" id="Seg_1814" s="T178">e-BIT</ta>
            <ta e="T180" id="Seg_1815" s="T179">bu-nI</ta>
            <ta e="T181" id="Seg_1816" s="T180">kör-Ar</ta>
            <ta e="T182" id="Seg_1817" s="T181">dʼe</ta>
            <ta e="T183" id="Seg_1818" s="T182">bu</ta>
            <ta e="T184" id="Seg_1819" s="T183">kihi</ta>
            <ta e="T185" id="Seg_1820" s="T184">oj-A</ta>
            <ta e="T186" id="Seg_1821" s="T185">ɨstan-Ar</ta>
            <ta e="T187" id="Seg_1822" s="T186">ɨt-LArI-n</ta>
            <ta e="T188" id="Seg_1823" s="T187">hɨrga-tI-GAr</ta>
            <ta e="T189" id="Seg_1824" s="T188">olor-Ar</ta>
            <ta e="T190" id="Seg_1825" s="T189">da</ta>
            <ta e="T191" id="Seg_1826" s="T190">bar-Ar</ta>
            <ta e="T192" id="Seg_1827" s="T191">di͡ekki</ta>
            <ta e="T193" id="Seg_1828" s="T192">bu͡ol-Ar</ta>
            <ta e="T194" id="Seg_1829" s="T193">kelin-tI-n</ta>
            <ta e="T195" id="Seg_1830" s="T194">dek</ta>
            <ta e="T196" id="Seg_1831" s="T195">kaj-I-s-BIT-tA</ta>
            <ta e="T197" id="Seg_1832" s="T196">biːr</ta>
            <ta e="T198" id="Seg_1833" s="T197">tüː-LAːK</ta>
            <ta e="T199" id="Seg_1834" s="T198">kihi</ta>
            <ta e="T200" id="Seg_1835" s="T199">hit-An</ta>
            <ta e="T201" id="Seg_1836" s="T200">is-Ar</ta>
            <ta e="T202" id="Seg_1837" s="T201">e-BIT</ta>
            <ta e="T203" id="Seg_1838" s="T202">hit-An</ta>
            <ta e="T204" id="Seg_1839" s="T203">hɨrga-tI-n</ta>
            <ta e="T205" id="Seg_1840" s="T204">kelin-GI</ta>
            <ta e="T206" id="Seg_1841" s="T205">atak-tI-ttAn</ta>
            <ta e="T207" id="Seg_1842" s="T206">ɨl-BIT-tA</ta>
            <ta e="T208" id="Seg_1843" s="T207">ol</ta>
            <ta e="T209" id="Seg_1844" s="T208">kelin-GI</ta>
            <ta e="T210" id="Seg_1845" s="T209">atak-tA</ta>
            <ta e="T211" id="Seg_1846" s="T210">bu</ta>
            <ta e="T212" id="Seg_1847" s="T211">tüː-LAːK</ta>
            <ta e="T213" id="Seg_1848" s="T212">kihi</ta>
            <ta e="T214" id="Seg_1849" s="T213">iliː-tI-GAr</ta>
            <ta e="T215" id="Seg_1850" s="T214">kaːl-BIT</ta>
            <ta e="T216" id="Seg_1851" s="T215">ol</ta>
            <ta e="T217" id="Seg_1852" s="T216">ɨl-AːččI</ta>
            <ta e="T218" id="Seg_1853" s="T217">kihi</ta>
            <ta e="T219" id="Seg_1854" s="T218">ü͡ögüleː-BIT</ta>
            <ta e="T220" id="Seg_1855" s="T219">magan</ta>
            <ta e="T221" id="Seg_1856" s="T220">ɨraːktaːgɨ</ta>
            <ta e="T222" id="Seg_1857" s="T221">dʼon-LAr-tA</ta>
            <ta e="T223" id="Seg_1858" s="T222">tönün-Ar</ta>
            <ta e="T224" id="Seg_1859" s="T223">bu͡ol-AːjA-GIt</ta>
            <ta e="T225" id="Seg_1860" s="T224">bihigi</ta>
            <ta e="T226" id="Seg_1861" s="T225">beje-BItI-n</ta>
            <ta e="T227" id="Seg_1862" s="T226">hurak</ta>
            <ta e="T228" id="Seg_1863" s="T227">ɨːt-Ar-nI</ta>
            <ta e="T229" id="Seg_1864" s="T228">höbüleː-BAT</ta>
            <ta e="T230" id="Seg_1865" s="T229">bu͡ol-A-BIt</ta>
            <ta e="T231" id="Seg_1866" s="T230">anɨ</ta>
            <ta e="T232" id="Seg_1867" s="T231">kim</ta>
            <ta e="T233" id="Seg_1868" s="T232">eme</ta>
            <ta e="T234" id="Seg_1869" s="T233">bihigi</ta>
            <ta e="T235" id="Seg_1870" s="T234">dek</ta>
            <ta e="T236" id="Seg_1871" s="T235">kel-IAK.[tA]</ta>
            <ta e="T237" id="Seg_1872" s="T236">da</ta>
            <ta e="T238" id="Seg_1873" s="T237">bihigi</ta>
            <ta e="T239" id="Seg_1874" s="T238">tönün-A-r-IAK-BIt</ta>
            <ta e="T240" id="Seg_1875" s="T239">hu͡ok-tA</ta>
            <ta e="T241" id="Seg_1876" s="T240">di͡e-BIT</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1877" s="T0">long.ago</ta>
            <ta e="T2" id="Seg_1878" s="T1">one</ta>
            <ta e="T3" id="Seg_1879" s="T2">dog-PROPR</ta>
            <ta e="T4" id="Seg_1880" s="T3">human.being.[NOM]</ta>
            <ta e="T5" id="Seg_1881" s="T4">tundra-DAT/LOC</ta>
            <ta e="T6" id="Seg_1882" s="T5">go-PST2.[3SG]</ta>
            <ta e="T7" id="Seg_1883" s="T6">this</ta>
            <ta e="T8" id="Seg_1884" s="T7">go-CVB.SEQ</ta>
            <ta e="T9" id="Seg_1885" s="T8">drive-CVB.SEQ</ta>
            <ta e="T10" id="Seg_1886" s="T9">big</ta>
            <ta e="T11" id="Seg_1887" s="T10">very</ta>
            <ta e="T12" id="Seg_1888" s="T11">human.being.[NOM]</ta>
            <ta e="T13" id="Seg_1889" s="T12">trace-3SG-ACC</ta>
            <ta e="T14" id="Seg_1890" s="T13">snow-DAT/LOC</ta>
            <ta e="T15" id="Seg_1891" s="T14">see-PST2.[3SG]</ta>
            <ta e="T16" id="Seg_1892" s="T15">trace-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_1893" s="T16">size-3SG.[NOM]</ta>
            <ta e="T18" id="Seg_1894" s="T17">shield.of.a.hunter.[NOM]</ta>
            <ta e="T19" id="Seg_1895" s="T18">trace-3SG-ACC</ta>
            <ta e="T20" id="Seg_1896" s="T19">like</ta>
            <ta e="T21" id="Seg_1897" s="T20">be-PST2.[3SG]</ta>
            <ta e="T22" id="Seg_1898" s="T21">this</ta>
            <ta e="T23" id="Seg_1899" s="T22">see-PTCP.HAB</ta>
            <ta e="T24" id="Seg_1900" s="T23">human.being.[NOM]</ta>
            <ta e="T25" id="Seg_1901" s="T24">young.[NOM]</ta>
            <ta e="T26" id="Seg_1902" s="T25">be-PST2.[3SG]</ta>
            <ta e="T27" id="Seg_1903" s="T26">what-ACC</ta>
            <ta e="T28" id="Seg_1904" s="T27">NEG</ta>
            <ta e="T29" id="Seg_1905" s="T28">EMPH-so.much</ta>
            <ta e="T30" id="Seg_1906" s="T29">be.afraid-NEG.[3SG]</ta>
            <ta e="T31" id="Seg_1907" s="T30">that.EMPH.[NOM]</ta>
            <ta e="T32" id="Seg_1908" s="T31">despite</ta>
            <ta e="T33" id="Seg_1909" s="T32">3SG.[NOM]</ta>
            <ta e="T34" id="Seg_1910" s="T33">be.rather.afraid-PST2.[3SG]</ta>
            <ta e="T35" id="Seg_1911" s="T34">by.day</ta>
            <ta e="T36" id="Seg_1912" s="T35">go-PTCP.FUT-3SG-ACC</ta>
            <ta e="T37" id="Seg_1913" s="T36">evening.[NOM]</ta>
            <ta e="T38" id="Seg_1914" s="T37">become-PTCP.PRS-3SG-ACC</ta>
            <ta e="T39" id="Seg_1915" s="T38">await-PST2.[3SG]</ta>
            <ta e="T40" id="Seg_1916" s="T39">sit-CAUS-CVB.SEQ</ta>
            <ta e="T41" id="Seg_1917" s="T40">this</ta>
            <ta e="T42" id="Seg_1918" s="T41">evening.[NOM]</ta>
            <ta e="T43" id="Seg_1919" s="T42">be-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T44" id="Seg_1920" s="T43">dog-3SG-DAT/LOC</ta>
            <ta e="T45" id="Seg_1921" s="T44">sit.down-CVB.SEQ</ta>
            <ta e="T46" id="Seg_1922" s="T45">trace-ACC</ta>
            <ta e="T47" id="Seg_1923" s="T46">follow-CVB.SEQ</ta>
            <ta e="T48" id="Seg_1924" s="T47">go-PRS.[3SG]</ta>
            <ta e="T49" id="Seg_1925" s="T48">this</ta>
            <ta e="T50" id="Seg_1926" s="T49">big</ta>
            <ta e="T51" id="Seg_1927" s="T50">people.[NOM]</ta>
            <ta e="T52" id="Seg_1928" s="T51">trace-3PL.[NOM]</ta>
            <ta e="T53" id="Seg_1929" s="T52">become.more-CVB.SEQ</ta>
            <ta e="T54" id="Seg_1930" s="T53">go-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_1931" s="T54">big</ta>
            <ta e="T56" id="Seg_1932" s="T55">very</ta>
            <ta e="T57" id="Seg_1933" s="T56">yurt-DAT/LOC</ta>
            <ta e="T58" id="Seg_1934" s="T57">reach-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_1935" s="T58">big</ta>
            <ta e="T60" id="Seg_1936" s="T59">very</ta>
            <ta e="T61" id="Seg_1937" s="T60">sledge-PL.[NOM]</ta>
            <ta e="T62" id="Seg_1938" s="T61">stand-PTCP.PRS-3PL-ACC</ta>
            <ta e="T63" id="Seg_1939" s="T62">see-PST2.[3SG]</ta>
            <ta e="T64" id="Seg_1940" s="T63">dog-PL-3SG-ACC</ta>
            <ta e="T65" id="Seg_1941" s="T64">come.back-PTCP.PRS</ta>
            <ta e="T66" id="Seg_1942" s="T65">way-3SG-ACC</ta>
            <ta e="T67" id="Seg_1943" s="T66">to</ta>
            <ta e="T68" id="Seg_1944" s="T67">place-CVB.SEQ</ta>
            <ta e="T69" id="Seg_1945" s="T68">after</ta>
            <ta e="T70" id="Seg_1946" s="T69">come-PTCP.HAB</ta>
            <ta e="T71" id="Seg_1947" s="T70">human.being.[NOM]</ta>
            <ta e="T72" id="Seg_1948" s="T71">yurt.[NOM]</ta>
            <ta e="T73" id="Seg_1949" s="T72">upper.part-3SG-DAT/LOC</ta>
            <ta e="T74" id="Seg_1950" s="T73">go.out-EP-PST2.[3SG]</ta>
            <ta e="T75" id="Seg_1951" s="T74">go.out-CVB.SEQ</ta>
            <ta e="T76" id="Seg_1952" s="T75">chimney-DAT/LOC</ta>
            <ta e="T77" id="Seg_1953" s="T76">look.into-CVB.SEQ</ta>
            <ta e="T78" id="Seg_1954" s="T77">see-PST2-3SG</ta>
            <ta e="T79" id="Seg_1955" s="T78">fire-3PL.[NOM]</ta>
            <ta e="T80" id="Seg_1956" s="T79">go.out-EP-PST2.[3SG]</ta>
            <ta e="T81" id="Seg_1957" s="T80">coal-3SG.[NOM]</ta>
            <ta e="T82" id="Seg_1958" s="T81">just</ta>
            <ta e="T83" id="Seg_1959" s="T82">get.red-CVB.SIM</ta>
            <ta e="T84" id="Seg_1960" s="T83">lie-PRS.[3SG]</ta>
            <ta e="T85" id="Seg_1961" s="T84">suddenly</ta>
            <ta e="T86" id="Seg_1962" s="T85">hear-EP-PST2-3SG</ta>
            <ta e="T87" id="Seg_1963" s="T86">one</ta>
            <ta e="T88" id="Seg_1964" s="T87">human.being.[NOM]</ta>
            <ta e="T89" id="Seg_1965" s="T88">tale-VBZ-PRS.[3SG]</ta>
            <ta e="T90" id="Seg_1966" s="T89">apparently</ta>
            <ta e="T91" id="Seg_1967" s="T90">white</ta>
            <ta e="T92" id="Seg_1968" s="T91">czar.[NOM]</ta>
            <ta e="T93" id="Seg_1969" s="T92">people-3PL.[NOM]</ta>
            <ta e="T94" id="Seg_1970" s="T93">there.is-3PL</ta>
            <ta e="T95" id="Seg_1971" s="T94">it.is.said</ta>
            <ta e="T96" id="Seg_1972" s="T95">say-CVB.SEQ</ta>
            <ta e="T97" id="Seg_1973" s="T96">tell-PTCP.PRS</ta>
            <ta e="T98" id="Seg_1974" s="T97">be-PST2.[3SG]</ta>
            <ta e="T99" id="Seg_1975" s="T98">this</ta>
            <ta e="T100" id="Seg_1976" s="T99">tale-AG.[NOM]</ta>
            <ta e="T101" id="Seg_1977" s="T100">that</ta>
            <ta e="T102" id="Seg_1978" s="T101">white</ta>
            <ta e="T103" id="Seg_1979" s="T102">czar.[NOM]</ta>
            <ta e="T104" id="Seg_1980" s="T103">people-3PL.[NOM]</ta>
            <ta e="T105" id="Seg_1981" s="T104">one</ta>
            <ta e="T106" id="Seg_1982" s="T105">reindeer.calf.[NOM]</ta>
            <ta e="T107" id="Seg_1983" s="T106">fur-3SG-ABL</ta>
            <ta e="T108" id="Seg_1984" s="T107">dress-PRS-3PL</ta>
            <ta e="T109" id="Seg_1985" s="T108">one</ta>
            <ta e="T110" id="Seg_1986" s="T109">reindeer.calf.[NOM]</ta>
            <ta e="T111" id="Seg_1987" s="T110">hind.leg-3SG-ACC</ta>
            <ta e="T112" id="Seg_1988" s="T111">two-MLTP</ta>
            <ta e="T113" id="Seg_1989" s="T112">meal-VBZ-PRS-3PL</ta>
            <ta e="T114" id="Seg_1990" s="T113">it.is.said</ta>
            <ta e="T115" id="Seg_1991" s="T114">say-PRS.[3SG]</ta>
            <ta e="T116" id="Seg_1992" s="T115">this-DAT/LOC</ta>
            <ta e="T117" id="Seg_1993" s="T116">hear-PTCP.HAB-PL-3SG.[NOM]</ta>
            <ta e="T118" id="Seg_1994" s="T117">laugh-PRS-3PL</ta>
            <ta e="T119" id="Seg_1995" s="T118">well</ta>
            <ta e="T120" id="Seg_1996" s="T119">strange</ta>
            <ta e="T121" id="Seg_1997" s="T120">EMPH</ta>
            <ta e="T122" id="Seg_1998" s="T121">people.[NOM]</ta>
            <ta e="T123" id="Seg_1999" s="T122">be-PST2-3PL</ta>
            <ta e="T124" id="Seg_2000" s="T123">say-CVB.SEQ</ta>
            <ta e="T125" id="Seg_2001" s="T124">this-ACC</ta>
            <ta e="T126" id="Seg_2002" s="T125">hear-CVB.SEQ</ta>
            <ta e="T127" id="Seg_2003" s="T126">this</ta>
            <ta e="T128" id="Seg_2004" s="T127">human.being.[NOM]</ta>
            <ta e="T129" id="Seg_2005" s="T128">very-ADVZ</ta>
            <ta e="T130" id="Seg_2006" s="T129">see-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T131" id="Seg_2007" s="T130">want-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_2008" s="T131">long.ago-ADJZ</ta>
            <ta e="T133" id="Seg_2009" s="T132">human.being-DAT/LOC</ta>
            <ta e="T134" id="Seg_2010" s="T133">flintstone-3SG-ACC</ta>
            <ta e="T135" id="Seg_2011" s="T134">with</ta>
            <ta e="T136" id="Seg_2012" s="T135">together</ta>
            <ta e="T137" id="Seg_2013" s="T136">there.is</ta>
            <ta e="T138" id="Seg_2014" s="T137">be-HAB.[3SG]</ta>
            <ta e="T139" id="Seg_2015" s="T138">rotten</ta>
            <ta e="T140" id="Seg_2016" s="T139">wood.[NOM]</ta>
            <ta e="T141" id="Seg_2017" s="T140">MOD</ta>
            <ta e="T142" id="Seg_2018" s="T141">dry.splints.[NOM]</ta>
            <ta e="T143" id="Seg_2019" s="T142">Q</ta>
            <ta e="T144" id="Seg_2020" s="T143">this-INSTR</ta>
            <ta e="T145" id="Seg_2021" s="T144">spark-ACC</ta>
            <ta e="T146" id="Seg_2022" s="T145">inflame-CVB.SEQ</ta>
            <ta e="T147" id="Seg_2023" s="T146">light-PRS-3PL</ta>
            <ta e="T148" id="Seg_2024" s="T147">this</ta>
            <ta e="T149" id="Seg_2025" s="T148">human.being-DAT/LOC</ta>
            <ta e="T150" id="Seg_2026" s="T149">dry.splints.[NOM]</ta>
            <ta e="T151" id="Seg_2027" s="T150">there.is</ta>
            <ta e="T152" id="Seg_2028" s="T151">be-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_2029" s="T152">that-3SG-ACC</ta>
            <ta e="T154" id="Seg_2030" s="T153">light-CVB.SEQ</ta>
            <ta e="T155" id="Seg_2031" s="T154">chimney-3SG-INSTR</ta>
            <ta e="T156" id="Seg_2032" s="T155">throw-PST2.[3SG]</ta>
            <ta e="T157" id="Seg_2033" s="T156">dry.splints-3SG.[NOM]</ta>
            <ta e="T158" id="Seg_2034" s="T157">coal-DAT/LOC</ta>
            <ta e="T159" id="Seg_2035" s="T158">fall-CVB.SEQ</ta>
            <ta e="T160" id="Seg_2036" s="T159">unexpectedly</ta>
            <ta e="T161" id="Seg_2037" s="T160">blaze.[NOM]</ta>
            <ta e="T162" id="Seg_2038" s="T161">make-PST2.[3SG]</ta>
            <ta e="T163" id="Seg_2039" s="T162">there</ta>
            <ta e="T164" id="Seg_2040" s="T163">see-PST2-3SG</ta>
            <ta e="T165" id="Seg_2041" s="T164">fire.[NOM]</ta>
            <ta e="T166" id="Seg_2042" s="T165">front-3SG-DAT/LOC</ta>
            <ta e="T167" id="Seg_2043" s="T166">one</ta>
            <ta e="T168" id="Seg_2044" s="T167">human.being.[NOM]</ta>
            <ta e="T169" id="Seg_2045" s="T168">tale-VBZ-CVB.SIM</ta>
            <ta e="T170" id="Seg_2046" s="T169">sit-PTCP.PRS</ta>
            <ta e="T171" id="Seg_2047" s="T170">be-PST2.[3SG]</ta>
            <ta e="T172" id="Seg_2048" s="T171">this</ta>
            <ta e="T173" id="Seg_2049" s="T172">human.being.[NOM]</ta>
            <ta e="T174" id="Seg_2050" s="T173">naked.[NOM]</ta>
            <ta e="T175" id="Seg_2051" s="T174">be-CVB.SEQ</ta>
            <ta e="T176" id="Seg_2052" s="T175">after</ta>
            <ta e="T177" id="Seg_2053" s="T176">completely</ta>
            <ta e="T178" id="Seg_2054" s="T177">animal.hair.[NOM]</ta>
            <ta e="T179" id="Seg_2055" s="T178">be-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_2056" s="T179">this-ACC</ta>
            <ta e="T181" id="Seg_2057" s="T180">see-PRS.[3SG]</ta>
            <ta e="T182" id="Seg_2058" s="T181">well</ta>
            <ta e="T183" id="Seg_2059" s="T182">this</ta>
            <ta e="T184" id="Seg_2060" s="T183">human.being.[NOM]</ta>
            <ta e="T185" id="Seg_2061" s="T184">jump-CVB.SIM</ta>
            <ta e="T186" id="Seg_2062" s="T185">break-PRS.[3SG]</ta>
            <ta e="T187" id="Seg_2063" s="T186">dog-3PL-GEN</ta>
            <ta e="T188" id="Seg_2064" s="T187">sledge-3SG-DAT/LOC</ta>
            <ta e="T189" id="Seg_2065" s="T188">sit.down-PRS.[3SG]</ta>
            <ta e="T190" id="Seg_2066" s="T189">and</ta>
            <ta e="T191" id="Seg_2067" s="T190">go-PTCP.PRS</ta>
            <ta e="T192" id="Seg_2068" s="T191">in.the.direction</ta>
            <ta e="T193" id="Seg_2069" s="T192">be-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_2070" s="T193">back-3SG-ACC</ta>
            <ta e="T195" id="Seg_2071" s="T194">to</ta>
            <ta e="T196" id="Seg_2072" s="T195">follow-EP-RECP/COLL-PST2-3SG</ta>
            <ta e="T197" id="Seg_2073" s="T196">one</ta>
            <ta e="T198" id="Seg_2074" s="T197">animal.hair-PROPR</ta>
            <ta e="T199" id="Seg_2075" s="T198">human.being.[NOM]</ta>
            <ta e="T200" id="Seg_2076" s="T199">chase-CVB.SEQ</ta>
            <ta e="T201" id="Seg_2077" s="T200">go-PTCP.PRS</ta>
            <ta e="T202" id="Seg_2078" s="T201">be-PST2.[3SG]</ta>
            <ta e="T203" id="Seg_2079" s="T202">chase-CVB.SEQ</ta>
            <ta e="T204" id="Seg_2080" s="T203">sledge-3SG-GEN</ta>
            <ta e="T205" id="Seg_2081" s="T204">back-ADJZ</ta>
            <ta e="T206" id="Seg_2082" s="T205">leg-3SG-ABL</ta>
            <ta e="T207" id="Seg_2083" s="T206">take-PST2-3SG</ta>
            <ta e="T208" id="Seg_2084" s="T207">that</ta>
            <ta e="T209" id="Seg_2085" s="T208">back-ADJZ</ta>
            <ta e="T210" id="Seg_2086" s="T209">leg-3SG.[NOM]</ta>
            <ta e="T211" id="Seg_2087" s="T210">this</ta>
            <ta e="T212" id="Seg_2088" s="T211">animal.hair-PROPR</ta>
            <ta e="T213" id="Seg_2089" s="T212">human.being.[NOM]</ta>
            <ta e="T214" id="Seg_2090" s="T213">hand-3SG-DAT/LOC</ta>
            <ta e="T215" id="Seg_2091" s="T214">stay-PST2.[3SG]</ta>
            <ta e="T216" id="Seg_2092" s="T215">that</ta>
            <ta e="T217" id="Seg_2093" s="T216">take-PTCP.HAB</ta>
            <ta e="T218" id="Seg_2094" s="T217">human.being.[NOM]</ta>
            <ta e="T219" id="Seg_2095" s="T218">shout-PST2.[3SG]</ta>
            <ta e="T220" id="Seg_2096" s="T219">white</ta>
            <ta e="T221" id="Seg_2097" s="T220">czar.[NOM]</ta>
            <ta e="T222" id="Seg_2098" s="T221">people-PL-3SG.[NOM]</ta>
            <ta e="T223" id="Seg_2099" s="T222">come.back-PTCP.PRS</ta>
            <ta e="T224" id="Seg_2100" s="T223">be-APPR-2PL</ta>
            <ta e="T225" id="Seg_2101" s="T224">1PL.[NOM]</ta>
            <ta e="T226" id="Seg_2102" s="T225">self-1PL-GEN</ta>
            <ta e="T227" id="Seg_2103" s="T226">message.[NOM]</ta>
            <ta e="T228" id="Seg_2104" s="T227">send-PTCP.PRS-ACC</ta>
            <ta e="T229" id="Seg_2105" s="T228">agree-NEG.PTCP</ta>
            <ta e="T230" id="Seg_2106" s="T229">be-PRS-1PL</ta>
            <ta e="T231" id="Seg_2107" s="T230">now</ta>
            <ta e="T232" id="Seg_2108" s="T231">who.[NOM]</ta>
            <ta e="T233" id="Seg_2109" s="T232">INDEF</ta>
            <ta e="T234" id="Seg_2110" s="T233">1PL.[NOM]</ta>
            <ta e="T235" id="Seg_2111" s="T234">to</ta>
            <ta e="T236" id="Seg_2112" s="T235">come-FUT.[3SG]</ta>
            <ta e="T237" id="Seg_2113" s="T236">and</ta>
            <ta e="T238" id="Seg_2114" s="T237">1PL.[NOM]</ta>
            <ta e="T239" id="Seg_2115" s="T238">come.back-EP-CAUS-FUT-1PL</ta>
            <ta e="T240" id="Seg_2116" s="T239">NEG-3SG</ta>
            <ta e="T241" id="Seg_2117" s="T240">say-PST2.[3SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_2118" s="T0">vor.langer.Zeit</ta>
            <ta e="T2" id="Seg_2119" s="T1">eins</ta>
            <ta e="T3" id="Seg_2120" s="T2">Hund-PROPR</ta>
            <ta e="T4" id="Seg_2121" s="T3">Mensch.[NOM]</ta>
            <ta e="T5" id="Seg_2122" s="T4">Tundra-DAT/LOC</ta>
            <ta e="T6" id="Seg_2123" s="T5">gehen-PST2.[3SG]</ta>
            <ta e="T7" id="Seg_2124" s="T6">dieses</ta>
            <ta e="T8" id="Seg_2125" s="T7">gehen-CVB.SEQ</ta>
            <ta e="T9" id="Seg_2126" s="T8">fahren-CVB.SEQ</ta>
            <ta e="T10" id="Seg_2127" s="T9">groß</ta>
            <ta e="T11" id="Seg_2128" s="T10">sehr</ta>
            <ta e="T12" id="Seg_2129" s="T11">Mensch.[NOM]</ta>
            <ta e="T13" id="Seg_2130" s="T12">Spur-3SG-ACC</ta>
            <ta e="T14" id="Seg_2131" s="T13">Schnee-DAT/LOC</ta>
            <ta e="T15" id="Seg_2132" s="T14">sehen-PST2.[3SG]</ta>
            <ta e="T16" id="Seg_2133" s="T15">Spur-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_2134" s="T16">Größe-3SG.[NOM]</ta>
            <ta e="T18" id="Seg_2135" s="T17">Schutzschild.bei.der.Jagd.[NOM]</ta>
            <ta e="T19" id="Seg_2136" s="T18">Spur-3SG-ACC</ta>
            <ta e="T20" id="Seg_2137" s="T19">wie</ta>
            <ta e="T21" id="Seg_2138" s="T20">sein-PST2.[3SG]</ta>
            <ta e="T22" id="Seg_2139" s="T21">dieses</ta>
            <ta e="T23" id="Seg_2140" s="T22">sehen-PTCP.HAB</ta>
            <ta e="T24" id="Seg_2141" s="T23">Mensch.[NOM]</ta>
            <ta e="T25" id="Seg_2142" s="T24">jung.[NOM]</ta>
            <ta e="T26" id="Seg_2143" s="T25">sein-PST2.[3SG]</ta>
            <ta e="T27" id="Seg_2144" s="T26">was-ACC</ta>
            <ta e="T28" id="Seg_2145" s="T27">NEG</ta>
            <ta e="T29" id="Seg_2146" s="T28">EMPH-so.viel</ta>
            <ta e="T30" id="Seg_2147" s="T29">Angst.haben-NEG.[3SG]</ta>
            <ta e="T31" id="Seg_2148" s="T30">jenes.EMPH.[NOM]</ta>
            <ta e="T32" id="Seg_2149" s="T31">trotz</ta>
            <ta e="T33" id="Seg_2150" s="T32">3SG.[NOM]</ta>
            <ta e="T34" id="Seg_2151" s="T33">etwas.Angst.haben-PST2.[3SG]</ta>
            <ta e="T35" id="Seg_2152" s="T34">am.Tag</ta>
            <ta e="T36" id="Seg_2153" s="T35">gehen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T37" id="Seg_2154" s="T36">Abend.[NOM]</ta>
            <ta e="T38" id="Seg_2155" s="T37">werden-PTCP.PRS-3SG-ACC</ta>
            <ta e="T39" id="Seg_2156" s="T38">erwarten-PST2.[3SG]</ta>
            <ta e="T40" id="Seg_2157" s="T39">sitzen-CAUS-CVB.SEQ</ta>
            <ta e="T41" id="Seg_2158" s="T40">dieses</ta>
            <ta e="T42" id="Seg_2159" s="T41">Abend.[NOM]</ta>
            <ta e="T43" id="Seg_2160" s="T42">sein-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T44" id="Seg_2161" s="T43">Hund-3SG-DAT/LOC</ta>
            <ta e="T45" id="Seg_2162" s="T44">sich.setzen-CVB.SEQ</ta>
            <ta e="T46" id="Seg_2163" s="T45">Spur-ACC</ta>
            <ta e="T47" id="Seg_2164" s="T46">folgen-CVB.SEQ</ta>
            <ta e="T48" id="Seg_2165" s="T47">gehen-PRS.[3SG]</ta>
            <ta e="T49" id="Seg_2166" s="T48">dieses</ta>
            <ta e="T50" id="Seg_2167" s="T49">groß</ta>
            <ta e="T51" id="Seg_2168" s="T50">Leute.[NOM]</ta>
            <ta e="T52" id="Seg_2169" s="T51">Spur-3PL.[NOM]</ta>
            <ta e="T53" id="Seg_2170" s="T52">mehr.werden-CVB.SEQ</ta>
            <ta e="T54" id="Seg_2171" s="T53">gehen-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_2172" s="T54">groß</ta>
            <ta e="T56" id="Seg_2173" s="T55">sehr</ta>
            <ta e="T57" id="Seg_2174" s="T56">Jurte-DAT/LOC</ta>
            <ta e="T58" id="Seg_2175" s="T57">ankommen-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_2176" s="T58">groß</ta>
            <ta e="T60" id="Seg_2177" s="T59">sehr</ta>
            <ta e="T61" id="Seg_2178" s="T60">Schlitten-PL.[NOM]</ta>
            <ta e="T62" id="Seg_2179" s="T61">stehen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T63" id="Seg_2180" s="T62">sehen-PST2.[3SG]</ta>
            <ta e="T64" id="Seg_2181" s="T63">Hund-PL-3SG-ACC</ta>
            <ta e="T65" id="Seg_2182" s="T64">zurückkommen-PTCP.PRS</ta>
            <ta e="T66" id="Seg_2183" s="T65">Weg-3SG-ACC</ta>
            <ta e="T67" id="Seg_2184" s="T66">zu</ta>
            <ta e="T68" id="Seg_2185" s="T67">stellen-CVB.SEQ</ta>
            <ta e="T69" id="Seg_2186" s="T68">nachdem</ta>
            <ta e="T70" id="Seg_2187" s="T69">kommen-PTCP.HAB</ta>
            <ta e="T71" id="Seg_2188" s="T70">Mensch.[NOM]</ta>
            <ta e="T72" id="Seg_2189" s="T71">Jurte.[NOM]</ta>
            <ta e="T73" id="Seg_2190" s="T72">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T74" id="Seg_2191" s="T73">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T75" id="Seg_2192" s="T74">hinausgehen-CVB.SEQ</ta>
            <ta e="T76" id="Seg_2193" s="T75">Rauchloch-DAT/LOC</ta>
            <ta e="T77" id="Seg_2194" s="T76">hineinschauen-CVB.SEQ</ta>
            <ta e="T78" id="Seg_2195" s="T77">sehen-PST2-3SG</ta>
            <ta e="T79" id="Seg_2196" s="T78">Feuer-3PL.[NOM]</ta>
            <ta e="T80" id="Seg_2197" s="T79">ausgehen-EP-PST2.[3SG]</ta>
            <ta e="T81" id="Seg_2198" s="T80">Kohle-3SG.[NOM]</ta>
            <ta e="T82" id="Seg_2199" s="T81">nur</ta>
            <ta e="T83" id="Seg_2200" s="T82">rot.werden-CVB.SIM</ta>
            <ta e="T84" id="Seg_2201" s="T83">liegen-PRS.[3SG]</ta>
            <ta e="T85" id="Seg_2202" s="T84">plötzlich</ta>
            <ta e="T86" id="Seg_2203" s="T85">hören-EP-PST2-3SG</ta>
            <ta e="T87" id="Seg_2204" s="T86">eins</ta>
            <ta e="T88" id="Seg_2205" s="T87">Mensch.[NOM]</ta>
            <ta e="T89" id="Seg_2206" s="T88">Märchen-VBZ-PRS.[3SG]</ta>
            <ta e="T90" id="Seg_2207" s="T89">offenbar</ta>
            <ta e="T91" id="Seg_2208" s="T90">weiß</ta>
            <ta e="T92" id="Seg_2209" s="T91">Zar.[NOM]</ta>
            <ta e="T93" id="Seg_2210" s="T92">Leute-3PL.[NOM]</ta>
            <ta e="T94" id="Seg_2211" s="T93">es.gibt-3PL</ta>
            <ta e="T95" id="Seg_2212" s="T94">man.sagt</ta>
            <ta e="T96" id="Seg_2213" s="T95">sagen-CVB.SEQ</ta>
            <ta e="T97" id="Seg_2214" s="T96">erzählen-PTCP.PRS</ta>
            <ta e="T98" id="Seg_2215" s="T97">sein-PST2.[3SG]</ta>
            <ta e="T99" id="Seg_2216" s="T98">dieses</ta>
            <ta e="T100" id="Seg_2217" s="T99">Märchen-AG.[NOM]</ta>
            <ta e="T101" id="Seg_2218" s="T100">jenes</ta>
            <ta e="T102" id="Seg_2219" s="T101">weiß</ta>
            <ta e="T103" id="Seg_2220" s="T102">Zar.[NOM]</ta>
            <ta e="T104" id="Seg_2221" s="T103">Leute-3PL.[NOM]</ta>
            <ta e="T105" id="Seg_2222" s="T104">eins</ta>
            <ta e="T106" id="Seg_2223" s="T105">Rentierkalb.[NOM]</ta>
            <ta e="T107" id="Seg_2224" s="T106">Fell-3SG-ABL</ta>
            <ta e="T108" id="Seg_2225" s="T107">sich.anziehen-PRS-3PL</ta>
            <ta e="T109" id="Seg_2226" s="T108">eins</ta>
            <ta e="T110" id="Seg_2227" s="T109">Rentierkalb.[NOM]</ta>
            <ta e="T111" id="Seg_2228" s="T110">Hinterbein-3SG-ACC</ta>
            <ta e="T112" id="Seg_2229" s="T111">zwei-MLTP</ta>
            <ta e="T113" id="Seg_2230" s="T112">Speise-VBZ-PRS-3PL</ta>
            <ta e="T114" id="Seg_2231" s="T113">man.sagt</ta>
            <ta e="T115" id="Seg_2232" s="T114">sagen-PRS.[3SG]</ta>
            <ta e="T116" id="Seg_2233" s="T115">dieses-DAT/LOC</ta>
            <ta e="T117" id="Seg_2234" s="T116">hören-PTCP.HAB-PL-3SG.[NOM]</ta>
            <ta e="T118" id="Seg_2235" s="T117">lachen-PRS-3PL</ta>
            <ta e="T119" id="Seg_2236" s="T118">doch</ta>
            <ta e="T120" id="Seg_2237" s="T119">merkwürdig</ta>
            <ta e="T121" id="Seg_2238" s="T120">EMPH</ta>
            <ta e="T122" id="Seg_2239" s="T121">Leute.[NOM]</ta>
            <ta e="T123" id="Seg_2240" s="T122">sein-PST2-3PL</ta>
            <ta e="T124" id="Seg_2241" s="T123">sagen-CVB.SEQ</ta>
            <ta e="T125" id="Seg_2242" s="T124">dieses-ACC</ta>
            <ta e="T126" id="Seg_2243" s="T125">hören-CVB.SEQ</ta>
            <ta e="T127" id="Seg_2244" s="T126">dieses</ta>
            <ta e="T128" id="Seg_2245" s="T127">Mensch.[NOM]</ta>
            <ta e="T129" id="Seg_2246" s="T128">sehr-ADVZ</ta>
            <ta e="T130" id="Seg_2247" s="T129">sehen-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T131" id="Seg_2248" s="T130">wollen-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_2249" s="T131">vor.langer.Zeit-ADJZ</ta>
            <ta e="T133" id="Seg_2250" s="T132">Mensch-DAT/LOC</ta>
            <ta e="T134" id="Seg_2251" s="T133">Feuerstein-3SG-ACC</ta>
            <ta e="T135" id="Seg_2252" s="T134">mit</ta>
            <ta e="T136" id="Seg_2253" s="T135">zusammen</ta>
            <ta e="T137" id="Seg_2254" s="T136">es.gibt</ta>
            <ta e="T138" id="Seg_2255" s="T137">sein-HAB.[3SG]</ta>
            <ta e="T139" id="Seg_2256" s="T138">faul</ta>
            <ta e="T140" id="Seg_2257" s="T139">Holz.[NOM]</ta>
            <ta e="T141" id="Seg_2258" s="T140">MOD</ta>
            <ta e="T142" id="Seg_2259" s="T141">trockene.Holzspäne.[NOM]</ta>
            <ta e="T143" id="Seg_2260" s="T142">Q</ta>
            <ta e="T144" id="Seg_2261" s="T143">dieses-INSTR</ta>
            <ta e="T145" id="Seg_2262" s="T144">Funke-ACC</ta>
            <ta e="T146" id="Seg_2263" s="T145">entzünden-CVB.SEQ</ta>
            <ta e="T147" id="Seg_2264" s="T146">anzünden-PRS-3PL</ta>
            <ta e="T148" id="Seg_2265" s="T147">dieses</ta>
            <ta e="T149" id="Seg_2266" s="T148">Mensch-DAT/LOC</ta>
            <ta e="T150" id="Seg_2267" s="T149">trockene.Holzspäne.[NOM]</ta>
            <ta e="T151" id="Seg_2268" s="T150">es.gibt</ta>
            <ta e="T152" id="Seg_2269" s="T151">sein-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_2270" s="T152">jenes-3SG-ACC</ta>
            <ta e="T154" id="Seg_2271" s="T153">anzünden-CVB.SEQ</ta>
            <ta e="T155" id="Seg_2272" s="T154">Rauchloch-3SG-INSTR</ta>
            <ta e="T156" id="Seg_2273" s="T155">werfen-PST2.[3SG]</ta>
            <ta e="T157" id="Seg_2274" s="T156">trockene.Holzspäne-3SG.[NOM]</ta>
            <ta e="T158" id="Seg_2275" s="T157">Kohle-DAT/LOC</ta>
            <ta e="T159" id="Seg_2276" s="T158">fallen-CVB.SEQ</ta>
            <ta e="T160" id="Seg_2277" s="T159">unerwartet</ta>
            <ta e="T161" id="Seg_2278" s="T160">Aufflammen.[NOM]</ta>
            <ta e="T162" id="Seg_2279" s="T161">machen-PST2.[3SG]</ta>
            <ta e="T163" id="Seg_2280" s="T162">dort</ta>
            <ta e="T164" id="Seg_2281" s="T163">sehen-PST2-3SG</ta>
            <ta e="T165" id="Seg_2282" s="T164">Feuer.[NOM]</ta>
            <ta e="T166" id="Seg_2283" s="T165">Vorderteil-3SG-DAT/LOC</ta>
            <ta e="T167" id="Seg_2284" s="T166">eins</ta>
            <ta e="T168" id="Seg_2285" s="T167">Mensch.[NOM]</ta>
            <ta e="T169" id="Seg_2286" s="T168">Märchen-VBZ-CVB.SIM</ta>
            <ta e="T170" id="Seg_2287" s="T169">sitzen-PTCP.PRS</ta>
            <ta e="T171" id="Seg_2288" s="T170">sein-PST2.[3SG]</ta>
            <ta e="T172" id="Seg_2289" s="T171">dieses</ta>
            <ta e="T173" id="Seg_2290" s="T172">Mensch.[NOM]</ta>
            <ta e="T174" id="Seg_2291" s="T173">nackt.[NOM]</ta>
            <ta e="T175" id="Seg_2292" s="T174">sein-CVB.SEQ</ta>
            <ta e="T176" id="Seg_2293" s="T175">nachdem</ta>
            <ta e="T177" id="Seg_2294" s="T176">ganz</ta>
            <ta e="T178" id="Seg_2295" s="T177">Tierhaar.[NOM]</ta>
            <ta e="T179" id="Seg_2296" s="T178">sein-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_2297" s="T179">dieses-ACC</ta>
            <ta e="T181" id="Seg_2298" s="T180">sehen-PRS.[3SG]</ta>
            <ta e="T182" id="Seg_2299" s="T181">doch</ta>
            <ta e="T183" id="Seg_2300" s="T182">dieses</ta>
            <ta e="T184" id="Seg_2301" s="T183">Mensch.[NOM]</ta>
            <ta e="T185" id="Seg_2302" s="T184">springen-CVB.SIM</ta>
            <ta e="T186" id="Seg_2303" s="T185">brechen-PRS.[3SG]</ta>
            <ta e="T187" id="Seg_2304" s="T186">Hund-3PL-GEN</ta>
            <ta e="T188" id="Seg_2305" s="T187">Schlitten-3SG-DAT/LOC</ta>
            <ta e="T189" id="Seg_2306" s="T188">sich.setzen-PRS.[3SG]</ta>
            <ta e="T190" id="Seg_2307" s="T189">und</ta>
            <ta e="T191" id="Seg_2308" s="T190">gehen-PTCP.PRS</ta>
            <ta e="T192" id="Seg_2309" s="T191">in.Richtung</ta>
            <ta e="T193" id="Seg_2310" s="T192">sein-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_2311" s="T193">Hinterteil-3SG-ACC</ta>
            <ta e="T195" id="Seg_2312" s="T194">zu</ta>
            <ta e="T196" id="Seg_2313" s="T195">verfolgen-EP-RECP/COLL-PST2-3SG</ta>
            <ta e="T197" id="Seg_2314" s="T196">eins</ta>
            <ta e="T198" id="Seg_2315" s="T197">Tierhaar-PROPR</ta>
            <ta e="T199" id="Seg_2316" s="T198">Mensch.[NOM]</ta>
            <ta e="T200" id="Seg_2317" s="T199">verfolgen-CVB.SEQ</ta>
            <ta e="T201" id="Seg_2318" s="T200">gehen-PTCP.PRS</ta>
            <ta e="T202" id="Seg_2319" s="T201">sein-PST2.[3SG]</ta>
            <ta e="T203" id="Seg_2320" s="T202">verfolgen-CVB.SEQ</ta>
            <ta e="T204" id="Seg_2321" s="T203">Schlitten-3SG-GEN</ta>
            <ta e="T205" id="Seg_2322" s="T204">Hinterteil-ADJZ</ta>
            <ta e="T206" id="Seg_2323" s="T205">Bein-3SG-ABL</ta>
            <ta e="T207" id="Seg_2324" s="T206">nehmen-PST2-3SG</ta>
            <ta e="T208" id="Seg_2325" s="T207">jenes</ta>
            <ta e="T209" id="Seg_2326" s="T208">Hinterteil-ADJZ</ta>
            <ta e="T210" id="Seg_2327" s="T209">Bein-3SG.[NOM]</ta>
            <ta e="T211" id="Seg_2328" s="T210">dieses</ta>
            <ta e="T212" id="Seg_2329" s="T211">Tierhaar-PROPR</ta>
            <ta e="T213" id="Seg_2330" s="T212">Mensch.[NOM]</ta>
            <ta e="T214" id="Seg_2331" s="T213">Hand-3SG-DAT/LOC</ta>
            <ta e="T215" id="Seg_2332" s="T214">bleiben-PST2.[3SG]</ta>
            <ta e="T216" id="Seg_2333" s="T215">jenes</ta>
            <ta e="T217" id="Seg_2334" s="T216">nehmen-PTCP.HAB</ta>
            <ta e="T218" id="Seg_2335" s="T217">Mensch.[NOM]</ta>
            <ta e="T219" id="Seg_2336" s="T218">schreien-PST2.[3SG]</ta>
            <ta e="T220" id="Seg_2337" s="T219">weiß</ta>
            <ta e="T221" id="Seg_2338" s="T220">Zar.[NOM]</ta>
            <ta e="T222" id="Seg_2339" s="T221">Leute-PL-3SG.[NOM]</ta>
            <ta e="T223" id="Seg_2340" s="T222">zurückkommen-PTCP.PRS</ta>
            <ta e="T224" id="Seg_2341" s="T223">sein-APPR-2PL</ta>
            <ta e="T225" id="Seg_2342" s="T224">1PL.[NOM]</ta>
            <ta e="T226" id="Seg_2343" s="T225">selbst-1PL-GEN</ta>
            <ta e="T227" id="Seg_2344" s="T226">Nachricht.[NOM]</ta>
            <ta e="T228" id="Seg_2345" s="T227">schicken-PTCP.PRS-ACC</ta>
            <ta e="T229" id="Seg_2346" s="T228">einverstanden.sein-NEG.PTCP</ta>
            <ta e="T230" id="Seg_2347" s="T229">sein-PRS-1PL</ta>
            <ta e="T231" id="Seg_2348" s="T230">jetzt</ta>
            <ta e="T232" id="Seg_2349" s="T231">wer.[NOM]</ta>
            <ta e="T233" id="Seg_2350" s="T232">INDEF</ta>
            <ta e="T234" id="Seg_2351" s="T233">1PL.[NOM]</ta>
            <ta e="T235" id="Seg_2352" s="T234">zu</ta>
            <ta e="T236" id="Seg_2353" s="T235">kommen-FUT.[3SG]</ta>
            <ta e="T237" id="Seg_2354" s="T236">und</ta>
            <ta e="T238" id="Seg_2355" s="T237">1PL.[NOM]</ta>
            <ta e="T239" id="Seg_2356" s="T238">zurückkommen-EP-CAUS-FUT-1PL</ta>
            <ta e="T240" id="Seg_2357" s="T239">NEG-3SG</ta>
            <ta e="T241" id="Seg_2358" s="T240">sagen-PST2.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2359" s="T0">давно</ta>
            <ta e="T2" id="Seg_2360" s="T1">один</ta>
            <ta e="T3" id="Seg_2361" s="T2">собака-PROPR</ta>
            <ta e="T4" id="Seg_2362" s="T3">человек.[NOM]</ta>
            <ta e="T5" id="Seg_2363" s="T4">тундра-DAT/LOC</ta>
            <ta e="T6" id="Seg_2364" s="T5">идти-PST2.[3SG]</ta>
            <ta e="T7" id="Seg_2365" s="T6">этот</ta>
            <ta e="T8" id="Seg_2366" s="T7">идти-CVB.SEQ</ta>
            <ta e="T9" id="Seg_2367" s="T8">ехать-CVB.SEQ</ta>
            <ta e="T10" id="Seg_2368" s="T9">большой</ta>
            <ta e="T11" id="Seg_2369" s="T10">очень</ta>
            <ta e="T12" id="Seg_2370" s="T11">человек.[NOM]</ta>
            <ta e="T13" id="Seg_2371" s="T12">след-3SG-ACC</ta>
            <ta e="T14" id="Seg_2372" s="T13">снег-DAT/LOC</ta>
            <ta e="T15" id="Seg_2373" s="T14">видеть-PST2.[3SG]</ta>
            <ta e="T16" id="Seg_2374" s="T15">след-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_2375" s="T16">величина-3SG.[NOM]</ta>
            <ta e="T18" id="Seg_2376" s="T17">щит.для.укрытия.при.охоте.[NOM]</ta>
            <ta e="T19" id="Seg_2377" s="T18">след-3SG-ACC</ta>
            <ta e="T20" id="Seg_2378" s="T19">как</ta>
            <ta e="T21" id="Seg_2379" s="T20">быть-PST2.[3SG]</ta>
            <ta e="T22" id="Seg_2380" s="T21">этот</ta>
            <ta e="T23" id="Seg_2381" s="T22">видеть-PTCP.HAB</ta>
            <ta e="T24" id="Seg_2382" s="T23">человек.[NOM]</ta>
            <ta e="T25" id="Seg_2383" s="T24">молодой.[NOM]</ta>
            <ta e="T26" id="Seg_2384" s="T25">быть-PST2.[3SG]</ta>
            <ta e="T27" id="Seg_2385" s="T26">что-ACC</ta>
            <ta e="T28" id="Seg_2386" s="T27">NEG</ta>
            <ta e="T29" id="Seg_2387" s="T28">EMPH-столько</ta>
            <ta e="T30" id="Seg_2388" s="T29">бояться-NEG.[3SG]</ta>
            <ta e="T31" id="Seg_2389" s="T30">тот.EMPH.[NOM]</ta>
            <ta e="T32" id="Seg_2390" s="T31">вопреки</ta>
            <ta e="T33" id="Seg_2391" s="T32">3SG.[NOM]</ta>
            <ta e="T34" id="Seg_2392" s="T33">побаиваться-PST2.[3SG]</ta>
            <ta e="T35" id="Seg_2393" s="T34">днем</ta>
            <ta e="T36" id="Seg_2394" s="T35">идти-PTCP.FUT-3SG-ACC</ta>
            <ta e="T37" id="Seg_2395" s="T36">вечер.[NOM]</ta>
            <ta e="T38" id="Seg_2396" s="T37">становиться-PTCP.PRS-3SG-ACC</ta>
            <ta e="T39" id="Seg_2397" s="T38">ожидать-PST2.[3SG]</ta>
            <ta e="T40" id="Seg_2398" s="T39">сидеть-CAUS-CVB.SEQ</ta>
            <ta e="T41" id="Seg_2399" s="T40">этот</ta>
            <ta e="T42" id="Seg_2400" s="T41">вечер.[NOM]</ta>
            <ta e="T43" id="Seg_2401" s="T42">быть-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T44" id="Seg_2402" s="T43">собака-3SG-DAT/LOC</ta>
            <ta e="T45" id="Seg_2403" s="T44">сесть-CVB.SEQ</ta>
            <ta e="T46" id="Seg_2404" s="T45">след-ACC</ta>
            <ta e="T47" id="Seg_2405" s="T46">следовать-CVB.SEQ</ta>
            <ta e="T48" id="Seg_2406" s="T47">идти-PRS.[3SG]</ta>
            <ta e="T49" id="Seg_2407" s="T48">этот</ta>
            <ta e="T50" id="Seg_2408" s="T49">большой</ta>
            <ta e="T51" id="Seg_2409" s="T50">люди.[NOM]</ta>
            <ta e="T52" id="Seg_2410" s="T51">след-3PL.[NOM]</ta>
            <ta e="T53" id="Seg_2411" s="T52">умножаться-CVB.SEQ</ta>
            <ta e="T54" id="Seg_2412" s="T53">идти-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_2413" s="T54">большой</ta>
            <ta e="T56" id="Seg_2414" s="T55">очень</ta>
            <ta e="T57" id="Seg_2415" s="T56">юрта-DAT/LOC</ta>
            <ta e="T58" id="Seg_2416" s="T57">доезжать-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_2417" s="T58">большой</ta>
            <ta e="T60" id="Seg_2418" s="T59">очень</ta>
            <ta e="T61" id="Seg_2419" s="T60">нарта-PL.[NOM]</ta>
            <ta e="T62" id="Seg_2420" s="T61">стоять-PTCP.PRS-3PL-ACC</ta>
            <ta e="T63" id="Seg_2421" s="T62">видеть-PST2.[3SG]</ta>
            <ta e="T64" id="Seg_2422" s="T63">собака-PL-3SG-ACC</ta>
            <ta e="T65" id="Seg_2423" s="T64">возвращаться-PTCP.PRS</ta>
            <ta e="T66" id="Seg_2424" s="T65">дорога-3SG-ACC</ta>
            <ta e="T67" id="Seg_2425" s="T66">к</ta>
            <ta e="T68" id="Seg_2426" s="T67">ставить-CVB.SEQ</ta>
            <ta e="T69" id="Seg_2427" s="T68">после</ta>
            <ta e="T70" id="Seg_2428" s="T69">приходить-PTCP.HAB</ta>
            <ta e="T71" id="Seg_2429" s="T70">человек.[NOM]</ta>
            <ta e="T72" id="Seg_2430" s="T71">юрта.[NOM]</ta>
            <ta e="T73" id="Seg_2431" s="T72">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T74" id="Seg_2432" s="T73">выйти-EP-PST2.[3SG]</ta>
            <ta e="T75" id="Seg_2433" s="T74">выйти-CVB.SEQ</ta>
            <ta e="T76" id="Seg_2434" s="T75">дымоход-DAT/LOC</ta>
            <ta e="T77" id="Seg_2435" s="T76">загладывать-CVB.SEQ</ta>
            <ta e="T78" id="Seg_2436" s="T77">видеть-PST2-3SG</ta>
            <ta e="T79" id="Seg_2437" s="T78">огонь-3PL.[NOM]</ta>
            <ta e="T80" id="Seg_2438" s="T79">гаснуть-EP-PST2.[3SG]</ta>
            <ta e="T81" id="Seg_2439" s="T80">уголь-3SG.[NOM]</ta>
            <ta e="T82" id="Seg_2440" s="T81">только</ta>
            <ta e="T83" id="Seg_2441" s="T82">краснеть-CVB.SIM</ta>
            <ta e="T84" id="Seg_2442" s="T83">лежать-PRS.[3SG]</ta>
            <ta e="T85" id="Seg_2443" s="T84">вдруг</ta>
            <ta e="T86" id="Seg_2444" s="T85">слышать-EP-PST2-3SG</ta>
            <ta e="T87" id="Seg_2445" s="T86">один</ta>
            <ta e="T88" id="Seg_2446" s="T87">человек.[NOM]</ta>
            <ta e="T89" id="Seg_2447" s="T88">сказка-VBZ-PRS.[3SG]</ta>
            <ta e="T90" id="Seg_2448" s="T89">наверное</ta>
            <ta e="T91" id="Seg_2449" s="T90">белый</ta>
            <ta e="T92" id="Seg_2450" s="T91">царь.[NOM]</ta>
            <ta e="T93" id="Seg_2451" s="T92">люди-3PL.[NOM]</ta>
            <ta e="T94" id="Seg_2452" s="T93">есть-3PL</ta>
            <ta e="T95" id="Seg_2453" s="T94">говорят</ta>
            <ta e="T96" id="Seg_2454" s="T95">говорить-CVB.SEQ</ta>
            <ta e="T97" id="Seg_2455" s="T96">рассказывать-PTCP.PRS</ta>
            <ta e="T98" id="Seg_2456" s="T97">быть-PST2.[3SG]</ta>
            <ta e="T99" id="Seg_2457" s="T98">этот</ta>
            <ta e="T100" id="Seg_2458" s="T99">сказка-AG.[NOM]</ta>
            <ta e="T101" id="Seg_2459" s="T100">тот</ta>
            <ta e="T102" id="Seg_2460" s="T101">белый</ta>
            <ta e="T103" id="Seg_2461" s="T102">царь.[NOM]</ta>
            <ta e="T104" id="Seg_2462" s="T103">люди-3PL.[NOM]</ta>
            <ta e="T105" id="Seg_2463" s="T104">один</ta>
            <ta e="T106" id="Seg_2464" s="T105">олененок.[NOM]</ta>
            <ta e="T107" id="Seg_2465" s="T106">шкура-3SG-ABL</ta>
            <ta e="T108" id="Seg_2466" s="T107">одеваться-PRS-3PL</ta>
            <ta e="T109" id="Seg_2467" s="T108">один</ta>
            <ta e="T110" id="Seg_2468" s="T109">олененок.[NOM]</ta>
            <ta e="T111" id="Seg_2469" s="T110">задняя.нога-3SG-ACC</ta>
            <ta e="T112" id="Seg_2470" s="T111">два-MLTP</ta>
            <ta e="T113" id="Seg_2471" s="T112">пища-VBZ-PRS-3PL</ta>
            <ta e="T114" id="Seg_2472" s="T113">говорят</ta>
            <ta e="T115" id="Seg_2473" s="T114">говорить-PRS.[3SG]</ta>
            <ta e="T116" id="Seg_2474" s="T115">этот-DAT/LOC</ta>
            <ta e="T117" id="Seg_2475" s="T116">слышать-PTCP.HAB-PL-3SG.[NOM]</ta>
            <ta e="T118" id="Seg_2476" s="T117">смеяться-PRS-3PL</ta>
            <ta e="T119" id="Seg_2477" s="T118">вот</ta>
            <ta e="T120" id="Seg_2478" s="T119">удивительный</ta>
            <ta e="T121" id="Seg_2479" s="T120">EMPH</ta>
            <ta e="T122" id="Seg_2480" s="T121">люди.[NOM]</ta>
            <ta e="T123" id="Seg_2481" s="T122">быть-PST2-3PL</ta>
            <ta e="T124" id="Seg_2482" s="T123">говорить-CVB.SEQ</ta>
            <ta e="T125" id="Seg_2483" s="T124">этот-ACC</ta>
            <ta e="T126" id="Seg_2484" s="T125">слышать-CVB.SEQ</ta>
            <ta e="T127" id="Seg_2485" s="T126">этот</ta>
            <ta e="T128" id="Seg_2486" s="T127">человек.[NOM]</ta>
            <ta e="T129" id="Seg_2487" s="T128">очень-ADVZ</ta>
            <ta e="T130" id="Seg_2488" s="T129">видеть-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T131" id="Seg_2489" s="T130">хотеть-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_2490" s="T131">давно-ADJZ</ta>
            <ta e="T133" id="Seg_2491" s="T132">человек-DAT/LOC</ta>
            <ta e="T134" id="Seg_2492" s="T133">огниво-3SG-ACC</ta>
            <ta e="T135" id="Seg_2493" s="T134">с</ta>
            <ta e="T136" id="Seg_2494" s="T135">вместе</ta>
            <ta e="T137" id="Seg_2495" s="T136">есть</ta>
            <ta e="T138" id="Seg_2496" s="T137">быть-HAB.[3SG]</ta>
            <ta e="T139" id="Seg_2497" s="T138">гнилой</ta>
            <ta e="T140" id="Seg_2498" s="T139">дерево.[NOM]</ta>
            <ta e="T141" id="Seg_2499" s="T140">MOD</ta>
            <ta e="T142" id="Seg_2500" s="T141">сухие.стружки.[NOM]</ta>
            <ta e="T143" id="Seg_2501" s="T142">Q</ta>
            <ta e="T144" id="Seg_2502" s="T143">этот-INSTR</ta>
            <ta e="T145" id="Seg_2503" s="T144">искра-ACC</ta>
            <ta e="T146" id="Seg_2504" s="T145">разжигать-CVB.SEQ</ta>
            <ta e="T147" id="Seg_2505" s="T146">зажигать-PRS-3PL</ta>
            <ta e="T148" id="Seg_2506" s="T147">этот</ta>
            <ta e="T149" id="Seg_2507" s="T148">человек-DAT/LOC</ta>
            <ta e="T150" id="Seg_2508" s="T149">сухие.стружки.[NOM]</ta>
            <ta e="T151" id="Seg_2509" s="T150">есть</ta>
            <ta e="T152" id="Seg_2510" s="T151">быть-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_2511" s="T152">тот-3SG-ACC</ta>
            <ta e="T154" id="Seg_2512" s="T153">зажигать-CVB.SEQ</ta>
            <ta e="T155" id="Seg_2513" s="T154">дымоход-3SG-INSTR</ta>
            <ta e="T156" id="Seg_2514" s="T155">бросать-PST2.[3SG]</ta>
            <ta e="T157" id="Seg_2515" s="T156">сухие.стружки-3SG.[NOM]</ta>
            <ta e="T158" id="Seg_2516" s="T157">уголь-DAT/LOC</ta>
            <ta e="T159" id="Seg_2517" s="T158">падать-CVB.SEQ</ta>
            <ta e="T160" id="Seg_2518" s="T159">неожиданно</ta>
            <ta e="T161" id="Seg_2519" s="T160">вспыхнвание.[NOM]</ta>
            <ta e="T162" id="Seg_2520" s="T161">делать-PST2.[3SG]</ta>
            <ta e="T163" id="Seg_2521" s="T162">там</ta>
            <ta e="T164" id="Seg_2522" s="T163">видеть-PST2-3SG</ta>
            <ta e="T165" id="Seg_2523" s="T164">огонь.[NOM]</ta>
            <ta e="T166" id="Seg_2524" s="T165">передняя.часть-3SG-DAT/LOC</ta>
            <ta e="T167" id="Seg_2525" s="T166">один</ta>
            <ta e="T168" id="Seg_2526" s="T167">человек.[NOM]</ta>
            <ta e="T169" id="Seg_2527" s="T168">сказка-VBZ-CVB.SIM</ta>
            <ta e="T170" id="Seg_2528" s="T169">сидеть-PTCP.PRS</ta>
            <ta e="T171" id="Seg_2529" s="T170">быть-PST2.[3SG]</ta>
            <ta e="T172" id="Seg_2530" s="T171">этот</ta>
            <ta e="T173" id="Seg_2531" s="T172">человек.[NOM]</ta>
            <ta e="T174" id="Seg_2532" s="T173">голый.[NOM]</ta>
            <ta e="T175" id="Seg_2533" s="T174">быть-CVB.SEQ</ta>
            <ta e="T176" id="Seg_2534" s="T175">после</ta>
            <ta e="T177" id="Seg_2535" s="T176">полностью</ta>
            <ta e="T178" id="Seg_2536" s="T177">шерсть.[NOM]</ta>
            <ta e="T179" id="Seg_2537" s="T178">быть-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_2538" s="T179">этот-ACC</ta>
            <ta e="T181" id="Seg_2539" s="T180">видеть-PRS.[3SG]</ta>
            <ta e="T182" id="Seg_2540" s="T181">вот</ta>
            <ta e="T183" id="Seg_2541" s="T182">этот</ta>
            <ta e="T184" id="Seg_2542" s="T183">человек.[NOM]</ta>
            <ta e="T185" id="Seg_2543" s="T184">прыгать-CVB.SIM</ta>
            <ta e="T186" id="Seg_2544" s="T185">ломаться-PRS.[3SG]</ta>
            <ta e="T187" id="Seg_2545" s="T186">собака-3PL-GEN</ta>
            <ta e="T188" id="Seg_2546" s="T187">сани-3SG-DAT/LOC</ta>
            <ta e="T189" id="Seg_2547" s="T188">сесть-PRS.[3SG]</ta>
            <ta e="T190" id="Seg_2548" s="T189">да</ta>
            <ta e="T191" id="Seg_2549" s="T190">идти-PTCP.PRS</ta>
            <ta e="T192" id="Seg_2550" s="T191">в.сторону</ta>
            <ta e="T193" id="Seg_2551" s="T192">быть-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_2552" s="T193">задняя.часть-3SG-ACC</ta>
            <ta e="T195" id="Seg_2553" s="T194">к</ta>
            <ta e="T196" id="Seg_2554" s="T195">идти.по.следам-EP-RECP/COLL-PST2-3SG</ta>
            <ta e="T197" id="Seg_2555" s="T196">один</ta>
            <ta e="T198" id="Seg_2556" s="T197">шерсть-PROPR</ta>
            <ta e="T199" id="Seg_2557" s="T198">человек.[NOM]</ta>
            <ta e="T200" id="Seg_2558" s="T199">догонять-CVB.SEQ</ta>
            <ta e="T201" id="Seg_2559" s="T200">идти-PTCP.PRS</ta>
            <ta e="T202" id="Seg_2560" s="T201">быть-PST2.[3SG]</ta>
            <ta e="T203" id="Seg_2561" s="T202">догонять-CVB.SEQ</ta>
            <ta e="T204" id="Seg_2562" s="T203">сани-3SG-GEN</ta>
            <ta e="T205" id="Seg_2563" s="T204">задняя.часть-ADJZ</ta>
            <ta e="T206" id="Seg_2564" s="T205">нога-3SG-ABL</ta>
            <ta e="T207" id="Seg_2565" s="T206">взять-PST2-3SG</ta>
            <ta e="T208" id="Seg_2566" s="T207">тот</ta>
            <ta e="T209" id="Seg_2567" s="T208">задняя.часть-ADJZ</ta>
            <ta e="T210" id="Seg_2568" s="T209">нога-3SG.[NOM]</ta>
            <ta e="T211" id="Seg_2569" s="T210">этот</ta>
            <ta e="T212" id="Seg_2570" s="T211">шерсть-PROPR</ta>
            <ta e="T213" id="Seg_2571" s="T212">человек.[NOM]</ta>
            <ta e="T214" id="Seg_2572" s="T213">рука-3SG-DAT/LOC</ta>
            <ta e="T215" id="Seg_2573" s="T214">оставаться-PST2.[3SG]</ta>
            <ta e="T216" id="Seg_2574" s="T215">тот</ta>
            <ta e="T217" id="Seg_2575" s="T216">взять-PTCP.HAB</ta>
            <ta e="T218" id="Seg_2576" s="T217">человек.[NOM]</ta>
            <ta e="T219" id="Seg_2577" s="T218">кричать-PST2.[3SG]</ta>
            <ta e="T220" id="Seg_2578" s="T219">белый</ta>
            <ta e="T221" id="Seg_2579" s="T220">царь.[NOM]</ta>
            <ta e="T222" id="Seg_2580" s="T221">люди-PL-3SG.[NOM]</ta>
            <ta e="T223" id="Seg_2581" s="T222">возвращаться-PTCP.PRS</ta>
            <ta e="T224" id="Seg_2582" s="T223">быть-APPR-2PL</ta>
            <ta e="T225" id="Seg_2583" s="T224">1PL.[NOM]</ta>
            <ta e="T226" id="Seg_2584" s="T225">сам-1PL-GEN</ta>
            <ta e="T227" id="Seg_2585" s="T226">весть.[NOM]</ta>
            <ta e="T228" id="Seg_2586" s="T227">послать-PTCP.PRS-ACC</ta>
            <ta e="T229" id="Seg_2587" s="T228">согласить-NEG.PTCP</ta>
            <ta e="T230" id="Seg_2588" s="T229">быть-PRS-1PL</ta>
            <ta e="T231" id="Seg_2589" s="T230">теперь</ta>
            <ta e="T232" id="Seg_2590" s="T231">кто.[NOM]</ta>
            <ta e="T233" id="Seg_2591" s="T232">INDEF</ta>
            <ta e="T234" id="Seg_2592" s="T233">1PL.[NOM]</ta>
            <ta e="T235" id="Seg_2593" s="T234">к</ta>
            <ta e="T236" id="Seg_2594" s="T235">приходить-FUT.[3SG]</ta>
            <ta e="T237" id="Seg_2595" s="T236">да</ta>
            <ta e="T238" id="Seg_2596" s="T237">1PL.[NOM]</ta>
            <ta e="T239" id="Seg_2597" s="T238">возвращаться-EP-CAUS-FUT-1PL</ta>
            <ta e="T240" id="Seg_2598" s="T239">NEG-3SG</ta>
            <ta e="T241" id="Seg_2599" s="T240">говорить-PST2.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2600" s="T0">adv</ta>
            <ta e="T2" id="Seg_2601" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_2602" s="T2">n-n&gt;adj</ta>
            <ta e="T4" id="Seg_2603" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_2604" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_2605" s="T5">v-v:tense-v:pred.pn</ta>
            <ta e="T7" id="Seg_2606" s="T6">dempro</ta>
            <ta e="T8" id="Seg_2607" s="T7">v-v:cvb</ta>
            <ta e="T9" id="Seg_2608" s="T8">v-v:cvb</ta>
            <ta e="T10" id="Seg_2609" s="T9">adj</ta>
            <ta e="T11" id="Seg_2610" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_2611" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_2612" s="T12">n-n:poss-n:case</ta>
            <ta e="T14" id="Seg_2613" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_2614" s="T14">v-v:tense-v:pred.pn</ta>
            <ta e="T16" id="Seg_2615" s="T15">n-n:(poss)-n:case</ta>
            <ta e="T17" id="Seg_2616" s="T16">n-n:(poss)-n:case</ta>
            <ta e="T18" id="Seg_2617" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_2618" s="T18">n-n:poss-n:case</ta>
            <ta e="T20" id="Seg_2619" s="T19">post</ta>
            <ta e="T21" id="Seg_2620" s="T20">v-v:tense-v:pred.pn</ta>
            <ta e="T22" id="Seg_2621" s="T21">dempro</ta>
            <ta e="T23" id="Seg_2622" s="T22">v-v:ptcp</ta>
            <ta e="T24" id="Seg_2623" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_2624" s="T24">adj-n:case</ta>
            <ta e="T26" id="Seg_2625" s="T25">v-v:tense-v:pred.pn</ta>
            <ta e="T27" id="Seg_2626" s="T26">que-pro:case</ta>
            <ta e="T28" id="Seg_2627" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_2628" s="T28">adv&gt;adv-adv</ta>
            <ta e="T30" id="Seg_2629" s="T29">v-v:(neg)-v:pred.pn</ta>
            <ta e="T31" id="Seg_2630" s="T30">dempro-pro:case</ta>
            <ta e="T32" id="Seg_2631" s="T31">post</ta>
            <ta e="T33" id="Seg_2632" s="T32">pers-pro:case</ta>
            <ta e="T34" id="Seg_2633" s="T33">v-v:tense-v:pred.pn</ta>
            <ta e="T35" id="Seg_2634" s="T34">adv</ta>
            <ta e="T36" id="Seg_2635" s="T35">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T37" id="Seg_2636" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_2637" s="T37">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T39" id="Seg_2638" s="T38">v-v:tense-v:pred.pn</ta>
            <ta e="T40" id="Seg_2639" s="T39">v-v&gt;v-v:cvb</ta>
            <ta e="T41" id="Seg_2640" s="T40">dempro</ta>
            <ta e="T42" id="Seg_2641" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_2642" s="T42">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T44" id="Seg_2643" s="T43">n-n:poss-n:case</ta>
            <ta e="T45" id="Seg_2644" s="T44">v-v:cvb</ta>
            <ta e="T46" id="Seg_2645" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_2646" s="T46">v-v:cvb</ta>
            <ta e="T48" id="Seg_2647" s="T47">v-v:tense-v:pred.pn</ta>
            <ta e="T49" id="Seg_2648" s="T48">dempro</ta>
            <ta e="T50" id="Seg_2649" s="T49">adj</ta>
            <ta e="T51" id="Seg_2650" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_2651" s="T51">n-n:(poss)-n:case</ta>
            <ta e="T53" id="Seg_2652" s="T52">v-v:cvb</ta>
            <ta e="T54" id="Seg_2653" s="T53">v-v:tense-v:pred.pn</ta>
            <ta e="T55" id="Seg_2654" s="T54">adj</ta>
            <ta e="T56" id="Seg_2655" s="T55">ptcl</ta>
            <ta e="T57" id="Seg_2656" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_2657" s="T57">v-v:tense-v:pred.pn</ta>
            <ta e="T59" id="Seg_2658" s="T58">adj</ta>
            <ta e="T60" id="Seg_2659" s="T59">post</ta>
            <ta e="T61" id="Seg_2660" s="T60">n-n:(num)-n:case</ta>
            <ta e="T62" id="Seg_2661" s="T61">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T63" id="Seg_2662" s="T62">v-v:tense-v:pred.pn</ta>
            <ta e="T64" id="Seg_2663" s="T63">n-n:(num)-n:poss-n:case</ta>
            <ta e="T65" id="Seg_2664" s="T64">v-v:ptcp</ta>
            <ta e="T66" id="Seg_2665" s="T65">n-n:poss-n:case</ta>
            <ta e="T67" id="Seg_2666" s="T66">post</ta>
            <ta e="T68" id="Seg_2667" s="T67">v-v:cvb</ta>
            <ta e="T69" id="Seg_2668" s="T68">post</ta>
            <ta e="T70" id="Seg_2669" s="T69">v-v:ptcp</ta>
            <ta e="T71" id="Seg_2670" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_2671" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_2672" s="T72">n-n:poss-n:case</ta>
            <ta e="T74" id="Seg_2673" s="T73">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T75" id="Seg_2674" s="T74">v-v:cvb</ta>
            <ta e="T76" id="Seg_2675" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_2676" s="T76">v-v:cvb</ta>
            <ta e="T78" id="Seg_2677" s="T77">v-v:tense-v:poss.pn</ta>
            <ta e="T79" id="Seg_2678" s="T78">n-n:(poss)-n:case</ta>
            <ta e="T80" id="Seg_2679" s="T79">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T81" id="Seg_2680" s="T80">n-n:(poss)-n:case</ta>
            <ta e="T82" id="Seg_2681" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_2682" s="T82">v-v:cvb</ta>
            <ta e="T84" id="Seg_2683" s="T83">v-v:tense-v:pred.pn</ta>
            <ta e="T85" id="Seg_2684" s="T84">adv</ta>
            <ta e="T86" id="Seg_2685" s="T85">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T87" id="Seg_2686" s="T86">cardnum</ta>
            <ta e="T88" id="Seg_2687" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_2688" s="T88">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T90" id="Seg_2689" s="T89">adv</ta>
            <ta e="T91" id="Seg_2690" s="T90">adj</ta>
            <ta e="T92" id="Seg_2691" s="T91">n-n:case</ta>
            <ta e="T93" id="Seg_2692" s="T92">n-n:(poss)-n:case</ta>
            <ta e="T94" id="Seg_2693" s="T93">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T95" id="Seg_2694" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_2695" s="T95">v-v:cvb</ta>
            <ta e="T97" id="Seg_2696" s="T96">v-v:ptcp</ta>
            <ta e="T98" id="Seg_2697" s="T97">v-v:tense-v:pred.pn</ta>
            <ta e="T99" id="Seg_2698" s="T98">dempro</ta>
            <ta e="T100" id="Seg_2699" s="T99">n-n&gt;n-n:case</ta>
            <ta e="T101" id="Seg_2700" s="T100">dempro</ta>
            <ta e="T102" id="Seg_2701" s="T101">adj</ta>
            <ta e="T103" id="Seg_2702" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_2703" s="T103">n-n:(poss)-n:case</ta>
            <ta e="T105" id="Seg_2704" s="T104">cardnum</ta>
            <ta e="T106" id="Seg_2705" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_2706" s="T106">n-n:poss-n:case</ta>
            <ta e="T108" id="Seg_2707" s="T107">v-v:tense-v:pred.pn</ta>
            <ta e="T109" id="Seg_2708" s="T108">cardnum</ta>
            <ta e="T110" id="Seg_2709" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_2710" s="T110">n-n:poss-n:case</ta>
            <ta e="T112" id="Seg_2711" s="T111">cardnum-cardnum&gt;adv</ta>
            <ta e="T113" id="Seg_2712" s="T112">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T114" id="Seg_2713" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_2714" s="T114">v-v:tense-v:pred.pn</ta>
            <ta e="T116" id="Seg_2715" s="T115">dempro-pro:case</ta>
            <ta e="T117" id="Seg_2716" s="T116">v-v:ptcp-v:(num)-v:(poss)-v:(case)</ta>
            <ta e="T118" id="Seg_2717" s="T117">v-v:tense-v:pred.pn</ta>
            <ta e="T119" id="Seg_2718" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_2719" s="T119">adj</ta>
            <ta e="T121" id="Seg_2720" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_2721" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_2722" s="T122">v-v:tense-v:pred.pn</ta>
            <ta e="T124" id="Seg_2723" s="T123">v-v:cvb</ta>
            <ta e="T125" id="Seg_2724" s="T124">dempro-pro:case</ta>
            <ta e="T126" id="Seg_2725" s="T125">v-v:cvb</ta>
            <ta e="T127" id="Seg_2726" s="T126">dempro</ta>
            <ta e="T128" id="Seg_2727" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_2728" s="T128">adv-adv&gt;adv</ta>
            <ta e="T130" id="Seg_2729" s="T129">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T131" id="Seg_2730" s="T130">v-v:tense-v:pred.pn</ta>
            <ta e="T132" id="Seg_2731" s="T131">adv-adv&gt;adj</ta>
            <ta e="T133" id="Seg_2732" s="T132">n-n:case</ta>
            <ta e="T134" id="Seg_2733" s="T133">n-n:poss-n:case</ta>
            <ta e="T135" id="Seg_2734" s="T134">post</ta>
            <ta e="T136" id="Seg_2735" s="T135">adv</ta>
            <ta e="T137" id="Seg_2736" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_2737" s="T137">v-v:mood-v:pred.pn</ta>
            <ta e="T139" id="Seg_2738" s="T138">adj</ta>
            <ta e="T140" id="Seg_2739" s="T139">n-n:case</ta>
            <ta e="T141" id="Seg_2740" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_2741" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_2742" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_2743" s="T143">dempro-pro:case</ta>
            <ta e="T145" id="Seg_2744" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_2745" s="T145">v-v:cvb</ta>
            <ta e="T147" id="Seg_2746" s="T146">v-v:tense-v:pred.pn</ta>
            <ta e="T148" id="Seg_2747" s="T147">dempro</ta>
            <ta e="T149" id="Seg_2748" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_2749" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_2750" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_2751" s="T151">v-v:tense-v:pred.pn</ta>
            <ta e="T153" id="Seg_2752" s="T152">dempro-pro:(poss)-pro:case</ta>
            <ta e="T154" id="Seg_2753" s="T153">v-v:cvb</ta>
            <ta e="T155" id="Seg_2754" s="T154">n-n:poss-n:case</ta>
            <ta e="T156" id="Seg_2755" s="T155">v-v:tense-v:pred.pn</ta>
            <ta e="T157" id="Seg_2756" s="T156">n-n:(poss)-n:case</ta>
            <ta e="T158" id="Seg_2757" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_2758" s="T158">v-v:cvb</ta>
            <ta e="T160" id="Seg_2759" s="T159">adv</ta>
            <ta e="T161" id="Seg_2760" s="T160">n-n:case</ta>
            <ta e="T162" id="Seg_2761" s="T161">v-v:tense-v:pred.pn</ta>
            <ta e="T163" id="Seg_2762" s="T162">adv</ta>
            <ta e="T164" id="Seg_2763" s="T163">v-v:tense-v:poss.pn</ta>
            <ta e="T165" id="Seg_2764" s="T164">n-n:case</ta>
            <ta e="T166" id="Seg_2765" s="T165">n-n:poss-n:case</ta>
            <ta e="T167" id="Seg_2766" s="T166">cardnum</ta>
            <ta e="T168" id="Seg_2767" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_2768" s="T168">n-n&gt;v-v:cvb</ta>
            <ta e="T170" id="Seg_2769" s="T169">v-v:ptcp</ta>
            <ta e="T171" id="Seg_2770" s="T170">v-v:tense-v:pred.pn</ta>
            <ta e="T172" id="Seg_2771" s="T171">dempro</ta>
            <ta e="T173" id="Seg_2772" s="T172">n-n:case</ta>
            <ta e="T174" id="Seg_2773" s="T173">adj-n:case</ta>
            <ta e="T175" id="Seg_2774" s="T174">v-v:cvb</ta>
            <ta e="T176" id="Seg_2775" s="T175">post</ta>
            <ta e="T177" id="Seg_2776" s="T176">adv</ta>
            <ta e="T178" id="Seg_2777" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_2778" s="T178">v-v:tense-v:pred.pn</ta>
            <ta e="T180" id="Seg_2779" s="T179">dempro-pro:case</ta>
            <ta e="T181" id="Seg_2780" s="T180">v-v:tense-v:pred.pn</ta>
            <ta e="T182" id="Seg_2781" s="T181">ptcl</ta>
            <ta e="T183" id="Seg_2782" s="T182">dempro</ta>
            <ta e="T184" id="Seg_2783" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_2784" s="T184">v-v:cvb</ta>
            <ta e="T186" id="Seg_2785" s="T185">v-v:tense-v:pred.pn</ta>
            <ta e="T187" id="Seg_2786" s="T186">n-n:poss-n:case</ta>
            <ta e="T188" id="Seg_2787" s="T187">n-n:poss-n:case</ta>
            <ta e="T189" id="Seg_2788" s="T188">v-v:tense-v:pred.pn</ta>
            <ta e="T190" id="Seg_2789" s="T189">conj</ta>
            <ta e="T191" id="Seg_2790" s="T190">v-v:ptcp</ta>
            <ta e="T192" id="Seg_2791" s="T191">post</ta>
            <ta e="T193" id="Seg_2792" s="T192">v-v:tense-v:pred.pn</ta>
            <ta e="T194" id="Seg_2793" s="T193">n-n:poss-n:case</ta>
            <ta e="T195" id="Seg_2794" s="T194">post</ta>
            <ta e="T196" id="Seg_2795" s="T195">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T197" id="Seg_2796" s="T196">cardnum</ta>
            <ta e="T198" id="Seg_2797" s="T197">n-n&gt;adj</ta>
            <ta e="T199" id="Seg_2798" s="T198">n-n:case</ta>
            <ta e="T200" id="Seg_2799" s="T199">v-v:cvb</ta>
            <ta e="T201" id="Seg_2800" s="T200">v-v:ptcp</ta>
            <ta e="T202" id="Seg_2801" s="T201">v-v:tense-v:pred.pn</ta>
            <ta e="T203" id="Seg_2802" s="T202">v-v:cvb</ta>
            <ta e="T204" id="Seg_2803" s="T203">n-n:poss-n:case</ta>
            <ta e="T205" id="Seg_2804" s="T204">n-n&gt;adj</ta>
            <ta e="T206" id="Seg_2805" s="T205">n-n:poss-n:case</ta>
            <ta e="T207" id="Seg_2806" s="T206">v-v:tense-v:poss.pn</ta>
            <ta e="T208" id="Seg_2807" s="T207">dempro</ta>
            <ta e="T209" id="Seg_2808" s="T208">n-n&gt;adj</ta>
            <ta e="T210" id="Seg_2809" s="T209">n-n:(poss)-n:case</ta>
            <ta e="T211" id="Seg_2810" s="T210">dempro</ta>
            <ta e="T212" id="Seg_2811" s="T211">n-n&gt;adj</ta>
            <ta e="T213" id="Seg_2812" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_2813" s="T213">n-n:poss-n:case</ta>
            <ta e="T215" id="Seg_2814" s="T214">v-v:tense-v:pred.pn</ta>
            <ta e="T216" id="Seg_2815" s="T215">dempro</ta>
            <ta e="T217" id="Seg_2816" s="T216">v-v:ptcp</ta>
            <ta e="T218" id="Seg_2817" s="T217">n-n:case</ta>
            <ta e="T219" id="Seg_2818" s="T218">v-v:tense-v:pred.pn</ta>
            <ta e="T220" id="Seg_2819" s="T219">adj</ta>
            <ta e="T221" id="Seg_2820" s="T220">n-n:case</ta>
            <ta e="T222" id="Seg_2821" s="T221">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T223" id="Seg_2822" s="T222">v-v:ptcp</ta>
            <ta e="T224" id="Seg_2823" s="T223">v-v:mood-v:pred.pn</ta>
            <ta e="T225" id="Seg_2824" s="T224">pers-pro:case</ta>
            <ta e="T226" id="Seg_2825" s="T225">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T227" id="Seg_2826" s="T226">n-n:case</ta>
            <ta e="T228" id="Seg_2827" s="T227">v-v:ptcp-v:(case)</ta>
            <ta e="T229" id="Seg_2828" s="T228">v-v:ptcp</ta>
            <ta e="T230" id="Seg_2829" s="T229">v-v:tense-v:pred.pn</ta>
            <ta e="T231" id="Seg_2830" s="T230">adv</ta>
            <ta e="T232" id="Seg_2831" s="T231">que-pro:case</ta>
            <ta e="T233" id="Seg_2832" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_2833" s="T233">pers-pro:case</ta>
            <ta e="T235" id="Seg_2834" s="T234">post</ta>
            <ta e="T236" id="Seg_2835" s="T235">v-v:tense-v:poss.pn</ta>
            <ta e="T237" id="Seg_2836" s="T236">conj</ta>
            <ta e="T238" id="Seg_2837" s="T237">pers-pro:case</ta>
            <ta e="T239" id="Seg_2838" s="T238">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T240" id="Seg_2839" s="T239">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T241" id="Seg_2840" s="T240">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2841" s="T0">adv</ta>
            <ta e="T2" id="Seg_2842" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_2843" s="T2">adj</ta>
            <ta e="T4" id="Seg_2844" s="T3">n</ta>
            <ta e="T5" id="Seg_2845" s="T4">n</ta>
            <ta e="T6" id="Seg_2846" s="T5">v</ta>
            <ta e="T7" id="Seg_2847" s="T6">dempro</ta>
            <ta e="T8" id="Seg_2848" s="T7">v</ta>
            <ta e="T9" id="Seg_2849" s="T8">v</ta>
            <ta e="T10" id="Seg_2850" s="T9">adj</ta>
            <ta e="T11" id="Seg_2851" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_2852" s="T11">n</ta>
            <ta e="T13" id="Seg_2853" s="T12">n</ta>
            <ta e="T14" id="Seg_2854" s="T13">n</ta>
            <ta e="T15" id="Seg_2855" s="T14">v</ta>
            <ta e="T16" id="Seg_2856" s="T15">n</ta>
            <ta e="T17" id="Seg_2857" s="T16">n</ta>
            <ta e="T18" id="Seg_2858" s="T17">n</ta>
            <ta e="T19" id="Seg_2859" s="T18">n</ta>
            <ta e="T20" id="Seg_2860" s="T19">post</ta>
            <ta e="T21" id="Seg_2861" s="T20">cop</ta>
            <ta e="T22" id="Seg_2862" s="T21">dempro</ta>
            <ta e="T23" id="Seg_2863" s="T22">v</ta>
            <ta e="T24" id="Seg_2864" s="T23">n</ta>
            <ta e="T25" id="Seg_2865" s="T24">adj</ta>
            <ta e="T26" id="Seg_2866" s="T25">cop</ta>
            <ta e="T27" id="Seg_2867" s="T26">que</ta>
            <ta e="T28" id="Seg_2868" s="T27">post</ta>
            <ta e="T29" id="Seg_2869" s="T28">adv</ta>
            <ta e="T30" id="Seg_2870" s="T29">v</ta>
            <ta e="T31" id="Seg_2871" s="T30">dempro</ta>
            <ta e="T32" id="Seg_2872" s="T31">post</ta>
            <ta e="T33" id="Seg_2873" s="T32">pers</ta>
            <ta e="T34" id="Seg_2874" s="T33">v</ta>
            <ta e="T35" id="Seg_2875" s="T34">adv</ta>
            <ta e="T36" id="Seg_2876" s="T35">v</ta>
            <ta e="T37" id="Seg_2877" s="T36">n</ta>
            <ta e="T38" id="Seg_2878" s="T37">cop</ta>
            <ta e="T39" id="Seg_2879" s="T38">v</ta>
            <ta e="T40" id="Seg_2880" s="T39">v</ta>
            <ta e="T41" id="Seg_2881" s="T40">dempro</ta>
            <ta e="T42" id="Seg_2882" s="T41">n</ta>
            <ta e="T43" id="Seg_2883" s="T42">cop</ta>
            <ta e="T44" id="Seg_2884" s="T43">n</ta>
            <ta e="T45" id="Seg_2885" s="T44">v</ta>
            <ta e="T46" id="Seg_2886" s="T45">n</ta>
            <ta e="T47" id="Seg_2887" s="T46">v</ta>
            <ta e="T48" id="Seg_2888" s="T47">aux</ta>
            <ta e="T49" id="Seg_2889" s="T48">dempro</ta>
            <ta e="T50" id="Seg_2890" s="T49">adj</ta>
            <ta e="T51" id="Seg_2891" s="T50">n</ta>
            <ta e="T52" id="Seg_2892" s="T51">n</ta>
            <ta e="T53" id="Seg_2893" s="T52">v</ta>
            <ta e="T54" id="Seg_2894" s="T53">aux</ta>
            <ta e="T55" id="Seg_2895" s="T54">adj</ta>
            <ta e="T56" id="Seg_2896" s="T55">ptcl</ta>
            <ta e="T57" id="Seg_2897" s="T56">n</ta>
            <ta e="T58" id="Seg_2898" s="T57">v</ta>
            <ta e="T59" id="Seg_2899" s="T58">adj</ta>
            <ta e="T60" id="Seg_2900" s="T59">post</ta>
            <ta e="T61" id="Seg_2901" s="T60">n</ta>
            <ta e="T62" id="Seg_2902" s="T61">v</ta>
            <ta e="T63" id="Seg_2903" s="T62">v</ta>
            <ta e="T64" id="Seg_2904" s="T63">n</ta>
            <ta e="T65" id="Seg_2905" s="T64">v</ta>
            <ta e="T66" id="Seg_2906" s="T65">n</ta>
            <ta e="T67" id="Seg_2907" s="T66">post</ta>
            <ta e="T68" id="Seg_2908" s="T67">v</ta>
            <ta e="T69" id="Seg_2909" s="T68">post</ta>
            <ta e="T70" id="Seg_2910" s="T69">v</ta>
            <ta e="T71" id="Seg_2911" s="T70">n</ta>
            <ta e="T72" id="Seg_2912" s="T71">n</ta>
            <ta e="T73" id="Seg_2913" s="T72">n</ta>
            <ta e="T74" id="Seg_2914" s="T73">v</ta>
            <ta e="T75" id="Seg_2915" s="T74">v</ta>
            <ta e="T76" id="Seg_2916" s="T75">n</ta>
            <ta e="T77" id="Seg_2917" s="T76">v</ta>
            <ta e="T78" id="Seg_2918" s="T77">v</ta>
            <ta e="T79" id="Seg_2919" s="T78">n</ta>
            <ta e="T80" id="Seg_2920" s="T79">v</ta>
            <ta e="T81" id="Seg_2921" s="T80">n</ta>
            <ta e="T82" id="Seg_2922" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_2923" s="T82">v</ta>
            <ta e="T84" id="Seg_2924" s="T83">aux</ta>
            <ta e="T85" id="Seg_2925" s="T84">adv</ta>
            <ta e="T86" id="Seg_2926" s="T85">v</ta>
            <ta e="T87" id="Seg_2927" s="T86">cardnum</ta>
            <ta e="T88" id="Seg_2928" s="T87">n</ta>
            <ta e="T89" id="Seg_2929" s="T88">v</ta>
            <ta e="T90" id="Seg_2930" s="T89">adv</ta>
            <ta e="T91" id="Seg_2931" s="T90">adj</ta>
            <ta e="T92" id="Seg_2932" s="T91">n</ta>
            <ta e="T93" id="Seg_2933" s="T92">n</ta>
            <ta e="T94" id="Seg_2934" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_2935" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_2936" s="T95">v</ta>
            <ta e="T97" id="Seg_2937" s="T96">v</ta>
            <ta e="T98" id="Seg_2938" s="T97">aux</ta>
            <ta e="T99" id="Seg_2939" s="T98">dempro</ta>
            <ta e="T100" id="Seg_2940" s="T99">n</ta>
            <ta e="T101" id="Seg_2941" s="T100">dempro</ta>
            <ta e="T102" id="Seg_2942" s="T101">adj</ta>
            <ta e="T103" id="Seg_2943" s="T102">n</ta>
            <ta e="T104" id="Seg_2944" s="T103">n</ta>
            <ta e="T105" id="Seg_2945" s="T104">cardnum</ta>
            <ta e="T106" id="Seg_2946" s="T105">n</ta>
            <ta e="T107" id="Seg_2947" s="T106">n</ta>
            <ta e="T108" id="Seg_2948" s="T107">v</ta>
            <ta e="T109" id="Seg_2949" s="T108">cardnum</ta>
            <ta e="T110" id="Seg_2950" s="T109">n</ta>
            <ta e="T111" id="Seg_2951" s="T110">n</ta>
            <ta e="T112" id="Seg_2952" s="T111">adv</ta>
            <ta e="T113" id="Seg_2953" s="T112">v</ta>
            <ta e="T114" id="Seg_2954" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_2955" s="T114">v</ta>
            <ta e="T116" id="Seg_2956" s="T115">dempro</ta>
            <ta e="T117" id="Seg_2957" s="T116">n</ta>
            <ta e="T118" id="Seg_2958" s="T117">v</ta>
            <ta e="T119" id="Seg_2959" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_2960" s="T119">adj</ta>
            <ta e="T121" id="Seg_2961" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_2962" s="T121">n</ta>
            <ta e="T123" id="Seg_2963" s="T122">cop</ta>
            <ta e="T124" id="Seg_2964" s="T123">v</ta>
            <ta e="T125" id="Seg_2965" s="T124">dempro</ta>
            <ta e="T126" id="Seg_2966" s="T125">v</ta>
            <ta e="T127" id="Seg_2967" s="T126">dempro</ta>
            <ta e="T128" id="Seg_2968" s="T127">n</ta>
            <ta e="T129" id="Seg_2969" s="T128">adv</ta>
            <ta e="T130" id="Seg_2970" s="T129">v</ta>
            <ta e="T131" id="Seg_2971" s="T130">v</ta>
            <ta e="T132" id="Seg_2972" s="T131">adj</ta>
            <ta e="T133" id="Seg_2973" s="T132">n</ta>
            <ta e="T134" id="Seg_2974" s="T133">n</ta>
            <ta e="T135" id="Seg_2975" s="T134">post</ta>
            <ta e="T136" id="Seg_2976" s="T135">adv</ta>
            <ta e="T137" id="Seg_2977" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_2978" s="T137">cop</ta>
            <ta e="T139" id="Seg_2979" s="T138">adj</ta>
            <ta e="T140" id="Seg_2980" s="T139">n</ta>
            <ta e="T141" id="Seg_2981" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_2982" s="T141">n</ta>
            <ta e="T143" id="Seg_2983" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_2984" s="T143">dempro</ta>
            <ta e="T145" id="Seg_2985" s="T144">n</ta>
            <ta e="T146" id="Seg_2986" s="T145">v</ta>
            <ta e="T147" id="Seg_2987" s="T146">v</ta>
            <ta e="T148" id="Seg_2988" s="T147">dempro</ta>
            <ta e="T149" id="Seg_2989" s="T148">n</ta>
            <ta e="T150" id="Seg_2990" s="T149">n</ta>
            <ta e="T151" id="Seg_2991" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_2992" s="T151">cop</ta>
            <ta e="T153" id="Seg_2993" s="T152">dempro</ta>
            <ta e="T154" id="Seg_2994" s="T153">v</ta>
            <ta e="T155" id="Seg_2995" s="T154">n</ta>
            <ta e="T156" id="Seg_2996" s="T155">v</ta>
            <ta e="T157" id="Seg_2997" s="T156">n</ta>
            <ta e="T158" id="Seg_2998" s="T157">n</ta>
            <ta e="T159" id="Seg_2999" s="T158">v</ta>
            <ta e="T160" id="Seg_3000" s="T159">adv</ta>
            <ta e="T161" id="Seg_3001" s="T160">n</ta>
            <ta e="T162" id="Seg_3002" s="T161">v</ta>
            <ta e="T163" id="Seg_3003" s="T162">adv</ta>
            <ta e="T164" id="Seg_3004" s="T163">v</ta>
            <ta e="T165" id="Seg_3005" s="T164">n</ta>
            <ta e="T166" id="Seg_3006" s="T165">n</ta>
            <ta e="T167" id="Seg_3007" s="T166">cardnum</ta>
            <ta e="T168" id="Seg_3008" s="T167">n</ta>
            <ta e="T169" id="Seg_3009" s="T168">v</ta>
            <ta e="T170" id="Seg_3010" s="T169">v</ta>
            <ta e="T171" id="Seg_3011" s="T170">aux</ta>
            <ta e="T172" id="Seg_3012" s="T171">dempro</ta>
            <ta e="T173" id="Seg_3013" s="T172">n</ta>
            <ta e="T174" id="Seg_3014" s="T173">adj</ta>
            <ta e="T175" id="Seg_3015" s="T174">cop</ta>
            <ta e="T176" id="Seg_3016" s="T175">post</ta>
            <ta e="T177" id="Seg_3017" s="T176">adv</ta>
            <ta e="T178" id="Seg_3018" s="T177">n</ta>
            <ta e="T179" id="Seg_3019" s="T178">cop</ta>
            <ta e="T180" id="Seg_3020" s="T179">dempro</ta>
            <ta e="T181" id="Seg_3021" s="T180">v</ta>
            <ta e="T182" id="Seg_3022" s="T181">ptcl</ta>
            <ta e="T183" id="Seg_3023" s="T182">dempro</ta>
            <ta e="T184" id="Seg_3024" s="T183">n</ta>
            <ta e="T185" id="Seg_3025" s="T184">v</ta>
            <ta e="T186" id="Seg_3026" s="T185">aux</ta>
            <ta e="T187" id="Seg_3027" s="T186">n</ta>
            <ta e="T188" id="Seg_3028" s="T187">n</ta>
            <ta e="T189" id="Seg_3029" s="T188">v</ta>
            <ta e="T190" id="Seg_3030" s="T189">conj</ta>
            <ta e="T191" id="Seg_3031" s="T190">v</ta>
            <ta e="T192" id="Seg_3032" s="T191">post</ta>
            <ta e="T193" id="Seg_3033" s="T192">cop</ta>
            <ta e="T194" id="Seg_3034" s="T193">n</ta>
            <ta e="T195" id="Seg_3035" s="T194">post</ta>
            <ta e="T196" id="Seg_3036" s="T195">v</ta>
            <ta e="T197" id="Seg_3037" s="T196">cardnum</ta>
            <ta e="T198" id="Seg_3038" s="T197">adj</ta>
            <ta e="T199" id="Seg_3039" s="T198">n</ta>
            <ta e="T200" id="Seg_3040" s="T199">v</ta>
            <ta e="T201" id="Seg_3041" s="T200">v</ta>
            <ta e="T202" id="Seg_3042" s="T201">aux</ta>
            <ta e="T203" id="Seg_3043" s="T202">v</ta>
            <ta e="T204" id="Seg_3044" s="T203">n</ta>
            <ta e="T205" id="Seg_3045" s="T204">adj</ta>
            <ta e="T206" id="Seg_3046" s="T205">n</ta>
            <ta e="T207" id="Seg_3047" s="T206">v</ta>
            <ta e="T208" id="Seg_3048" s="T207">dempro</ta>
            <ta e="T209" id="Seg_3049" s="T208">adj</ta>
            <ta e="T210" id="Seg_3050" s="T209">n</ta>
            <ta e="T211" id="Seg_3051" s="T210">dempro</ta>
            <ta e="T212" id="Seg_3052" s="T211">adj</ta>
            <ta e="T213" id="Seg_3053" s="T212">n</ta>
            <ta e="T214" id="Seg_3054" s="T213">n</ta>
            <ta e="T215" id="Seg_3055" s="T214">v</ta>
            <ta e="T216" id="Seg_3056" s="T215">dempro</ta>
            <ta e="T217" id="Seg_3057" s="T216">v</ta>
            <ta e="T218" id="Seg_3058" s="T217">n</ta>
            <ta e="T219" id="Seg_3059" s="T218">v</ta>
            <ta e="T220" id="Seg_3060" s="T219">adj</ta>
            <ta e="T221" id="Seg_3061" s="T220">n</ta>
            <ta e="T222" id="Seg_3062" s="T221">n</ta>
            <ta e="T223" id="Seg_3063" s="T222">v</ta>
            <ta e="T224" id="Seg_3064" s="T223">aux</ta>
            <ta e="T225" id="Seg_3065" s="T224">pers</ta>
            <ta e="T226" id="Seg_3066" s="T225">emphpro</ta>
            <ta e="T227" id="Seg_3067" s="T226">n</ta>
            <ta e="T228" id="Seg_3068" s="T227">v</ta>
            <ta e="T229" id="Seg_3069" s="T228">v</ta>
            <ta e="T230" id="Seg_3070" s="T229">aux</ta>
            <ta e="T231" id="Seg_3071" s="T230">adv</ta>
            <ta e="T232" id="Seg_3072" s="T231">que</ta>
            <ta e="T233" id="Seg_3073" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_3074" s="T233">pers</ta>
            <ta e="T235" id="Seg_3075" s="T234">post</ta>
            <ta e="T236" id="Seg_3076" s="T235">v</ta>
            <ta e="T237" id="Seg_3077" s="T236">conj</ta>
            <ta e="T238" id="Seg_3078" s="T237">pers</ta>
            <ta e="T239" id="Seg_3079" s="T238">v</ta>
            <ta e="T240" id="Seg_3080" s="T239">ptcl</ta>
            <ta e="T241" id="Seg_3081" s="T240">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_3082" s="T0">adv:Time</ta>
            <ta e="T3" id="Seg_3083" s="T2">np:Com</ta>
            <ta e="T4" id="Seg_3084" s="T3">np.h:A</ta>
            <ta e="T5" id="Seg_3085" s="T4">np:G</ta>
            <ta e="T12" id="Seg_3086" s="T11">np.h:Poss</ta>
            <ta e="T13" id="Seg_3087" s="T12">np:St</ta>
            <ta e="T14" id="Seg_3088" s="T13">np:L</ta>
            <ta e="T15" id="Seg_3089" s="T14">0.3.h:E</ta>
            <ta e="T16" id="Seg_3090" s="T15">np:Poss</ta>
            <ta e="T17" id="Seg_3091" s="T16">np:Th</ta>
            <ta e="T24" id="Seg_3092" s="T23">np.h:Th</ta>
            <ta e="T30" id="Seg_3093" s="T29">0.3.h:E</ta>
            <ta e="T33" id="Seg_3094" s="T32">pro.h:A</ta>
            <ta e="T36" id="Seg_3095" s="T35">0.3.h:A</ta>
            <ta e="T39" id="Seg_3096" s="T38">0.3.h:A</ta>
            <ta e="T44" id="Seg_3097" s="T43">np:G</ta>
            <ta e="T46" id="Seg_3098" s="T45">np:Th</ta>
            <ta e="T48" id="Seg_3099" s="T46">0.3.h:A</ta>
            <ta e="T51" id="Seg_3100" s="T50">np.h:Poss</ta>
            <ta e="T52" id="Seg_3101" s="T51">np:Th</ta>
            <ta e="T57" id="Seg_3102" s="T56">np:G</ta>
            <ta e="T58" id="Seg_3103" s="T57">0.3.h:Th</ta>
            <ta e="T61" id="Seg_3104" s="T60">np:Th</ta>
            <ta e="T63" id="Seg_3105" s="T62">0.3.h:E</ta>
            <ta e="T64" id="Seg_3106" s="T63">np:Th</ta>
            <ta e="T71" id="Seg_3107" s="T70">np.h:A</ta>
            <ta e="T73" id="Seg_3108" s="T72">np:G</ta>
            <ta e="T76" id="Seg_3109" s="T75">np:G</ta>
            <ta e="T78" id="Seg_3110" s="T77">0.3.h:A</ta>
            <ta e="T79" id="Seg_3111" s="T78">np:P</ta>
            <ta e="T81" id="Seg_3112" s="T80">np:Th</ta>
            <ta e="T86" id="Seg_3113" s="T85">0.3.h:E</ta>
            <ta e="T88" id="Seg_3114" s="T87">np.h:A</ta>
            <ta e="T93" id="Seg_3115" s="T92">np.h:Th</ta>
            <ta e="T100" id="Seg_3116" s="T99">np.h:A</ta>
            <ta e="T103" id="Seg_3117" s="T102">np.h:Poss</ta>
            <ta e="T104" id="Seg_3118" s="T103">np.h:A</ta>
            <ta e="T111" id="Seg_3119" s="T110">np:P</ta>
            <ta e="T113" id="Seg_3120" s="T112">0.3.h:A</ta>
            <ta e="T115" id="Seg_3121" s="T114">0.3.h:A</ta>
            <ta e="T117" id="Seg_3122" s="T116">np.h:A</ta>
            <ta e="T123" id="Seg_3123" s="T122">0.3.h:Th</ta>
            <ta e="T128" id="Seg_3124" s="T127">np.h:E</ta>
            <ta e="T133" id="Seg_3125" s="T132">np.h:Poss</ta>
            <ta e="T140" id="Seg_3126" s="T139">np:Th</ta>
            <ta e="T142" id="Seg_3127" s="T141">np:Th</ta>
            <ta e="T144" id="Seg_3128" s="T143">pro:Ins</ta>
            <ta e="T145" id="Seg_3129" s="T144">np:P</ta>
            <ta e="T147" id="Seg_3130" s="T146">0.3.h:A</ta>
            <ta e="T149" id="Seg_3131" s="T148">np.h:Poss</ta>
            <ta e="T150" id="Seg_3132" s="T149">np:Th</ta>
            <ta e="T153" id="Seg_3133" s="T152">pro:P</ta>
            <ta e="T155" id="Seg_3134" s="T154">np:Path</ta>
            <ta e="T156" id="Seg_3135" s="T155">0.3.h:A</ta>
            <ta e="T157" id="Seg_3136" s="T156">np:P</ta>
            <ta e="T158" id="Seg_3137" s="T157">np:G</ta>
            <ta e="T164" id="Seg_3138" s="T163">0.3.h:E</ta>
            <ta e="T166" id="Seg_3139" s="T165">np:L</ta>
            <ta e="T168" id="Seg_3140" s="T167">np.h:Th</ta>
            <ta e="T173" id="Seg_3141" s="T172">np.h:Th</ta>
            <ta e="T180" id="Seg_3142" s="T179">pro:St</ta>
            <ta e="T181" id="Seg_3143" s="T180">0.3.h:A</ta>
            <ta e="T184" id="Seg_3144" s="T183">np.h:A</ta>
            <ta e="T188" id="Seg_3145" s="T187">0.3.h:Poss np:G</ta>
            <ta e="T189" id="Seg_3146" s="T188">0.3.h:A</ta>
            <ta e="T192" id="Seg_3147" s="T190">pp:G</ta>
            <ta e="T199" id="Seg_3148" s="T198">np.h:A</ta>
            <ta e="T204" id="Seg_3149" s="T203">np:Poss</ta>
            <ta e="T207" id="Seg_3150" s="T206">0.3.h:A</ta>
            <ta e="T210" id="Seg_3151" s="T209">np:Th</ta>
            <ta e="T213" id="Seg_3152" s="T212">np.h:Poss</ta>
            <ta e="T214" id="Seg_3153" s="T213">np:L</ta>
            <ta e="T218" id="Seg_3154" s="T217">np.h:A</ta>
            <ta e="T221" id="Seg_3155" s="T220">np.h:Poss</ta>
            <ta e="T224" id="Seg_3156" s="T222">0.2.h:A</ta>
            <ta e="T225" id="Seg_3157" s="T224">pro.h:E</ta>
            <ta e="T227" id="Seg_3158" s="T226">np:Th</ta>
            <ta e="T232" id="Seg_3159" s="T231">pro.h:A</ta>
            <ta e="T235" id="Seg_3160" s="T233">pp:G</ta>
            <ta e="T238" id="Seg_3161" s="T237">pro.h:A</ta>
            <ta e="T241" id="Seg_3162" s="T240">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_3163" s="T3">np.h:S</ta>
            <ta e="T6" id="Seg_3164" s="T5">v:pred</ta>
            <ta e="T9" id="Seg_3165" s="T6">s:adv</ta>
            <ta e="T13" id="Seg_3166" s="T12">np:O</ta>
            <ta e="T15" id="Seg_3167" s="T14">0.3.h:S v:pred</ta>
            <ta e="T17" id="Seg_3168" s="T16">np:S</ta>
            <ta e="T19" id="Seg_3169" s="T18">n:pred</ta>
            <ta e="T21" id="Seg_3170" s="T20">cop</ta>
            <ta e="T23" id="Seg_3171" s="T22">s:rel</ta>
            <ta e="T24" id="Seg_3172" s="T23">np.h:S</ta>
            <ta e="T25" id="Seg_3173" s="T24">adj:pred</ta>
            <ta e="T26" id="Seg_3174" s="T25">cop</ta>
            <ta e="T27" id="Seg_3175" s="T26">pro:O</ta>
            <ta e="T30" id="Seg_3176" s="T29">0.3.h:S v:pred</ta>
            <ta e="T33" id="Seg_3177" s="T32">pro.h:S</ta>
            <ta e="T34" id="Seg_3178" s="T33">v:pred</ta>
            <ta e="T36" id="Seg_3179" s="T34">s:comp</ta>
            <ta e="T38" id="Seg_3180" s="T36">s:comp</ta>
            <ta e="T39" id="Seg_3181" s="T38">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_3182" s="T40">s:temp</ta>
            <ta e="T45" id="Seg_3183" s="T43">s:adv</ta>
            <ta e="T46" id="Seg_3184" s="T45">np:O</ta>
            <ta e="T48" id="Seg_3185" s="T46">0.3.h:S v:pred</ta>
            <ta e="T52" id="Seg_3186" s="T51">np:S</ta>
            <ta e="T54" id="Seg_3187" s="T52">v:pred</ta>
            <ta e="T58" id="Seg_3188" s="T57">0.3.h:S v:pred</ta>
            <ta e="T62" id="Seg_3189" s="T58">s:comp</ta>
            <ta e="T63" id="Seg_3190" s="T62">0.3.h:S v:pred</ta>
            <ta e="T69" id="Seg_3191" s="T63">s:temp</ta>
            <ta e="T70" id="Seg_3192" s="T69">s:rel</ta>
            <ta e="T71" id="Seg_3193" s="T70">np.h:S</ta>
            <ta e="T74" id="Seg_3194" s="T73">v:pred</ta>
            <ta e="T75" id="Seg_3195" s="T74">s:adv</ta>
            <ta e="T78" id="Seg_3196" s="T77">0.3.h:S v:pred</ta>
            <ta e="T79" id="Seg_3197" s="T78">np:S</ta>
            <ta e="T80" id="Seg_3198" s="T79">v:pred</ta>
            <ta e="T81" id="Seg_3199" s="T80">np:S</ta>
            <ta e="T84" id="Seg_3200" s="T82">v:pred</ta>
            <ta e="T86" id="Seg_3201" s="T85">0.3.h:S v:pred</ta>
            <ta e="T88" id="Seg_3202" s="T87">np.h:S</ta>
            <ta e="T89" id="Seg_3203" s="T88">v:pred</ta>
            <ta e="T93" id="Seg_3204" s="T92">np.h:S</ta>
            <ta e="T94" id="Seg_3205" s="T93">ptcl:pred</ta>
            <ta e="T98" id="Seg_3206" s="T96">v:pred</ta>
            <ta e="T100" id="Seg_3207" s="T99">np.h:S</ta>
            <ta e="T104" id="Seg_3208" s="T103">np.h:S</ta>
            <ta e="T108" id="Seg_3209" s="T107">v:pred</ta>
            <ta e="T111" id="Seg_3210" s="T110">np:O</ta>
            <ta e="T113" id="Seg_3211" s="T112">0.3.h:S v:pred</ta>
            <ta e="T115" id="Seg_3212" s="T114">0.3.h:S v:pred</ta>
            <ta e="T117" id="Seg_3213" s="T116">np.h:S</ta>
            <ta e="T118" id="Seg_3214" s="T117">v:pred</ta>
            <ta e="T122" id="Seg_3215" s="T121">n:pred</ta>
            <ta e="T123" id="Seg_3216" s="T122">0.3.h:S cop</ta>
            <ta e="T126" id="Seg_3217" s="T124">s:temp</ta>
            <ta e="T128" id="Seg_3218" s="T127">np.h:S</ta>
            <ta e="T130" id="Seg_3219" s="T128">s:comp</ta>
            <ta e="T131" id="Seg_3220" s="T130">v:pred</ta>
            <ta e="T137" id="Seg_3221" s="T136">ptcl:pred</ta>
            <ta e="T138" id="Seg_3222" s="T137">cop</ta>
            <ta e="T140" id="Seg_3223" s="T139">np:S</ta>
            <ta e="T142" id="Seg_3224" s="T141">np:S</ta>
            <ta e="T145" id="Seg_3225" s="T144">np:O</ta>
            <ta e="T147" id="Seg_3226" s="T146">0.3.h:S v:pred</ta>
            <ta e="T150" id="Seg_3227" s="T149">np:S</ta>
            <ta e="T151" id="Seg_3228" s="T150">ptcl:pred</ta>
            <ta e="T152" id="Seg_3229" s="T151">cop</ta>
            <ta e="T153" id="Seg_3230" s="T152">pro:O</ta>
            <ta e="T154" id="Seg_3231" s="T153">s:adv</ta>
            <ta e="T156" id="Seg_3232" s="T155">0.3.h:S v:pred</ta>
            <ta e="T157" id="Seg_3233" s="T156">np:S</ta>
            <ta e="T159" id="Seg_3234" s="T157">s:adv</ta>
            <ta e="T161" id="Seg_3235" s="T160">np:O</ta>
            <ta e="T162" id="Seg_3236" s="T161">v:pred</ta>
            <ta e="T164" id="Seg_3237" s="T163">0.3.h:S v:pred</ta>
            <ta e="T168" id="Seg_3238" s="T167">np.h:S</ta>
            <ta e="T169" id="Seg_3239" s="T168">s:adv</ta>
            <ta e="T171" id="Seg_3240" s="T169">v:pred</ta>
            <ta e="T173" id="Seg_3241" s="T172">np.h:S</ta>
            <ta e="T176" id="Seg_3242" s="T173">s:adv</ta>
            <ta e="T178" id="Seg_3243" s="T177">n:pred</ta>
            <ta e="T179" id="Seg_3244" s="T178">cop</ta>
            <ta e="T180" id="Seg_3245" s="T179">pro:O</ta>
            <ta e="T181" id="Seg_3246" s="T180">0.3.h:S v:pred</ta>
            <ta e="T184" id="Seg_3247" s="T183">np.h:S</ta>
            <ta e="T186" id="Seg_3248" s="T184">v:pred</ta>
            <ta e="T189" id="Seg_3249" s="T188">0.3.h:S v:pred</ta>
            <ta e="T199" id="Seg_3250" s="T198">np.h:S</ta>
            <ta e="T202" id="Seg_3251" s="T200">v:pred</ta>
            <ta e="T203" id="Seg_3252" s="T202">s:temp</ta>
            <ta e="T207" id="Seg_3253" s="T206">0.3.h:S v:pred</ta>
            <ta e="T210" id="Seg_3254" s="T209">np:S</ta>
            <ta e="T215" id="Seg_3255" s="T214">v:pred</ta>
            <ta e="T217" id="Seg_3256" s="T216">s:rel</ta>
            <ta e="T218" id="Seg_3257" s="T217">np.h:S</ta>
            <ta e="T219" id="Seg_3258" s="T218">v:pred</ta>
            <ta e="T224" id="Seg_3259" s="T222">0.2.h:S v:pred</ta>
            <ta e="T225" id="Seg_3260" s="T224">pro.h:S</ta>
            <ta e="T228" id="Seg_3261" s="T225">s:comp</ta>
            <ta e="T230" id="Seg_3262" s="T228">v:pred</ta>
            <ta e="T237" id="Seg_3263" s="T230">s:cond</ta>
            <ta e="T238" id="Seg_3264" s="T237">pro.h:S</ta>
            <ta e="T240" id="Seg_3265" s="T238">v:pred</ta>
            <ta e="T241" id="Seg_3266" s="T240">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T18" id="Seg_3267" s="T17">EV:cult</ta>
            <ta e="T91" id="Seg_3268" s="T90">YAK:core</ta>
            <ta e="T102" id="Seg_3269" s="T101">YAK:core</ta>
            <ta e="T190" id="Seg_3270" s="T189">RUS:gram</ta>
            <ta e="T220" id="Seg_3271" s="T219">YAK:core</ta>
            <ta e="T237" id="Seg_3272" s="T236">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T18" id="Seg_3273" s="T17">Vsub Vsub</ta>
            <ta e="T190" id="Seg_3274" s="T189">Vsub</ta>
            <ta e="T237" id="Seg_3275" s="T236">Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T18" id="Seg_3276" s="T17">dir:bare</ta>
            <ta e="T91" id="Seg_3277" s="T90">dir:bare</ta>
            <ta e="T102" id="Seg_3278" s="T101">dir:bare</ta>
            <ta e="T190" id="Seg_3279" s="T189">dir:bare</ta>
            <ta e="T220" id="Seg_3280" s="T219">dir:bare</ta>
            <ta e="T237" id="Seg_3281" s="T236">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_3282" s="T0">Long ago one man went with dogs into the Tundra.</ta>
            <ta e="T15" id="Seg_3283" s="T6">On his way he saw traces of a very big human in the snow.</ta>
            <ta e="T21" id="Seg_3284" s="T15">The trace was apparently as big as the trace of a hunting shield.</ta>
            <ta e="T30" id="Seg_3285" s="T21">The human, who saw it, was young and was not that much afraid.</ta>
            <ta e="T36" id="Seg_3286" s="T30">But still he was rather afraid to go by day.</ta>
            <ta e="T40" id="Seg_3287" s="T36">He waited till evening fell.</ta>
            <ta e="T48" id="Seg_3288" s="T40">As evening had come, he sat down onto his dog sledge and followed the trace.</ta>
            <ta e="T54" id="Seg_3289" s="T48">The [number of] big people's traces increased.</ta>
            <ta e="T58" id="Seg_3290" s="T54">He reached a very big yurt.</ta>
            <ta e="T63" id="Seg_3291" s="T58">He saw big [cargo] sledges standing there.</ta>
            <ta e="T84" id="Seg_3292" s="T63">After having placed the dogs in the direction of the way he had come from, the human climbed up the yurt, looked into the chimney and saw, that the fire had gone out, just the coals were glowing red.</ta>
            <ta e="T90" id="Seg_3293" s="T84">Suddenly he heard that one human apparently told a tale.</ta>
            <ta e="T100" id="Seg_3294" s="T90">"There are the people of a white czar, they say", that storyteller was telling.</ta>
            <ta e="T115" id="Seg_3295" s="T100">"Those czar's people dress themselves with fur of a reindeer calf, they cook two meals out of the hind legs of one reindeer calf, they say," he says.</ta>
            <ta e="T118" id="Seg_3296" s="T115">There the listeners laugh:</ta>
            <ta e="T124" id="Seg_3297" s="T118">"Well, those are really strange people", they said.</ta>
            <ta e="T131" id="Seg_3298" s="T124">As he heard this, this human he wanted to see [them] very much.</ta>
            <ta e="T143" id="Seg_3299" s="T131">Earlier humans used to take along a flintstone together with rotten wood or dry splints.</ta>
            <ta e="T147" id="Seg_3300" s="T143">They inflamed sparks with those. </ta>
            <ta e="T152" id="Seg_3301" s="T147">That human apparently had dry splints.</ta>
            <ta e="T171" id="Seg_3302" s="T152">He lighted those and threw them into the chimney, the splints fell onto the coals and flashed them up suddenly, there he saw one human sitting in front of the fire, telling stories.</ta>
            <ta e="T179" id="Seg_3303" s="T171">That human was naked, but completely covered with hair.</ta>
            <ta e="T193" id="Seg_3304" s="T179">As he saw this, the human jumped down from the yurt, sat down onto his dog sledge and went back from where he had come.</ta>
            <ta e="T215" id="Seg_3305" s="T193">He looked back, one hairy human was following him, as he caught up with him, he seized the back legs of the sledge, those back legs stayed in the hand of the hairy human.</ta>
            <ta e="T219" id="Seg_3306" s="T215">That human, who had taken the back legs, shouted:</ta>
            <ta e="T230" id="Seg_3307" s="T219">"People of the white czar, don't you dare to come back, we do not want [any] message about us to be spread out.</ta>
            <ta e="T241" id="Seg_3308" s="T230">If somebody comes to us now, we won't let him go back!", he said.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_3309" s="T0">Vor langer Zeit fuhr einmal ein Mensch mit Hunden in die Tundra.</ta>
            <ta e="T15" id="Seg_3310" s="T6">Als er fuhr, sah er im Schnee die Spuren eines riesigen Menschen.</ta>
            <ta e="T21" id="Seg_3311" s="T15">Die Spur war wohl so groß wie die Spur eines Schutzschildes bei der Jagd.</ta>
            <ta e="T30" id="Seg_3312" s="T21">Dieser Mensch, der sie sah, war wohl jung und hatte nicht viel Angst.</ta>
            <ta e="T36" id="Seg_3313" s="T30">Dennoch hatte er etwas Angst davor, bei Tag zu gehen.</ta>
            <ta e="T40" id="Seg_3314" s="T36">Er wartete, bis es Abend wurde.</ta>
            <ta e="T48" id="Seg_3315" s="T40">Als es Abend geworden war, setzte er sich auf seinen Hundeschlitten und folgte der Spur.</ta>
            <ta e="T54" id="Seg_3316" s="T48">Die Spuren der großen Leute wurden mehr.</ta>
            <ta e="T58" id="Seg_3317" s="T54">Er kam zu einer riesigen Jurte.</ta>
            <ta e="T63" id="Seg_3318" s="T58">Er sah dort große [Last]Schlitten stehen.</ta>
            <ta e="T84" id="Seg_3319" s="T63">Der Mensch, der gekommen war, stellte die Hunde in Richtung des Weges, den er gekommen war, kletterte auf die Jurte, schaute in das Rauchloch und sah, dass das Feuer ausgegangen war, dass nur die Kohlen etwas rot glühten.</ta>
            <ta e="T90" id="Seg_3320" s="T84">Plötzlich hörte er, dass ein Mensch offenbar ein Märchen erzählte.</ta>
            <ta e="T100" id="Seg_3321" s="T90">"Es gibt, sagt man, die Leute eines weißen Zaren", erzählte dieser Märchenerzähler.</ta>
            <ta e="T115" id="Seg_3322" s="T100">"Die Leute dieses weißen Zaren ziehen das Fell eines Rentierkalbes an, aus den Hinterbeinen eines Rentierkalbes machen sie zwei Mahlzeiten, sagt man", sagt er.</ta>
            <ta e="T118" id="Seg_3323" s="T115">Da lachen die Zuhörer:</ta>
            <ta e="T124" id="Seg_3324" s="T118">"Das sind aber merkwürdige Leute", sagten sie.</ta>
            <ta e="T131" id="Seg_3325" s="T124">Als er das hörte, wollte dieser Mensch sie unbedingt sehen.</ta>
            <ta e="T143" id="Seg_3326" s="T131">Früher hatten die Menschen einen Feuerstein und verfaultes Holz oder trockene Holzspäne dabei.</ta>
            <ta e="T147" id="Seg_3327" s="T143">Damit entfachten sie Funken.</ta>
            <ta e="T152" id="Seg_3328" s="T147">Dieser Mensch hatte wohl trockene Holzspäne.</ta>
            <ta e="T171" id="Seg_3329" s="T152">Er zündete diese an und warf sie durch das Rauchloch, die Holzspäne fielen auf die Kohlen und diese loderten auf, da sah er, dass vor dem Feuer ein Mensch saß und Märchen erzählte.</ta>
            <ta e="T179" id="Seg_3330" s="T171">Dieser Mensch war nackt, aber ganz mit Haaren bedeckt.</ta>
            <ta e="T193" id="Seg_3331" s="T179">Als er das sah, sprang der Mensch von der Jurte, setzte sich auf seinen Hundeschlitten und fuhr zurück.</ta>
            <ta e="T215" id="Seg_3332" s="T193">Er schaute zurück, es verfolgte ihn ein behaartet Mensch, als er ihn erreicht hatte, griff er die hinteren Beine des Schlittens, diese hinteren Beine blieben in der Hand des behaarten Menschen.</ta>
            <ta e="T219" id="Seg_3333" s="T215">Dieser Mensch, der die hinteren Beine genommen hatte, schrie:</ta>
            <ta e="T230" id="Seg_3334" s="T219">"Leute des weißen Zaren, kehrt ja nicht zurück, wir wollen nicht, dass sich die Nachricht über uns verbreitet.</ta>
            <ta e="T241" id="Seg_3335" s="T230">Wenn jetzt jemand zu uns kommt, dann lassen wir ihn nicht zurück!", sagte er.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_3336" s="T0">В старину один человек поехал на собаках в тундру.</ta>
            <ta e="T15" id="Seg_3337" s="T6">По дороге он увидел на снегу следы огромного человека.</ta>
            <ta e="T21" id="Seg_3338" s="T15">Величина следа была со след щита для укрытия при охоте.</ta>
            <ta e="T30" id="Seg_3339" s="T21">Тот человек, увидевший [след], молодой был и не очень боязливый.</ta>
            <ta e="T36" id="Seg_3340" s="T30">Но все-таки он не решился поехать [по следу] днем.</ta>
            <ta e="T40" id="Seg_3341" s="T36">Стал дожидаться вечерней поры.</ta>
            <ta e="T48" id="Seg_3342" s="T40">Когда наступил вечер, сев на собачьи нарты, поехал по следу.</ta>
            <ta e="T54" id="Seg_3343" s="T48">Следов огромных людей стало больше.</ta>
            <ta e="T58" id="Seg_3344" s="T54">Подъехал к громадной юрте.</ta>
            <ta e="T63" id="Seg_3345" s="T58">Увидел: стоят очень большие [грузовые] нарты.</ta>
            <ta e="T84" id="Seg_3346" s="T63">Поставив собак мордами в обратную сторону, пришедший человек взобрался на балаган, поднявшись, заглянул в дымоход: огонь в их очаге потух, только угольки краснеют.</ta>
            <ta e="T90" id="Seg_3347" s="T84">Вдруг слышит, один человек сказывает, кажется, олонгко.</ta>
            <ta e="T100" id="Seg_3348" s="T90">"Есть, говорят, белого царя люди", сказывает, оказывается, этот рассказчик.</ta>
            <ta e="T115" id="Seg_3349" s="T100">"Того белого царя человек одевается в шкуру одного олененка, из задней ноги оленя варят два обеда, говорят", так сказывает.</ta>
            <ta e="T118" id="Seg_3350" s="T115">Тут слушатели смеются:</ta>
            <ta e="T124" id="Seg_3351" s="T118">"Вот ведь удивительные люди", говорят.</ta>
            <ta e="T131" id="Seg_3352" s="T124">Услышав это, человек тот очень захотел увидеть [их].</ta>
            <ta e="T143" id="Seg_3353" s="T131">В старину обычно человек носил вместе с огнивом гнилушку или сухие стружки из тальника.</ta>
            <ta e="T147" id="Seg_3354" s="T143">Ими разжигали от искорки огонь.</ta>
            <ta e="T152" id="Seg_3355" s="T147">У того человека были сухие стружки.</ta>
            <ta e="T171" id="Seg_3356" s="T152">Подпалив это, бросил в дымоход, упав на уголек, [они] неожиданно вспыхнули, тут он увидел, [что] перед очагом один человек сидит и сказывает олонгко.</ta>
            <ta e="T179" id="Seg_3357" s="T171">Тот человек сидел голым, но весь был покрыт волосами.</ta>
            <ta e="T193" id="Seg_3358" s="T179">Как только увидел это, тот человек спрыгнул [с балагана], сел на собачьи нарты и помчался прочь.</ta>
            <ta e="T215" id="Seg_3359" s="T193">Оглянулся назад — один волосатый человек догоняет его, догнав, схватил за задние копылья нарты, те задние копылья остались в руке волосатого человека.</ta>
            <ta e="T219" id="Seg_3360" s="T215">Тот человек, выдернувший [копылья], прокричал:</ta>
            <ta e="T230" id="Seg_3361" s="T219">"Люди белого паря, не смейте возвращаться, мы не хотим, чтобы слух о нас разнесся по свету.</ta>
            <ta e="T241" id="Seg_3362" s="T230">Впредь, если кто-нибудь приедет в нашу сторону, мы обратно не отпустим!", сказал.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_3363" s="T0">В старину один человек поехал на собаках в тундру.</ta>
            <ta e="T15" id="Seg_3364" s="T6">По дороге он увидел на снегу следы огромного человека.</ta>
            <ta e="T21" id="Seg_3365" s="T15">Величина следа была со след тюргэ для укрытия при охоте на диких.</ta>
            <ta e="T30" id="Seg_3366" s="T21">Тот человек, увидевший [след], молодой был и не очень боязливый.</ta>
            <ta e="T36" id="Seg_3367" s="T30">Но все-таки он не решился поехать [по следу] днем.</ta>
            <ta e="T40" id="Seg_3368" s="T36">Стал дожидаться вечерней поры.</ta>
            <ta e="T48" id="Seg_3369" s="T40">Когда наступил вечер, сев на собачьи нарты, поехал по следу.</ta>
            <ta e="T54" id="Seg_3370" s="T48">Следов огромных людей стало больше.</ta>
            <ta e="T58" id="Seg_3371" s="T54">Подъехал к громадному балагану.</ta>
            <ta e="T63" id="Seg_3372" s="T58">Увидел: стоят очень большие грузовые нарты.</ta>
            <ta e="T84" id="Seg_3373" s="T63">Поставив собак мордами в обратную сторону, пришедший человек взобрался на балаган, поднявшись, заглянул в дымоход: огонь в их очаге потух, только угольки краснеют.</ta>
            <ta e="T90" id="Seg_3374" s="T84">Вдруг слышит, один человек сказывает, кажется, олонгко.</ta>
            <ta e="T100" id="Seg_3375" s="T90">— Есть, говорят, белого царя люди, — сказывает, оказывается, этот олонгкосут.</ta>
            <ta e="T115" id="Seg_3376" s="T100">— Того белого царя человек одевается в шкуру одного тугута, из задней ноги оленя варят два обеда, говорят, — так сказывает.</ta>
            <ta e="T118" id="Seg_3377" s="T115">Тут слушатели смеются:</ta>
            <ta e="T124" id="Seg_3378" s="T118">— Вот ведь удивительные люди, — говорят.</ta>
            <ta e="T131" id="Seg_3379" s="T124">Услышав это, человек тот очень захотел увидеть [их].</ta>
            <ta e="T143" id="Seg_3380" s="T131">В старину обычно человек носил вместе с огнивом гнилушку или сухие стружки из тальника.</ta>
            <ta e="T147" id="Seg_3381" s="T143">Ими разжигали от искорки огонь.</ta>
            <ta e="T152" id="Seg_3382" s="T147">У того человека были сухие стружки.</ta>
            <ta e="T171" id="Seg_3383" s="T152">Подпалив это, бросил в дымоход, упав на уголек, [они] неожиданно вспыхнули, тут он увидел, [что] перед очагом один человек сидит и сказывает олонгко.</ta>
            <ta e="T179" id="Seg_3384" s="T171">Тот человек сидел голым, но весь был покрыт волосами.</ta>
            <ta e="T193" id="Seg_3385" s="T179">Как только увидел это, тот человек спрыгнул [с балагана], сел на собачьи нарты и помчался прочь.</ta>
            <ta e="T215" id="Seg_3386" s="T193">Оглянулся назад — один волосатый человек догоняет его, догнав, схватил за задние копылья нарты, те задние копылья остались в руке волосатого человека.</ta>
            <ta e="T219" id="Seg_3387" s="T215">Тот человек, выдернувший [копылья], прокричал:</ta>
            <ta e="T230" id="Seg_3388" s="T219">— Люди белого паря, не смейте возвращаться, мы не хотим, чтобы слух о нас разнесся по свету.</ta>
            <ta e="T241" id="Seg_3389" s="T230">Впредь, если кто-нибудь приедет в нашу сторону, мы обратно не отпустим!</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
