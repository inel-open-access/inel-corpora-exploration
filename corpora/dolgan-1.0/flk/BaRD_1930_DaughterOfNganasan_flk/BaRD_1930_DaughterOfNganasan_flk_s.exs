<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID113B4EF5-1026-A02E-5F35-D9F1B5A7924C">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>BaRD_1930_DaughterOfNganasan_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\BaRD_1930_DaughterOfNganasan_flk\BaRD_1930_DaughterOfNganasan_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">600</ud-information>
            <ud-information attribute-name="# HIAT:w">465</ud-information>
            <ud-information attribute-name="# e">465</ud-information>
            <ud-information attribute-name="# HIAT:u">71</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BaRD">
            <abbreviation>BaRD</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
         <tli id="T349" time="178.5" type="appl" />
         <tli id="T350" time="179.0" type="appl" />
         <tli id="T351" time="179.5" type="appl" />
         <tli id="T352" time="180.0" type="appl" />
         <tli id="T353" time="180.5" type="appl" />
         <tli id="T354" time="181.0" type="appl" />
         <tli id="T355" time="181.5" type="appl" />
         <tli id="T356" time="182.0" type="appl" />
         <tli id="T357" time="182.5" type="appl" />
         <tli id="T358" time="183.0" type="appl" />
         <tli id="T359" time="183.5" type="appl" />
         <tli id="T360" time="184.0" type="appl" />
         <tli id="T361" time="184.5" type="appl" />
         <tli id="T362" time="185.0" type="appl" />
         <tli id="T363" time="185.5" type="appl" />
         <tli id="T364" time="186.0" type="appl" />
         <tli id="T365" time="186.5" type="appl" />
         <tli id="T366" time="187.0" type="appl" />
         <tli id="T367" time="187.5" type="appl" />
         <tli id="T368" time="188.0" type="appl" />
         <tli id="T369" time="188.5" type="appl" />
         <tli id="T370" time="189.0" type="appl" />
         <tli id="T371" time="189.5" type="appl" />
         <tli id="T372" time="190.0" type="appl" />
         <tli id="T373" time="190.5" type="appl" />
         <tli id="T374" time="191.0" type="appl" />
         <tli id="T375" time="191.5" type="appl" />
         <tli id="T376" time="192.0" type="appl" />
         <tli id="T377" time="192.5" type="appl" />
         <tli id="T378" time="193.0" type="appl" />
         <tli id="T379" time="193.5" type="appl" />
         <tli id="T380" time="194.0" type="appl" />
         <tli id="T381" time="194.5" type="appl" />
         <tli id="T382" time="195.0" type="appl" />
         <tli id="T383" time="195.5" type="appl" />
         <tli id="T384" time="196.0" type="appl" />
         <tli id="T385" time="196.5" type="appl" />
         <tli id="T386" time="197.0" type="appl" />
         <tli id="T387" time="197.5" type="appl" />
         <tli id="T388" time="198.0" type="appl" />
         <tli id="T389" time="198.5" type="appl" />
         <tli id="T390" time="199.0" type="appl" />
         <tli id="T391" time="199.5" type="appl" />
         <tli id="T392" time="200.0" type="appl" />
         <tli id="T393" time="200.5" type="appl" />
         <tli id="T394" time="201.0" type="appl" />
         <tli id="T395" time="201.5" type="appl" />
         <tli id="T396" time="202.0" type="appl" />
         <tli id="T397" time="202.5" type="appl" />
         <tli id="T398" time="203.0" type="appl" />
         <tli id="T399" time="203.5" type="appl" />
         <tli id="T400" time="204.0" type="appl" />
         <tli id="T401" time="204.5" type="appl" />
         <tli id="T402" time="205.0" type="appl" />
         <tli id="T403" time="205.5" type="appl" />
         <tli id="T404" time="206.0" type="appl" />
         <tli id="T405" time="206.5" type="appl" />
         <tli id="T406" time="207.0" type="appl" />
         <tli id="T407" time="207.5" type="appl" />
         <tli id="T408" time="208.0" type="appl" />
         <tli id="T409" time="208.5" type="appl" />
         <tli id="T410" time="209.0" type="appl" />
         <tli id="T411" time="209.5" type="appl" />
         <tli id="T412" time="210.0" type="appl" />
         <tli id="T413" time="210.5" type="appl" />
         <tli id="T414" time="211.0" type="appl" />
         <tli id="T415" time="211.5" type="appl" />
         <tli id="T416" time="212.0" type="appl" />
         <tli id="T417" time="212.5" type="appl" />
         <tli id="T418" time="213.0" type="appl" />
         <tli id="T419" time="213.5" type="appl" />
         <tli id="T420" time="214.0" type="appl" />
         <tli id="T421" time="214.5" type="appl" />
         <tli id="T422" time="215.0" type="appl" />
         <tli id="T423" time="215.5" type="appl" />
         <tli id="T424" time="216.0" type="appl" />
         <tli id="T425" time="216.5" type="appl" />
         <tli id="T426" time="217.0" type="appl" />
         <tli id="T427" time="217.5" type="appl" />
         <tli id="T428" time="218.0" type="appl" />
         <tli id="T429" time="218.5" type="appl" />
         <tli id="T430" time="219.0" type="appl" />
         <tli id="T431" time="219.5" type="appl" />
         <tli id="T432" time="220.0" type="appl" />
         <tli id="T433" time="220.5" type="appl" />
         <tli id="T434" time="221.0" type="appl" />
         <tli id="T435" time="221.5" type="appl" />
         <tli id="T436" time="222.0" type="appl" />
         <tli id="T437" time="222.5" type="appl" />
         <tli id="T438" time="223.0" type="appl" />
         <tli id="T439" time="223.5" type="appl" />
         <tli id="T440" time="224.0" type="appl" />
         <tli id="T441" time="224.5" type="appl" />
         <tli id="T442" time="225.0" type="appl" />
         <tli id="T443" time="225.5" type="appl" />
         <tli id="T444" time="226.0" type="appl" />
         <tli id="T445" time="226.5" type="appl" />
         <tli id="T446" time="227.0" type="appl" />
         <tli id="T447" time="227.5" type="appl" />
         <tli id="T448" time="228.0" type="appl" />
         <tli id="T449" time="228.5" type="appl" />
         <tli id="T450" time="229.0" type="appl" />
         <tli id="T451" time="229.5" type="appl" />
         <tli id="T452" time="230.0" type="appl" />
         <tli id="T453" time="230.5" type="appl" />
         <tli id="T454" time="231.0" type="appl" />
         <tli id="T455" time="231.5" type="appl" />
         <tli id="T456" time="232.0" type="appl" />
         <tli id="T457" time="232.5" type="appl" />
         <tli id="T458" time="233.0" type="appl" />
         <tli id="T459" time="233.5" type="appl" />
         <tli id="T460" time="234.0" type="appl" />
         <tli id="T461" time="234.5" type="appl" />
         <tli id="T462" time="235.0" type="appl" />
         <tli id="T463" time="235.5" type="appl" />
         <tli id="T464" time="236.0" type="appl" />
         <tli id="T465" time="236.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BaRD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T465" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Bɨlɨr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">biːr</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">haːmaj</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">ogonnʼoro</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">olorbut</ts>
                  <nts id="Seg_17" n="HIAT:ip">,</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">kɨːstaːk</ts>
                  <nts id="Seg_21" n="HIAT:ip">.</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_24" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">Bu</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">kɨːha</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">ulaːtan</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">kusaːjka</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">bu͡olan</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">baran</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">ölön</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">kaːlbɨt</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_51" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">Manɨ</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">agata</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">hürdeːktik</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">ɨtɨːr</ts>
                  <nts id="Seg_63" n="HIAT:ip">.</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_66" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">Karajaːrɨ</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">gɨmmɨttarɨn</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">eter</ts>
                  <nts id="Seg_75" n="HIAT:ip">:</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_78" n="HIAT:u" s="T21">
                  <nts id="Seg_79" n="HIAT:ip">"</nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">Min</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">kɨːspɨn</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">bi͡ek</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">bejebin</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">kɨtta</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">ti͡eje</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_99" n="HIAT:w" s="T27">hɨldʼɨ͡am</ts>
                  <nts id="Seg_100" n="HIAT:ip">,</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_103" n="HIAT:w" s="T28">kini</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_106" n="HIAT:w" s="T29">tilli͡ege</ts>
                  <nts id="Seg_107" n="HIAT:ip">,</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">ulakan</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">ojuːŋŋa</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">tilinneri͡em</ts>
                  <nts id="Seg_117" n="HIAT:ip">"</nts>
                  <nts id="Seg_118" n="HIAT:ip">,</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_121" n="HIAT:w" s="T33">diːr</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_125" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_127" n="HIAT:w" s="T34">Dʼe</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_130" n="HIAT:w" s="T35">kɨːhɨn</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_133" n="HIAT:w" s="T36">hɨrgaga</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_136" n="HIAT:w" s="T37">ti͡eje</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_139" n="HIAT:w" s="T38">hɨldʼar</ts>
                  <nts id="Seg_140" n="HIAT:ip">.</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_143" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">Ol</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_148" n="HIAT:w" s="T40">hɨldʼan</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_151" n="HIAT:w" s="T41">ojunnarɨ</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_154" n="HIAT:w" s="T42">kördöhör</ts>
                  <nts id="Seg_155" n="HIAT:ip">,</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_158" n="HIAT:w" s="T43">biːrdere</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_161" n="HIAT:w" s="T44">da</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_164" n="HIAT:w" s="T45">bu͡olbat</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_168" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">Araj</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_173" n="HIAT:w" s="T47">ikki</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_176" n="HIAT:w" s="T48">ulakan</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_179" n="HIAT:w" s="T49">ojunu</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_182" n="HIAT:w" s="T50">kɨːrdarar</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_185" n="HIAT:w" s="T51">ikki</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_188" n="HIAT:w" s="T52">dʼɨla</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_191" n="HIAT:w" s="T53">aːspɨtɨn</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_194" n="HIAT:w" s="T54">genne</ts>
                  <nts id="Seg_195" n="HIAT:ip">.</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_198" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_200" n="HIAT:w" s="T55">Kɨːhɨn</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_203" n="HIAT:w" s="T56">ete</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_206" n="HIAT:w" s="T57">dabnu͡o</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_209" n="HIAT:w" s="T58">hɨtɨjan</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_212" n="HIAT:w" s="T59">uŋu͡oktara</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_215" n="HIAT:w" s="T60">ere</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_218" n="HIAT:w" s="T61">kaːlbɨt</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_222" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_224" n="HIAT:w" s="T62">Bu</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_227" n="HIAT:w" s="T63">ikki</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_230" n="HIAT:w" s="T64">ojun</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_233" n="HIAT:w" s="T65">kɨːrannar</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_236" n="HIAT:w" s="T66">tugu</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_239" n="HIAT:w" s="T67">da</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_242" n="HIAT:w" s="T68">gɨmmattar</ts>
                  <nts id="Seg_243" n="HIAT:ip">.</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_246" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_248" n="HIAT:w" s="T69">Ühüs</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_251" n="HIAT:w" s="T70">hɨla</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_254" n="HIAT:w" s="T71">bu͡olar</ts>
                  <nts id="Seg_255" n="HIAT:ip">.</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_258" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_260" n="HIAT:w" s="T72">Ol</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_263" n="HIAT:w" s="T73">haːmaj</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_266" n="HIAT:w" s="T74">ɨksatɨgar</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_269" n="HIAT:w" s="T75">ičeːn</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_272" n="HIAT:w" s="T76">baːr</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_275" n="HIAT:w" s="T77">ebit</ts>
                  <nts id="Seg_276" n="HIAT:ip">.</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_279" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_281" n="HIAT:w" s="T78">Bu</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_284" n="HIAT:w" s="T79">ičeːn</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_287" n="HIAT:w" s="T80">berke</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_290" n="HIAT:w" s="T81">ahɨnar</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_293" n="HIAT:w" s="T82">haːmajɨn</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_297" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_299" n="HIAT:w" s="T83">Biːrde</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_302" n="HIAT:w" s="T84">tühüːr</ts>
                  <nts id="Seg_303" n="HIAT:ip">.</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_306" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_308" n="HIAT:w" s="T85">Ol</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_311" n="HIAT:w" s="T86">tühü͡ön</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_314" n="HIAT:w" s="T87">baran</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_317" n="HIAT:w" s="T88">haːmajɨgar</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_320" n="HIAT:w" s="T89">kelen</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_323" n="HIAT:w" s="T90">kepsiːr</ts>
                  <nts id="Seg_324" n="HIAT:ip">:</nts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_327" n="HIAT:u" s="T91">
                  <nts id="Seg_328" n="HIAT:ip">"</nts>
                  <ts e="T92" id="Seg_330" n="HIAT:w" s="T91">Bu</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_333" n="HIAT:w" s="T92">en</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_336" n="HIAT:w" s="T93">kɨːhɨŋ</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_339" n="HIAT:w" s="T94">tilli͡ek</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_342" n="HIAT:w" s="T95">bɨhɨːlaːk</ts>
                  <nts id="Seg_343" n="HIAT:ip">,</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_346" n="HIAT:w" s="T96">mu͡okaha</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_349" n="HIAT:w" s="T97">biːr</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_352" n="HIAT:w" s="T98">ete</ts>
                  <nts id="Seg_353" n="HIAT:ip">.</nts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_356" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_358" n="HIAT:w" s="T99">Ogoŋ</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_361" n="HIAT:w" s="T100">kutun</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_364" n="HIAT:w" s="T101">hi͡ebit</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_367" n="HIAT:w" s="T102">ölüːte</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_370" n="HIAT:w" s="T103">ebege</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_373" n="HIAT:w" s="T104">ölüː</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_376" n="HIAT:w" s="T105">uːtugar</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_379" n="HIAT:w" s="T106">ildʼe</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_382" n="HIAT:w" s="T107">hɨldʼar</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_385" n="HIAT:w" s="T108">ebit</ts>
                  <nts id="Seg_386" n="HIAT:ip">.</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_389" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_391" n="HIAT:w" s="T109">Biːrges</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_394" n="HIAT:w" s="T110">ojunuŋ</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_397" n="HIAT:w" s="T111">kɨːl</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_400" n="HIAT:w" s="T112">bu͡olan</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_403" n="HIAT:w" s="T113">hirinen</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_406" n="HIAT:w" s="T114">batan</ts>
                  <nts id="Seg_407" n="HIAT:ip">,</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_410" n="HIAT:w" s="T115">tüːte</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_413" n="HIAT:w" s="T116">kɨraːrɨ</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_416" n="HIAT:w" s="T117">kajagaska</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_419" n="HIAT:w" s="T118">bappat</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_422" n="HIAT:w" s="T119">bu͡olar</ts>
                  <nts id="Seg_423" n="HIAT:ip">,</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_426" n="HIAT:w" s="T120">biːrges</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_429" n="HIAT:w" s="T121">kɨːl</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_432" n="HIAT:w" s="T122">bu͡olan</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_435" n="HIAT:w" s="T123">uːnnan</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_438" n="HIAT:w" s="T124">ustan</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_441" n="HIAT:w" s="T125">tijdegine</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_444" n="HIAT:w" s="T126">dolguna</ts>
                  <nts id="Seg_445" n="HIAT:ip">,</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_448" n="HIAT:w" s="T127">kabaːrɨ</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_451" n="HIAT:w" s="T128">ere</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_454" n="HIAT:w" s="T129">gɨnnagɨna</ts>
                  <nts id="Seg_455" n="HIAT:ip">,</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_458" n="HIAT:w" s="T130">kutun</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_461" n="HIAT:w" s="T131">ildʼen</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_464" n="HIAT:w" s="T132">keːher</ts>
                  <nts id="Seg_465" n="HIAT:ip">.</nts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_468" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_470" n="HIAT:w" s="T133">Togus</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_473" n="HIAT:w" s="T134">hɨl</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_476" n="HIAT:w" s="T135">ogurduk</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_479" n="HIAT:w" s="T136">batɨhɨ͡aktara</ts>
                  <nts id="Seg_480" n="HIAT:ip">,</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_483" n="HIAT:w" s="T137">onton</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_486" n="HIAT:w" s="T138">kajtak</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_489" n="HIAT:w" s="T139">bu͡olar</ts>
                  <nts id="Seg_490" n="HIAT:ip">.</nts>
                  <nts id="Seg_491" n="HIAT:ip">"</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_494" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_496" n="HIAT:w" s="T140">Onu͡oga</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_499" n="HIAT:w" s="T141">haːmaj</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_502" n="HIAT:w" s="T142">ogonnʼoro</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_505" n="HIAT:w" s="T143">eter</ts>
                  <nts id="Seg_506" n="HIAT:ip">:</nts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_509" n="HIAT:u" s="T144">
                  <nts id="Seg_510" n="HIAT:ip">"</nts>
                  <ts e="T145" id="Seg_512" n="HIAT:w" s="T144">Ol</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_515" n="HIAT:w" s="T145">ojun</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_518" n="HIAT:w" s="T146">kɨ͡ajbatɨn</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_521" n="HIAT:w" s="T147">biler</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_524" n="HIAT:w" s="T148">kihi</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_527" n="HIAT:w" s="T149">bejeŋ</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_530" n="HIAT:w" s="T150">togo</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_533" n="HIAT:w" s="T151">tilinneren</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_536" n="HIAT:w" s="T152">körbökkün</ts>
                  <nts id="Seg_537" n="HIAT:ip">"</nts>
                  <nts id="Seg_538" n="HIAT:ip">,</nts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_541" n="HIAT:w" s="T153">diːr</ts>
                  <nts id="Seg_542" n="HIAT:ip">,</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_545" n="HIAT:w" s="T154">aːttahar</ts>
                  <nts id="Seg_546" n="HIAT:ip">.</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T159" id="Seg_549" n="HIAT:u" s="T155">
                  <nts id="Seg_550" n="HIAT:ip">"</nts>
                  <ts e="T156" id="Seg_552" n="HIAT:w" s="T155">Dʼe</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_555" n="HIAT:w" s="T156">berke</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_558" n="HIAT:w" s="T157">muŋnannɨŋ</ts>
                  <nts id="Seg_559" n="HIAT:ip">"</nts>
                  <nts id="Seg_560" n="HIAT:ip">,</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_563" n="HIAT:w" s="T158">diːr</ts>
                  <nts id="Seg_564" n="HIAT:ip">.</nts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_567" n="HIAT:u" s="T159">
                  <nts id="Seg_568" n="HIAT:ip">"</nts>
                  <ts e="T160" id="Seg_570" n="HIAT:w" s="T159">Kajtak</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_573" n="HIAT:w" s="T160">gɨnabɨn</ts>
                  <nts id="Seg_574" n="HIAT:ip">,</nts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_577" n="HIAT:w" s="T161">tilinnegine</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_580" n="HIAT:w" s="T162">mi͡eke</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_583" n="HIAT:w" s="T163">dʼaktar</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_586" n="HIAT:w" s="T164">bi͡eri͡eŋ</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_589" n="HIAT:w" s="T165">duː</ts>
                  <nts id="Seg_590" n="HIAT:ip">?</nts>
                  <nts id="Seg_591" n="HIAT:ip">"</nts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_594" n="HIAT:u" s="T166">
                  <nts id="Seg_595" n="HIAT:ip">"</nts>
                  <ts e="T167" id="Seg_597" n="HIAT:w" s="T166">Baːjɨm</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_600" n="HIAT:w" s="T167">aŋarɨn</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_603" n="HIAT:w" s="T168">bi͡eri͡em</ts>
                  <nts id="Seg_604" n="HIAT:ip">,</nts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_607" n="HIAT:w" s="T169">bejetin</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_610" n="HIAT:w" s="T170">bi͡eri͡em</ts>
                  <nts id="Seg_611" n="HIAT:ip">,</nts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_614" n="HIAT:w" s="T171">tilinner</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_617" n="HIAT:w" s="T172">ere</ts>
                  <nts id="Seg_618" n="HIAT:ip">"</nts>
                  <nts id="Seg_619" n="HIAT:ip">,</nts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_622" n="HIAT:w" s="T173">diːr</ts>
                  <nts id="Seg_623" n="HIAT:ip">.</nts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_626" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_628" n="HIAT:w" s="T174">Ičeːne</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_631" n="HIAT:w" s="T175">utujan</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_634" n="HIAT:w" s="T176">kaːlar</ts>
                  <nts id="Seg_635" n="HIAT:ip">.</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T185" id="Seg_638" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_640" n="HIAT:w" s="T177">Bu</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_643" n="HIAT:w" s="T178">utuja</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_646" n="HIAT:w" s="T179">hɨtan</ts>
                  <nts id="Seg_647" n="HIAT:ip">,</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_650" n="HIAT:w" s="T180">dʼe</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_653" n="HIAT:w" s="T181">ol</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_656" n="HIAT:w" s="T182">ölüːtün</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_659" n="HIAT:w" s="T183">hiriger</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_662" n="HIAT:w" s="T184">tijer</ts>
                  <nts id="Seg_663" n="HIAT:ip">.</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_666" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_668" n="HIAT:w" s="T185">Dʼe</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_671" n="HIAT:w" s="T186">ol</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_674" n="HIAT:w" s="T187">ölüːtün</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_677" n="HIAT:w" s="T188">hiriger</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_680" n="HIAT:w" s="T189">tijer</ts>
                  <nts id="Seg_681" n="HIAT:ip">.</nts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_684" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_686" n="HIAT:w" s="T190">Tijbite</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_689" n="HIAT:w" s="T191">elʼbek</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_692" n="HIAT:w" s="T192">bagajɨ</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_695" n="HIAT:w" s="T193">dʼon</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_698" n="HIAT:w" s="T194">üŋküːlüː</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_701" n="HIAT:w" s="T195">turallar</ts>
                  <nts id="Seg_702" n="HIAT:ip">.</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_705" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_707" n="HIAT:w" s="T196">Manna</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_710" n="HIAT:w" s="T197">kördüː</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_713" n="HIAT:w" s="T198">hatɨːr</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_716" n="HIAT:w" s="T199">kɨːstarɨn</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_719" n="HIAT:w" s="T200">kutun</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_721" n="HIAT:ip">—</nts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_724" n="HIAT:w" s="T201">tu͡ok</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_727" n="HIAT:w" s="T202">keli͡ej</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_730" n="HIAT:w" s="T203">vovse</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_733" n="HIAT:w" s="T204">bulbat</ts>
                  <nts id="Seg_734" n="HIAT:ip">.</nts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_737" n="HIAT:u" s="T205">
                  <nts id="Seg_738" n="HIAT:ip">"</nts>
                  <ts e="T206" id="Seg_740" n="HIAT:w" s="T205">Onton</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_743" n="HIAT:w" s="T206">bu</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_746" n="HIAT:w" s="T207">hirten</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_749" n="HIAT:w" s="T208">atɨn</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_752" n="HIAT:w" s="T209">mi͡estege</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_755" n="HIAT:w" s="T210">barɨːm</ts>
                  <nts id="Seg_756" n="HIAT:ip">"</nts>
                  <nts id="Seg_757" n="HIAT:ip">,</nts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_760" n="HIAT:w" s="T211">diːr</ts>
                  <nts id="Seg_761" n="HIAT:ip">.</nts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_764" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_766" n="HIAT:w" s="T212">Tijer</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_769" n="HIAT:w" s="T213">biːr</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_772" n="HIAT:w" s="T214">hirge</ts>
                  <nts id="Seg_773" n="HIAT:ip">.</nts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_776" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_778" n="HIAT:w" s="T215">Onno</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_781" n="HIAT:w" s="T216">üs</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_784" n="HIAT:w" s="T217">tebis</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_787" n="HIAT:w" s="T218">teŋ</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_790" n="HIAT:w" s="T219">hɨrajdaːk</ts>
                  <nts id="Seg_791" n="HIAT:ip">,</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_794" n="HIAT:w" s="T220">taŋastaːk</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_797" n="HIAT:w" s="T221">kɨːs</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_800" n="HIAT:w" s="T222">baːr</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_803" n="HIAT:w" s="T223">ebit</ts>
                  <nts id="Seg_804" n="HIAT:ip">.</nts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_807" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_809" n="HIAT:w" s="T224">Mantan</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_812" n="HIAT:w" s="T225">biːrgestere</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_815" n="HIAT:w" s="T226">ogonnʼor</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_818" n="HIAT:w" s="T227">kɨːha</ts>
                  <nts id="Seg_819" n="HIAT:ip">.</nts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_822" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_824" n="HIAT:w" s="T228">Kajalara</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_827" n="HIAT:w" s="T229">ol</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_830" n="HIAT:w" s="T230">kɨːhɨn</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_833" n="HIAT:w" s="T231">kɨ͡ajan</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_836" n="HIAT:w" s="T232">bilbet</ts>
                  <nts id="Seg_837" n="HIAT:ip">.</nts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_840" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_842" n="HIAT:w" s="T233">Taŋastarɨn</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_845" n="HIAT:w" s="T234">hiːgin</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_848" n="HIAT:w" s="T235">aːgan</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_851" n="HIAT:w" s="T236">körör</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_853" n="HIAT:ip">—</nts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_856" n="HIAT:w" s="T237">aksaːna</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_859" n="HIAT:w" s="T238">barɨta</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_862" n="HIAT:w" s="T239">biːr</ts>
                  <nts id="Seg_863" n="HIAT:ip">.</nts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_866" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_868" n="HIAT:w" s="T240">Muŋnana</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_871" n="HIAT:w" s="T241">hatɨːr</ts>
                  <nts id="Seg_872" n="HIAT:ip">.</nts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_875" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_877" n="HIAT:w" s="T242">Bu</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_880" n="HIAT:w" s="T243">kɨrgɨttar</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_883" n="HIAT:w" s="T244">üs</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_886" n="HIAT:w" s="T245">dʼi͡eleːkter</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_889" n="HIAT:w" s="T246">tus</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_892" n="HIAT:w" s="T247">tuspa</ts>
                  <nts id="Seg_893" n="HIAT:ip">.</nts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_896" n="HIAT:u" s="T248">
                  <ts e="T249" id="Seg_898" n="HIAT:w" s="T248">Onno</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_901" n="HIAT:w" s="T249">ki͡ehe</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_904" n="HIAT:w" s="T250">üŋküːtten</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_907" n="HIAT:w" s="T251">kelen</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_910" n="HIAT:w" s="T252">utujan</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_913" n="HIAT:w" s="T253">kaːlallar</ts>
                  <nts id="Seg_914" n="HIAT:ip">.</nts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T261" id="Seg_917" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_919" n="HIAT:w" s="T254">Kɨrgɨttarɨn</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_922" n="HIAT:w" s="T255">kɨtta</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_925" n="HIAT:w" s="T256">kepseteːri</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_928" n="HIAT:w" s="T257">gɨnar</ts>
                  <nts id="Seg_929" n="HIAT:ip">,</nts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_932" n="HIAT:w" s="T258">biːrdestere</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_935" n="HIAT:w" s="T259">da</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_938" n="HIAT:w" s="T260">haŋarbat</ts>
                  <nts id="Seg_939" n="HIAT:ip">.</nts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_942" n="HIAT:u" s="T261">
                  <ts e="T262" id="Seg_944" n="HIAT:w" s="T261">Kajdak</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_947" n="HIAT:w" s="T262">bu͡olu͡omuj</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_950" n="HIAT:w" s="T263">di͡en</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_953" n="HIAT:w" s="T264">hananar</ts>
                  <nts id="Seg_954" n="HIAT:ip">.</nts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T271" id="Seg_957" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_959" n="HIAT:w" s="T265">Birgesterin</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_962" n="HIAT:w" s="T266">dʼi͡etiger</ts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_965" n="HIAT:w" s="T267">hahan</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_968" n="HIAT:w" s="T268">kiːren</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_971" n="HIAT:w" s="T269">utujtun</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_974" n="HIAT:w" s="T270">ketiːr</ts>
                  <nts id="Seg_975" n="HIAT:ip">.</nts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_978" n="HIAT:u" s="T271">
                  <ts e="T272" id="Seg_980" n="HIAT:w" s="T271">Ugurduk</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_983" n="HIAT:w" s="T272">üs</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_986" n="HIAT:w" s="T273">dʼi͡ege</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_989" n="HIAT:w" s="T274">ühü͡önneriger</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_992" n="HIAT:w" s="T275">konno</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_995" n="HIAT:w" s="T276">da</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_998" n="HIAT:w" s="T277">tugu</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1001" n="HIAT:w" s="T278">daː</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1004" n="HIAT:w" s="T279">bulan</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1007" n="HIAT:w" s="T280">ɨlbata</ts>
                  <nts id="Seg_1008" n="HIAT:ip">.</nts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_1011" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_1013" n="HIAT:w" s="T281">Dʼe</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1016" n="HIAT:w" s="T282">biːrde</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1019" n="HIAT:w" s="T283">ühü͡ön</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1022" n="HIAT:w" s="T284">biːr</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1025" n="HIAT:w" s="T285">dʼi͡ege</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1028" n="HIAT:w" s="T286">munnʼustallara</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1031" n="HIAT:w" s="T287">bu͡olbut</ts>
                  <nts id="Seg_1032" n="HIAT:ip">.</nts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1035" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_1037" n="HIAT:w" s="T288">Manna</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1040" n="HIAT:w" s="T289">kiːren</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1043" n="HIAT:w" s="T290">ičeːn</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1046" n="HIAT:w" s="T291">haha</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1049" n="HIAT:w" s="T292">hɨppɨt</ts>
                  <nts id="Seg_1050" n="HIAT:ip">.</nts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_1053" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1055" n="HIAT:w" s="T293">Bu</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1058" n="HIAT:w" s="T294">hɨttagɨna</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1061" n="HIAT:w" s="T295">biːr</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1064" n="HIAT:w" s="T296">kɨːs</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1067" n="HIAT:w" s="T297">araj</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1070" n="HIAT:w" s="T298">hanaːrgaːbɨt</ts>
                  <nts id="Seg_1071" n="HIAT:ip">.</nts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1074" n="HIAT:u" s="T299">
                  <ts e="T300" id="Seg_1076" n="HIAT:w" s="T299">Manɨ</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1079" n="HIAT:w" s="T300">ikki</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1082" n="HIAT:w" s="T301">dogoro</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1085" n="HIAT:w" s="T302">ɨjɨppɨttar</ts>
                  <nts id="Seg_1086" n="HIAT:ip">:</nts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1089" n="HIAT:u" s="T303">
                  <nts id="Seg_1090" n="HIAT:ip">"</nts>
                  <ts e="T304" id="Seg_1092" n="HIAT:w" s="T303">Tu͡ok</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1095" n="HIAT:w" s="T304">bu͡olan</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1098" n="HIAT:w" s="T305">hannaːrgɨːgɨn</ts>
                  <nts id="Seg_1099" n="HIAT:ip">"</nts>
                  <nts id="Seg_1100" n="HIAT:ip">,</nts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1103" n="HIAT:w" s="T306">di͡en</ts>
                  <nts id="Seg_1104" n="HIAT:ip">.</nts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T309" id="Seg_1107" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_1109" n="HIAT:w" s="T307">Kɨːstara</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1112" n="HIAT:w" s="T308">eppit</ts>
                  <nts id="Seg_1113" n="HIAT:ip">:</nts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_1116" n="HIAT:u" s="T309">
                  <nts id="Seg_1117" n="HIAT:ip">"</nts>
                  <ts e="T310" id="Seg_1119" n="HIAT:w" s="T309">Min</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1122" n="HIAT:w" s="T310">ijem</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1125" n="HIAT:w" s="T311">biːr</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1128" n="HIAT:w" s="T312">bert</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1131" n="HIAT:w" s="T313">keri͡esteːk</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1134" n="HIAT:w" s="T314">heppin</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1137" n="HIAT:w" s="T315">bi͡erimije</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1140" n="HIAT:w" s="T316">ɨːppɨt</ts>
                  <nts id="Seg_1141" n="HIAT:ip">.</nts>
                  <nts id="Seg_1142" n="HIAT:ip">"</nts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T320" id="Seg_1145" n="HIAT:u" s="T317">
                  <ts e="T318" id="Seg_1147" n="HIAT:w" s="T317">Onu</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1150" n="HIAT:w" s="T318">dogordoro</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1153" n="HIAT:w" s="T319">eppitter</ts>
                  <nts id="Seg_1154" n="HIAT:ip">:</nts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_1157" n="HIAT:u" s="T320">
                  <nts id="Seg_1158" n="HIAT:ip">"</nts>
                  <ts e="T321" id="Seg_1160" n="HIAT:w" s="T320">Tu͡ok</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1163" n="HIAT:w" s="T321">bu͡olu͡oj</ts>
                  <nts id="Seg_1164" n="HIAT:ip">,</nts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1167" n="HIAT:w" s="T322">ijete</ts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1170" n="HIAT:w" s="T323">kellegine</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1173" n="HIAT:w" s="T324">bejete</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1176" n="HIAT:w" s="T325">egelen</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1179" n="HIAT:w" s="T326">bi͡eri͡e</ts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1182" n="HIAT:w" s="T327">bu͡ollaga</ts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1185" n="HIAT:w" s="T328">diː</ts>
                  <nts id="Seg_1186" n="HIAT:ip">.</nts>
                  <nts id="Seg_1187" n="HIAT:ip">"</nts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T337" id="Seg_1190" n="HIAT:u" s="T329">
                  <nts id="Seg_1191" n="HIAT:ip">"</nts>
                  <ts e="T330" id="Seg_1193" n="HIAT:w" s="T329">Dʼe</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1196" n="HIAT:w" s="T330">bu</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1199" n="HIAT:w" s="T331">ebit</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1202" n="HIAT:w" s="T332">bu͡ollaga</ts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1205" n="HIAT:w" s="T333">ogonnʼor</ts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1208" n="HIAT:w" s="T334">kɨːha</ts>
                  <nts id="Seg_1209" n="HIAT:ip">"</nts>
                  <nts id="Seg_1210" n="HIAT:ip">,</nts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1213" n="HIAT:w" s="T335">diː</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1216" n="HIAT:w" s="T336">hanɨːr</ts>
                  <nts id="Seg_1217" n="HIAT:ip">.</nts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_1220" n="HIAT:u" s="T337">
                  <ts e="T338" id="Seg_1222" n="HIAT:w" s="T337">Utujbutun</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1225" n="HIAT:w" s="T338">kenne</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1228" n="HIAT:w" s="T339">kuːhan</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1231" n="HIAT:w" s="T340">baran</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1234" n="HIAT:w" s="T341">dʼe</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1237" n="HIAT:w" s="T342">kötör</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1240" n="HIAT:w" s="T343">diː</ts>
                  <nts id="Seg_1241" n="HIAT:ip">.</nts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1244" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1246" n="HIAT:w" s="T344">Ol</ts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1249" n="HIAT:w" s="T345">biri͡emege</ts>
                  <nts id="Seg_1250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1252" n="HIAT:w" s="T346">tukarɨ</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1255" n="HIAT:w" s="T347">togus</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1258" n="HIAT:w" s="T348">kun</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1261" n="HIAT:w" s="T349">utuja</ts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1264" n="HIAT:w" s="T350">hɨppɨt</ts>
                  <nts id="Seg_1265" n="HIAT:ip">.</nts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1268" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1270" n="HIAT:w" s="T351">Dʼe</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1273" n="HIAT:w" s="T352">kuːhan</ts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1276" n="HIAT:w" s="T353">baran</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1279" n="HIAT:w" s="T354">ekkiri͡en</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1282" n="HIAT:w" s="T355">turar</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1285" n="HIAT:w" s="T356">diː</ts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1288" n="HIAT:w" s="T357">uhuktan</ts>
                  <nts id="Seg_1289" n="HIAT:ip">.</nts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T362" id="Seg_1292" n="HIAT:u" s="T358">
                  <ts e="T359" id="Seg_1294" n="HIAT:w" s="T358">Manna</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1297" n="HIAT:w" s="T359">hüːrenner</ts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1300" n="HIAT:w" s="T360">löčü͡ögü</ts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1303" n="HIAT:w" s="T361">egeleller</ts>
                  <nts id="Seg_1304" n="HIAT:ip">.</nts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1307" n="HIAT:u" s="T362">
                  <nts id="Seg_1308" n="HIAT:ip">"</nts>
                  <ts e="T363" id="Seg_1310" n="HIAT:w" s="T362">Uŋu͡oktarɨn</ts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1313" n="HIAT:w" s="T363">egeliŋ</ts>
                  <nts id="Seg_1314" n="HIAT:ip">"</nts>
                  <nts id="Seg_1315" n="HIAT:ip">,</nts>
                  <nts id="Seg_1316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1318" n="HIAT:w" s="T364">diː-diː</ts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1321" n="HIAT:w" s="T365">kɨːran</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1324" n="HIAT:w" s="T366">nʼühüjer</ts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1327" n="HIAT:w" s="T367">diː</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1330" n="HIAT:w" s="T368">üs</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1333" n="HIAT:w" s="T369">tüːnneːk</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1336" n="HIAT:w" s="T370">künü</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1339" n="HIAT:w" s="T371">ölüː</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1342" n="HIAT:w" s="T372">dojdututtan</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1345" n="HIAT:w" s="T373">tönnön</ts>
                  <nts id="Seg_1346" n="HIAT:ip">.</nts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T379" id="Seg_1349" n="HIAT:u" s="T374">
                  <ts e="T375" id="Seg_1351" n="HIAT:w" s="T374">Onu͡oga</ts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1354" n="HIAT:w" s="T375">taba</ts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1357" n="HIAT:w" s="T376">tiriːtiger</ts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1360" n="HIAT:w" s="T377">egeleller</ts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1363" n="HIAT:w" s="T378">oŋu͡oktarɨn</ts>
                  <nts id="Seg_1364" n="HIAT:ip">.</nts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T391" id="Seg_1367" n="HIAT:u" s="T379">
                  <ts e="T380" id="Seg_1369" n="HIAT:w" s="T379">Körbüttere</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1372" n="HIAT:w" s="T380">kɨːhɨ</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1375" n="HIAT:w" s="T381">kötögön</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1378" n="HIAT:w" s="T382">turar</ts>
                  <nts id="Seg_1379" n="HIAT:ip">,</nts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1382" n="HIAT:w" s="T383">tolʼko</ts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1385" n="HIAT:w" s="T384">kɨːstara</ts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1388" n="HIAT:w" s="T385">kamnaːbat</ts>
                  <nts id="Seg_1389" n="HIAT:ip">,</nts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1392" n="HIAT:w" s="T386">ölbüt</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1395" n="HIAT:w" s="T387">duː</ts>
                  <nts id="Seg_1396" n="HIAT:ip">,</nts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1399" n="HIAT:w" s="T388">utujbut</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1402" n="HIAT:w" s="T389">duː</ts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1405" n="HIAT:w" s="T390">kördük</ts>
                  <nts id="Seg_1406" n="HIAT:ip">.</nts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T398" id="Seg_1409" n="HIAT:u" s="T391">
                  <ts e="T392" id="Seg_1411" n="HIAT:w" s="T391">Baː</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1414" n="HIAT:w" s="T392">kɨːs</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1417" n="HIAT:w" s="T393">uŋu͡oktarɨn</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1420" n="HIAT:w" s="T394">ihiger</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1423" n="HIAT:w" s="T395">hukujduː</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1426" n="HIAT:w" s="T396">kaːlɨːr</ts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1429" n="HIAT:w" s="T397">diː</ts>
                  <nts id="Seg_1430" n="HIAT:ip">.</nts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1433" n="HIAT:u" s="T398">
                  <ts e="T399" id="Seg_1435" n="HIAT:w" s="T398">Mantɨta</ts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1438" n="HIAT:w" s="T399">tillen</ts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1441" n="HIAT:w" s="T400">kelen</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1444" n="HIAT:w" s="T401">dʼe</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1447" n="HIAT:w" s="T402">baː</ts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1450" n="HIAT:w" s="T403">ogonnʼoruŋ</ts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1453" n="HIAT:w" s="T404">kɨːha</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1456" n="HIAT:w" s="T405">bu͡olar</ts>
                  <nts id="Seg_1457" n="HIAT:ip">.</nts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T411" id="Seg_1460" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_1462" n="HIAT:w" s="T406">Manɨ</ts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1465" n="HIAT:w" s="T407">ičeːniŋ</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1468" n="HIAT:w" s="T408">dʼaktar</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1471" n="HIAT:w" s="T409">gɨnan</ts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1474" n="HIAT:w" s="T410">oloror</ts>
                  <nts id="Seg_1475" n="HIAT:ip">.</nts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_1478" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_1480" n="HIAT:w" s="T411">Kaːlaːččɨ</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1483" n="HIAT:w" s="T412">ikki</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1486" n="HIAT:w" s="T413">dogordoro</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1489" n="HIAT:w" s="T414">üŋküːge</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1492" n="HIAT:w" s="T415">ɨgɨraːrɨ</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1495" n="HIAT:w" s="T416">gɨmmɨttar</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1497" n="HIAT:ip">—</nts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1500" n="HIAT:w" s="T417">kihilere</ts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1503" n="HIAT:w" s="T418">hu͡ok</ts>
                  <nts id="Seg_1504" n="HIAT:ip">.</nts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T422" id="Seg_1507" n="HIAT:u" s="T419">
                  <ts e="T420" id="Seg_1509" n="HIAT:w" s="T419">Bilbittere</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1512" n="HIAT:w" s="T420">ojun</ts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1515" n="HIAT:w" s="T421">ilpit</ts>
                  <nts id="Seg_1516" n="HIAT:ip">.</nts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T425" id="Seg_1519" n="HIAT:u" s="T422">
                  <ts e="T423" id="Seg_1521" n="HIAT:w" s="T422">Manɨ</ts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1524" n="HIAT:w" s="T423">dʼe</ts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1527" n="HIAT:w" s="T424">kɨjakanallar</ts>
                  <nts id="Seg_1528" n="HIAT:ip">.</nts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T433" id="Seg_1531" n="HIAT:u" s="T425">
                  <ts e="T426" id="Seg_1533" n="HIAT:w" s="T425">Tilleːččigin</ts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1536" n="HIAT:w" s="T426">bulan</ts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1539" n="HIAT:w" s="T427">hanaːtɨn</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1542" n="HIAT:w" s="T428">erijen</ts>
                  <nts id="Seg_1543" n="HIAT:ip">,</nts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1546" n="HIAT:w" s="T429">ü͡ör</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1549" n="HIAT:w" s="T430">bu͡olan</ts>
                  <nts id="Seg_1550" n="HIAT:ip">,</nts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1553" n="HIAT:w" s="T431">udagan</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1556" n="HIAT:w" s="T432">gɨnallar</ts>
                  <nts id="Seg_1557" n="HIAT:ip">.</nts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1560" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_1562" n="HIAT:w" s="T433">Udagan</ts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1565" n="HIAT:w" s="T434">gɨnaːrɨ</ts>
                  <nts id="Seg_1566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1568" n="HIAT:w" s="T435">kaːjallar</ts>
                  <nts id="Seg_1569" n="HIAT:ip">:</nts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T444" id="Seg_1572" n="HIAT:u" s="T436">
                  <nts id="Seg_1573" n="HIAT:ip">"</nts>
                  <ts e="T437" id="Seg_1575" n="HIAT:w" s="T436">Bihigi</ts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1578" n="HIAT:w" s="T437">ejigin</ts>
                  <nts id="Seg_1579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1581" n="HIAT:w" s="T438">hi͡ekpit</ts>
                  <nts id="Seg_1582" n="HIAT:ip">,</nts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1585" n="HIAT:w" s="T439">emeːksin</ts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1588" n="HIAT:w" s="T440">tiriːtin</ts>
                  <nts id="Seg_1589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1591" n="HIAT:w" s="T441">hüleŋŋin</ts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1594" n="HIAT:w" s="T442">düŋür</ts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1597" n="HIAT:w" s="T443">oŋostubatakkɨna</ts>
                  <nts id="Seg_1598" n="HIAT:ip">.</nts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1601" n="HIAT:u" s="T444">
                  <ts e="T445" id="Seg_1603" n="HIAT:w" s="T444">Dʼe</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1606" n="HIAT:w" s="T445">onno</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1609" n="HIAT:w" s="T446">biːr</ts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1612" n="HIAT:w" s="T447">emeːksini</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1615" n="HIAT:w" s="T448">kastaːnnar</ts>
                  <nts id="Seg_1616" n="HIAT:ip">,</nts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1619" n="HIAT:w" s="T449">ol</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1622" n="HIAT:w" s="T450">tiriːtinen</ts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1625" n="HIAT:w" s="T451">düŋür</ts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1628" n="HIAT:w" s="T452">oŋostollor</ts>
                  <nts id="Seg_1629" n="HIAT:ip">.</nts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T462" id="Seg_1632" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_1634" n="HIAT:w" s="T453">Ol</ts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1637" n="HIAT:w" s="T454">domugar</ts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1640" n="HIAT:w" s="T455">iti</ts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1643" n="HIAT:w" s="T456">ɨ͡aldʼar</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1646" n="HIAT:w" s="T457">bu͡olluŋ</ts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1649" n="HIAT:w" s="T458">daː</ts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1652" n="HIAT:w" s="T459">haːmaj</ts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1655" n="HIAT:w" s="T460">biːrdiː</ts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1658" n="HIAT:w" s="T461">ölböt</ts>
                  <nts id="Seg_1659" n="HIAT:ip">.</nts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T465" id="Seg_1662" n="HIAT:u" s="T462">
                  <ts e="T463" id="Seg_1664" n="HIAT:w" s="T462">Haːmaj</ts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1667" n="HIAT:w" s="T463">ordannan</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1670" n="HIAT:w" s="T464">baranar</ts>
                  <nts id="Seg_1671" n="HIAT:ip">.</nts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T465" id="Seg_1673" n="sc" s="T0">
               <ts e="T1" id="Seg_1675" n="e" s="T0">Bɨlɨr </ts>
               <ts e="T2" id="Seg_1677" n="e" s="T1">biːr </ts>
               <ts e="T3" id="Seg_1679" n="e" s="T2">haːmaj </ts>
               <ts e="T4" id="Seg_1681" n="e" s="T3">ogonnʼoro </ts>
               <ts e="T5" id="Seg_1683" n="e" s="T4">olorbut, </ts>
               <ts e="T6" id="Seg_1685" n="e" s="T5">kɨːstaːk. </ts>
               <ts e="T7" id="Seg_1687" n="e" s="T6">Bu </ts>
               <ts e="T8" id="Seg_1689" n="e" s="T7">kɨːha </ts>
               <ts e="T9" id="Seg_1691" n="e" s="T8">ulaːtan </ts>
               <ts e="T10" id="Seg_1693" n="e" s="T9">kusaːjka </ts>
               <ts e="T11" id="Seg_1695" n="e" s="T10">bu͡olan </ts>
               <ts e="T12" id="Seg_1697" n="e" s="T11">baran </ts>
               <ts e="T13" id="Seg_1699" n="e" s="T12">ölön </ts>
               <ts e="T14" id="Seg_1701" n="e" s="T13">kaːlbɨt. </ts>
               <ts e="T15" id="Seg_1703" n="e" s="T14">Manɨ </ts>
               <ts e="T16" id="Seg_1705" n="e" s="T15">agata </ts>
               <ts e="T17" id="Seg_1707" n="e" s="T16">hürdeːktik </ts>
               <ts e="T18" id="Seg_1709" n="e" s="T17">ɨtɨːr. </ts>
               <ts e="T19" id="Seg_1711" n="e" s="T18">Karajaːrɨ </ts>
               <ts e="T20" id="Seg_1713" n="e" s="T19">gɨmmɨttarɨn </ts>
               <ts e="T21" id="Seg_1715" n="e" s="T20">eter: </ts>
               <ts e="T22" id="Seg_1717" n="e" s="T21">"Min </ts>
               <ts e="T23" id="Seg_1719" n="e" s="T22">kɨːspɨn </ts>
               <ts e="T24" id="Seg_1721" n="e" s="T23">bi͡ek </ts>
               <ts e="T25" id="Seg_1723" n="e" s="T24">bejebin </ts>
               <ts e="T26" id="Seg_1725" n="e" s="T25">kɨtta </ts>
               <ts e="T27" id="Seg_1727" n="e" s="T26">ti͡eje </ts>
               <ts e="T28" id="Seg_1729" n="e" s="T27">hɨldʼɨ͡am, </ts>
               <ts e="T29" id="Seg_1731" n="e" s="T28">kini </ts>
               <ts e="T30" id="Seg_1733" n="e" s="T29">tilli͡ege, </ts>
               <ts e="T31" id="Seg_1735" n="e" s="T30">ulakan </ts>
               <ts e="T32" id="Seg_1737" n="e" s="T31">ojuːŋŋa </ts>
               <ts e="T33" id="Seg_1739" n="e" s="T32">tilinneri͡em", </ts>
               <ts e="T34" id="Seg_1741" n="e" s="T33">diːr. </ts>
               <ts e="T35" id="Seg_1743" n="e" s="T34">Dʼe </ts>
               <ts e="T36" id="Seg_1745" n="e" s="T35">kɨːhɨn </ts>
               <ts e="T37" id="Seg_1747" n="e" s="T36">hɨrgaga </ts>
               <ts e="T38" id="Seg_1749" n="e" s="T37">ti͡eje </ts>
               <ts e="T39" id="Seg_1751" n="e" s="T38">hɨldʼar. </ts>
               <ts e="T40" id="Seg_1753" n="e" s="T39">Ol </ts>
               <ts e="T41" id="Seg_1755" n="e" s="T40">hɨldʼan </ts>
               <ts e="T42" id="Seg_1757" n="e" s="T41">ojunnarɨ </ts>
               <ts e="T43" id="Seg_1759" n="e" s="T42">kördöhör, </ts>
               <ts e="T44" id="Seg_1761" n="e" s="T43">biːrdere </ts>
               <ts e="T45" id="Seg_1763" n="e" s="T44">da </ts>
               <ts e="T46" id="Seg_1765" n="e" s="T45">bu͡olbat. </ts>
               <ts e="T47" id="Seg_1767" n="e" s="T46">Araj </ts>
               <ts e="T48" id="Seg_1769" n="e" s="T47">ikki </ts>
               <ts e="T49" id="Seg_1771" n="e" s="T48">ulakan </ts>
               <ts e="T50" id="Seg_1773" n="e" s="T49">ojunu </ts>
               <ts e="T51" id="Seg_1775" n="e" s="T50">kɨːrdarar </ts>
               <ts e="T52" id="Seg_1777" n="e" s="T51">ikki </ts>
               <ts e="T53" id="Seg_1779" n="e" s="T52">dʼɨla </ts>
               <ts e="T54" id="Seg_1781" n="e" s="T53">aːspɨtɨn </ts>
               <ts e="T55" id="Seg_1783" n="e" s="T54">genne. </ts>
               <ts e="T56" id="Seg_1785" n="e" s="T55">Kɨːhɨn </ts>
               <ts e="T57" id="Seg_1787" n="e" s="T56">ete </ts>
               <ts e="T58" id="Seg_1789" n="e" s="T57">dabnu͡o </ts>
               <ts e="T59" id="Seg_1791" n="e" s="T58">hɨtɨjan </ts>
               <ts e="T60" id="Seg_1793" n="e" s="T59">uŋu͡oktara </ts>
               <ts e="T61" id="Seg_1795" n="e" s="T60">ere </ts>
               <ts e="T62" id="Seg_1797" n="e" s="T61">kaːlbɨt. </ts>
               <ts e="T63" id="Seg_1799" n="e" s="T62">Bu </ts>
               <ts e="T64" id="Seg_1801" n="e" s="T63">ikki </ts>
               <ts e="T65" id="Seg_1803" n="e" s="T64">ojun </ts>
               <ts e="T66" id="Seg_1805" n="e" s="T65">kɨːrannar </ts>
               <ts e="T67" id="Seg_1807" n="e" s="T66">tugu </ts>
               <ts e="T68" id="Seg_1809" n="e" s="T67">da </ts>
               <ts e="T69" id="Seg_1811" n="e" s="T68">gɨmmattar. </ts>
               <ts e="T70" id="Seg_1813" n="e" s="T69">Ühüs </ts>
               <ts e="T71" id="Seg_1815" n="e" s="T70">hɨla </ts>
               <ts e="T72" id="Seg_1817" n="e" s="T71">bu͡olar. </ts>
               <ts e="T73" id="Seg_1819" n="e" s="T72">Ol </ts>
               <ts e="T74" id="Seg_1821" n="e" s="T73">haːmaj </ts>
               <ts e="T75" id="Seg_1823" n="e" s="T74">ɨksatɨgar </ts>
               <ts e="T76" id="Seg_1825" n="e" s="T75">ičeːn </ts>
               <ts e="T77" id="Seg_1827" n="e" s="T76">baːr </ts>
               <ts e="T78" id="Seg_1829" n="e" s="T77">ebit. </ts>
               <ts e="T79" id="Seg_1831" n="e" s="T78">Bu </ts>
               <ts e="T80" id="Seg_1833" n="e" s="T79">ičeːn </ts>
               <ts e="T81" id="Seg_1835" n="e" s="T80">berke </ts>
               <ts e="T82" id="Seg_1837" n="e" s="T81">ahɨnar </ts>
               <ts e="T83" id="Seg_1839" n="e" s="T82">haːmajɨn. </ts>
               <ts e="T84" id="Seg_1841" n="e" s="T83">Biːrde </ts>
               <ts e="T85" id="Seg_1843" n="e" s="T84">tühüːr. </ts>
               <ts e="T86" id="Seg_1845" n="e" s="T85">Ol </ts>
               <ts e="T87" id="Seg_1847" n="e" s="T86">tühü͡ön </ts>
               <ts e="T88" id="Seg_1849" n="e" s="T87">baran </ts>
               <ts e="T89" id="Seg_1851" n="e" s="T88">haːmajɨgar </ts>
               <ts e="T90" id="Seg_1853" n="e" s="T89">kelen </ts>
               <ts e="T91" id="Seg_1855" n="e" s="T90">kepsiːr: </ts>
               <ts e="T92" id="Seg_1857" n="e" s="T91">"Bu </ts>
               <ts e="T93" id="Seg_1859" n="e" s="T92">en </ts>
               <ts e="T94" id="Seg_1861" n="e" s="T93">kɨːhɨŋ </ts>
               <ts e="T95" id="Seg_1863" n="e" s="T94">tilli͡ek </ts>
               <ts e="T96" id="Seg_1865" n="e" s="T95">bɨhɨːlaːk, </ts>
               <ts e="T97" id="Seg_1867" n="e" s="T96">mu͡okaha </ts>
               <ts e="T98" id="Seg_1869" n="e" s="T97">biːr </ts>
               <ts e="T99" id="Seg_1871" n="e" s="T98">ete. </ts>
               <ts e="T100" id="Seg_1873" n="e" s="T99">Ogoŋ </ts>
               <ts e="T101" id="Seg_1875" n="e" s="T100">kutun </ts>
               <ts e="T102" id="Seg_1877" n="e" s="T101">hi͡ebit </ts>
               <ts e="T103" id="Seg_1879" n="e" s="T102">ölüːte </ts>
               <ts e="T104" id="Seg_1881" n="e" s="T103">ebege </ts>
               <ts e="T105" id="Seg_1883" n="e" s="T104">ölüː </ts>
               <ts e="T106" id="Seg_1885" n="e" s="T105">uːtugar </ts>
               <ts e="T107" id="Seg_1887" n="e" s="T106">ildʼe </ts>
               <ts e="T108" id="Seg_1889" n="e" s="T107">hɨldʼar </ts>
               <ts e="T109" id="Seg_1891" n="e" s="T108">ebit. </ts>
               <ts e="T110" id="Seg_1893" n="e" s="T109">Biːrges </ts>
               <ts e="T111" id="Seg_1895" n="e" s="T110">ojunuŋ </ts>
               <ts e="T112" id="Seg_1897" n="e" s="T111">kɨːl </ts>
               <ts e="T113" id="Seg_1899" n="e" s="T112">bu͡olan </ts>
               <ts e="T114" id="Seg_1901" n="e" s="T113">hirinen </ts>
               <ts e="T115" id="Seg_1903" n="e" s="T114">batan, </ts>
               <ts e="T116" id="Seg_1905" n="e" s="T115">tüːte </ts>
               <ts e="T117" id="Seg_1907" n="e" s="T116">kɨraːrɨ </ts>
               <ts e="T118" id="Seg_1909" n="e" s="T117">kajagaska </ts>
               <ts e="T119" id="Seg_1911" n="e" s="T118">bappat </ts>
               <ts e="T120" id="Seg_1913" n="e" s="T119">bu͡olar, </ts>
               <ts e="T121" id="Seg_1915" n="e" s="T120">biːrges </ts>
               <ts e="T122" id="Seg_1917" n="e" s="T121">kɨːl </ts>
               <ts e="T123" id="Seg_1919" n="e" s="T122">bu͡olan </ts>
               <ts e="T124" id="Seg_1921" n="e" s="T123">uːnnan </ts>
               <ts e="T125" id="Seg_1923" n="e" s="T124">ustan </ts>
               <ts e="T126" id="Seg_1925" n="e" s="T125">tijdegine </ts>
               <ts e="T127" id="Seg_1927" n="e" s="T126">dolguna, </ts>
               <ts e="T128" id="Seg_1929" n="e" s="T127">kabaːrɨ </ts>
               <ts e="T129" id="Seg_1931" n="e" s="T128">ere </ts>
               <ts e="T130" id="Seg_1933" n="e" s="T129">gɨnnagɨna, </ts>
               <ts e="T131" id="Seg_1935" n="e" s="T130">kutun </ts>
               <ts e="T132" id="Seg_1937" n="e" s="T131">ildʼen </ts>
               <ts e="T133" id="Seg_1939" n="e" s="T132">keːher. </ts>
               <ts e="T134" id="Seg_1941" n="e" s="T133">Togus </ts>
               <ts e="T135" id="Seg_1943" n="e" s="T134">hɨl </ts>
               <ts e="T136" id="Seg_1945" n="e" s="T135">ogurduk </ts>
               <ts e="T137" id="Seg_1947" n="e" s="T136">batɨhɨ͡aktara, </ts>
               <ts e="T138" id="Seg_1949" n="e" s="T137">onton </ts>
               <ts e="T139" id="Seg_1951" n="e" s="T138">kajtak </ts>
               <ts e="T140" id="Seg_1953" n="e" s="T139">bu͡olar." </ts>
               <ts e="T141" id="Seg_1955" n="e" s="T140">Onu͡oga </ts>
               <ts e="T142" id="Seg_1957" n="e" s="T141">haːmaj </ts>
               <ts e="T143" id="Seg_1959" n="e" s="T142">ogonnʼoro </ts>
               <ts e="T144" id="Seg_1961" n="e" s="T143">eter: </ts>
               <ts e="T145" id="Seg_1963" n="e" s="T144">"Ol </ts>
               <ts e="T146" id="Seg_1965" n="e" s="T145">ojun </ts>
               <ts e="T147" id="Seg_1967" n="e" s="T146">kɨ͡ajbatɨn </ts>
               <ts e="T148" id="Seg_1969" n="e" s="T147">biler </ts>
               <ts e="T149" id="Seg_1971" n="e" s="T148">kihi </ts>
               <ts e="T150" id="Seg_1973" n="e" s="T149">bejeŋ </ts>
               <ts e="T151" id="Seg_1975" n="e" s="T150">togo </ts>
               <ts e="T152" id="Seg_1977" n="e" s="T151">tilinneren </ts>
               <ts e="T153" id="Seg_1979" n="e" s="T152">körbökkün", </ts>
               <ts e="T154" id="Seg_1981" n="e" s="T153">diːr, </ts>
               <ts e="T155" id="Seg_1983" n="e" s="T154">aːttahar. </ts>
               <ts e="T156" id="Seg_1985" n="e" s="T155">"Dʼe </ts>
               <ts e="T157" id="Seg_1987" n="e" s="T156">berke </ts>
               <ts e="T158" id="Seg_1989" n="e" s="T157">muŋnannɨŋ", </ts>
               <ts e="T159" id="Seg_1991" n="e" s="T158">diːr. </ts>
               <ts e="T160" id="Seg_1993" n="e" s="T159">"Kajtak </ts>
               <ts e="T161" id="Seg_1995" n="e" s="T160">gɨnabɨn, </ts>
               <ts e="T162" id="Seg_1997" n="e" s="T161">tilinnegine </ts>
               <ts e="T163" id="Seg_1999" n="e" s="T162">mi͡eke </ts>
               <ts e="T164" id="Seg_2001" n="e" s="T163">dʼaktar </ts>
               <ts e="T165" id="Seg_2003" n="e" s="T164">bi͡eri͡eŋ </ts>
               <ts e="T166" id="Seg_2005" n="e" s="T165">duː?" </ts>
               <ts e="T167" id="Seg_2007" n="e" s="T166">"Baːjɨm </ts>
               <ts e="T168" id="Seg_2009" n="e" s="T167">aŋarɨn </ts>
               <ts e="T169" id="Seg_2011" n="e" s="T168">bi͡eri͡em, </ts>
               <ts e="T170" id="Seg_2013" n="e" s="T169">bejetin </ts>
               <ts e="T171" id="Seg_2015" n="e" s="T170">bi͡eri͡em, </ts>
               <ts e="T172" id="Seg_2017" n="e" s="T171">tilinner </ts>
               <ts e="T173" id="Seg_2019" n="e" s="T172">ere", </ts>
               <ts e="T174" id="Seg_2021" n="e" s="T173">diːr. </ts>
               <ts e="T175" id="Seg_2023" n="e" s="T174">Ičeːne </ts>
               <ts e="T176" id="Seg_2025" n="e" s="T175">utujan </ts>
               <ts e="T177" id="Seg_2027" n="e" s="T176">kaːlar. </ts>
               <ts e="T178" id="Seg_2029" n="e" s="T177">Bu </ts>
               <ts e="T179" id="Seg_2031" n="e" s="T178">utuja </ts>
               <ts e="T180" id="Seg_2033" n="e" s="T179">hɨtan, </ts>
               <ts e="T181" id="Seg_2035" n="e" s="T180">dʼe </ts>
               <ts e="T182" id="Seg_2037" n="e" s="T181">ol </ts>
               <ts e="T183" id="Seg_2039" n="e" s="T182">ölüːtün </ts>
               <ts e="T184" id="Seg_2041" n="e" s="T183">hiriger </ts>
               <ts e="T185" id="Seg_2043" n="e" s="T184">tijer. </ts>
               <ts e="T186" id="Seg_2045" n="e" s="T185">Dʼe </ts>
               <ts e="T187" id="Seg_2047" n="e" s="T186">ol </ts>
               <ts e="T188" id="Seg_2049" n="e" s="T187">ölüːtün </ts>
               <ts e="T189" id="Seg_2051" n="e" s="T188">hiriger </ts>
               <ts e="T190" id="Seg_2053" n="e" s="T189">tijer. </ts>
               <ts e="T191" id="Seg_2055" n="e" s="T190">Tijbite </ts>
               <ts e="T192" id="Seg_2057" n="e" s="T191">elʼbek </ts>
               <ts e="T193" id="Seg_2059" n="e" s="T192">bagajɨ </ts>
               <ts e="T194" id="Seg_2061" n="e" s="T193">dʼon </ts>
               <ts e="T195" id="Seg_2063" n="e" s="T194">üŋküːlüː </ts>
               <ts e="T196" id="Seg_2065" n="e" s="T195">turallar. </ts>
               <ts e="T197" id="Seg_2067" n="e" s="T196">Manna </ts>
               <ts e="T198" id="Seg_2069" n="e" s="T197">kördüː </ts>
               <ts e="T199" id="Seg_2071" n="e" s="T198">hatɨːr </ts>
               <ts e="T200" id="Seg_2073" n="e" s="T199">kɨːstarɨn </ts>
               <ts e="T201" id="Seg_2075" n="e" s="T200">kutun — </ts>
               <ts e="T202" id="Seg_2077" n="e" s="T201">tu͡ok </ts>
               <ts e="T203" id="Seg_2079" n="e" s="T202">keli͡ej </ts>
               <ts e="T204" id="Seg_2081" n="e" s="T203">vovse </ts>
               <ts e="T205" id="Seg_2083" n="e" s="T204">bulbat. </ts>
               <ts e="T206" id="Seg_2085" n="e" s="T205">"Onton </ts>
               <ts e="T207" id="Seg_2087" n="e" s="T206">bu </ts>
               <ts e="T208" id="Seg_2089" n="e" s="T207">hirten </ts>
               <ts e="T209" id="Seg_2091" n="e" s="T208">atɨn </ts>
               <ts e="T210" id="Seg_2093" n="e" s="T209">mi͡estege </ts>
               <ts e="T211" id="Seg_2095" n="e" s="T210">barɨːm", </ts>
               <ts e="T212" id="Seg_2097" n="e" s="T211">diːr. </ts>
               <ts e="T213" id="Seg_2099" n="e" s="T212">Tijer </ts>
               <ts e="T214" id="Seg_2101" n="e" s="T213">biːr </ts>
               <ts e="T215" id="Seg_2103" n="e" s="T214">hirge. </ts>
               <ts e="T216" id="Seg_2105" n="e" s="T215">Onno </ts>
               <ts e="T217" id="Seg_2107" n="e" s="T216">üs </ts>
               <ts e="T218" id="Seg_2109" n="e" s="T217">tebis </ts>
               <ts e="T219" id="Seg_2111" n="e" s="T218">teŋ </ts>
               <ts e="T220" id="Seg_2113" n="e" s="T219">hɨrajdaːk, </ts>
               <ts e="T221" id="Seg_2115" n="e" s="T220">taŋastaːk </ts>
               <ts e="T222" id="Seg_2117" n="e" s="T221">kɨːs </ts>
               <ts e="T223" id="Seg_2119" n="e" s="T222">baːr </ts>
               <ts e="T224" id="Seg_2121" n="e" s="T223">ebit. </ts>
               <ts e="T225" id="Seg_2123" n="e" s="T224">Mantan </ts>
               <ts e="T226" id="Seg_2125" n="e" s="T225">biːrgestere </ts>
               <ts e="T227" id="Seg_2127" n="e" s="T226">ogonnʼor </ts>
               <ts e="T228" id="Seg_2129" n="e" s="T227">kɨːha. </ts>
               <ts e="T229" id="Seg_2131" n="e" s="T228">Kajalara </ts>
               <ts e="T230" id="Seg_2133" n="e" s="T229">ol </ts>
               <ts e="T231" id="Seg_2135" n="e" s="T230">kɨːhɨn </ts>
               <ts e="T232" id="Seg_2137" n="e" s="T231">kɨ͡ajan </ts>
               <ts e="T233" id="Seg_2139" n="e" s="T232">bilbet. </ts>
               <ts e="T234" id="Seg_2141" n="e" s="T233">Taŋastarɨn </ts>
               <ts e="T235" id="Seg_2143" n="e" s="T234">hiːgin </ts>
               <ts e="T236" id="Seg_2145" n="e" s="T235">aːgan </ts>
               <ts e="T237" id="Seg_2147" n="e" s="T236">körör — </ts>
               <ts e="T238" id="Seg_2149" n="e" s="T237">aksaːna </ts>
               <ts e="T239" id="Seg_2151" n="e" s="T238">barɨta </ts>
               <ts e="T240" id="Seg_2153" n="e" s="T239">biːr. </ts>
               <ts e="T241" id="Seg_2155" n="e" s="T240">Muŋnana </ts>
               <ts e="T242" id="Seg_2157" n="e" s="T241">hatɨːr. </ts>
               <ts e="T243" id="Seg_2159" n="e" s="T242">Bu </ts>
               <ts e="T244" id="Seg_2161" n="e" s="T243">kɨrgɨttar </ts>
               <ts e="T245" id="Seg_2163" n="e" s="T244">üs </ts>
               <ts e="T246" id="Seg_2165" n="e" s="T245">dʼi͡eleːkter </ts>
               <ts e="T247" id="Seg_2167" n="e" s="T246">tus </ts>
               <ts e="T248" id="Seg_2169" n="e" s="T247">tuspa. </ts>
               <ts e="T249" id="Seg_2171" n="e" s="T248">Onno </ts>
               <ts e="T250" id="Seg_2173" n="e" s="T249">ki͡ehe </ts>
               <ts e="T251" id="Seg_2175" n="e" s="T250">üŋküːtten </ts>
               <ts e="T252" id="Seg_2177" n="e" s="T251">kelen </ts>
               <ts e="T253" id="Seg_2179" n="e" s="T252">utujan </ts>
               <ts e="T254" id="Seg_2181" n="e" s="T253">kaːlallar. </ts>
               <ts e="T255" id="Seg_2183" n="e" s="T254">Kɨrgɨttarɨn </ts>
               <ts e="T256" id="Seg_2185" n="e" s="T255">kɨtta </ts>
               <ts e="T257" id="Seg_2187" n="e" s="T256">kepseteːri </ts>
               <ts e="T258" id="Seg_2189" n="e" s="T257">gɨnar, </ts>
               <ts e="T259" id="Seg_2191" n="e" s="T258">biːrdestere </ts>
               <ts e="T260" id="Seg_2193" n="e" s="T259">da </ts>
               <ts e="T261" id="Seg_2195" n="e" s="T260">haŋarbat. </ts>
               <ts e="T262" id="Seg_2197" n="e" s="T261">Kajdak </ts>
               <ts e="T263" id="Seg_2199" n="e" s="T262">bu͡olu͡omuj </ts>
               <ts e="T264" id="Seg_2201" n="e" s="T263">di͡en </ts>
               <ts e="T265" id="Seg_2203" n="e" s="T264">hananar. </ts>
               <ts e="T266" id="Seg_2205" n="e" s="T265">Birgesterin </ts>
               <ts e="T267" id="Seg_2207" n="e" s="T266">dʼi͡etiger </ts>
               <ts e="T268" id="Seg_2209" n="e" s="T267">hahan </ts>
               <ts e="T269" id="Seg_2211" n="e" s="T268">kiːren </ts>
               <ts e="T270" id="Seg_2213" n="e" s="T269">utujtun </ts>
               <ts e="T271" id="Seg_2215" n="e" s="T270">ketiːr. </ts>
               <ts e="T272" id="Seg_2217" n="e" s="T271">Ugurduk </ts>
               <ts e="T273" id="Seg_2219" n="e" s="T272">üs </ts>
               <ts e="T274" id="Seg_2221" n="e" s="T273">dʼi͡ege </ts>
               <ts e="T275" id="Seg_2223" n="e" s="T274">ühü͡önneriger </ts>
               <ts e="T276" id="Seg_2225" n="e" s="T275">konno </ts>
               <ts e="T277" id="Seg_2227" n="e" s="T276">da </ts>
               <ts e="T278" id="Seg_2229" n="e" s="T277">tugu </ts>
               <ts e="T279" id="Seg_2231" n="e" s="T278">daː </ts>
               <ts e="T280" id="Seg_2233" n="e" s="T279">bulan </ts>
               <ts e="T281" id="Seg_2235" n="e" s="T280">ɨlbata. </ts>
               <ts e="T282" id="Seg_2237" n="e" s="T281">Dʼe </ts>
               <ts e="T283" id="Seg_2239" n="e" s="T282">biːrde </ts>
               <ts e="T284" id="Seg_2241" n="e" s="T283">ühü͡ön </ts>
               <ts e="T285" id="Seg_2243" n="e" s="T284">biːr </ts>
               <ts e="T286" id="Seg_2245" n="e" s="T285">dʼi͡ege </ts>
               <ts e="T287" id="Seg_2247" n="e" s="T286">munnʼustallara </ts>
               <ts e="T288" id="Seg_2249" n="e" s="T287">bu͡olbut. </ts>
               <ts e="T289" id="Seg_2251" n="e" s="T288">Manna </ts>
               <ts e="T290" id="Seg_2253" n="e" s="T289">kiːren </ts>
               <ts e="T291" id="Seg_2255" n="e" s="T290">ičeːn </ts>
               <ts e="T292" id="Seg_2257" n="e" s="T291">haha </ts>
               <ts e="T293" id="Seg_2259" n="e" s="T292">hɨppɨt. </ts>
               <ts e="T294" id="Seg_2261" n="e" s="T293">Bu </ts>
               <ts e="T295" id="Seg_2263" n="e" s="T294">hɨttagɨna </ts>
               <ts e="T296" id="Seg_2265" n="e" s="T295">biːr </ts>
               <ts e="T297" id="Seg_2267" n="e" s="T296">kɨːs </ts>
               <ts e="T298" id="Seg_2269" n="e" s="T297">araj </ts>
               <ts e="T299" id="Seg_2271" n="e" s="T298">hanaːrgaːbɨt. </ts>
               <ts e="T300" id="Seg_2273" n="e" s="T299">Manɨ </ts>
               <ts e="T301" id="Seg_2275" n="e" s="T300">ikki </ts>
               <ts e="T302" id="Seg_2277" n="e" s="T301">dogoro </ts>
               <ts e="T303" id="Seg_2279" n="e" s="T302">ɨjɨppɨttar: </ts>
               <ts e="T304" id="Seg_2281" n="e" s="T303">"Tu͡ok </ts>
               <ts e="T305" id="Seg_2283" n="e" s="T304">bu͡olan </ts>
               <ts e="T306" id="Seg_2285" n="e" s="T305">hannaːrgɨːgɨn", </ts>
               <ts e="T307" id="Seg_2287" n="e" s="T306">di͡en. </ts>
               <ts e="T308" id="Seg_2289" n="e" s="T307">Kɨːstara </ts>
               <ts e="T309" id="Seg_2291" n="e" s="T308">eppit: </ts>
               <ts e="T310" id="Seg_2293" n="e" s="T309">"Min </ts>
               <ts e="T311" id="Seg_2295" n="e" s="T310">ijem </ts>
               <ts e="T312" id="Seg_2297" n="e" s="T311">biːr </ts>
               <ts e="T313" id="Seg_2299" n="e" s="T312">bert </ts>
               <ts e="T314" id="Seg_2301" n="e" s="T313">keri͡esteːk </ts>
               <ts e="T315" id="Seg_2303" n="e" s="T314">heppin </ts>
               <ts e="T316" id="Seg_2305" n="e" s="T315">bi͡erimije </ts>
               <ts e="T317" id="Seg_2307" n="e" s="T316">ɨːppɨt." </ts>
               <ts e="T318" id="Seg_2309" n="e" s="T317">Onu </ts>
               <ts e="T319" id="Seg_2311" n="e" s="T318">dogordoro </ts>
               <ts e="T320" id="Seg_2313" n="e" s="T319">eppitter: </ts>
               <ts e="T321" id="Seg_2315" n="e" s="T320">"Tu͡ok </ts>
               <ts e="T322" id="Seg_2317" n="e" s="T321">bu͡olu͡oj, </ts>
               <ts e="T323" id="Seg_2319" n="e" s="T322">ijete </ts>
               <ts e="T324" id="Seg_2321" n="e" s="T323">kellegine </ts>
               <ts e="T325" id="Seg_2323" n="e" s="T324">bejete </ts>
               <ts e="T326" id="Seg_2325" n="e" s="T325">egelen </ts>
               <ts e="T327" id="Seg_2327" n="e" s="T326">bi͡eri͡e </ts>
               <ts e="T328" id="Seg_2329" n="e" s="T327">bu͡ollaga </ts>
               <ts e="T329" id="Seg_2331" n="e" s="T328">diː." </ts>
               <ts e="T330" id="Seg_2333" n="e" s="T329">"Dʼe </ts>
               <ts e="T331" id="Seg_2335" n="e" s="T330">bu </ts>
               <ts e="T332" id="Seg_2337" n="e" s="T331">ebit </ts>
               <ts e="T333" id="Seg_2339" n="e" s="T332">bu͡ollaga </ts>
               <ts e="T334" id="Seg_2341" n="e" s="T333">ogonnʼor </ts>
               <ts e="T335" id="Seg_2343" n="e" s="T334">kɨːha", </ts>
               <ts e="T336" id="Seg_2345" n="e" s="T335">diː </ts>
               <ts e="T337" id="Seg_2347" n="e" s="T336">hanɨːr. </ts>
               <ts e="T338" id="Seg_2349" n="e" s="T337">Utujbutun </ts>
               <ts e="T339" id="Seg_2351" n="e" s="T338">kenne </ts>
               <ts e="T340" id="Seg_2353" n="e" s="T339">kuːhan </ts>
               <ts e="T341" id="Seg_2355" n="e" s="T340">baran </ts>
               <ts e="T342" id="Seg_2357" n="e" s="T341">dʼe </ts>
               <ts e="T343" id="Seg_2359" n="e" s="T342">kötör </ts>
               <ts e="T344" id="Seg_2361" n="e" s="T343">diː. </ts>
               <ts e="T345" id="Seg_2363" n="e" s="T344">Ol </ts>
               <ts e="T346" id="Seg_2365" n="e" s="T345">biri͡emege </ts>
               <ts e="T347" id="Seg_2367" n="e" s="T346">tukarɨ </ts>
               <ts e="T348" id="Seg_2369" n="e" s="T347">togus </ts>
               <ts e="T349" id="Seg_2371" n="e" s="T348">kun </ts>
               <ts e="T350" id="Seg_2373" n="e" s="T349">utuja </ts>
               <ts e="T351" id="Seg_2375" n="e" s="T350">hɨppɨt. </ts>
               <ts e="T352" id="Seg_2377" n="e" s="T351">Dʼe </ts>
               <ts e="T353" id="Seg_2379" n="e" s="T352">kuːhan </ts>
               <ts e="T354" id="Seg_2381" n="e" s="T353">baran </ts>
               <ts e="T355" id="Seg_2383" n="e" s="T354">ekkiri͡en </ts>
               <ts e="T356" id="Seg_2385" n="e" s="T355">turar </ts>
               <ts e="T357" id="Seg_2387" n="e" s="T356">diː </ts>
               <ts e="T358" id="Seg_2389" n="e" s="T357">uhuktan. </ts>
               <ts e="T359" id="Seg_2391" n="e" s="T358">Manna </ts>
               <ts e="T360" id="Seg_2393" n="e" s="T359">hüːrenner </ts>
               <ts e="T361" id="Seg_2395" n="e" s="T360">löčü͡ögü </ts>
               <ts e="T362" id="Seg_2397" n="e" s="T361">egeleller. </ts>
               <ts e="T363" id="Seg_2399" n="e" s="T362">"Uŋu͡oktarɨn </ts>
               <ts e="T364" id="Seg_2401" n="e" s="T363">egeliŋ", </ts>
               <ts e="T365" id="Seg_2403" n="e" s="T364">diː-diː </ts>
               <ts e="T366" id="Seg_2405" n="e" s="T365">kɨːran </ts>
               <ts e="T367" id="Seg_2407" n="e" s="T366">nʼühüjer </ts>
               <ts e="T368" id="Seg_2409" n="e" s="T367">diː </ts>
               <ts e="T369" id="Seg_2411" n="e" s="T368">üs </ts>
               <ts e="T370" id="Seg_2413" n="e" s="T369">tüːnneːk </ts>
               <ts e="T371" id="Seg_2415" n="e" s="T370">künü </ts>
               <ts e="T372" id="Seg_2417" n="e" s="T371">ölüː </ts>
               <ts e="T373" id="Seg_2419" n="e" s="T372">dojdututtan </ts>
               <ts e="T374" id="Seg_2421" n="e" s="T373">tönnön. </ts>
               <ts e="T375" id="Seg_2423" n="e" s="T374">Onu͡oga </ts>
               <ts e="T376" id="Seg_2425" n="e" s="T375">taba </ts>
               <ts e="T377" id="Seg_2427" n="e" s="T376">tiriːtiger </ts>
               <ts e="T378" id="Seg_2429" n="e" s="T377">egeleller </ts>
               <ts e="T379" id="Seg_2431" n="e" s="T378">oŋu͡oktarɨn. </ts>
               <ts e="T380" id="Seg_2433" n="e" s="T379">Körbüttere </ts>
               <ts e="T381" id="Seg_2435" n="e" s="T380">kɨːhɨ </ts>
               <ts e="T382" id="Seg_2437" n="e" s="T381">kötögön </ts>
               <ts e="T383" id="Seg_2439" n="e" s="T382">turar, </ts>
               <ts e="T384" id="Seg_2441" n="e" s="T383">tolʼko </ts>
               <ts e="T385" id="Seg_2443" n="e" s="T384">kɨːstara </ts>
               <ts e="T386" id="Seg_2445" n="e" s="T385">kamnaːbat, </ts>
               <ts e="T387" id="Seg_2447" n="e" s="T386">ölbüt </ts>
               <ts e="T388" id="Seg_2449" n="e" s="T387">duː, </ts>
               <ts e="T389" id="Seg_2451" n="e" s="T388">utujbut </ts>
               <ts e="T390" id="Seg_2453" n="e" s="T389">duː </ts>
               <ts e="T391" id="Seg_2455" n="e" s="T390">kördük. </ts>
               <ts e="T392" id="Seg_2457" n="e" s="T391">Baː </ts>
               <ts e="T393" id="Seg_2459" n="e" s="T392">kɨːs </ts>
               <ts e="T394" id="Seg_2461" n="e" s="T393">uŋu͡oktarɨn </ts>
               <ts e="T395" id="Seg_2463" n="e" s="T394">ihiger </ts>
               <ts e="T396" id="Seg_2465" n="e" s="T395">hukujduː </ts>
               <ts e="T397" id="Seg_2467" n="e" s="T396">kaːlɨːr </ts>
               <ts e="T398" id="Seg_2469" n="e" s="T397">diː. </ts>
               <ts e="T399" id="Seg_2471" n="e" s="T398">Mantɨta </ts>
               <ts e="T400" id="Seg_2473" n="e" s="T399">tillen </ts>
               <ts e="T401" id="Seg_2475" n="e" s="T400">kelen </ts>
               <ts e="T402" id="Seg_2477" n="e" s="T401">dʼe </ts>
               <ts e="T403" id="Seg_2479" n="e" s="T402">baː </ts>
               <ts e="T404" id="Seg_2481" n="e" s="T403">ogonnʼoruŋ </ts>
               <ts e="T405" id="Seg_2483" n="e" s="T404">kɨːha </ts>
               <ts e="T406" id="Seg_2485" n="e" s="T405">bu͡olar. </ts>
               <ts e="T407" id="Seg_2487" n="e" s="T406">Manɨ </ts>
               <ts e="T408" id="Seg_2489" n="e" s="T407">ičeːniŋ </ts>
               <ts e="T409" id="Seg_2491" n="e" s="T408">dʼaktar </ts>
               <ts e="T410" id="Seg_2493" n="e" s="T409">gɨnan </ts>
               <ts e="T411" id="Seg_2495" n="e" s="T410">oloror. </ts>
               <ts e="T412" id="Seg_2497" n="e" s="T411">Kaːlaːččɨ </ts>
               <ts e="T413" id="Seg_2499" n="e" s="T412">ikki </ts>
               <ts e="T414" id="Seg_2501" n="e" s="T413">dogordoro </ts>
               <ts e="T415" id="Seg_2503" n="e" s="T414">üŋküːge </ts>
               <ts e="T416" id="Seg_2505" n="e" s="T415">ɨgɨraːrɨ </ts>
               <ts e="T417" id="Seg_2507" n="e" s="T416">gɨmmɨttar — </ts>
               <ts e="T418" id="Seg_2509" n="e" s="T417">kihilere </ts>
               <ts e="T419" id="Seg_2511" n="e" s="T418">hu͡ok. </ts>
               <ts e="T420" id="Seg_2513" n="e" s="T419">Bilbittere </ts>
               <ts e="T421" id="Seg_2515" n="e" s="T420">ojun </ts>
               <ts e="T422" id="Seg_2517" n="e" s="T421">ilpit. </ts>
               <ts e="T423" id="Seg_2519" n="e" s="T422">Manɨ </ts>
               <ts e="T424" id="Seg_2521" n="e" s="T423">dʼe </ts>
               <ts e="T425" id="Seg_2523" n="e" s="T424">kɨjakanallar. </ts>
               <ts e="T426" id="Seg_2525" n="e" s="T425">Tilleːččigin </ts>
               <ts e="T427" id="Seg_2527" n="e" s="T426">bulan </ts>
               <ts e="T428" id="Seg_2529" n="e" s="T427">hanaːtɨn </ts>
               <ts e="T429" id="Seg_2531" n="e" s="T428">erijen, </ts>
               <ts e="T430" id="Seg_2533" n="e" s="T429">ü͡ör </ts>
               <ts e="T431" id="Seg_2535" n="e" s="T430">bu͡olan, </ts>
               <ts e="T432" id="Seg_2537" n="e" s="T431">udagan </ts>
               <ts e="T433" id="Seg_2539" n="e" s="T432">gɨnallar. </ts>
               <ts e="T434" id="Seg_2541" n="e" s="T433">Udagan </ts>
               <ts e="T435" id="Seg_2543" n="e" s="T434">gɨnaːrɨ </ts>
               <ts e="T436" id="Seg_2545" n="e" s="T435">kaːjallar: </ts>
               <ts e="T437" id="Seg_2547" n="e" s="T436">"Bihigi </ts>
               <ts e="T438" id="Seg_2549" n="e" s="T437">ejigin </ts>
               <ts e="T439" id="Seg_2551" n="e" s="T438">hi͡ekpit, </ts>
               <ts e="T440" id="Seg_2553" n="e" s="T439">emeːksin </ts>
               <ts e="T441" id="Seg_2555" n="e" s="T440">tiriːtin </ts>
               <ts e="T442" id="Seg_2557" n="e" s="T441">hüleŋŋin </ts>
               <ts e="T443" id="Seg_2559" n="e" s="T442">düŋür </ts>
               <ts e="T444" id="Seg_2561" n="e" s="T443">oŋostubatakkɨna. </ts>
               <ts e="T445" id="Seg_2563" n="e" s="T444">Dʼe </ts>
               <ts e="T446" id="Seg_2565" n="e" s="T445">onno </ts>
               <ts e="T447" id="Seg_2567" n="e" s="T446">biːr </ts>
               <ts e="T448" id="Seg_2569" n="e" s="T447">emeːksini </ts>
               <ts e="T449" id="Seg_2571" n="e" s="T448">kastaːnnar, </ts>
               <ts e="T450" id="Seg_2573" n="e" s="T449">ol </ts>
               <ts e="T451" id="Seg_2575" n="e" s="T450">tiriːtinen </ts>
               <ts e="T452" id="Seg_2577" n="e" s="T451">düŋür </ts>
               <ts e="T453" id="Seg_2579" n="e" s="T452">oŋostollor. </ts>
               <ts e="T454" id="Seg_2581" n="e" s="T453">Ol </ts>
               <ts e="T455" id="Seg_2583" n="e" s="T454">domugar </ts>
               <ts e="T456" id="Seg_2585" n="e" s="T455">iti </ts>
               <ts e="T457" id="Seg_2587" n="e" s="T456">ɨ͡aldʼar </ts>
               <ts e="T458" id="Seg_2589" n="e" s="T457">bu͡olluŋ </ts>
               <ts e="T459" id="Seg_2591" n="e" s="T458">daː </ts>
               <ts e="T460" id="Seg_2593" n="e" s="T459">haːmaj </ts>
               <ts e="T461" id="Seg_2595" n="e" s="T460">biːrdiː </ts>
               <ts e="T462" id="Seg_2597" n="e" s="T461">ölböt. </ts>
               <ts e="T463" id="Seg_2599" n="e" s="T462">Haːmaj </ts>
               <ts e="T464" id="Seg_2601" n="e" s="T463">ordannan </ts>
               <ts e="T465" id="Seg_2603" n="e" s="T464">baranar. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_2604" s="T0">BaRD_1930_DaughterOfNganasan_flk.001 (001.001)</ta>
            <ta e="T14" id="Seg_2605" s="T6">BaRD_1930_DaughterOfNganasan_flk.002 (001.002)</ta>
            <ta e="T18" id="Seg_2606" s="T14">BaRD_1930_DaughterOfNganasan_flk.003 (001.003)</ta>
            <ta e="T21" id="Seg_2607" s="T18">BaRD_1930_DaughterOfNganasan_flk.004 (001.004)</ta>
            <ta e="T34" id="Seg_2608" s="T21">BaRD_1930_DaughterOfNganasan_flk.005 (001.005)</ta>
            <ta e="T39" id="Seg_2609" s="T34">BaRD_1930_DaughterOfNganasan_flk.006 (001.006)</ta>
            <ta e="T46" id="Seg_2610" s="T39">BaRD_1930_DaughterOfNganasan_flk.007 (001.007)</ta>
            <ta e="T55" id="Seg_2611" s="T46">BaRD_1930_DaughterOfNganasan_flk.008 (001.008)</ta>
            <ta e="T62" id="Seg_2612" s="T55">BaRD_1930_DaughterOfNganasan_flk.009 (001.009)</ta>
            <ta e="T69" id="Seg_2613" s="T62">BaRD_1930_DaughterOfNganasan_flk.010 (001.010)</ta>
            <ta e="T72" id="Seg_2614" s="T69">BaRD_1930_DaughterOfNganasan_flk.011 (001.011)</ta>
            <ta e="T78" id="Seg_2615" s="T72">BaRD_1930_DaughterOfNganasan_flk.012 (001.012)</ta>
            <ta e="T83" id="Seg_2616" s="T78">BaRD_1930_DaughterOfNganasan_flk.013 (001.013)</ta>
            <ta e="T85" id="Seg_2617" s="T83">BaRD_1930_DaughterOfNganasan_flk.014 (001.014)</ta>
            <ta e="T91" id="Seg_2618" s="T85">BaRD_1930_DaughterOfNganasan_flk.015 (001.015)</ta>
            <ta e="T99" id="Seg_2619" s="T91">BaRD_1930_DaughterOfNganasan_flk.016 (001.016)</ta>
            <ta e="T109" id="Seg_2620" s="T99">BaRD_1930_DaughterOfNganasan_flk.017 (001.017)</ta>
            <ta e="T133" id="Seg_2621" s="T109">BaRD_1930_DaughterOfNganasan_flk.018 (001.018)</ta>
            <ta e="T140" id="Seg_2622" s="T133">BaRD_1930_DaughterOfNganasan_flk.019 (001.019)</ta>
            <ta e="T144" id="Seg_2623" s="T140">BaRD_1930_DaughterOfNganasan_flk.020 (001.020)</ta>
            <ta e="T155" id="Seg_2624" s="T144">BaRD_1930_DaughterOfNganasan_flk.021 (001.021)</ta>
            <ta e="T159" id="Seg_2625" s="T155">BaRD_1930_DaughterOfNganasan_flk.022 (001.022)</ta>
            <ta e="T166" id="Seg_2626" s="T159">BaRD_1930_DaughterOfNganasan_flk.023 (001.023)</ta>
            <ta e="T174" id="Seg_2627" s="T166">BaRD_1930_DaughterOfNganasan_flk.024 (001.024)</ta>
            <ta e="T177" id="Seg_2628" s="T174">BaRD_1930_DaughterOfNganasan_flk.025 (001.025)</ta>
            <ta e="T185" id="Seg_2629" s="T177">BaRD_1930_DaughterOfNganasan_flk.026 (001.026)</ta>
            <ta e="T190" id="Seg_2630" s="T185">BaRD_1930_DaughterOfNganasan_flk.027 (001.027)</ta>
            <ta e="T196" id="Seg_2631" s="T190">BaRD_1930_DaughterOfNganasan_flk.028 (001.028)</ta>
            <ta e="T205" id="Seg_2632" s="T196">BaRD_1930_DaughterOfNganasan_flk.029 (001.029)</ta>
            <ta e="T212" id="Seg_2633" s="T205">BaRD_1930_DaughterOfNganasan_flk.030 (001.030)</ta>
            <ta e="T215" id="Seg_2634" s="T212">BaRD_1930_DaughterOfNganasan_flk.031 (001.031)</ta>
            <ta e="T224" id="Seg_2635" s="T215">BaRD_1930_DaughterOfNganasan_flk.032 (001.032)</ta>
            <ta e="T228" id="Seg_2636" s="T224">BaRD_1930_DaughterOfNganasan_flk.033 (001.033)</ta>
            <ta e="T233" id="Seg_2637" s="T228">BaRD_1930_DaughterOfNganasan_flk.034 (001.034)</ta>
            <ta e="T240" id="Seg_2638" s="T233">BaRD_1930_DaughterOfNganasan_flk.035 (001.035)</ta>
            <ta e="T242" id="Seg_2639" s="T240">BaRD_1930_DaughterOfNganasan_flk.036 (001.036)</ta>
            <ta e="T248" id="Seg_2640" s="T242">BaRD_1930_DaughterOfNganasan_flk.037 (001.037)</ta>
            <ta e="T254" id="Seg_2641" s="T248">BaRD_1930_DaughterOfNganasan_flk.038 (001.038)</ta>
            <ta e="T261" id="Seg_2642" s="T254">BaRD_1930_DaughterOfNganasan_flk.039 (001.039)</ta>
            <ta e="T265" id="Seg_2643" s="T261">BaRD_1930_DaughterOfNganasan_flk.040 (001.040)</ta>
            <ta e="T271" id="Seg_2644" s="T265">BaRD_1930_DaughterOfNganasan_flk.041 (001.041)</ta>
            <ta e="T281" id="Seg_2645" s="T271">BaRD_1930_DaughterOfNganasan_flk.042 (001.042)</ta>
            <ta e="T288" id="Seg_2646" s="T281">BaRD_1930_DaughterOfNganasan_flk.043 (001.043)</ta>
            <ta e="T293" id="Seg_2647" s="T288">BaRD_1930_DaughterOfNganasan_flk.044 (001.044)</ta>
            <ta e="T299" id="Seg_2648" s="T293">BaRD_1930_DaughterOfNganasan_flk.045 (001.045)</ta>
            <ta e="T303" id="Seg_2649" s="T299">BaRD_1930_DaughterOfNganasan_flk.046 (001.046)</ta>
            <ta e="T307" id="Seg_2650" s="T303">BaRD_1930_DaughterOfNganasan_flk.047 (001.047)</ta>
            <ta e="T309" id="Seg_2651" s="T307">BaRD_1930_DaughterOfNganasan_flk.048 (001.049)</ta>
            <ta e="T317" id="Seg_2652" s="T309">BaRD_1930_DaughterOfNganasan_flk.049 (001.050)</ta>
            <ta e="T320" id="Seg_2653" s="T317">BaRD_1930_DaughterOfNganasan_flk.050 (001.051)</ta>
            <ta e="T329" id="Seg_2654" s="T320">BaRD_1930_DaughterOfNganasan_flk.051 (001.052)</ta>
            <ta e="T337" id="Seg_2655" s="T329">BaRD_1930_DaughterOfNganasan_flk.052 (001.053)</ta>
            <ta e="T344" id="Seg_2656" s="T337">BaRD_1930_DaughterOfNganasan_flk.053 (001.054)</ta>
            <ta e="T351" id="Seg_2657" s="T344">BaRD_1930_DaughterOfNganasan_flk.054 (001.055)</ta>
            <ta e="T358" id="Seg_2658" s="T351">BaRD_1930_DaughterOfNganasan_flk.055 (001.056)</ta>
            <ta e="T362" id="Seg_2659" s="T358">BaRD_1930_DaughterOfNganasan_flk.056 (001.057)</ta>
            <ta e="T374" id="Seg_2660" s="T362">BaRD_1930_DaughterOfNganasan_flk.057 (001.058)</ta>
            <ta e="T379" id="Seg_2661" s="T374">BaRD_1930_DaughterOfNganasan_flk.058 (001.059)</ta>
            <ta e="T391" id="Seg_2662" s="T379">BaRD_1930_DaughterOfNganasan_flk.059 (001.060)</ta>
            <ta e="T398" id="Seg_2663" s="T391">BaRD_1930_DaughterOfNganasan_flk.060 (001.061)</ta>
            <ta e="T406" id="Seg_2664" s="T398">BaRD_1930_DaughterOfNganasan_flk.061 (001.062)</ta>
            <ta e="T411" id="Seg_2665" s="T406">BaRD_1930_DaughterOfNganasan_flk.062 (001.063)</ta>
            <ta e="T419" id="Seg_2666" s="T411">BaRD_1930_DaughterOfNganasan_flk.063 (001.064)</ta>
            <ta e="T422" id="Seg_2667" s="T419">BaRD_1930_DaughterOfNganasan_flk.064 (001.065)</ta>
            <ta e="T425" id="Seg_2668" s="T422">BaRD_1930_DaughterOfNganasan_flk.065 (001.066)</ta>
            <ta e="T433" id="Seg_2669" s="T425">BaRD_1930_DaughterOfNganasan_flk.066 (001.067)</ta>
            <ta e="T436" id="Seg_2670" s="T433">BaRD_1930_DaughterOfNganasan_flk.067 (001.068)</ta>
            <ta e="T444" id="Seg_2671" s="T436">BaRD_1930_DaughterOfNganasan_flk.068 (001.069)</ta>
            <ta e="T453" id="Seg_2672" s="T444">BaRD_1930_DaughterOfNganasan_flk.069 (001.070)</ta>
            <ta e="T462" id="Seg_2673" s="T453">BaRD_1930_DaughterOfNganasan_flk.070 (001.071)</ta>
            <ta e="T465" id="Seg_2674" s="T462">BaRD_1930_DaughterOfNganasan_flk.071 (001.072)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_2675" s="T0">Былыр биир һаамай огонньоро олорбут, кыыстаак.</ta>
            <ta e="T14" id="Seg_2676" s="T6">Бу кыыһа улаатан кусаайка буолан баран өлөн каалбыт.</ta>
            <ta e="T18" id="Seg_2677" s="T14">Маны агата һүрдээктик ытыыр.</ta>
            <ta e="T21" id="Seg_2678" s="T18">Карайаары гыммыттарын этэр:</ta>
            <ta e="T34" id="Seg_2679" s="T21">— Мин кыыспын биэк бэйэбин кытта тиэйэ һылдьыам, кини тиллиэгэ, улакан ойууҥҥа тилиннэриэм, — диир.</ta>
            <ta e="T39" id="Seg_2680" s="T34">Дьэ кыыһын һыргага тиэйэ һылдьар.</ta>
            <ta e="T46" id="Seg_2681" s="T39">Ол һылдьан ойуннары көрдөһөр, биирдэрэ да буолбат.</ta>
            <ta e="T55" id="Seg_2682" s="T46">Арай икки улакан ойуну кыырдарар икки дьыла ааспытын гэннэ.</ta>
            <ta e="T62" id="Seg_2683" s="T55">Кыыһын этэ дабнуо һытыйан уҥуоктара эрэ каалбыт.</ta>
            <ta e="T69" id="Seg_2684" s="T62">Бу икки ойун кыыраннар тугу да гымматтар.</ta>
            <ta e="T72" id="Seg_2685" s="T69">Үһүс һыла буолар.</ta>
            <ta e="T78" id="Seg_2686" s="T72">Ол һаамай ыксатыгар ичээн баар эбит.</ta>
            <ta e="T83" id="Seg_2687" s="T78">Бу ичээн бэркэ аһынар һаамайын.</ta>
            <ta e="T85" id="Seg_2688" s="T83">Биирдэ түһүүр.</ta>
            <ta e="T91" id="Seg_2689" s="T85">Ол түһүөн баран һаамайыгар кэлэн кэпсиир:</ta>
            <ta e="T99" id="Seg_2690" s="T91">— Бу эн кыыһыҥ тиллиэк быһыылаак, муокаһа биир этэ.</ta>
            <ta e="T109" id="Seg_2691" s="T99">Огоҥ кутун һиэбит өлүүтэ эбэгэ өлүү уутугар илдьэ һылдьар эбит.</ta>
            <ta e="T133" id="Seg_2692" s="T109">Бииргэс ойунуҥ кыыл буолан һиринэн батан, түүтэ кыраары кайагаска баппат буолар, бииргэс кыыл буолан ууннан устан тийдэгинэ долгуна, кабаары эрэ гыннагына, кутун илдьэн кээһэр.</ta>
            <ta e="T140" id="Seg_2693" s="T133">Тогус һыл огурдук батыһыактара, онтон кайтак буолар.</ta>
            <ta e="T144" id="Seg_2694" s="T140">Онуога һаамай огонньоро этэр:</ta>
            <ta e="T155" id="Seg_2695" s="T144">— Ол ойун кыайбатын билэр киһи бэйэҥ того тилиннэрэн көрбөккүн, — диир, ааттаһар.</ta>
            <ta e="T159" id="Seg_2696" s="T155">— Дьэ бэркэ муҥнанныҥ, — диир.</ta>
            <ta e="T166" id="Seg_2697" s="T159">— Кайтак гынабын, тилиннэгинэ миэкэ дьактар биэриэҥ дуу?</ta>
            <ta e="T174" id="Seg_2698" s="T166">— Баайым аҥарын биэриэм, бэйэтин биэриэм, тилиннэр эрэ, — диир.</ta>
            <ta e="T177" id="Seg_2699" s="T174">Ичээнэ утуйан каалар.</ta>
            <ta e="T185" id="Seg_2700" s="T177">Бу утуйа һытан, дьэ ол өлүүтүн һиригэр тийэр.</ta>
            <ta e="T190" id="Seg_2701" s="T185">Дьэ ол өлүүтүн һиригэр тийэр.</ta>
            <ta e="T196" id="Seg_2702" s="T190">Тийбитэ эльбэк багайы дьон үҥкүүлүү тураллар.</ta>
            <ta e="T205" id="Seg_2703" s="T196">Манна көрдүү һатыыр кыыстарын кутун — туок кэлиэй вовсе булбат.</ta>
            <ta e="T212" id="Seg_2704" s="T205">Онтон бу һиртэн атын миэстэгэ барыым — диир.</ta>
            <ta e="T215" id="Seg_2705" s="T212">Тийэр биир һиргэ.</ta>
            <ta e="T224" id="Seg_2706" s="T215">Онно үс тэбис тэҥ һырайдаак, таҥастаак кыыс баар эбит.</ta>
            <ta e="T228" id="Seg_2707" s="T224">Мантан бииргэстэрэ огонньор кыыһа.</ta>
            <ta e="T233" id="Seg_2708" s="T228">Кайалара ол кыыһын кыайан билбэт.</ta>
            <ta e="T240" id="Seg_2709" s="T233">Таҥастарын һиигин ааган көрөр — аксаана барыта биир.</ta>
            <ta e="T242" id="Seg_2710" s="T240">Муҥнана һатыыр.</ta>
            <ta e="T248" id="Seg_2711" s="T242">Бу кыргыттар үс дьиэлээктэр тус туспа.</ta>
            <ta e="T254" id="Seg_2712" s="T248">Онно киэһэ үҥкүүттэн кэлэн утуйан каалаллар.</ta>
            <ta e="T261" id="Seg_2713" s="T254">Кыргыттарын кытта кэпсэтээри гынар, биирдэстэрэ да һаҥарбат.</ta>
            <ta e="T265" id="Seg_2714" s="T261">Кайдак буолуомуй диэн һананар.</ta>
            <ta e="T271" id="Seg_2715" s="T265">Биргэстэрин дьиэтигэр һаһан киирэн утуйтун кэтиир.</ta>
            <ta e="T281" id="Seg_2716" s="T271">Угурдук үс дьиэгэ үһүөннэригэр конно да тугу даа булан ылбата.</ta>
            <ta e="T288" id="Seg_2717" s="T281">Дьэ биирдэ үһүөн биир дьиэгэ мунньусталлара буолбут.</ta>
            <ta e="T293" id="Seg_2718" s="T288">Манна киирэн ичээн һаһа һыппыт.</ta>
            <ta e="T299" id="Seg_2719" s="T293">Бу һыттагына биир кыыс арай һанааргаабыт.</ta>
            <ta e="T303" id="Seg_2720" s="T299">Маны икки догоро ыйыппыттар:</ta>
            <ta e="T307" id="Seg_2721" s="T303">— Туок буолан һаннааргыыгын? — диэн.</ta>
            <ta e="T309" id="Seg_2722" s="T307">Кыыстара эппит:</ta>
            <ta e="T317" id="Seg_2723" s="T309">— Мин ийэм биир бэрт кэриэстээк һэппин биэримийэ ыыппыт.</ta>
            <ta e="T320" id="Seg_2724" s="T317">Ону догордоро эппиттэр:</ta>
            <ta e="T329" id="Seg_2725" s="T320">— Туок буолуой, ийэтэ кэллэгинэ бэйэтэ эгэлэн биэриэ буоллага дии.</ta>
            <ta e="T337" id="Seg_2726" s="T329">— Дьэ бу эбит буоллага огонньор кыыһа, — дии һаныыр.</ta>
            <ta e="T344" id="Seg_2727" s="T337">Утуйбутун кэннэ кууһан баран дьэ көтөр дии.</ta>
            <ta e="T351" id="Seg_2728" s="T344">Ол бириэмэгэ тукары тогус кун утуйа һыппыт.</ta>
            <ta e="T358" id="Seg_2729" s="T351">Дьэ кууһан баран эккириэн турар дии уһуктан.</ta>
            <ta e="T362" id="Seg_2730" s="T358">Манна һүүрэннэр лөчүөгү эгэлэллэр.</ta>
            <ta e="T374" id="Seg_2731" s="T362">— Уҥуоктарын эгэлиҥ, — дии-дии кыыран ньүһүйэр дии үс түүннээк күнү өлүү дойдутуттан төннөн.</ta>
            <ta e="T379" id="Seg_2732" s="T374">Онуога таба тириитигэр эгэлэллэр оҥуоктарын.</ta>
            <ta e="T391" id="Seg_2733" s="T379">Көрбүттэрэ кыыһы көтөгөн турар, только кыыстара камнаабат, өлбүт дуу, утуйбут дуу көрдүк.</ta>
            <ta e="T398" id="Seg_2734" s="T391">Баа кыыс уҥуоктарын иһигэр һукуйдуу каалыыр дии.</ta>
            <ta e="T406" id="Seg_2735" s="T398">Мантыта тиллэн кэлэн дьэ баа огонньоруҥ кыыһа буолар.</ta>
            <ta e="T411" id="Seg_2736" s="T406">Маны ичээниҥ дьактар гынан олорор.</ta>
            <ta e="T419" id="Seg_2737" s="T411">Каалааччы икки догордоро үҥкүүгэ ыгыраары гыммыттар — киһилэрэ һуок.</ta>
            <ta e="T422" id="Seg_2738" s="T419">Билбиттэрэ ойун илпит.</ta>
            <ta e="T425" id="Seg_2739" s="T422">Маны дьэ кыйаканаллар.</ta>
            <ta e="T433" id="Seg_2740" s="T425">Тиллээччигин булан һанаатын эрийэн, үөр буолан, удаган гыналлар.</ta>
            <ta e="T436" id="Seg_2741" s="T433">Удаган гынаары каайаллар:</ta>
            <ta e="T444" id="Seg_2742" s="T436">— Биһиги эйигин һиэкпит, эмээксин тириитин һүлэҥҥин дүҥүр оҥостубатаккына.</ta>
            <ta e="T453" id="Seg_2743" s="T444">Дьэ онно биир эмээксини кастааннар, ол тириитинэн дүҥүр оҥостоллор.</ta>
            <ta e="T462" id="Seg_2744" s="T453">Ол домугар ити ыалдьар буоллуҥ даа һаамай биирдии өлбөт.</ta>
            <ta e="T465" id="Seg_2745" s="T462">Һаамай орданнан баранар.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_2746" s="T0">Bɨlɨr biːr haːmaj ogonnʼoro olorbut, kɨːstaːk. </ta>
            <ta e="T14" id="Seg_2747" s="T6">Bu kɨːha ulaːtan kusaːjka bu͡olan baran ölön kaːlbɨt. </ta>
            <ta e="T18" id="Seg_2748" s="T14">Manɨ agata hürdeːktik ɨtɨːr. </ta>
            <ta e="T21" id="Seg_2749" s="T18">Karajaːrɨ gɨmmɨttarɨn eter: </ta>
            <ta e="T34" id="Seg_2750" s="T21">"Min kɨːspɨn bi͡ek bejebin kɨtta ti͡eje hɨldʼɨ͡am, kini tilli͡ege, ulakan ojuːŋŋa tilinneri͡em", diːr. </ta>
            <ta e="T39" id="Seg_2751" s="T34">Dʼe kɨːhɨn hɨrgaga ti͡eje hɨldʼar. </ta>
            <ta e="T46" id="Seg_2752" s="T39">Ol hɨldʼan ojunnarɨ kördöhör, biːrdere da bu͡olbat. </ta>
            <ta e="T55" id="Seg_2753" s="T46">Araj ikki ulakan ojunu kɨːrdarar ikki dʼɨla aːspɨtɨn genne. </ta>
            <ta e="T62" id="Seg_2754" s="T55">Kɨːhɨn ete dabnu͡o hɨtɨjan uŋu͡oktara ere kaːlbɨt. </ta>
            <ta e="T69" id="Seg_2755" s="T62">Bu ikki ojun kɨːrannar tugu da gɨmmattar. </ta>
            <ta e="T72" id="Seg_2756" s="T69">Ühüs hɨla bu͡olar. </ta>
            <ta e="T78" id="Seg_2757" s="T72">Ol haːmaj ɨksatɨgar ičeːn baːr ebit. </ta>
            <ta e="T83" id="Seg_2758" s="T78">Bu ičeːn berke ahɨnar haːmajɨn. </ta>
            <ta e="T85" id="Seg_2759" s="T83">Biːrde tühüːr. </ta>
            <ta e="T91" id="Seg_2760" s="T85">Ol tühü͡ön baran haːmajɨgar kelen kepsiːr:</ta>
            <ta e="T99" id="Seg_2761" s="T91">"Bu en kɨːhɨŋ tilli͡ek bɨhɨːlaːk, mu͡okaha biːr ete. </ta>
            <ta e="T109" id="Seg_2762" s="T99">Ogoŋ kutun hi͡ebit ölüːte ebege ölüː uːtugar ildʼe hɨldʼar ebit. </ta>
            <ta e="T133" id="Seg_2763" s="T109">Biːrges ojunuŋ kɨːl bu͡olan hirinen batan, tüːte kɨraːrɨ kajagaska bappat bu͡olar, biːrges kɨːl bu͡olan uːnnan ustan tijdegine dolguna, kabaːrɨ ere gɨnnagɨna, kutun ildʼen keːher. </ta>
            <ta e="T140" id="Seg_2764" s="T133">Togus hɨl ogurduk batɨhɨ͡aktara, onton kajtak bu͡olar." </ta>
            <ta e="T144" id="Seg_2765" s="T140">Onu͡oga haːmaj ogonnʼoro eter:</ta>
            <ta e="T155" id="Seg_2766" s="T144">"Ol ojun kɨ͡ajbatɨn biler kihi bejeŋ togo tilinneren körbökkün", diːr, aːttahar. </ta>
            <ta e="T159" id="Seg_2767" s="T155">"Dʼe berke muŋnannɨŋ", diːr. </ta>
            <ta e="T166" id="Seg_2768" s="T159">"Kajtak gɨnabɨn, tilinnegine mi͡eke dʼaktar bi͡eri͡eŋ duː?"</ta>
            <ta e="T174" id="Seg_2769" s="T166">"Baːjɨm aŋarɨn bi͡eri͡em, bejetin bi͡eri͡em, tilinner ere", diːr. </ta>
            <ta e="T177" id="Seg_2770" s="T174">Ičeːne utujan kaːlar. </ta>
            <ta e="T185" id="Seg_2771" s="T177">Bu utuja hɨtan, dʼe ol ölüːtün hiriger tijer. </ta>
            <ta e="T190" id="Seg_2772" s="T185">Dʼe ol ölüːtün hiriger tijer. </ta>
            <ta e="T196" id="Seg_2773" s="T190">Tijbite elʼbek bagajɨ dʼon üŋküːlüː turallar. </ta>
            <ta e="T205" id="Seg_2774" s="T196">Manna kördüː hatɨːr kɨːstarɨn kutun — tu͡ok keli͡ej vovse bulbat. </ta>
            <ta e="T212" id="Seg_2775" s="T205">"Onton bu hirten atɨn mi͡estege barɨːm", diːr. </ta>
            <ta e="T215" id="Seg_2776" s="T212">Tijer biːr hirge. </ta>
            <ta e="T224" id="Seg_2777" s="T215">Onno üs tebis teŋ hɨrajdaːk, taŋastaːk kɨːs baːr ebit. </ta>
            <ta e="T228" id="Seg_2778" s="T224">Mantan biːrgestere ogonnʼor kɨːha. </ta>
            <ta e="T233" id="Seg_2779" s="T228">Kajalara ol kɨːhɨn kɨ͡ajan bilbet. </ta>
            <ta e="T240" id="Seg_2780" s="T233">Taŋastarɨn hiːgin aːgan körör — aksaːna barɨta biːr. </ta>
            <ta e="T242" id="Seg_2781" s="T240">Muŋnana hatɨːr. </ta>
            <ta e="T248" id="Seg_2782" s="T242">Bu kɨrgɨttar üs dʼi͡eleːkter tus tuspa. </ta>
            <ta e="T254" id="Seg_2783" s="T248">Onno ki͡ehe üŋküːtten kelen utujan kaːlallar. </ta>
            <ta e="T261" id="Seg_2784" s="T254">Kɨrgɨttarɨn kɨtta kepseteːri gɨnar, biːrdestere da haŋarbat. </ta>
            <ta e="T265" id="Seg_2785" s="T261">Kajdak bu͡olu͡omuj di͡en hananar. </ta>
            <ta e="T271" id="Seg_2786" s="T265">Birgesterin dʼi͡etiger hahan kiːren utujtun ketiːr. </ta>
            <ta e="T281" id="Seg_2787" s="T271">Ugurduk üs dʼi͡ege ühü͡önneriger konno da tugu daː bulan ɨlbata. </ta>
            <ta e="T288" id="Seg_2788" s="T281">Dʼe biːrde ühü͡ön biːr dʼi͡ege munnʼustallara bu͡olbut. </ta>
            <ta e="T293" id="Seg_2789" s="T288">Manna kiːren ičeːn haha hɨppɨt. </ta>
            <ta e="T299" id="Seg_2790" s="T293">Bu hɨttagɨna biːr kɨːs araj hanaːrgaːbɨt. </ta>
            <ta e="T303" id="Seg_2791" s="T299">Manɨ ikki dogoro ɨjɨppɨttar:</ta>
            <ta e="T307" id="Seg_2792" s="T303">"Tu͡ok bu͡olan hannaːrgɨːgɨn", di͡en. </ta>
            <ta e="T309" id="Seg_2793" s="T307">Kɨːstara eppit:</ta>
            <ta e="T317" id="Seg_2794" s="T309">"Min ijem biːr bert keri͡esteːk heppin bi͡erimije ɨːppɨt." </ta>
            <ta e="T320" id="Seg_2795" s="T317">Onu dogordoro eppitter:</ta>
            <ta e="T329" id="Seg_2796" s="T320">"Tu͡ok bu͡olu͡oj, ijete kellegine bejete egelen bi͡eri͡e bu͡ollaga diː." </ta>
            <ta e="T337" id="Seg_2797" s="T329">"Dʼe bu ebit bu͡ollaga ogonnʼor kɨːha", diː hanɨːr. </ta>
            <ta e="T344" id="Seg_2798" s="T337">Utujbutun kenne kuːhan baran dʼe kötör diː. </ta>
            <ta e="T351" id="Seg_2799" s="T344">Ol biri͡emege tukarɨ togus kun utuja hɨppɨt. </ta>
            <ta e="T358" id="Seg_2800" s="T351">Dʼe kuːhan baran ekkiri͡en turar diː uhuktan. </ta>
            <ta e="T362" id="Seg_2801" s="T358">Manna hüːrenner löčü͡ögü egeleller. </ta>
            <ta e="T374" id="Seg_2802" s="T362">"Uŋu͡oktarɨn egeliŋ", diː-diː kɨːran nʼühüjer diː üs tüːnneːk künü ölüː dojdututtan tönnön. </ta>
            <ta e="T379" id="Seg_2803" s="T374">Onu͡oga taba tiriːtiger egeleller oŋu͡oktarɨn. </ta>
            <ta e="T391" id="Seg_2804" s="T379">Körbüttere kɨːhɨ kötögön turar, tolʼko kɨːstara kamnaːbat, ölbüt duː, utujbut duː kördük. </ta>
            <ta e="T398" id="Seg_2805" s="T391">Baː kɨːs uŋu͡oktarɨn ihiger hukujduː kaːlɨːr diː. </ta>
            <ta e="T406" id="Seg_2806" s="T398">Mantɨta tillen kelen dʼe baː ogonnʼoruŋ kɨːha bu͡olar. </ta>
            <ta e="T411" id="Seg_2807" s="T406">Manɨ ičeːniŋ dʼaktar gɨnan oloror. </ta>
            <ta e="T419" id="Seg_2808" s="T411">Kaːlaːččɨ ikki dogordoro üŋküːge ɨgɨraːrɨ gɨmmɨttar — kihilere hu͡ok. </ta>
            <ta e="T422" id="Seg_2809" s="T419">Bilbittere ojun ilpit. </ta>
            <ta e="T425" id="Seg_2810" s="T422">Manɨ dʼe kɨjakanallar. </ta>
            <ta e="T433" id="Seg_2811" s="T425">Tilleːččigin bulan hanaːtɨn erijen, ü͡ör bu͡olan, udagan gɨnallar. </ta>
            <ta e="T436" id="Seg_2812" s="T433">Udagan gɨnaːrɨ kaːjallar:</ta>
            <ta e="T444" id="Seg_2813" s="T436">"Bihigi ejigin hi͡ekpit, emeːksin tiriːtin hüleŋŋin düŋür oŋostubatakkɨna." </ta>
            <ta e="T453" id="Seg_2814" s="T444">Dʼe onno biːr emeːksini kastaːnnar, ol tiriːtinen düŋür oŋostollor. </ta>
            <ta e="T462" id="Seg_2815" s="T453">Ol domugar iti ɨ͡aldʼar bu͡olluŋ daː haːmaj biːrdiː ölböt. </ta>
            <ta e="T465" id="Seg_2816" s="T462">Haːmaj ordannan baranar. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2817" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_2818" s="T1">biːr</ta>
            <ta e="T3" id="Seg_2819" s="T2">haːmaj</ta>
            <ta e="T4" id="Seg_2820" s="T3">ogonnʼor-o</ta>
            <ta e="T5" id="Seg_2821" s="T4">olor-but</ta>
            <ta e="T6" id="Seg_2822" s="T5">kɨːs-taːk</ta>
            <ta e="T7" id="Seg_2823" s="T6">bu</ta>
            <ta e="T8" id="Seg_2824" s="T7">kɨːh-a</ta>
            <ta e="T9" id="Seg_2825" s="T8">ulaːt-an</ta>
            <ta e="T10" id="Seg_2826" s="T9">kusaːjka</ta>
            <ta e="T11" id="Seg_2827" s="T10">bu͡ol-an</ta>
            <ta e="T12" id="Seg_2828" s="T11">baran</ta>
            <ta e="T13" id="Seg_2829" s="T12">öl-ön</ta>
            <ta e="T14" id="Seg_2830" s="T13">kaːl-bɨt</ta>
            <ta e="T15" id="Seg_2831" s="T14">ma-nɨ</ta>
            <ta e="T16" id="Seg_2832" s="T15">aga-ta</ta>
            <ta e="T17" id="Seg_2833" s="T16">hürdeːk-tik</ta>
            <ta e="T18" id="Seg_2834" s="T17">ɨtɨː-r</ta>
            <ta e="T19" id="Seg_2835" s="T18">karaj-aːrɨ</ta>
            <ta e="T20" id="Seg_2836" s="T19">gɨm-mɨt-tarɨ-n</ta>
            <ta e="T21" id="Seg_2837" s="T20">et-er</ta>
            <ta e="T22" id="Seg_2838" s="T21">min</ta>
            <ta e="T23" id="Seg_2839" s="T22">kɨːs-pɨ-n</ta>
            <ta e="T24" id="Seg_2840" s="T23">bi͡ek</ta>
            <ta e="T25" id="Seg_2841" s="T24">beje-bi-n</ta>
            <ta e="T26" id="Seg_2842" s="T25">kɨtta</ta>
            <ta e="T27" id="Seg_2843" s="T26">ti͡ej-e</ta>
            <ta e="T28" id="Seg_2844" s="T27">hɨldʼ-ɨ͡a-m</ta>
            <ta e="T29" id="Seg_2845" s="T28">kini</ta>
            <ta e="T30" id="Seg_2846" s="T29">till-i͡eg-e</ta>
            <ta e="T31" id="Seg_2847" s="T30">ulakan</ta>
            <ta e="T32" id="Seg_2848" s="T31">ojuːŋ-ŋa</ta>
            <ta e="T33" id="Seg_2849" s="T32">tilin-ner-i͡e-m</ta>
            <ta e="T34" id="Seg_2850" s="T33">diː-r</ta>
            <ta e="T35" id="Seg_2851" s="T34">dʼe</ta>
            <ta e="T36" id="Seg_2852" s="T35">kɨːh-ɨ-n</ta>
            <ta e="T37" id="Seg_2853" s="T36">hɨrga-ga</ta>
            <ta e="T38" id="Seg_2854" s="T37">ti͡ej-e</ta>
            <ta e="T39" id="Seg_2855" s="T38">hɨldʼ-ar</ta>
            <ta e="T40" id="Seg_2856" s="T39">ol</ta>
            <ta e="T41" id="Seg_2857" s="T40">hɨldʼ-an</ta>
            <ta e="T42" id="Seg_2858" s="T41">ojun-nar-ɨ</ta>
            <ta e="T43" id="Seg_2859" s="T42">körd-ö-h-ör</ta>
            <ta e="T44" id="Seg_2860" s="T43">biːr-dere</ta>
            <ta e="T45" id="Seg_2861" s="T44">da</ta>
            <ta e="T46" id="Seg_2862" s="T45">bu͡ol-bat</ta>
            <ta e="T47" id="Seg_2863" s="T46">araj</ta>
            <ta e="T48" id="Seg_2864" s="T47">ikki</ta>
            <ta e="T49" id="Seg_2865" s="T48">ulakan</ta>
            <ta e="T50" id="Seg_2866" s="T49">ojun-u</ta>
            <ta e="T51" id="Seg_2867" s="T50">kɨːr-dar-ar</ta>
            <ta e="T52" id="Seg_2868" s="T51">ikki</ta>
            <ta e="T53" id="Seg_2869" s="T52">dʼɨl-a</ta>
            <ta e="T54" id="Seg_2870" s="T53">aːs-pɨt-ɨ-n</ta>
            <ta e="T55" id="Seg_2871" s="T54">genne</ta>
            <ta e="T56" id="Seg_2872" s="T55">kɨːh-ɨ-n</ta>
            <ta e="T57" id="Seg_2873" s="T56">et-e</ta>
            <ta e="T58" id="Seg_2874" s="T57">dabnu͡o</ta>
            <ta e="T59" id="Seg_2875" s="T58">hɨtɨj-an</ta>
            <ta e="T60" id="Seg_2876" s="T59">uŋu͡ok-tara</ta>
            <ta e="T61" id="Seg_2877" s="T60">ere</ta>
            <ta e="T62" id="Seg_2878" s="T61">kaːl-bɨt</ta>
            <ta e="T63" id="Seg_2879" s="T62">bu</ta>
            <ta e="T64" id="Seg_2880" s="T63">ikki</ta>
            <ta e="T65" id="Seg_2881" s="T64">ojun</ta>
            <ta e="T66" id="Seg_2882" s="T65">kɨːr-an-nar</ta>
            <ta e="T67" id="Seg_2883" s="T66">tug-u</ta>
            <ta e="T68" id="Seg_2884" s="T67">da</ta>
            <ta e="T69" id="Seg_2885" s="T68">gɨm-mat-tar</ta>
            <ta e="T70" id="Seg_2886" s="T69">üh-üs</ta>
            <ta e="T71" id="Seg_2887" s="T70">hɨl-a</ta>
            <ta e="T72" id="Seg_2888" s="T71">bu͡ol-ar</ta>
            <ta e="T73" id="Seg_2889" s="T72">ol</ta>
            <ta e="T74" id="Seg_2890" s="T73">haːmaj</ta>
            <ta e="T75" id="Seg_2891" s="T74">ɨksa-tɨ-gar</ta>
            <ta e="T76" id="Seg_2892" s="T75">ičeːn</ta>
            <ta e="T77" id="Seg_2893" s="T76">baːr</ta>
            <ta e="T78" id="Seg_2894" s="T77">e-bit</ta>
            <ta e="T79" id="Seg_2895" s="T78">bu</ta>
            <ta e="T80" id="Seg_2896" s="T79">ičeːn</ta>
            <ta e="T81" id="Seg_2897" s="T80">berke</ta>
            <ta e="T82" id="Seg_2898" s="T81">ahɨn-ar</ta>
            <ta e="T83" id="Seg_2899" s="T82">haːmaj-ɨ-n</ta>
            <ta e="T84" id="Seg_2900" s="T83">biːrde</ta>
            <ta e="T85" id="Seg_2901" s="T84">tühüː-r</ta>
            <ta e="T86" id="Seg_2902" s="T85">ol</ta>
            <ta e="T87" id="Seg_2903" s="T86">tüh-ü͡ö-n</ta>
            <ta e="T88" id="Seg_2904" s="T87">baran</ta>
            <ta e="T89" id="Seg_2905" s="T88">haːmaj-ɨ-gar</ta>
            <ta e="T90" id="Seg_2906" s="T89">kel-en</ta>
            <ta e="T91" id="Seg_2907" s="T90">kepsiː-r</ta>
            <ta e="T92" id="Seg_2908" s="T91">bu</ta>
            <ta e="T93" id="Seg_2909" s="T92">en</ta>
            <ta e="T94" id="Seg_2910" s="T93">kɨːh-ɨ-ŋ</ta>
            <ta e="T95" id="Seg_2911" s="T94">till-i͡ek</ta>
            <ta e="T96" id="Seg_2912" s="T95">bɨhɨːlaːk</ta>
            <ta e="T97" id="Seg_2913" s="T96">mu͡okah-a</ta>
            <ta e="T98" id="Seg_2914" s="T97">biːr</ta>
            <ta e="T99" id="Seg_2915" s="T98">e-t-e</ta>
            <ta e="T100" id="Seg_2916" s="T99">ogo-ŋ</ta>
            <ta e="T101" id="Seg_2917" s="T100">kut-u-n</ta>
            <ta e="T102" id="Seg_2918" s="T101">hi͡e-bit</ta>
            <ta e="T103" id="Seg_2919" s="T102">ölüː-te</ta>
            <ta e="T104" id="Seg_2920" s="T103">ebe-ge</ta>
            <ta e="T105" id="Seg_2921" s="T104">ölüː</ta>
            <ta e="T106" id="Seg_2922" s="T105">uː-tu-gar</ta>
            <ta e="T107" id="Seg_2923" s="T106">ildʼ-e</ta>
            <ta e="T108" id="Seg_2924" s="T107">hɨldʼ-ar</ta>
            <ta e="T109" id="Seg_2925" s="T108">e-bit</ta>
            <ta e="T110" id="Seg_2926" s="T109">biːrges</ta>
            <ta e="T111" id="Seg_2927" s="T110">ojun-u-ŋ</ta>
            <ta e="T112" id="Seg_2928" s="T111">kɨːl</ta>
            <ta e="T113" id="Seg_2929" s="T112">bu͡ol-an</ta>
            <ta e="T114" id="Seg_2930" s="T113">hir-i-nen</ta>
            <ta e="T115" id="Seg_2931" s="T114">bat-an</ta>
            <ta e="T116" id="Seg_2932" s="T115">tüː-te</ta>
            <ta e="T117" id="Seg_2933" s="T116">kɨraːrɨ</ta>
            <ta e="T118" id="Seg_2934" s="T117">kajagas-ka</ta>
            <ta e="T119" id="Seg_2935" s="T118">bap-pat</ta>
            <ta e="T120" id="Seg_2936" s="T119">bu͡ol-ar</ta>
            <ta e="T121" id="Seg_2937" s="T120">biːrges</ta>
            <ta e="T122" id="Seg_2938" s="T121">kɨːl</ta>
            <ta e="T123" id="Seg_2939" s="T122">bu͡ol-an</ta>
            <ta e="T124" id="Seg_2940" s="T123">uː-nnan</ta>
            <ta e="T125" id="Seg_2941" s="T124">ust-an</ta>
            <ta e="T126" id="Seg_2942" s="T125">tij-deg-ine</ta>
            <ta e="T127" id="Seg_2943" s="T126">dolgun-a</ta>
            <ta e="T128" id="Seg_2944" s="T127">kab-aːrɨ</ta>
            <ta e="T129" id="Seg_2945" s="T128">ere</ta>
            <ta e="T130" id="Seg_2946" s="T129">gɨn-nag-ɨna</ta>
            <ta e="T131" id="Seg_2947" s="T130">kut-u-n</ta>
            <ta e="T132" id="Seg_2948" s="T131">ildʼ-en</ta>
            <ta e="T133" id="Seg_2949" s="T132">keːh-er</ta>
            <ta e="T134" id="Seg_2950" s="T133">togus</ta>
            <ta e="T135" id="Seg_2951" s="T134">hɨl</ta>
            <ta e="T136" id="Seg_2952" s="T135">ogurduk</ta>
            <ta e="T137" id="Seg_2953" s="T136">bat-ɨ-h-ɨ͡ak-tara</ta>
            <ta e="T138" id="Seg_2954" s="T137">onton</ta>
            <ta e="T139" id="Seg_2955" s="T138">kajtak</ta>
            <ta e="T140" id="Seg_2956" s="T139">bu͡ol-ar</ta>
            <ta e="T141" id="Seg_2957" s="T140">onu͡o-ga</ta>
            <ta e="T142" id="Seg_2958" s="T141">haːmaj</ta>
            <ta e="T143" id="Seg_2959" s="T142">ogonnʼor-o</ta>
            <ta e="T144" id="Seg_2960" s="T143">et-er</ta>
            <ta e="T145" id="Seg_2961" s="T144">ol</ta>
            <ta e="T146" id="Seg_2962" s="T145">ojun</ta>
            <ta e="T147" id="Seg_2963" s="T146">kɨ͡aj-bat-ɨ-n</ta>
            <ta e="T148" id="Seg_2964" s="T147">bil-er</ta>
            <ta e="T149" id="Seg_2965" s="T148">kihi</ta>
            <ta e="T150" id="Seg_2966" s="T149">beje-ŋ</ta>
            <ta e="T151" id="Seg_2967" s="T150">togo</ta>
            <ta e="T152" id="Seg_2968" s="T151">tilin-ner-en</ta>
            <ta e="T153" id="Seg_2969" s="T152">kör-bök-kün</ta>
            <ta e="T154" id="Seg_2970" s="T153">diː-r</ta>
            <ta e="T155" id="Seg_2971" s="T154">aːttah-ar</ta>
            <ta e="T156" id="Seg_2972" s="T155">dʼe</ta>
            <ta e="T157" id="Seg_2973" s="T156">berke</ta>
            <ta e="T158" id="Seg_2974" s="T157">muŋ-nan-nɨ-ŋ</ta>
            <ta e="T159" id="Seg_2975" s="T158">diː-r</ta>
            <ta e="T160" id="Seg_2976" s="T159">kajtak</ta>
            <ta e="T161" id="Seg_2977" s="T160">gɨn-a-bɨn</ta>
            <ta e="T162" id="Seg_2978" s="T161">tilin-neg-ine</ta>
            <ta e="T163" id="Seg_2979" s="T162">mi͡e-ke</ta>
            <ta e="T164" id="Seg_2980" s="T163">dʼaktar</ta>
            <ta e="T165" id="Seg_2981" s="T164">bi͡er-i͡e-ŋ</ta>
            <ta e="T166" id="Seg_2982" s="T165">duː</ta>
            <ta e="T167" id="Seg_2983" s="T166">baːj-ɨ-m</ta>
            <ta e="T168" id="Seg_2984" s="T167">aŋar-ɨ-n</ta>
            <ta e="T169" id="Seg_2985" s="T168">bi͡er-i͡e-m</ta>
            <ta e="T170" id="Seg_2986" s="T169">beje-ti-n</ta>
            <ta e="T171" id="Seg_2987" s="T170">bi͡er-i͡e-m</ta>
            <ta e="T172" id="Seg_2988" s="T171">tilin-ner</ta>
            <ta e="T173" id="Seg_2989" s="T172">ere</ta>
            <ta e="T174" id="Seg_2990" s="T173">diː-r</ta>
            <ta e="T175" id="Seg_2991" s="T174">ičeːn-e</ta>
            <ta e="T176" id="Seg_2992" s="T175">utuj-an</ta>
            <ta e="T177" id="Seg_2993" s="T176">kaːl-ar</ta>
            <ta e="T178" id="Seg_2994" s="T177">bu</ta>
            <ta e="T179" id="Seg_2995" s="T178">utuj-a</ta>
            <ta e="T180" id="Seg_2996" s="T179">hɨt-an</ta>
            <ta e="T181" id="Seg_2997" s="T180">dʼe</ta>
            <ta e="T182" id="Seg_2998" s="T181">ol</ta>
            <ta e="T183" id="Seg_2999" s="T182">ölüː-tü-n</ta>
            <ta e="T184" id="Seg_3000" s="T183">hir-i-ger</ta>
            <ta e="T185" id="Seg_3001" s="T184">tij-er</ta>
            <ta e="T186" id="Seg_3002" s="T185">dʼe</ta>
            <ta e="T187" id="Seg_3003" s="T186">ol</ta>
            <ta e="T188" id="Seg_3004" s="T187">ölüː-tü-n</ta>
            <ta e="T189" id="Seg_3005" s="T188">hir-i-ger</ta>
            <ta e="T190" id="Seg_3006" s="T189">tij-er</ta>
            <ta e="T191" id="Seg_3007" s="T190">tij-bit-e</ta>
            <ta e="T192" id="Seg_3008" s="T191">elʼbek</ta>
            <ta e="T193" id="Seg_3009" s="T192">bagajɨ</ta>
            <ta e="T194" id="Seg_3010" s="T193">dʼon</ta>
            <ta e="T195" id="Seg_3011" s="T194">üŋküːl-üː</ta>
            <ta e="T196" id="Seg_3012" s="T195">tur-al-lar</ta>
            <ta e="T197" id="Seg_3013" s="T196">manna</ta>
            <ta e="T198" id="Seg_3014" s="T197">körd-üː</ta>
            <ta e="T199" id="Seg_3015" s="T198">hatɨː-r</ta>
            <ta e="T200" id="Seg_3016" s="T199">kɨːs-tarɨ-n</ta>
            <ta e="T201" id="Seg_3017" s="T200">kut-u-n</ta>
            <ta e="T202" id="Seg_3018" s="T201">tu͡ok</ta>
            <ta e="T203" id="Seg_3019" s="T202">kel-i͡e=j</ta>
            <ta e="T204" id="Seg_3020" s="T203">vovse</ta>
            <ta e="T205" id="Seg_3021" s="T204">bul-bat</ta>
            <ta e="T206" id="Seg_3022" s="T205">onton</ta>
            <ta e="T207" id="Seg_3023" s="T206">bu</ta>
            <ta e="T208" id="Seg_3024" s="T207">hir-ten</ta>
            <ta e="T209" id="Seg_3025" s="T208">atɨn</ta>
            <ta e="T210" id="Seg_3026" s="T209">mi͡este-ge</ta>
            <ta e="T211" id="Seg_3027" s="T210">bar-ɨːm</ta>
            <ta e="T212" id="Seg_3028" s="T211">diː-r</ta>
            <ta e="T213" id="Seg_3029" s="T212">tij-er</ta>
            <ta e="T214" id="Seg_3030" s="T213">biːr</ta>
            <ta e="T215" id="Seg_3031" s="T214">hir-ge</ta>
            <ta e="T216" id="Seg_3032" s="T215">onno</ta>
            <ta e="T217" id="Seg_3033" s="T216">üs</ta>
            <ta e="T218" id="Seg_3034" s="T217">te=bis</ta>
            <ta e="T219" id="Seg_3035" s="T218">teŋ</ta>
            <ta e="T220" id="Seg_3036" s="T219">hɨraj-daːk</ta>
            <ta e="T221" id="Seg_3037" s="T220">taŋas-taːk</ta>
            <ta e="T222" id="Seg_3038" s="T221">kɨːs</ta>
            <ta e="T223" id="Seg_3039" s="T222">baːr</ta>
            <ta e="T224" id="Seg_3040" s="T223">e-bit</ta>
            <ta e="T225" id="Seg_3041" s="T224">man-tan</ta>
            <ta e="T226" id="Seg_3042" s="T225">biːrges-tere</ta>
            <ta e="T227" id="Seg_3043" s="T226">ogonnʼor</ta>
            <ta e="T228" id="Seg_3044" s="T227">kɨːh-a</ta>
            <ta e="T229" id="Seg_3045" s="T228">kaja-lara</ta>
            <ta e="T230" id="Seg_3046" s="T229">ol</ta>
            <ta e="T231" id="Seg_3047" s="T230">kɨːh-ɨ-n</ta>
            <ta e="T232" id="Seg_3048" s="T231">kɨ͡aj-an</ta>
            <ta e="T233" id="Seg_3049" s="T232">bil-bet</ta>
            <ta e="T234" id="Seg_3050" s="T233">taŋas-tarɨ-n</ta>
            <ta e="T235" id="Seg_3051" s="T234">hiːg-i-n</ta>
            <ta e="T236" id="Seg_3052" s="T235">aːg-an</ta>
            <ta e="T237" id="Seg_3053" s="T236">kör-ör</ta>
            <ta e="T238" id="Seg_3054" s="T237">aksaːn-a</ta>
            <ta e="T239" id="Seg_3055" s="T238">barɨta</ta>
            <ta e="T240" id="Seg_3056" s="T239">biːr</ta>
            <ta e="T241" id="Seg_3057" s="T240">muŋ-nan-a</ta>
            <ta e="T242" id="Seg_3058" s="T241">hatɨː-r</ta>
            <ta e="T243" id="Seg_3059" s="T242">bu</ta>
            <ta e="T244" id="Seg_3060" s="T243">kɨrgɨt-tar</ta>
            <ta e="T245" id="Seg_3061" s="T244">üs</ta>
            <ta e="T246" id="Seg_3062" s="T245">dʼi͡e-leːk-ter</ta>
            <ta e="T247" id="Seg_3063" s="T246">tus</ta>
            <ta e="T248" id="Seg_3064" s="T247">tuspa</ta>
            <ta e="T249" id="Seg_3065" s="T248">onno</ta>
            <ta e="T250" id="Seg_3066" s="T249">ki͡ehe</ta>
            <ta e="T251" id="Seg_3067" s="T250">üŋküː-tten</ta>
            <ta e="T252" id="Seg_3068" s="T251">kel-en</ta>
            <ta e="T253" id="Seg_3069" s="T252">utuj-an</ta>
            <ta e="T254" id="Seg_3070" s="T253">kaːl-al-lar</ta>
            <ta e="T255" id="Seg_3071" s="T254">kɨrgɨt-tar-ɨ-n</ta>
            <ta e="T256" id="Seg_3072" s="T255">kɨtta</ta>
            <ta e="T257" id="Seg_3073" s="T256">kepset-eːri</ta>
            <ta e="T258" id="Seg_3074" s="T257">gɨn-ar</ta>
            <ta e="T259" id="Seg_3075" s="T258">biːrdes-tere</ta>
            <ta e="T260" id="Seg_3076" s="T259">da</ta>
            <ta e="T261" id="Seg_3077" s="T260">haŋar-bat</ta>
            <ta e="T262" id="Seg_3078" s="T261">kajdak</ta>
            <ta e="T263" id="Seg_3079" s="T262">bu͡ol-u͡o-m=uj</ta>
            <ta e="T264" id="Seg_3080" s="T263">di͡e-n</ta>
            <ta e="T265" id="Seg_3081" s="T264">hanan-ar</ta>
            <ta e="T266" id="Seg_3082" s="T265">birges-teri-n</ta>
            <ta e="T267" id="Seg_3083" s="T266">dʼi͡e-ti-ger</ta>
            <ta e="T268" id="Seg_3084" s="T267">hah-an</ta>
            <ta e="T269" id="Seg_3085" s="T268">kiːr-en</ta>
            <ta e="T270" id="Seg_3086" s="T269">utuj-t-u-n</ta>
            <ta e="T271" id="Seg_3087" s="T270">ketiː-r</ta>
            <ta e="T272" id="Seg_3088" s="T271">ugurduk</ta>
            <ta e="T273" id="Seg_3089" s="T272">üs</ta>
            <ta e="T274" id="Seg_3090" s="T273">dʼi͡e-ge</ta>
            <ta e="T275" id="Seg_3091" s="T274">üh-ü͡ön-neri-ger</ta>
            <ta e="T276" id="Seg_3092" s="T275">kon-n-o</ta>
            <ta e="T277" id="Seg_3093" s="T276">da</ta>
            <ta e="T278" id="Seg_3094" s="T277">tug-u</ta>
            <ta e="T279" id="Seg_3095" s="T278">daː</ta>
            <ta e="T280" id="Seg_3096" s="T279">bul-an</ta>
            <ta e="T281" id="Seg_3097" s="T280">ɨl-ba-t-a</ta>
            <ta e="T282" id="Seg_3098" s="T281">dʼe</ta>
            <ta e="T283" id="Seg_3099" s="T282">biːrde</ta>
            <ta e="T284" id="Seg_3100" s="T283">üh-ü͡ön</ta>
            <ta e="T285" id="Seg_3101" s="T284">biːr</ta>
            <ta e="T286" id="Seg_3102" s="T285">dʼi͡e-ge</ta>
            <ta e="T287" id="Seg_3103" s="T286">munnʼus-t-al-lara</ta>
            <ta e="T288" id="Seg_3104" s="T287">bu͡ol-but</ta>
            <ta e="T289" id="Seg_3105" s="T288">manna</ta>
            <ta e="T290" id="Seg_3106" s="T289">kiːr-en</ta>
            <ta e="T291" id="Seg_3107" s="T290">ičeːn</ta>
            <ta e="T292" id="Seg_3108" s="T291">hah-a</ta>
            <ta e="T293" id="Seg_3109" s="T292">hɨp-pɨt</ta>
            <ta e="T294" id="Seg_3110" s="T293">bu</ta>
            <ta e="T295" id="Seg_3111" s="T294">hɨt-tag-ɨna</ta>
            <ta e="T296" id="Seg_3112" s="T295">biːr</ta>
            <ta e="T297" id="Seg_3113" s="T296">kɨːs</ta>
            <ta e="T298" id="Seg_3114" s="T297">araj</ta>
            <ta e="T299" id="Seg_3115" s="T298">hanaːrgaː-bɨt</ta>
            <ta e="T300" id="Seg_3116" s="T299">ma-nɨ</ta>
            <ta e="T301" id="Seg_3117" s="T300">ikki</ta>
            <ta e="T302" id="Seg_3118" s="T301">dogor-o</ta>
            <ta e="T303" id="Seg_3119" s="T302">ɨjɨp-pɨt-tar</ta>
            <ta e="T304" id="Seg_3120" s="T303">tu͡ok</ta>
            <ta e="T305" id="Seg_3121" s="T304">bu͡ol-an</ta>
            <ta e="T306" id="Seg_3122" s="T305">hannaːrg-ɨː-gɨn</ta>
            <ta e="T307" id="Seg_3123" s="T306">di͡e-n</ta>
            <ta e="T308" id="Seg_3124" s="T307">kɨːs-tara</ta>
            <ta e="T309" id="Seg_3125" s="T308">ep-pit</ta>
            <ta e="T310" id="Seg_3126" s="T309">min</ta>
            <ta e="T311" id="Seg_3127" s="T310">ije-m</ta>
            <ta e="T312" id="Seg_3128" s="T311">biːr</ta>
            <ta e="T313" id="Seg_3129" s="T312">bert</ta>
            <ta e="T314" id="Seg_3130" s="T313">keri͡es-teːk</ta>
            <ta e="T315" id="Seg_3131" s="T314">hep-pi-n</ta>
            <ta e="T316" id="Seg_3132" s="T315">bi͡er-i-mije</ta>
            <ta e="T317" id="Seg_3133" s="T316">ɨːp-pɨt</ta>
            <ta e="T318" id="Seg_3134" s="T317">o-nu</ta>
            <ta e="T319" id="Seg_3135" s="T318">dogor-doro</ta>
            <ta e="T320" id="Seg_3136" s="T319">ep-pit-ter</ta>
            <ta e="T321" id="Seg_3137" s="T320">tu͡ok</ta>
            <ta e="T322" id="Seg_3138" s="T321">bu͡ol-u͡o=j</ta>
            <ta e="T323" id="Seg_3139" s="T322">ije-te</ta>
            <ta e="T324" id="Seg_3140" s="T323">kel-leg-ine</ta>
            <ta e="T325" id="Seg_3141" s="T324">beje-te</ta>
            <ta e="T326" id="Seg_3142" s="T325">egel-en</ta>
            <ta e="T327" id="Seg_3143" s="T326">bi͡er-i͡e</ta>
            <ta e="T328" id="Seg_3144" s="T327">bu͡ollaga</ta>
            <ta e="T329" id="Seg_3145" s="T328">diː</ta>
            <ta e="T330" id="Seg_3146" s="T329">dʼe</ta>
            <ta e="T331" id="Seg_3147" s="T330">bu</ta>
            <ta e="T332" id="Seg_3148" s="T331">e-bit</ta>
            <ta e="T333" id="Seg_3149" s="T332">bu͡ollaga</ta>
            <ta e="T334" id="Seg_3150" s="T333">ogonnʼor</ta>
            <ta e="T335" id="Seg_3151" s="T334">kɨːh-a</ta>
            <ta e="T336" id="Seg_3152" s="T335">d-iː</ta>
            <ta e="T337" id="Seg_3153" s="T336">hanɨː-r</ta>
            <ta e="T338" id="Seg_3154" s="T337">utuj-but-u-n</ta>
            <ta e="T339" id="Seg_3155" s="T338">kenne</ta>
            <ta e="T340" id="Seg_3156" s="T339">kuːh-an</ta>
            <ta e="T341" id="Seg_3157" s="T340">baran</ta>
            <ta e="T342" id="Seg_3158" s="T341">dʼe</ta>
            <ta e="T343" id="Seg_3159" s="T342">köt-ör</ta>
            <ta e="T344" id="Seg_3160" s="T343">diː</ta>
            <ta e="T345" id="Seg_3161" s="T344">ol</ta>
            <ta e="T346" id="Seg_3162" s="T345">biri͡eme-ge</ta>
            <ta e="T347" id="Seg_3163" s="T346">tukarɨ</ta>
            <ta e="T348" id="Seg_3164" s="T347">togus</ta>
            <ta e="T349" id="Seg_3165" s="T348">kun</ta>
            <ta e="T350" id="Seg_3166" s="T349">utuj-a</ta>
            <ta e="T351" id="Seg_3167" s="T350">hɨp-pɨt</ta>
            <ta e="T352" id="Seg_3168" s="T351">dʼe</ta>
            <ta e="T353" id="Seg_3169" s="T352">kuːh-an</ta>
            <ta e="T354" id="Seg_3170" s="T353">baran</ta>
            <ta e="T355" id="Seg_3171" s="T354">ekkiri͡e-n</ta>
            <ta e="T356" id="Seg_3172" s="T355">tur-ar</ta>
            <ta e="T357" id="Seg_3173" s="T356">diː</ta>
            <ta e="T358" id="Seg_3174" s="T357">uhukt-an</ta>
            <ta e="T359" id="Seg_3175" s="T358">manna</ta>
            <ta e="T360" id="Seg_3176" s="T359">hüːr-en-ner</ta>
            <ta e="T361" id="Seg_3177" s="T360">löčü͡ög-ü</ta>
            <ta e="T362" id="Seg_3178" s="T361">egel-el-ler</ta>
            <ta e="T363" id="Seg_3179" s="T362">uŋu͡ok-tar-ɨ-n</ta>
            <ta e="T364" id="Seg_3180" s="T363">egel-i-ŋ</ta>
            <ta e="T365" id="Seg_3181" s="T364">d-iː-d-iː</ta>
            <ta e="T366" id="Seg_3182" s="T365">kɨːr-an</ta>
            <ta e="T367" id="Seg_3183" s="T366">nʼühüj-er</ta>
            <ta e="T368" id="Seg_3184" s="T367">diː</ta>
            <ta e="T369" id="Seg_3185" s="T368">üs</ta>
            <ta e="T370" id="Seg_3186" s="T369">tüːn-neːk</ta>
            <ta e="T371" id="Seg_3187" s="T370">kün-ü</ta>
            <ta e="T372" id="Seg_3188" s="T371">ölüː</ta>
            <ta e="T373" id="Seg_3189" s="T372">dojdu-tu-ttan</ta>
            <ta e="T374" id="Seg_3190" s="T373">tönn-ön</ta>
            <ta e="T375" id="Seg_3191" s="T374">onu͡o-ga</ta>
            <ta e="T376" id="Seg_3192" s="T375">taba</ta>
            <ta e="T377" id="Seg_3193" s="T376">tiriː-ti-ger</ta>
            <ta e="T378" id="Seg_3194" s="T377">egel-el-ler</ta>
            <ta e="T379" id="Seg_3195" s="T378">oŋu͡ok-tarɨ-n</ta>
            <ta e="T380" id="Seg_3196" s="T379">kör-büt-tere</ta>
            <ta e="T381" id="Seg_3197" s="T380">kɨːh-ɨ</ta>
            <ta e="T382" id="Seg_3198" s="T381">kötög-ön</ta>
            <ta e="T383" id="Seg_3199" s="T382">tur-ar</ta>
            <ta e="T384" id="Seg_3200" s="T383">tolʼko</ta>
            <ta e="T385" id="Seg_3201" s="T384">kɨːs-tara</ta>
            <ta e="T386" id="Seg_3202" s="T385">kamnaː-bat</ta>
            <ta e="T387" id="Seg_3203" s="T386">öl-büt</ta>
            <ta e="T388" id="Seg_3204" s="T387">duː</ta>
            <ta e="T389" id="Seg_3205" s="T388">utuj-but</ta>
            <ta e="T390" id="Seg_3206" s="T389">duː</ta>
            <ta e="T391" id="Seg_3207" s="T390">kördük</ta>
            <ta e="T392" id="Seg_3208" s="T391">baː</ta>
            <ta e="T393" id="Seg_3209" s="T392">kɨːs</ta>
            <ta e="T394" id="Seg_3210" s="T393">uŋu͡ok-tarɨ-n</ta>
            <ta e="T395" id="Seg_3211" s="T394">ih-i-ger</ta>
            <ta e="T396" id="Seg_3212" s="T395">hukuj-duː</ta>
            <ta e="T397" id="Seg_3213" s="T396">kaːlɨː-r</ta>
            <ta e="T398" id="Seg_3214" s="T397">diː</ta>
            <ta e="T399" id="Seg_3215" s="T398">man-tɨ-ta</ta>
            <ta e="T400" id="Seg_3216" s="T399">till-en</ta>
            <ta e="T401" id="Seg_3217" s="T400">kel-en</ta>
            <ta e="T402" id="Seg_3218" s="T401">dʼe</ta>
            <ta e="T403" id="Seg_3219" s="T402">baː</ta>
            <ta e="T404" id="Seg_3220" s="T403">ogonnʼor-u-ŋ</ta>
            <ta e="T405" id="Seg_3221" s="T404">kɨːh-a</ta>
            <ta e="T406" id="Seg_3222" s="T405">bu͡ol-ar</ta>
            <ta e="T407" id="Seg_3223" s="T406">ma-nɨ</ta>
            <ta e="T408" id="Seg_3224" s="T407">ičeːn-i-ŋ</ta>
            <ta e="T409" id="Seg_3225" s="T408">dʼaktar</ta>
            <ta e="T410" id="Seg_3226" s="T409">gɨn-an</ta>
            <ta e="T411" id="Seg_3227" s="T410">olor-or</ta>
            <ta e="T412" id="Seg_3228" s="T411">kaːl-aːččɨ</ta>
            <ta e="T413" id="Seg_3229" s="T412">ikki</ta>
            <ta e="T414" id="Seg_3230" s="T413">dogor-doro</ta>
            <ta e="T415" id="Seg_3231" s="T414">üŋküː-ge</ta>
            <ta e="T416" id="Seg_3232" s="T415">ɨgɨr-aːrɨ</ta>
            <ta e="T417" id="Seg_3233" s="T416">gɨm-mɨt-tar</ta>
            <ta e="T418" id="Seg_3234" s="T417">kihi-lere</ta>
            <ta e="T419" id="Seg_3235" s="T418">hu͡ok</ta>
            <ta e="T420" id="Seg_3236" s="T419">bil-bit-tere</ta>
            <ta e="T421" id="Seg_3237" s="T420">ojun</ta>
            <ta e="T422" id="Seg_3238" s="T421">il-pit</ta>
            <ta e="T423" id="Seg_3239" s="T422">ma-nɨ</ta>
            <ta e="T424" id="Seg_3240" s="T423">dʼe</ta>
            <ta e="T425" id="Seg_3241" s="T424">kɨjakan-al-lar</ta>
            <ta e="T426" id="Seg_3242" s="T425">till-eːčči-gi-n</ta>
            <ta e="T427" id="Seg_3243" s="T426">bul-an</ta>
            <ta e="T428" id="Seg_3244" s="T427">hanaː-tɨ-n</ta>
            <ta e="T429" id="Seg_3245" s="T428">erij-en</ta>
            <ta e="T430" id="Seg_3246" s="T429">ü͡ör</ta>
            <ta e="T431" id="Seg_3247" s="T430">bu͡ol-an</ta>
            <ta e="T432" id="Seg_3248" s="T431">udagan</ta>
            <ta e="T433" id="Seg_3249" s="T432">gɨn-al-lar</ta>
            <ta e="T434" id="Seg_3250" s="T433">udagan</ta>
            <ta e="T435" id="Seg_3251" s="T434">gɨn-aːrɨ</ta>
            <ta e="T436" id="Seg_3252" s="T435">kaːj-al-lar</ta>
            <ta e="T437" id="Seg_3253" s="T436">bihigi</ta>
            <ta e="T438" id="Seg_3254" s="T437">ejigi-n</ta>
            <ta e="T439" id="Seg_3255" s="T438">h-i͡ek-pit</ta>
            <ta e="T440" id="Seg_3256" s="T439">emeːksin</ta>
            <ta e="T441" id="Seg_3257" s="T440">tiriː-ti-n</ta>
            <ta e="T442" id="Seg_3258" s="T441">hül-eŋ-ŋin</ta>
            <ta e="T443" id="Seg_3259" s="T442">düŋür</ta>
            <ta e="T444" id="Seg_3260" s="T443">oŋost-u-ba-tak-kɨna</ta>
            <ta e="T445" id="Seg_3261" s="T444">dʼe</ta>
            <ta e="T446" id="Seg_3262" s="T445">onno</ta>
            <ta e="T447" id="Seg_3263" s="T446">biːr</ta>
            <ta e="T448" id="Seg_3264" s="T447">emeːksin-i</ta>
            <ta e="T449" id="Seg_3265" s="T448">kastaː-n-nar</ta>
            <ta e="T450" id="Seg_3266" s="T449">ol</ta>
            <ta e="T451" id="Seg_3267" s="T450">tiriː-ti-nen</ta>
            <ta e="T452" id="Seg_3268" s="T451">düŋür</ta>
            <ta e="T453" id="Seg_3269" s="T452">oŋost-ol-lor</ta>
            <ta e="T454" id="Seg_3270" s="T453">ol</ta>
            <ta e="T455" id="Seg_3271" s="T454">dom-u-gar</ta>
            <ta e="T456" id="Seg_3272" s="T455">iti</ta>
            <ta e="T457" id="Seg_3273" s="T456">ɨ͡aldʼ-ar</ta>
            <ta e="T458" id="Seg_3274" s="T457">bu͡olluŋ</ta>
            <ta e="T459" id="Seg_3275" s="T458">daː</ta>
            <ta e="T460" id="Seg_3276" s="T459">haːmaj</ta>
            <ta e="T461" id="Seg_3277" s="T460">biːr-diː</ta>
            <ta e="T462" id="Seg_3278" s="T461">öl-böt</ta>
            <ta e="T463" id="Seg_3279" s="T462">haːmaj</ta>
            <ta e="T464" id="Seg_3280" s="T463">orda-nnan</ta>
            <ta e="T465" id="Seg_3281" s="T464">baran-ar</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_3282" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_3283" s="T1">biːr</ta>
            <ta e="T3" id="Seg_3284" s="T2">haːmaj</ta>
            <ta e="T4" id="Seg_3285" s="T3">ogonnʼor-tA</ta>
            <ta e="T5" id="Seg_3286" s="T4">olor-BIT</ta>
            <ta e="T6" id="Seg_3287" s="T5">kɨːs-LAːK</ta>
            <ta e="T7" id="Seg_3288" s="T6">bu</ta>
            <ta e="T8" id="Seg_3289" s="T7">kɨːs-tA</ta>
            <ta e="T9" id="Seg_3290" s="T8">ulaːt-An</ta>
            <ta e="T10" id="Seg_3291" s="T9">küheːjke</ta>
            <ta e="T11" id="Seg_3292" s="T10">bu͡ol-An</ta>
            <ta e="T12" id="Seg_3293" s="T11">baran</ta>
            <ta e="T13" id="Seg_3294" s="T12">öl-An</ta>
            <ta e="T14" id="Seg_3295" s="T13">kaːl-BIT</ta>
            <ta e="T15" id="Seg_3296" s="T14">bu-nI</ta>
            <ta e="T16" id="Seg_3297" s="T15">aga-tA</ta>
            <ta e="T17" id="Seg_3298" s="T16">hürdeːk-LIk</ta>
            <ta e="T18" id="Seg_3299" s="T17">ɨtaː-Ar</ta>
            <ta e="T19" id="Seg_3300" s="T18">karaj-AːrI</ta>
            <ta e="T20" id="Seg_3301" s="T19">gɨn-BIT-LArI-n</ta>
            <ta e="T21" id="Seg_3302" s="T20">et-Ar</ta>
            <ta e="T22" id="Seg_3303" s="T21">min</ta>
            <ta e="T23" id="Seg_3304" s="T22">kɨːs-BI-n</ta>
            <ta e="T24" id="Seg_3305" s="T23">bi͡ek</ta>
            <ta e="T25" id="Seg_3306" s="T24">beje-BI-n</ta>
            <ta e="T26" id="Seg_3307" s="T25">kɨtta</ta>
            <ta e="T27" id="Seg_3308" s="T26">ti͡ej-A</ta>
            <ta e="T28" id="Seg_3309" s="T27">hɨrɨt-IAK-m</ta>
            <ta e="T29" id="Seg_3310" s="T28">gini</ta>
            <ta e="T30" id="Seg_3311" s="T29">tilin-IAK-tA</ta>
            <ta e="T31" id="Seg_3312" s="T30">ulakan</ta>
            <ta e="T32" id="Seg_3313" s="T31">ojun-GA</ta>
            <ta e="T33" id="Seg_3314" s="T32">tilin-TAr-IAK-m</ta>
            <ta e="T34" id="Seg_3315" s="T33">di͡e-Ar</ta>
            <ta e="T35" id="Seg_3316" s="T34">dʼe</ta>
            <ta e="T36" id="Seg_3317" s="T35">kɨːs-tI-n</ta>
            <ta e="T37" id="Seg_3318" s="T36">hɨrga-GA</ta>
            <ta e="T38" id="Seg_3319" s="T37">ti͡ej-A</ta>
            <ta e="T39" id="Seg_3320" s="T38">hɨrɨt-Ar</ta>
            <ta e="T40" id="Seg_3321" s="T39">ol</ta>
            <ta e="T41" id="Seg_3322" s="T40">hɨrɨt-An</ta>
            <ta e="T42" id="Seg_3323" s="T41">ojun-LAr-nI</ta>
            <ta e="T43" id="Seg_3324" s="T42">kördöː-A-s-Ar</ta>
            <ta e="T44" id="Seg_3325" s="T43">biːr-LArA</ta>
            <ta e="T45" id="Seg_3326" s="T44">da</ta>
            <ta e="T46" id="Seg_3327" s="T45">bu͡ol-BAT</ta>
            <ta e="T47" id="Seg_3328" s="T46">agaj</ta>
            <ta e="T48" id="Seg_3329" s="T47">ikki</ta>
            <ta e="T49" id="Seg_3330" s="T48">ulakan</ta>
            <ta e="T50" id="Seg_3331" s="T49">ojun-nI</ta>
            <ta e="T51" id="Seg_3332" s="T50">kɨːr-TAr-Ar</ta>
            <ta e="T52" id="Seg_3333" s="T51">ikki</ta>
            <ta e="T53" id="Seg_3334" s="T52">dʼɨl-tA</ta>
            <ta e="T54" id="Seg_3335" s="T53">aːs-BIT-tI-n</ta>
            <ta e="T55" id="Seg_3336" s="T54">genne</ta>
            <ta e="T56" id="Seg_3337" s="T55">kɨːs-tI-n</ta>
            <ta e="T57" id="Seg_3338" s="T56">et-tA</ta>
            <ta e="T58" id="Seg_3339" s="T57">dabnu͡o</ta>
            <ta e="T59" id="Seg_3340" s="T58">hɨtɨj-An</ta>
            <ta e="T60" id="Seg_3341" s="T59">oŋu͡ok-LArA</ta>
            <ta e="T61" id="Seg_3342" s="T60">ere</ta>
            <ta e="T62" id="Seg_3343" s="T61">kaːl-BIT</ta>
            <ta e="T63" id="Seg_3344" s="T62">bu</ta>
            <ta e="T64" id="Seg_3345" s="T63">ikki</ta>
            <ta e="T65" id="Seg_3346" s="T64">ojun</ta>
            <ta e="T66" id="Seg_3347" s="T65">kɨːr-An-LAr</ta>
            <ta e="T67" id="Seg_3348" s="T66">tu͡ok-nI</ta>
            <ta e="T68" id="Seg_3349" s="T67">da</ta>
            <ta e="T69" id="Seg_3350" s="T68">gɨn-BAT-LAr</ta>
            <ta e="T70" id="Seg_3351" s="T69">üs-Is</ta>
            <ta e="T71" id="Seg_3352" s="T70">dʼɨl-tA</ta>
            <ta e="T72" id="Seg_3353" s="T71">bu͡ol-Ar</ta>
            <ta e="T73" id="Seg_3354" s="T72">ol</ta>
            <ta e="T74" id="Seg_3355" s="T73">haːmaj</ta>
            <ta e="T75" id="Seg_3356" s="T74">ɨksa-tI-GAr</ta>
            <ta e="T76" id="Seg_3357" s="T75">ičeːn</ta>
            <ta e="T77" id="Seg_3358" s="T76">baːr</ta>
            <ta e="T78" id="Seg_3359" s="T77">e-BIT</ta>
            <ta e="T79" id="Seg_3360" s="T78">bu</ta>
            <ta e="T80" id="Seg_3361" s="T79">ičeːn</ta>
            <ta e="T81" id="Seg_3362" s="T80">berke</ta>
            <ta e="T82" id="Seg_3363" s="T81">ahɨn-Ar</ta>
            <ta e="T83" id="Seg_3364" s="T82">haːmaj-tI-n</ta>
            <ta e="T84" id="Seg_3365" s="T83">biːrde</ta>
            <ta e="T85" id="Seg_3366" s="T84">tüheː-Ar</ta>
            <ta e="T86" id="Seg_3367" s="T85">ol</ta>
            <ta e="T87" id="Seg_3368" s="T86">tüheː-IAK.[tI]-n</ta>
            <ta e="T88" id="Seg_3369" s="T87">baran</ta>
            <ta e="T89" id="Seg_3370" s="T88">haːmaj-tI-GAr</ta>
            <ta e="T90" id="Seg_3371" s="T89">kel-An</ta>
            <ta e="T91" id="Seg_3372" s="T90">kepseː-Ar</ta>
            <ta e="T92" id="Seg_3373" s="T91">bu</ta>
            <ta e="T93" id="Seg_3374" s="T92">en</ta>
            <ta e="T94" id="Seg_3375" s="T93">kɨːs-I-ŋ</ta>
            <ta e="T95" id="Seg_3376" s="T94">tilin-IAK</ta>
            <ta e="T96" id="Seg_3377" s="T95">bɨhɨːlaːk</ta>
            <ta e="T97" id="Seg_3378" s="T96">mu͡okas-tA</ta>
            <ta e="T98" id="Seg_3379" s="T97">biːr</ta>
            <ta e="T99" id="Seg_3380" s="T98">e-TI-tA</ta>
            <ta e="T100" id="Seg_3381" s="T99">ogo-ŋ</ta>
            <ta e="T101" id="Seg_3382" s="T100">kut-tI-n</ta>
            <ta e="T102" id="Seg_3383" s="T101">hi͡e-BIT</ta>
            <ta e="T103" id="Seg_3384" s="T102">ölüː-tA</ta>
            <ta e="T104" id="Seg_3385" s="T103">ebe-GA</ta>
            <ta e="T105" id="Seg_3386" s="T104">ölüː</ta>
            <ta e="T106" id="Seg_3387" s="T105">uː-tI-GAr</ta>
            <ta e="T107" id="Seg_3388" s="T106">ilt-A</ta>
            <ta e="T108" id="Seg_3389" s="T107">hɨrɨt-Ar</ta>
            <ta e="T109" id="Seg_3390" s="T108">e-BIT</ta>
            <ta e="T110" id="Seg_3391" s="T109">biːrges</ta>
            <ta e="T111" id="Seg_3392" s="T110">ojun-I-ŋ</ta>
            <ta e="T112" id="Seg_3393" s="T111">kɨːl</ta>
            <ta e="T113" id="Seg_3394" s="T112">bu͡ol-An</ta>
            <ta e="T114" id="Seg_3395" s="T113">hir-I-nAn</ta>
            <ta e="T115" id="Seg_3396" s="T114">bat-An</ta>
            <ta e="T116" id="Seg_3397" s="T115">tüː-tA</ta>
            <ta e="T117" id="Seg_3398" s="T116">kɨraːrɨ</ta>
            <ta e="T118" id="Seg_3399" s="T117">kajagas-GA</ta>
            <ta e="T119" id="Seg_3400" s="T118">bat-BAT</ta>
            <ta e="T120" id="Seg_3401" s="T119">bu͡ol-Ar</ta>
            <ta e="T121" id="Seg_3402" s="T120">biːrges</ta>
            <ta e="T122" id="Seg_3403" s="T121">kɨːl</ta>
            <ta e="T123" id="Seg_3404" s="T122">bu͡ol-An</ta>
            <ta e="T124" id="Seg_3405" s="T123">uː-nAn</ta>
            <ta e="T125" id="Seg_3406" s="T124">uhun-An</ta>
            <ta e="T126" id="Seg_3407" s="T125">tij-TAK-InA</ta>
            <ta e="T127" id="Seg_3408" s="T126">dolgun-tA</ta>
            <ta e="T128" id="Seg_3409" s="T127">kap-AːrI</ta>
            <ta e="T129" id="Seg_3410" s="T128">ere</ta>
            <ta e="T130" id="Seg_3411" s="T129">gɨn-TAK-InA</ta>
            <ta e="T131" id="Seg_3412" s="T130">kut-tI-n</ta>
            <ta e="T132" id="Seg_3413" s="T131">ilt-An</ta>
            <ta e="T133" id="Seg_3414" s="T132">keːs-Ar</ta>
            <ta e="T134" id="Seg_3415" s="T133">togus</ta>
            <ta e="T135" id="Seg_3416" s="T134">dʼɨl</ta>
            <ta e="T136" id="Seg_3417" s="T135">ogorduk</ta>
            <ta e="T137" id="Seg_3418" s="T136">bat-I-s-IAK-LArA</ta>
            <ta e="T138" id="Seg_3419" s="T137">onton</ta>
            <ta e="T139" id="Seg_3420" s="T138">kajdak</ta>
            <ta e="T140" id="Seg_3421" s="T139">bu͡ol-Ar</ta>
            <ta e="T141" id="Seg_3422" s="T140">ol-GA</ta>
            <ta e="T142" id="Seg_3423" s="T141">haːmaj</ta>
            <ta e="T143" id="Seg_3424" s="T142">ogonnʼor-tA</ta>
            <ta e="T144" id="Seg_3425" s="T143">et-Ar</ta>
            <ta e="T145" id="Seg_3426" s="T144">ol</ta>
            <ta e="T146" id="Seg_3427" s="T145">ojun</ta>
            <ta e="T147" id="Seg_3428" s="T146">kɨ͡aj-BAT-tI-n</ta>
            <ta e="T148" id="Seg_3429" s="T147">bil-Ar</ta>
            <ta e="T149" id="Seg_3430" s="T148">kihi</ta>
            <ta e="T150" id="Seg_3431" s="T149">beje-ŋ</ta>
            <ta e="T151" id="Seg_3432" s="T150">togo</ta>
            <ta e="T152" id="Seg_3433" s="T151">tilin-TAr-An</ta>
            <ta e="T153" id="Seg_3434" s="T152">kör-BAT-GIn</ta>
            <ta e="T154" id="Seg_3435" s="T153">di͡e-Ar</ta>
            <ta e="T155" id="Seg_3436" s="T154">aːttas-Ar</ta>
            <ta e="T156" id="Seg_3437" s="T155">dʼe</ta>
            <ta e="T157" id="Seg_3438" s="T156">berke</ta>
            <ta e="T158" id="Seg_3439" s="T157">muŋ-LAN-TI-ŋ</ta>
            <ta e="T159" id="Seg_3440" s="T158">di͡e-Ar</ta>
            <ta e="T160" id="Seg_3441" s="T159">kajdak</ta>
            <ta e="T161" id="Seg_3442" s="T160">gɨn-A-BIn</ta>
            <ta e="T162" id="Seg_3443" s="T161">tilin-TAK-InA</ta>
            <ta e="T163" id="Seg_3444" s="T162">min-GA</ta>
            <ta e="T164" id="Seg_3445" s="T163">dʼaktar</ta>
            <ta e="T165" id="Seg_3446" s="T164">bi͡er-IAK-ŋ</ta>
            <ta e="T166" id="Seg_3447" s="T165">du͡o</ta>
            <ta e="T167" id="Seg_3448" s="T166">baːj-I-m</ta>
            <ta e="T168" id="Seg_3449" s="T167">aŋar-tI-n</ta>
            <ta e="T169" id="Seg_3450" s="T168">bi͡er-IAK-m</ta>
            <ta e="T170" id="Seg_3451" s="T169">beje-tI-n</ta>
            <ta e="T171" id="Seg_3452" s="T170">bi͡er-IAK-m</ta>
            <ta e="T172" id="Seg_3453" s="T171">tilin-TAr</ta>
            <ta e="T173" id="Seg_3454" s="T172">ere</ta>
            <ta e="T174" id="Seg_3455" s="T173">di͡e-Ar</ta>
            <ta e="T175" id="Seg_3456" s="T174">ičeːn-tA</ta>
            <ta e="T176" id="Seg_3457" s="T175">utuj-An</ta>
            <ta e="T177" id="Seg_3458" s="T176">kaːl-Ar</ta>
            <ta e="T178" id="Seg_3459" s="T177">bu</ta>
            <ta e="T179" id="Seg_3460" s="T178">utuj-A</ta>
            <ta e="T180" id="Seg_3461" s="T179">hɨt-An</ta>
            <ta e="T181" id="Seg_3462" s="T180">dʼe</ta>
            <ta e="T182" id="Seg_3463" s="T181">ol</ta>
            <ta e="T183" id="Seg_3464" s="T182">ölüː-tI-n</ta>
            <ta e="T184" id="Seg_3465" s="T183">hir-tI-GAr</ta>
            <ta e="T185" id="Seg_3466" s="T184">tij-Ar</ta>
            <ta e="T186" id="Seg_3467" s="T185">dʼe</ta>
            <ta e="T187" id="Seg_3468" s="T186">ol</ta>
            <ta e="T188" id="Seg_3469" s="T187">ölüː-tI-n</ta>
            <ta e="T189" id="Seg_3470" s="T188">hir-tI-GAr</ta>
            <ta e="T190" id="Seg_3471" s="T189">tij-Ar</ta>
            <ta e="T191" id="Seg_3472" s="T190">tij-BIT-tA</ta>
            <ta e="T192" id="Seg_3473" s="T191">elbek</ta>
            <ta e="T193" id="Seg_3474" s="T192">bagajɨ</ta>
            <ta e="T194" id="Seg_3475" s="T193">dʼon</ta>
            <ta e="T195" id="Seg_3476" s="T194">üŋküːleː-A</ta>
            <ta e="T196" id="Seg_3477" s="T195">tur-Ar-LAr</ta>
            <ta e="T197" id="Seg_3478" s="T196">manna</ta>
            <ta e="T198" id="Seg_3479" s="T197">kördöː-A</ta>
            <ta e="T199" id="Seg_3480" s="T198">hataː-Ar</ta>
            <ta e="T200" id="Seg_3481" s="T199">kɨːs-LArI-n</ta>
            <ta e="T201" id="Seg_3482" s="T200">kut-tI-n</ta>
            <ta e="T202" id="Seg_3483" s="T201">tu͡ok</ta>
            <ta e="T203" id="Seg_3484" s="T202">kel-IAK.[tA]=Ij</ta>
            <ta e="T204" id="Seg_3485" s="T203">vovse</ta>
            <ta e="T205" id="Seg_3486" s="T204">bul-BAT</ta>
            <ta e="T206" id="Seg_3487" s="T205">onton</ta>
            <ta e="T207" id="Seg_3488" s="T206">bu</ta>
            <ta e="T208" id="Seg_3489" s="T207">hir-ttAn</ta>
            <ta e="T209" id="Seg_3490" s="T208">atɨn</ta>
            <ta e="T210" id="Seg_3491" s="T209">mi͡este-GA</ta>
            <ta e="T211" id="Seg_3492" s="T210">bar-Iːm</ta>
            <ta e="T212" id="Seg_3493" s="T211">di͡e-Ar</ta>
            <ta e="T213" id="Seg_3494" s="T212">tij-Ar</ta>
            <ta e="T214" id="Seg_3495" s="T213">biːr</ta>
            <ta e="T215" id="Seg_3496" s="T214">hir-GA</ta>
            <ta e="T216" id="Seg_3497" s="T215">onno</ta>
            <ta e="T217" id="Seg_3498" s="T216">üs</ta>
            <ta e="T218" id="Seg_3499" s="T217">[C^1][V^1][C^2]=bus</ta>
            <ta e="T219" id="Seg_3500" s="T218">teŋ</ta>
            <ta e="T220" id="Seg_3501" s="T219">hɨraj-LAːK</ta>
            <ta e="T221" id="Seg_3502" s="T220">taŋas-LAːK</ta>
            <ta e="T222" id="Seg_3503" s="T221">kɨːs</ta>
            <ta e="T223" id="Seg_3504" s="T222">baːr</ta>
            <ta e="T224" id="Seg_3505" s="T223">e-BIT</ta>
            <ta e="T225" id="Seg_3506" s="T224">bu-ttAn</ta>
            <ta e="T226" id="Seg_3507" s="T225">biːrges-LArA</ta>
            <ta e="T227" id="Seg_3508" s="T226">ogonnʼor</ta>
            <ta e="T228" id="Seg_3509" s="T227">kɨːs-tA</ta>
            <ta e="T229" id="Seg_3510" s="T228">kaja-LArA</ta>
            <ta e="T230" id="Seg_3511" s="T229">ol</ta>
            <ta e="T231" id="Seg_3512" s="T230">kɨːs-tI-n</ta>
            <ta e="T232" id="Seg_3513" s="T231">kɨ͡aj-An</ta>
            <ta e="T233" id="Seg_3514" s="T232">bil-BAT</ta>
            <ta e="T234" id="Seg_3515" s="T233">taŋas-LArI-n</ta>
            <ta e="T235" id="Seg_3516" s="T234">hiːk-tI-n</ta>
            <ta e="T236" id="Seg_3517" s="T235">aːk-An</ta>
            <ta e="T237" id="Seg_3518" s="T236">kör-Ar</ta>
            <ta e="T238" id="Seg_3519" s="T237">aksaːn-tA</ta>
            <ta e="T239" id="Seg_3520" s="T238">barɨta</ta>
            <ta e="T240" id="Seg_3521" s="T239">biːr</ta>
            <ta e="T241" id="Seg_3522" s="T240">muŋ-LAN-A</ta>
            <ta e="T242" id="Seg_3523" s="T241">hataː-Ar</ta>
            <ta e="T243" id="Seg_3524" s="T242">bu</ta>
            <ta e="T244" id="Seg_3525" s="T243">kɨːs-LAr</ta>
            <ta e="T245" id="Seg_3526" s="T244">üs</ta>
            <ta e="T246" id="Seg_3527" s="T245">dʼi͡e-LAːK-LAr</ta>
            <ta e="T247" id="Seg_3528" s="T246">tus</ta>
            <ta e="T248" id="Seg_3529" s="T247">tuspa</ta>
            <ta e="T249" id="Seg_3530" s="T248">onno</ta>
            <ta e="T250" id="Seg_3531" s="T249">ki͡ehe</ta>
            <ta e="T251" id="Seg_3532" s="T250">üŋküː-ttAn</ta>
            <ta e="T252" id="Seg_3533" s="T251">kel-An</ta>
            <ta e="T253" id="Seg_3534" s="T252">utuj-An</ta>
            <ta e="T254" id="Seg_3535" s="T253">kaːl-Ar-LAr</ta>
            <ta e="T255" id="Seg_3536" s="T254">kɨːs-LAr-tI-n</ta>
            <ta e="T256" id="Seg_3537" s="T255">kɨtta</ta>
            <ta e="T257" id="Seg_3538" s="T256">kepset-AːrI</ta>
            <ta e="T258" id="Seg_3539" s="T257">gɨn-Ar</ta>
            <ta e="T259" id="Seg_3540" s="T258">biːrges-LArA</ta>
            <ta e="T260" id="Seg_3541" s="T259">da</ta>
            <ta e="T261" id="Seg_3542" s="T260">haŋar-BAT</ta>
            <ta e="T262" id="Seg_3543" s="T261">kajdak</ta>
            <ta e="T263" id="Seg_3544" s="T262">bu͡ol-IAK-m=Ij</ta>
            <ta e="T264" id="Seg_3545" s="T263">di͡e-An</ta>
            <ta e="T265" id="Seg_3546" s="T264">hanan-Ar</ta>
            <ta e="T266" id="Seg_3547" s="T265">biːrges-LArI-n</ta>
            <ta e="T267" id="Seg_3548" s="T266">dʼi͡e-tI-GAr</ta>
            <ta e="T268" id="Seg_3549" s="T267">has-An</ta>
            <ta e="T269" id="Seg_3550" s="T268">kiːr-An</ta>
            <ta e="T270" id="Seg_3551" s="T269">utuj-BIT-tI-n</ta>
            <ta e="T271" id="Seg_3552" s="T270">keteː-Ar</ta>
            <ta e="T272" id="Seg_3553" s="T271">bugurduk</ta>
            <ta e="T273" id="Seg_3554" s="T272">üs</ta>
            <ta e="T274" id="Seg_3555" s="T273">dʼi͡e-GA</ta>
            <ta e="T275" id="Seg_3556" s="T274">üs-IAn-LArI-GAr</ta>
            <ta e="T276" id="Seg_3557" s="T275">kon-TI-tA</ta>
            <ta e="T277" id="Seg_3558" s="T276">da</ta>
            <ta e="T278" id="Seg_3559" s="T277">tu͡ok-nI</ta>
            <ta e="T279" id="Seg_3560" s="T278">da</ta>
            <ta e="T280" id="Seg_3561" s="T279">bul-An</ta>
            <ta e="T281" id="Seg_3562" s="T280">ɨl-BA-TI-tA</ta>
            <ta e="T282" id="Seg_3563" s="T281">dʼe</ta>
            <ta e="T283" id="Seg_3564" s="T282">biːrde</ta>
            <ta e="T284" id="Seg_3565" s="T283">üs-IAn</ta>
            <ta e="T285" id="Seg_3566" s="T284">biːr</ta>
            <ta e="T286" id="Seg_3567" s="T285">dʼi͡e-GA</ta>
            <ta e="T287" id="Seg_3568" s="T286">munnʼus-n-Ar-LArA</ta>
            <ta e="T288" id="Seg_3569" s="T287">bu͡ol-BIT</ta>
            <ta e="T289" id="Seg_3570" s="T288">manna</ta>
            <ta e="T290" id="Seg_3571" s="T289">kiːr-An</ta>
            <ta e="T291" id="Seg_3572" s="T290">ičeːn</ta>
            <ta e="T292" id="Seg_3573" s="T291">has-A</ta>
            <ta e="T293" id="Seg_3574" s="T292">hɨt-BIT</ta>
            <ta e="T294" id="Seg_3575" s="T293">bu</ta>
            <ta e="T295" id="Seg_3576" s="T294">hɨt-TAK-InA</ta>
            <ta e="T296" id="Seg_3577" s="T295">biːr</ta>
            <ta e="T297" id="Seg_3578" s="T296">kɨːs</ta>
            <ta e="T298" id="Seg_3579" s="T297">agaj</ta>
            <ta e="T299" id="Seg_3580" s="T298">hanaːrgaː-BIT</ta>
            <ta e="T300" id="Seg_3581" s="T299">bu-nI</ta>
            <ta e="T301" id="Seg_3582" s="T300">ikki</ta>
            <ta e="T302" id="Seg_3583" s="T301">dogor-tA</ta>
            <ta e="T303" id="Seg_3584" s="T302">ɨjɨt-BIT-LAr</ta>
            <ta e="T304" id="Seg_3585" s="T303">tu͡ok</ta>
            <ta e="T305" id="Seg_3586" s="T304">bu͡ol-An</ta>
            <ta e="T306" id="Seg_3587" s="T305">hanaːrgaː-A-GIn</ta>
            <ta e="T307" id="Seg_3588" s="T306">di͡e-An</ta>
            <ta e="T308" id="Seg_3589" s="T307">kɨːs-LArA</ta>
            <ta e="T309" id="Seg_3590" s="T308">et-BIT</ta>
            <ta e="T310" id="Seg_3591" s="T309">min</ta>
            <ta e="T311" id="Seg_3592" s="T310">inʼe-m</ta>
            <ta e="T312" id="Seg_3593" s="T311">biːr</ta>
            <ta e="T313" id="Seg_3594" s="T312">bert</ta>
            <ta e="T314" id="Seg_3595" s="T313">keri͡es-LAːK</ta>
            <ta e="T315" id="Seg_3596" s="T314">hep-BI-n</ta>
            <ta e="T316" id="Seg_3597" s="T315">bi͡er-I-mInA</ta>
            <ta e="T317" id="Seg_3598" s="T316">ɨːt-BIT</ta>
            <ta e="T318" id="Seg_3599" s="T317">ol-nI</ta>
            <ta e="T319" id="Seg_3600" s="T318">dogor-LArA</ta>
            <ta e="T320" id="Seg_3601" s="T319">et-BIT-LAr</ta>
            <ta e="T321" id="Seg_3602" s="T320">tu͡ok</ta>
            <ta e="T322" id="Seg_3603" s="T321">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T323" id="Seg_3604" s="T322">inʼe-tA</ta>
            <ta e="T324" id="Seg_3605" s="T323">kel-TAK-InA</ta>
            <ta e="T325" id="Seg_3606" s="T324">beje-tA</ta>
            <ta e="T326" id="Seg_3607" s="T325">egel-An</ta>
            <ta e="T327" id="Seg_3608" s="T326">bi͡er-IAK.[tA]</ta>
            <ta e="T328" id="Seg_3609" s="T327">bu͡ollaga</ta>
            <ta e="T329" id="Seg_3610" s="T328">diː</ta>
            <ta e="T330" id="Seg_3611" s="T329">dʼe</ta>
            <ta e="T331" id="Seg_3612" s="T330">bu</ta>
            <ta e="T332" id="Seg_3613" s="T331">e-BIT</ta>
            <ta e="T333" id="Seg_3614" s="T332">bu͡ollaga</ta>
            <ta e="T334" id="Seg_3615" s="T333">ogonnʼor</ta>
            <ta e="T335" id="Seg_3616" s="T334">kɨːs-tA</ta>
            <ta e="T336" id="Seg_3617" s="T335">di͡e-A</ta>
            <ta e="T337" id="Seg_3618" s="T336">hanaː-Ar</ta>
            <ta e="T338" id="Seg_3619" s="T337">utuj-BIT-tI-n</ta>
            <ta e="T339" id="Seg_3620" s="T338">genne</ta>
            <ta e="T340" id="Seg_3621" s="T339">kuːs-An</ta>
            <ta e="T341" id="Seg_3622" s="T340">baran</ta>
            <ta e="T342" id="Seg_3623" s="T341">dʼe</ta>
            <ta e="T343" id="Seg_3624" s="T342">köt-Ar</ta>
            <ta e="T344" id="Seg_3625" s="T343">diː</ta>
            <ta e="T345" id="Seg_3626" s="T344">ol</ta>
            <ta e="T346" id="Seg_3627" s="T345">biri͡eme-GA</ta>
            <ta e="T347" id="Seg_3628" s="T346">tukarɨ</ta>
            <ta e="T348" id="Seg_3629" s="T347">togus</ta>
            <ta e="T349" id="Seg_3630" s="T348">kün</ta>
            <ta e="T350" id="Seg_3631" s="T349">utuj-A</ta>
            <ta e="T351" id="Seg_3632" s="T350">hɨt-BIT</ta>
            <ta e="T352" id="Seg_3633" s="T351">dʼe</ta>
            <ta e="T353" id="Seg_3634" s="T352">kuːs-An</ta>
            <ta e="T354" id="Seg_3635" s="T353">baran</ta>
            <ta e="T355" id="Seg_3636" s="T354">ekkireː-An</ta>
            <ta e="T356" id="Seg_3637" s="T355">tur-Ar</ta>
            <ta e="T357" id="Seg_3638" s="T356">diː</ta>
            <ta e="T358" id="Seg_3639" s="T357">uhugun-An</ta>
            <ta e="T359" id="Seg_3640" s="T358">manna</ta>
            <ta e="T360" id="Seg_3641" s="T359">hüːr-An-LAr</ta>
            <ta e="T361" id="Seg_3642" s="T360">löčü͡ök-nI</ta>
            <ta e="T362" id="Seg_3643" s="T361">egel-Ar-LAr</ta>
            <ta e="T363" id="Seg_3644" s="T362">oŋu͡ok-LAr-tI-n</ta>
            <ta e="T364" id="Seg_3645" s="T363">egel-I-ŋ</ta>
            <ta e="T365" id="Seg_3646" s="T364">di͡e-A-di͡e-A</ta>
            <ta e="T366" id="Seg_3647" s="T365">kɨːr-An</ta>
            <ta e="T367" id="Seg_3648" s="T366">nʼühüj-Ar</ta>
            <ta e="T368" id="Seg_3649" s="T367">diː</ta>
            <ta e="T369" id="Seg_3650" s="T368">üs</ta>
            <ta e="T370" id="Seg_3651" s="T369">tüːn-LAːK</ta>
            <ta e="T371" id="Seg_3652" s="T370">kün-nI</ta>
            <ta e="T372" id="Seg_3653" s="T371">ölüː</ta>
            <ta e="T373" id="Seg_3654" s="T372">dojdu-tI-ttAn</ta>
            <ta e="T374" id="Seg_3655" s="T373">tönün-An</ta>
            <ta e="T375" id="Seg_3656" s="T374">ol-GA</ta>
            <ta e="T376" id="Seg_3657" s="T375">taba</ta>
            <ta e="T377" id="Seg_3658" s="T376">tiriː-tI-GAr</ta>
            <ta e="T378" id="Seg_3659" s="T377">egel-Ar-LAr</ta>
            <ta e="T379" id="Seg_3660" s="T378">oŋu͡ok-LArI-n</ta>
            <ta e="T380" id="Seg_3661" s="T379">kör-BIT-LArA</ta>
            <ta e="T381" id="Seg_3662" s="T380">kɨːs-nI</ta>
            <ta e="T382" id="Seg_3663" s="T381">kötök-An</ta>
            <ta e="T383" id="Seg_3664" s="T382">tur-Ar</ta>
            <ta e="T384" id="Seg_3665" s="T383">tolʼko</ta>
            <ta e="T385" id="Seg_3666" s="T384">kɨːs-LArA</ta>
            <ta e="T386" id="Seg_3667" s="T385">kamnaː-BAT</ta>
            <ta e="T387" id="Seg_3668" s="T386">öl-BIT</ta>
            <ta e="T388" id="Seg_3669" s="T387">du͡o</ta>
            <ta e="T389" id="Seg_3670" s="T388">utuj-BIT</ta>
            <ta e="T390" id="Seg_3671" s="T389">du͡o</ta>
            <ta e="T391" id="Seg_3672" s="T390">kördük</ta>
            <ta e="T392" id="Seg_3673" s="T391">bu</ta>
            <ta e="T393" id="Seg_3674" s="T392">kɨːs</ta>
            <ta e="T394" id="Seg_3675" s="T393">oŋu͡ok-LArI-n</ta>
            <ta e="T395" id="Seg_3676" s="T394">is-tI-GAr</ta>
            <ta e="T396" id="Seg_3677" s="T395">hokuj-LIː</ta>
            <ta e="T397" id="Seg_3678" s="T396">kaːlaː-Ar</ta>
            <ta e="T398" id="Seg_3679" s="T397">diː</ta>
            <ta e="T399" id="Seg_3680" s="T398">bu-tI-tA</ta>
            <ta e="T400" id="Seg_3681" s="T399">tilin-An</ta>
            <ta e="T401" id="Seg_3682" s="T400">kel-An</ta>
            <ta e="T402" id="Seg_3683" s="T401">dʼe</ta>
            <ta e="T403" id="Seg_3684" s="T402">bu</ta>
            <ta e="T404" id="Seg_3685" s="T403">ogonnʼor-I-ŋ</ta>
            <ta e="T405" id="Seg_3686" s="T404">kɨːs-tA</ta>
            <ta e="T406" id="Seg_3687" s="T405">bu͡ol-Ar</ta>
            <ta e="T407" id="Seg_3688" s="T406">bu-nI</ta>
            <ta e="T408" id="Seg_3689" s="T407">ičeːn-I-ŋ</ta>
            <ta e="T409" id="Seg_3690" s="T408">dʼaktar</ta>
            <ta e="T410" id="Seg_3691" s="T409">gɨn-An</ta>
            <ta e="T411" id="Seg_3692" s="T410">olor-Ar</ta>
            <ta e="T412" id="Seg_3693" s="T411">kaːl-AːččI</ta>
            <ta e="T413" id="Seg_3694" s="T412">ikki</ta>
            <ta e="T414" id="Seg_3695" s="T413">dogor-LArA</ta>
            <ta e="T415" id="Seg_3696" s="T414">üŋküː-GA</ta>
            <ta e="T416" id="Seg_3697" s="T415">ɨgɨr-AːrI</ta>
            <ta e="T417" id="Seg_3698" s="T416">gɨn-BIT-LAr</ta>
            <ta e="T418" id="Seg_3699" s="T417">kihi-LArA</ta>
            <ta e="T419" id="Seg_3700" s="T418">hu͡ok</ta>
            <ta e="T420" id="Seg_3701" s="T419">bil-BIT-LArA</ta>
            <ta e="T421" id="Seg_3702" s="T420">ojun</ta>
            <ta e="T422" id="Seg_3703" s="T421">ilt-BIT</ta>
            <ta e="T423" id="Seg_3704" s="T422">bu-nI</ta>
            <ta e="T424" id="Seg_3705" s="T423">dʼe</ta>
            <ta e="T425" id="Seg_3706" s="T424">kɨjgan-Ar-LAr</ta>
            <ta e="T426" id="Seg_3707" s="T425">tilin-AːččI-GI-n</ta>
            <ta e="T427" id="Seg_3708" s="T426">bul-An</ta>
            <ta e="T428" id="Seg_3709" s="T427">hanaː-tI-n</ta>
            <ta e="T429" id="Seg_3710" s="T428">erij-An</ta>
            <ta e="T430" id="Seg_3711" s="T429">ü͡ör</ta>
            <ta e="T431" id="Seg_3712" s="T430">bu͡ol-An</ta>
            <ta e="T432" id="Seg_3713" s="T431">udagan</ta>
            <ta e="T433" id="Seg_3714" s="T432">gɨn-Ar-LAr</ta>
            <ta e="T434" id="Seg_3715" s="T433">udagan</ta>
            <ta e="T435" id="Seg_3716" s="T434">gɨn-AːrI</ta>
            <ta e="T436" id="Seg_3717" s="T435">kaːj-Ar-LAr</ta>
            <ta e="T437" id="Seg_3718" s="T436">bihigi</ta>
            <ta e="T438" id="Seg_3719" s="T437">en-n</ta>
            <ta e="T439" id="Seg_3720" s="T438">hi͡e-IAK-BIt</ta>
            <ta e="T440" id="Seg_3721" s="T439">emeːksin</ta>
            <ta e="T441" id="Seg_3722" s="T440">tiriː-tI-n</ta>
            <ta e="T442" id="Seg_3723" s="T441">hül-An-GIn</ta>
            <ta e="T443" id="Seg_3724" s="T442">düŋür</ta>
            <ta e="T444" id="Seg_3725" s="T443">oŋohun-I-BA-TAK-GInA</ta>
            <ta e="T445" id="Seg_3726" s="T444">dʼe</ta>
            <ta e="T446" id="Seg_3727" s="T445">onno</ta>
            <ta e="T447" id="Seg_3728" s="T446">biːr</ta>
            <ta e="T448" id="Seg_3729" s="T447">emeːksin-nI</ta>
            <ta e="T449" id="Seg_3730" s="T448">kastaː-An-LAr</ta>
            <ta e="T450" id="Seg_3731" s="T449">ol</ta>
            <ta e="T451" id="Seg_3732" s="T450">tiriː-tI-nAn</ta>
            <ta e="T452" id="Seg_3733" s="T451">düŋür</ta>
            <ta e="T453" id="Seg_3734" s="T452">oŋohun-Ar-LAr</ta>
            <ta e="T454" id="Seg_3735" s="T453">ol</ta>
            <ta e="T455" id="Seg_3736" s="T454">dom-tI-GAr</ta>
            <ta e="T456" id="Seg_3737" s="T455">iti</ta>
            <ta e="T457" id="Seg_3738" s="T456">ɨ͡arɨj-Ar</ta>
            <ta e="T458" id="Seg_3739" s="T457">bu͡olluŋ</ta>
            <ta e="T459" id="Seg_3740" s="T458">da</ta>
            <ta e="T460" id="Seg_3741" s="T459">haːmaj</ta>
            <ta e="T461" id="Seg_3742" s="T460">biːr-LIː</ta>
            <ta e="T462" id="Seg_3743" s="T461">öl-BAT</ta>
            <ta e="T463" id="Seg_3744" s="T462">haːmaj</ta>
            <ta e="T464" id="Seg_3745" s="T463">orda-nAn</ta>
            <ta e="T465" id="Seg_3746" s="T464">baran-Ar</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_3747" s="T0">long.ago</ta>
            <ta e="T2" id="Seg_3748" s="T1">one</ta>
            <ta e="T3" id="Seg_3749" s="T2">Nganasan</ta>
            <ta e="T4" id="Seg_3750" s="T3">old.man-3SG.[NOM]</ta>
            <ta e="T5" id="Seg_3751" s="T4">live-PST2.[3SG]</ta>
            <ta e="T6" id="Seg_3752" s="T5">daughter-PROPR.[NOM]</ta>
            <ta e="T7" id="Seg_3753" s="T6">this</ta>
            <ta e="T8" id="Seg_3754" s="T7">daughter-3SG.[NOM]</ta>
            <ta e="T9" id="Seg_3755" s="T8">grow-CVB.SEQ</ta>
            <ta e="T10" id="Seg_3756" s="T9">housewife.[NOM]</ta>
            <ta e="T11" id="Seg_3757" s="T10">become-CVB.SEQ</ta>
            <ta e="T12" id="Seg_3758" s="T11">after</ta>
            <ta e="T13" id="Seg_3759" s="T12">die-CVB.SEQ</ta>
            <ta e="T14" id="Seg_3760" s="T13">stay-PST2.[3SG]</ta>
            <ta e="T15" id="Seg_3761" s="T14">this-ACC</ta>
            <ta e="T16" id="Seg_3762" s="T15">father-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_3763" s="T16">very-ADVZ</ta>
            <ta e="T18" id="Seg_3764" s="T17">cry-PRS.[3SG]</ta>
            <ta e="T19" id="Seg_3765" s="T18">bury-CVB.PURP</ta>
            <ta e="T20" id="Seg_3766" s="T19">wollen-PTCP.PST-3PL-ACC</ta>
            <ta e="T21" id="Seg_3767" s="T20">speak-PRS.[3SG]</ta>
            <ta e="T22" id="Seg_3768" s="T21">1SG.[NOM]</ta>
            <ta e="T23" id="Seg_3769" s="T22">daughter-1SG-ACC</ta>
            <ta e="T24" id="Seg_3770" s="T23">always</ta>
            <ta e="T25" id="Seg_3771" s="T24">self-1SG-ACC</ta>
            <ta e="T26" id="Seg_3772" s="T25">with</ta>
            <ta e="T27" id="Seg_3773" s="T26">load-CVB.SIM</ta>
            <ta e="T28" id="Seg_3774" s="T27">go-FUT-1SG</ta>
            <ta e="T29" id="Seg_3775" s="T28">3SG.[NOM]</ta>
            <ta e="T30" id="Seg_3776" s="T29">revive-FUT-3SG</ta>
            <ta e="T31" id="Seg_3777" s="T30">big</ta>
            <ta e="T32" id="Seg_3778" s="T31">shaman-DAT/LOC</ta>
            <ta e="T33" id="Seg_3779" s="T32">revive-CAUS-FUT-1SG</ta>
            <ta e="T34" id="Seg_3780" s="T33">say-PRS.[3SG]</ta>
            <ta e="T35" id="Seg_3781" s="T34">well</ta>
            <ta e="T36" id="Seg_3782" s="T35">daughter-3SG-ACC</ta>
            <ta e="T37" id="Seg_3783" s="T36">sledge-DAT/LOC</ta>
            <ta e="T38" id="Seg_3784" s="T37">load-CVB.SIM</ta>
            <ta e="T39" id="Seg_3785" s="T38">go-PRS.[3SG]</ta>
            <ta e="T40" id="Seg_3786" s="T39">that</ta>
            <ta e="T41" id="Seg_3787" s="T40">drive-CVB.SEQ</ta>
            <ta e="T42" id="Seg_3788" s="T41">shaman-PL-ACC</ta>
            <ta e="T43" id="Seg_3789" s="T42">beg-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T44" id="Seg_3790" s="T43">one-3PL.[NOM]</ta>
            <ta e="T45" id="Seg_3791" s="T44">NEG</ta>
            <ta e="T46" id="Seg_3792" s="T45">be-NEG.[3SG]</ta>
            <ta e="T47" id="Seg_3793" s="T46">only</ta>
            <ta e="T48" id="Seg_3794" s="T47">two</ta>
            <ta e="T49" id="Seg_3795" s="T48">big</ta>
            <ta e="T50" id="Seg_3796" s="T49">shaman-ACC</ta>
            <ta e="T51" id="Seg_3797" s="T50">shamanize-CAUS-PRS.[3SG]</ta>
            <ta e="T52" id="Seg_3798" s="T51">two</ta>
            <ta e="T53" id="Seg_3799" s="T52">year-3SG.[NOM]</ta>
            <ta e="T54" id="Seg_3800" s="T53">pass.by-PTCP.PST-3SG-ACC</ta>
            <ta e="T55" id="Seg_3801" s="T54">after</ta>
            <ta e="T56" id="Seg_3802" s="T55">girl-3SG-GEN</ta>
            <ta e="T57" id="Seg_3803" s="T56">body-3SG.[NOM]</ta>
            <ta e="T58" id="Seg_3804" s="T57">long.ago</ta>
            <ta e="T59" id="Seg_3805" s="T58">rot-CVB.SEQ</ta>
            <ta e="T60" id="Seg_3806" s="T59">bone-3PL.[NOM]</ta>
            <ta e="T61" id="Seg_3807" s="T60">just</ta>
            <ta e="T62" id="Seg_3808" s="T61">stay-PST2.[3SG]</ta>
            <ta e="T63" id="Seg_3809" s="T62">this</ta>
            <ta e="T64" id="Seg_3810" s="T63">two</ta>
            <ta e="T65" id="Seg_3811" s="T64">shaman.[NOM]</ta>
            <ta e="T66" id="Seg_3812" s="T65">shamanize-CVB.SEQ-3PL</ta>
            <ta e="T67" id="Seg_3813" s="T66">what-ACC</ta>
            <ta e="T68" id="Seg_3814" s="T67">NEG</ta>
            <ta e="T69" id="Seg_3815" s="T68">make-NEG-3PL</ta>
            <ta e="T70" id="Seg_3816" s="T69">three-ORD</ta>
            <ta e="T71" id="Seg_3817" s="T70">year-3SG.[NOM]</ta>
            <ta e="T72" id="Seg_3818" s="T71">become-PRS.[3SG]</ta>
            <ta e="T73" id="Seg_3819" s="T72">that</ta>
            <ta e="T74" id="Seg_3820" s="T73">Nganasan.[NOM]</ta>
            <ta e="T75" id="Seg_3821" s="T74">proximity-3SG-DAT/LOC</ta>
            <ta e="T76" id="Seg_3822" s="T75">clairvoyant.[NOM]</ta>
            <ta e="T77" id="Seg_3823" s="T76">there.is</ta>
            <ta e="T78" id="Seg_3824" s="T77">be-PST2.[3SG]</ta>
            <ta e="T79" id="Seg_3825" s="T78">this</ta>
            <ta e="T80" id="Seg_3826" s="T79">clairvoyant.[NOM]</ta>
            <ta e="T81" id="Seg_3827" s="T80">very</ta>
            <ta e="T82" id="Seg_3828" s="T81">feel.sorry-PRS.[3SG]</ta>
            <ta e="T83" id="Seg_3829" s="T82">Nganasan-3SG-ACC</ta>
            <ta e="T84" id="Seg_3830" s="T83">once</ta>
            <ta e="T85" id="Seg_3831" s="T84">dream-PRS.[3SG]</ta>
            <ta e="T86" id="Seg_3832" s="T85">that</ta>
            <ta e="T87" id="Seg_3833" s="T86">dream-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T88" id="Seg_3834" s="T87">after</ta>
            <ta e="T89" id="Seg_3835" s="T88">Nganasan-3SG-DAT/LOC</ta>
            <ta e="T90" id="Seg_3836" s="T89">come-CVB.SEQ</ta>
            <ta e="T91" id="Seg_3837" s="T90">tell-PRS.[3SG]</ta>
            <ta e="T92" id="Seg_3838" s="T91">this</ta>
            <ta e="T93" id="Seg_3839" s="T92">2SG.[NOM]</ta>
            <ta e="T94" id="Seg_3840" s="T93">daughter-EP-2SG.[NOM]</ta>
            <ta e="T95" id="Seg_3841" s="T94">revive-FUT.[3SG]</ta>
            <ta e="T96" id="Seg_3842" s="T95">apparently</ta>
            <ta e="T97" id="Seg_3843" s="T96">miracle-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_3844" s="T97">one</ta>
            <ta e="T99" id="Seg_3845" s="T98">be-PST1-3SG</ta>
            <ta e="T100" id="Seg_3846" s="T99">child-2SG.[NOM]</ta>
            <ta e="T101" id="Seg_3847" s="T100">soul-3SG-ACC</ta>
            <ta e="T102" id="Seg_3848" s="T101">eat-PTCP.PST</ta>
            <ta e="T103" id="Seg_3849" s="T102">spirit-3SG.[NOM]</ta>
            <ta e="T104" id="Seg_3850" s="T103">sea-DAT/LOC</ta>
            <ta e="T105" id="Seg_3851" s="T104">death.[NOM]</ta>
            <ta e="T106" id="Seg_3852" s="T105">water-3SG-DAT/LOC</ta>
            <ta e="T107" id="Seg_3853" s="T106">carry-CVB.SIM</ta>
            <ta e="T108" id="Seg_3854" s="T107">go-PTCP.PRS</ta>
            <ta e="T109" id="Seg_3855" s="T108">be-PST2.[3SG]</ta>
            <ta e="T110" id="Seg_3856" s="T109">one.out.of.two</ta>
            <ta e="T111" id="Seg_3857" s="T110">shaman-EP-2SG.[NOM]</ta>
            <ta e="T112" id="Seg_3858" s="T111">wild.reindeer.[NOM]</ta>
            <ta e="T113" id="Seg_3859" s="T112">become-CVB.SEQ</ta>
            <ta e="T114" id="Seg_3860" s="T113">earth-EP-INSTR</ta>
            <ta e="T115" id="Seg_3861" s="T114">follow-CVB.SEQ</ta>
            <ta e="T116" id="Seg_3862" s="T115">animal.hair-3SG.[NOM]</ta>
            <ta e="T117" id="Seg_3863" s="T116">icing.[NOM]</ta>
            <ta e="T118" id="Seg_3864" s="T117">hole-DAT/LOC</ta>
            <ta e="T119" id="Seg_3865" s="T118">fit-NEG.PTCP</ta>
            <ta e="T120" id="Seg_3866" s="T119">be-PRS.[3SG]</ta>
            <ta e="T121" id="Seg_3867" s="T120">one.out.of.two.[NOM]</ta>
            <ta e="T122" id="Seg_3868" s="T121">wild.reindeer.[NOM]</ta>
            <ta e="T123" id="Seg_3869" s="T122">become-CVB.SEQ</ta>
            <ta e="T124" id="Seg_3870" s="T123">water-INSTR</ta>
            <ta e="T125" id="Seg_3871" s="T124">flow-CVB.SEQ</ta>
            <ta e="T126" id="Seg_3872" s="T125">reach-TEMP-3SG</ta>
            <ta e="T127" id="Seg_3873" s="T126">wave-3SG.[NOM]</ta>
            <ta e="T128" id="Seg_3874" s="T127">catch-CVB.PURP</ta>
            <ta e="T129" id="Seg_3875" s="T128">just</ta>
            <ta e="T130" id="Seg_3876" s="T129">make-TEMP-3SG</ta>
            <ta e="T131" id="Seg_3877" s="T130">soul-3SG-ACC</ta>
            <ta e="T132" id="Seg_3878" s="T131">carry-CVB.SEQ</ta>
            <ta e="T133" id="Seg_3879" s="T132">throw-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_3880" s="T133">nine</ta>
            <ta e="T135" id="Seg_3881" s="T134">year.[NOM]</ta>
            <ta e="T136" id="Seg_3882" s="T135">like.that</ta>
            <ta e="T137" id="Seg_3883" s="T136">follow-EP-RECP/COLL-FUT-3PL</ta>
            <ta e="T138" id="Seg_3884" s="T137">then</ta>
            <ta e="T139" id="Seg_3885" s="T138">how</ta>
            <ta e="T140" id="Seg_3886" s="T139">be-PRS.[3SG]</ta>
            <ta e="T141" id="Seg_3887" s="T140">that-DAT/LOC</ta>
            <ta e="T142" id="Seg_3888" s="T141">Nganasan</ta>
            <ta e="T143" id="Seg_3889" s="T142">old.man-3SG.[NOM]</ta>
            <ta e="T144" id="Seg_3890" s="T143">speak-PRS.[3SG]</ta>
            <ta e="T145" id="Seg_3891" s="T144">that</ta>
            <ta e="T146" id="Seg_3892" s="T145">shaman.[NOM]</ta>
            <ta e="T147" id="Seg_3893" s="T146">can-NEG.PTCP-3SG-ACC</ta>
            <ta e="T148" id="Seg_3894" s="T147">know-PTCP.PRS</ta>
            <ta e="T149" id="Seg_3895" s="T148">human.being.[NOM]</ta>
            <ta e="T150" id="Seg_3896" s="T149">self-2SG.[NOM]</ta>
            <ta e="T151" id="Seg_3897" s="T150">why</ta>
            <ta e="T152" id="Seg_3898" s="T151">revive-CAUS-CVB.SEQ</ta>
            <ta e="T153" id="Seg_3899" s="T152">try-NEG-2SG</ta>
            <ta e="T154" id="Seg_3900" s="T153">say-PRS.[3SG]</ta>
            <ta e="T155" id="Seg_3901" s="T154">beg-PRS.[3SG]</ta>
            <ta e="T156" id="Seg_3902" s="T155">well</ta>
            <ta e="T157" id="Seg_3903" s="T156">very</ta>
            <ta e="T158" id="Seg_3904" s="T157">misery-VBZ-PST1-2SG</ta>
            <ta e="T159" id="Seg_3905" s="T158">say-PRS.[3SG]</ta>
            <ta e="T160" id="Seg_3906" s="T159">how</ta>
            <ta e="T161" id="Seg_3907" s="T160">make-PRS-1SG</ta>
            <ta e="T162" id="Seg_3908" s="T161">revive-TEMP-3SG</ta>
            <ta e="T163" id="Seg_3909" s="T162">1SG-DAT/LOC</ta>
            <ta e="T164" id="Seg_3910" s="T163">woman.[NOM]</ta>
            <ta e="T165" id="Seg_3911" s="T164">give-FUT-2SG</ta>
            <ta e="T166" id="Seg_3912" s="T165">Q</ta>
            <ta e="T167" id="Seg_3913" s="T166">wealth-EP-1SG.[NOM]</ta>
            <ta e="T168" id="Seg_3914" s="T167">half-3SG-ACC</ta>
            <ta e="T169" id="Seg_3915" s="T168">give-FUT-1SG</ta>
            <ta e="T170" id="Seg_3916" s="T169">self-3SG-ACC</ta>
            <ta e="T171" id="Seg_3917" s="T170">give-FUT-1SG</ta>
            <ta e="T172" id="Seg_3918" s="T171">revive-CAUS.[IMP.2SG]</ta>
            <ta e="T173" id="Seg_3919" s="T172">just</ta>
            <ta e="T174" id="Seg_3920" s="T173">say-PRS.[3SG]</ta>
            <ta e="T175" id="Seg_3921" s="T174">clairvoyant-3SG.[NOM]</ta>
            <ta e="T176" id="Seg_3922" s="T175">sleep-CVB.SEQ</ta>
            <ta e="T177" id="Seg_3923" s="T176">stay-PRS.[3SG]</ta>
            <ta e="T178" id="Seg_3924" s="T177">this</ta>
            <ta e="T179" id="Seg_3925" s="T178">sleep-CVB.SIM</ta>
            <ta e="T180" id="Seg_3926" s="T179">lie-CVB.SEQ</ta>
            <ta e="T181" id="Seg_3927" s="T180">well</ta>
            <ta e="T182" id="Seg_3928" s="T181">that</ta>
            <ta e="T183" id="Seg_3929" s="T182">death-3SG-GEN</ta>
            <ta e="T184" id="Seg_3930" s="T183">place-3SG-DAT/LOC</ta>
            <ta e="T185" id="Seg_3931" s="T184">reach-PRS.[3SG]</ta>
            <ta e="T186" id="Seg_3932" s="T185">well</ta>
            <ta e="T187" id="Seg_3933" s="T186">that</ta>
            <ta e="T188" id="Seg_3934" s="T187">death-3SG-GEN</ta>
            <ta e="T189" id="Seg_3935" s="T188">place-3SG-DAT/LOC</ta>
            <ta e="T190" id="Seg_3936" s="T189">reach-PRS.[3SG]</ta>
            <ta e="T191" id="Seg_3937" s="T190">reach-PST2-3SG</ta>
            <ta e="T192" id="Seg_3938" s="T191">many</ta>
            <ta e="T193" id="Seg_3939" s="T192">very</ta>
            <ta e="T194" id="Seg_3940" s="T193">people.[NOM]</ta>
            <ta e="T195" id="Seg_3941" s="T194">dance-CVB.SIM</ta>
            <ta e="T196" id="Seg_3942" s="T195">stand-PRS-3PL</ta>
            <ta e="T197" id="Seg_3943" s="T196">here</ta>
            <ta e="T198" id="Seg_3944" s="T197">search-CVB.SIM</ta>
            <ta e="T199" id="Seg_3945" s="T198">do.in.vain-PRS.[3SG]</ta>
            <ta e="T200" id="Seg_3946" s="T199">girl-3PL-GEN</ta>
            <ta e="T201" id="Seg_3947" s="T200">soul-3SG-ACC</ta>
            <ta e="T202" id="Seg_3948" s="T201">what.[NOM]</ta>
            <ta e="T203" id="Seg_3949" s="T202">come-FUT.[3SG]=Q</ta>
            <ta e="T204" id="Seg_3950" s="T203">at.all</ta>
            <ta e="T205" id="Seg_3951" s="T204">find-NEG.[3SG]</ta>
            <ta e="T206" id="Seg_3952" s="T205">then</ta>
            <ta e="T207" id="Seg_3953" s="T206">this</ta>
            <ta e="T208" id="Seg_3954" s="T207">place-ABL</ta>
            <ta e="T209" id="Seg_3955" s="T208">different</ta>
            <ta e="T210" id="Seg_3956" s="T209">place-DAT/LOC</ta>
            <ta e="T211" id="Seg_3957" s="T210">go-IMP.1SG</ta>
            <ta e="T212" id="Seg_3958" s="T211">think-PRS.[3SG]</ta>
            <ta e="T213" id="Seg_3959" s="T212">reach-PRS.[3SG]</ta>
            <ta e="T214" id="Seg_3960" s="T213">one</ta>
            <ta e="T215" id="Seg_3961" s="T214">place-DAT/LOC</ta>
            <ta e="T216" id="Seg_3962" s="T215">there</ta>
            <ta e="T217" id="Seg_3963" s="T216">three</ta>
            <ta e="T218" id="Seg_3964" s="T217">EMPH=EMPH</ta>
            <ta e="T219" id="Seg_3965" s="T218">identical</ta>
            <ta e="T220" id="Seg_3966" s="T219">face-PROPR</ta>
            <ta e="T221" id="Seg_3967" s="T220">clothes-PROPR</ta>
            <ta e="T222" id="Seg_3968" s="T221">girl.[NOM]</ta>
            <ta e="T223" id="Seg_3969" s="T222">there.is</ta>
            <ta e="T224" id="Seg_3970" s="T223">be-PST2.[3SG]</ta>
            <ta e="T225" id="Seg_3971" s="T224">this-ABL</ta>
            <ta e="T226" id="Seg_3972" s="T225">other-3PL.[NOM]</ta>
            <ta e="T227" id="Seg_3973" s="T226">old.man.[NOM]</ta>
            <ta e="T228" id="Seg_3974" s="T227">daughter-3SG.[NOM]</ta>
            <ta e="T229" id="Seg_3975" s="T228">what.kind.of-3PL.[NOM]</ta>
            <ta e="T230" id="Seg_3976" s="T229">that</ta>
            <ta e="T231" id="Seg_3977" s="T230">daughter-3SG-ACC</ta>
            <ta e="T232" id="Seg_3978" s="T231">can-CVB.SEQ</ta>
            <ta e="T233" id="Seg_3979" s="T232">get.to.know-NEG.[3SG]</ta>
            <ta e="T234" id="Seg_3980" s="T233">clothes-3PL-GEN</ta>
            <ta e="T235" id="Seg_3981" s="T234">seam-3SG-ACC</ta>
            <ta e="T236" id="Seg_3982" s="T235">count-CVB.SEQ</ta>
            <ta e="T237" id="Seg_3983" s="T236">try-PRS.[3SG]</ta>
            <ta e="T238" id="Seg_3984" s="T237">amount-3SG.[NOM]</ta>
            <ta e="T239" id="Seg_3985" s="T238">completely</ta>
            <ta e="T240" id="Seg_3986" s="T239">one</ta>
            <ta e="T241" id="Seg_3987" s="T240">misery-VBZ-CVB.SIM</ta>
            <ta e="T242" id="Seg_3988" s="T241">do.in.vain-PRS.[3SG]</ta>
            <ta e="T243" id="Seg_3989" s="T242">this</ta>
            <ta e="T244" id="Seg_3990" s="T243">girl-PL.[NOM]</ta>
            <ta e="T245" id="Seg_3991" s="T244">three</ta>
            <ta e="T246" id="Seg_3992" s="T245">house-PROPR-3PL</ta>
            <ta e="T247" id="Seg_3993" s="T246">side.[NOM]</ta>
            <ta e="T248" id="Seg_3994" s="T247">different.[NOM]</ta>
            <ta e="T249" id="Seg_3995" s="T248">thither</ta>
            <ta e="T250" id="Seg_3996" s="T249">in.the.evening</ta>
            <ta e="T251" id="Seg_3997" s="T250">dance-ABL</ta>
            <ta e="T252" id="Seg_3998" s="T251">come-CVB.SEQ</ta>
            <ta e="T253" id="Seg_3999" s="T252">sleep-CVB.SEQ</ta>
            <ta e="T254" id="Seg_4000" s="T253">stay-PRS-3PL</ta>
            <ta e="T255" id="Seg_4001" s="T254">girl-PL-3SG-ACC</ta>
            <ta e="T256" id="Seg_4002" s="T255">with</ta>
            <ta e="T257" id="Seg_4003" s="T256">chat-CVB.PURP</ta>
            <ta e="T258" id="Seg_4004" s="T257">want-PRS.[3SG]</ta>
            <ta e="T259" id="Seg_4005" s="T258">other-3PL.[NOM]</ta>
            <ta e="T260" id="Seg_4006" s="T259">NEG</ta>
            <ta e="T261" id="Seg_4007" s="T260">speak-NEG.[3SG]</ta>
            <ta e="T262" id="Seg_4008" s="T261">how</ta>
            <ta e="T263" id="Seg_4009" s="T262">become-FUT-1SG=Q</ta>
            <ta e="T264" id="Seg_4010" s="T263">think-CVB.SEQ</ta>
            <ta e="T265" id="Seg_4011" s="T264">reflect-PRS.[3SG]</ta>
            <ta e="T266" id="Seg_4012" s="T265">other-3PL-GEN</ta>
            <ta e="T267" id="Seg_4013" s="T266">house-3SG-DAT/LOC</ta>
            <ta e="T268" id="Seg_4014" s="T267">hide-CVB.SEQ</ta>
            <ta e="T269" id="Seg_4015" s="T268">go.in-CVB.SEQ</ta>
            <ta e="T270" id="Seg_4016" s="T269">sleep-PTCP.PST-3SG-ACC</ta>
            <ta e="T271" id="Seg_4017" s="T270">observe-PRS.[3SG]</ta>
            <ta e="T272" id="Seg_4018" s="T271">like.this</ta>
            <ta e="T273" id="Seg_4019" s="T272">three</ta>
            <ta e="T274" id="Seg_4020" s="T273">house-DAT/LOC</ta>
            <ta e="T275" id="Seg_4021" s="T274">three-COLL-3PL-DAT/LOC</ta>
            <ta e="T276" id="Seg_4022" s="T275">overnight-PST1-3SG</ta>
            <ta e="T277" id="Seg_4023" s="T276">and</ta>
            <ta e="T278" id="Seg_4024" s="T277">what-ACC</ta>
            <ta e="T279" id="Seg_4025" s="T278">NEG</ta>
            <ta e="T280" id="Seg_4026" s="T279">find-CVB.SEQ</ta>
            <ta e="T281" id="Seg_4027" s="T280">take-NEG-PST1-3SG</ta>
            <ta e="T282" id="Seg_4028" s="T281">well</ta>
            <ta e="T283" id="Seg_4029" s="T282">once</ta>
            <ta e="T284" id="Seg_4030" s="T283">three-COLL</ta>
            <ta e="T285" id="Seg_4031" s="T284">one</ta>
            <ta e="T286" id="Seg_4032" s="T285">house-DAT/LOC</ta>
            <ta e="T287" id="Seg_4033" s="T286">gather.oneself-REFL-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T288" id="Seg_4034" s="T287">be-PST2.[3SG]</ta>
            <ta e="T289" id="Seg_4035" s="T288">hither</ta>
            <ta e="T290" id="Seg_4036" s="T289">go.in-CVB.SEQ</ta>
            <ta e="T291" id="Seg_4037" s="T290">clairvoyant.[NOM]</ta>
            <ta e="T292" id="Seg_4038" s="T291">hide-CVB.SIM</ta>
            <ta e="T293" id="Seg_4039" s="T292">lie-PST2.[3SG]</ta>
            <ta e="T294" id="Seg_4040" s="T293">this</ta>
            <ta e="T295" id="Seg_4041" s="T294">lie-TEMP-3SG</ta>
            <ta e="T296" id="Seg_4042" s="T295">one</ta>
            <ta e="T297" id="Seg_4043" s="T296">girl.[NOM]</ta>
            <ta e="T298" id="Seg_4044" s="T297">only</ta>
            <ta e="T299" id="Seg_4045" s="T298">think.about-PST2.[3SG]</ta>
            <ta e="T300" id="Seg_4046" s="T299">this-ACC</ta>
            <ta e="T301" id="Seg_4047" s="T300">two</ta>
            <ta e="T302" id="Seg_4048" s="T301">friend-3SG.[NOM]</ta>
            <ta e="T303" id="Seg_4049" s="T302">ask-PST2-3PL</ta>
            <ta e="T304" id="Seg_4050" s="T303">what.[NOM]</ta>
            <ta e="T305" id="Seg_4051" s="T304">be-CVB.SEQ</ta>
            <ta e="T306" id="Seg_4052" s="T305">think.about-PRS-2SG</ta>
            <ta e="T307" id="Seg_4053" s="T306">say-CVB.SEQ</ta>
            <ta e="T308" id="Seg_4054" s="T307">girl-3PL.[NOM]</ta>
            <ta e="T309" id="Seg_4055" s="T308">speak-PST2.[3SG]</ta>
            <ta e="T310" id="Seg_4056" s="T309">1SG.[NOM]</ta>
            <ta e="T311" id="Seg_4057" s="T310">mother-1SG.[NOM]</ta>
            <ta e="T312" id="Seg_4058" s="T311">one</ta>
            <ta e="T313" id="Seg_4059" s="T312">very</ta>
            <ta e="T314" id="Seg_4060" s="T313">legacy-PROPR</ta>
            <ta e="T315" id="Seg_4061" s="T314">thing-1SG-ACC</ta>
            <ta e="T316" id="Seg_4062" s="T315">give-EP-NEG.CVB</ta>
            <ta e="T317" id="Seg_4063" s="T316">send-PST2.[3SG]</ta>
            <ta e="T318" id="Seg_4064" s="T317">that-ACC</ta>
            <ta e="T319" id="Seg_4065" s="T318">friend-3PL.[NOM]</ta>
            <ta e="T320" id="Seg_4066" s="T319">speak-PST2-3PL</ta>
            <ta e="T321" id="Seg_4067" s="T320">what</ta>
            <ta e="T322" id="Seg_4068" s="T321">be-FUT.[3SG]=Q</ta>
            <ta e="T323" id="Seg_4069" s="T322">mother-3SG.[NOM]</ta>
            <ta e="T324" id="Seg_4070" s="T323">come-TEMP-3SG</ta>
            <ta e="T325" id="Seg_4071" s="T324">self-3SG.[NOM]</ta>
            <ta e="T326" id="Seg_4072" s="T325">bring-CVB.SEQ</ta>
            <ta e="T327" id="Seg_4073" s="T326">give-FUT.[3SG]</ta>
            <ta e="T328" id="Seg_4074" s="T327">MOD</ta>
            <ta e="T329" id="Seg_4075" s="T328">EMPH</ta>
            <ta e="T330" id="Seg_4076" s="T329">well</ta>
            <ta e="T331" id="Seg_4077" s="T330">this</ta>
            <ta e="T332" id="Seg_4078" s="T331">be-PST2.[3SG]</ta>
            <ta e="T333" id="Seg_4079" s="T332">MOD</ta>
            <ta e="T334" id="Seg_4080" s="T333">old.man.[NOM]</ta>
            <ta e="T335" id="Seg_4081" s="T334">daughter-3SG.[NOM]</ta>
            <ta e="T336" id="Seg_4082" s="T335">think-CVB.SIM</ta>
            <ta e="T337" id="Seg_4083" s="T336">think-PRS.[3SG]</ta>
            <ta e="T338" id="Seg_4084" s="T337">fall.asleep-PTCP.PST-3SG-ACC</ta>
            <ta e="T339" id="Seg_4085" s="T338">after</ta>
            <ta e="T340" id="Seg_4086" s="T339">embrace-CVB.SEQ</ta>
            <ta e="T341" id="Seg_4087" s="T340">after</ta>
            <ta e="T342" id="Seg_4088" s="T341">well</ta>
            <ta e="T343" id="Seg_4089" s="T342">fly-PRS.[3SG]</ta>
            <ta e="T344" id="Seg_4090" s="T343">EMPH</ta>
            <ta e="T345" id="Seg_4091" s="T344">that</ta>
            <ta e="T346" id="Seg_4092" s="T345">time-DAT/LOC</ta>
            <ta e="T347" id="Seg_4093" s="T346">while</ta>
            <ta e="T348" id="Seg_4094" s="T347">nine</ta>
            <ta e="T349" id="Seg_4095" s="T348">day.[NOM]</ta>
            <ta e="T350" id="Seg_4096" s="T349">sleep-CVB.SIM</ta>
            <ta e="T351" id="Seg_4097" s="T350">lie-PST2.[3SG]</ta>
            <ta e="T352" id="Seg_4098" s="T351">well</ta>
            <ta e="T353" id="Seg_4099" s="T352">embrace-CVB.SEQ</ta>
            <ta e="T354" id="Seg_4100" s="T353">after</ta>
            <ta e="T355" id="Seg_4101" s="T354">jump-CVB.SEQ</ta>
            <ta e="T356" id="Seg_4102" s="T355">stand-PRS.[3SG]</ta>
            <ta e="T357" id="Seg_4103" s="T356">EMPH</ta>
            <ta e="T358" id="Seg_4104" s="T357">wake.up-CVB.SEQ</ta>
            <ta e="T359" id="Seg_4105" s="T358">here</ta>
            <ta e="T360" id="Seg_4106" s="T359">run-CVB.SEQ-3PL</ta>
            <ta e="T361" id="Seg_4107" s="T360">assistant-ACC</ta>
            <ta e="T362" id="Seg_4108" s="T361">bring-PRS-3PL</ta>
            <ta e="T363" id="Seg_4109" s="T362">bone-PL-3SG-ACC</ta>
            <ta e="T364" id="Seg_4110" s="T363">bring-EP-IMP.2PL</ta>
            <ta e="T365" id="Seg_4111" s="T364">say-CVB.SIM-say-CVB.SIM</ta>
            <ta e="T366" id="Seg_4112" s="T365">shamanize-CVB.SEQ</ta>
            <ta e="T367" id="Seg_4113" s="T366">%%-PRS.[3SG]</ta>
            <ta e="T368" id="Seg_4114" s="T367">EMPH</ta>
            <ta e="T369" id="Seg_4115" s="T368">three</ta>
            <ta e="T370" id="Seg_4116" s="T369">night-PROPR</ta>
            <ta e="T371" id="Seg_4117" s="T370">day-ACC</ta>
            <ta e="T372" id="Seg_4118" s="T371">death.[NOM]</ta>
            <ta e="T373" id="Seg_4119" s="T372">country-3SG-ABL</ta>
            <ta e="T374" id="Seg_4120" s="T373">come.back-CVB.SEQ</ta>
            <ta e="T375" id="Seg_4121" s="T374">that-DAT/LOC</ta>
            <ta e="T376" id="Seg_4122" s="T375">reindeer.[NOM]</ta>
            <ta e="T377" id="Seg_4123" s="T376">skin-3SG-DAT/LOC</ta>
            <ta e="T378" id="Seg_4124" s="T377">bring-PRS-3PL</ta>
            <ta e="T379" id="Seg_4125" s="T378">bone-3PL-ACC</ta>
            <ta e="T380" id="Seg_4126" s="T379">see-PST2-3PL</ta>
            <ta e="T381" id="Seg_4127" s="T380">girl-ACC</ta>
            <ta e="T382" id="Seg_4128" s="T381">raise-CVB.SEQ</ta>
            <ta e="T383" id="Seg_4129" s="T382">stand-PRS.[3SG]</ta>
            <ta e="T384" id="Seg_4130" s="T383">only</ta>
            <ta e="T385" id="Seg_4131" s="T384">girl-3PL.[NOM]</ta>
            <ta e="T386" id="Seg_4132" s="T385">move-NEG.[3SG]</ta>
            <ta e="T387" id="Seg_4133" s="T386">die-PTCP.PST.[NOM]</ta>
            <ta e="T388" id="Seg_4134" s="T387">MOD</ta>
            <ta e="T389" id="Seg_4135" s="T388">fall.asleep-PTCP.PST.[NOM]</ta>
            <ta e="T390" id="Seg_4136" s="T389">MOD</ta>
            <ta e="T391" id="Seg_4137" s="T390">similar</ta>
            <ta e="T392" id="Seg_4138" s="T391">this</ta>
            <ta e="T393" id="Seg_4139" s="T392">girl.[NOM]</ta>
            <ta e="T394" id="Seg_4140" s="T393">bone-3PL-ACC</ta>
            <ta e="T395" id="Seg_4141" s="T394">inside-3SG-DAT/LOC</ta>
            <ta e="T396" id="Seg_4142" s="T395">long.coat-SIM</ta>
            <ta e="T397" id="Seg_4143" s="T396">lay-PRS.[3SG]</ta>
            <ta e="T398" id="Seg_4144" s="T397">EMPH</ta>
            <ta e="T399" id="Seg_4145" s="T398">this-3SG-3SG.[NOM]</ta>
            <ta e="T400" id="Seg_4146" s="T399">revive-CVB.SEQ</ta>
            <ta e="T401" id="Seg_4147" s="T400">come-CVB.SEQ</ta>
            <ta e="T402" id="Seg_4148" s="T401">well</ta>
            <ta e="T403" id="Seg_4149" s="T402">this</ta>
            <ta e="T404" id="Seg_4150" s="T403">old.man-EP-2SG.[NOM]</ta>
            <ta e="T405" id="Seg_4151" s="T404">daughter-3SG.[NOM]</ta>
            <ta e="T406" id="Seg_4152" s="T405">be-PRS.[3SG]</ta>
            <ta e="T407" id="Seg_4153" s="T406">this-ACC</ta>
            <ta e="T408" id="Seg_4154" s="T407">clairvoyant-EP-2SG.[NOM]</ta>
            <ta e="T409" id="Seg_4155" s="T408">woman.[NOM]</ta>
            <ta e="T410" id="Seg_4156" s="T409">make-CVB.SEQ</ta>
            <ta e="T411" id="Seg_4157" s="T410">live-PRS.[3SG]</ta>
            <ta e="T412" id="Seg_4158" s="T411">stay-PTCP.HAB</ta>
            <ta e="T413" id="Seg_4159" s="T412">two</ta>
            <ta e="T414" id="Seg_4160" s="T413">friend-3PL.[NOM]</ta>
            <ta e="T415" id="Seg_4161" s="T414">dance-DAT/LOC</ta>
            <ta e="T416" id="Seg_4162" s="T415">invite-CVB.PURP</ta>
            <ta e="T417" id="Seg_4163" s="T416">want-PST2-3PL</ta>
            <ta e="T418" id="Seg_4164" s="T417">human.being-3PL.[NOM]</ta>
            <ta e="T419" id="Seg_4165" s="T418">NEG.EX</ta>
            <ta e="T420" id="Seg_4166" s="T419">get.to.know-PST2-3PL</ta>
            <ta e="T421" id="Seg_4167" s="T420">shaman.[NOM]</ta>
            <ta e="T422" id="Seg_4168" s="T421">carry-PST2.[3SG]</ta>
            <ta e="T423" id="Seg_4169" s="T422">this-ACC</ta>
            <ta e="T424" id="Seg_4170" s="T423">well</ta>
            <ta e="T425" id="Seg_4171" s="T424">get.angry-PRS-3PL</ta>
            <ta e="T426" id="Seg_4172" s="T425">revive-PTCP.HAB-2SG-ACC</ta>
            <ta e="T427" id="Seg_4173" s="T426">find-CVB.SEQ</ta>
            <ta e="T428" id="Seg_4174" s="T427">thought-3SG-ACC</ta>
            <ta e="T429" id="Seg_4175" s="T428">turn.around-CVB.SEQ</ta>
            <ta e="T430" id="Seg_4176" s="T429">soul.of.a.dead.[NOM]</ta>
            <ta e="T431" id="Seg_4177" s="T430">become-CVB.SEQ</ta>
            <ta e="T432" id="Seg_4178" s="T431">female.shaman.[NOM]</ta>
            <ta e="T433" id="Seg_4179" s="T432">make-PRS-3PL</ta>
            <ta e="T434" id="Seg_4180" s="T433">female.shaman.[NOM]</ta>
            <ta e="T435" id="Seg_4181" s="T434">make-CVB.PURP</ta>
            <ta e="T436" id="Seg_4182" s="T435">force-PRS-3PL</ta>
            <ta e="T437" id="Seg_4183" s="T436">1PL.[NOM]</ta>
            <ta e="T438" id="Seg_4184" s="T437">2SG-ACC</ta>
            <ta e="T439" id="Seg_4185" s="T438">eat-FUT-1PL</ta>
            <ta e="T440" id="Seg_4186" s="T439">old.woman.[NOM]</ta>
            <ta e="T441" id="Seg_4187" s="T440">skin-3SG-ACC</ta>
            <ta e="T442" id="Seg_4188" s="T441">skin-CVB.SEQ-2SG</ta>
            <ta e="T443" id="Seg_4189" s="T442">drum.of.the.shaman.[NOM]</ta>
            <ta e="T444" id="Seg_4190" s="T443">make-EP-NEG-TEMP-2SG</ta>
            <ta e="T445" id="Seg_4191" s="T444">well</ta>
            <ta e="T446" id="Seg_4192" s="T445">there</ta>
            <ta e="T447" id="Seg_4193" s="T446">one</ta>
            <ta e="T448" id="Seg_4194" s="T447">old.woman-ACC</ta>
            <ta e="T449" id="Seg_4195" s="T448">skin-CVB.SEQ-3PL</ta>
            <ta e="T450" id="Seg_4196" s="T449">that</ta>
            <ta e="T451" id="Seg_4197" s="T450">skin-3SG-INSTR</ta>
            <ta e="T452" id="Seg_4198" s="T451">drum.of.the.shaman.[NOM]</ta>
            <ta e="T453" id="Seg_4199" s="T452">make-PRS-3PL</ta>
            <ta e="T454" id="Seg_4200" s="T453">that</ta>
            <ta e="T455" id="Seg_4201" s="T454">sin-3SG-DAT/LOC</ta>
            <ta e="T456" id="Seg_4202" s="T455">that.[NOM]</ta>
            <ta e="T457" id="Seg_4203" s="T456">be.sick-PTCP.PRS</ta>
            <ta e="T458" id="Seg_4204" s="T457">MOD</ta>
            <ta e="T459" id="Seg_4205" s="T458">and</ta>
            <ta e="T460" id="Seg_4206" s="T459">Nganasan.[NOM]</ta>
            <ta e="T461" id="Seg_4207" s="T460">one-SIM</ta>
            <ta e="T462" id="Seg_4208" s="T461">die-NEG.[3SG]</ta>
            <ta e="T463" id="Seg_4209" s="T462">Nganasan.[NOM]</ta>
            <ta e="T464" id="Seg_4210" s="T463">horde-INSTR</ta>
            <ta e="T465" id="Seg_4211" s="T464">run.out-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_4212" s="T0">vor.langer.Zeit</ta>
            <ta e="T2" id="Seg_4213" s="T1">eins</ta>
            <ta e="T3" id="Seg_4214" s="T2">nganasanisch</ta>
            <ta e="T4" id="Seg_4215" s="T3">alter.Mann-3SG.[NOM]</ta>
            <ta e="T5" id="Seg_4216" s="T4">leben-PST2.[3SG]</ta>
            <ta e="T6" id="Seg_4217" s="T5">Tochter-PROPR.[NOM]</ta>
            <ta e="T7" id="Seg_4218" s="T6">dieses</ta>
            <ta e="T8" id="Seg_4219" s="T7">Tochter-3SG.[NOM]</ta>
            <ta e="T9" id="Seg_4220" s="T8">wachsen-CVB.SEQ</ta>
            <ta e="T10" id="Seg_4221" s="T9">Hausfrau.[NOM]</ta>
            <ta e="T11" id="Seg_4222" s="T10">werden-CVB.SEQ</ta>
            <ta e="T12" id="Seg_4223" s="T11">nachdem</ta>
            <ta e="T13" id="Seg_4224" s="T12">sterben-CVB.SEQ</ta>
            <ta e="T14" id="Seg_4225" s="T13">bleiben-PST2.[3SG]</ta>
            <ta e="T15" id="Seg_4226" s="T14">dieses-ACC</ta>
            <ta e="T16" id="Seg_4227" s="T15">Vater-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_4228" s="T16">sehr-ADVZ</ta>
            <ta e="T18" id="Seg_4229" s="T17">weinen-PRS.[3SG]</ta>
            <ta e="T19" id="Seg_4230" s="T18">begraben-CVB.PURP</ta>
            <ta e="T20" id="Seg_4231" s="T19">want-PTCP.PST-3PL-ACC</ta>
            <ta e="T21" id="Seg_4232" s="T20">sprechen-PRS.[3SG]</ta>
            <ta e="T22" id="Seg_4233" s="T21">1SG.[NOM]</ta>
            <ta e="T23" id="Seg_4234" s="T22">Tochter-1SG-ACC</ta>
            <ta e="T24" id="Seg_4235" s="T23">immer</ta>
            <ta e="T25" id="Seg_4236" s="T24">selbst-1SG-ACC</ta>
            <ta e="T26" id="Seg_4237" s="T25">mit</ta>
            <ta e="T27" id="Seg_4238" s="T26">einladen-CVB.SIM</ta>
            <ta e="T28" id="Seg_4239" s="T27">gehen-FUT-1SG</ta>
            <ta e="T29" id="Seg_4240" s="T28">3SG.[NOM]</ta>
            <ta e="T30" id="Seg_4241" s="T29">wieder.aufleben-FUT-3SG</ta>
            <ta e="T31" id="Seg_4242" s="T30">groß</ta>
            <ta e="T32" id="Seg_4243" s="T31">Schamane-DAT/LOC</ta>
            <ta e="T33" id="Seg_4244" s="T32">wieder.aufleben-CAUS-FUT-1SG</ta>
            <ta e="T34" id="Seg_4245" s="T33">sagen-PRS.[3SG]</ta>
            <ta e="T35" id="Seg_4246" s="T34">doch</ta>
            <ta e="T36" id="Seg_4247" s="T35">Tochter-3SG-ACC</ta>
            <ta e="T37" id="Seg_4248" s="T36">Schlitten-DAT/LOC</ta>
            <ta e="T38" id="Seg_4249" s="T37">einladen-CVB.SIM</ta>
            <ta e="T39" id="Seg_4250" s="T38">gehen-PRS.[3SG]</ta>
            <ta e="T40" id="Seg_4251" s="T39">jenes</ta>
            <ta e="T41" id="Seg_4252" s="T40">fahren-CVB.SEQ</ta>
            <ta e="T42" id="Seg_4253" s="T41">Schamane-PL-ACC</ta>
            <ta e="T43" id="Seg_4254" s="T42">bitten-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T44" id="Seg_4255" s="T43">eins-3PL.[NOM]</ta>
            <ta e="T45" id="Seg_4256" s="T44">NEG</ta>
            <ta e="T46" id="Seg_4257" s="T45">sein-NEG.[3SG]</ta>
            <ta e="T47" id="Seg_4258" s="T46">nur</ta>
            <ta e="T48" id="Seg_4259" s="T47">zwei</ta>
            <ta e="T49" id="Seg_4260" s="T48">groß</ta>
            <ta e="T50" id="Seg_4261" s="T49">Schamane-ACC</ta>
            <ta e="T51" id="Seg_4262" s="T50">schamanisieren-CAUS-PRS.[3SG]</ta>
            <ta e="T52" id="Seg_4263" s="T51">zwei</ta>
            <ta e="T53" id="Seg_4264" s="T52">Jahr-3SG.[NOM]</ta>
            <ta e="T54" id="Seg_4265" s="T53">vorbeigehen-PTCP.PST-3SG-ACC</ta>
            <ta e="T55" id="Seg_4266" s="T54">nachdem</ta>
            <ta e="T56" id="Seg_4267" s="T55">Mädchen-3SG-GEN</ta>
            <ta e="T57" id="Seg_4268" s="T56">Körper-3SG.[NOM]</ta>
            <ta e="T58" id="Seg_4269" s="T57">vor.langem</ta>
            <ta e="T59" id="Seg_4270" s="T58">faulen-CVB.SEQ</ta>
            <ta e="T60" id="Seg_4271" s="T59">Knochen-3PL.[NOM]</ta>
            <ta e="T61" id="Seg_4272" s="T60">nur</ta>
            <ta e="T62" id="Seg_4273" s="T61">bleiben-PST2.[3SG]</ta>
            <ta e="T63" id="Seg_4274" s="T62">dieses</ta>
            <ta e="T64" id="Seg_4275" s="T63">zwei</ta>
            <ta e="T65" id="Seg_4276" s="T64">Schamane.[NOM]</ta>
            <ta e="T66" id="Seg_4277" s="T65">schamanisieren-CVB.SEQ-3PL</ta>
            <ta e="T67" id="Seg_4278" s="T66">was-ACC</ta>
            <ta e="T68" id="Seg_4279" s="T67">NEG</ta>
            <ta e="T69" id="Seg_4280" s="T68">machen-NEG-3PL</ta>
            <ta e="T70" id="Seg_4281" s="T69">drei-ORD</ta>
            <ta e="T71" id="Seg_4282" s="T70">Jahr-3SG.[NOM]</ta>
            <ta e="T72" id="Seg_4283" s="T71">werden-PRS.[3SG]</ta>
            <ta e="T73" id="Seg_4284" s="T72">jenes</ta>
            <ta e="T74" id="Seg_4285" s="T73">Nganasane.[NOM]</ta>
            <ta e="T75" id="Seg_4286" s="T74">Nähe-3SG-DAT/LOC</ta>
            <ta e="T76" id="Seg_4287" s="T75">Hellseher.[NOM]</ta>
            <ta e="T77" id="Seg_4288" s="T76">es.gibt</ta>
            <ta e="T78" id="Seg_4289" s="T77">sein-PST2.[3SG]</ta>
            <ta e="T79" id="Seg_4290" s="T78">dieses</ta>
            <ta e="T80" id="Seg_4291" s="T79">Hellseher.[NOM]</ta>
            <ta e="T81" id="Seg_4292" s="T80">sehr</ta>
            <ta e="T82" id="Seg_4293" s="T81">bemitleiden-PRS.[3SG]</ta>
            <ta e="T83" id="Seg_4294" s="T82">Nganasane-3SG-ACC</ta>
            <ta e="T84" id="Seg_4295" s="T83">einmal</ta>
            <ta e="T85" id="Seg_4296" s="T84">träumen-PRS.[3SG]</ta>
            <ta e="T86" id="Seg_4297" s="T85">jenes</ta>
            <ta e="T87" id="Seg_4298" s="T86">träumen-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T88" id="Seg_4299" s="T87">nachdem</ta>
            <ta e="T89" id="Seg_4300" s="T88">Nganasane-3SG-DAT/LOC</ta>
            <ta e="T90" id="Seg_4301" s="T89">kommen-CVB.SEQ</ta>
            <ta e="T91" id="Seg_4302" s="T90">erzählen-PRS.[3SG]</ta>
            <ta e="T92" id="Seg_4303" s="T91">dieses</ta>
            <ta e="T93" id="Seg_4304" s="T92">2SG.[NOM]</ta>
            <ta e="T94" id="Seg_4305" s="T93">Tochter-EP-2SG.[NOM]</ta>
            <ta e="T95" id="Seg_4306" s="T94">wieder.aufleben-FUT.[3SG]</ta>
            <ta e="T96" id="Seg_4307" s="T95">offenbar</ta>
            <ta e="T97" id="Seg_4308" s="T96">Wunder-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_4309" s="T97">eins</ta>
            <ta e="T99" id="Seg_4310" s="T98">sein-PST1-3SG</ta>
            <ta e="T100" id="Seg_4311" s="T99">Kind-2SG.[NOM]</ta>
            <ta e="T101" id="Seg_4312" s="T100">Seele-3SG-ACC</ta>
            <ta e="T102" id="Seg_4313" s="T101">essen-PTCP.PST</ta>
            <ta e="T103" id="Seg_4314" s="T102">Geist-3SG.[NOM]</ta>
            <ta e="T104" id="Seg_4315" s="T103">Meer-DAT/LOC</ta>
            <ta e="T105" id="Seg_4316" s="T104">Tod.[NOM]</ta>
            <ta e="T106" id="Seg_4317" s="T105">Wasser-3SG-DAT/LOC</ta>
            <ta e="T107" id="Seg_4318" s="T106">tragen-CVB.SIM</ta>
            <ta e="T108" id="Seg_4319" s="T107">gehen-PTCP.PRS</ta>
            <ta e="T109" id="Seg_4320" s="T108">sein-PST2.[3SG]</ta>
            <ta e="T110" id="Seg_4321" s="T109">einer.von.zwei</ta>
            <ta e="T111" id="Seg_4322" s="T110">Schamane-EP-2SG.[NOM]</ta>
            <ta e="T112" id="Seg_4323" s="T111">wildes.Rentier.[NOM]</ta>
            <ta e="T113" id="Seg_4324" s="T112">werden-CVB.SEQ</ta>
            <ta e="T114" id="Seg_4325" s="T113">Erde-EP-INSTR</ta>
            <ta e="T115" id="Seg_4326" s="T114">folgen-CVB.SEQ</ta>
            <ta e="T116" id="Seg_4327" s="T115">Tierhaar-3SG.[NOM]</ta>
            <ta e="T117" id="Seg_4328" s="T116">Eisschicht.[NOM]</ta>
            <ta e="T118" id="Seg_4329" s="T117">Loch-DAT/LOC</ta>
            <ta e="T119" id="Seg_4330" s="T118">passen-NEG.PTCP</ta>
            <ta e="T120" id="Seg_4331" s="T119">sein-PRS.[3SG]</ta>
            <ta e="T121" id="Seg_4332" s="T120">einer.von.zwei.[NOM]</ta>
            <ta e="T122" id="Seg_4333" s="T121">wildes.Rentier.[NOM]</ta>
            <ta e="T123" id="Seg_4334" s="T122">werden-CVB.SEQ</ta>
            <ta e="T124" id="Seg_4335" s="T123">Wasser-INSTR</ta>
            <ta e="T125" id="Seg_4336" s="T124">fließen-CVB.SEQ</ta>
            <ta e="T126" id="Seg_4337" s="T125">ankommen-TEMP-3SG</ta>
            <ta e="T127" id="Seg_4338" s="T126">Welle-3SG.[NOM]</ta>
            <ta e="T128" id="Seg_4339" s="T127">fangen-CVB.PURP</ta>
            <ta e="T129" id="Seg_4340" s="T128">nur</ta>
            <ta e="T130" id="Seg_4341" s="T129">machen-TEMP-3SG</ta>
            <ta e="T131" id="Seg_4342" s="T130">Seele-3SG-ACC</ta>
            <ta e="T132" id="Seg_4343" s="T131">tragen-CVB.SEQ</ta>
            <ta e="T133" id="Seg_4344" s="T132">werfen-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_4345" s="T133">neun</ta>
            <ta e="T135" id="Seg_4346" s="T134">Jahr.[NOM]</ta>
            <ta e="T136" id="Seg_4347" s="T135">so</ta>
            <ta e="T137" id="Seg_4348" s="T136">folgen-EP-RECP/COLL-FUT-3PL</ta>
            <ta e="T138" id="Seg_4349" s="T137">dann</ta>
            <ta e="T139" id="Seg_4350" s="T138">wie</ta>
            <ta e="T140" id="Seg_4351" s="T139">sein-PRS.[3SG]</ta>
            <ta e="T141" id="Seg_4352" s="T140">jenes-DAT/LOC</ta>
            <ta e="T142" id="Seg_4353" s="T141">nganasanisch</ta>
            <ta e="T143" id="Seg_4354" s="T142">alter.Mann-3SG.[NOM]</ta>
            <ta e="T144" id="Seg_4355" s="T143">sprechen-PRS.[3SG]</ta>
            <ta e="T145" id="Seg_4356" s="T144">jenes</ta>
            <ta e="T146" id="Seg_4357" s="T145">Schamane.[NOM]</ta>
            <ta e="T147" id="Seg_4358" s="T146">können-NEG.PTCP-3SG-ACC</ta>
            <ta e="T148" id="Seg_4359" s="T147">wissen-PTCP.PRS</ta>
            <ta e="T149" id="Seg_4360" s="T148">Mensch.[NOM]</ta>
            <ta e="T150" id="Seg_4361" s="T149">selbst-2SG.[NOM]</ta>
            <ta e="T151" id="Seg_4362" s="T150">warum</ta>
            <ta e="T152" id="Seg_4363" s="T151">wieder.aufleben-CAUS-CVB.SEQ</ta>
            <ta e="T153" id="Seg_4364" s="T152">versuchen-NEG-2SG</ta>
            <ta e="T154" id="Seg_4365" s="T153">sagen-PRS.[3SG]</ta>
            <ta e="T155" id="Seg_4366" s="T154">bitten-PRS.[3SG]</ta>
            <ta e="T156" id="Seg_4367" s="T155">doch</ta>
            <ta e="T157" id="Seg_4368" s="T156">sehr</ta>
            <ta e="T158" id="Seg_4369" s="T157">Unglück-VBZ-PST1-2SG</ta>
            <ta e="T159" id="Seg_4370" s="T158">sagen-PRS.[3SG]</ta>
            <ta e="T160" id="Seg_4371" s="T159">wie</ta>
            <ta e="T161" id="Seg_4372" s="T160">machen-PRS-1SG</ta>
            <ta e="T162" id="Seg_4373" s="T161">wieder.aufleben-TEMP-3SG</ta>
            <ta e="T163" id="Seg_4374" s="T162">1SG-DAT/LOC</ta>
            <ta e="T164" id="Seg_4375" s="T163">Frau.[NOM]</ta>
            <ta e="T165" id="Seg_4376" s="T164">geben-FUT-2SG</ta>
            <ta e="T166" id="Seg_4377" s="T165">Q</ta>
            <ta e="T167" id="Seg_4378" s="T166">Reichtum-EP-1SG.[NOM]</ta>
            <ta e="T168" id="Seg_4379" s="T167">Hälfte-3SG-ACC</ta>
            <ta e="T169" id="Seg_4380" s="T168">geben-FUT-1SG</ta>
            <ta e="T170" id="Seg_4381" s="T169">selbst-3SG-ACC</ta>
            <ta e="T171" id="Seg_4382" s="T170">geben-FUT-1SG</ta>
            <ta e="T172" id="Seg_4383" s="T171">wieder.aufleben-CAUS.[IMP.2SG]</ta>
            <ta e="T173" id="Seg_4384" s="T172">nur</ta>
            <ta e="T174" id="Seg_4385" s="T173">sagen-PRS.[3SG]</ta>
            <ta e="T175" id="Seg_4386" s="T174">Hellseher-3SG.[NOM]</ta>
            <ta e="T176" id="Seg_4387" s="T175">schlafen-CVB.SEQ</ta>
            <ta e="T177" id="Seg_4388" s="T176">bleiben-PRS.[3SG]</ta>
            <ta e="T178" id="Seg_4389" s="T177">dieses</ta>
            <ta e="T179" id="Seg_4390" s="T178">schlafen-CVB.SIM</ta>
            <ta e="T180" id="Seg_4391" s="T179">liegen-CVB.SEQ</ta>
            <ta e="T181" id="Seg_4392" s="T180">doch</ta>
            <ta e="T182" id="Seg_4393" s="T181">jenes</ta>
            <ta e="T183" id="Seg_4394" s="T182">Tod-3SG-GEN</ta>
            <ta e="T184" id="Seg_4395" s="T183">Ort-3SG-DAT/LOC</ta>
            <ta e="T185" id="Seg_4396" s="T184">ankommen-PRS.[3SG]</ta>
            <ta e="T186" id="Seg_4397" s="T185">doch</ta>
            <ta e="T187" id="Seg_4398" s="T186">jenes</ta>
            <ta e="T188" id="Seg_4399" s="T187">Tod-3SG-GEN</ta>
            <ta e="T189" id="Seg_4400" s="T188">Ort-3SG-DAT/LOC</ta>
            <ta e="T190" id="Seg_4401" s="T189">ankommen-PRS.[3SG]</ta>
            <ta e="T191" id="Seg_4402" s="T190">ankommen-PST2-3SG</ta>
            <ta e="T192" id="Seg_4403" s="T191">viel</ta>
            <ta e="T193" id="Seg_4404" s="T192">sehr</ta>
            <ta e="T194" id="Seg_4405" s="T193">Leute.[NOM]</ta>
            <ta e="T195" id="Seg_4406" s="T194">tanzen-CVB.SIM</ta>
            <ta e="T196" id="Seg_4407" s="T195">stehen-PRS-3PL</ta>
            <ta e="T197" id="Seg_4408" s="T196">hier</ta>
            <ta e="T198" id="Seg_4409" s="T197">suchen-CVB.SIM</ta>
            <ta e="T199" id="Seg_4410" s="T198">vergeblich.tun-PRS.[3SG]</ta>
            <ta e="T200" id="Seg_4411" s="T199">Mädchen-3PL-GEN</ta>
            <ta e="T201" id="Seg_4412" s="T200">Seele-3SG-ACC</ta>
            <ta e="T202" id="Seg_4413" s="T201">was.[NOM]</ta>
            <ta e="T203" id="Seg_4414" s="T202">kommen-FUT.[3SG]=Q</ta>
            <ta e="T204" id="Seg_4415" s="T203">überhaupt</ta>
            <ta e="T205" id="Seg_4416" s="T204">finden-NEG.[3SG]</ta>
            <ta e="T206" id="Seg_4417" s="T205">dann</ta>
            <ta e="T207" id="Seg_4418" s="T206">dieses</ta>
            <ta e="T208" id="Seg_4419" s="T207">Ort-ABL</ta>
            <ta e="T209" id="Seg_4420" s="T208">anders</ta>
            <ta e="T210" id="Seg_4421" s="T209">Ort-DAT/LOC</ta>
            <ta e="T211" id="Seg_4422" s="T210">gehen-IMP.1SG</ta>
            <ta e="T212" id="Seg_4423" s="T211">denken-PRS.[3SG]</ta>
            <ta e="T213" id="Seg_4424" s="T212">ankommen-PRS.[3SG]</ta>
            <ta e="T214" id="Seg_4425" s="T213">eins</ta>
            <ta e="T215" id="Seg_4426" s="T214">Ort-DAT/LOC</ta>
            <ta e="T216" id="Seg_4427" s="T215">dort</ta>
            <ta e="T217" id="Seg_4428" s="T216">drei</ta>
            <ta e="T218" id="Seg_4429" s="T217">EMPH=EMPH</ta>
            <ta e="T219" id="Seg_4430" s="T218">identisch</ta>
            <ta e="T220" id="Seg_4431" s="T219">Gesicht-PROPR</ta>
            <ta e="T221" id="Seg_4432" s="T220">Kleidung-PROPR</ta>
            <ta e="T222" id="Seg_4433" s="T221">Mädchen.[NOM]</ta>
            <ta e="T223" id="Seg_4434" s="T222">es.gibt</ta>
            <ta e="T224" id="Seg_4435" s="T223">sein-PST2.[3SG]</ta>
            <ta e="T225" id="Seg_4436" s="T224">dieses-ABL</ta>
            <ta e="T226" id="Seg_4437" s="T225">anderer-3PL.[NOM]</ta>
            <ta e="T227" id="Seg_4438" s="T226">alter.Mann.[NOM]</ta>
            <ta e="T228" id="Seg_4439" s="T227">Tochter-3SG.[NOM]</ta>
            <ta e="T229" id="Seg_4440" s="T228">was.für.ein-3PL.[NOM]</ta>
            <ta e="T230" id="Seg_4441" s="T229">jenes</ta>
            <ta e="T231" id="Seg_4442" s="T230">Tochter-3SG-ACC</ta>
            <ta e="T232" id="Seg_4443" s="T231">können-CVB.SEQ</ta>
            <ta e="T233" id="Seg_4444" s="T232">erfahren-NEG.[3SG]</ta>
            <ta e="T234" id="Seg_4445" s="T233">Kleidung-3PL-GEN</ta>
            <ta e="T235" id="Seg_4446" s="T234">Naht-3SG-ACC</ta>
            <ta e="T236" id="Seg_4447" s="T235">zählen-CVB.SEQ</ta>
            <ta e="T237" id="Seg_4448" s="T236">versuchen-PRS.[3SG]</ta>
            <ta e="T238" id="Seg_4449" s="T237">Anzahl-3SG.[NOM]</ta>
            <ta e="T239" id="Seg_4450" s="T238">ganz</ta>
            <ta e="T240" id="Seg_4451" s="T239">eins</ta>
            <ta e="T241" id="Seg_4452" s="T240">Unglück-VBZ-CVB.SIM</ta>
            <ta e="T242" id="Seg_4453" s="T241">vergeblich.tun-PRS.[3SG]</ta>
            <ta e="T243" id="Seg_4454" s="T242">dieses</ta>
            <ta e="T244" id="Seg_4455" s="T243">Mädchen-PL.[NOM]</ta>
            <ta e="T245" id="Seg_4456" s="T244">drei</ta>
            <ta e="T246" id="Seg_4457" s="T245">Haus-PROPR-3PL</ta>
            <ta e="T247" id="Seg_4458" s="T246">Seite.[NOM]</ta>
            <ta e="T248" id="Seg_4459" s="T247">verschieden.[NOM]</ta>
            <ta e="T249" id="Seg_4460" s="T248">dorthin</ta>
            <ta e="T250" id="Seg_4461" s="T249">am.Abend</ta>
            <ta e="T251" id="Seg_4462" s="T250">Tanz-ABL</ta>
            <ta e="T252" id="Seg_4463" s="T251">kommen-CVB.SEQ</ta>
            <ta e="T253" id="Seg_4464" s="T252">schlafen-CVB.SEQ</ta>
            <ta e="T254" id="Seg_4465" s="T253">bleiben-PRS-3PL</ta>
            <ta e="T255" id="Seg_4466" s="T254">Mädchen-PL-3SG-ACC</ta>
            <ta e="T256" id="Seg_4467" s="T255">mit</ta>
            <ta e="T257" id="Seg_4468" s="T256">sich.unterhalten-CVB.PURP</ta>
            <ta e="T258" id="Seg_4469" s="T257">wollen-PRS.[3SG]</ta>
            <ta e="T259" id="Seg_4470" s="T258">anderer-3PL.[NOM]</ta>
            <ta e="T260" id="Seg_4471" s="T259">NEG</ta>
            <ta e="T261" id="Seg_4472" s="T260">sprechen-NEG.[3SG]</ta>
            <ta e="T262" id="Seg_4473" s="T261">wie</ta>
            <ta e="T263" id="Seg_4474" s="T262">werden-FUT-1SG=Q</ta>
            <ta e="T264" id="Seg_4475" s="T263">denken-CVB.SEQ</ta>
            <ta e="T265" id="Seg_4476" s="T264">nachdenken-PRS.[3SG]</ta>
            <ta e="T266" id="Seg_4477" s="T265">anderer-3PL-GEN</ta>
            <ta e="T267" id="Seg_4478" s="T266">Haus-3SG-DAT/LOC</ta>
            <ta e="T268" id="Seg_4479" s="T267">sich.verstecken-CVB.SEQ</ta>
            <ta e="T269" id="Seg_4480" s="T268">hineingehen-CVB.SEQ</ta>
            <ta e="T270" id="Seg_4481" s="T269">schlafen-PTCP.PST-3SG-ACC</ta>
            <ta e="T271" id="Seg_4482" s="T270">beobachten-PRS.[3SG]</ta>
            <ta e="T272" id="Seg_4483" s="T271">so</ta>
            <ta e="T273" id="Seg_4484" s="T272">drei</ta>
            <ta e="T274" id="Seg_4485" s="T273">Haus-DAT/LOC</ta>
            <ta e="T275" id="Seg_4486" s="T274">drei-COLL-3PL-DAT/LOC</ta>
            <ta e="T276" id="Seg_4487" s="T275">übernachten-PST1-3SG</ta>
            <ta e="T277" id="Seg_4488" s="T276">und</ta>
            <ta e="T278" id="Seg_4489" s="T277">was-ACC</ta>
            <ta e="T279" id="Seg_4490" s="T278">NEG</ta>
            <ta e="T280" id="Seg_4491" s="T279">finden-CVB.SEQ</ta>
            <ta e="T281" id="Seg_4492" s="T280">nehmen-NEG-PST1-3SG</ta>
            <ta e="T282" id="Seg_4493" s="T281">doch</ta>
            <ta e="T283" id="Seg_4494" s="T282">einmal</ta>
            <ta e="T284" id="Seg_4495" s="T283">drei-COLL</ta>
            <ta e="T285" id="Seg_4496" s="T284">eins</ta>
            <ta e="T286" id="Seg_4497" s="T285">Haus-DAT/LOC</ta>
            <ta e="T287" id="Seg_4498" s="T286">sich.sammeln-REFL-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T288" id="Seg_4499" s="T287">sein-PST2.[3SG]</ta>
            <ta e="T289" id="Seg_4500" s="T288">hierher</ta>
            <ta e="T290" id="Seg_4501" s="T289">hineingehen-CVB.SEQ</ta>
            <ta e="T291" id="Seg_4502" s="T290">Hellseher.[NOM]</ta>
            <ta e="T292" id="Seg_4503" s="T291">sich.verstecken-CVB.SIM</ta>
            <ta e="T293" id="Seg_4504" s="T292">liegen-PST2.[3SG]</ta>
            <ta e="T294" id="Seg_4505" s="T293">dieses</ta>
            <ta e="T295" id="Seg_4506" s="T294">liegen-TEMP-3SG</ta>
            <ta e="T296" id="Seg_4507" s="T295">eins</ta>
            <ta e="T297" id="Seg_4508" s="T296">Mädchen.[NOM]</ta>
            <ta e="T298" id="Seg_4509" s="T297">nur</ta>
            <ta e="T299" id="Seg_4510" s="T298">nachdenken-PST2.[3SG]</ta>
            <ta e="T300" id="Seg_4511" s="T299">dieses-ACC</ta>
            <ta e="T301" id="Seg_4512" s="T300">zwei</ta>
            <ta e="T302" id="Seg_4513" s="T301">Freund-3SG.[NOM]</ta>
            <ta e="T303" id="Seg_4514" s="T302">fragen-PST2-3PL</ta>
            <ta e="T304" id="Seg_4515" s="T303">was.[NOM]</ta>
            <ta e="T305" id="Seg_4516" s="T304">sein-CVB.SEQ</ta>
            <ta e="T306" id="Seg_4517" s="T305">nachdenken-PRS-2SG</ta>
            <ta e="T307" id="Seg_4518" s="T306">sagen-CVB.SEQ</ta>
            <ta e="T308" id="Seg_4519" s="T307">Mädchen-3PL.[NOM]</ta>
            <ta e="T309" id="Seg_4520" s="T308">sprechen-PST2.[3SG]</ta>
            <ta e="T310" id="Seg_4521" s="T309">1SG.[NOM]</ta>
            <ta e="T311" id="Seg_4522" s="T310">Mutter-1SG.[NOM]</ta>
            <ta e="T312" id="Seg_4523" s="T311">eins</ta>
            <ta e="T313" id="Seg_4524" s="T312">sehr</ta>
            <ta e="T314" id="Seg_4525" s="T313">Vermächtnis-PROPR</ta>
            <ta e="T315" id="Seg_4526" s="T314">Sache-1SG-ACC</ta>
            <ta e="T316" id="Seg_4527" s="T315">geben-EP-NEG.CVB</ta>
            <ta e="T317" id="Seg_4528" s="T316">schicken-PST2.[3SG]</ta>
            <ta e="T318" id="Seg_4529" s="T317">jenes-ACC</ta>
            <ta e="T319" id="Seg_4530" s="T318">Freund-3PL.[NOM]</ta>
            <ta e="T320" id="Seg_4531" s="T319">sprechen-PST2-3PL</ta>
            <ta e="T321" id="Seg_4532" s="T320">was</ta>
            <ta e="T322" id="Seg_4533" s="T321">sein-FUT.[3SG]=Q</ta>
            <ta e="T323" id="Seg_4534" s="T322">Mutter-3SG.[NOM]</ta>
            <ta e="T324" id="Seg_4535" s="T323">kommen-TEMP-3SG</ta>
            <ta e="T325" id="Seg_4536" s="T324">selbst-3SG.[NOM]</ta>
            <ta e="T326" id="Seg_4537" s="T325">bringen-CVB.SEQ</ta>
            <ta e="T327" id="Seg_4538" s="T326">geben-FUT.[3SG]</ta>
            <ta e="T328" id="Seg_4539" s="T327">MOD</ta>
            <ta e="T329" id="Seg_4540" s="T328">EMPH</ta>
            <ta e="T330" id="Seg_4541" s="T329">doch</ta>
            <ta e="T331" id="Seg_4542" s="T330">dieses</ta>
            <ta e="T332" id="Seg_4543" s="T331">sein-PST2.[3SG]</ta>
            <ta e="T333" id="Seg_4544" s="T332">MOD</ta>
            <ta e="T334" id="Seg_4545" s="T333">alter.Mann.[NOM]</ta>
            <ta e="T335" id="Seg_4546" s="T334">Tochter-3SG.[NOM]</ta>
            <ta e="T336" id="Seg_4547" s="T335">denken-CVB.SIM</ta>
            <ta e="T337" id="Seg_4548" s="T336">denken-PRS.[3SG]</ta>
            <ta e="T338" id="Seg_4549" s="T337">einschlafen-PTCP.PST-3SG-ACC</ta>
            <ta e="T339" id="Seg_4550" s="T338">nachdem</ta>
            <ta e="T340" id="Seg_4551" s="T339">umarmen-CVB.SEQ</ta>
            <ta e="T341" id="Seg_4552" s="T340">nachdem</ta>
            <ta e="T342" id="Seg_4553" s="T341">doch</ta>
            <ta e="T343" id="Seg_4554" s="T342">fliegen-PRS.[3SG]</ta>
            <ta e="T344" id="Seg_4555" s="T343">EMPH</ta>
            <ta e="T345" id="Seg_4556" s="T344">jenes</ta>
            <ta e="T346" id="Seg_4557" s="T345">Zeit-DAT/LOC</ta>
            <ta e="T347" id="Seg_4558" s="T346">solange</ta>
            <ta e="T348" id="Seg_4559" s="T347">neun</ta>
            <ta e="T349" id="Seg_4560" s="T348">Tag.[NOM]</ta>
            <ta e="T350" id="Seg_4561" s="T349">schlafen-CVB.SIM</ta>
            <ta e="T351" id="Seg_4562" s="T350">liegen-PST2.[3SG]</ta>
            <ta e="T352" id="Seg_4563" s="T351">doch</ta>
            <ta e="T353" id="Seg_4564" s="T352">umarmen-CVB.SEQ</ta>
            <ta e="T354" id="Seg_4565" s="T353">nachdem</ta>
            <ta e="T355" id="Seg_4566" s="T354">springen-CVB.SEQ</ta>
            <ta e="T356" id="Seg_4567" s="T355">stehen-PRS.[3SG]</ta>
            <ta e="T357" id="Seg_4568" s="T356">EMPH</ta>
            <ta e="T358" id="Seg_4569" s="T357">aufwachen-CVB.SEQ</ta>
            <ta e="T359" id="Seg_4570" s="T358">hier</ta>
            <ta e="T360" id="Seg_4571" s="T359">laufen-CVB.SEQ-3PL</ta>
            <ta e="T361" id="Seg_4572" s="T360">Helfer-ACC</ta>
            <ta e="T362" id="Seg_4573" s="T361">bringen-PRS-3PL</ta>
            <ta e="T363" id="Seg_4574" s="T362">Knochen-PL-3SG-ACC</ta>
            <ta e="T364" id="Seg_4575" s="T363">bringen-EP-IMP.2PL</ta>
            <ta e="T365" id="Seg_4576" s="T364">sagen-CVB.SIM-sagen-CVB.SIM</ta>
            <ta e="T366" id="Seg_4577" s="T365">schamanisieren-CVB.SEQ</ta>
            <ta e="T367" id="Seg_4578" s="T366">%%-PRS.[3SG]</ta>
            <ta e="T368" id="Seg_4579" s="T367">EMPH</ta>
            <ta e="T369" id="Seg_4580" s="T368">drei</ta>
            <ta e="T370" id="Seg_4581" s="T369">Nacht-PROPR</ta>
            <ta e="T371" id="Seg_4582" s="T370">Tag-ACC</ta>
            <ta e="T372" id="Seg_4583" s="T371">Tod.[NOM]</ta>
            <ta e="T373" id="Seg_4584" s="T372">Land-3SG-ABL</ta>
            <ta e="T374" id="Seg_4585" s="T373">zurückkommen-CVB.SEQ</ta>
            <ta e="T375" id="Seg_4586" s="T374">jenes-DAT/LOC</ta>
            <ta e="T376" id="Seg_4587" s="T375">Rentier.[NOM]</ta>
            <ta e="T377" id="Seg_4588" s="T376">Haut-3SG-DAT/LOC</ta>
            <ta e="T378" id="Seg_4589" s="T377">bringen-PRS-3PL</ta>
            <ta e="T379" id="Seg_4590" s="T378">Knochen-3PL-ACC</ta>
            <ta e="T380" id="Seg_4591" s="T379">sehen-PST2-3PL</ta>
            <ta e="T381" id="Seg_4592" s="T380">Mädchen-ACC</ta>
            <ta e="T382" id="Seg_4593" s="T381">heben-CVB.SEQ</ta>
            <ta e="T383" id="Seg_4594" s="T382">stehen-PRS.[3SG]</ta>
            <ta e="T384" id="Seg_4595" s="T383">nur</ta>
            <ta e="T385" id="Seg_4596" s="T384">Mädchen-3PL.[NOM]</ta>
            <ta e="T386" id="Seg_4597" s="T385">sich.bewegen-NEG.[3SG]</ta>
            <ta e="T387" id="Seg_4598" s="T386">sterben-PTCP.PST.[NOM]</ta>
            <ta e="T388" id="Seg_4599" s="T387">MOD</ta>
            <ta e="T389" id="Seg_4600" s="T388">einschlafen-PTCP.PST.[NOM]</ta>
            <ta e="T390" id="Seg_4601" s="T389">MOD</ta>
            <ta e="T391" id="Seg_4602" s="T390">ähnlich</ta>
            <ta e="T392" id="Seg_4603" s="T391">dieses</ta>
            <ta e="T393" id="Seg_4604" s="T392">Mädchen.[NOM]</ta>
            <ta e="T394" id="Seg_4605" s="T393">Knochen-3PL-ACC</ta>
            <ta e="T395" id="Seg_4606" s="T394">Inneres-3SG-DAT/LOC</ta>
            <ta e="T396" id="Seg_4607" s="T395">langer.Mantel-SIM</ta>
            <ta e="T397" id="Seg_4608" s="T396">legen-PRS.[3SG]</ta>
            <ta e="T398" id="Seg_4609" s="T397">EMPH</ta>
            <ta e="T399" id="Seg_4610" s="T398">dieses-3SG-3SG.[NOM]</ta>
            <ta e="T400" id="Seg_4611" s="T399">wieder.aufleben-CVB.SEQ</ta>
            <ta e="T401" id="Seg_4612" s="T400">kommen-CVB.SEQ</ta>
            <ta e="T402" id="Seg_4613" s="T401">doch</ta>
            <ta e="T403" id="Seg_4614" s="T402">dieses</ta>
            <ta e="T404" id="Seg_4615" s="T403">alter.Mann-EP-2SG.[NOM]</ta>
            <ta e="T405" id="Seg_4616" s="T404">Tochter-3SG.[NOM]</ta>
            <ta e="T406" id="Seg_4617" s="T405">sein-PRS.[3SG]</ta>
            <ta e="T407" id="Seg_4618" s="T406">dieses-ACC</ta>
            <ta e="T408" id="Seg_4619" s="T407">Hellseher-EP-2SG.[NOM]</ta>
            <ta e="T409" id="Seg_4620" s="T408">Frau.[NOM]</ta>
            <ta e="T410" id="Seg_4621" s="T409">machen-CVB.SEQ</ta>
            <ta e="T411" id="Seg_4622" s="T410">leben-PRS.[3SG]</ta>
            <ta e="T412" id="Seg_4623" s="T411">bleiben-PTCP.HAB</ta>
            <ta e="T413" id="Seg_4624" s="T412">zwei</ta>
            <ta e="T414" id="Seg_4625" s="T413">Freund-3PL.[NOM]</ta>
            <ta e="T415" id="Seg_4626" s="T414">Tanz-DAT/LOC</ta>
            <ta e="T416" id="Seg_4627" s="T415">einladen-CVB.PURP</ta>
            <ta e="T417" id="Seg_4628" s="T416">wollen-PST2-3PL</ta>
            <ta e="T418" id="Seg_4629" s="T417">Mensch-3PL.[NOM]</ta>
            <ta e="T419" id="Seg_4630" s="T418">NEG.EX</ta>
            <ta e="T420" id="Seg_4631" s="T419">erfahren-PST2-3PL</ta>
            <ta e="T421" id="Seg_4632" s="T420">Schamane.[NOM]</ta>
            <ta e="T422" id="Seg_4633" s="T421">tragen-PST2.[3SG]</ta>
            <ta e="T423" id="Seg_4634" s="T422">dieses-ACC</ta>
            <ta e="T424" id="Seg_4635" s="T423">doch</ta>
            <ta e="T425" id="Seg_4636" s="T424">böse.werden-PRS-3PL</ta>
            <ta e="T426" id="Seg_4637" s="T425">wieder.aufleben-PTCP.HAB-2SG-ACC</ta>
            <ta e="T427" id="Seg_4638" s="T426">finden-CVB.SEQ</ta>
            <ta e="T428" id="Seg_4639" s="T427">Gedanke-3SG-ACC</ta>
            <ta e="T429" id="Seg_4640" s="T428">umdrehen-CVB.SEQ</ta>
            <ta e="T430" id="Seg_4641" s="T429">Seele.eines.Verstorbenen.[NOM]</ta>
            <ta e="T431" id="Seg_4642" s="T430">werden-CVB.SEQ</ta>
            <ta e="T432" id="Seg_4643" s="T431">Schamanin.[NOM]</ta>
            <ta e="T433" id="Seg_4644" s="T432">machen-PRS-3PL</ta>
            <ta e="T434" id="Seg_4645" s="T433">Schamanin.[NOM]</ta>
            <ta e="T435" id="Seg_4646" s="T434">machen-CVB.PURP</ta>
            <ta e="T436" id="Seg_4647" s="T435">zwingen-PRS-3PL</ta>
            <ta e="T437" id="Seg_4648" s="T436">1PL.[NOM]</ta>
            <ta e="T438" id="Seg_4649" s="T437">2SG-ACC</ta>
            <ta e="T439" id="Seg_4650" s="T438">essen-FUT-1PL</ta>
            <ta e="T440" id="Seg_4651" s="T439">Alte.[NOM]</ta>
            <ta e="T441" id="Seg_4652" s="T440">Haut-3SG-ACC</ta>
            <ta e="T442" id="Seg_4653" s="T441">Haut.abziehen-CVB.SEQ-2SG</ta>
            <ta e="T443" id="Seg_4654" s="T442">Schamanentrommel.[NOM]</ta>
            <ta e="T444" id="Seg_4655" s="T443">machen-EP-NEG-TEMP-2SG</ta>
            <ta e="T445" id="Seg_4656" s="T444">doch</ta>
            <ta e="T446" id="Seg_4657" s="T445">dort</ta>
            <ta e="T447" id="Seg_4658" s="T446">eins</ta>
            <ta e="T448" id="Seg_4659" s="T447">Alte-ACC</ta>
            <ta e="T449" id="Seg_4660" s="T448">Haut.abziehen-CVB.SEQ-3PL</ta>
            <ta e="T450" id="Seg_4661" s="T449">jenes</ta>
            <ta e="T451" id="Seg_4662" s="T450">Haut-3SG-INSTR</ta>
            <ta e="T452" id="Seg_4663" s="T451">Schamanentrommel.[NOM]</ta>
            <ta e="T453" id="Seg_4664" s="T452">machen-PRS-3PL</ta>
            <ta e="T454" id="Seg_4665" s="T453">jenes</ta>
            <ta e="T455" id="Seg_4666" s="T454">Sünde-3SG-DAT/LOC</ta>
            <ta e="T456" id="Seg_4667" s="T455">dieses.[NOM]</ta>
            <ta e="T457" id="Seg_4668" s="T456">krank.sein-PTCP.PRS</ta>
            <ta e="T458" id="Seg_4669" s="T457">MOD</ta>
            <ta e="T459" id="Seg_4670" s="T458">und</ta>
            <ta e="T460" id="Seg_4671" s="T459">Nganasane.[NOM]</ta>
            <ta e="T461" id="Seg_4672" s="T460">eins-SIM</ta>
            <ta e="T462" id="Seg_4673" s="T461">sterben-NEG.[3SG]</ta>
            <ta e="T463" id="Seg_4674" s="T462">Nganasane.[NOM]</ta>
            <ta e="T464" id="Seg_4675" s="T463">Horde-INSTR</ta>
            <ta e="T465" id="Seg_4676" s="T464">zu.Ende.gehen-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_4677" s="T0">давно</ta>
            <ta e="T2" id="Seg_4678" s="T1">один</ta>
            <ta e="T3" id="Seg_4679" s="T2">нганасанский</ta>
            <ta e="T4" id="Seg_4680" s="T3">старик-3SG.[NOM]</ta>
            <ta e="T5" id="Seg_4681" s="T4">жить-PST2.[3SG]</ta>
            <ta e="T6" id="Seg_4682" s="T5">дочь-PROPR.[NOM]</ta>
            <ta e="T7" id="Seg_4683" s="T6">этот</ta>
            <ta e="T8" id="Seg_4684" s="T7">дочь-3SG.[NOM]</ta>
            <ta e="T9" id="Seg_4685" s="T8">расти-CVB.SEQ</ta>
            <ta e="T10" id="Seg_4686" s="T9">хозяйка.[NOM]</ta>
            <ta e="T11" id="Seg_4687" s="T10">становиться-CVB.SEQ</ta>
            <ta e="T12" id="Seg_4688" s="T11">после</ta>
            <ta e="T13" id="Seg_4689" s="T12">умирать-CVB.SEQ</ta>
            <ta e="T14" id="Seg_4690" s="T13">оставаться-PST2.[3SG]</ta>
            <ta e="T15" id="Seg_4691" s="T14">этот-ACC</ta>
            <ta e="T16" id="Seg_4692" s="T15">отец-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_4693" s="T16">очень-ADVZ</ta>
            <ta e="T18" id="Seg_4694" s="T17">плакать-PRS.[3SG]</ta>
            <ta e="T19" id="Seg_4695" s="T18">хоронить-CVB.PURP</ta>
            <ta e="T20" id="Seg_4696" s="T19">хотеть-PTCP.PST-3PL-ACC</ta>
            <ta e="T21" id="Seg_4697" s="T20">говорить-PRS.[3SG]</ta>
            <ta e="T22" id="Seg_4698" s="T21">1SG.[NOM]</ta>
            <ta e="T23" id="Seg_4699" s="T22">дочь-1SG-ACC</ta>
            <ta e="T24" id="Seg_4700" s="T23">всегда</ta>
            <ta e="T25" id="Seg_4701" s="T24">сам-1SG-ACC</ta>
            <ta e="T26" id="Seg_4702" s="T25">с</ta>
            <ta e="T27" id="Seg_4703" s="T26">грузить-CVB.SIM</ta>
            <ta e="T28" id="Seg_4704" s="T27">идти-FUT-1SG</ta>
            <ta e="T29" id="Seg_4705" s="T28">3SG.[NOM]</ta>
            <ta e="T30" id="Seg_4706" s="T29">оживать-FUT-3SG</ta>
            <ta e="T31" id="Seg_4707" s="T30">большой</ta>
            <ta e="T32" id="Seg_4708" s="T31">шаман-DAT/LOC</ta>
            <ta e="T33" id="Seg_4709" s="T32">оживать-CAUS-FUT-1SG</ta>
            <ta e="T34" id="Seg_4710" s="T33">говорить-PRS.[3SG]</ta>
            <ta e="T35" id="Seg_4711" s="T34">вот</ta>
            <ta e="T36" id="Seg_4712" s="T35">дочь-3SG-ACC</ta>
            <ta e="T37" id="Seg_4713" s="T36">сани-DAT/LOC</ta>
            <ta e="T38" id="Seg_4714" s="T37">грузить-CVB.SIM</ta>
            <ta e="T39" id="Seg_4715" s="T38">идти-PRS.[3SG]</ta>
            <ta e="T40" id="Seg_4716" s="T39">тот</ta>
            <ta e="T41" id="Seg_4717" s="T40">ехать-CVB.SEQ</ta>
            <ta e="T42" id="Seg_4718" s="T41">шаман-PL-ACC</ta>
            <ta e="T43" id="Seg_4719" s="T42">попросить-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T44" id="Seg_4720" s="T43">один-3PL.[NOM]</ta>
            <ta e="T45" id="Seg_4721" s="T44">NEG</ta>
            <ta e="T46" id="Seg_4722" s="T45">быть-NEG.[3SG]</ta>
            <ta e="T47" id="Seg_4723" s="T46">только</ta>
            <ta e="T48" id="Seg_4724" s="T47">два</ta>
            <ta e="T49" id="Seg_4725" s="T48">большой</ta>
            <ta e="T50" id="Seg_4726" s="T49">шаман-ACC</ta>
            <ta e="T51" id="Seg_4727" s="T50">камлать-CAUS-PRS.[3SG]</ta>
            <ta e="T52" id="Seg_4728" s="T51">два</ta>
            <ta e="T53" id="Seg_4729" s="T52">год-3SG.[NOM]</ta>
            <ta e="T54" id="Seg_4730" s="T53">проехать-PTCP.PST-3SG-ACC</ta>
            <ta e="T55" id="Seg_4731" s="T54">после.того</ta>
            <ta e="T56" id="Seg_4732" s="T55">девушка-3SG-GEN</ta>
            <ta e="T57" id="Seg_4733" s="T56">тело-3SG.[NOM]</ta>
            <ta e="T58" id="Seg_4734" s="T57">давно</ta>
            <ta e="T59" id="Seg_4735" s="T58">гнить-CVB.SEQ</ta>
            <ta e="T60" id="Seg_4736" s="T59">кость-3PL.[NOM]</ta>
            <ta e="T61" id="Seg_4737" s="T60">только</ta>
            <ta e="T62" id="Seg_4738" s="T61">оставаться-PST2.[3SG]</ta>
            <ta e="T63" id="Seg_4739" s="T62">этот</ta>
            <ta e="T64" id="Seg_4740" s="T63">два</ta>
            <ta e="T65" id="Seg_4741" s="T64">шаман.[NOM]</ta>
            <ta e="T66" id="Seg_4742" s="T65">камлать-CVB.SEQ-3PL</ta>
            <ta e="T67" id="Seg_4743" s="T66">что-ACC</ta>
            <ta e="T68" id="Seg_4744" s="T67">NEG</ta>
            <ta e="T69" id="Seg_4745" s="T68">делать-NEG-3PL</ta>
            <ta e="T70" id="Seg_4746" s="T69">три-ORD</ta>
            <ta e="T71" id="Seg_4747" s="T70">год-3SG.[NOM]</ta>
            <ta e="T72" id="Seg_4748" s="T71">становиться-PRS.[3SG]</ta>
            <ta e="T73" id="Seg_4749" s="T72">тот</ta>
            <ta e="T74" id="Seg_4750" s="T73">Нганасан.[NOM]</ta>
            <ta e="T75" id="Seg_4751" s="T74">близость-3SG-DAT/LOC</ta>
            <ta e="T76" id="Seg_4752" s="T75">провидец.[NOM]</ta>
            <ta e="T77" id="Seg_4753" s="T76">есть</ta>
            <ta e="T78" id="Seg_4754" s="T77">быть-PST2.[3SG]</ta>
            <ta e="T79" id="Seg_4755" s="T78">этот</ta>
            <ta e="T80" id="Seg_4756" s="T79">провидец.[NOM]</ta>
            <ta e="T81" id="Seg_4757" s="T80">очень</ta>
            <ta e="T82" id="Seg_4758" s="T81">жалеть-PRS.[3SG]</ta>
            <ta e="T83" id="Seg_4759" s="T82">Нганасан-3SG-ACC</ta>
            <ta e="T84" id="Seg_4760" s="T83">однажды</ta>
            <ta e="T85" id="Seg_4761" s="T84">сниться-PRS.[3SG]</ta>
            <ta e="T86" id="Seg_4762" s="T85">тот</ta>
            <ta e="T87" id="Seg_4763" s="T86">сниться-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T88" id="Seg_4764" s="T87">после</ta>
            <ta e="T89" id="Seg_4765" s="T88">Нганасан-3SG-DAT/LOC</ta>
            <ta e="T90" id="Seg_4766" s="T89">приходить-CVB.SEQ</ta>
            <ta e="T91" id="Seg_4767" s="T90">рассказывать-PRS.[3SG]</ta>
            <ta e="T92" id="Seg_4768" s="T91">этот</ta>
            <ta e="T93" id="Seg_4769" s="T92">2SG.[NOM]</ta>
            <ta e="T94" id="Seg_4770" s="T93">дочь-EP-2SG.[NOM]</ta>
            <ta e="T95" id="Seg_4771" s="T94">оживать-FUT.[3SG]</ta>
            <ta e="T96" id="Seg_4772" s="T95">наверное</ta>
            <ta e="T97" id="Seg_4773" s="T96">чудо-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_4774" s="T97">один</ta>
            <ta e="T99" id="Seg_4775" s="T98">быть-PST1-3SG</ta>
            <ta e="T100" id="Seg_4776" s="T99">ребенок-2SG.[NOM]</ta>
            <ta e="T101" id="Seg_4777" s="T100">душа-3SG-ACC</ta>
            <ta e="T102" id="Seg_4778" s="T101">есть-PTCP.PST</ta>
            <ta e="T103" id="Seg_4779" s="T102">дух-3SG.[NOM]</ta>
            <ta e="T104" id="Seg_4780" s="T103">море-DAT/LOC</ta>
            <ta e="T105" id="Seg_4781" s="T104">смерть.[NOM]</ta>
            <ta e="T106" id="Seg_4782" s="T105">вода-3SG-DAT/LOC</ta>
            <ta e="T107" id="Seg_4783" s="T106">носить-CVB.SIM</ta>
            <ta e="T108" id="Seg_4784" s="T107">идти-PTCP.PRS</ta>
            <ta e="T109" id="Seg_4785" s="T108">быть-PST2.[3SG]</ta>
            <ta e="T110" id="Seg_4786" s="T109">один.из.двух</ta>
            <ta e="T111" id="Seg_4787" s="T110">шаман-EP-2SG.[NOM]</ta>
            <ta e="T112" id="Seg_4788" s="T111">дикий.олень.[NOM]</ta>
            <ta e="T113" id="Seg_4789" s="T112">становиться-CVB.SEQ</ta>
            <ta e="T114" id="Seg_4790" s="T113">земля-EP-INSTR</ta>
            <ta e="T115" id="Seg_4791" s="T114">следовать-CVB.SEQ</ta>
            <ta e="T116" id="Seg_4792" s="T115">шерсть-3SG.[NOM]</ta>
            <ta e="T117" id="Seg_4793" s="T116">налед.[NOM]</ta>
            <ta e="T118" id="Seg_4794" s="T117">отверстие-DAT/LOC</ta>
            <ta e="T119" id="Seg_4795" s="T118">подходить-NEG.PTCP</ta>
            <ta e="T120" id="Seg_4796" s="T119">быть-PRS.[3SG]</ta>
            <ta e="T121" id="Seg_4797" s="T120">один.из.двух.[NOM]</ta>
            <ta e="T122" id="Seg_4798" s="T121">дикий.олень.[NOM]</ta>
            <ta e="T123" id="Seg_4799" s="T122">становиться-CVB.SEQ</ta>
            <ta e="T124" id="Seg_4800" s="T123">вода-INSTR</ta>
            <ta e="T125" id="Seg_4801" s="T124">течь-CVB.SEQ</ta>
            <ta e="T126" id="Seg_4802" s="T125">доезжать-TEMP-3SG</ta>
            <ta e="T127" id="Seg_4803" s="T126">волна-3SG.[NOM]</ta>
            <ta e="T128" id="Seg_4804" s="T127">поймать-CVB.PURP</ta>
            <ta e="T129" id="Seg_4805" s="T128">только</ta>
            <ta e="T130" id="Seg_4806" s="T129">делать-TEMP-3SG</ta>
            <ta e="T131" id="Seg_4807" s="T130">душа-3SG-ACC</ta>
            <ta e="T132" id="Seg_4808" s="T131">носить-CVB.SEQ</ta>
            <ta e="T133" id="Seg_4809" s="T132">бросать-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_4810" s="T133">девять</ta>
            <ta e="T135" id="Seg_4811" s="T134">год.[NOM]</ta>
            <ta e="T136" id="Seg_4812" s="T135">так</ta>
            <ta e="T137" id="Seg_4813" s="T136">следовать-EP-RECP/COLL-FUT-3PL</ta>
            <ta e="T138" id="Seg_4814" s="T137">потом</ta>
            <ta e="T139" id="Seg_4815" s="T138">как</ta>
            <ta e="T140" id="Seg_4816" s="T139">быть-PRS.[3SG]</ta>
            <ta e="T141" id="Seg_4817" s="T140">тот-DAT/LOC</ta>
            <ta e="T142" id="Seg_4818" s="T141">нганасанский</ta>
            <ta e="T143" id="Seg_4819" s="T142">старик-3SG.[NOM]</ta>
            <ta e="T144" id="Seg_4820" s="T143">говорить-PRS.[3SG]</ta>
            <ta e="T145" id="Seg_4821" s="T144">тот</ta>
            <ta e="T146" id="Seg_4822" s="T145">шаман.[NOM]</ta>
            <ta e="T147" id="Seg_4823" s="T146">мочь-NEG.PTCP-3SG-ACC</ta>
            <ta e="T148" id="Seg_4824" s="T147">знать-PTCP.PRS</ta>
            <ta e="T149" id="Seg_4825" s="T148">человек.[NOM]</ta>
            <ta e="T150" id="Seg_4826" s="T149">сам-2SG.[NOM]</ta>
            <ta e="T151" id="Seg_4827" s="T150">почему</ta>
            <ta e="T152" id="Seg_4828" s="T151">оживать-CAUS-CVB.SEQ</ta>
            <ta e="T153" id="Seg_4829" s="T152">попробовать-NEG-2SG</ta>
            <ta e="T154" id="Seg_4830" s="T153">говорить-PRS.[3SG]</ta>
            <ta e="T155" id="Seg_4831" s="T154">просить-PRS.[3SG]</ta>
            <ta e="T156" id="Seg_4832" s="T155">вот</ta>
            <ta e="T157" id="Seg_4833" s="T156">очень</ta>
            <ta e="T158" id="Seg_4834" s="T157">беда-VBZ-PST1-2SG</ta>
            <ta e="T159" id="Seg_4835" s="T158">говорить-PRS.[3SG]</ta>
            <ta e="T160" id="Seg_4836" s="T159">как</ta>
            <ta e="T161" id="Seg_4837" s="T160">делать-PRS-1SG</ta>
            <ta e="T162" id="Seg_4838" s="T161">оживать-TEMP-3SG</ta>
            <ta e="T163" id="Seg_4839" s="T162">1SG-DAT/LOC</ta>
            <ta e="T164" id="Seg_4840" s="T163">жена.[NOM]</ta>
            <ta e="T165" id="Seg_4841" s="T164">давать-FUT-2SG</ta>
            <ta e="T166" id="Seg_4842" s="T165">Q</ta>
            <ta e="T167" id="Seg_4843" s="T166">богатство-EP-1SG.[NOM]</ta>
            <ta e="T168" id="Seg_4844" s="T167">половина-3SG-ACC</ta>
            <ta e="T169" id="Seg_4845" s="T168">давать-FUT-1SG</ta>
            <ta e="T170" id="Seg_4846" s="T169">сам-3SG-ACC</ta>
            <ta e="T171" id="Seg_4847" s="T170">давать-FUT-1SG</ta>
            <ta e="T172" id="Seg_4848" s="T171">оживать-CAUS.[IMP.2SG]</ta>
            <ta e="T173" id="Seg_4849" s="T172">только</ta>
            <ta e="T174" id="Seg_4850" s="T173">говорить-PRS.[3SG]</ta>
            <ta e="T175" id="Seg_4851" s="T174">провидец-3SG.[NOM]</ta>
            <ta e="T176" id="Seg_4852" s="T175">спать-CVB.SEQ</ta>
            <ta e="T177" id="Seg_4853" s="T176">оставаться-PRS.[3SG]</ta>
            <ta e="T178" id="Seg_4854" s="T177">этот</ta>
            <ta e="T179" id="Seg_4855" s="T178">спать-CVB.SIM</ta>
            <ta e="T180" id="Seg_4856" s="T179">лежать-CVB.SEQ</ta>
            <ta e="T181" id="Seg_4857" s="T180">вот</ta>
            <ta e="T182" id="Seg_4858" s="T181">тот</ta>
            <ta e="T183" id="Seg_4859" s="T182">смерть-3SG-GEN</ta>
            <ta e="T184" id="Seg_4860" s="T183">место-3SG-DAT/LOC</ta>
            <ta e="T185" id="Seg_4861" s="T184">доезжать-PRS.[3SG]</ta>
            <ta e="T186" id="Seg_4862" s="T185">вот</ta>
            <ta e="T187" id="Seg_4863" s="T186">тот</ta>
            <ta e="T188" id="Seg_4864" s="T187">смерть-3SG-GEN</ta>
            <ta e="T189" id="Seg_4865" s="T188">место-3SG-DAT/LOC</ta>
            <ta e="T190" id="Seg_4866" s="T189">доезжать-PRS.[3SG]</ta>
            <ta e="T191" id="Seg_4867" s="T190">доезжать-PST2-3SG</ta>
            <ta e="T192" id="Seg_4868" s="T191">много</ta>
            <ta e="T193" id="Seg_4869" s="T192">очень</ta>
            <ta e="T194" id="Seg_4870" s="T193">люди.[NOM]</ta>
            <ta e="T195" id="Seg_4871" s="T194">плясать-CVB.SIM</ta>
            <ta e="T196" id="Seg_4872" s="T195">стоять-PRS-3PL</ta>
            <ta e="T197" id="Seg_4873" s="T196">здесь</ta>
            <ta e="T198" id="Seg_4874" s="T197">искать-CVB.SIM</ta>
            <ta e="T199" id="Seg_4875" s="T198">делать.напрасно-PRS.[3SG]</ta>
            <ta e="T200" id="Seg_4876" s="T199">девушка-3PL-GEN</ta>
            <ta e="T201" id="Seg_4877" s="T200">душа-3SG-ACC</ta>
            <ta e="T202" id="Seg_4878" s="T201">что.[NOM]</ta>
            <ta e="T203" id="Seg_4879" s="T202">приходить-FUT.[3SG]=Q</ta>
            <ta e="T204" id="Seg_4880" s="T203">вовсе</ta>
            <ta e="T205" id="Seg_4881" s="T204">найти-NEG.[3SG]</ta>
            <ta e="T206" id="Seg_4882" s="T205">потом</ta>
            <ta e="T207" id="Seg_4883" s="T206">этот</ta>
            <ta e="T208" id="Seg_4884" s="T207">место-ABL</ta>
            <ta e="T209" id="Seg_4885" s="T208">другой</ta>
            <ta e="T210" id="Seg_4886" s="T209">место-DAT/LOC</ta>
            <ta e="T211" id="Seg_4887" s="T210">идти-IMP.1SG</ta>
            <ta e="T212" id="Seg_4888" s="T211">думать-PRS.[3SG]</ta>
            <ta e="T213" id="Seg_4889" s="T212">доезжать-PRS.[3SG]</ta>
            <ta e="T214" id="Seg_4890" s="T213">один</ta>
            <ta e="T215" id="Seg_4891" s="T214">место-DAT/LOC</ta>
            <ta e="T216" id="Seg_4892" s="T215">там</ta>
            <ta e="T217" id="Seg_4893" s="T216">три</ta>
            <ta e="T218" id="Seg_4894" s="T217">EMPH=EMPH</ta>
            <ta e="T219" id="Seg_4895" s="T218">одинаковый</ta>
            <ta e="T220" id="Seg_4896" s="T219">лицо-PROPR</ta>
            <ta e="T221" id="Seg_4897" s="T220">одежда-PROPR</ta>
            <ta e="T222" id="Seg_4898" s="T221">девушка.[NOM]</ta>
            <ta e="T223" id="Seg_4899" s="T222">есть</ta>
            <ta e="T224" id="Seg_4900" s="T223">быть-PST2.[3SG]</ta>
            <ta e="T225" id="Seg_4901" s="T224">этот-ABL</ta>
            <ta e="T226" id="Seg_4902" s="T225">другой-3PL.[NOM]</ta>
            <ta e="T227" id="Seg_4903" s="T226">старик.[NOM]</ta>
            <ta e="T228" id="Seg_4904" s="T227">дочь-3SG.[NOM]</ta>
            <ta e="T229" id="Seg_4905" s="T228">какой-3PL.[NOM]</ta>
            <ta e="T230" id="Seg_4906" s="T229">тот</ta>
            <ta e="T231" id="Seg_4907" s="T230">дочь-3SG-ACC</ta>
            <ta e="T232" id="Seg_4908" s="T231">мочь-CVB.SEQ</ta>
            <ta e="T233" id="Seg_4909" s="T232">узнавать-NEG.[3SG]</ta>
            <ta e="T234" id="Seg_4910" s="T233">одежда-3PL-GEN</ta>
            <ta e="T235" id="Seg_4911" s="T234">шов-3SG-ACC</ta>
            <ta e="T236" id="Seg_4912" s="T235">считать-CVB.SEQ</ta>
            <ta e="T237" id="Seg_4913" s="T236">попробовать-PRS.[3SG]</ta>
            <ta e="T238" id="Seg_4914" s="T237">счет-3SG.[NOM]</ta>
            <ta e="T239" id="Seg_4915" s="T238">полностью</ta>
            <ta e="T240" id="Seg_4916" s="T239">один</ta>
            <ta e="T241" id="Seg_4917" s="T240">беда-VBZ-CVB.SIM</ta>
            <ta e="T242" id="Seg_4918" s="T241">делать.напрасно-PRS.[3SG]</ta>
            <ta e="T243" id="Seg_4919" s="T242">этот</ta>
            <ta e="T244" id="Seg_4920" s="T243">девушка-PL.[NOM]</ta>
            <ta e="T245" id="Seg_4921" s="T244">три</ta>
            <ta e="T246" id="Seg_4922" s="T245">дом-PROPR-3PL</ta>
            <ta e="T247" id="Seg_4923" s="T246">сторона.[NOM]</ta>
            <ta e="T248" id="Seg_4924" s="T247">различный.[NOM]</ta>
            <ta e="T249" id="Seg_4925" s="T248">туда</ta>
            <ta e="T250" id="Seg_4926" s="T249">вечером</ta>
            <ta e="T251" id="Seg_4927" s="T250">танец-ABL</ta>
            <ta e="T252" id="Seg_4928" s="T251">приходить-CVB.SEQ</ta>
            <ta e="T253" id="Seg_4929" s="T252">спать-CVB.SEQ</ta>
            <ta e="T254" id="Seg_4930" s="T253">оставаться-PRS-3PL</ta>
            <ta e="T255" id="Seg_4931" s="T254">девушка-PL-3SG-ACC</ta>
            <ta e="T256" id="Seg_4932" s="T255">с</ta>
            <ta e="T257" id="Seg_4933" s="T256">разговаривать-CVB.PURP</ta>
            <ta e="T258" id="Seg_4934" s="T257">хотеть-PRS.[3SG]</ta>
            <ta e="T259" id="Seg_4935" s="T258">другой-3PL.[NOM]</ta>
            <ta e="T260" id="Seg_4936" s="T259">NEG</ta>
            <ta e="T261" id="Seg_4937" s="T260">говорить-NEG.[3SG]</ta>
            <ta e="T262" id="Seg_4938" s="T261">как</ta>
            <ta e="T263" id="Seg_4939" s="T262">становиться-FUT-1SG=Q</ta>
            <ta e="T264" id="Seg_4940" s="T263">думать-CVB.SEQ</ta>
            <ta e="T265" id="Seg_4941" s="T264">задумать-PRS.[3SG]</ta>
            <ta e="T266" id="Seg_4942" s="T265">другой-3PL-GEN</ta>
            <ta e="T267" id="Seg_4943" s="T266">дом-3SG-DAT/LOC</ta>
            <ta e="T268" id="Seg_4944" s="T267">прятаться-CVB.SEQ</ta>
            <ta e="T269" id="Seg_4945" s="T268">входить-CVB.SEQ</ta>
            <ta e="T270" id="Seg_4946" s="T269">спать-PTCP.PST-3SG-ACC</ta>
            <ta e="T271" id="Seg_4947" s="T270">наблюдать-PRS.[3SG]</ta>
            <ta e="T272" id="Seg_4948" s="T271">так</ta>
            <ta e="T273" id="Seg_4949" s="T272">три</ta>
            <ta e="T274" id="Seg_4950" s="T273">дом-DAT/LOC</ta>
            <ta e="T275" id="Seg_4951" s="T274">три-COLL-3PL-DAT/LOC</ta>
            <ta e="T276" id="Seg_4952" s="T275">ночевать-PST1-3SG</ta>
            <ta e="T277" id="Seg_4953" s="T276">да</ta>
            <ta e="T278" id="Seg_4954" s="T277">что-ACC</ta>
            <ta e="T279" id="Seg_4955" s="T278">NEG</ta>
            <ta e="T280" id="Seg_4956" s="T279">найти-CVB.SEQ</ta>
            <ta e="T281" id="Seg_4957" s="T280">взять-NEG-PST1-3SG</ta>
            <ta e="T282" id="Seg_4958" s="T281">вот</ta>
            <ta e="T283" id="Seg_4959" s="T282">однажды</ta>
            <ta e="T284" id="Seg_4960" s="T283">три-COLL</ta>
            <ta e="T285" id="Seg_4961" s="T284">один</ta>
            <ta e="T286" id="Seg_4962" s="T285">дом-DAT/LOC</ta>
            <ta e="T287" id="Seg_4963" s="T286">собираться-REFL-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T288" id="Seg_4964" s="T287">быть-PST2.[3SG]</ta>
            <ta e="T289" id="Seg_4965" s="T288">сюда</ta>
            <ta e="T290" id="Seg_4966" s="T289">входить-CVB.SEQ</ta>
            <ta e="T291" id="Seg_4967" s="T290">провидец.[NOM]</ta>
            <ta e="T292" id="Seg_4968" s="T291">прятаться-CVB.SIM</ta>
            <ta e="T293" id="Seg_4969" s="T292">лежать-PST2.[3SG]</ta>
            <ta e="T294" id="Seg_4970" s="T293">этот</ta>
            <ta e="T295" id="Seg_4971" s="T294">лежать-TEMP-3SG</ta>
            <ta e="T296" id="Seg_4972" s="T295">один</ta>
            <ta e="T297" id="Seg_4973" s="T296">девушка.[NOM]</ta>
            <ta e="T298" id="Seg_4974" s="T297">только</ta>
            <ta e="T299" id="Seg_4975" s="T298">задуматься-PST2.[3SG]</ta>
            <ta e="T300" id="Seg_4976" s="T299">этот-ACC</ta>
            <ta e="T301" id="Seg_4977" s="T300">два</ta>
            <ta e="T302" id="Seg_4978" s="T301">друг-3SG.[NOM]</ta>
            <ta e="T303" id="Seg_4979" s="T302">спрашивать-PST2-3PL</ta>
            <ta e="T304" id="Seg_4980" s="T303">что.[NOM]</ta>
            <ta e="T305" id="Seg_4981" s="T304">быть-CVB.SEQ</ta>
            <ta e="T306" id="Seg_4982" s="T305">задуматься-PRS-2SG</ta>
            <ta e="T307" id="Seg_4983" s="T306">говорить-CVB.SEQ</ta>
            <ta e="T308" id="Seg_4984" s="T307">девушка-3PL.[NOM]</ta>
            <ta e="T309" id="Seg_4985" s="T308">говорить-PST2.[3SG]</ta>
            <ta e="T310" id="Seg_4986" s="T309">1SG.[NOM]</ta>
            <ta e="T311" id="Seg_4987" s="T310">мать-1SG.[NOM]</ta>
            <ta e="T312" id="Seg_4988" s="T311">один</ta>
            <ta e="T313" id="Seg_4989" s="T312">очень</ta>
            <ta e="T314" id="Seg_4990" s="T313">завет-PROPR</ta>
            <ta e="T315" id="Seg_4991" s="T314">вещь-1SG-ACC</ta>
            <ta e="T316" id="Seg_4992" s="T315">давать-EP-NEG.CVB</ta>
            <ta e="T317" id="Seg_4993" s="T316">послать-PST2.[3SG]</ta>
            <ta e="T318" id="Seg_4994" s="T317">тот-ACC</ta>
            <ta e="T319" id="Seg_4995" s="T318">друг-3PL.[NOM]</ta>
            <ta e="T320" id="Seg_4996" s="T319">говорить-PST2-3PL</ta>
            <ta e="T321" id="Seg_4997" s="T320">что</ta>
            <ta e="T322" id="Seg_4998" s="T321">быть-FUT.[3SG]=Q</ta>
            <ta e="T323" id="Seg_4999" s="T322">мать-3SG.[NOM]</ta>
            <ta e="T324" id="Seg_5000" s="T323">приходить-TEMP-3SG</ta>
            <ta e="T325" id="Seg_5001" s="T324">сам-3SG.[NOM]</ta>
            <ta e="T326" id="Seg_5002" s="T325">принести-CVB.SEQ</ta>
            <ta e="T327" id="Seg_5003" s="T326">давать-FUT.[3SG]</ta>
            <ta e="T328" id="Seg_5004" s="T327">MOD</ta>
            <ta e="T329" id="Seg_5005" s="T328">EMPH</ta>
            <ta e="T330" id="Seg_5006" s="T329">вот</ta>
            <ta e="T331" id="Seg_5007" s="T330">этот</ta>
            <ta e="T332" id="Seg_5008" s="T331">быть-PST2.[3SG]</ta>
            <ta e="T333" id="Seg_5009" s="T332">MOD</ta>
            <ta e="T334" id="Seg_5010" s="T333">старик.[NOM]</ta>
            <ta e="T335" id="Seg_5011" s="T334">дочь-3SG.[NOM]</ta>
            <ta e="T336" id="Seg_5012" s="T335">думать-CVB.SIM</ta>
            <ta e="T337" id="Seg_5013" s="T336">думать-PRS.[3SG]</ta>
            <ta e="T338" id="Seg_5014" s="T337">уснуть-PTCP.PST-3SG-ACC</ta>
            <ta e="T339" id="Seg_5015" s="T338">после.того</ta>
            <ta e="T340" id="Seg_5016" s="T339">обнимать-CVB.SEQ</ta>
            <ta e="T341" id="Seg_5017" s="T340">после</ta>
            <ta e="T342" id="Seg_5018" s="T341">вот</ta>
            <ta e="T343" id="Seg_5019" s="T342">летать-PRS.[3SG]</ta>
            <ta e="T344" id="Seg_5020" s="T343">EMPH</ta>
            <ta e="T345" id="Seg_5021" s="T344">тот</ta>
            <ta e="T346" id="Seg_5022" s="T345">время-DAT/LOC</ta>
            <ta e="T347" id="Seg_5023" s="T346">пока</ta>
            <ta e="T348" id="Seg_5024" s="T347">девять</ta>
            <ta e="T349" id="Seg_5025" s="T348">день.[NOM]</ta>
            <ta e="T350" id="Seg_5026" s="T349">спать-CVB.SIM</ta>
            <ta e="T351" id="Seg_5027" s="T350">лежать-PST2.[3SG]</ta>
            <ta e="T352" id="Seg_5028" s="T351">вот</ta>
            <ta e="T353" id="Seg_5029" s="T352">обнимать-CVB.SEQ</ta>
            <ta e="T354" id="Seg_5030" s="T353">после</ta>
            <ta e="T355" id="Seg_5031" s="T354">прыгать-CVB.SEQ</ta>
            <ta e="T356" id="Seg_5032" s="T355">стоять-PRS.[3SG]</ta>
            <ta e="T357" id="Seg_5033" s="T356">EMPH</ta>
            <ta e="T358" id="Seg_5034" s="T357">просыпаться-CVB.SEQ</ta>
            <ta e="T359" id="Seg_5035" s="T358">здесь</ta>
            <ta e="T360" id="Seg_5036" s="T359">бегать-CVB.SEQ-3PL</ta>
            <ta e="T361" id="Seg_5037" s="T360">помощник-ACC</ta>
            <ta e="T362" id="Seg_5038" s="T361">принести-PRS-3PL</ta>
            <ta e="T363" id="Seg_5039" s="T362">кость-PL-3SG-ACC</ta>
            <ta e="T364" id="Seg_5040" s="T363">принести-EP-IMP.2PL</ta>
            <ta e="T365" id="Seg_5041" s="T364">говорить-CVB.SIM-говорить-CVB.SIM</ta>
            <ta e="T366" id="Seg_5042" s="T365">камлать-CVB.SEQ</ta>
            <ta e="T367" id="Seg_5043" s="T366">%%-PRS.[3SG]</ta>
            <ta e="T368" id="Seg_5044" s="T367">EMPH</ta>
            <ta e="T369" id="Seg_5045" s="T368">три</ta>
            <ta e="T370" id="Seg_5046" s="T369">ночь-PROPR</ta>
            <ta e="T371" id="Seg_5047" s="T370">день-ACC</ta>
            <ta e="T372" id="Seg_5048" s="T371">смерть.[NOM]</ta>
            <ta e="T373" id="Seg_5049" s="T372">страна-3SG-ABL</ta>
            <ta e="T374" id="Seg_5050" s="T373">возвращаться-CVB.SEQ</ta>
            <ta e="T375" id="Seg_5051" s="T374">тот-DAT/LOC</ta>
            <ta e="T376" id="Seg_5052" s="T375">олень.[NOM]</ta>
            <ta e="T377" id="Seg_5053" s="T376">кожа-3SG-DAT/LOC</ta>
            <ta e="T378" id="Seg_5054" s="T377">принести-PRS-3PL</ta>
            <ta e="T379" id="Seg_5055" s="T378">кость-3PL-ACC</ta>
            <ta e="T380" id="Seg_5056" s="T379">видеть-PST2-3PL</ta>
            <ta e="T381" id="Seg_5057" s="T380">девушка-ACC</ta>
            <ta e="T382" id="Seg_5058" s="T381">поднимать-CVB.SEQ</ta>
            <ta e="T383" id="Seg_5059" s="T382">стоять-PRS.[3SG]</ta>
            <ta e="T384" id="Seg_5060" s="T383">только</ta>
            <ta e="T385" id="Seg_5061" s="T384">девушка-3PL.[NOM]</ta>
            <ta e="T386" id="Seg_5062" s="T385">двигаться-NEG.[3SG]</ta>
            <ta e="T387" id="Seg_5063" s="T386">умирать-PTCP.PST.[NOM]</ta>
            <ta e="T388" id="Seg_5064" s="T387">MOD</ta>
            <ta e="T389" id="Seg_5065" s="T388">уснуть-PTCP.PST.[NOM]</ta>
            <ta e="T390" id="Seg_5066" s="T389">MOD</ta>
            <ta e="T391" id="Seg_5067" s="T390">подобно</ta>
            <ta e="T392" id="Seg_5068" s="T391">этот</ta>
            <ta e="T393" id="Seg_5069" s="T392">девушка.[NOM]</ta>
            <ta e="T394" id="Seg_5070" s="T393">кость-3PL-ACC</ta>
            <ta e="T395" id="Seg_5071" s="T394">нутро-3SG-DAT/LOC</ta>
            <ta e="T396" id="Seg_5072" s="T395">сокуй-SIM</ta>
            <ta e="T397" id="Seg_5073" s="T396">класть-PRS.[3SG]</ta>
            <ta e="T398" id="Seg_5074" s="T397">EMPH</ta>
            <ta e="T399" id="Seg_5075" s="T398">этот-3SG-3SG.[NOM]</ta>
            <ta e="T400" id="Seg_5076" s="T399">оживать-CVB.SEQ</ta>
            <ta e="T401" id="Seg_5077" s="T400">приходить-CVB.SEQ</ta>
            <ta e="T402" id="Seg_5078" s="T401">вот</ta>
            <ta e="T403" id="Seg_5079" s="T402">этот</ta>
            <ta e="T404" id="Seg_5080" s="T403">старик-EP-2SG.[NOM]</ta>
            <ta e="T405" id="Seg_5081" s="T404">дочь-3SG.[NOM]</ta>
            <ta e="T406" id="Seg_5082" s="T405">быть-PRS.[3SG]</ta>
            <ta e="T407" id="Seg_5083" s="T406">этот-ACC</ta>
            <ta e="T408" id="Seg_5084" s="T407">провидец-EP-2SG.[NOM]</ta>
            <ta e="T409" id="Seg_5085" s="T408">жена.[NOM]</ta>
            <ta e="T410" id="Seg_5086" s="T409">делать-CVB.SEQ</ta>
            <ta e="T411" id="Seg_5087" s="T410">жить-PRS.[3SG]</ta>
            <ta e="T412" id="Seg_5088" s="T411">оставаться-PTCP.HAB</ta>
            <ta e="T413" id="Seg_5089" s="T412">два</ta>
            <ta e="T414" id="Seg_5090" s="T413">друг-3PL.[NOM]</ta>
            <ta e="T415" id="Seg_5091" s="T414">танец-DAT/LOC</ta>
            <ta e="T416" id="Seg_5092" s="T415">зазывать-CVB.PURP</ta>
            <ta e="T417" id="Seg_5093" s="T416">хотеть-PST2-3PL</ta>
            <ta e="T418" id="Seg_5094" s="T417">человек-3PL.[NOM]</ta>
            <ta e="T419" id="Seg_5095" s="T418">NEG.EX</ta>
            <ta e="T420" id="Seg_5096" s="T419">узнавать-PST2-3PL</ta>
            <ta e="T421" id="Seg_5097" s="T420">шаман.[NOM]</ta>
            <ta e="T422" id="Seg_5098" s="T421">носить-PST2.[3SG]</ta>
            <ta e="T423" id="Seg_5099" s="T422">этот-ACC</ta>
            <ta e="T424" id="Seg_5100" s="T423">вот</ta>
            <ta e="T425" id="Seg_5101" s="T424">рассердиться-PRS-3PL</ta>
            <ta e="T426" id="Seg_5102" s="T425">оживать-PTCP.HAB-2SG-ACC</ta>
            <ta e="T427" id="Seg_5103" s="T426">найти-CVB.SEQ</ta>
            <ta e="T428" id="Seg_5104" s="T427">мысль-3SG-ACC</ta>
            <ta e="T429" id="Seg_5105" s="T428">повернуть-CVB.SEQ</ta>
            <ta e="T430" id="Seg_5106" s="T429">душа.покойника.[NOM]</ta>
            <ta e="T431" id="Seg_5107" s="T430">становиться-CVB.SEQ</ta>
            <ta e="T432" id="Seg_5108" s="T431">шаманка.[NOM]</ta>
            <ta e="T433" id="Seg_5109" s="T432">делать-PRS-3PL</ta>
            <ta e="T434" id="Seg_5110" s="T433">шаманка.[NOM]</ta>
            <ta e="T435" id="Seg_5111" s="T434">делать-CVB.PURP</ta>
            <ta e="T436" id="Seg_5112" s="T435">принуждать-PRS-3PL</ta>
            <ta e="T437" id="Seg_5113" s="T436">1PL.[NOM]</ta>
            <ta e="T438" id="Seg_5114" s="T437">2SG-ACC</ta>
            <ta e="T439" id="Seg_5115" s="T438">есть-FUT-1PL</ta>
            <ta e="T440" id="Seg_5116" s="T439">старуха.[NOM]</ta>
            <ta e="T441" id="Seg_5117" s="T440">кожа-3SG-ACC</ta>
            <ta e="T442" id="Seg_5118" s="T441">сдирать.кожу-CVB.SEQ-2SG</ta>
            <ta e="T443" id="Seg_5119" s="T442">шаманский.бубен.[NOM]</ta>
            <ta e="T444" id="Seg_5120" s="T443">делать-EP-NEG-TEMP-2SG</ta>
            <ta e="T445" id="Seg_5121" s="T444">вот</ta>
            <ta e="T446" id="Seg_5122" s="T445">там</ta>
            <ta e="T447" id="Seg_5123" s="T446">один</ta>
            <ta e="T448" id="Seg_5124" s="T447">старуха-ACC</ta>
            <ta e="T449" id="Seg_5125" s="T448">сдирать.кожу-CVB.SEQ-3PL</ta>
            <ta e="T450" id="Seg_5126" s="T449">тот</ta>
            <ta e="T451" id="Seg_5127" s="T450">кожа-3SG-INSTR</ta>
            <ta e="T452" id="Seg_5128" s="T451">шаманский.бубен.[NOM]</ta>
            <ta e="T453" id="Seg_5129" s="T452">делать-PRS-3PL</ta>
            <ta e="T454" id="Seg_5130" s="T453">тот</ta>
            <ta e="T455" id="Seg_5131" s="T454">грех-3SG-DAT/LOC</ta>
            <ta e="T456" id="Seg_5132" s="T455">тот.[NOM]</ta>
            <ta e="T457" id="Seg_5133" s="T456">быть.больным-PTCP.PRS</ta>
            <ta e="T458" id="Seg_5134" s="T457">MOD</ta>
            <ta e="T459" id="Seg_5135" s="T458">да</ta>
            <ta e="T460" id="Seg_5136" s="T459">Нганасан.[NOM]</ta>
            <ta e="T461" id="Seg_5137" s="T460">один-SIM</ta>
            <ta e="T462" id="Seg_5138" s="T461">умирать-NEG.[3SG]</ta>
            <ta e="T463" id="Seg_5139" s="T462">Нганасан.[NOM]</ta>
            <ta e="T464" id="Seg_5140" s="T463">орда-INSTR</ta>
            <ta e="T465" id="Seg_5141" s="T464">кончаться-PRS.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_5142" s="T0">adv</ta>
            <ta e="T2" id="Seg_5143" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_5144" s="T2">adj</ta>
            <ta e="T4" id="Seg_5145" s="T3">n-n:(poss)-n:case</ta>
            <ta e="T5" id="Seg_5146" s="T4">v-v:tense-v:pred.pn</ta>
            <ta e="T6" id="Seg_5147" s="T5">n-n&gt;adj-n:case</ta>
            <ta e="T7" id="Seg_5148" s="T6">dempro</ta>
            <ta e="T8" id="Seg_5149" s="T7">n-n:(poss)-n:case</ta>
            <ta e="T9" id="Seg_5150" s="T8">v-v:cvb</ta>
            <ta e="T10" id="Seg_5151" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_5152" s="T10">v-v:cvb</ta>
            <ta e="T12" id="Seg_5153" s="T11">post</ta>
            <ta e="T13" id="Seg_5154" s="T12">v-v:cvb</ta>
            <ta e="T14" id="Seg_5155" s="T13">v-v:tense-v:pred.pn</ta>
            <ta e="T15" id="Seg_5156" s="T14">dempro-pro:case</ta>
            <ta e="T16" id="Seg_5157" s="T15">n-n:(poss)-n:case</ta>
            <ta e="T17" id="Seg_5158" s="T16">adv-adv&gt;adv</ta>
            <ta e="T18" id="Seg_5159" s="T17">v-v:tense-v:pred.pn</ta>
            <ta e="T19" id="Seg_5160" s="T18">v-v:cvb</ta>
            <ta e="T20" id="Seg_5161" s="T19">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T21" id="Seg_5162" s="T20">v-v:tense-v:pred.pn</ta>
            <ta e="T22" id="Seg_5163" s="T21">pers-pro:case</ta>
            <ta e="T23" id="Seg_5164" s="T22">n-n:poss-n:case</ta>
            <ta e="T24" id="Seg_5165" s="T23">adv</ta>
            <ta e="T25" id="Seg_5166" s="T24">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T26" id="Seg_5167" s="T25">post</ta>
            <ta e="T27" id="Seg_5168" s="T26">v-v:cvb</ta>
            <ta e="T28" id="Seg_5169" s="T27">v-v:tense-v:poss.pn</ta>
            <ta e="T29" id="Seg_5170" s="T28">pers-pro:case</ta>
            <ta e="T30" id="Seg_5171" s="T29">v-v:tense-v:poss.pn</ta>
            <ta e="T31" id="Seg_5172" s="T30">adj</ta>
            <ta e="T32" id="Seg_5173" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_5174" s="T32">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T34" id="Seg_5175" s="T33">v-v:tense-v:pred.pn</ta>
            <ta e="T35" id="Seg_5176" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_5177" s="T35">n-n:poss-n:case</ta>
            <ta e="T37" id="Seg_5178" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_5179" s="T37">v-v:cvb</ta>
            <ta e="T39" id="Seg_5180" s="T38">v-v:tense-v:pred.pn</ta>
            <ta e="T40" id="Seg_5181" s="T39">dempro</ta>
            <ta e="T41" id="Seg_5182" s="T40">v-v:cvb</ta>
            <ta e="T42" id="Seg_5183" s="T41">n-n:(num)-n:case</ta>
            <ta e="T43" id="Seg_5184" s="T42">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T44" id="Seg_5185" s="T43">cardnum-n:(poss)-n:case</ta>
            <ta e="T45" id="Seg_5186" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_5187" s="T45">v-v:(neg)-v:pred.pn</ta>
            <ta e="T47" id="Seg_5188" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_5189" s="T47">cardnum</ta>
            <ta e="T49" id="Seg_5190" s="T48">adj</ta>
            <ta e="T50" id="Seg_5191" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_5192" s="T50">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T52" id="Seg_5193" s="T51">cardnum</ta>
            <ta e="T53" id="Seg_5194" s="T52">n-n:(poss)-n:case</ta>
            <ta e="T54" id="Seg_5195" s="T53">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T55" id="Seg_5196" s="T54">post</ta>
            <ta e="T56" id="Seg_5197" s="T55">n-n:poss-n:case</ta>
            <ta e="T57" id="Seg_5198" s="T56">n-n:(poss)-n:case</ta>
            <ta e="T58" id="Seg_5199" s="T57">adv</ta>
            <ta e="T59" id="Seg_5200" s="T58">v-v:cvb</ta>
            <ta e="T60" id="Seg_5201" s="T59">n-n:(poss)-n:case</ta>
            <ta e="T61" id="Seg_5202" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_5203" s="T61">v-v:tense-v:pred.pn</ta>
            <ta e="T63" id="Seg_5204" s="T62">dempro</ta>
            <ta e="T64" id="Seg_5205" s="T63">cardnum</ta>
            <ta e="T65" id="Seg_5206" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_5207" s="T65">v-v:cvb-v:pred.pn</ta>
            <ta e="T67" id="Seg_5208" s="T66">que-pro:case</ta>
            <ta e="T68" id="Seg_5209" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_5210" s="T68">v-v:(neg)-v:pred.pn</ta>
            <ta e="T70" id="Seg_5211" s="T69">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T71" id="Seg_5212" s="T70">n-n:(poss)-n:case</ta>
            <ta e="T72" id="Seg_5213" s="T71">v-v:tense-v:pred.pn</ta>
            <ta e="T73" id="Seg_5214" s="T72">dempro</ta>
            <ta e="T74" id="Seg_5215" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_5216" s="T74">n-n:poss-n:case</ta>
            <ta e="T76" id="Seg_5217" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_5218" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_5219" s="T77">v-v:tense-v:pred.pn</ta>
            <ta e="T79" id="Seg_5220" s="T78">dempro</ta>
            <ta e="T80" id="Seg_5221" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_5222" s="T80">adv</ta>
            <ta e="T82" id="Seg_5223" s="T81">v-v:tense-v:pred.pn</ta>
            <ta e="T83" id="Seg_5224" s="T82">n-n:poss-n:case</ta>
            <ta e="T84" id="Seg_5225" s="T83">adv</ta>
            <ta e="T85" id="Seg_5226" s="T84">v-v:tense-v:pred.pn</ta>
            <ta e="T86" id="Seg_5227" s="T85">dempro</ta>
            <ta e="T87" id="Seg_5228" s="T86">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T88" id="Seg_5229" s="T87">post</ta>
            <ta e="T89" id="Seg_5230" s="T88">n-n:poss-n:case</ta>
            <ta e="T90" id="Seg_5231" s="T89">v-v:cvb</ta>
            <ta e="T91" id="Seg_5232" s="T90">v-v:tense-v:pred.pn</ta>
            <ta e="T92" id="Seg_5233" s="T91">dempro</ta>
            <ta e="T93" id="Seg_5234" s="T92">pers-pro:case</ta>
            <ta e="T94" id="Seg_5235" s="T93">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T95" id="Seg_5236" s="T94">v-v:tense.[v:pred.pn]</ta>
            <ta e="T96" id="Seg_5237" s="T95">adv</ta>
            <ta e="T97" id="Seg_5238" s="T96">n-n:(poss)-n:case</ta>
            <ta e="T98" id="Seg_5239" s="T97">cardnum</ta>
            <ta e="T99" id="Seg_5240" s="T98">v-v:tense-v:poss.pn</ta>
            <ta e="T100" id="Seg_5241" s="T99">n-n:(poss)-n:case</ta>
            <ta e="T101" id="Seg_5242" s="T100">n-n:poss-n:case</ta>
            <ta e="T102" id="Seg_5243" s="T101">v-v:ptcp</ta>
            <ta e="T103" id="Seg_5244" s="T102">n-n:(poss)-n:case</ta>
            <ta e="T104" id="Seg_5245" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_5246" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_5247" s="T105">n-n:poss-n:case</ta>
            <ta e="T107" id="Seg_5248" s="T106">v-v:cvb</ta>
            <ta e="T108" id="Seg_5249" s="T107">v-v:ptcp</ta>
            <ta e="T109" id="Seg_5250" s="T108">v-v:tense-v:pred.pn</ta>
            <ta e="T110" id="Seg_5251" s="T109">adj</ta>
            <ta e="T111" id="Seg_5252" s="T110">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T112" id="Seg_5253" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_5254" s="T112">v-v:cvb</ta>
            <ta e="T114" id="Seg_5255" s="T113">n-n:(ins)-n:case</ta>
            <ta e="T115" id="Seg_5256" s="T114">v-v:cvb</ta>
            <ta e="T116" id="Seg_5257" s="T115">n-n:(poss)-n:case</ta>
            <ta e="T117" id="Seg_5258" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_5259" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_5260" s="T118">v-v:ptcp</ta>
            <ta e="T120" id="Seg_5261" s="T119">v-v:tense-v:pred.pn</ta>
            <ta e="T121" id="Seg_5262" s="T120">adj.[n:case]</ta>
            <ta e="T122" id="Seg_5263" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_5264" s="T122">v-v:cvb</ta>
            <ta e="T124" id="Seg_5265" s="T123">n-n:case</ta>
            <ta e="T125" id="Seg_5266" s="T124">v-v:cvb</ta>
            <ta e="T126" id="Seg_5267" s="T125">v-v:mood-v:temp.pn</ta>
            <ta e="T127" id="Seg_5268" s="T126">n-n:(poss)-n:case</ta>
            <ta e="T128" id="Seg_5269" s="T127">v-v:cvb</ta>
            <ta e="T129" id="Seg_5270" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_5271" s="T129">v-v:mood-v:temp.pn</ta>
            <ta e="T131" id="Seg_5272" s="T130">n-n:poss-n:case</ta>
            <ta e="T132" id="Seg_5273" s="T131">v-v:cvb</ta>
            <ta e="T133" id="Seg_5274" s="T132">v-v:tense-v:pred.pn</ta>
            <ta e="T134" id="Seg_5275" s="T133">cardnum</ta>
            <ta e="T135" id="Seg_5276" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_5277" s="T135">adv</ta>
            <ta e="T137" id="Seg_5278" s="T136">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T138" id="Seg_5279" s="T137">adv</ta>
            <ta e="T139" id="Seg_5280" s="T138">que</ta>
            <ta e="T140" id="Seg_5281" s="T139">v-v:tense-v:pred.pn</ta>
            <ta e="T141" id="Seg_5282" s="T140">dempro-pro:case</ta>
            <ta e="T142" id="Seg_5283" s="T141">adj</ta>
            <ta e="T143" id="Seg_5284" s="T142">n-n:(poss)-n:case</ta>
            <ta e="T144" id="Seg_5285" s="T143">v-v:tense-v:pred.pn</ta>
            <ta e="T145" id="Seg_5286" s="T144">dempro</ta>
            <ta e="T146" id="Seg_5287" s="T145">n-n:case</ta>
            <ta e="T147" id="Seg_5288" s="T146">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T148" id="Seg_5289" s="T147">v-v:ptcp</ta>
            <ta e="T149" id="Seg_5290" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_5291" s="T149">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T151" id="Seg_5292" s="T150">que</ta>
            <ta e="T152" id="Seg_5293" s="T151">v-v&gt;v-v:cvb</ta>
            <ta e="T153" id="Seg_5294" s="T152">v-v:(neg)-v:pred.pn</ta>
            <ta e="T154" id="Seg_5295" s="T153">v-v:tense-v:pred.pn</ta>
            <ta e="T155" id="Seg_5296" s="T154">v-v:tense-v:pred.pn</ta>
            <ta e="T156" id="Seg_5297" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_5298" s="T156">adv</ta>
            <ta e="T158" id="Seg_5299" s="T157">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T159" id="Seg_5300" s="T158">v-v:tense-v:pred.pn</ta>
            <ta e="T160" id="Seg_5301" s="T159">que</ta>
            <ta e="T161" id="Seg_5302" s="T160">v-v:tense-v:pred.pn</ta>
            <ta e="T162" id="Seg_5303" s="T161">v-v:mood-v:temp.pn</ta>
            <ta e="T163" id="Seg_5304" s="T162">pers-pro:case</ta>
            <ta e="T164" id="Seg_5305" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_5306" s="T164">v-v:tense-v:poss.pn</ta>
            <ta e="T166" id="Seg_5307" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_5308" s="T166">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T168" id="Seg_5309" s="T167">n-n:poss-n:case</ta>
            <ta e="T169" id="Seg_5310" s="T168">v-v:tense-v:poss.pn</ta>
            <ta e="T170" id="Seg_5311" s="T169">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T171" id="Seg_5312" s="T170">v-v:tense-v:poss.pn</ta>
            <ta e="T172" id="Seg_5313" s="T171">v-v&gt;v-v:mood.pn</ta>
            <ta e="T173" id="Seg_5314" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_5315" s="T173">v-v:tense-v:pred.pn</ta>
            <ta e="T175" id="Seg_5316" s="T174">n-n:(poss)-n:case</ta>
            <ta e="T176" id="Seg_5317" s="T175">v-v:cvb</ta>
            <ta e="T177" id="Seg_5318" s="T176">v-v:tense-v:pred.pn</ta>
            <ta e="T178" id="Seg_5319" s="T177">dempro</ta>
            <ta e="T179" id="Seg_5320" s="T178">v-v:cvb</ta>
            <ta e="T180" id="Seg_5321" s="T179">v-v:cvb</ta>
            <ta e="T181" id="Seg_5322" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_5323" s="T181">dempro</ta>
            <ta e="T183" id="Seg_5324" s="T182">n-n:poss-n:case</ta>
            <ta e="T184" id="Seg_5325" s="T183">n-n:poss-n:case</ta>
            <ta e="T185" id="Seg_5326" s="T184">v-v:tense-v:pred.pn</ta>
            <ta e="T186" id="Seg_5327" s="T185">ptcl</ta>
            <ta e="T187" id="Seg_5328" s="T186">dempro</ta>
            <ta e="T188" id="Seg_5329" s="T187">n-n:poss-n:case</ta>
            <ta e="T189" id="Seg_5330" s="T188">n-n:poss-n:case</ta>
            <ta e="T190" id="Seg_5331" s="T189">v-v:tense-v:pred.pn</ta>
            <ta e="T191" id="Seg_5332" s="T190">v-v:tense-v:poss.pn</ta>
            <ta e="T192" id="Seg_5333" s="T191">quant</ta>
            <ta e="T193" id="Seg_5334" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_5335" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_5336" s="T194">v-v:cvb</ta>
            <ta e="T196" id="Seg_5337" s="T195">v-v:tense-v:pred.pn</ta>
            <ta e="T197" id="Seg_5338" s="T196">adv</ta>
            <ta e="T198" id="Seg_5339" s="T197">v-v:cvb</ta>
            <ta e="T199" id="Seg_5340" s="T198">v-v:tense-v:pred.pn</ta>
            <ta e="T200" id="Seg_5341" s="T199">n-n:poss-n:case</ta>
            <ta e="T201" id="Seg_5342" s="T200">n-n:poss-n:case</ta>
            <ta e="T202" id="Seg_5343" s="T201">que-pro:case</ta>
            <ta e="T203" id="Seg_5344" s="T202">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T204" id="Seg_5345" s="T203">adv</ta>
            <ta e="T205" id="Seg_5346" s="T204">v-v:(neg)-v:pred.pn</ta>
            <ta e="T206" id="Seg_5347" s="T205">adv</ta>
            <ta e="T207" id="Seg_5348" s="T206">dempro</ta>
            <ta e="T208" id="Seg_5349" s="T207">n-n:case</ta>
            <ta e="T209" id="Seg_5350" s="T208">adj</ta>
            <ta e="T210" id="Seg_5351" s="T209">n-n:case</ta>
            <ta e="T211" id="Seg_5352" s="T210">v-v:mood.pn</ta>
            <ta e="T212" id="Seg_5353" s="T211">v-v:tense-v:pred.pn</ta>
            <ta e="T213" id="Seg_5354" s="T212">v-v:tense-v:pred.pn</ta>
            <ta e="T214" id="Seg_5355" s="T213">cardnum</ta>
            <ta e="T215" id="Seg_5356" s="T214">n-n:case</ta>
            <ta e="T216" id="Seg_5357" s="T215">adv</ta>
            <ta e="T217" id="Seg_5358" s="T216">cardnum</ta>
            <ta e="T218" id="Seg_5359" s="T217">adj&gt;adj-adj</ta>
            <ta e="T219" id="Seg_5360" s="T218">adj</ta>
            <ta e="T220" id="Seg_5361" s="T219">n-n&gt;adj</ta>
            <ta e="T221" id="Seg_5362" s="T220">n-n&gt;adj</ta>
            <ta e="T222" id="Seg_5363" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_5364" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_5365" s="T223">v-v:tense-v:pred.pn</ta>
            <ta e="T225" id="Seg_5366" s="T224">dempro-pro:case</ta>
            <ta e="T226" id="Seg_5367" s="T225">adj-n:(poss)-n:case</ta>
            <ta e="T227" id="Seg_5368" s="T226">n-n:case</ta>
            <ta e="T228" id="Seg_5369" s="T227">n-n:(poss)-n:case</ta>
            <ta e="T229" id="Seg_5370" s="T228">que-pro:(poss)-pro:case</ta>
            <ta e="T230" id="Seg_5371" s="T229">dempro</ta>
            <ta e="T231" id="Seg_5372" s="T230">n-n:poss-n:case</ta>
            <ta e="T232" id="Seg_5373" s="T231">v-v:cvb</ta>
            <ta e="T233" id="Seg_5374" s="T232">v-v:(neg)-v:pred.pn</ta>
            <ta e="T234" id="Seg_5375" s="T233">n-n:poss-n:case</ta>
            <ta e="T235" id="Seg_5376" s="T234">n-n:poss-n:case</ta>
            <ta e="T236" id="Seg_5377" s="T235">v-v:cvb</ta>
            <ta e="T237" id="Seg_5378" s="T236">v-v:tense-v:pred.pn</ta>
            <ta e="T238" id="Seg_5379" s="T237">n-n:(poss)-n:case</ta>
            <ta e="T239" id="Seg_5380" s="T238">adv</ta>
            <ta e="T240" id="Seg_5381" s="T239">cardnum</ta>
            <ta e="T241" id="Seg_5382" s="T240">n-n&gt;v-v:cvb</ta>
            <ta e="T242" id="Seg_5383" s="T241">v-v:tense-v:pred.pn</ta>
            <ta e="T243" id="Seg_5384" s="T242">dempro</ta>
            <ta e="T244" id="Seg_5385" s="T243">n-n:(num)-n:case</ta>
            <ta e="T245" id="Seg_5386" s="T244">cardnum</ta>
            <ta e="T246" id="Seg_5387" s="T245">n-n&gt;adj-n:pred.pn</ta>
            <ta e="T247" id="Seg_5388" s="T246">n-n:case</ta>
            <ta e="T248" id="Seg_5389" s="T247">adj-n:case</ta>
            <ta e="T249" id="Seg_5390" s="T248">adv</ta>
            <ta e="T250" id="Seg_5391" s="T249">adv</ta>
            <ta e="T251" id="Seg_5392" s="T250">n-n:case</ta>
            <ta e="T252" id="Seg_5393" s="T251">v-v:cvb</ta>
            <ta e="T253" id="Seg_5394" s="T252">v-v:cvb</ta>
            <ta e="T254" id="Seg_5395" s="T253">v-v:tense-v:pred.pn</ta>
            <ta e="T255" id="Seg_5396" s="T254">n-n:(num)-n:poss-n:case</ta>
            <ta e="T256" id="Seg_5397" s="T255">post</ta>
            <ta e="T257" id="Seg_5398" s="T256">v-v:cvb</ta>
            <ta e="T258" id="Seg_5399" s="T257">v-v:tense-v:pred.pn</ta>
            <ta e="T259" id="Seg_5400" s="T258">adj-n:(poss)-n:case</ta>
            <ta e="T260" id="Seg_5401" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_5402" s="T260">v-v:(neg)-v:pred.pn</ta>
            <ta e="T262" id="Seg_5403" s="T261">que</ta>
            <ta e="T263" id="Seg_5404" s="T262">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T264" id="Seg_5405" s="T263">v-v:cvb</ta>
            <ta e="T265" id="Seg_5406" s="T264">v-v:tense-v:pred.pn</ta>
            <ta e="T266" id="Seg_5407" s="T265">adj-n:poss-n:case</ta>
            <ta e="T267" id="Seg_5408" s="T266">n-n:poss-n:case</ta>
            <ta e="T268" id="Seg_5409" s="T267">v-v:cvb</ta>
            <ta e="T269" id="Seg_5410" s="T268">v-v:cvb</ta>
            <ta e="T270" id="Seg_5411" s="T269">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T271" id="Seg_5412" s="T270">v-v:tense-v:pred.pn</ta>
            <ta e="T272" id="Seg_5413" s="T271">adv</ta>
            <ta e="T273" id="Seg_5414" s="T272">cardnum</ta>
            <ta e="T274" id="Seg_5415" s="T273">n-n:case</ta>
            <ta e="T275" id="Seg_5416" s="T274">cardnum-cardnum&gt;collnum-n:poss-n:case</ta>
            <ta e="T276" id="Seg_5417" s="T275">v-v:tense-v:poss.pn</ta>
            <ta e="T277" id="Seg_5418" s="T276">conj</ta>
            <ta e="T278" id="Seg_5419" s="T277">que-pro:case</ta>
            <ta e="T279" id="Seg_5420" s="T278">ptcl</ta>
            <ta e="T280" id="Seg_5421" s="T279">v-v:cvb</ta>
            <ta e="T281" id="Seg_5422" s="T280">v-v:(neg)-v:tense-v:poss.pn</ta>
            <ta e="T282" id="Seg_5423" s="T281">ptcl</ta>
            <ta e="T283" id="Seg_5424" s="T282">adv</ta>
            <ta e="T284" id="Seg_5425" s="T283">cardnum-cardnum&gt;collnum</ta>
            <ta e="T285" id="Seg_5426" s="T284">cardnum</ta>
            <ta e="T286" id="Seg_5427" s="T285">n-n:case</ta>
            <ta e="T287" id="Seg_5428" s="T286">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T288" id="Seg_5429" s="T287">v-v:tense-v:pred.pn</ta>
            <ta e="T289" id="Seg_5430" s="T288">adv</ta>
            <ta e="T290" id="Seg_5431" s="T289">v-v:cvb</ta>
            <ta e="T291" id="Seg_5432" s="T290">n-n:case</ta>
            <ta e="T292" id="Seg_5433" s="T291">v-v:cvb</ta>
            <ta e="T293" id="Seg_5434" s="T292">v-v:tense-v:pred.pn</ta>
            <ta e="T294" id="Seg_5435" s="T293">dempro</ta>
            <ta e="T295" id="Seg_5436" s="T294">v-v:mood-v:temp.pn</ta>
            <ta e="T296" id="Seg_5437" s="T295">cardnum</ta>
            <ta e="T297" id="Seg_5438" s="T296">n-n:case</ta>
            <ta e="T298" id="Seg_5439" s="T297">ptcl</ta>
            <ta e="T299" id="Seg_5440" s="T298">v-v:tense-v:pred.pn</ta>
            <ta e="T300" id="Seg_5441" s="T299">dempro-pro:case</ta>
            <ta e="T301" id="Seg_5442" s="T300">cardnum</ta>
            <ta e="T302" id="Seg_5443" s="T301">n-n:(poss)-n:case</ta>
            <ta e="T303" id="Seg_5444" s="T302">v-v:tense-v:pred.pn</ta>
            <ta e="T304" id="Seg_5445" s="T303">que-pro:case</ta>
            <ta e="T305" id="Seg_5446" s="T304">v-v:cvb</ta>
            <ta e="T306" id="Seg_5447" s="T305">v-v:tense-v:pred.pn</ta>
            <ta e="T307" id="Seg_5448" s="T306">v-v:cvb</ta>
            <ta e="T308" id="Seg_5449" s="T307">n-n:(poss)-n:case</ta>
            <ta e="T309" id="Seg_5450" s="T308">v-v:tense-v:pred.pn</ta>
            <ta e="T310" id="Seg_5451" s="T309">pers-pro:case</ta>
            <ta e="T311" id="Seg_5452" s="T310">n-n:(poss)-n:case</ta>
            <ta e="T312" id="Seg_5453" s="T311">cardnum</ta>
            <ta e="T313" id="Seg_5454" s="T312">adv</ta>
            <ta e="T314" id="Seg_5455" s="T313">n-n&gt;adj</ta>
            <ta e="T315" id="Seg_5456" s="T314">n-n:poss-n:case</ta>
            <ta e="T316" id="Seg_5457" s="T315">v-v:(ins)-v:cvb</ta>
            <ta e="T317" id="Seg_5458" s="T316">v-v:tense-v:pred.pn</ta>
            <ta e="T318" id="Seg_5459" s="T317">dempro-pro:case</ta>
            <ta e="T319" id="Seg_5460" s="T318">n-n:(poss)-n:case</ta>
            <ta e="T320" id="Seg_5461" s="T319">v-v:tense-v:pred.pn</ta>
            <ta e="T321" id="Seg_5462" s="T320">que</ta>
            <ta e="T322" id="Seg_5463" s="T321">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T323" id="Seg_5464" s="T322">n-n:(poss)-n:case</ta>
            <ta e="T324" id="Seg_5465" s="T323">v-v:mood-v:temp.pn</ta>
            <ta e="T325" id="Seg_5466" s="T324">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T326" id="Seg_5467" s="T325">v-v:cvb</ta>
            <ta e="T327" id="Seg_5468" s="T326">v-v:tense-v:poss.pn</ta>
            <ta e="T328" id="Seg_5469" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_5470" s="T328">ptcl</ta>
            <ta e="T330" id="Seg_5471" s="T329">ptcl</ta>
            <ta e="T331" id="Seg_5472" s="T330">dempro</ta>
            <ta e="T332" id="Seg_5473" s="T331">v-v:tense-v:pred.pn</ta>
            <ta e="T333" id="Seg_5474" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_5475" s="T333">n-n:case</ta>
            <ta e="T335" id="Seg_5476" s="T334">n-n:(poss)-n:case</ta>
            <ta e="T336" id="Seg_5477" s="T335">v-v:cvb</ta>
            <ta e="T337" id="Seg_5478" s="T336">v-v:tense-v:pred.pn</ta>
            <ta e="T338" id="Seg_5479" s="T337">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T339" id="Seg_5480" s="T338">post</ta>
            <ta e="T340" id="Seg_5481" s="T339">v-v:cvb</ta>
            <ta e="T341" id="Seg_5482" s="T340">post</ta>
            <ta e="T342" id="Seg_5483" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_5484" s="T342">v-v:tense-v:pred.pn</ta>
            <ta e="T344" id="Seg_5485" s="T343">ptcl</ta>
            <ta e="T345" id="Seg_5486" s="T344">dempro</ta>
            <ta e="T346" id="Seg_5487" s="T345">n-n:case</ta>
            <ta e="T347" id="Seg_5488" s="T346">post</ta>
            <ta e="T348" id="Seg_5489" s="T347">cardnum</ta>
            <ta e="T349" id="Seg_5490" s="T348">n-n:case</ta>
            <ta e="T350" id="Seg_5491" s="T349">v-v:cvb</ta>
            <ta e="T351" id="Seg_5492" s="T350">v-v:tense-v:pred.pn</ta>
            <ta e="T352" id="Seg_5493" s="T351">ptcl</ta>
            <ta e="T353" id="Seg_5494" s="T352">v-v:cvb</ta>
            <ta e="T354" id="Seg_5495" s="T353">post</ta>
            <ta e="T355" id="Seg_5496" s="T354">v-v:cvb</ta>
            <ta e="T356" id="Seg_5497" s="T355">v-v:tense-v:pred.pn</ta>
            <ta e="T357" id="Seg_5498" s="T356">ptcl</ta>
            <ta e="T358" id="Seg_5499" s="T357">v-v:cvb</ta>
            <ta e="T359" id="Seg_5500" s="T358">adv</ta>
            <ta e="T360" id="Seg_5501" s="T359">v-v:cvb-v:pred.pn</ta>
            <ta e="T361" id="Seg_5502" s="T360">n-n:case</ta>
            <ta e="T362" id="Seg_5503" s="T361">v-v:tense-v:pred.pn</ta>
            <ta e="T363" id="Seg_5504" s="T362">n-n:(num)-n:poss-n:case</ta>
            <ta e="T364" id="Seg_5505" s="T363">v-v:(ins)-v:mood.pn</ta>
            <ta e="T365" id="Seg_5506" s="T364">v-v:cvb-v-v:cvb</ta>
            <ta e="T366" id="Seg_5507" s="T365">v-v:cvb</ta>
            <ta e="T367" id="Seg_5508" s="T366">v-v:tense-v:pred.pn</ta>
            <ta e="T368" id="Seg_5509" s="T367">ptcl</ta>
            <ta e="T369" id="Seg_5510" s="T368">cardnum</ta>
            <ta e="T370" id="Seg_5511" s="T369">n-n&gt;adj</ta>
            <ta e="T371" id="Seg_5512" s="T370">n-n:case</ta>
            <ta e="T372" id="Seg_5513" s="T371">n-n:case</ta>
            <ta e="T373" id="Seg_5514" s="T372">n-n:poss-n:case</ta>
            <ta e="T374" id="Seg_5515" s="T373">v-v:cvb</ta>
            <ta e="T375" id="Seg_5516" s="T374">dempro-pro:case</ta>
            <ta e="T376" id="Seg_5517" s="T375">n-n:case</ta>
            <ta e="T377" id="Seg_5518" s="T376">n-n:poss-n:case</ta>
            <ta e="T378" id="Seg_5519" s="T377">v-v:tense-v:pred.pn</ta>
            <ta e="T379" id="Seg_5520" s="T378">n-n:poss-n:case</ta>
            <ta e="T380" id="Seg_5521" s="T379">v-v:tense-v:poss.pn</ta>
            <ta e="T381" id="Seg_5522" s="T380">n-n:case</ta>
            <ta e="T382" id="Seg_5523" s="T381">v-v:cvb</ta>
            <ta e="T383" id="Seg_5524" s="T382">v-v:tense-v:pred.pn</ta>
            <ta e="T384" id="Seg_5525" s="T383">adv</ta>
            <ta e="T385" id="Seg_5526" s="T384">n-n:(poss)-n:case</ta>
            <ta e="T386" id="Seg_5527" s="T385">v-v:(neg)-v:pred.pn</ta>
            <ta e="T387" id="Seg_5528" s="T386">v-v:ptcp-v:(case)</ta>
            <ta e="T388" id="Seg_5529" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_5530" s="T388">v-v:ptcp-v:(case)</ta>
            <ta e="T390" id="Seg_5531" s="T389">ptcl</ta>
            <ta e="T391" id="Seg_5532" s="T390">post</ta>
            <ta e="T392" id="Seg_5533" s="T391">dempro</ta>
            <ta e="T393" id="Seg_5534" s="T392">n-n:case</ta>
            <ta e="T394" id="Seg_5535" s="T393">n-n:poss-n:case</ta>
            <ta e="T395" id="Seg_5536" s="T394">n-n:poss-n:case</ta>
            <ta e="T396" id="Seg_5537" s="T395">n-n&gt;adv</ta>
            <ta e="T397" id="Seg_5538" s="T396">v-v:tense-v:pred.pn</ta>
            <ta e="T398" id="Seg_5539" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_5540" s="T398">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T400" id="Seg_5541" s="T399">v-v:cvb</ta>
            <ta e="T401" id="Seg_5542" s="T400">v-v:cvb</ta>
            <ta e="T402" id="Seg_5543" s="T401">ptcl</ta>
            <ta e="T403" id="Seg_5544" s="T402">dempro</ta>
            <ta e="T404" id="Seg_5545" s="T403">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T405" id="Seg_5546" s="T404">n-n:(poss)-n:case</ta>
            <ta e="T406" id="Seg_5547" s="T405">v-v:tense-v:pred.pn</ta>
            <ta e="T407" id="Seg_5548" s="T406">dempro-pro:case</ta>
            <ta e="T408" id="Seg_5549" s="T407">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T409" id="Seg_5550" s="T408">n-n:case</ta>
            <ta e="T410" id="Seg_5551" s="T409">v-v:cvb</ta>
            <ta e="T411" id="Seg_5552" s="T410">v-v:tense-v:pred.pn</ta>
            <ta e="T412" id="Seg_5553" s="T411">v-v:ptcp</ta>
            <ta e="T413" id="Seg_5554" s="T412">cardnum</ta>
            <ta e="T414" id="Seg_5555" s="T413">n-n:(poss)-n:case</ta>
            <ta e="T415" id="Seg_5556" s="T414">n-n:case</ta>
            <ta e="T416" id="Seg_5557" s="T415">v-v:cvb</ta>
            <ta e="T417" id="Seg_5558" s="T416">v-v:tense-v:pred.pn</ta>
            <ta e="T418" id="Seg_5559" s="T417">n-n:(poss)-n:case</ta>
            <ta e="T419" id="Seg_5560" s="T418">ptcl</ta>
            <ta e="T420" id="Seg_5561" s="T419">v-v:tense-v:poss.pn</ta>
            <ta e="T421" id="Seg_5562" s="T420">n-n:case</ta>
            <ta e="T422" id="Seg_5563" s="T421">v-v:tense-v:pred.pn</ta>
            <ta e="T423" id="Seg_5564" s="T422">dempro-pro:case</ta>
            <ta e="T424" id="Seg_5565" s="T423">ptcl</ta>
            <ta e="T425" id="Seg_5566" s="T424">v-v:tense-v:pred.pn</ta>
            <ta e="T426" id="Seg_5567" s="T425">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T427" id="Seg_5568" s="T426">v-v:cvb</ta>
            <ta e="T428" id="Seg_5569" s="T427">n-n:poss-n:case</ta>
            <ta e="T429" id="Seg_5570" s="T428">v-v:cvb</ta>
            <ta e="T430" id="Seg_5571" s="T429">n-n:case</ta>
            <ta e="T431" id="Seg_5572" s="T430">v-v:cvb</ta>
            <ta e="T432" id="Seg_5573" s="T431">n-n:case</ta>
            <ta e="T433" id="Seg_5574" s="T432">v-v:tense-v:pred.pn</ta>
            <ta e="T434" id="Seg_5575" s="T433">n-n:case</ta>
            <ta e="T435" id="Seg_5576" s="T434">v-v:cvb</ta>
            <ta e="T436" id="Seg_5577" s="T435">v-v:tense-v:pred.pn</ta>
            <ta e="T437" id="Seg_5578" s="T436">pers-pro:case</ta>
            <ta e="T438" id="Seg_5579" s="T437">pers-pro:case</ta>
            <ta e="T439" id="Seg_5580" s="T438">v-v:tense-v:poss.pn</ta>
            <ta e="T440" id="Seg_5581" s="T439">n-n:case</ta>
            <ta e="T441" id="Seg_5582" s="T440">n-n:poss-n:case</ta>
            <ta e="T442" id="Seg_5583" s="T441">v-v:cvb-v:pred.pn</ta>
            <ta e="T443" id="Seg_5584" s="T442">n-n:case</ta>
            <ta e="T444" id="Seg_5585" s="T443">v-v:(ins)-v:(neg)-v:mood-v:temp.pn</ta>
            <ta e="T445" id="Seg_5586" s="T444">ptcl</ta>
            <ta e="T446" id="Seg_5587" s="T445">adv</ta>
            <ta e="T447" id="Seg_5588" s="T446">cardnum</ta>
            <ta e="T448" id="Seg_5589" s="T447">n-n:case</ta>
            <ta e="T449" id="Seg_5590" s="T448">v-v:cvb-v:pred.pn</ta>
            <ta e="T450" id="Seg_5591" s="T449">dempro</ta>
            <ta e="T451" id="Seg_5592" s="T450">n-n:poss-n:case</ta>
            <ta e="T452" id="Seg_5593" s="T451">n-n:case</ta>
            <ta e="T453" id="Seg_5594" s="T452">v-v:tense-v:pred.pn</ta>
            <ta e="T454" id="Seg_5595" s="T453">dempro</ta>
            <ta e="T455" id="Seg_5596" s="T454">n-n:poss-n:case</ta>
            <ta e="T456" id="Seg_5597" s="T455">dempro-pro:case</ta>
            <ta e="T457" id="Seg_5598" s="T456">v-v:ptcp</ta>
            <ta e="T458" id="Seg_5599" s="T457">ptcl</ta>
            <ta e="T459" id="Seg_5600" s="T458">conj</ta>
            <ta e="T460" id="Seg_5601" s="T459">n-n:case</ta>
            <ta e="T461" id="Seg_5602" s="T460">cardnum-cardnum&gt;adv</ta>
            <ta e="T462" id="Seg_5603" s="T461">v-v:(neg)-v:pred.pn</ta>
            <ta e="T463" id="Seg_5604" s="T462">n-n:case</ta>
            <ta e="T464" id="Seg_5605" s="T463">n-n:case</ta>
            <ta e="T465" id="Seg_5606" s="T464">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_5607" s="T0">adv</ta>
            <ta e="T2" id="Seg_5608" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_5609" s="T2">adj</ta>
            <ta e="T4" id="Seg_5610" s="T3">n</ta>
            <ta e="T5" id="Seg_5611" s="T4">v</ta>
            <ta e="T6" id="Seg_5612" s="T5">adj</ta>
            <ta e="T7" id="Seg_5613" s="T6">dempro</ta>
            <ta e="T8" id="Seg_5614" s="T7">n</ta>
            <ta e="T9" id="Seg_5615" s="T8">v</ta>
            <ta e="T10" id="Seg_5616" s="T9">n</ta>
            <ta e="T11" id="Seg_5617" s="T10">cop</ta>
            <ta e="T12" id="Seg_5618" s="T11">post</ta>
            <ta e="T13" id="Seg_5619" s="T12">v</ta>
            <ta e="T14" id="Seg_5620" s="T13">aux</ta>
            <ta e="T15" id="Seg_5621" s="T14">dempro</ta>
            <ta e="T16" id="Seg_5622" s="T15">n</ta>
            <ta e="T17" id="Seg_5623" s="T16">adv</ta>
            <ta e="T18" id="Seg_5624" s="T17">v</ta>
            <ta e="T19" id="Seg_5625" s="T18">v</ta>
            <ta e="T20" id="Seg_5626" s="T19">v</ta>
            <ta e="T21" id="Seg_5627" s="T20">v</ta>
            <ta e="T22" id="Seg_5628" s="T21">pers</ta>
            <ta e="T23" id="Seg_5629" s="T22">n</ta>
            <ta e="T24" id="Seg_5630" s="T23">adv</ta>
            <ta e="T25" id="Seg_5631" s="T24">emphpro</ta>
            <ta e="T26" id="Seg_5632" s="T25">post</ta>
            <ta e="T27" id="Seg_5633" s="T26">v</ta>
            <ta e="T28" id="Seg_5634" s="T27">aux</ta>
            <ta e="T29" id="Seg_5635" s="T28">pers</ta>
            <ta e="T30" id="Seg_5636" s="T29">v</ta>
            <ta e="T31" id="Seg_5637" s="T30">adj</ta>
            <ta e="T32" id="Seg_5638" s="T31">n</ta>
            <ta e="T33" id="Seg_5639" s="T32">v</ta>
            <ta e="T34" id="Seg_5640" s="T33">v</ta>
            <ta e="T35" id="Seg_5641" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_5642" s="T35">n</ta>
            <ta e="T37" id="Seg_5643" s="T36">n</ta>
            <ta e="T38" id="Seg_5644" s="T37">v</ta>
            <ta e="T39" id="Seg_5645" s="T38">v</ta>
            <ta e="T40" id="Seg_5646" s="T39">dempro</ta>
            <ta e="T41" id="Seg_5647" s="T40">v</ta>
            <ta e="T42" id="Seg_5648" s="T41">n</ta>
            <ta e="T43" id="Seg_5649" s="T42">v</ta>
            <ta e="T44" id="Seg_5650" s="T43">cardnum</ta>
            <ta e="T45" id="Seg_5651" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_5652" s="T45">cop</ta>
            <ta e="T47" id="Seg_5653" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_5654" s="T47">cardnum</ta>
            <ta e="T49" id="Seg_5655" s="T48">adj</ta>
            <ta e="T50" id="Seg_5656" s="T49">n</ta>
            <ta e="T51" id="Seg_5657" s="T50">v</ta>
            <ta e="T52" id="Seg_5658" s="T51">cardnum</ta>
            <ta e="T53" id="Seg_5659" s="T52">n</ta>
            <ta e="T54" id="Seg_5660" s="T53">v</ta>
            <ta e="T55" id="Seg_5661" s="T54">post</ta>
            <ta e="T56" id="Seg_5662" s="T55">n</ta>
            <ta e="T57" id="Seg_5663" s="T56">n</ta>
            <ta e="T58" id="Seg_5664" s="T57">adv</ta>
            <ta e="T59" id="Seg_5665" s="T58">v</ta>
            <ta e="T60" id="Seg_5666" s="T59">n</ta>
            <ta e="T61" id="Seg_5667" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_5668" s="T61">v</ta>
            <ta e="T63" id="Seg_5669" s="T62">dempro</ta>
            <ta e="T64" id="Seg_5670" s="T63">cardnum</ta>
            <ta e="T65" id="Seg_5671" s="T64">n</ta>
            <ta e="T66" id="Seg_5672" s="T65">v</ta>
            <ta e="T67" id="Seg_5673" s="T66">que</ta>
            <ta e="T68" id="Seg_5674" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_5675" s="T68">v</ta>
            <ta e="T70" id="Seg_5676" s="T69">ordnum</ta>
            <ta e="T71" id="Seg_5677" s="T70">n</ta>
            <ta e="T72" id="Seg_5678" s="T71">cop</ta>
            <ta e="T73" id="Seg_5679" s="T72">dempro</ta>
            <ta e="T74" id="Seg_5680" s="T73">n</ta>
            <ta e="T75" id="Seg_5681" s="T74">n</ta>
            <ta e="T76" id="Seg_5682" s="T75">n</ta>
            <ta e="T77" id="Seg_5683" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_5684" s="T77">cop</ta>
            <ta e="T79" id="Seg_5685" s="T78">dempro</ta>
            <ta e="T80" id="Seg_5686" s="T79">n</ta>
            <ta e="T81" id="Seg_5687" s="T80">adv</ta>
            <ta e="T82" id="Seg_5688" s="T81">v</ta>
            <ta e="T83" id="Seg_5689" s="T82">n</ta>
            <ta e="T84" id="Seg_5690" s="T83">adv</ta>
            <ta e="T85" id="Seg_5691" s="T84">v</ta>
            <ta e="T86" id="Seg_5692" s="T85">dempro</ta>
            <ta e="T87" id="Seg_5693" s="T86">v</ta>
            <ta e="T88" id="Seg_5694" s="T87">post</ta>
            <ta e="T89" id="Seg_5695" s="T88">n</ta>
            <ta e="T90" id="Seg_5696" s="T89">v</ta>
            <ta e="T91" id="Seg_5697" s="T90">v</ta>
            <ta e="T92" id="Seg_5698" s="T91">dempro</ta>
            <ta e="T93" id="Seg_5699" s="T92">pers</ta>
            <ta e="T94" id="Seg_5700" s="T93">n</ta>
            <ta e="T95" id="Seg_5701" s="T94">v</ta>
            <ta e="T96" id="Seg_5702" s="T95">adv</ta>
            <ta e="T97" id="Seg_5703" s="T96">n</ta>
            <ta e="T98" id="Seg_5704" s="T97">cardnum</ta>
            <ta e="T99" id="Seg_5705" s="T98">cop</ta>
            <ta e="T100" id="Seg_5706" s="T99">n</ta>
            <ta e="T101" id="Seg_5707" s="T100">n</ta>
            <ta e="T102" id="Seg_5708" s="T101">v</ta>
            <ta e="T103" id="Seg_5709" s="T102">n</ta>
            <ta e="T104" id="Seg_5710" s="T103">n</ta>
            <ta e="T105" id="Seg_5711" s="T104">n</ta>
            <ta e="T106" id="Seg_5712" s="T105">n</ta>
            <ta e="T107" id="Seg_5713" s="T106">v</ta>
            <ta e="T108" id="Seg_5714" s="T107">aux</ta>
            <ta e="T109" id="Seg_5715" s="T108">aux</ta>
            <ta e="T110" id="Seg_5716" s="T109">adj</ta>
            <ta e="T111" id="Seg_5717" s="T110">n</ta>
            <ta e="T112" id="Seg_5718" s="T111">n</ta>
            <ta e="T113" id="Seg_5719" s="T112">cop</ta>
            <ta e="T114" id="Seg_5720" s="T113">n</ta>
            <ta e="T115" id="Seg_5721" s="T114">v</ta>
            <ta e="T116" id="Seg_5722" s="T115">n</ta>
            <ta e="T117" id="Seg_5723" s="T116">n</ta>
            <ta e="T118" id="Seg_5724" s="T117">n</ta>
            <ta e="T119" id="Seg_5725" s="T118">v</ta>
            <ta e="T120" id="Seg_5726" s="T119">aux</ta>
            <ta e="T121" id="Seg_5727" s="T120">adj</ta>
            <ta e="T122" id="Seg_5728" s="T121">n</ta>
            <ta e="T123" id="Seg_5729" s="T122">cop</ta>
            <ta e="T124" id="Seg_5730" s="T123">n</ta>
            <ta e="T125" id="Seg_5731" s="T124">v</ta>
            <ta e="T126" id="Seg_5732" s="T125">v</ta>
            <ta e="T127" id="Seg_5733" s="T126">n</ta>
            <ta e="T128" id="Seg_5734" s="T127">v</ta>
            <ta e="T129" id="Seg_5735" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_5736" s="T129">v</ta>
            <ta e="T131" id="Seg_5737" s="T130">n</ta>
            <ta e="T132" id="Seg_5738" s="T131">v</ta>
            <ta e="T133" id="Seg_5739" s="T132">aux</ta>
            <ta e="T134" id="Seg_5740" s="T133">cardnum</ta>
            <ta e="T135" id="Seg_5741" s="T134">n</ta>
            <ta e="T136" id="Seg_5742" s="T135">adv</ta>
            <ta e="T137" id="Seg_5743" s="T136">v</ta>
            <ta e="T138" id="Seg_5744" s="T137">adv</ta>
            <ta e="T139" id="Seg_5745" s="T138">que</ta>
            <ta e="T140" id="Seg_5746" s="T139">cop</ta>
            <ta e="T141" id="Seg_5747" s="T140">dempro</ta>
            <ta e="T142" id="Seg_5748" s="T141">adj</ta>
            <ta e="T143" id="Seg_5749" s="T142">n</ta>
            <ta e="T144" id="Seg_5750" s="T143">v</ta>
            <ta e="T145" id="Seg_5751" s="T144">dempro</ta>
            <ta e="T146" id="Seg_5752" s="T145">n</ta>
            <ta e="T147" id="Seg_5753" s="T146">v</ta>
            <ta e="T148" id="Seg_5754" s="T147">v</ta>
            <ta e="T149" id="Seg_5755" s="T148">n</ta>
            <ta e="T150" id="Seg_5756" s="T149">emphpro</ta>
            <ta e="T151" id="Seg_5757" s="T150">que</ta>
            <ta e="T152" id="Seg_5758" s="T151">v</ta>
            <ta e="T153" id="Seg_5759" s="T152">v</ta>
            <ta e="T154" id="Seg_5760" s="T153">v</ta>
            <ta e="T155" id="Seg_5761" s="T154">v</ta>
            <ta e="T156" id="Seg_5762" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_5763" s="T156">adv</ta>
            <ta e="T158" id="Seg_5764" s="T157">v</ta>
            <ta e="T159" id="Seg_5765" s="T158">v</ta>
            <ta e="T160" id="Seg_5766" s="T159">que</ta>
            <ta e="T161" id="Seg_5767" s="T160">v</ta>
            <ta e="T162" id="Seg_5768" s="T161">v</ta>
            <ta e="T163" id="Seg_5769" s="T162">pers</ta>
            <ta e="T164" id="Seg_5770" s="T163">n</ta>
            <ta e="T165" id="Seg_5771" s="T164">v</ta>
            <ta e="T166" id="Seg_5772" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_5773" s="T166">n</ta>
            <ta e="T168" id="Seg_5774" s="T167">n</ta>
            <ta e="T169" id="Seg_5775" s="T168">v</ta>
            <ta e="T170" id="Seg_5776" s="T169">emphpro</ta>
            <ta e="T171" id="Seg_5777" s="T170">v</ta>
            <ta e="T172" id="Seg_5778" s="T171">v</ta>
            <ta e="T173" id="Seg_5779" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_5780" s="T173">v</ta>
            <ta e="T175" id="Seg_5781" s="T174">n</ta>
            <ta e="T176" id="Seg_5782" s="T175">v</ta>
            <ta e="T177" id="Seg_5783" s="T176">aux</ta>
            <ta e="T178" id="Seg_5784" s="T177">dempro</ta>
            <ta e="T179" id="Seg_5785" s="T178">v</ta>
            <ta e="T180" id="Seg_5786" s="T179">aux</ta>
            <ta e="T181" id="Seg_5787" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_5788" s="T181">dempro</ta>
            <ta e="T183" id="Seg_5789" s="T182">n</ta>
            <ta e="T184" id="Seg_5790" s="T183">n</ta>
            <ta e="T185" id="Seg_5791" s="T184">v</ta>
            <ta e="T186" id="Seg_5792" s="T185">ptcl</ta>
            <ta e="T187" id="Seg_5793" s="T186">dempro</ta>
            <ta e="T188" id="Seg_5794" s="T187">n</ta>
            <ta e="T189" id="Seg_5795" s="T188">n</ta>
            <ta e="T190" id="Seg_5796" s="T189">v</ta>
            <ta e="T191" id="Seg_5797" s="T190">v</ta>
            <ta e="T192" id="Seg_5798" s="T191">quant</ta>
            <ta e="T193" id="Seg_5799" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_5800" s="T193">n</ta>
            <ta e="T195" id="Seg_5801" s="T194">v</ta>
            <ta e="T196" id="Seg_5802" s="T195">aux</ta>
            <ta e="T197" id="Seg_5803" s="T196">adv</ta>
            <ta e="T198" id="Seg_5804" s="T197">v</ta>
            <ta e="T199" id="Seg_5805" s="T198">v</ta>
            <ta e="T200" id="Seg_5806" s="T199">n</ta>
            <ta e="T201" id="Seg_5807" s="T200">n</ta>
            <ta e="T202" id="Seg_5808" s="T201">que</ta>
            <ta e="T203" id="Seg_5809" s="T202">v</ta>
            <ta e="T204" id="Seg_5810" s="T203">adv</ta>
            <ta e="T205" id="Seg_5811" s="T204">v</ta>
            <ta e="T206" id="Seg_5812" s="T205">adv</ta>
            <ta e="T207" id="Seg_5813" s="T206">dempro</ta>
            <ta e="T208" id="Seg_5814" s="T207">n</ta>
            <ta e="T209" id="Seg_5815" s="T208">adj</ta>
            <ta e="T210" id="Seg_5816" s="T209">n</ta>
            <ta e="T211" id="Seg_5817" s="T210">v</ta>
            <ta e="T212" id="Seg_5818" s="T211">v</ta>
            <ta e="T213" id="Seg_5819" s="T212">v</ta>
            <ta e="T214" id="Seg_5820" s="T213">cardnum</ta>
            <ta e="T215" id="Seg_5821" s="T214">n</ta>
            <ta e="T216" id="Seg_5822" s="T215">adv</ta>
            <ta e="T217" id="Seg_5823" s="T216">cardnum</ta>
            <ta e="T218" id="Seg_5824" s="T217">adj</ta>
            <ta e="T219" id="Seg_5825" s="T218">adj</ta>
            <ta e="T220" id="Seg_5826" s="T219">adj</ta>
            <ta e="T221" id="Seg_5827" s="T220">adj</ta>
            <ta e="T222" id="Seg_5828" s="T221">n</ta>
            <ta e="T223" id="Seg_5829" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_5830" s="T223">cop</ta>
            <ta e="T225" id="Seg_5831" s="T224">dempro</ta>
            <ta e="T226" id="Seg_5832" s="T225">adj</ta>
            <ta e="T227" id="Seg_5833" s="T226">n</ta>
            <ta e="T228" id="Seg_5834" s="T227">n</ta>
            <ta e="T229" id="Seg_5835" s="T228">que</ta>
            <ta e="T230" id="Seg_5836" s="T229">dempro</ta>
            <ta e="T231" id="Seg_5837" s="T230">n</ta>
            <ta e="T232" id="Seg_5838" s="T231">v</ta>
            <ta e="T233" id="Seg_5839" s="T232">v</ta>
            <ta e="T234" id="Seg_5840" s="T233">n</ta>
            <ta e="T235" id="Seg_5841" s="T234">n</ta>
            <ta e="T236" id="Seg_5842" s="T235">v</ta>
            <ta e="T237" id="Seg_5843" s="T236">v</ta>
            <ta e="T238" id="Seg_5844" s="T237">n</ta>
            <ta e="T239" id="Seg_5845" s="T238">adv</ta>
            <ta e="T240" id="Seg_5846" s="T239">cardnum</ta>
            <ta e="T241" id="Seg_5847" s="T240">v</ta>
            <ta e="T242" id="Seg_5848" s="T241">v</ta>
            <ta e="T243" id="Seg_5849" s="T242">dempro</ta>
            <ta e="T244" id="Seg_5850" s="T243">n</ta>
            <ta e="T245" id="Seg_5851" s="T244">cardnum</ta>
            <ta e="T246" id="Seg_5852" s="T245">adj</ta>
            <ta e="T247" id="Seg_5853" s="T246">n</ta>
            <ta e="T248" id="Seg_5854" s="T247">adj</ta>
            <ta e="T249" id="Seg_5855" s="T248">adv</ta>
            <ta e="T250" id="Seg_5856" s="T249">adv</ta>
            <ta e="T251" id="Seg_5857" s="T250">n</ta>
            <ta e="T252" id="Seg_5858" s="T251">v</ta>
            <ta e="T253" id="Seg_5859" s="T252">v</ta>
            <ta e="T254" id="Seg_5860" s="T253">aux</ta>
            <ta e="T255" id="Seg_5861" s="T254">n</ta>
            <ta e="T256" id="Seg_5862" s="T255">post</ta>
            <ta e="T257" id="Seg_5863" s="T256">v</ta>
            <ta e="T258" id="Seg_5864" s="T257">v</ta>
            <ta e="T259" id="Seg_5865" s="T258">adj</ta>
            <ta e="T260" id="Seg_5866" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_5867" s="T260">v</ta>
            <ta e="T262" id="Seg_5868" s="T261">que</ta>
            <ta e="T263" id="Seg_5869" s="T262">cop</ta>
            <ta e="T264" id="Seg_5870" s="T263">v</ta>
            <ta e="T265" id="Seg_5871" s="T264">v</ta>
            <ta e="T266" id="Seg_5872" s="T265">adj</ta>
            <ta e="T267" id="Seg_5873" s="T266">n</ta>
            <ta e="T268" id="Seg_5874" s="T267">v</ta>
            <ta e="T269" id="Seg_5875" s="T268">v</ta>
            <ta e="T270" id="Seg_5876" s="T269">v</ta>
            <ta e="T271" id="Seg_5877" s="T270">v</ta>
            <ta e="T272" id="Seg_5878" s="T271">adv</ta>
            <ta e="T273" id="Seg_5879" s="T272">cardnum</ta>
            <ta e="T274" id="Seg_5880" s="T273">n</ta>
            <ta e="T275" id="Seg_5881" s="T274">collnum</ta>
            <ta e="T276" id="Seg_5882" s="T275">v</ta>
            <ta e="T277" id="Seg_5883" s="T276">conj</ta>
            <ta e="T278" id="Seg_5884" s="T277">que</ta>
            <ta e="T279" id="Seg_5885" s="T278">post</ta>
            <ta e="T280" id="Seg_5886" s="T279">v</ta>
            <ta e="T281" id="Seg_5887" s="T280">aux</ta>
            <ta e="T282" id="Seg_5888" s="T281">ptcl</ta>
            <ta e="T283" id="Seg_5889" s="T282">adv</ta>
            <ta e="T284" id="Seg_5890" s="T283">collnum</ta>
            <ta e="T285" id="Seg_5891" s="T284">cardnum</ta>
            <ta e="T286" id="Seg_5892" s="T285">n</ta>
            <ta e="T287" id="Seg_5893" s="T286">v</ta>
            <ta e="T288" id="Seg_5894" s="T287">aux</ta>
            <ta e="T289" id="Seg_5895" s="T288">adv</ta>
            <ta e="T290" id="Seg_5896" s="T289">v</ta>
            <ta e="T291" id="Seg_5897" s="T290">n</ta>
            <ta e="T292" id="Seg_5898" s="T291">v</ta>
            <ta e="T293" id="Seg_5899" s="T292">aux</ta>
            <ta e="T294" id="Seg_5900" s="T293">dempro</ta>
            <ta e="T295" id="Seg_5901" s="T294">v</ta>
            <ta e="T296" id="Seg_5902" s="T295">cardnum</ta>
            <ta e="T297" id="Seg_5903" s="T296">n</ta>
            <ta e="T298" id="Seg_5904" s="T297">ptcl</ta>
            <ta e="T299" id="Seg_5905" s="T298">v</ta>
            <ta e="T300" id="Seg_5906" s="T299">dempro</ta>
            <ta e="T301" id="Seg_5907" s="T300">cardnum</ta>
            <ta e="T302" id="Seg_5908" s="T301">n</ta>
            <ta e="T303" id="Seg_5909" s="T302">v</ta>
            <ta e="T304" id="Seg_5910" s="T303">que</ta>
            <ta e="T305" id="Seg_5911" s="T304">cop</ta>
            <ta e="T306" id="Seg_5912" s="T305">v</ta>
            <ta e="T307" id="Seg_5913" s="T306">v</ta>
            <ta e="T308" id="Seg_5914" s="T307">n</ta>
            <ta e="T309" id="Seg_5915" s="T308">v</ta>
            <ta e="T310" id="Seg_5916" s="T309">pers</ta>
            <ta e="T311" id="Seg_5917" s="T310">n</ta>
            <ta e="T312" id="Seg_5918" s="T311">cardnum</ta>
            <ta e="T313" id="Seg_5919" s="T312">adv</ta>
            <ta e="T314" id="Seg_5920" s="T313">adj</ta>
            <ta e="T315" id="Seg_5921" s="T314">n</ta>
            <ta e="T316" id="Seg_5922" s="T315">v</ta>
            <ta e="T317" id="Seg_5923" s="T316">v</ta>
            <ta e="T318" id="Seg_5924" s="T317">dempro</ta>
            <ta e="T319" id="Seg_5925" s="T318">n</ta>
            <ta e="T320" id="Seg_5926" s="T319">v</ta>
            <ta e="T321" id="Seg_5927" s="T320">que</ta>
            <ta e="T322" id="Seg_5928" s="T321">cop</ta>
            <ta e="T323" id="Seg_5929" s="T322">n</ta>
            <ta e="T324" id="Seg_5930" s="T323">v</ta>
            <ta e="T325" id="Seg_5931" s="T324">emphpro</ta>
            <ta e="T326" id="Seg_5932" s="T325">v</ta>
            <ta e="T327" id="Seg_5933" s="T326">v</ta>
            <ta e="T328" id="Seg_5934" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_5935" s="T328">ptcl</ta>
            <ta e="T330" id="Seg_5936" s="T329">ptcl</ta>
            <ta e="T331" id="Seg_5937" s="T330">dempro</ta>
            <ta e="T332" id="Seg_5938" s="T331">cop</ta>
            <ta e="T333" id="Seg_5939" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_5940" s="T333">n</ta>
            <ta e="T335" id="Seg_5941" s="T334">n</ta>
            <ta e="T336" id="Seg_5942" s="T335">v</ta>
            <ta e="T337" id="Seg_5943" s="T336">v</ta>
            <ta e="T338" id="Seg_5944" s="T337">v</ta>
            <ta e="T339" id="Seg_5945" s="T338">post</ta>
            <ta e="T340" id="Seg_5946" s="T339">v</ta>
            <ta e="T341" id="Seg_5947" s="T340">post</ta>
            <ta e="T342" id="Seg_5948" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_5949" s="T342">v</ta>
            <ta e="T344" id="Seg_5950" s="T343">ptcl</ta>
            <ta e="T345" id="Seg_5951" s="T344">dempro</ta>
            <ta e="T346" id="Seg_5952" s="T345">n</ta>
            <ta e="T347" id="Seg_5953" s="T346">post</ta>
            <ta e="T348" id="Seg_5954" s="T347">cardnum</ta>
            <ta e="T349" id="Seg_5955" s="T348">n</ta>
            <ta e="T350" id="Seg_5956" s="T349">v</ta>
            <ta e="T351" id="Seg_5957" s="T350">aux</ta>
            <ta e="T352" id="Seg_5958" s="T351">ptcl</ta>
            <ta e="T353" id="Seg_5959" s="T352">v</ta>
            <ta e="T354" id="Seg_5960" s="T353">post</ta>
            <ta e="T355" id="Seg_5961" s="T354">v</ta>
            <ta e="T356" id="Seg_5962" s="T355">aux</ta>
            <ta e="T357" id="Seg_5963" s="T356">ptcl</ta>
            <ta e="T358" id="Seg_5964" s="T357">v</ta>
            <ta e="T359" id="Seg_5965" s="T358">adv</ta>
            <ta e="T360" id="Seg_5966" s="T359">v</ta>
            <ta e="T361" id="Seg_5967" s="T360">n</ta>
            <ta e="T362" id="Seg_5968" s="T361">v</ta>
            <ta e="T363" id="Seg_5969" s="T362">n</ta>
            <ta e="T364" id="Seg_5970" s="T363">v</ta>
            <ta e="T365" id="Seg_5971" s="T364">v</ta>
            <ta e="T366" id="Seg_5972" s="T365">v</ta>
            <ta e="T367" id="Seg_5973" s="T366">v</ta>
            <ta e="T368" id="Seg_5974" s="T367">ptcl</ta>
            <ta e="T369" id="Seg_5975" s="T368">cardnum</ta>
            <ta e="T370" id="Seg_5976" s="T369">adj</ta>
            <ta e="T371" id="Seg_5977" s="T370">n</ta>
            <ta e="T372" id="Seg_5978" s="T371">n</ta>
            <ta e="T373" id="Seg_5979" s="T372">n</ta>
            <ta e="T374" id="Seg_5980" s="T373">v</ta>
            <ta e="T375" id="Seg_5981" s="T374">dempro</ta>
            <ta e="T376" id="Seg_5982" s="T375">n</ta>
            <ta e="T377" id="Seg_5983" s="T376">n</ta>
            <ta e="T378" id="Seg_5984" s="T377">v</ta>
            <ta e="T379" id="Seg_5985" s="T378">n</ta>
            <ta e="T380" id="Seg_5986" s="T379">v</ta>
            <ta e="T381" id="Seg_5987" s="T380">n</ta>
            <ta e="T382" id="Seg_5988" s="T381">v</ta>
            <ta e="T383" id="Seg_5989" s="T382">aux</ta>
            <ta e="T384" id="Seg_5990" s="T383">adv</ta>
            <ta e="T385" id="Seg_5991" s="T384">n</ta>
            <ta e="T386" id="Seg_5992" s="T385">v</ta>
            <ta e="T387" id="Seg_5993" s="T386">v</ta>
            <ta e="T388" id="Seg_5994" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_5995" s="T388">v</ta>
            <ta e="T390" id="Seg_5996" s="T389">ptcl</ta>
            <ta e="T391" id="Seg_5997" s="T390">post</ta>
            <ta e="T392" id="Seg_5998" s="T391">dempro</ta>
            <ta e="T393" id="Seg_5999" s="T392">n</ta>
            <ta e="T394" id="Seg_6000" s="T393">n</ta>
            <ta e="T395" id="Seg_6001" s="T394">n</ta>
            <ta e="T396" id="Seg_6002" s="T395">adv</ta>
            <ta e="T397" id="Seg_6003" s="T396">v</ta>
            <ta e="T398" id="Seg_6004" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_6005" s="T398">dempro</ta>
            <ta e="T400" id="Seg_6006" s="T399">v</ta>
            <ta e="T401" id="Seg_6007" s="T400">aux</ta>
            <ta e="T402" id="Seg_6008" s="T401">ptcl</ta>
            <ta e="T403" id="Seg_6009" s="T402">dempro</ta>
            <ta e="T404" id="Seg_6010" s="T403">n</ta>
            <ta e="T405" id="Seg_6011" s="T404">n</ta>
            <ta e="T406" id="Seg_6012" s="T405">cop</ta>
            <ta e="T407" id="Seg_6013" s="T406">dempro</ta>
            <ta e="T408" id="Seg_6014" s="T407">n</ta>
            <ta e="T409" id="Seg_6015" s="T408">n</ta>
            <ta e="T410" id="Seg_6016" s="T409">v</ta>
            <ta e="T411" id="Seg_6017" s="T410">v</ta>
            <ta e="T412" id="Seg_6018" s="T411">v</ta>
            <ta e="T413" id="Seg_6019" s="T412">cardnum</ta>
            <ta e="T414" id="Seg_6020" s="T413">n</ta>
            <ta e="T415" id="Seg_6021" s="T414">n</ta>
            <ta e="T416" id="Seg_6022" s="T415">v</ta>
            <ta e="T417" id="Seg_6023" s="T416">v</ta>
            <ta e="T418" id="Seg_6024" s="T417">n</ta>
            <ta e="T419" id="Seg_6025" s="T418">ptcl</ta>
            <ta e="T420" id="Seg_6026" s="T419">v</ta>
            <ta e="T421" id="Seg_6027" s="T420">n</ta>
            <ta e="T422" id="Seg_6028" s="T421">v</ta>
            <ta e="T423" id="Seg_6029" s="T422">dempro</ta>
            <ta e="T424" id="Seg_6030" s="T423">ptcl</ta>
            <ta e="T425" id="Seg_6031" s="T424">v</ta>
            <ta e="T426" id="Seg_6032" s="T425">n</ta>
            <ta e="T427" id="Seg_6033" s="T426">v</ta>
            <ta e="T428" id="Seg_6034" s="T427">n</ta>
            <ta e="T429" id="Seg_6035" s="T428">v</ta>
            <ta e="T430" id="Seg_6036" s="T429">n</ta>
            <ta e="T431" id="Seg_6037" s="T430">cop</ta>
            <ta e="T432" id="Seg_6038" s="T431">n</ta>
            <ta e="T433" id="Seg_6039" s="T432">v</ta>
            <ta e="T434" id="Seg_6040" s="T433">n</ta>
            <ta e="T435" id="Seg_6041" s="T434">v</ta>
            <ta e="T436" id="Seg_6042" s="T435">v</ta>
            <ta e="T437" id="Seg_6043" s="T436">pers</ta>
            <ta e="T438" id="Seg_6044" s="T437">pers</ta>
            <ta e="T439" id="Seg_6045" s="T438">v</ta>
            <ta e="T440" id="Seg_6046" s="T439">n</ta>
            <ta e="T441" id="Seg_6047" s="T440">n</ta>
            <ta e="T442" id="Seg_6048" s="T441">v</ta>
            <ta e="T443" id="Seg_6049" s="T442">n</ta>
            <ta e="T444" id="Seg_6050" s="T443">v</ta>
            <ta e="T445" id="Seg_6051" s="T444">ptcl</ta>
            <ta e="T446" id="Seg_6052" s="T445">adv</ta>
            <ta e="T447" id="Seg_6053" s="T446">cardnum</ta>
            <ta e="T448" id="Seg_6054" s="T447">n</ta>
            <ta e="T449" id="Seg_6055" s="T448">v</ta>
            <ta e="T450" id="Seg_6056" s="T449">dempro</ta>
            <ta e="T451" id="Seg_6057" s="T450">n</ta>
            <ta e="T452" id="Seg_6058" s="T451">n</ta>
            <ta e="T453" id="Seg_6059" s="T452">v</ta>
            <ta e="T454" id="Seg_6060" s="T453">dempro</ta>
            <ta e="T455" id="Seg_6061" s="T454">n</ta>
            <ta e="T456" id="Seg_6062" s="T455">dempro</ta>
            <ta e="T457" id="Seg_6063" s="T456">v</ta>
            <ta e="T458" id="Seg_6064" s="T457">ptcl</ta>
            <ta e="T459" id="Seg_6065" s="T458">conj</ta>
            <ta e="T460" id="Seg_6066" s="T459">n</ta>
            <ta e="T461" id="Seg_6067" s="T460">adv</ta>
            <ta e="T462" id="Seg_6068" s="T461">v</ta>
            <ta e="T463" id="Seg_6069" s="T462">n</ta>
            <ta e="T464" id="Seg_6070" s="T463">n</ta>
            <ta e="T465" id="Seg_6071" s="T464">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_6072" s="T0">adv:Time</ta>
            <ta e="T4" id="Seg_6073" s="T3">np.h:Th</ta>
            <ta e="T6" id="Seg_6074" s="T5">0.3.h:Poss np.h:Th</ta>
            <ta e="T8" id="Seg_6075" s="T7">np.h:P</ta>
            <ta e="T9" id="Seg_6076" s="T8">0.3.h:Th</ta>
            <ta e="T11" id="Seg_6077" s="T10">0.3.h:Th</ta>
            <ta e="T16" id="Seg_6078" s="T15">np.h:A</ta>
            <ta e="T19" id="Seg_6079" s="T18">0.3.h:A</ta>
            <ta e="T20" id="Seg_6080" s="T19">0.3.h:E</ta>
            <ta e="T21" id="Seg_6081" s="T20">0.3.h:A</ta>
            <ta e="T22" id="Seg_6082" s="T21">pro.h:A</ta>
            <ta e="T23" id="Seg_6083" s="T22">0.1.h:Poss np.h:Th</ta>
            <ta e="T29" id="Seg_6084" s="T28">pro.h:Th</ta>
            <ta e="T32" id="Seg_6085" s="T31">np.h:A</ta>
            <ta e="T33" id="Seg_6086" s="T32">0.1.h:A</ta>
            <ta e="T34" id="Seg_6087" s="T33">0.3.h:A</ta>
            <ta e="T36" id="Seg_6088" s="T35">0.3.h:Poss np.h:Th</ta>
            <ta e="T37" id="Seg_6089" s="T36">np:G</ta>
            <ta e="T39" id="Seg_6090" s="T38">0.3.h:A</ta>
            <ta e="T41" id="Seg_6091" s="T40">0.3.h:A</ta>
            <ta e="T42" id="Seg_6092" s="T41">np.h:R</ta>
            <ta e="T43" id="Seg_6093" s="T42">0.3.h:A</ta>
            <ta e="T50" id="Seg_6094" s="T49">np.h:Th</ta>
            <ta e="T51" id="Seg_6095" s="T50">0.3.h:A</ta>
            <ta e="T53" id="Seg_6096" s="T52">np:Th</ta>
            <ta e="T56" id="Seg_6097" s="T55">np.h:Poss</ta>
            <ta e="T57" id="Seg_6098" s="T56">np:P</ta>
            <ta e="T60" id="Seg_6099" s="T59">0.3.h:Poss np:Th</ta>
            <ta e="T65" id="Seg_6100" s="T64">np.h:A</ta>
            <ta e="T67" id="Seg_6101" s="T66">pro:Th</ta>
            <ta e="T69" id="Seg_6102" s="T68">0.3.h:A</ta>
            <ta e="T71" id="Seg_6103" s="T70">np:Th</ta>
            <ta e="T74" id="Seg_6104" s="T73">np.h:Poss</ta>
            <ta e="T75" id="Seg_6105" s="T74">np:L</ta>
            <ta e="T76" id="Seg_6106" s="T75">np.h:Th</ta>
            <ta e="T80" id="Seg_6107" s="T79">np.h:A</ta>
            <ta e="T83" id="Seg_6108" s="T82">np.h:Th</ta>
            <ta e="T85" id="Seg_6109" s="T84">0.3.h:E</ta>
            <ta e="T87" id="Seg_6110" s="T86">0.3.h:E</ta>
            <ta e="T89" id="Seg_6111" s="T88">np.h:R</ta>
            <ta e="T90" id="Seg_6112" s="T89">0.3.h:A</ta>
            <ta e="T91" id="Seg_6113" s="T90">0.3.h:A</ta>
            <ta e="T93" id="Seg_6114" s="T92">pro.h:Poss</ta>
            <ta e="T94" id="Seg_6115" s="T93">np.h:Th</ta>
            <ta e="T97" id="Seg_6116" s="T96">np:Th</ta>
            <ta e="T100" id="Seg_6117" s="T99">0.2.h:Poss np.h:Poss</ta>
            <ta e="T101" id="Seg_6118" s="T100">np:P</ta>
            <ta e="T103" id="Seg_6119" s="T102">np.h:A</ta>
            <ta e="T104" id="Seg_6120" s="T103">np:G</ta>
            <ta e="T105" id="Seg_6121" s="T104">np:Poss</ta>
            <ta e="T106" id="Seg_6122" s="T105">np:G</ta>
            <ta e="T111" id="Seg_6123" s="T110">0.2.h:Poss np.h:Th</ta>
            <ta e="T113" id="Seg_6124" s="T112">0.3.h:Th</ta>
            <ta e="T114" id="Seg_6125" s="T113">np:Path</ta>
            <ta e="T115" id="Seg_6126" s="T114">0.3.h:A</ta>
            <ta e="T116" id="Seg_6127" s="T115">0.3.h:Poss np:Th</ta>
            <ta e="T121" id="Seg_6128" s="T120">np.h:Th</ta>
            <ta e="T123" id="Seg_6129" s="T122">0.3.h:Th</ta>
            <ta e="T124" id="Seg_6130" s="T123">np:Path</ta>
            <ta e="T127" id="Seg_6131" s="T126">np:Cau</ta>
            <ta e="T128" id="Seg_6132" s="T127">0.3.h:A</ta>
            <ta e="T130" id="Seg_6133" s="T129">0.3.h:E</ta>
            <ta e="T131" id="Seg_6134" s="T130">np:Th</ta>
            <ta e="T135" id="Seg_6135" s="T134">n:Time</ta>
            <ta e="T137" id="Seg_6136" s="T136">0.3.h:A</ta>
            <ta e="T143" id="Seg_6137" s="T142">np.h:A</ta>
            <ta e="T146" id="Seg_6138" s="T145">np.h:E</ta>
            <ta e="T149" id="Seg_6139" s="T148">np.h:E</ta>
            <ta e="T150" id="Seg_6140" s="T149">pro.h:A</ta>
            <ta e="T154" id="Seg_6141" s="T153">0.3.h:A</ta>
            <ta e="T155" id="Seg_6142" s="T154">0.3.h:A</ta>
            <ta e="T158" id="Seg_6143" s="T157">0.2.h:Th</ta>
            <ta e="T159" id="Seg_6144" s="T158">0.3.h:A</ta>
            <ta e="T161" id="Seg_6145" s="T160">0.1.h:A</ta>
            <ta e="T162" id="Seg_6146" s="T161">0.3.h:Th</ta>
            <ta e="T163" id="Seg_6147" s="T162">np.h:R</ta>
            <ta e="T165" id="Seg_6148" s="T164">0.2.h:A</ta>
            <ta e="T167" id="Seg_6149" s="T166">0.1.h:Poss np:Poss</ta>
            <ta e="T168" id="Seg_6150" s="T167">np:Th</ta>
            <ta e="T169" id="Seg_6151" s="T168">0.1.h:A</ta>
            <ta e="T170" id="Seg_6152" s="T169">pro.h:Th</ta>
            <ta e="T171" id="Seg_6153" s="T170">0.1.h:A</ta>
            <ta e="T172" id="Seg_6154" s="T171">0.2.h:A</ta>
            <ta e="T174" id="Seg_6155" s="T173">0.3.h:A</ta>
            <ta e="T175" id="Seg_6156" s="T174">np.h:Th</ta>
            <ta e="T180" id="Seg_6157" s="T179">0.3.h:Th</ta>
            <ta e="T184" id="Seg_6158" s="T183">np:G</ta>
            <ta e="T185" id="Seg_6159" s="T184">0.3.h:Th</ta>
            <ta e="T189" id="Seg_6160" s="T188">np:G</ta>
            <ta e="T190" id="Seg_6161" s="T189">0.3.h:Th</ta>
            <ta e="T191" id="Seg_6162" s="T190">0.3.h:Th</ta>
            <ta e="T194" id="Seg_6163" s="T193">np.h:A</ta>
            <ta e="T199" id="Seg_6164" s="T198">0.3.h:A</ta>
            <ta e="T200" id="Seg_6165" s="T199">np.h:Poss</ta>
            <ta e="T201" id="Seg_6166" s="T200">np:Th</ta>
            <ta e="T205" id="Seg_6167" s="T204">0.3.h:E</ta>
            <ta e="T208" id="Seg_6168" s="T207">np:So</ta>
            <ta e="T210" id="Seg_6169" s="T209">np:G</ta>
            <ta e="T211" id="Seg_6170" s="T210">0.1.h:A</ta>
            <ta e="T212" id="Seg_6171" s="T211">0.3.h:A</ta>
            <ta e="T213" id="Seg_6172" s="T212">0.3.h:Th</ta>
            <ta e="T215" id="Seg_6173" s="T214">np:G</ta>
            <ta e="T216" id="Seg_6174" s="T215">adv:L</ta>
            <ta e="T222" id="Seg_6175" s="T221">np.h:Th</ta>
            <ta e="T225" id="Seg_6176" s="T224">pro:So</ta>
            <ta e="T226" id="Seg_6177" s="T225">np.h:Th</ta>
            <ta e="T227" id="Seg_6178" s="T226">np.h:Poss</ta>
            <ta e="T233" id="Seg_6179" s="T232">0.3.h:E</ta>
            <ta e="T234" id="Seg_6180" s="T233">0.3.h:Poss np:Poss</ta>
            <ta e="T235" id="Seg_6181" s="T234">np:Th</ta>
            <ta e="T237" id="Seg_6182" s="T236">0.3.h:A</ta>
            <ta e="T238" id="Seg_6183" s="T237">np:Th</ta>
            <ta e="T242" id="Seg_6184" s="T241">0.3.h:A</ta>
            <ta e="T244" id="Seg_6185" s="T243">np.h:Poss</ta>
            <ta e="T246" id="Seg_6186" s="T245">np:Th</ta>
            <ta e="T249" id="Seg_6187" s="T248">adv:G</ta>
            <ta e="T250" id="Seg_6188" s="T249">adv:Time</ta>
            <ta e="T251" id="Seg_6189" s="T250">np:So</ta>
            <ta e="T252" id="Seg_6190" s="T251">0.3.h:A</ta>
            <ta e="T254" id="Seg_6191" s="T253">0.3.h:Th</ta>
            <ta e="T256" id="Seg_6192" s="T254">pp:Com</ta>
            <ta e="T257" id="Seg_6193" s="T256">0.3.h:A</ta>
            <ta e="T258" id="Seg_6194" s="T257">0.3.h:E</ta>
            <ta e="T259" id="Seg_6195" s="T258">pro.h:A</ta>
            <ta e="T263" id="Seg_6196" s="T262">0.1.h:Th</ta>
            <ta e="T265" id="Seg_6197" s="T264">0.3.h:E</ta>
            <ta e="T266" id="Seg_6198" s="T265">pro.h:Poss</ta>
            <ta e="T267" id="Seg_6199" s="T266">np:G</ta>
            <ta e="T268" id="Seg_6200" s="T267">0.3.h:A</ta>
            <ta e="T269" id="Seg_6201" s="T268">0.3.h:A</ta>
            <ta e="T270" id="Seg_6202" s="T269">0.3.h:Th</ta>
            <ta e="T271" id="Seg_6203" s="T270">0.3.h:A</ta>
            <ta e="T274" id="Seg_6204" s="T273">np:L</ta>
            <ta e="T275" id="Seg_6205" s="T274">np:L</ta>
            <ta e="T276" id="Seg_6206" s="T275">0.3.h:A</ta>
            <ta e="T278" id="Seg_6207" s="T277">pro:Th</ta>
            <ta e="T281" id="Seg_6208" s="T280">0.3.h:E</ta>
            <ta e="T284" id="Seg_6209" s="T283">np.h:A</ta>
            <ta e="T286" id="Seg_6210" s="T285">np:L</ta>
            <ta e="T289" id="Seg_6211" s="T288">adv:G</ta>
            <ta e="T290" id="Seg_6212" s="T289">0.3.h:A</ta>
            <ta e="T291" id="Seg_6213" s="T290">np.h:A</ta>
            <ta e="T295" id="Seg_6214" s="T294">0.3.h:Th</ta>
            <ta e="T297" id="Seg_6215" s="T296">np.h:E</ta>
            <ta e="T302" id="Seg_6216" s="T301">np.h:A</ta>
            <ta e="T304" id="Seg_6217" s="T303">pro:Th</ta>
            <ta e="T306" id="Seg_6218" s="T305">0.2.h:E</ta>
            <ta e="T307" id="Seg_6219" s="T306">0.3.h:A</ta>
            <ta e="T308" id="Seg_6220" s="T307">np.h:A</ta>
            <ta e="T310" id="Seg_6221" s="T309">pro.h:Poss</ta>
            <ta e="T311" id="Seg_6222" s="T310">np.h:A</ta>
            <ta e="T315" id="Seg_6223" s="T314">0.1.h:Poss np.h:Th</ta>
            <ta e="T316" id="Seg_6224" s="T315">0.3.h:A</ta>
            <ta e="T319" id="Seg_6225" s="T318">np.h:A</ta>
            <ta e="T323" id="Seg_6226" s="T322">np.h:A</ta>
            <ta e="T325" id="Seg_6227" s="T324">pro.h:A</ta>
            <ta e="T326" id="Seg_6228" s="T325">0.3.h:A</ta>
            <ta e="T331" id="Seg_6229" s="T330">pro.h:Th</ta>
            <ta e="T334" id="Seg_6230" s="T333">np.h:Poss</ta>
            <ta e="T337" id="Seg_6231" s="T336">0.3.h:E</ta>
            <ta e="T338" id="Seg_6232" s="T337">0.3.h:Th</ta>
            <ta e="T340" id="Seg_6233" s="T339">0.3.h:A</ta>
            <ta e="T343" id="Seg_6234" s="T342">0.3.h:A</ta>
            <ta e="T347" id="Seg_6235" s="T345">pp:Time</ta>
            <ta e="T349" id="Seg_6236" s="T348">n:Time</ta>
            <ta e="T351" id="Seg_6237" s="T350">0.3.h:Th</ta>
            <ta e="T353" id="Seg_6238" s="T352">0.3.h:A</ta>
            <ta e="T356" id="Seg_6239" s="T355">0.3.h:A</ta>
            <ta e="T358" id="Seg_6240" s="T357">0.3.h:Th</ta>
            <ta e="T360" id="Seg_6241" s="T359">0.3.h:A</ta>
            <ta e="T361" id="Seg_6242" s="T360">np.h:Th</ta>
            <ta e="T362" id="Seg_6243" s="T361">0.3.h:A</ta>
            <ta e="T363" id="Seg_6244" s="T362">0.3.h:Poss np:Th</ta>
            <ta e="T364" id="Seg_6245" s="T363">0.2.h:A</ta>
            <ta e="T365" id="Seg_6246" s="T364">0.3.h:A</ta>
            <ta e="T371" id="Seg_6247" s="T370">n:Time</ta>
            <ta e="T373" id="Seg_6248" s="T372">np:So</ta>
            <ta e="T374" id="Seg_6249" s="T373">0.3.h:Th</ta>
            <ta e="T377" id="Seg_6250" s="T376">np:L</ta>
            <ta e="T378" id="Seg_6251" s="T377">0.3.h:A</ta>
            <ta e="T379" id="Seg_6252" s="T378">0.3.h:Poss np:Th</ta>
            <ta e="T380" id="Seg_6253" s="T379">0.3.h:E</ta>
            <ta e="T381" id="Seg_6254" s="T380">np.h:Th</ta>
            <ta e="T383" id="Seg_6255" s="T382">0.3.h:A</ta>
            <ta e="T385" id="Seg_6256" s="T384">np.h:A</ta>
            <ta e="T387" id="Seg_6257" s="T386">0.3.h:P</ta>
            <ta e="T389" id="Seg_6258" s="T388">0.3.h:Th</ta>
            <ta e="T393" id="Seg_6259" s="T392">np.h:Poss</ta>
            <ta e="T394" id="Seg_6260" s="T393">np:Th</ta>
            <ta e="T395" id="Seg_6261" s="T394">np:G</ta>
            <ta e="T397" id="Seg_6262" s="T396">0.3.h:A</ta>
            <ta e="T399" id="Seg_6263" s="T398">pro.h:Th</ta>
            <ta e="T401" id="Seg_6264" s="T400">0.3.h:Th</ta>
            <ta e="T404" id="Seg_6265" s="T403">np.h:Poss</ta>
            <ta e="T407" id="Seg_6266" s="T406">pro.h:Th</ta>
            <ta e="T408" id="Seg_6267" s="T407">np.h:Th</ta>
            <ta e="T410" id="Seg_6268" s="T409">0.3.h:A</ta>
            <ta e="T414" id="Seg_6269" s="T413">np.h:E</ta>
            <ta e="T415" id="Seg_6270" s="T414">np:G</ta>
            <ta e="T416" id="Seg_6271" s="T415">0.3.h:A</ta>
            <ta e="T418" id="Seg_6272" s="T417">np.h:Th</ta>
            <ta e="T420" id="Seg_6273" s="T419">0.3.h:E</ta>
            <ta e="T421" id="Seg_6274" s="T420">np.h:A</ta>
            <ta e="T425" id="Seg_6275" s="T424">0.3.h:E</ta>
            <ta e="T426" id="Seg_6276" s="T425">np.h:Th</ta>
            <ta e="T427" id="Seg_6277" s="T426">0.3.h:E</ta>
            <ta e="T428" id="Seg_6278" s="T427">0.3.h:Poss np:Th</ta>
            <ta e="T429" id="Seg_6279" s="T428">0.3.h:A</ta>
            <ta e="T431" id="Seg_6280" s="T430">0.3.h:Th</ta>
            <ta e="T433" id="Seg_6281" s="T432">0.3.h:A</ta>
            <ta e="T435" id="Seg_6282" s="T434">0.3.h:Th</ta>
            <ta e="T436" id="Seg_6283" s="T435">0.3.h:A</ta>
            <ta e="T437" id="Seg_6284" s="T436">pro.h:A</ta>
            <ta e="T438" id="Seg_6285" s="T437">np.h:P</ta>
            <ta e="T440" id="Seg_6286" s="T439">np.h:Poss</ta>
            <ta e="T441" id="Seg_6287" s="T440">np:P</ta>
            <ta e="T442" id="Seg_6288" s="T441">0.2.h:A</ta>
            <ta e="T443" id="Seg_6289" s="T442">np:P</ta>
            <ta e="T444" id="Seg_6290" s="T443">0.2.h:A</ta>
            <ta e="T448" id="Seg_6291" s="T447">np.h:P</ta>
            <ta e="T449" id="Seg_6292" s="T448">0.3.h:A</ta>
            <ta e="T452" id="Seg_6293" s="T451">np:P</ta>
            <ta e="T453" id="Seg_6294" s="T452">0.3.h:A</ta>
            <ta e="T455" id="Seg_6295" s="T454">np:Cau</ta>
            <ta e="T460" id="Seg_6296" s="T459">np.h:P</ta>
            <ta e="T463" id="Seg_6297" s="T462">np.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_6298" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_6299" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_6300" s="T5">0.3.h:S adj:pred</ta>
            <ta e="T8" id="Seg_6301" s="T7">np.h:S</ta>
            <ta e="T9" id="Seg_6302" s="T8">s:temp</ta>
            <ta e="T12" id="Seg_6303" s="T9">s:temp</ta>
            <ta e="T14" id="Seg_6304" s="T13">v:pred</ta>
            <ta e="T16" id="Seg_6305" s="T15">np.h:S</ta>
            <ta e="T18" id="Seg_6306" s="T17">v:pred</ta>
            <ta e="T20" id="Seg_6307" s="T18">s:temp</ta>
            <ta e="T21" id="Seg_6308" s="T20">0.3.h:S v:pred</ta>
            <ta e="T22" id="Seg_6309" s="T21">pro.h:S</ta>
            <ta e="T23" id="Seg_6310" s="T22">np.h:O</ta>
            <ta e="T28" id="Seg_6311" s="T27">v:pred</ta>
            <ta e="T29" id="Seg_6312" s="T28">pro.h:S</ta>
            <ta e="T30" id="Seg_6313" s="T29">v:pred</ta>
            <ta e="T33" id="Seg_6314" s="T32">0.1.h:S v:pred</ta>
            <ta e="T34" id="Seg_6315" s="T33">0.3.h:S v:pred</ta>
            <ta e="T36" id="Seg_6316" s="T35">np.h:O</ta>
            <ta e="T39" id="Seg_6317" s="T38">0.3.h:S v:pred</ta>
            <ta e="T41" id="Seg_6318" s="T40">s:adv</ta>
            <ta e="T42" id="Seg_6319" s="T41">np.h:O</ta>
            <ta e="T43" id="Seg_6320" s="T42">0.3.h:S v:pred</ta>
            <ta e="T50" id="Seg_6321" s="T49">np.h:O</ta>
            <ta e="T51" id="Seg_6322" s="T50">0.3.h:S v:pred</ta>
            <ta e="T55" id="Seg_6323" s="T51">s:temp</ta>
            <ta e="T59" id="Seg_6324" s="T55">s:adv</ta>
            <ta e="T60" id="Seg_6325" s="T59">np:S</ta>
            <ta e="T62" id="Seg_6326" s="T61">v:pred</ta>
            <ta e="T66" id="Seg_6327" s="T62">s:adv</ta>
            <ta e="T67" id="Seg_6328" s="T66">pro:O</ta>
            <ta e="T69" id="Seg_6329" s="T68">0.3.h:S v:pred</ta>
            <ta e="T71" id="Seg_6330" s="T70">np:S</ta>
            <ta e="T72" id="Seg_6331" s="T71">cop</ta>
            <ta e="T76" id="Seg_6332" s="T75">np.h:S</ta>
            <ta e="T77" id="Seg_6333" s="T76">ptcl:pred</ta>
            <ta e="T78" id="Seg_6334" s="T77">cop</ta>
            <ta e="T80" id="Seg_6335" s="T79">np.h:S</ta>
            <ta e="T82" id="Seg_6336" s="T81">v:pred</ta>
            <ta e="T83" id="Seg_6337" s="T82">np.h:O</ta>
            <ta e="T85" id="Seg_6338" s="T84">0.3.h:S v:pred</ta>
            <ta e="T88" id="Seg_6339" s="T85">s:temp</ta>
            <ta e="T90" id="Seg_6340" s="T89">s:adv</ta>
            <ta e="T91" id="Seg_6341" s="T90">0.3.h:S v:pred</ta>
            <ta e="T94" id="Seg_6342" s="T93">np.h:S</ta>
            <ta e="T95" id="Seg_6343" s="T94">v:pred</ta>
            <ta e="T97" id="Seg_6344" s="T96">np:S</ta>
            <ta e="T99" id="Seg_6345" s="T98">cop</ta>
            <ta e="T102" id="Seg_6346" s="T99">s:rel</ta>
            <ta e="T103" id="Seg_6347" s="T102">np.h:S</ta>
            <ta e="T109" id="Seg_6348" s="T108">v:pred</ta>
            <ta e="T111" id="Seg_6349" s="T110">np.h:S</ta>
            <ta e="T113" id="Seg_6350" s="T111">s:temp</ta>
            <ta e="T115" id="Seg_6351" s="T113">s:adv</ta>
            <ta e="T116" id="Seg_6352" s="T115">np:S</ta>
            <ta e="T117" id="Seg_6353" s="T116">n:pred</ta>
            <ta e="T120" id="Seg_6354" s="T119">v:pred</ta>
            <ta e="T126" id="Seg_6355" s="T120">s:temp</ta>
            <ta e="T127" id="Seg_6356" s="T126">np:S</ta>
            <ta e="T130" id="Seg_6357" s="T127">s:temp</ta>
            <ta e="T131" id="Seg_6358" s="T130">np:O</ta>
            <ta e="T137" id="Seg_6359" s="T136">0.3.h:S v:pred</ta>
            <ta e="T140" id="Seg_6360" s="T139">cop</ta>
            <ta e="T143" id="Seg_6361" s="T142">np.h:S</ta>
            <ta e="T144" id="Seg_6362" s="T143">v:pred</ta>
            <ta e="T148" id="Seg_6363" s="T144">s:rel</ta>
            <ta e="T150" id="Seg_6364" s="T149">pro.h:S</ta>
            <ta e="T153" id="Seg_6365" s="T152">v:pred</ta>
            <ta e="T154" id="Seg_6366" s="T153">0.3.h:S v:pred</ta>
            <ta e="T155" id="Seg_6367" s="T154">0.3.h:S v:pred</ta>
            <ta e="T158" id="Seg_6368" s="T157">0.2.h:S v:pred</ta>
            <ta e="T159" id="Seg_6369" s="T158">0.3.h:S v:pred</ta>
            <ta e="T161" id="Seg_6370" s="T160">0.1.h:S v:pred</ta>
            <ta e="T162" id="Seg_6371" s="T161">s:cond</ta>
            <ta e="T165" id="Seg_6372" s="T164">0.2.h:S v:pred</ta>
            <ta e="T168" id="Seg_6373" s="T167">np:O</ta>
            <ta e="T169" id="Seg_6374" s="T168">0.1.h:S v:pred</ta>
            <ta e="T170" id="Seg_6375" s="T169">pro.h:O</ta>
            <ta e="T171" id="Seg_6376" s="T170">0.1.h:S v:pred</ta>
            <ta e="T172" id="Seg_6377" s="T171">0.2.h:S v:pred</ta>
            <ta e="T174" id="Seg_6378" s="T173">0.3.h:S v:pred</ta>
            <ta e="T175" id="Seg_6379" s="T174">np.h:S</ta>
            <ta e="T177" id="Seg_6380" s="T176">v:pred</ta>
            <ta e="T180" id="Seg_6381" s="T177">s:adv</ta>
            <ta e="T185" id="Seg_6382" s="T184">0.3.h:S v:pred</ta>
            <ta e="T190" id="Seg_6383" s="T189">0.3.h:S v:pred</ta>
            <ta e="T191" id="Seg_6384" s="T190">0.3.h:S v:pred</ta>
            <ta e="T194" id="Seg_6385" s="T193">np.h:S</ta>
            <ta e="T196" id="Seg_6386" s="T195">v:pred</ta>
            <ta e="T199" id="Seg_6387" s="T198">0.3.h:S v:pred</ta>
            <ta e="T201" id="Seg_6388" s="T200">np:O</ta>
            <ta e="T205" id="Seg_6389" s="T204">0.3.h:S v:pred</ta>
            <ta e="T211" id="Seg_6390" s="T210">0.1.h:S v:pred</ta>
            <ta e="T212" id="Seg_6391" s="T211">0.3.h:S v:pred</ta>
            <ta e="T213" id="Seg_6392" s="T212">0.3.h:S v:pred</ta>
            <ta e="T222" id="Seg_6393" s="T221">np.h:S</ta>
            <ta e="T223" id="Seg_6394" s="T222">ptcl:pred</ta>
            <ta e="T224" id="Seg_6395" s="T223">cop</ta>
            <ta e="T226" id="Seg_6396" s="T225">np.h:S</ta>
            <ta e="T228" id="Seg_6397" s="T227">n:pred</ta>
            <ta e="T233" id="Seg_6398" s="T232">0.3.h:S v:pred</ta>
            <ta e="T235" id="Seg_6399" s="T234">np:O</ta>
            <ta e="T237" id="Seg_6400" s="T236">0.3.h:S v:pred</ta>
            <ta e="T238" id="Seg_6401" s="T237">np:S</ta>
            <ta e="T240" id="Seg_6402" s="T239">quant:pred</ta>
            <ta e="T242" id="Seg_6403" s="T241">0.3.h:S v:pred</ta>
            <ta e="T244" id="Seg_6404" s="T243">np.h:S</ta>
            <ta e="T246" id="Seg_6405" s="T245">adj:pred</ta>
            <ta e="T252" id="Seg_6406" s="T248">s:temp</ta>
            <ta e="T254" id="Seg_6407" s="T253">0.3.h:S v:pred</ta>
            <ta e="T257" id="Seg_6408" s="T254">s:comp</ta>
            <ta e="T258" id="Seg_6409" s="T257">0.3.h:S v:pred</ta>
            <ta e="T259" id="Seg_6410" s="T258">pro.h:S</ta>
            <ta e="T261" id="Seg_6411" s="T260">v:pred</ta>
            <ta e="T263" id="Seg_6412" s="T262">0.1.h:S cop</ta>
            <ta e="T265" id="Seg_6413" s="T264">0.3.h:S v:pred</ta>
            <ta e="T269" id="Seg_6414" s="T265">s:temp</ta>
            <ta e="T270" id="Seg_6415" s="T269">s:comp</ta>
            <ta e="T271" id="Seg_6416" s="T270">0.3.h:S v:pred</ta>
            <ta e="T276" id="Seg_6417" s="T275">0.3.h:S v:pred</ta>
            <ta e="T278" id="Seg_6418" s="T277">pro:O</ta>
            <ta e="T281" id="Seg_6419" s="T280">0.3.h:S v:pred</ta>
            <ta e="T284" id="Seg_6420" s="T283">np.h:S</ta>
            <ta e="T288" id="Seg_6421" s="T287">v:pred</ta>
            <ta e="T290" id="Seg_6422" s="T288">s:temp</ta>
            <ta e="T291" id="Seg_6423" s="T290">np.h:S</ta>
            <ta e="T293" id="Seg_6424" s="T292">v:pred</ta>
            <ta e="T295" id="Seg_6425" s="T293">s:temp</ta>
            <ta e="T297" id="Seg_6426" s="T296">np.h:S</ta>
            <ta e="T299" id="Seg_6427" s="T298">v:pred</ta>
            <ta e="T302" id="Seg_6428" s="T301">np.h:S</ta>
            <ta e="T303" id="Seg_6429" s="T302">v:pred</ta>
            <ta e="T305" id="Seg_6430" s="T303">s:adv</ta>
            <ta e="T306" id="Seg_6431" s="T305">0.2.h:S v:pred</ta>
            <ta e="T307" id="Seg_6432" s="T306">s:adv</ta>
            <ta e="T308" id="Seg_6433" s="T307">np.h:S</ta>
            <ta e="T309" id="Seg_6434" s="T308">v:pred</ta>
            <ta e="T311" id="Seg_6435" s="T310">np.h:S</ta>
            <ta e="T316" id="Seg_6436" s="T311">s:adv</ta>
            <ta e="T317" id="Seg_6437" s="T316">v:pred</ta>
            <ta e="T319" id="Seg_6438" s="T318">np.h:S</ta>
            <ta e="T320" id="Seg_6439" s="T319">v:pred</ta>
            <ta e="T324" id="Seg_6440" s="T322">s:temp</ta>
            <ta e="T325" id="Seg_6441" s="T324">pro.h:S</ta>
            <ta e="T326" id="Seg_6442" s="T325">s:adv</ta>
            <ta e="T327" id="Seg_6443" s="T326">v:pred</ta>
            <ta e="T331" id="Seg_6444" s="T330">pro.h:S</ta>
            <ta e="T332" id="Seg_6445" s="T331">cop</ta>
            <ta e="T335" id="Seg_6446" s="T334">n:pred</ta>
            <ta e="T337" id="Seg_6447" s="T336">0.3.h:S v:pred</ta>
            <ta e="T339" id="Seg_6448" s="T337">s:temp</ta>
            <ta e="T341" id="Seg_6449" s="T339">s:temp</ta>
            <ta e="T343" id="Seg_6450" s="T342">0.3.h:S v:pred</ta>
            <ta e="T351" id="Seg_6451" s="T350">0.3.h:S v:pred</ta>
            <ta e="T354" id="Seg_6452" s="T352">s:temp</ta>
            <ta e="T356" id="Seg_6453" s="T355">0.3.h:S v:pred</ta>
            <ta e="T358" id="Seg_6454" s="T357">s:adv</ta>
            <ta e="T360" id="Seg_6455" s="T359">s:adv</ta>
            <ta e="T361" id="Seg_6456" s="T360">np.h:O</ta>
            <ta e="T362" id="Seg_6457" s="T361">0.3.h:S v:pred</ta>
            <ta e="T363" id="Seg_6458" s="T362">np:O</ta>
            <ta e="T364" id="Seg_6459" s="T363">0.2.h:S v:pred</ta>
            <ta e="T365" id="Seg_6460" s="T364">s:adv</ta>
            <ta e="T367" id="Seg_6461" s="T366">0.3.h:S v:pred</ta>
            <ta e="T374" id="Seg_6462" s="T371">s:adv</ta>
            <ta e="T378" id="Seg_6463" s="T377">0.3.h:S v:pred</ta>
            <ta e="T379" id="Seg_6464" s="T378">np:O</ta>
            <ta e="T380" id="Seg_6465" s="T379">0.3.h:S v:pred</ta>
            <ta e="T381" id="Seg_6466" s="T380">np.h:O</ta>
            <ta e="T383" id="Seg_6467" s="T382">0.3.h:S v:pred</ta>
            <ta e="T385" id="Seg_6468" s="T384">np.h:S</ta>
            <ta e="T386" id="Seg_6469" s="T385">v:pred</ta>
            <ta e="T391" id="Seg_6470" s="T386">s:adv</ta>
            <ta e="T394" id="Seg_6471" s="T393">np:O</ta>
            <ta e="T397" id="Seg_6472" s="T396">0.3.h:S v:pred</ta>
            <ta e="T399" id="Seg_6473" s="T398">pro.h:S</ta>
            <ta e="T401" id="Seg_6474" s="T399">s:temp</ta>
            <ta e="T405" id="Seg_6475" s="T404">n:pred</ta>
            <ta e="T406" id="Seg_6476" s="T405">cop</ta>
            <ta e="T408" id="Seg_6477" s="T407">np.h:S</ta>
            <ta e="T410" id="Seg_6478" s="T408">s:adv</ta>
            <ta e="T411" id="Seg_6479" s="T410">v:pred</ta>
            <ta e="T414" id="Seg_6480" s="T413">np.h:S</ta>
            <ta e="T416" id="Seg_6481" s="T414">s:comp</ta>
            <ta e="T417" id="Seg_6482" s="T416">v:pred</ta>
            <ta e="T418" id="Seg_6483" s="T417">np.h:S</ta>
            <ta e="T419" id="Seg_6484" s="T418">ptcl:pred</ta>
            <ta e="T420" id="Seg_6485" s="T419">0.3.h:S v:pred</ta>
            <ta e="T421" id="Seg_6486" s="T420">np.h:S</ta>
            <ta e="T422" id="Seg_6487" s="T421">v:pred</ta>
            <ta e="T425" id="Seg_6488" s="T424">0.3.h:S v:pred</ta>
            <ta e="T427" id="Seg_6489" s="T425">s:adv</ta>
            <ta e="T429" id="Seg_6490" s="T427">s:adv</ta>
            <ta e="T431" id="Seg_6491" s="T429">s:adv</ta>
            <ta e="T433" id="Seg_6492" s="T432">0.3.h:S v:pred</ta>
            <ta e="T435" id="Seg_6493" s="T433">s:comp</ta>
            <ta e="T436" id="Seg_6494" s="T435">0.3.h:S v:pred</ta>
            <ta e="T437" id="Seg_6495" s="T436">pro.h:S</ta>
            <ta e="T438" id="Seg_6496" s="T437">pro.h:O</ta>
            <ta e="T439" id="Seg_6497" s="T438">v:pred</ta>
            <ta e="T444" id="Seg_6498" s="T439">s:cond</ta>
            <ta e="T449" id="Seg_6499" s="T445">s:temp</ta>
            <ta e="T452" id="Seg_6500" s="T451">np:O</ta>
            <ta e="T453" id="Seg_6501" s="T452">0.3.h:S v:pred</ta>
            <ta e="T460" id="Seg_6502" s="T459">np.h:S</ta>
            <ta e="T462" id="Seg_6503" s="T461">v:pred</ta>
            <ta e="T463" id="Seg_6504" s="T462">np.h:S</ta>
            <ta e="T465" id="Seg_6505" s="T464">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T4" id="Seg_6506" s="T3">new</ta>
            <ta e="T6" id="Seg_6507" s="T5">0.giv-active new</ta>
            <ta e="T8" id="Seg_6508" s="T7">giv-active</ta>
            <ta e="T9" id="Seg_6509" s="T8">0.giv-active</ta>
            <ta e="T11" id="Seg_6510" s="T10">0.giv-active</ta>
            <ta e="T16" id="Seg_6511" s="T15">giv-inactive</ta>
            <ta e="T21" id="Seg_6512" s="T20">0.giv-active 0.quot-sp</ta>
            <ta e="T22" id="Seg_6513" s="T21">giv-active-Q</ta>
            <ta e="T23" id="Seg_6514" s="T22">giv-inactive-Q</ta>
            <ta e="T25" id="Seg_6515" s="T24">giv-active-Q</ta>
            <ta e="T29" id="Seg_6516" s="T28">giv-active-Q</ta>
            <ta e="T32" id="Seg_6517" s="T31">new-Q</ta>
            <ta e="T33" id="Seg_6518" s="T32">0.giv-active-Q</ta>
            <ta e="T34" id="Seg_6519" s="T33">0.giv-active 0.quot-sp</ta>
            <ta e="T36" id="Seg_6520" s="T35">giv-active</ta>
            <ta e="T37" id="Seg_6521" s="T36">new</ta>
            <ta e="T39" id="Seg_6522" s="T38">0.giv-active</ta>
            <ta e="T41" id="Seg_6523" s="T40">0.giv-active</ta>
            <ta e="T42" id="Seg_6524" s="T41">new</ta>
            <ta e="T43" id="Seg_6525" s="T42">0.giv-active</ta>
            <ta e="T44" id="Seg_6526" s="T43">accs-inf</ta>
            <ta e="T50" id="Seg_6527" s="T49">accs-inf</ta>
            <ta e="T51" id="Seg_6528" s="T50">0.giv-active</ta>
            <ta e="T53" id="Seg_6529" s="T52">new</ta>
            <ta e="T56" id="Seg_6530" s="T55">giv-inactive</ta>
            <ta e="T57" id="Seg_6531" s="T56">accs-inf</ta>
            <ta e="T60" id="Seg_6532" s="T59">accs-inf</ta>
            <ta e="T65" id="Seg_6533" s="T64">giv-inactive</ta>
            <ta e="T69" id="Seg_6534" s="T68">0.giv-active</ta>
            <ta e="T71" id="Seg_6535" s="T70">accs-sit</ta>
            <ta e="T74" id="Seg_6536" s="T73">giv-inactive</ta>
            <ta e="T75" id="Seg_6537" s="T74">accs-inf</ta>
            <ta e="T76" id="Seg_6538" s="T75">new</ta>
            <ta e="T80" id="Seg_6539" s="T79">giv-active</ta>
            <ta e="T83" id="Seg_6540" s="T82">giv-active</ta>
            <ta e="T85" id="Seg_6541" s="T84">0.giv-active</ta>
            <ta e="T87" id="Seg_6542" s="T86">0.giv-active</ta>
            <ta e="T89" id="Seg_6543" s="T88">giv-inactive</ta>
            <ta e="T90" id="Seg_6544" s="T89">0.giv-active</ta>
            <ta e="T91" id="Seg_6545" s="T90">0.giv-active 0.quot-sp</ta>
            <ta e="T93" id="Seg_6546" s="T92">giv-active-Q</ta>
            <ta e="T94" id="Seg_6547" s="T93">giv-inactive-Q</ta>
            <ta e="T100" id="Seg_6548" s="T99">giv-active-Q</ta>
            <ta e="T101" id="Seg_6549" s="T100">accs-inf-Q</ta>
            <ta e="T103" id="Seg_6550" s="T102">new-Q</ta>
            <ta e="T104" id="Seg_6551" s="T103">accs-gen-Q</ta>
            <ta e="T106" id="Seg_6552" s="T105">accs-gen-Q</ta>
            <ta e="T111" id="Seg_6553" s="T110">accs-inf-Q</ta>
            <ta e="T113" id="Seg_6554" s="T112">0.giv-active-Q</ta>
            <ta e="T114" id="Seg_6555" s="T113">accs-gen-Q</ta>
            <ta e="T115" id="Seg_6556" s="T114">0.giv-active-Q</ta>
            <ta e="T116" id="Seg_6557" s="T115">accs-inf-Q</ta>
            <ta e="T117" id="Seg_6558" s="T116">new-Q</ta>
            <ta e="T121" id="Seg_6559" s="T120">accs-inf-Q</ta>
            <ta e="T123" id="Seg_6560" s="T122">0.giv-active-Q</ta>
            <ta e="T124" id="Seg_6561" s="T123">accs-gen-Q</ta>
            <ta e="T127" id="Seg_6562" s="T126">accs-inf-Q</ta>
            <ta e="T128" id="Seg_6563" s="T127">0.giv-active-Q</ta>
            <ta e="T130" id="Seg_6564" s="T129">0.giv-active-Q</ta>
            <ta e="T131" id="Seg_6565" s="T130">giv-active-Q</ta>
            <ta e="T137" id="Seg_6566" s="T136">0.giv-active-Q</ta>
            <ta e="T143" id="Seg_6567" s="T142">giv-inactive</ta>
            <ta e="T144" id="Seg_6568" s="T143">quot-sp</ta>
            <ta e="T146" id="Seg_6569" s="T145">giv-inactive-Q</ta>
            <ta e="T150" id="Seg_6570" s="T149">giv-inactive-Q</ta>
            <ta e="T154" id="Seg_6571" s="T153">0.giv-active 0.quot-sp</ta>
            <ta e="T155" id="Seg_6572" s="T154">0.giv-active</ta>
            <ta e="T158" id="Seg_6573" s="T157">0.giv-active-Q</ta>
            <ta e="T159" id="Seg_6574" s="T158">0.giv-active 0.quot-sp</ta>
            <ta e="T161" id="Seg_6575" s="T160">0.giv-active-Q</ta>
            <ta e="T162" id="Seg_6576" s="T161">0.giv-inactive-Q</ta>
            <ta e="T163" id="Seg_6577" s="T162">giv-active-Q</ta>
            <ta e="T165" id="Seg_6578" s="T164">0.giv-active-Q</ta>
            <ta e="T167" id="Seg_6579" s="T166">new-Q</ta>
            <ta e="T168" id="Seg_6580" s="T167">accs-inf-Q</ta>
            <ta e="T169" id="Seg_6581" s="T168">0.giv-active-Q</ta>
            <ta e="T170" id="Seg_6582" s="T169">giv-active-Q</ta>
            <ta e="T171" id="Seg_6583" s="T170">0.giv-active-Q</ta>
            <ta e="T172" id="Seg_6584" s="T171">0.giv-active-Q</ta>
            <ta e="T174" id="Seg_6585" s="T173">0.giv-active 0.quot-sp</ta>
            <ta e="T175" id="Seg_6586" s="T174">giv-active</ta>
            <ta e="T180" id="Seg_6587" s="T179">0.giv-active</ta>
            <ta e="T184" id="Seg_6588" s="T183">accs-inf</ta>
            <ta e="T185" id="Seg_6589" s="T184">0.giv-active</ta>
            <ta e="T189" id="Seg_6590" s="T188">giv-active</ta>
            <ta e="T190" id="Seg_6591" s="T189">0.giv-active</ta>
            <ta e="T191" id="Seg_6592" s="T190">0.giv-active</ta>
            <ta e="T194" id="Seg_6593" s="T193">new</ta>
            <ta e="T199" id="Seg_6594" s="T198">0.giv-active</ta>
            <ta e="T200" id="Seg_6595" s="T199">giv-inactive</ta>
            <ta e="T201" id="Seg_6596" s="T200">giv-inactive</ta>
            <ta e="T205" id="Seg_6597" s="T204">0.giv-active</ta>
            <ta e="T208" id="Seg_6598" s="T207">giv-inactive-Q</ta>
            <ta e="T210" id="Seg_6599" s="T209">accs-inf-Q</ta>
            <ta e="T211" id="Seg_6600" s="T210">0.giv-active-Q</ta>
            <ta e="T212" id="Seg_6601" s="T211">0.giv-active 0.quot-th</ta>
            <ta e="T213" id="Seg_6602" s="T212">0.giv-active</ta>
            <ta e="T215" id="Seg_6603" s="T214">giv-active</ta>
            <ta e="T222" id="Seg_6604" s="T221">new</ta>
            <ta e="T225" id="Seg_6605" s="T224">giv-active</ta>
            <ta e="T226" id="Seg_6606" s="T225">accs-inf</ta>
            <ta e="T233" id="Seg_6607" s="T232">0.giv-inactive</ta>
            <ta e="T234" id="Seg_6608" s="T233">accs-inf</ta>
            <ta e="T235" id="Seg_6609" s="T234">accs-inf</ta>
            <ta e="T237" id="Seg_6610" s="T236">0.giv-active</ta>
            <ta e="T238" id="Seg_6611" s="T237">accs-inf</ta>
            <ta e="T242" id="Seg_6612" s="T241">0.giv-active</ta>
            <ta e="T244" id="Seg_6613" s="T243">giv-inactive</ta>
            <ta e="T246" id="Seg_6614" s="T245">new</ta>
            <ta e="T251" id="Seg_6615" s="T250">new</ta>
            <ta e="T252" id="Seg_6616" s="T251">0.giv-active</ta>
            <ta e="T254" id="Seg_6617" s="T253">0.giv-active</ta>
            <ta e="T255" id="Seg_6618" s="T254">0.giv-active</ta>
            <ta e="T257" id="Seg_6619" s="T256">0.giv-inactive</ta>
            <ta e="T258" id="Seg_6620" s="T257">0.giv-active</ta>
            <ta e="T259" id="Seg_6621" s="T258">accs-inf</ta>
            <ta e="T263" id="Seg_6622" s="T262">0.giv-active-Q</ta>
            <ta e="T265" id="Seg_6623" s="T264">0.giv-active 0.quot-th</ta>
            <ta e="T266" id="Seg_6624" s="T265">accs-inf</ta>
            <ta e="T267" id="Seg_6625" s="T266">accs-inf</ta>
            <ta e="T268" id="Seg_6626" s="T267">0.giv-active</ta>
            <ta e="T269" id="Seg_6627" s="T268">0.giv-active</ta>
            <ta e="T270" id="Seg_6628" s="T269">0.giv-active</ta>
            <ta e="T271" id="Seg_6629" s="T270">0.giv-active</ta>
            <ta e="T274" id="Seg_6630" s="T273">giv-inactive</ta>
            <ta e="T275" id="Seg_6631" s="T274">giv-active</ta>
            <ta e="T276" id="Seg_6632" s="T275">0.giv-active</ta>
            <ta e="T281" id="Seg_6633" s="T280">0.giv-active</ta>
            <ta e="T284" id="Seg_6634" s="T283">giv-inactive</ta>
            <ta e="T286" id="Seg_6635" s="T285">accs-inf</ta>
            <ta e="T290" id="Seg_6636" s="T289">0.giv-inactive</ta>
            <ta e="T291" id="Seg_6637" s="T290">giv-active</ta>
            <ta e="T295" id="Seg_6638" s="T294">0.accs-inf</ta>
            <ta e="T297" id="Seg_6639" s="T296">giv-active</ta>
            <ta e="T302" id="Seg_6640" s="T301">accs-inf</ta>
            <ta e="T303" id="Seg_6641" s="T302">quot-sp</ta>
            <ta e="T306" id="Seg_6642" s="T305">0.giv-inactive-Q</ta>
            <ta e="T307" id="Seg_6643" s="T306">0.giv-active 0.quot-sp</ta>
            <ta e="T308" id="Seg_6644" s="T307">giv-active</ta>
            <ta e="T309" id="Seg_6645" s="T308">quot-sp</ta>
            <ta e="T310" id="Seg_6646" s="T309">giv-active-Q</ta>
            <ta e="T311" id="Seg_6647" s="T310">accs-inf-Q</ta>
            <ta e="T315" id="Seg_6648" s="T314">new-Q</ta>
            <ta e="T316" id="Seg_6649" s="T315">0.giv-active-Q</ta>
            <ta e="T319" id="Seg_6650" s="T318">giv-inactive-Q</ta>
            <ta e="T320" id="Seg_6651" s="T319">quot-sp</ta>
            <ta e="T323" id="Seg_6652" s="T322">giv-inactive-Q</ta>
            <ta e="T325" id="Seg_6653" s="T324">giv-active-Q</ta>
            <ta e="T326" id="Seg_6654" s="T325">0.giv-active-Q</ta>
            <ta e="T331" id="Seg_6655" s="T330">giv-inactive-Q</ta>
            <ta e="T337" id="Seg_6656" s="T336">0.giv-inactive 0.quot-th</ta>
            <ta e="T338" id="Seg_6657" s="T337">0.giv-active</ta>
            <ta e="T340" id="Seg_6658" s="T339">0.giv-active</ta>
            <ta e="T343" id="Seg_6659" s="T342">0.giv-active</ta>
            <ta e="T351" id="Seg_6660" s="T350">0.giv-active</ta>
            <ta e="T353" id="Seg_6661" s="T352">0.giv-active</ta>
            <ta e="T356" id="Seg_6662" s="T355">0.giv-active</ta>
            <ta e="T358" id="Seg_6663" s="T357">0.giv-active</ta>
            <ta e="T360" id="Seg_6664" s="T359">0.accs-sit</ta>
            <ta e="T361" id="Seg_6665" s="T360">accs-inf</ta>
            <ta e="T362" id="Seg_6666" s="T361">giv-active</ta>
            <ta e="T363" id="Seg_6667" s="T362">giv-inactive-Q</ta>
            <ta e="T364" id="Seg_6668" s="T363">0.giv-active-Q</ta>
            <ta e="T365" id="Seg_6669" s="T364">0.giv-inactive 0.quot-sp</ta>
            <ta e="T367" id="Seg_6670" s="T366">0.giv-active</ta>
            <ta e="T373" id="Seg_6671" s="T372">giv-inactive</ta>
            <ta e="T374" id="Seg_6672" s="T373">0.giv-active</ta>
            <ta e="T377" id="Seg_6673" s="T376">new</ta>
            <ta e="T378" id="Seg_6674" s="T377">0.giv-active</ta>
            <ta e="T379" id="Seg_6675" s="T378">giv-active</ta>
            <ta e="T380" id="Seg_6676" s="T379">0.giv-active</ta>
            <ta e="T381" id="Seg_6677" s="T380">giv-inactive</ta>
            <ta e="T383" id="Seg_6678" s="T382">0.giv-active</ta>
            <ta e="T385" id="Seg_6679" s="T384">giv-active</ta>
            <ta e="T387" id="Seg_6680" s="T386">0.giv-active</ta>
            <ta e="T389" id="Seg_6681" s="T388">0.giv-active</ta>
            <ta e="T393" id="Seg_6682" s="T392">giv-active</ta>
            <ta e="T394" id="Seg_6683" s="T393">giv-inactive</ta>
            <ta e="T397" id="Seg_6684" s="T396">0.giv-active</ta>
            <ta e="T399" id="Seg_6685" s="T398">giv-active</ta>
            <ta e="T401" id="Seg_6686" s="T400">0.giv-active</ta>
            <ta e="T404" id="Seg_6687" s="T403">giv-inactive</ta>
            <ta e="T407" id="Seg_6688" s="T406">giv-active</ta>
            <ta e="T408" id="Seg_6689" s="T407">giv-inactive</ta>
            <ta e="T410" id="Seg_6690" s="T409">0.giv-active</ta>
            <ta e="T414" id="Seg_6691" s="T413">giv-inactive</ta>
            <ta e="T415" id="Seg_6692" s="T414">giv-inactive</ta>
            <ta e="T416" id="Seg_6693" s="T415">0.giv-active</ta>
            <ta e="T420" id="Seg_6694" s="T419">0.giv-active</ta>
            <ta e="T421" id="Seg_6695" s="T420">giv-inactive</ta>
            <ta e="T425" id="Seg_6696" s="T424">0.giv-active</ta>
            <ta e="T426" id="Seg_6697" s="T425">giv-inactive</ta>
            <ta e="T427" id="Seg_6698" s="T426">0.giv-active</ta>
            <ta e="T428" id="Seg_6699" s="T427">accs-inf</ta>
            <ta e="T429" id="Seg_6700" s="T428">0.giv-active</ta>
            <ta e="T431" id="Seg_6701" s="T430">0.giv-active</ta>
            <ta e="T433" id="Seg_6702" s="T432">0.giv-active</ta>
            <ta e="T435" id="Seg_6703" s="T434">0.giv-active</ta>
            <ta e="T436" id="Seg_6704" s="T435">0.giv-active</ta>
            <ta e="T437" id="Seg_6705" s="T436">giv-active-Q</ta>
            <ta e="T438" id="Seg_6706" s="T437">giv-active-Q</ta>
            <ta e="T440" id="Seg_6707" s="T439">new-Q</ta>
            <ta e="T441" id="Seg_6708" s="T440">accs-inf-Q</ta>
            <ta e="T442" id="Seg_6709" s="T441">0.giv-active-Q</ta>
            <ta e="T443" id="Seg_6710" s="T442">new-Q</ta>
            <ta e="T444" id="Seg_6711" s="T443">0.giv-active-Q</ta>
            <ta e="T448" id="Seg_6712" s="T447">giv-active</ta>
            <ta e="T449" id="Seg_6713" s="T448">0.giv-inactive</ta>
            <ta e="T451" id="Seg_6714" s="T450">giv-active</ta>
            <ta e="T452" id="Seg_6715" s="T451">giv-active</ta>
            <ta e="T453" id="Seg_6716" s="T452">0.giv-active</ta>
            <ta e="T460" id="Seg_6717" s="T459">accs-gen</ta>
            <ta e="T463" id="Seg_6718" s="T462">giv-active</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T1" id="Seg_6719" s="T0">top.int.concr</ta>
            <ta e="T6" id="Seg_6720" s="T5">0.top.int.concr</ta>
            <ta e="T8" id="Seg_6721" s="T6">top.int.concr</ta>
            <ta e="T18" id="Seg_6722" s="T17">0.top.int.abstr.</ta>
            <ta e="T20" id="Seg_6723" s="T18">top.int.concr</ta>
            <ta e="T28" id="Seg_6724" s="T27">0.top.int.concr</ta>
            <ta e="T29" id="Seg_6725" s="T28">top.int.concr</ta>
            <ta e="T33" id="Seg_6726" s="T32">0.top.int.concr</ta>
            <ta e="T39" id="Seg_6727" s="T38">0.top.int.concr</ta>
            <ta e="T43" id="Seg_6728" s="T42">0.top.int.concr</ta>
            <ta e="T51" id="Seg_6729" s="T50">0.top.int.concr</ta>
            <ta e="T57" id="Seg_6730" s="T55">top.int.concr</ta>
            <ta e="T65" id="Seg_6731" s="T62">top.int.concr</ta>
            <ta e="T69" id="Seg_6732" s="T68">0.top.int.concr</ta>
            <ta e="T72" id="Seg_6733" s="T71">0.top.int.abstr</ta>
            <ta e="T75" id="Seg_6734" s="T72">top.int.concr</ta>
            <ta e="T80" id="Seg_6735" s="T78">top.int.concr</ta>
            <ta e="T84" id="Seg_6736" s="T83">top.int.concr</ta>
            <ta e="T88" id="Seg_6737" s="T85">top.int.concr</ta>
            <ta e="T94" id="Seg_6738" s="T91">top.int.concr</ta>
            <ta e="T103" id="Seg_6739" s="T99">top.int.concr</ta>
            <ta e="T137" id="Seg_6740" s="T136">0.top.int.concr</ta>
            <ta e="T141" id="Seg_6741" s="T140">top.int.concr</ta>
            <ta e="T158" id="Seg_6742" s="T157">0.top.int.concr</ta>
            <ta e="T162" id="Seg_6743" s="T161">top.int.concr.contr</ta>
            <ta e="T169" id="Seg_6744" s="T168">0.top.int.concr</ta>
            <ta e="T171" id="Seg_6745" s="T170">0.top.int.concr</ta>
            <ta e="T175" id="Seg_6746" s="T174">top.int.concr</ta>
            <ta e="T180" id="Seg_6747" s="T177">top.int.concr</ta>
            <ta e="T190" id="Seg_6748" s="T189">0.top.int.concr</ta>
            <ta e="T191" id="Seg_6749" s="T190">0.top.int.concr</ta>
            <ta e="T196" id="Seg_6750" s="T195">0.top.int.abstr</ta>
            <ta e="T197" id="Seg_6751" s="T196">top.int.concr</ta>
            <ta e="T205" id="Seg_6752" s="T204">0.top.int.concr</ta>
            <ta e="T211" id="Seg_6753" s="T210">0.top.int.concr</ta>
            <ta e="T213" id="Seg_6754" s="T212">0.top.int.concr</ta>
            <ta e="T216" id="Seg_6755" s="T215">top.int.concr</ta>
            <ta e="T226" id="Seg_6756" s="T224">top.int.concr</ta>
            <ta e="T233" id="Seg_6757" s="T232">0.top.int.concr</ta>
            <ta e="T237" id="Seg_6758" s="T236">0.top.int.concr</ta>
            <ta e="T238" id="Seg_6759" s="T237">top.int.concr</ta>
            <ta e="T242" id="Seg_6760" s="T241">0.top.int.concr</ta>
            <ta e="T244" id="Seg_6761" s="T242">top.int.concr</ta>
            <ta e="T254" id="Seg_6762" s="T253">0.top.int.concr</ta>
            <ta e="T258" id="Seg_6763" s="T257">0.top.int.concr</ta>
            <ta e="T271" id="Seg_6764" s="T270">0.top.int.concr</ta>
            <ta e="T276" id="Seg_6765" s="T275">0.top.int.concr</ta>
            <ta e="T281" id="Seg_6766" s="T280">0.top.int.concr</ta>
            <ta e="T288" id="Seg_6767" s="T287">0.top.int.abstr.</ta>
            <ta e="T291" id="Seg_6768" s="T290">top.int.concr</ta>
            <ta e="T295" id="Seg_6769" s="T293">top.int.concr</ta>
            <ta e="T303" id="Seg_6770" s="T302">0.top.int.abstr.</ta>
            <ta e="T308" id="Seg_6771" s="T307">top.int.concr</ta>
            <ta e="T311" id="Seg_6772" s="T309">top.int.concr</ta>
            <ta e="T320" id="Seg_6773" s="T319">0.top.int.abstr.</ta>
            <ta e="T324" id="Seg_6774" s="T322">top.int.concr</ta>
            <ta e="T339" id="Seg_6775" s="T337">top.int.concr</ta>
            <ta e="T347" id="Seg_6776" s="T344">top.int.concr</ta>
            <ta e="T349" id="Seg_6777" s="T347">top.int.concr</ta>
            <ta e="T354" id="Seg_6778" s="T351">top.int.concr</ta>
            <ta e="T362" id="Seg_6779" s="T361">0.top.int.abstr</ta>
            <ta e="T378" id="Seg_6780" s="T377">0.top.int.abstr.</ta>
            <ta e="T380" id="Seg_6781" s="T379">0.top.int.concr</ta>
            <ta e="T383" id="Seg_6782" s="T382">0.top.int.concr</ta>
            <ta e="T385" id="Seg_6783" s="T384">top.int.concr</ta>
            <ta e="T394" id="Seg_6784" s="T391">top.int.concr</ta>
            <ta e="T399" id="Seg_6785" s="T398">top.int.concr</ta>
            <ta e="T406" id="Seg_6786" s="T405">0.top.int.concr</ta>
            <ta e="T407" id="Seg_6787" s="T406">top.int.concr</ta>
            <ta e="T414" id="Seg_6788" s="T411">top.int.concr</ta>
            <ta e="T420" id="Seg_6789" s="T419">0.top.int.concr</ta>
            <ta e="T425" id="Seg_6790" s="T424">0.top.int.abstr.</ta>
            <ta e="T433" id="Seg_6791" s="T432">0.top.int.concr</ta>
            <ta e="T436" id="Seg_6792" s="T435">0.top.int.concr</ta>
            <ta e="T437" id="Seg_6793" s="T436">top.int.concr</ta>
            <ta e="T449" id="Seg_6794" s="T448">0.top.int.concr</ta>
            <ta e="T451" id="Seg_6795" s="T449">top.int.concr</ta>
            <ta e="T455" id="Seg_6796" s="T453">top.int.concr</ta>
            <ta e="T463" id="Seg_6797" s="T462">top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T5" id="Seg_6798" s="T0">foc.wid</ta>
            <ta e="T6" id="Seg_6799" s="T5">foc.int</ta>
            <ta e="T14" id="Seg_6800" s="T12">foc.int</ta>
            <ta e="T18" id="Seg_6801" s="T14">foc.wid</ta>
            <ta e="T21" id="Seg_6802" s="T20">foc.int</ta>
            <ta e="T28" id="Seg_6803" s="T21">foc.int</ta>
            <ta e="T30" id="Seg_6804" s="T29">foc.int</ta>
            <ta e="T32" id="Seg_6805" s="T31">foc.nar</ta>
            <ta e="T39" id="Seg_6806" s="T34">foc.int</ta>
            <ta e="T43" id="Seg_6807" s="T39">foc.int</ta>
            <ta e="T45" id="Seg_6808" s="T43">foc.nar</ta>
            <ta e="T50" id="Seg_6809" s="T47">foc.nar</ta>
            <ta e="T59" id="Seg_6810" s="T57">foc.int</ta>
            <ta e="T61" id="Seg_6811" s="T59">foc.nar</ta>
            <ta e="T66" id="Seg_6812" s="T65">foc.int</ta>
            <ta e="T72" id="Seg_6813" s="T69">foc.wid</ta>
            <ta e="T76" id="Seg_6814" s="T75">foc.nar</ta>
            <ta e="T83" id="Seg_6815" s="T80">foc.int</ta>
            <ta e="T85" id="Seg_6816" s="T84">foc.int</ta>
            <ta e="T91" id="Seg_6817" s="T88">foc.int</ta>
            <ta e="T95" id="Seg_6818" s="T94">foc.nar</ta>
            <ta e="T109" id="Seg_6819" s="T103">foc.int</ta>
            <ta e="T135" id="Seg_6820" s="T133">foc.nar</ta>
            <ta e="T144" id="Seg_6821" s="T143">foc.int</ta>
            <ta e="T151" id="Seg_6822" s="T150">foc.nar</ta>
            <ta e="T158" id="Seg_6823" s="T155">foc.int</ta>
            <ta e="T163" id="Seg_6824" s="T162">foc.nar</ta>
            <ta e="T168" id="Seg_6825" s="T166">foc.nar</ta>
            <ta e="T170" id="Seg_6826" s="T169">foc.nar</ta>
            <ta e="T172" id="Seg_6827" s="T171">foc.nar</ta>
            <ta e="T177" id="Seg_6828" s="T175">foc.int</ta>
            <ta e="T185" id="Seg_6829" s="T182">foc.int</ta>
            <ta e="T190" id="Seg_6830" s="T186">foc.int</ta>
            <ta e="T191" id="Seg_6831" s="T190">foc.int</ta>
            <ta e="T196" id="Seg_6832" s="T191">foc.wid</ta>
            <ta e="T199" id="Seg_6833" s="T197">foc.nar</ta>
            <ta e="T205" id="Seg_6834" s="T203">foc.nar</ta>
            <ta e="T210" id="Seg_6835" s="T208">foc.nar</ta>
            <ta e="T215" id="Seg_6836" s="T213">foc.nar</ta>
            <ta e="T222" id="Seg_6837" s="T216">foc.nar</ta>
            <ta e="T228" id="Seg_6838" s="T226">foc.nar</ta>
            <ta e="T229" id="Seg_6839" s="T228">foc.nar</ta>
            <ta e="T237" id="Seg_6840" s="T233">foc.int</ta>
            <ta e="T240" id="Seg_6841" s="T238">foc.nar</ta>
            <ta e="T242" id="Seg_6842" s="T240">foc.int</ta>
            <ta e="T246" id="Seg_6843" s="T244">foc.int</ta>
            <ta e="T254" id="Seg_6844" s="T248">foc.int</ta>
            <ta e="T258" id="Seg_6845" s="T254">foc.int</ta>
            <ta e="T260" id="Seg_6846" s="T258">foc.nar</ta>
            <ta e="T262" id="Seg_6847" s="T261">foc.nar</ta>
            <ta e="T271" id="Seg_6848" s="T265">foc.int</ta>
            <ta e="T276" id="Seg_6849" s="T271">foc.int</ta>
            <ta e="T279" id="Seg_6850" s="T277">foc.nar</ta>
            <ta e="T286" id="Seg_6851" s="T284">foc.nar</ta>
            <ta e="T293" id="Seg_6852" s="T288">foc.int</ta>
            <ta e="T299" id="Seg_6853" s="T297">foc.int</ta>
            <ta e="T303" id="Seg_6854" s="T299">foc.wid</ta>
            <ta e="T304" id="Seg_6855" s="T303">foc.nar</ta>
            <ta e="T309" id="Seg_6856" s="T308">foc.int</ta>
            <ta e="T316" id="Seg_6857" s="T311">foc.nar</ta>
            <ta e="T320" id="Seg_6858" s="T317">foc.wid</ta>
            <ta e="T325" id="Seg_6859" s="T324">foc.nar</ta>
            <ta e="T331" id="Seg_6860" s="T330">foc.nar</ta>
            <ta e="T343" id="Seg_6861" s="T339">foc.int</ta>
            <ta e="T351" id="Seg_6862" s="T349">foc.nar</ta>
            <ta e="T356" id="Seg_6863" s="T354">foc.int</ta>
            <ta e="T362" id="Seg_6864" s="T358">foc.wid</ta>
            <ta e="T364" id="Seg_6865" s="T362">foc.int</ta>
            <ta e="T377" id="Seg_6866" s="T375">foc.nar</ta>
            <ta e="T380" id="Seg_6867" s="T379">foc.int</ta>
            <ta e="T383" id="Seg_6868" s="T380">foc.int</ta>
            <ta e="T386" id="Seg_6869" s="T385">foc.int</ta>
            <ta e="T397" id="Seg_6870" s="T394">foc.int</ta>
            <ta e="T401" id="Seg_6871" s="T399">foc.int</ta>
            <ta e="T405" id="Seg_6872" s="T402">foc.nar</ta>
            <ta e="T411" id="Seg_6873" s="T406">foc.wid</ta>
            <ta e="T417" id="Seg_6874" s="T414">foc.int</ta>
            <ta e="T419" id="Seg_6875" s="T417">foc.wid</ta>
            <ta e="T420" id="Seg_6876" s="T419">foc.int</ta>
            <ta e="T422" id="Seg_6877" s="T420">foc.wid</ta>
            <ta e="T425" id="Seg_6878" s="T424">foc.int</ta>
            <ta e="T433" id="Seg_6879" s="T425">foc.int</ta>
            <ta e="T436" id="Seg_6880" s="T433">foc.int</ta>
            <ta e="T439" id="Seg_6881" s="T437">foc.int</ta>
            <ta e="T449" id="Seg_6882" s="T445">foc.int</ta>
            <ta e="T453" id="Seg_6883" s="T451">foc.int</ta>
            <ta e="T462" id="Seg_6884" s="T459">foc.wid</ta>
            <ta e="T464" id="Seg_6885" s="T463">foc.nar</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_6886" s="T2">EV:cult</ta>
            <ta e="T10" id="Seg_6887" s="T9">RUS:cult</ta>
            <ta e="T24" id="Seg_6888" s="T23">RUS:core</ta>
            <ta e="T58" id="Seg_6889" s="T57">RUS:core</ta>
            <ta e="T74" id="Seg_6890" s="T73">EV:cult</ta>
            <ta e="T83" id="Seg_6891" s="T82">EV:cult</ta>
            <ta e="T89" id="Seg_6892" s="T88">EV:cult</ta>
            <ta e="T142" id="Seg_6893" s="T141">EV:cult</ta>
            <ta e="T204" id="Seg_6894" s="T203">RUS:mod</ta>
            <ta e="T210" id="Seg_6895" s="T209">RUS:core</ta>
            <ta e="T277" id="Seg_6896" s="T276">RUS:gram</ta>
            <ta e="T302" id="Seg_6897" s="T301">EV:core</ta>
            <ta e="T319" id="Seg_6898" s="T318">EV:core</ta>
            <ta e="T346" id="Seg_6899" s="T345">RUS:cult</ta>
            <ta e="T361" id="Seg_6900" s="T360">RUS:cult</ta>
            <ta e="T384" id="Seg_6901" s="T383">RUS:mod</ta>
            <ta e="T396" id="Seg_6902" s="T395">RUS:cult</ta>
            <ta e="T414" id="Seg_6903" s="T413">EV:core</ta>
            <ta e="T459" id="Seg_6904" s="T458">RUS:gram</ta>
            <ta e="T460" id="Seg_6905" s="T459">EV:cult</ta>
            <ta e="T463" id="Seg_6906" s="T462">EV:cult</ta>
            <ta e="T464" id="Seg_6907" s="T463">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T3" id="Seg_6908" s="T2">Vsub</ta>
            <ta e="T10" id="Seg_6909" s="T9">fortition Vsub fortition Vsub</ta>
            <ta e="T24" id="Seg_6910" s="T23">fortition Vsub</ta>
            <ta e="T58" id="Seg_6911" s="T57">fortition Vsub</ta>
            <ta e="T74" id="Seg_6912" s="T73">Vsub</ta>
            <ta e="T83" id="Seg_6913" s="T82">Vsub</ta>
            <ta e="T89" id="Seg_6914" s="T88">Vsub</ta>
            <ta e="T142" id="Seg_6915" s="T141">Vsub</ta>
            <ta e="T210" id="Seg_6916" s="T209">Vsub Vsub</ta>
            <ta e="T346" id="Seg_6917" s="T345">fortition medVins Vsub Vsub</ta>
            <ta e="T361" id="Seg_6918" s="T360">Csub Vsub Vsub</ta>
            <ta e="T396" id="Seg_6919" s="T395">Csub Vsub</ta>
            <ta e="T459" id="Seg_6920" s="T458">Vsub</ta>
            <ta e="T460" id="Seg_6921" s="T459">Vsub</ta>
            <ta e="T463" id="Seg_6922" s="T462">Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T3" id="Seg_6923" s="T2">dir:bare</ta>
            <ta e="T10" id="Seg_6924" s="T9">dir:bare</ta>
            <ta e="T24" id="Seg_6925" s="T23">dir:bare</ta>
            <ta e="T58" id="Seg_6926" s="T57">dir:bare</ta>
            <ta e="T74" id="Seg_6927" s="T73">dir:bare</ta>
            <ta e="T83" id="Seg_6928" s="T82">dir:infl</ta>
            <ta e="T89" id="Seg_6929" s="T88">dir:infl</ta>
            <ta e="T142" id="Seg_6930" s="T141">dir:bare</ta>
            <ta e="T204" id="Seg_6931" s="T203">dir:bare</ta>
            <ta e="T210" id="Seg_6932" s="T209">dir:infl</ta>
            <ta e="T277" id="Seg_6933" s="T276">dir:bare</ta>
            <ta e="T302" id="Seg_6934" s="T301">dir:infl</ta>
            <ta e="T319" id="Seg_6935" s="T318">dir:infl</ta>
            <ta e="T346" id="Seg_6936" s="T345">dir:infl</ta>
            <ta e="T361" id="Seg_6937" s="T360">dir:infl</ta>
            <ta e="T384" id="Seg_6938" s="T383">dir:bare</ta>
            <ta e="T396" id="Seg_6939" s="T395">dir:infl</ta>
            <ta e="T414" id="Seg_6940" s="T413">dir:infl</ta>
            <ta e="T459" id="Seg_6941" s="T458">dir:bare</ta>
            <ta e="T460" id="Seg_6942" s="T459">dir:bare</ta>
            <ta e="T463" id="Seg_6943" s="T462">dir:bare</ta>
            <ta e="T464" id="Seg_6944" s="T463">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_6945" s="T0">Long ago there lived an old Nganasan man, he had a daughter.</ta>
            <ta e="T14" id="Seg_6946" s="T6">This daughter died after having reached the nubile age.</ta>
            <ta e="T18" id="Seg_6947" s="T14">Her father cried heavily.</ta>
            <ta e="T21" id="Seg_6948" s="T18">As they wanted to bury her, he said:</ta>
            <ta e="T34" id="Seg_6949" s="T21">"I will always take my daughter with me, she will revive, I will make the big shaman revive her", he says.</ta>
            <ta e="T39" id="Seg_6950" s="T34">He loads his daughter on a sledge and drives off.</ta>
            <ta e="T46" id="Seg_6951" s="T39">He drives and begs the shamans, but no one of them [agrees].</ta>
            <ta e="T55" id="Seg_6952" s="T46">Only after two years he made two big shamans shamanize.</ta>
            <ta e="T62" id="Seg_6953" s="T55">The girl's body had rotten long ago, just the bones remained.</ta>
            <ta e="T69" id="Seg_6954" s="T62">These two shamans shamanize, they can't do anything.</ta>
            <ta e="T72" id="Seg_6955" s="T69">The third year came.</ta>
            <ta e="T78" id="Seg_6956" s="T72">There was a clairvoyant in the neighbourhood of that Nganasan.</ta>
            <ta e="T83" id="Seg_6957" s="T78">This clairvoyant felt very sorry for the Nganasan.</ta>
            <ta e="T85" id="Seg_6958" s="T83">Once he dreams.</ta>
            <ta e="T91" id="Seg_6959" s="T85">After that dream he comes and tells the Nganasan:</ta>
            <ta e="T99" id="Seg_6960" s="T91">"Your daughter will, apparently, revive, that is the miracle.</ta>
            <ta e="T109" id="Seg_6961" s="T99">The spirit, that ate your child's soul, carries it to the sea, into the water of death.</ta>
            <ta e="T133" id="Seg_6962" s="T109">One of your two shamans, having turned into a wild reindeer, is following her on earth, its fur is covered with ice, that he does not fit through the hole, the other one, after having become a wild reindeer, as he is swimming in the water and almost catches [the soul], the waves take it away.</ta>
            <ta e="T140" id="Seg_6963" s="T133">They will follow it for nine years, what then will be [is unknown]."</ta>
            <ta e="T144" id="Seg_6964" s="T140">On that the Nganasan old man says:</ta>
            <ta e="T155" id="Seg_6965" s="T144">"If you know that the shamans cannot handle it, why don't you try to revive her?", he begs.</ta>
            <ta e="T159" id="Seg_6966" s="T155">"You are suffering a lot", he says.</ta>
            <ta e="T166" id="Seg_6967" s="T159">"What shall I do, if she revives, will you marry her to me?"</ta>
            <ta e="T174" id="Seg_6968" s="T166">"I will give you the half of my wealth, I will give herself to you, just revive her", he says.</ta>
            <ta e="T177" id="Seg_6969" s="T174">The clairvoyant falls asleep.</ta>
            <ta e="T185" id="Seg_6970" s="T177">While he is sleeping, he comes into the land of the death.</ta>
            <ta e="T190" id="Seg_6971" s="T185">Well, he comes into the land of the death.</ta>
            <ta e="T196" id="Seg_6972" s="T190">As he came there, very many people were dancing there.</ta>
            <ta e="T205" id="Seg_6973" s="T196">He searches for the girl's soul in vain, but he does not find it.</ta>
            <ta e="T212" id="Seg_6974" s="T205">"I should go from here to another place", he thought.</ta>
            <ta e="T215" id="Seg_6975" s="T212">He comes to another place.</ta>
            <ta e="T224" id="Seg_6976" s="T215">There were, apparently, three girls with identical faces and clothes.</ta>
            <ta e="T228" id="Seg_6977" s="T224">One of them was the old man's daughter.</ta>
            <ta e="T233" id="Seg_6978" s="T228">Which one of them is his daughter, he can not discern.</ta>
            <ta e="T240" id="Seg_6979" s="T233">He tries to count the seams on their clothes, the amount is completely the same.</ta>
            <ta e="T242" id="Seg_6980" s="T240">He struggles in vain.</ta>
            <ta e="T248" id="Seg_6981" s="T242">The girls have got three houses, they stand apart from one another.</ta>
            <ta e="T254" id="Seg_6982" s="T248">In the evening they come from dancing there and fall asleep.</ta>
            <ta e="T261" id="Seg_6983" s="T254">He wants to talk with the girls, but none of them speaks.</ta>
            <ta e="T265" id="Seg_6984" s="T261">"What will be with me", he thinks.</ta>
            <ta e="T271" id="Seg_6985" s="T265">He goes into the house of one of them, hides and observes how she sleeps.</ta>
            <ta e="T281" id="Seg_6986" s="T271">So he stayed in all three house, but did not find anything.</ta>
            <ta e="T288" id="Seg_6987" s="T281">Once, all three of them gathered in one house.</ta>
            <ta e="T293" id="Seg_6988" s="T288">The clairvoyant went in and hid himself.</ta>
            <ta e="T299" id="Seg_6989" s="T293">As she was lying there, one girl is thinking.</ta>
            <ta e="T303" id="Seg_6990" s="T299">There her friends asked:</ta>
            <ta e="T307" id="Seg_6991" s="T303">"What is it that you think about?", they said.</ta>
            <ta e="T309" id="Seg_6992" s="T307">The girl said:</ta>
            <ta e="T317" id="Seg_6993" s="T309">"My mother sent my without giving me my dearest thing."</ta>
            <ta e="T320" id="Seg_6994" s="T317">Then her friends said:</ta>
            <ta e="T329" id="Seg_6995" s="T320">"So what, when the mother comes, she herself will then give it [to you]."</ta>
            <ta e="T337" id="Seg_6996" s="T329">"Well, that is apparently the old man's daughter", he thinks.</ta>
            <ta e="T344" id="Seg_6997" s="T337">After she fell asleep, he embraces her and flies away.</ta>
            <ta e="T351" id="Seg_6998" s="T344">All this time, nine days long, the clairvoyant slept.</ta>
            <ta e="T358" id="Seg_6999" s="T351">After having embraced her, he wakes up and jumps up.</ta>
            <ta e="T362" id="Seg_7000" s="T358">There they run and bring the [shaman's] assistant.</ta>
            <ta e="T374" id="Seg_7001" s="T362">"Bring her bones", he says, shamanizes three days and nights and comes back from the land of the death.</ta>
            <ta e="T379" id="Seg_7002" s="T374">There they bring her bones on a reindeer fur.</ta>
            <ta e="T391" id="Seg_7003" s="T379">They saw that raises the girl, but the girl is not moving as if she was dead or has fallen asleep.</ta>
            <ta e="T398" id="Seg_7004" s="T391">He puts the girl's bones inside, like in a coat.</ta>
            <ta e="T406" id="Seg_7005" s="T398">She revives and it is the old man's daughter.</ta>
            <ta e="T411" id="Seg_7006" s="T406">The clairvoyant marries her and lives [with her].</ta>
            <ta e="T419" id="Seg_7007" s="T411">Her two remaining friends wanted to invite her for a dance, but nobody was there.</ta>
            <ta e="T422" id="Seg_7008" s="T419">They got to know that a shaman had taken her.</ta>
            <ta e="T425" id="Seg_7009" s="T422">There they got angry.</ta>
            <ta e="T433" id="Seg_7010" s="T425">They turn into dead souls, find the revived one, take her thoughts and make a female shaman out of her.</ta>
            <ta e="T436" id="Seg_7011" s="T433">They force her to be a female shaman:</ta>
            <ta e="T444" id="Seg_7012" s="T436">"We will eat you, if you do not skin an old woman and make a shaman's drum out of the skin."</ta>
            <ta e="T453" id="Seg_7013" s="T444">There they skin an old woman and make a shaman's drum out of her skin.</ta>
            <ta e="T462" id="Seg_7014" s="T453">Because of this sin, as soon as somebody is sick, Nganasans do not die alone.</ta>
            <ta e="T465" id="Seg_7015" s="T462">The Nganasans die in big groups.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_7016" s="T0">Vor langer Zeit lebte ein nganasanischer alter Mann, er hatte eine Tochter.</ta>
            <ta e="T14" id="Seg_7017" s="T6">Diese Tochter starb, als sie ins heiratsfähige Alter gekommen war.</ta>
            <ta e="T18" id="Seg_7018" s="T14">Da weinte der Vater sehr.</ta>
            <ta e="T21" id="Seg_7019" s="T18">Als man sie begraben wollte, sagte er:</ta>
            <ta e="T34" id="Seg_7020" s="T21">"Ich nehme meine Tochter immer mit, sie wird wieder zum Leben erwachen, ich lasse den großen Schamanen sie wieder zum Leben erwecken", sagt er.</ta>
            <ta e="T39" id="Seg_7021" s="T34">Da lädt er seine Tochter auf einen Schlitten und fährt los.</ta>
            <ta e="T46" id="Seg_7022" s="T39">Er fährt und bittet die Schamanen, aber keiner von ihnen ist [einverstanden].</ta>
            <ta e="T55" id="Seg_7023" s="T46">Erst als zwei Jahre vergangen waren, ließ er zwei große Schamanen schamanisieren.</ta>
            <ta e="T62" id="Seg_7024" s="T55">Der Körper des Mädchen war schon lange verfault, nur die Knochen waren geblieben.</ta>
            <ta e="T69" id="Seg_7025" s="T62">Diese beiden Schamanen schamanisieren, sie können nichts machen.</ta>
            <ta e="T72" id="Seg_7026" s="T69">Es kam das dritte Jahr.</ta>
            <ta e="T78" id="Seg_7027" s="T72">In der Nähe dieses Nganasanen gab es einen Hellseher.</ta>
            <ta e="T83" id="Seg_7028" s="T78">Dieser Hellseher bemitleidete den Nganasanen sehr.</ta>
            <ta e="T85" id="Seg_7029" s="T83">Einmal träumt er.</ta>
            <ta e="T91" id="Seg_7030" s="T85">Nachdem er geträumt hat, kommt er und erzählt dem Nganasanen:</ta>
            <ta e="T99" id="Seg_7031" s="T91">"Deine Tochter wird wohl wieder auferstehen, das ist das Wunder.</ta>
            <ta e="T109" id="Seg_7032" s="T99">Der Geist, der die Seele deiner Tochter gefressen hat, trägt sie wohl ins Meer, ins Wasser des Todes.</ta>
            <ta e="T133" id="Seg_7033" s="T109">Einer deiner beiden Schamanen, nachdem er sich in ein wildes Rentier verwandelt hat, folgt ihr der Erde entlang, sein Haar ist mit einer Eisschicht bedeckt, er passt nicht durch das Loch, dem anderen der beiden, nachdem er sich in ein wildes Rentier verwandelt hat, während er durchs Wasser hinschwimmt und er gerade [die Seele] fangen möchte, trägt eine Welle die Seele davon.</ta>
            <ta e="T140" id="Seg_7034" s="T133">Neun Jahre lang werden sie sie verfolgen, wie es dann sein wird [weiß man nicht]."</ta>
            <ta e="T144" id="Seg_7035" s="T140">Darauf sagt der nganasanische alte Mann:</ta>
            <ta e="T155" id="Seg_7036" s="T144">"Wenn du weißt, dass der Schamane es nicht kann, warum versuchst du nicht selber, sie zum Leben zu erwecken?", bittet er.</ta>
            <ta e="T159" id="Seg_7037" s="T155">"Du leidest wohl ziemlich", sagt er.</ta>
            <ta e="T166" id="Seg_7038" s="T159">"Was soll ich tun, wenn sie wieder aufersteht, gibst du sie mir zur Frau?"</ta>
            <ta e="T174" id="Seg_7039" s="T166">"Ich gebe dir die Hälfte meines Reichtums, ich gebe dir sie selber, mach sie nur wieder lebendig", sagt er.</ta>
            <ta e="T177" id="Seg_7040" s="T174">Der Hellseher schläft ein.</ta>
            <ta e="T185" id="Seg_7041" s="T177">Während er schläft, kommt er in das Land des Todes.</ta>
            <ta e="T190" id="Seg_7042" s="T185">Er kam in das Land des Todes.</ta>
            <ta e="T196" id="Seg_7043" s="T190">Er kam dorthin und dort tanzten sehr viele Leute.</ta>
            <ta e="T205" id="Seg_7044" s="T196">Dort sucht er die Seele des Mädchens vergeblich, aber er findet nichts.</ta>
            <ta e="T212" id="Seg_7045" s="T205">"Ich muss von diesem Ort an einen anderen gehen", dachte er.</ta>
            <ta e="T215" id="Seg_7046" s="T212">Er kommt an einen [anderen] Ort.</ta>
            <ta e="T224" id="Seg_7047" s="T215">Dort waren offenbar drei Mädchen mit ganz gleichem Gesicht, mit ganz gleicher Kleidung.</ta>
            <ta e="T228" id="Seg_7048" s="T224">Eine von ihnen war die Tochter des alten Mannes.</ta>
            <ta e="T233" id="Seg_7049" s="T228">Welche die Tochter ist, kann er nicht erkennen.</ta>
            <ta e="T240" id="Seg_7050" s="T233">Er versucht die Nähte ihrer Kleidung zu zählen, die Anzahl ist gleich.</ta>
            <ta e="T242" id="Seg_7051" s="T240">Er quält sich vergeblich.</ta>
            <ta e="T248" id="Seg_7052" s="T242">Die Mächen haben drei Häuser, sie stehen alle für sich.</ta>
            <ta e="T254" id="Seg_7053" s="T248">Am Abend kommen sie vom Tanz dorthin und schlafen ein.</ta>
            <ta e="T261" id="Seg_7054" s="T254">Er möchte mit den Mädchen sprechen, aber keine sagt etwas.</ta>
            <ta e="T265" id="Seg_7055" s="T261">"Was wird mit mir sein", denkt er nach.</ta>
            <ta e="T271" id="Seg_7056" s="T265">Er geht in das Haus von einer hinein, versteckt sich und beobachtet, wie sie schläft.</ta>
            <ta e="T281" id="Seg_7057" s="T271">So übernachtete er in allen drei Häuser, aber er fand nichts heraus.</ta>
            <ta e="T288" id="Seg_7058" s="T281">Einmal geschah es, dass sich alle drei in einem Haus versammelten.</ta>
            <ta e="T293" id="Seg_7059" s="T288">Der Hellseher ging hinein und versteckte sich. </ta>
            <ta e="T299" id="Seg_7060" s="T293">Als sie so da lag, dachte ein Mädchen nach.</ta>
            <ta e="T303" id="Seg_7061" s="T299">Da fragten die beiden Freundinnen:</ta>
            <ta e="T307" id="Seg_7062" s="T303">"Was ist, dass du grübelst?", sagten sie.</ta>
            <ta e="T309" id="Seg_7063" s="T307">Das Mädchen sagte:</ta>
            <ta e="T317" id="Seg_7064" s="T309">"Meine Mutter hat mich geschickt, ohne mir meine sehnlichste Sache zu geben."</ta>
            <ta e="T320" id="Seg_7065" s="T317">Da sagten die Freundinnen:</ta>
            <ta e="T329" id="Seg_7066" s="T320">"Na und, wenn die Mutter kommt, dann wird sie es eben selbst bringen und dir geben."</ta>
            <ta e="T337" id="Seg_7067" s="T329">"Nun, das ist wohl die Tochter des alten Mannes", denkt er.</ta>
            <ta e="T344" id="Seg_7068" s="T337">Nachdem sie eingeschlafen ist, umarmt er sie und fliegt los.</ta>
            <ta e="T351" id="Seg_7069" s="T344">Und während dieser ganzen Zeit, neun Tage lang, schlief der Hellseher.</ta>
            <ta e="T358" id="Seg_7070" s="T351">Nachdem er sie umarmt hat, wacht er auf und springt auf.</ta>
            <ta e="T362" id="Seg_7071" s="T358">Da laufen sie und brachten den Helfer [des Schamanen].</ta>
            <ta e="T374" id="Seg_7072" s="T362">"Bringt ihre Knochen", sagte er, schamanisiert drei Tage und Nächte und kehrt aus dem Land des Todes zurück.</ta>
            <ta e="T379" id="Seg_7073" s="T374">Da bringen sie auf einem Rentierfell ihre Knochen.</ta>
            <ta e="T391" id="Seg_7074" s="T379">Sie sahen, er hebt das Mädchen hoch, nur bewegt sich das Mädchen nicht, als sei sie tot oder würde schlafen.</ta>
            <ta e="T398" id="Seg_7075" s="T391">Die Knochen dieses Mädchens legt er hinein, wie in einen Mantel.</ta>
            <ta e="T406" id="Seg_7076" s="T398">Jene wird lebendig und es ist die Tochter des alten Mannes.</ta>
            <ta e="T411" id="Seg_7077" s="T406">Der Hellseher nahm sie zur Frau und lebte [mit ihr].</ta>
            <ta e="T419" id="Seg_7078" s="T411">Die zwei zurückgebliebenen Freundinnen wollten sie zum Tanz einladen, aber kein Mensch war da.</ta>
            <ta e="T422" id="Seg_7079" s="T419">Sie erfuhren, dass ein Schamane sie mitgenommen hatte.</ta>
            <ta e="T425" id="Seg_7080" s="T422">Da wurden sie böse.</ta>
            <ta e="T433" id="Seg_7081" s="T425">Sie verwandelten sich in tote Seelen, fanden die Auferstandene, bemächtigten sie ihres Verstandes und machten sie zur Schamanin.</ta>
            <ta e="T436" id="Seg_7082" s="T433">Sie zwingen sie Schamanin zu werden:</ta>
            <ta e="T444" id="Seg_7083" s="T436">"Wir essen dich, wenn du einer alten Frau nicht die Haut abziehst und daraus eine Schamanentrommel machst."</ta>
            <ta e="T453" id="Seg_7084" s="T444">Da ziehen sie einer alten Frau die Haut ab und machen daraus eine Schamanentrommel.</ta>
            <ta e="T462" id="Seg_7085" s="T453">Wegen dieser Sünde, sobald jemand krank ist, sterben die Nganasanen nicht allein.</ta>
            <ta e="T465" id="Seg_7086" s="T462">Die Nganasanen sterben in ganzen Gruppen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_7087" s="T0">В старину жил один старик-нганасанин. Была у него дочь.</ta>
            <ta e="T14" id="Seg_7088" s="T6">Эта дочь, достигнув возраста невесты, умерла.</ta>
            <ta e="T18" id="Seg_7089" s="T14">Тут отец ее сильно загоревал.</ta>
            <ta e="T21" id="Seg_7090" s="T18">Когда захотели похоронить, сказал:</ta>
            <ta e="T34" id="Seg_7091" s="T21">"Я свою дочь век буду возить с собой, она воскреснет, я попрошу большого шамана оживить ее", говорит.</ta>
            <ta e="T39" id="Seg_7092" s="T34">Вот дочку возит на нарте.</ta>
            <ta e="T46" id="Seg_7093" s="T39">Ездит и просит шаманов, но ни один не [соглашается].</ta>
            <ta e="T55" id="Seg_7094" s="T46">Только когда прошло два года, упросил камлать двух больших шаманов.</ta>
            <ta e="T62" id="Seg_7095" s="T55">Труп девушки давно сгнил, остались одни кости.</ta>
            <ta e="T69" id="Seg_7096" s="T62">Эти два шамана покамлали, но ничего не смогли сделать.</ta>
            <ta e="T72" id="Seg_7097" s="T69">Третий год настает.</ta>
            <ta e="T78" id="Seg_7098" s="T72">Недалеко от этого нганасанина кочевал провидец.</ta>
            <ta e="T83" id="Seg_7099" s="T78">Этот провидец очень жалел соседа нганасанина.</ta>
            <ta e="T85" id="Seg_7100" s="T83">Однажды он видит [вещий] сон.</ta>
            <ta e="T91" id="Seg_7101" s="T85">Увидев такой сон, приходит рассказать нганасанину:</ta>
            <ta e="T99" id="Seg_7102" s="T91">"Твоя дочь, видимо, воскреснет, а удивительное вот в чем.</ta>
            <ta e="T109" id="Seg_7103" s="T99">Дух, похитивший душу твоей дочери, держит ее, оказывается, в море, в воде смерти.</ta>
            <ta e="T133" id="Seg_7104" s="T109">Один из твоих шаманов, став диким оленем, идет по его следу по земле, но шерсть его покрывается слоем наледи и он не может пролезть в отверстие [между мирами], а когда другой, обернувшись диким оленем, подплывает по воде и готов подхватить [душу], волна относит от него ее душу.</ta>
            <ta e="T140" id="Seg_7105" s="T133">Девять лет так они будут преследовать, а что потом случится — неизвестно."</ta>
            <ta e="T144" id="Seg_7106" s="T140">На это старик-нганасанин говорит:</ta>
            <ta e="T155" id="Seg_7107" s="T144">"Если знаешь, что шаману это не под силу, почему сам не попытаешься оживить ее?", умоляет.</ta>
            <ta e="T159" id="Seg_7108" s="T155">"Да, сильно ты страдаешь", говорит.</ta>
            <ta e="T166" id="Seg_7109" s="T159">"Что же мне делать, ну, если она воскреснет, дашь мне ее в жены?"</ta>
            <ta e="T174" id="Seg_7110" s="T166">"Половину своего богатства дам, ее саму отдам, только оживи", говорит.</ta>
            <ta e="T177" id="Seg_7111" s="T174">Провидец углубился в сон.</ta>
            <ta e="T185" id="Seg_7112" s="T177">Вот во сне приходит в ту страну смерти.</ta>
            <ta e="T190" id="Seg_7113" s="T185">Приходит в страну смерти.</ta>
            <ta e="T196" id="Seg_7114" s="T190">Пришел туда, много людей танцевали.</ta>
            <ta e="T205" id="Seg_7115" s="T196">Пытался в этом месте искать душу девушки, но ничего не было, не нашел.</ta>
            <ta e="T212" id="Seg_7116" s="T205">"Из этого места в другое пойду", — думает.</ta>
            <ta e="T215" id="Seg_7117" s="T212">Приходит в другое место.</ta>
            <ta e="T224" id="Seg_7118" s="T215">А там, оказывается, есть три девушки совершенно одинаковые: и по лицу, и по одежде.</ta>
            <ta e="T228" id="Seg_7119" s="T224">Одна из них и была дочерью старика.</ta>
            <ta e="T233" id="Seg_7120" s="T228">Кто из них та девушка, не может угадать.</ta>
            <ta e="T240" id="Seg_7121" s="T233">Считает, сколько швов на их одежде — счет одинаковый.</ta>
            <ta e="T242" id="Seg_7122" s="T240">Измучается совсем.</ta>
            <ta e="T248" id="Seg_7123" s="T242">У этих девушек три жилища, они стоят отдельно друг от друга.</ta>
            <ta e="T254" id="Seg_7124" s="T248">Вечером после танцев приходят туда и засыпают.</ta>
            <ta e="T261" id="Seg_7125" s="T254">Хочет поговорить с девушками, но ни одна не отвечает.</ta>
            <ta e="T265" id="Seg_7126" s="T261">"Что будет со мной", думает.</ta>
            <ta e="T271" id="Seg_7127" s="T265">Заходит к одной в дом и тайком наблюдает, как спит.</ta>
            <ta e="T281" id="Seg_7128" s="T271">Так побывал во всех трех домах, но ничего не выяснил.</ta>
            <ta e="T288" id="Seg_7129" s="T281">Вот однажды случилось так, что собрались они втроем в одном доме.</ta>
            <ta e="T293" id="Seg_7130" s="T288">Провидец вошел и спрятался.</ta>
            <ta e="T299" id="Seg_7131" s="T293">Когда так лежала, одна из девушек загрустила.</ta>
            <ta e="T303" id="Seg_7132" s="T299">Тогда обе подруги спрашивают:</ta>
            <ta e="T307" id="Seg_7133" s="T303">— О чем ты печалишься? — сказали.</ta>
            <ta e="T309" id="Seg_7134" s="T307">Девушка сказал:</ta>
            <ta e="T317" id="Seg_7135" s="T309">— Моя мать отправила меня, не отдав самую заветную вещь.</ta>
            <ta e="T320" id="Seg_7136" s="T317">Тут подруги сказали:</ta>
            <ta e="T329" id="Seg_7137" s="T320">— Ну и что, когда мать придет, сама принесет и отдаст.</ta>
            <ta e="T337" id="Seg_7138" s="T329">— Ведь эта, оказывается, дочь старика, — думает.</ta>
            <ta e="T344" id="Seg_7139" s="T337">Как уснула, обнимает ее и полетает.</ta>
            <ta e="T351" id="Seg_7140" s="T344">И все это время, все девять суток, спал [провидец].</ta>
            <ta e="T358" id="Seg_7141" s="T351">Обняв ее, просыпается и вскакивает.</ta>
            <ta e="T362" id="Seg_7142" s="T358">Тут бегут и приводят помощника [шамана].</ta>
            <ta e="T374" id="Seg_7143" s="T362">"Принесите ее кости", так говоря, камлает неистово три дня и три ночи подряд, возвращаясь из страны смерти.</ta>
            <ta e="T379" id="Seg_7144" s="T374">Тут приносят на оленьей шкуре ее кости.</ta>
            <ta e="T391" id="Seg_7145" s="T379">Увидели, что держит на руках девушку, только девушка не движется, будто мертвая или спящая.</ta>
            <ta e="T398" id="Seg_7146" s="T391">Кости той девушки вкладывает вовнутрь, словно в малицу.</ta>
            <ta e="T406" id="Seg_7147" s="T398">Та оживает и это дочь старика.</ta>
            <ta e="T411" id="Seg_7148" s="T406">Провидец, взяв ее в жены, живет [вместе с ней].</ta>
            <ta e="T419" id="Seg_7149" s="T411">Две оставшиеся подруги хотели было позвать ее на пляску — а никого нет.</ta>
            <ta e="T422" id="Seg_7150" s="T419">Узнали, что увел шаман.</ta>
            <ta e="T425" id="Seg_7151" s="T422">Тогда они разгневались.</ta>
            <ta e="T433" id="Seg_7152" s="T425">Став блуждающими духами, находят ожившую, завладеют ее разумом, делают из нее шаманка.</ta>
            <ta e="T436" id="Seg_7153" s="T433">Принуждают стать шаманкой:</ta>
            <ta e="T444" id="Seg_7154" s="T436">— Мы тебя съедим, если не сдерешь кожу со старухи и не обтянешь ею свой бубен.</ta>
            <ta e="T453" id="Seg_7155" s="T444">Вот тогда сдирают кожу с одной старухи и ею делают бубен.</ta>
            <ta e="T462" id="Seg_7156" s="T453">Из-за этого греха, как только начнется большая болезнь, нганасаны не поодиночке умирают.</ta>
            <ta e="T465" id="Seg_7157" s="T462">Нганасаны целыми родами пропадают.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_7158" s="T0">В старину жил один старик-нганасанин. Была у него дочь.</ta>
            <ta e="T14" id="Seg_7159" s="T6">Эта дочь, достигнув возраста невесты, умерла.</ta>
            <ta e="T18" id="Seg_7160" s="T14">Тут отец ее сильно загоревал.</ta>
            <ta e="T21" id="Seg_7161" s="T18">Когда захотели похоронить, сказал:</ta>
            <ta e="T34" id="Seg_7162" s="T21">— Я свою дочь век буду возить с собой, она воскреснет, я попрошу большого шамана оживить ее.</ta>
            <ta e="T39" id="Seg_7163" s="T34">Вот дочку возит на нарте.</ta>
            <ta e="T46" id="Seg_7164" s="T39">Ездит и просит шаманов покамлать, но ни один не соглашается.</ta>
            <ta e="T55" id="Seg_7165" s="T46">Только когда прошло два года, упросил камлать двух больших шаманов.</ta>
            <ta e="T62" id="Seg_7166" s="T55">Труп девушки давно сгнил, остались одни кости.</ta>
            <ta e="T69" id="Seg_7167" s="T62">Эти дна шамана покамлали, но ничего не смогли сделать.</ta>
            <ta e="T72" id="Seg_7168" s="T69">Третий год настает.</ta>
            <ta e="T78" id="Seg_7169" s="T72">Недалеко от этого нганасанина кочевал провидец.</ta>
            <ta e="T83" id="Seg_7170" s="T78">Этот провидец очень жалел соседа нганасанина.</ta>
            <ta e="T85" id="Seg_7171" s="T83">Однажды он увидел [вещий] сон.</ta>
            <ta e="T91" id="Seg_7172" s="T85">Увидев такой сон, пошел рассказать нганасанину:</ta>
            <ta e="T99" id="Seg_7173" s="T91">— Твоя дочь, видимо, воскреснет, а удивительное вот в чем.</ta>
            <ta e="T109" id="Seg_7174" s="T99">Дух, похитивший душу твоей дочери, держит ее, оказывается, в море, в воде смерти.</ta>
            <ta e="T133" id="Seg_7175" s="T109">Один из твоих шаманов, став диким оленем, идет по его следу по земле, но шерсть его покрывается слоем наледи и он не может пролезть в отверстие [между мирами], а когда другой, обернувшись диким оленем, подплывает по воде и готов подхватить [душу], волна относит от него ее душу.</ta>
            <ta e="T140" id="Seg_7176" s="T133">Девять лет так они будут преследовать, а что потом случится — неизвестно.</ta>
            <ta e="T144" id="Seg_7177" s="T140">На это старик-нганасанин говорит:</ta>
            <ta e="T155" id="Seg_7178" s="T144">— Если знаешь, что шаману это не под силу, почему сам не попытаешься оживить ее? — умоляет.</ta>
            <ta e="T159" id="Seg_7179" s="T155">— Да, сильно ты страдаешь, — говорит.</ta>
            <ta e="T166" id="Seg_7180" s="T159">— Что же мне делать, ну, если она воскреснет, дашь мне ее в жены?</ta>
            <ta e="T174" id="Seg_7181" s="T166">— Половину своего богатства дам, ее саму отдам, только оживи.</ta>
            <ta e="T177" id="Seg_7182" s="T174">Провидец углубился в сон.</ta>
            <ta e="T185" id="Seg_7183" s="T177">Вот во сне приходит в ту страну смерти.</ta>
            <ta e="T190" id="Seg_7184" s="T185">Приходит, </ta>
            <ta e="T196" id="Seg_7185" s="T190">а там множество людей танцует.</ta>
            <ta e="T205" id="Seg_7186" s="T196">Пытался в этом месте искать душу девушки, но ничего не было, не нашел.</ta>
            <ta e="T212" id="Seg_7187" s="T205">"Из этого места в другое пойду", — думает.</ta>
            <ta e="T215" id="Seg_7188" s="T212">Приходит в другое место.</ta>
            <ta e="T224" id="Seg_7189" s="T215">А там, оказывается, есть три девушки совершенно одинаковые: и по лицу, и по одежде.</ta>
            <ta e="T228" id="Seg_7190" s="T224">Одна из них и была дочерью старика.</ta>
            <ta e="T233" id="Seg_7191" s="T228">Кто из них та девушка, не может угадать.</ta>
            <ta e="T240" id="Seg_7192" s="T233">Считает, сколько швов на их одежде — счет одинаковый.</ta>
            <ta e="T242" id="Seg_7193" s="T240">Измучился совсем.</ta>
            <ta e="T248" id="Seg_7194" s="T242">У этих девушек три жилища, они стоят отдельно друг от друга.</ta>
            <ta e="T254" id="Seg_7195" s="T248">Вечером после танцев каждая приходит в свой дом и засыпает.</ta>
            <ta e="T261" id="Seg_7196" s="T254">Хочет поговорить с девушками, но ни одна не отвечает.</ta>
            <ta e="T265" id="Seg_7197" s="T261">Думает, как же быть.</ta>
            <ta e="T271" id="Seg_7198" s="T265">Заходит к одной в дом и тайком наблюдает, как спит.</ta>
            <ta e="T281" id="Seg_7199" s="T271">Так побывал во всех трех домах, но ничего не выяснил.</ta>
            <ta e="T288" id="Seg_7200" s="T281">Вот однажды случилось так, что собрались они втроем в одном доме.</ta>
            <ta e="T293" id="Seg_7201" s="T288">Провидец, спрятавшись, там лежит.</ta>
            <ta e="T299" id="Seg_7202" s="T293">Вот одна из девушек загрустила.</ta>
            <ta e="T303" id="Seg_7203" s="T299">Тогда обе подруги спрашивают:</ta>
            <ta e="T307" id="Seg_7204" s="T303">— О чем ты печалишься?</ta>
            <ta e="T309" id="Seg_7205" s="T307">Девушка говорит:</ta>
            <ta e="T317" id="Seg_7206" s="T309">— Моя мать отправила меня, не отдав самую заветную вещь.</ta>
            <ta e="T320" id="Seg_7207" s="T317">Тут подруги сказали:</ta>
            <ta e="T329" id="Seg_7208" s="T320">— Ну и что, когда мать придет, сама принесет и отдаст.</ta>
            <ta e="T337" id="Seg_7209" s="T329">— Ведь эта, оказывается, дочь старика, — подумал.</ta>
            <ta e="T344" id="Seg_7210" s="T337">Как уснула, обнял ее, поднял и полетел.</ta>
            <ta e="T351" id="Seg_7211" s="T344">И все это время, все девять суток, спал [провидец].</ta>
            <ta e="T358" id="Seg_7212" s="T351">Когда проснулся, вскочил — держит ее в своих объятиях.</ta>
            <ta e="T362" id="Seg_7213" s="T358">Тут бегут и приводят помощника шамана.</ta>
            <ta e="T374" id="Seg_7214" s="T362">— Принесите ее кости, — так говоря, камлает неистово три дня и три ночи подряд, возвращаясь из страны смерти.</ta>
            <ta e="T379" id="Seg_7215" s="T374">Тут приносят на оленьей шкуре ее кости.</ta>
            <ta e="T391" id="Seg_7216" s="T379">Видят: стоит, держа на руках девушку, только девушка не движется, будто мертвая или спящая.</ta>
            <ta e="T398" id="Seg_7217" s="T391">Кости той девушки вложил вовнутрь, словно в малицу.</ta>
            <ta e="T406" id="Seg_7218" s="T398">Та ожила, и оказалось, что это дочь старика.</ta>
            <ta e="T411" id="Seg_7219" s="T406">Провидец, взяв ее в жены, живет вместе с ней.</ta>
            <ta e="T419" id="Seg_7220" s="T411">Две оставшиеся подруги хотели было позвать ее на пляску — а подружки нет.</ta>
            <ta e="T422" id="Seg_7221" s="T419">Узнали, что увел шаман.</ta>
            <ta e="T425" id="Seg_7222" s="T422">Тогда они разгневались.</ta>
            <ta e="T433" id="Seg_7223" s="T425">Став блуждающими духами, нашли ожившую, завладели ее разумом, заставили стать шаманкой.</ta>
            <ta e="T436" id="Seg_7224" s="T433">Принуждая стать шаманкой, пригрозили:</ta>
            <ta e="T444" id="Seg_7225" s="T436">— Мы тебя съедим, если не сдерешь кожу со старухи и не обтянешь ею свой бубен.</ta>
            <ta e="T453" id="Seg_7226" s="T444">Вот тогда содрали кожу с одной старухи и ею обтянули бубен.</ta>
            <ta e="T462" id="Seg_7227" s="T453">Из-за этого греха, как только начнется большая болезнь, нганасаны не поодиночке умирают.</ta>
            <ta e="T465" id="Seg_7228" s="T462">Нганасаны целыми родами пропадают,</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
