<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDD82542FE-39BF-041B-1651-E5E1B16F8529">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>ChuAE_1968_Lyypyrdaan_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\ChuAE_1968_Lyypyrdaan_flk\ChuAE_1968_Lyypyrdaan_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">647</ud-information>
            <ud-information attribute-name="# HIAT:w">478</ud-information>
            <ud-information attribute-name="# e">478</ud-information>
            <ud-information attribute-name="# HIAT:u">88</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="ChAE">
            <abbreviation>ChuAE</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
         <tli id="T349" time="178.5" type="appl" />
         <tli id="T350" time="179.0" type="appl" />
         <tli id="T351" time="179.5" type="appl" />
         <tli id="T352" time="180.0" type="appl" />
         <tli id="T353" time="180.5" type="appl" />
         <tli id="T354" time="181.0" type="appl" />
         <tli id="T355" time="181.5" type="appl" />
         <tli id="T356" time="182.0" type="appl" />
         <tli id="T357" time="182.5" type="appl" />
         <tli id="T358" time="183.0" type="appl" />
         <tli id="T359" time="183.5" type="appl" />
         <tli id="T360" time="184.0" type="appl" />
         <tli id="T361" time="184.5" type="appl" />
         <tli id="T362" time="185.0" type="appl" />
         <tli id="T363" time="185.5" type="appl" />
         <tli id="T364" time="186.0" type="appl" />
         <tli id="T365" time="186.5" type="appl" />
         <tli id="T366" time="187.0" type="appl" />
         <tli id="T367" time="187.5" type="appl" />
         <tli id="T368" time="188.0" type="appl" />
         <tli id="T369" time="188.5" type="appl" />
         <tli id="T370" time="189.0" type="appl" />
         <tli id="T371" time="189.5" type="appl" />
         <tli id="T372" time="190.0" type="appl" />
         <tli id="T373" time="190.5" type="appl" />
         <tli id="T374" time="191.0" type="appl" />
         <tli id="T375" time="191.5" type="appl" />
         <tli id="T376" time="192.0" type="appl" />
         <tli id="T377" time="192.5" type="appl" />
         <tli id="T378" time="193.0" type="appl" />
         <tli id="T379" time="193.5" type="appl" />
         <tli id="T380" time="194.0" type="appl" />
         <tli id="T381" time="194.5" type="appl" />
         <tli id="T382" time="195.0" type="appl" />
         <tli id="T383" time="195.5" type="appl" />
         <tli id="T384" time="196.0" type="appl" />
         <tli id="T385" time="196.5" type="appl" />
         <tli id="T386" time="197.0" type="appl" />
         <tli id="T387" time="197.5" type="appl" />
         <tli id="T388" time="198.0" type="appl" />
         <tli id="T389" time="198.5" type="appl" />
         <tli id="T390" time="199.0" type="appl" />
         <tli id="T391" time="199.5" type="appl" />
         <tli id="T392" time="200.0" type="appl" />
         <tli id="T393" time="200.5" type="appl" />
         <tli id="T394" time="201.0" type="appl" />
         <tli id="T395" time="201.5" type="appl" />
         <tli id="T396" time="202.0" type="appl" />
         <tli id="T397" time="202.5" type="appl" />
         <tli id="T398" time="203.0" type="appl" />
         <tli id="T399" time="203.5" type="appl" />
         <tli id="T400" time="204.0" type="appl" />
         <tli id="T401" time="204.5" type="appl" />
         <tli id="T402" time="205.0" type="appl" />
         <tli id="T403" time="205.5" type="appl" />
         <tli id="T404" time="206.0" type="appl" />
         <tli id="T405" time="206.5" type="appl" />
         <tli id="T406" time="207.0" type="appl" />
         <tli id="T407" time="207.5" type="appl" />
         <tli id="T408" time="208.0" type="appl" />
         <tli id="T409" time="208.5" type="appl" />
         <tli id="T410" time="209.0" type="appl" />
         <tli id="T411" time="209.5" type="appl" />
         <tli id="T412" time="210.0" type="appl" />
         <tli id="T413" time="210.5" type="appl" />
         <tli id="T414" time="211.0" type="appl" />
         <tli id="T415" time="211.5" type="appl" />
         <tli id="T416" time="212.0" type="appl" />
         <tli id="T417" time="212.5" type="appl" />
         <tli id="T418" time="213.0" type="appl" />
         <tli id="T419" time="213.5" type="appl" />
         <tli id="T420" time="214.0" type="appl" />
         <tli id="T421" time="214.5" type="appl" />
         <tli id="T422" time="215.0" type="appl" />
         <tli id="T423" time="215.5" type="appl" />
         <tli id="T424" time="216.0" type="appl" />
         <tli id="T425" time="216.5" type="appl" />
         <tli id="T426" time="217.0" type="appl" />
         <tli id="T427" time="217.5" type="appl" />
         <tli id="T428" time="218.0" type="appl" />
         <tli id="T429" time="218.5" type="appl" />
         <tli id="T430" time="219.0" type="appl" />
         <tli id="T431" time="219.5" type="appl" />
         <tli id="T432" time="220.0" type="appl" />
         <tli id="T433" time="220.5" type="appl" />
         <tli id="T434" time="221.0" type="appl" />
         <tli id="T435" time="221.5" type="appl" />
         <tli id="T436" time="222.0" type="appl" />
         <tli id="T437" time="222.5" type="appl" />
         <tli id="T438" time="223.0" type="appl" />
         <tli id="T439" time="223.5" type="appl" />
         <tli id="T440" time="224.0" type="appl" />
         <tli id="T441" time="224.5" type="appl" />
         <tli id="T442" time="225.0" type="appl" />
         <tli id="T443" time="225.5" type="appl" />
         <tli id="T444" time="226.0" type="appl" />
         <tli id="T445" time="226.5" type="appl" />
         <tli id="T446" time="227.0" type="appl" />
         <tli id="T447" time="227.5" type="appl" />
         <tli id="T448" time="228.0" type="appl" />
         <tli id="T449" time="228.5" type="appl" />
         <tli id="T450" time="229.0" type="appl" />
         <tli id="T451" time="229.5" type="appl" />
         <tli id="T452" time="230.0" type="appl" />
         <tli id="T453" time="230.5" type="appl" />
         <tli id="T454" time="231.0" type="appl" />
         <tli id="T455" time="231.5" type="appl" />
         <tli id="T456" time="232.0" type="appl" />
         <tli id="T457" time="232.5" type="appl" />
         <tli id="T458" time="233.0" type="appl" />
         <tli id="T459" time="233.5" type="appl" />
         <tli id="T460" time="234.0" type="appl" />
         <tli id="T461" time="234.5" type="appl" />
         <tli id="T462" time="235.0" type="appl" />
         <tli id="T463" time="235.5" type="appl" />
         <tli id="T464" time="236.0" type="appl" />
         <tli id="T465" time="236.5" type="appl" />
         <tli id="T466" time="237.0" type="appl" />
         <tli id="T467" time="237.5" type="appl" />
         <tli id="T468" time="238.0" type="appl" />
         <tli id="T469" time="238.5" type="appl" />
         <tli id="T470" time="239.0" type="appl" />
         <tli id="T471" time="239.5" type="appl" />
         <tli id="T472" time="240.0" type="appl" />
         <tli id="T473" time="240.5" type="appl" />
         <tli id="T474" time="241.0" type="appl" />
         <tli id="T475" time="241.5" type="appl" />
         <tli id="T476" time="242.0" type="appl" />
         <tli id="T477" time="242.5" type="appl" />
         <tli id="T478" time="243.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="ChAE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T478" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Lɨːpɨrdaːn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">dʼe</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">oloror</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Lɨːpɨrdaːn</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">kaja</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">ere</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">di͡ekki</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">hɨldʼar</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_32" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">Bu</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">hɨldʼan</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">dʼaktardaːk</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">kihini</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">bular</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_50" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">U͡ollaːk</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">ol</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">kihi</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_62" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">Dʼaktardaːk</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">kihiŋ</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">tuspa</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">köspüt</ts>
                  <nts id="Seg_74" n="HIAT:ip">,</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">hogotogun</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_81" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">Karaŋa</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">bu͡olan</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">ki͡ehe</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">biːr</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">hirge</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">tüspüt</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_102" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_104" n="HIAT:w" s="T27">Utujaːrɨ</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">atagɨn</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">hɨgɨnnʼaktanan</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">erdegine</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">u͡ota</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">kasta</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">da</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">eppit</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_129" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">Bu</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">kihiŋ</ts>
                  <nts id="Seg_135" n="HIAT:ip">:</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_138" n="HIAT:u" s="T37">
                  <nts id="Seg_139" n="HIAT:ip">"</nts>
                  <ts e="T38" id="Seg_141" n="HIAT:w" s="T37">Min</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_144" n="HIAT:w" s="T38">künüs</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_147" n="HIAT:w" s="T39">hɨldʼɨbɨtɨm</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_150" n="HIAT:w" s="T40">diː</ts>
                  <nts id="Seg_151" n="HIAT:ip">,</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_154" n="HIAT:w" s="T41">tu͡ok</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_157" n="HIAT:w" s="T42">keli͡ej</ts>
                  <nts id="Seg_158" n="HIAT:ip">?</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_161" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_163" n="HIAT:w" s="T43">U͡otum</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_166" n="HIAT:w" s="T44">togo</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">eter</ts>
                  <nts id="Seg_170" n="HIAT:ip">"</nts>
                  <nts id="Seg_171" n="HIAT:ip">,</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_174" n="HIAT:w" s="T46">di͡en</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_177" n="HIAT:w" s="T47">kuttammat</ts>
                  <nts id="Seg_178" n="HIAT:ip">,</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_181" n="HIAT:w" s="T48">hɨtar</ts>
                  <nts id="Seg_182" n="HIAT:ip">.</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_185" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_187" n="HIAT:w" s="T49">Lɨːpɨrdaːn</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_190" n="HIAT:w" s="T50">kelen</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_193" n="HIAT:w" s="T51">bu</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_196" n="HIAT:w" s="T52">kömüs</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_199" n="HIAT:w" s="T53">ü͡östeːk</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_202" n="HIAT:w" s="T54">haːlaːk</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_205" n="HIAT:w" s="T55">kihini</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_208" n="HIAT:w" s="T56">ölörön</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_211" n="HIAT:w" s="T57">keːher</ts>
                  <nts id="Seg_212" n="HIAT:ip">,</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_215" n="HIAT:w" s="T58">alaŋaː</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_218" n="HIAT:w" s="T59">haː</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_221" n="HIAT:w" s="T60">bu͡ollaga</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_224" n="HIAT:w" s="T61">diː</ts>
                  <nts id="Seg_225" n="HIAT:ip">.</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_228" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_230" n="HIAT:w" s="T62">Kihin</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_233" n="HIAT:w" s="T63">dʼaktara</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_236" n="HIAT:w" s="T64">uččuguj</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_239" n="HIAT:w" s="T65">ogotun</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_242" n="HIAT:w" s="T66">kɨtta</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_245" n="HIAT:w" s="T67">hirge</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_248" n="HIAT:w" s="T68">küreːn</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_251" n="HIAT:w" s="T69">kaːlar</ts>
                  <nts id="Seg_252" n="HIAT:ip">.</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_255" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_257" n="HIAT:w" s="T70">Dʼaktarɨŋ</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_260" n="HIAT:w" s="T71">tuga</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_263" n="HIAT:w" s="T72">da</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_266" n="HIAT:w" s="T73">hu͡ok</ts>
                  <nts id="Seg_267" n="HIAT:ip">,</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_270" n="HIAT:w" s="T74">bu͡or</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_273" n="HIAT:w" s="T75">golomokoːŋŋo</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_276" n="HIAT:w" s="T76">oloror</ts>
                  <nts id="Seg_277" n="HIAT:ip">.</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_280" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_282" n="HIAT:w" s="T77">Oloŋko</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_285" n="HIAT:w" s="T78">ogoto</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_288" n="HIAT:w" s="T79">ör</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_291" n="HIAT:w" s="T80">bu͡olbat</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_294" n="HIAT:w" s="T81">ulaːtan</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_297" n="HIAT:w" s="T82">kaːlar</ts>
                  <nts id="Seg_298" n="HIAT:ip">.</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_301" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_303" n="HIAT:w" s="T83">Ogo</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_306" n="HIAT:w" s="T84">taksan</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_309" n="HIAT:w" s="T85">körbüte</ts>
                  <nts id="Seg_310" n="HIAT:ip">,</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_313" n="HIAT:w" s="T86">dʼi͡etin</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_316" n="HIAT:w" s="T87">ürdütüger</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_319" n="HIAT:w" s="T88">čeːlkeːler</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_322" n="HIAT:w" s="T89">hɨldʼallar</ts>
                  <nts id="Seg_323" n="HIAT:ip">.</nts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_326" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_328" n="HIAT:w" s="T90">Ijetiger</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_331" n="HIAT:w" s="T91">kiːren</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_334" n="HIAT:w" s="T92">kepsiːr</ts>
                  <nts id="Seg_335" n="HIAT:ip">:</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_338" n="HIAT:u" s="T93">
                  <nts id="Seg_339" n="HIAT:ip">"</nts>
                  <ts e="T94" id="Seg_341" n="HIAT:w" s="T93">Eː</ts>
                  <nts id="Seg_342" n="HIAT:ip">,</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_345" n="HIAT:w" s="T94">oččogo</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_348" n="HIAT:w" s="T95">kuruppaːskɨlar</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_351" n="HIAT:w" s="T96">bu͡ollaga</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_354" n="HIAT:w" s="T97">diː</ts>
                  <nts id="Seg_355" n="HIAT:ip">"</nts>
                  <nts id="Seg_356" n="HIAT:ip">,</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_359" n="HIAT:w" s="T98">diːr</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_362" n="HIAT:w" s="T99">ijete</ts>
                  <nts id="Seg_363" n="HIAT:ip">.</nts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_366" n="HIAT:u" s="T100">
                  <nts id="Seg_367" n="HIAT:ip">"</nts>
                  <ts e="T101" id="Seg_369" n="HIAT:w" s="T100">Oːt</ts>
                  <nts id="Seg_370" n="HIAT:ip">,</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_373" n="HIAT:w" s="T101">agalaːgɨm</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_376" n="HIAT:w" s="T102">ebite</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_379" n="HIAT:w" s="T103">bu͡ollar</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_382" n="HIAT:w" s="T104">kajdak</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_385" n="HIAT:w" s="T105">ere</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_388" n="HIAT:w" s="T106">gɨnɨ͡a</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_391" n="HIAT:w" s="T107">ete</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_394" n="HIAT:w" s="T108">bu͡ollaga</ts>
                  <nts id="Seg_395" n="HIAT:ip">"</nts>
                  <nts id="Seg_396" n="HIAT:ip">,</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_399" n="HIAT:w" s="T109">diːr</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_402" n="HIAT:w" s="T110">u͡ol</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_405" n="HIAT:w" s="T111">ijetiger</ts>
                  <nts id="Seg_406" n="HIAT:ip">.</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_409" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_411" n="HIAT:w" s="T112">Ijete</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_414" n="HIAT:w" s="T113">u͡ol</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_417" n="HIAT:w" s="T114">agalaːgɨn</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_420" n="HIAT:w" s="T115">tuhunan</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_423" n="HIAT:w" s="T116">amattan</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_426" n="HIAT:w" s="T117">kepseːbet</ts>
                  <nts id="Seg_427" n="HIAT:ip">.</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_430" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_432" n="HIAT:w" s="T118">Ije</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_435" n="HIAT:w" s="T119">ahɨn</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_438" n="HIAT:w" s="T120">bɨhan</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_441" n="HIAT:w" s="T121">mu͡ot</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_444" n="HIAT:w" s="T122">oŋoror</ts>
                  <nts id="Seg_445" n="HIAT:ip">,</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_448" n="HIAT:w" s="T123">u͡ol</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_451" n="HIAT:w" s="T124">manan</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_454" n="HIAT:w" s="T125">tuhak</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_457" n="HIAT:w" s="T126">oŋostor</ts>
                  <nts id="Seg_458" n="HIAT:ip">.</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_461" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_463" n="HIAT:w" s="T127">Manan</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_466" n="HIAT:w" s="T128">kuruppaːskɨnɨ</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_469" n="HIAT:w" s="T129">ölörön</ts>
                  <nts id="Seg_470" n="HIAT:ip">,</nts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_473" n="HIAT:w" s="T130">onon</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_476" n="HIAT:w" s="T131">iːttinen</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_479" n="HIAT:w" s="T132">kihi</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_482" n="HIAT:w" s="T133">bu͡olan</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_485" n="HIAT:w" s="T134">olorollor</ts>
                  <nts id="Seg_486" n="HIAT:ip">.</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_489" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_491" n="HIAT:w" s="T135">Bu</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_494" n="HIAT:w" s="T136">oloron</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_497" n="HIAT:w" s="T137">u͡ol</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_500" n="HIAT:w" s="T138">bi͡ek</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_503" n="HIAT:w" s="T139">ɨjɨtar</ts>
                  <nts id="Seg_504" n="HIAT:ip">:</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_507" n="HIAT:u" s="T140">
                  <nts id="Seg_508" n="HIAT:ip">"</nts>
                  <ts e="T141" id="Seg_510" n="HIAT:w" s="T140">Min</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_513" n="HIAT:w" s="T141">teːteleːk</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_516" n="HIAT:w" s="T142">eti͡em</ts>
                  <nts id="Seg_517" n="HIAT:ip">?</nts>
                  <nts id="Seg_518" n="HIAT:ip">"</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_521" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_523" n="HIAT:w" s="T143">Ijete</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_526" n="HIAT:w" s="T144">amattan</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_529" n="HIAT:w" s="T145">haŋarbat</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_532" n="HIAT:w" s="T146">ebit</ts>
                  <nts id="Seg_533" n="HIAT:ip">,</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_536" n="HIAT:w" s="T147">kistiːr</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_539" n="HIAT:w" s="T148">ebit</ts>
                  <nts id="Seg_540" n="HIAT:ip">.</nts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_543" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_545" n="HIAT:w" s="T149">Lɨːpɨrdaːn</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_548" n="HIAT:w" s="T150">emi͡e</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_551" n="HIAT:w" s="T151">kihini</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_554" n="HIAT:w" s="T152">ölöröːrü</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_557" n="HIAT:w" s="T153">hɨldʼar</ts>
                  <nts id="Seg_558" n="HIAT:ip">.</nts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_561" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_563" n="HIAT:w" s="T154">Biːr</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_566" n="HIAT:w" s="T155">u͡ollaːk</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_569" n="HIAT:w" s="T156">emeːksiŋŋe</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_572" n="HIAT:w" s="T157">keler</ts>
                  <nts id="Seg_573" n="HIAT:ip">.</nts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_576" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_578" n="HIAT:w" s="T158">Araj</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_581" n="HIAT:w" s="T159">körbüte</ts>
                  <nts id="Seg_582" n="HIAT:ip">,</nts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_585" n="HIAT:w" s="T160">hürdeːk</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_588" n="HIAT:w" s="T161">hu͡on</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_591" n="HIAT:w" s="T162">tiːt</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_594" n="HIAT:w" s="T163">mas</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_597" n="HIAT:w" s="T164">turar</ts>
                  <nts id="Seg_598" n="HIAT:ip">.</nts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_601" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_603" n="HIAT:w" s="T165">Bu</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_606" n="HIAT:w" s="T166">maska</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_609" n="HIAT:w" s="T167">emeːksin</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_612" n="HIAT:w" s="T168">u͡ola</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_615" n="HIAT:w" s="T169">haːtɨn</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_618" n="HIAT:w" s="T170">iːlen</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_621" n="HIAT:w" s="T171">keːspit</ts>
                  <nts id="Seg_622" n="HIAT:ip">.</nts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_625" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_627" n="HIAT:w" s="T172">Lɨːpɨrdaːn</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_630" n="HIAT:w" s="T173">ɨjɨtar</ts>
                  <nts id="Seg_631" n="HIAT:ip">:</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_634" n="HIAT:u" s="T174">
                  <nts id="Seg_635" n="HIAT:ip">"</nts>
                  <ts e="T175" id="Seg_637" n="HIAT:w" s="T174">Iti</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_640" n="HIAT:w" s="T175">kim</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_643" n="HIAT:w" s="T176">haːtaj</ts>
                  <nts id="Seg_644" n="HIAT:ip">?</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_647" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_649" n="HIAT:w" s="T177">Tu͡ok</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_652" n="HIAT:w" s="T178">kergenneːkkin</ts>
                  <nts id="Seg_653" n="HIAT:ip">?</nts>
                  <nts id="Seg_654" n="HIAT:ip">"</nts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_657" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_659" n="HIAT:w" s="T179">Emeːksin</ts>
                  <nts id="Seg_660" n="HIAT:ip">:</nts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_663" n="HIAT:u" s="T180">
                  <nts id="Seg_664" n="HIAT:ip">"</nts>
                  <ts e="T181" id="Seg_666" n="HIAT:w" s="T180">U͡ollaːkpɨn</ts>
                  <nts id="Seg_667" n="HIAT:ip">"</nts>
                  <nts id="Seg_668" n="HIAT:ip">,</nts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_671" n="HIAT:w" s="T181">diːr</ts>
                  <nts id="Seg_672" n="HIAT:ip">.</nts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_675" n="HIAT:u" s="T182">
                  <nts id="Seg_676" n="HIAT:ip">"</nts>
                  <ts e="T183" id="Seg_678" n="HIAT:w" s="T182">Beː</ts>
                  <nts id="Seg_679" n="HIAT:ip">,</nts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_682" n="HIAT:w" s="T183">iti</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_685" n="HIAT:w" s="T184">haːtɨn</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_688" n="HIAT:w" s="T185">togo</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_691" n="HIAT:w" s="T186">keːspitej</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_694" n="HIAT:w" s="T187">keː</ts>
                  <nts id="Seg_695" n="HIAT:ip">?</nts>
                  <nts id="Seg_696" n="HIAT:ip">"</nts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_699" n="HIAT:u" s="T188">
                  <nts id="Seg_700" n="HIAT:ip">"</nts>
                  <ts e="T189" id="Seg_702" n="HIAT:w" s="T188">Iti</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_705" n="HIAT:w" s="T189">haː</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_708" n="HIAT:w" s="T190">küːhe</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_711" n="HIAT:w" s="T191">taksɨbɨt</ts>
                  <nts id="Seg_712" n="HIAT:ip">,</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_715" n="HIAT:w" s="T192">ol</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_718" n="HIAT:w" s="T193">ihin</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_721" n="HIAT:w" s="T194">keːspite</ts>
                  <nts id="Seg_722" n="HIAT:ip">"</nts>
                  <nts id="Seg_723" n="HIAT:ip">,</nts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_726" n="HIAT:w" s="T195">diːr</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_729" n="HIAT:w" s="T196">emeːksin</ts>
                  <nts id="Seg_730" n="HIAT:ip">.</nts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_733" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_735" n="HIAT:w" s="T197">Onton</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_738" n="HIAT:w" s="T198">boldok</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_741" n="HIAT:w" s="T199">hɨtar</ts>
                  <nts id="Seg_742" n="HIAT:ip">,</nts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_745" n="HIAT:w" s="T200">ulakan</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_748" n="HIAT:w" s="T201">bagajɨ</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_751" n="HIAT:w" s="T202">taːs</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_754" n="HIAT:w" s="T203">kamenʼ</ts>
                  <nts id="Seg_755" n="HIAT:ip">.</nts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_758" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_760" n="HIAT:w" s="T204">Manɨ</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_763" n="HIAT:w" s="T205">emi͡e</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_766" n="HIAT:w" s="T206">ɨjɨtar</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_769" n="HIAT:w" s="T207">bili</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_772" n="HIAT:w" s="T208">kihi</ts>
                  <nts id="Seg_773" n="HIAT:ip">.</nts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_776" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_778" n="HIAT:w" s="T209">Emeːksin</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_781" n="HIAT:w" s="T210">onu</ts>
                  <nts id="Seg_782" n="HIAT:ip">:</nts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_785" n="HIAT:u" s="T211">
                  <nts id="Seg_786" n="HIAT:ip">"</nts>
                  <ts e="T212" id="Seg_788" n="HIAT:w" s="T211">Iti</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_791" n="HIAT:w" s="T212">čuŋkujdagɨna</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_794" n="HIAT:w" s="T213">meːčik</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_797" n="HIAT:w" s="T214">gɨnaːččɨ</ts>
                  <nts id="Seg_798" n="HIAT:ip">"</nts>
                  <nts id="Seg_799" n="HIAT:ip">,</nts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_802" n="HIAT:w" s="T215">diːr</ts>
                  <nts id="Seg_803" n="HIAT:ip">.</nts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_806" n="HIAT:u" s="T216">
                  <nts id="Seg_807" n="HIAT:ip">"</nts>
                  <ts e="T217" id="Seg_809" n="HIAT:w" s="T216">Ol</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_812" n="HIAT:w" s="T217">kajaga</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_815" n="HIAT:w" s="T218">di͡eri</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_818" n="HIAT:w" s="T219">teptegine</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_821" n="HIAT:w" s="T220">töttörü</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_824" n="HIAT:w" s="T221">čekinijen</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_827" n="HIAT:w" s="T222">keleːčči</ts>
                  <nts id="Seg_828" n="HIAT:ip">.</nts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_831" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_833" n="HIAT:w" s="T223">Ol</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_836" n="HIAT:w" s="T224">kurduk</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_839" n="HIAT:w" s="T225">oːnnʼoːčču</ts>
                  <nts id="Seg_840" n="HIAT:ip">"</nts>
                  <nts id="Seg_841" n="HIAT:ip">,</nts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_844" n="HIAT:w" s="T226">diːr</ts>
                  <nts id="Seg_845" n="HIAT:ip">.</nts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_848" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_850" n="HIAT:w" s="T227">Lɨːpɨrdaːn</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_853" n="HIAT:w" s="T228">eter</ts>
                  <nts id="Seg_854" n="HIAT:ip">:</nts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_857" n="HIAT:u" s="T229">
                  <nts id="Seg_858" n="HIAT:ip">"</nts>
                  <ts e="T230" id="Seg_860" n="HIAT:w" s="T229">Harsɨn</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_863" n="HIAT:w" s="T230">min</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_866" n="HIAT:w" s="T231">töröːbüt</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_869" n="HIAT:w" s="T232">künnenebin</ts>
                  <nts id="Seg_870" n="HIAT:ip">.</nts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_873" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_875" n="HIAT:w" s="T233">Onno</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_878" n="HIAT:w" s="T234">u͡oluŋ</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_881" n="HIAT:w" s="T235">keli͡ektin</ts>
                  <nts id="Seg_882" n="HIAT:ip">.</nts>
                  <nts id="Seg_883" n="HIAT:ip">"</nts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_886" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_888" n="HIAT:w" s="T236">Bu</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_891" n="HIAT:w" s="T237">u͡ol</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_894" n="HIAT:w" s="T238">dʼi͡etiger</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_897" n="HIAT:w" s="T239">kelbitiger</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_900" n="HIAT:w" s="T240">ijete</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_903" n="HIAT:w" s="T241">barɨtɨn</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_906" n="HIAT:w" s="T242">kepseːn</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_909" n="HIAT:w" s="T243">bi͡erer</ts>
                  <nts id="Seg_910" n="HIAT:ip">:</nts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_913" n="HIAT:u" s="T244">
                  <nts id="Seg_914" n="HIAT:ip">"</nts>
                  <ts e="T245" id="Seg_916" n="HIAT:w" s="T244">ɨŋɨrallar</ts>
                  <nts id="Seg_917" n="HIAT:ip">"</nts>
                  <nts id="Seg_918" n="HIAT:ip">,</nts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_921" n="HIAT:w" s="T245">diːr</ts>
                  <nts id="Seg_922" n="HIAT:ip">.</nts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_925" n="HIAT:u" s="T246">
                  <ts e="T247" id="Seg_927" n="HIAT:w" s="T246">Dʼe</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_930" n="HIAT:w" s="T247">araj</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_933" n="HIAT:w" s="T248">bu</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_936" n="HIAT:w" s="T249">kihiŋ</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_939" n="HIAT:w" s="T250">Lɨːpɨrdaːŋŋa</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_942" n="HIAT:w" s="T251">barda</ts>
                  <nts id="Seg_943" n="HIAT:ip">.</nts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_946" n="HIAT:u" s="T252">
                  <ts e="T253" id="Seg_948" n="HIAT:w" s="T252">Lɨːpɨrdaːn</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_951" n="HIAT:w" s="T253">kihileriger</ts>
                  <nts id="Seg_952" n="HIAT:ip">:</nts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_955" n="HIAT:u" s="T254">
                  <nts id="Seg_956" n="HIAT:ip">"</nts>
                  <ts e="T255" id="Seg_958" n="HIAT:w" s="T254">Bu</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_961" n="HIAT:w" s="T255">kihini</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_964" n="HIAT:w" s="T256">kürü͡ölü͡ökpüt</ts>
                  <nts id="Seg_965" n="HIAT:ip">,</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_968" n="HIAT:w" s="T257">ölörü͡ökpüt</ts>
                  <nts id="Seg_969" n="HIAT:ip">"</nts>
                  <nts id="Seg_970" n="HIAT:ip">,</nts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_973" n="HIAT:w" s="T258">di͡en</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_976" n="HIAT:w" s="T259">haŋarar</ts>
                  <nts id="Seg_977" n="HIAT:ip">.</nts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_980" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_982" n="HIAT:w" s="T260">Kelbitiger</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_985" n="HIAT:w" s="T261">ɨ͡aldʼɨttarɨn</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_988" n="HIAT:w" s="T262">kürü͡öleːn</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_991" n="HIAT:w" s="T263">kebiheller</ts>
                  <nts id="Seg_992" n="HIAT:ip">.</nts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T267" id="Seg_995" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_997" n="HIAT:w" s="T264">Ölöröːrüler</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1000" n="HIAT:w" s="T265">hubelespitter</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1003" n="HIAT:w" s="T266">bu͡ollaga</ts>
                  <nts id="Seg_1004" n="HIAT:ip">.</nts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T271" id="Seg_1007" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_1009" n="HIAT:w" s="T267">Lɨːpɨrdaːn</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1012" n="HIAT:w" s="T268">hallan</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1015" n="HIAT:w" s="T269">dogottorugar</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1018" n="HIAT:w" s="T270">haŋarar</ts>
                  <nts id="Seg_1019" n="HIAT:ip">:</nts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_1022" n="HIAT:u" s="T271">
                  <nts id="Seg_1023" n="HIAT:ip">"</nts>
                  <ts e="T272" id="Seg_1025" n="HIAT:w" s="T271">Bu</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1028" n="HIAT:w" s="T272">tu͡ok</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1031" n="HIAT:w" s="T273">bu͡olaŋŋɨt</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1034" n="HIAT:w" s="T274">munnʼullagɨt</ts>
                  <nts id="Seg_1035" n="HIAT:ip">?</nts>
                  <nts id="Seg_1036" n="HIAT:ip">"</nts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_1039" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_1041" n="HIAT:w" s="T275">ɨ͡aldʼɨt</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1044" n="HIAT:w" s="T276">manna</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1047" n="HIAT:w" s="T277">eter</ts>
                  <nts id="Seg_1048" n="HIAT:ip">:</nts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T284" id="Seg_1051" n="HIAT:u" s="T278">
                  <nts id="Seg_1052" n="HIAT:ip">"</nts>
                  <ts e="T279" id="Seg_1054" n="HIAT:w" s="T278">Ogonnʼor</ts>
                  <nts id="Seg_1055" n="HIAT:ip">,</nts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1058" n="HIAT:w" s="T279">en</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1061" n="HIAT:w" s="T280">miːgin</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1064" n="HIAT:w" s="T281">ölöröːrü</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1067" n="HIAT:w" s="T282">gɨnargɨn</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1070" n="HIAT:w" s="T283">bilebin</ts>
                  <nts id="Seg_1071" n="HIAT:ip">.</nts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_1074" n="HIAT:u" s="T284">
                  <ts e="T285" id="Seg_1076" n="HIAT:w" s="T284">Min</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1079" n="HIAT:w" s="T285">bu</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1082" n="HIAT:w" s="T286">hogotok</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1085" n="HIAT:w" s="T287">bɨhɨjalaːkpɨn</ts>
                  <nts id="Seg_1086" n="HIAT:ip">,</nts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1089" n="HIAT:w" s="T288">onon</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1092" n="HIAT:w" s="T289">noru͡okkun</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1095" n="HIAT:w" s="T290">biːrde</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1098" n="HIAT:w" s="T291">ergiji͡em</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1101" n="HIAT:w" s="T292">ete</ts>
                  <nts id="Seg_1102" n="HIAT:ip">"</nts>
                  <nts id="Seg_1103" n="HIAT:ip">,</nts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1106" n="HIAT:w" s="T293">diːr</ts>
                  <nts id="Seg_1107" n="HIAT:ip">.</nts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T298" id="Seg_1110" n="HIAT:u" s="T294">
                  <nts id="Seg_1111" n="HIAT:ip">"</nts>
                  <ts e="T295" id="Seg_1113" n="HIAT:w" s="T294">Kaja-kuː</ts>
                  <nts id="Seg_1114" n="HIAT:ip">,</nts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1117" n="HIAT:w" s="T295">iti</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1120" n="HIAT:w" s="T296">kurduk</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1123" n="HIAT:w" s="T297">gɨnɨma</ts>
                  <nts id="Seg_1124" n="HIAT:ip">.</nts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1127" n="HIAT:u" s="T298">
                  <ts e="T299" id="Seg_1129" n="HIAT:w" s="T298">Min</ts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1132" n="HIAT:w" s="T299">kɨːspɨn</ts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1135" n="HIAT:w" s="T300">dʼaktar</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1138" n="HIAT:w" s="T301">gɨn</ts>
                  <nts id="Seg_1139" n="HIAT:ip">"</nts>
                  <nts id="Seg_1140" n="HIAT:ip">,</nts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1143" n="HIAT:w" s="T302">diːr</ts>
                  <nts id="Seg_1144" n="HIAT:ip">.</nts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T309" id="Seg_1147" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1149" n="HIAT:w" s="T303">ɨ͡aldʼɨta</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1152" n="HIAT:w" s="T304">Lɨːpɨrdaːn</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1155" n="HIAT:w" s="T305">kɨːhɨn</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1158" n="HIAT:w" s="T306">dʼaktar</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1161" n="HIAT:w" s="T307">gɨna</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1164" n="HIAT:w" s="T308">hɨtar</ts>
                  <nts id="Seg_1165" n="HIAT:ip">.</nts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1168" n="HIAT:u" s="T309">
                  <ts e="T310" id="Seg_1170" n="HIAT:w" s="T309">Bu</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1173" n="HIAT:w" s="T310">u͡ol</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1176" n="HIAT:w" s="T311">ulaːtan</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1179" n="HIAT:w" s="T312">baran</ts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1182" n="HIAT:w" s="T313">teːtetin</ts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1185" n="HIAT:w" s="T314">ölörbüt</ts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1188" n="HIAT:w" s="T315">kihini</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1191" n="HIAT:w" s="T316">bi͡ek</ts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1194" n="HIAT:w" s="T317">kördüːr</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1197" n="HIAT:w" s="T318">ebit</ts>
                  <nts id="Seg_1198" n="HIAT:ip">.</nts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1201" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1203" n="HIAT:w" s="T319">Agata</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1206" n="HIAT:w" s="T320">ölbüt</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1209" n="HIAT:w" s="T321">hiriger</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1212" n="HIAT:w" s="T322">ulaːtan</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1215" n="HIAT:w" s="T323">baran</ts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1218" n="HIAT:w" s="T324">bi͡ek</ts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1221" n="HIAT:w" s="T325">hɨldʼar</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1224" n="HIAT:w" s="T326">ebit</ts>
                  <nts id="Seg_1225" n="HIAT:ip">.</nts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_1228" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1230" n="HIAT:w" s="T327">Araj</ts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1233" n="HIAT:w" s="T328">bu</ts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1236" n="HIAT:w" s="T329">hɨttagɨna</ts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1239" n="HIAT:w" s="T330">ikki</ts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1242" n="HIAT:w" s="T331">kihi</ts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1245" n="HIAT:w" s="T332">iheller</ts>
                  <nts id="Seg_1246" n="HIAT:ip">.</nts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T340" id="Seg_1249" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_1251" n="HIAT:w" s="T333">Araj</ts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1254" n="HIAT:w" s="T334">biːrdere</ts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1257" n="HIAT:w" s="T335">toŋ</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1260" n="HIAT:w" s="T336">ebekeni</ts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1263" n="HIAT:w" s="T337">tü͡öhüger</ts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1266" n="HIAT:w" s="T338">baːjan</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1269" n="HIAT:w" s="T339">iher</ts>
                  <nts id="Seg_1270" n="HIAT:ip">.</nts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T343" id="Seg_1273" n="HIAT:u" s="T340">
                  <ts e="T341" id="Seg_1275" n="HIAT:w" s="T340">Manɨ͡aka</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1278" n="HIAT:w" s="T341">dogoro</ts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1281" n="HIAT:w" s="T342">diːr</ts>
                  <nts id="Seg_1282" n="HIAT:ip">:</nts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1285" n="HIAT:u" s="T343">
                  <nts id="Seg_1286" n="HIAT:ip">"</nts>
                  <ts e="T344" id="Seg_1288" n="HIAT:w" s="T343">Manna</ts>
                  <nts id="Seg_1289" n="HIAT:ip">,</nts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1292" n="HIAT:w" s="T344">kaja</ts>
                  <nts id="Seg_1293" n="HIAT:ip">,</nts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1296" n="HIAT:w" s="T345">Lɨːpɨrdaːn</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1299" n="HIAT:w" s="T346">kömüs</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1302" n="HIAT:w" s="T347">ü͡östeːk</ts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1305" n="HIAT:w" s="T348">haːlaːk</ts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1308" n="HIAT:w" s="T349">kihini</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1311" n="HIAT:w" s="T350">ölörbüte</ts>
                  <nts id="Seg_1312" n="HIAT:ip">.</nts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T356" id="Seg_1315" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1317" n="HIAT:w" s="T351">Baːr</ts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1320" n="HIAT:w" s="T352">ol</ts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1323" n="HIAT:w" s="T353">kihi</ts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1326" n="HIAT:w" s="T354">u͡ola</ts>
                  <nts id="Seg_1327" n="HIAT:ip">"</nts>
                  <nts id="Seg_1328" n="HIAT:ip">,</nts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1331" n="HIAT:w" s="T355">diːr</ts>
                  <nts id="Seg_1332" n="HIAT:ip">.</nts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1335" n="HIAT:u" s="T356">
                  <ts e="T357" id="Seg_1337" n="HIAT:w" s="T356">U͡ol</ts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1340" n="HIAT:w" s="T357">onno</ts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1343" n="HIAT:w" s="T358">dʼe</ts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1346" n="HIAT:w" s="T359">agalaːgɨn</ts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1349" n="HIAT:w" s="T360">ister</ts>
                  <nts id="Seg_1350" n="HIAT:ip">.</nts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T368" id="Seg_1353" n="HIAT:u" s="T361">
                  <ts e="T362" id="Seg_1355" n="HIAT:w" s="T361">Bu</ts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1358" n="HIAT:w" s="T362">kihilergin</ts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1361" n="HIAT:w" s="T363">dʼe</ts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1364" n="HIAT:w" s="T364">ü͡ögülüːr</ts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1367" n="HIAT:w" s="T365">diː</ts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1370" n="HIAT:w" s="T366">bu</ts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1373" n="HIAT:w" s="T367">ogoŋ</ts>
                  <nts id="Seg_1374" n="HIAT:ip">.</nts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_1377" n="HIAT:u" s="T368">
                  <nts id="Seg_1378" n="HIAT:ip">"</nts>
                  <ts e="T369" id="Seg_1380" n="HIAT:w" s="T368">Hirbit</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1383" n="HIAT:w" s="T369">kuttuːr</ts>
                  <nts id="Seg_1384" n="HIAT:ip">"</nts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1387" n="HIAT:w" s="T370">di͡en</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1390" n="HIAT:w" s="T371">manna</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1393" n="HIAT:w" s="T372">ebekennerin</ts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1396" n="HIAT:w" s="T373">kaːllara</ts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1399" n="HIAT:w" s="T374">ku͡otallar</ts>
                  <nts id="Seg_1400" n="HIAT:ip">.</nts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T383" id="Seg_1403" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1405" n="HIAT:w" s="T375">U͡ol</ts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1408" n="HIAT:w" s="T376">ebekeni</ts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1411" n="HIAT:w" s="T377">kɨtta</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1414" n="HIAT:w" s="T378">bölüːgetin</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1417" n="HIAT:w" s="T379">dʼi͡etiger</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1420" n="HIAT:w" s="T380">ijetiger</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1423" n="HIAT:w" s="T381">ildʼe</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1426" n="HIAT:w" s="T382">barar</ts>
                  <nts id="Seg_1427" n="HIAT:ip">.</nts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T386" id="Seg_1430" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_1432" n="HIAT:w" s="T383">Bularɨ</ts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1435" n="HIAT:w" s="T384">hiːller</ts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1438" n="HIAT:w" s="T385">bu͡ollaga</ts>
                  <nts id="Seg_1439" n="HIAT:ip">.</nts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T388" id="Seg_1442" n="HIAT:u" s="T386">
                  <ts e="T387" id="Seg_1444" n="HIAT:w" s="T386">Ijete</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1447" n="HIAT:w" s="T387">hemeliːr</ts>
                  <nts id="Seg_1448" n="HIAT:ip">:</nts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1451" n="HIAT:u" s="T388">
                  <nts id="Seg_1452" n="HIAT:ip">"</nts>
                  <ts e="T389" id="Seg_1454" n="HIAT:w" s="T388">Bu</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1457" n="HIAT:w" s="T389">tu͡ok</ts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1460" n="HIAT:w" s="T390">ör</ts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1463" n="HIAT:w" s="T391">hɨldʼagɨn</ts>
                  <nts id="Seg_1464" n="HIAT:ip">?</nts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T394" id="Seg_1467" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1469" n="HIAT:w" s="T392">Ölörön</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1472" n="HIAT:w" s="T393">keːhi͡ektere</ts>
                  <nts id="Seg_1473" n="HIAT:ip">.</nts>
                  <nts id="Seg_1474" n="HIAT:ip">"</nts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_1477" n="HIAT:u" s="T394">
                  <ts e="T395" id="Seg_1479" n="HIAT:w" s="T394">U͡ola</ts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1482" n="HIAT:w" s="T395">ijetin</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1485" n="HIAT:w" s="T396">haŋatɨn</ts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1488" n="HIAT:w" s="T397">amattan</ts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1491" n="HIAT:w" s="T398">istibet</ts>
                  <nts id="Seg_1492" n="HIAT:ip">.</nts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T404" id="Seg_1495" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_1497" n="HIAT:w" s="T399">Bu</ts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1500" n="HIAT:w" s="T400">u͡ol</ts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1503" n="HIAT:w" s="T401">bi͡ek</ts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1506" n="HIAT:w" s="T402">agatɨn</ts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1509" n="HIAT:w" s="T403">kördüːr</ts>
                  <nts id="Seg_1510" n="HIAT:ip">.</nts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T411" id="Seg_1513" n="HIAT:u" s="T404">
                  <ts e="T405" id="Seg_1515" n="HIAT:w" s="T404">Bu</ts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1518" n="HIAT:w" s="T405">baran</ts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1521" n="HIAT:w" s="T406">Lɨːpɨrdaːn</ts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1524" n="HIAT:w" s="T407">ogonnʼor</ts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1527" n="HIAT:w" s="T408">dʼi͡etin</ts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1530" n="HIAT:w" s="T409">dʼe</ts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1533" n="HIAT:w" s="T410">bular</ts>
                  <nts id="Seg_1534" n="HIAT:ip">.</nts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T418" id="Seg_1537" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_1539" n="HIAT:w" s="T411">Bu</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1542" n="HIAT:w" s="T412">bulan</ts>
                  <nts id="Seg_1543" n="HIAT:ip">,</nts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1546" n="HIAT:w" s="T413">ki͡eheːŋŋi</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1549" n="HIAT:w" s="T414">uːnnan</ts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1552" n="HIAT:w" s="T415">utuja</ts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1555" n="HIAT:w" s="T416">hɨttaktarɨna</ts>
                  <nts id="Seg_1556" n="HIAT:ip">,</nts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1559" n="HIAT:w" s="T417">keler</ts>
                  <nts id="Seg_1560" n="HIAT:ip">.</nts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T428" id="Seg_1563" n="HIAT:u" s="T418">
                  <ts e="T419" id="Seg_1565" n="HIAT:w" s="T418">Utuja</ts>
                  <nts id="Seg_1566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1568" n="HIAT:w" s="T419">hɨtar</ts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1571" n="HIAT:w" s="T420">kihini</ts>
                  <nts id="Seg_1572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1574" n="HIAT:w" s="T421">kustugunan</ts>
                  <nts id="Seg_1575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1577" n="HIAT:w" s="T422">hu͡on</ts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1580" n="HIAT:w" s="T423">iŋiːrderin</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1583" n="HIAT:w" s="T424">bɨhɨta</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1586" n="HIAT:w" s="T425">ɨtɨ͡alɨːr</ts>
                  <nts id="Seg_1587" n="HIAT:ip">,</nts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1590" n="HIAT:w" s="T426">Lɨːpɨrdaːn</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1593" n="HIAT:w" s="T427">kütü͡ötün</ts>
                  <nts id="Seg_1594" n="HIAT:ip">.</nts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T434" id="Seg_1597" n="HIAT:u" s="T428">
                  <nts id="Seg_1598" n="HIAT:ip">"</nts>
                  <ts e="T429" id="Seg_1600" n="HIAT:w" s="T428">Dʼe</ts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1603" n="HIAT:w" s="T429">bu</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1606" n="HIAT:w" s="T430">kɨnnɨm</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1609" n="HIAT:w" s="T431">kuhaganɨgar</ts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1612" n="HIAT:w" s="T432">ölön</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1615" n="HIAT:w" s="T433">erebin</ts>
                  <nts id="Seg_1616" n="HIAT:ip">.</nts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T439" id="Seg_1619" n="HIAT:u" s="T434">
                  <ts e="T435" id="Seg_1621" n="HIAT:w" s="T434">Min</ts>
                  <nts id="Seg_1622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1624" n="HIAT:w" s="T435">en</ts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1627" n="HIAT:w" s="T436">agatɨn</ts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1630" n="HIAT:w" s="T437">ölörbötögüm</ts>
                  <nts id="Seg_1631" n="HIAT:ip">"</nts>
                  <nts id="Seg_1632" n="HIAT:ip">,</nts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1635" n="HIAT:w" s="T438">diːr</ts>
                  <nts id="Seg_1636" n="HIAT:ip">.</nts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T443" id="Seg_1639" n="HIAT:u" s="T439">
                  <nts id="Seg_1640" n="HIAT:ip">"</nts>
                  <ts e="T440" id="Seg_1642" n="HIAT:w" s="T439">Ajɨlaːgɨn</ts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1645" n="HIAT:w" s="T440">miːgin</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1648" n="HIAT:w" s="T441">tɨːmmɨn</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1651" n="HIAT:w" s="T442">bɨs</ts>
                  <nts id="Seg_1652" n="HIAT:ip">.</nts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T449" id="Seg_1655" n="HIAT:u" s="T443">
                  <ts e="T444" id="Seg_1657" n="HIAT:w" s="T443">Anɨ</ts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1660" n="HIAT:w" s="T444">da</ts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1663" n="HIAT:w" s="T445">tu͡ok</ts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1666" n="HIAT:w" s="T446">kihite</ts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1669" n="HIAT:w" s="T447">bu͡olu͡omuj</ts>
                  <nts id="Seg_1670" n="HIAT:ip">"</nts>
                  <nts id="Seg_1671" n="HIAT:ip">,</nts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1674" n="HIAT:w" s="T448">diːr</ts>
                  <nts id="Seg_1675" n="HIAT:ip">.</nts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T456" id="Seg_1678" n="HIAT:u" s="T449">
                  <ts e="T450" id="Seg_1680" n="HIAT:w" s="T449">Bu</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1683" n="HIAT:w" s="T450">ogoŋ</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1686" n="HIAT:w" s="T451">bu</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1689" n="HIAT:w" s="T452">kihini</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1692" n="HIAT:w" s="T453">tɨːnɨn</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1695" n="HIAT:w" s="T454">bɨhan</ts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1698" n="HIAT:w" s="T455">keːher</ts>
                  <nts id="Seg_1699" n="HIAT:ip">.</nts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_1702" n="HIAT:u" s="T456">
                  <ts e="T457" id="Seg_1704" n="HIAT:w" s="T456">Bu</ts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1707" n="HIAT:w" s="T457">ogoŋ</ts>
                  <nts id="Seg_1708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1710" n="HIAT:w" s="T458">manɨ</ts>
                  <nts id="Seg_1711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1713" n="HIAT:w" s="T459">ister</ts>
                  <nts id="Seg_1714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1716" n="HIAT:w" s="T460">da</ts>
                  <nts id="Seg_1717" n="HIAT:ip">,</nts>
                  <nts id="Seg_1718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1720" n="HIAT:w" s="T461">Lɨːpɨrdaːnɨ</ts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1723" n="HIAT:w" s="T462">ölörön</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1726" n="HIAT:w" s="T463">keːher</ts>
                  <nts id="Seg_1727" n="HIAT:ip">.</nts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T468" id="Seg_1730" n="HIAT:u" s="T464">
                  <ts e="T465" id="Seg_1732" n="HIAT:w" s="T464">Baːjɨn-totun</ts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1735" n="HIAT:w" s="T465">dʼi͡etiger</ts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1738" n="HIAT:w" s="T466">ildʼe</ts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1741" n="HIAT:w" s="T467">barar</ts>
                  <nts id="Seg_1742" n="HIAT:ip">.</nts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T477" id="Seg_1745" n="HIAT:u" s="T468">
                  <ts e="T469" id="Seg_1747" n="HIAT:w" s="T468">Bu</ts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1750" n="HIAT:w" s="T469">baːjgɨn</ts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1753" n="HIAT:w" s="T470">dʼi͡etiger</ts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1756" n="HIAT:w" s="T471">ti͡erden</ts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1759" n="HIAT:w" s="T472">ijetin</ts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1762" n="HIAT:w" s="T473">kɨtta</ts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1765" n="HIAT:w" s="T474">bajan-tajan</ts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1768" n="HIAT:w" s="T475">dʼe</ts>
                  <nts id="Seg_1769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1771" n="HIAT:w" s="T476">olorbuta</ts>
                  <nts id="Seg_1772" n="HIAT:ip">.</nts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T478" id="Seg_1775" n="HIAT:u" s="T477">
                  <ts e="T478" id="Seg_1777" n="HIAT:w" s="T477">Elete</ts>
                  <nts id="Seg_1778" n="HIAT:ip">.</nts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T478" id="Seg_1780" n="sc" s="T0">
               <ts e="T1" id="Seg_1782" n="e" s="T0">Lɨːpɨrdaːn </ts>
               <ts e="T2" id="Seg_1784" n="e" s="T1">dʼe </ts>
               <ts e="T3" id="Seg_1786" n="e" s="T2">oloror. </ts>
               <ts e="T4" id="Seg_1788" n="e" s="T3">Lɨːpɨrdaːn </ts>
               <ts e="T5" id="Seg_1790" n="e" s="T4">kaja </ts>
               <ts e="T6" id="Seg_1792" n="e" s="T5">ere </ts>
               <ts e="T7" id="Seg_1794" n="e" s="T6">di͡ekki </ts>
               <ts e="T8" id="Seg_1796" n="e" s="T7">hɨldʼar. </ts>
               <ts e="T9" id="Seg_1798" n="e" s="T8">Bu </ts>
               <ts e="T10" id="Seg_1800" n="e" s="T9">hɨldʼan </ts>
               <ts e="T11" id="Seg_1802" n="e" s="T10">dʼaktardaːk </ts>
               <ts e="T12" id="Seg_1804" n="e" s="T11">kihini </ts>
               <ts e="T13" id="Seg_1806" n="e" s="T12">bular. </ts>
               <ts e="T14" id="Seg_1808" n="e" s="T13">U͡ollaːk </ts>
               <ts e="T15" id="Seg_1810" n="e" s="T14">ol </ts>
               <ts e="T16" id="Seg_1812" n="e" s="T15">kihi. </ts>
               <ts e="T17" id="Seg_1814" n="e" s="T16">Dʼaktardaːk </ts>
               <ts e="T18" id="Seg_1816" n="e" s="T17">kihiŋ </ts>
               <ts e="T19" id="Seg_1818" n="e" s="T18">tuspa </ts>
               <ts e="T20" id="Seg_1820" n="e" s="T19">köspüt, </ts>
               <ts e="T21" id="Seg_1822" n="e" s="T20">hogotogun. </ts>
               <ts e="T22" id="Seg_1824" n="e" s="T21">Karaŋa </ts>
               <ts e="T23" id="Seg_1826" n="e" s="T22">bu͡olan </ts>
               <ts e="T24" id="Seg_1828" n="e" s="T23">ki͡ehe </ts>
               <ts e="T25" id="Seg_1830" n="e" s="T24">biːr </ts>
               <ts e="T26" id="Seg_1832" n="e" s="T25">hirge </ts>
               <ts e="T27" id="Seg_1834" n="e" s="T26">tüspüt. </ts>
               <ts e="T28" id="Seg_1836" n="e" s="T27">Utujaːrɨ </ts>
               <ts e="T29" id="Seg_1838" n="e" s="T28">atagɨn </ts>
               <ts e="T30" id="Seg_1840" n="e" s="T29">hɨgɨnnʼaktanan </ts>
               <ts e="T31" id="Seg_1842" n="e" s="T30">erdegine </ts>
               <ts e="T32" id="Seg_1844" n="e" s="T31">u͡ota </ts>
               <ts e="T33" id="Seg_1846" n="e" s="T32">kasta </ts>
               <ts e="T34" id="Seg_1848" n="e" s="T33">da </ts>
               <ts e="T35" id="Seg_1850" n="e" s="T34">eppit. </ts>
               <ts e="T36" id="Seg_1852" n="e" s="T35">Bu </ts>
               <ts e="T37" id="Seg_1854" n="e" s="T36">kihiŋ: </ts>
               <ts e="T38" id="Seg_1856" n="e" s="T37">"Min </ts>
               <ts e="T39" id="Seg_1858" n="e" s="T38">künüs </ts>
               <ts e="T40" id="Seg_1860" n="e" s="T39">hɨldʼɨbɨtɨm </ts>
               <ts e="T41" id="Seg_1862" n="e" s="T40">diː, </ts>
               <ts e="T42" id="Seg_1864" n="e" s="T41">tu͡ok </ts>
               <ts e="T43" id="Seg_1866" n="e" s="T42">keli͡ej? </ts>
               <ts e="T44" id="Seg_1868" n="e" s="T43">U͡otum </ts>
               <ts e="T45" id="Seg_1870" n="e" s="T44">togo </ts>
               <ts e="T46" id="Seg_1872" n="e" s="T45">eter", </ts>
               <ts e="T47" id="Seg_1874" n="e" s="T46">di͡en </ts>
               <ts e="T48" id="Seg_1876" n="e" s="T47">kuttammat, </ts>
               <ts e="T49" id="Seg_1878" n="e" s="T48">hɨtar. </ts>
               <ts e="T50" id="Seg_1880" n="e" s="T49">Lɨːpɨrdaːn </ts>
               <ts e="T51" id="Seg_1882" n="e" s="T50">kelen </ts>
               <ts e="T52" id="Seg_1884" n="e" s="T51">bu </ts>
               <ts e="T53" id="Seg_1886" n="e" s="T52">kömüs </ts>
               <ts e="T54" id="Seg_1888" n="e" s="T53">ü͡östeːk </ts>
               <ts e="T55" id="Seg_1890" n="e" s="T54">haːlaːk </ts>
               <ts e="T56" id="Seg_1892" n="e" s="T55">kihini </ts>
               <ts e="T57" id="Seg_1894" n="e" s="T56">ölörön </ts>
               <ts e="T58" id="Seg_1896" n="e" s="T57">keːher, </ts>
               <ts e="T59" id="Seg_1898" n="e" s="T58">alaŋaː </ts>
               <ts e="T60" id="Seg_1900" n="e" s="T59">haː </ts>
               <ts e="T61" id="Seg_1902" n="e" s="T60">bu͡ollaga </ts>
               <ts e="T62" id="Seg_1904" n="e" s="T61">diː. </ts>
               <ts e="T63" id="Seg_1906" n="e" s="T62">Kihin </ts>
               <ts e="T64" id="Seg_1908" n="e" s="T63">dʼaktara </ts>
               <ts e="T65" id="Seg_1910" n="e" s="T64">uččuguj </ts>
               <ts e="T66" id="Seg_1912" n="e" s="T65">ogotun </ts>
               <ts e="T67" id="Seg_1914" n="e" s="T66">kɨtta </ts>
               <ts e="T68" id="Seg_1916" n="e" s="T67">hirge </ts>
               <ts e="T69" id="Seg_1918" n="e" s="T68">küreːn </ts>
               <ts e="T70" id="Seg_1920" n="e" s="T69">kaːlar. </ts>
               <ts e="T71" id="Seg_1922" n="e" s="T70">Dʼaktarɨŋ </ts>
               <ts e="T72" id="Seg_1924" n="e" s="T71">tuga </ts>
               <ts e="T73" id="Seg_1926" n="e" s="T72">da </ts>
               <ts e="T74" id="Seg_1928" n="e" s="T73">hu͡ok, </ts>
               <ts e="T75" id="Seg_1930" n="e" s="T74">bu͡or </ts>
               <ts e="T76" id="Seg_1932" n="e" s="T75">golomokoːŋŋo </ts>
               <ts e="T77" id="Seg_1934" n="e" s="T76">oloror. </ts>
               <ts e="T78" id="Seg_1936" n="e" s="T77">Oloŋko </ts>
               <ts e="T79" id="Seg_1938" n="e" s="T78">ogoto </ts>
               <ts e="T80" id="Seg_1940" n="e" s="T79">ör </ts>
               <ts e="T81" id="Seg_1942" n="e" s="T80">bu͡olbat </ts>
               <ts e="T82" id="Seg_1944" n="e" s="T81">ulaːtan </ts>
               <ts e="T83" id="Seg_1946" n="e" s="T82">kaːlar. </ts>
               <ts e="T84" id="Seg_1948" n="e" s="T83">Ogo </ts>
               <ts e="T85" id="Seg_1950" n="e" s="T84">taksan </ts>
               <ts e="T86" id="Seg_1952" n="e" s="T85">körbüte, </ts>
               <ts e="T87" id="Seg_1954" n="e" s="T86">dʼi͡etin </ts>
               <ts e="T88" id="Seg_1956" n="e" s="T87">ürdütüger </ts>
               <ts e="T89" id="Seg_1958" n="e" s="T88">čeːlkeːler </ts>
               <ts e="T90" id="Seg_1960" n="e" s="T89">hɨldʼallar. </ts>
               <ts e="T91" id="Seg_1962" n="e" s="T90">Ijetiger </ts>
               <ts e="T92" id="Seg_1964" n="e" s="T91">kiːren </ts>
               <ts e="T93" id="Seg_1966" n="e" s="T92">kepsiːr: </ts>
               <ts e="T94" id="Seg_1968" n="e" s="T93">"Eː, </ts>
               <ts e="T95" id="Seg_1970" n="e" s="T94">oččogo </ts>
               <ts e="T96" id="Seg_1972" n="e" s="T95">kuruppaːskɨlar </ts>
               <ts e="T97" id="Seg_1974" n="e" s="T96">bu͡ollaga </ts>
               <ts e="T98" id="Seg_1976" n="e" s="T97">diː", </ts>
               <ts e="T99" id="Seg_1978" n="e" s="T98">diːr </ts>
               <ts e="T100" id="Seg_1980" n="e" s="T99">ijete. </ts>
               <ts e="T101" id="Seg_1982" n="e" s="T100">"Oːt, </ts>
               <ts e="T102" id="Seg_1984" n="e" s="T101">agalaːgɨm </ts>
               <ts e="T103" id="Seg_1986" n="e" s="T102">ebite </ts>
               <ts e="T104" id="Seg_1988" n="e" s="T103">bu͡ollar </ts>
               <ts e="T105" id="Seg_1990" n="e" s="T104">kajdak </ts>
               <ts e="T106" id="Seg_1992" n="e" s="T105">ere </ts>
               <ts e="T107" id="Seg_1994" n="e" s="T106">gɨnɨ͡a </ts>
               <ts e="T108" id="Seg_1996" n="e" s="T107">ete </ts>
               <ts e="T109" id="Seg_1998" n="e" s="T108">bu͡ollaga", </ts>
               <ts e="T110" id="Seg_2000" n="e" s="T109">diːr </ts>
               <ts e="T111" id="Seg_2002" n="e" s="T110">u͡ol </ts>
               <ts e="T112" id="Seg_2004" n="e" s="T111">ijetiger. </ts>
               <ts e="T113" id="Seg_2006" n="e" s="T112">Ijete </ts>
               <ts e="T114" id="Seg_2008" n="e" s="T113">u͡ol </ts>
               <ts e="T115" id="Seg_2010" n="e" s="T114">agalaːgɨn </ts>
               <ts e="T116" id="Seg_2012" n="e" s="T115">tuhunan </ts>
               <ts e="T117" id="Seg_2014" n="e" s="T116">amattan </ts>
               <ts e="T118" id="Seg_2016" n="e" s="T117">kepseːbet. </ts>
               <ts e="T119" id="Seg_2018" n="e" s="T118">Ije </ts>
               <ts e="T120" id="Seg_2020" n="e" s="T119">ahɨn </ts>
               <ts e="T121" id="Seg_2022" n="e" s="T120">bɨhan </ts>
               <ts e="T122" id="Seg_2024" n="e" s="T121">mu͡ot </ts>
               <ts e="T123" id="Seg_2026" n="e" s="T122">oŋoror, </ts>
               <ts e="T124" id="Seg_2028" n="e" s="T123">u͡ol </ts>
               <ts e="T125" id="Seg_2030" n="e" s="T124">manan </ts>
               <ts e="T126" id="Seg_2032" n="e" s="T125">tuhak </ts>
               <ts e="T127" id="Seg_2034" n="e" s="T126">oŋostor. </ts>
               <ts e="T128" id="Seg_2036" n="e" s="T127">Manan </ts>
               <ts e="T129" id="Seg_2038" n="e" s="T128">kuruppaːskɨnɨ </ts>
               <ts e="T130" id="Seg_2040" n="e" s="T129">ölörön, </ts>
               <ts e="T131" id="Seg_2042" n="e" s="T130">onon </ts>
               <ts e="T132" id="Seg_2044" n="e" s="T131">iːttinen </ts>
               <ts e="T133" id="Seg_2046" n="e" s="T132">kihi </ts>
               <ts e="T134" id="Seg_2048" n="e" s="T133">bu͡olan </ts>
               <ts e="T135" id="Seg_2050" n="e" s="T134">olorollor. </ts>
               <ts e="T136" id="Seg_2052" n="e" s="T135">Bu </ts>
               <ts e="T137" id="Seg_2054" n="e" s="T136">oloron </ts>
               <ts e="T138" id="Seg_2056" n="e" s="T137">u͡ol </ts>
               <ts e="T139" id="Seg_2058" n="e" s="T138">bi͡ek </ts>
               <ts e="T140" id="Seg_2060" n="e" s="T139">ɨjɨtar: </ts>
               <ts e="T141" id="Seg_2062" n="e" s="T140">"Min </ts>
               <ts e="T142" id="Seg_2064" n="e" s="T141">teːteleːk </ts>
               <ts e="T143" id="Seg_2066" n="e" s="T142">eti͡em?" </ts>
               <ts e="T144" id="Seg_2068" n="e" s="T143">Ijete </ts>
               <ts e="T145" id="Seg_2070" n="e" s="T144">amattan </ts>
               <ts e="T146" id="Seg_2072" n="e" s="T145">haŋarbat </ts>
               <ts e="T147" id="Seg_2074" n="e" s="T146">ebit, </ts>
               <ts e="T148" id="Seg_2076" n="e" s="T147">kistiːr </ts>
               <ts e="T149" id="Seg_2078" n="e" s="T148">ebit. </ts>
               <ts e="T150" id="Seg_2080" n="e" s="T149">Lɨːpɨrdaːn </ts>
               <ts e="T151" id="Seg_2082" n="e" s="T150">emi͡e </ts>
               <ts e="T152" id="Seg_2084" n="e" s="T151">kihini </ts>
               <ts e="T153" id="Seg_2086" n="e" s="T152">ölöröːrü </ts>
               <ts e="T154" id="Seg_2088" n="e" s="T153">hɨldʼar. </ts>
               <ts e="T155" id="Seg_2090" n="e" s="T154">Biːr </ts>
               <ts e="T156" id="Seg_2092" n="e" s="T155">u͡ollaːk </ts>
               <ts e="T157" id="Seg_2094" n="e" s="T156">emeːksiŋŋe </ts>
               <ts e="T158" id="Seg_2096" n="e" s="T157">keler. </ts>
               <ts e="T159" id="Seg_2098" n="e" s="T158">Araj </ts>
               <ts e="T160" id="Seg_2100" n="e" s="T159">körbüte, </ts>
               <ts e="T161" id="Seg_2102" n="e" s="T160">hürdeːk </ts>
               <ts e="T162" id="Seg_2104" n="e" s="T161">hu͡on </ts>
               <ts e="T163" id="Seg_2106" n="e" s="T162">tiːt </ts>
               <ts e="T164" id="Seg_2108" n="e" s="T163">mas </ts>
               <ts e="T165" id="Seg_2110" n="e" s="T164">turar. </ts>
               <ts e="T166" id="Seg_2112" n="e" s="T165">Bu </ts>
               <ts e="T167" id="Seg_2114" n="e" s="T166">maska </ts>
               <ts e="T168" id="Seg_2116" n="e" s="T167">emeːksin </ts>
               <ts e="T169" id="Seg_2118" n="e" s="T168">u͡ola </ts>
               <ts e="T170" id="Seg_2120" n="e" s="T169">haːtɨn </ts>
               <ts e="T171" id="Seg_2122" n="e" s="T170">iːlen </ts>
               <ts e="T172" id="Seg_2124" n="e" s="T171">keːspit. </ts>
               <ts e="T173" id="Seg_2126" n="e" s="T172">Lɨːpɨrdaːn </ts>
               <ts e="T174" id="Seg_2128" n="e" s="T173">ɨjɨtar: </ts>
               <ts e="T175" id="Seg_2130" n="e" s="T174">"Iti </ts>
               <ts e="T176" id="Seg_2132" n="e" s="T175">kim </ts>
               <ts e="T177" id="Seg_2134" n="e" s="T176">haːtaj? </ts>
               <ts e="T178" id="Seg_2136" n="e" s="T177">Tu͡ok </ts>
               <ts e="T179" id="Seg_2138" n="e" s="T178">kergenneːkkin?" </ts>
               <ts e="T180" id="Seg_2140" n="e" s="T179">Emeːksin: </ts>
               <ts e="T181" id="Seg_2142" n="e" s="T180">"U͡ollaːkpɨn", </ts>
               <ts e="T182" id="Seg_2144" n="e" s="T181">diːr. </ts>
               <ts e="T183" id="Seg_2146" n="e" s="T182">"Beː, </ts>
               <ts e="T184" id="Seg_2148" n="e" s="T183">iti </ts>
               <ts e="T185" id="Seg_2150" n="e" s="T184">haːtɨn </ts>
               <ts e="T186" id="Seg_2152" n="e" s="T185">togo </ts>
               <ts e="T187" id="Seg_2154" n="e" s="T186">keːspitej </ts>
               <ts e="T188" id="Seg_2156" n="e" s="T187">keː?" </ts>
               <ts e="T189" id="Seg_2158" n="e" s="T188">"Iti </ts>
               <ts e="T190" id="Seg_2160" n="e" s="T189">haː </ts>
               <ts e="T191" id="Seg_2162" n="e" s="T190">küːhe </ts>
               <ts e="T192" id="Seg_2164" n="e" s="T191">taksɨbɨt, </ts>
               <ts e="T193" id="Seg_2166" n="e" s="T192">ol </ts>
               <ts e="T194" id="Seg_2168" n="e" s="T193">ihin </ts>
               <ts e="T195" id="Seg_2170" n="e" s="T194">keːspite", </ts>
               <ts e="T196" id="Seg_2172" n="e" s="T195">diːr </ts>
               <ts e="T197" id="Seg_2174" n="e" s="T196">emeːksin. </ts>
               <ts e="T198" id="Seg_2176" n="e" s="T197">Onton </ts>
               <ts e="T199" id="Seg_2178" n="e" s="T198">boldok </ts>
               <ts e="T200" id="Seg_2180" n="e" s="T199">hɨtar, </ts>
               <ts e="T201" id="Seg_2182" n="e" s="T200">ulakan </ts>
               <ts e="T202" id="Seg_2184" n="e" s="T201">bagajɨ </ts>
               <ts e="T203" id="Seg_2186" n="e" s="T202">taːs </ts>
               <ts e="T204" id="Seg_2188" n="e" s="T203">kamenʼ. </ts>
               <ts e="T205" id="Seg_2190" n="e" s="T204">Manɨ </ts>
               <ts e="T206" id="Seg_2192" n="e" s="T205">emi͡e </ts>
               <ts e="T207" id="Seg_2194" n="e" s="T206">ɨjɨtar </ts>
               <ts e="T208" id="Seg_2196" n="e" s="T207">bili </ts>
               <ts e="T209" id="Seg_2198" n="e" s="T208">kihi. </ts>
               <ts e="T210" id="Seg_2200" n="e" s="T209">Emeːksin </ts>
               <ts e="T211" id="Seg_2202" n="e" s="T210">onu: </ts>
               <ts e="T212" id="Seg_2204" n="e" s="T211">"Iti </ts>
               <ts e="T213" id="Seg_2206" n="e" s="T212">čuŋkujdagɨna </ts>
               <ts e="T214" id="Seg_2208" n="e" s="T213">meːčik </ts>
               <ts e="T215" id="Seg_2210" n="e" s="T214">gɨnaːččɨ", </ts>
               <ts e="T216" id="Seg_2212" n="e" s="T215">diːr. </ts>
               <ts e="T217" id="Seg_2214" n="e" s="T216">"Ol </ts>
               <ts e="T218" id="Seg_2216" n="e" s="T217">kajaga </ts>
               <ts e="T219" id="Seg_2218" n="e" s="T218">di͡eri </ts>
               <ts e="T220" id="Seg_2220" n="e" s="T219">teptegine </ts>
               <ts e="T221" id="Seg_2222" n="e" s="T220">töttörü </ts>
               <ts e="T222" id="Seg_2224" n="e" s="T221">čekinijen </ts>
               <ts e="T223" id="Seg_2226" n="e" s="T222">keleːčči. </ts>
               <ts e="T224" id="Seg_2228" n="e" s="T223">Ol </ts>
               <ts e="T225" id="Seg_2230" n="e" s="T224">kurduk </ts>
               <ts e="T226" id="Seg_2232" n="e" s="T225">oːnnʼoːčču", </ts>
               <ts e="T227" id="Seg_2234" n="e" s="T226">diːr. </ts>
               <ts e="T228" id="Seg_2236" n="e" s="T227">Lɨːpɨrdaːn </ts>
               <ts e="T229" id="Seg_2238" n="e" s="T228">eter: </ts>
               <ts e="T230" id="Seg_2240" n="e" s="T229">"Harsɨn </ts>
               <ts e="T231" id="Seg_2242" n="e" s="T230">min </ts>
               <ts e="T232" id="Seg_2244" n="e" s="T231">töröːbüt </ts>
               <ts e="T233" id="Seg_2246" n="e" s="T232">künnenebin. </ts>
               <ts e="T234" id="Seg_2248" n="e" s="T233">Onno </ts>
               <ts e="T235" id="Seg_2250" n="e" s="T234">u͡oluŋ </ts>
               <ts e="T236" id="Seg_2252" n="e" s="T235">keli͡ektin." </ts>
               <ts e="T237" id="Seg_2254" n="e" s="T236">Bu </ts>
               <ts e="T238" id="Seg_2256" n="e" s="T237">u͡ol </ts>
               <ts e="T239" id="Seg_2258" n="e" s="T238">dʼi͡etiger </ts>
               <ts e="T240" id="Seg_2260" n="e" s="T239">kelbitiger </ts>
               <ts e="T241" id="Seg_2262" n="e" s="T240">ijete </ts>
               <ts e="T242" id="Seg_2264" n="e" s="T241">barɨtɨn </ts>
               <ts e="T243" id="Seg_2266" n="e" s="T242">kepseːn </ts>
               <ts e="T244" id="Seg_2268" n="e" s="T243">bi͡erer: </ts>
               <ts e="T245" id="Seg_2270" n="e" s="T244">"ɨŋɨrallar", </ts>
               <ts e="T246" id="Seg_2272" n="e" s="T245">diːr. </ts>
               <ts e="T247" id="Seg_2274" n="e" s="T246">Dʼe </ts>
               <ts e="T248" id="Seg_2276" n="e" s="T247">araj </ts>
               <ts e="T249" id="Seg_2278" n="e" s="T248">bu </ts>
               <ts e="T250" id="Seg_2280" n="e" s="T249">kihiŋ </ts>
               <ts e="T251" id="Seg_2282" n="e" s="T250">Lɨːpɨrdaːŋŋa </ts>
               <ts e="T252" id="Seg_2284" n="e" s="T251">barda. </ts>
               <ts e="T253" id="Seg_2286" n="e" s="T252">Lɨːpɨrdaːn </ts>
               <ts e="T254" id="Seg_2288" n="e" s="T253">kihileriger: </ts>
               <ts e="T255" id="Seg_2290" n="e" s="T254">"Bu </ts>
               <ts e="T256" id="Seg_2292" n="e" s="T255">kihini </ts>
               <ts e="T257" id="Seg_2294" n="e" s="T256">kürü͡ölü͡ökpüt, </ts>
               <ts e="T258" id="Seg_2296" n="e" s="T257">ölörü͡ökpüt", </ts>
               <ts e="T259" id="Seg_2298" n="e" s="T258">di͡en </ts>
               <ts e="T260" id="Seg_2300" n="e" s="T259">haŋarar. </ts>
               <ts e="T261" id="Seg_2302" n="e" s="T260">Kelbitiger </ts>
               <ts e="T262" id="Seg_2304" n="e" s="T261">ɨ͡aldʼɨttarɨn </ts>
               <ts e="T263" id="Seg_2306" n="e" s="T262">kürü͡öleːn </ts>
               <ts e="T264" id="Seg_2308" n="e" s="T263">kebiheller. </ts>
               <ts e="T265" id="Seg_2310" n="e" s="T264">Ölöröːrüler </ts>
               <ts e="T266" id="Seg_2312" n="e" s="T265">hubelespitter </ts>
               <ts e="T267" id="Seg_2314" n="e" s="T266">bu͡ollaga. </ts>
               <ts e="T268" id="Seg_2316" n="e" s="T267">Lɨːpɨrdaːn </ts>
               <ts e="T269" id="Seg_2318" n="e" s="T268">hallan </ts>
               <ts e="T270" id="Seg_2320" n="e" s="T269">dogottorugar </ts>
               <ts e="T271" id="Seg_2322" n="e" s="T270">haŋarar: </ts>
               <ts e="T272" id="Seg_2324" n="e" s="T271">"Bu </ts>
               <ts e="T273" id="Seg_2326" n="e" s="T272">tu͡ok </ts>
               <ts e="T274" id="Seg_2328" n="e" s="T273">bu͡olaŋŋɨt </ts>
               <ts e="T275" id="Seg_2330" n="e" s="T274">munnʼullagɨt?" </ts>
               <ts e="T276" id="Seg_2332" n="e" s="T275">ɨ͡aldʼɨt </ts>
               <ts e="T277" id="Seg_2334" n="e" s="T276">manna </ts>
               <ts e="T278" id="Seg_2336" n="e" s="T277">eter: </ts>
               <ts e="T279" id="Seg_2338" n="e" s="T278">"Ogonnʼor, </ts>
               <ts e="T280" id="Seg_2340" n="e" s="T279">en </ts>
               <ts e="T281" id="Seg_2342" n="e" s="T280">miːgin </ts>
               <ts e="T282" id="Seg_2344" n="e" s="T281">ölöröːrü </ts>
               <ts e="T283" id="Seg_2346" n="e" s="T282">gɨnargɨn </ts>
               <ts e="T284" id="Seg_2348" n="e" s="T283">bilebin. </ts>
               <ts e="T285" id="Seg_2350" n="e" s="T284">Min </ts>
               <ts e="T286" id="Seg_2352" n="e" s="T285">bu </ts>
               <ts e="T287" id="Seg_2354" n="e" s="T286">hogotok </ts>
               <ts e="T288" id="Seg_2356" n="e" s="T287">bɨhɨjalaːkpɨn, </ts>
               <ts e="T289" id="Seg_2358" n="e" s="T288">onon </ts>
               <ts e="T290" id="Seg_2360" n="e" s="T289">noru͡okkun </ts>
               <ts e="T291" id="Seg_2362" n="e" s="T290">biːrde </ts>
               <ts e="T292" id="Seg_2364" n="e" s="T291">ergiji͡em </ts>
               <ts e="T293" id="Seg_2366" n="e" s="T292">ete", </ts>
               <ts e="T294" id="Seg_2368" n="e" s="T293">diːr. </ts>
               <ts e="T295" id="Seg_2370" n="e" s="T294">"Kaja-kuː, </ts>
               <ts e="T296" id="Seg_2372" n="e" s="T295">iti </ts>
               <ts e="T297" id="Seg_2374" n="e" s="T296">kurduk </ts>
               <ts e="T298" id="Seg_2376" n="e" s="T297">gɨnɨma. </ts>
               <ts e="T299" id="Seg_2378" n="e" s="T298">Min </ts>
               <ts e="T300" id="Seg_2380" n="e" s="T299">kɨːspɨn </ts>
               <ts e="T301" id="Seg_2382" n="e" s="T300">dʼaktar </ts>
               <ts e="T302" id="Seg_2384" n="e" s="T301">gɨn", </ts>
               <ts e="T303" id="Seg_2386" n="e" s="T302">diːr. </ts>
               <ts e="T304" id="Seg_2388" n="e" s="T303">ɨ͡aldʼɨta </ts>
               <ts e="T305" id="Seg_2390" n="e" s="T304">Lɨːpɨrdaːn </ts>
               <ts e="T306" id="Seg_2392" n="e" s="T305">kɨːhɨn </ts>
               <ts e="T307" id="Seg_2394" n="e" s="T306">dʼaktar </ts>
               <ts e="T308" id="Seg_2396" n="e" s="T307">gɨna </ts>
               <ts e="T309" id="Seg_2398" n="e" s="T308">hɨtar. </ts>
               <ts e="T310" id="Seg_2400" n="e" s="T309">Bu </ts>
               <ts e="T311" id="Seg_2402" n="e" s="T310">u͡ol </ts>
               <ts e="T312" id="Seg_2404" n="e" s="T311">ulaːtan </ts>
               <ts e="T313" id="Seg_2406" n="e" s="T312">baran </ts>
               <ts e="T314" id="Seg_2408" n="e" s="T313">teːtetin </ts>
               <ts e="T315" id="Seg_2410" n="e" s="T314">ölörbüt </ts>
               <ts e="T316" id="Seg_2412" n="e" s="T315">kihini </ts>
               <ts e="T317" id="Seg_2414" n="e" s="T316">bi͡ek </ts>
               <ts e="T318" id="Seg_2416" n="e" s="T317">kördüːr </ts>
               <ts e="T319" id="Seg_2418" n="e" s="T318">ebit. </ts>
               <ts e="T320" id="Seg_2420" n="e" s="T319">Agata </ts>
               <ts e="T321" id="Seg_2422" n="e" s="T320">ölbüt </ts>
               <ts e="T322" id="Seg_2424" n="e" s="T321">hiriger </ts>
               <ts e="T323" id="Seg_2426" n="e" s="T322">ulaːtan </ts>
               <ts e="T324" id="Seg_2428" n="e" s="T323">baran </ts>
               <ts e="T325" id="Seg_2430" n="e" s="T324">bi͡ek </ts>
               <ts e="T326" id="Seg_2432" n="e" s="T325">hɨldʼar </ts>
               <ts e="T327" id="Seg_2434" n="e" s="T326">ebit. </ts>
               <ts e="T328" id="Seg_2436" n="e" s="T327">Araj </ts>
               <ts e="T329" id="Seg_2438" n="e" s="T328">bu </ts>
               <ts e="T330" id="Seg_2440" n="e" s="T329">hɨttagɨna </ts>
               <ts e="T331" id="Seg_2442" n="e" s="T330">ikki </ts>
               <ts e="T332" id="Seg_2444" n="e" s="T331">kihi </ts>
               <ts e="T333" id="Seg_2446" n="e" s="T332">iheller. </ts>
               <ts e="T334" id="Seg_2448" n="e" s="T333">Araj </ts>
               <ts e="T335" id="Seg_2450" n="e" s="T334">biːrdere </ts>
               <ts e="T336" id="Seg_2452" n="e" s="T335">toŋ </ts>
               <ts e="T337" id="Seg_2454" n="e" s="T336">ebekeni </ts>
               <ts e="T338" id="Seg_2456" n="e" s="T337">tü͡öhüger </ts>
               <ts e="T339" id="Seg_2458" n="e" s="T338">baːjan </ts>
               <ts e="T340" id="Seg_2460" n="e" s="T339">iher. </ts>
               <ts e="T341" id="Seg_2462" n="e" s="T340">Manɨ͡aka </ts>
               <ts e="T342" id="Seg_2464" n="e" s="T341">dogoro </ts>
               <ts e="T343" id="Seg_2466" n="e" s="T342">diːr: </ts>
               <ts e="T344" id="Seg_2468" n="e" s="T343">"Manna, </ts>
               <ts e="T345" id="Seg_2470" n="e" s="T344">kaja, </ts>
               <ts e="T346" id="Seg_2472" n="e" s="T345">Lɨːpɨrdaːn </ts>
               <ts e="T347" id="Seg_2474" n="e" s="T346">kömüs </ts>
               <ts e="T348" id="Seg_2476" n="e" s="T347">ü͡östeːk </ts>
               <ts e="T349" id="Seg_2478" n="e" s="T348">haːlaːk </ts>
               <ts e="T350" id="Seg_2480" n="e" s="T349">kihini </ts>
               <ts e="T351" id="Seg_2482" n="e" s="T350">ölörbüte. </ts>
               <ts e="T352" id="Seg_2484" n="e" s="T351">Baːr </ts>
               <ts e="T353" id="Seg_2486" n="e" s="T352">ol </ts>
               <ts e="T354" id="Seg_2488" n="e" s="T353">kihi </ts>
               <ts e="T355" id="Seg_2490" n="e" s="T354">u͡ola", </ts>
               <ts e="T356" id="Seg_2492" n="e" s="T355">diːr. </ts>
               <ts e="T357" id="Seg_2494" n="e" s="T356">U͡ol </ts>
               <ts e="T358" id="Seg_2496" n="e" s="T357">onno </ts>
               <ts e="T359" id="Seg_2498" n="e" s="T358">dʼe </ts>
               <ts e="T360" id="Seg_2500" n="e" s="T359">agalaːgɨn </ts>
               <ts e="T361" id="Seg_2502" n="e" s="T360">ister. </ts>
               <ts e="T362" id="Seg_2504" n="e" s="T361">Bu </ts>
               <ts e="T363" id="Seg_2506" n="e" s="T362">kihilergin </ts>
               <ts e="T364" id="Seg_2508" n="e" s="T363">dʼe </ts>
               <ts e="T365" id="Seg_2510" n="e" s="T364">ü͡ögülüːr </ts>
               <ts e="T366" id="Seg_2512" n="e" s="T365">diː </ts>
               <ts e="T367" id="Seg_2514" n="e" s="T366">bu </ts>
               <ts e="T368" id="Seg_2516" n="e" s="T367">ogoŋ. </ts>
               <ts e="T369" id="Seg_2518" n="e" s="T368">"Hirbit </ts>
               <ts e="T370" id="Seg_2520" n="e" s="T369">kuttuːr" </ts>
               <ts e="T371" id="Seg_2522" n="e" s="T370">di͡en </ts>
               <ts e="T372" id="Seg_2524" n="e" s="T371">manna </ts>
               <ts e="T373" id="Seg_2526" n="e" s="T372">ebekennerin </ts>
               <ts e="T374" id="Seg_2528" n="e" s="T373">kaːllara </ts>
               <ts e="T375" id="Seg_2530" n="e" s="T374">ku͡otallar. </ts>
               <ts e="T376" id="Seg_2532" n="e" s="T375">U͡ol </ts>
               <ts e="T377" id="Seg_2534" n="e" s="T376">ebekeni </ts>
               <ts e="T378" id="Seg_2536" n="e" s="T377">kɨtta </ts>
               <ts e="T379" id="Seg_2538" n="e" s="T378">bölüːgetin </ts>
               <ts e="T380" id="Seg_2540" n="e" s="T379">dʼi͡etiger </ts>
               <ts e="T381" id="Seg_2542" n="e" s="T380">ijetiger </ts>
               <ts e="T382" id="Seg_2544" n="e" s="T381">ildʼe </ts>
               <ts e="T383" id="Seg_2546" n="e" s="T382">barar. </ts>
               <ts e="T384" id="Seg_2548" n="e" s="T383">Bularɨ </ts>
               <ts e="T385" id="Seg_2550" n="e" s="T384">hiːller </ts>
               <ts e="T386" id="Seg_2552" n="e" s="T385">bu͡ollaga. </ts>
               <ts e="T387" id="Seg_2554" n="e" s="T386">Ijete </ts>
               <ts e="T388" id="Seg_2556" n="e" s="T387">hemeliːr: </ts>
               <ts e="T389" id="Seg_2558" n="e" s="T388">"Bu </ts>
               <ts e="T390" id="Seg_2560" n="e" s="T389">tu͡ok </ts>
               <ts e="T391" id="Seg_2562" n="e" s="T390">ör </ts>
               <ts e="T392" id="Seg_2564" n="e" s="T391">hɨldʼagɨn? </ts>
               <ts e="T393" id="Seg_2566" n="e" s="T392">Ölörön </ts>
               <ts e="T394" id="Seg_2568" n="e" s="T393">keːhi͡ektere." </ts>
               <ts e="T395" id="Seg_2570" n="e" s="T394">U͡ola </ts>
               <ts e="T396" id="Seg_2572" n="e" s="T395">ijetin </ts>
               <ts e="T397" id="Seg_2574" n="e" s="T396">haŋatɨn </ts>
               <ts e="T398" id="Seg_2576" n="e" s="T397">amattan </ts>
               <ts e="T399" id="Seg_2578" n="e" s="T398">istibet. </ts>
               <ts e="T400" id="Seg_2580" n="e" s="T399">Bu </ts>
               <ts e="T401" id="Seg_2582" n="e" s="T400">u͡ol </ts>
               <ts e="T402" id="Seg_2584" n="e" s="T401">bi͡ek </ts>
               <ts e="T403" id="Seg_2586" n="e" s="T402">agatɨn </ts>
               <ts e="T404" id="Seg_2588" n="e" s="T403">kördüːr. </ts>
               <ts e="T405" id="Seg_2590" n="e" s="T404">Bu </ts>
               <ts e="T406" id="Seg_2592" n="e" s="T405">baran </ts>
               <ts e="T407" id="Seg_2594" n="e" s="T406">Lɨːpɨrdaːn </ts>
               <ts e="T408" id="Seg_2596" n="e" s="T407">ogonnʼor </ts>
               <ts e="T409" id="Seg_2598" n="e" s="T408">dʼi͡etin </ts>
               <ts e="T410" id="Seg_2600" n="e" s="T409">dʼe </ts>
               <ts e="T411" id="Seg_2602" n="e" s="T410">bular. </ts>
               <ts e="T412" id="Seg_2604" n="e" s="T411">Bu </ts>
               <ts e="T413" id="Seg_2606" n="e" s="T412">bulan, </ts>
               <ts e="T414" id="Seg_2608" n="e" s="T413">ki͡eheːŋŋi </ts>
               <ts e="T415" id="Seg_2610" n="e" s="T414">uːnnan </ts>
               <ts e="T416" id="Seg_2612" n="e" s="T415">utuja </ts>
               <ts e="T417" id="Seg_2614" n="e" s="T416">hɨttaktarɨna, </ts>
               <ts e="T418" id="Seg_2616" n="e" s="T417">keler. </ts>
               <ts e="T419" id="Seg_2618" n="e" s="T418">Utuja </ts>
               <ts e="T420" id="Seg_2620" n="e" s="T419">hɨtar </ts>
               <ts e="T421" id="Seg_2622" n="e" s="T420">kihini </ts>
               <ts e="T422" id="Seg_2624" n="e" s="T421">kustugunan </ts>
               <ts e="T423" id="Seg_2626" n="e" s="T422">hu͡on </ts>
               <ts e="T424" id="Seg_2628" n="e" s="T423">iŋiːrderin </ts>
               <ts e="T425" id="Seg_2630" n="e" s="T424">bɨhɨta </ts>
               <ts e="T426" id="Seg_2632" n="e" s="T425">ɨtɨ͡alɨːr, </ts>
               <ts e="T427" id="Seg_2634" n="e" s="T426">Lɨːpɨrdaːn </ts>
               <ts e="T428" id="Seg_2636" n="e" s="T427">kütü͡ötün. </ts>
               <ts e="T429" id="Seg_2638" n="e" s="T428">"Dʼe </ts>
               <ts e="T430" id="Seg_2640" n="e" s="T429">bu </ts>
               <ts e="T431" id="Seg_2642" n="e" s="T430">kɨnnɨm </ts>
               <ts e="T432" id="Seg_2644" n="e" s="T431">kuhaganɨgar </ts>
               <ts e="T433" id="Seg_2646" n="e" s="T432">ölön </ts>
               <ts e="T434" id="Seg_2648" n="e" s="T433">erebin. </ts>
               <ts e="T435" id="Seg_2650" n="e" s="T434">Min </ts>
               <ts e="T436" id="Seg_2652" n="e" s="T435">en </ts>
               <ts e="T437" id="Seg_2654" n="e" s="T436">agatɨn </ts>
               <ts e="T438" id="Seg_2656" n="e" s="T437">ölörbötögüm", </ts>
               <ts e="T439" id="Seg_2658" n="e" s="T438">diːr. </ts>
               <ts e="T440" id="Seg_2660" n="e" s="T439">"Ajɨlaːgɨn </ts>
               <ts e="T441" id="Seg_2662" n="e" s="T440">miːgin </ts>
               <ts e="T442" id="Seg_2664" n="e" s="T441">tɨːmmɨn </ts>
               <ts e="T443" id="Seg_2666" n="e" s="T442">bɨs. </ts>
               <ts e="T444" id="Seg_2668" n="e" s="T443">Anɨ </ts>
               <ts e="T445" id="Seg_2670" n="e" s="T444">da </ts>
               <ts e="T446" id="Seg_2672" n="e" s="T445">tu͡ok </ts>
               <ts e="T447" id="Seg_2674" n="e" s="T446">kihite </ts>
               <ts e="T448" id="Seg_2676" n="e" s="T447">bu͡olu͡omuj", </ts>
               <ts e="T449" id="Seg_2678" n="e" s="T448">diːr. </ts>
               <ts e="T450" id="Seg_2680" n="e" s="T449">Bu </ts>
               <ts e="T451" id="Seg_2682" n="e" s="T450">ogoŋ </ts>
               <ts e="T452" id="Seg_2684" n="e" s="T451">bu </ts>
               <ts e="T453" id="Seg_2686" n="e" s="T452">kihini </ts>
               <ts e="T454" id="Seg_2688" n="e" s="T453">tɨːnɨn </ts>
               <ts e="T455" id="Seg_2690" n="e" s="T454">bɨhan </ts>
               <ts e="T456" id="Seg_2692" n="e" s="T455">keːher. </ts>
               <ts e="T457" id="Seg_2694" n="e" s="T456">Bu </ts>
               <ts e="T458" id="Seg_2696" n="e" s="T457">ogoŋ </ts>
               <ts e="T459" id="Seg_2698" n="e" s="T458">manɨ </ts>
               <ts e="T460" id="Seg_2700" n="e" s="T459">ister </ts>
               <ts e="T461" id="Seg_2702" n="e" s="T460">da, </ts>
               <ts e="T462" id="Seg_2704" n="e" s="T461">Lɨːpɨrdaːnɨ </ts>
               <ts e="T463" id="Seg_2706" n="e" s="T462">ölörön </ts>
               <ts e="T464" id="Seg_2708" n="e" s="T463">keːher. </ts>
               <ts e="T465" id="Seg_2710" n="e" s="T464">Baːjɨn-totun </ts>
               <ts e="T466" id="Seg_2712" n="e" s="T465">dʼi͡etiger </ts>
               <ts e="T467" id="Seg_2714" n="e" s="T466">ildʼe </ts>
               <ts e="T468" id="Seg_2716" n="e" s="T467">barar. </ts>
               <ts e="T469" id="Seg_2718" n="e" s="T468">Bu </ts>
               <ts e="T470" id="Seg_2720" n="e" s="T469">baːjgɨn </ts>
               <ts e="T471" id="Seg_2722" n="e" s="T470">dʼi͡etiger </ts>
               <ts e="T472" id="Seg_2724" n="e" s="T471">ti͡erden </ts>
               <ts e="T473" id="Seg_2726" n="e" s="T472">ijetin </ts>
               <ts e="T474" id="Seg_2728" n="e" s="T473">kɨtta </ts>
               <ts e="T475" id="Seg_2730" n="e" s="T474">bajan-tajan </ts>
               <ts e="T476" id="Seg_2732" n="e" s="T475">dʼe </ts>
               <ts e="T477" id="Seg_2734" n="e" s="T476">olorbuta. </ts>
               <ts e="T478" id="Seg_2736" n="e" s="T477">Elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_2737" s="T0">ChuAE_1968_Lyypyrdaan_flk.001 (001.001)</ta>
            <ta e="T8" id="Seg_2738" s="T3">ChuAE_1968_Lyypyrdaan_flk.002 (001.002)</ta>
            <ta e="T13" id="Seg_2739" s="T8">ChuAE_1968_Lyypyrdaan_flk.003 (001.003)</ta>
            <ta e="T16" id="Seg_2740" s="T13">ChuAE_1968_Lyypyrdaan_flk.004 (001.004)</ta>
            <ta e="T21" id="Seg_2741" s="T16">ChuAE_1968_Lyypyrdaan_flk.005 (001.005)</ta>
            <ta e="T27" id="Seg_2742" s="T21">ChuAE_1968_Lyypyrdaan_flk.006 (001.006)</ta>
            <ta e="T35" id="Seg_2743" s="T27">ChuAE_1968_Lyypyrdaan_flk.007 (001.007)</ta>
            <ta e="T37" id="Seg_2744" s="T35">ChuAE_1968_Lyypyrdaan_flk.008 (001.008)</ta>
            <ta e="T43" id="Seg_2745" s="T37">ChuAE_1968_Lyypyrdaan_flk.009 (001.008)</ta>
            <ta e="T49" id="Seg_2746" s="T43">ChuAE_1968_Lyypyrdaan_flk.010 (001.009)</ta>
            <ta e="T62" id="Seg_2747" s="T49">ChuAE_1968_Lyypyrdaan_flk.011 (001.011)</ta>
            <ta e="T70" id="Seg_2748" s="T62">ChuAE_1968_Lyypyrdaan_flk.012 (001.012)</ta>
            <ta e="T77" id="Seg_2749" s="T70">ChuAE_1968_Lyypyrdaan_flk.013 (001.013)</ta>
            <ta e="T83" id="Seg_2750" s="T77">ChuAE_1968_Lyypyrdaan_flk.014 (001.014)</ta>
            <ta e="T90" id="Seg_2751" s="T83">ChuAE_1968_Lyypyrdaan_flk.015 (001.015)</ta>
            <ta e="T93" id="Seg_2752" s="T90">ChuAE_1968_Lyypyrdaan_flk.016 (001.016)</ta>
            <ta e="T100" id="Seg_2753" s="T93">ChuAE_1968_Lyypyrdaan_flk.017 (001.017)</ta>
            <ta e="T112" id="Seg_2754" s="T100">ChuAE_1968_Lyypyrdaan_flk.018 (001.018)</ta>
            <ta e="T118" id="Seg_2755" s="T112">ChuAE_1968_Lyypyrdaan_flk.019 (001.019)</ta>
            <ta e="T127" id="Seg_2756" s="T118">ChuAE_1968_Lyypyrdaan_flk.020 (001.020)</ta>
            <ta e="T135" id="Seg_2757" s="T127">ChuAE_1968_Lyypyrdaan_flk.021 (001.021)</ta>
            <ta e="T140" id="Seg_2758" s="T135">ChuAE_1968_Lyypyrdaan_flk.022 (001.022)</ta>
            <ta e="T143" id="Seg_2759" s="T140">ChuAE_1968_Lyypyrdaan_flk.023 (001.022)</ta>
            <ta e="T149" id="Seg_2760" s="T143">ChuAE_1968_Lyypyrdaan_flk.024 (001.023)</ta>
            <ta e="T154" id="Seg_2761" s="T149">ChuAE_1968_Lyypyrdaan_flk.025 (001.024)</ta>
            <ta e="T158" id="Seg_2762" s="T154">ChuAE_1968_Lyypyrdaan_flk.026 (001.025)</ta>
            <ta e="T165" id="Seg_2763" s="T158">ChuAE_1968_Lyypyrdaan_flk.027 (001.026)</ta>
            <ta e="T172" id="Seg_2764" s="T165">ChuAE_1968_Lyypyrdaan_flk.028 (001.027)</ta>
            <ta e="T174" id="Seg_2765" s="T172">ChuAE_1968_Lyypyrdaan_flk.029 (001.028)</ta>
            <ta e="T177" id="Seg_2766" s="T174">ChuAE_1968_Lyypyrdaan_flk.030 (001.029)</ta>
            <ta e="T179" id="Seg_2767" s="T177">ChuAE_1968_Lyypyrdaan_flk.031 (001.030)</ta>
            <ta e="T180" id="Seg_2768" s="T179">ChuAE_1968_Lyypyrdaan_flk.032 (001.031)</ta>
            <ta e="T182" id="Seg_2769" s="T180">ChuAE_1968_Lyypyrdaan_flk.033 (001.032)</ta>
            <ta e="T188" id="Seg_2770" s="T182">ChuAE_1968_Lyypyrdaan_flk.034 (001.033)</ta>
            <ta e="T197" id="Seg_2771" s="T188">ChuAE_1968_Lyypyrdaan_flk.035 (001.034)</ta>
            <ta e="T204" id="Seg_2772" s="T197">ChuAE_1968_Lyypyrdaan_flk.036 (001.035)</ta>
            <ta e="T209" id="Seg_2773" s="T204">ChuAE_1968_Lyypyrdaan_flk.037 (001.036)</ta>
            <ta e="T211" id="Seg_2774" s="T209">ChuAE_1968_Lyypyrdaan_flk.038 (001.037)</ta>
            <ta e="T216" id="Seg_2775" s="T211">ChuAE_1968_Lyypyrdaan_flk.039 (001.038)</ta>
            <ta e="T223" id="Seg_2776" s="T216">ChuAE_1968_Lyypyrdaan_flk.040 (001.039)</ta>
            <ta e="T227" id="Seg_2777" s="T223">ChuAE_1968_Lyypyrdaan_flk.041 (001.040)</ta>
            <ta e="T229" id="Seg_2778" s="T227">ChuAE_1968_Lyypyrdaan_flk.042 (001.041)</ta>
            <ta e="T233" id="Seg_2779" s="T229">ChuAE_1968_Lyypyrdaan_flk.043 (001.042)</ta>
            <ta e="T236" id="Seg_2780" s="T233">ChuAE_1968_Lyypyrdaan_flk.044 (001.043)</ta>
            <ta e="T244" id="Seg_2781" s="T236">ChuAE_1968_Lyypyrdaan_flk.045 (001.044)</ta>
            <ta e="T246" id="Seg_2782" s="T244">ChuAE_1968_Lyypyrdaan_flk.046 (001.045)</ta>
            <ta e="T252" id="Seg_2783" s="T246">ChuAE_1968_Lyypyrdaan_flk.047 (001.046)</ta>
            <ta e="T254" id="Seg_2784" s="T252">ChuAE_1968_Lyypyrdaan_flk.048 (001.047)</ta>
            <ta e="T260" id="Seg_2785" s="T254">ChuAE_1968_Lyypyrdaan_flk.049 (001.048)</ta>
            <ta e="T264" id="Seg_2786" s="T260">ChuAE_1968_Lyypyrdaan_flk.050 (001.049)</ta>
            <ta e="T267" id="Seg_2787" s="T264">ChuAE_1968_Lyypyrdaan_flk.051 (001.050)</ta>
            <ta e="T271" id="Seg_2788" s="T267">ChuAE_1968_Lyypyrdaan_flk.052 (001.051)</ta>
            <ta e="T275" id="Seg_2789" s="T271">ChuAE_1968_Lyypyrdaan_flk.053 (001.052)</ta>
            <ta e="T278" id="Seg_2790" s="T275">ChuAE_1968_Lyypyrdaan_flk.054 (001.053)</ta>
            <ta e="T284" id="Seg_2791" s="T278">ChuAE_1968_Lyypyrdaan_flk.055 (001.054)</ta>
            <ta e="T294" id="Seg_2792" s="T284">ChuAE_1968_Lyypyrdaan_flk.056 (001.055)</ta>
            <ta e="T298" id="Seg_2793" s="T294">ChuAE_1968_Lyypyrdaan_flk.057 (001.056)</ta>
            <ta e="T303" id="Seg_2794" s="T298">ChuAE_1968_Lyypyrdaan_flk.058 (001.057)</ta>
            <ta e="T309" id="Seg_2795" s="T303">ChuAE_1968_Lyypyrdaan_flk.059 (001.058)</ta>
            <ta e="T319" id="Seg_2796" s="T309">ChuAE_1968_Lyypyrdaan_flk.060 (001.059)</ta>
            <ta e="T327" id="Seg_2797" s="T319">ChuAE_1968_Lyypyrdaan_flk.061 (001.060)</ta>
            <ta e="T333" id="Seg_2798" s="T327">ChuAE_1968_Lyypyrdaan_flk.062 (001.061)</ta>
            <ta e="T340" id="Seg_2799" s="T333">ChuAE_1968_Lyypyrdaan_flk.063 (001.062)</ta>
            <ta e="T343" id="Seg_2800" s="T340">ChuAE_1968_Lyypyrdaan_flk.064 (001.063)</ta>
            <ta e="T351" id="Seg_2801" s="T343">ChuAE_1968_Lyypyrdaan_flk.065 (001.064)</ta>
            <ta e="T356" id="Seg_2802" s="T351">ChuAE_1968_Lyypyrdaan_flk.066 (001.065)</ta>
            <ta e="T361" id="Seg_2803" s="T356">ChuAE_1968_Lyypyrdaan_flk.067 (001.066)</ta>
            <ta e="T368" id="Seg_2804" s="T361">ChuAE_1968_Lyypyrdaan_flk.068 (001.067)</ta>
            <ta e="T375" id="Seg_2805" s="T368">ChuAE_1968_Lyypyrdaan_flk.069 (001.068)</ta>
            <ta e="T383" id="Seg_2806" s="T375">ChuAE_1968_Lyypyrdaan_flk.070 (001.069)</ta>
            <ta e="T386" id="Seg_2807" s="T383">ChuAE_1968_Lyypyrdaan_flk.071 (001.070)</ta>
            <ta e="T388" id="Seg_2808" s="T386">ChuAE_1968_Lyypyrdaan_flk.072 (001.071)</ta>
            <ta e="T392" id="Seg_2809" s="T388">ChuAE_1968_Lyypyrdaan_flk.073 (001.072)</ta>
            <ta e="T394" id="Seg_2810" s="T392">ChuAE_1968_Lyypyrdaan_flk.074 (001.073)</ta>
            <ta e="T399" id="Seg_2811" s="T394">ChuAE_1968_Lyypyrdaan_flk.075 (001.074)</ta>
            <ta e="T404" id="Seg_2812" s="T399">ChuAE_1968_Lyypyrdaan_flk.076 (001.075)</ta>
            <ta e="T411" id="Seg_2813" s="T404">ChuAE_1968_Lyypyrdaan_flk.077 (001.076)</ta>
            <ta e="T418" id="Seg_2814" s="T411">ChuAE_1968_Lyypyrdaan_flk.078 (001.077)</ta>
            <ta e="T428" id="Seg_2815" s="T418">ChuAE_1968_Lyypyrdaan_flk.079 (001.078)</ta>
            <ta e="T434" id="Seg_2816" s="T428">ChuAE_1968_Lyypyrdaan_flk.080 (001.079)</ta>
            <ta e="T439" id="Seg_2817" s="T434">ChuAE_1968_Lyypyrdaan_flk.081 (001.080)</ta>
            <ta e="T443" id="Seg_2818" s="T439">ChuAE_1968_Lyypyrdaan_flk.082 (001.081)</ta>
            <ta e="T449" id="Seg_2819" s="T443">ChuAE_1968_Lyypyrdaan_flk.083 (001.082)</ta>
            <ta e="T456" id="Seg_2820" s="T449">ChuAE_1968_Lyypyrdaan_flk.084 (001.084)</ta>
            <ta e="T464" id="Seg_2821" s="T456">ChuAE_1968_Lyypyrdaan_flk.085 (001.085)</ta>
            <ta e="T468" id="Seg_2822" s="T464">ChuAE_1968_Lyypyrdaan_flk.086 (001.086)</ta>
            <ta e="T477" id="Seg_2823" s="T468">ChuAE_1968_Lyypyrdaan_flk.087 (001.087)</ta>
            <ta e="T478" id="Seg_2824" s="T477">ChuAE_1968_Lyypyrdaan_flk.088 (001.088)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_2825" s="T0">Лыыпырдаан дьэ олорор.</ta>
            <ta e="T8" id="Seg_2826" s="T3">Лыыпырдаан кайа эрэ диэкки һылдьар.</ta>
            <ta e="T13" id="Seg_2827" s="T8">Бу һылдьан дьактардаак киһини булар.</ta>
            <ta e="T16" id="Seg_2828" s="T13">Уоллаак ол киһи.</ta>
            <ta e="T21" id="Seg_2829" s="T16">Дьактардаак киһиҥ туспа көспүт, һоготогун.</ta>
            <ta e="T27" id="Seg_2830" s="T21">Караҥа буолан киэһэ биир һиргэ түспүт.</ta>
            <ta e="T35" id="Seg_2831" s="T27">Утуйаары атагын һыгынньактанан эрдэгинэ уота каста да эппит.</ta>
            <ta e="T37" id="Seg_2832" s="T35">Бу киһиҥ: </ta>
            <ta e="T43" id="Seg_2833" s="T37">"Мин күнүс һылдьыбытым дии, туок кэлиэй?</ta>
            <ta e="T49" id="Seg_2834" s="T43">Уотум того этэр?" — диэн куттаммат, һытар.</ta>
            <ta e="T62" id="Seg_2835" s="T49">Лыыпырдаан кэлэн бу көмүс үөстээк һаалаак киһини өлөрөн кээһэр, алаҥаа һаа буоллага дии.</ta>
            <ta e="T70" id="Seg_2836" s="T62">Киһин дьактара уччугуй оготун кытта һиргэ күрээн каалар.</ta>
            <ta e="T77" id="Seg_2837" s="T70">Дьактарыҥ туга да һуок, буор голомокооҥҥо олорор.</ta>
            <ta e="T83" id="Seg_2838" s="T77">Олоҥко огото өр буолбат — улаатан каалар.</ta>
            <ta e="T90" id="Seg_2839" s="T83">Ого таксан көрбүтэ, дьиэтин үрдүтүгэр чээлкээлэр һылдьаллар.</ta>
            <ta e="T93" id="Seg_2840" s="T90">Ийэтигэр киирэн кэпсиир:</ta>
            <ta e="T100" id="Seg_2841" s="T93">— Ээ, оччого куруппааскылар буоллага дии, — диир ийэтэ.</ta>
            <ta e="T112" id="Seg_2842" s="T100">— Оот, агалаагым эбитэ буоллар кайдак эрэ гыныа этэ буоллага, — диир уол ийэтигэр.</ta>
            <ta e="T118" id="Seg_2843" s="T112">Ийэтэ уол агалаагын туһунан аматтан кэпсээбэт.</ta>
            <ta e="T127" id="Seg_2844" s="T118">Ийэ аһын быһан муот оҥорор, уол манан туһак оҥостор.</ta>
            <ta e="T135" id="Seg_2845" s="T127">Манан куруппааскыны өлөрөн, онон ииттинэн киһи буолан олороллор.</ta>
            <ta e="T140" id="Seg_2846" s="T135">Бу олорон уол биэк ыйытар: </ta>
            <ta e="T143" id="Seg_2847" s="T140">"Мин тээтэлээк этиэм?" </ta>
            <ta e="T149" id="Seg_2848" s="T143">Ийэтэ аматтан һаҥарбат эбит, кистиир эбит.</ta>
            <ta e="T154" id="Seg_2849" s="T149">Лыыпырдаан эмиэ киһини өлөрөөрү һылдьар.</ta>
            <ta e="T158" id="Seg_2850" s="T154">Биир уоллаак эмээксиҥҥэ кэлэр.</ta>
            <ta e="T165" id="Seg_2851" s="T158">Арай көрбүтэ, һүрдээк һуон тиит мас турар.</ta>
            <ta e="T172" id="Seg_2852" s="T165">Бу маска эмээксин уола һаатын иилэн кээспит.</ta>
            <ta e="T174" id="Seg_2853" s="T172">Лыыпырдаан ыйытар:</ta>
            <ta e="T177" id="Seg_2854" s="T174">— Ити ким һаатай?</ta>
            <ta e="T179" id="Seg_2855" s="T177">Туок кэргэннээккин?</ta>
            <ta e="T180" id="Seg_2856" s="T179">Эмээксин:</ta>
            <ta e="T182" id="Seg_2857" s="T180">— Уоллаакпын, — диир.</ta>
            <ta e="T188" id="Seg_2858" s="T182">— Бээ, ити һаатын того кээспитэй — кээ?</ta>
            <ta e="T197" id="Seg_2859" s="T188">— Ити һаа күүһэ таксыбыт, ол иһин кээспитэ, — диир эмээксин.</ta>
            <ta e="T204" id="Seg_2860" s="T197">Онтон болдок һытар, улакан багайы таас-камень.</ta>
            <ta e="T209" id="Seg_2861" s="T204">Маны эмиэ ыйытар били киһи.</ta>
            <ta e="T211" id="Seg_2862" s="T209">Эмээксин ону:</ta>
            <ta e="T216" id="Seg_2863" s="T211">— Ити чуҥкуйдагына мээчик гынааччы, — диир.</ta>
            <ta e="T223" id="Seg_2864" s="T216">— Ол кайага диэри тэптэгинэ төттөрү чэкинийэн кэлээччи.</ta>
            <ta e="T227" id="Seg_2865" s="T223">Ол курдук оонньооччу, — диир.</ta>
            <ta e="T229" id="Seg_2866" s="T227">Лыыпырдаан этэр:</ta>
            <ta e="T233" id="Seg_2867" s="T229">— һарсын мин төрөөбүт күннэнэбин.</ta>
            <ta e="T236" id="Seg_2868" s="T233">Онно уолуҥ кэлиэктин.</ta>
            <ta e="T244" id="Seg_2869" s="T236">Бу уол дьиэтигэр кэлбитигэр ийэтэ барытын кэпсээн биэрэр,</ta>
            <ta e="T246" id="Seg_2870" s="T244">— Ыҥыраллар, — диир.</ta>
            <ta e="T252" id="Seg_2871" s="T246">Дьэ арай бу киһиҥ Лыыпырдааҥҥа барда.</ta>
            <ta e="T254" id="Seg_2872" s="T252">Лыыпырдаан киһилэригэр:</ta>
            <ta e="T260" id="Seg_2873" s="T254">— Бу киһини күрүөлүөкпүт, өлөрүөкпүт, — диэн һаҥарар.</ta>
            <ta e="T264" id="Seg_2874" s="T260">Кэлбитигэр ыалдьыттарын күрүөлээн кэбиһэллэр.</ta>
            <ta e="T267" id="Seg_2875" s="T264">Өлөрөөрүлэр һубэлэспиттэр буоллага.</ta>
            <ta e="T271" id="Seg_2876" s="T267">Лыыпырдаан һаллан доготторугар һаҥарар:</ta>
            <ta e="T275" id="Seg_2877" s="T271">— Бу туок буолаҥҥыт мунньуллагыт?</ta>
            <ta e="T278" id="Seg_2878" s="T275">Ыалдьыт манна этэр:</ta>
            <ta e="T284" id="Seg_2879" s="T278">— Огонньор, эн миигин өлөрөөрү гынаргын билэбин.</ta>
            <ta e="T294" id="Seg_2880" s="T284">Мин бу һоготок быһыйалаакпын, онон норуоккун биирдэ эргийиэм этэ, — диир.</ta>
            <ta e="T298" id="Seg_2881" s="T294">— Кайа-куу, ити курдук гыныма.</ta>
            <ta e="T303" id="Seg_2882" s="T298">Мин кыыспын дьактар гын, — диир.</ta>
            <ta e="T309" id="Seg_2883" s="T303">Ыалдьыта Лыыпырдаан кыыһын дьактар гына һытар.</ta>
            <ta e="T319" id="Seg_2884" s="T309">Бу уол улаатан баран тээтэтин өлөрбүт киһини биэк көрдүүр эбит.</ta>
            <ta e="T327" id="Seg_2885" s="T319">Агата өлбүт һиригэр улаатан баран биэк һылдьар эбит.</ta>
            <ta e="T333" id="Seg_2886" s="T327">Арай бу һыттагына икки киһи иһэллэр.</ta>
            <ta e="T340" id="Seg_2887" s="T333">Арай биирдэрэ тоҥ эбэкэни түөһүгэр баайан иһэр.</ta>
            <ta e="T343" id="Seg_2888" s="T340">Маныака догоро диир:</ta>
            <ta e="T351" id="Seg_2889" s="T343">— Манна, кайа, Лыыпырдаан көмүс үөстээк һаалаак киһини өлөрбүтэ.</ta>
            <ta e="T356" id="Seg_2890" s="T351">Баар ол киһи уола, — диир.</ta>
            <ta e="T361" id="Seg_2891" s="T356">Уол онно дьэ агалаагын истэр.</ta>
            <ta e="T368" id="Seg_2892" s="T361">Бу киһилэргин дьэ үөгүлүүр дии бу огоҥ.</ta>
            <ta e="T375" id="Seg_2893" s="T368">"Һирбит куттуур" диэн манна эбэкэннэрин кааллара куоталлар.</ta>
            <ta e="T383" id="Seg_2894" s="T375">Уол эбэкэни кытта бөлүүгэтин дьиэтигэр ийэтигэр илдьэ барар.</ta>
            <ta e="T386" id="Seg_2895" s="T383">Булары һииллэр буоллага.</ta>
            <ta e="T388" id="Seg_2896" s="T386">Ийэтэ һэмэлиир:</ta>
            <ta e="T392" id="Seg_2897" s="T388">— Бу туок өр һылдьагын?</ta>
            <ta e="T394" id="Seg_2898" s="T392">Өлөрөн кээһиэктэрэ.</ta>
            <ta e="T399" id="Seg_2899" s="T394">Уола ийэтин һаҥатын аматтан истибэт.</ta>
            <ta e="T404" id="Seg_2900" s="T399">Бу уол биэк агатын көрдүүр.</ta>
            <ta e="T411" id="Seg_2901" s="T404">Бу баран Лыыпырдаан огонньор дьиэтин дьэ булар.</ta>
            <ta e="T418" id="Seg_2902" s="T411">Бу булан, киэһээҥҥи ууннан утуйа һыттактарына, кэлэр.</ta>
            <ta e="T428" id="Seg_2903" s="T418">Утуйа һытар киһини кустугунан һуон иҥиирдэрин быһыта ытыалыыр, Лыыпырдаан күтүөтүн.</ta>
            <ta e="T434" id="Seg_2904" s="T428">— Дьэ бу кынным куһаганыгар өлөн эрэбин.</ta>
            <ta e="T439" id="Seg_2905" s="T434">Мин эн агатын өлөрбөтөгүм, — диир.</ta>
            <ta e="T443" id="Seg_2906" s="T439">— Айылаагын миигин тыыммын быс.</ta>
            <ta e="T449" id="Seg_2907" s="T443">Аны да туок киһитэ буолуомуй?! — диир.</ta>
            <ta e="T456" id="Seg_2908" s="T449">Бу огоҥ бу киһини тыынын быһан кээһэр.</ta>
            <ta e="T464" id="Seg_2909" s="T456">Бу огоҥ маны истэр да, Лыыпырдааны өлөрөн кээһэр.</ta>
            <ta e="T468" id="Seg_2910" s="T464">Баайын-тотун дьиэтигэр илдьэ барар.</ta>
            <ta e="T477" id="Seg_2911" s="T468">Бу баайгын дьиэтигэр тиэрдэн ийэтин кытта байан-тайан дьэ олорбута.</ta>
            <ta e="T478" id="Seg_2912" s="T477">Элэтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_2913" s="T0">Lɨːpɨrdaːn dʼe oloror. </ta>
            <ta e="T8" id="Seg_2914" s="T3">Lɨːpɨrdaːn kaja ere di͡ekki hɨldʼar. </ta>
            <ta e="T13" id="Seg_2915" s="T8">Bu hɨldʼan dʼaktardaːk kihini bular. </ta>
            <ta e="T16" id="Seg_2916" s="T13">U͡ollaːk ol kihi. </ta>
            <ta e="T21" id="Seg_2917" s="T16">Dʼaktardaːk kihiŋ tuspa köspüt, hogotogun. </ta>
            <ta e="T27" id="Seg_2918" s="T21">Karaŋa bu͡olan ki͡ehe biːr hirge tüspüt. </ta>
            <ta e="T35" id="Seg_2919" s="T27">Utujaːrɨ atagɨn hɨgɨnnʼaktanan erdegine u͡ota kasta da eppit. </ta>
            <ta e="T37" id="Seg_2920" s="T35">Bu kihiŋ: </ta>
            <ta e="T43" id="Seg_2921" s="T37">"Min künüs hɨldʼɨbɨtɨm diː, tu͡ok keli͡ej? </ta>
            <ta e="T49" id="Seg_2922" s="T43">U͡otum togo eter", di͡en kuttammat, hɨtar. </ta>
            <ta e="T62" id="Seg_2923" s="T49">Lɨːpɨrdaːn kelen bu kömüs ü͡östeːk haːlaːk kihini ölörön keːher, alaŋaː haː bu͡ollaga diː. </ta>
            <ta e="T70" id="Seg_2924" s="T62">Kihin dʼaktara uččuguj ogotun kɨtta hirge küreːn kaːlar. </ta>
            <ta e="T77" id="Seg_2925" s="T70">Dʼaktarɨŋ tuga da hu͡ok, bu͡or golomokoːŋŋo oloror. </ta>
            <ta e="T83" id="Seg_2926" s="T77">Oloŋko ogoto ör bu͡olbat — ulaːtan kaːlar. </ta>
            <ta e="T90" id="Seg_2927" s="T83">Ogo taksan körbüte, dʼi͡etin ürdütüger čeːlkeːler hɨldʼallar. </ta>
            <ta e="T93" id="Seg_2928" s="T90">Ijetiger kiːren kepsiːr: </ta>
            <ta e="T100" id="Seg_2929" s="T93">"Eː, oččogo kuruppaːskɨlar bu͡ollaga diː", diːr ijete. </ta>
            <ta e="T112" id="Seg_2930" s="T100">"Oːt, agalaːgɨm ebite bu͡ollar kajdak ere gɨnɨ͡a ete bu͡ollaga", diːr u͡ol ijetiger. </ta>
            <ta e="T118" id="Seg_2931" s="T112">Ijete u͡ol agalaːgɨn tuhunan amattan kepseːbet. </ta>
            <ta e="T127" id="Seg_2932" s="T118">Ije ahɨn bɨhan mu͡ot oŋoror, u͡ol manan tuhak oŋostor. </ta>
            <ta e="T135" id="Seg_2933" s="T127">Manan kuruppaːskɨnɨ ölörön, onon iːttinen kihi bu͡olan olorollor. </ta>
            <ta e="T140" id="Seg_2934" s="T135">Bu oloron u͡ol bi͡ek ɨjɨtar: </ta>
            <ta e="T143" id="Seg_2935" s="T140">"Min teːteleːk eti͡em?" </ta>
            <ta e="T149" id="Seg_2936" s="T143">Ijete amattan haŋarbat ebit, kistiːr ebit. </ta>
            <ta e="T154" id="Seg_2937" s="T149">Lɨːpɨrdaːn emi͡e kihini ölöröːrü hɨldʼar. </ta>
            <ta e="T158" id="Seg_2938" s="T154">Biːr u͡ollaːk emeːksiŋŋe keler. </ta>
            <ta e="T165" id="Seg_2939" s="T158">Araj körbüte, hürdeːk hu͡on tiːt mas turar. </ta>
            <ta e="T172" id="Seg_2940" s="T165">Bu maska emeːksin u͡ola haːtɨn iːlen keːspit. </ta>
            <ta e="T174" id="Seg_2941" s="T172">Lɨːpɨrdaːn ɨjɨtar: </ta>
            <ta e="T177" id="Seg_2942" s="T174">"Iti kim haːtaj? </ta>
            <ta e="T179" id="Seg_2943" s="T177">Tu͡ok kergenneːkkin?" </ta>
            <ta e="T180" id="Seg_2944" s="T179">Emeːksin: </ta>
            <ta e="T182" id="Seg_2945" s="T180">"U͡ollaːkpɨn", diːr. </ta>
            <ta e="T188" id="Seg_2946" s="T182">"Beː, iti haːtɨn togo keːspitej — keː? </ta>
            <ta e="T197" id="Seg_2947" s="T188">"Iti haː küːhe taksɨbɨt, ol ihin keːspite", diːr emeːksin. </ta>
            <ta e="T204" id="Seg_2948" s="T197">Onton boldok hɨtar, ulakan bagajɨ taːs kamenʼ. </ta>
            <ta e="T209" id="Seg_2949" s="T204">Manɨ emi͡e ɨjɨtar bili kihi. </ta>
            <ta e="T211" id="Seg_2950" s="T209">Emeːksin onu: </ta>
            <ta e="T216" id="Seg_2951" s="T211">"Iti čuŋkujdagɨna meːčik gɨnaːččɨ", diːr. </ta>
            <ta e="T223" id="Seg_2952" s="T216">"Ol kajaga di͡eri teptegine töttörü čekinijen keleːčči. </ta>
            <ta e="T227" id="Seg_2953" s="T223">Ol kurduk oːnnʼoːčču", diːr. </ta>
            <ta e="T229" id="Seg_2954" s="T227">Lɨːpɨrdaːn eter: </ta>
            <ta e="T233" id="Seg_2955" s="T229">"Harsɨn min töröːbüt künnenebin. </ta>
            <ta e="T236" id="Seg_2956" s="T233">Onno u͡oluŋ keli͡ektin." </ta>
            <ta e="T244" id="Seg_2957" s="T236">Bu u͡ol dʼi͡etiger kelbitiger ijete barɨtɨn kepseːn bi͡erer: </ta>
            <ta e="T246" id="Seg_2958" s="T244">"ɨŋɨrallar", diːr. </ta>
            <ta e="T252" id="Seg_2959" s="T246">Dʼe araj bu kihiŋ Lɨːpɨrdaːŋŋa barda. </ta>
            <ta e="T254" id="Seg_2960" s="T252">Lɨːpɨrdaːn kihileriger: </ta>
            <ta e="T260" id="Seg_2961" s="T254">"Bu kihini kürü͡ölü͡ökpüt, ölörü͡ökpüt", di͡en haŋarar. </ta>
            <ta e="T264" id="Seg_2962" s="T260">Kelbitiger ɨ͡aldʼɨttarɨn kürü͡öleːn kebiheller. </ta>
            <ta e="T267" id="Seg_2963" s="T264">Ölöröːrüler hubelespitter bu͡ollaga. </ta>
            <ta e="T271" id="Seg_2964" s="T267">Lɨːpɨrdaːn hallan dogottorugar haŋarar: </ta>
            <ta e="T275" id="Seg_2965" s="T271">"Bu tu͡ok bu͡olaŋŋɨt munnʼullagɨt?" </ta>
            <ta e="T278" id="Seg_2966" s="T275">ɨ͡aldʼɨt manna eter: </ta>
            <ta e="T284" id="Seg_2967" s="T278">"Ogonnʼor, en miːgin ölöröːrü gɨnargɨn bilebin. </ta>
            <ta e="T294" id="Seg_2968" s="T284">Min bu hogotok bɨhɨjalaːkpɨn, onon noru͡okkun biːrde ergiji͡em ete", diːr. </ta>
            <ta e="T298" id="Seg_2969" s="T294">"Kaja-kuː, iti kurduk gɨnɨma. </ta>
            <ta e="T303" id="Seg_2970" s="T298">Min kɨːspɨn dʼaktar gɨn", diːr. </ta>
            <ta e="T309" id="Seg_2971" s="T303">ɨ͡aldʼɨta Lɨːpɨrdaːn kɨːhɨn dʼaktar gɨna hɨtar. </ta>
            <ta e="T319" id="Seg_2972" s="T309">Bu u͡ol ulaːtan baran teːtetin ölörbüt kihini bi͡ek kördüːr ebit. </ta>
            <ta e="T327" id="Seg_2973" s="T319">Agata ölbüt hiriger ulaːtan baran bi͡ek hɨldʼar ebit. </ta>
            <ta e="T333" id="Seg_2974" s="T327">Araj bu hɨttagɨna ikki kihi iheller. </ta>
            <ta e="T340" id="Seg_2975" s="T333">Araj biːrdere toŋ ebekeni tü͡öhüger baːjan iher. </ta>
            <ta e="T343" id="Seg_2976" s="T340">Manɨ͡aka dogoro diːr: </ta>
            <ta e="T351" id="Seg_2977" s="T343">"Manna, kaja, Lɨːpɨrdaːn kömüs ü͡östeːk haːlaːk kihini ölörbüte. </ta>
            <ta e="T356" id="Seg_2978" s="T351">Baːr ol kihi u͡ola", diːr. </ta>
            <ta e="T361" id="Seg_2979" s="T356">U͡ol onno dʼe agalaːgɨn ister. </ta>
            <ta e="T368" id="Seg_2980" s="T361">Bu kihilergin dʼe ü͡ögülüːr diː bu ogoŋ. </ta>
            <ta e="T375" id="Seg_2981" s="T368">"Hirbit kuttuːr" di͡en manna ebekennerin kaːllara ku͡otallar. </ta>
            <ta e="T383" id="Seg_2982" s="T375">U͡ol ebekeni kɨtta bölüːgetin dʼi͡etiger ijetiger ildʼe barar. </ta>
            <ta e="T386" id="Seg_2983" s="T383">Bularɨ hiːller bu͡ollaga. </ta>
            <ta e="T388" id="Seg_2984" s="T386">Ijete hemeliːr: </ta>
            <ta e="T392" id="Seg_2985" s="T388">"Bu tu͡ok ör hɨldʼagɨn? </ta>
            <ta e="T394" id="Seg_2986" s="T392">Ölörön keːhi͡ektere." </ta>
            <ta e="T399" id="Seg_2987" s="T394">U͡ola ijetin haŋatɨn amattan istibet. </ta>
            <ta e="T404" id="Seg_2988" s="T399">Bu u͡ol bi͡ek agatɨn kördüːr. </ta>
            <ta e="T411" id="Seg_2989" s="T404">Bu baran Lɨːpɨrdaːn ogonnʼor dʼi͡etin dʼe bular. </ta>
            <ta e="T418" id="Seg_2990" s="T411">Bu bulan, ki͡eheːŋŋi uːnnan utuja hɨttaktarɨna, keler. </ta>
            <ta e="T428" id="Seg_2991" s="T418">Utuja hɨtar kihini kustugunan hu͡on iŋiːrderin bɨhɨta ɨtɨ͡alɨːr, Lɨːpɨrdaːn kütü͡ötün. </ta>
            <ta e="T434" id="Seg_2992" s="T428">"Dʼe bu kɨnnɨm kuhaganɨgar ölön erebin. </ta>
            <ta e="T439" id="Seg_2993" s="T434">Min en agatɨn ölörbötögüm", diːr. </ta>
            <ta e="T443" id="Seg_2994" s="T439">"Ajɨlaːgɨn miːgin tɨːmmɨn bɨs. </ta>
            <ta e="T449" id="Seg_2995" s="T443">Anɨ da tu͡ok kihite bu͡olu͡omuj", diːr. </ta>
            <ta e="T456" id="Seg_2996" s="T449">Bu ogoŋ bu kihini tɨːnɨn bɨhan keːher. </ta>
            <ta e="T464" id="Seg_2997" s="T456">Bu ogoŋ manɨ ister da, Lɨːpɨrdaːnɨ ölörön keːher. </ta>
            <ta e="T468" id="Seg_2998" s="T464">Baːjɨn-totun dʼi͡etiger ildʼe barar. </ta>
            <ta e="T477" id="Seg_2999" s="T468">Bu baːjgɨn dʼi͡etiger ti͡erden ijetin kɨtta bajan-tajan dʼe olorbuta. </ta>
            <ta e="T478" id="Seg_3000" s="T477">Elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_3001" s="T0">Lɨːpɨrdaːn</ta>
            <ta e="T2" id="Seg_3002" s="T1">dʼe</ta>
            <ta e="T3" id="Seg_3003" s="T2">olor-or</ta>
            <ta e="T4" id="Seg_3004" s="T3">Lɨːpɨrdaːn</ta>
            <ta e="T5" id="Seg_3005" s="T4">kaja</ta>
            <ta e="T6" id="Seg_3006" s="T5">ere</ta>
            <ta e="T7" id="Seg_3007" s="T6">di͡ekki</ta>
            <ta e="T8" id="Seg_3008" s="T7">hɨldʼ-ar</ta>
            <ta e="T9" id="Seg_3009" s="T8">bu</ta>
            <ta e="T10" id="Seg_3010" s="T9">hɨldʼ-an</ta>
            <ta e="T11" id="Seg_3011" s="T10">dʼaktar-daːk</ta>
            <ta e="T12" id="Seg_3012" s="T11">kihi-ni</ta>
            <ta e="T13" id="Seg_3013" s="T12">bul-ar</ta>
            <ta e="T14" id="Seg_3014" s="T13">u͡ol-laːk</ta>
            <ta e="T15" id="Seg_3015" s="T14">ol</ta>
            <ta e="T16" id="Seg_3016" s="T15">kihi</ta>
            <ta e="T17" id="Seg_3017" s="T16">dʼaktar-daːk</ta>
            <ta e="T18" id="Seg_3018" s="T17">kihi-ŋ</ta>
            <ta e="T19" id="Seg_3019" s="T18">tuspa</ta>
            <ta e="T20" id="Seg_3020" s="T19">kös-püt</ta>
            <ta e="T21" id="Seg_3021" s="T20">hogotogun</ta>
            <ta e="T22" id="Seg_3022" s="T21">karaŋa</ta>
            <ta e="T23" id="Seg_3023" s="T22">bu͡ol-an</ta>
            <ta e="T24" id="Seg_3024" s="T23">ki͡ehe</ta>
            <ta e="T25" id="Seg_3025" s="T24">biːr</ta>
            <ta e="T26" id="Seg_3026" s="T25">hir-ge</ta>
            <ta e="T27" id="Seg_3027" s="T26">tüs-püt</ta>
            <ta e="T28" id="Seg_3028" s="T27">utuj-aːrɨ</ta>
            <ta e="T29" id="Seg_3029" s="T28">atag-ɨ-n</ta>
            <ta e="T30" id="Seg_3030" s="T29">hɨgɨnnʼak-tan-an</ta>
            <ta e="T31" id="Seg_3031" s="T30">er-deg-ine</ta>
            <ta e="T32" id="Seg_3032" s="T31">u͡ot-a</ta>
            <ta e="T33" id="Seg_3033" s="T32">kas-ta</ta>
            <ta e="T34" id="Seg_3034" s="T33">da</ta>
            <ta e="T35" id="Seg_3035" s="T34">ep-pit</ta>
            <ta e="T36" id="Seg_3036" s="T35">bu</ta>
            <ta e="T37" id="Seg_3037" s="T36">kihi-ŋ</ta>
            <ta e="T38" id="Seg_3038" s="T37">min</ta>
            <ta e="T39" id="Seg_3039" s="T38">künüs</ta>
            <ta e="T40" id="Seg_3040" s="T39">hɨldʼ-ɨ-bɨt-ɨ-m</ta>
            <ta e="T41" id="Seg_3041" s="T40">diː</ta>
            <ta e="T42" id="Seg_3042" s="T41">tu͡ok</ta>
            <ta e="T43" id="Seg_3043" s="T42">kel-i͡e=j</ta>
            <ta e="T44" id="Seg_3044" s="T43">u͡ot-u-m</ta>
            <ta e="T45" id="Seg_3045" s="T44">togo</ta>
            <ta e="T46" id="Seg_3046" s="T45">et-er</ta>
            <ta e="T47" id="Seg_3047" s="T46">di͡e-n</ta>
            <ta e="T48" id="Seg_3048" s="T47">kuttam-mat</ta>
            <ta e="T49" id="Seg_3049" s="T48">hɨt-ar</ta>
            <ta e="T50" id="Seg_3050" s="T49">Lɨːpɨrdaːn</ta>
            <ta e="T51" id="Seg_3051" s="T50">kel-en</ta>
            <ta e="T52" id="Seg_3052" s="T51">bu</ta>
            <ta e="T53" id="Seg_3053" s="T52">kömüs</ta>
            <ta e="T54" id="Seg_3054" s="T53">ü͡ös-teːk</ta>
            <ta e="T55" id="Seg_3055" s="T54">haː-laːk</ta>
            <ta e="T56" id="Seg_3056" s="T55">kihi-ni</ta>
            <ta e="T57" id="Seg_3057" s="T56">ölör-ön</ta>
            <ta e="T58" id="Seg_3058" s="T57">keːh-er</ta>
            <ta e="T59" id="Seg_3059" s="T58">alaŋaː</ta>
            <ta e="T60" id="Seg_3060" s="T59">haː</ta>
            <ta e="T61" id="Seg_3061" s="T60">bu͡ollaga</ta>
            <ta e="T62" id="Seg_3062" s="T61">diː</ta>
            <ta e="T63" id="Seg_3063" s="T62">kihi-ŋ</ta>
            <ta e="T64" id="Seg_3064" s="T63">dʼaktar-a</ta>
            <ta e="T65" id="Seg_3065" s="T64">uččuguj</ta>
            <ta e="T66" id="Seg_3066" s="T65">ogo-tu-n</ta>
            <ta e="T67" id="Seg_3067" s="T66">kɨtta</ta>
            <ta e="T68" id="Seg_3068" s="T67">hir-ge</ta>
            <ta e="T69" id="Seg_3069" s="T68">küreː-n</ta>
            <ta e="T70" id="Seg_3070" s="T69">kaːl-ar</ta>
            <ta e="T71" id="Seg_3071" s="T70">dʼaktar-ɨ-ŋ</ta>
            <ta e="T72" id="Seg_3072" s="T71">tug-a</ta>
            <ta e="T73" id="Seg_3073" s="T72">da</ta>
            <ta e="T74" id="Seg_3074" s="T73">hu͡ok</ta>
            <ta e="T75" id="Seg_3075" s="T74">bu͡or</ta>
            <ta e="T76" id="Seg_3076" s="T75">golomo-koːŋ-ŋo</ta>
            <ta e="T77" id="Seg_3077" s="T76">olor-or</ta>
            <ta e="T78" id="Seg_3078" s="T77">oloŋko</ta>
            <ta e="T79" id="Seg_3079" s="T78">ogo-to</ta>
            <ta e="T80" id="Seg_3080" s="T79">ör</ta>
            <ta e="T81" id="Seg_3081" s="T80">bu͡ol-bat</ta>
            <ta e="T82" id="Seg_3082" s="T81">ulaːt-an</ta>
            <ta e="T83" id="Seg_3083" s="T82">kaːl-ar</ta>
            <ta e="T84" id="Seg_3084" s="T83">ogo</ta>
            <ta e="T85" id="Seg_3085" s="T84">taks-an</ta>
            <ta e="T86" id="Seg_3086" s="T85">kör-büt-e</ta>
            <ta e="T87" id="Seg_3087" s="T86">dʼi͡e-ti-n</ta>
            <ta e="T88" id="Seg_3088" s="T87">ürdü-tü-ger</ta>
            <ta e="T89" id="Seg_3089" s="T88">čeːlkeː-ler</ta>
            <ta e="T90" id="Seg_3090" s="T89">hɨldʼ-al-lar</ta>
            <ta e="T91" id="Seg_3091" s="T90">ije-ti-ger</ta>
            <ta e="T92" id="Seg_3092" s="T91">kiːr-en</ta>
            <ta e="T93" id="Seg_3093" s="T92">kepsiː-r</ta>
            <ta e="T94" id="Seg_3094" s="T93">eː</ta>
            <ta e="T95" id="Seg_3095" s="T94">oččogo</ta>
            <ta e="T96" id="Seg_3096" s="T95">kuruppaːskɨ-lar</ta>
            <ta e="T97" id="Seg_3097" s="T96">bu͡ollaga</ta>
            <ta e="T98" id="Seg_3098" s="T97">diː</ta>
            <ta e="T99" id="Seg_3099" s="T98">diː-r</ta>
            <ta e="T100" id="Seg_3100" s="T99">ije-te</ta>
            <ta e="T101" id="Seg_3101" s="T100">oːt</ta>
            <ta e="T102" id="Seg_3102" s="T101">aga-laːg-ɨ-m</ta>
            <ta e="T103" id="Seg_3103" s="T102">e-bit-e</ta>
            <ta e="T104" id="Seg_3104" s="T103">bu͡ol-lar</ta>
            <ta e="T105" id="Seg_3105" s="T104">kajdak</ta>
            <ta e="T106" id="Seg_3106" s="T105">ere</ta>
            <ta e="T107" id="Seg_3107" s="T106">gɨn-ɨ͡a</ta>
            <ta e="T108" id="Seg_3108" s="T107">e-t-e</ta>
            <ta e="T109" id="Seg_3109" s="T108">bu͡ollaga</ta>
            <ta e="T110" id="Seg_3110" s="T109">diː-r</ta>
            <ta e="T111" id="Seg_3111" s="T110">u͡ol</ta>
            <ta e="T112" id="Seg_3112" s="T111">ije-ti-ger</ta>
            <ta e="T113" id="Seg_3113" s="T112">ije-te</ta>
            <ta e="T114" id="Seg_3114" s="T113">u͡ol</ta>
            <ta e="T115" id="Seg_3115" s="T114">aga-laːg-ɨ-n</ta>
            <ta e="T116" id="Seg_3116" s="T115">tuh-u-nan</ta>
            <ta e="T117" id="Seg_3117" s="T116">amattan</ta>
            <ta e="T118" id="Seg_3118" s="T117">kepseː-bet</ta>
            <ta e="T119" id="Seg_3119" s="T118">ije</ta>
            <ta e="T120" id="Seg_3120" s="T119">ah-ɨ-n</ta>
            <ta e="T121" id="Seg_3121" s="T120">bɨh-an</ta>
            <ta e="T122" id="Seg_3122" s="T121">mu͡ot</ta>
            <ta e="T123" id="Seg_3123" s="T122">oŋor-or</ta>
            <ta e="T124" id="Seg_3124" s="T123">u͡ol</ta>
            <ta e="T125" id="Seg_3125" s="T124">ma-nan</ta>
            <ta e="T126" id="Seg_3126" s="T125">tuhak</ta>
            <ta e="T127" id="Seg_3127" s="T126">oŋost-or</ta>
            <ta e="T128" id="Seg_3128" s="T127">ma-nan</ta>
            <ta e="T129" id="Seg_3129" s="T128">kuruppaːskɨ-nɨ</ta>
            <ta e="T130" id="Seg_3130" s="T129">ölör-ön</ta>
            <ta e="T131" id="Seg_3131" s="T130">o-non</ta>
            <ta e="T132" id="Seg_3132" s="T131">iːt-tin-en</ta>
            <ta e="T133" id="Seg_3133" s="T132">kihi</ta>
            <ta e="T134" id="Seg_3134" s="T133">bu͡ol-an</ta>
            <ta e="T135" id="Seg_3135" s="T134">olor-ol-lor</ta>
            <ta e="T136" id="Seg_3136" s="T135">bu</ta>
            <ta e="T137" id="Seg_3137" s="T136">olor-on</ta>
            <ta e="T138" id="Seg_3138" s="T137">u͡ol</ta>
            <ta e="T139" id="Seg_3139" s="T138">bi͡ek</ta>
            <ta e="T140" id="Seg_3140" s="T139">ɨjɨt-ar</ta>
            <ta e="T141" id="Seg_3141" s="T140">min</ta>
            <ta e="T142" id="Seg_3142" s="T141">teːte-leːk</ta>
            <ta e="T143" id="Seg_3143" s="T142">e-ti͡e-m</ta>
            <ta e="T144" id="Seg_3144" s="T143">ije-te</ta>
            <ta e="T145" id="Seg_3145" s="T144">amattan</ta>
            <ta e="T146" id="Seg_3146" s="T145">haŋar-bat</ta>
            <ta e="T147" id="Seg_3147" s="T146">e-bit</ta>
            <ta e="T148" id="Seg_3148" s="T147">kistiː-r</ta>
            <ta e="T149" id="Seg_3149" s="T148">e-bit</ta>
            <ta e="T150" id="Seg_3150" s="T149">Lɨːpɨrdaːn</ta>
            <ta e="T151" id="Seg_3151" s="T150">emi͡e</ta>
            <ta e="T152" id="Seg_3152" s="T151">kihi-ni</ta>
            <ta e="T153" id="Seg_3153" s="T152">ölör-öːrü</ta>
            <ta e="T154" id="Seg_3154" s="T153">hɨldʼ-ar</ta>
            <ta e="T155" id="Seg_3155" s="T154">biːr</ta>
            <ta e="T156" id="Seg_3156" s="T155">u͡ol-laːk</ta>
            <ta e="T157" id="Seg_3157" s="T156">emeːksiŋ-ŋe</ta>
            <ta e="T158" id="Seg_3158" s="T157">kel-er</ta>
            <ta e="T159" id="Seg_3159" s="T158">araj</ta>
            <ta e="T160" id="Seg_3160" s="T159">kör-büt-e</ta>
            <ta e="T161" id="Seg_3161" s="T160">hürdeːk</ta>
            <ta e="T162" id="Seg_3162" s="T161">hu͡on</ta>
            <ta e="T163" id="Seg_3163" s="T162">tiːt</ta>
            <ta e="T164" id="Seg_3164" s="T163">mas</ta>
            <ta e="T165" id="Seg_3165" s="T164">tur-ar</ta>
            <ta e="T166" id="Seg_3166" s="T165">bu</ta>
            <ta e="T167" id="Seg_3167" s="T166">mas-ka</ta>
            <ta e="T168" id="Seg_3168" s="T167">emeːksin</ta>
            <ta e="T169" id="Seg_3169" s="T168">u͡ol-a</ta>
            <ta e="T170" id="Seg_3170" s="T169">haː-tɨ-n</ta>
            <ta e="T171" id="Seg_3171" s="T170">iːl-en</ta>
            <ta e="T172" id="Seg_3172" s="T171">keːs-pit</ta>
            <ta e="T173" id="Seg_3173" s="T172">Lɨːpɨrdaːn</ta>
            <ta e="T174" id="Seg_3174" s="T173">ɨjɨt-ar</ta>
            <ta e="T175" id="Seg_3175" s="T174">iti</ta>
            <ta e="T176" id="Seg_3176" s="T175">kim</ta>
            <ta e="T177" id="Seg_3177" s="T176">haː-ta=j</ta>
            <ta e="T178" id="Seg_3178" s="T177">tu͡ok</ta>
            <ta e="T179" id="Seg_3179" s="T178">kergen-neːk-kin</ta>
            <ta e="T180" id="Seg_3180" s="T179">emeːksin</ta>
            <ta e="T181" id="Seg_3181" s="T180">u͡ol-laːk-pɨn</ta>
            <ta e="T182" id="Seg_3182" s="T181">diː-r</ta>
            <ta e="T183" id="Seg_3183" s="T182">beː</ta>
            <ta e="T184" id="Seg_3184" s="T183">iti</ta>
            <ta e="T185" id="Seg_3185" s="T184">haː-tɨ-n</ta>
            <ta e="T186" id="Seg_3186" s="T185">togo</ta>
            <ta e="T187" id="Seg_3187" s="T186">keːs-pit-e=j</ta>
            <ta e="T188" id="Seg_3188" s="T187">keː</ta>
            <ta e="T189" id="Seg_3189" s="T188">iti</ta>
            <ta e="T190" id="Seg_3190" s="T189">haː</ta>
            <ta e="T191" id="Seg_3191" s="T190">küːh-e</ta>
            <ta e="T192" id="Seg_3192" s="T191">taks-ɨ-bɨt</ta>
            <ta e="T193" id="Seg_3193" s="T192">ol</ta>
            <ta e="T194" id="Seg_3194" s="T193">ihin</ta>
            <ta e="T195" id="Seg_3195" s="T194">keːs-pit-e</ta>
            <ta e="T196" id="Seg_3196" s="T195">diː-r</ta>
            <ta e="T197" id="Seg_3197" s="T196">emeːksin</ta>
            <ta e="T198" id="Seg_3198" s="T197">onton</ta>
            <ta e="T199" id="Seg_3199" s="T198">boldok</ta>
            <ta e="T200" id="Seg_3200" s="T199">hɨt-ar</ta>
            <ta e="T201" id="Seg_3201" s="T200">ulakan</ta>
            <ta e="T202" id="Seg_3202" s="T201">bagajɨ</ta>
            <ta e="T203" id="Seg_3203" s="T202">taːs</ta>
            <ta e="T205" id="Seg_3204" s="T204">ma-nɨ</ta>
            <ta e="T206" id="Seg_3205" s="T205">emi͡e</ta>
            <ta e="T207" id="Seg_3206" s="T206">ɨjɨt-ar</ta>
            <ta e="T208" id="Seg_3207" s="T207">bili</ta>
            <ta e="T209" id="Seg_3208" s="T208">kihi</ta>
            <ta e="T210" id="Seg_3209" s="T209">emeːksin</ta>
            <ta e="T211" id="Seg_3210" s="T210">o-nu</ta>
            <ta e="T212" id="Seg_3211" s="T211">iti</ta>
            <ta e="T213" id="Seg_3212" s="T212">čuŋkuj-dag-ɨna</ta>
            <ta e="T214" id="Seg_3213" s="T213">meːčik</ta>
            <ta e="T215" id="Seg_3214" s="T214">gɨn-aːččɨ</ta>
            <ta e="T216" id="Seg_3215" s="T215">diː-r</ta>
            <ta e="T217" id="Seg_3216" s="T216">ol</ta>
            <ta e="T218" id="Seg_3217" s="T217">kaja-ga</ta>
            <ta e="T219" id="Seg_3218" s="T218">di͡eri</ta>
            <ta e="T220" id="Seg_3219" s="T219">tep-teg-ine</ta>
            <ta e="T221" id="Seg_3220" s="T220">töttörü</ta>
            <ta e="T222" id="Seg_3221" s="T221">čekinij-en</ta>
            <ta e="T223" id="Seg_3222" s="T222">kel-eːčči</ta>
            <ta e="T224" id="Seg_3223" s="T223">ol</ta>
            <ta e="T225" id="Seg_3224" s="T224">kurduk</ta>
            <ta e="T226" id="Seg_3225" s="T225">oːnnʼoː-čču</ta>
            <ta e="T227" id="Seg_3226" s="T226">diː-r</ta>
            <ta e="T228" id="Seg_3227" s="T227">Lɨːpɨrdaːn</ta>
            <ta e="T229" id="Seg_3228" s="T228">et-er</ta>
            <ta e="T230" id="Seg_3229" s="T229">harsɨn</ta>
            <ta e="T231" id="Seg_3230" s="T230">min</ta>
            <ta e="T232" id="Seg_3231" s="T231">töröː-büt</ta>
            <ta e="T233" id="Seg_3232" s="T232">kün-nen-e-bin</ta>
            <ta e="T234" id="Seg_3233" s="T233">onno</ta>
            <ta e="T235" id="Seg_3234" s="T234">u͡ol-u-ŋ</ta>
            <ta e="T236" id="Seg_3235" s="T235">kel-i͡ek-tin</ta>
            <ta e="T237" id="Seg_3236" s="T236">bu</ta>
            <ta e="T238" id="Seg_3237" s="T237">u͡ol</ta>
            <ta e="T239" id="Seg_3238" s="T238">dʼi͡e-ti-ger</ta>
            <ta e="T240" id="Seg_3239" s="T239">kel-bit-i-ger</ta>
            <ta e="T241" id="Seg_3240" s="T240">ije-te</ta>
            <ta e="T242" id="Seg_3241" s="T241">barɨ-tɨ-n</ta>
            <ta e="T243" id="Seg_3242" s="T242">kepseː-n</ta>
            <ta e="T244" id="Seg_3243" s="T243">bi͡er-er</ta>
            <ta e="T245" id="Seg_3244" s="T244">ɨŋɨr-al-lar</ta>
            <ta e="T246" id="Seg_3245" s="T245">diː-r</ta>
            <ta e="T247" id="Seg_3246" s="T246">dʼe</ta>
            <ta e="T248" id="Seg_3247" s="T247">araj</ta>
            <ta e="T249" id="Seg_3248" s="T248">bu</ta>
            <ta e="T250" id="Seg_3249" s="T249">kihi-ŋ</ta>
            <ta e="T251" id="Seg_3250" s="T250">Lɨːpɨrdaːŋ-ŋa</ta>
            <ta e="T252" id="Seg_3251" s="T251">bar-d-a</ta>
            <ta e="T253" id="Seg_3252" s="T252">Lɨːpɨrdaːn</ta>
            <ta e="T254" id="Seg_3253" s="T253">kihi-ler-i-ger</ta>
            <ta e="T255" id="Seg_3254" s="T254">bu</ta>
            <ta e="T256" id="Seg_3255" s="T255">kihi-ni</ta>
            <ta e="T257" id="Seg_3256" s="T256">kürü͡ö-l-ü͡ök-püt</ta>
            <ta e="T258" id="Seg_3257" s="T257">ölör-ü͡ök-püt</ta>
            <ta e="T259" id="Seg_3258" s="T258">di͡e-n</ta>
            <ta e="T260" id="Seg_3259" s="T259">haŋar-ar</ta>
            <ta e="T261" id="Seg_3260" s="T260">kel-bit-i-ger</ta>
            <ta e="T262" id="Seg_3261" s="T261">ɨ͡aldʼɨt-tarɨ-n</ta>
            <ta e="T263" id="Seg_3262" s="T262">kürü͡ö-leː-n</ta>
            <ta e="T264" id="Seg_3263" s="T263">kebih-el-ler</ta>
            <ta e="T265" id="Seg_3264" s="T264">ölör-öːrü-ler</ta>
            <ta e="T266" id="Seg_3265" s="T265">hubel-e-s-pit-ter</ta>
            <ta e="T267" id="Seg_3266" s="T266">bu͡ollaga</ta>
            <ta e="T268" id="Seg_3267" s="T267">Lɨːpɨrdaːn</ta>
            <ta e="T269" id="Seg_3268" s="T268">hall-an</ta>
            <ta e="T270" id="Seg_3269" s="T269">dogot-tor-u-gar</ta>
            <ta e="T271" id="Seg_3270" s="T270">haŋar-ar</ta>
            <ta e="T272" id="Seg_3271" s="T271">bu</ta>
            <ta e="T273" id="Seg_3272" s="T272">tu͡ok</ta>
            <ta e="T274" id="Seg_3273" s="T273">bu͡ol-aŋ-ŋɨt</ta>
            <ta e="T275" id="Seg_3274" s="T274">munnʼ-u-ll-a-gɨt</ta>
            <ta e="T276" id="Seg_3275" s="T275">ɨ͡aldʼɨt</ta>
            <ta e="T277" id="Seg_3276" s="T276">manna</ta>
            <ta e="T278" id="Seg_3277" s="T277">et-er</ta>
            <ta e="T279" id="Seg_3278" s="T278">ogonnʼor</ta>
            <ta e="T280" id="Seg_3279" s="T279">en</ta>
            <ta e="T281" id="Seg_3280" s="T280">miːgi-n</ta>
            <ta e="T282" id="Seg_3281" s="T281">ölör-öːrü</ta>
            <ta e="T283" id="Seg_3282" s="T282">gɨn-ar-gɨ-n</ta>
            <ta e="T284" id="Seg_3283" s="T283">bil-e-bin</ta>
            <ta e="T285" id="Seg_3284" s="T284">min</ta>
            <ta e="T286" id="Seg_3285" s="T285">bu</ta>
            <ta e="T287" id="Seg_3286" s="T286">hogotok</ta>
            <ta e="T288" id="Seg_3287" s="T287">bɨhɨja-laːk-pɨn</ta>
            <ta e="T289" id="Seg_3288" s="T288">o-non</ta>
            <ta e="T290" id="Seg_3289" s="T289">noru͡ok-ku-n</ta>
            <ta e="T291" id="Seg_3290" s="T290">biːrde</ta>
            <ta e="T292" id="Seg_3291" s="T291">ergij-i͡e-m</ta>
            <ta e="T293" id="Seg_3292" s="T292">e-t-e</ta>
            <ta e="T294" id="Seg_3293" s="T293">diː-r</ta>
            <ta e="T295" id="Seg_3294" s="T294">kaja=kuː</ta>
            <ta e="T296" id="Seg_3295" s="T295">iti</ta>
            <ta e="T297" id="Seg_3296" s="T296">kurduk</ta>
            <ta e="T298" id="Seg_3297" s="T297">gɨn-ɨ-ma</ta>
            <ta e="T299" id="Seg_3298" s="T298">min</ta>
            <ta e="T300" id="Seg_3299" s="T299">kɨːs-pɨ-n</ta>
            <ta e="T301" id="Seg_3300" s="T300">dʼaktar</ta>
            <ta e="T302" id="Seg_3301" s="T301">gɨn</ta>
            <ta e="T303" id="Seg_3302" s="T302">diː-r</ta>
            <ta e="T304" id="Seg_3303" s="T303">ɨ͡aldʼɨt-a</ta>
            <ta e="T305" id="Seg_3304" s="T304">Lɨːpɨrdaːn</ta>
            <ta e="T306" id="Seg_3305" s="T305">kɨːh-ɨ-n</ta>
            <ta e="T307" id="Seg_3306" s="T306">dʼaktar</ta>
            <ta e="T308" id="Seg_3307" s="T307">gɨn-a</ta>
            <ta e="T309" id="Seg_3308" s="T308">hɨt-ar</ta>
            <ta e="T310" id="Seg_3309" s="T309">bu</ta>
            <ta e="T311" id="Seg_3310" s="T310">u͡ol</ta>
            <ta e="T312" id="Seg_3311" s="T311">ulaːt-an</ta>
            <ta e="T313" id="Seg_3312" s="T312">baran</ta>
            <ta e="T314" id="Seg_3313" s="T313">teːte-ti-n</ta>
            <ta e="T315" id="Seg_3314" s="T314">ölör-büt</ta>
            <ta e="T316" id="Seg_3315" s="T315">kihi-ni</ta>
            <ta e="T317" id="Seg_3316" s="T316">bi͡ek</ta>
            <ta e="T318" id="Seg_3317" s="T317">kördüː-r</ta>
            <ta e="T319" id="Seg_3318" s="T318">e-bit</ta>
            <ta e="T320" id="Seg_3319" s="T319">aga-ta</ta>
            <ta e="T321" id="Seg_3320" s="T320">öl-büt</ta>
            <ta e="T322" id="Seg_3321" s="T321">hir-i-ger</ta>
            <ta e="T323" id="Seg_3322" s="T322">ulaːt-an</ta>
            <ta e="T324" id="Seg_3323" s="T323">baran</ta>
            <ta e="T325" id="Seg_3324" s="T324">bi͡ek</ta>
            <ta e="T326" id="Seg_3325" s="T325">hɨldʼ-ar</ta>
            <ta e="T327" id="Seg_3326" s="T326">e-bit</ta>
            <ta e="T328" id="Seg_3327" s="T327">araj</ta>
            <ta e="T329" id="Seg_3328" s="T328">bu</ta>
            <ta e="T330" id="Seg_3329" s="T329">hɨt-tag-ɨna</ta>
            <ta e="T331" id="Seg_3330" s="T330">ikki</ta>
            <ta e="T332" id="Seg_3331" s="T331">kihi</ta>
            <ta e="T333" id="Seg_3332" s="T332">ih-el-ler</ta>
            <ta e="T334" id="Seg_3333" s="T333">araj</ta>
            <ta e="T335" id="Seg_3334" s="T334">biːr-dere</ta>
            <ta e="T336" id="Seg_3335" s="T335">toŋ</ta>
            <ta e="T337" id="Seg_3336" s="T336">ebeke-ni</ta>
            <ta e="T338" id="Seg_3337" s="T337">tü͡öh-ü-ger</ta>
            <ta e="T339" id="Seg_3338" s="T338">baːj-an</ta>
            <ta e="T340" id="Seg_3339" s="T339">ih-er</ta>
            <ta e="T341" id="Seg_3340" s="T340">manɨ͡a-ka</ta>
            <ta e="T342" id="Seg_3341" s="T341">dogor-o</ta>
            <ta e="T343" id="Seg_3342" s="T342">diː-r</ta>
            <ta e="T344" id="Seg_3343" s="T343">manna</ta>
            <ta e="T345" id="Seg_3344" s="T344">kaja</ta>
            <ta e="T346" id="Seg_3345" s="T345">Lɨːpɨrdaːn</ta>
            <ta e="T347" id="Seg_3346" s="T346">kömüs</ta>
            <ta e="T348" id="Seg_3347" s="T347">ü͡ös-teːk</ta>
            <ta e="T349" id="Seg_3348" s="T348">haː-laːk</ta>
            <ta e="T350" id="Seg_3349" s="T349">kihi-ni</ta>
            <ta e="T351" id="Seg_3350" s="T350">ölör-büt-e</ta>
            <ta e="T352" id="Seg_3351" s="T351">baːr</ta>
            <ta e="T353" id="Seg_3352" s="T352">ol</ta>
            <ta e="T354" id="Seg_3353" s="T353">kihi</ta>
            <ta e="T355" id="Seg_3354" s="T354">u͡ol-a</ta>
            <ta e="T356" id="Seg_3355" s="T355">diː-r</ta>
            <ta e="T357" id="Seg_3356" s="T356">u͡ol</ta>
            <ta e="T358" id="Seg_3357" s="T357">onno</ta>
            <ta e="T359" id="Seg_3358" s="T358">dʼe</ta>
            <ta e="T360" id="Seg_3359" s="T359">aga-laːg-ɨ-n</ta>
            <ta e="T361" id="Seg_3360" s="T360">ist-er</ta>
            <ta e="T362" id="Seg_3361" s="T361">bu</ta>
            <ta e="T363" id="Seg_3362" s="T362">kihi-ler-gi-n</ta>
            <ta e="T364" id="Seg_3363" s="T363">dʼe</ta>
            <ta e="T365" id="Seg_3364" s="T364">ü͡ögülüː-r</ta>
            <ta e="T366" id="Seg_3365" s="T365">diː</ta>
            <ta e="T367" id="Seg_3366" s="T366">bu</ta>
            <ta e="T368" id="Seg_3367" s="T367">ogo-ŋ</ta>
            <ta e="T369" id="Seg_3368" s="T368">hir-bit</ta>
            <ta e="T370" id="Seg_3369" s="T369">kuttuː-r</ta>
            <ta e="T371" id="Seg_3370" s="T370">di͡e-n</ta>
            <ta e="T372" id="Seg_3371" s="T371">manna</ta>
            <ta e="T373" id="Seg_3372" s="T372">ebe-ken-neri-n</ta>
            <ta e="T374" id="Seg_3373" s="T373">kaːl-lar-a</ta>
            <ta e="T375" id="Seg_3374" s="T374">ku͡ot-al-lar</ta>
            <ta e="T376" id="Seg_3375" s="T375">u͡ol</ta>
            <ta e="T377" id="Seg_3376" s="T376">ebeke-ni</ta>
            <ta e="T378" id="Seg_3377" s="T377">kɨtta</ta>
            <ta e="T379" id="Seg_3378" s="T378">bölüːge-ti-n</ta>
            <ta e="T380" id="Seg_3379" s="T379">dʼi͡e-ti-ger</ta>
            <ta e="T381" id="Seg_3380" s="T380">ije-ti-ger</ta>
            <ta e="T382" id="Seg_3381" s="T381">ildʼ-e</ta>
            <ta e="T383" id="Seg_3382" s="T382">bar-ar</ta>
            <ta e="T384" id="Seg_3383" s="T383">bu-lar-ɨ</ta>
            <ta e="T385" id="Seg_3384" s="T384">hiː-l-ler</ta>
            <ta e="T386" id="Seg_3385" s="T385">bu͡ollaga</ta>
            <ta e="T387" id="Seg_3386" s="T386">ije-te</ta>
            <ta e="T388" id="Seg_3387" s="T387">hemeliː-r</ta>
            <ta e="T389" id="Seg_3388" s="T388">bu</ta>
            <ta e="T390" id="Seg_3389" s="T389">tu͡ok</ta>
            <ta e="T391" id="Seg_3390" s="T390">ör</ta>
            <ta e="T392" id="Seg_3391" s="T391">hɨldʼ-a-gɨn</ta>
            <ta e="T393" id="Seg_3392" s="T392">ölör-ön</ta>
            <ta e="T394" id="Seg_3393" s="T393">keːh-i͡ek-tere</ta>
            <ta e="T395" id="Seg_3394" s="T394">u͡ol-a</ta>
            <ta e="T396" id="Seg_3395" s="T395">ije-ti-n</ta>
            <ta e="T397" id="Seg_3396" s="T396">haŋa-tɨ-n</ta>
            <ta e="T398" id="Seg_3397" s="T397">amattan</ta>
            <ta e="T399" id="Seg_3398" s="T398">ist-i-bet</ta>
            <ta e="T400" id="Seg_3399" s="T399">bu</ta>
            <ta e="T401" id="Seg_3400" s="T400">u͡ol</ta>
            <ta e="T402" id="Seg_3401" s="T401">bi͡ek</ta>
            <ta e="T403" id="Seg_3402" s="T402">aga-tɨ-n</ta>
            <ta e="T404" id="Seg_3403" s="T403">kördüː-r</ta>
            <ta e="T405" id="Seg_3404" s="T404">bu</ta>
            <ta e="T406" id="Seg_3405" s="T405">baran</ta>
            <ta e="T407" id="Seg_3406" s="T406">Lɨːpɨrdaːn</ta>
            <ta e="T408" id="Seg_3407" s="T407">ogonnʼor</ta>
            <ta e="T409" id="Seg_3408" s="T408">dʼi͡e-ti-n</ta>
            <ta e="T410" id="Seg_3409" s="T409">dʼe</ta>
            <ta e="T411" id="Seg_3410" s="T410">bul-ar</ta>
            <ta e="T412" id="Seg_3411" s="T411">bu</ta>
            <ta e="T413" id="Seg_3412" s="T412">bul-an</ta>
            <ta e="T414" id="Seg_3413" s="T413">ki͡eheːŋŋi</ta>
            <ta e="T415" id="Seg_3414" s="T414">uː-nnan</ta>
            <ta e="T416" id="Seg_3415" s="T415">utuj-a</ta>
            <ta e="T417" id="Seg_3416" s="T416">hɨt-tak-tarɨna</ta>
            <ta e="T418" id="Seg_3417" s="T417">kel-er</ta>
            <ta e="T419" id="Seg_3418" s="T418">utuj-a</ta>
            <ta e="T420" id="Seg_3419" s="T419">hɨt-ar</ta>
            <ta e="T421" id="Seg_3420" s="T420">kihi-ni</ta>
            <ta e="T422" id="Seg_3421" s="T421">kustug-u-nan</ta>
            <ta e="T423" id="Seg_3422" s="T422">hu͡on</ta>
            <ta e="T424" id="Seg_3423" s="T423">iŋiːr-deri-n</ta>
            <ta e="T425" id="Seg_3424" s="T424">bɨhɨt-a</ta>
            <ta e="T426" id="Seg_3425" s="T425">ɨt-ɨ͡alɨː-r</ta>
            <ta e="T427" id="Seg_3426" s="T426">Lɨːpɨrdaːn</ta>
            <ta e="T428" id="Seg_3427" s="T427">kütü͡öt-ü-n</ta>
            <ta e="T429" id="Seg_3428" s="T428">dʼe</ta>
            <ta e="T430" id="Seg_3429" s="T429">bu</ta>
            <ta e="T431" id="Seg_3430" s="T430">kɨnn-ɨ-m</ta>
            <ta e="T432" id="Seg_3431" s="T431">kuhagan-ɨ-gar</ta>
            <ta e="T433" id="Seg_3432" s="T432">öl-ön</ta>
            <ta e="T434" id="Seg_3433" s="T433">er-e-bin</ta>
            <ta e="T435" id="Seg_3434" s="T434">min</ta>
            <ta e="T436" id="Seg_3435" s="T435">en</ta>
            <ta e="T437" id="Seg_3436" s="T436">aga-gɨ-n</ta>
            <ta e="T438" id="Seg_3437" s="T437">ölör-bötög-ü-m</ta>
            <ta e="T439" id="Seg_3438" s="T438">diː-r</ta>
            <ta e="T440" id="Seg_3439" s="T439">ajɨlaːgɨn</ta>
            <ta e="T441" id="Seg_3440" s="T440">miːgi-n</ta>
            <ta e="T442" id="Seg_3441" s="T441">tɨːm-mɨ-n</ta>
            <ta e="T443" id="Seg_3442" s="T442">bɨs</ta>
            <ta e="T444" id="Seg_3443" s="T443">anɨ</ta>
            <ta e="T445" id="Seg_3444" s="T444">da</ta>
            <ta e="T446" id="Seg_3445" s="T445">tu͡ok</ta>
            <ta e="T447" id="Seg_3446" s="T446">kihi-te</ta>
            <ta e="T448" id="Seg_3447" s="T447">bu͡ol-u͡o-m=uj</ta>
            <ta e="T449" id="Seg_3448" s="T448">diː-r</ta>
            <ta e="T450" id="Seg_3449" s="T449">bu</ta>
            <ta e="T451" id="Seg_3450" s="T450">ogo-ŋ</ta>
            <ta e="T452" id="Seg_3451" s="T451">bu</ta>
            <ta e="T453" id="Seg_3452" s="T452">kihi-ni</ta>
            <ta e="T454" id="Seg_3453" s="T453">tɨːn-ɨ-n</ta>
            <ta e="T455" id="Seg_3454" s="T454">bɨh-an</ta>
            <ta e="T456" id="Seg_3455" s="T455">keːh-er</ta>
            <ta e="T457" id="Seg_3456" s="T456">bu</ta>
            <ta e="T458" id="Seg_3457" s="T457">ogo-ŋ</ta>
            <ta e="T459" id="Seg_3458" s="T458">ma-nɨ</ta>
            <ta e="T460" id="Seg_3459" s="T459">ist-er</ta>
            <ta e="T461" id="Seg_3460" s="T460">da</ta>
            <ta e="T462" id="Seg_3461" s="T461">Lɨːpɨrdaːn-ɨ</ta>
            <ta e="T463" id="Seg_3462" s="T462">ölör-ön</ta>
            <ta e="T464" id="Seg_3463" s="T463">keːh-er</ta>
            <ta e="T465" id="Seg_3464" s="T464">baːj-ɨ-n-tot-u-n</ta>
            <ta e="T466" id="Seg_3465" s="T465">dʼi͡e-ti-ger</ta>
            <ta e="T467" id="Seg_3466" s="T466">ildʼ-e</ta>
            <ta e="T468" id="Seg_3467" s="T467">bar-ar</ta>
            <ta e="T469" id="Seg_3468" s="T468">bu</ta>
            <ta e="T470" id="Seg_3469" s="T469">baːj-gɨ-n</ta>
            <ta e="T471" id="Seg_3470" s="T470">dʼi͡e-ti-ger</ta>
            <ta e="T472" id="Seg_3471" s="T471">ti͡erd-en</ta>
            <ta e="T473" id="Seg_3472" s="T472">ije-ti-n</ta>
            <ta e="T474" id="Seg_3473" s="T473">kɨtta</ta>
            <ta e="T475" id="Seg_3474" s="T474">baj-an-taj-an</ta>
            <ta e="T476" id="Seg_3475" s="T475">dʼe</ta>
            <ta e="T477" id="Seg_3476" s="T476">olor-but-a</ta>
            <ta e="T478" id="Seg_3477" s="T477">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_3478" s="T0">Lɨːpɨrdaːn</ta>
            <ta e="T2" id="Seg_3479" s="T1">dʼe</ta>
            <ta e="T3" id="Seg_3480" s="T2">olor-Ar</ta>
            <ta e="T4" id="Seg_3481" s="T3">Lɨːpɨrdaːn</ta>
            <ta e="T5" id="Seg_3482" s="T4">kaja</ta>
            <ta e="T6" id="Seg_3483" s="T5">ere</ta>
            <ta e="T7" id="Seg_3484" s="T6">di͡ekki</ta>
            <ta e="T8" id="Seg_3485" s="T7">hɨrɨt-Ar</ta>
            <ta e="T9" id="Seg_3486" s="T8">bu</ta>
            <ta e="T10" id="Seg_3487" s="T9">hɨrɨt-An</ta>
            <ta e="T11" id="Seg_3488" s="T10">dʼaktar-LAːK</ta>
            <ta e="T12" id="Seg_3489" s="T11">kihi-nI</ta>
            <ta e="T13" id="Seg_3490" s="T12">bul-Ar</ta>
            <ta e="T14" id="Seg_3491" s="T13">u͡ol-LAːK</ta>
            <ta e="T15" id="Seg_3492" s="T14">ol</ta>
            <ta e="T16" id="Seg_3493" s="T15">kihi</ta>
            <ta e="T17" id="Seg_3494" s="T16">dʼaktar-LAːK</ta>
            <ta e="T18" id="Seg_3495" s="T17">kihi-ŋ</ta>
            <ta e="T19" id="Seg_3496" s="T18">tuspa</ta>
            <ta e="T20" id="Seg_3497" s="T19">kös-BIT</ta>
            <ta e="T21" id="Seg_3498" s="T20">hogotogun</ta>
            <ta e="T22" id="Seg_3499" s="T21">karaŋa</ta>
            <ta e="T23" id="Seg_3500" s="T22">bu͡ol-An</ta>
            <ta e="T24" id="Seg_3501" s="T23">ki͡ehe</ta>
            <ta e="T25" id="Seg_3502" s="T24">biːr</ta>
            <ta e="T26" id="Seg_3503" s="T25">hir-GA</ta>
            <ta e="T27" id="Seg_3504" s="T26">tüs-BIT</ta>
            <ta e="T28" id="Seg_3505" s="T27">utuj-AːrI</ta>
            <ta e="T29" id="Seg_3506" s="T28">atak-tI-n</ta>
            <ta e="T30" id="Seg_3507" s="T29">hɨgɨnʼak-LAN-An</ta>
            <ta e="T31" id="Seg_3508" s="T30">er-TAK-InA</ta>
            <ta e="T32" id="Seg_3509" s="T31">u͡ot-tA</ta>
            <ta e="T33" id="Seg_3510" s="T32">kas-TA</ta>
            <ta e="T34" id="Seg_3511" s="T33">da</ta>
            <ta e="T35" id="Seg_3512" s="T34">et-BIT</ta>
            <ta e="T36" id="Seg_3513" s="T35">bu</ta>
            <ta e="T37" id="Seg_3514" s="T36">kihi-ŋ</ta>
            <ta e="T38" id="Seg_3515" s="T37">min</ta>
            <ta e="T39" id="Seg_3516" s="T38">künüs</ta>
            <ta e="T40" id="Seg_3517" s="T39">hɨrɨt-I-BIT-I-m</ta>
            <ta e="T41" id="Seg_3518" s="T40">diː</ta>
            <ta e="T42" id="Seg_3519" s="T41">tu͡ok</ta>
            <ta e="T43" id="Seg_3520" s="T42">kel-IAK.[tA]=Ij</ta>
            <ta e="T44" id="Seg_3521" s="T43">u͡ot-I-m</ta>
            <ta e="T45" id="Seg_3522" s="T44">togo</ta>
            <ta e="T46" id="Seg_3523" s="T45">et-Ar</ta>
            <ta e="T47" id="Seg_3524" s="T46">di͡e-An</ta>
            <ta e="T48" id="Seg_3525" s="T47">kuttan-BAT</ta>
            <ta e="T49" id="Seg_3526" s="T48">hɨt-Ar</ta>
            <ta e="T50" id="Seg_3527" s="T49">Lɨːpɨrdaːn</ta>
            <ta e="T51" id="Seg_3528" s="T50">kel-An</ta>
            <ta e="T52" id="Seg_3529" s="T51">bu</ta>
            <ta e="T53" id="Seg_3530" s="T52">kömüs</ta>
            <ta e="T54" id="Seg_3531" s="T53">ü͡ös-LAːK</ta>
            <ta e="T55" id="Seg_3532" s="T54">haː-LAːK</ta>
            <ta e="T56" id="Seg_3533" s="T55">kihi-nI</ta>
            <ta e="T57" id="Seg_3534" s="T56">ölör-An</ta>
            <ta e="T58" id="Seg_3535" s="T57">keːs-Ar</ta>
            <ta e="T59" id="Seg_3536" s="T58">alaŋaː</ta>
            <ta e="T60" id="Seg_3537" s="T59">haː</ta>
            <ta e="T61" id="Seg_3538" s="T60">bu͡ollaga</ta>
            <ta e="T62" id="Seg_3539" s="T61">diː</ta>
            <ta e="T63" id="Seg_3540" s="T62">kihi-ŋ</ta>
            <ta e="T64" id="Seg_3541" s="T63">dʼaktar-tA</ta>
            <ta e="T65" id="Seg_3542" s="T64">küččügüj</ta>
            <ta e="T66" id="Seg_3543" s="T65">ogo-tI-n</ta>
            <ta e="T67" id="Seg_3544" s="T66">kɨtta</ta>
            <ta e="T68" id="Seg_3545" s="T67">hir-GA</ta>
            <ta e="T69" id="Seg_3546" s="T68">küreː-An</ta>
            <ta e="T70" id="Seg_3547" s="T69">kaːl-Ar</ta>
            <ta e="T71" id="Seg_3548" s="T70">dʼaktar-I-ŋ</ta>
            <ta e="T72" id="Seg_3549" s="T71">tu͡ok-tA</ta>
            <ta e="T73" id="Seg_3550" s="T72">da</ta>
            <ta e="T74" id="Seg_3551" s="T73">hu͡ok</ta>
            <ta e="T75" id="Seg_3552" s="T74">bu͡or</ta>
            <ta e="T76" id="Seg_3553" s="T75">golomo-kAːN-GA</ta>
            <ta e="T77" id="Seg_3554" s="T76">olor-Ar</ta>
            <ta e="T78" id="Seg_3555" s="T77">oloŋko</ta>
            <ta e="T79" id="Seg_3556" s="T78">ogo-tA</ta>
            <ta e="T80" id="Seg_3557" s="T79">ör</ta>
            <ta e="T81" id="Seg_3558" s="T80">bu͡ol-BAT</ta>
            <ta e="T82" id="Seg_3559" s="T81">ulaːt-An</ta>
            <ta e="T83" id="Seg_3560" s="T82">kaːl-Ar</ta>
            <ta e="T84" id="Seg_3561" s="T83">ogo</ta>
            <ta e="T85" id="Seg_3562" s="T84">tagɨs-An</ta>
            <ta e="T86" id="Seg_3563" s="T85">kör-BIT-tA</ta>
            <ta e="T87" id="Seg_3564" s="T86">dʼi͡e-tI-n</ta>
            <ta e="T88" id="Seg_3565" s="T87">ürüt-tI-GAr</ta>
            <ta e="T89" id="Seg_3566" s="T88">čeːlkeː-LAr</ta>
            <ta e="T90" id="Seg_3567" s="T89">hɨrɨt-Ar-LAr</ta>
            <ta e="T91" id="Seg_3568" s="T90">inʼe-tI-GAr</ta>
            <ta e="T92" id="Seg_3569" s="T91">kiːr-An</ta>
            <ta e="T93" id="Seg_3570" s="T92">kepseː-Ar</ta>
            <ta e="T94" id="Seg_3571" s="T93">eː</ta>
            <ta e="T95" id="Seg_3572" s="T94">oččogo</ta>
            <ta e="T96" id="Seg_3573" s="T95">kurpaːskɨ-LAr</ta>
            <ta e="T97" id="Seg_3574" s="T96">bu͡ollaga</ta>
            <ta e="T98" id="Seg_3575" s="T97">diː</ta>
            <ta e="T99" id="Seg_3576" s="T98">di͡e-Ar</ta>
            <ta e="T100" id="Seg_3577" s="T99">inʼe-tA</ta>
            <ta e="T101" id="Seg_3578" s="T100">oː</ta>
            <ta e="T102" id="Seg_3579" s="T101">aga-LAːK-I-m</ta>
            <ta e="T103" id="Seg_3580" s="T102">e-BIT-tA</ta>
            <ta e="T104" id="Seg_3581" s="T103">bu͡ol-TAR</ta>
            <ta e="T105" id="Seg_3582" s="T104">kajdak</ta>
            <ta e="T106" id="Seg_3583" s="T105">ere</ta>
            <ta e="T107" id="Seg_3584" s="T106">gɨn-IAK.[tA]</ta>
            <ta e="T108" id="Seg_3585" s="T107">e-TI-tA</ta>
            <ta e="T109" id="Seg_3586" s="T108">bu͡ollaga</ta>
            <ta e="T110" id="Seg_3587" s="T109">di͡e-Ar</ta>
            <ta e="T111" id="Seg_3588" s="T110">u͡ol</ta>
            <ta e="T112" id="Seg_3589" s="T111">inʼe-tI-GAr</ta>
            <ta e="T113" id="Seg_3590" s="T112">inʼe-tA</ta>
            <ta e="T114" id="Seg_3591" s="T113">u͡ol</ta>
            <ta e="T115" id="Seg_3592" s="T114">aga-LAːK-I-n</ta>
            <ta e="T116" id="Seg_3593" s="T115">tus-tI-nAn</ta>
            <ta e="T117" id="Seg_3594" s="T116">amattan</ta>
            <ta e="T118" id="Seg_3595" s="T117">kepseː-BAT</ta>
            <ta e="T119" id="Seg_3596" s="T118">inʼe</ta>
            <ta e="T120" id="Seg_3597" s="T119">as-tI-n</ta>
            <ta e="T121" id="Seg_3598" s="T120">bɨs-An</ta>
            <ta e="T122" id="Seg_3599" s="T121">mu͡ot</ta>
            <ta e="T123" id="Seg_3600" s="T122">oŋor-Ar</ta>
            <ta e="T124" id="Seg_3601" s="T123">u͡ol</ta>
            <ta e="T125" id="Seg_3602" s="T124">bu-nAn</ta>
            <ta e="T126" id="Seg_3603" s="T125">tuhak</ta>
            <ta e="T127" id="Seg_3604" s="T126">oŋohun-Ar</ta>
            <ta e="T128" id="Seg_3605" s="T127">bu-nAn</ta>
            <ta e="T129" id="Seg_3606" s="T128">kurpaːskɨ-nI</ta>
            <ta e="T130" id="Seg_3607" s="T129">ölör-An</ta>
            <ta e="T131" id="Seg_3608" s="T130">ol-nAn</ta>
            <ta e="T132" id="Seg_3609" s="T131">iːt-LIN-An</ta>
            <ta e="T133" id="Seg_3610" s="T132">kihi</ta>
            <ta e="T134" id="Seg_3611" s="T133">bu͡ol-An</ta>
            <ta e="T135" id="Seg_3612" s="T134">olor-Ar-LAr</ta>
            <ta e="T136" id="Seg_3613" s="T135">bu</ta>
            <ta e="T137" id="Seg_3614" s="T136">olor-An</ta>
            <ta e="T138" id="Seg_3615" s="T137">u͡ol</ta>
            <ta e="T139" id="Seg_3616" s="T138">bi͡ek</ta>
            <ta e="T140" id="Seg_3617" s="T139">ɨjɨt-Ar</ta>
            <ta e="T141" id="Seg_3618" s="T140">min</ta>
            <ta e="T142" id="Seg_3619" s="T141">teːte-LAːK</ta>
            <ta e="T143" id="Seg_3620" s="T142">e-TI-m</ta>
            <ta e="T144" id="Seg_3621" s="T143">inʼe-tA</ta>
            <ta e="T145" id="Seg_3622" s="T144">amattan</ta>
            <ta e="T146" id="Seg_3623" s="T145">haŋar-BAT</ta>
            <ta e="T147" id="Seg_3624" s="T146">e-BIT</ta>
            <ta e="T148" id="Seg_3625" s="T147">kisteː-Ar</ta>
            <ta e="T149" id="Seg_3626" s="T148">e-BIT</ta>
            <ta e="T150" id="Seg_3627" s="T149">Lɨːpɨrdaːn</ta>
            <ta e="T151" id="Seg_3628" s="T150">emi͡e</ta>
            <ta e="T152" id="Seg_3629" s="T151">kihi-nI</ta>
            <ta e="T153" id="Seg_3630" s="T152">ölör-AːrI</ta>
            <ta e="T154" id="Seg_3631" s="T153">hɨrɨt-Ar</ta>
            <ta e="T155" id="Seg_3632" s="T154">biːr</ta>
            <ta e="T156" id="Seg_3633" s="T155">u͡ol-LAːK</ta>
            <ta e="T157" id="Seg_3634" s="T156">emeːksin-GA</ta>
            <ta e="T158" id="Seg_3635" s="T157">kel-Ar</ta>
            <ta e="T159" id="Seg_3636" s="T158">agaj</ta>
            <ta e="T160" id="Seg_3637" s="T159">kör-BIT-tA</ta>
            <ta e="T161" id="Seg_3638" s="T160">hürdeːk</ta>
            <ta e="T162" id="Seg_3639" s="T161">hu͡on</ta>
            <ta e="T163" id="Seg_3640" s="T162">tiːt</ta>
            <ta e="T164" id="Seg_3641" s="T163">mas</ta>
            <ta e="T165" id="Seg_3642" s="T164">tur-Ar</ta>
            <ta e="T166" id="Seg_3643" s="T165">bu</ta>
            <ta e="T167" id="Seg_3644" s="T166">mas-GA</ta>
            <ta e="T168" id="Seg_3645" s="T167">emeːksin</ta>
            <ta e="T169" id="Seg_3646" s="T168">u͡ol-tA</ta>
            <ta e="T170" id="Seg_3647" s="T169">haː-tI-n</ta>
            <ta e="T171" id="Seg_3648" s="T170">iːl-An</ta>
            <ta e="T172" id="Seg_3649" s="T171">keːs-BIT</ta>
            <ta e="T173" id="Seg_3650" s="T172">Lɨːpɨrdaːn</ta>
            <ta e="T174" id="Seg_3651" s="T173">ɨjɨt-Ar</ta>
            <ta e="T175" id="Seg_3652" s="T174">iti</ta>
            <ta e="T176" id="Seg_3653" s="T175">kim</ta>
            <ta e="T177" id="Seg_3654" s="T176">haː-tA=Ij</ta>
            <ta e="T178" id="Seg_3655" s="T177">tu͡ok</ta>
            <ta e="T179" id="Seg_3656" s="T178">kergen-LAːK-GIn</ta>
            <ta e="T180" id="Seg_3657" s="T179">emeːksin</ta>
            <ta e="T181" id="Seg_3658" s="T180">u͡ol-LAːK-BIn</ta>
            <ta e="T182" id="Seg_3659" s="T181">di͡e-Ar</ta>
            <ta e="T183" id="Seg_3660" s="T182">beː</ta>
            <ta e="T184" id="Seg_3661" s="T183">iti</ta>
            <ta e="T185" id="Seg_3662" s="T184">haː-tI-n</ta>
            <ta e="T186" id="Seg_3663" s="T185">togo</ta>
            <ta e="T187" id="Seg_3664" s="T186">keːs-BIT-tA=Ij</ta>
            <ta e="T188" id="Seg_3665" s="T187">keː</ta>
            <ta e="T189" id="Seg_3666" s="T188">iti</ta>
            <ta e="T190" id="Seg_3667" s="T189">haː</ta>
            <ta e="T191" id="Seg_3668" s="T190">küːs-tA</ta>
            <ta e="T192" id="Seg_3669" s="T191">tagɨs-I-BIT</ta>
            <ta e="T193" id="Seg_3670" s="T192">ol</ta>
            <ta e="T194" id="Seg_3671" s="T193">ihin</ta>
            <ta e="T195" id="Seg_3672" s="T194">keːs-BIT-tA</ta>
            <ta e="T196" id="Seg_3673" s="T195">di͡e-Ar</ta>
            <ta e="T197" id="Seg_3674" s="T196">emeːksin</ta>
            <ta e="T198" id="Seg_3675" s="T197">onton</ta>
            <ta e="T199" id="Seg_3676" s="T198">boldok</ta>
            <ta e="T200" id="Seg_3677" s="T199">hɨt-Ar</ta>
            <ta e="T201" id="Seg_3678" s="T200">ulakan</ta>
            <ta e="T202" id="Seg_3679" s="T201">bagajɨ</ta>
            <ta e="T203" id="Seg_3680" s="T202">taːs</ta>
            <ta e="T205" id="Seg_3681" s="T204">bu-nI</ta>
            <ta e="T206" id="Seg_3682" s="T205">emi͡e</ta>
            <ta e="T207" id="Seg_3683" s="T206">ɨjɨt-Ar</ta>
            <ta e="T208" id="Seg_3684" s="T207">bili</ta>
            <ta e="T209" id="Seg_3685" s="T208">kihi</ta>
            <ta e="T210" id="Seg_3686" s="T209">emeːksin</ta>
            <ta e="T211" id="Seg_3687" s="T210">ol-nI</ta>
            <ta e="T212" id="Seg_3688" s="T211">iti</ta>
            <ta e="T213" id="Seg_3689" s="T212">čüŋküj-TAK-InA</ta>
            <ta e="T214" id="Seg_3690" s="T213">möːčük</ta>
            <ta e="T215" id="Seg_3691" s="T214">gɨn-AːččI</ta>
            <ta e="T216" id="Seg_3692" s="T215">di͡e-Ar</ta>
            <ta e="T217" id="Seg_3693" s="T216">ol</ta>
            <ta e="T218" id="Seg_3694" s="T217">kaja-GA</ta>
            <ta e="T219" id="Seg_3695" s="T218">di͡eri</ta>
            <ta e="T220" id="Seg_3696" s="T219">tep-TAK-InA</ta>
            <ta e="T221" id="Seg_3697" s="T220">töttörü</ta>
            <ta e="T222" id="Seg_3698" s="T221">čekenij-An</ta>
            <ta e="T223" id="Seg_3699" s="T222">kel-AːččI</ta>
            <ta e="T224" id="Seg_3700" s="T223">ol</ta>
            <ta e="T225" id="Seg_3701" s="T224">kurduk</ta>
            <ta e="T226" id="Seg_3702" s="T225">oːnnʼoː-AːččI</ta>
            <ta e="T227" id="Seg_3703" s="T226">di͡e-Ar</ta>
            <ta e="T228" id="Seg_3704" s="T227">Lɨːpɨrdaːn</ta>
            <ta e="T229" id="Seg_3705" s="T228">et-Ar</ta>
            <ta e="T230" id="Seg_3706" s="T229">harsɨn</ta>
            <ta e="T231" id="Seg_3707" s="T230">min</ta>
            <ta e="T232" id="Seg_3708" s="T231">töröː-BIT</ta>
            <ta e="T233" id="Seg_3709" s="T232">kün-LAN-A-BIn</ta>
            <ta e="T234" id="Seg_3710" s="T233">onno</ta>
            <ta e="T235" id="Seg_3711" s="T234">u͡ol-I-ŋ</ta>
            <ta e="T236" id="Seg_3712" s="T235">kel-IAK-TIn</ta>
            <ta e="T237" id="Seg_3713" s="T236">bu</ta>
            <ta e="T238" id="Seg_3714" s="T237">u͡ol</ta>
            <ta e="T239" id="Seg_3715" s="T238">dʼi͡e-tI-GAr</ta>
            <ta e="T240" id="Seg_3716" s="T239">kel-BIT-tI-GAr</ta>
            <ta e="T241" id="Seg_3717" s="T240">inʼe-tA</ta>
            <ta e="T242" id="Seg_3718" s="T241">barɨ-tI-n</ta>
            <ta e="T243" id="Seg_3719" s="T242">kepseː-An</ta>
            <ta e="T244" id="Seg_3720" s="T243">bi͡er-Ar</ta>
            <ta e="T245" id="Seg_3721" s="T244">ɨŋɨr-Ar-LAr</ta>
            <ta e="T246" id="Seg_3722" s="T245">di͡e-Ar</ta>
            <ta e="T247" id="Seg_3723" s="T246">dʼe</ta>
            <ta e="T248" id="Seg_3724" s="T247">agaj</ta>
            <ta e="T249" id="Seg_3725" s="T248">bu</ta>
            <ta e="T250" id="Seg_3726" s="T249">kihi-ŋ</ta>
            <ta e="T251" id="Seg_3727" s="T250">Lɨːpɨrdaːn-GA</ta>
            <ta e="T252" id="Seg_3728" s="T251">bar-TI-tA</ta>
            <ta e="T253" id="Seg_3729" s="T252">Lɨːpɨrdaːn</ta>
            <ta e="T254" id="Seg_3730" s="T253">kihi-LAr-tI-GAr</ta>
            <ta e="T255" id="Seg_3731" s="T254">bu</ta>
            <ta e="T256" id="Seg_3732" s="T255">kihi-nI</ta>
            <ta e="T257" id="Seg_3733" s="T256">kürü͡ö-LAː-IAK-BIt</ta>
            <ta e="T258" id="Seg_3734" s="T257">ölör-IAK-BIt</ta>
            <ta e="T259" id="Seg_3735" s="T258">di͡e-An</ta>
            <ta e="T260" id="Seg_3736" s="T259">haŋar-Ar</ta>
            <ta e="T261" id="Seg_3737" s="T260">kel-BIT-tI-GAr</ta>
            <ta e="T262" id="Seg_3738" s="T261">ɨ͡aldʼɨt-LArI-n</ta>
            <ta e="T263" id="Seg_3739" s="T262">kürü͡ö-LAː-An</ta>
            <ta e="T264" id="Seg_3740" s="T263">keːs-Ar-LAr</ta>
            <ta e="T265" id="Seg_3741" s="T264">ölör-AːrI-LAr</ta>
            <ta e="T266" id="Seg_3742" s="T265">hübeleː-A-s-BIT-LAr</ta>
            <ta e="T267" id="Seg_3743" s="T266">bu͡ollaga</ta>
            <ta e="T268" id="Seg_3744" s="T267">Lɨːpɨrdaːn</ta>
            <ta e="T269" id="Seg_3745" s="T268">halɨn-An</ta>
            <ta e="T270" id="Seg_3746" s="T269">dogor-LAr-tI-GAr</ta>
            <ta e="T271" id="Seg_3747" s="T270">haŋar-Ar</ta>
            <ta e="T272" id="Seg_3748" s="T271">bu</ta>
            <ta e="T273" id="Seg_3749" s="T272">tu͡ok</ta>
            <ta e="T274" id="Seg_3750" s="T273">bu͡ol-An-GIt</ta>
            <ta e="T275" id="Seg_3751" s="T274">mus-I-LIN-A-GIt</ta>
            <ta e="T276" id="Seg_3752" s="T275">ɨ͡aldʼɨt</ta>
            <ta e="T277" id="Seg_3753" s="T276">manna</ta>
            <ta e="T278" id="Seg_3754" s="T277">et-Ar</ta>
            <ta e="T279" id="Seg_3755" s="T278">ogonnʼor</ta>
            <ta e="T280" id="Seg_3756" s="T279">en</ta>
            <ta e="T281" id="Seg_3757" s="T280">min-n</ta>
            <ta e="T282" id="Seg_3758" s="T281">ölör-AːrI</ta>
            <ta e="T283" id="Seg_3759" s="T282">gɨn-Ar-GI-n</ta>
            <ta e="T284" id="Seg_3760" s="T283">bil-A-BIn</ta>
            <ta e="T285" id="Seg_3761" s="T284">min</ta>
            <ta e="T286" id="Seg_3762" s="T285">bu</ta>
            <ta e="T287" id="Seg_3763" s="T286">čogotok</ta>
            <ta e="T288" id="Seg_3764" s="T287">bɨhɨja-LAːK-BIn</ta>
            <ta e="T289" id="Seg_3765" s="T288">ol-nAn</ta>
            <ta e="T290" id="Seg_3766" s="T289">noru͡ot-GI-n</ta>
            <ta e="T291" id="Seg_3767" s="T290">biːrde</ta>
            <ta e="T292" id="Seg_3768" s="T291">ergij-IAK-m</ta>
            <ta e="T293" id="Seg_3769" s="T292">e-TI-tA</ta>
            <ta e="T294" id="Seg_3770" s="T293">di͡e-Ar</ta>
            <ta e="T295" id="Seg_3771" s="T294">kaja=kuː</ta>
            <ta e="T296" id="Seg_3772" s="T295">iti</ta>
            <ta e="T297" id="Seg_3773" s="T296">kurduk</ta>
            <ta e="T298" id="Seg_3774" s="T297">gɨn-I-BA</ta>
            <ta e="T299" id="Seg_3775" s="T298">min</ta>
            <ta e="T300" id="Seg_3776" s="T299">kɨːs-BI-n</ta>
            <ta e="T301" id="Seg_3777" s="T300">dʼaktar</ta>
            <ta e="T302" id="Seg_3778" s="T301">gɨn</ta>
            <ta e="T303" id="Seg_3779" s="T302">di͡e-Ar</ta>
            <ta e="T304" id="Seg_3780" s="T303">ɨ͡aldʼɨt-tA</ta>
            <ta e="T305" id="Seg_3781" s="T304">Lɨːpɨrdaːn</ta>
            <ta e="T306" id="Seg_3782" s="T305">kɨːs-tI-n</ta>
            <ta e="T307" id="Seg_3783" s="T306">dʼaktar</ta>
            <ta e="T308" id="Seg_3784" s="T307">gɨn-A</ta>
            <ta e="T309" id="Seg_3785" s="T308">hɨt-Ar</ta>
            <ta e="T310" id="Seg_3786" s="T309">bu</ta>
            <ta e="T311" id="Seg_3787" s="T310">u͡ol</ta>
            <ta e="T312" id="Seg_3788" s="T311">ulaːt-An</ta>
            <ta e="T313" id="Seg_3789" s="T312">baran</ta>
            <ta e="T314" id="Seg_3790" s="T313">teːte-tI-n</ta>
            <ta e="T315" id="Seg_3791" s="T314">ölör-BIT</ta>
            <ta e="T316" id="Seg_3792" s="T315">kihi-nI</ta>
            <ta e="T317" id="Seg_3793" s="T316">bi͡ek</ta>
            <ta e="T318" id="Seg_3794" s="T317">kördöː-Ar</ta>
            <ta e="T319" id="Seg_3795" s="T318">e-BIT</ta>
            <ta e="T320" id="Seg_3796" s="T319">aga-tA</ta>
            <ta e="T321" id="Seg_3797" s="T320">öl-BIT</ta>
            <ta e="T322" id="Seg_3798" s="T321">hir-tI-GAr</ta>
            <ta e="T323" id="Seg_3799" s="T322">ulaːt-An</ta>
            <ta e="T324" id="Seg_3800" s="T323">baran</ta>
            <ta e="T325" id="Seg_3801" s="T324">bi͡ek</ta>
            <ta e="T326" id="Seg_3802" s="T325">hɨrɨt-Ar</ta>
            <ta e="T327" id="Seg_3803" s="T326">e-BIT</ta>
            <ta e="T328" id="Seg_3804" s="T327">agaj</ta>
            <ta e="T329" id="Seg_3805" s="T328">bu</ta>
            <ta e="T330" id="Seg_3806" s="T329">hɨt-TAK-InA</ta>
            <ta e="T331" id="Seg_3807" s="T330">ikki</ta>
            <ta e="T332" id="Seg_3808" s="T331">kihi</ta>
            <ta e="T333" id="Seg_3809" s="T332">is-Ar-LAr</ta>
            <ta e="T334" id="Seg_3810" s="T333">agaj</ta>
            <ta e="T335" id="Seg_3811" s="T334">biːr-LArA</ta>
            <ta e="T336" id="Seg_3812" s="T335">toŋ</ta>
            <ta e="T337" id="Seg_3813" s="T336">ebekeː-nI</ta>
            <ta e="T338" id="Seg_3814" s="T337">tü͡ös-tI-GAr</ta>
            <ta e="T339" id="Seg_3815" s="T338">baːj-An</ta>
            <ta e="T340" id="Seg_3816" s="T339">is-Ar</ta>
            <ta e="T341" id="Seg_3817" s="T340">bu-GA</ta>
            <ta e="T342" id="Seg_3818" s="T341">dogor-tA</ta>
            <ta e="T343" id="Seg_3819" s="T342">di͡e-Ar</ta>
            <ta e="T344" id="Seg_3820" s="T343">manna</ta>
            <ta e="T345" id="Seg_3821" s="T344">kaja</ta>
            <ta e="T346" id="Seg_3822" s="T345">Lɨːpɨrdaːn</ta>
            <ta e="T347" id="Seg_3823" s="T346">kömüs</ta>
            <ta e="T348" id="Seg_3824" s="T347">ü͡ös-LAːK</ta>
            <ta e="T349" id="Seg_3825" s="T348">haː-LAːK</ta>
            <ta e="T350" id="Seg_3826" s="T349">kihi-nI</ta>
            <ta e="T351" id="Seg_3827" s="T350">ölör-BIT-tA</ta>
            <ta e="T352" id="Seg_3828" s="T351">baːr</ta>
            <ta e="T353" id="Seg_3829" s="T352">ol</ta>
            <ta e="T354" id="Seg_3830" s="T353">kihi</ta>
            <ta e="T355" id="Seg_3831" s="T354">u͡ol-tA</ta>
            <ta e="T356" id="Seg_3832" s="T355">di͡e-Ar</ta>
            <ta e="T357" id="Seg_3833" s="T356">u͡ol</ta>
            <ta e="T358" id="Seg_3834" s="T357">onno</ta>
            <ta e="T359" id="Seg_3835" s="T358">dʼe</ta>
            <ta e="T360" id="Seg_3836" s="T359">aga-LAːK-tI-n</ta>
            <ta e="T361" id="Seg_3837" s="T360">ihit-Ar</ta>
            <ta e="T362" id="Seg_3838" s="T361">bu</ta>
            <ta e="T363" id="Seg_3839" s="T362">kihi-LAr-GI-n</ta>
            <ta e="T364" id="Seg_3840" s="T363">dʼe</ta>
            <ta e="T365" id="Seg_3841" s="T364">ü͡ögüleː-Ar</ta>
            <ta e="T366" id="Seg_3842" s="T365">diː</ta>
            <ta e="T367" id="Seg_3843" s="T366">bu</ta>
            <ta e="T368" id="Seg_3844" s="T367">ogo-ŋ</ta>
            <ta e="T369" id="Seg_3845" s="T368">hir-BIt</ta>
            <ta e="T370" id="Seg_3846" s="T369">kuttaː-Ar</ta>
            <ta e="T371" id="Seg_3847" s="T370">di͡e-An</ta>
            <ta e="T372" id="Seg_3848" s="T371">manna</ta>
            <ta e="T373" id="Seg_3849" s="T372">ebe-kAːN-LArI-n</ta>
            <ta e="T374" id="Seg_3850" s="T373">kaːl-TAr-A</ta>
            <ta e="T375" id="Seg_3851" s="T374">ku͡ot-Ar-LAr</ta>
            <ta e="T376" id="Seg_3852" s="T375">u͡ol</ta>
            <ta e="T377" id="Seg_3853" s="T376">ebekeː-nI</ta>
            <ta e="T378" id="Seg_3854" s="T377">kɨtta</ta>
            <ta e="T379" id="Seg_3855" s="T378">bölüːge-tI-n</ta>
            <ta e="T380" id="Seg_3856" s="T379">dʼi͡e-tI-GAr</ta>
            <ta e="T381" id="Seg_3857" s="T380">inʼe-tI-GAr</ta>
            <ta e="T382" id="Seg_3858" s="T381">ilt-A</ta>
            <ta e="T383" id="Seg_3859" s="T382">bar-Ar</ta>
            <ta e="T384" id="Seg_3860" s="T383">bu-LAr-nI</ta>
            <ta e="T385" id="Seg_3861" s="T384">hi͡e-Ar-LAr</ta>
            <ta e="T386" id="Seg_3862" s="T385">bu͡ollaga</ta>
            <ta e="T387" id="Seg_3863" s="T386">inʼe-tA</ta>
            <ta e="T388" id="Seg_3864" s="T387">hemeleː-Ar</ta>
            <ta e="T389" id="Seg_3865" s="T388">bu</ta>
            <ta e="T390" id="Seg_3866" s="T389">tu͡ok</ta>
            <ta e="T391" id="Seg_3867" s="T390">ör</ta>
            <ta e="T392" id="Seg_3868" s="T391">hɨrɨt-A-GIn</ta>
            <ta e="T393" id="Seg_3869" s="T392">ölör-An</ta>
            <ta e="T394" id="Seg_3870" s="T393">keːs-IAK-LArA</ta>
            <ta e="T395" id="Seg_3871" s="T394">u͡ol-tA</ta>
            <ta e="T396" id="Seg_3872" s="T395">inʼe-tI-n</ta>
            <ta e="T397" id="Seg_3873" s="T396">haŋa-tI-n</ta>
            <ta e="T398" id="Seg_3874" s="T397">amattan</ta>
            <ta e="T399" id="Seg_3875" s="T398">ihit-I-BAT</ta>
            <ta e="T400" id="Seg_3876" s="T399">bu</ta>
            <ta e="T401" id="Seg_3877" s="T400">u͡ol</ta>
            <ta e="T402" id="Seg_3878" s="T401">bi͡ek</ta>
            <ta e="T403" id="Seg_3879" s="T402">aga-tI-n</ta>
            <ta e="T404" id="Seg_3880" s="T403">kördöː-Ar</ta>
            <ta e="T405" id="Seg_3881" s="T404">bu</ta>
            <ta e="T406" id="Seg_3882" s="T405">baran</ta>
            <ta e="T407" id="Seg_3883" s="T406">Lɨːpɨrdaːn</ta>
            <ta e="T408" id="Seg_3884" s="T407">ogonnʼor</ta>
            <ta e="T409" id="Seg_3885" s="T408">dʼi͡e-tI-n</ta>
            <ta e="T410" id="Seg_3886" s="T409">dʼe</ta>
            <ta e="T411" id="Seg_3887" s="T410">bul-Ar</ta>
            <ta e="T412" id="Seg_3888" s="T411">bu</ta>
            <ta e="T413" id="Seg_3889" s="T412">bul-An</ta>
            <ta e="T414" id="Seg_3890" s="T413">ki͡eheːŋŋi</ta>
            <ta e="T415" id="Seg_3891" s="T414">uː-nAn</ta>
            <ta e="T416" id="Seg_3892" s="T415">utuj-A</ta>
            <ta e="T417" id="Seg_3893" s="T416">hɨt-TAK-TArInA</ta>
            <ta e="T418" id="Seg_3894" s="T417">kel-Ar</ta>
            <ta e="T419" id="Seg_3895" s="T418">utuj-A</ta>
            <ta e="T420" id="Seg_3896" s="T419">hɨt-Ar</ta>
            <ta e="T421" id="Seg_3897" s="T420">kihi-nI</ta>
            <ta e="T422" id="Seg_3898" s="T421">kustuk-tI-nAn</ta>
            <ta e="T423" id="Seg_3899" s="T422">hu͡on</ta>
            <ta e="T424" id="Seg_3900" s="T423">iŋiːr-LArI-n</ta>
            <ta e="T425" id="Seg_3901" s="T424">bɨhɨt-A</ta>
            <ta e="T426" id="Seg_3902" s="T425">ɨt-IAlAː-Ar</ta>
            <ta e="T427" id="Seg_3903" s="T426">Lɨːpɨrdaːn</ta>
            <ta e="T428" id="Seg_3904" s="T427">kütü͡öt-tI-n</ta>
            <ta e="T429" id="Seg_3905" s="T428">dʼe</ta>
            <ta e="T430" id="Seg_3906" s="T429">bu</ta>
            <ta e="T431" id="Seg_3907" s="T430">kɨlɨn-I-m</ta>
            <ta e="T432" id="Seg_3908" s="T431">kuhagan-tI-GAr</ta>
            <ta e="T433" id="Seg_3909" s="T432">öl-An</ta>
            <ta e="T434" id="Seg_3910" s="T433">er-A-BIn</ta>
            <ta e="T435" id="Seg_3911" s="T434">min</ta>
            <ta e="T436" id="Seg_3912" s="T435">en</ta>
            <ta e="T437" id="Seg_3913" s="T436">aga-GI-n</ta>
            <ta e="T438" id="Seg_3914" s="T437">ölör-BAtAK-I-m</ta>
            <ta e="T439" id="Seg_3915" s="T438">di͡e-Ar</ta>
            <ta e="T440" id="Seg_3916" s="T439">ajɨlaːgɨn</ta>
            <ta e="T441" id="Seg_3917" s="T440">min-n</ta>
            <ta e="T442" id="Seg_3918" s="T441">tɨːn-BI-n</ta>
            <ta e="T443" id="Seg_3919" s="T442">bɨs</ta>
            <ta e="T444" id="Seg_3920" s="T443">anɨ</ta>
            <ta e="T445" id="Seg_3921" s="T444">da</ta>
            <ta e="T446" id="Seg_3922" s="T445">tu͡ok</ta>
            <ta e="T447" id="Seg_3923" s="T446">kihi-tA</ta>
            <ta e="T448" id="Seg_3924" s="T447">bu͡ol-IAK-m=Ij</ta>
            <ta e="T449" id="Seg_3925" s="T448">di͡e-Ar</ta>
            <ta e="T450" id="Seg_3926" s="T449">bu</ta>
            <ta e="T451" id="Seg_3927" s="T450">ogo-ŋ</ta>
            <ta e="T452" id="Seg_3928" s="T451">bu</ta>
            <ta e="T453" id="Seg_3929" s="T452">kihi-nI</ta>
            <ta e="T454" id="Seg_3930" s="T453">tɨːn-tI-n</ta>
            <ta e="T455" id="Seg_3931" s="T454">bɨs-An</ta>
            <ta e="T456" id="Seg_3932" s="T455">keːs-Ar</ta>
            <ta e="T457" id="Seg_3933" s="T456">bu</ta>
            <ta e="T458" id="Seg_3934" s="T457">ogo-ŋ</ta>
            <ta e="T459" id="Seg_3935" s="T458">bu-nI</ta>
            <ta e="T460" id="Seg_3936" s="T459">ihit-Ar</ta>
            <ta e="T461" id="Seg_3937" s="T460">da</ta>
            <ta e="T462" id="Seg_3938" s="T461">Lɨːpɨrdaːn-nI</ta>
            <ta e="T463" id="Seg_3939" s="T462">ölör-An</ta>
            <ta e="T464" id="Seg_3940" s="T463">keːs-Ar</ta>
            <ta e="T465" id="Seg_3941" s="T464">baːj-tI-n-tot-tI-n</ta>
            <ta e="T466" id="Seg_3942" s="T465">dʼi͡e-tI-GAr</ta>
            <ta e="T467" id="Seg_3943" s="T466">ilt-A</ta>
            <ta e="T468" id="Seg_3944" s="T467">bar-Ar</ta>
            <ta e="T469" id="Seg_3945" s="T468">bu</ta>
            <ta e="T470" id="Seg_3946" s="T469">baːj-GI-n</ta>
            <ta e="T471" id="Seg_3947" s="T470">dʼi͡e-tI-GAr</ta>
            <ta e="T472" id="Seg_3948" s="T471">ti͡ert-An</ta>
            <ta e="T473" id="Seg_3949" s="T472">inʼe-tI-n</ta>
            <ta e="T474" id="Seg_3950" s="T473">kɨtta</ta>
            <ta e="T475" id="Seg_3951" s="T474">baːj-An-baːj-An</ta>
            <ta e="T476" id="Seg_3952" s="T475">dʼe</ta>
            <ta e="T477" id="Seg_3953" s="T476">olor-BIT-tA</ta>
            <ta e="T478" id="Seg_3954" s="T477">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_3955" s="T0">Lyypyrdaan.[NOM]</ta>
            <ta e="T2" id="Seg_3956" s="T1">well</ta>
            <ta e="T3" id="Seg_3957" s="T2">live-PRS.[3SG]</ta>
            <ta e="T4" id="Seg_3958" s="T3">Lyypyrdaan.[NOM]</ta>
            <ta e="T5" id="Seg_3959" s="T4">what.kind.of</ta>
            <ta e="T6" id="Seg_3960" s="T5">INDEF</ta>
            <ta e="T7" id="Seg_3961" s="T6">in.the.direction</ta>
            <ta e="T8" id="Seg_3962" s="T7">go-PRS.[3SG]</ta>
            <ta e="T9" id="Seg_3963" s="T8">this</ta>
            <ta e="T10" id="Seg_3964" s="T9">go-CVB.SEQ</ta>
            <ta e="T11" id="Seg_3965" s="T10">woman-PROPR</ta>
            <ta e="T12" id="Seg_3966" s="T11">human.being-ACC</ta>
            <ta e="T13" id="Seg_3967" s="T12">find-PRS.[3SG]</ta>
            <ta e="T14" id="Seg_3968" s="T13">boy-PROPR.[NOM]</ta>
            <ta e="T15" id="Seg_3969" s="T14">that</ta>
            <ta e="T16" id="Seg_3970" s="T15">human.being.[NOM]</ta>
            <ta e="T17" id="Seg_3971" s="T16">woman-PROPR</ta>
            <ta e="T18" id="Seg_3972" s="T17">human.being-2SG.[NOM]</ta>
            <ta e="T19" id="Seg_3973" s="T18">individually</ta>
            <ta e="T20" id="Seg_3974" s="T19">nomadize-PST2.[3SG]</ta>
            <ta e="T21" id="Seg_3975" s="T20">lonely</ta>
            <ta e="T22" id="Seg_3976" s="T21">dawn.[NOM]</ta>
            <ta e="T23" id="Seg_3977" s="T22">be-CVB.SEQ</ta>
            <ta e="T24" id="Seg_3978" s="T23">in.the.evening</ta>
            <ta e="T25" id="Seg_3979" s="T24">one</ta>
            <ta e="T26" id="Seg_3980" s="T25">place-DAT/LOC</ta>
            <ta e="T27" id="Seg_3981" s="T26">fall-PST2.[3SG]</ta>
            <ta e="T28" id="Seg_3982" s="T27">sleep-CVB.PURP</ta>
            <ta e="T29" id="Seg_3983" s="T28">shoes-3SG-ACC</ta>
            <ta e="T30" id="Seg_3984" s="T29">naked-VBZ-CVB.SEQ</ta>
            <ta e="T31" id="Seg_3985" s="T30">be-TEMP-3SG</ta>
            <ta e="T32" id="Seg_3986" s="T31">fire-3SG.[NOM]</ta>
            <ta e="T33" id="Seg_3987" s="T32">how.much-MLTP</ta>
            <ta e="T34" id="Seg_3988" s="T33">INDEF</ta>
            <ta e="T35" id="Seg_3989" s="T34">make.a.noise-PST2.[3SG]</ta>
            <ta e="T36" id="Seg_3990" s="T35">this</ta>
            <ta e="T37" id="Seg_3991" s="T36">human.being-2SG.[NOM]</ta>
            <ta e="T38" id="Seg_3992" s="T37">1SG.[NOM]</ta>
            <ta e="T39" id="Seg_3993" s="T38">by.day</ta>
            <ta e="T40" id="Seg_3994" s="T39">go-EP-PST2-EP-1SG</ta>
            <ta e="T41" id="Seg_3995" s="T40">EMPH</ta>
            <ta e="T42" id="Seg_3996" s="T41">what.[NOM]</ta>
            <ta e="T43" id="Seg_3997" s="T42">come-FUT.[3SG]=Q</ta>
            <ta e="T44" id="Seg_3998" s="T43">fire-EP-1SG.[NOM]</ta>
            <ta e="T45" id="Seg_3999" s="T44">why</ta>
            <ta e="T46" id="Seg_4000" s="T45">speak-PRS.[3SG]</ta>
            <ta e="T47" id="Seg_4001" s="T46">think-CVB.SEQ</ta>
            <ta e="T48" id="Seg_4002" s="T47">be.afraid-NEG.[3SG]</ta>
            <ta e="T49" id="Seg_4003" s="T48">lie.down-PRS.[3SG]</ta>
            <ta e="T50" id="Seg_4004" s="T49">Lyypyrdaan.[NOM]</ta>
            <ta e="T51" id="Seg_4005" s="T50">come-CVB.SEQ</ta>
            <ta e="T52" id="Seg_4006" s="T51">this</ta>
            <ta e="T53" id="Seg_4007" s="T52">silver.[NOM]</ta>
            <ta e="T54" id="Seg_4008" s="T53">fathom-PROPR</ta>
            <ta e="T55" id="Seg_4009" s="T54">bow-PROPR</ta>
            <ta e="T56" id="Seg_4010" s="T55">human.being-ACC</ta>
            <ta e="T57" id="Seg_4011" s="T56">kill-CVB.SEQ</ta>
            <ta e="T58" id="Seg_4012" s="T57">throw-PRS.[3SG]</ta>
            <ta e="T59" id="Seg_4013" s="T58">bow.[NOM]</ta>
            <ta e="T60" id="Seg_4014" s="T59">bow.[NOM]</ta>
            <ta e="T61" id="Seg_4015" s="T60">MOD</ta>
            <ta e="T62" id="Seg_4016" s="T61">EMPH</ta>
            <ta e="T63" id="Seg_4017" s="T62">human.being-2SG.[NOM]</ta>
            <ta e="T64" id="Seg_4018" s="T63">woman-3SG.[NOM]</ta>
            <ta e="T65" id="Seg_4019" s="T64">small</ta>
            <ta e="T66" id="Seg_4020" s="T65">child-3SG-ACC</ta>
            <ta e="T67" id="Seg_4021" s="T66">with</ta>
            <ta e="T68" id="Seg_4022" s="T67">place-DAT/LOC</ta>
            <ta e="T69" id="Seg_4023" s="T68">escape-CVB.SEQ</ta>
            <ta e="T70" id="Seg_4024" s="T69">stay-PRS.[3SG]</ta>
            <ta e="T71" id="Seg_4025" s="T70">woman-EP-2SG.[NOM]</ta>
            <ta e="T72" id="Seg_4026" s="T71">what-POSS</ta>
            <ta e="T73" id="Seg_4027" s="T72">NEG</ta>
            <ta e="T74" id="Seg_4028" s="T73">NEG.[3SG]</ta>
            <ta e="T75" id="Seg_4029" s="T74">earth.[NOM]</ta>
            <ta e="T76" id="Seg_4030" s="T75">pit_dwelling-DIM-DAT/LOC</ta>
            <ta e="T77" id="Seg_4031" s="T76">live-PRS.[3SG]</ta>
            <ta e="T78" id="Seg_4032" s="T77">tale.[NOM]</ta>
            <ta e="T79" id="Seg_4033" s="T78">child-3SG.[NOM]</ta>
            <ta e="T80" id="Seg_4034" s="T79">long</ta>
            <ta e="T81" id="Seg_4035" s="T80">be-NEG.[3SG]</ta>
            <ta e="T82" id="Seg_4036" s="T81">grow-CVB.SEQ</ta>
            <ta e="T83" id="Seg_4037" s="T82">stay-PRS.[3SG]</ta>
            <ta e="T84" id="Seg_4038" s="T83">child.[NOM]</ta>
            <ta e="T85" id="Seg_4039" s="T84">go.out-CVB.SEQ</ta>
            <ta e="T86" id="Seg_4040" s="T85">see-PST2-3SG</ta>
            <ta e="T87" id="Seg_4041" s="T86">house-3SG-GEN</ta>
            <ta e="T88" id="Seg_4042" s="T87">upper.part-3SG-DAT/LOC</ta>
            <ta e="T89" id="Seg_4043" s="T88">white-PL.[NOM]</ta>
            <ta e="T90" id="Seg_4044" s="T89">go-PRS-3PL</ta>
            <ta e="T91" id="Seg_4045" s="T90">mother-3SG-DAT/LOC</ta>
            <ta e="T92" id="Seg_4046" s="T91">go.in-CVB.SEQ</ta>
            <ta e="T93" id="Seg_4047" s="T92">tell-PRS.[3SG]</ta>
            <ta e="T94" id="Seg_4048" s="T93">hey</ta>
            <ta e="T95" id="Seg_4049" s="T94">then</ta>
            <ta e="T96" id="Seg_4050" s="T95">partridge-PL.[NOM]</ta>
            <ta e="T97" id="Seg_4051" s="T96">MOD</ta>
            <ta e="T98" id="Seg_4052" s="T97">EMPH</ta>
            <ta e="T99" id="Seg_4053" s="T98">say-PRS.[3SG]</ta>
            <ta e="T100" id="Seg_4054" s="T99">mother-3SG.[NOM]</ta>
            <ta e="T101" id="Seg_4055" s="T100">oh</ta>
            <ta e="T102" id="Seg_4056" s="T101">father-PROPR-EP-1SG.[NOM]</ta>
            <ta e="T103" id="Seg_4057" s="T102">be-PST2-3SG</ta>
            <ta e="T104" id="Seg_4058" s="T103">be-COND.[3SG]</ta>
            <ta e="T105" id="Seg_4059" s="T104">how</ta>
            <ta e="T106" id="Seg_4060" s="T105">INDEF</ta>
            <ta e="T107" id="Seg_4061" s="T106">make-FUT.[3SG]</ta>
            <ta e="T108" id="Seg_4062" s="T107">be-PST1-3SG</ta>
            <ta e="T109" id="Seg_4063" s="T108">MOD</ta>
            <ta e="T110" id="Seg_4064" s="T109">say-PRS.[3SG]</ta>
            <ta e="T111" id="Seg_4065" s="T110">boy.[NOM]</ta>
            <ta e="T112" id="Seg_4066" s="T111">mother-3SG-DAT/LOC</ta>
            <ta e="T113" id="Seg_4067" s="T112">mother-3SG.[NOM]</ta>
            <ta e="T114" id="Seg_4068" s="T113">son.[NOM]</ta>
            <ta e="T115" id="Seg_4069" s="T114">father-PROPR-EP-GEN</ta>
            <ta e="T116" id="Seg_4070" s="T115">side-3SG-INSTR</ta>
            <ta e="T117" id="Seg_4071" s="T116">at.all</ta>
            <ta e="T118" id="Seg_4072" s="T117">tell-NEG.[3SG]</ta>
            <ta e="T119" id="Seg_4073" s="T118">mother.[NOM]</ta>
            <ta e="T120" id="Seg_4074" s="T119">hair-3SG-ACC</ta>
            <ta e="T121" id="Seg_4075" s="T120">cut-CVB.SEQ</ta>
            <ta e="T122" id="Seg_4076" s="T121">thread.[NOM]</ta>
            <ta e="T123" id="Seg_4077" s="T122">make-PRS.[3SG]</ta>
            <ta e="T124" id="Seg_4078" s="T123">boy.[NOM]</ta>
            <ta e="T125" id="Seg_4079" s="T124">this-INSTR</ta>
            <ta e="T126" id="Seg_4080" s="T125">catching.loop.[NOM]</ta>
            <ta e="T127" id="Seg_4081" s="T126">make-PRS.[3SG]</ta>
            <ta e="T128" id="Seg_4082" s="T127">this-INSTR</ta>
            <ta e="T129" id="Seg_4083" s="T128">partridge-ACC</ta>
            <ta e="T130" id="Seg_4084" s="T129">kill-CVB.SEQ</ta>
            <ta e="T131" id="Seg_4085" s="T130">that-INSTR</ta>
            <ta e="T132" id="Seg_4086" s="T131">feed-PASS/REFL-CVB.SEQ</ta>
            <ta e="T133" id="Seg_4087" s="T132">human.being.[NOM]</ta>
            <ta e="T134" id="Seg_4088" s="T133">be-CVB.SEQ</ta>
            <ta e="T135" id="Seg_4089" s="T134">live-PRS-3PL</ta>
            <ta e="T136" id="Seg_4090" s="T135">this</ta>
            <ta e="T137" id="Seg_4091" s="T136">live-CVB.SEQ</ta>
            <ta e="T138" id="Seg_4092" s="T137">boy.[NOM]</ta>
            <ta e="T139" id="Seg_4093" s="T138">always</ta>
            <ta e="T140" id="Seg_4094" s="T139">ask-PRS.[3SG]</ta>
            <ta e="T141" id="Seg_4095" s="T140">1SG.[NOM]</ta>
            <ta e="T142" id="Seg_4096" s="T141">father-PROPR.[NOM]</ta>
            <ta e="T143" id="Seg_4097" s="T142">be-PST1-1SG</ta>
            <ta e="T144" id="Seg_4098" s="T143">mother-3SG.[NOM]</ta>
            <ta e="T145" id="Seg_4099" s="T144">at.all</ta>
            <ta e="T146" id="Seg_4100" s="T145">say-NEG.PTCP</ta>
            <ta e="T147" id="Seg_4101" s="T146">be-PST2.[3SG]</ta>
            <ta e="T148" id="Seg_4102" s="T147">hide-PTCP.PRS</ta>
            <ta e="T149" id="Seg_4103" s="T148">be-PST2.[3SG]</ta>
            <ta e="T150" id="Seg_4104" s="T149">Lyypyrdaan.[NOM]</ta>
            <ta e="T151" id="Seg_4105" s="T150">again</ta>
            <ta e="T152" id="Seg_4106" s="T151">human.being-ACC</ta>
            <ta e="T153" id="Seg_4107" s="T152">kill-CVB.PURP</ta>
            <ta e="T154" id="Seg_4108" s="T153">go-PRS.[3SG]</ta>
            <ta e="T155" id="Seg_4109" s="T154">one</ta>
            <ta e="T156" id="Seg_4110" s="T155">son-PROPR</ta>
            <ta e="T157" id="Seg_4111" s="T156">old.woman-DAT/LOC</ta>
            <ta e="T158" id="Seg_4112" s="T157">come-PRS.[3SG]</ta>
            <ta e="T159" id="Seg_4113" s="T158">suddenly</ta>
            <ta e="T160" id="Seg_4114" s="T159">see-PST2-3SG</ta>
            <ta e="T161" id="Seg_4115" s="T160">very</ta>
            <ta e="T162" id="Seg_4116" s="T161">fat</ta>
            <ta e="T163" id="Seg_4117" s="T162">larch.[NOM]</ta>
            <ta e="T164" id="Seg_4118" s="T163">tree.[NOM]</ta>
            <ta e="T165" id="Seg_4119" s="T164">stand-PRS.[3SG]</ta>
            <ta e="T166" id="Seg_4120" s="T165">this</ta>
            <ta e="T167" id="Seg_4121" s="T166">tree-DAT/LOC</ta>
            <ta e="T168" id="Seg_4122" s="T167">old.woman.[NOM]</ta>
            <ta e="T169" id="Seg_4123" s="T168">son-3SG.[NOM]</ta>
            <ta e="T170" id="Seg_4124" s="T169">bow-3SG-ACC</ta>
            <ta e="T171" id="Seg_4125" s="T170">put.on-CVB.SEQ</ta>
            <ta e="T172" id="Seg_4126" s="T171">throw-PST2.[3SG]</ta>
            <ta e="T173" id="Seg_4127" s="T172">Lyypyrdaan.[NOM]</ta>
            <ta e="T174" id="Seg_4128" s="T173">ask-PRS.[3SG]</ta>
            <ta e="T175" id="Seg_4129" s="T174">that.[NOM]</ta>
            <ta e="T176" id="Seg_4130" s="T175">who.[NOM]</ta>
            <ta e="T177" id="Seg_4131" s="T176">bow-3SG.[NOM]=Q</ta>
            <ta e="T178" id="Seg_4132" s="T177">what</ta>
            <ta e="T179" id="Seg_4133" s="T178">family-PROPR-2SG</ta>
            <ta e="T180" id="Seg_4134" s="T179">old.woman.[NOM]</ta>
            <ta e="T181" id="Seg_4135" s="T180">boy-PROPR-1SG</ta>
            <ta e="T182" id="Seg_4136" s="T181">say-PRS.[3SG]</ta>
            <ta e="T183" id="Seg_4137" s="T182">INTJ</ta>
            <ta e="T184" id="Seg_4138" s="T183">that</ta>
            <ta e="T185" id="Seg_4139" s="T184">bow-3SG-ACC</ta>
            <ta e="T186" id="Seg_4140" s="T185">why</ta>
            <ta e="T187" id="Seg_4141" s="T186">let-PST2-3SG=Q</ta>
            <ta e="T188" id="Seg_4142" s="T187">EMPH</ta>
            <ta e="T189" id="Seg_4143" s="T188">that.[NOM]</ta>
            <ta e="T190" id="Seg_4144" s="T189">bow.[NOM]</ta>
            <ta e="T191" id="Seg_4145" s="T190">power-3SG.[NOM]</ta>
            <ta e="T192" id="Seg_4146" s="T191">go.out-EP-PST2.[3SG]</ta>
            <ta e="T193" id="Seg_4147" s="T192">that.[NOM]</ta>
            <ta e="T194" id="Seg_4148" s="T193">because.of</ta>
            <ta e="T195" id="Seg_4149" s="T194">let-PST2-3SG</ta>
            <ta e="T196" id="Seg_4150" s="T195">say-PRS.[3SG]</ta>
            <ta e="T197" id="Seg_4151" s="T196">old.woman.[NOM]</ta>
            <ta e="T198" id="Seg_4152" s="T197">then</ta>
            <ta e="T199" id="Seg_4153" s="T198">big.stone.[NOM]</ta>
            <ta e="T200" id="Seg_4154" s="T199">lie-PRS.[3SG]</ta>
            <ta e="T201" id="Seg_4155" s="T200">big</ta>
            <ta e="T202" id="Seg_4156" s="T201">very</ta>
            <ta e="T203" id="Seg_4157" s="T202">stone.[NOM]</ta>
            <ta e="T205" id="Seg_4158" s="T204">this-ACC</ta>
            <ta e="T206" id="Seg_4159" s="T205">also</ta>
            <ta e="T207" id="Seg_4160" s="T206">ask-PRS.[3SG]</ta>
            <ta e="T208" id="Seg_4161" s="T207">exactly.this</ta>
            <ta e="T209" id="Seg_4162" s="T208">human.being.[NOM]</ta>
            <ta e="T210" id="Seg_4163" s="T209">old.woman.[NOM]</ta>
            <ta e="T211" id="Seg_4164" s="T210">that-ACC</ta>
            <ta e="T212" id="Seg_4165" s="T211">that.[NOM]</ta>
            <ta e="T213" id="Seg_4166" s="T212">be.bored-TEMP-3SG</ta>
            <ta e="T214" id="Seg_4167" s="T213">ball.[NOM]</ta>
            <ta e="T215" id="Seg_4168" s="T214">make-HAB.[3SG]</ta>
            <ta e="T216" id="Seg_4169" s="T215">say-PRS.[3SG]</ta>
            <ta e="T217" id="Seg_4170" s="T216">that</ta>
            <ta e="T218" id="Seg_4171" s="T217">mountain-DAT/LOC</ta>
            <ta e="T219" id="Seg_4172" s="T218">until</ta>
            <ta e="T220" id="Seg_4173" s="T219">kick-TEMP-3SG</ta>
            <ta e="T221" id="Seg_4174" s="T220">back</ta>
            <ta e="T222" id="Seg_4175" s="T221">roll-CVB.SEQ</ta>
            <ta e="T223" id="Seg_4176" s="T222">come-HAB.[3SG]</ta>
            <ta e="T224" id="Seg_4177" s="T223">that.[NOM]</ta>
            <ta e="T225" id="Seg_4178" s="T224">like</ta>
            <ta e="T226" id="Seg_4179" s="T225">play-HAB.[3SG]</ta>
            <ta e="T227" id="Seg_4180" s="T226">say-PRS.[3SG]</ta>
            <ta e="T228" id="Seg_4181" s="T227">Lyypyrdaan.[NOM]</ta>
            <ta e="T229" id="Seg_4182" s="T228">speak-PRS.[3SG]</ta>
            <ta e="T230" id="Seg_4183" s="T229">tomorrow</ta>
            <ta e="T231" id="Seg_4184" s="T230">1SG.[NOM]</ta>
            <ta e="T232" id="Seg_4185" s="T231">be.born-PTCP.PST</ta>
            <ta e="T233" id="Seg_4186" s="T232">day-VBZ-PRS-1SG</ta>
            <ta e="T234" id="Seg_4187" s="T233">there</ta>
            <ta e="T235" id="Seg_4188" s="T234">boy-EP-2SG.[NOM]</ta>
            <ta e="T236" id="Seg_4189" s="T235">come-FUT-IMP.3SG</ta>
            <ta e="T237" id="Seg_4190" s="T236">this</ta>
            <ta e="T238" id="Seg_4191" s="T237">son.[NOM]</ta>
            <ta e="T239" id="Seg_4192" s="T238">house-3SG-DAT/LOC</ta>
            <ta e="T240" id="Seg_4193" s="T239">come-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T241" id="Seg_4194" s="T240">mother-3SG.[NOM]</ta>
            <ta e="T242" id="Seg_4195" s="T241">whole-3SG-ACC</ta>
            <ta e="T243" id="Seg_4196" s="T242">tell-CVB.SEQ</ta>
            <ta e="T244" id="Seg_4197" s="T243">give-PRS.[3SG]</ta>
            <ta e="T245" id="Seg_4198" s="T244">invite-PRS-3PL</ta>
            <ta e="T246" id="Seg_4199" s="T245">say-PRS.[3SG]</ta>
            <ta e="T247" id="Seg_4200" s="T246">well</ta>
            <ta e="T248" id="Seg_4201" s="T247">only</ta>
            <ta e="T249" id="Seg_4202" s="T248">this</ta>
            <ta e="T250" id="Seg_4203" s="T249">human.being-2SG.[NOM]</ta>
            <ta e="T251" id="Seg_4204" s="T250">Lyypyrdaan-DAT/LOC</ta>
            <ta e="T252" id="Seg_4205" s="T251">go-PST1-3SG</ta>
            <ta e="T253" id="Seg_4206" s="T252">Lyypyrdaan.[NOM]</ta>
            <ta e="T254" id="Seg_4207" s="T253">human.being-PL-3SG-DAT/LOC</ta>
            <ta e="T255" id="Seg_4208" s="T254">this</ta>
            <ta e="T256" id="Seg_4209" s="T255">human.being-ACC</ta>
            <ta e="T257" id="Seg_4210" s="T256">enclosure-VBZ-FUT-1PL</ta>
            <ta e="T258" id="Seg_4211" s="T257">kill-FUT-1PL</ta>
            <ta e="T259" id="Seg_4212" s="T258">say-CVB.SEQ</ta>
            <ta e="T260" id="Seg_4213" s="T259">speak-PRS.[3SG]</ta>
            <ta e="T261" id="Seg_4214" s="T260">come-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T262" id="Seg_4215" s="T261">guest-3PL-ACC</ta>
            <ta e="T263" id="Seg_4216" s="T262">enclosure-VBZ-CVB.SEQ</ta>
            <ta e="T264" id="Seg_4217" s="T263">throw-PRS-3PL</ta>
            <ta e="T265" id="Seg_4218" s="T264">kill-CVB.PURP-3PL</ta>
            <ta e="T266" id="Seg_4219" s="T265">advise-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T267" id="Seg_4220" s="T266">MOD</ta>
            <ta e="T268" id="Seg_4221" s="T267">Lyypyrdaan.[NOM]</ta>
            <ta e="T269" id="Seg_4222" s="T268">fear-CVB.SEQ</ta>
            <ta e="T270" id="Seg_4223" s="T269">friend-PL-3SG-DAT/LOC</ta>
            <ta e="T271" id="Seg_4224" s="T270">speak-PRS.[3SG]</ta>
            <ta e="T272" id="Seg_4225" s="T271">this</ta>
            <ta e="T273" id="Seg_4226" s="T272">what.[NOM]</ta>
            <ta e="T274" id="Seg_4227" s="T273">be-CVB.SEQ-2PL</ta>
            <ta e="T275" id="Seg_4228" s="T274">gather-EP-PASS/REFL-PRS-2PL</ta>
            <ta e="T276" id="Seg_4229" s="T275">guest.[NOM]</ta>
            <ta e="T277" id="Seg_4230" s="T276">here</ta>
            <ta e="T278" id="Seg_4231" s="T277">speak-PRS.[3SG]</ta>
            <ta e="T279" id="Seg_4232" s="T278">old.man.[NOM]</ta>
            <ta e="T280" id="Seg_4233" s="T279">2SG.[NOM]</ta>
            <ta e="T281" id="Seg_4234" s="T280">1SG-ACC</ta>
            <ta e="T282" id="Seg_4235" s="T281">kill-CVB.PURP</ta>
            <ta e="T283" id="Seg_4236" s="T282">want-PTCP.PRS-2SG-ACC</ta>
            <ta e="T284" id="Seg_4237" s="T283">know-PRS-1SG</ta>
            <ta e="T285" id="Seg_4238" s="T284">1SG.[NOM]</ta>
            <ta e="T286" id="Seg_4239" s="T285">this</ta>
            <ta e="T287" id="Seg_4240" s="T286">lonely</ta>
            <ta e="T288" id="Seg_4241" s="T287">small.knife-PROPR-1SG</ta>
            <ta e="T289" id="Seg_4242" s="T288">that-INSTR</ta>
            <ta e="T290" id="Seg_4243" s="T289">people-2SG-ACC</ta>
            <ta e="T291" id="Seg_4244" s="T290">once</ta>
            <ta e="T292" id="Seg_4245" s="T291">extinguish-FUT-1SG</ta>
            <ta e="T293" id="Seg_4246" s="T292">be-PST1-3SG</ta>
            <ta e="T294" id="Seg_4247" s="T293">say-PRS.[3SG]</ta>
            <ta e="T295" id="Seg_4248" s="T294">well=EMPH</ta>
            <ta e="T296" id="Seg_4249" s="T295">that.[NOM]</ta>
            <ta e="T297" id="Seg_4250" s="T296">like</ta>
            <ta e="T298" id="Seg_4251" s="T297">make-EP-NEG.[IMP.2SG]</ta>
            <ta e="T299" id="Seg_4252" s="T298">1SG.[NOM]</ta>
            <ta e="T300" id="Seg_4253" s="T299">daughter-1SG-ACC</ta>
            <ta e="T301" id="Seg_4254" s="T300">woman.[NOM]</ta>
            <ta e="T302" id="Seg_4255" s="T301">make.[IMP.2SG]</ta>
            <ta e="T303" id="Seg_4256" s="T302">say-PRS.[3SG]</ta>
            <ta e="T304" id="Seg_4257" s="T303">guest-3SG.[NOM]</ta>
            <ta e="T305" id="Seg_4258" s="T304">Lyypyrdaan.[NOM]</ta>
            <ta e="T306" id="Seg_4259" s="T305">daughter-3SG-ACC</ta>
            <ta e="T307" id="Seg_4260" s="T306">woman.[NOM]</ta>
            <ta e="T308" id="Seg_4261" s="T307">make-CVB.SIM</ta>
            <ta e="T309" id="Seg_4262" s="T308">lie-PRS.[3SG]</ta>
            <ta e="T310" id="Seg_4263" s="T309">this</ta>
            <ta e="T311" id="Seg_4264" s="T310">boy.[NOM]</ta>
            <ta e="T312" id="Seg_4265" s="T311">grow-CVB.SEQ</ta>
            <ta e="T313" id="Seg_4266" s="T312">after</ta>
            <ta e="T314" id="Seg_4267" s="T313">father-3SG-ACC</ta>
            <ta e="T315" id="Seg_4268" s="T314">kill-PTCP.PST</ta>
            <ta e="T316" id="Seg_4269" s="T315">human.being-ACC</ta>
            <ta e="T317" id="Seg_4270" s="T316">always</ta>
            <ta e="T318" id="Seg_4271" s="T317">search-PTCP.PRS</ta>
            <ta e="T319" id="Seg_4272" s="T318">be-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_4273" s="T319">father-3SG.[NOM]</ta>
            <ta e="T321" id="Seg_4274" s="T320">die-PTCP.PST</ta>
            <ta e="T322" id="Seg_4275" s="T321">place-3SG-DAT/LOC</ta>
            <ta e="T323" id="Seg_4276" s="T322">grow-CVB.SEQ</ta>
            <ta e="T324" id="Seg_4277" s="T323">after</ta>
            <ta e="T325" id="Seg_4278" s="T324">always</ta>
            <ta e="T326" id="Seg_4279" s="T325">go-PTCP.PRS</ta>
            <ta e="T327" id="Seg_4280" s="T326">be-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_4281" s="T327">only</ta>
            <ta e="T329" id="Seg_4282" s="T328">this</ta>
            <ta e="T330" id="Seg_4283" s="T329">lie-TEMP-3SG</ta>
            <ta e="T331" id="Seg_4284" s="T330">two</ta>
            <ta e="T332" id="Seg_4285" s="T331">human.being.[NOM]</ta>
            <ta e="T333" id="Seg_4286" s="T332">go-PRS-3PL</ta>
            <ta e="T334" id="Seg_4287" s="T333">only</ta>
            <ta e="T335" id="Seg_4288" s="T334">one-3PL.[NOM]</ta>
            <ta e="T336" id="Seg_4289" s="T335">frozen</ta>
            <ta e="T337" id="Seg_4290" s="T336">bear-ACC</ta>
            <ta e="T338" id="Seg_4291" s="T337">breast-3SG-DAT/LOC</ta>
            <ta e="T339" id="Seg_4292" s="T338">tie-CVB.SEQ</ta>
            <ta e="T340" id="Seg_4293" s="T339">go-PRS.[3SG]</ta>
            <ta e="T341" id="Seg_4294" s="T340">this-DAT/LOC</ta>
            <ta e="T342" id="Seg_4295" s="T341">friend-3SG.[NOM]</ta>
            <ta e="T343" id="Seg_4296" s="T342">say-PRS.[3SG]</ta>
            <ta e="T344" id="Seg_4297" s="T343">here</ta>
            <ta e="T345" id="Seg_4298" s="T344">well</ta>
            <ta e="T346" id="Seg_4299" s="T345">Lyypyrdaan.[NOM]</ta>
            <ta e="T347" id="Seg_4300" s="T346">silver</ta>
            <ta e="T348" id="Seg_4301" s="T347">fathom-PROPR</ta>
            <ta e="T349" id="Seg_4302" s="T348">bow-PROPR</ta>
            <ta e="T350" id="Seg_4303" s="T349">human.being-ACC</ta>
            <ta e="T351" id="Seg_4304" s="T350">kill-PST2-3SG</ta>
            <ta e="T352" id="Seg_4305" s="T351">there.is</ta>
            <ta e="T353" id="Seg_4306" s="T352">that</ta>
            <ta e="T354" id="Seg_4307" s="T353">human.being.[NOM]</ta>
            <ta e="T355" id="Seg_4308" s="T354">son-3SG.[NOM]</ta>
            <ta e="T356" id="Seg_4309" s="T355">say-PRS.[3SG]</ta>
            <ta e="T357" id="Seg_4310" s="T356">boy.[NOM]</ta>
            <ta e="T358" id="Seg_4311" s="T357">there</ta>
            <ta e="T359" id="Seg_4312" s="T358">well</ta>
            <ta e="T360" id="Seg_4313" s="T359">father-PROPR-3SG-ACC</ta>
            <ta e="T361" id="Seg_4314" s="T360">hear-PRS.[3SG]</ta>
            <ta e="T362" id="Seg_4315" s="T361">this</ta>
            <ta e="T363" id="Seg_4316" s="T362">human.being-PL-2SG-ACC</ta>
            <ta e="T364" id="Seg_4317" s="T363">well</ta>
            <ta e="T365" id="Seg_4318" s="T364">shout-PRS.[3SG]</ta>
            <ta e="T366" id="Seg_4319" s="T365">EMPH</ta>
            <ta e="T367" id="Seg_4320" s="T366">this</ta>
            <ta e="T368" id="Seg_4321" s="T367">child-2SG.[NOM]</ta>
            <ta e="T369" id="Seg_4322" s="T368">place-1PL.[NOM]</ta>
            <ta e="T370" id="Seg_4323" s="T369">scare-PRS.[3SG]</ta>
            <ta e="T371" id="Seg_4324" s="T370">think-CVB.SEQ</ta>
            <ta e="T372" id="Seg_4325" s="T371">here</ta>
            <ta e="T373" id="Seg_4326" s="T372">bear-DIM-3PL-ACC</ta>
            <ta e="T374" id="Seg_4327" s="T373">stay-CAUS-CVB.SIM</ta>
            <ta e="T375" id="Seg_4328" s="T374">go.away-PRS-3PL</ta>
            <ta e="T376" id="Seg_4329" s="T375">boy.[NOM]</ta>
            <ta e="T377" id="Seg_4330" s="T376">bear-ACC</ta>
            <ta e="T378" id="Seg_4331" s="T377">with</ta>
            <ta e="T379" id="Seg_4332" s="T378">beluga-3SG-ACC</ta>
            <ta e="T380" id="Seg_4333" s="T379">house-3SG-DAT/LOC</ta>
            <ta e="T381" id="Seg_4334" s="T380">mother-3SG-DAT/LOC</ta>
            <ta e="T382" id="Seg_4335" s="T381">carry-CVB.SIM</ta>
            <ta e="T383" id="Seg_4336" s="T382">go-PRS.[3SG]</ta>
            <ta e="T384" id="Seg_4337" s="T383">this-PL-ACC</ta>
            <ta e="T385" id="Seg_4338" s="T384">eat-PRS-3PL</ta>
            <ta e="T386" id="Seg_4339" s="T385">MOD</ta>
            <ta e="T387" id="Seg_4340" s="T386">mother-3SG.[NOM]</ta>
            <ta e="T388" id="Seg_4341" s="T387">rebuke-PRS.[3SG]</ta>
            <ta e="T389" id="Seg_4342" s="T388">this</ta>
            <ta e="T390" id="Seg_4343" s="T389">what.[NOM]</ta>
            <ta e="T391" id="Seg_4344" s="T390">long</ta>
            <ta e="T392" id="Seg_4345" s="T391">go-PRS-2SG</ta>
            <ta e="T393" id="Seg_4346" s="T392">kill-CVB.SEQ</ta>
            <ta e="T394" id="Seg_4347" s="T393">throw-FUT-3PL</ta>
            <ta e="T395" id="Seg_4348" s="T394">son-3SG.[NOM]</ta>
            <ta e="T396" id="Seg_4349" s="T395">mother-3SG-GEN</ta>
            <ta e="T397" id="Seg_4350" s="T396">word-3SG-ACC</ta>
            <ta e="T398" id="Seg_4351" s="T397">at.all</ta>
            <ta e="T399" id="Seg_4352" s="T398">hear-EP-NEG.[3SG]</ta>
            <ta e="T400" id="Seg_4353" s="T399">this</ta>
            <ta e="T401" id="Seg_4354" s="T400">boy.[NOM]</ta>
            <ta e="T402" id="Seg_4355" s="T401">always</ta>
            <ta e="T403" id="Seg_4356" s="T402">father-3SG-ACC</ta>
            <ta e="T404" id="Seg_4357" s="T403">search-PRS.[3SG]</ta>
            <ta e="T405" id="Seg_4358" s="T404">this.[NOM]</ta>
            <ta e="T406" id="Seg_4359" s="T405">after</ta>
            <ta e="T407" id="Seg_4360" s="T406">Lyypyrdaan.[NOM]</ta>
            <ta e="T408" id="Seg_4361" s="T407">old.man.[NOM]</ta>
            <ta e="T409" id="Seg_4362" s="T408">house-3SG-ACC</ta>
            <ta e="T410" id="Seg_4363" s="T409">well</ta>
            <ta e="T411" id="Seg_4364" s="T410">find-PRS.[3SG]</ta>
            <ta e="T412" id="Seg_4365" s="T411">this</ta>
            <ta e="T413" id="Seg_4366" s="T412">find-CVB.SEQ</ta>
            <ta e="T414" id="Seg_4367" s="T413">evening</ta>
            <ta e="T415" id="Seg_4368" s="T414">sleep-INSTR</ta>
            <ta e="T416" id="Seg_4369" s="T415">sleep-CVB.SIM</ta>
            <ta e="T417" id="Seg_4370" s="T416">lie-TEMP-3PL</ta>
            <ta e="T418" id="Seg_4371" s="T417">come-PRS.[3SG]</ta>
            <ta e="T419" id="Seg_4372" s="T418">sleep-CVB.SIM</ta>
            <ta e="T420" id="Seg_4373" s="T419">lie-PTCP.PRS</ta>
            <ta e="T421" id="Seg_4374" s="T420">human.being-ACC</ta>
            <ta e="T422" id="Seg_4375" s="T421">arrow-3SG-INSTR</ta>
            <ta e="T423" id="Seg_4376" s="T422">fat</ta>
            <ta e="T424" id="Seg_4377" s="T423">sinew-3PL-ACC</ta>
            <ta e="T425" id="Seg_4378" s="T424">cut-CVB.SIM</ta>
            <ta e="T426" id="Seg_4379" s="T425">shoot-FREQ-PRS.[3SG]</ta>
            <ta e="T427" id="Seg_4380" s="T426">Lyypyrdaan.[NOM]</ta>
            <ta e="T428" id="Seg_4381" s="T427">son_in_law-3SG-ACC</ta>
            <ta e="T429" id="Seg_4382" s="T428">well</ta>
            <ta e="T430" id="Seg_4383" s="T429">this</ta>
            <ta e="T431" id="Seg_4384" s="T430">father_in_law-EP-1SG.[NOM]</ta>
            <ta e="T432" id="Seg_4385" s="T431">bad-3SG-DAT/LOC</ta>
            <ta e="T433" id="Seg_4386" s="T432">die-CVB.SEQ</ta>
            <ta e="T434" id="Seg_4387" s="T433">be-PRS-1SG</ta>
            <ta e="T435" id="Seg_4388" s="T434">1SG.[NOM]</ta>
            <ta e="T436" id="Seg_4389" s="T435">2SG.[NOM]</ta>
            <ta e="T437" id="Seg_4390" s="T436">father-2SG-ACC</ta>
            <ta e="T438" id="Seg_4391" s="T437">kill-PST2.NEG-EP-1SG</ta>
            <ta e="T439" id="Seg_4392" s="T438">say-PRS.[3SG]</ta>
            <ta e="T440" id="Seg_4393" s="T439">better</ta>
            <ta e="T441" id="Seg_4394" s="T440">1SG-ACC</ta>
            <ta e="T442" id="Seg_4395" s="T441">soul-1SG-ACC</ta>
            <ta e="T443" id="Seg_4396" s="T442">cut.[IMP.2SG]</ta>
            <ta e="T444" id="Seg_4397" s="T443">now</ta>
            <ta e="T445" id="Seg_4398" s="T444">and</ta>
            <ta e="T446" id="Seg_4399" s="T445">what.[NOM]</ta>
            <ta e="T447" id="Seg_4400" s="T446">human.being-3SG.[NOM]</ta>
            <ta e="T448" id="Seg_4401" s="T447">become-FUT-1SG=Q</ta>
            <ta e="T449" id="Seg_4402" s="T448">say-PRS.[3SG]</ta>
            <ta e="T450" id="Seg_4403" s="T449">this</ta>
            <ta e="T451" id="Seg_4404" s="T450">child-2SG.[NOM]</ta>
            <ta e="T452" id="Seg_4405" s="T451">this</ta>
            <ta e="T453" id="Seg_4406" s="T452">human.being-ACC</ta>
            <ta e="T454" id="Seg_4407" s="T453">soul-3SG-ACC</ta>
            <ta e="T455" id="Seg_4408" s="T454">cut-CVB.SEQ</ta>
            <ta e="T456" id="Seg_4409" s="T455">throw-PRS.[3SG]</ta>
            <ta e="T457" id="Seg_4410" s="T456">this</ta>
            <ta e="T458" id="Seg_4411" s="T457">child-2SG.[NOM]</ta>
            <ta e="T459" id="Seg_4412" s="T458">this-ACC</ta>
            <ta e="T460" id="Seg_4413" s="T459">hear-PRS.[3SG]</ta>
            <ta e="T461" id="Seg_4414" s="T460">and</ta>
            <ta e="T462" id="Seg_4415" s="T461">Lyypyrdaan-ACC</ta>
            <ta e="T463" id="Seg_4416" s="T462">kill-CVB.SEQ</ta>
            <ta e="T464" id="Seg_4417" s="T463">throw-PRS.[3SG]</ta>
            <ta e="T465" id="Seg_4418" s="T464">wealth-3SG-ACC-satiety-3SG-ACC</ta>
            <ta e="T466" id="Seg_4419" s="T465">house-3SG-DAT/LOC</ta>
            <ta e="T467" id="Seg_4420" s="T466">carry-CVB.SIM</ta>
            <ta e="T468" id="Seg_4421" s="T467">go-PRS.[3SG]</ta>
            <ta e="T469" id="Seg_4422" s="T468">this</ta>
            <ta e="T470" id="Seg_4423" s="T469">wealth-2SG-ACC</ta>
            <ta e="T471" id="Seg_4424" s="T470">house-3SG-DAT/LOC</ta>
            <ta e="T472" id="Seg_4425" s="T471">bring-CVB.SEQ</ta>
            <ta e="T473" id="Seg_4426" s="T472">mother-3SG-ACC</ta>
            <ta e="T474" id="Seg_4427" s="T473">with</ta>
            <ta e="T475" id="Seg_4428" s="T474">be.rich-CVB.SEQ-be.rich-CVB.SEQ</ta>
            <ta e="T476" id="Seg_4429" s="T475">well</ta>
            <ta e="T477" id="Seg_4430" s="T476">live-PST2-3SG</ta>
            <ta e="T478" id="Seg_4431" s="T477">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_4432" s="T0">Lyypyrdaan.[NOM]</ta>
            <ta e="T2" id="Seg_4433" s="T1">doch</ta>
            <ta e="T3" id="Seg_4434" s="T2">leben-PRS.[3SG]</ta>
            <ta e="T4" id="Seg_4435" s="T3">Lyypyrdaan.[NOM]</ta>
            <ta e="T5" id="Seg_4436" s="T4">was.für.ein</ta>
            <ta e="T6" id="Seg_4437" s="T5">INDEF</ta>
            <ta e="T7" id="Seg_4438" s="T6">in.Richtung</ta>
            <ta e="T8" id="Seg_4439" s="T7">gehen-PRS.[3SG]</ta>
            <ta e="T9" id="Seg_4440" s="T8">dieses</ta>
            <ta e="T10" id="Seg_4441" s="T9">gehen-CVB.SEQ</ta>
            <ta e="T11" id="Seg_4442" s="T10">Frau-PROPR</ta>
            <ta e="T12" id="Seg_4443" s="T11">Mensch-ACC</ta>
            <ta e="T13" id="Seg_4444" s="T12">finden-PRS.[3SG]</ta>
            <ta e="T14" id="Seg_4445" s="T13">Junge-PROPR.[NOM]</ta>
            <ta e="T15" id="Seg_4446" s="T14">jenes</ta>
            <ta e="T16" id="Seg_4447" s="T15">Mensch.[NOM]</ta>
            <ta e="T17" id="Seg_4448" s="T16">Frau-PROPR</ta>
            <ta e="T18" id="Seg_4449" s="T17">Mensch-2SG.[NOM]</ta>
            <ta e="T19" id="Seg_4450" s="T18">einzeln</ta>
            <ta e="T20" id="Seg_4451" s="T19">nomadisieren-PST2.[3SG]</ta>
            <ta e="T21" id="Seg_4452" s="T20">einsam</ta>
            <ta e="T22" id="Seg_4453" s="T21">Dämmerung.[NOM]</ta>
            <ta e="T23" id="Seg_4454" s="T22">sein-CVB.SEQ</ta>
            <ta e="T24" id="Seg_4455" s="T23">am.Abend</ta>
            <ta e="T25" id="Seg_4456" s="T24">eins</ta>
            <ta e="T26" id="Seg_4457" s="T25">Ort-DAT/LOC</ta>
            <ta e="T27" id="Seg_4458" s="T26">fallen-PST2.[3SG]</ta>
            <ta e="T28" id="Seg_4459" s="T27">schlafen-CVB.PURP</ta>
            <ta e="T29" id="Seg_4460" s="T28">Schuhe-3SG-ACC</ta>
            <ta e="T30" id="Seg_4461" s="T29">nackt-VBZ-CVB.SEQ</ta>
            <ta e="T31" id="Seg_4462" s="T30">sein-TEMP-3SG</ta>
            <ta e="T32" id="Seg_4463" s="T31">Feuer-3SG.[NOM]</ta>
            <ta e="T33" id="Seg_4464" s="T32">wie.viel-MLTP</ta>
            <ta e="T34" id="Seg_4465" s="T33">INDEF</ta>
            <ta e="T35" id="Seg_4466" s="T34">ein.Geräusch.machen-PST2.[3SG]</ta>
            <ta e="T36" id="Seg_4467" s="T35">dieses</ta>
            <ta e="T37" id="Seg_4468" s="T36">Mensch-2SG.[NOM]</ta>
            <ta e="T38" id="Seg_4469" s="T37">1SG.[NOM]</ta>
            <ta e="T39" id="Seg_4470" s="T38">am.Tag</ta>
            <ta e="T40" id="Seg_4471" s="T39">gehen-EP-PST2-EP-1SG</ta>
            <ta e="T41" id="Seg_4472" s="T40">EMPH</ta>
            <ta e="T42" id="Seg_4473" s="T41">was.[NOM]</ta>
            <ta e="T43" id="Seg_4474" s="T42">kommen-FUT.[3SG]=Q</ta>
            <ta e="T44" id="Seg_4475" s="T43">Feuer-EP-1SG.[NOM]</ta>
            <ta e="T45" id="Seg_4476" s="T44">warum</ta>
            <ta e="T46" id="Seg_4477" s="T45">sprechen-PRS.[3SG]</ta>
            <ta e="T47" id="Seg_4478" s="T46">denken-CVB.SEQ</ta>
            <ta e="T48" id="Seg_4479" s="T47">Angst.haben-NEG.[3SG]</ta>
            <ta e="T49" id="Seg_4480" s="T48">sich.hinlegen-PRS.[3SG]</ta>
            <ta e="T50" id="Seg_4481" s="T49">Lyypyrdaan.[NOM]</ta>
            <ta e="T51" id="Seg_4482" s="T50">kommen-CVB.SEQ</ta>
            <ta e="T52" id="Seg_4483" s="T51">dieses</ta>
            <ta e="T53" id="Seg_4484" s="T52">Silber.[NOM]</ta>
            <ta e="T54" id="Seg_4485" s="T53">Klafter-PROPR</ta>
            <ta e="T55" id="Seg_4486" s="T54">Bogen-PROPR</ta>
            <ta e="T56" id="Seg_4487" s="T55">Mensch-ACC</ta>
            <ta e="T57" id="Seg_4488" s="T56">töten-CVB.SEQ</ta>
            <ta e="T58" id="Seg_4489" s="T57">werfen-PRS.[3SG]</ta>
            <ta e="T59" id="Seg_4490" s="T58">Bogen.[NOM]</ta>
            <ta e="T60" id="Seg_4491" s="T59">Bogen.[NOM]</ta>
            <ta e="T61" id="Seg_4492" s="T60">MOD</ta>
            <ta e="T62" id="Seg_4493" s="T61">EMPH</ta>
            <ta e="T63" id="Seg_4494" s="T62">Mensch-2SG.[NOM]</ta>
            <ta e="T64" id="Seg_4495" s="T63">Frau-3SG.[NOM]</ta>
            <ta e="T65" id="Seg_4496" s="T64">klein</ta>
            <ta e="T66" id="Seg_4497" s="T65">Kind-3SG-ACC</ta>
            <ta e="T67" id="Seg_4498" s="T66">mit</ta>
            <ta e="T68" id="Seg_4499" s="T67">Ort-DAT/LOC</ta>
            <ta e="T69" id="Seg_4500" s="T68">entfliehen-CVB.SEQ</ta>
            <ta e="T70" id="Seg_4501" s="T69">bleiben-PRS.[3SG]</ta>
            <ta e="T71" id="Seg_4502" s="T70">Frau-EP-2SG.[NOM]</ta>
            <ta e="T72" id="Seg_4503" s="T71">was-POSS</ta>
            <ta e="T73" id="Seg_4504" s="T72">NEG</ta>
            <ta e="T74" id="Seg_4505" s="T73">NEG.[3SG]</ta>
            <ta e="T75" id="Seg_4506" s="T74">Boden.[NOM]</ta>
            <ta e="T76" id="Seg_4507" s="T75">Erdhütte-DIM-DAT/LOC</ta>
            <ta e="T77" id="Seg_4508" s="T76">leben-PRS.[3SG]</ta>
            <ta e="T78" id="Seg_4509" s="T77">Märchen.[NOM]</ta>
            <ta e="T79" id="Seg_4510" s="T78">Kind-3SG.[NOM]</ta>
            <ta e="T80" id="Seg_4511" s="T79">lange</ta>
            <ta e="T81" id="Seg_4512" s="T80">sein-NEG.[3SG]</ta>
            <ta e="T82" id="Seg_4513" s="T81">wachsen-CVB.SEQ</ta>
            <ta e="T83" id="Seg_4514" s="T82">bleiben-PRS.[3SG]</ta>
            <ta e="T84" id="Seg_4515" s="T83">Kind.[NOM]</ta>
            <ta e="T85" id="Seg_4516" s="T84">hinausgehen-CVB.SEQ</ta>
            <ta e="T86" id="Seg_4517" s="T85">sehen-PST2-3SG</ta>
            <ta e="T87" id="Seg_4518" s="T86">Haus-3SG-GEN</ta>
            <ta e="T88" id="Seg_4519" s="T87">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T89" id="Seg_4520" s="T88">weiß-PL.[NOM]</ta>
            <ta e="T90" id="Seg_4521" s="T89">gehen-PRS-3PL</ta>
            <ta e="T91" id="Seg_4522" s="T90">Mutter-3SG-DAT/LOC</ta>
            <ta e="T92" id="Seg_4523" s="T91">hineingehen-CVB.SEQ</ta>
            <ta e="T93" id="Seg_4524" s="T92">erzählen-PRS.[3SG]</ta>
            <ta e="T94" id="Seg_4525" s="T93">hey</ta>
            <ta e="T95" id="Seg_4526" s="T94">dann</ta>
            <ta e="T96" id="Seg_4527" s="T95">Rebhuhn-PL.[NOM]</ta>
            <ta e="T97" id="Seg_4528" s="T96">MOD</ta>
            <ta e="T98" id="Seg_4529" s="T97">EMPH</ta>
            <ta e="T99" id="Seg_4530" s="T98">sagen-PRS.[3SG]</ta>
            <ta e="T100" id="Seg_4531" s="T99">Mutter-3SG.[NOM]</ta>
            <ta e="T101" id="Seg_4532" s="T100">oh</ta>
            <ta e="T102" id="Seg_4533" s="T101">Vater-PROPR-EP-1SG.[NOM]</ta>
            <ta e="T103" id="Seg_4534" s="T102">sein-PST2-3SG</ta>
            <ta e="T104" id="Seg_4535" s="T103">sein-COND.[3SG]</ta>
            <ta e="T105" id="Seg_4536" s="T104">wie</ta>
            <ta e="T106" id="Seg_4537" s="T105">INDEF</ta>
            <ta e="T107" id="Seg_4538" s="T106">machen-FUT.[3SG]</ta>
            <ta e="T108" id="Seg_4539" s="T107">sein-PST1-3SG</ta>
            <ta e="T109" id="Seg_4540" s="T108">MOD</ta>
            <ta e="T110" id="Seg_4541" s="T109">sagen-PRS.[3SG]</ta>
            <ta e="T111" id="Seg_4542" s="T110">Junge.[NOM]</ta>
            <ta e="T112" id="Seg_4543" s="T111">Mutter-3SG-DAT/LOC</ta>
            <ta e="T113" id="Seg_4544" s="T112">Mutter-3SG.[NOM]</ta>
            <ta e="T114" id="Seg_4545" s="T113">Sohn.[NOM]</ta>
            <ta e="T115" id="Seg_4546" s="T114">Vater-PROPR-EP-GEN</ta>
            <ta e="T116" id="Seg_4547" s="T115">Seite-3SG-INSTR</ta>
            <ta e="T117" id="Seg_4548" s="T116">völlig</ta>
            <ta e="T118" id="Seg_4549" s="T117">erzählen-NEG.[3SG]</ta>
            <ta e="T119" id="Seg_4550" s="T118">Mutter.[NOM]</ta>
            <ta e="T120" id="Seg_4551" s="T119">Haar-3SG-ACC</ta>
            <ta e="T121" id="Seg_4552" s="T120">schneiden-CVB.SEQ</ta>
            <ta e="T122" id="Seg_4553" s="T121">Faden.[NOM]</ta>
            <ta e="T123" id="Seg_4554" s="T122">machen-PRS.[3SG]</ta>
            <ta e="T124" id="Seg_4555" s="T123">Junge.[NOM]</ta>
            <ta e="T125" id="Seg_4556" s="T124">dieses-INSTR</ta>
            <ta e="T126" id="Seg_4557" s="T125">Fangschlinge.[NOM]</ta>
            <ta e="T127" id="Seg_4558" s="T126">machen-PRS.[3SG]</ta>
            <ta e="T128" id="Seg_4559" s="T127">dieses-INSTR</ta>
            <ta e="T129" id="Seg_4560" s="T128">Rebhuhn-ACC</ta>
            <ta e="T130" id="Seg_4561" s="T129">töten-CVB.SEQ</ta>
            <ta e="T131" id="Seg_4562" s="T130">jenes-INSTR</ta>
            <ta e="T132" id="Seg_4563" s="T131">füttern-PASS/REFL-CVB.SEQ</ta>
            <ta e="T133" id="Seg_4564" s="T132">Mensch.[NOM]</ta>
            <ta e="T134" id="Seg_4565" s="T133">sein-CVB.SEQ</ta>
            <ta e="T135" id="Seg_4566" s="T134">leben-PRS-3PL</ta>
            <ta e="T136" id="Seg_4567" s="T135">dieses</ta>
            <ta e="T137" id="Seg_4568" s="T136">leben-CVB.SEQ</ta>
            <ta e="T138" id="Seg_4569" s="T137">Junge.[NOM]</ta>
            <ta e="T139" id="Seg_4570" s="T138">immer</ta>
            <ta e="T140" id="Seg_4571" s="T139">fragen-PRS.[3SG]</ta>
            <ta e="T141" id="Seg_4572" s="T140">1SG.[NOM]</ta>
            <ta e="T142" id="Seg_4573" s="T141">Vater-PROPR.[NOM]</ta>
            <ta e="T143" id="Seg_4574" s="T142">sein-PST1-1SG</ta>
            <ta e="T144" id="Seg_4575" s="T143">Mutter-3SG.[NOM]</ta>
            <ta e="T145" id="Seg_4576" s="T144">völlig</ta>
            <ta e="T146" id="Seg_4577" s="T145">sagen-NEG.PTCP</ta>
            <ta e="T147" id="Seg_4578" s="T146">sein-PST2.[3SG]</ta>
            <ta e="T148" id="Seg_4579" s="T147">verstecken-PTCP.PRS</ta>
            <ta e="T149" id="Seg_4580" s="T148">sein-PST2.[3SG]</ta>
            <ta e="T150" id="Seg_4581" s="T149">Lyypyrdaan.[NOM]</ta>
            <ta e="T151" id="Seg_4582" s="T150">wieder</ta>
            <ta e="T152" id="Seg_4583" s="T151">Mensch-ACC</ta>
            <ta e="T153" id="Seg_4584" s="T152">töten-CVB.PURP</ta>
            <ta e="T154" id="Seg_4585" s="T153">gehen-PRS.[3SG]</ta>
            <ta e="T155" id="Seg_4586" s="T154">eins</ta>
            <ta e="T156" id="Seg_4587" s="T155">Sohn-PROPR</ta>
            <ta e="T157" id="Seg_4588" s="T156">Alte-DAT/LOC</ta>
            <ta e="T158" id="Seg_4589" s="T157">kommen-PRS.[3SG]</ta>
            <ta e="T159" id="Seg_4590" s="T158">plötzlich</ta>
            <ta e="T160" id="Seg_4591" s="T159">sehen-PST2-3SG</ta>
            <ta e="T161" id="Seg_4592" s="T160">sehr</ta>
            <ta e="T162" id="Seg_4593" s="T161">dick</ta>
            <ta e="T163" id="Seg_4594" s="T162">Lärche.[NOM]</ta>
            <ta e="T164" id="Seg_4595" s="T163">Baum.[NOM]</ta>
            <ta e="T165" id="Seg_4596" s="T164">stehen-PRS.[3SG]</ta>
            <ta e="T166" id="Seg_4597" s="T165">dieses</ta>
            <ta e="T167" id="Seg_4598" s="T166">Baum-DAT/LOC</ta>
            <ta e="T168" id="Seg_4599" s="T167">Alte.[NOM]</ta>
            <ta e="T169" id="Seg_4600" s="T168">Sohn-3SG.[NOM]</ta>
            <ta e="T170" id="Seg_4601" s="T169">Bogen-3SG-ACC</ta>
            <ta e="T171" id="Seg_4602" s="T170">umhängen-CVB.SEQ</ta>
            <ta e="T172" id="Seg_4603" s="T171">werfen-PST2.[3SG]</ta>
            <ta e="T173" id="Seg_4604" s="T172">Lyypyrdaan.[NOM]</ta>
            <ta e="T174" id="Seg_4605" s="T173">fragen-PRS.[3SG]</ta>
            <ta e="T175" id="Seg_4606" s="T174">dieses.[NOM]</ta>
            <ta e="T176" id="Seg_4607" s="T175">wer.[NOM]</ta>
            <ta e="T177" id="Seg_4608" s="T176">Bogen-3SG.[NOM]=Q</ta>
            <ta e="T178" id="Seg_4609" s="T177">was</ta>
            <ta e="T179" id="Seg_4610" s="T178">Familie-PROPR-2SG</ta>
            <ta e="T180" id="Seg_4611" s="T179">Alte.[NOM]</ta>
            <ta e="T181" id="Seg_4612" s="T180">Junge-PROPR-1SG</ta>
            <ta e="T182" id="Seg_4613" s="T181">sagen-PRS.[3SG]</ta>
            <ta e="T183" id="Seg_4614" s="T182">INTJ</ta>
            <ta e="T184" id="Seg_4615" s="T183">dieses</ta>
            <ta e="T185" id="Seg_4616" s="T184">Bogen-3SG-ACC</ta>
            <ta e="T186" id="Seg_4617" s="T185">warum</ta>
            <ta e="T187" id="Seg_4618" s="T186">lassen-PST2-3SG=Q</ta>
            <ta e="T188" id="Seg_4619" s="T187">EMPH</ta>
            <ta e="T189" id="Seg_4620" s="T188">dieses.[NOM]</ta>
            <ta e="T190" id="Seg_4621" s="T189">Bogen.[NOM]</ta>
            <ta e="T191" id="Seg_4622" s="T190">Kraft-3SG.[NOM]</ta>
            <ta e="T192" id="Seg_4623" s="T191">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T193" id="Seg_4624" s="T192">jenes.[NOM]</ta>
            <ta e="T194" id="Seg_4625" s="T193">wegen</ta>
            <ta e="T195" id="Seg_4626" s="T194">lassen-PST2-3SG</ta>
            <ta e="T196" id="Seg_4627" s="T195">sagen-PRS.[3SG]</ta>
            <ta e="T197" id="Seg_4628" s="T196">Alte.[NOM]</ta>
            <ta e="T198" id="Seg_4629" s="T197">dann</ta>
            <ta e="T199" id="Seg_4630" s="T198">großer.Stein.[NOM]</ta>
            <ta e="T200" id="Seg_4631" s="T199">liegen-PRS.[3SG]</ta>
            <ta e="T201" id="Seg_4632" s="T200">groß</ta>
            <ta e="T202" id="Seg_4633" s="T201">sehr</ta>
            <ta e="T203" id="Seg_4634" s="T202">Stein.[NOM]</ta>
            <ta e="T205" id="Seg_4635" s="T204">dieses-ACC</ta>
            <ta e="T206" id="Seg_4636" s="T205">auch</ta>
            <ta e="T207" id="Seg_4637" s="T206">fragen-PRS.[3SG]</ta>
            <ta e="T208" id="Seg_4638" s="T207">eben.der</ta>
            <ta e="T209" id="Seg_4639" s="T208">Mensch.[NOM]</ta>
            <ta e="T210" id="Seg_4640" s="T209">Alte.[NOM]</ta>
            <ta e="T211" id="Seg_4641" s="T210">jenes-ACC</ta>
            <ta e="T212" id="Seg_4642" s="T211">dieses.[NOM]</ta>
            <ta e="T213" id="Seg_4643" s="T212">sich.langweilen-TEMP-3SG</ta>
            <ta e="T214" id="Seg_4644" s="T213">Ball.[NOM]</ta>
            <ta e="T215" id="Seg_4645" s="T214">machen-HAB.[3SG]</ta>
            <ta e="T216" id="Seg_4646" s="T215">sagen-PRS.[3SG]</ta>
            <ta e="T217" id="Seg_4647" s="T216">jenes</ta>
            <ta e="T218" id="Seg_4648" s="T217">Berg-DAT/LOC</ta>
            <ta e="T219" id="Seg_4649" s="T218">bis.zu</ta>
            <ta e="T220" id="Seg_4650" s="T219">treten-TEMP-3SG</ta>
            <ta e="T221" id="Seg_4651" s="T220">zurück</ta>
            <ta e="T222" id="Seg_4652" s="T221">rollen-CVB.SEQ</ta>
            <ta e="T223" id="Seg_4653" s="T222">kommen-HAB.[3SG]</ta>
            <ta e="T224" id="Seg_4654" s="T223">jenes.[NOM]</ta>
            <ta e="T225" id="Seg_4655" s="T224">wie</ta>
            <ta e="T226" id="Seg_4656" s="T225">spielen-HAB.[3SG]</ta>
            <ta e="T227" id="Seg_4657" s="T226">sagen-PRS.[3SG]</ta>
            <ta e="T228" id="Seg_4658" s="T227">Lyypyrdaan.[NOM]</ta>
            <ta e="T229" id="Seg_4659" s="T228">sprechen-PRS.[3SG]</ta>
            <ta e="T230" id="Seg_4660" s="T229">morgen</ta>
            <ta e="T231" id="Seg_4661" s="T230">1SG.[NOM]</ta>
            <ta e="T232" id="Seg_4662" s="T231">geboren.werden-PTCP.PST</ta>
            <ta e="T233" id="Seg_4663" s="T232">Tag-VBZ-PRS-1SG</ta>
            <ta e="T234" id="Seg_4664" s="T233">dort</ta>
            <ta e="T235" id="Seg_4665" s="T234">Junge-EP-2SG.[NOM]</ta>
            <ta e="T236" id="Seg_4666" s="T235">kommen-FUT-IMP.3SG</ta>
            <ta e="T237" id="Seg_4667" s="T236">dieses</ta>
            <ta e="T238" id="Seg_4668" s="T237">Sohn.[NOM]</ta>
            <ta e="T239" id="Seg_4669" s="T238">Haus-3SG-DAT/LOC</ta>
            <ta e="T240" id="Seg_4670" s="T239">kommen-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T241" id="Seg_4671" s="T240">Mutter-3SG.[NOM]</ta>
            <ta e="T242" id="Seg_4672" s="T241">ganz-3SG-ACC</ta>
            <ta e="T243" id="Seg_4673" s="T242">erzählen-CVB.SEQ</ta>
            <ta e="T244" id="Seg_4674" s="T243">geben-PRS.[3SG]</ta>
            <ta e="T245" id="Seg_4675" s="T244">einladen-PRS-3PL</ta>
            <ta e="T246" id="Seg_4676" s="T245">sagen-PRS.[3SG]</ta>
            <ta e="T247" id="Seg_4677" s="T246">doch</ta>
            <ta e="T248" id="Seg_4678" s="T247">nur</ta>
            <ta e="T249" id="Seg_4679" s="T248">dieses</ta>
            <ta e="T250" id="Seg_4680" s="T249">Mensch-2SG.[NOM]</ta>
            <ta e="T251" id="Seg_4681" s="T250">Lyypyrdaan-DAT/LOC</ta>
            <ta e="T252" id="Seg_4682" s="T251">gehen-PST1-3SG</ta>
            <ta e="T253" id="Seg_4683" s="T252">Lyypyrdaan.[NOM]</ta>
            <ta e="T254" id="Seg_4684" s="T253">Mensch-PL-3SG-DAT/LOC</ta>
            <ta e="T255" id="Seg_4685" s="T254">dieses</ta>
            <ta e="T256" id="Seg_4686" s="T255">Mensch-ACC</ta>
            <ta e="T257" id="Seg_4687" s="T256">Umzäunung-VBZ-FUT-1PL</ta>
            <ta e="T258" id="Seg_4688" s="T257">töten-FUT-1PL</ta>
            <ta e="T259" id="Seg_4689" s="T258">sagen-CVB.SEQ</ta>
            <ta e="T260" id="Seg_4690" s="T259">sprechen-PRS.[3SG]</ta>
            <ta e="T261" id="Seg_4691" s="T260">kommen-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T262" id="Seg_4692" s="T261">Gast-3PL-ACC</ta>
            <ta e="T263" id="Seg_4693" s="T262">Umzäunung-VBZ-CVB.SEQ</ta>
            <ta e="T264" id="Seg_4694" s="T263">werfen-PRS-3PL</ta>
            <ta e="T265" id="Seg_4695" s="T264">töten-CVB.PURP-3PL</ta>
            <ta e="T266" id="Seg_4696" s="T265">raten-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T267" id="Seg_4697" s="T266">MOD</ta>
            <ta e="T268" id="Seg_4698" s="T267">Lyypyrdaan.[NOM]</ta>
            <ta e="T269" id="Seg_4699" s="T268">Angst.haben-CVB.SEQ</ta>
            <ta e="T270" id="Seg_4700" s="T269">Freund-PL-3SG-DAT/LOC</ta>
            <ta e="T271" id="Seg_4701" s="T270">sprechen-PRS.[3SG]</ta>
            <ta e="T272" id="Seg_4702" s="T271">dieses</ta>
            <ta e="T273" id="Seg_4703" s="T272">was.[NOM]</ta>
            <ta e="T274" id="Seg_4704" s="T273">sein-CVB.SEQ-2PL</ta>
            <ta e="T275" id="Seg_4705" s="T274">sammeln-EP-PASS/REFL-PRS-2PL</ta>
            <ta e="T276" id="Seg_4706" s="T275">Gast.[NOM]</ta>
            <ta e="T277" id="Seg_4707" s="T276">hier</ta>
            <ta e="T278" id="Seg_4708" s="T277">sprechen-PRS.[3SG]</ta>
            <ta e="T279" id="Seg_4709" s="T278">alter.Mann.[NOM]</ta>
            <ta e="T280" id="Seg_4710" s="T279">2SG.[NOM]</ta>
            <ta e="T281" id="Seg_4711" s="T280">1SG-ACC</ta>
            <ta e="T282" id="Seg_4712" s="T281">töten-CVB.PURP</ta>
            <ta e="T283" id="Seg_4713" s="T282">wollen-PTCP.PRS-2SG-ACC</ta>
            <ta e="T284" id="Seg_4714" s="T283">wissen-PRS-1SG</ta>
            <ta e="T285" id="Seg_4715" s="T284">1SG.[NOM]</ta>
            <ta e="T286" id="Seg_4716" s="T285">dieses</ta>
            <ta e="T287" id="Seg_4717" s="T286">einsam</ta>
            <ta e="T288" id="Seg_4718" s="T287">kleines.Messer-PROPR-1SG</ta>
            <ta e="T289" id="Seg_4719" s="T288">jenes-INSTR</ta>
            <ta e="T290" id="Seg_4720" s="T289">Volk-2SG-ACC</ta>
            <ta e="T291" id="Seg_4721" s="T290">einmal</ta>
            <ta e="T292" id="Seg_4722" s="T291">auslöschen-FUT-1SG</ta>
            <ta e="T293" id="Seg_4723" s="T292">sein-PST1-3SG</ta>
            <ta e="T294" id="Seg_4724" s="T293">sagen-PRS.[3SG]</ta>
            <ta e="T295" id="Seg_4725" s="T294">na=EMPH</ta>
            <ta e="T296" id="Seg_4726" s="T295">dieses.[NOM]</ta>
            <ta e="T297" id="Seg_4727" s="T296">wie</ta>
            <ta e="T298" id="Seg_4728" s="T297">machen-EP-NEG.[IMP.2SG]</ta>
            <ta e="T299" id="Seg_4729" s="T298">1SG.[NOM]</ta>
            <ta e="T300" id="Seg_4730" s="T299">Tochter-1SG-ACC</ta>
            <ta e="T301" id="Seg_4731" s="T300">Frau.[NOM]</ta>
            <ta e="T302" id="Seg_4732" s="T301">machen.[IMP.2SG]</ta>
            <ta e="T303" id="Seg_4733" s="T302">sagen-PRS.[3SG]</ta>
            <ta e="T304" id="Seg_4734" s="T303">Gast-3SG.[NOM]</ta>
            <ta e="T305" id="Seg_4735" s="T304">Lyypyrdaan.[NOM]</ta>
            <ta e="T306" id="Seg_4736" s="T305">Tochter-3SG-ACC</ta>
            <ta e="T307" id="Seg_4737" s="T306">Frau.[NOM]</ta>
            <ta e="T308" id="Seg_4738" s="T307">machen-CVB.SIM</ta>
            <ta e="T309" id="Seg_4739" s="T308">liegen-PRS.[3SG]</ta>
            <ta e="T310" id="Seg_4740" s="T309">dieses</ta>
            <ta e="T311" id="Seg_4741" s="T310">Junge.[NOM]</ta>
            <ta e="T312" id="Seg_4742" s="T311">wachsen-CVB.SEQ</ta>
            <ta e="T313" id="Seg_4743" s="T312">nachdem</ta>
            <ta e="T314" id="Seg_4744" s="T313">Vater-3SG-ACC</ta>
            <ta e="T315" id="Seg_4745" s="T314">töten-PTCP.PST</ta>
            <ta e="T316" id="Seg_4746" s="T315">Mensch-ACC</ta>
            <ta e="T317" id="Seg_4747" s="T316">immer</ta>
            <ta e="T318" id="Seg_4748" s="T317">suchen-PTCP.PRS</ta>
            <ta e="T319" id="Seg_4749" s="T318">sein-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_4750" s="T319">Vater-3SG.[NOM]</ta>
            <ta e="T321" id="Seg_4751" s="T320">sterben-PTCP.PST</ta>
            <ta e="T322" id="Seg_4752" s="T321">Ort-3SG-DAT/LOC</ta>
            <ta e="T323" id="Seg_4753" s="T322">wachsen-CVB.SEQ</ta>
            <ta e="T324" id="Seg_4754" s="T323">nachdem</ta>
            <ta e="T325" id="Seg_4755" s="T324">immer</ta>
            <ta e="T326" id="Seg_4756" s="T325">gehen-PTCP.PRS</ta>
            <ta e="T327" id="Seg_4757" s="T326">sein-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_4758" s="T327">nur</ta>
            <ta e="T329" id="Seg_4759" s="T328">dieses</ta>
            <ta e="T330" id="Seg_4760" s="T329">liegen-TEMP-3SG</ta>
            <ta e="T331" id="Seg_4761" s="T330">zwei</ta>
            <ta e="T332" id="Seg_4762" s="T331">Mensch.[NOM]</ta>
            <ta e="T333" id="Seg_4763" s="T332">gehen-PRS-3PL</ta>
            <ta e="T334" id="Seg_4764" s="T333">nur</ta>
            <ta e="T335" id="Seg_4765" s="T334">eins-3PL.[NOM]</ta>
            <ta e="T336" id="Seg_4766" s="T335">gefroren</ta>
            <ta e="T337" id="Seg_4767" s="T336">Bär-ACC</ta>
            <ta e="T338" id="Seg_4768" s="T337">Brust-3SG-DAT/LOC</ta>
            <ta e="T339" id="Seg_4769" s="T338">binden-CVB.SEQ</ta>
            <ta e="T340" id="Seg_4770" s="T339">gehen-PRS.[3SG]</ta>
            <ta e="T341" id="Seg_4771" s="T340">dieses-DAT/LOC</ta>
            <ta e="T342" id="Seg_4772" s="T341">Freund-3SG.[NOM]</ta>
            <ta e="T343" id="Seg_4773" s="T342">sagen-PRS.[3SG]</ta>
            <ta e="T344" id="Seg_4774" s="T343">hier</ta>
            <ta e="T345" id="Seg_4775" s="T344">na</ta>
            <ta e="T346" id="Seg_4776" s="T345">Lyypyrdaan.[NOM]</ta>
            <ta e="T347" id="Seg_4777" s="T346">Silber</ta>
            <ta e="T348" id="Seg_4778" s="T347">Klafter-PROPR</ta>
            <ta e="T349" id="Seg_4779" s="T348">Bogen-PROPR</ta>
            <ta e="T350" id="Seg_4780" s="T349">Mensch-ACC</ta>
            <ta e="T351" id="Seg_4781" s="T350">töten-PST2-3SG</ta>
            <ta e="T352" id="Seg_4782" s="T351">es.gibt</ta>
            <ta e="T353" id="Seg_4783" s="T352">jenes</ta>
            <ta e="T354" id="Seg_4784" s="T353">Mensch.[NOM]</ta>
            <ta e="T355" id="Seg_4785" s="T354">Sohn-3SG.[NOM]</ta>
            <ta e="T356" id="Seg_4786" s="T355">sagen-PRS.[3SG]</ta>
            <ta e="T357" id="Seg_4787" s="T356">Junge.[NOM]</ta>
            <ta e="T358" id="Seg_4788" s="T357">dort</ta>
            <ta e="T359" id="Seg_4789" s="T358">doch</ta>
            <ta e="T360" id="Seg_4790" s="T359">Vater-PROPR-3SG-ACC</ta>
            <ta e="T361" id="Seg_4791" s="T360">hören-PRS.[3SG]</ta>
            <ta e="T362" id="Seg_4792" s="T361">dieses</ta>
            <ta e="T363" id="Seg_4793" s="T362">Mensch-PL-2SG-ACC</ta>
            <ta e="T364" id="Seg_4794" s="T363">doch</ta>
            <ta e="T365" id="Seg_4795" s="T364">schreien-PRS.[3SG]</ta>
            <ta e="T366" id="Seg_4796" s="T365">EMPH</ta>
            <ta e="T367" id="Seg_4797" s="T366">dieses</ta>
            <ta e="T368" id="Seg_4798" s="T367">Kind-2SG.[NOM]</ta>
            <ta e="T369" id="Seg_4799" s="T368">Ort-1PL.[NOM]</ta>
            <ta e="T370" id="Seg_4800" s="T369">erschrecken-PRS.[3SG]</ta>
            <ta e="T371" id="Seg_4801" s="T370">denken-CVB.SEQ</ta>
            <ta e="T372" id="Seg_4802" s="T371">hier</ta>
            <ta e="T373" id="Seg_4803" s="T372">Bär-DIM-3PL-ACC</ta>
            <ta e="T374" id="Seg_4804" s="T373">bleiben-CAUS-CVB.SIM</ta>
            <ta e="T375" id="Seg_4805" s="T374">weggehen-PRS-3PL</ta>
            <ta e="T376" id="Seg_4806" s="T375">Junge.[NOM]</ta>
            <ta e="T377" id="Seg_4807" s="T376">Bär-ACC</ta>
            <ta e="T378" id="Seg_4808" s="T377">mit</ta>
            <ta e="T379" id="Seg_4809" s="T378">Beluga-3SG-ACC</ta>
            <ta e="T380" id="Seg_4810" s="T379">Haus-3SG-DAT/LOC</ta>
            <ta e="T381" id="Seg_4811" s="T380">Mutter-3SG-DAT/LOC</ta>
            <ta e="T382" id="Seg_4812" s="T381">tragen-CVB.SIM</ta>
            <ta e="T383" id="Seg_4813" s="T382">gehen-PRS.[3SG]</ta>
            <ta e="T384" id="Seg_4814" s="T383">dieses-PL-ACC</ta>
            <ta e="T385" id="Seg_4815" s="T384">essen-PRS-3PL</ta>
            <ta e="T386" id="Seg_4816" s="T385">MOD</ta>
            <ta e="T387" id="Seg_4817" s="T386">Mutter-3SG.[NOM]</ta>
            <ta e="T388" id="Seg_4818" s="T387">rügen-PRS.[3SG]</ta>
            <ta e="T389" id="Seg_4819" s="T388">dieses</ta>
            <ta e="T390" id="Seg_4820" s="T389">was.[NOM]</ta>
            <ta e="T391" id="Seg_4821" s="T390">lange</ta>
            <ta e="T392" id="Seg_4822" s="T391">gehen-PRS-2SG</ta>
            <ta e="T393" id="Seg_4823" s="T392">töten-CVB.SEQ</ta>
            <ta e="T394" id="Seg_4824" s="T393">werfen-FUT-3PL</ta>
            <ta e="T395" id="Seg_4825" s="T394">Sohn-3SG.[NOM]</ta>
            <ta e="T396" id="Seg_4826" s="T395">Mutter-3SG-GEN</ta>
            <ta e="T397" id="Seg_4827" s="T396">Wort-3SG-ACC</ta>
            <ta e="T398" id="Seg_4828" s="T397">völlig</ta>
            <ta e="T399" id="Seg_4829" s="T398">hören-EP-NEG.[3SG]</ta>
            <ta e="T400" id="Seg_4830" s="T399">dieses</ta>
            <ta e="T401" id="Seg_4831" s="T400">Junge.[NOM]</ta>
            <ta e="T402" id="Seg_4832" s="T401">immer</ta>
            <ta e="T403" id="Seg_4833" s="T402">Vater-3SG-ACC</ta>
            <ta e="T404" id="Seg_4834" s="T403">suchen-PRS.[3SG]</ta>
            <ta e="T405" id="Seg_4835" s="T404">dieses.[NOM]</ta>
            <ta e="T406" id="Seg_4836" s="T405">nachdem</ta>
            <ta e="T407" id="Seg_4837" s="T406">Lyypyrdaan.[NOM]</ta>
            <ta e="T408" id="Seg_4838" s="T407">alter.Mann.[NOM]</ta>
            <ta e="T409" id="Seg_4839" s="T408">Haus-3SG-ACC</ta>
            <ta e="T410" id="Seg_4840" s="T409">doch</ta>
            <ta e="T411" id="Seg_4841" s="T410">finden-PRS.[3SG]</ta>
            <ta e="T412" id="Seg_4842" s="T411">dieses</ta>
            <ta e="T413" id="Seg_4843" s="T412">finden-CVB.SEQ</ta>
            <ta e="T414" id="Seg_4844" s="T413">abendlich</ta>
            <ta e="T415" id="Seg_4845" s="T414">Schlaf-INSTR</ta>
            <ta e="T416" id="Seg_4846" s="T415">schlafen-CVB.SIM</ta>
            <ta e="T417" id="Seg_4847" s="T416">liegen-TEMP-3PL</ta>
            <ta e="T418" id="Seg_4848" s="T417">kommen-PRS.[3SG]</ta>
            <ta e="T419" id="Seg_4849" s="T418">schlafen-CVB.SIM</ta>
            <ta e="T420" id="Seg_4850" s="T419">liegen-PTCP.PRS</ta>
            <ta e="T421" id="Seg_4851" s="T420">Mensch-ACC</ta>
            <ta e="T422" id="Seg_4852" s="T421">Pfeil-3SG-INSTR</ta>
            <ta e="T423" id="Seg_4853" s="T422">dick</ta>
            <ta e="T424" id="Seg_4854" s="T423">Sehne-3PL-ACC</ta>
            <ta e="T425" id="Seg_4855" s="T424">schneiden-CVB.SIM</ta>
            <ta e="T426" id="Seg_4856" s="T425">schießen-FREQ-PRS.[3SG]</ta>
            <ta e="T427" id="Seg_4857" s="T426">Lyypyrdaan.[NOM]</ta>
            <ta e="T428" id="Seg_4858" s="T427">Schwiegersohn-3SG-ACC</ta>
            <ta e="T429" id="Seg_4859" s="T428">doch</ta>
            <ta e="T430" id="Seg_4860" s="T429">dieses</ta>
            <ta e="T431" id="Seg_4861" s="T430">Schwiegervater-EP-1SG.[NOM]</ta>
            <ta e="T432" id="Seg_4862" s="T431">schlecht-3SG-DAT/LOC</ta>
            <ta e="T433" id="Seg_4863" s="T432">sterben-CVB.SEQ</ta>
            <ta e="T434" id="Seg_4864" s="T433">sein-PRS-1SG</ta>
            <ta e="T435" id="Seg_4865" s="T434">1SG.[NOM]</ta>
            <ta e="T436" id="Seg_4866" s="T435">2SG.[NOM]</ta>
            <ta e="T437" id="Seg_4867" s="T436">Vater-2SG-ACC</ta>
            <ta e="T438" id="Seg_4868" s="T437">töten-PST2.NEG-EP-1SG</ta>
            <ta e="T439" id="Seg_4869" s="T438">sagen-PRS.[3SG]</ta>
            <ta e="T440" id="Seg_4870" s="T439">besser</ta>
            <ta e="T441" id="Seg_4871" s="T440">1SG-ACC</ta>
            <ta e="T442" id="Seg_4872" s="T441">Seele-1SG-ACC</ta>
            <ta e="T443" id="Seg_4873" s="T442">schneiden.[IMP.2SG]</ta>
            <ta e="T444" id="Seg_4874" s="T443">jetzt</ta>
            <ta e="T445" id="Seg_4875" s="T444">und</ta>
            <ta e="T446" id="Seg_4876" s="T445">was.[NOM]</ta>
            <ta e="T447" id="Seg_4877" s="T446">Mensch-3SG.[NOM]</ta>
            <ta e="T448" id="Seg_4878" s="T447">werden-FUT-1SG=Q</ta>
            <ta e="T449" id="Seg_4879" s="T448">sagen-PRS.[3SG]</ta>
            <ta e="T450" id="Seg_4880" s="T449">dieses</ta>
            <ta e="T451" id="Seg_4881" s="T450">Kind-2SG.[NOM]</ta>
            <ta e="T452" id="Seg_4882" s="T451">dieses</ta>
            <ta e="T453" id="Seg_4883" s="T452">Mensch-ACC</ta>
            <ta e="T454" id="Seg_4884" s="T453">Seele-3SG-ACC</ta>
            <ta e="T455" id="Seg_4885" s="T454">schneiden-CVB.SEQ</ta>
            <ta e="T456" id="Seg_4886" s="T455">werfen-PRS.[3SG]</ta>
            <ta e="T457" id="Seg_4887" s="T456">dieses</ta>
            <ta e="T458" id="Seg_4888" s="T457">Kind-2SG.[NOM]</ta>
            <ta e="T459" id="Seg_4889" s="T458">dieses-ACC</ta>
            <ta e="T460" id="Seg_4890" s="T459">hören-PRS.[3SG]</ta>
            <ta e="T461" id="Seg_4891" s="T460">und</ta>
            <ta e="T462" id="Seg_4892" s="T461">Lyypyrdaan-ACC</ta>
            <ta e="T463" id="Seg_4893" s="T462">töten-CVB.SEQ</ta>
            <ta e="T464" id="Seg_4894" s="T463">werfen-PRS.[3SG]</ta>
            <ta e="T465" id="Seg_4895" s="T464">Reichtum-3SG-ACC-Sattheit-3SG-ACC</ta>
            <ta e="T466" id="Seg_4896" s="T465">Haus-3SG-DAT/LOC</ta>
            <ta e="T467" id="Seg_4897" s="T466">tragen-CVB.SIM</ta>
            <ta e="T468" id="Seg_4898" s="T467">gehen-PRS.[3SG]</ta>
            <ta e="T469" id="Seg_4899" s="T468">dieses</ta>
            <ta e="T470" id="Seg_4900" s="T469">Reichtum-2SG-ACC</ta>
            <ta e="T471" id="Seg_4901" s="T470">Haus-3SG-DAT/LOC</ta>
            <ta e="T472" id="Seg_4902" s="T471">bringen-CVB.SEQ</ta>
            <ta e="T473" id="Seg_4903" s="T472">Mutter-3SG-ACC</ta>
            <ta e="T474" id="Seg_4904" s="T473">mit</ta>
            <ta e="T475" id="Seg_4905" s="T474">reich.sein-CVB.SEQ-reich.sein-CVB.SEQ</ta>
            <ta e="T476" id="Seg_4906" s="T475">doch</ta>
            <ta e="T477" id="Seg_4907" s="T476">leben-PST2-3SG</ta>
            <ta e="T478" id="Seg_4908" s="T477">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_4909" s="T0">Лыыпырдаан.[NOM]</ta>
            <ta e="T2" id="Seg_4910" s="T1">вот</ta>
            <ta e="T3" id="Seg_4911" s="T2">жить-PRS.[3SG]</ta>
            <ta e="T4" id="Seg_4912" s="T3">Лыыпырдаан.[NOM]</ta>
            <ta e="T5" id="Seg_4913" s="T4">какой</ta>
            <ta e="T6" id="Seg_4914" s="T5">INDEF</ta>
            <ta e="T7" id="Seg_4915" s="T6">в.сторону</ta>
            <ta e="T8" id="Seg_4916" s="T7">идти-PRS.[3SG]</ta>
            <ta e="T9" id="Seg_4917" s="T8">этот</ta>
            <ta e="T10" id="Seg_4918" s="T9">идти-CVB.SEQ</ta>
            <ta e="T11" id="Seg_4919" s="T10">жена-PROPR</ta>
            <ta e="T12" id="Seg_4920" s="T11">человек-ACC</ta>
            <ta e="T13" id="Seg_4921" s="T12">найти-PRS.[3SG]</ta>
            <ta e="T14" id="Seg_4922" s="T13">мальчик-PROPR.[NOM]</ta>
            <ta e="T15" id="Seg_4923" s="T14">тот</ta>
            <ta e="T16" id="Seg_4924" s="T15">человек.[NOM]</ta>
            <ta e="T17" id="Seg_4925" s="T16">жена-PROPR</ta>
            <ta e="T18" id="Seg_4926" s="T17">человек-2SG.[NOM]</ta>
            <ta e="T19" id="Seg_4927" s="T18">отдельно</ta>
            <ta e="T20" id="Seg_4928" s="T19">кочевать-PST2.[3SG]</ta>
            <ta e="T21" id="Seg_4929" s="T20">одиноко</ta>
            <ta e="T22" id="Seg_4930" s="T21">рассвет.[NOM]</ta>
            <ta e="T23" id="Seg_4931" s="T22">быть-CVB.SEQ</ta>
            <ta e="T24" id="Seg_4932" s="T23">вечером</ta>
            <ta e="T25" id="Seg_4933" s="T24">один</ta>
            <ta e="T26" id="Seg_4934" s="T25">место-DAT/LOC</ta>
            <ta e="T27" id="Seg_4935" s="T26">падать-PST2.[3SG]</ta>
            <ta e="T28" id="Seg_4936" s="T27">спать-CVB.PURP</ta>
            <ta e="T29" id="Seg_4937" s="T28">обувь-3SG-ACC</ta>
            <ta e="T30" id="Seg_4938" s="T29">голый-VBZ-CVB.SEQ</ta>
            <ta e="T31" id="Seg_4939" s="T30">быть-TEMP-3SG</ta>
            <ta e="T32" id="Seg_4940" s="T31">огонь-3SG.[NOM]</ta>
            <ta e="T33" id="Seg_4941" s="T32">сколько-MLTP</ta>
            <ta e="T34" id="Seg_4942" s="T33">INDED</ta>
            <ta e="T35" id="Seg_4943" s="T34">делать.звук-PST2.[3SG]</ta>
            <ta e="T36" id="Seg_4944" s="T35">этот</ta>
            <ta e="T37" id="Seg_4945" s="T36">человек-2SG.[NOM]</ta>
            <ta e="T38" id="Seg_4946" s="T37">1SG.[NOM]</ta>
            <ta e="T39" id="Seg_4947" s="T38">днем</ta>
            <ta e="T40" id="Seg_4948" s="T39">идти-EP-PST2-EP-1SG</ta>
            <ta e="T41" id="Seg_4949" s="T40">EMPH</ta>
            <ta e="T42" id="Seg_4950" s="T41">что.[NOM]</ta>
            <ta e="T43" id="Seg_4951" s="T42">приходить-FUT.[3SG]=Q</ta>
            <ta e="T44" id="Seg_4952" s="T43">огонь-EP-1SG.[NOM]</ta>
            <ta e="T45" id="Seg_4953" s="T44">почему</ta>
            <ta e="T46" id="Seg_4954" s="T45">говорить-PRS.[3SG]</ta>
            <ta e="T47" id="Seg_4955" s="T46">думать-CVB.SEQ</ta>
            <ta e="T48" id="Seg_4956" s="T47">бояться-NEG.[3SG]</ta>
            <ta e="T49" id="Seg_4957" s="T48">ложиться-PRS.[3SG]</ta>
            <ta e="T50" id="Seg_4958" s="T49">Лыыпырдаан.[NOM]</ta>
            <ta e="T51" id="Seg_4959" s="T50">приходить-CVB.SEQ</ta>
            <ta e="T52" id="Seg_4960" s="T51">этот</ta>
            <ta e="T53" id="Seg_4961" s="T52">серебро.[NOM]</ta>
            <ta e="T54" id="Seg_4962" s="T53">сажень-PROPR</ta>
            <ta e="T55" id="Seg_4963" s="T54">лук-PROPR</ta>
            <ta e="T56" id="Seg_4964" s="T55">человек-ACC</ta>
            <ta e="T57" id="Seg_4965" s="T56">убить-CVB.SEQ</ta>
            <ta e="T58" id="Seg_4966" s="T57">бросать-PRS.[3SG]</ta>
            <ta e="T59" id="Seg_4967" s="T58">лук.[NOM]</ta>
            <ta e="T60" id="Seg_4968" s="T59">лук.[NOM]</ta>
            <ta e="T61" id="Seg_4969" s="T60">MOD</ta>
            <ta e="T62" id="Seg_4970" s="T61">EMPH</ta>
            <ta e="T63" id="Seg_4971" s="T62">человек-2SG.[NOM]</ta>
            <ta e="T64" id="Seg_4972" s="T63">жена-3SG.[NOM]</ta>
            <ta e="T65" id="Seg_4973" s="T64">маленький</ta>
            <ta e="T66" id="Seg_4974" s="T65">ребенок-3SG-ACC</ta>
            <ta e="T67" id="Seg_4975" s="T66">с</ta>
            <ta e="T68" id="Seg_4976" s="T67">место-DAT/LOC</ta>
            <ta e="T69" id="Seg_4977" s="T68">спасаться-CVB.SEQ</ta>
            <ta e="T70" id="Seg_4978" s="T69">оставаться-PRS.[3SG]</ta>
            <ta e="T71" id="Seg_4979" s="T70">жена-EP-2SG.[NOM]</ta>
            <ta e="T72" id="Seg_4980" s="T71">что-POSS</ta>
            <ta e="T73" id="Seg_4981" s="T72">NEG</ta>
            <ta e="T74" id="Seg_4982" s="T73">NEG.[3SG]</ta>
            <ta e="T75" id="Seg_4983" s="T74">земля.[NOM]</ta>
            <ta e="T76" id="Seg_4984" s="T75">землянка-DIM-DAT/LOC</ta>
            <ta e="T77" id="Seg_4985" s="T76">жить-PRS.[3SG]</ta>
            <ta e="T78" id="Seg_4986" s="T77">сказка.[NOM]</ta>
            <ta e="T79" id="Seg_4987" s="T78">ребенок-3SG.[NOM]</ta>
            <ta e="T80" id="Seg_4988" s="T79">долго</ta>
            <ta e="T81" id="Seg_4989" s="T80">быть-NEG.[3SG]</ta>
            <ta e="T82" id="Seg_4990" s="T81">расти-CVB.SEQ</ta>
            <ta e="T83" id="Seg_4991" s="T82">оставаться-PRS.[3SG]</ta>
            <ta e="T84" id="Seg_4992" s="T83">ребенок.[NOM]</ta>
            <ta e="T85" id="Seg_4993" s="T84">выйти-CVB.SEQ</ta>
            <ta e="T86" id="Seg_4994" s="T85">видеть-PST2-3SG</ta>
            <ta e="T87" id="Seg_4995" s="T86">дом-3SG-GEN</ta>
            <ta e="T88" id="Seg_4996" s="T87">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T89" id="Seg_4997" s="T88">белый-PL.[NOM]</ta>
            <ta e="T90" id="Seg_4998" s="T89">идти-PRS-3PL</ta>
            <ta e="T91" id="Seg_4999" s="T90">мать-3SG-DAT/LOC</ta>
            <ta e="T92" id="Seg_5000" s="T91">входить-CVB.SEQ</ta>
            <ta e="T93" id="Seg_5001" s="T92">рассказывать-PRS.[3SG]</ta>
            <ta e="T94" id="Seg_5002" s="T93">ээ</ta>
            <ta e="T95" id="Seg_5003" s="T94">тогда</ta>
            <ta e="T96" id="Seg_5004" s="T95">куропатка-PL.[NOM]</ta>
            <ta e="T97" id="Seg_5005" s="T96">MOD</ta>
            <ta e="T98" id="Seg_5006" s="T97">EMPH</ta>
            <ta e="T99" id="Seg_5007" s="T98">говорить-PRS.[3SG]</ta>
            <ta e="T100" id="Seg_5008" s="T99">мать-3SG.[NOM]</ta>
            <ta e="T101" id="Seg_5009" s="T100">о</ta>
            <ta e="T102" id="Seg_5010" s="T101">отец-PROPR-EP-1SG.[NOM]</ta>
            <ta e="T103" id="Seg_5011" s="T102">быть-PST2-3SG</ta>
            <ta e="T104" id="Seg_5012" s="T103">быть-COND.[3SG]</ta>
            <ta e="T105" id="Seg_5013" s="T104">как</ta>
            <ta e="T106" id="Seg_5014" s="T105">INDEF</ta>
            <ta e="T107" id="Seg_5015" s="T106">делать-FUT.[3SG]</ta>
            <ta e="T108" id="Seg_5016" s="T107">быть-PST1-3SG</ta>
            <ta e="T109" id="Seg_5017" s="T108">MOD</ta>
            <ta e="T110" id="Seg_5018" s="T109">говорить-PRS.[3SG]</ta>
            <ta e="T111" id="Seg_5019" s="T110">мальчик.[NOM]</ta>
            <ta e="T112" id="Seg_5020" s="T111">мать-3SG-DAT/LOC</ta>
            <ta e="T113" id="Seg_5021" s="T112">мать-3SG.[NOM]</ta>
            <ta e="T114" id="Seg_5022" s="T113">сын.[NOM]</ta>
            <ta e="T115" id="Seg_5023" s="T114">отец-PROPR-EP-GEN</ta>
            <ta e="T116" id="Seg_5024" s="T115">сторона-3SG-INSTR</ta>
            <ta e="T117" id="Seg_5025" s="T116">вообще</ta>
            <ta e="T118" id="Seg_5026" s="T117">рассказывать-NEG.[3SG]</ta>
            <ta e="T119" id="Seg_5027" s="T118">мать.[NOM]</ta>
            <ta e="T120" id="Seg_5028" s="T119">волос-3SG-ACC</ta>
            <ta e="T121" id="Seg_5029" s="T120">резать-CVB.SEQ</ta>
            <ta e="T122" id="Seg_5030" s="T121">нить.[NOM]</ta>
            <ta e="T123" id="Seg_5031" s="T122">делать-PRS.[3SG]</ta>
            <ta e="T124" id="Seg_5032" s="T123">мальчик.[NOM]</ta>
            <ta e="T125" id="Seg_5033" s="T124">этот-INSTR</ta>
            <ta e="T126" id="Seg_5034" s="T125">силок.[NOM]</ta>
            <ta e="T127" id="Seg_5035" s="T126">делать-PRS.[3SG]</ta>
            <ta e="T128" id="Seg_5036" s="T127">этот-INSTR</ta>
            <ta e="T129" id="Seg_5037" s="T128">куропатка-ACC</ta>
            <ta e="T130" id="Seg_5038" s="T129">убить-CVB.SEQ</ta>
            <ta e="T131" id="Seg_5039" s="T130">тот-INSTR</ta>
            <ta e="T132" id="Seg_5040" s="T131">кормить-PASS/REFL-CVB.SEQ</ta>
            <ta e="T133" id="Seg_5041" s="T132">человек.[NOM]</ta>
            <ta e="T134" id="Seg_5042" s="T133">быть-CVB.SEQ</ta>
            <ta e="T135" id="Seg_5043" s="T134">жить-PRS-3PL</ta>
            <ta e="T136" id="Seg_5044" s="T135">этот</ta>
            <ta e="T137" id="Seg_5045" s="T136">жить-CVB.SEQ</ta>
            <ta e="T138" id="Seg_5046" s="T137">мальчик.[NOM]</ta>
            <ta e="T139" id="Seg_5047" s="T138">всегда</ta>
            <ta e="T140" id="Seg_5048" s="T139">спрашивать-PRS.[3SG]</ta>
            <ta e="T141" id="Seg_5049" s="T140">1SG.[NOM]</ta>
            <ta e="T142" id="Seg_5050" s="T141">отец-PROPR.[NOM]</ta>
            <ta e="T143" id="Seg_5051" s="T142">быть-PST1-1SG</ta>
            <ta e="T144" id="Seg_5052" s="T143">мать-3SG.[NOM]</ta>
            <ta e="T145" id="Seg_5053" s="T144">вообще</ta>
            <ta e="T146" id="Seg_5054" s="T145">говорить-NEG.PTCP</ta>
            <ta e="T147" id="Seg_5055" s="T146">быть-PST2.[3SG]</ta>
            <ta e="T148" id="Seg_5056" s="T147">скрывать-PTCP.PRS</ta>
            <ta e="T149" id="Seg_5057" s="T148">быть-PST2.[3SG]</ta>
            <ta e="T150" id="Seg_5058" s="T149">Лыыпырдаан.[NOM]</ta>
            <ta e="T151" id="Seg_5059" s="T150">опять</ta>
            <ta e="T152" id="Seg_5060" s="T151">человек-ACC</ta>
            <ta e="T153" id="Seg_5061" s="T152">убить-CVB.PURP</ta>
            <ta e="T154" id="Seg_5062" s="T153">идти-PRS.[3SG]</ta>
            <ta e="T155" id="Seg_5063" s="T154">один</ta>
            <ta e="T156" id="Seg_5064" s="T155">сын-PROPR</ta>
            <ta e="T157" id="Seg_5065" s="T156">старуха-DAT/LOC</ta>
            <ta e="T158" id="Seg_5066" s="T157">приходить-PRS.[3SG]</ta>
            <ta e="T159" id="Seg_5067" s="T158">вдруг</ta>
            <ta e="T160" id="Seg_5068" s="T159">видеть-PST2-3SG</ta>
            <ta e="T161" id="Seg_5069" s="T160">очень</ta>
            <ta e="T162" id="Seg_5070" s="T161">толстый</ta>
            <ta e="T163" id="Seg_5071" s="T162">лиственница.[NOM]</ta>
            <ta e="T164" id="Seg_5072" s="T163">дерево.[NOM]</ta>
            <ta e="T165" id="Seg_5073" s="T164">стоять-PRS.[3SG]</ta>
            <ta e="T166" id="Seg_5074" s="T165">этот</ta>
            <ta e="T167" id="Seg_5075" s="T166">дерево-DAT/LOC</ta>
            <ta e="T168" id="Seg_5076" s="T167">старуха.[NOM]</ta>
            <ta e="T169" id="Seg_5077" s="T168">сын-3SG.[NOM]</ta>
            <ta e="T170" id="Seg_5078" s="T169">лук-3SG-ACC</ta>
            <ta e="T171" id="Seg_5079" s="T170">накидывать-CVB.SEQ</ta>
            <ta e="T172" id="Seg_5080" s="T171">бросать-PST2.[3SG]</ta>
            <ta e="T173" id="Seg_5081" s="T172">Лыыпырдаан.[NOM]</ta>
            <ta e="T174" id="Seg_5082" s="T173">спрашивать-PRS.[3SG]</ta>
            <ta e="T175" id="Seg_5083" s="T174">тот.[NOM]</ta>
            <ta e="T176" id="Seg_5084" s="T175">кто.[NOM]</ta>
            <ta e="T177" id="Seg_5085" s="T176">лук-3SG.[NOM]=Q</ta>
            <ta e="T178" id="Seg_5086" s="T177">что</ta>
            <ta e="T179" id="Seg_5087" s="T178">семья-PROPR-2SG</ta>
            <ta e="T180" id="Seg_5088" s="T179">старуха.[NOM]</ta>
            <ta e="T181" id="Seg_5089" s="T180">мальчик-PROPR-1SG</ta>
            <ta e="T182" id="Seg_5090" s="T181">говорить-PRS.[3SG]</ta>
            <ta e="T183" id="Seg_5091" s="T182">INTJ</ta>
            <ta e="T184" id="Seg_5092" s="T183">тот</ta>
            <ta e="T185" id="Seg_5093" s="T184">лук-3SG-ACC</ta>
            <ta e="T186" id="Seg_5094" s="T185">почему</ta>
            <ta e="T187" id="Seg_5095" s="T186">оставлять-PST2-3SG=Q</ta>
            <ta e="T188" id="Seg_5096" s="T187">EMPH</ta>
            <ta e="T189" id="Seg_5097" s="T188">тот.[NOM]</ta>
            <ta e="T190" id="Seg_5098" s="T189">лук.[NOM]</ta>
            <ta e="T191" id="Seg_5099" s="T190">сила-3SG.[NOM]</ta>
            <ta e="T192" id="Seg_5100" s="T191">выйти-EP-PST2.[3SG]</ta>
            <ta e="T193" id="Seg_5101" s="T192">тот.[NOM]</ta>
            <ta e="T194" id="Seg_5102" s="T193">из_за</ta>
            <ta e="T195" id="Seg_5103" s="T194">оставлять-PST2-3SG</ta>
            <ta e="T196" id="Seg_5104" s="T195">говорить-PRS.[3SG]</ta>
            <ta e="T197" id="Seg_5105" s="T196">старуха.[NOM]</ta>
            <ta e="T198" id="Seg_5106" s="T197">потом</ta>
            <ta e="T199" id="Seg_5107" s="T198">валун.[NOM]</ta>
            <ta e="T200" id="Seg_5108" s="T199">лежать-PRS.[3SG]</ta>
            <ta e="T201" id="Seg_5109" s="T200">большой</ta>
            <ta e="T202" id="Seg_5110" s="T201">очень</ta>
            <ta e="T203" id="Seg_5111" s="T202">камень.[NOM]</ta>
            <ta e="T205" id="Seg_5112" s="T204">этот-ACC</ta>
            <ta e="T206" id="Seg_5113" s="T205">тоже</ta>
            <ta e="T207" id="Seg_5114" s="T206">спрашивать-PRS.[3SG]</ta>
            <ta e="T208" id="Seg_5115" s="T207">тот.самый</ta>
            <ta e="T209" id="Seg_5116" s="T208">человек.[NOM]</ta>
            <ta e="T210" id="Seg_5117" s="T209">старуха.[NOM]</ta>
            <ta e="T211" id="Seg_5118" s="T210">тот-ACC</ta>
            <ta e="T212" id="Seg_5119" s="T211">тот.[NOM]</ta>
            <ta e="T213" id="Seg_5120" s="T212">тосковать-TEMP-3SG</ta>
            <ta e="T214" id="Seg_5121" s="T213">мяч.[NOM]</ta>
            <ta e="T215" id="Seg_5122" s="T214">делать-HAB.[3SG]</ta>
            <ta e="T216" id="Seg_5123" s="T215">говорить-PRS.[3SG]</ta>
            <ta e="T217" id="Seg_5124" s="T216">тот</ta>
            <ta e="T218" id="Seg_5125" s="T217">гора-DAT/LOC</ta>
            <ta e="T219" id="Seg_5126" s="T218">пока</ta>
            <ta e="T220" id="Seg_5127" s="T219">пнуть-TEMP-3SG</ta>
            <ta e="T221" id="Seg_5128" s="T220">назад</ta>
            <ta e="T222" id="Seg_5129" s="T221">кататься-CVB.SEQ</ta>
            <ta e="T223" id="Seg_5130" s="T222">приходить-HAB.[3SG]</ta>
            <ta e="T224" id="Seg_5131" s="T223">тот.[NOM]</ta>
            <ta e="T225" id="Seg_5132" s="T224">как</ta>
            <ta e="T226" id="Seg_5133" s="T225">играть-HAB.[3SG]</ta>
            <ta e="T227" id="Seg_5134" s="T226">говорить-PRS.[3SG]</ta>
            <ta e="T228" id="Seg_5135" s="T227">Лыыпырдаан.[NOM]</ta>
            <ta e="T229" id="Seg_5136" s="T228">говорить-PRS.[3SG]</ta>
            <ta e="T230" id="Seg_5137" s="T229">завтра</ta>
            <ta e="T231" id="Seg_5138" s="T230">1SG.[NOM]</ta>
            <ta e="T232" id="Seg_5139" s="T231">родиться-PTCP.PST</ta>
            <ta e="T233" id="Seg_5140" s="T232">день-VBZ-PRS-1SG</ta>
            <ta e="T234" id="Seg_5141" s="T233">там</ta>
            <ta e="T235" id="Seg_5142" s="T234">мальчик-EP-2SG.[NOM]</ta>
            <ta e="T236" id="Seg_5143" s="T235">приходить-FUT-IMP.3SG</ta>
            <ta e="T237" id="Seg_5144" s="T236">этот</ta>
            <ta e="T238" id="Seg_5145" s="T237">сын.[NOM]</ta>
            <ta e="T239" id="Seg_5146" s="T238">дом-3SG-DAT/LOC</ta>
            <ta e="T240" id="Seg_5147" s="T239">приходить-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T241" id="Seg_5148" s="T240">мать-3SG.[NOM]</ta>
            <ta e="T242" id="Seg_5149" s="T241">целый-3SG-ACC</ta>
            <ta e="T243" id="Seg_5150" s="T242">рассказывать-CVB.SEQ</ta>
            <ta e="T244" id="Seg_5151" s="T243">давать-PRS.[3SG]</ta>
            <ta e="T245" id="Seg_5152" s="T244">приглашать-PRS-3PL</ta>
            <ta e="T246" id="Seg_5153" s="T245">говорить-PRS.[3SG]</ta>
            <ta e="T247" id="Seg_5154" s="T246">вот</ta>
            <ta e="T248" id="Seg_5155" s="T247">только</ta>
            <ta e="T249" id="Seg_5156" s="T248">этот</ta>
            <ta e="T250" id="Seg_5157" s="T249">человек-2SG.[NOM]</ta>
            <ta e="T251" id="Seg_5158" s="T250">Лыыпырдаан-DAT/LOC</ta>
            <ta e="T252" id="Seg_5159" s="T251">идти-PST1-3SG</ta>
            <ta e="T253" id="Seg_5160" s="T252">Лыыпырдаан.[NOM]</ta>
            <ta e="T254" id="Seg_5161" s="T253">человек-PL-3SG-DAT/LOC</ta>
            <ta e="T255" id="Seg_5162" s="T254">этот</ta>
            <ta e="T256" id="Seg_5163" s="T255">человек-ACC</ta>
            <ta e="T257" id="Seg_5164" s="T256">изгородь-VBZ-FUT-1PL</ta>
            <ta e="T258" id="Seg_5165" s="T257">убить-FUT-1PL</ta>
            <ta e="T259" id="Seg_5166" s="T258">говорить-CVB.SEQ</ta>
            <ta e="T260" id="Seg_5167" s="T259">говорить-PRS.[3SG]</ta>
            <ta e="T261" id="Seg_5168" s="T260">приходить-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T262" id="Seg_5169" s="T261">гость-3PL-ACC</ta>
            <ta e="T263" id="Seg_5170" s="T262">изгородь-VBZ-CVB.SEQ</ta>
            <ta e="T264" id="Seg_5171" s="T263">бросать-PRS-3PL</ta>
            <ta e="T265" id="Seg_5172" s="T264">убить-CVB.PURP-3PL</ta>
            <ta e="T266" id="Seg_5173" s="T265">советовать-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T267" id="Seg_5174" s="T266">MOD</ta>
            <ta e="T268" id="Seg_5175" s="T267">Лыыпырдаан.[NOM]</ta>
            <ta e="T269" id="Seg_5176" s="T268">бояться-CVB.SEQ</ta>
            <ta e="T270" id="Seg_5177" s="T269">друг-PL-3SG-DAT/LOC</ta>
            <ta e="T271" id="Seg_5178" s="T270">говорить-PRS.[3SG]</ta>
            <ta e="T272" id="Seg_5179" s="T271">этот</ta>
            <ta e="T273" id="Seg_5180" s="T272">что.[NOM]</ta>
            <ta e="T274" id="Seg_5181" s="T273">быть-CVB.SEQ-2PL</ta>
            <ta e="T275" id="Seg_5182" s="T274">собирать-EP-PASS/REFL-PRS-2PL</ta>
            <ta e="T276" id="Seg_5183" s="T275">гость.[NOM]</ta>
            <ta e="T277" id="Seg_5184" s="T276">здесь</ta>
            <ta e="T278" id="Seg_5185" s="T277">говорить-PRS.[3SG]</ta>
            <ta e="T279" id="Seg_5186" s="T278">старик.[NOM]</ta>
            <ta e="T280" id="Seg_5187" s="T279">2SG.[NOM]</ta>
            <ta e="T281" id="Seg_5188" s="T280">1SG-ACC</ta>
            <ta e="T282" id="Seg_5189" s="T281">убить-CVB.PURP</ta>
            <ta e="T283" id="Seg_5190" s="T282">хотеть-PTCP.PRS-2SG-ACC</ta>
            <ta e="T284" id="Seg_5191" s="T283">знать-PRS-1SG</ta>
            <ta e="T285" id="Seg_5192" s="T284">1SG.[NOM]</ta>
            <ta e="T286" id="Seg_5193" s="T285">этот</ta>
            <ta e="T287" id="Seg_5194" s="T286">одинаковый</ta>
            <ta e="T288" id="Seg_5195" s="T287">ножичек-PROPR-1SG</ta>
            <ta e="T289" id="Seg_5196" s="T288">тот-INSTR</ta>
            <ta e="T290" id="Seg_5197" s="T289">народ-2SG-ACC</ta>
            <ta e="T291" id="Seg_5198" s="T290">однажды</ta>
            <ta e="T292" id="Seg_5199" s="T291">истребить-FUT-1SG</ta>
            <ta e="T293" id="Seg_5200" s="T292">быть-PST1-3SG</ta>
            <ta e="T294" id="Seg_5201" s="T293">говорить-PRS.[3SG]</ta>
            <ta e="T295" id="Seg_5202" s="T294">эй=EMPH</ta>
            <ta e="T296" id="Seg_5203" s="T295">тот.[NOM]</ta>
            <ta e="T297" id="Seg_5204" s="T296">как</ta>
            <ta e="T298" id="Seg_5205" s="T297">делать-EP-NEG.[IMP.2SG]</ta>
            <ta e="T299" id="Seg_5206" s="T298">1SG.[NOM]</ta>
            <ta e="T300" id="Seg_5207" s="T299">дочь-1SG-ACC</ta>
            <ta e="T301" id="Seg_5208" s="T300">жена.[NOM]</ta>
            <ta e="T302" id="Seg_5209" s="T301">делать.[IMP.2SG]</ta>
            <ta e="T303" id="Seg_5210" s="T302">говорить-PRS.[3SG]</ta>
            <ta e="T304" id="Seg_5211" s="T303">гость-3SG.[NOM]</ta>
            <ta e="T305" id="Seg_5212" s="T304">Лыыпырдаан.[NOM]</ta>
            <ta e="T306" id="Seg_5213" s="T305">дочь-3SG-ACC</ta>
            <ta e="T307" id="Seg_5214" s="T306">жена.[NOM]</ta>
            <ta e="T308" id="Seg_5215" s="T307">делать-CVB.SIM</ta>
            <ta e="T309" id="Seg_5216" s="T308">лежать-PRS.[3SG]</ta>
            <ta e="T310" id="Seg_5217" s="T309">этот</ta>
            <ta e="T311" id="Seg_5218" s="T310">мальчик.[NOM]</ta>
            <ta e="T312" id="Seg_5219" s="T311">расти-CVB.SEQ</ta>
            <ta e="T313" id="Seg_5220" s="T312">после</ta>
            <ta e="T314" id="Seg_5221" s="T313">отец-3SG-ACC</ta>
            <ta e="T315" id="Seg_5222" s="T314">убить-PTCP.PST</ta>
            <ta e="T316" id="Seg_5223" s="T315">человек-ACC</ta>
            <ta e="T317" id="Seg_5224" s="T316">всегда</ta>
            <ta e="T318" id="Seg_5225" s="T317">искать-PTCP.PRS</ta>
            <ta e="T319" id="Seg_5226" s="T318">быть-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_5227" s="T319">отец-3SG.[NOM]</ta>
            <ta e="T321" id="Seg_5228" s="T320">умирать-PTCP.PST</ta>
            <ta e="T322" id="Seg_5229" s="T321">место-3SG-DAT/LOC</ta>
            <ta e="T323" id="Seg_5230" s="T322">расти-CVB.SEQ</ta>
            <ta e="T324" id="Seg_5231" s="T323">после</ta>
            <ta e="T325" id="Seg_5232" s="T324">всегда</ta>
            <ta e="T326" id="Seg_5233" s="T325">идти-PTCP.PRS</ta>
            <ta e="T327" id="Seg_5234" s="T326">быть-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_5235" s="T327">только</ta>
            <ta e="T329" id="Seg_5236" s="T328">этот</ta>
            <ta e="T330" id="Seg_5237" s="T329">лежать-TEMP-3SG</ta>
            <ta e="T331" id="Seg_5238" s="T330">два</ta>
            <ta e="T332" id="Seg_5239" s="T331">человек.[NOM]</ta>
            <ta e="T333" id="Seg_5240" s="T332">идти-PRS-3PL</ta>
            <ta e="T334" id="Seg_5241" s="T333">только</ta>
            <ta e="T335" id="Seg_5242" s="T334">один-3PL.[NOM]</ta>
            <ta e="T336" id="Seg_5243" s="T335">мерзлый</ta>
            <ta e="T337" id="Seg_5244" s="T336">медведь-ACC</ta>
            <ta e="T338" id="Seg_5245" s="T337">грудь-3SG-DAT/LOC</ta>
            <ta e="T339" id="Seg_5246" s="T338">связывать-CVB.SEQ</ta>
            <ta e="T340" id="Seg_5247" s="T339">идти-PRS.[3SG]</ta>
            <ta e="T341" id="Seg_5248" s="T340">этот-DAT/LOC</ta>
            <ta e="T342" id="Seg_5249" s="T341">друг-3SG.[NOM]</ta>
            <ta e="T343" id="Seg_5250" s="T342">говорить-PRS.[3SG]</ta>
            <ta e="T344" id="Seg_5251" s="T343">здесь</ta>
            <ta e="T345" id="Seg_5252" s="T344">эй</ta>
            <ta e="T346" id="Seg_5253" s="T345">Лыыпырдаан.[NOM]</ta>
            <ta e="T347" id="Seg_5254" s="T346">серебро</ta>
            <ta e="T348" id="Seg_5255" s="T347">сажень-PROPR</ta>
            <ta e="T349" id="Seg_5256" s="T348">лук-PROPR</ta>
            <ta e="T350" id="Seg_5257" s="T349">человек-ACC</ta>
            <ta e="T351" id="Seg_5258" s="T350">убить-PST2-3SG</ta>
            <ta e="T352" id="Seg_5259" s="T351">есть</ta>
            <ta e="T353" id="Seg_5260" s="T352">тот</ta>
            <ta e="T354" id="Seg_5261" s="T353">человек.[NOM]</ta>
            <ta e="T355" id="Seg_5262" s="T354">сын-3SG.[NOM]</ta>
            <ta e="T356" id="Seg_5263" s="T355">говорить-PRS.[3SG]</ta>
            <ta e="T357" id="Seg_5264" s="T356">мальчик.[NOM]</ta>
            <ta e="T358" id="Seg_5265" s="T357">там</ta>
            <ta e="T359" id="Seg_5266" s="T358">вот</ta>
            <ta e="T360" id="Seg_5267" s="T359">отец-PROPR-3SG-ACC</ta>
            <ta e="T361" id="Seg_5268" s="T360">слышать-PRS.[3SG]</ta>
            <ta e="T362" id="Seg_5269" s="T361">этот</ta>
            <ta e="T363" id="Seg_5270" s="T362">человек-PL-2SG-ACC</ta>
            <ta e="T364" id="Seg_5271" s="T363">вот</ta>
            <ta e="T365" id="Seg_5272" s="T364">кричать-PRS.[3SG]</ta>
            <ta e="T366" id="Seg_5273" s="T365">EMPH</ta>
            <ta e="T367" id="Seg_5274" s="T366">этот</ta>
            <ta e="T368" id="Seg_5275" s="T367">ребенок-2SG.[NOM]</ta>
            <ta e="T369" id="Seg_5276" s="T368">место-1PL.[NOM]</ta>
            <ta e="T370" id="Seg_5277" s="T369">путать-PRS.[3SG]</ta>
            <ta e="T371" id="Seg_5278" s="T370">думать-CVB.SEQ</ta>
            <ta e="T372" id="Seg_5279" s="T371">здесь</ta>
            <ta e="T373" id="Seg_5280" s="T372">медведь-DIM-3PL-ACC</ta>
            <ta e="T374" id="Seg_5281" s="T373">оставаться-CAUS-CVB.SIM</ta>
            <ta e="T375" id="Seg_5282" s="T374">уходить-PRS-3PL</ta>
            <ta e="T376" id="Seg_5283" s="T375">мальчик.[NOM]</ta>
            <ta e="T377" id="Seg_5284" s="T376">медведь-ACC</ta>
            <ta e="T378" id="Seg_5285" s="T377">с</ta>
            <ta e="T379" id="Seg_5286" s="T378">белуха-3SG-ACC</ta>
            <ta e="T380" id="Seg_5287" s="T379">дом-3SG-DAT/LOC</ta>
            <ta e="T381" id="Seg_5288" s="T380">мать-3SG-DAT/LOC</ta>
            <ta e="T382" id="Seg_5289" s="T381">носить-CVB.SIM</ta>
            <ta e="T383" id="Seg_5290" s="T382">идти-PRS.[3SG]</ta>
            <ta e="T384" id="Seg_5291" s="T383">этот-PL-ACC</ta>
            <ta e="T385" id="Seg_5292" s="T384">есть-PRS-3PL</ta>
            <ta e="T386" id="Seg_5293" s="T385">MOD</ta>
            <ta e="T387" id="Seg_5294" s="T386">мать-3SG.[NOM]</ta>
            <ta e="T388" id="Seg_5295" s="T387">ругать-PRS.[3SG]</ta>
            <ta e="T389" id="Seg_5296" s="T388">этот</ta>
            <ta e="T390" id="Seg_5297" s="T389">что.[NOM]</ta>
            <ta e="T391" id="Seg_5298" s="T390">долго</ta>
            <ta e="T392" id="Seg_5299" s="T391">идти-PRS-2SG</ta>
            <ta e="T393" id="Seg_5300" s="T392">убить-CVB.SEQ</ta>
            <ta e="T394" id="Seg_5301" s="T393">бросать-FUT-3PL</ta>
            <ta e="T395" id="Seg_5302" s="T394">сын-3SG.[NOM]</ta>
            <ta e="T396" id="Seg_5303" s="T395">мать-3SG-GEN</ta>
            <ta e="T397" id="Seg_5304" s="T396">слово-3SG-ACC</ta>
            <ta e="T398" id="Seg_5305" s="T397">вообще</ta>
            <ta e="T399" id="Seg_5306" s="T398">слышать-EP-NEG.[3SG]</ta>
            <ta e="T400" id="Seg_5307" s="T399">этот</ta>
            <ta e="T401" id="Seg_5308" s="T400">мальчик.[NOM]</ta>
            <ta e="T402" id="Seg_5309" s="T401">всегда</ta>
            <ta e="T403" id="Seg_5310" s="T402">отец-3SG-ACC</ta>
            <ta e="T404" id="Seg_5311" s="T403">искать-PRS.[3SG]</ta>
            <ta e="T405" id="Seg_5312" s="T404">этот.[NOM]</ta>
            <ta e="T406" id="Seg_5313" s="T405">после</ta>
            <ta e="T407" id="Seg_5314" s="T406">Лыыпырдаан.[NOM]</ta>
            <ta e="T408" id="Seg_5315" s="T407">старик.[NOM]</ta>
            <ta e="T409" id="Seg_5316" s="T408">дом-3SG-ACC</ta>
            <ta e="T410" id="Seg_5317" s="T409">вот</ta>
            <ta e="T411" id="Seg_5318" s="T410">найти-PRS.[3SG]</ta>
            <ta e="T412" id="Seg_5319" s="T411">этот</ta>
            <ta e="T413" id="Seg_5320" s="T412">найти-CVB.SEQ</ta>
            <ta e="T414" id="Seg_5321" s="T413">вечерний</ta>
            <ta e="T415" id="Seg_5322" s="T414">сон-INSTR</ta>
            <ta e="T416" id="Seg_5323" s="T415">спать-CVB.SIM</ta>
            <ta e="T417" id="Seg_5324" s="T416">лежать-TEMP-3PL</ta>
            <ta e="T418" id="Seg_5325" s="T417">приходить-PRS.[3SG]</ta>
            <ta e="T419" id="Seg_5326" s="T418">спать-CVB.SIM</ta>
            <ta e="T420" id="Seg_5327" s="T419">лежать-PTCP.PRS</ta>
            <ta e="T421" id="Seg_5328" s="T420">человек-ACC</ta>
            <ta e="T422" id="Seg_5329" s="T421">стрела-3SG-INSTR</ta>
            <ta e="T423" id="Seg_5330" s="T422">толстый</ta>
            <ta e="T424" id="Seg_5331" s="T423">сухожилие-3PL-ACC</ta>
            <ta e="T425" id="Seg_5332" s="T424">резать-CVB.SIM</ta>
            <ta e="T426" id="Seg_5333" s="T425">стрелять-FREQ-PRS.[3SG]</ta>
            <ta e="T427" id="Seg_5334" s="T426">Лыыпырдаан.[NOM]</ta>
            <ta e="T428" id="Seg_5335" s="T427">зять-3SG-ACC</ta>
            <ta e="T429" id="Seg_5336" s="T428">вот</ta>
            <ta e="T430" id="Seg_5337" s="T429">этот</ta>
            <ta e="T431" id="Seg_5338" s="T430">тесть-EP-1SG.[NOM]</ta>
            <ta e="T432" id="Seg_5339" s="T431">плохой-3SG-DAT/LOC</ta>
            <ta e="T433" id="Seg_5340" s="T432">умирать-CVB.SEQ</ta>
            <ta e="T434" id="Seg_5341" s="T433">быть-PRS-1SG</ta>
            <ta e="T435" id="Seg_5342" s="T434">1SG.[NOM]</ta>
            <ta e="T436" id="Seg_5343" s="T435">2SG.[NOM]</ta>
            <ta e="T437" id="Seg_5344" s="T436">отец-2SG-ACC</ta>
            <ta e="T438" id="Seg_5345" s="T437">убить-PST2.NEG-EP-1SG</ta>
            <ta e="T439" id="Seg_5346" s="T438">говорить-PRS.[3SG]</ta>
            <ta e="T440" id="Seg_5347" s="T439">лучше</ta>
            <ta e="T441" id="Seg_5348" s="T440">1SG-ACC</ta>
            <ta e="T442" id="Seg_5349" s="T441">душа-1SG-ACC</ta>
            <ta e="T443" id="Seg_5350" s="T442">резать.[IMP.2SG]</ta>
            <ta e="T444" id="Seg_5351" s="T443">теперь</ta>
            <ta e="T445" id="Seg_5352" s="T444">да</ta>
            <ta e="T446" id="Seg_5353" s="T445">что.[NOM]</ta>
            <ta e="T447" id="Seg_5354" s="T446">человек-3SG.[NOM]</ta>
            <ta e="T448" id="Seg_5355" s="T447">становиться-FUT-1SG=Q</ta>
            <ta e="T449" id="Seg_5356" s="T448">говорить-PRS.[3SG]</ta>
            <ta e="T450" id="Seg_5357" s="T449">этот</ta>
            <ta e="T451" id="Seg_5358" s="T450">ребенок-2SG.[NOM]</ta>
            <ta e="T452" id="Seg_5359" s="T451">этот</ta>
            <ta e="T453" id="Seg_5360" s="T452">человек-ACC</ta>
            <ta e="T454" id="Seg_5361" s="T453">душа-3SG-ACC</ta>
            <ta e="T455" id="Seg_5362" s="T454">резать-CVB.SEQ</ta>
            <ta e="T456" id="Seg_5363" s="T455">бросать-PRS.[3SG]</ta>
            <ta e="T457" id="Seg_5364" s="T456">этот</ta>
            <ta e="T458" id="Seg_5365" s="T457">ребенок-2SG.[NOM]</ta>
            <ta e="T459" id="Seg_5366" s="T458">этот-ACC</ta>
            <ta e="T460" id="Seg_5367" s="T459">слышать-PRS.[3SG]</ta>
            <ta e="T461" id="Seg_5368" s="T460">да</ta>
            <ta e="T462" id="Seg_5369" s="T461">Лыыпырдаан-ACC</ta>
            <ta e="T463" id="Seg_5370" s="T462">убить-CVB.SEQ</ta>
            <ta e="T464" id="Seg_5371" s="T463">бросать-PRS.[3SG]</ta>
            <ta e="T465" id="Seg_5372" s="T464">богатство-3SG-ACC-сытность-3SG-ACC</ta>
            <ta e="T466" id="Seg_5373" s="T465">дом-3SG-DAT/LOC</ta>
            <ta e="T467" id="Seg_5374" s="T466">носить-CVB.SIM</ta>
            <ta e="T468" id="Seg_5375" s="T467">идти-PRS.[3SG]</ta>
            <ta e="T469" id="Seg_5376" s="T468">этот</ta>
            <ta e="T470" id="Seg_5377" s="T469">богатство-2SG-ACC</ta>
            <ta e="T471" id="Seg_5378" s="T470">дом-3SG-DAT/LOC</ta>
            <ta e="T472" id="Seg_5379" s="T471">довезти-CVB.SEQ</ta>
            <ta e="T473" id="Seg_5380" s="T472">мать-3SG-ACC</ta>
            <ta e="T474" id="Seg_5381" s="T473">с</ta>
            <ta e="T475" id="Seg_5382" s="T474">быть.богатым-CVB.SEQ-быть.богатым-CVB.SEQ</ta>
            <ta e="T476" id="Seg_5383" s="T475">вот</ta>
            <ta e="T477" id="Seg_5384" s="T476">жить-PST2-3SG</ta>
            <ta e="T478" id="Seg_5385" s="T477">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_5386" s="T0">propr-n:case</ta>
            <ta e="T2" id="Seg_5387" s="T1">ptcl</ta>
            <ta e="T3" id="Seg_5388" s="T2">v-v:tense-v:pred.pn</ta>
            <ta e="T4" id="Seg_5389" s="T3">propr-n:case</ta>
            <ta e="T5" id="Seg_5390" s="T4">que</ta>
            <ta e="T6" id="Seg_5391" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_5392" s="T6">post</ta>
            <ta e="T8" id="Seg_5393" s="T7">v-v:tense-v:pred.pn</ta>
            <ta e="T9" id="Seg_5394" s="T8">dempro</ta>
            <ta e="T10" id="Seg_5395" s="T9">v-v:cvb</ta>
            <ta e="T11" id="Seg_5396" s="T10">n-n&gt;adj</ta>
            <ta e="T12" id="Seg_5397" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_5398" s="T12">v-v:tense-v:pred.pn</ta>
            <ta e="T14" id="Seg_5399" s="T13">n-n&gt;adj-n:case</ta>
            <ta e="T15" id="Seg_5400" s="T14">dempro</ta>
            <ta e="T16" id="Seg_5401" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_5402" s="T16">n-n&gt;adj</ta>
            <ta e="T18" id="Seg_5403" s="T17">n-n:(poss)-n:case</ta>
            <ta e="T19" id="Seg_5404" s="T18">adv</ta>
            <ta e="T20" id="Seg_5405" s="T19">v-v:tense-v:pred.pn</ta>
            <ta e="T21" id="Seg_5406" s="T20">adv</ta>
            <ta e="T22" id="Seg_5407" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_5408" s="T22">v-v:cvb</ta>
            <ta e="T24" id="Seg_5409" s="T23">adv</ta>
            <ta e="T25" id="Seg_5410" s="T24">cardnum</ta>
            <ta e="T26" id="Seg_5411" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_5412" s="T26">v-v:tense-v:pred.pn</ta>
            <ta e="T28" id="Seg_5413" s="T27">v-v:cvb</ta>
            <ta e="T29" id="Seg_5414" s="T28">n-n:poss-n:case</ta>
            <ta e="T30" id="Seg_5415" s="T29">adj-adj&gt;v-v:cvb</ta>
            <ta e="T31" id="Seg_5416" s="T30">v-v:mood-v:temp.pn</ta>
            <ta e="T32" id="Seg_5417" s="T31">n-n:(poss)-n:case</ta>
            <ta e="T33" id="Seg_5418" s="T32">que-que&gt;adv</ta>
            <ta e="T34" id="Seg_5419" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_5420" s="T34">v-v:tense-v:pred.pn</ta>
            <ta e="T36" id="Seg_5421" s="T35">dempro</ta>
            <ta e="T37" id="Seg_5422" s="T36">n-n:(poss)-n:case</ta>
            <ta e="T38" id="Seg_5423" s="T37">pers-pro:case</ta>
            <ta e="T39" id="Seg_5424" s="T38">adv</ta>
            <ta e="T40" id="Seg_5425" s="T39">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T41" id="Seg_5426" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_5427" s="T41">que-pro:case</ta>
            <ta e="T43" id="Seg_5428" s="T42">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T44" id="Seg_5429" s="T43">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T45" id="Seg_5430" s="T44">que</ta>
            <ta e="T46" id="Seg_5431" s="T45">v-v:tense-v:pred.pn</ta>
            <ta e="T47" id="Seg_5432" s="T46">v-v:cvb</ta>
            <ta e="T48" id="Seg_5433" s="T47">v-v:(neg)-v:pred.pn</ta>
            <ta e="T49" id="Seg_5434" s="T48">v-v:tense-v:pred.pn</ta>
            <ta e="T50" id="Seg_5435" s="T49">propr-n:case</ta>
            <ta e="T51" id="Seg_5436" s="T50">v-v:cvb</ta>
            <ta e="T52" id="Seg_5437" s="T51">dempro</ta>
            <ta e="T53" id="Seg_5438" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_5439" s="T53">n-n&gt;adj</ta>
            <ta e="T55" id="Seg_5440" s="T54">n-n&gt;adj</ta>
            <ta e="T56" id="Seg_5441" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_5442" s="T56">v-v:cvb</ta>
            <ta e="T58" id="Seg_5443" s="T57">v-v:tense-v:pred.pn</ta>
            <ta e="T59" id="Seg_5444" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_5445" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_5446" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_5447" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_5448" s="T62">n-n:(poss)-n:case</ta>
            <ta e="T64" id="Seg_5449" s="T63">n-n:(poss)-n:case</ta>
            <ta e="T65" id="Seg_5450" s="T64">adj</ta>
            <ta e="T66" id="Seg_5451" s="T65">n-n:poss-n:case</ta>
            <ta e="T67" id="Seg_5452" s="T66">post</ta>
            <ta e="T68" id="Seg_5453" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_5454" s="T68">v-v:cvb</ta>
            <ta e="T70" id="Seg_5455" s="T69">v-v:tense-v:pred.pn</ta>
            <ta e="T71" id="Seg_5456" s="T70">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T72" id="Seg_5457" s="T71">que-pro:(poss)</ta>
            <ta e="T73" id="Seg_5458" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_5459" s="T73">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T75" id="Seg_5460" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_5461" s="T75">n-n&gt;n-n:case</ta>
            <ta e="T77" id="Seg_5462" s="T76">v-v:tense-v:pred.pn</ta>
            <ta e="T78" id="Seg_5463" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_5464" s="T78">n-n:(poss)-n:case</ta>
            <ta e="T80" id="Seg_5465" s="T79">adv</ta>
            <ta e="T81" id="Seg_5466" s="T80">v-v:(neg)-v:pred.pn</ta>
            <ta e="T82" id="Seg_5467" s="T81">v-v:cvb</ta>
            <ta e="T83" id="Seg_5468" s="T82">v-v:tense-v:pred.pn</ta>
            <ta e="T84" id="Seg_5469" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_5470" s="T84">v-v:cvb</ta>
            <ta e="T86" id="Seg_5471" s="T85">v-v:tense-v:poss.pn</ta>
            <ta e="T87" id="Seg_5472" s="T86">n-n:poss-n:case</ta>
            <ta e="T88" id="Seg_5473" s="T87">n-n:poss-n:case</ta>
            <ta e="T89" id="Seg_5474" s="T88">adj-n:(num)-n:case</ta>
            <ta e="T90" id="Seg_5475" s="T89">v-v:tense-v:pred.pn</ta>
            <ta e="T91" id="Seg_5476" s="T90">n-n:poss-n:case</ta>
            <ta e="T92" id="Seg_5477" s="T91">v-v:cvb</ta>
            <ta e="T93" id="Seg_5478" s="T92">v-v:tense-v:pred.pn</ta>
            <ta e="T94" id="Seg_5479" s="T93">interj</ta>
            <ta e="T95" id="Seg_5480" s="T94">adv</ta>
            <ta e="T96" id="Seg_5481" s="T95">n-n:(num)-n:case</ta>
            <ta e="T97" id="Seg_5482" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_5483" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_5484" s="T98">v-v:tense-v:pred.pn</ta>
            <ta e="T100" id="Seg_5485" s="T99">n-n:(poss)-n:case</ta>
            <ta e="T101" id="Seg_5486" s="T100">interj</ta>
            <ta e="T102" id="Seg_5487" s="T101">n-n&gt;adj-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T103" id="Seg_5488" s="T102">v-v:tense-v:poss.pn</ta>
            <ta e="T104" id="Seg_5489" s="T103">v-v:mood-v:pred.pn</ta>
            <ta e="T105" id="Seg_5490" s="T104">que</ta>
            <ta e="T106" id="Seg_5491" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_5492" s="T106">v-v:tense-v:poss.pn</ta>
            <ta e="T108" id="Seg_5493" s="T107">v-v:tense-v:poss.pn</ta>
            <ta e="T109" id="Seg_5494" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_5495" s="T109">v-v:tense-v:pred.pn</ta>
            <ta e="T111" id="Seg_5496" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_5497" s="T111">n-n:poss-n:case</ta>
            <ta e="T113" id="Seg_5498" s="T112">n-n:(poss)-n:case</ta>
            <ta e="T114" id="Seg_5499" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_5500" s="T114">n-n&gt;adj-n:(ins)-n:case</ta>
            <ta e="T116" id="Seg_5501" s="T115">n-n:poss-n:case</ta>
            <ta e="T117" id="Seg_5502" s="T116">adv</ta>
            <ta e="T118" id="Seg_5503" s="T117">v-v:(neg)-v:pred.pn</ta>
            <ta e="T119" id="Seg_5504" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_5505" s="T119">n-n:poss-n:case</ta>
            <ta e="T121" id="Seg_5506" s="T120">v-v:cvb</ta>
            <ta e="T122" id="Seg_5507" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_5508" s="T122">v-v:tense-v:pred.pn</ta>
            <ta e="T124" id="Seg_5509" s="T123">n-n:case</ta>
            <ta e="T125" id="Seg_5510" s="T124">dempro-pro:case</ta>
            <ta e="T126" id="Seg_5511" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_5512" s="T126">v-v:tense-v:pred.pn</ta>
            <ta e="T128" id="Seg_5513" s="T127">dempro-pro:case</ta>
            <ta e="T129" id="Seg_5514" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_5515" s="T129">v-v:cvb</ta>
            <ta e="T131" id="Seg_5516" s="T130">dempro-pro:case</ta>
            <ta e="T132" id="Seg_5517" s="T131">v-v&gt;v-v:cvb</ta>
            <ta e="T133" id="Seg_5518" s="T132">n-n:case</ta>
            <ta e="T134" id="Seg_5519" s="T133">v-v:cvb</ta>
            <ta e="T135" id="Seg_5520" s="T134">v-v:tense-v:pred.pn</ta>
            <ta e="T136" id="Seg_5521" s="T135">dempro</ta>
            <ta e="T137" id="Seg_5522" s="T136">v-v:cvb</ta>
            <ta e="T138" id="Seg_5523" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_5524" s="T138">adv</ta>
            <ta e="T140" id="Seg_5525" s="T139">v-v:tense-v:pred.pn</ta>
            <ta e="T141" id="Seg_5526" s="T140">pers-pro:case</ta>
            <ta e="T142" id="Seg_5527" s="T141">n-n&gt;adj-n:case</ta>
            <ta e="T143" id="Seg_5528" s="T142">v-v:tense-v:poss.pn</ta>
            <ta e="T144" id="Seg_5529" s="T143">n-n:(poss)-n:case</ta>
            <ta e="T145" id="Seg_5530" s="T144">adv</ta>
            <ta e="T146" id="Seg_5531" s="T145">v-v:ptcp</ta>
            <ta e="T147" id="Seg_5532" s="T146">v-v:tense-v:pred.pn</ta>
            <ta e="T148" id="Seg_5533" s="T147">v-v:ptcp</ta>
            <ta e="T149" id="Seg_5534" s="T148">v-v:tense-v:pred.pn</ta>
            <ta e="T150" id="Seg_5535" s="T149">propr-n:case</ta>
            <ta e="T151" id="Seg_5536" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_5537" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_5538" s="T152">v-v:cvb</ta>
            <ta e="T154" id="Seg_5539" s="T153">v-v:tense-v:pred.pn</ta>
            <ta e="T155" id="Seg_5540" s="T154">cardnum</ta>
            <ta e="T156" id="Seg_5541" s="T155">n-n&gt;adj</ta>
            <ta e="T157" id="Seg_5542" s="T156">n-n:case</ta>
            <ta e="T158" id="Seg_5543" s="T157">v-v:tense-v:pred.pn</ta>
            <ta e="T159" id="Seg_5544" s="T158">adv</ta>
            <ta e="T160" id="Seg_5545" s="T159">v-v:tense-v:poss.pn</ta>
            <ta e="T161" id="Seg_5546" s="T160">adv</ta>
            <ta e="T162" id="Seg_5547" s="T161">adj</ta>
            <ta e="T163" id="Seg_5548" s="T162">n-n:case</ta>
            <ta e="T164" id="Seg_5549" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_5550" s="T164">v-v:tense-v:pred.pn</ta>
            <ta e="T166" id="Seg_5551" s="T165">dempro</ta>
            <ta e="T167" id="Seg_5552" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_5553" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_5554" s="T168">n-n:(poss)-n:case</ta>
            <ta e="T170" id="Seg_5555" s="T169">n-n:poss-n:case</ta>
            <ta e="T171" id="Seg_5556" s="T170">v-v:cvb</ta>
            <ta e="T172" id="Seg_5557" s="T171">v-v:tense-v:pred.pn</ta>
            <ta e="T173" id="Seg_5558" s="T172">propr-n:case</ta>
            <ta e="T174" id="Seg_5559" s="T173">v-v:tense-v:pred.pn</ta>
            <ta e="T175" id="Seg_5560" s="T174">dempro-pro:case</ta>
            <ta e="T176" id="Seg_5561" s="T175">que-pro:case</ta>
            <ta e="T177" id="Seg_5562" s="T176">n-n:(poss)-n:case-ptcl</ta>
            <ta e="T178" id="Seg_5563" s="T177">que</ta>
            <ta e="T179" id="Seg_5564" s="T178">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T180" id="Seg_5565" s="T179">n-n:case</ta>
            <ta e="T181" id="Seg_5566" s="T180">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T182" id="Seg_5567" s="T181">v-v:tense-v:pred.pn</ta>
            <ta e="T183" id="Seg_5568" s="T182">interj</ta>
            <ta e="T184" id="Seg_5569" s="T183">dempro</ta>
            <ta e="T185" id="Seg_5570" s="T184">n-n:poss-n:case</ta>
            <ta e="T186" id="Seg_5571" s="T185">que</ta>
            <ta e="T187" id="Seg_5572" s="T186">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T188" id="Seg_5573" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_5574" s="T188">dempro-pro:case</ta>
            <ta e="T190" id="Seg_5575" s="T189">n-n:case</ta>
            <ta e="T191" id="Seg_5576" s="T190">n-n:(poss)-n:case</ta>
            <ta e="T192" id="Seg_5577" s="T191">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T193" id="Seg_5578" s="T192">dempro-pro:case</ta>
            <ta e="T194" id="Seg_5579" s="T193">post</ta>
            <ta e="T195" id="Seg_5580" s="T194">v-v:tense-v:poss.pn</ta>
            <ta e="T196" id="Seg_5581" s="T195">v-v:tense-v:pred.pn</ta>
            <ta e="T197" id="Seg_5582" s="T196">n-n:case</ta>
            <ta e="T198" id="Seg_5583" s="T197">adv</ta>
            <ta e="T199" id="Seg_5584" s="T198">n-n:case</ta>
            <ta e="T200" id="Seg_5585" s="T199">v-v:tense-v:pred.pn</ta>
            <ta e="T201" id="Seg_5586" s="T200">adj</ta>
            <ta e="T202" id="Seg_5587" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_5588" s="T202">n-n:case</ta>
            <ta e="T205" id="Seg_5589" s="T204">dempro-pro:case</ta>
            <ta e="T206" id="Seg_5590" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_5591" s="T206">v-v:tense-v:pred.pn</ta>
            <ta e="T208" id="Seg_5592" s="T207">dempro</ta>
            <ta e="T209" id="Seg_5593" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_5594" s="T209">n-n:case</ta>
            <ta e="T211" id="Seg_5595" s="T210">dempro-pro:case</ta>
            <ta e="T212" id="Seg_5596" s="T211">dempro-pro:case</ta>
            <ta e="T213" id="Seg_5597" s="T212">v-v:mood-v:temp.pn</ta>
            <ta e="T214" id="Seg_5598" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_5599" s="T214">v-v:mood-v:pred.pn</ta>
            <ta e="T216" id="Seg_5600" s="T215">v-v:tense-v:pred.pn</ta>
            <ta e="T217" id="Seg_5601" s="T216">dempro</ta>
            <ta e="T218" id="Seg_5602" s="T217">n-n:case</ta>
            <ta e="T219" id="Seg_5603" s="T218">post</ta>
            <ta e="T220" id="Seg_5604" s="T219">v-v:mood-v:temp.pn</ta>
            <ta e="T221" id="Seg_5605" s="T220">adv</ta>
            <ta e="T222" id="Seg_5606" s="T221">v-v:cvb</ta>
            <ta e="T223" id="Seg_5607" s="T222">v-v:mood-v:pred.pn</ta>
            <ta e="T224" id="Seg_5608" s="T223">dempro-pro:case</ta>
            <ta e="T225" id="Seg_5609" s="T224">post</ta>
            <ta e="T226" id="Seg_5610" s="T225">v-v:mood-v:pred.pn</ta>
            <ta e="T227" id="Seg_5611" s="T226">v-v:tense-v:pred.pn</ta>
            <ta e="T228" id="Seg_5612" s="T227">propr-n:case</ta>
            <ta e="T229" id="Seg_5613" s="T228">v-v:tense-v:pred.pn</ta>
            <ta e="T230" id="Seg_5614" s="T229">adv</ta>
            <ta e="T231" id="Seg_5615" s="T230">pers-pro:case</ta>
            <ta e="T232" id="Seg_5616" s="T231">v-v:ptcp</ta>
            <ta e="T233" id="Seg_5617" s="T232">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T234" id="Seg_5618" s="T233">adv</ta>
            <ta e="T235" id="Seg_5619" s="T234">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T236" id="Seg_5620" s="T235">v-v:tense-v:mood.pn</ta>
            <ta e="T237" id="Seg_5621" s="T236">dempro</ta>
            <ta e="T238" id="Seg_5622" s="T237">n-n:case</ta>
            <ta e="T239" id="Seg_5623" s="T238">n-n:poss-n:case</ta>
            <ta e="T240" id="Seg_5624" s="T239">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T241" id="Seg_5625" s="T240">n-n:(poss)-n:case</ta>
            <ta e="T242" id="Seg_5626" s="T241">adj-n:poss-n:case</ta>
            <ta e="T243" id="Seg_5627" s="T242">v-v:cvb</ta>
            <ta e="T244" id="Seg_5628" s="T243">v-v:tense-v:pred.pn</ta>
            <ta e="T245" id="Seg_5629" s="T244">v-v:tense-v:pred.pn</ta>
            <ta e="T246" id="Seg_5630" s="T245">v-v:tense-v:pred.pn</ta>
            <ta e="T247" id="Seg_5631" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_5632" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_5633" s="T248">dempro</ta>
            <ta e="T250" id="Seg_5634" s="T249">n-n:(poss)-n:case</ta>
            <ta e="T251" id="Seg_5635" s="T250">propr-n:case</ta>
            <ta e="T252" id="Seg_5636" s="T251">v-v:tense-v:poss.pn</ta>
            <ta e="T253" id="Seg_5637" s="T252">propr-n:case</ta>
            <ta e="T254" id="Seg_5638" s="T253">n-n:(num)-n:poss-n:case</ta>
            <ta e="T255" id="Seg_5639" s="T254">dempro</ta>
            <ta e="T256" id="Seg_5640" s="T255">n-n:case</ta>
            <ta e="T257" id="Seg_5641" s="T256">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T258" id="Seg_5642" s="T257">v-v:tense-v:poss.pn</ta>
            <ta e="T259" id="Seg_5643" s="T258">v-v:cvb</ta>
            <ta e="T260" id="Seg_5644" s="T259">v-v:tense-v:pred.pn</ta>
            <ta e="T261" id="Seg_5645" s="T260">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T262" id="Seg_5646" s="T261">n-n:poss-n:case</ta>
            <ta e="T263" id="Seg_5647" s="T262">n-n&gt;v-v:cvb</ta>
            <ta e="T264" id="Seg_5648" s="T263">v-v:tense-v:pred.pn</ta>
            <ta e="T265" id="Seg_5649" s="T264">v-v:cvb-v:pred.pn</ta>
            <ta e="T266" id="Seg_5650" s="T265">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T267" id="Seg_5651" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_5652" s="T267">propr-n:case</ta>
            <ta e="T269" id="Seg_5653" s="T268">v-v:cvb</ta>
            <ta e="T270" id="Seg_5654" s="T269">n-n:(num)-n:poss-n:case</ta>
            <ta e="T271" id="Seg_5655" s="T270">v-v:tense-v:pred.pn</ta>
            <ta e="T272" id="Seg_5656" s="T271">dempro</ta>
            <ta e="T273" id="Seg_5657" s="T272">que-pro:case</ta>
            <ta e="T274" id="Seg_5658" s="T273">v-v:cvb-v:pred.pn</ta>
            <ta e="T275" id="Seg_5659" s="T274">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T276" id="Seg_5660" s="T275">n-n:case</ta>
            <ta e="T277" id="Seg_5661" s="T276">adv</ta>
            <ta e="T278" id="Seg_5662" s="T277">v-v:tense-v:pred.pn</ta>
            <ta e="T279" id="Seg_5663" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_5664" s="T279">pers-pro:case</ta>
            <ta e="T281" id="Seg_5665" s="T280">pers-pro:case</ta>
            <ta e="T282" id="Seg_5666" s="T281">v-v:cvb</ta>
            <ta e="T283" id="Seg_5667" s="T282">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T284" id="Seg_5668" s="T283">v-v:tense-v:pred.pn</ta>
            <ta e="T285" id="Seg_5669" s="T284">pers-pro:case</ta>
            <ta e="T286" id="Seg_5670" s="T285">dempro</ta>
            <ta e="T287" id="Seg_5671" s="T286">adj</ta>
            <ta e="T288" id="Seg_5672" s="T287">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T289" id="Seg_5673" s="T288">dempro-pro:case</ta>
            <ta e="T290" id="Seg_5674" s="T289">n-n:poss-n:case</ta>
            <ta e="T291" id="Seg_5675" s="T290">adv</ta>
            <ta e="T292" id="Seg_5676" s="T291">v-v:tense-v:poss.pn</ta>
            <ta e="T293" id="Seg_5677" s="T292">v-v:tense-v:poss.pn</ta>
            <ta e="T294" id="Seg_5678" s="T293">v-v:tense-v:pred.pn</ta>
            <ta e="T295" id="Seg_5679" s="T294">interj-ptcl</ta>
            <ta e="T296" id="Seg_5680" s="T295">dempro-pro:case</ta>
            <ta e="T297" id="Seg_5681" s="T296">post</ta>
            <ta e="T298" id="Seg_5682" s="T297">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T299" id="Seg_5683" s="T298">pers-pro:case</ta>
            <ta e="T300" id="Seg_5684" s="T299">n-n:poss-n:case</ta>
            <ta e="T301" id="Seg_5685" s="T300">n-n:case</ta>
            <ta e="T302" id="Seg_5686" s="T301">v-v:mood.pn</ta>
            <ta e="T303" id="Seg_5687" s="T302">v-v:tense-v:pred.pn</ta>
            <ta e="T304" id="Seg_5688" s="T303">n-n:(poss)-n:case</ta>
            <ta e="T305" id="Seg_5689" s="T304">propr-n:case</ta>
            <ta e="T306" id="Seg_5690" s="T305">n-n:poss-n:case</ta>
            <ta e="T307" id="Seg_5691" s="T306">n-n:case</ta>
            <ta e="T308" id="Seg_5692" s="T307">v-v:cvb</ta>
            <ta e="T309" id="Seg_5693" s="T308">v-v:tense-v:pred.pn</ta>
            <ta e="T310" id="Seg_5694" s="T309">dempro</ta>
            <ta e="T311" id="Seg_5695" s="T310">n-n:case</ta>
            <ta e="T312" id="Seg_5696" s="T311">v-v:cvb</ta>
            <ta e="T313" id="Seg_5697" s="T312">post</ta>
            <ta e="T314" id="Seg_5698" s="T313">n-n:poss-n:case</ta>
            <ta e="T315" id="Seg_5699" s="T314">v-v:ptcp</ta>
            <ta e="T316" id="Seg_5700" s="T315">n-n:case</ta>
            <ta e="T317" id="Seg_5701" s="T316">adv</ta>
            <ta e="T318" id="Seg_5702" s="T317">v-v:ptcp</ta>
            <ta e="T319" id="Seg_5703" s="T318">v-v:tense-v:pred.pn</ta>
            <ta e="T320" id="Seg_5704" s="T319">n-n:(poss)-n:case</ta>
            <ta e="T321" id="Seg_5705" s="T320">v-v:ptcp</ta>
            <ta e="T322" id="Seg_5706" s="T321">n-n:poss-n:case</ta>
            <ta e="T323" id="Seg_5707" s="T322">v-v:cvb</ta>
            <ta e="T324" id="Seg_5708" s="T323">post</ta>
            <ta e="T325" id="Seg_5709" s="T324">adv</ta>
            <ta e="T326" id="Seg_5710" s="T325">v-v:ptcp</ta>
            <ta e="T327" id="Seg_5711" s="T326">v-v:tense-v:pred.pn</ta>
            <ta e="T328" id="Seg_5712" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_5713" s="T328">dempro</ta>
            <ta e="T330" id="Seg_5714" s="T329">v-v:mood-v:temp.pn</ta>
            <ta e="T331" id="Seg_5715" s="T330">cardnum</ta>
            <ta e="T332" id="Seg_5716" s="T331">n-n:case</ta>
            <ta e="T333" id="Seg_5717" s="T332">v-v:tense-v:pred.pn</ta>
            <ta e="T334" id="Seg_5718" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_5719" s="T334">cardnum-n:(poss)-n:case</ta>
            <ta e="T336" id="Seg_5720" s="T335">adj</ta>
            <ta e="T337" id="Seg_5721" s="T336">n-n:case</ta>
            <ta e="T338" id="Seg_5722" s="T337">n-n:poss-n:case</ta>
            <ta e="T339" id="Seg_5723" s="T338">v-v:cvb</ta>
            <ta e="T340" id="Seg_5724" s="T339">v-v:tense-v:pred.pn</ta>
            <ta e="T341" id="Seg_5725" s="T340">dempro-pro:case</ta>
            <ta e="T342" id="Seg_5726" s="T341">n-n:(poss)-n:case</ta>
            <ta e="T343" id="Seg_5727" s="T342">v-v:tense-v:pred.pn</ta>
            <ta e="T344" id="Seg_5728" s="T343">adv</ta>
            <ta e="T345" id="Seg_5729" s="T344">interj</ta>
            <ta e="T346" id="Seg_5730" s="T345">propr-n:case</ta>
            <ta e="T347" id="Seg_5731" s="T346">n</ta>
            <ta e="T348" id="Seg_5732" s="T347">n-n&gt;adj</ta>
            <ta e="T349" id="Seg_5733" s="T348">n-n&gt;adj</ta>
            <ta e="T350" id="Seg_5734" s="T349">n-n:case</ta>
            <ta e="T351" id="Seg_5735" s="T350">v-v:tense-v:poss.pn</ta>
            <ta e="T352" id="Seg_5736" s="T351">ptcl</ta>
            <ta e="T353" id="Seg_5737" s="T352">dempro</ta>
            <ta e="T354" id="Seg_5738" s="T353">n-n:case</ta>
            <ta e="T355" id="Seg_5739" s="T354">n-n:(poss)-n:case</ta>
            <ta e="T356" id="Seg_5740" s="T355">v-v:tense-v:pred.pn</ta>
            <ta e="T357" id="Seg_5741" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_5742" s="T357">adv</ta>
            <ta e="T359" id="Seg_5743" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_5744" s="T359">n-n&gt;adj-n:poss-n:case</ta>
            <ta e="T361" id="Seg_5745" s="T360">v-v:tense-v:pred.pn</ta>
            <ta e="T362" id="Seg_5746" s="T361">dempro</ta>
            <ta e="T363" id="Seg_5747" s="T362">n-n:(num)-n:poss-n:case</ta>
            <ta e="T364" id="Seg_5748" s="T363">ptcl</ta>
            <ta e="T365" id="Seg_5749" s="T364">v-v:tense-v:pred.pn</ta>
            <ta e="T366" id="Seg_5750" s="T365">ptcl</ta>
            <ta e="T367" id="Seg_5751" s="T366">dempro</ta>
            <ta e="T368" id="Seg_5752" s="T367">n-n:(poss)-n:case</ta>
            <ta e="T369" id="Seg_5753" s="T368">n-n:(poss)-n:case</ta>
            <ta e="T370" id="Seg_5754" s="T369">v-v:tense-v:pred.pn</ta>
            <ta e="T371" id="Seg_5755" s="T370">v-v:cvb</ta>
            <ta e="T372" id="Seg_5756" s="T371">adv</ta>
            <ta e="T373" id="Seg_5757" s="T372">n-n&gt;n-n:poss-n:case</ta>
            <ta e="T374" id="Seg_5758" s="T373">v-v&gt;v-v:cvb</ta>
            <ta e="T375" id="Seg_5759" s="T374">v-v:tense-v:pred.pn</ta>
            <ta e="T376" id="Seg_5760" s="T375">n-n:case</ta>
            <ta e="T377" id="Seg_5761" s="T376">n-n:case</ta>
            <ta e="T378" id="Seg_5762" s="T377">post</ta>
            <ta e="T379" id="Seg_5763" s="T378">n-n:poss-n:case</ta>
            <ta e="T380" id="Seg_5764" s="T379">n-n:poss-n:case</ta>
            <ta e="T381" id="Seg_5765" s="T380">n-n:poss-n:case</ta>
            <ta e="T382" id="Seg_5766" s="T381">v-v:cvb</ta>
            <ta e="T383" id="Seg_5767" s="T382">v-v:tense-v:pred.pn</ta>
            <ta e="T384" id="Seg_5768" s="T383">dempro-pro:(num)-pro:case</ta>
            <ta e="T385" id="Seg_5769" s="T384">v-v:tense-v:pred.pn</ta>
            <ta e="T386" id="Seg_5770" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_5771" s="T386">n-n:(poss)-n:case</ta>
            <ta e="T388" id="Seg_5772" s="T387">v-v:tense-v:pred.pn</ta>
            <ta e="T389" id="Seg_5773" s="T388">dempro</ta>
            <ta e="T390" id="Seg_5774" s="T389">que-pro:case</ta>
            <ta e="T391" id="Seg_5775" s="T390">adv</ta>
            <ta e="T392" id="Seg_5776" s="T391">v-v:tense-v:pred.pn</ta>
            <ta e="T393" id="Seg_5777" s="T392">v-v:cvb</ta>
            <ta e="T394" id="Seg_5778" s="T393">v-v:tense-v:poss.pn</ta>
            <ta e="T395" id="Seg_5779" s="T394">n-n:(poss)-n:case</ta>
            <ta e="T396" id="Seg_5780" s="T395">n-n:poss-n:case</ta>
            <ta e="T397" id="Seg_5781" s="T396">n-n:poss-n:case</ta>
            <ta e="T398" id="Seg_5782" s="T397">adv</ta>
            <ta e="T399" id="Seg_5783" s="T398">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T400" id="Seg_5784" s="T399">dempro</ta>
            <ta e="T401" id="Seg_5785" s="T400">n-n:case</ta>
            <ta e="T402" id="Seg_5786" s="T401">adv</ta>
            <ta e="T403" id="Seg_5787" s="T402">n-n:poss-n:case</ta>
            <ta e="T404" id="Seg_5788" s="T403">v-v:tense-v:pred.pn</ta>
            <ta e="T405" id="Seg_5789" s="T404">dempro-pro:case</ta>
            <ta e="T406" id="Seg_5790" s="T405">post</ta>
            <ta e="T407" id="Seg_5791" s="T406">propr-n:case</ta>
            <ta e="T408" id="Seg_5792" s="T407">n-n:case</ta>
            <ta e="T409" id="Seg_5793" s="T408">n-n:poss-n:case</ta>
            <ta e="T410" id="Seg_5794" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_5795" s="T410">v-v:tense-v:pred.pn</ta>
            <ta e="T412" id="Seg_5796" s="T411">dempro</ta>
            <ta e="T413" id="Seg_5797" s="T412">v-v:cvb</ta>
            <ta e="T414" id="Seg_5798" s="T413">adj</ta>
            <ta e="T415" id="Seg_5799" s="T414">n-n:case</ta>
            <ta e="T416" id="Seg_5800" s="T415">v-v:cvb</ta>
            <ta e="T417" id="Seg_5801" s="T416">v-v:mood-v:temp.pn</ta>
            <ta e="T418" id="Seg_5802" s="T417">v-v:tense-v:pred.pn</ta>
            <ta e="T419" id="Seg_5803" s="T418">v-v:cvb</ta>
            <ta e="T420" id="Seg_5804" s="T419">v-v:ptcp</ta>
            <ta e="T421" id="Seg_5805" s="T420">n-n:case</ta>
            <ta e="T422" id="Seg_5806" s="T421">n-n:poss-n:case</ta>
            <ta e="T423" id="Seg_5807" s="T422">adj</ta>
            <ta e="T424" id="Seg_5808" s="T423">n-n:poss-n:case</ta>
            <ta e="T425" id="Seg_5809" s="T424">v-v:cvb</ta>
            <ta e="T426" id="Seg_5810" s="T425">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T427" id="Seg_5811" s="T426">propr-n:case</ta>
            <ta e="T428" id="Seg_5812" s="T427">n-n:poss-n:case</ta>
            <ta e="T429" id="Seg_5813" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_5814" s="T429">dempro</ta>
            <ta e="T431" id="Seg_5815" s="T430">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T432" id="Seg_5816" s="T431">adj-n:poss-n:case</ta>
            <ta e="T433" id="Seg_5817" s="T432">v-v:cvb</ta>
            <ta e="T434" id="Seg_5818" s="T433">v-v:tense-v:pred.pn</ta>
            <ta e="T435" id="Seg_5819" s="T434">pers-pro:case</ta>
            <ta e="T436" id="Seg_5820" s="T435">pers-pro:case</ta>
            <ta e="T437" id="Seg_5821" s="T436">n-n:poss-n:case</ta>
            <ta e="T438" id="Seg_5822" s="T437">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T439" id="Seg_5823" s="T438">v-v:tense-v:pred.pn</ta>
            <ta e="T440" id="Seg_5824" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_5825" s="T440">pers-pro:case</ta>
            <ta e="T442" id="Seg_5826" s="T441">n-n:poss-n:case</ta>
            <ta e="T443" id="Seg_5827" s="T442">v-v:mood.pn</ta>
            <ta e="T444" id="Seg_5828" s="T443">adv</ta>
            <ta e="T445" id="Seg_5829" s="T444">conj</ta>
            <ta e="T446" id="Seg_5830" s="T445">que-pro:case</ta>
            <ta e="T447" id="Seg_5831" s="T446">n-n:(poss)-n:case</ta>
            <ta e="T448" id="Seg_5832" s="T447">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T449" id="Seg_5833" s="T448">v-v:tense-v:pred.pn</ta>
            <ta e="T450" id="Seg_5834" s="T449">dempro</ta>
            <ta e="T451" id="Seg_5835" s="T450">n-n:(poss)-n:case</ta>
            <ta e="T452" id="Seg_5836" s="T451">dempro</ta>
            <ta e="T453" id="Seg_5837" s="T452">n-n:case</ta>
            <ta e="T454" id="Seg_5838" s="T453">n-n:poss-n:case</ta>
            <ta e="T455" id="Seg_5839" s="T454">v-v:cvb</ta>
            <ta e="T456" id="Seg_5840" s="T455">v-v:tense-v:pred.pn</ta>
            <ta e="T457" id="Seg_5841" s="T456">dempro</ta>
            <ta e="T458" id="Seg_5842" s="T457">n-n:(poss)-n:case</ta>
            <ta e="T459" id="Seg_5843" s="T458">dempro-pro:case</ta>
            <ta e="T460" id="Seg_5844" s="T459">v-v:tense-v:pred.pn</ta>
            <ta e="T461" id="Seg_5845" s="T460">conj</ta>
            <ta e="T462" id="Seg_5846" s="T461">propr-n:case</ta>
            <ta e="T463" id="Seg_5847" s="T462">v-v:cvb</ta>
            <ta e="T464" id="Seg_5848" s="T463">v-v:tense-v:pred.pn</ta>
            <ta e="T465" id="Seg_5849" s="T464">n-n:poss-n:case-n-n:poss-n:case</ta>
            <ta e="T466" id="Seg_5850" s="T465">n-n:poss-n:case</ta>
            <ta e="T467" id="Seg_5851" s="T466">v-v:cvb</ta>
            <ta e="T468" id="Seg_5852" s="T467">v-v:tense-v:pred.pn</ta>
            <ta e="T469" id="Seg_5853" s="T468">dempro</ta>
            <ta e="T470" id="Seg_5854" s="T469">n-n:poss-n:case</ta>
            <ta e="T471" id="Seg_5855" s="T470">n-n:poss-n:case</ta>
            <ta e="T472" id="Seg_5856" s="T471">v-v:cvb</ta>
            <ta e="T473" id="Seg_5857" s="T472">n-n:poss-n:case</ta>
            <ta e="T474" id="Seg_5858" s="T473">post</ta>
            <ta e="T475" id="Seg_5859" s="T474">v-v:cvb-v-v:cvb</ta>
            <ta e="T476" id="Seg_5860" s="T475">ptcl</ta>
            <ta e="T477" id="Seg_5861" s="T476">v-v:tense-v:poss.pn</ta>
            <ta e="T478" id="Seg_5862" s="T477">adj-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_5863" s="T0">propr</ta>
            <ta e="T2" id="Seg_5864" s="T1">ptcl</ta>
            <ta e="T3" id="Seg_5865" s="T2">v</ta>
            <ta e="T4" id="Seg_5866" s="T3">propr</ta>
            <ta e="T5" id="Seg_5867" s="T4">que</ta>
            <ta e="T6" id="Seg_5868" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_5869" s="T6">post</ta>
            <ta e="T8" id="Seg_5870" s="T7">v</ta>
            <ta e="T9" id="Seg_5871" s="T8">dempro</ta>
            <ta e="T10" id="Seg_5872" s="T9">v</ta>
            <ta e="T11" id="Seg_5873" s="T10">adj</ta>
            <ta e="T12" id="Seg_5874" s="T11">n</ta>
            <ta e="T13" id="Seg_5875" s="T12">v</ta>
            <ta e="T14" id="Seg_5876" s="T13">adj</ta>
            <ta e="T15" id="Seg_5877" s="T14">dempro</ta>
            <ta e="T16" id="Seg_5878" s="T15">n</ta>
            <ta e="T17" id="Seg_5879" s="T16">adj</ta>
            <ta e="T18" id="Seg_5880" s="T17">n</ta>
            <ta e="T19" id="Seg_5881" s="T18">adv</ta>
            <ta e="T20" id="Seg_5882" s="T19">v</ta>
            <ta e="T21" id="Seg_5883" s="T20">adv</ta>
            <ta e="T22" id="Seg_5884" s="T21">n</ta>
            <ta e="T23" id="Seg_5885" s="T22">cop</ta>
            <ta e="T24" id="Seg_5886" s="T23">adv</ta>
            <ta e="T25" id="Seg_5887" s="T24">cardnum</ta>
            <ta e="T26" id="Seg_5888" s="T25">n</ta>
            <ta e="T27" id="Seg_5889" s="T26">v</ta>
            <ta e="T28" id="Seg_5890" s="T27">v</ta>
            <ta e="T29" id="Seg_5891" s="T28">n</ta>
            <ta e="T30" id="Seg_5892" s="T29">v</ta>
            <ta e="T31" id="Seg_5893" s="T30">aux</ta>
            <ta e="T32" id="Seg_5894" s="T31">n</ta>
            <ta e="T33" id="Seg_5895" s="T32">adv</ta>
            <ta e="T34" id="Seg_5896" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_5897" s="T34">v</ta>
            <ta e="T36" id="Seg_5898" s="T35">dempro</ta>
            <ta e="T37" id="Seg_5899" s="T36">n</ta>
            <ta e="T38" id="Seg_5900" s="T37">pers</ta>
            <ta e="T39" id="Seg_5901" s="T38">adv</ta>
            <ta e="T40" id="Seg_5902" s="T39">v</ta>
            <ta e="T41" id="Seg_5903" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_5904" s="T41">que</ta>
            <ta e="T43" id="Seg_5905" s="T42">v</ta>
            <ta e="T44" id="Seg_5906" s="T43">n</ta>
            <ta e="T45" id="Seg_5907" s="T44">que</ta>
            <ta e="T46" id="Seg_5908" s="T45">v</ta>
            <ta e="T47" id="Seg_5909" s="T46">v</ta>
            <ta e="T48" id="Seg_5910" s="T47">v</ta>
            <ta e="T49" id="Seg_5911" s="T48">v</ta>
            <ta e="T50" id="Seg_5912" s="T49">propr</ta>
            <ta e="T51" id="Seg_5913" s="T50">v</ta>
            <ta e="T52" id="Seg_5914" s="T51">dempro</ta>
            <ta e="T53" id="Seg_5915" s="T52">n</ta>
            <ta e="T54" id="Seg_5916" s="T53">adj</ta>
            <ta e="T55" id="Seg_5917" s="T54">adj</ta>
            <ta e="T56" id="Seg_5918" s="T55">n</ta>
            <ta e="T57" id="Seg_5919" s="T56">v</ta>
            <ta e="T58" id="Seg_5920" s="T57">aux</ta>
            <ta e="T59" id="Seg_5921" s="T58">n</ta>
            <ta e="T60" id="Seg_5922" s="T59">n</ta>
            <ta e="T61" id="Seg_5923" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_5924" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_5925" s="T62">n</ta>
            <ta e="T64" id="Seg_5926" s="T63">n</ta>
            <ta e="T65" id="Seg_5927" s="T64">adj</ta>
            <ta e="T66" id="Seg_5928" s="T65">n</ta>
            <ta e="T67" id="Seg_5929" s="T66">post</ta>
            <ta e="T68" id="Seg_5930" s="T67">n</ta>
            <ta e="T69" id="Seg_5931" s="T68">v</ta>
            <ta e="T70" id="Seg_5932" s="T69">aux</ta>
            <ta e="T71" id="Seg_5933" s="T70">n</ta>
            <ta e="T72" id="Seg_5934" s="T71">que</ta>
            <ta e="T73" id="Seg_5935" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_5936" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_5937" s="T74">n</ta>
            <ta e="T76" id="Seg_5938" s="T75">n</ta>
            <ta e="T77" id="Seg_5939" s="T76">v</ta>
            <ta e="T78" id="Seg_5940" s="T77">n</ta>
            <ta e="T79" id="Seg_5941" s="T78">n</ta>
            <ta e="T80" id="Seg_5942" s="T79">adv</ta>
            <ta e="T81" id="Seg_5943" s="T80">cop</ta>
            <ta e="T82" id="Seg_5944" s="T81">v</ta>
            <ta e="T83" id="Seg_5945" s="T82">aux</ta>
            <ta e="T84" id="Seg_5946" s="T83">n</ta>
            <ta e="T85" id="Seg_5947" s="T84">v</ta>
            <ta e="T86" id="Seg_5948" s="T85">v</ta>
            <ta e="T87" id="Seg_5949" s="T86">n</ta>
            <ta e="T88" id="Seg_5950" s="T87">n</ta>
            <ta e="T89" id="Seg_5951" s="T88">n</ta>
            <ta e="T90" id="Seg_5952" s="T89">v</ta>
            <ta e="T91" id="Seg_5953" s="T90">n</ta>
            <ta e="T92" id="Seg_5954" s="T91">v</ta>
            <ta e="T93" id="Seg_5955" s="T92">v</ta>
            <ta e="T94" id="Seg_5956" s="T93">interj</ta>
            <ta e="T95" id="Seg_5957" s="T94">adv</ta>
            <ta e="T96" id="Seg_5958" s="T95">n</ta>
            <ta e="T97" id="Seg_5959" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_5960" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_5961" s="T98">v</ta>
            <ta e="T100" id="Seg_5962" s="T99">n</ta>
            <ta e="T101" id="Seg_5963" s="T100">interj</ta>
            <ta e="T102" id="Seg_5964" s="T101">adj</ta>
            <ta e="T103" id="Seg_5965" s="T102">cop</ta>
            <ta e="T104" id="Seg_5966" s="T103">aux</ta>
            <ta e="T105" id="Seg_5967" s="T104">que</ta>
            <ta e="T106" id="Seg_5968" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_5969" s="T106">v</ta>
            <ta e="T108" id="Seg_5970" s="T107">aux</ta>
            <ta e="T109" id="Seg_5971" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_5972" s="T109">v</ta>
            <ta e="T111" id="Seg_5973" s="T110">n</ta>
            <ta e="T112" id="Seg_5974" s="T111">n</ta>
            <ta e="T113" id="Seg_5975" s="T112">n</ta>
            <ta e="T114" id="Seg_5976" s="T113">n</ta>
            <ta e="T115" id="Seg_5977" s="T114">n</ta>
            <ta e="T116" id="Seg_5978" s="T115">n</ta>
            <ta e="T117" id="Seg_5979" s="T116">adv</ta>
            <ta e="T118" id="Seg_5980" s="T117">v</ta>
            <ta e="T119" id="Seg_5981" s="T118">n</ta>
            <ta e="T120" id="Seg_5982" s="T119">n</ta>
            <ta e="T121" id="Seg_5983" s="T120">v</ta>
            <ta e="T122" id="Seg_5984" s="T121">n</ta>
            <ta e="T123" id="Seg_5985" s="T122">v</ta>
            <ta e="T124" id="Seg_5986" s="T123">n</ta>
            <ta e="T125" id="Seg_5987" s="T124">dempro</ta>
            <ta e="T126" id="Seg_5988" s="T125">n</ta>
            <ta e="T127" id="Seg_5989" s="T126">v</ta>
            <ta e="T128" id="Seg_5990" s="T127">dempro</ta>
            <ta e="T129" id="Seg_5991" s="T128">n</ta>
            <ta e="T130" id="Seg_5992" s="T129">v</ta>
            <ta e="T131" id="Seg_5993" s="T130">dempro</ta>
            <ta e="T132" id="Seg_5994" s="T131">v</ta>
            <ta e="T133" id="Seg_5995" s="T132">n</ta>
            <ta e="T134" id="Seg_5996" s="T133">cop</ta>
            <ta e="T135" id="Seg_5997" s="T134">v</ta>
            <ta e="T136" id="Seg_5998" s="T135">dempro</ta>
            <ta e="T137" id="Seg_5999" s="T136">v</ta>
            <ta e="T138" id="Seg_6000" s="T137">n</ta>
            <ta e="T139" id="Seg_6001" s="T138">adv</ta>
            <ta e="T140" id="Seg_6002" s="T139">v</ta>
            <ta e="T141" id="Seg_6003" s="T140">pers</ta>
            <ta e="T142" id="Seg_6004" s="T141">adj</ta>
            <ta e="T143" id="Seg_6005" s="T142">cop</ta>
            <ta e="T144" id="Seg_6006" s="T143">n</ta>
            <ta e="T145" id="Seg_6007" s="T144">adv</ta>
            <ta e="T146" id="Seg_6008" s="T145">v</ta>
            <ta e="T147" id="Seg_6009" s="T146">aux</ta>
            <ta e="T148" id="Seg_6010" s="T147">v</ta>
            <ta e="T149" id="Seg_6011" s="T148">aux</ta>
            <ta e="T150" id="Seg_6012" s="T149">propr</ta>
            <ta e="T151" id="Seg_6013" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_6014" s="T151">n</ta>
            <ta e="T153" id="Seg_6015" s="T152">v</ta>
            <ta e="T154" id="Seg_6016" s="T153">v</ta>
            <ta e="T155" id="Seg_6017" s="T154">cardnum</ta>
            <ta e="T156" id="Seg_6018" s="T155">adj</ta>
            <ta e="T157" id="Seg_6019" s="T156">n</ta>
            <ta e="T158" id="Seg_6020" s="T157">v</ta>
            <ta e="T159" id="Seg_6021" s="T158">adv</ta>
            <ta e="T160" id="Seg_6022" s="T159">v</ta>
            <ta e="T161" id="Seg_6023" s="T160">adv</ta>
            <ta e="T162" id="Seg_6024" s="T161">adj</ta>
            <ta e="T163" id="Seg_6025" s="T162">n</ta>
            <ta e="T164" id="Seg_6026" s="T163">n</ta>
            <ta e="T165" id="Seg_6027" s="T164">v</ta>
            <ta e="T166" id="Seg_6028" s="T165">dempro</ta>
            <ta e="T167" id="Seg_6029" s="T166">n</ta>
            <ta e="T168" id="Seg_6030" s="T167">n</ta>
            <ta e="T169" id="Seg_6031" s="T168">n</ta>
            <ta e="T170" id="Seg_6032" s="T169">n</ta>
            <ta e="T171" id="Seg_6033" s="T170">v</ta>
            <ta e="T172" id="Seg_6034" s="T171">aux</ta>
            <ta e="T173" id="Seg_6035" s="T172">propr</ta>
            <ta e="T174" id="Seg_6036" s="T173">v</ta>
            <ta e="T175" id="Seg_6037" s="T174">dempro</ta>
            <ta e="T176" id="Seg_6038" s="T175">que</ta>
            <ta e="T177" id="Seg_6039" s="T176">n</ta>
            <ta e="T178" id="Seg_6040" s="T177">que</ta>
            <ta e="T179" id="Seg_6041" s="T178">adj</ta>
            <ta e="T180" id="Seg_6042" s="T179">n</ta>
            <ta e="T181" id="Seg_6043" s="T180">adj</ta>
            <ta e="T182" id="Seg_6044" s="T181">v</ta>
            <ta e="T183" id="Seg_6045" s="T182">interj</ta>
            <ta e="T184" id="Seg_6046" s="T183">dempro</ta>
            <ta e="T185" id="Seg_6047" s="T184">n</ta>
            <ta e="T186" id="Seg_6048" s="T185">que</ta>
            <ta e="T187" id="Seg_6049" s="T186">v</ta>
            <ta e="T188" id="Seg_6050" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_6051" s="T188">dempro</ta>
            <ta e="T190" id="Seg_6052" s="T189">n</ta>
            <ta e="T191" id="Seg_6053" s="T190">n</ta>
            <ta e="T192" id="Seg_6054" s="T191">v</ta>
            <ta e="T193" id="Seg_6055" s="T192">dempro</ta>
            <ta e="T194" id="Seg_6056" s="T193">post</ta>
            <ta e="T195" id="Seg_6057" s="T194">v</ta>
            <ta e="T196" id="Seg_6058" s="T195">v</ta>
            <ta e="T197" id="Seg_6059" s="T196">n</ta>
            <ta e="T198" id="Seg_6060" s="T197">adv</ta>
            <ta e="T199" id="Seg_6061" s="T198">n</ta>
            <ta e="T200" id="Seg_6062" s="T199">v</ta>
            <ta e="T201" id="Seg_6063" s="T200">adj</ta>
            <ta e="T202" id="Seg_6064" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_6065" s="T202">n</ta>
            <ta e="T205" id="Seg_6066" s="T204">dempro</ta>
            <ta e="T206" id="Seg_6067" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_6068" s="T206">v</ta>
            <ta e="T208" id="Seg_6069" s="T207">dempro</ta>
            <ta e="T209" id="Seg_6070" s="T208">n</ta>
            <ta e="T210" id="Seg_6071" s="T209">n</ta>
            <ta e="T211" id="Seg_6072" s="T210">dempro</ta>
            <ta e="T212" id="Seg_6073" s="T211">dempro</ta>
            <ta e="T213" id="Seg_6074" s="T212">v</ta>
            <ta e="T214" id="Seg_6075" s="T213">n</ta>
            <ta e="T215" id="Seg_6076" s="T214">v</ta>
            <ta e="T216" id="Seg_6077" s="T215">v</ta>
            <ta e="T217" id="Seg_6078" s="T216">dempro</ta>
            <ta e="T218" id="Seg_6079" s="T217">n</ta>
            <ta e="T219" id="Seg_6080" s="T218">post</ta>
            <ta e="T220" id="Seg_6081" s="T219">v</ta>
            <ta e="T221" id="Seg_6082" s="T220">adv</ta>
            <ta e="T222" id="Seg_6083" s="T221">v</ta>
            <ta e="T223" id="Seg_6084" s="T222">v</ta>
            <ta e="T224" id="Seg_6085" s="T223">dempro</ta>
            <ta e="T225" id="Seg_6086" s="T224">post</ta>
            <ta e="T226" id="Seg_6087" s="T225">v</ta>
            <ta e="T227" id="Seg_6088" s="T226">v</ta>
            <ta e="T228" id="Seg_6089" s="T227">propr</ta>
            <ta e="T229" id="Seg_6090" s="T228">v</ta>
            <ta e="T230" id="Seg_6091" s="T229">adv</ta>
            <ta e="T231" id="Seg_6092" s="T230">pers</ta>
            <ta e="T232" id="Seg_6093" s="T231">v</ta>
            <ta e="T233" id="Seg_6094" s="T232">v</ta>
            <ta e="T234" id="Seg_6095" s="T233">adv</ta>
            <ta e="T235" id="Seg_6096" s="T234">n</ta>
            <ta e="T236" id="Seg_6097" s="T235">v</ta>
            <ta e="T237" id="Seg_6098" s="T236">dempro</ta>
            <ta e="T238" id="Seg_6099" s="T237">n</ta>
            <ta e="T239" id="Seg_6100" s="T238">n</ta>
            <ta e="T240" id="Seg_6101" s="T239">v</ta>
            <ta e="T241" id="Seg_6102" s="T240">n</ta>
            <ta e="T242" id="Seg_6103" s="T241">adj</ta>
            <ta e="T243" id="Seg_6104" s="T242">v</ta>
            <ta e="T244" id="Seg_6105" s="T243">aux</ta>
            <ta e="T245" id="Seg_6106" s="T244">v</ta>
            <ta e="T246" id="Seg_6107" s="T245">v</ta>
            <ta e="T247" id="Seg_6108" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_6109" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_6110" s="T248">dempro</ta>
            <ta e="T250" id="Seg_6111" s="T249">n</ta>
            <ta e="T251" id="Seg_6112" s="T250">propr</ta>
            <ta e="T252" id="Seg_6113" s="T251">v</ta>
            <ta e="T253" id="Seg_6114" s="T252">propr</ta>
            <ta e="T254" id="Seg_6115" s="T253">n</ta>
            <ta e="T255" id="Seg_6116" s="T254">dempro</ta>
            <ta e="T256" id="Seg_6117" s="T255">n</ta>
            <ta e="T257" id="Seg_6118" s="T256">v</ta>
            <ta e="T258" id="Seg_6119" s="T257">v</ta>
            <ta e="T259" id="Seg_6120" s="T258">v</ta>
            <ta e="T260" id="Seg_6121" s="T259">v</ta>
            <ta e="T261" id="Seg_6122" s="T260">v</ta>
            <ta e="T262" id="Seg_6123" s="T261">n</ta>
            <ta e="T263" id="Seg_6124" s="T262">v</ta>
            <ta e="T264" id="Seg_6125" s="T263">aux</ta>
            <ta e="T265" id="Seg_6126" s="T264">v</ta>
            <ta e="T266" id="Seg_6127" s="T265">v</ta>
            <ta e="T267" id="Seg_6128" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_6129" s="T267">propr</ta>
            <ta e="T269" id="Seg_6130" s="T268">v</ta>
            <ta e="T270" id="Seg_6131" s="T269">n</ta>
            <ta e="T271" id="Seg_6132" s="T270">v</ta>
            <ta e="T272" id="Seg_6133" s="T271">dempro</ta>
            <ta e="T273" id="Seg_6134" s="T272">que</ta>
            <ta e="T274" id="Seg_6135" s="T273">cop</ta>
            <ta e="T275" id="Seg_6136" s="T274">v</ta>
            <ta e="T276" id="Seg_6137" s="T275">n</ta>
            <ta e="T277" id="Seg_6138" s="T276">adv</ta>
            <ta e="T278" id="Seg_6139" s="T277">v</ta>
            <ta e="T279" id="Seg_6140" s="T278">n</ta>
            <ta e="T280" id="Seg_6141" s="T279">pers</ta>
            <ta e="T281" id="Seg_6142" s="T280">pers</ta>
            <ta e="T282" id="Seg_6143" s="T281">v</ta>
            <ta e="T283" id="Seg_6144" s="T282">v</ta>
            <ta e="T284" id="Seg_6145" s="T283">v</ta>
            <ta e="T285" id="Seg_6146" s="T284">pers</ta>
            <ta e="T286" id="Seg_6147" s="T285">dempro</ta>
            <ta e="T287" id="Seg_6148" s="T286">adj</ta>
            <ta e="T288" id="Seg_6149" s="T287">adj</ta>
            <ta e="T289" id="Seg_6150" s="T288">dempro</ta>
            <ta e="T290" id="Seg_6151" s="T289">n</ta>
            <ta e="T291" id="Seg_6152" s="T290">adv</ta>
            <ta e="T292" id="Seg_6153" s="T291">v</ta>
            <ta e="T293" id="Seg_6154" s="T292">aux</ta>
            <ta e="T294" id="Seg_6155" s="T293">v</ta>
            <ta e="T295" id="Seg_6156" s="T294">interj</ta>
            <ta e="T296" id="Seg_6157" s="T295">dempro</ta>
            <ta e="T297" id="Seg_6158" s="T296">post</ta>
            <ta e="T298" id="Seg_6159" s="T297">v</ta>
            <ta e="T299" id="Seg_6160" s="T298">pers</ta>
            <ta e="T300" id="Seg_6161" s="T299">n</ta>
            <ta e="T301" id="Seg_6162" s="T300">n</ta>
            <ta e="T302" id="Seg_6163" s="T301">v</ta>
            <ta e="T303" id="Seg_6164" s="T302">v</ta>
            <ta e="T304" id="Seg_6165" s="T303">n</ta>
            <ta e="T305" id="Seg_6166" s="T304">propr</ta>
            <ta e="T306" id="Seg_6167" s="T305">n</ta>
            <ta e="T307" id="Seg_6168" s="T306">n</ta>
            <ta e="T308" id="Seg_6169" s="T307">v</ta>
            <ta e="T309" id="Seg_6170" s="T308">aux</ta>
            <ta e="T310" id="Seg_6171" s="T309">dempro</ta>
            <ta e="T311" id="Seg_6172" s="T310">n</ta>
            <ta e="T312" id="Seg_6173" s="T311">v</ta>
            <ta e="T313" id="Seg_6174" s="T312">post</ta>
            <ta e="T314" id="Seg_6175" s="T313">n</ta>
            <ta e="T315" id="Seg_6176" s="T314">v</ta>
            <ta e="T316" id="Seg_6177" s="T315">n</ta>
            <ta e="T317" id="Seg_6178" s="T316">adv</ta>
            <ta e="T318" id="Seg_6179" s="T317">v</ta>
            <ta e="T319" id="Seg_6180" s="T318">aux</ta>
            <ta e="T320" id="Seg_6181" s="T319">n</ta>
            <ta e="T321" id="Seg_6182" s="T320">v</ta>
            <ta e="T322" id="Seg_6183" s="T321">n</ta>
            <ta e="T323" id="Seg_6184" s="T322">v</ta>
            <ta e="T324" id="Seg_6185" s="T323">post</ta>
            <ta e="T325" id="Seg_6186" s="T324">adv</ta>
            <ta e="T326" id="Seg_6187" s="T325">v</ta>
            <ta e="T327" id="Seg_6188" s="T326">aux</ta>
            <ta e="T328" id="Seg_6189" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_6190" s="T328">dempro</ta>
            <ta e="T330" id="Seg_6191" s="T329">v</ta>
            <ta e="T331" id="Seg_6192" s="T330">cardnum</ta>
            <ta e="T332" id="Seg_6193" s="T331">n</ta>
            <ta e="T333" id="Seg_6194" s="T332">v</ta>
            <ta e="T334" id="Seg_6195" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_6196" s="T334">cardnum</ta>
            <ta e="T336" id="Seg_6197" s="T335">adj</ta>
            <ta e="T337" id="Seg_6198" s="T336">n</ta>
            <ta e="T338" id="Seg_6199" s="T337">n</ta>
            <ta e="T339" id="Seg_6200" s="T338">v</ta>
            <ta e="T340" id="Seg_6201" s="T339">aux</ta>
            <ta e="T341" id="Seg_6202" s="T340">dempro</ta>
            <ta e="T342" id="Seg_6203" s="T341">n</ta>
            <ta e="T343" id="Seg_6204" s="T342">v</ta>
            <ta e="T344" id="Seg_6205" s="T343">adv</ta>
            <ta e="T345" id="Seg_6206" s="T344">interj</ta>
            <ta e="T346" id="Seg_6207" s="T345">propr</ta>
            <ta e="T347" id="Seg_6208" s="T346">n</ta>
            <ta e="T348" id="Seg_6209" s="T347">adj</ta>
            <ta e="T349" id="Seg_6210" s="T348">adj</ta>
            <ta e="T350" id="Seg_6211" s="T349">n</ta>
            <ta e="T351" id="Seg_6212" s="T350">v</ta>
            <ta e="T352" id="Seg_6213" s="T351">ptcl</ta>
            <ta e="T353" id="Seg_6214" s="T352">dempro</ta>
            <ta e="T354" id="Seg_6215" s="T353">n</ta>
            <ta e="T355" id="Seg_6216" s="T354">n</ta>
            <ta e="T356" id="Seg_6217" s="T355">v</ta>
            <ta e="T357" id="Seg_6218" s="T356">n</ta>
            <ta e="T358" id="Seg_6219" s="T357">adv</ta>
            <ta e="T359" id="Seg_6220" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_6221" s="T359">adj</ta>
            <ta e="T361" id="Seg_6222" s="T360">v</ta>
            <ta e="T362" id="Seg_6223" s="T361">dempro</ta>
            <ta e="T363" id="Seg_6224" s="T362">n</ta>
            <ta e="T364" id="Seg_6225" s="T363">ptcl</ta>
            <ta e="T365" id="Seg_6226" s="T364">v</ta>
            <ta e="T366" id="Seg_6227" s="T365">ptcl</ta>
            <ta e="T367" id="Seg_6228" s="T366">dempro</ta>
            <ta e="T368" id="Seg_6229" s="T367">n</ta>
            <ta e="T369" id="Seg_6230" s="T368">n</ta>
            <ta e="T370" id="Seg_6231" s="T369">v</ta>
            <ta e="T371" id="Seg_6232" s="T370">v</ta>
            <ta e="T372" id="Seg_6233" s="T371">adv</ta>
            <ta e="T373" id="Seg_6234" s="T372">n</ta>
            <ta e="T374" id="Seg_6235" s="T373">v</ta>
            <ta e="T375" id="Seg_6236" s="T374">v</ta>
            <ta e="T376" id="Seg_6237" s="T375">n</ta>
            <ta e="T377" id="Seg_6238" s="T376">n</ta>
            <ta e="T378" id="Seg_6239" s="T377">post</ta>
            <ta e="T379" id="Seg_6240" s="T378">n</ta>
            <ta e="T380" id="Seg_6241" s="T379">n</ta>
            <ta e="T381" id="Seg_6242" s="T380">n</ta>
            <ta e="T382" id="Seg_6243" s="T381">v</ta>
            <ta e="T383" id="Seg_6244" s="T382">aux</ta>
            <ta e="T384" id="Seg_6245" s="T383">dempro</ta>
            <ta e="T385" id="Seg_6246" s="T384">v</ta>
            <ta e="T386" id="Seg_6247" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_6248" s="T386">n</ta>
            <ta e="T388" id="Seg_6249" s="T387">v</ta>
            <ta e="T389" id="Seg_6250" s="T388">dempro</ta>
            <ta e="T390" id="Seg_6251" s="T389">que</ta>
            <ta e="T391" id="Seg_6252" s="T390">adv</ta>
            <ta e="T392" id="Seg_6253" s="T391">v</ta>
            <ta e="T393" id="Seg_6254" s="T392">v</ta>
            <ta e="T394" id="Seg_6255" s="T393">aux</ta>
            <ta e="T395" id="Seg_6256" s="T394">n</ta>
            <ta e="T396" id="Seg_6257" s="T395">n</ta>
            <ta e="T397" id="Seg_6258" s="T396">n</ta>
            <ta e="T398" id="Seg_6259" s="T397">adv</ta>
            <ta e="T399" id="Seg_6260" s="T398">v</ta>
            <ta e="T400" id="Seg_6261" s="T399">dempro</ta>
            <ta e="T401" id="Seg_6262" s="T400">n</ta>
            <ta e="T402" id="Seg_6263" s="T401">adv</ta>
            <ta e="T403" id="Seg_6264" s="T402">n</ta>
            <ta e="T404" id="Seg_6265" s="T403">v</ta>
            <ta e="T405" id="Seg_6266" s="T404">dempro</ta>
            <ta e="T406" id="Seg_6267" s="T405">post</ta>
            <ta e="T407" id="Seg_6268" s="T406">propr</ta>
            <ta e="T408" id="Seg_6269" s="T407">n</ta>
            <ta e="T409" id="Seg_6270" s="T408">n</ta>
            <ta e="T410" id="Seg_6271" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_6272" s="T410">v</ta>
            <ta e="T412" id="Seg_6273" s="T411">dempro</ta>
            <ta e="T413" id="Seg_6274" s="T412">v</ta>
            <ta e="T414" id="Seg_6275" s="T413">adj</ta>
            <ta e="T415" id="Seg_6276" s="T414">n</ta>
            <ta e="T416" id="Seg_6277" s="T415">v</ta>
            <ta e="T417" id="Seg_6278" s="T416">aux</ta>
            <ta e="T418" id="Seg_6279" s="T417">v</ta>
            <ta e="T419" id="Seg_6280" s="T418">v</ta>
            <ta e="T420" id="Seg_6281" s="T419">aux</ta>
            <ta e="T421" id="Seg_6282" s="T420">n</ta>
            <ta e="T422" id="Seg_6283" s="T421">n</ta>
            <ta e="T423" id="Seg_6284" s="T422">adj</ta>
            <ta e="T424" id="Seg_6285" s="T423">n</ta>
            <ta e="T425" id="Seg_6286" s="T424">v</ta>
            <ta e="T426" id="Seg_6287" s="T425">v</ta>
            <ta e="T427" id="Seg_6288" s="T426">propr</ta>
            <ta e="T428" id="Seg_6289" s="T427">n</ta>
            <ta e="T429" id="Seg_6290" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_6291" s="T429">dempro</ta>
            <ta e="T431" id="Seg_6292" s="T430">n</ta>
            <ta e="T432" id="Seg_6293" s="T431">n</ta>
            <ta e="T433" id="Seg_6294" s="T432">v</ta>
            <ta e="T434" id="Seg_6295" s="T433">aux</ta>
            <ta e="T435" id="Seg_6296" s="T434">pers</ta>
            <ta e="T436" id="Seg_6297" s="T435">pers</ta>
            <ta e="T437" id="Seg_6298" s="T436">n</ta>
            <ta e="T438" id="Seg_6299" s="T437">v</ta>
            <ta e="T439" id="Seg_6300" s="T438">v</ta>
            <ta e="T440" id="Seg_6301" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_6302" s="T440">pers</ta>
            <ta e="T442" id="Seg_6303" s="T441">n</ta>
            <ta e="T443" id="Seg_6304" s="T442">v</ta>
            <ta e="T444" id="Seg_6305" s="T443">adv</ta>
            <ta e="T445" id="Seg_6306" s="T444">conj</ta>
            <ta e="T446" id="Seg_6307" s="T445">que</ta>
            <ta e="T447" id="Seg_6308" s="T446">n</ta>
            <ta e="T448" id="Seg_6309" s="T447">cop</ta>
            <ta e="T449" id="Seg_6310" s="T448">v</ta>
            <ta e="T450" id="Seg_6311" s="T449">dempro</ta>
            <ta e="T451" id="Seg_6312" s="T450">n</ta>
            <ta e="T452" id="Seg_6313" s="T451">dempro</ta>
            <ta e="T453" id="Seg_6314" s="T452">n</ta>
            <ta e="T454" id="Seg_6315" s="T453">n</ta>
            <ta e="T455" id="Seg_6316" s="T454">v</ta>
            <ta e="T456" id="Seg_6317" s="T455">aux</ta>
            <ta e="T457" id="Seg_6318" s="T456">dempro</ta>
            <ta e="T458" id="Seg_6319" s="T457">n</ta>
            <ta e="T459" id="Seg_6320" s="T458">dempro</ta>
            <ta e="T460" id="Seg_6321" s="T459">v</ta>
            <ta e="T461" id="Seg_6322" s="T460">conj</ta>
            <ta e="T462" id="Seg_6323" s="T461">propr</ta>
            <ta e="T463" id="Seg_6324" s="T462">v</ta>
            <ta e="T464" id="Seg_6325" s="T463">aux</ta>
            <ta e="T465" id="Seg_6326" s="T464">n</ta>
            <ta e="T466" id="Seg_6327" s="T465">n</ta>
            <ta e="T467" id="Seg_6328" s="T466">v</ta>
            <ta e="T468" id="Seg_6329" s="T467">aux</ta>
            <ta e="T469" id="Seg_6330" s="T468">dempro</ta>
            <ta e="T470" id="Seg_6331" s="T469">n</ta>
            <ta e="T471" id="Seg_6332" s="T470">n</ta>
            <ta e="T472" id="Seg_6333" s="T471">v</ta>
            <ta e="T473" id="Seg_6334" s="T472">n</ta>
            <ta e="T474" id="Seg_6335" s="T473">post</ta>
            <ta e="T475" id="Seg_6336" s="T474">v</ta>
            <ta e="T476" id="Seg_6337" s="T475">ptcl</ta>
            <ta e="T477" id="Seg_6338" s="T476">v</ta>
            <ta e="T478" id="Seg_6339" s="T477">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_6340" s="T0">There lives Lyypyrdaan.</ta>
            <ta e="T8" id="Seg_6341" s="T3">Lyypyrdaan goes somewhere.</ta>
            <ta e="T13" id="Seg_6342" s="T8">While going he meets a human with a wife.</ta>
            <ta e="T16" id="Seg_6343" s="T13">That human has got a son.</ta>
            <ta e="T21" id="Seg_6344" s="T16">The human and his wife were nomadizing alone.</ta>
            <ta e="T27" id="Seg_6345" s="T21">In the evening, as it got dark, they came to one place.</ta>
            <ta e="T35" id="Seg_6346" s="T27">As he began to take off his shoes to go to sleep, the fire crackled a couple of times.</ta>
            <ta e="T37" id="Seg_6347" s="T35">That human: </ta>
            <ta e="T43" id="Seg_6348" s="T37">"I went by day, what can it be?</ta>
            <ta e="T49" id="Seg_6349" s="T43">Why is my fire speaking?", he thinks, not being afraid, and lies down.</ta>
            <ta e="T62" id="Seg_6350" s="T49">Lyypyrdaan comes and kills that human, who has a silver plated bow, such a bow.</ta>
            <ta e="T70" id="Seg_6351" s="T62">The woman of that human escapes with her small child.</ta>
            <ta e="T77" id="Seg_6352" s="T70">This woman doesn't have anything, she lives in a small pit-dwelling.</ta>
            <ta e="T83" id="Seg_6353" s="T77">The child grows up, it doesn't last long in a tale.</ta>
            <ta e="T90" id="Seg_6354" s="T83">The child went out and saw white creatures running on the roof.</ta>
            <ta e="T93" id="Seg_6355" s="T90">It goes back to his mother and tells her:</ta>
            <ta e="T100" id="Seg_6356" s="T93">"Ah, that is probably partridges", his mother say.</ta>
            <ta e="T112" id="Seg_6357" s="T100">"Oh, if I just had a father, he could do something", the boy says to his mother.</ta>
            <ta e="T118" id="Seg_6358" s="T112">His mother doesn't tell anything about his father.</ta>
            <ta e="T127" id="Seg_6359" s="T118">His mother cuts off her hair and makes threads out of it, the boy makes catching loops out of them.</ta>
            <ta e="T135" id="Seg_6360" s="T127">So they kill partridges and live on them.</ta>
            <ta e="T140" id="Seg_6361" s="T135">The boy keeps asking: </ta>
            <ta e="T143" id="Seg_6362" s="T140">"Did I have a father?"</ta>
            <ta e="T149" id="Seg_6363" s="T143">His mother said nothing at all, apparently, she was hiding it.</ta>
            <ta e="T154" id="Seg_6364" s="T149">Lyypyrdaan goes again to kill people.</ta>
            <ta e="T158" id="Seg_6365" s="T154">He comes to one woman with her son.</ta>
            <ta e="T165" id="Seg_6366" s="T158">Suddenly he saw a very fat larch standing there.</ta>
            <ta e="T172" id="Seg_6367" s="T165">The old woman's son put his bow on this tree.</ta>
            <ta e="T174" id="Seg_6368" s="T172">Lyypyrdaan asks:</ta>
            <ta e="T177" id="Seg_6369" s="T174">"Whose bow is that?</ta>
            <ta e="T179" id="Seg_6370" s="T177">Who is your family?"</ta>
            <ta e="T180" id="Seg_6371" s="T179">The old woman:</ta>
            <ta e="T182" id="Seg_6372" s="T180">"I have a son", she says.</ta>
            <ta e="T188" id="Seg_6373" s="T182">"But why did he leave his bow there?"</ta>
            <ta e="T197" id="Seg_6374" s="T188">"The power of that bow went out, that's why he left it", the old woman says.</ta>
            <ta e="T204" id="Seg_6375" s="T197">There also lies a very huge stone.</ta>
            <ta e="T209" id="Seg_6376" s="T204">This human asks about it, too.</ta>
            <ta e="T211" id="Seg_6377" s="T209">The old woman on that:</ta>
            <ta e="T216" id="Seg_6378" s="T211">"When he is bored, he plays ball with it", she says.</ta>
            <ta e="T223" id="Seg_6379" s="T216">"Whe he kicks it, it rolls until that mountain and back.</ta>
            <ta e="T227" id="Seg_6380" s="T223">That's how he usually plays", she says.</ta>
            <ta e="T229" id="Seg_6381" s="T227">Lyypyrdaan says:</ta>
            <ta e="T233" id="Seg_6382" s="T229">"Tomorrow is my birthday.</ta>
            <ta e="T236" id="Seg_6383" s="T233">Your son should come."</ta>
            <ta e="T244" id="Seg_6384" s="T236">As her son comes home, his mother tells him everything:</ta>
            <ta e="T246" id="Seg_6385" s="T244">"They invited", she says.</ta>
            <ta e="T252" id="Seg_6386" s="T246">Well, this human went to Lyypyrdaan.</ta>
            <ta e="T254" id="Seg_6387" s="T252">Lyypyrdaan to his people:</ta>
            <ta e="T260" id="Seg_6388" s="T254">We will surround this human and kill him", he says.</ta>
            <ta e="T264" id="Seg_6389" s="T260">As the the guest came, they surround him.</ta>
            <ta e="T267" id="Seg_6390" s="T264">After all they had decided to kill him.</ta>
            <ta e="T271" id="Seg_6391" s="T267">Lyypyrdaan is scared and says to his friends:</ta>
            <ta e="T275" id="Seg_6392" s="T271">"Why are you gathering?"</ta>
            <ta e="T278" id="Seg_6393" s="T275">There the guest says:</ta>
            <ta e="T284" id="Seg_6394" s="T278">"Old man, I know that you want to kill me.</ta>
            <ta e="T294" id="Seg_6395" s="T284">I just have this small knife, but I could extinguish all your people with it", he says. </ta>
            <ta e="T298" id="Seg_6396" s="T294">"Oh, do not do that.</ta>
            <ta e="T303" id="Seg_6397" s="T298">Marry my daughter", he says.</ta>
            <ta e="T309" id="Seg_6398" s="T303">The guest marries Lyypyrdaan's daughter.</ta>
            <ta e="T319" id="Seg_6399" s="T309">As this boy had grown up, he kept searching for the human who had killed his father.</ta>
            <ta e="T327" id="Seg_6400" s="T319">As he had grown up up, he kept going to the place where his father died.</ta>
            <ta e="T333" id="Seg_6401" s="T327">Once, as he is lying there, two humans are passing by.</ta>
            <ta e="T340" id="Seg_6402" s="T333">One of them is carrying a frozen bear corpse on his breast.</ta>
            <ta e="T343" id="Seg_6403" s="T340">His friend says:</ta>
            <ta e="T351" id="Seg_6404" s="T343">"Well, here Lyypyrdaan killed a human with a silver plated bow.</ta>
            <ta e="T356" id="Seg_6405" s="T351">This human's son is still living", he says.</ta>
            <ta e="T361" id="Seg_6406" s="T356">There the boy hears that he had a father.</ta>
            <ta e="T368" id="Seg_6407" s="T361">This child shouts to these humans.</ta>
            <ta e="T375" id="Seg_6408" s="T368">They think, that the spirit of this place is frightening them, they run away, leaving the bear corpse there.</ta>
            <ta e="T383" id="Seg_6409" s="T375">The boy carries the bear with the beluga home to his mother.</ta>
            <ta e="T386" id="Seg_6410" s="T383">They eat them for sure.</ta>
            <ta e="T388" id="Seg_6411" s="T386">His mother is rebuking his son:</ta>
            <ta e="T392" id="Seg_6412" s="T388">"Why are you away for so long?</ta>
            <ta e="T394" id="Seg_6413" s="T392">They will kill you."</ta>
            <ta e="T399" id="Seg_6414" s="T394">The son doesn't listen to his mother at all.</ta>
            <ta e="T404" id="Seg_6415" s="T399">He keeps searching for his father.</ta>
            <ta e="T411" id="Seg_6416" s="T404">So he finds the house of the old man Lyypyrdaan.</ta>
            <ta e="T418" id="Seg_6417" s="T411">He finds it, as in the evening everybody falls asleep, he comes there.</ta>
            <ta e="T428" id="Seg_6418" s="T418">With his arrow he shoots through the fat sinews of a sleeping man, the Lyypyrdaan's son-in-law.</ta>
            <ta e="T434" id="Seg_6419" s="T428">"Well, I'm dying for my father-in-law's misdeed.</ta>
            <ta e="T439" id="Seg_6420" s="T434">It was not me who killed your father", that one says.</ta>
            <ta e="T443" id="Seg_6421" s="T439">"You'd better kill me.</ta>
            <ta e="T449" id="Seg_6422" s="T443">How shall I live now?!", he says.</ta>
            <ta e="T456" id="Seg_6423" s="T449">The child kills that human.</ta>
            <ta e="T464" id="Seg_6424" s="T456">As he has heard his words, he kills Lyypyrdaan, too.</ta>
            <ta e="T468" id="Seg_6425" s="T464">He carries his wealth and prosperity to his home.</ta>
            <ta e="T477" id="Seg_6426" s="T468">He brought this wealth home and lived together with his mother wealthy and well.</ta>
            <ta e="T478" id="Seg_6427" s="T477">The end.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_6428" s="T0">Es lebt Lyypyrdaan.</ta>
            <ta e="T8" id="Seg_6429" s="T3">Lyypyrdaan geht irgendwohin.</ta>
            <ta e="T13" id="Seg_6430" s="T8">So gehend stößt er auf einen Menschen mit Frau.</ta>
            <ta e="T16" id="Seg_6431" s="T13">Dieser Mensch hat einen Sohn.</ta>
            <ta e="T21" id="Seg_6432" s="T16">Der Mensch mit der Frau nomadisierte alleine.</ta>
            <ta e="T27" id="Seg_6433" s="T21">Am Abend, als es dämmerte, kam er an einen Ort.</ta>
            <ta e="T35" id="Seg_6434" s="T27">Als er anfing, seine Schuhe auszuziehen, um zu schlafen, prasselte das Feuer ein paar mal.</ta>
            <ta e="T37" id="Seg_6435" s="T35">Dieser Mensch: </ta>
            <ta e="T43" id="Seg_6436" s="T37">"Ich bin doch am Tag gegangen, was kann das sein?</ta>
            <ta e="T49" id="Seg_6437" s="T43">Warum spricht mein Feuer?", denkt er, hat keine Angst und legt sich hin.</ta>
            <ta e="T62" id="Seg_6438" s="T49">Lyypyrdaan kommt und tötet diesen Menschen, der ein mit Silber überzogenen Bogen hat, also so einen Bogen.</ta>
            <ta e="T70" id="Seg_6439" s="T62">Die Frau dieses Menschen enflieht mit ihrem kleinen Kind.</ta>
            <ta e="T77" id="Seg_6440" s="T70">Die Frau hat nichts mehr, sie lebt in einer kleinen Erdhütte.</ta>
            <ta e="T83" id="Seg_6441" s="T77">Das Kind wird groß, im Märchen dauert das nicht lange.</ta>
            <ta e="T90" id="Seg_6442" s="T83">Das Kind ging hinaus und sah, dass auf dem Dach weiße Gestalten liefen.</ta>
            <ta e="T93" id="Seg_6443" s="T90">Es geht zu seiner Mutter hinein und erzählte:</ta>
            <ta e="T100" id="Seg_6444" s="T93">"Ach, das sind wohl Rebhühner", sagt die Mutter.</ta>
            <ta e="T112" id="Seg_6445" s="T100">"Oh, wenn ich nur einen Vater hätte, er könnte etwas machen", sagt der Junge zu seiner Mutter.</ta>
            <ta e="T118" id="Seg_6446" s="T112">Die Mutter erzählt überhaupt nichts über den Vater.</ta>
            <ta e="T127" id="Seg_6447" s="T118">Die Mutter schneidet ihre Haare ab und macht daraus Faden, der Junge macht daraus Fangschlingen.</ta>
            <ta e="T135" id="Seg_6448" s="T127">Damit töten sie Rebhühner und ernähren sich von ihnen, so leben sie.</ta>
            <ta e="T140" id="Seg_6449" s="T135">Der Junge fragt immer wieder: </ta>
            <ta e="T143" id="Seg_6450" s="T140">"Hatte ich einen Vater?"</ta>
            <ta e="T149" id="Seg_6451" s="T143">Die Mutter sagte offenbar nichts, sie verbarg alles.</ta>
            <ta e="T154" id="Seg_6452" s="T149">Lyypyrdaan geht wieder, um Menschen zu töten.</ta>
            <ta e="T158" id="Seg_6453" s="T154">Er kommt zu einer Frau mit ihrem Sohn.</ta>
            <ta e="T165" id="Seg_6454" s="T158">Er sah plötzlich, dass dort eine sehr dicke Lärche steht.</ta>
            <ta e="T172" id="Seg_6455" s="T165">An diesen Baum hängte der Sohn der alten Frau seinen Bogen.</ta>
            <ta e="T174" id="Seg_6456" s="T172">Lyypyrdaan fragt:</ta>
            <ta e="T177" id="Seg_6457" s="T174">"Wessen Bogen ist das?</ta>
            <ta e="T179" id="Seg_6458" s="T177">Zu wem gehörst du?"</ta>
            <ta e="T180" id="Seg_6459" s="T179">Die alte Frau:</ta>
            <ta e="T182" id="Seg_6460" s="T180">"Ich habe einen Sohn", sagt sie.</ta>
            <ta e="T188" id="Seg_6461" s="T182">"Aber warum hat er denn seinen Bogen dort gelassen?"</ta>
            <ta e="T197" id="Seg_6462" s="T188">"Die Kraft dieses Bogens ging aus, deshalb ließ er ihn", sagt die alte Frau.</ta>
            <ta e="T204" id="Seg_6463" s="T197">Dort liegt noch so ein sehr, sehr großer Stein.</ta>
            <ta e="T209" id="Seg_6464" s="T204">Danach fragt dieser Mensch auch.</ta>
            <ta e="T211" id="Seg_6465" s="T209">Die alte Frau darauf:</ta>
            <ta e="T216" id="Seg_6466" s="T211">"Wenn er sich langweilt, dann spielt er damit Ball", sagt sie.</ta>
            <ta e="T223" id="Seg_6467" s="T216">"Wenn er ihn schießt, dann rollt er bis zu jenem Berg und wieder zurück.</ta>
            <ta e="T227" id="Seg_6468" s="T223">So spielt er normalerweise", sagt sie.</ta>
            <ta e="T229" id="Seg_6469" s="T227">Lyypyrdaan sagt:</ta>
            <ta e="T233" id="Seg_6470" s="T229">"Morgen habe ich Geburtstag.</ta>
            <ta e="T236" id="Seg_6471" s="T233">Da soll dein Sohn kommen."</ta>
            <ta e="T244" id="Seg_6472" s="T236">Als der Sohn nach Hause kommt, erzählt die Mutter alles:</ta>
            <ta e="T246" id="Seg_6473" s="T244">"Es wird eingeladen", sagt sie.</ta>
            <ta e="T252" id="Seg_6474" s="T246">Nun, dieser Mensch ging zu Lyypyrdaan.</ta>
            <ta e="T254" id="Seg_6475" s="T252">Lyypyrdaan zu seinen Leuten:</ta>
            <ta e="T260" id="Seg_6476" s="T254">Wir umzingeln diesen Menschen und töten ihn", sagt er.</ta>
            <ta e="T264" id="Seg_6477" s="T260">Als er kommt, umzingeln sie ihren Gast.</ta>
            <ta e="T267" id="Seg_6478" s="T264">Schließlich hatten sie beschlossen, ihn zu töten.</ta>
            <ta e="T271" id="Seg_6479" s="T267">Lyypyrdaan hat Angst und sagt zu seinen Freunden:</ta>
            <ta e="T275" id="Seg_6480" s="T271">"Warum versammelt ihr euch?"</ta>
            <ta e="T278" id="Seg_6481" s="T275">Da sagt der Gast:</ta>
            <ta e="T284" id="Seg_6482" s="T278">"Alter Mann, ich weiß, dass du mich töten möchtest.</ta>
            <ta e="T294" id="Seg_6483" s="T284">Ich habe nur ein einsames kleines Messer, doch ich könnte damit dein ganzes Volk auslöschen", sagt er.</ta>
            <ta e="T298" id="Seg_6484" s="T294">"Ach, tu das nicht.</ta>
            <ta e="T303" id="Seg_6485" s="T298">Heirate meine Tochter", sagte er.</ta>
            <ta e="T309" id="Seg_6486" s="T303">Der Gast heiratet die Tochter von Lyypyrdaan.</ta>
            <ta e="T319" id="Seg_6487" s="T309">Als dieser Junge groß geworden war, suchte er wohl immer nach dem Menschen, der seinen Vater getötet hatte.</ta>
            <ta e="T327" id="Seg_6488" s="T319">Als er erwachsen geworden war, ging er offenbar immer zu dem Ort, an dem sein Vater gestorben war.</ta>
            <ta e="T333" id="Seg_6489" s="T327">Als er dort einmal liegt, gehen zwei Menschen dort entlang.</ta>
            <ta e="T340" id="Seg_6490" s="T333">Einer von ihnen trägt einen gefrorenen Bären auf der Brust.</ta>
            <ta e="T343" id="Seg_6491" s="T340">Da sagt sein Freund:</ta>
            <ta e="T351" id="Seg_6492" s="T343">"Nun, hier brachte Lyypyrdaan einen Menschen mit versilbertem Bogen um.</ta>
            <ta e="T356" id="Seg_6493" s="T351">Der Sohn dieses Menschen lebt", sagt er.</ta>
            <ta e="T361" id="Seg_6494" s="T356">Da hört der Junge, dass er einen Vater hatte.</ta>
            <ta e="T368" id="Seg_6495" s="T361">Da schreit dieses Kind zu den Leuten.</ta>
            <ta e="T375" id="Seg_6496" s="T368">Sie denken, dass der Geist dieses Ortes sie ängstigt, sie laufen davon und lassen den Kadaver des Bären dort.</ta>
            <ta e="T383" id="Seg_6497" s="T375">Der Junge trägt den Bären mit dem Beluga nach Hause zu seiner Mutter.</ta>
            <ta e="T386" id="Seg_6498" s="T383">Sie essen diese.</ta>
            <ta e="T388" id="Seg_6499" s="T386">Die Mutter tadelt ihn:</ta>
            <ta e="T392" id="Seg_6500" s="T388">"Was bist du so lange unterwegs?</ta>
            <ta e="T394" id="Seg_6501" s="T392">Sie werden dich töten."</ta>
            <ta e="T399" id="Seg_6502" s="T394">Der Sohn hört überhaupt nicht auf die Worte seiner Mutter.</ta>
            <ta e="T404" id="Seg_6503" s="T399">Er sucht ständig nach seinem Vater.</ta>
            <ta e="T411" id="Seg_6504" s="T404">Und so findet er das Haus des alten Mannes Lyypyrdaan.</ta>
            <ta e="T418" id="Seg_6505" s="T411">Er fand es, und als sie abends alle einschlafen, kommt er.</ta>
            <ta e="T428" id="Seg_6506" s="T418">Er schießt einem schlafenden Menschen mit seinem Pfeil in die fetten Sehnen, dem Schwiegersohn Lyypyrdaans.</ta>
            <ta e="T434" id="Seg_6507" s="T428">"Nun krepiere ich wegen der Schlechtheit meines Schwiegervaters.</ta>
            <ta e="T439" id="Seg_6508" s="T434">Nicht ich habe deinen Vater umgebracht", sagt jener.</ta>
            <ta e="T443" id="Seg_6509" s="T439">"Töte mich lieber.</ta>
            <ta e="T449" id="Seg_6510" s="T443">Wie soll ich denn jetzt leben?!", sagt er.</ta>
            <ta e="T456" id="Seg_6511" s="T449">Da tötet der Junge diesen Menschen.</ta>
            <ta e="T464" id="Seg_6512" s="T456">Als es das gehört hat, tötet dieses Kind auch Lyypyrdaan.</ta>
            <ta e="T468" id="Seg_6513" s="T464">Den Reichtum und Wohlstand nimmt er mit nach Hause.</ta>
            <ta e="T477" id="Seg_6514" s="T468">Er brachte diesen Reichtum nach Hause und lebte mit seiner Mutter reich und gut.</ta>
            <ta e="T478" id="Seg_6515" s="T477">Ende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_6516" s="T0">Вот живет Лыыпырдаан.</ta>
            <ta e="T8" id="Seg_6517" s="T3">Лыыпырдаан все бродит где-то.</ta>
            <ta e="T13" id="Seg_6518" s="T8">Однажды он набредает на человека с женой.</ta>
            <ta e="T16" id="Seg_6519" s="T13">У этого человека есть сын.</ta>
            <ta e="T21" id="Seg_6520" s="T16">Человек с женой кочевал в одиночку.</ta>
            <ta e="T27" id="Seg_6521" s="T21">Вечером, когда наступила темнота, он остановился в одном месте.</ta>
            <ta e="T35" id="Seg_6522" s="T27">Когда он снимал перед сном обувь, несколько раз затрещал огонь.</ta>
            <ta e="T37" id="Seg_6523" s="T35">Этот человек: </ta>
            <ta e="T43" id="Seg_6524" s="T37">"Я же днем ходил благополучно, что может быть?</ta>
            <ta e="T49" id="Seg_6525" s="T43">Почему заговорил мой огонь?", думает, не боясь, и ложится.</ta>
            <ta e="T62" id="Seg_6526" s="T49">Ночью Лыыпырдаан приходит и убивает этого человека, у которого есть посеребренный лук, его оружие-то.</ta>
            <ta e="T70" id="Seg_6527" s="T62">Жена того человека с малым ребенком убегает.</ta>
            <ta e="T77" id="Seg_6528" s="T70">Женщина осталась без ничего, стала жить в маленькой землянке-голомо.</ta>
            <ta e="T83" id="Seg_6529" s="T77">Ребенок вырос — в олонгко это быстро происходит.</ta>
            <ta e="T90" id="Seg_6530" s="T83">Мальчик вышел во двор — по крыше бегают беленькие существа.</ta>
            <ta e="T93" id="Seg_6531" s="T90">Он вернулся к матери и рассказал об этом.</ta>
            <ta e="T100" id="Seg_6532" s="T93">"А, наверное, куропатки бегают", говорит мать.</ta>
            <ta e="T112" id="Seg_6533" s="T100">"О, если бы у меня был отец, он-то уж что-нибудь придумал бы", говорит мальчик матери.</ta>
            <ta e="T118" id="Seg_6534" s="T112">Мать мальчику ничего не рассказывает об отце.</ta>
            <ta e="T127" id="Seg_6535" s="T118">Мать срезывает свои волосы, сплетает из них силки, мальчик начинает ставить петли.</ta>
            <ta e="T135" id="Seg_6536" s="T127">Так добывая куропаток, ими питаясь, живут.</ta>
            <ta e="T140" id="Seg_6537" s="T135">Мальчик век-то спрашивает: </ta>
            <ta e="T143" id="Seg_6538" s="T140">"У меня был отец?"</ta>
            <ta e="T149" id="Seg_6539" s="T143">Мать, оказывается, ничего [о нем] не говорила, скрывала.</ta>
            <ta e="T154" id="Seg_6540" s="T149">Лыыпырдаан опять ищет людей, чтобы убить.</ta>
            <ta e="T158" id="Seg_6541" s="T154">Приходит к одной старухе с сыном.</ta>
            <ta e="T165" id="Seg_6542" s="T158">Там вдруг видит, стоит толстенная лиственница.</ta>
            <ta e="T172" id="Seg_6543" s="T165">На это дерево сын старухи повесил лук.</ta>
            <ta e="T174" id="Seg_6544" s="T172">Лыыпырдаан спрашивает:</ta>
            <ta e="T177" id="Seg_6545" s="T174">"Это чей лук?</ta>
            <ta e="T179" id="Seg_6546" s="T177">С кем живешь?"</ta>
            <ta e="T180" id="Seg_6547" s="T179">Старуха:</ta>
            <ta e="T182" id="Seg_6548" s="T180">"Сын есть у меня", говорит.</ta>
            <ta e="T188" id="Seg_6549" s="T182">"Постой, а почему он оставил свой лук?"</ta>
            <ta e="T197" id="Seg_6550" s="T188">"Сила этого лука закончилась, поэтому он оставил", говорит старуха.</ta>
            <ta e="T204" id="Seg_6551" s="T197">Тут же лежит валун, большущий камень.</ta>
            <ta e="T209" id="Seg_6552" s="T204">Об этом тоже спрашивает тот человек.</ta>
            <ta e="T211" id="Seg_6553" s="T209">Старуха на это:</ta>
            <ta e="T216" id="Seg_6554" s="T211">"Когда заскучает, этим [камнем] в мяч играет", говорит.</ta>
            <ta e="T223" id="Seg_6555" s="T216">"Как пнет его, так до той горы катится и обратно скатывается.</ta>
            <ta e="T227" id="Seg_6556" s="T223">Так обычно играет", говорит.</ta>
            <ta e="T229" id="Seg_6557" s="T227">Лыыпырдаан говорит:</ta>
            <ta e="T233" id="Seg_6558" s="T229">"Завтра у меня именины.</ta>
            <ta e="T236" id="Seg_6559" s="T233">Пусть придет твой сын."</ta>
            <ta e="T244" id="Seg_6560" s="T236">Когда сын приходит домой, мать ему все рассказывает:</ta>
            <ta e="T246" id="Seg_6561" s="T244">"Приглашают", говорит.</ta>
            <ta e="T252" id="Seg_6562" s="T246">Ну, вот, пошел этот человек к Лыыпырдаану.</ta>
            <ta e="T254" id="Seg_6563" s="T252">А Лыыпырдаан предупредил своих:</ta>
            <ta e="T260" id="Seg_6564" s="T254">"Этого человека окружим и убьем", говорит.</ta>
            <ta e="T264" id="Seg_6565" s="T260">Как приходит гость, сразу его окружают.</ta>
            <ta e="T267" id="Seg_6566" s="T264">Ведь договорились его убить.</ta>
            <ta e="T271" id="Seg_6567" s="T267">Но Лыыпырдаан испугается и говорит своим:</ta>
            <ta e="T275" id="Seg_6568" s="T271">"Зачем собираетесь?"</ta>
            <ta e="T278" id="Seg_6569" s="T275">Тут гость говорит:</ta>
            <ta e="T284" id="Seg_6570" s="T278">"Старик, я знаю, что ты хочешь меня убить.</ta>
            <ta e="T294" id="Seg_6571" s="T284">Вот у меня есть только ножичек, но я бы им разом истребил весь твой народ", говорит.</ta>
            <ta e="T298" id="Seg_6572" s="T294">"Что ты, так не делай.</ta>
            <ta e="T303" id="Seg_6573" s="T298">Женись-ка на моей дочери", говорит.</ta>
            <ta e="T309" id="Seg_6574" s="T303">Гость женится на дочери Лыыпырдаана.</ta>
            <ta e="T319" id="Seg_6575" s="T309">А тот парень, став взрослым, век-то искал убийцу отца.</ta>
            <ta e="T327" id="Seg_6576" s="T319">Возмужав, постоянно, оказывается, приходил на место, где убили отца.</ta>
            <ta e="T333" id="Seg_6577" s="T327">Однажды, когда там лежит, мимо проходят два человека.</ta>
            <ta e="T340" id="Seg_6578" s="T333">Один из них носит мерзлую тушу медведя.</ta>
            <ta e="T343" id="Seg_6579" s="T340">Тут спутник его говорит:</ta>
            <ta e="T351" id="Seg_6580" s="T343">"Вот здесь Лыыпырдаан убил человека с посеребренным луком.</ta>
            <ta e="T356" id="Seg_6581" s="T351">А сын того человека жив", говорит.</ta>
            <ta e="T361" id="Seg_6582" s="T356">Вот тут парень и узнавает, что был у него отец.</ta>
            <ta e="T368" id="Seg_6583" s="T361">Парень этот как закричит на тех людей.</ta>
            <ta e="T375" id="Seg_6584" s="T368">Думая, что это пугает дух-хозяин местности, те убегают, оставив тушу медведя.</ta>
            <ta e="T383" id="Seg_6585" s="T375">Парень тушу медведя и добытую им белуху уносит.</ta>
            <ta e="T386" id="Seg_6586" s="T383">Едят их, конечно.</ta>
            <ta e="T388" id="Seg_6587" s="T386">Мать ругает сына:</ta>
            <ta e="T392" id="Seg_6588" s="T388">"Зачем так долго пропадаешь?</ta>
            <ta e="T394" id="Seg_6589" s="T392">Убьют тебя."</ta>
            <ta e="T399" id="Seg_6590" s="T394">Сын вовсе не слушается матери.</ta>
            <ta e="T404" id="Seg_6591" s="T399">Неотступно ищет отца.</ta>
            <ta e="T411" id="Seg_6592" s="T404">Вот как-то находит он жилище старика Лыыпырдаана.</ta>
            <ta e="T418" id="Seg_6593" s="T411">Находит, когда все вечером засыпают, он приходит.</ta>
            <ta e="T428" id="Seg_6594" s="T418">Стрелой простреливает главные сухожилия на ногах спящего человека, зятя Лыыпырдаана.</ta>
            <ta e="T434" id="Seg_6595" s="T428">"Вот гибну из-за злодейств тестя.</ta>
            <ta e="T439" id="Seg_6596" s="T434">Не я убил твоего отца", говорит тот.</ta>
            <ta e="T443" id="Seg_6597" s="T439">"Лучше добей меня.</ta>
            <ta e="T449" id="Seg_6598" s="T443">Как же теперь я буду жить?!", говорит.</ta>
            <ta e="T456" id="Seg_6599" s="T449">Парень тут же добивает того человека.</ta>
            <ta e="T464" id="Seg_6600" s="T456">Услышав его слова, этот парень убивает и Лыыпырдаана.</ta>
            <ta e="T468" id="Seg_6601" s="T464">Богатства и имущество его уносит домой.</ta>
            <ta e="T477" id="Seg_6602" s="T468">Богатством этим завладев, зажили с матерью богато и сытно.</ta>
            <ta e="T478" id="Seg_6603" s="T477">Конец.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_6604" s="T0">Вот живет Лыыпырдаан.</ta>
            <ta e="T8" id="Seg_6605" s="T3">Лыыпырдаан все бродит где-то.</ta>
            <ta e="T13" id="Seg_6606" s="T8">Однажды он набрел на человека с женой.</ta>
            <ta e="T16" id="Seg_6607" s="T13">У этого человека был сын.</ta>
            <ta e="T21" id="Seg_6608" s="T16">Человек с женой кочевал в одиночку.</ta>
            <ta e="T27" id="Seg_6609" s="T21">Вечером, когда наступила темнота, он остановился в одном месте.</ta>
            <ta e="T35" id="Seg_6610" s="T27">Когда он снимал перед сном обувь, несколько раз затрещал огонь.</ta>
            <ta e="T37" id="Seg_6611" s="T35">Этот человек не испугался, но подумал: </ta>
            <ta e="T43" id="Seg_6612" s="T37">"Я же днем ходил благополучно, что может быть?</ta>
            <ta e="T49" id="Seg_6613" s="T43">Почему заговорил мой огонь?" — и лег спать.</ta>
            <ta e="T62" id="Seg_6614" s="T49">Ночью Лыыпырдаан пришел и убил этого человека, у которого был посеребренный лук, его оружие-то.</ta>
            <ta e="T70" id="Seg_6615" s="T62">Жена того человека с малым ребенком убежала.</ta>
            <ta e="T77" id="Seg_6616" s="T70">Женщина осталась без ничего, стала жить в маленькой землянке-голомо.</ta>
            <ta e="T83" id="Seg_6617" s="T77">Ребенок вырос — в олонгко это быстро происходит.</ta>
            <ta e="T90" id="Seg_6618" s="T83">Мальчик вышел во двор — по крыше бегают беленькие существа.</ta>
            <ta e="T93" id="Seg_6619" s="T90">Он вернулся к матери и рассказал об этом.</ta>
            <ta e="T100" id="Seg_6620" s="T93">— А, наверное, куропатки бегают, — говорит мать.</ta>
            <ta e="T112" id="Seg_6621" s="T100">— О, если бы у меня был отец, он-то уж что-нибудь придумал бы, — сказал мальчик матери.</ta>
            <ta e="T118" id="Seg_6622" s="T112">Мать мальчику ничего не рассказывала об отце.</ta>
            <ta e="T127" id="Seg_6623" s="T118">Мать срезала свои волосы, сплела из них силки, мальчик начал ставить петли.</ta>
            <ta e="T135" id="Seg_6624" s="T127">Так добывая куропаток, ими питаясь, жили.</ta>
            <ta e="T140" id="Seg_6625" s="T135">Мальчик век-то допытывался: </ta>
            <ta e="T143" id="Seg_6626" s="T140">"У меня был отец?" </ta>
            <ta e="T149" id="Seg_6627" s="T143">Мать, оказывается, ничего [о нем] не говорила, скрывала.</ta>
            <ta e="T154" id="Seg_6628" s="T149">Лыыпырдаан опять ищет людей, чтобы убить.</ta>
            <ta e="T158" id="Seg_6629" s="T154">Приходит к одной старухе с сыном.</ta>
            <ta e="T165" id="Seg_6630" s="T158">Там вдруг видит, стоит толстенная лиственница.</ta>
            <ta e="T172" id="Seg_6631" s="T165">На это дерево сын старухи повесил лук.</ta>
            <ta e="T174" id="Seg_6632" s="T172">Лыыпырдаан спрашивает:</ta>
            <ta e="T177" id="Seg_6633" s="T174">— Это чей лук?</ta>
            <ta e="T179" id="Seg_6634" s="T177">С кем живешь?</ta>
            <ta e="T180" id="Seg_6635" s="T179">Старуха говорит:</ta>
            <ta e="T182" id="Seg_6636" s="T180">— Сын есть у меня.</ta>
            <ta e="T188" id="Seg_6637" s="T182">— Постой, а почему он оставил свой лук?</ta>
            <ta e="T197" id="Seg_6638" s="T188">— Этот лук для него слабый, поэтому оставил, — отвечает старуха.</ta>
            <ta e="T204" id="Seg_6639" s="T197">Тут же лежал валун, большущий камень.</ta>
            <ta e="T209" id="Seg_6640" s="T204">Об этом тоже спрашивает тот человек.</ta>
            <ta e="T211" id="Seg_6641" s="T209">Старуха на это отвечает.</ta>
            <ta e="T216" id="Seg_6642" s="T211">— Когда заскучает, этим [камнем] в мяч играет.</ta>
            <ta e="T223" id="Seg_6643" s="T216">Как пнет его, так до той горы катится и обратно скатывается.</ta>
            <ta e="T227" id="Seg_6644" s="T223">Так обычно играет.</ta>
            <ta e="T229" id="Seg_6645" s="T227">Лыыпырдаан говорит:</ta>
            <ta e="T233" id="Seg_6646" s="T229">— Завтра у меня именины.</ta>
            <ta e="T236" id="Seg_6647" s="T233">Пусть придет твой сын.</ta>
            <ta e="T244" id="Seg_6648" s="T236">Когда сын пришел домой, мать ему все рассказала.</ta>
            <ta e="T246" id="Seg_6649" s="T244">— Приглашают, — говорит.</ta>
            <ta e="T252" id="Seg_6650" s="T246">Ну, вот, пошел этот человек к Лыыпырдаану.</ta>
            <ta e="T254" id="Seg_6651" s="T252">А Лыыпырдаан предупредил своих:</ta>
            <ta e="T260" id="Seg_6652" s="T254">— Этого человека окружим и убьем.</ta>
            <ta e="T264" id="Seg_6653" s="T260">Как пришел гость, сразу его окружили.</ta>
            <ta e="T267" id="Seg_6654" s="T264">Ведь договорились его убить.</ta>
            <ta e="T271" id="Seg_6655" s="T267">Но Лыыпырдаан испугался и говорит своим:</ta>
            <ta e="T275" id="Seg_6656" s="T271">— Зачем собираетесь?</ta>
            <ta e="T278" id="Seg_6657" s="T275">Тут гость сказал:</ta>
            <ta e="T284" id="Seg_6658" s="T278">— Старик, я знаю, что ты хочешь меня убить.</ta>
            <ta e="T294" id="Seg_6659" s="T284">Вот у меня есть только ножичек, но я бы им разом истребил весь твой народ.</ta>
            <ta e="T298" id="Seg_6660" s="T294">— Что ты, так не делай.</ta>
            <ta e="T303" id="Seg_6661" s="T298">Женись-ка на моей дочери, — говорит.</ta>
            <ta e="T309" id="Seg_6662" s="T303">Гость женился на дочери Лыыпырдаана.</ta>
            <ta e="T319" id="Seg_6663" s="T309">А тот парень, став взрослым, век-то искал убийцу отца.</ta>
            <ta e="T327" id="Seg_6664" s="T319">Возмужав, постоянно, оказывается, приходил на место, где убили отца.</ta>
            <ta e="T333" id="Seg_6665" s="T327">Однажды, когда там лежал, мимо проходили два человека.</ta>
            <ta e="T340" id="Seg_6666" s="T333">Один из них нес мерзлую тушу медведя.</ta>
            <ta e="T343" id="Seg_6667" s="T340">Тут спутник его говорит:</ta>
            <ta e="T351" id="Seg_6668" s="T343">— Вот здесь Лыыпырдаан убил человека с посеребренным луком.</ta>
            <ta e="T356" id="Seg_6669" s="T351">А сын того человека жив.</ta>
            <ta e="T361" id="Seg_6670" s="T356">Вот тут парень и узнал, что был у него отец.</ta>
            <ta e="T368" id="Seg_6671" s="T361">Парень этот как закричит на тех людей.</ta>
            <ta e="T375" id="Seg_6672" s="T368">Думая, что это пугает дух-хозяин местности, те убежали, оставив тушу медведя.</ta>
            <ta e="T383" id="Seg_6673" s="T375">Парень тушу медведя и добытую им белуху унес.</ta>
            <ta e="T386" id="Seg_6674" s="T383">Едят их, конечно.</ta>
            <ta e="T388" id="Seg_6675" s="T386">Мать ругает сына:</ta>
            <ta e="T392" id="Seg_6676" s="T388">— Зачем так долго пропадаешь?</ta>
            <ta e="T394" id="Seg_6677" s="T392">Убьют тебя.</ta>
            <ta e="T399" id="Seg_6678" s="T394">Сын вовсе не слушается матери.</ta>
            <ta e="T404" id="Seg_6679" s="T399">Неотступно ищет убийц отца.</ta>
            <ta e="T411" id="Seg_6680" s="T404">Вот как-то нашел он жилище старика Лыыпырдаана.</ta>
            <ta e="T418" id="Seg_6681" s="T411">Пришел, когда все вечером заснули.</ta>
            <ta e="T428" id="Seg_6682" s="T418">Стрелой прострелил главные сухожилия на ногах спящего человека, зятя Лыыпырдаана.</ta>
            <ta e="T434" id="Seg_6683" s="T428">— Вот гибну из-за злодейств тестя.</ta>
            <ta e="T439" id="Seg_6684" s="T434">Не я убил твоего отца, — говорит тот.</ta>
            <ta e="T443" id="Seg_6685" s="T439">— Лучше добей меня.</ta>
            <ta e="T449" id="Seg_6686" s="T443">Как же теперь я буду жить?! — говорит.</ta>
            <ta e="T456" id="Seg_6687" s="T449">Парень тут же добил того человека.</ta>
            <ta e="T464" id="Seg_6688" s="T456">Услышав его слова, этот парень убил и Лыыпырдаана.</ta>
            <ta e="T468" id="Seg_6689" s="T464">Богатства и имущество его унес домой.</ta>
            <ta e="T477" id="Seg_6690" s="T468">Богатством этим завладев, зажили с матерью богато и сытно.</ta>
            <ta e="T478" id="Seg_6691" s="T477">Конец.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
