<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID494527FA-94ED-083A-9CC8-FAE55A5A7169">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoMA_1964_FoxDeceiver_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="creation-date">2017-02-27T11:21:33.954+01:00</ud-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\PoMA_1964_FoxDeceiver_flk\PoMA_1964_FoxDeceiver_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">508</ud-information>
            <ud-information attribute-name="# HIAT:w">382</ud-information>
            <ud-information attribute-name="# e">382</ud-information>
            <ud-information attribute-name="# HIAT:u">67</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoMA">
            <abbreviation>PoMA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
         <tli id="T349" time="178.5" type="appl" />
         <tli id="T350" time="179.0" type="appl" />
         <tli id="T351" time="179.5" type="appl" />
         <tli id="T352" time="180.0" type="appl" />
         <tli id="T353" time="180.5" type="appl" />
         <tli id="T354" time="181.0" type="appl" />
         <tli id="T355" time="181.5" type="appl" />
         <tli id="T356" time="182.0" type="appl" />
         <tli id="T357" time="182.5" type="appl" />
         <tli id="T358" time="183.0" type="appl" />
         <tli id="T359" time="183.5" type="appl" />
         <tli id="T360" time="184.0" type="appl" />
         <tli id="T361" time="184.5" type="appl" />
         <tli id="T362" time="185.0" type="appl" />
         <tli id="T363" time="185.5" type="appl" />
         <tli id="T364" time="186.0" type="appl" />
         <tli id="T365" time="186.5" type="appl" />
         <tli id="T366" time="187.0" type="appl" />
         <tli id="T367" time="187.5" type="appl" />
         <tli id="T368" time="188.0" type="appl" />
         <tli id="T369" time="188.5" type="appl" />
         <tli id="T370" time="189.0" type="appl" />
         <tli id="T371" time="189.5" type="appl" />
         <tli id="T372" time="190.0" type="appl" />
         <tli id="T373" time="190.5" type="appl" />
         <tli id="T374" time="191.0" type="appl" />
         <tli id="T375" time="191.5" type="appl" />
         <tli id="T376" time="192.0" type="appl" />
         <tli id="T377" time="192.5" type="appl" />
         <tli id="T378" time="193.0" type="appl" />
         <tli id="T379" time="193.5" type="appl" />
         <tli id="T380" time="194.0" type="appl" />
         <tli id="T381" time="194.5" type="appl" />
         <tli id="T382" time="195.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoMA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T382" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Umnahɨt</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">paːsɨnaj</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">olorbut</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Bu</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">paːsɨnaj</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">ölbütüger</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">bu͡or</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">golomotugar</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">u͡ol</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">ogoto</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">tulaːjak</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">kaːlar</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_44" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">Bu</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">ogo</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">aččɨktaːn</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">ölörö</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">bu͡olar</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_62" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">Bunu</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">hahɨl</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">bular</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_74" n="HIAT:u" s="T20">
                  <nts id="Seg_75" n="HIAT:ip">"</nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">Min</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">enʼigin</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">kihi</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">oŋoru͡om</ts>
                  <nts id="Seg_87" n="HIAT:ip">"</nts>
                  <nts id="Seg_88" n="HIAT:ip">,</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">diːr</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_95" n="HIAT:u" s="T25">
                  <nts id="Seg_96" n="HIAT:ip">"</nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">Baːj</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">kihi</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_104" n="HIAT:w" s="T27">bu͡olu͡oŋ</ts>
                  <nts id="Seg_105" n="HIAT:ip">,</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_108" n="HIAT:w" s="T28">baːj</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_111" n="HIAT:w" s="T29">dʼaktarɨ</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_114" n="HIAT:w" s="T30">dʼaktar</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">ɨlan</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">bi͡eri͡em</ts>
                  <nts id="Seg_121" n="HIAT:ip">.</nts>
                  <nts id="Seg_122" n="HIAT:ip">"</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_125" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">Innʼe</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">di͡en</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">baran</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">hahɨl</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">Etiŋ</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T38">ɨraːktaːgɨtɨgar</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">barar</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_149" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_151" n="HIAT:w" s="T40">Etiŋ</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_154" n="HIAT:w" s="T41">ɨraːktaːgɨtɨgar</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_157" n="HIAT:w" s="T42">tijen</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_160" n="HIAT:w" s="T43">diːr</ts>
                  <nts id="Seg_161" n="HIAT:ip">:</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_164" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_166" n="HIAT:w" s="T44">Aːttaːk</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">baːj</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_172" n="HIAT:w" s="T46">u͡ol</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_175" n="HIAT:w" s="T47">kihi</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_178" n="HIAT:w" s="T48">baːr</ts>
                  <nts id="Seg_179" n="HIAT:ip">.</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_182" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_184" n="HIAT:w" s="T49">Gini</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_187" n="HIAT:w" s="T50">en</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_190" n="HIAT:w" s="T51">kɨːskɨn</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_193" n="HIAT:w" s="T52">hu͡orumnʼulata</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_196" n="HIAT:w" s="T53">ɨːtta</ts>
                  <nts id="Seg_197" n="HIAT:ip">.</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_200" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_202" n="HIAT:w" s="T54">Kɨːhɨŋ</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_205" n="HIAT:w" s="T55">huluːtun</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_208" n="HIAT:w" s="T56">tɨ͡a</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_211" n="HIAT:w" s="T57">kɨːlɨn</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_214" n="HIAT:w" s="T58">bastɨŋɨn</ts>
                  <nts id="Seg_215" n="HIAT:ip">,</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_218" n="HIAT:w" s="T59">araːhɨn</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_221" n="HIAT:w" s="T60">ɨːtɨ͡ak</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_224" n="HIAT:w" s="T61">bu͡olla</ts>
                  <nts id="Seg_225" n="HIAT:ip">.</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_228" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_230" n="HIAT:w" s="T62">Ol</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_233" n="HIAT:w" s="T63">kɨːllarɨ</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_236" n="HIAT:w" s="T64">tus</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_239" n="HIAT:w" s="T65">tuhunan</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_242" n="HIAT:w" s="T66">dʼi͡ege</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_245" n="HIAT:w" s="T67">as</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_248" n="HIAT:w" s="T68">belemneːn</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_251" n="HIAT:w" s="T69">tohuju͡oŋ</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_254" n="HIAT:w" s="T70">ühü</ts>
                  <nts id="Seg_255" n="HIAT:ip">"</nts>
                  <nts id="Seg_256" n="HIAT:ip">,</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_259" n="HIAT:w" s="T71">diːr</ts>
                  <nts id="Seg_260" n="HIAT:ip">.</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_263" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_265" n="HIAT:w" s="T72">Etiŋ</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_268" n="HIAT:w" s="T73">ɨraːktaːgɨ</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_271" n="HIAT:w" s="T74">höbülemmit</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_275" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_277" n="HIAT:w" s="T75">Hahɨl</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_280" n="HIAT:w" s="T76">albun</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_283" n="HIAT:w" s="T77">töttörü</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_286" n="HIAT:w" s="T78">hüːrbüt</ts>
                  <nts id="Seg_287" n="HIAT:ip">.</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_290" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_292" n="HIAT:w" s="T79">Tɨ͡a</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_295" n="HIAT:w" s="T80">kɨːllarɨn</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_298" n="HIAT:w" s="T81">araːhɨn</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_301" n="HIAT:w" s="T82">körsön</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_304" n="HIAT:w" s="T83">haŋarar</ts>
                  <nts id="Seg_305" n="HIAT:ip">:</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_308" n="HIAT:u" s="T84">
                  <nts id="Seg_309" n="HIAT:ip">"</nts>
                  <ts e="T85" id="Seg_311" n="HIAT:w" s="T84">Etiŋ</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_314" n="HIAT:w" s="T85">ɨraːktaːgɨ</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_317" n="HIAT:w" s="T86">baːja-toto</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_320" n="HIAT:w" s="T87">batɨmɨja</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_323" n="HIAT:w" s="T88">ehi͡eke</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_326" n="HIAT:w" s="T89">as</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_329" n="HIAT:w" s="T90">terijde</ts>
                  <nts id="Seg_330" n="HIAT:ip">.</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_333" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_335" n="HIAT:w" s="T91">Onu</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_338" n="HIAT:w" s="T92">baran</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_341" n="HIAT:w" s="T93">ahɨːr</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_344" n="HIAT:w" s="T94">ühügüt</ts>
                  <nts id="Seg_345" n="HIAT:ip">.</nts>
                  <nts id="Seg_346" n="HIAT:ip">"</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_349" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_351" n="HIAT:w" s="T95">Kɨːl</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_354" n="HIAT:w" s="T96">bögö</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_357" n="HIAT:w" s="T97">aːlin</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_360" n="HIAT:w" s="T98">bu͡olan</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_363" n="HIAT:w" s="T99">tijeller</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_366" n="HIAT:w" s="T100">ahɨː</ts>
                  <nts id="Seg_367" n="HIAT:ip">.</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_370" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_372" n="HIAT:w" s="T101">Etiŋ</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_375" n="HIAT:w" s="T102">ɨraːktaːgɨ</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_378" n="HIAT:w" s="T103">ü͡örer</ts>
                  <nts id="Seg_379" n="HIAT:ip">,</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_381" n="HIAT:ip">"</nts>
                  <ts e="T105" id="Seg_383" n="HIAT:w" s="T104">kütü͡ötüm</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_386" n="HIAT:w" s="T105">baːj</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_389" n="HIAT:w" s="T106">kihi</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_392" n="HIAT:w" s="T107">ebit</ts>
                  <nts id="Seg_393" n="HIAT:ip">"</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_396" n="HIAT:w" s="T108">diːr</ts>
                  <nts id="Seg_397" n="HIAT:ip">.</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_400" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_402" n="HIAT:w" s="T109">Kɨːllar</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_405" n="HIAT:w" s="T110">ahɨː</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_408" n="HIAT:w" s="T111">turdaktarɨna</ts>
                  <nts id="Seg_409" n="HIAT:ip">,</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_412" n="HIAT:w" s="T112">bili</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_415" n="HIAT:w" s="T113">dʼi͡eleri</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_418" n="HIAT:w" s="T114">kataːn</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_421" n="HIAT:w" s="T115">kebiheller</ts>
                  <nts id="Seg_422" n="HIAT:ip">.</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_425" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_427" n="HIAT:w" s="T116">Hahɨl</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_430" n="HIAT:w" s="T117">albun</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_433" n="HIAT:w" s="T118">Etiŋ</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_436" n="HIAT:w" s="T119">ɨraːktaːgɨga</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_439" n="HIAT:w" s="T120">tijer</ts>
                  <nts id="Seg_440" n="HIAT:ip">.</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_443" n="HIAT:u" s="T121">
                  <nts id="Seg_444" n="HIAT:ip">"</nts>
                  <ts e="T122" id="Seg_446" n="HIAT:w" s="T121">Kaja</ts>
                  <nts id="Seg_447" n="HIAT:ip">,</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_450" n="HIAT:w" s="T122">kɨːllar</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_453" n="HIAT:w" s="T123">kelliler</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_456" n="HIAT:w" s="T124">dʼürü</ts>
                  <nts id="Seg_457" n="HIAT:ip">?</nts>
                  <nts id="Seg_458" n="HIAT:ip">"</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_461" n="HIAT:u" s="T125">
                  <nts id="Seg_462" n="HIAT:ip">"</nts>
                  <ts e="T126" id="Seg_464" n="HIAT:w" s="T125">Kelliler</ts>
                  <nts id="Seg_465" n="HIAT:ip">.</nts>
                  <nts id="Seg_466" n="HIAT:ip">"</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_469" n="HIAT:u" s="T126">
                  <nts id="Seg_470" n="HIAT:ip">"</nts>
                  <ts e="T127" id="Seg_472" n="HIAT:w" s="T126">Kütü͡öt</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_475" n="HIAT:w" s="T127">u͡ol</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_478" n="HIAT:w" s="T128">boku͡oja</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_481" n="HIAT:w" s="T129">hu͡ok</ts>
                  <nts id="Seg_482" n="HIAT:ip">.</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_485" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_487" n="HIAT:w" s="T130">Hotoru</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_490" n="HIAT:w" s="T131">holotu͡oj</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_493" n="HIAT:w" s="T132">karaːbɨnan</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_496" n="HIAT:w" s="T133">keli͡e</ts>
                  <nts id="Seg_497" n="HIAT:ip">"</nts>
                  <nts id="Seg_498" n="HIAT:ip">,</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_501" n="HIAT:w" s="T134">diːr</ts>
                  <nts id="Seg_502" n="HIAT:ip">.</nts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_505" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_507" n="HIAT:w" s="T135">U͡olugar</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_510" n="HIAT:w" s="T136">töttörü</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_513" n="HIAT:w" s="T137">hüːren</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_516" n="HIAT:w" s="T138">tijer</ts>
                  <nts id="Seg_517" n="HIAT:ip">.</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_520" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_522" n="HIAT:w" s="T139">Talagɨnan</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_525" n="HIAT:w" s="T140">kuhagan</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_528" n="HIAT:w" s="T141">bolu͡ot</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_531" n="HIAT:w" s="T142">oŋortoror</ts>
                  <nts id="Seg_532" n="HIAT:ip">.</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_535" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_537" n="HIAT:w" s="T143">Araːs</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_540" n="HIAT:w" s="T144">hir</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_543" n="HIAT:w" s="T145">hibekkitinen</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_546" n="HIAT:w" s="T146">himeter</ts>
                  <nts id="Seg_547" n="HIAT:ip">,</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_550" n="HIAT:w" s="T147">ɨraːktan</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_553" n="HIAT:w" s="T148">hi͡ederej</ts>
                  <nts id="Seg_554" n="HIAT:ip">,</nts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_557" n="HIAT:w" s="T149">basku͡oj</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_560" n="HIAT:w" s="T150">bu͡olan</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_563" n="HIAT:w" s="T151">köstör</ts>
                  <nts id="Seg_564" n="HIAT:ip">.</nts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_567" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_569" n="HIAT:w" s="T152">Tɨ͡al</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_572" n="HIAT:w" s="T153">kün</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_575" n="HIAT:w" s="T154">Etiŋ</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_578" n="HIAT:w" s="T155">ɨraːktaːgɨga</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_581" n="HIAT:w" s="T156">ustallar</ts>
                  <nts id="Seg_582" n="HIAT:ip">.</nts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_585" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_587" n="HIAT:w" s="T157">Etiŋ</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_590" n="HIAT:w" s="T158">ɨraːktaːgɨ</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_593" n="HIAT:w" s="T159">körbüte</ts>
                  <nts id="Seg_594" n="HIAT:ip">,</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_597" n="HIAT:w" s="T160">bajgal</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_600" n="HIAT:w" s="T161">ustun</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_603" n="HIAT:w" s="T162">holotu͡oj</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_606" n="HIAT:w" s="T163">karaːp</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_609" n="HIAT:w" s="T164">iher</ts>
                  <nts id="Seg_610" n="HIAT:ip">,</nts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_613" n="HIAT:w" s="T165">hi͡ederej</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_616" n="HIAT:w" s="T166">öŋnöːk</ts>
                  <nts id="Seg_617" n="HIAT:ip">.</nts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_620" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_622" n="HIAT:w" s="T167">ɨraːktaːgɨ</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_625" n="HIAT:w" s="T168">ü͡örer</ts>
                  <nts id="Seg_626" n="HIAT:ip">.</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_629" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_631" n="HIAT:w" s="T169">Tɨ͡al</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_634" n="HIAT:w" s="T170">tüspütüger</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_637" n="HIAT:w" s="T171">bolu͡ot</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_640" n="HIAT:w" s="T172">ɨhɨllan</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_643" n="HIAT:w" s="T173">kaːlar</ts>
                  <nts id="Seg_644" n="HIAT:ip">.</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_647" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_649" n="HIAT:w" s="T174">Hahɨl</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_652" n="HIAT:w" s="T175">albun</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_655" n="HIAT:w" s="T176">Etiŋ</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_658" n="HIAT:w" s="T177">ɨraːktaːgɨga</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_661" n="HIAT:w" s="T178">hüːren</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_664" n="HIAT:w" s="T179">tijer</ts>
                  <nts id="Seg_665" n="HIAT:ip">,</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_668" n="HIAT:w" s="T180">diːr</ts>
                  <nts id="Seg_669" n="HIAT:ip">:</nts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_672" n="HIAT:u" s="T181">
                  <nts id="Seg_673" n="HIAT:ip">"</nts>
                  <ts e="T182" id="Seg_675" n="HIAT:w" s="T181">i͡edeːn</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_678" n="HIAT:w" s="T182">bu͡olla</ts>
                  <nts id="Seg_679" n="HIAT:ip">,</nts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_682" n="HIAT:w" s="T183">holotu͡oj</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_685" n="HIAT:w" s="T184">karaːp</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_688" n="HIAT:w" s="T185">timirde</ts>
                  <nts id="Seg_689" n="HIAT:ip">.</nts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_692" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_694" n="HIAT:w" s="T186">Kütü͡ötüŋ</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_697" n="HIAT:w" s="T187">kihi</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_700" n="HIAT:w" s="T188">berde</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_703" n="HIAT:w" s="T189">bu͡olan</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_706" n="HIAT:w" s="T190">orto</ts>
                  <nts id="Seg_707" n="HIAT:ip">,</nts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_710" n="HIAT:w" s="T191">tahagaha</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_713" n="HIAT:w" s="T192">barɨta</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_716" n="HIAT:w" s="T193">uːga</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_719" n="HIAT:w" s="T194">barda</ts>
                  <nts id="Seg_720" n="HIAT:ip">,</nts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_723" n="HIAT:w" s="T195">bejete</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_726" n="HIAT:w" s="T196">hɨgɨnnʼak</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_729" n="HIAT:w" s="T197">kaːlla</ts>
                  <nts id="Seg_730" n="HIAT:ip">.</nts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_733" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_735" n="HIAT:w" s="T198">Taŋasta</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_738" n="HIAT:w" s="T199">ɨːt</ts>
                  <nts id="Seg_739" n="HIAT:ip">.</nts>
                  <nts id="Seg_740" n="HIAT:ip">"</nts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_743" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_745" n="HIAT:w" s="T200">Etiŋ</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_748" n="HIAT:w" s="T201">ɨraːktaːgɨ</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_751" n="HIAT:w" s="T202">u͡olga</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_754" n="HIAT:w" s="T203">bastɨŋ</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_757" n="HIAT:w" s="T204">taŋahɨ</ts>
                  <nts id="Seg_758" n="HIAT:ip">,</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_761" n="HIAT:w" s="T205">bejetin</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_764" n="HIAT:w" s="T206">karaːbɨn</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_767" n="HIAT:w" s="T207">ɨːtar</ts>
                  <nts id="Seg_768" n="HIAT:ip">.</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_771" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_773" n="HIAT:w" s="T208">Hahɨl</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_776" n="HIAT:w" s="T209">albun</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_779" n="HIAT:w" s="T210">u͡olun</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_782" n="HIAT:w" s="T211">hübeliːr</ts>
                  <nts id="Seg_783" n="HIAT:ip">:</nts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_786" n="HIAT:u" s="T212">
                  <nts id="Seg_787" n="HIAT:ip">"</nts>
                  <ts e="T213" id="Seg_789" n="HIAT:w" s="T212">ɨraːktaːgɨga</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_792" n="HIAT:w" s="T213">tijeŋŋin</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_795" n="HIAT:w" s="T214">taŋaskɨn</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_798" n="HIAT:w" s="T215">körünüme</ts>
                  <nts id="Seg_799" n="HIAT:ip">,</nts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_802" n="HIAT:w" s="T216">oduːrgu͡oktara</ts>
                  <nts id="Seg_803" n="HIAT:ip">.</nts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_806" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_808" n="HIAT:w" s="T217">Toŋkos</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_811" n="HIAT:w" s="T218">ere</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_814" n="HIAT:w" s="T219">gɨnan</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_817" n="HIAT:w" s="T220">doroːbolos</ts>
                  <nts id="Seg_818" n="HIAT:ip">.</nts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_821" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_823" n="HIAT:w" s="T221">Ahɨːrgar</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_826" n="HIAT:w" s="T222">dʼon</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_829" n="HIAT:w" s="T223">ahɨːrɨn</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_832" n="HIAT:w" s="T224">körön</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_835" n="HIAT:w" s="T225">ahaː</ts>
                  <nts id="Seg_836" n="HIAT:ip">,</nts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_839" n="HIAT:w" s="T226">arɨgɨnɨ</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_842" n="HIAT:w" s="T227">bert</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_845" n="HIAT:w" s="T228">kɨratɨk</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_848" n="HIAT:w" s="T229">is</ts>
                  <nts id="Seg_849" n="HIAT:ip">.</nts>
                  <nts id="Seg_850" n="HIAT:ip">"</nts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_853" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_855" n="HIAT:w" s="T230">U͡ol</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_858" n="HIAT:w" s="T231">Etiŋ</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_861" n="HIAT:w" s="T232">ɨraːktaːgɨga</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_864" n="HIAT:w" s="T233">tijer</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_867" n="HIAT:w" s="T234">karaːbɨn</ts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_870" n="HIAT:w" s="T235">miːnen</ts>
                  <nts id="Seg_871" n="HIAT:ip">,</nts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_874" n="HIAT:w" s="T236">taŋahɨn</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_877" n="HIAT:w" s="T237">taŋnan</ts>
                  <nts id="Seg_878" n="HIAT:ip">.</nts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T241" id="Seg_881" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_883" n="HIAT:w" s="T238">Kurum</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_886" n="HIAT:w" s="T239">bögö</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_889" n="HIAT:w" s="T240">bu͡olar</ts>
                  <nts id="Seg_890" n="HIAT:ip">.</nts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_893" n="HIAT:u" s="T241">
                  <ts e="T242" id="Seg_895" n="HIAT:w" s="T241">Karaːbɨnan</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_898" n="HIAT:w" s="T242">tönnöllör</ts>
                  <nts id="Seg_899" n="HIAT:ip">.</nts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_902" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_904" n="HIAT:w" s="T243">Hahɨl</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_907" n="HIAT:w" s="T244">albun</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_910" n="HIAT:w" s="T245">haŋarar</ts>
                  <nts id="Seg_911" n="HIAT:ip">:</nts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_914" n="HIAT:u" s="T246">
                  <nts id="Seg_915" n="HIAT:ip">"</nts>
                  <ts e="T247" id="Seg_917" n="HIAT:w" s="T246">Bu͡or</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_920" n="HIAT:w" s="T247">golomogor</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_923" n="HIAT:w" s="T248">toktoːbokko</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_926" n="HIAT:w" s="T249">aːhaːr</ts>
                  <nts id="Seg_927" n="HIAT:ip">,</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_930" n="HIAT:w" s="T250">atɨn</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_933" n="HIAT:w" s="T251">ɨraːktaːgɨ</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_936" n="HIAT:w" s="T252">holotu͡oj</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_939" n="HIAT:w" s="T253">gu͡oratɨgar</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_942" n="HIAT:w" s="T254">čugahaːn</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_945" n="HIAT:w" s="T255">baran</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_948" n="HIAT:w" s="T256">toktoːr</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_951" n="HIAT:w" s="T257">u͡onna</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_954" n="HIAT:w" s="T258">minigin</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_957" n="HIAT:w" s="T259">ɨŋɨrtaraːr</ts>
                  <nts id="Seg_958" n="HIAT:ip">.</nts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_961" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_963" n="HIAT:w" s="T260">Dʼonum-noru͡otum</ts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_966" n="HIAT:w" s="T261">töhö</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_969" n="HIAT:w" s="T262">berke</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_972" n="HIAT:w" s="T263">olororun</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_975" n="HIAT:w" s="T264">bilen</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_978" n="HIAT:w" s="T265">kel</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_981" n="HIAT:w" s="T266">di͡er</ts>
                  <nts id="Seg_982" n="HIAT:ip">,</nts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_985" n="HIAT:w" s="T267">mini͡eke</ts>
                  <nts id="Seg_986" n="HIAT:ip">.</nts>
                  <nts id="Seg_987" n="HIAT:ip">"</nts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_990" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_992" n="HIAT:w" s="T268">Hahɨl</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_995" n="HIAT:w" s="T269">eppitin</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_998" n="HIAT:w" s="T270">kurduk</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1001" n="HIAT:w" s="T271">u͡ol</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1004" n="HIAT:w" s="T272">bejetin</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1007" n="HIAT:w" s="T273">golomotun</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1010" n="HIAT:w" s="T274">aːhar</ts>
                  <nts id="Seg_1011" n="HIAT:ip">.</nts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_1014" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_1016" n="HIAT:w" s="T275">Atɨn</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1019" n="HIAT:w" s="T276">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1022" n="HIAT:w" s="T277">holotu͡oj</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1025" n="HIAT:w" s="T278">gu͡oratɨgar</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1028" n="HIAT:w" s="T279">čugahɨːr</ts>
                  <nts id="Seg_1029" n="HIAT:ip">.</nts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_1032" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_1034" n="HIAT:w" s="T280">Toktoːt</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1037" n="HIAT:w" s="T281">turan</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1040" n="HIAT:w" s="T282">diːr</ts>
                  <nts id="Seg_1041" n="HIAT:ip">:</nts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1044" n="HIAT:u" s="T283">
                  <nts id="Seg_1045" n="HIAT:ip">"</nts>
                  <ts e="T284" id="Seg_1047" n="HIAT:w" s="T283">Dʼonum-noru͡otum</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1050" n="HIAT:w" s="T284">töhö</ts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1053" n="HIAT:w" s="T285">berke</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1056" n="HIAT:w" s="T286">olororun</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1059" n="HIAT:w" s="T287">bili͡em</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1062" n="HIAT:w" s="T288">ete</ts>
                  <nts id="Seg_1063" n="HIAT:ip">.</nts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_1066" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_1068" n="HIAT:w" s="T289">Bili</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1071" n="HIAT:w" s="T290">hahɨlɨm</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1074" n="HIAT:w" s="T291">kanna</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1077" n="HIAT:w" s="T292">barda</ts>
                  <nts id="Seg_1078" n="HIAT:ip">,</nts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1081" n="HIAT:w" s="T293">bilen</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1084" n="HIAT:w" s="T294">keli͡en</ts>
                  <nts id="Seg_1085" n="HIAT:ip">"</nts>
                  <nts id="Seg_1086" n="HIAT:ip">,</nts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1089" n="HIAT:w" s="T295">diːr</ts>
                  <nts id="Seg_1090" n="HIAT:ip">.</nts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1093" n="HIAT:u" s="T296">
                  <ts e="T297" id="Seg_1095" n="HIAT:w" s="T296">Hahɨl</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1098" n="HIAT:w" s="T297">albun</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1101" n="HIAT:w" s="T298">kelbitiger</ts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1104" n="HIAT:w" s="T299">haŋarar</ts>
                  <nts id="Seg_1105" n="HIAT:ip">.</nts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1108" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1110" n="HIAT:w" s="T300">Hahɨl</ts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1113" n="HIAT:w" s="T301">albun</ts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1116" n="HIAT:w" s="T302">baran</ts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1119" n="HIAT:w" s="T303">holotu͡oj</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1122" n="HIAT:w" s="T304">gu͡orat</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1125" n="HIAT:w" s="T305">ɨraːktaːgɨtɨgar</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1128" n="HIAT:w" s="T306">haŋarar</ts>
                  <nts id="Seg_1129" n="HIAT:ip">:</nts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1132" n="HIAT:u" s="T307">
                  <nts id="Seg_1133" n="HIAT:ip">"</nts>
                  <ts e="T308" id="Seg_1135" n="HIAT:w" s="T307">Etiŋ</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1138" n="HIAT:w" s="T308">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1141" n="HIAT:w" s="T309">ehigini</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1144" n="HIAT:w" s="T310">ehe</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1147" n="HIAT:w" s="T311">kelle</ts>
                  <nts id="Seg_1148" n="HIAT:ip">.</nts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_1151" n="HIAT:u" s="T312">
                  <ts e="T313" id="Seg_1153" n="HIAT:w" s="T312">Ku͡ota</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1156" n="HIAT:w" s="T313">oksuŋ</ts>
                  <nts id="Seg_1157" n="HIAT:ip">.</nts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T322" id="Seg_1160" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1162" n="HIAT:w" s="T314">Iti</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1165" n="HIAT:w" s="T315">tɨ͡aga</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1168" n="HIAT:w" s="T316">taksan</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1171" n="HIAT:w" s="T317">hahɨŋ</ts>
                  <nts id="Seg_1172" n="HIAT:ip">,</nts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1175" n="HIAT:w" s="T318">töbögüt</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1178" n="HIAT:w" s="T319">ere</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1181" n="HIAT:w" s="T320">köstör</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1184" n="HIAT:w" s="T321">kurduk</ts>
                  <nts id="Seg_1185" n="HIAT:ip">.</nts>
                  <nts id="Seg_1186" n="HIAT:ip">"</nts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T332" id="Seg_1189" n="HIAT:u" s="T322">
                  <ts e="T323" id="Seg_1191" n="HIAT:w" s="T322">Holotu͡oj</ts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1194" n="HIAT:w" s="T323">gu͡orat</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1197" n="HIAT:w" s="T324">ɨraːktaːgɨtɨn</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1200" n="HIAT:w" s="T325">noru͡ota</ts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1203" n="HIAT:w" s="T326">barɨ</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1206" n="HIAT:w" s="T327">tɨ͡aga</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1209" n="HIAT:w" s="T328">ku͡otar</ts>
                  <nts id="Seg_1210" n="HIAT:ip">,</nts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1213" n="HIAT:w" s="T329">Etiŋ</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1216" n="HIAT:w" s="T330">ɨraːktaːgɨttan</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1219" n="HIAT:w" s="T331">kuttanan</ts>
                  <nts id="Seg_1220" n="HIAT:ip">.</nts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T337" id="Seg_1223" n="HIAT:u" s="T332">
                  <ts e="T333" id="Seg_1225" n="HIAT:w" s="T332">Hahɨl</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1228" n="HIAT:w" s="T333">albun</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1231" n="HIAT:w" s="T334">u͡olga</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1234" n="HIAT:w" s="T335">ɨtaːn</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1237" n="HIAT:w" s="T336">keler</ts>
                  <nts id="Seg_1238" n="HIAT:ip">:</nts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1241" n="HIAT:u" s="T337">
                  <nts id="Seg_1242" n="HIAT:ip">"</nts>
                  <ts e="T338" id="Seg_1244" n="HIAT:w" s="T337">Dʼommutun</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1247" n="HIAT:w" s="T338">Abaːhɨ</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1250" n="HIAT:w" s="T339">ɨraːktaːgɨta</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1253" n="HIAT:w" s="T340">baraːbɨt</ts>
                  <nts id="Seg_1254" n="HIAT:ip">.</nts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T347" id="Seg_1257" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1259" n="HIAT:w" s="T341">Hi͡en-hi͡en</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1262" n="HIAT:w" s="T342">baran</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1265" n="HIAT:w" s="T343">ol</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1268" n="HIAT:w" s="T344">ojuːrga</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1271" n="HIAT:w" s="T345">haha</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1274" n="HIAT:w" s="T346">hɨtallar</ts>
                  <nts id="Seg_1275" n="HIAT:ip">.</nts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T350" id="Seg_1278" n="HIAT:u" s="T347">
                  <ts e="T348" id="Seg_1280" n="HIAT:w" s="T347">Kɨlɨŋŋɨn</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1283" n="HIAT:w" s="T348">kördös</ts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1285" n="HIAT:ip">—</nts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1288" n="HIAT:w" s="T349">etiŋneːtin</ts>
                  <nts id="Seg_1289" n="HIAT:ip">.</nts>
                  <nts id="Seg_1290" n="HIAT:ip">"</nts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T355" id="Seg_1293" n="HIAT:u" s="T350">
                  <ts e="T351" id="Seg_1295" n="HIAT:w" s="T350">U͡ol</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1298" n="HIAT:w" s="T351">kɨnnɨtɨttan</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1301" n="HIAT:w" s="T352">kördöhör</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1303" n="HIAT:ip">"</nts>
                  <ts e="T354" id="Seg_1305" n="HIAT:w" s="T353">etiŋneː</ts>
                  <nts id="Seg_1306" n="HIAT:ip">"</nts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1309" n="HIAT:w" s="T354">di͡en</ts>
                  <nts id="Seg_1310" n="HIAT:ip">.</nts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T368" id="Seg_1313" n="HIAT:u" s="T355">
                  <ts e="T356" id="Seg_1315" n="HIAT:w" s="T355">Etiŋ</ts>
                  <nts id="Seg_1316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1318" n="HIAT:w" s="T356">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1321" n="HIAT:w" s="T357">dʼe</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1324" n="HIAT:w" s="T358">etiŋneːn</ts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1327" n="HIAT:w" s="T359">haːjar</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1330" n="HIAT:w" s="T360">bili</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1333" n="HIAT:w" s="T361">ojuːru</ts>
                  <nts id="Seg_1334" n="HIAT:ip">,</nts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1337" n="HIAT:w" s="T362">holotu͡oj</ts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1340" n="HIAT:w" s="T363">gu͡orat</ts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1343" n="HIAT:w" s="T364">biːr</ts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1346" n="HIAT:w" s="T365">da</ts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1349" n="HIAT:w" s="T366">kihite</ts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1352" n="HIAT:w" s="T367">orpot</ts>
                  <nts id="Seg_1353" n="HIAT:ip">.</nts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1356" n="HIAT:u" s="T368">
                  <ts e="T369" id="Seg_1358" n="HIAT:w" s="T368">U͡ol</ts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1361" n="HIAT:w" s="T369">bili</ts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1364" n="HIAT:w" s="T370">holotu͡oj</ts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1367" n="HIAT:w" s="T371">gu͡orakka</ts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1370" n="HIAT:w" s="T372">kiːren</ts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1373" n="HIAT:w" s="T373">oloksujar</ts>
                  <nts id="Seg_1374" n="HIAT:ip">,</nts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1377" n="HIAT:w" s="T374">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1380" n="HIAT:w" s="T375">bu͡olar</ts>
                  <nts id="Seg_1381" n="HIAT:ip">.</nts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1384" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1386" n="HIAT:w" s="T376">Etiŋ</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1389" n="HIAT:w" s="T377">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1392" n="HIAT:w" s="T378">kɨːhɨn</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1395" n="HIAT:w" s="T379">ɨllaga</ts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1398" n="HIAT:w" s="T380">ol</ts>
                  <nts id="Seg_1399" n="HIAT:ip">.</nts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T382" id="Seg_1402" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1404" n="HIAT:w" s="T381">Elete</ts>
                  <nts id="Seg_1405" n="HIAT:ip">.</nts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T382" id="Seg_1407" n="sc" s="T0">
               <ts e="T1" id="Seg_1409" n="e" s="T0">Umnahɨt </ts>
               <ts e="T2" id="Seg_1411" n="e" s="T1">paːsɨnaj </ts>
               <ts e="T3" id="Seg_1413" n="e" s="T2">olorbut. </ts>
               <ts e="T4" id="Seg_1415" n="e" s="T3">Bu </ts>
               <ts e="T5" id="Seg_1417" n="e" s="T4">paːsɨnaj </ts>
               <ts e="T6" id="Seg_1419" n="e" s="T5">ölbütüger </ts>
               <ts e="T7" id="Seg_1421" n="e" s="T6">bu͡or </ts>
               <ts e="T8" id="Seg_1423" n="e" s="T7">golomotugar </ts>
               <ts e="T9" id="Seg_1425" n="e" s="T8">u͡ol </ts>
               <ts e="T10" id="Seg_1427" n="e" s="T9">ogoto </ts>
               <ts e="T11" id="Seg_1429" n="e" s="T10">tulaːjak </ts>
               <ts e="T12" id="Seg_1431" n="e" s="T11">kaːlar. </ts>
               <ts e="T13" id="Seg_1433" n="e" s="T12">Bu </ts>
               <ts e="T14" id="Seg_1435" n="e" s="T13">ogo </ts>
               <ts e="T15" id="Seg_1437" n="e" s="T14">aččɨktaːn </ts>
               <ts e="T16" id="Seg_1439" n="e" s="T15">ölörö </ts>
               <ts e="T17" id="Seg_1441" n="e" s="T16">bu͡olar. </ts>
               <ts e="T18" id="Seg_1443" n="e" s="T17">Bunu </ts>
               <ts e="T19" id="Seg_1445" n="e" s="T18">hahɨl </ts>
               <ts e="T20" id="Seg_1447" n="e" s="T19">bular. </ts>
               <ts e="T21" id="Seg_1449" n="e" s="T20">"Min </ts>
               <ts e="T22" id="Seg_1451" n="e" s="T21">enʼigin </ts>
               <ts e="T23" id="Seg_1453" n="e" s="T22">kihi </ts>
               <ts e="T24" id="Seg_1455" n="e" s="T23">oŋoru͡om", </ts>
               <ts e="T25" id="Seg_1457" n="e" s="T24">diːr. </ts>
               <ts e="T26" id="Seg_1459" n="e" s="T25">"Baːj </ts>
               <ts e="T27" id="Seg_1461" n="e" s="T26">kihi </ts>
               <ts e="T28" id="Seg_1463" n="e" s="T27">bu͡olu͡oŋ, </ts>
               <ts e="T29" id="Seg_1465" n="e" s="T28">baːj </ts>
               <ts e="T30" id="Seg_1467" n="e" s="T29">dʼaktarɨ </ts>
               <ts e="T31" id="Seg_1469" n="e" s="T30">dʼaktar </ts>
               <ts e="T32" id="Seg_1471" n="e" s="T31">ɨlan </ts>
               <ts e="T33" id="Seg_1473" n="e" s="T32">bi͡eri͡em." </ts>
               <ts e="T34" id="Seg_1475" n="e" s="T33">Innʼe </ts>
               <ts e="T35" id="Seg_1477" n="e" s="T34">di͡en </ts>
               <ts e="T36" id="Seg_1479" n="e" s="T35">baran </ts>
               <ts e="T37" id="Seg_1481" n="e" s="T36">hahɨl </ts>
               <ts e="T38" id="Seg_1483" n="e" s="T37">Etiŋ </ts>
               <ts e="T39" id="Seg_1485" n="e" s="T38">ɨraːktaːgɨtɨgar </ts>
               <ts e="T40" id="Seg_1487" n="e" s="T39">barar. </ts>
               <ts e="T41" id="Seg_1489" n="e" s="T40">Etiŋ </ts>
               <ts e="T42" id="Seg_1491" n="e" s="T41">ɨraːktaːgɨtɨgar </ts>
               <ts e="T43" id="Seg_1493" n="e" s="T42">tijen </ts>
               <ts e="T44" id="Seg_1495" n="e" s="T43">diːr: </ts>
               <ts e="T45" id="Seg_1497" n="e" s="T44">Aːttaːk </ts>
               <ts e="T46" id="Seg_1499" n="e" s="T45">baːj </ts>
               <ts e="T47" id="Seg_1501" n="e" s="T46">u͡ol </ts>
               <ts e="T48" id="Seg_1503" n="e" s="T47">kihi </ts>
               <ts e="T49" id="Seg_1505" n="e" s="T48">baːr. </ts>
               <ts e="T50" id="Seg_1507" n="e" s="T49">Gini </ts>
               <ts e="T51" id="Seg_1509" n="e" s="T50">en </ts>
               <ts e="T52" id="Seg_1511" n="e" s="T51">kɨːskɨn </ts>
               <ts e="T53" id="Seg_1513" n="e" s="T52">hu͡orumnʼulata </ts>
               <ts e="T54" id="Seg_1515" n="e" s="T53">ɨːtta. </ts>
               <ts e="T55" id="Seg_1517" n="e" s="T54">Kɨːhɨŋ </ts>
               <ts e="T56" id="Seg_1519" n="e" s="T55">huluːtun </ts>
               <ts e="T57" id="Seg_1521" n="e" s="T56">tɨ͡a </ts>
               <ts e="T58" id="Seg_1523" n="e" s="T57">kɨːlɨn </ts>
               <ts e="T59" id="Seg_1525" n="e" s="T58">bastɨŋɨn, </ts>
               <ts e="T60" id="Seg_1527" n="e" s="T59">araːhɨn </ts>
               <ts e="T61" id="Seg_1529" n="e" s="T60">ɨːtɨ͡ak </ts>
               <ts e="T62" id="Seg_1531" n="e" s="T61">bu͡olla. </ts>
               <ts e="T63" id="Seg_1533" n="e" s="T62">Ol </ts>
               <ts e="T64" id="Seg_1535" n="e" s="T63">kɨːllarɨ </ts>
               <ts e="T65" id="Seg_1537" n="e" s="T64">tus </ts>
               <ts e="T66" id="Seg_1539" n="e" s="T65">tuhunan </ts>
               <ts e="T67" id="Seg_1541" n="e" s="T66">dʼi͡ege </ts>
               <ts e="T68" id="Seg_1543" n="e" s="T67">as </ts>
               <ts e="T69" id="Seg_1545" n="e" s="T68">belemneːn </ts>
               <ts e="T70" id="Seg_1547" n="e" s="T69">tohuju͡oŋ </ts>
               <ts e="T71" id="Seg_1549" n="e" s="T70">ühü", </ts>
               <ts e="T72" id="Seg_1551" n="e" s="T71">diːr. </ts>
               <ts e="T73" id="Seg_1553" n="e" s="T72">Etiŋ </ts>
               <ts e="T74" id="Seg_1555" n="e" s="T73">ɨraːktaːgɨ </ts>
               <ts e="T75" id="Seg_1557" n="e" s="T74">höbülemmit. </ts>
               <ts e="T76" id="Seg_1559" n="e" s="T75">Hahɨl </ts>
               <ts e="T77" id="Seg_1561" n="e" s="T76">albun </ts>
               <ts e="T78" id="Seg_1563" n="e" s="T77">töttörü </ts>
               <ts e="T79" id="Seg_1565" n="e" s="T78">hüːrbüt. </ts>
               <ts e="T80" id="Seg_1567" n="e" s="T79">Tɨ͡a </ts>
               <ts e="T81" id="Seg_1569" n="e" s="T80">kɨːllarɨn </ts>
               <ts e="T82" id="Seg_1571" n="e" s="T81">araːhɨn </ts>
               <ts e="T83" id="Seg_1573" n="e" s="T82">körsön </ts>
               <ts e="T84" id="Seg_1575" n="e" s="T83">haŋarar: </ts>
               <ts e="T85" id="Seg_1577" n="e" s="T84">"Etiŋ </ts>
               <ts e="T86" id="Seg_1579" n="e" s="T85">ɨraːktaːgɨ </ts>
               <ts e="T87" id="Seg_1581" n="e" s="T86">baːja-toto </ts>
               <ts e="T88" id="Seg_1583" n="e" s="T87">batɨmɨja </ts>
               <ts e="T89" id="Seg_1585" n="e" s="T88">ehi͡eke </ts>
               <ts e="T90" id="Seg_1587" n="e" s="T89">as </ts>
               <ts e="T91" id="Seg_1589" n="e" s="T90">terijde. </ts>
               <ts e="T92" id="Seg_1591" n="e" s="T91">Onu </ts>
               <ts e="T93" id="Seg_1593" n="e" s="T92">baran </ts>
               <ts e="T94" id="Seg_1595" n="e" s="T93">ahɨːr </ts>
               <ts e="T95" id="Seg_1597" n="e" s="T94">ühügüt." </ts>
               <ts e="T96" id="Seg_1599" n="e" s="T95">Kɨːl </ts>
               <ts e="T97" id="Seg_1601" n="e" s="T96">bögö </ts>
               <ts e="T98" id="Seg_1603" n="e" s="T97">aːlin </ts>
               <ts e="T99" id="Seg_1605" n="e" s="T98">bu͡olan </ts>
               <ts e="T100" id="Seg_1607" n="e" s="T99">tijeller </ts>
               <ts e="T101" id="Seg_1609" n="e" s="T100">ahɨː. </ts>
               <ts e="T102" id="Seg_1611" n="e" s="T101">Etiŋ </ts>
               <ts e="T103" id="Seg_1613" n="e" s="T102">ɨraːktaːgɨ </ts>
               <ts e="T104" id="Seg_1615" n="e" s="T103">ü͡örer, </ts>
               <ts e="T105" id="Seg_1617" n="e" s="T104">"kütü͡ötüm </ts>
               <ts e="T106" id="Seg_1619" n="e" s="T105">baːj </ts>
               <ts e="T107" id="Seg_1621" n="e" s="T106">kihi </ts>
               <ts e="T108" id="Seg_1623" n="e" s="T107">ebit" </ts>
               <ts e="T109" id="Seg_1625" n="e" s="T108">diːr. </ts>
               <ts e="T110" id="Seg_1627" n="e" s="T109">Kɨːllar </ts>
               <ts e="T111" id="Seg_1629" n="e" s="T110">ahɨː </ts>
               <ts e="T112" id="Seg_1631" n="e" s="T111">turdaktarɨna, </ts>
               <ts e="T113" id="Seg_1633" n="e" s="T112">bili </ts>
               <ts e="T114" id="Seg_1635" n="e" s="T113">dʼi͡eleri </ts>
               <ts e="T115" id="Seg_1637" n="e" s="T114">kataːn </ts>
               <ts e="T116" id="Seg_1639" n="e" s="T115">kebiheller. </ts>
               <ts e="T117" id="Seg_1641" n="e" s="T116">Hahɨl </ts>
               <ts e="T118" id="Seg_1643" n="e" s="T117">albun </ts>
               <ts e="T119" id="Seg_1645" n="e" s="T118">Etiŋ </ts>
               <ts e="T120" id="Seg_1647" n="e" s="T119">ɨraːktaːgɨga </ts>
               <ts e="T121" id="Seg_1649" n="e" s="T120">tijer. </ts>
               <ts e="T122" id="Seg_1651" n="e" s="T121">"Kaja, </ts>
               <ts e="T123" id="Seg_1653" n="e" s="T122">kɨːllar </ts>
               <ts e="T124" id="Seg_1655" n="e" s="T123">kelliler </ts>
               <ts e="T125" id="Seg_1657" n="e" s="T124">dʼürü?" </ts>
               <ts e="T126" id="Seg_1659" n="e" s="T125">"Kelliler." </ts>
               <ts e="T127" id="Seg_1661" n="e" s="T126">"Kütü͡öt </ts>
               <ts e="T128" id="Seg_1663" n="e" s="T127">u͡ol </ts>
               <ts e="T129" id="Seg_1665" n="e" s="T128">boku͡oja </ts>
               <ts e="T130" id="Seg_1667" n="e" s="T129">hu͡ok. </ts>
               <ts e="T131" id="Seg_1669" n="e" s="T130">Hotoru </ts>
               <ts e="T132" id="Seg_1671" n="e" s="T131">holotu͡oj </ts>
               <ts e="T133" id="Seg_1673" n="e" s="T132">karaːbɨnan </ts>
               <ts e="T134" id="Seg_1675" n="e" s="T133">keli͡e", </ts>
               <ts e="T135" id="Seg_1677" n="e" s="T134">diːr. </ts>
               <ts e="T136" id="Seg_1679" n="e" s="T135">U͡olugar </ts>
               <ts e="T137" id="Seg_1681" n="e" s="T136">töttörü </ts>
               <ts e="T138" id="Seg_1683" n="e" s="T137">hüːren </ts>
               <ts e="T139" id="Seg_1685" n="e" s="T138">tijer. </ts>
               <ts e="T140" id="Seg_1687" n="e" s="T139">Talagɨnan </ts>
               <ts e="T141" id="Seg_1689" n="e" s="T140">kuhagan </ts>
               <ts e="T142" id="Seg_1691" n="e" s="T141">bolu͡ot </ts>
               <ts e="T143" id="Seg_1693" n="e" s="T142">oŋortoror. </ts>
               <ts e="T144" id="Seg_1695" n="e" s="T143">Araːs </ts>
               <ts e="T145" id="Seg_1697" n="e" s="T144">hir </ts>
               <ts e="T146" id="Seg_1699" n="e" s="T145">hibekkitinen </ts>
               <ts e="T147" id="Seg_1701" n="e" s="T146">himeter, </ts>
               <ts e="T148" id="Seg_1703" n="e" s="T147">ɨraːktan </ts>
               <ts e="T149" id="Seg_1705" n="e" s="T148">hi͡ederej, </ts>
               <ts e="T150" id="Seg_1707" n="e" s="T149">basku͡oj </ts>
               <ts e="T151" id="Seg_1709" n="e" s="T150">bu͡olan </ts>
               <ts e="T152" id="Seg_1711" n="e" s="T151">köstör. </ts>
               <ts e="T153" id="Seg_1713" n="e" s="T152">Tɨ͡al </ts>
               <ts e="T154" id="Seg_1715" n="e" s="T153">kün </ts>
               <ts e="T155" id="Seg_1717" n="e" s="T154">Etiŋ </ts>
               <ts e="T156" id="Seg_1719" n="e" s="T155">ɨraːktaːgɨga </ts>
               <ts e="T157" id="Seg_1721" n="e" s="T156">ustallar. </ts>
               <ts e="T158" id="Seg_1723" n="e" s="T157">Etiŋ </ts>
               <ts e="T159" id="Seg_1725" n="e" s="T158">ɨraːktaːgɨ </ts>
               <ts e="T160" id="Seg_1727" n="e" s="T159">körbüte, </ts>
               <ts e="T161" id="Seg_1729" n="e" s="T160">bajgal </ts>
               <ts e="T162" id="Seg_1731" n="e" s="T161">ustun </ts>
               <ts e="T163" id="Seg_1733" n="e" s="T162">holotu͡oj </ts>
               <ts e="T164" id="Seg_1735" n="e" s="T163">karaːp </ts>
               <ts e="T165" id="Seg_1737" n="e" s="T164">iher, </ts>
               <ts e="T166" id="Seg_1739" n="e" s="T165">hi͡ederej </ts>
               <ts e="T167" id="Seg_1741" n="e" s="T166">öŋnöːk. </ts>
               <ts e="T168" id="Seg_1743" n="e" s="T167">ɨraːktaːgɨ </ts>
               <ts e="T169" id="Seg_1745" n="e" s="T168">ü͡örer. </ts>
               <ts e="T170" id="Seg_1747" n="e" s="T169">Tɨ͡al </ts>
               <ts e="T171" id="Seg_1749" n="e" s="T170">tüspütüger </ts>
               <ts e="T172" id="Seg_1751" n="e" s="T171">bolu͡ot </ts>
               <ts e="T173" id="Seg_1753" n="e" s="T172">ɨhɨllan </ts>
               <ts e="T174" id="Seg_1755" n="e" s="T173">kaːlar. </ts>
               <ts e="T175" id="Seg_1757" n="e" s="T174">Hahɨl </ts>
               <ts e="T176" id="Seg_1759" n="e" s="T175">albun </ts>
               <ts e="T177" id="Seg_1761" n="e" s="T176">Etiŋ </ts>
               <ts e="T178" id="Seg_1763" n="e" s="T177">ɨraːktaːgɨga </ts>
               <ts e="T179" id="Seg_1765" n="e" s="T178">hüːren </ts>
               <ts e="T180" id="Seg_1767" n="e" s="T179">tijer, </ts>
               <ts e="T181" id="Seg_1769" n="e" s="T180">diːr: </ts>
               <ts e="T182" id="Seg_1771" n="e" s="T181">"i͡edeːn </ts>
               <ts e="T183" id="Seg_1773" n="e" s="T182">bu͡olla, </ts>
               <ts e="T184" id="Seg_1775" n="e" s="T183">holotu͡oj </ts>
               <ts e="T185" id="Seg_1777" n="e" s="T184">karaːp </ts>
               <ts e="T186" id="Seg_1779" n="e" s="T185">timirde. </ts>
               <ts e="T187" id="Seg_1781" n="e" s="T186">Kütü͡ötüŋ </ts>
               <ts e="T188" id="Seg_1783" n="e" s="T187">kihi </ts>
               <ts e="T189" id="Seg_1785" n="e" s="T188">berde </ts>
               <ts e="T190" id="Seg_1787" n="e" s="T189">bu͡olan </ts>
               <ts e="T191" id="Seg_1789" n="e" s="T190">orto, </ts>
               <ts e="T192" id="Seg_1791" n="e" s="T191">tahagaha </ts>
               <ts e="T193" id="Seg_1793" n="e" s="T192">barɨta </ts>
               <ts e="T194" id="Seg_1795" n="e" s="T193">uːga </ts>
               <ts e="T195" id="Seg_1797" n="e" s="T194">barda, </ts>
               <ts e="T196" id="Seg_1799" n="e" s="T195">bejete </ts>
               <ts e="T197" id="Seg_1801" n="e" s="T196">hɨgɨnnʼak </ts>
               <ts e="T198" id="Seg_1803" n="e" s="T197">kaːlla. </ts>
               <ts e="T199" id="Seg_1805" n="e" s="T198">Taŋasta </ts>
               <ts e="T200" id="Seg_1807" n="e" s="T199">ɨːt." </ts>
               <ts e="T201" id="Seg_1809" n="e" s="T200">Etiŋ </ts>
               <ts e="T202" id="Seg_1811" n="e" s="T201">ɨraːktaːgɨ </ts>
               <ts e="T203" id="Seg_1813" n="e" s="T202">u͡olga </ts>
               <ts e="T204" id="Seg_1815" n="e" s="T203">bastɨŋ </ts>
               <ts e="T205" id="Seg_1817" n="e" s="T204">taŋahɨ, </ts>
               <ts e="T206" id="Seg_1819" n="e" s="T205">bejetin </ts>
               <ts e="T207" id="Seg_1821" n="e" s="T206">karaːbɨn </ts>
               <ts e="T208" id="Seg_1823" n="e" s="T207">ɨːtar. </ts>
               <ts e="T209" id="Seg_1825" n="e" s="T208">Hahɨl </ts>
               <ts e="T210" id="Seg_1827" n="e" s="T209">albun </ts>
               <ts e="T211" id="Seg_1829" n="e" s="T210">u͡olun </ts>
               <ts e="T212" id="Seg_1831" n="e" s="T211">hübeliːr: </ts>
               <ts e="T213" id="Seg_1833" n="e" s="T212">"ɨraːktaːgɨga </ts>
               <ts e="T214" id="Seg_1835" n="e" s="T213">tijeŋŋin </ts>
               <ts e="T215" id="Seg_1837" n="e" s="T214">taŋaskɨn </ts>
               <ts e="T216" id="Seg_1839" n="e" s="T215">körünüme, </ts>
               <ts e="T217" id="Seg_1841" n="e" s="T216">oduːrgu͡oktara. </ts>
               <ts e="T218" id="Seg_1843" n="e" s="T217">Toŋkos </ts>
               <ts e="T219" id="Seg_1845" n="e" s="T218">ere </ts>
               <ts e="T220" id="Seg_1847" n="e" s="T219">gɨnan </ts>
               <ts e="T221" id="Seg_1849" n="e" s="T220">doroːbolos. </ts>
               <ts e="T222" id="Seg_1851" n="e" s="T221">Ahɨːrgar </ts>
               <ts e="T223" id="Seg_1853" n="e" s="T222">dʼon </ts>
               <ts e="T224" id="Seg_1855" n="e" s="T223">ahɨːrɨn </ts>
               <ts e="T225" id="Seg_1857" n="e" s="T224">körön </ts>
               <ts e="T226" id="Seg_1859" n="e" s="T225">ahaː, </ts>
               <ts e="T227" id="Seg_1861" n="e" s="T226">arɨgɨnɨ </ts>
               <ts e="T228" id="Seg_1863" n="e" s="T227">bert </ts>
               <ts e="T229" id="Seg_1865" n="e" s="T228">kɨratɨk </ts>
               <ts e="T230" id="Seg_1867" n="e" s="T229">is." </ts>
               <ts e="T231" id="Seg_1869" n="e" s="T230">U͡ol </ts>
               <ts e="T232" id="Seg_1871" n="e" s="T231">Etiŋ </ts>
               <ts e="T233" id="Seg_1873" n="e" s="T232">ɨraːktaːgɨga </ts>
               <ts e="T234" id="Seg_1875" n="e" s="T233">tijer </ts>
               <ts e="T235" id="Seg_1877" n="e" s="T234">karaːbɨn </ts>
               <ts e="T236" id="Seg_1879" n="e" s="T235">miːnen, </ts>
               <ts e="T237" id="Seg_1881" n="e" s="T236">taŋahɨn </ts>
               <ts e="T238" id="Seg_1883" n="e" s="T237">taŋnan. </ts>
               <ts e="T239" id="Seg_1885" n="e" s="T238">Kurum </ts>
               <ts e="T240" id="Seg_1887" n="e" s="T239">bögö </ts>
               <ts e="T241" id="Seg_1889" n="e" s="T240">bu͡olar. </ts>
               <ts e="T242" id="Seg_1891" n="e" s="T241">Karaːbɨnan </ts>
               <ts e="T243" id="Seg_1893" n="e" s="T242">tönnöllör. </ts>
               <ts e="T244" id="Seg_1895" n="e" s="T243">Hahɨl </ts>
               <ts e="T245" id="Seg_1897" n="e" s="T244">albun </ts>
               <ts e="T246" id="Seg_1899" n="e" s="T245">haŋarar: </ts>
               <ts e="T247" id="Seg_1901" n="e" s="T246">"Bu͡or </ts>
               <ts e="T248" id="Seg_1903" n="e" s="T247">golomogor </ts>
               <ts e="T249" id="Seg_1905" n="e" s="T248">toktoːbokko </ts>
               <ts e="T250" id="Seg_1907" n="e" s="T249">aːhaːr, </ts>
               <ts e="T251" id="Seg_1909" n="e" s="T250">atɨn </ts>
               <ts e="T252" id="Seg_1911" n="e" s="T251">ɨraːktaːgɨ </ts>
               <ts e="T253" id="Seg_1913" n="e" s="T252">holotu͡oj </ts>
               <ts e="T254" id="Seg_1915" n="e" s="T253">gu͡oratɨgar </ts>
               <ts e="T255" id="Seg_1917" n="e" s="T254">čugahaːn </ts>
               <ts e="T256" id="Seg_1919" n="e" s="T255">baran </ts>
               <ts e="T257" id="Seg_1921" n="e" s="T256">toktoːr </ts>
               <ts e="T258" id="Seg_1923" n="e" s="T257">u͡onna </ts>
               <ts e="T259" id="Seg_1925" n="e" s="T258">minigin </ts>
               <ts e="T260" id="Seg_1927" n="e" s="T259">ɨŋɨrtaraːr. </ts>
               <ts e="T261" id="Seg_1929" n="e" s="T260">Dʼonum-noru͡otum </ts>
               <ts e="T262" id="Seg_1931" n="e" s="T261">töhö </ts>
               <ts e="T263" id="Seg_1933" n="e" s="T262">berke </ts>
               <ts e="T264" id="Seg_1935" n="e" s="T263">olororun </ts>
               <ts e="T265" id="Seg_1937" n="e" s="T264">bilen </ts>
               <ts e="T266" id="Seg_1939" n="e" s="T265">kel </ts>
               <ts e="T267" id="Seg_1941" n="e" s="T266">di͡er, </ts>
               <ts e="T268" id="Seg_1943" n="e" s="T267">mini͡eke." </ts>
               <ts e="T269" id="Seg_1945" n="e" s="T268">Hahɨl </ts>
               <ts e="T270" id="Seg_1947" n="e" s="T269">eppitin </ts>
               <ts e="T271" id="Seg_1949" n="e" s="T270">kurduk </ts>
               <ts e="T272" id="Seg_1951" n="e" s="T271">u͡ol </ts>
               <ts e="T273" id="Seg_1953" n="e" s="T272">bejetin </ts>
               <ts e="T274" id="Seg_1955" n="e" s="T273">golomotun </ts>
               <ts e="T275" id="Seg_1957" n="e" s="T274">aːhar. </ts>
               <ts e="T276" id="Seg_1959" n="e" s="T275">Atɨn </ts>
               <ts e="T277" id="Seg_1961" n="e" s="T276">ɨraːktaːgɨ </ts>
               <ts e="T278" id="Seg_1963" n="e" s="T277">holotu͡oj </ts>
               <ts e="T279" id="Seg_1965" n="e" s="T278">gu͡oratɨgar </ts>
               <ts e="T280" id="Seg_1967" n="e" s="T279">čugahɨːr. </ts>
               <ts e="T281" id="Seg_1969" n="e" s="T280">Toktoːt </ts>
               <ts e="T282" id="Seg_1971" n="e" s="T281">turan </ts>
               <ts e="T283" id="Seg_1973" n="e" s="T282">diːr: </ts>
               <ts e="T284" id="Seg_1975" n="e" s="T283">"Dʼonum-noru͡otum </ts>
               <ts e="T285" id="Seg_1977" n="e" s="T284">töhö </ts>
               <ts e="T286" id="Seg_1979" n="e" s="T285">berke </ts>
               <ts e="T287" id="Seg_1981" n="e" s="T286">olororun </ts>
               <ts e="T288" id="Seg_1983" n="e" s="T287">bili͡em </ts>
               <ts e="T289" id="Seg_1985" n="e" s="T288">ete. </ts>
               <ts e="T290" id="Seg_1987" n="e" s="T289">Bili </ts>
               <ts e="T291" id="Seg_1989" n="e" s="T290">hahɨlɨm </ts>
               <ts e="T292" id="Seg_1991" n="e" s="T291">kanna </ts>
               <ts e="T293" id="Seg_1993" n="e" s="T292">barda, </ts>
               <ts e="T294" id="Seg_1995" n="e" s="T293">bilen </ts>
               <ts e="T295" id="Seg_1997" n="e" s="T294">keli͡en", </ts>
               <ts e="T296" id="Seg_1999" n="e" s="T295">diːr. </ts>
               <ts e="T297" id="Seg_2001" n="e" s="T296">Hahɨl </ts>
               <ts e="T298" id="Seg_2003" n="e" s="T297">albun </ts>
               <ts e="T299" id="Seg_2005" n="e" s="T298">kelbitiger </ts>
               <ts e="T300" id="Seg_2007" n="e" s="T299">haŋarar. </ts>
               <ts e="T301" id="Seg_2009" n="e" s="T300">Hahɨl </ts>
               <ts e="T302" id="Seg_2011" n="e" s="T301">albun </ts>
               <ts e="T303" id="Seg_2013" n="e" s="T302">baran </ts>
               <ts e="T304" id="Seg_2015" n="e" s="T303">holotu͡oj </ts>
               <ts e="T305" id="Seg_2017" n="e" s="T304">gu͡orat </ts>
               <ts e="T306" id="Seg_2019" n="e" s="T305">ɨraːktaːgɨtɨgar </ts>
               <ts e="T307" id="Seg_2021" n="e" s="T306">haŋarar: </ts>
               <ts e="T308" id="Seg_2023" n="e" s="T307">"Etiŋ </ts>
               <ts e="T309" id="Seg_2025" n="e" s="T308">ɨraːktaːgɨ </ts>
               <ts e="T310" id="Seg_2027" n="e" s="T309">ehigini </ts>
               <ts e="T311" id="Seg_2029" n="e" s="T310">ehe </ts>
               <ts e="T312" id="Seg_2031" n="e" s="T311">kelle. </ts>
               <ts e="T313" id="Seg_2033" n="e" s="T312">Ku͡ota </ts>
               <ts e="T314" id="Seg_2035" n="e" s="T313">oksuŋ. </ts>
               <ts e="T315" id="Seg_2037" n="e" s="T314">Iti </ts>
               <ts e="T316" id="Seg_2039" n="e" s="T315">tɨ͡aga </ts>
               <ts e="T317" id="Seg_2041" n="e" s="T316">taksan </ts>
               <ts e="T318" id="Seg_2043" n="e" s="T317">hahɨŋ, </ts>
               <ts e="T319" id="Seg_2045" n="e" s="T318">töbögüt </ts>
               <ts e="T320" id="Seg_2047" n="e" s="T319">ere </ts>
               <ts e="T321" id="Seg_2049" n="e" s="T320">köstör </ts>
               <ts e="T322" id="Seg_2051" n="e" s="T321">kurduk." </ts>
               <ts e="T323" id="Seg_2053" n="e" s="T322">Holotu͡oj </ts>
               <ts e="T324" id="Seg_2055" n="e" s="T323">gu͡orat </ts>
               <ts e="T325" id="Seg_2057" n="e" s="T324">ɨraːktaːgɨtɨn </ts>
               <ts e="T326" id="Seg_2059" n="e" s="T325">noru͡ota </ts>
               <ts e="T327" id="Seg_2061" n="e" s="T326">barɨ </ts>
               <ts e="T328" id="Seg_2063" n="e" s="T327">tɨ͡aga </ts>
               <ts e="T329" id="Seg_2065" n="e" s="T328">ku͡otar, </ts>
               <ts e="T330" id="Seg_2067" n="e" s="T329">Etiŋ </ts>
               <ts e="T331" id="Seg_2069" n="e" s="T330">ɨraːktaːgɨttan </ts>
               <ts e="T332" id="Seg_2071" n="e" s="T331">kuttanan. </ts>
               <ts e="T333" id="Seg_2073" n="e" s="T332">Hahɨl </ts>
               <ts e="T334" id="Seg_2075" n="e" s="T333">albun </ts>
               <ts e="T335" id="Seg_2077" n="e" s="T334">u͡olga </ts>
               <ts e="T336" id="Seg_2079" n="e" s="T335">ɨtaːn </ts>
               <ts e="T337" id="Seg_2081" n="e" s="T336">keler: </ts>
               <ts e="T338" id="Seg_2083" n="e" s="T337">"Dʼommutun </ts>
               <ts e="T339" id="Seg_2085" n="e" s="T338">Abaːhɨ </ts>
               <ts e="T340" id="Seg_2087" n="e" s="T339">ɨraːktaːgɨta </ts>
               <ts e="T341" id="Seg_2089" n="e" s="T340">baraːbɨt. </ts>
               <ts e="T342" id="Seg_2091" n="e" s="T341">Hi͡en-hi͡en </ts>
               <ts e="T343" id="Seg_2093" n="e" s="T342">baran </ts>
               <ts e="T344" id="Seg_2095" n="e" s="T343">ol </ts>
               <ts e="T345" id="Seg_2097" n="e" s="T344">ojuːrga </ts>
               <ts e="T346" id="Seg_2099" n="e" s="T345">haha </ts>
               <ts e="T347" id="Seg_2101" n="e" s="T346">hɨtallar. </ts>
               <ts e="T348" id="Seg_2103" n="e" s="T347">Kɨlɨŋŋɨn </ts>
               <ts e="T349" id="Seg_2105" n="e" s="T348">kördös — </ts>
               <ts e="T350" id="Seg_2107" n="e" s="T349">etiŋneːtin." </ts>
               <ts e="T351" id="Seg_2109" n="e" s="T350">U͡ol </ts>
               <ts e="T352" id="Seg_2111" n="e" s="T351">kɨnnɨtɨttan </ts>
               <ts e="T353" id="Seg_2113" n="e" s="T352">kördöhör </ts>
               <ts e="T354" id="Seg_2115" n="e" s="T353">"etiŋneː" </ts>
               <ts e="T355" id="Seg_2117" n="e" s="T354">di͡en. </ts>
               <ts e="T356" id="Seg_2119" n="e" s="T355">Etiŋ </ts>
               <ts e="T357" id="Seg_2121" n="e" s="T356">ɨraːktaːgɨ </ts>
               <ts e="T358" id="Seg_2123" n="e" s="T357">dʼe </ts>
               <ts e="T359" id="Seg_2125" n="e" s="T358">etiŋneːn </ts>
               <ts e="T360" id="Seg_2127" n="e" s="T359">haːjar </ts>
               <ts e="T361" id="Seg_2129" n="e" s="T360">bili </ts>
               <ts e="T362" id="Seg_2131" n="e" s="T361">ojuːru, </ts>
               <ts e="T363" id="Seg_2133" n="e" s="T362">holotu͡oj </ts>
               <ts e="T364" id="Seg_2135" n="e" s="T363">gu͡orat </ts>
               <ts e="T365" id="Seg_2137" n="e" s="T364">biːr </ts>
               <ts e="T366" id="Seg_2139" n="e" s="T365">da </ts>
               <ts e="T367" id="Seg_2141" n="e" s="T366">kihite </ts>
               <ts e="T368" id="Seg_2143" n="e" s="T367">orpot. </ts>
               <ts e="T369" id="Seg_2145" n="e" s="T368">U͡ol </ts>
               <ts e="T370" id="Seg_2147" n="e" s="T369">bili </ts>
               <ts e="T371" id="Seg_2149" n="e" s="T370">holotu͡oj </ts>
               <ts e="T372" id="Seg_2151" n="e" s="T371">gu͡orakka </ts>
               <ts e="T373" id="Seg_2153" n="e" s="T372">kiːren </ts>
               <ts e="T374" id="Seg_2155" n="e" s="T373">oloksujar, </ts>
               <ts e="T375" id="Seg_2157" n="e" s="T374">ɨraːktaːgɨ </ts>
               <ts e="T376" id="Seg_2159" n="e" s="T375">bu͡olar. </ts>
               <ts e="T377" id="Seg_2161" n="e" s="T376">Etiŋ </ts>
               <ts e="T378" id="Seg_2163" n="e" s="T377">ɨraːktaːgɨ </ts>
               <ts e="T379" id="Seg_2165" n="e" s="T378">kɨːhɨn </ts>
               <ts e="T380" id="Seg_2167" n="e" s="T379">ɨllaga </ts>
               <ts e="T381" id="Seg_2169" n="e" s="T380">ol. </ts>
               <ts e="T382" id="Seg_2171" n="e" s="T381">Elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_2172" s="T0">PoMA_1964_FoxDeceiver_flk.001</ta>
            <ta e="T12" id="Seg_2173" s="T3">PoMA_1964_FoxDeceiver_flk.002</ta>
            <ta e="T17" id="Seg_2174" s="T12">PoMA_1964_FoxDeceiver_flk.003</ta>
            <ta e="T20" id="Seg_2175" s="T17">PoMA_1964_FoxDeceiver_flk.004</ta>
            <ta e="T25" id="Seg_2176" s="T20">PoMA_1964_FoxDeceiver_flk.005</ta>
            <ta e="T33" id="Seg_2177" s="T25">PoMA_1964_FoxDeceiver_flk.006</ta>
            <ta e="T40" id="Seg_2178" s="T33">PoMA_1964_FoxDeceiver_flk.007</ta>
            <ta e="T44" id="Seg_2179" s="T40">PoMA_1964_FoxDeceiver_flk.008</ta>
            <ta e="T49" id="Seg_2180" s="T44">PoMA_1964_FoxDeceiver_flk.009</ta>
            <ta e="T54" id="Seg_2181" s="T49">PoMA_1964_FoxDeceiver_flk.010</ta>
            <ta e="T62" id="Seg_2182" s="T54">PoMA_1964_FoxDeceiver_flk.011</ta>
            <ta e="T72" id="Seg_2183" s="T62">PoMA_1964_FoxDeceiver_flk.012</ta>
            <ta e="T75" id="Seg_2184" s="T72">PoMA_1964_FoxDeceiver_flk.013</ta>
            <ta e="T79" id="Seg_2185" s="T75">PoMA_1964_FoxDeceiver_flk.014</ta>
            <ta e="T84" id="Seg_2186" s="T79">PoMA_1964_FoxDeceiver_flk.015</ta>
            <ta e="T91" id="Seg_2187" s="T84">PoMA_1964_FoxDeceiver_flk.016</ta>
            <ta e="T95" id="Seg_2188" s="T91">PoMA_1964_FoxDeceiver_flk.017</ta>
            <ta e="T101" id="Seg_2189" s="T95">PoMA_1964_FoxDeceiver_flk.018</ta>
            <ta e="T109" id="Seg_2190" s="T101">PoMA_1964_FoxDeceiver_flk.019</ta>
            <ta e="T116" id="Seg_2191" s="T109">PoMA_1964_FoxDeceiver_flk.020</ta>
            <ta e="T121" id="Seg_2192" s="T116">PoMA_1964_FoxDeceiver_flk.021</ta>
            <ta e="T125" id="Seg_2193" s="T121">PoMA_1964_FoxDeceiver_flk.022</ta>
            <ta e="T126" id="Seg_2194" s="T125">PoMA_1964_FoxDeceiver_flk.023</ta>
            <ta e="T130" id="Seg_2195" s="T126">PoMA_1964_FoxDeceiver_flk.024</ta>
            <ta e="T135" id="Seg_2196" s="T130">PoMA_1964_FoxDeceiver_flk.025</ta>
            <ta e="T139" id="Seg_2197" s="T135">PoMA_1964_FoxDeceiver_flk.026</ta>
            <ta e="T143" id="Seg_2198" s="T139">PoMA_1964_FoxDeceiver_flk.027</ta>
            <ta e="T152" id="Seg_2199" s="T143">PoMA_1964_FoxDeceiver_flk.028</ta>
            <ta e="T157" id="Seg_2200" s="T152">PoMA_1964_FoxDeceiver_flk.029</ta>
            <ta e="T167" id="Seg_2201" s="T157">PoMA_1964_FoxDeceiver_flk.030</ta>
            <ta e="T169" id="Seg_2202" s="T167">PoMA_1964_FoxDeceiver_flk.031</ta>
            <ta e="T174" id="Seg_2203" s="T169">PoMA_1964_FoxDeceiver_flk.032</ta>
            <ta e="T181" id="Seg_2204" s="T174">PoMA_1964_FoxDeceiver_flk.033</ta>
            <ta e="T186" id="Seg_2205" s="T181">PoMA_1964_FoxDeceiver_flk.034</ta>
            <ta e="T198" id="Seg_2206" s="T186">PoMA_1964_FoxDeceiver_flk.035</ta>
            <ta e="T200" id="Seg_2207" s="T198">PoMA_1964_FoxDeceiver_flk.036</ta>
            <ta e="T208" id="Seg_2208" s="T200">PoMA_1964_FoxDeceiver_flk.037</ta>
            <ta e="T212" id="Seg_2209" s="T208">PoMA_1964_FoxDeceiver_flk.038</ta>
            <ta e="T217" id="Seg_2210" s="T212">PoMA_1964_FoxDeceiver_flk.039</ta>
            <ta e="T221" id="Seg_2211" s="T217">PoMA_1964_FoxDeceiver_flk.040</ta>
            <ta e="T230" id="Seg_2212" s="T221">PoMA_1964_FoxDeceiver_flk.041</ta>
            <ta e="T238" id="Seg_2213" s="T230">PoMA_1964_FoxDeceiver_flk.042</ta>
            <ta e="T241" id="Seg_2214" s="T238">PoMA_1964_FoxDeceiver_flk.043</ta>
            <ta e="T243" id="Seg_2215" s="T241">PoMA_1964_FoxDeceiver_flk.044</ta>
            <ta e="T246" id="Seg_2216" s="T243">PoMA_1964_FoxDeceiver_flk.045</ta>
            <ta e="T260" id="Seg_2217" s="T246">PoMA_1964_FoxDeceiver_flk.046</ta>
            <ta e="T268" id="Seg_2218" s="T260">PoMA_1964_FoxDeceiver_flk.047</ta>
            <ta e="T275" id="Seg_2219" s="T268">PoMA_1964_FoxDeceiver_flk.048</ta>
            <ta e="T280" id="Seg_2220" s="T275">PoMA_1964_FoxDeceiver_flk.049</ta>
            <ta e="T283" id="Seg_2221" s="T280">PoMA_1964_FoxDeceiver_flk.050</ta>
            <ta e="T289" id="Seg_2222" s="T283">PoMA_1964_FoxDeceiver_flk.051</ta>
            <ta e="T296" id="Seg_2223" s="T289">PoMA_1964_FoxDeceiver_flk.052</ta>
            <ta e="T300" id="Seg_2224" s="T296">PoMA_1964_FoxDeceiver_flk.053</ta>
            <ta e="T307" id="Seg_2225" s="T300">PoMA_1964_FoxDeceiver_flk.054</ta>
            <ta e="T312" id="Seg_2226" s="T307">PoMA_1964_FoxDeceiver_flk.055</ta>
            <ta e="T314" id="Seg_2227" s="T312">PoMA_1964_FoxDeceiver_flk.056</ta>
            <ta e="T322" id="Seg_2228" s="T314">PoMA_1964_FoxDeceiver_flk.057</ta>
            <ta e="T332" id="Seg_2229" s="T322">PoMA_1964_FoxDeceiver_flk.058</ta>
            <ta e="T337" id="Seg_2230" s="T332">PoMA_1964_FoxDeceiver_flk.059</ta>
            <ta e="T341" id="Seg_2231" s="T337">PoMA_1964_FoxDeceiver_flk.060</ta>
            <ta e="T347" id="Seg_2232" s="T341">PoMA_1964_FoxDeceiver_flk.061</ta>
            <ta e="T350" id="Seg_2233" s="T347">PoMA_1964_FoxDeceiver_flk.062</ta>
            <ta e="T355" id="Seg_2234" s="T350">PoMA_1964_FoxDeceiver_flk.063</ta>
            <ta e="T368" id="Seg_2235" s="T355">PoMA_1964_FoxDeceiver_flk.064</ta>
            <ta e="T376" id="Seg_2236" s="T368">PoMA_1964_FoxDeceiver_flk.065</ta>
            <ta e="T381" id="Seg_2237" s="T376">PoMA_1964_FoxDeceiver_flk.066</ta>
            <ta e="T382" id="Seg_2238" s="T381">PoMA_1964_FoxDeceiver_flk.067</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_2239" s="T0">Умнаһыт паасынай олорбут.</ta>
            <ta e="T12" id="Seg_2240" s="T3">Бу паасынай өлбүтүгэр буор голомотугар уол огото тулаайак каалар.</ta>
            <ta e="T17" id="Seg_2241" s="T12">Бу ого аччыктаан өлөрө буолар.</ta>
            <ta e="T20" id="Seg_2242" s="T17">Буну һаһыл булар.</ta>
            <ta e="T25" id="Seg_2243" s="T20">— Мин эньигин киһи оҥоруом, — диир,</ta>
            <ta e="T33" id="Seg_2244" s="T25">— Баай киһи буолуоҥ, баай дьактары дьактар ылан биэриэм.</ta>
            <ta e="T40" id="Seg_2245" s="T33">Инньэ диэн баран һаһыл Этиҥ ыраактаагытыгар барар.</ta>
            <ta e="T44" id="Seg_2246" s="T40">Этиҥ ыраактаагытыгар тийэн диир:</ta>
            <ta e="T49" id="Seg_2247" s="T44">— Ааттаак баай уол киһи баар.</ta>
            <ta e="T54" id="Seg_2248" s="T49">Гини эн кыыскын һуорумньулата ыытта.</ta>
            <ta e="T62" id="Seg_2249" s="T54">Кыыһыҥ һулуутун тыа кыылын бастыҥын, арааһын ыытыак буолла.</ta>
            <ta e="T72" id="Seg_2250" s="T62">Ол кыыллары тус туһунан дьиэгэ ас бэлэмнээн тоһуйуоҥ үһү, — диир.</ta>
            <ta e="T75" id="Seg_2251" s="T72">Этиҥ ыраактаагы һөбүлэммит.</ta>
            <ta e="T79" id="Seg_2252" s="T75">Һаһыл албун төттөрү һүүрбүт.</ta>
            <ta e="T84" id="Seg_2253" s="T79">Тыа кыылларын арааһын көрсөн һаҥарар:</ta>
            <ta e="T91" id="Seg_2254" s="T84">— Этиҥ ыраактаагы баайа-тото батымыйа эһиэкэ ас тэрийдэ.</ta>
            <ta e="T95" id="Seg_2255" s="T91">Ону баран аһыыр үһүгүт.</ta>
            <ta e="T101" id="Seg_2256" s="T95">Кыыл бөгө аалин буолан тийэллэр аһыы.</ta>
            <ta e="T109" id="Seg_2257" s="T101">Этиҥ ыраактаагы үөрэр, күтүөтүм баай киһи эбит диир.</ta>
            <ta e="T116" id="Seg_2258" s="T109">Кыыллар аһыы турдактарына, били дьиэлэри катаан кэбиһэллэр.</ta>
            <ta e="T121" id="Seg_2259" s="T116">Һаһыл албун Этиҥ ыраактаагыга тийэр.</ta>
            <ta e="T125" id="Seg_2260" s="T121">— Кайа, кыыллар кэллилэр дьүрү?</ta>
            <ta e="T126" id="Seg_2261" s="T125">— Кэллилэр.</ta>
            <ta e="T130" id="Seg_2262" s="T126">— Күтүөт уол бокуойа һуок.</ta>
            <ta e="T135" id="Seg_2263" s="T130">Һотору һолотуой караабынан кэлиэ, — диир.</ta>
            <ta e="T139" id="Seg_2264" s="T135">Уолугар төттөрү һүүрэн тийэр.</ta>
            <ta e="T143" id="Seg_2265" s="T139">Талагынан куһаган болуот оҥорторор.</ta>
            <ta e="T152" id="Seg_2266" s="T143">Араас һир һибэккитинэн һимэтэр, ыраактан һиэдэрэй, баскуой буолан көстөр.</ta>
            <ta e="T157" id="Seg_2267" s="T152">Тыал күн Этиҥ ыраактаагыга усталлар.</ta>
            <ta e="T167" id="Seg_2268" s="T157">Этиҥ ыраактаагы көрбүтэ: байгал устун һолотуой караап иһэр, һиэдэрэй өҥнөөк.</ta>
            <ta e="T169" id="Seg_2269" s="T167">Ыраактаагы үөрэр.</ta>
            <ta e="T174" id="Seg_2270" s="T169">Тыал түспүтүгэр болуот ыһыллан каалар.</ta>
            <ta e="T181" id="Seg_2271" s="T174">Һаһыл албун Этиҥ ыраактаагыга һүүрэн тийэр, диир:</ta>
            <ta e="T186" id="Seg_2272" s="T181">— Иэдээн буолла, һолотуой караап тимирдэ.</ta>
            <ta e="T198" id="Seg_2273" s="T186">Күтүөтүҥ киһи бэрдэ буолан орто, таһагаһа барыта ууга барда, бэйэтэ һыгынньак каалла.</ta>
            <ta e="T200" id="Seg_2274" s="T198">Таҥаста ыыт.</ta>
            <ta e="T208" id="Seg_2275" s="T200">Этиҥ ыраактаагы уолга бастыҥ таҥаһы, бэйэтин караабын ыытар.</ta>
            <ta e="T212" id="Seg_2276" s="T208">һаһыл албун уолун һүбэлиир:</ta>
            <ta e="T217" id="Seg_2277" s="T212">— Ыраактаагыга тийэҥҥин таҥаскын көрүнүмэ, одуургуоктара.</ta>
            <ta e="T221" id="Seg_2278" s="T217">Тоҥкос эрэ гынан дорооболос.</ta>
            <ta e="T230" id="Seg_2279" s="T221">Аһыыргар дьон аһыырын көрөн аһаа, арыгыны бэрт кыратык ис.</ta>
            <ta e="T238" id="Seg_2280" s="T230">Уол Этиҥ ыраактаагыга тийэр караабын миинэн, таҥаһын таҥнан.</ta>
            <ta e="T241" id="Seg_2281" s="T238">Курум бөгө буолар.</ta>
            <ta e="T243" id="Seg_2282" s="T241">Караабынан төннөллөр.</ta>
            <ta e="T246" id="Seg_2283" s="T243">һаһыл албун һаҥарар:</ta>
            <ta e="T260" id="Seg_2284" s="T246">— Буор голомогор токтообокко ааһаар, атын ыраактаагы һолотуой гуоратыгар чугаһаан баран токтоор уонна минигин ыҥыртараар.</ta>
            <ta e="T268" id="Seg_2285" s="T260">Дьонум-норуотум төһө бэркэ олорорун билэн кэл диэр, миниэкэ.</ta>
            <ta e="T275" id="Seg_2286" s="T268">һаһыл эппитин курдук уол бэйэтин голомотун ааһар.</ta>
            <ta e="T280" id="Seg_2287" s="T275">Атын ыраактаагы һолотуой гуоратыгар чугаһыыр.</ta>
            <ta e="T283" id="Seg_2288" s="T280">Токтоот туран диир:</ta>
            <ta e="T289" id="Seg_2289" s="T283">— Дьонум-норуотум төһө бэркэ олорорун билиэм этэ.</ta>
            <ta e="T296" id="Seg_2290" s="T289">Били һаһылым канна барда, билэн кэлиэн, — диир.</ta>
            <ta e="T300" id="Seg_2291" s="T296">Һаһыл албун кэлбитигэр һаҥарар.</ta>
            <ta e="T307" id="Seg_2292" s="T300">Һаһыл албун баран һолотуой гуорат ыраактаагытыгар һаҥарар:</ta>
            <ta e="T312" id="Seg_2293" s="T307">— Этиҥ ыраактаагы эһигини эһэ кэллэ.</ta>
            <ta e="T314" id="Seg_2294" s="T312">Куота оксуҥ.</ta>
            <ta e="T322" id="Seg_2295" s="T314">Ити тыага таксан һаһыҥ, төбөгүт эрэ көстөр курдук.</ta>
            <ta e="T332" id="Seg_2296" s="T322">һолотуой гуорат ыраактаагытын норуота бары тыага куотар, Этиҥ ыраактаагыттан куттанан.</ta>
            <ta e="T337" id="Seg_2297" s="T332">Һаһыл албун уолга ытаан кэлэр:</ta>
            <ta e="T341" id="Seg_2298" s="T337">— Дьоммутун Абааһы ыраактаагыта бараабыт.</ta>
            <ta e="T347" id="Seg_2299" s="T341">Һиэн-һиэн баран ол ойуурга һаһа һыталлар.</ta>
            <ta e="T350" id="Seg_2300" s="T347">Кылыҥҥын көрдөс — этиҥнээтин.</ta>
            <ta e="T355" id="Seg_2301" s="T350">Уол кыннытыттан көрдөһөр "этиҥнээ" диэн.</ta>
            <ta e="T368" id="Seg_2302" s="T355">Этиҥ ыраактаагы дьэ этиҥнээн һаайар били ойууру, һолотуой гуорат биир да киһитэ орпот.</ta>
            <ta e="T376" id="Seg_2303" s="T368">Уол били һолотуой гуоракка киирэн олоксуйар, ыраактаагы буолар.</ta>
            <ta e="T381" id="Seg_2304" s="T376">Этиҥ ыраактаагы кыыһын ыллага ол.</ta>
            <ta e="T382" id="Seg_2305" s="T381">Элэтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_2306" s="T0">Umnahɨt paːsɨnaj olorbut. </ta>
            <ta e="T12" id="Seg_2307" s="T3">Bu paːsɨnaj ölbütüger bu͡or golomotugar u͡ol ogoto tulaːjak kaːlar. </ta>
            <ta e="T17" id="Seg_2308" s="T12">Bu ogo aččɨktaːn ölörö bu͡olar. </ta>
            <ta e="T20" id="Seg_2309" s="T17">Bunu hahɨl bular. </ta>
            <ta e="T25" id="Seg_2310" s="T20">"Min enʼigin kihi oŋoru͡om", diːr.</ta>
            <ta e="T33" id="Seg_2311" s="T25">"Baːj kihi bu͡olu͡oŋ, baːj dʼaktarɨ dʼaktar ɨlan bi͡eri͡em." </ta>
            <ta e="T40" id="Seg_2312" s="T33">Innʼe di͡en baran hahɨl Etiŋ ɨraːktaːgɨtɨgar barar. </ta>
            <ta e="T44" id="Seg_2313" s="T40">Etiŋ ɨraːktaːgɨtɨgar tijen diːr:</ta>
            <ta e="T49" id="Seg_2314" s="T44">"Aːttaːk baːj u͡ol kihi baːr. </ta>
            <ta e="T54" id="Seg_2315" s="T49">Gini en kɨːskɨn hu͡orumnʼulata ɨːtta. </ta>
            <ta e="T62" id="Seg_2316" s="T54">Kɨːhɨŋ huluːtun tɨ͡a kɨːlɨn bastɨŋɨn, araːhɨn ɨːtɨ͡ak bu͡olla. </ta>
            <ta e="T72" id="Seg_2317" s="T62">Ol kɨːllarɨ tus tuhunan dʼi͡ege as belemneːn tohuju͡oŋ ühü", diːr. </ta>
            <ta e="T75" id="Seg_2318" s="T72">Etiŋ ɨraːktaːgɨ höbülemmit. </ta>
            <ta e="T79" id="Seg_2319" s="T75">Hahɨl albun töttörü hüːrbüt. </ta>
            <ta e="T84" id="Seg_2320" s="T79">Tɨ͡a kɨːllarɨn araːhɨn körsön haŋarar: </ta>
            <ta e="T91" id="Seg_2321" s="T84">"Etiŋ ɨraːktaːgɨ baːja-toto batɨmɨja ehi͡eke as terijde. </ta>
            <ta e="T95" id="Seg_2322" s="T91">Onu baran ahɨːr ühügüt." </ta>
            <ta e="T101" id="Seg_2323" s="T95">Kɨːl bögö aːlin bu͡olan tijeller ahɨː. </ta>
            <ta e="T109" id="Seg_2324" s="T101">Etiŋ ɨraːktaːgɨ ü͡örer, "kütü͡ötüm baːj kihi ebit" diːr. </ta>
            <ta e="T116" id="Seg_2325" s="T109">Kɨːllar ahɨː turdaktarɨna, bili dʼi͡eleri kataːn kebiheller. </ta>
            <ta e="T121" id="Seg_2326" s="T116">Hahɨl albun Etiŋ ɨraːktaːgɨga tijer. </ta>
            <ta e="T125" id="Seg_2327" s="T121">"Kaja, kɨːllar kelliler dʼürü?" </ta>
            <ta e="T126" id="Seg_2328" s="T125">"Kelliler." </ta>
            <ta e="T130" id="Seg_2329" s="T126">"Kütü͡öt u͡ol boku͡oja hu͡ok. </ta>
            <ta e="T135" id="Seg_2330" s="T130">Hotoru holotu͡oj karaːbɨnan keli͡e", diːr. </ta>
            <ta e="T139" id="Seg_2331" s="T135">U͡olugar töttörü hüːren tijer. </ta>
            <ta e="T143" id="Seg_2332" s="T139">Talagɨnan kuhagan bolu͡ot oŋortoror. </ta>
            <ta e="T152" id="Seg_2333" s="T143">Araːs hir hibekkitinen himeter, ɨraːktan hi͡ederej, basku͡oj bu͡olan köstör. </ta>
            <ta e="T157" id="Seg_2334" s="T152">Tɨ͡al kün Etiŋ ɨraːktaːgɨga ustallar. </ta>
            <ta e="T167" id="Seg_2335" s="T157">Etiŋ ɨraːktaːgɨ körbüte, bajgal ustun holotu͡oj karaːp iher, hi͡ederej öŋnöːk. </ta>
            <ta e="T169" id="Seg_2336" s="T167">ɨraːktaːgɨ ü͡örer. </ta>
            <ta e="T174" id="Seg_2337" s="T169">Tɨ͡al tüspütüger bolu͡ot ɨhɨllan kaːlar. </ta>
            <ta e="T181" id="Seg_2338" s="T174">Hahɨl albun Etiŋ ɨraːktaːgɨga hüːren tijer, diːr: </ta>
            <ta e="T186" id="Seg_2339" s="T181">"i͡edeːn bu͡olla, holotu͡oj karaːp timirde. </ta>
            <ta e="T198" id="Seg_2340" s="T186">Kütü͡ötüŋ kihi berde bu͡olan orto, tahagaha barɨta uːga barda, bejete hɨgɨnnʼak kaːlla. </ta>
            <ta e="T200" id="Seg_2341" s="T198">Taŋasta ɨːt." </ta>
            <ta e="T208" id="Seg_2342" s="T200">Etiŋ ɨraːktaːgɨ u͡olga bastɨŋ taŋahɨ, bejetin karaːbɨn ɨːtar. </ta>
            <ta e="T212" id="Seg_2343" s="T208">Hahɨl albun u͡olun hübeliːr:</ta>
            <ta e="T217" id="Seg_2344" s="T212">"ɨraːktaːgɨga tijeŋŋin taŋaskɨn körünüme, oduːrgu͡oktara. </ta>
            <ta e="T221" id="Seg_2345" s="T217">Toŋkos ere gɨnan doroːbolos. </ta>
            <ta e="T230" id="Seg_2346" s="T221">Ahɨːrgar dʼon ahɨːrɨn körön ahaː, arɨgɨnɨ bert kɨratɨk is." </ta>
            <ta e="T238" id="Seg_2347" s="T230">U͡ol Etiŋ ɨraːktaːgɨga tijer karaːbɨn miːnen, taŋahɨn taŋnan. </ta>
            <ta e="T241" id="Seg_2348" s="T238">Kurum bögö bu͡olar. </ta>
            <ta e="T243" id="Seg_2349" s="T241">Karaːbɨnan tönnöllör.</ta>
            <ta e="T246" id="Seg_2350" s="T243">Hahɨl albun haŋarar:</ta>
            <ta e="T260" id="Seg_2351" s="T246">"Bu͡or golomogor toktoːbokko aːhaːr, atɨn ɨraːktaːgɨ holotu͡oj gu͡oratɨgar čugahaːn baran toktoːr u͡onna minigin ɨŋɨrtaraːr. </ta>
            <ta e="T268" id="Seg_2352" s="T260">Dʼonum-noru͡otum töhö berke olororun bilen kel di͡er, mini͡eke." </ta>
            <ta e="T275" id="Seg_2353" s="T268">Hahɨl eppitin kurduk u͡ol bejetin golomotun aːhar. </ta>
            <ta e="T280" id="Seg_2354" s="T275">Atɨn ɨraːktaːgɨ holotu͡oj gu͡oratɨgar čugahɨːr. </ta>
            <ta e="T283" id="Seg_2355" s="T280">Toktoːt turan diːr:</ta>
            <ta e="T289" id="Seg_2356" s="T283">"Dʼonum-noru͡otum töhö berke olororun bili͡em ete. </ta>
            <ta e="T296" id="Seg_2357" s="T289">Bili hahɨlɨm kanna barda, bilen keli͡en", diːr. </ta>
            <ta e="T300" id="Seg_2358" s="T296">Hahɨl albun kelbitiger haŋarar. </ta>
            <ta e="T307" id="Seg_2359" s="T300">Hahɨl albun baran holotu͡oj gu͡orat ɨraːktaːgɨtɨgar haŋarar:</ta>
            <ta e="T312" id="Seg_2360" s="T307">"Etiŋ ɨraːktaːgɨ ehigini ehe kelle. </ta>
            <ta e="T314" id="Seg_2361" s="T312">Ku͡ota oksuŋ. </ta>
            <ta e="T322" id="Seg_2362" s="T314">Iti tɨ͡aga taksan hahɨŋ, töbögüt ere köstör kurduk." </ta>
            <ta e="T332" id="Seg_2363" s="T322">Holotu͡oj gu͡orat ɨraːktaːgɨtɨn noru͡ota barɨ tɨ͡aga ku͡otar, Etiŋ ɨraːktaːgɨttan kuttanan. </ta>
            <ta e="T337" id="Seg_2364" s="T332">Hahɨl albun u͡olga ɨtaːn keler:</ta>
            <ta e="T341" id="Seg_2365" s="T337">"Dʼommutun Abaːhɨ ɨraːktaːgɨta baraːbɨt.</ta>
            <ta e="T347" id="Seg_2366" s="T341">Hi͡en-hi͡en baran ol ojuːrga haha hɨtallar. </ta>
            <ta e="T350" id="Seg_2367" s="T347">Kɨlɨŋŋɨn kördös — etiŋneːtin." </ta>
            <ta e="T355" id="Seg_2368" s="T350">U͡ol kɨnnɨtɨttan kördöhör "etiŋneː" di͡en. </ta>
            <ta e="T368" id="Seg_2369" s="T355">Etiŋ ɨraːktaːgɨ dʼe etiŋneːn haːjar bili ojuːru, holotu͡oj gu͡orat biːr da kihite orpot. </ta>
            <ta e="T376" id="Seg_2370" s="T368">U͡ol bili holotu͡oj gu͡orakka kiːren oloksujar, ɨraːktaːgɨ bu͡olar. </ta>
            <ta e="T381" id="Seg_2371" s="T376">Etiŋ ɨraːktaːgɨ kɨːhɨn ɨllaga ol. </ta>
            <ta e="T382" id="Seg_2372" s="T381">Elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2373" s="T0">umnahɨt</ta>
            <ta e="T2" id="Seg_2374" s="T1">paːsɨnaj</ta>
            <ta e="T3" id="Seg_2375" s="T2">olor-but</ta>
            <ta e="T4" id="Seg_2376" s="T3">bu</ta>
            <ta e="T5" id="Seg_2377" s="T4">paːsɨnaj</ta>
            <ta e="T6" id="Seg_2378" s="T5">öl-büt-ü-ger</ta>
            <ta e="T7" id="Seg_2379" s="T6">bu͡or</ta>
            <ta e="T8" id="Seg_2380" s="T7">golomo-tu-gar</ta>
            <ta e="T9" id="Seg_2381" s="T8">u͡ol</ta>
            <ta e="T10" id="Seg_2382" s="T9">ogo-to</ta>
            <ta e="T11" id="Seg_2383" s="T10">tulaːjak</ta>
            <ta e="T12" id="Seg_2384" s="T11">kaːl-ar</ta>
            <ta e="T13" id="Seg_2385" s="T12">bu</ta>
            <ta e="T14" id="Seg_2386" s="T13">ogo</ta>
            <ta e="T15" id="Seg_2387" s="T14">aččɨktaː-n</ta>
            <ta e="T16" id="Seg_2388" s="T15">ölör-ö</ta>
            <ta e="T17" id="Seg_2389" s="T16">bu͡ol-ar</ta>
            <ta e="T18" id="Seg_2390" s="T17">bu-nu</ta>
            <ta e="T19" id="Seg_2391" s="T18">hahɨl</ta>
            <ta e="T20" id="Seg_2392" s="T19">bul-ar</ta>
            <ta e="T21" id="Seg_2393" s="T20">min</ta>
            <ta e="T22" id="Seg_2394" s="T21">enʼigi-n</ta>
            <ta e="T23" id="Seg_2395" s="T22">kihi</ta>
            <ta e="T24" id="Seg_2396" s="T23">oŋor-u͡o-m</ta>
            <ta e="T25" id="Seg_2397" s="T24">diː-r</ta>
            <ta e="T26" id="Seg_2398" s="T25">baːj</ta>
            <ta e="T27" id="Seg_2399" s="T26">kihi</ta>
            <ta e="T28" id="Seg_2400" s="T27">bu͡ol-u͡o-ŋ</ta>
            <ta e="T29" id="Seg_2401" s="T28">baːj</ta>
            <ta e="T30" id="Seg_2402" s="T29">dʼaktar-ɨ</ta>
            <ta e="T31" id="Seg_2403" s="T30">dʼaktar</ta>
            <ta e="T32" id="Seg_2404" s="T31">ɨl-an</ta>
            <ta e="T33" id="Seg_2405" s="T32">bi͡er-i͡e-m</ta>
            <ta e="T34" id="Seg_2406" s="T33">innʼe</ta>
            <ta e="T35" id="Seg_2407" s="T34">di͡e-n</ta>
            <ta e="T36" id="Seg_2408" s="T35">baran</ta>
            <ta e="T37" id="Seg_2409" s="T36">hahɨl</ta>
            <ta e="T38" id="Seg_2410" s="T37">Etiŋ</ta>
            <ta e="T39" id="Seg_2411" s="T38">ɨraːktaːgɨ-tɨ-gar</ta>
            <ta e="T40" id="Seg_2412" s="T39">bar-ar</ta>
            <ta e="T41" id="Seg_2413" s="T40">Etiŋ</ta>
            <ta e="T42" id="Seg_2414" s="T41">ɨraːktaːgɨ-tɨ-gar</ta>
            <ta e="T43" id="Seg_2415" s="T42">tij-en</ta>
            <ta e="T44" id="Seg_2416" s="T43">diː-r</ta>
            <ta e="T45" id="Seg_2417" s="T44">aːt-taːk</ta>
            <ta e="T46" id="Seg_2418" s="T45">baːj</ta>
            <ta e="T47" id="Seg_2419" s="T46">u͡ol</ta>
            <ta e="T48" id="Seg_2420" s="T47">kihi</ta>
            <ta e="T49" id="Seg_2421" s="T48">baːr</ta>
            <ta e="T50" id="Seg_2422" s="T49">gini</ta>
            <ta e="T51" id="Seg_2423" s="T50">en</ta>
            <ta e="T52" id="Seg_2424" s="T51">kɨːs-kɨ-n</ta>
            <ta e="T53" id="Seg_2425" s="T52">hu͡orumnʼu-l-a-t-a</ta>
            <ta e="T54" id="Seg_2426" s="T53">ɨːt-t-a</ta>
            <ta e="T55" id="Seg_2427" s="T54">kɨːh-ɨ-ŋ</ta>
            <ta e="T56" id="Seg_2428" s="T55">huluː-tu-n</ta>
            <ta e="T57" id="Seg_2429" s="T56">tɨ͡a</ta>
            <ta e="T58" id="Seg_2430" s="T57">kɨːl-ɨ-n</ta>
            <ta e="T59" id="Seg_2431" s="T58">bastɨŋ-ɨ-n</ta>
            <ta e="T60" id="Seg_2432" s="T59">araːh-ɨ-n</ta>
            <ta e="T61" id="Seg_2433" s="T60">ɨːt-ɨ͡ak</ta>
            <ta e="T62" id="Seg_2434" s="T61">bu͡ol-l-a</ta>
            <ta e="T63" id="Seg_2435" s="T62">ol</ta>
            <ta e="T64" id="Seg_2436" s="T63">kɨːl-lar-ɨ</ta>
            <ta e="T65" id="Seg_2437" s="T64">tus</ta>
            <ta e="T66" id="Seg_2438" s="T65">tuh-u-nan</ta>
            <ta e="T67" id="Seg_2439" s="T66">dʼi͡e-ge</ta>
            <ta e="T68" id="Seg_2440" s="T67">as</ta>
            <ta e="T69" id="Seg_2441" s="T68">belem-neː-n</ta>
            <ta e="T70" id="Seg_2442" s="T69">tohuj-u͡o-ŋ</ta>
            <ta e="T71" id="Seg_2443" s="T70">ühü</ta>
            <ta e="T72" id="Seg_2444" s="T71">diː-r</ta>
            <ta e="T73" id="Seg_2445" s="T72">Etiŋ</ta>
            <ta e="T74" id="Seg_2446" s="T73">ɨraːktaːgɨ</ta>
            <ta e="T75" id="Seg_2447" s="T74">höbül-e-m-mit</ta>
            <ta e="T76" id="Seg_2448" s="T75">hahɨl</ta>
            <ta e="T77" id="Seg_2449" s="T76">albun</ta>
            <ta e="T78" id="Seg_2450" s="T77">töttörü</ta>
            <ta e="T79" id="Seg_2451" s="T78">hüːr-büt</ta>
            <ta e="T80" id="Seg_2452" s="T79">tɨ͡a</ta>
            <ta e="T81" id="Seg_2453" s="T80">kɨːl-lar-ɨ-n</ta>
            <ta e="T82" id="Seg_2454" s="T81">araːh-ɨ-n</ta>
            <ta e="T83" id="Seg_2455" s="T82">körs-ön</ta>
            <ta e="T84" id="Seg_2456" s="T83">haŋar-ar</ta>
            <ta e="T85" id="Seg_2457" s="T84">Etiŋ</ta>
            <ta e="T86" id="Seg_2458" s="T85">ɨraːktaːgɨ</ta>
            <ta e="T87" id="Seg_2459" s="T86">baːj-a-tot-o</ta>
            <ta e="T88" id="Seg_2460" s="T87">bat-ɨ-mɨja</ta>
            <ta e="T89" id="Seg_2461" s="T88">ehi͡e-ke</ta>
            <ta e="T90" id="Seg_2462" s="T89">as</ta>
            <ta e="T91" id="Seg_2463" s="T90">terij-d-e</ta>
            <ta e="T92" id="Seg_2464" s="T91">o-nu</ta>
            <ta e="T93" id="Seg_2465" s="T92">baran</ta>
            <ta e="T94" id="Seg_2466" s="T93">ahɨː-r</ta>
            <ta e="T95" id="Seg_2467" s="T94">ühü-güt</ta>
            <ta e="T96" id="Seg_2468" s="T95">kɨːl</ta>
            <ta e="T97" id="Seg_2469" s="T96">bögö</ta>
            <ta e="T98" id="Seg_2470" s="T97">aːlin</ta>
            <ta e="T99" id="Seg_2471" s="T98">bu͡ol-an</ta>
            <ta e="T100" id="Seg_2472" s="T99">tij-el-ler</ta>
            <ta e="T101" id="Seg_2473" s="T100">ah-ɨː</ta>
            <ta e="T102" id="Seg_2474" s="T101">Etiŋ</ta>
            <ta e="T103" id="Seg_2475" s="T102">ɨraːktaːgɨ</ta>
            <ta e="T104" id="Seg_2476" s="T103">ü͡ör-er</ta>
            <ta e="T105" id="Seg_2477" s="T104">kütü͡öt-ü-m</ta>
            <ta e="T106" id="Seg_2478" s="T105">baːj</ta>
            <ta e="T107" id="Seg_2479" s="T106">kihi</ta>
            <ta e="T108" id="Seg_2480" s="T107">e-bit</ta>
            <ta e="T109" id="Seg_2481" s="T108">diː-r</ta>
            <ta e="T110" id="Seg_2482" s="T109">kɨːl-lar</ta>
            <ta e="T111" id="Seg_2483" s="T110">ah-ɨː</ta>
            <ta e="T112" id="Seg_2484" s="T111">tur-dak-tarɨna</ta>
            <ta e="T113" id="Seg_2485" s="T112">bili</ta>
            <ta e="T114" id="Seg_2486" s="T113">dʼi͡e-ler-i</ta>
            <ta e="T115" id="Seg_2487" s="T114">kataː-n</ta>
            <ta e="T116" id="Seg_2488" s="T115">kebih-el-ler</ta>
            <ta e="T117" id="Seg_2489" s="T116">hahɨl</ta>
            <ta e="T118" id="Seg_2490" s="T117">albun</ta>
            <ta e="T119" id="Seg_2491" s="T118">Etiŋ</ta>
            <ta e="T120" id="Seg_2492" s="T119">ɨraːktaːgɨ-ga</ta>
            <ta e="T121" id="Seg_2493" s="T120">tij-er</ta>
            <ta e="T122" id="Seg_2494" s="T121">kaja</ta>
            <ta e="T123" id="Seg_2495" s="T122">kɨːl-lar</ta>
            <ta e="T124" id="Seg_2496" s="T123">kel-li-ler</ta>
            <ta e="T125" id="Seg_2497" s="T124">dʼürü</ta>
            <ta e="T126" id="Seg_2498" s="T125">kel-li-ler</ta>
            <ta e="T127" id="Seg_2499" s="T126">kütü͡öt</ta>
            <ta e="T128" id="Seg_2500" s="T127">u͡ol</ta>
            <ta e="T129" id="Seg_2501" s="T128">boku͡oj-a</ta>
            <ta e="T130" id="Seg_2502" s="T129">hu͡ok</ta>
            <ta e="T131" id="Seg_2503" s="T130">hotoru</ta>
            <ta e="T132" id="Seg_2504" s="T131">holotu͡oj</ta>
            <ta e="T133" id="Seg_2505" s="T132">karaːb-ɨ-nan</ta>
            <ta e="T134" id="Seg_2506" s="T133">kel-i͡e</ta>
            <ta e="T135" id="Seg_2507" s="T134">diː-r</ta>
            <ta e="T136" id="Seg_2508" s="T135">u͡ol-u-gar</ta>
            <ta e="T137" id="Seg_2509" s="T136">töttörü</ta>
            <ta e="T138" id="Seg_2510" s="T137">hüːr-en</ta>
            <ta e="T139" id="Seg_2511" s="T138">tij-er</ta>
            <ta e="T140" id="Seg_2512" s="T139">talag-ɨ-nan</ta>
            <ta e="T141" id="Seg_2513" s="T140">kuhagan</ta>
            <ta e="T142" id="Seg_2514" s="T141">bolu͡ot</ta>
            <ta e="T143" id="Seg_2515" s="T142">oŋor-tor-or</ta>
            <ta e="T144" id="Seg_2516" s="T143">araːs</ta>
            <ta e="T145" id="Seg_2517" s="T144">hir</ta>
            <ta e="T146" id="Seg_2518" s="T145">hibekki-ti-nen</ta>
            <ta e="T147" id="Seg_2519" s="T146">him-e-t-er</ta>
            <ta e="T148" id="Seg_2520" s="T147">ɨraːk-tan</ta>
            <ta e="T149" id="Seg_2521" s="T148">hi͡ederej</ta>
            <ta e="T150" id="Seg_2522" s="T149">basku͡oj</ta>
            <ta e="T151" id="Seg_2523" s="T150">bu͡ol-an</ta>
            <ta e="T152" id="Seg_2524" s="T151">köst-ör</ta>
            <ta e="T153" id="Seg_2525" s="T152">tɨ͡al</ta>
            <ta e="T154" id="Seg_2526" s="T153">kün</ta>
            <ta e="T155" id="Seg_2527" s="T154">Etiŋ</ta>
            <ta e="T156" id="Seg_2528" s="T155">ɨraːktaːgɨ-ga</ta>
            <ta e="T157" id="Seg_2529" s="T156">ust-al-lar</ta>
            <ta e="T158" id="Seg_2530" s="T157">Etiŋ</ta>
            <ta e="T159" id="Seg_2531" s="T158">ɨraːktaːgɨ</ta>
            <ta e="T160" id="Seg_2532" s="T159">kör-büt-e</ta>
            <ta e="T161" id="Seg_2533" s="T160">bajgal</ta>
            <ta e="T162" id="Seg_2534" s="T161">ustun</ta>
            <ta e="T163" id="Seg_2535" s="T162">holotu͡oj</ta>
            <ta e="T164" id="Seg_2536" s="T163">karaːp</ta>
            <ta e="T165" id="Seg_2537" s="T164">ih-er</ta>
            <ta e="T166" id="Seg_2538" s="T165">hi͡ederej</ta>
            <ta e="T167" id="Seg_2539" s="T166">öŋnöːk</ta>
            <ta e="T168" id="Seg_2540" s="T167">ɨraːktaːgɨ</ta>
            <ta e="T169" id="Seg_2541" s="T168">ü͡ör-er</ta>
            <ta e="T170" id="Seg_2542" s="T169">tɨ͡al</ta>
            <ta e="T171" id="Seg_2543" s="T170">tüs-püt-ü-ger</ta>
            <ta e="T172" id="Seg_2544" s="T171">bolu͡ot</ta>
            <ta e="T173" id="Seg_2545" s="T172">ɨh-ɨ-ll-an</ta>
            <ta e="T174" id="Seg_2546" s="T173">kaːl-ar</ta>
            <ta e="T175" id="Seg_2547" s="T174">hahɨl</ta>
            <ta e="T176" id="Seg_2548" s="T175">albun</ta>
            <ta e="T177" id="Seg_2549" s="T176">Etiŋ</ta>
            <ta e="T178" id="Seg_2550" s="T177">ɨraːktaːgɨ-ga</ta>
            <ta e="T179" id="Seg_2551" s="T178">hüːr-en</ta>
            <ta e="T180" id="Seg_2552" s="T179">tij-er</ta>
            <ta e="T181" id="Seg_2553" s="T180">diː-r</ta>
            <ta e="T182" id="Seg_2554" s="T181">i͡edeːn</ta>
            <ta e="T183" id="Seg_2555" s="T182">bu͡ol-l-a</ta>
            <ta e="T184" id="Seg_2556" s="T183">holotu͡oj</ta>
            <ta e="T185" id="Seg_2557" s="T184">karaːp</ta>
            <ta e="T186" id="Seg_2558" s="T185">timir-d-e</ta>
            <ta e="T187" id="Seg_2559" s="T186">kütü͡öt-ü-ŋ</ta>
            <ta e="T188" id="Seg_2560" s="T187">kihi</ta>
            <ta e="T189" id="Seg_2561" s="T188">berde</ta>
            <ta e="T190" id="Seg_2562" s="T189">bu͡ol-an</ta>
            <ta e="T191" id="Seg_2563" s="T190">or-t-o</ta>
            <ta e="T192" id="Seg_2564" s="T191">tahagah-a</ta>
            <ta e="T193" id="Seg_2565" s="T192">barɨ-ta</ta>
            <ta e="T194" id="Seg_2566" s="T193">uː-ga</ta>
            <ta e="T195" id="Seg_2567" s="T194">bar-d-a</ta>
            <ta e="T196" id="Seg_2568" s="T195">beje-te</ta>
            <ta e="T197" id="Seg_2569" s="T196">hɨgɨnnʼak</ta>
            <ta e="T198" id="Seg_2570" s="T197">kaːl-l-a</ta>
            <ta e="T199" id="Seg_2571" s="T198">taŋas-ta</ta>
            <ta e="T200" id="Seg_2572" s="T199">ɨːt</ta>
            <ta e="T201" id="Seg_2573" s="T200">Etiŋ</ta>
            <ta e="T202" id="Seg_2574" s="T201">ɨraːktaːgɨ</ta>
            <ta e="T203" id="Seg_2575" s="T202">u͡ol-ga</ta>
            <ta e="T204" id="Seg_2576" s="T203">bastɨŋ</ta>
            <ta e="T205" id="Seg_2577" s="T204">taŋah-ɨ</ta>
            <ta e="T206" id="Seg_2578" s="T205">beje-ti-n</ta>
            <ta e="T207" id="Seg_2579" s="T206">karaːb-ɨ-n</ta>
            <ta e="T208" id="Seg_2580" s="T207">ɨːt-ar</ta>
            <ta e="T209" id="Seg_2581" s="T208">hahɨl</ta>
            <ta e="T210" id="Seg_2582" s="T209">albun</ta>
            <ta e="T211" id="Seg_2583" s="T210">u͡ol-u-n</ta>
            <ta e="T212" id="Seg_2584" s="T211">hübeliː-r</ta>
            <ta e="T213" id="Seg_2585" s="T212">ɨraːktaːgɨ-ga</ta>
            <ta e="T214" id="Seg_2586" s="T213">tij-eŋ-ŋin</ta>
            <ta e="T215" id="Seg_2587" s="T214">taŋas-kɨ-n</ta>
            <ta e="T216" id="Seg_2588" s="T215">kör-ü-n-ü-me</ta>
            <ta e="T217" id="Seg_2589" s="T216">oduːrg-u͡ok-tara</ta>
            <ta e="T218" id="Seg_2590" s="T217">toŋkos</ta>
            <ta e="T219" id="Seg_2591" s="T218">ere</ta>
            <ta e="T220" id="Seg_2592" s="T219">gɨn-an</ta>
            <ta e="T221" id="Seg_2593" s="T220">doroːbolos</ta>
            <ta e="T222" id="Seg_2594" s="T221">ahɨː-r-ga-r</ta>
            <ta e="T223" id="Seg_2595" s="T222">dʼon</ta>
            <ta e="T224" id="Seg_2596" s="T223">ahɨː-r-ɨ-n</ta>
            <ta e="T225" id="Seg_2597" s="T224">kör-ön</ta>
            <ta e="T226" id="Seg_2598" s="T225">ahaː</ta>
            <ta e="T227" id="Seg_2599" s="T226">arɨgɨ-nɨ</ta>
            <ta e="T228" id="Seg_2600" s="T227">bert</ta>
            <ta e="T229" id="Seg_2601" s="T228">kɨratɨk</ta>
            <ta e="T230" id="Seg_2602" s="T229">is</ta>
            <ta e="T231" id="Seg_2603" s="T230">u͡ol</ta>
            <ta e="T232" id="Seg_2604" s="T231">Etiŋ</ta>
            <ta e="T233" id="Seg_2605" s="T232">ɨraːktaːgɨ-ga</ta>
            <ta e="T234" id="Seg_2606" s="T233">tij-er</ta>
            <ta e="T235" id="Seg_2607" s="T234">karaːb-ɨ-n</ta>
            <ta e="T236" id="Seg_2608" s="T235">miːn-en</ta>
            <ta e="T237" id="Seg_2609" s="T236">taŋah-ɨ-n</ta>
            <ta e="T238" id="Seg_2610" s="T237">taŋn-an</ta>
            <ta e="T239" id="Seg_2611" s="T238">kurum</ta>
            <ta e="T240" id="Seg_2612" s="T239">bögö</ta>
            <ta e="T241" id="Seg_2613" s="T240">bu͡ol-ar</ta>
            <ta e="T242" id="Seg_2614" s="T241">karaːb-ɨ-nan</ta>
            <ta e="T243" id="Seg_2615" s="T242">tönn-öl-lör</ta>
            <ta e="T244" id="Seg_2616" s="T243">hahɨl</ta>
            <ta e="T245" id="Seg_2617" s="T244">albun</ta>
            <ta e="T246" id="Seg_2618" s="T245">haŋar-ar</ta>
            <ta e="T247" id="Seg_2619" s="T246">bu͡or</ta>
            <ta e="T248" id="Seg_2620" s="T247">golomo-go-r</ta>
            <ta e="T249" id="Seg_2621" s="T248">toktoː-bokko</ta>
            <ta e="T250" id="Seg_2622" s="T249">aːh-aːr</ta>
            <ta e="T251" id="Seg_2623" s="T250">atɨn</ta>
            <ta e="T252" id="Seg_2624" s="T251">ɨraːktaːgɨ</ta>
            <ta e="T253" id="Seg_2625" s="T252">holotu͡oj</ta>
            <ta e="T254" id="Seg_2626" s="T253">gu͡orat-ɨ-gar</ta>
            <ta e="T255" id="Seg_2627" s="T254">čugahaː-n</ta>
            <ta e="T256" id="Seg_2628" s="T255">baran</ta>
            <ta e="T257" id="Seg_2629" s="T256">tokt-oːr</ta>
            <ta e="T258" id="Seg_2630" s="T257">u͡onna</ta>
            <ta e="T259" id="Seg_2631" s="T258">minigi-n</ta>
            <ta e="T260" id="Seg_2632" s="T259">ɨŋɨr-tar-aːr</ta>
            <ta e="T261" id="Seg_2633" s="T260">dʼon-u-m-noru͡ot-u-m</ta>
            <ta e="T262" id="Seg_2634" s="T261">töhö</ta>
            <ta e="T263" id="Seg_2635" s="T262">berke</ta>
            <ta e="T264" id="Seg_2636" s="T263">olor-or-u-n</ta>
            <ta e="T265" id="Seg_2637" s="T264">bil-en</ta>
            <ta e="T266" id="Seg_2638" s="T265">kel</ta>
            <ta e="T267" id="Seg_2639" s="T266">di͡e-r</ta>
            <ta e="T268" id="Seg_2640" s="T267">mini͡e-ke</ta>
            <ta e="T269" id="Seg_2641" s="T268">hahɨl</ta>
            <ta e="T270" id="Seg_2642" s="T269">ep-pit-i-n</ta>
            <ta e="T271" id="Seg_2643" s="T270">kurduk</ta>
            <ta e="T272" id="Seg_2644" s="T271">u͡ol</ta>
            <ta e="T273" id="Seg_2645" s="T272">beje-ti-n</ta>
            <ta e="T274" id="Seg_2646" s="T273">golomo-tu-n</ta>
            <ta e="T275" id="Seg_2647" s="T274">aːh-ar</ta>
            <ta e="T276" id="Seg_2648" s="T275">atɨn</ta>
            <ta e="T277" id="Seg_2649" s="T276">ɨraːktaːgɨ</ta>
            <ta e="T278" id="Seg_2650" s="T277">holotu͡oj</ta>
            <ta e="T279" id="Seg_2651" s="T278">gu͡orat-ɨ-gar</ta>
            <ta e="T280" id="Seg_2652" s="T279">čugahɨː-r</ta>
            <ta e="T281" id="Seg_2653" s="T280">tokt-oːt</ta>
            <ta e="T282" id="Seg_2654" s="T281">tur-an</ta>
            <ta e="T283" id="Seg_2655" s="T282">diː-r</ta>
            <ta e="T284" id="Seg_2656" s="T283">dʼon-u-m-noru͡ot-u-m</ta>
            <ta e="T285" id="Seg_2657" s="T284">töhö</ta>
            <ta e="T286" id="Seg_2658" s="T285">berke</ta>
            <ta e="T287" id="Seg_2659" s="T286">olor-or-u-n</ta>
            <ta e="T288" id="Seg_2660" s="T287">bil-i͡e-m</ta>
            <ta e="T289" id="Seg_2661" s="T288">e-t-e</ta>
            <ta e="T290" id="Seg_2662" s="T289">bili</ta>
            <ta e="T291" id="Seg_2663" s="T290">hahɨl-ɨ-m</ta>
            <ta e="T292" id="Seg_2664" s="T291">kanna</ta>
            <ta e="T293" id="Seg_2665" s="T292">bar-d-a</ta>
            <ta e="T294" id="Seg_2666" s="T293">bil-en</ta>
            <ta e="T295" id="Seg_2667" s="T294">kel-i͡e-n</ta>
            <ta e="T296" id="Seg_2668" s="T295">diː-r</ta>
            <ta e="T297" id="Seg_2669" s="T296">hahɨl</ta>
            <ta e="T298" id="Seg_2670" s="T297">albun</ta>
            <ta e="T299" id="Seg_2671" s="T298">kel-bit-i-ger</ta>
            <ta e="T300" id="Seg_2672" s="T299">haŋar-ar</ta>
            <ta e="T301" id="Seg_2673" s="T300">hahɨl</ta>
            <ta e="T302" id="Seg_2674" s="T301">albun</ta>
            <ta e="T303" id="Seg_2675" s="T302">bar-an</ta>
            <ta e="T304" id="Seg_2676" s="T303">holotu͡oj</ta>
            <ta e="T305" id="Seg_2677" s="T304">gu͡orat</ta>
            <ta e="T306" id="Seg_2678" s="T305">ɨraːktaːgɨ-tɨ-gar</ta>
            <ta e="T307" id="Seg_2679" s="T306">haŋar-ar</ta>
            <ta e="T308" id="Seg_2680" s="T307">Etiŋ</ta>
            <ta e="T309" id="Seg_2681" s="T308">ɨraːktaːgɨ</ta>
            <ta e="T310" id="Seg_2682" s="T309">ehigi-ni</ta>
            <ta e="T311" id="Seg_2683" s="T310">eh-e</ta>
            <ta e="T312" id="Seg_2684" s="T311">kel-l-e</ta>
            <ta e="T313" id="Seg_2685" s="T312">ku͡ot-a</ta>
            <ta e="T314" id="Seg_2686" s="T313">oks-u-ŋ</ta>
            <ta e="T315" id="Seg_2687" s="T314">iti</ta>
            <ta e="T316" id="Seg_2688" s="T315">tɨ͡a-ga</ta>
            <ta e="T317" id="Seg_2689" s="T316">taks-an</ta>
            <ta e="T318" id="Seg_2690" s="T317">hah-ɨ-ŋ</ta>
            <ta e="T319" id="Seg_2691" s="T318">töbö-güt</ta>
            <ta e="T320" id="Seg_2692" s="T319">ere</ta>
            <ta e="T321" id="Seg_2693" s="T320">köst-ör</ta>
            <ta e="T322" id="Seg_2694" s="T321">kurduk</ta>
            <ta e="T323" id="Seg_2695" s="T322">holotu͡oj</ta>
            <ta e="T324" id="Seg_2696" s="T323">gu͡orat</ta>
            <ta e="T325" id="Seg_2697" s="T324">ɨraːktaːgɨ-tɨ-n</ta>
            <ta e="T326" id="Seg_2698" s="T325">noru͡ot-a</ta>
            <ta e="T327" id="Seg_2699" s="T326">barɨ</ta>
            <ta e="T328" id="Seg_2700" s="T327">tɨ͡a-ga</ta>
            <ta e="T329" id="Seg_2701" s="T328">ku͡ot-ar</ta>
            <ta e="T330" id="Seg_2702" s="T329">Etiŋ</ta>
            <ta e="T331" id="Seg_2703" s="T330">ɨraːktaːgɨ-ttan</ta>
            <ta e="T332" id="Seg_2704" s="T331">kuttan-an</ta>
            <ta e="T333" id="Seg_2705" s="T332">hahɨl</ta>
            <ta e="T334" id="Seg_2706" s="T333">albun</ta>
            <ta e="T335" id="Seg_2707" s="T334">u͡ol-ga</ta>
            <ta e="T336" id="Seg_2708" s="T335">ɨtaː-n</ta>
            <ta e="T337" id="Seg_2709" s="T336">kel-er</ta>
            <ta e="T338" id="Seg_2710" s="T337">dʼom-mutu-n</ta>
            <ta e="T339" id="Seg_2711" s="T338">abaːhɨ</ta>
            <ta e="T340" id="Seg_2712" s="T339">ɨraːktaːgɨ-ta</ta>
            <ta e="T341" id="Seg_2713" s="T340">baraː-bɨt</ta>
            <ta e="T342" id="Seg_2714" s="T341">hi͡e-n-hi͡e-n</ta>
            <ta e="T343" id="Seg_2715" s="T342">baran</ta>
            <ta e="T344" id="Seg_2716" s="T343">ol</ta>
            <ta e="T345" id="Seg_2717" s="T344">ojuːr-ga</ta>
            <ta e="T346" id="Seg_2718" s="T345">hah-a</ta>
            <ta e="T347" id="Seg_2719" s="T346">hɨt-al-lar</ta>
            <ta e="T348" id="Seg_2720" s="T347">kɨlɨŋ-ŋɨ-n</ta>
            <ta e="T349" id="Seg_2721" s="T348">körd-ö-s</ta>
            <ta e="T350" id="Seg_2722" s="T349">etiŋ-neː-tin</ta>
            <ta e="T351" id="Seg_2723" s="T350">u͡ol</ta>
            <ta e="T352" id="Seg_2724" s="T351">kɨnn-ɨ-tɨ-ttan</ta>
            <ta e="T353" id="Seg_2725" s="T352">körd-ö-h-ör</ta>
            <ta e="T354" id="Seg_2726" s="T353">etiŋ-neː</ta>
            <ta e="T355" id="Seg_2727" s="T354">di͡e-n</ta>
            <ta e="T356" id="Seg_2728" s="T355">Etiŋ</ta>
            <ta e="T357" id="Seg_2729" s="T356">ɨraːktaːgɨ</ta>
            <ta e="T358" id="Seg_2730" s="T357">dʼe</ta>
            <ta e="T359" id="Seg_2731" s="T358">etiŋ-neː-n</ta>
            <ta e="T360" id="Seg_2732" s="T359">haːj-ar</ta>
            <ta e="T361" id="Seg_2733" s="T360">bili</ta>
            <ta e="T362" id="Seg_2734" s="T361">ojuːr-u</ta>
            <ta e="T363" id="Seg_2735" s="T362">holotu͡oj</ta>
            <ta e="T364" id="Seg_2736" s="T363">gu͡orat</ta>
            <ta e="T365" id="Seg_2737" s="T364">biːr</ta>
            <ta e="T366" id="Seg_2738" s="T365">da</ta>
            <ta e="T367" id="Seg_2739" s="T366">kihi-te</ta>
            <ta e="T368" id="Seg_2740" s="T367">or-pot</ta>
            <ta e="T369" id="Seg_2741" s="T368">u͡ol</ta>
            <ta e="T370" id="Seg_2742" s="T369">bili</ta>
            <ta e="T371" id="Seg_2743" s="T370">holotu͡oj</ta>
            <ta e="T372" id="Seg_2744" s="T371">gu͡orak-ka</ta>
            <ta e="T373" id="Seg_2745" s="T372">kiːr-en</ta>
            <ta e="T374" id="Seg_2746" s="T373">olok-suj-ar</ta>
            <ta e="T375" id="Seg_2747" s="T374">ɨraːktaːgɨ</ta>
            <ta e="T376" id="Seg_2748" s="T375">bu͡ol-ar</ta>
            <ta e="T377" id="Seg_2749" s="T376">Etiŋ</ta>
            <ta e="T378" id="Seg_2750" s="T377">ɨraːktaːgɨ</ta>
            <ta e="T379" id="Seg_2751" s="T378">kɨːh-ɨ-n</ta>
            <ta e="T380" id="Seg_2752" s="T379">ɨl-lag-a</ta>
            <ta e="T381" id="Seg_2753" s="T380">ol</ta>
            <ta e="T382" id="Seg_2754" s="T381">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2755" s="T0">umnahɨt</ta>
            <ta e="T2" id="Seg_2756" s="T1">paːsɨnaj</ta>
            <ta e="T3" id="Seg_2757" s="T2">olor-BIT</ta>
            <ta e="T4" id="Seg_2758" s="T3">bu</ta>
            <ta e="T5" id="Seg_2759" s="T4">paːsɨnaj</ta>
            <ta e="T6" id="Seg_2760" s="T5">öl-BIT-tI-GAr</ta>
            <ta e="T7" id="Seg_2761" s="T6">bu͡or</ta>
            <ta e="T8" id="Seg_2762" s="T7">golomo-tI-GAr</ta>
            <ta e="T9" id="Seg_2763" s="T8">u͡ol</ta>
            <ta e="T10" id="Seg_2764" s="T9">ogo-tA</ta>
            <ta e="T11" id="Seg_2765" s="T10">tulaːjak</ta>
            <ta e="T12" id="Seg_2766" s="T11">kaːl-Ar</ta>
            <ta e="T13" id="Seg_2767" s="T12">bu</ta>
            <ta e="T14" id="Seg_2768" s="T13">ogo</ta>
            <ta e="T15" id="Seg_2769" s="T14">aččɨktaː-An</ta>
            <ta e="T16" id="Seg_2770" s="T15">ölör-A</ta>
            <ta e="T17" id="Seg_2771" s="T16">bu͡ol-Ar</ta>
            <ta e="T18" id="Seg_2772" s="T17">bu-nI</ta>
            <ta e="T19" id="Seg_2773" s="T18">hahɨl</ta>
            <ta e="T20" id="Seg_2774" s="T19">bul-Ar</ta>
            <ta e="T21" id="Seg_2775" s="T20">min</ta>
            <ta e="T22" id="Seg_2776" s="T21">en-n</ta>
            <ta e="T23" id="Seg_2777" s="T22">kihi</ta>
            <ta e="T24" id="Seg_2778" s="T23">oŋor-IAK-m</ta>
            <ta e="T25" id="Seg_2779" s="T24">di͡e-Ar</ta>
            <ta e="T26" id="Seg_2780" s="T25">baːj</ta>
            <ta e="T27" id="Seg_2781" s="T26">kihi</ta>
            <ta e="T28" id="Seg_2782" s="T27">bu͡ol-IAK-ŋ</ta>
            <ta e="T29" id="Seg_2783" s="T28">baːj</ta>
            <ta e="T30" id="Seg_2784" s="T29">dʼaktar-nI</ta>
            <ta e="T31" id="Seg_2785" s="T30">dʼaktar</ta>
            <ta e="T32" id="Seg_2786" s="T31">ɨl-An</ta>
            <ta e="T33" id="Seg_2787" s="T32">bi͡er-IAK-m</ta>
            <ta e="T34" id="Seg_2788" s="T33">innʼe</ta>
            <ta e="T35" id="Seg_2789" s="T34">di͡e-An</ta>
            <ta e="T36" id="Seg_2790" s="T35">baran</ta>
            <ta e="T37" id="Seg_2791" s="T36">hahɨl</ta>
            <ta e="T38" id="Seg_2792" s="T37">etiŋ</ta>
            <ta e="T39" id="Seg_2793" s="T38">ɨraːktaːgɨ-tI-GAr</ta>
            <ta e="T40" id="Seg_2794" s="T39">bar-Ar</ta>
            <ta e="T41" id="Seg_2795" s="T40">etiŋ</ta>
            <ta e="T42" id="Seg_2796" s="T41">ɨraːktaːgɨ-tI-GAr</ta>
            <ta e="T43" id="Seg_2797" s="T42">tij-An</ta>
            <ta e="T44" id="Seg_2798" s="T43">di͡e-Ar</ta>
            <ta e="T45" id="Seg_2799" s="T44">aːt-LAːK</ta>
            <ta e="T46" id="Seg_2800" s="T45">baːj</ta>
            <ta e="T47" id="Seg_2801" s="T46">u͡ol</ta>
            <ta e="T48" id="Seg_2802" s="T47">kihi</ta>
            <ta e="T49" id="Seg_2803" s="T48">baːr</ta>
            <ta e="T50" id="Seg_2804" s="T49">gini</ta>
            <ta e="T51" id="Seg_2805" s="T50">en</ta>
            <ta e="T52" id="Seg_2806" s="T51">kɨːs-GI-n</ta>
            <ta e="T53" id="Seg_2807" s="T52">hu͡orumnʼu-LAː-A-t-A</ta>
            <ta e="T54" id="Seg_2808" s="T53">ɨːt-TI-tA</ta>
            <ta e="T55" id="Seg_2809" s="T54">kɨːs-I-ŋ</ta>
            <ta e="T56" id="Seg_2810" s="T55">huluː-tI-n</ta>
            <ta e="T57" id="Seg_2811" s="T56">tɨ͡a</ta>
            <ta e="T58" id="Seg_2812" s="T57">kɨːl-tI-n</ta>
            <ta e="T59" id="Seg_2813" s="T58">bastɨŋ-tI-n</ta>
            <ta e="T60" id="Seg_2814" s="T59">araːs-tI-n</ta>
            <ta e="T61" id="Seg_2815" s="T60">ɨːt-IAK</ta>
            <ta e="T62" id="Seg_2816" s="T61">bu͡ol-TI-tA</ta>
            <ta e="T63" id="Seg_2817" s="T62">ol</ta>
            <ta e="T64" id="Seg_2818" s="T63">kɨːl-LAr-tI</ta>
            <ta e="T65" id="Seg_2819" s="T64">tus</ta>
            <ta e="T66" id="Seg_2820" s="T65">tus-tI-nAn</ta>
            <ta e="T67" id="Seg_2821" s="T66">dʼi͡e-GA</ta>
            <ta e="T68" id="Seg_2822" s="T67">as</ta>
            <ta e="T69" id="Seg_2823" s="T68">belem-LAː-An</ta>
            <ta e="T70" id="Seg_2824" s="T69">tohuj-IAK-ŋ</ta>
            <ta e="T71" id="Seg_2825" s="T70">ühü</ta>
            <ta e="T72" id="Seg_2826" s="T71">di͡e-Ar</ta>
            <ta e="T73" id="Seg_2827" s="T72">etiŋ</ta>
            <ta e="T74" id="Seg_2828" s="T73">ɨraːktaːgɨ</ta>
            <ta e="T75" id="Seg_2829" s="T74">höbüleː-A-n-BIT</ta>
            <ta e="T76" id="Seg_2830" s="T75">hahɨl</ta>
            <ta e="T77" id="Seg_2831" s="T76">albun</ta>
            <ta e="T78" id="Seg_2832" s="T77">töttörü</ta>
            <ta e="T79" id="Seg_2833" s="T78">hüːr-BIT</ta>
            <ta e="T80" id="Seg_2834" s="T79">tɨ͡a</ta>
            <ta e="T81" id="Seg_2835" s="T80">kɨːl-LAr-tI-n</ta>
            <ta e="T82" id="Seg_2836" s="T81">araːs-tI-n</ta>
            <ta e="T83" id="Seg_2837" s="T82">körüs-An</ta>
            <ta e="T84" id="Seg_2838" s="T83">haŋar-Ar</ta>
            <ta e="T85" id="Seg_2839" s="T84">etiŋ</ta>
            <ta e="T86" id="Seg_2840" s="T85">ɨraːktaːgɨ</ta>
            <ta e="T87" id="Seg_2841" s="T86">baːj-tA-tot-tA</ta>
            <ta e="T88" id="Seg_2842" s="T87">bat-I-mInA</ta>
            <ta e="T89" id="Seg_2843" s="T88">ehigi-GA</ta>
            <ta e="T90" id="Seg_2844" s="T89">as</ta>
            <ta e="T91" id="Seg_2845" s="T90">terij-TI-tA</ta>
            <ta e="T92" id="Seg_2846" s="T91">ol-nI</ta>
            <ta e="T93" id="Seg_2847" s="T92">baran</ta>
            <ta e="T94" id="Seg_2848" s="T93">ahaː-Ar</ta>
            <ta e="T95" id="Seg_2849" s="T94">ühü-GIt</ta>
            <ta e="T96" id="Seg_2850" s="T95">kɨːl</ta>
            <ta e="T97" id="Seg_2851" s="T96">bögö</ta>
            <ta e="T98" id="Seg_2852" s="T97">aːlin</ta>
            <ta e="T99" id="Seg_2853" s="T98">bu͡ol-An</ta>
            <ta e="T100" id="Seg_2854" s="T99">tij-Ar-LAr</ta>
            <ta e="T101" id="Seg_2855" s="T100">ahaː-A</ta>
            <ta e="T102" id="Seg_2856" s="T101">etiŋ</ta>
            <ta e="T103" id="Seg_2857" s="T102">ɨraːktaːgɨ</ta>
            <ta e="T104" id="Seg_2858" s="T103">ü͡ör-Ar</ta>
            <ta e="T105" id="Seg_2859" s="T104">kütü͡öt-I-m</ta>
            <ta e="T106" id="Seg_2860" s="T105">baːj</ta>
            <ta e="T107" id="Seg_2861" s="T106">kihi</ta>
            <ta e="T108" id="Seg_2862" s="T107">e-BIT</ta>
            <ta e="T109" id="Seg_2863" s="T108">di͡e-Ar</ta>
            <ta e="T110" id="Seg_2864" s="T109">kɨːl-LAr</ta>
            <ta e="T111" id="Seg_2865" s="T110">ahaː-A</ta>
            <ta e="T112" id="Seg_2866" s="T111">tur-TAK-TArInA</ta>
            <ta e="T113" id="Seg_2867" s="T112">bili</ta>
            <ta e="T114" id="Seg_2868" s="T113">dʼi͡e-LAr-nI</ta>
            <ta e="T115" id="Seg_2869" s="T114">kataː-An</ta>
            <ta e="T116" id="Seg_2870" s="T115">keːs-Ar-LAr</ta>
            <ta e="T117" id="Seg_2871" s="T116">hahɨl</ta>
            <ta e="T118" id="Seg_2872" s="T117">albun</ta>
            <ta e="T119" id="Seg_2873" s="T118">etiŋ</ta>
            <ta e="T120" id="Seg_2874" s="T119">ɨraːktaːgɨ-GA</ta>
            <ta e="T121" id="Seg_2875" s="T120">tij-Ar</ta>
            <ta e="T122" id="Seg_2876" s="T121">kaja</ta>
            <ta e="T123" id="Seg_2877" s="T122">kɨːl-LAr</ta>
            <ta e="T124" id="Seg_2878" s="T123">kel-TI-LAr</ta>
            <ta e="T125" id="Seg_2879" s="T124">dʼürü</ta>
            <ta e="T126" id="Seg_2880" s="T125">kel-TI-LAr</ta>
            <ta e="T127" id="Seg_2881" s="T126">kütü͡öt</ta>
            <ta e="T128" id="Seg_2882" s="T127">u͡ol</ta>
            <ta e="T129" id="Seg_2883" s="T128">boku͡oj-tA</ta>
            <ta e="T130" id="Seg_2884" s="T129">hu͡ok</ta>
            <ta e="T131" id="Seg_2885" s="T130">hotoru</ta>
            <ta e="T132" id="Seg_2886" s="T131">holotu͡oj</ta>
            <ta e="T133" id="Seg_2887" s="T132">karaːp-I-nAn</ta>
            <ta e="T134" id="Seg_2888" s="T133">kel-IAK.[tA]</ta>
            <ta e="T135" id="Seg_2889" s="T134">di͡e-Ar</ta>
            <ta e="T136" id="Seg_2890" s="T135">u͡ol-tI-GAr</ta>
            <ta e="T137" id="Seg_2891" s="T136">töttörü</ta>
            <ta e="T138" id="Seg_2892" s="T137">hüːr-An</ta>
            <ta e="T139" id="Seg_2893" s="T138">tij-Ar</ta>
            <ta e="T140" id="Seg_2894" s="T139">talak-I-nAn</ta>
            <ta e="T141" id="Seg_2895" s="T140">kuhagan</ta>
            <ta e="T142" id="Seg_2896" s="T141">bolu͡ot</ta>
            <ta e="T143" id="Seg_2897" s="T142">oŋor-TAr-Ar</ta>
            <ta e="T144" id="Seg_2898" s="T143">araːs</ta>
            <ta e="T145" id="Seg_2899" s="T144">hir</ta>
            <ta e="T146" id="Seg_2900" s="T145">hibekki-tI-nAn</ta>
            <ta e="T147" id="Seg_2901" s="T146">himeː-A-t-Ar</ta>
            <ta e="T148" id="Seg_2902" s="T147">ɨraːk-ttAn</ta>
            <ta e="T149" id="Seg_2903" s="T148">hi͡ederej</ta>
            <ta e="T150" id="Seg_2904" s="T149">bosku͡oj</ta>
            <ta e="T151" id="Seg_2905" s="T150">bu͡ol-An</ta>
            <ta e="T152" id="Seg_2906" s="T151">köhün-Ar</ta>
            <ta e="T153" id="Seg_2907" s="T152">tɨ͡al</ta>
            <ta e="T154" id="Seg_2908" s="T153">kün</ta>
            <ta e="T155" id="Seg_2909" s="T154">etiŋ</ta>
            <ta e="T156" id="Seg_2910" s="T155">ɨraːktaːgɨ-GA</ta>
            <ta e="T157" id="Seg_2911" s="T156">uhun-Ar-LAr</ta>
            <ta e="T158" id="Seg_2912" s="T157">etiŋ</ta>
            <ta e="T159" id="Seg_2913" s="T158">ɨraːktaːgɨ</ta>
            <ta e="T160" id="Seg_2914" s="T159">kör-BIT-tA</ta>
            <ta e="T161" id="Seg_2915" s="T160">bajgal</ta>
            <ta e="T162" id="Seg_2916" s="T161">üstün</ta>
            <ta e="T163" id="Seg_2917" s="T162">holotu͡oj</ta>
            <ta e="T164" id="Seg_2918" s="T163">karaːp</ta>
            <ta e="T165" id="Seg_2919" s="T164">is-Ar</ta>
            <ta e="T166" id="Seg_2920" s="T165">hi͡ederej</ta>
            <ta e="T167" id="Seg_2921" s="T166">öŋnöːk</ta>
            <ta e="T168" id="Seg_2922" s="T167">ɨraːktaːgɨ</ta>
            <ta e="T169" id="Seg_2923" s="T168">ü͡ör-Ar</ta>
            <ta e="T170" id="Seg_2924" s="T169">tɨ͡al</ta>
            <ta e="T171" id="Seg_2925" s="T170">tüs-BIT-tI-GAr</ta>
            <ta e="T172" id="Seg_2926" s="T171">bolu͡ot</ta>
            <ta e="T173" id="Seg_2927" s="T172">ɨs-I-LIN-An</ta>
            <ta e="T174" id="Seg_2928" s="T173">kaːl-Ar</ta>
            <ta e="T175" id="Seg_2929" s="T174">hahɨl</ta>
            <ta e="T176" id="Seg_2930" s="T175">albun</ta>
            <ta e="T177" id="Seg_2931" s="T176">etiŋ</ta>
            <ta e="T178" id="Seg_2932" s="T177">ɨraːktaːgɨ-GA</ta>
            <ta e="T179" id="Seg_2933" s="T178">hüːr-An</ta>
            <ta e="T180" id="Seg_2934" s="T179">tij-Ar</ta>
            <ta e="T181" id="Seg_2935" s="T180">di͡e-Ar</ta>
            <ta e="T182" id="Seg_2936" s="T181">i͡edeːn</ta>
            <ta e="T183" id="Seg_2937" s="T182">bu͡ol-TI-tA</ta>
            <ta e="T184" id="Seg_2938" s="T183">holotu͡oj</ta>
            <ta e="T185" id="Seg_2939" s="T184">karaːp</ta>
            <ta e="T186" id="Seg_2940" s="T185">timir-TI-tA</ta>
            <ta e="T187" id="Seg_2941" s="T186">kütü͡öt-I-ŋ</ta>
            <ta e="T188" id="Seg_2942" s="T187">kihi</ta>
            <ta e="T189" id="Seg_2943" s="T188">berde</ta>
            <ta e="T190" id="Seg_2944" s="T189">bu͡ol-An</ta>
            <ta e="T191" id="Seg_2945" s="T190">ort-TI-tA</ta>
            <ta e="T192" id="Seg_2946" s="T191">tahagas-tA</ta>
            <ta e="T193" id="Seg_2947" s="T192">barɨ-tA</ta>
            <ta e="T194" id="Seg_2948" s="T193">uː-GA</ta>
            <ta e="T195" id="Seg_2949" s="T194">bar-TI-tA</ta>
            <ta e="T196" id="Seg_2950" s="T195">beje-tA</ta>
            <ta e="T197" id="Seg_2951" s="T196">hɨgɨnʼak</ta>
            <ta e="T198" id="Seg_2952" s="T197">kaːl-TI-tA</ta>
            <ta e="T199" id="Seg_2953" s="T198">taŋas-TA</ta>
            <ta e="T200" id="Seg_2954" s="T199">ɨːt</ta>
            <ta e="T201" id="Seg_2955" s="T200">etiŋ</ta>
            <ta e="T202" id="Seg_2956" s="T201">ɨraːktaːgɨ</ta>
            <ta e="T203" id="Seg_2957" s="T202">u͡ol-GA</ta>
            <ta e="T204" id="Seg_2958" s="T203">bastɨŋ</ta>
            <ta e="T205" id="Seg_2959" s="T204">taŋas-nI</ta>
            <ta e="T206" id="Seg_2960" s="T205">beje-tI-n</ta>
            <ta e="T207" id="Seg_2961" s="T206">karaːp-tI-n</ta>
            <ta e="T208" id="Seg_2962" s="T207">ɨːt-Ar</ta>
            <ta e="T209" id="Seg_2963" s="T208">hahɨl</ta>
            <ta e="T210" id="Seg_2964" s="T209">albun</ta>
            <ta e="T211" id="Seg_2965" s="T210">u͡ol-tI-n</ta>
            <ta e="T212" id="Seg_2966" s="T211">hübeleː-Ar</ta>
            <ta e="T213" id="Seg_2967" s="T212">ɨraːktaːgɨ-GA</ta>
            <ta e="T214" id="Seg_2968" s="T213">tij-An-GIn</ta>
            <ta e="T215" id="Seg_2969" s="T214">taŋas-GI-n</ta>
            <ta e="T216" id="Seg_2970" s="T215">kör-I-n-I-m</ta>
            <ta e="T217" id="Seg_2971" s="T216">oduːrgaː-IAK-LArA</ta>
            <ta e="T218" id="Seg_2972" s="T217">toŋkos</ta>
            <ta e="T219" id="Seg_2973" s="T218">ere</ta>
            <ta e="T220" id="Seg_2974" s="T219">gɨn-An</ta>
            <ta e="T221" id="Seg_2975" s="T220">doroːbolos</ta>
            <ta e="T222" id="Seg_2976" s="T221">ahaː-Ar-GA-r</ta>
            <ta e="T223" id="Seg_2977" s="T222">dʼon</ta>
            <ta e="T224" id="Seg_2978" s="T223">ahaː-Ar-tI-n</ta>
            <ta e="T225" id="Seg_2979" s="T224">kör-An</ta>
            <ta e="T226" id="Seg_2980" s="T225">ahaː</ta>
            <ta e="T227" id="Seg_2981" s="T226">aragiː-nI</ta>
            <ta e="T228" id="Seg_2982" s="T227">bert</ta>
            <ta e="T229" id="Seg_2983" s="T228">kɨratɨk</ta>
            <ta e="T230" id="Seg_2984" s="T229">is</ta>
            <ta e="T231" id="Seg_2985" s="T230">u͡ol</ta>
            <ta e="T232" id="Seg_2986" s="T231">etiŋ</ta>
            <ta e="T233" id="Seg_2987" s="T232">ɨraːktaːgɨ-GA</ta>
            <ta e="T234" id="Seg_2988" s="T233">tij-Ar</ta>
            <ta e="T235" id="Seg_2989" s="T234">karaːp-tI-n</ta>
            <ta e="T236" id="Seg_2990" s="T235">miːn-An</ta>
            <ta e="T237" id="Seg_2991" s="T236">taŋas-tI-n</ta>
            <ta e="T238" id="Seg_2992" s="T237">taŋɨn-An</ta>
            <ta e="T239" id="Seg_2993" s="T238">kurum</ta>
            <ta e="T240" id="Seg_2994" s="T239">bögö</ta>
            <ta e="T241" id="Seg_2995" s="T240">bu͡ol-Ar</ta>
            <ta e="T242" id="Seg_2996" s="T241">karaːp-tI-nAn</ta>
            <ta e="T243" id="Seg_2997" s="T242">tönün-Ar-LAr</ta>
            <ta e="T244" id="Seg_2998" s="T243">hahɨl</ta>
            <ta e="T245" id="Seg_2999" s="T244">albun</ta>
            <ta e="T246" id="Seg_3000" s="T245">haŋar-Ar</ta>
            <ta e="T247" id="Seg_3001" s="T246">bu͡or</ta>
            <ta e="T248" id="Seg_3002" s="T247">golomo-GA-r</ta>
            <ta e="T249" id="Seg_3003" s="T248">toktoː-BAkkA</ta>
            <ta e="T250" id="Seg_3004" s="T249">aːs-Aːr</ta>
            <ta e="T251" id="Seg_3005" s="T250">atɨn</ta>
            <ta e="T252" id="Seg_3006" s="T251">ɨraːktaːgɨ</ta>
            <ta e="T253" id="Seg_3007" s="T252">holotu͡oj</ta>
            <ta e="T254" id="Seg_3008" s="T253">gu͡orat-tI-GAr</ta>
            <ta e="T255" id="Seg_3009" s="T254">čugahaː-An</ta>
            <ta e="T256" id="Seg_3010" s="T255">baran</ta>
            <ta e="T257" id="Seg_3011" s="T256">toktoː-Aːr</ta>
            <ta e="T258" id="Seg_3012" s="T257">u͡onna</ta>
            <ta e="T259" id="Seg_3013" s="T258">min-n</ta>
            <ta e="T260" id="Seg_3014" s="T259">ɨŋɨr-TAr-Aːr</ta>
            <ta e="T261" id="Seg_3015" s="T260">dʼon-I-m-noru͡ot-I-m</ta>
            <ta e="T262" id="Seg_3016" s="T261">töhö</ta>
            <ta e="T263" id="Seg_3017" s="T262">berke</ta>
            <ta e="T264" id="Seg_3018" s="T263">olor-Ar-tI-n</ta>
            <ta e="T265" id="Seg_3019" s="T264">bil-An</ta>
            <ta e="T266" id="Seg_3020" s="T265">kel</ta>
            <ta e="T267" id="Seg_3021" s="T266">di͡e-Aːr</ta>
            <ta e="T268" id="Seg_3022" s="T267">min-GA</ta>
            <ta e="T269" id="Seg_3023" s="T268">hahɨl</ta>
            <ta e="T270" id="Seg_3024" s="T269">et-BIT-tI-n</ta>
            <ta e="T271" id="Seg_3025" s="T270">kurduk</ta>
            <ta e="T272" id="Seg_3026" s="T271">u͡ol</ta>
            <ta e="T273" id="Seg_3027" s="T272">beje-tI-n</ta>
            <ta e="T274" id="Seg_3028" s="T273">golomo-tI-n</ta>
            <ta e="T275" id="Seg_3029" s="T274">aːs-Ar</ta>
            <ta e="T276" id="Seg_3030" s="T275">atɨn</ta>
            <ta e="T277" id="Seg_3031" s="T276">ɨraːktaːgɨ</ta>
            <ta e="T278" id="Seg_3032" s="T277">holotu͡oj</ta>
            <ta e="T279" id="Seg_3033" s="T278">gu͡orat-tI-GAr</ta>
            <ta e="T280" id="Seg_3034" s="T279">čugahaː-Ar</ta>
            <ta e="T281" id="Seg_3035" s="T280">toktoː-AːT</ta>
            <ta e="T282" id="Seg_3036" s="T281">tur-An</ta>
            <ta e="T283" id="Seg_3037" s="T282">di͡e-Ar</ta>
            <ta e="T284" id="Seg_3038" s="T283">dʼon-I-m-noru͡ot-I-m</ta>
            <ta e="T285" id="Seg_3039" s="T284">töhö</ta>
            <ta e="T286" id="Seg_3040" s="T285">berke</ta>
            <ta e="T287" id="Seg_3041" s="T286">olor-Ar-tI-n</ta>
            <ta e="T288" id="Seg_3042" s="T287">bil-IAK-m</ta>
            <ta e="T289" id="Seg_3043" s="T288">e-TI-tA</ta>
            <ta e="T290" id="Seg_3044" s="T289">bili</ta>
            <ta e="T291" id="Seg_3045" s="T290">hahɨl-I-m</ta>
            <ta e="T292" id="Seg_3046" s="T291">kanna</ta>
            <ta e="T293" id="Seg_3047" s="T292">bar-TI-tA</ta>
            <ta e="T294" id="Seg_3048" s="T293">bil-An</ta>
            <ta e="T295" id="Seg_3049" s="T294">kel-IAK.[tI]-n</ta>
            <ta e="T296" id="Seg_3050" s="T295">di͡e-Ar</ta>
            <ta e="T297" id="Seg_3051" s="T296">hahɨl</ta>
            <ta e="T298" id="Seg_3052" s="T297">albun</ta>
            <ta e="T299" id="Seg_3053" s="T298">kel-BIT-tI-GAr</ta>
            <ta e="T300" id="Seg_3054" s="T299">haŋar-Ar</ta>
            <ta e="T301" id="Seg_3055" s="T300">hahɨl</ta>
            <ta e="T302" id="Seg_3056" s="T301">albun</ta>
            <ta e="T303" id="Seg_3057" s="T302">bar-An</ta>
            <ta e="T304" id="Seg_3058" s="T303">holotu͡oj</ta>
            <ta e="T305" id="Seg_3059" s="T304">gu͡orat</ta>
            <ta e="T306" id="Seg_3060" s="T305">ɨraːktaːgɨ-tI-GAr</ta>
            <ta e="T307" id="Seg_3061" s="T306">haŋar-Ar</ta>
            <ta e="T308" id="Seg_3062" s="T307">etiŋ</ta>
            <ta e="T309" id="Seg_3063" s="T308">ɨraːktaːgɨ</ta>
            <ta e="T310" id="Seg_3064" s="T309">ehigi-nI</ta>
            <ta e="T311" id="Seg_3065" s="T310">es-A</ta>
            <ta e="T312" id="Seg_3066" s="T311">kel-TI-tA</ta>
            <ta e="T313" id="Seg_3067" s="T312">ku͡ot-A</ta>
            <ta e="T314" id="Seg_3068" s="T313">ogus-I-ŋ</ta>
            <ta e="T315" id="Seg_3069" s="T314">iti</ta>
            <ta e="T316" id="Seg_3070" s="T315">tɨ͡a-GA</ta>
            <ta e="T317" id="Seg_3071" s="T316">tagɨs-An</ta>
            <ta e="T318" id="Seg_3072" s="T317">has-I-ŋ</ta>
            <ta e="T319" id="Seg_3073" s="T318">töbö-GIt</ta>
            <ta e="T320" id="Seg_3074" s="T319">ere</ta>
            <ta e="T321" id="Seg_3075" s="T320">köhün-Ar</ta>
            <ta e="T322" id="Seg_3076" s="T321">kurduk</ta>
            <ta e="T323" id="Seg_3077" s="T322">holotu͡oj</ta>
            <ta e="T324" id="Seg_3078" s="T323">gu͡orat</ta>
            <ta e="T325" id="Seg_3079" s="T324">ɨraːktaːgɨ-tI-n</ta>
            <ta e="T326" id="Seg_3080" s="T325">noru͡ot-tA</ta>
            <ta e="T327" id="Seg_3081" s="T326">barɨ</ta>
            <ta e="T328" id="Seg_3082" s="T327">tɨ͡a-GA</ta>
            <ta e="T329" id="Seg_3083" s="T328">ku͡ot-Ar</ta>
            <ta e="T330" id="Seg_3084" s="T329">etiŋ</ta>
            <ta e="T331" id="Seg_3085" s="T330">ɨraːktaːgɨ-ttAn</ta>
            <ta e="T332" id="Seg_3086" s="T331">kuttan-An</ta>
            <ta e="T333" id="Seg_3087" s="T332">hahɨl</ta>
            <ta e="T334" id="Seg_3088" s="T333">albun</ta>
            <ta e="T335" id="Seg_3089" s="T334">u͡ol-GA</ta>
            <ta e="T336" id="Seg_3090" s="T335">ɨtaː-An</ta>
            <ta e="T337" id="Seg_3091" s="T336">kel-Ar</ta>
            <ta e="T338" id="Seg_3092" s="T337">dʼon-BItI-n</ta>
            <ta e="T339" id="Seg_3093" s="T338">abaːhɨ</ta>
            <ta e="T340" id="Seg_3094" s="T339">ɨraːktaːgɨ-tA</ta>
            <ta e="T341" id="Seg_3095" s="T340">baraː-BIT</ta>
            <ta e="T342" id="Seg_3096" s="T341">hi͡e-An-hi͡e-An</ta>
            <ta e="T343" id="Seg_3097" s="T342">baran</ta>
            <ta e="T344" id="Seg_3098" s="T343">ol</ta>
            <ta e="T345" id="Seg_3099" s="T344">ojuːr-GA</ta>
            <ta e="T346" id="Seg_3100" s="T345">has-A</ta>
            <ta e="T347" id="Seg_3101" s="T346">hɨt-Ar-LAr</ta>
            <ta e="T348" id="Seg_3102" s="T347">kɨlɨn-GI-n</ta>
            <ta e="T349" id="Seg_3103" s="T348">kördöː-A-s</ta>
            <ta e="T350" id="Seg_3104" s="T349">etiŋ-LAː-TIn</ta>
            <ta e="T351" id="Seg_3105" s="T350">u͡ol</ta>
            <ta e="T352" id="Seg_3106" s="T351">kɨlɨn-I-tI-ttAn</ta>
            <ta e="T353" id="Seg_3107" s="T352">kördöː-A-s-Ar</ta>
            <ta e="T354" id="Seg_3108" s="T353">etiŋ-LAː</ta>
            <ta e="T355" id="Seg_3109" s="T354">di͡e-An</ta>
            <ta e="T356" id="Seg_3110" s="T355">etiŋ</ta>
            <ta e="T357" id="Seg_3111" s="T356">ɨraːktaːgɨ</ta>
            <ta e="T358" id="Seg_3112" s="T357">dʼe</ta>
            <ta e="T359" id="Seg_3113" s="T358">etiŋ-LAː-An</ta>
            <ta e="T360" id="Seg_3114" s="T359">haːj-Ar</ta>
            <ta e="T361" id="Seg_3115" s="T360">bili</ta>
            <ta e="T362" id="Seg_3116" s="T361">ojuːr-nI</ta>
            <ta e="T363" id="Seg_3117" s="T362">holotu͡oj</ta>
            <ta e="T364" id="Seg_3118" s="T363">gu͡orat</ta>
            <ta e="T365" id="Seg_3119" s="T364">biːr</ta>
            <ta e="T366" id="Seg_3120" s="T365">da</ta>
            <ta e="T367" id="Seg_3121" s="T366">kihi-tA</ta>
            <ta e="T368" id="Seg_3122" s="T367">ort-BAT</ta>
            <ta e="T369" id="Seg_3123" s="T368">u͡ol</ta>
            <ta e="T370" id="Seg_3124" s="T369">bili</ta>
            <ta e="T371" id="Seg_3125" s="T370">holotu͡oj</ta>
            <ta e="T372" id="Seg_3126" s="T371">gu͡orat-GA</ta>
            <ta e="T373" id="Seg_3127" s="T372">kiːr-An</ta>
            <ta e="T374" id="Seg_3128" s="T373">olok-TIj-Ar</ta>
            <ta e="T375" id="Seg_3129" s="T374">ɨraːktaːgɨ</ta>
            <ta e="T376" id="Seg_3130" s="T375">bu͡ol-Ar</ta>
            <ta e="T377" id="Seg_3131" s="T376">etiŋ</ta>
            <ta e="T378" id="Seg_3132" s="T377">ɨraːktaːgɨ</ta>
            <ta e="T379" id="Seg_3133" s="T378">kɨːs-tI-n</ta>
            <ta e="T380" id="Seg_3134" s="T379">ɨl-TAK-tA</ta>
            <ta e="T381" id="Seg_3135" s="T380">ol</ta>
            <ta e="T382" id="Seg_3136" s="T381">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_3137" s="T0">poor</ta>
            <ta e="T2" id="Seg_3138" s="T1">farmer.[NOM]</ta>
            <ta e="T3" id="Seg_3139" s="T2">live-PST2.[3SG]</ta>
            <ta e="T4" id="Seg_3140" s="T3">this</ta>
            <ta e="T5" id="Seg_3141" s="T4">farmer.[NOM]</ta>
            <ta e="T6" id="Seg_3142" s="T5">die-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T7" id="Seg_3143" s="T6">earth.[NOM]</ta>
            <ta e="T8" id="Seg_3144" s="T7">pit_dwelling-3SG-DAT/LOC</ta>
            <ta e="T9" id="Seg_3145" s="T8">boy.[NOM]</ta>
            <ta e="T10" id="Seg_3146" s="T9">child-3SG.[NOM]</ta>
            <ta e="T11" id="Seg_3147" s="T10">orphan.[NOM]</ta>
            <ta e="T12" id="Seg_3148" s="T11">stay-PRS.[3SG]</ta>
            <ta e="T13" id="Seg_3149" s="T12">this</ta>
            <ta e="T14" id="Seg_3150" s="T13">child.[NOM]</ta>
            <ta e="T15" id="Seg_3151" s="T14">hunger-CVB.SEQ</ta>
            <ta e="T16" id="Seg_3152" s="T15">kill-CVB.SIM</ta>
            <ta e="T17" id="Seg_3153" s="T16">be-PRS.[3SG]</ta>
            <ta e="T18" id="Seg_3154" s="T17">this-ACC</ta>
            <ta e="T19" id="Seg_3155" s="T18">fox.[NOM]</ta>
            <ta e="T20" id="Seg_3156" s="T19">find-PRS.[3SG]</ta>
            <ta e="T21" id="Seg_3157" s="T20">1SG.[NOM]</ta>
            <ta e="T22" id="Seg_3158" s="T21">2SG-ACC</ta>
            <ta e="T23" id="Seg_3159" s="T22">human.being.[NOM]</ta>
            <ta e="T24" id="Seg_3160" s="T23">make-FUT-1SG</ta>
            <ta e="T25" id="Seg_3161" s="T24">say-PRS.[3SG]</ta>
            <ta e="T26" id="Seg_3162" s="T25">rich</ta>
            <ta e="T27" id="Seg_3163" s="T26">human.being.[NOM]</ta>
            <ta e="T28" id="Seg_3164" s="T27">be-FUT-2SG</ta>
            <ta e="T29" id="Seg_3165" s="T28">rich</ta>
            <ta e="T30" id="Seg_3166" s="T29">woman-ACC</ta>
            <ta e="T31" id="Seg_3167" s="T30">woman.[NOM]</ta>
            <ta e="T32" id="Seg_3168" s="T31">take-CVB.SEQ</ta>
            <ta e="T33" id="Seg_3169" s="T32">give-FUT-1SG</ta>
            <ta e="T34" id="Seg_3170" s="T33">so</ta>
            <ta e="T35" id="Seg_3171" s="T34">say-CVB.SEQ</ta>
            <ta e="T36" id="Seg_3172" s="T35">after</ta>
            <ta e="T37" id="Seg_3173" s="T36">fox.[NOM]</ta>
            <ta e="T38" id="Seg_3174" s="T37">Thunder</ta>
            <ta e="T39" id="Seg_3175" s="T38">czar-3SG-DAT/LOC</ta>
            <ta e="T40" id="Seg_3176" s="T39">go-PRS.[3SG]</ta>
            <ta e="T41" id="Seg_3177" s="T40">Thunder</ta>
            <ta e="T42" id="Seg_3178" s="T41">czar-3SG-DAT/LOC</ta>
            <ta e="T43" id="Seg_3179" s="T42">reach-CVB.SEQ</ta>
            <ta e="T44" id="Seg_3180" s="T43">say-PRS.[3SG]</ta>
            <ta e="T45" id="Seg_3181" s="T44">rank-PROPR</ta>
            <ta e="T46" id="Seg_3182" s="T45">rich</ta>
            <ta e="T47" id="Seg_3183" s="T46">boy.[NOM]</ta>
            <ta e="T48" id="Seg_3184" s="T47">human.being.[NOM]</ta>
            <ta e="T49" id="Seg_3185" s="T48">there.is</ta>
            <ta e="T50" id="Seg_3186" s="T49">3SG.[NOM]</ta>
            <ta e="T51" id="Seg_3187" s="T50">2SG.[NOM]</ta>
            <ta e="T52" id="Seg_3188" s="T51">daughter-2SG-ACC</ta>
            <ta e="T53" id="Seg_3189" s="T52">courting-VBZ-EP-CAUS-CVB.SIM</ta>
            <ta e="T54" id="Seg_3190" s="T53">send-PST1-3SG</ta>
            <ta e="T55" id="Seg_3191" s="T54">daughter-EP-2SG.[NOM]</ta>
            <ta e="T56" id="Seg_3192" s="T55">bride.price-3SG-ACC</ta>
            <ta e="T57" id="Seg_3193" s="T56">forest.[NOM]</ta>
            <ta e="T58" id="Seg_3194" s="T57">animal-3SG-ACC</ta>
            <ta e="T59" id="Seg_3195" s="T58">best-3SG-ACC</ta>
            <ta e="T60" id="Seg_3196" s="T59">different-3SG-ACC</ta>
            <ta e="T61" id="Seg_3197" s="T60">send-PTCP.FUT</ta>
            <ta e="T62" id="Seg_3198" s="T61">be-PST1-3SG</ta>
            <ta e="T63" id="Seg_3199" s="T62">that</ta>
            <ta e="T64" id="Seg_3200" s="T63">animal-PL-ACC</ta>
            <ta e="T65" id="Seg_3201" s="T64">side.[NOM]</ta>
            <ta e="T66" id="Seg_3202" s="T65">side-3SG-INSTR</ta>
            <ta e="T67" id="Seg_3203" s="T66">house-DAT/LOC</ta>
            <ta e="T68" id="Seg_3204" s="T67">food.[NOM]</ta>
            <ta e="T69" id="Seg_3205" s="T68">ready-VBZ-CVB.SEQ</ta>
            <ta e="T70" id="Seg_3206" s="T69">receive-FUT-2SG</ta>
            <ta e="T71" id="Seg_3207" s="T70">it.is.said</ta>
            <ta e="T72" id="Seg_3208" s="T71">say-PRS.[3SG]</ta>
            <ta e="T73" id="Seg_3209" s="T72">Thunder</ta>
            <ta e="T74" id="Seg_3210" s="T73">czar.[NOM]</ta>
            <ta e="T75" id="Seg_3211" s="T74">agree-EP-MED-PST2.[3SG]</ta>
            <ta e="T76" id="Seg_3212" s="T75">fox.[NOM]</ta>
            <ta e="T77" id="Seg_3213" s="T76">deceiver.[NOM]</ta>
            <ta e="T78" id="Seg_3214" s="T77">back</ta>
            <ta e="T79" id="Seg_3215" s="T78">run-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_3216" s="T79">forest.[NOM]</ta>
            <ta e="T81" id="Seg_3217" s="T80">animal-PL-3SG-ACC</ta>
            <ta e="T82" id="Seg_3218" s="T81">different-3SG-ACC</ta>
            <ta e="T83" id="Seg_3219" s="T82">meet-CVB.SEQ</ta>
            <ta e="T84" id="Seg_3220" s="T83">speak-PRS.[3SG]</ta>
            <ta e="T85" id="Seg_3221" s="T84">Thunder</ta>
            <ta e="T86" id="Seg_3222" s="T85">czar.[NOM]</ta>
            <ta e="T87" id="Seg_3223" s="T86">wealth-3SG.[NOM]-satiety-3SG.[NOM]</ta>
            <ta e="T88" id="Seg_3224" s="T87">refuse-EP-NEG.CVB</ta>
            <ta e="T89" id="Seg_3225" s="T88">2PL-DAT/LOC</ta>
            <ta e="T90" id="Seg_3226" s="T89">food.[NOM]</ta>
            <ta e="T91" id="Seg_3227" s="T90">prepare-PST1-3SG</ta>
            <ta e="T92" id="Seg_3228" s="T91">that-ACC</ta>
            <ta e="T93" id="Seg_3229" s="T92">after</ta>
            <ta e="T94" id="Seg_3230" s="T93">eat-PTCP.PRS</ta>
            <ta e="T95" id="Seg_3231" s="T94">it.is.said-2PL</ta>
            <ta e="T96" id="Seg_3232" s="T95">animal.[NOM]</ta>
            <ta e="T97" id="Seg_3233" s="T96">a.lot</ta>
            <ta e="T98" id="Seg_3234" s="T97">herd.[NOM]</ta>
            <ta e="T99" id="Seg_3235" s="T98">be-CVB.SEQ</ta>
            <ta e="T100" id="Seg_3236" s="T99">reach-PRS-3PL</ta>
            <ta e="T101" id="Seg_3237" s="T100">eat-CVB.SIM</ta>
            <ta e="T102" id="Seg_3238" s="T101">Thunder</ta>
            <ta e="T103" id="Seg_3239" s="T102">czar.[NOM]</ta>
            <ta e="T104" id="Seg_3240" s="T103">be.happy-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_3241" s="T104">son_in_law-EP-1SG.[NOM]</ta>
            <ta e="T106" id="Seg_3242" s="T105">rich</ta>
            <ta e="T107" id="Seg_3243" s="T106">human.being.[NOM]</ta>
            <ta e="T108" id="Seg_3244" s="T107">be-PST2.[3SG]</ta>
            <ta e="T109" id="Seg_3245" s="T108">think-PRS.[3SG]</ta>
            <ta e="T110" id="Seg_3246" s="T109">animal-PL.[NOM]</ta>
            <ta e="T111" id="Seg_3247" s="T110">eat-CVB.SIM</ta>
            <ta e="T112" id="Seg_3248" s="T111">stand-TEMP-3PL</ta>
            <ta e="T113" id="Seg_3249" s="T112">exactly.this</ta>
            <ta e="T114" id="Seg_3250" s="T113">house-PL-ACC</ta>
            <ta e="T115" id="Seg_3251" s="T114">lock-CVB.SEQ</ta>
            <ta e="T116" id="Seg_3252" s="T115">throw-PRS-3PL</ta>
            <ta e="T117" id="Seg_3253" s="T116">fox.[NOM]</ta>
            <ta e="T118" id="Seg_3254" s="T117">deceiver.[NOM]</ta>
            <ta e="T119" id="Seg_3255" s="T118">Thunder</ta>
            <ta e="T120" id="Seg_3256" s="T119">czar-DAT/LOC</ta>
            <ta e="T121" id="Seg_3257" s="T120">reach-PRS.[3SG]</ta>
            <ta e="T122" id="Seg_3258" s="T121">well</ta>
            <ta e="T123" id="Seg_3259" s="T122">animal-PL.[NOM]</ta>
            <ta e="T124" id="Seg_3260" s="T123">come-PST1-3PL</ta>
            <ta e="T125" id="Seg_3261" s="T124">Q</ta>
            <ta e="T126" id="Seg_3262" s="T125">come-PST1-3PL</ta>
            <ta e="T127" id="Seg_3263" s="T126">son_in_law.[NOM]</ta>
            <ta e="T128" id="Seg_3264" s="T127">boy.[NOM]</ta>
            <ta e="T129" id="Seg_3265" s="T128">free.time-POSS</ta>
            <ta e="T130" id="Seg_3266" s="T129">NEG.[3SG]</ta>
            <ta e="T131" id="Seg_3267" s="T130">soon</ta>
            <ta e="T132" id="Seg_3268" s="T131">gold</ta>
            <ta e="T133" id="Seg_3269" s="T132">ship-EP-INSTR</ta>
            <ta e="T134" id="Seg_3270" s="T133">come-FUT.[3SG]</ta>
            <ta e="T135" id="Seg_3271" s="T134">say-PRS.[3SG]</ta>
            <ta e="T136" id="Seg_3272" s="T135">boy-3SG-DAT/LOC</ta>
            <ta e="T137" id="Seg_3273" s="T136">back</ta>
            <ta e="T138" id="Seg_3274" s="T137">run-CVB.SEQ</ta>
            <ta e="T139" id="Seg_3275" s="T138">reach-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_3276" s="T139">willow.bush-EP-INSTR</ta>
            <ta e="T141" id="Seg_3277" s="T140">bad</ta>
            <ta e="T142" id="Seg_3278" s="T141">raft.[NOM]</ta>
            <ta e="T143" id="Seg_3279" s="T142">make-CAUS-PRS.[3SG]</ta>
            <ta e="T144" id="Seg_3280" s="T143">different</ta>
            <ta e="T145" id="Seg_3281" s="T144">earth.[NOM]</ta>
            <ta e="T146" id="Seg_3282" s="T145">flower-3SG-INSTR</ta>
            <ta e="T147" id="Seg_3283" s="T146">decorate-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T148" id="Seg_3284" s="T147">distant-ABL</ta>
            <ta e="T149" id="Seg_3285" s="T148">elegant.[NOM]</ta>
            <ta e="T150" id="Seg_3286" s="T149">beautiful.[NOM]</ta>
            <ta e="T151" id="Seg_3287" s="T150">be-CVB.SEQ</ta>
            <ta e="T152" id="Seg_3288" s="T151">to.be.on.view-PRS.[3SG]</ta>
            <ta e="T153" id="Seg_3289" s="T152">wind.[NOM]</ta>
            <ta e="T154" id="Seg_3290" s="T153">day.[NOM]</ta>
            <ta e="T155" id="Seg_3291" s="T154">Thunder</ta>
            <ta e="T156" id="Seg_3292" s="T155">czar-DAT/LOC</ta>
            <ta e="T157" id="Seg_3293" s="T156">flow-PRS-3PL</ta>
            <ta e="T158" id="Seg_3294" s="T157">Thunder</ta>
            <ta e="T159" id="Seg_3295" s="T158">czar.[NOM]</ta>
            <ta e="T160" id="Seg_3296" s="T159">see-PST2-3SG</ta>
            <ta e="T161" id="Seg_3297" s="T160">sea.[NOM]</ta>
            <ta e="T162" id="Seg_3298" s="T161">through</ta>
            <ta e="T163" id="Seg_3299" s="T162">gold</ta>
            <ta e="T164" id="Seg_3300" s="T163">ship.[NOM]</ta>
            <ta e="T165" id="Seg_3301" s="T164">go-PRS.[3SG]</ta>
            <ta e="T166" id="Seg_3302" s="T165">elegant.[NOM]</ta>
            <ta e="T167" id="Seg_3303" s="T166">colourful.[NOM]</ta>
            <ta e="T168" id="Seg_3304" s="T167">czar.[NOM]</ta>
            <ta e="T169" id="Seg_3305" s="T168">be.happy-PRS.[3SG]</ta>
            <ta e="T170" id="Seg_3306" s="T169">wind.[NOM]</ta>
            <ta e="T171" id="Seg_3307" s="T170">fall-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T172" id="Seg_3308" s="T171">raft.[NOM]</ta>
            <ta e="T173" id="Seg_3309" s="T172">scatter-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T174" id="Seg_3310" s="T173">stay-PRS.[3SG]</ta>
            <ta e="T175" id="Seg_3311" s="T174">fox.[NOM]</ta>
            <ta e="T176" id="Seg_3312" s="T175">deceiver.[NOM]</ta>
            <ta e="T177" id="Seg_3313" s="T176">Thunder</ta>
            <ta e="T178" id="Seg_3314" s="T177">czar-DAT/LOC</ta>
            <ta e="T179" id="Seg_3315" s="T178">run-CVB.SEQ</ta>
            <ta e="T180" id="Seg_3316" s="T179">reach-PRS.[3SG]</ta>
            <ta e="T181" id="Seg_3317" s="T180">say-PRS.[3SG]</ta>
            <ta e="T182" id="Seg_3318" s="T181">calamity.[NOM]</ta>
            <ta e="T183" id="Seg_3319" s="T182">be-PST1-3SG</ta>
            <ta e="T184" id="Seg_3320" s="T183">gold</ta>
            <ta e="T185" id="Seg_3321" s="T184">ship.[NOM]</ta>
            <ta e="T186" id="Seg_3322" s="T185">sink-PST1-3SG</ta>
            <ta e="T187" id="Seg_3323" s="T186">son_in_law-EP-2SG.[NOM]</ta>
            <ta e="T188" id="Seg_3324" s="T187">human.being.[NOM]</ta>
            <ta e="T189" id="Seg_3325" s="T188">keen.[NOM]</ta>
            <ta e="T190" id="Seg_3326" s="T189">be-CVB.SEQ</ta>
            <ta e="T191" id="Seg_3327" s="T190">survive-PST1-3SG</ta>
            <ta e="T192" id="Seg_3328" s="T191">luggage-3SG.[NOM]</ta>
            <ta e="T193" id="Seg_3329" s="T192">every-3SG.[NOM]</ta>
            <ta e="T194" id="Seg_3330" s="T193">water-DAT/LOC</ta>
            <ta e="T195" id="Seg_3331" s="T194">go-PST1-3SG</ta>
            <ta e="T196" id="Seg_3332" s="T195">self-3SG.[NOM]</ta>
            <ta e="T197" id="Seg_3333" s="T196">naked.[NOM]</ta>
            <ta e="T198" id="Seg_3334" s="T197">stay-PST1-3SG</ta>
            <ta e="T199" id="Seg_3335" s="T198">clothes-PART</ta>
            <ta e="T200" id="Seg_3336" s="T199">send.[IMP.2SG]</ta>
            <ta e="T201" id="Seg_3337" s="T200">Thunder</ta>
            <ta e="T202" id="Seg_3338" s="T201">czar.[NOM]</ta>
            <ta e="T203" id="Seg_3339" s="T202">boy-DAT/LOC</ta>
            <ta e="T204" id="Seg_3340" s="T203">best</ta>
            <ta e="T205" id="Seg_3341" s="T204">clothes-ACC</ta>
            <ta e="T206" id="Seg_3342" s="T205">self-3SG-GEN</ta>
            <ta e="T207" id="Seg_3343" s="T206">ship-3SG-ACC</ta>
            <ta e="T208" id="Seg_3344" s="T207">send-PRS.[3SG]</ta>
            <ta e="T209" id="Seg_3345" s="T208">fox.[NOM]</ta>
            <ta e="T210" id="Seg_3346" s="T209">deceiver.[NOM]</ta>
            <ta e="T211" id="Seg_3347" s="T210">boy-3SG-ACC</ta>
            <ta e="T212" id="Seg_3348" s="T211">advise-PRS.[3SG]</ta>
            <ta e="T213" id="Seg_3349" s="T212">czar-DAT/LOC</ta>
            <ta e="T214" id="Seg_3350" s="T213">reach-CVB.SEQ-2SG</ta>
            <ta e="T215" id="Seg_3351" s="T214">clothes-2SG-ACC</ta>
            <ta e="T216" id="Seg_3352" s="T215">see-EP-MED-EP-NEG.[IMP.2SG]</ta>
            <ta e="T217" id="Seg_3353" s="T216">be.surprised-FUT-3PL</ta>
            <ta e="T218" id="Seg_3354" s="T217">curtsey.[NOM]</ta>
            <ta e="T219" id="Seg_3355" s="T218">just</ta>
            <ta e="T220" id="Seg_3356" s="T219">make-CVB.SEQ</ta>
            <ta e="T221" id="Seg_3357" s="T220">greet.[IMP.2SG]</ta>
            <ta e="T222" id="Seg_3358" s="T221">eat-PTCP.PRS-2SG-DAT/LOC</ta>
            <ta e="T223" id="Seg_3359" s="T222">people.[NOM]</ta>
            <ta e="T224" id="Seg_3360" s="T223">eat-PTCP.PRS-3SG-ACC</ta>
            <ta e="T225" id="Seg_3361" s="T224">see-CVB.SEQ</ta>
            <ta e="T226" id="Seg_3362" s="T225">eat.[IMP.2SG]</ta>
            <ta e="T227" id="Seg_3363" s="T226">wine-ACC</ta>
            <ta e="T228" id="Seg_3364" s="T227">very</ta>
            <ta e="T229" id="Seg_3365" s="T228">few</ta>
            <ta e="T230" id="Seg_3366" s="T229">drink.[IMP.2SG]</ta>
            <ta e="T231" id="Seg_3367" s="T230">boy.[NOM]</ta>
            <ta e="T232" id="Seg_3368" s="T231">Thunder</ta>
            <ta e="T233" id="Seg_3369" s="T232">czar-DAT/LOC</ta>
            <ta e="T234" id="Seg_3370" s="T233">reach-PRS.[3SG]</ta>
            <ta e="T235" id="Seg_3371" s="T234">ship-3SG-ACC</ta>
            <ta e="T236" id="Seg_3372" s="T235">mount-CVB.SEQ</ta>
            <ta e="T237" id="Seg_3373" s="T236">clothes-3SG-ACC</ta>
            <ta e="T238" id="Seg_3374" s="T237">dress-CVB.SEQ</ta>
            <ta e="T239" id="Seg_3375" s="T238">wedding.[NOM]</ta>
            <ta e="T240" id="Seg_3376" s="T239">excessive.[NOM]</ta>
            <ta e="T241" id="Seg_3377" s="T240">be-PRS.[3SG]</ta>
            <ta e="T242" id="Seg_3378" s="T241">ship-3SG-INSTR</ta>
            <ta e="T243" id="Seg_3379" s="T242">come.back-PRS-3PL</ta>
            <ta e="T244" id="Seg_3380" s="T243">fox.[NOM]</ta>
            <ta e="T245" id="Seg_3381" s="T244">deceiver.[NOM]</ta>
            <ta e="T246" id="Seg_3382" s="T245">speak-PRS.[3SG]</ta>
            <ta e="T247" id="Seg_3383" s="T246">earth.[NOM]</ta>
            <ta e="T248" id="Seg_3384" s="T247">pit_dwelling-2SG-DAT/LOC</ta>
            <ta e="T249" id="Seg_3385" s="T248">stop-NEG.CVB.SIM</ta>
            <ta e="T250" id="Seg_3386" s="T249">pass.by-FUT.[IMP.2SG]</ta>
            <ta e="T251" id="Seg_3387" s="T250">foreign</ta>
            <ta e="T252" id="Seg_3388" s="T251">czar.[NOM]</ta>
            <ta e="T253" id="Seg_3389" s="T252">gold</ta>
            <ta e="T254" id="Seg_3390" s="T253">city-3SG-DAT/LOC</ta>
            <ta e="T255" id="Seg_3391" s="T254">come.closer-CVB.SEQ</ta>
            <ta e="T256" id="Seg_3392" s="T255">after</ta>
            <ta e="T257" id="Seg_3393" s="T256">stop-FUT.[IMP.2SG]</ta>
            <ta e="T258" id="Seg_3394" s="T257">and</ta>
            <ta e="T259" id="Seg_3395" s="T258">1SG-ACC</ta>
            <ta e="T260" id="Seg_3396" s="T259">call-CAUS-FUT.[IMP.2SG]</ta>
            <ta e="T261" id="Seg_3397" s="T260">people-EP-1SG.[NOM]-people-EP-1SG.[NOM]</ta>
            <ta e="T262" id="Seg_3398" s="T261">how.much</ta>
            <ta e="T263" id="Seg_3399" s="T262">very</ta>
            <ta e="T264" id="Seg_3400" s="T263">live-PTCP.PRS-3SG-ACC</ta>
            <ta e="T265" id="Seg_3401" s="T264">know-CVB.SEQ</ta>
            <ta e="T266" id="Seg_3402" s="T265">come.[IMP.2SG]</ta>
            <ta e="T267" id="Seg_3403" s="T266">say-FUT.[IMP.2SG]</ta>
            <ta e="T268" id="Seg_3404" s="T267">1SG-DAT/LOC</ta>
            <ta e="T269" id="Seg_3405" s="T268">fox.[NOM]</ta>
            <ta e="T270" id="Seg_3406" s="T269">speak-PTCP.PST-3SG-ACC</ta>
            <ta e="T271" id="Seg_3407" s="T270">like</ta>
            <ta e="T272" id="Seg_3408" s="T271">boy.[NOM]</ta>
            <ta e="T273" id="Seg_3409" s="T272">self-3SG-GEN</ta>
            <ta e="T274" id="Seg_3410" s="T273">pit_dwelling-3SG-ACC</ta>
            <ta e="T275" id="Seg_3411" s="T274">pass.by-PRS.[3SG]</ta>
            <ta e="T276" id="Seg_3412" s="T275">foreign</ta>
            <ta e="T277" id="Seg_3413" s="T276">czar.[NOM]</ta>
            <ta e="T278" id="Seg_3414" s="T277">gold</ta>
            <ta e="T279" id="Seg_3415" s="T278">city-3SG-DAT/LOC</ta>
            <ta e="T280" id="Seg_3416" s="T279">come.closer-PRS.[3SG]</ta>
            <ta e="T281" id="Seg_3417" s="T280">stop-CVB.ANT</ta>
            <ta e="T282" id="Seg_3418" s="T281">stand-CVB.SEQ</ta>
            <ta e="T283" id="Seg_3419" s="T282">say-PRS.[3SG]</ta>
            <ta e="T284" id="Seg_3420" s="T283">people-EP-1SG.[NOM]-people-EP-1SG.[NOM]</ta>
            <ta e="T285" id="Seg_3421" s="T284">how.much</ta>
            <ta e="T286" id="Seg_3422" s="T285">very</ta>
            <ta e="T287" id="Seg_3423" s="T286">live-PTCP.PRS-3SG-ACC</ta>
            <ta e="T288" id="Seg_3424" s="T287">know-FUT-1SG</ta>
            <ta e="T289" id="Seg_3425" s="T288">be-PST1-3SG</ta>
            <ta e="T290" id="Seg_3426" s="T289">exactly.this</ta>
            <ta e="T291" id="Seg_3427" s="T290">fox-EP-1SG.[NOM]</ta>
            <ta e="T292" id="Seg_3428" s="T291">whereto</ta>
            <ta e="T293" id="Seg_3429" s="T292">go-PST1-3SG</ta>
            <ta e="T294" id="Seg_3430" s="T293">get.to.know-CVB.SEQ</ta>
            <ta e="T295" id="Seg_3431" s="T294">come-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T296" id="Seg_3432" s="T295">say-PRS.[3SG]</ta>
            <ta e="T297" id="Seg_3433" s="T296">fox.[NOM]</ta>
            <ta e="T298" id="Seg_3434" s="T297">deceiver.[NOM]</ta>
            <ta e="T299" id="Seg_3435" s="T298">come-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T300" id="Seg_3436" s="T299">speak-PRS.[3SG]</ta>
            <ta e="T301" id="Seg_3437" s="T300">fox.[NOM]</ta>
            <ta e="T302" id="Seg_3438" s="T301">deceiver.[NOM]</ta>
            <ta e="T303" id="Seg_3439" s="T302">go-CVB.SEQ</ta>
            <ta e="T304" id="Seg_3440" s="T303">gold</ta>
            <ta e="T305" id="Seg_3441" s="T304">city.[NOM]</ta>
            <ta e="T306" id="Seg_3442" s="T305">czar-3SG-DAT/LOC</ta>
            <ta e="T307" id="Seg_3443" s="T306">speak-PRS.[3SG]</ta>
            <ta e="T308" id="Seg_3444" s="T307">Thunder</ta>
            <ta e="T309" id="Seg_3445" s="T308">czar.[NOM]</ta>
            <ta e="T310" id="Seg_3446" s="T309">2PL-ACC</ta>
            <ta e="T311" id="Seg_3447" s="T310">defeat-CVB.SIM</ta>
            <ta e="T312" id="Seg_3448" s="T311">come-PST1-3SG</ta>
            <ta e="T313" id="Seg_3449" s="T312">flee-CVB.SIM</ta>
            <ta e="T314" id="Seg_3450" s="T313">beat-EP-IMP.2PL</ta>
            <ta e="T315" id="Seg_3451" s="T314">that</ta>
            <ta e="T316" id="Seg_3452" s="T315">forest-DAT/LOC</ta>
            <ta e="T317" id="Seg_3453" s="T316">go.out-CVB.SEQ</ta>
            <ta e="T318" id="Seg_3454" s="T317">hide-EP-IMP.2PL</ta>
            <ta e="T319" id="Seg_3455" s="T318">head-2PL.[NOM]</ta>
            <ta e="T320" id="Seg_3456" s="T319">just</ta>
            <ta e="T321" id="Seg_3457" s="T320">to.be.on.view-PTCP.PRS.[NOM]</ta>
            <ta e="T322" id="Seg_3458" s="T321">like</ta>
            <ta e="T323" id="Seg_3459" s="T322">gold</ta>
            <ta e="T324" id="Seg_3460" s="T323">city.[NOM]</ta>
            <ta e="T325" id="Seg_3461" s="T324">czar-3SG-GEN</ta>
            <ta e="T326" id="Seg_3462" s="T325">people-3SG.[NOM]</ta>
            <ta e="T327" id="Seg_3463" s="T326">whole</ta>
            <ta e="T328" id="Seg_3464" s="T327">forest-DAT/LOC</ta>
            <ta e="T329" id="Seg_3465" s="T328">flee-PRS.[3SG]</ta>
            <ta e="T330" id="Seg_3466" s="T329">Thunder</ta>
            <ta e="T331" id="Seg_3467" s="T330">czar-ABL</ta>
            <ta e="T332" id="Seg_3468" s="T331">be.afraid-CVB.SEQ</ta>
            <ta e="T333" id="Seg_3469" s="T332">fox.[NOM]</ta>
            <ta e="T334" id="Seg_3470" s="T333">deceiver.[NOM]</ta>
            <ta e="T335" id="Seg_3471" s="T334">boy-DAT/LOC</ta>
            <ta e="T336" id="Seg_3472" s="T335">cry-CVB.SEQ</ta>
            <ta e="T337" id="Seg_3473" s="T336">come-PRS.[3SG]</ta>
            <ta e="T338" id="Seg_3474" s="T337">people-1PL-ACC</ta>
            <ta e="T339" id="Seg_3475" s="T338">evil.spirit.[NOM]</ta>
            <ta e="T340" id="Seg_3476" s="T339">czar-3SG.[NOM]</ta>
            <ta e="T341" id="Seg_3477" s="T340">defeat-PST2.[3SG]</ta>
            <ta e="T342" id="Seg_3478" s="T341">eat-CVB.SEQ-eat-CVB.SEQ</ta>
            <ta e="T343" id="Seg_3479" s="T342">after</ta>
            <ta e="T344" id="Seg_3480" s="T343">that</ta>
            <ta e="T345" id="Seg_3481" s="T344">forest-DAT/LOC</ta>
            <ta e="T346" id="Seg_3482" s="T345">hide-CVB.SIM</ta>
            <ta e="T347" id="Seg_3483" s="T346">lie-PRS-3PL</ta>
            <ta e="T348" id="Seg_3484" s="T347">father_in_law-2SG-ACC</ta>
            <ta e="T349" id="Seg_3485" s="T348">beg-EP-MED.[IMP.2SG]</ta>
            <ta e="T350" id="Seg_3486" s="T349">lightning-VBZ-IMP.3SG</ta>
            <ta e="T351" id="Seg_3487" s="T350">boy.[NOM]</ta>
            <ta e="T352" id="Seg_3488" s="T351">father_in_law-EP-3SG-ABL</ta>
            <ta e="T353" id="Seg_3489" s="T352">beg-EP-MED-PRS.[3SG]</ta>
            <ta e="T354" id="Seg_3490" s="T353">lightning-VBZ.[IMP.2SG]</ta>
            <ta e="T355" id="Seg_3491" s="T354">say-CVB.SEQ</ta>
            <ta e="T356" id="Seg_3492" s="T355">Thunder</ta>
            <ta e="T357" id="Seg_3493" s="T356">czar.[NOM]</ta>
            <ta e="T358" id="Seg_3494" s="T357">well</ta>
            <ta e="T359" id="Seg_3495" s="T358">thunder-VBZ-CVB.SEQ</ta>
            <ta e="T360" id="Seg_3496" s="T359">beat-PRS.[3SG]</ta>
            <ta e="T361" id="Seg_3497" s="T360">exactly.this</ta>
            <ta e="T362" id="Seg_3498" s="T361">forest-ACC</ta>
            <ta e="T363" id="Seg_3499" s="T362">gold</ta>
            <ta e="T364" id="Seg_3500" s="T363">city.[NOM]</ta>
            <ta e="T365" id="Seg_3501" s="T364">one</ta>
            <ta e="T366" id="Seg_3502" s="T365">NEG</ta>
            <ta e="T367" id="Seg_3503" s="T366">human.being-3SG.[NOM]</ta>
            <ta e="T368" id="Seg_3504" s="T367">survive-NEG.[3SG]</ta>
            <ta e="T369" id="Seg_3505" s="T368">boy.[NOM]</ta>
            <ta e="T370" id="Seg_3506" s="T369">exactly.this</ta>
            <ta e="T371" id="Seg_3507" s="T370">gold</ta>
            <ta e="T372" id="Seg_3508" s="T371">city-DAT/LOC</ta>
            <ta e="T373" id="Seg_3509" s="T372">go.in-CVB.SEQ</ta>
            <ta e="T374" id="Seg_3510" s="T373">life-INCH-PRS.[3SG]</ta>
            <ta e="T375" id="Seg_3511" s="T374">czar.[NOM]</ta>
            <ta e="T376" id="Seg_3512" s="T375">be-PRS.[3SG]</ta>
            <ta e="T377" id="Seg_3513" s="T376">Thunder</ta>
            <ta e="T378" id="Seg_3514" s="T377">czar.[NOM]</ta>
            <ta e="T379" id="Seg_3515" s="T378">girl-3SG-ACC</ta>
            <ta e="T380" id="Seg_3516" s="T379">take-INFER-3SG</ta>
            <ta e="T381" id="Seg_3517" s="T380">that.[NOM]</ta>
            <ta e="T382" id="Seg_3518" s="T381">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gd">
            <ta e="T1" id="Seg_3519" s="T0">arm</ta>
            <ta e="T2" id="Seg_3520" s="T1">Bauer.[NOM]</ta>
            <ta e="T3" id="Seg_3521" s="T2">leben-PST2.[3SG]</ta>
            <ta e="T4" id="Seg_3522" s="T3">dieses</ta>
            <ta e="T5" id="Seg_3523" s="T4">Bauer.[NOM]</ta>
            <ta e="T6" id="Seg_3524" s="T5">sterben-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T7" id="Seg_3525" s="T6">Boden.[NOM]</ta>
            <ta e="T8" id="Seg_3526" s="T7">Erdhütte-3SG-DAT/LOC</ta>
            <ta e="T9" id="Seg_3527" s="T8">Junge.[NOM]</ta>
            <ta e="T10" id="Seg_3528" s="T9">Kind-3SG.[NOM]</ta>
            <ta e="T11" id="Seg_3529" s="T10">Waise.[NOM]</ta>
            <ta e="T12" id="Seg_3530" s="T11">bleiben-PRS.[3SG]</ta>
            <ta e="T13" id="Seg_3531" s="T12">dieses</ta>
            <ta e="T14" id="Seg_3532" s="T13">Kind.[NOM]</ta>
            <ta e="T15" id="Seg_3533" s="T14">Hunger.haben-CVB.SEQ</ta>
            <ta e="T16" id="Seg_3534" s="T15">töten-CVB.SIM</ta>
            <ta e="T17" id="Seg_3535" s="T16">sein-PRS.[3SG]</ta>
            <ta e="T18" id="Seg_3536" s="T17">dieses-ACC</ta>
            <ta e="T19" id="Seg_3537" s="T18">Fuchs.[NOM]</ta>
            <ta e="T20" id="Seg_3538" s="T19">finden-PRS.[3SG]</ta>
            <ta e="T21" id="Seg_3539" s="T20">1SG.[NOM]</ta>
            <ta e="T22" id="Seg_3540" s="T21">2SG-ACC</ta>
            <ta e="T23" id="Seg_3541" s="T22">Mensch.[NOM]</ta>
            <ta e="T24" id="Seg_3542" s="T23">machen-FUT-1SG</ta>
            <ta e="T25" id="Seg_3543" s="T24">sagen-PRS.[3SG]</ta>
            <ta e="T26" id="Seg_3544" s="T25">reich</ta>
            <ta e="T27" id="Seg_3545" s="T26">Mensch.[NOM]</ta>
            <ta e="T28" id="Seg_3546" s="T27">sein-FUT-2SG</ta>
            <ta e="T29" id="Seg_3547" s="T28">reich</ta>
            <ta e="T30" id="Seg_3548" s="T29">Frau-ACC</ta>
            <ta e="T31" id="Seg_3549" s="T30">Frau.[NOM]</ta>
            <ta e="T32" id="Seg_3550" s="T31">nehmen-CVB.SEQ</ta>
            <ta e="T33" id="Seg_3551" s="T32">geben-FUT-1SG</ta>
            <ta e="T34" id="Seg_3552" s="T33">so</ta>
            <ta e="T35" id="Seg_3553" s="T34">sagen-CVB.SEQ</ta>
            <ta e="T36" id="Seg_3554" s="T35">nachdem</ta>
            <ta e="T37" id="Seg_3555" s="T36">Fuchs.[NOM]</ta>
            <ta e="T38" id="Seg_3556" s="T37">Donner</ta>
            <ta e="T39" id="Seg_3557" s="T38">Zar-3SG-DAT/LOC</ta>
            <ta e="T40" id="Seg_3558" s="T39">gehen-PRS.[3SG]</ta>
            <ta e="T41" id="Seg_3559" s="T40">Donner</ta>
            <ta e="T42" id="Seg_3560" s="T41">Zar-3SG-DAT/LOC</ta>
            <ta e="T43" id="Seg_3561" s="T42">ankommen-CVB.SEQ</ta>
            <ta e="T44" id="Seg_3562" s="T43">sagen-PRS.[3SG]</ta>
            <ta e="T45" id="Seg_3563" s="T44">Rang-PROPR</ta>
            <ta e="T46" id="Seg_3564" s="T45">reich</ta>
            <ta e="T47" id="Seg_3565" s="T46">Junge.[NOM]</ta>
            <ta e="T48" id="Seg_3566" s="T47">Mensch.[NOM]</ta>
            <ta e="T49" id="Seg_3567" s="T48">es.gibt</ta>
            <ta e="T50" id="Seg_3568" s="T49">3SG.[NOM]</ta>
            <ta e="T51" id="Seg_3569" s="T50">2SG.[NOM]</ta>
            <ta e="T52" id="Seg_3570" s="T51">Tochter-2SG-ACC</ta>
            <ta e="T53" id="Seg_3571" s="T52">Brautwerbung-VBZ-EP-CAUS-CVB.SIM</ta>
            <ta e="T54" id="Seg_3572" s="T53">schicken-PST1-3SG</ta>
            <ta e="T55" id="Seg_3573" s="T54">Tochter-EP-2SG.[NOM]</ta>
            <ta e="T56" id="Seg_3574" s="T55">Brautpreis-3SG-ACC</ta>
            <ta e="T57" id="Seg_3575" s="T56">Wald.[NOM]</ta>
            <ta e="T58" id="Seg_3576" s="T57">Tier-3SG-ACC</ta>
            <ta e="T59" id="Seg_3577" s="T58">bester-3SG-ACC</ta>
            <ta e="T60" id="Seg_3578" s="T59">ausgewählt-3SG-ACC</ta>
            <ta e="T61" id="Seg_3579" s="T60">schicken-PTCP.FUT</ta>
            <ta e="T62" id="Seg_3580" s="T61">sein-PST1-3SG</ta>
            <ta e="T63" id="Seg_3581" s="T62">jenes</ta>
            <ta e="T64" id="Seg_3582" s="T63">Tier-PL-ACC</ta>
            <ta e="T65" id="Seg_3583" s="T64">Seite.[NOM]</ta>
            <ta e="T66" id="Seg_3584" s="T65">Seite-3SG-INSTR</ta>
            <ta e="T67" id="Seg_3585" s="T66">Haus-DAT/LOC</ta>
            <ta e="T68" id="Seg_3586" s="T67">Nahrung.[NOM]</ta>
            <ta e="T69" id="Seg_3587" s="T68">fertig-VBZ-CVB.SEQ</ta>
            <ta e="T70" id="Seg_3588" s="T69">empfangen-FUT-2SG</ta>
            <ta e="T71" id="Seg_3589" s="T70">man.sagt</ta>
            <ta e="T72" id="Seg_3590" s="T71">sagen-PRS.[3SG]</ta>
            <ta e="T73" id="Seg_3591" s="T72">Donner</ta>
            <ta e="T74" id="Seg_3592" s="T73">Zar.[NOM]</ta>
            <ta e="T75" id="Seg_3593" s="T74">einverstanden.sein-EP-MED-PST2.[3SG]</ta>
            <ta e="T76" id="Seg_3594" s="T75">Fuchs.[NOM]</ta>
            <ta e="T77" id="Seg_3595" s="T76">Betrug.[NOM]</ta>
            <ta e="T78" id="Seg_3596" s="T77">zurück</ta>
            <ta e="T79" id="Seg_3597" s="T78">laufen-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_3598" s="T79">Wald.[NOM]</ta>
            <ta e="T81" id="Seg_3599" s="T80">Tier-PL-3SG-ACC</ta>
            <ta e="T82" id="Seg_3600" s="T81">unterschiedlich-3SG-ACC</ta>
            <ta e="T83" id="Seg_3601" s="T82">treffen-CVB.SEQ</ta>
            <ta e="T84" id="Seg_3602" s="T83">sprechen-PRS.[3SG]</ta>
            <ta e="T85" id="Seg_3603" s="T84">Donner</ta>
            <ta e="T86" id="Seg_3604" s="T85">Zar.[NOM]</ta>
            <ta e="T87" id="Seg_3605" s="T86">Reichtum-3SG.[NOM]-Sattheit-3SG.[NOM]</ta>
            <ta e="T88" id="Seg_3606" s="T87">verweigern-EP-NEG.CVB</ta>
            <ta e="T89" id="Seg_3607" s="T88">2PL-DAT/LOC</ta>
            <ta e="T90" id="Seg_3608" s="T89">Nahrung.[NOM]</ta>
            <ta e="T91" id="Seg_3609" s="T90">vorbereiten-PST1-3SG</ta>
            <ta e="T92" id="Seg_3610" s="T91">jenes-ACC</ta>
            <ta e="T93" id="Seg_3611" s="T92">nachdem</ta>
            <ta e="T94" id="Seg_3612" s="T93">essen-PTCP.PRS</ta>
            <ta e="T95" id="Seg_3613" s="T94">man.sagt-2PL</ta>
            <ta e="T96" id="Seg_3614" s="T95">Tier.[NOM]</ta>
            <ta e="T97" id="Seg_3615" s="T96">sehr.viel</ta>
            <ta e="T98" id="Seg_3616" s="T97">Herde.[NOM]</ta>
            <ta e="T99" id="Seg_3617" s="T98">sein-CVB.SEQ</ta>
            <ta e="T100" id="Seg_3618" s="T99">ankommen-PRS-3PL</ta>
            <ta e="T101" id="Seg_3619" s="T100">essen-CVB.SIM</ta>
            <ta e="T102" id="Seg_3620" s="T101">Donner</ta>
            <ta e="T103" id="Seg_3621" s="T102">Zar.[NOM]</ta>
            <ta e="T104" id="Seg_3622" s="T103">sich.freuen-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_3623" s="T104">Schwiegersohn-EP-1SG.[NOM]</ta>
            <ta e="T106" id="Seg_3624" s="T105">reich</ta>
            <ta e="T107" id="Seg_3625" s="T106">Mensch.[NOM]</ta>
            <ta e="T108" id="Seg_3626" s="T107">sein-PST2.[3SG]</ta>
            <ta e="T109" id="Seg_3627" s="T108">denken-PRS.[3SG]</ta>
            <ta e="T110" id="Seg_3628" s="T109">Tier-PL.[NOM]</ta>
            <ta e="T111" id="Seg_3629" s="T110">essen-CVB.SIM</ta>
            <ta e="T112" id="Seg_3630" s="T111">anfangen-TEMP-3PL</ta>
            <ta e="T113" id="Seg_3631" s="T112">eben.der</ta>
            <ta e="T114" id="Seg_3632" s="T113">Haus-PL-ACC</ta>
            <ta e="T115" id="Seg_3633" s="T114">verriegeln-CVB.SEQ</ta>
            <ta e="T116" id="Seg_3634" s="T115">werfen-PRS-3PL</ta>
            <ta e="T117" id="Seg_3635" s="T116">Fuchs.[NOM]</ta>
            <ta e="T118" id="Seg_3636" s="T117">Betrüger.[NOM]</ta>
            <ta e="T119" id="Seg_3637" s="T118">Donner</ta>
            <ta e="T120" id="Seg_3638" s="T119">Zar-DAT/LOC</ta>
            <ta e="T121" id="Seg_3639" s="T120">ankommen-PRS.[3SG]</ta>
            <ta e="T122" id="Seg_3640" s="T121">na</ta>
            <ta e="T123" id="Seg_3641" s="T122">Tier-PL.[NOM]</ta>
            <ta e="T124" id="Seg_3642" s="T123">kommen-PST1-3PL</ta>
            <ta e="T125" id="Seg_3643" s="T124">Q</ta>
            <ta e="T126" id="Seg_3644" s="T125">kommen-PST1-3PL</ta>
            <ta e="T127" id="Seg_3645" s="T126">Schwiegersohn.[NOM]</ta>
            <ta e="T128" id="Seg_3646" s="T127">Junge.[NOM]</ta>
            <ta e="T129" id="Seg_3647" s="T128">Freizeit-POSS</ta>
            <ta e="T130" id="Seg_3648" s="T129">NEG.[3SG]</ta>
            <ta e="T131" id="Seg_3649" s="T130">bald</ta>
            <ta e="T132" id="Seg_3650" s="T131">golden</ta>
            <ta e="T133" id="Seg_3651" s="T132">Schiff-EP-INSTR</ta>
            <ta e="T134" id="Seg_3652" s="T133">kommen-FUT.[3SG]</ta>
            <ta e="T135" id="Seg_3653" s="T134">sagen-PRS.[3SG]</ta>
            <ta e="T136" id="Seg_3654" s="T135">Junge-3SG-DAT/LOC</ta>
            <ta e="T137" id="Seg_3655" s="T136">zurück</ta>
            <ta e="T138" id="Seg_3656" s="T137">laufen-CVB.SEQ</ta>
            <ta e="T139" id="Seg_3657" s="T138">ankommen-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_3658" s="T139">Weidenstrauch-EP-INSTR</ta>
            <ta e="T141" id="Seg_3659" s="T140">schlecht</ta>
            <ta e="T142" id="Seg_3660" s="T141">Floß.[NOM]</ta>
            <ta e="T143" id="Seg_3661" s="T142">machen-CAUS-PRS.[3SG]</ta>
            <ta e="T144" id="Seg_3662" s="T143">unterschiedlich</ta>
            <ta e="T145" id="Seg_3663" s="T144">Erde.[NOM]</ta>
            <ta e="T146" id="Seg_3664" s="T145">Blume-3SG-INSTR</ta>
            <ta e="T147" id="Seg_3665" s="T146">verzieren-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T148" id="Seg_3666" s="T147">fern-ABL</ta>
            <ta e="T149" id="Seg_3667" s="T148">elegant.[NOM]</ta>
            <ta e="T150" id="Seg_3668" s="T149">schön.[NOM]</ta>
            <ta e="T151" id="Seg_3669" s="T150">sein-CVB.SEQ</ta>
            <ta e="T152" id="Seg_3670" s="T151">zu.sehen.sein-PRS.[3SG]</ta>
            <ta e="T153" id="Seg_3671" s="T152">Wind.[NOM]</ta>
            <ta e="T154" id="Seg_3672" s="T153">Tag.[NOM]</ta>
            <ta e="T155" id="Seg_3673" s="T154">Donner</ta>
            <ta e="T156" id="Seg_3674" s="T155">Zar-DAT/LOC</ta>
            <ta e="T157" id="Seg_3675" s="T156">fließen-PRS-3PL</ta>
            <ta e="T158" id="Seg_3676" s="T157">Donner</ta>
            <ta e="T159" id="Seg_3677" s="T158">Zar.[NOM]</ta>
            <ta e="T160" id="Seg_3678" s="T159">sehen-PST2-3SG</ta>
            <ta e="T161" id="Seg_3679" s="T160">Meer.[NOM]</ta>
            <ta e="T162" id="Seg_3680" s="T161">durch</ta>
            <ta e="T163" id="Seg_3681" s="T162">golden</ta>
            <ta e="T164" id="Seg_3682" s="T163">Schiff.[NOM]</ta>
            <ta e="T165" id="Seg_3683" s="T164">gehen-PRS.[3SG]</ta>
            <ta e="T166" id="Seg_3684" s="T165">elegant.[NOM]</ta>
            <ta e="T167" id="Seg_3685" s="T166">farbig.[NOM]</ta>
            <ta e="T168" id="Seg_3686" s="T167">Zar.[NOM]</ta>
            <ta e="T169" id="Seg_3687" s="T168">sich.freuen-PRS.[3SG]</ta>
            <ta e="T170" id="Seg_3688" s="T169">Wind.[NOM]</ta>
            <ta e="T171" id="Seg_3689" s="T170">fallen-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T172" id="Seg_3690" s="T171">Floß.[NOM]</ta>
            <ta e="T173" id="Seg_3691" s="T172">verstreuen-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T174" id="Seg_3692" s="T173">bleiben-PRS.[3SG]</ta>
            <ta e="T175" id="Seg_3693" s="T174">Fuchs.[NOM]</ta>
            <ta e="T176" id="Seg_3694" s="T175">Betrug.[NOM]</ta>
            <ta e="T177" id="Seg_3695" s="T176">Donner</ta>
            <ta e="T178" id="Seg_3696" s="T177">Zar-DAT/LOC</ta>
            <ta e="T179" id="Seg_3697" s="T178">laufen-CVB.SEQ</ta>
            <ta e="T180" id="Seg_3698" s="T179">ankommen-PRS.[3SG]</ta>
            <ta e="T181" id="Seg_3699" s="T180">sagen-PRS.[3SG]</ta>
            <ta e="T182" id="Seg_3700" s="T181">Unglück.[NOM]</ta>
            <ta e="T183" id="Seg_3701" s="T182">sein-PST1-3SG</ta>
            <ta e="T184" id="Seg_3702" s="T183">golden</ta>
            <ta e="T185" id="Seg_3703" s="T184">Schiff.[NOM]</ta>
            <ta e="T186" id="Seg_3704" s="T185">sinken-PST1-3SG</ta>
            <ta e="T187" id="Seg_3705" s="T186">Schwiegersohn-EP-2SG.[NOM]</ta>
            <ta e="T188" id="Seg_3706" s="T187">Mensch.[NOM]</ta>
            <ta e="T189" id="Seg_3707" s="T188">kühn.[NOM]</ta>
            <ta e="T190" id="Seg_3708" s="T189">sein-CVB.SEQ</ta>
            <ta e="T191" id="Seg_3709" s="T190">überleben-PST1-3SG</ta>
            <ta e="T192" id="Seg_3710" s="T191">Gepäck-3SG.[NOM]</ta>
            <ta e="T193" id="Seg_3711" s="T192">jeder-3SG.[NOM]</ta>
            <ta e="T194" id="Seg_3712" s="T193">Wasser-DAT/LOC</ta>
            <ta e="T195" id="Seg_3713" s="T194">gehen-PST1-3SG</ta>
            <ta e="T196" id="Seg_3714" s="T195">selbst-3SG.[NOM]</ta>
            <ta e="T197" id="Seg_3715" s="T196">nackt.[NOM]</ta>
            <ta e="T198" id="Seg_3716" s="T197">bleiben-PST1-3SG</ta>
            <ta e="T199" id="Seg_3717" s="T198">Kleidung-PART</ta>
            <ta e="T200" id="Seg_3718" s="T199">schicken.[IMP.2SG]</ta>
            <ta e="T201" id="Seg_3719" s="T200">Donner</ta>
            <ta e="T202" id="Seg_3720" s="T201">Zar.[NOM]</ta>
            <ta e="T203" id="Seg_3721" s="T202">Junge-DAT/LOC</ta>
            <ta e="T204" id="Seg_3722" s="T203">bester</ta>
            <ta e="T205" id="Seg_3723" s="T204">Kleidung-ACC</ta>
            <ta e="T206" id="Seg_3724" s="T205">selbst-3SG-GEN</ta>
            <ta e="T207" id="Seg_3725" s="T206">Schiff-3SG-ACC</ta>
            <ta e="T208" id="Seg_3726" s="T207">schicken-PRS.[3SG]</ta>
            <ta e="T209" id="Seg_3727" s="T208">Fuchs.[NOM]</ta>
            <ta e="T210" id="Seg_3728" s="T209">Betrüger.[NOM]</ta>
            <ta e="T211" id="Seg_3729" s="T210">Junge-3SG-ACC</ta>
            <ta e="T212" id="Seg_3730" s="T211">raten-PRS.[3SG]</ta>
            <ta e="T213" id="Seg_3731" s="T212">Zar-DAT/LOC</ta>
            <ta e="T214" id="Seg_3732" s="T213">ankommen-CVB.SEQ-2SG</ta>
            <ta e="T215" id="Seg_3733" s="T214">Kleidung-2SG-ACC</ta>
            <ta e="T216" id="Seg_3734" s="T215">sehen-EP-MED-EP-NEG.[IMP.2SG]</ta>
            <ta e="T217" id="Seg_3735" s="T216">sich.wundern-FUT-3PL</ta>
            <ta e="T218" id="Seg_3736" s="T217">Knicks.[NOM]</ta>
            <ta e="T219" id="Seg_3737" s="T218">nur</ta>
            <ta e="T220" id="Seg_3738" s="T219">machen-CVB.SEQ</ta>
            <ta e="T221" id="Seg_3739" s="T220">begrüßen.[IMP.2SG]</ta>
            <ta e="T222" id="Seg_3740" s="T221">essen-PTCP.PRS-2SG-DAT/LOC</ta>
            <ta e="T223" id="Seg_3741" s="T222">Leute.[NOM]</ta>
            <ta e="T224" id="Seg_3742" s="T223">essen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T225" id="Seg_3743" s="T224">sehen-CVB.SEQ</ta>
            <ta e="T226" id="Seg_3744" s="T225">essen.[IMP.2SG]</ta>
            <ta e="T227" id="Seg_3745" s="T226">Wein-ACC</ta>
            <ta e="T228" id="Seg_3746" s="T227">sehr</ta>
            <ta e="T229" id="Seg_3747" s="T228">wenig</ta>
            <ta e="T230" id="Seg_3748" s="T229">trinken.[IMP.2SG]</ta>
            <ta e="T231" id="Seg_3749" s="T230">Junge.[NOM]</ta>
            <ta e="T232" id="Seg_3750" s="T231">Donner</ta>
            <ta e="T233" id="Seg_3751" s="T232">Zar-DAT/LOC</ta>
            <ta e="T234" id="Seg_3752" s="T233">ankommen-PRS.[3SG]</ta>
            <ta e="T235" id="Seg_3753" s="T234">Schiff-3SG-ACC</ta>
            <ta e="T236" id="Seg_3754" s="T235">besteigen-CVB.SEQ</ta>
            <ta e="T237" id="Seg_3755" s="T236">Kleidung-3SG-ACC</ta>
            <ta e="T238" id="Seg_3756" s="T237">sich.anziehen-CVB.SEQ</ta>
            <ta e="T239" id="Seg_3757" s="T238">Hochzeit.[NOM]</ta>
            <ta e="T240" id="Seg_3758" s="T239">ausschweifend.[NOM]</ta>
            <ta e="T241" id="Seg_3759" s="T240">sein-PRS.[3SG]</ta>
            <ta e="T242" id="Seg_3760" s="T241">Schiff-3SG-INSTR</ta>
            <ta e="T243" id="Seg_3761" s="T242">zurückkommen-PRS-3PL</ta>
            <ta e="T244" id="Seg_3762" s="T243">Fuchs.[NOM]</ta>
            <ta e="T245" id="Seg_3763" s="T244">Betrug.[NOM]</ta>
            <ta e="T246" id="Seg_3764" s="T245">sprechen-PRS.[3SG]</ta>
            <ta e="T247" id="Seg_3765" s="T246">Boden.[NOM]</ta>
            <ta e="T248" id="Seg_3766" s="T247">Erdhütte-2SG-DAT/LOC</ta>
            <ta e="T249" id="Seg_3767" s="T248">halten-NEG.CVB.SIM</ta>
            <ta e="T250" id="Seg_3768" s="T249">vorbeigehen-FUT.[IMP.2SG]</ta>
            <ta e="T251" id="Seg_3769" s="T250">fremd</ta>
            <ta e="T252" id="Seg_3770" s="T251">Zar.[NOM]</ta>
            <ta e="T253" id="Seg_3771" s="T252">golden</ta>
            <ta e="T254" id="Seg_3772" s="T253">Stadt-3SG-DAT/LOC</ta>
            <ta e="T255" id="Seg_3773" s="T254">sich.nähern-CVB.SEQ</ta>
            <ta e="T256" id="Seg_3774" s="T255">nachdem</ta>
            <ta e="T257" id="Seg_3775" s="T256">halten-FUT.[IMP.2SG]</ta>
            <ta e="T258" id="Seg_3776" s="T257">und</ta>
            <ta e="T259" id="Seg_3777" s="T258">1SG-ACC</ta>
            <ta e="T260" id="Seg_3778" s="T259">rufen-CAUS-FUT.[IMP.2SG]</ta>
            <ta e="T261" id="Seg_3779" s="T260">Volk-EP-1SG.[NOM]-Volk-EP-1SG.[NOM]</ta>
            <ta e="T262" id="Seg_3780" s="T261">wie.viel</ta>
            <ta e="T263" id="Seg_3781" s="T262">sehr</ta>
            <ta e="T264" id="Seg_3782" s="T263">leben-PTCP.PRS-3SG-ACC</ta>
            <ta e="T265" id="Seg_3783" s="T264">wissen-CVB.SEQ</ta>
            <ta e="T266" id="Seg_3784" s="T265">kommen.[IMP.2SG]</ta>
            <ta e="T267" id="Seg_3785" s="T266">sagen-FUT.[IMP.2SG]</ta>
            <ta e="T268" id="Seg_3786" s="T267">1SG-DAT/LOC</ta>
            <ta e="T269" id="Seg_3787" s="T268">Fuchs.[NOM]</ta>
            <ta e="T270" id="Seg_3788" s="T269">sprechen-PTCP.PST-3SG-ACC</ta>
            <ta e="T271" id="Seg_3789" s="T270">wie</ta>
            <ta e="T272" id="Seg_3790" s="T271">Junge.[NOM]</ta>
            <ta e="T273" id="Seg_3791" s="T272">selbst-3SG-GEN</ta>
            <ta e="T274" id="Seg_3792" s="T273">Erdhütte-3SG-ACC</ta>
            <ta e="T275" id="Seg_3793" s="T274">vorbeigehen-PRS.[3SG]</ta>
            <ta e="T276" id="Seg_3794" s="T275">fremd</ta>
            <ta e="T277" id="Seg_3795" s="T276">Zar.[NOM]</ta>
            <ta e="T278" id="Seg_3796" s="T277">golden</ta>
            <ta e="T279" id="Seg_3797" s="T278">Stadt-3SG-DAT/LOC</ta>
            <ta e="T280" id="Seg_3798" s="T279">sich.nähern-PRS.[3SG]</ta>
            <ta e="T281" id="Seg_3799" s="T280">halten-CVB.ANT</ta>
            <ta e="T282" id="Seg_3800" s="T281">stehen-CVB.SEQ</ta>
            <ta e="T283" id="Seg_3801" s="T282">sagen-PRS.[3SG]</ta>
            <ta e="T284" id="Seg_3802" s="T283">Volk-EP-1SG.[NOM]-Volk-EP-1SG.[NOM]</ta>
            <ta e="T285" id="Seg_3803" s="T284">wie.viel</ta>
            <ta e="T286" id="Seg_3804" s="T285">sehr</ta>
            <ta e="T287" id="Seg_3805" s="T286">leben-PTCP.PRS-3SG-ACC</ta>
            <ta e="T288" id="Seg_3806" s="T287">wissen-FUT-1SG</ta>
            <ta e="T289" id="Seg_3807" s="T288">sein-PST1-3SG</ta>
            <ta e="T290" id="Seg_3808" s="T289">eben.der</ta>
            <ta e="T291" id="Seg_3809" s="T290">Fuchs-EP-1SG.[NOM]</ta>
            <ta e="T292" id="Seg_3810" s="T291">wohin</ta>
            <ta e="T293" id="Seg_3811" s="T292">gehen-PST1-3SG</ta>
            <ta e="T294" id="Seg_3812" s="T293">bemerken-CVB.SEQ</ta>
            <ta e="T295" id="Seg_3813" s="T294">kommen-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T296" id="Seg_3814" s="T295">sagen-PRS.[3SG]</ta>
            <ta e="T297" id="Seg_3815" s="T296">Fuchs.[NOM]</ta>
            <ta e="T298" id="Seg_3816" s="T297">Betrug.[NOM]</ta>
            <ta e="T299" id="Seg_3817" s="T298">kommen-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T300" id="Seg_3818" s="T299">sprechen-PRS.[3SG]</ta>
            <ta e="T301" id="Seg_3819" s="T300">Fuchs.[NOM]</ta>
            <ta e="T302" id="Seg_3820" s="T301">Betrug.[NOM]</ta>
            <ta e="T303" id="Seg_3821" s="T302">gehen-CVB.SEQ</ta>
            <ta e="T304" id="Seg_3822" s="T303">golden</ta>
            <ta e="T305" id="Seg_3823" s="T304">Stadt.[NOM]</ta>
            <ta e="T306" id="Seg_3824" s="T305">Zar-3SG-DAT/LOC</ta>
            <ta e="T307" id="Seg_3825" s="T306">sprechen-PRS.[3SG]</ta>
            <ta e="T308" id="Seg_3826" s="T307">Donner</ta>
            <ta e="T309" id="Seg_3827" s="T308">Zar.[NOM]</ta>
            <ta e="T310" id="Seg_3828" s="T309">2PL-ACC</ta>
            <ta e="T311" id="Seg_3829" s="T310">vernichten-CVB.SIM</ta>
            <ta e="T312" id="Seg_3830" s="T311">kommen-PST1-3SG</ta>
            <ta e="T313" id="Seg_3831" s="T312">fliehen-CVB.SIM</ta>
            <ta e="T314" id="Seg_3832" s="T313">schlagen-EP-IMP.2PL</ta>
            <ta e="T315" id="Seg_3833" s="T314">dieses</ta>
            <ta e="T316" id="Seg_3834" s="T315">Wald-DAT/LOC</ta>
            <ta e="T317" id="Seg_3835" s="T316">hinausgehen-CVB.SEQ</ta>
            <ta e="T318" id="Seg_3836" s="T317">sich.verstecken-EP-IMP.2PL</ta>
            <ta e="T319" id="Seg_3837" s="T318">Kopf-2PL.[NOM]</ta>
            <ta e="T320" id="Seg_3838" s="T319">nur</ta>
            <ta e="T321" id="Seg_3839" s="T320">zu.sehen.sein-PTCP.PRS.[NOM]</ta>
            <ta e="T322" id="Seg_3840" s="T321">wie</ta>
            <ta e="T323" id="Seg_3841" s="T322">golden</ta>
            <ta e="T324" id="Seg_3842" s="T323">Stadt.[NOM]</ta>
            <ta e="T325" id="Seg_3843" s="T324">Zar-3SG-GEN</ta>
            <ta e="T326" id="Seg_3844" s="T325">Volk-3SG.[NOM]</ta>
            <ta e="T327" id="Seg_3845" s="T326">jeder</ta>
            <ta e="T328" id="Seg_3846" s="T327">Wald-DAT/LOC</ta>
            <ta e="T329" id="Seg_3847" s="T328">fliehen-PRS.[3SG]</ta>
            <ta e="T330" id="Seg_3848" s="T329">Donner</ta>
            <ta e="T331" id="Seg_3849" s="T330">Zar-ABL</ta>
            <ta e="T332" id="Seg_3850" s="T331">Angst.haben-CVB.SEQ</ta>
            <ta e="T333" id="Seg_3851" s="T332">Fuchs.[NOM]</ta>
            <ta e="T334" id="Seg_3852" s="T333">Betrüger.[NOM]</ta>
            <ta e="T335" id="Seg_3853" s="T334">Junge-DAT/LOC</ta>
            <ta e="T336" id="Seg_3854" s="T335">weinen-CVB.SEQ</ta>
            <ta e="T337" id="Seg_3855" s="T336">kommen-PRS.[3SG]</ta>
            <ta e="T338" id="Seg_3856" s="T337">Volk-1PL-ACC</ta>
            <ta e="T339" id="Seg_3857" s="T338">böser.Geist.[NOM]</ta>
            <ta e="T340" id="Seg_3858" s="T339">Zar-3SG.[NOM]</ta>
            <ta e="T341" id="Seg_3859" s="T340">vernichten-PST2.[3SG]</ta>
            <ta e="T342" id="Seg_3860" s="T341">essen-CVB.SEQ-essen-CVB.SEQ</ta>
            <ta e="T343" id="Seg_3861" s="T342">nachdem</ta>
            <ta e="T344" id="Seg_3862" s="T343">jenes</ta>
            <ta e="T345" id="Seg_3863" s="T344">Wald-DAT/LOC</ta>
            <ta e="T346" id="Seg_3864" s="T345">sich.verstecken-CVB.SIM</ta>
            <ta e="T347" id="Seg_3865" s="T346">liegen-PRS-3PL</ta>
            <ta e="T348" id="Seg_3866" s="T347">Schwiegervater-2SG-ACC</ta>
            <ta e="T349" id="Seg_3867" s="T348">bitten-EP-MED.[IMP.2SG]</ta>
            <ta e="T350" id="Seg_3868" s="T349">Blitz-VBZ-IMP.3SG</ta>
            <ta e="T351" id="Seg_3869" s="T350">Junge.[NOM]</ta>
            <ta e="T352" id="Seg_3870" s="T351">Schwiegervater-EP-3SG-ABL</ta>
            <ta e="T353" id="Seg_3871" s="T352">bitten-EP-MED-PRS.[3SG]</ta>
            <ta e="T354" id="Seg_3872" s="T353">Blitz-VBZ.[IMP.2SG]</ta>
            <ta e="T355" id="Seg_3873" s="T354">sagen-CVB.SEQ</ta>
            <ta e="T356" id="Seg_3874" s="T355">Donner</ta>
            <ta e="T357" id="Seg_3875" s="T356">Zar.[NOM]</ta>
            <ta e="T358" id="Seg_3876" s="T357">doch</ta>
            <ta e="T359" id="Seg_3877" s="T358">Donner-VBZ-CVB.SEQ</ta>
            <ta e="T360" id="Seg_3878" s="T359">schlagen-PRS.[3SG]</ta>
            <ta e="T361" id="Seg_3879" s="T360">eben.der</ta>
            <ta e="T362" id="Seg_3880" s="T361">Wald-ACC</ta>
            <ta e="T363" id="Seg_3881" s="T362">golden</ta>
            <ta e="T364" id="Seg_3882" s="T363">Stadt.[NOM]</ta>
            <ta e="T365" id="Seg_3883" s="T364">eins</ta>
            <ta e="T366" id="Seg_3884" s="T365">NEG</ta>
            <ta e="T367" id="Seg_3885" s="T366">Mensch-3SG.[NOM]</ta>
            <ta e="T368" id="Seg_3886" s="T367">überleben-NEG.[3SG]</ta>
            <ta e="T369" id="Seg_3887" s="T368">Junge.[NOM]</ta>
            <ta e="T370" id="Seg_3888" s="T369">eben.der</ta>
            <ta e="T371" id="Seg_3889" s="T370">golden</ta>
            <ta e="T372" id="Seg_3890" s="T371">Stadt-DAT/LOC</ta>
            <ta e="T373" id="Seg_3891" s="T372">hineingehen-CVB.SEQ</ta>
            <ta e="T374" id="Seg_3892" s="T373">Leben-INCH-PRS.[3SG]</ta>
            <ta e="T375" id="Seg_3893" s="T374">Zar.[NOM]</ta>
            <ta e="T376" id="Seg_3894" s="T375">werden-PRS.[3SG]</ta>
            <ta e="T377" id="Seg_3895" s="T376">Donner</ta>
            <ta e="T378" id="Seg_3896" s="T377">Zar.[NOM]</ta>
            <ta e="T379" id="Seg_3897" s="T378">Mädchen-3SG-ACC</ta>
            <ta e="T380" id="Seg_3898" s="T379">nehmen-INFER-3SG</ta>
            <ta e="T381" id="Seg_3899" s="T380">jenes.[NOM]</ta>
            <ta e="T382" id="Seg_3900" s="T381">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3901" s="T0">нищий</ta>
            <ta e="T2" id="Seg_3902" s="T1">крестьянин.[NOM]</ta>
            <ta e="T3" id="Seg_3903" s="T2">жить-PST2.[3SG]</ta>
            <ta e="T4" id="Seg_3904" s="T3">этот</ta>
            <ta e="T5" id="Seg_3905" s="T4">крестьянин.[NOM]</ta>
            <ta e="T6" id="Seg_3906" s="T5">умирать-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T7" id="Seg_3907" s="T6">земля.[NOM]</ta>
            <ta e="T8" id="Seg_3908" s="T7">землянка-3SG-DAT/LOC</ta>
            <ta e="T9" id="Seg_3909" s="T8">мальчик.[NOM]</ta>
            <ta e="T10" id="Seg_3910" s="T9">ребенок-3SG.[NOM]</ta>
            <ta e="T11" id="Seg_3911" s="T10">сирота.[NOM]</ta>
            <ta e="T12" id="Seg_3912" s="T11">оставаться-PRS.[3SG]</ta>
            <ta e="T13" id="Seg_3913" s="T12">этот</ta>
            <ta e="T14" id="Seg_3914" s="T13">ребенок.[NOM]</ta>
            <ta e="T15" id="Seg_3915" s="T14">проголодаться-CVB.SEQ</ta>
            <ta e="T16" id="Seg_3916" s="T15">убить-CVB.SIM</ta>
            <ta e="T17" id="Seg_3917" s="T16">быть-PRS.[3SG]</ta>
            <ta e="T18" id="Seg_3918" s="T17">этот-ACC</ta>
            <ta e="T19" id="Seg_3919" s="T18">лиса.[NOM]</ta>
            <ta e="T20" id="Seg_3920" s="T19">найти-PRS.[3SG]</ta>
            <ta e="T21" id="Seg_3921" s="T20">1SG.[NOM]</ta>
            <ta e="T22" id="Seg_3922" s="T21">2SG-ACC</ta>
            <ta e="T23" id="Seg_3923" s="T22">человек.[NOM]</ta>
            <ta e="T24" id="Seg_3924" s="T23">делать-FUT-1SG</ta>
            <ta e="T25" id="Seg_3925" s="T24">говорить-PRS.[3SG]</ta>
            <ta e="T26" id="Seg_3926" s="T25">богатый</ta>
            <ta e="T27" id="Seg_3927" s="T26">человек.[NOM]</ta>
            <ta e="T28" id="Seg_3928" s="T27">быть-FUT-2SG</ta>
            <ta e="T29" id="Seg_3929" s="T28">богатый</ta>
            <ta e="T30" id="Seg_3930" s="T29">жена-ACC</ta>
            <ta e="T31" id="Seg_3931" s="T30">жена.[NOM]</ta>
            <ta e="T32" id="Seg_3932" s="T31">взять-CVB.SEQ</ta>
            <ta e="T33" id="Seg_3933" s="T32">давать-FUT-1SG</ta>
            <ta e="T34" id="Seg_3934" s="T33">так</ta>
            <ta e="T35" id="Seg_3935" s="T34">говорить-CVB.SEQ</ta>
            <ta e="T36" id="Seg_3936" s="T35">после</ta>
            <ta e="T37" id="Seg_3937" s="T36">лиса.[NOM]</ta>
            <ta e="T38" id="Seg_3938" s="T37">Гром</ta>
            <ta e="T39" id="Seg_3939" s="T38">царь-3SG-DAT/LOC</ta>
            <ta e="T40" id="Seg_3940" s="T39">идти-PRS.[3SG]</ta>
            <ta e="T41" id="Seg_3941" s="T40">Гром</ta>
            <ta e="T42" id="Seg_3942" s="T41">царь-3SG-DAT/LOC</ta>
            <ta e="T43" id="Seg_3943" s="T42">доезжать-CVB.SEQ</ta>
            <ta e="T44" id="Seg_3944" s="T43">говорить-PRS.[3SG]</ta>
            <ta e="T45" id="Seg_3945" s="T44">чин-PROPR</ta>
            <ta e="T46" id="Seg_3946" s="T45">богатый</ta>
            <ta e="T47" id="Seg_3947" s="T46">мальчик.[NOM]</ta>
            <ta e="T48" id="Seg_3948" s="T47">человек.[NOM]</ta>
            <ta e="T49" id="Seg_3949" s="T48">есть</ta>
            <ta e="T50" id="Seg_3950" s="T49">3SG.[NOM]</ta>
            <ta e="T51" id="Seg_3951" s="T50">2SG.[NOM]</ta>
            <ta e="T52" id="Seg_3952" s="T51">дочь-2SG-ACC</ta>
            <ta e="T53" id="Seg_3953" s="T52">сватовство-VBZ-EP-CAUS-CVB.SIM</ta>
            <ta e="T54" id="Seg_3954" s="T53">послать-PST1-3SG</ta>
            <ta e="T55" id="Seg_3955" s="T54">дочь-EP-2SG.[NOM]</ta>
            <ta e="T56" id="Seg_3956" s="T55">калым-3SG-ACC</ta>
            <ta e="T57" id="Seg_3957" s="T56">лес.[NOM]</ta>
            <ta e="T58" id="Seg_3958" s="T57">зверь-3SG-ACC</ta>
            <ta e="T59" id="Seg_3959" s="T58">лучший-3SG-ACC</ta>
            <ta e="T60" id="Seg_3960" s="T59">разный-3SG-ACC</ta>
            <ta e="T61" id="Seg_3961" s="T60">послать-PTCP.FUT</ta>
            <ta e="T62" id="Seg_3962" s="T61">быть-PST1-3SG</ta>
            <ta e="T63" id="Seg_3963" s="T62">тот</ta>
            <ta e="T64" id="Seg_3964" s="T63">зверь-PL-ACC</ta>
            <ta e="T65" id="Seg_3965" s="T64">сторона.[NOM]</ta>
            <ta e="T66" id="Seg_3966" s="T65">сторона-3SG-INSTR</ta>
            <ta e="T67" id="Seg_3967" s="T66">дом-DAT/LOC</ta>
            <ta e="T68" id="Seg_3968" s="T67">пища.[NOM]</ta>
            <ta e="T69" id="Seg_3969" s="T68">готовый-VBZ-CVB.SEQ</ta>
            <ta e="T70" id="Seg_3970" s="T69">принимать-FUT-2SG</ta>
            <ta e="T71" id="Seg_3971" s="T70">говорят</ta>
            <ta e="T72" id="Seg_3972" s="T71">говорить-PRS.[3SG]</ta>
            <ta e="T73" id="Seg_3973" s="T72">Гром</ta>
            <ta e="T74" id="Seg_3974" s="T73">царь.[NOM]</ta>
            <ta e="T75" id="Seg_3975" s="T74">согласить-EP-MED-PST2.[3SG]</ta>
            <ta e="T76" id="Seg_3976" s="T75">лиса.[NOM]</ta>
            <ta e="T77" id="Seg_3977" s="T76">обманщик.[NOM]</ta>
            <ta e="T78" id="Seg_3978" s="T77">назад</ta>
            <ta e="T79" id="Seg_3979" s="T78">бегать-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_3980" s="T79">лес.[NOM]</ta>
            <ta e="T81" id="Seg_3981" s="T80">зверь-PL-3SG-ACC</ta>
            <ta e="T82" id="Seg_3982" s="T81">разный-3SG-ACC</ta>
            <ta e="T83" id="Seg_3983" s="T82">встречать-CVB.SEQ</ta>
            <ta e="T84" id="Seg_3984" s="T83">говорить-PRS.[3SG]</ta>
            <ta e="T85" id="Seg_3985" s="T84">Гром</ta>
            <ta e="T86" id="Seg_3986" s="T85">царь.[NOM]</ta>
            <ta e="T87" id="Seg_3987" s="T86">богатство-3SG.[NOM]-сытность-3SG.[NOM]</ta>
            <ta e="T88" id="Seg_3988" s="T87">отказывать-EP-NEG.CVB</ta>
            <ta e="T89" id="Seg_3989" s="T88">2PL-DAT/LOC</ta>
            <ta e="T90" id="Seg_3990" s="T89">пища.[NOM]</ta>
            <ta e="T91" id="Seg_3991" s="T90">готовиться-PST1-3SG</ta>
            <ta e="T92" id="Seg_3992" s="T91">тот-ACC</ta>
            <ta e="T93" id="Seg_3993" s="T92">после</ta>
            <ta e="T94" id="Seg_3994" s="T93">есть-PTCP.PRS</ta>
            <ta e="T95" id="Seg_3995" s="T94">говорят-2PL</ta>
            <ta e="T96" id="Seg_3996" s="T95">зверь.[NOM]</ta>
            <ta e="T97" id="Seg_3997" s="T96">очень.много</ta>
            <ta e="T98" id="Seg_3998" s="T97">стадо.[NOM]</ta>
            <ta e="T99" id="Seg_3999" s="T98">быть-CVB.SEQ</ta>
            <ta e="T100" id="Seg_4000" s="T99">доезжать-PRS-3PL</ta>
            <ta e="T101" id="Seg_4001" s="T100">есть-CVB.SIM</ta>
            <ta e="T102" id="Seg_4002" s="T101">Гром</ta>
            <ta e="T103" id="Seg_4003" s="T102">царь.[NOM]</ta>
            <ta e="T104" id="Seg_4004" s="T103">радоваться-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_4005" s="T104">зять-EP-1SG.[NOM]</ta>
            <ta e="T106" id="Seg_4006" s="T105">богатый</ta>
            <ta e="T107" id="Seg_4007" s="T106">человек.[NOM]</ta>
            <ta e="T108" id="Seg_4008" s="T107">быть-PST2.[3SG]</ta>
            <ta e="T109" id="Seg_4009" s="T108">думать-PRS.[3SG]</ta>
            <ta e="T110" id="Seg_4010" s="T109">зверь-PL.[NOM]</ta>
            <ta e="T111" id="Seg_4011" s="T110">есть-CVB.SIM</ta>
            <ta e="T112" id="Seg_4012" s="T111">стоять-TEMP-3PL</ta>
            <ta e="T113" id="Seg_4013" s="T112">тот.самый</ta>
            <ta e="T114" id="Seg_4014" s="T113">дом-PL-ACC</ta>
            <ta e="T115" id="Seg_4015" s="T114">запирать-CVB.SEQ</ta>
            <ta e="T116" id="Seg_4016" s="T115">бросать-PRS-3PL</ta>
            <ta e="T117" id="Seg_4017" s="T116">лиса.[NOM]</ta>
            <ta e="T118" id="Seg_4018" s="T117">обманщик.[NOM]</ta>
            <ta e="T119" id="Seg_4019" s="T118">Гром</ta>
            <ta e="T120" id="Seg_4020" s="T119">царь-DAT/LOC</ta>
            <ta e="T121" id="Seg_4021" s="T120">доезжать-PRS.[3SG]</ta>
            <ta e="T122" id="Seg_4022" s="T121">эй</ta>
            <ta e="T123" id="Seg_4023" s="T122">зверь-PL.[NOM]</ta>
            <ta e="T124" id="Seg_4024" s="T123">приходить-PST1-3PL</ta>
            <ta e="T125" id="Seg_4025" s="T124">Q</ta>
            <ta e="T126" id="Seg_4026" s="T125">приходить-PST1-3PL</ta>
            <ta e="T127" id="Seg_4027" s="T126">зять.[NOM]</ta>
            <ta e="T128" id="Seg_4028" s="T127">мальчик.[NOM]</ta>
            <ta e="T129" id="Seg_4029" s="T128">свобоное.время-POSS</ta>
            <ta e="T130" id="Seg_4030" s="T129">NEG.[3SG]</ta>
            <ta e="T131" id="Seg_4031" s="T130">скоро</ta>
            <ta e="T132" id="Seg_4032" s="T131">золотой</ta>
            <ta e="T133" id="Seg_4033" s="T132">корабль-EP-INSTR</ta>
            <ta e="T134" id="Seg_4034" s="T133">приходить-FUT.[3SG]</ta>
            <ta e="T135" id="Seg_4035" s="T134">говорить-PRS.[3SG]</ta>
            <ta e="T136" id="Seg_4036" s="T135">мальчик-3SG-DAT/LOC</ta>
            <ta e="T137" id="Seg_4037" s="T136">назад</ta>
            <ta e="T138" id="Seg_4038" s="T137">бегать-CVB.SEQ</ta>
            <ta e="T139" id="Seg_4039" s="T138">доезжать-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_4040" s="T139">тальник-EP-INSTR</ta>
            <ta e="T141" id="Seg_4041" s="T140">плохой</ta>
            <ta e="T142" id="Seg_4042" s="T141">плот.[NOM]</ta>
            <ta e="T143" id="Seg_4043" s="T142">делать-CAUS-PRS.[3SG]</ta>
            <ta e="T144" id="Seg_4044" s="T143">разный</ta>
            <ta e="T145" id="Seg_4045" s="T144">земля.[NOM]</ta>
            <ta e="T146" id="Seg_4046" s="T145">цвет-3SG-INSTR</ta>
            <ta e="T147" id="Seg_4047" s="T146">украшать-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T148" id="Seg_4048" s="T147">далекий-ABL</ta>
            <ta e="T149" id="Seg_4049" s="T148">нарядный.[NOM]</ta>
            <ta e="T150" id="Seg_4050" s="T149">красивый.[NOM]</ta>
            <ta e="T151" id="Seg_4051" s="T150">быть-CVB.SEQ</ta>
            <ta e="T152" id="Seg_4052" s="T151">быть.видно-PRS.[3SG]</ta>
            <ta e="T153" id="Seg_4053" s="T152">ветер.[NOM]</ta>
            <ta e="T154" id="Seg_4054" s="T153">день.[NOM]</ta>
            <ta e="T155" id="Seg_4055" s="T154">Гром</ta>
            <ta e="T156" id="Seg_4056" s="T155">царь-DAT/LOC</ta>
            <ta e="T157" id="Seg_4057" s="T156">течь-PRS-3PL</ta>
            <ta e="T158" id="Seg_4058" s="T157">Гром</ta>
            <ta e="T159" id="Seg_4059" s="T158">царь.[NOM]</ta>
            <ta e="T160" id="Seg_4060" s="T159">видеть-PST2-3SG</ta>
            <ta e="T161" id="Seg_4061" s="T160">море.[NOM]</ta>
            <ta e="T162" id="Seg_4062" s="T161">через</ta>
            <ta e="T163" id="Seg_4063" s="T162">золотой</ta>
            <ta e="T164" id="Seg_4064" s="T163">корабль.[NOM]</ta>
            <ta e="T165" id="Seg_4065" s="T164">идти-PRS.[3SG]</ta>
            <ta e="T166" id="Seg_4066" s="T165">нарядный.[NOM]</ta>
            <ta e="T167" id="Seg_4067" s="T166">цветной.[NOM]</ta>
            <ta e="T168" id="Seg_4068" s="T167">царь.[NOM]</ta>
            <ta e="T169" id="Seg_4069" s="T168">радоваться-PRS.[3SG]</ta>
            <ta e="T170" id="Seg_4070" s="T169">ветер.[NOM]</ta>
            <ta e="T171" id="Seg_4071" s="T170">падать-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T172" id="Seg_4072" s="T171">плот.[NOM]</ta>
            <ta e="T173" id="Seg_4073" s="T172">разбрасывать-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T174" id="Seg_4074" s="T173">оставаться-PRS.[3SG]</ta>
            <ta e="T175" id="Seg_4075" s="T174">лиса.[NOM]</ta>
            <ta e="T176" id="Seg_4076" s="T175">обманщик.[NOM]</ta>
            <ta e="T177" id="Seg_4077" s="T176">Гром</ta>
            <ta e="T178" id="Seg_4078" s="T177">царь-DAT/LOC</ta>
            <ta e="T179" id="Seg_4079" s="T178">бегать-CVB.SEQ</ta>
            <ta e="T180" id="Seg_4080" s="T179">доезжать-PRS.[3SG]</ta>
            <ta e="T181" id="Seg_4081" s="T180">говорить-PRS.[3SG]</ta>
            <ta e="T182" id="Seg_4082" s="T181">беда.[NOM]</ta>
            <ta e="T183" id="Seg_4083" s="T182">быть-PST1-3SG</ta>
            <ta e="T184" id="Seg_4084" s="T183">золотой</ta>
            <ta e="T185" id="Seg_4085" s="T184">корабль.[NOM]</ta>
            <ta e="T186" id="Seg_4086" s="T185">тонуть-PST1-3SG</ta>
            <ta e="T187" id="Seg_4087" s="T186">зять-EP-2SG.[NOM]</ta>
            <ta e="T188" id="Seg_4088" s="T187">человек.[NOM]</ta>
            <ta e="T189" id="Seg_4089" s="T188">отважный.[NOM]</ta>
            <ta e="T190" id="Seg_4090" s="T189">быть-CVB.SEQ</ta>
            <ta e="T191" id="Seg_4091" s="T190">выжить-PST1-3SG</ta>
            <ta e="T192" id="Seg_4092" s="T191">багаж-3SG.[NOM]</ta>
            <ta e="T193" id="Seg_4093" s="T192">каждый-3SG.[NOM]</ta>
            <ta e="T194" id="Seg_4094" s="T193">вода-DAT/LOC</ta>
            <ta e="T195" id="Seg_4095" s="T194">идти-PST1-3SG</ta>
            <ta e="T196" id="Seg_4096" s="T195">сам-3SG.[NOM]</ta>
            <ta e="T197" id="Seg_4097" s="T196">голый.[NOM]</ta>
            <ta e="T198" id="Seg_4098" s="T197">оставаться-PST1-3SG</ta>
            <ta e="T199" id="Seg_4099" s="T198">одежда-PART</ta>
            <ta e="T200" id="Seg_4100" s="T199">послать.[IMP.2SG]</ta>
            <ta e="T201" id="Seg_4101" s="T200">Гром</ta>
            <ta e="T202" id="Seg_4102" s="T201">царь.[NOM]</ta>
            <ta e="T203" id="Seg_4103" s="T202">мальчик-DAT/LOC</ta>
            <ta e="T204" id="Seg_4104" s="T203">лучший</ta>
            <ta e="T205" id="Seg_4105" s="T204">одежда-ACC</ta>
            <ta e="T206" id="Seg_4106" s="T205">сам-3SG-GEN</ta>
            <ta e="T207" id="Seg_4107" s="T206">корабль-3SG-ACC</ta>
            <ta e="T208" id="Seg_4108" s="T207">послать-PRS.[3SG]</ta>
            <ta e="T209" id="Seg_4109" s="T208">лиса.[NOM]</ta>
            <ta e="T210" id="Seg_4110" s="T209">обманщик.[NOM]</ta>
            <ta e="T211" id="Seg_4111" s="T210">мальчик-3SG-ACC</ta>
            <ta e="T212" id="Seg_4112" s="T211">советовать-PRS.[3SG]</ta>
            <ta e="T213" id="Seg_4113" s="T212">царь-DAT/LOC</ta>
            <ta e="T214" id="Seg_4114" s="T213">доезжать-CVB.SEQ-2SG</ta>
            <ta e="T215" id="Seg_4115" s="T214">одежда-2SG-ACC</ta>
            <ta e="T216" id="Seg_4116" s="T215">видеть-EP-MED-EP-NEG.[IMP.2SG]</ta>
            <ta e="T217" id="Seg_4117" s="T216">удивляться-FUT-3PL</ta>
            <ta e="T218" id="Seg_4118" s="T217">поклон.[NOM]</ta>
            <ta e="T219" id="Seg_4119" s="T218">только</ta>
            <ta e="T220" id="Seg_4120" s="T219">делать-CVB.SEQ</ta>
            <ta e="T221" id="Seg_4121" s="T220">здороваться.[IMP.2SG]</ta>
            <ta e="T222" id="Seg_4122" s="T221">есть-PTCP.PRS-2SG-DAT/LOC</ta>
            <ta e="T223" id="Seg_4123" s="T222">люди.[NOM]</ta>
            <ta e="T224" id="Seg_4124" s="T223">есть-PTCP.PRS-3SG-ACC</ta>
            <ta e="T225" id="Seg_4125" s="T224">видеть-CVB.SEQ</ta>
            <ta e="T226" id="Seg_4126" s="T225">есть.[IMP.2SG]</ta>
            <ta e="T227" id="Seg_4127" s="T226">вино-ACC</ta>
            <ta e="T228" id="Seg_4128" s="T227">очень</ta>
            <ta e="T229" id="Seg_4129" s="T228">мало</ta>
            <ta e="T230" id="Seg_4130" s="T229">пить.[IMP.2SG]</ta>
            <ta e="T231" id="Seg_4131" s="T230">мальчик.[NOM]</ta>
            <ta e="T232" id="Seg_4132" s="T231">Гром</ta>
            <ta e="T233" id="Seg_4133" s="T232">царь-DAT/LOC</ta>
            <ta e="T234" id="Seg_4134" s="T233">доезжать-PRS.[3SG]</ta>
            <ta e="T235" id="Seg_4135" s="T234">корабль-3SG-ACC</ta>
            <ta e="T236" id="Seg_4136" s="T235">садиться-CVB.SEQ</ta>
            <ta e="T237" id="Seg_4137" s="T236">одежда-3SG-ACC</ta>
            <ta e="T238" id="Seg_4138" s="T237">одеваться-CVB.SEQ</ta>
            <ta e="T239" id="Seg_4139" s="T238">свадьба.[NOM]</ta>
            <ta e="T240" id="Seg_4140" s="T239">необузданный.[NOM]</ta>
            <ta e="T241" id="Seg_4141" s="T240">быть-PRS.[3SG]</ta>
            <ta e="T242" id="Seg_4142" s="T241">корабль-3SG-INSTR</ta>
            <ta e="T243" id="Seg_4143" s="T242">возвращаться-PRS-3PL</ta>
            <ta e="T244" id="Seg_4144" s="T243">лиса.[NOM]</ta>
            <ta e="T245" id="Seg_4145" s="T244">обманщик.[NOM]</ta>
            <ta e="T246" id="Seg_4146" s="T245">говорить-PRS.[3SG]</ta>
            <ta e="T247" id="Seg_4147" s="T246">земля.[NOM]</ta>
            <ta e="T248" id="Seg_4148" s="T247">землянка-2SG-DAT/LOC</ta>
            <ta e="T249" id="Seg_4149" s="T248">останавливаться-NEG.CVB.SIM</ta>
            <ta e="T250" id="Seg_4150" s="T249">проехать-FUT.[IMP.2SG]</ta>
            <ta e="T251" id="Seg_4151" s="T250">чужой</ta>
            <ta e="T252" id="Seg_4152" s="T251">царь.[NOM]</ta>
            <ta e="T253" id="Seg_4153" s="T252">золотой</ta>
            <ta e="T254" id="Seg_4154" s="T253">город-3SG-DAT/LOC</ta>
            <ta e="T255" id="Seg_4155" s="T254">приближаться-CVB.SEQ</ta>
            <ta e="T256" id="Seg_4156" s="T255">после</ta>
            <ta e="T257" id="Seg_4157" s="T256">останавливаться-FUT.[IMP.2SG]</ta>
            <ta e="T258" id="Seg_4158" s="T257">и</ta>
            <ta e="T259" id="Seg_4159" s="T258">1SG-ACC</ta>
            <ta e="T260" id="Seg_4160" s="T259">звать-CAUS-FUT.[IMP.2SG]</ta>
            <ta e="T261" id="Seg_4161" s="T260">народ-EP-1SG.[NOM]-народ-EP-1SG.[NOM]</ta>
            <ta e="T262" id="Seg_4162" s="T261">сколько</ta>
            <ta e="T263" id="Seg_4163" s="T262">очень</ta>
            <ta e="T264" id="Seg_4164" s="T263">жить-PTCP.PRS-3SG-ACC</ta>
            <ta e="T265" id="Seg_4165" s="T264">знать-CVB.SEQ</ta>
            <ta e="T266" id="Seg_4166" s="T265">приходить.[IMP.2SG]</ta>
            <ta e="T267" id="Seg_4167" s="T266">говорить-FUT.[IMP.2SG]</ta>
            <ta e="T268" id="Seg_4168" s="T267">1SG-DAT/LOC</ta>
            <ta e="T269" id="Seg_4169" s="T268">лиса.[NOM]</ta>
            <ta e="T270" id="Seg_4170" s="T269">говорить-PTCP.PST-3SG-ACC</ta>
            <ta e="T271" id="Seg_4171" s="T270">как</ta>
            <ta e="T272" id="Seg_4172" s="T271">мальчик.[NOM]</ta>
            <ta e="T273" id="Seg_4173" s="T272">сам-3SG-GEN</ta>
            <ta e="T274" id="Seg_4174" s="T273">землянка-3SG-ACC</ta>
            <ta e="T275" id="Seg_4175" s="T274">проехать-PRS.[3SG]</ta>
            <ta e="T276" id="Seg_4176" s="T275">чужой</ta>
            <ta e="T277" id="Seg_4177" s="T276">царь.[NOM]</ta>
            <ta e="T278" id="Seg_4178" s="T277">золотой</ta>
            <ta e="T279" id="Seg_4179" s="T278">город-3SG-DAT/LOC</ta>
            <ta e="T280" id="Seg_4180" s="T279">приближаться-PRS.[3SG]</ta>
            <ta e="T281" id="Seg_4181" s="T280">останавливаться-CVB.ANT</ta>
            <ta e="T282" id="Seg_4182" s="T281">стоять-CVB.SEQ</ta>
            <ta e="T283" id="Seg_4183" s="T282">говорить-PRS.[3SG]</ta>
            <ta e="T284" id="Seg_4184" s="T283">народ-EP-1SG.[NOM]-народ-EP-1SG.[NOM]</ta>
            <ta e="T285" id="Seg_4185" s="T284">сколько</ta>
            <ta e="T286" id="Seg_4186" s="T285">очень</ta>
            <ta e="T287" id="Seg_4187" s="T286">жить-PTCP.PRS-3SG-ACC</ta>
            <ta e="T288" id="Seg_4188" s="T287">знать-FUT-1SG</ta>
            <ta e="T289" id="Seg_4189" s="T288">быть-PST1-3SG</ta>
            <ta e="T290" id="Seg_4190" s="T289">тот.самый</ta>
            <ta e="T291" id="Seg_4191" s="T290">лиса-EP-1SG.[NOM]</ta>
            <ta e="T292" id="Seg_4192" s="T291">куда</ta>
            <ta e="T293" id="Seg_4193" s="T292">идти-PST1-3SG</ta>
            <ta e="T294" id="Seg_4194" s="T293">узнавать-CVB.SEQ</ta>
            <ta e="T295" id="Seg_4195" s="T294">приходить-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T296" id="Seg_4196" s="T295">говорить-PRS.[3SG]</ta>
            <ta e="T297" id="Seg_4197" s="T296">лиса.[NOM]</ta>
            <ta e="T298" id="Seg_4198" s="T297">обманщик.[NOM]</ta>
            <ta e="T299" id="Seg_4199" s="T298">приходить-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T300" id="Seg_4200" s="T299">говорить-PRS.[3SG]</ta>
            <ta e="T301" id="Seg_4201" s="T300">лиса.[NOM]</ta>
            <ta e="T302" id="Seg_4202" s="T301">обманщик.[NOM]</ta>
            <ta e="T303" id="Seg_4203" s="T302">идти-CVB.SEQ</ta>
            <ta e="T304" id="Seg_4204" s="T303">золотой</ta>
            <ta e="T305" id="Seg_4205" s="T304">город.[NOM]</ta>
            <ta e="T306" id="Seg_4206" s="T305">царь-3SG-DAT/LOC</ta>
            <ta e="T307" id="Seg_4207" s="T306">говорить-PRS.[3SG]</ta>
            <ta e="T308" id="Seg_4208" s="T307">Гром</ta>
            <ta e="T309" id="Seg_4209" s="T308">царь.[NOM]</ta>
            <ta e="T310" id="Seg_4210" s="T309">2PL-ACC</ta>
            <ta e="T311" id="Seg_4211" s="T310">уничтожать-CVB.SIM</ta>
            <ta e="T312" id="Seg_4212" s="T311">приходить-PST1-3SG</ta>
            <ta e="T313" id="Seg_4213" s="T312">убежать-CVB.SIM</ta>
            <ta e="T314" id="Seg_4214" s="T313">бить-EP-IMP.2PL</ta>
            <ta e="T315" id="Seg_4215" s="T314">тот</ta>
            <ta e="T316" id="Seg_4216" s="T315">лес-DAT/LOC</ta>
            <ta e="T317" id="Seg_4217" s="T316">выйти-CVB.SEQ</ta>
            <ta e="T318" id="Seg_4218" s="T317">прятаться-EP-IMP.2PL</ta>
            <ta e="T319" id="Seg_4219" s="T318">голова-2PL.[NOM]</ta>
            <ta e="T320" id="Seg_4220" s="T319">только</ta>
            <ta e="T321" id="Seg_4221" s="T320">быть.видно-PTCP.PRS.[NOM]</ta>
            <ta e="T322" id="Seg_4222" s="T321">как</ta>
            <ta e="T323" id="Seg_4223" s="T322">золотой</ta>
            <ta e="T324" id="Seg_4224" s="T323">город.[NOM]</ta>
            <ta e="T325" id="Seg_4225" s="T324">царь-3SG-GEN</ta>
            <ta e="T326" id="Seg_4226" s="T325">народ-3SG.[NOM]</ta>
            <ta e="T327" id="Seg_4227" s="T326">целый</ta>
            <ta e="T328" id="Seg_4228" s="T327">лес-DAT/LOC</ta>
            <ta e="T329" id="Seg_4229" s="T328">убежать-PRS.[3SG]</ta>
            <ta e="T330" id="Seg_4230" s="T329">Гром</ta>
            <ta e="T331" id="Seg_4231" s="T330">царь-ABL</ta>
            <ta e="T332" id="Seg_4232" s="T331">бояться-CVB.SEQ</ta>
            <ta e="T333" id="Seg_4233" s="T332">лиса.[NOM]</ta>
            <ta e="T334" id="Seg_4234" s="T333">обманщик.[NOM]</ta>
            <ta e="T335" id="Seg_4235" s="T334">мальчик-DAT/LOC</ta>
            <ta e="T336" id="Seg_4236" s="T335">плакать-CVB.SEQ</ta>
            <ta e="T337" id="Seg_4237" s="T336">приходить-PRS.[3SG]</ta>
            <ta e="T338" id="Seg_4238" s="T337">народ-1PL-ACC</ta>
            <ta e="T339" id="Seg_4239" s="T338">злой.дух.[NOM]</ta>
            <ta e="T340" id="Seg_4240" s="T339">царь-3SG.[NOM]</ta>
            <ta e="T341" id="Seg_4241" s="T340">истребить-PST2.[3SG]</ta>
            <ta e="T342" id="Seg_4242" s="T341">есть-CVB.SEQ-есть-CVB.SEQ</ta>
            <ta e="T343" id="Seg_4243" s="T342">после</ta>
            <ta e="T344" id="Seg_4244" s="T343">тот</ta>
            <ta e="T345" id="Seg_4245" s="T344">лес-DAT/LOC</ta>
            <ta e="T346" id="Seg_4246" s="T345">прятаться-CVB.SIM</ta>
            <ta e="T347" id="Seg_4247" s="T346">лежать-PRS-3PL</ta>
            <ta e="T348" id="Seg_4248" s="T347">тесть-2SG-ACC</ta>
            <ta e="T349" id="Seg_4249" s="T348">попросить-EP-MED.[IMP.2SG]</ta>
            <ta e="T350" id="Seg_4250" s="T349">молния-VBZ-IMP.3SG</ta>
            <ta e="T351" id="Seg_4251" s="T350">мальчик.[NOM]</ta>
            <ta e="T352" id="Seg_4252" s="T351">тесть-EP-3SG-ABL</ta>
            <ta e="T353" id="Seg_4253" s="T352">попросить-EP-MED-PRS.[3SG]</ta>
            <ta e="T354" id="Seg_4254" s="T353">молния-VBZ.[IMP.2SG]</ta>
            <ta e="T355" id="Seg_4255" s="T354">говорить-CVB.SEQ</ta>
            <ta e="T356" id="Seg_4256" s="T355">Гром</ta>
            <ta e="T357" id="Seg_4257" s="T356">царь.[NOM]</ta>
            <ta e="T358" id="Seg_4258" s="T357">вот</ta>
            <ta e="T359" id="Seg_4259" s="T358">гром-VBZ-CVB.SEQ</ta>
            <ta e="T360" id="Seg_4260" s="T359">ударить-PRS.[3SG]</ta>
            <ta e="T361" id="Seg_4261" s="T360">тот.самый</ta>
            <ta e="T362" id="Seg_4262" s="T361">лес-ACC</ta>
            <ta e="T363" id="Seg_4263" s="T362">золотой</ta>
            <ta e="T364" id="Seg_4264" s="T363">город.[NOM]</ta>
            <ta e="T365" id="Seg_4265" s="T364">один</ta>
            <ta e="T366" id="Seg_4266" s="T365">NEG</ta>
            <ta e="T367" id="Seg_4267" s="T366">человек-3SG.[NOM]</ta>
            <ta e="T368" id="Seg_4268" s="T367">выжить-NEG.[3SG]</ta>
            <ta e="T369" id="Seg_4269" s="T368">мальчик.[NOM]</ta>
            <ta e="T370" id="Seg_4270" s="T369">тот.самый</ta>
            <ta e="T371" id="Seg_4271" s="T370">золотой</ta>
            <ta e="T372" id="Seg_4272" s="T371">город-DAT/LOC</ta>
            <ta e="T373" id="Seg_4273" s="T372">входить-CVB.SEQ</ta>
            <ta e="T374" id="Seg_4274" s="T373">жизнь-INCH-PRS.[3SG]</ta>
            <ta e="T375" id="Seg_4275" s="T374">царь.[NOM]</ta>
            <ta e="T376" id="Seg_4276" s="T375">быть-PRS.[3SG]</ta>
            <ta e="T377" id="Seg_4277" s="T376">Гром</ta>
            <ta e="T378" id="Seg_4278" s="T377">царь.[NOM]</ta>
            <ta e="T379" id="Seg_4279" s="T378">девушка-3SG-ACC</ta>
            <ta e="T380" id="Seg_4280" s="T379">взять-INFER-3SG</ta>
            <ta e="T381" id="Seg_4281" s="T380">тот.[NOM]</ta>
            <ta e="T382" id="Seg_4282" s="T381">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_4283" s="T0">adj</ta>
            <ta e="T2" id="Seg_4284" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_4285" s="T2">v-v:tense-v:pred.pn</ta>
            <ta e="T4" id="Seg_4286" s="T3">dempro</ta>
            <ta e="T5" id="Seg_4287" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_4288" s="T5">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T7" id="Seg_4289" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_4290" s="T7">n-n:poss-n:case</ta>
            <ta e="T9" id="Seg_4291" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_4292" s="T9">n-n:(poss)-n:case</ta>
            <ta e="T11" id="Seg_4293" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_4294" s="T11">v-v:tense-v:pred.pn</ta>
            <ta e="T13" id="Seg_4295" s="T12">dempro</ta>
            <ta e="T14" id="Seg_4296" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_4297" s="T14">v-v:cvb</ta>
            <ta e="T16" id="Seg_4298" s="T15">v-v:cvb</ta>
            <ta e="T17" id="Seg_4299" s="T16">v-v:tense-v:pred.pn</ta>
            <ta e="T18" id="Seg_4300" s="T17">dempro-pro:case</ta>
            <ta e="T19" id="Seg_4301" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_4302" s="T19">v-v:tense-v:pred.pn</ta>
            <ta e="T21" id="Seg_4303" s="T20">pers-pro:case</ta>
            <ta e="T22" id="Seg_4304" s="T21">pers-pro:case</ta>
            <ta e="T23" id="Seg_4305" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_4306" s="T23">v-v:tense-v:poss.pn</ta>
            <ta e="T25" id="Seg_4307" s="T24">v-v:tense-v:pred.pn</ta>
            <ta e="T26" id="Seg_4308" s="T25">adj</ta>
            <ta e="T27" id="Seg_4309" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_4310" s="T27">v-v:tense-v:poss.pn</ta>
            <ta e="T29" id="Seg_4311" s="T28">adj</ta>
            <ta e="T30" id="Seg_4312" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_4313" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_4314" s="T31">v-v:cvb</ta>
            <ta e="T33" id="Seg_4315" s="T32">v-v:tense-v:poss.pn</ta>
            <ta e="T34" id="Seg_4316" s="T33">adv</ta>
            <ta e="T35" id="Seg_4317" s="T34">v-v:cvb</ta>
            <ta e="T36" id="Seg_4318" s="T35">post</ta>
            <ta e="T37" id="Seg_4319" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_4320" s="T37">propr</ta>
            <ta e="T39" id="Seg_4321" s="T38">n-n:poss-n:case</ta>
            <ta e="T40" id="Seg_4322" s="T39">v-v:tense-v:pred.pn</ta>
            <ta e="T41" id="Seg_4323" s="T40">propr</ta>
            <ta e="T42" id="Seg_4324" s="T41">n-n:poss-n:case</ta>
            <ta e="T43" id="Seg_4325" s="T42">v-v:cvb</ta>
            <ta e="T44" id="Seg_4326" s="T43">v-v:tense-v:pred.pn</ta>
            <ta e="T45" id="Seg_4327" s="T44">n-n&gt;adj</ta>
            <ta e="T46" id="Seg_4328" s="T45">adj</ta>
            <ta e="T47" id="Seg_4329" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_4330" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_4331" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_4332" s="T49">pers-pro:case</ta>
            <ta e="T51" id="Seg_4333" s="T50">pers-pro:case</ta>
            <ta e="T52" id="Seg_4334" s="T51">n-n:poss-n:case</ta>
            <ta e="T53" id="Seg_4335" s="T52">n-n&gt;v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T54" id="Seg_4336" s="T53">v-v:tense-v:poss.pn</ta>
            <ta e="T55" id="Seg_4337" s="T54">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T56" id="Seg_4338" s="T55">n-n:poss-n:case</ta>
            <ta e="T57" id="Seg_4339" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_4340" s="T57">n-n:poss-n:case</ta>
            <ta e="T59" id="Seg_4341" s="T58">adj-n:poss-n:case</ta>
            <ta e="T60" id="Seg_4342" s="T59">adj-n:poss-n:case</ta>
            <ta e="T61" id="Seg_4343" s="T60">v-v:ptcp</ta>
            <ta e="T62" id="Seg_4344" s="T61">v-v:tense-v:poss.pn</ta>
            <ta e="T63" id="Seg_4345" s="T62">dempro</ta>
            <ta e="T64" id="Seg_4346" s="T63">n-n:(num)-n:case</ta>
            <ta e="T65" id="Seg_4347" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_4348" s="T65">n-n:poss-n:case</ta>
            <ta e="T67" id="Seg_4349" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_4350" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_4351" s="T68">adj-adj&gt;v-v:cvb</ta>
            <ta e="T70" id="Seg_4352" s="T69">v-v:tense-v:poss.pn</ta>
            <ta e="T71" id="Seg_4353" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_4354" s="T71">v-v:tense-v:pred.pn</ta>
            <ta e="T73" id="Seg_4355" s="T72">propr</ta>
            <ta e="T74" id="Seg_4356" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_4357" s="T74">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T76" id="Seg_4358" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_4359" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_4360" s="T77">adv</ta>
            <ta e="T79" id="Seg_4361" s="T78">v-v:tense-v:pred.pn</ta>
            <ta e="T80" id="Seg_4362" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_4363" s="T80">n-n:(num)-n:poss-n:case</ta>
            <ta e="T82" id="Seg_4364" s="T81">adj-n:poss-n:case</ta>
            <ta e="T83" id="Seg_4365" s="T82">v-v:cvb</ta>
            <ta e="T84" id="Seg_4366" s="T83">v-v:tense-v:pred.pn</ta>
            <ta e="T85" id="Seg_4367" s="T84">propr</ta>
            <ta e="T86" id="Seg_4368" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_4369" s="T86">n-n:(poss)-n:case-n-n:(poss)-n:case</ta>
            <ta e="T88" id="Seg_4370" s="T87">v-v:(ins)-v:cvb</ta>
            <ta e="T89" id="Seg_4371" s="T88">pers-pro:case</ta>
            <ta e="T90" id="Seg_4372" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_4373" s="T90">v-v:tense-v:poss.pn</ta>
            <ta e="T92" id="Seg_4374" s="T91">dempro-pro:case</ta>
            <ta e="T93" id="Seg_4375" s="T92">post</ta>
            <ta e="T94" id="Seg_4376" s="T93">v-v:ptcp</ta>
            <ta e="T95" id="Seg_4377" s="T94">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T96" id="Seg_4378" s="T95">n-n:case</ta>
            <ta e="T97" id="Seg_4379" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_4380" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_4381" s="T98">v-v:cvb</ta>
            <ta e="T100" id="Seg_4382" s="T99">v-v:tense-v:pred.pn</ta>
            <ta e="T101" id="Seg_4383" s="T100">v-v:cvb</ta>
            <ta e="T102" id="Seg_4384" s="T101">propr</ta>
            <ta e="T103" id="Seg_4385" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_4386" s="T103">v-v:tense-v:pred.pn</ta>
            <ta e="T105" id="Seg_4387" s="T104">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T106" id="Seg_4388" s="T105">adj</ta>
            <ta e="T107" id="Seg_4389" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_4390" s="T107">v-v:tense-v:pred.pn</ta>
            <ta e="T109" id="Seg_4391" s="T108">v-v:tense-v:pred.pn</ta>
            <ta e="T110" id="Seg_4392" s="T109">n-n:(num)-n:case</ta>
            <ta e="T111" id="Seg_4393" s="T110">v-v:cvb</ta>
            <ta e="T112" id="Seg_4394" s="T111">v-v:mood-v:temp.pn</ta>
            <ta e="T113" id="Seg_4395" s="T112">dempro</ta>
            <ta e="T114" id="Seg_4396" s="T113">n-n:(num)-n:case</ta>
            <ta e="T115" id="Seg_4397" s="T114">v-v:cvb</ta>
            <ta e="T116" id="Seg_4398" s="T115">v-v:tense-v:pred.pn</ta>
            <ta e="T117" id="Seg_4399" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_4400" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_4401" s="T118">propr</ta>
            <ta e="T120" id="Seg_4402" s="T119">n-n:case</ta>
            <ta e="T121" id="Seg_4403" s="T120">v-v:tense-v:pred.pn</ta>
            <ta e="T122" id="Seg_4404" s="T121">interj</ta>
            <ta e="T123" id="Seg_4405" s="T122">n-n:(num)-n:case</ta>
            <ta e="T124" id="Seg_4406" s="T123">v-v:tense-v:pred.pn</ta>
            <ta e="T125" id="Seg_4407" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_4408" s="T125">v-v:tense-v:pred.pn</ta>
            <ta e="T127" id="Seg_4409" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_4410" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_4411" s="T128">n-n:(poss)</ta>
            <ta e="T130" id="Seg_4412" s="T129">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T131" id="Seg_4413" s="T130">adv</ta>
            <ta e="T132" id="Seg_4414" s="T131">adj</ta>
            <ta e="T133" id="Seg_4415" s="T132">n-n:(ins)-n:case</ta>
            <ta e="T134" id="Seg_4416" s="T133">v-v:tense-v:poss.pn</ta>
            <ta e="T135" id="Seg_4417" s="T134">v-v:tense-v:pred.pn</ta>
            <ta e="T136" id="Seg_4418" s="T135">n-n:poss-n:case</ta>
            <ta e="T137" id="Seg_4419" s="T136">adv</ta>
            <ta e="T138" id="Seg_4420" s="T137">v-v:cvb</ta>
            <ta e="T139" id="Seg_4421" s="T138">v-v:tense-v:pred.pn</ta>
            <ta e="T140" id="Seg_4422" s="T139">n-n:(ins)-n:case</ta>
            <ta e="T141" id="Seg_4423" s="T140">adj</ta>
            <ta e="T142" id="Seg_4424" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_4425" s="T142">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T144" id="Seg_4426" s="T143">adj</ta>
            <ta e="T145" id="Seg_4427" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_4428" s="T145">n-n:poss-n:case</ta>
            <ta e="T147" id="Seg_4429" s="T146">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T148" id="Seg_4430" s="T147">adj-n:case</ta>
            <ta e="T149" id="Seg_4431" s="T148">adj-n:case</ta>
            <ta e="T150" id="Seg_4432" s="T149">adj-n:case</ta>
            <ta e="T151" id="Seg_4433" s="T150">v-v:cvb</ta>
            <ta e="T152" id="Seg_4434" s="T151">v-v:tense-v:pred.pn</ta>
            <ta e="T153" id="Seg_4435" s="T152">n-n:case</ta>
            <ta e="T154" id="Seg_4436" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_4437" s="T154">propr</ta>
            <ta e="T156" id="Seg_4438" s="T155">n-n:case</ta>
            <ta e="T157" id="Seg_4439" s="T156">v-v:tense-v:pred.pn</ta>
            <ta e="T158" id="Seg_4440" s="T157">propr</ta>
            <ta e="T159" id="Seg_4441" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_4442" s="T159">v-v:tense-v:poss.pn</ta>
            <ta e="T161" id="Seg_4443" s="T160">n-n:case</ta>
            <ta e="T162" id="Seg_4444" s="T161">post</ta>
            <ta e="T163" id="Seg_4445" s="T162">adj</ta>
            <ta e="T164" id="Seg_4446" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_4447" s="T164">v-v:tense-v:pred.pn</ta>
            <ta e="T166" id="Seg_4448" s="T165">adj-n:case</ta>
            <ta e="T167" id="Seg_4449" s="T166">adj-n:case</ta>
            <ta e="T168" id="Seg_4450" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_4451" s="T168">v-v:tense-v:pred.pn</ta>
            <ta e="T170" id="Seg_4452" s="T169">n-n:case</ta>
            <ta e="T171" id="Seg_4453" s="T170">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T172" id="Seg_4454" s="T171">n-n:case</ta>
            <ta e="T173" id="Seg_4455" s="T172">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T174" id="Seg_4456" s="T173">v-v:tense-v:pred.pn</ta>
            <ta e="T175" id="Seg_4457" s="T174">n-n:case</ta>
            <ta e="T176" id="Seg_4458" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_4459" s="T176">propr</ta>
            <ta e="T178" id="Seg_4460" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_4461" s="T178">v-v:cvb</ta>
            <ta e="T180" id="Seg_4462" s="T179">v-v:tense-v:pred.pn</ta>
            <ta e="T181" id="Seg_4463" s="T180">v-v:tense-v:pred.pn</ta>
            <ta e="T182" id="Seg_4464" s="T181">n-n:case</ta>
            <ta e="T183" id="Seg_4465" s="T182">v-v:tense-v:poss.pn</ta>
            <ta e="T184" id="Seg_4466" s="T183">adj</ta>
            <ta e="T185" id="Seg_4467" s="T184">n-n:case</ta>
            <ta e="T186" id="Seg_4468" s="T185">v-v:tense-v:poss.pn</ta>
            <ta e="T187" id="Seg_4469" s="T186">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T188" id="Seg_4470" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_4471" s="T188">adj-n:case</ta>
            <ta e="T190" id="Seg_4472" s="T189">v-v:cvb</ta>
            <ta e="T191" id="Seg_4473" s="T190">v-v:tense-v:poss.pn</ta>
            <ta e="T192" id="Seg_4474" s="T191">n-n:(poss)-n:case</ta>
            <ta e="T193" id="Seg_4475" s="T192">adj-n:(poss)-n:case</ta>
            <ta e="T194" id="Seg_4476" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_4477" s="T194">v-v:tense-v:poss.pn</ta>
            <ta e="T196" id="Seg_4478" s="T195">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T197" id="Seg_4479" s="T196">adj-n:case</ta>
            <ta e="T198" id="Seg_4480" s="T197">v-v:tense-v:poss.pn</ta>
            <ta e="T199" id="Seg_4481" s="T198">n-n:case</ta>
            <ta e="T200" id="Seg_4482" s="T199">v-v:mood.pn</ta>
            <ta e="T201" id="Seg_4483" s="T200">propr</ta>
            <ta e="T202" id="Seg_4484" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_4485" s="T202">n-n:case</ta>
            <ta e="T204" id="Seg_4486" s="T203">adj</ta>
            <ta e="T205" id="Seg_4487" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_4488" s="T205">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T207" id="Seg_4489" s="T206">n-n:poss-n:case</ta>
            <ta e="T208" id="Seg_4490" s="T207">v-v:tense-v:pred.pn</ta>
            <ta e="T209" id="Seg_4491" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_4492" s="T209">n-n:case</ta>
            <ta e="T211" id="Seg_4493" s="T210">n-n:poss-n:case</ta>
            <ta e="T212" id="Seg_4494" s="T211">v-v:tense-v:pred.pn</ta>
            <ta e="T213" id="Seg_4495" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_4496" s="T213">v-v:cvb-v:pred.pn</ta>
            <ta e="T215" id="Seg_4497" s="T214">n-n:poss-n:case</ta>
            <ta e="T216" id="Seg_4498" s="T215">v-v:(ins)-v&gt;v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T217" id="Seg_4499" s="T216">v-v:tense-v:poss.pn</ta>
            <ta e="T218" id="Seg_4500" s="T217">n-n:case</ta>
            <ta e="T219" id="Seg_4501" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_4502" s="T219">v-v:cvb</ta>
            <ta e="T221" id="Seg_4503" s="T220">v-v:mood.pn</ta>
            <ta e="T222" id="Seg_4504" s="T221">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T223" id="Seg_4505" s="T222">n-n:case</ta>
            <ta e="T224" id="Seg_4506" s="T223">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T225" id="Seg_4507" s="T224">v-v:cvb</ta>
            <ta e="T226" id="Seg_4508" s="T225">v-v:mood.pn</ta>
            <ta e="T227" id="Seg_4509" s="T226">n-n:case</ta>
            <ta e="T228" id="Seg_4510" s="T227">adv</ta>
            <ta e="T229" id="Seg_4511" s="T228">adv</ta>
            <ta e="T230" id="Seg_4512" s="T229">v-v:mood.pn</ta>
            <ta e="T231" id="Seg_4513" s="T230">n-n:case</ta>
            <ta e="T232" id="Seg_4514" s="T231">propr</ta>
            <ta e="T233" id="Seg_4515" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_4516" s="T233">v-v:tense-v:pred.pn</ta>
            <ta e="T235" id="Seg_4517" s="T234">n-n:poss-n:case</ta>
            <ta e="T236" id="Seg_4518" s="T235">v-v:cvb</ta>
            <ta e="T237" id="Seg_4519" s="T236">n-n:poss-n:case</ta>
            <ta e="T238" id="Seg_4520" s="T237">v-v:cvb</ta>
            <ta e="T239" id="Seg_4521" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_4522" s="T239">adj-n:case</ta>
            <ta e="T241" id="Seg_4523" s="T240">v-v:tense-v:pred.pn</ta>
            <ta e="T242" id="Seg_4524" s="T241">n-n:poss-n:case</ta>
            <ta e="T243" id="Seg_4525" s="T242">v-v:tense-v:pred.pn</ta>
            <ta e="T244" id="Seg_4526" s="T243">n-n:case</ta>
            <ta e="T245" id="Seg_4527" s="T244">n-n:case</ta>
            <ta e="T246" id="Seg_4528" s="T245">v-v:tense-v:pred.pn</ta>
            <ta e="T247" id="Seg_4529" s="T246">n-n:case</ta>
            <ta e="T248" id="Seg_4530" s="T247">n-n:poss-n:case</ta>
            <ta e="T249" id="Seg_4531" s="T248">v-v:cvb</ta>
            <ta e="T250" id="Seg_4532" s="T249">v-v:(tense)-v:mood.pn</ta>
            <ta e="T251" id="Seg_4533" s="T250">adj</ta>
            <ta e="T252" id="Seg_4534" s="T251">n-n:case</ta>
            <ta e="T253" id="Seg_4535" s="T252">adj</ta>
            <ta e="T254" id="Seg_4536" s="T253">n-n:poss-n:case</ta>
            <ta e="T255" id="Seg_4537" s="T254">v-v:cvb</ta>
            <ta e="T256" id="Seg_4538" s="T255">post</ta>
            <ta e="T257" id="Seg_4539" s="T256">v-v:(tense)-v:mood.pn</ta>
            <ta e="T258" id="Seg_4540" s="T257">conj</ta>
            <ta e="T259" id="Seg_4541" s="T258">pers-n:case</ta>
            <ta e="T260" id="Seg_4542" s="T259">v-v&gt;v-v:(tense)-v:mood.pn</ta>
            <ta e="T261" id="Seg_4543" s="T260">n-n:(ins)-n:(poss)-n:case-n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T262" id="Seg_4544" s="T261">que</ta>
            <ta e="T263" id="Seg_4545" s="T262">adv</ta>
            <ta e="T264" id="Seg_4546" s="T263">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T265" id="Seg_4547" s="T264">v-v:cvb</ta>
            <ta e="T266" id="Seg_4548" s="T265">v-v:mood.pn</ta>
            <ta e="T267" id="Seg_4549" s="T266">v-v:(tense)-v:mood.pn</ta>
            <ta e="T268" id="Seg_4550" s="T267">pers-pro:case</ta>
            <ta e="T269" id="Seg_4551" s="T268">n-n:case</ta>
            <ta e="T270" id="Seg_4552" s="T269">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T271" id="Seg_4553" s="T270">post</ta>
            <ta e="T272" id="Seg_4554" s="T271">n-n:case</ta>
            <ta e="T273" id="Seg_4555" s="T272">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T274" id="Seg_4556" s="T273">n-n:poss-n:case</ta>
            <ta e="T275" id="Seg_4557" s="T274">v-v:tense-v:pred.pn</ta>
            <ta e="T276" id="Seg_4558" s="T275">adj</ta>
            <ta e="T277" id="Seg_4559" s="T276">n-n:case</ta>
            <ta e="T278" id="Seg_4560" s="T277">adj</ta>
            <ta e="T279" id="Seg_4561" s="T278">n-n:poss-n:case</ta>
            <ta e="T280" id="Seg_4562" s="T279">v-v:tense-v:pred.pn</ta>
            <ta e="T281" id="Seg_4563" s="T280">v-v:cvb</ta>
            <ta e="T282" id="Seg_4564" s="T281">v-v:cvb</ta>
            <ta e="T283" id="Seg_4565" s="T282">v-v:tense-v:pred.pn</ta>
            <ta e="T284" id="Seg_4566" s="T283">n-n:(ins)-n:(poss)-n:case-n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T285" id="Seg_4567" s="T284">que</ta>
            <ta e="T286" id="Seg_4568" s="T285">adv</ta>
            <ta e="T287" id="Seg_4569" s="T286">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T288" id="Seg_4570" s="T287">v-v:tense-v:poss.pn</ta>
            <ta e="T289" id="Seg_4571" s="T288">v-v:tense-v:poss.pn</ta>
            <ta e="T290" id="Seg_4572" s="T289">dempro</ta>
            <ta e="T291" id="Seg_4573" s="T290">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T292" id="Seg_4574" s="T291">que</ta>
            <ta e="T293" id="Seg_4575" s="T292">v-v:tense-v:poss.pn</ta>
            <ta e="T294" id="Seg_4576" s="T293">v-v:cvb</ta>
            <ta e="T295" id="Seg_4577" s="T294">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T296" id="Seg_4578" s="T295">v-v:tense-v:pred.pn</ta>
            <ta e="T297" id="Seg_4579" s="T296">n-n:case</ta>
            <ta e="T298" id="Seg_4580" s="T297">n-n:case</ta>
            <ta e="T299" id="Seg_4581" s="T298">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T300" id="Seg_4582" s="T299">v-v:tense-v:pred.pn</ta>
            <ta e="T301" id="Seg_4583" s="T300">n-n:case</ta>
            <ta e="T302" id="Seg_4584" s="T301">n-n:case</ta>
            <ta e="T303" id="Seg_4585" s="T302">v-v:cvb</ta>
            <ta e="T304" id="Seg_4586" s="T303">adj</ta>
            <ta e="T305" id="Seg_4587" s="T304">n-n:case</ta>
            <ta e="T306" id="Seg_4588" s="T305">n-n:poss-n:case</ta>
            <ta e="T307" id="Seg_4589" s="T306">v-v:tense-v:pred.pn</ta>
            <ta e="T308" id="Seg_4590" s="T307">propr</ta>
            <ta e="T309" id="Seg_4591" s="T308">n-n:case</ta>
            <ta e="T310" id="Seg_4592" s="T309">pers-pro:case</ta>
            <ta e="T311" id="Seg_4593" s="T310">v-v:cvb</ta>
            <ta e="T312" id="Seg_4594" s="T311">v-v:tense-v:poss.pn</ta>
            <ta e="T313" id="Seg_4595" s="T312">v-v:cvb</ta>
            <ta e="T314" id="Seg_4596" s="T313">v-v:(ins)-v:mood.pn</ta>
            <ta e="T315" id="Seg_4597" s="T314">dempro</ta>
            <ta e="T316" id="Seg_4598" s="T315">n-n:case</ta>
            <ta e="T317" id="Seg_4599" s="T316">v-v:cvb</ta>
            <ta e="T318" id="Seg_4600" s="T317">v-v:(ins)-v:mood.pn</ta>
            <ta e="T319" id="Seg_4601" s="T318">n-n:(poss)-n:case</ta>
            <ta e="T320" id="Seg_4602" s="T319">ptcl</ta>
            <ta e="T321" id="Seg_4603" s="T320">v-v:ptcp-v:(case)</ta>
            <ta e="T322" id="Seg_4604" s="T321">post</ta>
            <ta e="T323" id="Seg_4605" s="T322">adj</ta>
            <ta e="T324" id="Seg_4606" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_4607" s="T324">n-n:poss-n:case</ta>
            <ta e="T326" id="Seg_4608" s="T325">n-n:(poss)-n:case</ta>
            <ta e="T327" id="Seg_4609" s="T326">adj</ta>
            <ta e="T328" id="Seg_4610" s="T327">n-n:case</ta>
            <ta e="T329" id="Seg_4611" s="T328">v-v:tense-v:pred.pn</ta>
            <ta e="T330" id="Seg_4612" s="T329">propr</ta>
            <ta e="T331" id="Seg_4613" s="T330">n-n:case</ta>
            <ta e="T332" id="Seg_4614" s="T331">v-v:cvb</ta>
            <ta e="T333" id="Seg_4615" s="T332">n-n:case</ta>
            <ta e="T334" id="Seg_4616" s="T333">n-n:case</ta>
            <ta e="T335" id="Seg_4617" s="T334">n-n:case</ta>
            <ta e="T336" id="Seg_4618" s="T335">v-v:cvb</ta>
            <ta e="T337" id="Seg_4619" s="T336">v-v:tense-v:pred.pn</ta>
            <ta e="T338" id="Seg_4620" s="T337">n-n:poss-n:case</ta>
            <ta e="T339" id="Seg_4621" s="T338">n-n:case</ta>
            <ta e="T340" id="Seg_4622" s="T339">n-n:(poss)-n:case</ta>
            <ta e="T341" id="Seg_4623" s="T340">v-v:tense-v:pred.pn</ta>
            <ta e="T342" id="Seg_4624" s="T341">v-v:cvb-v-v:cvb</ta>
            <ta e="T343" id="Seg_4625" s="T342">post</ta>
            <ta e="T344" id="Seg_4626" s="T343">dempro</ta>
            <ta e="T345" id="Seg_4627" s="T344">n-n:case</ta>
            <ta e="T346" id="Seg_4628" s="T345">v-v:cvb</ta>
            <ta e="T347" id="Seg_4629" s="T346">v-v:tense-v:pred.pn</ta>
            <ta e="T348" id="Seg_4630" s="T347">n-n:poss-n:case</ta>
            <ta e="T349" id="Seg_4631" s="T348">v-v:(ins)-v&gt;v-v:mood.pn</ta>
            <ta e="T350" id="Seg_4632" s="T349">n-n&gt;v-v:mood.pn</ta>
            <ta e="T351" id="Seg_4633" s="T350">n-n:case</ta>
            <ta e="T352" id="Seg_4634" s="T351">n-n:(ins)-n:poss-n:case</ta>
            <ta e="T353" id="Seg_4635" s="T352">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T354" id="Seg_4636" s="T353">n-n&gt;v-v:mood.pn</ta>
            <ta e="T355" id="Seg_4637" s="T354">v-v:cvb</ta>
            <ta e="T356" id="Seg_4638" s="T355">propr</ta>
            <ta e="T357" id="Seg_4639" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_4640" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_4641" s="T358">n-n&gt;v-v:cvb</ta>
            <ta e="T360" id="Seg_4642" s="T359">v-v:tense-v:pred.pn</ta>
            <ta e="T361" id="Seg_4643" s="T360">dempro</ta>
            <ta e="T362" id="Seg_4644" s="T361">n-n:case</ta>
            <ta e="T363" id="Seg_4645" s="T362">adj</ta>
            <ta e="T364" id="Seg_4646" s="T363">n-n:case</ta>
            <ta e="T365" id="Seg_4647" s="T364">cardnum</ta>
            <ta e="T366" id="Seg_4648" s="T365">ptcl</ta>
            <ta e="T367" id="Seg_4649" s="T366">n-n:(poss)-n:case</ta>
            <ta e="T368" id="Seg_4650" s="T367">v-v:(neg)-v:pred.pn</ta>
            <ta e="T369" id="Seg_4651" s="T368">n-n:case</ta>
            <ta e="T370" id="Seg_4652" s="T369">dempro</ta>
            <ta e="T371" id="Seg_4653" s="T370">adj</ta>
            <ta e="T372" id="Seg_4654" s="T371">n-n:case</ta>
            <ta e="T373" id="Seg_4655" s="T372">v-v:cvb</ta>
            <ta e="T374" id="Seg_4656" s="T373">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T375" id="Seg_4657" s="T374">n-n:case</ta>
            <ta e="T376" id="Seg_4658" s="T375">v-v:tense-v:pred.pn</ta>
            <ta e="T377" id="Seg_4659" s="T376">propr</ta>
            <ta e="T378" id="Seg_4660" s="T377">n-n:case</ta>
            <ta e="T379" id="Seg_4661" s="T378">n-n:poss-n:case</ta>
            <ta e="T380" id="Seg_4662" s="T379">v-v:mood-v:poss.pn</ta>
            <ta e="T381" id="Seg_4663" s="T380">dempro-pro:case</ta>
            <ta e="T382" id="Seg_4664" s="T381">adj-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_4665" s="T0">adj</ta>
            <ta e="T2" id="Seg_4666" s="T1">n</ta>
            <ta e="T3" id="Seg_4667" s="T2">v</ta>
            <ta e="T4" id="Seg_4668" s="T3">dempro</ta>
            <ta e="T5" id="Seg_4669" s="T4">n</ta>
            <ta e="T6" id="Seg_4670" s="T5">v</ta>
            <ta e="T7" id="Seg_4671" s="T6">n</ta>
            <ta e="T8" id="Seg_4672" s="T7">n</ta>
            <ta e="T9" id="Seg_4673" s="T8">n</ta>
            <ta e="T10" id="Seg_4674" s="T9">n</ta>
            <ta e="T11" id="Seg_4675" s="T10">n</ta>
            <ta e="T12" id="Seg_4676" s="T11">v</ta>
            <ta e="T13" id="Seg_4677" s="T12">dempro</ta>
            <ta e="T14" id="Seg_4678" s="T13">n</ta>
            <ta e="T15" id="Seg_4679" s="T14">v</ta>
            <ta e="T16" id="Seg_4680" s="T15">v</ta>
            <ta e="T17" id="Seg_4681" s="T16">aux</ta>
            <ta e="T18" id="Seg_4682" s="T17">dempro</ta>
            <ta e="T19" id="Seg_4683" s="T18">n</ta>
            <ta e="T20" id="Seg_4684" s="T19">v</ta>
            <ta e="T21" id="Seg_4685" s="T20">pers</ta>
            <ta e="T22" id="Seg_4686" s="T21">pers</ta>
            <ta e="T23" id="Seg_4687" s="T22">n</ta>
            <ta e="T24" id="Seg_4688" s="T23">v</ta>
            <ta e="T25" id="Seg_4689" s="T24">v</ta>
            <ta e="T26" id="Seg_4690" s="T25">adj</ta>
            <ta e="T27" id="Seg_4691" s="T26">n</ta>
            <ta e="T28" id="Seg_4692" s="T27">cop</ta>
            <ta e="T29" id="Seg_4693" s="T28">adj</ta>
            <ta e="T30" id="Seg_4694" s="T29">n</ta>
            <ta e="T31" id="Seg_4695" s="T30">n</ta>
            <ta e="T32" id="Seg_4696" s="T31">v</ta>
            <ta e="T33" id="Seg_4697" s="T32">aux</ta>
            <ta e="T34" id="Seg_4698" s="T33">adv</ta>
            <ta e="T35" id="Seg_4699" s="T34">v</ta>
            <ta e="T36" id="Seg_4700" s="T35">post</ta>
            <ta e="T37" id="Seg_4701" s="T36">n</ta>
            <ta e="T38" id="Seg_4702" s="T37">propr</ta>
            <ta e="T39" id="Seg_4703" s="T38">n</ta>
            <ta e="T40" id="Seg_4704" s="T39">v</ta>
            <ta e="T41" id="Seg_4705" s="T40">propr</ta>
            <ta e="T42" id="Seg_4706" s="T41">n</ta>
            <ta e="T43" id="Seg_4707" s="T42">v</ta>
            <ta e="T44" id="Seg_4708" s="T43">v</ta>
            <ta e="T45" id="Seg_4709" s="T44">adj</ta>
            <ta e="T46" id="Seg_4710" s="T45">adj</ta>
            <ta e="T47" id="Seg_4711" s="T46">n</ta>
            <ta e="T48" id="Seg_4712" s="T47">n</ta>
            <ta e="T49" id="Seg_4713" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_4714" s="T49">pers</ta>
            <ta e="T51" id="Seg_4715" s="T50">pers</ta>
            <ta e="T52" id="Seg_4716" s="T51">n</ta>
            <ta e="T53" id="Seg_4717" s="T52">v</ta>
            <ta e="T54" id="Seg_4718" s="T53">v</ta>
            <ta e="T55" id="Seg_4719" s="T54">n</ta>
            <ta e="T56" id="Seg_4720" s="T55">n</ta>
            <ta e="T57" id="Seg_4721" s="T56">n</ta>
            <ta e="T58" id="Seg_4722" s="T57">n</ta>
            <ta e="T59" id="Seg_4723" s="T58">n</ta>
            <ta e="T60" id="Seg_4724" s="T59">n</ta>
            <ta e="T61" id="Seg_4725" s="T60">v</ta>
            <ta e="T62" id="Seg_4726" s="T61">aux</ta>
            <ta e="T63" id="Seg_4727" s="T62">dempro</ta>
            <ta e="T64" id="Seg_4728" s="T63">n</ta>
            <ta e="T65" id="Seg_4729" s="T64">n</ta>
            <ta e="T66" id="Seg_4730" s="T65">n</ta>
            <ta e="T67" id="Seg_4731" s="T66">n</ta>
            <ta e="T68" id="Seg_4732" s="T67">n</ta>
            <ta e="T69" id="Seg_4733" s="T68">v</ta>
            <ta e="T70" id="Seg_4734" s="T69">v</ta>
            <ta e="T71" id="Seg_4735" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_4736" s="T71">v</ta>
            <ta e="T73" id="Seg_4737" s="T72">propr</ta>
            <ta e="T74" id="Seg_4738" s="T73">n</ta>
            <ta e="T75" id="Seg_4739" s="T74">v</ta>
            <ta e="T76" id="Seg_4740" s="T75">n</ta>
            <ta e="T77" id="Seg_4741" s="T76">n</ta>
            <ta e="T78" id="Seg_4742" s="T77">adv</ta>
            <ta e="T79" id="Seg_4743" s="T78">v</ta>
            <ta e="T80" id="Seg_4744" s="T79">n</ta>
            <ta e="T81" id="Seg_4745" s="T80">n</ta>
            <ta e="T82" id="Seg_4746" s="T81">adj</ta>
            <ta e="T83" id="Seg_4747" s="T82">v</ta>
            <ta e="T84" id="Seg_4748" s="T83">v</ta>
            <ta e="T85" id="Seg_4749" s="T84">propr</ta>
            <ta e="T86" id="Seg_4750" s="T85">n</ta>
            <ta e="T87" id="Seg_4751" s="T86">n</ta>
            <ta e="T88" id="Seg_4752" s="T87">v</ta>
            <ta e="T89" id="Seg_4753" s="T88">pers</ta>
            <ta e="T90" id="Seg_4754" s="T89">n</ta>
            <ta e="T91" id="Seg_4755" s="T90">v</ta>
            <ta e="T92" id="Seg_4756" s="T91">dempro</ta>
            <ta e="T93" id="Seg_4757" s="T92">post</ta>
            <ta e="T94" id="Seg_4758" s="T93">v</ta>
            <ta e="T95" id="Seg_4759" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_4760" s="T95">n</ta>
            <ta e="T97" id="Seg_4761" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_4762" s="T97">n</ta>
            <ta e="T99" id="Seg_4763" s="T98">cop</ta>
            <ta e="T100" id="Seg_4764" s="T99">v</ta>
            <ta e="T101" id="Seg_4765" s="T100">v</ta>
            <ta e="T102" id="Seg_4766" s="T101">propr</ta>
            <ta e="T103" id="Seg_4767" s="T102">n</ta>
            <ta e="T104" id="Seg_4768" s="T103">v</ta>
            <ta e="T105" id="Seg_4769" s="T104">n</ta>
            <ta e="T106" id="Seg_4770" s="T105">adj</ta>
            <ta e="T107" id="Seg_4771" s="T106">n</ta>
            <ta e="T108" id="Seg_4772" s="T107">cop</ta>
            <ta e="T109" id="Seg_4773" s="T108">v</ta>
            <ta e="T110" id="Seg_4774" s="T109">n</ta>
            <ta e="T111" id="Seg_4775" s="T110">v</ta>
            <ta e="T112" id="Seg_4776" s="T111">aux</ta>
            <ta e="T113" id="Seg_4777" s="T112">dempro</ta>
            <ta e="T114" id="Seg_4778" s="T113">n</ta>
            <ta e="T115" id="Seg_4779" s="T114">v</ta>
            <ta e="T116" id="Seg_4780" s="T115">aux</ta>
            <ta e="T117" id="Seg_4781" s="T116">n</ta>
            <ta e="T118" id="Seg_4782" s="T117">n</ta>
            <ta e="T119" id="Seg_4783" s="T118">propr</ta>
            <ta e="T120" id="Seg_4784" s="T119">n</ta>
            <ta e="T121" id="Seg_4785" s="T120">v</ta>
            <ta e="T122" id="Seg_4786" s="T121">interj</ta>
            <ta e="T123" id="Seg_4787" s="T122">n</ta>
            <ta e="T124" id="Seg_4788" s="T123">v</ta>
            <ta e="T125" id="Seg_4789" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_4790" s="T125">v</ta>
            <ta e="T127" id="Seg_4791" s="T126">n</ta>
            <ta e="T128" id="Seg_4792" s="T127">n</ta>
            <ta e="T129" id="Seg_4793" s="T128">n</ta>
            <ta e="T130" id="Seg_4794" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_4795" s="T130">adv</ta>
            <ta e="T132" id="Seg_4796" s="T131">adj</ta>
            <ta e="T133" id="Seg_4797" s="T132">n</ta>
            <ta e="T134" id="Seg_4798" s="T133">v</ta>
            <ta e="T135" id="Seg_4799" s="T134">v</ta>
            <ta e="T136" id="Seg_4800" s="T135">n</ta>
            <ta e="T137" id="Seg_4801" s="T136">adv</ta>
            <ta e="T138" id="Seg_4802" s="T137">v</ta>
            <ta e="T139" id="Seg_4803" s="T138">v</ta>
            <ta e="T140" id="Seg_4804" s="T139">n</ta>
            <ta e="T141" id="Seg_4805" s="T140">adj</ta>
            <ta e="T142" id="Seg_4806" s="T141">n</ta>
            <ta e="T143" id="Seg_4807" s="T142">v</ta>
            <ta e="T144" id="Seg_4808" s="T143">adj</ta>
            <ta e="T145" id="Seg_4809" s="T144">n</ta>
            <ta e="T146" id="Seg_4810" s="T145">n</ta>
            <ta e="T147" id="Seg_4811" s="T146">v</ta>
            <ta e="T148" id="Seg_4812" s="T147">adj</ta>
            <ta e="T149" id="Seg_4813" s="T148">adj</ta>
            <ta e="T150" id="Seg_4814" s="T149">adj</ta>
            <ta e="T151" id="Seg_4815" s="T150">cop</ta>
            <ta e="T152" id="Seg_4816" s="T151">v</ta>
            <ta e="T153" id="Seg_4817" s="T152">n</ta>
            <ta e="T154" id="Seg_4818" s="T153">n</ta>
            <ta e="T155" id="Seg_4819" s="T154">propr</ta>
            <ta e="T156" id="Seg_4820" s="T155">n</ta>
            <ta e="T157" id="Seg_4821" s="T156">v</ta>
            <ta e="T158" id="Seg_4822" s="T157">propr</ta>
            <ta e="T159" id="Seg_4823" s="T158">n</ta>
            <ta e="T160" id="Seg_4824" s="T159">v</ta>
            <ta e="T161" id="Seg_4825" s="T160">n</ta>
            <ta e="T162" id="Seg_4826" s="T161">post</ta>
            <ta e="T163" id="Seg_4827" s="T162">adj</ta>
            <ta e="T164" id="Seg_4828" s="T163">n</ta>
            <ta e="T165" id="Seg_4829" s="T164">v</ta>
            <ta e="T166" id="Seg_4830" s="T165">adj</ta>
            <ta e="T167" id="Seg_4831" s="T166">adj</ta>
            <ta e="T168" id="Seg_4832" s="T167">n</ta>
            <ta e="T169" id="Seg_4833" s="T168">v</ta>
            <ta e="T170" id="Seg_4834" s="T169">n</ta>
            <ta e="T171" id="Seg_4835" s="T170">v</ta>
            <ta e="T172" id="Seg_4836" s="T171">n</ta>
            <ta e="T173" id="Seg_4837" s="T172">v</ta>
            <ta e="T174" id="Seg_4838" s="T173">aux</ta>
            <ta e="T175" id="Seg_4839" s="T174">n</ta>
            <ta e="T176" id="Seg_4840" s="T175">n</ta>
            <ta e="T177" id="Seg_4841" s="T176">propr</ta>
            <ta e="T178" id="Seg_4842" s="T177">n</ta>
            <ta e="T179" id="Seg_4843" s="T178">v</ta>
            <ta e="T180" id="Seg_4844" s="T179">v</ta>
            <ta e="T181" id="Seg_4845" s="T180">v</ta>
            <ta e="T182" id="Seg_4846" s="T181">n</ta>
            <ta e="T183" id="Seg_4847" s="T182">cop</ta>
            <ta e="T184" id="Seg_4848" s="T183">adj</ta>
            <ta e="T185" id="Seg_4849" s="T184">n</ta>
            <ta e="T186" id="Seg_4850" s="T185">v</ta>
            <ta e="T187" id="Seg_4851" s="T186">n</ta>
            <ta e="T188" id="Seg_4852" s="T187">n</ta>
            <ta e="T189" id="Seg_4853" s="T188">adj</ta>
            <ta e="T190" id="Seg_4854" s="T189">cop</ta>
            <ta e="T191" id="Seg_4855" s="T190">v</ta>
            <ta e="T192" id="Seg_4856" s="T191">n</ta>
            <ta e="T193" id="Seg_4857" s="T192">adj</ta>
            <ta e="T194" id="Seg_4858" s="T193">n</ta>
            <ta e="T195" id="Seg_4859" s="T194">v</ta>
            <ta e="T196" id="Seg_4860" s="T195">emphpro</ta>
            <ta e="T197" id="Seg_4861" s="T196">adj</ta>
            <ta e="T198" id="Seg_4862" s="T197">v</ta>
            <ta e="T199" id="Seg_4863" s="T198">n</ta>
            <ta e="T200" id="Seg_4864" s="T199">v</ta>
            <ta e="T201" id="Seg_4865" s="T200">propr</ta>
            <ta e="T202" id="Seg_4866" s="T201">n</ta>
            <ta e="T203" id="Seg_4867" s="T202">n</ta>
            <ta e="T204" id="Seg_4868" s="T203">adj</ta>
            <ta e="T205" id="Seg_4869" s="T204">n</ta>
            <ta e="T206" id="Seg_4870" s="T205">emphpro</ta>
            <ta e="T207" id="Seg_4871" s="T206">n</ta>
            <ta e="T208" id="Seg_4872" s="T207">v</ta>
            <ta e="T209" id="Seg_4873" s="T208">n</ta>
            <ta e="T210" id="Seg_4874" s="T209">n</ta>
            <ta e="T211" id="Seg_4875" s="T210">n</ta>
            <ta e="T212" id="Seg_4876" s="T211">v</ta>
            <ta e="T213" id="Seg_4877" s="T212">n</ta>
            <ta e="T214" id="Seg_4878" s="T213">v</ta>
            <ta e="T215" id="Seg_4879" s="T214">n</ta>
            <ta e="T216" id="Seg_4880" s="T215">v</ta>
            <ta e="T217" id="Seg_4881" s="T216">v</ta>
            <ta e="T218" id="Seg_4882" s="T217">n</ta>
            <ta e="T219" id="Seg_4883" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_4884" s="T219">v</ta>
            <ta e="T221" id="Seg_4885" s="T220">v</ta>
            <ta e="T222" id="Seg_4886" s="T221">v</ta>
            <ta e="T223" id="Seg_4887" s="T222">n</ta>
            <ta e="T224" id="Seg_4888" s="T223">v</ta>
            <ta e="T225" id="Seg_4889" s="T224">v</ta>
            <ta e="T226" id="Seg_4890" s="T225">v</ta>
            <ta e="T227" id="Seg_4891" s="T226">n</ta>
            <ta e="T228" id="Seg_4892" s="T227">adv</ta>
            <ta e="T229" id="Seg_4893" s="T228">adv</ta>
            <ta e="T230" id="Seg_4894" s="T229">v</ta>
            <ta e="T231" id="Seg_4895" s="T230">n</ta>
            <ta e="T232" id="Seg_4896" s="T231">propr</ta>
            <ta e="T233" id="Seg_4897" s="T232">n</ta>
            <ta e="T234" id="Seg_4898" s="T233">v</ta>
            <ta e="T235" id="Seg_4899" s="T234">n</ta>
            <ta e="T236" id="Seg_4900" s="T235">v</ta>
            <ta e="T237" id="Seg_4901" s="T236">n</ta>
            <ta e="T238" id="Seg_4902" s="T237">v</ta>
            <ta e="T239" id="Seg_4903" s="T238">n</ta>
            <ta e="T240" id="Seg_4904" s="T239">adj</ta>
            <ta e="T241" id="Seg_4905" s="T240">cop</ta>
            <ta e="T242" id="Seg_4906" s="T241">n</ta>
            <ta e="T243" id="Seg_4907" s="T242">v</ta>
            <ta e="T244" id="Seg_4908" s="T243">n</ta>
            <ta e="T245" id="Seg_4909" s="T244">n</ta>
            <ta e="T246" id="Seg_4910" s="T245">v</ta>
            <ta e="T247" id="Seg_4911" s="T246">n</ta>
            <ta e="T248" id="Seg_4912" s="T247">n</ta>
            <ta e="T249" id="Seg_4913" s="T248">v</ta>
            <ta e="T250" id="Seg_4914" s="T249">v</ta>
            <ta e="T251" id="Seg_4915" s="T250">adj</ta>
            <ta e="T252" id="Seg_4916" s="T251">n</ta>
            <ta e="T253" id="Seg_4917" s="T252">adj</ta>
            <ta e="T254" id="Seg_4918" s="T253">n</ta>
            <ta e="T255" id="Seg_4919" s="T254">v</ta>
            <ta e="T256" id="Seg_4920" s="T255">post</ta>
            <ta e="T257" id="Seg_4921" s="T256">v</ta>
            <ta e="T258" id="Seg_4922" s="T257">conj</ta>
            <ta e="T259" id="Seg_4923" s="T258">pers</ta>
            <ta e="T260" id="Seg_4924" s="T259">v</ta>
            <ta e="T261" id="Seg_4925" s="T260">n</ta>
            <ta e="T262" id="Seg_4926" s="T261">que</ta>
            <ta e="T263" id="Seg_4927" s="T262">adv</ta>
            <ta e="T264" id="Seg_4928" s="T263">v</ta>
            <ta e="T265" id="Seg_4929" s="T264">v</ta>
            <ta e="T266" id="Seg_4930" s="T265">v</ta>
            <ta e="T267" id="Seg_4931" s="T266">v</ta>
            <ta e="T268" id="Seg_4932" s="T267">pers</ta>
            <ta e="T269" id="Seg_4933" s="T268">n</ta>
            <ta e="T270" id="Seg_4934" s="T269">v</ta>
            <ta e="T271" id="Seg_4935" s="T270">post</ta>
            <ta e="T272" id="Seg_4936" s="T271">n</ta>
            <ta e="T273" id="Seg_4937" s="T272">emphpro</ta>
            <ta e="T274" id="Seg_4938" s="T273">n</ta>
            <ta e="T275" id="Seg_4939" s="T274">v</ta>
            <ta e="T276" id="Seg_4940" s="T275">adj</ta>
            <ta e="T277" id="Seg_4941" s="T276">n</ta>
            <ta e="T278" id="Seg_4942" s="T277">adj</ta>
            <ta e="T279" id="Seg_4943" s="T278">n</ta>
            <ta e="T280" id="Seg_4944" s="T279">v</ta>
            <ta e="T281" id="Seg_4945" s="T280">v</ta>
            <ta e="T282" id="Seg_4946" s="T281">v</ta>
            <ta e="T283" id="Seg_4947" s="T282">v</ta>
            <ta e="T284" id="Seg_4948" s="T283">n</ta>
            <ta e="T285" id="Seg_4949" s="T284">que</ta>
            <ta e="T286" id="Seg_4950" s="T285">adv</ta>
            <ta e="T287" id="Seg_4951" s="T286">v</ta>
            <ta e="T288" id="Seg_4952" s="T287">v</ta>
            <ta e="T289" id="Seg_4953" s="T288">aux</ta>
            <ta e="T290" id="Seg_4954" s="T289">dempro</ta>
            <ta e="T291" id="Seg_4955" s="T290">n</ta>
            <ta e="T292" id="Seg_4956" s="T291">que</ta>
            <ta e="T293" id="Seg_4957" s="T292">v</ta>
            <ta e="T294" id="Seg_4958" s="T293">v</ta>
            <ta e="T295" id="Seg_4959" s="T294">v</ta>
            <ta e="T296" id="Seg_4960" s="T295">v</ta>
            <ta e="T297" id="Seg_4961" s="T296">n</ta>
            <ta e="T298" id="Seg_4962" s="T297">n</ta>
            <ta e="T299" id="Seg_4963" s="T298">v</ta>
            <ta e="T300" id="Seg_4964" s="T299">v</ta>
            <ta e="T301" id="Seg_4965" s="T300">n</ta>
            <ta e="T302" id="Seg_4966" s="T301">n</ta>
            <ta e="T303" id="Seg_4967" s="T302">v</ta>
            <ta e="T304" id="Seg_4968" s="T303">adj</ta>
            <ta e="T305" id="Seg_4969" s="T304">n</ta>
            <ta e="T306" id="Seg_4970" s="T305">n</ta>
            <ta e="T307" id="Seg_4971" s="T306">v</ta>
            <ta e="T308" id="Seg_4972" s="T307">propr</ta>
            <ta e="T309" id="Seg_4973" s="T308">n</ta>
            <ta e="T310" id="Seg_4974" s="T309">pers</ta>
            <ta e="T311" id="Seg_4975" s="T310">v</ta>
            <ta e="T312" id="Seg_4976" s="T311">v</ta>
            <ta e="T313" id="Seg_4977" s="T312">v</ta>
            <ta e="T314" id="Seg_4978" s="T313">aux</ta>
            <ta e="T315" id="Seg_4979" s="T314">dempro</ta>
            <ta e="T316" id="Seg_4980" s="T315">n</ta>
            <ta e="T317" id="Seg_4981" s="T316">v</ta>
            <ta e="T318" id="Seg_4982" s="T317">v</ta>
            <ta e="T319" id="Seg_4983" s="T318">n</ta>
            <ta e="T320" id="Seg_4984" s="T319">ptcl</ta>
            <ta e="T321" id="Seg_4985" s="T320">v</ta>
            <ta e="T322" id="Seg_4986" s="T321">post</ta>
            <ta e="T323" id="Seg_4987" s="T322">adj</ta>
            <ta e="T324" id="Seg_4988" s="T323">n</ta>
            <ta e="T325" id="Seg_4989" s="T324">n</ta>
            <ta e="T326" id="Seg_4990" s="T325">n</ta>
            <ta e="T327" id="Seg_4991" s="T326">adj</ta>
            <ta e="T328" id="Seg_4992" s="T327">n</ta>
            <ta e="T329" id="Seg_4993" s="T328">v</ta>
            <ta e="T330" id="Seg_4994" s="T329">propr</ta>
            <ta e="T331" id="Seg_4995" s="T330">n</ta>
            <ta e="T332" id="Seg_4996" s="T331">v</ta>
            <ta e="T333" id="Seg_4997" s="T332">n</ta>
            <ta e="T334" id="Seg_4998" s="T333">n</ta>
            <ta e="T335" id="Seg_4999" s="T334">n</ta>
            <ta e="T336" id="Seg_5000" s="T335">v</ta>
            <ta e="T337" id="Seg_5001" s="T336">v</ta>
            <ta e="T338" id="Seg_5002" s="T337">n</ta>
            <ta e="T339" id="Seg_5003" s="T338">n</ta>
            <ta e="T340" id="Seg_5004" s="T339">n</ta>
            <ta e="T341" id="Seg_5005" s="T340">v</ta>
            <ta e="T342" id="Seg_5006" s="T341">v</ta>
            <ta e="T343" id="Seg_5007" s="T342">post</ta>
            <ta e="T344" id="Seg_5008" s="T343">dempro</ta>
            <ta e="T345" id="Seg_5009" s="T344">n</ta>
            <ta e="T346" id="Seg_5010" s="T345">v</ta>
            <ta e="T347" id="Seg_5011" s="T346">aux</ta>
            <ta e="T348" id="Seg_5012" s="T347">n</ta>
            <ta e="T349" id="Seg_5013" s="T348">v</ta>
            <ta e="T350" id="Seg_5014" s="T349">v</ta>
            <ta e="T351" id="Seg_5015" s="T350">n</ta>
            <ta e="T352" id="Seg_5016" s="T351">n</ta>
            <ta e="T353" id="Seg_5017" s="T352">v</ta>
            <ta e="T354" id="Seg_5018" s="T353">v</ta>
            <ta e="T355" id="Seg_5019" s="T354">v</ta>
            <ta e="T356" id="Seg_5020" s="T355">propr</ta>
            <ta e="T357" id="Seg_5021" s="T356">n</ta>
            <ta e="T358" id="Seg_5022" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_5023" s="T358">v</ta>
            <ta e="T360" id="Seg_5024" s="T359">v</ta>
            <ta e="T361" id="Seg_5025" s="T360">dempro</ta>
            <ta e="T362" id="Seg_5026" s="T361">n</ta>
            <ta e="T363" id="Seg_5027" s="T362">adj</ta>
            <ta e="T364" id="Seg_5028" s="T363">n</ta>
            <ta e="T365" id="Seg_5029" s="T364">cardnum</ta>
            <ta e="T366" id="Seg_5030" s="T365">ptcl</ta>
            <ta e="T367" id="Seg_5031" s="T366">n</ta>
            <ta e="T368" id="Seg_5032" s="T367">v</ta>
            <ta e="T369" id="Seg_5033" s="T368">n</ta>
            <ta e="T370" id="Seg_5034" s="T369">dempro</ta>
            <ta e="T371" id="Seg_5035" s="T370">adj</ta>
            <ta e="T372" id="Seg_5036" s="T371">n</ta>
            <ta e="T373" id="Seg_5037" s="T372">v</ta>
            <ta e="T374" id="Seg_5038" s="T373">v</ta>
            <ta e="T375" id="Seg_5039" s="T374">n</ta>
            <ta e="T376" id="Seg_5040" s="T375">cop</ta>
            <ta e="T377" id="Seg_5041" s="T376">propr</ta>
            <ta e="T378" id="Seg_5042" s="T377">n</ta>
            <ta e="T379" id="Seg_5043" s="T378">n</ta>
            <ta e="T380" id="Seg_5044" s="T379">v</ta>
            <ta e="T381" id="Seg_5045" s="T380">dempro</ta>
            <ta e="T382" id="Seg_5046" s="T381">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_5047" s="T1">np.h:Th</ta>
            <ta e="T5" id="Seg_5048" s="T4">np.h:P</ta>
            <ta e="T8" id="Seg_5049" s="T7">0.3.h:Poss np:L</ta>
            <ta e="T10" id="Seg_5050" s="T9">0.3.h:Poss np.h:P</ta>
            <ta e="T14" id="Seg_5051" s="T13">np.h:E</ta>
            <ta e="T18" id="Seg_5052" s="T17">pro.h:Th</ta>
            <ta e="T19" id="Seg_5053" s="T18">np.h:A</ta>
            <ta e="T21" id="Seg_5054" s="T20">pro.h:A</ta>
            <ta e="T22" id="Seg_5055" s="T21">pro.h:P</ta>
            <ta e="T25" id="Seg_5056" s="T24">0.3.h:A</ta>
            <ta e="T28" id="Seg_5057" s="T27">0.2.h:Th</ta>
            <ta e="T30" id="Seg_5058" s="T29">np.h:Th</ta>
            <ta e="T33" id="Seg_5059" s="T32">0.1.h:A</ta>
            <ta e="T37" id="Seg_5060" s="T36">np.h:A</ta>
            <ta e="T39" id="Seg_5061" s="T38">0.3.h:Poss np:G</ta>
            <ta e="T42" id="Seg_5062" s="T41">0.3.h:Poss np:G</ta>
            <ta e="T43" id="Seg_5063" s="T42">0.3.h:A</ta>
            <ta e="T44" id="Seg_5064" s="T43">0.3.h:A</ta>
            <ta e="T48" id="Seg_5065" s="T47">np.h:Th</ta>
            <ta e="T50" id="Seg_5066" s="T49">pro.h:A</ta>
            <ta e="T51" id="Seg_5067" s="T50">pro.h:Poss</ta>
            <ta e="T52" id="Seg_5068" s="T51">np.h:Th</ta>
            <ta e="T55" id="Seg_5069" s="T54">0.2.h:Poss np.h:Poss</ta>
            <ta e="T57" id="Seg_5070" s="T56">np:Poss</ta>
            <ta e="T58" id="Seg_5071" s="T57">np:Th</ta>
            <ta e="T62" id="Seg_5072" s="T60">0.3.h:A</ta>
            <ta e="T64" id="Seg_5073" s="T63">np:Th</ta>
            <ta e="T67" id="Seg_5074" s="T66">np:L</ta>
            <ta e="T68" id="Seg_5075" s="T67">np:P</ta>
            <ta e="T70" id="Seg_5076" s="T69">0.2.h:A</ta>
            <ta e="T72" id="Seg_5077" s="T71">0.3.h:A</ta>
            <ta e="T74" id="Seg_5078" s="T73">np.h:E</ta>
            <ta e="T76" id="Seg_5079" s="T75">np.h:A</ta>
            <ta e="T80" id="Seg_5080" s="T79">np:Poss</ta>
            <ta e="T81" id="Seg_5081" s="T80">np:Th</ta>
            <ta e="T84" id="Seg_5082" s="T83">0.3.h:A</ta>
            <ta e="T86" id="Seg_5083" s="T85">np.h:A</ta>
            <ta e="T87" id="Seg_5084" s="T86">np:Th</ta>
            <ta e="T89" id="Seg_5085" s="T88">pro.h:B</ta>
            <ta e="T90" id="Seg_5086" s="T89">np:P</ta>
            <ta e="T92" id="Seg_5087" s="T91">pro:P</ta>
            <ta e="T95" id="Seg_5088" s="T94">0.2.h:A</ta>
            <ta e="T96" id="Seg_5089" s="T95">np:A</ta>
            <ta e="T103" id="Seg_5090" s="T102">np.h:E</ta>
            <ta e="T105" id="Seg_5091" s="T104">0.1.h:Poss np.h:Th</ta>
            <ta e="T109" id="Seg_5092" s="T108">0.3.h:E</ta>
            <ta e="T110" id="Seg_5093" s="T109">np.h:A</ta>
            <ta e="T114" id="Seg_5094" s="T113">np:P</ta>
            <ta e="T116" id="Seg_5095" s="T114">0.3.h:A</ta>
            <ta e="T117" id="Seg_5096" s="T116">np.h:A</ta>
            <ta e="T120" id="Seg_5097" s="T119">np:G</ta>
            <ta e="T123" id="Seg_5098" s="T122">np.h:A</ta>
            <ta e="T126" id="Seg_5099" s="T125">0.3.h:A</ta>
            <ta e="T128" id="Seg_5100" s="T127">np.h:Poss</ta>
            <ta e="T129" id="Seg_5101" s="T128">np:Th</ta>
            <ta e="T133" id="Seg_5102" s="T132">np:Ins</ta>
            <ta e="T134" id="Seg_5103" s="T133">0.3.h:A</ta>
            <ta e="T135" id="Seg_5104" s="T134">0.3.h:A</ta>
            <ta e="T136" id="Seg_5105" s="T135">np:G</ta>
            <ta e="T139" id="Seg_5106" s="T138">0.3.h:A</ta>
            <ta e="T140" id="Seg_5107" s="T139">np:Ins</ta>
            <ta e="T142" id="Seg_5108" s="T141">np:P</ta>
            <ta e="T143" id="Seg_5109" s="T142">0.3.h:A</ta>
            <ta e="T146" id="Seg_5110" s="T145">np:Ins</ta>
            <ta e="T147" id="Seg_5111" s="T146">0.3.h:A</ta>
            <ta e="T148" id="Seg_5112" s="T147">np:So</ta>
            <ta e="T152" id="Seg_5113" s="T151">0.3:Th</ta>
            <ta e="T154" id="Seg_5114" s="T153">n:Time</ta>
            <ta e="T156" id="Seg_5115" s="T155">np:G</ta>
            <ta e="T157" id="Seg_5116" s="T156">0.3.h:A</ta>
            <ta e="T159" id="Seg_5117" s="T158">np.h:E</ta>
            <ta e="T162" id="Seg_5118" s="T160">pp:Path</ta>
            <ta e="T164" id="Seg_5119" s="T163">np:Th</ta>
            <ta e="T168" id="Seg_5120" s="T167">np.h:E</ta>
            <ta e="T170" id="Seg_5121" s="T169">np:Th</ta>
            <ta e="T172" id="Seg_5122" s="T171">np:P</ta>
            <ta e="T175" id="Seg_5123" s="T174">np.h:A</ta>
            <ta e="T178" id="Seg_5124" s="T177">np:G</ta>
            <ta e="T181" id="Seg_5125" s="T180">0.3.h:A</ta>
            <ta e="T182" id="Seg_5126" s="T181">np:Th</ta>
            <ta e="T185" id="Seg_5127" s="T184">np:P</ta>
            <ta e="T187" id="Seg_5128" s="T186">np.h:P</ta>
            <ta e="T192" id="Seg_5129" s="T191">0.3.h:Poss np:Th</ta>
            <ta e="T194" id="Seg_5130" s="T193">np:G</ta>
            <ta e="T196" id="Seg_5131" s="T195">pro.h:P</ta>
            <ta e="T199" id="Seg_5132" s="T198">np:Th</ta>
            <ta e="T200" id="Seg_5133" s="T199">0.2.h:A</ta>
            <ta e="T202" id="Seg_5134" s="T201">np.h:A</ta>
            <ta e="T203" id="Seg_5135" s="T202">np.h:R</ta>
            <ta e="T205" id="Seg_5136" s="T204">np:Th</ta>
            <ta e="T206" id="Seg_5137" s="T205">pro.h:Poss</ta>
            <ta e="T207" id="Seg_5138" s="T206">np:Th</ta>
            <ta e="T209" id="Seg_5139" s="T208">np.h:A</ta>
            <ta e="T211" id="Seg_5140" s="T210">np.h:R</ta>
            <ta e="T213" id="Seg_5141" s="T212">np:G</ta>
            <ta e="T214" id="Seg_5142" s="T213">0.2.h:A</ta>
            <ta e="T215" id="Seg_5143" s="T214">0.2.h:Poss np:Th</ta>
            <ta e="T216" id="Seg_5144" s="T215">0.2.h:A</ta>
            <ta e="T217" id="Seg_5145" s="T216">0.3.h:E</ta>
            <ta e="T218" id="Seg_5146" s="T217">np:Th</ta>
            <ta e="T221" id="Seg_5147" s="T220">0.2.h:A</ta>
            <ta e="T222" id="Seg_5148" s="T221">0.2.h:A</ta>
            <ta e="T223" id="Seg_5149" s="T222">np.h:A</ta>
            <ta e="T225" id="Seg_5150" s="T224">0.2.h:A</ta>
            <ta e="T226" id="Seg_5151" s="T225">0.2.h:A</ta>
            <ta e="T227" id="Seg_5152" s="T226">np:P</ta>
            <ta e="T230" id="Seg_5153" s="T229">0.2.h:A</ta>
            <ta e="T231" id="Seg_5154" s="T230">np.h:A</ta>
            <ta e="T233" id="Seg_5155" s="T232">np:G</ta>
            <ta e="T235" id="Seg_5156" s="T234">0.3.h:Poss np:Th</ta>
            <ta e="T237" id="Seg_5157" s="T236">0.3.h:Poss np:Th</ta>
            <ta e="T239" id="Seg_5158" s="T238">np:Th</ta>
            <ta e="T242" id="Seg_5159" s="T241">np:Ins</ta>
            <ta e="T243" id="Seg_5160" s="T242">0.3.h:A</ta>
            <ta e="T244" id="Seg_5161" s="T243">np.h:A</ta>
            <ta e="T248" id="Seg_5162" s="T247">np:L</ta>
            <ta e="T249" id="Seg_5163" s="T248">0.2.h:A</ta>
            <ta e="T250" id="Seg_5164" s="T249">0.2.h:A</ta>
            <ta e="T252" id="Seg_5165" s="T251">np.h:Poss</ta>
            <ta e="T254" id="Seg_5166" s="T253">np:G</ta>
            <ta e="T255" id="Seg_5167" s="T254">0.2.h:Th</ta>
            <ta e="T257" id="Seg_5168" s="T256">0.2.h:A</ta>
            <ta e="T259" id="Seg_5169" s="T258">pro.h:Th</ta>
            <ta e="T260" id="Seg_5170" s="T259">0.2.h:A</ta>
            <ta e="T261" id="Seg_5171" s="T260">np.h:Th</ta>
            <ta e="T265" id="Seg_5172" s="T264">0.2.h:E</ta>
            <ta e="T266" id="Seg_5173" s="T265">0.2.h:A</ta>
            <ta e="T267" id="Seg_5174" s="T266">0.2.h:A</ta>
            <ta e="T268" id="Seg_5175" s="T267">pro.h:R</ta>
            <ta e="T269" id="Seg_5176" s="T268">np.h:A</ta>
            <ta e="T272" id="Seg_5177" s="T271">np.h:A</ta>
            <ta e="T273" id="Seg_5178" s="T272">pro.h:Poss</ta>
            <ta e="T274" id="Seg_5179" s="T273">np:Th</ta>
            <ta e="T277" id="Seg_5180" s="T276">np.h:Poss</ta>
            <ta e="T279" id="Seg_5181" s="T278">np:G</ta>
            <ta e="T280" id="Seg_5182" s="T279">0.3.h:Th</ta>
            <ta e="T281" id="Seg_5183" s="T280">0.3.h:A</ta>
            <ta e="T283" id="Seg_5184" s="T282">0.3.h:A</ta>
            <ta e="T284" id="Seg_5185" s="T283">0.1.h:Poss np.h:Th</ta>
            <ta e="T289" id="Seg_5186" s="T287">0.1.h:E</ta>
            <ta e="T291" id="Seg_5187" s="T290">0.1.h:Poss np.h:A</ta>
            <ta e="T296" id="Seg_5188" s="T295">0.3.h:A</ta>
            <ta e="T297" id="Seg_5189" s="T296">np.h:A</ta>
            <ta e="T300" id="Seg_5190" s="T299">0.3.h:A</ta>
            <ta e="T301" id="Seg_5191" s="T300">np.h:A</ta>
            <ta e="T305" id="Seg_5192" s="T304">np:Poss</ta>
            <ta e="T306" id="Seg_5193" s="T305">np.h:R</ta>
            <ta e="T307" id="Seg_5194" s="T306">0.3.h:A</ta>
            <ta e="T309" id="Seg_5195" s="T308">np.h:A</ta>
            <ta e="T310" id="Seg_5196" s="T309">pro.h:P</ta>
            <ta e="T314" id="Seg_5197" s="T312">0.2.h:A</ta>
            <ta e="T316" id="Seg_5198" s="T315">np:L</ta>
            <ta e="T318" id="Seg_5199" s="T317">0.2.h:A</ta>
            <ta e="T319" id="Seg_5200" s="T318">0.2.h:Poss np:Th</ta>
            <ta e="T324" id="Seg_5201" s="T323">np:Poss</ta>
            <ta e="T325" id="Seg_5202" s="T324">np.h:Poss</ta>
            <ta e="T326" id="Seg_5203" s="T325">np.h:A</ta>
            <ta e="T328" id="Seg_5204" s="T327">np:L</ta>
            <ta e="T332" id="Seg_5205" s="T331">0.3.h:E</ta>
            <ta e="T333" id="Seg_5206" s="T332">np.h:A</ta>
            <ta e="T335" id="Seg_5207" s="T334">np:G</ta>
            <ta e="T338" id="Seg_5208" s="T337">np.h:P</ta>
            <ta e="T339" id="Seg_5209" s="T338">np:Poss</ta>
            <ta e="T340" id="Seg_5210" s="T339">np.h:A</ta>
            <ta e="T342" id="Seg_5211" s="T341">0.3.h:A</ta>
            <ta e="T345" id="Seg_5212" s="T344">np:L</ta>
            <ta e="T347" id="Seg_5213" s="T345">0.3.h:A</ta>
            <ta e="T348" id="Seg_5214" s="T347">0.2.h:Poss np.h:Th</ta>
            <ta e="T349" id="Seg_5215" s="T348">0.2.h:A</ta>
            <ta e="T350" id="Seg_5216" s="T349">0.3.h:A</ta>
            <ta e="T351" id="Seg_5217" s="T350">np.h:A</ta>
            <ta e="T352" id="Seg_5218" s="T351">0.3.h:Poss np.h:Th</ta>
            <ta e="T354" id="Seg_5219" s="T353">0.2.h:A</ta>
            <ta e="T357" id="Seg_5220" s="T356">np.h:A</ta>
            <ta e="T362" id="Seg_5221" s="T361">np:P</ta>
            <ta e="T364" id="Seg_5222" s="T363">np:Poss</ta>
            <ta e="T367" id="Seg_5223" s="T366">np.h:P</ta>
            <ta e="T369" id="Seg_5224" s="T368">np.h:A</ta>
            <ta e="T372" id="Seg_5225" s="T371">np:G</ta>
            <ta e="T376" id="Seg_5226" s="T375">0.3.h:Th</ta>
            <ta e="T378" id="Seg_5227" s="T377">np.h:Poss</ta>
            <ta e="T379" id="Seg_5228" s="T378">np.h:Th</ta>
            <ta e="T381" id="Seg_5229" s="T380">pro.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_5230" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_5231" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_5232" s="T3">s:temp</ta>
            <ta e="T10" id="Seg_5233" s="T9">np.h:S</ta>
            <ta e="T12" id="Seg_5234" s="T11">v:pred</ta>
            <ta e="T14" id="Seg_5235" s="T13">np.h:S</ta>
            <ta e="T17" id="Seg_5236" s="T15">v:pred</ta>
            <ta e="T18" id="Seg_5237" s="T17">pro.h:O</ta>
            <ta e="T19" id="Seg_5238" s="T18">np.h:S</ta>
            <ta e="T20" id="Seg_5239" s="T19">v:pred</ta>
            <ta e="T21" id="Seg_5240" s="T20">pro.h:S</ta>
            <ta e="T22" id="Seg_5241" s="T21">pro.h:O</ta>
            <ta e="T24" id="Seg_5242" s="T23">v:pred</ta>
            <ta e="T25" id="Seg_5243" s="T24">0.3.h:S v:pred</ta>
            <ta e="T27" id="Seg_5244" s="T26">n:pred</ta>
            <ta e="T28" id="Seg_5245" s="T27">0.2.h:S cop</ta>
            <ta e="T30" id="Seg_5246" s="T29">np.h:O</ta>
            <ta e="T33" id="Seg_5247" s="T32">0.1.h:S v:pred</ta>
            <ta e="T36" id="Seg_5248" s="T33">s:temp</ta>
            <ta e="T37" id="Seg_5249" s="T36">np.h:S</ta>
            <ta e="T40" id="Seg_5250" s="T39">v:pred</ta>
            <ta e="T44" id="Seg_5251" s="T43">0.3.h:S v:pred</ta>
            <ta e="T49" id="Seg_5252" s="T48">ptcl:pred</ta>
            <ta e="T50" id="Seg_5253" s="T49">pro.h:S</ta>
            <ta e="T53" id="Seg_5254" s="T50">s:purp</ta>
            <ta e="T54" id="Seg_5255" s="T53">v:pred</ta>
            <ta e="T58" id="Seg_5256" s="T57">np:O</ta>
            <ta e="T62" id="Seg_5257" s="T60">0.3.h:S v:pred</ta>
            <ta e="T64" id="Seg_5258" s="T63">np:O</ta>
            <ta e="T70" id="Seg_5259" s="T69">0.2.h:O v:pred</ta>
            <ta e="T72" id="Seg_5260" s="T71">0.3.h:S v:pred</ta>
            <ta e="T74" id="Seg_5261" s="T73">np.h:S</ta>
            <ta e="T75" id="Seg_5262" s="T74">v:pred</ta>
            <ta e="T76" id="Seg_5263" s="T75">np.h:S</ta>
            <ta e="T79" id="Seg_5264" s="T78">v:pred</ta>
            <ta e="T81" id="Seg_5265" s="T80">np:O</ta>
            <ta e="T84" id="Seg_5266" s="T83">0.3.h:S v:pred</ta>
            <ta e="T86" id="Seg_5267" s="T85">np.h:S</ta>
            <ta e="T88" id="Seg_5268" s="T86">s:adv</ta>
            <ta e="T90" id="Seg_5269" s="T89">np:O</ta>
            <ta e="T91" id="Seg_5270" s="T90">v:pred</ta>
            <ta e="T92" id="Seg_5271" s="T91">pro:O</ta>
            <ta e="T95" id="Seg_5272" s="T94">0.2.h:S ptcl:pred</ta>
            <ta e="T96" id="Seg_5273" s="T95">np:S</ta>
            <ta e="T100" id="Seg_5274" s="T99">v:pred</ta>
            <ta e="T101" id="Seg_5275" s="T100">s:purp</ta>
            <ta e="T103" id="Seg_5276" s="T102">np.h:S</ta>
            <ta e="T104" id="Seg_5277" s="T103">v:pred</ta>
            <ta e="T105" id="Seg_5278" s="T104">np.h:S</ta>
            <ta e="T107" id="Seg_5279" s="T106">n:pred</ta>
            <ta e="T109" id="Seg_5280" s="T108">0.3.h:S v:pred</ta>
            <ta e="T112" id="Seg_5281" s="T109">s:temp</ta>
            <ta e="T114" id="Seg_5282" s="T113">np:O</ta>
            <ta e="T116" id="Seg_5283" s="T114">0.3.h:S v:pred</ta>
            <ta e="T117" id="Seg_5284" s="T116">np.h:S</ta>
            <ta e="T121" id="Seg_5285" s="T120">v:pred</ta>
            <ta e="T123" id="Seg_5286" s="T122">np.h:S</ta>
            <ta e="T124" id="Seg_5287" s="T123">v:pred</ta>
            <ta e="T126" id="Seg_5288" s="T125">0.3.h:S v:pred</ta>
            <ta e="T129" id="Seg_5289" s="T128">np:S</ta>
            <ta e="T130" id="Seg_5290" s="T129">ptcl:pred</ta>
            <ta e="T134" id="Seg_5291" s="T133">0.3.h:S v:pred</ta>
            <ta e="T135" id="Seg_5292" s="T134">0.3.h:S v:pred</ta>
            <ta e="T139" id="Seg_5293" s="T138">0.3.h:S v:pred</ta>
            <ta e="T142" id="Seg_5294" s="T141">np:O</ta>
            <ta e="T143" id="Seg_5295" s="T142">0.3.h:S v:pred</ta>
            <ta e="T147" id="Seg_5296" s="T146">0.3.h:S v:pred</ta>
            <ta e="T151" id="Seg_5297" s="T148">s:comp</ta>
            <ta e="T152" id="Seg_5298" s="T151">0.3.h:S v:pred</ta>
            <ta e="T157" id="Seg_5299" s="T156">0.3.h:S v:pred</ta>
            <ta e="T159" id="Seg_5300" s="T158">np.h:S</ta>
            <ta e="T160" id="Seg_5301" s="T159">v:pred</ta>
            <ta e="T164" id="Seg_5302" s="T163">np:S</ta>
            <ta e="T165" id="Seg_5303" s="T164">v:pred</ta>
            <ta e="T168" id="Seg_5304" s="T167">np.h:S</ta>
            <ta e="T169" id="Seg_5305" s="T168">v:pred</ta>
            <ta e="T171" id="Seg_5306" s="T169">s:temp</ta>
            <ta e="T172" id="Seg_5307" s="T171">np:S</ta>
            <ta e="T174" id="Seg_5308" s="T172">v:pred</ta>
            <ta e="T175" id="Seg_5309" s="T174">np.h:S</ta>
            <ta e="T180" id="Seg_5310" s="T179">v:pred</ta>
            <ta e="T181" id="Seg_5311" s="T180">0.3.h:S v:pred</ta>
            <ta e="T182" id="Seg_5312" s="T181">np:S</ta>
            <ta e="T183" id="Seg_5313" s="T182">cop</ta>
            <ta e="T185" id="Seg_5314" s="T184">np:S</ta>
            <ta e="T186" id="Seg_5315" s="T185">v:pred</ta>
            <ta e="T187" id="Seg_5316" s="T186">np.h:S</ta>
            <ta e="T190" id="Seg_5317" s="T187">s:rel</ta>
            <ta e="T191" id="Seg_5318" s="T190">v:pred</ta>
            <ta e="T192" id="Seg_5319" s="T191">np:S</ta>
            <ta e="T195" id="Seg_5320" s="T194">v:pred</ta>
            <ta e="T196" id="Seg_5321" s="T195">pro.h:S</ta>
            <ta e="T197" id="Seg_5322" s="T196">adj:pred</ta>
            <ta e="T198" id="Seg_5323" s="T197">cop</ta>
            <ta e="T199" id="Seg_5324" s="T198">np:O</ta>
            <ta e="T200" id="Seg_5325" s="T199">0.2.h:S v:pred</ta>
            <ta e="T202" id="Seg_5326" s="T201">np.h:S</ta>
            <ta e="T205" id="Seg_5327" s="T204">np:O</ta>
            <ta e="T207" id="Seg_5328" s="T206">np:O</ta>
            <ta e="T208" id="Seg_5329" s="T207">v:pred</ta>
            <ta e="T209" id="Seg_5330" s="T208">np.h:S</ta>
            <ta e="T211" id="Seg_5331" s="T210">np.h:O</ta>
            <ta e="T212" id="Seg_5332" s="T211">v:pred</ta>
            <ta e="T214" id="Seg_5333" s="T212">s:temp</ta>
            <ta e="T215" id="Seg_5334" s="T214">np:O</ta>
            <ta e="T216" id="Seg_5335" s="T215">0.2.h:S v:pred</ta>
            <ta e="T217" id="Seg_5336" s="T216">0.3.h:S v:pred</ta>
            <ta e="T220" id="Seg_5337" s="T217">s:adv</ta>
            <ta e="T221" id="Seg_5338" s="T220">0.2.h:S v:pred</ta>
            <ta e="T222" id="Seg_5339" s="T221">s:temp</ta>
            <ta e="T224" id="Seg_5340" s="T222">s:comp</ta>
            <ta e="T226" id="Seg_5341" s="T225">0.2.h:S v:pred</ta>
            <ta e="T227" id="Seg_5342" s="T226">np:O</ta>
            <ta e="T230" id="Seg_5343" s="T229">0.2.h:S v:pred</ta>
            <ta e="T231" id="Seg_5344" s="T230">np.h:S</ta>
            <ta e="T234" id="Seg_5345" s="T233">v:pred</ta>
            <ta e="T236" id="Seg_5346" s="T234">s:adv</ta>
            <ta e="T238" id="Seg_5347" s="T236">s:adv</ta>
            <ta e="T239" id="Seg_5348" s="T238">np:S</ta>
            <ta e="T240" id="Seg_5349" s="T239">adj:pred</ta>
            <ta e="T241" id="Seg_5350" s="T240">cop</ta>
            <ta e="T243" id="Seg_5351" s="T242">0.3.h:S v:pred</ta>
            <ta e="T244" id="Seg_5352" s="T243">np.h:S</ta>
            <ta e="T246" id="Seg_5353" s="T245">v:pred</ta>
            <ta e="T249" id="Seg_5354" s="T246">s:adv</ta>
            <ta e="T250" id="Seg_5355" s="T249">0.2.h:S v:pred</ta>
            <ta e="T256" id="Seg_5356" s="T250">s:temp</ta>
            <ta e="T257" id="Seg_5357" s="T256">0.2.h:S v:pred</ta>
            <ta e="T259" id="Seg_5358" s="T258">pro.h:O</ta>
            <ta e="T260" id="Seg_5359" s="T259">0.2.h:S v:pred</ta>
            <ta e="T264" id="Seg_5360" s="T260">s:comp</ta>
            <ta e="T266" id="Seg_5361" s="T265">0.2.h:S v:pred</ta>
            <ta e="T267" id="Seg_5362" s="T266">0.2.h:S v:pred</ta>
            <ta e="T271" id="Seg_5363" s="T268">s:adv</ta>
            <ta e="T272" id="Seg_5364" s="T271">np.h:S</ta>
            <ta e="T274" id="Seg_5365" s="T273">np:O</ta>
            <ta e="T275" id="Seg_5366" s="T274">v:pred</ta>
            <ta e="T280" id="Seg_5367" s="T279">0.3.h:S v:pred</ta>
            <ta e="T281" id="Seg_5368" s="T280">s:temp</ta>
            <ta e="T283" id="Seg_5369" s="T282">0.3.h:S v:pred</ta>
            <ta e="T287" id="Seg_5370" s="T283">s:comp</ta>
            <ta e="T289" id="Seg_5371" s="T287">0.1.h:S v:pred</ta>
            <ta e="T291" id="Seg_5372" s="T290">np.h:S</ta>
            <ta e="T293" id="Seg_5373" s="T292">v:pred</ta>
            <ta e="T296" id="Seg_5374" s="T295">0.3.h:S v:pred</ta>
            <ta e="T299" id="Seg_5375" s="T296">s:temp</ta>
            <ta e="T300" id="Seg_5376" s="T299">0.3.h:S v:pred</ta>
            <ta e="T303" id="Seg_5377" s="T300">s:adv</ta>
            <ta e="T307" id="Seg_5378" s="T306">0.3.h:S v:pred</ta>
            <ta e="T309" id="Seg_5379" s="T308">np.h:S</ta>
            <ta e="T311" id="Seg_5380" s="T309">s:purp</ta>
            <ta e="T312" id="Seg_5381" s="T311">v:pred</ta>
            <ta e="T314" id="Seg_5382" s="T312">0.2.h:S v:pred</ta>
            <ta e="T318" id="Seg_5383" s="T317">0.2.h:S v:pred</ta>
            <ta e="T322" id="Seg_5384" s="T318">s:adv</ta>
            <ta e="T326" id="Seg_5385" s="T325">np.h:S</ta>
            <ta e="T329" id="Seg_5386" s="T328">v:pred</ta>
            <ta e="T332" id="Seg_5387" s="T329">s:adv</ta>
            <ta e="T333" id="Seg_5388" s="T332">np.h:S</ta>
            <ta e="T337" id="Seg_5389" s="T336">v:pred</ta>
            <ta e="T338" id="Seg_5390" s="T337">np.h:O</ta>
            <ta e="T340" id="Seg_5391" s="T339">np.h:S</ta>
            <ta e="T341" id="Seg_5392" s="T340">v:pred</ta>
            <ta e="T343" id="Seg_5393" s="T341">s:temp</ta>
            <ta e="T347" id="Seg_5394" s="T345">0.3.h:S v:pred</ta>
            <ta e="T348" id="Seg_5395" s="T347">np.h:O</ta>
            <ta e="T349" id="Seg_5396" s="T348">0.2.h:S v:pred</ta>
            <ta e="T350" id="Seg_5397" s="T349">0.3.h:S v:pred</ta>
            <ta e="T351" id="Seg_5398" s="T350">np.h:S</ta>
            <ta e="T352" id="Seg_5399" s="T351">np.h:O</ta>
            <ta e="T353" id="Seg_5400" s="T352">v:pred</ta>
            <ta e="T354" id="Seg_5401" s="T353">0.2.h:S v:pred</ta>
            <ta e="T357" id="Seg_5402" s="T356">np.h:S</ta>
            <ta e="T360" id="Seg_5403" s="T359">v:pred</ta>
            <ta e="T362" id="Seg_5404" s="T361">np:O</ta>
            <ta e="T367" id="Seg_5405" s="T366">np.h:S</ta>
            <ta e="T368" id="Seg_5406" s="T367">v:pred</ta>
            <ta e="T369" id="Seg_5407" s="T368">np.h:S</ta>
            <ta e="T374" id="Seg_5408" s="T373">v:pred</ta>
            <ta e="T375" id="Seg_5409" s="T374">n:pred</ta>
            <ta e="T376" id="Seg_5410" s="T375">cop</ta>
            <ta e="T379" id="Seg_5411" s="T378">np.h:O</ta>
            <ta e="T380" id="Seg_5412" s="T379">v:pred</ta>
            <ta e="T381" id="Seg_5413" s="T380">pro.h:S</ta>
            <ta e="T382" id="Seg_5414" s="T381">n:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T2" id="Seg_5415" s="T1">new</ta>
            <ta e="T5" id="Seg_5416" s="T4">giv-active</ta>
            <ta e="T8" id="Seg_5417" s="T7">new</ta>
            <ta e="T10" id="Seg_5418" s="T9">accs-inf</ta>
            <ta e="T14" id="Seg_5419" s="T13">giv-active</ta>
            <ta e="T18" id="Seg_5420" s="T17">giv-active</ta>
            <ta e="T19" id="Seg_5421" s="T18">new</ta>
            <ta e="T21" id="Seg_5422" s="T20">giv-active-Q</ta>
            <ta e="T22" id="Seg_5423" s="T21">giv-active-Q</ta>
            <ta e="T25" id="Seg_5424" s="T24">0.quot-sp</ta>
            <ta e="T28" id="Seg_5425" s="T27">0.giv-active-Q</ta>
            <ta e="T30" id="Seg_5426" s="T29">new-Q</ta>
            <ta e="T33" id="Seg_5427" s="T32">0.giv-active-Q</ta>
            <ta e="T37" id="Seg_5428" s="T36">giv-active</ta>
            <ta e="T39" id="Seg_5429" s="T38">new</ta>
            <ta e="T42" id="Seg_5430" s="T41">giv-active</ta>
            <ta e="T43" id="Seg_5431" s="T42">0.giv-active</ta>
            <ta e="T44" id="Seg_5432" s="T43">0.quot-sp</ta>
            <ta e="T48" id="Seg_5433" s="T47">giv-inactive-Q</ta>
            <ta e="T50" id="Seg_5434" s="T49">giv-active-Q</ta>
            <ta e="T51" id="Seg_5435" s="T50">giv-inactive-Q</ta>
            <ta e="T52" id="Seg_5436" s="T51">accs-inf-Q</ta>
            <ta e="T55" id="Seg_5437" s="T54">giv-active-Q</ta>
            <ta e="T56" id="Seg_5438" s="T55">new-Q</ta>
            <ta e="T57" id="Seg_5439" s="T56">accs-gen-Q</ta>
            <ta e="T58" id="Seg_5440" s="T57">accs-gen-Q</ta>
            <ta e="T62" id="Seg_5441" s="T60">0.giv-active-Q</ta>
            <ta e="T64" id="Seg_5442" s="T63">giv-active-Q</ta>
            <ta e="T67" id="Seg_5443" s="T66">new-Q</ta>
            <ta e="T68" id="Seg_5444" s="T67">new-Q</ta>
            <ta e="T70" id="Seg_5445" s="T69">0.giv-active-Q</ta>
            <ta e="T72" id="Seg_5446" s="T71">0.quot-sp</ta>
            <ta e="T74" id="Seg_5447" s="T73">giv-active</ta>
            <ta e="T76" id="Seg_5448" s="T75">giv-inactive</ta>
            <ta e="T80" id="Seg_5449" s="T79">giv-inactive</ta>
            <ta e="T81" id="Seg_5450" s="T80">giv-inactive</ta>
            <ta e="T84" id="Seg_5451" s="T83">0.giv-active</ta>
            <ta e="T86" id="Seg_5452" s="T85">0.giv-inactive-Q</ta>
            <ta e="T87" id="Seg_5453" s="T86">new-Q</ta>
            <ta e="T89" id="Seg_5454" s="T88">0.giv-active-Q</ta>
            <ta e="T90" id="Seg_5455" s="T89">0.giv-inactive-Q</ta>
            <ta e="T92" id="Seg_5456" s="T91">0.giv-active-Q</ta>
            <ta e="T95" id="Seg_5457" s="T94">0.giv-active-Q</ta>
            <ta e="T96" id="Seg_5458" s="T95">giv-active</ta>
            <ta e="T98" id="Seg_5459" s="T97">accs-gen</ta>
            <ta e="T103" id="Seg_5460" s="T102">giv-inactive</ta>
            <ta e="T105" id="Seg_5461" s="T104">0.giv-inactive-Q</ta>
            <ta e="T109" id="Seg_5462" s="T108">quot-th</ta>
            <ta e="T110" id="Seg_5463" s="T109">giv-inactive</ta>
            <ta e="T114" id="Seg_5464" s="T113">giv-inactive</ta>
            <ta e="T116" id="Seg_5465" s="T114">0.giv-inactive</ta>
            <ta e="T117" id="Seg_5466" s="T116">giv-inactive</ta>
            <ta e="T120" id="Seg_5467" s="T119">giv-inactive</ta>
            <ta e="T123" id="Seg_5468" s="T122">giv-inactive-Q</ta>
            <ta e="T126" id="Seg_5469" s="T125">0.giv-active-Q</ta>
            <ta e="T128" id="Seg_5470" s="T127">giv-inactive-Q</ta>
            <ta e="T129" id="Seg_5471" s="T128">new-Q</ta>
            <ta e="T133" id="Seg_5472" s="T132">new-Q</ta>
            <ta e="T134" id="Seg_5473" s="T133">0.giv-active-Q</ta>
            <ta e="T135" id="Seg_5474" s="T134">0.quot-sp</ta>
            <ta e="T136" id="Seg_5475" s="T135">giv-active</ta>
            <ta e="T139" id="Seg_5476" s="T138">0.giv-active</ta>
            <ta e="T140" id="Seg_5477" s="T139">new</ta>
            <ta e="T142" id="Seg_5478" s="T141">new</ta>
            <ta e="T143" id="Seg_5479" s="T142">0.giv-active</ta>
            <ta e="T146" id="Seg_5480" s="T145">new</ta>
            <ta e="T147" id="Seg_5481" s="T146">0.giv-active</ta>
            <ta e="T152" id="Seg_5482" s="T151">0.giv-inactive</ta>
            <ta e="T153" id="Seg_5483" s="T152">new</ta>
            <ta e="T154" id="Seg_5484" s="T153">new</ta>
            <ta e="T156" id="Seg_5485" s="T155">giv-inactive</ta>
            <ta e="T157" id="Seg_5486" s="T156">accs-aggr</ta>
            <ta e="T159" id="Seg_5487" s="T158">giv-active</ta>
            <ta e="T161" id="Seg_5488" s="T160">accs-sit</ta>
            <ta e="T164" id="Seg_5489" s="T163">giv-inactive</ta>
            <ta e="T168" id="Seg_5490" s="T167">giv-active</ta>
            <ta e="T170" id="Seg_5491" s="T169">giv-inactive</ta>
            <ta e="T172" id="Seg_5492" s="T171">giv-inactive</ta>
            <ta e="T175" id="Seg_5493" s="T174">giv-inactive</ta>
            <ta e="T178" id="Seg_5494" s="T177">giv-inactive</ta>
            <ta e="T181" id="Seg_5495" s="T180">0.quot-sp</ta>
            <ta e="T182" id="Seg_5496" s="T181">accs-sit-Q</ta>
            <ta e="T185" id="Seg_5497" s="T184">giv-inactive-Q</ta>
            <ta e="T187" id="Seg_5498" s="T186">giv-inactive-Q</ta>
            <ta e="T192" id="Seg_5499" s="T191">accs-inf-Q</ta>
            <ta e="T194" id="Seg_5500" s="T193">accs-inf-Q</ta>
            <ta e="T196" id="Seg_5501" s="T195">giv-inactive-Q</ta>
            <ta e="T199" id="Seg_5502" s="T198">new-Q</ta>
            <ta e="T200" id="Seg_5503" s="T199">0.giv-inactive-Q</ta>
            <ta e="T202" id="Seg_5504" s="T201">giv-active</ta>
            <ta e="T203" id="Seg_5505" s="T202">giv-inactive</ta>
            <ta e="T205" id="Seg_5506" s="T204">giv-active</ta>
            <ta e="T206" id="Seg_5507" s="T205">giv-active</ta>
            <ta e="T207" id="Seg_5508" s="T206">new</ta>
            <ta e="T209" id="Seg_5509" s="T208">giv-inactive</ta>
            <ta e="T211" id="Seg_5510" s="T210">giv-active</ta>
            <ta e="T212" id="Seg_5511" s="T211">quot-sp</ta>
            <ta e="T213" id="Seg_5512" s="T212">giv-inactive-Q</ta>
            <ta e="T214" id="Seg_5513" s="T213">0.giv-active-Q</ta>
            <ta e="T215" id="Seg_5514" s="T214">giv-inactive-Q</ta>
            <ta e="T216" id="Seg_5515" s="T215">0.giv-active-Q</ta>
            <ta e="T217" id="Seg_5516" s="T216">0.new-Q</ta>
            <ta e="T218" id="Seg_5517" s="T217">new</ta>
            <ta e="T221" id="Seg_5518" s="T220">0.giv-active-Q</ta>
            <ta e="T222" id="Seg_5519" s="T221">0.giv-active-Q</ta>
            <ta e="T223" id="Seg_5520" s="T222">giv-inactive-Q</ta>
            <ta e="T225" id="Seg_5521" s="T224">0.giv-active-Q</ta>
            <ta e="T226" id="Seg_5522" s="T225">0.giv-active-Q</ta>
            <ta e="T227" id="Seg_5523" s="T226">new-Q</ta>
            <ta e="T230" id="Seg_5524" s="T229">0.giv-active-Q</ta>
            <ta e="T231" id="Seg_5525" s="T230">giv-active</ta>
            <ta e="T233" id="Seg_5526" s="T232">giv-inactive</ta>
            <ta e="T235" id="Seg_5527" s="T234">giv-inactive</ta>
            <ta e="T237" id="Seg_5528" s="T236">giv-inactive</ta>
            <ta e="T239" id="Seg_5529" s="T238">new</ta>
            <ta e="T242" id="Seg_5530" s="T241">giv-inactive</ta>
            <ta e="T243" id="Seg_5531" s="T242">0.giv-inactive</ta>
            <ta e="T244" id="Seg_5532" s="T243">giv-inactive</ta>
            <ta e="T246" id="Seg_5533" s="T245">quot-sp</ta>
            <ta e="T248" id="Seg_5534" s="T247">giv-inactive-Q</ta>
            <ta e="T249" id="Seg_5535" s="T248">0.giv-inactive-Q</ta>
            <ta e="T250" id="Seg_5536" s="T249">0.giv-active-Q</ta>
            <ta e="T252" id="Seg_5537" s="T251">new-Q</ta>
            <ta e="T254" id="Seg_5538" s="T253">accs-inf-Q</ta>
            <ta e="T255" id="Seg_5539" s="T254">0.giv-active-Q</ta>
            <ta e="T257" id="Seg_5540" s="T256">0.giv-active-Q</ta>
            <ta e="T259" id="Seg_5541" s="T258">giv-inactive-Q</ta>
            <ta e="T260" id="Seg_5542" s="T259">0.giv-active-Q</ta>
            <ta e="T261" id="Seg_5543" s="T260">new-Q</ta>
            <ta e="T266" id="Seg_5544" s="T265">0.giv-active-Q</ta>
            <ta e="T267" id="Seg_5545" s="T266">0.quot-sp-Q</ta>
            <ta e="T268" id="Seg_5546" s="T267">giv-active-Q</ta>
            <ta e="T269" id="Seg_5547" s="T268">giv-active</ta>
            <ta e="T272" id="Seg_5548" s="T271">giv-active</ta>
            <ta e="T273" id="Seg_5549" s="T272">giv-active</ta>
            <ta e="T274" id="Seg_5550" s="T273">giv-inactive</ta>
            <ta e="T277" id="Seg_5551" s="T276">giv-inactive</ta>
            <ta e="T279" id="Seg_5552" s="T278">giv-inactive</ta>
            <ta e="T280" id="Seg_5553" s="T279">0.giv-active</ta>
            <ta e="T281" id="Seg_5554" s="T280">0.giv-active</ta>
            <ta e="T283" id="Seg_5555" s="T282">0.quot-sp</ta>
            <ta e="T284" id="Seg_5556" s="T283">giv-inactive-Q</ta>
            <ta e="T289" id="Seg_5557" s="T287">0.giv-active-Q</ta>
            <ta e="T291" id="Seg_5558" s="T290">giv-inactive-Q</ta>
            <ta e="T296" id="Seg_5559" s="T295">0.quot-sp</ta>
            <ta e="T297" id="Seg_5560" s="T296">giv-active</ta>
            <ta e="T300" id="Seg_5561" s="T299">0.quot-sp</ta>
            <ta e="T301" id="Seg_5562" s="T300">giv-active</ta>
            <ta e="T305" id="Seg_5563" s="T304">giv-inactive</ta>
            <ta e="T306" id="Seg_5564" s="T305">giv-inactive</ta>
            <ta e="T307" id="Seg_5565" s="T306">quot-sp</ta>
            <ta e="T309" id="Seg_5566" s="T308">giv-inactive-Q</ta>
            <ta e="T310" id="Seg_5567" s="T309">accs-inf-Q</ta>
            <ta e="T314" id="Seg_5568" s="T312">0.giv-active-Q</ta>
            <ta e="T316" id="Seg_5569" s="T315">accs-sit-Q</ta>
            <ta e="T318" id="Seg_5570" s="T317">0.giv-active-Q</ta>
            <ta e="T319" id="Seg_5571" s="T318">accs-inf-Q</ta>
            <ta e="T324" id="Seg_5572" s="T323">giv-inactive</ta>
            <ta e="T325" id="Seg_5573" s="T324">giv-inactive</ta>
            <ta e="T326" id="Seg_5574" s="T325">giv-active</ta>
            <ta e="T328" id="Seg_5575" s="T327">giv-active</ta>
            <ta e="T331" id="Seg_5576" s="T330">giv-active</ta>
            <ta e="T332" id="Seg_5577" s="T331">0.giv-active</ta>
            <ta e="T333" id="Seg_5578" s="T332">giv-inactive</ta>
            <ta e="T335" id="Seg_5579" s="T334">giv-inactive</ta>
            <ta e="T338" id="Seg_5580" s="T337">giv-inactive-Q</ta>
            <ta e="T339" id="Seg_5581" s="T338">new-Q</ta>
            <ta e="T340" id="Seg_5582" s="T339">new-Q</ta>
            <ta e="T342" id="Seg_5583" s="T341">0.giv-active-Q</ta>
            <ta e="T345" id="Seg_5584" s="T344">giv-inactive-Q</ta>
            <ta e="T347" id="Seg_5585" s="T345">0.giv-active-Q</ta>
            <ta e="T348" id="Seg_5586" s="T347">giv-active-Q</ta>
            <ta e="T349" id="Seg_5587" s="T348">0.giv-active-Q</ta>
            <ta e="T350" id="Seg_5588" s="T349">0.giv-active-Q</ta>
            <ta e="T351" id="Seg_5589" s="T350">giv-active</ta>
            <ta e="T352" id="Seg_5590" s="T351">giv-active</ta>
            <ta e="T354" id="Seg_5591" s="T353">0.giv-active-Q</ta>
            <ta e="T355" id="Seg_5592" s="T354">quot-sp</ta>
            <ta e="T357" id="Seg_5593" s="T356">giv-active</ta>
            <ta e="T362" id="Seg_5594" s="T361">giv-inactive</ta>
            <ta e="T364" id="Seg_5595" s="T363">giv-inactive</ta>
            <ta e="T367" id="Seg_5596" s="T366">accs-inf</ta>
            <ta e="T369" id="Seg_5597" s="T368">giv-inactive</ta>
            <ta e="T372" id="Seg_5598" s="T371">giv-active</ta>
            <ta e="T376" id="Seg_5599" s="T375">0.giv-active</ta>
            <ta e="T378" id="Seg_5600" s="T377">giv-inactive</ta>
            <ta e="T379" id="Seg_5601" s="T378">giv-inactive</ta>
            <ta e="T381" id="Seg_5602" s="T380">giv-active</ta>
            <ta e="T382" id="Seg_5603" s="T381">accs-sit</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T3" id="Seg_5604" s="T2">0.top.int.abstr.</ta>
            <ta e="T6" id="Seg_5605" s="T3">top.int.concr</ta>
            <ta e="T14" id="Seg_5606" s="T13">top.int.concr</ta>
            <ta e="T18" id="Seg_5607" s="T17">top.int.concr</ta>
            <ta e="T21" id="Seg_5608" s="T20">top.int.concr</ta>
            <ta e="T28" id="Seg_5609" s="T27">0.top.int.concr</ta>
            <ta e="T33" id="Seg_5610" s="T32">0.top.int.concr</ta>
            <ta e="T36" id="Seg_5611" s="T33">top.int.concr</ta>
            <ta e="T44" id="Seg_5612" s="T43">0.top.int.concr</ta>
            <ta e="T49" id="Seg_5613" s="T48">0.top.int.abstr.</ta>
            <ta e="T50" id="Seg_5614" s="T49">top.int.concr</ta>
            <ta e="T62" id="Seg_5615" s="T60">0.top.int.concr</ta>
            <ta e="T64" id="Seg_5616" s="T63">top.int.concr</ta>
            <ta e="T74" id="Seg_5617" s="T73">top.int.concr</ta>
            <ta e="T76" id="Seg_5618" s="T75">top.int.concr</ta>
            <ta e="T84" id="Seg_5619" s="T83">0.top.int.concr</ta>
            <ta e="T86" id="Seg_5620" s="T85">top.int.concr</ta>
            <ta e="T96" id="Seg_5621" s="T95">top.int.concr</ta>
            <ta e="T103" id="Seg_5622" s="T102">top.int.concr</ta>
            <ta e="T105" id="Seg_5623" s="T104">top.int.concr</ta>
            <ta e="T112" id="Seg_5624" s="T109">top.int.concr</ta>
            <ta e="T117" id="Seg_5625" s="T116">top.int.concr</ta>
            <ta e="T123" id="Seg_5626" s="T122">top.int.concr</ta>
            <ta e="T126" id="Seg_5627" s="T125">0.top.int.concr</ta>
            <ta e="T128" id="Seg_5628" s="T126">top.int.concr</ta>
            <ta e="T134" id="Seg_5629" s="T133">0.top.int.concr</ta>
            <ta e="T139" id="Seg_5630" s="T138">0.top.int.concr</ta>
            <ta e="T143" id="Seg_5631" s="T142">0.top.int.concr</ta>
            <ta e="T147" id="Seg_5632" s="T146">0.top.int.concr</ta>
            <ta e="T152" id="Seg_5633" s="T151">0.top.int.concr</ta>
            <ta e="T154" id="Seg_5634" s="T153">top.int.concr</ta>
            <ta e="T159" id="Seg_5635" s="T158">top.int.concr</ta>
            <ta e="T162" id="Seg_5636" s="T160">top.int.concr</ta>
            <ta e="T168" id="Seg_5637" s="T167">top.int.concr</ta>
            <ta e="T171" id="Seg_5638" s="T169">top.int.concr</ta>
            <ta e="T175" id="Seg_5639" s="T174">top.int.concr</ta>
            <ta e="T183" id="Seg_5640" s="T182">0.top.int.abstr.</ta>
            <ta e="T185" id="Seg_5641" s="T183">top.int.concr</ta>
            <ta e="T187" id="Seg_5642" s="T186">top.int.concr</ta>
            <ta e="T192" id="Seg_5643" s="T191">top.int.concr.contr.</ta>
            <ta e="T196" id="Seg_5644" s="T195">top.int.concr</ta>
            <ta e="T202" id="Seg_5645" s="T201">top.int.concr</ta>
            <ta e="T209" id="Seg_5646" s="T208">top.int.concr</ta>
            <ta e="T214" id="Seg_5647" s="T212">top.int.concr</ta>
            <ta e="T217" id="Seg_5648" s="T216">0.top.int.concr</ta>
            <ta e="T222" id="Seg_5649" s="T221">top.int.concr</ta>
            <ta e="T230" id="Seg_5650" s="T229">0.top.int.concr</ta>
            <ta e="T231" id="Seg_5651" s="T230">top.int.concr</ta>
            <ta e="T241" id="Seg_5652" s="T240">0.top.int.abstr.</ta>
            <ta e="T243" id="Seg_5653" s="T242">0.top.int.concr</ta>
            <ta e="T244" id="Seg_5654" s="T243">top.int.concr</ta>
            <ta e="T256" id="Seg_5655" s="T250">top.int.concr</ta>
            <ta e="T272" id="Seg_5656" s="T271">top.int.concr</ta>
            <ta e="T280" id="Seg_5657" s="T279">0.top.int.concr</ta>
            <ta e="T283" id="Seg_5658" s="T282">0.top.int.concr</ta>
            <ta e="T289" id="Seg_5659" s="T287">0.top.int.concr</ta>
            <ta e="T291" id="Seg_5660" s="T290">top.int.concr</ta>
            <ta e="T299" id="Seg_5661" s="T296">top.int.concr</ta>
            <ta e="T301" id="Seg_5662" s="T300">top.int.concr</ta>
            <ta e="T309" id="Seg_5663" s="T308">top.int.concr</ta>
            <ta e="T319" id="Seg_5664" s="T318">top.int.concr</ta>
            <ta e="T327" id="Seg_5665" s="T322">top.int.concr</ta>
            <ta e="T333" id="Seg_5666" s="T332">top.int.concr</ta>
            <ta e="T338" id="Seg_5667" s="T337">top.int.concr</ta>
            <ta e="T343" id="Seg_5668" s="T341">top.int.concr</ta>
            <ta e="T351" id="Seg_5669" s="T350">top.int.concr</ta>
            <ta e="T357" id="Seg_5670" s="T356">top.int.concr</ta>
            <ta e="T367" id="Seg_5671" s="T362">top.int.concr</ta>
            <ta e="T369" id="Seg_5672" s="T368">top.int.concr</ta>
            <ta e="T376" id="Seg_5673" s="T375">0.top.int.concr</ta>
            <ta e="T381" id="Seg_5674" s="T380">0.top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T3" id="Seg_5675" s="T0">foc.wid</ta>
            <ta e="T12" id="Seg_5676" s="T6">foc.wid</ta>
            <ta e="T17" id="Seg_5677" s="T14">foc.int</ta>
            <ta e="T20" id="Seg_5678" s="T18">foc.int</ta>
            <ta e="T24" id="Seg_5679" s="T21">foc.int</ta>
            <ta e="T28" id="Seg_5680" s="T25">foc.int</ta>
            <ta e="T33" id="Seg_5681" s="T28">foc.int</ta>
            <ta e="T40" id="Seg_5682" s="T36">foc.wid</ta>
            <ta e="T44" id="Seg_5683" s="T40">foc.int</ta>
            <ta e="T49" id="Seg_5684" s="T44">foc.wid</ta>
            <ta e="T54" id="Seg_5685" s="T50">foc.int</ta>
            <ta e="T62" id="Seg_5686" s="T54">foc.int</ta>
            <ta e="T71" id="Seg_5687" s="T64">foc.int</ta>
            <ta e="T75" id="Seg_5688" s="T74">foc.int</ta>
            <ta e="T79" id="Seg_5689" s="T77">foc.int</ta>
            <ta e="T84" id="Seg_5690" s="T79">foc.int</ta>
            <ta e="T91" id="Seg_5691" s="T84">foc.wid</ta>
            <ta e="T101" id="Seg_5692" s="T97">foc.int</ta>
            <ta e="T104" id="Seg_5693" s="T103">foc.int</ta>
            <ta e="T108" id="Seg_5694" s="T105">foc.int</ta>
            <ta e="T116" id="Seg_5695" s="T112">foc.wid</ta>
            <ta e="T121" id="Seg_5696" s="T118">foc.int</ta>
            <ta e="T124" id="Seg_5697" s="T123">foc.ver</ta>
            <ta e="T126" id="Seg_5698" s="T125">foc.ver</ta>
            <ta e="T130" id="Seg_5699" s="T128">foc.int</ta>
            <ta e="T134" id="Seg_5700" s="T130">foc.int</ta>
            <ta e="T139" id="Seg_5701" s="T135">foc.int</ta>
            <ta e="T143" id="Seg_5702" s="T139">foc.int</ta>
            <ta e="T147" id="Seg_5703" s="T143">foc.int</ta>
            <ta e="T151" id="Seg_5704" s="T148">foc.nar</ta>
            <ta e="T157" id="Seg_5705" s="T152">foc.wid</ta>
            <ta e="T160" id="Seg_5706" s="T159">foc.int</ta>
            <ta e="T165" id="Seg_5707" s="T160">foc.wid</ta>
            <ta e="T169" id="Seg_5708" s="T168">foc.int</ta>
            <ta e="T174" id="Seg_5709" s="T172">foc.int</ta>
            <ta e="T180" id="Seg_5710" s="T176">foc.int</ta>
            <ta e="T182" id="Seg_5711" s="T181">foc.nar</ta>
            <ta e="T186" id="Seg_5712" s="T185">foc.nar</ta>
            <ta e="T191" id="Seg_5713" s="T187">foc.int</ta>
            <ta e="T195" id="Seg_5714" s="T192">foc.int</ta>
            <ta e="T198" id="Seg_5715" s="T196">foc.int</ta>
            <ta e="T199" id="Seg_5716" s="T198">foc.nar</ta>
            <ta e="T207" id="Seg_5717" s="T203">foc.nar</ta>
            <ta e="T212" id="Seg_5718" s="T210">foc.int</ta>
            <ta e="T216" id="Seg_5719" s="T214">foc.int</ta>
            <ta e="T217" id="Seg_5720" s="T216">foc.wid</ta>
            <ta e="T221" id="Seg_5721" s="T217">foc.wid</ta>
            <ta e="T226" id="Seg_5722" s="T222">foc.int</ta>
            <ta e="T229" id="Seg_5723" s="T226">foc.nar</ta>
            <ta e="T238" id="Seg_5724" s="T231">foc.int</ta>
            <ta e="T241" id="Seg_5725" s="T238">foc.wid</ta>
            <ta e="T243" id="Seg_5726" s="T241">foc.int</ta>
            <ta e="T246" id="Seg_5727" s="T245">foc.int</ta>
            <ta e="T250" id="Seg_5728" s="T246">foc.wid</ta>
            <ta e="T260" id="Seg_5729" s="T256">foc.int</ta>
            <ta e="T266" id="Seg_5730" s="T260">foc.int</ta>
            <ta e="T267" id="Seg_5731" s="T266">foc.int</ta>
            <ta e="T275" id="Seg_5732" s="T272">foc.int</ta>
            <ta e="T280" id="Seg_5733" s="T275">foc.int</ta>
            <ta e="T283" id="Seg_5734" s="T280">foc.int</ta>
            <ta e="T289" id="Seg_5735" s="T283">foc.int</ta>
            <ta e="T292" id="Seg_5736" s="T291">foc.nar</ta>
            <ta e="T300" id="Seg_5737" s="T299">foc.int</ta>
            <ta e="T307" id="Seg_5738" s="T302">foc.int</ta>
            <ta e="T312" id="Seg_5739" s="T309">foc.int</ta>
            <ta e="T314" id="Seg_5740" s="T312">foc.int</ta>
            <ta e="T318" id="Seg_5741" s="T314">foc.int</ta>
            <ta e="T320" id="Seg_5742" s="T318">foc.nar</ta>
            <ta e="T329" id="Seg_5743" s="T322">foc.wid</ta>
            <ta e="T337" id="Seg_5744" s="T334">foc.int</ta>
            <ta e="T340" id="Seg_5745" s="T338">foc.nar</ta>
            <ta e="T347" id="Seg_5746" s="T343">foc.wid</ta>
            <ta e="T349" id="Seg_5747" s="T347">foc.int</ta>
            <ta e="T350" id="Seg_5748" s="T349">foc.wid</ta>
            <ta e="T353" id="Seg_5749" s="T351">foc.int</ta>
            <ta e="T362" id="Seg_5750" s="T358">foc.int</ta>
            <ta e="T367" id="Seg_5751" s="T362">foc.nar</ta>
            <ta e="T374" id="Seg_5752" s="T369">foc.int</ta>
            <ta e="T375" id="Seg_5753" s="T374">foc.nar</ta>
            <ta e="T379" id="Seg_5754" s="T376">foc.nar</ta>
         </annotation>
         <annotation name="BOR" tierref="TIE0" />
         <annotation name="BOR-Phon" tierref="TIE2" />
         <annotation name="BOR-Morph" tierref="TIE3" />
         <annotation name="CS" tierref="TIE4" />
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_5755" s="T0">There lived a poor farmer.</ta>
            <ta e="T12" id="Seg_5756" s="T3">As this farmer died, his orphaned son stayed in the dugout [alone].</ta>
            <ta e="T17" id="Seg_5757" s="T12">This boy was starving.</ta>
            <ta e="T20" id="Seg_5758" s="T17">A fox found him.</ta>
            <ta e="T25" id="Seg_5759" s="T20">"I will make you alive again", it said.</ta>
            <ta e="T33" id="Seg_5760" s="T25">"You will be a rich man, I will marry you to a rich woman."</ta>
            <ta e="T40" id="Seg_5761" s="T33">After saying so the fox went to the czar Thunder. </ta>
            <ta e="T44" id="Seg_5762" s="T40">It came to the czar Thunder and said:</ta>
            <ta e="T49" id="Seg_5763" s="T44">"There is a very rich young man.</ta>
            <ta e="T54" id="Seg_5764" s="T49">He sent me to propose to your daughter.</ta>
            <ta e="T62" id="Seg_5765" s="T54">He will send the best selected forest animals as a bride price for his daughter.</ta>
            <ta e="T72" id="Seg_5766" s="T62">You shall receive these animals in separated houses with cooked food", it said.</ta>
            <ta e="T75" id="Seg_5767" s="T72">The czar Thunder agreed.</ta>
            <ta e="T79" id="Seg_5768" s="T75">The fox deceiver ran back.</ta>
            <ta e="T84" id="Seg_5769" s="T79">Meeting different forest animals it said [to them]:</ta>
            <ta e="T91" id="Seg_5770" s="T84">"The czar Thunder is rich and generous and he prepared food for you.</ta>
            <ta e="T95" id="Seg_5771" s="T91">He calls you to eat."</ta>
            <ta e="T101" id="Seg_5772" s="T95">A lot of animals came in herds to try the food.</ta>
            <ta e="T109" id="Seg_5773" s="T101">The czar Thunder was happy and thought what a rich man the son-in-law was.</ta>
            <ta e="T116" id="Seg_5774" s="T109">While the animals were eating, those houses were being locked up.</ta>
            <ta e="T121" id="Seg_5775" s="T116">The fox deceiver came to the czar Thunder.</ta>
            <ta e="T125" id="Seg_5776" s="T121">"Well, did the animals come?"</ta>
            <ta e="T126" id="Seg_5777" s="T125">"They came."</ta>
            <ta e="T130" id="Seg_5778" s="T126">"The son-in-law doesn't have time.</ta>
            <ta e="T135" id="Seg_5779" s="T130">He will come soon on a golden ship", it said.</ta>
            <ta e="T139" id="Seg_5780" s="T135">It ran back to the boy.</ta>
            <ta e="T143" id="Seg_5781" s="T139">It ordered to build a bad raft out of willow bushes.</ta>
            <ta e="T152" id="Seg_5782" s="T143">It ordered to decorate it with different field flowers so that it looked beautiful and elegant from far.</ta>
            <ta e="T157" id="Seg_5783" s="T152">On a windy day they went to the czar Thunder.</ta>
            <ta e="T167" id="Seg_5784" s="T157">The czar Thunder saw a golden ship, elegant and colourful.</ta>
            <ta e="T169" id="Seg_5785" s="T167">The czar was happy.</ta>
            <ta e="T174" id="Seg_5786" s="T169">As wind came up, the raft went to pieces.</ta>
            <ta e="T181" id="Seg_5787" s="T174">The fox deceiver ran to the czar Thunder and said:</ta>
            <ta e="T186" id="Seg_5788" s="T181">"A calamity has happened, the golden ship has sunk.</ta>
            <ta e="T198" id="Seg_5789" s="T186">Your son-in-law, a keen man, has survived, but his whole property is gone under the water, he himself is naked.</ta>
            <ta e="T200" id="Seg_5790" s="T198">Send [him] [some] clothes."</ta>
            <ta e="T208" id="Seg_5791" s="T200">The czar Thunder sent the best clothes and his own ship to the boy.</ta>
            <ta e="T212" id="Seg_5792" s="T208">The fox deceiver advised the boy:</ta>
            <ta e="T217" id="Seg_5793" s="T212">"When you come to the czar, don't look upon your clothes, they will be surprised.</ta>
            <ta e="T221" id="Seg_5794" s="T217">Greet [him] just with a courtsey.</ta>
            <ta e="T230" id="Seg_5795" s="T221">While eating keep an eye on how the others are eating, and eat also so, drink only few wine."</ta>
            <ta e="T238" id="Seg_5796" s="T230">The boy came to the czar Thunder on his ship, in his clothes.</ta>
            <ta e="T241" id="Seg_5797" s="T238">The wedding was very excessive.</ta>
            <ta e="T243" id="Seg_5798" s="T241">They came back by ship.</ta>
            <ta e="T246" id="Seg_5799" s="T243">The fox deceiver says:</ta>
            <ta e="T260" id="Seg_5800" s="T246">"Do not stop at your pit-dwelling, pass it by, and when you come closer to the golden city of a foreign czar, tell them to call me. </ta>
            <ta e="T268" id="Seg_5801" s="T260">Tell me to go and find out how your people live."</ta>
            <ta e="T275" id="Seg_5802" s="T268">The boy passed his pit-dwelling as the fox had told him to.</ta>
            <ta e="T280" id="Seg_5803" s="T275">He came closer to the golden city of a foreign czar.</ta>
            <ta e="T283" id="Seg_5804" s="T280">He stopped and said:</ta>
            <ta e="T289" id="Seg_5805" s="T283">"I want to know, how my people live.</ta>
            <ta e="T296" id="Seg_5806" s="T289">Where is my fox, it will find it out and come back", he said.</ta>
            <ta e="T300" id="Seg_5807" s="T296">As the fox deceiver came, he told it [to do that].</ta>
            <ta e="T307" id="Seg_5808" s="T300">The fox deceiver went to the czar of the golden city and said:</ta>
            <ta e="T312" id="Seg_5809" s="T307">"The czar Thunder is coming to defeat you.</ta>
            <ta e="T314" id="Seg_5810" s="T312">You better flee.</ta>
            <ta e="T322" id="Seg_5811" s="T314">Hide yourselves in that forest, so that just your heads are seen."</ta>
            <ta e="T332" id="Seg_5812" s="T322">All the people of the golden city fled into the forest, because they were afraid of the czar Thunder.</ta>
            <ta e="T337" id="Seg_5813" s="T332">The fox deceiver came to the boy crying:</ta>
            <ta e="T341" id="Seg_5814" s="T337">"Our people were defeated by the czar of the evil spirits.</ta>
            <ta e="T347" id="Seg_5815" s="T341">They ate all of them and hid themselves in that forest.</ta>
            <ta e="T350" id="Seg_5816" s="T347">Ask your father-in-law to make lightning."</ta>
            <ta e="T355" id="Seg_5817" s="T350">The boy asked his father-in-law and said "Make lightning".</ta>
            <ta e="T368" id="Seg_5818" s="T355">The czar Thunder let lightning strike into the forest so that not a single human of the golden city stayed alive.</ta>
            <ta e="T376" id="Seg_5819" s="T368">The boy started to live in that city and became a czar.</ta>
            <ta e="T381" id="Seg_5820" s="T376">That [boy] married the daughter of the czar Thunder.</ta>
            <ta e="T382" id="Seg_5821" s="T381">The end.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_5822" s="T0">Es lebte ein armer Bauer.</ta>
            <ta e="T12" id="Seg_5823" s="T3">Als dieser Bauer starb, blieb in seiner Erdhütte sein Sohn als Waise zurück.</ta>
            <ta e="T17" id="Seg_5824" s="T12">Dieser Junge starb vor Hunger.</ta>
            <ta e="T20" id="Seg_5825" s="T17">Ein Fuchs fand ihn.</ta>
            <ta e="T25" id="Seg_5826" s="T20">"Ich mache dich wieder lebendig", sagte er.</ta>
            <ta e="T33" id="Seg_5827" s="T25">"Du wirst ein reicher Mensch, ich verheirate dich mit einer reichen Frau."</ta>
            <ta e="T40" id="Seg_5828" s="T33">Nachdem er das gesagt hatte, ging der Fuchs zu seinem Zaren Donner.</ta>
            <ta e="T44" id="Seg_5829" s="T40">Er kam zu dem Zaren Donner und sagte:</ta>
            <ta e="T49" id="Seg_5830" s="T44">"Es gibt einen sehr reichen jungen Menschen.</ta>
            <ta e="T54" id="Seg_5831" s="T49">Er schickte mich, um um deine Tochter zu werben.</ta>
            <ta e="T62" id="Seg_5832" s="T54">Er wird als Brautpreis für deine Tochter die besten, ausgewähltesten Waldtiere schicken.</ta>
            <ta e="T72" id="Seg_5833" s="T62">Du sollst diese Tiere in unterschiedlichen Häusern empfangen und Nahrung vorbereiten", sagte er.</ta>
            <ta e="T75" id="Seg_5834" s="T72">Der Zar Donner war einverstanden.</ta>
            <ta e="T79" id="Seg_5835" s="T75">Der Betrügerfuchs lief zurück.</ta>
            <ta e="T84" id="Seg_5836" s="T79">Er traf unterschiedliche Waldtiere und sagte:</ta>
            <ta e="T91" id="Seg_5837" s="T84">"Der Zar Donner ist reich und großzügig und bereitet euch Nahrung vor.</ta>
            <ta e="T95" id="Seg_5838" s="T91">Man sagt wohl, dass ihr probieren wollt."</ta>
            <ta e="T101" id="Seg_5839" s="T95">Viele Tiere kamen in Herden zum Probieren.</ta>
            <ta e="T109" id="Seg_5840" s="T101">Der Zar Donner freute sich und dachte: "Mein Schwiegersohn ist wohl reich".</ta>
            <ta e="T116" id="Seg_5841" s="T109">Während die Tiere aßen, wurden diese Häuser verriegelt.</ta>
            <ta e="T121" id="Seg_5842" s="T116">Der Betrügerfuchs kam zum Zaren Donner.</ta>
            <ta e="T125" id="Seg_5843" s="T121">"Na, sind die Tiere gekommen?"</ta>
            <ta e="T126" id="Seg_5844" s="T125">"Sie sind gekommen."</ta>
            <ta e="T130" id="Seg_5845" s="T126">"Der Schwiegersohn ist sehr beschäftigt.</ta>
            <ta e="T135" id="Seg_5846" s="T130">Er wird bald mit einem goldenen Schiff kommen", sagte er.</ta>
            <ta e="T139" id="Seg_5847" s="T135">Er lief zu dem Jungen zurück.</ta>
            <ta e="T143" id="Seg_5848" s="T139">Aus Weidensträuchen ließ er ein schlechtes Floß machen.</ta>
            <ta e="T152" id="Seg_5849" s="T143">Er befahl es mit unterschiedlichen Feldblumen zu verzieren, damit es aus der Ferne schön und elegant aussieht.</ta>
            <ta e="T157" id="Seg_5850" s="T152">An einem windigen Tag fuhren sie zum Zaren Donner.</ta>
            <ta e="T167" id="Seg_5851" s="T157">Der Zar Donner sah, dass über das Meer ein goldenes Schiff fuhr, elegant und farbenfroh.</ta>
            <ta e="T169" id="Seg_5852" s="T167">Der Zar freute sich.</ta>
            <ta e="T174" id="Seg_5853" s="T169">Als Wind aufkam, wurde das Floß auseinander gerissen.</ta>
            <ta e="T181" id="Seg_5854" s="T174">Der Betrügerfuchs lief zum Zaren Donner und sagte:</ta>
            <ta e="T186" id="Seg_5855" s="T181">"Es ist ein Unglück geschehen, das goldene Schiff ist gesunken.</ta>
            <ta e="T198" id="Seg_5856" s="T186">Dein Schwiegersohn, ein kühner Mensch, hat überlebt, aber sein Hab und Gut sind versunken, er selbst ist nackt.</ta>
            <ta e="T200" id="Seg_5857" s="T198">Schick Kleidung."</ta>
            <ta e="T208" id="Seg_5858" s="T200">Der Zar Donner schickte dem Jungen die beste Kleidung und sein eigenes Schiff.</ta>
            <ta e="T212" id="Seg_5859" s="T208">Der Betrügerfuchs riet dem Jungen:</ta>
            <ta e="T217" id="Seg_5860" s="T212">"Wenn du zum Zaren kommst, schau dir deine Kleidung nicht an, sie werden erstaunt sein.</ta>
            <ta e="T221" id="Seg_5861" s="T217">Grüße nur mit einem Knicks.</ta>
            <ta e="T230" id="Seg_5862" s="T221">Guck beim Essen, wie die anderen essen, und iss auch so, trink nur wenig Wein."</ta>
            <ta e="T238" id="Seg_5863" s="T230">Der Junge kam zum Zaren Donner auf dessen Schiff, in dessen Kleidung.</ta>
            <ta e="T241" id="Seg_5864" s="T238">Die Hochzeit war sehr ausschweifend.</ta>
            <ta e="T243" id="Seg_5865" s="T241">Mit dem Schiff kehrten sie auch zurück.</ta>
            <ta e="T246" id="Seg_5866" s="T243">Der Betrügerfuchs sprach:</ta>
            <ta e="T260" id="Seg_5867" s="T246">"Fahr an deiner Erdhütte vorbei, wenn du an die goldene Stadt eines fremden Zaren kommst, halte dort und lass mich rufen.</ta>
            <ta e="T268" id="Seg_5868" s="T260">Sage: "Komm, um herauszufinden, wie mein Volk lebt" zu mir."</ta>
            <ta e="T275" id="Seg_5869" s="T268">Wie es der Fuchs gesagt hatte, fuhr der Junge an seiner Erdhütte vorbei.</ta>
            <ta e="T280" id="Seg_5870" s="T275">Er näherte sich der goldenen Stadt eines fremden Zaren.</ta>
            <ta e="T283" id="Seg_5871" s="T280">Er hielt an und sagte:</ta>
            <ta e="T289" id="Seg_5872" s="T283">"Ich möchte wissen, wie mein Volk lebt.</ta>
            <ta e="T296" id="Seg_5873" s="T289">Wohin mein Fuchs gegangen ist, er wird es herausfinden und kommen", sagte er.</ta>
            <ta e="T300" id="Seg_5874" s="T296">Als der Betrügerfuchs kam, sprach er es.</ta>
            <ta e="T307" id="Seg_5875" s="T300">Der Betrügerfuchs ging und sagte zum Zaren der goldenen Stadt :</ta>
            <ta e="T312" id="Seg_5876" s="T307">"Der Zar Donner kommt, um euch zu vernichten.</ta>
            <ta e="T314" id="Seg_5877" s="T312">Flieht lieber.</ta>
            <ta e="T322" id="Seg_5878" s="T314">Versteckt euch in diesem Wald, sodass nur eure Köpfe zu sehen sind."</ta>
            <ta e="T332" id="Seg_5879" s="T322">Das ganze Volk des Zaren der goldenen Stadt floh in den Wald, da es sich vor dem Zaren Donner fürchtete.</ta>
            <ta e="T337" id="Seg_5880" s="T332">Der Betrügerfuchs kam weinend zu dem Jungen:</ta>
            <ta e="T341" id="Seg_5881" s="T337">"Unser Volk wurde von dem Zaren der bösen Geister vernichtet.</ta>
            <ta e="T347" id="Seg_5882" s="T341">Sie haben alle gegessen und sich dann in diesem Wald versteckt.</ta>
            <ta e="T350" id="Seg_5883" s="T347">Bitte deinen Schwiegervater — er soll es blitzen lassen."</ta>
            <ta e="T355" id="Seg_5884" s="T350">Der Junge bat seinen Schwiegervater und sagte "Lass es blitzen".</ta>
            <ta e="T368" id="Seg_5885" s="T355">Da ließ der Zar des Donners einen Blitz in den Wald einschlagen, nicht ein Mensch aus der goldenen Stadt überlebte.</ta>
            <ta e="T376" id="Seg_5886" s="T368">Der Junge fing an in dieser Stadt zu leben und wurde Zar.</ta>
            <ta e="T381" id="Seg_5887" s="T376">So heiratete jener die Tochter des Zaren Donner.</ta>
            <ta e="T382" id="Seg_5888" s="T381">Ende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_5889" s="T0">Жил нищий крестьянин.</ta>
            <ta e="T12" id="Seg_5890" s="T3">Когда этот крестьянин помер, в земляном чуме-голомо остался сын сиротой.</ta>
            <ta e="T17" id="Seg_5891" s="T12">Этот мальчик от голода пропадал.</ta>
            <ta e="T20" id="Seg_5892" s="T17">Нашла его лиса.</ta>
            <ta e="T25" id="Seg_5893" s="T20">"Я тебя человеком сделаю", сказала.</ta>
            <ta e="T33" id="Seg_5894" s="T25">"Станешь богатым, женю тебя на богатой женщине."</ta>
            <ta e="T40" id="Seg_5895" s="T33">Сказав это, лиса пошла к царю Грому.</ta>
            <ta e="T44" id="Seg_5896" s="T40">Пришла к царю Грому и сказала:</ta>
            <ta e="T49" id="Seg_5897" s="T44">"Есть один очень богатый юноша.</ta>
            <ta e="T54" id="Seg_5898" s="T49">Он послал меня сватать твою дочь.</ta>
            <ta e="T62" id="Seg_5899" s="T54">Обещает калым прислать: лесных зверей самых отборных.</ta>
            <ta e="T72" id="Seg_5900" s="T62">Пусть примут тех зверей в отдельных домах, приготовив для них еду", сказала.</ta>
            <ta e="T75" id="Seg_5901" s="T72">Царь Гром согласился.</ta>
            <ta e="T79" id="Seg_5902" s="T75">Лиса-плутовка обратно побежала.</ta>
            <ta e="T84" id="Seg_5903" s="T79">Встречая разных лесных зверей, говорила:</ta>
            <ta e="T91" id="Seg_5904" s="T84">"Царь Гром такой богатый и щедрый, что приготовит для всех вас пищу.</ta>
            <ta e="T95" id="Seg_5905" s="T91">Зовет, чтоб отведали."</ta>
            <ta e="T101" id="Seg_5906" s="T95">Множество зверей стадами и стаями пришли отведать.</ta>
            <ta e="T109" id="Seg_5907" s="T101">Царь Гром обрадовался, что попался богатый зять.</ta>
            <ta e="T116" id="Seg_5908" s="T109">Когда звери ели, заперли те дома.</ta>
            <ta e="T121" id="Seg_5909" s="T116">Лиса-плутовка пришел к царю Грому.</ta>
            <ta e="T125" id="Seg_5910" s="T121">"Ну, пришли звери?"</ta>
            <ta e="T126" id="Seg_5911" s="T125">"Пришли."</ta>
            <ta e="T130" id="Seg_5912" s="T126">"Зять пока занят.</ta>
            <ta e="T135" id="Seg_5913" s="T130">Скоро прибудет на золотом корабле", сказала.</ta>
            <ta e="T139" id="Seg_5914" s="T135">Прибежала обратно к парню.</ta>
            <ta e="T143" id="Seg_5915" s="T139">Велела из тальника сколотить плохонький плотик.</ta>
            <ta e="T152" id="Seg_5916" s="T143">Велела украсить разными полевыми цветами, чтоб издали казался красивым и нарядным.</ta>
            <ta e="T157" id="Seg_5917" s="T152">В ветреный день плыли к царю Грому.</ta>
            <ta e="T167" id="Seg_5918" s="T157">Увидел царь Гром: по морю плывет золотой корабль, нарядный и цветной.</ta>
            <ta e="T169" id="Seg_5919" s="T167">Царь обрадовался.</ta>
            <ta e="T174" id="Seg_5920" s="T169">Когда поднялся ветер, плот развалился.</ta>
            <ta e="T181" id="Seg_5921" s="T174">Прибежала лиса-плутовка к царю Грому и сказала:</ta>
            <ta e="T186" id="Seg_5922" s="T181">"Случилась беда, потонул золотой корабль.</ta>
            <ta e="T198" id="Seg_5923" s="T186">Твой зять, человек отважный, спасся, а все имущество пропало, сам остался голым.</ta>
            <ta e="T200" id="Seg_5924" s="T198">Пришли одежду."</ta>
            <ta e="T208" id="Seg_5925" s="T200">Царь Гром послал парню лучшую одежду и свой корабль.</ta>
            <ta e="T212" id="Seg_5926" s="T208">Лиса-плутовка советовал парню:</ta>
            <ta e="T217" id="Seg_5927" s="T212">"У царя своей одежды не разглядывай, приметят.</ta>
            <ta e="T221" id="Seg_5928" s="T217">Здоровайся только поклоном.</ta>
            <ta e="T230" id="Seg_5929" s="T221">Во время еды примечай, как едят другие, вина пей совсем мало."</ta>
            <ta e="T238" id="Seg_5930" s="T230">Приехал парень к царю Грому на его корабле, в его одежде.</ta>
            <ta e="T241" id="Seg_5931" s="T238">Устроили богатую свадьбу.</ta>
            <ta e="T243" id="Seg_5932" s="T241">На том корабле и возвратились.</ta>
            <ta e="T246" id="Seg_5933" s="T243">Лиса-плутовка сказала:</ta>
            <ta e="T260" id="Seg_5934" s="T246">"Не останавливайся у своей землянки, остановись неподалеку от золотого города чужого царя и вели позвать меня.</ta>
            <ta e="T268" id="Seg_5935" s="T260">Мне прикажешь сходить узнать, как живется моему народу."</ta>
            <ta e="T275" id="Seg_5936" s="T268">Как сказала лиса, парень проехал мимо своего голомо.</ta>
            <ta e="T280" id="Seg_5937" s="T275">Приближался к золотому городу чужого царя.</ta>
            <ta e="T283" id="Seg_5938" s="T280">Останавился и сказал:</ta>
            <ta e="T289" id="Seg_5939" s="T283">"Хочу узнать, как живется моему народу.</ta>
            <ta e="T296" id="Seg_5940" s="T289">Где пропала моя лиса, разузнала бы", сказала.</ta>
            <ta e="T300" id="Seg_5941" s="T296">Когда пришла лиса-плутовка, отдал приказание.</ta>
            <ta e="T307" id="Seg_5942" s="T300">Лиса-плутовка прибежала и сказала царю золотого города:</ta>
            <ta e="T312" id="Seg_5943" s="T307">"Царь Гром идет вас уничтожить.</ta>
            <ta e="T314" id="Seg_5944" s="T312">Быстрее убегайте.</ta>
            <ta e="T322" id="Seg_5945" s="T314">Спрячьтесь в том лесу, чтоб только головы были видны."</ta>
            <ta e="T332" id="Seg_5946" s="T322">Весь народ царя золотого города убежал в лес, испугавшись царя Грома.</ta>
            <ta e="T337" id="Seg_5947" s="T332">Лиса-плутовка к парню возвратилась с плачем:</ta>
            <ta e="T341" id="Seg_5948" s="T337">"Народ наш истребил царь злых духов.</ta>
            <ta e="T347" id="Seg_5949" s="T341">Всех съев, спрятались в том лесу.</ta>
            <ta e="T350" id="Seg_5950" s="T347">Попроси тестя — пусть поразит их молнией."</ta>
            <ta e="T355" id="Seg_5951" s="T350">Парень просит тестя ударить [молнией].</ta>
            <ta e="T368" id="Seg_5952" s="T355">Вот царь Гром ударил [молнией] в тот лес — ни одного человека из золотого города не осталось в живых.</ta>
            <ta e="T376" id="Seg_5953" s="T368">Парень поселился в том золотом городе, стал царем.</ta>
            <ta e="T381" id="Seg_5954" s="T376">Так и женился [он] на дочери царя Грома.</ta>
            <ta e="T382" id="Seg_5955" s="T381">Конец.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_5956" s="T0">Жил нищий крестьянин.</ta>
            <ta e="T12" id="Seg_5957" s="T3">Когда этот крестьянин помер, в земляном чуме-голомо остался сын сиротой.</ta>
            <ta e="T17" id="Seg_5958" s="T12">Этот мальчик от голода пропадал.</ta>
            <ta e="T20" id="Seg_5959" s="T17">Нашла его лиса.</ta>
            <ta e="T25" id="Seg_5960" s="T20">— Я тебя человеком сделаю, — говорит.</ta>
            <ta e="T33" id="Seg_5961" s="T25">— Станешь богатым, женю тебя на богатой женщине.</ta>
            <ta e="T40" id="Seg_5962" s="T33">Сказав это, лиса пошла к царю Грому.</ta>
            <ta e="T44" id="Seg_5963" s="T40">Говорит царю Грому:</ta>
            <ta e="T49" id="Seg_5964" s="T44">— Есть один очень богатый юноша.</ta>
            <ta e="T54" id="Seg_5965" s="T49">Он послал меня сватать твою дочь.</ta>
            <ta e="T62" id="Seg_5966" s="T54">Обещает калым прислать: лесных зверей самых отборных.</ta>
            <ta e="T72" id="Seg_5967" s="T62">Пусть примут тех зверей в отдельных домах, приготовив для них еду, — сказала.</ta>
            <ta e="T75" id="Seg_5968" s="T72">Царь Гром согласился.</ta>
            <ta e="T79" id="Seg_5969" s="T75">Лиса-плутовка обратно побежала.</ta>
            <ta e="T84" id="Seg_5970" s="T79">Встречая разных лесных зверей, говорила:</ta>
            <ta e="T91" id="Seg_5971" s="T84">— Царь Гром такой богатый и щедрый, что велел для всех вас приготовить пищу.</ta>
            <ta e="T95" id="Seg_5972" s="T91">Зовет, чтоб отведали.</ta>
            <ta e="T101" id="Seg_5973" s="T95">Множество зверей стадами и стаями идет отведать.</ta>
            <ta e="T109" id="Seg_5974" s="T101">Царь Гром радуется, что попался богатый зять.</ta>
            <ta e="T116" id="Seg_5975" s="T109">Когда звери ели, заперли те дома.</ta>
            <ta e="T121" id="Seg_5976" s="T116">Лиса-плутовка приходит к царю Грому.</ta>
            <ta e="T125" id="Seg_5977" s="T121">— Ну, пришли звери?</ta>
            <ta e="T126" id="Seg_5978" s="T125">— Пришли.</ta>
            <ta e="T130" id="Seg_5979" s="T126">— Жених пока занят.</ta>
            <ta e="T135" id="Seg_5980" s="T130">Скоро прибудет на золотом корабле, — говорит.</ta>
            <ta e="T139" id="Seg_5981" s="T135">Прибегает обратно к парню.</ta>
            <ta e="T143" id="Seg_5982" s="T139">Велит из тальника сколотить плохонький плотик.</ta>
            <ta e="T152" id="Seg_5983" s="T143">Велит украсить разными полевыми цветами, чтоб издали казался красивым и нарядным.</ta>
            <ta e="T157" id="Seg_5984" s="T152">В ветреный день плывут к царю Грому.</ta>
            <ta e="T167" id="Seg_5985" s="T157">Видит царь Гром: по морю плывет золотой корабль, нарядный такой.</ta>
            <ta e="T169" id="Seg_5986" s="T167">Царь радуется.</ta>
            <ta e="T174" id="Seg_5987" s="T169">Когда поднялся ветер, плот развалился.</ta>
            <ta e="T181" id="Seg_5988" s="T174">Прибегает лиса-плутовка к царю Грому и говорит.</ta>
            <ta e="T186" id="Seg_5989" s="T181">— Случилась беда, потонул золотой корабль.</ta>
            <ta e="T198" id="Seg_5990" s="T186">Жених, человек отважный, спасся, а все имущество пропало, сам остался голым.</ta>
            <ta e="T200" id="Seg_5991" s="T198">Пришлите одежду.</ta>
            <ta e="T208" id="Seg_5992" s="T200">Царь Гром посылает парню лучшую одежду и свой корабль.</ta>
            <ta e="T212" id="Seg_5993" s="T208">Лиса-плутовка советует парню:</ta>
            <ta e="T217" id="Seg_5994" s="T212">— У царя своей одежды не разглядывай, приметят.</ta>
            <ta e="T221" id="Seg_5995" s="T217">Здоровайся только кивком.</ta>
            <ta e="T230" id="Seg_5996" s="T221">Во время еды примечай, как едят другие, вина пей совсем мало.</ta>
            <ta e="T238" id="Seg_5997" s="T230">Приезжает парень к царю Грому на его корабле, в его одежде.</ta>
            <ta e="T241" id="Seg_5998" s="T238">Устроили богатую свадьбу.</ta>
            <ta e="T243" id="Seg_5999" s="T241">На том корабле и возвращаются.</ta>
            <ta e="T246" id="Seg_6000" s="T243">Лиса-плутовка говорит парню:</ta>
            <ta e="T260" id="Seg_6001" s="T246">— Не останавливайся у своего земляного чума-голомо, остановись неподалеку от золотого города чужого царя и вели позвать меня.</ta>
            <ta e="T268" id="Seg_6002" s="T260">Мне прикажешь сходить узнать, как живется моему народу.</ta>
            <ta e="T275" id="Seg_6003" s="T268">Как советовала лиса, парень проезжает мимо своего голомо.</ta>
            <ta e="T280" id="Seg_6004" s="T275">Приближается к золотому городу чужого царя.</ta>
            <ta e="T283" id="Seg_6005" s="T280">Останавливается и говорит:</ta>
            <ta e="T289" id="Seg_6006" s="T283">— Хочу узнать, как живется моему народу.</ta>
            <ta e="T296" id="Seg_6007" s="T289">Где пропала моя лиса, разузнала бы, — говорит.</ta>
            <ta e="T300" id="Seg_6008" s="T296">Когда пришла лиса-плутовка, отдает приказание.</ta>
            <ta e="T307" id="Seg_6009" s="T300">Лиса-плутовка прибегает к царю золотого города и говорит:</ta>
            <ta e="T312" id="Seg_6010" s="T307">— Царь Гром идет вас уничтожить.</ta>
            <ta e="T314" id="Seg_6011" s="T312">Быстрее убегайте.</ta>
            <ta e="T322" id="Seg_6012" s="T314">Спрячьтесь в том лесу, чтоб только головы были видны.</ta>
            <ta e="T332" id="Seg_6013" s="T322">Весь народ царя золотого города убежал в лес, испугавшись царя Грома.</ta>
            <ta e="T337" id="Seg_6014" s="T332">Лиса-плутовка к парню возвращается с плачем:</ta>
            <ta e="T341" id="Seg_6015" s="T337">— Народ наш истребили люди царя Абаасы.</ta>
            <ta e="T347" id="Seg_6016" s="T341">Всех съев, спрятались в том лесу.</ta>
            <ta e="T350" id="Seg_6017" s="T347">Попроси тестя — пусть поразит их молнией.</ta>
            <ta e="T355" id="Seg_6018" s="T350">Парень просит тестя ударить [молнией].</ta>
            <ta e="T368" id="Seg_6019" s="T355">Вот царь Гром ударил [молнией] в тот лес — ни одного человека из золотого города не осталось в живых.</ta>
            <ta e="T376" id="Seg_6020" s="T368">Парень поселился в том золотом городе, стал царем.</ta>
            <ta e="T381" id="Seg_6021" s="T376">Так и женился [он] на дочери царя Грома.</ta>
            <ta e="T382" id="Seg_6022" s="T381">Конец.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
