<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDA844B133-9201-F45A-27C8-53CDA96927C9">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoXN_19701118_Chopochuka_flk</transcription-name>
         <referenced-file url="PoXN_19701118_Chopochuka_flk.wav" />
         <referenced-file url="PoXN_19701118_Chopochuka_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\PoXN_19701118_Chopochuka_flk\PoXN_19701118_Chopochuka_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1098</ud-information>
            <ud-information attribute-name="# HIAT:w">721</ud-information>
            <ud-information attribute-name="# e">727</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">6</ud-information>
            <ud-information attribute-name="# HIAT:u">115</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoXN">
            <abbreviation>PoXN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="1.982" type="appl" />
         <tli id="T2" time="3.965" type="appl" />
         <tli id="T3" time="5.948" type="appl" />
         <tli id="T4" time="7.803334594584831" />
         <tli id="T5" time="8.799" type="appl" />
         <tli id="T6" time="9.669" type="appl" />
         <tli id="T7" time="10.538" type="appl" />
         <tli id="T8" time="11.407" type="appl" />
         <tli id="T9" time="12.277" type="appl" />
         <tli id="T10" time="13.146" type="appl" />
         <tli id="T11" time="14.016" type="appl" />
         <tli id="T12" time="15.098330954192217" />
         <tli id="T13" time="15.474" type="appl" />
         <tli id="T14" time="16.062" type="appl" />
         <tli id="T15" time="16.651" type="appl" />
         <tli id="T16" time="18.086435699118564" />
         <tli id="T17" time="18.162" type="appl" />
         <tli id="T18" time="19.083" type="appl" />
         <tli id="T19" time="20.005" type="appl" />
         <tli id="T20" time="20.927" type="appl" />
         <tli id="T21" time="21.848" type="appl" />
         <tli id="T22" time="23.359701691747677" />
         <tli id="T23" time="23.59" type="appl" />
         <tli id="T24" time="24.411" type="appl" />
         <tli id="T25" time="25.231" type="appl" />
         <tli id="T26" time="26.052" type="appl" />
         <tli id="T27" time="26.872" type="appl" />
         <tli id="T28" time="27.693" type="appl" />
         <tli id="T29" time="28.513" type="appl" />
         <tli id="T30" time="29.334" type="appl" />
         <tli id="T31" time="30.972937802471375" />
         <tli id="T32" time="31.156" type="appl" />
         <tli id="T33" time="32.157" type="appl" />
         <tli id="T34" time="33.158" type="appl" />
         <tli id="T35" time="34.16" type="appl" />
         <tli id="T36" time="34.821" type="appl" />
         <tli id="T37" time="35.482" type="appl" />
         <tli id="T38" time="36.143" type="appl" />
         <tli id="T39" time="36.804" type="appl" />
         <tli id="T40" time="37.464" type="appl" />
         <tli id="T41" time="38.125" type="appl" />
         <tli id="T42" time="38.786" type="appl" />
         <tli id="T43" time="39.447" type="appl" />
         <tli id="T44" time="40.7328131668317" />
         <tli id="T45" time="40.792" type="appl" />
         <tli id="T46" time="41.477" type="appl" />
         <tli id="T47" time="42.161" type="appl" />
         <tli id="T48" time="42.845" type="appl" />
         <tli id="T49" time="43.53" type="appl" />
         <tli id="T50" time="44.214" type="appl" />
         <tli id="T51" time="44.898" type="appl" />
         <tli id="T52" time="45.582" type="appl" />
         <tli id="T53" time="46.267" type="appl" />
         <tli id="T54" time="46.951" type="appl" />
         <tli id="T55" time="47.635" type="appl" />
         <tli id="T56" time="48.32" type="appl" />
         <tli id="T57" time="49.11733174582609" />
         <tli id="T58" time="49.589" type="appl" />
         <tli id="T59" time="50.175" type="appl" />
         <tli id="T60" time="50.76" type="appl" />
         <tli id="T61" time="51.345" type="appl" />
         <tli id="T62" time="51.93" type="appl" />
         <tli id="T63" time="52.516" type="appl" />
         <tli id="T64" time="53.101" type="appl" />
         <tli id="T65" time="53.638" type="appl" />
         <tli id="T66" time="54.174" type="appl" />
         <tli id="T67" time="54.71" type="appl" />
         <tli id="T68" time="55.247" type="appl" />
         <tli id="T69" time="55.784" type="appl" />
         <tli id="T70" time="56.92593971056889" />
         <tli id="T71" time="57.163" type="appl" />
         <tli id="T72" time="58.007" type="appl" />
         <tli id="T73" time="58.85" type="appl" />
         <tli id="T74" time="59.693" type="appl" />
         <tli id="T75" time="60.537" type="appl" />
         <tli id="T76" time="61.91920927881062" />
         <tli id="T77" time="61.977" type="appl" />
         <tli id="T78" time="62.573" type="appl" />
         <tli id="T79" time="63.17" type="appl" />
         <tli id="T80" time="63.767" type="appl" />
         <tli id="T81" time="64.363" type="appl" />
         <tli id="T82" time="64.96" type="appl" />
         <tli id="T83" time="65.557" type="appl" />
         <tli id="T84" time="66.153" type="appl" />
         <tli id="T85" time="66.71000096398706" />
         <tli id="T86" time="67.863" type="appl" />
         <tli id="T87" time="68.976" type="appl" />
         <tli id="T88" time="70.09" type="appl" />
         <tli id="T89" time="71.24299646210778" />
         <tli id="T90" time="71.718" type="appl" />
         <tli id="T91" time="72.232" type="appl" />
         <tli id="T92" time="72.747" type="appl" />
         <tli id="T93" time="73.262" type="appl" />
         <tli id="T94" time="73.777" type="appl" />
         <tli id="T95" time="74.292" type="appl" />
         <tli id="T96" time="74.806" type="appl" />
         <tli id="T97" time="75.321" type="appl" />
         <tli id="T98" time="75.836" type="appl" />
         <tli id="T99" time="76.35" type="appl" />
         <tli id="T100" time="76.865" type="appl" />
         <tli id="T101" time="77.38" type="appl" />
         <tli id="T102" time="77.768" type="appl" />
         <tli id="T103" time="78.156" type="appl" />
         <tli id="T104" time="78.544" type="appl" />
         <tli id="T105" time="78.932" type="appl" />
         <tli id="T106" time="79.32" type="appl" />
         <tli id="T107" time="79.708" type="appl" />
         <tli id="T108" time="80.096" type="appl" />
         <tli id="T109" time="80.484" type="appl" />
         <tli id="T110" time="80.872" type="appl" />
         <tli id="T111" time="81.17333188924096" />
         <tli id="T112" time="81.953" type="appl" />
         <tli id="T113" time="82.645" type="appl" />
         <tli id="T114" time="83.338" type="appl" />
         <tli id="T115" time="84.031" type="appl" />
         <tli id="T116" time="84.724" type="appl" />
         <tli id="T117" time="85.416" type="appl" />
         <tli id="T118" time="86.109" type="appl" />
         <tli id="T119" time="86.725" type="appl" />
         <tli id="T120" time="87.34" type="appl" />
         <tli id="T121" time="87.956" type="appl" />
         <tli id="T122" time="88.572" type="appl" />
         <tli id="T123" time="89.187" type="appl" />
         <tli id="T124" time="89.803" type="appl" />
         <tli id="T125" time="90.419" type="appl" />
         <tli id="T126" time="91.034" type="appl" />
         <tli id="T127" time="91.65" type="appl" />
         <tli id="T128" time="92.057" type="appl" />
         <tli id="T129" time="92.465" type="appl" />
         <tli id="T130" time="92.872" type="appl" />
         <tli id="T131" time="93.28" type="appl" />
         <tli id="T132" time="93.687" type="appl" />
         <tli id="T133" time="94.094" type="appl" />
         <tli id="T134" time="94.502" type="appl" />
         <tli id="T135" time="94.909" type="appl" />
         <tli id="T136" time="95.992" type="appl" />
         <tli id="T137" time="97.076" type="appl" />
         <tli id="T138" time="98.159" type="appl" />
         <tli id="T139" time="99.042" type="appl" />
         <tli id="T140" time="99.924" type="appl" />
         <tli id="T141" time="100.807" type="appl" />
         <tli id="T142" time="101.289" type="appl" />
         <tli id="T143" time="101.771" type="appl" />
         <tli id="T144" time="102.254" type="appl" />
         <tli id="T145" time="102.736" type="appl" />
         <tli id="T146" time="103.218" type="appl" />
         <tli id="T147" time="103.55999913184107" />
         <tli id="T148" time="104.217" type="appl" />
         <tli id="T149" time="104.735" type="appl" />
         <tli id="T150" time="105.252" type="appl" />
         <tli id="T151" time="105.769" type="appl" />
         <tli id="T152" time="106.287" type="appl" />
         <tli id="T153" time="106.804" type="appl" />
         <tli id="T154" time="107.321" type="appl" />
         <tli id="T155" time="107.839" type="appl" />
         <tli id="T156" time="108.356" type="appl" />
         <tli id="T157" time="108.873" type="appl" />
         <tli id="T158" time="109.391" type="appl" />
         <tli id="T159" time="109.908" type="appl" />
         <tli id="T160" time="110.425" type="appl" />
         <tli id="T161" time="110.943" type="appl" />
         <tli id="T162" time="112.29189934240806" />
         <tli id="T163" time="112.802" type="appl" />
         <tli id="T164" time="114.145" type="appl" />
         <tli id="T165" time="115.40033620952231" />
         <tli id="T166" time="116.178" type="appl" />
         <tli id="T167" time="116.868" type="appl" />
         <tli id="T168" time="117.559" type="appl" />
         <tli id="T169" time="118.249" type="appl" />
         <tli id="T170" time="119.20000122867589" />
         <tli id="T171" time="119.473" type="appl" />
         <tli id="T172" time="120.006" type="appl" />
         <tli id="T173" time="120.54" type="appl" />
         <tli id="T174" time="121.073" type="appl" />
         <tli id="T175" time="121.606" type="appl" />
         <tli id="T176" time="122.139" type="appl" />
         <tli id="T177" time="122.672" type="appl" />
         <tli id="T178" time="123.206" type="appl" />
         <tli id="T179" time="123.739" type="appl" />
         <tli id="T180" time="124.20532793461213" />
         <tli id="T181" time="124.855" type="appl" />
         <tli id="T182" time="125.438" type="appl" />
         <tli id="T183" time="126.021" type="appl" />
         <tli id="T184" time="126.604" type="appl" />
         <tli id="T185" time="127.187" type="appl" />
         <tli id="T186" time="127.62333116099956" />
         <tli id="T187" time="128.152" type="appl" />
         <tli id="T188" time="128.535" type="appl" />
         <tli id="T189" time="128.918" type="appl" />
         <tli id="T190" time="129.15333766416475" />
         <tli id="T191" time="129.867" type="appl" />
         <tli id="T192" time="130.434" type="appl" />
         <tli id="T193" time="131.001" type="appl" />
         <tli id="T194" time="131.568" type="appl" />
         <tli id="T195" time="132.135" type="appl" />
         <tli id="T196" time="132.702" type="appl" />
         <tli id="T197" time="134.22495258608666" />
         <tli id="T198" time="134.3524762930433" type="intp" />
         <tli id="T199" time="134.48" type="appl" />
         <tli id="T200" time="135.086" type="appl" />
         <tli id="T201" time="135.692" type="appl" />
         <tli id="T202" time="136.298" type="appl" />
         <tli id="T203" time="136.904" type="appl" />
         <tli id="T204" time="137.509" type="appl" />
         <tli id="T205" time="138.115" type="appl" />
         <tli id="T206" time="138.829" type="appl" />
         <tli id="T207" time="139.543" type="appl" />
         <tli id="T208" time="140.256" type="appl" />
         <tli id="T209" time="140.97" type="appl" />
         <tli id="T210" time="141.608" type="appl" />
         <tli id="T211" time="142.245" type="appl" />
         <tli id="T212" time="142.883" type="appl" />
         <tli id="T213" time="143.378" type="appl" />
         <tli id="T214" time="143.874" type="appl" />
         <tli id="T215" time="144.369" type="appl" />
         <tli id="T216" time="144.865" type="appl" />
         <tli id="T217" time="145.2133409079142" />
         <tli id="T218" time="145.778" type="appl" />
         <tli id="T219" time="146.195" type="appl" />
         <tli id="T220" time="146.612" type="appl" />
         <tli id="T221" time="147.03" type="appl" />
         <tli id="T222" time="147.448" type="appl" />
         <tli id="T223" time="147.865" type="appl" />
         <tli id="T224" time="148.282" type="appl" />
         <tli id="T225" time="148.5400041548988" />
         <tli id="T226" time="149.318" type="appl" />
         <tli id="T227" time="149.936" type="appl" />
         <tli id="T228" time="150.554" type="appl" />
         <tli id="T229" time="151.52473166430732" />
         <tli id="T230" time="151.928" type="appl" />
         <tli id="T231" time="152.684" type="appl" />
         <tli id="T232" time="153.38666101208204" />
         <tli id="T233" time="153.988" type="appl" />
         <tli id="T234" time="154.537" type="appl" />
         <tli id="T235" time="155.085" type="appl" />
         <tli id="T236" time="155.633" type="appl" />
         <tli id="T237" time="156.182" type="appl" />
         <tli id="T238" time="157.2779915273148" />
         <tli id="T239" time="157.335" type="appl" />
         <tli id="T240" time="157.941" type="appl" />
         <tli id="T241" time="158.546" type="appl" />
         <tli id="T242" time="159.151" type="appl" />
         <tli id="T243" time="159.757" type="appl" />
         <tli id="T244" time="160.362" type="appl" />
         <tli id="T245" time="160.77" type="appl" />
         <tli id="T246" time="161.178" type="appl" />
         <tli id="T247" time="161.586" type="appl" />
         <tli id="T248" time="161.994" type="appl" />
         <tli id="T249" time="162.402" type="appl" />
         <tli id="T250" time="162.81" type="appl" />
         <tli id="T251" time="163.204" type="appl" />
         <tli id="T252" time="163.598" type="appl" />
         <tli id="T253" time="163.992" type="appl" />
         <tli id="T254" time="164.386" type="appl" />
         <tli id="T255" time="164.67333979551162" />
         <tli id="T256" time="165.223" type="appl" />
         <tli id="T257" time="165.667" type="appl" />
         <tli id="T258" time="166.11" type="appl" />
         <tli id="T259" time="166.553" type="appl" />
         <tli id="T260" time="166.997" type="appl" />
         <tli id="T261" time="167.33333186852587" />
         <tli id="T262" time="167.898" type="appl" />
         <tli id="T263" time="168.357" type="appl" />
         <tli id="T264" time="168.816" type="appl" />
         <tli id="T265" time="169.274" type="appl" />
         <tli id="T266" time="169.733" type="appl" />
         <tli id="T267" time="170.95115025156554" />
         <tli id="T268" time="171.179" type="appl" />
         <tli id="T269" time="172.167" type="appl" />
         <tli id="T270" time="173.154" type="appl" />
         <tli id="T271" time="174.142" type="appl" />
         <tli id="T272" time="175.13" type="appl" />
         <tli id="T273" time="175.598" type="appl" />
         <tli id="T274" time="176.065" type="appl" />
         <tli id="T275" time="176.532" type="appl" />
         <tli id="T276" time="177.0" type="appl" />
         <tli id="T277" time="177.468" type="appl" />
         <tli id="T278" time="177.935" type="appl" />
         <tli id="T279" time="178.402" type="appl" />
         <tli id="T280" time="178.80332862344682" />
         <tli id="T281" time="179.556" type="appl" />
         <tli id="T282" time="180.242" type="appl" />
         <tli id="T283" time="180.928" type="appl" />
         <tli id="T284" time="181.959" type="appl" />
         <tli id="T285" time="182.989" type="appl" />
         <tli id="T286" time="183.95999454258313" />
         <tli id="T287" time="185.131" type="appl" />
         <tli id="T288" time="186.243" type="appl" />
         <tli id="T289" time="187.354" type="appl" />
         <tli id="T290" time="187.782" type="appl" />
         <tli id="T291" time="188.211" type="appl" />
         <tli id="T292" time="188.639" type="appl" />
         <tli id="T293" time="189.067" type="appl" />
         <tli id="T294" time="189.495" type="appl" />
         <tli id="T295" time="189.924" type="appl" />
         <tli id="T296" time="190.352" type="appl" />
         <tli id="T297" time="190.78" type="appl" />
         <tli id="T298" time="191.345" type="appl" />
         <tli id="T299" time="191.91" type="appl" />
         <tli id="T300" time="192.475" type="appl" />
         <tli id="T301" time="192.91999730898303" />
         <tli id="T302" time="193.638" type="appl" />
         <tli id="T303" time="194.36165858616846" />
         <tli id="T304" time="194.692" type="appl" />
         <tli id="T305" time="195.149" type="appl" />
         <tli id="T306" time="195.605" type="appl" />
         <tli id="T307" time="196.062" type="appl" />
         <tli id="T308" time="196.519" type="appl" />
         <tli id="T309" time="196.976" type="appl" />
         <tli id="T310" time="197.433" type="appl" />
         <tli id="T311" time="197.89" type="appl" />
         <tli id="T312" time="198.346" type="appl" />
         <tli id="T313" time="198.803" type="appl" />
         <tli id="T314" time="199.46665849842375" />
         <tli id="T315" time="200.191" type="appl" />
         <tli id="T316" time="201.122" type="appl" />
         <tli id="T317" time="202.053" type="appl" />
         <tli id="T318" time="202.985" type="appl" />
         <tli id="T319" time="203.916" type="appl" />
         <tli id="T320" time="205.4307099404123" />
         <tli id="T321" time="205.432" type="appl" />
         <tli id="T322" time="206.016" type="appl" />
         <tli id="T323" time="206.601" type="appl" />
         <tli id="T324" time="207.186" type="appl" />
         <tli id="T325" time="207.771" type="appl" />
         <tli id="T326" time="208.355" type="appl" />
         <tli id="T327" time="208.94" type="appl" />
         <tli id="T328" time="209.338" type="appl" />
         <tli id="T329" time="209.736" type="appl" />
         <tli id="T330" time="210.134" type="appl" />
         <tli id="T331" time="210.532" type="appl" />
         <tli id="T332" time="210.93" type="appl" />
         <tli id="T333" time="211.328" type="appl" />
         <tli id="T334" time="211.726" type="appl" />
         <tli id="T335" time="213.36394196472156" />
         <tli id="T336" time="213.51997098236077" type="intp" />
         <tli id="T337" time="213.676" type="appl" />
         <tli id="T338" time="214.452" type="appl" />
         <tli id="T339" time="215.228" type="appl" />
         <tli id="T340" time="216.004" type="appl" />
         <tli id="T341" time="217.0333352099728" />
         <tli id="T342" time="217.496" type="appl" />
         <tli id="T343" time="218.213" type="appl" />
         <tli id="T344" time="218.929" type="appl" />
         <tli id="T345" time="219.645" type="appl" />
         <tli id="T346" time="220.361" type="appl" />
         <tli id="T347" time="221.078" type="appl" />
         <tli id="T348" time="221.71399158285007" />
         <tli id="T349" time="222.236" type="appl" />
         <tli id="T350" time="222.678" type="appl" />
         <tli id="T351" time="223.12" type="appl" />
         <tli id="T352" time="223.562" type="appl" />
         <tli id="T353" time="224.004" type="appl" />
         <tli id="T354" time="224.446" type="appl" />
         <tli id="T355" time="224.888" type="appl" />
         <tli id="T356" time="225.33" type="appl" />
         <tli id="T357" time="226.054" type="appl" />
         <tli id="T358" time="226.779" type="appl" />
         <tli id="T359" time="227.5296725237274" />
         <tli id="T360" time="228.30375118024853" />
         <tli id="T361" time="228.75041214296175" />
         <tli id="T362" time="229.28373866560437" />
         <tli id="T363" time="229.6370674868551" />
         <tli id="T364" time="229.6504006499212" />
         <tli id="T365" time="230.89038481506535" />
         <tli id="T366" time="231.3837118485098" />
         <tli id="T367" time="232.030370257214" />
         <tli id="T368" time="232.1570353063416" />
         <tli id="T369" time="232.65036233978608" />
         <tli id="T370" time="233.465" type="appl" />
         <tli id="T371" time="234.2933361503352" />
         <tli id="T372" time="234.69340991140905" type="intp" />
         <tli id="T373" time="235.09348367248293" type="intp" />
         <tli id="T374" time="235.4935574335568" type="intp" />
         <tli id="T375" time="235.89363119463064" type="intp" />
         <tli id="T376" time="236.29370495570453" type="intp" />
         <tli id="T377" time="236.6937787167784" type="intp" />
         <tli id="T378" time="237.09385247785227" type="intp" />
         <tli id="T379" time="237.49392623892612" type="intp" />
         <tli id="T380" time="237.894" type="appl" />
         <tli id="T381" time="238.376" type="appl" />
         <tli id="T382" time="238.857" type="appl" />
         <tli id="T383" time="239.338" type="appl" />
         <tli id="T384" time="239.819" type="appl" />
         <tli id="T385" time="240.3" type="appl" />
         <tli id="T386" time="240.782" type="appl" />
         <tli id="T387" time="242.09690837209385" />
         <tli id="T388" time="242.145" type="appl" />
         <tli id="T389" time="243.028" type="appl" />
         <tli id="T390" time="243.91" type="appl" />
         <tli id="T391" time="244.391" type="appl" />
         <tli id="T392" time="244.872" type="appl" />
         <tli id="T393" time="245.352" type="appl" />
         <tli id="T394" time="245.833" type="appl" />
         <tli id="T395" time="247.00351238040614" />
         <tli id="T396" time="247.018" type="appl" />
         <tli id="T397" time="247.722" type="appl" />
         <tli id="T398" time="248.426" type="appl" />
         <tli id="T399" time="249.13" type="appl" />
         <tli id="T400" time="249.833" type="appl" />
         <tli id="T401" time="250.537" type="appl" />
         <tli id="T402" time="251.241" type="appl" />
         <tli id="T403" time="251.945" type="appl" />
         <tli id="T404" time="252.61" type="appl" />
         <tli id="T405" time="253.275" type="appl" />
         <tli id="T406" time="253.94" type="appl" />
         <tli id="T407" time="254.605" type="appl" />
         <tli id="T408" time="255.29666168349686" />
         <tli id="T409" time="255.925" type="appl" />
         <tli id="T410" time="256.579" type="appl" />
         <tli id="T411" time="257.234" type="appl" />
         <tli id="T412" time="257.888" type="appl" />
         <tli id="T413" time="258.543" type="appl" />
         <tli id="T414" time="259.198" type="appl" />
         <tli id="T415" time="259.852" type="appl" />
         <tli id="T416" time="260.507" type="appl" />
         <tli id="T417" time="261.151" type="appl" />
         <tli id="T418" time="261.796" type="appl" />
         <tli id="T419" time="262.17334470015686" />
         <tli id="T420" time="262.798" type="appl" />
         <tli id="T421" time="263.155" type="appl" />
         <tli id="T422" time="263.512" type="appl" />
         <tli id="T423" time="263.93665551564413" />
         <tli id="T424" time="264.565" type="appl" />
         <tli id="T425" time="265.26" type="appl" />
         <tli id="T426" time="265.955" type="appl" />
         <tli id="T427" time="266.5232631091268" />
         <tli id="T428" time="267.201" type="appl" />
         <tli id="T429" time="267.752" type="appl" />
         <tli id="T430" time="268.303" type="appl" />
         <tli id="T431" time="268.853" type="appl" />
         <tli id="T432" time="269.404" type="appl" />
         <tli id="T433" time="269.955" type="appl" />
         <tli id="T434" time="270.7632089641358" />
         <tli id="T435" time="271.313" type="appl" />
         <tli id="T436" time="272.12" type="appl" />
         <tli id="T437" time="272.884" type="appl" />
         <tli id="T438" time="273.648" type="appl" />
         <tli id="T439" time="274.411" type="appl" />
         <tli id="T440" time="275.175" type="appl" />
         <tli id="T441" time="275.939" type="appl" />
         <tli id="T442" time="276.702" type="appl" />
         <tli id="T443" time="277.466" type="appl" />
         <tli id="T444" time="277.80978564455165" />
         <tli id="T445" time="279.062" type="appl" />
         <tli id="T446" time="279.894" type="appl" />
         <tli id="T447" time="280.727" type="appl" />
         <tli id="T448" time="281.559" type="appl" />
         <tli id="T449" time="282.391" type="appl" />
         <tli id="T450" time="283.223" type="appl" />
         <tli id="T451" time="284.056" type="appl" />
         <tli id="T452" time="284.888" type="appl" />
         <tli id="T453" time="285.72" type="appl" />
         <tli id="T454" time="286.393" type="appl" />
         <tli id="T455" time="287.067" type="appl" />
         <tli id="T456" time="287.48000069555957" />
         <tli id="T457" time="288.246" type="appl" />
         <tli id="T458" time="288.751" type="appl" />
         <tli id="T459" time="289.257" type="appl" />
         <tli id="T460" time="290.26295994825733" />
         <tli id="T461" time="290.55" type="appl" />
         <tli id="T462" time="291.337" type="appl" />
         <tli id="T463" time="292.125" type="appl" />
         <tli id="T464" time="292.912" type="appl" />
         <tli id="T465" time="294.42957340640294" />
         <tli id="T466" time="294.544" type="appl" />
         <tli id="T467" time="295.388" type="appl" />
         <tli id="T468" time="296.233" type="appl" />
         <tli id="T469" time="297.078" type="appl" />
         <tli id="T470" time="297.923" type="appl" />
         <tli id="T471" time="298.767" type="appl" />
         <tli id="T472" time="299.9095034265561" />
         <tli id="T473" time="300.333" type="appl" />
         <tli id="T474" time="301.055" type="appl" />
         <tli id="T475" time="301.776" type="appl" />
         <tli id="T476" time="302.497" type="appl" />
         <tli id="T477" time="303.219" type="appl" />
         <tli id="T478" time="303.70000189610175" />
         <tli id="T479" time="304.561" type="appl" />
         <tli id="T480" time="305.182" type="appl" />
         <tli id="T481" time="305.804" type="appl" />
         <tli id="T482" time="306.425" type="appl" />
         <tli id="T483" time="307.046" type="appl" />
         <tli id="T484" time="308.29606299511164" />
         <tli id="T485" time="308.466" type="appl" />
         <tli id="T486" time="309.265" type="appl" />
         <tli id="T487" time="310.064" type="appl" />
         <tli id="T488" time="310.863" type="appl" />
         <tli id="T489" time="311.662" type="appl" />
         <tli id="T490" time="312.461" type="appl" />
         <tli id="T491" time="313.26" type="appl" />
         <tli id="T492" time="314.06" type="appl" />
         <tli id="T493" time="314.859" type="appl" />
         <tli id="T494" time="315.658" type="appl" />
         <tli id="T495" time="316.457" type="appl" />
         <tli id="T496" time="317.256" type="appl" />
         <tli id="T497" time="318.055" type="appl" />
         <tli id="T498" time="319.60258527513565" />
         <tli id="T499" time="319.606" type="appl" />
         <tli id="T500" time="320.357" type="appl" />
         <tli id="T501" time="321.108" type="appl" />
         <tli id="T502" time="321.86" type="appl" />
         <tli id="T503" time="322.612" type="appl" />
         <tli id="T504" time="323.43633840064365" />
         <tli id="T505" time="323.993" type="appl" />
         <tli id="T506" time="324.622" type="appl" />
         <tli id="T507" time="325.252" type="appl" />
         <tli id="T508" time="325.881" type="appl" />
         <tli id="T509" time="326.511" type="appl" />
         <tli id="T510" time="327.14" type="appl" />
         <tli id="T511" time="327.77" type="appl" />
         <tli id="T512" time="328.52" type="appl" />
         <tli id="T513" time="329.27" type="appl" />
         <tli id="T514" time="330.02" type="appl" />
         <tli id="T515" time="330.77" type="appl" />
         <tli id="T516" time="331.52" type="appl" />
         <tli id="T517" time="332.27" type="appl" />
         <tli id="T518" time="333.02" type="appl" />
         <tli id="T519" time="333.77" type="appl" />
         <tli id="T520" time="334.52" type="appl" />
         <tli id="T521" time="335.054" type="appl" />
         <tli id="T522" time="335.589" type="appl" />
         <tli id="T523" time="336.123" type="appl" />
         <tli id="T524" time="336.658" type="appl" />
         <tli id="T525" time="337.192" type="appl" />
         <tli id="T526" time="337.727" type="appl" />
         <tli id="T527" time="338.261" type="appl" />
         <tli id="T528" time="338.796" type="appl" />
         <tli id="T529" time="340.04899083694795" />
         <tli id="T530" time="340.45" type="appl" />
         <tli id="T531" time="341.57" type="appl" />
         <tli id="T532" time="342.69" type="appl" />
         <tli id="T533" time="344.51560046407997" />
         <tli id="T534" time="344.754" type="appl" />
         <tli id="T535" time="345.697" type="appl" />
         <tli id="T536" time="346.8822369083067" />
         <tli id="T537" time="347.457" type="appl" />
         <tli id="T538" time="348.273" type="appl" />
         <tli id="T539" time="349.088" type="appl" />
         <tli id="T540" time="349.904" type="appl" />
         <tli id="T541" time="351.30884704624066" />
         <tli id="T542" time="351.329" type="appl" />
         <tli id="T543" time="351.938" type="appl" />
         <tli id="T544" time="352.547" type="appl" />
         <tli id="T545" time="353.156" type="appl" />
         <tli id="T546" time="353.765" type="appl" />
         <tli id="T547" time="354.374" type="appl" />
         <tli id="T548" time="355.9487877932316" />
         <tli id="T549" time="355.98" type="appl" />
         <tli id="T550" time="356.978" type="appl" />
         <tli id="T551" time="357.9950116625269" />
         <tli id="T552" time="358.621" type="appl" />
         <tli id="T553" time="359.266" type="appl" />
         <tli id="T554" time="359.912" type="appl" />
         <tli id="T555" time="360.558" type="appl" />
         <tli id="T556" time="361.203" type="appl" />
         <tli id="T557" time="361.849" type="appl" />
         <tli id="T558" time="362.494" type="appl" />
         <tli id="T559" time="363.7153552792151" />
         <tli id="T560" time="363.805" type="appl" />
         <tli id="T561" time="364.47" type="appl" />
         <tli id="T562" time="365.134" type="appl" />
         <tli id="T563" time="365.799" type="appl" />
         <tli id="T564" time="366.464" type="appl" />
         <tli id="T565" time="367.8553024112286" />
         <tli id="T566" time="367.93" type="appl" />
         <tli id="T567" time="368.731" type="appl" />
         <tli id="T568" time="369.532" type="appl" />
         <tli id="T569" time="370.333" type="appl" />
         <tli id="T570" time="371.134" type="appl" />
         <tli id="T571" time="371.935" type="appl" />
         <tli id="T572" time="372.736" type="appl" />
         <tli id="T573" time="373.537" type="appl" />
         <tli id="T574" time="374.338" type="appl" />
         <tli id="T575" time="375.139" type="appl" />
         <tli id="T576" time="376.74185559476155" />
         <tli id="T577" time="376.799" type="appl" />
         <tli id="T578" time="377.659" type="appl" />
         <tli id="T579" time="378.518" type="appl" />
         <tli id="T580" time="379.378" type="appl" />
         <tli id="T581" time="380.81513691144477" />
         <tli id="T582" time="380.86" type="appl" />
         <tli id="T583" time="381.483" type="appl" />
         <tli id="T584" time="382.106" type="appl" />
         <tli id="T585" time="382.729" type="appl" />
         <tli id="T586" time="383.352" type="appl" />
         <tli id="T587" time="383.975" type="appl" />
         <tli id="T588" time="384.598" type="appl" />
         <tli id="T589" time="385.221" type="appl" />
         <tli id="T590" time="385.844" type="appl" />
         <tli id="T591" time="386.467" type="appl" />
         <tli id="T592" time="387.4150526291474" />
         <tli id="T593" time="389.26836229533063" />
         <tli id="T594" time="389.332" type="appl" />
         <tli id="T595" time="390.311" type="appl" />
         <tli id="T596" time="391.29" type="appl" />
         <tli id="T597" time="392.82831683397023" />
         <tli id="T598" time="392.893" type="appl" />
         <tli id="T599" time="393.515" type="appl" />
         <tli id="T600" time="394.138" type="appl" />
         <tli id="T601" time="394.76" type="appl" />
         <tli id="T602" time="395.383" type="appl" />
         <tli id="T603" time="396.006" type="appl" />
         <tli id="T604" time="396.628" type="appl" />
         <tli id="T605" time="397.67492160848525" />
         <tli id="T606" time="397.812" type="appl" />
         <tli id="T607" time="398.373" type="appl" />
         <tli id="T608" time="398.934" type="appl" />
         <tli id="T609" time="399.494" type="appl" />
         <tli id="T610" time="400.055" type="appl" />
         <tli id="T611" time="400.616" type="appl" />
         <tli id="T612" time="402.34819526314135" />
         <tli id="T613" time="402.5150976315707" type="intp" />
         <tli id="T614" time="402.682" type="appl" />
         <tli id="T615" time="403.435" type="appl" />
         <tli id="T616" time="404.187" type="appl" />
         <tli id="T617" time="404.8533455635173" />
         <tli id="T618" time="405.297" type="appl" />
         <tli id="T619" time="405.654" type="appl" />
         <tli id="T620" time="406.01" type="appl" />
         <tli id="T621" time="406.367" type="appl" />
         <tli id="T622" time="406.724" type="appl" />
         <tli id="T623" time="407.081" type="appl" />
         <tli id="T624" time="407.438" type="appl" />
         <tli id="T625" time="407.795" type="appl" />
         <tli id="T626" time="408.151" type="appl" />
         <tli id="T627" time="408.508" type="appl" />
         <tli id="T628" time="408.865" type="appl" />
         <tli id="T629" time="409.222" type="appl" />
         <tli id="T630" time="409.579" type="appl" />
         <tli id="T631" time="409.936" type="appl" />
         <tli id="T632" time="410.292" type="appl" />
         <tli id="T633" time="410.649" type="appl" />
         <tli id="T634" time="411.006" type="appl" />
         <tli id="T635" time="411.621" type="appl" />
         <tli id="T636" time="412.235" type="appl" />
         <tli id="T637" time="412.696656861133" />
         <tli id="T638" time="413.294" type="appl" />
         <tli id="T639" time="413.737" type="appl" />
         <tli id="T640" time="414.181" type="appl" />
         <tli id="T641" time="414.624" type="appl" />
         <tli id="T642" />
         <tli id="T643" time="415.511" type="appl" />
         <tli id="T644" time="415.955" type="appl" />
         <tli id="T645" time="416.398" type="appl" />
         <tli id="T646" time="417.041991995299" />
         <tli id="T647" time="417.575" type="appl" />
         <tli id="T648" time="418.308" type="appl" />
         <tli id="T649" time="419.042" type="appl" />
         <tli id="T650" time="420.3012993315993" />
         <tli id="T651" time="420.369" type="appl" />
         <tli id="T652" time="420.962" type="appl" />
         <tli id="T653" time="421.556" type="appl" />
         <tli id="T654" time="422.15" type="appl" />
         <tli id="T655" time="422.744" type="appl" />
         <tli id="T656" time="423.338" type="appl" />
         <tli id="T657" time="423.931" type="appl" />
         <tli id="T658" time="424.525" type="appl" />
         <tli id="T659" time="425.119" type="appl" />
         <tli id="T660" time="425.712" type="appl" />
         <tli id="T661" time="426.306" type="appl" />
         <tli id="T662" time="427.4745410611428" />
         <tli id="T663" time="427.604" type="appl" />
         <tli id="T664" time="428.308" type="appl" />
         <tli id="T665" time="429.013" type="appl" />
         <tli id="T666" time="429.717" type="appl" />
         <tli id="T667" time="430.421" type="appl" />
         <tli id="T668" time="431.125" type="appl" />
         <tli id="T669" time="431.829" type="appl" />
         <tli id="T670" time="432.533" type="appl" />
         <tli id="T671" time="433.238" type="appl" />
         <tli id="T672" time="433.942" type="appl" />
         <tli id="T673" time="434.646" type="appl" />
         <tli id="T674" time="435.272" type="appl" />
         <tli id="T675" time="435.898" type="appl" />
         <tli id="T676" time="436.523" type="appl" />
         <tli id="T677" time="437.24774958856915" />
         <tli id="T678" time="437.63" type="appl" />
         <tli id="T679" time="438.111" type="appl" />
         <tli id="T680" time="438.592" type="appl" />
         <tli id="T681" time="439.073" type="appl" />
         <tli id="T682" time="439.554" type="appl" />
         <tli id="T683" time="440.035" type="appl" />
         <tli id="T684" time="440.516" type="appl" />
         <tli id="T685" time="440.997" type="appl" />
         <tli id="T686" time="441.6343602373049" />
         <tli id="T687" time="442.174" type="appl" />
         <tli id="T688" time="442.869" type="appl" />
         <tli id="T689" time="443.564" type="appl" />
         <tli id="T690" time="444.880985443892" />
         <tli id="T691" time="445.035" type="appl" />
         <tli id="T692" time="445.81" type="appl" />
         <tli id="T693" time="446.586" type="appl" />
         <tli id="T694" time="447.361" type="appl" />
         <tli id="T695" time="448.136" type="appl" />
         <tli id="T696" time="448.911" type="appl" />
         <tli id="T697" time="449.687" type="appl" />
         <tli id="T698" time="450.462" type="appl" />
         <tli id="T699" time="452.0208942657703" />
         <tli id="T700" time="452.095" type="appl" />
         <tli id="T701" time="452.954" type="appl" />
         <tli id="T702" time="453.812" type="appl" />
         <tli id="T703" time="454.671" type="appl" />
         <tli id="T704" time="455.529" type="appl" />
         <tli id="T705" time="456.388" type="appl" />
         <tli id="T706" time="457.246" type="appl" />
         <tli id="T707" time="458.105" type="appl" />
         <tli id="T708" time="458.963" type="appl" />
         <tli id="T709" time="459.822" type="appl" />
         <tli id="T710" time="462.34742906043846" />
         <tli id="T711" time="462.47961937362567" type="intp" />
         <tli id="T712" time="462.6118096868129" type="intp" />
         <tli id="T713" time="462.744" type="appl" />
         <tli id="T714" time="463.432" type="appl" />
         <tli id="T715" time="464.12" type="appl" />
         <tli id="T716" time="464.808" type="appl" />
         <tli id="T717" time="465.496" type="appl" />
         <tli id="T718" time="466.184" type="appl" />
         <tli id="T719" time="466.872" type="appl" />
         <tli id="T720" time="467.50001951078605" />
         <tli id="T721" time="468.884" type="appl" />
         <tli id="T722" time="470.208" type="appl" />
         <tli id="T723" time="470.918" type="appl" />
         <tli id="T724" time="471.627" type="appl" />
         <tli id="T725" time="472.337" type="appl" />
         <tli id="T726" time="473.047" type="appl" />
         <tli id="T727" time="473.757" type="appl" />
         <tli id="T728" time="474.466" type="appl" />
         <tli id="T729" time="475.176" type="appl" />
         <tli id="T730" time="475.886" type="appl" />
         <tli id="T731" time="476.596" type="appl" />
         <tli id="T732" time="477.305" type="appl" />
         <tli id="T733" time="478.015" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoXN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T733" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <nts id="Seg_4" n="HIAT:ip">(</nts>
                  <ats e="T1" id="Seg_5" n="HIAT:non-pho" s="T0">PAUSE</ats>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip">)</nts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_10" n="HIAT:w" s="T1">Čopočuka</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">olorbut</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">ühü</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_20" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_22" n="HIAT:w" s="T4">Bi͡es</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">mas</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">turu͡orbut</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">golomo</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">dʼi͡eleːk</ts>
                  <nts id="Seg_35" n="HIAT:ip">,</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">bi͡es</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">taːstaːk</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">sobo-ilimneːk</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_48" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">Kün</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">aːjɨ</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">ilimnene</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">kiːrer</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_63" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">Bu</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">kihi</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">balɨgɨ</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">logločču</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">musput</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_79" n="HIAT:ip">–</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">ilimitten</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_86" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_88" n="HIAT:w" s="T22">Innʼe</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_91" n="HIAT:w" s="T23">gɨnan</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">biːrde</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">barbɨta</ts>
                  <nts id="Seg_98" n="HIAT:ip">,</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">kɨrsalar</ts>
                  <nts id="Seg_102" n="HIAT:ip">,</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_105" n="HIAT:w" s="T27">tu͡oktar</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_108" n="HIAT:w" s="T28">hi͡ebitter</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_111" n="HIAT:w" s="T29">araj</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_114" n="HIAT:w" s="T30">balɨgɨn</ts>
                  <nts id="Seg_115" n="HIAT:ip">.</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_118" n="HIAT:u" s="T31">
                  <nts id="Seg_119" n="HIAT:ip">"</nts>
                  <ts e="T32" id="Seg_121" n="HIAT:w" s="T31">Munu</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">irdi͡ekke</ts>
                  <nts id="Seg_125" n="HIAT:ip">,</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_128" n="HIAT:w" s="T33">hu͡olun</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_131" n="HIAT:w" s="T34">irdi͡ekke</ts>
                  <nts id="Seg_132" n="HIAT:ip">.</nts>
                  <nts id="Seg_133" n="HIAT:ip">"</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_136" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_138" n="HIAT:w" s="T35">Balɨgɨn</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_141" n="HIAT:w" s="T36">dʼi͡etiger</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_144" n="HIAT:w" s="T37">tahan</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_147" n="HIAT:w" s="T38">baraːn</ts>
                  <nts id="Seg_148" n="HIAT:ip">,</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_151" n="HIAT:w" s="T39">bu</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_154" n="HIAT:w" s="T40">kihi</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_157" n="HIAT:w" s="T41">kɨrsa</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_160" n="HIAT:w" s="T42">hu͡olun</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_163" n="HIAT:w" s="T43">irdeːbit</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_167" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_169" n="HIAT:w" s="T44">Kɨrsa</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_172" n="HIAT:w" s="T45">hu͡olun</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_175" n="HIAT:w" s="T46">irdeːn</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_178" n="HIAT:w" s="T47">istegine</ts>
                  <nts id="Seg_179" n="HIAT:ip">,</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_182" n="HIAT:w" s="T48">kaja</ts>
                  <nts id="Seg_183" n="HIAT:ip">,</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_186" n="HIAT:w" s="T49">kɨrsa</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_189" n="HIAT:w" s="T50">tugu</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_192" n="HIAT:w" s="T51">i</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_195" n="HIAT:w" s="T52">biːr</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_198" n="HIAT:w" s="T53">talak</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_201" n="HIAT:w" s="T54">ihitten</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_204" n="HIAT:w" s="T55">tura</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_207" n="HIAT:w" s="T56">ekkireːbit</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_211" n="HIAT:u" s="T57">
                  <nts id="Seg_212" n="HIAT:ip">"</nts>
                  <ts e="T58" id="Seg_214" n="HIAT:w" s="T57">Ara</ts>
                  <nts id="Seg_215" n="HIAT:ip">,</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_218" n="HIAT:w" s="T58">Čopočuka</ts>
                  <nts id="Seg_219" n="HIAT:ip">,</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_222" n="HIAT:w" s="T59">kihini</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_225" n="HIAT:w" s="T60">hohuttuŋ</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_228" n="HIAT:w" s="T61">kaja</ts>
                  <nts id="Seg_229" n="HIAT:ip">,</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_232" n="HIAT:w" s="T62">kajtak</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_235" n="HIAT:w" s="T63">kelegin</ts>
                  <nts id="Seg_236" n="HIAT:ip">?</nts>
                  <nts id="Seg_237" n="HIAT:ip">"</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_240" n="HIAT:u" s="T64">
                  <nts id="Seg_241" n="HIAT:ip">"</nts>
                  <ts e="T65" id="Seg_243" n="HIAT:w" s="T64">Kaja</ts>
                  <nts id="Seg_244" n="HIAT:ip">,</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_247" n="HIAT:w" s="T65">balɨkpɨn</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_250" n="HIAT:w" s="T66">ke</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_253" n="HIAT:w" s="T67">togo</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_256" n="HIAT:w" s="T68">u͡orbukkutuj</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_259" n="HIAT:w" s="T69">ehigi</ts>
                  <nts id="Seg_260" n="HIAT:ip">?</nts>
                  <nts id="Seg_261" n="HIAT:ip">"</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_264" n="HIAT:u" s="T70">
                  <nts id="Seg_265" n="HIAT:ip">"</nts>
                  <ts e="T71" id="Seg_267" n="HIAT:w" s="T70">Ho</ts>
                  <nts id="Seg_268" n="HIAT:ip">,</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_271" n="HIAT:w" s="T71">min</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_274" n="HIAT:w" s="T72">u͡orbutum</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_277" n="HIAT:w" s="T73">du͡o</ts>
                  <nts id="Seg_278" n="HIAT:ip">"</nts>
                  <nts id="Seg_279" n="HIAT:ip">,</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_282" n="HIAT:w" s="T74">diː-diː</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_285" n="HIAT:w" s="T75">gɨmmɨt</ts>
                  <nts id="Seg_286" n="HIAT:ip">.</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_289" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_291" n="HIAT:w" s="T76">Mantɨŋ</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_294" n="HIAT:w" s="T77">kutujak</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_297" n="HIAT:w" s="T78">bu͡olbut</ts>
                  <nts id="Seg_298" n="HIAT:ip">,</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_301" n="HIAT:w" s="T79">kutujak</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_304" n="HIAT:w" s="T80">bu͡olan</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_307" n="HIAT:w" s="T81">dʼe</ts>
                  <nts id="Seg_308" n="HIAT:ip">,</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_311" n="HIAT:w" s="T82">ogonnʼoruŋ</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_314" n="HIAT:w" s="T83">dʼe</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_317" n="HIAT:w" s="T84">hɨldʼar</ts>
                  <nts id="Seg_318" n="HIAT:ip">.</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_321" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_323" n="HIAT:w" s="T85">Baːgɨnɨ</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_326" n="HIAT:w" s="T86">u͡oban</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_329" n="HIAT:w" s="T87">keːspit</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_331" n="HIAT:ip">–</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_334" n="HIAT:w" s="T88">kɨrsata</ts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_338" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_340" n="HIAT:w" s="T89">Bagɨŋ</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_343" n="HIAT:w" s="T90">ihin</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_346" n="HIAT:w" s="T91">kaja</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_348" n="HIAT:ip">(</nts>
                  <ts e="T94" id="Seg_350" n="HIAT:w" s="T92">ba-</ts>
                  <nts id="Seg_351" n="HIAT:ip">)</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_354" n="HIAT:w" s="T94">battan</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_357" n="HIAT:w" s="T95">bɨhan</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_360" n="HIAT:w" s="T96">taksa</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_362" n="HIAT:ip">(</nts>
                  <ts e="T99" id="Seg_364" n="HIAT:w" s="T97">ka-</ts>
                  <nts id="Seg_365" n="HIAT:ip">)</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_368" n="HIAT:w" s="T99">taksa</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_371" n="HIAT:w" s="T100">köppüt</ts>
                  <nts id="Seg_372" n="HIAT:ip">.</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_375" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_377" n="HIAT:w" s="T101">Innʼe</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_380" n="HIAT:w" s="T102">baran</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_383" n="HIAT:w" s="T103">baːgɨnɨ</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_386" n="HIAT:w" s="T104">hüler</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_389" n="HIAT:w" s="T105">da</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_392" n="HIAT:w" s="T106">karamaːnɨgar</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_395" n="HIAT:w" s="T107">uktar</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_398" n="HIAT:w" s="T108">daː</ts>
                  <nts id="Seg_399" n="HIAT:ip">,</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_402" n="HIAT:w" s="T109">kaːman</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_405" n="HIAT:w" s="T110">ispit</ts>
                  <nts id="Seg_406" n="HIAT:ip">.</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_409" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_411" n="HIAT:w" s="T111">Epi͡et</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_414" n="HIAT:w" s="T112">inni</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_417" n="HIAT:w" s="T113">di͡ek</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_420" n="HIAT:w" s="T114">kaːman</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_423" n="HIAT:w" s="T115">ihen</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_426" n="HIAT:w" s="T116">uskaːn</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_429" n="HIAT:w" s="T117">turu͡orbut</ts>
                  <nts id="Seg_430" n="HIAT:ip">.</nts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_433" n="HIAT:u" s="T118">
                  <nts id="Seg_434" n="HIAT:ip">"</nts>
                  <ts e="T119" id="Seg_436" n="HIAT:w" s="T118">Araː</ts>
                  <nts id="Seg_437" n="HIAT:ip">,</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_440" n="HIAT:w" s="T119">Čopočuka</ts>
                  <nts id="Seg_441" n="HIAT:ip">,</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_444" n="HIAT:w" s="T120">tu͡ok</ts>
                  <nts id="Seg_445" n="HIAT:ip">,</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_448" n="HIAT:w" s="T121">kaja</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_451" n="HIAT:w" s="T122">hirten</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_454" n="HIAT:w" s="T123">kelegin</ts>
                  <nts id="Seg_455" n="HIAT:ip">,</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_458" n="HIAT:w" s="T124">kihini</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_461" n="HIAT:w" s="T125">hohutaːktaːtɨŋ</ts>
                  <nts id="Seg_462" n="HIAT:ip">"</nts>
                  <nts id="Seg_463" n="HIAT:ip">,</nts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_466" n="HIAT:w" s="T126">diːbit</ts>
                  <nts id="Seg_467" n="HIAT:ip">.</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_470" n="HIAT:u" s="T127">
                  <nts id="Seg_471" n="HIAT:ip">"</nts>
                  <ts e="T128" id="Seg_473" n="HIAT:w" s="T127">Kaja</ts>
                  <nts id="Seg_474" n="HIAT:ip">,</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_477" n="HIAT:w" s="T128">balɨkpɨn</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_480" n="HIAT:w" s="T129">togo</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_483" n="HIAT:w" s="T130">barɨtɨn</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_486" n="HIAT:w" s="T131">hi͡ebikkitij</ts>
                  <nts id="Seg_487" n="HIAT:ip">,</nts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_490" n="HIAT:w" s="T132">balɨkpɨn</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_493" n="HIAT:w" s="T133">irdiː</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_496" n="HIAT:w" s="T134">hɨldʼabɨn</ts>
                  <nts id="Seg_497" n="HIAT:ip">.</nts>
                  <nts id="Seg_498" n="HIAT:ip">"</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_501" n="HIAT:u" s="T135">
                  <nts id="Seg_502" n="HIAT:ip">(</nts>
                  <nts id="Seg_503" n="HIAT:ip">(</nts>
                  <ats e="T136" id="Seg_504" n="HIAT:non-pho" s="T135">PAUSE</ats>
                  <nts id="Seg_505" n="HIAT:ip">)</nts>
                  <nts id="Seg_506" n="HIAT:ip">)</nts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_509" n="HIAT:w" s="T136">Maːgɨ</ts>
                  <nts id="Seg_510" n="HIAT:ip">…</nts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_513" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_515" n="HIAT:w" s="T138">Emi͡e</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_518" n="HIAT:w" s="T139">kaja</ts>
                  <nts id="Seg_519" n="HIAT:ip">…</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_522" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_524" n="HIAT:w" s="T141">Ti͡ej</ts>
                  <nts id="Seg_525" n="HIAT:ip">,</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_528" n="HIAT:w" s="T142">dʼe</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_531" n="HIAT:w" s="T143">ölörsöllör</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_534" n="HIAT:w" s="T144">e</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_537" n="HIAT:w" s="T145">uskaːnɨ</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_540" n="HIAT:w" s="T146">kɨtta</ts>
                  <nts id="Seg_541" n="HIAT:ip">.</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_544" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_546" n="HIAT:w" s="T147">Ölörsönnör</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_549" n="HIAT:w" s="T148">uskaːnɨ</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_552" n="HIAT:w" s="T149">tugun</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_555" n="HIAT:w" s="T150">kihili͡ej</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_558" n="HIAT:w" s="T151">ol</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_561" n="HIAT:w" s="T152">kihi</ts>
                  <nts id="Seg_562" n="HIAT:ip">–</nts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_565" n="HIAT:w" s="T153">paːgɨnɨ</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_568" n="HIAT:w" s="T154">ölörbüt</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_571" n="HIAT:w" s="T155">da</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_574" n="HIAT:w" s="T156">emi͡e</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_576" n="HIAT:ip">(</nts>
                  <nts id="Seg_577" n="HIAT:ip">(</nts>
                  <ats e="T158" id="Seg_578" n="HIAT:non-pho" s="T157">PAUSE</ats>
                  <nts id="Seg_579" n="HIAT:ip">)</nts>
                  <nts id="Seg_580" n="HIAT:ip">)</nts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_583" n="HIAT:w" s="T158">karamaːnɨgar</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_586" n="HIAT:w" s="T159">uktan</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_589" n="HIAT:w" s="T160">keːspit</ts>
                  <nts id="Seg_590" n="HIAT:ip">,</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_593" n="HIAT:w" s="T161">hülünen</ts>
                  <nts id="Seg_594" n="HIAT:ip">.</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_597" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_599" n="HIAT:w" s="T162">Emi͡e</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_602" n="HIAT:w" s="T163">kaːman</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_605" n="HIAT:w" s="T164">ispit</ts>
                  <nts id="Seg_606" n="HIAT:ip">.</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_609" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_611" n="HIAT:w" s="T165">Kaːman-kaːman</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_614" n="HIAT:w" s="T166">istegine</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_617" n="HIAT:w" s="T167">hahɨl</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_620" n="HIAT:w" s="T168">tura</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_623" n="HIAT:w" s="T169">ekkireːbit</ts>
                  <nts id="Seg_624" n="HIAT:ip">.</nts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_627" n="HIAT:u" s="T170">
                  <nts id="Seg_628" n="HIAT:ip">"</nts>
                  <ts e="T171" id="Seg_630" n="HIAT:w" s="T170">Oː</ts>
                  <nts id="Seg_631" n="HIAT:ip">,</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_634" n="HIAT:w" s="T171">Čopočuka</ts>
                  <nts id="Seg_635" n="HIAT:ip">,</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_638" n="HIAT:w" s="T172">kihini</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_641" n="HIAT:w" s="T173">hohuttuŋ</ts>
                  <nts id="Seg_642" n="HIAT:ip">,</nts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_645" n="HIAT:w" s="T174">kaja</ts>
                  <nts id="Seg_646" n="HIAT:ip">,</nts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_649" n="HIAT:w" s="T175">kaja</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_652" n="HIAT:w" s="T176">hirten</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_655" n="HIAT:w" s="T177">kelegin</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_658" n="HIAT:w" s="T178">bu</ts>
                  <nts id="Seg_659" n="HIAT:ip">"</nts>
                  <nts id="Seg_660" n="HIAT:ip">,</nts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_663" n="HIAT:w" s="T179">di͡ebit</ts>
                  <nts id="Seg_664" n="HIAT:ip">.</nts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_667" n="HIAT:u" s="T180">
                  <nts id="Seg_668" n="HIAT:ip">"</nts>
                  <ts e="T181" id="Seg_670" n="HIAT:w" s="T180">Kaja</ts>
                  <nts id="Seg_671" n="HIAT:ip">"</nts>
                  <nts id="Seg_672" n="HIAT:ip">,</nts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_675" n="HIAT:w" s="T181">di͡ebit</ts>
                  <nts id="Seg_676" n="HIAT:ip">,</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_678" n="HIAT:ip">"</nts>
                  <ts e="T183" id="Seg_680" n="HIAT:w" s="T182">ka</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_683" n="HIAT:w" s="T183">balɨkpɨn</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_686" n="HIAT:w" s="T184">barɨtɨn</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_689" n="HIAT:w" s="T185">hi͡ebikkit</ts>
                  <nts id="Seg_690" n="HIAT:ip">.</nts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_693" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_695" n="HIAT:w" s="T186">Ol</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_698" n="HIAT:w" s="T187">ihin</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_701" n="HIAT:w" s="T188">irdeːn</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_704" n="HIAT:w" s="T189">kelebin</ts>
                  <nts id="Seg_705" n="HIAT:ip">.</nts>
                  <nts id="Seg_706" n="HIAT:ip">"</nts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_709" n="HIAT:u" s="T190">
                  <nts id="Seg_710" n="HIAT:ip">"</nts>
                  <ts e="T191" id="Seg_712" n="HIAT:w" s="T190">Pa</ts>
                  <nts id="Seg_713" n="HIAT:ip">,</nts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_716" n="HIAT:w" s="T191">en</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_719" n="HIAT:w" s="T192">balɨkkar</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_722" n="HIAT:w" s="T193">bihigi</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_725" n="HIAT:w" s="T194">čugahaːbatakpɨt</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_728" n="HIAT:w" s="T195">daːganɨ</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_731" n="HIAT:w" s="T196">össü͡ö</ts>
                  <nts id="Seg_732" n="HIAT:ip">.</nts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_735" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_737" n="HIAT:w" s="T197">Kata</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_740" n="HIAT:w" s="T198">min</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_743" n="HIAT:w" s="T199">enigin</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_746" n="HIAT:w" s="T200">tɨrɨta</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_749" n="HIAT:w" s="T201">tɨːtan</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_752" n="HIAT:w" s="T202">hi͡en</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_755" n="HIAT:w" s="T203">keːhi͡em</ts>
                  <nts id="Seg_756" n="HIAT:ip">"</nts>
                  <nts id="Seg_757" n="HIAT:ip">,</nts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_760" n="HIAT:w" s="T204">di͡ebit</ts>
                  <nts id="Seg_761" n="HIAT:ip">.</nts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_764" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_766" n="HIAT:w" s="T205">Dʼe</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_769" n="HIAT:w" s="T206">ölörsöllör</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_772" n="HIAT:w" s="T207">bu</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_775" n="HIAT:w" s="T208">kihiler</ts>
                  <nts id="Seg_776" n="HIAT:ip">.</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_779" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_781" n="HIAT:w" s="T209">Čopočuka</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_784" n="HIAT:w" s="T210">kantɨtɨn</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_787" n="HIAT:w" s="T211">kihili͡ej</ts>
                  <nts id="Seg_788" n="HIAT:ip">:</nts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_791" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_793" n="HIAT:w" s="T212">Ti͡ere</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_796" n="HIAT:w" s="T213">battaːbɨt</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_799" n="HIAT:w" s="T214">da</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_802" n="HIAT:w" s="T215">ölörön</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_805" n="HIAT:w" s="T216">keːspit</ts>
                  <nts id="Seg_806" n="HIAT:ip">.</nts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_809" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_811" n="HIAT:w" s="T217">Maːŋɨnɨ</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_814" n="HIAT:w" s="T218">hülbüt</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_817" n="HIAT:w" s="T219">da</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_820" n="HIAT:w" s="T220">karamaːnɨgar</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_822" n="HIAT:ip">(</nts>
                  <ts e="T222" id="Seg_824" n="HIAT:w" s="T221">karamaːn</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_827" n="HIAT:w" s="T222">da</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_830" n="HIAT:w" s="T223">ulakan</ts>
                  <nts id="Seg_831" n="HIAT:ip">,</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_834" n="HIAT:w" s="T224">söktüm</ts>
                  <nts id="Seg_835" n="HIAT:ip">)</nts>
                  <nts id="Seg_836" n="HIAT:ip">.</nts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_839" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_841" n="HIAT:w" s="T225">Hahɨlɨn</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_844" n="HIAT:w" s="T226">uktan</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_847" n="HIAT:w" s="T227">keːspit</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_850" n="HIAT:w" s="T228">karamaːnɨgar</ts>
                  <nts id="Seg_851" n="HIAT:ip">.</nts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_854" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_856" n="HIAT:w" s="T229">Onton</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_859" n="HIAT:w" s="T230">kaːman</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_862" n="HIAT:w" s="T231">ispit</ts>
                  <nts id="Seg_863" n="HIAT:ip">.</nts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_866" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_868" n="HIAT:w" s="T232">Kaːman</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_870" n="HIAT:ip">(</nts>
                  <ts e="T234" id="Seg_872" n="HIAT:w" s="T233">ihe-</ts>
                  <nts id="Seg_873" n="HIAT:ip">)</nts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_876" n="HIAT:w" s="T234">istegine</ts>
                  <nts id="Seg_877" n="HIAT:ip">,</nts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_880" n="HIAT:w" s="T235">hi͡egen</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_883" n="HIAT:w" s="T236">tura</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_886" n="HIAT:w" s="T237">ekkireːbit</ts>
                  <nts id="Seg_887" n="HIAT:ip">.</nts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_890" n="HIAT:u" s="T238">
                  <nts id="Seg_891" n="HIAT:ip">"</nts>
                  <ts e="T239" id="Seg_893" n="HIAT:w" s="T238">Kaja</ts>
                  <nts id="Seg_894" n="HIAT:ip">,</nts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_897" n="HIAT:w" s="T239">Čopočuka</ts>
                  <nts id="Seg_898" n="HIAT:ip">,</nts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_901" n="HIAT:w" s="T240">kihini</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_904" n="HIAT:w" s="T241">hohuttuŋ</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_907" n="HIAT:w" s="T242">kata</ts>
                  <nts id="Seg_908" n="HIAT:ip">"</nts>
                  <nts id="Seg_909" n="HIAT:ip">,</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_912" n="HIAT:w" s="T243">di͡ebit</ts>
                  <nts id="Seg_913" n="HIAT:ip">.</nts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T250" id="Seg_916" n="HIAT:u" s="T244">
                  <nts id="Seg_917" n="HIAT:ip">"</nts>
                  <ts e="T245" id="Seg_919" n="HIAT:w" s="T244">Togo</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_922" n="HIAT:w" s="T245">hɨldʼagɨn</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_925" n="HIAT:w" s="T246">bu</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_928" n="HIAT:w" s="T247">di͡en</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_931" n="HIAT:w" s="T248">dojduga</ts>
                  <nts id="Seg_932" n="HIAT:ip">,</nts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_935" n="HIAT:w" s="T249">pa</ts>
                  <nts id="Seg_936" n="HIAT:ip">?</nts>
                  <nts id="Seg_937" n="HIAT:ip">"</nts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T255" id="Seg_940" n="HIAT:u" s="T250">
                  <nts id="Seg_941" n="HIAT:ip">"</nts>
                  <ts e="T251" id="Seg_943" n="HIAT:w" s="T250">Ka</ts>
                  <nts id="Seg_944" n="HIAT:ip">,</nts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_947" n="HIAT:w" s="T251">balɨkpɨn</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_950" n="HIAT:w" s="T252">togo</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_953" n="HIAT:w" s="T253">barɨtɨn</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_956" n="HIAT:w" s="T254">hi͡ebikkitij</ts>
                  <nts id="Seg_957" n="HIAT:ip">?</nts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T261" id="Seg_960" n="HIAT:u" s="T255">
                  <ts e="T256" id="Seg_962" n="HIAT:w" s="T255">Onu</ts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_965" n="HIAT:w" s="T256">irdiː</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_968" n="HIAT:w" s="T257">hɨldʼabɨn</ts>
                  <nts id="Seg_969" n="HIAT:ip">"</nts>
                  <nts id="Seg_970" n="HIAT:ip">,</nts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_973" n="HIAT:w" s="T258">di͡ebit</ts>
                  <nts id="Seg_974" n="HIAT:ip">,</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_976" n="HIAT:ip">"</nts>
                  <ts e="T260" id="Seg_978" n="HIAT:w" s="T259">össü͡ö</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_981" n="HIAT:w" s="T260">hi͡ebikkitij</ts>
                  <nts id="Seg_982" n="HIAT:ip">.</nts>
                  <nts id="Seg_983" n="HIAT:ip">"</nts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T267" id="Seg_986" n="HIAT:u" s="T261">
                  <ts e="T262" id="Seg_988" n="HIAT:w" s="T261">Kaban</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_991" n="HIAT:w" s="T262">ɨlar</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_994" n="HIAT:w" s="T263">da</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_997" n="HIAT:w" s="T264">maːŋɨnɨ</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1000" n="HIAT:w" s="T265">dʼe</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1003" n="HIAT:w" s="T266">ölörsör</ts>
                  <nts id="Seg_1004" n="HIAT:ip">.</nts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_1007" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_1009" n="HIAT:w" s="T267">Ölörsön</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1012" n="HIAT:w" s="T268">Čopočuka</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1015" n="HIAT:w" s="T269">tugun</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1018" n="HIAT:w" s="T270">kihili͡ej</ts>
                  <nts id="Seg_1019" n="HIAT:ip">,</nts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1022" n="HIAT:w" s="T271">kihileːbetek</ts>
                  <nts id="Seg_1023" n="HIAT:ip">.</nts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_1026" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_1028" n="HIAT:w" s="T272">Ölörbüt</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1031" n="HIAT:w" s="T273">da</ts>
                  <nts id="Seg_1032" n="HIAT:ip">,</nts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1035" n="HIAT:w" s="T274">emi͡e</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1038" n="HIAT:w" s="T275">hülbüt</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1041" n="HIAT:w" s="T276">da</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1044" n="HIAT:w" s="T277">karmaːnɨgar</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1047" n="HIAT:w" s="T278">uktan</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1050" n="HIAT:w" s="T279">keːspit</ts>
                  <nts id="Seg_1051" n="HIAT:ip">.</nts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_1054" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_1056" n="HIAT:w" s="T280">Emi͡e</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1059" n="HIAT:w" s="T281">kaːman</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1062" n="HIAT:w" s="T282">ispit</ts>
                  <nts id="Seg_1063" n="HIAT:ip">.</nts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1066" n="HIAT:u" s="T283">
                  <ts e="T284" id="Seg_1068" n="HIAT:w" s="T283">Kaːman-kaːman</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1071" n="HIAT:w" s="T284">börönü</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1074" n="HIAT:w" s="T285">körsübüt</ts>
                  <nts id="Seg_1075" n="HIAT:ip">.</nts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1078" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_1080" n="HIAT:w" s="T286">Börötö</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1083" n="HIAT:w" s="T287">tura</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1086" n="HIAT:w" s="T288">ekkireːbit</ts>
                  <nts id="Seg_1087" n="HIAT:ip">.</nts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1090" n="HIAT:u" s="T289">
                  <nts id="Seg_1091" n="HIAT:ip">"</nts>
                  <ts e="T290" id="Seg_1093" n="HIAT:w" s="T289">O</ts>
                  <nts id="Seg_1094" n="HIAT:ip">,</nts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1097" n="HIAT:w" s="T290">Čopočuka</ts>
                  <nts id="Seg_1098" n="HIAT:ip">,</nts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1101" n="HIAT:w" s="T291">kihini</ts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1104" n="HIAT:w" s="T292">hohuttuŋ</ts>
                  <nts id="Seg_1105" n="HIAT:ip">,</nts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1108" n="HIAT:w" s="T293">ka</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1111" n="HIAT:w" s="T294">kajdi͡ek</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1114" n="HIAT:w" s="T295">ihegin</ts>
                  <nts id="Seg_1115" n="HIAT:ip">"</nts>
                  <nts id="Seg_1116" n="HIAT:ip">,</nts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1119" n="HIAT:w" s="T296">di͡ebit</ts>
                  <nts id="Seg_1120" n="HIAT:ip">.</nts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_1123" n="HIAT:u" s="T297">
                  <nts id="Seg_1124" n="HIAT:ip">"</nts>
                  <ts e="T298" id="Seg_1126" n="HIAT:w" s="T297">Kaja</ts>
                  <nts id="Seg_1127" n="HIAT:ip">,</nts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1130" n="HIAT:w" s="T298">balɨkpɨn</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1133" n="HIAT:w" s="T299">togo</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1136" n="HIAT:w" s="T300">hi͡ebikkitij</ts>
                  <nts id="Seg_1137" n="HIAT:ip">?</nts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1140" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1142" n="HIAT:w" s="T301">Ontubun</ts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1145" n="HIAT:w" s="T302">irdiːbin</ts>
                  <nts id="Seg_1146" n="HIAT:ip">.</nts>
                  <nts id="Seg_1147" n="HIAT:ip">"</nts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_1150" n="HIAT:u" s="T303">
                  <nts id="Seg_1151" n="HIAT:ip">"</nts>
                  <ts e="T304" id="Seg_1153" n="HIAT:w" s="T303">Pa</ts>
                  <nts id="Seg_1154" n="HIAT:ip">,</nts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1157" n="HIAT:w" s="T304">en</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1160" n="HIAT:w" s="T305">balɨkkar</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1163" n="HIAT:w" s="T306">čugahaːbatakpɨt</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1166" n="HIAT:w" s="T307">daːganɨ</ts>
                  <nts id="Seg_1167" n="HIAT:ip">,</nts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1170" n="HIAT:w" s="T308">kata</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1173" n="HIAT:w" s="T309">min</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1176" n="HIAT:w" s="T310">enigin</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1179" n="HIAT:w" s="T311">hi͡en</ts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1182" n="HIAT:w" s="T312">keːhi͡em</ts>
                  <nts id="Seg_1183" n="HIAT:ip">"</nts>
                  <nts id="Seg_1184" n="HIAT:ip">,</nts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1187" n="HIAT:w" s="T313">di͡ebit</ts>
                  <nts id="Seg_1188" n="HIAT:ip">.</nts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T320" id="Seg_1191" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1193" n="HIAT:w" s="T314">Baːgɨnɨ</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1196" n="HIAT:w" s="T315">u͡oban</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1199" n="HIAT:w" s="T316">keːspit</ts>
                  <nts id="Seg_1200" n="HIAT:ip">,</nts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1203" n="HIAT:w" s="T317">ɨŋɨran</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1206" n="HIAT:w" s="T318">keːspit</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1208" n="HIAT:ip">–</nts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1211" n="HIAT:w" s="T319">Čopočukaːnɨ</ts>
                  <nts id="Seg_1212" n="HIAT:ip">.</nts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1215" n="HIAT:u" s="T320">
                  <ts e="T321" id="Seg_1217" n="HIAT:w" s="T320">Čopočuka</ts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1220" n="HIAT:w" s="T321">ihin</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1223" n="HIAT:w" s="T322">kaja</ts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1226" n="HIAT:w" s="T323">battɨːr</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1229" n="HIAT:w" s="T324">da</ts>
                  <nts id="Seg_1230" n="HIAT:ip">,</nts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1233" n="HIAT:w" s="T325">taksa</ts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1236" n="HIAT:w" s="T326">kötör</ts>
                  <nts id="Seg_1237" n="HIAT:ip">.</nts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_1240" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1242" n="HIAT:w" s="T327">Innʼe</ts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1245" n="HIAT:w" s="T328">gɨmmɨt</ts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1248" n="HIAT:w" s="T329">da</ts>
                  <nts id="Seg_1249" n="HIAT:ip">,</nts>
                  <nts id="Seg_1250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1252" n="HIAT:w" s="T330">hülbüt</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1255" n="HIAT:w" s="T331">da</ts>
                  <nts id="Seg_1256" n="HIAT:ip">,</nts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1259" n="HIAT:w" s="T332">karamaːnɨgar</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1262" n="HIAT:w" s="T333">uktan</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1265" n="HIAT:w" s="T334">keːspit</ts>
                  <nts id="Seg_1266" n="HIAT:ip">.</nts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1269" n="HIAT:u" s="T335">
                  <ts e="T336" id="Seg_1271" n="HIAT:w" s="T335">Onton</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1274" n="HIAT:w" s="T336">dʼe</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1277" n="HIAT:w" s="T337">kaːman</ts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1280" n="HIAT:w" s="T338">ispit</ts>
                  <nts id="Seg_1281" n="HIAT:ip">,</nts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1284" n="HIAT:w" s="T339">kaːman</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1287" n="HIAT:w" s="T340">ispit</ts>
                  <nts id="Seg_1288" n="HIAT:ip">.</nts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1291" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1293" n="HIAT:w" s="T341">O</ts>
                  <nts id="Seg_1294" n="HIAT:ip">,</nts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1297" n="HIAT:w" s="T342">ebekeː</ts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1300" n="HIAT:w" s="T343">tura</ts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1303" n="HIAT:w" s="T344">ekkireːkteːbit</ts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1306" n="HIAT:w" s="T345">diːn</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1309" n="HIAT:w" s="T346">diː</ts>
                  <nts id="Seg_1310" n="HIAT:ip">,</nts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1313" n="HIAT:w" s="T347">araj</ts>
                  <nts id="Seg_1314" n="HIAT:ip">.</nts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T356" id="Seg_1317" n="HIAT:u" s="T348">
                  <nts id="Seg_1318" n="HIAT:ip">"</nts>
                  <ts e="T349" id="Seg_1320" n="HIAT:w" s="T348">Oj</ts>
                  <nts id="Seg_1321" n="HIAT:ip">,</nts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1324" n="HIAT:w" s="T349">Čopočuka</ts>
                  <nts id="Seg_1325" n="HIAT:ip">,</nts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1328" n="HIAT:w" s="T350">togo</ts>
                  <nts id="Seg_1329" n="HIAT:ip">,</nts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1332" n="HIAT:w" s="T351">kaja</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1335" n="HIAT:w" s="T352">hirge</ts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1338" n="HIAT:w" s="T353">hɨldʼagɨn</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1341" n="HIAT:w" s="T354">bu</ts>
                  <nts id="Seg_1342" n="HIAT:ip">"</nts>
                  <nts id="Seg_1343" n="HIAT:ip">,</nts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1346" n="HIAT:w" s="T355">di͡ebit</ts>
                  <nts id="Seg_1347" n="HIAT:ip">.</nts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1350" n="HIAT:u" s="T356">
                  <nts id="Seg_1351" n="HIAT:ip">"</nts>
                  <ts e="T357" id="Seg_1353" n="HIAT:w" s="T356">Balɨkpɨn</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1356" n="HIAT:w" s="T357">togo</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1359" n="HIAT:w" s="T358">u͡orbukkutuj</ts>
                  <nts id="Seg_1360" n="HIAT:ip">?</nts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T364" id="Seg_1363" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_1365" n="HIAT:w" s="T359">Össü͡ö</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1368" n="HIAT:w" s="T360">balɨkpɨn</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1371" n="HIAT:w" s="T361">u͡orbukkut</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1373" n="HIAT:ip">(</nts>
                  <ts e="T364" id="Seg_1375" n="HIAT:w" s="T362">si͡e-</ts>
                  <nts id="Seg_1376" n="HIAT:ip">)</nts>
                  <nts id="Seg_1377" n="HIAT:ip">…</nts>
                  <nts id="Seg_1378" n="HIAT:ip">"</nts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T369" id="Seg_1381" n="HIAT:u" s="T364">
                  <nts id="Seg_1382" n="HIAT:ip">"</nts>
                  <ts e="T365" id="Seg_1384" n="HIAT:w" s="T364">Min</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1387" n="HIAT:w" s="T365">enigin</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1390" n="HIAT:w" s="T366">u͡oban</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1393" n="HIAT:w" s="T367">keːhi͡em</ts>
                  <nts id="Seg_1394" n="HIAT:ip">"</nts>
                  <nts id="Seg_1395" n="HIAT:ip">,</nts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1398" n="HIAT:w" s="T368">di͡ebit</ts>
                  <nts id="Seg_1399" n="HIAT:ip">.</nts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1402" n="HIAT:u" s="T369">
                  <ts e="T370" id="Seg_1404" n="HIAT:w" s="T369">Hi͡en</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1407" n="HIAT:w" s="T370">keːspit</ts>
                  <nts id="Seg_1408" n="HIAT:ip">.</nts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T379" id="Seg_1411" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1413" n="HIAT:w" s="T371">Ihin</ts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1416" n="HIAT:w" s="T372">kaja</ts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1419" n="HIAT:w" s="T373">battɨːr</ts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1422" n="HIAT:w" s="T374">da</ts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1425" n="HIAT:w" s="T375">taksa</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1428" n="HIAT:w" s="T376">kötör</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1431" n="HIAT:w" s="T377">bu</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1434" n="HIAT:w" s="T378">kihi</ts>
                  <nts id="Seg_1435" n="HIAT:ip">.</nts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T387" id="Seg_1438" n="HIAT:u" s="T379">
                  <ts e="T380" id="Seg_1440" n="HIAT:w" s="T379">Innʼe</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1443" n="HIAT:w" s="T380">baraːn</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1446" n="HIAT:w" s="T381">bahagɨnan</ts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1449" n="HIAT:w" s="T382">hülen</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1452" n="HIAT:w" s="T383">baraːn</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1455" n="HIAT:w" s="T384">dʼe</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1458" n="HIAT:w" s="T385">olorbut</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1461" n="HIAT:w" s="T386">adʼas</ts>
                  <nts id="Seg_1462" n="HIAT:ip">.</nts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1465" n="HIAT:u" s="T387">
                  <ts e="T388" id="Seg_1467" n="HIAT:w" s="T387">Bu</ts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1470" n="HIAT:w" s="T388">oloron</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1473" n="HIAT:w" s="T389">dumajdaːbɨt</ts>
                  <nts id="Seg_1474" n="HIAT:ip">:</nts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1477" n="HIAT:u" s="T390">
                  <nts id="Seg_1478" n="HIAT:ip">"</nts>
                  <ts e="T391" id="Seg_1480" n="HIAT:w" s="T390">Bu</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1483" n="HIAT:w" s="T391">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1486" n="HIAT:w" s="T392">kɨːhɨn</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1489" n="HIAT:w" s="T393">kördüː</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1492" n="HIAT:w" s="T394">barɨ͡akka</ts>
                  <nts id="Seg_1493" n="HIAT:ip">.</nts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_1496" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1498" n="HIAT:w" s="T395">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1501" n="HIAT:w" s="T396">hogotok</ts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1504" n="HIAT:w" s="T397">kɨːstaːk</ts>
                  <nts id="Seg_1505" n="HIAT:ip">,</nts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1508" n="HIAT:w" s="T398">di͡ebittere</ts>
                  <nts id="Seg_1509" n="HIAT:ip">,</nts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1512" n="HIAT:w" s="T399">onu</ts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1515" n="HIAT:w" s="T400">kördüː</ts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1518" n="HIAT:w" s="T401">barɨ͡akka</ts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1521" n="HIAT:w" s="T402">naːda</ts>
                  <nts id="Seg_1522" n="HIAT:ip">.</nts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T408" id="Seg_1525" n="HIAT:u" s="T403">
                  <ts e="T404" id="Seg_1527" n="HIAT:w" s="T403">Baččalaːk</ts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1530" n="HIAT:w" s="T404">tu͡oktaːk</ts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1533" n="HIAT:w" s="T405">kihi</ts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1536" n="HIAT:w" s="T406">ke</ts>
                  <nts id="Seg_1537" n="HIAT:ip">"</nts>
                  <nts id="Seg_1538" n="HIAT:ip">,</nts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1541" n="HIAT:w" s="T407">di͡ebit</ts>
                  <nts id="Seg_1542" n="HIAT:ip">.</nts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T416" id="Seg_1545" n="HIAT:u" s="T408">
                  <ts e="T409" id="Seg_1547" n="HIAT:w" s="T408">Dʼe</ts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1550" n="HIAT:w" s="T409">ɨraːktaːgɨga</ts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1553" n="HIAT:w" s="T410">baraːrɨ</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1556" n="HIAT:w" s="T411">ol</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1559" n="HIAT:w" s="T412">oloror</ts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1562" n="HIAT:w" s="T413">bu</ts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1565" n="HIAT:w" s="T414">kihiŋ</ts>
                  <nts id="Seg_1566" n="HIAT:ip">,</nts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1569" n="HIAT:w" s="T415">oloror</ts>
                  <nts id="Seg_1570" n="HIAT:ip">.</nts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_1573" n="HIAT:u" s="T416">
                  <nts id="Seg_1574" n="HIAT:ip">"</nts>
                  <ts e="T417" id="Seg_1576" n="HIAT:w" s="T416">Dʼe</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1579" n="HIAT:w" s="T417">kaːmɨ͡akka</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1582" n="HIAT:w" s="T418">üčügej</ts>
                  <nts id="Seg_1583" n="HIAT:ip">.</nts>
                  <nts id="Seg_1584" n="HIAT:ip">"</nts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T423" id="Seg_1587" n="HIAT:u" s="T419">
                  <ts e="T420" id="Seg_1589" n="HIAT:w" s="T419">Dʼe</ts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1592" n="HIAT:w" s="T420">kaːmar</ts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1595" n="HIAT:w" s="T421">bu</ts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1598" n="HIAT:w" s="T422">kihi</ts>
                  <nts id="Seg_1599" n="HIAT:ip">.</nts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T427" id="Seg_1602" n="HIAT:u" s="T423">
                  <ts e="T424" id="Seg_1604" n="HIAT:w" s="T423">Kaːman-kaːman</ts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1607" n="HIAT:w" s="T424">biːr</ts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1610" n="HIAT:w" s="T425">hirge</ts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1613" n="HIAT:w" s="T426">olorbut</ts>
                  <nts id="Seg_1614" n="HIAT:ip">.</nts>
                  <nts id="Seg_1615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T434" id="Seg_1617" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_1619" n="HIAT:w" s="T427">Oloron</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1622" n="HIAT:w" s="T428">körüleːbite</ts>
                  <nts id="Seg_1623" n="HIAT:ip">,</nts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1626" n="HIAT:w" s="T429">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1629" n="HIAT:w" s="T430">gi͡ene</ts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1632" n="HIAT:w" s="T431">kirili͡ehe</ts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1635" n="HIAT:w" s="T432">köstör</ts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1638" n="HIAT:w" s="T433">araj</ts>
                  <nts id="Seg_1639" n="HIAT:ip">.</nts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1642" n="HIAT:u" s="T434">
                  <nts id="Seg_1643" n="HIAT:ip">"</nts>
                  <ts e="T435" id="Seg_1645" n="HIAT:w" s="T434">Tijeːnibin</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1648" n="HIAT:w" s="T435">itinne</ts>
                  <nts id="Seg_1649" n="HIAT:ip">.</nts>
                  <nts id="Seg_1650" n="HIAT:ip">"</nts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T444" id="Seg_1653" n="HIAT:u" s="T436">
                  <ts e="T437" id="Seg_1655" n="HIAT:w" s="T436">Bu</ts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1658" n="HIAT:w" s="T437">kihi</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1661" n="HIAT:w" s="T438">kaːman-kaːman</ts>
                  <nts id="Seg_1662" n="HIAT:ip">,</nts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1665" n="HIAT:w" s="T439">kaːman-kaːman</ts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1668" n="HIAT:w" s="T440">gu͡orakka</ts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1671" n="HIAT:w" s="T441">kelbit</ts>
                  <nts id="Seg_1672" n="HIAT:ip">,</nts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1675" n="HIAT:w" s="T442">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1678" n="HIAT:w" s="T443">gi͡eniger</ts>
                  <nts id="Seg_1679" n="HIAT:ip">.</nts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1682" n="HIAT:u" s="T444">
                  <ts e="T445" id="Seg_1684" n="HIAT:w" s="T444">Kelbite</ts>
                  <nts id="Seg_1685" n="HIAT:ip">,</nts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1688" n="HIAT:w" s="T445">muŋ</ts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1691" n="HIAT:w" s="T446">tas</ts>
                  <nts id="Seg_1692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1694" n="HIAT:w" s="T447">di͡ek</ts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1697" n="HIAT:w" s="T448">kuhagan</ts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1700" n="HIAT:w" s="T449">bagajɨ</ts>
                  <nts id="Seg_1701" n="HIAT:ip">,</nts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1704" n="HIAT:w" s="T450">buru͡olaːk</ts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1707" n="HIAT:w" s="T451">golomo</ts>
                  <nts id="Seg_1708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1710" n="HIAT:w" s="T452">turar</ts>
                  <nts id="Seg_1711" n="HIAT:ip">.</nts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T456" id="Seg_1714" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_1716" n="HIAT:w" s="T453">Golomo</ts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1719" n="HIAT:w" s="T454">turar</ts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1722" n="HIAT:w" s="T455">araj</ts>
                  <nts id="Seg_1723" n="HIAT:ip">.</nts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T460" id="Seg_1726" n="HIAT:u" s="T456">
                  <nts id="Seg_1727" n="HIAT:ip">"</nts>
                  <ts e="T457" id="Seg_1729" n="HIAT:w" s="T456">Bunuga</ts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1732" n="HIAT:w" s="T457">kiːri͡ekke</ts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1735" n="HIAT:w" s="T458">üčügej</ts>
                  <nts id="Seg_1736" n="HIAT:ip">,</nts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1739" n="HIAT:w" s="T459">golomogo</ts>
                  <nts id="Seg_1740" n="HIAT:ip">.</nts>
                  <nts id="Seg_1741" n="HIAT:ip">"</nts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T465" id="Seg_1744" n="HIAT:u" s="T460">
                  <ts e="T461" id="Seg_1746" n="HIAT:w" s="T460">Golomogo</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1749" n="HIAT:w" s="T461">kötön</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1752" n="HIAT:w" s="T462">tüspüte</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1754" n="HIAT:ip">–</nts>
                  <nts id="Seg_1755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1757" n="HIAT:w" s="T463">ogonnʼordoːk</ts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1760" n="HIAT:w" s="T464">emeːksin</ts>
                  <nts id="Seg_1761" n="HIAT:ip">.</nts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T472" id="Seg_1764" n="HIAT:u" s="T465">
                  <ts e="T466" id="Seg_1766" n="HIAT:w" s="T465">Emeːksine</ts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1769" n="HIAT:w" s="T466">karaga</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1772" n="HIAT:w" s="T467">hu͡ok</ts>
                  <nts id="Seg_1773" n="HIAT:ip">,</nts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1776" n="HIAT:w" s="T468">ogonnʼoro</ts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1779" n="HIAT:w" s="T469">hi͡ese</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1782" n="HIAT:w" s="T470">hečen</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1785" n="HIAT:w" s="T471">hogus</ts>
                  <nts id="Seg_1786" n="HIAT:ip">.</nts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T478" id="Seg_1789" n="HIAT:u" s="T472">
                  <ts e="T473" id="Seg_1791" n="HIAT:w" s="T472">Dʼe</ts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1794" n="HIAT:w" s="T473">ahatallar</ts>
                  <nts id="Seg_1795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1797" n="HIAT:w" s="T474">manɨ</ts>
                  <nts id="Seg_1798" n="HIAT:ip">,</nts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1801" n="HIAT:w" s="T475">ahɨːr</ts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1804" n="HIAT:w" s="T476">bu</ts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1807" n="HIAT:w" s="T477">ɨ͡aldʼɨt</ts>
                  <nts id="Seg_1808" n="HIAT:ip">.</nts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T484" id="Seg_1811" n="HIAT:u" s="T478">
                  <nts id="Seg_1812" n="HIAT:ip">"</nts>
                  <ts e="T479" id="Seg_1814" n="HIAT:w" s="T478">Kaja-kaja</ts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1817" n="HIAT:w" s="T479">hirten</ts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1820" n="HIAT:w" s="T480">kelegin</ts>
                  <nts id="Seg_1821" n="HIAT:ip">"</nts>
                  <nts id="Seg_1822" n="HIAT:ip">,</nts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1825" n="HIAT:w" s="T481">di͡en</ts>
                  <nts id="Seg_1826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1828" n="HIAT:w" s="T482">ös</ts>
                  <nts id="Seg_1829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1831" n="HIAT:w" s="T483">ɨjɨtallar</ts>
                  <nts id="Seg_1832" n="HIAT:ip">.</nts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T498" id="Seg_1835" n="HIAT:u" s="T484">
                  <nts id="Seg_1836" n="HIAT:ip">"</nts>
                  <ts e="T485" id="Seg_1838" n="HIAT:w" s="T484">Min</ts>
                  <nts id="Seg_1839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1840" n="HIAT:ip">(</nts>
                  <nts id="Seg_1841" n="HIAT:ip">(</nts>
                  <ats e="T486" id="Seg_1842" n="HIAT:non-pho" s="T485">PAUSE</ats>
                  <nts id="Seg_1843" n="HIAT:ip">)</nts>
                  <nts id="Seg_1844" n="HIAT:ip">)</nts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1847" n="HIAT:w" s="T486">bi͡es</ts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1850" n="HIAT:w" s="T487">taːstaːk</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1853" n="HIAT:w" s="T488">hobo-ilimneːk</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1856" n="HIAT:w" s="T489">etim</ts>
                  <nts id="Seg_1857" n="HIAT:ip">,</nts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1860" n="HIAT:w" s="T490">ontubuttan</ts>
                  <nts id="Seg_1861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1863" n="HIAT:w" s="T491">balɨgɨ</ts>
                  <nts id="Seg_1864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1866" n="HIAT:w" s="T492">munnʼarbɨn</ts>
                  <nts id="Seg_1867" n="HIAT:ip">,</nts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1870" n="HIAT:w" s="T493">u͡ordarammɨn</ts>
                  <nts id="Seg_1871" n="HIAT:ip">,</nts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1874" n="HIAT:w" s="T494">ahɨːlaːktarɨ</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1877" n="HIAT:w" s="T495">ölörtöːn</ts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1880" n="HIAT:w" s="T496">kelebin</ts>
                  <nts id="Seg_1881" n="HIAT:ip">"</nts>
                  <nts id="Seg_1882" n="HIAT:ip">,</nts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1885" n="HIAT:w" s="T497">di͡ebit</ts>
                  <nts id="Seg_1886" n="HIAT:ip">.</nts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T504" id="Seg_1889" n="HIAT:u" s="T498">
                  <ts e="T499" id="Seg_1891" n="HIAT:w" s="T498">Eːk</ts>
                  <nts id="Seg_1892" n="HIAT:ip">,</nts>
                  <nts id="Seg_1893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1894" n="HIAT:ip">(</nts>
                  <ts e="T501" id="Seg_1896" n="HIAT:w" s="T499">n-</ts>
                  <nts id="Seg_1897" n="HIAT:ip">)</nts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1900" n="HIAT:w" s="T501">ahɨːlaːktarɨ</ts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1903" n="HIAT:w" s="T502">ölörön</ts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1906" n="HIAT:w" s="T503">kelbit</ts>
                  <nts id="Seg_1907" n="HIAT:ip">.</nts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T511" id="Seg_1910" n="HIAT:u" s="T504">
                  <ts e="T505" id="Seg_1912" n="HIAT:w" s="T504">Bu</ts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1915" n="HIAT:w" s="T505">ogonnʼotton</ts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1918" n="HIAT:w" s="T506">ɨjɨtar</ts>
                  <nts id="Seg_1919" n="HIAT:ip">,</nts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1921" n="HIAT:ip">"</nts>
                  <ts e="T508" id="Seg_1923" n="HIAT:w" s="T507">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1926" n="HIAT:w" s="T508">kɨːstaːk</ts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1929" n="HIAT:w" s="T509">duː</ts>
                  <nts id="Seg_1930" n="HIAT:ip">"</nts>
                  <nts id="Seg_1931" n="HIAT:ip">,</nts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1934" n="HIAT:w" s="T510">di͡en</ts>
                  <nts id="Seg_1935" n="HIAT:ip">.</nts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T520" id="Seg_1938" n="HIAT:u" s="T511">
                  <nts id="Seg_1939" n="HIAT:ip">"</nts>
                  <ts e="T512" id="Seg_1941" n="HIAT:w" s="T511">Hu͡očča-hogotok</ts>
                  <nts id="Seg_1942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1944" n="HIAT:w" s="T512">kɨːstaːk</ts>
                  <nts id="Seg_1945" n="HIAT:ip">,</nts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1948" n="HIAT:w" s="T513">kanna</ts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1951" n="HIAT:w" s="T514">ere</ts>
                  <nts id="Seg_1952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1953" n="HIAT:ip">(</nts>
                  <nts id="Seg_1954" n="HIAT:ip">(</nts>
                  <ats e="T516" id="Seg_1955" n="HIAT:non-pho" s="T515">PAUSE</ats>
                  <nts id="Seg_1956" n="HIAT:ip">)</nts>
                  <nts id="Seg_1957" n="HIAT:ip">)</nts>
                  <nts id="Seg_1958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1960" n="HIAT:w" s="T516">erge</ts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1963" n="HIAT:w" s="T517">bi͡ereller</ts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1966" n="HIAT:w" s="T518">ühü</ts>
                  <nts id="Seg_1967" n="HIAT:ip">"</nts>
                  <nts id="Seg_1968" n="HIAT:ip">,</nts>
                  <nts id="Seg_1969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1971" n="HIAT:w" s="T519">di͡en</ts>
                  <nts id="Seg_1972" n="HIAT:ip">.</nts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T529" id="Seg_1975" n="HIAT:u" s="T520">
                  <nts id="Seg_1976" n="HIAT:ip">"</nts>
                  <ts e="T521" id="Seg_1978" n="HIAT:w" s="T520">Ogonnʼor</ts>
                  <nts id="Seg_1979" n="HIAT:ip">,</nts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1982" n="HIAT:w" s="T521">en</ts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1985" n="HIAT:w" s="T522">hu͡orumdʼuga</ts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1988" n="HIAT:w" s="T523">barɨ͡aŋ</ts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1991" n="HIAT:w" s="T524">hu͡ok</ts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1994" n="HIAT:w" s="T525">duː</ts>
                  <nts id="Seg_1995" n="HIAT:ip">"</nts>
                  <nts id="Seg_1996" n="HIAT:ip">,</nts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1999" n="HIAT:w" s="T526">di͡ebit</ts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2002" n="HIAT:w" s="T527">baːjdɨ</ts>
                  <nts id="Seg_2003" n="HIAT:ip">,</nts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2006" n="HIAT:w" s="T528">ɨ͡aldʼɨta</ts>
                  <nts id="Seg_2007" n="HIAT:ip">.</nts>
                  <nts id="Seg_2008" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T533" id="Seg_2010" n="HIAT:u" s="T529">
                  <nts id="Seg_2011" n="HIAT:ip">"</nts>
                  <ts e="T530" id="Seg_2013" n="HIAT:w" s="T529">Abaga</ts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2016" n="HIAT:w" s="T530">ɨjɨtan</ts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2019" n="HIAT:w" s="T531">körü͡ökpüt</ts>
                  <nts id="Seg_2020" n="HIAT:ip">"</nts>
                  <nts id="Seg_2021" n="HIAT:ip">,</nts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2024" n="HIAT:w" s="T532">di͡ebit</ts>
                  <nts id="Seg_2025" n="HIAT:ip">.</nts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T536" id="Seg_2028" n="HIAT:u" s="T533">
                  <nts id="Seg_2029" n="HIAT:ip">"</nts>
                  <nts id="Seg_2030" n="HIAT:ip">"</nts>
                  <ts e="T534" id="Seg_2032" n="HIAT:w" s="T533">Biːr</ts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2035" n="HIAT:w" s="T534">ɨ͡aldʼɨt</ts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2038" n="HIAT:w" s="T535">kelbit</ts>
                  <nts id="Seg_2039" n="HIAT:ip">.</nts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T541" id="Seg_2042" n="HIAT:u" s="T536">
                  <ts e="T537" id="Seg_2044" n="HIAT:w" s="T536">Kɨːskɨn</ts>
                  <nts id="Seg_2045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2047" n="HIAT:w" s="T537">kördüːr</ts>
                  <nts id="Seg_2048" n="HIAT:ip">"</nts>
                  <nts id="Seg_2049" n="HIAT:ip">,</nts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2052" n="HIAT:w" s="T538">di͡eŋŋin</ts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2055" n="HIAT:w" s="T539">haŋar</ts>
                  <nts id="Seg_2056" n="HIAT:ip">"</nts>
                  <nts id="Seg_2057" n="HIAT:ip">,</nts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2060" n="HIAT:w" s="T540">di͡ebit</ts>
                  <nts id="Seg_2061" n="HIAT:ip">.</nts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T548" id="Seg_2064" n="HIAT:u" s="T541">
                  <ts e="T542" id="Seg_2066" n="HIAT:w" s="T541">Bu</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2069" n="HIAT:w" s="T542">kihi</ts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2072" n="HIAT:w" s="T543">baran</ts>
                  <nts id="Seg_2073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2075" n="HIAT:w" s="T544">kaːlbɨt</ts>
                  <nts id="Seg_2076" n="HIAT:ip">,</nts>
                  <nts id="Seg_2077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2079" n="HIAT:w" s="T545">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2082" n="HIAT:w" s="T546">kötön</ts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2085" n="HIAT:w" s="T547">tüspüt</ts>
                  <nts id="Seg_2086" n="HIAT:ip">.</nts>
                  <nts id="Seg_2087" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T551" id="Seg_2089" n="HIAT:u" s="T548">
                  <nts id="Seg_2090" n="HIAT:ip">"</nts>
                  <ts e="T549" id="Seg_2092" n="HIAT:w" s="T548">Kaja</ts>
                  <nts id="Seg_2093" n="HIAT:ip">,</nts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2096" n="HIAT:w" s="T549">togo</ts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2099" n="HIAT:w" s="T550">kelegin</ts>
                  <nts id="Seg_2100" n="HIAT:ip">?</nts>
                  <nts id="Seg_2101" n="HIAT:ip">"</nts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_2104" n="HIAT:u" s="T551">
                  <nts id="Seg_2105" n="HIAT:ip">"</nts>
                  <ts e="T552" id="Seg_2107" n="HIAT:w" s="T551">Hu͡ok</ts>
                  <nts id="Seg_2108" n="HIAT:ip">,</nts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2111" n="HIAT:w" s="T552">min</ts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2114" n="HIAT:w" s="T553">ɨ͡aldʼɨttaːkpɨn</ts>
                  <nts id="Seg_2115" n="HIAT:ip">,</nts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2118" n="HIAT:w" s="T554">kaja-kaja</ts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2121" n="HIAT:w" s="T555">hirten</ts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2124" n="HIAT:w" s="T556">kelbite</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2127" n="HIAT:w" s="T557">bu͡olla</ts>
                  <nts id="Seg_2128" n="HIAT:ip">,</nts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2131" n="HIAT:w" s="T558">bilbeppin</ts>
                  <nts id="Seg_2132" n="HIAT:ip">.</nts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T565" id="Seg_2135" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_2137" n="HIAT:w" s="T559">Ol</ts>
                  <nts id="Seg_2138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2140" n="HIAT:w" s="T560">kihi</ts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2143" n="HIAT:w" s="T561">kɨːskɨn</ts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2146" n="HIAT:w" s="T562">kördöːn</ts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2149" n="HIAT:w" s="T563">erer</ts>
                  <nts id="Seg_2150" n="HIAT:ip">"</nts>
                  <nts id="Seg_2151" n="HIAT:ip">,</nts>
                  <nts id="Seg_2152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2154" n="HIAT:w" s="T564">di͡ebit</ts>
                  <nts id="Seg_2155" n="HIAT:ip">.</nts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T576" id="Seg_2158" n="HIAT:u" s="T565">
                  <nts id="Seg_2159" n="HIAT:ip">"</nts>
                  <ts e="T566" id="Seg_2161" n="HIAT:w" s="T565">Innʼe</ts>
                  <nts id="Seg_2162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2164" n="HIAT:w" s="T566">gɨnan</ts>
                  <nts id="Seg_2165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2167" n="HIAT:w" s="T567">ol</ts>
                  <nts id="Seg_2168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2170" n="HIAT:w" s="T568">kihi</ts>
                  <nts id="Seg_2171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2173" n="HIAT:w" s="T569">gi͡enin</ts>
                  <nts id="Seg_2174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2176" n="HIAT:w" s="T570">hɨrajɨn-karagɨn</ts>
                  <nts id="Seg_2177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2179" n="HIAT:w" s="T571">körü͡ökpün</ts>
                  <nts id="Seg_2180" n="HIAT:ip">,</nts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2183" n="HIAT:w" s="T572">manna</ts>
                  <nts id="Seg_2184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2186" n="HIAT:w" s="T573">egeliŋ</ts>
                  <nts id="Seg_2187" n="HIAT:ip">"</nts>
                  <nts id="Seg_2188" n="HIAT:ip">,</nts>
                  <nts id="Seg_2189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2191" n="HIAT:w" s="T574">di͡ebit</ts>
                  <nts id="Seg_2192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2194" n="HIAT:w" s="T575">ɨraːktaːgɨta</ts>
                  <nts id="Seg_2195" n="HIAT:ip">.</nts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T581" id="Seg_2198" n="HIAT:u" s="T576">
                  <ts e="T577" id="Seg_2200" n="HIAT:w" s="T576">Bu</ts>
                  <nts id="Seg_2201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2203" n="HIAT:w" s="T577">ogonnʼor</ts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2206" n="HIAT:w" s="T578">baːjdɨ</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2209" n="HIAT:w" s="T579">ɨ͡aldʼɨtɨn</ts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2212" n="HIAT:w" s="T580">ilpit</ts>
                  <nts id="Seg_2213" n="HIAT:ip">.</nts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T592" id="Seg_2216" n="HIAT:u" s="T581">
                  <nts id="Seg_2217" n="HIAT:ip">"</nts>
                  <ts e="T582" id="Seg_2219" n="HIAT:w" s="T581">Pa</ts>
                  <nts id="Seg_2220" n="HIAT:ip">,</nts>
                  <nts id="Seg_2221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2223" n="HIAT:w" s="T582">min</ts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2226" n="HIAT:w" s="T583">itinnik</ts>
                  <nts id="Seg_2227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2229" n="HIAT:w" s="T584">kihige</ts>
                  <nts id="Seg_2230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2232" n="HIAT:w" s="T585">bi͡erbeppin</ts>
                  <nts id="Seg_2233" n="HIAT:ip">,</nts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2236" n="HIAT:w" s="T586">iliːtin-atagɨn</ts>
                  <nts id="Seg_2237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2239" n="HIAT:w" s="T587">tohutalaːn</ts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2242" n="HIAT:w" s="T588">baraːn</ts>
                  <nts id="Seg_2243" n="HIAT:ip">,</nts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2246" n="HIAT:w" s="T589">kaːjɨːga</ts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2249" n="HIAT:w" s="T590">bɨragɨŋ</ts>
                  <nts id="Seg_2250" n="HIAT:ip">"</nts>
                  <nts id="Seg_2251" n="HIAT:ip">,</nts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2254" n="HIAT:w" s="T591">di͡ebit</ts>
                  <nts id="Seg_2255" n="HIAT:ip">.</nts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T593" id="Seg_2258" n="HIAT:u" s="T592">
                  <ts e="T593" id="Seg_2260" n="HIAT:w" s="T592">Ek</ts>
                  <nts id="Seg_2261" n="HIAT:ip">.</nts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T597" id="Seg_2264" n="HIAT:u" s="T593">
                  <ts e="T594" id="Seg_2266" n="HIAT:w" s="T593">Bu</ts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2269" n="HIAT:w" s="T594">kihigin</ts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2272" n="HIAT:w" s="T595">iliːtin-atagɨn</ts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2275" n="HIAT:w" s="T596">tohuppataktar</ts>
                  <nts id="Seg_2276" n="HIAT:ip">.</nts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T605" id="Seg_2279" n="HIAT:u" s="T597">
                  <ts e="T598" id="Seg_2281" n="HIAT:w" s="T597">Bɨ͡annan</ts>
                  <nts id="Seg_2282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2284" n="HIAT:w" s="T598">kelgijen</ts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2287" n="HIAT:w" s="T599">baraːnnar</ts>
                  <nts id="Seg_2288" n="HIAT:ip">,</nts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2291" n="HIAT:w" s="T600">kaːjɨːga</ts>
                  <nts id="Seg_2292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2294" n="HIAT:w" s="T601">ugan</ts>
                  <nts id="Seg_2295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2297" n="HIAT:w" s="T602">keːspitter</ts>
                  <nts id="Seg_2298" n="HIAT:ip">,</nts>
                  <nts id="Seg_2299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2301" n="HIAT:w" s="T603">maːjdɨ</ts>
                  <nts id="Seg_2302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2304" n="HIAT:w" s="T604">kihigin</ts>
                  <nts id="Seg_2305" n="HIAT:ip">.</nts>
                  <nts id="Seg_2306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T612" id="Seg_2308" n="HIAT:u" s="T605">
                  <nts id="Seg_2309" n="HIAT:ip">(</nts>
                  <nts id="Seg_2310" n="HIAT:ip">(</nts>
                  <ats e="T606" id="Seg_2311" n="HIAT:non-pho" s="T605">PAUSE</ats>
                  <nts id="Seg_2312" n="HIAT:ip">)</nts>
                  <nts id="Seg_2313" n="HIAT:ip">)</nts>
                  <nts id="Seg_2314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2316" n="HIAT:w" s="T606">Bu</ts>
                  <nts id="Seg_2317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2319" n="HIAT:w" s="T607">kihi</ts>
                  <nts id="Seg_2320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2322" n="HIAT:w" s="T608">kutujak</ts>
                  <nts id="Seg_2323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2325" n="HIAT:w" s="T609">bu͡olan</ts>
                  <nts id="Seg_2326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2328" n="HIAT:w" s="T610">taksa</ts>
                  <nts id="Seg_2329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2331" n="HIAT:w" s="T611">köppüt</ts>
                  <nts id="Seg_2332" n="HIAT:ip">.</nts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T617" id="Seg_2335" n="HIAT:u" s="T612">
                  <ts e="T613" id="Seg_2337" n="HIAT:w" s="T612">Taksa</ts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2340" n="HIAT:w" s="T613">kötön</ts>
                  <nts id="Seg_2341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2343" n="HIAT:w" s="T614">ogonnʼorugar</ts>
                  <nts id="Seg_2344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2346" n="HIAT:w" s="T615">emi͡e</ts>
                  <nts id="Seg_2347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2349" n="HIAT:w" s="T616">kiːrbit</ts>
                  <nts id="Seg_2350" n="HIAT:ip">.</nts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T634" id="Seg_2353" n="HIAT:u" s="T617">
                  <ts e="T618" id="Seg_2355" n="HIAT:w" s="T617">Dʼe</ts>
                  <nts id="Seg_2356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2358" n="HIAT:w" s="T618">bilbeppin</ts>
                  <nts id="Seg_2359" n="HIAT:ip">,</nts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2362" n="HIAT:w" s="T619">maːjdɨŋɨtaːgar</ts>
                  <nts id="Seg_2363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2365" n="HIAT:w" s="T620">atɨn</ts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2368" n="HIAT:w" s="T621">kihi</ts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2371" n="HIAT:w" s="T622">kelbite</ts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2374" n="HIAT:w" s="T623">duː</ts>
                  <nts id="Seg_2375" n="HIAT:ip">,</nts>
                  <nts id="Seg_2376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2378" n="HIAT:w" s="T624">tu͡ok</ts>
                  <nts id="Seg_2379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2381" n="HIAT:w" s="T625">duː</ts>
                  <nts id="Seg_2382" n="HIAT:ip">,</nts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2385" n="HIAT:w" s="T626">emi͡e</ts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2387" n="HIAT:ip">"</nts>
                  <nts id="Seg_2388" n="HIAT:ip">"</nts>
                  <ts e="T628" id="Seg_2390" n="HIAT:w" s="T627">Kɨːskɨn</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2393" n="HIAT:w" s="T628">kördöːn</ts>
                  <nts id="Seg_2394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2396" n="HIAT:w" s="T629">erer</ts>
                  <nts id="Seg_2397" n="HIAT:ip">"</nts>
                  <nts id="Seg_2398" n="HIAT:ip">,</nts>
                  <nts id="Seg_2399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2401" n="HIAT:w" s="T630">di͡eŋŋin</ts>
                  <nts id="Seg_2402" n="HIAT:ip">,</nts>
                  <nts id="Seg_2403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2405" n="HIAT:w" s="T631">bar</ts>
                  <nts id="Seg_2406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2408" n="HIAT:w" s="T632">dʼe</ts>
                  <nts id="Seg_2409" n="HIAT:ip">"</nts>
                  <nts id="Seg_2410" n="HIAT:ip">,</nts>
                  <nts id="Seg_2411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2413" n="HIAT:w" s="T633">di͡ebit</ts>
                  <nts id="Seg_2414" n="HIAT:ip">.</nts>
                  <nts id="Seg_2415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T637" id="Seg_2417" n="HIAT:u" s="T634">
                  <ts e="T635" id="Seg_2419" n="HIAT:w" s="T634">Emi͡e</ts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2422" n="HIAT:w" s="T635">barbɨt</ts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2425" n="HIAT:w" s="T636">ogonnʼor</ts>
                  <nts id="Seg_2426" n="HIAT:ip">.</nts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T642" id="Seg_2429" n="HIAT:u" s="T637">
                  <ts e="T638" id="Seg_2431" n="HIAT:w" s="T637">Dʼe</ts>
                  <nts id="Seg_2432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2434" n="HIAT:w" s="T638">ol</ts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2437" n="HIAT:w" s="T639">baran</ts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2440" n="HIAT:w" s="T640">emi͡e</ts>
                  <nts id="Seg_2441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2443" n="HIAT:w" s="T641">di͡ebit</ts>
                  <nts id="Seg_2444" n="HIAT:ip">:</nts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T646" id="Seg_2447" n="HIAT:u" s="T642">
                  <nts id="Seg_2448" n="HIAT:ip">"</nts>
                  <ts e="T643" id="Seg_2450" n="HIAT:w" s="T642">Kaja</ts>
                  <nts id="Seg_2451" n="HIAT:ip">,</nts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2454" n="HIAT:w" s="T643">emi͡e</ts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2457" n="HIAT:w" s="T644">ɨ͡aldʼɨt</ts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2460" n="HIAT:w" s="T645">kelle</ts>
                  <nts id="Seg_2461" n="HIAT:ip">.</nts>
                  <nts id="Seg_2462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T650" id="Seg_2464" n="HIAT:u" s="T646">
                  <ts e="T647" id="Seg_2466" n="HIAT:w" s="T646">Kɨːskɨn</ts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2469" n="HIAT:w" s="T647">kördöːn</ts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2472" n="HIAT:w" s="T648">erer</ts>
                  <nts id="Seg_2473" n="HIAT:ip">"</nts>
                  <nts id="Seg_2474" n="HIAT:ip">,</nts>
                  <nts id="Seg_2475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2477" n="HIAT:w" s="T649">di͡ebit</ts>
                  <nts id="Seg_2478" n="HIAT:ip">.</nts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T662" id="Seg_2481" n="HIAT:u" s="T650">
                  <nts id="Seg_2482" n="HIAT:ip">"</nts>
                  <ts e="T651" id="Seg_2484" n="HIAT:w" s="T650">Dogottor</ts>
                  <nts id="Seg_2485" n="HIAT:ip">,</nts>
                  <nts id="Seg_2486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2488" n="HIAT:w" s="T651">maːjdɨ</ts>
                  <nts id="Seg_2489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2491" n="HIAT:w" s="T652">kaːjɨːga</ts>
                  <nts id="Seg_2492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2494" n="HIAT:w" s="T653">bɨragaːččɨ</ts>
                  <nts id="Seg_2495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2497" n="HIAT:w" s="T654">kihigitin</ts>
                  <nts id="Seg_2498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2500" n="HIAT:w" s="T655">körüŋ</ts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2503" n="HIAT:w" s="T656">dʼe</ts>
                  <nts id="Seg_2504" n="HIAT:ip">,</nts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2507" n="HIAT:w" s="T657">kajtak-kajtak</ts>
                  <nts id="Seg_2508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2510" n="HIAT:w" s="T658">kihini</ts>
                  <nts id="Seg_2511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2513" n="HIAT:w" s="T659">dʼabɨlɨːbɨt</ts>
                  <nts id="Seg_2514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2516" n="HIAT:w" s="T660">dʼürü</ts>
                  <nts id="Seg_2517" n="HIAT:ip">"</nts>
                  <nts id="Seg_2518" n="HIAT:ip">,</nts>
                  <nts id="Seg_2519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2521" n="HIAT:w" s="T661">di͡ebit</ts>
                  <nts id="Seg_2522" n="HIAT:ip">.</nts>
                  <nts id="Seg_2523" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T673" id="Seg_2525" n="HIAT:u" s="T662">
                  <ts e="T663" id="Seg_2527" n="HIAT:w" s="T662">Kaːjɨːga</ts>
                  <nts id="Seg_2528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2530" n="HIAT:w" s="T663">bɨrakpɨt</ts>
                  <nts id="Seg_2531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2533" n="HIAT:w" s="T664">kihite</ts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2536" n="HIAT:w" s="T665">hu͡ola</ts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2539" n="HIAT:w" s="T666">ere</ts>
                  <nts id="Seg_2540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2542" n="HIAT:w" s="T667">čöŋöjö</ts>
                  <nts id="Seg_2543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2545" n="HIAT:w" s="T668">hɨtar</ts>
                  <nts id="Seg_2546" n="HIAT:ip">,</nts>
                  <nts id="Seg_2547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2549" n="HIAT:w" s="T669">kelgijbit</ts>
                  <nts id="Seg_2550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2552" n="HIAT:w" s="T670">bɨ͡ata</ts>
                  <nts id="Seg_2553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2555" n="HIAT:w" s="T671">bɨ͡atɨnan</ts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2558" n="HIAT:w" s="T672">hɨtar</ts>
                  <nts id="Seg_2559" n="HIAT:ip">.</nts>
                  <nts id="Seg_2560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T677" id="Seg_2562" n="HIAT:u" s="T673">
                  <ts e="T674" id="Seg_2564" n="HIAT:w" s="T673">Dʼe</ts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2567" n="HIAT:w" s="T674">kaja</ts>
                  <nts id="Seg_2568" n="HIAT:ip">,</nts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2571" n="HIAT:w" s="T675">onnuk-onnuk</ts>
                  <nts id="Seg_2572" n="HIAT:ip">,</nts>
                  <nts id="Seg_2573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2575" n="HIAT:w" s="T676">kaja</ts>
                  <nts id="Seg_2576" n="HIAT:ip">.</nts>
                  <nts id="Seg_2577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T686" id="Seg_2579" n="HIAT:u" s="T677">
                  <nts id="Seg_2580" n="HIAT:ip">"</nts>
                  <ts e="T678" id="Seg_2582" n="HIAT:w" s="T677">O</ts>
                  <nts id="Seg_2583" n="HIAT:ip">,</nts>
                  <nts id="Seg_2584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2586" n="HIAT:w" s="T678">dʼe</ts>
                  <nts id="Seg_2587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2589" n="HIAT:w" s="T679">ile</ts>
                  <nts id="Seg_2590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2592" n="HIAT:w" s="T680">da</ts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2595" n="HIAT:w" s="T681">üčügej</ts>
                  <nts id="Seg_2596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2598" n="HIAT:w" s="T682">kihini</ts>
                  <nts id="Seg_2599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2601" n="HIAT:w" s="T683">gɨnnɨm</ts>
                  <nts id="Seg_2602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2604" n="HIAT:w" s="T684">e</ts>
                  <nts id="Seg_2605" n="HIAT:ip">,</nts>
                  <nts id="Seg_2606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2608" n="HIAT:w" s="T685">badaga</ts>
                  <nts id="Seg_2609" n="HIAT:ip">.</nts>
                  <nts id="Seg_2610" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T690" id="Seg_2612" n="HIAT:u" s="T686">
                  <ts e="T687" id="Seg_2614" n="HIAT:w" s="T686">Kɨːspɨn</ts>
                  <nts id="Seg_2615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2617" n="HIAT:w" s="T687">bi͡erebin</ts>
                  <nts id="Seg_2618" n="HIAT:ip">,</nts>
                  <nts id="Seg_2619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2621" n="HIAT:w" s="T688">di͡em</ts>
                  <nts id="Seg_2622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2624" n="HIAT:w" s="T689">bu͡olla</ts>
                  <nts id="Seg_2625" n="HIAT:ip">.</nts>
                  <nts id="Seg_2626" n="HIAT:ip">"</nts>
                  <nts id="Seg_2627" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T699" id="Seg_2629" n="HIAT:u" s="T690">
                  <ts e="T691" id="Seg_2631" n="HIAT:w" s="T690">Bu</ts>
                  <nts id="Seg_2632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2634" n="HIAT:w" s="T691">kihi</ts>
                  <nts id="Seg_2635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2637" n="HIAT:w" s="T692">maːgɨlaːk</ts>
                  <nts id="Seg_2638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2640" n="HIAT:w" s="T693">ahɨːlaːk</ts>
                  <nts id="Seg_2641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2643" n="HIAT:w" s="T694">ölörbütün</ts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2646" n="HIAT:w" s="T695">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2649" n="HIAT:w" s="T696">töhögüger</ts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2652" n="HIAT:w" s="T697">togo</ts>
                  <nts id="Seg_2653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2655" n="HIAT:w" s="T698">bɨrakpɨt</ts>
                  <nts id="Seg_2656" n="HIAT:ip">.</nts>
                  <nts id="Seg_2657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T710" id="Seg_2659" n="HIAT:u" s="T699">
                  <ts e="T700" id="Seg_2661" n="HIAT:w" s="T699">Innʼe</ts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2664" n="HIAT:w" s="T700">gɨnan</ts>
                  <nts id="Seg_2665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2667" n="HIAT:w" s="T701">köhün</ts>
                  <nts id="Seg_2668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2670" n="HIAT:w" s="T702">kennitiger</ts>
                  <nts id="Seg_2671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2673" n="HIAT:w" s="T703">čɨːčaːk</ts>
                  <nts id="Seg_2674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2676" n="HIAT:w" s="T704">ɨllɨːr</ts>
                  <nts id="Seg_2677" n="HIAT:ip">,</nts>
                  <nts id="Seg_2678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2680" n="HIAT:w" s="T705">godolutan</ts>
                  <nts id="Seg_2681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2683" n="HIAT:w" s="T706">araːran</ts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2686" n="HIAT:w" s="T707">ilpit</ts>
                  <nts id="Seg_2687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2689" n="HIAT:w" s="T708">bu</ts>
                  <nts id="Seg_2690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2692" n="HIAT:w" s="T709">kihi</ts>
                  <nts id="Seg_2693" n="HIAT:ip">.</nts>
                  <nts id="Seg_2694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T720" id="Seg_2696" n="HIAT:u" s="T710">
                  <ts e="T711" id="Seg_2698" n="HIAT:w" s="T710">Eŋin-eŋin</ts>
                  <nts id="Seg_2699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2701" n="HIAT:w" s="T711">haldaːttaːk-tu͡oktaːk</ts>
                  <nts id="Seg_2702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2704" n="HIAT:w" s="T712">bu</ts>
                  <nts id="Seg_2705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2707" n="HIAT:w" s="T713">kihi</ts>
                  <nts id="Seg_2708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2710" n="HIAT:w" s="T714">dʼe</ts>
                  <nts id="Seg_2711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2713" n="HIAT:w" s="T715">barar</ts>
                  <nts id="Seg_2714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2716" n="HIAT:w" s="T716">dʼi͡etiger</ts>
                  <nts id="Seg_2717" n="HIAT:ip">,</nts>
                  <nts id="Seg_2718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2720" n="HIAT:w" s="T717">kɨːhɨn</ts>
                  <nts id="Seg_2721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2723" n="HIAT:w" s="T718">ɨlan</ts>
                  <nts id="Seg_2724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2726" n="HIAT:w" s="T719">baraːn</ts>
                  <nts id="Seg_2727" n="HIAT:ip">.</nts>
                  <nts id="Seg_2728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T722" id="Seg_2730" n="HIAT:u" s="T720">
                  <ts e="T721" id="Seg_2732" n="HIAT:w" s="T720">Dʼe</ts>
                  <nts id="Seg_2733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2735" n="HIAT:w" s="T721">köhör</ts>
                  <nts id="Seg_2736" n="HIAT:ip">.</nts>
                  <nts id="Seg_2737" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T733" id="Seg_2739" n="HIAT:u" s="T722">
                  <ts e="T723" id="Seg_2741" n="HIAT:w" s="T722">Köhön</ts>
                  <nts id="Seg_2742" n="HIAT:ip">,</nts>
                  <nts id="Seg_2743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2745" n="HIAT:w" s="T723">iti</ts>
                  <nts id="Seg_2746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2748" n="HIAT:w" s="T724">kelenner</ts>
                  <nts id="Seg_2749" n="HIAT:ip">,</nts>
                  <nts id="Seg_2750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2752" n="HIAT:w" s="T725">iti</ts>
                  <nts id="Seg_2753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2755" n="HIAT:w" s="T726">dʼe</ts>
                  <nts id="Seg_2756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2758" n="HIAT:w" s="T727">iti</ts>
                  <nts id="Seg_2759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2761" n="HIAT:w" s="T728">bajan-toton</ts>
                  <nts id="Seg_2762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2764" n="HIAT:w" s="T729">olorbuta</ts>
                  <nts id="Seg_2765" n="HIAT:ip">,</nts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2768" n="HIAT:w" s="T730">dʼe</ts>
                  <nts id="Seg_2769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2771" n="HIAT:w" s="T731">iti</ts>
                  <nts id="Seg_2772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2774" n="HIAT:w" s="T732">elete</ts>
                  <nts id="Seg_2775" n="HIAT:ip">.</nts>
                  <nts id="Seg_2776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T733" id="Seg_2777" n="sc" s="T0">
               <ts e="T1" id="Seg_2779" n="e" s="T0">((PAUSE)) </ts>
               <ts e="T2" id="Seg_2781" n="e" s="T1">Čopočuka </ts>
               <ts e="T3" id="Seg_2783" n="e" s="T2">olorbut </ts>
               <ts e="T4" id="Seg_2785" n="e" s="T3">ühü. </ts>
               <ts e="T5" id="Seg_2787" n="e" s="T4">Bi͡es </ts>
               <ts e="T6" id="Seg_2789" n="e" s="T5">mas </ts>
               <ts e="T7" id="Seg_2791" n="e" s="T6">turu͡orbut </ts>
               <ts e="T8" id="Seg_2793" n="e" s="T7">golomo </ts>
               <ts e="T9" id="Seg_2795" n="e" s="T8">dʼi͡eleːk, </ts>
               <ts e="T10" id="Seg_2797" n="e" s="T9">bi͡es </ts>
               <ts e="T11" id="Seg_2799" n="e" s="T10">taːstaːk </ts>
               <ts e="T12" id="Seg_2801" n="e" s="T11">sobo-ilimneːk. </ts>
               <ts e="T13" id="Seg_2803" n="e" s="T12">Kün </ts>
               <ts e="T14" id="Seg_2805" n="e" s="T13">aːjɨ </ts>
               <ts e="T15" id="Seg_2807" n="e" s="T14">ilimnene </ts>
               <ts e="T16" id="Seg_2809" n="e" s="T15">kiːrer. </ts>
               <ts e="T17" id="Seg_2811" n="e" s="T16">Bu </ts>
               <ts e="T18" id="Seg_2813" n="e" s="T17">kihi </ts>
               <ts e="T19" id="Seg_2815" n="e" s="T18">balɨgɨ </ts>
               <ts e="T20" id="Seg_2817" n="e" s="T19">logločču </ts>
               <ts e="T21" id="Seg_2819" n="e" s="T20">musput – </ts>
               <ts e="T22" id="Seg_2821" n="e" s="T21">ilimitten. </ts>
               <ts e="T23" id="Seg_2823" n="e" s="T22">Innʼe </ts>
               <ts e="T24" id="Seg_2825" n="e" s="T23">gɨnan </ts>
               <ts e="T25" id="Seg_2827" n="e" s="T24">biːrde </ts>
               <ts e="T26" id="Seg_2829" n="e" s="T25">barbɨta, </ts>
               <ts e="T27" id="Seg_2831" n="e" s="T26">kɨrsalar, </ts>
               <ts e="T28" id="Seg_2833" n="e" s="T27">tu͡oktar </ts>
               <ts e="T29" id="Seg_2835" n="e" s="T28">hi͡ebitter </ts>
               <ts e="T30" id="Seg_2837" n="e" s="T29">araj </ts>
               <ts e="T31" id="Seg_2839" n="e" s="T30">balɨgɨn. </ts>
               <ts e="T32" id="Seg_2841" n="e" s="T31">"Munu </ts>
               <ts e="T33" id="Seg_2843" n="e" s="T32">irdi͡ekke, </ts>
               <ts e="T34" id="Seg_2845" n="e" s="T33">hu͡olun </ts>
               <ts e="T35" id="Seg_2847" n="e" s="T34">irdi͡ekke." </ts>
               <ts e="T36" id="Seg_2849" n="e" s="T35">Balɨgɨn </ts>
               <ts e="T37" id="Seg_2851" n="e" s="T36">dʼi͡etiger </ts>
               <ts e="T38" id="Seg_2853" n="e" s="T37">tahan </ts>
               <ts e="T39" id="Seg_2855" n="e" s="T38">baraːn, </ts>
               <ts e="T40" id="Seg_2857" n="e" s="T39">bu </ts>
               <ts e="T41" id="Seg_2859" n="e" s="T40">kihi </ts>
               <ts e="T42" id="Seg_2861" n="e" s="T41">kɨrsa </ts>
               <ts e="T43" id="Seg_2863" n="e" s="T42">hu͡olun </ts>
               <ts e="T44" id="Seg_2865" n="e" s="T43">irdeːbit. </ts>
               <ts e="T45" id="Seg_2867" n="e" s="T44">Kɨrsa </ts>
               <ts e="T46" id="Seg_2869" n="e" s="T45">hu͡olun </ts>
               <ts e="T47" id="Seg_2871" n="e" s="T46">irdeːn </ts>
               <ts e="T48" id="Seg_2873" n="e" s="T47">istegine, </ts>
               <ts e="T49" id="Seg_2875" n="e" s="T48">kaja, </ts>
               <ts e="T50" id="Seg_2877" n="e" s="T49">kɨrsa </ts>
               <ts e="T51" id="Seg_2879" n="e" s="T50">tugu </ts>
               <ts e="T52" id="Seg_2881" n="e" s="T51">i </ts>
               <ts e="T53" id="Seg_2883" n="e" s="T52">biːr </ts>
               <ts e="T54" id="Seg_2885" n="e" s="T53">talak </ts>
               <ts e="T55" id="Seg_2887" n="e" s="T54">ihitten </ts>
               <ts e="T56" id="Seg_2889" n="e" s="T55">tura </ts>
               <ts e="T57" id="Seg_2891" n="e" s="T56">ekkireːbit. </ts>
               <ts e="T58" id="Seg_2893" n="e" s="T57">"Ara, </ts>
               <ts e="T59" id="Seg_2895" n="e" s="T58">Čopočuka, </ts>
               <ts e="T60" id="Seg_2897" n="e" s="T59">kihini </ts>
               <ts e="T61" id="Seg_2899" n="e" s="T60">hohuttuŋ </ts>
               <ts e="T62" id="Seg_2901" n="e" s="T61">kaja, </ts>
               <ts e="T63" id="Seg_2903" n="e" s="T62">kajtak </ts>
               <ts e="T64" id="Seg_2905" n="e" s="T63">kelegin?" </ts>
               <ts e="T65" id="Seg_2907" n="e" s="T64">"Kaja, </ts>
               <ts e="T66" id="Seg_2909" n="e" s="T65">balɨkpɨn </ts>
               <ts e="T67" id="Seg_2911" n="e" s="T66">ke </ts>
               <ts e="T68" id="Seg_2913" n="e" s="T67">togo </ts>
               <ts e="T69" id="Seg_2915" n="e" s="T68">u͡orbukkutuj </ts>
               <ts e="T70" id="Seg_2917" n="e" s="T69">ehigi?" </ts>
               <ts e="T71" id="Seg_2919" n="e" s="T70">"Ho, </ts>
               <ts e="T72" id="Seg_2921" n="e" s="T71">min </ts>
               <ts e="T73" id="Seg_2923" n="e" s="T72">u͡orbutum </ts>
               <ts e="T74" id="Seg_2925" n="e" s="T73">du͡o", </ts>
               <ts e="T75" id="Seg_2927" n="e" s="T74">diː-diː </ts>
               <ts e="T76" id="Seg_2929" n="e" s="T75">gɨmmɨt. </ts>
               <ts e="T77" id="Seg_2931" n="e" s="T76">Mantɨŋ </ts>
               <ts e="T78" id="Seg_2933" n="e" s="T77">kutujak </ts>
               <ts e="T79" id="Seg_2935" n="e" s="T78">bu͡olbut, </ts>
               <ts e="T80" id="Seg_2937" n="e" s="T79">kutujak </ts>
               <ts e="T81" id="Seg_2939" n="e" s="T80">bu͡olan </ts>
               <ts e="T82" id="Seg_2941" n="e" s="T81">dʼe, </ts>
               <ts e="T83" id="Seg_2943" n="e" s="T82">ogonnʼoruŋ </ts>
               <ts e="T84" id="Seg_2945" n="e" s="T83">dʼe </ts>
               <ts e="T85" id="Seg_2947" n="e" s="T84">hɨldʼar. </ts>
               <ts e="T86" id="Seg_2949" n="e" s="T85">Baːgɨnɨ </ts>
               <ts e="T87" id="Seg_2951" n="e" s="T86">u͡oban </ts>
               <ts e="T88" id="Seg_2953" n="e" s="T87">keːspit – </ts>
               <ts e="T89" id="Seg_2955" n="e" s="T88">kɨrsata. </ts>
               <ts e="T90" id="Seg_2957" n="e" s="T89">Bagɨŋ </ts>
               <ts e="T91" id="Seg_2959" n="e" s="T90">ihin </ts>
               <ts e="T92" id="Seg_2961" n="e" s="T91">kaja </ts>
               <ts e="T94" id="Seg_2963" n="e" s="T92">(ba-) </ts>
               <ts e="T95" id="Seg_2965" n="e" s="T94">battan </ts>
               <ts e="T96" id="Seg_2967" n="e" s="T95">bɨhan </ts>
               <ts e="T97" id="Seg_2969" n="e" s="T96">taksa </ts>
               <ts e="T99" id="Seg_2971" n="e" s="T97">(ka-) </ts>
               <ts e="T100" id="Seg_2973" n="e" s="T99">taksa </ts>
               <ts e="T101" id="Seg_2975" n="e" s="T100">köppüt. </ts>
               <ts e="T102" id="Seg_2977" n="e" s="T101">Innʼe </ts>
               <ts e="T103" id="Seg_2979" n="e" s="T102">baran </ts>
               <ts e="T104" id="Seg_2981" n="e" s="T103">baːgɨnɨ </ts>
               <ts e="T105" id="Seg_2983" n="e" s="T104">hüler </ts>
               <ts e="T106" id="Seg_2985" n="e" s="T105">da </ts>
               <ts e="T107" id="Seg_2987" n="e" s="T106">karamaːnɨgar </ts>
               <ts e="T108" id="Seg_2989" n="e" s="T107">uktar </ts>
               <ts e="T109" id="Seg_2991" n="e" s="T108">daː, </ts>
               <ts e="T110" id="Seg_2993" n="e" s="T109">kaːman </ts>
               <ts e="T111" id="Seg_2995" n="e" s="T110">ispit. </ts>
               <ts e="T112" id="Seg_2997" n="e" s="T111">Epi͡et </ts>
               <ts e="T113" id="Seg_2999" n="e" s="T112">inni </ts>
               <ts e="T114" id="Seg_3001" n="e" s="T113">di͡ek </ts>
               <ts e="T115" id="Seg_3003" n="e" s="T114">kaːman </ts>
               <ts e="T116" id="Seg_3005" n="e" s="T115">ihen </ts>
               <ts e="T117" id="Seg_3007" n="e" s="T116">uskaːn </ts>
               <ts e="T118" id="Seg_3009" n="e" s="T117">turu͡orbut. </ts>
               <ts e="T119" id="Seg_3011" n="e" s="T118">"Araː, </ts>
               <ts e="T120" id="Seg_3013" n="e" s="T119">Čopočuka, </ts>
               <ts e="T121" id="Seg_3015" n="e" s="T120">tu͡ok, </ts>
               <ts e="T122" id="Seg_3017" n="e" s="T121">kaja </ts>
               <ts e="T123" id="Seg_3019" n="e" s="T122">hirten </ts>
               <ts e="T124" id="Seg_3021" n="e" s="T123">kelegin, </ts>
               <ts e="T125" id="Seg_3023" n="e" s="T124">kihini </ts>
               <ts e="T126" id="Seg_3025" n="e" s="T125">hohutaːktaːtɨŋ", </ts>
               <ts e="T127" id="Seg_3027" n="e" s="T126">diːbit. </ts>
               <ts e="T128" id="Seg_3029" n="e" s="T127">"Kaja, </ts>
               <ts e="T129" id="Seg_3031" n="e" s="T128">balɨkpɨn </ts>
               <ts e="T130" id="Seg_3033" n="e" s="T129">togo </ts>
               <ts e="T131" id="Seg_3035" n="e" s="T130">barɨtɨn </ts>
               <ts e="T132" id="Seg_3037" n="e" s="T131">hi͡ebikkitij, </ts>
               <ts e="T133" id="Seg_3039" n="e" s="T132">balɨkpɨn </ts>
               <ts e="T134" id="Seg_3041" n="e" s="T133">irdiː </ts>
               <ts e="T135" id="Seg_3043" n="e" s="T134">hɨldʼabɨn." </ts>
               <ts e="T136" id="Seg_3045" n="e" s="T135">((PAUSE)) </ts>
               <ts e="T138" id="Seg_3047" n="e" s="T136">Maːgɨ… </ts>
               <ts e="T139" id="Seg_3049" n="e" s="T138">Emi͡e </ts>
               <ts e="T141" id="Seg_3051" n="e" s="T139">kaja… </ts>
               <ts e="T142" id="Seg_3053" n="e" s="T141">Ti͡ej, </ts>
               <ts e="T143" id="Seg_3055" n="e" s="T142">dʼe </ts>
               <ts e="T144" id="Seg_3057" n="e" s="T143">ölörsöllör </ts>
               <ts e="T145" id="Seg_3059" n="e" s="T144">e </ts>
               <ts e="T146" id="Seg_3061" n="e" s="T145">uskaːnɨ </ts>
               <ts e="T147" id="Seg_3063" n="e" s="T146">kɨtta. </ts>
               <ts e="T148" id="Seg_3065" n="e" s="T147">Ölörsönnör </ts>
               <ts e="T149" id="Seg_3067" n="e" s="T148">uskaːnɨ </ts>
               <ts e="T150" id="Seg_3069" n="e" s="T149">tugun </ts>
               <ts e="T151" id="Seg_3071" n="e" s="T150">kihili͡ej </ts>
               <ts e="T152" id="Seg_3073" n="e" s="T151">ol </ts>
               <ts e="T153" id="Seg_3075" n="e" s="T152">kihi– </ts>
               <ts e="T154" id="Seg_3077" n="e" s="T153">paːgɨnɨ </ts>
               <ts e="T155" id="Seg_3079" n="e" s="T154">ölörbüt </ts>
               <ts e="T156" id="Seg_3081" n="e" s="T155">da </ts>
               <ts e="T157" id="Seg_3083" n="e" s="T156">emi͡e </ts>
               <ts e="T158" id="Seg_3085" n="e" s="T157">((PAUSE)) </ts>
               <ts e="T159" id="Seg_3087" n="e" s="T158">karamaːnɨgar </ts>
               <ts e="T160" id="Seg_3089" n="e" s="T159">uktan </ts>
               <ts e="T161" id="Seg_3091" n="e" s="T160">keːspit, </ts>
               <ts e="T162" id="Seg_3093" n="e" s="T161">hülünen. </ts>
               <ts e="T163" id="Seg_3095" n="e" s="T162">Emi͡e </ts>
               <ts e="T164" id="Seg_3097" n="e" s="T163">kaːman </ts>
               <ts e="T165" id="Seg_3099" n="e" s="T164">ispit. </ts>
               <ts e="T166" id="Seg_3101" n="e" s="T165">Kaːman-kaːman </ts>
               <ts e="T167" id="Seg_3103" n="e" s="T166">istegine </ts>
               <ts e="T168" id="Seg_3105" n="e" s="T167">hahɨl </ts>
               <ts e="T169" id="Seg_3107" n="e" s="T168">tura </ts>
               <ts e="T170" id="Seg_3109" n="e" s="T169">ekkireːbit. </ts>
               <ts e="T171" id="Seg_3111" n="e" s="T170">"Oː, </ts>
               <ts e="T172" id="Seg_3113" n="e" s="T171">Čopočuka, </ts>
               <ts e="T173" id="Seg_3115" n="e" s="T172">kihini </ts>
               <ts e="T174" id="Seg_3117" n="e" s="T173">hohuttuŋ, </ts>
               <ts e="T175" id="Seg_3119" n="e" s="T174">kaja, </ts>
               <ts e="T176" id="Seg_3121" n="e" s="T175">kaja </ts>
               <ts e="T177" id="Seg_3123" n="e" s="T176">hirten </ts>
               <ts e="T178" id="Seg_3125" n="e" s="T177">kelegin </ts>
               <ts e="T179" id="Seg_3127" n="e" s="T178">bu", </ts>
               <ts e="T180" id="Seg_3129" n="e" s="T179">di͡ebit. </ts>
               <ts e="T181" id="Seg_3131" n="e" s="T180">"Kaja", </ts>
               <ts e="T182" id="Seg_3133" n="e" s="T181">di͡ebit, </ts>
               <ts e="T183" id="Seg_3135" n="e" s="T182">"ka </ts>
               <ts e="T184" id="Seg_3137" n="e" s="T183">balɨkpɨn </ts>
               <ts e="T185" id="Seg_3139" n="e" s="T184">barɨtɨn </ts>
               <ts e="T186" id="Seg_3141" n="e" s="T185">hi͡ebikkit. </ts>
               <ts e="T187" id="Seg_3143" n="e" s="T186">Ol </ts>
               <ts e="T188" id="Seg_3145" n="e" s="T187">ihin </ts>
               <ts e="T189" id="Seg_3147" n="e" s="T188">irdeːn </ts>
               <ts e="T190" id="Seg_3149" n="e" s="T189">kelebin." </ts>
               <ts e="T191" id="Seg_3151" n="e" s="T190">"Pa, </ts>
               <ts e="T192" id="Seg_3153" n="e" s="T191">en </ts>
               <ts e="T193" id="Seg_3155" n="e" s="T192">balɨkkar </ts>
               <ts e="T194" id="Seg_3157" n="e" s="T193">bihigi </ts>
               <ts e="T195" id="Seg_3159" n="e" s="T194">čugahaːbatakpɨt </ts>
               <ts e="T196" id="Seg_3161" n="e" s="T195">daːganɨ </ts>
               <ts e="T197" id="Seg_3163" n="e" s="T196">össü͡ö. </ts>
               <ts e="T198" id="Seg_3165" n="e" s="T197">Kata </ts>
               <ts e="T199" id="Seg_3167" n="e" s="T198">min </ts>
               <ts e="T200" id="Seg_3169" n="e" s="T199">enigin </ts>
               <ts e="T201" id="Seg_3171" n="e" s="T200">tɨrɨta </ts>
               <ts e="T202" id="Seg_3173" n="e" s="T201">tɨːtan </ts>
               <ts e="T203" id="Seg_3175" n="e" s="T202">hi͡en </ts>
               <ts e="T204" id="Seg_3177" n="e" s="T203">keːhi͡em", </ts>
               <ts e="T205" id="Seg_3179" n="e" s="T204">di͡ebit. </ts>
               <ts e="T206" id="Seg_3181" n="e" s="T205">Dʼe </ts>
               <ts e="T207" id="Seg_3183" n="e" s="T206">ölörsöllör </ts>
               <ts e="T208" id="Seg_3185" n="e" s="T207">bu </ts>
               <ts e="T209" id="Seg_3187" n="e" s="T208">kihiler. </ts>
               <ts e="T210" id="Seg_3189" n="e" s="T209">Čopočuka </ts>
               <ts e="T211" id="Seg_3191" n="e" s="T210">kantɨtɨn </ts>
               <ts e="T212" id="Seg_3193" n="e" s="T211">kihili͡ej: </ts>
               <ts e="T213" id="Seg_3195" n="e" s="T212">Ti͡ere </ts>
               <ts e="T214" id="Seg_3197" n="e" s="T213">battaːbɨt </ts>
               <ts e="T215" id="Seg_3199" n="e" s="T214">da </ts>
               <ts e="T216" id="Seg_3201" n="e" s="T215">ölörön </ts>
               <ts e="T217" id="Seg_3203" n="e" s="T216">keːspit. </ts>
               <ts e="T218" id="Seg_3205" n="e" s="T217">Maːŋɨnɨ </ts>
               <ts e="T219" id="Seg_3207" n="e" s="T218">hülbüt </ts>
               <ts e="T220" id="Seg_3209" n="e" s="T219">da </ts>
               <ts e="T221" id="Seg_3211" n="e" s="T220">karamaːnɨgar </ts>
               <ts e="T222" id="Seg_3213" n="e" s="T221">(karamaːn </ts>
               <ts e="T223" id="Seg_3215" n="e" s="T222">da </ts>
               <ts e="T224" id="Seg_3217" n="e" s="T223">ulakan, </ts>
               <ts e="T225" id="Seg_3219" n="e" s="T224">söktüm). </ts>
               <ts e="T226" id="Seg_3221" n="e" s="T225">Hahɨlɨn </ts>
               <ts e="T227" id="Seg_3223" n="e" s="T226">uktan </ts>
               <ts e="T228" id="Seg_3225" n="e" s="T227">keːspit </ts>
               <ts e="T229" id="Seg_3227" n="e" s="T228">karamaːnɨgar. </ts>
               <ts e="T230" id="Seg_3229" n="e" s="T229">Onton </ts>
               <ts e="T231" id="Seg_3231" n="e" s="T230">kaːman </ts>
               <ts e="T232" id="Seg_3233" n="e" s="T231">ispit. </ts>
               <ts e="T233" id="Seg_3235" n="e" s="T232">Kaːman </ts>
               <ts e="T234" id="Seg_3237" n="e" s="T233">(ihe-) </ts>
               <ts e="T235" id="Seg_3239" n="e" s="T234">istegine, </ts>
               <ts e="T236" id="Seg_3241" n="e" s="T235">hi͡egen </ts>
               <ts e="T237" id="Seg_3243" n="e" s="T236">tura </ts>
               <ts e="T238" id="Seg_3245" n="e" s="T237">ekkireːbit. </ts>
               <ts e="T239" id="Seg_3247" n="e" s="T238">"Kaja, </ts>
               <ts e="T240" id="Seg_3249" n="e" s="T239">Čopočuka, </ts>
               <ts e="T241" id="Seg_3251" n="e" s="T240">kihini </ts>
               <ts e="T242" id="Seg_3253" n="e" s="T241">hohuttuŋ </ts>
               <ts e="T243" id="Seg_3255" n="e" s="T242">kata", </ts>
               <ts e="T244" id="Seg_3257" n="e" s="T243">di͡ebit. </ts>
               <ts e="T245" id="Seg_3259" n="e" s="T244">"Togo </ts>
               <ts e="T246" id="Seg_3261" n="e" s="T245">hɨldʼagɨn </ts>
               <ts e="T247" id="Seg_3263" n="e" s="T246">bu </ts>
               <ts e="T248" id="Seg_3265" n="e" s="T247">di͡en </ts>
               <ts e="T249" id="Seg_3267" n="e" s="T248">dojduga, </ts>
               <ts e="T250" id="Seg_3269" n="e" s="T249">pa?" </ts>
               <ts e="T251" id="Seg_3271" n="e" s="T250">"Ka, </ts>
               <ts e="T252" id="Seg_3273" n="e" s="T251">balɨkpɨn </ts>
               <ts e="T253" id="Seg_3275" n="e" s="T252">togo </ts>
               <ts e="T254" id="Seg_3277" n="e" s="T253">barɨtɨn </ts>
               <ts e="T255" id="Seg_3279" n="e" s="T254">hi͡ebikkitij? </ts>
               <ts e="T256" id="Seg_3281" n="e" s="T255">Onu </ts>
               <ts e="T257" id="Seg_3283" n="e" s="T256">irdiː </ts>
               <ts e="T258" id="Seg_3285" n="e" s="T257">hɨldʼabɨn", </ts>
               <ts e="T259" id="Seg_3287" n="e" s="T258">di͡ebit, </ts>
               <ts e="T260" id="Seg_3289" n="e" s="T259">"össü͡ö </ts>
               <ts e="T261" id="Seg_3291" n="e" s="T260">hi͡ebikkitij." </ts>
               <ts e="T262" id="Seg_3293" n="e" s="T261">Kaban </ts>
               <ts e="T263" id="Seg_3295" n="e" s="T262">ɨlar </ts>
               <ts e="T264" id="Seg_3297" n="e" s="T263">da </ts>
               <ts e="T265" id="Seg_3299" n="e" s="T264">maːŋɨnɨ </ts>
               <ts e="T266" id="Seg_3301" n="e" s="T265">dʼe </ts>
               <ts e="T267" id="Seg_3303" n="e" s="T266">ölörsör. </ts>
               <ts e="T268" id="Seg_3305" n="e" s="T267">Ölörsön </ts>
               <ts e="T269" id="Seg_3307" n="e" s="T268">Čopočuka </ts>
               <ts e="T270" id="Seg_3309" n="e" s="T269">tugun </ts>
               <ts e="T271" id="Seg_3311" n="e" s="T270">kihili͡ej, </ts>
               <ts e="T272" id="Seg_3313" n="e" s="T271">kihileːbetek. </ts>
               <ts e="T273" id="Seg_3315" n="e" s="T272">Ölörbüt </ts>
               <ts e="T274" id="Seg_3317" n="e" s="T273">da, </ts>
               <ts e="T275" id="Seg_3319" n="e" s="T274">emi͡e </ts>
               <ts e="T276" id="Seg_3321" n="e" s="T275">hülbüt </ts>
               <ts e="T277" id="Seg_3323" n="e" s="T276">da </ts>
               <ts e="T278" id="Seg_3325" n="e" s="T277">karmaːnɨgar </ts>
               <ts e="T279" id="Seg_3327" n="e" s="T278">uktan </ts>
               <ts e="T280" id="Seg_3329" n="e" s="T279">keːspit. </ts>
               <ts e="T281" id="Seg_3331" n="e" s="T280">Emi͡e </ts>
               <ts e="T282" id="Seg_3333" n="e" s="T281">kaːman </ts>
               <ts e="T283" id="Seg_3335" n="e" s="T282">ispit. </ts>
               <ts e="T284" id="Seg_3337" n="e" s="T283">Kaːman-kaːman </ts>
               <ts e="T285" id="Seg_3339" n="e" s="T284">börönü </ts>
               <ts e="T286" id="Seg_3341" n="e" s="T285">körsübüt. </ts>
               <ts e="T287" id="Seg_3343" n="e" s="T286">Börötö </ts>
               <ts e="T288" id="Seg_3345" n="e" s="T287">tura </ts>
               <ts e="T289" id="Seg_3347" n="e" s="T288">ekkireːbit. </ts>
               <ts e="T290" id="Seg_3349" n="e" s="T289">"O, </ts>
               <ts e="T291" id="Seg_3351" n="e" s="T290">Čopočuka, </ts>
               <ts e="T292" id="Seg_3353" n="e" s="T291">kihini </ts>
               <ts e="T293" id="Seg_3355" n="e" s="T292">hohuttuŋ, </ts>
               <ts e="T294" id="Seg_3357" n="e" s="T293">ka </ts>
               <ts e="T295" id="Seg_3359" n="e" s="T294">kajdi͡ek </ts>
               <ts e="T296" id="Seg_3361" n="e" s="T295">ihegin", </ts>
               <ts e="T297" id="Seg_3363" n="e" s="T296">di͡ebit. </ts>
               <ts e="T298" id="Seg_3365" n="e" s="T297">"Kaja, </ts>
               <ts e="T299" id="Seg_3367" n="e" s="T298">balɨkpɨn </ts>
               <ts e="T300" id="Seg_3369" n="e" s="T299">togo </ts>
               <ts e="T301" id="Seg_3371" n="e" s="T300">hi͡ebikkitij? </ts>
               <ts e="T302" id="Seg_3373" n="e" s="T301">Ontubun </ts>
               <ts e="T303" id="Seg_3375" n="e" s="T302">irdiːbin." </ts>
               <ts e="T304" id="Seg_3377" n="e" s="T303">"Pa, </ts>
               <ts e="T305" id="Seg_3379" n="e" s="T304">en </ts>
               <ts e="T306" id="Seg_3381" n="e" s="T305">balɨkkar </ts>
               <ts e="T307" id="Seg_3383" n="e" s="T306">čugahaːbatakpɨt </ts>
               <ts e="T308" id="Seg_3385" n="e" s="T307">daːganɨ, </ts>
               <ts e="T309" id="Seg_3387" n="e" s="T308">kata </ts>
               <ts e="T310" id="Seg_3389" n="e" s="T309">min </ts>
               <ts e="T311" id="Seg_3391" n="e" s="T310">enigin </ts>
               <ts e="T312" id="Seg_3393" n="e" s="T311">hi͡en </ts>
               <ts e="T313" id="Seg_3395" n="e" s="T312">keːhi͡em", </ts>
               <ts e="T314" id="Seg_3397" n="e" s="T313">di͡ebit. </ts>
               <ts e="T315" id="Seg_3399" n="e" s="T314">Baːgɨnɨ </ts>
               <ts e="T316" id="Seg_3401" n="e" s="T315">u͡oban </ts>
               <ts e="T317" id="Seg_3403" n="e" s="T316">keːspit, </ts>
               <ts e="T318" id="Seg_3405" n="e" s="T317">ɨŋɨran </ts>
               <ts e="T319" id="Seg_3407" n="e" s="T318">keːspit – </ts>
               <ts e="T320" id="Seg_3409" n="e" s="T319">Čopočukaːnɨ. </ts>
               <ts e="T321" id="Seg_3411" n="e" s="T320">Čopočuka </ts>
               <ts e="T322" id="Seg_3413" n="e" s="T321">ihin </ts>
               <ts e="T323" id="Seg_3415" n="e" s="T322">kaja </ts>
               <ts e="T324" id="Seg_3417" n="e" s="T323">battɨːr </ts>
               <ts e="T325" id="Seg_3419" n="e" s="T324">da, </ts>
               <ts e="T326" id="Seg_3421" n="e" s="T325">taksa </ts>
               <ts e="T327" id="Seg_3423" n="e" s="T326">kötör. </ts>
               <ts e="T328" id="Seg_3425" n="e" s="T327">Innʼe </ts>
               <ts e="T329" id="Seg_3427" n="e" s="T328">gɨmmɨt </ts>
               <ts e="T330" id="Seg_3429" n="e" s="T329">da, </ts>
               <ts e="T331" id="Seg_3431" n="e" s="T330">hülbüt </ts>
               <ts e="T332" id="Seg_3433" n="e" s="T331">da, </ts>
               <ts e="T333" id="Seg_3435" n="e" s="T332">karamaːnɨgar </ts>
               <ts e="T334" id="Seg_3437" n="e" s="T333">uktan </ts>
               <ts e="T335" id="Seg_3439" n="e" s="T334">keːspit. </ts>
               <ts e="T336" id="Seg_3441" n="e" s="T335">Onton </ts>
               <ts e="T337" id="Seg_3443" n="e" s="T336">dʼe </ts>
               <ts e="T338" id="Seg_3445" n="e" s="T337">kaːman </ts>
               <ts e="T339" id="Seg_3447" n="e" s="T338">ispit, </ts>
               <ts e="T340" id="Seg_3449" n="e" s="T339">kaːman </ts>
               <ts e="T341" id="Seg_3451" n="e" s="T340">ispit. </ts>
               <ts e="T342" id="Seg_3453" n="e" s="T341">O, </ts>
               <ts e="T343" id="Seg_3455" n="e" s="T342">ebekeː </ts>
               <ts e="T344" id="Seg_3457" n="e" s="T343">tura </ts>
               <ts e="T345" id="Seg_3459" n="e" s="T344">ekkireːkteːbit </ts>
               <ts e="T346" id="Seg_3461" n="e" s="T345">diːn </ts>
               <ts e="T347" id="Seg_3463" n="e" s="T346">diː, </ts>
               <ts e="T348" id="Seg_3465" n="e" s="T347">araj. </ts>
               <ts e="T349" id="Seg_3467" n="e" s="T348">"Oj, </ts>
               <ts e="T350" id="Seg_3469" n="e" s="T349">Čopočuka, </ts>
               <ts e="T351" id="Seg_3471" n="e" s="T350">togo, </ts>
               <ts e="T352" id="Seg_3473" n="e" s="T351">kaja </ts>
               <ts e="T353" id="Seg_3475" n="e" s="T352">hirge </ts>
               <ts e="T354" id="Seg_3477" n="e" s="T353">hɨldʼagɨn </ts>
               <ts e="T355" id="Seg_3479" n="e" s="T354">bu", </ts>
               <ts e="T356" id="Seg_3481" n="e" s="T355">di͡ebit. </ts>
               <ts e="T357" id="Seg_3483" n="e" s="T356">"Balɨkpɨn </ts>
               <ts e="T358" id="Seg_3485" n="e" s="T357">togo </ts>
               <ts e="T359" id="Seg_3487" n="e" s="T358">u͡orbukkutuj? </ts>
               <ts e="T360" id="Seg_3489" n="e" s="T359">Össü͡ö </ts>
               <ts e="T361" id="Seg_3491" n="e" s="T360">balɨkpɨn </ts>
               <ts e="T362" id="Seg_3493" n="e" s="T361">u͡orbukkut </ts>
               <ts e="T364" id="Seg_3495" n="e" s="T362">(si͡e-)…" </ts>
               <ts e="T365" id="Seg_3497" n="e" s="T364">"Min </ts>
               <ts e="T366" id="Seg_3499" n="e" s="T365">enigin </ts>
               <ts e="T367" id="Seg_3501" n="e" s="T366">u͡oban </ts>
               <ts e="T368" id="Seg_3503" n="e" s="T367">keːhi͡em", </ts>
               <ts e="T369" id="Seg_3505" n="e" s="T368">di͡ebit. </ts>
               <ts e="T370" id="Seg_3507" n="e" s="T369">Hi͡en </ts>
               <ts e="T371" id="Seg_3509" n="e" s="T370">keːspit. </ts>
               <ts e="T372" id="Seg_3511" n="e" s="T371">Ihin </ts>
               <ts e="T373" id="Seg_3513" n="e" s="T372">kaja </ts>
               <ts e="T374" id="Seg_3515" n="e" s="T373">battɨːr </ts>
               <ts e="T375" id="Seg_3517" n="e" s="T374">da </ts>
               <ts e="T376" id="Seg_3519" n="e" s="T375">taksa </ts>
               <ts e="T377" id="Seg_3521" n="e" s="T376">kötör </ts>
               <ts e="T378" id="Seg_3523" n="e" s="T377">bu </ts>
               <ts e="T379" id="Seg_3525" n="e" s="T378">kihi. </ts>
               <ts e="T380" id="Seg_3527" n="e" s="T379">Innʼe </ts>
               <ts e="T381" id="Seg_3529" n="e" s="T380">baraːn </ts>
               <ts e="T382" id="Seg_3531" n="e" s="T381">bahagɨnan </ts>
               <ts e="T383" id="Seg_3533" n="e" s="T382">hülen </ts>
               <ts e="T384" id="Seg_3535" n="e" s="T383">baraːn </ts>
               <ts e="T385" id="Seg_3537" n="e" s="T384">dʼe </ts>
               <ts e="T386" id="Seg_3539" n="e" s="T385">olorbut </ts>
               <ts e="T387" id="Seg_3541" n="e" s="T386">adʼas. </ts>
               <ts e="T388" id="Seg_3543" n="e" s="T387">Bu </ts>
               <ts e="T389" id="Seg_3545" n="e" s="T388">oloron </ts>
               <ts e="T390" id="Seg_3547" n="e" s="T389">dumajdaːbɨt: </ts>
               <ts e="T391" id="Seg_3549" n="e" s="T390">"Bu </ts>
               <ts e="T392" id="Seg_3551" n="e" s="T391">ɨraːktaːgɨ </ts>
               <ts e="T393" id="Seg_3553" n="e" s="T392">kɨːhɨn </ts>
               <ts e="T394" id="Seg_3555" n="e" s="T393">kördüː </ts>
               <ts e="T395" id="Seg_3557" n="e" s="T394">barɨ͡akka. </ts>
               <ts e="T396" id="Seg_3559" n="e" s="T395">ɨraːktaːgɨ </ts>
               <ts e="T397" id="Seg_3561" n="e" s="T396">hogotok </ts>
               <ts e="T398" id="Seg_3563" n="e" s="T397">kɨːstaːk, </ts>
               <ts e="T399" id="Seg_3565" n="e" s="T398">di͡ebittere, </ts>
               <ts e="T400" id="Seg_3567" n="e" s="T399">onu </ts>
               <ts e="T401" id="Seg_3569" n="e" s="T400">kördüː </ts>
               <ts e="T402" id="Seg_3571" n="e" s="T401">barɨ͡akka </ts>
               <ts e="T403" id="Seg_3573" n="e" s="T402">naːda. </ts>
               <ts e="T404" id="Seg_3575" n="e" s="T403">Baččalaːk </ts>
               <ts e="T405" id="Seg_3577" n="e" s="T404">tu͡oktaːk </ts>
               <ts e="T406" id="Seg_3579" n="e" s="T405">kihi </ts>
               <ts e="T407" id="Seg_3581" n="e" s="T406">ke", </ts>
               <ts e="T408" id="Seg_3583" n="e" s="T407">di͡ebit. </ts>
               <ts e="T409" id="Seg_3585" n="e" s="T408">Dʼe </ts>
               <ts e="T410" id="Seg_3587" n="e" s="T409">ɨraːktaːgɨga </ts>
               <ts e="T411" id="Seg_3589" n="e" s="T410">baraːrɨ </ts>
               <ts e="T412" id="Seg_3591" n="e" s="T411">ol </ts>
               <ts e="T413" id="Seg_3593" n="e" s="T412">oloror </ts>
               <ts e="T414" id="Seg_3595" n="e" s="T413">bu </ts>
               <ts e="T415" id="Seg_3597" n="e" s="T414">kihiŋ, </ts>
               <ts e="T416" id="Seg_3599" n="e" s="T415">oloror. </ts>
               <ts e="T417" id="Seg_3601" n="e" s="T416">"Dʼe </ts>
               <ts e="T418" id="Seg_3603" n="e" s="T417">kaːmɨ͡akka </ts>
               <ts e="T419" id="Seg_3605" n="e" s="T418">üčügej." </ts>
               <ts e="T420" id="Seg_3607" n="e" s="T419">Dʼe </ts>
               <ts e="T421" id="Seg_3609" n="e" s="T420">kaːmar </ts>
               <ts e="T422" id="Seg_3611" n="e" s="T421">bu </ts>
               <ts e="T423" id="Seg_3613" n="e" s="T422">kihi. </ts>
               <ts e="T424" id="Seg_3615" n="e" s="T423">Kaːman-kaːman </ts>
               <ts e="T425" id="Seg_3617" n="e" s="T424">biːr </ts>
               <ts e="T426" id="Seg_3619" n="e" s="T425">hirge </ts>
               <ts e="T427" id="Seg_3621" n="e" s="T426">olorbut. </ts>
               <ts e="T428" id="Seg_3623" n="e" s="T427">Oloron </ts>
               <ts e="T429" id="Seg_3625" n="e" s="T428">körüleːbite, </ts>
               <ts e="T430" id="Seg_3627" n="e" s="T429">ɨraːktaːgɨ </ts>
               <ts e="T431" id="Seg_3629" n="e" s="T430">gi͡ene </ts>
               <ts e="T432" id="Seg_3631" n="e" s="T431">kirili͡ehe </ts>
               <ts e="T433" id="Seg_3633" n="e" s="T432">köstör </ts>
               <ts e="T434" id="Seg_3635" n="e" s="T433">araj. </ts>
               <ts e="T435" id="Seg_3637" n="e" s="T434">"Tijeːnibin </ts>
               <ts e="T436" id="Seg_3639" n="e" s="T435">itinne." </ts>
               <ts e="T437" id="Seg_3641" n="e" s="T436">Bu </ts>
               <ts e="T438" id="Seg_3643" n="e" s="T437">kihi </ts>
               <ts e="T439" id="Seg_3645" n="e" s="T438">kaːman-kaːman, </ts>
               <ts e="T440" id="Seg_3647" n="e" s="T439">kaːman-kaːman </ts>
               <ts e="T441" id="Seg_3649" n="e" s="T440">gu͡orakka </ts>
               <ts e="T442" id="Seg_3651" n="e" s="T441">kelbit, </ts>
               <ts e="T443" id="Seg_3653" n="e" s="T442">ɨraːktaːgɨ </ts>
               <ts e="T444" id="Seg_3655" n="e" s="T443">gi͡eniger. </ts>
               <ts e="T445" id="Seg_3657" n="e" s="T444">Kelbite, </ts>
               <ts e="T446" id="Seg_3659" n="e" s="T445">muŋ </ts>
               <ts e="T447" id="Seg_3661" n="e" s="T446">tas </ts>
               <ts e="T448" id="Seg_3663" n="e" s="T447">di͡ek </ts>
               <ts e="T449" id="Seg_3665" n="e" s="T448">kuhagan </ts>
               <ts e="T450" id="Seg_3667" n="e" s="T449">bagajɨ, </ts>
               <ts e="T451" id="Seg_3669" n="e" s="T450">buru͡olaːk </ts>
               <ts e="T452" id="Seg_3671" n="e" s="T451">golomo </ts>
               <ts e="T453" id="Seg_3673" n="e" s="T452">turar. </ts>
               <ts e="T454" id="Seg_3675" n="e" s="T453">Golomo </ts>
               <ts e="T455" id="Seg_3677" n="e" s="T454">turar </ts>
               <ts e="T456" id="Seg_3679" n="e" s="T455">araj. </ts>
               <ts e="T457" id="Seg_3681" n="e" s="T456">"Bunuga </ts>
               <ts e="T458" id="Seg_3683" n="e" s="T457">kiːri͡ekke </ts>
               <ts e="T459" id="Seg_3685" n="e" s="T458">üčügej, </ts>
               <ts e="T460" id="Seg_3687" n="e" s="T459">golomogo." </ts>
               <ts e="T461" id="Seg_3689" n="e" s="T460">Golomogo </ts>
               <ts e="T462" id="Seg_3691" n="e" s="T461">kötön </ts>
               <ts e="T463" id="Seg_3693" n="e" s="T462">tüspüte – </ts>
               <ts e="T464" id="Seg_3695" n="e" s="T463">ogonnʼordoːk </ts>
               <ts e="T465" id="Seg_3697" n="e" s="T464">emeːksin. </ts>
               <ts e="T466" id="Seg_3699" n="e" s="T465">Emeːksine </ts>
               <ts e="T467" id="Seg_3701" n="e" s="T466">karaga </ts>
               <ts e="T468" id="Seg_3703" n="e" s="T467">hu͡ok, </ts>
               <ts e="T469" id="Seg_3705" n="e" s="T468">ogonnʼoro </ts>
               <ts e="T470" id="Seg_3707" n="e" s="T469">hi͡ese </ts>
               <ts e="T471" id="Seg_3709" n="e" s="T470">hečen </ts>
               <ts e="T472" id="Seg_3711" n="e" s="T471">hogus. </ts>
               <ts e="T473" id="Seg_3713" n="e" s="T472">Dʼe </ts>
               <ts e="T474" id="Seg_3715" n="e" s="T473">ahatallar </ts>
               <ts e="T475" id="Seg_3717" n="e" s="T474">manɨ, </ts>
               <ts e="T476" id="Seg_3719" n="e" s="T475">ahɨːr </ts>
               <ts e="T477" id="Seg_3721" n="e" s="T476">bu </ts>
               <ts e="T478" id="Seg_3723" n="e" s="T477">ɨ͡aldʼɨt. </ts>
               <ts e="T479" id="Seg_3725" n="e" s="T478">"Kaja-kaja </ts>
               <ts e="T480" id="Seg_3727" n="e" s="T479">hirten </ts>
               <ts e="T481" id="Seg_3729" n="e" s="T480">kelegin", </ts>
               <ts e="T482" id="Seg_3731" n="e" s="T481">di͡en </ts>
               <ts e="T483" id="Seg_3733" n="e" s="T482">ös </ts>
               <ts e="T484" id="Seg_3735" n="e" s="T483">ɨjɨtallar. </ts>
               <ts e="T485" id="Seg_3737" n="e" s="T484">"Min </ts>
               <ts e="T486" id="Seg_3739" n="e" s="T485">((PAUSE)) </ts>
               <ts e="T487" id="Seg_3741" n="e" s="T486">bi͡es </ts>
               <ts e="T488" id="Seg_3743" n="e" s="T487">taːstaːk </ts>
               <ts e="T489" id="Seg_3745" n="e" s="T488">hobo-ilimneːk </ts>
               <ts e="T490" id="Seg_3747" n="e" s="T489">etim, </ts>
               <ts e="T491" id="Seg_3749" n="e" s="T490">ontubuttan </ts>
               <ts e="T492" id="Seg_3751" n="e" s="T491">balɨgɨ </ts>
               <ts e="T493" id="Seg_3753" n="e" s="T492">munnʼarbɨn, </ts>
               <ts e="T494" id="Seg_3755" n="e" s="T493">u͡ordarammɨn, </ts>
               <ts e="T495" id="Seg_3757" n="e" s="T494">ahɨːlaːktarɨ </ts>
               <ts e="T496" id="Seg_3759" n="e" s="T495">ölörtöːn </ts>
               <ts e="T497" id="Seg_3761" n="e" s="T496">kelebin", </ts>
               <ts e="T498" id="Seg_3763" n="e" s="T497">di͡ebit. </ts>
               <ts e="T499" id="Seg_3765" n="e" s="T498">Eːk, </ts>
               <ts e="T501" id="Seg_3767" n="e" s="T499">(n-) </ts>
               <ts e="T502" id="Seg_3769" n="e" s="T501">ahɨːlaːktarɨ </ts>
               <ts e="T503" id="Seg_3771" n="e" s="T502">ölörön </ts>
               <ts e="T504" id="Seg_3773" n="e" s="T503">kelbit. </ts>
               <ts e="T505" id="Seg_3775" n="e" s="T504">Bu </ts>
               <ts e="T506" id="Seg_3777" n="e" s="T505">ogonnʼotton </ts>
               <ts e="T507" id="Seg_3779" n="e" s="T506">ɨjɨtar, </ts>
               <ts e="T508" id="Seg_3781" n="e" s="T507">"ɨraːktaːgɨ </ts>
               <ts e="T509" id="Seg_3783" n="e" s="T508">kɨːstaːk </ts>
               <ts e="T510" id="Seg_3785" n="e" s="T509">duː", </ts>
               <ts e="T511" id="Seg_3787" n="e" s="T510">di͡en. </ts>
               <ts e="T512" id="Seg_3789" n="e" s="T511">"Hu͡očča-hogotok </ts>
               <ts e="T513" id="Seg_3791" n="e" s="T512">kɨːstaːk, </ts>
               <ts e="T514" id="Seg_3793" n="e" s="T513">kanna </ts>
               <ts e="T515" id="Seg_3795" n="e" s="T514">ere </ts>
               <ts e="T516" id="Seg_3797" n="e" s="T515">((PAUSE)) </ts>
               <ts e="T517" id="Seg_3799" n="e" s="T516">erge </ts>
               <ts e="T518" id="Seg_3801" n="e" s="T517">bi͡ereller </ts>
               <ts e="T519" id="Seg_3803" n="e" s="T518">ühü", </ts>
               <ts e="T520" id="Seg_3805" n="e" s="T519">di͡en. </ts>
               <ts e="T521" id="Seg_3807" n="e" s="T520">"Ogonnʼor, </ts>
               <ts e="T522" id="Seg_3809" n="e" s="T521">en </ts>
               <ts e="T523" id="Seg_3811" n="e" s="T522">hu͡orumdʼuga </ts>
               <ts e="T524" id="Seg_3813" n="e" s="T523">barɨ͡aŋ </ts>
               <ts e="T525" id="Seg_3815" n="e" s="T524">hu͡ok </ts>
               <ts e="T526" id="Seg_3817" n="e" s="T525">duː", </ts>
               <ts e="T527" id="Seg_3819" n="e" s="T526">di͡ebit </ts>
               <ts e="T528" id="Seg_3821" n="e" s="T527">baːjdɨ, </ts>
               <ts e="T529" id="Seg_3823" n="e" s="T528">ɨ͡aldʼɨta. </ts>
               <ts e="T530" id="Seg_3825" n="e" s="T529">"Abaga </ts>
               <ts e="T531" id="Seg_3827" n="e" s="T530">ɨjɨtan </ts>
               <ts e="T532" id="Seg_3829" n="e" s="T531">körü͡ökpüt", </ts>
               <ts e="T533" id="Seg_3831" n="e" s="T532">di͡ebit. </ts>
               <ts e="T534" id="Seg_3833" n="e" s="T533">""Biːr </ts>
               <ts e="T535" id="Seg_3835" n="e" s="T534">ɨ͡aldʼɨt </ts>
               <ts e="T536" id="Seg_3837" n="e" s="T535">kelbit. </ts>
               <ts e="T537" id="Seg_3839" n="e" s="T536">Kɨːskɨn </ts>
               <ts e="T538" id="Seg_3841" n="e" s="T537">kördüːr", </ts>
               <ts e="T539" id="Seg_3843" n="e" s="T538">di͡eŋŋin </ts>
               <ts e="T540" id="Seg_3845" n="e" s="T539">haŋar", </ts>
               <ts e="T541" id="Seg_3847" n="e" s="T540">di͡ebit. </ts>
               <ts e="T542" id="Seg_3849" n="e" s="T541">Bu </ts>
               <ts e="T543" id="Seg_3851" n="e" s="T542">kihi </ts>
               <ts e="T544" id="Seg_3853" n="e" s="T543">baran </ts>
               <ts e="T545" id="Seg_3855" n="e" s="T544">kaːlbɨt, </ts>
               <ts e="T546" id="Seg_3857" n="e" s="T545">ɨraːktaːgɨ </ts>
               <ts e="T547" id="Seg_3859" n="e" s="T546">kötön </ts>
               <ts e="T548" id="Seg_3861" n="e" s="T547">tüspüt. </ts>
               <ts e="T549" id="Seg_3863" n="e" s="T548">"Kaja, </ts>
               <ts e="T550" id="Seg_3865" n="e" s="T549">togo </ts>
               <ts e="T551" id="Seg_3867" n="e" s="T550">kelegin?" </ts>
               <ts e="T552" id="Seg_3869" n="e" s="T551">"Hu͡ok, </ts>
               <ts e="T553" id="Seg_3871" n="e" s="T552">min </ts>
               <ts e="T554" id="Seg_3873" n="e" s="T553">ɨ͡aldʼɨttaːkpɨn, </ts>
               <ts e="T555" id="Seg_3875" n="e" s="T554">kaja-kaja </ts>
               <ts e="T556" id="Seg_3877" n="e" s="T555">hirten </ts>
               <ts e="T557" id="Seg_3879" n="e" s="T556">kelbite </ts>
               <ts e="T558" id="Seg_3881" n="e" s="T557">bu͡olla, </ts>
               <ts e="T559" id="Seg_3883" n="e" s="T558">bilbeppin. </ts>
               <ts e="T560" id="Seg_3885" n="e" s="T559">Ol </ts>
               <ts e="T561" id="Seg_3887" n="e" s="T560">kihi </ts>
               <ts e="T562" id="Seg_3889" n="e" s="T561">kɨːskɨn </ts>
               <ts e="T563" id="Seg_3891" n="e" s="T562">kördöːn </ts>
               <ts e="T564" id="Seg_3893" n="e" s="T563">erer", </ts>
               <ts e="T565" id="Seg_3895" n="e" s="T564">di͡ebit. </ts>
               <ts e="T566" id="Seg_3897" n="e" s="T565">"Innʼe </ts>
               <ts e="T567" id="Seg_3899" n="e" s="T566">gɨnan </ts>
               <ts e="T568" id="Seg_3901" n="e" s="T567">ol </ts>
               <ts e="T569" id="Seg_3903" n="e" s="T568">kihi </ts>
               <ts e="T570" id="Seg_3905" n="e" s="T569">gi͡enin </ts>
               <ts e="T571" id="Seg_3907" n="e" s="T570">hɨrajɨn-karagɨn </ts>
               <ts e="T572" id="Seg_3909" n="e" s="T571">körü͡ökpün, </ts>
               <ts e="T573" id="Seg_3911" n="e" s="T572">manna </ts>
               <ts e="T574" id="Seg_3913" n="e" s="T573">egeliŋ", </ts>
               <ts e="T575" id="Seg_3915" n="e" s="T574">di͡ebit </ts>
               <ts e="T576" id="Seg_3917" n="e" s="T575">ɨraːktaːgɨta. </ts>
               <ts e="T577" id="Seg_3919" n="e" s="T576">Bu </ts>
               <ts e="T578" id="Seg_3921" n="e" s="T577">ogonnʼor </ts>
               <ts e="T579" id="Seg_3923" n="e" s="T578">baːjdɨ </ts>
               <ts e="T580" id="Seg_3925" n="e" s="T579">ɨ͡aldʼɨtɨn </ts>
               <ts e="T581" id="Seg_3927" n="e" s="T580">ilpit. </ts>
               <ts e="T582" id="Seg_3929" n="e" s="T581">"Pa, </ts>
               <ts e="T583" id="Seg_3931" n="e" s="T582">min </ts>
               <ts e="T584" id="Seg_3933" n="e" s="T583">itinnik </ts>
               <ts e="T585" id="Seg_3935" n="e" s="T584">kihige </ts>
               <ts e="T586" id="Seg_3937" n="e" s="T585">bi͡erbeppin, </ts>
               <ts e="T587" id="Seg_3939" n="e" s="T586">iliːtin-atagɨn </ts>
               <ts e="T588" id="Seg_3941" n="e" s="T587">tohutalaːn </ts>
               <ts e="T589" id="Seg_3943" n="e" s="T588">baraːn, </ts>
               <ts e="T590" id="Seg_3945" n="e" s="T589">kaːjɨːga </ts>
               <ts e="T591" id="Seg_3947" n="e" s="T590">bɨragɨŋ", </ts>
               <ts e="T592" id="Seg_3949" n="e" s="T591">di͡ebit. </ts>
               <ts e="T593" id="Seg_3951" n="e" s="T592">Ek. </ts>
               <ts e="T594" id="Seg_3953" n="e" s="T593">Bu </ts>
               <ts e="T595" id="Seg_3955" n="e" s="T594">kihigin </ts>
               <ts e="T596" id="Seg_3957" n="e" s="T595">iliːtin-atagɨn </ts>
               <ts e="T597" id="Seg_3959" n="e" s="T596">tohuppataktar. </ts>
               <ts e="T598" id="Seg_3961" n="e" s="T597">Bɨ͡annan </ts>
               <ts e="T599" id="Seg_3963" n="e" s="T598">kelgijen </ts>
               <ts e="T600" id="Seg_3965" n="e" s="T599">baraːnnar, </ts>
               <ts e="T601" id="Seg_3967" n="e" s="T600">kaːjɨːga </ts>
               <ts e="T602" id="Seg_3969" n="e" s="T601">ugan </ts>
               <ts e="T603" id="Seg_3971" n="e" s="T602">keːspitter, </ts>
               <ts e="T604" id="Seg_3973" n="e" s="T603">maːjdɨ </ts>
               <ts e="T605" id="Seg_3975" n="e" s="T604">kihigin. </ts>
               <ts e="T606" id="Seg_3977" n="e" s="T605">((PAUSE)) </ts>
               <ts e="T607" id="Seg_3979" n="e" s="T606">Bu </ts>
               <ts e="T608" id="Seg_3981" n="e" s="T607">kihi </ts>
               <ts e="T609" id="Seg_3983" n="e" s="T608">kutujak </ts>
               <ts e="T610" id="Seg_3985" n="e" s="T609">bu͡olan </ts>
               <ts e="T611" id="Seg_3987" n="e" s="T610">taksa </ts>
               <ts e="T612" id="Seg_3989" n="e" s="T611">köppüt. </ts>
               <ts e="T613" id="Seg_3991" n="e" s="T612">Taksa </ts>
               <ts e="T614" id="Seg_3993" n="e" s="T613">kötön </ts>
               <ts e="T615" id="Seg_3995" n="e" s="T614">ogonnʼorugar </ts>
               <ts e="T616" id="Seg_3997" n="e" s="T615">emi͡e </ts>
               <ts e="T617" id="Seg_3999" n="e" s="T616">kiːrbit. </ts>
               <ts e="T618" id="Seg_4001" n="e" s="T617">Dʼe </ts>
               <ts e="T619" id="Seg_4003" n="e" s="T618">bilbeppin, </ts>
               <ts e="T620" id="Seg_4005" n="e" s="T619">maːjdɨŋɨtaːgar </ts>
               <ts e="T621" id="Seg_4007" n="e" s="T620">atɨn </ts>
               <ts e="T622" id="Seg_4009" n="e" s="T621">kihi </ts>
               <ts e="T623" id="Seg_4011" n="e" s="T622">kelbite </ts>
               <ts e="T624" id="Seg_4013" n="e" s="T623">duː, </ts>
               <ts e="T625" id="Seg_4015" n="e" s="T624">tu͡ok </ts>
               <ts e="T626" id="Seg_4017" n="e" s="T625">duː, </ts>
               <ts e="T627" id="Seg_4019" n="e" s="T626">emi͡e </ts>
               <ts e="T628" id="Seg_4021" n="e" s="T627">""Kɨːskɨn </ts>
               <ts e="T629" id="Seg_4023" n="e" s="T628">kördöːn </ts>
               <ts e="T630" id="Seg_4025" n="e" s="T629">erer", </ts>
               <ts e="T631" id="Seg_4027" n="e" s="T630">di͡eŋŋin, </ts>
               <ts e="T632" id="Seg_4029" n="e" s="T631">bar </ts>
               <ts e="T633" id="Seg_4031" n="e" s="T632">dʼe", </ts>
               <ts e="T634" id="Seg_4033" n="e" s="T633">di͡ebit. </ts>
               <ts e="T635" id="Seg_4035" n="e" s="T634">Emi͡e </ts>
               <ts e="T636" id="Seg_4037" n="e" s="T635">barbɨt </ts>
               <ts e="T637" id="Seg_4039" n="e" s="T636">ogonnʼor. </ts>
               <ts e="T638" id="Seg_4041" n="e" s="T637">Dʼe </ts>
               <ts e="T639" id="Seg_4043" n="e" s="T638">ol </ts>
               <ts e="T640" id="Seg_4045" n="e" s="T639">baran </ts>
               <ts e="T641" id="Seg_4047" n="e" s="T640">emi͡e </ts>
               <ts e="T642" id="Seg_4049" n="e" s="T641">di͡ebit: </ts>
               <ts e="T643" id="Seg_4051" n="e" s="T642">"Kaja, </ts>
               <ts e="T644" id="Seg_4053" n="e" s="T643">emi͡e </ts>
               <ts e="T645" id="Seg_4055" n="e" s="T644">ɨ͡aldʼɨt </ts>
               <ts e="T646" id="Seg_4057" n="e" s="T645">kelle. </ts>
               <ts e="T647" id="Seg_4059" n="e" s="T646">Kɨːskɨn </ts>
               <ts e="T648" id="Seg_4061" n="e" s="T647">kördöːn </ts>
               <ts e="T649" id="Seg_4063" n="e" s="T648">erer", </ts>
               <ts e="T650" id="Seg_4065" n="e" s="T649">di͡ebit. </ts>
               <ts e="T651" id="Seg_4067" n="e" s="T650">"Dogottor, </ts>
               <ts e="T652" id="Seg_4069" n="e" s="T651">maːjdɨ </ts>
               <ts e="T653" id="Seg_4071" n="e" s="T652">kaːjɨːga </ts>
               <ts e="T654" id="Seg_4073" n="e" s="T653">bɨragaːččɨ </ts>
               <ts e="T655" id="Seg_4075" n="e" s="T654">kihigitin </ts>
               <ts e="T656" id="Seg_4077" n="e" s="T655">körüŋ </ts>
               <ts e="T657" id="Seg_4079" n="e" s="T656">dʼe, </ts>
               <ts e="T658" id="Seg_4081" n="e" s="T657">kajtak-kajtak </ts>
               <ts e="T659" id="Seg_4083" n="e" s="T658">kihini </ts>
               <ts e="T660" id="Seg_4085" n="e" s="T659">dʼabɨlɨːbɨt </ts>
               <ts e="T661" id="Seg_4087" n="e" s="T660">dʼürü", </ts>
               <ts e="T662" id="Seg_4089" n="e" s="T661">di͡ebit. </ts>
               <ts e="T663" id="Seg_4091" n="e" s="T662">Kaːjɨːga </ts>
               <ts e="T664" id="Seg_4093" n="e" s="T663">bɨrakpɨt </ts>
               <ts e="T665" id="Seg_4095" n="e" s="T664">kihite </ts>
               <ts e="T666" id="Seg_4097" n="e" s="T665">hu͡ola </ts>
               <ts e="T667" id="Seg_4099" n="e" s="T666">ere </ts>
               <ts e="T668" id="Seg_4101" n="e" s="T667">čöŋöjö </ts>
               <ts e="T669" id="Seg_4103" n="e" s="T668">hɨtar, </ts>
               <ts e="T670" id="Seg_4105" n="e" s="T669">kelgijbit </ts>
               <ts e="T671" id="Seg_4107" n="e" s="T670">bɨ͡ata </ts>
               <ts e="T672" id="Seg_4109" n="e" s="T671">bɨ͡atɨnan </ts>
               <ts e="T673" id="Seg_4111" n="e" s="T672">hɨtar. </ts>
               <ts e="T674" id="Seg_4113" n="e" s="T673">Dʼe </ts>
               <ts e="T675" id="Seg_4115" n="e" s="T674">kaja, </ts>
               <ts e="T676" id="Seg_4117" n="e" s="T675">onnuk-onnuk, </ts>
               <ts e="T677" id="Seg_4119" n="e" s="T676">kaja. </ts>
               <ts e="T678" id="Seg_4121" n="e" s="T677">"O, </ts>
               <ts e="T679" id="Seg_4123" n="e" s="T678">dʼe </ts>
               <ts e="T680" id="Seg_4125" n="e" s="T679">ile </ts>
               <ts e="T681" id="Seg_4127" n="e" s="T680">da </ts>
               <ts e="T682" id="Seg_4129" n="e" s="T681">üčügej </ts>
               <ts e="T683" id="Seg_4131" n="e" s="T682">kihini </ts>
               <ts e="T684" id="Seg_4133" n="e" s="T683">gɨnnɨm </ts>
               <ts e="T685" id="Seg_4135" n="e" s="T684">e, </ts>
               <ts e="T686" id="Seg_4137" n="e" s="T685">badaga. </ts>
               <ts e="T687" id="Seg_4139" n="e" s="T686">Kɨːspɨn </ts>
               <ts e="T688" id="Seg_4141" n="e" s="T687">bi͡erebin, </ts>
               <ts e="T689" id="Seg_4143" n="e" s="T688">di͡em </ts>
               <ts e="T690" id="Seg_4145" n="e" s="T689">bu͡olla." </ts>
               <ts e="T691" id="Seg_4147" n="e" s="T690">Bu </ts>
               <ts e="T692" id="Seg_4149" n="e" s="T691">kihi </ts>
               <ts e="T693" id="Seg_4151" n="e" s="T692">maːgɨlaːk </ts>
               <ts e="T694" id="Seg_4153" n="e" s="T693">ahɨːlaːk </ts>
               <ts e="T695" id="Seg_4155" n="e" s="T694">ölörbütün </ts>
               <ts e="T696" id="Seg_4157" n="e" s="T695">ɨraːktaːgɨ </ts>
               <ts e="T697" id="Seg_4159" n="e" s="T696">töhögüger </ts>
               <ts e="T698" id="Seg_4161" n="e" s="T697">togo </ts>
               <ts e="T699" id="Seg_4163" n="e" s="T698">bɨrakpɨt. </ts>
               <ts e="T700" id="Seg_4165" n="e" s="T699">Innʼe </ts>
               <ts e="T701" id="Seg_4167" n="e" s="T700">gɨnan </ts>
               <ts e="T702" id="Seg_4169" n="e" s="T701">köhün </ts>
               <ts e="T703" id="Seg_4171" n="e" s="T702">kennitiger </ts>
               <ts e="T704" id="Seg_4173" n="e" s="T703">čɨːčaːk </ts>
               <ts e="T705" id="Seg_4175" n="e" s="T704">ɨllɨːr, </ts>
               <ts e="T706" id="Seg_4177" n="e" s="T705">godolutan </ts>
               <ts e="T707" id="Seg_4179" n="e" s="T706">araːran </ts>
               <ts e="T708" id="Seg_4181" n="e" s="T707">ilpit </ts>
               <ts e="T709" id="Seg_4183" n="e" s="T708">bu </ts>
               <ts e="T710" id="Seg_4185" n="e" s="T709">kihi. </ts>
               <ts e="T711" id="Seg_4187" n="e" s="T710">Eŋin-eŋin </ts>
               <ts e="T712" id="Seg_4189" n="e" s="T711">haldaːttaːk-tu͡oktaːk </ts>
               <ts e="T713" id="Seg_4191" n="e" s="T712">bu </ts>
               <ts e="T714" id="Seg_4193" n="e" s="T713">kihi </ts>
               <ts e="T715" id="Seg_4195" n="e" s="T714">dʼe </ts>
               <ts e="T716" id="Seg_4197" n="e" s="T715">barar </ts>
               <ts e="T717" id="Seg_4199" n="e" s="T716">dʼi͡etiger, </ts>
               <ts e="T718" id="Seg_4201" n="e" s="T717">kɨːhɨn </ts>
               <ts e="T719" id="Seg_4203" n="e" s="T718">ɨlan </ts>
               <ts e="T720" id="Seg_4205" n="e" s="T719">baraːn. </ts>
               <ts e="T721" id="Seg_4207" n="e" s="T720">Dʼe </ts>
               <ts e="T722" id="Seg_4209" n="e" s="T721">köhör. </ts>
               <ts e="T723" id="Seg_4211" n="e" s="T722">Köhön, </ts>
               <ts e="T724" id="Seg_4213" n="e" s="T723">iti </ts>
               <ts e="T725" id="Seg_4215" n="e" s="T724">kelenner, </ts>
               <ts e="T726" id="Seg_4217" n="e" s="T725">iti </ts>
               <ts e="T727" id="Seg_4219" n="e" s="T726">dʼe </ts>
               <ts e="T728" id="Seg_4221" n="e" s="T727">iti </ts>
               <ts e="T729" id="Seg_4223" n="e" s="T728">bajan-toton </ts>
               <ts e="T730" id="Seg_4225" n="e" s="T729">olorbuta, </ts>
               <ts e="T731" id="Seg_4227" n="e" s="T730">dʼe </ts>
               <ts e="T732" id="Seg_4229" n="e" s="T731">iti </ts>
               <ts e="T733" id="Seg_4231" n="e" s="T732">elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_4232" s="T0">PoXN_19701118_Chopochuka_flk.001 (001.001)</ta>
            <ta e="T12" id="Seg_4233" s="T4">PoXN_19701118_Chopochuka_flk.002 (001.002)</ta>
            <ta e="T16" id="Seg_4234" s="T12">PoXN_19701118_Chopochuka_flk.003 (001.003)</ta>
            <ta e="T22" id="Seg_4235" s="T16">PoXN_19701118_Chopochuka_flk.004 (001.004)</ta>
            <ta e="T31" id="Seg_4236" s="T22">PoXN_19701118_Chopochuka_flk.005 (001.005)</ta>
            <ta e="T35" id="Seg_4237" s="T31">PoXN_19701118_Chopochuka_flk.006 (001.006)</ta>
            <ta e="T44" id="Seg_4238" s="T35">PoXN_19701118_Chopochuka_flk.007 (001.007)</ta>
            <ta e="T57" id="Seg_4239" s="T44">PoXN_19701118_Chopochuka_flk.008 (001.008)</ta>
            <ta e="T64" id="Seg_4240" s="T57">PoXN_19701118_Chopochuka_flk.009 (001.009)</ta>
            <ta e="T70" id="Seg_4241" s="T64">PoXN_19701118_Chopochuka_flk.010 (001.010)</ta>
            <ta e="T76" id="Seg_4242" s="T70">PoXN_19701118_Chopochuka_flk.011 (001.011)</ta>
            <ta e="T85" id="Seg_4243" s="T76">PoXN_19701118_Chopochuka_flk.012 (001.012)</ta>
            <ta e="T89" id="Seg_4244" s="T85">PoXN_19701118_Chopochuka_flk.013 (001.013)</ta>
            <ta e="T101" id="Seg_4245" s="T89">PoXN_19701118_Chopochuka_flk.014 (001.014)</ta>
            <ta e="T111" id="Seg_4246" s="T101">PoXN_19701118_Chopochuka_flk.015 (001.015)</ta>
            <ta e="T118" id="Seg_4247" s="T111">PoXN_19701118_Chopochuka_flk.016 (001.016)</ta>
            <ta e="T127" id="Seg_4248" s="T118">PoXN_19701118_Chopochuka_flk.017 (001.017)</ta>
            <ta e="T135" id="Seg_4249" s="T127">PoXN_19701118_Chopochuka_flk.018 (001.018)</ta>
            <ta e="T138" id="Seg_4250" s="T135">PoXN_19701118_Chopochuka_flk.019 (001.019)</ta>
            <ta e="T141" id="Seg_4251" s="T138">PoXN_19701118_Chopochuka_flk.020 (001.020)</ta>
            <ta e="T147" id="Seg_4252" s="T141">PoXN_19701118_Chopochuka_flk.021 (001.021)</ta>
            <ta e="T162" id="Seg_4253" s="T147">PoXN_19701118_Chopochuka_flk.022 (001.022)</ta>
            <ta e="T165" id="Seg_4254" s="T162">PoXN_19701118_Chopochuka_flk.023 (001.023)</ta>
            <ta e="T170" id="Seg_4255" s="T165">PoXN_19701118_Chopochuka_flk.024 (001.024)</ta>
            <ta e="T180" id="Seg_4256" s="T170">PoXN_19701118_Chopochuka_flk.025 (001.025)</ta>
            <ta e="T186" id="Seg_4257" s="T180">PoXN_19701118_Chopochuka_flk.026 (001.026)</ta>
            <ta e="T190" id="Seg_4258" s="T186">PoXN_19701118_Chopochuka_flk.027 (001.027)</ta>
            <ta e="T197" id="Seg_4259" s="T190">PoXN_19701118_Chopochuka_flk.028 (001.028)</ta>
            <ta e="T205" id="Seg_4260" s="T197">PoXN_19701118_Chopochuka_flk.029 (001.029)</ta>
            <ta e="T209" id="Seg_4261" s="T205">PoXN_19701118_Chopochuka_flk.030 (001.030)</ta>
            <ta e="T212" id="Seg_4262" s="T209">PoXN_19701118_Chopochuka_flk.031 (001.031)</ta>
            <ta e="T217" id="Seg_4263" s="T212">PoXN_19701118_Chopochuka_flk.032 (001.032)</ta>
            <ta e="T225" id="Seg_4264" s="T217">PoXN_19701118_Chopochuka_flk.033 (001.033)</ta>
            <ta e="T229" id="Seg_4265" s="T225">PoXN_19701118_Chopochuka_flk.034 (001.034)</ta>
            <ta e="T232" id="Seg_4266" s="T229">PoXN_19701118_Chopochuka_flk.035 (001.035)</ta>
            <ta e="T238" id="Seg_4267" s="T232">PoXN_19701118_Chopochuka_flk.036 (001.036)</ta>
            <ta e="T244" id="Seg_4268" s="T238">PoXN_19701118_Chopochuka_flk.037 (001.037)</ta>
            <ta e="T250" id="Seg_4269" s="T244">PoXN_19701118_Chopochuka_flk.038 (001.038)</ta>
            <ta e="T255" id="Seg_4270" s="T250">PoXN_19701118_Chopochuka_flk.039 (001.039)</ta>
            <ta e="T261" id="Seg_4271" s="T255">PoXN_19701118_Chopochuka_flk.040 (001.040)</ta>
            <ta e="T267" id="Seg_4272" s="T261">PoXN_19701118_Chopochuka_flk.041 (001.041)</ta>
            <ta e="T272" id="Seg_4273" s="T267">PoXN_19701118_Chopochuka_flk.042 (001.042)</ta>
            <ta e="T280" id="Seg_4274" s="T272">PoXN_19701118_Chopochuka_flk.043 (001.043)</ta>
            <ta e="T283" id="Seg_4275" s="T280">PoXN_19701118_Chopochuka_flk.044 (001.044)</ta>
            <ta e="T286" id="Seg_4276" s="T283">PoXN_19701118_Chopochuka_flk.045 (001.045)</ta>
            <ta e="T289" id="Seg_4277" s="T286">PoXN_19701118_Chopochuka_flk.046 (001.046)</ta>
            <ta e="T297" id="Seg_4278" s="T289">PoXN_19701118_Chopochuka_flk.047 (001.047)</ta>
            <ta e="T301" id="Seg_4279" s="T297">PoXN_19701118_Chopochuka_flk.048 (001.048)</ta>
            <ta e="T303" id="Seg_4280" s="T301">PoXN_19701118_Chopochuka_flk.049 (001.049)</ta>
            <ta e="T314" id="Seg_4281" s="T303">PoXN_19701118_Chopochuka_flk.050 (001.050)</ta>
            <ta e="T320" id="Seg_4282" s="T314">PoXN_19701118_Chopochuka_flk.051 (001.051)</ta>
            <ta e="T327" id="Seg_4283" s="T320">PoXN_19701118_Chopochuka_flk.052 (001.052)</ta>
            <ta e="T335" id="Seg_4284" s="T327">PoXN_19701118_Chopochuka_flk.053 (001.053)</ta>
            <ta e="T341" id="Seg_4285" s="T335">PoXN_19701118_Chopochuka_flk.054 (001.054)</ta>
            <ta e="T348" id="Seg_4286" s="T341">PoXN_19701118_Chopochuka_flk.055 (001.055)</ta>
            <ta e="T356" id="Seg_4287" s="T348">PoXN_19701118_Chopochuka_flk.056 (001.056)</ta>
            <ta e="T359" id="Seg_4288" s="T356">PoXN_19701118_Chopochuka_flk.057 (001.057)</ta>
            <ta e="T364" id="Seg_4289" s="T359">PoXN_19701118_Chopochuka_flk.058 (001.058)</ta>
            <ta e="T369" id="Seg_4290" s="T364">PoXN_19701118_Chopochuka_flk.059 (001.059)</ta>
            <ta e="T371" id="Seg_4291" s="T369">PoXN_19701118_Chopochuka_flk.060 (001.060)</ta>
            <ta e="T379" id="Seg_4292" s="T371">PoXN_19701118_Chopochuka_flk.061 (001.061)</ta>
            <ta e="T387" id="Seg_4293" s="T379">PoXN_19701118_Chopochuka_flk.062 (001.062)</ta>
            <ta e="T390" id="Seg_4294" s="T387">PoXN_19701118_Chopochuka_flk.063 (001.063)</ta>
            <ta e="T395" id="Seg_4295" s="T390">PoXN_19701118_Chopochuka_flk.064 (001.064)</ta>
            <ta e="T403" id="Seg_4296" s="T395">PoXN_19701118_Chopochuka_flk.065 (001.065)</ta>
            <ta e="T408" id="Seg_4297" s="T403">PoXN_19701118_Chopochuka_flk.066 (001.066)</ta>
            <ta e="T416" id="Seg_4298" s="T408">PoXN_19701118_Chopochuka_flk.067 (001.067)</ta>
            <ta e="T419" id="Seg_4299" s="T416">PoXN_19701118_Chopochuka_flk.068 (001.068)</ta>
            <ta e="T423" id="Seg_4300" s="T419">PoXN_19701118_Chopochuka_flk.069 (001.069)</ta>
            <ta e="T427" id="Seg_4301" s="T423">PoXN_19701118_Chopochuka_flk.070 (001.070)</ta>
            <ta e="T434" id="Seg_4302" s="T427">PoXN_19701118_Chopochuka_flk.071 (001.071)</ta>
            <ta e="T436" id="Seg_4303" s="T434">PoXN_19701118_Chopochuka_flk.072 (001.072)</ta>
            <ta e="T444" id="Seg_4304" s="T436">PoXN_19701118_Chopochuka_flk.073 (001.073)</ta>
            <ta e="T453" id="Seg_4305" s="T444">PoXN_19701118_Chopochuka_flk.074 (001.074)</ta>
            <ta e="T456" id="Seg_4306" s="T453">PoXN_19701118_Chopochuka_flk.075 (001.075)</ta>
            <ta e="T460" id="Seg_4307" s="T456">PoXN_19701118_Chopochuka_flk.076 (001.076)</ta>
            <ta e="T465" id="Seg_4308" s="T460">PoXN_19701118_Chopochuka_flk.077 (001.077)</ta>
            <ta e="T472" id="Seg_4309" s="T465">PoXN_19701118_Chopochuka_flk.078 (001.078)</ta>
            <ta e="T478" id="Seg_4310" s="T472">PoXN_19701118_Chopochuka_flk.079 (001.079)</ta>
            <ta e="T484" id="Seg_4311" s="T478">PoXN_19701118_Chopochuka_flk.080 (001.080)</ta>
            <ta e="T498" id="Seg_4312" s="T484">PoXN_19701118_Chopochuka_flk.081 (001.081)</ta>
            <ta e="T504" id="Seg_4313" s="T498">PoXN_19701118_Chopochuka_flk.082 (001.082)</ta>
            <ta e="T511" id="Seg_4314" s="T504">PoXN_19701118_Chopochuka_flk.083 (001.083)</ta>
            <ta e="T520" id="Seg_4315" s="T511">PoXN_19701118_Chopochuka_flk.084 (001.084)</ta>
            <ta e="T529" id="Seg_4316" s="T520">PoXN_19701118_Chopochuka_flk.085 (001.085)</ta>
            <ta e="T533" id="Seg_4317" s="T529">PoXN_19701118_Chopochuka_flk.086 (001.086)</ta>
            <ta e="T536" id="Seg_4318" s="T533">PoXN_19701118_Chopochuka_flk.087 (001.087)</ta>
            <ta e="T541" id="Seg_4319" s="T536">PoXN_19701118_Chopochuka_flk.088 (001.088)</ta>
            <ta e="T548" id="Seg_4320" s="T541">PoXN_19701118_Chopochuka_flk.089 (001.089)</ta>
            <ta e="T551" id="Seg_4321" s="T548">PoXN_19701118_Chopochuka_flk.090 (001.090)</ta>
            <ta e="T559" id="Seg_4322" s="T551">PoXN_19701118_Chopochuka_flk.091 (001.091)</ta>
            <ta e="T565" id="Seg_4323" s="T559">PoXN_19701118_Chopochuka_flk.092 (001.092)</ta>
            <ta e="T576" id="Seg_4324" s="T565">PoXN_19701118_Chopochuka_flk.093 (001.093)</ta>
            <ta e="T581" id="Seg_4325" s="T576">PoXN_19701118_Chopochuka_flk.094 (001.094)</ta>
            <ta e="T592" id="Seg_4326" s="T581">PoXN_19701118_Chopochuka_flk.095 (001.095)</ta>
            <ta e="T593" id="Seg_4327" s="T592">PoXN_19701118_Chopochuka_flk.096 (001.096)</ta>
            <ta e="T597" id="Seg_4328" s="T593">PoXN_19701118_Chopochuka_flk.097 (001.097)</ta>
            <ta e="T605" id="Seg_4329" s="T597">PoXN_19701118_Chopochuka_flk.098 (001.098)</ta>
            <ta e="T612" id="Seg_4330" s="T605">PoXN_19701118_Chopochuka_flk.099 (001.099)</ta>
            <ta e="T617" id="Seg_4331" s="T612">PoXN_19701118_Chopochuka_flk.100 (001.100)</ta>
            <ta e="T634" id="Seg_4332" s="T617">PoXN_19701118_Chopochuka_flk.101 (001.101)</ta>
            <ta e="T637" id="Seg_4333" s="T634">PoXN_19701118_Chopochuka_flk.102 (001.102)</ta>
            <ta e="T642" id="Seg_4334" s="T637">PoXN_19701118_Chopochuka_flk.103 (001.103)</ta>
            <ta e="T646" id="Seg_4335" s="T642">PoXN_19701118_Chopochuka_flk.104 (001.103)</ta>
            <ta e="T650" id="Seg_4336" s="T646">PoXN_19701118_Chopochuka_flk.105 (001.104)</ta>
            <ta e="T662" id="Seg_4337" s="T650">PoXN_19701118_Chopochuka_flk.106 (001.105)</ta>
            <ta e="T673" id="Seg_4338" s="T662">PoXN_19701118_Chopochuka_flk.107 (001.106)</ta>
            <ta e="T677" id="Seg_4339" s="T673">PoXN_19701118_Chopochuka_flk.108 (001.107)</ta>
            <ta e="T686" id="Seg_4340" s="T677">PoXN_19701118_Chopochuka_flk.109 (001.108)</ta>
            <ta e="T690" id="Seg_4341" s="T686">PoXN_19701118_Chopochuka_flk.110 (001.109)</ta>
            <ta e="T699" id="Seg_4342" s="T690">PoXN_19701118_Chopochuka_flk.111 (001.110)</ta>
            <ta e="T710" id="Seg_4343" s="T699">PoXN_19701118_Chopochuka_flk.112 (001.111)</ta>
            <ta e="T720" id="Seg_4344" s="T710">PoXN_19701118_Chopochuka_flk.113 (001.112)</ta>
            <ta e="T722" id="Seg_4345" s="T720">PoXN_19701118_Chopochuka_flk.114 (001.113)</ta>
            <ta e="T733" id="Seg_4346" s="T722">PoXN_19701118_Chopochuka_flk.115 (001.114)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_4347" s="T0">Чопочука олорбут үһү.</ta>
            <ta e="T12" id="Seg_4348" s="T4">Биэс мас туруорбак голомо дьиэлээк, биэс таастаак собо-илимнээк.</ta>
            <ta e="T16" id="Seg_4349" s="T12">Күн аайы илимнэнэ киирэр.</ta>
            <ta e="T22" id="Seg_4350" s="T16">Бу киһи балыгы логлоччу муспут – илимиттэн.</ta>
            <ta e="T31" id="Seg_4351" s="T22">Инньэ гынан биирдэ барбыта – кырсалар, туоктар һиэбиттэр арай балыгын. </ta>
            <ta e="T35" id="Seg_4352" s="T31">"Муну ирдиэккэ, һуолун ирдиэккэ".</ta>
            <ta e="T44" id="Seg_4353" s="T35">Балыгын дьиэтигэр таһан бараан, бу киһи кырса һуолун ирдээбит.</ta>
            <ta e="T57" id="Seg_4354" s="T44">Кырса һуолун ирдээн истэгинэ, кайа, кырса тугу и биир талак иһиттэн тура эккирээбит. </ta>
            <ta e="T64" id="Seg_4355" s="T57">"Ара, Чопочука, киһини һоһуттуӈ кайа, кайтан (кантан) кэлэгин?"</ta>
            <ta e="T70" id="Seg_4356" s="T64">"Кайа, балыкпын киэ того уорбуккутуй эһиги?"</ta>
            <ta e="T76" id="Seg_4357" s="T70">"Һо, мин уорбутум дуо?" – диити гыммыт.</ta>
            <ta e="T85" id="Seg_4358" s="T76">Мантыӈ кутуйак буолбут, кутуйак буолан дьэ, огонньоруӈ дьэ һылдьар.</ta>
            <ta e="T89" id="Seg_4359" s="T85">Баагыны уобан кээспит – кырсата. </ta>
            <ta e="T101" id="Seg_4360" s="T89">Багыӈ иһин кайа ба.. баттан быһан такса ка.. такса көппүт.</ta>
            <ta e="T111" id="Seg_4361" s="T101">Инньэ баран баагыны һүлэр да карамааныгар уктар даа, кааман испит.</ta>
            <ta e="T118" id="Seg_4362" s="T111">Эпиэт инни диэк кааман иһэн ускаан туруорбут.</ta>
            <ta e="T127" id="Seg_4363" s="T118">"Ара, Чопочука, туок, кайа һиртэн кэлэгин, киһини һоһутаактаатыӈ?"– диибит.</ta>
            <ta e="T135" id="Seg_4364" s="T127">"Кайа, балыкпын того барытын һиэбиккитий, балыкпын ирдии һылдьабын". </ta>
            <ta e="T138" id="Seg_4365" s="T135">Маагы</ta>
            <ta e="T141" id="Seg_4366" s="T138">эмиэ кайа …</ta>
            <ta e="T147" id="Seg_4367" s="T141">тиэй, дьэ өлөрсөллөр э ускааны гытта.</ta>
            <ta e="T162" id="Seg_4368" s="T147">Өлөрсөннөр ускааны тугун киһилиэй ол киһи – паагыны (баһагынан?) өлөрбүт да эмиэ карамааныгар уктан кээспит, һүлүнэн.</ta>
            <ta e="T165" id="Seg_4369" s="T162">Эмиэ кааман испит. </ta>
            <ta e="T170" id="Seg_4370" s="T165">Кааман-кааман истэгинэ һаһыл тура эккирээбит.</ta>
            <ta e="T180" id="Seg_4371" s="T170">"О, Чопочука, киһини һоһуттуӈ. Кайа, кайа һиртэн кэлэгин бу?"– диэбит.</ta>
            <ta e="T186" id="Seg_4372" s="T180">"Кайа, – диэбит, – ка балыкпын барытын һиэбиккит.</ta>
            <ta e="T190" id="Seg_4373" s="T186">Ол иһин ирдээн кэлэбин".</ta>
            <ta e="T197" id="Seg_4374" s="T190">"Па, эн балыккар биһиги чугаһаабатакпыт дааганы өссүө". </ta>
            <ta e="T205" id="Seg_4375" s="T197">"Ката мин энигин тырыта тыытан һиэн кээһиэм" – диэбит.</ta>
            <ta e="T209" id="Seg_4376" s="T205">Дьэ өлөрсөллөр бу киһилэр.</ta>
            <ta e="T212" id="Seg_4377" s="T209">Чопочука кантытын киһилиэй:</ta>
            <ta e="T217" id="Seg_4378" s="T212">тиэрэ баттабыт да өлөрөн кээспит.</ta>
            <ta e="T225" id="Seg_4379" s="T217">Мааӈыны һүлбүт да карамааныгар (карамаана да улакана бэрт, сөктүм).</ta>
            <ta e="T229" id="Seg_4380" s="T225">Һаһылын уктан кээспит карамааныгар. </ta>
            <ta e="T232" id="Seg_4381" s="T229">Онтон кааман испит.</ta>
            <ta e="T238" id="Seg_4382" s="T232">Кааман и.. истэгинэ, һиэгэн тура эккирээбит.</ta>
            <ta e="T244" id="Seg_4383" s="T238">"Кайа, Чопочука, киһини һоһуттуӈ ката, – диэбит. </ta>
            <ta e="T250" id="Seg_4384" s="T244">– Того һылдьагын бу диэн дойдуга? Па."</ta>
            <ta e="T255" id="Seg_4385" s="T250">"Ка, былыкпыт того барытын һиэбиккитий?</ta>
            <ta e="T261" id="Seg_4386" s="T255">Ону ирдии һылдьабын, – диэбит, – өссүө һиэбиккитий".</ta>
            <ta e="T267" id="Seg_4387" s="T261">Кабан ылар да мааӈыны дьэ өлөрсөр. </ta>
            <ta e="T272" id="Seg_4388" s="T267">Өлөрсөн Чопочука тугун киһилиэй, киһилээбэтэк.</ta>
            <ta e="T280" id="Seg_4389" s="T272">Өлөрбүт да, эмиэ һүлбүт да кармааныгар уктан кээспит.</ta>
            <ta e="T283" id="Seg_4390" s="T280">Эмиэ кааман испит.</ta>
            <ta e="T286" id="Seg_4391" s="T283">Кааман-кааман бөрөнү көрсүбүт.</ta>
            <ta e="T289" id="Seg_4392" s="T286">Бөрөтө тура эккирээбит.</ta>
            <ta e="T297" id="Seg_4393" s="T289">"О, Чопочука, киһини һоһуттуӈ, ка кайдиэк иһэгин?" – диэбит.</ta>
            <ta e="T301" id="Seg_4394" s="T297">"Кайа, балыкпын того һиэбиккитий?</ta>
            <ta e="T303" id="Seg_4395" s="T301">Онтубун ирдиибин". </ta>
            <ta e="T314" id="Seg_4396" s="T303">"Па. Эн балыккар чугаһаабатакпыт дааганы". "Ката мин энигин һиэн кээһиэм", – диэбит.</ta>
            <ta e="T320" id="Seg_4397" s="T314">Баагыны уобан кээспит, ыӈыран кээспит – Чопочукааны. </ta>
            <ta e="T327" id="Seg_4398" s="T320">Чопочука иһин кайа баттыыр да, такса көтөр.</ta>
            <ta e="T335" id="Seg_4399" s="T327">Инньэ гыммыт да, һүлбут да, карамааныгар уктан кээспит. </ta>
            <ta e="T341" id="Seg_4400" s="T335">Онтон дьэ кааман испит, кааман испит.</ta>
            <ta e="T348" id="Seg_4401" s="T341">О, эбэкээ тураа эккирээктээбит диин, арай. </ta>
            <ta e="T356" id="Seg_4402" s="T348">"О, Чопочука, того, кайа һиргэ һылдьагын бу?" – диэбит.</ta>
            <ta e="T359" id="Seg_4403" s="T356">"Балыкпын того уорбуккутуй? </ta>
            <ta e="T364" id="Seg_4404" s="T359">Өссүө балыкпын уорбуккут сиэ.. </ta>
            <ta e="T369" id="Seg_4405" s="T364">мин энигин уобан кээһиэм", – диэбит.</ta>
            <ta e="T371" id="Seg_4406" s="T369">Һиэн кээспит. </ta>
            <ta e="T379" id="Seg_4407" s="T371">Иһин кайа баттыыр да такса көтөр бу киһи.</ta>
            <ta e="T387" id="Seg_4408" s="T379">Инньэ бараан баһагынан һүлэн бараан дьэ олорбут адьас.</ta>
            <ta e="T390" id="Seg_4409" s="T387">Бу олорон думайдаабыт:</ta>
            <ta e="T395" id="Seg_4410" s="T390">"Бу ыраактаагы кыыһын көрдүү барыакка.</ta>
            <ta e="T403" id="Seg_4411" s="T395">Ыраактаагы һоготок кыыстаак, диэбиттэрэ, ону көрдүү барыаккта наада.</ta>
            <ta e="T408" id="Seg_4412" s="T403">Баччалаак туоктаак киһи киэ", – диэбит.</ta>
            <ta e="T416" id="Seg_4413" s="T408">Дьэ ыраактаагыга бараары ол олорор бу киһиӈ, олорор.</ta>
            <ta e="T419" id="Seg_4414" s="T416">"Дьэ каамыакка үчүгэй".</ta>
            <ta e="T423" id="Seg_4415" s="T419">Дьэ каамар бу киһи.</ta>
            <ta e="T427" id="Seg_4416" s="T423">Кааман-кааман биир һиргэ олорбут.</ta>
            <ta e="T434" id="Seg_4417" s="T427">Олорон көрүлээбитэ, ыраактаагы гиэ.. кирилиэһэ көстөр арай. </ta>
            <ta e="T436" id="Seg_4418" s="T434">"Тийээнибин итиннэ".</ta>
            <ta e="T444" id="Seg_4419" s="T436">Бу киһи кааман-кааман, кааман-кааман гуоракка кэлбит, ити ыраактаагы гиэнигэр.</ta>
            <ta e="T453" id="Seg_4420" s="T444">Кэлбитэ, муӈ тас диэк куһаган багаи, буруолаак голомо турар.</ta>
            <ta e="T456" id="Seg_4421" s="T453">Голомо турар арай.</ta>
            <ta e="T460" id="Seg_4422" s="T456">"Бунуга киириэккэ үчүгэй, голомогоголомого </ta>
            <ta e="T465" id="Seg_4423" s="T460">Голомого көтөн түспүтэ – огонньодоок эмээксин. </ta>
            <ta e="T472" id="Seg_4424" s="T465">Эмээксинэ карага һуок, огонньоро һиэсэ һэчэн һогус. </ta>
            <ta e="T478" id="Seg_4425" s="T472">Дьэ аһаталлар, маны аһыыр бу ыалдьыт.</ta>
            <ta e="T484" id="Seg_4426" s="T478">"Кайа-кайа һиртэн кэлэгин?" – диэн өс ыйыталлар. </ta>
            <ta e="T498" id="Seg_4427" s="T484">"Мин биэс таастаак һобо- илимтээк этим, онтубуттан балыгы мунньарбын, уордараммын, аһыылаактары өлөртөөт кэлэбит, – диэбит.</ta>
            <ta e="T504" id="Seg_4428" s="T498">Эк, н аһыылаактары өлөрөн кэлбит. </ta>
            <ta e="T511" id="Seg_4429" s="T504">Бу огонньотон ыйытар, ыраактаагы кыыстаак дуу, диэн.</ta>
            <ta e="T520" id="Seg_4430" s="T511">Һуочча һоготок кыыстаак, канна эрэ эргэ биэрэллэр үһү, диэн.</ta>
            <ta e="T529" id="Seg_4431" s="T520">"Огонньор, эн һуорумдьуга барыаӈ һуок дуу? – диэбит баайды, ыалдьыта.</ta>
            <ta e="T533" id="Seg_4432" s="T529">"Абага ыйытан көрүөкпүт", – диэбит.</ta>
            <ta e="T536" id="Seg_4433" s="T533">Биир ыалдьыт кэлбит. </ta>
            <ta e="T541" id="Seg_4434" s="T536">Кыыскын көрдүүр, диэӈӈин һаӈар, диэбит.</ta>
            <ta e="T548" id="Seg_4435" s="T541">Бу киһи баран каалбыт. Ыраактаагы көтөн түспүт. </ta>
            <ta e="T551" id="Seg_4436" s="T548">"Кайа, того кэлэгин?" </ta>
            <ta e="T559" id="Seg_4437" s="T551">"Һуок. Мин ыалдьыттаакпын. Кайа-кайа һиртэн кэлбитэ буолла, билбэппин.</ta>
            <ta e="T565" id="Seg_4438" s="T559">Ол киһи кыыскын көрдөөн эрэр, "– диэбит. </ta>
            <ta e="T576" id="Seg_4439" s="T565">"Инньэ гынан ол киһи гиэнин һырайын-карагын көрүөкпүн, манна эгэлиӈ", – диэбит ыраактаагыта.</ta>
            <ta e="T581" id="Seg_4440" s="T576">Бу огонньор баайды ыалдьытын илпит. </ta>
            <ta e="T592" id="Seg_4441" s="T581">"Па, мин итинник киһигэ биэрбэппит. Илиитин-атагын тоһуталаан бараан, каайыыга бырагыӈ", – диэбит.</ta>
            <ta e="T593" id="Seg_4442" s="T592">Эк.</ta>
            <ta e="T597" id="Seg_4443" s="T593">Бу киһигин илиитин-атагын тоһуппатактар.</ta>
            <ta e="T605" id="Seg_4444" s="T597">Быаннан кэлгийэн барааннар, каайыыга уган кээспиттэр, маайды киһигин.</ta>
            <ta e="T612" id="Seg_4445" s="T605">… Бу киһи кутайак буолан такса көппүт.</ta>
            <ta e="T617" id="Seg_4446" s="T612">Такса көтөн огонньоругар эмиэ киирбит.</ta>
            <ta e="T634" id="Seg_4447" s="T617">Дьэ билбэппин, дьэ маайдыгытаагар атын киһи кэлбитэ дуу, туок дуу, эмиэ – "Кыыскын көрдөөн эрэр, диэӈӈин, бар дьэ", – диэбит. </ta>
            <ta e="T637" id="Seg_4448" s="T634">Эмиэ барбыт огонньо.</ta>
            <ta e="T642" id="Seg_4449" s="T637">Дьэ ол баран эмиэ диэбит: </ta>
            <ta e="T646" id="Seg_4450" s="T642">"Кайа, эмиэ ыалдьыт кэллэ.</ta>
            <ta e="T650" id="Seg_4451" s="T646">Кыыскын көрдөөн эрэр", – диэбит.</ta>
            <ta e="T662" id="Seg_4452" s="T650">"Доготтор, маайды каайыыга бырагааччы киһигитин көрүӈ дьэ, кайдак-кайдак киһини дьабылыыбыт дьүрү", – диэбит.</ta>
            <ta e="T673" id="Seg_4453" s="T662">Каайыыга быракпыт киһитэ һуола эрэ чөӈөйө һытар: кэлгийбит быата быатынан һытар.</ta>
            <ta e="T677" id="Seg_4454" s="T673">Дьэ кайа, оннук-оннук, кайа. </ta>
            <ta e="T686" id="Seg_4455" s="T677">"О, дьэ илэ да үчүгэй киһини гынным э, бадага.</ta>
            <ta e="T690" id="Seg_4456" s="T686">Кыыспын биэрэбит, диэм буолла".</ta>
            <ta e="T699" id="Seg_4457" s="T690">Бу киһи маагылаак аһыылаак өлөрбүтүн ыраактаагы төһөгүгэр того быракпыт.</ta>
            <ta e="T710" id="Seg_4458" s="T699">Инньэ гынан көһүн кэннитигэр чыычаак ыллыыр, годолутан арааран илпит бу киһи.</ta>
            <ta e="T720" id="Seg_4459" s="T710">Эӈин-эӈин һалдааттаак-туоктаак бу киһи дьэ барар дьиэтигэр, кыыһын ылан бараан.</ta>
            <ta e="T722" id="Seg_4460" s="T720">Дьэ көһөр.</ta>
            <ta e="T733" id="Seg_4461" s="T722">Көһөн, ити кэлэннэр, ити дьэ ити байан-тотон олорбута. Дьэ ити элэтэ. </ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_4462" s="T0">((PAUSE)) Čopočuka olorbut ühü. </ta>
            <ta e="T12" id="Seg_4463" s="T4">Bi͡es mas turu͡orbut golomo dʼi͡eleːk, bi͡es taːstaːk sobo-ilimneːk. </ta>
            <ta e="T16" id="Seg_4464" s="T12">Kün aːjɨ ilimnene kiːrer. </ta>
            <ta e="T22" id="Seg_4465" s="T16">Bu kihi balɨgɨ logločču musput – ilimitten. </ta>
            <ta e="T31" id="Seg_4466" s="T22">Innʼe gɨnan biːrde barbɨta, kɨrsalar, tu͡oktar hi͡ebitter araj balɨgɨn. </ta>
            <ta e="T35" id="Seg_4467" s="T31">"Munu irdi͡ekke, hu͡olun irdi͡ekke." </ta>
            <ta e="T44" id="Seg_4468" s="T35">Balɨgɨn dʼi͡etiger tahan baraːn, bu kihi kɨrsa hu͡olun irdeːbit. </ta>
            <ta e="T57" id="Seg_4469" s="T44">Kɨrsa hu͡olun irdeːn istegine, kaja, kɨrsa tugu i biːr talak ihitten tura ekkireːbit. </ta>
            <ta e="T64" id="Seg_4470" s="T57">"Ara, Čopočuka, kihini hohuttuŋ kaja, kajtak kelegin?" </ta>
            <ta e="T70" id="Seg_4471" s="T64">"Kaja, balɨkpɨn ke togo u͡orbukkutuj ehigi?" </ta>
            <ta e="T76" id="Seg_4472" s="T70">"Ho, min u͡orbutum du͡o", diː-diː gɨmmɨt. </ta>
            <ta e="T85" id="Seg_4473" s="T76">Mantɨŋ kutujak bu͡olbut, kutujak bu͡olan dʼe, ogonnʼoruŋ dʼe hɨldʼar. </ta>
            <ta e="T89" id="Seg_4474" s="T85">Baːgɨnɨ u͡oban keːspit – kɨrsata. </ta>
            <ta e="T101" id="Seg_4475" s="T89">Bagɨŋ ihin kaja (ba-) battan bɨhan taksa (ka-) taksa köppüt. </ta>
            <ta e="T111" id="Seg_4476" s="T101">Innʼe baran baːgɨnɨ hüler da karamaːnɨgar uktar daː, kaːman ispit. </ta>
            <ta e="T118" id="Seg_4477" s="T111">Epi͡et inni di͡ek kaːman ihen uskaːn turu͡orbut. </ta>
            <ta e="T127" id="Seg_4478" s="T118">"Araː, Čopočuka, tu͡ok, kaja hirten kelegin, kihini hohutaːktaːtɨŋ", diːbit. </ta>
            <ta e="T135" id="Seg_4479" s="T127">"Kaja, balɨkpɨn togo barɨtɨn hi͡ebikkitij, balɨkpɨn irdiː hɨldʼabɨn." </ta>
            <ta e="T138" id="Seg_4480" s="T135">((PAUSE)) Maːgɨ… </ta>
            <ta e="T141" id="Seg_4481" s="T138">Emi͡e kaja… </ta>
            <ta e="T147" id="Seg_4482" s="T141">Ti͡ej, dʼe ölörsöllör e uskaːnɨ kɨtta. </ta>
            <ta e="T162" id="Seg_4483" s="T147">Ölörsönnör uskaːnɨ tugun kihili͡ej ol kihi– paːgɨnɨ ölörbüt da emi͡e ((PAUSE)) karamaːnɨgar uktan keːspit, hülünen. </ta>
            <ta e="T165" id="Seg_4484" s="T162">Emi͡e kaːman ispit. </ta>
            <ta e="T170" id="Seg_4485" s="T165">Kaːman-kaːman istegine hahɨl tura ekkireːbit. </ta>
            <ta e="T180" id="Seg_4486" s="T170">"Oː, Čopočuka, kihini hohuttuŋ, kaja, kaja hirten kelegin bu", di͡ebit. </ta>
            <ta e="T186" id="Seg_4487" s="T180">"Kaja", di͡ebit, "ka balɨkpɨn barɨtɨn hi͡ebikkit. </ta>
            <ta e="T190" id="Seg_4488" s="T186">Ol ihin irdeːn kelebin." </ta>
            <ta e="T197" id="Seg_4489" s="T190">"Pa, en balɨkkar bihigi čugahaːbatakpɨt daːganɨ össü͡ö. </ta>
            <ta e="T205" id="Seg_4490" s="T197">Kata min enigin tɨrɨta tɨːtan hi͡en keːhi͡em", di͡ebit. </ta>
            <ta e="T209" id="Seg_4491" s="T205">Dʼe ölörsöllör bu kihiler. </ta>
            <ta e="T212" id="Seg_4492" s="T209">Čopočuka kantɨtɨn kihili͡ej: </ta>
            <ta e="T217" id="Seg_4493" s="T212">Ti͡ere battaːbɨt da ölörön keːspit. </ta>
            <ta e="T225" id="Seg_4494" s="T217">Maːŋɨnɨ hülbüt da karamaːnɨgar (karamaːn da ulakan, söktüm). </ta>
            <ta e="T229" id="Seg_4495" s="T225">Hahɨlɨn uktan keːspit karamaːnɨgar. </ta>
            <ta e="T232" id="Seg_4496" s="T229">Onton kaːman ispit. </ta>
            <ta e="T238" id="Seg_4497" s="T232">Kaːman (ihe-) istegine, hi͡egen tura ekkireːbit. </ta>
            <ta e="T244" id="Seg_4498" s="T238">"Kaja, Čopočuka, kihini hohuttuŋ kata", di͡ebit. </ta>
            <ta e="T250" id="Seg_4499" s="T244">"Togo hɨldʼagɨn bu di͡en dojduga, pa?" </ta>
            <ta e="T255" id="Seg_4500" s="T250">"Ka, balɨkpɨn togo barɨtɨn hi͡ebikkitij? </ta>
            <ta e="T261" id="Seg_4501" s="T255">Onu irdiː hɨldʼabɨn", di͡ebit, "össü͡ö hi͡ebikkitij." </ta>
            <ta e="T267" id="Seg_4502" s="T261">Kaban ɨlar da maːŋɨnɨ dʼe ölörsör. </ta>
            <ta e="T272" id="Seg_4503" s="T267">Ölörsön Čopočuka tugun kihili͡ej, kihileːbetek. </ta>
            <ta e="T280" id="Seg_4504" s="T272">Ölörbüt da, emi͡e hülbüt da karmaːnɨgar uktan keːspit. </ta>
            <ta e="T283" id="Seg_4505" s="T280">Emi͡e kaːman ispit. </ta>
            <ta e="T286" id="Seg_4506" s="T283">Kaːman-kaːman börönü körsübüt. </ta>
            <ta e="T289" id="Seg_4507" s="T286">Börötö tura ekkireːbit. </ta>
            <ta e="T297" id="Seg_4508" s="T289">"O, Čopočuka, kihini hohuttuŋ, ka kajdi͡ek ihegin", di͡ebit. </ta>
            <ta e="T301" id="Seg_4509" s="T297">"Kaja, balɨkpɨn togo hi͡ebikkitij? </ta>
            <ta e="T303" id="Seg_4510" s="T301">Ontubun irdiːbin." </ta>
            <ta e="T314" id="Seg_4511" s="T303">"Pa, en balɨkkar čugahaːbatakpɨt daːganɨ, kata min enigin hi͡en keːhi͡em", di͡ebit. </ta>
            <ta e="T320" id="Seg_4512" s="T314">Baːgɨnɨ u͡oban keːspit, ɨŋɨran keːspit – Čopočukaːnɨ. </ta>
            <ta e="T327" id="Seg_4513" s="T320">Čopočuka ihin kaja battɨːr da, taksa kötör. </ta>
            <ta e="T335" id="Seg_4514" s="T327">Innʼe gɨmmɨt da, hülbüt da, karamaːnɨgar uktan keːspit. </ta>
            <ta e="T341" id="Seg_4515" s="T335">Onton dʼe kaːman ispit, kaːman ispit. </ta>
            <ta e="T348" id="Seg_4516" s="T341">O, ebekeː tura ekkireːkteːbit diːn diː, araj. </ta>
            <ta e="T356" id="Seg_4517" s="T348">"Oj, Čopočuka, togo, kaja hirge hɨldʼagɨn bu", di͡ebit. </ta>
            <ta e="T359" id="Seg_4518" s="T356">"Balɨkpɨn togo u͡orbukkutuj? </ta>
            <ta e="T364" id="Seg_4519" s="T359">Össü͡ö balɨkpɨn u͡orbukkut (si͡e-)…" </ta>
            <ta e="T369" id="Seg_4520" s="T364">"Min enigin u͡oban keːhi͡em", di͡ebit. </ta>
            <ta e="T371" id="Seg_4521" s="T369">Hi͡en keːspit. </ta>
            <ta e="T379" id="Seg_4522" s="T371">Ihin kaja battɨːr da taksa kötör bu kihi. </ta>
            <ta e="T387" id="Seg_4523" s="T379">Innʼe baraːn bahagɨnan hülen baraːn dʼe olorbut adʼas. </ta>
            <ta e="T390" id="Seg_4524" s="T387">Bu oloron dumajdaːbɨt: </ta>
            <ta e="T395" id="Seg_4525" s="T390">"Bu ɨraːktaːgɨ kɨːhɨn kördüː barɨ͡akka. </ta>
            <ta e="T403" id="Seg_4526" s="T395">ɨraːktaːgɨ hogotok kɨːstaːk, di͡ebittere, onu kördüː barɨ͡akka naːda. </ta>
            <ta e="T408" id="Seg_4527" s="T403">Baččalaːk tu͡oktaːk kihi ke", di͡ebit. </ta>
            <ta e="T416" id="Seg_4528" s="T408">Dʼe ɨraːktaːgɨga baraːrɨ ol oloror bu kihiŋ, oloror. </ta>
            <ta e="T419" id="Seg_4529" s="T416">"Dʼe kaːmɨ͡akka üčügej." </ta>
            <ta e="T423" id="Seg_4530" s="T419">Dʼe kaːmar bu kihi. </ta>
            <ta e="T427" id="Seg_4531" s="T423">Kaːman-kaːman biːr hirge olorbut. </ta>
            <ta e="T434" id="Seg_4532" s="T427">Oloron körüleːbite, ɨraːktaːgɨ gi͡ene kirili͡ehe köstör araj. </ta>
            <ta e="T436" id="Seg_4533" s="T434">"Tijeːnibin itinne." </ta>
            <ta e="T444" id="Seg_4534" s="T436">Bu kihi kaːman-kaːman, kaːman-kaːman gu͡orakka kelbit, ɨraːktaːgɨ gi͡eniger. </ta>
            <ta e="T453" id="Seg_4535" s="T444">Kelbite, muŋ tas di͡ek kuhagan bagajɨ, buru͡olaːk golomo turar. </ta>
            <ta e="T456" id="Seg_4536" s="T453">Golomo turar araj. </ta>
            <ta e="T460" id="Seg_4537" s="T456">"Bunuga kiːri͡ekke üčügej, golomogo." </ta>
            <ta e="T465" id="Seg_4538" s="T460">Golomogo kötön tüspüte – ogonnʼordoːk emeːksin. </ta>
            <ta e="T472" id="Seg_4539" s="T465">Emeːksine karaga hu͡ok, ogonnʼoro hi͡ese hečen hogus. </ta>
            <ta e="T478" id="Seg_4540" s="T472">Dʼe ahatallar manɨ, ahɨːr bu ɨ͡aldʼɨt. </ta>
            <ta e="T484" id="Seg_4541" s="T478">"Kaja-kaja hirten kelegin", di͡en ös ɨjɨtallar. </ta>
            <ta e="T498" id="Seg_4542" s="T484">"Min ((PAUSE)) bi͡es taːstaːk hobo-ilimneːk etim, ontubuttan balɨgɨ munnʼarbɨn, u͡ordarammɨn, ahɨːlaːktarɨ ölörtöːn kelebin", di͡ebit. </ta>
            <ta e="T504" id="Seg_4543" s="T498">Eːk, (n-) ahɨːlaːktarɨ ölörön kelbit. </ta>
            <ta e="T511" id="Seg_4544" s="T504">Bu ogonnʼotton ɨjɨtar, "ɨraːktaːgɨ kɨːstaːk duː", di͡en. </ta>
            <ta e="T520" id="Seg_4545" s="T511">"Hu͡očča-hogotok kɨːstaːk, kanna ere ((PAUSE)) erge bi͡ereller ühü", di͡en. </ta>
            <ta e="T529" id="Seg_4546" s="T520">"Ogonnʼor, en hu͡orumdʼuga barɨ͡aŋ hu͡ok duː", di͡ebit baːjdɨ, ɨ͡aldʼɨta. </ta>
            <ta e="T533" id="Seg_4547" s="T529">"Abaga ɨjɨtan körü͡ökpüt", di͡ebit. </ta>
            <ta e="T536" id="Seg_4548" s="T533">""Biːr ɨ͡aldʼɨt kelbit. </ta>
            <ta e="T541" id="Seg_4549" s="T536">Kɨːskɨn kördüːr", di͡eŋŋin haŋar", di͡ebit. </ta>
            <ta e="T548" id="Seg_4550" s="T541">Bu kihi baran kaːlbɨt, ɨraːktaːgɨ kötön tüspüt. </ta>
            <ta e="T551" id="Seg_4551" s="T548">"Kaja, togo kelegin?" </ta>
            <ta e="T559" id="Seg_4552" s="T551">"Hu͡ok, min ɨ͡aldʼɨttaːkpɨn, kaja-kaja hirten kelbite bu͡olla, bilbeppin. </ta>
            <ta e="T565" id="Seg_4553" s="T559">Ol kihi kɨːskɨn kördöːn erer", di͡ebit. </ta>
            <ta e="T576" id="Seg_4554" s="T565">"Innʼe gɨnan ol kihi gi͡enin hɨrajɨn-karagɨn körü͡ökpün, manna egeliŋ", di͡ebit ɨraːktaːgɨta. </ta>
            <ta e="T581" id="Seg_4555" s="T576">Bu ogonnʼor baːjdɨ ɨ͡aldʼɨtɨn ilpit. </ta>
            <ta e="T592" id="Seg_4556" s="T581">"Pa, min itinnik kihige bi͡erbeppin, iliːtin-atagɨn tohutalaːn baraːn, kaːjɨːga bɨragɨŋ", di͡ebit. </ta>
            <ta e="T593" id="Seg_4557" s="T592">Ek. </ta>
            <ta e="T597" id="Seg_4558" s="T593">Bu kihigin iliːtin-atagɨn tohuppataktar. </ta>
            <ta e="T605" id="Seg_4559" s="T597">Bɨ͡annan kelgijen baraːnnar, kaːjɨːga ugan keːspitter, maːjdɨ kihigin. </ta>
            <ta e="T612" id="Seg_4560" s="T605">((PAUSE)) Bu kihi kutujak bu͡olan taksa köppüt. </ta>
            <ta e="T617" id="Seg_4561" s="T612">Taksa kötön ogonnʼorugar emi͡e kiːrbit. </ta>
            <ta e="T634" id="Seg_4562" s="T617">Dʼe bilbeppin, maːjdɨŋɨtaːgar atɨn kihi kelbite duː, tu͡ok duː, emi͡e ""Kɨːskɨn kördöːn erer", di͡eŋŋin, bar dʼe", di͡ebit. </ta>
            <ta e="T637" id="Seg_4563" s="T634">Emi͡e barbɨt ogonnʼor. </ta>
            <ta e="T642" id="Seg_4564" s="T637">Dʼe ol baran emi͡e di͡ebit: </ta>
            <ta e="T646" id="Seg_4565" s="T642">"Kaja, emi͡e ɨ͡aldʼɨt kelle. </ta>
            <ta e="T650" id="Seg_4566" s="T646">Kɨːskɨn kördöːn erer", di͡ebit. </ta>
            <ta e="T662" id="Seg_4567" s="T650">"Dogottor, maːjdɨ kaːjɨːga bɨragaːččɨ kihigitin körüŋ dʼe, kajtak-kajtak kihini dʼabɨlɨːbɨt dʼürü", di͡ebit. </ta>
            <ta e="T673" id="Seg_4568" s="T662">Kaːjɨːga bɨrakpɨt kihite hu͡ola ere čöŋöjö hɨtar, kelgijbit bɨ͡ata bɨ͡atɨnan hɨtar. </ta>
            <ta e="T677" id="Seg_4569" s="T673">Dʼe kaja, onnuk-onnuk, kaja. </ta>
            <ta e="T686" id="Seg_4570" s="T677">"O, dʼe ile da üčügej kihini gɨnnɨm e, badaga. </ta>
            <ta e="T690" id="Seg_4571" s="T686">Kɨːspɨn bi͡erebin, di͡em bu͡olla." </ta>
            <ta e="T699" id="Seg_4572" s="T690">Bu kihi maːgɨlaːk ahɨːlaːk ölörbütün ɨraːktaːgɨ töhögüger togo bɨrakpɨt. </ta>
            <ta e="T710" id="Seg_4573" s="T699">Innʼe gɨnan köhün kennitiger čɨːčaːk ɨllɨːr, godolutan araːran ilpit bu kihi. </ta>
            <ta e="T720" id="Seg_4574" s="T710">Eŋin-eŋin haldaːttaːk-tu͡oktaːk bu kihi dʼe barar dʼi͡etiger, kɨːhɨn ɨlan baraːn. </ta>
            <ta e="T722" id="Seg_4575" s="T720">Dʼe köhör. </ta>
            <ta e="T733" id="Seg_4576" s="T722">Köhön, iti kelenner, iti dʼe iti bajan-toton olorbuta, dʼe iti elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_4577" s="T1">Čopočuka</ta>
            <ta e="T3" id="Seg_4578" s="T2">olor-but</ta>
            <ta e="T4" id="Seg_4579" s="T3">ühü</ta>
            <ta e="T5" id="Seg_4580" s="T4">bi͡es</ta>
            <ta e="T6" id="Seg_4581" s="T5">mas</ta>
            <ta e="T7" id="Seg_4582" s="T6">turu͡or-but</ta>
            <ta e="T8" id="Seg_4583" s="T7">golomo</ta>
            <ta e="T9" id="Seg_4584" s="T8">dʼi͡e-leːk</ta>
            <ta e="T10" id="Seg_4585" s="T9">bi͡es</ta>
            <ta e="T11" id="Seg_4586" s="T10">taːs-taːk</ta>
            <ta e="T12" id="Seg_4587" s="T11">sobo-ilim-neːk</ta>
            <ta e="T13" id="Seg_4588" s="T12">kün</ta>
            <ta e="T14" id="Seg_4589" s="T13">aːjɨ</ta>
            <ta e="T15" id="Seg_4590" s="T14">ilim-nen-e</ta>
            <ta e="T16" id="Seg_4591" s="T15">kiːr-er</ta>
            <ta e="T17" id="Seg_4592" s="T16">bu</ta>
            <ta e="T18" id="Seg_4593" s="T17">kihi</ta>
            <ta e="T19" id="Seg_4594" s="T18">balɨg-ɨ</ta>
            <ta e="T20" id="Seg_4595" s="T19">loglo-čču</ta>
            <ta e="T21" id="Seg_4596" s="T20">mus-put</ta>
            <ta e="T22" id="Seg_4597" s="T21">ilim-i-tten</ta>
            <ta e="T23" id="Seg_4598" s="T22">innʼe</ta>
            <ta e="T24" id="Seg_4599" s="T23">gɨn-an</ta>
            <ta e="T25" id="Seg_4600" s="T24">biːrde</ta>
            <ta e="T26" id="Seg_4601" s="T25">bar-bɨt-a</ta>
            <ta e="T27" id="Seg_4602" s="T26">kɨrsa-lar</ta>
            <ta e="T28" id="Seg_4603" s="T27">tu͡ok-tar</ta>
            <ta e="T29" id="Seg_4604" s="T28">hi͡e-bit-ter</ta>
            <ta e="T30" id="Seg_4605" s="T29">araj</ta>
            <ta e="T31" id="Seg_4606" s="T30">balɨg-ɨ-n</ta>
            <ta e="T32" id="Seg_4607" s="T31">mu-nu</ta>
            <ta e="T33" id="Seg_4608" s="T32">ird-i͡ek-ke</ta>
            <ta e="T34" id="Seg_4609" s="T33">hu͡ol-u-n</ta>
            <ta e="T35" id="Seg_4610" s="T34">ird-i͡ek-ke</ta>
            <ta e="T36" id="Seg_4611" s="T35">balɨg-ɨ-n</ta>
            <ta e="T37" id="Seg_4612" s="T36">dʼi͡e-ti-ger</ta>
            <ta e="T38" id="Seg_4613" s="T37">tah-an</ta>
            <ta e="T39" id="Seg_4614" s="T38">baraːn</ta>
            <ta e="T40" id="Seg_4615" s="T39">bu</ta>
            <ta e="T41" id="Seg_4616" s="T40">kihi</ta>
            <ta e="T42" id="Seg_4617" s="T41">kɨrsa</ta>
            <ta e="T43" id="Seg_4618" s="T42">hu͡ol-u-n</ta>
            <ta e="T44" id="Seg_4619" s="T43">irdeː-bit</ta>
            <ta e="T45" id="Seg_4620" s="T44">kɨrsa</ta>
            <ta e="T46" id="Seg_4621" s="T45">hu͡ol-u-n</ta>
            <ta e="T47" id="Seg_4622" s="T46">irdeː-n</ta>
            <ta e="T48" id="Seg_4623" s="T47">is-teg-ine</ta>
            <ta e="T49" id="Seg_4624" s="T48">kaja</ta>
            <ta e="T50" id="Seg_4625" s="T49">kɨrsa</ta>
            <ta e="T51" id="Seg_4626" s="T50">tug-u</ta>
            <ta e="T52" id="Seg_4627" s="T51">i</ta>
            <ta e="T53" id="Seg_4628" s="T52">biːr</ta>
            <ta e="T54" id="Seg_4629" s="T53">talak</ta>
            <ta e="T55" id="Seg_4630" s="T54">ih-i-tten</ta>
            <ta e="T56" id="Seg_4631" s="T55">tur-a</ta>
            <ta e="T57" id="Seg_4632" s="T56">ekkireː-bit</ta>
            <ta e="T58" id="Seg_4633" s="T57">ara</ta>
            <ta e="T59" id="Seg_4634" s="T58">Čopočuka</ta>
            <ta e="T60" id="Seg_4635" s="T59">kihi-ni</ta>
            <ta e="T61" id="Seg_4636" s="T60">hohu-t-tu-ŋ</ta>
            <ta e="T62" id="Seg_4637" s="T61">kaja</ta>
            <ta e="T63" id="Seg_4638" s="T62">kajtak</ta>
            <ta e="T64" id="Seg_4639" s="T63">kel-e-gin</ta>
            <ta e="T65" id="Seg_4640" s="T64">kaja</ta>
            <ta e="T66" id="Seg_4641" s="T65">balɨk-pɨ-n</ta>
            <ta e="T67" id="Seg_4642" s="T66">ke</ta>
            <ta e="T68" id="Seg_4643" s="T67">togo</ta>
            <ta e="T69" id="Seg_4644" s="T68">u͡or-buk-kut=uj</ta>
            <ta e="T70" id="Seg_4645" s="T69">ehigi</ta>
            <ta e="T71" id="Seg_4646" s="T70">ho</ta>
            <ta e="T72" id="Seg_4647" s="T71">min</ta>
            <ta e="T73" id="Seg_4648" s="T72">u͡or-but-u-m</ta>
            <ta e="T74" id="Seg_4649" s="T73">du͡o</ta>
            <ta e="T75" id="Seg_4650" s="T74">d-iː-d-iː</ta>
            <ta e="T76" id="Seg_4651" s="T75">gɨm-mɨt</ta>
            <ta e="T77" id="Seg_4652" s="T76">man-tɨ-ŋ</ta>
            <ta e="T78" id="Seg_4653" s="T77">kutujak</ta>
            <ta e="T79" id="Seg_4654" s="T78">bu͡ol-but</ta>
            <ta e="T80" id="Seg_4655" s="T79">kutujak</ta>
            <ta e="T81" id="Seg_4656" s="T80">bu͡ol-an</ta>
            <ta e="T82" id="Seg_4657" s="T81">dʼe</ta>
            <ta e="T83" id="Seg_4658" s="T82">ogonnʼor-u-ŋ</ta>
            <ta e="T84" id="Seg_4659" s="T83">dʼe</ta>
            <ta e="T85" id="Seg_4660" s="T84">hɨldʼ-ar</ta>
            <ta e="T86" id="Seg_4661" s="T85">baːgɨ-nɨ</ta>
            <ta e="T87" id="Seg_4662" s="T86">u͡ob-an</ta>
            <ta e="T88" id="Seg_4663" s="T87">keːs-pit</ta>
            <ta e="T89" id="Seg_4664" s="T88">kɨrsa-ta</ta>
            <ta e="T90" id="Seg_4665" s="T89">bagɨ-ŋ</ta>
            <ta e="T91" id="Seg_4666" s="T90">ih-i-n</ta>
            <ta e="T92" id="Seg_4667" s="T91">kaja</ta>
            <ta e="T95" id="Seg_4668" s="T94">batt-an</ta>
            <ta e="T96" id="Seg_4669" s="T95">bɨh-an</ta>
            <ta e="T97" id="Seg_4670" s="T96">taks-a</ta>
            <ta e="T100" id="Seg_4671" s="T99">taks-a</ta>
            <ta e="T101" id="Seg_4672" s="T100">köp-püt</ta>
            <ta e="T102" id="Seg_4673" s="T101">innʼe</ta>
            <ta e="T103" id="Seg_4674" s="T102">baran</ta>
            <ta e="T104" id="Seg_4675" s="T103">baːgɨ-nɨ</ta>
            <ta e="T105" id="Seg_4676" s="T104">hül-er</ta>
            <ta e="T106" id="Seg_4677" s="T105">da</ta>
            <ta e="T107" id="Seg_4678" s="T106">karamaːn-ɨ-gar</ta>
            <ta e="T108" id="Seg_4679" s="T107">ukt-ar</ta>
            <ta e="T109" id="Seg_4680" s="T108">daː</ta>
            <ta e="T110" id="Seg_4681" s="T109">kaːm-an</ta>
            <ta e="T111" id="Seg_4682" s="T110">is-pit</ta>
            <ta e="T112" id="Seg_4683" s="T111">epi͡et</ta>
            <ta e="T113" id="Seg_4684" s="T112">inni</ta>
            <ta e="T114" id="Seg_4685" s="T113">di͡ek</ta>
            <ta e="T115" id="Seg_4686" s="T114">kaːm-an</ta>
            <ta e="T116" id="Seg_4687" s="T115">ih-en</ta>
            <ta e="T117" id="Seg_4688" s="T116">uskaːn</ta>
            <ta e="T118" id="Seg_4689" s="T117">turu͡or-but</ta>
            <ta e="T119" id="Seg_4690" s="T118">araː</ta>
            <ta e="T120" id="Seg_4691" s="T119">Čopočuka</ta>
            <ta e="T121" id="Seg_4692" s="T120">tu͡ok</ta>
            <ta e="T122" id="Seg_4693" s="T121">kaja</ta>
            <ta e="T123" id="Seg_4694" s="T122">hir-ten</ta>
            <ta e="T124" id="Seg_4695" s="T123">kel-e-gin</ta>
            <ta e="T125" id="Seg_4696" s="T124">kihi-ni</ta>
            <ta e="T126" id="Seg_4697" s="T125">hohu-t-aːktaː-tɨ-ŋ</ta>
            <ta e="T127" id="Seg_4698" s="T126">diː-bit</ta>
            <ta e="T128" id="Seg_4699" s="T127">kaja</ta>
            <ta e="T129" id="Seg_4700" s="T128">balɨk-pɨ-n</ta>
            <ta e="T130" id="Seg_4701" s="T129">togo</ta>
            <ta e="T131" id="Seg_4702" s="T130">barɨ-tɨ-n</ta>
            <ta e="T132" id="Seg_4703" s="T131">hi͡e-bik-kit=ij</ta>
            <ta e="T133" id="Seg_4704" s="T132">balɨk-pɨ-n</ta>
            <ta e="T134" id="Seg_4705" s="T133">ird-iː</ta>
            <ta e="T135" id="Seg_4706" s="T134">hɨldʼ-a-bɨn</ta>
            <ta e="T138" id="Seg_4707" s="T136">maːgɨ</ta>
            <ta e="T139" id="Seg_4708" s="T138">emi͡e</ta>
            <ta e="T141" id="Seg_4709" s="T139">kaja</ta>
            <ta e="T142" id="Seg_4710" s="T141">ti͡ej</ta>
            <ta e="T143" id="Seg_4711" s="T142">dʼe</ta>
            <ta e="T144" id="Seg_4712" s="T143">ölörs-öl-lör</ta>
            <ta e="T145" id="Seg_4713" s="T144">e</ta>
            <ta e="T146" id="Seg_4714" s="T145">uskaːn-ɨ</ta>
            <ta e="T147" id="Seg_4715" s="T146">kɨtta</ta>
            <ta e="T148" id="Seg_4716" s="T147">ölörs-ön-nör</ta>
            <ta e="T149" id="Seg_4717" s="T148">uskaːn-ɨ</ta>
            <ta e="T150" id="Seg_4718" s="T149">tug-u-n</ta>
            <ta e="T151" id="Seg_4719" s="T150">kihi-l-i͡e=j</ta>
            <ta e="T152" id="Seg_4720" s="T151">ol</ta>
            <ta e="T153" id="Seg_4721" s="T152">kihi</ta>
            <ta e="T154" id="Seg_4722" s="T153">paːgɨ-nɨ</ta>
            <ta e="T155" id="Seg_4723" s="T154">ölör-büt</ta>
            <ta e="T156" id="Seg_4724" s="T155">da</ta>
            <ta e="T157" id="Seg_4725" s="T156">emi͡e</ta>
            <ta e="T159" id="Seg_4726" s="T158">karamaːn-ɨ-gar</ta>
            <ta e="T160" id="Seg_4727" s="T159">ukt-an</ta>
            <ta e="T161" id="Seg_4728" s="T160">keːs-pit</ta>
            <ta e="T162" id="Seg_4729" s="T161">hül-ü-n-en</ta>
            <ta e="T163" id="Seg_4730" s="T162">emi͡e</ta>
            <ta e="T164" id="Seg_4731" s="T163">kaːm-an</ta>
            <ta e="T165" id="Seg_4732" s="T164">is-pit</ta>
            <ta e="T166" id="Seg_4733" s="T165">kaːm-an-kaːm-an</ta>
            <ta e="T167" id="Seg_4734" s="T166">is-teg-ine</ta>
            <ta e="T168" id="Seg_4735" s="T167">hahɨl</ta>
            <ta e="T169" id="Seg_4736" s="T168">tur-a</ta>
            <ta e="T170" id="Seg_4737" s="T169">ekkireː-bit</ta>
            <ta e="T171" id="Seg_4738" s="T170">oː</ta>
            <ta e="T172" id="Seg_4739" s="T171">Čopočuka</ta>
            <ta e="T173" id="Seg_4740" s="T172">kihi-ni</ta>
            <ta e="T174" id="Seg_4741" s="T173">hohu-t-tu-ŋ</ta>
            <ta e="T175" id="Seg_4742" s="T174">kaja</ta>
            <ta e="T176" id="Seg_4743" s="T175">kaja</ta>
            <ta e="T177" id="Seg_4744" s="T176">hir-ten</ta>
            <ta e="T178" id="Seg_4745" s="T177">kel-e-gin</ta>
            <ta e="T179" id="Seg_4746" s="T178">bu</ta>
            <ta e="T180" id="Seg_4747" s="T179">di͡e-bit</ta>
            <ta e="T181" id="Seg_4748" s="T180">kaja</ta>
            <ta e="T182" id="Seg_4749" s="T181">di͡e-bit</ta>
            <ta e="T183" id="Seg_4750" s="T182">ka</ta>
            <ta e="T184" id="Seg_4751" s="T183">balɨk-pɨ-n</ta>
            <ta e="T185" id="Seg_4752" s="T184">barɨ-tɨ-n</ta>
            <ta e="T186" id="Seg_4753" s="T185">hi͡e-bik-kit</ta>
            <ta e="T187" id="Seg_4754" s="T186">ol</ta>
            <ta e="T188" id="Seg_4755" s="T187">ihin</ta>
            <ta e="T189" id="Seg_4756" s="T188">irdeː-n</ta>
            <ta e="T190" id="Seg_4757" s="T189">kel-e-bin</ta>
            <ta e="T191" id="Seg_4758" s="T190">pa</ta>
            <ta e="T192" id="Seg_4759" s="T191">en</ta>
            <ta e="T193" id="Seg_4760" s="T192">balɨk-ka-r</ta>
            <ta e="T194" id="Seg_4761" s="T193">bihigi</ta>
            <ta e="T195" id="Seg_4762" s="T194">čugahaː-batak-pɨt</ta>
            <ta e="T196" id="Seg_4763" s="T195">daːganɨ</ta>
            <ta e="T197" id="Seg_4764" s="T196">össü͡ö</ta>
            <ta e="T198" id="Seg_4765" s="T197">kata</ta>
            <ta e="T199" id="Seg_4766" s="T198">min</ta>
            <ta e="T200" id="Seg_4767" s="T199">enigi-n</ta>
            <ta e="T201" id="Seg_4768" s="T200">tɨrɨt-a</ta>
            <ta e="T202" id="Seg_4769" s="T201">tɨːt-an</ta>
            <ta e="T203" id="Seg_4770" s="T202">hi͡e-n</ta>
            <ta e="T204" id="Seg_4771" s="T203">keːh-i͡e-m</ta>
            <ta e="T205" id="Seg_4772" s="T204">di͡e-bit</ta>
            <ta e="T206" id="Seg_4773" s="T205">dʼe</ta>
            <ta e="T207" id="Seg_4774" s="T206">ölörs-öl-lör</ta>
            <ta e="T208" id="Seg_4775" s="T207">bu</ta>
            <ta e="T209" id="Seg_4776" s="T208">kihi-ler</ta>
            <ta e="T210" id="Seg_4777" s="T209">Čopočuka</ta>
            <ta e="T211" id="Seg_4778" s="T210">kan-tɨ-tɨ-n</ta>
            <ta e="T212" id="Seg_4779" s="T211">kihi-l-i͡e=j</ta>
            <ta e="T213" id="Seg_4780" s="T212">ti͡er-e</ta>
            <ta e="T214" id="Seg_4781" s="T213">battaː-bɨt</ta>
            <ta e="T215" id="Seg_4782" s="T214">da</ta>
            <ta e="T216" id="Seg_4783" s="T215">ölör-ön</ta>
            <ta e="T217" id="Seg_4784" s="T216">keːs-pit</ta>
            <ta e="T218" id="Seg_4785" s="T217">maːŋɨ-nɨ</ta>
            <ta e="T219" id="Seg_4786" s="T218">hül-büt</ta>
            <ta e="T220" id="Seg_4787" s="T219">da</ta>
            <ta e="T221" id="Seg_4788" s="T220">karamaːn-ɨ-gar</ta>
            <ta e="T222" id="Seg_4789" s="T221">karamaːn</ta>
            <ta e="T223" id="Seg_4790" s="T222">da</ta>
            <ta e="T224" id="Seg_4791" s="T223">ulakan</ta>
            <ta e="T225" id="Seg_4792" s="T224">sök-tü-m</ta>
            <ta e="T226" id="Seg_4793" s="T225">hahɨl-ɨ-n</ta>
            <ta e="T227" id="Seg_4794" s="T226">ukt-an</ta>
            <ta e="T228" id="Seg_4795" s="T227">keːs-pit</ta>
            <ta e="T229" id="Seg_4796" s="T228">karamaːn-ɨ-gar</ta>
            <ta e="T230" id="Seg_4797" s="T229">onton</ta>
            <ta e="T231" id="Seg_4798" s="T230">kaːm-an</ta>
            <ta e="T232" id="Seg_4799" s="T231">is-pit</ta>
            <ta e="T233" id="Seg_4800" s="T232">kaːm-an</ta>
            <ta e="T234" id="Seg_4801" s="T233">ih-e</ta>
            <ta e="T235" id="Seg_4802" s="T234">is-teg-ine</ta>
            <ta e="T236" id="Seg_4803" s="T235">hi͡egen</ta>
            <ta e="T237" id="Seg_4804" s="T236">tur-a</ta>
            <ta e="T238" id="Seg_4805" s="T237">ekkireː-bit</ta>
            <ta e="T239" id="Seg_4806" s="T238">kaja</ta>
            <ta e="T240" id="Seg_4807" s="T239">Čopočuka</ta>
            <ta e="T241" id="Seg_4808" s="T240">kihi-ni</ta>
            <ta e="T242" id="Seg_4809" s="T241">hohu-t-tu-ŋ</ta>
            <ta e="T243" id="Seg_4810" s="T242">kata</ta>
            <ta e="T244" id="Seg_4811" s="T243">di͡e-bit</ta>
            <ta e="T245" id="Seg_4812" s="T244">togo</ta>
            <ta e="T246" id="Seg_4813" s="T245">hɨldʼ-a-gɨn</ta>
            <ta e="T247" id="Seg_4814" s="T246">bu</ta>
            <ta e="T248" id="Seg_4815" s="T247">di͡e-n</ta>
            <ta e="T249" id="Seg_4816" s="T248">dojdu-ga</ta>
            <ta e="T250" id="Seg_4817" s="T249">pa</ta>
            <ta e="T251" id="Seg_4818" s="T250">ka</ta>
            <ta e="T252" id="Seg_4819" s="T251">balɨk-pɨ-n</ta>
            <ta e="T253" id="Seg_4820" s="T252">togo</ta>
            <ta e="T254" id="Seg_4821" s="T253">barɨ-tɨ-n</ta>
            <ta e="T255" id="Seg_4822" s="T254">hi͡e-bik-kit=ij</ta>
            <ta e="T256" id="Seg_4823" s="T255">o-nu</ta>
            <ta e="T257" id="Seg_4824" s="T256">ird-iː</ta>
            <ta e="T258" id="Seg_4825" s="T257">hɨldʼ-a-bɨn</ta>
            <ta e="T259" id="Seg_4826" s="T258">di͡e-bit</ta>
            <ta e="T260" id="Seg_4827" s="T259">össü͡ö</ta>
            <ta e="T261" id="Seg_4828" s="T260">hi͡e-bik-kit=ij</ta>
            <ta e="T262" id="Seg_4829" s="T261">kab-an</ta>
            <ta e="T263" id="Seg_4830" s="T262">ɨl-ar</ta>
            <ta e="T264" id="Seg_4831" s="T263">da</ta>
            <ta e="T265" id="Seg_4832" s="T264">maːŋɨ-nɨ</ta>
            <ta e="T266" id="Seg_4833" s="T265">dʼe</ta>
            <ta e="T267" id="Seg_4834" s="T266">ölörs-ör</ta>
            <ta e="T268" id="Seg_4835" s="T267">ölörs-ön</ta>
            <ta e="T269" id="Seg_4836" s="T268">Čopočuka</ta>
            <ta e="T270" id="Seg_4837" s="T269">tug-u-n</ta>
            <ta e="T271" id="Seg_4838" s="T270">kihi-l-i͡e=j</ta>
            <ta e="T272" id="Seg_4839" s="T271">kihi-leː-betek</ta>
            <ta e="T273" id="Seg_4840" s="T272">ölör-büt</ta>
            <ta e="T274" id="Seg_4841" s="T273">da</ta>
            <ta e="T275" id="Seg_4842" s="T274">emi͡e</ta>
            <ta e="T276" id="Seg_4843" s="T275">hül-büt</ta>
            <ta e="T277" id="Seg_4844" s="T276">da</ta>
            <ta e="T278" id="Seg_4845" s="T277">karmaːn-ɨ-gar</ta>
            <ta e="T279" id="Seg_4846" s="T278">ukt-an</ta>
            <ta e="T280" id="Seg_4847" s="T279">keːs-pit</ta>
            <ta e="T281" id="Seg_4848" s="T280">emi͡e</ta>
            <ta e="T282" id="Seg_4849" s="T281">kaːm-an</ta>
            <ta e="T283" id="Seg_4850" s="T282">is-pit</ta>
            <ta e="T284" id="Seg_4851" s="T283">kaːm-an-kaːm-an</ta>
            <ta e="T285" id="Seg_4852" s="T284">börö-nü</ta>
            <ta e="T286" id="Seg_4853" s="T285">körs-ü-büt</ta>
            <ta e="T287" id="Seg_4854" s="T286">börö-tö</ta>
            <ta e="T288" id="Seg_4855" s="T287">tur-a</ta>
            <ta e="T289" id="Seg_4856" s="T288">ekkireː-bit</ta>
            <ta e="T290" id="Seg_4857" s="T289">o</ta>
            <ta e="T291" id="Seg_4858" s="T290">Čopočuka</ta>
            <ta e="T292" id="Seg_4859" s="T291">kihi-ni</ta>
            <ta e="T293" id="Seg_4860" s="T292">hohu-t-tu-ŋ</ta>
            <ta e="T294" id="Seg_4861" s="T293">ka</ta>
            <ta e="T295" id="Seg_4862" s="T294">kajdi͡ek</ta>
            <ta e="T296" id="Seg_4863" s="T295">ih-e-gin</ta>
            <ta e="T297" id="Seg_4864" s="T296">di͡e-bit</ta>
            <ta e="T298" id="Seg_4865" s="T297">kaja</ta>
            <ta e="T299" id="Seg_4866" s="T298">balɨk-pɨ-n</ta>
            <ta e="T300" id="Seg_4867" s="T299">togo</ta>
            <ta e="T301" id="Seg_4868" s="T300">hi͡e-bik-kit=ij</ta>
            <ta e="T302" id="Seg_4869" s="T301">on-tu-bu-n</ta>
            <ta e="T303" id="Seg_4870" s="T302">ird-iː-bin</ta>
            <ta e="T304" id="Seg_4871" s="T303">pa</ta>
            <ta e="T305" id="Seg_4872" s="T304">en</ta>
            <ta e="T306" id="Seg_4873" s="T305">balɨk-ka-r</ta>
            <ta e="T307" id="Seg_4874" s="T306">čugahaː-batak-pɨt</ta>
            <ta e="T308" id="Seg_4875" s="T307">daːganɨ</ta>
            <ta e="T309" id="Seg_4876" s="T308">kata</ta>
            <ta e="T310" id="Seg_4877" s="T309">min</ta>
            <ta e="T311" id="Seg_4878" s="T310">enigi-n</ta>
            <ta e="T312" id="Seg_4879" s="T311">hi͡e-n</ta>
            <ta e="T313" id="Seg_4880" s="T312">keːh-i͡e-m</ta>
            <ta e="T314" id="Seg_4881" s="T313">di͡e-bit</ta>
            <ta e="T315" id="Seg_4882" s="T314">baːgɨ-nɨ</ta>
            <ta e="T316" id="Seg_4883" s="T315">u͡ob-an</ta>
            <ta e="T317" id="Seg_4884" s="T316">keːs-pit</ta>
            <ta e="T318" id="Seg_4885" s="T317">ɨŋɨr-an</ta>
            <ta e="T319" id="Seg_4886" s="T318">keːs-pit</ta>
            <ta e="T320" id="Seg_4887" s="T319">Čopočukaː-nɨ</ta>
            <ta e="T321" id="Seg_4888" s="T320">Čopočuka</ta>
            <ta e="T322" id="Seg_4889" s="T321">ih-i-n</ta>
            <ta e="T323" id="Seg_4890" s="T322">kaj-a</ta>
            <ta e="T324" id="Seg_4891" s="T323">battɨː-r</ta>
            <ta e="T325" id="Seg_4892" s="T324">da</ta>
            <ta e="T326" id="Seg_4893" s="T325">taks-a</ta>
            <ta e="T327" id="Seg_4894" s="T326">köt-ör</ta>
            <ta e="T328" id="Seg_4895" s="T327">innʼe</ta>
            <ta e="T329" id="Seg_4896" s="T328">gɨm-mɨt</ta>
            <ta e="T330" id="Seg_4897" s="T329">da</ta>
            <ta e="T331" id="Seg_4898" s="T330">hül-büt</ta>
            <ta e="T332" id="Seg_4899" s="T331">da</ta>
            <ta e="T333" id="Seg_4900" s="T332">karamaːn-ɨ-gar</ta>
            <ta e="T334" id="Seg_4901" s="T333">ukt-an</ta>
            <ta e="T335" id="Seg_4902" s="T334">keːs-pit</ta>
            <ta e="T336" id="Seg_4903" s="T335">onton</ta>
            <ta e="T337" id="Seg_4904" s="T336">dʼe</ta>
            <ta e="T338" id="Seg_4905" s="T337">kaːm-an</ta>
            <ta e="T339" id="Seg_4906" s="T338">is-pit</ta>
            <ta e="T340" id="Seg_4907" s="T339">kaːm-an</ta>
            <ta e="T341" id="Seg_4908" s="T340">is-pit</ta>
            <ta e="T342" id="Seg_4909" s="T341">o</ta>
            <ta e="T343" id="Seg_4910" s="T342">ebekeː</ta>
            <ta e="T344" id="Seg_4911" s="T343">tur-a</ta>
            <ta e="T345" id="Seg_4912" s="T344">ekkireː-kteː-bit</ta>
            <ta e="T346" id="Seg_4913" s="T345">diːn</ta>
            <ta e="T347" id="Seg_4914" s="T346">diː</ta>
            <ta e="T348" id="Seg_4915" s="T347">araj</ta>
            <ta e="T349" id="Seg_4916" s="T348">oj</ta>
            <ta e="T350" id="Seg_4917" s="T349">Čopočuka</ta>
            <ta e="T351" id="Seg_4918" s="T350">togo</ta>
            <ta e="T352" id="Seg_4919" s="T351">kaja</ta>
            <ta e="T353" id="Seg_4920" s="T352">hir-ge</ta>
            <ta e="T354" id="Seg_4921" s="T353">hɨldʼ-a-gɨn</ta>
            <ta e="T355" id="Seg_4922" s="T354">bu</ta>
            <ta e="T356" id="Seg_4923" s="T355">di͡e-bit</ta>
            <ta e="T357" id="Seg_4924" s="T356">balɨk-pɨ-n</ta>
            <ta e="T358" id="Seg_4925" s="T357">togo</ta>
            <ta e="T359" id="Seg_4926" s="T358">u͡or-buk-kut=uj</ta>
            <ta e="T360" id="Seg_4927" s="T359">össü͡ö</ta>
            <ta e="T361" id="Seg_4928" s="T360">balɨk-pɨ-n</ta>
            <ta e="T362" id="Seg_4929" s="T361">u͡or-buk-kut</ta>
            <ta e="T364" id="Seg_4930" s="T362">si͡e</ta>
            <ta e="T365" id="Seg_4931" s="T364">min</ta>
            <ta e="T366" id="Seg_4932" s="T365">enigi-n</ta>
            <ta e="T367" id="Seg_4933" s="T366">u͡ob-an</ta>
            <ta e="T368" id="Seg_4934" s="T367">keːh-i͡e-m</ta>
            <ta e="T369" id="Seg_4935" s="T368">di͡e-bit</ta>
            <ta e="T370" id="Seg_4936" s="T369">hi͡e-n</ta>
            <ta e="T371" id="Seg_4937" s="T370">keːs-pit</ta>
            <ta e="T372" id="Seg_4938" s="T371">ih-i-n</ta>
            <ta e="T373" id="Seg_4939" s="T372">kaj-a</ta>
            <ta e="T374" id="Seg_4940" s="T373">battɨː-r</ta>
            <ta e="T375" id="Seg_4941" s="T374">da</ta>
            <ta e="T376" id="Seg_4942" s="T375">taks-a</ta>
            <ta e="T377" id="Seg_4943" s="T376">köt-ör</ta>
            <ta e="T378" id="Seg_4944" s="T377">bu</ta>
            <ta e="T379" id="Seg_4945" s="T378">kihi</ta>
            <ta e="T380" id="Seg_4946" s="T379">innʼe</ta>
            <ta e="T381" id="Seg_4947" s="T380">baraːn</ta>
            <ta e="T382" id="Seg_4948" s="T381">bahag-ɨ-nan</ta>
            <ta e="T383" id="Seg_4949" s="T382">hül-en</ta>
            <ta e="T384" id="Seg_4950" s="T383">baraːn</ta>
            <ta e="T385" id="Seg_4951" s="T384">dʼe</ta>
            <ta e="T386" id="Seg_4952" s="T385">olor-but</ta>
            <ta e="T387" id="Seg_4953" s="T386">adʼas</ta>
            <ta e="T388" id="Seg_4954" s="T387">bu</ta>
            <ta e="T389" id="Seg_4955" s="T388">olor-on</ta>
            <ta e="T390" id="Seg_4956" s="T389">dumaj-daː-bɨt</ta>
            <ta e="T391" id="Seg_4957" s="T390">bu</ta>
            <ta e="T392" id="Seg_4958" s="T391">ɨraːktaːgɨ</ta>
            <ta e="T393" id="Seg_4959" s="T392">kɨːh-ɨ-n</ta>
            <ta e="T394" id="Seg_4960" s="T393">körd-üː</ta>
            <ta e="T395" id="Seg_4961" s="T394">bar-ɨ͡ak-ka</ta>
            <ta e="T396" id="Seg_4962" s="T395">ɨraːktaːgɨ</ta>
            <ta e="T397" id="Seg_4963" s="T396">hogotok</ta>
            <ta e="T398" id="Seg_4964" s="T397">kɨːs-taːk</ta>
            <ta e="T399" id="Seg_4965" s="T398">di͡e-bit-tere</ta>
            <ta e="T400" id="Seg_4966" s="T399">o-nu</ta>
            <ta e="T401" id="Seg_4967" s="T400">körd-üː</ta>
            <ta e="T402" id="Seg_4968" s="T401">bar-ɨ͡ak-ka</ta>
            <ta e="T403" id="Seg_4969" s="T402">naːda</ta>
            <ta e="T404" id="Seg_4970" s="T403">bačča-laːk</ta>
            <ta e="T405" id="Seg_4971" s="T404">tu͡ok-taːk</ta>
            <ta e="T406" id="Seg_4972" s="T405">kihi</ta>
            <ta e="T407" id="Seg_4973" s="T406">ke</ta>
            <ta e="T408" id="Seg_4974" s="T407">di͡e-bit</ta>
            <ta e="T409" id="Seg_4975" s="T408">dʼe</ta>
            <ta e="T410" id="Seg_4976" s="T409">ɨraːktaːgɨ-ga</ta>
            <ta e="T411" id="Seg_4977" s="T410">bar-aːrɨ</ta>
            <ta e="T412" id="Seg_4978" s="T411">ol</ta>
            <ta e="T413" id="Seg_4979" s="T412">olor-or</ta>
            <ta e="T414" id="Seg_4980" s="T413">bu</ta>
            <ta e="T415" id="Seg_4981" s="T414">kihi-ŋ</ta>
            <ta e="T416" id="Seg_4982" s="T415">olor-or</ta>
            <ta e="T417" id="Seg_4983" s="T416">dʼe</ta>
            <ta e="T418" id="Seg_4984" s="T417">kaːm-ɨ͡ak-ka</ta>
            <ta e="T419" id="Seg_4985" s="T418">üčügej</ta>
            <ta e="T420" id="Seg_4986" s="T419">dʼe</ta>
            <ta e="T421" id="Seg_4987" s="T420">kaːm-ar</ta>
            <ta e="T422" id="Seg_4988" s="T421">bu</ta>
            <ta e="T423" id="Seg_4989" s="T422">kihi</ta>
            <ta e="T424" id="Seg_4990" s="T423">kaːm-an-kaːm-an</ta>
            <ta e="T425" id="Seg_4991" s="T424">biːr</ta>
            <ta e="T426" id="Seg_4992" s="T425">hir-ge</ta>
            <ta e="T427" id="Seg_4993" s="T426">olor-but</ta>
            <ta e="T428" id="Seg_4994" s="T427">olor-on</ta>
            <ta e="T429" id="Seg_4995" s="T428">körüleː-bit-e</ta>
            <ta e="T430" id="Seg_4996" s="T429">ɨraːktaːgɨ</ta>
            <ta e="T431" id="Seg_4997" s="T430">gi͡en-e</ta>
            <ta e="T432" id="Seg_4998" s="T431">kirili͡eh-e</ta>
            <ta e="T433" id="Seg_4999" s="T432">köst-ör</ta>
            <ta e="T434" id="Seg_5000" s="T433">araj</ta>
            <ta e="T435" id="Seg_5001" s="T434">tij-eːn-i-bin</ta>
            <ta e="T436" id="Seg_5002" s="T435">itinne</ta>
            <ta e="T437" id="Seg_5003" s="T436">bu</ta>
            <ta e="T438" id="Seg_5004" s="T437">kihi</ta>
            <ta e="T439" id="Seg_5005" s="T438">kaːm-an-kaːm-an</ta>
            <ta e="T440" id="Seg_5006" s="T439">kaːm-an-kaːm-an</ta>
            <ta e="T441" id="Seg_5007" s="T440">gu͡orak-ka</ta>
            <ta e="T442" id="Seg_5008" s="T441">kel-bit</ta>
            <ta e="T443" id="Seg_5009" s="T442">ɨraːktaːgɨ</ta>
            <ta e="T444" id="Seg_5010" s="T443">gi͡en-i-ger</ta>
            <ta e="T445" id="Seg_5011" s="T444">kel-bit-e</ta>
            <ta e="T446" id="Seg_5012" s="T445">muŋ</ta>
            <ta e="T447" id="Seg_5013" s="T446">tas</ta>
            <ta e="T448" id="Seg_5014" s="T447">di͡ek</ta>
            <ta e="T449" id="Seg_5015" s="T448">kuhagan</ta>
            <ta e="T450" id="Seg_5016" s="T449">bagajɨ</ta>
            <ta e="T451" id="Seg_5017" s="T450">buru͡o-laːk</ta>
            <ta e="T452" id="Seg_5018" s="T451">golomo</ta>
            <ta e="T453" id="Seg_5019" s="T452">tur-ar</ta>
            <ta e="T454" id="Seg_5020" s="T453">golomo</ta>
            <ta e="T455" id="Seg_5021" s="T454">tur-ar</ta>
            <ta e="T456" id="Seg_5022" s="T455">araj</ta>
            <ta e="T457" id="Seg_5023" s="T456">bunu-ga</ta>
            <ta e="T458" id="Seg_5024" s="T457">kiːr-i͡ek-ke</ta>
            <ta e="T459" id="Seg_5025" s="T458">üčügej</ta>
            <ta e="T460" id="Seg_5026" s="T459">golomo-go</ta>
            <ta e="T461" id="Seg_5027" s="T460">golomo-go</ta>
            <ta e="T462" id="Seg_5028" s="T461">köt-ön</ta>
            <ta e="T463" id="Seg_5029" s="T462">tüs-püt-e</ta>
            <ta e="T464" id="Seg_5030" s="T463">ogonnʼor-doːk</ta>
            <ta e="T465" id="Seg_5031" s="T464">emeːksin</ta>
            <ta e="T466" id="Seg_5032" s="T465">emeːksin-e</ta>
            <ta e="T467" id="Seg_5033" s="T466">karag-a</ta>
            <ta e="T468" id="Seg_5034" s="T467">hu͡ok</ta>
            <ta e="T469" id="Seg_5035" s="T468">ogonnʼor-o</ta>
            <ta e="T470" id="Seg_5036" s="T469">hi͡ese</ta>
            <ta e="T471" id="Seg_5037" s="T470">hečen</ta>
            <ta e="T472" id="Seg_5038" s="T471">hogus</ta>
            <ta e="T473" id="Seg_5039" s="T472">dʼe</ta>
            <ta e="T474" id="Seg_5040" s="T473">ah-a-t-al-lar</ta>
            <ta e="T475" id="Seg_5041" s="T474">ma-nɨ</ta>
            <ta e="T476" id="Seg_5042" s="T475">ahɨː-r</ta>
            <ta e="T477" id="Seg_5043" s="T476">bu</ta>
            <ta e="T478" id="Seg_5044" s="T477">ɨ͡aldʼɨt</ta>
            <ta e="T479" id="Seg_5045" s="T478">kaja-kaja</ta>
            <ta e="T480" id="Seg_5046" s="T479">hir-ten</ta>
            <ta e="T481" id="Seg_5047" s="T480">kel-e-gin</ta>
            <ta e="T482" id="Seg_5048" s="T481">di͡e-n</ta>
            <ta e="T483" id="Seg_5049" s="T482">ös</ta>
            <ta e="T484" id="Seg_5050" s="T483">ɨjɨt-al-lar</ta>
            <ta e="T485" id="Seg_5051" s="T484">min</ta>
            <ta e="T487" id="Seg_5052" s="T486">bi͡es</ta>
            <ta e="T488" id="Seg_5053" s="T487">taːs-taːk</ta>
            <ta e="T489" id="Seg_5054" s="T488">hobo-ilim-neːk</ta>
            <ta e="T490" id="Seg_5055" s="T489">e-ti-m</ta>
            <ta e="T491" id="Seg_5056" s="T490">on-tu-bu-ttan</ta>
            <ta e="T492" id="Seg_5057" s="T491">balɨg-ɨ</ta>
            <ta e="T493" id="Seg_5058" s="T492">munnʼ-ar-bɨ-n</ta>
            <ta e="T494" id="Seg_5059" s="T493">u͡or-dar-am-mɨn</ta>
            <ta e="T495" id="Seg_5060" s="T494">ahɨːlaːk-tar-ɨ</ta>
            <ta e="T496" id="Seg_5061" s="T495">ölör-töː-n</ta>
            <ta e="T497" id="Seg_5062" s="T496">kel-e-bin</ta>
            <ta e="T498" id="Seg_5063" s="T497">di͡e-bit</ta>
            <ta e="T499" id="Seg_5064" s="T498">eːk</ta>
            <ta e="T502" id="Seg_5065" s="T501">ahɨːlaːk-tar-ɨ</ta>
            <ta e="T503" id="Seg_5066" s="T502">ölör-ön</ta>
            <ta e="T504" id="Seg_5067" s="T503">kel-bit</ta>
            <ta e="T505" id="Seg_5068" s="T504">bu</ta>
            <ta e="T506" id="Seg_5069" s="T505">ogonnʼot-ton</ta>
            <ta e="T507" id="Seg_5070" s="T506">ɨjɨt-ar</ta>
            <ta e="T508" id="Seg_5071" s="T507">ɨraːktaːgɨ</ta>
            <ta e="T509" id="Seg_5072" s="T508">kɨːs-taːk</ta>
            <ta e="T510" id="Seg_5073" s="T509">duː</ta>
            <ta e="T511" id="Seg_5074" s="T510">di͡e-n</ta>
            <ta e="T512" id="Seg_5075" s="T511">hu͡očča-hogotok</ta>
            <ta e="T513" id="Seg_5076" s="T512">kɨːs-taːk</ta>
            <ta e="T514" id="Seg_5077" s="T513">kanna</ta>
            <ta e="T515" id="Seg_5078" s="T514">ere</ta>
            <ta e="T517" id="Seg_5079" s="T516">er-ge</ta>
            <ta e="T518" id="Seg_5080" s="T517">bi͡er-el-ler</ta>
            <ta e="T519" id="Seg_5081" s="T518">ühü</ta>
            <ta e="T520" id="Seg_5082" s="T519">di͡e-n</ta>
            <ta e="T521" id="Seg_5083" s="T520">ogonnʼor</ta>
            <ta e="T522" id="Seg_5084" s="T521">en</ta>
            <ta e="T523" id="Seg_5085" s="T522">hu͡orumdʼu-ga</ta>
            <ta e="T524" id="Seg_5086" s="T523">bar-ɨ͡a-ŋ</ta>
            <ta e="T525" id="Seg_5087" s="T524">hu͡ok</ta>
            <ta e="T526" id="Seg_5088" s="T525">duː</ta>
            <ta e="T527" id="Seg_5089" s="T526">di͡e-bit</ta>
            <ta e="T528" id="Seg_5090" s="T527">baːjdɨ</ta>
            <ta e="T529" id="Seg_5091" s="T528">ɨ͡aldʼɨt-a</ta>
            <ta e="T530" id="Seg_5092" s="T529">abaga</ta>
            <ta e="T531" id="Seg_5093" s="T530">ɨjɨt-an</ta>
            <ta e="T532" id="Seg_5094" s="T531">kör-ü͡ök-püt</ta>
            <ta e="T533" id="Seg_5095" s="T532">di͡e-bit</ta>
            <ta e="T534" id="Seg_5096" s="T533">biːr</ta>
            <ta e="T535" id="Seg_5097" s="T534">ɨ͡aldʼɨt</ta>
            <ta e="T536" id="Seg_5098" s="T535">kel-bit</ta>
            <ta e="T537" id="Seg_5099" s="T536">kɨːs-kɨ-n</ta>
            <ta e="T538" id="Seg_5100" s="T537">kördüː-r</ta>
            <ta e="T539" id="Seg_5101" s="T538">di͡e-ŋ-ŋin</ta>
            <ta e="T540" id="Seg_5102" s="T539">haŋar</ta>
            <ta e="T541" id="Seg_5103" s="T540">di͡e-bit</ta>
            <ta e="T542" id="Seg_5104" s="T541">bu</ta>
            <ta e="T543" id="Seg_5105" s="T542">kihi</ta>
            <ta e="T544" id="Seg_5106" s="T543">bar-an</ta>
            <ta e="T545" id="Seg_5107" s="T544">kaːl-bɨt</ta>
            <ta e="T546" id="Seg_5108" s="T545">ɨraːktaːgɨ</ta>
            <ta e="T547" id="Seg_5109" s="T546">köt-ön</ta>
            <ta e="T548" id="Seg_5110" s="T547">tüs-püt</ta>
            <ta e="T549" id="Seg_5111" s="T548">kaja</ta>
            <ta e="T550" id="Seg_5112" s="T549">togo</ta>
            <ta e="T551" id="Seg_5113" s="T550">kel-e-gin</ta>
            <ta e="T552" id="Seg_5114" s="T551">hu͡ok</ta>
            <ta e="T553" id="Seg_5115" s="T552">min</ta>
            <ta e="T554" id="Seg_5116" s="T553">ɨ͡aldʼɨt-taːk-pɨn</ta>
            <ta e="T555" id="Seg_5117" s="T554">kaja-kaja</ta>
            <ta e="T556" id="Seg_5118" s="T555">hir-ten</ta>
            <ta e="T557" id="Seg_5119" s="T556">kel-bit-e</ta>
            <ta e="T558" id="Seg_5120" s="T557">bu͡olla</ta>
            <ta e="T559" id="Seg_5121" s="T558">bil-bep-pin</ta>
            <ta e="T560" id="Seg_5122" s="T559">ol</ta>
            <ta e="T561" id="Seg_5123" s="T560">kihi</ta>
            <ta e="T562" id="Seg_5124" s="T561">kɨːs-kɨ-n</ta>
            <ta e="T563" id="Seg_5125" s="T562">kördöː-n</ta>
            <ta e="T564" id="Seg_5126" s="T563">er-er</ta>
            <ta e="T565" id="Seg_5127" s="T564">di͡e-bit</ta>
            <ta e="T566" id="Seg_5128" s="T565">innʼe</ta>
            <ta e="T567" id="Seg_5129" s="T566">gɨn-an</ta>
            <ta e="T568" id="Seg_5130" s="T567">ol</ta>
            <ta e="T569" id="Seg_5131" s="T568">kihi</ta>
            <ta e="T570" id="Seg_5132" s="T569">gi͡en-i-n</ta>
            <ta e="T571" id="Seg_5133" s="T570">hɨraj-ɨ-n-karag-ɨ-n</ta>
            <ta e="T572" id="Seg_5134" s="T571">kör-ü͡ök-pü-n</ta>
            <ta e="T573" id="Seg_5135" s="T572">manna</ta>
            <ta e="T574" id="Seg_5136" s="T573">egel-i-ŋ</ta>
            <ta e="T575" id="Seg_5137" s="T574">di͡e-bit</ta>
            <ta e="T576" id="Seg_5138" s="T575">ɨraːktaːgɨ-ta</ta>
            <ta e="T577" id="Seg_5139" s="T576">bu</ta>
            <ta e="T578" id="Seg_5140" s="T577">ogonnʼor</ta>
            <ta e="T579" id="Seg_5141" s="T578">baːjdɨ</ta>
            <ta e="T580" id="Seg_5142" s="T579">ɨ͡aldʼɨt-ɨ-n</ta>
            <ta e="T581" id="Seg_5143" s="T580">il-pit</ta>
            <ta e="T582" id="Seg_5144" s="T581">pa</ta>
            <ta e="T583" id="Seg_5145" s="T582">min</ta>
            <ta e="T584" id="Seg_5146" s="T583">itinnik</ta>
            <ta e="T585" id="Seg_5147" s="T584">kihi-ge</ta>
            <ta e="T586" id="Seg_5148" s="T585">bi͡er-bep-pin</ta>
            <ta e="T587" id="Seg_5149" s="T586">iliː-ti-n-atag-ɨ-n</ta>
            <ta e="T588" id="Seg_5150" s="T587">tohut-alaː-n</ta>
            <ta e="T589" id="Seg_5151" s="T588">baraːn</ta>
            <ta e="T590" id="Seg_5152" s="T589">kaːjɨː-ga</ta>
            <ta e="T591" id="Seg_5153" s="T590">bɨrag-ɨ-ŋ</ta>
            <ta e="T592" id="Seg_5154" s="T591">di͡e-bit</ta>
            <ta e="T593" id="Seg_5155" s="T592">ek</ta>
            <ta e="T594" id="Seg_5156" s="T593">bu</ta>
            <ta e="T595" id="Seg_5157" s="T594">kihi-gi-n</ta>
            <ta e="T596" id="Seg_5158" s="T595">iliː-ti-n-atag-ɨ-n</ta>
            <ta e="T597" id="Seg_5159" s="T596">tohup-patak-tar</ta>
            <ta e="T598" id="Seg_5160" s="T597">bɨ͡a-nnan</ta>
            <ta e="T599" id="Seg_5161" s="T598">kelgij-en</ta>
            <ta e="T600" id="Seg_5162" s="T599">bar-aːn-nar</ta>
            <ta e="T601" id="Seg_5163" s="T600">kaːjɨː-ga</ta>
            <ta e="T602" id="Seg_5164" s="T601">ug-an</ta>
            <ta e="T603" id="Seg_5165" s="T602">keːs-pit-ter</ta>
            <ta e="T604" id="Seg_5166" s="T603">maːjdɨ</ta>
            <ta e="T605" id="Seg_5167" s="T604">kihi-gi-n</ta>
            <ta e="T607" id="Seg_5168" s="T606">bu</ta>
            <ta e="T608" id="Seg_5169" s="T607">kihi</ta>
            <ta e="T609" id="Seg_5170" s="T608">kutujak</ta>
            <ta e="T610" id="Seg_5171" s="T609">bu͡ol-an</ta>
            <ta e="T611" id="Seg_5172" s="T610">taks-a</ta>
            <ta e="T612" id="Seg_5173" s="T611">köp-püt</ta>
            <ta e="T613" id="Seg_5174" s="T612">taks-a</ta>
            <ta e="T614" id="Seg_5175" s="T613">köt-ön</ta>
            <ta e="T615" id="Seg_5176" s="T614">ogonnʼor-u-gar</ta>
            <ta e="T616" id="Seg_5177" s="T615">emi͡e</ta>
            <ta e="T617" id="Seg_5178" s="T616">kiːr-bit</ta>
            <ta e="T618" id="Seg_5179" s="T617">dʼe</ta>
            <ta e="T619" id="Seg_5180" s="T618">bil-bep-pin</ta>
            <ta e="T620" id="Seg_5181" s="T619">maːjdɨŋ-ɨ-taːgar</ta>
            <ta e="T621" id="Seg_5182" s="T620">atɨn</ta>
            <ta e="T622" id="Seg_5183" s="T621">kihi</ta>
            <ta e="T623" id="Seg_5184" s="T622">kel-bit-e</ta>
            <ta e="T624" id="Seg_5185" s="T623">duː</ta>
            <ta e="T625" id="Seg_5186" s="T624">tu͡ok</ta>
            <ta e="T626" id="Seg_5187" s="T625">duː</ta>
            <ta e="T627" id="Seg_5188" s="T626">emi͡e</ta>
            <ta e="T628" id="Seg_5189" s="T627">kɨːs-kɨ-n</ta>
            <ta e="T629" id="Seg_5190" s="T628">kördöː-n</ta>
            <ta e="T630" id="Seg_5191" s="T629">er-er</ta>
            <ta e="T631" id="Seg_5192" s="T630">di͡e-ŋ-ŋin</ta>
            <ta e="T632" id="Seg_5193" s="T631">bar</ta>
            <ta e="T633" id="Seg_5194" s="T632">dʼe</ta>
            <ta e="T634" id="Seg_5195" s="T633">di͡e-bit</ta>
            <ta e="T635" id="Seg_5196" s="T634">emi͡e</ta>
            <ta e="T636" id="Seg_5197" s="T635">bar-bɨt</ta>
            <ta e="T637" id="Seg_5198" s="T636">ogonnʼor</ta>
            <ta e="T638" id="Seg_5199" s="T637">dʼe</ta>
            <ta e="T639" id="Seg_5200" s="T638">ol</ta>
            <ta e="T640" id="Seg_5201" s="T639">baran</ta>
            <ta e="T641" id="Seg_5202" s="T640">emi͡e</ta>
            <ta e="T642" id="Seg_5203" s="T641">di͡e-bit</ta>
            <ta e="T643" id="Seg_5204" s="T642">kaja</ta>
            <ta e="T644" id="Seg_5205" s="T643">emi͡e</ta>
            <ta e="T645" id="Seg_5206" s="T644">ɨ͡aldʼɨt</ta>
            <ta e="T646" id="Seg_5207" s="T645">kel-l-e</ta>
            <ta e="T647" id="Seg_5208" s="T646">kɨːs-kɨ-n</ta>
            <ta e="T648" id="Seg_5209" s="T647">kördöː-n</ta>
            <ta e="T649" id="Seg_5210" s="T648">er-er</ta>
            <ta e="T650" id="Seg_5211" s="T649">di͡e-bit</ta>
            <ta e="T651" id="Seg_5212" s="T650">dogot-tor</ta>
            <ta e="T652" id="Seg_5213" s="T651">maːjdɨ</ta>
            <ta e="T653" id="Seg_5214" s="T652">kaːjɨː-ga</ta>
            <ta e="T654" id="Seg_5215" s="T653">bɨrag-aːččɨ</ta>
            <ta e="T655" id="Seg_5216" s="T654">kihi-giti-n</ta>
            <ta e="T656" id="Seg_5217" s="T655">kör-ü-ŋ</ta>
            <ta e="T657" id="Seg_5218" s="T656">dʼe</ta>
            <ta e="T658" id="Seg_5219" s="T657">kajtak-kajtak</ta>
            <ta e="T659" id="Seg_5220" s="T658">kihi-ni</ta>
            <ta e="T660" id="Seg_5221" s="T659">dʼabɨl-ɨː-bɨt</ta>
            <ta e="T661" id="Seg_5222" s="T660">dʼürü</ta>
            <ta e="T662" id="Seg_5223" s="T661">di͡e-bit</ta>
            <ta e="T663" id="Seg_5224" s="T662">kaːjɨː-ga</ta>
            <ta e="T664" id="Seg_5225" s="T663">bɨrak-pɨt</ta>
            <ta e="T665" id="Seg_5226" s="T664">kihi-te</ta>
            <ta e="T666" id="Seg_5227" s="T665">hu͡ol-a</ta>
            <ta e="T667" id="Seg_5228" s="T666">ere</ta>
            <ta e="T668" id="Seg_5229" s="T667">čöŋöj-ö</ta>
            <ta e="T669" id="Seg_5230" s="T668">hɨt-ar</ta>
            <ta e="T670" id="Seg_5231" s="T669">kelgij-bit</ta>
            <ta e="T671" id="Seg_5232" s="T670">bɨ͡a-ta</ta>
            <ta e="T672" id="Seg_5233" s="T671">bɨ͡a-tɨ-nan</ta>
            <ta e="T673" id="Seg_5234" s="T672">hɨt-ar</ta>
            <ta e="T674" id="Seg_5235" s="T673">dʼe</ta>
            <ta e="T675" id="Seg_5236" s="T674">kaja</ta>
            <ta e="T676" id="Seg_5237" s="T675">onnuk-onnuk</ta>
            <ta e="T677" id="Seg_5238" s="T676">kaja</ta>
            <ta e="T678" id="Seg_5239" s="T677">o</ta>
            <ta e="T679" id="Seg_5240" s="T678">dʼe</ta>
            <ta e="T680" id="Seg_5241" s="T679">ile</ta>
            <ta e="T681" id="Seg_5242" s="T680">da</ta>
            <ta e="T682" id="Seg_5243" s="T681">üčügej</ta>
            <ta e="T683" id="Seg_5244" s="T682">kihi-ni</ta>
            <ta e="T684" id="Seg_5245" s="T683">gɨn-nɨ-m</ta>
            <ta e="T685" id="Seg_5246" s="T684">e</ta>
            <ta e="T686" id="Seg_5247" s="T685">badaga</ta>
            <ta e="T687" id="Seg_5248" s="T686">kɨːs-pɨ-n</ta>
            <ta e="T688" id="Seg_5249" s="T687">bi͡er-e-bin</ta>
            <ta e="T689" id="Seg_5250" s="T688">d-i͡e-m</ta>
            <ta e="T690" id="Seg_5251" s="T689">bu͡olla</ta>
            <ta e="T691" id="Seg_5252" s="T690">bu</ta>
            <ta e="T692" id="Seg_5253" s="T691">kihi</ta>
            <ta e="T693" id="Seg_5254" s="T692">maːgɨ-laːk</ta>
            <ta e="T694" id="Seg_5255" s="T693">ahɨːlaːk</ta>
            <ta e="T695" id="Seg_5256" s="T694">ölör-büt-ü-n</ta>
            <ta e="T696" id="Seg_5257" s="T695">ɨraːktaːgɨ</ta>
            <ta e="T697" id="Seg_5258" s="T696">töhög-ü-ger</ta>
            <ta e="T698" id="Seg_5259" s="T697">tog-o</ta>
            <ta e="T699" id="Seg_5260" s="T698">bɨrak-pɨt</ta>
            <ta e="T700" id="Seg_5261" s="T699">innʼe</ta>
            <ta e="T701" id="Seg_5262" s="T700">gɨn-an</ta>
            <ta e="T702" id="Seg_5263" s="T701">köh-ü-n</ta>
            <ta e="T703" id="Seg_5264" s="T702">kenni-ti-ger</ta>
            <ta e="T704" id="Seg_5265" s="T703">čɨːčaːk</ta>
            <ta e="T705" id="Seg_5266" s="T704">ɨllɨː-r</ta>
            <ta e="T706" id="Seg_5267" s="T705">godolutan</ta>
            <ta e="T707" id="Seg_5268" s="T706">araːr-an</ta>
            <ta e="T708" id="Seg_5269" s="T707">il-pit</ta>
            <ta e="T709" id="Seg_5270" s="T708">bu</ta>
            <ta e="T710" id="Seg_5271" s="T709">kihi</ta>
            <ta e="T711" id="Seg_5272" s="T710">eŋin-eŋin</ta>
            <ta e="T712" id="Seg_5273" s="T711">haldaːt-taːk-tu͡ok-taːk</ta>
            <ta e="T713" id="Seg_5274" s="T712">bu</ta>
            <ta e="T714" id="Seg_5275" s="T713">kihi</ta>
            <ta e="T715" id="Seg_5276" s="T714">dʼe</ta>
            <ta e="T716" id="Seg_5277" s="T715">bar-ar</ta>
            <ta e="T717" id="Seg_5278" s="T716">dʼi͡e-ti-ger</ta>
            <ta e="T718" id="Seg_5279" s="T717">kɨːh-ɨ-n</ta>
            <ta e="T719" id="Seg_5280" s="T718">ɨl-an</ta>
            <ta e="T720" id="Seg_5281" s="T719">baraːn</ta>
            <ta e="T721" id="Seg_5282" s="T720">dʼe</ta>
            <ta e="T722" id="Seg_5283" s="T721">köh-ör</ta>
            <ta e="T723" id="Seg_5284" s="T722">köh-ön</ta>
            <ta e="T724" id="Seg_5285" s="T723">iti</ta>
            <ta e="T725" id="Seg_5286" s="T724">kel-en-ner</ta>
            <ta e="T726" id="Seg_5287" s="T725">iti</ta>
            <ta e="T727" id="Seg_5288" s="T726">dʼe</ta>
            <ta e="T728" id="Seg_5289" s="T727">iti</ta>
            <ta e="T729" id="Seg_5290" s="T728">baj-an-tot-on</ta>
            <ta e="T730" id="Seg_5291" s="T729">olor-but-a</ta>
            <ta e="T731" id="Seg_5292" s="T730">dʼe</ta>
            <ta e="T732" id="Seg_5293" s="T731">iti</ta>
            <ta e="T733" id="Seg_5294" s="T732">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_5295" s="T1">Čopočuka</ta>
            <ta e="T3" id="Seg_5296" s="T2">olor-BIT</ta>
            <ta e="T4" id="Seg_5297" s="T3">ühü</ta>
            <ta e="T5" id="Seg_5298" s="T4">bi͡es</ta>
            <ta e="T6" id="Seg_5299" s="T5">mas</ta>
            <ta e="T7" id="Seg_5300" s="T6">turu͡or-BIT</ta>
            <ta e="T8" id="Seg_5301" s="T7">golomo</ta>
            <ta e="T9" id="Seg_5302" s="T8">dʼi͡e-LAːK</ta>
            <ta e="T10" id="Seg_5303" s="T9">bi͡es</ta>
            <ta e="T11" id="Seg_5304" s="T10">taːs-LAːK</ta>
            <ta e="T12" id="Seg_5305" s="T11">hobo-ilim-LAːK</ta>
            <ta e="T13" id="Seg_5306" s="T12">kün</ta>
            <ta e="T14" id="Seg_5307" s="T13">aːjɨ</ta>
            <ta e="T15" id="Seg_5308" s="T14">ilim-LAN-A</ta>
            <ta e="T16" id="Seg_5309" s="T15">kiːr-Ar</ta>
            <ta e="T17" id="Seg_5310" s="T16">bu</ta>
            <ta e="T18" id="Seg_5311" s="T17">kihi</ta>
            <ta e="T19" id="Seg_5312" s="T18">balɨk-nI</ta>
            <ta e="T20" id="Seg_5313" s="T19">logloj-ččI</ta>
            <ta e="T21" id="Seg_5314" s="T20">mus-BIT</ta>
            <ta e="T22" id="Seg_5315" s="T21">ilim-tI-ttAn</ta>
            <ta e="T23" id="Seg_5316" s="T22">innʼe</ta>
            <ta e="T24" id="Seg_5317" s="T23">gɨn-An</ta>
            <ta e="T25" id="Seg_5318" s="T24">biːrde</ta>
            <ta e="T26" id="Seg_5319" s="T25">bar-BIT-tA</ta>
            <ta e="T27" id="Seg_5320" s="T26">kɨrsa-LAr</ta>
            <ta e="T28" id="Seg_5321" s="T27">tu͡ok-LAr</ta>
            <ta e="T29" id="Seg_5322" s="T28">hi͡e-BIT-LAr</ta>
            <ta e="T30" id="Seg_5323" s="T29">agaj</ta>
            <ta e="T31" id="Seg_5324" s="T30">balɨk-tI-n</ta>
            <ta e="T32" id="Seg_5325" s="T31">bu-nI</ta>
            <ta e="T33" id="Seg_5326" s="T32">irdeː-IAK-GA</ta>
            <ta e="T34" id="Seg_5327" s="T33">hu͡ol-tI-n</ta>
            <ta e="T35" id="Seg_5328" s="T34">irdeː-IAK-GA</ta>
            <ta e="T36" id="Seg_5329" s="T35">balɨk-tI-n</ta>
            <ta e="T37" id="Seg_5330" s="T36">dʼi͡e-tI-GAr</ta>
            <ta e="T38" id="Seg_5331" s="T37">tas-An</ta>
            <ta e="T39" id="Seg_5332" s="T38">baran</ta>
            <ta e="T40" id="Seg_5333" s="T39">bu</ta>
            <ta e="T41" id="Seg_5334" s="T40">kihi</ta>
            <ta e="T42" id="Seg_5335" s="T41">kɨrsa</ta>
            <ta e="T43" id="Seg_5336" s="T42">hu͡ol-tI-n</ta>
            <ta e="T44" id="Seg_5337" s="T43">irdeː-BIT</ta>
            <ta e="T45" id="Seg_5338" s="T44">kɨrsa</ta>
            <ta e="T46" id="Seg_5339" s="T45">hu͡ol-tI-n</ta>
            <ta e="T47" id="Seg_5340" s="T46">irdeː-An</ta>
            <ta e="T48" id="Seg_5341" s="T47">is-TAK-InA</ta>
            <ta e="T49" id="Seg_5342" s="T48">kaja</ta>
            <ta e="T50" id="Seg_5343" s="T49">kɨrsa</ta>
            <ta e="T51" id="Seg_5344" s="T50">tu͡ok-nI</ta>
            <ta e="T52" id="Seg_5345" s="T51">i</ta>
            <ta e="T53" id="Seg_5346" s="T52">biːr</ta>
            <ta e="T54" id="Seg_5347" s="T53">talak</ta>
            <ta e="T55" id="Seg_5348" s="T54">is-tI-ttAn</ta>
            <ta e="T56" id="Seg_5349" s="T55">tur-A</ta>
            <ta e="T57" id="Seg_5350" s="T56">ekkireː-BIT</ta>
            <ta e="T58" id="Seg_5351" s="T57">araː</ta>
            <ta e="T59" id="Seg_5352" s="T58">Čopočuka</ta>
            <ta e="T60" id="Seg_5353" s="T59">kihi-nI</ta>
            <ta e="T61" id="Seg_5354" s="T60">hohuj-t-TI-ŋ</ta>
            <ta e="T62" id="Seg_5355" s="T61">kaja</ta>
            <ta e="T63" id="Seg_5356" s="T62">kajdak</ta>
            <ta e="T64" id="Seg_5357" s="T63">kel-A-GIn</ta>
            <ta e="T65" id="Seg_5358" s="T64">kaja</ta>
            <ta e="T66" id="Seg_5359" s="T65">balɨk-BI-n</ta>
            <ta e="T67" id="Seg_5360" s="T66">ka</ta>
            <ta e="T68" id="Seg_5361" s="T67">togo</ta>
            <ta e="T69" id="Seg_5362" s="T68">u͡or-BIT-GIt=Ij</ta>
            <ta e="T70" id="Seg_5363" s="T69">ehigi</ta>
            <ta e="T71" id="Seg_5364" s="T70">ho</ta>
            <ta e="T72" id="Seg_5365" s="T71">min</ta>
            <ta e="T73" id="Seg_5366" s="T72">u͡or-BIT-I-m</ta>
            <ta e="T74" id="Seg_5367" s="T73">du͡o</ta>
            <ta e="T75" id="Seg_5368" s="T74">di͡e-A-di͡e-A</ta>
            <ta e="T76" id="Seg_5369" s="T75">gɨn-BIT</ta>
            <ta e="T77" id="Seg_5370" s="T76">bu-tI-ŋ</ta>
            <ta e="T78" id="Seg_5371" s="T77">kutujak</ta>
            <ta e="T79" id="Seg_5372" s="T78">bu͡ol-BIT</ta>
            <ta e="T80" id="Seg_5373" s="T79">kutujak</ta>
            <ta e="T81" id="Seg_5374" s="T80">bu͡ol-An</ta>
            <ta e="T82" id="Seg_5375" s="T81">dʼe</ta>
            <ta e="T83" id="Seg_5376" s="T82">ogonnʼor-I-ŋ</ta>
            <ta e="T84" id="Seg_5377" s="T83">dʼe</ta>
            <ta e="T85" id="Seg_5378" s="T84">hɨrɨt-Ar</ta>
            <ta e="T86" id="Seg_5379" s="T85">baːgɨ-nI</ta>
            <ta e="T87" id="Seg_5380" s="T86">u͡op-An</ta>
            <ta e="T88" id="Seg_5381" s="T87">keːs-BIT</ta>
            <ta e="T89" id="Seg_5382" s="T88">kɨrsa-tA</ta>
            <ta e="T90" id="Seg_5383" s="T89">baːgɨ-ŋ</ta>
            <ta e="T91" id="Seg_5384" s="T90">is-tI-n</ta>
            <ta e="T92" id="Seg_5385" s="T91">kaja</ta>
            <ta e="T95" id="Seg_5386" s="T94">battaː-An</ta>
            <ta e="T96" id="Seg_5387" s="T95">bɨs-An</ta>
            <ta e="T97" id="Seg_5388" s="T96">tagɨs-A</ta>
            <ta e="T100" id="Seg_5389" s="T99">tagɨs-A</ta>
            <ta e="T101" id="Seg_5390" s="T100">köt-BIT</ta>
            <ta e="T102" id="Seg_5391" s="T101">innʼe</ta>
            <ta e="T103" id="Seg_5392" s="T102">baran</ta>
            <ta e="T104" id="Seg_5393" s="T103">baːgɨ-nI</ta>
            <ta e="T105" id="Seg_5394" s="T104">hül-Ar</ta>
            <ta e="T106" id="Seg_5395" s="T105">da</ta>
            <ta e="T107" id="Seg_5396" s="T106">karmaːn-tI-GAr</ta>
            <ta e="T108" id="Seg_5397" s="T107">ugun-Ar</ta>
            <ta e="T109" id="Seg_5398" s="T108">da</ta>
            <ta e="T110" id="Seg_5399" s="T109">kaːm-An</ta>
            <ta e="T111" id="Seg_5400" s="T110">is-BIT</ta>
            <ta e="T112" id="Seg_5401" s="T111">epeːt</ta>
            <ta e="T113" id="Seg_5402" s="T112">ilin</ta>
            <ta e="T114" id="Seg_5403" s="T113">dek</ta>
            <ta e="T115" id="Seg_5404" s="T114">kaːm-An</ta>
            <ta e="T116" id="Seg_5405" s="T115">is-An</ta>
            <ta e="T117" id="Seg_5406" s="T116">uskaːn</ta>
            <ta e="T118" id="Seg_5407" s="T117">turu͡or-BIT</ta>
            <ta e="T119" id="Seg_5408" s="T118">araː</ta>
            <ta e="T120" id="Seg_5409" s="T119">Čopočuka</ta>
            <ta e="T121" id="Seg_5410" s="T120">tu͡ok</ta>
            <ta e="T122" id="Seg_5411" s="T121">kaja</ta>
            <ta e="T123" id="Seg_5412" s="T122">hir-ttAn</ta>
            <ta e="T124" id="Seg_5413" s="T123">kel-A-GIn</ta>
            <ta e="T125" id="Seg_5414" s="T124">kihi-nI</ta>
            <ta e="T126" id="Seg_5415" s="T125">hohuj-t-AːktAː-TI-ŋ</ta>
            <ta e="T127" id="Seg_5416" s="T126">di͡e-BIT</ta>
            <ta e="T128" id="Seg_5417" s="T127">kaja</ta>
            <ta e="T129" id="Seg_5418" s="T128">balɨk-BI-n</ta>
            <ta e="T130" id="Seg_5419" s="T129">togo</ta>
            <ta e="T131" id="Seg_5420" s="T130">barɨ-tI-n</ta>
            <ta e="T132" id="Seg_5421" s="T131">hi͡e-BIT-GIt=Ij</ta>
            <ta e="T133" id="Seg_5422" s="T132">balɨk-BI-n</ta>
            <ta e="T134" id="Seg_5423" s="T133">irdeː-A</ta>
            <ta e="T135" id="Seg_5424" s="T134">hɨrɨt-A-BIn</ta>
            <ta e="T138" id="Seg_5425" s="T136">baːgɨ</ta>
            <ta e="T139" id="Seg_5426" s="T138">emi͡e</ta>
            <ta e="T141" id="Seg_5427" s="T139">kaja</ta>
            <ta e="T142" id="Seg_5428" s="T141">dʼe</ta>
            <ta e="T143" id="Seg_5429" s="T142">dʼe</ta>
            <ta e="T144" id="Seg_5430" s="T143">ölörös-Ar-LAr</ta>
            <ta e="T145" id="Seg_5431" s="T144">e</ta>
            <ta e="T146" id="Seg_5432" s="T145">uskaːn-nI</ta>
            <ta e="T147" id="Seg_5433" s="T146">kɨtta</ta>
            <ta e="T148" id="Seg_5434" s="T147">ölörös-An-LAr</ta>
            <ta e="T149" id="Seg_5435" s="T148">uskaːn-nI</ta>
            <ta e="T150" id="Seg_5436" s="T149">tu͡ok-tI-n</ta>
            <ta e="T151" id="Seg_5437" s="T150">kihi-LAː-IAK.[tA]=Ij</ta>
            <ta e="T152" id="Seg_5438" s="T151">ol</ta>
            <ta e="T153" id="Seg_5439" s="T152">kihi</ta>
            <ta e="T154" id="Seg_5440" s="T153">baːgɨ-nI</ta>
            <ta e="T155" id="Seg_5441" s="T154">ölör-BIT</ta>
            <ta e="T156" id="Seg_5442" s="T155">da</ta>
            <ta e="T157" id="Seg_5443" s="T156">emi͡e</ta>
            <ta e="T159" id="Seg_5444" s="T158">karmaːn-tI-GAr</ta>
            <ta e="T160" id="Seg_5445" s="T159">ugun-An</ta>
            <ta e="T161" id="Seg_5446" s="T160">keːs-BIT</ta>
            <ta e="T162" id="Seg_5447" s="T161">hül-I-n-An</ta>
            <ta e="T163" id="Seg_5448" s="T162">emi͡e</ta>
            <ta e="T164" id="Seg_5449" s="T163">kaːm-An</ta>
            <ta e="T165" id="Seg_5450" s="T164">is-BIT</ta>
            <ta e="T166" id="Seg_5451" s="T165">kaːm-An-kaːm-An</ta>
            <ta e="T167" id="Seg_5452" s="T166">is-TAK-InA</ta>
            <ta e="T168" id="Seg_5453" s="T167">hahɨl</ta>
            <ta e="T169" id="Seg_5454" s="T168">tur-A</ta>
            <ta e="T170" id="Seg_5455" s="T169">ekkireː-BIT</ta>
            <ta e="T171" id="Seg_5456" s="T170">oː</ta>
            <ta e="T172" id="Seg_5457" s="T171">Čopočuka</ta>
            <ta e="T173" id="Seg_5458" s="T172">kihi-nI</ta>
            <ta e="T174" id="Seg_5459" s="T173">hohuj-t-TI-ŋ</ta>
            <ta e="T175" id="Seg_5460" s="T174">kaja</ta>
            <ta e="T176" id="Seg_5461" s="T175">kaja</ta>
            <ta e="T177" id="Seg_5462" s="T176">hir-ttAn</ta>
            <ta e="T178" id="Seg_5463" s="T177">kel-A-GIn</ta>
            <ta e="T179" id="Seg_5464" s="T178">bu</ta>
            <ta e="T180" id="Seg_5465" s="T179">di͡e-BIT</ta>
            <ta e="T181" id="Seg_5466" s="T180">kaja</ta>
            <ta e="T182" id="Seg_5467" s="T181">di͡e-BIT</ta>
            <ta e="T183" id="Seg_5468" s="T182">ka</ta>
            <ta e="T184" id="Seg_5469" s="T183">balɨk-BI-n</ta>
            <ta e="T185" id="Seg_5470" s="T184">barɨ-tI-n</ta>
            <ta e="T186" id="Seg_5471" s="T185">hi͡e-BIT-GIt</ta>
            <ta e="T187" id="Seg_5472" s="T186">ol</ta>
            <ta e="T188" id="Seg_5473" s="T187">ihin</ta>
            <ta e="T189" id="Seg_5474" s="T188">irdeː-An</ta>
            <ta e="T190" id="Seg_5475" s="T189">kel-A-BIn</ta>
            <ta e="T191" id="Seg_5476" s="T190">pa</ta>
            <ta e="T192" id="Seg_5477" s="T191">en</ta>
            <ta e="T193" id="Seg_5478" s="T192">balɨk-GA-r</ta>
            <ta e="T194" id="Seg_5479" s="T193">bihigi</ta>
            <ta e="T195" id="Seg_5480" s="T194">čugahaː-BAtAK-BIt</ta>
            <ta e="T196" id="Seg_5481" s="T195">daːganɨ</ta>
            <ta e="T197" id="Seg_5482" s="T196">össü͡ö</ta>
            <ta e="T198" id="Seg_5483" s="T197">kata</ta>
            <ta e="T199" id="Seg_5484" s="T198">min</ta>
            <ta e="T200" id="Seg_5485" s="T199">en-n</ta>
            <ta e="T201" id="Seg_5486" s="T200">tɨrɨt-A</ta>
            <ta e="T202" id="Seg_5487" s="T201">tɨːt-An</ta>
            <ta e="T203" id="Seg_5488" s="T202">hi͡e-An</ta>
            <ta e="T204" id="Seg_5489" s="T203">keːs-IAK-m</ta>
            <ta e="T205" id="Seg_5490" s="T204">di͡e-BIT</ta>
            <ta e="T206" id="Seg_5491" s="T205">dʼe</ta>
            <ta e="T207" id="Seg_5492" s="T206">ölörös-Ar-LAr</ta>
            <ta e="T208" id="Seg_5493" s="T207">bu</ta>
            <ta e="T209" id="Seg_5494" s="T208">kihi-LAr</ta>
            <ta e="T210" id="Seg_5495" s="T209">Čopočuka</ta>
            <ta e="T211" id="Seg_5496" s="T210">kannɨk-tI-tI-n</ta>
            <ta e="T212" id="Seg_5497" s="T211">kihi-LAː-IAK.[tA]=Ij</ta>
            <ta e="T213" id="Seg_5498" s="T212">ti͡er-A</ta>
            <ta e="T214" id="Seg_5499" s="T213">battaː-BIT</ta>
            <ta e="T215" id="Seg_5500" s="T214">da</ta>
            <ta e="T216" id="Seg_5501" s="T215">ölör-An</ta>
            <ta e="T217" id="Seg_5502" s="T216">keːs-BIT</ta>
            <ta e="T218" id="Seg_5503" s="T217">baːgɨ-nI</ta>
            <ta e="T219" id="Seg_5504" s="T218">hül-BIT</ta>
            <ta e="T220" id="Seg_5505" s="T219">da</ta>
            <ta e="T221" id="Seg_5506" s="T220">karmaːn-tI-GAr</ta>
            <ta e="T222" id="Seg_5507" s="T221">karmaːn</ta>
            <ta e="T223" id="Seg_5508" s="T222">da</ta>
            <ta e="T224" id="Seg_5509" s="T223">ulakan</ta>
            <ta e="T225" id="Seg_5510" s="T224">hök-TI-m</ta>
            <ta e="T226" id="Seg_5511" s="T225">hahɨl-tI-n</ta>
            <ta e="T227" id="Seg_5512" s="T226">ugun-An</ta>
            <ta e="T228" id="Seg_5513" s="T227">keːs-BIT</ta>
            <ta e="T229" id="Seg_5514" s="T228">karmaːn-tI-GAr</ta>
            <ta e="T230" id="Seg_5515" s="T229">onton</ta>
            <ta e="T231" id="Seg_5516" s="T230">kaːm-An</ta>
            <ta e="T232" id="Seg_5517" s="T231">is-BIT</ta>
            <ta e="T233" id="Seg_5518" s="T232">kaːm-An</ta>
            <ta e="T234" id="Seg_5519" s="T233">is-A</ta>
            <ta e="T235" id="Seg_5520" s="T234">is-TAK-InA</ta>
            <ta e="T236" id="Seg_5521" s="T235">hi͡egen</ta>
            <ta e="T237" id="Seg_5522" s="T236">tur-A</ta>
            <ta e="T238" id="Seg_5523" s="T237">ekkireː-BIT</ta>
            <ta e="T239" id="Seg_5524" s="T238">kaja</ta>
            <ta e="T240" id="Seg_5525" s="T239">Čopočuka</ta>
            <ta e="T241" id="Seg_5526" s="T240">kihi-nI</ta>
            <ta e="T242" id="Seg_5527" s="T241">hohuj-t-TI-ŋ</ta>
            <ta e="T243" id="Seg_5528" s="T242">kata</ta>
            <ta e="T244" id="Seg_5529" s="T243">di͡e-BIT</ta>
            <ta e="T245" id="Seg_5530" s="T244">togo</ta>
            <ta e="T246" id="Seg_5531" s="T245">hɨrɨt-A-GIn</ta>
            <ta e="T247" id="Seg_5532" s="T246">bu</ta>
            <ta e="T248" id="Seg_5533" s="T247">di͡e-An</ta>
            <ta e="T249" id="Seg_5534" s="T248">dojdu-GA</ta>
            <ta e="T250" id="Seg_5535" s="T249">pa</ta>
            <ta e="T251" id="Seg_5536" s="T250">ka</ta>
            <ta e="T252" id="Seg_5537" s="T251">balɨk-BI-n</ta>
            <ta e="T253" id="Seg_5538" s="T252">togo</ta>
            <ta e="T254" id="Seg_5539" s="T253">barɨ-tI-n</ta>
            <ta e="T255" id="Seg_5540" s="T254">hi͡e-BIT-GIt=Ij</ta>
            <ta e="T256" id="Seg_5541" s="T255">ol-nI</ta>
            <ta e="T257" id="Seg_5542" s="T256">irdeː-A</ta>
            <ta e="T258" id="Seg_5543" s="T257">hɨrɨt-A-BIn</ta>
            <ta e="T259" id="Seg_5544" s="T258">di͡e-BIT</ta>
            <ta e="T260" id="Seg_5545" s="T259">össü͡ö</ta>
            <ta e="T261" id="Seg_5546" s="T260">hi͡e-BIT-GIt=Ij</ta>
            <ta e="T262" id="Seg_5547" s="T261">kap-An</ta>
            <ta e="T263" id="Seg_5548" s="T262">ɨl-Ar</ta>
            <ta e="T264" id="Seg_5549" s="T263">da</ta>
            <ta e="T265" id="Seg_5550" s="T264">baːgɨ-nI</ta>
            <ta e="T266" id="Seg_5551" s="T265">dʼe</ta>
            <ta e="T267" id="Seg_5552" s="T266">ölörös-Ar</ta>
            <ta e="T268" id="Seg_5553" s="T267">ölörös-An</ta>
            <ta e="T269" id="Seg_5554" s="T268">Čopočuka</ta>
            <ta e="T270" id="Seg_5555" s="T269">tu͡ok-tI-n</ta>
            <ta e="T271" id="Seg_5556" s="T270">kihi-LAː-IAK.[tA]=Ij</ta>
            <ta e="T272" id="Seg_5557" s="T271">kihi-LAː-BAtAK</ta>
            <ta e="T273" id="Seg_5558" s="T272">ölör-BIT</ta>
            <ta e="T274" id="Seg_5559" s="T273">da</ta>
            <ta e="T275" id="Seg_5560" s="T274">emi͡e</ta>
            <ta e="T276" id="Seg_5561" s="T275">hül-BIT</ta>
            <ta e="T277" id="Seg_5562" s="T276">da</ta>
            <ta e="T278" id="Seg_5563" s="T277">karmaːn-tI-GAr</ta>
            <ta e="T279" id="Seg_5564" s="T278">ugun-An</ta>
            <ta e="T280" id="Seg_5565" s="T279">keːs-BIT</ta>
            <ta e="T281" id="Seg_5566" s="T280">emi͡e</ta>
            <ta e="T282" id="Seg_5567" s="T281">kaːm-An</ta>
            <ta e="T283" id="Seg_5568" s="T282">is-BIT</ta>
            <ta e="T284" id="Seg_5569" s="T283">kaːm-An-kaːm-An</ta>
            <ta e="T285" id="Seg_5570" s="T284">börö-nI</ta>
            <ta e="T286" id="Seg_5571" s="T285">körüs-I-BIT</ta>
            <ta e="T287" id="Seg_5572" s="T286">börö-tA</ta>
            <ta e="T288" id="Seg_5573" s="T287">tur-A</ta>
            <ta e="T289" id="Seg_5574" s="T288">ekkireː-BIT</ta>
            <ta e="T290" id="Seg_5575" s="T289">o</ta>
            <ta e="T291" id="Seg_5576" s="T290">Čopočuka</ta>
            <ta e="T292" id="Seg_5577" s="T291">kihi-nI</ta>
            <ta e="T293" id="Seg_5578" s="T292">hohuj-t-TI-ŋ</ta>
            <ta e="T294" id="Seg_5579" s="T293">ka</ta>
            <ta e="T295" id="Seg_5580" s="T294">kajdi͡ek</ta>
            <ta e="T296" id="Seg_5581" s="T295">is-A-GIn</ta>
            <ta e="T297" id="Seg_5582" s="T296">di͡e-BIT</ta>
            <ta e="T298" id="Seg_5583" s="T297">kaja</ta>
            <ta e="T299" id="Seg_5584" s="T298">balɨk-BI-n</ta>
            <ta e="T300" id="Seg_5585" s="T299">togo</ta>
            <ta e="T301" id="Seg_5586" s="T300">hi͡e-BIT-GIt=Ij</ta>
            <ta e="T302" id="Seg_5587" s="T301">ol-tI-BI-n</ta>
            <ta e="T303" id="Seg_5588" s="T302">irdeː-A-BIn</ta>
            <ta e="T304" id="Seg_5589" s="T303">pa</ta>
            <ta e="T305" id="Seg_5590" s="T304">en</ta>
            <ta e="T306" id="Seg_5591" s="T305">balɨk-GA-r</ta>
            <ta e="T307" id="Seg_5592" s="T306">čugahaː-BAtAK-BIt</ta>
            <ta e="T308" id="Seg_5593" s="T307">daːganɨ</ta>
            <ta e="T309" id="Seg_5594" s="T308">kata</ta>
            <ta e="T310" id="Seg_5595" s="T309">min</ta>
            <ta e="T311" id="Seg_5596" s="T310">en-n</ta>
            <ta e="T312" id="Seg_5597" s="T311">hi͡e-An</ta>
            <ta e="T313" id="Seg_5598" s="T312">keːs-IAK-m</ta>
            <ta e="T314" id="Seg_5599" s="T313">di͡e-BIT</ta>
            <ta e="T315" id="Seg_5600" s="T314">baːgɨ-nI</ta>
            <ta e="T316" id="Seg_5601" s="T315">u͡op-An</ta>
            <ta e="T317" id="Seg_5602" s="T316">keːs-BIT</ta>
            <ta e="T318" id="Seg_5603" s="T317">ɨnʼɨr-An</ta>
            <ta e="T319" id="Seg_5604" s="T318">keːs-BIT</ta>
            <ta e="T320" id="Seg_5605" s="T319">Čopočuka-nI</ta>
            <ta e="T321" id="Seg_5606" s="T320">Čopočuka</ta>
            <ta e="T322" id="Seg_5607" s="T321">is-tI-n</ta>
            <ta e="T323" id="Seg_5608" s="T322">kaj-A</ta>
            <ta e="T324" id="Seg_5609" s="T323">battaː-Ar</ta>
            <ta e="T325" id="Seg_5610" s="T324">da</ta>
            <ta e="T326" id="Seg_5611" s="T325">tagɨs-A</ta>
            <ta e="T327" id="Seg_5612" s="T326">köt-Ar</ta>
            <ta e="T328" id="Seg_5613" s="T327">innʼe</ta>
            <ta e="T329" id="Seg_5614" s="T328">gɨn-BIT</ta>
            <ta e="T330" id="Seg_5615" s="T329">da</ta>
            <ta e="T331" id="Seg_5616" s="T330">hül-BIT</ta>
            <ta e="T332" id="Seg_5617" s="T331">da</ta>
            <ta e="T333" id="Seg_5618" s="T332">karmaːn-tI-GAr</ta>
            <ta e="T334" id="Seg_5619" s="T333">ugun-An</ta>
            <ta e="T335" id="Seg_5620" s="T334">keːs-BIT</ta>
            <ta e="T336" id="Seg_5621" s="T335">onton</ta>
            <ta e="T337" id="Seg_5622" s="T336">dʼe</ta>
            <ta e="T338" id="Seg_5623" s="T337">kaːm-An</ta>
            <ta e="T339" id="Seg_5624" s="T338">is-BIT</ta>
            <ta e="T340" id="Seg_5625" s="T339">kaːm-An</ta>
            <ta e="T341" id="Seg_5626" s="T340">is-BIT</ta>
            <ta e="T342" id="Seg_5627" s="T341">o</ta>
            <ta e="T343" id="Seg_5628" s="T342">ebekeː</ta>
            <ta e="T344" id="Seg_5629" s="T343">tur-A</ta>
            <ta e="T345" id="Seg_5630" s="T344">ekkireː-AːktAː-BIT</ta>
            <ta e="T346" id="Seg_5631" s="T345">diː</ta>
            <ta e="T347" id="Seg_5632" s="T346">diː</ta>
            <ta e="T348" id="Seg_5633" s="T347">agaj</ta>
            <ta e="T349" id="Seg_5634" s="T348">oj</ta>
            <ta e="T350" id="Seg_5635" s="T349">Čopočuka</ta>
            <ta e="T351" id="Seg_5636" s="T350">togo</ta>
            <ta e="T352" id="Seg_5637" s="T351">kaja</ta>
            <ta e="T353" id="Seg_5638" s="T352">hir-GA</ta>
            <ta e="T354" id="Seg_5639" s="T353">hɨrɨt-A-GIn</ta>
            <ta e="T355" id="Seg_5640" s="T354">bu</ta>
            <ta e="T356" id="Seg_5641" s="T355">di͡e-BIT</ta>
            <ta e="T357" id="Seg_5642" s="T356">balɨk-BI-n</ta>
            <ta e="T358" id="Seg_5643" s="T357">togo</ta>
            <ta e="T359" id="Seg_5644" s="T358">u͡or-BIT-GIt=Ij</ta>
            <ta e="T360" id="Seg_5645" s="T359">össü͡ö</ta>
            <ta e="T361" id="Seg_5646" s="T360">balɨk-BI-n</ta>
            <ta e="T362" id="Seg_5647" s="T361">u͡or-BIT-GIt</ta>
            <ta e="T364" id="Seg_5648" s="T362">hi͡e</ta>
            <ta e="T365" id="Seg_5649" s="T364">min</ta>
            <ta e="T366" id="Seg_5650" s="T365">en-n</ta>
            <ta e="T367" id="Seg_5651" s="T366">u͡op-An</ta>
            <ta e="T368" id="Seg_5652" s="T367">keːs-IAK-m</ta>
            <ta e="T369" id="Seg_5653" s="T368">di͡e-BIT</ta>
            <ta e="T370" id="Seg_5654" s="T369">hi͡e-An</ta>
            <ta e="T371" id="Seg_5655" s="T370">keːs-BIT</ta>
            <ta e="T372" id="Seg_5656" s="T371">is-tI-n</ta>
            <ta e="T373" id="Seg_5657" s="T372">kaj-A</ta>
            <ta e="T374" id="Seg_5658" s="T373">battaː-Ar</ta>
            <ta e="T375" id="Seg_5659" s="T374">da</ta>
            <ta e="T376" id="Seg_5660" s="T375">tagɨs-A</ta>
            <ta e="T377" id="Seg_5661" s="T376">köt-Ar</ta>
            <ta e="T378" id="Seg_5662" s="T377">bu</ta>
            <ta e="T379" id="Seg_5663" s="T378">kihi</ta>
            <ta e="T380" id="Seg_5664" s="T379">innʼe</ta>
            <ta e="T381" id="Seg_5665" s="T380">baran</ta>
            <ta e="T382" id="Seg_5666" s="T381">bahak-tI-nAn</ta>
            <ta e="T383" id="Seg_5667" s="T382">hül-An</ta>
            <ta e="T384" id="Seg_5668" s="T383">baran</ta>
            <ta e="T385" id="Seg_5669" s="T384">dʼe</ta>
            <ta e="T386" id="Seg_5670" s="T385">olor-BIT</ta>
            <ta e="T387" id="Seg_5671" s="T386">adʼas</ta>
            <ta e="T388" id="Seg_5672" s="T387">bu</ta>
            <ta e="T389" id="Seg_5673" s="T388">olor-An</ta>
            <ta e="T390" id="Seg_5674" s="T389">dumaj-LAː-BIT</ta>
            <ta e="T391" id="Seg_5675" s="T390">bu</ta>
            <ta e="T392" id="Seg_5676" s="T391">ɨraːktaːgɨ</ta>
            <ta e="T393" id="Seg_5677" s="T392">kɨːs-tI-n</ta>
            <ta e="T394" id="Seg_5678" s="T393">kördöː-A</ta>
            <ta e="T395" id="Seg_5679" s="T394">bar-IAK-GA</ta>
            <ta e="T396" id="Seg_5680" s="T395">ɨraːktaːgɨ</ta>
            <ta e="T397" id="Seg_5681" s="T396">čogotok</ta>
            <ta e="T398" id="Seg_5682" s="T397">kɨːs-LAːK</ta>
            <ta e="T399" id="Seg_5683" s="T398">di͡e-BIT-LArA</ta>
            <ta e="T400" id="Seg_5684" s="T399">ol-nI</ta>
            <ta e="T401" id="Seg_5685" s="T400">kördöː-A</ta>
            <ta e="T402" id="Seg_5686" s="T401">bar-IAK-GA</ta>
            <ta e="T403" id="Seg_5687" s="T402">naːda</ta>
            <ta e="T404" id="Seg_5688" s="T403">bačča-LAːK</ta>
            <ta e="T405" id="Seg_5689" s="T404">tu͡ok-LAːK</ta>
            <ta e="T406" id="Seg_5690" s="T405">kihi</ta>
            <ta e="T407" id="Seg_5691" s="T406">ka</ta>
            <ta e="T408" id="Seg_5692" s="T407">di͡e-BIT</ta>
            <ta e="T409" id="Seg_5693" s="T408">dʼe</ta>
            <ta e="T410" id="Seg_5694" s="T409">ɨraːktaːgɨ-GA</ta>
            <ta e="T411" id="Seg_5695" s="T410">bar-AːrI</ta>
            <ta e="T412" id="Seg_5696" s="T411">ol</ta>
            <ta e="T413" id="Seg_5697" s="T412">olor-Ar</ta>
            <ta e="T414" id="Seg_5698" s="T413">bu</ta>
            <ta e="T415" id="Seg_5699" s="T414">kihi-ŋ</ta>
            <ta e="T416" id="Seg_5700" s="T415">olor-Ar</ta>
            <ta e="T417" id="Seg_5701" s="T416">dʼe</ta>
            <ta e="T418" id="Seg_5702" s="T417">kaːm-IAK-GA</ta>
            <ta e="T419" id="Seg_5703" s="T418">üčügej</ta>
            <ta e="T420" id="Seg_5704" s="T419">dʼe</ta>
            <ta e="T421" id="Seg_5705" s="T420">kaːm-Ar</ta>
            <ta e="T422" id="Seg_5706" s="T421">bu</ta>
            <ta e="T423" id="Seg_5707" s="T422">kihi</ta>
            <ta e="T424" id="Seg_5708" s="T423">kaːm-An-kaːm-An</ta>
            <ta e="T425" id="Seg_5709" s="T424">biːr</ta>
            <ta e="T426" id="Seg_5710" s="T425">hir-GA</ta>
            <ta e="T427" id="Seg_5711" s="T426">olor-BIT</ta>
            <ta e="T428" id="Seg_5712" s="T427">olor-An</ta>
            <ta e="T429" id="Seg_5713" s="T428">körüleː-BIT-tA</ta>
            <ta e="T430" id="Seg_5714" s="T429">ɨraːktaːgɨ</ta>
            <ta e="T431" id="Seg_5715" s="T430">gi͡en-tA</ta>
            <ta e="T432" id="Seg_5716" s="T431">kirili͡es-tA</ta>
            <ta e="T433" id="Seg_5717" s="T432">köhün-Ar</ta>
            <ta e="T434" id="Seg_5718" s="T433">agaj</ta>
            <ta e="T435" id="Seg_5719" s="T434">tij-An-I-BIn</ta>
            <ta e="T436" id="Seg_5720" s="T435">itinne</ta>
            <ta e="T437" id="Seg_5721" s="T436">bu</ta>
            <ta e="T438" id="Seg_5722" s="T437">kihi</ta>
            <ta e="T439" id="Seg_5723" s="T438">kaːm-An-kaːm-An</ta>
            <ta e="T440" id="Seg_5724" s="T439">kaːm-An-kaːm-An</ta>
            <ta e="T441" id="Seg_5725" s="T440">gu͡orat-GA</ta>
            <ta e="T442" id="Seg_5726" s="T441">kel-BIT</ta>
            <ta e="T443" id="Seg_5727" s="T442">ɨraːktaːgɨ</ta>
            <ta e="T444" id="Seg_5728" s="T443">gi͡en-tI-GAr</ta>
            <ta e="T445" id="Seg_5729" s="T444">kel-BIT-tA</ta>
            <ta e="T446" id="Seg_5730" s="T445">muŋ</ta>
            <ta e="T447" id="Seg_5731" s="T446">tas</ta>
            <ta e="T448" id="Seg_5732" s="T447">dek</ta>
            <ta e="T449" id="Seg_5733" s="T448">kuhagan</ta>
            <ta e="T450" id="Seg_5734" s="T449">bagajɨ</ta>
            <ta e="T451" id="Seg_5735" s="T450">buru͡o-LAːK</ta>
            <ta e="T452" id="Seg_5736" s="T451">golomo</ta>
            <ta e="T453" id="Seg_5737" s="T452">tur-Ar</ta>
            <ta e="T454" id="Seg_5738" s="T453">golomo</ta>
            <ta e="T455" id="Seg_5739" s="T454">tur-Ar</ta>
            <ta e="T456" id="Seg_5740" s="T455">agaj</ta>
            <ta e="T457" id="Seg_5741" s="T456">bu-GA</ta>
            <ta e="T458" id="Seg_5742" s="T457">kiːr-IAK-GA</ta>
            <ta e="T459" id="Seg_5743" s="T458">üčügej</ta>
            <ta e="T460" id="Seg_5744" s="T459">golomo-GA</ta>
            <ta e="T461" id="Seg_5745" s="T460">golomo-GA</ta>
            <ta e="T462" id="Seg_5746" s="T461">köt-An</ta>
            <ta e="T463" id="Seg_5747" s="T462">tüs-BIT-tA</ta>
            <ta e="T464" id="Seg_5748" s="T463">ogonnʼor-LAːK</ta>
            <ta e="T465" id="Seg_5749" s="T464">emeːksin</ta>
            <ta e="T466" id="Seg_5750" s="T465">emeːksin-tA</ta>
            <ta e="T467" id="Seg_5751" s="T466">karak-tA</ta>
            <ta e="T468" id="Seg_5752" s="T467">hu͡ok</ta>
            <ta e="T469" id="Seg_5753" s="T468">ogonnʼor-tA</ta>
            <ta e="T470" id="Seg_5754" s="T469">hi͡ese</ta>
            <ta e="T471" id="Seg_5755" s="T470">hečen</ta>
            <ta e="T472" id="Seg_5756" s="T471">hogus</ta>
            <ta e="T473" id="Seg_5757" s="T472">dʼe</ta>
            <ta e="T474" id="Seg_5758" s="T473">ahaː-A-t-Ar-LAr</ta>
            <ta e="T475" id="Seg_5759" s="T474">bu-nI</ta>
            <ta e="T476" id="Seg_5760" s="T475">ahaː-Ar</ta>
            <ta e="T477" id="Seg_5761" s="T476">bu</ta>
            <ta e="T478" id="Seg_5762" s="T477">ɨ͡aldʼɨt</ta>
            <ta e="T479" id="Seg_5763" s="T478">kaja-kaja</ta>
            <ta e="T480" id="Seg_5764" s="T479">hir-ttAn</ta>
            <ta e="T481" id="Seg_5765" s="T480">kel-A-GIn</ta>
            <ta e="T482" id="Seg_5766" s="T481">di͡e-An</ta>
            <ta e="T483" id="Seg_5767" s="T482">ös</ta>
            <ta e="T484" id="Seg_5768" s="T483">ɨjɨt-Ar-LAr</ta>
            <ta e="T485" id="Seg_5769" s="T484">min</ta>
            <ta e="T487" id="Seg_5770" s="T486">bi͡es</ta>
            <ta e="T488" id="Seg_5771" s="T487">taːs-LAːK</ta>
            <ta e="T489" id="Seg_5772" s="T488">hobo-ilim-LAːK</ta>
            <ta e="T490" id="Seg_5773" s="T489">e-TI-m</ta>
            <ta e="T491" id="Seg_5774" s="T490">ol-tI-BI-ttAn</ta>
            <ta e="T492" id="Seg_5775" s="T491">balɨk-nI</ta>
            <ta e="T493" id="Seg_5776" s="T492">mus-Ar-BI-n</ta>
            <ta e="T494" id="Seg_5777" s="T493">u͡or-TAr-An-BIn</ta>
            <ta e="T495" id="Seg_5778" s="T494">ahɨːlaːk-LAr-nI</ta>
            <ta e="T496" id="Seg_5779" s="T495">ölör-TAː-An</ta>
            <ta e="T497" id="Seg_5780" s="T496">kel-A-BIn</ta>
            <ta e="T498" id="Seg_5781" s="T497">di͡e-BIT</ta>
            <ta e="T499" id="Seg_5782" s="T498">eːk</ta>
            <ta e="T502" id="Seg_5783" s="T501">ahɨːlaːk-LAr-nI</ta>
            <ta e="T503" id="Seg_5784" s="T502">ölör-An</ta>
            <ta e="T504" id="Seg_5785" s="T503">kel-BIT</ta>
            <ta e="T505" id="Seg_5786" s="T504">bu</ta>
            <ta e="T506" id="Seg_5787" s="T505">ogonnʼor-ttAn</ta>
            <ta e="T507" id="Seg_5788" s="T506">ɨjɨt-Ar</ta>
            <ta e="T508" id="Seg_5789" s="T507">ɨraːktaːgɨ</ta>
            <ta e="T509" id="Seg_5790" s="T508">kɨːs-LAːK</ta>
            <ta e="T510" id="Seg_5791" s="T509">du͡o</ta>
            <ta e="T511" id="Seg_5792" s="T510">di͡e-An</ta>
            <ta e="T512" id="Seg_5793" s="T511">[C^1][V^1][C^2]-čogotok</ta>
            <ta e="T513" id="Seg_5794" s="T512">kɨːs-LAːK</ta>
            <ta e="T514" id="Seg_5795" s="T513">kanna</ta>
            <ta e="T515" id="Seg_5796" s="T514">ere</ta>
            <ta e="T517" id="Seg_5797" s="T516">er-GA</ta>
            <ta e="T518" id="Seg_5798" s="T517">bi͡er-Ar-LAr</ta>
            <ta e="T519" id="Seg_5799" s="T518">ühü</ta>
            <ta e="T520" id="Seg_5800" s="T519">di͡e-An</ta>
            <ta e="T521" id="Seg_5801" s="T520">ogonnʼor</ta>
            <ta e="T522" id="Seg_5802" s="T521">en</ta>
            <ta e="T523" id="Seg_5803" s="T522">hu͡orumnʼu-GA</ta>
            <ta e="T524" id="Seg_5804" s="T523">bar-IAK-ŋ</ta>
            <ta e="T525" id="Seg_5805" s="T524">hu͡ok</ta>
            <ta e="T526" id="Seg_5806" s="T525">du͡o</ta>
            <ta e="T527" id="Seg_5807" s="T526">di͡e-BIT</ta>
            <ta e="T528" id="Seg_5808" s="T527">maːjdiːn</ta>
            <ta e="T529" id="Seg_5809" s="T528">ɨ͡aldʼɨt-tA</ta>
            <ta e="T530" id="Seg_5810" s="T529">abaga</ta>
            <ta e="T531" id="Seg_5811" s="T530">ɨjɨt-An</ta>
            <ta e="T532" id="Seg_5812" s="T531">kör-IAK-BIt</ta>
            <ta e="T533" id="Seg_5813" s="T532">di͡e-BIT</ta>
            <ta e="T534" id="Seg_5814" s="T533">biːr</ta>
            <ta e="T535" id="Seg_5815" s="T534">ɨ͡aldʼɨt</ta>
            <ta e="T536" id="Seg_5816" s="T535">kel-BIT</ta>
            <ta e="T537" id="Seg_5817" s="T536">kɨːs-GI-n</ta>
            <ta e="T538" id="Seg_5818" s="T537">kördöː-Ar</ta>
            <ta e="T539" id="Seg_5819" s="T538">di͡e-An-GIn</ta>
            <ta e="T540" id="Seg_5820" s="T539">haŋar</ta>
            <ta e="T541" id="Seg_5821" s="T540">di͡e-BIT</ta>
            <ta e="T542" id="Seg_5822" s="T541">bu</ta>
            <ta e="T543" id="Seg_5823" s="T542">kihi</ta>
            <ta e="T544" id="Seg_5824" s="T543">bar-An</ta>
            <ta e="T545" id="Seg_5825" s="T544">kaːl-BIT</ta>
            <ta e="T546" id="Seg_5826" s="T545">ɨraːktaːgɨ</ta>
            <ta e="T547" id="Seg_5827" s="T546">köt-An</ta>
            <ta e="T548" id="Seg_5828" s="T547">tüs-BIT</ta>
            <ta e="T549" id="Seg_5829" s="T548">kaja</ta>
            <ta e="T550" id="Seg_5830" s="T549">togo</ta>
            <ta e="T551" id="Seg_5831" s="T550">kel-A-GIn</ta>
            <ta e="T552" id="Seg_5832" s="T551">hu͡ok</ta>
            <ta e="T553" id="Seg_5833" s="T552">min</ta>
            <ta e="T554" id="Seg_5834" s="T553">ɨ͡aldʼɨt-LAːK-BIn</ta>
            <ta e="T555" id="Seg_5835" s="T554">kaja-kaja</ta>
            <ta e="T556" id="Seg_5836" s="T555">hir-ttAn</ta>
            <ta e="T557" id="Seg_5837" s="T556">kel-BIT-tA</ta>
            <ta e="T558" id="Seg_5838" s="T557">bu͡olla</ta>
            <ta e="T559" id="Seg_5839" s="T558">bil-BAT-BIn</ta>
            <ta e="T560" id="Seg_5840" s="T559">ol</ta>
            <ta e="T561" id="Seg_5841" s="T560">kihi</ta>
            <ta e="T562" id="Seg_5842" s="T561">kɨːs-GI-n</ta>
            <ta e="T563" id="Seg_5843" s="T562">kördöː-An</ta>
            <ta e="T564" id="Seg_5844" s="T563">er-Ar</ta>
            <ta e="T565" id="Seg_5845" s="T564">di͡e-BIT</ta>
            <ta e="T566" id="Seg_5846" s="T565">innʼe</ta>
            <ta e="T567" id="Seg_5847" s="T566">gɨn-An</ta>
            <ta e="T568" id="Seg_5848" s="T567">ol</ta>
            <ta e="T569" id="Seg_5849" s="T568">kihi</ta>
            <ta e="T570" id="Seg_5850" s="T569">gi͡en-tI-n</ta>
            <ta e="T571" id="Seg_5851" s="T570">hɨraj-tI-n-karak-tI-n</ta>
            <ta e="T572" id="Seg_5852" s="T571">kör-IAK-BI-n</ta>
            <ta e="T573" id="Seg_5853" s="T572">manna</ta>
            <ta e="T574" id="Seg_5854" s="T573">egel-I-ŋ</ta>
            <ta e="T575" id="Seg_5855" s="T574">di͡e-BIT</ta>
            <ta e="T576" id="Seg_5856" s="T575">ɨraːktaːgɨ-tA</ta>
            <ta e="T577" id="Seg_5857" s="T576">bu</ta>
            <ta e="T578" id="Seg_5858" s="T577">ogonnʼor</ta>
            <ta e="T579" id="Seg_5859" s="T578">maːjdiːn</ta>
            <ta e="T580" id="Seg_5860" s="T579">ɨ͡aldʼɨt-tI-n</ta>
            <ta e="T581" id="Seg_5861" s="T580">ilt-BIT</ta>
            <ta e="T582" id="Seg_5862" s="T581">pa</ta>
            <ta e="T583" id="Seg_5863" s="T582">min</ta>
            <ta e="T584" id="Seg_5864" s="T583">itinnik</ta>
            <ta e="T585" id="Seg_5865" s="T584">kihi-GA</ta>
            <ta e="T586" id="Seg_5866" s="T585">bi͡er-BAT-BIn</ta>
            <ta e="T587" id="Seg_5867" s="T586">iliː-tI-n-atak-tI-n</ta>
            <ta e="T588" id="Seg_5868" s="T587">tohut-AlAː-An</ta>
            <ta e="T589" id="Seg_5869" s="T588">baran</ta>
            <ta e="T590" id="Seg_5870" s="T589">kaːjɨː-GA</ta>
            <ta e="T591" id="Seg_5871" s="T590">bɨrak-I-ŋ</ta>
            <ta e="T592" id="Seg_5872" s="T591">di͡e-BIT</ta>
            <ta e="T593" id="Seg_5873" s="T592">eːk</ta>
            <ta e="T594" id="Seg_5874" s="T593">bu</ta>
            <ta e="T595" id="Seg_5875" s="T594">kihi-GI-n</ta>
            <ta e="T596" id="Seg_5876" s="T595">iliː-tI-n-atak-tI-n</ta>
            <ta e="T597" id="Seg_5877" s="T596">tohut-BAtAK-LAr</ta>
            <ta e="T598" id="Seg_5878" s="T597">bɨ͡a-nAn</ta>
            <ta e="T599" id="Seg_5879" s="T598">kelgij-An</ta>
            <ta e="T600" id="Seg_5880" s="T599">bar-An-LAr</ta>
            <ta e="T601" id="Seg_5881" s="T600">kaːjɨː-GA</ta>
            <ta e="T602" id="Seg_5882" s="T601">uk-An</ta>
            <ta e="T603" id="Seg_5883" s="T602">keːs-BIT-LAr</ta>
            <ta e="T604" id="Seg_5884" s="T603">maːjdiːn</ta>
            <ta e="T605" id="Seg_5885" s="T604">kihi-GI-n</ta>
            <ta e="T607" id="Seg_5886" s="T606">bu</ta>
            <ta e="T608" id="Seg_5887" s="T607">kihi</ta>
            <ta e="T609" id="Seg_5888" s="T608">kutujak</ta>
            <ta e="T610" id="Seg_5889" s="T609">bu͡ol-An</ta>
            <ta e="T611" id="Seg_5890" s="T610">tagɨs-A</ta>
            <ta e="T612" id="Seg_5891" s="T611">köt-BIT</ta>
            <ta e="T613" id="Seg_5892" s="T612">tagɨs-A</ta>
            <ta e="T614" id="Seg_5893" s="T613">köt-An</ta>
            <ta e="T615" id="Seg_5894" s="T614">ogonnʼor-tI-GAr</ta>
            <ta e="T616" id="Seg_5895" s="T615">emi͡e</ta>
            <ta e="T617" id="Seg_5896" s="T616">kiːr-BIT</ta>
            <ta e="T618" id="Seg_5897" s="T617">dʼe</ta>
            <ta e="T619" id="Seg_5898" s="T618">bil-BAT-BIn</ta>
            <ta e="T620" id="Seg_5899" s="T619">maːjdiːn-I-TAːgAr</ta>
            <ta e="T621" id="Seg_5900" s="T620">atɨn</ta>
            <ta e="T622" id="Seg_5901" s="T621">kihi</ta>
            <ta e="T623" id="Seg_5902" s="T622">kel-BIT-tA</ta>
            <ta e="T624" id="Seg_5903" s="T623">du͡o</ta>
            <ta e="T625" id="Seg_5904" s="T624">tu͡ok</ta>
            <ta e="T626" id="Seg_5905" s="T625">du͡o</ta>
            <ta e="T627" id="Seg_5906" s="T626">emi͡e</ta>
            <ta e="T628" id="Seg_5907" s="T627">kɨːs-GI-n</ta>
            <ta e="T629" id="Seg_5908" s="T628">kördöː-An</ta>
            <ta e="T630" id="Seg_5909" s="T629">er-Ar</ta>
            <ta e="T631" id="Seg_5910" s="T630">di͡e-An-GIn</ta>
            <ta e="T632" id="Seg_5911" s="T631">bar</ta>
            <ta e="T633" id="Seg_5912" s="T632">dʼe</ta>
            <ta e="T634" id="Seg_5913" s="T633">di͡e-BIT</ta>
            <ta e="T635" id="Seg_5914" s="T634">emi͡e</ta>
            <ta e="T636" id="Seg_5915" s="T635">bar-BIT</ta>
            <ta e="T637" id="Seg_5916" s="T636">ogonnʼor</ta>
            <ta e="T638" id="Seg_5917" s="T637">dʼe</ta>
            <ta e="T639" id="Seg_5918" s="T638">ol</ta>
            <ta e="T640" id="Seg_5919" s="T639">baran</ta>
            <ta e="T641" id="Seg_5920" s="T640">emi͡e</ta>
            <ta e="T642" id="Seg_5921" s="T641">di͡e-BIT</ta>
            <ta e="T643" id="Seg_5922" s="T642">kaja</ta>
            <ta e="T644" id="Seg_5923" s="T643">emi͡e</ta>
            <ta e="T645" id="Seg_5924" s="T644">ɨ͡aldʼɨt</ta>
            <ta e="T646" id="Seg_5925" s="T645">kel-TI-tA</ta>
            <ta e="T647" id="Seg_5926" s="T646">kɨːs-GI-n</ta>
            <ta e="T648" id="Seg_5927" s="T647">kördöː-An</ta>
            <ta e="T649" id="Seg_5928" s="T648">er-Ar</ta>
            <ta e="T650" id="Seg_5929" s="T649">di͡e-BIT</ta>
            <ta e="T651" id="Seg_5930" s="T650">dogor-LAr</ta>
            <ta e="T652" id="Seg_5931" s="T651">maːjdiːn</ta>
            <ta e="T653" id="Seg_5932" s="T652">kaːjɨː-GA</ta>
            <ta e="T654" id="Seg_5933" s="T653">bɨrak-AːččI</ta>
            <ta e="T655" id="Seg_5934" s="T654">kihi-GItI-n</ta>
            <ta e="T656" id="Seg_5935" s="T655">kör-I-ŋ</ta>
            <ta e="T657" id="Seg_5936" s="T656">dʼe</ta>
            <ta e="T658" id="Seg_5937" s="T657">kajdak-kajdak</ta>
            <ta e="T659" id="Seg_5938" s="T658">kihi-nI</ta>
            <ta e="T660" id="Seg_5939" s="T659">dʼabɨlaː-A-BIt</ta>
            <ta e="T661" id="Seg_5940" s="T660">dʼürü</ta>
            <ta e="T662" id="Seg_5941" s="T661">di͡e-BIT</ta>
            <ta e="T663" id="Seg_5942" s="T662">kaːjɨː-GA</ta>
            <ta e="T664" id="Seg_5943" s="T663">bɨrak-BIT</ta>
            <ta e="T665" id="Seg_5944" s="T664">kihi-tA</ta>
            <ta e="T666" id="Seg_5945" s="T665">hu͡ol-tA</ta>
            <ta e="T667" id="Seg_5946" s="T666">ere</ta>
            <ta e="T668" id="Seg_5947" s="T667">čonoj-A</ta>
            <ta e="T669" id="Seg_5948" s="T668">hɨt-Ar</ta>
            <ta e="T670" id="Seg_5949" s="T669">kelgij-BIT</ta>
            <ta e="T671" id="Seg_5950" s="T670">bɨ͡a-tA</ta>
            <ta e="T672" id="Seg_5951" s="T671">bɨ͡a-tI-nAn</ta>
            <ta e="T673" id="Seg_5952" s="T672">hɨt-Ar</ta>
            <ta e="T674" id="Seg_5953" s="T673">dʼe</ta>
            <ta e="T675" id="Seg_5954" s="T674">kaja</ta>
            <ta e="T676" id="Seg_5955" s="T675">onnuk-onnuk</ta>
            <ta e="T677" id="Seg_5956" s="T676">kaja</ta>
            <ta e="T678" id="Seg_5957" s="T677">o</ta>
            <ta e="T679" id="Seg_5958" s="T678">dʼe</ta>
            <ta e="T680" id="Seg_5959" s="T679">ile</ta>
            <ta e="T681" id="Seg_5960" s="T680">da</ta>
            <ta e="T682" id="Seg_5961" s="T681">üčügej</ta>
            <ta e="T683" id="Seg_5962" s="T682">kihi-nI</ta>
            <ta e="T684" id="Seg_5963" s="T683">gɨn-TI-m</ta>
            <ta e="T685" id="Seg_5964" s="T684">eː</ta>
            <ta e="T686" id="Seg_5965" s="T685">badaga</ta>
            <ta e="T687" id="Seg_5966" s="T686">kɨːs-BI-n</ta>
            <ta e="T688" id="Seg_5967" s="T687">bi͡er-A-BIn</ta>
            <ta e="T689" id="Seg_5968" s="T688">di͡e-IAK-m</ta>
            <ta e="T690" id="Seg_5969" s="T689">bu͡olla</ta>
            <ta e="T691" id="Seg_5970" s="T690">bu</ta>
            <ta e="T692" id="Seg_5971" s="T691">kihi</ta>
            <ta e="T693" id="Seg_5972" s="T692">baːgɨ-LAːK</ta>
            <ta e="T694" id="Seg_5973" s="T693">ahɨːlaːk</ta>
            <ta e="T695" id="Seg_5974" s="T694">ölör-BIT-tI-n</ta>
            <ta e="T696" id="Seg_5975" s="T695">ɨraːktaːgɨ</ta>
            <ta e="T697" id="Seg_5976" s="T696">töhök-tI-GAr</ta>
            <ta e="T698" id="Seg_5977" s="T697">tok-A</ta>
            <ta e="T699" id="Seg_5978" s="T698">bɨrak-BIT</ta>
            <ta e="T700" id="Seg_5979" s="T699">innʼe</ta>
            <ta e="T701" id="Seg_5980" s="T700">gɨn-An</ta>
            <ta e="T702" id="Seg_5981" s="T701">kös-tI-n</ta>
            <ta e="T703" id="Seg_5982" s="T702">kelin-tI-GAr</ta>
            <ta e="T704" id="Seg_5983" s="T703">čɨːčaːk</ta>
            <ta e="T705" id="Seg_5984" s="T704">ɨllaː-Ar</ta>
            <ta e="T706" id="Seg_5985" s="T705">godolutan</ta>
            <ta e="T707" id="Seg_5986" s="T706">araːr-An</ta>
            <ta e="T708" id="Seg_5987" s="T707">ilt-BIT</ta>
            <ta e="T709" id="Seg_5988" s="T708">bu</ta>
            <ta e="T710" id="Seg_5989" s="T709">kihi</ta>
            <ta e="T711" id="Seg_5990" s="T710">eŋin-eŋin</ta>
            <ta e="T712" id="Seg_5991" s="T711">haldaːt-LAːK-tu͡ok-LAːK</ta>
            <ta e="T713" id="Seg_5992" s="T712">bu</ta>
            <ta e="T714" id="Seg_5993" s="T713">kihi</ta>
            <ta e="T715" id="Seg_5994" s="T714">dʼe</ta>
            <ta e="T716" id="Seg_5995" s="T715">bar-Ar</ta>
            <ta e="T717" id="Seg_5996" s="T716">dʼi͡e-tI-GAr</ta>
            <ta e="T718" id="Seg_5997" s="T717">kɨːs-tI-n</ta>
            <ta e="T719" id="Seg_5998" s="T718">ɨl-An</ta>
            <ta e="T720" id="Seg_5999" s="T719">baran</ta>
            <ta e="T721" id="Seg_6000" s="T720">dʼe</ta>
            <ta e="T722" id="Seg_6001" s="T721">kös-Ar</ta>
            <ta e="T723" id="Seg_6002" s="T722">kös-An</ta>
            <ta e="T724" id="Seg_6003" s="T723">iti</ta>
            <ta e="T725" id="Seg_6004" s="T724">kel-An-LAr</ta>
            <ta e="T726" id="Seg_6005" s="T725">iti</ta>
            <ta e="T727" id="Seg_6006" s="T726">dʼe</ta>
            <ta e="T728" id="Seg_6007" s="T727">iti</ta>
            <ta e="T729" id="Seg_6008" s="T728">baːj-An-tot-An</ta>
            <ta e="T730" id="Seg_6009" s="T729">olor-BIT-tA</ta>
            <ta e="T731" id="Seg_6010" s="T730">dʼe</ta>
            <ta e="T732" id="Seg_6011" s="T731">iti</ta>
            <ta e="T733" id="Seg_6012" s="T732">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_6013" s="T1">Chopochuka.[NOM]</ta>
            <ta e="T3" id="Seg_6014" s="T2">live-PST2.[3SG]</ta>
            <ta e="T4" id="Seg_6015" s="T3">it.is.said</ta>
            <ta e="T5" id="Seg_6016" s="T4">five</ta>
            <ta e="T6" id="Seg_6017" s="T5">wood</ta>
            <ta e="T7" id="Seg_6018" s="T6">place-PTCP.PST</ta>
            <ta e="T8" id="Seg_6019" s="T7">four_cornered.poletent.[NOM]</ta>
            <ta e="T9" id="Seg_6020" s="T8">house-PROPR.[NOM]</ta>
            <ta e="T10" id="Seg_6021" s="T9">five</ta>
            <ta e="T11" id="Seg_6022" s="T10">stone-PROPR</ta>
            <ta e="T12" id="Seg_6023" s="T11">crucian.carp-net-PROPR.[NOM]</ta>
            <ta e="T13" id="Seg_6024" s="T12">day.[NOM]</ta>
            <ta e="T14" id="Seg_6025" s="T13">every</ta>
            <ta e="T15" id="Seg_6026" s="T14">net-VBZ-CVB.SIM</ta>
            <ta e="T16" id="Seg_6027" s="T15">go.in-PRS.[3SG]</ta>
            <ta e="T17" id="Seg_6028" s="T16">this</ta>
            <ta e="T18" id="Seg_6029" s="T17">human.being.[NOM]</ta>
            <ta e="T19" id="Seg_6030" s="T18">fish-ACC</ta>
            <ta e="T20" id="Seg_6031" s="T19">tower-ADVZ</ta>
            <ta e="T21" id="Seg_6032" s="T20">gather-PST2.[3SG]</ta>
            <ta e="T22" id="Seg_6033" s="T21">net-3SG-ABL</ta>
            <ta e="T23" id="Seg_6034" s="T22">so</ta>
            <ta e="T24" id="Seg_6035" s="T23">make-CVB.SEQ</ta>
            <ta e="T25" id="Seg_6036" s="T24">once</ta>
            <ta e="T26" id="Seg_6037" s="T25">go-PST2-3SG</ta>
            <ta e="T27" id="Seg_6038" s="T26">polar.fox-PL.[NOM]</ta>
            <ta e="T28" id="Seg_6039" s="T27">what-PL.[NOM]</ta>
            <ta e="T29" id="Seg_6040" s="T28">eat-PST2-3PL</ta>
            <ta e="T30" id="Seg_6041" s="T29">suddenly</ta>
            <ta e="T31" id="Seg_6042" s="T30">fish-3SG-ACC</ta>
            <ta e="T32" id="Seg_6043" s="T31">this-ACC</ta>
            <ta e="T33" id="Seg_6044" s="T32">follow-PTCP.FUT-DAT/LOC</ta>
            <ta e="T34" id="Seg_6045" s="T33">trace-3SG-ACC</ta>
            <ta e="T35" id="Seg_6046" s="T34">follow-PTCP.FUT-DAT/LOC</ta>
            <ta e="T36" id="Seg_6047" s="T35">fish-3SG-ACC</ta>
            <ta e="T37" id="Seg_6048" s="T36">house-3SG-DAT/LOC</ta>
            <ta e="T38" id="Seg_6049" s="T37">carry-CVB.SEQ</ta>
            <ta e="T39" id="Seg_6050" s="T38">after</ta>
            <ta e="T40" id="Seg_6051" s="T39">this</ta>
            <ta e="T41" id="Seg_6052" s="T40">human.being.[NOM]</ta>
            <ta e="T42" id="Seg_6053" s="T41">polar.fox.[NOM]</ta>
            <ta e="T43" id="Seg_6054" s="T42">trace-3SG-ACC</ta>
            <ta e="T44" id="Seg_6055" s="T43">follow-PST2.[3SG]</ta>
            <ta e="T45" id="Seg_6056" s="T44">polar.fox.[NOM]</ta>
            <ta e="T46" id="Seg_6057" s="T45">trace-3SG-ACC</ta>
            <ta e="T47" id="Seg_6058" s="T46">follow-CVB.SEQ</ta>
            <ta e="T48" id="Seg_6059" s="T47">go-TEMP-3SG</ta>
            <ta e="T49" id="Seg_6060" s="T48">well</ta>
            <ta e="T50" id="Seg_6061" s="T49">polar.fox.[NOM]</ta>
            <ta e="T51" id="Seg_6062" s="T50">what-ACC</ta>
            <ta e="T52" id="Seg_6063" s="T51">and</ta>
            <ta e="T53" id="Seg_6064" s="T52">one</ta>
            <ta e="T54" id="Seg_6065" s="T53">bush.[NOM]</ta>
            <ta e="T55" id="Seg_6066" s="T54">inside-3SG-ABL</ta>
            <ta e="T56" id="Seg_6067" s="T55">stand.up-CVB.SIM</ta>
            <ta e="T57" id="Seg_6068" s="T56">jump-PST2.[3SG]</ta>
            <ta e="T58" id="Seg_6069" s="T57">oh.dear</ta>
            <ta e="T59" id="Seg_6070" s="T58">Chopochuka.[NOM]</ta>
            <ta e="T60" id="Seg_6071" s="T59">human.being-ACC</ta>
            <ta e="T61" id="Seg_6072" s="T60">startle-CAUS-PST1-2SG</ta>
            <ta e="T62" id="Seg_6073" s="T61">well</ta>
            <ta e="T63" id="Seg_6074" s="T62">how</ta>
            <ta e="T64" id="Seg_6075" s="T63">come-PRS-2SG</ta>
            <ta e="T65" id="Seg_6076" s="T64">well</ta>
            <ta e="T66" id="Seg_6077" s="T65">fish-1SG-ACC</ta>
            <ta e="T67" id="Seg_6078" s="T66">well</ta>
            <ta e="T68" id="Seg_6079" s="T67">why</ta>
            <ta e="T69" id="Seg_6080" s="T68">steal-PST2-2PL=Q</ta>
            <ta e="T70" id="Seg_6081" s="T69">2PL.[NOM]</ta>
            <ta e="T71" id="Seg_6082" s="T70">what</ta>
            <ta e="T72" id="Seg_6083" s="T71">1SG.[NOM]</ta>
            <ta e="T73" id="Seg_6084" s="T72">steal-PST2-EP-1SG</ta>
            <ta e="T74" id="Seg_6085" s="T73">Q</ta>
            <ta e="T75" id="Seg_6086" s="T74">say-CVB.SIM-say-CVB.SIM</ta>
            <ta e="T76" id="Seg_6087" s="T75">make-PST2.[3SG]</ta>
            <ta e="T77" id="Seg_6088" s="T76">this-3SG-2SG.[NOM]</ta>
            <ta e="T78" id="Seg_6089" s="T77">mouse.[NOM]</ta>
            <ta e="T79" id="Seg_6090" s="T78">become-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_6091" s="T79">mouse.[NOM]</ta>
            <ta e="T81" id="Seg_6092" s="T80">become-CVB.SEQ</ta>
            <ta e="T82" id="Seg_6093" s="T81">well</ta>
            <ta e="T83" id="Seg_6094" s="T82">old.man-EP-2SG.[NOM]</ta>
            <ta e="T84" id="Seg_6095" s="T83">well</ta>
            <ta e="T85" id="Seg_6096" s="T84">go-PRS.[3SG]</ta>
            <ta e="T86" id="Seg_6097" s="T85">that-ACC</ta>
            <ta e="T87" id="Seg_6098" s="T86">put.into.the.mouth-CVB.SEQ</ta>
            <ta e="T88" id="Seg_6099" s="T87">throw-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_6100" s="T88">polar.fox-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_6101" s="T89">that-2SG.[NOM]</ta>
            <ta e="T91" id="Seg_6102" s="T90">belly-3SG-ACC</ta>
            <ta e="T92" id="Seg_6103" s="T91">well</ta>
            <ta e="T95" id="Seg_6104" s="T94">pull.down-CVB.SEQ</ta>
            <ta e="T96" id="Seg_6105" s="T95">cut-CVB.SEQ</ta>
            <ta e="T97" id="Seg_6106" s="T96">go.out-CVB.SIM</ta>
            <ta e="T100" id="Seg_6107" s="T99">go.out-CVB.SIM</ta>
            <ta e="T101" id="Seg_6108" s="T100">run-PST2.[3SG]</ta>
            <ta e="T102" id="Seg_6109" s="T101">so</ta>
            <ta e="T103" id="Seg_6110" s="T102">after</ta>
            <ta e="T104" id="Seg_6111" s="T103">that-ACC</ta>
            <ta e="T105" id="Seg_6112" s="T104">skin-PRS.[3SG]</ta>
            <ta e="T106" id="Seg_6113" s="T105">and</ta>
            <ta e="T107" id="Seg_6114" s="T106">pocket-3SG-DAT/LOC</ta>
            <ta e="T108" id="Seg_6115" s="T107">put.in-PRS.[3SG]</ta>
            <ta e="T109" id="Seg_6116" s="T108">and</ta>
            <ta e="T110" id="Seg_6117" s="T109">walk-CVB.SEQ</ta>
            <ta e="T111" id="Seg_6118" s="T110">go-PST2.[3SG]</ta>
            <ta e="T112" id="Seg_6119" s="T111">again</ta>
            <ta e="T113" id="Seg_6120" s="T112">front.[NOM]</ta>
            <ta e="T114" id="Seg_6121" s="T113">to</ta>
            <ta e="T115" id="Seg_6122" s="T114">walk-CVB.SEQ</ta>
            <ta e="T116" id="Seg_6123" s="T115">go-CVB.SEQ</ta>
            <ta e="T117" id="Seg_6124" s="T116">hare.[NOM]</ta>
            <ta e="T118" id="Seg_6125" s="T117">place-PST2.[3SG]</ta>
            <ta e="T119" id="Seg_6126" s="T118">oh.dear</ta>
            <ta e="T120" id="Seg_6127" s="T119">Chopochuka.[NOM]</ta>
            <ta e="T121" id="Seg_6128" s="T120">what.[NOM]</ta>
            <ta e="T122" id="Seg_6129" s="T121">what.kind.of</ta>
            <ta e="T123" id="Seg_6130" s="T122">place-ABL</ta>
            <ta e="T124" id="Seg_6131" s="T123">come-PRS-2SG</ta>
            <ta e="T125" id="Seg_6132" s="T124">human.being-ACC</ta>
            <ta e="T126" id="Seg_6133" s="T125">startle-CAUS-EMOT-PST1-2SG</ta>
            <ta e="T127" id="Seg_6134" s="T126">say-PST2.[3SG]</ta>
            <ta e="T128" id="Seg_6135" s="T127">well</ta>
            <ta e="T129" id="Seg_6136" s="T128">fish-1SG-ACC</ta>
            <ta e="T130" id="Seg_6137" s="T129">why</ta>
            <ta e="T131" id="Seg_6138" s="T130">whole-3SG-ACC</ta>
            <ta e="T132" id="Seg_6139" s="T131">eat-PST2-2PL=Q</ta>
            <ta e="T133" id="Seg_6140" s="T132">fish-1SG-ACC</ta>
            <ta e="T134" id="Seg_6141" s="T133">follow-CVB.SIM</ta>
            <ta e="T135" id="Seg_6142" s="T134">go-PRS-1SG</ta>
            <ta e="T138" id="Seg_6143" s="T136">that.[NOM]</ta>
            <ta e="T139" id="Seg_6144" s="T138">also</ta>
            <ta e="T141" id="Seg_6145" s="T139">well</ta>
            <ta e="T142" id="Seg_6146" s="T141">well</ta>
            <ta e="T143" id="Seg_6147" s="T142">well</ta>
            <ta e="T144" id="Seg_6148" s="T143">fight-PRS-3PL</ta>
            <ta e="T145" id="Seg_6149" s="T144">eh</ta>
            <ta e="T146" id="Seg_6150" s="T145">hare-ACC</ta>
            <ta e="T147" id="Seg_6151" s="T146">with</ta>
            <ta e="T148" id="Seg_6152" s="T147">fight-CVB.SEQ-3PL</ta>
            <ta e="T149" id="Seg_6153" s="T148">hare-ACC</ta>
            <ta e="T150" id="Seg_6154" s="T149">what-3SG-ACC</ta>
            <ta e="T151" id="Seg_6155" s="T150">human.being-VBZ-FUT.[3SG]=Q</ta>
            <ta e="T152" id="Seg_6156" s="T151">that</ta>
            <ta e="T153" id="Seg_6157" s="T152">human.being.[NOM]</ta>
            <ta e="T154" id="Seg_6158" s="T153">that-ACC</ta>
            <ta e="T155" id="Seg_6159" s="T154">kill-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_6160" s="T155">and</ta>
            <ta e="T157" id="Seg_6161" s="T156">again</ta>
            <ta e="T159" id="Seg_6162" s="T158">pocket-3SG-DAT/LOC</ta>
            <ta e="T160" id="Seg_6163" s="T159">put.in-CVB.SEQ</ta>
            <ta e="T161" id="Seg_6164" s="T160">throw-PST2.[3SG]</ta>
            <ta e="T162" id="Seg_6165" s="T161">skin-EP-MED-CVB.SEQ</ta>
            <ta e="T163" id="Seg_6166" s="T162">again</ta>
            <ta e="T164" id="Seg_6167" s="T163">walk-CVB.SEQ</ta>
            <ta e="T165" id="Seg_6168" s="T164">go-PST2.[3SG]</ta>
            <ta e="T166" id="Seg_6169" s="T165">walk-CVB.SEQ-walk-CVB.SEQ</ta>
            <ta e="T167" id="Seg_6170" s="T166">go-TEMP-3SG</ta>
            <ta e="T168" id="Seg_6171" s="T167">fox.[NOM]</ta>
            <ta e="T169" id="Seg_6172" s="T168">stand.up-CVB.SIM</ta>
            <ta e="T170" id="Seg_6173" s="T169">jump-PST2.[3SG]</ta>
            <ta e="T171" id="Seg_6174" s="T170">oh</ta>
            <ta e="T172" id="Seg_6175" s="T171">Chopochuka.[NOM]</ta>
            <ta e="T173" id="Seg_6176" s="T172">human.being-ACC</ta>
            <ta e="T174" id="Seg_6177" s="T173">startle-CAUS-PST1-2SG</ta>
            <ta e="T175" id="Seg_6178" s="T174">what.kind.of</ta>
            <ta e="T176" id="Seg_6179" s="T175">what.kind.of</ta>
            <ta e="T177" id="Seg_6180" s="T176">place-ABL</ta>
            <ta e="T178" id="Seg_6181" s="T177">come-PRS-2SG</ta>
            <ta e="T179" id="Seg_6182" s="T178">this</ta>
            <ta e="T180" id="Seg_6183" s="T179">say-PST2.[3SG]</ta>
            <ta e="T181" id="Seg_6184" s="T180">well</ta>
            <ta e="T182" id="Seg_6185" s="T181">say-PST2.[3SG]</ta>
            <ta e="T183" id="Seg_6186" s="T182">well</ta>
            <ta e="T184" id="Seg_6187" s="T183">fish-1SG-ACC</ta>
            <ta e="T185" id="Seg_6188" s="T184">whole-3SG-ACC</ta>
            <ta e="T186" id="Seg_6189" s="T185">eat-PST2-2PL</ta>
            <ta e="T187" id="Seg_6190" s="T186">that.[NOM]</ta>
            <ta e="T188" id="Seg_6191" s="T187">because.of</ta>
            <ta e="T189" id="Seg_6192" s="T188">follow-CVB.SEQ</ta>
            <ta e="T190" id="Seg_6193" s="T189">come-PRS-1SG</ta>
            <ta e="T191" id="Seg_6194" s="T190">pa</ta>
            <ta e="T192" id="Seg_6195" s="T191">2SG.[NOM]</ta>
            <ta e="T193" id="Seg_6196" s="T192">fish-2SG-DAT/LOC</ta>
            <ta e="T194" id="Seg_6197" s="T193">1PL.[NOM]</ta>
            <ta e="T195" id="Seg_6198" s="T194">come.closer-PST2.NEG-1PL</ta>
            <ta e="T196" id="Seg_6199" s="T195">EMPH</ta>
            <ta e="T197" id="Seg_6200" s="T196">still</ta>
            <ta e="T198" id="Seg_6201" s="T197">instead</ta>
            <ta e="T199" id="Seg_6202" s="T198">1SG.[NOM]</ta>
            <ta e="T200" id="Seg_6203" s="T199">2SG-ACC</ta>
            <ta e="T201" id="Seg_6204" s="T200">rip-CVB.SIM</ta>
            <ta e="T202" id="Seg_6205" s="T201">touch-CVB.SEQ</ta>
            <ta e="T203" id="Seg_6206" s="T202">eat-CVB.SEQ</ta>
            <ta e="T204" id="Seg_6207" s="T203">throw-FUT-1SG</ta>
            <ta e="T205" id="Seg_6208" s="T204">say-PST2.[3SG]</ta>
            <ta e="T206" id="Seg_6209" s="T205">well</ta>
            <ta e="T207" id="Seg_6210" s="T206">fight-PRS-3PL</ta>
            <ta e="T208" id="Seg_6211" s="T207">this</ta>
            <ta e="T209" id="Seg_6212" s="T208">human.being-PL.[NOM]</ta>
            <ta e="T210" id="Seg_6213" s="T209">Chopochuka.[NOM]</ta>
            <ta e="T211" id="Seg_6214" s="T210">what.kind.of-3SG-3SG-ACC</ta>
            <ta e="T212" id="Seg_6215" s="T211">human.being-VBZ-FUT.[3SG]=Q</ta>
            <ta e="T213" id="Seg_6216" s="T212">turn.inside.out-CVB.SIM</ta>
            <ta e="T214" id="Seg_6217" s="T213">pull.down-PST2.[3SG]</ta>
            <ta e="T215" id="Seg_6218" s="T214">and</ta>
            <ta e="T216" id="Seg_6219" s="T215">kill-CVB.SEQ</ta>
            <ta e="T217" id="Seg_6220" s="T216">throw-PST2.[3SG]</ta>
            <ta e="T218" id="Seg_6221" s="T217">that-ACC</ta>
            <ta e="T219" id="Seg_6222" s="T218">skin-PST2.[3SG]</ta>
            <ta e="T220" id="Seg_6223" s="T219">and</ta>
            <ta e="T221" id="Seg_6224" s="T220">pocket-3SG-DAT/LOC</ta>
            <ta e="T222" id="Seg_6225" s="T221">pocket.[NOM]</ta>
            <ta e="T223" id="Seg_6226" s="T222">EMPH</ta>
            <ta e="T224" id="Seg_6227" s="T223">big</ta>
            <ta e="T225" id="Seg_6228" s="T224">wonder-PST1-1SG</ta>
            <ta e="T226" id="Seg_6229" s="T225">fox-3SG-ACC</ta>
            <ta e="T227" id="Seg_6230" s="T226">put.in-CVB.SEQ</ta>
            <ta e="T228" id="Seg_6231" s="T227">throw-PST2.[3SG]</ta>
            <ta e="T229" id="Seg_6232" s="T228">pocket-3SG-DAT/LOC</ta>
            <ta e="T230" id="Seg_6233" s="T229">then</ta>
            <ta e="T231" id="Seg_6234" s="T230">walk-CVB.SEQ</ta>
            <ta e="T232" id="Seg_6235" s="T231">go-PST2.[3SG]</ta>
            <ta e="T233" id="Seg_6236" s="T232">walk-CVB.SEQ</ta>
            <ta e="T234" id="Seg_6237" s="T233">go-CVB.SIM</ta>
            <ta e="T235" id="Seg_6238" s="T234">go-TEMP-3SG</ta>
            <ta e="T236" id="Seg_6239" s="T235">wolverine.[NOM]</ta>
            <ta e="T237" id="Seg_6240" s="T236">stand.up-CVB.SIM</ta>
            <ta e="T238" id="Seg_6241" s="T237">jump-PST2.[3SG]</ta>
            <ta e="T239" id="Seg_6242" s="T238">well</ta>
            <ta e="T240" id="Seg_6243" s="T239">Chopochuka.[NOM]</ta>
            <ta e="T241" id="Seg_6244" s="T240">human.being-ACC</ta>
            <ta e="T242" id="Seg_6245" s="T241">startle-CAUS-PST1-2SG</ta>
            <ta e="T243" id="Seg_6246" s="T242">just</ta>
            <ta e="T244" id="Seg_6247" s="T243">say-PST2.[3SG]</ta>
            <ta e="T245" id="Seg_6248" s="T244">why</ta>
            <ta e="T246" id="Seg_6249" s="T245">go-PRS-2SG</ta>
            <ta e="T247" id="Seg_6250" s="T246">this</ta>
            <ta e="T248" id="Seg_6251" s="T247">say-CVB.SEQ</ta>
            <ta e="T249" id="Seg_6252" s="T248">country-DAT/LOC</ta>
            <ta e="T250" id="Seg_6253" s="T249">pa</ta>
            <ta e="T251" id="Seg_6254" s="T250">well</ta>
            <ta e="T252" id="Seg_6255" s="T251">fish-1SG-ACC</ta>
            <ta e="T253" id="Seg_6256" s="T252">why</ta>
            <ta e="T254" id="Seg_6257" s="T253">whole-3SG-ACC</ta>
            <ta e="T255" id="Seg_6258" s="T254">eat-PST2-2PL=Q</ta>
            <ta e="T256" id="Seg_6259" s="T255">that-ACC</ta>
            <ta e="T257" id="Seg_6260" s="T256">follow-CVB.SIM</ta>
            <ta e="T258" id="Seg_6261" s="T257">go-PRS-1SG</ta>
            <ta e="T259" id="Seg_6262" s="T258">say-PST2.[3SG]</ta>
            <ta e="T260" id="Seg_6263" s="T259">still</ta>
            <ta e="T261" id="Seg_6264" s="T260">eat-PST2-2PL=Q</ta>
            <ta e="T262" id="Seg_6265" s="T261">catch-CVB.SEQ</ta>
            <ta e="T263" id="Seg_6266" s="T262">take-PRS.[3SG]</ta>
            <ta e="T264" id="Seg_6267" s="T263">and</ta>
            <ta e="T265" id="Seg_6268" s="T264">that-ACC</ta>
            <ta e="T266" id="Seg_6269" s="T265">well</ta>
            <ta e="T267" id="Seg_6270" s="T266">fight-PRS.[3SG]</ta>
            <ta e="T268" id="Seg_6271" s="T267">fight-CVB.SEQ</ta>
            <ta e="T269" id="Seg_6272" s="T268">Chopochuka.[NOM]</ta>
            <ta e="T270" id="Seg_6273" s="T269">what-3SG-ACC</ta>
            <ta e="T271" id="Seg_6274" s="T270">human.being-VBZ-FUT.[3SG]=Q</ta>
            <ta e="T272" id="Seg_6275" s="T271">human.being-VBZ-PST2.NEG.[3SG]</ta>
            <ta e="T273" id="Seg_6276" s="T272">kill-PST2.[3SG]</ta>
            <ta e="T274" id="Seg_6277" s="T273">and</ta>
            <ta e="T275" id="Seg_6278" s="T274">also</ta>
            <ta e="T276" id="Seg_6279" s="T275">skin-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_6280" s="T276">and</ta>
            <ta e="T278" id="Seg_6281" s="T277">pocket-3SG-DAT/LOC</ta>
            <ta e="T279" id="Seg_6282" s="T278">put.in-CVB.SEQ</ta>
            <ta e="T280" id="Seg_6283" s="T279">throw-PST2.[3SG]</ta>
            <ta e="T281" id="Seg_6284" s="T280">again</ta>
            <ta e="T282" id="Seg_6285" s="T281">walk-CVB.SEQ</ta>
            <ta e="T283" id="Seg_6286" s="T282">go-PST2.[3SG]</ta>
            <ta e="T284" id="Seg_6287" s="T283">walk-CVB.SEQ-walk-CVB.SEQ</ta>
            <ta e="T285" id="Seg_6288" s="T284">wolf-ACC</ta>
            <ta e="T286" id="Seg_6289" s="T285">meet-EP-PST2.[3SG]</ta>
            <ta e="T287" id="Seg_6290" s="T286">wolf-3SG.[NOM]</ta>
            <ta e="T288" id="Seg_6291" s="T287">stand.up-CVB.SIM</ta>
            <ta e="T289" id="Seg_6292" s="T288">jump-PST2.[3SG]</ta>
            <ta e="T290" id="Seg_6293" s="T289">oh</ta>
            <ta e="T291" id="Seg_6294" s="T290">Chopochuka.[NOM]</ta>
            <ta e="T292" id="Seg_6295" s="T291">human.being-ACC</ta>
            <ta e="T293" id="Seg_6296" s="T292">startle-CAUS-PST1-2SG</ta>
            <ta e="T294" id="Seg_6297" s="T293">well</ta>
            <ta e="T295" id="Seg_6298" s="T294">whereto</ta>
            <ta e="T296" id="Seg_6299" s="T295">go-PRS-2SG</ta>
            <ta e="T297" id="Seg_6300" s="T296">say-PST2.[3SG]</ta>
            <ta e="T298" id="Seg_6301" s="T297">well</ta>
            <ta e="T299" id="Seg_6302" s="T298">fish-1SG-ACC</ta>
            <ta e="T300" id="Seg_6303" s="T299">why</ta>
            <ta e="T301" id="Seg_6304" s="T300">eat-PST2-2PL=Q</ta>
            <ta e="T302" id="Seg_6305" s="T301">that-3SG-1SG-ACC</ta>
            <ta e="T303" id="Seg_6306" s="T302">follow-PRS-1SG</ta>
            <ta e="T304" id="Seg_6307" s="T303">pa</ta>
            <ta e="T305" id="Seg_6308" s="T304">2SG.[NOM]</ta>
            <ta e="T306" id="Seg_6309" s="T305">fish-2SG-DAT/LOC</ta>
            <ta e="T307" id="Seg_6310" s="T306">come.closer-PST2.NEG-1PL</ta>
            <ta e="T308" id="Seg_6311" s="T307">EMPH</ta>
            <ta e="T309" id="Seg_6312" s="T308">instead</ta>
            <ta e="T310" id="Seg_6313" s="T309">1SG.[NOM]</ta>
            <ta e="T311" id="Seg_6314" s="T310">2SG-ACC</ta>
            <ta e="T312" id="Seg_6315" s="T311">eat-CVB.SEQ</ta>
            <ta e="T313" id="Seg_6316" s="T312">throw-FUT-1SG</ta>
            <ta e="T314" id="Seg_6317" s="T313">say-PST2.[3SG]</ta>
            <ta e="T315" id="Seg_6318" s="T314">that-ACC</ta>
            <ta e="T316" id="Seg_6319" s="T315">put.into.the.mouth-CVB.SEQ</ta>
            <ta e="T317" id="Seg_6320" s="T316">throw-PST2.[3SG]</ta>
            <ta e="T318" id="Seg_6321" s="T317">devour-CVB.SEQ</ta>
            <ta e="T319" id="Seg_6322" s="T318">throw-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_6323" s="T319">Chopochuka-ACC</ta>
            <ta e="T321" id="Seg_6324" s="T320">Chopochuka.[NOM]</ta>
            <ta e="T322" id="Seg_6325" s="T321">belly-3SG-ACC</ta>
            <ta e="T323" id="Seg_6326" s="T322">split-CVB.SIM</ta>
            <ta e="T324" id="Seg_6327" s="T323">pull.down-PRS.[3SG]</ta>
            <ta e="T325" id="Seg_6328" s="T324">and</ta>
            <ta e="T326" id="Seg_6329" s="T325">go.out-CVB.SIM</ta>
            <ta e="T327" id="Seg_6330" s="T326">run-PRS.[3SG]</ta>
            <ta e="T328" id="Seg_6331" s="T327">so</ta>
            <ta e="T329" id="Seg_6332" s="T328">make-PST2.[3SG]</ta>
            <ta e="T330" id="Seg_6333" s="T329">and</ta>
            <ta e="T331" id="Seg_6334" s="T330">skin-PST2.[3SG]</ta>
            <ta e="T332" id="Seg_6335" s="T331">and</ta>
            <ta e="T333" id="Seg_6336" s="T332">pocket-3SG-DAT/LOC</ta>
            <ta e="T334" id="Seg_6337" s="T333">put.in-CVB.SEQ</ta>
            <ta e="T335" id="Seg_6338" s="T334">throw-PST2.[3SG]</ta>
            <ta e="T336" id="Seg_6339" s="T335">then</ta>
            <ta e="T337" id="Seg_6340" s="T336">well</ta>
            <ta e="T338" id="Seg_6341" s="T337">walk-CVB.SEQ</ta>
            <ta e="T339" id="Seg_6342" s="T338">go-PST2.[3SG]</ta>
            <ta e="T340" id="Seg_6343" s="T339">walk-CVB.SEQ</ta>
            <ta e="T341" id="Seg_6344" s="T340">go-PST2.[3SG]</ta>
            <ta e="T342" id="Seg_6345" s="T341">oh</ta>
            <ta e="T343" id="Seg_6346" s="T342">bear.[NOM]</ta>
            <ta e="T344" id="Seg_6347" s="T343">stand.up-CVB.SIM</ta>
            <ta e="T345" id="Seg_6348" s="T344">jump-EMOT-PST2.[3SG]</ta>
            <ta e="T346" id="Seg_6349" s="T345">EMPH</ta>
            <ta e="T347" id="Seg_6350" s="T346">EMPH</ta>
            <ta e="T348" id="Seg_6351" s="T347">only</ta>
            <ta e="T349" id="Seg_6352" s="T348">EXCL</ta>
            <ta e="T350" id="Seg_6353" s="T349">Chopochuka.[NOM]</ta>
            <ta e="T351" id="Seg_6354" s="T350">why</ta>
            <ta e="T352" id="Seg_6355" s="T351">what.kind.of</ta>
            <ta e="T353" id="Seg_6356" s="T352">place-DAT/LOC</ta>
            <ta e="T354" id="Seg_6357" s="T353">go-PRS-2SG</ta>
            <ta e="T355" id="Seg_6358" s="T354">this</ta>
            <ta e="T356" id="Seg_6359" s="T355">say-PST2.[3SG]</ta>
            <ta e="T357" id="Seg_6360" s="T356">fish-1SG-ACC</ta>
            <ta e="T358" id="Seg_6361" s="T357">why</ta>
            <ta e="T359" id="Seg_6362" s="T358">steal-PST2-2PL=Q</ta>
            <ta e="T360" id="Seg_6363" s="T359">still</ta>
            <ta e="T361" id="Seg_6364" s="T360">fish-1SG-ACC</ta>
            <ta e="T362" id="Seg_6365" s="T361">steal-PST2-2PL</ta>
            <ta e="T364" id="Seg_6366" s="T362">eat</ta>
            <ta e="T365" id="Seg_6367" s="T364">1SG.[NOM]</ta>
            <ta e="T366" id="Seg_6368" s="T365">2SG-ACC</ta>
            <ta e="T367" id="Seg_6369" s="T366">put.into.the.mouth-CVB.SEQ</ta>
            <ta e="T368" id="Seg_6370" s="T367">throw-FUT-1SG</ta>
            <ta e="T369" id="Seg_6371" s="T368">say-PST2.[3SG]</ta>
            <ta e="T370" id="Seg_6372" s="T369">eat-CVB.SEQ</ta>
            <ta e="T371" id="Seg_6373" s="T370">throw-PST2.[3SG]</ta>
            <ta e="T372" id="Seg_6374" s="T371">belly-3SG-ACC</ta>
            <ta e="T373" id="Seg_6375" s="T372">split-CVB.SIM</ta>
            <ta e="T374" id="Seg_6376" s="T373">pull.down-PRS.[3SG]</ta>
            <ta e="T375" id="Seg_6377" s="T374">and</ta>
            <ta e="T376" id="Seg_6378" s="T375">go.out-CVB.SIM</ta>
            <ta e="T377" id="Seg_6379" s="T376">run-PRS.[3SG]</ta>
            <ta e="T378" id="Seg_6380" s="T377">this</ta>
            <ta e="T379" id="Seg_6381" s="T378">human.being.[NOM]</ta>
            <ta e="T380" id="Seg_6382" s="T379">so</ta>
            <ta e="T381" id="Seg_6383" s="T380">after</ta>
            <ta e="T382" id="Seg_6384" s="T381">knife-3SG-INSTR</ta>
            <ta e="T383" id="Seg_6385" s="T382">skin-CVB.SEQ</ta>
            <ta e="T384" id="Seg_6386" s="T383">after</ta>
            <ta e="T385" id="Seg_6387" s="T384">well</ta>
            <ta e="T386" id="Seg_6388" s="T385">live-PST2.[3SG]</ta>
            <ta e="T387" id="Seg_6389" s="T386">completely</ta>
            <ta e="T388" id="Seg_6390" s="T387">this</ta>
            <ta e="T389" id="Seg_6391" s="T388">live-CVB.SEQ</ta>
            <ta e="T390" id="Seg_6392" s="T389">think-VBZ-PST2.[3SG]</ta>
            <ta e="T391" id="Seg_6393" s="T390">this</ta>
            <ta e="T392" id="Seg_6394" s="T391">czar.[NOM]</ta>
            <ta e="T393" id="Seg_6395" s="T392">daughter-3SG-ACC</ta>
            <ta e="T394" id="Seg_6396" s="T393">search-CVB.SIM</ta>
            <ta e="T395" id="Seg_6397" s="T394">go-PTCP.FUT-DAT/LOC</ta>
            <ta e="T396" id="Seg_6398" s="T395">czar.[NOM]</ta>
            <ta e="T397" id="Seg_6399" s="T396">lonely</ta>
            <ta e="T398" id="Seg_6400" s="T397">daughter-PROPR.[NOM]</ta>
            <ta e="T399" id="Seg_6401" s="T398">say-PST2-3PL</ta>
            <ta e="T400" id="Seg_6402" s="T399">that-ACC</ta>
            <ta e="T401" id="Seg_6403" s="T400">search-CVB.SIM</ta>
            <ta e="T402" id="Seg_6404" s="T401">go-PTCP.FUT-DAT/LOC</ta>
            <ta e="T403" id="Seg_6405" s="T402">need.to</ta>
            <ta e="T404" id="Seg_6406" s="T403">so.much-PROPR</ta>
            <ta e="T405" id="Seg_6407" s="T404">what-PROPR</ta>
            <ta e="T406" id="Seg_6408" s="T405">human.being.[NOM]</ta>
            <ta e="T407" id="Seg_6409" s="T406">well</ta>
            <ta e="T408" id="Seg_6410" s="T407">think-PST2.[3SG]</ta>
            <ta e="T409" id="Seg_6411" s="T408">well</ta>
            <ta e="T410" id="Seg_6412" s="T409">czar-DAT/LOC</ta>
            <ta e="T411" id="Seg_6413" s="T410">go-CVB.PURP</ta>
            <ta e="T412" id="Seg_6414" s="T411">that</ta>
            <ta e="T413" id="Seg_6415" s="T412">sit-PRS.[3SG]</ta>
            <ta e="T414" id="Seg_6416" s="T413">this</ta>
            <ta e="T415" id="Seg_6417" s="T414">human.being-2SG.[NOM]</ta>
            <ta e="T416" id="Seg_6418" s="T415">sit-PRS.[3SG]</ta>
            <ta e="T417" id="Seg_6419" s="T416">well</ta>
            <ta e="T418" id="Seg_6420" s="T417">walk-PTCP.FUT-DAT/LOC</ta>
            <ta e="T419" id="Seg_6421" s="T418">good.[NOM]</ta>
            <ta e="T420" id="Seg_6422" s="T419">well</ta>
            <ta e="T421" id="Seg_6423" s="T420">walk-PRS.[3SG]</ta>
            <ta e="T422" id="Seg_6424" s="T421">this</ta>
            <ta e="T423" id="Seg_6425" s="T422">human.being.[NOM]</ta>
            <ta e="T424" id="Seg_6426" s="T423">walk-CVB.SEQ-walk-CVB.SEQ</ta>
            <ta e="T425" id="Seg_6427" s="T424">one</ta>
            <ta e="T426" id="Seg_6428" s="T425">place-DAT/LOC</ta>
            <ta e="T427" id="Seg_6429" s="T426">live-PST2.[3SG]</ta>
            <ta e="T428" id="Seg_6430" s="T427">live-CVB.SEQ</ta>
            <ta e="T429" id="Seg_6431" s="T428">look.at-PST2-3SG</ta>
            <ta e="T430" id="Seg_6432" s="T429">czar.[NOM]</ta>
            <ta e="T431" id="Seg_6433" s="T430">own-3SG</ta>
            <ta e="T432" id="Seg_6434" s="T431">porch-3SG.[NOM]</ta>
            <ta e="T433" id="Seg_6435" s="T432">to.be.on.view-PRS.[3SG]</ta>
            <ta e="T434" id="Seg_6436" s="T433">suddenly</ta>
            <ta e="T435" id="Seg_6437" s="T434">reach-CVB.SEQ-EP-1SG</ta>
            <ta e="T436" id="Seg_6438" s="T435">there</ta>
            <ta e="T437" id="Seg_6439" s="T436">this</ta>
            <ta e="T438" id="Seg_6440" s="T437">human.being.[NOM]</ta>
            <ta e="T439" id="Seg_6441" s="T438">walk-CVB.SEQ-walk-CVB.SEQ</ta>
            <ta e="T440" id="Seg_6442" s="T439">walk-CVB.SEQ-walk-CVB.SEQ</ta>
            <ta e="T441" id="Seg_6443" s="T440">city-DAT/LOC</ta>
            <ta e="T442" id="Seg_6444" s="T441">come-PST2.[3SG]</ta>
            <ta e="T443" id="Seg_6445" s="T442">czar.[NOM]</ta>
            <ta e="T444" id="Seg_6446" s="T443">own-3SG-DAT/LOC</ta>
            <ta e="T445" id="Seg_6447" s="T444">come-PST2-3SG</ta>
            <ta e="T446" id="Seg_6448" s="T445">most</ta>
            <ta e="T447" id="Seg_6449" s="T446">edge.[NOM]</ta>
            <ta e="T448" id="Seg_6450" s="T447">to</ta>
            <ta e="T449" id="Seg_6451" s="T448">bad</ta>
            <ta e="T450" id="Seg_6452" s="T449">very</ta>
            <ta e="T451" id="Seg_6453" s="T450">smoke-PROPR</ta>
            <ta e="T452" id="Seg_6454" s="T451">pit_dwelling.[NOM]</ta>
            <ta e="T453" id="Seg_6455" s="T452">stand-PRS.[3SG]</ta>
            <ta e="T454" id="Seg_6456" s="T453">pit_dwelling.[NOM]</ta>
            <ta e="T455" id="Seg_6457" s="T454">stand-PRS.[3SG]</ta>
            <ta e="T456" id="Seg_6458" s="T455">only</ta>
            <ta e="T457" id="Seg_6459" s="T456">this-DAT/LOC</ta>
            <ta e="T458" id="Seg_6460" s="T457">go.in-PTCP.FUT-DAT/LOC</ta>
            <ta e="T459" id="Seg_6461" s="T458">good.[NOM]</ta>
            <ta e="T460" id="Seg_6462" s="T459">pit_dwelling-DAT/LOC</ta>
            <ta e="T461" id="Seg_6463" s="T460">pit_dwelling-DAT/LOC</ta>
            <ta e="T462" id="Seg_6464" s="T461">run-CVB.SEQ</ta>
            <ta e="T463" id="Seg_6465" s="T462">fall-PST2-3SG</ta>
            <ta e="T464" id="Seg_6466" s="T463">old.man-PROPR</ta>
            <ta e="T465" id="Seg_6467" s="T464">old.woman.[NOM]</ta>
            <ta e="T466" id="Seg_6468" s="T465">old.woman-3SG.[NOM]</ta>
            <ta e="T467" id="Seg_6469" s="T466">eye-POSS</ta>
            <ta e="T468" id="Seg_6470" s="T467">NEG.[3SG]</ta>
            <ta e="T469" id="Seg_6471" s="T468">old.man-3SG.[NOM]</ta>
            <ta e="T470" id="Seg_6472" s="T469">a.little</ta>
            <ta e="T471" id="Seg_6473" s="T470">serious.[NOM]</ta>
            <ta e="T472" id="Seg_6474" s="T471">EMPH</ta>
            <ta e="T473" id="Seg_6475" s="T472">well</ta>
            <ta e="T474" id="Seg_6476" s="T473">eat-EP-CAUS-PRS-3PL</ta>
            <ta e="T475" id="Seg_6477" s="T474">this-ACC</ta>
            <ta e="T476" id="Seg_6478" s="T475">eat-PRS.[3SG]</ta>
            <ta e="T477" id="Seg_6479" s="T476">this</ta>
            <ta e="T478" id="Seg_6480" s="T477">guest.[NOM]</ta>
            <ta e="T479" id="Seg_6481" s="T478">what.kind.of-what.kind.of</ta>
            <ta e="T480" id="Seg_6482" s="T479">place-ABL</ta>
            <ta e="T481" id="Seg_6483" s="T480">come-PRS-2SG</ta>
            <ta e="T482" id="Seg_6484" s="T481">say-CVB.SEQ</ta>
            <ta e="T483" id="Seg_6485" s="T482">news.[NOM]</ta>
            <ta e="T484" id="Seg_6486" s="T483">ask-PRS-3PL</ta>
            <ta e="T485" id="Seg_6487" s="T484">1SG.[NOM]</ta>
            <ta e="T487" id="Seg_6488" s="T486">five</ta>
            <ta e="T488" id="Seg_6489" s="T487">stone-PROPR</ta>
            <ta e="T489" id="Seg_6490" s="T488">crucian.carp-net-PROPR.[NOM]</ta>
            <ta e="T490" id="Seg_6491" s="T489">be-PST1-1SG</ta>
            <ta e="T491" id="Seg_6492" s="T490">that-3SG-1SG-ABL</ta>
            <ta e="T492" id="Seg_6493" s="T491">fish-ACC</ta>
            <ta e="T493" id="Seg_6494" s="T492">gather-PTCP.PRS-1SG-ACC</ta>
            <ta e="T494" id="Seg_6495" s="T493">steal-PASS-CVB.SEQ-1SG</ta>
            <ta e="T495" id="Seg_6496" s="T494">unhappy-PL-ACC</ta>
            <ta e="T496" id="Seg_6497" s="T495">kill-ITER-CVB.SEQ</ta>
            <ta e="T497" id="Seg_6498" s="T496">come-PRS-1SG</ta>
            <ta e="T498" id="Seg_6499" s="T497">say-PST2.[3SG]</ta>
            <ta e="T499" id="Seg_6500" s="T498">ah</ta>
            <ta e="T502" id="Seg_6501" s="T501">unhappy-PL-ACC</ta>
            <ta e="T503" id="Seg_6502" s="T502">kill-CVB.SEQ</ta>
            <ta e="T504" id="Seg_6503" s="T503">come-PST2.[3SG]</ta>
            <ta e="T505" id="Seg_6504" s="T504">this</ta>
            <ta e="T506" id="Seg_6505" s="T505">old.man-ABL</ta>
            <ta e="T507" id="Seg_6506" s="T506">ask-PRS.[3SG]</ta>
            <ta e="T508" id="Seg_6507" s="T507">czar.[NOM]</ta>
            <ta e="T509" id="Seg_6508" s="T508">daughter-PROPR.[NOM]</ta>
            <ta e="T510" id="Seg_6509" s="T509">Q</ta>
            <ta e="T511" id="Seg_6510" s="T510">say-CVB.SEQ</ta>
            <ta e="T512" id="Seg_6511" s="T511">EMPH-lonely</ta>
            <ta e="T513" id="Seg_6512" s="T512">daughter-PROPR.[NOM]</ta>
            <ta e="T514" id="Seg_6513" s="T513">whereto</ta>
            <ta e="T515" id="Seg_6514" s="T514">INDEF</ta>
            <ta e="T517" id="Seg_6515" s="T516">husband-DAT/LOC</ta>
            <ta e="T518" id="Seg_6516" s="T517">give-PRS-3PL</ta>
            <ta e="T519" id="Seg_6517" s="T518">it.is.said</ta>
            <ta e="T520" id="Seg_6518" s="T519">say-CVB.SEQ</ta>
            <ta e="T521" id="Seg_6519" s="T520">old.man.[NOM]</ta>
            <ta e="T522" id="Seg_6520" s="T521">2SG.[NOM]</ta>
            <ta e="T523" id="Seg_6521" s="T522">matchmaker-DAT/LOC</ta>
            <ta e="T524" id="Seg_6522" s="T523">go-FUT-2SG</ta>
            <ta e="T525" id="Seg_6523" s="T524">NEG</ta>
            <ta e="T526" id="Seg_6524" s="T525">Q</ta>
            <ta e="T527" id="Seg_6525" s="T526">say-PST2.[3SG]</ta>
            <ta e="T528" id="Seg_6526" s="T527">recent.[NOM]</ta>
            <ta e="T529" id="Seg_6527" s="T528">guest-3SG.[NOM]</ta>
            <ta e="T530" id="Seg_6528" s="T529">in.either.case</ta>
            <ta e="T531" id="Seg_6529" s="T530">ask-CVB.SEQ</ta>
            <ta e="T532" id="Seg_6530" s="T531">try-FUT-1PL</ta>
            <ta e="T533" id="Seg_6531" s="T532">say-PST2.[3SG]</ta>
            <ta e="T534" id="Seg_6532" s="T533">one</ta>
            <ta e="T535" id="Seg_6533" s="T534">guest.[NOM]</ta>
            <ta e="T536" id="Seg_6534" s="T535">come-PST2.[3SG]</ta>
            <ta e="T537" id="Seg_6535" s="T536">daughter-2SG-ACC</ta>
            <ta e="T538" id="Seg_6536" s="T537">beg-PRS.[3SG]</ta>
            <ta e="T539" id="Seg_6537" s="T538">say-CVB.SEQ-2SG</ta>
            <ta e="T540" id="Seg_6538" s="T539">speak.[IMP.2SG]</ta>
            <ta e="T541" id="Seg_6539" s="T540">say-PST2.[3SG]</ta>
            <ta e="T542" id="Seg_6540" s="T541">this</ta>
            <ta e="T543" id="Seg_6541" s="T542">human.being.[NOM]</ta>
            <ta e="T544" id="Seg_6542" s="T543">go-CVB.SEQ</ta>
            <ta e="T545" id="Seg_6543" s="T544">stay-PST2.[3SG]</ta>
            <ta e="T546" id="Seg_6544" s="T545">czar.[NOM]</ta>
            <ta e="T547" id="Seg_6545" s="T546">run-CVB.SEQ</ta>
            <ta e="T548" id="Seg_6546" s="T547">fall-PST2.[3SG]</ta>
            <ta e="T549" id="Seg_6547" s="T548">well</ta>
            <ta e="T550" id="Seg_6548" s="T549">why</ta>
            <ta e="T551" id="Seg_6549" s="T550">come-PRS-2SG</ta>
            <ta e="T552" id="Seg_6550" s="T551">no</ta>
            <ta e="T553" id="Seg_6551" s="T552">1SG.[NOM]</ta>
            <ta e="T554" id="Seg_6552" s="T553">guest-PROPR-1SG</ta>
            <ta e="T555" id="Seg_6553" s="T554">what.kind.of-what.kind.of</ta>
            <ta e="T556" id="Seg_6554" s="T555">place-ABL</ta>
            <ta e="T557" id="Seg_6555" s="T556">come-PST2-3SG</ta>
            <ta e="T558" id="Seg_6556" s="T557">MOD</ta>
            <ta e="T559" id="Seg_6557" s="T558">know-NEG-1SG</ta>
            <ta e="T560" id="Seg_6558" s="T559">that</ta>
            <ta e="T561" id="Seg_6559" s="T560">human.being.[NOM]</ta>
            <ta e="T562" id="Seg_6560" s="T561">daughter-2SG-ACC</ta>
            <ta e="T563" id="Seg_6561" s="T562">beg-CVB.SEQ</ta>
            <ta e="T564" id="Seg_6562" s="T563">be-PRS.[3SG]</ta>
            <ta e="T565" id="Seg_6563" s="T564">say-PST2.[3SG]</ta>
            <ta e="T566" id="Seg_6564" s="T565">so</ta>
            <ta e="T567" id="Seg_6565" s="T566">make-CVB.SEQ</ta>
            <ta e="T568" id="Seg_6566" s="T567">that</ta>
            <ta e="T569" id="Seg_6567" s="T568">human.being.[NOM]</ta>
            <ta e="T570" id="Seg_6568" s="T569">own-3SG-GEN</ta>
            <ta e="T571" id="Seg_6569" s="T570">face-3SG-ACC-eye-3SG-ACC</ta>
            <ta e="T572" id="Seg_6570" s="T571">see-PTCP.FUT-1SG-ACC</ta>
            <ta e="T573" id="Seg_6571" s="T572">hither</ta>
            <ta e="T574" id="Seg_6572" s="T573">bring-EP-IMP.2PL</ta>
            <ta e="T575" id="Seg_6573" s="T574">say-PST2.[3SG]</ta>
            <ta e="T576" id="Seg_6574" s="T575">czar-3SG.[NOM]</ta>
            <ta e="T577" id="Seg_6575" s="T576">this</ta>
            <ta e="T578" id="Seg_6576" s="T577">old.man.[NOM]</ta>
            <ta e="T579" id="Seg_6577" s="T578">recent</ta>
            <ta e="T580" id="Seg_6578" s="T579">guest-3SG-ACC</ta>
            <ta e="T581" id="Seg_6579" s="T580">bring-PST2.[3SG]</ta>
            <ta e="T582" id="Seg_6580" s="T581">pa</ta>
            <ta e="T583" id="Seg_6581" s="T582">1SG.[NOM]</ta>
            <ta e="T584" id="Seg_6582" s="T583">such</ta>
            <ta e="T585" id="Seg_6583" s="T584">human.being-DAT/LOC</ta>
            <ta e="T586" id="Seg_6584" s="T585">give-NEG-1SG</ta>
            <ta e="T587" id="Seg_6585" s="T586">arm-3SG-ACC-leg-3SG-ACC</ta>
            <ta e="T588" id="Seg_6586" s="T587">break-FREQ-CVB.SEQ</ta>
            <ta e="T589" id="Seg_6587" s="T588">after</ta>
            <ta e="T590" id="Seg_6588" s="T589">prison-DAT/LOC</ta>
            <ta e="T591" id="Seg_6589" s="T590">throw-EP-IMP.2PL</ta>
            <ta e="T592" id="Seg_6590" s="T591">say-PST2.[3SG]</ta>
            <ta e="T593" id="Seg_6591" s="T592">ah</ta>
            <ta e="T594" id="Seg_6592" s="T593">this</ta>
            <ta e="T595" id="Seg_6593" s="T594">human.being-2SG-GEN</ta>
            <ta e="T596" id="Seg_6594" s="T595">arm-3SG-ACC-leg-3SG-ACC</ta>
            <ta e="T597" id="Seg_6595" s="T596">break-PST2.NEG-3PL</ta>
            <ta e="T598" id="Seg_6596" s="T597">string-INSTR</ta>
            <ta e="T599" id="Seg_6597" s="T598">tie-CVB.SEQ</ta>
            <ta e="T600" id="Seg_6598" s="T599">go-CVB.SEQ-3PL</ta>
            <ta e="T601" id="Seg_6599" s="T600">prison-DAT/LOC</ta>
            <ta e="T602" id="Seg_6600" s="T601">stick-CVB.SEQ</ta>
            <ta e="T603" id="Seg_6601" s="T602">throw-PST2-3PL</ta>
            <ta e="T604" id="Seg_6602" s="T603">not.long.ago</ta>
            <ta e="T605" id="Seg_6603" s="T604">human.being-2SG-ACC</ta>
            <ta e="T607" id="Seg_6604" s="T606">this</ta>
            <ta e="T608" id="Seg_6605" s="T607">human.being.[NOM]</ta>
            <ta e="T609" id="Seg_6606" s="T608">mouse.[NOM]</ta>
            <ta e="T610" id="Seg_6607" s="T609">become-CVB.SEQ</ta>
            <ta e="T611" id="Seg_6608" s="T610">go.out-CVB.SIM</ta>
            <ta e="T612" id="Seg_6609" s="T611">run-PST2.[3SG]</ta>
            <ta e="T613" id="Seg_6610" s="T612">go.out-CVB.SIM</ta>
            <ta e="T614" id="Seg_6611" s="T613">run-CVB.SEQ</ta>
            <ta e="T615" id="Seg_6612" s="T614">old.man-3SG-DAT/LOC</ta>
            <ta e="T616" id="Seg_6613" s="T615">again</ta>
            <ta e="T617" id="Seg_6614" s="T616">go.in-PST2.[3SG]</ta>
            <ta e="T618" id="Seg_6615" s="T617">well</ta>
            <ta e="T619" id="Seg_6616" s="T618">know-NEG-1SG</ta>
            <ta e="T620" id="Seg_6617" s="T619">recent-EP-COMP</ta>
            <ta e="T621" id="Seg_6618" s="T620">different</ta>
            <ta e="T622" id="Seg_6619" s="T621">human.being.[NOM]</ta>
            <ta e="T623" id="Seg_6620" s="T622">come-PST2-3SG</ta>
            <ta e="T624" id="Seg_6621" s="T623">Q</ta>
            <ta e="T625" id="Seg_6622" s="T624">what.[NOM]</ta>
            <ta e="T626" id="Seg_6623" s="T625">Q</ta>
            <ta e="T627" id="Seg_6624" s="T626">again</ta>
            <ta e="T628" id="Seg_6625" s="T627">daughter-2SG-ACC</ta>
            <ta e="T629" id="Seg_6626" s="T628">beg-CVB.SEQ</ta>
            <ta e="T630" id="Seg_6627" s="T629">be-PRS.[3SG]</ta>
            <ta e="T631" id="Seg_6628" s="T630">say-CVB.SEQ-2SG</ta>
            <ta e="T632" id="Seg_6629" s="T631">go.[IMP.2SG]</ta>
            <ta e="T633" id="Seg_6630" s="T632">well</ta>
            <ta e="T634" id="Seg_6631" s="T633">say-PST2.[3SG]</ta>
            <ta e="T635" id="Seg_6632" s="T634">again</ta>
            <ta e="T636" id="Seg_6633" s="T635">go-PST2.[3SG]</ta>
            <ta e="T637" id="Seg_6634" s="T636">old.man.[NOM]</ta>
            <ta e="T638" id="Seg_6635" s="T637">well</ta>
            <ta e="T639" id="Seg_6636" s="T638">that</ta>
            <ta e="T640" id="Seg_6637" s="T639">after</ta>
            <ta e="T641" id="Seg_6638" s="T640">again</ta>
            <ta e="T642" id="Seg_6639" s="T641">say-PST2.[3SG]</ta>
            <ta e="T643" id="Seg_6640" s="T642">well</ta>
            <ta e="T644" id="Seg_6641" s="T643">again</ta>
            <ta e="T645" id="Seg_6642" s="T644">guest.[NOM]</ta>
            <ta e="T646" id="Seg_6643" s="T645">come-PST1-3SG</ta>
            <ta e="T647" id="Seg_6644" s="T646">daughter-2SG-ACC</ta>
            <ta e="T648" id="Seg_6645" s="T647">beg-CVB.SEQ</ta>
            <ta e="T649" id="Seg_6646" s="T648">be-PRS.[3SG]</ta>
            <ta e="T650" id="Seg_6647" s="T649">say-PST2.[3SG]</ta>
            <ta e="T651" id="Seg_6648" s="T650">friend-PL.[NOM]</ta>
            <ta e="T652" id="Seg_6649" s="T651">not.long.ago</ta>
            <ta e="T653" id="Seg_6650" s="T652">prison-DAT/LOC</ta>
            <ta e="T654" id="Seg_6651" s="T653">throw-PTCP.HAB</ta>
            <ta e="T655" id="Seg_6652" s="T654">human.being-2PL-ACC</ta>
            <ta e="T656" id="Seg_6653" s="T655">see-EP-IMP.2PL</ta>
            <ta e="T657" id="Seg_6654" s="T656">well</ta>
            <ta e="T658" id="Seg_6655" s="T657">how-how</ta>
            <ta e="T659" id="Seg_6656" s="T658">human.being-ACC</ta>
            <ta e="T660" id="Seg_6657" s="T659">torture-PRS-1PL</ta>
            <ta e="T661" id="Seg_6658" s="T660">MOD</ta>
            <ta e="T662" id="Seg_6659" s="T661">say-PST2.[3SG]</ta>
            <ta e="T663" id="Seg_6660" s="T662">prison-DAT/LOC</ta>
            <ta e="T664" id="Seg_6661" s="T663">throw-PTCP.PST</ta>
            <ta e="T665" id="Seg_6662" s="T664">human.being-3SG.[NOM]</ta>
            <ta e="T666" id="Seg_6663" s="T665">trace-3SG.[NOM]</ta>
            <ta e="T667" id="Seg_6664" s="T666">just</ta>
            <ta e="T668" id="Seg_6665" s="T667">not.be.straight-CVB.SIM</ta>
            <ta e="T669" id="Seg_6666" s="T668">lie-PRS.[3SG]</ta>
            <ta e="T670" id="Seg_6667" s="T669">tie-PTCP.PST</ta>
            <ta e="T671" id="Seg_6668" s="T670">string-3SG.[NOM]</ta>
            <ta e="T672" id="Seg_6669" s="T671">string-3SG-INSTR</ta>
            <ta e="T673" id="Seg_6670" s="T672">lie-PRS.[3SG]</ta>
            <ta e="T674" id="Seg_6671" s="T673">well</ta>
            <ta e="T675" id="Seg_6672" s="T674">well</ta>
            <ta e="T676" id="Seg_6673" s="T675">such-such</ta>
            <ta e="T677" id="Seg_6674" s="T676">well</ta>
            <ta e="T678" id="Seg_6675" s="T677">oh</ta>
            <ta e="T679" id="Seg_6676" s="T678">well</ta>
            <ta e="T680" id="Seg_6677" s="T679">indeed</ta>
            <ta e="T681" id="Seg_6678" s="T680">EMPH</ta>
            <ta e="T682" id="Seg_6679" s="T681">good.[NOM]</ta>
            <ta e="T683" id="Seg_6680" s="T682">human.being-ACC</ta>
            <ta e="T684" id="Seg_6681" s="T683">make-PST1-1SG</ta>
            <ta e="T685" id="Seg_6682" s="T684">EMPH</ta>
            <ta e="T686" id="Seg_6683" s="T685">probably</ta>
            <ta e="T687" id="Seg_6684" s="T686">daughter-1SG-ACC</ta>
            <ta e="T688" id="Seg_6685" s="T687">give-PRS-1SG</ta>
            <ta e="T689" id="Seg_6686" s="T688">say-FUT-1SG</ta>
            <ta e="T690" id="Seg_6687" s="T689">MOD</ta>
            <ta e="T691" id="Seg_6688" s="T690">this</ta>
            <ta e="T692" id="Seg_6689" s="T691">human.being.[NOM]</ta>
            <ta e="T693" id="Seg_6690" s="T692">that-PROPR</ta>
            <ta e="T694" id="Seg_6691" s="T693">unhappy</ta>
            <ta e="T695" id="Seg_6692" s="T694">kill-PTCP.PST-3SG-ACC</ta>
            <ta e="T696" id="Seg_6693" s="T695">czar.[NOM]</ta>
            <ta e="T697" id="Seg_6694" s="T696">knee-3SG-DAT/LOC</ta>
            <ta e="T698" id="Seg_6695" s="T697">pour.out-CVB.SIM</ta>
            <ta e="T699" id="Seg_6696" s="T698">throw-PST2.[3SG]</ta>
            <ta e="T700" id="Seg_6697" s="T699">so</ta>
            <ta e="T701" id="Seg_6698" s="T700">make-CVB.SEQ</ta>
            <ta e="T702" id="Seg_6699" s="T701">reindeer.caravan-3SG-GEN</ta>
            <ta e="T703" id="Seg_6700" s="T702">back-3SG-DAT/LOC</ta>
            <ta e="T704" id="Seg_6701" s="T703">small.bird.[NOM]</ta>
            <ta e="T705" id="Seg_6702" s="T704">sing-PRS.[3SG]</ta>
            <ta e="T706" id="Seg_6703" s="T705">%%</ta>
            <ta e="T707" id="Seg_6704" s="T706">separate-CVB.SEQ</ta>
            <ta e="T708" id="Seg_6705" s="T707">bring-PST2.[3SG]</ta>
            <ta e="T709" id="Seg_6706" s="T708">this</ta>
            <ta e="T710" id="Seg_6707" s="T709">human.being.[NOM]</ta>
            <ta e="T711" id="Seg_6708" s="T710">different-different</ta>
            <ta e="T712" id="Seg_6709" s="T711">soldier-PROPR-what-PROPR</ta>
            <ta e="T713" id="Seg_6710" s="T712">this</ta>
            <ta e="T714" id="Seg_6711" s="T713">human.being.[NOM]</ta>
            <ta e="T715" id="Seg_6712" s="T714">well</ta>
            <ta e="T716" id="Seg_6713" s="T715">go-PRS.[3SG]</ta>
            <ta e="T717" id="Seg_6714" s="T716">house-3SG-DAT/LOC</ta>
            <ta e="T718" id="Seg_6715" s="T717">daughter-3SG-ACC</ta>
            <ta e="T719" id="Seg_6716" s="T718">take-CVB.SEQ</ta>
            <ta e="T720" id="Seg_6717" s="T719">after</ta>
            <ta e="T721" id="Seg_6718" s="T720">well</ta>
            <ta e="T722" id="Seg_6719" s="T721">nomadize-PRS.[3SG]</ta>
            <ta e="T723" id="Seg_6720" s="T722">nomadize-CVB.SEQ</ta>
            <ta e="T724" id="Seg_6721" s="T723">that.[NOM]</ta>
            <ta e="T725" id="Seg_6722" s="T724">come-CVB.SEQ-3PL</ta>
            <ta e="T726" id="Seg_6723" s="T725">that.[NOM]</ta>
            <ta e="T727" id="Seg_6724" s="T726">well</ta>
            <ta e="T728" id="Seg_6725" s="T727">that.[NOM]</ta>
            <ta e="T729" id="Seg_6726" s="T728">be.rich-CVB.SEQ-eat.ones.fill-CVB.SEQ</ta>
            <ta e="T730" id="Seg_6727" s="T729">live-PST2-3SG</ta>
            <ta e="T731" id="Seg_6728" s="T730">well</ta>
            <ta e="T732" id="Seg_6729" s="T731">that.[NOM]</ta>
            <ta e="T733" id="Seg_6730" s="T732">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T2" id="Seg_6731" s="T1">Tschopotschuka.[NOM]</ta>
            <ta e="T3" id="Seg_6732" s="T2">leben-PST2.[3SG]</ta>
            <ta e="T4" id="Seg_6733" s="T3">man.sagt</ta>
            <ta e="T5" id="Seg_6734" s="T4">fünf</ta>
            <ta e="T6" id="Seg_6735" s="T5">Holz</ta>
            <ta e="T7" id="Seg_6736" s="T6">stellen-PTCP.PST</ta>
            <ta e="T8" id="Seg_6737" s="T7">viereckiges.Stangenzelt.[NOM]</ta>
            <ta e="T9" id="Seg_6738" s="T8">Haus-PROPR.[NOM]</ta>
            <ta e="T10" id="Seg_6739" s="T9">fünf</ta>
            <ta e="T11" id="Seg_6740" s="T10">Stein-PROPR</ta>
            <ta e="T12" id="Seg_6741" s="T11">Karausche-Netz-PROPR.[NOM]</ta>
            <ta e="T13" id="Seg_6742" s="T12">Tag.[NOM]</ta>
            <ta e="T14" id="Seg_6743" s="T13">jeder</ta>
            <ta e="T15" id="Seg_6744" s="T14">Netz-VBZ-CVB.SIM</ta>
            <ta e="T16" id="Seg_6745" s="T15">hineingehen-PRS.[3SG]</ta>
            <ta e="T17" id="Seg_6746" s="T16">dieses</ta>
            <ta e="T18" id="Seg_6747" s="T17">Mensch.[NOM]</ta>
            <ta e="T19" id="Seg_6748" s="T18">Fisch-ACC</ta>
            <ta e="T20" id="Seg_6749" s="T19">aufragen-ADVZ</ta>
            <ta e="T21" id="Seg_6750" s="T20">sammeln-PST2.[3SG]</ta>
            <ta e="T22" id="Seg_6751" s="T21">Netz-3SG-ABL</ta>
            <ta e="T23" id="Seg_6752" s="T22">so</ta>
            <ta e="T24" id="Seg_6753" s="T23">machen-CVB.SEQ</ta>
            <ta e="T25" id="Seg_6754" s="T24">einmal</ta>
            <ta e="T26" id="Seg_6755" s="T25">gehen-PST2-3SG</ta>
            <ta e="T27" id="Seg_6756" s="T26">Polarfuchs-PL.[NOM]</ta>
            <ta e="T28" id="Seg_6757" s="T27">was-PL.[NOM]</ta>
            <ta e="T29" id="Seg_6758" s="T28">essen-PST2-3PL</ta>
            <ta e="T30" id="Seg_6759" s="T29">plötzlich</ta>
            <ta e="T31" id="Seg_6760" s="T30">Fisch-3SG-ACC</ta>
            <ta e="T32" id="Seg_6761" s="T31">dieses-ACC</ta>
            <ta e="T33" id="Seg_6762" s="T32">folgen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T34" id="Seg_6763" s="T33">Spur-3SG-ACC</ta>
            <ta e="T35" id="Seg_6764" s="T34">folgen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T36" id="Seg_6765" s="T35">Fisch-3SG-ACC</ta>
            <ta e="T37" id="Seg_6766" s="T36">Haus-3SG-DAT/LOC</ta>
            <ta e="T38" id="Seg_6767" s="T37">tragen-CVB.SEQ</ta>
            <ta e="T39" id="Seg_6768" s="T38">nachdem</ta>
            <ta e="T40" id="Seg_6769" s="T39">dieses</ta>
            <ta e="T41" id="Seg_6770" s="T40">Mensch.[NOM]</ta>
            <ta e="T42" id="Seg_6771" s="T41">Polarfuchs.[NOM]</ta>
            <ta e="T43" id="Seg_6772" s="T42">Spur-3SG-ACC</ta>
            <ta e="T44" id="Seg_6773" s="T43">folgen-PST2.[3SG]</ta>
            <ta e="T45" id="Seg_6774" s="T44">Polarfuchs.[NOM]</ta>
            <ta e="T46" id="Seg_6775" s="T45">Spur-3SG-ACC</ta>
            <ta e="T47" id="Seg_6776" s="T46">folgen-CVB.SEQ</ta>
            <ta e="T48" id="Seg_6777" s="T47">gehen-TEMP-3SG</ta>
            <ta e="T49" id="Seg_6778" s="T48">na</ta>
            <ta e="T50" id="Seg_6779" s="T49">Polarfuchs.[NOM]</ta>
            <ta e="T51" id="Seg_6780" s="T50">was-ACC</ta>
            <ta e="T52" id="Seg_6781" s="T51">und</ta>
            <ta e="T53" id="Seg_6782" s="T52">eins</ta>
            <ta e="T54" id="Seg_6783" s="T53">Strauch.[NOM]</ta>
            <ta e="T55" id="Seg_6784" s="T54">Inneres-3SG-ABL</ta>
            <ta e="T56" id="Seg_6785" s="T55">aufstehen-CVB.SIM</ta>
            <ta e="T57" id="Seg_6786" s="T56">springen-PST2.[3SG]</ta>
            <ta e="T58" id="Seg_6787" s="T57">oh.nein</ta>
            <ta e="T59" id="Seg_6788" s="T58">Tschopotschuka.[NOM]</ta>
            <ta e="T60" id="Seg_6789" s="T59">Mensch-ACC</ta>
            <ta e="T61" id="Seg_6790" s="T60">erschrecken-CAUS-PST1-2SG</ta>
            <ta e="T62" id="Seg_6791" s="T61">na</ta>
            <ta e="T63" id="Seg_6792" s="T62">wie</ta>
            <ta e="T64" id="Seg_6793" s="T63">kommen-PRS-2SG</ta>
            <ta e="T65" id="Seg_6794" s="T64">na</ta>
            <ta e="T66" id="Seg_6795" s="T65">Fisch-1SG-ACC</ta>
            <ta e="T67" id="Seg_6796" s="T66">nun</ta>
            <ta e="T68" id="Seg_6797" s="T67">warum</ta>
            <ta e="T69" id="Seg_6798" s="T68">stehlen-PST2-2PL=Q</ta>
            <ta e="T70" id="Seg_6799" s="T69">2PL.[NOM]</ta>
            <ta e="T71" id="Seg_6800" s="T70">was</ta>
            <ta e="T72" id="Seg_6801" s="T71">1SG.[NOM]</ta>
            <ta e="T73" id="Seg_6802" s="T72">stehlen-PST2-EP-1SG</ta>
            <ta e="T74" id="Seg_6803" s="T73">Q</ta>
            <ta e="T75" id="Seg_6804" s="T74">sagen-CVB.SIM-sagen-CVB.SIM</ta>
            <ta e="T76" id="Seg_6805" s="T75">machen-PST2.[3SG]</ta>
            <ta e="T77" id="Seg_6806" s="T76">dieses-3SG-2SG.[NOM]</ta>
            <ta e="T78" id="Seg_6807" s="T77">Maus.[NOM]</ta>
            <ta e="T79" id="Seg_6808" s="T78">werden-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_6809" s="T79">Maus.[NOM]</ta>
            <ta e="T81" id="Seg_6810" s="T80">werden-CVB.SEQ</ta>
            <ta e="T82" id="Seg_6811" s="T81">doch</ta>
            <ta e="T83" id="Seg_6812" s="T82">alter.Mann-EP-2SG.[NOM]</ta>
            <ta e="T84" id="Seg_6813" s="T83">doch</ta>
            <ta e="T85" id="Seg_6814" s="T84">gehen-PRS.[3SG]</ta>
            <ta e="T86" id="Seg_6815" s="T85">jener-ACC</ta>
            <ta e="T87" id="Seg_6816" s="T86">in.den.Mund.nehmen-CVB.SEQ</ta>
            <ta e="T88" id="Seg_6817" s="T87">werfen-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_6818" s="T88">Polarfuchs-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_6819" s="T89">jener-2SG.[NOM]</ta>
            <ta e="T91" id="Seg_6820" s="T90">Bauch-3SG-ACC</ta>
            <ta e="T92" id="Seg_6821" s="T91">na</ta>
            <ta e="T95" id="Seg_6822" s="T94">herunterziehen-CVB.SEQ</ta>
            <ta e="T96" id="Seg_6823" s="T95">schneiden-CVB.SEQ</ta>
            <ta e="T97" id="Seg_6824" s="T96">hinausgehen-CVB.SIM</ta>
            <ta e="T100" id="Seg_6825" s="T99">hinausgehen-CVB.SIM</ta>
            <ta e="T101" id="Seg_6826" s="T100">rennen-PST2.[3SG]</ta>
            <ta e="T102" id="Seg_6827" s="T101">so</ta>
            <ta e="T103" id="Seg_6828" s="T102">nachdem</ta>
            <ta e="T104" id="Seg_6829" s="T103">jener-ACC</ta>
            <ta e="T105" id="Seg_6830" s="T104">Haut.abziehen-PRS.[3SG]</ta>
            <ta e="T106" id="Seg_6831" s="T105">und</ta>
            <ta e="T107" id="Seg_6832" s="T106">Tasche-3SG-DAT/LOC</ta>
            <ta e="T108" id="Seg_6833" s="T107">hineintun-PRS.[3SG]</ta>
            <ta e="T109" id="Seg_6834" s="T108">und</ta>
            <ta e="T110" id="Seg_6835" s="T109">go-CVB.SEQ</ta>
            <ta e="T111" id="Seg_6836" s="T110">gehen-PST2.[3SG]</ta>
            <ta e="T112" id="Seg_6837" s="T111">wieder</ta>
            <ta e="T113" id="Seg_6838" s="T112">Vorderteil.[NOM]</ta>
            <ta e="T114" id="Seg_6839" s="T113">zu</ta>
            <ta e="T115" id="Seg_6840" s="T114">go-CVB.SEQ</ta>
            <ta e="T116" id="Seg_6841" s="T115">gehen-CVB.SEQ</ta>
            <ta e="T117" id="Seg_6842" s="T116">Hase.[NOM]</ta>
            <ta e="T118" id="Seg_6843" s="T117">stellen-PST2.[3SG]</ta>
            <ta e="T119" id="Seg_6844" s="T118">oh.nein</ta>
            <ta e="T120" id="Seg_6845" s="T119">Tschopotschuka.[NOM]</ta>
            <ta e="T121" id="Seg_6846" s="T120">was.[NOM]</ta>
            <ta e="T122" id="Seg_6847" s="T121">was.für.ein</ta>
            <ta e="T123" id="Seg_6848" s="T122">Ort-ABL</ta>
            <ta e="T124" id="Seg_6849" s="T123">kommen-PRS-2SG</ta>
            <ta e="T125" id="Seg_6850" s="T124">Mensch-ACC</ta>
            <ta e="T126" id="Seg_6851" s="T125">erschrecken-CAUS-EMOT-PST1-2SG</ta>
            <ta e="T127" id="Seg_6852" s="T126">sagen-PST2.[3SG]</ta>
            <ta e="T128" id="Seg_6853" s="T127">na</ta>
            <ta e="T129" id="Seg_6854" s="T128">Fisch-1SG-ACC</ta>
            <ta e="T130" id="Seg_6855" s="T129">warum</ta>
            <ta e="T131" id="Seg_6856" s="T130">ganz-3SG-ACC</ta>
            <ta e="T132" id="Seg_6857" s="T131">essen-PST2-2PL=Q</ta>
            <ta e="T133" id="Seg_6858" s="T132">Fisch-1SG-ACC</ta>
            <ta e="T134" id="Seg_6859" s="T133">folgen-CVB.SIM</ta>
            <ta e="T135" id="Seg_6860" s="T134">gehen-PRS-1SG</ta>
            <ta e="T138" id="Seg_6861" s="T136">jener.[NOM]</ta>
            <ta e="T139" id="Seg_6862" s="T138">auch</ta>
            <ta e="T141" id="Seg_6863" s="T139">na</ta>
            <ta e="T142" id="Seg_6864" s="T141">doch</ta>
            <ta e="T143" id="Seg_6865" s="T142">doch</ta>
            <ta e="T144" id="Seg_6866" s="T143">kämpfen-PRS-3PL</ta>
            <ta e="T145" id="Seg_6867" s="T144">äh</ta>
            <ta e="T146" id="Seg_6868" s="T145">Hase-ACC</ta>
            <ta e="T147" id="Seg_6869" s="T146">mit</ta>
            <ta e="T148" id="Seg_6870" s="T147">kämpfen-CVB.SEQ-3PL</ta>
            <ta e="T149" id="Seg_6871" s="T148">Hase-ACC</ta>
            <ta e="T150" id="Seg_6872" s="T149">was-3SG-ACC</ta>
            <ta e="T151" id="Seg_6873" s="T150">Mensch-VBZ-FUT.[3SG]=Q</ta>
            <ta e="T152" id="Seg_6874" s="T151">jenes</ta>
            <ta e="T153" id="Seg_6875" s="T152">Mensch.[NOM]</ta>
            <ta e="T154" id="Seg_6876" s="T153">jener-ACC</ta>
            <ta e="T155" id="Seg_6877" s="T154">töten-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_6878" s="T155">und</ta>
            <ta e="T157" id="Seg_6879" s="T156">wieder</ta>
            <ta e="T159" id="Seg_6880" s="T158">Tasche-3SG-DAT/LOC</ta>
            <ta e="T160" id="Seg_6881" s="T159">hineintun-CVB.SEQ</ta>
            <ta e="T161" id="Seg_6882" s="T160">werfen-PST2.[3SG]</ta>
            <ta e="T162" id="Seg_6883" s="T161">Haut.abziehen-EP-MED-CVB.SEQ</ta>
            <ta e="T163" id="Seg_6884" s="T162">wieder</ta>
            <ta e="T164" id="Seg_6885" s="T163">go-CVB.SEQ</ta>
            <ta e="T165" id="Seg_6886" s="T164">gehen-PST2.[3SG]</ta>
            <ta e="T166" id="Seg_6887" s="T165">go-CVB.SEQ-go-CVB.SEQ</ta>
            <ta e="T167" id="Seg_6888" s="T166">gehen-TEMP-3SG</ta>
            <ta e="T168" id="Seg_6889" s="T167">Fuchs.[NOM]</ta>
            <ta e="T169" id="Seg_6890" s="T168">aufstehen-CVB.SIM</ta>
            <ta e="T170" id="Seg_6891" s="T169">springen-PST2.[3SG]</ta>
            <ta e="T171" id="Seg_6892" s="T170">oh</ta>
            <ta e="T172" id="Seg_6893" s="T171">Tschopotschuka.[NOM]</ta>
            <ta e="T173" id="Seg_6894" s="T172">Mensch-ACC</ta>
            <ta e="T174" id="Seg_6895" s="T173">erschrecken-CAUS-PST1-2SG</ta>
            <ta e="T175" id="Seg_6896" s="T174">was.für.ein</ta>
            <ta e="T176" id="Seg_6897" s="T175">was.für.ein</ta>
            <ta e="T177" id="Seg_6898" s="T176">Ort-ABL</ta>
            <ta e="T178" id="Seg_6899" s="T177">kommen-PRS-2SG</ta>
            <ta e="T179" id="Seg_6900" s="T178">dieses</ta>
            <ta e="T180" id="Seg_6901" s="T179">sagen-PST2.[3SG]</ta>
            <ta e="T181" id="Seg_6902" s="T180">na</ta>
            <ta e="T182" id="Seg_6903" s="T181">sagen-PST2.[3SG]</ta>
            <ta e="T183" id="Seg_6904" s="T182">nun</ta>
            <ta e="T184" id="Seg_6905" s="T183">Fisch-1SG-ACC</ta>
            <ta e="T185" id="Seg_6906" s="T184">ganz-3SG-ACC</ta>
            <ta e="T186" id="Seg_6907" s="T185">essen-PST2-2PL</ta>
            <ta e="T187" id="Seg_6908" s="T186">jenes.[NOM]</ta>
            <ta e="T188" id="Seg_6909" s="T187">wegen</ta>
            <ta e="T189" id="Seg_6910" s="T188">folgen-CVB.SEQ</ta>
            <ta e="T190" id="Seg_6911" s="T189">kommen-PRS-1SG</ta>
            <ta e="T191" id="Seg_6912" s="T190">pa</ta>
            <ta e="T192" id="Seg_6913" s="T191">2SG.[NOM]</ta>
            <ta e="T193" id="Seg_6914" s="T192">Fisch-2SG-DAT/LOC</ta>
            <ta e="T194" id="Seg_6915" s="T193">1PL.[NOM]</ta>
            <ta e="T195" id="Seg_6916" s="T194">sich.nähern-PST2.NEG-1PL</ta>
            <ta e="T196" id="Seg_6917" s="T195">EMPH</ta>
            <ta e="T197" id="Seg_6918" s="T196">noch</ta>
            <ta e="T198" id="Seg_6919" s="T197">dafür</ta>
            <ta e="T199" id="Seg_6920" s="T198">1SG.[NOM]</ta>
            <ta e="T200" id="Seg_6921" s="T199">2SG-ACC</ta>
            <ta e="T201" id="Seg_6922" s="T200">reißen-CVB.SIM</ta>
            <ta e="T202" id="Seg_6923" s="T201">berühren-CVB.SEQ</ta>
            <ta e="T203" id="Seg_6924" s="T202">essen-CVB.SEQ</ta>
            <ta e="T204" id="Seg_6925" s="T203">werfen-FUT-1SG</ta>
            <ta e="T205" id="Seg_6926" s="T204">sagen-PST2.[3SG]</ta>
            <ta e="T206" id="Seg_6927" s="T205">doch</ta>
            <ta e="T207" id="Seg_6928" s="T206">kämpfen-PRS-3PL</ta>
            <ta e="T208" id="Seg_6929" s="T207">dieses</ta>
            <ta e="T209" id="Seg_6930" s="T208">Mensch-PL.[NOM]</ta>
            <ta e="T210" id="Seg_6931" s="T209">Tschopotschuka.[NOM]</ta>
            <ta e="T211" id="Seg_6932" s="T210">was.für.ein-3SG-3SG-ACC</ta>
            <ta e="T212" id="Seg_6933" s="T211">Mensch-VBZ-FUT.[3SG]=Q</ta>
            <ta e="T213" id="Seg_6934" s="T212">auf.links.drehen-CVB.SIM</ta>
            <ta e="T214" id="Seg_6935" s="T213">herunterziehen-PST2.[3SG]</ta>
            <ta e="T215" id="Seg_6936" s="T214">und</ta>
            <ta e="T216" id="Seg_6937" s="T215">töten-CVB.SEQ</ta>
            <ta e="T217" id="Seg_6938" s="T216">werfen-PST2.[3SG]</ta>
            <ta e="T218" id="Seg_6939" s="T217">jener-ACC</ta>
            <ta e="T219" id="Seg_6940" s="T218">Haut.abziehen-PST2.[3SG]</ta>
            <ta e="T220" id="Seg_6941" s="T219">und</ta>
            <ta e="T221" id="Seg_6942" s="T220">Tasche-3SG-DAT/LOC</ta>
            <ta e="T222" id="Seg_6943" s="T221">Tasche.[NOM]</ta>
            <ta e="T223" id="Seg_6944" s="T222">EMPH</ta>
            <ta e="T224" id="Seg_6945" s="T223">groß</ta>
            <ta e="T225" id="Seg_6946" s="T224">sich.wundern-PST1-1SG</ta>
            <ta e="T226" id="Seg_6947" s="T225">Fuchs-3SG-ACC</ta>
            <ta e="T227" id="Seg_6948" s="T226">hineintun-CVB.SEQ</ta>
            <ta e="T228" id="Seg_6949" s="T227">werfen-PST2.[3SG]</ta>
            <ta e="T229" id="Seg_6950" s="T228">Tasche-3SG-DAT/LOC</ta>
            <ta e="T230" id="Seg_6951" s="T229">dann</ta>
            <ta e="T231" id="Seg_6952" s="T230">go-CVB.SEQ</ta>
            <ta e="T232" id="Seg_6953" s="T231">gehen-PST2.[3SG]</ta>
            <ta e="T233" id="Seg_6954" s="T232">go-CVB.SEQ</ta>
            <ta e="T234" id="Seg_6955" s="T233">gehen-CVB.SIM</ta>
            <ta e="T235" id="Seg_6956" s="T234">gehen-TEMP-3SG</ta>
            <ta e="T236" id="Seg_6957" s="T235">Vielfraß.[NOM]</ta>
            <ta e="T237" id="Seg_6958" s="T236">aufstehen-CVB.SIM</ta>
            <ta e="T238" id="Seg_6959" s="T237">springen-PST2.[3SG]</ta>
            <ta e="T239" id="Seg_6960" s="T238">na</ta>
            <ta e="T240" id="Seg_6961" s="T239">Tschopotschuka.[NOM]</ta>
            <ta e="T241" id="Seg_6962" s="T240">Mensch-ACC</ta>
            <ta e="T242" id="Seg_6963" s="T241">erschrecken-CAUS-PST1-2SG</ta>
            <ta e="T243" id="Seg_6964" s="T242">gerade</ta>
            <ta e="T244" id="Seg_6965" s="T243">sagen-PST2.[3SG]</ta>
            <ta e="T245" id="Seg_6966" s="T244">warum</ta>
            <ta e="T246" id="Seg_6967" s="T245">gehen-PRS-2SG</ta>
            <ta e="T247" id="Seg_6968" s="T246">dieses</ta>
            <ta e="T248" id="Seg_6969" s="T247">sagen-CVB.SEQ</ta>
            <ta e="T249" id="Seg_6970" s="T248">Land-DAT/LOC</ta>
            <ta e="T250" id="Seg_6971" s="T249">pa</ta>
            <ta e="T251" id="Seg_6972" s="T250">nun</ta>
            <ta e="T252" id="Seg_6973" s="T251">Fisch-1SG-ACC</ta>
            <ta e="T253" id="Seg_6974" s="T252">warum</ta>
            <ta e="T254" id="Seg_6975" s="T253">ganz-3SG-ACC</ta>
            <ta e="T255" id="Seg_6976" s="T254">essen-PST2-2PL=Q</ta>
            <ta e="T256" id="Seg_6977" s="T255">jenes-ACC</ta>
            <ta e="T257" id="Seg_6978" s="T256">folgen-CVB.SIM</ta>
            <ta e="T258" id="Seg_6979" s="T257">gehen-PRS-1SG</ta>
            <ta e="T259" id="Seg_6980" s="T258">sagen-PST2.[3SG]</ta>
            <ta e="T260" id="Seg_6981" s="T259">noch</ta>
            <ta e="T261" id="Seg_6982" s="T260">essen-PST2-2PL=Q</ta>
            <ta e="T262" id="Seg_6983" s="T261">fangen-CVB.SEQ</ta>
            <ta e="T263" id="Seg_6984" s="T262">nehmen-PRS.[3SG]</ta>
            <ta e="T264" id="Seg_6985" s="T263">und</ta>
            <ta e="T265" id="Seg_6986" s="T264">jener-ACC</ta>
            <ta e="T266" id="Seg_6987" s="T265">doch</ta>
            <ta e="T267" id="Seg_6988" s="T266">kämpfen-PRS.[3SG]</ta>
            <ta e="T268" id="Seg_6989" s="T267">kämpfen-CVB.SEQ</ta>
            <ta e="T269" id="Seg_6990" s="T268">Tschopotschuka.[NOM]</ta>
            <ta e="T270" id="Seg_6991" s="T269">was-3SG-ACC</ta>
            <ta e="T271" id="Seg_6992" s="T270">Mensch-VBZ-FUT.[3SG]=Q</ta>
            <ta e="T272" id="Seg_6993" s="T271">Mensch-VBZ-PST2.NEG.[3SG]</ta>
            <ta e="T273" id="Seg_6994" s="T272">töten-PST2.[3SG]</ta>
            <ta e="T274" id="Seg_6995" s="T273">und</ta>
            <ta e="T275" id="Seg_6996" s="T274">auch</ta>
            <ta e="T276" id="Seg_6997" s="T275">Haut.abziehen-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_6998" s="T276">und</ta>
            <ta e="T278" id="Seg_6999" s="T277">Tasche-3SG-DAT/LOC</ta>
            <ta e="T279" id="Seg_7000" s="T278">hineintun-CVB.SEQ</ta>
            <ta e="T280" id="Seg_7001" s="T279">werfen-PST2.[3SG]</ta>
            <ta e="T281" id="Seg_7002" s="T280">wieder</ta>
            <ta e="T282" id="Seg_7003" s="T281">go-CVB.SEQ</ta>
            <ta e="T283" id="Seg_7004" s="T282">gehen-PST2.[3SG]</ta>
            <ta e="T284" id="Seg_7005" s="T283">go-CVB.SEQ-go-CVB.SEQ</ta>
            <ta e="T285" id="Seg_7006" s="T284">Wolf-ACC</ta>
            <ta e="T286" id="Seg_7007" s="T285">treffen-EP-PST2.[3SG]</ta>
            <ta e="T287" id="Seg_7008" s="T286">Wolf-3SG.[NOM]</ta>
            <ta e="T288" id="Seg_7009" s="T287">aufstehen-CVB.SIM</ta>
            <ta e="T289" id="Seg_7010" s="T288">springen-PST2.[3SG]</ta>
            <ta e="T290" id="Seg_7011" s="T289">oh</ta>
            <ta e="T291" id="Seg_7012" s="T290">Tschopotschuka.[NOM]</ta>
            <ta e="T292" id="Seg_7013" s="T291">Mensch-ACC</ta>
            <ta e="T293" id="Seg_7014" s="T292">erschrecken-CAUS-PST1-2SG</ta>
            <ta e="T294" id="Seg_7015" s="T293">nun</ta>
            <ta e="T295" id="Seg_7016" s="T294">wohin</ta>
            <ta e="T296" id="Seg_7017" s="T295">gehen-PRS-2SG</ta>
            <ta e="T297" id="Seg_7018" s="T296">sagen-PST2.[3SG]</ta>
            <ta e="T298" id="Seg_7019" s="T297">na</ta>
            <ta e="T299" id="Seg_7020" s="T298">Fisch-1SG-ACC</ta>
            <ta e="T300" id="Seg_7021" s="T299">warum</ta>
            <ta e="T301" id="Seg_7022" s="T300">essen-PST2-2PL=Q</ta>
            <ta e="T302" id="Seg_7023" s="T301">jenes-3SG-1SG-ACC</ta>
            <ta e="T303" id="Seg_7024" s="T302">folgen-PRS-1SG</ta>
            <ta e="T304" id="Seg_7025" s="T303">pa</ta>
            <ta e="T305" id="Seg_7026" s="T304">2SG.[NOM]</ta>
            <ta e="T306" id="Seg_7027" s="T305">Fisch-2SG-DAT/LOC</ta>
            <ta e="T307" id="Seg_7028" s="T306">sich.nähern-PST2.NEG-1PL</ta>
            <ta e="T308" id="Seg_7029" s="T307">EMPH</ta>
            <ta e="T309" id="Seg_7030" s="T308">dafür</ta>
            <ta e="T310" id="Seg_7031" s="T309">1SG.[NOM]</ta>
            <ta e="T311" id="Seg_7032" s="T310">2SG-ACC</ta>
            <ta e="T312" id="Seg_7033" s="T311">essen-CVB.SEQ</ta>
            <ta e="T313" id="Seg_7034" s="T312">werfen-FUT-1SG</ta>
            <ta e="T314" id="Seg_7035" s="T313">sagen-PST2.[3SG]</ta>
            <ta e="T315" id="Seg_7036" s="T314">jener-ACC</ta>
            <ta e="T316" id="Seg_7037" s="T315">in.den.Mund.nehmen-CVB.SEQ</ta>
            <ta e="T317" id="Seg_7038" s="T316">werfen-PST2.[3SG]</ta>
            <ta e="T318" id="Seg_7039" s="T317">verschlingen-CVB.SEQ</ta>
            <ta e="T319" id="Seg_7040" s="T318">werfen-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_7041" s="T319">Tschopotschuka-ACC</ta>
            <ta e="T321" id="Seg_7042" s="T320">Tschopotschuka.[NOM]</ta>
            <ta e="T322" id="Seg_7043" s="T321">Bauch-3SG-ACC</ta>
            <ta e="T323" id="Seg_7044" s="T322">spalten-CVB.SIM</ta>
            <ta e="T324" id="Seg_7045" s="T323">herunterziehen-PRS.[3SG]</ta>
            <ta e="T325" id="Seg_7046" s="T324">und</ta>
            <ta e="T326" id="Seg_7047" s="T325">hinausgehen-CVB.SIM</ta>
            <ta e="T327" id="Seg_7048" s="T326">rennen-PRS.[3SG]</ta>
            <ta e="T328" id="Seg_7049" s="T327">so</ta>
            <ta e="T329" id="Seg_7050" s="T328">machen-PST2.[3SG]</ta>
            <ta e="T330" id="Seg_7051" s="T329">und</ta>
            <ta e="T331" id="Seg_7052" s="T330">Haut.abziehen-PST2.[3SG]</ta>
            <ta e="T332" id="Seg_7053" s="T331">und</ta>
            <ta e="T333" id="Seg_7054" s="T332">Tasche-3SG-DAT/LOC</ta>
            <ta e="T334" id="Seg_7055" s="T333">hineintun-CVB.SEQ</ta>
            <ta e="T335" id="Seg_7056" s="T334">werfen-PST2.[3SG]</ta>
            <ta e="T336" id="Seg_7057" s="T335">dann</ta>
            <ta e="T337" id="Seg_7058" s="T336">doch</ta>
            <ta e="T338" id="Seg_7059" s="T337">go-CVB.SEQ</ta>
            <ta e="T339" id="Seg_7060" s="T338">gehen-PST2.[3SG]</ta>
            <ta e="T340" id="Seg_7061" s="T339">go-CVB.SEQ</ta>
            <ta e="T341" id="Seg_7062" s="T340">gehen-PST2.[3SG]</ta>
            <ta e="T342" id="Seg_7063" s="T341">oh</ta>
            <ta e="T343" id="Seg_7064" s="T342">Bär.[NOM]</ta>
            <ta e="T344" id="Seg_7065" s="T343">aufstehen-CVB.SIM</ta>
            <ta e="T345" id="Seg_7066" s="T344">springen-EMOT-PST2.[3SG]</ta>
            <ta e="T346" id="Seg_7067" s="T345">EMPH</ta>
            <ta e="T347" id="Seg_7068" s="T346">EMPH</ta>
            <ta e="T348" id="Seg_7069" s="T347">nur</ta>
            <ta e="T349" id="Seg_7070" s="T348">EXCL</ta>
            <ta e="T350" id="Seg_7071" s="T349">Tschopotschuka.[NOM]</ta>
            <ta e="T351" id="Seg_7072" s="T350">warum</ta>
            <ta e="T352" id="Seg_7073" s="T351">was.für.ein</ta>
            <ta e="T353" id="Seg_7074" s="T352">Ort-DAT/LOC</ta>
            <ta e="T354" id="Seg_7075" s="T353">gehen-PRS-2SG</ta>
            <ta e="T355" id="Seg_7076" s="T354">dieses</ta>
            <ta e="T356" id="Seg_7077" s="T355">sagen-PST2.[3SG]</ta>
            <ta e="T357" id="Seg_7078" s="T356">Fisch-1SG-ACC</ta>
            <ta e="T358" id="Seg_7079" s="T357">warum</ta>
            <ta e="T359" id="Seg_7080" s="T358">stehlen-PST2-2PL=Q</ta>
            <ta e="T360" id="Seg_7081" s="T359">noch</ta>
            <ta e="T361" id="Seg_7082" s="T360">Fisch-1SG-ACC</ta>
            <ta e="T362" id="Seg_7083" s="T361">stehlen-PST2-2PL</ta>
            <ta e="T364" id="Seg_7084" s="T362">essen</ta>
            <ta e="T365" id="Seg_7085" s="T364">1SG.[NOM]</ta>
            <ta e="T366" id="Seg_7086" s="T365">2SG-ACC</ta>
            <ta e="T367" id="Seg_7087" s="T366">in.den.Mund.nehmen-CVB.SEQ</ta>
            <ta e="T368" id="Seg_7088" s="T367">werfen-FUT-1SG</ta>
            <ta e="T369" id="Seg_7089" s="T368">sagen-PST2.[3SG]</ta>
            <ta e="T370" id="Seg_7090" s="T369">essen-CVB.SEQ</ta>
            <ta e="T371" id="Seg_7091" s="T370">werfen-PST2.[3SG]</ta>
            <ta e="T372" id="Seg_7092" s="T371">Bauch-3SG-ACC</ta>
            <ta e="T373" id="Seg_7093" s="T372">spalten-CVB.SIM</ta>
            <ta e="T374" id="Seg_7094" s="T373">herunterziehen-PRS.[3SG]</ta>
            <ta e="T375" id="Seg_7095" s="T374">und</ta>
            <ta e="T376" id="Seg_7096" s="T375">hinausgehen-CVB.SIM</ta>
            <ta e="T377" id="Seg_7097" s="T376">rennen-PRS.[3SG]</ta>
            <ta e="T378" id="Seg_7098" s="T377">dieses</ta>
            <ta e="T379" id="Seg_7099" s="T378">Mensch.[NOM]</ta>
            <ta e="T380" id="Seg_7100" s="T379">so</ta>
            <ta e="T381" id="Seg_7101" s="T380">nachdem</ta>
            <ta e="T382" id="Seg_7102" s="T381">Messer-3SG-INSTR</ta>
            <ta e="T383" id="Seg_7103" s="T382">Haut.abziehen-CVB.SEQ</ta>
            <ta e="T384" id="Seg_7104" s="T383">nachdem</ta>
            <ta e="T385" id="Seg_7105" s="T384">doch</ta>
            <ta e="T386" id="Seg_7106" s="T385">leben-PST2.[3SG]</ta>
            <ta e="T387" id="Seg_7107" s="T386">ganz</ta>
            <ta e="T388" id="Seg_7108" s="T387">dieses</ta>
            <ta e="T389" id="Seg_7109" s="T388">leben-CVB.SEQ</ta>
            <ta e="T390" id="Seg_7110" s="T389">denken-VBZ-PST2.[3SG]</ta>
            <ta e="T391" id="Seg_7111" s="T390">dieses</ta>
            <ta e="T392" id="Seg_7112" s="T391">Zar.[NOM]</ta>
            <ta e="T393" id="Seg_7113" s="T392">Tochter-3SG-ACC</ta>
            <ta e="T394" id="Seg_7114" s="T393">suchen-CVB.SIM</ta>
            <ta e="T395" id="Seg_7115" s="T394">gehen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T396" id="Seg_7116" s="T395">Zar.[NOM]</ta>
            <ta e="T397" id="Seg_7117" s="T396">einsam</ta>
            <ta e="T398" id="Seg_7118" s="T397">Tochter-PROPR.[NOM]</ta>
            <ta e="T399" id="Seg_7119" s="T398">sagen-PST2-3PL</ta>
            <ta e="T400" id="Seg_7120" s="T399">jenes-ACC</ta>
            <ta e="T401" id="Seg_7121" s="T400">suchen-CVB.SIM</ta>
            <ta e="T402" id="Seg_7122" s="T401">gehen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T403" id="Seg_7123" s="T402">man.muss</ta>
            <ta e="T404" id="Seg_7124" s="T403">so.viel-PROPR</ta>
            <ta e="T405" id="Seg_7125" s="T404">was-PROPR</ta>
            <ta e="T406" id="Seg_7126" s="T405">Mensch.[NOM]</ta>
            <ta e="T407" id="Seg_7127" s="T406">nun</ta>
            <ta e="T408" id="Seg_7128" s="T407">denken-PST2.[3SG]</ta>
            <ta e="T409" id="Seg_7129" s="T408">doch</ta>
            <ta e="T410" id="Seg_7130" s="T409">Zar-DAT/LOC</ta>
            <ta e="T411" id="Seg_7131" s="T410">gehen-CVB.PURP</ta>
            <ta e="T412" id="Seg_7132" s="T411">jenes</ta>
            <ta e="T413" id="Seg_7133" s="T412">sitzen-PRS.[3SG]</ta>
            <ta e="T414" id="Seg_7134" s="T413">dieses</ta>
            <ta e="T415" id="Seg_7135" s="T414">Mensch-2SG.[NOM]</ta>
            <ta e="T416" id="Seg_7136" s="T415">sitzen-PRS.[3SG]</ta>
            <ta e="T417" id="Seg_7137" s="T416">doch</ta>
            <ta e="T418" id="Seg_7138" s="T417">go-PTCP.FUT-DAT/LOC</ta>
            <ta e="T419" id="Seg_7139" s="T418">gut.[NOM]</ta>
            <ta e="T420" id="Seg_7140" s="T419">doch</ta>
            <ta e="T421" id="Seg_7141" s="T420">go-PRS.[3SG]</ta>
            <ta e="T422" id="Seg_7142" s="T421">dieses</ta>
            <ta e="T423" id="Seg_7143" s="T422">Mensch.[NOM]</ta>
            <ta e="T424" id="Seg_7144" s="T423">go-CVB.SEQ-go-CVB.SEQ</ta>
            <ta e="T425" id="Seg_7145" s="T424">eins</ta>
            <ta e="T426" id="Seg_7146" s="T425">Ort-DAT/LOC</ta>
            <ta e="T427" id="Seg_7147" s="T426">leben-PST2.[3SG]</ta>
            <ta e="T428" id="Seg_7148" s="T427">leben-CVB.SEQ</ta>
            <ta e="T429" id="Seg_7149" s="T428">schauen.auf-PST2-3SG</ta>
            <ta e="T430" id="Seg_7150" s="T429">Zar.[NOM]</ta>
            <ta e="T431" id="Seg_7151" s="T430">eigen-3SG</ta>
            <ta e="T432" id="Seg_7152" s="T431">Veranda-3SG.[NOM]</ta>
            <ta e="T433" id="Seg_7153" s="T432">zu.sehen.sein-PRS.[3SG]</ta>
            <ta e="T434" id="Seg_7154" s="T433">plötzlich</ta>
            <ta e="T435" id="Seg_7155" s="T434">ankommen-CVB.SEQ-EP-1SG</ta>
            <ta e="T436" id="Seg_7156" s="T435">dort</ta>
            <ta e="T437" id="Seg_7157" s="T436">dieses</ta>
            <ta e="T438" id="Seg_7158" s="T437">Mensch.[NOM]</ta>
            <ta e="T439" id="Seg_7159" s="T438">go-CVB.SEQ-go-CVB.SEQ</ta>
            <ta e="T440" id="Seg_7160" s="T439">go-CVB.SEQ-go-CVB.SEQ</ta>
            <ta e="T441" id="Seg_7161" s="T440">Stadt-DAT/LOC</ta>
            <ta e="T442" id="Seg_7162" s="T441">kommen-PST2.[3SG]</ta>
            <ta e="T443" id="Seg_7163" s="T442">Zar.[NOM]</ta>
            <ta e="T444" id="Seg_7164" s="T443">eigen-3SG-DAT/LOC</ta>
            <ta e="T445" id="Seg_7165" s="T444">kommen-PST2-3SG</ta>
            <ta e="T446" id="Seg_7166" s="T445">meist</ta>
            <ta e="T447" id="Seg_7167" s="T446">Rand.[NOM]</ta>
            <ta e="T448" id="Seg_7168" s="T447">zu</ta>
            <ta e="T449" id="Seg_7169" s="T448">schlecht</ta>
            <ta e="T450" id="Seg_7170" s="T449">sehr</ta>
            <ta e="T451" id="Seg_7171" s="T450">Rauch-PROPR</ta>
            <ta e="T452" id="Seg_7172" s="T451">Erdhütte.[NOM]</ta>
            <ta e="T453" id="Seg_7173" s="T452">stehen-PRS.[3SG]</ta>
            <ta e="T454" id="Seg_7174" s="T453">Erdhütte.[NOM]</ta>
            <ta e="T455" id="Seg_7175" s="T454">stehen-PRS.[3SG]</ta>
            <ta e="T456" id="Seg_7176" s="T455">nur</ta>
            <ta e="T457" id="Seg_7177" s="T456">dieses-DAT/LOC</ta>
            <ta e="T458" id="Seg_7178" s="T457">hineingehen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T459" id="Seg_7179" s="T458">gut.[NOM]</ta>
            <ta e="T460" id="Seg_7180" s="T459">Erdhütte-DAT/LOC</ta>
            <ta e="T461" id="Seg_7181" s="T460">Erdhütte-DAT/LOC</ta>
            <ta e="T462" id="Seg_7182" s="T461">rennen-CVB.SEQ</ta>
            <ta e="T463" id="Seg_7183" s="T462">fallen-PST2-3SG</ta>
            <ta e="T464" id="Seg_7184" s="T463">alter.Mann-PROPR</ta>
            <ta e="T465" id="Seg_7185" s="T464">Alte.[NOM]</ta>
            <ta e="T466" id="Seg_7186" s="T465">Alte-3SG.[NOM]</ta>
            <ta e="T467" id="Seg_7187" s="T466">Auge-POSS</ta>
            <ta e="T468" id="Seg_7188" s="T467">NEG.[3SG]</ta>
            <ta e="T469" id="Seg_7189" s="T468">alter.Mann-3SG.[NOM]</ta>
            <ta e="T470" id="Seg_7190" s="T469">ein.Bisschen</ta>
            <ta e="T471" id="Seg_7191" s="T470">ernsthaft.[NOM]</ta>
            <ta e="T472" id="Seg_7192" s="T471">EMPH</ta>
            <ta e="T473" id="Seg_7193" s="T472">doch</ta>
            <ta e="T474" id="Seg_7194" s="T473">essen-EP-CAUS-PRS-3PL</ta>
            <ta e="T475" id="Seg_7195" s="T474">dieses-ACC</ta>
            <ta e="T476" id="Seg_7196" s="T475">essen-PRS.[3SG]</ta>
            <ta e="T477" id="Seg_7197" s="T476">dieses</ta>
            <ta e="T478" id="Seg_7198" s="T477">Gast.[NOM]</ta>
            <ta e="T479" id="Seg_7199" s="T478">was.für.ein-was.für.ein</ta>
            <ta e="T480" id="Seg_7200" s="T479">Ort-ABL</ta>
            <ta e="T481" id="Seg_7201" s="T480">kommen-PRS-2SG</ta>
            <ta e="T482" id="Seg_7202" s="T481">sagen-CVB.SEQ</ta>
            <ta e="T483" id="Seg_7203" s="T482">Neuigkeit.[NOM]</ta>
            <ta e="T484" id="Seg_7204" s="T483">fragen-PRS-3PL</ta>
            <ta e="T485" id="Seg_7205" s="T484">1SG.[NOM]</ta>
            <ta e="T487" id="Seg_7206" s="T486">fünf</ta>
            <ta e="T488" id="Seg_7207" s="T487">Stein-PROPR</ta>
            <ta e="T489" id="Seg_7208" s="T488">Karausche-Netz-PROPR.[NOM]</ta>
            <ta e="T490" id="Seg_7209" s="T489">sein-PST1-1SG</ta>
            <ta e="T491" id="Seg_7210" s="T490">jenes-3SG-1SG-ABL</ta>
            <ta e="T492" id="Seg_7211" s="T491">Fisch-ACC</ta>
            <ta e="T493" id="Seg_7212" s="T492">sammeln-PTCP.PRS-1SG-ACC</ta>
            <ta e="T494" id="Seg_7213" s="T493">stehlen-PASS-CVB.SEQ-1SG</ta>
            <ta e="T495" id="Seg_7214" s="T494">unglücklich-PL-ACC</ta>
            <ta e="T496" id="Seg_7215" s="T495">töten-ITER-CVB.SEQ</ta>
            <ta e="T497" id="Seg_7216" s="T496">kommen-PRS-1SG</ta>
            <ta e="T498" id="Seg_7217" s="T497">sagen-PST2.[3SG]</ta>
            <ta e="T499" id="Seg_7218" s="T498">ach</ta>
            <ta e="T502" id="Seg_7219" s="T501">unglücklich-PL-ACC</ta>
            <ta e="T503" id="Seg_7220" s="T502">töten-CVB.SEQ</ta>
            <ta e="T504" id="Seg_7221" s="T503">kommen-PST2.[3SG]</ta>
            <ta e="T505" id="Seg_7222" s="T504">dieses</ta>
            <ta e="T506" id="Seg_7223" s="T505">alter.Mann-ABL</ta>
            <ta e="T507" id="Seg_7224" s="T506">fragen-PRS.[3SG]</ta>
            <ta e="T508" id="Seg_7225" s="T507">Zar.[NOM]</ta>
            <ta e="T509" id="Seg_7226" s="T508">Tochter-PROPR.[NOM]</ta>
            <ta e="T510" id="Seg_7227" s="T509">Q</ta>
            <ta e="T511" id="Seg_7228" s="T510">sagen-CVB.SEQ</ta>
            <ta e="T512" id="Seg_7229" s="T511">EMPH-einsam</ta>
            <ta e="T513" id="Seg_7230" s="T512">Tochter-PROPR.[NOM]</ta>
            <ta e="T514" id="Seg_7231" s="T513">wohin</ta>
            <ta e="T515" id="Seg_7232" s="T514">INDEF</ta>
            <ta e="T517" id="Seg_7233" s="T516">Ehemann-DAT/LOC</ta>
            <ta e="T518" id="Seg_7234" s="T517">geben-PRS-3PL</ta>
            <ta e="T519" id="Seg_7235" s="T518">man.sagt</ta>
            <ta e="T520" id="Seg_7236" s="T519">sagen-CVB.SEQ</ta>
            <ta e="T521" id="Seg_7237" s="T520">alter.Mann.[NOM]</ta>
            <ta e="T522" id="Seg_7238" s="T521">2SG.[NOM]</ta>
            <ta e="T523" id="Seg_7239" s="T522">Brautwerber-DAT/LOC</ta>
            <ta e="T524" id="Seg_7240" s="T523">gehen-FUT-2SG</ta>
            <ta e="T525" id="Seg_7241" s="T524">NEG</ta>
            <ta e="T526" id="Seg_7242" s="T525">Q</ta>
            <ta e="T527" id="Seg_7243" s="T526">sagen-PST2.[3SG]</ta>
            <ta e="T528" id="Seg_7244" s="T527">kürzlich.[NOM]</ta>
            <ta e="T529" id="Seg_7245" s="T528">Gast-3SG.[NOM]</ta>
            <ta e="T530" id="Seg_7246" s="T529">in.jedem.Fall</ta>
            <ta e="T531" id="Seg_7247" s="T530">fragen-CVB.SEQ</ta>
            <ta e="T532" id="Seg_7248" s="T531">versuchen-FUT-1PL</ta>
            <ta e="T533" id="Seg_7249" s="T532">sagen-PST2.[3SG]</ta>
            <ta e="T534" id="Seg_7250" s="T533">eins</ta>
            <ta e="T535" id="Seg_7251" s="T534">Gast.[NOM]</ta>
            <ta e="T536" id="Seg_7252" s="T535">kommen-PST2.[3SG]</ta>
            <ta e="T537" id="Seg_7253" s="T536">Tochter-2SG-ACC</ta>
            <ta e="T538" id="Seg_7254" s="T537">bitten-PRS.[3SG]</ta>
            <ta e="T539" id="Seg_7255" s="T538">sagen-CVB.SEQ-2SG</ta>
            <ta e="T540" id="Seg_7256" s="T539">sprechen.[IMP.2SG]</ta>
            <ta e="T541" id="Seg_7257" s="T540">sagen-PST2.[3SG]</ta>
            <ta e="T542" id="Seg_7258" s="T541">dieses</ta>
            <ta e="T543" id="Seg_7259" s="T542">Mensch.[NOM]</ta>
            <ta e="T544" id="Seg_7260" s="T543">gehen-CVB.SEQ</ta>
            <ta e="T545" id="Seg_7261" s="T544">bleiben-PST2.[3SG]</ta>
            <ta e="T546" id="Seg_7262" s="T545">Zar.[NOM]</ta>
            <ta e="T547" id="Seg_7263" s="T546">rennen-CVB.SEQ</ta>
            <ta e="T548" id="Seg_7264" s="T547">fallen-PST2.[3SG]</ta>
            <ta e="T549" id="Seg_7265" s="T548">na</ta>
            <ta e="T550" id="Seg_7266" s="T549">warum</ta>
            <ta e="T551" id="Seg_7267" s="T550">kommen-PRS-2SG</ta>
            <ta e="T552" id="Seg_7268" s="T551">nein</ta>
            <ta e="T553" id="Seg_7269" s="T552">1SG.[NOM]</ta>
            <ta e="T554" id="Seg_7270" s="T553">Gast-PROPR-1SG</ta>
            <ta e="T555" id="Seg_7271" s="T554">was.für.ein-was.für.ein</ta>
            <ta e="T556" id="Seg_7272" s="T555">Ort-ABL</ta>
            <ta e="T557" id="Seg_7273" s="T556">kommen-PST2-3SG</ta>
            <ta e="T558" id="Seg_7274" s="T557">MOD</ta>
            <ta e="T559" id="Seg_7275" s="T558">wissen-NEG-1SG</ta>
            <ta e="T560" id="Seg_7276" s="T559">jenes</ta>
            <ta e="T561" id="Seg_7277" s="T560">Mensch.[NOM]</ta>
            <ta e="T562" id="Seg_7278" s="T561">Tochter-2SG-ACC</ta>
            <ta e="T563" id="Seg_7279" s="T562">bitten-CVB.SEQ</ta>
            <ta e="T564" id="Seg_7280" s="T563">sein-PRS.[3SG]</ta>
            <ta e="T565" id="Seg_7281" s="T564">sagen-PST2.[3SG]</ta>
            <ta e="T566" id="Seg_7282" s="T565">so</ta>
            <ta e="T567" id="Seg_7283" s="T566">machen-CVB.SEQ</ta>
            <ta e="T568" id="Seg_7284" s="T567">jenes</ta>
            <ta e="T569" id="Seg_7285" s="T568">Mensch.[NOM]</ta>
            <ta e="T570" id="Seg_7286" s="T569">eigen-3SG-GEN</ta>
            <ta e="T571" id="Seg_7287" s="T570">Gesicht-3SG-ACC-Auge-3SG-ACC</ta>
            <ta e="T572" id="Seg_7288" s="T571">sehen-PTCP.FUT-1SG-ACC</ta>
            <ta e="T573" id="Seg_7289" s="T572">hierher</ta>
            <ta e="T574" id="Seg_7290" s="T573">bringen-EP-IMP.2PL</ta>
            <ta e="T575" id="Seg_7291" s="T574">sagen-PST2.[3SG]</ta>
            <ta e="T576" id="Seg_7292" s="T575">Zar-3SG.[NOM]</ta>
            <ta e="T577" id="Seg_7293" s="T576">dieses</ta>
            <ta e="T578" id="Seg_7294" s="T577">alter.Mann.[NOM]</ta>
            <ta e="T579" id="Seg_7295" s="T578">kürzlich</ta>
            <ta e="T580" id="Seg_7296" s="T579">Gast-3SG-ACC</ta>
            <ta e="T581" id="Seg_7297" s="T580">bringen-PST2.[3SG]</ta>
            <ta e="T582" id="Seg_7298" s="T581">pa</ta>
            <ta e="T583" id="Seg_7299" s="T582">1SG.[NOM]</ta>
            <ta e="T584" id="Seg_7300" s="T583">solch</ta>
            <ta e="T585" id="Seg_7301" s="T584">Mensch-DAT/LOC</ta>
            <ta e="T586" id="Seg_7302" s="T585">geben-NEG-1SG</ta>
            <ta e="T587" id="Seg_7303" s="T586">Arm-3SG-ACC-Bein-3SG-ACC</ta>
            <ta e="T588" id="Seg_7304" s="T587">brechen-FREQ-CVB.SEQ</ta>
            <ta e="T589" id="Seg_7305" s="T588">nachdem</ta>
            <ta e="T590" id="Seg_7306" s="T589">Gefängnis-DAT/LOC</ta>
            <ta e="T591" id="Seg_7307" s="T590">werfen-EP-IMP.2PL</ta>
            <ta e="T592" id="Seg_7308" s="T591">sagen-PST2.[3SG]</ta>
            <ta e="T593" id="Seg_7309" s="T592">ach</ta>
            <ta e="T594" id="Seg_7310" s="T593">dieses</ta>
            <ta e="T595" id="Seg_7311" s="T594">Mensch-2SG-GEN</ta>
            <ta e="T596" id="Seg_7312" s="T595">Arm-3SG-ACC-Bein-3SG-ACC</ta>
            <ta e="T597" id="Seg_7313" s="T596">brechen-PST2.NEG-3PL</ta>
            <ta e="T598" id="Seg_7314" s="T597">Schnur-INSTR</ta>
            <ta e="T599" id="Seg_7315" s="T598">anbinden-CVB.SEQ</ta>
            <ta e="T600" id="Seg_7316" s="T599">gehen-CVB.SEQ-3PL</ta>
            <ta e="T601" id="Seg_7317" s="T600">Gefängnis-DAT/LOC</ta>
            <ta e="T602" id="Seg_7318" s="T601">stecken-CVB.SEQ</ta>
            <ta e="T603" id="Seg_7319" s="T602">werfen-PST2-3PL</ta>
            <ta e="T604" id="Seg_7320" s="T603">vor.Kurzem</ta>
            <ta e="T605" id="Seg_7321" s="T604">Mensch-2SG-ACC</ta>
            <ta e="T607" id="Seg_7322" s="T606">dieses</ta>
            <ta e="T608" id="Seg_7323" s="T607">Mensch.[NOM]</ta>
            <ta e="T609" id="Seg_7324" s="T608">Maus.[NOM]</ta>
            <ta e="T610" id="Seg_7325" s="T609">werden-CVB.SEQ</ta>
            <ta e="T611" id="Seg_7326" s="T610">hinausgehen-CVB.SIM</ta>
            <ta e="T612" id="Seg_7327" s="T611">rennen-PST2.[3SG]</ta>
            <ta e="T613" id="Seg_7328" s="T612">hinausgehen-CVB.SIM</ta>
            <ta e="T614" id="Seg_7329" s="T613">rennen-CVB.SEQ</ta>
            <ta e="T615" id="Seg_7330" s="T614">alter.Mann-3SG-DAT/LOC</ta>
            <ta e="T616" id="Seg_7331" s="T615">wieder</ta>
            <ta e="T617" id="Seg_7332" s="T616">hineingehen-PST2.[3SG]</ta>
            <ta e="T618" id="Seg_7333" s="T617">doch</ta>
            <ta e="T619" id="Seg_7334" s="T618">wissen-NEG-1SG</ta>
            <ta e="T620" id="Seg_7335" s="T619">kürzlich-EP-COMP</ta>
            <ta e="T621" id="Seg_7336" s="T620">anders</ta>
            <ta e="T622" id="Seg_7337" s="T621">Mensch.[NOM]</ta>
            <ta e="T623" id="Seg_7338" s="T622">kommen-PST2-3SG</ta>
            <ta e="T624" id="Seg_7339" s="T623">Q</ta>
            <ta e="T625" id="Seg_7340" s="T624">was.[NOM]</ta>
            <ta e="T626" id="Seg_7341" s="T625">Q</ta>
            <ta e="T627" id="Seg_7342" s="T626">wieder</ta>
            <ta e="T628" id="Seg_7343" s="T627">Tochter-2SG-ACC</ta>
            <ta e="T629" id="Seg_7344" s="T628">bitten-CVB.SEQ</ta>
            <ta e="T630" id="Seg_7345" s="T629">sein-PRS.[3SG]</ta>
            <ta e="T631" id="Seg_7346" s="T630">sagen-CVB.SEQ-2SG</ta>
            <ta e="T632" id="Seg_7347" s="T631">gehen.[IMP.2SG]</ta>
            <ta e="T633" id="Seg_7348" s="T632">doch</ta>
            <ta e="T634" id="Seg_7349" s="T633">sagen-PST2.[3SG]</ta>
            <ta e="T635" id="Seg_7350" s="T634">wieder</ta>
            <ta e="T636" id="Seg_7351" s="T635">gehen-PST2.[3SG]</ta>
            <ta e="T637" id="Seg_7352" s="T636">alter.Mann.[NOM]</ta>
            <ta e="T638" id="Seg_7353" s="T637">doch</ta>
            <ta e="T639" id="Seg_7354" s="T638">jenes</ta>
            <ta e="T640" id="Seg_7355" s="T639">nachdem</ta>
            <ta e="T641" id="Seg_7356" s="T640">wieder</ta>
            <ta e="T642" id="Seg_7357" s="T641">sagen-PST2.[3SG]</ta>
            <ta e="T643" id="Seg_7358" s="T642">na</ta>
            <ta e="T644" id="Seg_7359" s="T643">wieder</ta>
            <ta e="T645" id="Seg_7360" s="T644">Gast.[NOM]</ta>
            <ta e="T646" id="Seg_7361" s="T645">kommen-PST1-3SG</ta>
            <ta e="T647" id="Seg_7362" s="T646">Tochter-2SG-ACC</ta>
            <ta e="T648" id="Seg_7363" s="T647">bitten-CVB.SEQ</ta>
            <ta e="T649" id="Seg_7364" s="T648">sein-PRS.[3SG]</ta>
            <ta e="T650" id="Seg_7365" s="T649">sagen-PST2.[3SG]</ta>
            <ta e="T651" id="Seg_7366" s="T650">Freund-PL.[NOM]</ta>
            <ta e="T652" id="Seg_7367" s="T651">vor.Kurzem</ta>
            <ta e="T653" id="Seg_7368" s="T652">Gefängnis-DAT/LOC</ta>
            <ta e="T654" id="Seg_7369" s="T653">werfen-PTCP.HAB</ta>
            <ta e="T655" id="Seg_7370" s="T654">Mensch-2PL-ACC</ta>
            <ta e="T656" id="Seg_7371" s="T655">sehen-EP-IMP.2PL</ta>
            <ta e="T657" id="Seg_7372" s="T656">doch</ta>
            <ta e="T658" id="Seg_7373" s="T657">wie-wie</ta>
            <ta e="T659" id="Seg_7374" s="T658">Mensch-ACC</ta>
            <ta e="T660" id="Seg_7375" s="T659">quälen-PRS-1PL</ta>
            <ta e="T661" id="Seg_7376" s="T660">MOD</ta>
            <ta e="T662" id="Seg_7377" s="T661">sagen-PST2.[3SG]</ta>
            <ta e="T663" id="Seg_7378" s="T662">Gefängnis-DAT/LOC</ta>
            <ta e="T664" id="Seg_7379" s="T663">werfen-PTCP.PST</ta>
            <ta e="T665" id="Seg_7380" s="T664">Mensch-3SG.[NOM]</ta>
            <ta e="T666" id="Seg_7381" s="T665">Spur-3SG.[NOM]</ta>
            <ta e="T667" id="Seg_7382" s="T666">nur</ta>
            <ta e="T668" id="Seg_7383" s="T667">nicht.gerade.sein-CVB.SIM</ta>
            <ta e="T669" id="Seg_7384" s="T668">liegen-PRS.[3SG]</ta>
            <ta e="T670" id="Seg_7385" s="T669">anbinden-PTCP.PST</ta>
            <ta e="T671" id="Seg_7386" s="T670">Schnur-3SG.[NOM]</ta>
            <ta e="T672" id="Seg_7387" s="T671">Schnur-3SG-INSTR</ta>
            <ta e="T673" id="Seg_7388" s="T672">liegen-PRS.[3SG]</ta>
            <ta e="T674" id="Seg_7389" s="T673">doch</ta>
            <ta e="T675" id="Seg_7390" s="T674">na</ta>
            <ta e="T676" id="Seg_7391" s="T675">solch-solch</ta>
            <ta e="T677" id="Seg_7392" s="T676">na</ta>
            <ta e="T678" id="Seg_7393" s="T677">oh</ta>
            <ta e="T679" id="Seg_7394" s="T678">doch</ta>
            <ta e="T680" id="Seg_7395" s="T679">tatsächlich</ta>
            <ta e="T681" id="Seg_7396" s="T680">EMPH</ta>
            <ta e="T682" id="Seg_7397" s="T681">gut.[NOM]</ta>
            <ta e="T683" id="Seg_7398" s="T682">Mensch-ACC</ta>
            <ta e="T684" id="Seg_7399" s="T683">machen-PST1-1SG</ta>
            <ta e="T685" id="Seg_7400" s="T684">EMPH</ta>
            <ta e="T686" id="Seg_7401" s="T685">wohl</ta>
            <ta e="T687" id="Seg_7402" s="T686">Tochter-1SG-ACC</ta>
            <ta e="T688" id="Seg_7403" s="T687">geben-PRS-1SG</ta>
            <ta e="T689" id="Seg_7404" s="T688">sagen-FUT-1SG</ta>
            <ta e="T690" id="Seg_7405" s="T689">MOD</ta>
            <ta e="T691" id="Seg_7406" s="T690">dieses</ta>
            <ta e="T692" id="Seg_7407" s="T691">Mensch.[NOM]</ta>
            <ta e="T693" id="Seg_7408" s="T692">jener-PROPR</ta>
            <ta e="T694" id="Seg_7409" s="T693">unglücklich</ta>
            <ta e="T695" id="Seg_7410" s="T694">töten-PTCP.PST-3SG-ACC</ta>
            <ta e="T696" id="Seg_7411" s="T695">Zar.[NOM]</ta>
            <ta e="T697" id="Seg_7412" s="T696">Knie-3SG-DAT/LOC</ta>
            <ta e="T698" id="Seg_7413" s="T697">ausgießen-CVB.SIM</ta>
            <ta e="T699" id="Seg_7414" s="T698">werfen-PST2.[3SG]</ta>
            <ta e="T700" id="Seg_7415" s="T699">so</ta>
            <ta e="T701" id="Seg_7416" s="T700">machen-CVB.SEQ</ta>
            <ta e="T702" id="Seg_7417" s="T701">Rentierkarawane-3SG-GEN</ta>
            <ta e="T703" id="Seg_7418" s="T702">Hinterteil-3SG-DAT/LOC</ta>
            <ta e="T704" id="Seg_7419" s="T703">Vögelchen.[NOM]</ta>
            <ta e="T705" id="Seg_7420" s="T704">singen-PRS.[3SG]</ta>
            <ta e="T706" id="Seg_7421" s="T705">%%</ta>
            <ta e="T707" id="Seg_7422" s="T706">trennen-CVB.SEQ</ta>
            <ta e="T708" id="Seg_7423" s="T707">bringen-PST2.[3SG]</ta>
            <ta e="T709" id="Seg_7424" s="T708">dieses</ta>
            <ta e="T710" id="Seg_7425" s="T709">Mensch.[NOM]</ta>
            <ta e="T711" id="Seg_7426" s="T710">unterschiedlich-unterschiedlich</ta>
            <ta e="T712" id="Seg_7427" s="T711">Soldat-PROPR-was-PROPR</ta>
            <ta e="T713" id="Seg_7428" s="T712">dieses</ta>
            <ta e="T714" id="Seg_7429" s="T713">Mensch.[NOM]</ta>
            <ta e="T715" id="Seg_7430" s="T714">doch</ta>
            <ta e="T716" id="Seg_7431" s="T715">gehen-PRS.[3SG]</ta>
            <ta e="T717" id="Seg_7432" s="T716">Haus-3SG-DAT/LOC</ta>
            <ta e="T718" id="Seg_7433" s="T717">Tochter-3SG-ACC</ta>
            <ta e="T719" id="Seg_7434" s="T718">nehmen-CVB.SEQ</ta>
            <ta e="T720" id="Seg_7435" s="T719">nachdem</ta>
            <ta e="T721" id="Seg_7436" s="T720">doch</ta>
            <ta e="T722" id="Seg_7437" s="T721">nomadisieren-PRS.[3SG]</ta>
            <ta e="T723" id="Seg_7438" s="T722">nomadisieren-CVB.SEQ</ta>
            <ta e="T724" id="Seg_7439" s="T723">dieses.[NOM]</ta>
            <ta e="T725" id="Seg_7440" s="T724">kommen-CVB.SEQ-3PL</ta>
            <ta e="T726" id="Seg_7441" s="T725">dieses.[NOM]</ta>
            <ta e="T727" id="Seg_7442" s="T726">doch</ta>
            <ta e="T728" id="Seg_7443" s="T727">dieses.[NOM]</ta>
            <ta e="T729" id="Seg_7444" s="T728">reich.sein-CVB.SEQ-sich.satt.essen-CVB.SEQ</ta>
            <ta e="T730" id="Seg_7445" s="T729">leben-PST2-3SG</ta>
            <ta e="T731" id="Seg_7446" s="T730">doch</ta>
            <ta e="T732" id="Seg_7447" s="T731">dieses.[NOM]</ta>
            <ta e="T733" id="Seg_7448" s="T732">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_7449" s="T1">Чопочука.[NOM]</ta>
            <ta e="T3" id="Seg_7450" s="T2">жить-PST2.[3SG]</ta>
            <ta e="T4" id="Seg_7451" s="T3">говорят</ta>
            <ta e="T5" id="Seg_7452" s="T4">пять</ta>
            <ta e="T6" id="Seg_7453" s="T5">дерево</ta>
            <ta e="T7" id="Seg_7454" s="T6">ставить-PTCP.PST</ta>
            <ta e="T8" id="Seg_7455" s="T7">четырехугольный.чум.[NOM]</ta>
            <ta e="T9" id="Seg_7456" s="T8">дом-PROPR.[NOM]</ta>
            <ta e="T10" id="Seg_7457" s="T9">пять</ta>
            <ta e="T11" id="Seg_7458" s="T10">камень-PROPR</ta>
            <ta e="T12" id="Seg_7459" s="T11">карась-сеть-PROPR.[NOM]</ta>
            <ta e="T13" id="Seg_7460" s="T12">день.[NOM]</ta>
            <ta e="T14" id="Seg_7461" s="T13">каждый</ta>
            <ta e="T15" id="Seg_7462" s="T14">сеть-VBZ-CVB.SIM</ta>
            <ta e="T16" id="Seg_7463" s="T15">входить-PRS.[3SG]</ta>
            <ta e="T17" id="Seg_7464" s="T16">этот</ta>
            <ta e="T18" id="Seg_7465" s="T17">человек.[NOM]</ta>
            <ta e="T19" id="Seg_7466" s="T18">рыба-ACC</ta>
            <ta e="T20" id="Seg_7467" s="T19">возвышаться-ADVZ</ta>
            <ta e="T21" id="Seg_7468" s="T20">собирать-PST2.[3SG]</ta>
            <ta e="T22" id="Seg_7469" s="T21">сеть-3SG-ABL</ta>
            <ta e="T23" id="Seg_7470" s="T22">так</ta>
            <ta e="T24" id="Seg_7471" s="T23">делать-CVB.SEQ</ta>
            <ta e="T25" id="Seg_7472" s="T24">однажды</ta>
            <ta e="T26" id="Seg_7473" s="T25">идти-PST2-3SG</ta>
            <ta e="T27" id="Seg_7474" s="T26">песец-PL.[NOM]</ta>
            <ta e="T28" id="Seg_7475" s="T27">что-PL.[NOM]</ta>
            <ta e="T29" id="Seg_7476" s="T28">есть-PST2-3PL</ta>
            <ta e="T30" id="Seg_7477" s="T29">вдруг</ta>
            <ta e="T31" id="Seg_7478" s="T30">рыба-3SG-ACC</ta>
            <ta e="T32" id="Seg_7479" s="T31">этот-ACC</ta>
            <ta e="T33" id="Seg_7480" s="T32">следовать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T34" id="Seg_7481" s="T33">след-3SG-ACC</ta>
            <ta e="T35" id="Seg_7482" s="T34">следовать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T36" id="Seg_7483" s="T35">рыба-3SG-ACC</ta>
            <ta e="T37" id="Seg_7484" s="T36">дом-3SG-DAT/LOC</ta>
            <ta e="T38" id="Seg_7485" s="T37">носить-CVB.SEQ</ta>
            <ta e="T39" id="Seg_7486" s="T38">после</ta>
            <ta e="T40" id="Seg_7487" s="T39">этот</ta>
            <ta e="T41" id="Seg_7488" s="T40">человек.[NOM]</ta>
            <ta e="T42" id="Seg_7489" s="T41">песец.[NOM]</ta>
            <ta e="T43" id="Seg_7490" s="T42">след-3SG-ACC</ta>
            <ta e="T44" id="Seg_7491" s="T43">следовать-PST2.[3SG]</ta>
            <ta e="T45" id="Seg_7492" s="T44">песец.[NOM]</ta>
            <ta e="T46" id="Seg_7493" s="T45">след-3SG-ACC</ta>
            <ta e="T47" id="Seg_7494" s="T46">следовать-CVB.SEQ</ta>
            <ta e="T48" id="Seg_7495" s="T47">идти-TEMP-3SG</ta>
            <ta e="T49" id="Seg_7496" s="T48">эй</ta>
            <ta e="T50" id="Seg_7497" s="T49">песец.[NOM]</ta>
            <ta e="T51" id="Seg_7498" s="T50">что-ACC</ta>
            <ta e="T52" id="Seg_7499" s="T51">и</ta>
            <ta e="T53" id="Seg_7500" s="T52">один</ta>
            <ta e="T54" id="Seg_7501" s="T53">куст.[NOM]</ta>
            <ta e="T55" id="Seg_7502" s="T54">нутро-3SG-ABL</ta>
            <ta e="T56" id="Seg_7503" s="T55">вставать-CVB.SIM</ta>
            <ta e="T57" id="Seg_7504" s="T56">прыгать-PST2.[3SG]</ta>
            <ta e="T58" id="Seg_7505" s="T57">вот.беда</ta>
            <ta e="T59" id="Seg_7506" s="T58">Чопочука.[NOM]</ta>
            <ta e="T60" id="Seg_7507" s="T59">человек-ACC</ta>
            <ta e="T61" id="Seg_7508" s="T60">испугаться-CAUS-PST1-2SG</ta>
            <ta e="T62" id="Seg_7509" s="T61">эй</ta>
            <ta e="T63" id="Seg_7510" s="T62">как</ta>
            <ta e="T64" id="Seg_7511" s="T63">приходить-PRS-2SG</ta>
            <ta e="T65" id="Seg_7512" s="T64">эй</ta>
            <ta e="T66" id="Seg_7513" s="T65">рыба-1SG-ACC</ta>
            <ta e="T67" id="Seg_7514" s="T66">вот</ta>
            <ta e="T68" id="Seg_7515" s="T67">почему</ta>
            <ta e="T69" id="Seg_7516" s="T68">красть-PST2-2PL=Q</ta>
            <ta e="T70" id="Seg_7517" s="T69">2PL.[NOM]</ta>
            <ta e="T71" id="Seg_7518" s="T70">хо</ta>
            <ta e="T72" id="Seg_7519" s="T71">1SG.[NOM]</ta>
            <ta e="T73" id="Seg_7520" s="T72">красть-PST2-EP-1SG</ta>
            <ta e="T74" id="Seg_7521" s="T73">Q</ta>
            <ta e="T75" id="Seg_7522" s="T74">говорить-CVB.SIM-говорить-CVB.SIM</ta>
            <ta e="T76" id="Seg_7523" s="T75">делать-PST2.[3SG]</ta>
            <ta e="T77" id="Seg_7524" s="T76">этот-3SG-2SG.[NOM]</ta>
            <ta e="T78" id="Seg_7525" s="T77">мышь.[NOM]</ta>
            <ta e="T79" id="Seg_7526" s="T78">становиться-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_7527" s="T79">мышь.[NOM]</ta>
            <ta e="T81" id="Seg_7528" s="T80">становиться-CVB.SEQ</ta>
            <ta e="T82" id="Seg_7529" s="T81">вот</ta>
            <ta e="T83" id="Seg_7530" s="T82">старик-EP-2SG.[NOM]</ta>
            <ta e="T84" id="Seg_7531" s="T83">вот</ta>
            <ta e="T85" id="Seg_7532" s="T84">идти-PRS.[3SG]</ta>
            <ta e="T86" id="Seg_7533" s="T85">тот-ACC</ta>
            <ta e="T87" id="Seg_7534" s="T86">брать.в.рот-CVB.SEQ</ta>
            <ta e="T88" id="Seg_7535" s="T87">бросать-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_7536" s="T88">песец-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_7537" s="T89">тот-2SG.[NOM]</ta>
            <ta e="T91" id="Seg_7538" s="T90">живот-3SG-ACC</ta>
            <ta e="T92" id="Seg_7539" s="T91">эй</ta>
            <ta e="T95" id="Seg_7540" s="T94">перевешивать-CVB.SEQ</ta>
            <ta e="T96" id="Seg_7541" s="T95">резать-CVB.SEQ</ta>
            <ta e="T97" id="Seg_7542" s="T96">выйти-CVB.SIM</ta>
            <ta e="T100" id="Seg_7543" s="T99">выйти-CVB.SIM</ta>
            <ta e="T101" id="Seg_7544" s="T100">бежать-PST2.[3SG]</ta>
            <ta e="T102" id="Seg_7545" s="T101">так</ta>
            <ta e="T103" id="Seg_7546" s="T102">после</ta>
            <ta e="T104" id="Seg_7547" s="T103">тот-ACC</ta>
            <ta e="T105" id="Seg_7548" s="T104">сдирать.кожу-PRS.[3SG]</ta>
            <ta e="T106" id="Seg_7549" s="T105">да</ta>
            <ta e="T107" id="Seg_7550" s="T106">карман-3SG-DAT/LOC</ta>
            <ta e="T108" id="Seg_7551" s="T107">вкладывать-PRS.[3SG]</ta>
            <ta e="T109" id="Seg_7552" s="T108">да</ta>
            <ta e="T110" id="Seg_7553" s="T109">идти-CVB.SEQ</ta>
            <ta e="T111" id="Seg_7554" s="T110">идти-PST2.[3SG]</ta>
            <ta e="T112" id="Seg_7555" s="T111">опять</ta>
            <ta e="T113" id="Seg_7556" s="T112">передняя.часть.[NOM]</ta>
            <ta e="T114" id="Seg_7557" s="T113">к</ta>
            <ta e="T115" id="Seg_7558" s="T114">идти-CVB.SEQ</ta>
            <ta e="T116" id="Seg_7559" s="T115">идти-CVB.SEQ</ta>
            <ta e="T117" id="Seg_7560" s="T116">заяц.[NOM]</ta>
            <ta e="T118" id="Seg_7561" s="T117">ставить-PST2.[3SG]</ta>
            <ta e="T119" id="Seg_7562" s="T118">вот.беда</ta>
            <ta e="T120" id="Seg_7563" s="T119">Чопочука.[NOM]</ta>
            <ta e="T121" id="Seg_7564" s="T120">что.[NOM]</ta>
            <ta e="T122" id="Seg_7565" s="T121">какой</ta>
            <ta e="T123" id="Seg_7566" s="T122">место-ABL</ta>
            <ta e="T124" id="Seg_7567" s="T123">приходить-PRS-2SG</ta>
            <ta e="T125" id="Seg_7568" s="T124">человек-ACC</ta>
            <ta e="T126" id="Seg_7569" s="T125">испугаться-CAUS-EMOT-PST1-2SG</ta>
            <ta e="T127" id="Seg_7570" s="T126">говорить-PST2.[3SG]</ta>
            <ta e="T128" id="Seg_7571" s="T127">эй</ta>
            <ta e="T129" id="Seg_7572" s="T128">рыба-1SG-ACC</ta>
            <ta e="T130" id="Seg_7573" s="T129">почему</ta>
            <ta e="T131" id="Seg_7574" s="T130">целый-3SG-ACC</ta>
            <ta e="T132" id="Seg_7575" s="T131">есть-PST2-2PL=Q</ta>
            <ta e="T133" id="Seg_7576" s="T132">рыба-1SG-ACC</ta>
            <ta e="T134" id="Seg_7577" s="T133">следовать-CVB.SIM</ta>
            <ta e="T135" id="Seg_7578" s="T134">идти-PRS-1SG</ta>
            <ta e="T138" id="Seg_7579" s="T136">тот.[NOM]</ta>
            <ta e="T139" id="Seg_7580" s="T138">тоже</ta>
            <ta e="T141" id="Seg_7581" s="T139">эй</ta>
            <ta e="T142" id="Seg_7582" s="T141">вот</ta>
            <ta e="T143" id="Seg_7583" s="T142">вот</ta>
            <ta e="T144" id="Seg_7584" s="T143">побить-PRS-3PL</ta>
            <ta e="T145" id="Seg_7585" s="T144">э</ta>
            <ta e="T146" id="Seg_7586" s="T145">заяц-ACC</ta>
            <ta e="T147" id="Seg_7587" s="T146">с</ta>
            <ta e="T148" id="Seg_7588" s="T147">побить-CVB.SEQ-3PL</ta>
            <ta e="T149" id="Seg_7589" s="T148">заяц-ACC</ta>
            <ta e="T150" id="Seg_7590" s="T149">что-3SG-ACC</ta>
            <ta e="T151" id="Seg_7591" s="T150">человек-VBZ-FUT.[3SG]=Q</ta>
            <ta e="T152" id="Seg_7592" s="T151">тот</ta>
            <ta e="T153" id="Seg_7593" s="T152">человек.[NOM]</ta>
            <ta e="T154" id="Seg_7594" s="T153">тот-ACC</ta>
            <ta e="T155" id="Seg_7595" s="T154">убить-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_7596" s="T155">да</ta>
            <ta e="T157" id="Seg_7597" s="T156">опять</ta>
            <ta e="T159" id="Seg_7598" s="T158">карман-3SG-DAT/LOC</ta>
            <ta e="T160" id="Seg_7599" s="T159">вкладывать-CVB.SEQ</ta>
            <ta e="T161" id="Seg_7600" s="T160">бросать-PST2.[3SG]</ta>
            <ta e="T162" id="Seg_7601" s="T161">сдирать.кожу-EP-MED-CVB.SEQ</ta>
            <ta e="T163" id="Seg_7602" s="T162">опять</ta>
            <ta e="T164" id="Seg_7603" s="T163">идти-CVB.SEQ</ta>
            <ta e="T165" id="Seg_7604" s="T164">идти-PST2.[3SG]</ta>
            <ta e="T166" id="Seg_7605" s="T165">идти-CVB.SEQ-идти-CVB.SEQ</ta>
            <ta e="T167" id="Seg_7606" s="T166">идти-TEMP-3SG</ta>
            <ta e="T168" id="Seg_7607" s="T167">лиса.[NOM]</ta>
            <ta e="T169" id="Seg_7608" s="T168">вставать-CVB.SIM</ta>
            <ta e="T170" id="Seg_7609" s="T169">прыгать-PST2.[3SG]</ta>
            <ta e="T171" id="Seg_7610" s="T170">о</ta>
            <ta e="T172" id="Seg_7611" s="T171">Чопочука.[NOM]</ta>
            <ta e="T173" id="Seg_7612" s="T172">человек-ACC</ta>
            <ta e="T174" id="Seg_7613" s="T173">испугаться-CAUS-PST1-2SG</ta>
            <ta e="T175" id="Seg_7614" s="T174">какой</ta>
            <ta e="T176" id="Seg_7615" s="T175">какой</ta>
            <ta e="T177" id="Seg_7616" s="T176">место-ABL</ta>
            <ta e="T178" id="Seg_7617" s="T177">приходить-PRS-2SG</ta>
            <ta e="T179" id="Seg_7618" s="T178">этот</ta>
            <ta e="T180" id="Seg_7619" s="T179">говорить-PST2.[3SG]</ta>
            <ta e="T181" id="Seg_7620" s="T180">эй</ta>
            <ta e="T182" id="Seg_7621" s="T181">говорить-PST2.[3SG]</ta>
            <ta e="T183" id="Seg_7622" s="T182">вот</ta>
            <ta e="T184" id="Seg_7623" s="T183">рыба-1SG-ACC</ta>
            <ta e="T185" id="Seg_7624" s="T184">целый-3SG-ACC</ta>
            <ta e="T186" id="Seg_7625" s="T185">есть-PST2-2PL</ta>
            <ta e="T187" id="Seg_7626" s="T186">тот.[NOM]</ta>
            <ta e="T188" id="Seg_7627" s="T187">из_за</ta>
            <ta e="T189" id="Seg_7628" s="T188">следовать-CVB.SEQ</ta>
            <ta e="T190" id="Seg_7629" s="T189">приходить-PRS-1SG</ta>
            <ta e="T191" id="Seg_7630" s="T190">па</ta>
            <ta e="T192" id="Seg_7631" s="T191">2SG.[NOM]</ta>
            <ta e="T193" id="Seg_7632" s="T192">рыба-2SG-DAT/LOC</ta>
            <ta e="T194" id="Seg_7633" s="T193">1PL.[NOM]</ta>
            <ta e="T195" id="Seg_7634" s="T194">приближаться-PST2.NEG-1PL</ta>
            <ta e="T196" id="Seg_7635" s="T195">EMPH</ta>
            <ta e="T197" id="Seg_7636" s="T196">еще</ta>
            <ta e="T198" id="Seg_7637" s="T197">зато</ta>
            <ta e="T199" id="Seg_7638" s="T198">1SG.[NOM]</ta>
            <ta e="T200" id="Seg_7639" s="T199">2SG-ACC</ta>
            <ta e="T201" id="Seg_7640" s="T200">рвать-CVB.SIM</ta>
            <ta e="T202" id="Seg_7641" s="T201">трогать-CVB.SEQ</ta>
            <ta e="T203" id="Seg_7642" s="T202">есть-CVB.SEQ</ta>
            <ta e="T204" id="Seg_7643" s="T203">бросать-FUT-1SG</ta>
            <ta e="T205" id="Seg_7644" s="T204">говорить-PST2.[3SG]</ta>
            <ta e="T206" id="Seg_7645" s="T205">вот</ta>
            <ta e="T207" id="Seg_7646" s="T206">побить-PRS-3PL</ta>
            <ta e="T208" id="Seg_7647" s="T207">этот</ta>
            <ta e="T209" id="Seg_7648" s="T208">человек-PL.[NOM]</ta>
            <ta e="T210" id="Seg_7649" s="T209">Чопочука.[NOM]</ta>
            <ta e="T211" id="Seg_7650" s="T210">какой-3SG-3SG-ACC</ta>
            <ta e="T212" id="Seg_7651" s="T211">человек-VBZ-FUT.[3SG]=Q</ta>
            <ta e="T213" id="Seg_7652" s="T212">вертеть.наизнанку-CVB.SIM</ta>
            <ta e="T214" id="Seg_7653" s="T213">перевешивать-PST2.[3SG]</ta>
            <ta e="T215" id="Seg_7654" s="T214">да</ta>
            <ta e="T216" id="Seg_7655" s="T215">убить-CVB.SEQ</ta>
            <ta e="T217" id="Seg_7656" s="T216">бросать-PST2.[3SG]</ta>
            <ta e="T218" id="Seg_7657" s="T217">тот-ACC</ta>
            <ta e="T219" id="Seg_7658" s="T218">сдирать.кожу-PST2.[3SG]</ta>
            <ta e="T220" id="Seg_7659" s="T219">да</ta>
            <ta e="T221" id="Seg_7660" s="T220">карман-3SG-DAT/LOC</ta>
            <ta e="T222" id="Seg_7661" s="T221">карман.[NOM]</ta>
            <ta e="T223" id="Seg_7662" s="T222">EMPH</ta>
            <ta e="T224" id="Seg_7663" s="T223">большой</ta>
            <ta e="T225" id="Seg_7664" s="T224">удивляться-PST1-1SG</ta>
            <ta e="T226" id="Seg_7665" s="T225">лиса-3SG-ACC</ta>
            <ta e="T227" id="Seg_7666" s="T226">вкладывать-CVB.SEQ</ta>
            <ta e="T228" id="Seg_7667" s="T227">бросать-PST2.[3SG]</ta>
            <ta e="T229" id="Seg_7668" s="T228">карман-3SG-DAT/LOC</ta>
            <ta e="T230" id="Seg_7669" s="T229">потом</ta>
            <ta e="T231" id="Seg_7670" s="T230">идти-CVB.SEQ</ta>
            <ta e="T232" id="Seg_7671" s="T231">идти-PST2.[3SG]</ta>
            <ta e="T233" id="Seg_7672" s="T232">идти-CVB.SEQ</ta>
            <ta e="T234" id="Seg_7673" s="T233">идти-CVB.SIM</ta>
            <ta e="T235" id="Seg_7674" s="T234">идти-TEMP-3SG</ta>
            <ta e="T236" id="Seg_7675" s="T235">росомаха.[NOM]</ta>
            <ta e="T237" id="Seg_7676" s="T236">вставать-CVB.SIM</ta>
            <ta e="T238" id="Seg_7677" s="T237">прыгать-PST2.[3SG]</ta>
            <ta e="T239" id="Seg_7678" s="T238">эй</ta>
            <ta e="T240" id="Seg_7679" s="T239">Чопочука.[NOM]</ta>
            <ta e="T241" id="Seg_7680" s="T240">человек-ACC</ta>
            <ta e="T242" id="Seg_7681" s="T241">испугаться-CAUS-PST1-2SG</ta>
            <ta e="T243" id="Seg_7682" s="T242">как.раз</ta>
            <ta e="T244" id="Seg_7683" s="T243">говорить-PST2.[3SG]</ta>
            <ta e="T245" id="Seg_7684" s="T244">почему</ta>
            <ta e="T246" id="Seg_7685" s="T245">идти-PRS-2SG</ta>
            <ta e="T247" id="Seg_7686" s="T246">этот</ta>
            <ta e="T248" id="Seg_7687" s="T247">говорить-CVB.SEQ</ta>
            <ta e="T249" id="Seg_7688" s="T248">страна-DAT/LOC</ta>
            <ta e="T250" id="Seg_7689" s="T249">па</ta>
            <ta e="T251" id="Seg_7690" s="T250">вот</ta>
            <ta e="T252" id="Seg_7691" s="T251">рыба-1SG-ACC</ta>
            <ta e="T253" id="Seg_7692" s="T252">почему</ta>
            <ta e="T254" id="Seg_7693" s="T253">целый-3SG-ACC</ta>
            <ta e="T255" id="Seg_7694" s="T254">есть-PST2-2PL=Q</ta>
            <ta e="T256" id="Seg_7695" s="T255">тот-ACC</ta>
            <ta e="T257" id="Seg_7696" s="T256">следовать-CVB.SIM</ta>
            <ta e="T258" id="Seg_7697" s="T257">идти-PRS-1SG</ta>
            <ta e="T259" id="Seg_7698" s="T258">говорить-PST2.[3SG]</ta>
            <ta e="T260" id="Seg_7699" s="T259">еще</ta>
            <ta e="T261" id="Seg_7700" s="T260">есть-PST2-2PL=Q</ta>
            <ta e="T262" id="Seg_7701" s="T261">поймать-CVB.SEQ</ta>
            <ta e="T263" id="Seg_7702" s="T262">взять-PRS.[3SG]</ta>
            <ta e="T264" id="Seg_7703" s="T263">да</ta>
            <ta e="T265" id="Seg_7704" s="T264">тот-ACC</ta>
            <ta e="T266" id="Seg_7705" s="T265">вот</ta>
            <ta e="T267" id="Seg_7706" s="T266">побить-PRS.[3SG]</ta>
            <ta e="T268" id="Seg_7707" s="T267">побить-CVB.SEQ</ta>
            <ta e="T269" id="Seg_7708" s="T268">Чопочука.[NOM]</ta>
            <ta e="T270" id="Seg_7709" s="T269">что-3SG-ACC</ta>
            <ta e="T271" id="Seg_7710" s="T270">человек-VBZ-FUT.[3SG]=Q</ta>
            <ta e="T272" id="Seg_7711" s="T271">человек-VBZ-PST2.NEG.[3SG]</ta>
            <ta e="T273" id="Seg_7712" s="T272">убить-PST2.[3SG]</ta>
            <ta e="T274" id="Seg_7713" s="T273">да</ta>
            <ta e="T275" id="Seg_7714" s="T274">тоже</ta>
            <ta e="T276" id="Seg_7715" s="T275">сдирать.кожу-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_7716" s="T276">да</ta>
            <ta e="T278" id="Seg_7717" s="T277">карман-3SG-DAT/LOC</ta>
            <ta e="T279" id="Seg_7718" s="T278">вкладывать-CVB.SEQ</ta>
            <ta e="T280" id="Seg_7719" s="T279">бросать-PST2.[3SG]</ta>
            <ta e="T281" id="Seg_7720" s="T280">опять</ta>
            <ta e="T282" id="Seg_7721" s="T281">идти-CVB.SEQ</ta>
            <ta e="T283" id="Seg_7722" s="T282">идти-PST2.[3SG]</ta>
            <ta e="T284" id="Seg_7723" s="T283">идти-CVB.SEQ-идти-CVB.SEQ</ta>
            <ta e="T285" id="Seg_7724" s="T284">волк-ACC</ta>
            <ta e="T286" id="Seg_7725" s="T285">встречать-EP-PST2.[3SG]</ta>
            <ta e="T287" id="Seg_7726" s="T286">волк-3SG.[NOM]</ta>
            <ta e="T288" id="Seg_7727" s="T287">вставать-CVB.SIM</ta>
            <ta e="T289" id="Seg_7728" s="T288">прыгать-PST2.[3SG]</ta>
            <ta e="T290" id="Seg_7729" s="T289">о</ta>
            <ta e="T291" id="Seg_7730" s="T290">Чопочука.[NOM]</ta>
            <ta e="T292" id="Seg_7731" s="T291">человек-ACC</ta>
            <ta e="T293" id="Seg_7732" s="T292">испугаться-CAUS-PST1-2SG</ta>
            <ta e="T294" id="Seg_7733" s="T293">вот</ta>
            <ta e="T295" id="Seg_7734" s="T294">куда</ta>
            <ta e="T296" id="Seg_7735" s="T295">идти-PRS-2SG</ta>
            <ta e="T297" id="Seg_7736" s="T296">говорить-PST2.[3SG]</ta>
            <ta e="T298" id="Seg_7737" s="T297">эй</ta>
            <ta e="T299" id="Seg_7738" s="T298">рыба-1SG-ACC</ta>
            <ta e="T300" id="Seg_7739" s="T299">почему</ta>
            <ta e="T301" id="Seg_7740" s="T300">есть-PST2-2PL=Q</ta>
            <ta e="T302" id="Seg_7741" s="T301">тот-3SG-1SG-ACC</ta>
            <ta e="T303" id="Seg_7742" s="T302">следовать-PRS-1SG</ta>
            <ta e="T304" id="Seg_7743" s="T303">па</ta>
            <ta e="T305" id="Seg_7744" s="T304">2SG.[NOM]</ta>
            <ta e="T306" id="Seg_7745" s="T305">рыба-2SG-DAT/LOC</ta>
            <ta e="T307" id="Seg_7746" s="T306">приближаться-PST2.NEG-1PL</ta>
            <ta e="T308" id="Seg_7747" s="T307">EMPH</ta>
            <ta e="T309" id="Seg_7748" s="T308">зато</ta>
            <ta e="T310" id="Seg_7749" s="T309">1SG.[NOM]</ta>
            <ta e="T311" id="Seg_7750" s="T310">2SG-ACC</ta>
            <ta e="T312" id="Seg_7751" s="T311">есть-CVB.SEQ</ta>
            <ta e="T313" id="Seg_7752" s="T312">бросать-FUT-1SG</ta>
            <ta e="T314" id="Seg_7753" s="T313">говорить-PST2.[3SG]</ta>
            <ta e="T315" id="Seg_7754" s="T314">тот-ACC</ta>
            <ta e="T316" id="Seg_7755" s="T315">брать.в.рот-CVB.SEQ</ta>
            <ta e="T317" id="Seg_7756" s="T316">бросать-PST2.[3SG]</ta>
            <ta e="T318" id="Seg_7757" s="T317">проглотить-CVB.SEQ</ta>
            <ta e="T319" id="Seg_7758" s="T318">бросать-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_7759" s="T319">Чопочука-ACC</ta>
            <ta e="T321" id="Seg_7760" s="T320">Чопочука.[NOM]</ta>
            <ta e="T322" id="Seg_7761" s="T321">живот-3SG-ACC</ta>
            <ta e="T323" id="Seg_7762" s="T322">колоть-CVB.SIM</ta>
            <ta e="T324" id="Seg_7763" s="T323">перевешивать-PRS.[3SG]</ta>
            <ta e="T325" id="Seg_7764" s="T324">да</ta>
            <ta e="T326" id="Seg_7765" s="T325">выйти-CVB.SIM</ta>
            <ta e="T327" id="Seg_7766" s="T326">бежать-PRS.[3SG]</ta>
            <ta e="T328" id="Seg_7767" s="T327">так</ta>
            <ta e="T329" id="Seg_7768" s="T328">делать-PST2.[3SG]</ta>
            <ta e="T330" id="Seg_7769" s="T329">да</ta>
            <ta e="T331" id="Seg_7770" s="T330">сдирать.кожу-PST2.[3SG]</ta>
            <ta e="T332" id="Seg_7771" s="T331">да</ta>
            <ta e="T333" id="Seg_7772" s="T332">карман-3SG-DAT/LOC</ta>
            <ta e="T334" id="Seg_7773" s="T333">вкладывать-CVB.SEQ</ta>
            <ta e="T335" id="Seg_7774" s="T334">бросать-PST2.[3SG]</ta>
            <ta e="T336" id="Seg_7775" s="T335">потом</ta>
            <ta e="T337" id="Seg_7776" s="T336">вот</ta>
            <ta e="T338" id="Seg_7777" s="T337">идти-CVB.SEQ</ta>
            <ta e="T339" id="Seg_7778" s="T338">идти-PST2.[3SG]</ta>
            <ta e="T340" id="Seg_7779" s="T339">идти-CVB.SEQ</ta>
            <ta e="T341" id="Seg_7780" s="T340">идти-PST2.[3SG]</ta>
            <ta e="T342" id="Seg_7781" s="T341">о</ta>
            <ta e="T343" id="Seg_7782" s="T342">медведь.[NOM]</ta>
            <ta e="T344" id="Seg_7783" s="T343">вставать-CVB.SIM</ta>
            <ta e="T345" id="Seg_7784" s="T344">прыгать-EMOT-PST2.[3SG]</ta>
            <ta e="T346" id="Seg_7785" s="T345">EMPH</ta>
            <ta e="T347" id="Seg_7786" s="T346">EMPH</ta>
            <ta e="T348" id="Seg_7787" s="T347">только</ta>
            <ta e="T349" id="Seg_7788" s="T348">EXCL</ta>
            <ta e="T350" id="Seg_7789" s="T349">Чопочука.[NOM]</ta>
            <ta e="T351" id="Seg_7790" s="T350">почему</ta>
            <ta e="T352" id="Seg_7791" s="T351">какой</ta>
            <ta e="T353" id="Seg_7792" s="T352">место-DAT/LOC</ta>
            <ta e="T354" id="Seg_7793" s="T353">идти-PRS-2SG</ta>
            <ta e="T355" id="Seg_7794" s="T354">этот</ta>
            <ta e="T356" id="Seg_7795" s="T355">говорить-PST2.[3SG]</ta>
            <ta e="T357" id="Seg_7796" s="T356">рыба-1SG-ACC</ta>
            <ta e="T358" id="Seg_7797" s="T357">почему</ta>
            <ta e="T359" id="Seg_7798" s="T358">красть-PST2-2PL=Q</ta>
            <ta e="T360" id="Seg_7799" s="T359">еще</ta>
            <ta e="T361" id="Seg_7800" s="T360">рыба-1SG-ACC</ta>
            <ta e="T362" id="Seg_7801" s="T361">красть-PST2-2PL</ta>
            <ta e="T364" id="Seg_7802" s="T362">есть</ta>
            <ta e="T365" id="Seg_7803" s="T364">1SG.[NOM]</ta>
            <ta e="T366" id="Seg_7804" s="T365">2SG-ACC</ta>
            <ta e="T367" id="Seg_7805" s="T366">брать.в.рот-CVB.SEQ</ta>
            <ta e="T368" id="Seg_7806" s="T367">бросать-FUT-1SG</ta>
            <ta e="T369" id="Seg_7807" s="T368">говорить-PST2.[3SG]</ta>
            <ta e="T370" id="Seg_7808" s="T369">есть-CVB.SEQ</ta>
            <ta e="T371" id="Seg_7809" s="T370">бросать-PST2.[3SG]</ta>
            <ta e="T372" id="Seg_7810" s="T371">живот-3SG-ACC</ta>
            <ta e="T373" id="Seg_7811" s="T372">колоть-CVB.SIM</ta>
            <ta e="T374" id="Seg_7812" s="T373">перевешивать-PRS.[3SG]</ta>
            <ta e="T375" id="Seg_7813" s="T374">да</ta>
            <ta e="T376" id="Seg_7814" s="T375">выйти-CVB.SIM</ta>
            <ta e="T377" id="Seg_7815" s="T376">бежать-PRS.[3SG]</ta>
            <ta e="T378" id="Seg_7816" s="T377">этот</ta>
            <ta e="T379" id="Seg_7817" s="T378">человек.[NOM]</ta>
            <ta e="T380" id="Seg_7818" s="T379">так</ta>
            <ta e="T381" id="Seg_7819" s="T380">после</ta>
            <ta e="T382" id="Seg_7820" s="T381">нож-3SG-INSTR</ta>
            <ta e="T383" id="Seg_7821" s="T382">сдирать.кожу-CVB.SEQ</ta>
            <ta e="T384" id="Seg_7822" s="T383">после</ta>
            <ta e="T385" id="Seg_7823" s="T384">вот</ta>
            <ta e="T386" id="Seg_7824" s="T385">жить-PST2.[3SG]</ta>
            <ta e="T387" id="Seg_7825" s="T386">совсем</ta>
            <ta e="T388" id="Seg_7826" s="T387">этот</ta>
            <ta e="T389" id="Seg_7827" s="T388">жить-CVB.SEQ</ta>
            <ta e="T390" id="Seg_7828" s="T389">думать-VBZ-PST2.[3SG]</ta>
            <ta e="T391" id="Seg_7829" s="T390">этот</ta>
            <ta e="T392" id="Seg_7830" s="T391">царь.[NOM]</ta>
            <ta e="T393" id="Seg_7831" s="T392">дочь-3SG-ACC</ta>
            <ta e="T394" id="Seg_7832" s="T393">искать-CVB.SIM</ta>
            <ta e="T395" id="Seg_7833" s="T394">идти-PTCP.FUT-DAT/LOC</ta>
            <ta e="T396" id="Seg_7834" s="T395">царь.[NOM]</ta>
            <ta e="T397" id="Seg_7835" s="T396">одинокий</ta>
            <ta e="T398" id="Seg_7836" s="T397">дочь-PROPR.[NOM]</ta>
            <ta e="T399" id="Seg_7837" s="T398">говорить-PST2-3PL</ta>
            <ta e="T400" id="Seg_7838" s="T399">тот-ACC</ta>
            <ta e="T401" id="Seg_7839" s="T400">искать-CVB.SIM</ta>
            <ta e="T402" id="Seg_7840" s="T401">идти-PTCP.FUT-DAT/LOC</ta>
            <ta e="T403" id="Seg_7841" s="T402">надо</ta>
            <ta e="T404" id="Seg_7842" s="T403">столько-PROPR</ta>
            <ta e="T405" id="Seg_7843" s="T404">что-PROPR</ta>
            <ta e="T406" id="Seg_7844" s="T405">человек.[NOM]</ta>
            <ta e="T407" id="Seg_7845" s="T406">вот</ta>
            <ta e="T408" id="Seg_7846" s="T407">думать-PST2.[3SG]</ta>
            <ta e="T409" id="Seg_7847" s="T408">вот</ta>
            <ta e="T410" id="Seg_7848" s="T409">царь-DAT/LOC</ta>
            <ta e="T411" id="Seg_7849" s="T410">идти-CVB.PURP</ta>
            <ta e="T412" id="Seg_7850" s="T411">тот</ta>
            <ta e="T413" id="Seg_7851" s="T412">сидеть-PRS.[3SG]</ta>
            <ta e="T414" id="Seg_7852" s="T413">этот</ta>
            <ta e="T415" id="Seg_7853" s="T414">человек-2SG.[NOM]</ta>
            <ta e="T416" id="Seg_7854" s="T415">сидеть-PRS.[3SG]</ta>
            <ta e="T417" id="Seg_7855" s="T416">вот</ta>
            <ta e="T418" id="Seg_7856" s="T417">идти-PTCP.FUT-DAT/LOC</ta>
            <ta e="T419" id="Seg_7857" s="T418">хороший.[NOM]</ta>
            <ta e="T420" id="Seg_7858" s="T419">вот</ta>
            <ta e="T421" id="Seg_7859" s="T420">идти-PRS.[3SG]</ta>
            <ta e="T422" id="Seg_7860" s="T421">этот</ta>
            <ta e="T423" id="Seg_7861" s="T422">человек.[NOM]</ta>
            <ta e="T424" id="Seg_7862" s="T423">идти-CVB.SEQ-идти-CVB.SEQ</ta>
            <ta e="T425" id="Seg_7863" s="T424">один</ta>
            <ta e="T426" id="Seg_7864" s="T425">место-DAT/LOC</ta>
            <ta e="T427" id="Seg_7865" s="T426">жить-PST2.[3SG]</ta>
            <ta e="T428" id="Seg_7866" s="T427">жить-CVB.SEQ</ta>
            <ta e="T429" id="Seg_7867" s="T428">заглянуть-PST2-3SG</ta>
            <ta e="T430" id="Seg_7868" s="T429">царь.[NOM]</ta>
            <ta e="T431" id="Seg_7869" s="T430">собственный-3SG</ta>
            <ta e="T432" id="Seg_7870" s="T431">крыльцо-3SG.[NOM]</ta>
            <ta e="T433" id="Seg_7871" s="T432">быть.видно-PRS.[3SG]</ta>
            <ta e="T434" id="Seg_7872" s="T433">вдруг</ta>
            <ta e="T435" id="Seg_7873" s="T434">доезжать-CVB.SEQ-EP-1SG</ta>
            <ta e="T436" id="Seg_7874" s="T435">там</ta>
            <ta e="T437" id="Seg_7875" s="T436">этот</ta>
            <ta e="T438" id="Seg_7876" s="T437">человек.[NOM]</ta>
            <ta e="T439" id="Seg_7877" s="T438">идти-CVB.SEQ-идти-CVB.SEQ</ta>
            <ta e="T440" id="Seg_7878" s="T439">идти-CVB.SEQ-идти-CVB.SEQ</ta>
            <ta e="T441" id="Seg_7879" s="T440">город-DAT/LOC</ta>
            <ta e="T442" id="Seg_7880" s="T441">приходить-PST2.[3SG]</ta>
            <ta e="T443" id="Seg_7881" s="T442">царь.[NOM]</ta>
            <ta e="T444" id="Seg_7882" s="T443">собственный-3SG-DAT/LOC</ta>
            <ta e="T445" id="Seg_7883" s="T444">приходить-PST2-3SG</ta>
            <ta e="T446" id="Seg_7884" s="T445">самый</ta>
            <ta e="T447" id="Seg_7885" s="T446">край.[NOM]</ta>
            <ta e="T448" id="Seg_7886" s="T447">к</ta>
            <ta e="T449" id="Seg_7887" s="T448">плохой</ta>
            <ta e="T450" id="Seg_7888" s="T449">очень</ta>
            <ta e="T451" id="Seg_7889" s="T450">дым-PROPR</ta>
            <ta e="T452" id="Seg_7890" s="T451">землянка.[NOM]</ta>
            <ta e="T453" id="Seg_7891" s="T452">стоять-PRS.[3SG]</ta>
            <ta e="T454" id="Seg_7892" s="T453">землянка.[NOM]</ta>
            <ta e="T455" id="Seg_7893" s="T454">стоять-PRS.[3SG]</ta>
            <ta e="T456" id="Seg_7894" s="T455">только</ta>
            <ta e="T457" id="Seg_7895" s="T456">этот-DAT/LOC</ta>
            <ta e="T458" id="Seg_7896" s="T457">входить-PTCP.FUT-DAT/LOC</ta>
            <ta e="T459" id="Seg_7897" s="T458">хороший.[NOM]</ta>
            <ta e="T460" id="Seg_7898" s="T459">землянка-DAT/LOC</ta>
            <ta e="T461" id="Seg_7899" s="T460">землянка-DAT/LOC</ta>
            <ta e="T462" id="Seg_7900" s="T461">бежать-CVB.SEQ</ta>
            <ta e="T463" id="Seg_7901" s="T462">падать-PST2-3SG</ta>
            <ta e="T464" id="Seg_7902" s="T463">старик-PROPR</ta>
            <ta e="T465" id="Seg_7903" s="T464">старуха.[NOM]</ta>
            <ta e="T466" id="Seg_7904" s="T465">старуха-3SG.[NOM]</ta>
            <ta e="T467" id="Seg_7905" s="T466">глаз-POSS</ta>
            <ta e="T468" id="Seg_7906" s="T467">NEG.[3SG]</ta>
            <ta e="T469" id="Seg_7907" s="T468">старик-3SG.[NOM]</ta>
            <ta e="T470" id="Seg_7908" s="T469">немного</ta>
            <ta e="T471" id="Seg_7909" s="T470">серьезный.[NOM]</ta>
            <ta e="T472" id="Seg_7910" s="T471">EMPH</ta>
            <ta e="T473" id="Seg_7911" s="T472">вот</ta>
            <ta e="T474" id="Seg_7912" s="T473">есть-EP-CAUS-PRS-3PL</ta>
            <ta e="T475" id="Seg_7913" s="T474">этот-ACC</ta>
            <ta e="T476" id="Seg_7914" s="T475">есть-PRS.[3SG]</ta>
            <ta e="T477" id="Seg_7915" s="T476">этот</ta>
            <ta e="T478" id="Seg_7916" s="T477">гость.[NOM]</ta>
            <ta e="T479" id="Seg_7917" s="T478">какой-какой</ta>
            <ta e="T480" id="Seg_7918" s="T479">место-ABL</ta>
            <ta e="T481" id="Seg_7919" s="T480">приходить-PRS-2SG</ta>
            <ta e="T482" id="Seg_7920" s="T481">говорить-CVB.SEQ</ta>
            <ta e="T483" id="Seg_7921" s="T482">новость.[NOM]</ta>
            <ta e="T484" id="Seg_7922" s="T483">спрашивать-PRS-3PL</ta>
            <ta e="T485" id="Seg_7923" s="T484">1SG.[NOM]</ta>
            <ta e="T487" id="Seg_7924" s="T486">пять</ta>
            <ta e="T488" id="Seg_7925" s="T487">камень-PROPR</ta>
            <ta e="T489" id="Seg_7926" s="T488">карась-сеть-PROPR.[NOM]</ta>
            <ta e="T490" id="Seg_7927" s="T489">быть-PST1-1SG</ta>
            <ta e="T491" id="Seg_7928" s="T490">тот-3SG-1SG-ABL</ta>
            <ta e="T492" id="Seg_7929" s="T491">рыба-ACC</ta>
            <ta e="T493" id="Seg_7930" s="T492">собирать-PTCP.PRS-1SG-ACC</ta>
            <ta e="T494" id="Seg_7931" s="T493">красть-PASS-CVB.SEQ-1SG</ta>
            <ta e="T495" id="Seg_7932" s="T494">горкий-PL-ACC</ta>
            <ta e="T496" id="Seg_7933" s="T495">убить-ITER-CVB.SEQ</ta>
            <ta e="T497" id="Seg_7934" s="T496">приходить-PRS-1SG</ta>
            <ta e="T498" id="Seg_7935" s="T497">говорить-PST2.[3SG]</ta>
            <ta e="T499" id="Seg_7936" s="T498">ай</ta>
            <ta e="T502" id="Seg_7937" s="T501">горкий-PL-ACC</ta>
            <ta e="T503" id="Seg_7938" s="T502">убить-CVB.SEQ</ta>
            <ta e="T504" id="Seg_7939" s="T503">приходить-PST2.[3SG]</ta>
            <ta e="T505" id="Seg_7940" s="T504">этот</ta>
            <ta e="T506" id="Seg_7941" s="T505">старик-ABL</ta>
            <ta e="T507" id="Seg_7942" s="T506">спрашивать-PRS.[3SG]</ta>
            <ta e="T508" id="Seg_7943" s="T507">царь.[NOM]</ta>
            <ta e="T509" id="Seg_7944" s="T508">дочь-PROPR.[NOM]</ta>
            <ta e="T510" id="Seg_7945" s="T509">Q</ta>
            <ta e="T511" id="Seg_7946" s="T510">говорить-CVB.SEQ</ta>
            <ta e="T512" id="Seg_7947" s="T511">EMPH-одинокий</ta>
            <ta e="T513" id="Seg_7948" s="T512">дочь-PROPR.[NOM]</ta>
            <ta e="T514" id="Seg_7949" s="T513">куда</ta>
            <ta e="T515" id="Seg_7950" s="T514">INDEF</ta>
            <ta e="T517" id="Seg_7951" s="T516">муж-DAT/LOC</ta>
            <ta e="T518" id="Seg_7952" s="T517">давать-PRS-3PL</ta>
            <ta e="T519" id="Seg_7953" s="T518">говорят</ta>
            <ta e="T520" id="Seg_7954" s="T519">говорить-CVB.SEQ</ta>
            <ta e="T521" id="Seg_7955" s="T520">старик.[NOM]</ta>
            <ta e="T522" id="Seg_7956" s="T521">2SG.[NOM]</ta>
            <ta e="T523" id="Seg_7957" s="T522">сват-DAT/LOC</ta>
            <ta e="T524" id="Seg_7958" s="T523">идти-FUT-2SG</ta>
            <ta e="T525" id="Seg_7959" s="T524">NEG</ta>
            <ta e="T526" id="Seg_7960" s="T525">Q</ta>
            <ta e="T527" id="Seg_7961" s="T526">говорить-PST2.[3SG]</ta>
            <ta e="T528" id="Seg_7962" s="T527">давешний.[NOM]</ta>
            <ta e="T529" id="Seg_7963" s="T528">гость-3SG.[NOM]</ta>
            <ta e="T530" id="Seg_7964" s="T529">на.всякий.случай</ta>
            <ta e="T531" id="Seg_7965" s="T530">спрашивать-CVB.SEQ</ta>
            <ta e="T532" id="Seg_7966" s="T531">попробовать-FUT-1PL</ta>
            <ta e="T533" id="Seg_7967" s="T532">говорить-PST2.[3SG]</ta>
            <ta e="T534" id="Seg_7968" s="T533">один</ta>
            <ta e="T535" id="Seg_7969" s="T534">гость.[NOM]</ta>
            <ta e="T536" id="Seg_7970" s="T535">приходить-PST2.[3SG]</ta>
            <ta e="T537" id="Seg_7971" s="T536">дочь-2SG-ACC</ta>
            <ta e="T538" id="Seg_7972" s="T537">попросить-PRS.[3SG]</ta>
            <ta e="T539" id="Seg_7973" s="T538">говорить-CVB.SEQ-2SG</ta>
            <ta e="T540" id="Seg_7974" s="T539">говорить.[IMP.2SG]</ta>
            <ta e="T541" id="Seg_7975" s="T540">говорить-PST2.[3SG]</ta>
            <ta e="T542" id="Seg_7976" s="T541">этот</ta>
            <ta e="T543" id="Seg_7977" s="T542">человек.[NOM]</ta>
            <ta e="T544" id="Seg_7978" s="T543">идти-CVB.SEQ</ta>
            <ta e="T545" id="Seg_7979" s="T544">оставаться-PST2.[3SG]</ta>
            <ta e="T546" id="Seg_7980" s="T545">царь.[NOM]</ta>
            <ta e="T547" id="Seg_7981" s="T546">бежать-CVB.SEQ</ta>
            <ta e="T548" id="Seg_7982" s="T547">падать-PST2.[3SG]</ta>
            <ta e="T549" id="Seg_7983" s="T548">эй</ta>
            <ta e="T550" id="Seg_7984" s="T549">почему</ta>
            <ta e="T551" id="Seg_7985" s="T550">приходить-PRS-2SG</ta>
            <ta e="T552" id="Seg_7986" s="T551">нет</ta>
            <ta e="T553" id="Seg_7987" s="T552">1SG.[NOM]</ta>
            <ta e="T554" id="Seg_7988" s="T553">гость-PROPR-1SG</ta>
            <ta e="T555" id="Seg_7989" s="T554">какой-какой</ta>
            <ta e="T556" id="Seg_7990" s="T555">место-ABL</ta>
            <ta e="T557" id="Seg_7991" s="T556">приходить-PST2-3SG</ta>
            <ta e="T558" id="Seg_7992" s="T557">MOD</ta>
            <ta e="T559" id="Seg_7993" s="T558">знать-NEG-1SG</ta>
            <ta e="T560" id="Seg_7994" s="T559">тот</ta>
            <ta e="T561" id="Seg_7995" s="T560">человек.[NOM]</ta>
            <ta e="T562" id="Seg_7996" s="T561">дочь-2SG-ACC</ta>
            <ta e="T563" id="Seg_7997" s="T562">попросить-CVB.SEQ</ta>
            <ta e="T564" id="Seg_7998" s="T563">быть-PRS.[3SG]</ta>
            <ta e="T565" id="Seg_7999" s="T564">говорить-PST2.[3SG]</ta>
            <ta e="T566" id="Seg_8000" s="T565">так</ta>
            <ta e="T567" id="Seg_8001" s="T566">делать-CVB.SEQ</ta>
            <ta e="T568" id="Seg_8002" s="T567">тот</ta>
            <ta e="T569" id="Seg_8003" s="T568">человек.[NOM]</ta>
            <ta e="T570" id="Seg_8004" s="T569">собственный-3SG-GEN</ta>
            <ta e="T571" id="Seg_8005" s="T570">лицо-3SG-ACC-глаз-3SG-ACC</ta>
            <ta e="T572" id="Seg_8006" s="T571">видеть-PTCP.FUT-1SG-ACC</ta>
            <ta e="T573" id="Seg_8007" s="T572">сюда</ta>
            <ta e="T574" id="Seg_8008" s="T573">принести-EP-IMP.2PL</ta>
            <ta e="T575" id="Seg_8009" s="T574">говорить-PST2.[3SG]</ta>
            <ta e="T576" id="Seg_8010" s="T575">царь-3SG.[NOM]</ta>
            <ta e="T577" id="Seg_8011" s="T576">этот</ta>
            <ta e="T578" id="Seg_8012" s="T577">старик.[NOM]</ta>
            <ta e="T579" id="Seg_8013" s="T578">давешний</ta>
            <ta e="T580" id="Seg_8014" s="T579">гость-3SG-ACC</ta>
            <ta e="T581" id="Seg_8015" s="T580">приносить-PST2.[3SG]</ta>
            <ta e="T582" id="Seg_8016" s="T581">па</ta>
            <ta e="T583" id="Seg_8017" s="T582">1SG.[NOM]</ta>
            <ta e="T584" id="Seg_8018" s="T583">такой</ta>
            <ta e="T585" id="Seg_8019" s="T584">человек-DAT/LOC</ta>
            <ta e="T586" id="Seg_8020" s="T585">давать-NEG-1SG</ta>
            <ta e="T587" id="Seg_8021" s="T586">рука-3SG-ACC-нога-3SG-ACC</ta>
            <ta e="T588" id="Seg_8022" s="T587">ломать-FREQ-CVB.SEQ</ta>
            <ta e="T589" id="Seg_8023" s="T588">после</ta>
            <ta e="T590" id="Seg_8024" s="T589">тюрьма-DAT/LOC</ta>
            <ta e="T591" id="Seg_8025" s="T590">бросать-EP-IMP.2PL</ta>
            <ta e="T592" id="Seg_8026" s="T591">говорить-PST2.[3SG]</ta>
            <ta e="T593" id="Seg_8027" s="T592">ай</ta>
            <ta e="T594" id="Seg_8028" s="T593">этот</ta>
            <ta e="T595" id="Seg_8029" s="T594">человек-2SG-GEN</ta>
            <ta e="T596" id="Seg_8030" s="T595">рука-3SG-ACC-нога-3SG-ACC</ta>
            <ta e="T597" id="Seg_8031" s="T596">ломать-PST2.NEG-3PL</ta>
            <ta e="T598" id="Seg_8032" s="T597">веревка-INSTR</ta>
            <ta e="T599" id="Seg_8033" s="T598">обвязывать-CVB.SEQ</ta>
            <ta e="T600" id="Seg_8034" s="T599">идти-CVB.SEQ-3PL</ta>
            <ta e="T601" id="Seg_8035" s="T600">тюрьма-DAT/LOC</ta>
            <ta e="T602" id="Seg_8036" s="T601">вставлять-CVB.SEQ</ta>
            <ta e="T603" id="Seg_8037" s="T602">бросать-PST2-3PL</ta>
            <ta e="T604" id="Seg_8038" s="T603">недавно</ta>
            <ta e="T605" id="Seg_8039" s="T604">человек-2SG-ACC</ta>
            <ta e="T607" id="Seg_8040" s="T606">этот</ta>
            <ta e="T608" id="Seg_8041" s="T607">человек.[NOM]</ta>
            <ta e="T609" id="Seg_8042" s="T608">мышь.[NOM]</ta>
            <ta e="T610" id="Seg_8043" s="T609">становиться-CVB.SEQ</ta>
            <ta e="T611" id="Seg_8044" s="T610">выйти-CVB.SIM</ta>
            <ta e="T612" id="Seg_8045" s="T611">бежать-PST2.[3SG]</ta>
            <ta e="T613" id="Seg_8046" s="T612">выйти-CVB.SIM</ta>
            <ta e="T614" id="Seg_8047" s="T613">бежать-CVB.SEQ</ta>
            <ta e="T615" id="Seg_8048" s="T614">старик-3SG-DAT/LOC</ta>
            <ta e="T616" id="Seg_8049" s="T615">опять</ta>
            <ta e="T617" id="Seg_8050" s="T616">входить-PST2.[3SG]</ta>
            <ta e="T618" id="Seg_8051" s="T617">вот</ta>
            <ta e="T619" id="Seg_8052" s="T618">знать-NEG-1SG</ta>
            <ta e="T620" id="Seg_8053" s="T619">давешний-EP-COMP</ta>
            <ta e="T621" id="Seg_8054" s="T620">другой</ta>
            <ta e="T622" id="Seg_8055" s="T621">человек.[NOM]</ta>
            <ta e="T623" id="Seg_8056" s="T622">приходить-PST2-3SG</ta>
            <ta e="T624" id="Seg_8057" s="T623">Q</ta>
            <ta e="T625" id="Seg_8058" s="T624">что.[NOM]</ta>
            <ta e="T626" id="Seg_8059" s="T625">Q</ta>
            <ta e="T627" id="Seg_8060" s="T626">опять</ta>
            <ta e="T628" id="Seg_8061" s="T627">дочь-2SG-ACC</ta>
            <ta e="T629" id="Seg_8062" s="T628">попросить-CVB.SEQ</ta>
            <ta e="T630" id="Seg_8063" s="T629">быть-PRS.[3SG]</ta>
            <ta e="T631" id="Seg_8064" s="T630">говорить-CVB.SEQ-2SG</ta>
            <ta e="T632" id="Seg_8065" s="T631">идти.[IMP.2SG]</ta>
            <ta e="T633" id="Seg_8066" s="T632">вот</ta>
            <ta e="T634" id="Seg_8067" s="T633">говорить-PST2.[3SG]</ta>
            <ta e="T635" id="Seg_8068" s="T634">опять</ta>
            <ta e="T636" id="Seg_8069" s="T635">идти-PST2.[3SG]</ta>
            <ta e="T637" id="Seg_8070" s="T636">старик.[NOM]</ta>
            <ta e="T638" id="Seg_8071" s="T637">вот</ta>
            <ta e="T639" id="Seg_8072" s="T638">тот</ta>
            <ta e="T640" id="Seg_8073" s="T639">после</ta>
            <ta e="T641" id="Seg_8074" s="T640">опять</ta>
            <ta e="T642" id="Seg_8075" s="T641">говорить-PST2.[3SG]</ta>
            <ta e="T643" id="Seg_8076" s="T642">эй</ta>
            <ta e="T644" id="Seg_8077" s="T643">опять</ta>
            <ta e="T645" id="Seg_8078" s="T644">гость.[NOM]</ta>
            <ta e="T646" id="Seg_8079" s="T645">приходить-PST1-3SG</ta>
            <ta e="T647" id="Seg_8080" s="T646">дочь-2SG-ACC</ta>
            <ta e="T648" id="Seg_8081" s="T647">попросить-CVB.SEQ</ta>
            <ta e="T649" id="Seg_8082" s="T648">быть-PRS.[3SG]</ta>
            <ta e="T650" id="Seg_8083" s="T649">говорить-PST2.[3SG]</ta>
            <ta e="T651" id="Seg_8084" s="T650">друг-PL.[NOM]</ta>
            <ta e="T652" id="Seg_8085" s="T651">недавно</ta>
            <ta e="T653" id="Seg_8086" s="T652">тюрьма-DAT/LOC</ta>
            <ta e="T654" id="Seg_8087" s="T653">бросать-PTCP.HAB</ta>
            <ta e="T655" id="Seg_8088" s="T654">человек-2PL-ACC</ta>
            <ta e="T656" id="Seg_8089" s="T655">видеть-EP-IMP.2PL</ta>
            <ta e="T657" id="Seg_8090" s="T656">вот</ta>
            <ta e="T658" id="Seg_8091" s="T657">как-как</ta>
            <ta e="T659" id="Seg_8092" s="T658">человек-ACC</ta>
            <ta e="T660" id="Seg_8093" s="T659">мучить-PRS-1PL</ta>
            <ta e="T661" id="Seg_8094" s="T660">MOD</ta>
            <ta e="T662" id="Seg_8095" s="T661">говорить-PST2.[3SG]</ta>
            <ta e="T663" id="Seg_8096" s="T662">тюрьма-DAT/LOC</ta>
            <ta e="T664" id="Seg_8097" s="T663">бросать-PTCP.PST</ta>
            <ta e="T665" id="Seg_8098" s="T664">человек-3SG.[NOM]</ta>
            <ta e="T666" id="Seg_8099" s="T665">след-3SG.[NOM]</ta>
            <ta e="T667" id="Seg_8100" s="T666">только</ta>
            <ta e="T668" id="Seg_8101" s="T667">быт.непрямым-CVB.SIM</ta>
            <ta e="T669" id="Seg_8102" s="T668">лежать-PRS.[3SG]</ta>
            <ta e="T670" id="Seg_8103" s="T669">обвязывать-PTCP.PST</ta>
            <ta e="T671" id="Seg_8104" s="T670">веревка-3SG.[NOM]</ta>
            <ta e="T672" id="Seg_8105" s="T671">веревка-3SG-INSTR</ta>
            <ta e="T673" id="Seg_8106" s="T672">лежать-PRS.[3SG]</ta>
            <ta e="T674" id="Seg_8107" s="T673">вот</ta>
            <ta e="T675" id="Seg_8108" s="T674">эй</ta>
            <ta e="T676" id="Seg_8109" s="T675">такой-такой</ta>
            <ta e="T677" id="Seg_8110" s="T676">эй</ta>
            <ta e="T678" id="Seg_8111" s="T677">о</ta>
            <ta e="T679" id="Seg_8112" s="T678">вот</ta>
            <ta e="T680" id="Seg_8113" s="T679">вправду</ta>
            <ta e="T681" id="Seg_8114" s="T680">EMPH</ta>
            <ta e="T682" id="Seg_8115" s="T681">хороший.[NOM]</ta>
            <ta e="T683" id="Seg_8116" s="T682">человек-ACC</ta>
            <ta e="T684" id="Seg_8117" s="T683">делать-PST1-1SG</ta>
            <ta e="T685" id="Seg_8118" s="T684">EMPH</ta>
            <ta e="T686" id="Seg_8119" s="T685">видно</ta>
            <ta e="T687" id="Seg_8120" s="T686">дочь-1SG-ACC</ta>
            <ta e="T688" id="Seg_8121" s="T687">давать-PRS-1SG</ta>
            <ta e="T689" id="Seg_8122" s="T688">говорить-FUT-1SG</ta>
            <ta e="T690" id="Seg_8123" s="T689">MOD</ta>
            <ta e="T691" id="Seg_8124" s="T690">этот</ta>
            <ta e="T692" id="Seg_8125" s="T691">человек.[NOM]</ta>
            <ta e="T693" id="Seg_8126" s="T692">тот-PROPR</ta>
            <ta e="T694" id="Seg_8127" s="T693">горкий</ta>
            <ta e="T695" id="Seg_8128" s="T694">убить-PTCP.PST-3SG-ACC</ta>
            <ta e="T696" id="Seg_8129" s="T695">царь.[NOM]</ta>
            <ta e="T697" id="Seg_8130" s="T696">колено-3SG-DAT/LOC</ta>
            <ta e="T698" id="Seg_8131" s="T697">выливать-CVB.SIM</ta>
            <ta e="T699" id="Seg_8132" s="T698">бросать-PST2.[3SG]</ta>
            <ta e="T700" id="Seg_8133" s="T699">так</ta>
            <ta e="T701" id="Seg_8134" s="T700">делать-CVB.SEQ</ta>
            <ta e="T702" id="Seg_8135" s="T701">аргиш-3SG-GEN</ta>
            <ta e="T703" id="Seg_8136" s="T702">задняя.часть-3SG-DAT/LOC</ta>
            <ta e="T704" id="Seg_8137" s="T703">птичка.[NOM]</ta>
            <ta e="T705" id="Seg_8138" s="T704">петь-PRS.[3SG]</ta>
            <ta e="T706" id="Seg_8139" s="T705">%%</ta>
            <ta e="T707" id="Seg_8140" s="T706">разделять-CVB.SEQ</ta>
            <ta e="T708" id="Seg_8141" s="T707">приносить-PST2.[3SG]</ta>
            <ta e="T709" id="Seg_8142" s="T708">этот</ta>
            <ta e="T710" id="Seg_8143" s="T709">человек.[NOM]</ta>
            <ta e="T711" id="Seg_8144" s="T710">разный-разный</ta>
            <ta e="T712" id="Seg_8145" s="T711">солдат-PROPR-что-PROPR</ta>
            <ta e="T713" id="Seg_8146" s="T712">этот</ta>
            <ta e="T714" id="Seg_8147" s="T713">человек.[NOM]</ta>
            <ta e="T715" id="Seg_8148" s="T714">вот</ta>
            <ta e="T716" id="Seg_8149" s="T715">идти-PRS.[3SG]</ta>
            <ta e="T717" id="Seg_8150" s="T716">дом-3SG-DAT/LOC</ta>
            <ta e="T718" id="Seg_8151" s="T717">дочь-3SG-ACC</ta>
            <ta e="T719" id="Seg_8152" s="T718">взять-CVB.SEQ</ta>
            <ta e="T720" id="Seg_8153" s="T719">после</ta>
            <ta e="T721" id="Seg_8154" s="T720">вот</ta>
            <ta e="T722" id="Seg_8155" s="T721">кочевать-PRS.[3SG]</ta>
            <ta e="T723" id="Seg_8156" s="T722">кочевать-CVB.SEQ</ta>
            <ta e="T724" id="Seg_8157" s="T723">тот.[NOM]</ta>
            <ta e="T725" id="Seg_8158" s="T724">приходить-CVB.SEQ-3PL</ta>
            <ta e="T726" id="Seg_8159" s="T725">тот.[NOM]</ta>
            <ta e="T727" id="Seg_8160" s="T726">вот</ta>
            <ta e="T728" id="Seg_8161" s="T727">тот.[NOM]</ta>
            <ta e="T729" id="Seg_8162" s="T728">быть.богатым-CVB.SEQ-наесться-CVB.SEQ</ta>
            <ta e="T730" id="Seg_8163" s="T729">жить-PST2-3SG</ta>
            <ta e="T731" id="Seg_8164" s="T730">вот</ta>
            <ta e="T732" id="Seg_8165" s="T731">тот.[NOM]</ta>
            <ta e="T733" id="Seg_8166" s="T732">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_8167" s="T1">propr-n:case</ta>
            <ta e="T3" id="Seg_8168" s="T2">v-v:tense-v:pred.pn</ta>
            <ta e="T4" id="Seg_8169" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_8170" s="T4">cardnum</ta>
            <ta e="T6" id="Seg_8171" s="T5">n</ta>
            <ta e="T7" id="Seg_8172" s="T6">v-v:ptcp</ta>
            <ta e="T8" id="Seg_8173" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_8174" s="T8">n-n&gt;adj-n:case</ta>
            <ta e="T10" id="Seg_8175" s="T9">cardnum</ta>
            <ta e="T11" id="Seg_8176" s="T10">n-n&gt;adj</ta>
            <ta e="T12" id="Seg_8177" s="T11">n-n-n&gt;adj-n:case</ta>
            <ta e="T13" id="Seg_8178" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_8179" s="T13">adj</ta>
            <ta e="T15" id="Seg_8180" s="T14">n-n&gt;v-v:cvb</ta>
            <ta e="T16" id="Seg_8181" s="T15">v-v:tense-v:pred.pn</ta>
            <ta e="T17" id="Seg_8182" s="T16">dempro</ta>
            <ta e="T18" id="Seg_8183" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_8184" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_8185" s="T19">v-v&gt;adv</ta>
            <ta e="T21" id="Seg_8186" s="T20">v-v:tense-v:pred.pn</ta>
            <ta e="T22" id="Seg_8187" s="T21">n-n:poss-n:case</ta>
            <ta e="T23" id="Seg_8188" s="T22">adv</ta>
            <ta e="T24" id="Seg_8189" s="T23">v-v:cvb</ta>
            <ta e="T25" id="Seg_8190" s="T24">adv</ta>
            <ta e="T26" id="Seg_8191" s="T25">v-v:tense-v:poss.pn</ta>
            <ta e="T27" id="Seg_8192" s="T26">n-n:(num)-n:case</ta>
            <ta e="T28" id="Seg_8193" s="T27">que-pro:(num)-pro:case</ta>
            <ta e="T29" id="Seg_8194" s="T28">v-v:tense-v:pred.pn</ta>
            <ta e="T30" id="Seg_8195" s="T29">adv</ta>
            <ta e="T31" id="Seg_8196" s="T30">n-n:poss-n:case</ta>
            <ta e="T32" id="Seg_8197" s="T31">dempro-pro:case</ta>
            <ta e="T33" id="Seg_8198" s="T32">v-v:ptcp-v:(case)</ta>
            <ta e="T34" id="Seg_8199" s="T33">n-n:poss-n:case</ta>
            <ta e="T35" id="Seg_8200" s="T34">v-v:ptcp-v:(case)</ta>
            <ta e="T36" id="Seg_8201" s="T35">n-n:poss-n:case</ta>
            <ta e="T37" id="Seg_8202" s="T36">n-n:poss-n:case</ta>
            <ta e="T38" id="Seg_8203" s="T37">v-v:cvb</ta>
            <ta e="T39" id="Seg_8204" s="T38">post</ta>
            <ta e="T40" id="Seg_8205" s="T39">dempro</ta>
            <ta e="T41" id="Seg_8206" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_8207" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_8208" s="T42">n-n:poss-n:case</ta>
            <ta e="T44" id="Seg_8209" s="T43">v-v:tense-v:pred.pn</ta>
            <ta e="T45" id="Seg_8210" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_8211" s="T45">n-n:poss-n:case</ta>
            <ta e="T47" id="Seg_8212" s="T46">v-v:cvb</ta>
            <ta e="T48" id="Seg_8213" s="T47">v-v:mood-v:temp.pn</ta>
            <ta e="T49" id="Seg_8214" s="T48">interj</ta>
            <ta e="T50" id="Seg_8215" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_8216" s="T50">que-pro:case</ta>
            <ta e="T52" id="Seg_8217" s="T51">conj</ta>
            <ta e="T53" id="Seg_8218" s="T52">cardnum</ta>
            <ta e="T54" id="Seg_8219" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_8220" s="T54">n-n:poss-n:case</ta>
            <ta e="T56" id="Seg_8221" s="T55">v-v:cvb</ta>
            <ta e="T57" id="Seg_8222" s="T56">v-v:tense-v:pred.pn</ta>
            <ta e="T58" id="Seg_8223" s="T57">interj</ta>
            <ta e="T59" id="Seg_8224" s="T58">propr-n:case</ta>
            <ta e="T60" id="Seg_8225" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_8226" s="T60">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T62" id="Seg_8227" s="T61">interj</ta>
            <ta e="T63" id="Seg_8228" s="T62">que</ta>
            <ta e="T64" id="Seg_8229" s="T63">v-v:tense-v:pred.pn</ta>
            <ta e="T65" id="Seg_8230" s="T64">interj</ta>
            <ta e="T66" id="Seg_8231" s="T65">n-n:poss-n:case</ta>
            <ta e="T67" id="Seg_8232" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_8233" s="T67">que</ta>
            <ta e="T69" id="Seg_8234" s="T68">v-v:tense-v:pred.pn-ptcl</ta>
            <ta e="T70" id="Seg_8235" s="T69">pers-pro:case</ta>
            <ta e="T71" id="Seg_8236" s="T70">interj</ta>
            <ta e="T72" id="Seg_8237" s="T71">pers-pro:case</ta>
            <ta e="T73" id="Seg_8238" s="T72">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T74" id="Seg_8239" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_8240" s="T74">v-v:cvb-v-v:cvb</ta>
            <ta e="T76" id="Seg_8241" s="T75">v-v:tense-v:pred.pn</ta>
            <ta e="T77" id="Seg_8242" s="T76">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T78" id="Seg_8243" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_8244" s="T78">v-v:tense-v:pred.pn</ta>
            <ta e="T80" id="Seg_8245" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_8246" s="T80">v-v:cvb</ta>
            <ta e="T82" id="Seg_8247" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_8248" s="T82">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T84" id="Seg_8249" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_8250" s="T84">v-v:tense-v:pred.pn</ta>
            <ta e="T86" id="Seg_8251" s="T85">dempro-pro:case</ta>
            <ta e="T87" id="Seg_8252" s="T86">v-v:cvb</ta>
            <ta e="T88" id="Seg_8253" s="T87">v-v:tense-v:pred.pn</ta>
            <ta e="T89" id="Seg_8254" s="T88">n-n:(poss)-n:case</ta>
            <ta e="T90" id="Seg_8255" s="T89">dempro-pro:(poss)-pro:case</ta>
            <ta e="T91" id="Seg_8256" s="T90">n-n:poss-n:case</ta>
            <ta e="T92" id="Seg_8257" s="T91">interj</ta>
            <ta e="T95" id="Seg_8258" s="T94">v-v:cvb</ta>
            <ta e="T96" id="Seg_8259" s="T95">v-v:cvb</ta>
            <ta e="T97" id="Seg_8260" s="T96">v-v:cvb</ta>
            <ta e="T100" id="Seg_8261" s="T99">v-v:cvb</ta>
            <ta e="T101" id="Seg_8262" s="T100">v-v:tense-v:pred.pn</ta>
            <ta e="T102" id="Seg_8263" s="T101">adv</ta>
            <ta e="T103" id="Seg_8264" s="T102">post</ta>
            <ta e="T104" id="Seg_8265" s="T103">dempro-pro:case</ta>
            <ta e="T105" id="Seg_8266" s="T104">v-v:tense-v:pred.pn</ta>
            <ta e="T106" id="Seg_8267" s="T105">conj</ta>
            <ta e="T107" id="Seg_8268" s="T106">n-n:poss-n:case</ta>
            <ta e="T108" id="Seg_8269" s="T107">v-v:tense-v:pred.pn</ta>
            <ta e="T109" id="Seg_8270" s="T108">conj</ta>
            <ta e="T110" id="Seg_8271" s="T109">v-v:cvb</ta>
            <ta e="T111" id="Seg_8272" s="T110">v-v:tense-v:pred.pn</ta>
            <ta e="T112" id="Seg_8273" s="T111">adv</ta>
            <ta e="T113" id="Seg_8274" s="T112">n-n:case</ta>
            <ta e="T114" id="Seg_8275" s="T113">post</ta>
            <ta e="T115" id="Seg_8276" s="T114">v-v:cvb</ta>
            <ta e="T116" id="Seg_8277" s="T115">v-v:cvb</ta>
            <ta e="T117" id="Seg_8278" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_8279" s="T117">v-v:tense-v:pred.pn</ta>
            <ta e="T119" id="Seg_8280" s="T118">interj</ta>
            <ta e="T120" id="Seg_8281" s="T119">propr-n:case</ta>
            <ta e="T121" id="Seg_8282" s="T120">que-pro:case</ta>
            <ta e="T122" id="Seg_8283" s="T121">que</ta>
            <ta e="T123" id="Seg_8284" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_8285" s="T123">v-v:tense-v:pred.pn</ta>
            <ta e="T125" id="Seg_8286" s="T124">n-n:case</ta>
            <ta e="T126" id="Seg_8287" s="T125">v-v&gt;v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T127" id="Seg_8288" s="T126">v-v:tense-v:pred.pn</ta>
            <ta e="T128" id="Seg_8289" s="T127">interj</ta>
            <ta e="T129" id="Seg_8290" s="T128">n-n:poss-n:case</ta>
            <ta e="T130" id="Seg_8291" s="T129">que</ta>
            <ta e="T131" id="Seg_8292" s="T130">adj-n:poss-n:case</ta>
            <ta e="T132" id="Seg_8293" s="T131">v-v:tense-v:pred.pn-ptcl</ta>
            <ta e="T133" id="Seg_8294" s="T132">n-n:poss-n:case</ta>
            <ta e="T134" id="Seg_8295" s="T133">v-v:cvb</ta>
            <ta e="T135" id="Seg_8296" s="T134">v-v:tense-v:pred.pn</ta>
            <ta e="T138" id="Seg_8297" s="T136">dempro-pro:case</ta>
            <ta e="T139" id="Seg_8298" s="T138">ptcl</ta>
            <ta e="T141" id="Seg_8299" s="T139">interj</ta>
            <ta e="T142" id="Seg_8300" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_8301" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_8302" s="T143">v-v:tense-v:pred.pn</ta>
            <ta e="T145" id="Seg_8303" s="T144">interj</ta>
            <ta e="T146" id="Seg_8304" s="T145">n-n:case</ta>
            <ta e="T147" id="Seg_8305" s="T146">post</ta>
            <ta e="T148" id="Seg_8306" s="T147">v-v:cvb-v:pred.pn</ta>
            <ta e="T149" id="Seg_8307" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_8308" s="T149">que-pro:(poss)-pro:case</ta>
            <ta e="T151" id="Seg_8309" s="T150">n-n&gt;v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T152" id="Seg_8310" s="T151">dempro</ta>
            <ta e="T153" id="Seg_8311" s="T152">n-n:case</ta>
            <ta e="T154" id="Seg_8312" s="T153">dempro-pro:case</ta>
            <ta e="T155" id="Seg_8313" s="T154">v-v:tense-v:pred.pn</ta>
            <ta e="T156" id="Seg_8314" s="T155">conj</ta>
            <ta e="T157" id="Seg_8315" s="T156">ptcl</ta>
            <ta e="T159" id="Seg_8316" s="T158">n-n:poss-n:case</ta>
            <ta e="T160" id="Seg_8317" s="T159">v-v:cvb</ta>
            <ta e="T161" id="Seg_8318" s="T160">v-v:tense-v:pred.pn</ta>
            <ta e="T162" id="Seg_8319" s="T161">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T163" id="Seg_8320" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_8321" s="T163">v-v:cvb</ta>
            <ta e="T165" id="Seg_8322" s="T164">v-v:tense-v:pred.pn</ta>
            <ta e="T166" id="Seg_8323" s="T165">v-v:cvb-v-v:cvb</ta>
            <ta e="T167" id="Seg_8324" s="T166">v-v:mood-v:temp.pn</ta>
            <ta e="T168" id="Seg_8325" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_8326" s="T168">v-v:cvb</ta>
            <ta e="T170" id="Seg_8327" s="T169">v-v:tense-v:pred.pn</ta>
            <ta e="T171" id="Seg_8328" s="T170">interj</ta>
            <ta e="T172" id="Seg_8329" s="T171">propr-n:case</ta>
            <ta e="T173" id="Seg_8330" s="T172">n-n:case</ta>
            <ta e="T174" id="Seg_8331" s="T173">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T175" id="Seg_8332" s="T174">que</ta>
            <ta e="T176" id="Seg_8333" s="T175">que</ta>
            <ta e="T177" id="Seg_8334" s="T176">n-n:case</ta>
            <ta e="T178" id="Seg_8335" s="T177">v-v:tense-v:pred.pn</ta>
            <ta e="T179" id="Seg_8336" s="T178">dempro</ta>
            <ta e="T180" id="Seg_8337" s="T179">v-v:tense-v:pred.pn</ta>
            <ta e="T181" id="Seg_8338" s="T180">interj</ta>
            <ta e="T182" id="Seg_8339" s="T181">v-v:tense-v:pred.pn</ta>
            <ta e="T183" id="Seg_8340" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_8341" s="T183">n-n:poss-n:case</ta>
            <ta e="T185" id="Seg_8342" s="T184">adj-n:poss-n:case</ta>
            <ta e="T186" id="Seg_8343" s="T185">v-v:tense-v:pred.pn</ta>
            <ta e="T187" id="Seg_8344" s="T186">dempro-pro:case</ta>
            <ta e="T188" id="Seg_8345" s="T187">post</ta>
            <ta e="T189" id="Seg_8346" s="T188">v-v:cvb</ta>
            <ta e="T190" id="Seg_8347" s="T189">v-v:tense-v:pred.pn</ta>
            <ta e="T191" id="Seg_8348" s="T190">interj</ta>
            <ta e="T192" id="Seg_8349" s="T191">pers-pro:case</ta>
            <ta e="T193" id="Seg_8350" s="T192">n-n:poss-n:case</ta>
            <ta e="T194" id="Seg_8351" s="T193">pers-pro:case</ta>
            <ta e="T195" id="Seg_8352" s="T194">v-v:neg-v:pred.pn</ta>
            <ta e="T196" id="Seg_8353" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_8354" s="T196">adv</ta>
            <ta e="T198" id="Seg_8355" s="T197">adv</ta>
            <ta e="T199" id="Seg_8356" s="T198">pers-pro:case</ta>
            <ta e="T200" id="Seg_8357" s="T199">pers-pro:case</ta>
            <ta e="T201" id="Seg_8358" s="T200">v-v:cvb</ta>
            <ta e="T202" id="Seg_8359" s="T201">v-v:cvb</ta>
            <ta e="T203" id="Seg_8360" s="T202">v-v:cvb</ta>
            <ta e="T204" id="Seg_8361" s="T203">v-v:tense-v:poss.pn</ta>
            <ta e="T205" id="Seg_8362" s="T204">v-v:tense-v:pred.pn</ta>
            <ta e="T206" id="Seg_8363" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_8364" s="T206">v-v:tense-v:pred.pn</ta>
            <ta e="T208" id="Seg_8365" s="T207">dempro</ta>
            <ta e="T209" id="Seg_8366" s="T208">n-n:(num)-n:case</ta>
            <ta e="T210" id="Seg_8367" s="T209">propr-n:case</ta>
            <ta e="T211" id="Seg_8368" s="T210">que-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T212" id="Seg_8369" s="T211">n-n&gt;v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T213" id="Seg_8370" s="T212">v-v:cvb</ta>
            <ta e="T214" id="Seg_8371" s="T213">v-v:tense-v:pred.pn</ta>
            <ta e="T215" id="Seg_8372" s="T214">conj</ta>
            <ta e="T216" id="Seg_8373" s="T215">v-v:cvb</ta>
            <ta e="T217" id="Seg_8374" s="T216">v-v:tense-v:pred.pn</ta>
            <ta e="T218" id="Seg_8375" s="T217">dempro-pro:case</ta>
            <ta e="T219" id="Seg_8376" s="T218">v-v:tense-v:pred.pn</ta>
            <ta e="T220" id="Seg_8377" s="T219">conj</ta>
            <ta e="T221" id="Seg_8378" s="T220">n-n:poss-n:case</ta>
            <ta e="T222" id="Seg_8379" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_8380" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_8381" s="T223">adj</ta>
            <ta e="T225" id="Seg_8382" s="T224">v-v:tense-v:poss.pn</ta>
            <ta e="T226" id="Seg_8383" s="T225">n-n:poss-n:case</ta>
            <ta e="T227" id="Seg_8384" s="T226">v-v:cvb</ta>
            <ta e="T228" id="Seg_8385" s="T227">v-v:tense-v:pred.pn</ta>
            <ta e="T229" id="Seg_8386" s="T228">n-n:poss-n:case</ta>
            <ta e="T230" id="Seg_8387" s="T229">adv</ta>
            <ta e="T231" id="Seg_8388" s="T230">v-v:cvb</ta>
            <ta e="T232" id="Seg_8389" s="T231">v-v:tense-v:pred.pn</ta>
            <ta e="T233" id="Seg_8390" s="T232">v-v:cvb</ta>
            <ta e="T234" id="Seg_8391" s="T233">v-v:cvb</ta>
            <ta e="T235" id="Seg_8392" s="T234">v-v:mood-v:temp.pn</ta>
            <ta e="T236" id="Seg_8393" s="T235">n-n:case</ta>
            <ta e="T237" id="Seg_8394" s="T236">v-v:cvb</ta>
            <ta e="T238" id="Seg_8395" s="T237">v-v:tense-v:pred.pn</ta>
            <ta e="T239" id="Seg_8396" s="T238">interj</ta>
            <ta e="T240" id="Seg_8397" s="T239">propr-n:case</ta>
            <ta e="T241" id="Seg_8398" s="T240">n-n:case</ta>
            <ta e="T242" id="Seg_8399" s="T241">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T243" id="Seg_8400" s="T242">adv</ta>
            <ta e="T244" id="Seg_8401" s="T243">v-v:tense-v:pred.pn</ta>
            <ta e="T245" id="Seg_8402" s="T244">que</ta>
            <ta e="T246" id="Seg_8403" s="T245">v-v:tense-v:pred.pn</ta>
            <ta e="T247" id="Seg_8404" s="T246">dempro</ta>
            <ta e="T248" id="Seg_8405" s="T247">v-v:cvb</ta>
            <ta e="T249" id="Seg_8406" s="T248">n-n:case</ta>
            <ta e="T250" id="Seg_8407" s="T249">interj</ta>
            <ta e="T251" id="Seg_8408" s="T250">ptcl</ta>
            <ta e="T252" id="Seg_8409" s="T251">n-n:poss-n:case</ta>
            <ta e="T253" id="Seg_8410" s="T252">que</ta>
            <ta e="T254" id="Seg_8411" s="T253">adj-n:poss-n:case</ta>
            <ta e="T255" id="Seg_8412" s="T254">v-v:tense-v:pred.pn-ptcl</ta>
            <ta e="T256" id="Seg_8413" s="T255">dempro-pro:case</ta>
            <ta e="T257" id="Seg_8414" s="T256">v-v:cvb</ta>
            <ta e="T258" id="Seg_8415" s="T257">v-v:tense-v:pred.pn</ta>
            <ta e="T259" id="Seg_8416" s="T258">v-v:tense-v:pred.pn</ta>
            <ta e="T260" id="Seg_8417" s="T259">adv</ta>
            <ta e="T261" id="Seg_8418" s="T260">v-v:tense-v:pred.pn-ptcl</ta>
            <ta e="T262" id="Seg_8419" s="T261">v-v:cvb</ta>
            <ta e="T263" id="Seg_8420" s="T262">v-v:tense-v:pred.pn</ta>
            <ta e="T264" id="Seg_8421" s="T263">conj</ta>
            <ta e="T265" id="Seg_8422" s="T264">dempro-pro:case</ta>
            <ta e="T266" id="Seg_8423" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_8424" s="T266">v-v:tense-v:pred.pn</ta>
            <ta e="T268" id="Seg_8425" s="T267">v-v:cvb</ta>
            <ta e="T269" id="Seg_8426" s="T268">propr-n:case</ta>
            <ta e="T270" id="Seg_8427" s="T269">que-pro:(poss)-pro:case</ta>
            <ta e="T271" id="Seg_8428" s="T270">n-n&gt;v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T272" id="Seg_8429" s="T271">n-n&gt;v-v:neg-v:pred.pn</ta>
            <ta e="T273" id="Seg_8430" s="T272">v-v:tense-v:pred.pn</ta>
            <ta e="T274" id="Seg_8431" s="T273">conj</ta>
            <ta e="T275" id="Seg_8432" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_8433" s="T275">v-v:tense-v:pred.pn</ta>
            <ta e="T277" id="Seg_8434" s="T276">conj</ta>
            <ta e="T278" id="Seg_8435" s="T277">n-n:poss-n:case</ta>
            <ta e="T279" id="Seg_8436" s="T278">v-v:cvb</ta>
            <ta e="T280" id="Seg_8437" s="T279">v-v:tense-v:pred.pn</ta>
            <ta e="T281" id="Seg_8438" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_8439" s="T281">v-v:cvb</ta>
            <ta e="T283" id="Seg_8440" s="T282">v-v:tense-v:pred.pn</ta>
            <ta e="T284" id="Seg_8441" s="T283">v-v:cvb-v-v:cvb</ta>
            <ta e="T285" id="Seg_8442" s="T284">n-n:case</ta>
            <ta e="T286" id="Seg_8443" s="T285">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T287" id="Seg_8444" s="T286">n-n:(poss)-n:case</ta>
            <ta e="T288" id="Seg_8445" s="T287">v-v:cvb</ta>
            <ta e="T289" id="Seg_8446" s="T288">v-v:tense-v:pred.pn</ta>
            <ta e="T290" id="Seg_8447" s="T289">interj</ta>
            <ta e="T291" id="Seg_8448" s="T290">propr-n:case</ta>
            <ta e="T292" id="Seg_8449" s="T291">n-n:case</ta>
            <ta e="T293" id="Seg_8450" s="T292">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T294" id="Seg_8451" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_8452" s="T294">que</ta>
            <ta e="T296" id="Seg_8453" s="T295">v-v:tense-v:pred.pn</ta>
            <ta e="T297" id="Seg_8454" s="T296">v-v:tense-v:pred.pn</ta>
            <ta e="T298" id="Seg_8455" s="T297">interj</ta>
            <ta e="T299" id="Seg_8456" s="T298">n-n:poss-n:case</ta>
            <ta e="T300" id="Seg_8457" s="T299">que</ta>
            <ta e="T301" id="Seg_8458" s="T300">v-v:tense-v:pred.pn-ptcl</ta>
            <ta e="T302" id="Seg_8459" s="T301">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T303" id="Seg_8460" s="T302">v-v:tense-v:pred.pn</ta>
            <ta e="T304" id="Seg_8461" s="T303">interj</ta>
            <ta e="T305" id="Seg_8462" s="T304">pers-pro:case</ta>
            <ta e="T306" id="Seg_8463" s="T305">n-n:poss-n:case</ta>
            <ta e="T307" id="Seg_8464" s="T306">v-v:neg-v:pred.pn</ta>
            <ta e="T308" id="Seg_8465" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_8466" s="T308">adv</ta>
            <ta e="T310" id="Seg_8467" s="T309">pers-pro:case</ta>
            <ta e="T311" id="Seg_8468" s="T310">pers-pro:case</ta>
            <ta e="T312" id="Seg_8469" s="T311">v-v:cvb</ta>
            <ta e="T313" id="Seg_8470" s="T312">v-v:tense-v:poss.pn</ta>
            <ta e="T314" id="Seg_8471" s="T313">v-v:tense-v:pred.pn</ta>
            <ta e="T315" id="Seg_8472" s="T314">dempro-pro:case</ta>
            <ta e="T316" id="Seg_8473" s="T315">v-v:cvb</ta>
            <ta e="T317" id="Seg_8474" s="T316">v-v:tense-v:pred.pn</ta>
            <ta e="T318" id="Seg_8475" s="T317">v-v:cvb</ta>
            <ta e="T319" id="Seg_8476" s="T318">v-v:tense-v:pred.pn</ta>
            <ta e="T320" id="Seg_8477" s="T319">propr-n:case</ta>
            <ta e="T321" id="Seg_8478" s="T320">propr-n:case</ta>
            <ta e="T322" id="Seg_8479" s="T321">n-n:poss-n:case</ta>
            <ta e="T323" id="Seg_8480" s="T322">v-v:cvb</ta>
            <ta e="T324" id="Seg_8481" s="T323">v-v:tense-v:pred.pn</ta>
            <ta e="T325" id="Seg_8482" s="T324">conj</ta>
            <ta e="T326" id="Seg_8483" s="T325">v-v:cvb</ta>
            <ta e="T327" id="Seg_8484" s="T326">v-v:tense-v:pred.pn</ta>
            <ta e="T328" id="Seg_8485" s="T327">adv</ta>
            <ta e="T329" id="Seg_8486" s="T328">v-v:tense-v:pred.pn</ta>
            <ta e="T330" id="Seg_8487" s="T329">conj</ta>
            <ta e="T331" id="Seg_8488" s="T330">v-v:tense-v:pred.pn</ta>
            <ta e="T332" id="Seg_8489" s="T331">conj</ta>
            <ta e="T333" id="Seg_8490" s="T332">n-n:poss-n:case</ta>
            <ta e="T334" id="Seg_8491" s="T333">v-v:cvb</ta>
            <ta e="T335" id="Seg_8492" s="T334">v-v:tense-v:pred.pn</ta>
            <ta e="T336" id="Seg_8493" s="T335">adv</ta>
            <ta e="T337" id="Seg_8494" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_8495" s="T337">v-v:cvb</ta>
            <ta e="T339" id="Seg_8496" s="T338">v-v:tense-v:pred.pn</ta>
            <ta e="T340" id="Seg_8497" s="T339">v-v:cvb</ta>
            <ta e="T341" id="Seg_8498" s="T340">v-v:tense-v:pred.pn</ta>
            <ta e="T342" id="Seg_8499" s="T341">interj</ta>
            <ta e="T343" id="Seg_8500" s="T342">n-n:case</ta>
            <ta e="T344" id="Seg_8501" s="T343">v-v:cvb</ta>
            <ta e="T345" id="Seg_8502" s="T344">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T346" id="Seg_8503" s="T345">ptcl</ta>
            <ta e="T347" id="Seg_8504" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_8505" s="T347">ptcl</ta>
            <ta e="T349" id="Seg_8506" s="T348">interj</ta>
            <ta e="T350" id="Seg_8507" s="T349">propr-n:case</ta>
            <ta e="T351" id="Seg_8508" s="T350">que</ta>
            <ta e="T352" id="Seg_8509" s="T351">que</ta>
            <ta e="T353" id="Seg_8510" s="T352">n-n:case</ta>
            <ta e="T354" id="Seg_8511" s="T353">v-v:tense-v:pred.pn</ta>
            <ta e="T355" id="Seg_8512" s="T354">dempro</ta>
            <ta e="T356" id="Seg_8513" s="T355">v-v:tense-v:pred.pn</ta>
            <ta e="T357" id="Seg_8514" s="T356">n-n:poss-n:case</ta>
            <ta e="T358" id="Seg_8515" s="T357">que</ta>
            <ta e="T359" id="Seg_8516" s="T358">v-v:tense-v:pred.pn-ptcl</ta>
            <ta e="T360" id="Seg_8517" s="T359">adv</ta>
            <ta e="T361" id="Seg_8518" s="T360">n-n:poss-n:case</ta>
            <ta e="T362" id="Seg_8519" s="T361">v-v:tense-v:pred.pn</ta>
            <ta e="T364" id="Seg_8520" s="T362">v</ta>
            <ta e="T365" id="Seg_8521" s="T364">pers-pro:case</ta>
            <ta e="T366" id="Seg_8522" s="T365">pers-pro:case</ta>
            <ta e="T367" id="Seg_8523" s="T366">v-v:cvb</ta>
            <ta e="T368" id="Seg_8524" s="T367">v-v:tense-v:poss.pn</ta>
            <ta e="T369" id="Seg_8525" s="T368">v-v:tense-v:pred.pn</ta>
            <ta e="T370" id="Seg_8526" s="T369">v-v:cvb</ta>
            <ta e="T371" id="Seg_8527" s="T370">v-v:tense-v:pred.pn</ta>
            <ta e="T372" id="Seg_8528" s="T371">n-n:poss-n:case</ta>
            <ta e="T373" id="Seg_8529" s="T372">v-v:cvb</ta>
            <ta e="T374" id="Seg_8530" s="T373">v-v:tense-v:pred.pn</ta>
            <ta e="T375" id="Seg_8531" s="T374">conj</ta>
            <ta e="T376" id="Seg_8532" s="T375">v-v:cvb</ta>
            <ta e="T377" id="Seg_8533" s="T376">v-v:tense-v:pred.pn</ta>
            <ta e="T378" id="Seg_8534" s="T377">dempro</ta>
            <ta e="T379" id="Seg_8535" s="T378">n-n:case</ta>
            <ta e="T380" id="Seg_8536" s="T379">adv</ta>
            <ta e="T381" id="Seg_8537" s="T380">post</ta>
            <ta e="T382" id="Seg_8538" s="T381">n-n:poss-n:case</ta>
            <ta e="T383" id="Seg_8539" s="T382">v-v:cvb</ta>
            <ta e="T384" id="Seg_8540" s="T383">post</ta>
            <ta e="T385" id="Seg_8541" s="T384">ptcl</ta>
            <ta e="T386" id="Seg_8542" s="T385">v-v:tense-v:pred.pn</ta>
            <ta e="T387" id="Seg_8543" s="T386">adv</ta>
            <ta e="T388" id="Seg_8544" s="T387">dempro</ta>
            <ta e="T389" id="Seg_8545" s="T388">v-v:cvb</ta>
            <ta e="T390" id="Seg_8546" s="T389">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T391" id="Seg_8547" s="T390">dempro</ta>
            <ta e="T392" id="Seg_8548" s="T391">n-n:case</ta>
            <ta e="T393" id="Seg_8549" s="T392">n-n:poss-n:case</ta>
            <ta e="T394" id="Seg_8550" s="T393">v-v:cvb</ta>
            <ta e="T395" id="Seg_8551" s="T394">v-v:ptcp-v:(case)</ta>
            <ta e="T396" id="Seg_8552" s="T395">n-n:case</ta>
            <ta e="T397" id="Seg_8553" s="T396">adj</ta>
            <ta e="T398" id="Seg_8554" s="T397">n-n&gt;adj-n:case</ta>
            <ta e="T399" id="Seg_8555" s="T398">v-v:tense-v:poss.pn</ta>
            <ta e="T400" id="Seg_8556" s="T399">dempro-pro:case</ta>
            <ta e="T401" id="Seg_8557" s="T400">v-v:cvb</ta>
            <ta e="T402" id="Seg_8558" s="T401">v-v:ptcp-v:(case)</ta>
            <ta e="T403" id="Seg_8559" s="T402">ptcl</ta>
            <ta e="T404" id="Seg_8560" s="T403">adv-adv&gt;adj</ta>
            <ta e="T405" id="Seg_8561" s="T404">que-que&gt;adj</ta>
            <ta e="T406" id="Seg_8562" s="T405">n-n:case</ta>
            <ta e="T407" id="Seg_8563" s="T406">ptcl</ta>
            <ta e="T408" id="Seg_8564" s="T407">v-v:tense-v:pred.pn</ta>
            <ta e="T409" id="Seg_8565" s="T408">ptcl</ta>
            <ta e="T410" id="Seg_8566" s="T409">n-n:case</ta>
            <ta e="T411" id="Seg_8567" s="T410">v-v:cvb</ta>
            <ta e="T412" id="Seg_8568" s="T411">dempro</ta>
            <ta e="T413" id="Seg_8569" s="T412">v-v:tense-v:pred.pn</ta>
            <ta e="T414" id="Seg_8570" s="T413">dempro</ta>
            <ta e="T415" id="Seg_8571" s="T414">n-n:(poss)-n:case</ta>
            <ta e="T416" id="Seg_8572" s="T415">v-v:tense-v:pred.pn</ta>
            <ta e="T417" id="Seg_8573" s="T416">ptcl</ta>
            <ta e="T418" id="Seg_8574" s="T417">v-v:ptcp-v:(case)</ta>
            <ta e="T419" id="Seg_8575" s="T418">adj-n:case</ta>
            <ta e="T420" id="Seg_8576" s="T419">ptcl</ta>
            <ta e="T421" id="Seg_8577" s="T420">v-v:tense-v:pred.pn</ta>
            <ta e="T422" id="Seg_8578" s="T421">dempro</ta>
            <ta e="T423" id="Seg_8579" s="T422">n-n:case</ta>
            <ta e="T424" id="Seg_8580" s="T423">v-v:cvb-v-v:cvb</ta>
            <ta e="T425" id="Seg_8581" s="T424">cardnum</ta>
            <ta e="T426" id="Seg_8582" s="T425">n-n:case</ta>
            <ta e="T427" id="Seg_8583" s="T426">v-v:tense-v:pred.pn</ta>
            <ta e="T428" id="Seg_8584" s="T427">v-v:cvb</ta>
            <ta e="T429" id="Seg_8585" s="T428">v-v:tense-v:poss.pn</ta>
            <ta e="T430" id="Seg_8586" s="T429">n-n:case</ta>
            <ta e="T431" id="Seg_8587" s="T430">adj-n:(poss)</ta>
            <ta e="T432" id="Seg_8588" s="T431">n-n:(poss)-n:case</ta>
            <ta e="T433" id="Seg_8589" s="T432">v-v:tense-v:pred.pn</ta>
            <ta e="T434" id="Seg_8590" s="T433">adv</ta>
            <ta e="T435" id="Seg_8591" s="T434">v-v:cvb-v:(ins)-v:pred.pn</ta>
            <ta e="T436" id="Seg_8592" s="T435">adv</ta>
            <ta e="T437" id="Seg_8593" s="T436">dempro</ta>
            <ta e="T438" id="Seg_8594" s="T437">n-n:case</ta>
            <ta e="T439" id="Seg_8595" s="T438">v-v:cvb-v-v:cvb</ta>
            <ta e="T440" id="Seg_8596" s="T439">v-v:cvb-v-v:cvb</ta>
            <ta e="T441" id="Seg_8597" s="T440">n-n:case</ta>
            <ta e="T442" id="Seg_8598" s="T441">v-v:tense-v:pred.pn</ta>
            <ta e="T443" id="Seg_8599" s="T442">n-n:case</ta>
            <ta e="T444" id="Seg_8600" s="T443">adj-n:poss-n:case</ta>
            <ta e="T445" id="Seg_8601" s="T444">v-v:tense-v:poss.pn</ta>
            <ta e="T446" id="Seg_8602" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_8603" s="T446">n-n:case</ta>
            <ta e="T448" id="Seg_8604" s="T447">post</ta>
            <ta e="T449" id="Seg_8605" s="T448">adj</ta>
            <ta e="T450" id="Seg_8606" s="T449">ptcl</ta>
            <ta e="T451" id="Seg_8607" s="T450">n-n&gt;adj</ta>
            <ta e="T452" id="Seg_8608" s="T451">n-n:case</ta>
            <ta e="T453" id="Seg_8609" s="T452">v-v:tense-v:pred.pn</ta>
            <ta e="T454" id="Seg_8610" s="T453">n-n:case</ta>
            <ta e="T455" id="Seg_8611" s="T454">v-v:tense-v:pred.pn</ta>
            <ta e="T456" id="Seg_8612" s="T455">ptcl</ta>
            <ta e="T457" id="Seg_8613" s="T456">dempro-pro:case</ta>
            <ta e="T458" id="Seg_8614" s="T457">v-v:ptcp-v:(case)</ta>
            <ta e="T459" id="Seg_8615" s="T458">adj-n:case</ta>
            <ta e="T460" id="Seg_8616" s="T459">n-n:case</ta>
            <ta e="T461" id="Seg_8617" s="T460">n-n:case</ta>
            <ta e="T462" id="Seg_8618" s="T461">v-v:cvb</ta>
            <ta e="T463" id="Seg_8619" s="T462">v-v:tense-v:poss.pn</ta>
            <ta e="T464" id="Seg_8620" s="T463">n-n&gt;adj</ta>
            <ta e="T465" id="Seg_8621" s="T464">n-n:case</ta>
            <ta e="T466" id="Seg_8622" s="T465">n-n:(poss)-n:case</ta>
            <ta e="T467" id="Seg_8623" s="T466">n-n:(poss)</ta>
            <ta e="T468" id="Seg_8624" s="T467">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T469" id="Seg_8625" s="T468">n-n:(poss)-n:case</ta>
            <ta e="T470" id="Seg_8626" s="T469">quant</ta>
            <ta e="T471" id="Seg_8627" s="T470">adj-n:case</ta>
            <ta e="T472" id="Seg_8628" s="T471">ptcl</ta>
            <ta e="T473" id="Seg_8629" s="T472">ptcl</ta>
            <ta e="T474" id="Seg_8630" s="T473">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T475" id="Seg_8631" s="T474">dempro-pro:case</ta>
            <ta e="T476" id="Seg_8632" s="T475">v-v:tense-v:pred.pn</ta>
            <ta e="T477" id="Seg_8633" s="T476">dempro</ta>
            <ta e="T478" id="Seg_8634" s="T477">n-n:case</ta>
            <ta e="T479" id="Seg_8635" s="T478">que-que</ta>
            <ta e="T480" id="Seg_8636" s="T479">n-n:case</ta>
            <ta e="T481" id="Seg_8637" s="T480">v-v:tense-v:pred.pn</ta>
            <ta e="T482" id="Seg_8638" s="T481">v-v:cvb</ta>
            <ta e="T483" id="Seg_8639" s="T482">n-n:case</ta>
            <ta e="T484" id="Seg_8640" s="T483">v-v:tense-v:pred.pn</ta>
            <ta e="T485" id="Seg_8641" s="T484">pers-pro:case</ta>
            <ta e="T487" id="Seg_8642" s="T486">cardnum</ta>
            <ta e="T488" id="Seg_8643" s="T487">n-n&gt;adj</ta>
            <ta e="T489" id="Seg_8644" s="T488">n-n-n&gt;adj-n:case</ta>
            <ta e="T490" id="Seg_8645" s="T489">v-v:tense-v:poss.pn</ta>
            <ta e="T491" id="Seg_8646" s="T490">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T492" id="Seg_8647" s="T491">n-n:case</ta>
            <ta e="T493" id="Seg_8648" s="T492">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T494" id="Seg_8649" s="T493">v-v&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T495" id="Seg_8650" s="T494">adj-n:(num)-n:case</ta>
            <ta e="T496" id="Seg_8651" s="T495">v-v&gt;v-v:cvb</ta>
            <ta e="T497" id="Seg_8652" s="T496">v-v:tense-v:pred.pn</ta>
            <ta e="T498" id="Seg_8653" s="T497">v-v:tense-v:pred.pn</ta>
            <ta e="T499" id="Seg_8654" s="T498">interj</ta>
            <ta e="T502" id="Seg_8655" s="T501">adj-n:(num)-n:case</ta>
            <ta e="T503" id="Seg_8656" s="T502">v-v:cvb</ta>
            <ta e="T504" id="Seg_8657" s="T503">v-v:tense-v:pred.pn</ta>
            <ta e="T505" id="Seg_8658" s="T504">dempro</ta>
            <ta e="T506" id="Seg_8659" s="T505">n-n:case</ta>
            <ta e="T507" id="Seg_8660" s="T506">v-v:tense-v:pred.pn</ta>
            <ta e="T508" id="Seg_8661" s="T507">n-n:case</ta>
            <ta e="T509" id="Seg_8662" s="T508">n-n&gt;adj-n:case</ta>
            <ta e="T510" id="Seg_8663" s="T509">ptcl</ta>
            <ta e="T511" id="Seg_8664" s="T510">v-v:cvb</ta>
            <ta e="T512" id="Seg_8665" s="T511">adj&gt;adj-adj</ta>
            <ta e="T513" id="Seg_8666" s="T512">n-n&gt;adj-n:case</ta>
            <ta e="T514" id="Seg_8667" s="T513">que</ta>
            <ta e="T515" id="Seg_8668" s="T514">ptcl</ta>
            <ta e="T517" id="Seg_8669" s="T516">n-n:case</ta>
            <ta e="T518" id="Seg_8670" s="T517">v-v:tense-v:pred.pn</ta>
            <ta e="T519" id="Seg_8671" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_8672" s="T519">v-v:cvb</ta>
            <ta e="T521" id="Seg_8673" s="T520">n-n:case</ta>
            <ta e="T522" id="Seg_8674" s="T521">pers-pro:case</ta>
            <ta e="T523" id="Seg_8675" s="T522">n-n:case</ta>
            <ta e="T524" id="Seg_8676" s="T523">v-v:tense-v:poss.pn</ta>
            <ta e="T525" id="Seg_8677" s="T524">ptcl</ta>
            <ta e="T526" id="Seg_8678" s="T525">ptcl</ta>
            <ta e="T527" id="Seg_8679" s="T526">v-v:tense-v:pred.pn</ta>
            <ta e="T528" id="Seg_8680" s="T527">adj-n:case</ta>
            <ta e="T529" id="Seg_8681" s="T528">n-n:(poss)-n:case</ta>
            <ta e="T530" id="Seg_8682" s="T529">adv</ta>
            <ta e="T531" id="Seg_8683" s="T530">v-v:cvb</ta>
            <ta e="T532" id="Seg_8684" s="T531">v-v:tense-v:poss.pn</ta>
            <ta e="T533" id="Seg_8685" s="T532">v-v:tense-v:pred.pn</ta>
            <ta e="T534" id="Seg_8686" s="T533">cardnum</ta>
            <ta e="T535" id="Seg_8687" s="T534">n-n:case</ta>
            <ta e="T536" id="Seg_8688" s="T535">v-v:tense-v:pred.pn</ta>
            <ta e="T537" id="Seg_8689" s="T536">n-n:poss-n:case</ta>
            <ta e="T538" id="Seg_8690" s="T537">v-v:tense-v:pred.pn</ta>
            <ta e="T539" id="Seg_8691" s="T538">v-v:cvb-v:pred.pn</ta>
            <ta e="T540" id="Seg_8692" s="T539">v-v:mood.pn</ta>
            <ta e="T541" id="Seg_8693" s="T540">v-v:tense-v:pred.pn</ta>
            <ta e="T542" id="Seg_8694" s="T541">dempro</ta>
            <ta e="T543" id="Seg_8695" s="T542">n-n:case</ta>
            <ta e="T544" id="Seg_8696" s="T543">v-v:cvb</ta>
            <ta e="T545" id="Seg_8697" s="T544">v-v:tense-v:pred.pn</ta>
            <ta e="T546" id="Seg_8698" s="T545">n-n:case</ta>
            <ta e="T547" id="Seg_8699" s="T546">v-v:cvb</ta>
            <ta e="T548" id="Seg_8700" s="T547">v-v:tense-v:pred.pn</ta>
            <ta e="T549" id="Seg_8701" s="T548">interj</ta>
            <ta e="T550" id="Seg_8702" s="T549">que</ta>
            <ta e="T551" id="Seg_8703" s="T550">v-v:tense-v:pred.pn</ta>
            <ta e="T552" id="Seg_8704" s="T551">ptcl</ta>
            <ta e="T553" id="Seg_8705" s="T552">pers-pro:case</ta>
            <ta e="T554" id="Seg_8706" s="T553">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T555" id="Seg_8707" s="T554">que-que</ta>
            <ta e="T556" id="Seg_8708" s="T555">n-n:case</ta>
            <ta e="T557" id="Seg_8709" s="T556">v-v:tense-v:poss.pn</ta>
            <ta e="T558" id="Seg_8710" s="T557">ptcl</ta>
            <ta e="T559" id="Seg_8711" s="T558">v-v:(neg)-v:pred.pn</ta>
            <ta e="T560" id="Seg_8712" s="T559">dempro</ta>
            <ta e="T561" id="Seg_8713" s="T560">n-n:case</ta>
            <ta e="T562" id="Seg_8714" s="T561">n-n:poss-n:case</ta>
            <ta e="T563" id="Seg_8715" s="T562">v-v:cvb</ta>
            <ta e="T564" id="Seg_8716" s="T563">v-v:tense-v:pred.pn</ta>
            <ta e="T565" id="Seg_8717" s="T564">v-v:tense-v:pred.pn</ta>
            <ta e="T566" id="Seg_8718" s="T565">adv</ta>
            <ta e="T567" id="Seg_8719" s="T566">v-v:cvb</ta>
            <ta e="T568" id="Seg_8720" s="T567">dempro</ta>
            <ta e="T569" id="Seg_8721" s="T568">n-n:case</ta>
            <ta e="T570" id="Seg_8722" s="T569">adj-n:poss-n:case</ta>
            <ta e="T571" id="Seg_8723" s="T570">n-n:poss-n:case-n-n:poss-n:case</ta>
            <ta e="T572" id="Seg_8724" s="T571">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T573" id="Seg_8725" s="T572">adv</ta>
            <ta e="T574" id="Seg_8726" s="T573">v-v:(ins)-v:mood.pn</ta>
            <ta e="T575" id="Seg_8727" s="T574">v-v:tense-v:pred.pn</ta>
            <ta e="T576" id="Seg_8728" s="T575">n-n:(poss)-n:case</ta>
            <ta e="T577" id="Seg_8729" s="T576">dempro</ta>
            <ta e="T578" id="Seg_8730" s="T577">n-n:case</ta>
            <ta e="T579" id="Seg_8731" s="T578">adj</ta>
            <ta e="T580" id="Seg_8732" s="T579">n-n:poss-n:case</ta>
            <ta e="T581" id="Seg_8733" s="T580">v-v:tense-v:pred.pn</ta>
            <ta e="T582" id="Seg_8734" s="T581">interj</ta>
            <ta e="T583" id="Seg_8735" s="T582">pers-pro:case</ta>
            <ta e="T584" id="Seg_8736" s="T583">dempro</ta>
            <ta e="T585" id="Seg_8737" s="T584">n-n:case</ta>
            <ta e="T586" id="Seg_8738" s="T585">v-v:(neg)-v:pred.pn</ta>
            <ta e="T587" id="Seg_8739" s="T586">n-n:poss-n:case-n-n:poss-n:case</ta>
            <ta e="T588" id="Seg_8740" s="T587">v-v&gt;v-v:cvb</ta>
            <ta e="T589" id="Seg_8741" s="T588">post</ta>
            <ta e="T590" id="Seg_8742" s="T589">n-n:case</ta>
            <ta e="T591" id="Seg_8743" s="T590">v-v:(ins)-v:mood.pn</ta>
            <ta e="T592" id="Seg_8744" s="T591">v-v:tense-v:pred.pn</ta>
            <ta e="T593" id="Seg_8745" s="T592">interj</ta>
            <ta e="T594" id="Seg_8746" s="T593">dempro</ta>
            <ta e="T595" id="Seg_8747" s="T594">n-n:poss-n:case</ta>
            <ta e="T596" id="Seg_8748" s="T595">n-n:poss-n:case-n-n:poss-n:case</ta>
            <ta e="T597" id="Seg_8749" s="T596">v-v:neg-v:pred.pn</ta>
            <ta e="T598" id="Seg_8750" s="T597">n-n:case</ta>
            <ta e="T599" id="Seg_8751" s="T598">v-v:cvb</ta>
            <ta e="T600" id="Seg_8752" s="T599">v-v:cvb-v:pred.pn</ta>
            <ta e="T601" id="Seg_8753" s="T600">n-n:case</ta>
            <ta e="T602" id="Seg_8754" s="T601">v-v:cvb</ta>
            <ta e="T603" id="Seg_8755" s="T602">v-v:tense-v:pred.pn</ta>
            <ta e="T604" id="Seg_8756" s="T603">adv</ta>
            <ta e="T605" id="Seg_8757" s="T604">n-n:poss-n:case</ta>
            <ta e="T607" id="Seg_8758" s="T606">dempro</ta>
            <ta e="T608" id="Seg_8759" s="T607">n-n:case</ta>
            <ta e="T609" id="Seg_8760" s="T608">n-n:case</ta>
            <ta e="T610" id="Seg_8761" s="T609">v-v:cvb</ta>
            <ta e="T611" id="Seg_8762" s="T610">v-v:cvb</ta>
            <ta e="T612" id="Seg_8763" s="T611">v-v:tense-v:pred.pn</ta>
            <ta e="T613" id="Seg_8764" s="T612">v-v:cvb</ta>
            <ta e="T614" id="Seg_8765" s="T613">v-v:cvb</ta>
            <ta e="T615" id="Seg_8766" s="T614">n-n:poss-n:case</ta>
            <ta e="T616" id="Seg_8767" s="T615">ptcl</ta>
            <ta e="T617" id="Seg_8768" s="T616">v-v:tense-v:pred.pn</ta>
            <ta e="T618" id="Seg_8769" s="T617">ptcl</ta>
            <ta e="T619" id="Seg_8770" s="T618">v-v:(neg)-v:pred.pn</ta>
            <ta e="T620" id="Seg_8771" s="T619">adj-n:(ins)-n:case</ta>
            <ta e="T621" id="Seg_8772" s="T620">adj</ta>
            <ta e="T622" id="Seg_8773" s="T621">n-n:case</ta>
            <ta e="T623" id="Seg_8774" s="T622">v-v:tense-v:poss.pn</ta>
            <ta e="T624" id="Seg_8775" s="T623">ptcl</ta>
            <ta e="T625" id="Seg_8776" s="T624">que-pro:case</ta>
            <ta e="T626" id="Seg_8777" s="T625">ptcl</ta>
            <ta e="T627" id="Seg_8778" s="T626">ptcl</ta>
            <ta e="T628" id="Seg_8779" s="T627">n-n:poss-n:case</ta>
            <ta e="T629" id="Seg_8780" s="T628">v-v:cvb</ta>
            <ta e="T630" id="Seg_8781" s="T629">v-v:tense-v:pred.pn</ta>
            <ta e="T631" id="Seg_8782" s="T630">v-v:cvb-v:pred.pn</ta>
            <ta e="T632" id="Seg_8783" s="T631">v-v:mood.pn</ta>
            <ta e="T633" id="Seg_8784" s="T632">ptcl</ta>
            <ta e="T634" id="Seg_8785" s="T633">v-v:tense-v:pred.pn</ta>
            <ta e="T635" id="Seg_8786" s="T634">ptcl</ta>
            <ta e="T636" id="Seg_8787" s="T635">v-v:tense-v:pred.pn</ta>
            <ta e="T637" id="Seg_8788" s="T636">n-n:case</ta>
            <ta e="T638" id="Seg_8789" s="T637">ptcl</ta>
            <ta e="T639" id="Seg_8790" s="T638">dempro</ta>
            <ta e="T640" id="Seg_8791" s="T639">post</ta>
            <ta e="T641" id="Seg_8792" s="T640">ptcl</ta>
            <ta e="T642" id="Seg_8793" s="T641">v-v:tense-v:pred.pn</ta>
            <ta e="T643" id="Seg_8794" s="T642">interj</ta>
            <ta e="T644" id="Seg_8795" s="T643">ptcl</ta>
            <ta e="T645" id="Seg_8796" s="T644">n-n:case</ta>
            <ta e="T646" id="Seg_8797" s="T645">v-v:tense-v:poss.pn</ta>
            <ta e="T647" id="Seg_8798" s="T646">n-n:poss-n:case</ta>
            <ta e="T648" id="Seg_8799" s="T647">v-v:cvb</ta>
            <ta e="T649" id="Seg_8800" s="T648">v-v:tense-v:pred.pn</ta>
            <ta e="T650" id="Seg_8801" s="T649">v-v:tense-v:pred.pn</ta>
            <ta e="T651" id="Seg_8802" s="T650">n-n:(num)-n:case</ta>
            <ta e="T652" id="Seg_8803" s="T651">adv</ta>
            <ta e="T653" id="Seg_8804" s="T652">n-n:case</ta>
            <ta e="T654" id="Seg_8805" s="T653">v-v:ptcp</ta>
            <ta e="T655" id="Seg_8806" s="T654">n-n:poss-n:case</ta>
            <ta e="T656" id="Seg_8807" s="T655">v-v:(ins)-v:mood.pn</ta>
            <ta e="T657" id="Seg_8808" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_8809" s="T657">que-que</ta>
            <ta e="T659" id="Seg_8810" s="T658">n-n:case</ta>
            <ta e="T660" id="Seg_8811" s="T659">v-v:tense-v:pred.pn</ta>
            <ta e="T661" id="Seg_8812" s="T660">ptcl</ta>
            <ta e="T662" id="Seg_8813" s="T661">v-v:tense-v:pred.pn</ta>
            <ta e="T663" id="Seg_8814" s="T662">n-n:case</ta>
            <ta e="T664" id="Seg_8815" s="T663">v-v:ptcp</ta>
            <ta e="T665" id="Seg_8816" s="T664">n-n:(poss)-n:case</ta>
            <ta e="T666" id="Seg_8817" s="T665">n-n:(poss)-n:case</ta>
            <ta e="T667" id="Seg_8818" s="T666">ptcl</ta>
            <ta e="T668" id="Seg_8819" s="T667">v-v:cvb</ta>
            <ta e="T669" id="Seg_8820" s="T668">v-v:tense-v:pred.pn</ta>
            <ta e="T670" id="Seg_8821" s="T669">v-v:ptcp</ta>
            <ta e="T671" id="Seg_8822" s="T670">n-n:(poss)-n:case</ta>
            <ta e="T672" id="Seg_8823" s="T671">n-n:poss-n:case</ta>
            <ta e="T673" id="Seg_8824" s="T672">v-v:tense-v:pred.pn</ta>
            <ta e="T674" id="Seg_8825" s="T673">ptcl</ta>
            <ta e="T675" id="Seg_8826" s="T674">interj</ta>
            <ta e="T676" id="Seg_8827" s="T675">dempro-dempro</ta>
            <ta e="T677" id="Seg_8828" s="T676">interj</ta>
            <ta e="T678" id="Seg_8829" s="T677">interj</ta>
            <ta e="T679" id="Seg_8830" s="T678">ptcl</ta>
            <ta e="T680" id="Seg_8831" s="T679">adv</ta>
            <ta e="T681" id="Seg_8832" s="T680">ptcl</ta>
            <ta e="T682" id="Seg_8833" s="T681">adj-n:case</ta>
            <ta e="T683" id="Seg_8834" s="T682">n-n:case</ta>
            <ta e="T684" id="Seg_8835" s="T683">v-v:tense-v:poss.pn</ta>
            <ta e="T685" id="Seg_8836" s="T684">ptcl</ta>
            <ta e="T686" id="Seg_8837" s="T685">ptcl</ta>
            <ta e="T687" id="Seg_8838" s="T686">n-n:poss-n:case</ta>
            <ta e="T688" id="Seg_8839" s="T687">v-v:tense-v:pred.pn</ta>
            <ta e="T689" id="Seg_8840" s="T688">v-v:tense-v:poss.pn</ta>
            <ta e="T690" id="Seg_8841" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_8842" s="T690">dempro</ta>
            <ta e="T692" id="Seg_8843" s="T691">n-n:case</ta>
            <ta e="T693" id="Seg_8844" s="T692">dempro-dempro&gt;adj</ta>
            <ta e="T694" id="Seg_8845" s="T693">adj</ta>
            <ta e="T695" id="Seg_8846" s="T694">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T696" id="Seg_8847" s="T695">n-n:case</ta>
            <ta e="T697" id="Seg_8848" s="T696">n-n:poss-n:case</ta>
            <ta e="T698" id="Seg_8849" s="T697">v-v:cvb</ta>
            <ta e="T699" id="Seg_8850" s="T698">v-v:tense-v:pred.pn</ta>
            <ta e="T700" id="Seg_8851" s="T699">adv</ta>
            <ta e="T701" id="Seg_8852" s="T700">v-v:cvb</ta>
            <ta e="T702" id="Seg_8853" s="T701">n-n:poss-n:case</ta>
            <ta e="T703" id="Seg_8854" s="T702">n-n:poss-n:case</ta>
            <ta e="T704" id="Seg_8855" s="T703">n-n:case</ta>
            <ta e="T705" id="Seg_8856" s="T704">v-v:tense-v:pred.pn</ta>
            <ta e="T706" id="Seg_8857" s="T705">%%</ta>
            <ta e="T707" id="Seg_8858" s="T706">v-v:cvb</ta>
            <ta e="T708" id="Seg_8859" s="T707">v-v:tense-v:pred.pn</ta>
            <ta e="T709" id="Seg_8860" s="T708">dempro</ta>
            <ta e="T710" id="Seg_8861" s="T709">n-n:case</ta>
            <ta e="T711" id="Seg_8862" s="T710">adj-adj</ta>
            <ta e="T712" id="Seg_8863" s="T711">n-n&gt;adj-que-que&gt;adj</ta>
            <ta e="T713" id="Seg_8864" s="T712">dempro</ta>
            <ta e="T714" id="Seg_8865" s="T713">n-n:case</ta>
            <ta e="T715" id="Seg_8866" s="T714">ptcl</ta>
            <ta e="T716" id="Seg_8867" s="T715">v-v:tense-v:pred.pn</ta>
            <ta e="T717" id="Seg_8868" s="T716">n-n:poss-n:case</ta>
            <ta e="T718" id="Seg_8869" s="T717">n-n:poss-n:case</ta>
            <ta e="T719" id="Seg_8870" s="T718">v-v:cvb</ta>
            <ta e="T720" id="Seg_8871" s="T719">post</ta>
            <ta e="T721" id="Seg_8872" s="T720">ptcl</ta>
            <ta e="T722" id="Seg_8873" s="T721">v-v:tense-v:pred.pn</ta>
            <ta e="T723" id="Seg_8874" s="T722">v-v:cvb</ta>
            <ta e="T724" id="Seg_8875" s="T723">dempro-pro:case</ta>
            <ta e="T725" id="Seg_8876" s="T724">v-v:cvb-v:pred.pn</ta>
            <ta e="T726" id="Seg_8877" s="T725">dempro-pro:case</ta>
            <ta e="T727" id="Seg_8878" s="T726">ptcl</ta>
            <ta e="T728" id="Seg_8879" s="T727">dempro-pro:case</ta>
            <ta e="T729" id="Seg_8880" s="T728">v-v:cvb-v-v:cvb</ta>
            <ta e="T730" id="Seg_8881" s="T729">v-v:tense-v:poss.pn</ta>
            <ta e="T731" id="Seg_8882" s="T730">ptcl</ta>
            <ta e="T732" id="Seg_8883" s="T731">dempro-pro:case</ta>
            <ta e="T733" id="Seg_8884" s="T732">adj-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_8885" s="T1">propr</ta>
            <ta e="T3" id="Seg_8886" s="T2">v</ta>
            <ta e="T4" id="Seg_8887" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_8888" s="T4">cardnum</ta>
            <ta e="T6" id="Seg_8889" s="T5">n</ta>
            <ta e="T7" id="Seg_8890" s="T6">v</ta>
            <ta e="T8" id="Seg_8891" s="T7">n</ta>
            <ta e="T9" id="Seg_8892" s="T8">adj</ta>
            <ta e="T10" id="Seg_8893" s="T9">cardnum</ta>
            <ta e="T11" id="Seg_8894" s="T10">adj</ta>
            <ta e="T12" id="Seg_8895" s="T11">adj</ta>
            <ta e="T13" id="Seg_8896" s="T12">n</ta>
            <ta e="T14" id="Seg_8897" s="T13">adj</ta>
            <ta e="T15" id="Seg_8898" s="T14">v</ta>
            <ta e="T16" id="Seg_8899" s="T15">v</ta>
            <ta e="T17" id="Seg_8900" s="T16">dempro</ta>
            <ta e="T18" id="Seg_8901" s="T17">n</ta>
            <ta e="T19" id="Seg_8902" s="T18">n</ta>
            <ta e="T20" id="Seg_8903" s="T19">adv</ta>
            <ta e="T21" id="Seg_8904" s="T20">v</ta>
            <ta e="T22" id="Seg_8905" s="T21">n</ta>
            <ta e="T23" id="Seg_8906" s="T22">adv</ta>
            <ta e="T24" id="Seg_8907" s="T23">v</ta>
            <ta e="T25" id="Seg_8908" s="T24">adv</ta>
            <ta e="T26" id="Seg_8909" s="T25">v</ta>
            <ta e="T27" id="Seg_8910" s="T26">n</ta>
            <ta e="T28" id="Seg_8911" s="T27">que</ta>
            <ta e="T29" id="Seg_8912" s="T28">v</ta>
            <ta e="T30" id="Seg_8913" s="T29">adv</ta>
            <ta e="T31" id="Seg_8914" s="T30">n</ta>
            <ta e="T32" id="Seg_8915" s="T31">dempro</ta>
            <ta e="T33" id="Seg_8916" s="T32">v</ta>
            <ta e="T34" id="Seg_8917" s="T33">n</ta>
            <ta e="T35" id="Seg_8918" s="T34">v</ta>
            <ta e="T36" id="Seg_8919" s="T35">n</ta>
            <ta e="T37" id="Seg_8920" s="T36">n</ta>
            <ta e="T38" id="Seg_8921" s="T37">v</ta>
            <ta e="T39" id="Seg_8922" s="T38">post</ta>
            <ta e="T40" id="Seg_8923" s="T39">dempro</ta>
            <ta e="T41" id="Seg_8924" s="T40">n</ta>
            <ta e="T42" id="Seg_8925" s="T41">n</ta>
            <ta e="T43" id="Seg_8926" s="T42">n</ta>
            <ta e="T44" id="Seg_8927" s="T43">v</ta>
            <ta e="T45" id="Seg_8928" s="T44">n</ta>
            <ta e="T46" id="Seg_8929" s="T45">n</ta>
            <ta e="T47" id="Seg_8930" s="T46">v</ta>
            <ta e="T48" id="Seg_8931" s="T47">aux</ta>
            <ta e="T49" id="Seg_8932" s="T48">interj</ta>
            <ta e="T50" id="Seg_8933" s="T49">n</ta>
            <ta e="T51" id="Seg_8934" s="T50">que</ta>
            <ta e="T52" id="Seg_8935" s="T51">conj</ta>
            <ta e="T53" id="Seg_8936" s="T52">cardnum</ta>
            <ta e="T54" id="Seg_8937" s="T53">n</ta>
            <ta e="T55" id="Seg_8938" s="T54">n</ta>
            <ta e="T56" id="Seg_8939" s="T55">v</ta>
            <ta e="T57" id="Seg_8940" s="T56">v</ta>
            <ta e="T58" id="Seg_8941" s="T57">interj</ta>
            <ta e="T59" id="Seg_8942" s="T58">propr</ta>
            <ta e="T60" id="Seg_8943" s="T59">n</ta>
            <ta e="T61" id="Seg_8944" s="T60">v</ta>
            <ta e="T62" id="Seg_8945" s="T61">interj</ta>
            <ta e="T63" id="Seg_8946" s="T62">que</ta>
            <ta e="T64" id="Seg_8947" s="T63">v</ta>
            <ta e="T65" id="Seg_8948" s="T64">interj</ta>
            <ta e="T66" id="Seg_8949" s="T65">n</ta>
            <ta e="T67" id="Seg_8950" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_8951" s="T67">que</ta>
            <ta e="T69" id="Seg_8952" s="T68">v</ta>
            <ta e="T70" id="Seg_8953" s="T69">pers</ta>
            <ta e="T71" id="Seg_8954" s="T70">interj</ta>
            <ta e="T72" id="Seg_8955" s="T71">pers</ta>
            <ta e="T73" id="Seg_8956" s="T72">v</ta>
            <ta e="T74" id="Seg_8957" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_8958" s="T74">v</ta>
            <ta e="T76" id="Seg_8959" s="T75">aux</ta>
            <ta e="T77" id="Seg_8960" s="T76">dempro</ta>
            <ta e="T78" id="Seg_8961" s="T77">n</ta>
            <ta e="T79" id="Seg_8962" s="T78">cop</ta>
            <ta e="T80" id="Seg_8963" s="T79">n</ta>
            <ta e="T81" id="Seg_8964" s="T80">cop</ta>
            <ta e="T82" id="Seg_8965" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_8966" s="T82">n</ta>
            <ta e="T84" id="Seg_8967" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_8968" s="T84">v</ta>
            <ta e="T86" id="Seg_8969" s="T85">dempro</ta>
            <ta e="T87" id="Seg_8970" s="T86">v</ta>
            <ta e="T88" id="Seg_8971" s="T87">aux</ta>
            <ta e="T89" id="Seg_8972" s="T88">n</ta>
            <ta e="T90" id="Seg_8973" s="T89">dempro</ta>
            <ta e="T91" id="Seg_8974" s="T90">n</ta>
            <ta e="T92" id="Seg_8975" s="T91">interj</ta>
            <ta e="T95" id="Seg_8976" s="T94">v</ta>
            <ta e="T96" id="Seg_8977" s="T95">v</ta>
            <ta e="T97" id="Seg_8978" s="T96">v</ta>
            <ta e="T100" id="Seg_8979" s="T99">v</ta>
            <ta e="T101" id="Seg_8980" s="T100">v</ta>
            <ta e="T102" id="Seg_8981" s="T101">adv</ta>
            <ta e="T103" id="Seg_8982" s="T102">post</ta>
            <ta e="T104" id="Seg_8983" s="T103">dempro</ta>
            <ta e="T105" id="Seg_8984" s="T104">v</ta>
            <ta e="T106" id="Seg_8985" s="T105">conj</ta>
            <ta e="T107" id="Seg_8986" s="T106">n</ta>
            <ta e="T108" id="Seg_8987" s="T107">v</ta>
            <ta e="T109" id="Seg_8988" s="T108">conj</ta>
            <ta e="T110" id="Seg_8989" s="T109">v</ta>
            <ta e="T111" id="Seg_8990" s="T110">aux</ta>
            <ta e="T112" id="Seg_8991" s="T111">adv</ta>
            <ta e="T113" id="Seg_8992" s="T112">n</ta>
            <ta e="T114" id="Seg_8993" s="T113">post</ta>
            <ta e="T115" id="Seg_8994" s="T114">v</ta>
            <ta e="T116" id="Seg_8995" s="T115">aux</ta>
            <ta e="T117" id="Seg_8996" s="T116">n</ta>
            <ta e="T118" id="Seg_8997" s="T117">v</ta>
            <ta e="T119" id="Seg_8998" s="T118">interj</ta>
            <ta e="T120" id="Seg_8999" s="T119">propr</ta>
            <ta e="T121" id="Seg_9000" s="T120">que</ta>
            <ta e="T122" id="Seg_9001" s="T121">que</ta>
            <ta e="T123" id="Seg_9002" s="T122">n</ta>
            <ta e="T124" id="Seg_9003" s="T123">v</ta>
            <ta e="T125" id="Seg_9004" s="T124">n</ta>
            <ta e="T126" id="Seg_9005" s="T125">v</ta>
            <ta e="T127" id="Seg_9006" s="T126">v</ta>
            <ta e="T128" id="Seg_9007" s="T127">interj</ta>
            <ta e="T129" id="Seg_9008" s="T128">n</ta>
            <ta e="T130" id="Seg_9009" s="T129">que</ta>
            <ta e="T131" id="Seg_9010" s="T130">adj</ta>
            <ta e="T132" id="Seg_9011" s="T131">v</ta>
            <ta e="T133" id="Seg_9012" s="T132">n</ta>
            <ta e="T134" id="Seg_9013" s="T133">v</ta>
            <ta e="T135" id="Seg_9014" s="T134">aux</ta>
            <ta e="T138" id="Seg_9015" s="T136">dempro</ta>
            <ta e="T139" id="Seg_9016" s="T138">ptcl</ta>
            <ta e="T141" id="Seg_9017" s="T139">interj</ta>
            <ta e="T142" id="Seg_9018" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_9019" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_9020" s="T143">v</ta>
            <ta e="T145" id="Seg_9021" s="T144">interj</ta>
            <ta e="T146" id="Seg_9022" s="T145">n</ta>
            <ta e="T147" id="Seg_9023" s="T146">post</ta>
            <ta e="T148" id="Seg_9024" s="T147">v</ta>
            <ta e="T149" id="Seg_9025" s="T148">n</ta>
            <ta e="T150" id="Seg_9026" s="T149">que</ta>
            <ta e="T151" id="Seg_9027" s="T150">v</ta>
            <ta e="T152" id="Seg_9028" s="T151">dempro</ta>
            <ta e="T153" id="Seg_9029" s="T152">n</ta>
            <ta e="T154" id="Seg_9030" s="T153">dempro</ta>
            <ta e="T155" id="Seg_9031" s="T154">v</ta>
            <ta e="T156" id="Seg_9032" s="T155">conj</ta>
            <ta e="T157" id="Seg_9033" s="T156">ptcl</ta>
            <ta e="T159" id="Seg_9034" s="T158">n</ta>
            <ta e="T160" id="Seg_9035" s="T159">v</ta>
            <ta e="T161" id="Seg_9036" s="T160">aux</ta>
            <ta e="T162" id="Seg_9037" s="T161">v</ta>
            <ta e="T163" id="Seg_9038" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_9039" s="T163">v</ta>
            <ta e="T165" id="Seg_9040" s="T164">aux</ta>
            <ta e="T166" id="Seg_9041" s="T165">v</ta>
            <ta e="T167" id="Seg_9042" s="T166">aux</ta>
            <ta e="T168" id="Seg_9043" s="T167">n</ta>
            <ta e="T169" id="Seg_9044" s="T168">v</ta>
            <ta e="T170" id="Seg_9045" s="T169">v</ta>
            <ta e="T171" id="Seg_9046" s="T170">interj</ta>
            <ta e="T172" id="Seg_9047" s="T171">propr</ta>
            <ta e="T173" id="Seg_9048" s="T172">n</ta>
            <ta e="T174" id="Seg_9049" s="T173">v</ta>
            <ta e="T175" id="Seg_9050" s="T174">que</ta>
            <ta e="T176" id="Seg_9051" s="T175">que</ta>
            <ta e="T177" id="Seg_9052" s="T176">n</ta>
            <ta e="T178" id="Seg_9053" s="T177">v</ta>
            <ta e="T179" id="Seg_9054" s="T178">dempro</ta>
            <ta e="T180" id="Seg_9055" s="T179">v</ta>
            <ta e="T181" id="Seg_9056" s="T180">interj</ta>
            <ta e="T182" id="Seg_9057" s="T181">v</ta>
            <ta e="T183" id="Seg_9058" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_9059" s="T183">n</ta>
            <ta e="T185" id="Seg_9060" s="T184">adj</ta>
            <ta e="T186" id="Seg_9061" s="T185">v</ta>
            <ta e="T187" id="Seg_9062" s="T186">dempro</ta>
            <ta e="T188" id="Seg_9063" s="T187">post</ta>
            <ta e="T189" id="Seg_9064" s="T188">v</ta>
            <ta e="T190" id="Seg_9065" s="T189">aux</ta>
            <ta e="T191" id="Seg_9066" s="T190">interj</ta>
            <ta e="T192" id="Seg_9067" s="T191">pers</ta>
            <ta e="T193" id="Seg_9068" s="T192">n</ta>
            <ta e="T194" id="Seg_9069" s="T193">pers</ta>
            <ta e="T195" id="Seg_9070" s="T194">v</ta>
            <ta e="T196" id="Seg_9071" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_9072" s="T196">adv</ta>
            <ta e="T198" id="Seg_9073" s="T197">adv</ta>
            <ta e="T199" id="Seg_9074" s="T198">pers</ta>
            <ta e="T200" id="Seg_9075" s="T199">pers</ta>
            <ta e="T201" id="Seg_9076" s="T200">v</ta>
            <ta e="T202" id="Seg_9077" s="T201">v</ta>
            <ta e="T203" id="Seg_9078" s="T202">v</ta>
            <ta e="T204" id="Seg_9079" s="T203">aux</ta>
            <ta e="T205" id="Seg_9080" s="T204">v</ta>
            <ta e="T206" id="Seg_9081" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_9082" s="T206">v</ta>
            <ta e="T208" id="Seg_9083" s="T207">dempro</ta>
            <ta e="T209" id="Seg_9084" s="T208">n</ta>
            <ta e="T210" id="Seg_9085" s="T209">propr</ta>
            <ta e="T211" id="Seg_9086" s="T210">que</ta>
            <ta e="T212" id="Seg_9087" s="T211">v</ta>
            <ta e="T213" id="Seg_9088" s="T212">v</ta>
            <ta e="T214" id="Seg_9089" s="T213">aux</ta>
            <ta e="T215" id="Seg_9090" s="T214">conj</ta>
            <ta e="T216" id="Seg_9091" s="T215">v</ta>
            <ta e="T217" id="Seg_9092" s="T216">aux</ta>
            <ta e="T218" id="Seg_9093" s="T217">dempro</ta>
            <ta e="T219" id="Seg_9094" s="T218">v</ta>
            <ta e="T220" id="Seg_9095" s="T219">conj</ta>
            <ta e="T221" id="Seg_9096" s="T220">n</ta>
            <ta e="T222" id="Seg_9097" s="T221">n</ta>
            <ta e="T223" id="Seg_9098" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_9099" s="T223">adj</ta>
            <ta e="T225" id="Seg_9100" s="T224">v</ta>
            <ta e="T226" id="Seg_9101" s="T225">n</ta>
            <ta e="T227" id="Seg_9102" s="T226">v</ta>
            <ta e="T228" id="Seg_9103" s="T227">aux</ta>
            <ta e="T229" id="Seg_9104" s="T228">n</ta>
            <ta e="T230" id="Seg_9105" s="T229">adv</ta>
            <ta e="T231" id="Seg_9106" s="T230">v</ta>
            <ta e="T232" id="Seg_9107" s="T231">aux</ta>
            <ta e="T233" id="Seg_9108" s="T232">v</ta>
            <ta e="T234" id="Seg_9109" s="T233">v</ta>
            <ta e="T235" id="Seg_9110" s="T234">aux</ta>
            <ta e="T236" id="Seg_9111" s="T235">n</ta>
            <ta e="T237" id="Seg_9112" s="T236">v</ta>
            <ta e="T238" id="Seg_9113" s="T237">v</ta>
            <ta e="T239" id="Seg_9114" s="T238">interj</ta>
            <ta e="T240" id="Seg_9115" s="T239">propr</ta>
            <ta e="T241" id="Seg_9116" s="T240">n</ta>
            <ta e="T242" id="Seg_9117" s="T241">v</ta>
            <ta e="T243" id="Seg_9118" s="T242">adv</ta>
            <ta e="T244" id="Seg_9119" s="T243">v</ta>
            <ta e="T245" id="Seg_9120" s="T244">que</ta>
            <ta e="T246" id="Seg_9121" s="T245">v</ta>
            <ta e="T247" id="Seg_9122" s="T246">dempro</ta>
            <ta e="T248" id="Seg_9123" s="T247">v</ta>
            <ta e="T249" id="Seg_9124" s="T248">n</ta>
            <ta e="T250" id="Seg_9125" s="T249">interj</ta>
            <ta e="T251" id="Seg_9126" s="T250">ptcl</ta>
            <ta e="T252" id="Seg_9127" s="T251">n</ta>
            <ta e="T253" id="Seg_9128" s="T252">que</ta>
            <ta e="T254" id="Seg_9129" s="T253">adj</ta>
            <ta e="T255" id="Seg_9130" s="T254">v</ta>
            <ta e="T256" id="Seg_9131" s="T255">dempro</ta>
            <ta e="T257" id="Seg_9132" s="T256">v</ta>
            <ta e="T258" id="Seg_9133" s="T257">aux</ta>
            <ta e="T259" id="Seg_9134" s="T258">v</ta>
            <ta e="T260" id="Seg_9135" s="T259">adv</ta>
            <ta e="T261" id="Seg_9136" s="T260">v</ta>
            <ta e="T262" id="Seg_9137" s="T261">v</ta>
            <ta e="T263" id="Seg_9138" s="T262">aux</ta>
            <ta e="T264" id="Seg_9139" s="T263">conj</ta>
            <ta e="T265" id="Seg_9140" s="T264">dempro</ta>
            <ta e="T266" id="Seg_9141" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_9142" s="T266">v</ta>
            <ta e="T268" id="Seg_9143" s="T267">v</ta>
            <ta e="T269" id="Seg_9144" s="T268">propr</ta>
            <ta e="T270" id="Seg_9145" s="T269">que</ta>
            <ta e="T271" id="Seg_9146" s="T270">v</ta>
            <ta e="T272" id="Seg_9147" s="T271">v</ta>
            <ta e="T273" id="Seg_9148" s="T272">v</ta>
            <ta e="T274" id="Seg_9149" s="T273">conj</ta>
            <ta e="T275" id="Seg_9150" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_9151" s="T275">v</ta>
            <ta e="T277" id="Seg_9152" s="T276">conj</ta>
            <ta e="T278" id="Seg_9153" s="T277">n</ta>
            <ta e="T279" id="Seg_9154" s="T278">v</ta>
            <ta e="T280" id="Seg_9155" s="T279">aux</ta>
            <ta e="T281" id="Seg_9156" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_9157" s="T281">v</ta>
            <ta e="T283" id="Seg_9158" s="T282">aux</ta>
            <ta e="T284" id="Seg_9159" s="T283">v</ta>
            <ta e="T285" id="Seg_9160" s="T284">n</ta>
            <ta e="T286" id="Seg_9161" s="T285">v</ta>
            <ta e="T287" id="Seg_9162" s="T286">n</ta>
            <ta e="T288" id="Seg_9163" s="T287">v</ta>
            <ta e="T289" id="Seg_9164" s="T288">v</ta>
            <ta e="T290" id="Seg_9165" s="T289">interj</ta>
            <ta e="T291" id="Seg_9166" s="T290">propr</ta>
            <ta e="T292" id="Seg_9167" s="T291">n</ta>
            <ta e="T293" id="Seg_9168" s="T292">v</ta>
            <ta e="T294" id="Seg_9169" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_9170" s="T294">que</ta>
            <ta e="T296" id="Seg_9171" s="T295">v</ta>
            <ta e="T297" id="Seg_9172" s="T296">v</ta>
            <ta e="T298" id="Seg_9173" s="T297">interj</ta>
            <ta e="T299" id="Seg_9174" s="T298">n</ta>
            <ta e="T300" id="Seg_9175" s="T299">que</ta>
            <ta e="T301" id="Seg_9176" s="T300">v</ta>
            <ta e="T302" id="Seg_9177" s="T301">dempro</ta>
            <ta e="T303" id="Seg_9178" s="T302">v</ta>
            <ta e="T304" id="Seg_9179" s="T303">interj</ta>
            <ta e="T305" id="Seg_9180" s="T304">pers</ta>
            <ta e="T306" id="Seg_9181" s="T305">n</ta>
            <ta e="T307" id="Seg_9182" s="T306">v</ta>
            <ta e="T308" id="Seg_9183" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_9184" s="T308">adv</ta>
            <ta e="T310" id="Seg_9185" s="T309">pers</ta>
            <ta e="T311" id="Seg_9186" s="T310">pers</ta>
            <ta e="T312" id="Seg_9187" s="T311">v</ta>
            <ta e="T313" id="Seg_9188" s="T312">aux</ta>
            <ta e="T314" id="Seg_9189" s="T313">v</ta>
            <ta e="T315" id="Seg_9190" s="T314">dempro</ta>
            <ta e="T316" id="Seg_9191" s="T315">v</ta>
            <ta e="T317" id="Seg_9192" s="T316">aux</ta>
            <ta e="T318" id="Seg_9193" s="T317">v</ta>
            <ta e="T319" id="Seg_9194" s="T318">aux</ta>
            <ta e="T320" id="Seg_9195" s="T319">propr</ta>
            <ta e="T321" id="Seg_9196" s="T320">propr</ta>
            <ta e="T322" id="Seg_9197" s="T321">n</ta>
            <ta e="T323" id="Seg_9198" s="T322">v</ta>
            <ta e="T324" id="Seg_9199" s="T323">aux</ta>
            <ta e="T325" id="Seg_9200" s="T324">conj</ta>
            <ta e="T326" id="Seg_9201" s="T325">v</ta>
            <ta e="T327" id="Seg_9202" s="T326">v</ta>
            <ta e="T328" id="Seg_9203" s="T327">adv</ta>
            <ta e="T329" id="Seg_9204" s="T328">v</ta>
            <ta e="T330" id="Seg_9205" s="T329">conj</ta>
            <ta e="T331" id="Seg_9206" s="T330">v</ta>
            <ta e="T332" id="Seg_9207" s="T331">conj</ta>
            <ta e="T333" id="Seg_9208" s="T332">n</ta>
            <ta e="T334" id="Seg_9209" s="T333">v</ta>
            <ta e="T335" id="Seg_9210" s="T334">aux</ta>
            <ta e="T336" id="Seg_9211" s="T335">adv</ta>
            <ta e="T337" id="Seg_9212" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_9213" s="T337">v</ta>
            <ta e="T339" id="Seg_9214" s="T338">aux</ta>
            <ta e="T340" id="Seg_9215" s="T339">v</ta>
            <ta e="T341" id="Seg_9216" s="T340">aux</ta>
            <ta e="T342" id="Seg_9217" s="T341">interj</ta>
            <ta e="T343" id="Seg_9218" s="T342">n</ta>
            <ta e="T344" id="Seg_9219" s="T343">v</ta>
            <ta e="T345" id="Seg_9220" s="T344">v</ta>
            <ta e="T346" id="Seg_9221" s="T345">ptcl</ta>
            <ta e="T347" id="Seg_9222" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_9223" s="T347">ptcl</ta>
            <ta e="T349" id="Seg_9224" s="T348">interj</ta>
            <ta e="T350" id="Seg_9225" s="T349">propr</ta>
            <ta e="T351" id="Seg_9226" s="T350">que</ta>
            <ta e="T352" id="Seg_9227" s="T351">que</ta>
            <ta e="T353" id="Seg_9228" s="T352">n</ta>
            <ta e="T354" id="Seg_9229" s="T353">v</ta>
            <ta e="T355" id="Seg_9230" s="T354">dempro</ta>
            <ta e="T356" id="Seg_9231" s="T355">v</ta>
            <ta e="T357" id="Seg_9232" s="T356">n</ta>
            <ta e="T358" id="Seg_9233" s="T357">que</ta>
            <ta e="T359" id="Seg_9234" s="T358">v</ta>
            <ta e="T360" id="Seg_9235" s="T359">adv</ta>
            <ta e="T361" id="Seg_9236" s="T360">n</ta>
            <ta e="T362" id="Seg_9237" s="T361">v</ta>
            <ta e="T364" id="Seg_9238" s="T362">v</ta>
            <ta e="T365" id="Seg_9239" s="T364">pers</ta>
            <ta e="T366" id="Seg_9240" s="T365">pers</ta>
            <ta e="T367" id="Seg_9241" s="T366">v</ta>
            <ta e="T368" id="Seg_9242" s="T367">aux</ta>
            <ta e="T369" id="Seg_9243" s="T368">v</ta>
            <ta e="T370" id="Seg_9244" s="T369">v</ta>
            <ta e="T371" id="Seg_9245" s="T370">aux</ta>
            <ta e="T372" id="Seg_9246" s="T371">n</ta>
            <ta e="T373" id="Seg_9247" s="T372">v</ta>
            <ta e="T374" id="Seg_9248" s="T373">aux</ta>
            <ta e="T375" id="Seg_9249" s="T374">conj</ta>
            <ta e="T376" id="Seg_9250" s="T375">v</ta>
            <ta e="T377" id="Seg_9251" s="T376">v</ta>
            <ta e="T378" id="Seg_9252" s="T377">dempro</ta>
            <ta e="T379" id="Seg_9253" s="T378">n</ta>
            <ta e="T380" id="Seg_9254" s="T379">adv</ta>
            <ta e="T381" id="Seg_9255" s="T380">post</ta>
            <ta e="T382" id="Seg_9256" s="T381">n</ta>
            <ta e="T383" id="Seg_9257" s="T382">v</ta>
            <ta e="T384" id="Seg_9258" s="T383">post</ta>
            <ta e="T385" id="Seg_9259" s="T384">ptcl</ta>
            <ta e="T386" id="Seg_9260" s="T385">v</ta>
            <ta e="T387" id="Seg_9261" s="T386">adv</ta>
            <ta e="T388" id="Seg_9262" s="T387">dempro</ta>
            <ta e="T389" id="Seg_9263" s="T388">v</ta>
            <ta e="T390" id="Seg_9264" s="T389">v</ta>
            <ta e="T391" id="Seg_9265" s="T390">dempro</ta>
            <ta e="T392" id="Seg_9266" s="T391">n</ta>
            <ta e="T393" id="Seg_9267" s="T392">n</ta>
            <ta e="T394" id="Seg_9268" s="T393">v</ta>
            <ta e="T395" id="Seg_9269" s="T394">v</ta>
            <ta e="T396" id="Seg_9270" s="T395">n</ta>
            <ta e="T397" id="Seg_9271" s="T396">adj</ta>
            <ta e="T398" id="Seg_9272" s="T397">adj</ta>
            <ta e="T399" id="Seg_9273" s="T398">v</ta>
            <ta e="T400" id="Seg_9274" s="T399">dempro</ta>
            <ta e="T401" id="Seg_9275" s="T400">v</ta>
            <ta e="T402" id="Seg_9276" s="T401">v</ta>
            <ta e="T403" id="Seg_9277" s="T402">ptcl</ta>
            <ta e="T404" id="Seg_9278" s="T403">adj</ta>
            <ta e="T405" id="Seg_9279" s="T404">adj</ta>
            <ta e="T406" id="Seg_9280" s="T405">n</ta>
            <ta e="T407" id="Seg_9281" s="T406">ptcl</ta>
            <ta e="T408" id="Seg_9282" s="T407">v</ta>
            <ta e="T409" id="Seg_9283" s="T408">ptcl</ta>
            <ta e="T410" id="Seg_9284" s="T409">n</ta>
            <ta e="T411" id="Seg_9285" s="T410">v</ta>
            <ta e="T412" id="Seg_9286" s="T411">dempro</ta>
            <ta e="T413" id="Seg_9287" s="T412">v</ta>
            <ta e="T414" id="Seg_9288" s="T413">dempro</ta>
            <ta e="T415" id="Seg_9289" s="T414">n</ta>
            <ta e="T416" id="Seg_9290" s="T415">v</ta>
            <ta e="T417" id="Seg_9291" s="T416">ptcl</ta>
            <ta e="T418" id="Seg_9292" s="T417">v</ta>
            <ta e="T419" id="Seg_9293" s="T418">adj</ta>
            <ta e="T420" id="Seg_9294" s="T419">ptcl</ta>
            <ta e="T421" id="Seg_9295" s="T420">v</ta>
            <ta e="T422" id="Seg_9296" s="T421">dempro</ta>
            <ta e="T423" id="Seg_9297" s="T422">n</ta>
            <ta e="T424" id="Seg_9298" s="T423">v</ta>
            <ta e="T425" id="Seg_9299" s="T424">cardnum</ta>
            <ta e="T426" id="Seg_9300" s="T425">n</ta>
            <ta e="T427" id="Seg_9301" s="T426">v</ta>
            <ta e="T428" id="Seg_9302" s="T427">v</ta>
            <ta e="T429" id="Seg_9303" s="T428">v</ta>
            <ta e="T430" id="Seg_9304" s="T429">n</ta>
            <ta e="T431" id="Seg_9305" s="T430">adj</ta>
            <ta e="T432" id="Seg_9306" s="T431">n</ta>
            <ta e="T433" id="Seg_9307" s="T432">v</ta>
            <ta e="T434" id="Seg_9308" s="T433">adv</ta>
            <ta e="T435" id="Seg_9309" s="T434">v</ta>
            <ta e="T436" id="Seg_9310" s="T435">adv</ta>
            <ta e="T437" id="Seg_9311" s="T436">dempro</ta>
            <ta e="T438" id="Seg_9312" s="T437">n</ta>
            <ta e="T439" id="Seg_9313" s="T438">v</ta>
            <ta e="T440" id="Seg_9314" s="T439">v</ta>
            <ta e="T441" id="Seg_9315" s="T440">n</ta>
            <ta e="T442" id="Seg_9316" s="T441">v</ta>
            <ta e="T443" id="Seg_9317" s="T442">n</ta>
            <ta e="T444" id="Seg_9318" s="T443">n</ta>
            <ta e="T445" id="Seg_9319" s="T444">v</ta>
            <ta e="T446" id="Seg_9320" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_9321" s="T446">n</ta>
            <ta e="T448" id="Seg_9322" s="T447">post</ta>
            <ta e="T449" id="Seg_9323" s="T448">adj</ta>
            <ta e="T450" id="Seg_9324" s="T449">ptcl</ta>
            <ta e="T451" id="Seg_9325" s="T450">adj</ta>
            <ta e="T452" id="Seg_9326" s="T451">n</ta>
            <ta e="T453" id="Seg_9327" s="T452">v</ta>
            <ta e="T454" id="Seg_9328" s="T453">n</ta>
            <ta e="T455" id="Seg_9329" s="T454">v</ta>
            <ta e="T456" id="Seg_9330" s="T455">ptcl</ta>
            <ta e="T457" id="Seg_9331" s="T456">dempro</ta>
            <ta e="T458" id="Seg_9332" s="T457">v</ta>
            <ta e="T459" id="Seg_9333" s="T458">adj</ta>
            <ta e="T460" id="Seg_9334" s="T459">n</ta>
            <ta e="T461" id="Seg_9335" s="T460">n</ta>
            <ta e="T462" id="Seg_9336" s="T461">v</ta>
            <ta e="T463" id="Seg_9337" s="T462">aux</ta>
            <ta e="T464" id="Seg_9338" s="T463">adj</ta>
            <ta e="T465" id="Seg_9339" s="T464">n</ta>
            <ta e="T466" id="Seg_9340" s="T465">n</ta>
            <ta e="T467" id="Seg_9341" s="T466">n</ta>
            <ta e="T468" id="Seg_9342" s="T467">ptcl</ta>
            <ta e="T469" id="Seg_9343" s="T468">n</ta>
            <ta e="T470" id="Seg_9344" s="T469">quant</ta>
            <ta e="T471" id="Seg_9345" s="T470">adj</ta>
            <ta e="T472" id="Seg_9346" s="T471">ptcl</ta>
            <ta e="T473" id="Seg_9347" s="T472">ptcl</ta>
            <ta e="T474" id="Seg_9348" s="T473">v</ta>
            <ta e="T475" id="Seg_9349" s="T474">dempro</ta>
            <ta e="T476" id="Seg_9350" s="T475">v</ta>
            <ta e="T477" id="Seg_9351" s="T476">dempro</ta>
            <ta e="T478" id="Seg_9352" s="T477">n</ta>
            <ta e="T479" id="Seg_9353" s="T478">que</ta>
            <ta e="T480" id="Seg_9354" s="T479">n</ta>
            <ta e="T481" id="Seg_9355" s="T480">v</ta>
            <ta e="T482" id="Seg_9356" s="T481">v</ta>
            <ta e="T483" id="Seg_9357" s="T482">n</ta>
            <ta e="T484" id="Seg_9358" s="T483">v</ta>
            <ta e="T485" id="Seg_9359" s="T484">pers</ta>
            <ta e="T487" id="Seg_9360" s="T486">cardnum</ta>
            <ta e="T488" id="Seg_9361" s="T487">adj</ta>
            <ta e="T489" id="Seg_9362" s="T488">adj</ta>
            <ta e="T490" id="Seg_9363" s="T489">cop</ta>
            <ta e="T491" id="Seg_9364" s="T490">dempro</ta>
            <ta e="T492" id="Seg_9365" s="T491">n</ta>
            <ta e="T493" id="Seg_9366" s="T492">v</ta>
            <ta e="T494" id="Seg_9367" s="T493">v</ta>
            <ta e="T495" id="Seg_9368" s="T494">n</ta>
            <ta e="T496" id="Seg_9369" s="T495">v</ta>
            <ta e="T497" id="Seg_9370" s="T496">v</ta>
            <ta e="T498" id="Seg_9371" s="T497">v</ta>
            <ta e="T499" id="Seg_9372" s="T498">interj</ta>
            <ta e="T502" id="Seg_9373" s="T501">n</ta>
            <ta e="T503" id="Seg_9374" s="T502">v</ta>
            <ta e="T504" id="Seg_9375" s="T503">v</ta>
            <ta e="T505" id="Seg_9376" s="T504">dempro</ta>
            <ta e="T506" id="Seg_9377" s="T505">n</ta>
            <ta e="T507" id="Seg_9378" s="T506">v</ta>
            <ta e="T508" id="Seg_9379" s="T507">n</ta>
            <ta e="T509" id="Seg_9380" s="T508">adj</ta>
            <ta e="T510" id="Seg_9381" s="T509">ptcl</ta>
            <ta e="T511" id="Seg_9382" s="T510">v</ta>
            <ta e="T512" id="Seg_9383" s="T511">adj</ta>
            <ta e="T513" id="Seg_9384" s="T512">adj</ta>
            <ta e="T514" id="Seg_9385" s="T513">que</ta>
            <ta e="T515" id="Seg_9386" s="T514">ptcl</ta>
            <ta e="T517" id="Seg_9387" s="T516">n</ta>
            <ta e="T518" id="Seg_9388" s="T517">v</ta>
            <ta e="T519" id="Seg_9389" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_9390" s="T519">v</ta>
            <ta e="T521" id="Seg_9391" s="T520">n</ta>
            <ta e="T522" id="Seg_9392" s="T521">pers</ta>
            <ta e="T523" id="Seg_9393" s="T522">n</ta>
            <ta e="T524" id="Seg_9394" s="T523">v</ta>
            <ta e="T525" id="Seg_9395" s="T524">ptcl</ta>
            <ta e="T526" id="Seg_9396" s="T525">ptcl</ta>
            <ta e="T527" id="Seg_9397" s="T526">v</ta>
            <ta e="T528" id="Seg_9398" s="T527">n</ta>
            <ta e="T529" id="Seg_9399" s="T528">n</ta>
            <ta e="T530" id="Seg_9400" s="T529">adv</ta>
            <ta e="T531" id="Seg_9401" s="T530">v</ta>
            <ta e="T532" id="Seg_9402" s="T531">v</ta>
            <ta e="T533" id="Seg_9403" s="T532">v</ta>
            <ta e="T534" id="Seg_9404" s="T533">cardnum</ta>
            <ta e="T535" id="Seg_9405" s="T534">n</ta>
            <ta e="T536" id="Seg_9406" s="T535">v</ta>
            <ta e="T537" id="Seg_9407" s="T536">n</ta>
            <ta e="T538" id="Seg_9408" s="T537">v</ta>
            <ta e="T539" id="Seg_9409" s="T538">v</ta>
            <ta e="T540" id="Seg_9410" s="T539">v</ta>
            <ta e="T541" id="Seg_9411" s="T540">v</ta>
            <ta e="T542" id="Seg_9412" s="T541">dempro</ta>
            <ta e="T543" id="Seg_9413" s="T542">n</ta>
            <ta e="T544" id="Seg_9414" s="T543">v</ta>
            <ta e="T545" id="Seg_9415" s="T544">aux</ta>
            <ta e="T546" id="Seg_9416" s="T545">n</ta>
            <ta e="T547" id="Seg_9417" s="T546">v</ta>
            <ta e="T548" id="Seg_9418" s="T547">aux</ta>
            <ta e="T549" id="Seg_9419" s="T548">interj</ta>
            <ta e="T550" id="Seg_9420" s="T549">que</ta>
            <ta e="T551" id="Seg_9421" s="T550">v</ta>
            <ta e="T552" id="Seg_9422" s="T551">ptcl</ta>
            <ta e="T553" id="Seg_9423" s="T552">pers</ta>
            <ta e="T554" id="Seg_9424" s="T553">adj</ta>
            <ta e="T555" id="Seg_9425" s="T554">que</ta>
            <ta e="T556" id="Seg_9426" s="T555">n</ta>
            <ta e="T557" id="Seg_9427" s="T556">v</ta>
            <ta e="T558" id="Seg_9428" s="T557">ptcl</ta>
            <ta e="T559" id="Seg_9429" s="T558">v</ta>
            <ta e="T560" id="Seg_9430" s="T559">dempro</ta>
            <ta e="T561" id="Seg_9431" s="T560">n</ta>
            <ta e="T562" id="Seg_9432" s="T561">n</ta>
            <ta e="T563" id="Seg_9433" s="T562">v</ta>
            <ta e="T564" id="Seg_9434" s="T563">aux</ta>
            <ta e="T565" id="Seg_9435" s="T564">v</ta>
            <ta e="T566" id="Seg_9436" s="T565">adv</ta>
            <ta e="T567" id="Seg_9437" s="T566">v</ta>
            <ta e="T568" id="Seg_9438" s="T567">dempro</ta>
            <ta e="T569" id="Seg_9439" s="T568">n</ta>
            <ta e="T570" id="Seg_9440" s="T569">adj</ta>
            <ta e="T571" id="Seg_9441" s="T570">n</ta>
            <ta e="T572" id="Seg_9442" s="T571">v</ta>
            <ta e="T573" id="Seg_9443" s="T572">adv</ta>
            <ta e="T574" id="Seg_9444" s="T573">v</ta>
            <ta e="T575" id="Seg_9445" s="T574">v</ta>
            <ta e="T576" id="Seg_9446" s="T575">n</ta>
            <ta e="T577" id="Seg_9447" s="T576">dempro</ta>
            <ta e="T578" id="Seg_9448" s="T577">n</ta>
            <ta e="T579" id="Seg_9449" s="T578">adj</ta>
            <ta e="T580" id="Seg_9450" s="T579">n</ta>
            <ta e="T581" id="Seg_9451" s="T580">v</ta>
            <ta e="T582" id="Seg_9452" s="T581">interj</ta>
            <ta e="T583" id="Seg_9453" s="T582">pers</ta>
            <ta e="T584" id="Seg_9454" s="T583">dempro</ta>
            <ta e="T585" id="Seg_9455" s="T584">n</ta>
            <ta e="T586" id="Seg_9456" s="T585">v</ta>
            <ta e="T587" id="Seg_9457" s="T586">n</ta>
            <ta e="T588" id="Seg_9458" s="T587">v</ta>
            <ta e="T589" id="Seg_9459" s="T588">post</ta>
            <ta e="T590" id="Seg_9460" s="T589">n</ta>
            <ta e="T591" id="Seg_9461" s="T590">v</ta>
            <ta e="T592" id="Seg_9462" s="T591">v</ta>
            <ta e="T593" id="Seg_9463" s="T592">interj</ta>
            <ta e="T594" id="Seg_9464" s="T593">dempro</ta>
            <ta e="T595" id="Seg_9465" s="T594">n</ta>
            <ta e="T596" id="Seg_9466" s="T595">n</ta>
            <ta e="T597" id="Seg_9467" s="T596">v</ta>
            <ta e="T598" id="Seg_9468" s="T597">n</ta>
            <ta e="T599" id="Seg_9469" s="T598">v</ta>
            <ta e="T600" id="Seg_9470" s="T599">aux</ta>
            <ta e="T601" id="Seg_9471" s="T600">n</ta>
            <ta e="T602" id="Seg_9472" s="T601">v</ta>
            <ta e="T603" id="Seg_9473" s="T602">aux</ta>
            <ta e="T604" id="Seg_9474" s="T603">adv</ta>
            <ta e="T605" id="Seg_9475" s="T604">n</ta>
            <ta e="T607" id="Seg_9476" s="T606">dempro</ta>
            <ta e="T608" id="Seg_9477" s="T607">n</ta>
            <ta e="T609" id="Seg_9478" s="T608">n</ta>
            <ta e="T610" id="Seg_9479" s="T609">cop</ta>
            <ta e="T611" id="Seg_9480" s="T610">v</ta>
            <ta e="T612" id="Seg_9481" s="T611">v</ta>
            <ta e="T613" id="Seg_9482" s="T612">v</ta>
            <ta e="T614" id="Seg_9483" s="T613">v</ta>
            <ta e="T615" id="Seg_9484" s="T614">n</ta>
            <ta e="T616" id="Seg_9485" s="T615">ptcl</ta>
            <ta e="T617" id="Seg_9486" s="T616">v</ta>
            <ta e="T618" id="Seg_9487" s="T617">ptcl</ta>
            <ta e="T619" id="Seg_9488" s="T618">v</ta>
            <ta e="T620" id="Seg_9489" s="T619">n</ta>
            <ta e="T621" id="Seg_9490" s="T620">adj</ta>
            <ta e="T622" id="Seg_9491" s="T621">n</ta>
            <ta e="T623" id="Seg_9492" s="T622">v</ta>
            <ta e="T624" id="Seg_9493" s="T623">ptcl</ta>
            <ta e="T625" id="Seg_9494" s="T624">que</ta>
            <ta e="T626" id="Seg_9495" s="T625">ptcl</ta>
            <ta e="T627" id="Seg_9496" s="T626">ptcl</ta>
            <ta e="T628" id="Seg_9497" s="T627">n</ta>
            <ta e="T629" id="Seg_9498" s="T628">v</ta>
            <ta e="T630" id="Seg_9499" s="T629">aux</ta>
            <ta e="T631" id="Seg_9500" s="T630">v</ta>
            <ta e="T632" id="Seg_9501" s="T631">v</ta>
            <ta e="T633" id="Seg_9502" s="T632">ptcl</ta>
            <ta e="T634" id="Seg_9503" s="T633">v</ta>
            <ta e="T635" id="Seg_9504" s="T634">ptcl</ta>
            <ta e="T636" id="Seg_9505" s="T635">v</ta>
            <ta e="T637" id="Seg_9506" s="T636">n</ta>
            <ta e="T638" id="Seg_9507" s="T637">ptcl</ta>
            <ta e="T639" id="Seg_9508" s="T638">dempro</ta>
            <ta e="T640" id="Seg_9509" s="T639">post</ta>
            <ta e="T641" id="Seg_9510" s="T640">ptcl</ta>
            <ta e="T642" id="Seg_9511" s="T641">v</ta>
            <ta e="T643" id="Seg_9512" s="T642">interj</ta>
            <ta e="T644" id="Seg_9513" s="T643">ptcl</ta>
            <ta e="T645" id="Seg_9514" s="T644">n</ta>
            <ta e="T646" id="Seg_9515" s="T645">v</ta>
            <ta e="T647" id="Seg_9516" s="T646">n</ta>
            <ta e="T648" id="Seg_9517" s="T647">v</ta>
            <ta e="T649" id="Seg_9518" s="T648">aux</ta>
            <ta e="T650" id="Seg_9519" s="T649">v</ta>
            <ta e="T651" id="Seg_9520" s="T650">n</ta>
            <ta e="T652" id="Seg_9521" s="T651">adv</ta>
            <ta e="T653" id="Seg_9522" s="T652">n</ta>
            <ta e="T654" id="Seg_9523" s="T653">v</ta>
            <ta e="T655" id="Seg_9524" s="T654">n</ta>
            <ta e="T656" id="Seg_9525" s="T655">v</ta>
            <ta e="T657" id="Seg_9526" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_9527" s="T657">que</ta>
            <ta e="T659" id="Seg_9528" s="T658">n</ta>
            <ta e="T660" id="Seg_9529" s="T659">v</ta>
            <ta e="T661" id="Seg_9530" s="T660">ptcl</ta>
            <ta e="T662" id="Seg_9531" s="T661">v</ta>
            <ta e="T663" id="Seg_9532" s="T662">n</ta>
            <ta e="T664" id="Seg_9533" s="T663">v</ta>
            <ta e="T665" id="Seg_9534" s="T664">n</ta>
            <ta e="T666" id="Seg_9535" s="T665">n</ta>
            <ta e="T667" id="Seg_9536" s="T666">ptcl</ta>
            <ta e="T668" id="Seg_9537" s="T667">v</ta>
            <ta e="T669" id="Seg_9538" s="T668">v</ta>
            <ta e="T670" id="Seg_9539" s="T669">v</ta>
            <ta e="T671" id="Seg_9540" s="T670">n</ta>
            <ta e="T672" id="Seg_9541" s="T671">n</ta>
            <ta e="T673" id="Seg_9542" s="T672">v</ta>
            <ta e="T674" id="Seg_9543" s="T673">ptcl</ta>
            <ta e="T675" id="Seg_9544" s="T674">interj</ta>
            <ta e="T676" id="Seg_9545" s="T675">dempro</ta>
            <ta e="T677" id="Seg_9546" s="T676">interj</ta>
            <ta e="T678" id="Seg_9547" s="T677">interj</ta>
            <ta e="T679" id="Seg_9548" s="T678">ptcl</ta>
            <ta e="T680" id="Seg_9549" s="T679">adv</ta>
            <ta e="T681" id="Seg_9550" s="T680">ptcl</ta>
            <ta e="T682" id="Seg_9551" s="T681">adj</ta>
            <ta e="T683" id="Seg_9552" s="T682">n</ta>
            <ta e="T684" id="Seg_9553" s="T683">v</ta>
            <ta e="T685" id="Seg_9554" s="T684">ptcl</ta>
            <ta e="T686" id="Seg_9555" s="T685">ptcl</ta>
            <ta e="T687" id="Seg_9556" s="T686">n</ta>
            <ta e="T688" id="Seg_9557" s="T687">v</ta>
            <ta e="T689" id="Seg_9558" s="T688">v</ta>
            <ta e="T690" id="Seg_9559" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_9560" s="T690">dempro</ta>
            <ta e="T692" id="Seg_9561" s="T691">n</ta>
            <ta e="T693" id="Seg_9562" s="T692">adj</ta>
            <ta e="T694" id="Seg_9563" s="T693">adj</ta>
            <ta e="T695" id="Seg_9564" s="T694">n</ta>
            <ta e="T696" id="Seg_9565" s="T695">n</ta>
            <ta e="T697" id="Seg_9566" s="T696">n</ta>
            <ta e="T698" id="Seg_9567" s="T697">v</ta>
            <ta e="T699" id="Seg_9568" s="T698">aux</ta>
            <ta e="T700" id="Seg_9569" s="T699">adv</ta>
            <ta e="T701" id="Seg_9570" s="T700">v</ta>
            <ta e="T702" id="Seg_9571" s="T701">n</ta>
            <ta e="T703" id="Seg_9572" s="T702">n</ta>
            <ta e="T704" id="Seg_9573" s="T703">n</ta>
            <ta e="T705" id="Seg_9574" s="T704">v</ta>
            <ta e="T706" id="Seg_9575" s="T705">%%</ta>
            <ta e="T707" id="Seg_9576" s="T706">v</ta>
            <ta e="T708" id="Seg_9577" s="T707">v</ta>
            <ta e="T709" id="Seg_9578" s="T708">dempro</ta>
            <ta e="T710" id="Seg_9579" s="T709">n</ta>
            <ta e="T711" id="Seg_9580" s="T710">adj</ta>
            <ta e="T712" id="Seg_9581" s="T711">adj</ta>
            <ta e="T713" id="Seg_9582" s="T712">dempro</ta>
            <ta e="T714" id="Seg_9583" s="T713">n</ta>
            <ta e="T715" id="Seg_9584" s="T714">ptcl</ta>
            <ta e="T716" id="Seg_9585" s="T715">v</ta>
            <ta e="T717" id="Seg_9586" s="T716">n</ta>
            <ta e="T718" id="Seg_9587" s="T717">n</ta>
            <ta e="T719" id="Seg_9588" s="T718">v</ta>
            <ta e="T720" id="Seg_9589" s="T719">post</ta>
            <ta e="T721" id="Seg_9590" s="T720">ptcl</ta>
            <ta e="T722" id="Seg_9591" s="T721">v</ta>
            <ta e="T723" id="Seg_9592" s="T722">v</ta>
            <ta e="T724" id="Seg_9593" s="T723">dempro</ta>
            <ta e="T725" id="Seg_9594" s="T724">v</ta>
            <ta e="T726" id="Seg_9595" s="T725">dempro</ta>
            <ta e="T727" id="Seg_9596" s="T726">ptcl</ta>
            <ta e="T728" id="Seg_9597" s="T727">dempro</ta>
            <ta e="T729" id="Seg_9598" s="T728">v</ta>
            <ta e="T730" id="Seg_9599" s="T729">v</ta>
            <ta e="T731" id="Seg_9600" s="T730">ptcl</ta>
            <ta e="T732" id="Seg_9601" s="T731">dempro</ta>
            <ta e="T733" id="Seg_9602" s="T732">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_9603" s="T1">np.h:Th</ta>
            <ta e="T9" id="Seg_9604" s="T8">0.3.h:Poss np:Th</ta>
            <ta e="T12" id="Seg_9605" s="T11">0.3.h:Poss np:Th</ta>
            <ta e="T13" id="Seg_9606" s="T12">n:Time</ta>
            <ta e="T16" id="Seg_9607" s="T15">0.3.h:A</ta>
            <ta e="T18" id="Seg_9608" s="T17">np.h:A</ta>
            <ta e="T19" id="Seg_9609" s="T18">np:Th</ta>
            <ta e="T22" id="Seg_9610" s="T21">0.3.h:Poss np:So</ta>
            <ta e="T26" id="Seg_9611" s="T25">0.3.h:A</ta>
            <ta e="T27" id="Seg_9612" s="T26">np:A</ta>
            <ta e="T28" id="Seg_9613" s="T27">pro:A</ta>
            <ta e="T31" id="Seg_9614" s="T30">0.3.h:Poss np:P</ta>
            <ta e="T32" id="Seg_9615" s="T31">pro:Th</ta>
            <ta e="T34" id="Seg_9616" s="T33">0.3:Poss np:Th</ta>
            <ta e="T36" id="Seg_9617" s="T35">np:Th</ta>
            <ta e="T37" id="Seg_9618" s="T36">0.3.h:Poss np:G</ta>
            <ta e="T41" id="Seg_9619" s="T40">np.h:A</ta>
            <ta e="T42" id="Seg_9620" s="T41">np:Poss</ta>
            <ta e="T43" id="Seg_9621" s="T42">np:Th</ta>
            <ta e="T45" id="Seg_9622" s="T44">np:Poss</ta>
            <ta e="T46" id="Seg_9623" s="T45">np:Th</ta>
            <ta e="T48" id="Seg_9624" s="T47">0.3.h:A</ta>
            <ta e="T50" id="Seg_9625" s="T49">np.h:A</ta>
            <ta e="T55" id="Seg_9626" s="T54">np:So</ta>
            <ta e="T60" id="Seg_9627" s="T59">np.h:E</ta>
            <ta e="T61" id="Seg_9628" s="T60">0.2.h:A</ta>
            <ta e="T64" id="Seg_9629" s="T63">0.2.h:A</ta>
            <ta e="T66" id="Seg_9630" s="T65">0.1.h:Poss np:Th</ta>
            <ta e="T70" id="Seg_9631" s="T69">pro.h:A</ta>
            <ta e="T72" id="Seg_9632" s="T71">pro.h:A</ta>
            <ta e="T76" id="Seg_9633" s="T74">0.3.h:A</ta>
            <ta e="T77" id="Seg_9634" s="T76">pro.h:A</ta>
            <ta e="T83" id="Seg_9635" s="T82">np.h:A</ta>
            <ta e="T86" id="Seg_9636" s="T85">pro.h:P</ta>
            <ta e="T88" id="Seg_9637" s="T86">0.3.h:A</ta>
            <ta e="T90" id="Seg_9638" s="T89">pro.h:Poss</ta>
            <ta e="T91" id="Seg_9639" s="T90">np:P</ta>
            <ta e="T101" id="Seg_9640" s="T100">0.3.h:A</ta>
            <ta e="T104" id="Seg_9641" s="T103">pro.h:P</ta>
            <ta e="T105" id="Seg_9642" s="T104">0.3.h:A</ta>
            <ta e="T107" id="Seg_9643" s="T106">0.3.h:Poss np:G</ta>
            <ta e="T108" id="Seg_9644" s="T107">0.3.h:A</ta>
            <ta e="T111" id="Seg_9645" s="T109">0.3.h:A</ta>
            <ta e="T114" id="Seg_9646" s="T112">pp:G</ta>
            <ta e="T117" id="Seg_9647" s="T116">np.h:A</ta>
            <ta e="T123" id="Seg_9648" s="T122">np:So</ta>
            <ta e="T124" id="Seg_9649" s="T123">0.2.h:A</ta>
            <ta e="T125" id="Seg_9650" s="T124">np.h:E</ta>
            <ta e="T126" id="Seg_9651" s="T125">0.2.h:A</ta>
            <ta e="T127" id="Seg_9652" s="T126">0.3.h:A</ta>
            <ta e="T129" id="Seg_9653" s="T128">0.1.h:Poss np:P</ta>
            <ta e="T132" id="Seg_9654" s="T131">0.2.h:A</ta>
            <ta e="T133" id="Seg_9655" s="T132">np:Th</ta>
            <ta e="T135" id="Seg_9656" s="T133">0.1.h:A</ta>
            <ta e="T144" id="Seg_9657" s="T143">0.3.h:A</ta>
            <ta e="T147" id="Seg_9658" s="T145">pp:Com</ta>
            <ta e="T148" id="Seg_9659" s="T147">0.3.h:A</ta>
            <ta e="T149" id="Seg_9660" s="T148">np.h:P</ta>
            <ta e="T153" id="Seg_9661" s="T152">np.h:A</ta>
            <ta e="T154" id="Seg_9662" s="T153">pro.h:P</ta>
            <ta e="T155" id="Seg_9663" s="T154">0.3.h:A</ta>
            <ta e="T159" id="Seg_9664" s="T158">0.3.h:Poss np:G</ta>
            <ta e="T161" id="Seg_9665" s="T159">0.3.h:A</ta>
            <ta e="T165" id="Seg_9666" s="T163">0.3.h:A</ta>
            <ta e="T167" id="Seg_9667" s="T166">0.3.h:A</ta>
            <ta e="T168" id="Seg_9668" s="T167">np.h:A</ta>
            <ta e="T173" id="Seg_9669" s="T172">np.h:E</ta>
            <ta e="T174" id="Seg_9670" s="T173">0.2.h:A</ta>
            <ta e="T177" id="Seg_9671" s="T176">np:So</ta>
            <ta e="T178" id="Seg_9672" s="T177">0.2.h:A</ta>
            <ta e="T180" id="Seg_9673" s="T179">0.3.h:A</ta>
            <ta e="T182" id="Seg_9674" s="T181">0.3.h:A</ta>
            <ta e="T184" id="Seg_9675" s="T183">0.1.h:Poss np:P</ta>
            <ta e="T186" id="Seg_9676" s="T185">0.2.h:A</ta>
            <ta e="T190" id="Seg_9677" s="T188">0.1.h:A</ta>
            <ta e="T192" id="Seg_9678" s="T191">pro.h:Poss</ta>
            <ta e="T193" id="Seg_9679" s="T192">np:G</ta>
            <ta e="T194" id="Seg_9680" s="T193">pro.h:A</ta>
            <ta e="T199" id="Seg_9681" s="T198">pro.h:A</ta>
            <ta e="T200" id="Seg_9682" s="T199">pro.h:P</ta>
            <ta e="T205" id="Seg_9683" s="T204">0.3.h:A</ta>
            <ta e="T209" id="Seg_9684" s="T208">np.h:A</ta>
            <ta e="T210" id="Seg_9685" s="T209">np.h:A</ta>
            <ta e="T211" id="Seg_9686" s="T210">pro.h:P</ta>
            <ta e="T214" id="Seg_9687" s="T212">0.3.h:A</ta>
            <ta e="T217" id="Seg_9688" s="T215">0.3.h:A </ta>
            <ta e="T218" id="Seg_9689" s="T217">pro.h:P</ta>
            <ta e="T219" id="Seg_9690" s="T218">0.3.h:A</ta>
            <ta e="T221" id="Seg_9691" s="T220">0.3.h:Poss np:G</ta>
            <ta e="T222" id="Seg_9692" s="T221">np:Th</ta>
            <ta e="T225" id="Seg_9693" s="T224">0.1.h:E</ta>
            <ta e="T226" id="Seg_9694" s="T225">np.h:Th</ta>
            <ta e="T228" id="Seg_9695" s="T226">0.3.h:A</ta>
            <ta e="T229" id="Seg_9696" s="T228">0.3.h:Poss np:G</ta>
            <ta e="T230" id="Seg_9697" s="T229">adv:Time</ta>
            <ta e="T232" id="Seg_9698" s="T230">0.3.h:A</ta>
            <ta e="T235" id="Seg_9699" s="T234">0.3.h:A</ta>
            <ta e="T236" id="Seg_9700" s="T235">np.h:A</ta>
            <ta e="T241" id="Seg_9701" s="T240">np.h:E</ta>
            <ta e="T242" id="Seg_9702" s="T241">0.2.h:A</ta>
            <ta e="T244" id="Seg_9703" s="T243">0.3.h:A</ta>
            <ta e="T246" id="Seg_9704" s="T245">0.2.h:A</ta>
            <ta e="T249" id="Seg_9705" s="T248">np:L</ta>
            <ta e="T252" id="Seg_9706" s="T251">0.1.h:Poss np:P</ta>
            <ta e="T255" id="Seg_9707" s="T254">0.2.h:A</ta>
            <ta e="T256" id="Seg_9708" s="T255">pro:Th</ta>
            <ta e="T258" id="Seg_9709" s="T256">0.1.h:A</ta>
            <ta e="T259" id="Seg_9710" s="T258">0.3.h:A</ta>
            <ta e="T261" id="Seg_9711" s="T260">0.2.h:A</ta>
            <ta e="T263" id="Seg_9712" s="T261">0.3.h:A</ta>
            <ta e="T265" id="Seg_9713" s="T264">pro.h:P</ta>
            <ta e="T267" id="Seg_9714" s="T266">0.3.h:A</ta>
            <ta e="T269" id="Seg_9715" s="T268">np.h:A</ta>
            <ta e="T270" id="Seg_9716" s="T269">pro.h:P</ta>
            <ta e="T272" id="Seg_9717" s="T271">0.3.h:A</ta>
            <ta e="T273" id="Seg_9718" s="T272">0.3.h:A</ta>
            <ta e="T276" id="Seg_9719" s="T275">0.3.h:A</ta>
            <ta e="T278" id="Seg_9720" s="T277">0.3.h:Poss np:G</ta>
            <ta e="T280" id="Seg_9721" s="T278">0.3.h:A</ta>
            <ta e="T283" id="Seg_9722" s="T281">0.3.h:A</ta>
            <ta e="T285" id="Seg_9723" s="T284">np.h:Th</ta>
            <ta e="T286" id="Seg_9724" s="T285">0.3.h:A</ta>
            <ta e="T287" id="Seg_9725" s="T286">np.h:A</ta>
            <ta e="T292" id="Seg_9726" s="T291">np.h:E</ta>
            <ta e="T293" id="Seg_9727" s="T292">0.2.h:A</ta>
            <ta e="T295" id="Seg_9728" s="T294">pro:G</ta>
            <ta e="T296" id="Seg_9729" s="T295">0.2.h:A</ta>
            <ta e="T297" id="Seg_9730" s="T296">0.3.h:A</ta>
            <ta e="T299" id="Seg_9731" s="T298">0.1.h:Poss np:P</ta>
            <ta e="T301" id="Seg_9732" s="T300">0.2.h:A</ta>
            <ta e="T302" id="Seg_9733" s="T301">pro:Th</ta>
            <ta e="T303" id="Seg_9734" s="T302">0.1.h:A</ta>
            <ta e="T305" id="Seg_9735" s="T304">pro.h:Poss</ta>
            <ta e="T306" id="Seg_9736" s="T305">np:G</ta>
            <ta e="T307" id="Seg_9737" s="T306">0.1.h:A</ta>
            <ta e="T310" id="Seg_9738" s="T309">pro.h:A</ta>
            <ta e="T311" id="Seg_9739" s="T310">pro.h:P</ta>
            <ta e="T314" id="Seg_9740" s="T313">0.3.h:A</ta>
            <ta e="T315" id="Seg_9741" s="T314">pro.h:P</ta>
            <ta e="T317" id="Seg_9742" s="T315">0.3.h:A</ta>
            <ta e="T319" id="Seg_9743" s="T317">0.3.h:A</ta>
            <ta e="T320" id="Seg_9744" s="T319">np.h:P</ta>
            <ta e="T321" id="Seg_9745" s="T320">np.h:A</ta>
            <ta e="T322" id="Seg_9746" s="T321">0.3.h:Poss np:P</ta>
            <ta e="T327" id="Seg_9747" s="T326">0.3.h:A</ta>
            <ta e="T329" id="Seg_9748" s="T328">0.3.h:A</ta>
            <ta e="T331" id="Seg_9749" s="T330">0.3.h:A</ta>
            <ta e="T333" id="Seg_9750" s="T332">0.3.h:Poss np:G</ta>
            <ta e="T335" id="Seg_9751" s="T333">0.3.h:A</ta>
            <ta e="T336" id="Seg_9752" s="T335">adv:Time</ta>
            <ta e="T339" id="Seg_9753" s="T337">0.3.h:A</ta>
            <ta e="T341" id="Seg_9754" s="T339">0.3.h:A</ta>
            <ta e="T343" id="Seg_9755" s="T342">np.h:A</ta>
            <ta e="T353" id="Seg_9756" s="T352">np:G</ta>
            <ta e="T354" id="Seg_9757" s="T353">0.2.h:A</ta>
            <ta e="T356" id="Seg_9758" s="T355">0.3.h:A</ta>
            <ta e="T357" id="Seg_9759" s="T356">0.1.h:Poss np:P</ta>
            <ta e="T359" id="Seg_9760" s="T358">0.2.h:A</ta>
            <ta e="T361" id="Seg_9761" s="T360">0.1.h:Poss np:Th</ta>
            <ta e="T362" id="Seg_9762" s="T361">0.2.h:A</ta>
            <ta e="T365" id="Seg_9763" s="T364">pro.h:A</ta>
            <ta e="T366" id="Seg_9764" s="T365">pro.h:P</ta>
            <ta e="T369" id="Seg_9765" s="T368">0.3.h:A</ta>
            <ta e="T371" id="Seg_9766" s="T369">0.3.h:A</ta>
            <ta e="T372" id="Seg_9767" s="T371">0.3.h:Poss np:P</ta>
            <ta e="T374" id="Seg_9768" s="T372">0.3.h:A</ta>
            <ta e="T379" id="Seg_9769" s="T378">np.h:A</ta>
            <ta e="T382" id="Seg_9770" s="T381">0.3.h:Poss np:Ins</ta>
            <ta e="T386" id="Seg_9771" s="T385">0.3.h:Th</ta>
            <ta e="T390" id="Seg_9772" s="T389">0.3.h:E</ta>
            <ta e="T392" id="Seg_9773" s="T391">np.h:Poss</ta>
            <ta e="T393" id="Seg_9774" s="T392">np.h:Th</ta>
            <ta e="T396" id="Seg_9775" s="T395">np.h:Poss</ta>
            <ta e="T398" id="Seg_9776" s="T397">np.h:Th</ta>
            <ta e="T399" id="Seg_9777" s="T398">0.3.h:A</ta>
            <ta e="T400" id="Seg_9778" s="T399">pro.h:Th</ta>
            <ta e="T408" id="Seg_9779" s="T407">0.3.h:E</ta>
            <ta e="T410" id="Seg_9780" s="T409">np:G</ta>
            <ta e="T415" id="Seg_9781" s="T414">np.h:Th</ta>
            <ta e="T416" id="Seg_9782" s="T415">0.3.h:Th</ta>
            <ta e="T423" id="Seg_9783" s="T422">np.h:A</ta>
            <ta e="T426" id="Seg_9784" s="T425">np:L</ta>
            <ta e="T427" id="Seg_9785" s="T426">0.3.h:Th</ta>
            <ta e="T429" id="Seg_9786" s="T428">0.3.h:A</ta>
            <ta e="T430" id="Seg_9787" s="T429">np.h:Poss</ta>
            <ta e="T432" id="Seg_9788" s="T431">np:Th</ta>
            <ta e="T435" id="Seg_9789" s="T434">0.1.h:Th</ta>
            <ta e="T436" id="Seg_9790" s="T435">adv:G</ta>
            <ta e="T438" id="Seg_9791" s="T437">np.h:A</ta>
            <ta e="T441" id="Seg_9792" s="T440">np:G</ta>
            <ta e="T443" id="Seg_9793" s="T442">np.h:Poss</ta>
            <ta e="T444" id="Seg_9794" s="T443">np:G</ta>
            <ta e="T445" id="Seg_9795" s="T444">0.3.h:A</ta>
            <ta e="T448" id="Seg_9796" s="T446">pp:L</ta>
            <ta e="T452" id="Seg_9797" s="T451">np:Th</ta>
            <ta e="T454" id="Seg_9798" s="T453">np:Th</ta>
            <ta e="T457" id="Seg_9799" s="T456">pro:G</ta>
            <ta e="T460" id="Seg_9800" s="T459">np:G</ta>
            <ta e="T461" id="Seg_9801" s="T460">np:G</ta>
            <ta e="T463" id="Seg_9802" s="T461">0.3.h:A</ta>
            <ta e="T466" id="Seg_9803" s="T465">np.h:Th</ta>
            <ta e="T469" id="Seg_9804" s="T468">np.h:Th</ta>
            <ta e="T474" id="Seg_9805" s="T473">0.3.h:A</ta>
            <ta e="T475" id="Seg_9806" s="T474">pro.h:B</ta>
            <ta e="T478" id="Seg_9807" s="T477">np.h:A</ta>
            <ta e="T480" id="Seg_9808" s="T479">np:So</ta>
            <ta e="T481" id="Seg_9809" s="T480">0.2.h:A</ta>
            <ta e="T483" id="Seg_9810" s="T482">np:Th</ta>
            <ta e="T484" id="Seg_9811" s="T483">0.3.h:A</ta>
            <ta e="T485" id="Seg_9812" s="T484">pro.h:Poss</ta>
            <ta e="T489" id="Seg_9813" s="T488">np:Th</ta>
            <ta e="T491" id="Seg_9814" s="T490">pro:So</ta>
            <ta e="T492" id="Seg_9815" s="T491">np:Th</ta>
            <ta e="T493" id="Seg_9816" s="T492">0.1.h:A</ta>
            <ta e="T494" id="Seg_9817" s="T493">0.1.h:E</ta>
            <ta e="T495" id="Seg_9818" s="T494">np.h:P</ta>
            <ta e="T497" id="Seg_9819" s="T496">0.1.h:A</ta>
            <ta e="T498" id="Seg_9820" s="T497">0.3.h:A</ta>
            <ta e="T502" id="Seg_9821" s="T501">np.h:P</ta>
            <ta e="T504" id="Seg_9822" s="T503">0.3.h:A</ta>
            <ta e="T506" id="Seg_9823" s="T505">np.h:R</ta>
            <ta e="T507" id="Seg_9824" s="T506">0.3.h:A</ta>
            <ta e="T508" id="Seg_9825" s="T507">np.h:Poss</ta>
            <ta e="T509" id="Seg_9826" s="T508">np.h:Th</ta>
            <ta e="T513" id="Seg_9827" s="T512">0.3.h:Poss np.h:Th</ta>
            <ta e="T514" id="Seg_9828" s="T513">pro:G</ta>
            <ta e="T518" id="Seg_9829" s="T517">0.3.h:A</ta>
            <ta e="T522" id="Seg_9830" s="T521">pro.h:A</ta>
            <ta e="T529" id="Seg_9831" s="T528">np.h:A</ta>
            <ta e="T532" id="Seg_9832" s="T531">0.1.h:A</ta>
            <ta e="T533" id="Seg_9833" s="T532">0.3.h:A</ta>
            <ta e="T535" id="Seg_9834" s="T534">np.h:A</ta>
            <ta e="T537" id="Seg_9835" s="T536">0.2.h:Poss np.h:Th</ta>
            <ta e="T538" id="Seg_9836" s="T537">0.3.h:A</ta>
            <ta e="T540" id="Seg_9837" s="T539">0.2.h:A</ta>
            <ta e="T541" id="Seg_9838" s="T540">0.3.h:A</ta>
            <ta e="T543" id="Seg_9839" s="T542">np.h:A</ta>
            <ta e="T546" id="Seg_9840" s="T545">np:G</ta>
            <ta e="T548" id="Seg_9841" s="T546">0.3.h:A</ta>
            <ta e="T551" id="Seg_9842" s="T550">0.2.h:A</ta>
            <ta e="T553" id="Seg_9843" s="T552">pro.h:Poss</ta>
            <ta e="T554" id="Seg_9844" s="T553">np.h:Th</ta>
            <ta e="T556" id="Seg_9845" s="T555">np:So</ta>
            <ta e="T557" id="Seg_9846" s="T556">0.3.h:A</ta>
            <ta e="T559" id="Seg_9847" s="T558">0.1.h:E</ta>
            <ta e="T561" id="Seg_9848" s="T560">np.h:A</ta>
            <ta e="T562" id="Seg_9849" s="T561">0.2.h:Poss np.h:Th</ta>
            <ta e="T565" id="Seg_9850" s="T564">0.3.h:A</ta>
            <ta e="T569" id="Seg_9851" s="T568">np.h:Poss</ta>
            <ta e="T571" id="Seg_9852" s="T570">np:Th</ta>
            <ta e="T572" id="Seg_9853" s="T571">0.1.h:E</ta>
            <ta e="T573" id="Seg_9854" s="T572">adv:G</ta>
            <ta e="T574" id="Seg_9855" s="T573">0.2.h:A</ta>
            <ta e="T576" id="Seg_9856" s="T575">np.h:A</ta>
            <ta e="T578" id="Seg_9857" s="T577">np.h:A</ta>
            <ta e="T580" id="Seg_9858" s="T579">np.h:Th</ta>
            <ta e="T583" id="Seg_9859" s="T582">pro.h:A</ta>
            <ta e="T585" id="Seg_9860" s="T584">np.h:R</ta>
            <ta e="T587" id="Seg_9861" s="T586">0.3.h:Poss np:P</ta>
            <ta e="T590" id="Seg_9862" s="T589">np:G</ta>
            <ta e="T591" id="Seg_9863" s="T590">0.2.h:A</ta>
            <ta e="T592" id="Seg_9864" s="T591">0.3.h:A</ta>
            <ta e="T595" id="Seg_9865" s="T594">np.h:Poss</ta>
            <ta e="T596" id="Seg_9866" s="T595">np:P</ta>
            <ta e="T597" id="Seg_9867" s="T596">0.3.h:A</ta>
            <ta e="T598" id="Seg_9868" s="T597">np:Ins</ta>
            <ta e="T600" id="Seg_9869" s="T599">0.3.h:A</ta>
            <ta e="T601" id="Seg_9870" s="T600">np:G</ta>
            <ta e="T603" id="Seg_9871" s="T601">0.3.h:A</ta>
            <ta e="T605" id="Seg_9872" s="T604">np.h:Th</ta>
            <ta e="T608" id="Seg_9873" s="T607">np.h:A</ta>
            <ta e="T615" id="Seg_9874" s="T614">np:G</ta>
            <ta e="T617" id="Seg_9875" s="T616">0.3.h:A</ta>
            <ta e="T619" id="Seg_9876" s="T618">0.1.h:E</ta>
            <ta e="T622" id="Seg_9877" s="T621">np.h:A</ta>
            <ta e="T628" id="Seg_9878" s="T627">0.2.h:Poss np.h:Th</ta>
            <ta e="T630" id="Seg_9879" s="T628">0.3.h:A</ta>
            <ta e="T631" id="Seg_9880" s="T630">0.2.h:A</ta>
            <ta e="T632" id="Seg_9881" s="T631">0.2.h:A</ta>
            <ta e="T634" id="Seg_9882" s="T633">0.3.h:A</ta>
            <ta e="T637" id="Seg_9883" s="T636">np.h:A</ta>
            <ta e="T642" id="Seg_9884" s="T641">0.3.h:A</ta>
            <ta e="T645" id="Seg_9885" s="T644">np.h:A</ta>
            <ta e="T647" id="Seg_9886" s="T646">0.2.h:Poss np.h:Th</ta>
            <ta e="T649" id="Seg_9887" s="T647">0.3.h:A</ta>
            <ta e="T650" id="Seg_9888" s="T649">0.3.h:A</ta>
            <ta e="T653" id="Seg_9889" s="T652">np:G</ta>
            <ta e="T655" id="Seg_9890" s="T654">np.h:Th</ta>
            <ta e="T656" id="Seg_9891" s="T655">0.2.h:A</ta>
            <ta e="T659" id="Seg_9892" s="T658">np.h:P</ta>
            <ta e="T660" id="Seg_9893" s="T659">0.1.h:A</ta>
            <ta e="T662" id="Seg_9894" s="T661">0.3.h:A</ta>
            <ta e="T663" id="Seg_9895" s="T662">np:G</ta>
            <ta e="T665" id="Seg_9896" s="T664">np.h:Poss</ta>
            <ta e="T666" id="Seg_9897" s="T665">np:Th</ta>
            <ta e="T671" id="Seg_9898" s="T670">np:Th</ta>
            <ta e="T683" id="Seg_9899" s="T682">np.h:Th</ta>
            <ta e="T684" id="Seg_9900" s="T683">0.1.h:A</ta>
            <ta e="T687" id="Seg_9901" s="T686">0.1.h:Poss np.h:Th</ta>
            <ta e="T688" id="Seg_9902" s="T687">0.1.h:A</ta>
            <ta e="T689" id="Seg_9903" s="T688">0.1.h:A</ta>
            <ta e="T692" id="Seg_9904" s="T691">np.h:A</ta>
            <ta e="T695" id="Seg_9905" s="T694">np.h:Th</ta>
            <ta e="T696" id="Seg_9906" s="T695">np.h:Poss</ta>
            <ta e="T697" id="Seg_9907" s="T696">np:G</ta>
            <ta e="T703" id="Seg_9908" s="T702">np:L</ta>
            <ta e="T704" id="Seg_9909" s="T703">np:A</ta>
            <ta e="T710" id="Seg_9910" s="T709">np.h:A</ta>
            <ta e="T712" id="Seg_9911" s="T711">np:Com</ta>
            <ta e="T714" id="Seg_9912" s="T713">np.h:A</ta>
            <ta e="T717" id="Seg_9913" s="T716">np:G</ta>
            <ta e="T718" id="Seg_9914" s="T717">np.h:Th</ta>
            <ta e="T722" id="Seg_9915" s="T721">0.3.h:A</ta>
            <ta e="T725" id="Seg_9916" s="T724">0.3.h:A</ta>
            <ta e="T730" id="Seg_9917" s="T729">0.3.h:Th</ta>
            <ta e="T732" id="Seg_9918" s="T731">pro:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_9919" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_9920" s="T2">v:pred</ta>
            <ta e="T7" id="Seg_9921" s="T5">s:rel</ta>
            <ta e="T9" id="Seg_9922" s="T8">0.3.h:S n:pred</ta>
            <ta e="T12" id="Seg_9923" s="T11">0.3.h:S n:pred</ta>
            <ta e="T16" id="Seg_9924" s="T15">0.3.h:S v:pred</ta>
            <ta e="T18" id="Seg_9925" s="T17">np.h:S</ta>
            <ta e="T19" id="Seg_9926" s="T18">np:O</ta>
            <ta e="T21" id="Seg_9927" s="T20">v:pred</ta>
            <ta e="T26" id="Seg_9928" s="T25">0.3.h:S v:pred</ta>
            <ta e="T27" id="Seg_9929" s="T26">np:S</ta>
            <ta e="T28" id="Seg_9930" s="T27">pro:S</ta>
            <ta e="T29" id="Seg_9931" s="T28">v:pred</ta>
            <ta e="T31" id="Seg_9932" s="T30">np:O</ta>
            <ta e="T39" id="Seg_9933" s="T35">s:temp</ta>
            <ta e="T41" id="Seg_9934" s="T40">np.h:S</ta>
            <ta e="T43" id="Seg_9935" s="T42">np:O</ta>
            <ta e="T44" id="Seg_9936" s="T43">v:pred</ta>
            <ta e="T48" id="Seg_9937" s="T44">s:temp</ta>
            <ta e="T50" id="Seg_9938" s="T49">np.h:S</ta>
            <ta e="T57" id="Seg_9939" s="T56">v:pred</ta>
            <ta e="T60" id="Seg_9940" s="T59">np.h:O</ta>
            <ta e="T61" id="Seg_9941" s="T60">0.2.h:S v:pred</ta>
            <ta e="T64" id="Seg_9942" s="T63">0.2.h:S v:pred</ta>
            <ta e="T66" id="Seg_9943" s="T65">np:O</ta>
            <ta e="T69" id="Seg_9944" s="T68">v:pred</ta>
            <ta e="T70" id="Seg_9945" s="T69">pro.h:S</ta>
            <ta e="T72" id="Seg_9946" s="T71">pro.h:S</ta>
            <ta e="T73" id="Seg_9947" s="T72">v:pred</ta>
            <ta e="T76" id="Seg_9948" s="T74">0.3.h:S v:pred</ta>
            <ta e="T77" id="Seg_9949" s="T76">pro.h:S</ta>
            <ta e="T78" id="Seg_9950" s="T77">n:pred</ta>
            <ta e="T79" id="Seg_9951" s="T78">cop</ta>
            <ta e="T82" id="Seg_9952" s="T79">s:temp</ta>
            <ta e="T83" id="Seg_9953" s="T82">np.h:S</ta>
            <ta e="T85" id="Seg_9954" s="T84">v:pred</ta>
            <ta e="T86" id="Seg_9955" s="T85">pro.h:O</ta>
            <ta e="T88" id="Seg_9956" s="T86">0.3.h:S v:pred</ta>
            <ta e="T96" id="Seg_9957" s="T89">s:temp</ta>
            <ta e="T101" id="Seg_9958" s="T100">0.3.h:S v:pred</ta>
            <ta e="T104" id="Seg_9959" s="T103">pro.h:O</ta>
            <ta e="T105" id="Seg_9960" s="T104">0.3.h:S v:pred</ta>
            <ta e="T108" id="Seg_9961" s="T107">0.3.h:S v:pred</ta>
            <ta e="T111" id="Seg_9962" s="T109">0.3.h:S v:pred</ta>
            <ta e="T116" id="Seg_9963" s="T111">s:temp</ta>
            <ta e="T117" id="Seg_9964" s="T116">np.h:S</ta>
            <ta e="T118" id="Seg_9965" s="T117">v:pred</ta>
            <ta e="T124" id="Seg_9966" s="T123">0.2.h:S v:pred</ta>
            <ta e="T125" id="Seg_9967" s="T124">np.h:O</ta>
            <ta e="T126" id="Seg_9968" s="T125">0.2.h:S v:pred</ta>
            <ta e="T127" id="Seg_9969" s="T126">0.3.h:S v:pred</ta>
            <ta e="T129" id="Seg_9970" s="T128">np:O</ta>
            <ta e="T132" id="Seg_9971" s="T131">0.2.h:S v:pred</ta>
            <ta e="T133" id="Seg_9972" s="T132">np:O</ta>
            <ta e="T135" id="Seg_9973" s="T133">0.1.h:S v:pred</ta>
            <ta e="T144" id="Seg_9974" s="T143">0.3.h:S v:pred</ta>
            <ta e="T148" id="Seg_9975" s="T147">s:temp</ta>
            <ta e="T149" id="Seg_9976" s="T148">np.h:O</ta>
            <ta e="T151" id="Seg_9977" s="T150">v:pred</ta>
            <ta e="T153" id="Seg_9978" s="T152">np.h:S</ta>
            <ta e="T154" id="Seg_9979" s="T153">pro.h:O</ta>
            <ta e="T155" id="Seg_9980" s="T154">0.3.h:S v:pred</ta>
            <ta e="T161" id="Seg_9981" s="T159">0.3.h:S v:pred</ta>
            <ta e="T162" id="Seg_9982" s="T161">s:temp</ta>
            <ta e="T165" id="Seg_9983" s="T163">0.3.h:S v:pred</ta>
            <ta e="T167" id="Seg_9984" s="T165">s:temp</ta>
            <ta e="T168" id="Seg_9985" s="T167">np.h:S</ta>
            <ta e="T170" id="Seg_9986" s="T169">v:pred</ta>
            <ta e="T173" id="Seg_9987" s="T172">np.h:O</ta>
            <ta e="T174" id="Seg_9988" s="T173">0.2.h:S v:pred</ta>
            <ta e="T178" id="Seg_9989" s="T177">0.2.h:S v:pred</ta>
            <ta e="T180" id="Seg_9990" s="T179">0.3.h:S v:pred</ta>
            <ta e="T182" id="Seg_9991" s="T181">0.3.h:S v:pred</ta>
            <ta e="T184" id="Seg_9992" s="T183">np:O</ta>
            <ta e="T186" id="Seg_9993" s="T185">0.2.h:S v:pred</ta>
            <ta e="T190" id="Seg_9994" s="T188">0.1.h:S v:pred</ta>
            <ta e="T194" id="Seg_9995" s="T193">pro.h:S</ta>
            <ta e="T195" id="Seg_9996" s="T194">v:pred</ta>
            <ta e="T199" id="Seg_9997" s="T198">pro.h:S</ta>
            <ta e="T200" id="Seg_9998" s="T199">pro.h:O</ta>
            <ta e="T202" id="Seg_9999" s="T200">s:temp</ta>
            <ta e="T204" id="Seg_10000" s="T202">v:pred</ta>
            <ta e="T205" id="Seg_10001" s="T204">0.3.h:S v:pred</ta>
            <ta e="T207" id="Seg_10002" s="T206">v:pred</ta>
            <ta e="T209" id="Seg_10003" s="T208">np.h:S</ta>
            <ta e="T210" id="Seg_10004" s="T209">np.h:S</ta>
            <ta e="T211" id="Seg_10005" s="T210">pro.h:O</ta>
            <ta e="T212" id="Seg_10006" s="T211">v:pred</ta>
            <ta e="T214" id="Seg_10007" s="T212">0.3.h:S v:pred</ta>
            <ta e="T217" id="Seg_10008" s="T215">0.3.h:S v:pred</ta>
            <ta e="T218" id="Seg_10009" s="T217">pro.h:O</ta>
            <ta e="T219" id="Seg_10010" s="T218">0.3.h:S v:pred</ta>
            <ta e="T222" id="Seg_10011" s="T221">np:S</ta>
            <ta e="T224" id="Seg_10012" s="T223">adj:pred</ta>
            <ta e="T225" id="Seg_10013" s="T224">0.1.h:S v:pred</ta>
            <ta e="T226" id="Seg_10014" s="T225">np.h:O</ta>
            <ta e="T228" id="Seg_10015" s="T226">0.3.h:S v:pred</ta>
            <ta e="T232" id="Seg_10016" s="T230">0.3.h:S v:pred</ta>
            <ta e="T235" id="Seg_10017" s="T232">s:temp</ta>
            <ta e="T236" id="Seg_10018" s="T235">np.h:S</ta>
            <ta e="T238" id="Seg_10019" s="T237">v:pred</ta>
            <ta e="T241" id="Seg_10020" s="T240">np.h:O</ta>
            <ta e="T242" id="Seg_10021" s="T241">0.2.h:S v:pred</ta>
            <ta e="T244" id="Seg_10022" s="T243">0.3.h:S v:pred</ta>
            <ta e="T246" id="Seg_10023" s="T245">0.2.h:S v:pred</ta>
            <ta e="T252" id="Seg_10024" s="T251">np:O</ta>
            <ta e="T255" id="Seg_10025" s="T254">0.2.h:S v:pred</ta>
            <ta e="T256" id="Seg_10026" s="T255">pro:O</ta>
            <ta e="T258" id="Seg_10027" s="T256">0.1.h:S v:pred</ta>
            <ta e="T259" id="Seg_10028" s="T258">0.3.h:S v:pred</ta>
            <ta e="T261" id="Seg_10029" s="T260">0.2.h:S v:pred</ta>
            <ta e="T263" id="Seg_10030" s="T261">0.3.h:S v:pred</ta>
            <ta e="T265" id="Seg_10031" s="T264">pro.h:O</ta>
            <ta e="T267" id="Seg_10032" s="T266">0.3.h:S v:pred</ta>
            <ta e="T268" id="Seg_10033" s="T267">s:adv</ta>
            <ta e="T269" id="Seg_10034" s="T268">np.h:S</ta>
            <ta e="T270" id="Seg_10035" s="T269">pro.h:O</ta>
            <ta e="T271" id="Seg_10036" s="T270">v:pred</ta>
            <ta e="T272" id="Seg_10037" s="T271">0.3.h:S v:pred</ta>
            <ta e="T273" id="Seg_10038" s="T272">0.3.h:S v:pred</ta>
            <ta e="T276" id="Seg_10039" s="T275">0.3.h:S v:pred</ta>
            <ta e="T280" id="Seg_10040" s="T278">0.3.h:S v:pred</ta>
            <ta e="T283" id="Seg_10041" s="T281">0.3.h:S v:pred</ta>
            <ta e="T284" id="Seg_10042" s="T283">s:temp</ta>
            <ta e="T285" id="Seg_10043" s="T284">np.h:O</ta>
            <ta e="T286" id="Seg_10044" s="T285">0.3.h:S v:pred</ta>
            <ta e="T287" id="Seg_10045" s="T286">np.h:S</ta>
            <ta e="T289" id="Seg_10046" s="T288">v:pred</ta>
            <ta e="T292" id="Seg_10047" s="T291">np.h:O</ta>
            <ta e="T293" id="Seg_10048" s="T292">0.2.h:S v:pred</ta>
            <ta e="T296" id="Seg_10049" s="T295">0.2.h:S v:pred</ta>
            <ta e="T297" id="Seg_10050" s="T296">0.3.h:S v:pred</ta>
            <ta e="T299" id="Seg_10051" s="T298">np:O</ta>
            <ta e="T301" id="Seg_10052" s="T300">0.2.h:S v:pred</ta>
            <ta e="T302" id="Seg_10053" s="T301">pro:O</ta>
            <ta e="T303" id="Seg_10054" s="T302">0.1.h:S v:pred</ta>
            <ta e="T307" id="Seg_10055" s="T306">0.1.h:S v:pred</ta>
            <ta e="T310" id="Seg_10056" s="T309">pro.h:S</ta>
            <ta e="T311" id="Seg_10057" s="T310">pro.h:O</ta>
            <ta e="T313" id="Seg_10058" s="T311">v:pred</ta>
            <ta e="T314" id="Seg_10059" s="T313">0.3.h:S v:pred</ta>
            <ta e="T315" id="Seg_10060" s="T314">pro.h:O</ta>
            <ta e="T317" id="Seg_10061" s="T315">0.3.h:S v:pred</ta>
            <ta e="T319" id="Seg_10062" s="T317">0.3.h:S v:pred</ta>
            <ta e="T321" id="Seg_10063" s="T320">np.h:S</ta>
            <ta e="T322" id="Seg_10064" s="T321">np:O</ta>
            <ta e="T324" id="Seg_10065" s="T322">v:pred</ta>
            <ta e="T327" id="Seg_10066" s="T326">0.3.h:S v:pred</ta>
            <ta e="T329" id="Seg_10067" s="T328">0.3.h:S v:pred</ta>
            <ta e="T331" id="Seg_10068" s="T330">0.3.h:S v:pred</ta>
            <ta e="T335" id="Seg_10069" s="T333">0.3.h:S v:pred</ta>
            <ta e="T339" id="Seg_10070" s="T337">0.3.h:S v:pred</ta>
            <ta e="T341" id="Seg_10071" s="T339">0.3.h:S v:pred</ta>
            <ta e="T343" id="Seg_10072" s="T342">np.h:S</ta>
            <ta e="T345" id="Seg_10073" s="T344">v:pred</ta>
            <ta e="T354" id="Seg_10074" s="T353">0.2.h:S v:pred</ta>
            <ta e="T356" id="Seg_10075" s="T355">0.3.h:S v:pred</ta>
            <ta e="T357" id="Seg_10076" s="T356">np:O</ta>
            <ta e="T359" id="Seg_10077" s="T358">0.2.h:S v:pred</ta>
            <ta e="T361" id="Seg_10078" s="T360">np:O</ta>
            <ta e="T362" id="Seg_10079" s="T361">0.2.h:S v:pred</ta>
            <ta e="T365" id="Seg_10080" s="T364">pro.h:S</ta>
            <ta e="T366" id="Seg_10081" s="T365">pro.h:O</ta>
            <ta e="T368" id="Seg_10082" s="T366">v:pred</ta>
            <ta e="T369" id="Seg_10083" s="T368">0.3.h:S v:pred</ta>
            <ta e="T371" id="Seg_10084" s="T369">0.3.h:S v:pred</ta>
            <ta e="T372" id="Seg_10085" s="T371">np:O</ta>
            <ta e="T374" id="Seg_10086" s="T372">0.3.h:S v:pred</ta>
            <ta e="T377" id="Seg_10087" s="T376">v:pred</ta>
            <ta e="T379" id="Seg_10088" s="T378">np.h:S</ta>
            <ta e="T384" id="Seg_10089" s="T381">s:temp</ta>
            <ta e="T386" id="Seg_10090" s="T385">0.3.h:S v:pred</ta>
            <ta e="T389" id="Seg_10091" s="T388">s:adv</ta>
            <ta e="T390" id="Seg_10092" s="T389">0.3.h:S v:pred</ta>
            <ta e="T394" id="Seg_10093" s="T390">s:comp</ta>
            <ta e="T395" id="Seg_10094" s="T394">v:pred</ta>
            <ta e="T396" id="Seg_10095" s="T395">np.h:S</ta>
            <ta e="T398" id="Seg_10096" s="T397">adj:pred</ta>
            <ta e="T399" id="Seg_10097" s="T398">0.3.h:S v:pred</ta>
            <ta e="T401" id="Seg_10098" s="T399">s:comp</ta>
            <ta e="T403" id="Seg_10099" s="T402">ptcl:pred</ta>
            <ta e="T408" id="Seg_10100" s="T407">0.3.h:S v:pred</ta>
            <ta e="T411" id="Seg_10101" s="T409">s:purp</ta>
            <ta e="T415" id="Seg_10102" s="T414">np.h:S v:pred</ta>
            <ta e="T416" id="Seg_10103" s="T415">0.3.h:S v:pred</ta>
            <ta e="T419" id="Seg_10104" s="T418">adj:pred</ta>
            <ta e="T421" id="Seg_10105" s="T420">v:pred</ta>
            <ta e="T423" id="Seg_10106" s="T422">np.h:S</ta>
            <ta e="T424" id="Seg_10107" s="T423">s:adv</ta>
            <ta e="T427" id="Seg_10108" s="T426">0.3.h:S v:pred</ta>
            <ta e="T428" id="Seg_10109" s="T427">s:adv</ta>
            <ta e="T429" id="Seg_10110" s="T428">0.3.h:S v:pred</ta>
            <ta e="T432" id="Seg_10111" s="T431">np:S</ta>
            <ta e="T433" id="Seg_10112" s="T432">v:pred</ta>
            <ta e="T435" id="Seg_10113" s="T434">0.1.h:S v:pred</ta>
            <ta e="T438" id="Seg_10114" s="T437">np.h:S</ta>
            <ta e="T439" id="Seg_10115" s="T438">s:temp</ta>
            <ta e="T440" id="Seg_10116" s="T439">s:temp</ta>
            <ta e="T442" id="Seg_10117" s="T441">v:pred</ta>
            <ta e="T445" id="Seg_10118" s="T444">0.3.h:S v:pred</ta>
            <ta e="T452" id="Seg_10119" s="T451">np:S</ta>
            <ta e="T453" id="Seg_10120" s="T452">v:pred</ta>
            <ta e="T454" id="Seg_10121" s="T453">np:S</ta>
            <ta e="T455" id="Seg_10122" s="T454">v:pred</ta>
            <ta e="T459" id="Seg_10123" s="T458">adj:pred</ta>
            <ta e="T463" id="Seg_10124" s="T461">0.3.h:S v:pred</ta>
            <ta e="T466" id="Seg_10125" s="T465">np.h:S</ta>
            <ta e="T468" id="Seg_10126" s="T467">ptcl:pred</ta>
            <ta e="T469" id="Seg_10127" s="T468">np.h:S</ta>
            <ta e="T471" id="Seg_10128" s="T470">adj:pred</ta>
            <ta e="T474" id="Seg_10129" s="T473">0.3.h:S v:pred</ta>
            <ta e="T475" id="Seg_10130" s="T474">pro.h:O</ta>
            <ta e="T476" id="Seg_10131" s="T475">v:pred</ta>
            <ta e="T478" id="Seg_10132" s="T477">np.h:S</ta>
            <ta e="T481" id="Seg_10133" s="T480">0.2.h:S v:pred</ta>
            <ta e="T482" id="Seg_10134" s="T481">s:adv</ta>
            <ta e="T483" id="Seg_10135" s="T482">np:O</ta>
            <ta e="T484" id="Seg_10136" s="T483">0.3.h:S v:pred</ta>
            <ta e="T485" id="Seg_10137" s="T484">pro.h:S</ta>
            <ta e="T489" id="Seg_10138" s="T488">n:pred</ta>
            <ta e="T490" id="Seg_10139" s="T489">cop</ta>
            <ta e="T493" id="Seg_10140" s="T490">s:temp</ta>
            <ta e="T494" id="Seg_10141" s="T493">s:temp</ta>
            <ta e="T496" id="Seg_10142" s="T494">s:temp</ta>
            <ta e="T497" id="Seg_10143" s="T496">0.1.h:S v:pred</ta>
            <ta e="T498" id="Seg_10144" s="T497">0.3.h:S v:pred</ta>
            <ta e="T503" id="Seg_10145" s="T501">s:temp</ta>
            <ta e="T504" id="Seg_10146" s="T503">0.3.h:S v:pred</ta>
            <ta e="T507" id="Seg_10147" s="T506">0.3.h:S v:pred</ta>
            <ta e="T508" id="Seg_10148" s="T507">np.h:S</ta>
            <ta e="T509" id="Seg_10149" s="T508">adj:pred</ta>
            <ta e="T513" id="Seg_10150" s="T512">0.3.h:S n:pred</ta>
            <ta e="T518" id="Seg_10151" s="T517">0.3.h:S v:pred</ta>
            <ta e="T522" id="Seg_10152" s="T521">pro.h:S</ta>
            <ta e="T525" id="Seg_10153" s="T523">v:pred</ta>
            <ta e="T527" id="Seg_10154" s="T526">v:pred</ta>
            <ta e="T529" id="Seg_10155" s="T528">np.h:S</ta>
            <ta e="T532" id="Seg_10156" s="T531">0.1.h:S v:pred</ta>
            <ta e="T533" id="Seg_10157" s="T532">0.3.h:S v:pred</ta>
            <ta e="T535" id="Seg_10158" s="T534">np.h:S</ta>
            <ta e="T536" id="Seg_10159" s="T535">v:pred</ta>
            <ta e="T537" id="Seg_10160" s="T536">np.h:O</ta>
            <ta e="T538" id="Seg_10161" s="T537">0.3.h:S v:pred</ta>
            <ta e="T540" id="Seg_10162" s="T539">0.2.h:S v:pred</ta>
            <ta e="T541" id="Seg_10163" s="T540">0.3.h:S v:pred</ta>
            <ta e="T543" id="Seg_10164" s="T542">np.h:S</ta>
            <ta e="T545" id="Seg_10165" s="T543">v:pred</ta>
            <ta e="T548" id="Seg_10166" s="T546">0.3.h:S v:pred</ta>
            <ta e="T551" id="Seg_10167" s="T550">0.2.h:S v:pred</ta>
            <ta e="T553" id="Seg_10168" s="T552">pro.h:S</ta>
            <ta e="T554" id="Seg_10169" s="T553">adj:pred</ta>
            <ta e="T557" id="Seg_10170" s="T556">0.3.h:S v:pred</ta>
            <ta e="T559" id="Seg_10171" s="T558">0.1.h:S v:pred</ta>
            <ta e="T561" id="Seg_10172" s="T560">np.h:S</ta>
            <ta e="T562" id="Seg_10173" s="T561">np.h:O</ta>
            <ta e="T564" id="Seg_10174" s="T562">v:pred</ta>
            <ta e="T565" id="Seg_10175" s="T564">0.3.h:S v:pred</ta>
            <ta e="T572" id="Seg_10176" s="T567">s:purp</ta>
            <ta e="T574" id="Seg_10177" s="T573">0.2.h:S v:pred</ta>
            <ta e="T575" id="Seg_10178" s="T574">v:pred</ta>
            <ta e="T576" id="Seg_10179" s="T575">np.h:S</ta>
            <ta e="T578" id="Seg_10180" s="T577">np.h:S</ta>
            <ta e="T580" id="Seg_10181" s="T579">np.h:O</ta>
            <ta e="T581" id="Seg_10182" s="T580">v:pred</ta>
            <ta e="T583" id="Seg_10183" s="T582">pro.h:S</ta>
            <ta e="T586" id="Seg_10184" s="T585">v:pred</ta>
            <ta e="T589" id="Seg_10185" s="T586">s:temp</ta>
            <ta e="T591" id="Seg_10186" s="T590">0.2.h:S v:pred</ta>
            <ta e="T592" id="Seg_10187" s="T591">0.3.h:S v:pred</ta>
            <ta e="T596" id="Seg_10188" s="T595">np:O</ta>
            <ta e="T597" id="Seg_10189" s="T596">0.3.h:S v:pred</ta>
            <ta e="T600" id="Seg_10190" s="T597">s:temp</ta>
            <ta e="T603" id="Seg_10191" s="T601">0.3.h:S v:pred</ta>
            <ta e="T608" id="Seg_10192" s="T607">np.h:S</ta>
            <ta e="T610" id="Seg_10193" s="T608">s:temp</ta>
            <ta e="T612" id="Seg_10194" s="T611">v:pred</ta>
            <ta e="T614" id="Seg_10195" s="T612">s:temp</ta>
            <ta e="T617" id="Seg_10196" s="T616">0.3.h:S v:pred</ta>
            <ta e="T619" id="Seg_10197" s="T618">0.1.h:S v:pred</ta>
            <ta e="T622" id="Seg_10198" s="T621">np.h:S</ta>
            <ta e="T623" id="Seg_10199" s="T622">v:pred</ta>
            <ta e="T628" id="Seg_10200" s="T627">np.h:O</ta>
            <ta e="T630" id="Seg_10201" s="T628">0.3.h:S v:pred</ta>
            <ta e="T631" id="Seg_10202" s="T630">s:adv</ta>
            <ta e="T632" id="Seg_10203" s="T631">0.2.h:S v:pred</ta>
            <ta e="T634" id="Seg_10204" s="T633">0.3.h:S v:pred</ta>
            <ta e="T636" id="Seg_10205" s="T635">v:pred</ta>
            <ta e="T637" id="Seg_10206" s="T636">np.h:S</ta>
            <ta e="T640" id="Seg_10207" s="T638">s:temp</ta>
            <ta e="T642" id="Seg_10208" s="T641">0.3.h:S v:pred</ta>
            <ta e="T645" id="Seg_10209" s="T644">np.h:S</ta>
            <ta e="T646" id="Seg_10210" s="T645">v:pred</ta>
            <ta e="T647" id="Seg_10211" s="T646">np.h:O</ta>
            <ta e="T649" id="Seg_10212" s="T647">0.3.h:S v:pred</ta>
            <ta e="T650" id="Seg_10213" s="T649">0.3.h:S v:pred</ta>
            <ta e="T654" id="Seg_10214" s="T651">s:rel</ta>
            <ta e="T655" id="Seg_10215" s="T654">np.h:O</ta>
            <ta e="T656" id="Seg_10216" s="T655">0.2.h:S v:pred</ta>
            <ta e="T659" id="Seg_10217" s="T658">np.h:O</ta>
            <ta e="T660" id="Seg_10218" s="T659">0.1.h:S v:pred</ta>
            <ta e="T662" id="Seg_10219" s="T661">0.3.h:S v:pred</ta>
            <ta e="T664" id="Seg_10220" s="T662">s:rel</ta>
            <ta e="T666" id="Seg_10221" s="T665">np:S</ta>
            <ta e="T668" id="Seg_10222" s="T667">s:adv</ta>
            <ta e="T669" id="Seg_10223" s="T668">v:pred</ta>
            <ta e="T670" id="Seg_10224" s="T669">s:rel</ta>
            <ta e="T671" id="Seg_10225" s="T670">np:S</ta>
            <ta e="T673" id="Seg_10226" s="T672">v:pred</ta>
            <ta e="T683" id="Seg_10227" s="T682">np.h:O</ta>
            <ta e="T684" id="Seg_10228" s="T683">0.1.h:S v:pred</ta>
            <ta e="T687" id="Seg_10229" s="T686">np.h:O</ta>
            <ta e="T688" id="Seg_10230" s="T687">0.1.h:S v:pred</ta>
            <ta e="T689" id="Seg_10231" s="T688">0.1.h:S v:pred</ta>
            <ta e="T692" id="Seg_10232" s="T691">np.h:S</ta>
            <ta e="T695" id="Seg_10233" s="T694">np.h:O</ta>
            <ta e="T699" id="Seg_10234" s="T697">v:pred</ta>
            <ta e="T704" id="Seg_10235" s="T703">np:S</ta>
            <ta e="T705" id="Seg_10236" s="T704">v:pred</ta>
            <ta e="T708" id="Seg_10237" s="T707">v:pred</ta>
            <ta e="T710" id="Seg_10238" s="T709">np.h:S</ta>
            <ta e="T714" id="Seg_10239" s="T713">np.h:S</ta>
            <ta e="T716" id="Seg_10240" s="T715">v:pred</ta>
            <ta e="T720" id="Seg_10241" s="T717">s:temp</ta>
            <ta e="T722" id="Seg_10242" s="T721">0.3.h:S v:pred</ta>
            <ta e="T723" id="Seg_10243" s="T722">s:adv</ta>
            <ta e="T725" id="Seg_10244" s="T723">s:temp</ta>
            <ta e="T729" id="Seg_10245" s="T728">s:adv</ta>
            <ta e="T730" id="Seg_10246" s="T729">0.3.h:S v:pred</ta>
            <ta e="T732" id="Seg_10247" s="T731">pro:S</ta>
            <ta e="T733" id="Seg_10248" s="T732">n:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T2" id="Seg_10249" s="T1">new</ta>
            <ta e="T9" id="Seg_10250" s="T8">0.giv-active new</ta>
            <ta e="T12" id="Seg_10251" s="T11">0.giv-active new</ta>
            <ta e="T16" id="Seg_10252" s="T15">0.giv-active</ta>
            <ta e="T18" id="Seg_10253" s="T17">giv-active</ta>
            <ta e="T19" id="Seg_10254" s="T18">accs-inf</ta>
            <ta e="T22" id="Seg_10255" s="T21">giv-inactive</ta>
            <ta e="T26" id="Seg_10256" s="T25">0.giv-active</ta>
            <ta e="T27" id="Seg_10257" s="T26">new</ta>
            <ta e="T31" id="Seg_10258" s="T30">giv-active</ta>
            <ta e="T32" id="Seg_10259" s="T31">giv-active</ta>
            <ta e="T34" id="Seg_10260" s="T33">accs-inf</ta>
            <ta e="T36" id="Seg_10261" s="T35">giv-inactive</ta>
            <ta e="T37" id="Seg_10262" s="T36">accs-inf</ta>
            <ta e="T41" id="Seg_10263" s="T40">giv-inactive</ta>
            <ta e="T42" id="Seg_10264" s="T41">giv-active</ta>
            <ta e="T43" id="Seg_10265" s="T42">giv-active</ta>
            <ta e="T45" id="Seg_10266" s="T44">giv-active</ta>
            <ta e="T46" id="Seg_10267" s="T45">giv-active</ta>
            <ta e="T48" id="Seg_10268" s="T47">0.giv-active</ta>
            <ta e="T50" id="Seg_10269" s="T49">giv-active</ta>
            <ta e="T54" id="Seg_10270" s="T53">new</ta>
            <ta e="T55" id="Seg_10271" s="T54">accs-inf</ta>
            <ta e="T59" id="Seg_10272" s="T58">giv-active-Q</ta>
            <ta e="T61" id="Seg_10273" s="T60">0.giv-active-Q</ta>
            <ta e="T64" id="Seg_10274" s="T63">0.giv-active-Q</ta>
            <ta e="T66" id="Seg_10275" s="T65">giv-inactive-Q</ta>
            <ta e="T70" id="Seg_10276" s="T69">giv-inactive-Q</ta>
            <ta e="T72" id="Seg_10277" s="T71">giv-active-Q</ta>
            <ta e="T76" id="Seg_10278" s="T74">0.giv-active 0.quot-sp</ta>
            <ta e="T77" id="Seg_10279" s="T76">giv-inactive</ta>
            <ta e="T83" id="Seg_10280" s="T82">giv-active</ta>
            <ta e="T86" id="Seg_10281" s="T85">giv-active</ta>
            <ta e="T88" id="Seg_10282" s="T86">0.giv-inactive</ta>
            <ta e="T89" id="Seg_10283" s="T88">giv-active</ta>
            <ta e="T90" id="Seg_10284" s="T89">giv-active</ta>
            <ta e="T91" id="Seg_10285" s="T90">accs-inf</ta>
            <ta e="T101" id="Seg_10286" s="T100">0.giv-active</ta>
            <ta e="T104" id="Seg_10287" s="T103">giv-active</ta>
            <ta e="T105" id="Seg_10288" s="T104">0.giv-active</ta>
            <ta e="T107" id="Seg_10289" s="T106">new</ta>
            <ta e="T108" id="Seg_10290" s="T107">0.giv-active</ta>
            <ta e="T111" id="Seg_10291" s="T109">0.giv-active</ta>
            <ta e="T117" id="Seg_10292" s="T116">new</ta>
            <ta e="T120" id="Seg_10293" s="T119">giv-inactive-Q</ta>
            <ta e="T124" id="Seg_10294" s="T123">0.giv-active-Q</ta>
            <ta e="T126" id="Seg_10295" s="T125">0.giv-active-Q</ta>
            <ta e="T127" id="Seg_10296" s="T126">0.giv-active 0.quot-sp</ta>
            <ta e="T129" id="Seg_10297" s="T128">giv-inactive-Q</ta>
            <ta e="T132" id="Seg_10298" s="T131">0.accs-inf-Q</ta>
            <ta e="T133" id="Seg_10299" s="T132">giv-active-Q</ta>
            <ta e="T135" id="Seg_10300" s="T133">0.giv-active-Q</ta>
            <ta e="T144" id="Seg_10301" s="T143">0.accs-aggr</ta>
            <ta e="T146" id="Seg_10302" s="T145">giv-active</ta>
            <ta e="T148" id="Seg_10303" s="T147">0.giv-active</ta>
            <ta e="T149" id="Seg_10304" s="T148">giv-active</ta>
            <ta e="T153" id="Seg_10305" s="T152">giv-active</ta>
            <ta e="T154" id="Seg_10306" s="T153">giv-active</ta>
            <ta e="T155" id="Seg_10307" s="T154">0.giv-active</ta>
            <ta e="T159" id="Seg_10308" s="T158">giv-inactive</ta>
            <ta e="T161" id="Seg_10309" s="T159">0.giv-active</ta>
            <ta e="T165" id="Seg_10310" s="T163">0.giv-active</ta>
            <ta e="T167" id="Seg_10311" s="T166">0.giv-active</ta>
            <ta e="T168" id="Seg_10312" s="T167">new</ta>
            <ta e="T172" id="Seg_10313" s="T171">giv-active-Q</ta>
            <ta e="T174" id="Seg_10314" s="T173">0.giv-active-Q</ta>
            <ta e="T178" id="Seg_10315" s="T177">0.giv-active-Q</ta>
            <ta e="T180" id="Seg_10316" s="T179">0.giv-active 0.quot-sp</ta>
            <ta e="T182" id="Seg_10317" s="T181">0.giv-active 0.quot-sp</ta>
            <ta e="T184" id="Seg_10318" s="T183">giv-inactive-Q</ta>
            <ta e="T186" id="Seg_10319" s="T185">0.accs-inf-Q</ta>
            <ta e="T190" id="Seg_10320" s="T188">0.giv-active-Q</ta>
            <ta e="T192" id="Seg_10321" s="T191">giv-active-Q</ta>
            <ta e="T193" id="Seg_10322" s="T192">giv-inactive-Q</ta>
            <ta e="T194" id="Seg_10323" s="T193">giv-inactive-Q</ta>
            <ta e="T199" id="Seg_10324" s="T198">giv-active-Q</ta>
            <ta e="T200" id="Seg_10325" s="T199">giv-active-Q</ta>
            <ta e="T205" id="Seg_10326" s="T204">0.giv-active 0.quot-sp</ta>
            <ta e="T209" id="Seg_10327" s="T208">accs-aggr</ta>
            <ta e="T210" id="Seg_10328" s="T209">giv-active</ta>
            <ta e="T211" id="Seg_10329" s="T210">giv-active</ta>
            <ta e="T214" id="Seg_10330" s="T212">0.giv-active</ta>
            <ta e="T217" id="Seg_10331" s="T215">0.giv-active</ta>
            <ta e="T218" id="Seg_10332" s="T217">giv-inactive</ta>
            <ta e="T219" id="Seg_10333" s="T218">0.giv-active</ta>
            <ta e="T221" id="Seg_10334" s="T220">giv-inactive</ta>
            <ta e="T222" id="Seg_10335" s="T221">giv-active</ta>
            <ta e="T225" id="Seg_10336" s="T224">0.accs-sit</ta>
            <ta e="T226" id="Seg_10337" s="T225">giv-active</ta>
            <ta e="T228" id="Seg_10338" s="T226">0.giv-active</ta>
            <ta e="T229" id="Seg_10339" s="T228">giv-active</ta>
            <ta e="T232" id="Seg_10340" s="T230">0.giv-active</ta>
            <ta e="T235" id="Seg_10341" s="T234">0.giv-active</ta>
            <ta e="T236" id="Seg_10342" s="T235">new</ta>
            <ta e="T240" id="Seg_10343" s="T239">giv-active-Q</ta>
            <ta e="T242" id="Seg_10344" s="T241">0.giv-active-Q</ta>
            <ta e="T244" id="Seg_10345" s="T243">0.giv-active 0.quot-sp</ta>
            <ta e="T246" id="Seg_10346" s="T245">0.giv-active-Q</ta>
            <ta e="T249" id="Seg_10347" s="T248">accs-sit-Q</ta>
            <ta e="T252" id="Seg_10348" s="T251">giv-inactive-Q</ta>
            <ta e="T255" id="Seg_10349" s="T254">0.accs-inf-Q</ta>
            <ta e="T256" id="Seg_10350" s="T255">giv-active-Q</ta>
            <ta e="T258" id="Seg_10351" s="T256">0.giv-active-Q</ta>
            <ta e="T259" id="Seg_10352" s="T258">0.giv-active 0.quot-sp</ta>
            <ta e="T261" id="Seg_10353" s="T260">0.giv-active-Q</ta>
            <ta e="T263" id="Seg_10354" s="T261">0.giv-active</ta>
            <ta e="T265" id="Seg_10355" s="T264">giv-active</ta>
            <ta e="T267" id="Seg_10356" s="T266">0.giv-active</ta>
            <ta e="T269" id="Seg_10357" s="T268">giv-active</ta>
            <ta e="T270" id="Seg_10358" s="T269">giv-active</ta>
            <ta e="T272" id="Seg_10359" s="T271">0.giv-active</ta>
            <ta e="T273" id="Seg_10360" s="T272">0.giv-active</ta>
            <ta e="T276" id="Seg_10361" s="T275">0.giv-active</ta>
            <ta e="T278" id="Seg_10362" s="T277">giv-inactive</ta>
            <ta e="T280" id="Seg_10363" s="T278">0.giv-active</ta>
            <ta e="T283" id="Seg_10364" s="T281">0.giv-active</ta>
            <ta e="T285" id="Seg_10365" s="T284">new</ta>
            <ta e="T286" id="Seg_10366" s="T285">0.giv-active</ta>
            <ta e="T287" id="Seg_10367" s="T286">giv-active</ta>
            <ta e="T291" id="Seg_10368" s="T290">giv-inactive-Q</ta>
            <ta e="T293" id="Seg_10369" s="T292">0.giv-active-Q</ta>
            <ta e="T296" id="Seg_10370" s="T295">0.giv-active-Q</ta>
            <ta e="T297" id="Seg_10371" s="T296">0.giv-active 0.quot-sp</ta>
            <ta e="T299" id="Seg_10372" s="T298">giv-inactive-Q</ta>
            <ta e="T301" id="Seg_10373" s="T300">0.accs-inf-Q</ta>
            <ta e="T302" id="Seg_10374" s="T301">giv-active-Q</ta>
            <ta e="T303" id="Seg_10375" s="T302">0.giv-active-Q</ta>
            <ta e="T305" id="Seg_10376" s="T304">giv-active-Q</ta>
            <ta e="T306" id="Seg_10377" s="T305">giv-active-Q</ta>
            <ta e="T307" id="Seg_10378" s="T306">0.giv-inactive-Q</ta>
            <ta e="T310" id="Seg_10379" s="T309">giv-active-Q</ta>
            <ta e="T311" id="Seg_10380" s="T310">giv-active-Q</ta>
            <ta e="T314" id="Seg_10381" s="T313">0.giv-active 0.quot-sp</ta>
            <ta e="T315" id="Seg_10382" s="T314">giv-active</ta>
            <ta e="T317" id="Seg_10383" s="T315">0.giv-active</ta>
            <ta e="T319" id="Seg_10384" s="T317">0.giv-active</ta>
            <ta e="T320" id="Seg_10385" s="T319">giv-active</ta>
            <ta e="T321" id="Seg_10386" s="T320">giv-active</ta>
            <ta e="T322" id="Seg_10387" s="T321">accs-inf</ta>
            <ta e="T327" id="Seg_10388" s="T326">0.giv-active</ta>
            <ta e="T329" id="Seg_10389" s="T328">0.giv-active</ta>
            <ta e="T331" id="Seg_10390" s="T330">0.giv-active</ta>
            <ta e="T333" id="Seg_10391" s="T332">giv-inactive</ta>
            <ta e="T335" id="Seg_10392" s="T333">0.giv-active</ta>
            <ta e="T339" id="Seg_10393" s="T337">0.giv-active</ta>
            <ta e="T341" id="Seg_10394" s="T339">0.giv-active</ta>
            <ta e="T343" id="Seg_10395" s="T342">new</ta>
            <ta e="T350" id="Seg_10396" s="T349">giv-inactive-Q</ta>
            <ta e="T354" id="Seg_10397" s="T353">0.giv-active-Q</ta>
            <ta e="T356" id="Seg_10398" s="T355">0.giv-active 0.quot-sp</ta>
            <ta e="T357" id="Seg_10399" s="T356">giv-inactive-Q</ta>
            <ta e="T359" id="Seg_10400" s="T358">0.accs-inf-Q</ta>
            <ta e="T361" id="Seg_10401" s="T360">giv-active-Q</ta>
            <ta e="T362" id="Seg_10402" s="T361">0.giv-active-Q</ta>
            <ta e="T365" id="Seg_10403" s="T364">giv-active-Q</ta>
            <ta e="T366" id="Seg_10404" s="T365">giv-active-Q</ta>
            <ta e="T369" id="Seg_10405" s="T368">0.giv-active 0.quot-sp</ta>
            <ta e="T371" id="Seg_10406" s="T369">0.giv-active</ta>
            <ta e="T372" id="Seg_10407" s="T371">accs-inf</ta>
            <ta e="T374" id="Seg_10408" s="T372">0.giv-inactive</ta>
            <ta e="T379" id="Seg_10409" s="T378">giv-active</ta>
            <ta e="T382" id="Seg_10410" s="T381">new</ta>
            <ta e="T386" id="Seg_10411" s="T385">0.giv-active</ta>
            <ta e="T390" id="Seg_10412" s="T389">0.giv-active 0.quot-th</ta>
            <ta e="T392" id="Seg_10413" s="T391">new-Q</ta>
            <ta e="T393" id="Seg_10414" s="T392">accs-inf-Q</ta>
            <ta e="T396" id="Seg_10415" s="T395">giv-active-Q</ta>
            <ta e="T398" id="Seg_10416" s="T397">giv-active-Q</ta>
            <ta e="T400" id="Seg_10417" s="T399">giv-active-Q</ta>
            <ta e="T406" id="Seg_10418" s="T405">giv-active-Q</ta>
            <ta e="T408" id="Seg_10419" s="T407">0.giv-inactive 0.quot-th</ta>
            <ta e="T410" id="Seg_10420" s="T409">giv-active</ta>
            <ta e="T415" id="Seg_10421" s="T414">giv-active</ta>
            <ta e="T416" id="Seg_10422" s="T415">0.giv-active</ta>
            <ta e="T423" id="Seg_10423" s="T422">giv-inactive</ta>
            <ta e="T427" id="Seg_10424" s="T426">0.giv-active</ta>
            <ta e="T429" id="Seg_10425" s="T428">0.giv-active</ta>
            <ta e="T430" id="Seg_10426" s="T429">giv-inactive</ta>
            <ta e="T432" id="Seg_10427" s="T431">new</ta>
            <ta e="T435" id="Seg_10428" s="T434">0.giv-active-Q</ta>
            <ta e="T438" id="Seg_10429" s="T437">giv-active</ta>
            <ta e="T441" id="Seg_10430" s="T440">new</ta>
            <ta e="T443" id="Seg_10431" s="T442">giv-inactive</ta>
            <ta e="T444" id="Seg_10432" s="T443">giv-active</ta>
            <ta e="T445" id="Seg_10433" s="T444">0.giv-active</ta>
            <ta e="T447" id="Seg_10434" s="T446">accs-inf</ta>
            <ta e="T452" id="Seg_10435" s="T451">new</ta>
            <ta e="T454" id="Seg_10436" s="T453">giv-active</ta>
            <ta e="T457" id="Seg_10437" s="T456">giv-active-Q</ta>
            <ta e="T460" id="Seg_10438" s="T459">giv-active-Q</ta>
            <ta e="T461" id="Seg_10439" s="T460">giv-active</ta>
            <ta e="T463" id="Seg_10440" s="T461">0.giv-inactive</ta>
            <ta e="T464" id="Seg_10441" s="T463">new</ta>
            <ta e="T465" id="Seg_10442" s="T464">new</ta>
            <ta e="T466" id="Seg_10443" s="T465">giv-active</ta>
            <ta e="T467" id="Seg_10444" s="T466">accs-inf</ta>
            <ta e="T469" id="Seg_10445" s="T468">giv-active</ta>
            <ta e="T474" id="Seg_10446" s="T473">0.accs-aggr</ta>
            <ta e="T475" id="Seg_10447" s="T474">giv-inactive</ta>
            <ta e="T478" id="Seg_10448" s="T477">giv-active</ta>
            <ta e="T481" id="Seg_10449" s="T480">0.giv-active-Q</ta>
            <ta e="T484" id="Seg_10450" s="T483">0.giv-active 0.quot-sp</ta>
            <ta e="T485" id="Seg_10451" s="T484">giv-active-Q</ta>
            <ta e="T489" id="Seg_10452" s="T488">giv-inactive-Q</ta>
            <ta e="T491" id="Seg_10453" s="T490">giv-active-Q</ta>
            <ta e="T492" id="Seg_10454" s="T491">giv-inactive-Q</ta>
            <ta e="T493" id="Seg_10455" s="T492">0.giv-active-Q</ta>
            <ta e="T494" id="Seg_10456" s="T493">0.giv-active-Q</ta>
            <ta e="T495" id="Seg_10457" s="T494">accs-aggr-Q</ta>
            <ta e="T497" id="Seg_10458" s="T496">0.giv-active-Q</ta>
            <ta e="T498" id="Seg_10459" s="T497">0.giv-active 0.quot-sp</ta>
            <ta e="T502" id="Seg_10460" s="T501">giv-active</ta>
            <ta e="T504" id="Seg_10461" s="T503">0.giv-active</ta>
            <ta e="T506" id="Seg_10462" s="T505">giv-inactive</ta>
            <ta e="T507" id="Seg_10463" s="T506">0.giv-active 0.quot-sp</ta>
            <ta e="T508" id="Seg_10464" s="T507">giv-inactive-Q</ta>
            <ta e="T509" id="Seg_10465" s="T508">giv-inactive-Q</ta>
            <ta e="T513" id="Seg_10466" s="T512">0.giv-active-Q giv-active-Q</ta>
            <ta e="T520" id="Seg_10467" s="T519">0.quot-sp</ta>
            <ta e="T521" id="Seg_10468" s="T520">giv-active-Q</ta>
            <ta e="T522" id="Seg_10469" s="T521">giv-active-Q</ta>
            <ta e="T527" id="Seg_10470" s="T526">quot-sp</ta>
            <ta e="T529" id="Seg_10471" s="T528">giv-inactive</ta>
            <ta e="T532" id="Seg_10472" s="T531">0.accs-aggr-Q</ta>
            <ta e="T533" id="Seg_10473" s="T532">0.giv-active 0.quot-sp</ta>
            <ta e="T535" id="Seg_10474" s="T534">giv-active-Q-Q</ta>
            <ta e="T537" id="Seg_10475" s="T536">giv-inactive-Q-Q</ta>
            <ta e="T538" id="Seg_10476" s="T537">0.giv-active-Q-Q</ta>
            <ta e="T539" id="Seg_10477" s="T538">0.giv-inactive-Q 0.quot-sp-Q</ta>
            <ta e="T540" id="Seg_10478" s="T539">0.giv-active-Q</ta>
            <ta e="T541" id="Seg_10479" s="T540">0.giv-active 0.quot-sp</ta>
            <ta e="T543" id="Seg_10480" s="T542">giv-active</ta>
            <ta e="T546" id="Seg_10481" s="T545">giv-active</ta>
            <ta e="T548" id="Seg_10482" s="T546">0.giv-active</ta>
            <ta e="T551" id="Seg_10483" s="T550">0.giv-active-Q</ta>
            <ta e="T553" id="Seg_10484" s="T552">giv-active-Q</ta>
            <ta e="T554" id="Seg_10485" s="T553">giv-inactive-Q</ta>
            <ta e="T557" id="Seg_10486" s="T556">0.giv-active-Q</ta>
            <ta e="T559" id="Seg_10487" s="T558">0.giv-active-Q</ta>
            <ta e="T561" id="Seg_10488" s="T560">giv-active-Q</ta>
            <ta e="T562" id="Seg_10489" s="T561">giv-inactive-Q</ta>
            <ta e="T565" id="Seg_10490" s="T564">0.giv-active 0.quot-sp</ta>
            <ta e="T569" id="Seg_10491" s="T568">giv-active-Q</ta>
            <ta e="T571" id="Seg_10492" s="T570">accs-inf-Q</ta>
            <ta e="T572" id="Seg_10493" s="T571">0.giv-active-Q</ta>
            <ta e="T574" id="Seg_10494" s="T573">0.accs-inf-Q</ta>
            <ta e="T575" id="Seg_10495" s="T574">quot-sp</ta>
            <ta e="T576" id="Seg_10496" s="T575">giv-active</ta>
            <ta e="T578" id="Seg_10497" s="T577">giv-active</ta>
            <ta e="T580" id="Seg_10498" s="T579">giv-active</ta>
            <ta e="T583" id="Seg_10499" s="T582">giv-inactive-Q</ta>
            <ta e="T585" id="Seg_10500" s="T584">giv-active-Q</ta>
            <ta e="T587" id="Seg_10501" s="T586">accs-inf-Q</ta>
            <ta e="T590" id="Seg_10502" s="T589">new-Q</ta>
            <ta e="T591" id="Seg_10503" s="T590">0.giv-inactive-Q</ta>
            <ta e="T592" id="Seg_10504" s="T591">0.giv-active 0.quot-sp</ta>
            <ta e="T595" id="Seg_10505" s="T594">giv-active</ta>
            <ta e="T596" id="Seg_10506" s="T595">giv-active</ta>
            <ta e="T597" id="Seg_10507" s="T596">0.giv-active</ta>
            <ta e="T598" id="Seg_10508" s="T597">new</ta>
            <ta e="T600" id="Seg_10509" s="T599">0.giv-active</ta>
            <ta e="T601" id="Seg_10510" s="T600">giv-inactive</ta>
            <ta e="T603" id="Seg_10511" s="T601">0.giv-active</ta>
            <ta e="T605" id="Seg_10512" s="T604">giv-active</ta>
            <ta e="T608" id="Seg_10513" s="T607">giv-active</ta>
            <ta e="T615" id="Seg_10514" s="T614">giv-inactive</ta>
            <ta e="T617" id="Seg_10515" s="T616">0.giv-active</ta>
            <ta e="T619" id="Seg_10516" s="T618">0.giv-inactive</ta>
            <ta e="T628" id="Seg_10517" s="T627">giv-inactive-Q</ta>
            <ta e="T630" id="Seg_10518" s="T628">0.giv-active-Q</ta>
            <ta e="T631" id="Seg_10519" s="T630">0.giv-active-Q 0.quot-sp-Q</ta>
            <ta e="T632" id="Seg_10520" s="T631">0.giv-active-Q</ta>
            <ta e="T634" id="Seg_10521" s="T633">0.giv-active 0.quot-sp</ta>
            <ta e="T637" id="Seg_10522" s="T636">giv-active</ta>
            <ta e="T642" id="Seg_10523" s="T641">0.giv-active 0.quot-sp</ta>
            <ta e="T645" id="Seg_10524" s="T644">giv-inactive-Q</ta>
            <ta e="T647" id="Seg_10525" s="T646">giv-inactive-Q</ta>
            <ta e="T649" id="Seg_10526" s="T647">0.giv-active-Q</ta>
            <ta e="T650" id="Seg_10527" s="T649">0.giv-active 0.quot-sp</ta>
            <ta e="T651" id="Seg_10528" s="T650">giv-inactive-Q</ta>
            <ta e="T653" id="Seg_10529" s="T652">giv-inactive-Q</ta>
            <ta e="T655" id="Seg_10530" s="T654">giv-active-Q</ta>
            <ta e="T656" id="Seg_10531" s="T655">0.giv-active-Q</ta>
            <ta e="T659" id="Seg_10532" s="T658">giv-active-Q</ta>
            <ta e="T660" id="Seg_10533" s="T659">0.accs-aggr-Q</ta>
            <ta e="T662" id="Seg_10534" s="T661">0.giv-active 0.quot-sp</ta>
            <ta e="T663" id="Seg_10535" s="T662">giv-active</ta>
            <ta e="T665" id="Seg_10536" s="T664">giv-active</ta>
            <ta e="T666" id="Seg_10537" s="T665">accs-inf</ta>
            <ta e="T671" id="Seg_10538" s="T670">giv-inactive</ta>
            <ta e="T683" id="Seg_10539" s="T682">giv-inactive-Q</ta>
            <ta e="T684" id="Seg_10540" s="T683">0.giv-inactive-Q</ta>
            <ta e="T687" id="Seg_10541" s="T686">giv-inactive-Q</ta>
            <ta e="T688" id="Seg_10542" s="T687">0.giv-active-Q</ta>
            <ta e="T689" id="Seg_10543" s="T688">0.giv-active-Q 0.quot-sp-Q</ta>
            <ta e="T692" id="Seg_10544" s="T691">giv-inactive</ta>
            <ta e="T695" id="Seg_10545" s="T694">giv-inactive</ta>
            <ta e="T696" id="Seg_10546" s="T695">giv-active</ta>
            <ta e="T697" id="Seg_10547" s="T696">accs-inf</ta>
            <ta e="T702" id="Seg_10548" s="T701">accs-gen</ta>
            <ta e="T703" id="Seg_10549" s="T702">accs-inf</ta>
            <ta e="T704" id="Seg_10550" s="T703">new</ta>
            <ta e="T710" id="Seg_10551" s="T709">giv-active</ta>
            <ta e="T712" id="Seg_10552" s="T711">new</ta>
            <ta e="T714" id="Seg_10553" s="T713">giv-active</ta>
            <ta e="T717" id="Seg_10554" s="T716">giv-inactive</ta>
            <ta e="T718" id="Seg_10555" s="T717">giv-inactive</ta>
            <ta e="T722" id="Seg_10556" s="T721">0.giv-active</ta>
            <ta e="T725" id="Seg_10557" s="T724">0.accs-aggr</ta>
            <ta e="T730" id="Seg_10558" s="T729">0.giv-active</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T9" id="Seg_10559" s="T8">0.top.int.concr</ta>
            <ta e="T12" id="Seg_10560" s="T11">0.top.int.concr</ta>
            <ta e="T16" id="Seg_10561" s="T15">0.top.int.concr</ta>
            <ta e="T18" id="Seg_10562" s="T16">top.int.concr</ta>
            <ta e="T26" id="Seg_10563" s="T25">0.top.int.concr</ta>
            <ta e="T41" id="Seg_10564" s="T39">top.int.concr</ta>
            <ta e="T48" id="Seg_10565" s="T44">top.int.concr</ta>
            <ta e="T61" id="Seg_10566" s="T60">0.top.int.concr</ta>
            <ta e="T66" id="Seg_10567" s="T65">top.int.concr</ta>
            <ta e="T72" id="Seg_10568" s="T71">top.int.concr</ta>
            <ta e="T77" id="Seg_10569" s="T76">top.int.concr</ta>
            <ta e="T83" id="Seg_10570" s="T82">top.int.concr</ta>
            <ta e="T86" id="Seg_10571" s="T85">top.int.concr</ta>
            <ta e="T101" id="Seg_10572" s="T100">0.top.int.concr</ta>
            <ta e="T105" id="Seg_10573" s="T104">0.top.int.concr</ta>
            <ta e="T108" id="Seg_10574" s="T107">0.top.int.concr</ta>
            <ta e="T111" id="Seg_10575" s="T109">0.top.int.concr</ta>
            <ta e="T116" id="Seg_10576" s="T111">top.int.concr</ta>
            <ta e="T126" id="Seg_10577" s="T125">0.top.int.concr</ta>
            <ta e="T129" id="Seg_10578" s="T128">top.int.concr</ta>
            <ta e="T133" id="Seg_10579" s="T132">top.int.concr</ta>
            <ta e="T149" id="Seg_10580" s="T148">top.int.concr</ta>
            <ta e="T155" id="Seg_10581" s="T154">0.top.int.concr</ta>
            <ta e="T161" id="Seg_10582" s="T159">0.top.int.concr</ta>
            <ta e="T165" id="Seg_10583" s="T163">0.top.int.concr</ta>
            <ta e="T167" id="Seg_10584" s="T165">top.int.concr</ta>
            <ta e="T174" id="Seg_10585" s="T173">0.top.int.concr</ta>
            <ta e="T186" id="Seg_10586" s="T185">0.top.int.concr</ta>
            <ta e="T193" id="Seg_10587" s="T191">top.int.concr</ta>
            <ta e="T199" id="Seg_10588" s="T198">top.int.concr</ta>
            <ta e="T210" id="Seg_10589" s="T209">top.int.concr</ta>
            <ta e="T214" id="Seg_10590" s="T212">0.top.int.concr</ta>
            <ta e="T217" id="Seg_10591" s="T215">0.top.int.concr</ta>
            <ta e="T219" id="Seg_10592" s="T218">0.top.int.concr</ta>
            <ta e="T222" id="Seg_10593" s="T221">top.int.concr</ta>
            <ta e="T228" id="Seg_10594" s="T226">0.top.int.concr</ta>
            <ta e="T230" id="Seg_10595" s="T229">top.int.concr</ta>
            <ta e="T235" id="Seg_10596" s="T232">top.int.concr</ta>
            <ta e="T242" id="Seg_10597" s="T241">0.top.int.concr</ta>
            <ta e="T252" id="Seg_10598" s="T251">top.int.concr</ta>
            <ta e="T256" id="Seg_10599" s="T255">top.int.concr</ta>
            <ta e="T261" id="Seg_10600" s="T260">0.top.int.concr</ta>
            <ta e="T263" id="Seg_10601" s="T261">0.top.int.concr</ta>
            <ta e="T267" id="Seg_10602" s="T266">0.top.int.concr</ta>
            <ta e="T269" id="Seg_10603" s="T268">top.int.concr</ta>
            <ta e="T272" id="Seg_10604" s="T271">0.top.int.concr</ta>
            <ta e="T273" id="Seg_10605" s="T272">0.top.int.concr</ta>
            <ta e="T276" id="Seg_10606" s="T275">0.top.int.concr</ta>
            <ta e="T280" id="Seg_10607" s="T278">0.top.int.concr</ta>
            <ta e="T283" id="Seg_10608" s="T281">0.top.int.concr</ta>
            <ta e="T286" id="Seg_10609" s="T285">0.top.int.concr</ta>
            <ta e="T287" id="Seg_10610" s="T286">top.int.concr</ta>
            <ta e="T293" id="Seg_10611" s="T292">0.top.int.concr</ta>
            <ta e="T299" id="Seg_10612" s="T298">top.int.concr</ta>
            <ta e="T302" id="Seg_10613" s="T301">top.int.concr</ta>
            <ta e="T306" id="Seg_10614" s="T304">top.int.concr</ta>
            <ta e="T310" id="Seg_10615" s="T309">top.int.concr</ta>
            <ta e="T317" id="Seg_10616" s="T315">0.top.int.concr</ta>
            <ta e="T319" id="Seg_10617" s="T317">0.top.int.concr</ta>
            <ta e="T321" id="Seg_10618" s="T320">top.int.concr</ta>
            <ta e="T327" id="Seg_10619" s="T326">0.top.int.concr</ta>
            <ta e="T329" id="Seg_10620" s="T328">0.top.int.concr</ta>
            <ta e="T331" id="Seg_10621" s="T330">0.top.int.concr</ta>
            <ta e="T335" id="Seg_10622" s="T333">0.top.int.concr</ta>
            <ta e="T336" id="Seg_10623" s="T335">top.int.concr</ta>
            <ta e="T341" id="Seg_10624" s="T339">0.top.int.concr</ta>
            <ta e="T357" id="Seg_10625" s="T356">top.int.concr</ta>
            <ta e="T362" id="Seg_10626" s="T361">0.top.int.concr</ta>
            <ta e="T365" id="Seg_10627" s="T364">top.int.concr</ta>
            <ta e="T371" id="Seg_10628" s="T369">0.top.int.concr</ta>
            <ta e="T372" id="Seg_10629" s="T371">top.int.concr</ta>
            <ta e="T384" id="Seg_10630" s="T381">top.int.concr</ta>
            <ta e="T390" id="Seg_10631" s="T389">0.top.int.concr</ta>
            <ta e="T396" id="Seg_10632" s="T395">top.int.concr</ta>
            <ta e="T400" id="Seg_10633" s="T399">top.int.concr</ta>
            <ta e="T416" id="Seg_10634" s="T415">0.top.int.concr</ta>
            <ta e="T418" id="Seg_10635" s="T417">top.int.concr</ta>
            <ta e="T427" id="Seg_10636" s="T426">0.top.int.concr</ta>
            <ta e="T429" id="Seg_10637" s="T428">0.top.int.concr</ta>
            <ta e="T432" id="Seg_10638" s="T429">top.int.concr</ta>
            <ta e="T435" id="Seg_10639" s="T434">0.top.int.concr</ta>
            <ta e="T438" id="Seg_10640" s="T436">top.int.concr</ta>
            <ta e="T445" id="Seg_10641" s="T444">0.top.int.concr</ta>
            <ta e="T448" id="Seg_10642" s="T445">top.int.concr</ta>
            <ta e="T458" id="Seg_10643" s="T456">top.int.concr</ta>
            <ta e="T463" id="Seg_10644" s="T462">0.top.int.concr</ta>
            <ta e="T466" id="Seg_10645" s="T465">top.int.concr</ta>
            <ta e="T469" id="Seg_10646" s="T468">top.int.concr</ta>
            <ta e="T474" id="Seg_10647" s="T473">0.top.int.concr</ta>
            <ta e="T485" id="Seg_10648" s="T484">top.int.concr</ta>
            <ta e="T493" id="Seg_10649" s="T490">top.int.concr</ta>
            <ta e="T497" id="Seg_10650" s="T496">0.top.int.concr</ta>
            <ta e="T504" id="Seg_10651" s="T503">0.top.int.concr</ta>
            <ta e="T507" id="Seg_10652" s="T506">0.top.int.concr</ta>
            <ta e="T508" id="Seg_10653" s="T507">top.int.concr</ta>
            <ta e="T513" id="Seg_10654" s="T512">0.top.int.concr</ta>
            <ta e="T518" id="Seg_10655" s="T517">0.top.int.concr</ta>
            <ta e="T532" id="Seg_10656" s="T531">0.top.int.concr</ta>
            <ta e="T538" id="Seg_10657" s="T537">0.top.int.concr</ta>
            <ta e="T543" id="Seg_10658" s="T541">top.int.concr</ta>
            <ta e="T548" id="Seg_10659" s="T546">0.top.int.concr</ta>
            <ta e="T553" id="Seg_10660" s="T552">top.int.concr</ta>
            <ta e="T557" id="Seg_10661" s="T556">0.top.int.concr</ta>
            <ta e="T559" id="Seg_10662" s="T558">0.top.int.concr</ta>
            <ta e="T561" id="Seg_10663" s="T559">top.int.concr</ta>
            <ta e="T578" id="Seg_10664" s="T576">top.int.concr</ta>
            <ta e="T583" id="Seg_10665" s="T582">top.int.concr</ta>
            <ta e="T597" id="Seg_10666" s="T596">0.top.int.concr</ta>
            <ta e="T603" id="Seg_10667" s="T601">0.top.int.concr</ta>
            <ta e="T608" id="Seg_10668" s="T606">top.int.concr</ta>
            <ta e="T617" id="Seg_10669" s="T616">0.top.int.concr</ta>
            <ta e="T619" id="Seg_10670" s="T618">0.top.int.concr</ta>
            <ta e="T630" id="Seg_10671" s="T628">0.top.int.concr</ta>
            <ta e="T642" id="Seg_10672" s="T641">0.top.int.concr</ta>
            <ta e="T649" id="Seg_10673" s="T647">0.top.int.concr</ta>
            <ta e="T665" id="Seg_10674" s="T662">top.int.concr</ta>
            <ta e="T671" id="Seg_10675" s="T669">top.int.concr</ta>
            <ta e="T684" id="Seg_10676" s="T683">0.top.int.concr</ta>
            <ta e="T688" id="Seg_10677" s="T687">0.top.int.concr</ta>
            <ta e="T692" id="Seg_10678" s="T690">top.int.concr</ta>
            <ta e="T703" id="Seg_10679" s="T701">top.int.concr</ta>
            <ta e="T722" id="Seg_10680" s="T721">0.top.int.concr</ta>
            <ta e="T730" id="Seg_10681" s="T729">0.top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T4" id="Seg_10682" s="T1">foc.wid</ta>
            <ta e="T9" id="Seg_10683" s="T4">foc.int</ta>
            <ta e="T12" id="Seg_10684" s="T9">foc.int</ta>
            <ta e="T16" id="Seg_10685" s="T12">foc.int</ta>
            <ta e="T21" id="Seg_10686" s="T18">foc.int</ta>
            <ta e="T26" id="Seg_10687" s="T22">foc.int</ta>
            <ta e="T29" id="Seg_10688" s="T26">foc.wid</ta>
            <ta e="T33" id="Seg_10689" s="T31">foc.wid</ta>
            <ta e="T35" id="Seg_10690" s="T33">foc.wid</ta>
            <ta e="T44" id="Seg_10691" s="T41">foc.int</ta>
            <ta e="T57" id="Seg_10692" s="T49">foc.wid</ta>
            <ta e="T61" id="Seg_10693" s="T59">foc.int</ta>
            <ta e="T63" id="Seg_10694" s="T62">foc.nar</ta>
            <ta e="T68" id="Seg_10695" s="T67">foc.nar</ta>
            <ta e="T74" id="Seg_10696" s="T73">foc.ver</ta>
            <ta e="T79" id="Seg_10697" s="T77">foc.int</ta>
            <ta e="T85" id="Seg_10698" s="T84">foc.int</ta>
            <ta e="T88" id="Seg_10699" s="T86">foc.int</ta>
            <ta e="T101" id="Seg_10700" s="T89">foc.int</ta>
            <ta e="T105" id="Seg_10701" s="T101">foc.int</ta>
            <ta e="T108" id="Seg_10702" s="T106">foc.int</ta>
            <ta e="T111" id="Seg_10703" s="T109">foc.int</ta>
            <ta e="T118" id="Seg_10704" s="T116">foc.wid</ta>
            <ta e="T123" id="Seg_10705" s="T121">foc.nar</ta>
            <ta e="T126" id="Seg_10706" s="T124">foc.int</ta>
            <ta e="T130" id="Seg_10707" s="T129">foc.nar</ta>
            <ta e="T135" id="Seg_10708" s="T133">foc.int</ta>
            <ta e="T147" id="Seg_10709" s="T142">foc.wid</ta>
            <ta e="T151" id="Seg_10710" s="T149">foc.int</ta>
            <ta e="T155" id="Seg_10711" s="T153">foc.int</ta>
            <ta e="T161" id="Seg_10712" s="T156">foc.int</ta>
            <ta e="T165" id="Seg_10713" s="T162">foc.int</ta>
            <ta e="T170" id="Seg_10714" s="T167">foc.wid</ta>
            <ta e="T174" id="Seg_10715" s="T172">foc.int</ta>
            <ta e="T177" id="Seg_10716" s="T175">foc.nar</ta>
            <ta e="T186" id="Seg_10717" s="T183">foc.int</ta>
            <ta e="T190" id="Seg_10718" s="T186">foc.int</ta>
            <ta e="T196" id="Seg_10719" s="T194">foc.nar</ta>
            <ta e="T204" id="Seg_10720" s="T199">foc.int</ta>
            <ta e="T209" id="Seg_10721" s="T205">foc.wid</ta>
            <ta e="T212" id="Seg_10722" s="T210">foc.int</ta>
            <ta e="T214" id="Seg_10723" s="T212">foc.int</ta>
            <ta e="T217" id="Seg_10724" s="T215">foc.int</ta>
            <ta e="T219" id="Seg_10725" s="T217">foc.int</ta>
            <ta e="T224" id="Seg_10726" s="T223">foc.nar</ta>
            <ta e="T228" id="Seg_10727" s="T225">foc.int</ta>
            <ta e="T232" id="Seg_10728" s="T230">foc.int</ta>
            <ta e="T238" id="Seg_10729" s="T235">foc.wid</ta>
            <ta e="T242" id="Seg_10730" s="T240">foc.int</ta>
            <ta e="T245" id="Seg_10731" s="T244">foc.nar</ta>
            <ta e="T253" id="Seg_10732" s="T252">foc.nar</ta>
            <ta e="T258" id="Seg_10733" s="T256">foc.int</ta>
            <ta e="T261" id="Seg_10734" s="T259">foc.int</ta>
            <ta e="T263" id="Seg_10735" s="T261">foc.int</ta>
            <ta e="T267" id="Seg_10736" s="T264">foc.int</ta>
            <ta e="T271" id="Seg_10737" s="T269">foc.int</ta>
            <ta e="T272" id="Seg_10738" s="T271">foc.int</ta>
            <ta e="T273" id="Seg_10739" s="T272">foc.int</ta>
            <ta e="T276" id="Seg_10740" s="T274">foc.int</ta>
            <ta e="T280" id="Seg_10741" s="T277">foc.int</ta>
            <ta e="T283" id="Seg_10742" s="T280">foc.int</ta>
            <ta e="T286" id="Seg_10743" s="T283">foc.int</ta>
            <ta e="T289" id="Seg_10744" s="T287">foc.int</ta>
            <ta e="T293" id="Seg_10745" s="T291">foc.int</ta>
            <ta e="T295" id="Seg_10746" s="T294">foc.nar</ta>
            <ta e="T300" id="Seg_10747" s="T299">foc.nar</ta>
            <ta e="T303" id="Seg_10748" s="T302">foc.int</ta>
            <ta e="T307" id="Seg_10749" s="T306">foc.nar</ta>
            <ta e="T313" id="Seg_10750" s="T310">foc.int</ta>
            <ta e="T317" id="Seg_10751" s="T314">foc.int</ta>
            <ta e="T319" id="Seg_10752" s="T317">foc.int</ta>
            <ta e="T324" id="Seg_10753" s="T321">foc.int</ta>
            <ta e="T327" id="Seg_10754" s="T325">foc.int</ta>
            <ta e="T329" id="Seg_10755" s="T327">foc.int</ta>
            <ta e="T331" id="Seg_10756" s="T330">foc.int</ta>
            <ta e="T335" id="Seg_10757" s="T332">foc.int</ta>
            <ta e="T339" id="Seg_10758" s="T337">foc.int</ta>
            <ta e="T341" id="Seg_10759" s="T339">foc.int</ta>
            <ta e="T348" id="Seg_10760" s="T342">foc.wid</ta>
            <ta e="T353" id="Seg_10761" s="T351">foc.nar</ta>
            <ta e="T358" id="Seg_10762" s="T357">foc.nar</ta>
            <ta e="T362" id="Seg_10763" s="T359">foc.int</ta>
            <ta e="T368" id="Seg_10764" s="T365">foc.int</ta>
            <ta e="T371" id="Seg_10765" s="T369">foc.int</ta>
            <ta e="T378" id="Seg_10766" s="T372">foc.int</ta>
            <ta e="T386" id="Seg_10767" s="T384">foc.int</ta>
            <ta e="T390" id="Seg_10768" s="T387">foc.int</ta>
            <ta e="T395" id="Seg_10769" s="T390">foc.wid</ta>
            <ta e="T398" id="Seg_10770" s="T396">foc.int</ta>
            <ta e="T403" id="Seg_10771" s="T400">foc.int</ta>
            <ta e="T416" id="Seg_10772" s="T415">foc.int</ta>
            <ta e="T419" id="Seg_10773" s="T418">foc.nar</ta>
            <ta e="T421" id="Seg_10774" s="T420">foc.ver</ta>
            <ta e="T427" id="Seg_10775" s="T423">foc.int</ta>
            <ta e="T429" id="Seg_10776" s="T427">foc.int</ta>
            <ta e="T433" id="Seg_10777" s="T432">foc.int</ta>
            <ta e="T436" id="Seg_10778" s="T434">foc.int</ta>
            <ta e="T442" id="Seg_10779" s="T438">foc.int</ta>
            <ta e="T445" id="Seg_10780" s="T444">foc.int</ta>
            <ta e="T453" id="Seg_10781" s="T448">foc.int</ta>
            <ta e="T456" id="Seg_10782" s="T453">foc.wid</ta>
            <ta e="T459" id="Seg_10783" s="T458">foc.nar</ta>
            <ta e="T463" id="Seg_10784" s="T460">foc.int</ta>
            <ta e="T465" id="Seg_10785" s="T463">foc.wid</ta>
            <ta e="T468" id="Seg_10786" s="T466">foc.int</ta>
            <ta e="T472" id="Seg_10787" s="T469">foc.int</ta>
            <ta e="T475" id="Seg_10788" s="T473">foc.int</ta>
            <ta e="T476" id="Seg_10789" s="T475">foc.int</ta>
            <ta e="T480" id="Seg_10790" s="T478">foc.nar</ta>
            <ta e="T490" id="Seg_10791" s="T486">foc.int</ta>
            <ta e="T494" id="Seg_10792" s="T493">foc.int</ta>
            <ta e="T497" id="Seg_10793" s="T494">foc.int</ta>
            <ta e="T504" id="Seg_10794" s="T501">foc.int</ta>
            <ta e="T507" id="Seg_10795" s="T504">foc.int</ta>
            <ta e="T510" id="Seg_10796" s="T509">foc.ver</ta>
            <ta e="T512" id="Seg_10797" s="T511">foc.nar</ta>
            <ta e="T515" id="Seg_10798" s="T513">foc.nar</ta>
            <ta e="T526" id="Seg_10799" s="T525">foc.ver</ta>
            <ta e="T532" id="Seg_10800" s="T529">foc.int</ta>
            <ta e="T536" id="Seg_10801" s="T533">foc.wid</ta>
            <ta e="T538" id="Seg_10802" s="T536">foc.int</ta>
            <ta e="T545" id="Seg_10803" s="T543">foc.int</ta>
            <ta e="T548" id="Seg_10804" s="T545">foc.int</ta>
            <ta e="T550" id="Seg_10805" s="T549">foc.nar</ta>
            <ta e="T554" id="Seg_10806" s="T553">foc.int</ta>
            <ta e="T556" id="Seg_10807" s="T554">foc.nar</ta>
            <ta e="T559" id="Seg_10808" s="T558">foc.int</ta>
            <ta e="T564" id="Seg_10809" s="T561">foc.int</ta>
            <ta e="T574" id="Seg_10810" s="T572">foc.int</ta>
            <ta e="T581" id="Seg_10811" s="T578">foc.int</ta>
            <ta e="T586" id="Seg_10812" s="T585">foc.nar</ta>
            <ta e="T591" id="Seg_10813" s="T586">foc.int</ta>
            <ta e="T597" id="Seg_10814" s="T596">foc.contr</ta>
            <ta e="T603" id="Seg_10815" s="T597">foc.int</ta>
            <ta e="T612" id="Seg_10816" s="T608">foc.int</ta>
            <ta e="T617" id="Seg_10817" s="T612">foc.int</ta>
            <ta e="T626" id="Seg_10818" s="T618">foc.int</ta>
            <ta e="T630" id="Seg_10819" s="T627">foc.int</ta>
            <ta e="T636" id="Seg_10820" s="T634">foc.int</ta>
            <ta e="T642" id="Seg_10821" s="T637">foc.int</ta>
            <ta e="T646" id="Seg_10822" s="T643">foc.wid</ta>
            <ta e="T649" id="Seg_10823" s="T646">foc.int</ta>
            <ta e="T656" id="Seg_10824" s="T651">foc.int</ta>
            <ta e="T658" id="Seg_10825" s="T657">foc.nar</ta>
            <ta e="T667" id="Seg_10826" s="T665">foc.nar</ta>
            <ta e="T673" id="Seg_10827" s="T671">foc.int</ta>
            <ta e="T680" id="Seg_10828" s="T679">foc.ver</ta>
            <ta e="T688" id="Seg_10829" s="T686">foc.int</ta>
            <ta e="T699" id="Seg_10830" s="T695">foc.int</ta>
            <ta e="T705" id="Seg_10831" s="T703">foc.int</ta>
            <ta e="T717" id="Seg_10832" s="T710">foc.wid</ta>
            <ta e="T722" id="Seg_10833" s="T720">foc.int</ta>
            <ta e="T730" id="Seg_10834" s="T727">foc.int</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_10835" s="T7">EV:cult</ta>
            <ta e="T52" id="Seg_10836" s="T51">RUS:gram</ta>
            <ta e="T106" id="Seg_10837" s="T105">RUS:gram</ta>
            <ta e="T107" id="Seg_10838" s="T106">RUS:cult</ta>
            <ta e="T109" id="Seg_10839" s="T108">RUS:gram</ta>
            <ta e="T112" id="Seg_10840" s="T111">RUS:mod</ta>
            <ta e="T156" id="Seg_10841" s="T155">RUS:gram</ta>
            <ta e="T159" id="Seg_10842" s="T158">RUS:cult</ta>
            <ta e="T197" id="Seg_10843" s="T196">RUS:mod</ta>
            <ta e="T215" id="Seg_10844" s="T214">RUS:gram</ta>
            <ta e="T220" id="Seg_10845" s="T219">RUS:gram</ta>
            <ta e="T221" id="Seg_10846" s="T220">RUS:cult</ta>
            <ta e="T222" id="Seg_10847" s="T221">RUS:cult</ta>
            <ta e="T229" id="Seg_10848" s="T228">RUS:cult</ta>
            <ta e="T260" id="Seg_10849" s="T259">RUS:mod</ta>
            <ta e="T264" id="Seg_10850" s="T263">RUS:gram</ta>
            <ta e="T274" id="Seg_10851" s="T273">RUS:gram</ta>
            <ta e="T277" id="Seg_10852" s="T276">RUS:gram</ta>
            <ta e="T278" id="Seg_10853" s="T277">RUS:cult</ta>
            <ta e="T325" id="Seg_10854" s="T324">RUS:gram</ta>
            <ta e="T330" id="Seg_10855" s="T329">RUS:gram</ta>
            <ta e="T332" id="Seg_10856" s="T331">RUS:gram</ta>
            <ta e="T333" id="Seg_10857" s="T332">RUS:cult</ta>
            <ta e="T360" id="Seg_10858" s="T359">RUS:mod</ta>
            <ta e="T375" id="Seg_10859" s="T374">RUS:gram</ta>
            <ta e="T390" id="Seg_10860" s="T389">RUS:core</ta>
            <ta e="T403" id="Seg_10861" s="T402">RUS:mod</ta>
            <ta e="T432" id="Seg_10862" s="T431">RUS:cult</ta>
            <ta e="T441" id="Seg_10863" s="T440">RUS:cult</ta>
            <ta e="T452" id="Seg_10864" s="T451">EV:cult</ta>
            <ta e="T454" id="Seg_10865" s="T453">EV:cult</ta>
            <ta e="T460" id="Seg_10866" s="T459">EV:cult</ta>
            <ta e="T461" id="Seg_10867" s="T460">EV:cult</ta>
            <ta e="T651" id="Seg_10868" s="T650">EV:core</ta>
            <ta e="T712" id="Seg_10869" s="T711">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T107" id="Seg_10870" s="T106">medVins Vsub</ta>
            <ta e="T112" id="Seg_10871" s="T111">Vsub Vsub</ta>
            <ta e="T159" id="Seg_10872" s="T158">medVins Vsub</ta>
            <ta e="T197" id="Seg_10873" s="T196">Vsub Csub Vsub</ta>
            <ta e="T221" id="Seg_10874" s="T220">medVins Vsub</ta>
            <ta e="T222" id="Seg_10875" s="T221">medVins Vsub</ta>
            <ta e="T229" id="Seg_10876" s="T228">medVins Vsub</ta>
            <ta e="T260" id="Seg_10877" s="T259">Vsub Csub Vsub</ta>
            <ta e="T278" id="Seg_10878" s="T277">medVins Vsub</ta>
            <ta e="T333" id="Seg_10879" s="T332">medVins Vsub</ta>
            <ta e="T360" id="Seg_10880" s="T359">Vsub Csub Vsub</ta>
            <ta e="T403" id="Seg_10881" s="T402">Vsub</ta>
            <ta e="T432" id="Seg_10882" s="T431">medVins Vsub medVins lenition</ta>
            <ta e="T441" id="Seg_10883" s="T440">Vsub Vsub</ta>
            <ta e="T712" id="Seg_10884" s="T711">Csub Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T8" id="Seg_10885" s="T7">dir:bare</ta>
            <ta e="T52" id="Seg_10886" s="T51">dir:bare</ta>
            <ta e="T106" id="Seg_10887" s="T105">dir:bare</ta>
            <ta e="T107" id="Seg_10888" s="T106">dir:infl</ta>
            <ta e="T109" id="Seg_10889" s="T108">dir:bare</ta>
            <ta e="T112" id="Seg_10890" s="T111">dir:bare</ta>
            <ta e="T156" id="Seg_10891" s="T155">dir:bare</ta>
            <ta e="T159" id="Seg_10892" s="T158">dir:infl</ta>
            <ta e="T197" id="Seg_10893" s="T196">dir:bare</ta>
            <ta e="T215" id="Seg_10894" s="T214">dir:bare</ta>
            <ta e="T220" id="Seg_10895" s="T219">dir:bare</ta>
            <ta e="T221" id="Seg_10896" s="T220">dir:infl</ta>
            <ta e="T222" id="Seg_10897" s="T221">dir:bare</ta>
            <ta e="T229" id="Seg_10898" s="T228">dir:infl</ta>
            <ta e="T260" id="Seg_10899" s="T259">dir:bare</ta>
            <ta e="T264" id="Seg_10900" s="T263">dir:bare</ta>
            <ta e="T274" id="Seg_10901" s="T273">dir:bare</ta>
            <ta e="T277" id="Seg_10902" s="T276">dir:bare</ta>
            <ta e="T278" id="Seg_10903" s="T277">dir:infl</ta>
            <ta e="T325" id="Seg_10904" s="T324">dir:bare</ta>
            <ta e="T330" id="Seg_10905" s="T329">dir:bare</ta>
            <ta e="T332" id="Seg_10906" s="T331">dir:bare</ta>
            <ta e="T333" id="Seg_10907" s="T332">dir:infl</ta>
            <ta e="T360" id="Seg_10908" s="T359">dir:bare</ta>
            <ta e="T375" id="Seg_10909" s="T374">dir:bare</ta>
            <ta e="T390" id="Seg_10910" s="T389">indir:infl</ta>
            <ta e="T403" id="Seg_10911" s="T402">dir:bare</ta>
            <ta e="T432" id="Seg_10912" s="T431">dir:infl</ta>
            <ta e="T441" id="Seg_10913" s="T440">dir:infl</ta>
            <ta e="T452" id="Seg_10914" s="T451">dir:bare</ta>
            <ta e="T454" id="Seg_10915" s="T453">dir:bare</ta>
            <ta e="T460" id="Seg_10916" s="T459">dir:infl</ta>
            <ta e="T461" id="Seg_10917" s="T460">dir:infl</ta>
            <ta e="T651" id="Seg_10918" s="T650">dir:infl</ta>
            <ta e="T712" id="Seg_10919" s="T711">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_10920" s="T0">There lived Chopochuka.</ta>
            <ta e="T12" id="Seg_10921" s="T4">He has a pole tent, which is built up with five pieces of wood, he has got a net for crucian carps with five stones.</ta>
            <ta e="T16" id="Seg_10922" s="T12">Every day he goes fishing with the net.</ta>
            <ta e="T22" id="Seg_10923" s="T16">This human gathered the fish together on piles, from his net.</ta>
            <ta e="T31" id="Seg_10924" s="T22">So he went once, and polar foxes or so ate his fish there.</ta>
            <ta e="T35" id="Seg_10925" s="T31">"One needs to follow it, one needs to follow his trace."</ta>
            <ta e="T44" id="Seg_10926" s="T35">After he had carried the fish into his house, this human followed the polar foxes trace.</ta>
            <ta e="T57" id="Seg_10927" s="T44">As he was following the polar foxes trace, well, the polar fox jumped out of a bush.</ta>
            <ta e="T64" id="Seg_10928" s="T57">"Oh, Chopochuka, you frightened someone, where do you come from?"</ta>
            <ta e="T70" id="Seg_10929" s="T64">"Well, why did you steal my fish?"</ta>
            <ta e="T76" id="Seg_10930" s="T70">"Well, did I steal, though?", he said.</ta>
            <ta e="T85" id="Seg_10931" s="T76">That one became a mouse, having become a mouse, the old man goes.</ta>
            <ta e="T89" id="Seg_10932" s="T85">That one he devoured, the polar fox.</ta>
            <ta e="T101" id="Seg_10933" s="T89">The belly of that one, well, he cut it and jumped out.</ta>
            <ta e="T111" id="Seg_10934" s="T101">So he skinned that one, put it into his pocket and went.</ta>
            <ta e="T118" id="Seg_10935" s="T111">As he was going again, a hare turned up.</ta>
            <ta e="T127" id="Seg_10936" s="T118">"Oh, Chopochuka, what, where do you come from, you did frighten someone", he said.</ta>
            <ta e="T135" id="Seg_10937" s="T127">"Well, why did you eat all of my fish, I'm looking for my fish."</ta>
            <ta e="T138" id="Seg_10938" s="T135">That one…</ta>
            <ta e="T141" id="Seg_10939" s="T138">Well, also…</ta>
            <ta e="T147" id="Seg_10940" s="T141">Well, he fights, eh, with the hare.</ta>
            <ta e="T162" id="Seg_10941" s="T147">As they are fighting, this human hardly lets the hare live; he killed that one and put it into his pocket, after he had skinned it.</ta>
            <ta e="T165" id="Seg_10942" s="T162">Again he was walking.</ta>
            <ta e="T170" id="Seg_10943" s="T165">As he was walking, a fox turned up there.</ta>
            <ta e="T180" id="Seg_10944" s="T170">"Oh, Chopochuka, you did frighten someone, where, where do you come from?", it said.</ta>
            <ta e="T186" id="Seg_10945" s="T180">"Well", he said, "you ate up all my fish.</ta>
            <ta e="T190" id="Seg_10946" s="T186">Therefore I'm following you."</ta>
            <ta e="T197" id="Seg_10947" s="T190">"Pa, we even didn't come close to your fish."</ta>
            <ta e="T205" id="Seg_10948" s="T197">"Instead I'll rip you up and eat you", it said.</ta>
            <ta e="T209" id="Seg_10949" s="T205">Well, they fight, those people.</ta>
            <ta e="T212" id="Seg_10950" s="T209">Chopochuka hardly lets that one live:</ta>
            <ta e="T217" id="Seg_10951" s="T212">He turned it down and killed it.</ta>
            <ta e="T225" id="Seg_10952" s="T217">He skinned that one and into his pocket; the pocket is big, I suppose.</ta>
            <ta e="T229" id="Seg_10953" s="T225">He put the fox into his pocket.</ta>
            <ta e="T232" id="Seg_10954" s="T229">Then he went on.</ta>
            <ta e="T238" id="Seg_10955" s="T232">As he was walking, a wolverine turned up.</ta>
            <ta e="T244" id="Seg_10956" s="T238">"Hey, Chopochuka, you did just frighten someone", it said.</ta>
            <ta e="T250" id="Seg_10957" s="T244">"Why are you going in this country, what?"</ta>
            <ta e="T255" id="Seg_10958" s="T250">"Well, why did you eat all of my fish?</ta>
            <ta e="T261" id="Seg_10959" s="T255">I'm looking for it", he said, "you were still eating."</ta>
            <ta e="T267" id="Seg_10960" s="T261">He catches it and fights.</ta>
            <ta e="T272" id="Seg_10961" s="T267">Fighting so Chopchuka hardly lets it live, he didn't let it live.</ta>
            <ta e="T280" id="Seg_10962" s="T272">He killed it, skinned it and put it into his pocket.</ta>
            <ta e="T283" id="Seg_10963" s="T280">Again he went on.</ta>
            <ta e="T286" id="Seg_10964" s="T283">He was walking and met a wolf.</ta>
            <ta e="T289" id="Seg_10965" s="T286">The wolf turned up.</ta>
            <ta e="T297" id="Seg_10966" s="T289">"Oh, Chopochuka, you did frighten someone, well, where are you going?", it said.</ta>
            <ta e="T301" id="Seg_10967" s="T297">"Well, why did you eat my fish?</ta>
            <ta e="T303" id="Seg_10968" s="T301">I'm looking for it."</ta>
            <ta e="T314" id="Seg_10969" s="T303">"Pa, we even didn't come close to your fish, instead I'll eat you", it said. </ta>
            <ta e="T320" id="Seg_10970" s="T314">He put that one into the mouth and devoured him, Chopochuka.</ta>
            <ta e="T327" id="Seg_10971" s="T320">Chopochuka splits its belly and runs out.</ta>
            <ta e="T335" id="Seg_10972" s="T327">So he did, he skinned it and put it into his pocket.</ta>
            <ta e="T341" id="Seg_10973" s="T335">Then he went on and on.</ta>
            <ta e="T348" id="Seg_10974" s="T341">Oh, and there a bear up then.</ta>
            <ta e="T356" id="Seg_10975" s="T348">"Oh, Chopochuka, why, where are you going then?", it said.</ta>
            <ta e="T359" id="Seg_10976" s="T356">"Why did you steal my fish?</ta>
            <ta e="T364" id="Seg_10977" s="T359">You still stole my fish…"</ta>
            <ta e="T369" id="Seg_10978" s="T364">"I'll devour you", it said.</ta>
            <ta e="T371" id="Seg_10979" s="T369">It ate him.</ta>
            <ta e="T379" id="Seg_10980" s="T371">He splits it belly and runs out, this human.</ta>
            <ta e="T387" id="Seg_10981" s="T379">After he had skinned it with his knife, he lived in plenty.</ta>
            <ta e="T390" id="Seg_10982" s="T387">He lived and thought:</ta>
            <ta e="T395" id="Seg_10983" s="T390">"One should go and look for the czar's daughter.</ta>
            <ta e="T403" id="Seg_10984" s="T395">The czar has a single daughter, they said, one needs to search her.</ta>
            <ta e="T408" id="Seg_10985" s="T403">A human who owns that much", he thought.</ta>
            <ta e="T416" id="Seg_10986" s="T408">Well, in order to go to the czar, this human is sitting, he is sitting.</ta>
            <ta e="T419" id="Seg_10987" s="T416">"Well, it is good to go there."</ta>
            <ta e="T423" id="Seg_10988" s="T419">Well, he goes, this human.</ta>
            <ta e="T427" id="Seg_10989" s="T423">He was going and going and lived at one place.</ta>
            <ta e="T434" id="Seg_10990" s="T427">He was living and looked around, the czar's porch is suddenly visible.</ta>
            <ta e="T436" id="Seg_10991" s="T434">"I'm arriving there."</ta>
            <ta e="T444" id="Seg_10992" s="T436">This human was going and going, going and going and he came into a town, into the czar's one.</ta>
            <ta e="T453" id="Seg_10993" s="T444">He came there, at the edge the stands a very bad, smoking pit-dwelling.</ta>
            <ta e="T456" id="Seg_10994" s="T453">A pit-dwelling is standing there.</ta>
            <ta e="T460" id="Seg_10995" s="T456">"It is good to go there, into the pit-dwelling."</ta>
            <ta e="T465" id="Seg_10996" s="T460">He ran into the pit-dwelling; an old man and an old woman.</ta>
            <ta e="T472" id="Seg_10997" s="T465">The old woman has no eyes, the old man is serious.</ta>
            <ta e="T478" id="Seg_10998" s="T472">They give him food, the guest eats.</ta>
            <ta e="T484" id="Seg_10999" s="T478">"Where do you come from?", they ask for news.</ta>
            <ta e="T498" id="Seg_11000" s="T484">"I had a net for crucian carps with five stones, as was collecting the fish out of it, I was robbed, I killed the unhappy ones and come here", he said.</ta>
            <ta e="T504" id="Seg_11001" s="T498">Eh, he killed the unhappy ones and came.</ta>
            <ta e="T511" id="Seg_11002" s="T504">He asks the old man: "Does the czar have a daughter?"</ta>
            <ta e="T520" id="Seg_11003" s="T511">"He has a single daughter, she will married somewhere, it is said", he said.</ta>
            <ta e="T529" id="Seg_11004" s="T520">"Old man, won't you go as a matchmaker", that one asked, the guest.</ta>
            <ta e="T533" id="Seg_11005" s="T529">"In either case we'll try to ask", he said.</ta>
            <ta e="T536" id="Seg_11006" s="T533">"'A guest has come.</ta>
            <ta e="T541" id="Seg_11007" s="T536">He asks for your daughter', say [to him]", he said.</ta>
            <ta e="T548" id="Seg_11008" s="T541">This human went, he ran to the czar.</ta>
            <ta e="T551" id="Seg_11009" s="T548">"Well, why are you coming?"</ta>
            <ta e="T559" id="Seg_11010" s="T551">"No, I have guest, he came from somewhere, I don't know.</ta>
            <ta e="T565" id="Seg_11011" s="T559">That human asks for your daughter", he said.</ta>
            <ta e="T576" id="Seg_11012" s="T565">"So then, for I can see this human's face and eyes, bring him here", the czar said.</ta>
            <ta e="T581" id="Seg_11013" s="T576">The old man brought that guest.</ta>
            <ta e="T592" id="Seg_11014" s="T581">"Pa, I don't give [her] to such a man, break his arms and legs and throw him into prison", he said.</ta>
            <ta e="T593" id="Seg_11015" s="T592">So.</ta>
            <ta e="T597" id="Seg_11016" s="T593">They didn't break that human's arms and legs.</ta>
            <ta e="T605" id="Seg_11017" s="T597">They tied him up with a rope and threw him into prison, that human.</ta>
            <ta e="T612" id="Seg_11018" s="T605">That human became a mouse and ran out.</ta>
            <ta e="T617" id="Seg_11019" s="T612">He ran out and went again to the old man.</ta>
            <ta e="T634" id="Seg_11020" s="T617">I don't know, whether a different human came or what, again he said "'He asks for your daughter', go and say it to him."</ta>
            <ta e="T637" id="Seg_11021" s="T634">The old man went again.</ta>
            <ta e="T642" id="Seg_11022" s="T637">He went and said again: </ta>
            <ta e="T646" id="Seg_11023" s="T642">"Well, there came a guest again.</ta>
            <ta e="T650" id="Seg_11024" s="T646">He asks for your daughter", he said.</ta>
            <ta e="T662" id="Seg_11025" s="T650">"Friends, look after the human which you threw into prison recently, what kind of human we are torturing there", he said.</ta>
            <ta e="T673" id="Seg_11026" s="T662">There is just a crooked trace of the human which was thrown into prison, the tied up rope lies there like a rope.</ta>
            <ta e="T677" id="Seg_11027" s="T673">Well then, so, well.</ta>
            <ta e="T686" id="Seg_11028" s="T677">"Oh, well, I indeed did [= locked up] a good human probably.</ta>
            <ta e="T690" id="Seg_11029" s="T686">I'll give my daughter, I'm saying."</ta>
            <ta e="T699" id="Seg_11030" s="T690">That one threw all the unhappy ones which he had killed onto the czar's knees.</ta>
            <ta e="T710" id="Seg_11031" s="T699">So behind the reindeer caravan a small bird is singing, this human separated and brought (…).</ta>
            <ta e="T720" id="Seg_11032" s="T710">With different soldiers and alike this human is going home, after he had taken the daughter.</ta>
            <ta e="T722" id="Seg_11033" s="T720">Well, he nomadizes.</ta>
            <ta e="T733" id="Seg_11034" s="T722">Nomadizing they came there, he lived in richness and satiety, well, that's the end.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_11035" s="T0">Es lebte Tschopotschuka.</ta>
            <ta e="T12" id="Seg_11036" s="T4">Er hat ein mit fünf Hölzern aufgestelltes Stangenzelt, er hat ein Karauschennetz mit fünf Steinen.</ta>
            <ta e="T16" id="Seg_11037" s="T12">Jeden Tag geht er mit dem Netz fischen.</ta>
            <ta e="T22" id="Seg_11038" s="T16">Dieser Mensch legte den Fisch auf Haufen zusammen, aus seinem Netz.</ta>
            <ta e="T31" id="Seg_11039" s="T22">So ging er einmal, und Polarfüchse und oder so fraßen da seinen Fisch.</ta>
            <ta e="T35" id="Seg_11040" s="T31">"Den muss man verfolgen, seiner Spur muss man folgen."</ta>
            <ta e="T44" id="Seg_11041" s="T35">Nachdem er den Fisch in sein Haus getragen hatte, folgte dieser Mensch der Spur des Polarfuchses.</ta>
            <ta e="T57" id="Seg_11042" s="T44">Als er der Spur des Polarfuchses folgte, nun, da sprang der Polarfuchs aus einem Busch hervor.</ta>
            <ta e="T64" id="Seg_11043" s="T57">"Oh, Tschopotschuka, du hast jemanden erschreckt, woher kommst du?"</ta>
            <ta e="T70" id="Seg_11044" s="T64">"Hey, warum habt ihr meinen Fisch gestohlen?"</ta>
            <ta e="T76" id="Seg_11045" s="T70">"Was, habe ich gestohlen?", sagte er.</ta>
            <ta e="T85" id="Seg_11046" s="T76">Jener wurde zu einer Maus, zu einer Maus geworden geht der alte Mann.</ta>
            <ta e="T89" id="Seg_11047" s="T85">Diesen verschlang er, der Polarfuchs.</ta>
            <ta e="T101" id="Seg_11048" s="T89">Den Bauch von diesem zerschnitt er und sprang hinaus.</ta>
            <ta e="T111" id="Seg_11049" s="T101">So zog er diesem die Haut ab, steckte es in die Tasche und ging.</ta>
            <ta e="T118" id="Seg_11050" s="T111">Als er wieder weiterging, stellte sich ihm ein Hase.</ta>
            <ta e="T127" id="Seg_11051" s="T118">"Oh, Tschopotschuka, was, woher kommst du, du hast jemanden erschreckt", sagte er.</ta>
            <ta e="T135" id="Seg_11052" s="T127">"Na, warum habt ihr meinen ganzen Fisch gefressen, ich suche meinen Fisch."</ta>
            <ta e="T138" id="Seg_11053" s="T135">Jener…</ta>
            <ta e="T141" id="Seg_11054" s="T138">Also auch…</ta>
            <ta e="T147" id="Seg_11055" s="T141">Nun, er kämpft, äh, mit dem Hasen.</ta>
            <ta e="T162" id="Seg_11056" s="T147">Während sie kämpfen, lässt dieser Mensch den Hasen kaum am Leben; er tötete ihn und steckte ihn auch in seine Tasche, nachdem er ihm die Haut abgezogen hatte.</ta>
            <ta e="T165" id="Seg_11057" s="T162">Wieder ging er.</ta>
            <ta e="T170" id="Seg_11058" s="T165">Als er so ging und ging, kam ein Fuchs angesprungen.</ta>
            <ta e="T180" id="Seg_11059" s="T170">"Oh, Tschopotschuka, du hast jemanden erschreckt, woher, woher kommst du denn?", sagte er.</ta>
            <ta e="T186" id="Seg_11060" s="T180">"Nun", sagte er, "ihr habt meinen ganzen Fisch gefressen.</ta>
            <ta e="T190" id="Seg_11061" s="T186">Deshalb verfolge ich euch."</ta>
            <ta e="T197" id="Seg_11062" s="T190">"Pa, wir haben uns deinem Fisch nicht einmal genähert."</ta>
            <ta e="T205" id="Seg_11063" s="T197">"Dafür zerreiße ich dich und esse dich", sagte er.</ta>
            <ta e="T209" id="Seg_11064" s="T205">Nun, sie kämpfen, diese Leute.</ta>
            <ta e="T212" id="Seg_11065" s="T209">Tschopotschuka lässt jenen kaum am Leben: </ta>
            <ta e="T217" id="Seg_11066" s="T212">Er warf ihn auf die Seite und tötete ihn.</ta>
            <ta e="T225" id="Seg_11067" s="T217">Er zog jenem die Haut ab und in die Tasche; die Tasche ist wohl groß, wundere ich mich.</ta>
            <ta e="T229" id="Seg_11068" s="T225">Er steckte den Fuchs in seine Tasche.</ta>
            <ta e="T232" id="Seg_11069" s="T229">Dann ging er.</ta>
            <ta e="T238" id="Seg_11070" s="T232">Während er ging, kam ein Vielfraß gesprungen.</ta>
            <ta e="T244" id="Seg_11071" s="T238">"Hey, Tschopotschuka, du hast gerade jemanden erschreckt", sagte es.</ta>
            <ta e="T250" id="Seg_11072" s="T244">"Warum läufst du in diesem Land herum, was?"</ta>
            <ta e="T255" id="Seg_11073" s="T250">"Nun, warum habt ihr meinen ganzen Fisch gefressen?</ta>
            <ta e="T261" id="Seg_11074" s="T255">Ich suche ihn", sagte er, "ihr habt noch gegessen."</ta>
            <ta e="T267" id="Seg_11075" s="T261">Er greift jenen und er kämpft.</ta>
            <ta e="T272" id="Seg_11076" s="T267">So kämpfend lässt Tschopotschuka ihn kaum, er ließ es nicht am Leben.</ta>
            <ta e="T280" id="Seg_11077" s="T272">Er tötete ihn, zog auch ihm die Haut ab und steckte es in seine Tasche.</ta>
            <ta e="T283" id="Seg_11078" s="T280">Wieder ging er.</ta>
            <ta e="T286" id="Seg_11079" s="T283">Er ging und ging und traf einen Wolf.</ta>
            <ta e="T289" id="Seg_11080" s="T286">Der Wolf sprang auf.</ta>
            <ta e="T297" id="Seg_11081" s="T289">"Oh, Tschopotschuka, du hast jemanden erschreckt, na, wohin gehst du?", sagte er.</ta>
            <ta e="T301" id="Seg_11082" s="T297">"Nun, warum habt ihr meinen Fisch gefressen?</ta>
            <ta e="T303" id="Seg_11083" s="T301">Den suche ich."</ta>
            <ta e="T314" id="Seg_11084" s="T303">"Pa, deinem Fisch haben wir uns nicht einmal genähert, dafür fresse ich dich", sagte er.</ta>
            <ta e="T320" id="Seg_11085" s="T314">Er nahm jenen in den Mund und verschlang ihn, den Tschopotschuka.</ta>
            <ta e="T327" id="Seg_11086" s="T320">Tschopotschuka spaltet seinen Bauch und rennt hinaus.</ta>
            <ta e="T335" id="Seg_11087" s="T327">So machte er, zog ihm die Haut ab und steckte ihn in die Tasche.</ta>
            <ta e="T341" id="Seg_11088" s="T335">Dann ging und ging er.</ta>
            <ta e="T348" id="Seg_11089" s="T341">Oh, und da kam ein Bär gesprungen, so.</ta>
            <ta e="T356" id="Seg_11090" s="T348">"Oh, Tschopotschuka, warum, wohin gehst du so?", sagte er.</ta>
            <ta e="T359" id="Seg_11091" s="T356">"Warum habt ihr meinen Fisch gestohlen?</ta>
            <ta e="T364" id="Seg_11092" s="T359">Ihr habt meinen Fisch gestohlen…"</ta>
            <ta e="T369" id="Seg_11093" s="T364">"Ich verschlinge dich", sagte er.</ta>
            <ta e="T371" id="Seg_11094" s="T369">Er fraß ihn.</ta>
            <ta e="T379" id="Seg_11095" s="T371">Er spaltet seinen Bauch und rennt hinaus, dieser Mensch.</ta>
            <ta e="T387" id="Seg_11096" s="T379">Nachdem er ihm mit seinem Messer die Haut abgezogen hatte, lebte er ausgefüllt.</ta>
            <ta e="T390" id="Seg_11097" s="T387">Er lebte und er dachte:</ta>
            <ta e="T395" id="Seg_11098" s="T390">"Man sollte gehen und die Tochter des Zaren suchen.</ta>
            <ta e="T403" id="Seg_11099" s="T395">Der Zar hat eine einzige Tochter, wurde gesagt, die muss man suchen gehen.</ta>
            <ta e="T408" id="Seg_11100" s="T403">Ein Mensch, der so viel hat", dachte er.</ta>
            <ta e="T416" id="Seg_11101" s="T408">Nun, um zum Zaren zu gehen, sitzt dieser Mensch, er sitzt.</ta>
            <ta e="T419" id="Seg_11102" s="T416">"Nun, es ist gut zu gehen."</ta>
            <ta e="T423" id="Seg_11103" s="T419">Da geht dieser Mensch.</ta>
            <ta e="T427" id="Seg_11104" s="T423">Er ging und ging und lebte an einem Ort.</ta>
            <ta e="T434" id="Seg_11105" s="T427">Er lebte und sah sich um, die Veranda des Zaren ist auf einmal zu sehen.</ta>
            <ta e="T436" id="Seg_11106" s="T434">"Ich bin hier angekommen."</ta>
            <ta e="T444" id="Seg_11107" s="T436">Dieser Mensch ging und ging, er ging und ging und kam in eine Stadt, in die des Zaren.</ta>
            <ta e="T453" id="Seg_11108" s="T444">Er kam an, ganz am Rand steht eine sehr schlechte, rauchende Erdhütte.</ta>
            <ta e="T456" id="Seg_11109" s="T453">Eine Erdhütte steht dort bloß.</ta>
            <ta e="T460" id="Seg_11110" s="T456">"Dorthin zu gehen ist gut, in die Erdhütte."</ta>
            <ta e="T465" id="Seg_11111" s="T460">Er lief in die Erdhütte; ein alter Mann und eine alte Frau.</ta>
            <ta e="T472" id="Seg_11112" s="T465">Die alte Frau hat keine Augen, der alte Mann ist ernst.</ta>
            <ta e="T478" id="Seg_11113" s="T472">Sie geben ihm zu essen, der Gast isst.</ta>
            <ta e="T484" id="Seg_11114" s="T478">"Woher kommst du", fragen sie nach Neuigkeiten.</ta>
            <ta e="T498" id="Seg_11115" s="T484">"Ich hatte ein Karauschennetz mit fünf Steinen, als ich die Fische daraus sammelte, wurde ich bestohlen, ich tötete die Unglücklichen und komme", sagte er.</ta>
            <ta e="T504" id="Seg_11116" s="T498">Ah, er tötete die Unglücklichen und kam.</ta>
            <ta e="T511" id="Seg_11117" s="T504">Er fragt den alten Mann: "Hat der Zar eine Tochter?"</ta>
            <ta e="T520" id="Seg_11118" s="T511">"Er hat eine einzige Tochter, die wird wohl irgendwohin verheiratet, sagt man", sagt er.</ta>
            <ta e="T529" id="Seg_11119" s="T520">"Alter Mann, willst du nicht als Brautwerber gehen", sagte jener, der Gast.</ta>
            <ta e="T533" id="Seg_11120" s="T529">"In jedem Fall versuchen wir zu fragen", sagte er.</ta>
            <ta e="T536" id="Seg_11121" s="T533">"'Es ist ein Gast gekommen.</ta>
            <ta e="T541" id="Seg_11122" s="T536">Er bittet um deine Tochter', sage [ihm]", sagte er.</ta>
            <ta e="T548" id="Seg_11123" s="T541">Dieser Mensch ging los, er rannte zum Zaren.</ta>
            <ta e="T551" id="Seg_11124" s="T548">"Na, warum kommst du?"</ta>
            <ta e="T559" id="Seg_11125" s="T551">"Nein, ich habe einen Gast, der kam von irgendwoher, ich weiß es nicht.</ta>
            <ta e="T565" id="Seg_11126" s="T559">Dieser Mensch bittet um deine Tochter", sagte er.</ta>
            <ta e="T576" id="Seg_11127" s="T565">"Nun dann, damit das Gesicht und die Augen dieses Menschen sehen kann, bringt ihn hierher", sagte der Zar.</ta>
            <ta e="T581" id="Seg_11128" s="T576">Der alte Mann brachte jenen Gast.</ta>
            <ta e="T592" id="Seg_11129" s="T581">"Pa, ich gebe [sie] nicht einem solchen Menschen, brecht seine Arme und Beine und werft ihn ins Gefängnis", sagte er.</ta>
            <ta e="T593" id="Seg_11130" s="T592">So.</ta>
            <ta e="T597" id="Seg_11131" s="T593">Sie brachen diesem Menschen nicht die Arme und Beine.</ta>
            <ta e="T605" id="Seg_11132" s="T597">Sie banden ihn mit einem Seil fest und warfen ihn ins Gefängnis, jenen Menschen.</ta>
            <ta e="T612" id="Seg_11133" s="T605">Dieser Mensch wurde eine Maus und rannte hinaus.</ta>
            <ta e="T617" id="Seg_11134" s="T612">Er rannte hinaus und ging wieder zu dem alten Mann.</ta>
            <ta e="T634" id="Seg_11135" s="T617">Ich weiß nicht, ob ein von dem eben unterschiedlicher Mensch kam oder so, wieder sagte er "'Er bittet um deine Tochter', geh und sag ihm das."</ta>
            <ta e="T637" id="Seg_11136" s="T634">Wieder ging der alte Mann.</ta>
            <ta e="T642" id="Seg_11137" s="T637">Er ging und sagte wieder: </ta>
            <ta e="T646" id="Seg_11138" s="T642">"Na, es ist wieder ein Gast gekommen.</ta>
            <ta e="T650" id="Seg_11139" s="T646">Er bittet um deine Tochter", sagte er.</ta>
            <ta e="T662" id="Seg_11140" s="T650">"Freunde, seht nach dem Menschen, den ihr vor Kurzem ins Gefängnis geworfen habt, was für einen Menschen wir da quälen", sagte er.</ta>
            <ta e="T673" id="Seg_11141" s="T662">Vom ins Gefängnis geworfenen Mensch ist nur eine ungerade Spur da, das festgebundene Seil liegt wie ein Seil.</ta>
            <ta e="T677" id="Seg_11142" s="T673">Nun gut, so so, nun.</ta>
            <ta e="T686" id="Seg_11143" s="T677">"Oh, nun habe ich wohl wirklich einen guten Menschen gemacht [=eingesperrt].</ta>
            <ta e="T690" id="Seg_11144" s="T686">Ich gebe meine Tochter, das sage ich."</ta>
            <ta e="T699" id="Seg_11145" s="T690">Dieser Mensch, alle Unglücklichen, die er getötet hatte, warf er dem Zaren auf die Knie.</ta>
            <ta e="T710" id="Seg_11146" s="T699">So singt hinter der Rentierkarawane ein Vögelchen, dieser Mensch trennte und brachte (…).</ta>
            <ta e="T720" id="Seg_11147" s="T710">Mit unterschiedlichen Soldaten und so geht dieser Mensch nach Hause, nachdem er die Tochter genommen hat.</ta>
            <ta e="T722" id="Seg_11148" s="T720">Nun, er nomadisiert.</ta>
            <ta e="T733" id="Seg_11149" s="T722">Nomadisierend kamen sie, und er lebte in Reichtum und Wohlstand, nun, das ist das Ende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_11150" s="T0">Чопочука жил, говорят. </ta>
            <ta e="T12" id="Seg_11151" s="T4">С пятью деревьями поставленными голомо дом имеет, с пятью камнями собо-сети имеет.</ta>
            <ta e="T16" id="Seg_11152" s="T12">Каждый день сети ставить заходит.</ta>
            <ta e="T22" id="Seg_11153" s="T16">Этот человек рыбу кучей сложил – из сети.</ta>
            <ta e="T31" id="Seg_11154" s="T22">Таким образом однажды пошёл – песцы, всякие съели вот его рыбу.</ta>
            <ta e="T35" id="Seg_11155" s="T31">"Этого преследовать, его путь проследить".</ta>
            <ta e="T44" id="Seg_11156" s="T35">Рыбу свою в дом свой перенеся, этот человек песца путь проследил.</ta>
            <ta e="T57" id="Seg_11157" s="T44">Песца путь когда прослеживал, ну, песец что и из одного куста вскочил.</ta>
            <ta e="T64" id="Seg_11158" s="T57">"Ой, Чопочука, человека (меня) напугал вот, откуда идeшь?"</ta>
            <ta e="T70" id="Seg_11159" s="T64"> "Ой, а рыбу мою почему украли вы?"</ta>
            <ta e="T76" id="Seg_11160" s="T70">"Хо, я украл разве?" – говоря сделал.</ta>
            <ta e="T85" id="Seg_11161" s="T76">Этот мышкой стал, превратившись в мышку, старик вот ходит.</ta>
            <ta e="T89" id="Seg_11162" s="T85">Того проглотил – песец его.</ta>
            <ta e="T101" id="Seg_11163" s="T89">Тот всё нутро вовсе .. надавив, изрезав вышедши .. выскочил.</ta>
            <ta e="T111" id="Seg_11164" s="T101">Таким образом того разднлывает и в карман свой положил как, прошагал.</ta>
            <ta e="T118" id="Seg_11165" s="T111">Опять вперёд шагая, зайца поставил.</ta>
            <ta e="T127" id="Seg_11166" s="T118">"Ой, Чопочука, что, откуда идёшь, человека (меня) напугал?" – сказал.</ta>
            <ta e="T135" id="Seg_11167" s="T127">"Ой, рыбу мою почему всю съели, рыбу прослеживаю (ищу)".</ta>
            <ta e="T138" id="Seg_11168" s="T135">Тот</ta>
            <ta e="T141" id="Seg_11169" s="T138">тоже вот …</ta>
            <ta e="T147" id="Seg_11170" s="T141">так, вот дерутся э с зайцем.</ta>
            <ta e="T162" id="Seg_11171" s="T147">При драке зайца разве в живых оставит этот человек – того убил да опять в карман свой положил, разделав.</ta>
            <ta e="T165" id="Seg_11172" s="T162">Опять пошагал.</ta>
            <ta e="T170" id="Seg_11173" s="T165">Когда шёл-шёл, лиса подскочила.</ta>
            <ta e="T180" id="Seg_11174" s="T170">"О, Чопочцка, человека напугал. Откуда, откуда идёшь вот? – сказал. </ta>
            <ta e="T186" id="Seg_11175" s="T180">"Ну, – сказал, – вот рыбу мою всю съели.</ta>
            <ta e="T190" id="Seg_11176" s="T186">Поэтому, проследив, пришёл".</ta>
            <ta e="T197" id="Seg_11177" s="T190">"Нет, к твоей рыбе мы не приблизились даже ещё ".</ta>
            <ta e="T205" id="Seg_11178" s="T197">"Зато я тебя разорвав съем" – сказал.</ta>
            <ta e="T209" id="Seg_11179" s="T205">Вот дерутся эти люди.</ta>
            <ta e="T212" id="Seg_11180" s="T209">Чопочука разве сжалится:</ta>
            <ta e="T217" id="Seg_11181" s="T212">в сторону надавил да убил.</ta>
            <ta e="T225" id="Seg_11182" s="T217">Того разделал да в карман (и карман-то у него большой, удивляюсь).</ta>
            <ta e="T229" id="Seg_11183" s="T225">Лису свою положил в карман. </ta>
            <ta e="T232" id="Seg_11184" s="T229">Потом зашагал.</ta>
            <ta e="T238" id="Seg_11185" s="T232">Когда шёл, соболь вскочил.</ta>
            <ta e="T244" id="Seg_11186" s="T238">"Ой, Чопочука, человека (меня) напугал ведь, – сказал. </ta>
            <ta e="T250" id="Seg_11187" s="T244">– Почему ходишь на этой земле? Ой".</ta>
            <ta e="T255" id="Seg_11188" s="T250">"Ну, рыбу мою почему всю съели?</ta>
            <ta e="T261" id="Seg_11189" s="T255">То прослеживаю (ищу), – сказал, – ещё съели".</ta>
            <ta e="T267" id="Seg_11190" s="T261">Как схватит да того вот дерётся.</ta>
            <ta e="T272" id="Seg_11191" s="T267">Дерясь Чопочука разве оставит его, не оставит человеком (живым).</ta>
            <ta e="T280" id="Seg_11192" s="T272">Убил да, опять разделал и в карман положил.</ta>
            <ta e="T283" id="Seg_11193" s="T280">Опять зашагал. </ta>
            <ta e="T286" id="Seg_11194" s="T283">Шагая волка встретил.</ta>
            <ta e="T289" id="Seg_11195" s="T286">Волк вскочил. </ta>
            <ta e="T297" id="Seg_11196" s="T289">"О, Чопочука, человека (меня) напугал, о где идёшь?" – сказал.</ta>
            <ta e="T301" id="Seg_11197" s="T297">"Ну, рыбу мою почему съели?</ta>
            <ta e="T303" id="Seg_11198" s="T301">Ту ищу".</ta>
            <ta e="T314" id="Seg_11199" s="T303">"Не. К твоей рыбе не приближались даже".</ta>
            <ta e="T320" id="Seg_11200" s="T314">Того в рот взял, проглотил – Чопочуку.</ta>
            <ta e="T327" id="Seg_11201" s="T320">Чопочука нутро как надават, выскочил.</ta>
            <ta e="T335" id="Seg_11202" s="T327">Так сделав, выдернул, в карман положил.</ta>
            <ta e="T341" id="Seg_11203" s="T335">Потом вот снова зашагал, зашагал.</ta>
            <ta e="T348" id="Seg_11204" s="T341">О, медведь как вскочил вот, только.</ta>
            <ta e="T356" id="Seg_11205" s="T348">"О, Чопочука, почему, в какой земле ходишь (где) это? "– сказал.</ta>
            <ta e="T359" id="Seg_11206" s="T356">"Рыбу мою почему украли?</ta>
            <ta e="T364" id="Seg_11207" s="T359">Ещё рыбу мою украли съе.. </ta>
            <ta e="T369" id="Seg_11208" s="T364">я тебя проглотив съем", – сказал.</ta>
            <ta e="T371" id="Seg_11209" s="T369">Съел. </ta>
            <ta e="T379" id="Seg_11210" s="T371">Нутро как надавит и выскочил этот человек (он).</ta>
            <ta e="T387" id="Seg_11211" s="T379">Таким образом ножом разделав, вот убил нарочно.</ta>
            <ta e="T390" id="Seg_11212" s="T387">Вот сидя подумал:</ta>
            <ta e="T395" id="Seg_11213" s="T390">"Вот царя дочь попрсить идти надо.</ta>
            <ta e="T403" id="Seg_11214" s="T395">Царь единственную дочь имеет, сказали, того искать идти надо.</ta>
            <ta e="T408" id="Seg_11215" s="T403">Столько всего имеющий четовек-то", – сказал.</ta>
            <ta e="T416" id="Seg_11216" s="T408">Вот к царю чтобы пойти вот сидит этот человек твой, сидит.</ta>
            <ta e="T419" id="Seg_11217" s="T416">"Ну, пойти хорошо".</ta>
            <ta e="T423" id="Seg_11218" s="T419">Вот зашагал этот человек.</ta>
            <ta e="T427" id="Seg_11219" s="T423">Шагая на одной земле зажил.</ta>
            <ta e="T434" id="Seg_11220" s="T427">Сидя осмотрелся, царя .. крыльцо виднеется вот.</ta>
            <ta e="T436" id="Seg_11221" s="T434">"Дойду, наверное, до туда".</ta>
            <ta e="T444" id="Seg_11222" s="T436">Этот человек зашагав, зашагав, в город пришёл,к этому царю относящемуся.</ta>
            <ta e="T453" id="Seg_11223" s="T444">Пришел, в самой сторонке очень плохой, с дымом голомо (дом) стоит.</ta>
            <ta e="T456" id="Seg_11224" s="T453">Голомо стоит только.</ta>
            <ta e="T460" id="Seg_11225" s="T456">"Сюда зайти хорошо, в голомло</ta>
            <ta e="T465" id="Seg_11226" s="T460">В голомо заскочил – со стариком старуха.</ta>
            <ta e="T472" id="Seg_11227" s="T465">Старуха его без глаза, старик так себе.</ta>
            <ta e="T478" id="Seg_11228" s="T472">Вот кормят, это есть этот гость.</ta>
            <ta e="T484" id="Seg_11229" s="T478">"Из какой-какой земли пришёл?" – говоря, весть спрашивает.</ta>
            <ta e="T498" id="Seg_11230" s="T484">"Я пять с камнями собо-сети имел, оттуда рыбу сложив, дав украсть, горьких (вороваты?) убив пришёл", – сказал.</ta>
            <ta e="T504" id="Seg_11231" s="T498">Ок, н горьких убив пришёл.</ta>
            <ta e="T511" id="Seg_11232" s="T504">У этого старика спрашивает, царь имеет ли дочь, говоря.</ta>
            <ta e="T520" id="Seg_11233" s="T511">Единственную дочь имеет, где-то замуж выдают вроде, говоря.</ta>
            <ta e="T529" id="Seg_11234" s="T520">"Старик, ты сватом не пойдёшь ли? – сказал тот , его гость.</ta>
            <ta e="T533" id="Seg_11235" s="T529">"На всякий случай спросить попробуем", – сказал.</ta>
            <ta e="T536" id="Seg_11236" s="T533">Один гость пришёл.</ta>
            <ta e="T541" id="Seg_11237" s="T536">Дочь твою просит, говоря, скажи, сказал.</ta>
            <ta e="T548" id="Seg_11238" s="T541">Этот человек ушёл. Царь прискокал.</ta>
            <ta e="T551" id="Seg_11239" s="T548">"Вот, почуму пришёл?"</ta>
            <ta e="T559" id="Seg_11240" s="T551">"Нет. Я гостя имею. Из какой-какой земли пришёл, не знаю.</ta>
            <ta e="T565" id="Seg_11241" s="T559">Тот человек дочь твою просит", – сказал.</ta>
            <ta e="T576" id="Seg_11242" s="T565">"Таким образом того человека лицо-глаз чтоб посмотреть, сюда приведите", – сказал царь его.</ta>
            <ta e="T581" id="Seg_11243" s="T576">Этот старик того гостя повёл.</ta>
            <ta e="T592" id="Seg_11244" s="T581">"Не, я такому человеку не отдаю. Руки-ноги переломав, в тюрму бросьте", - сказал.</ta>
            <ta e="T593" id="Seg_11245" s="T592">Вот. </ta>
            <ta e="T597" id="Seg_11246" s="T593">Этого человека руки-ноги не переломали.</ta>
            <ta e="T605" id="Seg_11247" s="T597">Верёвкой перевязав, в тюрму засунули, того человека. </ta>
            <ta e="T612" id="Seg_11248" s="T605">… Этот человек, став мышкой, выскочил.</ta>
            <ta e="T617" id="Seg_11249" s="T612">Выскочив, к старику опять зашёл.</ta>
            <ta e="T634" id="Seg_11250" s="T617">Вот не знаю, вот чем тот другой человек пришёл ли, что ли, снова – "Дочь твою просит, говоря, иди-ка", – сказал.</ta>
            <ta e="T637" id="Seg_11251" s="T634">Опять пошёл старик.</ta>
            <ta e="T642" id="Seg_11252" s="T637">Вот уйдя опять сказал: </ta>
            <ta e="T646" id="Seg_11253" s="T642">"Ну, опять гость пришёл.</ta>
            <ta e="T650" id="Seg_11254" s="T646">Дочь твою просит", – сказал.</ta>
            <ta e="T662" id="Seg_11255" s="T650">"Друзья, тогда в тюрьму брошенного вами человека вашего посмотрите вот, какого человека мучаем что ли", – сказал.</ta>
            <ta e="T673" id="Seg_11256" s="T662">В тюрму заброшенного человека след только еле виднеется: перевезявшая верёвка верёвкой лежит.</ta>
            <ta e="T677" id="Seg_11257" s="T673">Ну вот, так-так, вот.</ta>
            <ta e="T686" id="Seg_11258" s="T677">"О, вот точно да хорошего человека сделал (наказал) э, вроде.</ta>
            <ta e="T690" id="Seg_11259" s="T686">Дочь свою отдаю, скажу ведь".</ta>
            <ta e="T699" id="Seg_11260" s="T690">Этот человек стольких горьких что убил, царю на колени высыпал.</ta>
            <ta e="T710" id="Seg_11261" s="T699">Таким оьразом за аргишом его птица поёт, с гулом разделив увёл этот человек.</ta>
            <ta e="T720" id="Seg_11262" s="T710">С разными солдатами-прочими этот человек как пошёл домой, девушку забрав.</ta>
            <ta e="T722" id="Seg_11263" s="T720">Вот аргишит. </ta>
            <ta e="T733" id="Seg_11264" s="T722">Аргишив, это приехав, этот вот так в достатке зажил. Вот это всё.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T212" id="Seg_11265" s="T209">[DCh]: Not clear why interrogative stem "kan-" here instead of demonstrative stem "on-" or "man-"</ta>
            <ta e="T548" id="Seg_11266" s="T541">[DCh]: Not clear, why no DAT/LOC at "ɨraːktaːgɨ".</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
