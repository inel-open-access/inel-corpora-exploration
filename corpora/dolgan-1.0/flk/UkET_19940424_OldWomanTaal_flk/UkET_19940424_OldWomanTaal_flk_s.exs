<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID97D85F58-6B0A-E9FE-A4DB-8A2F2E72CF78">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>UkET_1994_OldWomanTaal_flk</transcription-name>
         <referenced-file url="UkET_19940424_OldWomanTaal_flk.wav" />
         <referenced-file url="UkET_19940424_OldWomanTaal_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\UkET_19940424_OldWomanTaal_flk\UkET_19940424_OldWomanTaal_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">636</ud-information>
            <ud-information attribute-name="# HIAT:w">400</ud-information>
            <ud-information attribute-name="# e">401</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">56</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="UkET">
            <abbreviation>UkET</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" />
         <tli id="T1" time="0.49950000000000233" type="intp" />
         <tli id="T2" time="0.9990000000000048" type="intp" />
         <tli id="T3" time="1.498500000000007" type="intp" />
         <tli id="T4" time="1.9980000000000093" type="intp" />
         <tli id="T5" time="2.4975000000000116" type="intp" />
         <tli id="T6" time="2.997000000000014" type="intp" />
         <tli id="T7" time="3.4965000000000166" type="intp" />
         <tli id="T8" time="3.996000000000019" type="intp" />
         <tli id="T9" time="4.495500000000021" type="intp" />
         <tli id="T10" time="4.995000000000023" type="intp" />
         <tli id="T11" time="5.494500000000025" type="intp" />
         <tli id="T12" time="5.994000000000028" type="appl" />
         <tli id="T13" time="6.581000000000017" type="appl" />
         <tli id="T14" time="7.168000000000006" type="appl" />
         <tli id="T15" time="7.7549999999999955" type="appl" />
         <tli id="T16" time="8.341999999999985" type="appl" />
         <tli id="T17" time="8.930000000000064" type="appl" />
         <tli id="T18" time="9.517000000000053" type="appl" />
         <tli id="T19" time="10.104000000000042" type="appl" />
         <tli id="T20" time="10.691000000000031" type="appl" />
         <tli id="T21" time="11.27800000000002" type="appl" />
         <tli id="T22" time="11.865000000000009" type="appl" />
         <tli id="T23" time="12.3386669921875" />
         <tli id="T24" time="12.701667073567709" type="intp" />
         <tli id="T25" time="13.064667154947918" type="intp" />
         <tli id="T26" time="13.427667236328126" type="intp" />
         <tli id="T27" time="13.790667317708333" />
         <tli id="T28" time="14.317000000000007" type="appl" />
         <tli id="T29" time="14.652000000000044" type="appl" />
         <tli id="T30" time="14.988000000000056" type="appl" />
         <tli id="T31" time="15.32400000000007" type="appl" />
         <tli id="T32" time="15.659999999999968" type="appl" />
         <tli id="T33" time="15.995999999999981" type="appl" />
         <tli id="T34" time="16.331000000000017" type="appl" />
         <tli id="T35" time="16.487000325520835" />
         <tli id="T36" time="17.202999999999975" type="appl" />
         <tli id="T37" time="17.74000000000001" type="appl" />
         <tli id="T38" time="18.276000000000067" type="appl" />
         <tli id="T39" time="18.812000000000012" type="appl" />
         <tli id="T40" time="19.28200000000004" type="appl" />
         <tli id="T41" time="19.750999999999976" type="appl" />
         <tli id="T42" time="20.220000000000027" type="appl" />
         <tli id="T43" time="20.690000000000055" type="appl" />
         <tli id="T44" time="21.159999999999968" type="appl" />
         <tli id="T45" time="21.62900000000002" type="appl" />
         <tli id="T46" time="22.09800000000007" type="appl" />
         <tli id="T47" time="22.567999999999984" type="appl" />
         <tli id="T48" time="23.03800000000001" type="appl" />
         <tli id="T49" time="23.507000000000062" type="appl" />
         <tli id="T50" time="23.966999999999985" type="appl" />
         <tli id="T51" time="24.427999999999997" type="appl" />
         <tli id="T52" time="24.888000000000034" type="appl" />
         <tli id="T53" time="25.34800000000007" type="appl" />
         <tli id="T54" time="25.807999999999993" type="appl" />
         <tli id="T55" time="26.26800000000003" type="appl" />
         <tli id="T56" time="26.729000000000042" type="appl" />
         <tli id="T57" time="27.189000000000078" type="appl" />
         <tli id="T58" time="27.649" type="appl" />
         <tli id="T59" time="28.110000000000014" type="appl" />
         <tli id="T60" time="28.57000000000005" type="appl" />
         <tli id="T61" time="28.976666666666667" />
         <tli id="T62" time="29.401000000000067" type="appl" />
         <tli id="T63" time="29.771000000000072" type="appl" />
         <tli id="T64" time="30.142000000000053" type="appl" />
         <tli id="T65" time="30.513000000000034" type="appl" />
         <tli id="T66" time="30.883000000000038" type="appl" />
         <tli id="T67" time="31.25400000000002" type="appl" />
         <tli id="T68" time="31.624000000000024" type="appl" />
         <tli id="T69" time="31.995000000000005" type="appl" />
         <tli id="T70" time="32.365999999999985" type="appl" />
         <tli id="T71" time="32.73599999999999" type="appl" />
         <tli id="T72" time="33.36033203125" />
         <tli id="T73" time="33.51700000000005" type="appl" />
         <tli id="T74" time="33.928" type="appl" />
         <tli id="T75" time="34.33800000000008" type="appl" />
         <tli id="T76" time="34.74800000000005" type="appl" />
         <tli id="T77" time="35.158000000000015" type="appl" />
         <tli id="T78" time="35.569000000000074" type="appl" />
         <tli id="T79" time="35.97900000000004" type="appl" />
         <tli id="T80" time="36.38900000000001" type="appl" />
         <tli id="T81" time="36.80000000000007" type="appl" />
         <tli id="T82" time="37.11" />
         <tli id="T83" time="37.66300000000001" type="appl" />
         <tli id="T84" time="38.115999999999985" type="appl" />
         <tli id="T85" time="38.567999999999984" type="appl" />
         <tli id="T86" time="39.02100000000007" type="appl" />
         <tli id="T87" time="39.474000000000046" type="appl" />
         <tli id="T88" time="39.926000000000045" type="appl" />
         <tli id="T89" time="40.37900000000002" type="appl" />
         <tli id="T90" time="40.831999999999994" type="appl" />
         <tli id="T91" time="41.28499999999997" type="appl" />
         <tli id="T92" time="41.738000000000056" type="appl" />
         <tli id="T403" time="41.964000000000055" type="intp" />
         <tli id="T93" time="42.190000000000055" type="appl" />
         <tli id="T94" time="42.64300000000003" type="appl" />
         <tli id="T95" time="43.096000000000004" type="appl" />
         <tli id="T96" time="43.54899999999998" type="appl" />
         <tli id="T98" time="44.454000000000065" type="appl" />
         <tli id="T99" time="44.91899999999998" type="appl" />
         <tli id="T100" time="45.38499999999999" type="appl" />
         <tli id="T101" time="45.85000000000002" type="appl" />
         <tli id="T102" time="46.31600000000003" type="appl" />
         <tli id="T103" time="46.78100000000006" type="appl" />
         <tli id="T104" time="47.24700000000007" type="appl" />
         <tli id="T105" time="47.71199999999999" type="appl" />
         <tli id="T106" time="48.17700000000002" type="appl" />
         <tli id="T107" time="48.64300000000003" type="appl" />
         <tli id="T108" time="49.10800000000006" type="appl" />
         <tli id="T109" time="49.57400000000007" type="appl" />
         <tli id="T110" time="50.03899999999999" type="appl" />
         <tli id="T111" time="50.471000000000004" type="appl" />
         <tli id="T112" time="50.90300000000002" type="appl" />
         <tli id="T113" time="51.33600000000001" type="appl" />
         <tli id="T114" time="51.76800000000003" type="appl" />
         <tli id="T115" time="52.093333333333334" />
         <tli id="T116" time="52.53800000000001" type="appl" />
         <tli id="T117" time="52.875999999999976" type="appl" />
         <tli id="T118" time="53.21300000000008" type="appl" />
         <tli id="T119" time="53.551000000000045" type="appl" />
         <tli id="T120" time="53.88900000000001" type="appl" />
         <tli id="T121" time="54.226999999999975" type="appl" />
         <tli id="T122" time="54.56400000000008" type="appl" />
         <tli id="T123" time="54.902000000000044" type="appl" />
         <tli id="T124" time="55.09333333333333" />
         <tli id="T125" time="55.672000000000025" type="appl" />
         <tli id="T126" time="56.10500000000002" type="appl" />
         <tli id="T127" time="56.537000000000035" type="appl" />
         <tli id="T128" time="56.96900000000005" type="appl" />
         <tli id="T129" time="57.402000000000044" type="appl" />
         <tli id="T130" time="57.83400000000006" type="appl" />
         <tli id="T131" time="58.266000000000076" type="appl" />
         <tli id="T132" time="58.69799999999998" type="appl" />
         <tli id="T133" time="59.13099999999997" type="appl" />
         <tli id="T134" time="59.82000000000001" />
         <tli id="T135" time="60.02600000000007" type="appl" />
         <tli id="T136" time="60.49000000000001" type="appl" />
         <tli id="T137" time="60.952999999999975" type="appl" />
         <tli id="T138" time="61.41700000000003" type="appl" />
         <tli id="T139" time="62.24" />
         <tli id="T140" time="62.67966634114583" type="intp" />
         <tli id="T141" time="63.11933268229166" type="intp" />
         <tli id="T142" time="63.558999023437494" type="intp" />
         <tli id="T143" time="63.99866536458333" />
         <tli id="T144" time="64.42000000000007" type="appl" />
         <tli id="T145" time="64.82100000000003" type="appl" />
         <tli id="T146" time="65.221" type="appl" />
         <tli id="T147" time="65.62200000000007" type="appl" />
         <tli id="T148" time="66.02200000000005" type="appl" />
         <tli id="T149" time="66.423" type="appl" />
         <tli id="T150" time="66.82299999999998" type="appl" />
         <tli id="T151" time="67.22400000000005" type="appl" />
         <tli id="T152" time="67.62400000000002" type="appl" />
         <tli id="T153" time="68.35166666666667" />
         <tli id="T154" time="68.72666666666667" type="intp" />
         <tli id="T155" time="69.10166666666667" type="intp" />
         <tli id="T156" time="69.47666666666667" type="intp" />
         <tli id="T157" time="69.85166666666667" type="intp" />
         <tli id="T158" time="70.22666666666667" type="intp" />
         <tli id="T159" time="70.60166666666667" type="intp" />
         <tli id="T160" time="70.97666666666667" type="intp" />
         <tli id="T161" time="71.35166666666667" type="intp" />
         <tli id="T162" time="71.72666666666667" type="intp" />
         <tli id="T163" time="72.10166666666667" type="intp" />
         <tli id="T164" time="72.47666666666667" type="intp" />
         <tli id="T165" time="72.85166666666667" type="intp" />
         <tli id="T166" time="73.22666666666667" />
         <tli id="T167" time="73.96666666666667" />
         <tli id="T168" time="74.39333333333333" />
         <tli id="T169" time="74.53333333333333" />
         <tli id="T170" time="75.02" />
         <tli id="T171" time="75.28666666666668" />
         <tli id="T172" time="75.35333333333332" />
         <tli id="T173" time="75.94666666666667" />
         <tli id="T174" time="78.85625000000003" type="intp" />
         <tli id="T175" time="79.13433333333336" type="intp" />
         <tli id="T176" time="79.41241666666669" type="intp" />
         <tli id="T177" time="79.69050000000001" type="intp" />
         <tli id="T178" time="79.96858333333334" type="intp" />
         <tli id="T179" time="80.24666666666667" />
         <tli id="T180" time="81.11333333333333" />
         <tli id="T181" time="81.32299999999998" type="appl" />
         <tli id="T182" time="82.45600000000002" type="appl" />
         <tli id="T183" time="82.96" />
         <tli id="T184" time="83.18" />
         <tli id="T185" time="83.41333333333334" />
         <tli id="T186" time="83.69333333333333" />
         <tli id="T187" time="84.53333333333335" />
         <tli id="T188" time="85.22" />
         <tli id="T189" time="85.58666666666667" />
         <tli id="T190" time="85.92666666666666" />
         <tli id="T191" time="86.26" />
         <tli id="T192" time="86.6" />
         <tli id="T193" time="87.05333333333333" />
         <tli id="T194" time="87.92666666666668" />
         <tli id="T195" time="90.22059999999999" type="intp" />
         <tli id="T196" time="90.48554999999999" type="intp" />
         <tli id="T197" time="90.75049999999999" type="intp" />
         <tli id="T198" time="91.01544999999999" type="intp" />
         <tli id="T199" time="91.28039999999999" type="intp" />
         <tli id="T200" time="91.54534999999998" type="intp" />
         <tli id="T201" time="91.81029999999998" type="intp" />
         <tli id="T202" time="92.07524999999998" type="intp" />
         <tli id="T203" time="92.34019999999998" type="intp" />
         <tli id="T204" time="92.60514999999998" type="intp" />
         <tli id="T205" time="92.87009999999998" type="intp" />
         <tli id="T206" time="92.95333333333333" />
         <tli id="T207" time="93.36" />
         <tli id="T208" time="93.83000000000004" type="appl" />
         <tli id="T209" time="94.25999999999999" type="appl" />
         <tli id="T210" time="94.69000000000005" type="appl" />
         <tli id="T211" time="95.12" type="appl" />
         <tli id="T212" time="95.55000000000007" type="appl" />
         <tli id="T213" time="95.98000000000002" type="appl" />
         <tli id="T214" time="96.40999999999997" type="appl" />
         <tli id="T215" time="96.84000000000003" type="appl" />
         <tli id="T216" time="97.26999999999998" type="appl" />
         <tli id="T217" time="97.70000000000005" type="appl" />
         <tli id="T218" time="98.12666666666671" type="intp" />
         <tli id="T219" time="99.0" />
         <tli id="T220" time="99.63333333333334" />
         <tli id="T221" time="100.25999999999999" type="appl" />
         <tli id="T222" time="100.84000000000003" type="appl" />
         <tli id="T223" time="101.42000000000007" type="appl" />
         <tli id="T224" time="102.00099999999998" type="appl" />
         <tli id="T225" time="102.96666666666667" />
         <tli id="T226" time="103.06000000000006" type="appl" />
         <tli id="T227" time="103.54000000000008" type="appl" />
         <tli id="T228" time="104.019" type="appl" />
         <tli id="T229" time="104.49800000000005" type="appl" />
         <tli id="T230" time="104.97800000000007" type="appl" />
         <tli id="T231" time="105.457" type="appl" />
         <tli id="T232" time="105.93600000000004" type="appl" />
         <tli id="T233" time="106.41500000000008" type="appl" />
         <tli id="T234" time="106.89499999999998" type="appl" />
         <tli id="T235" time="107.68666666666667" />
         <tli id="T236" time="107.80363636363639" type="intp" />
         <tli id="T237" time="108.23327272727275" type="intp" />
         <tli id="T238" time="108.66290909090911" type="intp" />
         <tli id="T239" time="109.09254545454547" type="intp" />
         <tli id="T240" time="109.52218181818183" type="intp" />
         <tli id="T241" time="109.9518181818182" type="intp" />
         <tli id="T242" time="110.38145454545456" type="intp" />
         <tli id="T243" time="110.81109090909092" type="intp" />
         <tli id="T244" time="111.24072727272728" type="intp" />
         <tli id="T245" time="111.34" />
         <tli id="T246" time="112.25333333333333" />
         <tli id="T247" time="112.57900000000006" type="appl" />
         <tli id="T248" time="113.05899999999997" type="appl" />
         <tli id="T249" time="113.53800000000001" type="appl" />
         <tli id="T250" time="114.01700000000005" type="appl" />
         <tli id="T251" time="114.49700000000007" type="appl" />
         <tli id="T252" time="114.976" type="appl" />
         <tli id="T253" time="115.45500000000004" type="appl" />
         <tli id="T254" time="115.93399999999997" type="appl" />
         <tli id="T255" time="116.41399999999999" type="appl" />
         <tli id="T256" time="117.01299479166666" />
         <tli id="T257" time="117.61199999999997" type="appl" />
         <tli id="T258" time="118.33100000000002" type="appl" />
         <tli id="T259" time="119.17666666666668" />
         <tli id="T260" time="119.48300000000006" type="appl" />
         <tli id="T261" time="119.91500000000008" type="appl" />
         <tli id="T262" time="120.34800000000007" type="appl" />
         <tli id="T263" time="120.78100000000006" type="appl" />
         <tli id="T264" time="121.21400000000006" type="appl" />
         <tli id="T265" time="121.64600000000007" type="appl" />
         <tli id="T266" time="122.07900000000006" type="appl" />
         <tli id="T267" time="122.51200000000006" type="appl" />
         <tli id="T268" time="122.94400000000007" type="appl" />
         <tli id="T269" time="123.37700000000007" type="appl" />
         <tli id="T270" time="123.82187500000006" type="intp" />
         <tli id="T271" time="124.26675000000006" type="intp" />
         <tli id="T272" time="124.71162500000005" type="intp" />
         <tli id="T273" time="124.89649739583332" />
         <tli id="T274" time="125.04000000000002" />
         <tli id="T275" time="125.22" />
         <tli id="T276" time="125.89333333333335" />
         <tli id="T277" time="126.93600000000004" type="appl" />
         <tli id="T278" time="127.37400000000002" type="appl" />
         <tli id="T279" time="127.81200000000001" type="appl" />
         <tli id="T280" time="128.25099999999998" type="appl" />
         <tli id="T281" time="128.68900000000008" type="appl" />
         <tli id="T282" time="129.12700000000007" type="appl" />
         <tli id="T283" time="129.56500000000005" type="appl" />
         <tli id="T284" time="130.00400000000002" type="appl" />
         <tli id="T285" time="130.442" type="appl" />
         <tli id="T286" time="130.68666666666667" />
         <tli id="T287" time="131.46000000000004" type="appl" />
         <tli id="T288" time="132.04000000000008" type="appl" />
         <tli id="T289" time="132.62" type="appl" />
         <tli id="T290" time="133.111" type="appl" />
         <tli id="T291" time="133.60199999999998" type="appl" />
         <tli id="T292" time="134.09400000000005" type="appl" />
         <tli id="T293" time="134.95166666666665" />
         <tli id="T294" time="135.106" type="appl" />
         <tli id="T295" time="135.62700000000007" type="appl" />
         <tli id="T296" time="136.14800000000002" type="appl" />
         <tli id="T297" time="136.668" type="appl" />
         <tli id="T298" time="137.18900000000008" type="appl" />
         <tli id="T299" time="137.77666666666667" />
         <tli id="T300" time="138.15950000000004" type="intp" />
         <tli id="T301" time="138.60900000000004" type="intp" />
         <tli id="T302" time="139.05850000000004" type="intp" />
         <tli id="T303" time="139.50800000000004" type="intp" />
         <tli id="T304" time="139.95750000000004" type="intp" />
         <tli id="T305" time="140.40700000000004" type="intp" />
         <tli id="T306" time="140.85650000000004" type="intp" />
         <tli id="T307" time="140.94666666666666" />
         <tli id="T308" time="141.15333333333334" />
         <tli id="T309" time="142.20500000000004" type="appl" />
         <tli id="T310" time="142.60290000000003" type="intp" />
         <tli id="T311" time="143.00080000000003" type="intp" />
         <tli id="T312" time="143.39870000000002" type="intp" />
         <tli id="T313" time="143.7966" type="intp" />
         <tli id="T314" time="144.1945" type="intp" />
         <tli id="T315" time="144.5924" type="intp" />
         <tli id="T316" time="144.9903" type="intp" />
         <tli id="T317" time="145.38819999999998" type="intp" />
         <tli id="T318" time="145.82609375" />
         <tli id="T319" time="146.03733072916665" />
         <tli id="T320" time="146.54600000000005" type="appl" />
         <tli id="T321" time="146.909" type="appl" />
         <tli id="T322" time="147.27100000000007" type="appl" />
         <tli id="T323" time="147.63300000000004" type="appl" />
         <tli id="T324" time="147.995" type="appl" />
         <tli id="T325" time="148.35800000000006" type="appl" />
         <tli id="T326" time="148.8" />
         <tli id="T327" time="149.27800000000002" type="appl" />
         <tli id="T328" time="149.836" type="appl" />
         <tli id="T329" time="150.394" type="appl" />
         <tli id="T330" time="150.952" type="appl" />
         <tli id="T331" time="151.51" type="appl" />
         <tli id="T332" time="151.98000000000002" type="appl" />
         <tli id="T333" time="152.45000000000005" type="appl" />
         <tli id="T334" time="152.92000000000007" type="appl" />
         <tli id="T335" time="153.39100000000008" type="appl" />
         <tli id="T336" time="153.861" type="appl" />
         <tli id="T337" time="154.33100000000002" type="appl" />
         <tli id="T338" time="155.00766927083333" />
         <tli id="T339" time="155.25775000000004" type="intp" />
         <tli id="T340" time="155.71450000000004" type="intp" />
         <tli id="T341" time="156.17125000000004" type="intp" />
         <tli id="T342" time="156.62800000000004" type="intp" />
         <tli id="T343" time="157.08475000000004" type="intp" />
         <tli id="T344" time="157.54150000000004" type="intp" />
         <tli id="T345" time="157.8" />
         <tli id="T346" time="158.45500000000004" type="appl" />
         <tli id="T347" time="158.885" type="appl" />
         <tli id="T348" time="159.31500000000005" type="appl" />
         <tli id="T349" time="159.745" type="appl" />
         <tli id="T350" time="160.17500000000007" type="appl" />
         <tli id="T351" time="160.606" type="appl" />
         <tli id="T352" time="161.03600000000006" type="appl" />
         <tli id="T353" time="161.466" type="appl" />
         <tli id="T354" time="161.89600000000007" type="appl" />
         <tli id="T355" time="162.32600000000002" type="appl" />
         <tli id="T356" time="163.13333333333335" />
         <tli id="T357" time="163.32900000000006" type="appl" />
         <tli id="T358" time="164.33299999999997" type="appl" />
         <tli id="T359" time="164.91899999999998" type="appl" />
         <tli id="T360" time="165.50400000000002" type="appl" />
         <tli id="T361" time="165.94" />
         <tli id="T362" time="166.46000000000004" type="appl" />
         <tli id="T363" time="166.83100000000002" type="appl" />
         <tli id="T364" time="167.20100000000002" type="appl" />
         <tli id="T365" time="167.57100000000003" type="appl" />
         <tli id="T366" time="167.942" type="appl" />
         <tli id="T367" time="168.312" type="appl" />
         <tli id="T368" time="168.68200000000002" type="appl" />
         <tli id="T369" time="169.05200000000002" type="appl" />
         <tli id="T370" time="169.423" type="appl" />
         <tli id="T371" time="169.91299479166665" />
         <tli id="T372" time="170.173875" type="intp" />
         <tli id="T373" time="170.55475" type="intp" />
         <tli id="T374" time="170.93562500000002" type="intp" />
         <tli id="T375" time="171.31650000000002" type="intp" />
         <tli id="T376" time="171.69737500000002" type="intp" />
         <tli id="T377" time="172.07825000000003" type="intp" />
         <tli id="T378" time="172.3257942708333" />
         <tli id="T379" time="172.6866666666667" />
         <tli id="T380" time="173.19400000000007" type="appl" />
         <tli id="T381" time="173.54899999999998" type="appl" />
         <tli id="T382" time="173.90300000000002" type="appl" />
         <tli id="T383" time="174.25800000000004" type="appl" />
         <tli id="T384" time="174.61199999999997" type="appl" />
         <tli id="T385" time="174.96699999999998" type="appl" />
         <tli id="T386" time="175.4743359375" />
         <tli id="T387" time="176.43333333333334" />
         <tli id="T388" time="176.478" type="intp" />
         <tli id="T389" time="177.1431640625" />
         <tli id="T390" time="177.64166666666665" />
         <tli id="T391" time="178.02300000000002" type="appl" />
         <tli id="T392" time="178.41200000000003" type="appl" />
         <tli id="T393" time="178.80000000000007" type="appl" />
         <tli id="T394" time="179.188" type="appl" />
         <tli id="T395" time="179.577" type="appl" />
         <tli id="T396" time="179.96500000000003" type="appl" />
         <tli id="T397" time="180.35300000000007" type="appl" />
         <tli id="T398" time="180.74200000000008" type="appl" />
         <tli id="T399" time="180.95" />
         <tli id="T400" time="181.346" type="appl" />
         <tli id="T401" time="181.56100000000004" type="appl" />
         <tli id="T402" time="181.77700000000004" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="UkET"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T402" id="Seg_0" n="sc" s="T0">
               <ts e="T11" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Taːl</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">emeːksin</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">olorbut</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">ühü</ts>
                  <nts id="Seg_15" n="HIAT:ip">,</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_18" n="HIAT:w" s="T4">ojulaːk</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_21" n="HIAT:w" s="T5">ottoːk</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_24" n="HIAT:w" s="T6">orto</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">togus</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">dojduga</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_32" n="HIAT:ip">–</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">Taːl</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">emeːksin</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_42" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">Bu</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">Taːl</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">emeːksin</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">bu͡ollagɨna</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">biːrde</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">ki͡ehennen</ts>
                  <nts id="Seg_60" n="HIAT:ip">,</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">kaja</ts>
                  <nts id="Seg_64" n="HIAT:ip">,</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">čaːj</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">ihinen-ihinen</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">baraːn</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_76" n="HIAT:w" s="T21">utujan</ts>
                  <nts id="Seg_77" n="HIAT:ip">…</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_80" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">Utujbut</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_85" n="HIAT:w" s="T24">duː</ts>
                  <nts id="Seg_86" n="HIAT:ip">,</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_89" n="HIAT:w" s="T25">kajdak</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_92" n="HIAT:w" s="T26">duː</ts>
                  <nts id="Seg_93" n="HIAT:ip">?</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_96" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">Ulakaːta</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">testibite</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">duː</ts>
                  <nts id="Seg_105" n="HIAT:ip">,</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_108" n="HIAT:w" s="T30">hamɨːr</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_111" n="HIAT:w" s="T31">tüspüt</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_114" n="HIAT:w" s="T32">duː</ts>
                  <nts id="Seg_115" n="HIAT:ip">,</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_118" n="HIAT:w" s="T33">tu͡ok</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_121" n="HIAT:w" s="T34">duː</ts>
                  <nts id="Seg_122" n="HIAT:ip">?</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_125" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_127" n="HIAT:w" s="T35">Tellege</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_130" n="HIAT:w" s="T36">ilijbit</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_133" n="HIAT:w" s="T37">emi͡eksin</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_136" n="HIAT:w" s="T38">gi͡ene</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_140" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_142" n="HIAT:w" s="T39">Bu</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_145" n="HIAT:w" s="T40">tellege</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_148" n="HIAT:w" s="T41">ilijen</ts>
                  <nts id="Seg_149" n="HIAT:ip">,</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_152" n="HIAT:w" s="T42">emeːksin</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_155" n="HIAT:w" s="T43">harsi͡erda</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_158" n="HIAT:w" s="T44">kühün</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_161" n="HIAT:w" s="T45">ebit</ts>
                  <nts id="Seg_162" n="HIAT:ip">,</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_165" n="HIAT:w" s="T46">buːs</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_168" n="HIAT:w" s="T47">toŋorun</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_171" n="HIAT:w" s="T48">hagɨna</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_175" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_177" n="HIAT:w" s="T49">Emeːksin</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_180" n="HIAT:w" s="T50">turan</ts>
                  <nts id="Seg_181" n="HIAT:ip">,</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_184" n="HIAT:w" s="T51">oččogo</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_187" n="HIAT:w" s="T52">tu͡ok</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_190" n="HIAT:w" s="T53">u͡ota</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_193" n="HIAT:w" s="T54">keli͡ej</ts>
                  <nts id="Seg_194" n="HIAT:ip">,</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_197" n="HIAT:w" s="T55">ile</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_200" n="HIAT:w" s="T56">u͡ottaːk</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_203" n="HIAT:w" s="T57">bu͡olu͡oktaraj</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_205" n="HIAT:ip">–</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_208" n="HIAT:w" s="T58">ačaːk</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_211" n="HIAT:w" s="T59">bu͡olla</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_214" n="HIAT:w" s="T60">onton</ts>
                  <nts id="Seg_215" n="HIAT:ip">.</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_218" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_220" n="HIAT:w" s="T61">Ol</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_223" n="HIAT:w" s="T62">ihin</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_226" n="HIAT:w" s="T63">eni</ts>
                  <nts id="Seg_227" n="HIAT:ip">,</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_230" n="HIAT:w" s="T64">tahaːra</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_233" n="HIAT:w" s="T65">künneːk</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_236" n="HIAT:w" s="T66">bagaj</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_239" n="HIAT:w" s="T67">küŋŋe</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_242" n="HIAT:w" s="T68">araŋahɨgar</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_245" n="HIAT:w" s="T69">tellegin</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_248" n="HIAT:w" s="T70">ɨjaːn</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_251" n="HIAT:w" s="T71">keːspit</ts>
                  <nts id="Seg_252" n="HIAT:ip">.</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_255" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_257" n="HIAT:w" s="T72">Kaja</ts>
                  <nts id="Seg_258" n="HIAT:ip">,</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_261" n="HIAT:w" s="T73">tellege</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_264" n="HIAT:w" s="T74">bu͡olla</ts>
                  <nts id="Seg_265" n="HIAT:ip">,</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_268" n="HIAT:w" s="T75">kaja</ts>
                  <nts id="Seg_269" n="HIAT:ip">,</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_272" n="HIAT:w" s="T76">čaːjɨn</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_275" n="HIAT:w" s="T77">ihe</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_278" n="HIAT:w" s="T78">olordoguna</ts>
                  <nts id="Seg_279" n="HIAT:ip">,</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_281" n="HIAT:ip">"</nts>
                  <ts e="T80" id="Seg_283" n="HIAT:w" s="T79">suːk</ts>
                  <nts id="Seg_284" n="HIAT:ip">"</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_287" n="HIAT:w" s="T80">gɨmmɨta</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_290" n="HIAT:w" s="T81">bu͡ollagɨna</ts>
                  <nts id="Seg_291" n="HIAT:ip">.</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_294" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_296" n="HIAT:w" s="T82">Taksa</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_299" n="HIAT:w" s="T83">köppüte</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_302" n="HIAT:w" s="T84">emeːksin</ts>
                  <nts id="Seg_303" n="HIAT:ip">,</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_306" n="HIAT:w" s="T85">taksa</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_309" n="HIAT:w" s="T86">köppüte</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_312" n="HIAT:w" s="T87">bu͡ollagɨna</ts>
                  <nts id="Seg_313" n="HIAT:ip">,</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_316" n="HIAT:w" s="T88">tellege</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_319" n="HIAT:w" s="T89">kötön</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_322" n="HIAT:w" s="T90">bu͡ollagɨna</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_325" n="HIAT:w" s="T91">kü͡ölüŋ</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_327" n="HIAT:ip">(</nts>
                  <nts id="Seg_328" n="HIAT:ip">(</nts>
                  <ats e="T403" id="Seg_329" n="HIAT:non-pho" s="T92">…</ats>
                  <nts id="Seg_330" n="HIAT:ip">)</nts>
                  <nts id="Seg_331" n="HIAT:ip">)</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_334" n="HIAT:w" s="T403">uː</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_337" n="HIAT:w" s="T93">bahɨnar</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_340" n="HIAT:w" s="T94">kölüjetin</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_343" n="HIAT:w" s="T95">kɨrɨːtɨgar</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_346" n="HIAT:w" s="T96">tüspüt</ts>
                  <nts id="Seg_347" n="HIAT:ip">.</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_350" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_352" n="HIAT:w" s="T98">Kajaː</ts>
                  <nts id="Seg_353" n="HIAT:ip">,</nts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_356" n="HIAT:w" s="T99">emeːksin</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_359" n="HIAT:w" s="T100">bu͡ollagɨna</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_362" n="HIAT:w" s="T101">baran</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_365" n="HIAT:w" s="T102">kaban</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_368" n="HIAT:w" s="T103">ɨlan</ts>
                  <nts id="Seg_369" n="HIAT:ip">,</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_371" n="HIAT:ip">"</nts>
                  <ts e="T105" id="Seg_373" n="HIAT:w" s="T104">telleppin</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_376" n="HIAT:w" s="T105">kaban</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_379" n="HIAT:w" s="T106">ɨlammɨn</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_382" n="HIAT:w" s="T107">egeli͡em</ts>
                  <nts id="Seg_383" n="HIAT:ip">"</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_386" n="HIAT:w" s="T108">di͡ebit</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_389" n="HIAT:w" s="T109">eːt</ts>
                  <nts id="Seg_390" n="HIAT:ip">.</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_393" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_395" n="HIAT:w" s="T110">Tɨ͡ala</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_397" n="HIAT:ip">"</nts>
                  <ts e="T112" id="Seg_399" n="HIAT:w" s="T111">hüːk</ts>
                  <nts id="Seg_400" n="HIAT:ip">"</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_403" n="HIAT:w" s="T112">gɨnnaran</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_406" n="HIAT:w" s="T113">buːska</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_409" n="HIAT:w" s="T114">kiːrbit</ts>
                  <nts id="Seg_410" n="HIAT:ip">.</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_413" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_415" n="HIAT:w" s="T115">Kaja</ts>
                  <nts id="Seg_416" n="HIAT:ip">,</nts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_419" n="HIAT:w" s="T116">kühün</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_422" n="HIAT:w" s="T117">ke</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_425" n="HIAT:w" s="T118">čelgien</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_428" n="HIAT:w" s="T119">turar</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_431" n="HIAT:w" s="T120">buːska</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_434" n="HIAT:w" s="T121">bu</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_437" n="HIAT:w" s="T122">emeːksin</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_440" n="HIAT:w" s="T123">kiːrbit</ts>
                  <nts id="Seg_441" n="HIAT:ip">.</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_444" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_446" n="HIAT:w" s="T124">Munu</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_449" n="HIAT:w" s="T125">batan</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_452" n="HIAT:w" s="T126">hüːrebin</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_455" n="HIAT:w" s="T127">di͡en</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_458" n="HIAT:w" s="T128">kaltɨrɨjan</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_461" n="HIAT:w" s="T129">tühen</ts>
                  <nts id="Seg_462" n="HIAT:ip">,</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_465" n="HIAT:w" s="T130">öttügün</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_468" n="HIAT:w" s="T131">bulgu</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_471" n="HIAT:w" s="T132">tüspüt</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_474" n="HIAT:w" s="T133">emeːksin</ts>
                  <nts id="Seg_475" n="HIAT:ip">.</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_478" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_480" n="HIAT:w" s="T134">Kaja</ts>
                  <nts id="Seg_481" n="HIAT:ip">,</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_484" n="HIAT:w" s="T135">emeːksin</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_487" n="HIAT:w" s="T136">bu͡o</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_490" n="HIAT:w" s="T137">di͡en</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_493" n="HIAT:w" s="T138">bu͡olar</ts>
                  <nts id="Seg_494" n="HIAT:ip">:</nts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_497" n="HIAT:u" s="T139">
                  <nts id="Seg_498" n="HIAT:ip">"</nts>
                  <ts e="T140" id="Seg_500" n="HIAT:w" s="T139">Buːs</ts>
                  <nts id="Seg_501" n="HIAT:ip">,</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_504" n="HIAT:w" s="T140">buːs</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_507" n="HIAT:w" s="T141">berkin</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_510" n="HIAT:w" s="T142">duː</ts>
                  <nts id="Seg_511" n="HIAT:ip">"</nts>
                  <nts id="Seg_512" n="HIAT:ip">,</nts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_515" n="HIAT:w" s="T143">diːr</ts>
                  <nts id="Seg_516" n="HIAT:ip">.</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_519" n="HIAT:u" s="T144">
                  <nts id="Seg_520" n="HIAT:ip">"</nts>
                  <ts e="T145" id="Seg_522" n="HIAT:w" s="T144">Eː</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_525" n="HIAT:w" s="T145">dʼe</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_528" n="HIAT:w" s="T146">berpin</ts>
                  <nts id="Seg_529" n="HIAT:ip">,</nts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_532" n="HIAT:w" s="T147">de</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_535" n="HIAT:w" s="T148">berpin</ts>
                  <nts id="Seg_536" n="HIAT:ip">,</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_539" n="HIAT:w" s="T149">en</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_542" n="HIAT:w" s="T150">öttükkün</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_545" n="HIAT:w" s="T151">tohuttum</ts>
                  <nts id="Seg_546" n="HIAT:ip">"</nts>
                  <nts id="Seg_547" n="HIAT:ip">,</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_550" n="HIAT:w" s="T152">di͡en</ts>
                  <nts id="Seg_551" n="HIAT:ip">.</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_554" n="HIAT:u" s="T153">
                  <nts id="Seg_555" n="HIAT:ip">"</nts>
                  <ts e="T154" id="Seg_557" n="HIAT:w" s="T153">Onnuk</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_560" n="HIAT:w" s="T154">bert</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_563" n="HIAT:w" s="T155">kihi</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_566" n="HIAT:w" s="T156">ki͡e</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_569" n="HIAT:w" s="T157">togo</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_572" n="HIAT:w" s="T158">haːs</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_575" n="HIAT:w" s="T159">bu͡olu͡o</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_578" n="HIAT:w" s="T160">daː</ts>
                  <nts id="Seg_579" n="HIAT:ip">,</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_582" n="HIAT:w" s="T161">kün</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_585" n="HIAT:w" s="T162">u͡otuttan</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_588" n="HIAT:w" s="T163">uːlan</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_591" n="HIAT:w" s="T164">nʼolbojon</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_594" n="HIAT:w" s="T165">tüheːččiginij</ts>
                  <nts id="Seg_595" n="HIAT:ip">"</nts>
                  <nts id="Seg_596" n="HIAT:ip">,</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_599" n="HIAT:w" s="T166">di͡ebit</ts>
                  <nts id="Seg_600" n="HIAT:ip">.</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_603" n="HIAT:u" s="T167">
                  <nts id="Seg_604" n="HIAT:ip">"</nts>
                  <ts e="T168" id="Seg_606" n="HIAT:w" s="T167">Kün</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_609" n="HIAT:w" s="T168">u͡ota</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_612" n="HIAT:w" s="T169">bert</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_615" n="HIAT:w" s="T170">da</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_618" n="HIAT:w" s="T171">bert</ts>
                  <nts id="Seg_619" n="HIAT:ip">"</nts>
                  <nts id="Seg_620" n="HIAT:ip">,</nts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_623" n="HIAT:w" s="T172">diːr</ts>
                  <nts id="Seg_624" n="HIAT:ip">.</nts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_627" n="HIAT:u" s="T173">
                  <nts id="Seg_628" n="HIAT:ip">"</nts>
                  <ts e="T174" id="Seg_630" n="HIAT:w" s="T173">Minigin</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_633" n="HIAT:w" s="T174">uːlarɨmɨ͡aktaːgar</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_636" n="HIAT:w" s="T175">bu͡olu͡ok</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_639" n="HIAT:w" s="T176">eŋin-eŋitteri</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_642" n="HIAT:w" s="T177">uːlaraːččɨ</ts>
                  <nts id="Seg_643" n="HIAT:ip">"</nts>
                  <nts id="Seg_644" n="HIAT:ip">,</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_647" n="HIAT:w" s="T178">diːr</ts>
                  <nts id="Seg_648" n="HIAT:ip">.</nts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_651" n="HIAT:u" s="T179">
                  <nts id="Seg_652" n="HIAT:ip">"</nts>
                  <ts e="T180" id="Seg_654" n="HIAT:w" s="T179">Küːn</ts>
                  <nts id="Seg_655" n="HIAT:ip">!</nts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_658" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_660" n="HIAT:w" s="T180">Berkin</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_663" n="HIAT:w" s="T181">du͡o</ts>
                  <nts id="Seg_664" n="HIAT:ip">?</nts>
                  <nts id="Seg_665" n="HIAT:ip">"</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_668" n="HIAT:u" s="T182">
                  <nts id="Seg_669" n="HIAT:ip">"</nts>
                  <ts e="T183" id="Seg_671" n="HIAT:w" s="T182">Dʼe</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_674" n="HIAT:w" s="T183">berpin</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_677" n="HIAT:w" s="T184">da</ts>
                  <nts id="Seg_678" n="HIAT:ip">,</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_681" n="HIAT:w" s="T185">berpi͡en</ts>
                  <nts id="Seg_682" n="HIAT:ip">"</nts>
                  <nts id="Seg_683" n="HIAT:ip">,</nts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_686" n="HIAT:w" s="T186">diːr</ts>
                  <nts id="Seg_687" n="HIAT:ip">.</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_690" n="HIAT:u" s="T187">
                  <nts id="Seg_691" n="HIAT:ip">"</nts>
                  <ts e="T188" id="Seg_693" n="HIAT:w" s="T187">Kaːrɨ</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_696" n="HIAT:w" s="T188">da</ts>
                  <nts id="Seg_697" n="HIAT:ip">,</nts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_700" n="HIAT:w" s="T189">buːhu</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_703" n="HIAT:w" s="T190">da</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_706" n="HIAT:w" s="T191">ordorbokko</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_709" n="HIAT:w" s="T192">iri͡ereːččibin</ts>
                  <nts id="Seg_710" n="HIAT:ip">"</nts>
                  <nts id="Seg_711" n="HIAT:ip">,</nts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_714" n="HIAT:w" s="T193">diːr</ts>
                  <nts id="Seg_715" n="HIAT:ip">.</nts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_718" n="HIAT:u" s="T194">
                  <nts id="Seg_719" n="HIAT:ip">"</nts>
                  <ts e="T195" id="Seg_721" n="HIAT:w" s="T194">Paː</ts>
                  <nts id="Seg_722" n="HIAT:ip">,</nts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_725" n="HIAT:w" s="T195">onnuk</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_728" n="HIAT:w" s="T196">bert</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_731" n="HIAT:w" s="T197">kihi</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_734" n="HIAT:w" s="T198">keː</ts>
                  <nts id="Seg_735" n="HIAT:ip">,</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_738" n="HIAT:w" s="T199">togo</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_741" n="HIAT:w" s="T200">taːs</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_744" n="HIAT:w" s="T201">kaja</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_747" n="HIAT:w" s="T202">kalkatɨgar</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_750" n="HIAT:w" s="T203">ketegiger</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_753" n="HIAT:w" s="T204">kistene</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_756" n="HIAT:w" s="T205">hɨtaːččɨgɨn</ts>
                  <nts id="Seg_757" n="HIAT:ip">"</nts>
                  <nts id="Seg_758" n="HIAT:ip">,</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_761" n="HIAT:w" s="T206">diːr</ts>
                  <nts id="Seg_762" n="HIAT:ip">.</nts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_765" n="HIAT:u" s="T207">
                  <nts id="Seg_766" n="HIAT:ip">"</nts>
                  <ts e="T208" id="Seg_768" n="HIAT:w" s="T207">Taːs</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_771" n="HIAT:w" s="T208">kaja</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_774" n="HIAT:w" s="T209">bert</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_777" n="HIAT:w" s="T210">daː</ts>
                  <nts id="Seg_778" n="HIAT:ip">,</nts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_781" n="HIAT:w" s="T211">bert</ts>
                  <nts id="Seg_782" n="HIAT:ip">"</nts>
                  <nts id="Seg_783" n="HIAT:ip">,</nts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_786" n="HIAT:w" s="T212">diːr</ts>
                  <nts id="Seg_787" n="HIAT:ip">,</nts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_789" n="HIAT:ip">"</nts>
                  <ts e="T214" id="Seg_791" n="HIAT:w" s="T213">minigin</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_794" n="HIAT:w" s="T214">innibitten</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_797" n="HIAT:w" s="T215">kalkalaːččɨ</ts>
                  <nts id="Seg_798" n="HIAT:ip">"</nts>
                  <nts id="Seg_799" n="HIAT:ip">,</nts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_802" n="HIAT:w" s="T216">diːr</ts>
                  <nts id="Seg_803" n="HIAT:ip">.</nts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_806" n="HIAT:u" s="T217">
                  <nts id="Seg_807" n="HIAT:ip">"</nts>
                  <ts e="T218" id="Seg_809" n="HIAT:w" s="T217">Taːs</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_812" n="HIAT:w" s="T218">kaja-e</ts>
                  <nts id="Seg_813" n="HIAT:ip">!</nts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_816" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_818" n="HIAT:w" s="T219">Berkin</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_821" n="HIAT:w" s="T220">du͡o</ts>
                  <nts id="Seg_822" n="HIAT:ip">?</nts>
                  <nts id="Seg_823" n="HIAT:ip">"</nts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_826" n="HIAT:u" s="T221">
                  <nts id="Seg_827" n="HIAT:ip">"</nts>
                  <ts e="T222" id="Seg_829" n="HIAT:w" s="T221">Če</ts>
                  <nts id="Seg_830" n="HIAT:ip">,</nts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_833" n="HIAT:w" s="T222">berpin</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_836" n="HIAT:w" s="T223">da</ts>
                  <nts id="Seg_837" n="HIAT:ip">,</nts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_840" n="HIAT:w" s="T224">berpi͡en</ts>
                  <nts id="Seg_841" n="HIAT:ip">.</nts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_844" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_846" n="HIAT:w" s="T225">Künü</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_849" n="HIAT:w" s="T226">bagas</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_852" n="HIAT:w" s="T227">kalkalaːččɨbɨn</ts>
                  <nts id="Seg_853" n="HIAT:ip">"</nts>
                  <nts id="Seg_854" n="HIAT:ip">,</nts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_857" n="HIAT:w" s="T228">diːr</ts>
                  <nts id="Seg_858" n="HIAT:ip">,</nts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_860" n="HIAT:ip">"</nts>
                  <ts e="T230" id="Seg_862" n="HIAT:w" s="T229">kɨhɨnɨ</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_865" n="HIAT:w" s="T230">bɨha</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_868" n="HIAT:w" s="T231">da</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_871" n="HIAT:w" s="T232">karaŋa</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_874" n="HIAT:w" s="T233">oŋoroːččubɨn</ts>
                  <nts id="Seg_875" n="HIAT:ip">"</nts>
                  <nts id="Seg_876" n="HIAT:ip">,</nts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_879" n="HIAT:w" s="T234">diːr</ts>
                  <nts id="Seg_880" n="HIAT:ip">.</nts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_883" n="HIAT:u" s="T235">
                  <nts id="Seg_884" n="HIAT:ip">"</nts>
                  <ts e="T236" id="Seg_886" n="HIAT:w" s="T235">Onnuk</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_889" n="HIAT:w" s="T236">bert</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_892" n="HIAT:w" s="T237">kihi</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_895" n="HIAT:w" s="T238">ki͡e</ts>
                  <nts id="Seg_896" n="HIAT:ip">,</nts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_899" n="HIAT:w" s="T239">togo</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_902" n="HIAT:w" s="T240">üːteːn</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_905" n="HIAT:w" s="T241">kutujaktarga</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_908" n="HIAT:w" s="T242">supturut</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_911" n="HIAT:w" s="T243">üːtete</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_914" n="HIAT:w" s="T244">hɨldʼagɨn</ts>
                  <nts id="Seg_915" n="HIAT:ip">"</nts>
                  <nts id="Seg_916" n="HIAT:ip">,</nts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_919" n="HIAT:w" s="T245">di͡ebit</ts>
                  <nts id="Seg_920" n="HIAT:ip">.</nts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_923" n="HIAT:u" s="T246">
                  <nts id="Seg_924" n="HIAT:ip">"</nts>
                  <ts e="T247" id="Seg_926" n="HIAT:w" s="T246">Če</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_929" n="HIAT:w" s="T247">üːteːn</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_932" n="HIAT:w" s="T248">kutujaktar</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_935" n="HIAT:w" s="T249">berter</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_938" n="HIAT:w" s="T250">da</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_941" n="HIAT:w" s="T251">berteːr</ts>
                  <nts id="Seg_942" n="HIAT:ip">,</nts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_945" n="HIAT:w" s="T252">bert</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_948" n="HIAT:w" s="T253">hɨtɨː</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_951" n="HIAT:w" s="T254">tumsulaːktar</ts>
                  <nts id="Seg_952" n="HIAT:ip">"</nts>
                  <nts id="Seg_953" n="HIAT:ip">,</nts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_956" n="HIAT:w" s="T255">diːr</ts>
                  <nts id="Seg_957" n="HIAT:ip">.</nts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_960" n="HIAT:u" s="T256">
                  <nts id="Seg_961" n="HIAT:ip">"</nts>
                  <ts e="T257" id="Seg_963" n="HIAT:w" s="T256">Kutujaktar</ts>
                  <nts id="Seg_964" n="HIAT:ip">,</nts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_967" n="HIAT:w" s="T257">berkit</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_970" n="HIAT:w" s="T258">du͡o</ts>
                  <nts id="Seg_971" n="HIAT:ip">?</nts>
                  <nts id="Seg_972" n="HIAT:ip">"</nts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T269" id="Seg_975" n="HIAT:u" s="T259">
                  <nts id="Seg_976" n="HIAT:ip">"</nts>
                  <ts e="T260" id="Seg_978" n="HIAT:w" s="T259">Berpit</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_981" n="HIAT:w" s="T260">da</ts>
                  <nts id="Seg_982" n="HIAT:ip">,</nts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_985" n="HIAT:w" s="T261">berpi͡et</ts>
                  <nts id="Seg_986" n="HIAT:ip">,</nts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_989" n="HIAT:w" s="T262">taːs</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_992" n="HIAT:w" s="T263">kajanɨ</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_995" n="HIAT:w" s="T264">kanan</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_998" n="HIAT:w" s="T265">hanɨːr</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1001" n="HIAT:w" s="T266">üktüː</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1004" n="HIAT:w" s="T267">taksaːččɨbɨt</ts>
                  <nts id="Seg_1005" n="HIAT:ip">"</nts>
                  <nts id="Seg_1006" n="HIAT:ip">,</nts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1009" n="HIAT:w" s="T268">diː</ts>
                  <nts id="Seg_1010" n="HIAT:ip">.</nts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_1013" n="HIAT:u" s="T269">
                  <nts id="Seg_1014" n="HIAT:ip">"</nts>
                  <ts e="T270" id="Seg_1016" n="HIAT:w" s="T269">Onnuk</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1019" n="HIAT:w" s="T270">bert</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1022" n="HIAT:w" s="T271">kihiler</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1025" n="HIAT:w" s="T272">ki͡e</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1028" n="HIAT:w" s="T273">togo</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1031" n="HIAT:w" s="T274">kɨrsaga</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1034" n="HIAT:w" s="T275">hi͡eteːččigitij</ts>
                  <nts id="Seg_1035" n="HIAT:ip">"</nts>
                  <nts id="Seg_1036" n="HIAT:ip">,</nts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1039" n="HIAT:w" s="T276">di͡ebit</ts>
                  <nts id="Seg_1040" n="HIAT:ip">.</nts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1043" n="HIAT:u" s="T277">
                  <nts id="Seg_1044" n="HIAT:ip">"</nts>
                  <ts e="T278" id="Seg_1046" n="HIAT:w" s="T277">Kɨrsa</ts>
                  <nts id="Seg_1047" n="HIAT:ip">,</nts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1050" n="HIAT:w" s="T278">kɨrsa</ts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1053" n="HIAT:w" s="T279">hi͡emne</ts>
                  <nts id="Seg_1054" n="HIAT:ip">"</nts>
                  <nts id="Seg_1055" n="HIAT:ip">,</nts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1058" n="HIAT:w" s="T280">diːr</ts>
                  <nts id="Seg_1059" n="HIAT:ip">,</nts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1061" n="HIAT:ip">"</nts>
                  <ts e="T282" id="Seg_1063" n="HIAT:w" s="T281">bihigini</ts>
                  <nts id="Seg_1064" n="HIAT:ip">,</nts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1067" n="HIAT:w" s="T282">hi͡ečči</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1070" n="HIAT:w" s="T283">da</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1073" n="HIAT:w" s="T284">hi͡ečči</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1076" n="HIAT:w" s="T285">kɨrsa</ts>
                  <nts id="Seg_1077" n="HIAT:ip">"</nts>
                  <nts id="Seg_1078" n="HIAT:ip">.</nts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1081" n="HIAT:u" s="T286">
                  <nts id="Seg_1082" n="HIAT:ip">"</nts>
                  <ts e="T287" id="Seg_1084" n="HIAT:w" s="T286">Kɨrsaː</ts>
                  <nts id="Seg_1085" n="HIAT:ip">,</nts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1088" n="HIAT:w" s="T287">berkin</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1091" n="HIAT:w" s="T288">du͡o</ts>
                  <nts id="Seg_1092" n="HIAT:ip">?</nts>
                  <nts id="Seg_1093" n="HIAT:ip">"</nts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1096" n="HIAT:u" s="T289">
                  <nts id="Seg_1097" n="HIAT:ip">"</nts>
                  <ts e="T290" id="Seg_1099" n="HIAT:w" s="T289">Berpin</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1102" n="HIAT:w" s="T290">da</ts>
                  <nts id="Seg_1103" n="HIAT:ip">,</nts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1106" n="HIAT:w" s="T291">berpi͡en</ts>
                  <nts id="Seg_1107" n="HIAT:ip">"</nts>
                  <nts id="Seg_1108" n="HIAT:ip">,</nts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1111" n="HIAT:w" s="T292">diːr</ts>
                  <nts id="Seg_1112" n="HIAT:ip">.</nts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_1115" n="HIAT:u" s="T293">
                  <nts id="Seg_1116" n="HIAT:ip">"</nts>
                  <ts e="T294" id="Seg_1118" n="HIAT:w" s="T293">Kutujagɨ</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1121" n="HIAT:w" s="T294">kantan</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1124" n="HIAT:w" s="T295">hanɨːr</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1127" n="HIAT:w" s="T296">kahan</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1130" n="HIAT:w" s="T297">ɨlan</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1133" n="HIAT:w" s="T298">hi͡eččibit</ts>
                  <nts id="Seg_1134" n="HIAT:ip">"</nts>
                  <nts id="Seg_1135" n="HIAT:ip">.</nts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T309" id="Seg_1138" n="HIAT:u" s="T299">
                  <nts id="Seg_1139" n="HIAT:ip">"</nts>
                  <ts e="T300" id="Seg_1141" n="HIAT:w" s="T299">Onnuk</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1144" n="HIAT:w" s="T300">bert</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1147" n="HIAT:w" s="T301">kihi</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1150" n="HIAT:w" s="T302">ki͡e</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1153" n="HIAT:w" s="T303">čaːrkaːnnaːk</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1156" n="HIAT:w" s="T304">saka</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1159" n="HIAT:w" s="T305">ogolorgo</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1162" n="HIAT:w" s="T306">togo</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1165" n="HIAT:w" s="T307">kaptaraːččɨgɨnɨj</ts>
                  <nts id="Seg_1166" n="HIAT:ip">"</nts>
                  <nts id="Seg_1167" n="HIAT:ip">,</nts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1170" n="HIAT:w" s="T308">di͡ebit</ts>
                  <nts id="Seg_1171" n="HIAT:ip">.</nts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1174" n="HIAT:u" s="T309">
                  <nts id="Seg_1175" n="HIAT:ip">"</nts>
                  <ts e="T310" id="Seg_1177" n="HIAT:w" s="T309">Čaːrkaːnnaːk</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1180" n="HIAT:w" s="T310">saka</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1183" n="HIAT:w" s="T311">ogoloro</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1186" n="HIAT:w" s="T312">kaːr</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1189" n="HIAT:w" s="T313">annɨgar</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1192" n="HIAT:w" s="T314">kömpütterin</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1195" n="HIAT:w" s="T315">kantan</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1198" n="HIAT:w" s="T316">bili͡emij</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1201" n="HIAT:w" s="T317">kapkaːnɨ</ts>
                  <nts id="Seg_1202" n="HIAT:ip">"</nts>
                  <nts id="Seg_1203" n="HIAT:ip">,</nts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1206" n="HIAT:w" s="T318">diːr</ts>
                  <nts id="Seg_1207" n="HIAT:ip">.</nts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_1210" n="HIAT:u" s="T319">
                  <nts id="Seg_1211" n="HIAT:ip">"</nts>
                  <ts e="T320" id="Seg_1213" n="HIAT:w" s="T319">Ol</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1216" n="HIAT:w" s="T320">ihin</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1219" n="HIAT:w" s="T321">tuttaraːččɨbɨn</ts>
                  <nts id="Seg_1220" n="HIAT:ip">"</nts>
                  <nts id="Seg_1221" n="HIAT:ip">,</nts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1224" n="HIAT:w" s="T322">diːr</ts>
                  <nts id="Seg_1225" n="HIAT:ip">,</nts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1227" n="HIAT:ip">"</nts>
                  <ts e="T324" id="Seg_1229" n="HIAT:w" s="T323">ahɨː</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1232" n="HIAT:w" s="T324">kelemmin</ts>
                  <nts id="Seg_1233" n="HIAT:ip">"</nts>
                  <nts id="Seg_1234" n="HIAT:ip">,</nts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1237" n="HIAT:w" s="T325">diːr</ts>
                  <nts id="Seg_1238" n="HIAT:ip">.</nts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T331" id="Seg_1241" n="HIAT:u" s="T326">
                  <nts id="Seg_1242" n="HIAT:ip">"</nts>
                  <ts e="T327" id="Seg_1244" n="HIAT:w" s="T326">Čaːrkaːnnaːk</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1247" n="HIAT:w" s="T327">saka</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1250" n="HIAT:w" s="T328">ogoloro</ts>
                  <nts id="Seg_1251" n="HIAT:ip">,</nts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1254" n="HIAT:w" s="T329">berkit</ts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1257" n="HIAT:w" s="T330">duː</ts>
                  <nts id="Seg_1258" n="HIAT:ip">?</nts>
                  <nts id="Seg_1259" n="HIAT:ip">"</nts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T338" id="Seg_1262" n="HIAT:u" s="T331">
                  <nts id="Seg_1263" n="HIAT:ip">"</nts>
                  <ts e="T332" id="Seg_1265" n="HIAT:w" s="T331">Berpit</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1268" n="HIAT:w" s="T332">da</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1271" n="HIAT:w" s="T333">berpi͡et</ts>
                  <nts id="Seg_1272" n="HIAT:ip">,</nts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1275" n="HIAT:w" s="T334">kɨrsanɨ</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1278" n="HIAT:w" s="T335">kaččaga</ts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1281" n="HIAT:w" s="T336">hanɨːr</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1284" n="HIAT:w" s="T337">kabaːččɨbɨt</ts>
                  <nts id="Seg_1285" n="HIAT:ip">.</nts>
                  <nts id="Seg_1286" n="HIAT:ip">"</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1289" n="HIAT:u" s="T338">
                  <nts id="Seg_1290" n="HIAT:ip">"</nts>
                  <ts e="T339" id="Seg_1292" n="HIAT:w" s="T338">Onnuk</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1295" n="HIAT:w" s="T339">bert</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1298" n="HIAT:w" s="T340">kihiler</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1301" n="HIAT:w" s="T341">ke</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1304" n="HIAT:w" s="T342">togo</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1307" n="HIAT:w" s="T343">ojuŋŋa</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1310" n="HIAT:w" s="T344">si͡eteːččigitij</ts>
                  <nts id="Seg_1311" n="HIAT:ip">"</nts>
                  <nts id="Seg_1312" n="HIAT:ip">,</nts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1315" n="HIAT:w" s="T345">di͡ebit</ts>
                  <nts id="Seg_1316" n="HIAT:ip">.</nts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T355" id="Seg_1319" n="HIAT:u" s="T346">
                  <nts id="Seg_1320" n="HIAT:ip">"</nts>
                  <ts e="T347" id="Seg_1322" n="HIAT:w" s="T346">Ojun</ts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1325" n="HIAT:w" s="T347">hürdeːk</ts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1328" n="HIAT:w" s="T348">bu͡olla</ts>
                  <nts id="Seg_1329" n="HIAT:ip">"</nts>
                  <nts id="Seg_1330" n="HIAT:ip">,</nts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1333" n="HIAT:w" s="T349">di͡ebit</ts>
                  <nts id="Seg_1334" n="HIAT:ip">,</nts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1336" n="HIAT:ip">"</nts>
                  <ts e="T351" id="Seg_1338" n="HIAT:w" s="T350">hi͡em</ts>
                  <nts id="Seg_1339" n="HIAT:ip">,</nts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1342" n="HIAT:w" s="T351">di͡ege</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1345" n="HIAT:w" s="T352">da</ts>
                  <nts id="Seg_1346" n="HIAT:ip">,</nts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1349" n="HIAT:w" s="T353">hi͡ečči</ts>
                  <nts id="Seg_1350" n="HIAT:ip">"</nts>
                  <nts id="Seg_1351" n="HIAT:ip">,</nts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1354" n="HIAT:w" s="T354">diː</ts>
                  <nts id="Seg_1355" n="HIAT:ip">.</nts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T356" id="Seg_1358" n="HIAT:u" s="T355">
                  <nts id="Seg_1359" n="HIAT:ip">"</nts>
                  <ts e="T356" id="Seg_1361" n="HIAT:w" s="T355">Ojuːn</ts>
                  <nts id="Seg_1362" n="HIAT:ip">!</nts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1365" n="HIAT:u" s="T356">
                  <ts e="T357" id="Seg_1367" n="HIAT:w" s="T356">Berkin</ts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1370" n="HIAT:w" s="T357">du͡o</ts>
                  <nts id="Seg_1371" n="HIAT:ip">?</nts>
                  <nts id="Seg_1372" n="HIAT:ip">"</nts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1375" n="HIAT:u" s="T358">
                  <nts id="Seg_1376" n="HIAT:ip">"</nts>
                  <ts e="T359" id="Seg_1378" n="HIAT:w" s="T358">Berpin</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1381" n="HIAT:w" s="T359">da</ts>
                  <nts id="Seg_1382" n="HIAT:ip">,</nts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1385" n="HIAT:w" s="T360">berpi͡en</ts>
                  <nts id="Seg_1386" n="HIAT:ip">.</nts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1389" n="HIAT:u" s="T361">
                  <ts e="T362" id="Seg_1391" n="HIAT:w" s="T361">Kaččaga</ts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1394" n="HIAT:w" s="T362">hanaːtɨm</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1397" n="HIAT:w" s="T363">daː</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1400" n="HIAT:w" s="T364">hi͡eččibin</ts>
                  <nts id="Seg_1401" n="HIAT:ip">,</nts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1404" n="HIAT:w" s="T365">kuhagannɨk</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1407" n="HIAT:w" s="T366">haŋarbɨt</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1410" n="HIAT:w" s="T367">kihi</ts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1413" n="HIAT:w" s="T368">bu͡olu͡o</ts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1416" n="HIAT:w" s="T369">da</ts>
                  <nts id="Seg_1417" n="HIAT:ip">"</nts>
                  <nts id="Seg_1418" n="HIAT:ip">,</nts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1421" n="HIAT:w" s="T370">diːr</ts>
                  <nts id="Seg_1422" n="HIAT:ip">.</nts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T379" id="Seg_1425" n="HIAT:u" s="T371">
                  <nts id="Seg_1426" n="HIAT:ip">"</nts>
                  <ts e="T372" id="Seg_1428" n="HIAT:w" s="T371">Onnuk</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1431" n="HIAT:w" s="T372">bert</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1434" n="HIAT:w" s="T373">kihi</ts>
                  <nts id="Seg_1435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1437" n="HIAT:w" s="T374">ki͡e</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1440" n="HIAT:w" s="T375">togo</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1443" n="HIAT:w" s="T376">himi͡ertke</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1446" n="HIAT:w" s="T377">hi͡eteːččiginij</ts>
                  <nts id="Seg_1447" n="HIAT:ip">"</nts>
                  <nts id="Seg_1448" n="HIAT:ip">,</nts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1451" n="HIAT:w" s="T378">diːr</ts>
                  <nts id="Seg_1452" n="HIAT:ip">.</nts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T386" id="Seg_1455" n="HIAT:u" s="T379">
                  <nts id="Seg_1456" n="HIAT:ip">"</nts>
                  <ts e="T380" id="Seg_1458" n="HIAT:w" s="T379">Himi͡ert</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1461" n="HIAT:w" s="T380">baː</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1464" n="HIAT:w" s="T381">kimi͡eke</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1467" n="HIAT:w" s="T382">da</ts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1470" n="HIAT:w" s="T383">tɨllaːččɨta</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1473" n="HIAT:w" s="T384">hu͡ok</ts>
                  <nts id="Seg_1474" n="HIAT:ip">"</nts>
                  <nts id="Seg_1475" n="HIAT:ip">,</nts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1478" n="HIAT:w" s="T385">di͡ebit</ts>
                  <nts id="Seg_1479" n="HIAT:ip">.</nts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T387" id="Seg_1482" n="HIAT:u" s="T386">
                  <nts id="Seg_1483" n="HIAT:ip">"</nts>
                  <ts e="T387" id="Seg_1485" n="HIAT:w" s="T386">Himi͡ert</ts>
                  <nts id="Seg_1486" n="HIAT:ip">!</nts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1489" n="HIAT:u" s="T387">
                  <ts e="T388" id="Seg_1491" n="HIAT:w" s="T387">Berkin</ts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1494" n="HIAT:w" s="T388">du͡o</ts>
                  <nts id="Seg_1495" n="HIAT:ip">"</nts>
                  <nts id="Seg_1496" n="HIAT:ip">,</nts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1499" n="HIAT:w" s="T389">diːr</ts>
                  <nts id="Seg_1500" n="HIAT:ip">.</nts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_1503" n="HIAT:u" s="T390">
                  <nts id="Seg_1504" n="HIAT:ip">"</nts>
                  <ts e="T391" id="Seg_1506" n="HIAT:w" s="T390">Berpin</ts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1509" n="HIAT:w" s="T391">da</ts>
                  <nts id="Seg_1510" n="HIAT:ip">,</nts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1513" n="HIAT:w" s="T392">berpin</ts>
                  <nts id="Seg_1514" n="HIAT:ip">,</nts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1517" n="HIAT:w" s="T393">enigin</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1520" n="HIAT:w" s="T394">da</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1523" n="HIAT:w" s="T395">hi͡em</ts>
                  <nts id="Seg_1524" n="HIAT:ip">"</nts>
                  <nts id="Seg_1525" n="HIAT:ip">,</nts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1528" n="HIAT:w" s="T396">di͡ebite</ts>
                  <nts id="Seg_1529" n="HIAT:ip">,</nts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1532" n="HIAT:w" s="T397">hi͡en</ts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1535" n="HIAT:w" s="T398">keːspite</ts>
                  <nts id="Seg_1536" n="HIAT:ip">.</nts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T402" id="Seg_1539" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_1541" n="HIAT:w" s="T399">Dʼe</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1544" n="HIAT:w" s="T400">iti</ts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1547" n="HIAT:w" s="T401">elete</ts>
                  <nts id="Seg_1548" n="HIAT:ip">.</nts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T402" id="Seg_1550" n="sc" s="T0">
               <ts e="T1" id="Seg_1552" n="e" s="T0">Taːl </ts>
               <ts e="T2" id="Seg_1554" n="e" s="T1">emeːksin </ts>
               <ts e="T3" id="Seg_1556" n="e" s="T2">olorbut, </ts>
               <ts e="T4" id="Seg_1558" n="e" s="T3">ühü, </ts>
               <ts e="T5" id="Seg_1560" n="e" s="T4">ojulaːk </ts>
               <ts e="T6" id="Seg_1562" n="e" s="T5">ottoːk </ts>
               <ts e="T7" id="Seg_1564" n="e" s="T6">orto </ts>
               <ts e="T8" id="Seg_1566" n="e" s="T7">togus </ts>
               <ts e="T9" id="Seg_1568" n="e" s="T8">dojduga – </ts>
               <ts e="T10" id="Seg_1570" n="e" s="T9">Taːl </ts>
               <ts e="T11" id="Seg_1572" n="e" s="T10">emeːksin. </ts>
               <ts e="T12" id="Seg_1574" n="e" s="T11">Bu </ts>
               <ts e="T13" id="Seg_1576" n="e" s="T12">Taːl </ts>
               <ts e="T14" id="Seg_1578" n="e" s="T13">emeːksin </ts>
               <ts e="T15" id="Seg_1580" n="e" s="T14">bu͡ollagɨna </ts>
               <ts e="T16" id="Seg_1582" n="e" s="T15">biːrde </ts>
               <ts e="T17" id="Seg_1584" n="e" s="T16">ki͡ehennen, </ts>
               <ts e="T18" id="Seg_1586" n="e" s="T17">kaja, </ts>
               <ts e="T19" id="Seg_1588" n="e" s="T18">čaːj </ts>
               <ts e="T20" id="Seg_1590" n="e" s="T19">ihinen-ihinen </ts>
               <ts e="T21" id="Seg_1592" n="e" s="T20">baraːn </ts>
               <ts e="T23" id="Seg_1594" n="e" s="T21">utujan… </ts>
               <ts e="T24" id="Seg_1596" n="e" s="T23">Utujbut </ts>
               <ts e="T25" id="Seg_1598" n="e" s="T24">duː, </ts>
               <ts e="T26" id="Seg_1600" n="e" s="T25">kajdak </ts>
               <ts e="T27" id="Seg_1602" n="e" s="T26">duː? </ts>
               <ts e="T28" id="Seg_1604" n="e" s="T27">Ulakaːta </ts>
               <ts e="T29" id="Seg_1606" n="e" s="T28">testibite </ts>
               <ts e="T30" id="Seg_1608" n="e" s="T29">duː, </ts>
               <ts e="T31" id="Seg_1610" n="e" s="T30">hamɨːr </ts>
               <ts e="T32" id="Seg_1612" n="e" s="T31">tüspüt </ts>
               <ts e="T33" id="Seg_1614" n="e" s="T32">duː, </ts>
               <ts e="T34" id="Seg_1616" n="e" s="T33">tu͡ok </ts>
               <ts e="T35" id="Seg_1618" n="e" s="T34">duː? </ts>
               <ts e="T36" id="Seg_1620" n="e" s="T35">Tellege </ts>
               <ts e="T37" id="Seg_1622" n="e" s="T36">ilijbit </ts>
               <ts e="T38" id="Seg_1624" n="e" s="T37">emi͡eksin </ts>
               <ts e="T39" id="Seg_1626" n="e" s="T38">gi͡ene. </ts>
               <ts e="T40" id="Seg_1628" n="e" s="T39">Bu </ts>
               <ts e="T41" id="Seg_1630" n="e" s="T40">tellege </ts>
               <ts e="T42" id="Seg_1632" n="e" s="T41">ilijen, </ts>
               <ts e="T43" id="Seg_1634" n="e" s="T42">emeːksin </ts>
               <ts e="T44" id="Seg_1636" n="e" s="T43">harsi͡erda </ts>
               <ts e="T45" id="Seg_1638" n="e" s="T44">kühün </ts>
               <ts e="T46" id="Seg_1640" n="e" s="T45">ebit, </ts>
               <ts e="T47" id="Seg_1642" n="e" s="T46">buːs </ts>
               <ts e="T48" id="Seg_1644" n="e" s="T47">toŋorun </ts>
               <ts e="T49" id="Seg_1646" n="e" s="T48">hagɨna. </ts>
               <ts e="T50" id="Seg_1648" n="e" s="T49">Emeːksin </ts>
               <ts e="T51" id="Seg_1650" n="e" s="T50">turan, </ts>
               <ts e="T52" id="Seg_1652" n="e" s="T51">oččogo </ts>
               <ts e="T53" id="Seg_1654" n="e" s="T52">tu͡ok </ts>
               <ts e="T54" id="Seg_1656" n="e" s="T53">u͡ota </ts>
               <ts e="T55" id="Seg_1658" n="e" s="T54">keli͡ej, </ts>
               <ts e="T56" id="Seg_1660" n="e" s="T55">ile </ts>
               <ts e="T57" id="Seg_1662" n="e" s="T56">u͡ottaːk </ts>
               <ts e="T58" id="Seg_1664" n="e" s="T57">bu͡olu͡oktaraj – </ts>
               <ts e="T59" id="Seg_1666" n="e" s="T58">ačaːk </ts>
               <ts e="T60" id="Seg_1668" n="e" s="T59">bu͡olla </ts>
               <ts e="T61" id="Seg_1670" n="e" s="T60">onton. </ts>
               <ts e="T62" id="Seg_1672" n="e" s="T61">Ol </ts>
               <ts e="T63" id="Seg_1674" n="e" s="T62">ihin </ts>
               <ts e="T64" id="Seg_1676" n="e" s="T63">eni, </ts>
               <ts e="T65" id="Seg_1678" n="e" s="T64">tahaːra </ts>
               <ts e="T66" id="Seg_1680" n="e" s="T65">künneːk </ts>
               <ts e="T67" id="Seg_1682" n="e" s="T66">bagaj </ts>
               <ts e="T68" id="Seg_1684" n="e" s="T67">küŋŋe </ts>
               <ts e="T69" id="Seg_1686" n="e" s="T68">araŋahɨgar </ts>
               <ts e="T70" id="Seg_1688" n="e" s="T69">tellegin </ts>
               <ts e="T71" id="Seg_1690" n="e" s="T70">ɨjaːn </ts>
               <ts e="T72" id="Seg_1692" n="e" s="T71">keːspit. </ts>
               <ts e="T73" id="Seg_1694" n="e" s="T72">Kaja, </ts>
               <ts e="T74" id="Seg_1696" n="e" s="T73">tellege </ts>
               <ts e="T75" id="Seg_1698" n="e" s="T74">bu͡olla, </ts>
               <ts e="T76" id="Seg_1700" n="e" s="T75">kaja, </ts>
               <ts e="T77" id="Seg_1702" n="e" s="T76">čaːjɨn </ts>
               <ts e="T78" id="Seg_1704" n="e" s="T77">ihe </ts>
               <ts e="T79" id="Seg_1706" n="e" s="T78">olordoguna, </ts>
               <ts e="T80" id="Seg_1708" n="e" s="T79">"suːk" </ts>
               <ts e="T81" id="Seg_1710" n="e" s="T80">gɨmmɨta </ts>
               <ts e="T82" id="Seg_1712" n="e" s="T81">bu͡ollagɨna. </ts>
               <ts e="T83" id="Seg_1714" n="e" s="T82">Taksa </ts>
               <ts e="T84" id="Seg_1716" n="e" s="T83">köppüte </ts>
               <ts e="T85" id="Seg_1718" n="e" s="T84">emeːksin, </ts>
               <ts e="T86" id="Seg_1720" n="e" s="T85">taksa </ts>
               <ts e="T87" id="Seg_1722" n="e" s="T86">köppüte </ts>
               <ts e="T88" id="Seg_1724" n="e" s="T87">bu͡ollagɨna, </ts>
               <ts e="T89" id="Seg_1726" n="e" s="T88">tellege </ts>
               <ts e="T90" id="Seg_1728" n="e" s="T89">kötön </ts>
               <ts e="T91" id="Seg_1730" n="e" s="T90">bu͡ollagɨna </ts>
               <ts e="T92" id="Seg_1732" n="e" s="T91">kü͡ölüŋ </ts>
               <ts e="T403" id="Seg_1734" n="e" s="T92">((…)) </ts>
               <ts e="T93" id="Seg_1736" n="e" s="T403">uː </ts>
               <ts e="T94" id="Seg_1738" n="e" s="T93">bahɨnar </ts>
               <ts e="T95" id="Seg_1740" n="e" s="T94">kölüjetin </ts>
               <ts e="T96" id="Seg_1742" n="e" s="T95">kɨrɨːtɨgar </ts>
               <ts e="T98" id="Seg_1744" n="e" s="T96">tüspüt. </ts>
               <ts e="T99" id="Seg_1746" n="e" s="T98">Kajaː, </ts>
               <ts e="T100" id="Seg_1748" n="e" s="T99">emeːksin </ts>
               <ts e="T101" id="Seg_1750" n="e" s="T100">bu͡ollagɨna </ts>
               <ts e="T102" id="Seg_1752" n="e" s="T101">baran </ts>
               <ts e="T103" id="Seg_1754" n="e" s="T102">kaban </ts>
               <ts e="T104" id="Seg_1756" n="e" s="T103">ɨlan, </ts>
               <ts e="T105" id="Seg_1758" n="e" s="T104">"telleppin </ts>
               <ts e="T106" id="Seg_1760" n="e" s="T105">kaban </ts>
               <ts e="T107" id="Seg_1762" n="e" s="T106">ɨlammɨn </ts>
               <ts e="T108" id="Seg_1764" n="e" s="T107">egeli͡em" </ts>
               <ts e="T109" id="Seg_1766" n="e" s="T108">di͡ebit </ts>
               <ts e="T110" id="Seg_1768" n="e" s="T109">eːt. </ts>
               <ts e="T111" id="Seg_1770" n="e" s="T110">Tɨ͡ala </ts>
               <ts e="T112" id="Seg_1772" n="e" s="T111">"hüːk" </ts>
               <ts e="T113" id="Seg_1774" n="e" s="T112">gɨnnaran </ts>
               <ts e="T114" id="Seg_1776" n="e" s="T113">buːska </ts>
               <ts e="T115" id="Seg_1778" n="e" s="T114">kiːrbit. </ts>
               <ts e="T116" id="Seg_1780" n="e" s="T115">Kaja, </ts>
               <ts e="T117" id="Seg_1782" n="e" s="T116">kühün </ts>
               <ts e="T118" id="Seg_1784" n="e" s="T117">ke </ts>
               <ts e="T119" id="Seg_1786" n="e" s="T118">čelgien </ts>
               <ts e="T120" id="Seg_1788" n="e" s="T119">turar </ts>
               <ts e="T121" id="Seg_1790" n="e" s="T120">buːska </ts>
               <ts e="T122" id="Seg_1792" n="e" s="T121">bu </ts>
               <ts e="T123" id="Seg_1794" n="e" s="T122">emeːksin </ts>
               <ts e="T124" id="Seg_1796" n="e" s="T123">kiːrbit. </ts>
               <ts e="T125" id="Seg_1798" n="e" s="T124">Munu </ts>
               <ts e="T126" id="Seg_1800" n="e" s="T125">batan </ts>
               <ts e="T127" id="Seg_1802" n="e" s="T126">hüːrebin </ts>
               <ts e="T128" id="Seg_1804" n="e" s="T127">di͡en </ts>
               <ts e="T129" id="Seg_1806" n="e" s="T128">kaltɨrɨjan </ts>
               <ts e="T130" id="Seg_1808" n="e" s="T129">tühen, </ts>
               <ts e="T131" id="Seg_1810" n="e" s="T130">öttügün </ts>
               <ts e="T132" id="Seg_1812" n="e" s="T131">bulgu </ts>
               <ts e="T133" id="Seg_1814" n="e" s="T132">tüspüt </ts>
               <ts e="T134" id="Seg_1816" n="e" s="T133">emeːksin. </ts>
               <ts e="T135" id="Seg_1818" n="e" s="T134">Kaja, </ts>
               <ts e="T136" id="Seg_1820" n="e" s="T135">emeːksin </ts>
               <ts e="T137" id="Seg_1822" n="e" s="T136">bu͡o </ts>
               <ts e="T138" id="Seg_1824" n="e" s="T137">di͡en </ts>
               <ts e="T139" id="Seg_1826" n="e" s="T138">bu͡olar: </ts>
               <ts e="T140" id="Seg_1828" n="e" s="T139">"Buːs, </ts>
               <ts e="T141" id="Seg_1830" n="e" s="T140">buːs </ts>
               <ts e="T142" id="Seg_1832" n="e" s="T141">berkin </ts>
               <ts e="T143" id="Seg_1834" n="e" s="T142">duː", </ts>
               <ts e="T144" id="Seg_1836" n="e" s="T143">diːr. </ts>
               <ts e="T145" id="Seg_1838" n="e" s="T144">"Eː </ts>
               <ts e="T146" id="Seg_1840" n="e" s="T145">dʼe </ts>
               <ts e="T147" id="Seg_1842" n="e" s="T146">berpin, </ts>
               <ts e="T148" id="Seg_1844" n="e" s="T147">de </ts>
               <ts e="T149" id="Seg_1846" n="e" s="T148">berpin, </ts>
               <ts e="T150" id="Seg_1848" n="e" s="T149">en </ts>
               <ts e="T151" id="Seg_1850" n="e" s="T150">öttükkün </ts>
               <ts e="T152" id="Seg_1852" n="e" s="T151">tohuttum", </ts>
               <ts e="T153" id="Seg_1854" n="e" s="T152">di͡en. </ts>
               <ts e="T154" id="Seg_1856" n="e" s="T153">"Onnuk </ts>
               <ts e="T155" id="Seg_1858" n="e" s="T154">bert </ts>
               <ts e="T156" id="Seg_1860" n="e" s="T155">kihi </ts>
               <ts e="T157" id="Seg_1862" n="e" s="T156">ki͡e </ts>
               <ts e="T158" id="Seg_1864" n="e" s="T157">togo </ts>
               <ts e="T159" id="Seg_1866" n="e" s="T158">haːs </ts>
               <ts e="T160" id="Seg_1868" n="e" s="T159">bu͡olu͡o </ts>
               <ts e="T161" id="Seg_1870" n="e" s="T160">daː, </ts>
               <ts e="T162" id="Seg_1872" n="e" s="T161">kün </ts>
               <ts e="T163" id="Seg_1874" n="e" s="T162">u͡otuttan </ts>
               <ts e="T164" id="Seg_1876" n="e" s="T163">uːlan </ts>
               <ts e="T165" id="Seg_1878" n="e" s="T164">nʼolbojon </ts>
               <ts e="T166" id="Seg_1880" n="e" s="T165">tüheːččiginij", </ts>
               <ts e="T167" id="Seg_1882" n="e" s="T166">di͡ebit. </ts>
               <ts e="T168" id="Seg_1884" n="e" s="T167">"Kün </ts>
               <ts e="T169" id="Seg_1886" n="e" s="T168">u͡ota </ts>
               <ts e="T170" id="Seg_1888" n="e" s="T169">bert </ts>
               <ts e="T171" id="Seg_1890" n="e" s="T170">da </ts>
               <ts e="T172" id="Seg_1892" n="e" s="T171">bert", </ts>
               <ts e="T173" id="Seg_1894" n="e" s="T172">diːr. </ts>
               <ts e="T174" id="Seg_1896" n="e" s="T173">"Minigin </ts>
               <ts e="T175" id="Seg_1898" n="e" s="T174">uːlarɨmɨ͡aktaːgar </ts>
               <ts e="T176" id="Seg_1900" n="e" s="T175">bu͡olu͡ok </ts>
               <ts e="T177" id="Seg_1902" n="e" s="T176">eŋin-eŋitteri </ts>
               <ts e="T178" id="Seg_1904" n="e" s="T177">uːlaraːččɨ", </ts>
               <ts e="T179" id="Seg_1906" n="e" s="T178">diːr. </ts>
               <ts e="T180" id="Seg_1908" n="e" s="T179">"Küːn! </ts>
               <ts e="T181" id="Seg_1910" n="e" s="T180">Berkin </ts>
               <ts e="T182" id="Seg_1912" n="e" s="T181">du͡o?" </ts>
               <ts e="T183" id="Seg_1914" n="e" s="T182">"Dʼe </ts>
               <ts e="T184" id="Seg_1916" n="e" s="T183">berpin </ts>
               <ts e="T185" id="Seg_1918" n="e" s="T184">da, </ts>
               <ts e="T186" id="Seg_1920" n="e" s="T185">berpi͡en", </ts>
               <ts e="T187" id="Seg_1922" n="e" s="T186">diːr. </ts>
               <ts e="T188" id="Seg_1924" n="e" s="T187">"Kaːrɨ </ts>
               <ts e="T189" id="Seg_1926" n="e" s="T188">da, </ts>
               <ts e="T190" id="Seg_1928" n="e" s="T189">buːhu </ts>
               <ts e="T191" id="Seg_1930" n="e" s="T190">da </ts>
               <ts e="T192" id="Seg_1932" n="e" s="T191">ordorbokko </ts>
               <ts e="T193" id="Seg_1934" n="e" s="T192">iri͡ereːččibin", </ts>
               <ts e="T194" id="Seg_1936" n="e" s="T193">diːr. </ts>
               <ts e="T195" id="Seg_1938" n="e" s="T194">"Paː, </ts>
               <ts e="T196" id="Seg_1940" n="e" s="T195">onnuk </ts>
               <ts e="T197" id="Seg_1942" n="e" s="T196">bert </ts>
               <ts e="T198" id="Seg_1944" n="e" s="T197">kihi </ts>
               <ts e="T199" id="Seg_1946" n="e" s="T198">keː, </ts>
               <ts e="T200" id="Seg_1948" n="e" s="T199">togo </ts>
               <ts e="T201" id="Seg_1950" n="e" s="T200">taːs </ts>
               <ts e="T202" id="Seg_1952" n="e" s="T201">kaja </ts>
               <ts e="T203" id="Seg_1954" n="e" s="T202">kalkatɨgar </ts>
               <ts e="T204" id="Seg_1956" n="e" s="T203">ketegiger </ts>
               <ts e="T205" id="Seg_1958" n="e" s="T204">kistene </ts>
               <ts e="T206" id="Seg_1960" n="e" s="T205">hɨtaːččɨgɨn", </ts>
               <ts e="T207" id="Seg_1962" n="e" s="T206">diːr. </ts>
               <ts e="T208" id="Seg_1964" n="e" s="T207">"Taːs </ts>
               <ts e="T209" id="Seg_1966" n="e" s="T208">kaja </ts>
               <ts e="T210" id="Seg_1968" n="e" s="T209">bert </ts>
               <ts e="T211" id="Seg_1970" n="e" s="T210">daː, </ts>
               <ts e="T212" id="Seg_1972" n="e" s="T211">bert", </ts>
               <ts e="T213" id="Seg_1974" n="e" s="T212">diːr, </ts>
               <ts e="T214" id="Seg_1976" n="e" s="T213">"minigin </ts>
               <ts e="T215" id="Seg_1978" n="e" s="T214">innibitten </ts>
               <ts e="T216" id="Seg_1980" n="e" s="T215">kalkalaːččɨ", </ts>
               <ts e="T217" id="Seg_1982" n="e" s="T216">diːr. </ts>
               <ts e="T218" id="Seg_1984" n="e" s="T217">"Taːs </ts>
               <ts e="T219" id="Seg_1986" n="e" s="T218">kaja-e! </ts>
               <ts e="T220" id="Seg_1988" n="e" s="T219">Berkin </ts>
               <ts e="T221" id="Seg_1990" n="e" s="T220">du͡o?" </ts>
               <ts e="T222" id="Seg_1992" n="e" s="T221">"Če, </ts>
               <ts e="T223" id="Seg_1994" n="e" s="T222">berpin </ts>
               <ts e="T224" id="Seg_1996" n="e" s="T223">da, </ts>
               <ts e="T225" id="Seg_1998" n="e" s="T224">berpi͡en. </ts>
               <ts e="T226" id="Seg_2000" n="e" s="T225">Künü </ts>
               <ts e="T227" id="Seg_2002" n="e" s="T226">bagas </ts>
               <ts e="T228" id="Seg_2004" n="e" s="T227">kalkalaːččɨbɨn", </ts>
               <ts e="T229" id="Seg_2006" n="e" s="T228">diːr, </ts>
               <ts e="T230" id="Seg_2008" n="e" s="T229">"kɨhɨnɨ </ts>
               <ts e="T231" id="Seg_2010" n="e" s="T230">bɨha </ts>
               <ts e="T232" id="Seg_2012" n="e" s="T231">da </ts>
               <ts e="T233" id="Seg_2014" n="e" s="T232">karaŋa </ts>
               <ts e="T234" id="Seg_2016" n="e" s="T233">oŋoroːččubɨn", </ts>
               <ts e="T235" id="Seg_2018" n="e" s="T234">diːr. </ts>
               <ts e="T236" id="Seg_2020" n="e" s="T235">"Onnuk </ts>
               <ts e="T237" id="Seg_2022" n="e" s="T236">bert </ts>
               <ts e="T238" id="Seg_2024" n="e" s="T237">kihi </ts>
               <ts e="T239" id="Seg_2026" n="e" s="T238">ki͡e, </ts>
               <ts e="T240" id="Seg_2028" n="e" s="T239">togo </ts>
               <ts e="T241" id="Seg_2030" n="e" s="T240">üːteːn </ts>
               <ts e="T242" id="Seg_2032" n="e" s="T241">kutujaktarga </ts>
               <ts e="T243" id="Seg_2034" n="e" s="T242">supturut </ts>
               <ts e="T244" id="Seg_2036" n="e" s="T243">üːtete </ts>
               <ts e="T245" id="Seg_2038" n="e" s="T244">hɨldʼagɨn", </ts>
               <ts e="T246" id="Seg_2040" n="e" s="T245">di͡ebit. </ts>
               <ts e="T247" id="Seg_2042" n="e" s="T246">"Če </ts>
               <ts e="T248" id="Seg_2044" n="e" s="T247">üːteːn </ts>
               <ts e="T249" id="Seg_2046" n="e" s="T248">kutujaktar </ts>
               <ts e="T250" id="Seg_2048" n="e" s="T249">berter </ts>
               <ts e="T251" id="Seg_2050" n="e" s="T250">da </ts>
               <ts e="T252" id="Seg_2052" n="e" s="T251">berteːr, </ts>
               <ts e="T253" id="Seg_2054" n="e" s="T252">bert </ts>
               <ts e="T254" id="Seg_2056" n="e" s="T253">hɨtɨː </ts>
               <ts e="T255" id="Seg_2058" n="e" s="T254">tumsulaːktar", </ts>
               <ts e="T256" id="Seg_2060" n="e" s="T255">diːr. </ts>
               <ts e="T257" id="Seg_2062" n="e" s="T256">"Kutujaktar, </ts>
               <ts e="T258" id="Seg_2064" n="e" s="T257">berkit </ts>
               <ts e="T259" id="Seg_2066" n="e" s="T258">du͡o?" </ts>
               <ts e="T260" id="Seg_2068" n="e" s="T259">"Berpit </ts>
               <ts e="T261" id="Seg_2070" n="e" s="T260">da, </ts>
               <ts e="T262" id="Seg_2072" n="e" s="T261">berpi͡et, </ts>
               <ts e="T263" id="Seg_2074" n="e" s="T262">taːs </ts>
               <ts e="T264" id="Seg_2076" n="e" s="T263">kajanɨ </ts>
               <ts e="T265" id="Seg_2078" n="e" s="T264">kanan </ts>
               <ts e="T266" id="Seg_2080" n="e" s="T265">hanɨːr </ts>
               <ts e="T267" id="Seg_2082" n="e" s="T266">üktüː </ts>
               <ts e="T268" id="Seg_2084" n="e" s="T267">taksaːččɨbɨt", </ts>
               <ts e="T269" id="Seg_2086" n="e" s="T268">diː. </ts>
               <ts e="T270" id="Seg_2088" n="e" s="T269">"Onnuk </ts>
               <ts e="T271" id="Seg_2090" n="e" s="T270">bert </ts>
               <ts e="T272" id="Seg_2092" n="e" s="T271">kihiler </ts>
               <ts e="T273" id="Seg_2094" n="e" s="T272">ki͡e </ts>
               <ts e="T274" id="Seg_2096" n="e" s="T273">togo </ts>
               <ts e="T275" id="Seg_2098" n="e" s="T274">kɨrsaga </ts>
               <ts e="T276" id="Seg_2100" n="e" s="T275">hi͡eteːččigitij", </ts>
               <ts e="T277" id="Seg_2102" n="e" s="T276">di͡ebit. </ts>
               <ts e="T278" id="Seg_2104" n="e" s="T277">"Kɨrsa, </ts>
               <ts e="T279" id="Seg_2106" n="e" s="T278">kɨrsa </ts>
               <ts e="T280" id="Seg_2108" n="e" s="T279">hi͡emne", </ts>
               <ts e="T281" id="Seg_2110" n="e" s="T280">diːr, </ts>
               <ts e="T282" id="Seg_2112" n="e" s="T281">"bihigini, </ts>
               <ts e="T283" id="Seg_2114" n="e" s="T282">hi͡ečči </ts>
               <ts e="T284" id="Seg_2116" n="e" s="T283">da </ts>
               <ts e="T285" id="Seg_2118" n="e" s="T284">hi͡ečči </ts>
               <ts e="T286" id="Seg_2120" n="e" s="T285">kɨrsa". </ts>
               <ts e="T287" id="Seg_2122" n="e" s="T286">"Kɨrsaː, </ts>
               <ts e="T288" id="Seg_2124" n="e" s="T287">berkin </ts>
               <ts e="T289" id="Seg_2126" n="e" s="T288">du͡o?" </ts>
               <ts e="T290" id="Seg_2128" n="e" s="T289">"Berpin </ts>
               <ts e="T291" id="Seg_2130" n="e" s="T290">da, </ts>
               <ts e="T292" id="Seg_2132" n="e" s="T291">berpi͡en", </ts>
               <ts e="T293" id="Seg_2134" n="e" s="T292">diːr. </ts>
               <ts e="T294" id="Seg_2136" n="e" s="T293">"Kutujagɨ </ts>
               <ts e="T295" id="Seg_2138" n="e" s="T294">kantan </ts>
               <ts e="T296" id="Seg_2140" n="e" s="T295">hanɨːr </ts>
               <ts e="T297" id="Seg_2142" n="e" s="T296">kahan </ts>
               <ts e="T298" id="Seg_2144" n="e" s="T297">ɨlan </ts>
               <ts e="T299" id="Seg_2146" n="e" s="T298">hi͡eččibit". </ts>
               <ts e="T300" id="Seg_2148" n="e" s="T299">"Onnuk </ts>
               <ts e="T301" id="Seg_2150" n="e" s="T300">bert </ts>
               <ts e="T302" id="Seg_2152" n="e" s="T301">kihi </ts>
               <ts e="T303" id="Seg_2154" n="e" s="T302">ki͡e </ts>
               <ts e="T304" id="Seg_2156" n="e" s="T303">čaːrkaːnnaːk </ts>
               <ts e="T305" id="Seg_2158" n="e" s="T304">saka </ts>
               <ts e="T306" id="Seg_2160" n="e" s="T305">ogolorgo </ts>
               <ts e="T307" id="Seg_2162" n="e" s="T306">togo </ts>
               <ts e="T308" id="Seg_2164" n="e" s="T307">kaptaraːččɨgɨnɨj", </ts>
               <ts e="T309" id="Seg_2166" n="e" s="T308">di͡ebit. </ts>
               <ts e="T310" id="Seg_2168" n="e" s="T309">"Čaːrkaːnnaːk </ts>
               <ts e="T311" id="Seg_2170" n="e" s="T310">saka </ts>
               <ts e="T312" id="Seg_2172" n="e" s="T311">ogoloro </ts>
               <ts e="T313" id="Seg_2174" n="e" s="T312">kaːr </ts>
               <ts e="T314" id="Seg_2176" n="e" s="T313">annɨgar </ts>
               <ts e="T315" id="Seg_2178" n="e" s="T314">kömpütterin </ts>
               <ts e="T316" id="Seg_2180" n="e" s="T315">kantan </ts>
               <ts e="T317" id="Seg_2182" n="e" s="T316">bili͡emij </ts>
               <ts e="T318" id="Seg_2184" n="e" s="T317">kapkaːnɨ", </ts>
               <ts e="T319" id="Seg_2186" n="e" s="T318">diːr. </ts>
               <ts e="T320" id="Seg_2188" n="e" s="T319">"Ol </ts>
               <ts e="T321" id="Seg_2190" n="e" s="T320">ihin </ts>
               <ts e="T322" id="Seg_2192" n="e" s="T321">tuttaraːččɨbɨn", </ts>
               <ts e="T323" id="Seg_2194" n="e" s="T322">diːr, </ts>
               <ts e="T324" id="Seg_2196" n="e" s="T323">"ahɨː </ts>
               <ts e="T325" id="Seg_2198" n="e" s="T324">kelemmin", </ts>
               <ts e="T326" id="Seg_2200" n="e" s="T325">diːr. </ts>
               <ts e="T327" id="Seg_2202" n="e" s="T326">"Čaːrkaːnnaːk </ts>
               <ts e="T328" id="Seg_2204" n="e" s="T327">saka </ts>
               <ts e="T329" id="Seg_2206" n="e" s="T328">ogoloro, </ts>
               <ts e="T330" id="Seg_2208" n="e" s="T329">berkit </ts>
               <ts e="T331" id="Seg_2210" n="e" s="T330">duː?" </ts>
               <ts e="T332" id="Seg_2212" n="e" s="T331">"Berpit </ts>
               <ts e="T333" id="Seg_2214" n="e" s="T332">da </ts>
               <ts e="T334" id="Seg_2216" n="e" s="T333">berpi͡et, </ts>
               <ts e="T335" id="Seg_2218" n="e" s="T334">kɨrsanɨ </ts>
               <ts e="T336" id="Seg_2220" n="e" s="T335">kaččaga </ts>
               <ts e="T337" id="Seg_2222" n="e" s="T336">hanɨːr </ts>
               <ts e="T338" id="Seg_2224" n="e" s="T337">kabaːččɨbɨt." </ts>
               <ts e="T339" id="Seg_2226" n="e" s="T338">"Onnuk </ts>
               <ts e="T340" id="Seg_2228" n="e" s="T339">bert </ts>
               <ts e="T341" id="Seg_2230" n="e" s="T340">kihiler </ts>
               <ts e="T342" id="Seg_2232" n="e" s="T341">ke </ts>
               <ts e="T343" id="Seg_2234" n="e" s="T342">togo </ts>
               <ts e="T344" id="Seg_2236" n="e" s="T343">ojuŋŋa </ts>
               <ts e="T345" id="Seg_2238" n="e" s="T344">si͡eteːččigitij", </ts>
               <ts e="T346" id="Seg_2240" n="e" s="T345">di͡ebit. </ts>
               <ts e="T347" id="Seg_2242" n="e" s="T346">"Ojun </ts>
               <ts e="T348" id="Seg_2244" n="e" s="T347">hürdeːk </ts>
               <ts e="T349" id="Seg_2246" n="e" s="T348">bu͡olla", </ts>
               <ts e="T350" id="Seg_2248" n="e" s="T349">di͡ebit, </ts>
               <ts e="T351" id="Seg_2250" n="e" s="T350">"hi͡em, </ts>
               <ts e="T352" id="Seg_2252" n="e" s="T351">di͡ege </ts>
               <ts e="T353" id="Seg_2254" n="e" s="T352">da, </ts>
               <ts e="T354" id="Seg_2256" n="e" s="T353">hi͡ečči", </ts>
               <ts e="T355" id="Seg_2258" n="e" s="T354">diː. </ts>
               <ts e="T356" id="Seg_2260" n="e" s="T355">"Ojuːn! </ts>
               <ts e="T357" id="Seg_2262" n="e" s="T356">Berkin </ts>
               <ts e="T358" id="Seg_2264" n="e" s="T357">du͡o?" </ts>
               <ts e="T359" id="Seg_2266" n="e" s="T358">"Berpin </ts>
               <ts e="T360" id="Seg_2268" n="e" s="T359">da, </ts>
               <ts e="T361" id="Seg_2270" n="e" s="T360">berpi͡en. </ts>
               <ts e="T362" id="Seg_2272" n="e" s="T361">Kaččaga </ts>
               <ts e="T363" id="Seg_2274" n="e" s="T362">hanaːtɨm </ts>
               <ts e="T364" id="Seg_2276" n="e" s="T363">daː </ts>
               <ts e="T365" id="Seg_2278" n="e" s="T364">hi͡eččibin, </ts>
               <ts e="T366" id="Seg_2280" n="e" s="T365">kuhagannɨk </ts>
               <ts e="T367" id="Seg_2282" n="e" s="T366">haŋarbɨt </ts>
               <ts e="T368" id="Seg_2284" n="e" s="T367">kihi </ts>
               <ts e="T369" id="Seg_2286" n="e" s="T368">bu͡olu͡o </ts>
               <ts e="T370" id="Seg_2288" n="e" s="T369">da", </ts>
               <ts e="T371" id="Seg_2290" n="e" s="T370">diːr. </ts>
               <ts e="T372" id="Seg_2292" n="e" s="T371">"Onnuk </ts>
               <ts e="T373" id="Seg_2294" n="e" s="T372">bert </ts>
               <ts e="T374" id="Seg_2296" n="e" s="T373">kihi </ts>
               <ts e="T375" id="Seg_2298" n="e" s="T374">ki͡e </ts>
               <ts e="T376" id="Seg_2300" n="e" s="T375">togo </ts>
               <ts e="T377" id="Seg_2302" n="e" s="T376">himi͡ertke </ts>
               <ts e="T378" id="Seg_2304" n="e" s="T377">hi͡eteːččiginij", </ts>
               <ts e="T379" id="Seg_2306" n="e" s="T378">diːr. </ts>
               <ts e="T380" id="Seg_2308" n="e" s="T379">"Himi͡ert </ts>
               <ts e="T381" id="Seg_2310" n="e" s="T380">baː </ts>
               <ts e="T382" id="Seg_2312" n="e" s="T381">kimi͡eke </ts>
               <ts e="T383" id="Seg_2314" n="e" s="T382">da </ts>
               <ts e="T384" id="Seg_2316" n="e" s="T383">tɨllaːččɨta </ts>
               <ts e="T385" id="Seg_2318" n="e" s="T384">hu͡ok", </ts>
               <ts e="T386" id="Seg_2320" n="e" s="T385">di͡ebit. </ts>
               <ts e="T387" id="Seg_2322" n="e" s="T386">"Himi͡ert! </ts>
               <ts e="T388" id="Seg_2324" n="e" s="T387">Berkin </ts>
               <ts e="T389" id="Seg_2326" n="e" s="T388">du͡o", </ts>
               <ts e="T390" id="Seg_2328" n="e" s="T389">diːr. </ts>
               <ts e="T391" id="Seg_2330" n="e" s="T390">"Berpin </ts>
               <ts e="T392" id="Seg_2332" n="e" s="T391">da, </ts>
               <ts e="T393" id="Seg_2334" n="e" s="T392">berpin, </ts>
               <ts e="T394" id="Seg_2336" n="e" s="T393">enigin </ts>
               <ts e="T395" id="Seg_2338" n="e" s="T394">da </ts>
               <ts e="T396" id="Seg_2340" n="e" s="T395">hi͡em", </ts>
               <ts e="T397" id="Seg_2342" n="e" s="T396">di͡ebite, </ts>
               <ts e="T398" id="Seg_2344" n="e" s="T397">hi͡en </ts>
               <ts e="T399" id="Seg_2346" n="e" s="T398">keːspite. </ts>
               <ts e="T400" id="Seg_2348" n="e" s="T399">Dʼe </ts>
               <ts e="T401" id="Seg_2350" n="e" s="T400">iti </ts>
               <ts e="T402" id="Seg_2352" n="e" s="T401">elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T11" id="Seg_2353" s="T0">UkET_19940424_OldWomanTaal_flk.001 (001.001)</ta>
            <ta e="T23" id="Seg_2354" s="T11">UkET_19940424_OldWomanTaal_flk.002 (001.002)</ta>
            <ta e="T27" id="Seg_2355" s="T23">UkET_19940424_OldWomanTaal_flk.003 (001.003)</ta>
            <ta e="T35" id="Seg_2356" s="T27">UkET_19940424_OldWomanTaal_flk.004 (001.004)</ta>
            <ta e="T39" id="Seg_2357" s="T35">UkET_19940424_OldWomanTaal_flk.005 (001.005)</ta>
            <ta e="T49" id="Seg_2358" s="T39">UkET_19940424_OldWomanTaal_flk.006 (001.006)</ta>
            <ta e="T61" id="Seg_2359" s="T49">UkET_19940424_OldWomanTaal_flk.007 (001.007)</ta>
            <ta e="T72" id="Seg_2360" s="T61">UkET_19940424_OldWomanTaal_flk.008 (001.008)</ta>
            <ta e="T82" id="Seg_2361" s="T72">UkET_19940424_OldWomanTaal_flk.009 (001.009)</ta>
            <ta e="T98" id="Seg_2362" s="T82">UkET_19940424_OldWomanTaal_flk.010 (001.010)</ta>
            <ta e="T110" id="Seg_2363" s="T98">UkET_19940424_OldWomanTaal_flk.011 (001.011)</ta>
            <ta e="T115" id="Seg_2364" s="T110">UkET_19940424_OldWomanTaal_flk.012 (001.012)</ta>
            <ta e="T124" id="Seg_2365" s="T115">UkET_19940424_OldWomanTaal_flk.013 (001.013)</ta>
            <ta e="T134" id="Seg_2366" s="T124">UkET_19940424_OldWomanTaal_flk.014 (001.014)</ta>
            <ta e="T139" id="Seg_2367" s="T134">UkET_19940424_OldWomanTaal_flk.015 (001.015)</ta>
            <ta e="T144" id="Seg_2368" s="T139">UkET_19940424_OldWomanTaal_flk.016 (001.016)</ta>
            <ta e="T153" id="Seg_2369" s="T144">UkET_19940424_OldWomanTaal_flk.017 (001.018)</ta>
            <ta e="T167" id="Seg_2370" s="T153">UkET_19940424_OldWomanTaal_flk.018 (001.019)</ta>
            <ta e="T173" id="Seg_2371" s="T167">UkET_19940424_OldWomanTaal_flk.019 (001.021)</ta>
            <ta e="T179" id="Seg_2372" s="T173">UkET_19940424_OldWomanTaal_flk.020 (001.022)</ta>
            <ta e="T180" id="Seg_2373" s="T179">UkET_19940424_OldWomanTaal_flk.021 (001.023)</ta>
            <ta e="T182" id="Seg_2374" s="T180">UkET_19940424_OldWomanTaal_flk.022 (001.024)</ta>
            <ta e="T187" id="Seg_2375" s="T182">UkET_19940424_OldWomanTaal_flk.023 (001.025)</ta>
            <ta e="T194" id="Seg_2376" s="T187">UkET_19940424_OldWomanTaal_flk.024 (001.026)</ta>
            <ta e="T207" id="Seg_2377" s="T194">UkET_19940424_OldWomanTaal_flk.025 (001.027)</ta>
            <ta e="T217" id="Seg_2378" s="T207">UkET_19940424_OldWomanTaal_flk.026 (001.029)</ta>
            <ta e="T219" id="Seg_2379" s="T217">UkET_19940424_OldWomanTaal_flk.027 (001.030)</ta>
            <ta e="T221" id="Seg_2380" s="T219">UkET_19940424_OldWomanTaal_flk.028 (001.031)</ta>
            <ta e="T225" id="Seg_2381" s="T221">UkET_19940424_OldWomanTaal_flk.029 (001.032)</ta>
            <ta e="T235" id="Seg_2382" s="T225">UkET_19940424_OldWomanTaal_flk.030 (001.033)</ta>
            <ta e="T246" id="Seg_2383" s="T235">UkET_19940424_OldWomanTaal_flk.031 (001.034)</ta>
            <ta e="T256" id="Seg_2384" s="T246">UkET_19940424_OldWomanTaal_flk.032 (001.036)</ta>
            <ta e="T259" id="Seg_2385" s="T256">UkET_19940424_OldWomanTaal_flk.033 (001.037)</ta>
            <ta e="T269" id="Seg_2386" s="T259">UkET_19940424_OldWomanTaal_flk.034 (001.038)</ta>
            <ta e="T277" id="Seg_2387" s="T269">UkET_19940424_OldWomanTaal_flk.035 (001.039)</ta>
            <ta e="T286" id="Seg_2388" s="T277">UkET_19940424_OldWomanTaal_flk.036 (001.041)</ta>
            <ta e="T289" id="Seg_2389" s="T286">UkET_19940424_OldWomanTaal_flk.037 (001.042)</ta>
            <ta e="T293" id="Seg_2390" s="T289">UkET_19940424_OldWomanTaal_flk.038 (001.043)</ta>
            <ta e="T299" id="Seg_2391" s="T293">UkET_19940424_OldWomanTaal_flk.039 (001.044)</ta>
            <ta e="T309" id="Seg_2392" s="T299">UkET_19940424_OldWomanTaal_flk.040 (001.045)</ta>
            <ta e="T319" id="Seg_2393" s="T309">UkET_19940424_OldWomanTaal_flk.041 (001.047)</ta>
            <ta e="T326" id="Seg_2394" s="T319">UkET_19940424_OldWomanTaal_flk.042 (001.049)</ta>
            <ta e="T331" id="Seg_2395" s="T326">UkET_19940424_OldWomanTaal_flk.043 (001.050)</ta>
            <ta e="T338" id="Seg_2396" s="T331">UkET_19940424_OldWomanTaal_flk.044 (001.051)</ta>
            <ta e="T346" id="Seg_2397" s="T338">UkET_19940424_OldWomanTaal_flk.045 (001.052)</ta>
            <ta e="T355" id="Seg_2398" s="T346">UkET_19940424_OldWomanTaal_flk.046 (001.054)</ta>
            <ta e="T356" id="Seg_2399" s="T355">UkET_19940424_OldWomanTaal_flk.047 (001.055)</ta>
            <ta e="T358" id="Seg_2400" s="T356">UkET_19940424_OldWomanTaal_flk.048 (001.056)</ta>
            <ta e="T361" id="Seg_2401" s="T358">UkET_19940424_OldWomanTaal_flk.049 (001.057)</ta>
            <ta e="T371" id="Seg_2402" s="T361">UkET_19940424_OldWomanTaal_flk.050 (001.058)</ta>
            <ta e="T379" id="Seg_2403" s="T371">UkET_19940424_OldWomanTaal_flk.051 (001.059)</ta>
            <ta e="T386" id="Seg_2404" s="T379">UkET_19940424_OldWomanTaal_flk.052 (001.061)</ta>
            <ta e="T387" id="Seg_2405" s="T386">UkET_19940424_OldWomanTaal_flk.053 (001.062)</ta>
            <ta e="T390" id="Seg_2406" s="T387">UkET_19940424_OldWomanTaal_flk.054 (001.063)</ta>
            <ta e="T399" id="Seg_2407" s="T390">UkET_19940424_OldWomanTaal_flk.055 (001.065)</ta>
            <ta e="T402" id="Seg_2408" s="T399">UkET_19940424_OldWomanTaal_flk.056 (001.066)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T11" id="Seg_2409" s="T0">ЕТ: Таал эмиэксин олорбут, үһү, ойулаак отток орто тогус дойдуга – Таал эмээксин.</ta>
            <ta e="T23" id="Seg_2410" s="T11">ЕТ: Бу Таал эмээксин буоллагына биирдэ киэһэннэн, кайа, чаай иһинэн-иһинэн бараан утуйан.. </ta>
            <ta e="T27" id="Seg_2411" s="T23">ЕТ: утуйбут дуу, кайдак дуу?</ta>
            <ta e="T35" id="Seg_2412" s="T27">Улакаата тэстибитэ дуу, һамыыр түспүт дуу, туок дуу?</ta>
            <ta e="T39" id="Seg_2413" s="T35">Тэллэгэ илийбит эмиэксин гиэнэ.</ta>
            <ta e="T49" id="Seg_2414" s="T39">ЕТ: Бу тэллэгэ илийэн, эмээксин һарсиэрда күһүн эбит, буус тоӈорун һагына.</ta>
            <ta e="T61" id="Seg_2415" s="T49">ЕТ: Эмиэксин туран, оччого туок уота кэлиэй, илэ уоттаак буолуоктарай – ачаак буолла онтон. </ta>
            <ta e="T72" id="Seg_2416" s="T61">ЕТ: Ол иһин эни, таһаара күннээк багай күӈӈэ араӈаһыгар тэллэгин ыйан кээспит.</ta>
            <ta e="T82" id="Seg_2417" s="T72">ЕТ: Кайа, тэллэгэ буолла, кайа, чаайын иһэ олордогуна, "суук" гыммыта буллагына.</ta>
            <ta e="T98" id="Seg_2418" s="T82">Такса көппүтэ эмээксин, такса көппүтэ буоллагына, тэллэгэ көтөн буоллагына күөлүӈ .. уу баһынар көлүйэтин кырыытыгар түспүт.</ta>
            <ta e="T110" id="Seg_2419" s="T98">ЕТ: Кайа-а, эмиэксин буоллагына баран кабан ылан, "тэллэппин кабан ыламмын эгэлиэм" диэбит эт.</ta>
            <ta e="T115" id="Seg_2420" s="T110">ЕТ: Тыала "һүүк" гыннаран бууска киирбит.</ta>
            <ta e="T124" id="Seg_2421" s="T115">ЕТ: Кайа, күһүн кэ чэлгийэн турар бууска бу эмээксин киирбит. </ta>
            <ta e="T134" id="Seg_2422" s="T124">ЕТ: Муну батан һүүрэбин диэн калтырыйан түһэн, өттүгүн булгу түспүт эмээксин. </ta>
            <ta e="T139" id="Seg_2423" s="T134">ЕТ: Кайа, эмээксин буо диэн буолар:</ta>
            <ta e="T153" id="Seg_2424" s="T144">ЕТ: "Э-э дэ бэртпин, да бэртпин. Эн өттүккүн тоһуттум", – диэбит. </ta>
            <ta e="T167" id="Seg_2425" s="T153">ЕТ: "Оннук бэрт киһи киэ того һаас буолуо даа, күн уотуттан уулан ньолбойон түһээччигиний?" – диэбит.</ta>
            <ta e="T173" id="Seg_2426" s="T167">ЕТ: "Күн уота бэрт да бэрт, –диир, </ta>
            <ta e="T179" id="Seg_2427" s="T173">– минигин улларымактаагар буолуок эӈин-эӈиттэри улларааччы, – диир.</ta>
            <ta e="T180" id="Seg_2428" s="T179">ET: "Күүн! </ta>
            <ta e="T182" id="Seg_2429" s="T180">Бэрткин дуо?"</ta>
            <ta e="T187" id="Seg_2430" s="T182">ЕТ: "Дьэ бэртпин да, бэртпиэн, – диир </ta>
            <ta e="T194" id="Seg_2431" s="T187">– каары да, бууһу да ордорбокко ириэрээччибин", – диир.</ta>
            <ta e="T207" id="Seg_2432" s="T194">ЕТ: "Па-а, оннук бэрт киһи кээ, того таас кайа калкатыгар кэтэгигэр кистэнэ һытааччыгын?" – диир.</ta>
            <ta e="T217" id="Seg_2433" s="T207">ET: "Таас кайа бэрт даа, бэрт, – диир, – минигин иннибиттэн калкалааччы, – диир. </ta>
            <ta e="T219" id="Seg_2434" s="T217">ЕТ: "Таас кайа-э! </ta>
            <ta e="T221" id="Seg_2435" s="T219">Бэрткин дуо?"</ta>
            <ta e="T225" id="Seg_2436" s="T221">ЕТ: "Чэ, бертпин да, бэртпиэн".</ta>
            <ta e="T235" id="Seg_2437" s="T225">ЕТ: "Күнү багас калкалааччыбын, -- диир. Кыһыны быһа да караӈа оӈороччубын", -- диир.</ta>
            <ta e="T246" id="Seg_2438" s="T235">ЕТ: "Оннук бэрт киһи киэ, того үүтээн кутуйакктарга суптурут үүттэтэ һылдьагын?", – диэбит.</ta>
            <ta e="T256" id="Seg_2439" s="T246">"Чэ үүтээн кутуйактар бэртэр да бэртээр. Бэрт һытыы тумсулаактар", – диир. </ta>
            <ta e="T259" id="Seg_2440" s="T256">ЕТ: "Кутуйактар, бэрткит дуо?"</ta>
            <ta e="T269" id="Seg_2441" s="T259">ЕТ: "Бэртпит да, бэртпиэт, таас кайаны канан һаныыр үктүү таксааччыбыт", – дии.</ta>
            <ta e="T277" id="Seg_2442" s="T269">ЕТ: "Оннук бэрт киһилэр киэ то(того) кырсага һиэтээччигитий?" – диэбит.</ta>
            <ta e="T286" id="Seg_2443" s="T277">ЕТ: "Кырса, кырса һиэмнэ, – диир, – биһигини. Һиэччи да һиэччи кырса".</ta>
            <ta e="T289" id="Seg_2444" s="T286">ЕТ: "Кырсэ-э, бэрткин дуо?"</ta>
            <ta e="T293" id="Seg_2445" s="T289">ЕТ: "Бэртпин да, бэртпиэн", – диир.</ta>
            <ta e="T299" id="Seg_2446" s="T293">ЕТ: "Кутуйагы кантан һаныыр каһан ылан һиэччибит".</ta>
            <ta e="T309" id="Seg_2447" s="T299">ЕТ: "Оннук бэрт киһи киэ чаркааннаак сака оголорго того каптарааччыгыный?" – диэбит.</ta>
            <ta e="T319" id="Seg_2448" s="T309">ЕТ: "Чааркааннаак сака оголоро каар анныгар көмпүттэрин кантан билиэмий (капкааны?), – диир.</ta>
            <ta e="T326" id="Seg_2449" s="T319">– Ол иһин туттарааччыбын, – диир, – аһы кэлэммин", – диир.</ta>
            <ta e="T331" id="Seg_2450" s="T326">ЕТ: "Чарканнаак сака оголоро, бэркит дуо?"</ta>
            <ta e="T338" id="Seg_2451" s="T331">ЕТ: "Бэрпит да бэрпээт, кырсаны каччага һаныыр кабааччыбыт," – дии.</ta>
            <ta e="T346" id="Seg_2452" s="T338">ЕТ: "Оннук бэрт киһилэр киэ того ойуӈӈа сиэтээччигитий(һиэтэччигитий)? – диэбит.</ta>
            <ta e="T355" id="Seg_2453" s="T346">ЕТ: "Ойун һүрдээк буолла, – диэбит, – һиэм, диэгэ да, һиэччи".</ta>
            <ta e="T356" id="Seg_2454" s="T355">ЕТ: "Ойуун! </ta>
            <ta e="T358" id="Seg_2455" s="T356">Бэрткин дуо?"</ta>
            <ta e="T361" id="Seg_2456" s="T358">ЕТ: "Бэртпин да, бэртпиэн.</ta>
            <ta e="T371" id="Seg_2457" s="T361">ЕТ: Каччага һанаатым даа һиэччибин, куһаганнык һаӈарбыт киһи буолуо да", – диир.</ta>
            <ta e="T379" id="Seg_2458" s="T371">ЕТ: "Оннук бэрт киһи киэ того һимиэрткэ һиэтээччигиний? – диир.</ta>
            <ta e="T386" id="Seg_2459" s="T379">ЕТ: "Һимиэрт баа(багас) кимэкэ да тыллааччыта һуок", – диэбит.</ta>
            <ta e="T387" id="Seg_2460" s="T386">"Һимиэ-э-эрт! </ta>
            <ta e="T390" id="Seg_2461" s="T387">– диир.</ta>
            <ta e="T399" id="Seg_2462" s="T390">ЕТ: "Бэртпин да, бэртпин, энигин да һиэм", – диэбитэ, һиэн кээспитэ.</ta>
            <ta e="T402" id="Seg_2463" s="T399">ЕТ: Дьэ ити элэтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T11" id="Seg_2464" s="T0">Taːl emeːksin olorbut, ühü, ojulaːk ottoːk orto togus dojduga – Taːl emeːksin. </ta>
            <ta e="T23" id="Seg_2465" s="T11">Bu Taːl emeːksin bu͡ollagɨna biːrde ki͡ehennen, kaja, čaːj ihinen-ihinen baraːn utujan… </ta>
            <ta e="T27" id="Seg_2466" s="T23">Utujbut duː, kajdak duː? </ta>
            <ta e="T35" id="Seg_2467" s="T27">Ulakaːta testibite duː, hamɨːr tüspüt duː, tu͡ok duː? </ta>
            <ta e="T39" id="Seg_2468" s="T35">Tellege ilijbit emi͡eksin gi͡ene. </ta>
            <ta e="T49" id="Seg_2469" s="T39">Bu tellege ilijen, emeːksin harsi͡erda kühün ebit, buːs toŋorun hagɨna. </ta>
            <ta e="T61" id="Seg_2470" s="T49">Emeːksin turan, oččogo tu͡ok u͡ota keli͡ej, ile u͡ottaːk bu͡olu͡oktaraj – ačaːk bu͡olla onton. </ta>
            <ta e="T72" id="Seg_2471" s="T61">Ol ihin eni, tahaːra künneːk bagaj küŋŋe araŋahɨgar tellegin ɨjaːn keːspit. </ta>
            <ta e="T82" id="Seg_2472" s="T72">Kaja, tellege bu͡olla, kaja, čaːjɨn ihe olordoguna, "suːk" gɨmmɨta bu͡ollagɨna. </ta>
            <ta e="T98" id="Seg_2473" s="T82">Taksa köppüte emeːksin, taksa köppüte bu͡ollagɨna, tellege kötön bu͡ollagɨna kü͡ölüŋ ((…)) uː bahɨnar kölüjetin kɨrɨːtɨgar tüspüt. </ta>
            <ta e="T110" id="Seg_2474" s="T98">Kajaː, emeːksin bu͡ollagɨna baran kaban ɨlan, "telleppin kaban ɨlammɨn egeli͡em" di͡ebit eːt. </ta>
            <ta e="T115" id="Seg_2475" s="T110">Tɨ͡ala "hüːk" gɨnnaran buːska kiːrbit. </ta>
            <ta e="T124" id="Seg_2476" s="T115">Kaja, kühün ke čelgien turar buːska bu emeːksin kiːrbit. </ta>
            <ta e="T134" id="Seg_2477" s="T124">Munu batan hüːrebin di͡en kaltɨrɨjan tühen, öttügün bulgu tüspüt emeːksin. </ta>
            <ta e="T139" id="Seg_2478" s="T134">Kaja, emeːksin bu͡o di͡en bu͡olar: </ta>
            <ta e="T144" id="Seg_2479" s="T139">"Buːs, buːs berkin duː", diːr. </ta>
            <ta e="T153" id="Seg_2480" s="T144">"Eː dʼe berpin, de berpin, en öttükkün tohuttum", di͡en. </ta>
            <ta e="T167" id="Seg_2481" s="T153">"Onnuk bert kihi ki͡e togo haːs bu͡olu͡o daː, kün u͡otuttan uːlan nʼolbojon tüheːččiginij", di͡ebit. </ta>
            <ta e="T173" id="Seg_2482" s="T167">"Kün u͡ota bert da bert", diːr. </ta>
            <ta e="T179" id="Seg_2483" s="T173">"Minigin uːlarɨmɨ͡aktaːgar bu͡olu͡ok eŋin-eŋitteri uːlaraːččɨ", diːr. </ta>
            <ta e="T180" id="Seg_2484" s="T179">"Küːn! </ta>
            <ta e="T182" id="Seg_2485" s="T180">Berkin du͡o?" </ta>
            <ta e="T187" id="Seg_2486" s="T182">"Dʼe berpin da, berpi͡en", diːr. </ta>
            <ta e="T194" id="Seg_2487" s="T187">"Kaːrɨ da, buːhu da ordorbokko iri͡ereːččibin", diːr. </ta>
            <ta e="T207" id="Seg_2488" s="T194">"Paː, onnuk bert kihi keː, togo taːs kaja kalkatɨgar ketegiger kistene hɨtaːččɨgɨn", diːr. </ta>
            <ta e="T217" id="Seg_2489" s="T207">"Taːs kaja bert daː, bert", diːr, "minigin innibitten kalkalaːččɨ", diːr. </ta>
            <ta e="T219" id="Seg_2490" s="T217">"Taːs kaja-e! </ta>
            <ta e="T221" id="Seg_2491" s="T219">Berkin du͡o?" </ta>
            <ta e="T225" id="Seg_2492" s="T221">"Če, berpin da, berpi͡en. </ta>
            <ta e="T235" id="Seg_2493" s="T225">Künü bagas kalkalaːččɨbɨn", diːr, "kɨhɨnɨ bɨha da karaŋa oŋoroːččubɨn", diːr. </ta>
            <ta e="T246" id="Seg_2494" s="T235">"Onnuk bert kihi ki͡e, togo üːteːn kutujaktarga supturut üːtete hɨldʼagɨn", di͡ebit. </ta>
            <ta e="T256" id="Seg_2495" s="T246">"Če üːteːn kutujaktar berter da berteːr, bert hɨtɨː tumsulaːktar", diːr. </ta>
            <ta e="T259" id="Seg_2496" s="T256">"Kutujaktar, berkit du͡o?" </ta>
            <ta e="T269" id="Seg_2497" s="T259">"Berpit da, berpi͡et, taːs kajanɨ kanan hanɨːr üktüː taksaːččɨbɨt", diː. </ta>
            <ta e="T277" id="Seg_2498" s="T269">"Onnuk bert kihiler ki͡e togo kɨrsaga hi͡eteːččigitij", di͡ebit. </ta>
            <ta e="T286" id="Seg_2499" s="T277">"Kɨrsa, kɨrsa hi͡emne", diːr, "bihigini, hi͡ečči da hi͡ečči kɨrsa". </ta>
            <ta e="T289" id="Seg_2500" s="T286">"Kɨrsaː, berkin du͡o?" </ta>
            <ta e="T293" id="Seg_2501" s="T289">"Berpin da, berpi͡en", diːr. </ta>
            <ta e="T299" id="Seg_2502" s="T293">"Kutujagɨ kantan hanɨːr kahan ɨlan hi͡eččibit". </ta>
            <ta e="T309" id="Seg_2503" s="T299">"Onnuk bert kihi ki͡e čaːrkaːnnaːk saka ogolorgo togo kaptaraːččɨgɨnɨj", di͡ebit. </ta>
            <ta e="T319" id="Seg_2504" s="T309">"Čaːrkaːnnaːk saka ogoloro kaːr annɨgar kömpütterin kantan bili͡emij kapkaːnɨ", diːr. </ta>
            <ta e="T326" id="Seg_2505" s="T319">"Ol ihin tuttaraːččɨbɨn", diːr, "ahɨː kelemmin", diːr. </ta>
            <ta e="T331" id="Seg_2506" s="T326">"Čaːrkaːnnaːk saka ogoloro, berkit duː?" </ta>
            <ta e="T338" id="Seg_2507" s="T331">"Berpit da berpi͡et, kɨrsanɨ kaččaga hanɨːr kabaːččɨbɨt." </ta>
            <ta e="T346" id="Seg_2508" s="T338">"Onnuk bert kihiler ke togo ojuŋŋa si͡eteːččigitij", di͡ebit. </ta>
            <ta e="T355" id="Seg_2509" s="T346">"Ojun hürdeːk bu͡olla", di͡ebit, "hi͡em, di͡ege da, hi͡ečči", diː. </ta>
            <ta e="T356" id="Seg_2510" s="T355">"Ojuːn! </ta>
            <ta e="T358" id="Seg_2511" s="T356">Berkin du͡o?" </ta>
            <ta e="T361" id="Seg_2512" s="T358">"Berpin da, berpi͡en. </ta>
            <ta e="T371" id="Seg_2513" s="T361">Kaččaga hanaːtɨm daː hi͡eččibin, kuhagannɨk haŋarbɨt kihi bu͡olu͡o da", diːr. </ta>
            <ta e="T379" id="Seg_2514" s="T371">"Onnuk bert kihi ki͡e togo himi͡ertke hi͡eteːččiginij", diːr. </ta>
            <ta e="T386" id="Seg_2515" s="T379">"Himi͡ert baː kimi͡eke da tɨllaːččɨta hu͡ok", di͡ebit. </ta>
            <ta e="T387" id="Seg_2516" s="T386">"Himi͡ert! </ta>
            <ta e="T390" id="Seg_2517" s="T387">Berkin du͡o", diːr. </ta>
            <ta e="T399" id="Seg_2518" s="T390">"Berpin da, berpin, enigin da hi͡em", di͡ebite, hi͡en keːspite. </ta>
            <ta e="T402" id="Seg_2519" s="T399">Dʼe iti elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2520" s="T0">Taːl</ta>
            <ta e="T2" id="Seg_2521" s="T1">emeːksin</ta>
            <ta e="T3" id="Seg_2522" s="T2">olor-but</ta>
            <ta e="T4" id="Seg_2523" s="T3">ühü</ta>
            <ta e="T5" id="Seg_2524" s="T4">oju-laːk</ta>
            <ta e="T6" id="Seg_2525" s="T5">ot-toːk</ta>
            <ta e="T7" id="Seg_2526" s="T6">orto</ta>
            <ta e="T8" id="Seg_2527" s="T7">togus</ta>
            <ta e="T9" id="Seg_2528" s="T8">dojdu-ga</ta>
            <ta e="T10" id="Seg_2529" s="T9">Taːl</ta>
            <ta e="T11" id="Seg_2530" s="T10">emeːksin</ta>
            <ta e="T12" id="Seg_2531" s="T11">bu</ta>
            <ta e="T13" id="Seg_2532" s="T12">Taːl</ta>
            <ta e="T14" id="Seg_2533" s="T13">emeːksin</ta>
            <ta e="T15" id="Seg_2534" s="T14">bu͡ollagɨna</ta>
            <ta e="T16" id="Seg_2535" s="T15">biːrde</ta>
            <ta e="T17" id="Seg_2536" s="T16">ki͡ehe-nnen</ta>
            <ta e="T18" id="Seg_2537" s="T17">kaja</ta>
            <ta e="T19" id="Seg_2538" s="T18">čaːj</ta>
            <ta e="T20" id="Seg_2539" s="T19">ih-i-n-en-ih-i-n-en</ta>
            <ta e="T21" id="Seg_2540" s="T20">baraːn</ta>
            <ta e="T23" id="Seg_2541" s="T21">utuj-an</ta>
            <ta e="T24" id="Seg_2542" s="T23">utuj-but</ta>
            <ta e="T25" id="Seg_2543" s="T24">duː</ta>
            <ta e="T26" id="Seg_2544" s="T25">kajdak</ta>
            <ta e="T27" id="Seg_2545" s="T26">duː</ta>
            <ta e="T28" id="Seg_2546" s="T27">ulakaː-ta</ta>
            <ta e="T29" id="Seg_2547" s="T28">test-i-bit-e</ta>
            <ta e="T30" id="Seg_2548" s="T29">duː</ta>
            <ta e="T31" id="Seg_2549" s="T30">hamɨːr</ta>
            <ta e="T32" id="Seg_2550" s="T31">tüs-püt</ta>
            <ta e="T33" id="Seg_2551" s="T32">duː</ta>
            <ta e="T34" id="Seg_2552" s="T33">tu͡ok</ta>
            <ta e="T35" id="Seg_2553" s="T34">duː</ta>
            <ta e="T36" id="Seg_2554" s="T35">telleg-e</ta>
            <ta e="T37" id="Seg_2555" s="T36">ilij-bit</ta>
            <ta e="T38" id="Seg_2556" s="T37">emi͡eksin</ta>
            <ta e="T39" id="Seg_2557" s="T38">gi͡en-e</ta>
            <ta e="T40" id="Seg_2558" s="T39">bu</ta>
            <ta e="T41" id="Seg_2559" s="T40">telleg-e</ta>
            <ta e="T42" id="Seg_2560" s="T41">ilij-en</ta>
            <ta e="T43" id="Seg_2561" s="T42">emeːksin</ta>
            <ta e="T44" id="Seg_2562" s="T43">harsi͡erda</ta>
            <ta e="T45" id="Seg_2563" s="T44">kühün</ta>
            <ta e="T46" id="Seg_2564" s="T45">e-bit</ta>
            <ta e="T47" id="Seg_2565" s="T46">buːs</ta>
            <ta e="T48" id="Seg_2566" s="T47">toŋ-or-u-n</ta>
            <ta e="T49" id="Seg_2567" s="T48">hagɨna</ta>
            <ta e="T50" id="Seg_2568" s="T49">emeːksin</ta>
            <ta e="T51" id="Seg_2569" s="T50">tur-an</ta>
            <ta e="T52" id="Seg_2570" s="T51">oččogo</ta>
            <ta e="T53" id="Seg_2571" s="T52">tu͡ok</ta>
            <ta e="T54" id="Seg_2572" s="T53">u͡ot-a</ta>
            <ta e="T55" id="Seg_2573" s="T54">kel-i͡e=j</ta>
            <ta e="T56" id="Seg_2574" s="T55">ile</ta>
            <ta e="T57" id="Seg_2575" s="T56">u͡ot-taːk</ta>
            <ta e="T58" id="Seg_2576" s="T57">bu͡ol-u͡ok-tara=j</ta>
            <ta e="T59" id="Seg_2577" s="T58">ačaːk</ta>
            <ta e="T60" id="Seg_2578" s="T59">bu͡ol-l-a</ta>
            <ta e="T61" id="Seg_2579" s="T60">onton</ta>
            <ta e="T62" id="Seg_2580" s="T61">ol</ta>
            <ta e="T63" id="Seg_2581" s="T62">ihin</ta>
            <ta e="T64" id="Seg_2582" s="T63">eni</ta>
            <ta e="T65" id="Seg_2583" s="T64">tahaːra</ta>
            <ta e="T66" id="Seg_2584" s="T65">kün-neːk</ta>
            <ta e="T67" id="Seg_2585" s="T66">bagaj</ta>
            <ta e="T68" id="Seg_2586" s="T67">küŋ-ŋe</ta>
            <ta e="T69" id="Seg_2587" s="T68">araŋah-ɨ-gar</ta>
            <ta e="T70" id="Seg_2588" s="T69">telleg-i-n</ta>
            <ta e="T71" id="Seg_2589" s="T70">ɨjaː-n</ta>
            <ta e="T72" id="Seg_2590" s="T71">keːs-pit</ta>
            <ta e="T73" id="Seg_2591" s="T72">kaja</ta>
            <ta e="T74" id="Seg_2592" s="T73">telleg-e</ta>
            <ta e="T75" id="Seg_2593" s="T74">bu͡olla</ta>
            <ta e="T76" id="Seg_2594" s="T75">kaja</ta>
            <ta e="T77" id="Seg_2595" s="T76">čaːj-ɨ-n</ta>
            <ta e="T78" id="Seg_2596" s="T77">ih-e</ta>
            <ta e="T79" id="Seg_2597" s="T78">olor-dog-una</ta>
            <ta e="T80" id="Seg_2598" s="T79">suːk</ta>
            <ta e="T81" id="Seg_2599" s="T80">gɨm-mɨt-a</ta>
            <ta e="T82" id="Seg_2600" s="T81">bu͡ollagɨna</ta>
            <ta e="T83" id="Seg_2601" s="T82">taks-a</ta>
            <ta e="T84" id="Seg_2602" s="T83">köp-püt-e</ta>
            <ta e="T85" id="Seg_2603" s="T84">emeːksin</ta>
            <ta e="T86" id="Seg_2604" s="T85">taks-a</ta>
            <ta e="T87" id="Seg_2605" s="T86">köp-püt-e</ta>
            <ta e="T88" id="Seg_2606" s="T87">bu͡ollagɨna</ta>
            <ta e="T89" id="Seg_2607" s="T88">telleg-e</ta>
            <ta e="T90" id="Seg_2608" s="T89">köt-ön</ta>
            <ta e="T91" id="Seg_2609" s="T90">bu͡ol-lag-ɨna</ta>
            <ta e="T92" id="Seg_2610" s="T91">kü͡öl-ü-ŋ</ta>
            <ta e="T93" id="Seg_2611" s="T403">uː</ta>
            <ta e="T94" id="Seg_2612" s="T93">bahɨn-ar</ta>
            <ta e="T95" id="Seg_2613" s="T94">kölüje-ti-n</ta>
            <ta e="T96" id="Seg_2614" s="T95">kɨrɨː-tɨ-gar</ta>
            <ta e="T98" id="Seg_2615" s="T96">tüs-püt</ta>
            <ta e="T99" id="Seg_2616" s="T98">kajaː</ta>
            <ta e="T100" id="Seg_2617" s="T99">emeːksin</ta>
            <ta e="T101" id="Seg_2618" s="T100">bu͡ollagɨna</ta>
            <ta e="T102" id="Seg_2619" s="T101">bar-an</ta>
            <ta e="T103" id="Seg_2620" s="T102">kab-an</ta>
            <ta e="T104" id="Seg_2621" s="T103">ɨl-an</ta>
            <ta e="T105" id="Seg_2622" s="T104">tellep-pi-n</ta>
            <ta e="T106" id="Seg_2623" s="T105">kab-an</ta>
            <ta e="T107" id="Seg_2624" s="T106">ɨl-am-mɨn</ta>
            <ta e="T108" id="Seg_2625" s="T107">egel-i͡e-m</ta>
            <ta e="T109" id="Seg_2626" s="T108">di͡e-bit</ta>
            <ta e="T110" id="Seg_2627" s="T109">eːt</ta>
            <ta e="T111" id="Seg_2628" s="T110">tɨ͡al-a</ta>
            <ta e="T112" id="Seg_2629" s="T111">hüːk</ta>
            <ta e="T113" id="Seg_2630" s="T112">gɨn-nar-an</ta>
            <ta e="T114" id="Seg_2631" s="T113">buːs-ka</ta>
            <ta e="T115" id="Seg_2632" s="T114">kiːr-bit</ta>
            <ta e="T116" id="Seg_2633" s="T115">kaja</ta>
            <ta e="T117" id="Seg_2634" s="T116">kühün</ta>
            <ta e="T118" id="Seg_2635" s="T117">ke</ta>
            <ta e="T119" id="Seg_2636" s="T118">čelgien</ta>
            <ta e="T120" id="Seg_2637" s="T119">tur-ar</ta>
            <ta e="T121" id="Seg_2638" s="T120">buːs-ka</ta>
            <ta e="T122" id="Seg_2639" s="T121">bu</ta>
            <ta e="T123" id="Seg_2640" s="T122">emeːksin</ta>
            <ta e="T124" id="Seg_2641" s="T123">kiːr-bit</ta>
            <ta e="T125" id="Seg_2642" s="T124">mu-nu</ta>
            <ta e="T126" id="Seg_2643" s="T125">bat-an</ta>
            <ta e="T127" id="Seg_2644" s="T126">hüːr-e-bin</ta>
            <ta e="T128" id="Seg_2645" s="T127">di͡e-n</ta>
            <ta e="T129" id="Seg_2646" s="T128">kaltɨrɨj-an</ta>
            <ta e="T130" id="Seg_2647" s="T129">tüh-en</ta>
            <ta e="T131" id="Seg_2648" s="T130">öttüg-ü-n</ta>
            <ta e="T132" id="Seg_2649" s="T131">bulgu</ta>
            <ta e="T133" id="Seg_2650" s="T132">tüs-püt</ta>
            <ta e="T134" id="Seg_2651" s="T133">emeːksin</ta>
            <ta e="T135" id="Seg_2652" s="T134">kaja</ta>
            <ta e="T136" id="Seg_2653" s="T135">emeːksin</ta>
            <ta e="T137" id="Seg_2654" s="T136">bu͡o</ta>
            <ta e="T138" id="Seg_2655" s="T137">di͡e-n</ta>
            <ta e="T139" id="Seg_2656" s="T138">bu͡ol-ar</ta>
            <ta e="T140" id="Seg_2657" s="T139">buːs</ta>
            <ta e="T141" id="Seg_2658" s="T140">buːs</ta>
            <ta e="T142" id="Seg_2659" s="T141">ber-kin</ta>
            <ta e="T143" id="Seg_2660" s="T142">duː</ta>
            <ta e="T144" id="Seg_2661" s="T143">diː-r</ta>
            <ta e="T145" id="Seg_2662" s="T144">eː</ta>
            <ta e="T146" id="Seg_2663" s="T145">dʼe</ta>
            <ta e="T147" id="Seg_2664" s="T146">ber-pin</ta>
            <ta e="T148" id="Seg_2665" s="T147">de</ta>
            <ta e="T149" id="Seg_2666" s="T148">ber-pin</ta>
            <ta e="T150" id="Seg_2667" s="T149">en</ta>
            <ta e="T151" id="Seg_2668" s="T150">öttük-kü-n</ta>
            <ta e="T152" id="Seg_2669" s="T151">tohut-tu-m</ta>
            <ta e="T153" id="Seg_2670" s="T152">di͡e-n</ta>
            <ta e="T154" id="Seg_2671" s="T153">onnuk</ta>
            <ta e="T155" id="Seg_2672" s="T154">bert</ta>
            <ta e="T156" id="Seg_2673" s="T155">kihi</ta>
            <ta e="T157" id="Seg_2674" s="T156">ki͡e</ta>
            <ta e="T158" id="Seg_2675" s="T157">togo</ta>
            <ta e="T159" id="Seg_2676" s="T158">haːs</ta>
            <ta e="T160" id="Seg_2677" s="T159">bu͡ol-u͡o</ta>
            <ta e="T161" id="Seg_2678" s="T160">daː</ta>
            <ta e="T162" id="Seg_2679" s="T161">kün</ta>
            <ta e="T163" id="Seg_2680" s="T162">u͡ot-u-ttan</ta>
            <ta e="T164" id="Seg_2681" s="T163">uːl-an</ta>
            <ta e="T165" id="Seg_2682" s="T164">nʼolboj-on</ta>
            <ta e="T166" id="Seg_2683" s="T165">tüh-eːčči-gin=ij</ta>
            <ta e="T167" id="Seg_2684" s="T166">di͡e-bit</ta>
            <ta e="T168" id="Seg_2685" s="T167">kün</ta>
            <ta e="T169" id="Seg_2686" s="T168">u͡ot-a</ta>
            <ta e="T170" id="Seg_2687" s="T169">bert</ta>
            <ta e="T171" id="Seg_2688" s="T170">da</ta>
            <ta e="T172" id="Seg_2689" s="T171">bert</ta>
            <ta e="T173" id="Seg_2690" s="T172">diː-r</ta>
            <ta e="T174" id="Seg_2691" s="T173">minigi-n</ta>
            <ta e="T175" id="Seg_2692" s="T174">uːl-a-r-ɨ-m-ɨ͡ak-taːgar</ta>
            <ta e="T176" id="Seg_2693" s="T175">bu͡ol-u͡ok</ta>
            <ta e="T177" id="Seg_2694" s="T176">eŋin-eŋit-ter-i</ta>
            <ta e="T178" id="Seg_2695" s="T177">uːl-a-r-aːččɨ</ta>
            <ta e="T179" id="Seg_2696" s="T178">diː-r</ta>
            <ta e="T180" id="Seg_2697" s="T179">küːn</ta>
            <ta e="T181" id="Seg_2698" s="T180">ber-kin</ta>
            <ta e="T182" id="Seg_2699" s="T181">du͡o</ta>
            <ta e="T183" id="Seg_2700" s="T182">dʼe</ta>
            <ta e="T184" id="Seg_2701" s="T183">ber-pin</ta>
            <ta e="T185" id="Seg_2702" s="T184">da</ta>
            <ta e="T186" id="Seg_2703" s="T185">ber-pi͡en</ta>
            <ta e="T187" id="Seg_2704" s="T186">diː-r</ta>
            <ta e="T188" id="Seg_2705" s="T187">kaːr-ɨ</ta>
            <ta e="T189" id="Seg_2706" s="T188">da</ta>
            <ta e="T190" id="Seg_2707" s="T189">buːh-u</ta>
            <ta e="T191" id="Seg_2708" s="T190">da</ta>
            <ta e="T192" id="Seg_2709" s="T191">ord-o-r-bokko</ta>
            <ta e="T193" id="Seg_2710" s="T192">ir-i͡er-eːčči-bin</ta>
            <ta e="T194" id="Seg_2711" s="T193">diː-r</ta>
            <ta e="T195" id="Seg_2712" s="T194">paː</ta>
            <ta e="T196" id="Seg_2713" s="T195">onnuk</ta>
            <ta e="T197" id="Seg_2714" s="T196">bert</ta>
            <ta e="T198" id="Seg_2715" s="T197">kihi</ta>
            <ta e="T199" id="Seg_2716" s="T198">keː</ta>
            <ta e="T200" id="Seg_2717" s="T199">togo</ta>
            <ta e="T201" id="Seg_2718" s="T200">taːs</ta>
            <ta e="T202" id="Seg_2719" s="T201">kaja</ta>
            <ta e="T203" id="Seg_2720" s="T202">kalka-tɨ-gar</ta>
            <ta e="T204" id="Seg_2721" s="T203">keteg-i-ger</ta>
            <ta e="T205" id="Seg_2722" s="T204">kisten-e</ta>
            <ta e="T206" id="Seg_2723" s="T205">hɨt-aːččɨ-gɨn</ta>
            <ta e="T207" id="Seg_2724" s="T206">diː-r</ta>
            <ta e="T208" id="Seg_2725" s="T207">taːs</ta>
            <ta e="T209" id="Seg_2726" s="T208">kaja</ta>
            <ta e="T210" id="Seg_2727" s="T209">bert</ta>
            <ta e="T211" id="Seg_2728" s="T210">daː</ta>
            <ta e="T212" id="Seg_2729" s="T211">bert</ta>
            <ta e="T213" id="Seg_2730" s="T212">diː-r</ta>
            <ta e="T214" id="Seg_2731" s="T213">minigi-n</ta>
            <ta e="T215" id="Seg_2732" s="T214">inni-bi-tten</ta>
            <ta e="T216" id="Seg_2733" s="T215">kalkalaː-ččɨ</ta>
            <ta e="T217" id="Seg_2734" s="T216">diː-r</ta>
            <ta e="T218" id="Seg_2735" s="T217">taːs</ta>
            <ta e="T219" id="Seg_2736" s="T218">kaja=e</ta>
            <ta e="T220" id="Seg_2737" s="T219">ber-kin</ta>
            <ta e="T221" id="Seg_2738" s="T220">du͡o</ta>
            <ta e="T222" id="Seg_2739" s="T221">če</ta>
            <ta e="T223" id="Seg_2740" s="T222">ber-pin</ta>
            <ta e="T224" id="Seg_2741" s="T223">da</ta>
            <ta e="T225" id="Seg_2742" s="T224">ber-pi͡en</ta>
            <ta e="T226" id="Seg_2743" s="T225">kün-ü</ta>
            <ta e="T227" id="Seg_2744" s="T226">bagas</ta>
            <ta e="T228" id="Seg_2745" s="T227">kalkalaː-ččɨ-bɨn</ta>
            <ta e="T229" id="Seg_2746" s="T228">diː-r</ta>
            <ta e="T230" id="Seg_2747" s="T229">kɨhɨn-ɨ</ta>
            <ta e="T231" id="Seg_2748" s="T230">bɨha</ta>
            <ta e="T232" id="Seg_2749" s="T231">da</ta>
            <ta e="T233" id="Seg_2750" s="T232">karaŋa</ta>
            <ta e="T234" id="Seg_2751" s="T233">oŋor-oːčču-bɨn</ta>
            <ta e="T235" id="Seg_2752" s="T234">diː-r</ta>
            <ta e="T236" id="Seg_2753" s="T235">onnuk</ta>
            <ta e="T237" id="Seg_2754" s="T236">bert</ta>
            <ta e="T238" id="Seg_2755" s="T237">kihi</ta>
            <ta e="T239" id="Seg_2756" s="T238">ki͡e</ta>
            <ta e="T240" id="Seg_2757" s="T239">togo</ta>
            <ta e="T241" id="Seg_2758" s="T240">üːteːn</ta>
            <ta e="T242" id="Seg_2759" s="T241">kutujak-tar-ga</ta>
            <ta e="T243" id="Seg_2760" s="T242">supturut</ta>
            <ta e="T244" id="Seg_2761" s="T243">üːt-e-t-e</ta>
            <ta e="T245" id="Seg_2762" s="T244">hɨldʼ-a-gɨn</ta>
            <ta e="T246" id="Seg_2763" s="T245">di͡e-bit</ta>
            <ta e="T247" id="Seg_2764" s="T246">če</ta>
            <ta e="T248" id="Seg_2765" s="T247">üːteːn</ta>
            <ta e="T249" id="Seg_2766" s="T248">kutujak-tar</ta>
            <ta e="T250" id="Seg_2767" s="T249">ber-ter</ta>
            <ta e="T251" id="Seg_2768" s="T250">da</ta>
            <ta e="T252" id="Seg_2769" s="T251">ber-teːr</ta>
            <ta e="T253" id="Seg_2770" s="T252">bert</ta>
            <ta e="T254" id="Seg_2771" s="T253">hɨtɨː</ta>
            <ta e="T255" id="Seg_2772" s="T254">tumsu-laːk-tar</ta>
            <ta e="T256" id="Seg_2773" s="T255">diː-r</ta>
            <ta e="T257" id="Seg_2774" s="T256">kutujak-tar</ta>
            <ta e="T258" id="Seg_2775" s="T257">ber-kit</ta>
            <ta e="T259" id="Seg_2776" s="T258">du͡o</ta>
            <ta e="T260" id="Seg_2777" s="T259">ber-pit</ta>
            <ta e="T261" id="Seg_2778" s="T260">da</ta>
            <ta e="T262" id="Seg_2779" s="T261">ber-pi͡et</ta>
            <ta e="T263" id="Seg_2780" s="T262">taːs</ta>
            <ta e="T264" id="Seg_2781" s="T263">kaja-nɨ</ta>
            <ta e="T265" id="Seg_2782" s="T264">kanan</ta>
            <ta e="T266" id="Seg_2783" s="T265">hanɨː-r</ta>
            <ta e="T267" id="Seg_2784" s="T266">ükt-üː</ta>
            <ta e="T268" id="Seg_2785" s="T267">taks-aːččɨ-bɨt</ta>
            <ta e="T269" id="Seg_2786" s="T268">d-iː</ta>
            <ta e="T270" id="Seg_2787" s="T269">onnuk</ta>
            <ta e="T271" id="Seg_2788" s="T270">bert</ta>
            <ta e="T272" id="Seg_2789" s="T271">kihi-ler</ta>
            <ta e="T273" id="Seg_2790" s="T272">ki͡e</ta>
            <ta e="T274" id="Seg_2791" s="T273">togo</ta>
            <ta e="T275" id="Seg_2792" s="T274">kɨrsa-ga</ta>
            <ta e="T276" id="Seg_2793" s="T275">hi͡e-t-eːčči-git=ij</ta>
            <ta e="T277" id="Seg_2794" s="T276">di͡e-bit</ta>
            <ta e="T278" id="Seg_2795" s="T277">kɨrsa</ta>
            <ta e="T279" id="Seg_2796" s="T278">kɨrsa</ta>
            <ta e="T280" id="Seg_2797" s="T279">hi͡e-mne</ta>
            <ta e="T281" id="Seg_2798" s="T280">diː-r</ta>
            <ta e="T282" id="Seg_2799" s="T281">bihigi-ni</ta>
            <ta e="T283" id="Seg_2800" s="T282">hi͡e-čči</ta>
            <ta e="T284" id="Seg_2801" s="T283">da</ta>
            <ta e="T285" id="Seg_2802" s="T284">hi͡e-čči</ta>
            <ta e="T286" id="Seg_2803" s="T285">kɨrsa</ta>
            <ta e="T287" id="Seg_2804" s="T286">kɨrsaː</ta>
            <ta e="T288" id="Seg_2805" s="T287">ber-kin</ta>
            <ta e="T289" id="Seg_2806" s="T288">du͡o</ta>
            <ta e="T290" id="Seg_2807" s="T289">ber-pin</ta>
            <ta e="T291" id="Seg_2808" s="T290">da</ta>
            <ta e="T292" id="Seg_2809" s="T291">ber-pi͡en</ta>
            <ta e="T293" id="Seg_2810" s="T292">diː-r</ta>
            <ta e="T294" id="Seg_2811" s="T293">kutujag-ɨ</ta>
            <ta e="T295" id="Seg_2812" s="T294">kantan</ta>
            <ta e="T296" id="Seg_2813" s="T295">hanɨː-r</ta>
            <ta e="T297" id="Seg_2814" s="T296">kah-an</ta>
            <ta e="T298" id="Seg_2815" s="T297">ɨl-an</ta>
            <ta e="T299" id="Seg_2816" s="T298">hi͡e-čči-bit</ta>
            <ta e="T300" id="Seg_2817" s="T299">onnuk</ta>
            <ta e="T301" id="Seg_2818" s="T300">bert</ta>
            <ta e="T302" id="Seg_2819" s="T301">kihi</ta>
            <ta e="T303" id="Seg_2820" s="T302">ki͡e</ta>
            <ta e="T304" id="Seg_2821" s="T303">čaːrkaːn-naːk</ta>
            <ta e="T305" id="Seg_2822" s="T304">saka</ta>
            <ta e="T306" id="Seg_2823" s="T305">ogo-lor-go</ta>
            <ta e="T307" id="Seg_2824" s="T306">togo</ta>
            <ta e="T308" id="Seg_2825" s="T307">kap-tar-aːččɨ-gɨn=ɨj</ta>
            <ta e="T309" id="Seg_2826" s="T308">di͡e-bit</ta>
            <ta e="T310" id="Seg_2827" s="T309">čaːrkaːn-naːk</ta>
            <ta e="T311" id="Seg_2828" s="T310">saka</ta>
            <ta e="T312" id="Seg_2829" s="T311">ogo-loro</ta>
            <ta e="T313" id="Seg_2830" s="T312">kaːr</ta>
            <ta e="T314" id="Seg_2831" s="T313">ann-ɨ-gar</ta>
            <ta e="T315" id="Seg_2832" s="T314">köm-püt-teri-n</ta>
            <ta e="T316" id="Seg_2833" s="T315">kantan</ta>
            <ta e="T317" id="Seg_2834" s="T316">bil-i͡e-m=ij</ta>
            <ta e="T318" id="Seg_2835" s="T317">kapkaːn-ɨ</ta>
            <ta e="T319" id="Seg_2836" s="T318">diː-r</ta>
            <ta e="T320" id="Seg_2837" s="T319">ol</ta>
            <ta e="T321" id="Seg_2838" s="T320">ihin</ta>
            <ta e="T322" id="Seg_2839" s="T321">tut-tar-aːččɨ-bɨn</ta>
            <ta e="T323" id="Seg_2840" s="T322">diː-r</ta>
            <ta e="T324" id="Seg_2841" s="T323">ah-ɨː</ta>
            <ta e="T325" id="Seg_2842" s="T324">kel-em-min</ta>
            <ta e="T326" id="Seg_2843" s="T325">diː-r</ta>
            <ta e="T327" id="Seg_2844" s="T326">čaːrkaːn-naːk</ta>
            <ta e="T328" id="Seg_2845" s="T327">saka</ta>
            <ta e="T329" id="Seg_2846" s="T328">ogo-loro</ta>
            <ta e="T330" id="Seg_2847" s="T329">ber-kit</ta>
            <ta e="T331" id="Seg_2848" s="T330">duː</ta>
            <ta e="T332" id="Seg_2849" s="T331">ber-pit</ta>
            <ta e="T333" id="Seg_2850" s="T332">da</ta>
            <ta e="T334" id="Seg_2851" s="T333">ber-pi͡et</ta>
            <ta e="T335" id="Seg_2852" s="T334">kɨrsa-nɨ</ta>
            <ta e="T336" id="Seg_2853" s="T335">kaččaga</ta>
            <ta e="T337" id="Seg_2854" s="T336">hanɨː-r</ta>
            <ta e="T338" id="Seg_2855" s="T337">kab-aːččɨ-bɨt</ta>
            <ta e="T339" id="Seg_2856" s="T338">onnuk</ta>
            <ta e="T340" id="Seg_2857" s="T339">bert</ta>
            <ta e="T341" id="Seg_2858" s="T340">kihi-ler</ta>
            <ta e="T342" id="Seg_2859" s="T341">ke</ta>
            <ta e="T343" id="Seg_2860" s="T342">togo</ta>
            <ta e="T344" id="Seg_2861" s="T343">ojuŋ-ŋa</ta>
            <ta e="T345" id="Seg_2862" s="T344">si͡e-t-eːčči-git=ij</ta>
            <ta e="T346" id="Seg_2863" s="T345">di͡e-bit</ta>
            <ta e="T347" id="Seg_2864" s="T346">ojun</ta>
            <ta e="T348" id="Seg_2865" s="T347">hürdeːk</ta>
            <ta e="T349" id="Seg_2866" s="T348">bu͡ol-l-a</ta>
            <ta e="T350" id="Seg_2867" s="T349">di͡e-bit</ta>
            <ta e="T351" id="Seg_2868" s="T350">h-i͡e-m</ta>
            <ta e="T352" id="Seg_2869" s="T351">d-i͡eg-e</ta>
            <ta e="T353" id="Seg_2870" s="T352">da</ta>
            <ta e="T354" id="Seg_2871" s="T353">hi͡e-čči</ta>
            <ta e="T355" id="Seg_2872" s="T354">d-iː</ta>
            <ta e="T356" id="Seg_2873" s="T355">ojuːn</ta>
            <ta e="T357" id="Seg_2874" s="T356">ber-kin</ta>
            <ta e="T358" id="Seg_2875" s="T357">du͡o</ta>
            <ta e="T359" id="Seg_2876" s="T358">ber-pin</ta>
            <ta e="T360" id="Seg_2877" s="T359">da</ta>
            <ta e="T361" id="Seg_2878" s="T360">ber-pi͡en</ta>
            <ta e="T362" id="Seg_2879" s="T361">kaččaga</ta>
            <ta e="T363" id="Seg_2880" s="T362">hanaː-tɨ-m</ta>
            <ta e="T364" id="Seg_2881" s="T363">daː</ta>
            <ta e="T365" id="Seg_2882" s="T364">hi͡e-čči-bin</ta>
            <ta e="T366" id="Seg_2883" s="T365">kuhagan-nɨk</ta>
            <ta e="T367" id="Seg_2884" s="T366">haŋar-bɨt</ta>
            <ta e="T368" id="Seg_2885" s="T367">kihi</ta>
            <ta e="T369" id="Seg_2886" s="T368">bu͡ol-u͡o</ta>
            <ta e="T370" id="Seg_2887" s="T369">da</ta>
            <ta e="T371" id="Seg_2888" s="T370">diː-r</ta>
            <ta e="T372" id="Seg_2889" s="T371">onnuk</ta>
            <ta e="T373" id="Seg_2890" s="T372">bert</ta>
            <ta e="T374" id="Seg_2891" s="T373">kihi</ta>
            <ta e="T375" id="Seg_2892" s="T374">ki͡e</ta>
            <ta e="T376" id="Seg_2893" s="T375">togo</ta>
            <ta e="T377" id="Seg_2894" s="T376">himi͡ert-ke</ta>
            <ta e="T378" id="Seg_2895" s="T377">hi͡e-t-eːčči-gin=ij</ta>
            <ta e="T379" id="Seg_2896" s="T378">diː-r</ta>
            <ta e="T380" id="Seg_2897" s="T379">himi͡ert</ta>
            <ta e="T381" id="Seg_2898" s="T380">baː</ta>
            <ta e="T382" id="Seg_2899" s="T381">kimi͡e-ke</ta>
            <ta e="T383" id="Seg_2900" s="T382">da</ta>
            <ta e="T384" id="Seg_2901" s="T383">tɨl-laː-ččɨ-ta</ta>
            <ta e="T385" id="Seg_2902" s="T384">hu͡ok</ta>
            <ta e="T386" id="Seg_2903" s="T385">di͡e-bit</ta>
            <ta e="T387" id="Seg_2904" s="T386">himi͡ert</ta>
            <ta e="T388" id="Seg_2905" s="T387">ber-kin</ta>
            <ta e="T389" id="Seg_2906" s="T388">du͡o</ta>
            <ta e="T390" id="Seg_2907" s="T389">diː-r</ta>
            <ta e="T391" id="Seg_2908" s="T390">ber-pin</ta>
            <ta e="T392" id="Seg_2909" s="T391">da</ta>
            <ta e="T393" id="Seg_2910" s="T392">ber-pin</ta>
            <ta e="T394" id="Seg_2911" s="T393">enigi-n</ta>
            <ta e="T395" id="Seg_2912" s="T394">da</ta>
            <ta e="T396" id="Seg_2913" s="T395">h-i͡e-m</ta>
            <ta e="T397" id="Seg_2914" s="T396">di͡e-bit-e</ta>
            <ta e="T398" id="Seg_2915" s="T397">hi͡e-n</ta>
            <ta e="T399" id="Seg_2916" s="T398">keːs-pit-e</ta>
            <ta e="T400" id="Seg_2917" s="T399">dʼe</ta>
            <ta e="T401" id="Seg_2918" s="T400">iti</ta>
            <ta e="T402" id="Seg_2919" s="T401">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2920" s="T0">Taːl</ta>
            <ta e="T2" id="Seg_2921" s="T1">emeːksin</ta>
            <ta e="T3" id="Seg_2922" s="T2">olor-BIT</ta>
            <ta e="T4" id="Seg_2923" s="T3">ühü</ta>
            <ta e="T5" id="Seg_2924" s="T4">ojuː-LAːK</ta>
            <ta e="T6" id="Seg_2925" s="T5">ot-LAːK</ta>
            <ta e="T7" id="Seg_2926" s="T6">orto</ta>
            <ta e="T8" id="Seg_2927" s="T7">togus</ta>
            <ta e="T9" id="Seg_2928" s="T8">dojdu-GA</ta>
            <ta e="T10" id="Seg_2929" s="T9">Taːl</ta>
            <ta e="T11" id="Seg_2930" s="T10">emeːksin</ta>
            <ta e="T12" id="Seg_2931" s="T11">bu</ta>
            <ta e="T13" id="Seg_2932" s="T12">Taːl</ta>
            <ta e="T14" id="Seg_2933" s="T13">emeːksin</ta>
            <ta e="T15" id="Seg_2934" s="T14">bu͡ollagɨna</ta>
            <ta e="T16" id="Seg_2935" s="T15">biːrde</ta>
            <ta e="T17" id="Seg_2936" s="T16">ki͡ehe-nAn</ta>
            <ta e="T18" id="Seg_2937" s="T17">kaja</ta>
            <ta e="T19" id="Seg_2938" s="T18">čaːj</ta>
            <ta e="T20" id="Seg_2939" s="T19">is-I-n-An-is-I-n-An</ta>
            <ta e="T21" id="Seg_2940" s="T20">baran</ta>
            <ta e="T23" id="Seg_2941" s="T21">utuj-An</ta>
            <ta e="T24" id="Seg_2942" s="T23">utuj-BIT</ta>
            <ta e="T25" id="Seg_2943" s="T24">du͡o</ta>
            <ta e="T26" id="Seg_2944" s="T25">kajdak</ta>
            <ta e="T27" id="Seg_2945" s="T26">du͡o</ta>
            <ta e="T28" id="Seg_2946" s="T27">ulaga-tA</ta>
            <ta e="T29" id="Seg_2947" s="T28">tehin-I-BIT-tA</ta>
            <ta e="T30" id="Seg_2948" s="T29">du͡o</ta>
            <ta e="T31" id="Seg_2949" s="T30">hamɨːr</ta>
            <ta e="T32" id="Seg_2950" s="T31">tüs-BIT</ta>
            <ta e="T33" id="Seg_2951" s="T32">du͡o</ta>
            <ta e="T34" id="Seg_2952" s="T33">tu͡ok</ta>
            <ta e="T35" id="Seg_2953" s="T34">du͡o</ta>
            <ta e="T36" id="Seg_2954" s="T35">tellek-tA</ta>
            <ta e="T37" id="Seg_2955" s="T36">ilij-BIT</ta>
            <ta e="T38" id="Seg_2956" s="T37">emeːksin</ta>
            <ta e="T39" id="Seg_2957" s="T38">gi͡en-tA</ta>
            <ta e="T40" id="Seg_2958" s="T39">bu</ta>
            <ta e="T41" id="Seg_2959" s="T40">tellek-tA</ta>
            <ta e="T42" id="Seg_2960" s="T41">ilij-An</ta>
            <ta e="T43" id="Seg_2961" s="T42">emeːksin</ta>
            <ta e="T44" id="Seg_2962" s="T43">harsi͡erda</ta>
            <ta e="T45" id="Seg_2963" s="T44">kühün</ta>
            <ta e="T46" id="Seg_2964" s="T45">e-BIT</ta>
            <ta e="T47" id="Seg_2965" s="T46">buːs</ta>
            <ta e="T48" id="Seg_2966" s="T47">toŋ-Ar-tI-n</ta>
            <ta e="T49" id="Seg_2967" s="T48">hagɨna</ta>
            <ta e="T50" id="Seg_2968" s="T49">emeːksin</ta>
            <ta e="T51" id="Seg_2969" s="T50">tur-An</ta>
            <ta e="T52" id="Seg_2970" s="T51">oččogo</ta>
            <ta e="T53" id="Seg_2971" s="T52">tu͡ok</ta>
            <ta e="T54" id="Seg_2972" s="T53">u͡ot-tA</ta>
            <ta e="T55" id="Seg_2973" s="T54">kel-IAK.[tA]=Ij</ta>
            <ta e="T56" id="Seg_2974" s="T55">ile</ta>
            <ta e="T57" id="Seg_2975" s="T56">u͡ot-LAːK</ta>
            <ta e="T58" id="Seg_2976" s="T57">bu͡ol-IAK-LArA=Ij</ta>
            <ta e="T59" id="Seg_2977" s="T58">ačaːk</ta>
            <ta e="T60" id="Seg_2978" s="T59">bu͡ol-TI-tA</ta>
            <ta e="T61" id="Seg_2979" s="T60">onton</ta>
            <ta e="T62" id="Seg_2980" s="T61">ol</ta>
            <ta e="T63" id="Seg_2981" s="T62">ihin</ta>
            <ta e="T64" id="Seg_2982" s="T63">eni</ta>
            <ta e="T65" id="Seg_2983" s="T64">tahaːra</ta>
            <ta e="T66" id="Seg_2984" s="T65">kün-LAːK</ta>
            <ta e="T67" id="Seg_2985" s="T66">bagajɨ</ta>
            <ta e="T68" id="Seg_2986" s="T67">kün-GA</ta>
            <ta e="T69" id="Seg_2987" s="T68">araŋas-tI-GAr</ta>
            <ta e="T70" id="Seg_2988" s="T69">tellek-tI-n</ta>
            <ta e="T71" id="Seg_2989" s="T70">ɨjaː-An</ta>
            <ta e="T72" id="Seg_2990" s="T71">keːs-BIT</ta>
            <ta e="T73" id="Seg_2991" s="T72">kaja</ta>
            <ta e="T74" id="Seg_2992" s="T73">tellek-tA</ta>
            <ta e="T75" id="Seg_2993" s="T74">bu͡olla</ta>
            <ta e="T76" id="Seg_2994" s="T75">kaja</ta>
            <ta e="T77" id="Seg_2995" s="T76">čaːj-tI-n</ta>
            <ta e="T78" id="Seg_2996" s="T77">is-A</ta>
            <ta e="T79" id="Seg_2997" s="T78">olor-TAK-InA</ta>
            <ta e="T80" id="Seg_2998" s="T79">suːk</ta>
            <ta e="T81" id="Seg_2999" s="T80">gɨn-BIT-tA</ta>
            <ta e="T82" id="Seg_3000" s="T81">bu͡ollagɨna</ta>
            <ta e="T83" id="Seg_3001" s="T82">tagɨs-A</ta>
            <ta e="T84" id="Seg_3002" s="T83">köt-BIT-tA</ta>
            <ta e="T85" id="Seg_3003" s="T84">emeːksin</ta>
            <ta e="T86" id="Seg_3004" s="T85">tagɨs-A</ta>
            <ta e="T87" id="Seg_3005" s="T86">köt-BIT-tA</ta>
            <ta e="T88" id="Seg_3006" s="T87">bu͡ollagɨna</ta>
            <ta e="T89" id="Seg_3007" s="T88">tellek-tA</ta>
            <ta e="T90" id="Seg_3008" s="T89">köt-An</ta>
            <ta e="T91" id="Seg_3009" s="T90">bu͡ol-TAK-InA</ta>
            <ta e="T92" id="Seg_3010" s="T91">kü͡öl-I-ŋ</ta>
            <ta e="T93" id="Seg_3011" s="T403">uː</ta>
            <ta e="T94" id="Seg_3012" s="T93">bahɨn-Ar</ta>
            <ta e="T95" id="Seg_3013" s="T94">kölüje-tI-n</ta>
            <ta e="T96" id="Seg_3014" s="T95">kɨrɨː-tI-GAr</ta>
            <ta e="T98" id="Seg_3015" s="T96">tüs-BIT</ta>
            <ta e="T99" id="Seg_3016" s="T98">kajaː</ta>
            <ta e="T100" id="Seg_3017" s="T99">emeːksin</ta>
            <ta e="T101" id="Seg_3018" s="T100">bu͡ollagɨna</ta>
            <ta e="T102" id="Seg_3019" s="T101">bar-An</ta>
            <ta e="T103" id="Seg_3020" s="T102">kap-An</ta>
            <ta e="T104" id="Seg_3021" s="T103">ɨl-An</ta>
            <ta e="T105" id="Seg_3022" s="T104">tellek-BI-n</ta>
            <ta e="T106" id="Seg_3023" s="T105">kap-An</ta>
            <ta e="T107" id="Seg_3024" s="T106">ɨl-An-BIn</ta>
            <ta e="T108" id="Seg_3025" s="T107">egel-IAK-m</ta>
            <ta e="T109" id="Seg_3026" s="T108">di͡e-BIT</ta>
            <ta e="T110" id="Seg_3027" s="T109">eːt</ta>
            <ta e="T111" id="Seg_3028" s="T110">tɨ͡al-tA</ta>
            <ta e="T112" id="Seg_3029" s="T111">suːk</ta>
            <ta e="T113" id="Seg_3030" s="T112">gɨn-TAr-An</ta>
            <ta e="T114" id="Seg_3031" s="T113">buːs-GA</ta>
            <ta e="T115" id="Seg_3032" s="T114">kiːr-BIT</ta>
            <ta e="T116" id="Seg_3033" s="T115">kaja</ta>
            <ta e="T117" id="Seg_3034" s="T116">kühün</ta>
            <ta e="T118" id="Seg_3035" s="T117">ka</ta>
            <ta e="T119" id="Seg_3036" s="T118">čelgien</ta>
            <ta e="T120" id="Seg_3037" s="T119">tur-Ar</ta>
            <ta e="T121" id="Seg_3038" s="T120">buːs-GA</ta>
            <ta e="T122" id="Seg_3039" s="T121">bu</ta>
            <ta e="T123" id="Seg_3040" s="T122">emeːksin</ta>
            <ta e="T124" id="Seg_3041" s="T123">kiːr-BIT</ta>
            <ta e="T125" id="Seg_3042" s="T124">bu-nI</ta>
            <ta e="T126" id="Seg_3043" s="T125">bat-An</ta>
            <ta e="T127" id="Seg_3044" s="T126">hüːr-A-BIn</ta>
            <ta e="T128" id="Seg_3045" s="T127">di͡e-An</ta>
            <ta e="T129" id="Seg_3046" s="T128">kaltɨrɨj-An</ta>
            <ta e="T130" id="Seg_3047" s="T129">tüs-An</ta>
            <ta e="T131" id="Seg_3048" s="T130">öttük-tI-n</ta>
            <ta e="T132" id="Seg_3049" s="T131">bulgu</ta>
            <ta e="T133" id="Seg_3050" s="T132">tüs-BIT</ta>
            <ta e="T134" id="Seg_3051" s="T133">emeːksin</ta>
            <ta e="T135" id="Seg_3052" s="T134">kaja</ta>
            <ta e="T136" id="Seg_3053" s="T135">emeːksin</ta>
            <ta e="T137" id="Seg_3054" s="T136">bu͡o</ta>
            <ta e="T138" id="Seg_3055" s="T137">di͡e-An</ta>
            <ta e="T139" id="Seg_3056" s="T138">bu͡ol-Ar</ta>
            <ta e="T140" id="Seg_3057" s="T139">buːs</ta>
            <ta e="T141" id="Seg_3058" s="T140">buːs</ta>
            <ta e="T142" id="Seg_3059" s="T141">bert-GIn</ta>
            <ta e="T143" id="Seg_3060" s="T142">du͡o</ta>
            <ta e="T144" id="Seg_3061" s="T143">di͡e-Ar</ta>
            <ta e="T145" id="Seg_3062" s="T144">eː</ta>
            <ta e="T146" id="Seg_3063" s="T145">dʼe</ta>
            <ta e="T147" id="Seg_3064" s="T146">bert-BIn</ta>
            <ta e="T148" id="Seg_3065" s="T147">dʼe</ta>
            <ta e="T149" id="Seg_3066" s="T148">bert-BIn</ta>
            <ta e="T150" id="Seg_3067" s="T149">en</ta>
            <ta e="T151" id="Seg_3068" s="T150">öttük-GI-n</ta>
            <ta e="T152" id="Seg_3069" s="T151">tohut-TI-m</ta>
            <ta e="T153" id="Seg_3070" s="T152">di͡e-An</ta>
            <ta e="T154" id="Seg_3071" s="T153">onnuk</ta>
            <ta e="T155" id="Seg_3072" s="T154">bert</ta>
            <ta e="T156" id="Seg_3073" s="T155">kihi</ta>
            <ta e="T157" id="Seg_3074" s="T156">keː</ta>
            <ta e="T158" id="Seg_3075" s="T157">togo</ta>
            <ta e="T159" id="Seg_3076" s="T158">haːs</ta>
            <ta e="T160" id="Seg_3077" s="T159">bu͡ol-IAK.[tA]</ta>
            <ta e="T161" id="Seg_3078" s="T160">da</ta>
            <ta e="T162" id="Seg_3079" s="T161">kün</ta>
            <ta e="T163" id="Seg_3080" s="T162">u͡ot-tI-ttAn</ta>
            <ta e="T164" id="Seg_3081" s="T163">uːl-An</ta>
            <ta e="T165" id="Seg_3082" s="T164">nʼolboj-An</ta>
            <ta e="T166" id="Seg_3083" s="T165">tüs-AːččI-GIn=Ij</ta>
            <ta e="T167" id="Seg_3084" s="T166">di͡e-BIT</ta>
            <ta e="T168" id="Seg_3085" s="T167">kün</ta>
            <ta e="T169" id="Seg_3086" s="T168">u͡ot-tA</ta>
            <ta e="T170" id="Seg_3087" s="T169">bert</ta>
            <ta e="T171" id="Seg_3088" s="T170">da</ta>
            <ta e="T172" id="Seg_3089" s="T171">bert</ta>
            <ta e="T173" id="Seg_3090" s="T172">di͡e-Ar</ta>
            <ta e="T174" id="Seg_3091" s="T173">min-n</ta>
            <ta e="T175" id="Seg_3092" s="T174">uːl-A-r-I-m-IAK-TAːgAr</ta>
            <ta e="T176" id="Seg_3093" s="T175">bu͡ol-IAK</ta>
            <ta e="T177" id="Seg_3094" s="T176">eŋin-eŋin-LAr-nI</ta>
            <ta e="T178" id="Seg_3095" s="T177">uːl-A-r-AːččI</ta>
            <ta e="T179" id="Seg_3096" s="T178">di͡e-Ar</ta>
            <ta e="T180" id="Seg_3097" s="T179">kün</ta>
            <ta e="T181" id="Seg_3098" s="T180">bert-GIn</ta>
            <ta e="T182" id="Seg_3099" s="T181">du͡o</ta>
            <ta e="T183" id="Seg_3100" s="T182">dʼe</ta>
            <ta e="T184" id="Seg_3101" s="T183">bert-BIn</ta>
            <ta e="T185" id="Seg_3102" s="T184">da</ta>
            <ta e="T186" id="Seg_3103" s="T185">bert-BIn</ta>
            <ta e="T187" id="Seg_3104" s="T186">di͡e-Ar</ta>
            <ta e="T188" id="Seg_3105" s="T187">kaːr-nI</ta>
            <ta e="T189" id="Seg_3106" s="T188">da</ta>
            <ta e="T190" id="Seg_3107" s="T189">buːs-nI</ta>
            <ta e="T191" id="Seg_3108" s="T190">da</ta>
            <ta e="T192" id="Seg_3109" s="T191">ort-A-r-BAkkA</ta>
            <ta e="T193" id="Seg_3110" s="T192">ir-IAr-AːččI-BIn</ta>
            <ta e="T194" id="Seg_3111" s="T193">di͡e-Ar</ta>
            <ta e="T195" id="Seg_3112" s="T194">paː</ta>
            <ta e="T196" id="Seg_3113" s="T195">onnuk</ta>
            <ta e="T197" id="Seg_3114" s="T196">bert</ta>
            <ta e="T198" id="Seg_3115" s="T197">kihi</ta>
            <ta e="T199" id="Seg_3116" s="T198">keː</ta>
            <ta e="T200" id="Seg_3117" s="T199">togo</ta>
            <ta e="T201" id="Seg_3118" s="T200">taːs</ta>
            <ta e="T202" id="Seg_3119" s="T201">kaja</ta>
            <ta e="T203" id="Seg_3120" s="T202">kalka-tI-GAr</ta>
            <ta e="T204" id="Seg_3121" s="T203">ketek-tI-GAr</ta>
            <ta e="T205" id="Seg_3122" s="T204">kisten-A</ta>
            <ta e="T206" id="Seg_3123" s="T205">hɨt-AːččI-GIn</ta>
            <ta e="T207" id="Seg_3124" s="T206">di͡e-Ar</ta>
            <ta e="T208" id="Seg_3125" s="T207">taːs</ta>
            <ta e="T209" id="Seg_3126" s="T208">kaja</ta>
            <ta e="T210" id="Seg_3127" s="T209">bert</ta>
            <ta e="T211" id="Seg_3128" s="T210">da</ta>
            <ta e="T212" id="Seg_3129" s="T211">bert</ta>
            <ta e="T213" id="Seg_3130" s="T212">di͡e-Ar</ta>
            <ta e="T214" id="Seg_3131" s="T213">min-n</ta>
            <ta e="T215" id="Seg_3132" s="T214">ilin-BI-ttAn</ta>
            <ta e="T216" id="Seg_3133" s="T215">kalkalaː-AːččI</ta>
            <ta e="T217" id="Seg_3134" s="T216">di͡e-Ar</ta>
            <ta e="T218" id="Seg_3135" s="T217">taːs</ta>
            <ta e="T219" id="Seg_3136" s="T218">kaja=e</ta>
            <ta e="T220" id="Seg_3137" s="T219">bert-GIn</ta>
            <ta e="T221" id="Seg_3138" s="T220">du͡o</ta>
            <ta e="T222" id="Seg_3139" s="T221">dʼe</ta>
            <ta e="T223" id="Seg_3140" s="T222">bert-BIn</ta>
            <ta e="T224" id="Seg_3141" s="T223">da</ta>
            <ta e="T225" id="Seg_3142" s="T224">bert-BIn</ta>
            <ta e="T226" id="Seg_3143" s="T225">kün-nI</ta>
            <ta e="T227" id="Seg_3144" s="T226">bagas</ta>
            <ta e="T228" id="Seg_3145" s="T227">kalkalaː-AːččI-BIn</ta>
            <ta e="T229" id="Seg_3146" s="T228">di͡e-Ar</ta>
            <ta e="T230" id="Seg_3147" s="T229">kɨhɨn-nI</ta>
            <ta e="T231" id="Seg_3148" s="T230">bɨha</ta>
            <ta e="T232" id="Seg_3149" s="T231">da</ta>
            <ta e="T233" id="Seg_3150" s="T232">karaŋa</ta>
            <ta e="T234" id="Seg_3151" s="T233">oŋor-AːččI-BIn</ta>
            <ta e="T235" id="Seg_3152" s="T234">di͡e-Ar</ta>
            <ta e="T236" id="Seg_3153" s="T235">onnuk</ta>
            <ta e="T237" id="Seg_3154" s="T236">bert</ta>
            <ta e="T238" id="Seg_3155" s="T237">kihi</ta>
            <ta e="T239" id="Seg_3156" s="T238">keː</ta>
            <ta e="T240" id="Seg_3157" s="T239">togo</ta>
            <ta e="T241" id="Seg_3158" s="T240">üːteːn</ta>
            <ta e="T242" id="Seg_3159" s="T241">kutujak-LAr-GA</ta>
            <ta e="T243" id="Seg_3160" s="T242">huptu</ta>
            <ta e="T244" id="Seg_3161" s="T243">üt-A-t-A</ta>
            <ta e="T245" id="Seg_3162" s="T244">hɨrɨt-A-GIn</ta>
            <ta e="T246" id="Seg_3163" s="T245">di͡e-BIT</ta>
            <ta e="T247" id="Seg_3164" s="T246">dʼe</ta>
            <ta e="T248" id="Seg_3165" s="T247">üːteːn</ta>
            <ta e="T249" id="Seg_3166" s="T248">kutujak-LAr</ta>
            <ta e="T250" id="Seg_3167" s="T249">bert-LAr</ta>
            <ta e="T251" id="Seg_3168" s="T250">da</ta>
            <ta e="T252" id="Seg_3169" s="T251">bert-LAr</ta>
            <ta e="T253" id="Seg_3170" s="T252">bert</ta>
            <ta e="T254" id="Seg_3171" s="T253">hɨtɨː</ta>
            <ta e="T255" id="Seg_3172" s="T254">tumsu-LAːK-LAr</ta>
            <ta e="T256" id="Seg_3173" s="T255">di͡e-Ar</ta>
            <ta e="T257" id="Seg_3174" s="T256">kutujak-LAr</ta>
            <ta e="T258" id="Seg_3175" s="T257">bert-GIt</ta>
            <ta e="T259" id="Seg_3176" s="T258">du͡o</ta>
            <ta e="T260" id="Seg_3177" s="T259">bert-BIt</ta>
            <ta e="T261" id="Seg_3178" s="T260">da</ta>
            <ta e="T262" id="Seg_3179" s="T261">bert-BIt</ta>
            <ta e="T263" id="Seg_3180" s="T262">taːs</ta>
            <ta e="T264" id="Seg_3181" s="T263">kaja-nI</ta>
            <ta e="T265" id="Seg_3182" s="T264">kanan</ta>
            <ta e="T266" id="Seg_3183" s="T265">hanaː-Ar</ta>
            <ta e="T267" id="Seg_3184" s="T266">ükteː-A</ta>
            <ta e="T268" id="Seg_3185" s="T267">tagɨs-AːččI-BIt</ta>
            <ta e="T269" id="Seg_3186" s="T268">di͡e-A</ta>
            <ta e="T270" id="Seg_3187" s="T269">onnuk</ta>
            <ta e="T271" id="Seg_3188" s="T270">bert</ta>
            <ta e="T272" id="Seg_3189" s="T271">kihi-LAr</ta>
            <ta e="T273" id="Seg_3190" s="T272">keː</ta>
            <ta e="T274" id="Seg_3191" s="T273">togo</ta>
            <ta e="T275" id="Seg_3192" s="T274">kɨrsa-GA</ta>
            <ta e="T276" id="Seg_3193" s="T275">hi͡e-t-AːččI-GIt=Ij</ta>
            <ta e="T277" id="Seg_3194" s="T276">di͡e-BIT</ta>
            <ta e="T278" id="Seg_3195" s="T277">kɨrsa</ta>
            <ta e="T279" id="Seg_3196" s="T278">kɨrsa</ta>
            <ta e="T280" id="Seg_3197" s="T279">hi͡e-mInA</ta>
            <ta e="T281" id="Seg_3198" s="T280">di͡e-Ar</ta>
            <ta e="T282" id="Seg_3199" s="T281">bihigi-nI</ta>
            <ta e="T283" id="Seg_3200" s="T282">hi͡e-AːččI</ta>
            <ta e="T284" id="Seg_3201" s="T283">da</ta>
            <ta e="T285" id="Seg_3202" s="T284">hi͡e-AːččI</ta>
            <ta e="T286" id="Seg_3203" s="T285">kɨrsa</ta>
            <ta e="T287" id="Seg_3204" s="T286">kɨrsa</ta>
            <ta e="T288" id="Seg_3205" s="T287">bert-GIn</ta>
            <ta e="T289" id="Seg_3206" s="T288">du͡o</ta>
            <ta e="T290" id="Seg_3207" s="T289">bert-BIn</ta>
            <ta e="T291" id="Seg_3208" s="T290">da</ta>
            <ta e="T292" id="Seg_3209" s="T291">bert-BIn</ta>
            <ta e="T293" id="Seg_3210" s="T292">di͡e-Ar</ta>
            <ta e="T294" id="Seg_3211" s="T293">kutujak-nI</ta>
            <ta e="T295" id="Seg_3212" s="T294">kantan</ta>
            <ta e="T296" id="Seg_3213" s="T295">hanaː-Ar</ta>
            <ta e="T297" id="Seg_3214" s="T296">kas-An</ta>
            <ta e="T298" id="Seg_3215" s="T297">ɨl-An</ta>
            <ta e="T299" id="Seg_3216" s="T298">hi͡e-AːččI-BIt</ta>
            <ta e="T300" id="Seg_3217" s="T299">onnuk</ta>
            <ta e="T301" id="Seg_3218" s="T300">bert</ta>
            <ta e="T302" id="Seg_3219" s="T301">kihi</ta>
            <ta e="T303" id="Seg_3220" s="T302">keː</ta>
            <ta e="T304" id="Seg_3221" s="T303">čaːrkaːn-LAːK</ta>
            <ta e="T305" id="Seg_3222" s="T304">haka</ta>
            <ta e="T306" id="Seg_3223" s="T305">ogo-LAr-GA</ta>
            <ta e="T307" id="Seg_3224" s="T306">togo</ta>
            <ta e="T308" id="Seg_3225" s="T307">kap-TAr-AːččI-GIn=Ij</ta>
            <ta e="T309" id="Seg_3226" s="T308">di͡e-BIT</ta>
            <ta e="T310" id="Seg_3227" s="T309">čaːrkaːn-LAːK</ta>
            <ta e="T311" id="Seg_3228" s="T310">haka</ta>
            <ta e="T312" id="Seg_3229" s="T311">ogo-LArA</ta>
            <ta e="T313" id="Seg_3230" s="T312">kaːr</ta>
            <ta e="T314" id="Seg_3231" s="T313">alɨn-tI-GAr</ta>
            <ta e="T315" id="Seg_3232" s="T314">köm-BIT-LArI-n</ta>
            <ta e="T316" id="Seg_3233" s="T315">kantan</ta>
            <ta e="T317" id="Seg_3234" s="T316">bil-IAK-m=Ij</ta>
            <ta e="T318" id="Seg_3235" s="T317">kapkaːn-nI</ta>
            <ta e="T319" id="Seg_3236" s="T318">di͡e-Ar</ta>
            <ta e="T320" id="Seg_3237" s="T319">ol</ta>
            <ta e="T321" id="Seg_3238" s="T320">ihin</ta>
            <ta e="T322" id="Seg_3239" s="T321">tut-TAr-AːččI-BIn</ta>
            <ta e="T323" id="Seg_3240" s="T322">di͡e-Ar</ta>
            <ta e="T324" id="Seg_3241" s="T323">ahaː-A</ta>
            <ta e="T325" id="Seg_3242" s="T324">kel-An-BIn</ta>
            <ta e="T326" id="Seg_3243" s="T325">di͡e-Ar</ta>
            <ta e="T327" id="Seg_3244" s="T326">čaːrkaːn-LAːK</ta>
            <ta e="T328" id="Seg_3245" s="T327">haka</ta>
            <ta e="T329" id="Seg_3246" s="T328">ogo-LArA</ta>
            <ta e="T330" id="Seg_3247" s="T329">bert-GIt</ta>
            <ta e="T331" id="Seg_3248" s="T330">du͡o</ta>
            <ta e="T332" id="Seg_3249" s="T331">bert-BIt</ta>
            <ta e="T333" id="Seg_3250" s="T332">da</ta>
            <ta e="T334" id="Seg_3251" s="T333">bert-BIt</ta>
            <ta e="T335" id="Seg_3252" s="T334">kɨrsa-nI</ta>
            <ta e="T336" id="Seg_3253" s="T335">kaččaga</ta>
            <ta e="T337" id="Seg_3254" s="T336">hanaː-Ar</ta>
            <ta e="T338" id="Seg_3255" s="T337">kap-AːččI-BIt</ta>
            <ta e="T339" id="Seg_3256" s="T338">onnuk</ta>
            <ta e="T340" id="Seg_3257" s="T339">bert</ta>
            <ta e="T341" id="Seg_3258" s="T340">kihi-LAr</ta>
            <ta e="T342" id="Seg_3259" s="T341">ka</ta>
            <ta e="T343" id="Seg_3260" s="T342">togo</ta>
            <ta e="T344" id="Seg_3261" s="T343">ojun-GA</ta>
            <ta e="T345" id="Seg_3262" s="T344">hi͡e-t-AːččI-GIt=Ij</ta>
            <ta e="T346" id="Seg_3263" s="T345">di͡e-BIT</ta>
            <ta e="T347" id="Seg_3264" s="T346">ojun</ta>
            <ta e="T348" id="Seg_3265" s="T347">hürdeːk</ta>
            <ta e="T349" id="Seg_3266" s="T348">bu͡ol-TI-tA</ta>
            <ta e="T350" id="Seg_3267" s="T349">di͡e-BIT</ta>
            <ta e="T351" id="Seg_3268" s="T350">hi͡e-IAK-m</ta>
            <ta e="T352" id="Seg_3269" s="T351">di͡e-IAK-tA</ta>
            <ta e="T353" id="Seg_3270" s="T352">da</ta>
            <ta e="T354" id="Seg_3271" s="T353">hi͡e-AːččI</ta>
            <ta e="T355" id="Seg_3272" s="T354">di͡e-A</ta>
            <ta e="T356" id="Seg_3273" s="T355">ojun</ta>
            <ta e="T357" id="Seg_3274" s="T356">bert-GIn</ta>
            <ta e="T358" id="Seg_3275" s="T357">du͡o</ta>
            <ta e="T359" id="Seg_3276" s="T358">bert-BIn</ta>
            <ta e="T360" id="Seg_3277" s="T359">da</ta>
            <ta e="T361" id="Seg_3278" s="T360">bert-BIn</ta>
            <ta e="T362" id="Seg_3279" s="T361">kaččaga</ta>
            <ta e="T363" id="Seg_3280" s="T362">hanaː-TI-m</ta>
            <ta e="T364" id="Seg_3281" s="T363">da</ta>
            <ta e="T365" id="Seg_3282" s="T364">hi͡e-AːččI-BIn</ta>
            <ta e="T366" id="Seg_3283" s="T365">kuhagan-LIk</ta>
            <ta e="T367" id="Seg_3284" s="T366">haŋar-BIT</ta>
            <ta e="T368" id="Seg_3285" s="T367">kihi</ta>
            <ta e="T369" id="Seg_3286" s="T368">bu͡ol-IAK.[tA]</ta>
            <ta e="T370" id="Seg_3287" s="T369">da</ta>
            <ta e="T371" id="Seg_3288" s="T370">di͡e-Ar</ta>
            <ta e="T372" id="Seg_3289" s="T371">onnuk</ta>
            <ta e="T373" id="Seg_3290" s="T372">bert</ta>
            <ta e="T374" id="Seg_3291" s="T373">kihi</ta>
            <ta e="T375" id="Seg_3292" s="T374">keː</ta>
            <ta e="T376" id="Seg_3293" s="T375">togo</ta>
            <ta e="T377" id="Seg_3294" s="T376">himi͡ert-GA</ta>
            <ta e="T378" id="Seg_3295" s="T377">hi͡e-t-AːččI-GIn=Ij</ta>
            <ta e="T379" id="Seg_3296" s="T378">di͡e-Ar</ta>
            <ta e="T380" id="Seg_3297" s="T379">himi͡ert</ta>
            <ta e="T381" id="Seg_3298" s="T380">bu</ta>
            <ta e="T382" id="Seg_3299" s="T381">kim-GA</ta>
            <ta e="T383" id="Seg_3300" s="T382">da</ta>
            <ta e="T384" id="Seg_3301" s="T383">tɨl-LAː-AːččI-tA</ta>
            <ta e="T385" id="Seg_3302" s="T384">hu͡ok</ta>
            <ta e="T386" id="Seg_3303" s="T385">di͡e-BIT</ta>
            <ta e="T387" id="Seg_3304" s="T386">himi͡ert</ta>
            <ta e="T388" id="Seg_3305" s="T387">bert-GIn</ta>
            <ta e="T389" id="Seg_3306" s="T388">du͡o</ta>
            <ta e="T390" id="Seg_3307" s="T389">di͡e-Ar</ta>
            <ta e="T391" id="Seg_3308" s="T390">bert-BIn</ta>
            <ta e="T392" id="Seg_3309" s="T391">da</ta>
            <ta e="T393" id="Seg_3310" s="T392">bert-BIn</ta>
            <ta e="T394" id="Seg_3311" s="T393">en-n</ta>
            <ta e="T395" id="Seg_3312" s="T394">da</ta>
            <ta e="T396" id="Seg_3313" s="T395">hi͡e-IAK-m</ta>
            <ta e="T397" id="Seg_3314" s="T396">di͡e-BIT-tA</ta>
            <ta e="T398" id="Seg_3315" s="T397">hi͡e-An</ta>
            <ta e="T399" id="Seg_3316" s="T398">keːs-BIT-tA</ta>
            <ta e="T400" id="Seg_3317" s="T399">dʼe</ta>
            <ta e="T401" id="Seg_3318" s="T400">iti</ta>
            <ta e="T402" id="Seg_3319" s="T401">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_3320" s="T0">Taal.[NOM]</ta>
            <ta e="T2" id="Seg_3321" s="T1">old.woman.[NOM]</ta>
            <ta e="T3" id="Seg_3322" s="T2">live-PST2.[3SG]</ta>
            <ta e="T4" id="Seg_3323" s="T3">it.is.said</ta>
            <ta e="T5" id="Seg_3324" s="T4">ornament-PROPR</ta>
            <ta e="T6" id="Seg_3325" s="T5">color-PROPR</ta>
            <ta e="T7" id="Seg_3326" s="T6">middle</ta>
            <ta e="T8" id="Seg_3327" s="T7">nine</ta>
            <ta e="T9" id="Seg_3328" s="T8">world-DAT/LOC</ta>
            <ta e="T10" id="Seg_3329" s="T9">Taal.[NOM]</ta>
            <ta e="T11" id="Seg_3330" s="T10">old.woman.[NOM]</ta>
            <ta e="T12" id="Seg_3331" s="T11">this</ta>
            <ta e="T13" id="Seg_3332" s="T12">Taal.[NOM]</ta>
            <ta e="T14" id="Seg_3333" s="T13">old.woman.[NOM]</ta>
            <ta e="T15" id="Seg_3334" s="T14">though</ta>
            <ta e="T16" id="Seg_3335" s="T15">once</ta>
            <ta e="T17" id="Seg_3336" s="T16">evening-INSTR</ta>
            <ta e="T18" id="Seg_3337" s="T17">well</ta>
            <ta e="T19" id="Seg_3338" s="T18">tea.[NOM]</ta>
            <ta e="T20" id="Seg_3339" s="T19">drink-EP-MED-CVB.SEQ-drink-EP-MED-CVB.SEQ</ta>
            <ta e="T21" id="Seg_3340" s="T20">after</ta>
            <ta e="T23" id="Seg_3341" s="T21">sleep-CVB.SEQ</ta>
            <ta e="T24" id="Seg_3342" s="T23">fall.asleep-PST2.[3SG]</ta>
            <ta e="T25" id="Seg_3343" s="T24">Q</ta>
            <ta e="T26" id="Seg_3344" s="T25">how</ta>
            <ta e="T27" id="Seg_3345" s="T26">Q</ta>
            <ta e="T28" id="Seg_3346" s="T27">edge.of.chum-3SG.[NOM]</ta>
            <ta e="T29" id="Seg_3347" s="T28">drip-EP-PST2-3SG</ta>
            <ta e="T30" id="Seg_3348" s="T29">Q</ta>
            <ta e="T31" id="Seg_3349" s="T30">rain.[NOM]</ta>
            <ta e="T32" id="Seg_3350" s="T31">fall-PST2.[3SG]</ta>
            <ta e="T33" id="Seg_3351" s="T32">Q</ta>
            <ta e="T34" id="Seg_3352" s="T33">what.[NOM]</ta>
            <ta e="T35" id="Seg_3353" s="T34">Q</ta>
            <ta e="T36" id="Seg_3354" s="T35">bedding-3SG.[NOM]</ta>
            <ta e="T37" id="Seg_3355" s="T36">get.wet-PST2.[3SG]</ta>
            <ta e="T38" id="Seg_3356" s="T37">old.woman.[NOM]</ta>
            <ta e="T39" id="Seg_3357" s="T38">own-3SG.[NOM]</ta>
            <ta e="T40" id="Seg_3358" s="T39">this</ta>
            <ta e="T41" id="Seg_3359" s="T40">bedding-3SG.[NOM]</ta>
            <ta e="T42" id="Seg_3360" s="T41">get.wet-CVB.SEQ</ta>
            <ta e="T43" id="Seg_3361" s="T42">old.woman.[NOM]</ta>
            <ta e="T44" id="Seg_3362" s="T43">in.the.morning</ta>
            <ta e="T45" id="Seg_3363" s="T44">autumn.[NOM]</ta>
            <ta e="T46" id="Seg_3364" s="T45">be-PST2.[3SG]</ta>
            <ta e="T47" id="Seg_3365" s="T46">ice.[NOM]</ta>
            <ta e="T48" id="Seg_3366" s="T47">freeze-PTCP.PRS-3SG-ACC</ta>
            <ta e="T49" id="Seg_3367" s="T48">when</ta>
            <ta e="T50" id="Seg_3368" s="T49">old.woman.[NOM]</ta>
            <ta e="T51" id="Seg_3369" s="T50">stand.up-CVB.SEQ</ta>
            <ta e="T52" id="Seg_3370" s="T51">then</ta>
            <ta e="T53" id="Seg_3371" s="T52">what.[NOM]</ta>
            <ta e="T54" id="Seg_3372" s="T53">fire-3SG.[NOM]</ta>
            <ta e="T55" id="Seg_3373" s="T54">come-FUT.[3SG]=Q</ta>
            <ta e="T56" id="Seg_3374" s="T55">indeed</ta>
            <ta e="T57" id="Seg_3375" s="T56">fire-PROPR.[NOM]</ta>
            <ta e="T58" id="Seg_3376" s="T57">be-FUT-3PL=Q</ta>
            <ta e="T59" id="Seg_3377" s="T58">stove.[NOM]</ta>
            <ta e="T60" id="Seg_3378" s="T59">be-PST1-3SG</ta>
            <ta e="T61" id="Seg_3379" s="T60">then</ta>
            <ta e="T62" id="Seg_3380" s="T61">that.[NOM]</ta>
            <ta e="T63" id="Seg_3381" s="T62">because.of</ta>
            <ta e="T64" id="Seg_3382" s="T63">apparently</ta>
            <ta e="T65" id="Seg_3383" s="T64">outside</ta>
            <ta e="T66" id="Seg_3384" s="T65">sun-PROPR</ta>
            <ta e="T67" id="Seg_3385" s="T66">very</ta>
            <ta e="T68" id="Seg_3386" s="T67">day-DAT/LOC</ta>
            <ta e="T69" id="Seg_3387" s="T68">storage-3SG-DAT/LOC</ta>
            <ta e="T70" id="Seg_3388" s="T69">bedding-3SG-ACC</ta>
            <ta e="T71" id="Seg_3389" s="T70">hang.up-CVB.SEQ</ta>
            <ta e="T72" id="Seg_3390" s="T71">throw-PST2.[3SG]</ta>
            <ta e="T73" id="Seg_3391" s="T72">well</ta>
            <ta e="T74" id="Seg_3392" s="T73">bedding-3SG.[NOM]</ta>
            <ta e="T75" id="Seg_3393" s="T74">MOD</ta>
            <ta e="T76" id="Seg_3394" s="T75">well</ta>
            <ta e="T77" id="Seg_3395" s="T76">tea-3SG-ACC</ta>
            <ta e="T78" id="Seg_3396" s="T77">drink-CVB.SIM</ta>
            <ta e="T79" id="Seg_3397" s="T78">sit-TEMP-3SG</ta>
            <ta e="T80" id="Seg_3398" s="T79">whish</ta>
            <ta e="T81" id="Seg_3399" s="T80">make-PST2-3SG</ta>
            <ta e="T82" id="Seg_3400" s="T81">though</ta>
            <ta e="T83" id="Seg_3401" s="T82">go.out-CVB.SIM</ta>
            <ta e="T84" id="Seg_3402" s="T83">run-PST2-3SG</ta>
            <ta e="T85" id="Seg_3403" s="T84">old.woman.[NOM]</ta>
            <ta e="T86" id="Seg_3404" s="T85">go.out-CVB.SIM</ta>
            <ta e="T87" id="Seg_3405" s="T86">run-PST2-3SG</ta>
            <ta e="T88" id="Seg_3406" s="T87">though</ta>
            <ta e="T89" id="Seg_3407" s="T88">bedding-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_3408" s="T89">fly-CVB.SEQ</ta>
            <ta e="T91" id="Seg_3409" s="T90">be-TEMP-3SG</ta>
            <ta e="T92" id="Seg_3410" s="T91">lake-EP-2SG.[NOM]</ta>
            <ta e="T93" id="Seg_3411" s="T403">water.[NOM]</ta>
            <ta e="T94" id="Seg_3412" s="T93">scoop-PTCP.PRS</ta>
            <ta e="T95" id="Seg_3413" s="T94">small.lake-3SG-GEN</ta>
            <ta e="T96" id="Seg_3414" s="T95">edge-3SG-DAT/LOC</ta>
            <ta e="T98" id="Seg_3415" s="T96">fall-PST2.[3SG]</ta>
            <ta e="T99" id="Seg_3416" s="T98">hey</ta>
            <ta e="T100" id="Seg_3417" s="T99">old.woman.[NOM]</ta>
            <ta e="T101" id="Seg_3418" s="T100">though</ta>
            <ta e="T102" id="Seg_3419" s="T101">go-CVB.SEQ</ta>
            <ta e="T103" id="Seg_3420" s="T102">catch-CVB.SEQ</ta>
            <ta e="T104" id="Seg_3421" s="T103">take-CVB.SEQ</ta>
            <ta e="T105" id="Seg_3422" s="T104">bedding-1SG-ACC</ta>
            <ta e="T106" id="Seg_3423" s="T105">catch-CVB.SEQ</ta>
            <ta e="T107" id="Seg_3424" s="T106">take-CVB.SEQ-1SG</ta>
            <ta e="T108" id="Seg_3425" s="T107">bring-FUT-1SG</ta>
            <ta e="T109" id="Seg_3426" s="T108">think-PST2.[3SG]</ta>
            <ta e="T110" id="Seg_3427" s="T109">EVID</ta>
            <ta e="T111" id="Seg_3428" s="T110">wind-3SG.[NOM]</ta>
            <ta e="T112" id="Seg_3429" s="T111">whish</ta>
            <ta e="T113" id="Seg_3430" s="T112">make-CAUS-CVB.SEQ</ta>
            <ta e="T114" id="Seg_3431" s="T113">ice-DAT/LOC</ta>
            <ta e="T115" id="Seg_3432" s="T114">go.in-PST2.[3SG]</ta>
            <ta e="T116" id="Seg_3433" s="T115">well</ta>
            <ta e="T117" id="Seg_3434" s="T116">in.autumn</ta>
            <ta e="T118" id="Seg_3435" s="T117">well</ta>
            <ta e="T119" id="Seg_3436" s="T118">open</ta>
            <ta e="T120" id="Seg_3437" s="T119">stand-PTCP.PRS</ta>
            <ta e="T121" id="Seg_3438" s="T120">ice-DAT/LOC</ta>
            <ta e="T122" id="Seg_3439" s="T121">this</ta>
            <ta e="T123" id="Seg_3440" s="T122">old.woman.[NOM]</ta>
            <ta e="T124" id="Seg_3441" s="T123">go.in-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_3442" s="T124">this-ACC</ta>
            <ta e="T126" id="Seg_3443" s="T125">chase-CVB.SEQ</ta>
            <ta e="T127" id="Seg_3444" s="T126">run-PRS-1SG</ta>
            <ta e="T128" id="Seg_3445" s="T127">think-CVB.SEQ</ta>
            <ta e="T129" id="Seg_3446" s="T128">slip-CVB.SEQ</ta>
            <ta e="T130" id="Seg_3447" s="T129">fall-CVB.SEQ</ta>
            <ta e="T131" id="Seg_3448" s="T130">hip-3SG-ACC</ta>
            <ta e="T132" id="Seg_3449" s="T131">by.all.means</ta>
            <ta e="T133" id="Seg_3450" s="T132">fall-PST2.[3SG]</ta>
            <ta e="T134" id="Seg_3451" s="T133">old.woman.[NOM]</ta>
            <ta e="T135" id="Seg_3452" s="T134">well</ta>
            <ta e="T136" id="Seg_3453" s="T135">old.woman.[NOM]</ta>
            <ta e="T137" id="Seg_3454" s="T136">EMPH</ta>
            <ta e="T138" id="Seg_3455" s="T137">say-CVB.SEQ</ta>
            <ta e="T139" id="Seg_3456" s="T138">be-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_3457" s="T139">ice.[NOM]</ta>
            <ta e="T141" id="Seg_3458" s="T140">ice.[NOM]</ta>
            <ta e="T142" id="Seg_3459" s="T141">powerful-2SG</ta>
            <ta e="T143" id="Seg_3460" s="T142">Q</ta>
            <ta e="T144" id="Seg_3461" s="T143">say-PRS.[3SG]</ta>
            <ta e="T145" id="Seg_3462" s="T144">AFFIRM</ta>
            <ta e="T146" id="Seg_3463" s="T145">well</ta>
            <ta e="T147" id="Seg_3464" s="T146">powerful-1SG</ta>
            <ta e="T148" id="Seg_3465" s="T147">well</ta>
            <ta e="T149" id="Seg_3466" s="T148">powerful-1SG</ta>
            <ta e="T150" id="Seg_3467" s="T149">2SG.[NOM]</ta>
            <ta e="T151" id="Seg_3468" s="T150">hip-2SG-ACC</ta>
            <ta e="T152" id="Seg_3469" s="T151">break-PST1-1SG</ta>
            <ta e="T153" id="Seg_3470" s="T152">say-CVB.SEQ</ta>
            <ta e="T154" id="Seg_3471" s="T153">such</ta>
            <ta e="T155" id="Seg_3472" s="T154">powerful</ta>
            <ta e="T156" id="Seg_3473" s="T155">human.being.[NOM]</ta>
            <ta e="T157" id="Seg_3474" s="T156">EMPH</ta>
            <ta e="T158" id="Seg_3475" s="T157">why</ta>
            <ta e="T159" id="Seg_3476" s="T158">spring.[NOM]</ta>
            <ta e="T160" id="Seg_3477" s="T159">be-FUT.[3SG]</ta>
            <ta e="T161" id="Seg_3478" s="T160">and</ta>
            <ta e="T162" id="Seg_3479" s="T161">sun.[NOM]</ta>
            <ta e="T163" id="Seg_3480" s="T162">light-3SG-ABL</ta>
            <ta e="T164" id="Seg_3481" s="T163">thaw-CVB.SEQ</ta>
            <ta e="T165" id="Seg_3482" s="T164">be.prolonged-CVB.SEQ</ta>
            <ta e="T166" id="Seg_3483" s="T165">fall-HAB-2SG=Q</ta>
            <ta e="T167" id="Seg_3484" s="T166">say-PST2.[3SG]</ta>
            <ta e="T168" id="Seg_3485" s="T167">sun.[NOM]</ta>
            <ta e="T169" id="Seg_3486" s="T168">light-3SG.[NOM]</ta>
            <ta e="T170" id="Seg_3487" s="T169">powerful.[NOM]</ta>
            <ta e="T171" id="Seg_3488" s="T170">and</ta>
            <ta e="T172" id="Seg_3489" s="T171">powerful.[NOM]</ta>
            <ta e="T173" id="Seg_3490" s="T172">say-PRS.[3SG]</ta>
            <ta e="T174" id="Seg_3491" s="T173">1SG-ACC</ta>
            <ta e="T175" id="Seg_3492" s="T174">thaw-EP-CAUS-EP-NEG-PTCP.FUT-COMP</ta>
            <ta e="T176" id="Seg_3493" s="T175">be-PTCP.FUT</ta>
            <ta e="T177" id="Seg_3494" s="T176">different-different-PL-ACC</ta>
            <ta e="T178" id="Seg_3495" s="T177">thaw-EP-CAUS-HAB.[3SG]</ta>
            <ta e="T179" id="Seg_3496" s="T178">say-PRS.[3SG]</ta>
            <ta e="T180" id="Seg_3497" s="T179">sun.[NOM]</ta>
            <ta e="T181" id="Seg_3498" s="T180">powerful-2SG</ta>
            <ta e="T182" id="Seg_3499" s="T181">Q</ta>
            <ta e="T183" id="Seg_3500" s="T182">well</ta>
            <ta e="T184" id="Seg_3501" s="T183">powerful-1SG</ta>
            <ta e="T185" id="Seg_3502" s="T184">and</ta>
            <ta e="T186" id="Seg_3503" s="T185">power-1SG</ta>
            <ta e="T187" id="Seg_3504" s="T186">say-PRS.[3SG]</ta>
            <ta e="T188" id="Seg_3505" s="T187">snow-ACC</ta>
            <ta e="T189" id="Seg_3506" s="T188">and</ta>
            <ta e="T190" id="Seg_3507" s="T189">ice-ACC</ta>
            <ta e="T191" id="Seg_3508" s="T190">and</ta>
            <ta e="T192" id="Seg_3509" s="T191">remain-EP-CAUS-NEG.CVB.SIM</ta>
            <ta e="T193" id="Seg_3510" s="T192">melt-CAUS-HAB-1SG</ta>
            <ta e="T194" id="Seg_3511" s="T193">say-PRS.[3SG]</ta>
            <ta e="T195" id="Seg_3512" s="T194">jeez</ta>
            <ta e="T196" id="Seg_3513" s="T195">such</ta>
            <ta e="T197" id="Seg_3514" s="T196">powerful</ta>
            <ta e="T198" id="Seg_3515" s="T197">human.being.[NOM]</ta>
            <ta e="T199" id="Seg_3516" s="T198">EMPH</ta>
            <ta e="T200" id="Seg_3517" s="T199">why</ta>
            <ta e="T201" id="Seg_3518" s="T200">stone.[NOM]</ta>
            <ta e="T202" id="Seg_3519" s="T201">mountain.[NOM]</ta>
            <ta e="T203" id="Seg_3520" s="T202">protection-3SG-DAT/LOC</ta>
            <ta e="T204" id="Seg_3521" s="T203">nape-3SG-DAT/LOC</ta>
            <ta e="T205" id="Seg_3522" s="T204">hide-CVB.SIM</ta>
            <ta e="T206" id="Seg_3523" s="T205">lie-HAB-2SG</ta>
            <ta e="T207" id="Seg_3524" s="T206">say-PRS.[3SG]</ta>
            <ta e="T208" id="Seg_3525" s="T207">stone.[NOM]</ta>
            <ta e="T209" id="Seg_3526" s="T208">mountain.[NOM]</ta>
            <ta e="T210" id="Seg_3527" s="T209">powerful.[NOM]</ta>
            <ta e="T211" id="Seg_3528" s="T210">and</ta>
            <ta e="T212" id="Seg_3529" s="T211">powerful.[NOM]</ta>
            <ta e="T213" id="Seg_3530" s="T212">say-PRS.[3SG]</ta>
            <ta e="T214" id="Seg_3531" s="T213">1SG-ACC</ta>
            <ta e="T215" id="Seg_3532" s="T214">front-1SG-ABL</ta>
            <ta e="T216" id="Seg_3533" s="T215">protect-HAB.[3SG]</ta>
            <ta e="T217" id="Seg_3534" s="T216">say-PRS.[3SG]</ta>
            <ta e="T218" id="Seg_3535" s="T217">stone.[NOM]</ta>
            <ta e="T219" id="Seg_3536" s="T218">mountain.[NOM]=EMPH</ta>
            <ta e="T220" id="Seg_3537" s="T219">powerful-2SG</ta>
            <ta e="T221" id="Seg_3538" s="T220">Q</ta>
            <ta e="T222" id="Seg_3539" s="T221">well</ta>
            <ta e="T223" id="Seg_3540" s="T222">powerful-1SG</ta>
            <ta e="T224" id="Seg_3541" s="T223">NEG</ta>
            <ta e="T225" id="Seg_3542" s="T224">powerful-1SG</ta>
            <ta e="T226" id="Seg_3543" s="T225">sun-ACC</ta>
            <ta e="T227" id="Seg_3544" s="T226">EMPH</ta>
            <ta e="T228" id="Seg_3545" s="T227">protect-HAB-1SG</ta>
            <ta e="T229" id="Seg_3546" s="T228">say-PRS.[3SG]</ta>
            <ta e="T230" id="Seg_3547" s="T229">winter-ACC</ta>
            <ta e="T231" id="Seg_3548" s="T230">during</ta>
            <ta e="T232" id="Seg_3549" s="T231">EMPH</ta>
            <ta e="T233" id="Seg_3550" s="T232">darkness.[NOM]</ta>
            <ta e="T234" id="Seg_3551" s="T233">make-HAB-1SG</ta>
            <ta e="T235" id="Seg_3552" s="T234">say-PRS.[3SG]</ta>
            <ta e="T236" id="Seg_3553" s="T235">such</ta>
            <ta e="T237" id="Seg_3554" s="T236">powerful</ta>
            <ta e="T238" id="Seg_3555" s="T237">human.being.[NOM]</ta>
            <ta e="T239" id="Seg_3556" s="T238">EMPH</ta>
            <ta e="T240" id="Seg_3557" s="T239">why</ta>
            <ta e="T241" id="Seg_3558" s="T240">narrowheaded</ta>
            <ta e="T242" id="Seg_3559" s="T241">lemming-PL-DAT/LOC</ta>
            <ta e="T243" id="Seg_3560" s="T242">through</ta>
            <ta e="T244" id="Seg_3561" s="T243">push-EP-CAUS-CVB.SIM</ta>
            <ta e="T245" id="Seg_3562" s="T244">go-PRS-2SG</ta>
            <ta e="T246" id="Seg_3563" s="T245">say-PST2.[3SG]</ta>
            <ta e="T247" id="Seg_3564" s="T246">well</ta>
            <ta e="T248" id="Seg_3565" s="T247">narrowheaded</ta>
            <ta e="T249" id="Seg_3566" s="T248">lemming-PL.[NOM]</ta>
            <ta e="T250" id="Seg_3567" s="T249">powerful-PL.[NOM]</ta>
            <ta e="T251" id="Seg_3568" s="T250">and</ta>
            <ta e="T252" id="Seg_3569" s="T251">powerful-PL.[NOM]</ta>
            <ta e="T253" id="Seg_3570" s="T252">very</ta>
            <ta e="T254" id="Seg_3571" s="T253">sharp</ta>
            <ta e="T255" id="Seg_3572" s="T254">snout-PROPR-PL.[NOM]</ta>
            <ta e="T256" id="Seg_3573" s="T255">say-PRS.[3SG]</ta>
            <ta e="T257" id="Seg_3574" s="T256">lemming-PL.[NOM]</ta>
            <ta e="T258" id="Seg_3575" s="T257">powerful-2PL</ta>
            <ta e="T259" id="Seg_3576" s="T258">Q</ta>
            <ta e="T260" id="Seg_3577" s="T259">powerful-1PL</ta>
            <ta e="T261" id="Seg_3578" s="T260">and</ta>
            <ta e="T262" id="Seg_3579" s="T261">powerful-1PL</ta>
            <ta e="T263" id="Seg_3580" s="T262">stone.[NOM]</ta>
            <ta e="T264" id="Seg_3581" s="T263">mountain-ACC</ta>
            <ta e="T265" id="Seg_3582" s="T264">which.way</ta>
            <ta e="T266" id="Seg_3583" s="T265">think-PTCP.PRS.[NOM]</ta>
            <ta e="T267" id="Seg_3584" s="T266">go-CVB.SIM</ta>
            <ta e="T268" id="Seg_3585" s="T267">go.out-HAB-1PL</ta>
            <ta e="T269" id="Seg_3586" s="T268">say-CVB.SIM</ta>
            <ta e="T270" id="Seg_3587" s="T269">such</ta>
            <ta e="T271" id="Seg_3588" s="T270">powerful.[NOM]</ta>
            <ta e="T272" id="Seg_3589" s="T271">human.being-PL.[NOM]</ta>
            <ta e="T273" id="Seg_3590" s="T272">EMPH</ta>
            <ta e="T274" id="Seg_3591" s="T273">why</ta>
            <ta e="T275" id="Seg_3592" s="T274">polar.fox-DAT/LOC</ta>
            <ta e="T276" id="Seg_3593" s="T275">eat-CAUS-HAB-2PL=Q</ta>
            <ta e="T277" id="Seg_3594" s="T276">say-PST2.[3SG]</ta>
            <ta e="T278" id="Seg_3595" s="T277">polar.fox.[NOM]</ta>
            <ta e="T279" id="Seg_3596" s="T278">polar.fox.[NOM]</ta>
            <ta e="T280" id="Seg_3597" s="T279">eat-NEG.CVB</ta>
            <ta e="T281" id="Seg_3598" s="T280">say-PRS.[3SG]</ta>
            <ta e="T282" id="Seg_3599" s="T281">1PL-ACC</ta>
            <ta e="T283" id="Seg_3600" s="T282">eat-HAB.[3SG]</ta>
            <ta e="T284" id="Seg_3601" s="T283">and</ta>
            <ta e="T285" id="Seg_3602" s="T284">eat-HAB.[3SG]</ta>
            <ta e="T286" id="Seg_3603" s="T285">polar.fox.[NOM]</ta>
            <ta e="T287" id="Seg_3604" s="T286">polar.fox.[NOM]</ta>
            <ta e="T288" id="Seg_3605" s="T287">powerful-2SG</ta>
            <ta e="T289" id="Seg_3606" s="T288">Q</ta>
            <ta e="T290" id="Seg_3607" s="T289">powerful-1SG</ta>
            <ta e="T291" id="Seg_3608" s="T290">and</ta>
            <ta e="T292" id="Seg_3609" s="T291">powerful-1SG</ta>
            <ta e="T293" id="Seg_3610" s="T292">say-PRS.[3SG]</ta>
            <ta e="T294" id="Seg_3611" s="T293">lemming-ACC</ta>
            <ta e="T295" id="Seg_3612" s="T294">where.from</ta>
            <ta e="T296" id="Seg_3613" s="T295">think-PTCP.PRS.[NOM]</ta>
            <ta e="T297" id="Seg_3614" s="T296">dig-CVB.SEQ</ta>
            <ta e="T298" id="Seg_3615" s="T297">take-CVB.SEQ</ta>
            <ta e="T299" id="Seg_3616" s="T298">eat-HAB-1PL</ta>
            <ta e="T300" id="Seg_3617" s="T299">such</ta>
            <ta e="T301" id="Seg_3618" s="T300">powerful.[NOM]</ta>
            <ta e="T302" id="Seg_3619" s="T301">human.being.[NOM]</ta>
            <ta e="T303" id="Seg_3620" s="T302">EMPH</ta>
            <ta e="T304" id="Seg_3621" s="T303">trap-PROPR</ta>
            <ta e="T305" id="Seg_3622" s="T304">Dolgan</ta>
            <ta e="T306" id="Seg_3623" s="T305">child-PL-DAT/LOC</ta>
            <ta e="T307" id="Seg_3624" s="T306">why</ta>
            <ta e="T308" id="Seg_3625" s="T307">catch-CAUS-HAB-2SG=Q</ta>
            <ta e="T309" id="Seg_3626" s="T308">say-PST2.[3SG]</ta>
            <ta e="T310" id="Seg_3627" s="T309">trap-PROPR</ta>
            <ta e="T311" id="Seg_3628" s="T310">Dolgan</ta>
            <ta e="T312" id="Seg_3629" s="T311">child-3PL.[NOM]</ta>
            <ta e="T313" id="Seg_3630" s="T312">snow.[NOM]</ta>
            <ta e="T314" id="Seg_3631" s="T313">lower.part-3SG-DAT/LOC</ta>
            <ta e="T315" id="Seg_3632" s="T314">bury-PTCP.PST-3PL-ACC</ta>
            <ta e="T316" id="Seg_3633" s="T315">where.from</ta>
            <ta e="T317" id="Seg_3634" s="T316">notice-FUT-1SG=Q</ta>
            <ta e="T318" id="Seg_3635" s="T317">gin.trap-ACC</ta>
            <ta e="T319" id="Seg_3636" s="T318">say-PRS.[3SG]</ta>
            <ta e="T320" id="Seg_3637" s="T319">that.[NOM]</ta>
            <ta e="T321" id="Seg_3638" s="T320">because.of</ta>
            <ta e="T322" id="Seg_3639" s="T321">grab-CAUS-HAB-1SG</ta>
            <ta e="T323" id="Seg_3640" s="T322">say-PRS.[3SG]</ta>
            <ta e="T324" id="Seg_3641" s="T323">eat-CVB.SIM</ta>
            <ta e="T325" id="Seg_3642" s="T324">come-CVB.SEQ-1SG</ta>
            <ta e="T326" id="Seg_3643" s="T325">say-PRS.[3SG]</ta>
            <ta e="T327" id="Seg_3644" s="T326">trap-PROPR</ta>
            <ta e="T328" id="Seg_3645" s="T327">Dolgan</ta>
            <ta e="T329" id="Seg_3646" s="T328">child-3PL.[NOM]</ta>
            <ta e="T330" id="Seg_3647" s="T329">powerful-2PL</ta>
            <ta e="T331" id="Seg_3648" s="T330">Q</ta>
            <ta e="T332" id="Seg_3649" s="T331">powerful-1PL</ta>
            <ta e="T333" id="Seg_3650" s="T332">and</ta>
            <ta e="T334" id="Seg_3651" s="T333">powerful-1PL</ta>
            <ta e="T335" id="Seg_3652" s="T334">polar.fox-ACC</ta>
            <ta e="T336" id="Seg_3653" s="T335">when</ta>
            <ta e="T337" id="Seg_3654" s="T336">think-PTCP.PRS.[NOM]</ta>
            <ta e="T338" id="Seg_3655" s="T337">catch-HAB-1PL</ta>
            <ta e="T339" id="Seg_3656" s="T338">such</ta>
            <ta e="T340" id="Seg_3657" s="T339">powerful.[NOM]</ta>
            <ta e="T341" id="Seg_3658" s="T340">human.being-PL.[NOM]</ta>
            <ta e="T342" id="Seg_3659" s="T341">well</ta>
            <ta e="T343" id="Seg_3660" s="T342">why</ta>
            <ta e="T344" id="Seg_3661" s="T343">shaman-DAT/LOC</ta>
            <ta e="T345" id="Seg_3662" s="T344">eat-CAUS-HAB-2PL=Q</ta>
            <ta e="T346" id="Seg_3663" s="T345">say-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_3664" s="T346">shaman.[NOM]</ta>
            <ta e="T348" id="Seg_3665" s="T347">experienced.[NOM]</ta>
            <ta e="T349" id="Seg_3666" s="T348">be-PST1-3SG</ta>
            <ta e="T350" id="Seg_3667" s="T349">say-PST2.[3SG]</ta>
            <ta e="T351" id="Seg_3668" s="T350">eat-FUT-1SG</ta>
            <ta e="T352" id="Seg_3669" s="T351">say-FUT-3SG</ta>
            <ta e="T353" id="Seg_3670" s="T352">and</ta>
            <ta e="T354" id="Seg_3671" s="T353">eat-HAB.[3SG]</ta>
            <ta e="T355" id="Seg_3672" s="T354">say-CVB.SIM</ta>
            <ta e="T356" id="Seg_3673" s="T355">shaman.[NOM]</ta>
            <ta e="T357" id="Seg_3674" s="T356">powerful-2SG</ta>
            <ta e="T358" id="Seg_3675" s="T357">Q</ta>
            <ta e="T359" id="Seg_3676" s="T358">powerful-1SG</ta>
            <ta e="T360" id="Seg_3677" s="T359">NEG</ta>
            <ta e="T361" id="Seg_3678" s="T360">powerful-1SG</ta>
            <ta e="T362" id="Seg_3679" s="T361">when</ta>
            <ta e="T363" id="Seg_3680" s="T362">wish-PST1-1SG</ta>
            <ta e="T364" id="Seg_3681" s="T363">and</ta>
            <ta e="T365" id="Seg_3682" s="T364">eat-HAB-1SG</ta>
            <ta e="T366" id="Seg_3683" s="T365">bad-ADVZ</ta>
            <ta e="T367" id="Seg_3684" s="T366">speak-PTCP.PST</ta>
            <ta e="T368" id="Seg_3685" s="T367">human.being.[NOM]</ta>
            <ta e="T369" id="Seg_3686" s="T368">be-FUT.[3SG]</ta>
            <ta e="T370" id="Seg_3687" s="T369">and</ta>
            <ta e="T371" id="Seg_3688" s="T370">say-PRS.[3SG]</ta>
            <ta e="T372" id="Seg_3689" s="T371">such</ta>
            <ta e="T373" id="Seg_3690" s="T372">powerful</ta>
            <ta e="T374" id="Seg_3691" s="T373">human.being.[NOM]</ta>
            <ta e="T375" id="Seg_3692" s="T374">EMPH</ta>
            <ta e="T376" id="Seg_3693" s="T375">why</ta>
            <ta e="T377" id="Seg_3694" s="T376">death-DAT/LOC</ta>
            <ta e="T378" id="Seg_3695" s="T377">eat-CAUS-HAB-2SG=Q</ta>
            <ta e="T379" id="Seg_3696" s="T378">say-PRS.[3SG]</ta>
            <ta e="T380" id="Seg_3697" s="T379">death.[NOM]</ta>
            <ta e="T381" id="Seg_3698" s="T380">this</ta>
            <ta e="T382" id="Seg_3699" s="T381">who-DAT/LOC</ta>
            <ta e="T383" id="Seg_3700" s="T382">NEG</ta>
            <ta e="T384" id="Seg_3701" s="T383">word-VBZ-HAB-3SG</ta>
            <ta e="T385" id="Seg_3702" s="T384">NEG.[3SG]</ta>
            <ta e="T386" id="Seg_3703" s="T385">say-PST2.[3SG]</ta>
            <ta e="T387" id="Seg_3704" s="T386">death.[NOM]</ta>
            <ta e="T388" id="Seg_3705" s="T387">powerful-2SG</ta>
            <ta e="T389" id="Seg_3706" s="T388">Q</ta>
            <ta e="T390" id="Seg_3707" s="T389">say-PRS.[3SG]</ta>
            <ta e="T391" id="Seg_3708" s="T390">powerful-1SG</ta>
            <ta e="T392" id="Seg_3709" s="T391">and</ta>
            <ta e="T393" id="Seg_3710" s="T392">powerful-1SG</ta>
            <ta e="T394" id="Seg_3711" s="T393">2SG-ACC</ta>
            <ta e="T395" id="Seg_3712" s="T394">and</ta>
            <ta e="T396" id="Seg_3713" s="T395">eat-FUT-1SG</ta>
            <ta e="T397" id="Seg_3714" s="T396">say-PST2-3SG</ta>
            <ta e="T398" id="Seg_3715" s="T397">eat-CVB.SEQ</ta>
            <ta e="T399" id="Seg_3716" s="T398">throw-PST2-3SG</ta>
            <ta e="T400" id="Seg_3717" s="T399">well</ta>
            <ta e="T401" id="Seg_3718" s="T400">that.[NOM]</ta>
            <ta e="T402" id="Seg_3719" s="T401">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_3720" s="T0">Taal.[NOM]</ta>
            <ta e="T2" id="Seg_3721" s="T1">Alte.[NOM]</ta>
            <ta e="T3" id="Seg_3722" s="T2">leben-PST2.[3SG]</ta>
            <ta e="T4" id="Seg_3723" s="T3">man.sagt</ta>
            <ta e="T5" id="Seg_3724" s="T4">Ornament-PROPR</ta>
            <ta e="T6" id="Seg_3725" s="T5">Farbe-PROPR</ta>
            <ta e="T7" id="Seg_3726" s="T6">mittlerer</ta>
            <ta e="T8" id="Seg_3727" s="T7">neun</ta>
            <ta e="T9" id="Seg_3728" s="T8">Welt-DAT/LOC</ta>
            <ta e="T10" id="Seg_3729" s="T9">Taal.[NOM]</ta>
            <ta e="T11" id="Seg_3730" s="T10">Alte.[NOM]</ta>
            <ta e="T12" id="Seg_3731" s="T11">dieses</ta>
            <ta e="T13" id="Seg_3732" s="T12">Taal.[NOM]</ta>
            <ta e="T14" id="Seg_3733" s="T13">Alte.[NOM]</ta>
            <ta e="T15" id="Seg_3734" s="T14">aber</ta>
            <ta e="T16" id="Seg_3735" s="T15">einmal</ta>
            <ta e="T17" id="Seg_3736" s="T16">Abend-INSTR</ta>
            <ta e="T18" id="Seg_3737" s="T17">na</ta>
            <ta e="T19" id="Seg_3738" s="T18">Tee.[NOM]</ta>
            <ta e="T20" id="Seg_3739" s="T19">trinken-EP-MED-CVB.SEQ-trinken-EP-MED-CVB.SEQ</ta>
            <ta e="T21" id="Seg_3740" s="T20">nachdem</ta>
            <ta e="T23" id="Seg_3741" s="T21">schlafen-CVB.SEQ</ta>
            <ta e="T24" id="Seg_3742" s="T23">einschlafen-PST2.[3SG]</ta>
            <ta e="T25" id="Seg_3743" s="T24">Q</ta>
            <ta e="T26" id="Seg_3744" s="T25">wie</ta>
            <ta e="T27" id="Seg_3745" s="T26">Q</ta>
            <ta e="T28" id="Seg_3746" s="T27">Rand.des.Zelts-3SG.[NOM]</ta>
            <ta e="T29" id="Seg_3747" s="T28">tropfen-EP-PST2-3SG</ta>
            <ta e="T30" id="Seg_3748" s="T29">Q</ta>
            <ta e="T31" id="Seg_3749" s="T30">Regen.[NOM]</ta>
            <ta e="T32" id="Seg_3750" s="T31">fallen-PST2.[3SG]</ta>
            <ta e="T33" id="Seg_3751" s="T32">Q</ta>
            <ta e="T34" id="Seg_3752" s="T33">was.[NOM]</ta>
            <ta e="T35" id="Seg_3753" s="T34">Q</ta>
            <ta e="T36" id="Seg_3754" s="T35">Bettwäsche-3SG.[NOM]</ta>
            <ta e="T37" id="Seg_3755" s="T36">nass.werden-PST2.[3SG]</ta>
            <ta e="T38" id="Seg_3756" s="T37">Alte.[NOM]</ta>
            <ta e="T39" id="Seg_3757" s="T38">eigen-3SG.[NOM]</ta>
            <ta e="T40" id="Seg_3758" s="T39">dieses</ta>
            <ta e="T41" id="Seg_3759" s="T40">Bettwäsche-3SG.[NOM]</ta>
            <ta e="T42" id="Seg_3760" s="T41">nass.werden-CVB.SEQ</ta>
            <ta e="T43" id="Seg_3761" s="T42">Alte.[NOM]</ta>
            <ta e="T44" id="Seg_3762" s="T43">am.Morgen</ta>
            <ta e="T45" id="Seg_3763" s="T44">Herbst.[NOM]</ta>
            <ta e="T46" id="Seg_3764" s="T45">sein-PST2.[3SG]</ta>
            <ta e="T47" id="Seg_3765" s="T46">Eis.[NOM]</ta>
            <ta e="T48" id="Seg_3766" s="T47">frieren-PTCP.PRS-3SG-ACC</ta>
            <ta e="T49" id="Seg_3767" s="T48">als</ta>
            <ta e="T50" id="Seg_3768" s="T49">Alte.[NOM]</ta>
            <ta e="T51" id="Seg_3769" s="T50">aufstehen-CVB.SEQ</ta>
            <ta e="T52" id="Seg_3770" s="T51">dann</ta>
            <ta e="T53" id="Seg_3771" s="T52">was.[NOM]</ta>
            <ta e="T54" id="Seg_3772" s="T53">Feuer-3SG.[NOM]</ta>
            <ta e="T55" id="Seg_3773" s="T54">kommen-FUT.[3SG]=Q</ta>
            <ta e="T56" id="Seg_3774" s="T55">tatsächlich</ta>
            <ta e="T57" id="Seg_3775" s="T56">Feuer-PROPR.[NOM]</ta>
            <ta e="T58" id="Seg_3776" s="T57">sein-FUT-3PL=Q</ta>
            <ta e="T59" id="Seg_3777" s="T58">Herd.[NOM]</ta>
            <ta e="T60" id="Seg_3778" s="T59">sein-PST1-3SG</ta>
            <ta e="T61" id="Seg_3779" s="T60">dann</ta>
            <ta e="T62" id="Seg_3780" s="T61">jenes.[NOM]</ta>
            <ta e="T63" id="Seg_3781" s="T62">wegen</ta>
            <ta e="T64" id="Seg_3782" s="T63">offenbar</ta>
            <ta e="T65" id="Seg_3783" s="T64">draußen</ta>
            <ta e="T66" id="Seg_3784" s="T65">Sonne-PROPR</ta>
            <ta e="T67" id="Seg_3785" s="T66">sehr</ta>
            <ta e="T68" id="Seg_3786" s="T67">Tag-DAT/LOC</ta>
            <ta e="T69" id="Seg_3787" s="T68">Speicher-3SG-DAT/LOC</ta>
            <ta e="T70" id="Seg_3788" s="T69">Bettwäsche-3SG-ACC</ta>
            <ta e="T71" id="Seg_3789" s="T70">aufhängen-CVB.SEQ</ta>
            <ta e="T72" id="Seg_3790" s="T71">werfen-PST2.[3SG]</ta>
            <ta e="T73" id="Seg_3791" s="T72">na</ta>
            <ta e="T74" id="Seg_3792" s="T73">Bettwäsche-3SG.[NOM]</ta>
            <ta e="T75" id="Seg_3793" s="T74">MOD</ta>
            <ta e="T76" id="Seg_3794" s="T75">na</ta>
            <ta e="T77" id="Seg_3795" s="T76">Tee-3SG-ACC</ta>
            <ta e="T78" id="Seg_3796" s="T77">trinken-CVB.SIM</ta>
            <ta e="T79" id="Seg_3797" s="T78">sitzen-TEMP-3SG</ta>
            <ta e="T80" id="Seg_3798" s="T79">hui</ta>
            <ta e="T81" id="Seg_3799" s="T80">machen-PST2-3SG</ta>
            <ta e="T82" id="Seg_3800" s="T81">aber</ta>
            <ta e="T83" id="Seg_3801" s="T82">hinausgehen-CVB.SIM</ta>
            <ta e="T84" id="Seg_3802" s="T83">rennen-PST2-3SG</ta>
            <ta e="T85" id="Seg_3803" s="T84">Alte.[NOM]</ta>
            <ta e="T86" id="Seg_3804" s="T85">hinausgehen-CVB.SIM</ta>
            <ta e="T87" id="Seg_3805" s="T86">rennen-PST2-3SG</ta>
            <ta e="T88" id="Seg_3806" s="T87">aber</ta>
            <ta e="T89" id="Seg_3807" s="T88">Bettwäsche-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_3808" s="T89">fliegen-CVB.SEQ</ta>
            <ta e="T91" id="Seg_3809" s="T90">sein-TEMP-3SG</ta>
            <ta e="T92" id="Seg_3810" s="T91">See-EP-2SG.[NOM]</ta>
            <ta e="T93" id="Seg_3811" s="T403">Wasser.[NOM]</ta>
            <ta e="T94" id="Seg_3812" s="T93">schöpfen-PTCP.PRS</ta>
            <ta e="T95" id="Seg_3813" s="T94">kleiner.See-3SG-GEN</ta>
            <ta e="T96" id="Seg_3814" s="T95">Rand-3SG-DAT/LOC</ta>
            <ta e="T98" id="Seg_3815" s="T96">fallen-PST2.[3SG]</ta>
            <ta e="T99" id="Seg_3816" s="T98">hey</ta>
            <ta e="T100" id="Seg_3817" s="T99">Alte.[NOM]</ta>
            <ta e="T101" id="Seg_3818" s="T100">aber</ta>
            <ta e="T102" id="Seg_3819" s="T101">gehen-CVB.SEQ</ta>
            <ta e="T103" id="Seg_3820" s="T102">fangen-CVB.SEQ</ta>
            <ta e="T104" id="Seg_3821" s="T103">nehmen-CVB.SEQ</ta>
            <ta e="T105" id="Seg_3822" s="T104">Bettwäsche-1SG-ACC</ta>
            <ta e="T106" id="Seg_3823" s="T105">fangen-CVB.SEQ</ta>
            <ta e="T107" id="Seg_3824" s="T106">nehmen-CVB.SEQ-1SG</ta>
            <ta e="T108" id="Seg_3825" s="T107">bringen-FUT-1SG</ta>
            <ta e="T109" id="Seg_3826" s="T108">denken-PST2.[3SG]</ta>
            <ta e="T110" id="Seg_3827" s="T109">EVID</ta>
            <ta e="T111" id="Seg_3828" s="T110">Wind-3SG.[NOM]</ta>
            <ta e="T112" id="Seg_3829" s="T111">hui</ta>
            <ta e="T113" id="Seg_3830" s="T112">machen-CAUS-CVB.SEQ</ta>
            <ta e="T114" id="Seg_3831" s="T113">Eis-DAT/LOC</ta>
            <ta e="T115" id="Seg_3832" s="T114">hineingehen-PST2.[3SG]</ta>
            <ta e="T116" id="Seg_3833" s="T115">na</ta>
            <ta e="T117" id="Seg_3834" s="T116">im.Herbst</ta>
            <ta e="T118" id="Seg_3835" s="T117">nun</ta>
            <ta e="T119" id="Seg_3836" s="T118">offen</ta>
            <ta e="T120" id="Seg_3837" s="T119">stehen-PTCP.PRS</ta>
            <ta e="T121" id="Seg_3838" s="T120">Eis-DAT/LOC</ta>
            <ta e="T122" id="Seg_3839" s="T121">dieses</ta>
            <ta e="T123" id="Seg_3840" s="T122">Alte.[NOM]</ta>
            <ta e="T124" id="Seg_3841" s="T123">hineingehen-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_3842" s="T124">dieses-ACC</ta>
            <ta e="T126" id="Seg_3843" s="T125">jagen-CVB.SEQ</ta>
            <ta e="T127" id="Seg_3844" s="T126">laufen-PRS-1SG</ta>
            <ta e="T128" id="Seg_3845" s="T127">denken-CVB.SEQ</ta>
            <ta e="T129" id="Seg_3846" s="T128">ausrutschen-CVB.SEQ</ta>
            <ta e="T130" id="Seg_3847" s="T129">fallen-CVB.SEQ</ta>
            <ta e="T131" id="Seg_3848" s="T130">Hüfte-3SG-ACC</ta>
            <ta e="T132" id="Seg_3849" s="T131">unbedingt</ta>
            <ta e="T133" id="Seg_3850" s="T132">fallen-PST2.[3SG]</ta>
            <ta e="T134" id="Seg_3851" s="T133">Alte.[NOM]</ta>
            <ta e="T135" id="Seg_3852" s="T134">na</ta>
            <ta e="T136" id="Seg_3853" s="T135">Alte.[NOM]</ta>
            <ta e="T137" id="Seg_3854" s="T136">EMPH</ta>
            <ta e="T138" id="Seg_3855" s="T137">sagen-CVB.SEQ</ta>
            <ta e="T139" id="Seg_3856" s="T138">sein-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_3857" s="T139">Eis.[NOM]</ta>
            <ta e="T141" id="Seg_3858" s="T140">Eis.[NOM]</ta>
            <ta e="T142" id="Seg_3859" s="T141">kräftig-2SG</ta>
            <ta e="T143" id="Seg_3860" s="T142">Q</ta>
            <ta e="T144" id="Seg_3861" s="T143">sagen-PRS.[3SG]</ta>
            <ta e="T145" id="Seg_3862" s="T144">AFFIRM</ta>
            <ta e="T146" id="Seg_3863" s="T145">doch</ta>
            <ta e="T147" id="Seg_3864" s="T146">kräftig-1SG</ta>
            <ta e="T148" id="Seg_3865" s="T147">doch</ta>
            <ta e="T149" id="Seg_3866" s="T148">kräftig-1SG</ta>
            <ta e="T150" id="Seg_3867" s="T149">2SG.[NOM]</ta>
            <ta e="T151" id="Seg_3868" s="T150">Hüfte-2SG-ACC</ta>
            <ta e="T152" id="Seg_3869" s="T151">brechen-PST1-1SG</ta>
            <ta e="T153" id="Seg_3870" s="T152">sagen-CVB.SEQ</ta>
            <ta e="T154" id="Seg_3871" s="T153">solch</ta>
            <ta e="T155" id="Seg_3872" s="T154">kräftig</ta>
            <ta e="T156" id="Seg_3873" s="T155">Mensch.[NOM]</ta>
            <ta e="T157" id="Seg_3874" s="T156">EMPH</ta>
            <ta e="T158" id="Seg_3875" s="T157">warum</ta>
            <ta e="T159" id="Seg_3876" s="T158">Frühling.[NOM]</ta>
            <ta e="T160" id="Seg_3877" s="T159">sein-FUT.[3SG]</ta>
            <ta e="T161" id="Seg_3878" s="T160">und</ta>
            <ta e="T162" id="Seg_3879" s="T161">Sonne.[NOM]</ta>
            <ta e="T163" id="Seg_3880" s="T162">Licht-3SG-ABL</ta>
            <ta e="T164" id="Seg_3881" s="T163">tauen-CVB.SEQ</ta>
            <ta e="T165" id="Seg_3882" s="T164">langgezogen.sein-CVB.SEQ</ta>
            <ta e="T166" id="Seg_3883" s="T165">fallen-HAB-2SG=Q</ta>
            <ta e="T167" id="Seg_3884" s="T166">sagen-PST2.[3SG]</ta>
            <ta e="T168" id="Seg_3885" s="T167">Sonne.[NOM]</ta>
            <ta e="T169" id="Seg_3886" s="T168">Licht-3SG.[NOM]</ta>
            <ta e="T170" id="Seg_3887" s="T169">kräftig.[NOM]</ta>
            <ta e="T171" id="Seg_3888" s="T170">und</ta>
            <ta e="T172" id="Seg_3889" s="T171">kräftig.[NOM]</ta>
            <ta e="T173" id="Seg_3890" s="T172">sagen-PRS.[3SG]</ta>
            <ta e="T174" id="Seg_3891" s="T173">1SG-ACC</ta>
            <ta e="T175" id="Seg_3892" s="T174">tauen-EP-CAUS-EP-NEG-PTCP.FUT-COMP</ta>
            <ta e="T176" id="Seg_3893" s="T175">sein-PTCP.FUT</ta>
            <ta e="T177" id="Seg_3894" s="T176">unterschiedlich-unterschiedlich-PL-ACC</ta>
            <ta e="T178" id="Seg_3895" s="T177">tauen-EP-CAUS-HAB.[3SG]</ta>
            <ta e="T179" id="Seg_3896" s="T178">sagen-PRS.[3SG]</ta>
            <ta e="T180" id="Seg_3897" s="T179">Sonne.[NOM]</ta>
            <ta e="T181" id="Seg_3898" s="T180">kräftig-2SG</ta>
            <ta e="T182" id="Seg_3899" s="T181">Q</ta>
            <ta e="T183" id="Seg_3900" s="T182">doch</ta>
            <ta e="T184" id="Seg_3901" s="T183">kräftig-1SG</ta>
            <ta e="T185" id="Seg_3902" s="T184">und</ta>
            <ta e="T186" id="Seg_3903" s="T185">kräftig-1SG</ta>
            <ta e="T187" id="Seg_3904" s="T186">sagen-PRS.[3SG]</ta>
            <ta e="T188" id="Seg_3905" s="T187">Schnee-ACC</ta>
            <ta e="T189" id="Seg_3906" s="T188">und</ta>
            <ta e="T190" id="Seg_3907" s="T189">Eis-ACC</ta>
            <ta e="T191" id="Seg_3908" s="T190">und</ta>
            <ta e="T192" id="Seg_3909" s="T191">übrig.bleiben-EP-CAUS-NEG.CVB.SIM</ta>
            <ta e="T193" id="Seg_3910" s="T192">schmelzen-CAUS-HAB-1SG</ta>
            <ta e="T194" id="Seg_3911" s="T193">sagen-PRS.[3SG]</ta>
            <ta e="T195" id="Seg_3912" s="T194">ui</ta>
            <ta e="T196" id="Seg_3913" s="T195">solch</ta>
            <ta e="T197" id="Seg_3914" s="T196">kräftig</ta>
            <ta e="T198" id="Seg_3915" s="T197">Mensch.[NOM]</ta>
            <ta e="T199" id="Seg_3916" s="T198">EMPH</ta>
            <ta e="T200" id="Seg_3917" s="T199">warum</ta>
            <ta e="T201" id="Seg_3918" s="T200">Stein.[NOM]</ta>
            <ta e="T202" id="Seg_3919" s="T201">Berg.[NOM]</ta>
            <ta e="T203" id="Seg_3920" s="T202">Schutz-3SG-DAT/LOC</ta>
            <ta e="T204" id="Seg_3921" s="T203">Nacken-3SG-DAT/LOC</ta>
            <ta e="T205" id="Seg_3922" s="T204">sich.verstecken-CVB.SIM</ta>
            <ta e="T206" id="Seg_3923" s="T205">liegen-HAB-2SG</ta>
            <ta e="T207" id="Seg_3924" s="T206">sagen-PRS.[3SG]</ta>
            <ta e="T208" id="Seg_3925" s="T207">Stein.[NOM]</ta>
            <ta e="T209" id="Seg_3926" s="T208">Berg.[NOM]</ta>
            <ta e="T210" id="Seg_3927" s="T209">kräftig.[NOM]</ta>
            <ta e="T211" id="Seg_3928" s="T210">und</ta>
            <ta e="T212" id="Seg_3929" s="T211">kräftig.[NOM]</ta>
            <ta e="T213" id="Seg_3930" s="T212">sagen-PRS.[3SG]</ta>
            <ta e="T214" id="Seg_3931" s="T213">1SG-ACC</ta>
            <ta e="T215" id="Seg_3932" s="T214">Vorderteil-1SG-ABL</ta>
            <ta e="T216" id="Seg_3933" s="T215">beschützen-HAB.[3SG]</ta>
            <ta e="T217" id="Seg_3934" s="T216">sagen-PRS.[3SG]</ta>
            <ta e="T218" id="Seg_3935" s="T217">Stein.[NOM]</ta>
            <ta e="T219" id="Seg_3936" s="T218">Berg.[NOM]=EMPH</ta>
            <ta e="T220" id="Seg_3937" s="T219">kräftig-2SG</ta>
            <ta e="T221" id="Seg_3938" s="T220">Q</ta>
            <ta e="T222" id="Seg_3939" s="T221">doch</ta>
            <ta e="T223" id="Seg_3940" s="T222">kräftig-1SG</ta>
            <ta e="T224" id="Seg_3941" s="T223">NEG</ta>
            <ta e="T225" id="Seg_3942" s="T224">kräftig-1SG</ta>
            <ta e="T226" id="Seg_3943" s="T225">Sonne-ACC</ta>
            <ta e="T227" id="Seg_3944" s="T226">EMPH</ta>
            <ta e="T228" id="Seg_3945" s="T227">beschützen-HAB-1SG</ta>
            <ta e="T229" id="Seg_3946" s="T228">sagen-PRS.[3SG]</ta>
            <ta e="T230" id="Seg_3947" s="T229">Winter-ACC</ta>
            <ta e="T231" id="Seg_3948" s="T230">während</ta>
            <ta e="T232" id="Seg_3949" s="T231">EMPH</ta>
            <ta e="T233" id="Seg_3950" s="T232">Dunkelheit.[NOM]</ta>
            <ta e="T234" id="Seg_3951" s="T233">machen-HAB-1SG</ta>
            <ta e="T235" id="Seg_3952" s="T234">sagen-PRS.[3SG]</ta>
            <ta e="T236" id="Seg_3953" s="T235">solch</ta>
            <ta e="T237" id="Seg_3954" s="T236">kräftig</ta>
            <ta e="T238" id="Seg_3955" s="T237">Mensch.[NOM]</ta>
            <ta e="T239" id="Seg_3956" s="T238">EMPH</ta>
            <ta e="T240" id="Seg_3957" s="T239">warum</ta>
            <ta e="T241" id="Seg_3958" s="T240">spitzmäulig</ta>
            <ta e="T242" id="Seg_3959" s="T241">Lemming-PL-DAT/LOC</ta>
            <ta e="T243" id="Seg_3960" s="T242">durch</ta>
            <ta e="T244" id="Seg_3961" s="T243">stoßen-EP-CAUS-CVB.SIM</ta>
            <ta e="T245" id="Seg_3962" s="T244">gehen-PRS-2SG</ta>
            <ta e="T246" id="Seg_3963" s="T245">sagen-PST2.[3SG]</ta>
            <ta e="T247" id="Seg_3964" s="T246">doch</ta>
            <ta e="T248" id="Seg_3965" s="T247">spitzmäulig</ta>
            <ta e="T249" id="Seg_3966" s="T248">Lemming-PL.[NOM]</ta>
            <ta e="T250" id="Seg_3967" s="T249">kräftig-PL.[NOM]</ta>
            <ta e="T251" id="Seg_3968" s="T250">und</ta>
            <ta e="T252" id="Seg_3969" s="T251">kräftig-PL.[NOM]</ta>
            <ta e="T253" id="Seg_3970" s="T252">sehr</ta>
            <ta e="T254" id="Seg_3971" s="T253">scharf</ta>
            <ta e="T255" id="Seg_3972" s="T254">Schnauze-PROPR-PL.[NOM]</ta>
            <ta e="T256" id="Seg_3973" s="T255">sagen-PRS.[3SG]</ta>
            <ta e="T257" id="Seg_3974" s="T256">Lemming-PL.[NOM]</ta>
            <ta e="T258" id="Seg_3975" s="T257">kräftig-2PL</ta>
            <ta e="T259" id="Seg_3976" s="T258">Q</ta>
            <ta e="T260" id="Seg_3977" s="T259">kräftig-1PL</ta>
            <ta e="T261" id="Seg_3978" s="T260">und</ta>
            <ta e="T262" id="Seg_3979" s="T261">kräftig-1PL</ta>
            <ta e="T263" id="Seg_3980" s="T262">Stein.[NOM]</ta>
            <ta e="T264" id="Seg_3981" s="T263">Berg-ACC</ta>
            <ta e="T265" id="Seg_3982" s="T264">wo.entlang</ta>
            <ta e="T266" id="Seg_3983" s="T265">denken-PTCP.PRS.[NOM]</ta>
            <ta e="T267" id="Seg_3984" s="T266">gehen-CVB.SIM</ta>
            <ta e="T268" id="Seg_3985" s="T267">hinausgehen-HAB-1PL</ta>
            <ta e="T269" id="Seg_3986" s="T268">sagen-CVB.SIM</ta>
            <ta e="T270" id="Seg_3987" s="T269">solch</ta>
            <ta e="T271" id="Seg_3988" s="T270">kräftig.[NOM]</ta>
            <ta e="T272" id="Seg_3989" s="T271">Mensch-PL.[NOM]</ta>
            <ta e="T273" id="Seg_3990" s="T272">EMPH</ta>
            <ta e="T274" id="Seg_3991" s="T273">warum</ta>
            <ta e="T275" id="Seg_3992" s="T274">Polarfuchs-DAT/LOC</ta>
            <ta e="T276" id="Seg_3993" s="T275">essen-CAUS-HAB-2PL=Q</ta>
            <ta e="T277" id="Seg_3994" s="T276">sagen-PST2.[3SG]</ta>
            <ta e="T278" id="Seg_3995" s="T277">Polarfuchs.[NOM]</ta>
            <ta e="T279" id="Seg_3996" s="T278">Polarfuchs.[NOM]</ta>
            <ta e="T280" id="Seg_3997" s="T279">essen-NEG.CVB</ta>
            <ta e="T281" id="Seg_3998" s="T280">sagen-PRS.[3SG]</ta>
            <ta e="T282" id="Seg_3999" s="T281">1PL-ACC</ta>
            <ta e="T283" id="Seg_4000" s="T282">essen-HAB.[3SG]</ta>
            <ta e="T284" id="Seg_4001" s="T283">und</ta>
            <ta e="T285" id="Seg_4002" s="T284">essen-HAB.[3SG]</ta>
            <ta e="T286" id="Seg_4003" s="T285">Polarfuchs.[NOM]</ta>
            <ta e="T287" id="Seg_4004" s="T286">Polarfuchs.[NOM]</ta>
            <ta e="T288" id="Seg_4005" s="T287">kräftig-2SG</ta>
            <ta e="T289" id="Seg_4006" s="T288">Q</ta>
            <ta e="T290" id="Seg_4007" s="T289">kräftig-1SG</ta>
            <ta e="T291" id="Seg_4008" s="T290">und</ta>
            <ta e="T292" id="Seg_4009" s="T291">kräftig-1SG</ta>
            <ta e="T293" id="Seg_4010" s="T292">sagen-PRS.[3SG]</ta>
            <ta e="T294" id="Seg_4011" s="T293">Lemming-ACC</ta>
            <ta e="T295" id="Seg_4012" s="T294">woher</ta>
            <ta e="T296" id="Seg_4013" s="T295">denken-PTCP.PRS.[NOM]</ta>
            <ta e="T297" id="Seg_4014" s="T296">graben-CVB.SEQ</ta>
            <ta e="T298" id="Seg_4015" s="T297">nehmen-CVB.SEQ</ta>
            <ta e="T299" id="Seg_4016" s="T298">essen-HAB-1PL</ta>
            <ta e="T300" id="Seg_4017" s="T299">solch</ta>
            <ta e="T301" id="Seg_4018" s="T300">kräftig.[NOM]</ta>
            <ta e="T302" id="Seg_4019" s="T301">Mensch.[NOM]</ta>
            <ta e="T303" id="Seg_4020" s="T302">EMPH</ta>
            <ta e="T304" id="Seg_4021" s="T303">Falle-PROPR</ta>
            <ta e="T305" id="Seg_4022" s="T304">dolganisch</ta>
            <ta e="T306" id="Seg_4023" s="T305">Kind-PL-DAT/LOC</ta>
            <ta e="T307" id="Seg_4024" s="T306">warum</ta>
            <ta e="T308" id="Seg_4025" s="T307">fangen-CAUS-HAB-2SG=Q</ta>
            <ta e="T309" id="Seg_4026" s="T308">sagen-PST2.[3SG]</ta>
            <ta e="T310" id="Seg_4027" s="T309">Falle-PROPR</ta>
            <ta e="T311" id="Seg_4028" s="T310">dolganisch</ta>
            <ta e="T312" id="Seg_4029" s="T311">Kind-3PL.[NOM]</ta>
            <ta e="T313" id="Seg_4030" s="T312">Schnee.[NOM]</ta>
            <ta e="T314" id="Seg_4031" s="T313">Unterteil-3SG-DAT/LOC</ta>
            <ta e="T315" id="Seg_4032" s="T314">vergraben-PTCP.PST-3PL-ACC</ta>
            <ta e="T316" id="Seg_4033" s="T315">woher</ta>
            <ta e="T317" id="Seg_4034" s="T316">bemerken-FUT-1SG=Q</ta>
            <ta e="T318" id="Seg_4035" s="T317">Fangeisen-ACC</ta>
            <ta e="T319" id="Seg_4036" s="T318">sagen-PRS.[3SG]</ta>
            <ta e="T320" id="Seg_4037" s="T319">jenes.[NOM]</ta>
            <ta e="T321" id="Seg_4038" s="T320">wegen</ta>
            <ta e="T322" id="Seg_4039" s="T321">greifen-CAUS-HAB-1SG</ta>
            <ta e="T323" id="Seg_4040" s="T322">sagen-PRS.[3SG]</ta>
            <ta e="T324" id="Seg_4041" s="T323">essen-CVB.SIM</ta>
            <ta e="T325" id="Seg_4042" s="T324">kommen-CVB.SEQ-1SG</ta>
            <ta e="T326" id="Seg_4043" s="T325">sagen-PRS.[3SG]</ta>
            <ta e="T327" id="Seg_4044" s="T326">Falle-PROPR</ta>
            <ta e="T328" id="Seg_4045" s="T327">dolganisch</ta>
            <ta e="T329" id="Seg_4046" s="T328">Kind-3PL.[NOM]</ta>
            <ta e="T330" id="Seg_4047" s="T329">kräftig-2PL</ta>
            <ta e="T331" id="Seg_4048" s="T330">Q</ta>
            <ta e="T332" id="Seg_4049" s="T331">kräftig-1PL</ta>
            <ta e="T333" id="Seg_4050" s="T332">und</ta>
            <ta e="T334" id="Seg_4051" s="T333">kräftig-1PL</ta>
            <ta e="T335" id="Seg_4052" s="T334">Polarfuchs-ACC</ta>
            <ta e="T336" id="Seg_4053" s="T335">wann</ta>
            <ta e="T337" id="Seg_4054" s="T336">denken-PTCP.PRS.[NOM]</ta>
            <ta e="T338" id="Seg_4055" s="T337">fangen-HAB-1PL</ta>
            <ta e="T339" id="Seg_4056" s="T338">solch</ta>
            <ta e="T340" id="Seg_4057" s="T339">kräftig.[NOM]</ta>
            <ta e="T341" id="Seg_4058" s="T340">Mensch-PL.[NOM]</ta>
            <ta e="T342" id="Seg_4059" s="T341">nun</ta>
            <ta e="T343" id="Seg_4060" s="T342">warum</ta>
            <ta e="T344" id="Seg_4061" s="T343">Schamane-DAT/LOC</ta>
            <ta e="T345" id="Seg_4062" s="T344">essen-CAUS-HAB-2PL=Q</ta>
            <ta e="T346" id="Seg_4063" s="T345">sagen-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_4064" s="T346">Schamane.[NOM]</ta>
            <ta e="T348" id="Seg_4065" s="T347">erfahren.[NOM]</ta>
            <ta e="T349" id="Seg_4066" s="T348">sein-PST1-3SG</ta>
            <ta e="T350" id="Seg_4067" s="T349">sagen-PST2.[3SG]</ta>
            <ta e="T351" id="Seg_4068" s="T350">essen-FUT-1SG</ta>
            <ta e="T352" id="Seg_4069" s="T351">sagen-FUT-3SG</ta>
            <ta e="T353" id="Seg_4070" s="T352">und</ta>
            <ta e="T354" id="Seg_4071" s="T353">essen-HAB.[3SG]</ta>
            <ta e="T355" id="Seg_4072" s="T354">sagen-CVB.SIM</ta>
            <ta e="T356" id="Seg_4073" s="T355">Schamane.[NOM]</ta>
            <ta e="T357" id="Seg_4074" s="T356">kräftig-2SG</ta>
            <ta e="T358" id="Seg_4075" s="T357">Q</ta>
            <ta e="T359" id="Seg_4076" s="T358">kräftig-1SG</ta>
            <ta e="T360" id="Seg_4077" s="T359">NEG</ta>
            <ta e="T361" id="Seg_4078" s="T360">kräftig-1SG</ta>
            <ta e="T362" id="Seg_4079" s="T361">wann</ta>
            <ta e="T363" id="Seg_4080" s="T362">wünschen-PST1-1SG</ta>
            <ta e="T364" id="Seg_4081" s="T363">und</ta>
            <ta e="T365" id="Seg_4082" s="T364">essen-HAB-1SG</ta>
            <ta e="T366" id="Seg_4083" s="T365">schlecht-ADVZ</ta>
            <ta e="T367" id="Seg_4084" s="T366">sprechen-PTCP.PST</ta>
            <ta e="T368" id="Seg_4085" s="T367">Mensch.[NOM]</ta>
            <ta e="T369" id="Seg_4086" s="T368">sein-FUT.[3SG]</ta>
            <ta e="T370" id="Seg_4087" s="T369">und</ta>
            <ta e="T371" id="Seg_4088" s="T370">sagen-PRS.[3SG]</ta>
            <ta e="T372" id="Seg_4089" s="T371">solch</ta>
            <ta e="T373" id="Seg_4090" s="T372">kräftig</ta>
            <ta e="T374" id="Seg_4091" s="T373">Mensch.[NOM]</ta>
            <ta e="T375" id="Seg_4092" s="T374">EMPH</ta>
            <ta e="T376" id="Seg_4093" s="T375">warum</ta>
            <ta e="T377" id="Seg_4094" s="T376">Tod-DAT/LOC</ta>
            <ta e="T378" id="Seg_4095" s="T377">essen-CAUS-HAB-2SG=Q</ta>
            <ta e="T379" id="Seg_4096" s="T378">sagen-PRS.[3SG]</ta>
            <ta e="T380" id="Seg_4097" s="T379">Tod.[NOM]</ta>
            <ta e="T381" id="Seg_4098" s="T380">dieses</ta>
            <ta e="T382" id="Seg_4099" s="T381">wer-DAT/LOC</ta>
            <ta e="T383" id="Seg_4100" s="T382">NEG</ta>
            <ta e="T384" id="Seg_4101" s="T383">Wort-VBZ-HAB-3SG</ta>
            <ta e="T385" id="Seg_4102" s="T384">NEG.[3SG]</ta>
            <ta e="T386" id="Seg_4103" s="T385">sagen-PST2.[3SG]</ta>
            <ta e="T387" id="Seg_4104" s="T386">Tod.[NOM]</ta>
            <ta e="T388" id="Seg_4105" s="T387">kräftig-2SG</ta>
            <ta e="T389" id="Seg_4106" s="T388">Q</ta>
            <ta e="T390" id="Seg_4107" s="T389">sagen-PRS.[3SG]</ta>
            <ta e="T391" id="Seg_4108" s="T390">kräftig-1SG</ta>
            <ta e="T392" id="Seg_4109" s="T391">und</ta>
            <ta e="T393" id="Seg_4110" s="T392">kräftig-1SG</ta>
            <ta e="T394" id="Seg_4111" s="T393">2SG-ACC</ta>
            <ta e="T395" id="Seg_4112" s="T394">und</ta>
            <ta e="T396" id="Seg_4113" s="T395">essen-FUT-1SG</ta>
            <ta e="T397" id="Seg_4114" s="T396">sagen-PST2-3SG</ta>
            <ta e="T398" id="Seg_4115" s="T397">essen-CVB.SEQ</ta>
            <ta e="T399" id="Seg_4116" s="T398">werfen-PST2-3SG</ta>
            <ta e="T400" id="Seg_4117" s="T399">doch</ta>
            <ta e="T401" id="Seg_4118" s="T400">dieses.[NOM]</ta>
            <ta e="T402" id="Seg_4119" s="T401">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_4120" s="T0">Таал.[NOM]</ta>
            <ta e="T2" id="Seg_4121" s="T1">старуха.[NOM]</ta>
            <ta e="T3" id="Seg_4122" s="T2">жить-PST2.[3SG]</ta>
            <ta e="T4" id="Seg_4123" s="T3">говорят</ta>
            <ta e="T5" id="Seg_4124" s="T4">орнамент-PROPR</ta>
            <ta e="T6" id="Seg_4125" s="T5">краска-PROPR</ta>
            <ta e="T7" id="Seg_4126" s="T6">средний</ta>
            <ta e="T8" id="Seg_4127" s="T7">девять</ta>
            <ta e="T9" id="Seg_4128" s="T8">мир-DAT/LOC</ta>
            <ta e="T10" id="Seg_4129" s="T9">Таал.[NOM]</ta>
            <ta e="T11" id="Seg_4130" s="T10">старуха.[NOM]</ta>
            <ta e="T12" id="Seg_4131" s="T11">этот</ta>
            <ta e="T13" id="Seg_4132" s="T12">Таал.[NOM]</ta>
            <ta e="T14" id="Seg_4133" s="T13">старуха.[NOM]</ta>
            <ta e="T15" id="Seg_4134" s="T14">однако</ta>
            <ta e="T16" id="Seg_4135" s="T15">однажды</ta>
            <ta e="T17" id="Seg_4136" s="T16">вечер-INSTR</ta>
            <ta e="T18" id="Seg_4137" s="T17">эй</ta>
            <ta e="T19" id="Seg_4138" s="T18">чай.[NOM]</ta>
            <ta e="T20" id="Seg_4139" s="T19">пить-EP-MED-CVB.SEQ-пить-EP-MED-CVB.SEQ</ta>
            <ta e="T21" id="Seg_4140" s="T20">после</ta>
            <ta e="T23" id="Seg_4141" s="T21">спать-CVB.SEQ</ta>
            <ta e="T24" id="Seg_4142" s="T23">уснуть-PST2.[3SG]</ta>
            <ta e="T25" id="Seg_4143" s="T24">Q</ta>
            <ta e="T26" id="Seg_4144" s="T25">как</ta>
            <ta e="T27" id="Seg_4145" s="T26">Q</ta>
            <ta e="T28" id="Seg_4146" s="T27">край.чума-3SG.[NOM]</ta>
            <ta e="T29" id="Seg_4147" s="T28">капать-EP-PST2-3SG</ta>
            <ta e="T30" id="Seg_4148" s="T29">Q</ta>
            <ta e="T31" id="Seg_4149" s="T30">дождь.[NOM]</ta>
            <ta e="T32" id="Seg_4150" s="T31">падать-PST2.[3SG]</ta>
            <ta e="T33" id="Seg_4151" s="T32">Q</ta>
            <ta e="T34" id="Seg_4152" s="T33">что.[NOM]</ta>
            <ta e="T35" id="Seg_4153" s="T34">Q</ta>
            <ta e="T36" id="Seg_4154" s="T35">постельное.белье-3SG.[NOM]</ta>
            <ta e="T37" id="Seg_4155" s="T36">промокать-PST2.[3SG]</ta>
            <ta e="T38" id="Seg_4156" s="T37">старуха.[NOM]</ta>
            <ta e="T39" id="Seg_4157" s="T38">собственный-3SG.[NOM]</ta>
            <ta e="T40" id="Seg_4158" s="T39">этот</ta>
            <ta e="T41" id="Seg_4159" s="T40">постельное.белье-3SG.[NOM]</ta>
            <ta e="T42" id="Seg_4160" s="T41">промокать-CVB.SEQ</ta>
            <ta e="T43" id="Seg_4161" s="T42">старуха.[NOM]</ta>
            <ta e="T44" id="Seg_4162" s="T43">утром</ta>
            <ta e="T45" id="Seg_4163" s="T44">осень.[NOM]</ta>
            <ta e="T46" id="Seg_4164" s="T45">быть-PST2.[3SG]</ta>
            <ta e="T47" id="Seg_4165" s="T46">лед.[NOM]</ta>
            <ta e="T48" id="Seg_4166" s="T47">мерзнуть-PTCP.PRS-3SG-ACC</ta>
            <ta e="T49" id="Seg_4167" s="T48">когда</ta>
            <ta e="T50" id="Seg_4168" s="T49">старуха.[NOM]</ta>
            <ta e="T51" id="Seg_4169" s="T50">вставать-CVB.SEQ</ta>
            <ta e="T52" id="Seg_4170" s="T51">тогда</ta>
            <ta e="T53" id="Seg_4171" s="T52">что.[NOM]</ta>
            <ta e="T54" id="Seg_4172" s="T53">огонь-3SG.[NOM]</ta>
            <ta e="T55" id="Seg_4173" s="T54">приходить-FUT.[3SG]=Q</ta>
            <ta e="T56" id="Seg_4174" s="T55">вправду</ta>
            <ta e="T57" id="Seg_4175" s="T56">огонь-PROPR.[NOM]</ta>
            <ta e="T58" id="Seg_4176" s="T57">быть-FUT-3PL=Q</ta>
            <ta e="T59" id="Seg_4177" s="T58">очаг.[NOM]</ta>
            <ta e="T60" id="Seg_4178" s="T59">быть-PST1-3SG</ta>
            <ta e="T61" id="Seg_4179" s="T60">потом</ta>
            <ta e="T62" id="Seg_4180" s="T61">тот.[NOM]</ta>
            <ta e="T63" id="Seg_4181" s="T62">из_за</ta>
            <ta e="T64" id="Seg_4182" s="T63">очевидно</ta>
            <ta e="T65" id="Seg_4183" s="T64">на.улице</ta>
            <ta e="T66" id="Seg_4184" s="T65">солнце-PROPR</ta>
            <ta e="T67" id="Seg_4185" s="T66">очень</ta>
            <ta e="T68" id="Seg_4186" s="T67">день-DAT/LOC</ta>
            <ta e="T69" id="Seg_4187" s="T68">лабаз-3SG-DAT/LOC</ta>
            <ta e="T70" id="Seg_4188" s="T69">постельное.белье-3SG-ACC</ta>
            <ta e="T71" id="Seg_4189" s="T70">повесить-CVB.SEQ</ta>
            <ta e="T72" id="Seg_4190" s="T71">бросать-PST2.[3SG]</ta>
            <ta e="T73" id="Seg_4191" s="T72">эй</ta>
            <ta e="T74" id="Seg_4192" s="T73">постельное.белье-3SG.[NOM]</ta>
            <ta e="T75" id="Seg_4193" s="T74">MOD</ta>
            <ta e="T76" id="Seg_4194" s="T75">эй</ta>
            <ta e="T77" id="Seg_4195" s="T76">чай-3SG-ACC</ta>
            <ta e="T78" id="Seg_4196" s="T77">пить-CVB.SIM</ta>
            <ta e="T79" id="Seg_4197" s="T78">сидеть-TEMP-3SG</ta>
            <ta e="T80" id="Seg_4198" s="T79">фью</ta>
            <ta e="T81" id="Seg_4199" s="T80">делать-PST2-3SG</ta>
            <ta e="T82" id="Seg_4200" s="T81">однако</ta>
            <ta e="T83" id="Seg_4201" s="T82">выйти-CVB.SIM</ta>
            <ta e="T84" id="Seg_4202" s="T83">бежать-PST2-3SG</ta>
            <ta e="T85" id="Seg_4203" s="T84">старуха.[NOM]</ta>
            <ta e="T86" id="Seg_4204" s="T85">выйти-CVB.SIM</ta>
            <ta e="T87" id="Seg_4205" s="T86">бежать-PST2-3SG</ta>
            <ta e="T88" id="Seg_4206" s="T87">однако</ta>
            <ta e="T89" id="Seg_4207" s="T88">постельное.белье-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_4208" s="T89">летать-CVB.SEQ</ta>
            <ta e="T91" id="Seg_4209" s="T90">быть-TEMP-3SG</ta>
            <ta e="T92" id="Seg_4210" s="T91">озеро-EP-2SG.[NOM]</ta>
            <ta e="T93" id="Seg_4211" s="T403">вода.[NOM]</ta>
            <ta e="T94" id="Seg_4212" s="T93">черпать-PTCP.PRS</ta>
            <ta e="T95" id="Seg_4213" s="T94">маленькое.озеро-3SG-GEN</ta>
            <ta e="T96" id="Seg_4214" s="T95">край-3SG-DAT/LOC</ta>
            <ta e="T98" id="Seg_4215" s="T96">падать-PST2.[3SG]</ta>
            <ta e="T99" id="Seg_4216" s="T98">ой</ta>
            <ta e="T100" id="Seg_4217" s="T99">старуха.[NOM]</ta>
            <ta e="T101" id="Seg_4218" s="T100">однако</ta>
            <ta e="T102" id="Seg_4219" s="T101">идти-CVB.SEQ</ta>
            <ta e="T103" id="Seg_4220" s="T102">поймать-CVB.SEQ</ta>
            <ta e="T104" id="Seg_4221" s="T103">взять-CVB.SEQ</ta>
            <ta e="T105" id="Seg_4222" s="T104">постельное.белье-1SG-ACC</ta>
            <ta e="T106" id="Seg_4223" s="T105">поймать-CVB.SEQ</ta>
            <ta e="T107" id="Seg_4224" s="T106">взять-CVB.SEQ-1SG</ta>
            <ta e="T108" id="Seg_4225" s="T107">принести-FUT-1SG</ta>
            <ta e="T109" id="Seg_4226" s="T108">думать-PST2.[3SG]</ta>
            <ta e="T110" id="Seg_4227" s="T109">EVID</ta>
            <ta e="T111" id="Seg_4228" s="T110">ветер-3SG.[NOM]</ta>
            <ta e="T112" id="Seg_4229" s="T111">фью</ta>
            <ta e="T113" id="Seg_4230" s="T112">делать-CAUS-CVB.SEQ</ta>
            <ta e="T114" id="Seg_4231" s="T113">лед-DAT/LOC</ta>
            <ta e="T115" id="Seg_4232" s="T114">входить-PST2.[3SG]</ta>
            <ta e="T116" id="Seg_4233" s="T115">эй</ta>
            <ta e="T117" id="Seg_4234" s="T116">осенью</ta>
            <ta e="T118" id="Seg_4235" s="T117">вот</ta>
            <ta e="T119" id="Seg_4236" s="T118">открытый</ta>
            <ta e="T120" id="Seg_4237" s="T119">стоять-PTCP.PRS</ta>
            <ta e="T121" id="Seg_4238" s="T120">лед-DAT/LOC</ta>
            <ta e="T122" id="Seg_4239" s="T121">этот</ta>
            <ta e="T123" id="Seg_4240" s="T122">старуха.[NOM]</ta>
            <ta e="T124" id="Seg_4241" s="T123">входить-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_4242" s="T124">этот-ACC</ta>
            <ta e="T126" id="Seg_4243" s="T125">гнать-CVB.SEQ</ta>
            <ta e="T127" id="Seg_4244" s="T126">бегать-PRS-1SG</ta>
            <ta e="T128" id="Seg_4245" s="T127">думать-CVB.SEQ</ta>
            <ta e="T129" id="Seg_4246" s="T128">поскользнуться-CVB.SEQ</ta>
            <ta e="T130" id="Seg_4247" s="T129">падать-CVB.SEQ</ta>
            <ta e="T131" id="Seg_4248" s="T130">бедро-3SG-ACC</ta>
            <ta e="T132" id="Seg_4249" s="T131">непременно</ta>
            <ta e="T133" id="Seg_4250" s="T132">падать-PST2.[3SG]</ta>
            <ta e="T134" id="Seg_4251" s="T133">старуха.[NOM]</ta>
            <ta e="T135" id="Seg_4252" s="T134">эй</ta>
            <ta e="T136" id="Seg_4253" s="T135">старуха.[NOM]</ta>
            <ta e="T137" id="Seg_4254" s="T136">EMPH</ta>
            <ta e="T138" id="Seg_4255" s="T137">говорить-CVB.SEQ</ta>
            <ta e="T139" id="Seg_4256" s="T138">быть-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_4257" s="T139">лед.[NOM]</ta>
            <ta e="T141" id="Seg_4258" s="T140">лед.[NOM]</ta>
            <ta e="T142" id="Seg_4259" s="T141">сильный-2SG</ta>
            <ta e="T143" id="Seg_4260" s="T142">Q</ta>
            <ta e="T144" id="Seg_4261" s="T143">говорить-PRS.[3SG]</ta>
            <ta e="T145" id="Seg_4262" s="T144">AFFIRM</ta>
            <ta e="T146" id="Seg_4263" s="T145">вот</ta>
            <ta e="T147" id="Seg_4264" s="T146">сильный-1SG</ta>
            <ta e="T148" id="Seg_4265" s="T147">вот</ta>
            <ta e="T149" id="Seg_4266" s="T148">сильный-1SG</ta>
            <ta e="T150" id="Seg_4267" s="T149">2SG.[NOM]</ta>
            <ta e="T151" id="Seg_4268" s="T150">бедро-2SG-ACC</ta>
            <ta e="T152" id="Seg_4269" s="T151">ломать-PST1-1SG</ta>
            <ta e="T153" id="Seg_4270" s="T152">говорить-CVB.SEQ</ta>
            <ta e="T154" id="Seg_4271" s="T153">такой</ta>
            <ta e="T155" id="Seg_4272" s="T154">сильный</ta>
            <ta e="T156" id="Seg_4273" s="T155">человек.[NOM]</ta>
            <ta e="T157" id="Seg_4274" s="T156">EMPH</ta>
            <ta e="T158" id="Seg_4275" s="T157">почему</ta>
            <ta e="T159" id="Seg_4276" s="T158">весна.[NOM]</ta>
            <ta e="T160" id="Seg_4277" s="T159">быть-FUT.[3SG]</ta>
            <ta e="T161" id="Seg_4278" s="T160">да</ta>
            <ta e="T162" id="Seg_4279" s="T161">солнце.[NOM]</ta>
            <ta e="T163" id="Seg_4280" s="T162">свет-3SG-ABL</ta>
            <ta e="T164" id="Seg_4281" s="T163">таять-CVB.SEQ</ta>
            <ta e="T165" id="Seg_4282" s="T164">быт.продолговатым-CVB.SEQ</ta>
            <ta e="T166" id="Seg_4283" s="T165">падать-HAB-2SG=Q</ta>
            <ta e="T167" id="Seg_4284" s="T166">говорить-PST2.[3SG]</ta>
            <ta e="T168" id="Seg_4285" s="T167">солнце.[NOM]</ta>
            <ta e="T169" id="Seg_4286" s="T168">свет-3SG.[NOM]</ta>
            <ta e="T170" id="Seg_4287" s="T169">сильный.[NOM]</ta>
            <ta e="T171" id="Seg_4288" s="T170">да</ta>
            <ta e="T172" id="Seg_4289" s="T171">сильный.[NOM]</ta>
            <ta e="T173" id="Seg_4290" s="T172">говорить-PRS.[3SG]</ta>
            <ta e="T174" id="Seg_4291" s="T173">1SG-ACC</ta>
            <ta e="T175" id="Seg_4292" s="T174">таять-EP-CAUS-EP-NEG-PTCP.FUT-COMP</ta>
            <ta e="T176" id="Seg_4293" s="T175">быть-PTCP.FUT</ta>
            <ta e="T177" id="Seg_4294" s="T176">разный-разный-PL-ACC</ta>
            <ta e="T178" id="Seg_4295" s="T177">таять-EP-CAUS-HAB.[3SG]</ta>
            <ta e="T179" id="Seg_4296" s="T178">говорить-PRS.[3SG]</ta>
            <ta e="T180" id="Seg_4297" s="T179">солнце.[NOM]</ta>
            <ta e="T181" id="Seg_4298" s="T180">сильный-2SG</ta>
            <ta e="T182" id="Seg_4299" s="T181">Q</ta>
            <ta e="T183" id="Seg_4300" s="T182">вот</ta>
            <ta e="T184" id="Seg_4301" s="T183">сильный-1SG</ta>
            <ta e="T185" id="Seg_4302" s="T184">да</ta>
            <ta e="T186" id="Seg_4303" s="T185">сильный-1SG</ta>
            <ta e="T187" id="Seg_4304" s="T186">говорить-PRS.[3SG]</ta>
            <ta e="T188" id="Seg_4305" s="T187">снег-ACC</ta>
            <ta e="T189" id="Seg_4306" s="T188">да</ta>
            <ta e="T190" id="Seg_4307" s="T189">лед-ACC</ta>
            <ta e="T191" id="Seg_4308" s="T190">да</ta>
            <ta e="T192" id="Seg_4309" s="T191">быть.лишным-EP-CAUS-NEG.CVB.SIM</ta>
            <ta e="T193" id="Seg_4310" s="T192">таять-CAUS-HAB-1SG</ta>
            <ta e="T194" id="Seg_4311" s="T193">говорить-PRS.[3SG]</ta>
            <ta e="T195" id="Seg_4312" s="T194">фуу</ta>
            <ta e="T196" id="Seg_4313" s="T195">такой</ta>
            <ta e="T197" id="Seg_4314" s="T196">сильный</ta>
            <ta e="T198" id="Seg_4315" s="T197">человек.[NOM]</ta>
            <ta e="T199" id="Seg_4316" s="T198">EMPH</ta>
            <ta e="T200" id="Seg_4317" s="T199">почему</ta>
            <ta e="T201" id="Seg_4318" s="T200">камень.[NOM]</ta>
            <ta e="T202" id="Seg_4319" s="T201">гора.[NOM]</ta>
            <ta e="T203" id="Seg_4320" s="T202">защита-3SG-DAT/LOC</ta>
            <ta e="T204" id="Seg_4321" s="T203">затылок-3SG-DAT/LOC</ta>
            <ta e="T205" id="Seg_4322" s="T204">прятаться-CVB.SIM</ta>
            <ta e="T206" id="Seg_4323" s="T205">лежать-HAB-2SG</ta>
            <ta e="T207" id="Seg_4324" s="T206">говорить-PRS.[3SG]</ta>
            <ta e="T208" id="Seg_4325" s="T207">камень.[NOM]</ta>
            <ta e="T209" id="Seg_4326" s="T208">гора.[NOM]</ta>
            <ta e="T210" id="Seg_4327" s="T209">сильный.[NOM]</ta>
            <ta e="T211" id="Seg_4328" s="T210">да</ta>
            <ta e="T212" id="Seg_4329" s="T211">сильный.[NOM]</ta>
            <ta e="T213" id="Seg_4330" s="T212">говорить-PRS.[3SG]</ta>
            <ta e="T214" id="Seg_4331" s="T213">1SG-ACC</ta>
            <ta e="T215" id="Seg_4332" s="T214">передняя.часть-1SG-ABL</ta>
            <ta e="T216" id="Seg_4333" s="T215">защитить-HAB.[3SG]</ta>
            <ta e="T217" id="Seg_4334" s="T216">говорить-PRS.[3SG]</ta>
            <ta e="T218" id="Seg_4335" s="T217">камень.[NOM]</ta>
            <ta e="T219" id="Seg_4336" s="T218">гора.[NOM]=EMPH</ta>
            <ta e="T220" id="Seg_4337" s="T219">сильный-2SG</ta>
            <ta e="T221" id="Seg_4338" s="T220">Q</ta>
            <ta e="T222" id="Seg_4339" s="T221">вот</ta>
            <ta e="T223" id="Seg_4340" s="T222">сильный-1SG</ta>
            <ta e="T224" id="Seg_4341" s="T223">NEG</ta>
            <ta e="T225" id="Seg_4342" s="T224">сильный-1SG</ta>
            <ta e="T226" id="Seg_4343" s="T225">солнце-ACC</ta>
            <ta e="T227" id="Seg_4344" s="T226">EMPH</ta>
            <ta e="T228" id="Seg_4345" s="T227">защитить-HAB-1SG</ta>
            <ta e="T229" id="Seg_4346" s="T228">говорить-PRS.[3SG]</ta>
            <ta e="T230" id="Seg_4347" s="T229">зима-ACC</ta>
            <ta e="T231" id="Seg_4348" s="T230">в.течение</ta>
            <ta e="T232" id="Seg_4349" s="T231">EMPH</ta>
            <ta e="T233" id="Seg_4350" s="T232">темнота.[NOM]</ta>
            <ta e="T234" id="Seg_4351" s="T233">делать-HAB-1SG</ta>
            <ta e="T235" id="Seg_4352" s="T234">говорить-PRS.[3SG]</ta>
            <ta e="T236" id="Seg_4353" s="T235">такой</ta>
            <ta e="T237" id="Seg_4354" s="T236">сильный</ta>
            <ta e="T238" id="Seg_4355" s="T237">человек.[NOM]</ta>
            <ta e="T239" id="Seg_4356" s="T238">EMPH</ta>
            <ta e="T240" id="Seg_4357" s="T239">почему</ta>
            <ta e="T241" id="Seg_4358" s="T240">остромордый</ta>
            <ta e="T242" id="Seg_4359" s="T241">лемминг-PL-DAT/LOC</ta>
            <ta e="T243" id="Seg_4360" s="T242">сквозь</ta>
            <ta e="T244" id="Seg_4361" s="T243">толкать-EP-CAUS-CVB.SIM</ta>
            <ta e="T245" id="Seg_4362" s="T244">идти-PRS-2SG</ta>
            <ta e="T246" id="Seg_4363" s="T245">говорить-PST2.[3SG]</ta>
            <ta e="T247" id="Seg_4364" s="T246">вот</ta>
            <ta e="T248" id="Seg_4365" s="T247">остромордый</ta>
            <ta e="T249" id="Seg_4366" s="T248">лемминг-PL.[NOM]</ta>
            <ta e="T250" id="Seg_4367" s="T249">сильный-PL.[NOM]</ta>
            <ta e="T251" id="Seg_4368" s="T250">да</ta>
            <ta e="T252" id="Seg_4369" s="T251">сильный-PL.[NOM]</ta>
            <ta e="T253" id="Seg_4370" s="T252">очень</ta>
            <ta e="T254" id="Seg_4371" s="T253">острый</ta>
            <ta e="T255" id="Seg_4372" s="T254">морда-PROPR-PL.[NOM]</ta>
            <ta e="T256" id="Seg_4373" s="T255">говорить-PRS.[3SG]</ta>
            <ta e="T257" id="Seg_4374" s="T256">лемминг-PL.[NOM]</ta>
            <ta e="T258" id="Seg_4375" s="T257">сильный-2PL</ta>
            <ta e="T259" id="Seg_4376" s="T258">Q</ta>
            <ta e="T260" id="Seg_4377" s="T259">сильный-1PL</ta>
            <ta e="T261" id="Seg_4378" s="T260">да</ta>
            <ta e="T262" id="Seg_4379" s="T261">сильный-1PL</ta>
            <ta e="T263" id="Seg_4380" s="T262">камень.[NOM]</ta>
            <ta e="T264" id="Seg_4381" s="T263">гора-ACC</ta>
            <ta e="T265" id="Seg_4382" s="T264">каким.местом</ta>
            <ta e="T266" id="Seg_4383" s="T265">думать-PTCP.PRS.[NOM]</ta>
            <ta e="T267" id="Seg_4384" s="T266">идти-CVB.SIM</ta>
            <ta e="T268" id="Seg_4385" s="T267">выйти-HAB-1PL</ta>
            <ta e="T269" id="Seg_4386" s="T268">говорить-CVB.SIM</ta>
            <ta e="T270" id="Seg_4387" s="T269">такой</ta>
            <ta e="T271" id="Seg_4388" s="T270">сильный.[NOM]</ta>
            <ta e="T272" id="Seg_4389" s="T271">человек-PL.[NOM]</ta>
            <ta e="T273" id="Seg_4390" s="T272">EMPH</ta>
            <ta e="T274" id="Seg_4391" s="T273">почему</ta>
            <ta e="T275" id="Seg_4392" s="T274">песец-DAT/LOC</ta>
            <ta e="T276" id="Seg_4393" s="T275">есть-CAUS-HAB-2PL=Q</ta>
            <ta e="T277" id="Seg_4394" s="T276">говорить-PST2.[3SG]</ta>
            <ta e="T278" id="Seg_4395" s="T277">песец.[NOM]</ta>
            <ta e="T279" id="Seg_4396" s="T278">песец.[NOM]</ta>
            <ta e="T280" id="Seg_4397" s="T279">есть-NEG.CVB</ta>
            <ta e="T281" id="Seg_4398" s="T280">говорить-PRS.[3SG]</ta>
            <ta e="T282" id="Seg_4399" s="T281">1PL-ACC</ta>
            <ta e="T283" id="Seg_4400" s="T282">есть-HAB.[3SG]</ta>
            <ta e="T284" id="Seg_4401" s="T283">да</ta>
            <ta e="T285" id="Seg_4402" s="T284">есть-HAB.[3SG]</ta>
            <ta e="T286" id="Seg_4403" s="T285">песец.[NOM]</ta>
            <ta e="T287" id="Seg_4404" s="T286">песец.[NOM]</ta>
            <ta e="T288" id="Seg_4405" s="T287">сильный-2SG</ta>
            <ta e="T289" id="Seg_4406" s="T288">Q</ta>
            <ta e="T290" id="Seg_4407" s="T289">сильный-1SG</ta>
            <ta e="T291" id="Seg_4408" s="T290">да</ta>
            <ta e="T292" id="Seg_4409" s="T291">сильный-1SG</ta>
            <ta e="T293" id="Seg_4410" s="T292">говорить-PRS.[3SG]</ta>
            <ta e="T294" id="Seg_4411" s="T293">лемминг-ACC</ta>
            <ta e="T295" id="Seg_4412" s="T294">откуда</ta>
            <ta e="T296" id="Seg_4413" s="T295">думать-PTCP.PRS.[NOM]</ta>
            <ta e="T297" id="Seg_4414" s="T296">копать-CVB.SEQ</ta>
            <ta e="T298" id="Seg_4415" s="T297">взять-CVB.SEQ</ta>
            <ta e="T299" id="Seg_4416" s="T298">есть-HAB-1PL</ta>
            <ta e="T300" id="Seg_4417" s="T299">такой</ta>
            <ta e="T301" id="Seg_4418" s="T300">сильный.[NOM]</ta>
            <ta e="T302" id="Seg_4419" s="T301">человек.[NOM]</ta>
            <ta e="T303" id="Seg_4420" s="T302">EMPH</ta>
            <ta e="T304" id="Seg_4421" s="T303">капкан-PROPR</ta>
            <ta e="T305" id="Seg_4422" s="T304">долганский</ta>
            <ta e="T306" id="Seg_4423" s="T305">ребенок-PL-DAT/LOC</ta>
            <ta e="T307" id="Seg_4424" s="T306">почему</ta>
            <ta e="T308" id="Seg_4425" s="T307">поймать-CAUS-HAB-2SG=Q</ta>
            <ta e="T309" id="Seg_4426" s="T308">говорить-PST2.[3SG]</ta>
            <ta e="T310" id="Seg_4427" s="T309">капкан-PROPR</ta>
            <ta e="T311" id="Seg_4428" s="T310">долганский</ta>
            <ta e="T312" id="Seg_4429" s="T311">ребенок-3PL.[NOM]</ta>
            <ta e="T313" id="Seg_4430" s="T312">снег.[NOM]</ta>
            <ta e="T314" id="Seg_4431" s="T313">нижняя.часть-3SG-DAT/LOC</ta>
            <ta e="T315" id="Seg_4432" s="T314">закопать-PTCP.PST-3PL-ACC</ta>
            <ta e="T316" id="Seg_4433" s="T315">откуда</ta>
            <ta e="T317" id="Seg_4434" s="T316">замечать-FUT-1SG=Q</ta>
            <ta e="T318" id="Seg_4435" s="T317">капкан-ACC</ta>
            <ta e="T319" id="Seg_4436" s="T318">говорить-PRS.[3SG]</ta>
            <ta e="T320" id="Seg_4437" s="T319">тот.[NOM]</ta>
            <ta e="T321" id="Seg_4438" s="T320">из_за</ta>
            <ta e="T322" id="Seg_4439" s="T321">хватать-CAUS-HAB-1SG</ta>
            <ta e="T323" id="Seg_4440" s="T322">говорить-PRS.[3SG]</ta>
            <ta e="T324" id="Seg_4441" s="T323">есть-CVB.SIM</ta>
            <ta e="T325" id="Seg_4442" s="T324">приходить-CVB.SEQ-1SG</ta>
            <ta e="T326" id="Seg_4443" s="T325">говорить-PRS.[3SG]</ta>
            <ta e="T327" id="Seg_4444" s="T326">капкан-PROPR</ta>
            <ta e="T328" id="Seg_4445" s="T327">долганский</ta>
            <ta e="T329" id="Seg_4446" s="T328">ребенок-3PL.[NOM]</ta>
            <ta e="T330" id="Seg_4447" s="T329">сильный-2PL</ta>
            <ta e="T331" id="Seg_4448" s="T330">Q</ta>
            <ta e="T332" id="Seg_4449" s="T331">сильный-1PL</ta>
            <ta e="T333" id="Seg_4450" s="T332">да</ta>
            <ta e="T334" id="Seg_4451" s="T333">сильный-1PL</ta>
            <ta e="T335" id="Seg_4452" s="T334">песец-ACC</ta>
            <ta e="T336" id="Seg_4453" s="T335">когда</ta>
            <ta e="T337" id="Seg_4454" s="T336">думать-PTCP.PRS.[NOM]</ta>
            <ta e="T338" id="Seg_4455" s="T337">поймать-HAB-1PL</ta>
            <ta e="T339" id="Seg_4456" s="T338">такой</ta>
            <ta e="T340" id="Seg_4457" s="T339">сильный.[NOM]</ta>
            <ta e="T341" id="Seg_4458" s="T340">человек-PL.[NOM]</ta>
            <ta e="T342" id="Seg_4459" s="T341">вот</ta>
            <ta e="T343" id="Seg_4460" s="T342">почему</ta>
            <ta e="T344" id="Seg_4461" s="T343">шаман-DAT/LOC</ta>
            <ta e="T345" id="Seg_4462" s="T344">есть-CAUS-HAB-2PL=Q</ta>
            <ta e="T346" id="Seg_4463" s="T345">говорить-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_4464" s="T346">шаман.[NOM]</ta>
            <ta e="T348" id="Seg_4465" s="T347">бывалый.[NOM]</ta>
            <ta e="T349" id="Seg_4466" s="T348">быть-PST1-3SG</ta>
            <ta e="T350" id="Seg_4467" s="T349">говорить-PST2.[3SG]</ta>
            <ta e="T351" id="Seg_4468" s="T350">есть-FUT-1SG</ta>
            <ta e="T352" id="Seg_4469" s="T351">говорить-FUT-3SG</ta>
            <ta e="T353" id="Seg_4470" s="T352">да</ta>
            <ta e="T354" id="Seg_4471" s="T353">есть-HAB.[3SG]</ta>
            <ta e="T355" id="Seg_4472" s="T354">говорить-CVB.SIM</ta>
            <ta e="T356" id="Seg_4473" s="T355">шаман.[NOM]</ta>
            <ta e="T357" id="Seg_4474" s="T356">сильный-2SG</ta>
            <ta e="T358" id="Seg_4475" s="T357">Q</ta>
            <ta e="T359" id="Seg_4476" s="T358">сильный-1SG</ta>
            <ta e="T360" id="Seg_4477" s="T359">NEG</ta>
            <ta e="T361" id="Seg_4478" s="T360">сильный-1SG</ta>
            <ta e="T362" id="Seg_4479" s="T361">когда</ta>
            <ta e="T363" id="Seg_4480" s="T362">желать-PST1-1SG</ta>
            <ta e="T364" id="Seg_4481" s="T363">да</ta>
            <ta e="T365" id="Seg_4482" s="T364">есть-HAB-1SG</ta>
            <ta e="T366" id="Seg_4483" s="T365">плохой-ADVZ</ta>
            <ta e="T367" id="Seg_4484" s="T366">говорить-PTCP.PST</ta>
            <ta e="T368" id="Seg_4485" s="T367">человек.[NOM]</ta>
            <ta e="T369" id="Seg_4486" s="T368">быть-FUT.[3SG]</ta>
            <ta e="T370" id="Seg_4487" s="T369">да</ta>
            <ta e="T371" id="Seg_4488" s="T370">говорить-PRS.[3SG]</ta>
            <ta e="T372" id="Seg_4489" s="T371">такой</ta>
            <ta e="T373" id="Seg_4490" s="T372">сильный</ta>
            <ta e="T374" id="Seg_4491" s="T373">человек.[NOM]</ta>
            <ta e="T375" id="Seg_4492" s="T374">EMPH</ta>
            <ta e="T376" id="Seg_4493" s="T375">почему</ta>
            <ta e="T377" id="Seg_4494" s="T376">смерть-DAT/LOC</ta>
            <ta e="T378" id="Seg_4495" s="T377">есть-CAUS-HAB-2SG=Q</ta>
            <ta e="T379" id="Seg_4496" s="T378">говорить-PRS.[3SG]</ta>
            <ta e="T380" id="Seg_4497" s="T379">смерть.[NOM]</ta>
            <ta e="T381" id="Seg_4498" s="T380">этот</ta>
            <ta e="T382" id="Seg_4499" s="T381">кто-DAT/LOC</ta>
            <ta e="T383" id="Seg_4500" s="T382">NEG</ta>
            <ta e="T384" id="Seg_4501" s="T383">слово-VBZ-HAB-3SG</ta>
            <ta e="T385" id="Seg_4502" s="T384">NEG.[3SG]</ta>
            <ta e="T386" id="Seg_4503" s="T385">говорить-PST2.[3SG]</ta>
            <ta e="T387" id="Seg_4504" s="T386">смерть.[NOM]</ta>
            <ta e="T388" id="Seg_4505" s="T387">сильный-2SG</ta>
            <ta e="T389" id="Seg_4506" s="T388">Q</ta>
            <ta e="T390" id="Seg_4507" s="T389">говорить-PRS.[3SG]</ta>
            <ta e="T391" id="Seg_4508" s="T390">сильный-1SG</ta>
            <ta e="T392" id="Seg_4509" s="T391">да</ta>
            <ta e="T393" id="Seg_4510" s="T392">сильный-1SG</ta>
            <ta e="T394" id="Seg_4511" s="T393">2SG-ACC</ta>
            <ta e="T395" id="Seg_4512" s="T394">да</ta>
            <ta e="T396" id="Seg_4513" s="T395">есть-FUT-1SG</ta>
            <ta e="T397" id="Seg_4514" s="T396">говорить-PST2-3SG</ta>
            <ta e="T398" id="Seg_4515" s="T397">есть-CVB.SEQ</ta>
            <ta e="T399" id="Seg_4516" s="T398">бросать-PST2-3SG</ta>
            <ta e="T400" id="Seg_4517" s="T399">вот</ta>
            <ta e="T401" id="Seg_4518" s="T400">тот.[NOM]</ta>
            <ta e="T402" id="Seg_4519" s="T401">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_4520" s="T0">propr-n:case</ta>
            <ta e="T2" id="Seg_4521" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_4522" s="T2">v-v:tense-v:pred.pn</ta>
            <ta e="T4" id="Seg_4523" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_4524" s="T4">n-n&gt;adj</ta>
            <ta e="T6" id="Seg_4525" s="T5">n-n&gt;adj</ta>
            <ta e="T7" id="Seg_4526" s="T6">adj</ta>
            <ta e="T8" id="Seg_4527" s="T7">cardnum</ta>
            <ta e="T9" id="Seg_4528" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_4529" s="T9">propr-n:case</ta>
            <ta e="T11" id="Seg_4530" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_4531" s="T11">dempro</ta>
            <ta e="T13" id="Seg_4532" s="T12">propr-n:case</ta>
            <ta e="T14" id="Seg_4533" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_4534" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_4535" s="T15">adv</ta>
            <ta e="T17" id="Seg_4536" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_4537" s="T17">interj</ta>
            <ta e="T19" id="Seg_4538" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_4539" s="T19">v-v:(ins)-v&gt;v-v:cvb-v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T21" id="Seg_4540" s="T20">post</ta>
            <ta e="T23" id="Seg_4541" s="T21">v-v:cvb</ta>
            <ta e="T24" id="Seg_4542" s="T23">v-v:tense-v:pred.pn</ta>
            <ta e="T25" id="Seg_4543" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_4544" s="T25">que</ta>
            <ta e="T27" id="Seg_4545" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_4546" s="T27">n-n:(poss)-n:case</ta>
            <ta e="T29" id="Seg_4547" s="T28">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T30" id="Seg_4548" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_4549" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_4550" s="T31">v-v:tense-v:pred.pn</ta>
            <ta e="T33" id="Seg_4551" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_4552" s="T33">que-pro:case</ta>
            <ta e="T35" id="Seg_4553" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_4554" s="T35">n-n:(poss)-n:case</ta>
            <ta e="T37" id="Seg_4555" s="T36">v-v:tense-v:pred.pn</ta>
            <ta e="T38" id="Seg_4556" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_4557" s="T38">adj-n:(poss)-n:case</ta>
            <ta e="T40" id="Seg_4558" s="T39">dempro</ta>
            <ta e="T41" id="Seg_4559" s="T40">n-n:(poss)-n:case</ta>
            <ta e="T42" id="Seg_4560" s="T41">v-v:cvb</ta>
            <ta e="T43" id="Seg_4561" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_4562" s="T43">adv</ta>
            <ta e="T45" id="Seg_4563" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_4564" s="T45">v-v:tense-v:pred.pn</ta>
            <ta e="T47" id="Seg_4565" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_4566" s="T47">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T49" id="Seg_4567" s="T48">post</ta>
            <ta e="T50" id="Seg_4568" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_4569" s="T50">v-v:cvb</ta>
            <ta e="T52" id="Seg_4570" s="T51">adv</ta>
            <ta e="T53" id="Seg_4571" s="T52">que-pro:case</ta>
            <ta e="T54" id="Seg_4572" s="T53">n-n:(poss)-n:case</ta>
            <ta e="T55" id="Seg_4573" s="T54">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T56" id="Seg_4574" s="T55">adv</ta>
            <ta e="T57" id="Seg_4575" s="T56">n-n&gt;adj-n:case</ta>
            <ta e="T58" id="Seg_4576" s="T57">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T59" id="Seg_4577" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_4578" s="T59">v-v:tense-v:poss.pn</ta>
            <ta e="T61" id="Seg_4579" s="T60">adv</ta>
            <ta e="T62" id="Seg_4580" s="T61">dempro-pro:case</ta>
            <ta e="T63" id="Seg_4581" s="T62">post</ta>
            <ta e="T64" id="Seg_4582" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_4583" s="T64">adv</ta>
            <ta e="T66" id="Seg_4584" s="T65">n-n&gt;adj</ta>
            <ta e="T67" id="Seg_4585" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_4586" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_4587" s="T68">n-n:poss-n:case</ta>
            <ta e="T70" id="Seg_4588" s="T69">n-n:poss-n:case</ta>
            <ta e="T71" id="Seg_4589" s="T70">v-v:cvb</ta>
            <ta e="T72" id="Seg_4590" s="T71">v-v:tense-v:pred.pn</ta>
            <ta e="T73" id="Seg_4591" s="T72">interj</ta>
            <ta e="T74" id="Seg_4592" s="T73">n-n:(poss)-n:case</ta>
            <ta e="T75" id="Seg_4593" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_4594" s="T75">interj</ta>
            <ta e="T77" id="Seg_4595" s="T76">n-n:poss-n:case</ta>
            <ta e="T78" id="Seg_4596" s="T77">v-v:cvb</ta>
            <ta e="T79" id="Seg_4597" s="T78">v-v:mood-v:temp.pn</ta>
            <ta e="T80" id="Seg_4598" s="T79">interj</ta>
            <ta e="T81" id="Seg_4599" s="T80">v-v:tense-v:poss.pn</ta>
            <ta e="T82" id="Seg_4600" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_4601" s="T82">v-v:cvb</ta>
            <ta e="T84" id="Seg_4602" s="T83">v-v:tense-v:poss.pn</ta>
            <ta e="T85" id="Seg_4603" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_4604" s="T85">v-v:cvb</ta>
            <ta e="T87" id="Seg_4605" s="T86">v-v:tense-v:poss.pn</ta>
            <ta e="T88" id="Seg_4606" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_4607" s="T88">n-n:(poss)-n:case</ta>
            <ta e="T90" id="Seg_4608" s="T89">v-v:cvb</ta>
            <ta e="T91" id="Seg_4609" s="T90">v-v:mood-v:temp.pn</ta>
            <ta e="T92" id="Seg_4610" s="T91">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T93" id="Seg_4611" s="T403">n-n:case</ta>
            <ta e="T94" id="Seg_4612" s="T93">v-v:ptcp</ta>
            <ta e="T95" id="Seg_4613" s="T94">n-n:poss-n:case</ta>
            <ta e="T96" id="Seg_4614" s="T95">n-n:poss-n:case</ta>
            <ta e="T98" id="Seg_4615" s="T96">v-v:tense-v:pred.pn</ta>
            <ta e="T99" id="Seg_4616" s="T98">interj</ta>
            <ta e="T100" id="Seg_4617" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_4618" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_4619" s="T101">v-v:cvb</ta>
            <ta e="T103" id="Seg_4620" s="T102">v-v:cvb</ta>
            <ta e="T104" id="Seg_4621" s="T103">v-v:cvb</ta>
            <ta e="T105" id="Seg_4622" s="T104">n-n:poss-n:case</ta>
            <ta e="T106" id="Seg_4623" s="T105">v-v:cvb</ta>
            <ta e="T107" id="Seg_4624" s="T106">v-v:cvb-v:pred.pn</ta>
            <ta e="T108" id="Seg_4625" s="T107">v-v:tense-v:poss.pn</ta>
            <ta e="T109" id="Seg_4626" s="T108">v-v:tense-v:pred.pn</ta>
            <ta e="T110" id="Seg_4627" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_4628" s="T110">n-n:(poss)-n:case</ta>
            <ta e="T112" id="Seg_4629" s="T111">interj</ta>
            <ta e="T113" id="Seg_4630" s="T112">v-v&gt;v-v:cvb</ta>
            <ta e="T114" id="Seg_4631" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_4632" s="T114">v-v:tense-v:pred.pn</ta>
            <ta e="T116" id="Seg_4633" s="T115">interj</ta>
            <ta e="T117" id="Seg_4634" s="T116">adv</ta>
            <ta e="T118" id="Seg_4635" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_4636" s="T118">adj</ta>
            <ta e="T120" id="Seg_4637" s="T119">v-v:ptcp</ta>
            <ta e="T121" id="Seg_4638" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_4639" s="T121">dempro</ta>
            <ta e="T123" id="Seg_4640" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_4641" s="T123">v-v:tense-v:pred.pn</ta>
            <ta e="T125" id="Seg_4642" s="T124">dempro-pro:case</ta>
            <ta e="T126" id="Seg_4643" s="T125">v-v:cvb</ta>
            <ta e="T127" id="Seg_4644" s="T126">v-v:tense-v:pred.pn</ta>
            <ta e="T128" id="Seg_4645" s="T127">v-v:cvb</ta>
            <ta e="T129" id="Seg_4646" s="T128">v-v:cvb</ta>
            <ta e="T130" id="Seg_4647" s="T129">v-v:cvb</ta>
            <ta e="T131" id="Seg_4648" s="T130">n-n:poss-n:case</ta>
            <ta e="T132" id="Seg_4649" s="T131">adv</ta>
            <ta e="T133" id="Seg_4650" s="T132">v-v:tense-v:pred.pn</ta>
            <ta e="T134" id="Seg_4651" s="T133">n-n:case</ta>
            <ta e="T135" id="Seg_4652" s="T134">interj</ta>
            <ta e="T136" id="Seg_4653" s="T135">n-n:case</ta>
            <ta e="T137" id="Seg_4654" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_4655" s="T137">v-v:cvb</ta>
            <ta e="T139" id="Seg_4656" s="T138">v-v:tense-v:pred.pn</ta>
            <ta e="T140" id="Seg_4657" s="T139">n-n:case</ta>
            <ta e="T141" id="Seg_4658" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_4659" s="T141">adj-n:(pred.pn)</ta>
            <ta e="T143" id="Seg_4660" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_4661" s="T143">v-v:tense-v:pred.pn</ta>
            <ta e="T145" id="Seg_4662" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_4663" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_4664" s="T146">adj-n:(pred.pn)</ta>
            <ta e="T148" id="Seg_4665" s="T147">ptcl</ta>
            <ta e="T149" id="Seg_4666" s="T148">adj-n:(pred.pn)</ta>
            <ta e="T150" id="Seg_4667" s="T149">pers-pro:case</ta>
            <ta e="T151" id="Seg_4668" s="T150">n-n:poss-n:case</ta>
            <ta e="T152" id="Seg_4669" s="T151">v-v:tense-v:poss.pn</ta>
            <ta e="T153" id="Seg_4670" s="T152">v-v:cvb</ta>
            <ta e="T154" id="Seg_4671" s="T153">dempro</ta>
            <ta e="T155" id="Seg_4672" s="T154">adj</ta>
            <ta e="T156" id="Seg_4673" s="T155">n-n:case</ta>
            <ta e="T157" id="Seg_4674" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_4675" s="T157">que</ta>
            <ta e="T159" id="Seg_4676" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_4677" s="T159">v-v:tense-v:poss.pn</ta>
            <ta e="T161" id="Seg_4678" s="T160">conj</ta>
            <ta e="T162" id="Seg_4679" s="T161">n-n:case</ta>
            <ta e="T163" id="Seg_4680" s="T162">n-n:poss-n:case</ta>
            <ta e="T164" id="Seg_4681" s="T163">v-v:cvb</ta>
            <ta e="T165" id="Seg_4682" s="T164">v-v:cvb</ta>
            <ta e="T166" id="Seg_4683" s="T165">v-v:mood-v:pred.pn-ptcl</ta>
            <ta e="T167" id="Seg_4684" s="T166">v-v:tense-v:pred.pn</ta>
            <ta e="T168" id="Seg_4685" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_4686" s="T168">n-n:(poss)-n:case</ta>
            <ta e="T170" id="Seg_4687" s="T169">adj-n:case</ta>
            <ta e="T171" id="Seg_4688" s="T170">conj</ta>
            <ta e="T172" id="Seg_4689" s="T171">adj-n:case</ta>
            <ta e="T173" id="Seg_4690" s="T172">v-v:tense-v:pred.pn</ta>
            <ta e="T174" id="Seg_4691" s="T173">pers-pro:case</ta>
            <ta e="T175" id="Seg_4692" s="T174">v-v:(ins)-v&gt;v-v:(ins)-v:(neg)-v:ptcp-v:(case)</ta>
            <ta e="T176" id="Seg_4693" s="T175">v-v:ptcp</ta>
            <ta e="T177" id="Seg_4694" s="T176">adj-adj-n:(num)-n:case</ta>
            <ta e="T178" id="Seg_4695" s="T177">v-v:(ins)-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T179" id="Seg_4696" s="T178">v-v:tense-v:pred.pn</ta>
            <ta e="T180" id="Seg_4697" s="T179">n-n:case</ta>
            <ta e="T181" id="Seg_4698" s="T180">adj-n:(pred.pn)</ta>
            <ta e="T182" id="Seg_4699" s="T181">ptcl</ta>
            <ta e="T183" id="Seg_4700" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_4701" s="T183">adj-n:(pred.pn)</ta>
            <ta e="T185" id="Seg_4702" s="T184">conj</ta>
            <ta e="T186" id="Seg_4703" s="T185">adj-n:(pred.pn)</ta>
            <ta e="T187" id="Seg_4704" s="T186">v-v:tense-v:pred.pn</ta>
            <ta e="T188" id="Seg_4705" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_4706" s="T188">conj</ta>
            <ta e="T190" id="Seg_4707" s="T189">n-n:case</ta>
            <ta e="T191" id="Seg_4708" s="T190">conj</ta>
            <ta e="T192" id="Seg_4709" s="T191">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T193" id="Seg_4710" s="T192">v-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T194" id="Seg_4711" s="T193">v-v:tense-v:pred.pn</ta>
            <ta e="T195" id="Seg_4712" s="T194">interj</ta>
            <ta e="T196" id="Seg_4713" s="T195">dempro</ta>
            <ta e="T197" id="Seg_4714" s="T196">adj</ta>
            <ta e="T198" id="Seg_4715" s="T197">n-n:case</ta>
            <ta e="T199" id="Seg_4716" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_4717" s="T199">que</ta>
            <ta e="T201" id="Seg_4718" s="T200">n-n:case</ta>
            <ta e="T202" id="Seg_4719" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_4720" s="T202">n-n:poss-n:case</ta>
            <ta e="T204" id="Seg_4721" s="T203">n-n:poss-n:case</ta>
            <ta e="T205" id="Seg_4722" s="T204">v-v:cvb</ta>
            <ta e="T206" id="Seg_4723" s="T205">v-v:mood-v:pred.pn</ta>
            <ta e="T207" id="Seg_4724" s="T206">v-v:tense-v:pred.pn</ta>
            <ta e="T208" id="Seg_4725" s="T207">n-n:case</ta>
            <ta e="T209" id="Seg_4726" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_4727" s="T209">adj-n:case</ta>
            <ta e="T211" id="Seg_4728" s="T210">conj</ta>
            <ta e="T212" id="Seg_4729" s="T211">adj-n:case</ta>
            <ta e="T213" id="Seg_4730" s="T212">v-v:tense-v:pred.pn</ta>
            <ta e="T214" id="Seg_4731" s="T213">pers-pro:case</ta>
            <ta e="T215" id="Seg_4732" s="T214">n-n:poss-n:case</ta>
            <ta e="T216" id="Seg_4733" s="T215">v-v:mood-v:pred.pn</ta>
            <ta e="T217" id="Seg_4734" s="T216">v-v:tense-v:pred.pn</ta>
            <ta e="T218" id="Seg_4735" s="T217">n-n:case</ta>
            <ta e="T219" id="Seg_4736" s="T218">n-n:case-ptcl</ta>
            <ta e="T220" id="Seg_4737" s="T219">adj-n:(pred.pn)</ta>
            <ta e="T221" id="Seg_4738" s="T220">ptcl</ta>
            <ta e="T222" id="Seg_4739" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_4740" s="T222">adj-n:(pred.pn)</ta>
            <ta e="T224" id="Seg_4741" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_4742" s="T224">adj-n:(pred.pn)</ta>
            <ta e="T226" id="Seg_4743" s="T225">n-n:case</ta>
            <ta e="T227" id="Seg_4744" s="T226">ptcl</ta>
            <ta e="T228" id="Seg_4745" s="T227">v-v:mood-v:pred.pn</ta>
            <ta e="T229" id="Seg_4746" s="T228">v-v:tense-v:pred.pn</ta>
            <ta e="T230" id="Seg_4747" s="T229">n-n:case</ta>
            <ta e="T231" id="Seg_4748" s="T230">post</ta>
            <ta e="T232" id="Seg_4749" s="T231">ptcl</ta>
            <ta e="T233" id="Seg_4750" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_4751" s="T233">v-v:mood-v:pred.pn</ta>
            <ta e="T235" id="Seg_4752" s="T234">v-v:tense-v:pred.pn</ta>
            <ta e="T236" id="Seg_4753" s="T235">dempro</ta>
            <ta e="T237" id="Seg_4754" s="T236">adj</ta>
            <ta e="T238" id="Seg_4755" s="T237">n-n:case</ta>
            <ta e="T239" id="Seg_4756" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_4757" s="T239">que</ta>
            <ta e="T241" id="Seg_4758" s="T240">adj</ta>
            <ta e="T242" id="Seg_4759" s="T241">n-n:(num)-n:case</ta>
            <ta e="T243" id="Seg_4760" s="T242">post</ta>
            <ta e="T244" id="Seg_4761" s="T243">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T245" id="Seg_4762" s="T244">v-v:tense-v:pred.pn</ta>
            <ta e="T246" id="Seg_4763" s="T245">v-v:tense-v:pred.pn</ta>
            <ta e="T247" id="Seg_4764" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_4765" s="T247">adj</ta>
            <ta e="T249" id="Seg_4766" s="T248">n-n:(num)-n:case</ta>
            <ta e="T250" id="Seg_4767" s="T249">adj-n:(num)-n:case</ta>
            <ta e="T251" id="Seg_4768" s="T250">conj</ta>
            <ta e="T252" id="Seg_4769" s="T251">adj-n:(num)-n:case</ta>
            <ta e="T253" id="Seg_4770" s="T252">adv</ta>
            <ta e="T254" id="Seg_4771" s="T253">adj</ta>
            <ta e="T255" id="Seg_4772" s="T254">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T256" id="Seg_4773" s="T255">v-v:tense-v:pred.pn</ta>
            <ta e="T257" id="Seg_4774" s="T256">n-n:(num)-n:case</ta>
            <ta e="T258" id="Seg_4775" s="T257">adj-n:(pred.pn)</ta>
            <ta e="T259" id="Seg_4776" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_4777" s="T259">adj-n:(pred.pn)</ta>
            <ta e="T261" id="Seg_4778" s="T260">conj</ta>
            <ta e="T262" id="Seg_4779" s="T261">adj-n:(pred.pn)</ta>
            <ta e="T263" id="Seg_4780" s="T262">n-n:case</ta>
            <ta e="T264" id="Seg_4781" s="T263">n-n:case</ta>
            <ta e="T265" id="Seg_4782" s="T264">que</ta>
            <ta e="T266" id="Seg_4783" s="T265">v-v:ptcp-v:(case)</ta>
            <ta e="T267" id="Seg_4784" s="T266">v-v:cvb</ta>
            <ta e="T268" id="Seg_4785" s="T267">v-v:mood-v:pred.pn</ta>
            <ta e="T269" id="Seg_4786" s="T268">v-v:cvb</ta>
            <ta e="T270" id="Seg_4787" s="T269">dempro</ta>
            <ta e="T271" id="Seg_4788" s="T270">adj-n:case</ta>
            <ta e="T272" id="Seg_4789" s="T271">n-n:(num)-n:case</ta>
            <ta e="T273" id="Seg_4790" s="T272">ptcl</ta>
            <ta e="T274" id="Seg_4791" s="T273">que</ta>
            <ta e="T275" id="Seg_4792" s="T274">n-n:case</ta>
            <ta e="T276" id="Seg_4793" s="T275">v-v&gt;v-v:mood-v:pred.pn-ptcl</ta>
            <ta e="T277" id="Seg_4794" s="T276">v-v:tense-v:pred.pn</ta>
            <ta e="T278" id="Seg_4795" s="T277">n-n:case</ta>
            <ta e="T279" id="Seg_4796" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_4797" s="T279">v-v:cvb</ta>
            <ta e="T281" id="Seg_4798" s="T280">v-v:tense-v:pred.pn</ta>
            <ta e="T282" id="Seg_4799" s="T281">pers-pro:case</ta>
            <ta e="T283" id="Seg_4800" s="T282">v-v:mood-v:pred.pn</ta>
            <ta e="T284" id="Seg_4801" s="T283">conj</ta>
            <ta e="T285" id="Seg_4802" s="T284">v-v:mood-v:pred.pn</ta>
            <ta e="T286" id="Seg_4803" s="T285">n-n:case</ta>
            <ta e="T287" id="Seg_4804" s="T286">n-n:case</ta>
            <ta e="T288" id="Seg_4805" s="T287">adj-n:(pred.pn)</ta>
            <ta e="T289" id="Seg_4806" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_4807" s="T289">adj-n:(pred.pn)</ta>
            <ta e="T291" id="Seg_4808" s="T290">conj</ta>
            <ta e="T292" id="Seg_4809" s="T291">adj-n:(pred.pn)</ta>
            <ta e="T293" id="Seg_4810" s="T292">v-v:tense-v:pred.pn</ta>
            <ta e="T294" id="Seg_4811" s="T293">n-n:case</ta>
            <ta e="T295" id="Seg_4812" s="T294">que</ta>
            <ta e="T296" id="Seg_4813" s="T295">v-v:ptcp-v:(case)</ta>
            <ta e="T297" id="Seg_4814" s="T296">v-v:cvb</ta>
            <ta e="T298" id="Seg_4815" s="T297">v-v:cvb</ta>
            <ta e="T299" id="Seg_4816" s="T298">v-v:mood-v:pred.pn</ta>
            <ta e="T300" id="Seg_4817" s="T299">dempro</ta>
            <ta e="T301" id="Seg_4818" s="T300">adj-n:case</ta>
            <ta e="T302" id="Seg_4819" s="T301">n-n:case</ta>
            <ta e="T303" id="Seg_4820" s="T302">ptcl</ta>
            <ta e="T304" id="Seg_4821" s="T303">n-n&gt;adj</ta>
            <ta e="T305" id="Seg_4822" s="T304">adj</ta>
            <ta e="T306" id="Seg_4823" s="T305">n-n:(num)-n:case</ta>
            <ta e="T307" id="Seg_4824" s="T306">que</ta>
            <ta e="T308" id="Seg_4825" s="T307">v-v&gt;v-v:mood-v:pred.pn-ptcl</ta>
            <ta e="T309" id="Seg_4826" s="T308">v-v:tense-v:pred.pn</ta>
            <ta e="T310" id="Seg_4827" s="T309">n-n&gt;adj</ta>
            <ta e="T311" id="Seg_4828" s="T310">adj</ta>
            <ta e="T312" id="Seg_4829" s="T311">n-n:(poss)-n:case</ta>
            <ta e="T313" id="Seg_4830" s="T312">n-n:case</ta>
            <ta e="T314" id="Seg_4831" s="T313">n-n:poss-n:case</ta>
            <ta e="T315" id="Seg_4832" s="T314">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T316" id="Seg_4833" s="T315">que</ta>
            <ta e="T317" id="Seg_4834" s="T316">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T318" id="Seg_4835" s="T317">n-n:case</ta>
            <ta e="T319" id="Seg_4836" s="T318">v-v:tense-v:pred.pn</ta>
            <ta e="T320" id="Seg_4837" s="T319">dempro-pro:case</ta>
            <ta e="T321" id="Seg_4838" s="T320">post</ta>
            <ta e="T322" id="Seg_4839" s="T321">v-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T323" id="Seg_4840" s="T322">v-v:tense-v:pred.pn</ta>
            <ta e="T324" id="Seg_4841" s="T323">v-v:cvb</ta>
            <ta e="T325" id="Seg_4842" s="T324">v-v:cvb-v:pred.pn</ta>
            <ta e="T326" id="Seg_4843" s="T325">v-v:tense-v:pred.pn</ta>
            <ta e="T327" id="Seg_4844" s="T326">n-n&gt;adj</ta>
            <ta e="T328" id="Seg_4845" s="T327">adj</ta>
            <ta e="T329" id="Seg_4846" s="T328">n-n:(poss)-n:case</ta>
            <ta e="T330" id="Seg_4847" s="T329">adj-n:(pred.pn)</ta>
            <ta e="T331" id="Seg_4848" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_4849" s="T331">adj-n:(pred.pn)</ta>
            <ta e="T333" id="Seg_4850" s="T332">conj</ta>
            <ta e="T334" id="Seg_4851" s="T333">adj-n:(pred.pn)</ta>
            <ta e="T335" id="Seg_4852" s="T334">n-n:case</ta>
            <ta e="T336" id="Seg_4853" s="T335">que</ta>
            <ta e="T337" id="Seg_4854" s="T336">v-v:ptcp-v:(case)</ta>
            <ta e="T338" id="Seg_4855" s="T337">v-v:mood-v:pred.pn</ta>
            <ta e="T339" id="Seg_4856" s="T338">dempro</ta>
            <ta e="T340" id="Seg_4857" s="T339">adj-n:case</ta>
            <ta e="T341" id="Seg_4858" s="T340">n-n:(num)-n:case</ta>
            <ta e="T342" id="Seg_4859" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_4860" s="T342">que</ta>
            <ta e="T344" id="Seg_4861" s="T343">n-n:case</ta>
            <ta e="T345" id="Seg_4862" s="T344">v-v&gt;v-v:mood-v:pred.pn-ptcl</ta>
            <ta e="T346" id="Seg_4863" s="T345">v-v:tense-v:pred.pn</ta>
            <ta e="T347" id="Seg_4864" s="T346">n-n:case</ta>
            <ta e="T348" id="Seg_4865" s="T347">adj-n:case</ta>
            <ta e="T349" id="Seg_4866" s="T348">v-v:tense-v:poss.pn</ta>
            <ta e="T350" id="Seg_4867" s="T349">v-v:tense-v:pred.pn</ta>
            <ta e="T351" id="Seg_4868" s="T350">v-v:tense-v:poss.pn</ta>
            <ta e="T352" id="Seg_4869" s="T351">v-v:tense-v:poss.pn</ta>
            <ta e="T353" id="Seg_4870" s="T352">conj</ta>
            <ta e="T354" id="Seg_4871" s="T353">v-v:mood-v:pred.pn</ta>
            <ta e="T355" id="Seg_4872" s="T354">v-v:cvb</ta>
            <ta e="T356" id="Seg_4873" s="T355">n-n:case</ta>
            <ta e="T357" id="Seg_4874" s="T356">adj-n:(pred.pn)</ta>
            <ta e="T358" id="Seg_4875" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_4876" s="T358">adj-n:(pred.pn)</ta>
            <ta e="T360" id="Seg_4877" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_4878" s="T360">adj-n:(pred.pn)</ta>
            <ta e="T362" id="Seg_4879" s="T361">que</ta>
            <ta e="T363" id="Seg_4880" s="T362">v-v:tense-v:poss.pn</ta>
            <ta e="T364" id="Seg_4881" s="T363">conj</ta>
            <ta e="T365" id="Seg_4882" s="T364">v-v:mood-v:pred.pn</ta>
            <ta e="T366" id="Seg_4883" s="T365">adj-adj&gt;adv</ta>
            <ta e="T367" id="Seg_4884" s="T366">v-v:ptcp</ta>
            <ta e="T368" id="Seg_4885" s="T367">n-n:case</ta>
            <ta e="T369" id="Seg_4886" s="T368">v-v:tense-v:poss.pn</ta>
            <ta e="T370" id="Seg_4887" s="T369">conj</ta>
            <ta e="T371" id="Seg_4888" s="T370">v-v:tense-v:pred.pn</ta>
            <ta e="T372" id="Seg_4889" s="T371">dempro</ta>
            <ta e="T373" id="Seg_4890" s="T372">adj</ta>
            <ta e="T374" id="Seg_4891" s="T373">n-n:case</ta>
            <ta e="T375" id="Seg_4892" s="T374">ptcl</ta>
            <ta e="T376" id="Seg_4893" s="T375">que</ta>
            <ta e="T377" id="Seg_4894" s="T376">n-n:case</ta>
            <ta e="T378" id="Seg_4895" s="T377">v-v&gt;v-v:mood-v:pred.pn-ptcl</ta>
            <ta e="T379" id="Seg_4896" s="T378">v-v:tense-v:pred.pn</ta>
            <ta e="T380" id="Seg_4897" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_4898" s="T380">dempro</ta>
            <ta e="T382" id="Seg_4899" s="T381">que-pro:case</ta>
            <ta e="T383" id="Seg_4900" s="T382">ptcl</ta>
            <ta e="T384" id="Seg_4901" s="T383">n-n&gt;v-v:mood-v:poss.pn</ta>
            <ta e="T385" id="Seg_4902" s="T384">ptcl.[ptcl:(pred.pn)]</ta>
            <ta e="T386" id="Seg_4903" s="T385">v-v:tense-v:pred.pn</ta>
            <ta e="T387" id="Seg_4904" s="T386">n-n:case</ta>
            <ta e="T388" id="Seg_4905" s="T387">adj-n:(pred.pn)</ta>
            <ta e="T389" id="Seg_4906" s="T388">ptcl</ta>
            <ta e="T390" id="Seg_4907" s="T389">v-v:tense-v:pred.pn</ta>
            <ta e="T391" id="Seg_4908" s="T390">adj-n:(pred.pn)</ta>
            <ta e="T392" id="Seg_4909" s="T391">conj</ta>
            <ta e="T393" id="Seg_4910" s="T392">adj-n:(pred.pn)</ta>
            <ta e="T394" id="Seg_4911" s="T393">pers-pro:case</ta>
            <ta e="T395" id="Seg_4912" s="T394">conj</ta>
            <ta e="T396" id="Seg_4913" s="T395">v-v:tense-v:poss.pn</ta>
            <ta e="T397" id="Seg_4914" s="T396">v-v:tense-v:poss.pn</ta>
            <ta e="T398" id="Seg_4915" s="T397">v-v:cvb</ta>
            <ta e="T399" id="Seg_4916" s="T398">v-v:tense-v:poss.pn</ta>
            <ta e="T400" id="Seg_4917" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_4918" s="T400">dempro-pro:case</ta>
            <ta e="T402" id="Seg_4919" s="T401">adj-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_4920" s="T0">propr</ta>
            <ta e="T2" id="Seg_4921" s="T1">n</ta>
            <ta e="T3" id="Seg_4922" s="T2">v</ta>
            <ta e="T4" id="Seg_4923" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_4924" s="T4">adj</ta>
            <ta e="T6" id="Seg_4925" s="T5">adj</ta>
            <ta e="T7" id="Seg_4926" s="T6">adj</ta>
            <ta e="T8" id="Seg_4927" s="T7">cardnum</ta>
            <ta e="T9" id="Seg_4928" s="T8">n</ta>
            <ta e="T10" id="Seg_4929" s="T9">propr</ta>
            <ta e="T11" id="Seg_4930" s="T10">n</ta>
            <ta e="T12" id="Seg_4931" s="T11">dempro</ta>
            <ta e="T13" id="Seg_4932" s="T12">propr</ta>
            <ta e="T14" id="Seg_4933" s="T13">n</ta>
            <ta e="T15" id="Seg_4934" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_4935" s="T15">adv</ta>
            <ta e="T17" id="Seg_4936" s="T16">n</ta>
            <ta e="T18" id="Seg_4937" s="T17">interj</ta>
            <ta e="T19" id="Seg_4938" s="T18">n</ta>
            <ta e="T20" id="Seg_4939" s="T19">v</ta>
            <ta e="T21" id="Seg_4940" s="T20">post</ta>
            <ta e="T23" id="Seg_4941" s="T21">v</ta>
            <ta e="T24" id="Seg_4942" s="T23">v</ta>
            <ta e="T25" id="Seg_4943" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_4944" s="T25">que</ta>
            <ta e="T27" id="Seg_4945" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_4946" s="T27">n</ta>
            <ta e="T29" id="Seg_4947" s="T28">v</ta>
            <ta e="T30" id="Seg_4948" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_4949" s="T30">n</ta>
            <ta e="T32" id="Seg_4950" s="T31">v</ta>
            <ta e="T33" id="Seg_4951" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_4952" s="T33">que</ta>
            <ta e="T35" id="Seg_4953" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_4954" s="T35">n</ta>
            <ta e="T37" id="Seg_4955" s="T36">v</ta>
            <ta e="T38" id="Seg_4956" s="T37">n</ta>
            <ta e="T39" id="Seg_4957" s="T38">adj</ta>
            <ta e="T40" id="Seg_4958" s="T39">dempro</ta>
            <ta e="T41" id="Seg_4959" s="T40">n</ta>
            <ta e="T42" id="Seg_4960" s="T41">v</ta>
            <ta e="T43" id="Seg_4961" s="T42">n</ta>
            <ta e="T44" id="Seg_4962" s="T43">adv</ta>
            <ta e="T45" id="Seg_4963" s="T44">n</ta>
            <ta e="T46" id="Seg_4964" s="T45">cop</ta>
            <ta e="T47" id="Seg_4965" s="T46">n</ta>
            <ta e="T48" id="Seg_4966" s="T47">v</ta>
            <ta e="T49" id="Seg_4967" s="T48">post</ta>
            <ta e="T50" id="Seg_4968" s="T49">n</ta>
            <ta e="T51" id="Seg_4969" s="T50">v</ta>
            <ta e="T52" id="Seg_4970" s="T51">adv</ta>
            <ta e="T53" id="Seg_4971" s="T52">que</ta>
            <ta e="T54" id="Seg_4972" s="T53">n</ta>
            <ta e="T55" id="Seg_4973" s="T54">v</ta>
            <ta e="T56" id="Seg_4974" s="T55">adv</ta>
            <ta e="T57" id="Seg_4975" s="T56">n</ta>
            <ta e="T58" id="Seg_4976" s="T57">cop</ta>
            <ta e="T59" id="Seg_4977" s="T58">n</ta>
            <ta e="T60" id="Seg_4978" s="T59">cop</ta>
            <ta e="T61" id="Seg_4979" s="T60">adv</ta>
            <ta e="T62" id="Seg_4980" s="T61">dempro</ta>
            <ta e="T63" id="Seg_4981" s="T62">post</ta>
            <ta e="T64" id="Seg_4982" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_4983" s="T64">adv</ta>
            <ta e="T66" id="Seg_4984" s="T65">adj</ta>
            <ta e="T67" id="Seg_4985" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_4986" s="T67">n</ta>
            <ta e="T69" id="Seg_4987" s="T68">n</ta>
            <ta e="T70" id="Seg_4988" s="T69">n</ta>
            <ta e="T71" id="Seg_4989" s="T70">v</ta>
            <ta e="T72" id="Seg_4990" s="T71">aux</ta>
            <ta e="T73" id="Seg_4991" s="T72">interj</ta>
            <ta e="T74" id="Seg_4992" s="T73">n</ta>
            <ta e="T75" id="Seg_4993" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_4994" s="T75">interj</ta>
            <ta e="T77" id="Seg_4995" s="T76">n</ta>
            <ta e="T78" id="Seg_4996" s="T77">v</ta>
            <ta e="T79" id="Seg_4997" s="T78">v</ta>
            <ta e="T80" id="Seg_4998" s="T79">interj</ta>
            <ta e="T81" id="Seg_4999" s="T80">v</ta>
            <ta e="T82" id="Seg_5000" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_5001" s="T82">v</ta>
            <ta e="T84" id="Seg_5002" s="T83">v</ta>
            <ta e="T85" id="Seg_5003" s="T84">n</ta>
            <ta e="T86" id="Seg_5004" s="T85">v</ta>
            <ta e="T87" id="Seg_5005" s="T86">v</ta>
            <ta e="T88" id="Seg_5006" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_5007" s="T88">n</ta>
            <ta e="T90" id="Seg_5008" s="T89">v</ta>
            <ta e="T91" id="Seg_5009" s="T90">aux</ta>
            <ta e="T92" id="Seg_5010" s="T91">n</ta>
            <ta e="T93" id="Seg_5011" s="T403">n</ta>
            <ta e="T94" id="Seg_5012" s="T93">v</ta>
            <ta e="T95" id="Seg_5013" s="T94">n</ta>
            <ta e="T96" id="Seg_5014" s="T95">n</ta>
            <ta e="T98" id="Seg_5015" s="T96">v</ta>
            <ta e="T99" id="Seg_5016" s="T98">interj</ta>
            <ta e="T100" id="Seg_5017" s="T99">n</ta>
            <ta e="T101" id="Seg_5018" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_5019" s="T101">v</ta>
            <ta e="T103" id="Seg_5020" s="T102">v</ta>
            <ta e="T104" id="Seg_5021" s="T103">aux</ta>
            <ta e="T105" id="Seg_5022" s="T104">n</ta>
            <ta e="T106" id="Seg_5023" s="T105">v</ta>
            <ta e="T107" id="Seg_5024" s="T106">aux</ta>
            <ta e="T108" id="Seg_5025" s="T107">v</ta>
            <ta e="T109" id="Seg_5026" s="T108">v</ta>
            <ta e="T110" id="Seg_5027" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_5028" s="T110">n</ta>
            <ta e="T112" id="Seg_5029" s="T111">interj</ta>
            <ta e="T113" id="Seg_5030" s="T112">v</ta>
            <ta e="T114" id="Seg_5031" s="T113">n</ta>
            <ta e="T115" id="Seg_5032" s="T114">v</ta>
            <ta e="T116" id="Seg_5033" s="T115">interj</ta>
            <ta e="T117" id="Seg_5034" s="T116">adv</ta>
            <ta e="T118" id="Seg_5035" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_5036" s="T118">adj</ta>
            <ta e="T120" id="Seg_5037" s="T119">v</ta>
            <ta e="T121" id="Seg_5038" s="T120">n</ta>
            <ta e="T122" id="Seg_5039" s="T121">dempro</ta>
            <ta e="T123" id="Seg_5040" s="T122">n</ta>
            <ta e="T124" id="Seg_5041" s="T123">v</ta>
            <ta e="T125" id="Seg_5042" s="T124">dempro</ta>
            <ta e="T126" id="Seg_5043" s="T125">v</ta>
            <ta e="T127" id="Seg_5044" s="T126">v</ta>
            <ta e="T128" id="Seg_5045" s="T127">v</ta>
            <ta e="T129" id="Seg_5046" s="T128">v</ta>
            <ta e="T130" id="Seg_5047" s="T129">aux</ta>
            <ta e="T131" id="Seg_5048" s="T130">n</ta>
            <ta e="T132" id="Seg_5049" s="T131">adv</ta>
            <ta e="T133" id="Seg_5050" s="T132">v</ta>
            <ta e="T134" id="Seg_5051" s="T133">n</ta>
            <ta e="T135" id="Seg_5052" s="T134">interj</ta>
            <ta e="T136" id="Seg_5053" s="T135">n</ta>
            <ta e="T137" id="Seg_5054" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_5055" s="T137">v</ta>
            <ta e="T139" id="Seg_5056" s="T138">aux</ta>
            <ta e="T140" id="Seg_5057" s="T139">n</ta>
            <ta e="T141" id="Seg_5058" s="T140">n</ta>
            <ta e="T142" id="Seg_5059" s="T141">adj</ta>
            <ta e="T143" id="Seg_5060" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_5061" s="T143">v</ta>
            <ta e="T145" id="Seg_5062" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_5063" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_5064" s="T146">adj</ta>
            <ta e="T148" id="Seg_5065" s="T147">ptcl</ta>
            <ta e="T149" id="Seg_5066" s="T148">adj</ta>
            <ta e="T150" id="Seg_5067" s="T149">pers</ta>
            <ta e="T151" id="Seg_5068" s="T150">n</ta>
            <ta e="T152" id="Seg_5069" s="T151">v</ta>
            <ta e="T153" id="Seg_5070" s="T152">v</ta>
            <ta e="T154" id="Seg_5071" s="T153">dempro</ta>
            <ta e="T155" id="Seg_5072" s="T154">adj</ta>
            <ta e="T156" id="Seg_5073" s="T155">n</ta>
            <ta e="T157" id="Seg_5074" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_5075" s="T157">que</ta>
            <ta e="T159" id="Seg_5076" s="T158">n</ta>
            <ta e="T160" id="Seg_5077" s="T159">cop</ta>
            <ta e="T161" id="Seg_5078" s="T160">conj</ta>
            <ta e="T162" id="Seg_5079" s="T161">n</ta>
            <ta e="T163" id="Seg_5080" s="T162">n</ta>
            <ta e="T164" id="Seg_5081" s="T163">v</ta>
            <ta e="T165" id="Seg_5082" s="T164">v</ta>
            <ta e="T166" id="Seg_5083" s="T165">aux</ta>
            <ta e="T167" id="Seg_5084" s="T166">v</ta>
            <ta e="T168" id="Seg_5085" s="T167">n</ta>
            <ta e="T169" id="Seg_5086" s="T168">n</ta>
            <ta e="T170" id="Seg_5087" s="T169">adj</ta>
            <ta e="T171" id="Seg_5088" s="T170">conj</ta>
            <ta e="T172" id="Seg_5089" s="T171">adj</ta>
            <ta e="T173" id="Seg_5090" s="T172">v</ta>
            <ta e="T174" id="Seg_5091" s="T173">pers</ta>
            <ta e="T175" id="Seg_5092" s="T174">v</ta>
            <ta e="T176" id="Seg_5093" s="T175">cop</ta>
            <ta e="T177" id="Seg_5094" s="T176">adj</ta>
            <ta e="T178" id="Seg_5095" s="T177">v</ta>
            <ta e="T179" id="Seg_5096" s="T178">v</ta>
            <ta e="T180" id="Seg_5097" s="T179">n</ta>
            <ta e="T181" id="Seg_5098" s="T180">adj</ta>
            <ta e="T182" id="Seg_5099" s="T181">ptcl</ta>
            <ta e="T183" id="Seg_5100" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_5101" s="T183">adj</ta>
            <ta e="T185" id="Seg_5102" s="T184">conj</ta>
            <ta e="T186" id="Seg_5103" s="T185">adj</ta>
            <ta e="T187" id="Seg_5104" s="T186">v</ta>
            <ta e="T188" id="Seg_5105" s="T187">n</ta>
            <ta e="T189" id="Seg_5106" s="T188">conj</ta>
            <ta e="T190" id="Seg_5107" s="T189">n</ta>
            <ta e="T191" id="Seg_5108" s="T190">conj</ta>
            <ta e="T192" id="Seg_5109" s="T191">v</ta>
            <ta e="T193" id="Seg_5110" s="T192">v</ta>
            <ta e="T194" id="Seg_5111" s="T193">v</ta>
            <ta e="T195" id="Seg_5112" s="T194">interj</ta>
            <ta e="T196" id="Seg_5113" s="T195">dempro</ta>
            <ta e="T197" id="Seg_5114" s="T196">adj</ta>
            <ta e="T198" id="Seg_5115" s="T197">n</ta>
            <ta e="T199" id="Seg_5116" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_5117" s="T199">que</ta>
            <ta e="T201" id="Seg_5118" s="T200">n</ta>
            <ta e="T202" id="Seg_5119" s="T201">n</ta>
            <ta e="T203" id="Seg_5120" s="T202">n</ta>
            <ta e="T204" id="Seg_5121" s="T203">n</ta>
            <ta e="T205" id="Seg_5122" s="T204">v</ta>
            <ta e="T206" id="Seg_5123" s="T205">aux</ta>
            <ta e="T207" id="Seg_5124" s="T206">v</ta>
            <ta e="T208" id="Seg_5125" s="T207">n</ta>
            <ta e="T209" id="Seg_5126" s="T208">n</ta>
            <ta e="T210" id="Seg_5127" s="T209">adj</ta>
            <ta e="T211" id="Seg_5128" s="T210">conj</ta>
            <ta e="T212" id="Seg_5129" s="T211">adj</ta>
            <ta e="T213" id="Seg_5130" s="T212">v</ta>
            <ta e="T214" id="Seg_5131" s="T213">pers</ta>
            <ta e="T215" id="Seg_5132" s="T214">n</ta>
            <ta e="T216" id="Seg_5133" s="T215">v</ta>
            <ta e="T217" id="Seg_5134" s="T216">v</ta>
            <ta e="T218" id="Seg_5135" s="T217">n</ta>
            <ta e="T219" id="Seg_5136" s="T218">n</ta>
            <ta e="T220" id="Seg_5137" s="T219">adj</ta>
            <ta e="T221" id="Seg_5138" s="T220">ptcl</ta>
            <ta e="T222" id="Seg_5139" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_5140" s="T222">adj</ta>
            <ta e="T224" id="Seg_5141" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_5142" s="T224">adj</ta>
            <ta e="T226" id="Seg_5143" s="T225">n</ta>
            <ta e="T227" id="Seg_5144" s="T226">ptcl</ta>
            <ta e="T228" id="Seg_5145" s="T227">v</ta>
            <ta e="T229" id="Seg_5146" s="T228">v</ta>
            <ta e="T230" id="Seg_5147" s="T229">n</ta>
            <ta e="T231" id="Seg_5148" s="T230">post</ta>
            <ta e="T232" id="Seg_5149" s="T231">ptcl</ta>
            <ta e="T233" id="Seg_5150" s="T232">n</ta>
            <ta e="T234" id="Seg_5151" s="T233">v</ta>
            <ta e="T235" id="Seg_5152" s="T234">v</ta>
            <ta e="T236" id="Seg_5153" s="T235">dempro</ta>
            <ta e="T237" id="Seg_5154" s="T236">adj</ta>
            <ta e="T238" id="Seg_5155" s="T237">n</ta>
            <ta e="T239" id="Seg_5156" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_5157" s="T239">que</ta>
            <ta e="T241" id="Seg_5158" s="T240">adj</ta>
            <ta e="T242" id="Seg_5159" s="T241">n</ta>
            <ta e="T243" id="Seg_5160" s="T242">post</ta>
            <ta e="T244" id="Seg_5161" s="T243">v</ta>
            <ta e="T245" id="Seg_5162" s="T244">aux</ta>
            <ta e="T246" id="Seg_5163" s="T245">v</ta>
            <ta e="T247" id="Seg_5164" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_5165" s="T247">adj</ta>
            <ta e="T249" id="Seg_5166" s="T248">n</ta>
            <ta e="T250" id="Seg_5167" s="T249">adj</ta>
            <ta e="T251" id="Seg_5168" s="T250">conj</ta>
            <ta e="T252" id="Seg_5169" s="T251">adj</ta>
            <ta e="T253" id="Seg_5170" s="T252">adv</ta>
            <ta e="T254" id="Seg_5171" s="T253">adj</ta>
            <ta e="T255" id="Seg_5172" s="T254">adj</ta>
            <ta e="T256" id="Seg_5173" s="T255">v</ta>
            <ta e="T257" id="Seg_5174" s="T256">n</ta>
            <ta e="T258" id="Seg_5175" s="T257">adj</ta>
            <ta e="T259" id="Seg_5176" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_5177" s="T259">adj</ta>
            <ta e="T261" id="Seg_5178" s="T260">conj</ta>
            <ta e="T262" id="Seg_5179" s="T261">adj</ta>
            <ta e="T263" id="Seg_5180" s="T262">n</ta>
            <ta e="T264" id="Seg_5181" s="T263">n</ta>
            <ta e="T265" id="Seg_5182" s="T264">que</ta>
            <ta e="T266" id="Seg_5183" s="T265">v</ta>
            <ta e="T267" id="Seg_5184" s="T266">v</ta>
            <ta e="T268" id="Seg_5185" s="T267">v</ta>
            <ta e="T269" id="Seg_5186" s="T268">v</ta>
            <ta e="T270" id="Seg_5187" s="T269">dempro</ta>
            <ta e="T271" id="Seg_5188" s="T270">adj</ta>
            <ta e="T272" id="Seg_5189" s="T271">n</ta>
            <ta e="T273" id="Seg_5190" s="T272">ptcl</ta>
            <ta e="T274" id="Seg_5191" s="T273">que</ta>
            <ta e="T275" id="Seg_5192" s="T274">n</ta>
            <ta e="T276" id="Seg_5193" s="T275">v</ta>
            <ta e="T277" id="Seg_5194" s="T276">v</ta>
            <ta e="T278" id="Seg_5195" s="T277">n</ta>
            <ta e="T279" id="Seg_5196" s="T278">n</ta>
            <ta e="T280" id="Seg_5197" s="T279">v</ta>
            <ta e="T281" id="Seg_5198" s="T280">v</ta>
            <ta e="T282" id="Seg_5199" s="T281">pers</ta>
            <ta e="T283" id="Seg_5200" s="T282">v</ta>
            <ta e="T284" id="Seg_5201" s="T283">conj</ta>
            <ta e="T285" id="Seg_5202" s="T284">v</ta>
            <ta e="T286" id="Seg_5203" s="T285">n</ta>
            <ta e="T287" id="Seg_5204" s="T286">n</ta>
            <ta e="T288" id="Seg_5205" s="T287">adj</ta>
            <ta e="T289" id="Seg_5206" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_5207" s="T289">adj</ta>
            <ta e="T291" id="Seg_5208" s="T290">conj</ta>
            <ta e="T292" id="Seg_5209" s="T291">adj</ta>
            <ta e="T293" id="Seg_5210" s="T292">v</ta>
            <ta e="T294" id="Seg_5211" s="T293">n</ta>
            <ta e="T295" id="Seg_5212" s="T294">que</ta>
            <ta e="T296" id="Seg_5213" s="T295">v</ta>
            <ta e="T297" id="Seg_5214" s="T296">v</ta>
            <ta e="T298" id="Seg_5215" s="T297">aux</ta>
            <ta e="T299" id="Seg_5216" s="T298">v</ta>
            <ta e="T300" id="Seg_5217" s="T299">dempro</ta>
            <ta e="T301" id="Seg_5218" s="T300">adj</ta>
            <ta e="T302" id="Seg_5219" s="T301">n</ta>
            <ta e="T303" id="Seg_5220" s="T302">ptcl</ta>
            <ta e="T304" id="Seg_5221" s="T303">adj</ta>
            <ta e="T305" id="Seg_5222" s="T304">adj</ta>
            <ta e="T306" id="Seg_5223" s="T305">n</ta>
            <ta e="T307" id="Seg_5224" s="T306">que</ta>
            <ta e="T308" id="Seg_5225" s="T307">v</ta>
            <ta e="T309" id="Seg_5226" s="T308">v</ta>
            <ta e="T310" id="Seg_5227" s="T309">adj</ta>
            <ta e="T311" id="Seg_5228" s="T310">adj</ta>
            <ta e="T312" id="Seg_5229" s="T311">n</ta>
            <ta e="T313" id="Seg_5230" s="T312">n</ta>
            <ta e="T314" id="Seg_5231" s="T313">n</ta>
            <ta e="T315" id="Seg_5232" s="T314">v</ta>
            <ta e="T316" id="Seg_5233" s="T315">que</ta>
            <ta e="T317" id="Seg_5234" s="T316">v</ta>
            <ta e="T318" id="Seg_5235" s="T317">n</ta>
            <ta e="T319" id="Seg_5236" s="T318">v</ta>
            <ta e="T320" id="Seg_5237" s="T319">dempro</ta>
            <ta e="T321" id="Seg_5238" s="T320">post</ta>
            <ta e="T322" id="Seg_5239" s="T321">v</ta>
            <ta e="T323" id="Seg_5240" s="T322">v</ta>
            <ta e="T324" id="Seg_5241" s="T323">v</ta>
            <ta e="T325" id="Seg_5242" s="T324">v</ta>
            <ta e="T326" id="Seg_5243" s="T325">v</ta>
            <ta e="T327" id="Seg_5244" s="T326">adj</ta>
            <ta e="T328" id="Seg_5245" s="T327">adj</ta>
            <ta e="T329" id="Seg_5246" s="T328">n</ta>
            <ta e="T330" id="Seg_5247" s="T329">adj</ta>
            <ta e="T331" id="Seg_5248" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_5249" s="T331">adj</ta>
            <ta e="T333" id="Seg_5250" s="T332">conj</ta>
            <ta e="T334" id="Seg_5251" s="T333">adj</ta>
            <ta e="T335" id="Seg_5252" s="T334">n</ta>
            <ta e="T336" id="Seg_5253" s="T335">que</ta>
            <ta e="T337" id="Seg_5254" s="T336">v</ta>
            <ta e="T338" id="Seg_5255" s="T337">v</ta>
            <ta e="T339" id="Seg_5256" s="T338">dempro</ta>
            <ta e="T340" id="Seg_5257" s="T339">adj</ta>
            <ta e="T341" id="Seg_5258" s="T340">n</ta>
            <ta e="T342" id="Seg_5259" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_5260" s="T342">que</ta>
            <ta e="T344" id="Seg_5261" s="T343">n</ta>
            <ta e="T345" id="Seg_5262" s="T344">v</ta>
            <ta e="T346" id="Seg_5263" s="T345">v</ta>
            <ta e="T347" id="Seg_5264" s="T346">n</ta>
            <ta e="T348" id="Seg_5265" s="T347">adj</ta>
            <ta e="T349" id="Seg_5266" s="T348">cop</ta>
            <ta e="T350" id="Seg_5267" s="T349">v</ta>
            <ta e="T351" id="Seg_5268" s="T350">v</ta>
            <ta e="T352" id="Seg_5269" s="T351">v</ta>
            <ta e="T353" id="Seg_5270" s="T352">conj</ta>
            <ta e="T354" id="Seg_5271" s="T353">v</ta>
            <ta e="T355" id="Seg_5272" s="T354">v</ta>
            <ta e="T356" id="Seg_5273" s="T355">n</ta>
            <ta e="T357" id="Seg_5274" s="T356">adj</ta>
            <ta e="T358" id="Seg_5275" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_5276" s="T358">adj</ta>
            <ta e="T360" id="Seg_5277" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_5278" s="T360">adj</ta>
            <ta e="T362" id="Seg_5279" s="T361">que</ta>
            <ta e="T363" id="Seg_5280" s="T362">v</ta>
            <ta e="T364" id="Seg_5281" s="T363">conj</ta>
            <ta e="T365" id="Seg_5282" s="T364">v</ta>
            <ta e="T366" id="Seg_5283" s="T365">adv</ta>
            <ta e="T367" id="Seg_5284" s="T366">v</ta>
            <ta e="T368" id="Seg_5285" s="T367">n</ta>
            <ta e="T369" id="Seg_5286" s="T368">cop</ta>
            <ta e="T370" id="Seg_5287" s="T369">conj</ta>
            <ta e="T371" id="Seg_5288" s="T370">v</ta>
            <ta e="T372" id="Seg_5289" s="T371">dempro</ta>
            <ta e="T373" id="Seg_5290" s="T372">adj</ta>
            <ta e="T374" id="Seg_5291" s="T373">n</ta>
            <ta e="T375" id="Seg_5292" s="T374">ptcl</ta>
            <ta e="T376" id="Seg_5293" s="T375">que</ta>
            <ta e="T377" id="Seg_5294" s="T376">n</ta>
            <ta e="T378" id="Seg_5295" s="T377">v</ta>
            <ta e="T379" id="Seg_5296" s="T378">v</ta>
            <ta e="T380" id="Seg_5297" s="T379">n</ta>
            <ta e="T381" id="Seg_5298" s="T380">dempro</ta>
            <ta e="T382" id="Seg_5299" s="T381">que</ta>
            <ta e="T383" id="Seg_5300" s="T382">ptcl</ta>
            <ta e="T384" id="Seg_5301" s="T383">v</ta>
            <ta e="T385" id="Seg_5302" s="T384">ptcl</ta>
            <ta e="T386" id="Seg_5303" s="T385">v</ta>
            <ta e="T387" id="Seg_5304" s="T386">n</ta>
            <ta e="T388" id="Seg_5305" s="T387">adj</ta>
            <ta e="T389" id="Seg_5306" s="T388">ptcl</ta>
            <ta e="T390" id="Seg_5307" s="T389">v</ta>
            <ta e="T391" id="Seg_5308" s="T390">adj</ta>
            <ta e="T392" id="Seg_5309" s="T391">conj</ta>
            <ta e="T393" id="Seg_5310" s="T392">adj</ta>
            <ta e="T394" id="Seg_5311" s="T393">pers</ta>
            <ta e="T395" id="Seg_5312" s="T394">conj</ta>
            <ta e="T396" id="Seg_5313" s="T395">v</ta>
            <ta e="T397" id="Seg_5314" s="T396">v</ta>
            <ta e="T398" id="Seg_5315" s="T397">v</ta>
            <ta e="T399" id="Seg_5316" s="T398">aux</ta>
            <ta e="T400" id="Seg_5317" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_5318" s="T400">dempro</ta>
            <ta e="T402" id="Seg_5319" s="T401">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T19" id="Seg_5320" s="T18">RUS:cult</ta>
            <ta e="T59" id="Seg_5321" s="T58">RUS:cult</ta>
            <ta e="T77" id="Seg_5322" s="T76">RUS:cult</ta>
            <ta e="T161" id="Seg_5323" s="T160">RUS:gram</ta>
            <ta e="T171" id="Seg_5324" s="T170">RUS:gram</ta>
            <ta e="T185" id="Seg_5325" s="T184">RUS:gram</ta>
            <ta e="T189" id="Seg_5326" s="T188">RUS:gram</ta>
            <ta e="T191" id="Seg_5327" s="T190">RUS:gram</ta>
            <ta e="T211" id="Seg_5328" s="T210">RUS:gram</ta>
            <ta e="T251" id="Seg_5329" s="T250">RUS:gram</ta>
            <ta e="T261" id="Seg_5330" s="T260">RUS:gram</ta>
            <ta e="T284" id="Seg_5331" s="T283">RUS:gram</ta>
            <ta e="T291" id="Seg_5332" s="T290">RUS:gram</ta>
            <ta e="T318" id="Seg_5333" s="T317">RUS:cult</ta>
            <ta e="T333" id="Seg_5334" s="T332">RUS:gram</ta>
            <ta e="T353" id="Seg_5335" s="T352">RUS:gram</ta>
            <ta e="T364" id="Seg_5336" s="T363">RUS:gram</ta>
            <ta e="T370" id="Seg_5337" s="T369">RUS:gram</ta>
            <ta e="T377" id="Seg_5338" s="T376">RUS:core</ta>
            <ta e="T380" id="Seg_5339" s="T379">RUS:core</ta>
            <ta e="T387" id="Seg_5340" s="T386">RUS:core</ta>
            <ta e="T392" id="Seg_5341" s="T391">RUS:gram</ta>
            <ta e="T395" id="Seg_5342" s="T394">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T11" id="Seg_5343" s="T0">There lived the old woman Taal, it is said, in the colorful middle nine worlds decorated with ornaments, the old woman Taal.</ta>
            <ta e="T23" id="Seg_5344" s="T11">But this old woman Taal, once in the evening, well, having drunk tea, sleeping…</ta>
            <ta e="T27" id="Seg_5345" s="T23">Did she fall asleep or what?</ta>
            <ta e="T35" id="Seg_5346" s="T27">Did the edge of the chum drip, did it rain, or what?</ta>
            <ta e="T39" id="Seg_5347" s="T35">The bedding got wet, the old woman's one.</ta>
            <ta e="T49" id="Seg_5348" s="T39">The bedding got wet, the old woman in the morning, it was autumn when the ice was freezing.</ta>
            <ta e="T61" id="Seg_5349" s="T49">The old woman stood up, what kind of fire was there, was it a proper stove, it was a fire place.</ta>
            <ta e="T72" id="Seg_5350" s="T61">Therefore the old woman hung up her bedding on the storage on a very sunny day, apparently.</ta>
            <ta e="T82" id="Seg_5351" s="T72">Well, the bedding then, well, while she was sitting and drinking tea, [it] made "whish".</ta>
            <ta e="T98" id="Seg_5352" s="T82">There the old woman ran out, she ran our, when her bedding was flying, the lake (...) at the edge of the small lake, where she was scooping water, she fell down.</ta>
            <ta e="T110" id="Seg_5353" s="T98">Oh, but the old woman went and caught [it], "I'll catch my bedding and bring it", she was thinking apparently.</ta>
            <ta e="T115" id="Seg_5354" s="T110">The wind let it make "whish" and [the bedding] flew onto the ice.</ta>
            <ta e="T124" id="Seg_5355" s="T115">Well, in autumn the old woman went out onto the open ice.</ta>
            <ta e="T134" id="Seg_5356" s="T124">"Now I'll get it", the old woman thought, slipped and broke her hip.</ta>
            <ta e="T139" id="Seg_5357" s="T134">Well, the old woman says:</ta>
            <ta e="T144" id="Seg_5358" s="T139">"Ice, ice, are you strong?", she says.</ta>
            <ta e="T153" id="Seg_5359" s="T144">"Yes, I am the strongest, I broke your hip", it says.</ta>
            <ta e="T167" id="Seg_5360" s="T153">"If you are so strong, why do you melt in the sunlight, when spring is coming?", she said.</ta>
            <ta e="T173" id="Seg_5361" s="T167">"The sunlight is the strongest", it says.</ta>
            <ta e="T179" id="Seg_5362" s="T173">"Apart from me it melts different things", it says.</ta>
            <ta e="T180" id="Seg_5363" s="T179">"Sun!</ta>
            <ta e="T182" id="Seg_5364" s="T180">Are you strong?"</ta>
            <ta e="T187" id="Seg_5365" s="T182">"I am the strongest", it said.</ta>
            <ta e="T194" id="Seg_5366" s="T187">"I melt snow and ice not leaving anything", it says.</ta>
            <ta e="T207" id="Seg_5367" s="T194">"Oh, if you are so strong, why do you hide behind the stone mountain?", she says.</ta>
            <ta e="T217" id="Seg_5368" s="T207">"The stone mountain is the strongest", it says, "it covers from the front", it says.</ta>
            <ta e="T219" id="Seg_5369" s="T217">"Stone mountain!</ta>
            <ta e="T221" id="Seg_5370" s="T219">Are you strong?"</ta>
            <ta e="T225" id="Seg_5371" s="T221">"Well, I am the strongest.</ta>
            <ta e="T235" id="Seg_5372" s="T225">I cover the sun, though", it says, "during the whole winter I make it dark", it says.</ta>
            <ta e="T246" id="Seg_5373" s="T235">"If you are so strong, why do you let the narrowheaded lemmings pierce you?", she said.</ta>
            <ta e="T256" id="Seg_5374" s="T246">"Well, the narrowheaded lemmings are the strongest, they have very sharp snouts", it says.</ta>
            <ta e="T259" id="Seg_5375" s="T256">"Lemmings, are you strong?"</ta>
            <ta e="T269" id="Seg_5376" s="T259">"We are the strongest, we come out of the mountain via different ways", they said.</ta>
            <ta e="T277" id="Seg_5377" s="T269">"If you are so strong, why do you let the polar fox eat you?", she said.</ta>
            <ta e="T286" id="Seg_5378" s="T277">"Of course", it says, "the polar fox eats us, the polar fox eats and eats."</ta>
            <ta e="T289" id="Seg_5379" s="T286">"Polar fox, are you strong?"</ta>
            <ta e="T293" id="Seg_5380" s="T289">"I am the strongest", it said.</ta>
            <ta e="T299" id="Seg_5381" s="T293">"We dig out lemmings everywhere and eat them."</ta>
            <ta e="T309" id="Seg_5382" s="T299">"If you are so strong, why do you let the Dolgan children catch you with traps?", she said.</ta>
            <ta e="T319" id="Seg_5383" s="T309">"When the Dolgan children bury them under the snow, how shall I notice the gin traps?", it says.</ta>
            <ta e="T326" id="Seg_5384" s="T319">"Therefore I let [them] catch me", it says, "when I come eating", it says.</ta>
            <ta e="T331" id="Seg_5385" s="T326">"Dolgan children with traps, are you strong?"</ta>
            <ta e="T338" id="Seg_5386" s="T331">"We are the strongest, we catch the polar fox whenever we want."</ta>
            <ta e="T346" id="Seg_5387" s="T338">"If you are so strong, why do you let the shaman eat you", she said.</ta>
            <ta e="T355" id="Seg_5388" s="T346">"The shaman is very experiences", it says, "he says 'I'll eat [you]' and eats", it says.</ta>
            <ta e="T356" id="Seg_5389" s="T355">"Shaman!</ta>
            <ta e="T358" id="Seg_5390" s="T356">Are you strong?"</ta>
            <ta e="T361" id="Seg_5391" s="T358">"I am the strongest.</ta>
            <ta e="T371" id="Seg_5392" s="T361">If I want, I'll eat, it is a human talking bad", he says.</ta>
            <ta e="T379" id="Seg_5393" s="T371">"If you are so strong, why do you let the death eat you", she says.</ta>
            <ta e="T386" id="Seg_5394" s="T379">"The death doesn't say anything to you", he said.</ta>
            <ta e="T387" id="Seg_5395" s="T386">"Death!</ta>
            <ta e="T390" id="Seg_5396" s="T387">Are you strong?", she says.</ta>
            <ta e="T399" id="Seg_5397" s="T390">"I am the strongest and I'll eat you", it said and ate her.</ta>
            <ta e="T402" id="Seg_5398" s="T399">Well, that's all.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T11" id="Seg_5399" s="T0">Es lebte die alte Frau Taal, sagt man, in den mit Ornamenten verzierten und farbigen mittleren neun Welten, die alte Frau Taal.</ta>
            <ta e="T23" id="Seg_5400" s="T11">Diese Alte Taal aber, einmal am Abend, nun, nachdem sie Tee getrunken hatte, schlief…</ta>
            <ta e="T27" id="Seg_5401" s="T23">Schlief sie ein oder wie?</ta>
            <ta e="T35" id="Seg_5402" s="T27">Tropfte der Rand des Zelts, fiel Regen, oder was?</ta>
            <ta e="T39" id="Seg_5403" s="T35">Die Bettwäsche wurde nass, die von der Alten.</ta>
            <ta e="T49" id="Seg_5404" s="T39">Die Bettwäsche wurde nass, die Alte am Morgen, es war Herbst, als das Eis fror.</ta>
            <ta e="T61" id="Seg_5405" s="T49">Die Alte stand auf, was für ein Feuer war dann da, war es ein richtiger Ofen – es war eine Feuerstelle.</ta>
            <ta e="T72" id="Seg_5406" s="T61">Deshalb offenbar hängte die Alte ihre Bettwäsche in den sehr sonnigen Tag auf den Speicher.</ta>
            <ta e="T82" id="Seg_5407" s="T72">Nun, die Bettwäsche also, nun, als sie saß und Tee trank, machte da "hui".</ta>
            <ta e="T98" id="Seg_5408" s="T82">Da rannte die Alte hinaus, sie rannte also hinaus, als ihre Bettwäsche flog, der See (…) am Rande des kleinen Sees, wo sie Wasser schöpfte, fiel sie hinunter.</ta>
            <ta e="T110" id="Seg_5409" s="T98">Oh, die Alte aber ging und fing, "ich fange mein Bettwäsche und bringe sie", dachte sie wohl.</ta>
            <ta e="T115" id="Seg_5410" s="T110">Der Wind ließ es "hui" machen und [die Bettwäsche] flog aufs Eis.</ta>
            <ta e="T124" id="Seg_5411" s="T115">Nun, im Herbst ging die Alte auf das offene Eis hinaus.</ta>
            <ta e="T134" id="Seg_5412" s="T124">"Jetzt kriege ich sie", dachte die Alte, rutschte aus und brach sich die Hüfte.</ta>
            <ta e="T139" id="Seg_5413" s="T134">Nun, das sagt die Alte:</ta>
            <ta e="T144" id="Seg_5414" s="T139">"Eis, Eis, bist du stark?", sagt sie.</ta>
            <ta e="T153" id="Seg_5415" s="T144">"Ja, ich bin der stärkste, ich habe deine Hüfte gebrochen", sagt es.</ta>
            <ta e="T167" id="Seg_5416" s="T153">"Wenn du so stark bist, warum, wenn der Frühling kommt, taust du dann vom Licht der Sonne?", sagte sie.</ta>
            <ta e="T173" id="Seg_5417" s="T167">"Das Licht der Sonne ist der stärkste", sagt es.</ta>
            <ta e="T179" id="Seg_5418" s="T173">"Außer, dass es mich wegtaut, taut es Verschiedenes weg", sagt es.</ta>
            <ta e="T180" id="Seg_5419" s="T179">"Sonne!</ta>
            <ta e="T182" id="Seg_5420" s="T180">Bist du stark?"</ta>
            <ta e="T187" id="Seg_5421" s="T182">"Ich bin die stärkste", sagt sie.</ta>
            <ta e="T194" id="Seg_5422" s="T187">"Schnee und Eis lasse ich schmelzen, ohne etwas übrig zu lassen", sagt sie.</ta>
            <ta e="T207" id="Seg_5423" s="T194">"Oh, wenn du so stark bist, warum versteckst du dich dann hinter dem Steinberg?", sagt sie.</ta>
            <ta e="T217" id="Seg_5424" s="T207">"Der Steinberg ist am stärksten", sagt sie, "er verdeckt mich von vorne", sagt sie.</ta>
            <ta e="T219" id="Seg_5425" s="T217">"Steinberg!</ta>
            <ta e="T221" id="Seg_5426" s="T219">Bist du stark?"</ta>
            <ta e="T225" id="Seg_5427" s="T221">"Nun, ich bin am stärksten.</ta>
            <ta e="T235" id="Seg_5428" s="T225">Ich verdecke doch die Sonne", sagt er, "den ganzen Winter sorge ich für Dunkelheit", sagt er.</ta>
            <ta e="T246" id="Seg_5429" s="T235">"Wenn du so stark bist, warum lässt du dich von den spitzmäuligen Lemmingen dann durchstoßen?", sagte sie.</ta>
            <ta e="T256" id="Seg_5430" s="T246">"Nun, die sptzmäuligen Lemminge sind die stärksten, sie haben sehr scharfe Schnauzen", sagt er.</ta>
            <ta e="T259" id="Seg_5431" s="T256">"Lemminge, seid ihr stark?"</ta>
            <ta e="T269" id="Seg_5432" s="T259">"Wir sind die stärksten, wir kommen auf allen möglichen Wegen aus dem Berg heraus", sagten sie.</ta>
            <ta e="T277" id="Seg_5433" s="T269">"Wenn ihr so stark seid, warum lasst ihr euch dann vom Polarfuchs fressen?", sagte sie.</ta>
            <ta e="T286" id="Seg_5434" s="T277">"Natürlich", sagt er, "frisst uns der Polarfuchs, es frisst und frisst der Polarfuchs."</ta>
            <ta e="T289" id="Seg_5435" s="T286">"Polarfuchs, bist du stark?"</ta>
            <ta e="T293" id="Seg_5436" s="T289">"Ich bin der stärkste", sagt er.</ta>
            <ta e="T299" id="Seg_5437" s="T293">"Wir graben Lemminge überall aus und fressen sie."</ta>
            <ta e="T309" id="Seg_5438" s="T299">"Wenn du so stark bist, warum lässt du dich von dolganischen Kindern mit Fallen fangen?", sagte sie.</ta>
            <ta e="T319" id="Seg_5439" s="T309">"Wenn die dolganischen Kindern sie unter dem Schnee vergraben, wie soll ich die Fangeisen bemerken?", sagt er.</ta>
            <ta e="T326" id="Seg_5440" s="T319">"Deshalb lasse ich mich fangen", sagt er, "wenn ich zum Fressen komme", sagt er.</ta>
            <ta e="T331" id="Seg_5441" s="T326">"Dolganische Kinder mit Fallen, seid ihr stark?"</ta>
            <ta e="T338" id="Seg_5442" s="T331">"Wir sind die stärksten, wir fangen den Polarfuchs, wann immer wir wollen."</ta>
            <ta e="T346" id="Seg_5443" s="T338">"Wenn ihr so stark seid, warum lasst ihr euch vom Schamanen essen", sagte sie.</ta>
            <ta e="T355" id="Seg_5444" s="T346">"Der Schamane ist sehr erfahren", sagt es, "er sagt 'ich esse [dich]' und isst", sagt es.</ta>
            <ta e="T356" id="Seg_5445" s="T355">"Schamane!</ta>
            <ta e="T358" id="Seg_5446" s="T356">Bist du stark?"</ta>
            <ta e="T361" id="Seg_5447" s="T358">"Ich bin der stärkste.</ta>
            <ta e="T371" id="Seg_5448" s="T361">Wenn ich möchte, dann esse ich, es ist ein schlecht sprechender Mensch", sagt er.</ta>
            <ta e="T379" id="Seg_5449" s="T371">"Wenn du so stark bist, warum lässt du dich vom Tod fressen", sagt sie.</ta>
            <ta e="T386" id="Seg_5450" s="T379">"Der Tod sagt niemandem etwas", sagt er.</ta>
            <ta e="T387" id="Seg_5451" s="T386">"Tod!</ta>
            <ta e="T390" id="Seg_5452" s="T387">Bist du stark?", sagt sie.</ta>
            <ta e="T399" id="Seg_5453" s="T390">"Ich bin der stärkste und ich werde dich fressen", sagte er und fraß sie.</ta>
            <ta e="T402" id="Seg_5454" s="T399">Nun, das ist alles.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T11" id="Seg_5455" s="T0">ЕТ: Тал старуха жила, говорят, в разноцветном цветочом в середине девятого мира – Тал старуха.</ta>
            <ta e="T23" id="Seg_5456" s="T11">ЕТ: Тут Тал старуха однажды вечером, чаю напившись заснув.. </ta>
            <ta e="T27" id="Seg_5457" s="T23">ЕТ: уснула или, что ли?</ta>
            <ta e="T35" id="Seg_5458" s="T27">С краю чума протекло ли, дождь пролил или, что ли?</ta>
            <ta e="T39" id="Seg_5459" s="T35">Перина промокла у старухи.</ta>
            <ta e="T49" id="Seg_5460" s="T39">ЕТ: Эта перина намокнув, старухи на утро осень, оказывается, ледостата время.</ta>
            <ta e="T61" id="Seg_5461" s="T49">ЕТ: Старуха проснувшись, тогда какой огонь будет, настоящей печки не было – очаг только был. </ta>
            <ta e="T72" id="Seg_5462" s="T61">ЕТ: Поэтому, наверное, на улице в солнечную погоду навешала перину повесила.</ta>
            <ta e="T82" id="Seg_5463" s="T72">ЕТ: Боже, перина тут, боже, чай пока сидела пила, "фью" как сделала.</ta>
            <ta e="T98" id="Seg_5464" s="T82">Выскочила как старуха выскочила, как выскочила старуха, а перина влетела и у озера.., где воду брала с краю упала. </ta>
            <ta e="T110" id="Seg_5465" s="T98">ЕТ: Бог мой, старуха тут вот сходив схватла, "перину схвачу и принесу" подумала.</ta>
            <ta e="T115" id="Seg_5466" s="T110">ЕТ: Ветер, "фью" просвистнув, на лед зашла (перина). </ta>
            <ta e="T124" id="Seg_5467" s="T115">ЕТ: Бог мой, осенью на скользкий лед старушка забралась.</ta>
            <ta e="T134" id="Seg_5468" s="T124">ЕТ: Сейчас догоню, подумав, подскользнувшись, и на бок со всей силой упала старуха. </ta>
            <ta e="T139" id="Seg_5469" s="T134">ЕТ: И вот, старуха промолвила тут:</ta>
            <ta e="T144" id="Seg_5470" s="T139">ЕТ: "Лёд, лёд ты самый ли?"</ta>
            <ta e="T153" id="Seg_5471" s="T144">ЕТ: "Да, самый из самых. Твой бок вот проломил", – ответил.</ta>
            <ta e="T167" id="Seg_5472" s="T153">ЕТ: "Если такой самый, почему как весна наступает от солнечного света растаявши падаешь?" – спросил.</ta>
            <ta e="T173" id="Seg_5473" s="T167">ЕТ: "Солнечный свет самый, самый, – ответил </ta>
            <ta e="T179" id="Seg_5474" s="T173">– пуще меня растапливает всякое, –говорит".</ta>
            <ta e="T180" id="Seg_5475" s="T179">ET: "Солнце! </ta>
            <ta e="T182" id="Seg_5476" s="T180">Ты самый?"</ta>
            <ta e="T187" id="Seg_5477" s="T182">ЕТ: "Ну самый, самый, – отвечает, </ta>
            <ta e="T194" id="Seg_5478" s="T187">– хоть снег, хоть лёд нещадя растапливаю", – говорит.</ta>
            <ta e="T207" id="Seg_5479" s="T194">ЕТ: "Фу-у, если такой самый лучший, почему за каменной горой прячешься лежишь?" – спрашивает.</ta>
            <ta e="T217" id="Seg_5480" s="T207">ЕТ: "Каменная гора самая лучшая, – говорит, – меня спререди прячет, – говорит.</ta>
            <ta e="T219" id="Seg_5481" s="T217">ЕТ: "Каменная гора! </ta>
            <ta e="T221" id="Seg_5482" s="T219">Ты самая лучшая?"</ta>
            <ta e="T225" id="Seg_5483" s="T221">ЕТ: "Ну, самая лучшая".</ta>
            <ta e="T235" id="Seg_5484" s="T225">ЕТ: "Солнце-то закрываю, -- говорит. Всю зиму тьмой накрываю", --говорит.</ta>
            <ta e="T246" id="Seg_5485" s="T235">ЕТ: "Если такой самый лучший, почему мышам насквозь продырявить себя даёшь?", – говорит.</ta>
            <ta e="T256" id="Seg_5486" s="T246">"Ну леменги самые-самые. Очень острые носы имеют", – говорит.</ta>
            <ta e="T259" id="Seg_5487" s="T256">ЕТ: "Леменги, вы самые лучшие?"</ta>
            <ta e="T269" id="Seg_5488" s="T259">ЕТ: "Самые, самые, каменную гору, хоть где дыру проделав, выходим", – отвечают.</ta>
            <ta e="T277" id="Seg_5489" s="T269">ЕТ: "Если такие лучшие, почему песцам даёте себя съесть?" – спросила.</ta>
            <ta e="T286" id="Seg_5490" s="T277">ЕТ: "Песец, песец конечно кушает, – говорит, – нас.Кушает, всегда кушает нас песец".</ta>
            <ta e="T289" id="Seg_5491" s="T286">ЕТ: "Песец, ты самый луший?"</ta>
            <ta e="T293" id="Seg_5492" s="T289">ЕТ: "Лучший из лучших", – отвечает.</ta>
            <ta e="T299" id="Seg_5493" s="T293">ЕТ: "Леменга хоть где раскапаю да съедаю".</ta>
            <ta e="T309" id="Seg_5494" s="T299">ЕТ: "Если такой лучший, почему даёшь себя долганским детям поймать?" – спросила.</ta>
            <ta e="T319" id="Seg_5495" s="T309">ЕТ: "Долганские дети под снегом спрячутся, я откуда могу знать? – говорит.</ta>
            <ta e="T326" id="Seg_5496" s="T319">Поэтому поддаюсь им, – говорит, – кушать когда прихожу", – говорит.</ta>
            <ta e="T331" id="Seg_5497" s="T326">ЕТ: "Долганские дети вы самые лучшие?" </ta>
            <ta e="T338" id="Seg_5498" s="T331">ЕТ: "Самые, самые, песца в любое время можем поймать", – отвечают. </ta>
            <ta e="T346" id="Seg_5499" s="T338">ЕТ: "Если такие самые лучшие, почему шаману даёте себя съесть?" –спросила.</ta>
            <ta e="T355" id="Seg_5500" s="T346">ЕТ: "Шаман очень сильный конечно, – сказал, – съем скажет да и скушает".</ta>
            <ta e="T356" id="Seg_5501" s="T355">ЕТ: "Шаман! </ta>
            <ta e="T358" id="Seg_5502" s="T356">Ты лучший?"</ta>
            <ta e="T361" id="Seg_5503" s="T358">ЕТ: "Лучший из лучших.</ta>
            <ta e="T371" id="Seg_5504" s="T361">ЕТ: Когда захочу поедаю плохо подумавший человек если будет", – говорит.</ta>
            <ta e="T379" id="Seg_5505" s="T371">ЕТ: "Если такой лучший, почему смерти поддаёшься? – спрашивает.</ta>
            <ta e="T386" id="Seg_5506" s="T379">ЕТ: "Смерть ведь никому ничего не говорит", – сказал.</ta>
            <ta e="T387" id="Seg_5507" s="T386">"Смерть! </ta>
            <ta e="T390" id="Seg_5508" s="T387">ЕТ: Ты самая?" – спросила.</ta>
            <ta e="T399" id="Seg_5509" s="T390">ЕТ: "Самая из самых тебя даже съем", – сказала и съела.</ta>
            <ta e="T402" id="Seg_5510" s="T399">ЕТ: На этом всё. </ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T403" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
